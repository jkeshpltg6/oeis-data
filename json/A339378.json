{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A339378",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 339378,
			"data": "0,2,3,4,5,7,7,8,9,13,11,17,13,15,14,16,17,25,19,21,16,27,23,25,25,29,27,23,29,68,31,32,38,49,32,59,37,51,40,57,41,66,43,43,52,55,47,59,49,57,44,45,53,59,36,81,46,61,59,84,61,63,76,64,38,102,67,81",
			"name": "Let n be a positive integer. For each prime divisor p of n, consider the highest power of p which does not exceed n. The sum a(n) of these powers is defined as the power-sum of n.",
			"comment": [
				"The idea of this sequence comes from the 85th Eötvös-Kürschák Competition of 1985, Class 9 - 12, Category 1, Round 1, Problem 2. In the problem it was asked to prove that there exist infinitely many positive integers n such as n \u003c a(n) (see link).",
				"The Kürschák Mathematical Competition, founded in 1894 in Hungary, but known as Eötvös Mathematical Competition until 1938, is the oldest modern mathematical competition for students in the world.",
				"For each prime divisor p of n, the highest power of p which does not exceed n is equal to p^(floor(log_p(n))) [for p=2, see A000523 and A053644].",
				"There exist two infinite families of numbers (see De Koninck \u0026 Mercier reference) that satisfy n \u003c a(n):",
				"--\u003e n = 2^k + 2, k\u003e=2 or n is in A052548 \\ {3,4}, and,",
				"--\u003e n = 2*p with odd prime p or n is in A100484 \\ {4}.",
				"Other integers like 12, 20, 30, 33, 36, 39, 42, 45, 48, 50 satisfy also n \u003c a(n)."
			],
			"reference": [
				"J.-M. De Koninck \u0026 A. Mercier, 1001 Problèmes en Théorie Classique des Nombres, Ellipses, 2004, Problème 683, pp. 89 and 294."
			],
			"link": [
				"Eötvös-Kürschák Competitions, \u003ca href=\"http://www.batmath.it/matematica/raccolte_es/ek_competitions/ek_competitions.pdf\"\u003eProblem 2\u003c/a\u003e, 85th Eötvös-Kürschák Competition 1985.",
				"\u003ca href=\"/index/O#Olympiads\"\u003eIndex to sequences related to Olympiads and other Mathematical competitions\u003c/a\u003e."
			],
			"formula": [
				"a(n) = Sum_{p | n} p^(floor(log_p(n))).",
				"a(n) = n iff n = p^k , p prime, k \u003e= 1 (A246655)."
			],
			"example": [
				"12 = 2^2 * 3^1; the highest power of 2 which does not exceed 12 is 2^3 and the highest power of 3 which does not exceed 12 is 3^2, hence a(12) = 2^3 + 3^2 = 2^(floor(log_2(12)) + 3^(floor(log_3(12)) = 17."
			],
			"maple": [
				"pf := n -\u003e NumberTheory:-PrimeFactors(n): a := n -\u003e add(p^ilog[p](n), p in pf(n)):",
				"seq(a(n), n=1..68); # _Peter Luschny_, Dec 07 2020"
			],
			"mathematica": [
				"f[n_, p_] := p^Floor[Log[p, n]]; a[1] = 0; a[n_] := Plus @@ (f[n, #] \u0026 /@ FactorInteger[n][[;; , 1]]); Array[a, 100] (* _Amiram Eldar_, Dec 06 2020 *)"
			],
			"program": [
				"(PARI) a(n) = my(f=factor(n)); sum(k=1, #f~, my(p=f[k,1]); p^logint(n, p)); \\\\ _Michel Marcus_, Dec 06 2020"
			],
			"xref": [
				"Cf. A000523, A052548, A053644, A100484, A246655."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Bernard Schott_, Dec 06 2020",
			"references": 1,
			"revision": 34,
			"time": "2020-12-08T10:24:01-05:00",
			"created": "2020-12-08T10:24:01-05:00"
		}
	]
}