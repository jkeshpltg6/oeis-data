{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A287874",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 287874,
			"data": "1,10,11,1010,101,1011,111,1011,1110,10101,1011,101011,1101,10111,11101,10100,10001,101110,10011,1010101,11111,101011,10111,101111,10110,101101,1111,1010111,11101,1011101,11111,10101,111011,1010001,101111,10101110,100101",
			"name": "Concatenate prime factorization as in A080670, but write everything in binary.",
			"comment": [
				"As in A080670 the prime factorization of n is written as p1^e1*...*pN^eN (except for exponents eK = 1 which are omitted), with all factors and exponents in binary (cf. A007088). Then \"^\" and \"*\" signs are dropped and all binary digits are concatenated.",
				"See A230625 for the terms written in base 10, and for further information (fixed points, trajectories)."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A287874/b287874.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A007088(A230625(n)). - _R. J. Mathar_, Jun 16 2017"
			],
			"example": [
				"a(1) = 1 by convention.",
				"a(2) = 10 (= 2 written in binary).",
				"a(4) = 1010 = concatenate(10,10), since 4 = 2^2 = 10[2] ^ 10[2].",
				"a(6) = 1011 = concatenate(10,11), since 6 = 2*3 = 10[2] * 11[2].",
				"a(8) = 1011 = concatenate(10,11), since 8 = 2^3 = 10[2] ^ 11[2]."
			],
			"maple": [
				"f:= proc(n) local F, L, i;",
				"    F:= map(op,subs(1=NULL, sort(ifactors(n)[2], (a,b) -\u003e a[1] \u003c b[1])));",
				"    F:= map(convert, F, binary);",
				"    L:= map(length,F);",
				"    L:= ListTools:-Reverse(ListTools:-PartialSums(ListTools:-Reverse(L)));",
				"    add(F[i]*10^L[i+1],i=1..nops(F)-1)+F[-1];",
				"end proc:",
				"f(1):= 1:",
				"map(f, [$1..100]); # _Robert Israel_, Jun 20 2017"
			],
			"mathematica": [
				"fn[1] = 1; fn[n_] := FromDigits[Flatten[IntegerDigits[DeleteCases[Flatten[FactorInteger[n]], 1], 2]]];",
				"Table[fn[n], {n, 37}] (* _Robert Price_, Mar 16 2020 *)"
			],
			"program": [
				"(PARI) A287874(n)=if(n\u003e1,fromdigits(concat(apply(binary,select(t-\u003et\u003e1,concat(Col(factor(n))~)))),10),1) \\\\ _M. F. Hasler_, Jun 21 2017",
				"(Python)",
				"from sympy import factorint",
				"def a(n):",
				"    f=factorint(n)",
				"    return 1 if n==1 else int(\"\".join(bin(i)[2:] + bin(f[i])[2:] if f[i]!=1 else bin(i)[2:] for i in f))",
				"print([a(n) for n in range(1, 101)]) # _Indranil Ghosh_, Jun 23 2017"
			],
			"xref": [
				"Cf. A080670, A230625, A230626, A230627, A287875."
			],
			"keyword": "nonn,base",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Jun 15 2017",
			"ext": [
				"Edited by _M. F. Hasler_, Jun 21 2017"
			],
			"references": 8,
			"revision": 33,
			"time": "2021-04-11T03:59:04-04:00",
			"created": "2017-06-15T15:02:05-04:00"
		}
	]
}