{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A156749",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 156749,
			"data": "-1,0,-1,-1,-2,-1,-1,0,-1,-1,-2,-2,-2,-1,-2,-2,-2,-1,-1,0,-1,-1,-2,-2,-2,-1,-1,-1,-2,-1,-1,-1,-2,-2,-3,-2,-2,-2,-3,-3,-4,-4,-4,-3,-3,-3,-3,-2,-2,-1,-2,-2,-3,-2,-2,-1,-1,-1,-1,-1,-1,-1,-2,-2,-3,-3,-3,-2,-3,-3",
			"name": "For all numbers k(n) congruent to -1 or +1 (mod 4) starting with k(n) = {3,5,7,9,11,...}, a(k(n)) is incremented by the congruence (mod 4) if k(n) is prime and by 0 if k(n) is composite.",
			"comment": [
				"The fact that a(k(n)) is predominantly negative exhibits the Chebyshev Bias (where the congruences that are not quadratic residues generally lead in the prime number races, at least for \"small\" integers, over the congruences that are quadratic residues).",
				"This bias seems caused (among other causes?) by the presence of all those squares (even powers) coprime to 4 taking away opportunities for primes to appear in the quadratic residue class +1 (mod 4), while the non-quadratic residue class -1 (mod 4) is squarefree.",
				"The density of squares congruent to +1 (mod 4) is 1/(4*sqrt(k(n))) since 1/2 of squares are congruent to +1 (mod 4), while the density of primes in either residue classes -1 or +1 (mod 4) is 1/(phi(4)*log(k(n))), with phi(4) = 2.",
				"Here 1 is quadratic residue mod 4, but 3 (or equivalently -1) is quadratic non-residue mod 4. All the even powers (included in the squares) map congruences {-1, +1} to {+1, +1} respectively and so contribute to the bias, whereas all the odd powers map {-1, +1} to {-1, +1} respectively and so do not contribute to the bias.",
				"One would then expect the ratio of this bias, if caused exclusively by the even powers, relative to the number of primes in either congruences to asymptotically tend towards to 0 as k(n) increases (since 1/(4*sqrt(k(n))) is o(1/(phi(4)*log(k(n)))).",
				"The persistence or not of such bias in absolute value then does not contradict The Prime Number Theorem for Arithmetic Progressions (Dirichlet) which states that the asymptotic (relative) ratio of the count of prime numbers in each congruence class coprime to m tends to 1 in the limit towards infinity. (Cf. 'Prime Number Races' link below.)",
				"Also, even if this bias grows in absolute value, it is expected to be drowned out (albeit very slowly) by the increasing fluctuations in the number of primes in each congruence classes coprime to 4 since, assuming the truth of the Riemann Hypothesis, their maximum amplitude would be, with x standing for k(n) in our case, h(x) = O(sqrt(x)*log(x)) \u003c= C*sqrt(x)*log(x) in absolute value which gives relative fluctuations of order h(x)/x = O(log(x)/sqrt(x)) \u003c= C*log(x)/sqrt(x) in the densities of primes pi(x, {4, 1})/x and pi(x, {4, 3})/x in either congruence classes.",
				"Since 1/(4*sqrt(x)) is o(log(x)/sqrt(x)) the bias will eventually be overwhelmed by the \"pink noise or nearly 1/f noise\" corresponding to the fluctuations in the prime densities in either congruence classes. The falsehood of the Riemann Hypothesis would imply even greater fluctuations since the RH corresponds to the minimal h(x).",
				"We get pink noise or nearly 1/f noise if we consider the prime density fluctuations of pi(x, {4, k})/x as an amplitude spectrum over x (with a power density spectrum of (C*log(x)/sqrt(x))^2 = ((C*log(x))^2)/x and see x as the frequency f. This power density spectrum is then nearly 1/x and would have nearly equal energy (although slowly increasing as (C*log(x))^2) for each octave of x. (Cf. 'Prime Numbers: A Computational Perspective' link below.)",
				"Among the positive integers k(n) up to 100000 that are congruent to -1 or +1 (mod 4) [indexed from n = 1 to 49999, with k(n) = 4*ceiling(n/2) + (-1)^n], a tie is attained or maintained, with a(k(n)) = 0, for only 34 integers and that bias favoring the non-quadratic residue class -1 (mod 4) gets violated only once, i.e., a(k(n) = +1, for index n = 13430 (corresponding to the prime k(n) = 26861 congruent to +1 (mod 4) since n is even) where the congruence + 1 leads once!"
			],
			"reference": [
				"Richard E. Crandall and Carl Pomerance, Prime Numbers: A Computational Perspective"
			],
			"link": [
				"Daniel Forgues, \u003ca href=\"/A156749/b156749.txt\"\u003eTable of n, a(n) for n = 1..49999\u003c/a\u003e",
				"A. Granville and G. Martin, \u003ca href=\"http://www.arxiv.org/abs/math.NT/0408319\"\u003ePrime Number Races\u003c/a\u003e, arXiv:math/0408319 [math.NT], 2004.",
				"Eric Weisstein, \u003ca href=\"http://mathworld.wolfram.com/ChebyshevBias.html\"\u003eChebyshev Bias\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Pink_noise\"\u003ePink noise\u003c/a\u003e"
			],
			"formula": [
				"a(n) = -A066520(2*n+1) = A066339(2*n+1) - A066490(2*n+1). [_Jonathan Sondow_, May 17 2013]"
			],
			"mathematica": [
				"Table[Which[!PrimeQ[2*n+1], 0, Mod[2*n+1, 4] == 1, 1, True, -1], {n, 1, 100}] // Accumulate (* _Jean-François Alcover_, Dec 09 2014 *)"
			],
			"xref": [
				"Cf. A066339, A066490, A066520, A007350, A007351, A038691, A096628, A156707, A156709, A156706, A101264, A075743."
			],
			"keyword": "sign",
			"offset": "1,5",
			"author": "_Daniel Forgues_, Feb 14 2009",
			"ext": [
				"Edited by _Daniel Forgues_, Mar 01 2009, Mar 29 2009"
			],
			"references": 12,
			"revision": 24,
			"time": "2019-11-25T00:58:16-05:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}