{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A182222",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 182222,
			"data": "1,1,1,2,2,1,4,4,3,1,10,10,9,4,1,26,26,25,16,5,1,76,76,75,56,25,6,1,232,232,231,197,105,36,7,1,764,764,763,694,441,176,49,8,1,2620,2620,2619,2494,1785,856,273,64,9,1,9496,9496,9495,9244,7308,3952,1506,400,81,10,1",
			"name": "Number T(n,k) of standard Young tableaux of n cells and height \u003e= k; triangle T(n,k), n\u003e=0, 0\u003c=k\u003c=n, read by rows.",
			"comment": [
				"Also number of self-inverse permutations in S_n with longest increasing subsequence of length \u003e= k.  T(4,3) = 4: 1234, 1243, 1324, 2134;  T(3,0) = T(3,1) = 4: 123, 132, 213, 321;  T(5,3) = 16: 12345, 12354, 12435, 12543, 13245, 13254, 14325, 14523, 15342, 21345, 21354, 21435, 32145, 34125, 42315, 52341."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A182222/b182222.txt\"\u003eRows n = 0..50, flattened\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Involution_(mathematics)\"\u003eInvolution (mathematics)\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Young_tableau\"\u003eYoung tableau\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = A182172(n,n) - A182172(n,k-1) for k\u003e0, T(n,0) = A182172(n,n)."
			],
			"example": [
				"T(4,3) = 4, there are 4 standard Young tableaux of 4 cells and height \u003e= 3:",
				"  +---+   +------+   +------+   +------+",
				"  | 1 |   | 1  2 |   | 1  3 |   | 1  4 |",
				"  | 2 |   | 3 .--+   | 2 .--+   | 2 .--+",
				"  | 3 |   | 4 |      | 4 |      | 3 |",
				"  | 4 |   +---+      +---+      +---+",
				"  +---+",
				"Triangle T(n,k) begins:",
				"    1;",
				"    1,   1;",
				"    2,   2,   1;",
				"    4,   4,   3,   1;",
				"   10,  10,   9,   4,   1;",
				"   26,  26,  25,  16,   5,   1;",
				"   76,  76,  75,  56,  25,   6,  1;",
				"  232, 232, 231, 197, 105,  36,  7,  1;",
				"  764, 764, 763, 694, 441, 176, 49,  8,  1;",
				"  ..."
			],
			"maple": [
				"h:= proc(l) local n; n:=nops(l); add(i, i=l)! /mul(mul(1+l[i]-j+",
				"       add(`if`(l[k]\u003e=j, 1, 0), k=i+1..n), j=1..l[i]), i=1..n)",
				"    end:",
				"g:= proc(n, i, l) option remember;",
				"      `if`(n=0, h(l), `if`(i\u003c1, 0, `if`(i=1, h([l[], 1$n]),",
				"        g(n, i-1, l) +`if`(i\u003en, 0, g(n-i, i, [l[], i])))))",
				"    end:",
				"T:= (n, k)-\u003e g(n, n, []) -`if`(k=0, 0, g(n, k-1, [])):",
				"seq(seq(T(n, k), k=0..n), n=0..12);"
			],
			"mathematica": [
				"h[l_] := Module[{n = Length[l]}, Sum[i, {i, l}]! / Product[ Product[1 + l[[i]] - j + Sum [If[l[[k]] \u003e= j, 1, 0], {k, i+1, n}], {j, 1, l[[i]]}], {i, 1, n}]];",
				"g[n_, i_, l_] := g[n, i, l] = If[n == 0, h[l], If[i \u003c 1, 0, If[i == 1, h[Join[l, Array[1\u0026, n]]], g [n, i-1, l] + If[i \u003e n, 0, g[n-i, i, Append[l, i]]]]]];",
				"t[n_, k_] := g[n, n, {}] - If[k == 0, 0, g[n, k-1, {}]];",
				"Table[Table[t[n, k], {k, 0, n}], {n, 0, 12}] // Flatten (* _Jean-François Alcover_, Dec 12 2013, translated from Maple *)"
			],
			"xref": [
				"Columns 0-10 give: A000085, A000085 (for n\u003e0), A001189, A218263, A218264, A218265, A218266, A218267, A218268, A218269, A218262.",
				"Diagonal and lower diagonals give: A000012, A000027(n+1), A000290(n+1) for n\u003e0, A131423(n+1) for n\u003e1.",
				"T(2n,n) gives A318289.",
				"Cf. A047884, A049400, A182172."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Alois P. Heinz_, Apr 19 2012",
			"references": 11,
			"revision": 38,
			"time": "2021-10-29T05:06:06-04:00",
			"created": "2012-04-21T04:22:37-04:00"
		}
	]
}