{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A160953",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 160953,
			"data": "1,511,9841,130816,488281,5028751,6725601,33488896,64566801,249511591,235794769,1287360256,883708281,3436782111,4805173321,8573157376,7411742281,32993635311,17927094321,63874967296,66186639441",
			"name": "a(n) = Sum_{d|n} Moebius(n/d)*d^(b-1)/phi(n) for b = 10.",
			"comment": [
				"a(n) is the number of lattices L in Z^9 such that the quotient group Z^9 / L is C_n. - _Álvar Ibeas_, Nov 03 2015"
			],
			"reference": [
				"J. H. Kwak and J. Lee, Enumeration of graph coverings, surface branched coverings and related group theory, in Combinatorial and Computational Mathematics (Pohang, 2000), ed. S. Hong et al., World Scientific, Singapore 2001, pp. 97-161. See p. 134."
			],
			"link": [
				"Enrique Pérez Herrero, \u003ca href=\"/A160953/b160953.txt\"\u003eTable of n, a(n) for n = 1..5000\u003c/a\u003e",
				"Enrique Pérez Herrero, \u003ca href=\"https://sites.google.com/site/psychgeom/psychgeom/JordanTotientFunction.m?attredirects=0\u0026amp;d=1\"\u003eMathematica Package: Jordan Totient Function.\u003c/a\u003e"
			],
			"formula": [
				"a(n) = J_9(n)/phi(n) = A069094(n)/A000010(n).",
				"From _Álvar Ibeas_, Nov 03 2015: (Start)",
				"Multiplicative with a(p^e) = p^(8e-8) * (p^9-1) / (p-1).",
				"For squarefree n, a(n) = A000203(n^8).",
				"(End)"
			],
			"maple": [
				"A160953 := proc(n)",
				"    add(numtheory[mobius](n/d)*d^9,d=numtheory[divisors](n)) ;",
				"    %/numtheory[phi](n) ;",
				"end proc:",
				"for n from 1 to 5000 do",
				"    printf(\"%d %d\\n\",n,A160953(n)) ;",
				"end do: # _R. J. Mathar_, Mar 14 2016"
			],
			"mathematica": [
				"JordanTotient[n_,k_:1]:=DivisorSum[n,#^k*MoebiusMu[n/#]\u0026]/;(n\u003e0)\u0026\u0026IntegerQ[n];",
				"A160953[n_]:=JordanTotient[n,9]/JordanTotient[n];"
			],
			"program": [
				"(PARI) vector(100, n, sumdiv(n^8, d, if(ispower(d, 9), moebius(sqrtnint(d, 9))*sigma(n^8/d), 0))) \\\\ _Altug Alkan_, Nov 05 2015",
				"(PARI) a(n) = {f = factor(n); for (i=1, #f~, p = f[i,1]; f[i,1] = p^(8*f[i,2]-8)*(p^9-1)/(p-1); f[i,2] = 1;); factorback(f);} \\\\ _Michel Marcus_, Nov 12 2015"
			],
			"keyword": "nonn,mult",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Nov 19 2009",
			"ext": [
				"Definition corrected by _Enrique Pérez Herrero_, Oct 30 2010"
			],
			"references": 4,
			"revision": 26,
			"time": "2016-03-14T17:30:12-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}