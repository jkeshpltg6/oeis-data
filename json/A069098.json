{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A069098",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 69098,
			"data": "1,1,4,1,2,1,4,27,8,1,12,1,32,9,256,1,432,1,16,81,512,1,12,3125,2048,19683,256,1,72,1,65536,6561,32768,25,1728,1,131072,59049,32,1,2592,1,65536,135,2097152,1,6912,823543,800000,4782969,1048576,1,2519424,15625,2048,43046721,134217728,1,144,1,536870912,243,65536,390625,3359232,1",
			"name": "Number of minimal monic annihilator polynomials over the ring of integers modulo n.",
			"link": [
				"Antti Karttunen, \u003ca href=\"/A069098/b069098.txt\"\u003eTable of n, a(n) for n = 2..2051\u003c/a\u003e",
				"A. Vardy, \u003ca href=\"/A069098/a069098.txt\"\u003eComments and C program\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 1 if n is prime.",
				"Let n = Product_{i=1..m} p_i^e_i be the prime factorization of n. For p prime and integers k, q, define N(k, p, q) = p^(Sum_{j=0..k-1} b(j)) where b(j) is the largest integer b in {0, 1, 2, ..., q} such that p^b divides j!. Then a(n) = Product_{i=1..m} N(S(n), p_i, e_i) where S(n) is the n-th Kempner number (sequence A002034), i.e., S(n) is the smallest integer k such that n divides k!. - Navin Kashyap (nkashyap(AT)ece.ucsd.edu), Aug 07 2002"
			],
			"example": [
				"a(6)=2 because there are exactly two minimal annihilator polynomials over Z_6, namely X^3 + 5x and X^3 + 3x^2 + 2x."
			],
			"mathematica": [
				"A002034[n_] := Module[ {m = 1}, While[ !IntegerQ[m!/n], m++]; m]; b[j_, p_, q_] := Module[ {m = q}, While[ !Divisible[j!, p^m], m--]; m]; nn[k_, p_, q_] := p^Sum[b[j, p, q], {j, 0, k-1}]; a[n_?PrimeQ] = 1; a[n_] := Module[ {fi, m, pp, ee}, fi = FactorInteger[n]; m = Length[fi]; pp = fi[[All, 1]]; ee = fi[[All, 2]]; Product[ nn[ A002034[n], pp[[i]], ee[[i]]], {i, 1, m}]]; Table[ a[n], {n, 2, 50}] (* _Jean-François Alcover_, Dec 19 2011, after Navin Kashyap *)"
			],
			"program": [
				"(PARI)",
				"A002034(n) = if(1==n,n,my(s=factor(n)[, 1], k=s[#s], f=Mod(k!, n)); while(f, f*=k++); (k)); \\\\ From A002034",
				"A069098auxN(k, p, q) = { my(s=0); for(j=0,k-1,forstep(b=q,0,-1,if(0==lift(Mod(j!,(p^b))), s += b; break))); (p^s); };",
				"A069098(n) = { my(f=factor(n), K=A002034(n)); prod(i=1, #f~, A069098auxN(K, f[i, 1], f[i, 2])); }; \\\\ _Antti Karttunen_, Oct 09 2018"
			],
			"xref": [
				"Cf. A002034."
			],
			"keyword": "nonn,nice",
			"offset": "2,3",
			"author": "Alexander Vardy (vardy(AT)montblanc.ucsd.edu), Apr 05 2002",
			"ext": [
				"More terms from Navin Kashyap (nkashyap(AT)ece.ucsd.edu), Aug 07 2002",
				"Data section further extended by _Antti Karttunen_, Oct 09 2018"
			],
			"references": 1,
			"revision": 20,
			"time": "2018-10-09T15:15:13-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}