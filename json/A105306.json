{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A105306",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 105306,
			"data": "1,1,1,2,2,1,4,5,3,1,8,12,9,4,1,16,28,25,14,5,1,32,64,66,44,20,6,1,64,144,168,129,70,27,7,1,128,320,416,360,225,104,35,8,1,256,704,1008,968,681,363,147,44,9,1,512,1536,2400,2528,1970,1182,553,200,54,10,1,1024",
			"name": "Triangle read by rows: T(n,k) is the number of directed column-convex polyominoes of area n, having the top of the rightmost column at height k.",
			"comment": [
				"From _Gary W. Adamson_, Apr 24 2005: (Start)",
				"Let A be the array",
				"  1, 0, 0, 0, 0, 0, ...",
				"  0, 1, 0, 0, 0, 0, ...",
				"  1, 0, 1, 0, 0, 0, ...",
				"  0, 2, 0, 1, 0, 0, ...",
				"  1, 0, 3, 0, 1, 0, ...",
				"  0, 3, 0, 4, 0, 1, ...",
				"  ...",
				"where columns are bin(n,k) with alternating zeros. (Row sums = 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, ...(Fibonacci numbers).) Let P = infinite lower triangular Pascal triangle matrix (A007318). Form P * A: this gives the rows of the present sequence. [Comment corrected by _Philippe Deléham_, Dec 09 2008] (End)",
				"T(n,k) is the number of nondecreasing Dyck paths of semilength n, having height of rightmost peak equal to k. Example: T(4,1)=4 because we have UDUDUDUD, UDUUDDUD, UUDDUDUD and UUUDDDUD, where U=(1,1) and D=(1,-1). Sum of row n = Fibonacci(2n-1) (A001519). Basically the same as A062110.",
				"T(n,k) is the number of permutations of [n] with length n-k that avoid the patterns 321 and 3412. - _Bridget Tenner_, Sep 28 2005",
				"T(2*n-1,n)/n = A001003(n-1) (little Schroeder numbers). Proof with Lagrange inversion of inverse of g.f. of A001003.",
				"Row sums = odd-indexed Fibonacci numbers.",
				"Diagonal sums: A077998. - _Philippe Deléham_, Nov 16 2008",
				"Central coefficients are A176479. Inverse is A125692. - _Paul Barry_, Apr 18 2010",
				"Riordan matrix ((1-x)/(1-2x),(x-x^2)/(1-2x)). - _Emanuele Munarini_, Mar 22 2011",
				"T(n,k) is the number of ideals in the fence Z(2n-1) with  n-k elements of rank 0. - _Emanuele Munarini_, Mar 22 2011",
				"Triangle T(n,k), 1 \u003c= k \u003c= n, read by rows, given by (0,1,1,0,0,0,0,0,0,0,...) DELTA (1,0,0,0,0,0,0,0,0,0,...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Oct 30 2011",
				"T(n,k) is the number of permutations of [n] for which k is equal to both the length and reflection length. - _Bridget Tenner_, Feb 22 2012"
			],
			"reference": [
				"V. E. Hoggatt, Jr. and Marjorie Bicknell, editors: \"A Primer for the Fibonacci Numbers\", 1970, p. 87."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A105306/b105306.txt\"\u003eTable of n, a(n) for n = 1..11325\u003c/a\u003e (rows 1 \u003c= n \u003c= 150, flattened)",
				"E. Barcucci, A. Del Lungo, S. Fezzi and R. Pinzani, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(97)82778-1\"\u003eNondecreasing Dyck paths and q-Fibonacci numbers\u003c/a\u003e, Discrete Math., 170, 1997, 211-217.",
				"Eunice Y. S. Chan, Robert M. Corless, Laureano Gonzalez-Vega, J. Rafael Sendra, Juana Sendra, Steven E. Thornton, \u003ca href=\"https://arxiv.org/abs/1809.10664\"\u003eBohemian Upper Hessenberg Toeplitz Matrices\u003c/a\u003e, arXiv:1809.10664 [cs.SC], 2018.",
				"Eunice Y. S. Chan, Robert M. Corless, Laureano Gonzalez-Vega, J. Rafael Sendra, Juana Sendra, \u003ca href=\"https://arxiv.org/abs/1907.10677\"\u003eUpper Hessenberg and Toeplitz Bohemians\u003c/a\u003e, arXiv:1907.10677 [cs.SC], 2019.",
				"E. Deutsch and H. Prodinger, \u003ca href=\"http://dx.doi.org/10.1016/S0304-3975(03)00222-6\"\u003eA bijection between directed column-convex polyominoes and ordered trees of height at most three\u003c/a\u003e, Theoretical Comp. Science, 307, 2003, 319-325.",
				"Sergi Elizalde, Rigoberto Flórez, and José Luis Ramírez, \u003ca href=\"https://doi.org/10.26493/1855-3974.2478.d1b\"\u003eEnumerating symmetric peaks in non-decreasing Dyck paths\u003c/a\u003e, Ars Mathematica Contemporanea (2021).",
				"Milan Janjić, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL21/Janjic/janjic93.html\"\u003eWords and Linear Recurrences\u003c/a\u003e, J. Int. Seq. 21 (2018), #18.1.4.",
				"V. V. Kruchinin and D. V. Kruchinin, \u003ca href=\"http://arxiv.org/abs/1206.0877\"\u003eA Method for Obtaining Generating Function for Central Coefficients of Triangles\u003c/a\u003e, arXiv preprint arXiv:1206.0877 [math.CO], 2012, and \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL15/Kruchinin/kruchinin5.html\"\u003eJ. Int. Seq. 15 (2012) #12.9.3\u003c/a\u003e.",
				"E. Munarini, N. Zagaglia Salvi, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(02)00378-3\"\u003eOn the Rank Polynomial of the Lattice of Order Ideals of Fences and Crowns\u003c/a\u003e, Discrete Mathematics 259 (2002), 163-177.",
				"T. K. Petersen and B. E. Tenner, \u003ca href=\"http://arxiv.org/abs/1202.4765\"\u003eThe depth of a permutation\u003c/a\u003e, arXiv:1202.4765 [math.CO], 2012-2014..",
				"M. Pétréolle, \u003ca href=\"http://arxiv.org/abs/1403.1130\"\u003eCharacterization of Cyclically Fully commutative elements in finite and affine Coxeter Groups\u003c/a\u003e, arXiv preprint arXiv:1403.1130 [math.GR], 2014.",
				"Bridget Eileen Tenner, \u003ca href=\"https://arxiv.org/abs/2001.05011\"\u003eInterval structures in the Bruhat and weak orders\u003c/a\u003e, arXiv:2001.05011 [math.CO], 2020.",
				"S.-n. Zheng and S.-l. Yang, \u003ca href=\"http://dx.doi.org/10.1155/2014/848374\"\u003eOn the-Shifted Central Coefficients of Riordan Matrices\u003c/a\u003e, Journal of Applied Mathematics, Volume 2014, Article ID 848374, 8 pages."
			],
			"formula": [
				"T(n,k) = sum(binomial(k+j, k-1)*binomial(n-k-1, j), j=0..n-k-1) (0\u003c=k\u003c=n). (This appears to be incorrect. - _Emanuele Munarini_, Mar 22 2011)",
				"G.f.: t*z*(1-z)/(1 - 2*z - t*z*(1-z)).",
				"From _Emanuele Munarini_, Mar 22 2011: (Start)",
				"T(n,k) = Sum_{i=0..n-k} binomial(k+1,i)*binomial(n-i,k)*(-1)^i*2^(n-k-i).",
				"T(n,k) = Sum_{i=0..n-k} binomial(k+1,i)*M(i,n-k-i)*2^(n-k-i), where M(n,k) = n*(n+1)*(n+2)*...*(n+k-1)/k!. (End)",
				"T(n,k) = 2*T(n-1,k) + T(n-1,k-1) - T(n-2,k-1), T(0,0) = T(1,0) = T(1,1) = 1, T(n,k) = 0 if k\u003en or if k\u003c0. - _Philippe Deléham_, Oct 30 2013"
			],
			"example": [
				"Triangle begins:",
				"   1;",
				"   1,   1;",
				"   2,   2,   1;",
				"   4,   5,   3,   1;",
				"   8,  12,   9,   4,   1;",
				"  16,  28   25,  14,   5,   1;",
				"  32,  64,  66,  44,  20,   6,   1;",
				"  64, 144, 168, 129,  70,  27,   7,   1;",
				"  ...",
				"From _Paul Barry_, Apr 18 2010: (Start)",
				"Production matrix is",
				"   1,  1;",
				"   1,  1,  1;",
				"   0,  1,  1,  1;",
				"  -1,  0,  1,  1,  1;",
				"   0, -1,  0,  1,  1,  1;",
				"   2,  0, -1,  0,  1,  1,  1;",
				"   0,  2,  0, -1,  0,  1,  1,  1;",
				"  -5,  0,  2,  0, -1,  0,  1,  1,  1;",
				"   0, -5,  0,  2,  0, -1,  0,  1,  1,  1;",
				"  14,  0, -5,  0,  2,  0, -1,  0,  1,  1,  1; (End)"
			],
			"maple": [
				"T:=proc(n,k) if k\u003cn-1 then sum(binomial(k+j,k-1)*binomial(n-k-1,j),j=0..n-k-1) elif k=n-1 then n-1 elif k=n then 1 else 0 fi end: for n from 1 to 12 do seq(T(n,k),k=1..n) od; # yields sequence in triangular form"
			],
			"mathematica": [
				"t[n_, k_] := 2^(n-2*k-1)*Binomial[n, k]*Hypergeometric2F1[-k-1, -k, -n, -1]; t[n_, n_] = 1; Table[t[n, k], {n, 0, 12}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Jan 28 2014 *)"
			],
			"program": [
				"(Maxima) create_list(sum(binomial(k+1,i)*binomial(n-i,k)*(-1)^i*2^(n-k-i),i,0,n-k),n,0,8,k,0,n); /* _Emanuele Munarini_, Mar 22 2011 */"
			],
			"xref": [
				"Cf. A001519. Essentially the same array as A062110.",
				"Row sums = A001519(n-1), n \u003e= 1."
			],
			"keyword": "nonn,tabl",
			"offset": "1,4",
			"author": "_Emeric Deutsch_, Apr 25 2005",
			"ext": [
				"Entry revised by _N. J. A. Sloane_, Apr 27 2007"
			],
			"references": 8,
			"revision": 83,
			"time": "2021-07-14T11:37:16-04:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}