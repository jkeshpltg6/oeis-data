{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A347354",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 347354,
			"data": "1,2,1,3,4,1,2,5,1,6,1,2,7,1,3,2,1,8,1,4,2,1,9,10,1,2,11,1,3,1,2,12,1,4,13,1,2,1,14,15,1,2,3,1,6,2,1,4,1,16,2,1,17,18,1,2,1,3,5,1,2,19,1,4,2,1,20,1,3,21,1,2,22,1,9,1,2,4,1,8,2,1,3",
			"name": "a(n) = sum of T(n,k) - T(n-1,k) for row n of A347285.",
			"comment": [
				"If for row n, k \u003e A089576(n-1), we interpret T(n-1,k) = 0.",
				"Also the length of d = T(n,k) - T(n-1,k) in row n of A347285 such that d \u003e 1, with 0 \u003c= d \u003c= 1.",
				"Compactification of A347284 via indices k of primorials A002110(k). This is the most efficient compactification of A347284, superior to binary compactification via A347287. It employs the fact that A347284 concerns products of primorials, i.e., is a subset of A025487.",
				"We can construct row n of A347285 by summing a constant array of a(k) 1's for 1 \u003c= k \u003c= n-1."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A347354/b347354.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"A347284(n) = Product_{k=1..n} A002110(a(k))."
			],
			"example": [
				"Relation of a(n) and irregular triangle A347285, placing \".\" after the term in the current row where T(n,k) no longer exceeds T(n-1,k). Since the rows of A347285 reach a fixed point of 0, we interpret T(n,k) for vacant T(n-1,k) as exceeding same.",
				"n    Row n of A347285   a(n)",
				"-----------------------------",
				"0:    0",
				"1:    1.                   1",
				"2:    2  1.                2",
				"3:    3. 1                 1",
				"4:    4  2  1.             3",
				"5:    5  3  2  1.          4",
				"6:    6. 3  2  1           1",
				"7:    7  4. 2  1           2",
				"8:    8  5  3  2  1.       5",
				"9:    9. 5  3  2  1        1",
				"10:  10  6  4  3  2  1.    6",
				"...",
				"a(3) = 1 since row 3 of A347285 has {3,1} while row 2 has {2,1}; only the first term of the former exceeds the analogous term in the latter.",
				"a(4) = 3 since row 4 = {4,2,1} and row 3 = {3,1}; all 3 terms of the former are larger than the analogous term in the latter, etc."
			],
			"mathematica": [
				"Block[{nn = 84, a = {0}, c, e, m}, e[1] = 0; Do[c = 1; e[1]++; Do[Set[m, j]; Which[e[j - 1] == 1, Break[], IntegerQ@ e[j], If[e[j] \u003c #, e[j]++; c++] \u0026@ Floor@ Log[Prime[j], Prime[j - 1]^e[j - 1]], True, Set[e[j], 1]], {j, 2, k}]; AppendTo[a, c + Boole[c == m - 2]], {k, 2, nn}]; MapAt[# - 1 \u0026, a, 4]]"
			],
			"xref": [
				"Cf. A002110, A025487, A089576, A347284, A347285, A347287."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Michael De Vlieger_, Sep 16 2021",
			"references": 4,
			"revision": 10,
			"time": "2021-09-25T14:59:25-04:00",
			"created": "2021-09-22T18:49:55-04:00"
		}
	]
}