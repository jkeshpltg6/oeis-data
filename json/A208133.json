{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A208133",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 208133,
			"data": "1,2,2,8,2,4,2,12,9,4,2,16,2,4,4,31,2,18,2,16,4,4,2,24,11,4,14,16,2,8,2,42,4,4,4,72,2,4,4,24,2,8,2,16,18,4,2,62,13,22,4,16,2,28,4,24,4,4,2,32,2,4,18,90,4,8,2,16,4,8,2,108,2,4,22,16",
			"name": "Total number of subgroups of rank \u003c= 2 of a certain class of abelian groups of order n defined as direct products of Z/(mZ) X Z/(kZ) with m|k.",
			"comment": [
				"Level function l_tau^2(n) of Bhowmik and Wu.",
				"Records occur at 1, 2, 4, 8, 12, 16, 32, 36, 64, 72, 108, 128, 144, 288, 432, 576, 1152, 1296, 2304, 3600, 5184, 7200, 9216, 10368, 14112, 14400, 20736, 28224, 28800, 32400, 57600, ... and they are: 1, 2, 8, 12, 16, 31, 42, 72, 90, 108, 112, 116, 279, 378, 434, 810, 1044, 1302, 2025, 3069, 3780, 4158, 4644, 4872, 4914, 8910, 9450, 10530, 11484, 14322, 22275, ... - _Antti Karttunen_, Mar 21 2018"
			],
			"reference": [
				"A. Laurincikas, The universality of Dirichlet series attached to finite Abelian groups, in \"Number Theory\", Proc. Turku Sympos. on Number Theory, May 31-June 4, 1999, p 179."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A208133/b208133.txt\"\u003eTable of n, a(n) for n = 1..65537\u003c/a\u003e",
				"G. Bhowmik, Jie Wu, \u003ca href=\"http://dx.doi.org/10.1515/crll.2001.004\"\u003eZeta function of subgroups of abelian groups and average orders\u003c/a\u003e, J. reine angew. Math. 530 (2001) 1-15.",
				"Vaclav Kotesovec, \u003ca href=\"/A208133/a208133.jpg\"\u003eGraph - the asymptotic ratio (1000000 terms)\u003c/a\u003e"
			],
			"formula": [
				"Dirichlet g.f.: zeta(s)^2*zeta(2s)^2*zeta(2s-1)*Product_{primes p} (1 + 1/p^(2s) - 2/p^(3s)).",
				"Sum_{k=1..n} a(k) ~ c * Pi^4 * log(n)^2 * n / 144, where c = A330594 = Product_{primes p} (1 + 1/p^2 - 2/p^3) = 1.10696011195321767665117913000743959294954883365812241904313404497877733241... - _Vaclav Kotesovec_, Dec 18 2019",
				"More precise asymptotics: Let f(s) = Product_{primes p} (1 + 1/p^(2*s) - 2/p^(3*s)), then Sum_{k=1..n} a(k) ~ n*Pi^2 * (Pi^2 * f(1) * log(n)^2 + 2*Pi^2 * log(n) * (f(1) * (-1 + 8*gamma - 48*log(A) + 4*log(2*Pi)) + f'(1)) + Pi^2 * (2*f(1)*(1 + 25*gamma^2 + 576*log(A)^2 + log(A) * (48 - 96*log(2*Pi)) - 8*gamma * (1 + 36*log(A) - 3*log(2*Pi)) - 4*log(2*Pi) + 4*log(2*Pi)^2 - 6*sg1) + 2*(-1 + 8*gamma - 48*log(A) + 4*log(2*Pi))*f'(1) + f''(1)) + 48*f(1)*zeta''(2)) / 144, where f(1) = A330594, f'(1) = A330594 * (-A335705) = f(1) * Sum_{primes p} = -2*(p-3) * log(p) / (p^3 + p - 2) = -0.087825458097278818094375273108270679512035928574..., f''(1) = A330594 * (A335705^2 + A335706) = f'(1)^2/f(1) + f(1) * Sum_{primes p} = 2*p*(2*p^3 - 9*p^2 - 1) * log(p)^2) / (p^3 + p - 2)^2 =  0.26722508718782634450711076996710402451611235402675360769..., zeta''(2) = A201994, A is the Glaisher-Kinkelin constant A074962, gamma is the Euler-Mascheroni constant A001620 and sg1 is the first Stieltjes constant (see A082633). - _Vaclav Kotesovec_, Jun 18 2020"
			],
			"maple": [
				"L300828 := [ 1, 0, 0, 1, 0, 0, 0, -2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, -2, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0",
				"] ;",
				"L010052 := [ 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];",
				"L037213 := [ 1, 0, 0, 2, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ] ;",
				"Lx := DIRICHLET(L300828,L037213) ;",
				"Lx := DIRICHLET(Lx,L010052) ;",
				"Lx := DIRICHLET(Lx,L010052) ;",
				"Lx := MOBIUSi(Lx) ;",
				"Lx := MOBIUSi(Lx) ;",
				"# Name of initial list L1 changed to L300828 to refer to sequence A300828 by _Antti Karttunen_, Mar 21 2018"
			],
			"program": [
				"(PARI)",
				"A037213(n) = if(issquare(n),sqrtint(n),0);",
				"A300828(n) = { if(1==n, return(1)); my(val=1, v=factor(n), d=matsize(v)[1]); for(i=1,d, if(v[i,2] \u003c 2 || v[i,2] \u003e 3, return(0)); if (v[i,2] == 3, val *= -2)); return(val); };",
				"a208133s1(n) = sumdiv(n,d,A037213(n/d)*A300828(d));",
				"a208133s2(n) = sumdiv(n,d,issquare(n/d)*a208133s1(d));",
				"a208133s3(n) = sumdiv(n,d,issquare(n/d)*a208133s2(d));",
				"a208133s4(n) = sumdiv(n,d,a208133s3(d));",
				"A208133(n) = sumdiv(n,d,a208133s4(d)); \\\\ _Antti Karttunen_, Mar 21 2018, after _R. J. Mathar_'s Maple code",
				"(PARI) for(n=1, 100, print1(direuler(p=2, n, (1 + X + 2*X^2)/(1 - X)^3/(1 + X)^2/(1 - p*X^2))[n], \", \")) \\\\ _Vaclav Kotesovec_, Jun 18 2020"
			],
			"xref": [
				"Cf. A010052, A037213, A300828."
			],
			"keyword": "nonn,mult",
			"offset": "1,2",
			"author": "_R. J. Mathar_, Mar 29 2012",
			"references": 5,
			"revision": 33,
			"time": "2020-06-18T04:41:23-04:00",
			"created": "2012-03-30T15:21:51-04:00"
		}
	]
}