{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A349407",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 349407,
			"data": "1,1,3,11,3,17,7,5,9,29,7,35,13,9,15,47,11,53,19,13,21,65,15,71,25,17,27,83,19,89,31,21,33,101,23,107,37,25,39,119,27,125,43,29,45,137,31,143,49,33,51,155,35,161,55,37,57,173,39,179,61,41,63,191,43",
			"name": "The Farkas map: a(n) = x/3 if x mod 3 = 0; a(n) = (3x+1)/2 if x mod 3 \u003c\u003e 0 and x mod 4 = 3; a(n) = (x+1)/2 if x mod 3 \u003c\u003e 0 and x mod 4 = 1, where x = 2n-1.",
			"comment": [
				"The official Farkas map is given by: if x mod 3 = 0, F(x) = x/3; if x mod 4 = 3, F(x) = (3x+1)/2; otherwise F(x) = (x+1)/2. The map takes a positive odd integer and produces a positive odd integer.",
				"Farkas proves that the trajectory of the iterates of the map starting from any positive odd integer always reaches 1.",
				"If displayed as a rectangular array with six columns, the columns are A016921, A016813, A016945, A350461, A004767, A239129 (see example). - _Omar E. Pol_, Jan 01 2022"
			],
			"reference": [
				"H. M. Farkas, \"Variants of the 3N+1 Conjecture and Multiplicative Semigroups\", in Entov, Pinchover and Sageev, \"Geometry, Spectral Theory, Groups, and Dynamics\", Contemporary Mathematics, vol. 387, American Mathematical Society, 2005, p. 121.",
				"J. C. Lagarias, ed., The Ultimate Challenge: The 3x+1 Problem, American Mathematical Society, 2010, p. 74."
			],
			"link": [
				"\u003ca href=\"/index/Rec#order_12\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,0,0,0,0,2,0,0,0,0,0,-1)."
			],
			"example": [
				"From _Omar E. Pol_, Jan 01 2022: (Start)",
				"Written as a rectangular array with six columns read by rows the sequence begins:",
				"   1,  1,  3,  11,  3,  17;",
				"   7,  5,  9,  29,  7,  35;",
				"  13,  9, 15,  47, 11,  53;",
				"  19, 13, 21,  65, 15,  71;",
				"  25, 17, 27,  83, 19,  89;",
				"  31, 21, 33, 101, 23, 107;",
				"  37, 25, 39, 119, 27, 125;",
				"  43, 29, 45, 137, 31, 143;",
				"  49, 33, 51, 155, 35, 161;",
				"  55, 37, 57, 173, 39, 179;",
				"...",
				"(End)"
			],
			"mathematica": [
				"nterms=100;Table[x=2n-1;If[Mod[x,3]==0,x/=3,If[Mod[x,4]==3,x=(3x+1)/2,x=(x+1)/2]];x,{n,nterms}]",
				"(* Second program *)",
				"nterms=100;LinearRecurrence[{0,0,0,0,0,2,0,0,0,0,0,-1},{1,1,3,11,3,17,7,5,9,29,7,35},nterms]"
			],
			"program": [
				"(PARI) a(n)=if (n%3==2, 2*n\\3, if (n%2, n, 3*n-1)) \\\\ _Charles R Greathouse IV_, Nov 16 2021",
				"(Python)",
				"def a(n):",
				"    x = 2*n - 1",
				"    return x//3 if x%3 == 0 else ((3*x+1)//2 if x%4 == 3 else (x+1)//2)",
				"print([a(n) for n in range(1, 66)]) # _Michael S. Branicky_, Nov 16 2021"
			],
			"xref": [
				"Cf. A006370, A014682, A016813, A016921, A016945, A004767, A239129, A350461."
			],
			"keyword": "nonn,easy",
			"offset": "1,3",
			"author": "_Paolo Xausa_, Nov 16 2021",
			"references": 2,
			"revision": 51,
			"time": "2022-01-01T21:24:19-05:00",
			"created": "2021-12-22T14:46:35-05:00"
		}
	]
}