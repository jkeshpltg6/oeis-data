{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A054489",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 54489,
			"data": "1,10,59,344,2005,11686,68111,396980,2313769,13485634,78600035,458114576,2670087421,15562409950,90704372279,528663823724,3081278570065,17959007596666,104672767009931,610077594462920",
			"name": "Expansion of (1+4*x)/(1-6*x+x^2).",
			"comment": [
				"Numbers n such that 8*n^2 + 41 is a square.",
				"(x, y) = (a(n), a(n+1)) are solutions to x^2 + y^2 - 6*x*y = 41. - _John O. Oladokun_, Mar 17 2021"
			],
			"reference": [
				"A. H. Beiler, Recreations in the Theory of Numbers, Dover, N. Y., 1964, pp. 122-125, 194-196."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A054489/b054489.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"I. Adler, \u003ca href=\"http://www.fq.math.ca/Scanned/7-2/adler.pdf\"\u003eThree Diophantine equations - Part II\u003c/a\u003e, Fib. Quart., 7 (1969), pp. 181-193.",
				"E. I. Emerson, \u003ca href=\"http://www.fq.math.ca/Scanned/7-3/emerson.pdf\"\u003eRecurrent Sequences in the Equation DQ^2=R^2+N\u003c/a\u003e, Fib. Quart., 7 (1969), pp. 231-242.",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (6,-1)."
			],
			"formula": [
				"a(n) = 6*a(n-1) - a(n-2), a(0)=1, a(1)=10.",
				"a(n) = (10*((3+2*sqrt(2))^n - (3-2*sqrt(2))^n) - ((3+2*sqrt(2))^(n-1) - (3-2*sqrt(2))^(n-1)))/(4*sqrt(2)).",
				"From _G. C. Greubel_, Jan 19 2020: (Start)",
				"a(n) = ChebyshevU(n,3) + 4*ChebyshevU(n-1,3).",
				"a(n) = (Pell(2*n+2) + 4*Pell(2*n))/2 = (Pell-Lucas(2*n+1) + 3*Pell(2*n))/2.",
				"E.g.f.: exp(3*x)*( cosh(2*sqrt(2)*x) + 7*sinh(2*sqrt(2)*x)/(2*sqrt(2)) ). (End)"
			],
			"maple": [
				"a[0]:=1: a[1]:=10: for n from 2 to 26 do a[n]:=6*a[n-1]-a[n-2] od: seq(a[n], n=0..19); # _Zerinvary Lajos_, Jul 26 2006"
			],
			"mathematica": [
				"Table[(LucasL[2*n+1, 2] + 3*Fibonacci[2*n, 2])/2, {n,0,30}] (* _G. C. Greubel_, Jan 19 2020 *)"
			],
			"program": [
				"(PARI) vector(31, n, polchebyshev(n-1,2,3) +4*polchebyshev(n-2,2,3) ) \\\\ _G. C. Greubel_, Jan 19 2020",
				"(MAGMA) I:=[1,10]; [n le 2 select I[n] else 6*Self(n-1) - Self(n-2): n in [1..30]]; // _G. C. Greubel_, Jan 19 2020",
				"(Sage) [chebyshev_U(n,3) +4*chebyshev_U(n-1,3) for n in (0..30)] # _G. C. Greubel_, Jan 19 2020",
				"(GAP) a:=[1,10];; for n in [3..30] do a[n]:=6*a[n-1]-a[n-2]; od; a; # _G. C. Greubel_, Jan 19 2020"
			],
			"xref": [
				"Cf. A000129, A002203, A038761, A054488."
			],
			"keyword": "easy,nonn",
			"offset": "0,2",
			"author": "_Barry E. Williams_, May 04 2000",
			"ext": [
				"More terms from _James A. Sellers_, May 05 2000"
			],
			"references": 5,
			"revision": 32,
			"time": "2021-03-25T05:10:13-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}