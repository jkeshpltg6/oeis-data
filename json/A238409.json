{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A238409",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 238409,
			"data": "16,16,24,24,10,10,32,32,32,23,23,23,40,9,23,35,9,31,31,40,35,23,48,35,48,25,31,43,40,10,48,31,39,37,23,23,43,31,35,38,25,39,56,48,43,24,30,51,39,38,10,50,56,28,31,43,31,45,37,48,50,10,51,51,23,30,56,46,31,38,38,37,64,28,47,50,39,32,64,43,48,41,47,38,51,39,24,58,42,50,56,56,39,25,51,40,64,53",
			"name": "The (rounded) augmented Zagreb index of the rooted tree with Matula number n (n \u003e= 3).",
			"comment": [
				"The augmented Zagreb index of a graph is defined as the summation of [(d(u)d(v)/(d(u)+d(v) - 2)]^3 over all edges uv of G, where d(w) denotes the degree of the vertex w.",
				"The Matula number of a rooted tree can be defined in the following recursive manner: to the one-vertex tree there corresponds the number 1; to a tree T with root degree 1 there corresponds the t-th prime number, where t is the Matula  number of the tree obtained from T by deleting the edge emanating from the root; to a tree T with root degree m\u003e=2 there corresponds the product of the Matula numbers of the m branches of T."
			],
			"reference": [
				"E. Deutsch, Rooted tree statistics from Matula numbers, Discrete Applied Math., 160, 2012, 2314-2322.",
				"B. Furtula, A. Graovac and D. Vukicevic, Augmented Zagreb index, J. Math. Chem., 48, 2010, 370-380.",
				"F. Goebel, On a 1-1 correspondence between rooted trees and natural numbers, J. Combin. Theory, B 29 (1980), 141-143.",
				"I. Gutman and A. Ivic, On Matula numbers, Discrete Math., 150, 1996, 131-142.",
				"I. Gutman and Y-N. Yeh, Deducing properties of trees from their Matula numbers, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"Y. Huang, B. Liu, L. Gan, Augmented Zagreb index of connected graphs, Comm. Math. Comp. Chem. (MATCH), 67, 2012, 483-494.",
				"D. W. Matula, A natural rooted tree enumeration by prime factorization, SIAM Review, 10, 1968, 273.",
				"D. Wang, Y. Huang and B. Liu, Bounds on augmented Zagreb index, Comm. Math. Comp. Chem. (MATCH), 68, 2012, 209-216."
			],
			"link": [
				"E. Deutsch, \u003ca href=\"http://arxiv.org/abs/1111.4288\"\u003eRooted tree statistics from Matula numbers\u003c/a\u003e, arXiv1111.4288.",
				"\u003ca href=\"/index/Mat#matula\"\u003eIndex entries for sequences related to Matula-Goebel numbers\u003c/a\u003e"
			],
			"formula": [
				"There are recurrence relations that give the augmented Zagreb index of an \"elevated\" rooted tree (attach a new vertex to the root which becomes the root of the new tree) and of the merge of two rooted trees (identify the two roots). They make use of the sequence of the degrees of the level-1 vertices (denoted by DL in the Maple program). However, since the augmented Zagreb index of the 1-edge graph P[2] is infinity, we replace the rooted trees that are obtained by the merge of P[2] with a rooted tree T by the elevated T. In  other words, an even Matula number n is replaced by the (n/2)-th prime number.",
				"In the Maple program, F(n) gives the actual (not rounded) augmented Zagreb index of the rooted tree with Matula number n. For example, F(987654321) = 58297/256; the corresponding tree is the 29-vertex tree given in Fig. 2 of the Deutsch reference."
			],
			"example": [
				"a(5)=24; indeed the rooted tree with Matula number 5 is the path PQRS (rooted at P). The edges PQ and RS have endpoints of degrees 1 and 2 and the edge QR has endpoints  of degrees 2 and 2; consequently, the contributions of these 3 edges to the augmented Zagreb index are 8, 8 and 8."
			],
			"maple": [
				"f := proc (x, y) options operator, arrow: x^3*y^3/(x+y-2)^3 end proc: c := 16: with(numtheory): F := proc (n) local DL, r, s: DL := proc (n) if n = 2 then [1] elif bigomega(n) = 1 then [1+bigomega(pi(n))] else [op(DL(op(1, factorset(n)))), op(DL(n/op(1, factorset(n))))] end if end proc: r := proc (n) options operator, arrow: op(1, factorset(n)) end proc: s := proc (n) options operator, arrow: n/r(n) end proc: if n = 3 then 16 elif `mod`(n, 2) = 0 then F(ithprime((1/2)*n)) elif bigomega(n) = 1 then F(pi(n))-(sum(f(DL(pi(n))[j], bigomega(pi(n))), j = 1 .. bigomega(pi(n))))+sum(f(DL(pi(n))[j], 1+bigomega(pi(n))), j = 1 .. bigomega(pi(n)))+f(1, 1+bigomega(pi(n))) else F(r(n))+F(s(n))-(sum(f(DL(r(n))[j], bigomega(r(n))), j = 1 .. bigomega(r(n))))-(sum(f(DL(s(n))[j], bigomega(s(n))), j = 1 .. bigomega(s(n))))+sum(f(DL(r(n))[j], bigomega(n)), j = 1 .. bigomega(r(n)))+sum(f(DL(s(n))[j], bigomega(n)), j = 1 .. bigomega(s(n))) end if end proc: a := proc (n) options operator, arrow: round(F(n)) end proc: seq(a(n), n = 3 .. 100);"
			],
			"xref": [
				"Cf. A238408."
			],
			"keyword": "nonn",
			"offset": "3,1",
			"author": "_Emeric Deutsch_, Feb 26 2014",
			"references": 1,
			"revision": 12,
			"time": "2017-03-07T11:24:02-05:00",
			"created": "2014-02-26T14:24:34-05:00"
		}
	]
}