{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A020988",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 20988,
			"data": "0,2,10,42,170,682,2730,10922,43690,174762,699050,2796202,11184810,44739242,178956970,715827882,2863311530,11453246122,45812984490,183251937962,733007751850,2932031007402,11728124029610,46912496118442",
			"name": "a(n) = (2/3)*(4^n-1).",
			"comment": [
				"Numbers whose binary representation is 10, n times (see A163662(n) for n \u003e= 1). - _Alexandre Wajnberg_, May 31 2005",
				"Numbers whose base-4 representation consists entirely of 2's; twice base-4 repunits. - _Franklin T. Adams-Watters_, Mar 29 2006",
				"Expected time to finish a random Tower of Hanoi problem with 2n disks using optimal moves, so (since 2n is even and A010684(2n) = 1) a(n) = A060590(2n). - _Henry Bottomley_, Apr 05 2001",
				"a(n) is the number of derangements of [2n + 3] with runs consisting of consecutive integers. E.g., a(1) = 10 because the derangements of {1, 2, 3, 4, 5} with runs consisting of consecutive integers are 5|1234, 45|123, 345|12, 2345|1, 5|4|123, 5|34|12, 45|23|1, 345|2|1, 5|4|23|1, 5|34|2|1 (the bars delimit the runs). - _Emeric Deutsch_, May 26 2003",
				"For n \u003e 0, also smallest numbers having in binary representation exactly n + 1 maximal groups of consecutive zeros: A087120(n) = a(n-1), see A087116. - _Reinhard Zumkeller_, Aug 14 2003",
				"Number of walks of length 2n + 3 between any two diametrically opposite vertices of the cycle graph C_6. Example: a(0) = 2 because in the cycle ABCDEF we have two walks of length 3 between A and D: ABCD and AFED. - _Emeric Deutsch_, Apr 01 2004",
				"From _Paul Barry_, May 18 2003: (Start)",
				"Row sums of triangle using cumulative sums of odd-indexed rows of Pascal's triangle (start with zeros for completeness):",
				"            0  0",
				"            1  1",
				"         1  4  4  1",
				"      1  6 14 14  6  1",
				"   1  8 27 49 49 27  8  1 (End)",
				"a(n) gives the position of the n-th zero in A173732, i.e., A173732(a(n)) = 0 for all n and this gives all the zeros in A173732. - _Howard A. Landman_, Mar 14 2010",
				"Smallest number having alternating bit sum -n. Cf. A065359. For n = 0, 1, ..., the last digit of a(n) is 0, 2, 0, 2, ... . - _Washington Bomfim_, Jan 22 2011",
				"Number of toothpicks minus 1 in the toothpick structure of A139250 after 2^n stages. - _Omar E. Pol_, Mar 15 2012",
				"For n \u003e 0 also partial sums of the odd powers of 2 (A004171). - _K. G. Stier_, Nov 04 2013",
				"Values of m such that binomial(4*m + 2, m) is odd. Cf. A002450. - _Peter Bala_, Oct 06 2015",
				"For a(n) \u003e 2, values of m such that m is two steps away from a power of 2 under the Collatz iteration. - _Roderick MacPhee_, Nov 10 2016",
				"a(n) is the position of the first occurrence of 2^(n+1)-1 in A020986. See the Brillhart and Morton link, pp. 856-857. - _John Keith_, Jan 12 2021"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A020988/b020988.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e (terms 0..170 from Vincenzo Librandi)",
				"Andrei Asinowski, Cyril Banderier, and Benjamin Hackl, \u003ca href=\"https://benjamin-hackl.at/downloads/2019_ABH_popstack-extremal.pdf\"\u003eOn extremal cases of pop-stack sorting\u003c/a\u003e, Permutation Patterns (Zürich, Switzerland, 2019).",
				"Andrei Asinowski, Cyril Banderier, and Benjamin Hackl, \u003ca href=\"https://arxiv.org/abs/2003.04912\"\u003eFlip-sort and combinatorial aspects of pop-stack sorting\u003c/a\u003e, arXiv:2003.04912 [math.CO], 2020.",
				"Peter Bala, \u003ca href=\"/A002450/a002450.txt\"\u003eA characterization of A002450, A020988 and A080674.\u003c/a\u003e",
				"John Brillhart and Peter Morton, \u003ca href=\"http://www.maa.org/programs/maa-awards/writing-awards/a-case-study-in-mathematical-research-the-golay-rudin-shapiro-sequence\"\u003eA case study in mathematical research: the Golay-Rudin-Shapiro sequence\u003c/a\u003e, Amer. Math. Monthly, 103 (1996) 854-869.",
				"Nobushige Kurokawa, \u003ca href=\"http://cage.ugent.be/~kthas/Fun/library/Kurokawa.pdf\"\u003eZeta functions over F_1\u003c/a\u003e, Proc. Japan Acad., 81, Ser. A (2005), 180-184. See Theorem 3 (3).",
				"Andrei K. Svinin, \u003ca href=\"http://arxiv.org/abs/1603.05748\"\u003eTuenter polynomials and a Catalan triangle\u003c/a\u003e, arXiv:1603.05748 [math.CO], 2016. See p.3.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (5,-4)."
			],
			"formula": [
				"a(n) = 4*a(n-1) + 2, a(0) = 0.",
				"a(n) = A026644(2*n).",
				"a(n) = A007583(n) - 1 = A039301(n+1) - 2 = A083584(n-1) + 1.",
				"E.g.f. : (2/3)*(exp(4*x)-exp(x)). - _Paul Barry_, May 18 2003",
				"a(n) = A007583(n+1) - 1 = A039301(n+2) - 2 = A083584(n) + 1. - _Ralf Stephan_, Jun 14 2003",
				"G.f.: 2*x/((1-x)*(1-4*x)). - _R. J. Mathar_, Sep 17 2008",
				"a(n) = a(n-1) + 2^(2n-1), a(0) = 0. - _Washington Bomfim_, Jan 22 2011",
				"a(n) = A193652(2*n). - _Reinhard Zumkeller_, Aug 08 2011",
				"a(n) = 5*a(n-1) - 4*a(n-2) (n \u003e 1), a(0) = 0, a(1) = 2. - _L. Edson Jeffery_, Mar 02 2012",
				"a(n) = (2/3)*A024036(n). - _Omar E. Pol_, Mar 15 2012",
				"a(n) = 2*A002450(n). - _Yosu Yurramendi_, Jan 24 2017",
				"From _Seiichi Manyama_, Nov 24 2017: (Start)",
				"Zeta_{GL(2)/F_1}(s) = Product_{k = 1..4} (s-k)^(-b(2,k)), where Sum b(2,k)*t^k = t*(t-1)*(t^2-1). That is Zeta_{GL(2)/F_1}(s) = (s-3)*(s-2)/((s-4)*(s-1)).",
				"Zeta_{GL(2)/F_1}(s) = Product_{n \u003e 0} (1 - (1/s)^n)^(-A295521(n)) = Product_{n \u003e 0} (1 - x^n)^(-A295521(n)) = (1-3*x)*(1-2*x)/((1-4*x)*(1-x)) = 1 + Sum_{k \u003e 0} a(k-1)*x^k (x=1/s). (End)",
				"From _Oboifeng Dira_, May 29 2020: (Start)",
				"a(n) = A078008(2n+1) (second bisection).",
				"a(n) = Sum_{k=0..n} binomial(2n+1, ((n+2) mod 3)+3k). (End)"
			],
			"maple": [
				"A020988 := proc(n)",
				"    2*(4^n-1)/3 ;",
				"end proc: # _R. J. Mathar_, Feb 19 2015"
			],
			"mathematica": [
				"(2(4^Range[0, 30] - 1))/3 (* or *) LinearRecurrence[{5, -4}, {0, 2}, 30] (* _Harvey P. Dale_, Sep 25 2013 *)"
			],
			"program": [
				"(MAGMA) [(2/3)*(4^n-1): n in [0..40] ]; // _Vincenzo Librandi_, Apr 28 2011",
				"(PARI) vector(100, n, n--; (2/3)*(4^n-1)) \\\\ _Altug Alkan_, Oct 06 2015",
				"(PARI) Vec(2*z/((1-z)*(1-4*z)) + O(z^30)) \\\\ _Altug Alkan_, Oct 11 2015",
				"(Scala) (((List.fill(20)(4: BigInt)).scanLeft(1: BigInt)(_ * _)).map(2 * _)).scanLeft(0: BigInt)(_ + _) // _Alonso del Arte_, Sep 12 2019"
			],
			"xref": [
				"Cf. A020989, A108019, A295521."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Edited by _N. J. A. Sloane_, Sep 06 2006"
			],
			"references": 67,
			"revision": 143,
			"time": "2021-01-17T11:15:49-05:00",
			"created": "1998-06-14T03:00:00-04:00"
		}
	]
}