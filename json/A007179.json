{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007179",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7179,
			"id": "M3284",
			"data": "0,1,1,4,6,16,28,64,120,256,496,1024,2016,4096,8128,16384,32640,65536,130816,262144,523776,1048576,2096128,4194304,8386560,16777216,33550336,67108864,134209536,268435456",
			"name": "Dual pairs of integrals arising from reflection coefficients.",
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A007179/b007179.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"Paul Barry, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL8/Barry/barry84.html\"\u003eA Catalan Transform and Related Transformations on Integer Sequences\u003c/a\u003e, Journal of Integer Sequences, Vol. 8 (2005), Article 05.4.5.",
				"J. Heading, \u003ca href=\"http://dx.doi.org/10.1088/0305-4470/14/2/011\"\u003eTheorem relating to the development of a reflection coefficient in terms of a small parameter\u003c/a\u003e, J. Phys. A 14 (1981), 357-367.",
				"Kyu-Hwan Lee, Se-jin Oh, \u003ca href=\"http://arxiv.org/abs/1601.06685\"\u003eCatalan triangle numbers and binomial coefficients\u003c/a\u003e, arXiv:1601.06685 [math.CO], 2016.",
				"A. Yajima, \u003ca href=\"https://www.jstage.jst.go.jp/article/bcsj/87/11/87_20140204/_pdf\"\u003eHow to calculate the number of stereoisomers of inositol-homologs\u003c/a\u003e, Bull. Chem. Soc. Jpn. 2014, 87, 1260-1264 | doi:10.1246/bcsj.20140204. See Tables 1 and 2 (and text). - _N. J. A. Sloane_, Mar 26 2015",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,2,-4)."
			],
			"formula": [
				"From _Paul Barry_, Apr 28 2004: (Start)",
				"  Binomial transform is (A000244(n)+A001333(n))/2.",
				"  G.f.: x*(1-x)/((1-2*x)*(1-2*x^2)).",
				"  a(n) = 2*a(n-1)+2*a(n-2)-4*a(n-3).",
				"  a(n) = 2^n/2-2^(n/2)*(1+(-1)^n)/4. (End)",
				"G.f.: (1+x*Q(0))*x/(1-x), where Q(k)= 1 - 1/(2^k - 2*x*2^(2*k)/(2*x*2^k - 1/(1 + 1/(2*2^k - 8*x*2^(2*k)/(4*x*2^k + 1/Q(k+1)))))); (continued fraction). - _Sergei N. Gladkovskii_, May 22 2013"
			],
			"maple": [
				"f := n-\u003e if n mod 2 = 0 then 2^(n-1)-2^((n-2)/2) else 2^(n-1); fi;"
			],
			"mathematica": [
				"LinearRecurrence[{2,2,-4},{0,1,1},30] (* _Harvey P. Dale_, Nov 30 2015 *)"
			],
			"program": [
				"(MAGMA) [Floor(2^n/2-2^(n/2)*(1+(-1)^n)/4): n in [0..40]]; // _Vincenzo Librandi_, Aug 20 2011",
				"(PARI) Vec(x*(1-x)/((1-2*x)*(1-2*x^2)) + O(x^50)) \\\\ _Michel Marcus_, Jan 28 2016"
			],
			"xref": [
				"Column k=2 of A309748."
			],
			"keyword": "nonn,easy",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_, _Simon Plouffe_",
			"references": 10,
			"revision": 41,
			"time": "2019-09-19T02:18:06-04:00",
			"created": "1994-04-28T03:00:00-04:00"
		}
	]
}