{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A202017",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 202017,
			"data": "1,2,3,9,4,52,64,5,195,855,625,6,606,6546,15306,7776,7,1701,38486,201866,305571,117649,8,4488,194160,1950320,6244680,6806472,2097152,9,11367,887949,15597315,90665595,200503701,168205743,43046721",
			"name": "Triangle of coefficients of the numerator polynomials of the rational o.g.f.'s of the diagonals of A059297.",
			"comment": [
				"If a triangular array has an e.g.f. of the form exp(t*F(x)) with F(0) = 0, then the o.g.f.'s for the diagonals of the triangle are rational functions in t [Drake, 1.10]. The rational functions are the coefficients in the compositional inverse (with respect to x) (x-t*F(x))^(-1).",
				"Triangle A059297 has e.g.f. exp(t*x*exp(x)). The present triangle lists the coefficients of the numerator polynomials of the rational o.g.f.'s of the diagonals of A059297. Drake, Example 1.10.9, gives three combinatorial interpretations for these coefficients (but note the expansion at the bottom of p.68 is for (x-t*(-W(-x))^(-1), W(x) the Lambert W function, and not for (x-t*x*exp(x))^(-1) as stated there). Row reversal of A155163."
			],
			"link": [
				"Peter Bala, \u003ca href=\"/A112007/a112007_Bala.txt\"\u003eDiagonals of triangles with generating function exp(t*F(x)).\u003c/a\u003e",
				"B. Drake, \u003ca href=\"http://people.brandeis.edu/~gessel/homepage/students/drakethesis.pdf\"\u003eAn inversion theorem for labeled trees and some limits of areas under lattice paths\u003c/a\u003e, A dissertation presented to the Faculty of the Graduate School of Arts and Sciences of Brandeis University."
			],
			"formula": [
				"T(n,k) = sum {j = 0..k} (-1)^(k-j)*C(2*n+1,k-j)*C(n+j,j)*j^n.",
				"The compositional inverse (with respect to x) (x-t*x*exp(x))^-1 = x/(1-t) + 2*t/(1-t)^3*x^2/2! + (3*t+9*t^2)/(1-t)^5*x^3/3! + (4*t+52*t^2+64*t^3)/(1-t)^7*x^4/4! + .... The numerator polynomials begin 1, 2*t, (3*t+9*t^2), .... The initial 1 has been omitted from the array. Row sums appear to be A001813."
			],
			"example": [
				"Triangle begins",
				"..n\\k.|...1.....2......3.......4.......5.......6",
				"= = = = = = = = = = = = = = = = = = = = = = = =",
				"..1..|...2",
				"..2..|...3.....9",
				"..3..|...4....52.....64",
				"..4..|...5...195....855.....625",
				"..5..|...6...606...6546...15306....7776",
				"..6..|...7..1701..38486..201866..305571..117649",
				"..."
			],
			"xref": [
				"Cf. A059297, A155163 (row reverse)."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "1,2",
			"author": "_Peter Bala_, Dec 08 2011",
			"references": 3,
			"revision": 12,
			"time": "2019-10-21T16:25:00-04:00",
			"created": "2011-12-08T19:09:40-05:00"
		}
	]
}