{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A057641",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 57641,
			"data": "0,0,1,0,4,0,7,2,7,5,13,0,17,9,12,8,23,5,27,8,21,20,34,1,33,25,30,17,46,7,50,22,40,37,46,6,62,43,50,19,70,19,74,37,46,55,82,9,79,46,70,47,95,32,83,38,81,74,107,2,112,81,76,56,102,45,125,70,103,58,133,14,138,101",
			"name": "a(n) = floor(H(n) + exp(H(n))*log(H(n))) - sigma(n), where H(n) = Sum_{k=1..n} 1/k and sigma(n) (A000203) is the sum of the divisors of n.",
			"comment": [
				"Theorem (Lagarias): a(n) is nonnegative for all n if and only if the Riemann Hypothesis is true.",
				"Up to rank n=10^4, zeros occur only at n=1,2,4,6 and 12; ones occur at n=3 and n=24. The first occurrence of k = 0,1,2,3,... is at n = 1,3,8,-1,5,10,36,7,16,14,-1,-1,15,11,72,... where -1 means that k does not occur among the first 10^4 terms. - _Robert G. Wilson v_, Dec 06 2010, reformulated by _M. F. Hasler_, Sep 09 2011",
				"Looking at the graph of this sequence, it appears that there is a slowly growing lower bound. It is even more apparent when larger ranges of points are computed. Numbers A176679(n+2) and A222761(n) give the (x,y) coordinates of the n-th point. - _T. D. Noe_, Mar 28 2013"
			],
			"reference": [
				"G. Robin, Grandes valeurs de la fonction somme des diviseurs et hypothèse de Riemann, J. Math. Pures Appl. 63 (1984), 187-213."
			],
			"link": [
				"Peter Luschny, \u003ca href=\"/A057641/b057641.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e (first 10000 terms from T. D. Noe)",
				"J. C. Lagarias, \u003ca href=\"https://arxiv.org/abs/math/0008177\"\u003eAn elementary problem equivalent to the Riemann hypothesis\u003c/a\u003e, arXiv:math/0008177 [math.NT], 2000-2001; Am. Math. Monthly 109 (#6, 2002), 534-543.",
				"S. Nazardonyavi and S. Yakubovich, \u003ca href=\"http://arxiv.org/abs/1306.3434\"\u003eDelicacy of the Riemann hypothesis and certain subsequences of superabundant numbers\u003c/a\u003e, arXiv preprint arXiv:1306.3434 [math.NT], 2013."
			],
			"formula": [
				"a(n) = A057640(n) - A000203(n). - _Omar E. Pol_, Oct 25 2019"
			],
			"mathematica": [
				"f[n_] := Block[{h = HarmonicNumber@n}, Floor[h + Exp@h*Log@h] - DivisorSigma[1, n]]; Array[f, 74] (* _Robert G. Wilson v_, Dec 06 2010 *)"
			],
			"program": [
				"(PARI) a(n)={my(H=sum(k=1,n,1/k)); floor(exp(H)*log(H)+H) - sigma(n)}",
				"list_A057641(Nmax,H=0,S=1)=for(n=S,Nmax, H+=1/n; print1(floor(exp(H)*log(H)+H) - sigma(n),\",\"))  \\\\ _M. F. Hasler_, Sep 09 2011"
			],
			"xref": [
				"Cf. A057640, A000203, A076633, A067698, A079526, A058209."
			],
			"keyword": "nonn,nice,easy",
			"offset": "1,5",
			"author": "_N. J. A. Sloane_, Oct 12 2000",
			"ext": [
				"Five more terms from _Robert G. Wilson v_, Dec 06 2010",
				"I deleted some unproved assertions by _Robert G. Wilson v_ about the presence of 0's, 1's, ... in this sequence. - _N. J. A. Sloane_, Dec 07 2010"
			],
			"references": 20,
			"revision": 54,
			"time": "2020-10-18T14:43:53-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}