{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A227551",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 227551,
			"data": "1,0,1,0,1,0,1,1,0,1,1,0,1,2,0,1,3,0,1,3,1,0,1,3,2,0,1,5,2,0,1,5,4,0,1,5,6,0,1,6,7,1,0,1,6,10,1,0,1,7,11,3,0,1,9,13,4,0,1,7,18,6,0,1,8,20,9,0,1,10,21,14,0,1,9,27,16,1,0,1,10,29,22,2",
			"name": "Number T(n,k) of partitions of n into distinct parts with boundary size k; triangle T(n,k), n\u003e=0, 0\u003c=k\u003c=A227568(n), read by rows.",
			"comment": [
				"The boundary size is the number of parts having fewer than two neighbors."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A227551/b227551.txt\"\u003eRows n = 0..600, flattened\u003c/a\u003e"
			],
			"example": [
				"T(12,1) = 1: [12].",
				"T(12,2) = 6: [1,11], [2,10], [3,4,5], [3,9], [4,8], [5,7].",
				"T(12,3) = 7: [1,2,3,6], [1,2,9], [1,3,8], [1,4,7], [1,5,6], [2,3,7], [2,4,6].",
				"T(12,4) = 1: [1,2,4,5].",
				"Triangle T(n,k) begins:",
				"  1;",
				"  0, 1;",
				"  0, 1;",
				"  0, 1, 1;",
				"  0, 1, 1;",
				"  0, 1, 2;",
				"  0, 1, 3;",
				"  0, 1, 3, 1;",
				"  0, 1, 3, 2;",
				"  0, 1, 5, 2;",
				"  0, 1, 5, 4;",
				"  0, 1, 5, 6;",
				"  0, 1, 6, 7, 1;"
			],
			"maple": [
				"b:= proc(n, i, t) option remember; `if`(n=0, `if`(t\u003e1, x, 1),",
				"      expand(`if`(i\u003c1, 0, `if`(t\u003e1, x, 1)*b(n, i-1, iquo(t, 2))+",
				"      `if`(i\u003en, 0, `if`(t=2, x, 1)*b(n-i, i-1, iquo(t, 2)+2)))))",
				"    end:",
				"T:= n-\u003e (p-\u003eseq(coeff(p, x, i), i=0..degree(p)))(b(n$2, 0)):",
				"seq(T(n), n=0..30);"
			],
			"mathematica": [
				"b[n_, i_, t_] := b[n, i, t] = If[n == 0, If[t \u003e 1, x, 1], Expand[If[i \u003c 1, 0, If[t \u003e 1, x, 1]*b[n, i - 1, Quotient[t, 2]] + If[i \u003e n, 0, If[t == 2, x, 1]*b[n - i, i - 1, Quotient[t, 2] + 2]]]]]; T[n_] := Function [p, Table[Coefficient[p, x, i], {i, 0, Exponent[p, x]}]][b[n, n, 0]]; Table[T[n], {n, 0, 30}] // Flatten (* _Jean-François Alcover_, Dec 12 2016, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Columns k=0-10 give: A000007, A057427, A227559, A227560, A227561, A227562, A227563, A227564, A227565, A227566, A227567.",
				"Row sums give: A000009.",
				"Last elements of rows give: A227552.",
				"Cf. A227345 (a version with trailing zeros), A053993, A201077, A227568, A224878 (one part of size 0 allowed)."
			],
			"keyword": "nonn,look,tabf",
			"offset": "0,14",
			"author": "_Alois P. Heinz_, Jul 16 2013",
			"references": 14,
			"revision": 33,
			"time": "2019-01-11T08:44:06-05:00",
			"created": "2013-07-16T10:09:31-04:00"
		}
	]
}