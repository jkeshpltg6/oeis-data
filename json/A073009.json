{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A073009",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 73009,
			"data": "1,2,9,1,2,8,5,9,9,7,0,6,2,6,6,3,5,4,0,4,0,7,2,8,2,5,9,0,5,9,5,6,0,0,5,4,1,4,9,8,6,1,9,3,6,8,2,7,4,5,2,2,3,1,7,3,1,0,0,0,2,4,4,5,1,3,6,9,4,4,5,3,8,7,6,5,2,3,4,4,5,5,5,5,8,8,1,7,0,4,1,1,2,9,4,2,9,7,0,8,9,8,4,9,9",
			"name": "Decimal expansion of Sum_{n \u003e= 1} 1/n^n.",
			"comment": [
				"This is also equal to Integral_{x = 0..1} dx/x^x.",
				"10*Sum_{n = 1..infinity} 1/p(n)^p(n+1), where p(n) and p(n+1) are consecutive primes, is equal to Sum_{n = 1..infinity} 1/n^n up to the fifth decimal digit. - _Paolo P. Lava_, May 21 2013"
			],
			"link": [
				"Kenny Lau, \u003ca href=\"/A073009/b073009.txt\"\u003eTable of n, a(n) for n = 1..10001\u003c/a\u003e",
				"Johan Bernoulli, Demonstratio Methodi Analyticae, qua usus est pro determinanda aliqua Quadratura exponentiali per seriem, Actis Eruditorum A (1697), p. 131. Collected in \u003ca href=\"https://archive.org/details/johannisbernoul00berngoog\"\u003eOpera Omnia, vol. 3\u003c/a\u003e, 1742. See \u003ca href=\"https://archive.org/stream/johannisbernoul00berngoog#page/n406/mode/2up\"\u003ep. 376ff\u003c/a\u003e.",
				"M. L. Glasser, \u003ca href=\"https://doi.org/10.1080/00029890.2019.1565856\"\u003eA note on Beukers's and related integrals\u003c/a\u003e, Amer. Math. Monthly 126(4) (2019), 361-363.",
				"Jaroslav Hančl and Simon Kristensen, \u003ca href=\"https://arxiv.org/abs/1802.03946\"\u003eMetrical irrationality results related to values of the Riemann zeta-function\u003c/a\u003e, arXiv:1802.03946 [math.NT], 2018.",
				"Randall Munroe, \u003ca href=\"http://xkcd.com/1047/\"\u003eApproximations\u003c/a\u003e.",
				"Simon Plouffe, \u003ca href=\"https://web.archive.org/web/20150913023027/http://www.worldwideschool.org/library/books/sci/math/MiscellaneousMathematicalConstants/chap87.html\"\u003eSum(1/n^n, n=1..infinity)\u003c/a\u003e. [internet archive]",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PowerTower.html\"\u003ePower Tower\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/SophomoresDream.html\"\u003eSophomore's Dream\u003c/a\u003e."
			],
			"formula": [
				"Constant also equals the double integral Integral_{y = 0..1} Integral_{x = 0..1} 1/(x*y)^(x*y) dx dy. - _Peter Bala_, Mar 04 2012",
				"Approximately log(3)^e, see Munroe link. - _Charles R Greathouse IV_, Apr 25 2012",
				"Another approximation is A + A^(-19), where A is Glaisher-Kinkelin constant (A074962). - _Noam Shalev_, Jan 16 2015",
				"From _Petros Hadjicostas_, Jun 29 2020: (Start)",
				"Equals -Integral_{x=0..1, y=0..1} dx dy/((x*y)^(x*y)*log(x*y)). (Apply Theorem 1 or Theorem 2 of Glasser (2019) to the integral Integral_{x = 0..1} dx/x^x.)",
				"Equals -Integral_{x=0..1} log(x)/x^x dx. (Apply Theorem 1 or Theorem 2 of Glasser (2019) to the double integral of _Peter Bala_ above.) (End)"
			],
			"example": [
				"1.291285997062663540407282590595600541498619368..."
			],
			"maple": [
				"evalf(Sum(1/n^n, n=1..infinity), 120); # _Vaclav Kotesovec_, Jun 24 2016"
			],
			"mathematica": [
				"RealDigits[N[Sum[1/n^n, {n, 1, Infinity}], 110]] [[1]]"
			],
			"program": [
				"(PARI) suminf(n=1,n^-n) \\\\ _Charles R Greathouse IV_, Apr 25 2012"
			],
			"xref": [
				"Cf. A077178 (continued fraction expansion).",
				"Cf. A083648, A229191, A245637."
			],
			"keyword": "cons,nonn",
			"offset": "1,2",
			"author": "_Robert G. Wilson v_, Aug 03 2002",
			"references": 42,
			"revision": 64,
			"time": "2020-07-30T01:28:21-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}