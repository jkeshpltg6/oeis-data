{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001196",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1196,
			"data": "0,3,12,15,48,51,60,63,192,195,204,207,240,243,252,255,768,771,780,783,816,819,828,831,960,963,972,975,1008,1011,1020,1023,3072,3075,3084,3087,3120,3123,3132,3135,3264,3267,3276,3279,3312",
			"name": "Double-bitters: only even length runs in binary expansion.",
			"comment": [
				"Numbers whose set of base 4 digits is {0,3}. - _Ray Chandler_, Aug 03 2004",
				"n such that there exists a permutation p_1, ..., p_n of 1, ..., n such that i + p_i is a power of 4 for every i. - _Ray Chandler_, Aug 03 2004",
				"The first 2^n terms of the sequence could be obtained using the Cantor-like process for the segment [0, 4^n-1]. E.g., for n=1 we have [0, {1, 2}, 3] such that numbers outside of braces are the first 2 terms of the sequence; for n=2 we have [0, {1, 2}, 3, {4, 5, 6, 7, 8, 9, 10, 11}, 12, {13, 14}, 15] such that the numbers outside of braces are the first 4 terms of the sequence, etc. - _Vladimir Shevelev_, Dec 17 2012",
				"From _Emeric Deutsch_, Jan 26 2018: (Start)",
				"Also, the indices of the compositions having only even parts. For the definition of the index of a composition, see A298644. For example, 195 is in the sequence since its binary form is 11000011 and the composition [2,4,2] has only even parts. 132 is not in the sequence since its binary form is 10000100 and the composition [1,4,1,2] also has odd parts.",
				"The command c(n) from the Maple program yields the composition having index n. (End)",
				"After the k-th step of generating the Koch snowflake curve, label the edges of the curve consecutively 0..3*4^k-1 starting from a vertex of the originating triangle. a(0), a(1), ... a(2^k-1) are the labels of the edges contained in one edge of the originating triangle. Add 4^k to each label to get the labels for the next edge of the triangle. Compare with A191108 in respect of the Sierpinski arrowhead curve. - _Peter Munn_, Aug 18 2019"
			],
			"link": [
				"Sean A. Irvine, \u003ca href=\"/A001196/b001196.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Ralf Stephan, \u003ca href=\"/somedcgf.html\"\u003eSome divide-and-conquer sequences with (relatively) simple ordinary generating functions\u003c/a\u003e, 2004.",
				"Ralf Stephan, \u003ca href=\"/A079944/a079944.ps\"\u003eTable of generating functions\u003c/a\u003e. [ps file]",
				"Ralf Stefan, \u003ca href=\"/A001196/a001196.pdf\"\u003eTable of generating functions\u003c/a\u003e. [pdf file]",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/KochSnowflake.html\"\u003eKoch Snowflake\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Koch_snowflake\"\u003eKoch snowflake\u003c/a\u003e.",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"formula": [
				"a(2n) = 4*a(n), a(2n+1) = 4*a(n) + 3.",
				"a(n) = 3 * A000695(n)."
			],
			"maple": [
				"Runs := proc (L) local j, r, i, k: j := 1: r[j] := L[1]: for i from 2 to nops(L) do if L[i] = L[i-1] then r[j] := r[j], L[i] else j := j+1: r[j] := L[i] end if end do: [seq([r[k]], k = 1 .. j)] end proc: RunLengths := proc (L) map(nops, Runs(L)) end proc: c := proc (n) ListTools:-Reverse(convert(n, base, 2)): RunLengths(%) end proc: A := {}: for n to 3350 do if type(product(1+c(n)[j], j = 1 .. nops(c(n))), odd) = true then A := `union`(A, {n}) else  end if end do: A; # most of the Maple  program is due to _W. Edwin Clark_. - _Emeric Deutsch_, Jan 26 2018"
			],
			"mathematica": [
				"fQ[n_] := Union@ Mod[Length@# \u0026 /@ Split@ IntegerDigits[n, 2], 2] == {0}; Select[ Range@ 10000, fQ] (* Or *)",
				"fQ[n_] := Union@ Join[IntegerDigits[n, 4], {0, 3}] == {0, 3}; Select[ Range@ 10000, fQ] (* _Robert G. Wilson v_, Dec 24 2012 *)"
			],
			"program": [
				"(Haskell)",
				"a001196 n = if n == 0 then 0 else 4 * a001196 n' + 3 * b",
				"            where (n',b) = divMod n 2",
				"-- _Reinhard Zumkeller_, Feb 21 2014",
				"(Python)",
				"def inA001196(n):",
				"    while n != 0:",
				"        if n%4 == 1 or n%4 == 2:",
				"            return 0",
				"        n = n//4",
				"    return 1",
				"n, a = 0, 0",
				"while n \u003c 20:",
				"    if inA001196(a):",
				"        print(n,a)",
				"        n = n+1",
				"    a = a+1 # _A.H.M. Smeets_, Aug 19 2019",
				"(Python)",
				"from itertools import groupby",
				"def ok2lb(n):",
				"  if n == 0: return True  # by convention",
				"  return all(len(list(g))%2 == 0 for k, g in groupby(bin(n)[2:]))",
				"print([i for i in range(3313) if ok2lb(i)]) # _Michael S. Branicky_, Jan 04 2021",
				"(PARI) a(n) = 3*fromdigits(binary(n),4); \\\\ _Kevin Ryde_, Nov 07 2020"
			],
			"xref": [
				"3 times the Moser-de Bruijn sequence A000695.",
				"Two digits in other bases: A005823, A097252-A097262, A191108.",
				"Digit duplication in other bases: A338086, A338754."
			],
			"keyword": "nonn,base,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, based on an email from Bart la Bastide (bart(AT)xs4all.nl)",
			"references": 36,
			"revision": 78,
			"time": "2021-01-04T04:06:13-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}