{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A268301",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 268301,
			"data": "1,-7,-70,-795,-13802,-277782,-6093708,-139376659,-3297234754,-79988099074,-1979248977748,-49758116194846,-1267321717299236,-32631825106297228,-848030793254951704,-22214311484843607811,-585938143786366837938,-15548874443787002057610,-414829266882771282611204,-11120089118043870668697578,-299364678394845043715844268,-8090271856987498430846360564",
			"name": "G.f. satisfies: -1 = Product_{n\u003e=1} (1-x^n) * (1 - x^n*A(x)) * (1 - x^(n-1)/A(x)), where g.f. A(x) = Sum_{n\u003e=0} a(n)/2*(x/4)^n.",
			"comment": [
				"The g.f. utilizes the Jacobi Triple Product: Product_{n\u003e=1} (1-x^n)*(1 - x^n*a)*(1 - x^(n-1)/a) = Sum_{n=-oo..+oo} (-1)^n * x^(n*(n+1)/2) * a^n."
			],
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A268301/b268301.txt\"\u003eTable of n, a(n) for n = 0..300\u003c/a\u003e"
			],
			"formula": [
				"Given g.f. A(x) = Sum_{n\u003e=0} a(n)/2 * (x/4)^n, then gf. also satisfies:",
				"(1) -1 = Sum_{n=-oo..+oo} (-1)^n * x^(n*(n+1)/2) * A(x)^n,",
				"(2) A(x) = Product_{n\u003e=1} (1-x^n) * (1 - x^n/A(x)) * (1 - x^(n-1)*A(x)),",
				"(3) A(x) = Sum_{n=-oo..+oo} (-1)^n * x^(n*(n-1)/2) * A(x)^n.",
				"(4) x = Sum_{n\u003e=1} A268299(n) * x^n * A(x)^n.",
				"a(n) is odd iff n = 2^k-1 for k\u003e=0 (conjecture).",
				"a(n) ~  -c * d^n / n^(3/2), where d = 29.101591090693617170487962339050658... and c = 0.1385938593465955724446602611055779... . - _Vaclav Kotesovec_, Mar 02 2016"
			],
			"example": [
				"G.f.: A(x) = 1/2 - 7/2*x/4 - 70/2*x^2/4^2 - 795/2*x^3/4^3 - 13802/2*x^4/4^4 - 277782/2*x^5/4^5 - 6093708/2*x^6/4^6 - 139376659/2*x^7/4^7 - 3297234754/2*x^8/4^8 - 79988099074/2*x^9/4^9 - 1979248977748/2*x^10/4^10 -...",
				"where g.f. A(x) satisfies the Jacobi Triple Product:",
				"-1 = (1-x)*(1-x*A(x))*(1-1/A(x)) * (1-x^2)*(1-x^2*A(x))*(1-x/A(x)) * (1-x^3)*(1-x^3*A(x))*(1-x^2/A(x)) * (1-x^4)*(1-x^4*A(x))*(1-x^3/A(x)) * (1-x^5)*(1-x^5*A(x))*(1-x^4/A(x)) * (1-x^6)*(1-x^6*A(x))*(1-x^5/A(x)) *...",
				"also",
				"A(x) = (1-x)*(1-x/A(x))*(1-A(x)) * (1-x^2)*(1-x^2/A(x))*(1-x*A(x)) * (1-x^3)*(1-x^3/A(x))*(1-x^2*A(x)) * (1-x^4)*(1-x^4/A(x))*(1-x^3*A(x)) * (1-x^5)*(1-x^5/A(x))*(1-x^4*A(x)) * (1-x^6)*(1-x^6/A(x))*(1-x^5*A(x)) *...",
				"RELATED SERIES.",
				"1/A(x) = 2 + 7*2*x/4 + 119*2*x^2/4^2 + 2118*2*x^3/4^3 + 42523*2*x^4/4^4 + 914922*2*x^5/4^5 + 20745494*2*x^6/4^6 + 487390092*2*x^7/4^7 + 11764545555*2*x^8/4^8 + 289962708802*2*x^9/4^9 +...+ A268300(n)*2*x^n/4^n +...",
				"Series_Reversion( x*A(x) ) = 2*x + 7*x^2 + 84*x^3 + 1240*x^4 + 20942*x^5 + 382344*x^6 + 7354688*x^7 + 146810440*x^8 + 3012778758*x^9 + 63167322872*x^10 +...+ A268299(n)*x^n +..., an integer series.",
				"Let J(x) = Sum_{n\u003e=1} x^(n*(n-1)/2) * (A(x)^(n-1) + 1/A(x)^n),",
				"then J(x) is an integer series:",
				"J(x) = 3 + 8*x + 28*x^2 + 144*x^3 + 736*x^4 + 4024*x^5 + 22912*x^6 + 134784*x^7 + 813476*x^8 + 5010904*x^9 + 31379808*x^10 +..+ A268302(n)*x^n +...",
				"and J(x) = Product_{n\u003e=1} (1-x^n) * (1 + x^n*A(x)) * (1 + x^(n-1)/A(x)).",
				"Conjecture: Product_{n\u003e=1} (1-x^n) * (1 + k*x^n*A(x)) * (1 + k*x^(n-1)/A(x)) yields an integer series for all integer k."
			],
			"program": [
				"(PARI) {a(n) = my(A=1/2+x,t=floor(sqrt(2*n+1)+1/2)); for(i=0,n, A = (A + sum(m=-t,t, x^(m*(m-1)/2) * (-A)^m +x*O(x^n)) )/2 ); 2*4^n * polcoeff(A,n)}",
				"for(n=0,30,print1(a(n),\", \"))"
			],
			"xref": [
				"Cf. A268300, A268302, A268299, A190791."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Paul D. Hanna_, Feb 25 2016",
			"references": 4,
			"revision": 21,
			"time": "2016-03-02T03:53:43-05:00",
			"created": "2016-02-25T23:06:56-05:00"
		}
	]
}