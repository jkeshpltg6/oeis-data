{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A256739",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 256739,
			"data": "1,3,2,6,4,6,6,12,10,12,10,12,12,10,8,24,16,30,18,24,16,30,22,24,28,20,18,20,28,24,30,48,40,48,32,60,36,54,40,48,40,48,42,60,40,58,46,48,54,36,32,40,52,54,56,40,40,36,58,48,60,34,32,96,72,120,66",
			"name": "Unique sequence satisfying SumXOR_{d divides n} a(d) = n for any n\u003e0, where SumXOR is the analog of summation under the binary XOR operation.",
			"comment": [
				"Replacing \"SumXOR\" by \"Sum\" in the name leads to the Euler totient function (A000010).",
				"Replacing \"SumXOR\" by \"Product\" in the name leads to the exponential of Mangoldt function (A014963).",
				"a(p) = p-1 for any prime p\u003e2.",
				"a(2^k) = 2^k+2^(k-1) for any k\u003e0.",
				"A070939(a(n)) = A070939(n) for any n\u003e0.",
				"The graph of this sequence is quite remarkable. - _N. J. A. Sloane_, Apr 09 2015",
				"Xor-Moebius transform of natural numbers, A000027. See A295901 for a list of some of the properties of this transform. - _Antti Karttunen_, Dec 29 2017"
			],
			"link": [
				"Paul Tek, \u003ca href=\"/A256739/b256739.txt\"\u003eTable of n, a(n) for n = 1..16383\u003c/a\u003e",
				"Paul Tek, \u003ca href=\"/A256739/a256739.gp.txt\"\u003ePARI program for this sequence\u003c/a\u003e"
			],
			"formula": [
				"a(n) = n XOR ( SumXOR_{d divides n and d \u003c n} a(d) ) for any n\u003e0.",
				"From _Antti Karttunen_, Dec 29 2017: (Start)",
				"a(n) = SumXOR_{d|n} A296206(d).",
				"a(n) = n XOR A296207(n), where XOR is bitwise exclusive or, A003987.",
				"(End)"
			],
			"mathematica": [
				"a = Table[0, {16383}];",
				"Do[pa = n; Do[pa = BitXor[pa, a[[d]]], {d, Divisors[n]}]; a[[n]] = pa, {n, Length[a]}];",
				"a (* _Jean-François Alcover_, Oct 18 2019, after _Paul Tek_ *)"
			],
			"program": [
				"(PARI) See Links section.",
				"A256739(n) = { my(v=0); fordiv(n, d, if(issquarefree(n/d), v=bitxor(v, d))); (v); } \\\\ _Antti Karttunen_, Dec 29 2017, after code in A295901."
			],
			"xref": [
				"Cf. A000010, A003987, A014963, A295901, A296206, A296207, A297107 (fixed points)."
			],
			"keyword": "nonn,base,look",
			"offset": "1,2",
			"author": "_Paul Tek_, Apr 09 2015",
			"references": 9,
			"revision": 21,
			"time": "2019-10-18T11:28:09-04:00",
			"created": "2015-04-09T16:07:30-04:00"
		}
	]
}