{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A258065",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 258065,
			"data": "1,0,-2,-4,-1,8,6,4,-7,-8,-2,-4,10,-8,-4,0,2,16,-2,16,5,-8,0,-12,-12,-16,-2,12,-9,0,6,8,2,16,12,-20,0,-8,22,0,18,8,-32,0,4,8,-26,-28,-13,-8,0,12,-6,24,2,20,18,0,30,-16,-3,-8,-10,20,0,-16,14,-16",
			"name": "Expansion of (phi(-x^3) * f(-x^2))^2 in powers of x where phi(), f() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A258065/b258065.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of (f(x, x^2) * f(-x))^2 in powers of x where f() is the Ramanujan general theta function.",
				"Expansion of q^(-1/6) * (eta(q^2) * eta(q^3)^2 / eta(q^6))^2 in powers of q.",
				"Euler transform of period 6 sequence [ 0, -2, -4, -2, 0, -4, ...].",
				"a(n) = A030188(3*n)."
			],
			"example": [
				"G.f. = 1 - 2*x^2 - 4*x^3 - x^4 + 8*x^5 + 6*x^6 + 4*x^7 - 7*x^8 - 8*x^9 + ...",
				"G.f. = q - 2*q^13 - 4*q^19 - q^25 + 8*q^31 + 6*q^37 + 4*q^43 - 7*q^49 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (EllipticTheta[ 4, 0, x^3] QPochhammer[ x^2])^2, {x, 0, n}];",
				"a[ n_] := SeriesCoefficient[ (QPochhammer[ x^2] QPochhammer[ x^3]^2 / QPochhammer[ x^6])^2, {x, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, A = x * O(x^n); polcoeff( (eta(x^2 + A) * eta(x^3 + A)^2 / eta(x^6 + A))^2, n))};",
				"(MAGMA) A := Basis( ModularForms( Gamma0(72), 2), 409); A[2] - 2*A[14];",
				"(MAGMA) A := Basis( CuspForms( Gamma0(72), 2), 409); A[1];"
			],
			"keyword": "sign",
			"offset": "0,3",
			"author": "_Michael Somos_, May 18 2015",
			"references": 1,
			"revision": 10,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2015-05-18T17:46:08-04:00"
		}
	]
}