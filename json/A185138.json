{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A185138",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 185138,
			"data": "0,0,1,1,3,3,15,6,14,10,45,15,33,21,91,28,60,36,153,45,95,55,231,66,138,78,325,91,189,105,435,120,248,136,561,153,315,171,703,190,390,210,861,231,473,253,1035,276,564,300,1225,325",
			"name": "a(4*n) = n*(4*n-1); a(2*n+1) = n*(n+1)/2; a(4*n+2) = (2*n+1)*(4*n+1).",
			"comment": [
				"a(n) is divisible by the n-th term of the sequence 3, 3, 1, 1, 3, 3 (periodically repeated with period 6).",
				"a(n) is divisible by b(floor((n-1)/3)), where b(n) = 1, 3, 2, 3, 7, 3, 5, 3, 13, 3, 8, 3, 19, 3,... , n\u003e=0, is defined by inserting a 3 after each entry of A165355.",
				"(n+1)*(n+2)*(n+3)/2=3*A000292(n+1) is divisible by a(n+2), so there is an integer sequence c(n)= 3*A000292(n+1)/a(n+2) = 3, 12, 10, 20, 7, 28, 18,... with c(2*n)=A123167(n+1) and c(n)/A109613(n+2)=A176895(n).",
				"The sequence of denominators of a(n+2)/n has period length 8: 1, 2, 1, 4, 1, 1, 1, 4.",
				"A table T(k,c) = a(1+c*(1+2k)) of (2*k+1)-sections starts as follows:",
				"0  1  1   3   3   15...",
				"0  3  6  45  21   60...",
				"0 15 15  60  55  325...",
				"0 14 28 231 105  315...",
				"0 45 45 189 171 1035...",
				"The table of T'(k,c) = T(k,c)/(2k+1), columns c\u003e=0, looks as follows, construction similar to A165943:",
				"0 1 1  3  3  15  6  14   k=0",
				"0 1 2 15  7  20 15  77   k=1",
				"0 3 3 12 11  65 24  63   k=2",
				"0 2 4 33 15  45 33 175   k=3",
				"0 5 5 21 19 115 42 112   k=4",
				"0 3 6 51 23  70 51 273   k=5",
				"The entries T'(k,c) are divisible by A060819(c).",
				"Differences are T'(2,c)-T'(0,c) = T'(4,c)-T'(2,c) = 0, 2, 2, 9, 8, 50, 18, 49, 32, ... which is A168077(c) multiplied by the c-th term of the period-4 sequence 2, 2, 2, 1.",
				"Differences are T'(3,c)- T'(1,c) = T'(5,c)-T'(3,c) = 0, 1, 2, 18, 8, 25, 18, 98, 32,... which is  A168077(c) multiplied by the period-4 sequence 2, 1, 2, 2.",
				"The reduced fractions T'(0,c)/T'(1,c) = 1, 1/2, 1/5, 3/7, 3/4, 2/5, 2/11, 5/13, 5/7, 3/8, 3/17, 7/19, .., c\u003e=1, have a numerator sequence A026741(floor(c/2)+1). The denominator sequence is f(c) = 1, 2, 5, 7, 4, 5,.. = A001651(c+1)/A130658(c+1), with f(2*c+1) +f(2*c+2) = 3, 12, 9, 24 .. =3*A022998(c)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A185138/b185138.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_12\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,0,0,3,0,0,0,-3,0,0,0,1)."
			],
			"formula": [
				"a(n) = 3*a(n-4) - 3*a(n-8) + a(n-12).",
				"a(2*n) = A064038(2*n), a(2*n+1) = A000217(n).",
				"a(n) = 3*A208950(n)/A109613(n).",
				"a(n+1) = A060819(n) * A026741(n+2)(floor(n/2)).",
				"G.f.: -x^2*(3*x^8+x^7+5*x^6+3*x^5+12*x^4+3*x^3+3*x^2+x+1)/ ((x-1)^3*(x+1)^3*(x^2+1)^3). - _R. J. Mathar_, Mar 22 2012",
				"a(n) = (4*n^2-3*n-1+(2*n^2-3*n+1)*(-1)^n + n*(n-1)*(1+(-1)^n)*(-1)^((2*n-3-(-1)^n)/4))/16. - _Luce ETIENNE_, May 13 2016"
			],
			"maple": [
				"A185138 := proc(n)",
				"        if n mod 4 = 0 then",
				"                return n/4*(n-1) ;",
				"        elif n mod 2 = 1 then",
				"                return (n-1)*(n+1)/8 ;",
				"        else",
				"                return (n-1)*n/2 ;",
				"        end if;",
				"end proc: # _R. J. Mathar_, Apr 05 2012"
			],
			"mathematica": [
				"Clear[b];b[1] = 0; b[2] = 0; b[3] = 1; b[4] = 1; b[5] = 3; b[6] = 3; b[7] = 15;b[8] = 6;b[n_Integer] := b[n] = ((-2 + n) (-4 (-4 + n) (-3 + n) (-2 + n) (8 + n (-9 + 2 n)) b[-3 + n] + (-5 + n) ((-3 +n) ((-4 + n) (211 + 2 n (-215 + n (147 + n (-41 + 4 n)))) - 4 (-1 + n) (19 + n (-13 + 2 n)) b[-2 + n]) - 4 (-4 + n)^2 (8 + n (-9 + 2 n)) b[-1 + n])))/(4 (-5 + n) (-4 + n) (-3 + n)^2 (19 + n (-13 + 2 n)))",
				"a = Table[b[n], {n, 1, 52}] (* _Roger L. Bagula_, Mar 14 2012 *)",
				"LinearRecurrence[{0,0,0,3,0,0,0,-3,0,0,0,1},{0,0,1,1,3,3,15,6,14,10,45,15},60] (* _Harvey P. Dale_, Nov 23 2015 *)"
			],
			"program": [
				"(PARI) x='x+O('x^50); concat([0,0], Vec(-x^2*(3*x^8+x^7+5*x^6+3*x^5+12*x^4+3*x^3+3*x^2+x+1)/ ((x-1)^3*(x+1)^3*(x^2+1)^3))) \\\\ _G. C. Greubel_, Jun 23 2017"
			],
			"xref": [
				"Cf. A033991, A000217, A014634."
			],
			"keyword": "nonn,easy,less",
			"offset": "0,5",
			"author": "_Paul Curtz_, Mar 12 2012",
			"references": 2,
			"revision": 52,
			"time": "2017-06-23T14:04:26-04:00",
			"created": "2012-04-09T16:55:42-04:00"
		}
	]
}