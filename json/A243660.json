{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A243660",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 243660,
			"data": "1,3,2,12,16,5,55,110,70,14,273,728,702,288,42,1428,4760,6160,3850,1155,132,7752,31008,50388,42432,19448,4576,429,43263,201894,395010,418950,259350,93366,18018,1430,246675,1315600,3010700,3853696,3010700,1466080,433160,70720,4862",
			"name": "Triangle read by rows: the x = 1+q Narayana triangle at m=2.",
			"comment": [
				"See Novelli-Thibon (2014) for precise definition."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A243660/b243660.txt\"\u003eTable of n, a(n) for n = 1..1275\u003c/a\u003e (first 50 rows)",
				"J.-C. Novelli and J.-Y. Thibon, \u003ca href=\"http://arxiv.org/abs/1403.5962\"\u003eHopf Algebras of m-permutations,(m+1)-ary trees, and m-parking functions\u003c/a\u003e, arXiv preprint arXiv:1403.5962 [math.CO], 2014. See Fig. 8."
			],
			"formula": [
				"From _Werner Schulte_, Nov 23 2018: (Start)",
				"T(n,k) = binomial(3*n+1-k,n-k) * binomial(2*n,k-1) / n.",
				"More generally: T(n,k) = binomial((m+1)*n+1-k,n-k) * binomial(m*n,k-1) / n, where m = 2.",
				"Sum_{k=1..n} (-1)^k * T(n,k) = -1. (End)"
			],
			"example": [
				"Triangle begins:",
				"     1;",
				"     3,    2;",
				"    12,   16,    5;",
				"    55,  110,   70,   14;",
				"   273,  728,  702,  288,   42;",
				"  1428, 4760, 6160, 3850, 1155,  132;",
				"  ..."
			],
			"mathematica": [
				"polrecip[P_, x_] := P /. x -\u003e 1/x // Together // Numerator;",
				"P[n_, m_] := Sum[Binomial[m n + 1, k] Binomial[(m+1) n - k, n - k] (1-x)^k x^(n-k), {k, 0, n}]/(m n + 1);",
				"T[m_] := Reap[For[i=1, i \u003c= 20, i++, z = polrecip[P[i, m], x] /. x -\u003e 1+q; Sow[CoefficientList[z, q]]]][[2, 1]];",
				"T[2] // Flatten (* _Jean-François Alcover_, Oct 08 2018, from PARI *)"
			],
			"program": [
				"(PARI)",
				"N(n,m)=sum(k=0,n,binomial(m*n+1,k)*binomial((m+1)*n-k,n-k)*(1-x)^k*x^(n-k))/(m*n+1);",
				"T(m)=for(i=1,20,z=subst(polrecip(N(i,m)),x,1+q);print(Vecrev(z)));",
				"T(2)  /* _Lars Blomberg_, Jul 17 2017 */",
				"(PARI) T(n,k) = binomial(3*n+1-k,n-k) * binomial(2*n,k-1) / n; \\\\ _Andrew Howroyd_, Nov 23 2018"
			],
			"xref": [
				"Row sums give A034015(n-1).",
				"The case m=1 is A126216 or A033282 (its mirror image).",
				"The case m=3 is A243661.",
				"The right diagonal is A000108.",
				"The left column is A001764."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Jun 13 2014",
			"ext": [
				"Corrected example and a(22)-a(43) from _Lars Blomberg_, Jul 12 2017",
				"a(44)-a(45) from _Werner Schulte_, Nov 23 2018"
			],
			"references": 2,
			"revision": 60,
			"time": "2021-11-08T08:20:14-05:00",
			"created": "2014-06-13T19:02:27-04:00"
		}
	]
}