{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A319195",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 319195,
			"data": "1,0,0,1,0,2,0,0,0,1,0,1,1,0,0,2,0,0,0,0,1,0,1,0,1,0,0,1,1,0,2,1,0,0,0,2,0,1,2,0,0,0,0,0,1,0,1,0,0,1,0,0,1,0,1,0,2,0,1,0,0,0,1,1,0,1,1,1,0,0,2,1,0,0,0,0,2,0,1,0,2,0,0,1,2,0,2,2,0,0,0,0,0,0,1,0,1,0,0,0,1,0",
			"name": "Irregular triangle with the unique representation of positive integers in the tribonacci ABC-representation.",
			"comment": [
				"The row length L(n) of this irregular triangle is A316714(n), n \u003e= 1.",
				"This representation is based on the complementary and disjoint sets A, B and C given by the sequences A278040, A278039 and A278041, respectively. In the present notation sequences A, B and C are denoted by 1, 0 and 2.",
				"The numbers are represented by iterations of these sequences always starting with B(0) = 0 (in analogy to the Wythoff B sequence in the Fibonacci case). Uniqueness requires that the representations end in A(B(0)) or C(B(0)).",
				"B^[k](0) (k-fold iterations) for k \u003e= 2 are forbidden. One could represent the number 0 by B(0), but this is not done here, because it is found that the ABC-representations of positive numbers is equivalent to the tribonacci representation of positive numbers given in A278038 for n \u003e= 1 (n = 0 is not represented by T(1) = A000073(1) = 0. This representation uses the tribonacci numbers {T(k)}_{k \u003e= 3} = {1, 2, 4, 7, 13, ...} for uniqueness reason).",
				"For this table the operation of sequences A, B and C is denoted by 1, 0 and 2, respectively, and the brackets and the final argument (0) of B(0) are not recorded. E.g., A(B(C(B(0)))) is written as 1020.",
				"Another form of this table is given in A316713 where A, B and C are denoted 2, 1 and 3, respectively.",
				"An equivalent such representation is given by A317206 using different complementary sequences A, B and C, related to our B = A278039,  A = A278040, and C = A278041: A(n) = A003144(n) = A278039(n-1) + 1, B(n) = A003145(n) =  A278040(n-1) + 1, C(n) = A003146(n) = A278041(n-1) + 1 with n \u003e= 1.",
				"The present representation is the analog to the Wythoff representation of positive numbers (A189921 or A317208) using the Wythoff A and B sequences A000201 and A001950, respectively.",
				"The number length of the ABC-representation of n \u003e= 1 is L(n) = A316714(n). The number of 0's (B's), 1's (A's) and 2's (C's) of the representation of n is A316715, A316716, A316717."
			],
			"link": [
				"Wolfdieter Lang, \u003ca href=\"https://arxiv.org/abs/1810.09787\"\u003eThe Tribonacci and ABC Representations of Numbers are Equivalent\u003c/a\u003e, arXiv preprint arXiv:1810.09787 [math.NT], 2018."
			],
			"example": [
				"The complementary and disjoint sequences A, B, C begin, for n \u003e= 0:",
				"n: 0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15  16  17  18  19  20  21  22 ...",
				"A: 1  5  8 12 14 18 21 25 29 32 36 38 42 45 49 52  56  58  62  65  69  73  76 ...",
				"B: 0  2  4  6  7  9 11 13 15 17 19 20 22 24 26 28  30  31  33  35  37  39  41 ...",
				"C: 3 10 16 23 27 34 40 47 54 60 67 71 78 84 91 97 104 108 115 121 128 135 141 ...",
				"---------------------------------------------------------------------------------",
				"The ABC-representation of the positive integers begins:",
				"                                              #(1)   #(2)    #(3)    L(n)",
				"                                           A316715 A316716 A316717 A316714",
				"n = 1:      10               A(B(0)) =  1      1      1       0       2",
				"n = 2:     010            B(A(B(0))) =  2      2      1       0       3",
				"n = 3:      20               C(B(0)) =  3      1      0       1       2",
				"n = 4:    0010         B(B(A(B(0)))) =  4      3      1       0       4",
				"n = 5:     110            A(A(B(0))) =  5      1      2       0       3",
				"n = 6:     020            B(C(B(0))) =  6      2      0       1       3",
				"n = 7:   00010      B(B(B(A(B(0))))) =  7      4      1       0       5",
				"n = 8:    1010         A(B(A(B(0)))) =  8      2      2       0       4",
				"n = 9:    0110         B(A(A(B(0)))) =  9      2      2       0       4",
				"n = 10:    210            C(A(B(0))) = 10      1      1       1       3",
				"n = 11:   0020         B(B(C(B(0)))) = 11      3      0       1       4",
				"n = 12:    120            A(C(B(0))) = 12      1      1       1       3",
				"n = 13: 000010   B(B(B(B(A(B(0)))))) = 13      5      1       0       6",
				"n = 14:  10010      A(B(B(A(B(0))))) = 14      3      2       0       5",
				"n = 15:  01010      B(A(B(A(B(0))))) = 15      3      2       0       5",
				"n = 16:   2010         C(B(A(B(0)))) = 16      2      1       1       4",
				"n = 17:  00110      B(B(A(A(B(0))))) = 17      3      2       0       5",
				"n = 18:   1110         A(A(A(B(0)))) = 18      1      3       0       4",
				"n = 19:   0210         B(C(A(B(0)))) = 19      2      1       1       4",
				"n = 20:  00020      B(B(B(C(B(0))))) = 20      4      0       1       5",
				"..."
			],
			"xref": [
				"Cf. A000073, A000201, A001950, A189921, A003144, A003145, A003146, A278038, A278040, A278039, A278041, A316713, A316714, A316715, A316716, A316717, A317208."
			],
			"keyword": "nonn,tabf,easy",
			"offset": "1,6",
			"author": "_Wolfdieter Lang_, Sep 13 2018",
			"references": 0,
			"revision": 16,
			"time": "2020-04-06T18:58:13-04:00",
			"created": "2018-09-17T17:53:26-04:00"
		}
	]
}