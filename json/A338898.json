{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A338898",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 338898,
			"data": "1,1,1,2,2,2,1,3,1,4,2,3,2,4,1,5,3,3,1,6,2,5,1,7,3,4,1,8,2,6,1,9,4,4,2,7,3,5,2,8,1,10,1,11,3,6,2,9,1,12,4,5,1,13,3,7,1,14,2,10,4,6,2,11,1,15,3,8,1,16,2,12,3,9,1,17,4,7,5,5,1,18,2",
			"name": "Concatenated sequence of prime indices of semiprimes (A001358).",
			"comment": [
				"This is a triangle with two columns and weakly increasing rows, namely {A338912(n), A338913(n)}.",
				"A semiprime is a product of any two prime numbers. A prime index of n is a number m such that the m-th prime number divides n. The multiset of prime indices of n is row n of A112798."
			],
			"example": [
				"The sequence of semiprimes together with their prime indices begins:",
				"      4: {1,1}     46: {1,9}      91: {4,6}     141: {2,15}",
				"      6: {1,2}     49: {4,4}      93: {2,11}    142: {1,20}",
				"      9: {2,2}     51: {2,7}      94: {1,15}    143: {5,6}",
				"     10: {1,3}     55: {3,5}      95: {3,8}     145: {3,10}",
				"     14: {1,4}     57: {2,8}     106: {1,16}    146: {1,21}",
				"     15: {2,3}     58: {1,10}    111: {2,12}    155: {3,11}",
				"     21: {2,4}     62: {1,11}    115: {3,9}     158: {1,22}",
				"     22: {1,5}     65: {3,6}     118: {1,17}    159: {2,16}",
				"     25: {3,3}     69: {2,9}     119: {4,7}     161: {4,9}",
				"     26: {1,6}     74: {1,12}    121: {5,5}     166: {1,23}",
				"     33: {2,5}     77: {4,5}     122: {1,18}    169: {6,6}",
				"     34: {1,7}     82: {1,13}    123: {2,13}    177: {2,17}",
				"     35: {3,4}     85: {3,7}     129: {2,14}    178: {1,24}",
				"     38: {1,8}     86: {1,14}    133: {4,8}     183: {2,18}",
				"     39: {2,6}     87: {2,10}    134: {1,19}    185: {3,12}"
			],
			"mathematica": [
				"primeMS[n_]:=If[n==1,{},Flatten[Cases[FactorInteger[n],{p_,k_}:\u003eTable[PrimePi[p],{k}]]]];",
				"Join@@primeMS/@Select[Range[100],PrimeOmega[#]==2\u0026]"
			],
			"xref": [
				"A112798 restricted to rows of length 2 gives this triangle.",
				"A115392 is the row number for the first appearance of each positive integer.",
				"A176506 gives row differences.",
				"A338899 is the squarefree version.",
				"A338912 is column 1.",
				"A338913 is column 2.",
				"A001221 counts a number's distinct prime indices.",
				"A001222 counts a number's prime indices.",
				"A001358 lists semiprimes.",
				"A004526 counts 2-part partitions.",
				"A006881 lists squarefree semiprimes.",
				"A037143 lists primes and semiprimes.",
				"A046315 and A100484 list odd and even semiprimes.",
				"A046388 and A100484 list odd and even squarefree semiprimes.",
				"A065516 gives first differences of semiprimes.",
				"A084126 and A084127 give the prime factors of semiprimes.",
				"A270650 and A270652 give the prime indices of squarefree semiprimes.",
				"A320655 counts factorizations into semiprimes.",
				"Cf. A056239, A101048, A320892, A320912, A338900, A338901, A338904, A338906, A338907, A338910, A338911."
			],
			"keyword": "nonn,tabf",
			"offset": "1,4",
			"author": "_Gus Wiseman_, Nov 15 2020",
			"references": 44,
			"revision": 8,
			"time": "2020-11-20T17:19:09-05:00",
			"created": "2020-11-20T17:19:09-05:00"
		}
	]
}