{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A324495",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 324495,
			"data": "1,3,4,7,9,9,10,15,18,18,20,18,20,20,21,31,37,37,40,37,40,40,41,37,40,40,41,40,41,41,42,63,74,74,78,74,78,78,80,74,78,78,80,78,80,80,82,74,78,78,80,78,80,80,82,78,80,80,82,80,82,82,83,127,147,147,153",
			"name": "Average number of steps t(n) required to get n by repeatedly toggling one of the ceiling(log_2(n)) bits of the binary result of the previous step at a random position with equal probability of the bit positions, starting with all bits 0. The fractional part of t is given separately, i.e., t(n) = a(n) + A324496(n)/A324497(n).",
			"comment": [
				"The problem is related to random walks on the edges of n-dimensional hypercubes.",
				"a(n) is only dependent on the length of the binary representation A070939(n) and on the binary weight A000120(n)."
			],
			"link": [
				"Hugo Pfoertner, \u003ca href=\"/A324495/b324495.txt\"\u003eTable of n, a(n) for n = 1..4096\u003c/a\u003e",
				"P. Diaconis, R. L. Graham, J. A. Morrison, \u003ca href=\"https://statistics.stanford.edu/research/asymptotic-analysis-random-walk-hypercube-many-dimensions\"\u003eAsymptotic Analysis of a Random Walk on a Hypercube with Many Dimensions\u003c/a\u003e, Technical Report EFS NFS 307, Department of Statistics, Stanford University, December 1988.",
				"Persi Diaconis, R. L. Graham, J. A. Morrison, \u003ca href=\"https://doi.org/10.1002/rsa.3240010105\"\u003eAsymptotic analysis of a random walk on a hypercube with many dimensions\"\u003c/a\u003e, Random Structures \u0026 Algorithms, Volume 1, Issue 1, Pages 51-72, Spring 1990.",
				"IBM Research, \u003ca href=\"https://www.research.ibm.com/haifa/ponderthis/challenges/April2006.html\"\u003ePonder This April 2006 - Challenge\u003c/a\u003e, Random walks on an n-dimensional hypercube.",
				"IBM Research, \u003ca href=\"https://www.research.ibm.com/haifa/ponderthis/solutions/April2006.html\"\u003ePonder This April 2006 - Challenge\u003c/a\u003e, Solution."
			],
			"example": [
				"a(5) = 9 is given by the sum of occurrence probabilities of toggle chains of even lengths 2*k, multiplied by the lengths.",
				"a(5) = Sum_{k\u003e=1} 4*k*7^(k-1) / 3^(2*k) = 9.",
				"The corresponding simulation results for 10^10 toggle chains are",
				"  2*k Probability P    2*k*P      Cumulated",
				"    2   0.22222334  0.44444668    0.444447",
				"    4   0.17284183  0.69136731    1.135814",
				"    6   0.13442963  0.80657780    1.942392",
				"    8   0.10455718  0.83645746    2.778849",
				"   10   0.08131600  0.81315998    3.592009",
				"   ...",
				"  196   0.00000000  0.00000002    9.000068",
				".",
				"a(7) = Sum_{k\u003e=1} 2*(2*k+1)*7^(k-1) / 3^(2*k) = 10."
			],
			"xref": [
				"Cf. A000120, A003149, A070939, A099627, A324496, A324497."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Hugo Pfoertner_, Mar 05 2019",
			"references": 3,
			"revision": 35,
			"time": "2020-09-20T15:05:46-04:00",
			"created": "2019-03-06T08:35:20-05:00"
		}
	]
}