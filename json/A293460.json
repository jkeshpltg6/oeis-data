{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A293460",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 293460,
			"data": "0,1,1,1,1,2,1,1,1,2,1,2,1,2,2,1,1,2,1,2,2,2,1,2,1,2,1,2,1,2,1,1,2,2,2,2,1,2,2,2,1,2,1,2,2,2,1,2,1,2,2,2,1,2,2,2,2,2,1,2,1,2,2,1,2,3,2,3,3,4,3,4,3,4,4,4,4,5,4,5,4,5,4,5,4,4,4",
			"name": "a(n) = Sum_{k=1..n} sign(omega(n+1) - omega(n)) (where omega(m) = A001221(m), the number of distinct primes dividing m).",
			"comment": [
				"The sign function is defined by:",
				"- sign(0) = 0,",
				"- sign(n) = +1 for any n \u003e 0,",
				"- sign(n) = -1 for any n \u003c 0.",
				"a(n) corresponds to the number of integers up to n in A294277 minus the number of integers up to n in A294278.",
				"The first negative value occurs at a(178) = -1.",
				"Will this sequence change sign indefinitely?"
			],
			"link": [
				"Georg Fischer, \u003ca href=\"/A293460/b293460.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A293460/a293460.png\"\u003eLine graph of the first 10000 terms\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A293460/a293460_1.png\"\u003eLine graph of the first 100000000 terms\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A293460/a293460_2.png\"\u003eLine graph of the first 1000000000 terms\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A293460/a293460_3.png\"\u003eLine graph of the first 10000000000 terms\u003c/a\u003e"
			],
			"formula": [
				"a(0) = 0, and for any n \u003e 0:",
				"- a(A294277(n)) = a(A294277(n)-1) + 1,",
				"- a(A006049(n)) = a(A006049(n)-1),",
				"- a(A294278(n)) = a(A294278(n)-1) - 1.",
				"Also: a(n) = #{ k / A294277(k) \u003c= n } - #{ k / A294278(k) \u003c= n }."
			],
			"example": [
				"The following table shows the first terms of the sequence, alongside sign(omega(n+1)-omega(n)), omega(n+1) and omega(n):",
				"n       a(n)    sign    w(n+1)  w(n)",
				"-       ----    ----    ------  ----",
				"0       0",
				"1       1       1       1       0",
				"2       1       0       1       1",
				"3       1       0       1       1",
				"4       1       0       1       1",
				"5       2       1       2       1",
				"6       1       -1      1       2",
				"7       1       0       1       1",
				"8       1       0       1       1",
				"9       2       1       2       1",
				"10      1       -1      1       2",
				"11      2       1       2       1",
				"12      1       -1      1       2",
				"13      2       1       2       1",
				"14      2       0       2       2",
				"15      1       -1      1       2",
				"16      1       0       1       1",
				"17      2       1       2       1",
				"18      1       -1      1       2",
				"19      2       1       2       1",
				"20      2       0       2       2"
			],
			"program": [
				"(PARI) s = 0; for (n=1, 87, print1 (s \", \"); s += sign(omega(n+1)-omega(n)))"
			],
			"xref": [
				"Cf. A001221, A006049, A294277, A294278."
			],
			"keyword": "sign",
			"offset": "0,6",
			"author": "_Rémy Sigrist_, Oct 26 2017",
			"references": 1,
			"revision": 56,
			"time": "2019-02-19T10:22:00-05:00",
			"created": "2017-10-28T09:24:42-04:00"
		}
	]
}