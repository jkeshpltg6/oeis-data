{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A242100",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 242100,
			"data": "6,10,12,18,20,24,30,34,36,40,42,48,56,66,68,72,80,84,90,96,108,110,130,132,136,144,150,156,160,182,192,210,222,240,246,252,258,260,264,270,272,288,306,320,324,342,350,380,384,392,420,462,506,514,516,520",
			"name": "Numbers of the form m = b^i + b^j, where b \u003e 1 and i \u003e j \u003e 0.",
			"comment": [
				"If m is a term, then there is a base b \u003e 1 such that the base-b representation of m has digital sum = 2.",
				"The base b for which m = b^i + b^j is not uniquely determined. Example: 12 = 2^3+2^2 = 3^2 +3^1."
			],
			"link": [
				"Hieronymus Fischer, \u003ca href=\"/A242100/b242100.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) \u003c n^2 for n \u003e 4.",
				"lim a(n)/n^2 = 1, for n --\u003e infinity."
			],
			"example": [
				"a(1)    = 6, since 2 = 2^2 + 2^1.",
				"a(7)    = 30, since 30 = 3^3 + 3^1.",
				"a(10)   = 40.",
				"a(10^2) = 1722.",
				"a(10^3) = 377610.",
				"a(10^4) = 70635620.",
				"a(10^5) = 8830078992.",
				"a(10^6) = 951958292172.",
				"a(10^7) = 97932587392010.",
				"a(10^8) = 9908034917287656.",
				"a(10^9) = 995834160614903742."
			],
			"program": [
				"(Smalltalk)",
				"distinctPowersWithOffset: d",
				"  \"Answers an array which holds the first n numbers of the form b^i + b^j + d, i\u003ej\u003e0, where b is any natural number \u003e 1, d is any integer number, and n is the receiver (d=0 for this sequence).",
				"  Usage: n distinctPowersWithOffset: 0",
				"  Answer: #(6 10 12 ...) [first n terms]\"",
				"  | n terms m |",
				"  terms := SortedCollection new.",
				"  n := self.",
				"  m := n squared max: 20.",
				"  terms := m floorDistinctPowersWithOffset: d.",
				"  ^terms copyFrom: 1 to: n",
				"----------",
				"floorDistinctPowersWithOffset: d",
				"  \"Answers an array which holds the numbers \u003c n of the form b^i + b^j + d, i\u003ej\u003e0, where b is any natural number \u003e 1, d is any integer number, and n is the receiver (d=0 for this sequence).",
				"  Usage: n floorDistinctPowersWithOffset: 0",
				"  Answer: #(6 10 12 18 ...) [all terms \u003c n]\"",
				"  | bmax p q n m terms a |",
				"  terms := OrderedCollection new.",
				"  n := self.",
				"  bmax := ((4 * (n - d) + 1) sqrtTruncated - 1) // 2.",
				"  2 to: bmax",
				"    do:",
				"         [:b |",
				"         p := b * b.",
				"         q := b.",
				"         a := p + q + d.",
				"         [a \u003c n] whileTrue:",
				"                   [[q \u003c p and: [a \u003c n]] whileTrue:",
				"                            [terms add: a.",
				"                            q := b * q.",
				"                            a := p + q + d].",
				"                   p := b * p.",
				"                   q := b.",
				"                   a := p + q + d]].",
				"  ^terms asSet asOrderedCollection sorted"
			],
			"xref": [
				"Cf. A001597, A018900, A239709, A239710."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Hieronymus Fischer_, May 04 2014",
			"references": 2,
			"revision": 13,
			"time": "2014-05-14T19:43:27-04:00",
			"created": "2014-05-14T19:41:20-04:00"
		}
	]
}