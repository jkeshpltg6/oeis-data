{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A300153",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 300153,
			"data": "1,4,4,2,1,3,8,12,12,8,3,4,1,4,5,12,20,24,24,20,12,4,2,9,1,10,3,7,16,28,4,40,40,4,28,16,5,8,12,12,1,12,14,8,9,20,36,48,56,60,60,56,48,36,20,6,3,2,4,20,1,21,4,3,5,11,24,44,60,72,80,84,84,80",
			"name": "Square array T(n, k) read by antidiagonals upwards, n \u003e 0 and k \u003e 0: T(n, k) is the number of parts inscribed in a rose or rhodonea curve with polar coordinates r = cos(t * (k/n)).",
			"comment": [
				"For any real p \u003e 0, the rose or rhodonea curve with polar coordinates r = cos(t * p):",
				"- is dense in the unit disk when p is irrational,",
				"- is closed when p is rational, say p = u/v in reduced form; in that case, the number of parts inscribed in the curve is T(v, u),",
				"- see also the illustration in Links section."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A300153/a300153.png\"\u003eIllustration of the first terms\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Rose.html\"\u003eRose\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Rose_(mathematics)\"\u003eRose (mathematics)\u003c/a\u003e"
			],
			"formula": [
				"T(1, k) = A022998(k).",
				"T(n, k) = T(n/gcd(n, k), k/gcd(n, k)).",
				"Empirically, when gcd(n, k) = 1, we have the following formulas depending on the parity of n and of k:",
				"             | k is odd                       | k is even",
				"   ----------+--------------------------------+--------------------",
				"   n is odd  | T(n, k) =     k * A029578(n+1) | T(n, k) = 2 * k * n",
				"   n is even | T(n, k) = 2 * k * A029578(n+1) | N/A"
			],
			"example": [
				"Array T(n, k) begins:",
				"  n\\k|    1    2    3    4    5    6    7    8    9",
				"  ---+---------------------------------------------",
				"    1|    1    4    3    8    5   12    7   16    9",
				"    2|    4    1   12    4   20    3   28    8   36",
				"    3|    2   12    1   24   10    4   14   48    3",
				"    4|    8    4   24    1   40   12   56    4   72",
				"    5|    3   20    9   40    1   60   21   80   27",
				"    6|   12    2    4   12   60    1   84   24   12",
				"    7|    4   28   12   56   20   84    1  112   36",
				"    8|   16    8   48    4   80   24  112    1  144",
				"    9|    5   36    2   72   25   12   35  144    1",
				"   10|   20    3   60   20    4    9  140   40  180",
				"   11|    6   44   18   88   30  132   42  176   54",
				"...",
				"The following diagram shows the curve for T(2, 1) and the corresponding 4 parts:",
				"                         |",
				"               ########     ########",
				"           #####      #######      #####",
				"        ###          ###   ###          ###",
				"      ###           ##   |   ##           ###",
				"     ##            ##         ##            ##",
				"    ##             #  Part #2  #             ##",
				"   ##              ##         ##              ##",
				"   #                ###  |  ###                #",
				"  -#- - - Part #3  - -#######- -  Part #1 - - -#-",
				"   #                ###  |  ###                #",
				"   ##              ##         ##              ##",
				"    ##             #  Part #4  #             ##",
				"     ##            ##         ##            ##",
				"      ###           ##   |   ##           ###",
				"        ###          ###   ###          ###",
				"           #####      #######      #####",
				"               ########     ########",
				"                         |"
			],
			"xref": [
				"Cf. A022998, A029578."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Rémy Sigrist_, Feb 26 2018",
			"references": 2,
			"revision": 19,
			"time": "2018-02-28T08:33:51-05:00",
			"created": "2018-02-28T08:33:51-05:00"
		}
	]
}