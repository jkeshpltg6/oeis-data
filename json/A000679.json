{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000679",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 679,
			"id": "M1470 N0581",
			"data": "1,1,2,5,14,51,267,2328,56092,10494213,49487365422",
			"name": "Number of groups of order 2^n.",
			"reference": [
				"James Gleick, Faster, Vintage Books, NY, 2000 (see pp. 259-261).",
				"M. Hall, Jr. and J. K. Senior, The Groups of Order 2^n (n \u003c= 6). Macmillan, NY, 1964.",
				"Newman, M. F. (1990). Groups of prime-power order. In Groups—Canberra 1989 (pp. 49-62). Springer, Berlin, Heidelberg. See Table 1.",
				"Newman, M. F. and O'Brien, E. A.; A CAYLEY library for the groups of order dividing 128. Group theory (Singapore, 1987), 437-442, de Gruyter, Berlin-New York, 1989.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Hans Ulrich Besche and Bettina Eick, \u003ca href=\"https://doi.org/10.1006/jsco.1998.0258\"\u003eConstruction of finite groups\u003c/a\u003e, Journal of Symbolic Computation, Vol. 27, No. 4, Apr 15 1999, pp. 387-404.",
				"Hans Ulrich Besche and Bettina Eick, \u003ca href=\"http://www.icm.tu-bs.de/~beick/publ/1000.ps\"\u003eThe groups of order at most 1000 except 512 and 768\u003c/a\u003e, Journal of Symbolic Computation, Vol. 27, No. 4, Apr 15 1999, pp. 405-413.",
				"Hans Ulrich Besche, Bettina Eick and E. A. O'Brien, \u003ca href=\"https://doi.org/10.1090/S1079-6762-01-00087-7\"\u003eThe groups of order at most 2000\u003c/a\u003e, Electron. Res. Announc. Amer. Math. Soc. 7 (2001), 1-4.",
				"Hans Ulrich Besche, \u003ca href=\"http://www.icm.tu-bs.de/ag_algebra/software/small/small.html\"\u003eThe Small Groups library\u003c/a\u003e",
				"Bettina Eick and E. A. O'Brien, \u003ca href=\"http://www.math.auckland.ac.nz/~obrien/research/count-pgroups.pdf\"\u003eEnumerating p-groups\u003c/a\u003e. Group theory. J. Austral. Math. Soc. Ser. A 67 (1999), no. 2, 191-205.",
				"R. K. Guy, \u003ca href=\"/A005347/a005347.pdf\"\u003eThe Second Strong Law of Small Numbers\u003c/a\u003e, Math. Mag, 63 (1990), no. 1, 3-20. [Annotated scanned copy]",
				"Rodney James and John Cannon, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1969-0238953-8\"\u003eComputation of isomorphism classes of p-groups\u003c/a\u003e, Mathematics of Computation 23.105 (1969): 135-140.",
				"R. James, M. F. Newman, and E. A. O'Brien, \u003ca href=\"https://doi.org/10.1016/0021-8693(90)90244-I\"\u003eThe Groups of Order 128\u003c/a\u003e, J. Algebra 129, 136-158, 1990.",
				"G. A. Miller, \u003ca href=\"http://www.jstor.org/stable/2370630\"\u003eDetermination of all the groups of order 64\u003c/a\u003e, Amer. J. Math., 52 (1930), 617-634.",
				"E. A. O'Brien, \u003ca href=\"https://doi.org/10.1016/0021-8693(91)90261-6\"\u003eThe Groups of Order 256\u003c/a\u003e J. Algebra 143, 219-235, 1991.",
				"E. Rodemich, \u003ca href=\"http://dx.doi.org/10.1016/0021-8693(90)90244-I\"\u003eThe groups of order 128\u003c/a\u003e, J. Algebra 67 (1980), no. 1, 129-142.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/FiniteGroup.html\"\u003eFinite Group\u003c/a\u003e",
				"M. Wild, \u003ca href=\"https://doi.org/10.2307/30037381\"\u003eThe groups of order 16 made easy\u003c/a\u003e, Amer. Math. Monthly, 112 (No. 1, 2005), 20-31.",
				"\u003ca href=\"/index/Gre#groups\"\u003eIndex entries for sequences related to groups\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 2^((2/27)n^3 + O(n^(8/3)))."
			],
			"example": [
				"G.f. = 1 + x + 2*x^2 + 5*x^3 + 14*x^4 + 51*x^5 + 267*x^6 + 2328*x^7 + ..."
			],
			"maple": [
				"seq(GroupTheory:--NumGroups(2^n),n=0..10); # _Robert Israel_, Oct 15 2017"
			],
			"mathematica": [
				"Join[{1}, FiniteGroupCount[2^Range[10]]] (* _Vincenzo Librandi_, Mar 28 2018 *)"
			],
			"program": [
				"(GAP) A000679 := List([0..8],n -\u003e NumberSmallGroups(2^n)); # _Muniru A Asiru_, Oct 15 2017"
			],
			"xref": [
				"Cf. A000001, A046058."
			],
			"keyword": "nonn,hard,more,nice",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"a(9) and a(10) found by Eamonn O'Brien"
			],
			"references": 22,
			"revision": 67,
			"time": "2019-01-03T07:59:37-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}