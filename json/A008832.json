{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008832",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8832,
			"data": "0,1,13,2,16,14,6,3,8,17,12,15,5,7,11,4,10,9",
			"name": "Discrete logarithm of n to the base 2 modulo 19.",
			"comment": [
				"Equivalently, a(n) is the multiplicative order of n with respect to base 2 (modulo 19), i.e., a(n) is the base-2 logarithm of the smallest k such that 2^k mod 19 = n. - _Jon E. Schoenfield_, Aug 13 2021"
			],
			"reference": [
				"Carl Friedrich Gauss, \"Disquisitiones Arithmeticae\", Yale University Press, 1965; see p. 37.",
				"I. M. Vinogradov, Elements of Number Theory, p. 221."
			],
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"https://mathworld.wolfram.com/DiscreteLogarithm.html\"\u003eDiscrete Logarithm\u003c/a\u003e."
			],
			"formula": [
				"2^a(n) == n (mod 19). - _Michael S. Branicky_, Aug 13 2021"
			],
			"example": [
				"From _Jon E. Schoenfield_, Aug 13 2021: (Start)",
				"Sequence is a permutation of the 18 integers 0..17:",
				"   k     2^k  2^k mod 19",
				"  --  ------  ----------",
				"   0       1           1  so a(1)  =  0",
				"   1       2           2  so a(2)  =  1",
				"   2       4           4  so a(4)  =  2",
				"   3       8           8  so a(8)  =  3",
				"   4      16          16  so a(16) =  4",
				"   5      32          13  so a(13) =  5",
				"   6      64           7  so a(7)  =  6",
				"   7     128          14  so a(14) =  7",
				"   8     256           9  so a(9)  =  8",
				"   9     512          18  so a(18) =  9",
				"  10    1024          17  so a(17) = 10",
				"  11    2048          15  so a(15) = 11",
				"  12    4096          11  so a(11) = 12",
				"  13    8192           3  so a(3)  = 13",
				"  14   16384           6  so a(6)  = 14",
				"  15   32768          12  so a(12) = 15",
				"  16   65536           5  so a(5)  = 16",
				"  17  131072          10  so a(10) = 17",
				"  18  262144           1",
				"but a(1) = 0, so the sequence is finite with 18 terms.",
				"(End)"
			],
			"maple": [
				"[ seq(mlog(n,2,19), n=1..18) ];"
			],
			"mathematica": [
				"a[1]=0; a[n_]:=MultiplicativeOrder[2, 19, {n}]; Array[a, 18] (* _Vincenzo Librandi_, Mar 21 2020 *)"
			],
			"program": [
				"(PARI) a(n) = znlog(n, Mod(2, 19)); \\\\ _Kevin Ryde_, Aug 13 2021",
				"(Python)",
				"from sympy.ntheory import discrete_log",
				"def a(n): return discrete_log(19, n, 2)",
				"print([a(n) for n in range(1, 19)]) # _Michael S. Branicky_, Aug 13 2021"
			],
			"xref": [
				"Cf. A036120."
			],
			"keyword": "nonn,base,fini,full",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Offset corrected by _Jon E. Schoenfield_, Aug 12 2021"
			],
			"references": 0,
			"revision": 31,
			"time": "2021-08-14T16:03:27-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}