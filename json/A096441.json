{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A096441",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 96441,
			"data": "1,2,2,4,3,7,5,11,8,17,12,26,18,37,27,54,38,76,54,106,76,145,104,199,142,266,192,357,256,472,340,621,448,809,585,1053,760,1354,982,1740,1260,2218,1610,2818,2048,3559,2590,4485,3264,5616,4097,7018,5120,8728,6378",
			"name": "Number of palindromic and unimodal compositions of n. Equivalently, the number of orbits under conjugation of even nilpotent n X n matrices.",
			"comment": [
				"Number of partitions of n such that all differences between successive parts are even, see example. [_Joerg Arndt_, Dec 27 2012]",
				"Number of partitions of n where either all parts are odd or all parts are even. - _Omar E. Pol_, Aug 16 2013",
				"From _Gus Wiseman_, Jan 13 2022: (Start)",
				"Also the number of integer partitions of n with all even multiplicities (or run-lengths) except possibly the first. These are the conjugates of the partitions described by Joerg Arndt above. For example, the a(1) = 1 through a(8) = 11 partitions are:",
				"  (1)  (2)   (3)    (4)     (5)      (6)       (7)        (8)",
				"       (11)  (111)  (22)    (311)    (33)      (322)      (44)",
				"                    (211)   (11111)  (222)     (511)      (422)",
				"                    (1111)           (411)     (31111)    (611)",
				"                                     (2211)    (1111111)  (2222)",
				"                                     (21111)              (3311)",
				"                                     (111111)             (22211)",
				"                                                          (41111)",
				"                                                          (221111)",
				"                                                          (2111111)",
				"                                                          (11111111)",
				"(End)"
			],
			"reference": [
				"A. G. Elashvili and V. G. Kac, Classification of good gradings of simple Lie algebras. Lie groups and invariant theory, 85-104, Amer. Math. Soc. Transl. Ser. 2, 213, Amer. Math. Soc., Providence, RI, 2005."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A096441/b096441.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Karin Baur and Nolan Wallach, \u003ca href=\"http://dx.doi.org/10.1090/S1088-4165-05-00262-1\"\u003eNice parabolic subalgebras of reductive Lie algebras\u003c/a\u003e, Represent. Theory 9 (2005), 1-29.",
				"A. G. Elashvili and V. G. Kac, \u003ca href=\"http://arxiv.org/abs/math-ph/0312030\"\u003eClassification of good gradings of simple Lie algebras\u003c/a\u003e, arXiv:math-ph/0312030, 2002-2004."
			],
			"formula": [
				"G.f.: sum(j\u003e=1, q^j * (1-q^j)/prod(i=1..j, 1-q^(2*i) ) ).",
				"G.f.: F + G - 2, where F = prod(j\u003e=1, 1/(1-q^(2*j) ), G = prod(j\u003e=0, 1/(1-q^(2*j+1)) ).",
				"a(2*n) = A000041(n) + A000009(2*n); a(2*n-1) = A000009(2*n-1). - _Vladeta Jovovic_, Aug 11 2004",
				"a(n) = A000009(n) + A035363(n) = A000041(n) - A006477(n). - _Omar E. Pol_, Aug 16 2013"
			],
			"example": [
				"From _Joerg Arndt_, Dec 27 2012: (Start)",
				"There are a(10)=17 partitions of 10 where all differences between successive parts are even:",
				"[ 1]  [ 1 1 1 1 1 1 1 1 1 1 ]",
				"[ 2]  [ 2 2 2 2 2 ]",
				"[ 3]  [ 3 1 1 1 1 1 1 1 ]",
				"[ 4]  [ 3 3 1 1 1 1 ]",
				"[ 5]  [ 3 3 3 1 ]",
				"[ 6]  [ 4 2 2 2 ]",
				"[ 7]  [ 4 4 2 ]",
				"[ 8]  [ 5 1 1 1 1 1 ]",
				"[ 9]  [ 5 3 1 1 ]",
				"[10]  [ 5 5 ]",
				"[11]  [ 6 2 2 ]",
				"[12]  [ 6 4 ]",
				"[13]  [ 7 1 1 1 ]",
				"[14]  [ 7 3 ]",
				"[15]  [ 8 2 ]",
				"[16]  [ 9 1 ]",
				"[17]  [ 10 ]",
				"(End)"
			],
			"maple": [
				"b:= proc(n, i) option remember; `if`(i\u003en, 0,",
				"      `if`(irem(n, i)=0, 1, 0) +add(`if`(irem(j, 2)=0,",
				"       b(n-i*j, i+1), 0), j=0..n/i))",
				"    end:",
				"a:= n-\u003e b(n, 1):",
				"seq(a(n), n=1..60);  # _Alois P. Heinz_, Mar 26 2014"
			],
			"mathematica": [
				"(* The following Mathematica program first generates all of the palindromic, unimodal compositions of n and then counts them. *)",
				"Pal[n_] := Block[{i, j, k, m, Q, L}, If[n == 1, Return[{{1}}]]; If[n == 2, Return[{{1, 1}, {2}}]]; L = {{n}}; If[Mod[n, 2] == 0, L = Append[L, {n/2, n/2}]]; For[i = 1, i \u003c n, i++, Q = Pal[n - 2i]; m = Length[Q]; For[j = 1, j \u003c= m, j++, If[i \u003c= Q[[j, 1]], L = Append[L, Append[Prepend[Q[[j]], i], i]]]]]; L] NoPal[n_] := Length[Pal[n]]",
				"a[n_] := PartitionsQ[n] + If[EvenQ[n], PartitionsP[n/2], 0]; Table[a[n], {n, 1, 55}] (* _Jean-François Alcover_, Mar 17 2014, after _Vladeta Jovovic_ *)",
				"Table[Length[Select[IntegerPartitions[n],And@@EvenQ/@Rest[Length/@Split[#]]\u0026]],{n,1,30}] (* _Gus Wiseman_, Jan 13 2022 *)"
			],
			"program": [
				"(PARI) x='x+O('x^66); Vec(eta(x^2)/eta(x)+1/eta(x^2)-2) \\\\ _Joerg Arndt_, Jan 17 2016"
			],
			"xref": [
				"Bisections are A078408 and A096967.",
				"The complement in partitions is counted by A006477",
				"A version for compositions is A016116.",
				"A pointed version is A035363, ranked by A066207.",
				"A000041 counts integer partitions.",
				"A025065 counts palindromic partitions.",
				"A027187 counts partitions with even length/maximum.",
				"A035377 counts partitions using multiples of 3.",
				"A058696 counts partitions of even numbers, ranked by A300061.",
				"A340785 counts factorizations into even factors.",
				"Cf. A000009, A002865, A027383, A035457, A117298, A117989, A168021, A274230, A345170, A349060, A349061."
			],
			"keyword": "nonn,changed",
			"offset": "1,2",
			"author": "Nolan R. Wallach (nwallach(AT)ucsd.edu), Aug 10 2004",
			"references": 21,
			"revision": 36,
			"time": "2022-01-16T11:08:51-05:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}