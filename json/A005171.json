{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005171",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5171,
			"data": "1,0,0,1,0,1,0,1,1,1,0,1,0,1,1,1,0,1,0,1,1,1,0,1,1,1,1,1,0,1,0,1,1,1,1,1,0,1,1,1,0,1,0,1,1,1,0,1,1,1,1,1,0,1,1,1,1,1,0,1,0,1,1,1,1,1,0,1,1,1,0,1,0,1,1,1,1,1,0,1,1,1,0,1,1,1,1,1,0,1,1,1,1,1,1,1,0,1,1,1,0,1,0,1,1",
			"name": "Characteristic function of nonprimes: 0 if n is prime, else 1.",
			"comment": [
				"Number of orbits of length n in map whose periodic points are A023890. - _Thomas Ward_",
				"Characteristic function of nonprimes A018252. - _Jonathan Vos Post_, Dec 30 2007",
				"Triangle A157423 = A005171 in every column. A052284 = INVERT transform of A005171, and the eigensequence of triangle A157423. - _Gary W. Adamson_, Feb 28 2009"
			],
			"reference": [
				"Douglas Hofstadter, Fluid Concepts and Creative Analogies: Computer Models of the Fundamental Mechanisms of Thought."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A005171/b005171.txt\"\u003eTable of n, a(n) for n = 1..65537\u003c/a\u003e",
				"R. K. Guy, \u003ca href=\"/A005165/a005165.pdf\"\u003eThe strong law of small numbers\u003c/a\u003e. Amer. Math. Monthly 95 (1988), no. 8, 697-712. [Annotated scanned copy]",
				"Y. Puri and T. Ward, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL4/WARD/short.html\"\u003eArithmetic and growth of periodic orbits\u003c/a\u003e, J. Integer Seqs., Vol. 4 (2001), #01.2.1.",
				"Yash Puri and Thomas Ward, \u003ca href=\"http://www.fq.math.ca/Scanned/39-5/puri.pdf\"\u003eA dynamical property unique to the Lucas sequence\u003c/a\u003e, Fibonacci Quarterly, Volume 39, Number 5 (November 2001), pp. 398-402.",
				"\u003ca href=\"/index/Ch#char_fns\"\u003eIndex entries for characteristic functions\u003c/a\u003e"
			],
			"formula": [
				"If b(n) is the n-th term of A023890, then a(n)=(1/n)* Sum_{ d divides n } mu(d)*a(n/d). E.g., a(6) = 1 since the 6th term of A023890 is 7 and the first term is 1.",
				"a(n) = 1-[(n-1)!^2 mod n], with n\u003e=1. - _Paolo P. Lava_, Jun 11 2007",
				"a(n) = NOT(A010051(n)) = 1 - A010051(n). - _Jonathan Vos Post_, Dec 30 2007",
				"a(n) equals the first column in a table T defined by the recurrence: If n = k then T(n,k) = 1 else if k = 1 then T(n,k) = 1 - Product_{k divides n} of T(n,k), else if k divides n then T(n,k) = T(n/k,1). This is true since T(n,k) = 0 when k divides n and n/k is prime which results in Product_{k divides n} = 0 for the composite numbers and where k ranges from 2 to n. Therefore there is a remaining 1 in the expression 1-Product_{k divides n}, in the first column. Provided below is a Mathematica program as an illustration. - _Mats Granvik_, Sep 21 2013",
				"a(n) = A057427(A239968(n)). - _Reinhard Zumkeller_, Mar 30 2014"
			],
			"maple": [
				"A005171 := proc(n)",
				"    if isprime(n) then",
				"        0 ;",
				"    else",
				"        1 ;",
				"    end if;",
				"end proc: # _R. J. Mathar_, May 26 2017"
			],
			"mathematica": [
				"f[n_] := If[PrimeQ@ n, 0, 1]; Array[f, 105] (* _Robert G. Wilson v_, Jun 20 2011 *)",
				"nn = 105; t[n_, k_] :=  t[n, k] = If[n == k, 1, If[k == 1, 1 - Product[t[n, k + i], {i, 1, n - 1}], If[Mod[n, k] == 0, t[n/k, 1], 1], 1]]; Table[t[n, 1], {n, 1, nn}] (* _Mats Granvik_, Sep 21 2013 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c1, 0, !isprime(n)) /* _Michael Somos_, Jun 08 2005 */",
				"(Haskell)",
				"a005171 = (1 -) . a010051  -- _Reinhard Zumkeller_, Mar 30 2014",
				"(Python)",
				"from sympy import isprime",
				"def a(n): return int(not isprime(n))",
				"print([a(n) for n in range(1, 106)]) # _Michael S. Branicky_, Oct 28 2021"
			],
			"xref": [
				"Cf. A010051, A018252, A023890.",
				"Cf. A157423, A157424, A052284, A050374."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_Russ Cox_",
			"references": 70,
			"revision": 77,
			"time": "2021-11-01T12:21:29-04:00",
			"created": "1991-05-16T03:00:00-04:00"
		}
	]
}