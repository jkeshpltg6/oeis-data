{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A334539",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 334539,
			"data": "1,3,8,11,25,20,40,9,45,41,158,200,14,185,636,589,595,432,773,3196,1249,50,7703,7661,12954,25629,14885,41189,23200,87410,33969,63225,20486,212825,58621,152952,135263,2743830,729008,384150,908629,126746,4543899,3448777,8531396",
			"name": "The eventual period of a sequence b(n, m) where b(n, 1) = 1 and the m-th term is the number of occurrences of b(n, m-1) in the list of integers from b(n, max(m-n, 1)) to b(n, m-1).",
			"comment": [
				"To generate the sequence b(n, m) for some n, start with the value 1 and then repeatedly append the number of times the last element of the sequence appears in the previous n terms. b(n, m) eventually becomes periodic for all n.",
				"By the pigeonhole principle, a(n) has an upper bound of n^n.",
				"The growth of a(n) appears to be roughly exponential."
			],
			"link": [
				"Elad Michael, \u003ca href=\"/A334539/b334539.txt\"\u003eTable of n, a(n) for n = 1..100\u003c/a\u003e (terms 1..76 from Johan Westin)",
				"Reddit user supermac30, \u003ca href=\"https://redd.it/gdsjth\"\u003eFoggy Sequences\u003c/a\u003e"
			],
			"example": [
				"The sequence b(3, m) is 1, 1, 2, 1, 2, 2, 2, 3, 1, 1, 2, ... the period of which is 8.",
				"The sequence b(4, m) is 1, 1, 2, 1, 3, 1, 2, 1, 2, 2, 3, 1, 1, 2, ... the period of which is 11.",
				"The sequence b(5, m) is 1, 1, 2, 1, 3, 1, 3, 2, 1, 2, 2, 3, 1, 2, 3, 2, 2, 3, 2, 3, 2, 3, 3, 3, 4, 1, 1, 2, ... the period of which is 25."
			],
			"mathematica": [
				"a[k_] := Block[{b = Append[0 Range@k, 1], A=\u003c||\u003e, n=0}, While[True, n++; b = Rest@ b; AppendTo[b, Count[b, b[[-1]]]]; If[ KeyExistsQ[A, b], Break[]]; A[b] = n]; n - A[b]]; Array[a, 30] (* _Giovanni Resta_, May 06 2020 *)"
			],
			"program": [
				"(Python)",
				"import sympy",
				"def A334539(n):",
				"  return next(sympy.cycle_length(lambda x:x[1:]+(x.count(x[-1]),),(0,)*(n-1)+(1,)))[0] # _Pontus von Brömssen_, May 05 2021"
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Mark Bedaywi_, May 05 2020",
			"ext": [
				"More terms from _Giovanni Resta_, May 06 2020"
			],
			"references": 2,
			"revision": 52,
			"time": "2021-05-05T17:25:43-04:00",
			"created": "2020-05-10T08:48:53-04:00"
		}
	]
}