{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A293882",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 293882,
			"data": "0,4,1,1,3,1,22,1,3,3,1,1,1,13,10,3,4,2,7,1,4,6,2,4,1,1,6,2,1,2,1,1,2,3,42,3,6,3,2,1,1,1,2,2,8,2,4,1,2,3,1,1,1,2,5,8,3,1,1,3,2,3,2,11,1,3,6,6,1,1,3,1,1,103,2,2,2,3,2,44,2,1,1,2,1,5,1,9,1,1,5,1,1,7,1,2,2,1,1,1,5,1,1,1,4,45",
			"name": "Continued fraction expansion of the minimum ripple factor for a reflectionless, Chebyshev filter, in the limit where the order approaches infinity.",
			"comment": [
				"This is the smallest ripple factor (a constant) for which the prototype elements of the generalized reflectionless filter topology (see Morgan, 2017) needs no negative elements, where the order of the filter approaches infinity. It is also the ripple factor for which the first two and last two Chebyshev prototype parameters (of the canonical ladder, or Cauer, topology) are equal.",
				"Other related sequences in the OEIS are the decimal and continued fraction expansions of the limiting ripple factors for third, fifth, seventh, and ninth order, as well as for the limiting case where the order diverges to infinity. As these ripple factors do approach a common limit very quickly, the sequences for the fifth- and higher-order constants share the same initial terms, to greater length as the order increases.",
				"There are simple radical expressions for the third- and fifth-order constants (see formulas). Further, the third-order constant is a quadratic irrational, thus having a repeating continued fraction expansion. I do not know if such simple expressions or patterns exist for the higher-order constants or the limiting (infinite-order) constant."
			],
			"reference": [
				"M. Morgan, Reflectionless Filters, Norwood, MA: Artech House, pp. 129-132, January 2017."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A293882/b293882.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"1/(4 + 1/(1 + 1/(1 + 1/(3 + 1/(1 + 1/(22 + 1/(1 + 1/(3 + 1/(3+..."
			],
			"mathematica": [
				"ContinuedFraction[Sqrt[Exp[4 ArcTanh[Exp[-(Pi Sqrt[2])]]] - 1],130]"
			],
			"program": [
				"(MAGMA) R:= RealField();ContinuedFraction(Sqrt(Exp( 4*Argtanh(Exp (-(Pi(R)*Sqrt(2))))) - 1)); \\\\ _Michel Marcus_, Feb 17 2018",
				"(PARI) contfrac(sqrt(exp(4*atanh(exp(-Pi*sqrt(2)))) - 1)) \\\\ _Michel Marcus_, Feb 17 2018"
			],
			"xref": [
				"Decimal expansions (A020784, A293409, A293415, A293416, A293417) and continued fractions (A040021, A293768, A293769, A293770, A293882) for third-, fifth-, seventh-, ninth-order and the limiting \"infinite-order\" constant, respectively."
			],
			"keyword": "cofr,easy,nonn",
			"offset": "1,2",
			"author": "_Matthew A. Morgan_, Oct 18 2017",
			"references": 8,
			"revision": 17,
			"time": "2019-02-22T01:53:44-05:00",
			"created": "2017-10-20T15:44:12-04:00"
		}
	]
}