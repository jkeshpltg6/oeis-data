{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A294185",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 294185,
			"data": "0,0,1,2,2,1,2,3,2,2,4,3,1,3,2,2,5,3,0,4,3,2,5,5,1,4,3,1,5,3,2,6,3,0,6,5,2,6,6,0,6,5,1,6,5,1,4,3,0,7,5,2,5,6,2,9,7,1,8,6,0,6,4,0,8,5,1,3,7,2,9,7,0,7,5,2,9,6,0,9,5,0,7,11,1,6,6,1",
			"name": "Number of distinct lesser twin primes which are in Goldbach partitions of 2n.",
			"comment": [
				"Tomas Oliveira e Silva in 2012 experimentally confirmed that all even numbers \u003c= 4*10^18 have at least one Goldbach partition (GP) with a prime 9781 or less. Detailed examination of all even numbers \u003c 10^6 showed that the most popular prime in all GPs is 3 (78497 occurrences), then 5 (70328), then 7 (62185), then 11 (48582), then 13 (40916), then 17 (31091), then 19 (29791) -- all these primes are twin primes. These results gave rise to a hypothesis that twin primes should be rather frequent in GP, especially those relatively small.",
				"Further empirical experiments demonstrated, surprisingly, there are in general two categories of even numbers n: category 1 - with 0, 1, or 2 distinct lesser twin primes in all possible GPs(n), and category 2 - with fast increasing number of distinct lesser twin primes in GPs(n).",
				"First occurrence of k, k=0,1,2...: 1, 3, 4, 8, 11, 17, 32, 50, 59, 56, 98, 84, 105, 104, ..., . - _Robert G. Wilson v_, Jul 24 2018",
				"Records: 0, 1, 2, 3, 4, 5, 6, 7, 9, 11, 13, 14, 15, 17, 20, 22, 25, 28, 32, 33, 36, 37, 43, ..., . - _Robert G. Wilson v_, Jul 24 2018"
			],
			"link": [
				"Robert G. Wilson v, \u003ca href=\"/A294185/b294185.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Marcin Barylski, \u003ca href=\"/A294185/a294185.png\"\u003ePlot of first 20000 elements of the A294185\u003c/a\u003e",
				"Marcin Barylski, \u003ca href=\"/A294185/a294185_1.cpp.txt\"\u003eC++ program for generating A294185\u003c/a\u003e",
				"Tomas Oliveira e Silva, \u003ca href=\"http://sweet.ua.pt/tos/goldbach.html\"\u003eGoldbach conjecture verification\u003c/a\u003e"
			],
			"example": [
				"a(5) = 2 because 2*5=10 has two ordered Goldbach partitions: 3+7 and 5+5. 3 is a lesser twin prime (because 3 and 5 are twin primes), 5 is a lesser twin prime (because 5 and 7 are twin primes)."
			],
			"mathematica": [
				"a[n_] := Block[{c = 0, p = 3, lst = {}}, While[p \u003c n + 1, If[ PrimeQ[2n - p], AppendTo[lst, {p, 2n - p}]]; p = NextPrime@p]; Length@Select[Union@ Flatten@ lst, PrimeQ[# + 2] \u0026]]; Array[a, 88] (* _Robert G. Wilson v_, Jul 24 2018 *)"
			],
			"program": [
				"(C++) See Barylski link.",
				"(PARI) isltwin(p) = isprime(p) \u0026\u0026 isprime(p+2);",
				"a(n) = {vtp = []; forprime(p = 2, n, if (isprime(2*n-p), if (isltwin(p), vtp = concat(vtp, p)); if (isltwin(2*n-p), vtp = concat(vtp, 2*n-p)););); #Set(vtp);} \\\\ _Michel Marcus_, Mar 01 2018"
			],
			"xref": [
				"Cf. A002372 (number of ordered Goldbach partitions), A001359 (lesser of twin primes), A294186, A295424."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Marcin Barylski_, Feb 11 2018",
			"references": 3,
			"revision": 54,
			"time": "2018-08-06T05:31:24-04:00",
			"created": "2018-03-25T14:02:39-04:00"
		}
	]
}