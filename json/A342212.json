{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A342212",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 342212,
			"data": "1,1,3,6,10,15,21,38,64",
			"name": "Largest number of maximal bipartite node-induced subgraphs of an n-node graph.",
			"comment": [
				"Byskov, Madsen, and Skjernaa (2005) construct a 10-node graph with 105 maximal bipartite subgraphs, so a(10) \u003e= 105. By taking disjoint copies of this graph, it follows that a(10*n) \u003e= 105^n."
			],
			"link": [
				"Jesper Makholm Byskov, Bolette Ammitzbøll Madsen, and Bjarke Skjernaa, \u003ca href=\"https://doi.org/10.1002/jgt.20041\"\u003eOn the number of maximal bipartite subgraphs of a graph\u003c/a\u003e, Journal of Graph Theory 48 (2005), 127-132."
			],
			"formula": [
				"a(m+n) \u003e= a(m)*a(n).",
				"a(n) \u003c= n*12^(n/4). (Byskov, Madsen, and Skjernaa (2005))",
				"1.5926... = 105^(1/10) \u003c= liminf a(n)^(1/n) \u003c= limsup a(n)^(1/n) \u003c= 12^(1/4) = 1.8612... . (Byskov, Madsen, and Skjernaa (2005))"
			],
			"example": [
				"The following list shows all optimal graphs (i.e., graphs having n nodes and a(n) maximal bipartite subgraphs) for 1 \u003c= n \u003c= 9. Here, CK(n_1, ..., n_k) is the graph obtained by arranging complete graphs of orders n_1, ..., n_k (in that order) in a cycle, and joining all nodes in neighboring parts with edges. (The graph in the paper by Byskov, Madsen, and Skjernaa, which shows that a(10) \u003e= 105, is CK(2, 2, 2, 2, 2).)",
				"        n = 1: the 1-node graph;",
				"        n = 2: the complete graph and the empty graph;",
				"  3 \u003c= n \u003c= 6: the complete graph;",
				"        n = 7: CK(1, 1, 2, 1, 2) (the Moser spindle) and the complete graph;",
				"        n = 8: CK(1, 2, 1, 2, 2) and the 4-antiprism graph;",
				"        n = 9: CK(1, 2, 2, 1, 3)."
			],
			"xref": [
				"Cf. A342211, A342213, A342324."
			],
			"keyword": "nonn,more",
			"offset": "1,3",
			"author": "_Pontus von Brömssen_, Mar 05 2021",
			"references": 3,
			"revision": 11,
			"time": "2021-03-11T21:12:16-05:00",
			"created": "2021-03-11T21:12:16-05:00"
		}
	]
}