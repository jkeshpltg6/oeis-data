{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A284343",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 284343,
			"data": "1,1,2,2,1,1,4,1,1,3,1,3,2,1,3,3,2,3,5,2,3,4,6,1,3,5,1,6,1,3,7,2,2,5,6,5,6,3,6,4,1,3,4,5,4,5,7,2,3,8,6,7,3,4,8,3,2,6,3,5,7,3,8,7,2,4,10,4,4,7,9,7,2,4,2,7,3,5,11,2,4",
			"name": "Number of ways to write n as x^2 + y^2 + z^2 + w^2 with x,y,z,w nonnegative integers and y \u003c= z such that 2*x + y - z is either zero or a power of 8 (including 8^0 = 1).",
			"comment": [
				"Conjecture: (i) For any c = 1,2,4, each n = 0,1,2,... can be written as x^2 + y^2 + z^2 + w^2 with x,y,z,w nonnegative integers and y \u003c= z such that c*(2*x+y-z) is either zero or a power of eight (including 8^0 = 1).",
				"(ii) Each n = 0,1,2,... can be written as x^2 + y^2 + z^2 + w^2 with x,y,z,w nonnegative integers such that P(x,y,z,w) is either zero or a power of four (including 4^0 = 1), whenever P(x,y,z,w) is among the polynomials 2*x-y, x+y-z, x-y-z, x+y-2*z, 2*x+y-z, 2*x-y-z, 2*x-2*y-z, x+2*y-3*z, 2*x+2*y-2*z, 2*x+2*y-4*z, 3*x-2*y-z, x+3*y-3*z, 2*x+3*y-3*z, 4*x+2*y-2*z, 8*x+2*y-2*z, 2*(x-y)+z-w, 4*(x-y)+2*(z-w).",
				"Part (i) of the conjecture is stronger than the first part of Conjecture 4.4 in the linked JNT paper (see also A273432).",
				"Modifying the proofs of Theorem 1.1 and Theorem 1.2(i) in the linked JNT paper slightly, we see that for any a = 1,4 and m = 4,5,6 we can write each n = 0,1,2,... as a*x^m + y^2 + z^2 + w^2 with x,y,z,w nonnegative integers such that x is either zero or a power of two (including 2^0 = 1), and that for any b = 1,2 each n = 0,1,2,... can be written as x^2 + y^2 + z^2 + w^2 with x,y,z,w nonnegative integers such that b*(x-y) is either zero or a power of 4 (including 4^0 = 1)."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A284343/b284343.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2016.11.008\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, J. Number Theory 175(2017), 167-190.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1701.05868\"\u003eRestricted sums of four squares\u003c/a\u003e, arXiv:1701.05868 [math.NT], 2017."
			],
			"example": [
				"a(4) = 1 since 4 = 0^2 + 0^2 + 0^2 + 2^2 with 0 = 0 and 2*0 + 0 - 0 = 0.",
				"a(5) = 1 since 5 = 1^2 + 0^2 + 2^2 + 0^2 with 0 \u003c 2 and 2*1 + 0 - 2 = 0.",
				"a(7) = 1 since 7 = 1^2 + 1^2 + 2^2 + 1^2 with 1 \u003c 2 and 2*1 + 1 - 2 = 8^0.",
				"a(40) = 1 since 40 = 4^2 + 2^2 + 2^2 + 4^2 with 2 = 2 and 2*4 + 2 - 2 = 8.",
				"a(138) = 1 since 138 = 3^2 + 5^2 + 10^2 + 2^2 with 5 \u003c 10 and 2*3 + 5 - 10 = 8^0.",
				"a(1832) = 1 since 1832 = 4^2 + 30^2 + 30^2 + 4^2 with 30 = 30 and 2*4 + 30 - 30 = 8.",
				"a(2976) = 1 since 2976 = 20^2 + 16^2 + 48^2 + 4^2 with 16 \u003c 48 and 2*20 + 16 - 48 = 8."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]];",
				"Pow[n_]:=Pow[n]=n==0||(n\u003e0\u0026\u0026IntegerQ[Log[8,n]]);",
				"Do[r=0;Do[If[SQ[n-x^2-y^2-z^2]\u0026\u0026Pow[2x+y-z],r=r+1],{x,0,Sqrt[n]},{y,0,Sqrt[(n-x^2)/2]},{z,y,Sqrt[n-x^2-y^2]}];Print[n,\" \",r],{n,0,80}]"
			],
			"xref": [
				"Cf. A000079, A000118, A000290, A000302, A000578, A001018, A271518, A273432, A279612."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Zhi-Wei Sun_, Mar 25 2017",
			"references": 1,
			"revision": 14,
			"time": "2017-03-25T14:24:37-04:00",
			"created": "2017-03-25T14:24:37-04:00"
		}
	]
}