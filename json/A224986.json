{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A224986",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 224986,
			"data": "1,1,1,1,2,96,8153726976,320352637207127391364950814323398779319161580421120",
			"name": "a(n) = Product_{k=1..n-4} (n-k-2)!^(k*k!).",
			"comment": [
				"Consider words on n symbols that contain every permutation of those n symbols as contiguous substrings. The minimal length of such a string was conjectured to equal A007489(n) (see A180632). This sequence is a lower bound on the number of distinct (up to relabeling the symbols) such strings of the conjectured minimal length.",
				"It was conjectured in the Ashlock paper that, for all n, there is only one string of length A007489(n) containing all permutations. This sequence shows that this conjecture fails as n grows.",
				"In 2014 Houston has shown that the first conjecture about the minimal length is also false for all n \u003e 5. In particular, A180632(6) \u003c= 872 = A007489(n). - _M. F. Hasler_, Jul 28 2020",
				"The next term a(9) ~ 2.18e291 is too large to be displayed here. - _M. F. Hasler_, Jul 29 2020"
			],
			"reference": [
				"D. Ashlock and J. Tillotson, Construction of small superpermutations and minimal injective superstrings. Congressus Numerantium, 93 (1993), 91-98."
			],
			"link": [
				"Robin Houston, \u003ca href=\"http://arxiv.org/abs/1408.5108\"\u003eTackling the Minimal Superpermutation Problem\u003c/a\u003e, arXiv:1408.5108 [math.CO], 2014.",
				"Nathaniel Johnston, \u003ca href=\"http://arxiv.org/abs/1303.4150\"\u003eNon-uniqueness of minimal superpermutations\u003c/a\u003e, arXiv:1303.4150 [math.CO], 2013; Discrete Math., 313 (2013), 1553-1557.",
				"Nathaniel Johnston, \u003ca href=\"http://www.njohnston.ca/2013/04/the-minimal-superpermutation-problem/\"\u003eThe Minimal Superpermutation Problem\u003c/a\u003e, 2013."
			],
			"example": [
				"a(n) = 1 for n \u003c= 4, which agrees with the fact that the minimal strings containing all permutations in these cases are unique (see A180632)."
			],
			"maple": [
				"seq(product((n-k-2)!^(k*k!),k=1..max(n-4,0)),n=1..8);"
			],
			"program": [
				"(PARI) apply( {A224986(n)=prod(k=1,n-4,(n-k-2)!^(k*k!))}, [1..8]) \\\\ _M. F. Hasler_, Jul 29 2020"
			],
			"xref": [
				"Cf. A180632, A188428."
			],
			"keyword": "nonn,easy",
			"offset": "1,5",
			"author": "_Nathaniel Johnston_, Apr 22 2013",
			"references": 4,
			"revision": 15,
			"time": "2020-08-06T22:11:49-04:00",
			"created": "2013-04-22T12:29:08-04:00"
		}
	]
}