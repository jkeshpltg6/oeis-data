{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A082175",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 82175,
			"data": "2,2,4,2,6,4,4,4,8,2,6,8,6,8,10,4,6,8,12,2,8,12,8,6,12,8,8,6,18,12,4,8,16,8,12,14,8,4,16,18,6,8,20,8,14,16,14,12,6,12,16,4,14,20,16,8,20,14,8,8,28,20,10,4,22,16,10",
			"name": "Number of reduced indefinite quadratic forms over the integers in two variables with discriminants D(n)=A079896(n).",
			"comment": [
				"An indefinite quadratic form in two variables over the integers, a*x^2 + b*x*y + c*y^2 with discriminant D = b^2 - 4*a*c \u003e 0, 0 or 1 (mod 4) and not a square, is called reduced if b\u003e0 and f(D) - min(|2*a|,|2*c|) \u003c= b \u003c f(D), with f(D) := ceiling(sqrt(D)). See the Scholz-Schoeneberg reference for this definitions."
			],
			"reference": [
				"A. Scholz and B. Schoeneberg, Einführung in die Zahlentheorie, 5. Aufl., de Gruyter, Berlin, New York, 1973, ch.IV, par.31, p. 112."
			],
			"formula": [
				"a(n)= number of reduced indefinite quadratic forms over the integers for D(n)=A079896(n) (counting also nonprimitive forms)."
			],
			"example": [
				"a(0)=2 because there are two reduced forms for D(0)=A079896(0)=5, namely [a,b,c]=[-1, 1, 1] and [1, 1, -1]; here f(5)=3.",
				"a(4)=6: for D(4)=A079896(4)=17 (f(17)=5) the 6 reduced [a,b,c] forms are [[-2, 1, 2], [2, 1, -2], [-2, 3, 1], [-1, 3, 2], [1, 3, -2], [2, 3, -1]]. They are all primitive (that is a,b and c are relatively prime).",
				"a(5)=4: for D(5)=A079896(5)=20 (f(20)=5) there are four reduced forms: [-2, 2, 2], [2, 2, -2], [-1, 4, 1] and [1, 4, -1], Here two of them are nonprimitive, namely [-2, 2, 2], [2, 2, -2].",
				"a(10)=6, D(10)=A079896(10)=32 (f(32)=6); the 6 reduced forms are [-4, 4, 1], [-2, 4, 2], [-1, 4, 4], [1, 4, -4], [2, 4, -2] and [4, 4, -1]. Two of them are nonprimitive, namely [ -2, 4, 2] and [2, 4, -2]. Therefore A082174(10)=4."
			],
			"xref": [
				"Cf. A082174 (number of primitive reduced forms)."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_Wolfdieter Lang_, Apr 11 2003",
			"references": 3,
			"revision": 12,
			"time": "2021-01-18T05:13:51-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}