{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A133457",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 133457,
			"data": "0,1,0,1,2,0,2,1,2,0,1,2,3,0,3,1,3,0,1,3,2,3,0,2,3,1,2,3,0,1,2,3,4,0,4,1,4,0,1,4,2,4,0,2,4,1,2,4,0,1,2,4,3,4,0,3,4,1,3,4,0,1,3,4,2,3,4,0,2,3,4,1,2,3,4,0,1,2,3,4,5,0,5,1,5,0,1,5,2,5,0,2,5,1,2,5,0,1,2,5,3,5,0,3,5",
			"name": "Irregular triangle read by rows: row n gives exponents in expression for n as a sum of powers of 2.",
			"comment": [
				"This sequence contains every increasing finite sequence. For example, the finite sequence {0,2,3,5} arises from n = 45.",
				"Essentially A030308(n,k)*k, then entries removed where A030308(n,k)=0. - _R. J. Mathar_, Nov 30 2007",
				"In the corresponding irregular triangle {a(n)+1}, the m-th row gives all positive integer roots m_i of polynomial {m,k}. - see link [Shevelev]; see also A264613. - _Vladimir Shevelev_, Dec 13 2015"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A133457/b133457.txt\"\u003eRows n = 1..1024 of triangle, flattened\u003c/a\u003e",
				"Vladimir Shevelev, \u003ca href=\"http://www.emis.de/journals/INTEGERS/papers/m1/m1.Abstract.html\"\u003eThe number of permutations with prescribed up-down structure as a function of two variables\u003c/a\u003e, INTEGERS, 12 (2012), #A1. (See Section 3, Theorem 21 and Section 8, Theorem 50)"
			],
			"formula": [
				"a(n) = A048793(n) - 1."
			],
			"example": [
				"1 = 2^0.",
				"2 = 2^1.",
				"3 = 2^0 + 2^1.",
				"4 = 2^2.",
				"5 = 2^0 + 2^2.",
				"etc. and reading the exponents gives the rows of the triangle."
			],
			"maple": [
				"A133457 := proc(n) local a,bdigs,i ; a := [] ; bdigs := convert(n,base,2) ; for i from 1 to nops(bdigs) do if op(i,bdigs) \u003c\u003e 0 then a := [op(a),i-1] ; fi ; od: a ; end: seq(op(A133457(n)),n=1..80) ; # _R. J. Mathar_, Nov 30 2007"
			],
			"mathematica": [
				"Array[Join @@ Position[#, 1] - 1 \u0026@ Reverse@ IntegerDigits[#, 2] \u0026, 41] // Flatten (* _Michael De Vlieger_, Oct 08 2017 *)"
			],
			"program": [
				"(Haskell)",
				"a133457 n k = a133457_tabf !! (n-1) !! n",
				"a133457_row n = a133457_tabf !! (n-1)",
				"a133457_tabf = map (fst . unzip . filter ((\u003e 0) . snd) . zip [0..]) $",
				"                   tail a030308_tabf",
				"-- _Reinhard Zumkeller_, Oct 28 2013, Feb 06 2013"
			],
			"xref": [
				"Cf. A003071, A030308, A048793, A067255, A264613.",
				"Cf. A073642 (row sums), A272011 (rows reversed)."
			],
			"keyword": "base,tabf,easy,nonn,look",
			"offset": "1,5",
			"author": "_Masahiko Shin_, Nov 27 2007",
			"ext": [
				"More terms from _R. J. Mathar_, Nov 30 2007"
			],
			"references": 22,
			"revision": 39,
			"time": "2021-12-22T02:20:12-05:00",
			"created": "2007-12-09T03:00:00-05:00"
		}
	]
}