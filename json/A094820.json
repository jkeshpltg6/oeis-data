{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A094820",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 94820,
			"data": "1,2,3,5,6,8,9,11,13,15,16,19,20,22,24,27,28,31,32,35,37,39,40,44,46,48,50,53,54,58,59,62,64,66,68,73,74,76,78,82,83,87,88,91,94,96,97,102,104,107,109,112,113,117,119,123,125,127,128,134,135,137,140,144,146,150",
			"name": "Partial sums of A038548.",
			"comment": [
				"a(n) = number of pairs (c,d) of integers such that 0 \u003c c \u003c= d and c*d \u003c= n. - _Clark Kimberling_, Jun 18 2011",
				"Equivalently, the number of representations of n in the form x + y*z, where x, y, and z are positive integers and y \u003c= z. - _John W. Layman_, Feb 21 2012"
			],
			"link": [
				"Peter Kagey, \u003ca href=\"/A094820/b094820.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Vaclav Kotesovec, \u003ca href=\"/A094820/a094820.jpg\"\u003eGraph - the asymptotic ratio (100000 terms)\u003c/a\u003e"
			],
			"formula": [
				"G.f.: (1/(1 - x))*Sum_{k\u003e=1} x^(k^2)/(1 - x^k). - _Ilya Gutkovskiy_, Apr 13 2017",
				"a(n) ~ (log(n) + 2*gamma - 1)*n/2 + sqrt(n)/2, where gamma is the Euler-Mascheroni constant A001620. - _Vaclav Kotesovec_, Aug 19 2019"
			],
			"maple": [
				"ListTools:-PartialSums([seq(ceil(numtheory:-tau(n)/2), n=1..100)]); # _Robert Israel_, Feb 24 2016"
			],
			"mathematica": [
				"f[n_, k_] := Floor[n/k] - Floor[(n - 1)/k]",
				"g[n_, k_] := If[k^2 \u003c= n, f[n, k], 0]",
				"Table[Sum[f[n, k], {k, 1, n}], {n, 1, 100}] (* A000005 *)",
				"t = Table[Sum[g[n, k], {k, 1, n}], {n, 1, 100}]",
				"(* A038548 *)",
				"a[n_] := Sum[t[[i]], {i, 1, n}]",
				"Table[a[n], {n, 1, 100}]  (* A094820 *)",
				"(* from Clark Kimberling, Jun 18 2011 *)",
				"Table[Sum[Boole[d \u003c= Sqrt[n]], {d, Divisors[n]}], {n, 1, 66}] // Accumulate (* _Jean-François Alcover_, Dec 13 2012 *)"
			],
			"program": [
				"(Ruby)",
				"  def a(n)",
				"    (1..Math.sqrt(n)).inject(0) { |accum, i| accum + 1 + (n/i).to_i - i }",
				"  end # _Peter Kagey_, Feb 24 2016",
				"(PARI) a(n) = sum(k=1, n, ceil(numdiv(k)/2)); \\\\ _Michel Marcus_, Feb 24 2016"
			],
			"xref": [
				"Cf. A091626, A038548."
			],
			"keyword": "easy,nonn",
			"offset": "1,2",
			"author": "_Vladeta Jovovic_, Jun 12 2004",
			"references": 16,
			"revision": 52,
			"time": "2019-08-19T05:09:37-04:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}