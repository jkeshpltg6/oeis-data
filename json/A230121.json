{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A230121",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 230121,
			"data": "0,0,1,0,0,1,0,1,2,1,1,0,2,1,2,1,2,3,2,2,6,1,3,5,1,2,3,5,2,1,3,3,3,4,3,8,2,5,11,2,5,8,4,6,4,9,4,6,5,4,6,3,8,8,5,8,10,7,7,11,8,6,7,8,5,9,7,6,8,7,7,8,13,9,11,10,7,22,9,10,13,3,6,10,8,17,12,7,9,10,16,6,18,18,10,15,9,12,20,5",
			"name": "Number of ways to write n = x + y + z (0 \u003c x \u003c= y \u003c= z) such that x*(x+1)/2 + y*(y+1)/2 + z*(z+1)/2 is a triangular number.",
			"comment": [
				"Conjecture: (i) a(n) \u003e 0 except for n = 1, 2, 4, 5, 7, 12. Moreover, for each n = 20, 21, ... there are three distinct positive integers x, y and z with x + y + z = n such that x*(x+1)/2 + y*(y+1)/2 + z*(z+1)/2 is a triangular number.",
				"(ii) A positive integer n cannot be written as x + y + z (x, y, z \u003e 0) with x^2 + y^2 + z^2 a square if and only if n has the form 2^r*3^s or the form 2^r*7, where r and s are nonnegative integers.",
				"(iii) Any integer n \u003e 14 can be written as a + b + c + d, where a, b, c, d are positive integers with a^2 + b^2 + c^2 + d^2 a square. If n \u003e 20 is not among 22, 28, 30, 38, 44, 60, then we may require additionally that a, b, c, d are pairwise distinct.",
				"(iv) For each integer n \u003e 50 not equal to 71, there are positive integers a, b, c, d with a + b + c + d = n such that both a^2 + b^2 and c^2 + d^2 are squares.",
				"Part (ii) and the first assertion in part (iii) were confirmed by Chao Huang and Zhi-Wei Sun in 2021. - _Zhi-Wei Sun_, May 09 2021"
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A230121/b230121.txt\"\u003eTable of n, a(n) for n = 1..3000\u003c/a\u003e",
				"Chao Huang and Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/2105.03416\"\u003eOn partitions of integers with restrictions involving squares\u003c/a\u003e, arXiv:2105.03416 [math.NT], 2021.",
				"Zhi-Wei Sun, \u003ca href=\"http://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;7aed50fa.1310\"\u003eDiophantine problems involving triangular numbers and squares\u003c/a\u003e, a message to Number Theory List, Oct. 11, 2013."
			],
			"example": [
				"a(16) = 1 since 16 = 3 + 6 + 7 and 3*4/2 + 6*7/2 + 7*8/2 = 55 = 10*11/2."
			],
			"mathematica": [
				"SQ[n_]:=IntegerQ[Sqrt[n]]",
				"T[n_]:=n(n+1)/2",
				"a[n_]:=Sum[If[SQ[8(T[i]+T[j]+T[n-i-j])+1],1,0],{i,1,n/3},{j,i,(n-i)/2}]",
				"Table[a[n],{n,1,100}]"
			],
			"program": [
				"(PARI) a(n)=my(t=(n+1)*n/2,s);sum(x=1,n\\3,s=t-n--*x;sum(y=x,n\\2,is_A000217(s-(n-y)*y))) \\\\ - _M. F. Hasler_, Oct 11 2013"
			],
			"xref": [
				"Cf. A000217, A000290.",
				"Cf. A008443, A053604, A053603, A052343, A052344, A008441."
			],
			"keyword": "nonn",
			"offset": "1,9",
			"author": "_Zhi-Wei Sun_, Oct 10 2013",
			"references": 12,
			"revision": 24,
			"time": "2021-05-11T06:00:16-04:00",
			"created": "2013-10-10T09:19:33-04:00"
		}
	]
}