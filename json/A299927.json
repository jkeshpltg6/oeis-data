{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A299927",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 299927,
			"data": "1,1,2,3,1,4,3,1,5,6,4,1,6,10,10,5,1,7,15,20,15,6,1,8,21,35,35,21,7,1,9,28,56,70,56,28,8,1,10,36,84,126,126,84,36,9,1,11,45,120,210,252,210,120,45,10,1,12,55,165,330,462,462,330,165,55,11,1",
			"name": "Number of permutations of length n that avoid the patterns 213 and 312 and have k double ascents, read by rows.",
			"comment": [
				"In a permutation avoiding 213 and 312, all digits before n are increasing and all digits after n are decreasing.  If k=0, either n is the first digit or the second digit of the permutation.  If k \u003e= 1, there are binomial(n-1, k+1) ways to choose k+1 digits before n; these digits together with n account for k double ascents.",
				"For n \u003e= 1, the sum of row n is 2^(n-1)."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A299927/b299927.txt\"\u003eTable of n, a(n) for n = 0..11176\u003c/a\u003e (rows 0 \u003c= n \u003c= 150, flattened).",
				"M. Bukata, R. Kulwicki, N. Lewandowski, L. Pudwell, J. Roth, and T. Wheeland, \u003ca href=\"https://arxiv.org/abs/1812.07112\"\u003eDistributions of Statistics over Pattern-Avoiding Permutations\u003c/a\u003e, arXiv preprint arXiv:1812.07112 [math.CO], 2018.",
				"Paul M. Rakotomamonjy, Sandrataniaina R. Andriantsoa, Arthur Randrianarivony, \u003ca href=\"https://arxiv.org/abs/1910.13809\"\u003eCrossings over permutations avoiding some pairs of three length-patterns\u003c/a\u003e, arXiv:1910.13809 [math.CO], 2019."
			],
			"formula": [
				"If k=0 and n\u003e0, a(n,k)=n.",
				"If k \u003e= 1, a(n,k) = binomial(n-1,k+1)."
			],
			"example": [
				"a(5,0)=5.  This counts the permutations 15432, 25431, 35421, 45321, and 54321.",
				"a(5,1)=6.  This counts the permutations 12543, 13542, 14532, 23541, 24531, and 34521.",
				"Triangle begins:",
				"   1;",
				"   1;",
				"   2;",
				"   3,  1;",
				"   4,  3,   1;",
				"   5,  6,   4,   1;",
				"   6, 10,  10,   5,   1;",
				"   7, 15,  20,  15,   6,   1;",
				"   8, 21,  35,  35,  21,   7,   1;",
				"   9, 28,  56,  70,  56,  28,   8,   1;",
				"  10, 36,  84, 126, 126,  84,  36,   9,  1;",
				"  11, 45, 120, 210, 252, 210, 120,  45, 10,  1;",
				"  12, 55, 165, 330, 462, 462, 330, 165, 55, 11, 1;"
			],
			"maple": [
				"f:= proc(n, k)",
				"if n = 0 and k = 0 then return 1:",
				"elif k = 0 then return n:",
				"else return binomial(n-1, k+1):",
				"fi: end:",
				"f(0, 0), f(1, 0), seq(seq(f(i, j), j = 0 .. i-2), i = 2 .. 12)"
			],
			"mathematica": [
				"Table[Which[And[n \u003e 0, k == 0], n, k \u003e= 1, Binomial[n - 1, k + 1], True, 1], {n, 0, 12}, {k, 0, If[n \u003c 2, 0, n - 2]}] // Flatten (* _Michael De Vlieger_, Feb 07 2019 *)"
			],
			"keyword": "easy,nonn,tabf",
			"offset": "0,3",
			"author": "_Lara Pudwell_, Dec 15 2018",
			"references": 1,
			"revision": 41,
			"time": "2020-01-21T00:03:45-05:00",
			"created": "2018-12-16T13:15:01-05:00"
		}
	]
}