{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A285089",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 285089,
			"data": "1,4,2,9,6,3,16,12,8,10,25,20,15,18,5,36,30,24,28,21,14,49,42,35,40,32,50,7,64,56,48,54,45,66,27,44,81,72,63,70,60,84,55,78,33,100,90,80,88,77,104,91,98,65,22,121,110,99,108,96,126,112,170,105,52",
			"name": "Rectangular array by antidiagonals: row n is the ordered sequence of numbers k that minimize |d(n+1-k) - d(k)|, where d(i) are the divisors of n.",
			"comment": [
				"Every positive integer occurs exactly once, so that as a sequence, this is a permutation of the natural numbers, A000027.",
				"Every prime (A000040) occurs in column 1.",
				"Row 1: A000290 (squares)",
				"Row 2: A002378) (oblong numbers)",
				"Row 3: A005563",
				"Row 4: A028552 (for n\u003e=2)"
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A285089/b285089.txt\"\u003eAntidiagonals n = 1..60, flattened \u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"row 1:  k^2 for k\u003e=1",
				"row 2: k*(k+1) for k\u003e=1",
				"row 3: k*k+2) for k\u003e=3",
				"row 4: k*(k+3) for k\u003e=2",
				"row 5: k*(k+4) for k \u003e=3",
				"row 6: k*(k+5) for k\u003e=5",
				"row 7: k*(k+6) for k\u003e=7"
			],
			"example": [
				"Taking n = 12, the divisors are 1,2,3,4,6,12, so that for k=1..6, the numbers d(n+1-k) - d(k) are 12-1, 6-2, 4-3, 3-4, 2-6, 1-12.  Thus, the number k that minimizes |d(n+1-k) - d(k)| is 1, so that 12 appears in row 1 (with the top row as row 0), consisting of numbers for which the minimal value is 1.",
				"Northwest corner:",
				"1   4   9   16   25   36   49   64   81   10",
				"2   6   12  20   30   42   56   72   90   110",
				"3   8   15  24   35   48   63   80   99   120",
				"10  18  28  40   54   70   88   108  130  154",
				"5   21  32  45   60   77   96   117  140  165",
				"14  50  66  84   104  126  160  176  204  234",
				"7   27  55  91   112  135  160  187  216  247",
				"44  78  98  170  198  228  260  294  330  368"
			],
			"mathematica": [
				"d[n_] := Divisors[n]; k[n_] := Length[d[n]]; x[n_, i_] := d[n][[i]];",
				"a[n_] := If[OddQ[k[n]], 0, x[n, k[n]/2 + 1] - x[n, k[n]/2]]",
				"t = Table[a[j], {j, 1, 30000}];",
				"r[n_] := Flatten[Position[t, n]]; v[n_, k_] := r[n][[k]];",
				"w = Table[v[n, k], {n, 0, 10}, {k, 1, 10}];",
				"TableForm[w] (* A285089, array *)",
				"Table[v[n - k, k], {n, 0, 60}, {k, n, 1, -1}] // Flatten (* A285089, sequence *)"
			],
			"xref": [
				"Cf. A000027, A000040, A000290, A095833, A163280, A285090."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Apr 13 2017",
			"references": 2,
			"revision": 6,
			"time": "2017-04-14T03:59:22-04:00",
			"created": "2017-04-14T03:59:22-04:00"
		}
	]
}