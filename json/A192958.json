{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A192958",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 192958,
			"data": "1,-1,3,7,17,33,61,107,183,307,509,837,1369,2231,3627,5887,9545,15465,25045,40547,65631,106219,171893,278157,450097,728303,1178451,1906807,3085313,4992177,8077549,13069787,21147399,34217251,55364717,89582037",
			"name": "Constant term of the reduction by x^2 -\u003e x+1 of the polynomial p(n,x) defined at Comments.",
			"comment": [
				"The titular polynomials are defined recursively: p(n,x) = x*p(n-1,x) - 2 + n^2, with p(0,x)=1.  For an introduction to reductions of polynomials by substitutions such as x^2 -\u003e x+1, see A192232 and A192744."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A192958/b192958.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-2,-1,1)."
			],
			"formula": [
				"a(n) = 3*a(n-1) - 2*a(n-2) - a(n-3) + a(n-4).",
				"From _R. J. Mathar_, May 09 2014: (Start)",
				"G.f.: (1 -4*x +8*x^2 -3*x^3)/((1-x-x^2)*(1-x)^2).",
				"a(n) - 2*a(n-1) +a(n-2) = A022089(n-3). (End)",
				"a(n) = 6*Fibonacci(n+1) - (2*n+5). - _G. C. Greubel_, Jul 12 2019"
			],
			"mathematica": [
				"(* First program *)",
				"q = x^2; s = x + 1; z = 40;",
				"p[0, x]:= 1;",
				"p[n_, x_]:= x*p[n-1, x] + n^2 - 2;",
				"Table[Expand[p[n, x]], {n, 0, 7}]",
				"reduce[{p1_, q_, s_, x_}]:= FixedPoint[(s PolynomialQuotient @@ #1 + PolynomialRemainder @@ #1 \u0026)[{#1, q, x}] \u0026, p1]",
				"t = Table[reduce[{p[n, x], q, s, x}], {n, 0, z}];",
				"u1 = Table[Coefficient[Part[t, n], x, 0], {n, 1, z}] (* A192958 *)",
				"u2 = Table[Coefficient[Part[t, n], x, 1], {n, 1, z}] (* A192959 *)",
				"(* Second program *)",
				"With[{F=Fibonacci}, Table[6*F[n+1]-(2*n+5), {n,0,40}]] (* _G. C. Greubel_, Jul 12 2019 *)"
			],
			"program": [
				"(PARI) vector(40, n, n--; f=fibonacci; 6*f(n+1)-(2*n+5)) \\\\ _G. C. Greubel_, Jul 12 2019",
				"(MAGMA) F:=Fibonacci; [6*F(n+1)-(2*n+5): n in [0..40]]; // _G. C. Greubel_, Jul 12 2019",
				"(Sage) f=fibonacci; [6*f(n+1)-(2*n+5) for n in (0..40)] # _G. C. Greubel_, Jul 12 2019",
				"(GAP) F:=Fibonacci;; List([0..40], n-\u003e 6*F(n+1)-(2*n+5)); # _G. C. Greubel_, Jul 12 2019"
			],
			"xref": [
				"Cf. A000045, A192232, A192744, A192951, A192959."
			],
			"keyword": "sign",
			"offset": "0,3",
			"author": "_Clark Kimberling_, Jul 13 2011",
			"references": 3,
			"revision": 15,
			"time": "2019-07-12T20:18:19-04:00",
			"created": "2011-07-13T20:17:34-04:00"
		}
	]
}