{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A156952",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 156952,
			"data": "1,1,1,1,5,1,1,105,105,1,1,8925,187425,8925,1,1,3043425,5432513625,5432513625,3043425,1,1,4154275125,2528644954460625,214934821129153125,2528644954460625,4154275125,1",
			"name": "Triangle T(n, k, q) = t(n,q)/(t(k,q)*t(n-k,q)), where t(n, k) = Product_{j=1..n} q-Pochhammer(j, k+1, k+1)/(1-(k+1))^j and t(n, 0) = n!, with q = 3, read by rows.",
			"reference": [
				"Steve Roman, The Umbral Calculus, Dover Publications, New York (1984), page 182."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A156952/b156952.txt\"\u003eRows n = 0..20 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k, q) = t(n,q)/(t(k,q)*t(n-k,q)), where t(n, k) = Product_{j=1..n} q-Pochhammer(j, k+1, k+1)/(1-(k+1))^j and t(n, 0) = n!, with q = 3."
			],
			"example": [
				"Triangle begins as:",
				"  1;",
				"  1,       1;",
				"  1,       5,          1;",
				"  1,     105,        105,          1;",
				"  1,    8925,     187425,       8925,       1;",
				"  1, 3043425, 5432513625, 5432513625, 3043425, 1;"
			],
			"mathematica": [
				"t[n_, k_]= If[k==0, n!, Product[QPochhammer[k+1, k+1, j]/(-k)^j, {j, n}]];",
				"T[n_, k_, q_]= t[n, q]/(t[k, q]*t[n-k, q]);",
				"Table[T[n,k,3], {n,0,10}, {k,0,n}]//Flatten (* modified by _G. C. Greubel_, Jan 08 2022 *)"
			],
			"program": [
				"(Sage)",
				"from sage.combinat.q_analogues import q_pochhammer",
				"def t(n,k): return factorial(n) if (k==0) else product( q_pochhammer(j, k+1, k+1)/(-k)^j for j in (1..n) )",
				"def T(n,k,q): return t(n,q)/(t(k,q)*t(n-k,q))",
				"flatten([[T(n,k,3) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Jan 08 2022"
			],
			"xref": [
				"Cf. A007318 (q=0), A156950 (q=1), A156951 (q=2), this sequence (q=3).",
				"Cf. A156953."
			],
			"keyword": "nonn,tabl,changed",
			"offset": "0,5",
			"author": "_Roger L. Bagula_, Feb 19 2009",
			"ext": [
				"Edited by _G. C. Greubel_, Jan 08 2022"
			],
			"references": 4,
			"revision": 6,
			"time": "2022-01-09T02:32:21-05:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}