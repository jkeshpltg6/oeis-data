{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A143382",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 143382,
			"data": "1,2,5,17,71,121,731,1711,41099,370019,740101,2713789,1206137,423355111,846710651,1814380259,203210595443,12654139763,531473870981,43758015399281,525096184837561,441080795274037,22054039763790029",
			"name": "Numerator of Sum_{k=0..n} 1/k!!.",
			"comment": [
				"Denominators are A143383. A143382(n)/A143383(n) is to A007676(n)/A007676(n) as double factorials are to factorials. A143382/A143383 fractions begin:",
				"n numerator/denominator",
				"0 1/0!! = 1/1",
				"1 1/0!! + 1/1!! = 2/1",
				"2 1/0!! + 1/1!! + 1/2!! = 5/2",
				"3 1/0!! + 1/1!! + 1/2!! + 1/3!! = 17/6",
				"4 1/0!! + 1/1!! + 1/2!! + 1/3!! + 1/4!! = 71/24",
				"5 1/0!! + 1/1!! + 1/2!! + 1/3!! + 1/4!! + 1/5!! = 121/40",
				"6 1/0!! + 1/1!! + 1/2!! + 1/3!! + 1/4!! + 1/5!! + 1/6!! = 731/240",
				"The series converges to sqrt(e) + sqrt((e*Pi)/2)*erf(1/sqrt(2)) = 3.0594074053425761445... whose decimal expansion is given by A143280. The analogs of A094007 and A094008 are determined by 2 being the only prime denominator in the convergents to the sum of reciprocals of double factorials and prime numerators beginning: a(1) = 2, a(2) = 5, a(3) = 17, a(4) = 71, a(15) = 1814380259, a(19) = 43758015399281, a(21) = 441080795274037, a(23) = 867081905243923."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A143382/b143382.txt\"\u003eTable of n, a(n) for n = 0..500\u003c/a\u003e",
				"Eric W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/DoubleFactorial.html\"\u003eDouble Factorial\u003c/a\u003e. Gives formula for limit of series, which was independently derived by Max Alekseyev."
			],
			"formula": [
				"Numerators of Sum_{k=0..n} 1/k!! = Sum_{k=0..n} 1/A006882(k)."
			],
			"example": [
				"a(3) = 17 because 1/0!! + 1/1!! + 1/2!! + 1/3!! = 17/6.",
				"a(15) = 1814380259 because 1814380259/593049600 = 1/1 + 1/1 + 1/2 + 1/3 + 1/8 + 1/15 + 1/48 + 1/105 + 1/384 + 1/945 + 1/3840 + 1/10395 + 1/46080 + 1/135135 + 1/645120 + 1/2027025."
			],
			"mathematica": [
				"Table[Numerator[Sum[1/k!!, {k, 0, n}]], {n, 0, 30}] (* _G. C. Greubel_, Mar 28 2019 *)"
			],
			"program": [
				"(PARI) vector(25, n, n--; numerator(sum(k=0,n, 1/prod(j=0,floor((k-1)/2), (k - 2*j)) ))) \\\\ _G. C. Greubel_, Mar 28 2019",
				"(MAGMA) [n le 0 select 1 else Numerator( 1 + (\u0026+[ 1/(0 + (\u0026*[k-2*j: j in [0..Floor((k-1)/2)]])) : k in [1..n]]) ): n in [0..25]]; // _G. C. Greubel_, Mar 28 2019",
				"(Sage) [numerator(sum( 1/product((k - 2*j) for j in (0..floor((k-1)/2)))   for k in (0..n))) for n in (0..25)] # _G. C. Greubel_, Mar 28 2019"
			],
			"xref": [
				"Cf. A006882 (n!!), A094007, A143280 (m(2))), A143383 (denominators)."
			],
			"keyword": "easy,frac,nonn",
			"offset": "0,2",
			"author": "_Jonathan Vos Post_, Aug 11 2008",
			"references": 2,
			"revision": 10,
			"time": "2019-03-28T20:21:12-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}