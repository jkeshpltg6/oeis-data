{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A253563",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 253563,
			"data": "1,2,4,3,8,6,9,5,16,12,18,10,27,15,25,7,32,24,36,20,54,30,50,14,81,45,75,21,125,35,49,11,64,48,72,40,108,60,100,28,162,90,150,42,250,70,98,22,243,135,225,63,375,105,147,33,625,175,245,55,343,77,121,13,128,96,144,80,216,120,200,56,324,180,300,84,500,140,196,44",
			"name": "Permutation of natural numbers: a(0) = 1, a(1) = 2; after which, a(2n) = A253560(a(n)), a(2n+1) = A253550(a(n)).",
			"comment": [
				"This sequence can be represented as a binary tree. Each child to the left is obtained by applying A253560 to the parent, and each child to the right is obtained by applying A253550 to the parent:",
				"                                     1",
				"                                     |",
				"                  ...................2...................",
				"                 4                                       3",
				"       8......../ \\........6                   9......../ \\........5",
				"      / \\                 / \\                 / \\                 / \\",
				"     /   \\               /   \\               /   \\               /   \\",
				"    /     \\             /     \\             /     \\             /     \\",
				"  16       12         18       10         27       15         25       7",
				"32  24   36  20     54  30   50  14     81  45   75  21    125  35   49 11",
				"etc.",
				"Sequence A253565 is the mirror image of the same tree. Also in binary trees A005940 and A163511 the terms on level of the tree are some permutation of the terms present on the level n of this tree. A252464(n) tells distance of n from 1 in all these trees. Of these four trees, this is the one where the left child is always larger than the right child.",
				"Note that the indexing of sequence starts from 0, although its range starts from one.",
				"a(n) (n\u003e=1) can be obtained by the composition of a bijection between {1,2,3,4,...} and the set of integer partitions and a bijection between the set of integer partitions and {2,3,4,...}. Explanation on the example n=10. Write 2*n = 20 as a binary number: 10100. Consider a Ferrers board whose southeast border is obtained by replacing each 1 by an east step and each 0 by a north step. We obtain the Ferrers board of the partition p = (2,2,1). Finally, a(10) = 2'*2'*1', where m' = m-th prime. Thus, a(10)= 3*3*2 = 18. - _Emeric Deutsch_, Sep 17 2016"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A253563/b253563.txt\"\u003eTable of n, a(n) for n = 0..8191\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(0) = 1, a(1) = 2; after which, a(2n) = A253560(a(n)), a(2n+1) = A253550(a(n)).",
				"As a composition of other permutations:",
				"a(n) = A122111(A005940(n+1)).",
				"a(n) = A253565(A054429(n)).",
				"Other identities and observations. For all n \u003e= 0:",
				"A002110(n) =  a(A002450(n)). [Primorials occur at positions (4^n - 1)/3.]",
				"For all n \u003e= 1: a(2n) - a(2n+1) \u003e 0. [See the comment above.]"
			],
			"maple": [
				"a:= proc(n) local m; m:= n; [0]; while m\u003e0 do `if`(1=",
				"      irem(m, 2, 'm'), map(x-\u003e x+1, %), [%[], 0]) od:",
				"      `if`(n=0, 1, mul(ithprime(i), i=%))",
				"    end:",
				"seq(a(n), n=0..100);  # _Alois P. Heinz_, Aug 23 2017"
			],
			"mathematica": [
				"p[n_] := p[n] = FactorInteger[n][[-1, 1]];",
				"b[n_] := n p[n];",
				"c[1] = 1; c[n_] := (n/p[n]) NextPrime[p[n]];",
				"a[0] = 1; a[1] = 2; a[n_] := a[n] = If[EvenQ[n], b[a[n/2]], c[a[(n-1)/2]]];",
				"a /@ Range[0, 100] (* _Jean-François Alcover_, Feb 15 2021 *)"
			],
			"program": [
				"(Scheme, two versions, the other one using memoizing definec-macro)",
				"(definec (A253563 n) (cond ((\u003c n 2) (+ 1 n)) ((even? n) (A253560 (A253563 (/ n 2)))) (else (A253550 (A253563 (/ (- n 1) 2))))))",
				"(define (A253563 n) (A122111 (A005940 (+ 1 n))))"
			],
			"xref": [
				"Inverse: A253564.",
				"Cf. A252737 (row sums), A252738 (row products).",
				"Cf. A002110, A002450, A005940, A122111, A252464, A253561, A253550, A253560, A253565, A054429, A252464."
			],
			"keyword": "nonn,look,tabf",
			"offset": "0,2",
			"author": "_Antti Karttunen_, Jan 03 2015",
			"references": 18,
			"revision": 33,
			"time": "2021-02-15T04:42:28-05:00",
			"created": "2015-01-22T01:31:09-05:00"
		}
	]
}