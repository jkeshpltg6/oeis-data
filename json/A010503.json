{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A010503",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 10503,
			"data": "7,0,7,1,0,6,7,8,1,1,8,6,5,4,7,5,2,4,4,0,0,8,4,4,3,6,2,1,0,4,8,4,9,0,3,9,2,8,4,8,3,5,9,3,7,6,8,8,4,7,4,0,3,6,5,8,8,3,3,9,8,6,8,9,9,5,3,6,6,2,3,9,2,3,1,0,5,3,5,1,9,4,2,5,1,9,3,7,6,7,1,6,3,8,2,0,7,8,6,3,6,7,5,0,6",
			"name": "Decimal expansion of 1/sqrt(2).",
			"comment": [
				"The decimal expansion of sqrt(50) = 5*sqrt(2) = 7.0710678118654752440... gives essentially the same sequence.",
				"1/sqrt(2) = cos(Pi/4) = sqrt(2)/2. - _Eric Desbiaux_, Nov 05 2008",
				"Also real and imaginary part of the square root of the imaginary unit. - _Alonso del Arte_, Jan 07 2011",
				"1/sqrt(2) = (1/2)^(1/2) = (1/4)^(1/4) (see the comments in A072364).",
				"If a triangle has sides whose lengths form a harmonic progression in the ratio 1 : 1/(1 + d) : 1/(1 + 2d) then the triangle inequality condition requires that d be in the range -1 + 1/sqrt(2) \u003c d \u003c 1/sqrt(2). - _Frank M Jackson_, Oct 11 2011",
				"Let s_2(n) be the sum of the base-2 digits of n and epsilon(n) = (-1)^s_2(n), the Thue-Morse sequence A010060, then prod(n \u003e= 0, ((2*n + 1)/(2*n + 2))^epsilon(n) ) = 1/sqrt(2). - _Jonathan Vos Post_, Jun 03 2012",
				"The square root of 1/2 and thus it follows from the Pythagorean theorem that it is the sine of 45 degrees (and the cosine of 45 degrees). - _Alonso del Arte_, Sep 24 2012",
				"Circumscribed sphere radius for a regular octahedron with unit edges. In electric engineering, ratio of effective amplitude to peak amplitude of an alternating current/voltage. - _Stanislav Sykora_, Feb 10 2014",
				"Radius of midsphere (tangent to edges) in a cube with unit edges. - _Stanislav Sykora_, Mar 27 2014"
			],
			"link": [
				"Harry J. Smith, \u003ca href=\"/A010503/b010503.txt\"\u003eTable of n, a(n) for n = 0..20000\u003c/a\u003e",
				"P. C. Fishburn and J. A. Reeds, \u003ca href=\"http://dx.doi.org/10.1137/S0895480191219350\"\u003eBell inequalities, Grothendieck's constant and root two\u003c/a\u003e, SIAM J. Discrete Math., Vol. 7, No. 1, Feb. 1994, pp 48-56.",
				"J. Sondow and D. Marques, \u003ca href=\"http://arxiv.org/abs/1108.6096\"\u003eAlgebraic and transcendental solutions of some exponential equations\u003c/a\u003e, Annales Mathematicae et Informaticae 37 (2010) 151-164; arXiv:1108.6096 [math.NT], 2011, see p. 3 in the link.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DigitProduct.html\"\u003eDigit Product\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Platonic solid\"\u003ePlatonic solid\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 9 - A268682(n). As constants, this sequence is 1 - A268682. - _Philippe Deléham_, Feb 21 2016",
				"From _Amiram Eldar_, Jun 29 2020: (Start)",
				"Equals sin(Pi/4) = cos(Pi/4).",
				"Equals Integral_{x=0..Pi/4} cos(x) dx. (End)",
				"Equals (1/2)*A019884 + A019824 * A010527 = A019851 * A019896 + A019812 * A019857. - _R. J. Mathar_, Jan 27 2021"
			],
			"example": [
				"0.7071067811865475..."
			],
			"maple": [
				"Digits:=100; evalf(1/sqrt(2)); _Wesley Ivan Hurt_, Mar 27 2014"
			],
			"mathematica": [
				"N[ 1/Sqrt[2], 200]",
				"RealDigits[1/Sqrt[2],10,120][[1]] (* _Harvey P. Dale_, Mar 25 2019 *)"
			],
			"program": [
				"(PARI) default(realprecision, 20080); x=10*(1/sqrt(2)); for (n=0, 20000, d=floor(x); x=(x-d)*10; write(\"b010503.txt\", n, \" \", d)); \\\\ _Harry J. Smith_, Jun 02 2009",
				"(MAGMA) 1/Sqrt(2); // _Vincenzo Librandi_, Feb 21 2016"
			],
			"xref": [
				"Cf. A040042, A072364, A268682.",
				"Cf. A073084 (infinite tetration limit).",
				"Platonic solids circumradii: A010527 (cube), A019881 (icosahedron), A179296 (dodecahedron), A187110 (tetrahedron).",
				"Platonic solids midradii: A020765 (tetrahedron), A020761 (octahedron), A019863 (icosahedron), A239798 (dodecahedron)."
			],
			"keyword": "nonn,cons,easy",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Harry J. Smith_, Jun 02 2009"
			],
			"references": 55,
			"revision": 111,
			"time": "2021-05-28T17:19:50-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}