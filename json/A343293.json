{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A343293",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 343293,
			"data": "36,64,81,512,196,16384,1089,8589934592,3844,4611686018427387904,31329,191561942608236107294793378393788647952342390272950272,478864",
			"name": "a(n+1) is the smallest preimage k such that A008477(k) = a(n) with a(1) = 36.",
			"comment": [
				"Equivalently, when g is the reciprocal application of f = A008477 as defined in the Name, the terms of this sequence are the successive terms of the infinite iterated sequence {m, g(m), g(g(m)), g(g(g(m))), ...} that begins with m = a(1) = 36, hence f(a(n)) = a(n-1).",
				"Why choose 36? Because it is the smallest integer for which there exists such an infinite iterated sequence, with g(36) = 64; then f(36) = 32 with the periodic sequence (32, 25, 32, 25, ...) (see A062307). Explanation: 36 is the first nonsquarefree number in A342973 that is also squareful. The nonsquarefree terms \u003c 36: 12, 18, 20, 24, 28 in A342973 are not squareful (A332785), so they have no preimage by f.",
				"When a(n-1) has several preimages by f, as a(n) is the smallest preimage, this sequence is well defined (see examples).",
				"All the terms are nonsquarefree but also powerful, hence they are in A001694.",
				"a(n) \u003c a(n+2) (last comment in A008477) but a(n) \u003c a(n+1) or a(n) \u003e a(n+1).",
				"Prime factorizations from a(1) to a(13): 2^2*3^2, 2^6, 3^4, 2^9, 2^2*7^2, 2^14, 3^2*11^2, 2^33, 2^2*31^2, 2^62, 3^2*59^2, 2^177, 2^4*173^2.",
				"It appears that a(2m) = 2^q for some q\u003e1 and a(2m+1) = r^2 for some r\u003e1.",
				"a(14) \u003c= 2^692."
			],
			"link": [
				"Annales Concours Général, \u003ca href=\"https://www.freemaths.fr/annales-composition-mathematiques-concours-general/concours-general-mathematiques-sujet-serie-s-2012.pdf\"\u003eSujet Concours Général 2012\u003c/a\u003e (in French, problems).",
				"Annales Concours Général, \u003ca href=\"https://www.freemaths.fr/annales-composition-mathematiques-concours-general/concours-general-mathematiques-corrige-serie-s-2012.pdf\"\u003eCorrigé Concours Général 2012\u003c/a\u003e (in French, solutions)."
			],
			"example": [
				"a(1) = 36; 64 = 2^6 so f(64) = 6^2 = 36, also 192 = 2^6*3^1 and f(192) = 6^2*1^3 = 36 we have f(64) = f(192) = 36; but as 64 \u003c 192, hence g(36) = 64 and a(2) = 64.",
				"a(2) = 64 = f(81) = f(256), but as 81 \u003c 256, g(64) = 81 and a(3) = 81.",
				"a(4) = 512 = f(196) = f(400), but as 196 \u003c 400, g(512) = 196 and a(5) = 196."
			],
			"xref": [
				"Cf. A001694, A008477, A062307, A332785, A342973."
			],
			"keyword": "nonn,more",
			"offset": "1,1",
			"author": "_Bernard Schott_, Apr 11 2021",
			"ext": [
				"a(10)-a(13) from _Bert Dobbelaere_, Apr 13 2021"
			],
			"references": 2,
			"revision": 37,
			"time": "2021-04-15T10:18:14-04:00",
			"created": "2021-04-11T23:50:58-04:00"
		}
	]
}