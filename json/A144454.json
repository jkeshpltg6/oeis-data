{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A144454",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 144454,
			"data": "0,1,8,5,8,35,16,7,80,11,40,143,56,65,224,85,32,323,40,133,440,161,176,575,208,75,728,87,280,899,320,341,1088,385,136,1295,152,481,1520,533,560,1763,616,215,2024,235,736,2303,800,833,2600,901,312,2915,336,1045",
			"name": "First trisection of A061039.",
			"comment": [
				"Numerator of (n^2-1)/(9n^2). Denominator is A147650(n).",
				"Terms alternate between even and odd. The sequence modulo 9 reads 0,1,8,5,8,8,7,7,8,2,4,8,2,2,8,4,5,... (Is there a meaning to the interpretation as the constant 0.1858877824822845...?) The first appearance of 3 (mod 9) is at a(26)=75, the second at a(55)=336. The first appearance of 6 (mod 9) is at a(28)=87, the second at a(53)=312.",
				"a(n) also gives the numerator of (n^2 - 1)/(3*((2*n)^2 - 1)) =: r(n-1), with denominators A300295(n-1), for n \u003e= 1. For the proof see a comment in A300295; also for details on r(n) with the Jolley reference. - _Wolfdieter Lang_, Mar 15 2018",
				"a(n) is also the numerator of Sum_{k=0..n} (1/((2*k-3)(2*k-1)*(2*k+1))). This summation is an offset adjusted form of formula 209 in Jolley's \"Summation of Series\". The closed form is given in the Lang comment above. - _Gary Detlefs_, Mar 15 2018"
			],
			"reference": [
				"L. B. W. Jolley, \"Summation of Series\", Dover Publications, 1961, pp. 40, 41."
			],
			"link": [
				"Harvey P. Dale, \u003ca href=\"/A144454/b144454.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_27\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, -3, 0, 0, 0, 0, 0, 0, 0, 0, 1)."
			],
			"formula": [
				"a(n) = A061039(3n).",
				"For n \u003e 27, a(n) = 3*a(n-9) - 3*a(n-18) + a(n-27). - _Harvey P. Dale_, Jan 16 2013",
				"a(n) = (n^2 - 1)/9 if n == 1 (mod 9) or == 8 (mod 9). For other n: a(n) = (n^2 - 1)/3 if n == 1 (mod 3) or == 2(mod 3), and a(n) = n^2 - 1 if n == 0 (mod 3). The proof uses the first comment and gcd(n^2-1, n^2) = 1. - _Wolfdieter Lang_, Mar 15 2018",
				"G.f.: x^2*(1 + 8*x + 5*x^2 + 8*x^3 + 35*x^4 + 16*x^5 + 7*x^6 + 80*x^7 + 11*x^8 + 37*x^9 + 119*x^10 + 41*x^11 + 41*x^12 + 119*x^13 + 37*x^14 + 11*x^15 + 83*x^16 + 7*x^17 + 16*x^18 + 35*x^19 + 8*x^20 + 5*x^21 + 8*x^22 + x^23 - x^25) / ((1 - x)^3*(1 + x + x^2)^3*(1 + x^3 + x^6)^3). - _Colin Barker_, Mar 15 2018"
			],
			"example": [
				"The rationals (n^2 - 1)/(9*n^2) begin: 0/1, 1/12, 8/81, 5/48, 8/75, 35/324, 16/147, 7/64, 80/729, 11/100, 40/363, 143/1296, 56/507, 65/588, ... - _Wolfdieter Lang_, Mar 15 2018"
			],
			"maple": [
				"P:=n-\u003e sum(1/((2*k-3)*(2*k-1)*(2*k+1)), k = 0 n); seq(numer(P(i))i,=1..50) # _Gary Detlefs_, Mar 15 2018"
			],
			"mathematica": [
				"Numerator[Table[((n-1)(n+1))/(9n^2),{n,60}]] (* or *) LinearRecurrence[ {0,0,0,0,0,0,0,0,3,0,0,0,0,0,0,0,0,-3,0,0,0,0,0,0,0,0,1},{0,1,8,5,8,35,16,7,80,11,40,143,56,65,224,85,32,323,40,133,440,161,176,575,208,75,728},60] (* _Harvey P. Dale_, Jan 16 2013 *)"
			],
			"program": [
				"(PARI) concat(0, Vec(x^2*(1 + 8*x + 5*x^2 + 8*x^3 + 35*x^4 + 16*x^5 + 7*x^6 + 80*x^7 + 11*x^8 + 37*x^9 + 119*x^10 + 41*x^11 + 41*x^12 + 119*x^13 + 37*x^14 + 11*x^15 + 83*x^16 + 7*x^17 + 16*x^18 + 35*x^19 + 8*x^20 + 5*x^21 + 8*x^22 + x^23 - x^25) / ((1 - x)^3*(1 + x + x^2)^3*(1 + x^3 + x^6)^3) + O(x^60))) \\\\ _Colin Barker_, Mar 15 2018",
				"(PARI) a(n) = numerator(1/9-1/(3*n)^2); \\\\ _Altug Alkan_, Mar 15 2018"
			],
			"xref": [
				"Cf. A147650, A300295."
			],
			"keyword": "nonn,frac,easy",
			"offset": "1,3",
			"author": "_Paul Curtz_, Oct 07 2008",
			"ext": [
				"Edited and extended by _R. J. Mathar_, Oct 24 2008"
			],
			"references": 9,
			"revision": 54,
			"time": "2021-09-26T05:06:26-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}