{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A077221",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 77221,
			"data": "0,1,8,17,32,49,72,97,128,161,200,241,288,337,392,449,512,577,648,721,800,881,968,1057,1152,1249,1352,1457,1568,1681,1800,1921,2048,2177,2312,2449,2592,2737,2888,3041,3200,3361,3528,3697,3872,4049,4232",
			"name": "a(0) = 0 and then alternately even and odd numbers in increasing order such that the sum of any two successive terms is a square.",
			"comment": [
				"This sequence arises from reading the line from 0, in the direction 0, 1, ... and the same line from 0, in the direction 0, 8, ..., in the square spiral whose vertices are the triangular numbers A000217. Cf. A139591, etc. - _Omar E. Pol_, May 03 2008",
				"The general formula for alternating sums of powers of odd integers is in terms of the Swiss-Knife polynomials P(n,x) A153641 (P(n,0)-(-1)^k*P(n,2*k))/2. Here n=2, thus a(k) = |(P(2,0)-(-1)^k*P(2,2*k))/2|. - _Peter Luschny_, Jul 12 2009",
				"Axis perpendicular to A046092 in the square spiral whose vertices are the triangular numbers A000217. See the comment above. - _Omar E. Pol_, Sep 14 2011",
				"Column 8 of A195040. - _Omar E. Pol_, Sep 28 2011",
				"Concentric octagonal numbers. A139098 and A069129 interleaved. - _Omar E. Pol_, Sep 17 2011",
				"Subsequence of A194274. - _Bruno Berselli_, Sep 22 2011",
				"Partial sums of A047522. - _Reinhard Zumkeller_, Jan 07 2012",
				"Alternating sum of the first n odd squares in decreasing order, n \u003e= 1. Also number of \"ON\" cells at n-th stage in simple 2-dimensional cellular automaton. The rules are: on the infinite square grid, start with all cells OFF, so a(0) = 0. Turn a single cell to the ON state, so a(1) = 1. At each subsequent step, the neighbor cells of each cell of the old generation are turned ON, and the cells of the old generation are turned OFF. Here \"neighbor\" refers to the eight adjacent cells of each ON cell. See example. - _Omar E. Pol_, Feb 16 2014"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A077221/b077221.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Bruno Berselli, \u003ca href=\"http://www.base5forum.it/upload/A_077221.jpg\"\u003eAn origin of A077221 (illustration)\u003c/a\u003e (see Pol's comment).",
				"N. J. A. Sloane, \u003ca href=\"/wiki/Catalog_of_Toothpick_and_CA_Sequences_in_OEIS\"\u003eCatalog of Toothpick and Cellular Automata Sequences in the OEIS\u003c/a\u003e",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,0,-2,1)."
			],
			"formula": [
				"a(2n) = 8*n^2, a(2n+1) = 8*n(n+1) + 1.",
				"From _Ralf Stephan_, Mar 31 2003: (Start)",
				"a(n) = 2*n^2 + 4*n + 1 [+1 if n is odd] with a(0)=1.",
				"G.f.: x*(x^2+6*x+1)/(1-x)^3/(1+x). (End)",
				"Row sums of triangle A131925; binomial transform of (1, 7, 2, 4, -8, 16, -32, ...). - _Gary W. Adamson_, Jul 29 2007",
				"a(n) = a(-n); a(n+1) = A195605(n) - (-1)^n. - _Bruno Berselli_, Sep 22 2011",
				"a(n) = 2*n^2 + ((-1)^n-1)/2. - _Omar E. Pol_, Sep 28 2011"
			],
			"example": [
				"From _Omar E. Pol_, Feb 16 2014: (Start)",
				"Illustration of initial terms as a cellular automaton:",
				".",
				".                                   O O O O O O O",
				".                     O O O O O     O           O",
				".           O O O     O       O     O   O O O   O",
				".     O     O   O     O   O   O     O   O   O   O",
				".           O O O     O       O     O   O O O   O",
				".                     O O O O O     O           O",
				".                                   O O O O O O O",
				".",
				".     1       8           17              32",
				".",
				"(End)"
			],
			"maple": [
				"a := n -\u003e 2*n^2 - (n mod 2); # _Peter Luschny_, Jul 12 2009"
			],
			"mathematica": [
				"a=1;lst={a};Do[b=n^2-a;AppendTo[lst,b];a=b,{n,3,6!,2}];lst (* _Vladimir Joseph Stephan Orlovsky_, May 18 2009 *)"
			],
			"program": [
				"(MAGMA) [2*n^2 - (n mod 2): n in [0..50]]; // _Vincenzo Librandi_, Sep 22 2011",
				"(Haskell)",
				"a077221 n = a077221_list !! n",
				"a077221_list = scanl (+) 0 a047522_list",
				"-- _Reinhard Zumkeller_, Jan 07 2012"
			],
			"xref": [
				"Cf. A077222, A131925, A032528, A195041, A195042, A195142."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Amarnath Murthy_, Nov 03 2002",
			"ext": [
				"Extended by _Ralf Stephan_, Mar 31 2003"
			],
			"references": 31,
			"revision": 67,
			"time": "2021-05-04T01:06:21-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}