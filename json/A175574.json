{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A175574",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 175574,
			"data": "1,1,8,0,3,4,0,5,9,9,0,1,6,0,9,6,2,2,6,0,4,5,3,3,7,9,4,0,5,5,8,4,8,8,5,8,7,2,3,3,7,1,6,6,3,4,8,8,1,4,4,7,2,9,9,5,1,5,8,6,4,3,9,9,4,0,4,3,0,4,1,8,0,7,2,0,7,1,5,7,9,4,9,7,8,4,5,8,6,1,6,1,9,5,8,0,7,9,5,4,2,0,9,4,5",
			"name": "Decimal expansion of sqrt(Pi) / (Gamma(3/4))^2 .",
			"comment": [
				"Entry 34 c of chapter 11 of Ramanujan's second notebook.",
				"This constant is also the ratio T(Pi/2)/T(0), where T(Pi/2) is the exact pendulum period for an amplitude of Pi/2 and T(0) the approximate period 2*Pi*sqrt(L/g) for small angles. - _Jean-François Alcover_, Aug 05 2014"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A175574/b175574.txt\"\u003eTable of n, a(n) for n = 1..5000\u003c/a\u003e",
				"Bruce C. Berndt, \u003ca href=\"http://dx.doi.org/10.1112/blms/15.4.273\"\u003eChapter 11 of Ramanujan's second notebook\u003c/a\u003e, Bull. Lond. Math. Soc. vol 15 no 4 (1983) 273-320.",
				"Claudio Carvalhaes and Patrick Suppes, \u003ca href=\"https://suppes-corpus.stanford.edu/sites/g/files/sbiybj7316/f/approximations_for_the_period_of_the_simple_pendulum_based_on_the_arithmetic-geometric_mean_431.pdf\"\u003eApproximations for the period of the simple pendulum based on the arithmetic-geometric mean\u003c/a\u003e."
			],
			"formula": [
				"Equals A002161 /A068465^2.",
				"Equals 2F1([1/2,1/2],[1],1/2) = 1/agm(1, sqrt(1/2)) = gamma(1/4)^2/(2*Pi^(3/2)).",
				"Equals 2*sqrt(2)*K(-1)/Pi, where K is the complete elliptic integral of the first kind, K(-1) being A085565. - _Jean-François Alcover_, Jun 03 2014",
				"Equals Product_{k\u003e=1} (1-(-1)^k/(2*k)) =  3/2 * 3/4 * 7/6 * 7/8 * 11/10 * 11/12 * ... .  - _Richard R. Forberg_, Dec 05 2015",
				"Reciprocal of A096427. Equals ( Sum_{n = -inf..inf} exp(-Pi*n^2) )^2, a rapidly converging series. For example, summing from n = -5 to n = 5 gives the constant correct to 49 decimal places. - _Peter Bala_, Mar 06 2019",
				"Equals Sum_{k\u003e=0) binomial(2*k,k)^2/2^(5*k). - _Amiram Eldar_, Aug 26 2020"
			],
			"example": [
				"1.18034059901609622604533794.."
			],
			"maple": [
				"sqrt(Pi)/GAMMA(3/4)^2 ; evalf(%) ;"
			],
			"mathematica": [
				"First@ RealDigits[N[Sqrt@ Pi/Gamma[3/4]^2, 120]] (* _Michael De Vlieger_, Dec 06 2015 *)"
			],
			"program": [
				"(PARI) sqrt(Pi)/gamma(3/4)^2 \\\\ _Altug Alkan_, Dec 05 2015",
				"(MATLAB) sqrt(pi)/gamma(3/4)^2 \\\\ _Altug Alkan_, Dec 05 2015"
			],
			"xref": [
				"Cf. A002161, A068465, A085565, A096427."
			],
			"keyword": "cons,easy,nonn",
			"offset": "1,3",
			"author": "_R. J. Mathar_, Jul 15 2010",
			"ext": [
				"A-number typo for sqrt(Pi) corrected by _R. J. Mathar_, Aug 01 2010"
			],
			"references": 5,
			"revision": 45,
			"time": "2020-08-26T03:06:05-04:00",
			"created": "2010-07-31T03:00:00-04:00"
		}
	]
}