{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005835",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5835,
			"id": "M4094",
			"data": "6,12,18,20,24,28,30,36,40,42,48,54,56,60,66,72,78,80,84,88,90,96,100,102,104,108,112,114,120,126,132,138,140,144,150,156,160,162,168,174,176,180,186,192,196,198,200,204,208,210,216,220,222,224,228,234,240,246,252,258,260,264",
			"name": "Pseudoperfect (or semiperfect) numbers n: some subset of the proper divisors of n sums to n.",
			"comment": [
				"In other words, some subset of the numbers { 1 \u003c= d \u003c n : d divides n } adds up to n. - _N. J. A. Sloane_, Apr 06 2008",
				"Also, numbers n such that A033630(n) \u003e 1. - _Reinhard Zumkeller_, Mar 02 2007",
				"Deficient numbers cannot be pseudoperfect. This sequence includes the perfect numbers (A000396). By definition, it does not include the weird, i.e., abundant but not pseudoperfect, numbers (A006037).",
				"From _Daniel Forgues_, Feb 07 2011: (Start)",
				"The first odd pseudoperfect number is a(233) = 945.",
				"An empirical observation (from the graph) is that it seems that the n-th pseudoperfect number would be asymptotic to 4n, or equivalently that the asymptotic density of pseudoperfect numbers would be 1/4. Any proof of this? (End)",
				"A065205(a(n)) \u003e 0; A210455(a(n)) = 1. - _Reinhard Zumkeller_, Jan 21 2013",
				"Deléglise (1998) shows that abundant numbers have asymptotic density \u003c 0.2480, resolving the question which he attributes to Henri Cohen of whether the abundant numbers have density greater or less than 1/4. The density of pseudoperfect numbers is the difference between the densities of abundant numbers (A005101) and weird numbers (A006037), since the remaining integers are perfect numbers (A000396), which have density 0. Using the first 22 primitive pseudoperfect numbers (A006036) and the fact that every multiple of a pseudoperfect number is pseudoperfect it can be shown that the density of pseudoperfect numbers is \u003e 0.23790. - _Jaycob Coleman_, Oct 26 2013",
				"The odd terms of this sequence are given by the odd abundant numbers A005231, up to hypothetical (so far unknown) odd weird numbers (A006037). - _M. F. Hasler_, Nov 23 2017",
				"The term \"pseudoperfect numbers\" was coined by Sierpiński (1965). The alternative term \"semiperfect numbers\" was coined by Zachariou and Zachariou (1972). - _Amiram Eldar_, Dec 04 2020"
			],
			"reference": [
				"Richard K. Guy, Unsolved Problems in Number Theory, 3rd edition, Springer, 2004, Section B2, pp. 74-75.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A005835/b005835.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (terms 1..1000 from T. D. Noe)",
				"Anonymous, \u003ca href=\"http://www-maths.swan.ac.uk/pgrads/bb/project/node36.html\"\u003eSemiperfect Numbers: Definition\u003c/a\u003e [Broken link]",
				"Stan Benkoski, \u003ca href=\"http://dx.doi.org/10.2307%2F2316276\"\u003eProblem E2308\u003c/a\u003e, Amer. Math. Monthly, Vol. 79, No. 7 (1972), p. 774.",
				"S. J. Benkoski and P. Erdős, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1974-0347726-9\"\u003eOn weird and pseudoperfect numbers\u003c/a\u003e, Math. Comp., Vol. 28, No. 126 (1974), pp. 617-623. \u003ca href=\"https://doi.org/10.1090/S0025-5718-75-99676-3\"\u003eCorrigendum\u003c/a\u003e, Math. Comp., Vol. 29, No. 130 (1975), pp. 673-674.",
				"David Eppstein, \u003ca href=\"http://www.ics.uci.edu/~eppstein/numth/egypt/odd-one.html\"\u003eIs it known whether a group of Egyptian fractions with odd, distinct denominators can add up to 1?\u003c/a\u003e, 1996.",
				"Richard K. Guy, \u003ca href=\"https://doi.org/10.1007/978-0-387-26677-0\"\u003eUnsolved Problems in Number Theory\u003c/a\u003e, 3rd edition, Springer, 2004, Section B2, pp. 74-75.",
				"Wacław Sierpiński, \u003ca href=\"https://eudml.org/doc/259169\"\u003eSur les nombres pseudoparfaits\u003c/a\u003e, Matematički Vesnik, Vol. 2 (17), No. 33 (1965), pp. 212-213.",
				"Jonathan Sondow and Kieren MacMillan, \u003ca href=\"http://www.jstor.org/stable/10.4169/amer.math.monthly.124.3.232\"\u003ePrimary pseudoperfect numbers, arithmetic progressions, and the Erdős-Moser equation\u003c/a\u003e, Amer. Math. Monthly, Vol. 124, No. 3 (2017), pp. 232-240; \u003ca href=\"http://arxiv.org/abs/1812.06566\"\u003earXiv:math preprint\u003c/a\u003e, arXiv:math/1812.06566 [math.NT], 2018.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/SemiperfectNumber.html\"\u003eSemiperfect Number\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Semiperfect_number\"\u003eSemiperfect number\u003c/a\u003e.",
				"Andreas Zachariou and Eleni Zachariou, \u003ca href=\"http://www.hms.gr/apothema/?s=sa\u0026amp;i=261\"\u003ePerfect, Semi-Perfect and Ore Numbers\u003c/a\u003e,\" Bull. Soc. Math. Grèce (New Ser.), Vol. 13, No. 13A (1972), pp. 12-22; \u003ca href=\"https://eudml.org/doc/238923\"\u003ealternative link\u003c/a\u003e."
			],
			"example": [
				"6 = 1+2+3, 12 = 1+2+3+6, 18 = 3+6+9, etc.",
				"70 is not a member since the proper divisors of 70 are {1, 2, 5, 7, 10, 14, 35} and no subset adds to 70."
			],
			"maple": [
				"with(combinat):",
				"isA005835 := proc(n)",
				"    local b, S;",
				"    b:=false;",
				"    S:=subsets(numtheory[divisors](n) minus {n});",
				"    while not S[finished] do",
				"        if convert(S[nextvalue](), `+`)=n then",
				"            b:=true;",
				"            break",
				"        end if ;",
				"    end do;",
				"    b",
				"end proc:",
				"for n from 1 do",
				"    if isA005835(n) then",
				"        print(n);",
				"    end if;",
				"end do: # _Walter Kehowski_, Aug 12 2005"
			],
			"mathematica": [
				"A005835 = Flatten[ Position[ A033630, q_/; q\u003e1 ] ] (* _Wouter Meeussen_ *)",
				"pseudoPerfectQ[n_] := Module[{divs = Most[Divisors[n]]}, MemberQ[Total/@Subsets[ divs, Length[ divs]], n]]; A005835 = Select[Range[300],pseudoPerfectQ] (* _Harvey P. Dale_, Sep 19 2011 *)",
				"A005835 = {}; n = 0; While[Length[A005835] \u003c 100, n++; d = Most[Divisors[n]]; c = SeriesCoefficient[Series[Product[1 + x^d[[i]], {i, Length[d]}], {x, 0, n}], n]; If[c \u003e 0, AppendTo[A005835, n]]]; A005835 (* _T. D. Noe_, Dec 29 2011 *)"
			],
			"program": [
				"(PARI) is_A005835(n, d=divisors(n)[^-1], s=vecsum(d), m=#d)={ m||return; while(d[m]\u003en, s-=d[m]; m--||return); d[m]==n || if(n\u003cs, is_A005835(n-d[m], d, s-d[m], m-1) || is_A005835(n, d, s-d[m], m-1), n==s)} \\\\ Returns nonzero iff n is the sum of a subset of d, which defaults to the set of proper divisors of n. Improved using more recent PARI syntax by _M. F. Hasler_, Jul 15 2016, Jul 27 2016. NOTE: This function is also used (with 2nd optional arg) in A136446, A122036 and possibly in A006037. - _M. F. Hasler_, Jul 28 2016",
				"for(n=1,1000,is_A005835(n)\u0026\u0026print1(n\",\")) \\\\ _M. F. Hasler_, Apr 06 2008",
				"(Haskell)",
				"a005835 n = a005835_list !! (n-1)",
				"a005835_list = filter ((== 1) . a210455) [1..]",
				"-- _Reinhard Zumkeller_, Jan 21 2013"
			],
			"xref": [
				"Subsequence of A023196; complement of A136447.",
				"See A136446 for another version.",
				"Cf. A006036, A005100, A033630, A000396, A005231.",
				"Cf. A109761 (subsequence)."
			],
			"keyword": "nonn,nice,easy",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Better description and more terms from _Jud McCranie_, Oct 15 1997"
			],
			"references": 60,
			"revision": 122,
			"time": "2021-07-09T01:33:44-04:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}