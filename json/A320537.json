{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A320537",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 320537,
			"data": "2,4,10,6,14,50,8,22,70,230,12,26,98,250,1150,16,34,110,290,1250,5050,18,38,130,310,1450,5150,22310,20,44,154,370,1550,5290,23230,106030,24,46,170,406,1850,5350,23690,106490,510050,28,52,182,410,2030,5450,24610,107410,513130,2065450",
			"name": "Square array read by antidiagonals in which T(n,k) is the n-th even number j with the property that the symmetric representation of sigma(j) has k parts.",
			"comment": [
				"This is a permutation of the positive even numbers (A299174).",
				"The union of all odd-indexed columns gives A319796, the even numbers in A071562.",
				"The union of all even-indexed columns gives A319802, the even numbers in A071561."
			],
			"example": [
				"From _Hartmut F. W. Hoft_, Oct 06 2021: (Start)",
				"The 10x10 section of table T(n,k):",
				"(Table with first 20 terms from _Omar E. Pol_)",
				"------------------------------------------------------------------",
				"n\\k | 1   2   3    4    5     6     7      8       9       10  ...",
				"------------------------------------------------------------------",
				"  1 | 2   10  50   230  1150  5050  22310  106030  510050  2065450",
				"  2 | 4   14  70   250  1250  5150  23230  106490  513130  2115950",
				"  3 | 6   22  98   290  1450  5290  23690  107410  520150  2126050",
				"  4 | 8   26  110  310  1550  5350  24610  110170  530150  2157850",
				"  5 | 12  34  130  370  1850  5450  25070  112010  530450  2164070",
				"  6 | 16  38  154  406  2030  5650  25250  112930  532450  2168150",
				"  7 | 18  44  170  410  2050  5750  25750  114770  534290  2176550",
				"  8 | 20  46  182  430  2150  6250  25990  115690  537050  2186650",
				"  9 | 24  52  190  434  2170  6350  26450  116150  540350  2216950",
				"  10| 28  58  238  470  2350  6550  26750  117070  544870  2219650",
				"   ... (End)"
			],
			"mathematica": [
				"(* function a341969 is defined in A341969 *)",
				"sArray[b_, pMax_] := Module[{list=Table[{}, pMax], i, p}, For[i=2, i\u003c=b, i+=2, p=Length[Select[SplitBy[a341969[i], #!=0\u0026], #[[1]]!=0\u0026]]; If[p\u003c=pMax\u0026\u0026Length[list[[p]]]\u003cpMax, AppendTo[list[[p]], i]]]; list]",
				"rank[n_] := n-row[n-1](row[n-1]+1)/2",
				"parts[n_] := row[n-1]-rank[n]+2",
				"a320537[sMatrix_, aD_] := Map[sMatrix[[rank[#], parts[#]]]\u0026, Range[aD (aD+1)/2]]/; MatrixQ[sMatrix]\u0026\u0026aD\u003c=Length[sMatrix]",
				"m2500000=sArray[2500000, 10] (* entire 10x10 matrix needs to be computed *)",
				"a320537[m2500000, 10] (* Sequence Data a(1..55) *)",
				"(* _Hartmut F. W. Hoft_, Oct 06 2021 *)"
			],
			"xref": [
				"Row 1 is A320521.",
				"Column 1 gives A174973 = A238443, without the 1.",
				"Column 2 gives A244894.",
				"Cf. A000203, A071561, A071562, A236104, A237270, A237271, A237593, A238443, A239663, A239665, A239929, A240062, A245092, A262626, A299174, A319796, A319802, A341969, A341970, A341971, A346969, A348171."
			],
			"keyword": "nonn,tabl",
			"offset": "1,1",
			"author": "_Omar E. Pol_, Oct 15 2018",
			"ext": [
				"Terms a(21) and beyond from _Hartmut F. W. Hoft_, Oct 06 2021"
			],
			"references": 3,
			"revision": 35,
			"time": "2021-11-03T22:21:44-04:00",
			"created": "2018-10-15T08:43:32-04:00"
		}
	]
}