{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A196047",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 196047,
			"data": "0,1,3,2,6,4,5,3,6,7,10,5,8,6,9,4,9,7,7,8,8,11,11,6,12,9,9,7,12,10,15,5,13,10,11,8,10,8,11,9,13,9,11,12,12,12,15,7,10,13,12,10,9,10,16,8,10,13,14,11,13,16,11,6,14,14,12,11,14,12,14,9,14,11,15,9,15,12,17,10,12,14,17,10,15,12,15,13,12,13,13,13,18,16,13,8,19,11,16,14",
			"name": "Path length of the rooted tree with Matula-Goebel number n.",
			"comment": [
				"The path length of a rooted tree is defined as the sum of distances of all nodes to the root of the tree.",
				"The Matula-Goebel number of a rooted tree is defined in the following recursive manner: to the one-vertex tree there corresponds the number 1; to a tree T with root degree 1 there corresponds the t-th prime number, where t is the Matula-Goebel number of the tree obtained from T by deleting the edge emanating from the root; to a tree T with root degree m\u003e=2 there corresponds the product of the Matula-Goebel numbers of the m branches of T."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A196047/b196047.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Emeric Deutsch, \u003ca href=\"http://arxiv.org/abs/1111.4288\"\u003eTree statistics from Matula numbers\u003c/a\u003e, arXiv preprint arXiv:1111.4288 [math.CO], 2011.",
				"F. Goebel, \u003ca href=\"http://dx.doi.org/10.1016/0095-8956(80)90049-0\"\u003eOn a 1-1-correspondence between rooted trees and natural numbers\u003c/a\u003e, J. Combin. Theory, B 29 (1980), 141-143.",
				"I. Gutman and A. Ivic, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(95)00182-V\"\u003eOn Matula numbers\u003c/a\u003e, Discrete Math., 150, 1996, 131-142.",
				"I. Gutman and Yeong-Nan Yeh, \u003ca href=\"http://www.emis.de/journals/PIMB/067/3.html\"\u003eDeducing properties of trees from their Matula numbers\u003c/a\u003e, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"D. Matula, \u003ca href=\"http://www.jstor.org/stable/2027327\"\u003eA natural rooted tree enumeration by prime factorization\u003c/a\u003e, SIAM Rev. 10 (1968) 273.",
				"\u003ca href=\"/index/Mat#matula\"\u003eIndex entries for sequences related to Matula-Goebel numbers\u003c/a\u003e"
			],
			"formula": [
				"a(1)=0; if n=p(t) (= the t-th prime) then a(n)=a(t)+N(t), where N(t) is the number of nodes of the rooted tree with Matula number t; if n=rs (r,s\u003e=2), then a(n)=a(r)+a(s). The Maple program is based on this recursive formula.",
				"a(n) = A196048(n) + A343006(n). - _François Marques_, Apr 02 2021"
			],
			"example": [
				"a(7) = 5 because the rooted tree with Matula-Goebel number 7 is the rooted tree Y (0+1+2+2 = 5).",
				"a(2^m) = m because the rooted tree with Matula-Goebel number 2^m is a star with m edges."
			],
			"maple": [
				"with(numtheory): a := proc (n) local r, s, N: r := proc (n) options operator, arrow: op(1, factorset(n)) end proc: s := proc (n) options operator, arrow: n/r(n) end proc: N := proc (n) if n = 1 then 1 elif bigomega(n) = 1 then 1+N(pi(n)) else N(r(n))+N(s(n))-1 end if end proc: if n = 1 then 0 elif bigomega(n) = 1 then a(pi(n))+N(pi(n)) else a(r(n))+a(s(n)) end if end proc: seq(a(n), n = 1 .. 100);"
			],
			"program": [
				"(Haskell)",
				"import Data.List (genericIndex)",
				"a196047 n = genericIndex a196047_list (n - 1)",
				"a196047_list = 0 : g 2 where",
				"   g x = y : g (x + 1) where",
				"     y = if t \u003e 0 then a196047 t + a061775 t else a196047 r + a196047 s",
				"         where t = a049084 x; r = a020639 x; s = x `div` r",
				"-- _Reinhard Zumkeller_, Sep 03 2013",
				"(PARI) NPl(n) = { if(n==1, return([1,0]),",
				"    my(f=factor(n)~, v=Mat(vector(#f,k,NPl(primepi(f[1,k]))~))  );",
				"    return( [ 1+sum(k=1,#f,v[1,k]*f[2,k]) , sum(k=1,#f,(v[1,k]+v[2,k])*f[2,k]) ] ) )",
				"  };",
				"A196047(n) = NPl(n)[2]; \\\\ _François Marques_, Apr 02 2021"
			],
			"xref": [
				"Cf. A049084, A020639.",
				"Cf. A196048, A343006."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Emeric Deutsch_, Sep 27 2011",
			"references": 7,
			"revision": 23,
			"time": "2021-04-25T22:15:53-04:00",
			"created": "2011-09-27T13:04:05-04:00"
		}
	]
}