{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A129194",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 129194,
			"data": "0,1,2,9,8,25,18,49,32,81,50,121,72,169,98,225,128,289,162,361,200,441,242,529,288,625,338,729,392,841,450,961,512,1089,578,1225,648,1369,722,1521,800,1681,882,1849,968,2025,1058,2209,1152,2401,1250,2601,1352",
			"name": "a(n) = n^2*(3/4 - (-1)^n/4).",
			"comment": [
				"The numerator of the integral is 2,1,2,1,2,1....; the moments of the integral are 2/(n+1)^2.",
				"The sequence alternates between twice a square and an odd square, A001105(n) and A016754(n).",
				"Partial sums of the positive elements give the absolute values of A122576. - _Omar E. Pol_, Aug 22 2011",
				"Partial sums of the positive elements give A212760. - _Omar E. Pol_, Dec 28 2013",
				"Conjecture: denominator of 4/n - 2/n^2. - _Wesley Ivan Hurt_, Jul 11 2016",
				"Multiplicative because both A000290 and A040001 are. - _Andrew Howroyd_, Jul 25 2018"
			],
			"reference": [
				"G. Pólya and G. Szegő, Problems and Theorems in Analysis II (Springer 1924, reprinted 1976), Part Eight, Chap. 1, Sect. 7, Problem 73."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A129194/b129194.txt\"\u003eTable of n, a(n) for n = 0..960\u003c/a\u003e",
				"John M. Campbell, \u003ca href=\"http://arxiv.org/abs/1105.3399\"\u003eAn Integral Representation of Kekulé Numbers, and Double Integrals Related to Smarandache Sequences\u003c/a\u003e, arXiv preprint arXiv:1105.3399 [math.GM], 2011.",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,3,0,-3,0,1)."
			],
			"formula": [
				"G.f.: x*(1 + 2*x + 6*x^2 + 2*x^3 + x^4)/(1-x^2)^3; a(n+1) = denominator((1/(2*Pi))*Integral_{t=0..2*Pi} exp(i*n*t)(-((Pi-t)/i)^2)), i=sqrt(-1).",
				"a(n) = 3*a(n-2) - 3*a(n-4) + a(n-6) for n \u003e 5. - _Paul Curtz_, Mar 07 2011",
				"a(n) is the numerator of the coefficient of x^4 in the Maclaurin expansion of exp(-n*x^2). - _Francesco Daddi_, Aug 04 2011",
				"O.g.f. as a Lambert series: x*Sum_{n \u003e= 1} J_2(n)*x^n/(1 + x^n), where J_2(n) denotes the Jordan totient function A007434(n). See Pólya and Szegő. - _Peter Bala_, Dec 28 2013",
				"From _Ilya Gutkovskiy_, Jul 11 2016: (Start)",
				"E.g.f.: x*((2*x + 1)*sinh(x) + (x + 2)*cosh(x))/2.",
				"Sum_{n\u003e=1} 1/a(n) = 7*Pi^2/48 = 1.43931730849219813191336327081... (End)",
				"a(n) = A000290(n) / A040001(n). - _Andrew Howroyd_, Jul 25 2018"
			],
			"maple": [
				"A129194:=n-\u003en^2*(3/4 - (-1)^n/4): seq(A129194(n), n=0..80); # _Wesley Ivan Hurt_, Jul 11 2016"
			],
			"mathematica": [
				"Table[n^2*(3/4 - (-1)^n/4), {n, 0, 60}] (* _Wesley Ivan Hurt_, Jul 11 2016 *)"
			],
			"program": [
				"(MAGMA) [n^2*(3/4-(-1)^n/4): n in [0..50]]; // _Vincenzo Librandi_, Apr 26 2011",
				"(PARI) a(n) = lcm(2, n^2)/2; \\\\ _Andrew Howroyd_, Jul 25 2018"
			],
			"xref": [
				"Cf. A016742, A010713, A105398, A152020, A000290, A061038, A061040, A061050. - _Paul Curtz_, Nov 21 2008",
				"Cf. A040001, A129204."
			],
			"keyword": "easy,frac,nonn,mult",
			"offset": "0,3",
			"author": "_Paul Barry_, Apr 02 2007",
			"ext": [
				"More terms from _Michel Marcus_, Dec 28 2013"
			],
			"references": 13,
			"revision": 60,
			"time": "2019-12-24T21:42:36-05:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}