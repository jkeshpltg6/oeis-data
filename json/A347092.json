{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A347092",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 347092,
			"data": "1,-4,-6,5,-10,24,-14,-4,10,40,-22,-30,-26,56,60,5,-34,-40,-38,-50,84,88,-46,24,26,104,-6,-70,-58,-240,-62,-4,132,136,140,50,-74,152,156,40,-82,-336,-86,-110,-100,184,-94,-30,50,-104,204,-130,-106,24,220,56,228,232,-118,300,-122,248,-140,5,260,-528,-134",
			"name": "Dirichlet inverse of A322577, which is the convolution of Dedekind psi with Euler phi.",
			"comment": [
				"Multiplicative because A322577 is."
			],
			"link": [
				"Sebastian Karlsson, \u003ca href=\"/A347092/b347092.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(1) = 1; a(n) = -Sum_{d|n, d \u003c n} A322577(n/d) * a(d).",
				"a(n) = A347093(n) - A322577(n).",
				"From _Sebastian Karlsson_, Oct 29 2021: (Start)",
				"Dirichlet g.f.: zeta(2*s)/zeta(s-1)^2.",
				"a(n) = Sum_{d|n} A323363(n/d)*A023900(d).",
				"Multiplicative with a(p^e) = 1 + p^2 if e is even, -2*p if e is odd. (End)"
			],
			"program": [
				"(PARI)",
				"up_to = 16384;",
				"A001615(n) = if(1==n,n, my(f=factor(n)); prod(i=1, #f~, f[i, 1]^f[i, 2] + f[i, 1]^(f[i, 2]-1))); \\\\ After code in A001615",
				"A322577(n) = sumdiv(n,d,A001615(n/d)*eulerphi(d));",
				"DirInverseCorrect(v) = { my(u=vector(#v)); u[1] = (1/v[1]); for(n=2, #v, u[n] = (-u[1]*sumdiv(n, d, if(d\u003cn, v[n/d]*u[d], 0)))); (u) }; \\\\ Compute the Dirichlet inverse of the sequence given in input vector v.",
				"v347092 = DirInverseCorrect(vector(up_to,n,A322577(n)));",
				"A347092(n) = v347092[n];",
				"(PARI) A347092(n) = { my(f=factor(n)); prod(i=1, #f~, if(f[i, 2]%2, -2*f[i, 1], 1+(f[i, 1]^2))); }; \\\\ (after _Sebastian Karlsson_'s multiplicative formula) - _Antti Karttunen_, Nov 11 2021",
				"(Haskell)",
				"import Math.NumberTheory.Primes",
				"a n = product . map (\\(p, e) -\u003e if even e then 1 + unPrime p^2 else -2*unPrime p) . factorise $ n -- _Sebastian Karlsson_, Oct 29 2021",
				"(Python)",
				"from sympy import factorint, prod",
				"def f(p, e): return 1 + p**2 if e%2 == 0 else -2*p",
				"def a(n):",
				"    factors = factorint(n)",
				"    return prod(f(p, factors[p]) for p in factors) # _Sebastian Karlsson_, Oct 29 2021"
			],
			"xref": [
				"Cf. A000010, A001615, A322577, A347093.",
				"Cf. A023900, A323363."
			],
			"keyword": "sign,mult",
			"offset": "1,2",
			"author": "_Antti Karttunen_, Aug 18 2021",
			"references": 2,
			"revision": 13,
			"time": "2021-11-14T14:35:48-05:00",
			"created": "2021-08-18T21:44:05-04:00"
		}
	]
}