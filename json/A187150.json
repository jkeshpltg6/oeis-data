{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A187150",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 187150,
			"data": "1,-2,1,-2,0,4,1,2,-5,0,-5,4,1,-2,-5,0,7,4,7,0,-4,-10,7,-8,0,4,0,-8,2,0,1,-2,0,2,0,14,7,0,-5,10,-11,-8,-10,-2,0,10,-4,4,0,0,-5,-8,-11,10,0,0,14,-2,20,0,-11,4,13,2,-5,-14,0,-14,13,0,-11,-14,8,-2,0,10,13,-18,0,0,-5",
			"name": "Expansion of psi(-x)^4 / chi(-x)^2 in powers of x where psi(), chi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A187150/b187150.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-7/12) * (eta(q) * eta(q^4)^2 / eta(q^2))^2 in powers of q.",
				"Euler transform of period 4 sequence [ -2, 0, -2, -4, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (576 t)) = 288 (t/i)^2 g(t) where q = exp(2 Pi i t) and g() is the g.f. for A187149."
			],
			"example": [
				"G.f. = 1 - 2*x + x^2 - 2*x^3 + 4*x^5 + x^6 + 2*x^7 - 5*x^8 - 5*x^10 + ...",
				"G.f. = q^7 - 2*q^19 + q^31 - 2*q^43 + 4*q^67 + q^79 + 2*q^91 - 5*q^103 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (QPochhammer[ x] QPochhammer[ x^4]^2 / QPochhammer[ x^2])^2, {x, 0, n}]; (* _Michael Somos_, Sep 02 2015 *)",
				"a[ n_] := SeriesCoefficient[ (1/4) x^(-1/2) EllipticTheta[ 2, Pi/4, x^(1/2)]^4 / QPochhammer[ x, x^2]^2, {x, 0, n}]; (* _Michael Somos_, Sep 02 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( (eta(x + A) * eta(x^4 + A)^2 / eta(x^2 + A))^2, n))};"
			],
			"xref": [
				"Cf. A187149."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Mar 05 2011",
			"references": 3,
			"revision": 15,
			"time": "2021-03-12T22:24:46-05:00",
			"created": "2011-03-05T22:49:05-05:00"
		}
	]
}