{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A132180",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 132180,
			"data": "1,1,3,1,6,3,12,5,21,10,36,15,60,26,96,39,150,63,228,92,342,140,504,201,732,295,1050,415,1488,591,2088,818,2901,1140,3996,1554,5460,2126,7404,2861,9972,3855,13344,5126,17748,6816,23472,8970,30876,11793,40413",
			"name": "Expansion of f(q, q^2) * f(-q^3) / f(-q^2)^2 in powers of q where f(, ), f() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A132180/b132180.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of eta(q^3)^3 / (eta(q) * eta(q^2) * eta(q^6)) in powers of q.",
				"Euler transform of period 6 sequence [ 1, 2, -2, 2, 1, 0, ...].",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2)) where f(u, v) = (v^2 - 2*u)^3 - u^4 * (2*u - 3*v^2) * (4*u - 3*v^2).",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (6 t)) = (2/3) g(t) where q = exp(2 Pi i t) and g() is the g.f. for A132179.",
				"G.f.: Product_{k\u003e0} (1 + x^k + x^(2*k))^2 / ( (1 + x^k)^2 * (1 - x^k + x^(2*k))).",
				"a(2*n) = A128128(n). a(2*n + 1) =  A132302(n)."
			],
			"example": [
				"G.f. = 1 + q + 3*q^2 + q^3 + 6*q^4 + 3*q^5 + 12*q^6 + 5*q^7 + 21*q^8 + 10*q^9 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ q^3]^3 / (QPochhammer[ q] QPochhammer[ q^2] QPochhammer[ q^6]), {q, 0, n}]; (* _Michael Somos_, Apr 26 2015 *)",
				"a[ n_] := SeriesCoefficient[ QPochhammer[ -q, q^3] QPochhammer[ -q^2, q^3] QPochhammer[ q^3]^2 / QPochhammer[ q^2]^2, {q, 0, n}]; (* _Michael Somos_, Nov 01 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^3 + A)^3 / (eta(x + A) * eta(x^2 + A) * eta(x^6 + A)), n))};"
			],
			"xref": [
				"Cf. A128128, A132179, A132302."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Michael Somos_, Aug 12 2007",
			"references": 4,
			"revision": 12,
			"time": "2021-03-12T22:24:44-05:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}