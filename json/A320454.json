{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A320454",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 320454,
			"data": "1,2,3,4,6,5,7,8,9,10,12,15,14,11,13,16,18,20,21,22,24,25,27,30,28,32,36,40,35,33,26,17,19,23,29,31,34,38,39,42,44,45,48,50,49,54,60,56,55,52,51,57,46,58,62,37,41,43,47,53,59,61,63,64,72,75,70",
			"name": "Lexicographically earliest sequence of distinct positive terms such that a(1) = 1, a(2) = 2, and for any n \u003e 2, the greatest prime factor of a(n) does not exceed the prime next to the greatest prime factor of a(n-1).",
			"comment": [
				"More formally, for any n \u003e 0, A061395(a(n+1)) \u003c= A061395(a(n)) + 1.",
				"This sequence is a permutation of the natural numbers, with inverse A320455:",
				"- by induction: for any k \u003e 0, every number with greatest prime factor prime(k) (where prime(k) denotes the k-th prime number) appear in the sequence:",
				"- for k = 1: we can always choose a number with greatest prime factor 2, so eventually every number with greatest prime factor 2 will appear in the sequence,",
				"- for any k \u003e 1: provided every number with greatest prime factor prime(k) appear in the sequence: after a number with greatest prime factor prime(k), say w, we can always choose a number \u003c w with greatest prime factor prime(k+1), so eventually every number with greatest prime factor prime(k+1) will appear in the sequence, QED.",
				"The prime numbers appear in ascending order as clusters in the sequence; the first prime clusters are:",
				"- 2 terms: a(2) = 2, a(3) = 3,",
				"- 2 terms: a(6) = 5, a(7) = 7,",
				"- 2 terms: a(14) = 11, a(15) = 13,",
				"- 5 terms: a(32) = 17, ..., a(36) = 31,",
				"- 7 terms: a(56) = 37, ..., a(62) = 61,",
				"- 14 terms: a(139) = 67, ..., a(152) = 131,",
				"- 26 terms: a(343) = 137, ..., a(368) = 271,",
				"- 43 terms: a(745) = 277, ..., a(787) = 547,",
				"- 85 terms: a(1893) = 557, ..., a(1977) = 1109,",
				"- 145 terms: a(3963) = 1117, ..., a(4107) = 2221,",
				"- 276 terms: a(10047) = 2237, ..., a(10322) = 4463,",
				"- 506 terms: a(24973) = 4481, ..., a(25478) = 8951,",
				"- 942 terms: a(44952) = 8963, ..., a(45893) = 17923.",
				"See A320503 for a similar sequence."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A320454/b320454.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A320454/a320454.png\"\u003eScatterplot of the first 50000 terms\u003c/a\u003e (with prime terms highlighted)",
				"Rémy Sigrist, \u003ca href=\"/A320454/a320454.gp.txt\"\u003ePARI program for A320454\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"example": [
				"The first terms, alongside the greatest prime factor of a(n) and A061395(a(n)), are:",
				"  n   a(n)  gpf(a(n))  A320454(a(n))",
				"  --  ----  ---------  -------------",
				"   1     1        N/A              0",
				"   2     2          2              1",
				"   3     3          3              2",
				"   4     4          2              1",
				"   5     6          3              2",
				"   6     5          5              3",
				"   7     7          7              4",
				"   8     8          2              1",
				"   9     9          3              2",
				"  10    10          5              3",
				"  11    12          3              2",
				"  12    15          5              3",
				"  13    14          7              4",
				"  14    11         11              5",
				"  15    13         13              6"
			],
			"mathematica": [
				"Nest[Append[#, Block[{k = 3, p}, While[Nand[Set[p, FactorInteger[k][[-1, 1]]] \u003c= NextPrime[#[[-1, -1]] ], FreeQ[#[[All, 1]], k ]], k++]; {k, p}]] \u0026, {{1, 1}, {2, 2}}, 65][[All, 1]] (* _Michael De Vlieger_, Oct 17 2018 *)"
			],
			"program": [
				"(PARI) See Links section."
			],
			"xref": [
				"Cf. A061395, A320455 (inverse), A320503."
			],
			"keyword": "nonn,look",
			"offset": "1,2",
			"author": "_Rémy Sigrist_, Oct 13 2018",
			"references": 3,
			"revision": 20,
			"time": "2018-10-18T14:54:04-04:00",
			"created": "2018-10-17T21:46:50-04:00"
		}
	]
}