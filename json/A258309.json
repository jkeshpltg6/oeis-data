{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A258309",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 258309,
			"data": "1,1,1,1,1,2,1,1,3,4,1,1,4,7,9,1,1,5,10,23,21,1,1,6,13,43,71,51,1,1,7,16,69,151,255,127,1,1,8,19,101,261,703,911,323,1,1,9,22,139,401,1485,2983,3535,835,1,1,10,25,183,571,2691,6973,14977,13903,2188",
			"name": "A(n,k) is the sum over all Motzkin paths of length n of products over all peaks p of (k*x_p+y_p)/y_p, where x_p and y_p are the coordinates of peak p; square array A(n,k), n\u003e=0, k\u003e=0, read by antidiagonals.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A258309/b258309.txt\"\u003eAntidiagonals n = 0..140, flattened\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Motzkin_number\"\u003eMotzkin number\u003c/a\u003e"
			],
			"formula": [
				"A(n,k) = Sum_{i=0..min(floor(n/2),k)} C(k,i) * i! * A258310(n,i)."
			],
			"example": [
				"Square array A(n,k) begins:",
				":  1,   1,   1,    1,    1,    1,    1, ...",
				":  1,   1,   1,    1,    1,    1,    1, ...",
				":  2,   3,   4,    5,    6,    7,    8, ...",
				":  4,   7,  10,   13,   16,   19,   22, ...",
				":  9,  23,  43,   69,  101,  139,  183, ...",
				": 21,  71, 151,  261,  401,  571,  771, ...",
				": 51, 255, 703, 1485, 2691, 4411, 6735, ..."
			],
			"maple": [
				"b:= proc(x, y, t, k) option remember; `if`(y\u003ex or y\u003c0, 0,",
				"      `if`(x=0, 1, b(x-1, y-1, false, k)*`if`(t, (k*x+y)/y, 1)",
				"                  +b(x-1, y, false, k) +b(x-1, y+1, true, k)))",
				"    end:",
				"A:= (n, k)-\u003e b(n, 0, false, k):",
				"seq(seq(A(n, d-n), n=0..d), d=0..12);"
			],
			"mathematica": [
				"b[x_, y_, t_, k_] := b[x, y, t, k] = If[y \u003e x || y \u003c 0, 0, If[x == 0, 1, b[x - 1, y - 1, False, k]*If[t, (k*x + y)/y, 1] + b[x - 1, y, False, k] + b[x - 1, y + 1, True, k]]];",
				"A[n_, k_] := b[n, 0, False, k];",
				"Table[A[n, d - n], {d, 0, 12}, {n, 0, d}] // Flatten (* _Jean-François Alcover_, May 04 2017, translated from Maple *)"
			],
			"xref": [
				"Columns k=0-1 give: A001006, A140456(n+2).",
				"Main diagonal gives A261785.",
				"Cf. A258306, A258310."
			],
			"keyword": "nonn,tabl",
			"offset": "0,6",
			"author": "_Alois P. Heinz_, May 25 2015",
			"references": 4,
			"revision": 15,
			"time": "2017-05-04T08:23:46-04:00",
			"created": "2015-05-26T17:28:27-04:00"
		}
	]
}