{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A283151",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 283151,
			"data": "1,6,-1,45,-15,1,360,-180,24,-1,2970,-1980,396,-33,1,24948,-20790,5544,-693,42,-1,212058,-212058,70686,-11781,1071,-51,1,1817640,-2120580,848232,-176715,21420,-1530,60,-1,15677145,-20902860,9754668,-2438667,369495,-35190,2070,-69,1,135868590,-203802885",
			"name": "Triangle read by rows: Riordan array (1/(1-9x)^(2/3), x/(9x-1)).",
			"comment": [
				"This is an example of a Riordan group involution.",
				"Dual Riordan array of A283150.",
				"With A283150 and A248324, forms doubly infinite Riordan array. For b and c the sequences A283150 and A248324, respectively, and i,j \u003e= 0, the doubly infinite array with d(i,j) = a(i,j), d(-j,-i) = b(i,j), d(i,-j) = c(i,j), and d(-i,j) = 0 (except d(0,0)=1) is a doubly infinite Riordan array."
			],
			"link": [
				"Peter Bala, \u003ca href=\"/A264772/a264772_1.pdf\"\u003eA 4-parameter family of embedded Riordan arrays\u003c/a\u003e",
				"Peter Bala, \u003ca href=\"/A081577/a081577.pdf\"\u003eA note on the diagonals of a proper Riordan Array\u003c/a\u003e",
				"H. Prodinger, \u003ca href=\"https://www.fq.math.ca/Scanned/32-5/prodinger.pdf\"\u003eSome information about the binomial transform\u003c/a\u003e, The Fibonacci Quarterly, 32, 1994, 412-415.",
				"Thomas M. Richardson, \u003ca href=\"http://arxiv.org/abs/1609.01193\"\u003eThe three 'R's and Dual Riordan Arrays\u003c/a\u003e, arXiv:1609.01193 [math.CO], 2016."
			],
			"formula": [
				"a(m,n) = binomial(-n-2/3, m-n)*(-1)^m*9^(m-n).",
				"G.f.: (1-9x)^(1/3)/(xy-9x+1).",
				"Recurrence: a(m,n) = a(m,n-1)*(n-1-m)/(9*n-3) for 0 \u003c n \u003c= m; matrix inverse of a(m,n) is a(m,n). - _Werner Schulte_, Aug 05 2017",
				"From _Peter Bala_, Mar 05 2018 (Start):",
				"Let P(n,x) = Sum_{k = 0..n} T(n,k)*x^(n-k) denote the n-th row polynomial in descending powers of x. Then (-1)^n*P(n,x) is the n-th degree Taylor polynomial of (1 - 9*x)^(n-1/3) about 0. For example, for n = 4 we have (1 - 9*x)^(11/3) = 2970*x^4 - 1980*x^3 + 396*x^2 - 33*x + 1 + O(x^5).",
				"Let R(n,x) denote the n-th row polynomial of this triangle. The polynomial R(n,9*x) has the e.g.f. Sum_{k = 0..n} T(n,k)*(9*x)^k/k!. The e.g.f. for the n-th diagonal of the triangle (starting at n = 0 for the main diagonal) equals exp(-x) * the e.g.f. for the polynomial R(n,9*x). For example, when n = 3 we have exp(-x)*(360 - 180*(9*x) + 24*(9*x)^2/2! - (9*x)^3/3!) = 360 - 1980*x + 5544*x^2/2! - 11781*x^3/3! + 21420*x^4/4! - ....",
				"Let F(x) = (1 - ( 1 - 9*x)^(1/3))/(3*x). See A025748. The derivatives of F(x) are related to the row polynomials P(n,x) by the identity x^n/n! * (d/dx)^n(F(x)) = 1/(3*x)*( (-1)^n - P(n,x)/(1 - 9*x)^(n-1/3) ), n = 0,1,2,.... Cf. A283151 and A046521. (End)",
				"From _Peter Bala_, Aug 18 2021: (Start)",
				"T(n,k) = (-1)^k*binomial(n-1/3, n-k)*9^(n-k).",
				"Analogous to the binomial transform we have the following sequence transformation formula: g(n) = Sum_{k = 0..n} T(n,k)*b^(n-k)*f(k) iff f(n) = Sum_{k = 0..n} T(n,k)*b^(n-k)*g(k). See Prodinger, bottom of p. 413, with b replaced with 9*b, c = -1 and d = 2/3.",
				"Equivalently, if F(x) = Sum_{n \u003e= 0} f(n)*x^n and G(x) = Sum_{n \u003e= 0} g(n)*x^n are a pair of formal power series then",
				"G(x) = (1/(1 - 9*b*x)^(2/3)) * F(x/(1 - 9*b*x)) iff F(x) = (1/(1 + 9*b*x)^(2/3)) * G(x/(1 + 9*b*x)).",
				"The infinitesimal generator of the unsigned array has the sequence (9*n+6) n\u003e=0 on the main subdiagonal and zeros elsewhere. The m-th power of the unsigned array has entries m^(n-k)*|T(n,k)|. (End)"
			],
			"example": [
				"Triangle begins",
				"         1;",
				"         6,        -1;",
				"        45,       -15,       1;",
				"       360,      -180,      24,       -1;",
				"      2970,     -1980,     396,      -33,      1;",
				"     24948,    -20790,    5544,     -693,     42,     -1;",
				"    212058,   -212058,   70686,   -11781,   1071,    -51,    1;",
				"   1817640,  -2120580,  848232,  -176715,  21420,  -1530,   60,  -1;",
				"  15677145, -20902860, 9754668, -2438667, 369495, -35190, 2070, -69, 1;"
			],
			"xref": [
				"Cf. A004988, A248324, A283150, A025748, A046521."
			],
			"keyword": "sign,tabl,easy",
			"offset": "0,2",
			"author": "_Tom Richardson_, Mar 01 2017",
			"ext": [
				"Offset corrected by _Werner Schulte_, Aug 05 2017"
			],
			"references": 3,
			"revision": 44,
			"time": "2021-09-03T01:59:05-04:00",
			"created": "2017-03-11T21:10:26-05:00"
		}
	]
}