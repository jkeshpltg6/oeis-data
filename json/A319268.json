{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A319268",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 319268,
			"data": "1,2,4,8,3,9,6,16,5,12,7,18,10,24,14,32,36,20,11,25,13,15,33,72,19,21,22,49,26,28,30,64,17,144,38,40,42,45,23,50,52,27,57,60,31,66,34,288,37,39,81,84,43,91,47,98,101,105,54,56,29,120,62,128,132,68",
			"name": "Lexicographically earliest sequence of distinct positive terms such that for any n \u003e 0, the binary representation of n^2 starts with the binary representation of a(n).",
			"comment": [
				"This sequence is a permutation of the natural numbers with inverse A319499.",
				"We can build a variant of this sequence for any base b \u003e 1.",
				"We can build a variant of this sequence for any strictly increasing sequence of nonnegative integers."
			],
			"link": [
				"Ivan Neretin, \u003ca href=\"/A319268/b319268.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A319268/a319268.gp.txt\"\u003ePARI program for A319268\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A319268/a319268.png\"\u003eColored logarithmic scatterplot of the first 10000 terms\u003c/a\u003e (where the color is function of A070939(n^2) - A070939(a(n)))",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"example": [
				"The first terms, alongside the binary representation of n^2 with a(n) in parentheses, are:",
				"  n   a(n)  bin(n^2)",
				"  --  ----  --------",
				"   1     1          (1)",
				"   2     2        (10)0",
				"   3     4       (100)1",
				"   4     8      (1000)0",
				"   5     3      (11)001",
				"   6     9     (1001)00",
				"   7     6     (110)001",
				"   8    16    (10000)00",
				"   9     5    (101)0001",
				"  10    12    (1100)100",
				"  11     7    (111)1001",
				"  12    18   (10010)000",
				"  13    10   (1010)1001",
				"  14    24   (11000)100",
				"  15    14   (1110)0001",
				"  16    32  (100000)000",
				"  17    36  (100100)001",
				"  18    20  (10100)0100",
				"  19    11  (1011)01001",
				"  20    25  (11001)0000"
			],
			"mathematica": [
				"a = {1}; Do[r = IntegerDigits[n^2, 2]; AppendTo[a, Min@Complement[Table[FromDigits[Take[r, k], 2], {k, Length@r}],a]], {n, 2, 66}]; a (* _Ivan Neretin_, Oct 24 2018 *)"
			],
			"program": [
				"(PARI) See Links section."
			],
			"xref": [
				"Cf. A000290, A070939, A272679, A319499."
			],
			"keyword": "nonn,base",
			"offset": "1,2",
			"author": "_Rémy Sigrist_, Sep 16 2018",
			"references": 2,
			"revision": 15,
			"time": "2018-11-09T07:33:45-05:00",
			"created": "2018-09-21T12:30:53-04:00"
		}
	]
}