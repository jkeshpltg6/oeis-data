{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A214284",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 214284,
			"data": "1,1,0,0,1,1,0,0,0,1,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0",
			"name": "Characteristic function of squares or five times squares.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"A195198 is a similar sequence except with three instead of five. - _Michael Somos_, Oct 22 2017"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A214284/b214284.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"S. Cooper and M. Hirschhorn, \u003ca href=\"http://dx.doi.org/10.1216/rmjm/1008959672\"\u003eOn some infinite product identities\u003c/a\u003e, Rocky Mountain J. Math., 31 (2001) 131-139. see p. 134 Theorem 4.",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of f(q, q^9) * f(-q^8, -q^12) / f(-q^4, -q^16) in powers of q where f(, ) is Ramanujan's general theta function.",
				"Expansion of f(q^3, q^7) * f(-q^2, -q^3) / f(-q, -q^4) in powers of q where f(, ) is Ramanujan's general theta function.",
				"Euler transform of period 20 sequence [1, -1, 0, 1, 0, 0, 0, -1, 1, -1, 1, -1, 0, 0, 0, 1, 0, -1, 1, -1, ...].",
				"a(n) is multiplicative with a(0) = a(5^e) = 1, a(p^e) = 1 if e is even, 0 otherwise.",
				"G.f.: (theta_3(q) + theta_3(q^5)) / 2 = 1 + (Sum_{k\u003e0} x^(k^2) + x^(5*k^2)).",
				"Dirichlet g.f.: zeta(2*s) * (1 + 5^-s).",
				"a(4*n + 2) = a(4*n + 3) = 0. a(4*n + 1) = A127693(n). a(5*n) = a(n)."
			],
			"example": [
				"G.f. = 1 + x + x^4 + x^5 + x^9 + x^16 + x^20 + x^25 + x^36 + x^45 + x^49 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ Series[ (EllipticTheta[ 3, 0, q] + EllipticTheta[ 3, 0, q^5]) / 2, {q, 0, n}], {q, 0, n}];",
				"a[ n_] := If[ n \u003c 0, 0, Boole[ OddQ [ Length @ Divisors @ n] || OddQ [ Length @ Divisors[5 n]]]];"
			],
			"program": [
				"(PARI) {a(n) = issquare(n) || issquare(5*n)};",
				"(PARI) {a(n) = if( n\u003c1, n==0, direuler( p=2, n, if( p==5, 1 + X, 1) / (1 - X^2))[n])};"
			],
			"xref": [
				"Cf. A127693, A195198."
			],
			"keyword": "nonn,mult,easy",
			"offset": "0,1",
			"author": "_Michael Somos_, Jul 09 2012",
			"references": 3,
			"revision": 26,
			"time": "2021-03-12T22:24:46-05:00",
			"created": "2012-07-10T10:15:10-04:00"
		}
	]
}