{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A091811",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 91811,
			"data": "1,3,2,10,15,6,35,84,70,20,126,420,540,315,70,462,1980,3465,3080,1386,252,1716,9009,20020,24024,16380,6006,924,6435,40040,108108,163800,150150,83160,25740,3432,24310,175032,556920,1021020,1178100,875160",
			"name": "Array read by rows: T(n,k) = binomial(n+k-2,k-1)*binomial(2*n-1,n-k).",
			"comment": [
				"Alternating sum of elements of n-th row = 1.",
				"If a certain event has a probability p of occurring in any given trial, the probability of its occurring at least n times in 2n-1 trials is Sum_{k=1..n} T(n,k)*(-1)^(k-1)*p^(n+k-1). For example, the probability of its occurring at least 4 out of 7 times is 35p^4 - 84p^5 + 70p^6 - 20p^7. - _Matthew Vandermast_, Jun 05 2004",
				"With the row polynomial defined as R(n,x) = Sum {k = 1..n} T(n,k)*x^k, the row polynomial is related to the regularized incomplete Beta function I_x(a,b), through the relation R(n,x) = -(-x)^{-n+1}*I_{-x}(n,n). - _Leo C. Stein_, Jun 06 2019"
			],
			"formula": [
				"O.g.f.: x*t*(1+2*x-sqrt(1-4*t*(x+1)))/(2*(x+t)*sqrt(1-4*t*(x+1))) = x*t + (3*x+2*x^2)*t^2 + (10*x+15*x^2+6*x^3)*t^3 + .... - _Peter Bala_, Apr 10 2012",
				"Sum {k = 1..n} (-1)^(k-1)*T(n,k)*2^(n-k) = 4^(n-1). Row polynomial R(n+1,x) = (2*n+1)!/n!^2*x*int {y = 0..1} (y*(1+x*y))^n dy. Row sums A178792. - _Peter Bala_, Apr 10 2012"
			],
			"example": [
				"Triangle starts:",
				"    1,",
				"    3,   2,",
				"   10,  15,   6,",
				"   35,  84,  70,  20,",
				"  126, 420, 540, 315, 70,",
				"  ..."
			],
			"mathematica": [
				"t[n_, k_] := Binomial[n+k-2, k-1]*Binomial[2n-1, n-k]; Table[t[n, k], {n, 1, 9}, {k, 1, n}] // Flatten (* _Jean-François Alcover_, Dec 06 2012 *)"
			],
			"program": [
				"(PARI) T(x,y)=binomial(x+y-2,y-1)*binomial(2*x-1,x-y)",
				"(MAGMA) [[Binomial(n+k-2,k-1)*Binomial(2*n-1,n-k): k in [1..n]]: n in [1.. 15]]; // _Vincenzo Librandi_, Jun 15 2015"
			],
			"xref": [
				"Cf. A001700 (first column), A002740 (second column), A000984 (main diagonal), A033876 (second diagonal), A178792 (row sums)."
			],
			"keyword": "nonn,tabl,nice",
			"offset": "1,2",
			"author": "_Benoit Cloitre_, Mar 18 2004",
			"references": 1,
			"revision": 38,
			"time": "2019-06-10T23:36:17-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}