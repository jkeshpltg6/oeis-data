{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A343071",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 343071,
			"data": "0,0,5,0,7,20,27,0,11,28,39,80,91,108,119,0,19,44,63,112,131,156,175,320,339,364,383,432,451,476,495,0,35,76,111,176,211,252,287,448,483,524,559,624,659,700,735,1280,1315,1356,1391,1456,1491,1532,1567,1728,1763,1804,1839,1904,1939,1980,2015",
			"name": "Irregular triangle read by rows: T(n,k) = 2^(2n+1) * L(n,k), where L(n,k) is the k-th left endpoint after the n-th step of removal in the construction of the Smith-Volterra-Cantor set (SVC), 0 \u003c= k \u003c= 2^n - 1.",
			"comment": [
				"The SVC (or fat Cantor set) is a subset of [0, 1] similar to the standard Cantor set， constructed as follows:",
				"We start from C_0 = [0, 1], which is written as [0/2, 2/2] for convenience.",
				"- Step 1: Remove the middle open interval of length 1/4 in C_0 (namely (3/8, 5/8)), leaving the union of 2 closed intervals C_1 = [0/8, 3/8] U [5/8, 8/8];",
				"- Step 2: Remove the middle open interval of length 1/4^2 in each of the closed interval of C_1 (namely (5/32, 7/32) and (27/32, 29/32)), leaving the union of 4 closed intervals C_2 = [0/32, 5/32] U [7/32, 12/32] U [20/32, 27/32] U [29/32, 32/32].",
				"...",
				"- Step n: Remove the middle open interval of length 1/4^n in each of the closed interval of C_{n-1}, leaving the union of 4 closed intervals C_n.",
				"The SVC is thereby defined as the intersection of all {C_n} for n \u003e= 0.",
				"After the n-th step, the k-th left closed interval (from left to right) is [L(n,k), R(n,k)] = [T(n,k)/2^(2n+1), A343072(n,k)/2^(2n+1)].",
				"Like the standard Cantor set, the SVC is perfect (i.e., closed and every point inside is an accumulation point of itself) and has empty interior. The difference between the SVC and the standard Cantor set is that the SVC has positive Lebesgue measure, namely 1 - (1/4) - (1/4^2)*2 - (1/4^3)*2^2 - ... = 1/2.",
				"The construction of the famous Volterra's function (a function differentiable everywhere on [0, 1] whose derivative is bounded yet not Riemann integratable) is based on the SVC."
			],
			"link": [
				"Jianing Song, \u003ca href=\"/A343071/b343071.txt\"\u003eTable of n, a(n) for n = 0..16382\u003c/a\u003e (Rows n = 0..13).",
				"Jianing Song, \u003ca href=\"/A343071/a343071.txt\"\u003eConstruction of SVC after steps 0..13\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.m.wikipedia.org/wiki/Smith-Volterra-Cantor_set\"\u003eSmith-Volterra-Cantor set\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.m.wikipedia.org/wiki/Volterra%27s_function\"\u003eVolterra's function\u003c/a\u003e"
			],
			"formula": [
				"A343072(n,k) - T(n,k) = 2^n + 1, which corresponds to the fact that each closed interval of C_n is of length (2^n + 1)/2^(2n+1).",
				"For n \u003e= 0, 0 \u003c= k \u003c= 2^(n-1) - 1, T(n,2k) = 4*T(n-1,k), T(n,2k+1) = 4*A343072(n-1,k) - (2^n + 1) = 4*T(n-1,k) + (2^n + 3).",
				"For k = Sum_{i=0..n-1} (b_i) * 2^i, b_i = 0 or 1, T(n,k) = Sum_{i=0..n-1} (b_i) * 4^i * (2^(n-i) + 3).",
				"T(n,k) = 2^(2n+1) - A343072(n,(2^n-1)-k)."
			],
			"example": [
				"Table begins",
				"0,",
				"0, 5,",
				"0, 7, 20, 27,",
				"0, 11, 28, 39, 80, 91, 108, 119,",
				"0, 19, 44, 63, 112, 131, 156, 175, 320, 339, 364, 383, 432, 451, 476, 495,",
				"0, 35, 76, 111, 176, 211, 252, 287, 448, 483, 524, 559, 624, 659, 700, 735, 1280, 1315, 1356, 1391, 1456, 1491, 1532, 1567, 1728, 1763, 1804, 1839, 1904, 1939, 1980, 2015,",
				"...",
				"After the n-th step of construction, we have",
				"C_0 = [0/2, 2/2],",
				"C_1 = [0/8, 3/8] U [5/8, 8/8],",
				"C_2 = [0/32, 5/32] U [7/32, 12/32] U [20/32, 27/32] U [29/32, 32/32],",
				"C_3 = [0/128, 9/128] U [11/128, 20/128] U [28/128, 37/128] U [39/128, 48/128] U [80/128, 89/128] U [91/128, 100/128] U [108/128, 117/128] U [119/128, 128/128],",
				"..."
			],
			"program": [
				"(PARI) T(n,k) = my(t=0); for(i=0, n-1, t+=(k%2)*4^i*(2^(n-i)+3); k=k\\2); t"
			],
			"xref": [
				"Cf. A343072 (the right endpoints)."
			],
			"keyword": "nonn,tabf,easy",
			"offset": "0,3",
			"author": "_Jianing Song_, Apr 04 2021",
			"references": 2,
			"revision": 25,
			"time": "2021-05-17T04:33:00-04:00",
			"created": "2021-04-04T10:02:19-04:00"
		}
	]
}