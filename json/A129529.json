{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A129529",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 129529,
			"data": "1,3,6,3,10,8,8,1,15,15,21,18,9,3,21,24,39,45,48,30,24,9,3,28,35,62,82,107,108,101,81,62,37,17,8,1,36,48,90,129,186,222,264,252,255,219,183,126,90,48,27,9,3,45,63,123,186,285,372,492,561,624,648,651,597,537,435",
			"name": "Triangle read by rows: T(n,k) is the number of ternary words of length n on {0,1,2} that have k inversions (n \u003e= 0, k \u003e= 0).",
			"comment": [
				"Row n has 1 + floor(n^2/3) terms.",
				"Row sums are equal to 3^n = A000244(n).",
				"Alternating row sums are 3^(ceiling(n/2)) = A108411(n+1).",
				"T(n,0) = (n+1)*(n+2)/2 = A000217(n+1).",
				"Sum_{k\u003e=0} k*T(n,k) = 3^(n-1)*n*(n-1)/2 = A129530(n).",
				"This sequence is mentioned in the Andrews-Savage-Wilf paper. - _Omar E. Pol_, Jan 30 2012"
			],
			"reference": [
				"M. Bona, Combinatorics of Permutations, Chapman \u0026 Hall/CRC, Boca Raton, FL, 2004, pp. 57-61.",
				"G. E. Andrews, The Theory of Partitions, Addison-Wesley, 1976."
			],
			"link": [
				"G. E. Andrews, C. D. Savage and H. S. Wilf, \u003ca href=\"http://www.math.psu.edu/andrews/pdf/286.pdf\"\u003eHypergeometric identities associated with statistics on words\u003c/a\u003e",
				"Mark A. Shattuck and Carl G. Wagner, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL8/Shattuck2/shattuck44.html\"\u003eParity Theorems for Statistics on Lattice Paths and Laguerre Configurations\u003c/a\u003e, Journal of Integer Sequences, Vol. 8 (2005), Article 05.5.1."
			],
			"formula": [
				"Generating polynomial of row n is Sum_{i=0..n} Sum_{j=0..n-i} binomial[n; i,j,n-i-j], where binomial[n;a,b,c] (a+b+c=n) is a q-multinomial coefficient."
			],
			"example": [
				"T(3,2)=8 because we have 100, 110, 120, 200, 201, 211, 220 and 221.",
				"Triangle starts:",
				"   1;",
				"   3;",
				"   6,  3;",
				"  10,  8,  8,  1;",
				"  15, 15, 21, 18,  9,  3;",
				"  21, 24, 39, 45, 48, 30, 24,  9,  3;"
			],
			"maple": [
				"for n from 0 to 40 do br[n]:=sum(q^i,i=0..n-1) od: for n from 0 to 40 do f[n]:=simplify(product(br[j],j=1..n)) od: mbr:=(n,a,b,c)-\u003esimplify(f[n]/f[a]/f[b]/f[c]): for n from 0 to 9 do G[n]:=sort(simplify(sum(sum(mbr(n,a,b,n-a-b),b=0..n-a),a=0..n))) od: for n from 0 to 9 do seq(coeff(G[n],q,j),j=0..floor(n^2/3)) od; # yields sequence in triangular form"
			],
			"xref": [
				"Cf. A000244, A083906, A108411, A000217, A129530, A129531, A129532."
			],
			"keyword": "nonn,tabf",
			"offset": "0,2",
			"author": "_Emeric Deutsch_, Apr 22 2007",
			"references": 1,
			"revision": 14,
			"time": "2019-11-15T23:18:27-05:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}