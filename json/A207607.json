{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A207607",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 207607,
			"data": "1,1,2,1,5,2,1,9,9,2,1,14,25,13,2,1,20,55,49,17,2,1,27,105,140,81,21,2,1,35,182,336,285,121,25,2,1,44,294,714,825,506,169,29,2,1,54,450,1386,2079,1716,819,225,33,2,1,65,660,2508,4719,5005,3185,1240,289,37,2",
			"name": "Triangle of coefficients of polynomials v(n,x) jointly generated with A207606; see Formula section.",
			"comment": [
				"Subtriangle of the triangle T(n,k) given by (1, 0, 1/2, 1/2, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (0, 2, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Mar 03 2012"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A207607/b207607.txt\"\u003eRows n = 1..100 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"u(n,x) = u(n-1,x) + v(n-1,x), v(n,x) = x*u(n-1,x) + (x+1)v(n-1,x), where u(1,x)=1, v(1,x)=1.",
				"T(n,k) = 2*T(n-1,k) + T(n-1,k-1) - T(n-2,k). - _Philippe Deléham_, Mar 03 2012",
				"G.f.: (1-x+y*x)/(1-(y+2)*x+x^2). - _Philippe Deléham_, Mar 03 2012",
				"For n \u003e= 1, Sum{k=0..n} T(n,k)*x^k = A000012(n), A001906(n), A001834(n-1), A055271(n-1), A038761(n-1), A056914(n-1) for x = 0, 1, 2, 3, 4, 5 respectively. - _Philippe Deléham_, Mar 03 2012",
				"T(n,k) = C(n+k-1,2*k) + 2*C(n+k-1,2*k-1). where C is binomial. - _Yuchun Ji_, May 23 2019",
				"T(n,k) = T(n-1,k) + A207606(n,k-1). - _Yuchun Ji_, May 28 2019",
				"Sum_{k=1..n} T(n, k)*x^k = { 4*(-1)^(n-1)*A016921(n-1) (x=-4), 3*(-1)^(n-1) * A130815(n-1) (x=-3), 2*(-1)^(n-1)*A010684(n-1) (x=-2), A057079(n+1) (x=-1), 0 (x=0), A001906(n) = Fibonacci(2*n) (x=1), 2*A001834(n-1) (x=2), 3*A055271(n-1) (x=3), 4*A038761(n-1) (x=4) }. - _G. C. Greubel_, Mar 15 2020"
			],
			"example": [
				"First five rows:",
				"  1;",
				"  1,  2;",
				"  1,  5,  2;",
				"  1,  9,  9,  2;",
				"  1, 14, 25, 13,  2;",
				"Triangle (1, 0, 1/2, 1/2, 0, 0, 0, ...) DELTA (0, 2, -1, 0, 0, 0, 0, ...) begins:",
				"  1;",
				"  1,  0;",
				"  1,  2,  0;",
				"  1,  5,  2,  0;",
				"  1,  9,  9,  2,  0;",
				"  1, 14, 25, 13,  2,  0;",
				"  1, 20, 55, 49, 17,  2,  0;",
				"  ...",
				"1 = 2*1 - 1, 20 = 2*14 + 1 - 9, 55 = 2*25 + 14 - 9, 49 = 2*13 + 25 - 2, 17 = 2*2 + 1 - 0, 2 = 2*0 + 2 - 0. - _Philippe Deléham_, Mar 03 2012"
			],
			"maple": [
				"A207607:= (n,k) -\u003e `if`(k=1, 1, binomial(n+k-3, 2*k-2) + 2*binomial(n+k-3, 2*k-3) ); seq(seq(A207607(n, k), k = 1..n), n = 1..10); # _G. C. Greubel_, Mar 15 2020"
			],
			"mathematica": [
				"(* First program *)",
				"u[1, x_] := 1; v[1, x_] := 1; z = 16;",
				"u[n_, x_] := u[n - 1, x] + v[n - 1, x]",
				"v[n_, x_] := x*u[n - 1, x] + (x + 1)*v[n - 1, x]",
				"Table[Factor[u[n, x]], {n, 1, z}]",
				"Table[Factor[v[n, x]], {n, 1, z}]",
				"cu = Table[CoefficientList[u[n, x], x], {n, 1, z}];",
				"TableForm[cu]",
				"Flatten[%]  (* A207606 *)",
				"Table[Expand[v[n, x]], {n, 1, z}]",
				"cv = Table[CoefficientList[v[n, x], x], {n, 1, z}];",
				"TableForm[cv]",
				"Flatten[%]  (* A207607 *)",
				"(* Second program *)",
				"Table[If[k==1, 1, Binomial[n+k-3, 2*k-2] + 2*Binomial[n+k-3, 2*k-3]], {n, 10}, {k, n}]//Flatten (* _G. C. Greubel_, Mar 15 2020 *)"
			],
			"program": [
				"(Python)",
				"from sympy import Poly",
				"from sympy.abc import x",
				"def u(n, x): return 1 if n==1 else u(n - 1, x) + v(n - 1, x)",
				"def v(n, x): return 1 if n==1 else x*u(n - 1, x) + (x + 1)*v(n - 1, x)",
				"def a(n): return Poly(v(n, x), x).all_coeffs()[::-1]",
				"for n in range(1, 13): print(a(n)) # _Indranil Ghosh_, May 28 2017",
				"(Sage)",
				"def T(n, k):",
				"    if k == 1: return 1",
				"    else: return binomial(n+k-3, 2*k-2) + 2*binomial(n+k-3, 2*k-3)",
				"[[T(n, k) for k in (1..n)] for n in (1..12)] # _G. C. Greubel_, Mar 15 2020"
			],
			"xref": [
				"Cf. A207606."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Clark Kimberling_, Feb 19 2012",
			"references": 3,
			"revision": 38,
			"time": "2020-04-13T05:56:58-04:00",
			"created": "2012-02-19T21:12:32-05:00"
		}
	]
}