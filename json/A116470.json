{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A116470",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 116470,
			"data": "0,1,2,3,4,5,7,8,11,13,18,21,29,34,47,55,76,89,123,144,199,233,322,377,521,610,843,987,1364,1597,2207,2584,3571,4181,5778,6765,9349,10946,15127,17711,24476,28657,39603,46368,64079,75025,103682,121393,167761",
			"name": "All distinct Fibonacci and Lucas numbers.",
			"comment": [
				"See A115339 for an essentially identical sequence."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A116470/b116470.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex to sequences with linear recurrences with constant coefficients\u003c/a\u003e, signature (0,1,0,1)."
			],
			"formula": [
				"a(0) = 0, a(1) = 1, a(2) = 2, a(3) = 3, a(4) = 4, a(5) = 5, a(6) = 7, a(n) = a(n-2) + a(n-4) for n\u003e6.",
				"a(2*n) = Lucas(n+1) = Fibonacci(n) + Fibonacci(n+2) for n\u003e1.",
				"a(2*n+1) = Fibonacci(n+3) for n\u003e2.",
				"G.f.: -x*(x^2+x+1)*(x^3+x+1)/(-1+x^4+x^2). - Maksym Voznyy (voznyy(AT)mail.ru), Aug 12 2009",
				"a(n) = FL((n + 2 + 3*(n mod 2))/2, n mod 2, 1/2)) for n \u003e= 3. Here FL(n, a, b) = hypergeom([a - n/2, b - n/2], [1 - n], -4). - _Peter Luschny_, Sep 03 2019"
			],
			"maple": [
				"FL := (n, a, b) -\u003e hypergeom([a - n/2, b - n/2], [1 - n], -4):",
				"a := n -\u003e `if`(n \u003c 3, n, FL((n + 2 + 3*irem(n, 2))/2, irem(n, 2), 1/2)):",
				"seq(simplify(a(n)), n=0..52); # _Peter Luschny_, Sep 03 2019"
			],
			"mathematica": [
				"CoefficientList[Series[-x*(x^2 + x + 1)*(x^3 + x + 1)/(-1 + x^4 + x^2), {x, 0, 50}], x] (* _G. C. Greubel_, Dec 21 2017 *)",
				"With[{nn=50},Select[Union[Join[LucasL[Range[0,nn]],Fibonacci[Range[0,nn]]]],#\u003c=200000\u0026]] (* _Harvey P. Dale_, Jul 05 2019 *)"
			],
			"program": [
				"(Haskell)",
				"import Data.List (transpose)",
				"a116470 n = a116470_list !! n",
				"a116470_list = 0 : 1 : 2 : concat",
				"               (transpose [drop 4 a000045_list, drop 3 a000032_list])",
				"-- _Reinhard Zumkeller_, Aug 03 2013",
				"(PARI) x='x+O('x^30); concat([0], Vec(-x*(x^2+x+1)*(x^3+x+1)/( -1+x^4 +x^2))) \\\\ _G. C. Greubel_, Dec 21 2017",
				"(PARI) a(n)=if(n\u003c6, n, if(n%2, fibonacci(n\\2+3), fibonacci(n\\2)+fibonacci(n\\2+2))) \\\\ _Charles R Greathouse IV_, Oct 14 2021"
			],
			"xref": [
				"Union of A000045 and A000032.",
				"Cf. A288219 (even bisection)."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Alexander Adamchuk_, Aug 13 2006",
			"references": 5,
			"revision": 25,
			"time": "2021-10-14T09:10:02-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}