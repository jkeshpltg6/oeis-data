{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005528",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5528,
			"id": "M0950",
			"data": "1,2,4,5,6,9,10,11,12,14,15,16,19,20,22,23,24,25,26,27,28,29,33,34,35,36,37,39,40,42,44,45,48,49,51,52,53,54,56,58,59,60,61,62,63,64,65,66,67,69,71,74,77,78,79,80,81,82,84,85,86,87,88,89,90,92,94,95,96",
			"name": "Størmer numbers or arc-cotangent irreducible numbers: numbers k such that the largest prime factor of k^2 + 1 is \u003e= 2*k.",
			"comment": [
				"Also numbers k such that k^2 + 1 has a primitive divisor, hence (by Everest \u0026 Harman, Theorem 1.4) 1.1n \u003c a(n) \u003c 1.88n for large enough n. They conjecture that a(n) ~ cn where c = 1/log 2 = 1.4426.... - _Charles R Greathouse IV_, Nov 15 2014",
				"Named after the Norwegian mathematician and astrophysicist Carl Størmer (1874-1957). - _Amiram Eldar_, Jun 08 2021"
			],
			"reference": [
				"John H. Conway and R. K. Guy, The Book of Numbers, Copernicus Press, p. 246.",
				"Graham Everest and Glyn Harman, On primitive divisors of n^2 + b, in Number Theory and Polynomials (James McKee and Chris Smyth, ed.), London Mathematical Society 2008.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"John Todd, Table of Arctangents, National Bureau of Standards, Washington, DC, 1951, p. 2."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A005528/b005528.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (terms 1..1000 from T. D. Noe)",
				"Graham Everest and Glyn Harman, \u003ca href=\"http://arxiv.org/abs/math/0701234\"\u003eOn primitive divisors of n^2 + b\u003c/a\u003e, arXiv:math/0701234 [math.NT], 2007.",
				"Carl Størmer, \u003ca href=\"https://archive.org/details/archivformathema1918961897oslo/page/n121/mode/2up\"\u003eSur l'application de la théorie des nombres entiers complexes à la solution en nombres rationnels x_1 x_2... x_n c_1 c_2... c_n, k de l'équation: c_1 arc tg x_1 + c_2 arc tg x_2 + ... + c_n arc tg x_n = k * Pi/4\u003c/a\u003e, Archiv for mathematik og naturvidenskab, Vol. 19, No. 3 (1896), pp. 1-96.",
				"John Todd, \u003ca href=\"http://www.jstor.org/stable/2305526\"\u003eA problem on arc tangent relations\u003c/a\u003e, Amer. Math. Monthly, 56 (1949), 517-528.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/StormerNumber.html\"\u003eStørmer Number\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/St%C3%B8rmer_number\"\u003eStørmer number\u003c/a\u003e."
			],
			"mathematica": [
				"Select[Range[96], FactorInteger[#^2 + 1][[-1, 1]] \u003e= 2 # \u0026] (* _Jean-François Alcover_, Apr 11 2011 *)"
			],
			"program": [
				"(PARI) is(n)=my(f=factor(n^2+1)[,1]);f[#f]\u003e=2*n \\\\ _Charles R Greathouse IV_, Nov 14 2014",
				"(Haskell)",
				"a005528 n = a005528_list !! (n-1)",
				"a005528_list = filter (\\x -\u003e 2 * x \u003c= a006530 (x ^ 2 + 1)) [1..]",
				"-- _Reinhard Zumkeller_, Jun 12 2015",
				"(Python)",
				"from sympy import factorint",
				"def ok(n): return max(factorint(n*n + 1)) \u003e= 2*n",
				"print(list(filter(ok, range(1, 97)))) # _Michael S. Branicky_, Aug 30 2021"
			],
			"xref": [
				"Cf. A002312, A006530.",
				"Cf. A084925 (hyperbolic analog)."
			],
			"keyword": "nonn,nice,easy",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_ and _J. H. Conway_",
			"references": 10,
			"revision": 52,
			"time": "2021-08-30T10:53:53-04:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}