{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A012494",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 12494,
			"data": "1,-3,45,-1743,125625,-14554683,2473184805,-579439207623,179018972217585,-70518070842040563,34495620120141463965,-20515677772241956573503,14578232896601243652363945,-12198268199871431840616166443,11871344562637111570703016357525",
			"name": "Expansion of e.g.f. arctan(sin(x)) (odd powers only).",
			"comment": [
				"arctan(sin(x)) = x - 3*x^3/3! + 45*x^5/5! - 1743*x^7/7! + 125625*x^9/9! + ....",
				"Absolute values are coefficients in expansion of",
				"arctanh(arcsinh(x)) = x + 3*x^3/3! + 45*x^5/5! + 1743*x^7/7! + ....",
				"arccot(sin(x)) = Pi/2 - x + 3*x^3/3! - 45*x^5/5! + 1743*x^7/7! - ...."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A012494/b012494.txt\"\u003eTable of n, a(n) for n = 0..127\u003c/a\u003e"
			],
			"formula": [
				"a(n) = n!*sum(k=1..ceiling(n/2), (1+(-1)^(n-2*k+1))*2^(1-2*k)*sum(i=0..(2*k-1)/2, (-1)^((n+1)/2-i)*binomial(2*k-1,i)*(2*i-2*k+1)^n/n!)/(2*k-1)), n\u003e0. _Vladimir Kruchinin_, Feb 25 2011",
				"G.f.: cos(x) /(1 + sin^2(x)) = 1 - 3*x^2/2! + 45*x^4/4! - ... . - _Peter Bala_, Feb 06 2017",
				"a(n) ~ (-1)^n * (2*n)! / (log(1+sqrt(2)))^(2*n+1). - _Vaclav Kotesovec_, Aug 17 2018"
			],
			"maple": [
				"a:= n-\u003e (t-\u003e t!*coeff(series(arctan(sin(x)), x, t+1), x, t))(2*n+1):",
				"seq(a(n), n=0..20);  # _Alois P. Heinz_, Aug 16 2018"
			],
			"mathematica": [
				"Drop[ Range[0, 25]! CoefficientList[ Series[ ArcTan[ Sin[x]], {x, 0, 25}], x], {1, 25, 2}] (* Or *)",
				"f[n_] := n!Sum[(1 + (-1)^(n - 2k + 1))2^(1 - 2k)Sum[(-1)^((n + 1)/2 - j)Binomial[2k - 1, j]((2j - 2k + 1)^n/n!)/(2k - 1), {j, 0, (2k - 1)/2}], {k, Ceiling[n/2]}]; Table[ f[n], {n, 1, 25, 2}] (* _Robert G. Wilson v_ *)"
			],
			"program": [
				"(Maxima) a(n):=n!*sum((1+(-1)^(n-2*k+1))*2^(1-2*k)*sum((-1)^((n+1)/2-i)*binomial(2*k-1,i)*(2*i-2*k+1)^n/n!,i,0,(2*k-1)/2)/(2*k-1),k,1,ceiling((n)/2)); /* _Vladimir Kruchinin_, Feb 25 2011 */",
				"(Maxima) a(n):=sum(sum((2*i-2*k-1)^(2*n+1)*binomial(2*k+1,i)*(-1)^(n-i+1),i,0,k)/(4^k*(2*k+1)),k,0,n); /* _Vladimir Kruchinin_, Feb 04 2012 */"
			],
			"xref": [
				"Bisection of A003704, A013208.",
				"Cf. A101923, A001209, A000364, A000281, A156134, A002437.",
				"Cf. other sequences with a g.f. of the form cos(x)/(1 - k*sin^2(x)): A000364 (k=1), A001209 (k=1/2), A000281 (k=2), A156134 (k=3), A002437 (k=4)."
			],
			"keyword": "sign,easy",
			"offset": "0,2",
			"author": "Patrick Demichel (patrick.demichel(AT)hp.com)",
			"references": 5,
			"revision": 51,
			"time": "2018-08-17T02:41:53-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}