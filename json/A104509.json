{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A104509",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 104509,
			"data": "1,1,-1,3,-2,1,4,-6,3,-1,7,-12,10,-4,1,11,-25,25,-15,5,-1,18,-48,60,-44,21,-6,1,29,-91,133,-119,70,-28,7,-1,47,-168,284,-296,210,-104,36,-8,1,76,-306,585,-699,576,-342,147,-45,9,-1,123,-550,1175,-1580,1485,-1022,525,-200,55,-10,1,199,-979,2310,-3454,3641",
			"name": "Matrix inverse of triangle A104505, which is the right-hand side of triangle A084610 of coefficients in (1 + x - x^2)^n.",
			"comment": [
				"Riordan array ( (1 + x^2/(1 - x - x^2), -x/(1 - x - x^2) ) belonging to the hitting time subgroup of the Riordan group (see Peart and Woan). - _Peter Bala_, Jun 29 2015",
				"The sums of absolute values along steep diagonals in this triangle are: 1, 1, 3, 4 + |-1|, 7 + |-2|, 11 + |-6|, 18 + |-12| + 1, ... and these are the tribonacci numbers A000213 that begin with 1, 1, 1, 3. To see this, replace the y in the g.f. A(x,y) = (1 + x^2)/(1-x-x^2 + x*y) with y=-x^2, multiply by x, and add 1, to obtain the g.f. (1 - x^2)/(1-x-x^2-x^3) for A000213. - _Noah Carey_ and _Greg Dresden_, Nov 02 2021"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A104509/b104509.txt\"\u003eTable of n, a(n) for n = 0..10152\u003c/a\u003e (rows 0 to 141, flattened).",
				"P. Peart and W.-J. Woan, \u003ca href=\"http://dx.doi.org/10.1016/S0166-218X(99)00166-3\"\u003eA divisibility property for a subgroup of Riordan matrices\u003c/a\u003e, Discrete Applied Mathematics, Vol. 98, Issue 3, Jan 2000, 255-263.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Lucas_polynomials\"\u003eLucas polynomials\u003c/a\u003e."
			],
			"formula": [
				"For n\u003e=1, a(n,k) = (-1)^k * Sum_{i=0..[(n-k)/2]} n/(n-i) * binomial(n-i,i) * binomial(n-2*i,k) = (-1)^k * Sum_{i=0..[(n-k)/2]} n/(n-i) * binomial(n-k-i,i) * binomial(n-i,k). - _Max Alekseyev_, Oct 11 2021",
				"G.f.: A(x, y) = (1 + x^2)/(1-x-x^2 + x*y).",
				"G.f. for column k: g_k(x) = -(x^2+1)*x^k/(x^2+x-1)^(k+1). - _Robert Israel_, Jun 30 2015",
				"G.f. for row n\u003e=1 is the Lucas polynomial L_n(1-x). - _Max Alekseyev_, Oct 11 2021"
			],
			"example": [
				"Rows begin:",
				"   1;",
				"   1,   -1;",
				"   3,   -2,   1;",
				"   4,   -6,   3,   -1;",
				"   7,  -12,  10,   -4,   1;",
				"  11,  -25,  25,  -15,   5,   -1;",
				"  18,  -48,  60,  -44,  21,   -6,   1;",
				"  29,  -91, 133, -119,  70,  -28,   7,  -1;",
				"  47, -168, 284, -296, 210, -104,  36,  -8, 1;",
				"  76, -306, 585, -699, 576, -342, 147, -45, 9, -1; ..."
			],
			"maple": [
				"S:= series((1 + x^2)/(1-x-x^2 + x*y),x, 20):",
				"for n from 0 to 19 do R[n]:= coeff(S,x,n) od:",
				"seq(seq(coeff(R[n],y,j),j=0..n), n=0..19); # _Robert Israel_, Jun 30 2015"
			],
			"mathematica": [
				"nmax = 11;",
				"T[n_, k_] := Coefficient[(1 + x - x^2)^n, x, n + k];",
				"M = Table[T[n, k], {n, 0, nmax}, {k, 0, nmax}] // Inverse;",
				"Table[M[[n+1, k+1]], {n, 0, nmax}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Mar 27 2019 *)"
			],
			"program": [
				"(PARI) { T(n,k) = my(X=x+x*O(x^n),Y=y+y*O(y^k)); polcoeff(polcoeff((1 + X^2)/(1-X-X^2 + X*Y),n,x),k,y); }",
				"(PARI) { tabl(nn) = my(m = matrix(nn, nn, n, k, n--; k--; if((n\u003ck)||(k\u003c0), 0, polcoeff((1+x-x^2)^n, n+k, x)))^(-1)); for (n=1, nn, for (k=1, n, print1(m[n, k], \", \");); print(););} \\\\ _Michel Marcus_, Jun 30 2015",
				"(PARI) { A104509(n,k) = if(n==0, k==0, (-1)^k * sum(i=0, (n-k)\\2, n/(n-i) * binomial(n-k-i,i) * binomial(n-i,k) )); } \\\\ _Max Alekseyev_, Oct 11 2021"
			],
			"xref": [
				"Leftmost column is A000204 (Lucas numbers). Other columns include: A045925, A067988. Row sums are: {1,0,2,0,2,0,2,...}. Absolute row sums form: A099425. Antidiagonal sums are: {1,1,2,2,2,2,2,...}. Absolute antidiagonal sums are: A084214.",
				"Cf. A104505, A084610, A000213."
			],
			"keyword": "sign,tabl",
			"offset": "0,4",
			"author": "_Paul D. Hanna_, Mar 11 2005",
			"references": 6,
			"revision": 41,
			"time": "2021-12-11T04:29:54-05:00",
			"created": "2005-04-09T03:00:00-04:00"
		}
	]
}