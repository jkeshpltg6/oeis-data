{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A085737",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 85737,
			"data": "1,1,1,1,1,1,0,1,1,0,-1,1,2,1,-1,0,-1,1,1,-1,0,1,-1,-1,8,-1,-1,1,0,1,-1,4,4,-1,1,0,-1,1,-1,-4,8,-4,-1,1,-1,0,-1,1,-8,4,4,-8,1,-1,0,5,-5,7,4,-116,32,-116,4,7,-5,5,0,5,-5,32,-28,16,16,-28,32,-5,5,0",
			"name": "Numerators in triangle formed from Bernoulli numbers.",
			"comment": [
				"Triangle is determined by rules 0) the top number is 1; 1) each number is the sum of the two below it; 2) it is left-right symmetric; 3) the numbers in each of the border rows, after the first 3, are alternately 0.",
				"Up to signs this is the difference table of the Bernoulli numbers (see A212196). The Sage script below is based on L. Seidel's algorithm and does not make use of a library function for the Bernoulli numbers; in fact it generates the Bernoulli numbers on the fly. - _Peter Luschny_, May 04 2012"
			],
			"link": [
				"Fabien Lange and Michel Grabisch, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2008.12.007\"\u003eThe interaction transform for functions on lattices\u003c/a\u003e Discrete Math. 309 (2009), no. 12, 4037-4048. [From _N. J. A. Sloane_, Nov 26 2011]",
				"Peter Luschny, \u003ca href=\"http://oeis.org/wiki/User:Peter_Luschny/ComputationAndAsymptoticsOfBernoulliNumbers\"\u003eThe computation and asymptotics of the Bernoulli numbers\u003c/a\u003e.",
				"Ludwig Seidel, \u003ca href=\"https://www.zobodat.at/pdf/Sitz-Ber-Akad-Muenchen-math-Kl_1877_0157-0187.pdf\"\u003eÜber eine einfache Entstehungsweise der Bernoulli'schen Zahlen und einiger verwandten Reihen\u003c/a\u003e, Sitzungsberichte der mathematisch-physikalischen Classe der königlich bayerischen Akademie der Wissenschaften zu München, volume 7 (1877), 157-187. [_Peter Luschny_, May 04 2012]"
			],
			"formula": [
				"T(n, 0) = (-1)^n*Bernoulli(n), T(n, k) = T(n-1, k-1) - T(n, k-1) for k=1..n.",
				"T(n,k) = Sum_{j=0..k} binomial(k,j)*Bernoulli(n-j). [Lange and Grabisch]"
			],
			"example": [
				"Triangle of fractions begins",
				"    1;",
				"   1/2,   1/2;",
				"   1/6,   1/3,   1/6;",
				"    0,    1/6,   1/6,     0;",
				"  -1/30,  1/30,  2/15,   1/30,  -1/30;",
				"    0,   -1/30,  1/15,   1/15,  -1/30,    0;",
				"   1/42, -1/42, -1/105,  8/105, -1/105, -1/42,   1/42;",
				"    0,    1/42, -1/21,   4/105,  4/105, -1/21,   1/42,   0;",
				"  -1/30,  1/30, -1/105, -4/105,  8/105, -4/105, -1/105, 1/30, -1/30;"
			],
			"maple": [
				"nmax:=11; for n from 0 to nmax do T(n, 0):= (-1)^n*bernoulli(n) od: for n from 1 to nmax do for k from 1 to n do  T(n, k) := T(n-1, k-1) - T(n, k-1) od: od: for n from 0 to nmax do seq(T(n, k), k=0..n) od: seq(seq(numer(T(n, k)), k=0..n), n=0..nmax);  # _Johannes W. Meijer_, Jun 29 2011, revised Nov 25 2012"
			],
			"mathematica": [
				"t[n_, 0] := (-1)^n*BernoulliB[n]; t[n_, k_] := t[n, k] = t[n-1, k-1] - t[n, k-1]; Table[t[n, k] // Numerator, {n, 0, 11}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Jan 07 2014 *)"
			],
			"program": [
				"(Sage)",
				"def BernoulliDifferenceTable(n) :",
				"    def T(S, a) :",
				"        R = [a]",
				"        for s in S :",
				"            a -= s",
				"            R.append(a)",
				"        return R",
				"    def M(A, p) :",
				"        R = T(A,0)",
				"        S = add(r for r in R)",
				"        return -S / (2*p+3)",
				"    R = [1/1]",
				"    A = [1/2,-1/2]; R.extend(A)",
				"    for k in (0..n-2) :",
				"        A = T(A,M(A,k)); R.extend(A)",
				"        A = T(A,0); R.extend(A)",
				"    return R",
				"def A085737_list(n) : return [numerator(q) for q in BernoulliDifferenceTable(n)]",
				"# _Peter Luschny_, May 04 2012"
			],
			"xref": [
				"Cf. A085738, A212196. See A051714/A051715 for another triangle that generates the Bernoulli numbers."
			],
			"keyword": "sign,frac,tabl",
			"offset": "0,13",
			"author": "_N. J. A. Sloane_, following a suggestion of _J. H. Conway_, Jul 23 2003",
			"ext": [
				"Sign flipped in formula by _Johannes W. Meijer_, Jun 29 2011"
			],
			"references": 13,
			"revision": 35,
			"time": "2020-03-16T14:24:45-04:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}