{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A035095",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 35095,
			"data": "3,7,11,29,23,53,103,191,47,59,311,149,83,173,283,107,709,367,269,569,293,317,167,179,389,607,619,643,1091,227,509,263,823,557,1193,907,1571,653,2339,347,359,1087,383,773,3547,797,2111,2677,5449,2749,467",
			"name": "Smallest prime congruent to 1 (mod prime(n)).",
			"comment": [
				"This is a version of the \"least prime in special arithmetic progressions\" problem.",
				"Smallest numbers m such that largest prime factor of Phi(m) = p(n), the n-th prime, also seems to be prime and identical to n-th term of A035095. See A068211, A068212, A065966: Min[x : A068211(x)=p(n)] = A035095(n); e.g., Phi(a(7)) = Phi(103) = 2*3*17, of which 17 = p(7) is the largest prime factor, arising first here.",
				"It appears that A035095, A066674, A125878 are probably all the same, but see the comments in A066674. - _N. J. A. Sloane_, Jan 05 2013",
				"Minimum of the smallest prime factors of F(n,i) = (i^prime(n)-1)/(i-1), when i runs through all integers in [2, prime(n)]. Every prime factor of F(n,i) is congruent to 1 modulo prime(n). - _Vladimir Shevelev_, Nov 26 2014",
				"Conjecture: a(n) is the smallest prime p such that gpf(p-1) = prime(n). See A023503. - _Thomas Ordowski_, Aug 06 2017"
			],
			"reference": [
				"E. Landau, Handbuch der Lehre von der Verteilung der Primzahlen, Bd 1 (reprinted Chelsea 1953).",
				"E. C. Titchmarsh, A divisor problem, Renc. Circ. Math. Palermo, 54 (1930) pp. 414-429.",
				"P. Turan, Uber Primzahlen der arithmetischen Progression, Acta Sci. Math. (Szeged), 8 (1936/37) pp. 226-235."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A035095/b035095.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"P. Erdős, \u003ca href=\"http://acta.fyx.hu/acta/showCustomerArticle.action?id=5970\u0026amp;dataObjectType=article\u0026amp;sessionDataSetId=28afa5176abeef7e\"\u003eOn some application of Brun's method\u003c/a\u003e, Acta Sci. Math (Szeged), v. 13, 1949, pp. 57-63.",
				"A. Granville and C. Pomerance, \u003ca href=\"http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.56.5520\u0026amp;rep=rep1\u0026amp;type=pdf\"\u003eOn the least prime in certain arithmetic progressions\u003c/a\u003e",
				"A. Granville and C. Pomerance, \u003ca href=\"http://dx.doi.org/10.1112/jlms/s2-41.2.193\"\u003eOn the least prime in certain arithmetic progressions\u003c/a\u003e J. Lond Math Soc s2-41 (2) (1990), pp. 193-200.",
				"D. R. Heath-Brown, \u003ca href=\"http://dx.doi.org/10.1017/S0305004100054657\"\u003ealmost-primes in arithmetic progressions in short intervals\u003c/a\u003e, Math Proc Cambr. Phil Soc v 83 (1978), pp. 357-375.",
				"D. R. Heath-Brown, \u003ca href=\"http://dx.doi.org/10.1093/qmath/41.4.405\"\u003eSiegel zeros and the least prime in arithmetic progression\u003c/a\u003e, Quart. J. of Math 41 (49) (1990), pp. 405-418.",
				"H.-J. Kanold, \u003ca href=\"http://dx.doi.org/10.1007/BF01362452\"\u003eUber Primzahlen in arithmetischen Folgen\u003c/a\u003e, Math. Ann. v 156 (1964) pp. 393-395.",
				"U. V. Linnik, On the least prime in an arithmetic progression. I. The basic theorem, Rec. Math (N.S.) v 15 (57) (1944), pp 139-178. \u003ca href=\"http://www.ams.org/mathscinet-getitem?mr=12111\"\u003eMR0012111\u003c/a\u003e",
				"C. Pomerance, \u003ca href=\"http://dx.doi.org/10.1016/0022-314X(80)90056-6\"\u003eA note on the least prime in an arithmetic progression\u003c/a\u003e J. Number Theory 12 (2) (1980), pp. 218-223.",
				"K. Prachar, \u003ca href=\"http://dx.doi.org/10.1515/crll.1961.206.3\"\u003eUber die kleinste Primzahl in einer arithmetischen Reihe\u003c/a\u003e, J Reine Angew Math. 206 (1961) pp. 3-4.",
				"A. Schinzel, \u003ca href=\"http://dx.doi.org/10.1515/crll.1962.210.121\"\u003eRemark on the paper of K. Prachar Uber die kleinste..\u003c/a\u003e, J. Reine Angew Math. v 210 (1962) pp. 122-122.",
				"S. S. Wagstaff, Jr., \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1978-0491465-4\"\u003eThe irregular primes to 125000\u003c/a\u003e, Math. Comp., 32 (1978) pp. 583-591.",
				"S. S. Wagstaff, Jr, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1979-0528061-7\"\u003eGreatest of the Least Primes in Arithmetic Progressions Having a Given Modulus\u003c/a\u003e, Math. Comp., 33 (147) (1979) pp. 1073-1080.",
				"\u003ca href=\"/index/Pri#primes_AP\"\u003eIndex entries for sequences related to primes in arithmetic progressions\u003c/a\u003e"
			],
			"formula": [
				"According to a long-standing conjecture (see the 1979 Wagstaff reference), a(n) \u003c= prime(n)^2 + 1. This would be sufficient to imply that a(n) is the smallest prime such that greatest prime divisor of a(n)-1 is prime(n), the n-th prime: A006530(a(n)-1) = A000040(n). This in turn would be sufficient to imply that no value occurs twice in this sequence. - _Franklin T. Adams-Watters_, Jun 18 2010",
				"a(n) = 1 + A035096(n)*A000040(n). - _Zak Seidov_, Dec 27 2013"
			],
			"example": [
				"a(8) = 191 because in the prime(8)k+1 = 19k+1 sequence, 191 is the smallest prime."
			],
			"mathematica": [
				"a[n_] := Block[{p = Prime[n]}, r = 1 + p; While[ !PrimeQ[r], r += p]; r]; Array[a, 51] (* _Jean-François Alcover_, Sep 20 2011, after PARI *)",
				"a[n_]:=If[n\u003c2,3,Block[{p=Prime[n]},r=1+2*p;While[!PrimeQ[r],r+=2*p]];r];Array[a,51] (* _Zak Seidov_, Dec 14 2013 *)"
			],
			"program": [
				"(PARI) a(n)=local(p,r);p=prime(n);r=1;while(!isprime(r),r+=p);r",
				"(PARI) {my(N=66); forprime(p=2, , forprime(q=p+1,10^10, if((q-1)%p==0, print1(q,\", \"); N-=1; break)); if(N==0,break)); } \\\\ _Joerg Arndt_, May 27 2016"
			],
			"xref": [
				"Cf. A034694, A032448, A006530, A006093, A035096, A000040, A019434, A058383.",
				"Cf. A068211, A068212, A065966, A000010, A070844-A070858, A061092.",
				"Cf. A000040."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Labos Elemer_",
			"ext": [
				"Edited by _Franklin T. Adams-Watters_, Jun 18 2010",
				"Minor edits by _N. J. A. Sloane_, Jun 27 2010",
				"Edited by _N. J. A. Sloane_, Jan 05 2013"
			],
			"references": 20,
			"revision": 79,
			"time": "2018-05-12T12:53:05-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}