{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A020754",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 20754,
			"data": "1,3,7,47,241,843,22019,217069,1092746,8870023,221167421,47255689914,82462576219,1043460553363,79180770078547,3215226335143217,23742453640900971,125781000834058567",
			"name": "Increasing gaps between squarefree numbers (lower end).",
			"comment": [
				"We only consider gaps that set new records. The first gap of size 12 occurs (at 221167421) before the first gap of size 11 (at 262315466) and so for n\u003e10, the n-th term in this sequence does not correspond to the first gap of length n. See A020753.  - _Nathan McNew_, Dec 02 2020",
				"The length of these runs are significantly shorter than would be predicted by a naive random model (for such a model see, e.g., Gordon, Schilling, \u0026 Waterman). For example, with n = a(18) and p = 6/Pi^2 the expected largest run is about 77.9 with variance 6.7, while A020753(18) = 18 which is 23 standard deviations smaller. - _Charles R Greathouse IV_, Oct 29 2021"
			],
			"link": [
				"Tsz Ho Chan, \u003ca href=\"https://arxiv.org/abs/2110.09990\"\u003eNew small gaps between squarefree numbers\u003c/a\u003e, arXiv:2110.09990 [math.NT], 2021.",
				"Louis Gordon, Mark F. Schilling, and Michael S. Waterman, \u003ca href=\"http://blog.thegrandlocus.com/static/misc/Gordon_Schilling_Waterman_1986.pdf\"\u003eAn extreme value theory for long head runs\u003c/a\u003e, Probability Theory and Related Fields, Vol. 72 (1986), pp. 279-287.",
				"Michael J. Mossinghoff, Tomás Oliveira e Silva, and Tim Trudgian, \u003ca href=\"https://arxiv.org/abs/1912.04972\"\u003eThe distribution of k-free numbers\u003c/a\u003e, arXiv:1912.04972 [math.NT], 2019. See Table 3, p. 14."
			],
			"formula": [
				"a(n) = A020755(n) - A020753(n); also a(n) = A020754(n+[n\u003e10]) - 1 at least for n \u003c 19. - _M. F. Hasler_, Dec 28 2015"
			],
			"example": [
				"The first gap in A005117 occurs between 1 and 2 and has length 1. The next largest gap occurs between 3 and 5 and has length 2. The next largest gap is between 7 and 10 and has length 3. Etc."
			],
			"program": [
				"(PARI) A020754(n)=for(k=L=1, 9e9, issquarefree(k)||next; k-L\u003e=n\u0026\u0026return(L); L=k) \\\\ For illustrative purpose only, not useful for n\u003e10. - _M. F. Hasler_, Dec 28 2015",
				"(PARI) r=0; L=1; forsquarefree(n=2,10^8,t=n[1]-L; if(t\u003er,r=t; print1(L\", \")); L=n[1]) \\\\ _Charles R Greathouse IV_, Oct 22 2021"
			],
			"xref": [
				"Cf. A005117, A020753, A020755, A045882, A051681."
			],
			"keyword": "nonn,hard,nice",
			"offset": "1,2",
			"author": "_David W. Wilson_",
			"ext": [
				"Thanks to _Christian G. Bower_ for additional comments.",
				"a(16)-a(18) from A045882 by _Jens Kruse Andersen_, May 01 2015"
			],
			"references": 14,
			"revision": 45,
			"time": "2021-10-30T05:00:42-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}