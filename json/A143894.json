{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A143894",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 143894,
			"data": "1,8,26,48,79,168,326,496,755,1296,2106,3072,4460,6840,10284,14448,20165,29184,41640,56880,77352,107472,147902,197616,263019,354888,475516,624048,816065,1076736,1413142,1826416,2353446,3050400,3936754,5022720",
			"name": "Expansion of (chi(q)^5 * chi(-q))^2 in powers of q where chi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A143894/b143894.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(1/2) * (eta(q^2)^9 / (eta(q)^4 * eta(q^4)^5))^2 in powers of q.",
				"Euler transform of period 4 sequence [ 8, -10, 8, 0, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (16 t)) = 2 g(t) where q = exp(2 Pi i t) and g() is the g.f. for A143895.",
				"G.f.: (Product_{k\u003e0} (1 + x^k)^4 / (1 + x^(2*k))^5)^2.",
				"a(2*n) = A052241(n). a(2*n + 1) = 8 * A022571(n).",
				"a(n) ~ exp(sqrt(n)*Pi) / (sqrt(2) * n^(3/4)). - _Vaclav Kotesovec_, Sep 08 2015"
			],
			"example": [
				"G.f. = 1 + 8*x + 26*x^2 + 48*x^3 + 79*x^4 + 168*x^5 + 326*x^6 + 496*x^7 + ...",
				"G.f. = 1/q + 8*q + 26*q^3 + 48*q^5 + 79*q^7 + 168*q^9 + 326*q^11 + 496*q^13 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (QPochhammer[ x^2]^9 / (QPochhammer[ x]^4 QPochhammer[ x^4]^5))^2, {x, 0, n}]; (* _Michael Somos_, Apr 26 2015 *)",
				"nmax = 40; CoefficientList[Series[Product[((1 + x^k)^4 / (1 + x^(2*k))^5)^2, {k, 1, nmax}], {x, 0, nmax}], x] (* _Vaclav Kotesovec_, Sep 08 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( (eta(x^2 + A)^9 / (eta(x + A)^4 * eta(x^4 + A)^5))^2, n))};"
			],
			"xref": [
				"Cf. A022571, A052241, A143895."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Michael Somos_, Sep 04 2008",
			"references": 2,
			"revision": 15,
			"time": "2021-03-12T22:24:45-05:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}