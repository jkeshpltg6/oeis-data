{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A052558",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 52558,
			"data": "1,1,4,12,72,360,2880,20160,201600,1814400,21772800,239500800,3353011200,43589145600,697426329600,10461394944000,188305108992000,3201186852864000,64023737057280000,1216451004088320000,26761922089943040000,562000363888803840000",
			"name": "a(n) = n! *((-1)^n + 2*n + 3)/4.",
			"comment": [
				"Stirling transform of (-1)^(n+1)*a(n-1) = [1, -1, 4, -12, 72, -360, ...] is A052841(n-1) = [1,0,2,6,38,270,...]. - _Michael Somos_, Mar 04 2004",
				"The Stirling transform of this sequence is A258369. - _Philippe Deléham_, May 17 2005; corrected by _Ilya Gutkovskiy_, Jul 25 2018",
				"Ignoring reflections, this is the number of ways of connecting n+2 equally-spaced points on a circle with a path of n+1 line segments. See A030077 for the number of distinct lengths. - _T. D. Noe_, Jan 05 2007",
				"Row sums of triangle A136581. - _Gary W. Adamson_, Jan 09 2008",
				"From _Gary W. Adamson_, Apr 20 2009: (Start)",
				"Signed: (+ - - + + - - + +, ...) = eigensequence of triangle A002260 (1,2,3,...); \"Crescendo\" with alternate signs.",
				"Example: -360 = (1, 1, -1, -4, 12, 71) dot (1, -2, 3, -4, 5, -6) = (1, -2, -3, 16, 60, -432). (End)",
				"From _Emeric Deutsch_, Jul 18 2009: (Start)",
				"a(n) is the number of odd fixed points in all permutations of {1, 2, ..., n+1}, Example: a(2)=4 because we have 1'23', 1'32, 312, 213', 231, and 321, where the odd fixed points are marked.",
				"a(n) = (n+1)! - A052591(n). (End)",
				"a(n) is also the number of permutations of [n+1] starting with an even number. - _Olivier Gérard_, Nov 07 2011"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A052558/b052558.txt\"\u003eTable of n, a(n) for n=0..100\u003c/a\u003e",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=500\"\u003eEncyclopedia of Combinatorial Structures 500\u003c/a\u003e"
			],
			"formula": [
				"a(n) = a(n-1) + (n^2-1)*a(n-2), with a(1)=1, a(0)=1.",
				"a(n) = ((-1)^n + 2*n + 3)*n!/4.",
				"Let u(1)=1, u(n) = Sum_{k=1..n-1} u(k)*k*(-1)^(k-1) then a(n) = abs(u(n+2)). - _Benoit Cloitre_, Nov 14 2003",
				"E.g.f.: 1/((1-x)*(1-x^2)).",
				"a(n) = (n+1)!/2 if n is odd; a(n) = n!(n+2)/2 if n is even. - _Emeric Deutsch_, Jul 18 2009",
				"E.g.f.: G(0)/(1+x) where G(k) = 1 + 2*x*(k+1)/((2*k+1) - x*(2*k+1)*(2*k+3)/(x*(2*k+3) + 2*(k+1)/G(k+1) )); (recursively defined continued fraction). - _Sergei N. Gladkovskii_, Dec 21 2012"
			],
			"maple": [
				"spec := [S,{S=Prod(Sequence(Z),Sequence(Prod(Z,Z)))},labeled]: seq(combstruct[count](spec,size=n), n=0..20);"
			],
			"mathematica": [
				"Table[n!((-1)^n+2n+3)/4,{n,0,30}] (* _Harvey P. Dale_, Aug 16 2014 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c0,0,(1+n\\2)*n!)",
				"(PARI) a(n)=if(n\u003c0, 0, n!*polcoeff(1/(1-x)/(1-x^2)+x*O(x^n), n))",
				"(MAGMA) [((-1)^n +2*n +3)*Factorial(n)/4: n in [0..30]]; // _G. C. Greubel_, May 07 2019",
				"(Sage) [((-1)^n +2*n +3)*factorial(n)/4 for n in (0..30)] # _G. C. Greubel_, May 07 2019",
				"(GAP) List([0..30], n-\u003e ((-1)^n +2*n +3)*Factorial(n)/4) # _G. C. Greubel_, May 07 2019"
			],
			"xref": [
				"Cf. A136581.",
				"Cf. A002260. - _Gary W. Adamson_, Apr 20 2009",
				"Cf. A052591. - _Emeric Deutsch_, Jul 18 2009",
				"Cf. A052618, A077611, A199495. - _Olivier Gérard_, Nov 07 2011"
			],
			"keyword": "easy,nonn",
			"offset": "0,3",
			"author": "encyclopedia(AT)pommard.inria.fr, Jan 25 2000",
			"references": 10,
			"revision": 53,
			"time": "2019-05-07T17:38:31-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}