{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A332649",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 332649,
			"data": "1,1,1,1,1,1,1,1,1,1,1,1,1,2,1,1,1,1,2,3,1,1,1,1,3,4,6,1,1,1,1,3,7,8,11,1,1,1,1,4,8,25,19,23,1,1,1,1,4,13,31,88,48,47,1,1,1,1,5,14,67,132,366,126,106,1,1,1,1,5,20,80,372,636,1583,355,235,1",
			"name": "Array read by antidiagonals: T(n,k) is the number of unlabeled k-gonal cacti having n polygons.",
			"comment": [
				"The number of nodes will be n*(k-1) + 1."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A332649/b332649.txt\"\u003eTable of n, a(n) for n = 0..1325\u003c/a\u003e",
				"Maryam Bahrani and Jérémie Lumbroso, \u003ca href=\"http://arxiv.org/abs/1608.01465\"\u003eEnumerations, Forbidden Subgraph Characterizations, and the Split-Decomposition\u003c/a\u003e, arXiv:1608.01465 [math.CO], 2016.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Cactus_graph\"\u003eCactus graph\u003c/a\u003e",
				"\u003ca href=\"/index/Ca#cacti\"\u003eIndex entries for sequences related to cacti\u003c/a\u003e"
			],
			"example": [
				"Array begins:",
				"======================================================",
				"n\\k | 1  2   3    4    5     6     7     8      9",
				"----+-------------------------------------------------",
				"  0 | 1  1   1    1    1     1     1     1      1 ...",
				"  1 | 1  1   1    1    1     1     1     1      1 ...",
				"  2 | 1  1   1    1    1     1     1     1      1 ...",
				"  3 | 1  2   2    3    3     4     4     5      5 ...",
				"  4 | 1  3   4    7    8    13    14    20     22 ...",
				"  5 | 1  6   8   25   31    67    80   143    165 ...",
				"  6 | 1 11  19   88  132   372   504  1093   1391 ...",
				"  7 | 1 23  48  366  636  2419  3659  9722  13485 ...",
				"  8 | 1 47 126 1583 3280 16551 28254 91391 138728 ...",
				"..."
			],
			"program": [
				"(PARI) \\\\ here R(n,k) is column k+1 of A332648.",
				"EulerT(v)={Vec(exp(x*Ser(dirmul(v, vector(#v, n, 1/n))))-1, -#v)}",
				"R(n,k)={my(v=[]); for(n=1, n, my(g=1+x*Ser(v)); v=EulerT(Vec((g^k + g^(k%2)*subst(g^(k\\2), x, x^2))/2))); concat([1], v)}",
				"U(n,k)={my(p=Ser(R(n,k-1))); my(g(d)=subst(p + O(x*x^(n\\d)), x, x^d)); Vec(g(1) + x*sumdiv(k, d, eulerphi(d)*g(d)^(k/d))/(2*k) - x*(g(1)^k)/2 + x*if(k%2==0, g(2)^(k/2) - g(1)^2*g(2)^(k/2-1))/4)}",
				"T(n)={Mat(concat([vectorv(n+1,i,1)], vector(n, k, Col(U(n,k+1)))))}",
				"{ my(A=T(8)); for(n=1, #A, print(A[n,])) }"
			],
			"xref": [
				"Columns k=1..4 are A000012, A000055(n+1), A003081, A287892.",
				"Cf. A303694, A332648, A332650, A332651."
			],
			"keyword": "nonn,tabl",
			"offset": "0,14",
			"author": "_Andrew Howroyd_, Feb 18 2020",
			"references": 6,
			"revision": 10,
			"time": "2020-12-23T01:51:28-05:00",
			"created": "2020-02-18T18:33:12-05:00"
		}
	]
}