{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A122949",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 122949,
			"data": "1,3,26,426,11064,413640,20946960,1377648720,114078384000,11611761920640,1425189271161600,207609729886944000,35419018603306060800,6996657393055480550400,1584616114318716544665600,407930516160959891683584000,118458533875304716189544448000",
			"name": "Number of ordered pairs of permutations generating a transitive group.",
			"comment": [
				"From Dixon: The sequence is asymptotic to (n!)^2; when divided by n!^2, it has a high-order asymptotic contact with the probability that two randomly chosen permutations generate the symmetric group. Also: a(n)=(n-1)!*A003319(n+1), where A003319 is the number of connected [or indecomposable] permutations. The coefficients in the asymptotic expansion of a(n)/(n!)^2 are A113869 and in absolute value, they constitute A084357 (number of sets of sets of lists)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A122949/b122949.txt\"\u003eTable of n, a(n) for n = 1..253\u003c/a\u003e",
				"John D. Dixon, \u003ca href=\"http://www.combinatorics.org/Volume_12/Abstracts/v12i1r56.html\"\u003eAsymptotics of Generating the Symmetric and Alternating Groups\u003c/a\u003e, Electronic Journal of Combinatorics, vol 11(2), R56.",
				"P. Flajolet and R. Sedgewick, \u003ca href=\"http://algo.inria.fr/flajolet/Publications/books.html\"\u003eAnalytic Combinatorics\u003c/a\u003e, 2009; page 139."
			],
			"formula": [
				"Exponential generating function is: log(1+Sum_{n\u003e=1}n!*z^n).",
				"a(n) = (n!)^2 - (n-1)! * Sum_{k=1..n-1} a(k) * (n-k)! / (k-1)!. - _Ilya Gutkovskiy_, Jul 10 2020"
			],
			"example": [
				"a(2)=3 because there are 2!*2!=4 pairs of permutations, of which only [(1,1),(1,1)] does not generate a transitive group."
			],
			"maple": [
				"series(log(add(n!*z^n,n=0..Order+2)),z=0):seq(coeff(%,z,j)*j!,j=0..Order);"
			],
			"mathematica": [
				"max = 15; Drop[ CoefficientList[ Series[ Log[1 + Sum[n!*z^n, {n, 1, max}]], {z, 0, max}], z]* Range[0, max]!, 1](* _Jean-François Alcover_, Oct 05 2011 *)"
			],
			"program": [
				"(PARI) N=20; x='x+O('x^N); Vec(serlaplace(log(sum(k=0, N, k!*x^k)))) \\\\ _Seiichi Manyama_, Mar 01 2019"
			],
			"xref": [
				"Cf. A003319, A084357, A113869."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Philippe Flajolet_, Oct 25 2006",
			"ext": [
				"More terms from _Seiichi Manyama_, Mar 01 2019"
			],
			"references": 6,
			"revision": 27,
			"time": "2020-07-11T07:39:24-04:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}