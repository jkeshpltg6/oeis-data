{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A258900",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 258900,
			"data": "1,-6,-1764,-7700616,-147910405104,-8310698364852576,-1085420895640591777344,-284168646775526186095019136,-134459287943928269154814258953984,-106506405136317713669903020280294647296",
			"name": "E.g.f.: S(x) = Series_Reversion( Integral 1/(1-x^4)^(1/4) dx ), where the constant of integration is zero.",
			"formula": [
				"Let e.g.f. C(x) = Sum_{n\u003e=0} a(n)*x^(4*n)/(4*n)! and e.g.f. S(x) = Sum_{n\u003e=0} a(n)*x^(4*n+1)/(4*n+1)!, then C(x) and S(x) satisfy:",
				"(1) C(x)^4 + S(x)^4 = 1,",
				"(2) S'(x) = C(x),",
				"(3) C'(x) = -S(x)^3/C(x)^2,",
				"(4) C(x)^3 * C'(x) + S(x)^3 * S'(x) = 0,",
				"(5) S(x)/C(x) = Integral 1/C(x)^4 dx,",
				"(6) S(x)/C(x) = Series_Reversion( Integral 1/(1+x^4) dx ) = Series_Reversion( Sum_{n\u003e=0} (-1)^n * x^(4*n+1)/(4*n+1) ).",
				"(7) S(x)^2/C(x)^2 = tan( 2 * Integral S(x)/C(x) dx ).",
				"(8) C(x)^2 + I*S(x)^2 = exp( 2*I * Integral S(x)/C(x) dx )."
			],
			"example": [
				"E.g.f. with offset 0 is C(x) and e.g.f. with offset 1 is S(x) where:",
				"C(x) = 1 - 6*x^4/4! - 1764*x^8/8! - 7700616*x^12/12! - 147910405104*x^16/16! -...",
				"S(x) = x - 6*x^5/5! - 1764*x^9/9! - 7700616*x^13/13! - 147910405104*x^17/17! -...",
				"such that C(x)^4 + S(x)^4 = 1:",
				"C(x)^4 = 1 - 24*x^4/4! + 8064*x^8/8! + 2128896*x^12/12! + 52932870144*x^16/16! +...",
				"S(x)^4 = 24*x^4/4! - 8064*x^8/8! - 2128896*x^12/12! - 52932870144*x^16/16! -...",
				"Related Expansions.",
				"(1) The series reversion of S(x) is Integral 1/(1-x^4)^(1/4) dx:",
				"Series_Reversion(S(x)) = x + 6*x^5/5! + 6300*x^9/9! + 56133000*x^13/13! +...",
				"1/(1-x^4)^(1/4) = 1 + 6*x^4/4! + 6300*x^8/8! + 56133000*x^12/12! + 1992160170000*x^16/16! +...+ A258899(n)*x^(4*n)/(4*n)! +...",
				"(2) d/dx S(x)/C(x) = 1/C(x)^4:",
				"1/C(x)^4 = 1 + 24*x^4/4! + 32256*x^8/8! + 285272064*x^12/12! +...",
				"S(x)/C(x) = x + 24*x^5/5! + 32256*x^9/9! + 285272064*x^13/13! + 8967114326016*x^17/17! +...+ A258901(n)*x^(4*n+1)/(4*n+1)! +...",
				"where",
				"Series_Reversion(S(x)/C(x)) = x - x^5/5 + x^9/9 - x^13/13 + x^17/17 - x^21/21 +..."
			],
			"program": [
				"(PARI) /* E.g.f. Series_Reversion(Integral 1/(1-x^4)^(1/4) dx): */",
				"{a(n)=local(S=x); S = serreverse( intformal(  1/(1-x^4 +x*O(x^(4*n)))^(1/4) )); (4*n+1)!*polcoeff(S,4*n+1)}",
				"for(n=0,15,print1(a(n),\", \"))",
				"(PARI) /* E.g.f. C(x) with offset 0: */",
				"{a(n)=local(S=x, C=1+x); for(i=1, n, S=intformal(C +x*O(x^(4*n))); C=1-intformal(S^3/C^2 +x*O(x^(4*n))); ); (4*n)!*polcoeff(C, 4*n)}",
				"for(n=0, 15, print1(a(n), \", \"))",
				"(PARI) /* E.g.f. S(x) with offset 1: */",
				"{a(n)=local(S=x, C=1+x); for(i=1, n+1, S=intformal(C +x*O(x^(4*n))); C=1-intformal(S^3/C^2 +x*O(x^(4*n+1))); ); (4*n+1)!*polcoeff(S, 4*n+1)}",
				"for(n=0, 15, print1(a(n), \", \"))"
			],
			"xref": [
				"Cf. A258899, A258901, A258878."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Paul D. Hanna_, Jun 14 2015",
			"references": 3,
			"revision": 7,
			"time": "2015-06-14T14:57:42-04:00",
			"created": "2015-06-14T14:16:58-04:00"
		}
	]
}