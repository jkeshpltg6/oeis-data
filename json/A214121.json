{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A214121",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 214121,
			"data": "5,0,14,2,2,0,33,4,6,0,75,6,13,0,16,0,165,8,27,0,32,0,353,10,57,0,62,0,60,0,747,12,119,0,124,0,109,0,1577,14,247,0,250,0,206,0,184,0,3327,16,515,0,508,0,399,0,323,0,7015,18,1079,0,1046,0,790,0,590",
			"name": "Irregular array T(n,k) of the numbers of non-extendable (complete) non-self-adjacent simple paths ending at each of a minimal subset of nodes within a square lattice bounded by rectangles with nodal dimensions n and 3, n \u003e= 2.",
			"comment": [
				"The subset of nodes is contained in the top left-hand quarter of the rectangle and has nodal dimensions floor((n+1)/2) and 2 to capture all geometrically distinct counts. The quarter-rectangle is read by rows. The irregular array of numbers is:",
				"....k.....1.....2.....3.....4.....5.....6.....7.....8.....9....10....11....12",
				"..n",
				"..2.......5.....0",
				"..3......14.....2.....2.....0",
				"..4......33.....4.....6.....0",
				"..5......75.....6....13.....0....16.....0",
				"..6.....165.....8....27.....0....32.....0",
				"..7.....353....10....57.....0....62.....0....60.....0",
				"..8.....747....12...119.....0...124.....0...109.....0",
				"..9....1577....14...247.....0...250.....0...206.....0...184.....0",
				".10....3327....16...515.....0...508.....0...399.....0...323.....0",
				".11....7015....18..1079.....0..1046.....0...790.....0...590.....0...520.....0",
				".12...14785....20..2267.....0..2176.....0..1601.....0..1121.....0...877.....0",
				"where k indicates the position of the end node in the quarter-rectangle. For each n, the maximum value of k is 2*floor((n+1)/2). Reading this array by rows gives the sequence."
			],
			"link": [
				"C. H. Gribble, \u003ca href=\"https://oeis.org/wiki/Complete_non-self-adjacent_paths:Results_for_Square_Lattice\"\u003eComputed characteristics of complete non-self-adjacent paths in a square lattice bounded by various sizes of rectangle.\u003c/a\u003e",
				"C. H. Gribble, \u003ca href=\"https://oeis.org/wiki/Complete non-self-adjacent paths:Program\"\u003eComputes characteristics of complete non-self-adjacent paths in square and cubic lattices bounded by various sizes of rectangle and rectangular cuboid respectively.\u003c/a\u003e"
			],
			"formula": [
				"Let T(n,k) denote an element of the irregular array then it appears that",
				"T(n,k) = 0, n \u003e= 3, k = 2j, j \u003e= 2,",
				"T(n,1) - 2T(n-1,1) - T(n-4,1) - 8 = 0, n \u003e= 8,",
				"T(n,2) = 2(n-2), n \u003e= 2,",
				"T(n,3) - 2T(n-1,3) - T(n-4,3) + 2(n-7) = 0, n \u003e= 9,",
				"T(n,5) - 2T(n-1,5) - T(n-4,5) + 8(n-7) = 0, n \u003e= 10,",
				"T(n,7) - 2T(n-1,7) - T(n-4,7) + 20(n-8) + 8 = 0, n \u003e= 11,",
				"T(n,9) - 2T(n-1,9) - T(n-4,9) + 46(n-9) + 30 = 0, n \u003e= 13,",
				"T(n,11) - 2T(n-1,11) - T(n-4,11) + 104(n-10) + 84 = 0, n \u003e= 15,",
				"T(n,13) - 2T(n-1,13) - T(n-4,13) + 226(n-11) + 202 = 0, n \u003e= 15."
			],
			"example": [
				"When n = 2, the number of times (NT) each node in the rectangle is the end node (EN) of a complete non-self-adjacent simple path is",
				"EN 0 1 2",
				"   3 4 5",
				"NT 5 0 5",
				"   5 0 5",
				"To limit duplication, only the top left-hand corner 5 and the 0 to its right are stored in the sequence, i.e. T(2,1) = 5 and T(2,2) = 0."
			],
			"xref": [
				"Cf. A213106, A213249, A213089, A213954, A214119."
			],
			"keyword": "nonn,tabf",
			"offset": "2,1",
			"author": "_Christopher Hunt Gribble_, Jul 04 2012",
			"ext": [
				"Comment corrected by _Christopher Hunt Gribble_, Jul 22 2012"
			],
			"references": 7,
			"revision": 10,
			"time": "2012-07-23T12:45:37-04:00",
			"created": "2012-07-11T13:53:51-04:00"
		}
	]
}