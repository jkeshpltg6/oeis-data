{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A144150",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 144150,
			"data": "1,1,1,1,1,1,1,1,2,1,1,1,3,5,1,1,1,4,12,15,1,1,1,5,22,60,52,1,1,1,6,35,154,358,203,1,1,1,7,51,315,1304,2471,877,1,1,1,8,70,561,3455,12915,19302,4140,1,1,1,9,92,910,7556,44590,146115,167894,21147,1,1,1,10,117",
			"name": "Square array A(n,k), n \u003e= 0, k \u003e= 0, read by antidiagonals, where the e.g.f. of column k is 1+g^(k+1)(x) with g = x-\u003e exp(x)-1.",
			"comment": [
				"A(n,k) is also the number of (k+1)-level labeled rooted trees with n leaves.",
				"Number of ways to start with set {1,2,...,n} and then repeat k times: partition each set into subsets. - _Alois P. Heinz_, Aug 14 2015",
				"Equivalently, A(n,k) is the number of length k+1 multichains from bottom to top in the set partition lattice of an n-set. - _Geoffrey Critzer_, Dec 05 2020"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A144150/b144150.txt\"\u003eAntidiagonals n = 0..140, flattened\u003c/a\u003e",
				"E. T. Bell, \u003ca href=\"http://doi.org/10.2307/1968633\"\u003eThe Iterated Exponential Integers\u003c/a\u003e, Annals of Mathematics, 39(3) (1938), 539-557.",
				"Pierpaolo Natalini and Paolo Emilio Ricci, \u003ca href=\"https://doi.org/10.2298/AADM1702327N\"\u003eHigher order Bell polynomials and the relevant integer sequences\u003c/a\u003e, in Appl. Anal. Discrete Math. 11 (2017), 327-339.",
				"Pierpaolo Natalini and Paolo E. Ricci, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL20/Ricci/ricci3.html\"\u003eInteger Sequences Connected with Extensions of the Bell Polynomials\u003c/a\u003e, Journal of Integer Sequences, 2017, Vol. 20, #17.10.2.",
				"Ivar Henning Skau and Kai Forsberg Kristensen, \u003ca href=\"https://arxiv.org/abs/1903.07979\"\u003eAn asymptotic Formula for the iterated exponential Bell Numbers\u003c/a\u003e, arXiv:1903.07979 [math.CO], 2019.",
				"Ivar Henning Skau and Kai Forsberg Kristensen, \u003ca href=\"https://arxiv.org/abs/1903.08379\"\u003eSets of iterated Partitions and the Bell iterated Exponential Integers\u003c/a\u003e, arXiv:1903.08379 [math.CO], 2019.",
				"\u003ca href=\"/index/Ro#rooted\"\u003eIndex entries for sequences related to rooted trees\u003c/a\u003e"
			],
			"formula": [
				"E.g.f. of column k: 1 + g^(k+1)(x) with g = x-\u003e exp(x)-1.",
				"Column k+1 is Stirling transform of column k."
			],
			"example": [
				"Square array begins:",
				"  1,  1,   1,    1,    1,    1,  ...",
				"  1,  1,   1,    1,    1,    1,  ...",
				"  1,  2,   3,    4,    5,    6,  ...",
				"  1,  5,  12,   22,   35,   51,  ...",
				"  1, 15,  60,  154,  315,  561,  ...",
				"  1, 52, 358, 1304, 3455, 7556,  ..."
			],
			"maple": [
				"g:= proc(p) local b; b:= proc(n) option remember; if n=0 then 1",
				"      else (n-1)! *add(p(k)*b(n-k)/(k-1)!/(n-k)!, k=1..n) fi",
				"    end end:",
				"A:= (n,k)-\u003e (g@@k)(1)(n):",
				"seq(seq(A(n, d-n), n=0..d), d=0..12);",
				"# second Maple program:",
				"A:= proc(n, k) option remember; `if`(n=0 or k=0, 1,",
				"      add(binomial(n-1, j-1)*A(j, k-1)*A(n-j, k), j=1..n))",
				"    end:",
				"seq(seq(A(n, d-n), n=0..d), d=0..12);  # _Alois P. Heinz_, Aug 14 2015",
				"# third Maple program:",
				"b:= proc(n, t, m) option remember; `if`(t=0, 1, `if`(n=0,",
				"      b(m, t-1, 0), m*b(n-1, t, m)+b(n-1, t, m+1)))",
				"    end:",
				"A:= (n, k)-\u003e b(n, k, 0):",
				"seq(seq(A(n, d-n), n=0..d), d=0..12);  # _Alois P. Heinz_, Aug 04 2021"
			],
			"mathematica": [
				"g[k_] := g[k] = Nest[Function[x, E^x - 1], x, k]; a[n_, k_] := SeriesCoefficient[1 + g[k + 1], {x, 0, n}]*n!; Table[a[n - k, k], {n, 0, 12}, {k, n, 0, -1}] // Flatten (* _Jean-François Alcover_, Dec 06 2013 *)"
			],
			"program": [
				"(Python)",
				"from sympy.core.cache import cacheit",
				"from sympy import binomial",
				"@cacheit",
				"def A(n, k): return 1 if n==0 or k==0 else sum([binomial(n - 1, j - 1)*A(j, k - 1)*A(n - j, k) for j in range(1, n + 1)])",
				"for n in range(51): print([A(k, n - k) for k in range(n + 1)]) # _Indranil Ghosh_, Aug 07 2017"
			],
			"xref": [
				"Columns k=0-10 give: A000012, A000110, A000258, A000307, A000357, A000405, A001669, A081624, A081629, A081697, A081740.",
				"Rows n=0+1, 2-5 give: A000012, A000027, A000326, A005945, A005946.",
				"First lower diagonal gives A139383.",
				"First upper diagonal gives A346802.",
				"Main diagonal gives A261280.",
				"Cf. A000142, A111672, A290353."
			],
			"keyword": "nonn,tabl",
			"offset": "0,9",
			"author": "_Alois P. Heinz_, Sep 11 2008",
			"references": 24,
			"revision": 81,
			"time": "2021-08-23T22:32:34-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}