{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A245212",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 245212,
			"data": "1,3,5,7,9,13,13,15,20,25,21,25,25,37,43,31,33,46,37,53,63,61,45,41,64,73,74,81,57,95,61,63,103,97,115,70,73,109,123,101,81,147,85,137,166,133,93,57,132,170,163,165,105,154,187,161,183,169,117,131,121",
			"name": "a(n) = n * tau(n) - Sum_((d\u003cn) | n) (d * tau(d)).",
			"comment": [
				"If d are divisors of n then values of sequence a(n) are the bending moments at point 0 of static forces of sizes tau(d) operating in places d on the cantilever as the nonnegative number axis of length n with support at point 0 by the schema: a(n) = (n * tau(n)) - Sum_((d\u003cn) | n) (d * tau(d)).",
				"If a(n) = 0 then n must be \u003e 10^7.",
				"Conjecture: a(n) = sigma(n) iff n is a power of 2 (A000079).",
				"Number n = 72 is the smallest number n such that a(n) \u003c n (see A245213).",
				"Number n = 144 is the smallest number n such that a(n) \u003c 0 (see A245214)."
			],
			"link": [
				"Jens Kruse Andersen, \u003ca href=\"/A245212/b245212.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A038040(n) - A245211(n).",
				"a(n) = 2 * A038040(n) - A060640(n) = 2 * (n * tau(n))- Sum_(d | n) (d * tau(d))."
			],
			"example": [
				"For n = 6 with divisors [1, 2, 3, 6] we have: a(6) = 6 * tau(6) - (3 * tau(3) + 2 * tau(2) + 1 * tau(1) = 6*4 - (3*2+2*2+1*1) = 13."
			],
			"program": [
				"(MAGMA) [(2*(n*(#[d: d in Divisors(n)]))-(\u0026+[d*#([e: e in Divisors(d)]): d in Divisors(n)])): n in [1..1000]]",
				"(PARI) a(n) = sumdiv(n, d, (-1)^(d\u003cn)*d*numdiv(d)) \\\\ _Jens Kruse Andersen_, Aug 13 2014"
			],
			"xref": [
				"Cf. A000005, A100484, A245211, A245213, A245214."
			],
			"keyword": "sign",
			"offset": "1,2",
			"author": "_Jaroslav Krizek_, Jul 23 2014",
			"references": 4,
			"revision": 21,
			"time": "2015-09-23T09:11:49-04:00",
			"created": "2014-07-26T12:28:40-04:00"
		}
	]
}