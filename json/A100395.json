{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A100395",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 100395,
			"data": "13,2,3,5,19,59,47,11,43,139,277,61,107,23,79,29,457,167,461,109,197,41,311,727,151,257,53,163,2203,317,1637,479,347,223,1283,863,733,83,1297,89,271,859,1061,1871,2089,5591,557,113,1259,349,1553,3253,1129,2441",
			"name": "The smallest prime number q such that the greatest prime divisor of 2*q+1 equals the n-th prime.",
			"comment": [
				"The offset is 2 because prime(1)=2 is never a prime factor of an odd number."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A100395/b100395.txt\"\u003eTable of n, a(n) for n = 2..10001\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Min{x; x is prime number; A006530(2x+1) = prime(n)}."
			],
			"example": [
				"n=1: a(1)=13 because it is the least prime number such that the greatest prime divisor of 2*13 + 1 = 27 equals 3;",
				"n=2: a(2)=2 because the largest prime divisor of 2*a(2) + 1 = 5 is 5;",
				"n=6: a(6)=19 since the greatest prime factor of 2*19 + 1 = 39 = 3*13 is 13=prime(6)."
			],
			"maple": [
				"A100395 := proc(n)",
				"    p := ithprime(n) ;",
				"    for i from 1 do",
				"        q := ithprime(i) ;",
				"        numtheory[factorset](2*q+1) ;",
				"        if max(op(%)) = p then",
				"            return q;",
				"        end if;",
				"    end do:",
				"end proc:",
				"seq(A100395(n),n=2..60) ; # _R. J. Mathar_, Sep 22 2018"
			],
			"mathematica": [
				"gpf[n_] := FactorInteger[n][[-1, 1]]; n = 54; m = Prime[n + 1]; v = Table[0, {m}]; c = 0; p = 2; While[c \u003c n, g = gpf[2*p + 1]; If[g \u003c= m \u0026\u0026 v[[g]] == 0, c++; v[[g]] = p]; p = NextPrime[p]]; Select[v, # \u003e 0 \u0026] (* _Amiram Eldar_, Aug 08 2020 *)"
			],
			"xref": [
				"Cf. A006530, A023590, A100394."
			],
			"keyword": "nonn",
			"offset": "2,1",
			"author": "_Labos Elemer_, Dec 16 2004",
			"references": 1,
			"revision": 15,
			"time": "2020-08-08T11:29:44-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}