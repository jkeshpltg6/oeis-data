{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A277238",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 277238,
			"data": "1,2,6,10,12,22,28,38,42,52,56,78,90,108,120,142,150,170,178,204,212,232,240,286,310,346,370,412,436,472,496,542,558,598,614,666,682,722,738,796,812,852,868,920,936,976,992,1086,1134,1206,1254,1338,1386,1458,1506,1596,1644,1716,1764,1848,1896,1968",
			"name": "Folding numbers (see comments for the definition).",
			"comment": [
				"Folding numbers: Numbers with an even number of bits in their binary expansion such that the XOR of the left half and the reverse of the right half is the all-1's string. Numbers with an odd number of bits in their binary expansion such that the central bit is 1, and the XOR of the left (n-1)/2 bits and the reverse of the right (n-1)/2 bits is the all-1's string.",
				"Folding numbers with an even (resp. odd) number of bits form A035928 (resp. A276795). - _N. J. A. Sloane_, Nov 03 2016"
			],
			"link": [
				"Lars Blomberg, \u003ca href=\"/A277238/b277238.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Stack Exchange, \u003ca href=\"http://codegolf.stackexchange.com/questions/95458/folding-numbers\"\u003eProgramming Puzzles and Code Golf: Folding Numbers\u003c/a\u003e"
			],
			"example": [
				"178 in base 2 is 10110010. Taking the XOR of 1011 and 0100 (which is 0010 reversed) gives the result 1111, so 178 is in the sequence."
			],
			"maple": [
				"N:= 16: # to get all terms \u003c 2^N",
				"M[1]:= [[1]]: M[2]:= [[1,0]]:",
				"for d from 3 to N by 2 do",
				"  M[d]:= map(L -\u003e [op(L[1..(d-1)/2]),1,op(L[(d+1)/2..-1])], M[d-1]);",
				"  if d \u003c N then",
				"    M[d+1]:= map(L -\u003e ([op(L[1..(d-1)/2]),0,1,op(L[(d+1)/2..-1])],[op(L[1..(d-1)/2]),1,0,op(L[(d+1)/2..-1])]), M[d-1])",
				"  fi",
				"od:",
				"seq(seq(add(L[-i]*2^(i-1),i=1..d),L=M[d]),d=1..N); # _Robert Israel_, Nov 09 2016"
			],
			"mathematica": [
				"{1}~Join~Select[Range@ 2000, If[OddQ@ Length@ # \u0026\u0026 Take[#, {Ceiling[ Length[#]/2]}] == {0}, False, Union[Take[#, Floor[Length[#]/2]] + Reverse@ Take[#, -Floor[Length[#]/2]]] == {1}] \u0026@ IntegerDigits[#, 2] \u0026] (* _Michael De Vlieger_, Oct 07 2016 *)"
			],
			"program": [
				"(PARI) isok(n) = {if (n==1, return(1)); b = binary(n); if ((#b % 2) \u0026\u0026 (b[#b\\2+1] == 0), return (0)); vecmin(vector(#b1, k, bitxor(b[k], b[#b-k+1]))) == 1;} \\\\ _Michel Marcus_, Oct 07 2016"
			],
			"xref": [
				"Cf. A035928, A276795."
			],
			"keyword": "nonn,base",
			"offset": "1,2",
			"author": "_Taylor J. Smith_, Oct 06 2016",
			"references": 4,
			"revision": 34,
			"time": "2016-11-26T08:35:06-05:00",
			"created": "2016-11-03T01:28:51-04:00"
		}
	]
}