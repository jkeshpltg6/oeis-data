{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A324975",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 324975,
			"data": "6,10,12,8,8,10,6,6,8,18,52,12,12,18,98,164,22,6,50,8,96,34,52,46,52,6,6,156,20,46,36,32,16,8,304,36,20,36,10,316,76,468,8,30,24,1580,84,54,8,12,250,28,92,36,20,418,456,928,188,16,8,276,284,56,144",
			"name": "Rank of the n-th Carmichael number.",
			"comment": [
				"See A324974 for definition and explanation of rank of a special polygonal number, hence of rank of a Carmichael number A002997 by Kellner and Sondow 2019.",
				"The ranks of the primary Carmichael numbers A324316 form the subsequence A324976."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A324975/b324975.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Bernd C. Kellner and Jonathan Sondow, \u003ca href=\"https://arxiv.org/abs/1902.10672\"\u003eOn Carmichael and polygonal numbers, Bernoulli polynomials, and sums of base-p digits\u003c/a\u003e, arXiv:1902.10672 [math.NT], 2019.",
				"Bernd C. Kellner, \u003ca href=\"https://arxiv.org/abs/1902.11283\"\u003eOn primary Carmichael numbers\u003c/a\u003e, arXiv:1902.11283 [math.NT], 2019.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Polygonal_number\"\u003ePolygonal number\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 2+2*((m/p)-1)/(p-1), where m = A002997(n) and p is its greatest prime factor. (See Formula in A324974.) Hence a(n) is even, by Carmichael's theorem that p-1 divides (m/p)-1, for any prime factor p of a Carmichael number m."
			],
			"example": [
				"If m = A002997(1) = 561 = 3*11*17, then p = 17, so a(1) = 2+2*((561/17)-1)/(17-1) = 6."
			],
			"mathematica": [
				"T = Cases[Range[1, 10000000, 2], n_ /; Mod[n, CarmichaelLambda[n]] == 1 \u0026\u0026 ! PrimeQ[n]];",
				"GPF[n_] := Last[Select[Divisors[n], PrimeQ]];",
				"Table[2 + 2*(T[[i]]/GPF[T[[i]]] - 1)/(GPF[T[[i]]] - 1), {i, Length[T]}]"
			],
			"xref": [
				"Subsequence of A324974.",
				"A324976 is a subsequence.",
				"Cf. also A002997, A324316, A324972, A324973, A324977."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Bernd C. Kellner_ and _Jonathan Sondow_, Mar 24 2019",
			"references": 5,
			"revision": 18,
			"time": "2019-04-02T05:47:53-04:00",
			"created": "2019-03-25T04:33:34-04:00"
		}
	]
}