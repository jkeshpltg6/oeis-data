{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A319544",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 319544,
			"data": "1,2,6,24,19,-6,-186,-1656,-1647,-1566,-666,10224,10211,10042,7494,-33456,-33439,-33150,-27642,82824,82803,82362,72198,-172200,-172175,-171550,-154650,319200,319171,318330,292230,-543840,-543807,-542718,-504570,869880",
			"name": "a(n) = 1*2*3*4 - 5*6*7*8 + 9*10*11*12 - 13*14*15*16 + ... - (up to n).",
			"comment": [
				"In general, for alternating sequences that multiply the first k natural numbers, and subtract/add the products of the next k natural numbers (preserving the order of operations up to n), we have a(n) = (-1)^floor(n/k) * Sum_{i=1..k-1} (1-sign((n-i) mod k)) * (Product_{j=1..i} (n-j+1)) + Sum_{i=1..n} (-1)^(floor(i/k)+1) * (1-sign(i mod k)) * (Product_{j=1..k} (i-j+1)). Here k=4.",
				"An alternating version of A319205."
			],
			"formula": [
				"a(n) = (-1)^floor(n/4) * Sum_{i=1..3} (1-sign((n-i) mod 4)) * (Product_{j=1..i} (n-j+1)) + Sum_{i=1..n} (-1)^(floor(i/4)+1) * (1-sign(i mod 4)) * (Product_{j=1..4} (i-j+1))."
			],
			"example": [
				"a(1) = 1;",
				"a(2) = 1*2 = 2;",
				"a(3) = 1*2*3 = 6;",
				"a(4) = 1*2*3*4 = 24;",
				"a(5) = 1*2*3*4 - 5 = 19;",
				"a(6) = 1*2*3*4 - 5*6 = -6;",
				"a(7) = 1*2*3*4 - 5*6*7 = -186;",
				"a(8) = 1*2*3*4 - 5*6*7*8 = -1656;",
				"a(9) = 1*2*3*4 - 5*6*7*8 + 9 = -1647;",
				"a(10) = 1*2*3*4 - 5*6*7*8 + 9*10 = -1566;",
				"a(11) = 1*2*3*4 - 5*6*7*8 + 9*10*11 = -666;",
				"a(12) = 1*2*3*4 - 5*6*7*8 + 9*10*11*12 = 10224;",
				"a(13) = 1*2*3*4 - 5*6*7*8 + 9*10*11*12 - 13 = 10211;",
				"a(14) = 1*2*3*4 - 5*6*7*8 + 9*10*11*12 - 13*14 = 10042;",
				"a(15) = 1*2*3*4 - 5*6*7*8 + 9*10*11*12 - 13*14*15 = 7494; etc."
			],
			"mathematica": [
				"a[n_]:=(-1)^Floor[n/4]*Sum[(1-Sign[Mod[n-i,4]])*Product[n-j+1,{j,1,i}],{i,1,3}]+Sum[(-1)^(Floor[i/4]+1)*(1-Sign[Mod[i,4]])*Product[i-j+1,{j,1,3}],{i,1,n}]; Array[a, 30] (* _Stefano Spezia_, Sep 23 2018 *)"
			],
			"xref": [
				"For similar sequences, see: A001057 (k=1), A319373 (k=2), A319543 (k=3), this sequence (k=4), A319545 (k=5), A319546 (k=6), A319547 (k=7), A319549 (k=8), A319550 (k=9), A319551 (k=10).",
				"Cf. A319205."
			],
			"keyword": "sign,easy",
			"offset": "1,2",
			"author": "_Wesley Ivan Hurt_, Sep 22 2018",
			"references": 8,
			"revision": 13,
			"time": "2018-10-05T08:01:54-04:00",
			"created": "2018-10-05T08:01:54-04:00"
		}
	]
}