{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A340631",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 340631,
			"data": "7,23,7,13,9,15,11,17,13,19,15,21,17,23,19,25,21,27,23,29,25,31,27,33,29,35,31,37,33,39,35,41,37,43,39,45,41,47,43,49,45,51,47,53,49,55,51,57,53,59,55,61,57,63,59,65,61,67,63,69,65,71,67,73,69,75",
			"name": "a(n) is the minimum number of pebbles such that any assignment of those pebbles on a complete graph with n vertices is a next-player winning game in the two-player impartial pebbling game.",
			"comment": [
				"A move in an impartial two-player pebbling game consists of removing two pebbles from a vertex and adding one pebble to an adjacent vertex. The winning player is the one who makes the final allowable move. We start at n = 3 because we have shown a(2) does not exist."
			],
			"reference": [
				"E. R. Berlekamp, J. H. Conway, and R. K. Guy, Winning Ways for Your Mathematical Plays, Vol. 1, CRC Press, 2001."
			],
			"link": [
				"Eugene Fiorini, Max Lind, Andrew Woldar, and Tony W. H. Wong, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL24/Wong/wong31.html\"\u003eCharacterizing Winning Positions in the Impartial Two-Player Pebbling Game on Complete Graphs\u003c/a\u003e, J. Int. Seq., Vol. 24 (2021), Article 21.6.4.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,1,-1)."
			],
			"formula": [
				"For n\u003e2, a(2n-1)=2n+1, a(2n)=a(2n+5)=2n+7.",
				"G.f.: x^3*(12*x^4-10*x^3-23*x^2+16*x+7)/((x+1)*(x-1)^2).",
				"For n\u003e=5, a(n) = 2*A084964(n+4) - 1 = 2*A084964(n+2) + 1. - _Hugo Pfoertner_, Jan 14 2021"
			],
			"example": [
				"For n=3, a(3)=7 is the least number of pebbles for which every game in a next-player winning game regardless of assignment."
			],
			"mathematica": [
				"(* Given n and m, list all possible assignments. *)",
				"alltuples[n_, m_] := IntegerPartitions[m + n, {n}] - 1;",
				"(* Given an assignment, list all resultant assignments after one pebbling move; only work for n\u003e=3. *)",
				"pebblemoves[config_] := Block[{n, temp}, n = Length[config]; temp = Table[config, {i, n (n - 1)}] + Permutations[Join[{-2, 1}, Table[0, {i, n - 2}]]]; temp = Select[temp, Min[#] \u003e= 0 \u0026]; temp = ReverseSort[DeleteDuplicates[ReverseSort /@ temp]]];",
				"(* Given n and m, list all assignments that are P-games. *)",
				"Plist = {};plist[n_, m_] := Block[{index, tuples}, While[Length[Plist] \u003c n, index = Length[Plist]; AppendTo[Plist, {{Join[{1}, Table[0, {i, index}]]}}]]; Do[AppendTo[Plist[[n]], {}]; tuples = alltuples[n, i]; Do[If[Not[IntersectingQ[pebblemoves[tuples[[j]]], Plist[[n, i - 1]]]], AppendTo[Plist[[n, i]], tuples[[j]]]], {j, Length[tuples]}], {i, Length[Plist[[n]]] + 1, m}]; Plist[[n, m]]];",
				"(* Given n, print out the minimum m such that there are no P-games with m pebbles *)",
				"Do[m = 1; While[plist[n, m] != {}, m++]; Print[\"n=\", n, \" m=\", m], {n, 3, 20}]"
			],
			"xref": [
				"Cf. A084964."
			],
			"keyword": "nonn,easy",
			"offset": "3,1",
			"author": "_Eugene Fiorini_, _Max C. Lind_, _Andrew Woldar_, _Wing Hong Tony Wong_, Jan 13 2021",
			"references": 4,
			"revision": 28,
			"time": "2021-09-02T15:51:42-04:00",
			"created": "2021-01-14T20:30:49-05:00"
		}
	]
}