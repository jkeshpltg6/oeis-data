{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A213062",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 213062,
			"data": "1,0,11,24,38,50,71,87,106,127,151,185,211,249,288,325,364,406,459,508,550,613,676,728",
			"name": "Minimal sum x(1) +...+ x(n) such that 1/x(1) +...+ 1/x(n) = 1, the x(i) being n distinct positive integers.",
			"comment": [
				"The term a(2)=0 corresponds to the fact that 1 cannot be written as Egyptian fraction with 2 (distinct) terms."
			],
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/EgyptianFraction.html\"\u003eEgyptian Fraction\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Egyptian_fraction\"\u003eEgyptian fraction\u003c/a\u003e",
				"\u003ca href=\"/index/Ed#Egypt\"\u003eIndex entries for sequences related to Egyptian fractions\u003c/a\u003e"
			],
			"example": [
				"a(3) = 11 = 2 + 3 + 6, because 1/2+1/3+1/6 is the only Egyptian fraction with 3 terms having 1 as sum.",
				"a(4) = 24 = 2 + 4 + 6 + 12 is the smallest sum of denominators among the six 4-term Egyptian fractions equal to 1.",
				"a(5) = 38 = 3 + 4 + 5 + 6 + 20, least sum of denominators among 72 possible 5-term Egyptian fractions equal to 1.",
				"a(6) = 50 = 3 + 4 + 6 + 10 + 12 + 15, least sum of denominators among 2320 possible 6-term Egyptian fractions equal to 1.",
				"a(7) \u003c= 71 = 3 + 5 + 20 + 6 + 10 + 12 + 15 (obtained from n=6 using 1/4 = 1/5 + 1/20).",
				"a(8) \u003c= 114 = 3 + 5 + 20 + 7 + 42 + 10 + 12 + 15 (obtained using 1/6 = 1/7 + 1/42).",
				"a(9) \u003c= 145 = 3 + 6 + 30 + 20 + 7 + 42 + 10 + 12 + 15 (obtained using 1/5 = 1/6 + 1/30).",
				"a(10) \u003c= 202 = 3 + 6 + 30 + 20 + 8 + 56 + 42 + 10 + 12 + 15 (obtained using 1/7 = 1/8 + 1/56)."
			],
			"program": [
				"(PARI) a(n,M=9e9,s=1,m=2)={ n==1 \u0026 return((numerator(s)==1 \u0026 1 \u003e= m*s || s==1)/s); sum( k=m,m+n-1,1/k ) \u003c s \u0026 return; for(x=max(m,1\\s+1),n\\s, n*(x+(n-1)/2)\u003e=M \u0026 break; (m=a(n-1, M-x, s-1/x, x+1)) \u0026 M=min(M,x+m)); M} /* For n\u003e6, a good upper bound must be given as 2nd (optional) argument. Such a bound can be obtained using 1/x = 1/(x+1) + 1/x(x+1) in solutions for n-1, cf. Examples. */"
			],
			"xref": [
				"Cf. A030659. - _Alois P. Heinz_, Sep 21 2012"
			],
			"keyword": "nonn,more",
			"offset": "1,3",
			"author": "_M. F. Hasler_, Jun 03 2012",
			"ext": [
				"a(11)-a(24) from _Robert Price_, Aug 26 2012 - Sep 21 2012"
			],
			"references": 3,
			"revision": 26,
			"time": "2014-04-07T00:33:10-04:00",
			"created": "2012-06-03T23:33:56-04:00"
		}
	]
}