{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A112298",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 112298,
			"data": "1,-3,1,3,0,-3,2,-3,1,0,0,3,2,-6,0,3,0,-3,2,0,2,0,0,-3,1,-6,1,6,0,0,2,-3,0,0,0,3,2,-6,2,0,0,-6,2,0,0,0,0,3,3,-3,0,6,0,-3,0,-6,2,0,0,0,2,-6,2,3,0,0,2,0,0,0,0,-3,2,-6,1,6,0,-6,2,0,1,0,0,6,0,-6,0,0,0,0,4,0,2,0,0,-3,2,-9,0,3,0,0,2,-6,0",
			"name": "Expansion of (a(q) - 3*a(q^2) + 2*a(q^4)) / 6 in powers of q where a() is a cubic AGM theta function.",
			"comment": [
				"Cubic AGM theta functions: a(q) (see A004016), b(q) (A005928), c(q) (A005882).",
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A112298/b112298.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of b(q) * (b(q^4) - b(q)) / (3*b(q^2)) in powers of q  where b() is a cubic AGM theta function. - _Michael Somos_, Jan 17 2015",
				"Expansion of q * chi(-q)^3 * phi(-q^2) * psi(q^3) / chi(-q^6)^3 in powers of q where phi(), psi(), chi() are Ramanujan theta functions. - _Michael Somos_, Jan 17 2015",
				"Expansion of q * phi(-q)^2 * psi(q^6)^2 / (psi(-q) * psi(-q^3)) in powers of q where phi(), psi() are Ramanujan theta functions. - _Michael Somos_, Jan 17 2015",
				"Expansion of q * f(q) * f(-q, -q^5)^4 / f(q^3)^3 in powers of q where f() is a Ramanujan theta function. - _Michael Somos_, Jan 17 2015",
				"Expansion of (eta(q) * eta(q^12))^3 / (eta(q^2) * eta(q^3) * eta(q^4) * eta(q^6)) in powers of q.",
				"Euler transform of period 12 sequence [ -3, -2, -2, -1, -3, 0, -3, -1, -2, -2, -3, -2, ...].",
				"Moebius transform is period 12 sequence [ 1, -4, 0, 6, -1, 0, 1, -6, 0, 4, -1, 0, ...].",
				"Multiplicative with a(2^e) = 3(-1)^e if e\u003e0, a(3^e)=1, a(p^e) = e+1 if p == 1 (mod 6), a(p^e) = (1 + (-1)^e)/2 if p == 2 (mod 6).",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (12 t)) = 12^(1/2) (t/i) f(t) where q = exp(2 Pi i t).",
				"G.f.: Sum_{k\u003e0} Kronecker(-3, k) * x^k * (1 - x^k)^2 / (1 - x^(4*k)).",
				"a(n) = -(-1)^n * A244375(n). a(6*n + 5) = 0, a(3*n) = a(n).",
				"a(2*n) = -3 * A093829(n). a(2*n + 1) = A033762(n). a(3*n + 1) = A129576(n). a(4*n + 1) = A112604(n). a(4*n + 3) = A112605(n). a(6*n + 1) = A097195(n). a(6*n + 2) = -3 * A033687(n)."
			],
			"example": [
				"G.f. = q - 3*q^2 + q^3 + 3*q^4 - 3*q^6 + 2*q^7 - 3*q^8 + q^9 + 3*q^12 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ q QPochhammer[ q, q^2]^3 QPochhammer[ -q^6, q^6]^3 EllipticTheta[ 4, 0, q^2] EllipticTheta[ 2, 0, q^(3/2)] / (2 q^(3/8)), {q, 0, n}]; (* _Michael Somos_, Jan 17 2015 *)",
				"a[ n_] := If[ n \u003c 1, 0, DivisorSum[ n, JacobiSymbol[ -3, n/#] {1, -2, 1, 0}[[Mod[#, 4, 1]]] \u0026]]; (* _Michael Somos_, Jan 17 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, 0, sumdiv(n, d, kronecker(-3, n/d)*[0, 1, -2, 1][d%4 + 1]))};",
				"(PARI) {a(n) = my(A); if( n\u003c1, 0, n--; A = x * O(x^n); polcoeff( (eta(x + A) * eta(x^12 + A))^3/ (eta(x^2 + A) * eta(x^3 + A) * eta(x^4 + A) * eta(x^6 + A)), n))};",
				"(MAGMA) A := Basis( ModularForms( Gamma1(12), 1), 106); A[2] - 3*A[3] + A[4] + 3*A[5]; /* _Michael Somos_, Jan 17 2015 */"
			],
			"xref": [
				"Cf. A033687, A033762, A093829, A097195, A112604, A112605, A129576, A244375."
			],
			"keyword": "sign,mult",
			"offset": "1,2",
			"author": "_Michael Somos_, Sep 02 2005",
			"references": 4,
			"revision": 22,
			"time": "2021-03-12T22:24:43-05:00",
			"created": "2005-09-21T03:00:00-04:00"
		}
	]
}