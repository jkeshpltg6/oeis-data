{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A209302",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 209302,
			"data": "1,2,3,2,3,4,5,4,3,4,5,6,7,6,5,4,5,6,7,8,9,8,7,6,5,6,7,8,9,10,11,10,9,8,7,6,7,8,9,10,11,12,13,12,11,10,9,8,7,8,9,10,11,12,13,14,15,14,13,12,11,10,9,8,9,10,11,12,13,14,15,16,17,16,15,14",
			"name": "Table T(n,k) = max{n+k-1, n+k-1} n, k \u003e 0, read by sides of squares from T(1,n) to T(n,n), then from T(n,n) to T(n,1).",
			"link": [
				"Boris Putievskiy, \u003ca href=\"/A209302/b209302.txt\"\u003eRows n = 1..140 of triangle, flattened\u003c/a\u003e",
				"Boris Putievskiy, \u003ca href=\"http://arxiv.org/abs/1212.2732\"\u003eTransformations Integer Sequences And Pairing Functions\u003c/a\u003e arXiv:1212.2732 [math.CO], 2012."
			],
			"formula": [
				"In general, let m be a natural number. Table T(n,k) = max{m*n+k-m, n+m*k-m}. For the general case,",
				"a(n) = (m+1)*sqrt(n-1) + 1 - |n - floor(sqrt(n-1))^2 - floor(sqrt(n-1))|.",
				"For m=1,",
				"a(n) = 2*sqrt(n-1) + 1 - |n - floor(sqrt(n-1))^2 - floor(sqrt(n-1))|.",
				"a(n) = t + |t^2 - n|, where t = floor(sqrt(n)+1/2). - _Ridouane Oudra_, May 07 2019"
			],
			"example": [
				"The start of the sequence as a table for the general case:",
				"      1   m+1 2*m+1 3*m+1 4*m+1 5*m+1 6*m+1 ...",
				"    m+1   m+2 2*m+2 3*m+2 4*m+2 5*m+2 6*m+2 ...",
				"  2*m+1 2*m+2 2*m+3 3*m+3 4*m+3 5*m+3 6*m+3 ...",
				"  3*m+1 3*m+2 3*m+3 3*m+4 4*m+4 5*m+4 6*m+4 ...",
				"  4*m+1 4*m+2 4*m+3 4*m+4 4*m+5 5*m+5 6*m+5 ...",
				"  5*m+1 5*m+2 5*m+3 5*m+4 5*m+5 5*m+6 6*m+6 ...",
				"  6*m+1 6*m+2 6*m+3 6*m+4 6*m+5 6*m+6 6*m+7 ...",
				"  ...",
				"The start of the sequence as a triangular array read by rows for general case:",
				"      1;",
				"    m+1,   m+2,   m+1;",
				"  2*m+1, 2*m+2, 2*m+3, 2*m+2, 2*m+1;",
				"  3*m+1, 3*m+2, 3*m+3, 3*m+4, 3*m+3, 3*m+2, 3*m+1;",
				"  4*m+1, 4*m+2, 4*m+3, 4*m+4, 4*m+5, 4*m+4, 4*m+3, 4*m+2, 4*m+1;",
				"  ...",
				"Row r contains 2*r-1 terms: r*m+1, r*m+2, ... r*m+r, r*m+r+1, r*m+r, ..., r*m+2, r*m+1.",
				"The start of the sequence as triangle array read by rows for m=1:",
				"  1;",
				"  2,  3,  2;",
				"  3,  4,  5,  4,  3;",
				"  4,  5,  6,  7,  6,  5,  4;",
				"  5,  6,  7,  8,  9,  8,  7,  6,  5;",
				"  6,  7,  8,  9, 10, 11, 10,  9,  8,  7,  6;",
				"  7,  8,  9, 10, 11, 12, 13, 12, 11, 10,  9,  8,  7;",
				"  ..."
			],
			"program": [
				"(Python)",
				"result = 2*int(math.sqrt(n-1)) - abs(n-int(math.sqrt(n-1))**2 - int(math.sqrt(n-1)) -1) +1"
			],
			"xref": [
				"Cf.  A187760."
			],
			"keyword": "nonn,tabf",
			"offset": "1,2",
			"author": "_Boris Putievskiy_, Jan 18 2013",
			"references": 2,
			"revision": 23,
			"time": "2019-05-09T01:23:12-04:00",
			"created": "2013-01-19T13:45:48-05:00"
		}
	]
}