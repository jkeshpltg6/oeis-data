{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A333829",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 333829,
			"data": "1,2,1,5,10,1,14,73,37,1,42,476,651,126,1,132,2952,8530,4770,422,1,429,17886,95943,114612,31851,1422,1,1430,107305,987261,2162033,1317133,202953,4853,1,4862,642070,9613054,35196634,39471355,13792438,1262800,16786,1",
			"name": "Triangle read by rows: T(n,k) is the number of parking functions of length n with k strict descents. T(n,k) for n \u003e= 1 and 0 \u003c= k \u003c= n-1.",
			"comment": [
				"In a parking function w(1), ..., w(n), a strict descent is an index i such that w(i) \u003e w(i+1).",
				"Define a n-dimensional polytope as the convex hull of length n+1 nondecreasing parking functions. Then, the Ehrhart h*-polynomial of this polytope is the sum of T(n,k) * z^(n-1-k) for k = 0..n-1."
			],
			"link": [
				"Paul R. F. Schumacher, \u003ca href=\"https://www.emis.de/journals/JIS/VOL21/Schumacher/schu5.html\"\u003eDescents in Parking Functions\u003c/a\u003e, J. Int. Seq. 21 (2018), #18.2.3."
			],
			"example": [
				"The triangle T(n,k) begins:",
				"n/k  0    1    2    3    4    5",
				"1    1",
				"2    2    1",
				"3    5   10    1",
				"4   14   73   37    1",
				"5   42  476  651  126    1",
				"6  132 2952 8530 4770  422    1",
				"...",
				"The 10 parking functions of length 3 with 1 strict descent are: [[1, 2, 1], [2, 1, 1], [1, 3, 1], [3, 1, 1], [2, 1, 2], [2, 2, 1], [1, 3, 2], [2, 1, 3], [2, 3, 1], [3, 1, 2]]."
			],
			"program": [
				"(SageMath)",
				"var('z,t')",
				"assume(0\u003cz\u003c1)",
				"# Returns a polynomial which is the generating function of strict descents in permutations of a multiset of integers. The multiplicity of these integers are given by an integer partition l. The function uses an analytic expression rather than enumerating the combinatorial objects.",
				"def des_multiset(l):",
				"    return expand(factor(sum( mul( mul( t+i for i in range(1,k+1)) / factorial(k) for k in l ) * z**t , t , 0 , oo ) * (1-z)**(sum(l)+1) ))",
				"# Returns the numbers of noncrossing partitions of size n and type l (an integer partition of n), cf. Kreweras: \"Sur les partitions non-croisées d'un cycle\".",
				"def kreweras_numbers(l):",
				"    m = l.to_exp()",
				"    return mul( i for i in range(sum(l)-len(l)+2,sum(l)+1) ) / mul( factorial(i) for i in m )",
				"def Trow(n):",
				"    pol = sum( des_multiset(l) * kreweras_numbers(l) for l in Partitions(n) )",
				"    return pol.list()",
				"print([Trow(n) for n in (1..4)])",
				"(SageMath) # using[latte_int from LattE]",
				"# Install with \"sage -i latte_int\".",
				"# Another method is to compute the Ehrhart h^*-polynomial of a polytope.",
				"var('z,t')",
				"def Tpol(n):",
				"    p = Polyhedron( NonDecreasingParkingFunctions(n+1) ).ehrhart_polynomial()",
				"    return expand(factor( (1-z)**(n+1) * sum( p * z**t , t , 0 , oo ) ))",
				"def T(n,k):",
				"    return Tpol(n).list()[n-1-k]"
			],
			"xref": [
				"Row sums give A000272(n+1).",
				"Column k=0 gives A000108."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "1,2",
			"author": "_Matthieu Josuat-Vergès_, Apr 06 2020",
			"references": 0,
			"revision": 31,
			"time": "2020-04-10T07:56:53-04:00",
			"created": "2020-04-10T07:56:53-04:00"
		}
	]
}