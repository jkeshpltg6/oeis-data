{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A213658",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 213658,
			"data": "1,5,4,1,1,5,14,14,6,1,1,7,21,43,47,27,8,1,1,9,36,84,142,158,108,44,10,1,1,11,55,165,330,494,542,410,205,65,12,1,1,13,78,286,715,1287,1780,1908,1527,875,346,90,14,1,1,15,105,455,1365,3003,5005",
			"name": "Irregular triangle read by rows: T(n,k) is the number of dominating subsets with k vertices of the graph G(n) consisting of an edge ab and n triangles, each having one vertex identified with the vertex b.",
			"comment": [
				"Row n contains 2n + 2 entries.",
				"Sum of entries in row n = 3^n + 2^{2n+1} = A213659(n)."
			],
			"link": [
				"S. Alikhani and E. Deutsch, \u003ca href=\"http://arxiv.org/abs/1305.3734\"\u003eGraphs with domination roots in the right half-plane\u003c/a\u003e, arXiv preprint arXiv:1305.3734 [math.CO], 2013-2014.",
				"S. Alikhani and Y. H. Peng, \u003ca href=\"http://arxiv.org/abs/0905.2251\"\u003e Introduction to domination polynomial of a graph\u003c/a\u003e, arXiv:0905.2251 [math.CO], 2009.",
				"T. Kotek, J. Preen, F. Simon, P. Tittmann, and M. Trinks, \u003ca href=\"http://arxiv.org/abs/1206.5926\"\u003e Recurrence relations and splitting formulas for the domination polynomial\u003c/a\u003e, arXiv:1206.5926 [math.CO], 2012."
			],
			"formula": [
				"Generating polynomial of row n is x^(n+1)*(2+x)^n + x*(1+x)^(2*n+1); this is the domination polynomial of the graph G(n).",
				"T(n,k) = 2^(2*n+1-k)*binomial(n,k-n-1) + binomial(2*n+1,k-1) (n \u003e= 1; 1 \u003c= k \u003c= 2*n+2)."
			],
			"example": [
				"Row 1 is 1,5,4,1 because the graph G(1) is abcd with edges ab, bc, bd, and cd; there is 1 dominating subset of size 1 ({b}); all binomial(4,2)=6 subsets of size 2 of {a,b,c,d} with the exception of {c,d} are dominating; all binomial(4,3)=4 subsets of size 3 of {a,b,c,d} are dominating; obviously, {a,b,c,d} is dominating.",
				"Triangle starts:",
				"  1,  5,  4,  1;",
				"  1,  5, 14, 14,  6,  1;",
				"  1,  7, 21, 43, 47, 27,  8,  1;"
			],
			"maple": [
				"T := proc (n, k) options operator, arrow: 2^(2*n+1-k)*binomial(n, k-n-1)+binomial(2*n+1, k-1) end proc: for n to 8 do seq(T(n, k), k = 1 .. 2*n+2) end do; # yields sequence in triangular form"
			],
			"mathematica": [
				"row[n_] := CoefficientList[x^(n + 1)*(2 + x)^n + x*(1 + x)^(2*n + 1), x] // Rest;",
				"Table[row[n], {n, 1, 7}] // Flatten (* _Jean-François Alcover_, Dec 02 2017 *)"
			],
			"program": [
				"(MAGMA) /* As triangle */ [[2^(2*n+1-k)*Binomial(n,k-n-1) + Binomial(2*n+1,k-1): k in [1..2*n+2]]: n in [1.. 15]]; // _Vincenzo Librandi_, Jul 21 2019"
			],
			"xref": [
				"Cf. A213659."
			],
			"keyword": "nonn,tabf",
			"offset": "1,2",
			"author": "_Emeric Deutsch_, Jun 29 2012",
			"references": 1,
			"revision": 22,
			"time": "2019-07-21T08:23:04-04:00",
			"created": "2012-06-30T03:47:23-04:00"
		}
	]
}