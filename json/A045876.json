{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A045876",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 45876,
			"data": "1,2,3,4,5,6,7,8,9,11,11,33,44,55,66,77,88,99,110,22,33,22,55,66,77,88,99,110,121,33,44,55,33,77,88,99,110,121,132,44,55,66,77,44,99,110,121,132,143,55,66,77,88,99,55,121,132,143,154,66,77,88,99,110,121,66,143",
			"name": "Sum of different permutations of digits of n (leading 0's allowed).",
			"comment": [
				"Let the arithmetic mean of the digits of a 'D' digit number n be 'A', Let 'N' = number of distinct numbers that can be formed by permuting the digits of n and let 'I' = concatenation of 1 'D' times =(10^D-1)/9. then a(n) = A*N*I. E.g. Let n = 324541 then A= (3+2+4+5+4+1)/6 =19/6. N = 6!/(2!) = 360. I = 111111 a(n) = A*N*I = (19/6)*(360)*(111111) = 126666540. - _Amarnath Murthy_, Jul 14 2003",
				"It seems that the first person who has studied the sum of different permutations of digits of a given number was the French scientist Eugène Aristide Marre (1823-1918). See links. - _Bernard Schott_, Dec 06 2012"
			],
			"reference": [
				"Amarnath Murthy, An interesting result in combinatorics., Mathematics \u0026 Informatics Quarterly, Vol. 3, 1999, Bulgaria."
			],
			"link": [
				"A. Dunigan AtLee, \u003ca href=\"/A045876/b045876.txt\"\u003eTable of n, a(n) for n = 1..100000\u003c/a\u003e.",
				"A. Marre, \u003ca href=\"http://archive.numdam.org/ARCHIVE/NAM/NAM_1846_1_5_/NAM_1846_1_5__57_1/NAM_1846_1_5__57_1.pdf\"\u003eTrouver la somme de toutes les permutations différentes d'un nombre donné.\u003c/a\u003e, Nouvelles Annales de Mathématiques, 1ère série, tome 5 (1846), p. 57-60.",
				"Norbert Verdier, \u003ca href=\"http://www.les-mathematiques.net/phorum/read.php?17,777265\"\u003eQDV4 : Marre, Marre et Marre, page=1\u003c/a\u003e (French mathematical forum les-mathematiques.net)"
			],
			"formula": [
				"a(n) = ((10^A055642(n)-1)/9)*(A047726(n)*A007953(n)/A055642(n)). - _Altug Alkan_, Aug 29 2016"
			],
			"maple": [
				"f:= proc(x) local L,D,n,M,s,j;",
				"  L:= convert(x,base,10);",
				"  D:= [seq(numboccur(j,L),j=0..9)];",
				"  n:= nops(L);",
				"  M:= n!/mul(d!,d=D);",
				"  s:= add(j*D[j+1],j=0..9);",
				"  (10^n-1)*M/9/n*s",
				"end proc:",
				"map(f, [$1..100]); # _Robert Israel_, Jul 07 2015"
			],
			"mathematica": [
				"Table[Total[FromDigits /@ Permutations[IntegerDigits[n]]], {n, 100}] (* _T. D. Noe_, Dec 06 2012 *)"
			],
			"program": [
				"(PARI) A047726(n) = n=eval(Vec(Str(n))); (#n)!/prod(i=0, 9, sum(j=1, #n, n[j]==i)!);",
				"A055642(n) = #Str(n);",
				"A007953(n) = sumdigits(n);",
				"a(n) = ((10^A055642(n)-1)/9)*(A047726(n)*A007953(n)/A055642(n)); \\\\ Altug Alkan, Aug 29 2016",
				"(PARI) A045876(n) = {my(d=digits(n), q=1, v, t=1); v = vecsort(d); for(i=1, #v-1, if(v[i]==v[i+1], t++, q*=binomial(i, t); t=1)); q*binomial(#v, t)*(10^#d-1)*vecsum(d)/9/#d} \\\\ _David A. Corneth_, Oct 06 2016"
			],
			"xref": [
				"Same beginning as A033865. Cf. A061147."
			],
			"keyword": "easy,nonn,base,look",
			"offset": "1,2",
			"author": "_Erich Friedman_",
			"references": 18,
			"revision": 49,
			"time": "2016-10-06T14:20:28-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}