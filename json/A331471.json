{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A331471",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 331471,
			"data": "0,1,1,3,1,5,3,7,1,9,5,6,3,6,7,15,1,17,9,10,5,21,7,8,3,10,6,27,7,8,15,31,1,33,17,18,9,10,10,12,5,10,21,22,7,45,15,16,3,18,10,51,6,22,27,28,7,12,9,28,15,16,31,63,1,65,33,34,17,18,18,20,9,73",
			"name": "Consider the different ways to split the binary representation of n into palindromic parts; a(n) is the greatest possible sum of the parts of such a split.",
			"comment": [
				"Leading zeros are forbidden in the binary representation of n; however we allow leading zeros in the palindromic parts."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A331471/b331471.txt\"\u003eTable of n, a(n) for n = 0..8192\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A331471/a331471.gp.txt\"\u003ePARI program for A331471\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"formula": [
				"a(n) \u003e= A000120(n) with equality iff n = 0 or n is a power of 2.",
				"a(n) \u003c= n with equality iff n belongs to A006995."
			],
			"example": [
				"For n = 10:",
				"- the binary representation of 10 is \"1010\",",
				"- we can split it into \"1\" and \"0\" and \"1\" and \"0\" (1 and 0 and 1 and 0),",
				"- or into \"101\" and \"0\" (5 and 0),",
				"- or into \"1\" and \"010\" (1 and 2),",
				"- hence a(n) = max(2, 5, 3) = 5."
			],
			"mathematica": [
				"palQ[w_] := w == Reverse@w; ric[tg_, cr_] := Block[{m = Length@tg, t}, If[m == 0, Sow@ Total[ FromDigits[#, 2] \u0026 /@ cr], Do[ If[ palQ[t = Take[tg, k]], ric[Drop[tg, k], Join[cr, {t}]]], {k, m}]]]; a[n_] := Max[ Reap[ ric[ IntegerDigits[n, 2], {}]][[2, 1]]]; a /@ Range[0, 73] (* _Giovanni Resta_, Jan 19 2020 *)"
			],
			"program": [
				"(PARI) See Links section."
			],
			"xref": [
				"Cf. A000120, A006995, A215244, A331362."
			],
			"keyword": "nonn,base",
			"offset": "0,4",
			"author": "_Rémy Sigrist_, Jan 17 2020",
			"references": 2,
			"revision": 12,
			"time": "2020-01-19T05:03:57-05:00",
			"created": "2020-01-18T18:25:23-05:00"
		}
	]
}