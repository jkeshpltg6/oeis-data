{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A129402",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 129402,
			"data": "1,1,2,2,1,2,0,2,0,0,2,0,3,1,2,2,2,4,0,0,0,0,2,0,3,0,2,4,0,2,0,2,0,0,0,0,2,3,4,2,1,2,0,2,0,0,2,0,2,2,2,2,4,2,0,0,0,0,0,0,3,0,4,2,0,2,0,2,0,0,0,0,4,3,2,2,0,4,0,2,0,0,4,0,1,0,2",
			"name": "Expansion of phi(x^3) * psi(x^4) + x * phi(x) * psi(x^12) in powers of x where phi(), psi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"reference": [
				"N. J. Fine, Basic Hypergeometric Series and Applications, Amer. Math. Soc., 1988; p. 83, Eq. (32.57)."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A129402/b129402.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of f(x^2) * f(-x^3) / (chi(-x) * chi(x^6)) in powers of x where f(), chi() are Ramanujan theta functions.",
				"Expansion of q^(-1/2) * eta(q^3) * eta(x^4)^3 * eta(q^6) * eta(q^24) / (eta(q) * eta(q^8) * eta(q^12)^12) in powers of q.",
				"Euler transform of period 24 sequence [ 1, 1, 0, -2, 1, -1, 1, -1, 0, 1, 1, -2, 1, 1, 0, -1, 1, -1, 1, -2, 0, 1, 1, -2, ...].",
				"a(n) = b(2*n + 1) where b() is multiplicative with b(2^e) = 0^e, b(3^e) = 1, b(p^e) = e+1 if p == 1, 5, 7, 11 (mod 24), b(p^e) = (1 + (-1)^e)/2 if p == 13, 17, 19, 23 (mod 24).",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (48 t)) = 24^(1/2) (t/i) g(t) where q = exp(2 Pi i t) and g() is g.f. for A190611.",
				"a(12*n + 6) = a(12*n + 8) = a(12*n + 9) = a(12*n + 11) = 0. a(3*n + 1) = a(n).",
				"a(n) = A000377(2*n + 1). a(3*n + 2) = 2 * A128582(n). a(12*n) = A113780(n).",
				"a(n) = (-1)^n * A190615(n) = (-1)^floor( (n+1) / 2) * A128580(n). - _Michael Somos_, Nov 11 2015",
				"a(2*n) = A261118(n). a(2*n + 1) = A261119(n). a(3*n) = A261115(n). - _Michael Somos_, Nov 11 2015",
				"a(4*n) = A260308(n). a(4*n + 1) = A257920(n). a(4*n + 2) = 2 * A259895(n). - _Michael Somos_, Nov 11 2015",
				"a(n) = - A261122(4*n + 2). - _Michael Somos_, Nov 11 2015"
			],
			"example": [
				"G.f = 1 + x + 2*x^2 + 2*x^3 + x^4 + 2*x^5 + 2*x^7 + 2*x^10 + 3*x^12 + x^13 + 2*x^14 + ...",
				"G.f. = q + q^3 + 2*q^5 + 2*q^7 + q^9 + 2*q^11 + 2*q^15 + 2*q^21 + 3*q^25 + q^27 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 0, 0, DivisorSum[ 2 n + 1, KroneckerSymbol[ -6, #] \u0026]]; (* _Michael Somos_, Nov 11 2015 *)",
				"a[ n_] := SeriesCoefficient[ QPochhammer[ -x^2] QPochhammer[ x^3] QPochhammer[ -x, x] QPochhammer[ x^6, -x^6], {x, 0, n}]; (* _Michael Somos_, Nov 11 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, n = 2*n+1; sumdiv( n, d, kronecker( -6, d)))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^3 + A) * eta(x^4 + A)^3 * eta(x^6 + A) * eta(x^24 + A) / (eta(x + A) * eta(x^8 + A) * eta(x^12 + A)^2), n))};"
			],
			"xref": [
				"Cf. A000377, A128580, A128582, A113780, A190615, A257920, A259895, A260308, A261115, A261118, A261119, A261122."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Michael Somos_, Apr 13 2007",
			"references": 12,
			"revision": 15,
			"time": "2021-03-12T22:24:44-05:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}