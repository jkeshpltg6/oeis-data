{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003739",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3739,
			"data": "45,55125,59719680,64416925125,69471840376125,74922901143552000,80801651828175064605,87141671714980415665125,93979154798291442260459520,101353134069755356151903203125",
			"name": "Number of spanning trees in W_5 X P_n.",
			"reference": [
				"F. Faase, On the number of specific spanning subgraphs of the graphs G X P_n, Ars Combin. 49 (1998), 129-154."
			],
			"link": [
				"P. Raff, \u003ca href=\"/A003739/b003739.txt\"\u003eTable of n, a(n) for n = 1..200\u003c/a\u003e",
				"F. Faase, \u003ca href=\"http://www.iwriteiam.nl/Cpaper.zip\"\u003eOn the number of specific spanning subgraphs of the graphs G X P_n\u003c/a\u003e, Preliminary version of paper that appeared in Ars Combin. 49 (1998), 129-154.",
				"F. Faase, \u003ca href=\"http://www.iwriteiam.nl/counting.html\"\u003eCounting Hamiltonian cycles in product graphs\u003c/a\u003e",
				"F. Faase, \u003ca href=\"http://www.iwriteiam.nl/Cresults.html\"\u003eResults from the counting program\u003c/a\u003e",
				"P. Raff, \u003ca href=\"http://arxiv.org/abs/0809.2551\"\u003eSpanning Trees in Grid Graphs\u003c/a\u003e, arXiv:0809.2551 [math.CO], 2008.",
				"P. Raff, \u003ca href=\"http://www.math.rutgers.edu/~praff/span/5/12-13-14-15-23-24-35-45/index.xml\"\u003eAnalysis of the Number of Spanning Trees of W_5 x P_n.\u003c/a\u003e Contains sequence, recurrence, generating function, and more.",
				"P. Raff, \u003ca href=\"http://www.myraff.com/projects/spanning-trees-in-grid-graphs\"\u003eAnalysis of the Number of Spanning Trees of Grid Graphs\u003c/a\u003e.",
				"\u003ca href=\"/index/Tra#trees\"\u003eIndex entries for sequences related to trees\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_09\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1152,-80640,1442883,-4477824,4477824,-1442883,80640,-1152,1)."
			],
			"formula": [
				"a(n) = 1152*a(n-1) - 80640*a(n-2) + 1442883*a(n-3) - 4477824*a(n-4) + 4477824*a(n-5) - 1442883*a(n-6) + 80640*a(n-7) - 1152*a(n-8) + a(n-9).",
				"G.f.: 45*x*(1 +73*x -3456*x^2 +4534*x^3 +4534*x^4 -3456*x^5 +73*x^6 +x^7)/(1 -1152*x +80640*x^2 -1442883*x^3 +4477824*x^4 -447782*x^5 +1442883*x^6 -80640*x^7 +1152*x^8 -x^9)."
			],
			"maple": [
				"seq(coeff(series(45*x*(1+73*x-3456*x^2+4534*x^3+4534*x^4-3456*x^5+73*x^6 +x^7)/(1-1152*x+80640*x^2-1442883*x^3+4477824*x^4-447782*x^5+1442883*x^6 -80640*x^7+1152*x^8-x^9), x, n+1), x, n), n = 1..20); # _G. C. Greubel_, Dec 25 2019"
			],
			"mathematica": [
				"Rest@CoefficientList[Series[45*x*(1 +73*x -3456*x^2 +4534*x^3 +4534*x^4 -3456*x^5 +73*x^6 +x^7)/(1 -1152*x +80640*x^2 -1442883*x^3 +4477824*x^4 -447782*x^5 +1442883*x^6 -80640*x^7 +1152*x^8 -x^9), {x,0,20}], x] (* _G. C. Greubel_, Dec 25 2019 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^20)); Vec(45*x*(1 +73*x -3456*x^2 +4534*x^3 +4534*x^4 -3456*x^5 +73*x^6 +x^7)/(1 -1152*x +80640*x^2 -1442883*x^3 +4477824*x^4 -447782*x^5 +1442883*x^6 -80640*x^7 +1152*x^8 -x^9)) \\\\ _G. C. Greubel_, Dec 25 2019",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Integers(), 20); Coefficients(R!( 45*x*(1 +73*x -3456*x^2 +4534*x^3 +4534*x^4 -3456*x^5 +73*x^6 +x^7)/(1 -1152*x +80640*x^2 -1442883*x^3 +4477824*x^4 -447782*x^5 +1442883*x^6 -80640*x^7 +1152*x^8 -x^9) )); // _G. C. Greubel_, Dec 25 2019",
				"(Sage)",
				"def A077952_list(prec):",
				"    P.\u003cx\u003e = PowerSeriesRing(ZZ, prec)",
				"    return P( 45*x*(1 +73*x -3456*x^2 +4534*x^3 +4534*x^4 -3456*x^5 +73*x^6 +x^7)/(1 -1152*x +80640*x^2 -1442883*x^3 +4477824*x^4 -447782*x^5 +1442883*x^6 -80640*x^7 +1152*x^8 -x^9) ).list()",
				"a=A077952_list(20); a[1:] # _G. C. Greubel_, Dec 25 2019"
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_Frans J. Faase_",
			"ext": [
				"Added recurrence from Faase's web page. - _N. J. A. Sloane_, Feb 03 2009"
			],
			"references": 3,
			"revision": 27,
			"time": "2019-12-25T04:52:06-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}