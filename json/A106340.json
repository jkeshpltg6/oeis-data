{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A106340",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 106340,
			"data": "1,-1,1,1,-3,1,-1,9,-7,1,1,-45,55,-15,1,-1,585,-835,285,-31,1,1,-21105,30835,-11025,1351,-63,1,-1,1858185,-2719675,977445,-121891,6069,-127,1,1,-367958745,538607755,-193649085,24187051,-1213065,26335,-255,1,-1,157169540745,-230061795355,82717588485",
			"name": "Triangle T, read by rows, equal to the matrix inverse of the triangle defined by [T^-1](n,k) = (n-k)!*A008278(n+1,k+1), for n\u003e=k\u003e=0, where A008278 is a triangle of Stirling numbers of 2nd kind.",
			"comment": [
				"Row sums are {1,0,-1,2,-3,4,-5,6,...}. Column 1 is A106341."
			],
			"formula": [
				"T(n, k) = A106338(n, k)/k!, for n\u003e=k\u003e=0."
			],
			"example": [
				"Triangle T begins:",
				"  1;",
				"  -1,1;",
				"  1,-3,1;",
				"  -1,9,-7,1;",
				"  1,-45,55,-15,1;",
				"  -1,585,-835,285,-31,1;",
				"  1,-21105,30835,-11025,1351,-63,1;",
				"  -1,1858185,-2719675,977445,-121891,6069,-127,1;",
				"  1,-367958745,538607755,-193649085,24187051,-1213065,26335,-255,1;",
				"  ...",
				"Matrix inverse begins:",
				"  1;",
				"  1,1;",
				"  2,3,1;",
				"  6,12,7,1;",
				"  24,60,50,15,1;",
				"  120,360,390,180,31,1;",
				"  ...",
				"where [T^-1](n,k) = (n-k)!*A008278(n+1,k+1)."
			],
			"mathematica": [
				"rows = 10;",
				"M = Table[If[r \u003e= c, (r-c)! Sum[(-1)^(r-c-m+1) m^r/m!/(r-c-m+1)!, {m, 0, r-c+1}], 0], {r, rows}, {c, rows}] // Inverse;",
				"T[n_, k_] := M[[n+1, k+1]];",
				"Table[T[n, k], {n, 0, rows-1}, {k, 0, n}] (* _Jean-François Alcover_, Jun 27 2019, from PARI *)"
			],
			"program": [
				"(PARI) {T(n,k)=(matrix(n+1,n+1,r,c,if(r\u003e=c,(r-c)!* sum(m=0,r-c+1,(-1)^(r-c+1-m)*m^r/m!/(r-c+1-m)!)))^-1)[n+1,k+1]}",
				"(Sage)",
				"def A106340_matrix(d):",
				"    def A130850(n, k):   # EulerianNumber = A173018",
				"        return add(EulerianNumber(n,j)*binomial(n-j,k) for j in (0..n))",
				"    return matrix(ZZ, d, A130850).inverse()",
				"A106340_matrix(8)  # _Peter Luschny_, May 21 2013"
			],
			"xref": [
				"Cf. A106338, A008278, A106341."
			],
			"keyword": "sign,tabl",
			"offset": "0,5",
			"author": "_Paul D. Hanna_, May 01 2005",
			"references": 3,
			"revision": 12,
			"time": "2019-06-27T06:11:12-04:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}