{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000431",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 431,
			"id": "M2089 N0824",
			"data": "0,0,0,2,16,88,416,1824,7680,31616,128512,518656,2084864,8361984,33497088,134094848,536608768,2146926592,8588754944,34357248000,137433710592,549744803840,2199000186880,8796044787712,35184271425536,140737278640128,562949517213696",
			"name": "Expansion of 2*x^3/((1-2*x)^2*(1-4*x)).",
			"comment": [
				"Number of permutations of length n with exactly one valley. Also (for n\u003e0), the number of ways to pick two of the 2^(n-1) vertices of an n-1 cube that are not connected by an edge. - _Aaron Meyerowitz_, Apr 21 2014",
				"a(n+1), n \u003e= 1: Number of independent vertex pairs for Q_n, n \u003e= 1: 2^(n-1) * (2^n - (n+1)) = T_(2^n - 1) - n * 2^(n-1) = L_n - E_n = A006516(n) - A001787(n), where L_n is the number of vertex pairs and E_n is the number of vertex pairs yielding edges. (Cf. A027624.) - _Daniel Forgues_, Feb 19 2015",
				"From _Petros Hadjicostas_, Aug 08 2019: (Start)",
				"Apparently, by saying \"valley\" of a permutation of [n], _Aaron Meyerowitz_ indirectly assumes that a \"valley\" is an interior minimum of a permutation (i.e., we ignore possible minima at the endpoints). Since the complement of a permutation b_1 b_2 ... b_n (using one-line notation, not cycle notation) is (n+1-b_1) (n+1-b_2) ... (n+1-b_n), the current sequence is also the number of permutations of [n] with exactly one peak (that is, exactly one interior maximum).",
				"Comtet (pp. 260-261 in his book) calls these peaks \"intermediary peaks\" to distinguish them from \"left peaks\" and \"right peaks\" (i.e., maxima at the endpoints).",
				"(End)"
			],
			"reference": [
				"F. N. David, M. G. Kendall and D. E. Barton, Symmetric Function and Allied Tables, Cambridge, 1966, p. 261.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A000431/b000431.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"Désiré André, \u003ca href=\"https://doi.org/10.24033/bsmf.519\"\u003eMémoire sur les séquences des permutations circulaires\u003c/a\u003e, Bulletin de la S. M. F., tome 23 (1895), pp. 122-184.",
				"Nelson H. F. Beebe, \u003ca href=\"https://dx.doi.org/10.1007/978-3-319-64110-2_18\"\u003eThe Greek functions: gamma, psi, and zeta\u003c/a\u003e, In: The Mathematical-Function Computation Handbook, 2017. See pp. 549-550.",
				"S. Billey, K. Burdzy, and B. E. Sagan, \u003ca href=\"http://arxiv.org/abs/1209.0693\"\u003ePermutations with given peak set\u003c/a\u003e, arXiv preprint arXiv:1209.0693 [math.CO], 2012.",
				"S. Billey, K. Burdzy, and B. E. Sagan, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL16/Billey/billey2.html\"\u003ePermutations with given peak set\u003c/a\u003e, J. Int. Seq. 16 (2013), #13.6.1.",
				"C. J. Fewster, D. Siemssen, \u003ca href=\"http://arxiv.org/abs/1403.1723\"\u003eEnumerating Permutations by their Run Structure\u003c/a\u003e, arXiv preprint arXiv:1403.1723 [math.CO], 2014.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"R. G. Rieper and M. Zeleke, \u003ca href=\"https://arxiv.org/abs/math/0005180\"\u003eValleyless Sequences\u003c/a\u003e, arXiv:math/0005180 [math.CO], 2000.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (8,-20,16)"
			],
			"formula": [
				"From _Mitch Harris_, Apr 02 2004: (Start)",
				"a(n) = Sum_{1..2^(n+1) - 1} A007814(k).",
				"a(n) = (4^n - n 2^(n+1))/8 for n \u003e= 1.",
				"(End)",
				"a(n) = 2*A100575(n-1). - _R. J. Mathar_, Mar 14 2011",
				"a(n) = 2^(n-2) * (2^(n-1) - n), n \u003e= 1. - _Daniel Forgues_, Feb 24 2015"
			],
			"example": [
				"From _Petros Hadjicostas_, Aug 08 2019: (Start)",
				"We have a(3) = 2 because the permutations 123, 132, 213, 231, 312, and 321 have exactly 0, 1, 0, 1, 0, and 0 peaks, respectively. Also, they have 0, 0, 1, 0, 1, and 0 valleys, respectively.",
				"Note that permutations 132 and 231 (each one with 1 peak) are complements of permutations 312 and 213, respectively (each one with 1 valley).",
				"Also, a(4) = 16 because",
				"1234 -\u003e 0 peaks and 0 valleys (complement of 4321);",
				"1243 -\u003e 1 peak and  0 valleys (complement of 4312);",
				"1324 -\u003e 1 peak and 1 valley (complement of 4231);",
				"1342 -\u003e 1 peak and 0 valleys (complement of 4213);",
				"1423 -\u003e 1 peak and 1 valley (complement of 4132);",
				"1432 -\u003e 1 peal and 0 valleys (complement of 4123);",
				"2134 -\u003e 0 peaks and 1 valley (complement of 3421);",
				"2143 -\u003e 1 peak and 1 valley (complement of 3412);",
				"2314 -\u003e 1 peak and 1 valley (complement of 3241);",
				"2341 -\u003e 1 peak and 0 valleys (complement of 3214);",
				"2413 -\u003e 1 peak and 1 valley (complement of 3142);",
				"2431 -\u003e 1 peak and 0 valleys (complement of 3124);",
				"3124 -\u003e 0 peaks and 1 valley (complement of 2431);",
				"3142 -\u003e 1 peak and 1 valley (complement of 2413);",
				"3214 -\u003e 0 peaks and 1 valley (complement of 2341);",
				"3241 -\u003e 1 peak and 1 valley (complement of 2314);",
				"3412 -\u003e 1 peak and 1 valley (complement of 2143);",
				"3421 -\u003e 1 peak and 0 valleys (complement of 2134);",
				"4123 -\u003e 0 peaks and 1 valley (complement of 1432);",
				"4132 -\u003e 1 peak and 1 valley (complement of 1423);",
				"4213 -\u003e 0 peaks and 1 valley (complement of 1342);",
				"4231 -\u003e 1 peak and 1 valleys (complement of 1324);",
				"4312 -\u003e 0 peaks and 1 valley (complement of 1243);",
				"4321 -\u003e 0 peaks and 0 valleys (complement of 1234).",
				"(End)"
			],
			"maple": [
				"A000431:=-2/(4*z-1)/(-1+2*z)**2; # Conjectured by _Simon Plouffe_ in his 1992 dissertation. [Proved by Désiré André, 1895, p.154, for circular permutations (see A008303). _Peter Luschny_, Aug 07 2019]",
				"a:= n-\u003e if n=0 then 0 else (Matrix([[2,0,0]]). Matrix(3, (i,j)-\u003e if (i=j-1) then 1 elif j=1 then [8,-20,16][i] else 0 fi)^(n-1))[1,3] fi: seq(a(n), n=0..30); # _Alois P. Heinz_, Aug 26 2008"
			],
			"mathematica": [
				"nn = 30; CoefficientList[Series[2*x^3/((1 - 2*x)^2*(1 - 4*x)), {x, 0, nn}], x] (* _T. D. Noe_, Jun 20 2012 *)",
				"Join[{0}, LinearRecurrence[{8, -20, 16}, {0, 0, 2}, 30]] (* _Jean-François Alcover_, Jan 31 2016 *)"
			],
			"program": [
				"(MAGMA) [0] cat [(4^n - n*2^(n+1))/8: n in [1..30]]; // _Vincenzo Librandi_, Feb 18 2015",
				"(PARI) concat(vector(3), Vec(2*x^3/((1-2*x)^2*(1-4*x)) + O(x^40))) \\\\ _Michel Marcus_, Jan 31 2016"
			],
			"xref": [
				"Cf. A000487, A000517, A027624.",
				"Column k=1 of A008303."
			],
			"keyword": "nonn,easy",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_",
			"references": 8,
			"revision": 101,
			"time": "2021-03-12T22:32:34-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}