{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A318254",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 318254,
			"data": "1,1,1,1,3,-2,1,5,-20,16,1,7,-70,336,-272,1,9,-168,2016,-9792,7936,1,11,-330,7392,-89760,436480,-353792,1,13,-572,20592,-466752,5674240,-27595776,22368256,1,15,-910,48048,-1750320,39719680,-482926080,2348666880,-1903757312",
			"name": "Associated Omega numbers of order 2, triangle T(n,k) read by rows for n \u003e= 0 and 0 \u003c= k \u003c= n.",
			"comment": [
				"The Omega polynomials A318146 are defined by the recurrence P(m, 0) = 1 and for n\u003e=1 P(m, n) = x * Sum_{k=0..n-1} binomial(m*n-1, m*k)*t(m, n-k)*P(m, k) where t(m, n) are the generalized tangent numbers A318253. The Omega numbers are the coefficients of the Omega polynomials. The associated Omega numbers are the weights of P(m, k) in the recurrence formula."
			],
			"formula": [
				"T(m, n, k) = binomial(m*n-1, m*(n-k))*A318253(m, k) for k\u003e0 and 1 for k=0. We consider here the case m=2."
			],
			"example": [
				"Triangle starts:",
				"[0] [1]",
				"[1] [1,  1]",
				"[2] [1,  3,   -2]",
				"[3] [1,  5,  -20,    16]",
				"[4] [1,  7,  -70,   336,    -272]",
				"[5] [1,  9, -168,  2016,   -9792,    7936]",
				"[6] [1, 11, -330,  7392,  -89760,  436480,   -353792]",
				"[7] [1, 13, -572, 20592, -466752, 5674240, -27595776, 22368256]"
			],
			"maple": [
				"# The function TNum is defined in A318253.",
				"T := (m, n, k) -\u003e `if`(k=0, 1, binomial(m*n-1, m*(n-k))*TNum(m, k)):",
				"for n from 0 to 6 do seq(T(2, n, k), k=0..n) od;"
			],
			"program": [
				"(Sage)",
				"def AssociatedOmegaNumberTriangle(m, len):",
				"    R = ZZ[x]; B = [1]*len; L = [R(1)]*len; T = [[1]]",
				"    for k in (1..len-1):",
				"        s = x*sum(binomial(m*k-1, m*(k-j))*B[j]*L[k-j] for j in (1..k-1))",
				"        B[k] = c = 1 - s.subs(x=1); L[k] = R(expand(s + c*x))",
				"        T.append([1] + [binomial(m*k-1, m*(k-j))*B[j] for j in (1..k)])",
				"    return T",
				"A318254Triangle = lambda dim: AssociatedOmegaNumberTriangle(2, dim)",
				"print(A318254Triangle(8))"
			],
			"xref": [
				"Even-indexed rows of A220901 (up to signs).",
				"T(n, 0) = A005408, T(n, n) = A220901 (up to signs), row sums are A040000.",
				"Cf. A318146, A318253, A318255 (m=3)."
			],
			"keyword": "sign,tabl",
			"offset": "0,5",
			"author": "_Peter Luschny_, Aug 26 2018",
			"references": 2,
			"revision": 17,
			"time": "2020-03-13T17:00:09-04:00",
			"created": "2018-08-26T11:30:48-04:00"
		}
	]
}