{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A214257",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 214257,
			"data": "1,1,1,1,1,2,1,1,2,2,1,1,2,4,3,1,1,2,4,6,2,1,1,2,4,8,11,4,1,1,2,4,8,14,15,2,1,1,2,4,8,16,27,27,4,1,1,2,4,8,16,30,47,39,3,1,1,2,4,8,16,32,59,88,63,4,1,1,2,4,8,16,32,62,111,158,100,2",
			"name": "Number A(n,k) of compositions of n where the difference between largest and smallest parts is \u003c= k; square array A(n,k), n\u003e=0, k\u003e=0, read by antidiagonals.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A214257/b214257.txt\"\u003eAntidiagonals n = 0..150, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = Sum_{i=0..k} A214258(n,i)."
			],
			"example": [
				"A(3,0) =  2: [3], [1,1,1].",
				"A(4,1) =  6: [4], [2,2], [2,1,1], [1,2,1], [1,1,2], [1,1,1,1].",
				"A(5,1) =  8: [5], [3,2], [2,3], [2,2,1], [2,1,2], [2,1,1,1], [1,2,2], [1,2,1,1], [1,1,2,1], [1,1,1,2], [1,1,1,1,1],",
				"A(5,2) = 14: [5], [3,2], [3,1,1], [2,3], [2,2,1], [2,1,2], [2,1,1,1], [1,3,1], [1,2,2], [1,2,1,1], [1,1,3], [1,1,2,1], [1,1,1,2], [1,1,1,1,1].",
				"Square array A(n,k) begins:",
				"  1,  1,  1,  1,  1,  1,  1,  1, ...",
				"  1,  1,  1,  1,  1,  1,  1,  1, ...",
				"  2,  2,  2,  2,  2,  2,  2,  2, ...",
				"  2,  4,  4,  4,  4,  4,  4,  4, ...",
				"  3,  6,  8,  8,  8,  8,  8,  8, ...",
				"  2, 11, 14, 16, 16, 16, 16, 16, ...",
				"  4, 15, 27, 30, 32, 32, 32, 32, ...",
				"  2, 27, 47, 59, 62, 64, 64, 64, ..."
			],
			"maple": [
				"b:= proc(n, k, s, t) option remember;",
				"      `if`(n\u003c0, 0, `if`(n=0, 1, add(b(n-j, k,",
				"       min(s,j), max(t,j)), j=max(1, t-k+1)..s+k-1)))",
				"    end:",
				"A:= (n, k)-\u003e `if`(n=0, 1, add(b(n-j, k+1, j, j), j=1..n)):",
				"seq(seq(A(n, d-n), n=0..d), d=0..11);",
				"# second Maple program:",
				"b:= proc(n, s, t) option remember; `if`(n=0, x^(t-s),",
				"      add(b(n-j, min(s, j), max(t, j)), j=1..n))",
				"    end:",
				"T:= (n, k)-\u003e coeff(b(n$2, 0), x, k):",
				"A:= proc(n, k) option remember; `if`(k\u003c0, 0,",
				"      `if`(k\u003en, A(n$2), A(n, k-1)+T(n, k)))",
				"    end:",
				"seq(seq(A(n, d-n), n=0..d), d=0..11);  # _Alois P. Heinz_, Jan 05 2019"
			],
			"mathematica": [
				"b[n_, k_, s_, t_] := b[n, k, s, t] = If[n \u003c 0, 0, If[n == 0, 1, Sum [b[n - j, k, Min[s, j], Max[t, j]], {j, Max[1, t - k + 1], s + k - 1}]]]; A[n_, k_] := If[n == 0, 1, Sum[b[n - j, k + 1, j, j], {j, 1, n}]]; Table[Table[A[n, d - n], {n, 0, d}], {d, 0, 11}] // Flatten (* _Jean-François Alcover_, Dec 27 2013, translated from Maple *)"
			],
			"xref": [
				"Columns k=0-1 give: A000005, A072951.",
				"Main diagonal gives: A011782.",
				"Cf. A214246, A214247, A214248, A214249, A214258, A214268, A214269."
			],
			"keyword": "nonn,tabl",
			"offset": "0,6",
			"author": "_Alois P. Heinz_, Jul 08 2012",
			"references": 8,
			"revision": 22,
			"time": "2019-01-05T14:51:14-05:00",
			"created": "2012-07-10T09:13:34-04:00"
		}
	]
}