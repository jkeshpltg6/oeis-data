{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A194094",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 194094,
			"data": "1,-4,20,-64,164,-392,896,-1920,3908,-7684,14632,-27072,48896,-86408,149760,-255104,427652,-706568,1152020,-1855296,2954056,-4654080,7260288,-11221632,17194496,-26131980,39409960,-59003008,87728640,-129586568,190226176,-277587456,402779396,-581276160,834539560,-1192216320",
			"name": "Expansion of (2/Pi)*elliptic_E(k) in powers of q.",
			"comment": [
				"Let s = 16*q*(E1*E4^2/E2^3)^8 where Ek = prod(n\u003e=1, 1-q^(k*n) ) (s=k^2 where k is elliptic k), then the g.f. is hypergeom([-1/2, +1/2], [+1], s) (expansion of 2/Pi*elliptic_E(k) in powers of q)."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A194094/b194094.txt\"\u003eTable of n, a(n) for n = 0..2000\u003c/a\u003e"
			],
			"formula": [
				"Expansion of theta_3(q)^2 - 2 * (theta_4(q) / theta_3(q))^2 * Dq ( theta_4(q)^-2 ) = theta_3(q)^2 + 4 Dq (theta_4(q)) / (theta_4(q) * theta_3(q)^2) in powers of q where Dq (f) := q * df/dq. - _Michael Somos_, Jan 24 2012",
				"Expansion of (T4^4 * T3 + 4*q * d/dq T3) / T3^3 where T3 = theta_3(q) and T4 = theta_4(q). - _Joerg Arndt_, Sep 02 2015",
				"a(n) ~ (-1)^n * exp(Pi*sqrt(2*n)) / (Pi * 2^(9/4) * n^(3/4)). - _Vaclav Kotesovec_, Oct 06 2019"
			],
			"example": [
				"E(k(q)) = 1 - 4*q + 20*q^2 - 64*q^3 + 164*q^4 - 392*q^5 + 896*q^6 - 1920*q^7 +- ..."
			],
			"maple": [
				"N:= 100: # to get a(0) to a(N)",
				"t3:= curry(JacobiTheta3,0):",
				"t4:= curry(JacobiTheta4,0):",
				"Dq:= f -\u003e q*diff(f,q):",
				"E1:= t3(q)^2:",
				"E2a:= - 2*(t4(q)/t3(q))^2:",
				"E2b:= t4(q)^(-2):",
				"S1:= series(E1,q,N+1):",
				"S2a:= series(E2a,q,N+1):",
				"S2b:= series(Dq(series(E2b,q,N+1)),q,N+1):",
				"S:= series(S1+S2a*S2b,q,N+1):",
				"seq(coeff(S,q,j),j=0..N); # _Robert Israel_, Sep 02 2015"
			],
			"mathematica": [
				"a[ n_] := With[ {m = InverseEllipticNomeQ @ q}, SeriesCoefficient[ EllipticE[m] / (Pi/2), {q, 0, n}]] (* _Michael Somos_, Jan 24 2012 *)",
				"a[ n_] := SeriesCoefficient[ Hypergeometric2F1[ -1/2, 1/2, 1, ModularLambda[ Log[q] / (Pi I)]], {q, 0, n}] (* _Michael Somos_, Jan 24 2012 *)",
				"nmax = 30; dtheta = D[Normal[Series[EllipticTheta[3, 0, x], {x, 0, nmax}]], x]; CoefficientList[Series[(EllipticTheta[4, 0, x]^4 * EllipticTheta[3, 0, x] + 4*x*dtheta) / EllipticTheta[3, 0, x]^3, {x, 0, nmax}], x] (* _Vaclav Kotesovec_, Apr 10 2018 *)"
			],
			"xref": [
				"Cf. A004018 (elliptic K(k(q))), A115977 (elliptic k(q)^2)."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Joerg Arndt_, Aug 15 2011",
			"references": 8,
			"revision": 28,
			"time": "2019-10-06T03:12:56-04:00",
			"created": "2011-08-15T05:00:11-04:00"
		}
	]
}