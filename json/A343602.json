{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A343602",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 343602,
			"data": "0,1,-2,3,4,-11,-8,-5,-6,9,12,7,10,13,-38,-35,-32,-29,-26,-23,-20,-17,-14,-33,-24,-15,-18,27,36,21,30,39,16,19,22,25,28,31,34,37,40,-119,-116,-113,-110,-107,-104,-101,-98,-95,-92,-89,-86,-83,-80,-77,-74",
			"name": "For any positive number n, the balanced ternary representation of a(n) is obtained by left-rotating the balanced ternary representation of n until a nonzero digit appears again as the leftmost digit; a(0) = 0.",
			"comment": [
				"This sequence can be extended to negative indexes by setting a(-n) = -a(n) for any n \u003e 0. We then obtain a permutation of the integers (Z) with inverse A343601 (after a similar extension to negative indexes)."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A343602/b343602.txt\"\u003eTable of n, a(n) for n = 0..9841\u003c/a\u003e"
			],
			"formula": [
				"A065363(a(n)) = A065363(n).",
				"A134021(a(n)) = A134021(n).",
				"a^k(n) = n for k = A005812(n) (where a^k denotes the k-th iterate of a)."
			],
			"example": [
				"The first terms, in base 10 and in balanced ternary (where T denotes the digit -1), are:",
				"  n   a(n)  bter(n)  bter(a(n))",
				"  --  ----  -------  ----------",
				"   0     0        0           0",
				"   1     1        1           1",
				"   2    -2       1T          T1",
				"   3     3       10          10",
				"   4     4       11          11",
				"   5   -11      1TT         TT1",
				"   6    -8      1T0         T01",
				"   7    -5      1T1         T11",
				"   8    -6      10T         T10",
				"   9     9      100         100",
				"  10    12      101         110",
				"  11     7      11T         1T1",
				"  12    10      110         101",
				"  13    13      111         111",
				"  14   -38     1TTT        TTT1",
				"  15   -35     1TT0        TT01"
			],
			"program": [
				"(PARI) a(n) = { my (d = [], t); while (n, d = concat(t = centerlift(Mod(n,3)), d); n = (n-t)\\3); for (k=2, #d, if (d[k], return (fromdigits(concat(d[k..#d], d[1..k-1]), 3)))); return (fromdigits(d, 3)) }"
			],
			"xref": [
				"Cf. A005812, A065363, A134021, A139708 (binary variant), A343600 (ternary variant), A343603 (inverse)."
			],
			"keyword": "sign,base",
			"offset": "0,3",
			"author": "_Rémy Sigrist_, Apr 21 2021",
			"references": 2,
			"revision": 13,
			"time": "2021-04-23T01:20:35-04:00",
			"created": "2021-04-22T21:57:31-04:00"
		}
	]
}