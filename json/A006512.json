{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006512",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6512,
			"id": "M3763",
			"data": "5,7,13,19,31,43,61,73,103,109,139,151,181,193,199,229,241,271,283,313,349,421,433,463,523,571,601,619,643,661,811,823,829,859,883,1021,1033,1051,1063,1093,1153,1231,1279,1291,1303,1321,1429,1453,1483,1489,1609",
			"name": "Greater of twin primes.",
			"comment": [
				"Also primes that are the sum of two primes (which is possible only if 2 is one of the primes). - _Cino Hilliard_, Jul 02 2004, edited by _M. F. Hasler_, Nov 14 2019",
				"The set of greater of twin primes larger than five is a proper subset of the set of primes of the form 3n + 1 (A002476). - _Paul Muljadi_, Jun 05 2008",
				"Smallest prime \u003e n-th isolated composite. - _Juri-Stepan Gerasimov_, Nov 07 2009",
				"Subsequence of A175075. Union of a(n) and sequence A175080 is A175075. - _Jaroslav Krizek_, Jan 30 2010",
				"A164292(a(n))=1; A010051(a(n)+2)=0 for n \u003e 1. - _Reinhard Zumkeller_, Mar 29 2010",
				"Omega(n) = Omega(n-2); d(n) = d(n-2). - _Juri-Stepan Gerasimov_, Sep 19 2010",
				"Solutions of the equation (n-2)'+n' = 2, where n' is the arithmetic derivative of n. - _Paolo P. Lava_, Dec 18 2012",
				"Aside from the first term, all subsequent terms have digital root 1, 4, or 7. - _J. W. Helkenberg_, Jul 24 2013",
				"Also primes p with property that the sum of the successive gaps between primes \u003c= p is a prime number. - _Robert G. Wilson v_, Dec 19 2014",
				"The phrase \"x is an element of the {primes, positive integers} and there {exist no, exist} elements a,b of {1 and primes, primes}: a+b=x\" determines A133410, A067829, A025584, A006512, A166081, A014092, A014091 and A038609 for the first few hundred terms with only de-duplication or omitting/including 3, 4 and 6 in the case of A166081/A014091 and one case of omitting/including 3 given 1 isn't prime. - _Harry G. Coin_, Nov 25 2015",
				"The yet unproved Twin Prime Conjecture states that this sequence is infinite. - _M. F. Hasler_, Nov 14 2019"
			],
			"reference": [
				"See A001359 for further references and links.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A006512/b006512.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Harvey Dubner, \u003ca href=\"http://www.emis.de/journals/JIS/VOL8/Dubner/dubner71.html\"\u003eTwin Prime Statistics\u003c/a\u003e, Journal of Integer Sequences, Vol. 8 (2005), Article 05.4.2.",
				"R. K. Guy, \u003ca href=\"/A006511/a006511.pdf\"\u003eLetter to N. J. A. Sloane, Jun 1991\u003c/a\u003e",
				"Omar E. Pol, \u003ca href=\"http://www.polprimos.com\"\u003eDeterminacion geometrica de los numeros primos y perfectos\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Twin_prime#History\"\u003eTwin prime\u003c/a\u003e.",
				"\u003ca href=\"/index/Pri#gaps\"\u003eIndex entries for primes, gaps between\u003c/a\u003e"
			],
			"maple": [
				"for i from 1 to 253 do if ithprime(i+1) = ithprime(i) + 2 then print({ithprime(i+1)}); fi; od; # _Zerinvary Lajos_, Mar 19 2007",
				"P := select(isprime,[$1..1609]): select(p-\u003emember(p-2,P),P); # _Peter Luschny_, Mar 03 2011",
				"A006512 := proc(n)",
				"    2+A001359(n) ;",
				"end proc: # _R. J. Mathar_, Nov 26 2014"
			],
			"mathematica": [
				"Select[Prime[Range[254]], PrimeQ[# - 2] \u0026] (* _Robert G. Wilson v_, Jun 09 2005 *)",
				"Transpose[Select[Partition[Prime[Range[300]], 2, 1], Last[#] - First[#] == 2 \u0026]][[2]] (* _Harvey P. Dale_, Nov 02 2011 *)",
				"Cases[Prime[Range[500]] + 2, _?PrimeQ] (* _Fred Patrick Doty_, Aug 23 2017 *)"
			],
			"program": [
				"(PARI) select(p-\u003eisprime(p-2),primes(1000))",
				"(MAGMA) [n: n in PrimesUpTo(1610)|IsPrime(n-2)]; // _Bruno Berselli_, Feb 28 2011",
				"(Haskell)",
				"a006512 = (+ 2) . a001359 -- _Reinhard Zumkeller_, Feb 10 2015",
				"(PARI) a(n)=p=3; while(p+2 \u003c (p=nextprime(p+1)) || n--\u003e0, ); p",
				"vector(100, n, a(n)) \\\\ _Altug Alkan_, Dec 04 2015",
				"(Python)",
				"from sympy import primerange, isprime",
				"print([n for n in primerange(1, 2001) if isprime(n - 2)]) # _Indranil Ghosh_, Jul 20 2017"
			],
			"xref": [
				"Subsequence of A139690.",
				"Bisection of A077800.",
				"Cf. A001097, A001359, A014574, A067829, A002476."
			],
			"keyword": "nonn,nice,easy",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_",
			"references": 419,
			"revision": 155,
			"time": "2021-02-02T22:39:21-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}