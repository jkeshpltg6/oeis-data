{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A192404",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 192404,
			"data": "1,1,1,1,2,1,1,4,5,1,1,7,14,10,1,1,11,31,38,17,1,1,16,61,114,91,26,1,1,22,111,291,357,196,37,1,1,29,190,656,1131,971,384,50,1,1,37,309,1345,3092,3771,2367,694,65,1,1,46,481,2563,7575,12393,11150,5286,1173,82,1",
			"name": "G.f. satisfies: A(x,y) = 1 + Sum_{n\u003e=1} x^n*y*A(x,y)^n/(1 - y*A(x,y)^(2*n)), where A(x,y) = 1 + Sum_{n\u003e=1,k\u003e=1} T(n,k)*x^n*y^k; here the coefficients T(n,k) form a square array and are read by antidiagonals.",
			"comment": [
				"Related q-series identity:",
				"Sum_{n\u003e=1} x^n*y*q^n/(1-y*q^(2*n)) = Sum_{n\u003e=1} y^n*x*q^(2*n-1)/(1-x*q^(2*n-1)); here q=A(x,y)."
			],
			"formula": [
				"G.f. satisfies: A(x,y) = 1 + Sum_{n\u003e=1} y^n*x*A(x,y)^(2*n-1)/(1 - x*A(x,y)^(2*n-1))."
			],
			"example": [
				"Let A = g.f. A(x,y), then A satisfies the following relations:",
				"A = 1 + x*y*A/(1-y*A^2) + x^2*y*A^2/(1-y*A^4) + x^3*y*A^3/(1-y*A^6) +...",
				"A = 1 + y*x*A/(1-x*A) + y^2*x*A^3/(1-x*A^3) + y^3*x*A^5/(1-x*A^5) +...",
				"The square array of coefficients in A(x,y) begins:",
				"[1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,...];",
				"[0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,...];",
				"[0,1,2,5,10,17,26,37,50,65,82,101,122,145,170,197,226,257,...];",
				"[0,1,4,14,38,91,196,384,694,1173,1876,2866,4214,5999,8308,...];",
				"[0,1,7,31,114,357,971,2367,5286,10969,21367,39391,69202,...];",
				"[0,1,11,61,291,1131,3771,11150,29828,73329,167767,360791,...];",
				"[0,1,16,111,656,3092,12393,43464,136434,390259,1031194,...];",
				"[0,1,22,190,1345,7575,35839,146712,533050,1751371,5278494,...];",
				"[0,1,29,309,2563,17011,93599,441925,1838094,6865479,...];",
				"[0,1,37,481,4609,35563,224947,1212807,5721008,24088842,...];",
				"[0,1,46,721,7906,70021,504448,3078603,16340045,77009425,...]; ...",
				"in which the zeroth row and column are omitted from this sequence.",
				"EXPLICIT EXPANSIONS of the generating function are as follows.",
				"SUMMATION ALONG ROWS gives:",
				"A(x,y) = 1 + x*y/(1-y) + x^2*(y - y^2 + 2*y^3)/(1-y)^3 + x^3*(y - y^2 + 4*y^3 - 2*y^4 + 6*y^5)/(1-y)^5 + x^4*(y + 3*y^3 + 9*y^4 + 5*y^6 + 22*y^7)/(1-y)^7 + x^5*(y + 2*y^2 - 2*y^3 + 54*y^4 - 90*y^5 + 204*y^6 - 133*y^7 + 98*y^8 + 90*y^9)/(1-y)^9 +...",
				"in which the denominator polynomials are the odd powers of (1-y).",
				"The coefficients of y in the above numerator polynomials begin:",
				"[1];",
				"[1,-1,2];",
				"[1,-1,4,-2,6];",
				"[1,0,3,9,0,5,22];",
				"[1,2,-2,54,-90,204,-133,98,90];",
				"[1,5,-10,150,-329,964,-1339,2025,-1385,868,394];",
				"[1,9,-18,305,-667,2377,-3763,7967,-10012,14378,-10323,6388,1806];",
				"[1,14,-21,518,-819,3536,-2367,6387,-1660,20583,-35708,77417,-64888,43361,8558];...",
				"in which the row sums form A052701 (related to Catalan numbers), and the rightmost border forms the large Schroeder numbers (A006318).",
				"SUMMATION ALONG COLUMNS gives:",
				"A(x,y) = 1 + y*x/(1-x) + y^2*(x - x^2 + x^3)/(1-x)^3 + y^3*(x - x^3 + x^4 + x^5)/(1-x)^5 + y^4*(x + 3*x^2 - 11*x^3 + 23*x^4 - 24*x^5 + 12*x^6 + x^7)/(1-x)^7 + y^5*(x + 8*x^2 - 26*x^3 + 66*x^4 - 108*x^5 + 137*x^6 - 117*x^7 + 52*x^8 + x^9)/(1-x)^9 ...",
				"in which the denominator polynomials are the odd powers of (1-x).",
				"The coefficients of x in the above numerator polynomials begin:",
				"[1];",
				"[1,-1,1];",
				"[1,0,-1,1,1];",
				"[1,3,-11,23,-24,12,1];",
				"[1,8,-26,66,-108,137,-117,52,1];",
				"[1,15,-35,80,-90,95,-164,330,-377,186,1];",
				"[1,24,-19,-25,464,-1516,3075,-4066,3274,-928,-778,625,1];",
				"[1,35,49,-329,2023,-6479,15515,-28703,41895,-46744,36552,-15870,429,2054,1];...",
				"in which the row sums form the Catalan numbers (A000108)."
			],
			"program": [
				"(PARI) {T(n,k)=local(A=1+x*y);for(i=1,n+k,A=1+sum(m=1,n+k,x^m*y*A^m/(1-y*A^(2*m)+x*O(x^n)+y*O(y^k))));polcoeff(polcoeff(A,n,x),k,y)}",
				"(PARI) {T(n,k)=local(A=1+x*y);for(i=1,n+k,A=1+sum(m=1,n+k,y^m*x*A^(2*m-1)/(1-x*A^(2*m-1)+x*O(x^n)+y*O(y^k))));polcoeff(polcoeff(A,k,y),n,x)}",
				"/* Print the coefficients as a square array: */",
				"{for(n=1,12,for(k=1,18-n,print1(T(n,k),\",\"));print(\"\"))}",
				"/* Print the array in flattened format: */",
				"{for(n=1,12,for(k=1,n,print1(T(n-k+1,k),\",\"));)}"
			],
			"xref": [
				"Cf. A192405 (antidiagonal sums), A192406 (main diagonal), A192407."
			],
			"keyword": "nonn,tabl",
			"offset": "1,5",
			"author": "_Paul D. Hanna_, Jun 30 2011",
			"references": 3,
			"revision": 15,
			"time": "2014-05-11T08:31:35-04:00",
			"created": "2011-07-01T03:47:56-04:00"
		}
	]
}