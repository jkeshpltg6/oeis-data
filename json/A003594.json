{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003594",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3594,
			"data": "1,3,7,9,21,27,49,63,81,147,189,243,343,441,567,729,1029,1323,1701,2187,2401,3087,3969,5103,6561,7203,9261,11907,15309,16807,19683,21609,27783,35721,45927,50421,59049,64827,83349,107163,117649",
			"name": "Numbers of the form 3^i*7^j with i, j \u003e= 0.",
			"link": [
				"Vincenzo Librandi and Reinhard Zumkeller, \u003ca href=\"/A003594/b003594.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e, first 70 terms from Vincenzo Librandi",
				"Vaclav Kotesovec, \u003ca href=\"/A003594/a003594.jpg\"\u003eGraph - the asymptotic ratio (600000 terms)\u003c/a\u003e"
			],
			"formula": [
				"The characteristic function of this sequence is given by Sum_{n \u003e= 1} x^a(n) = Sum_{n \u003e= 1} mu(21*n)*x^n/(1 - x^n), where mu(n) is the Möbius function A008683. Cf. with the formula of Hanna in A051037. - _Peter Bala_, Mar 18 2019",
				"Sum_{n\u003e=1} 1/a(n) = (3*7)/((3-1)*(7-1)) = 7/4. - _Amiram Eldar_, Sep 22 2020",
				"a(n) ~ exp(sqrt(2*log(3)*log(7)*n)) / sqrt(21). - _Vaclav Kotesovec_, Sep 22 2020"
			],
			"mathematica": [
				"f[upto_]:=Sort[Select[Flatten[3^First[#] 7^Last[#] \u0026 /@ Tuples[{Range[0, Floor[Log[3, upto]]], Range[0, Floor[Log[7, upto]]]}]], # \u003c= upto \u0026]]; f[120000]  (* _Harvey P. Dale_, Mar 04 2011 *)",
				"fQ[n_] := PowerMod[21, n, n] == 0; Select[Range[120000], fQ] (* _Bruno Berselli_, Sep 24 2012 *)"
			],
			"program": [
				"(PARI) list(lim)=my(v=List(),N);for(n=0,log(lim)\\log(7),N=7^n;while(N\u003c=lim,listput(v,N);N*=3));vecsort(Vec(v)) \\\\ _Charles R Greathouse IV_, Jun 28 2011",
				"(MAGMA) [n: n in [1..120000] | PrimeDivisors(n) subset [3,7]]; // _Bruno Berselli_, Sep 24 2012",
				"(Haskell)",
				"import Data.Set (singleton, deleteFindMin, insert)",
				"a003594 n = a003594_list !! (n-1)",
				"a003594_list = f $ singleton 1 where",
				"   f s = y : f (insert (3 * y) $ insert (7 * y) s')",
				"               where (y, s') = deleteFindMin s",
				"-- _Reinhard Zumkeller_, May 16 2015",
				"(GAP) Filtered([1..120000],n-\u003ePowerMod(21,n,n)=0); # _Muniru A Asiru_, Mar 19 2019"
			],
			"xref": [
				"Cf. A003586, A003591, A003592, A003593, A003595."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_.",
			"references": 19,
			"revision": 46,
			"time": "2020-09-22T03:46:28-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}