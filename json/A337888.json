{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A337888",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 337888,
			"data": "1,2,1,3,10,1,4,56,49127,1,5,220,740360358,314824532572147370464,1,6,680,733776248840,38491882660671134164965704408524083,38343035259947576596859948806931124970404417593861154473053467181056,1",
			"name": "Array read by descending antidiagonals: T(n,k) is the number of unoriented colorings of the square faces of a regular n-dimensional orthotope (hypercube) using k or fewer colors.",
			"comment": [
				"Each chiral pair is counted as one when enumerating unoriented arrangements. Each face is a square bounded by four edges. For n=2, the figure is a square with one face. For n=3, the figure is a cube with 6 faces. For n=4, the figure is a tesseract with 24 faces. The number of faces is 2^(n-2)*C(n,2).",
				"Also the number of unoriented colorings of peaks of an n-dimensional orthoplex. A peak is an (n-3)-dimensional simplex."
			],
			"link": [
				"K. Balasubramanian, \u003ca href=\"https://doi.org/10.33187/jmsm.471940\"\u003eComputational enumeration of colorings of hyperplanes of hypercubes for all irreducible representations and applications\u003c/a\u003e, J. Math. Sci. \u0026 Mod. 1 (2018), 158-180."
			],
			"formula": [
				"The algorithm used in the Mathematica program below assigns each permutation of the axes to a partition of n and then considers separate conjugacy classes for axis reversals. It uses the formulas in Balasubramanian's paper. If the value of m is increased, one can enumerate colorings of higher-dimensional elements beginning with T(m,1).",
				"T(n,k) = A337887(n,k) - A337889(n,k) = (A337887(n,k) + A337890(n,k)) / 2 = A337889(n,k) + A337890(n,k)."
			],
			"example": [
				"Array begins with T(2,1):",
				" 1     2         3            4               5                 6 ...",
				" 1    10        56          220             680              1771 ...",
				" 1 49127 740360358 733776248840 155261523065875 12340612271439081 ..."
			],
			"mathematica": [
				"m=2; (* dimension of color element, here a square face *)",
				"Fi1[p1_] := Module[{g, h}, Coefficient[Product[g = GCD[k1, p1]; h = GCD[2 k1, p1]; (1 + 2 x^(k1/g))^(r1[[k1]] g) If[Divisible[k1, h], 1, (1+2x^(2 k1/h))^(r2[[k1]] h/2)], {k1, Flatten[Position[cs, n1_ /; n1 \u003e 0]]}], x, n - m]];",
				"FiSum[] := (Do[Fi2[k2] = Fi1[k2], {k2, Divisors[per]}];DivisorSum[per, DivisorSum[d1 = #, MoebiusMu[d1/#] Fi2[#] \u0026]/# \u0026]);",
				"CCPol[r_List] := (r1 = r; r2 = cs - r1; per = LCM @@ Table[If[cs[[j2]] == r1[[j2]], If[0 == cs[[j2]],1,j2], 2j2], {j2,n}]; Times @@ Binomial[cs, r1] 2^(n-Total[cs]) b^FiSum[]);",
				"PartPol[p_List] := (cs = Count[p, #]\u0026/@ Range[n]; Total[CCPol[#]\u0026/@ Tuples[Range[0,cs]]]);",
				"pc[p_List] := Module[{ci, mb}, mb = DeleteDuplicates[p]; ci = Count[p, #]\u0026/@ mb; n!/(Times@@(ci!) Times@@(mb^ci))] (*partition count*)",
				"row[n_Integer] := row[n] = Factor[(Total[(PartPol[#] pc[#])\u0026/@ IntegerPartitions[n]])/(n! 2^n)]",
				"array[n_, k_] := row[n] /. b -\u003e k",
				"Table[array[n,d+m-n], {d,6}, {n,m,d+m-1}] // Flatten"
			],
			"xref": [
				"Cf. A337887 (oriented), A337889 (chiral), A337890 (achiral).",
				"Other elements: A325013 (vertices), A337408 (edges).",
				"Other polytopes: A337884 (simplex), A337892 (orthoplex).",
				"Rows 2-4 are A000027, A198833, A331355."
			],
			"keyword": "tabl,nonn",
			"offset": "2,2",
			"author": "_Robert A. Russell_, Sep 28 2020",
			"references": 7,
			"revision": 4,
			"time": "2020-09-28T21:41:34-04:00",
			"created": "2020-09-28T21:41:34-04:00"
		}
	]
}