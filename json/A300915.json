{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A300915",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 300915,
			"data": "1,6,12,24,60,72,168,96,324,360,660,288,1092,1008,720,768,2448,1944,3420,1440,2016,3960,6072,1152,7500,6552,8748,4032,12180,4320,14880,6144,7920,14688,10080,7776,25308,20520,13104,5760",
			"name": "Order of the group PSL(2,Z_n).",
			"comment": [
				"The projective special linear group PSL(2,Z_n) is the quotient group of SL(2,Z_n) with its center.  The center of SL(2,Z_n) is the group of scalar matrices whose diagonal entry is x in Z_n such that x^2 = 1.  The elements of PSL(2,Z_n) are equivalence classes of 2 X 2 matrices with entries in Z_n where two matrices are equivalent if one is a scalar multiple of the other."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A300915/b300915.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Geoffrey Critzer, \u003ca href=\"https://esirc.emporia.edu/handle/123456789/3595\"\u003eCombinatorics of Vector Spaces over Finite Fields\u003c/a\u003e, Master's thesis, Emporia State University, 2018."
			],
			"formula": [
				"a(n) = A000056(n)/A060594(n)."
			],
			"mathematica": [
				"n := 2; nn = 40; \\[Gamma][n_, q_] := Product[q^n - q^i, {i, 0, n - 1}]; Prepend[ Table[Product[ FactorInteger[m][[All, 1]][[j]]^(n^2 (FactorInteger[m][[All, 2]][[j]] - 1)) \\[Gamma][n,FactorInteger[m][[All, 1]][[j]]], {j, 1, PrimeNu[m]}], {m, 2, nn}]/Table[EulerPhi[m], {m, 2, nn}]/ Table[Count[Mod[Select[Range[m], GCD[#, m] == 1 \u0026]^n, m], 1], {m, 2, nn}], 1]"
			],
			"program": [
				"(PARI) a(n) = {my(f=factor(n)); prod(i=1, #f~, my([p,e]=f[i,]); (p^2-1)*p^(3*e-2)/if(p==2, 2^min(2, e-1), 2))} \\\\ _Andrew Howroyd_, Aug 01 2018"
			],
			"xref": [
				"Cf. A000056, A060594."
			],
			"keyword": "nonn,mult",
			"offset": "1,2",
			"author": "_Geoffrey Critzer_, Mar 16 2018",
			"ext": [
				"Keyword:mult added by _Andrew Howroyd_, Aug 01 2018"
			],
			"references": 1,
			"revision": 26,
			"time": "2018-08-27T14:40:58-04:00",
			"created": "2018-03-16T10:26:55-04:00"
		}
	]
}