{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A219792",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 219792,
			"data": "1,1,3,3,5,3,7,3,7,5,11,3,13,7,5,5,17,7,19,5,7,11,23,3,25,13,19,7,29,5,31,15,11,17,13,7,37,19,13,5,41,7,43,11,13,23,47,5,43,25,17,13,53,19,25,7,19,29,59,5,61,31,7,17,13,11,67,17,23,13,71,7",
			"name": "Least k such that phi(k) = lambda(n), or 0 if there is no such k.",
			"comment": [
				"lambda(n) is the Carmichael lambda function A002322.",
				"a(n) = 0 for n = 209, 297, 413, 418, 517, ...",
				"If a(n) = p is a prime greater than 2, then n belongs to the finite set {p, p1, p2, ..., pk} that is a subsequence of A143417 (see the b-file in A143417). For example:",
				"a(n) = 3 for n = 3, 4, 6;",
				"a(n) = 5 for n = 5, 10, 15, 16, 20, 30, 40, 48, 60, 80, 120, 240;",
				"a(n) = 7 for n = 7, 9, 14, 18, 21, 28, ..., 480;",
				"a(n) = 11 for n = 11, 22, 33, 44, 66, 88, 132, 264;",
				"a(n) = 13 for n = 13, 26, 35, 39, ..., 65520."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A219792/b219792.txt\"\u003eTable of n, a(n) for n = 1..16384\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A049283(A002322(n))."
			],
			"example": [
				"a(6) = 3 because phi(3) = lambda(6) = 2."
			],
			"maple": [
				"with(numtheory): for n from 1 to 100 do: ii:=0:for k from 1 to 10^6 while(ii=0) do:if phi(k)=lambda(n) then ii:=1: printf(`%d, `,k):else fi:od:if ii=0 then printf(`%d, `,0): else fi:od:"
			],
			"mathematica": [
				"Table[k=0;While[!EulerPhi[k]==CarmichaelLambda[n],k++];k,{n,100}]",
				"Join[{1},Module[{nn=100,ep,lam},ep=Table[{k,EulerPhi[k]},{k,nn}];Table[ SelectFirst[ep,#[[2]]==CarmichaelLambda[n]\u0026],{n,2,nn}]][[All,1]]] (* _Harvey P. Dale_, Dec 24 2021 *)"
			],
			"program": [
				"(PARI) a(n)=my(t=lcm(znstar(n)[2]));if(t\u003e2,for(k=t+1,solve(x=t,2*t^2,x/(exp(Euler)*log(log(x))+3/log(log(x)))-t),if(eulerphi(k)==t,return(k)));0,2*t-1) \\\\ _Charles R Greathouse IV_, Nov 28 2012",
				"(PARI)",
				"A014197(n, m=1) = { n==1 \u0026\u0026 return(1+(m\u003c2)); my(p, q); sumdiv(n, d, if( d\u003e=m \u0026\u0026 isprime(d+1), sum( i=0, valuation(q=n\\d, p=d+1), A014197(q\\p^i, p))))} \\\\ This function from _M. F. Hasler_, Oct 05 2009",
				"A219792(n) = { my(x=lcm(znstar(n)[2])); if(0==A014197(x),0,for(k=1,oo,if(eulerphi(k)==x,return(k)))); }; \\\\ _Antti Karttunen_, Dec 05 2017"
			],
			"xref": [
				"Cf. A000010, A002322, A014197, A049283."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Michel Lagneau_, Nov 28 2012",
			"references": 1,
			"revision": 17,
			"time": "2021-12-24T14:54:14-05:00",
			"created": "2012-12-04T15:34:44-05:00"
		}
	]
}