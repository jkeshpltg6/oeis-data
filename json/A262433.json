{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A262433",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 262433,
			"data": "3,11,13,21,31,101,111,113,123,133,201,211,213,223,233,301,321,323,331,1003,1011,1021,1031,1033,1101,1113,1123,1131,1133,1201,1203,1213,1223,1231,1233,1311,1321,1323,2001,2011,2031,2033,2103,2113,2131,2133,2203",
			"name": "Quater-imaginary representation of the Gaussian primes with an even imaginary part.",
			"comment": [
				"Not all Gaussian primes will be in this list as complex numbers with an odd imaginary part require a value after the radix point (\".\") in the quater-imaginary number system."
			],
			"link": [
				"Donald Knuth, \u003ca href=\"http://dl.acm.org/citation.cfm?id=367233\"\u003eAn imaginary number system\u003c/a\u003e, Communications of the ACM 3 (4), April 1960, pp. 245-247.",
				"OEIS Wiki, \u003ca href=\"/wiki/Quater-imaginary_base\"\u003eQuater-imaginary base\u003c/a\u003e",
				"OEIS Wiki, \u003ca href=\"https://oeis.org/wiki/Gaussian_primes\"\u003eGaussian primes\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Quater-imaginary_base\"\u003eQuater-imaginary base\u003c/a\u003e"
			],
			"example": [
				"1231_(2i) = 1(2i)^3 + 2(2i)^2 + 3(2i)^1 + 1(2i)^0 = -7-2i which is a Gaussian prime."
			],
			"program": [
				"(C++)",
				"#include \u003cstdlib.h\u003e",
				"#include \u003cmath.h\u003e",
				"#include \u003ciostream\u003e",
				"#include \u003cfstream\u003e",
				"using namespace std;",
				"bool isPrime(int n)",
				"{",
				"  if (n \u003c= 1)",
				"    return false;",
				"  if (n == 2)",
				"    return true;",
				"  if (n % 2 == 0)",
				"    return false;",
				"  int rootNCeil = (int)sqrt(n) + 1;",
				"  for (int div = 3; div \u003c= rootNCeil;div+=2)",
				"  {",
				"    if (n % div == 0)",
				"      return false;",
				"  }",
				"  return true;",
				"}",
				"int main()",
				"{",
				"  const int maxDigits = 8;",
				"  unsigned int maxVal = 2 \u003c\u003c ((maxDigits * 2) - 1);",
				"  for (unsigned int n = 0; n \u003c maxVal; n++)",
				"  {",
				"    // Split binary representation of n into real part and imaginary part",
				"    int rp = (n \u0026 0x00000003) - ((n \u0026 0x00000030) \u003e\u003e 2) +",
				"      ((n \u0026 0x00000300) \u003e\u003e 4) - ((n \u0026 0x00003000) \u003e\u003e 6) +",
				"      ((n \u0026 0x00030000) \u003e\u003e 8) - ((n \u0026 0x00300000) \u003e\u003e 10) +",
				"      ((n \u0026 0x03000000) \u003e\u003e 12) - ((n \u0026 0x30000000) \u003e\u003e 14);",
				"    int ip = ((n \u0026 0x0000000c) \u003e\u003e 1) - ((n \u0026 0x000000c0) \u003e\u003e 3) +",
				"      ((n \u0026 0x00000c00) \u003e\u003e 5) - ((n \u0026 0x0000c000) \u003e\u003e 7) +",
				"      ((n \u0026 0x000c0000) \u003e\u003e 9) - ((n \u0026 0x00c00000) \u003e\u003e 11) +",
				"      ((n \u0026 0x0c000000) \u003e\u003e 13) - ((n \u0026 0xc0000000) \u003e\u003e 15);",
				"    if ((ip == 0 \u0026\u0026 (abs(rp) % 4 == 3) \u0026\u0026 isPrime(abs(rp))) ||",
				"        (rp == 0 \u0026\u0026 (abs(ip) % 4 == 3) \u0026\u0026 isPrime(abs(ip))) ||",
				"        (rp != 0 \u0026\u0026 ip != 0 \u0026\u0026 isPrime(rp*rp + ip*ip)) )",
				"    {",
				"      char digits[maxDigits + 1];",
				"      _itoa(n, digits, 4);",
				"      cout\u003c\u003cdigits \u003c\u003c \",\" \u003c\u003c rp \u003c\u003c (ip \u003e= 0 ? \"+\" : \"\") \u003c\u003c ip \u003c\u003c \"i\\n\";",
				"    }",
				"  }",
				"}"
			],
			"xref": [
				"A002145 when translated using A212494 is a subsequence.",
				"Real and imaginary parts of the Gaussian primes A103431, A103432."
			],
			"keyword": "nonn,base",
			"offset": "1,1",
			"author": "_Adam J.T. Partridge_, Sep 22 2015",
			"references": 0,
			"revision": 21,
			"time": "2015-10-23T11:28:50-04:00",
			"created": "2015-10-23T11:28:50-04:00"
		}
	]
}