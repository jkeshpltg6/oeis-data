{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A058306",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 58306,
			"data": "12,1,1,3,2,1,1,1,1,1,1,1,3,1,1,1,2,1,1,1,1,1,1,1,1,1,1,3,1,1,1,1,1,1,1,1,2,1,1,1,1,1,1,1,1,1,1,1,3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,1,1,1,1,1,1,1,1,1,1,3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1",
			"name": "Denominator of H(n), where H(0)=-1/12, H(n) = number of equivalence classes of positive definite quadratic forms a*x^2+b*x*y+c*y^2 with discriminant b^2-4ac = -n, counting forms equivalent to x^2+y^2 (resp. x^2+x*y+y^2) with multiplicity 1/2 (resp. 1/3).",
			"comment": [
				"H(n) is usually called the Hurwitz class number.",
				"a(n) = 1 unless n is of the form 3k^2 or 4k^2. - _Charles R Greathouse IV_, Apr 25 2013"
			],
			"reference": [
				"D. Zagier, The Eichler-Selberg Trace Formula on SL_2(Z), Appendix to S. Lang, Introduction to Modular Forms, Springer, 1976."
			],
			"link": [
				"Charles R Greathouse IV, \u003ca href=\"/A058306/b058306.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"N. Lygeros, O. Rozier, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL13/Lygeros/lygeros5.html\"\u003eA new solution to the equation tau(rho) == 0 (mod p)\u003c/a\u003e, J. Int. Seq. 13 (2010) # 10.7.4.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Hurwitz_class_number\"\u003eHurwitz class number\u003c/a\u003e."
			],
			"formula": [
				"H(n) = A259825(n) / 12. - _Michael Somos_, Jul 05 2015"
			],
			"example": [
				"-1/12, 0, 0, 1/3, 1/2, 0, 0, 1, 1, ..."
			],
			"mathematica": [
				"terms = 100; gf[m_] := With[{r = Range[-m, m]}, -2 Sum[(-1)^k*x^(k^2 + k)/(1 + (-x)^k)^2, {k, r}]/EllipticTheta[3, 0, x] - 2 Sum[(-1)^k*x^(k^2 + 2 k)/(1 + x^(2k))^2, {k, r}]/EllipticTheta[3, 0, -x]]; CoefficientList[ gf[terms // Sqrt // Ceiling] + O[x]^terms, x]/12 // Denominator (* _Jean-François Alcover_, Apr 02 2017, after _Michael Somos_ *)"
			],
			"program": [
				"(PARI) H(n)=sumdiv(core(n,1)[2],d,my(D=-n/d^2);if(D%4\u003c2,qfbclassno(D)/max(1,D+6)))",
				"a(n)=if(n,denominator(H(n)),12) \\\\ _Charles R Greathouse IV_, Apr 25 2013",
				"(PARI) a(n)=if(n,my(D=4-valuation(n,3)%2);denominator(if(issquare(n/D) \u0026\u0026 n%D==0, qfbclassno(-D)/max(1,6-D))),12) \\\\ _Charles R Greathouse IV_, Apr 25 2013",
				"(PARI) {a(n) = denominator( qfbhclassno( n))}; /* _Michael Somos_, Jul 06 2015 */"
			],
			"xref": [
				"Cf. A058305, A259825."
			],
			"keyword": "nonn,frac,easy",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_, Dec 09 2000",
			"references": 3,
			"revision": 30,
			"time": "2017-07-12T15:17:31-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}