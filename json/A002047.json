{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002047",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2047,
			"id": "M1688 N0666",
			"data": "1,2,6,28,244,2544,35600,659632,15106128,425802176,14409526080,577386122880",
			"name": "Number of 3 X (2n+1) zero-sum arrays with entries -n,..,0,..,n.",
			"comment": [
				"This can be interpreted as the number of ways to choose 2n+1 cells in a hexagonal grid of side n+1 such that no two are in the same row or left diagonal or right diagonal. - Alex Fink (a00(AT)shaw.ca), Mar 16 2005",
				"Also the number of transversals of a partial Latin square L of order 2n+1 in which L_{ij} = i+j if n+1 \u003c i+j \u003c 3n+3 and L_{ij} is empty otherwise. [Cavenagh-Wanless]",
				"Also the number of arrangements of the numbers n+1, n+1, ..., 3n+1, 3n+1 such that there are n numbers between the pair of n+1's, ..., 3n numbers between the pair of 3n+1's. For each of these arrangements and its mirror image, there is a bijection with a pair of the 3 X (2n+1) zero-sum arrays. - _Stephen J Scattergood_, Jul 19 2013",
				"Also the number of sigma-permutations of length 2n+1 [Kotzig-Laufer]. - _N. J. A. Sloane_, Jul 27 2015",
				"An (m,2n+1)-zero-sum array is an m X (2n+1) matrix whose m rows are permutations of the 2n+1 integers -n..n, the sum of each column is zero and the first row of the matrix is -n,-n+1,..,0,..,n-1,n. - _Gheorghe Coserea_, Dec 29 2016",
				"a(n-1) is also number of ways of placing 2*n-1 nonattacking rooks on a hexagonal board with edge-length n in Glinski's hexagonal chess. - _Vaclav Kotesovec_, Aug 15 2019"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"C. Bebeacua, T. Mansour, A. Postnikov and S. Severini, \u003ca href=\"https://arxiv.org/abs/math/0506334\"\u003eOn the X-rays of permutations\u003c/a\u003e, arXiv:math/0506334 [math.CO], 2005.",
				"B. T. Bennett and R. B. Potts, \u003ca href=\"https://doi.org/10.1017/S144678870000505X\"\u003eArrays and brooks\u003c/a\u003e, J. Austral. Math. Soc., 7 (1967), 23-31.",
				"B. T. Bennett and R. B. Potts, \u003ca href=\"/A002047/a002047_1.pdf\"\u003eArrays and brooks\u003c/a\u003e, J. Austral. Math. Soc., 7 (1967), 23-31. [Annotated scanned copy]",
				"N. J. Cavenagh and I. M. Wanless, \u003ca href=\"http://dx.doi.org/10.1016/j.dam.2009.09.006\"\u003eOn the number of transversals in Cayley tables of cyclic groups\u003c/a\u003e, Disc. Appl. Math. 158 (2010), 136-146.",
				"Gheorghe Coserea, \u003ca href=\"/A002047/a002047.txt\"\u003eSolutions for n=4\u003c/a\u003e.",
				"Gheorghe Coserea, \u003ca href=\"/A002047/a002047_1.txt\"\u003eSolutions for n=5\u003c/a\u003e.",
				"Gheorghe Coserea, \u003ca href=\"/A002047/a002047.mzn.txt\"\u003eMiniZinc model for generating solutions\u003c/a\u003e.",
				"A. Kotzig and P. J. Laufer, \u003ca href=\"http://www.jstor.org/stable/2321345\"\u003eWhen are permutations additive?\u003c/a\u003e, Amer. Math. Monthly, 85 (1978), 364-365.",
				"A. Kotzig and P. J. Laufer, \u003ca href=\"/A002047/a002047.pdf\"\u003eWhen are permutations additive?\u003c/a\u003e, Amer. Math. Monthly, 85 (1978), 364-365. [Annotated by C. L. Mallows, scanned copy, together with letter from C. L. Mallows and N. J. A. Sloane to A. Kotzig, Jul 25 1978]",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Hexagonal_chess#Gli%C5%84ski\u0026#39;s_hexagonal_chess\"\u003eHexagonal chess - Gliński's hexagonal chess\u003c/a\u003e"
			],
			"example": [
				"a(2) = 6 corresponds to",
				"..O.X.X.......X.X.O.......O.X.X.......X.O.X.......X.O.X.......X.X.O",
				".X.X.O.X.....X.O.X.X.....X.X.X.O.....X.X.X.O.....O.X.X.X.....O.X.X.X",
				"X.X.X.X.O...O.X.X.X.X...X.O.X.X.X...O.X.X.X.X...X.X.X.X.O...X.X.X.O.X",
				".O.X.X.X.....X.X.X.O.....X.X.X.O.....X.O.X.X.....X.X.O.X.....O.X.X.X",
				"..X.O.X.......X.O.X.......O.X.X.......X.X.O.......O.X.X.......X.X.O",
				"The bijection with a pair of the 3 X (2n+1) zero-sum arrays:",
				"n=1, a(1)=2 corresponds to",
				"                           3 4 2 3 2 4",
				"        and mirror image   4 2 3 2 4 3",
				"element                  2  3  4  -(2n+1) --\u003e -1  0  1",
				"position, left element   3  1  2  -( n+1) --\u003e  1 -1  0",
				"position  in mirror      2  3  1  -( n+1) --\u003e  0  1 -1",
				"                          -------               -------",
				"sum of column            7  7  7  -(4n+3)      0  0  0",
				"Swapping rows 2,3 yields the other 3 X 3 zero sum array.",
				"n=2, a(2)=6  an example and its mirror, so 2 of the 6 solutions:",
				"                           5 6 7 3 4 5 3 6 4 7",
				"            mirror image   7 4 6 3 5 4 3 7 6 5",
				"            3  4  5  6  7  -(2n+1) --\u003e -2 -1  0  1  2",
				"            4  5  1  2  3  -( n+1) --\u003e  1  2 -2 -1  0",
				"            4  2  5  3  1  -( n+1) --\u003e  1 -1  2  0 -2",
				"            --------------              --------------",
				"           11 11 11 11 11  -(4n+3) --\u003e  0  0  0  0  0",
				"Swapping rows 2,3 yields the other 3 X 5 zero sum array."
			],
			"xref": [
				"Cf. A014552. A diagonal of the triangle in A260333.",
				"Cf. A309260, A309746."
			],
			"keyword": "nonn,nice,more",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from Alex Fink (a00(AT)shaw.ca), Mar 16 2005",
				"a(10) and a(11) from _Ian Wanless_, Jul 30 2010, from the Cavenagh-Wanless paper"
			],
			"references": 11,
			"revision": 87,
			"time": "2019-08-18T12:22:23-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}