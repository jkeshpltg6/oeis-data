{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A328370",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 328370,
			"data": "48,75,140,195,1050,1925,1575,1648,2024,2295,5775,6128,8892,16587,9504,20735,62744,75495,186615,206504,196664,219975,199760,309135,266000,507759,312620,549219,526575,544784,573560,817479,587460,1057595,1000824,1902215,1081184,1331967,1139144,1159095,1140020,1763019",
			"name": "Quasi-amicable pairs.",
			"comment": [
				"Also called betrothed pairs, or quasiamicable pairs, or reduced amicable pairs.",
				"A pair of numbers x and y is called quasi-amicable if sigma(x) = sigma(y) = x + y + 1, where sigma(n) is the sum of the divisors of n.",
				"All known quasi-amicable pairs have opposite parity.",
				"First differs from A005276 at a(6).",
				"According to Hisanori Mishima (see link) there are 404 quasi-amicable pairs where the smaller part is less than 10^10. See A126160 for more values. - _Peter Luschny_, Nov 18 2019"
			],
			"link": [
				"R. K. Guy, Unsolved Problems in Number Theory, B5.",
				"P. Hagis and G. Lord, \u003ca href=\"https://doi.org/10.1090/S0025-5718-1977-0434939-3\"\u003eQuasi-amicable numbers\u003c/a\u003e, Math. Comp. 31 (1977), 608-611.",
				"Hisanori Mishima, \u003ca href=\"http://www.asahi-net.or.jp/~kc2h-msm/mathland/math09/math09t2.htm\"\u003eTable of quasi-amicable pairs under 10^10\u003c/a\u003e.",
				"Paul Pollack, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL14/Pollack/pollack3.html\"\u003eQuasi-Amicable Numbers are Rare\u003c/a\u003e, Journal of Integer Sequences, Vol. 14 (2011), Article 11.5.2.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/QuasiamicablePair.html\"\u003eQuasiamicable Pair.\u003c/a\u003e"
			],
			"formula": [
				"a(2*n-1) = A003502(n); a(2*n) = A003503(n)."
			],
			"example": [
				"Initial quasi-amicable pairs:",
				"    48,   75;",
				"   140,  195;",
				"  1050, 1925;",
				"  1575, 1648;",
				"  2024, 2295;",
				"...",
				"The sum of the divisors of 48 is 1 + 2 + 3 + 4 + 6 + 8 + 12 + 16 + 24 + 48 = 124. On the other hand the sum of the divisors of 75 is 1 + 3 + 5 + 15 + 25 + 75 = 124. Note that 48 + 75 + 1 = sigma(48) = sigma(75) = 124. The smallest quasi-amicable pair is (48, 75), so a(1) = 48 and a(2) = 75."
			],
			"maple": [
				"with(numtheory): aList := proc(searchbound)",
				"local r, n, m, L: L := []:",
				"for m from 1 to searchbound do",
				"   n := sigma(m) - m - 1:",
				"   if n \u003c= m then next fi;",
				"   r := sigma(n) - n - 1:",
				"   if r = m then L := [op(L), m, n] fi;",
				"od; L end:",
				"aList(10000); # _Peter Luschny_, Nov 18 2019"
			],
			"xref": [
				"A003502 and A003503 interleaved.",
				"Cf. A000203, A005276, A126160, A179612, A259180."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Omar E. Pol_, Oct 14 2019",
			"references": 0,
			"revision": 32,
			"time": "2019-11-18T16:09:28-05:00",
			"created": "2019-11-18T16:09:28-05:00"
		}
	]
}