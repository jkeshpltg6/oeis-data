{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A319933",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 319933,
			"data": "1,0,1,0,-1,1,0,-1,-2,1,0,0,-1,-3,1,0,0,2,0,-4,1,0,1,1,5,2,-5,1,0,0,2,0,8,5,-6,1,0,1,-2,0,-5,10,9,-7,1,0,0,0,-7,-4,-15,10,14,-8,1,0,0,-2,0,-10,-6,-30,7,20,-9,1,0,0,-2,0,8,-5,0,-49,0,27,-10,1",
			"name": "A(n, k) = [x^k] DedekindEta(x)^n, square array read by descending antidiagonals, A(n, k) for n \u003e= 0 and k \u003e= 0.",
			"comment": [
				"The columns are generated by polynomials whose coefficients constitute the triangle of signed D'Arcais numbers A078521 when multiplied with n!."
			],
			"reference": [
				"G. H. Hardy and E. M. Wright, An Introduction to the Theory of Numbers. Fifth ed., Clarendon Press, Oxford, 2003."
			],
			"link": [
				"M. Boylan, \u003ca href=\"http://dx.doi.org/10.1016/S0022-314X(02)00037-9\"\u003eExceptional congruences for the coefficients of certain eta-product newforms\u003c/a\u003e, J. Number Theory 98 (2003), no. 2, 377-389. MR1955423 (2003k:11071)",
				"S. R. Finch, \u003ca href=\"https://arxiv.org/abs/math/0701251\"\u003ePowers of Euler's q-Series\u003c/a\u003e, arXiv:math/0701251 [math.NT], 2007.",
				"V. Kotesovec, \u003ca href=\"http://oeis.org/A258232/a258232_2.pdf\"\u003eThe integration of q-series\u003c/a\u003e",
				"Y. Martin, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-96-01743-6\"\u003eMultiplicative eta-quotients\u003c/a\u003e, Trans. Amer. Math. Soc. 348 (1996), no. 12, 4825-4856, see page 4852 Table I.",
				"M. Newman, \u003ca href=\"/A000727/a000727.pdf\"\u003eA table of the coefficients of the powers of eta(tau)\u003c/a\u003e, Nederl. Akad. Wetensch. Proc. Ser. A. 59 = Indag. Math. 18 (1956), 204-216. [Annotated scanned copy]",
				"Tim Silverman, \u003ca href=\"http://arxiv.org/abs/1612.08085\"\u003eCounting Cliques in Finite Distant Graphs\u003c/a\u003e, arXiv preprint arXiv:1612.08085 [math.CO], 2016.",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"\u003ca href=\"/index/Pro#1mxtok\"\u003eIndex entries for expansions of Product_{k \u003e= 1} (1-x^k)^m\u003c/a\u003e"
			],
			"example": [
				"[ 0] 1,   0,   0,    0,     0,    0,     0,     0,     0,     0, ... A000007",
				"[ 1] 1,  -1,  -1,    0,     0,    1,     0,     1,     0,     0, ... A010815",
				"[ 2] 1,  -2,  -1,    2,     1,    2,    -2,     0,    -2,    -2, ... A002107",
				"[ 3] 1,  -3,   0,    5,     0,    0,    -7,     0,     0,     0, ... A010816",
				"[ 4] 1,  -4,   2,    8,    -5,   -4,   -10,     8,     9,     0, ... A000727",
				"[ 5] 1,  -5,   5,   10,   -15,   -6,    -5,    25,    15,   -20, ... A000728",
				"[ 6] 1,  -6,   9,   10,   -30,    0,    11,    42,     0,   -70, ... A000729",
				"[ 7] 1,  -7,  14,    7,   -49,   21,    35,    41,   -49,  -133, ... A000730",
				"[ 8] 1,  -8,  20,    0,   -70,   64,    56,     0,  -125,  -160, ... A000731",
				"[ 9] 1,  -9,  27,  -12,   -90,  135,    54,   -99,  -189,   -85, ... A010817",
				"[10] 1, -10,  35,  -30,  -105,  238,     0,  -260,  -165,   140, ... A010818",
				"    A001489,  v , A167541, v , A319931,  v ,         diagonal: A008705",
				"           A080956       A319930      A319932"
			],
			"maple": [
				"DedekindEta := (x, n) -\u003e mul(1-x^j, j=1..n):",
				"A319933row := proc(n, len) series(DedekindEta(x, len)^n, x, len+1):",
				"seq(coeff(%, x, j), j=0..len-1) end:",
				"seq(print([n], A319933row(n, 10)), n=0..10);"
			],
			"mathematica": [
				"eta[x_, n_] := Product[1 - x^j, {j, 1, n}];",
				"A[n_, k_] := SeriesCoefficient[eta[x, k]^n, {x, 0, k}];",
				"Table[A[n - k, k], {n, 0, 11}, {k, n, 0, -1}] // Flatten (* _Jean-François Alcover_, Nov 10 2018 *)"
			],
			"program": [
				"(Sage)",
				"from sage.modular.etaproducts import qexp_eta",
				"def A319933row(n, len):",
				"    return (qexp_eta(ZZ['q'], len+4)^n).list()[:len]",
				"for n in (0..10):",
				"    print(A319933row(n, 10))",
				"(Julia) # DedekindEta is defined in A000594",
				"for n in 0:10",
				"    DedekindEta(10, n) |\u003e println",
				"end"
			],
			"xref": [
				"Transpose of A286354.",
				"Cf. A078521, A319574 (JacobiTheta3)."
			],
			"keyword": "sign",
			"offset": "0,9",
			"author": "_Peter Luschny_, Oct 02 2018",
			"references": 4,
			"revision": 18,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2018-10-03T03:50:16-04:00"
		}
	]
}