{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A269598",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 269598,
			"data": "1,1,2,5,1,3,9,4,8,1,5,9,1,2,3,8,6,2,6,1,14,5,4,7,8,11,7,4,3,13,17,1,14,9,10,20,21,14,19,8,1,17,18,16,11,6,26,10,8,1,2,18,13,24,12,9,25,7,14",
			"name": "Irregular triangle giving T(n, k) = -(2*A269596(n, k)^(prime(n)-2) modulo prime(n) for n \u003e= 2.",
			"comment": [
				"The length of row n \u003e= 2 is (prime(n)-1)/2 = A005097(n-1).",
				"The irregular companion triangle -(2*A269597(n, k)^(prime(n)-2) modulo prime(n) is given in A269599.",
				"These numbers, called z_1 = z_1(x_1,prime(n)), appear in a recurrence for the approximation sequence {x_n(prime(n), b, x1)} of the p-adic integer sqrt(-b) with entries congruent to x1 modulo prime(n). The irregular triangle for the b values is given in  A269595(n, k) for n \u003e= 2 (odd primes), and A269596(n, k) gives the corresponding x1 values.",
				"T(n, k) is the unique solution of the first order congruence 2*A269596(n, k)*z(n, k) + 1 == 0 (mod prime(n)), with 0 \u003c= z(n, k) \u003c= prime(n)-1, for  n \u003e= 2.",
				"For a(n), n \u003e= 2, see column z_1 of the table of the paper given as a Wolfdieter Lang link."
			],
			"link": [
				"Wolfdieter Lang, \u003ca href=\"/A268922/a268922.pdf\"\u003eNote on a Recurrence for Approximation Sequences of p-adic Square Roots\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = modp(-(2*A269596(n, k)^(prime(n) -2), prime(n)), for n \u003e= 2 and k=1, 2, ...., (prime(n)-1)/2, with modp(a, p) giving the number a' from {0, 1, ...,  p-1} with a' == a (mod p).",
				"T(n, k) = prime(n) - A269599(n, k)."
			],
			"example": [
				"The irregular triangle T(n, k) begins (P(n) stands here for prime(n)):",
				"n, P(n)\\k 1  2  3  4  5  6  7  8  9 10 11 12 13 14",
				"2,   3:   1",
				"3,   5:   1  2",
				"4,   7:   5  1  3",
				"5,  11:   9  4  8  1  5",
				"6:  13:   9  1  2  3  8  6",
				"7,  17:   2  6  1 14  5  4  7  8",
				"8,  19:  11  7  4  3 13 17  1 14  9",
				"9,  23:  10 20 21 14 19  8  1 17 18 16 11",
				"10, 29:   6 26 10  8  1  2 18 13 24 12  9 25  7 14",
				"...",
				"T(5, 3) = 8  because 2*A269596(5, 3)*8 + 1 = 2*2*8 + 1 = 33 == 0 mod 11, hence modp(33, 11) = 0 , and 8 is the unique nonnegative solution \u003c= 10 of 2*A269596(5, 3)*z + 1 == 0 (mod 11)."
			],
			"mathematica": [
				"nn = 12; s = Table[Select[Range[Prime@ n - 1], JacobiSymbol[#, Prime@ n] == 1 \u0026], {n, nn}]; t = Table[Prime@ n - s[[n, (Prime@ n - 1)/2 - k + 1]], {n, Length@ s}, {k, (Prime@ n - 1)/2}] /. {} -\u003e {1}; u = Prepend[Table[SelectFirst[Range@ #, Function[x, Mod[x^2 + t[[n, k]], #] == 0]] \u0026@ Prime@ n, {n, 2, Length@ t}, {k, (Prime@ n - 1)/2}], {1}]; Table[SelectFirst[Range@ #, Function[z, Mod[-(2 u[[n, k]] z + 1), #] == 0]] \u0026@ Prime@ n, {n, 2, Length@ u}, {k, (Prime@ n - 1)/2}] // Flatten (* _Michael De Vlieger_, Apr 04 2016, Version 10 *)"
			],
			"xref": [
				"Cf. A000040, A005097, A269596, A269599 (companion)."
			],
			"keyword": "nonn,tabf,easy",
			"offset": "2,3",
			"author": "_Wolfdieter Lang_, Apr 03 2016",
			"references": 1,
			"revision": 11,
			"time": "2016-04-05T05:35:43-04:00",
			"created": "2016-04-04T23:49:45-04:00"
		}
	]
}