{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A255935",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 255935,
			"data": "0,1,2,1,2,0,1,3,3,2,1,4,6,4,0,1,5,10,10,5,2,1,6,15,20,15,6,0,1,7,21,35,35,21,7,2,1,8,28,56,70,56,28,8,0,1,9,36,84,126,126,84,36,9,2,1,10,45,120,210,252,210,120,45,10,0",
			"name": "Triangle read by rows: a(n) = Pascal's triangle A007318(n) + A197870(n+1).",
			"comment": [
				"Consider the difference table of a sequence with A000004(n)=0's as main diagonal. (Example: A000045(n).) We call this sequence an autosequence of the first kind.",
				"Based on Pascal's triangle, a(n) =",
				"0,                   T1",
				"1, 2,",
				"1, 2, 0,",
				"1, 3, 3, 2,",
				"etc.",
				"transforms every sequence s(n) in an autosequence of the first kind via the multiplication by the triangle",
				"s0,                  T2",
				"s0, s1,",
				"s0, s1, s2,",
				"s0, s1, s2, s3,",
				"etc.",
				"Examples.",
				"1) s(n) = A198631(n)/A006519(n+1), the second fractional Euler numbers (see A209308). This yields 0*1, 1*1+2*1/2=2, 1*1+2*1/2+0*0=2, 1*1+3*1/2++3*0+2*(-1/4)=2, ... .",
				"The autosequence is 0 followed by 2's or 2*(0,1,1,1,1,1,1,1,... = b(n)).",
				"b(n), the basic autosequence of the first kind, is not in the OEIS (see A140575 and A054977).",
				"2) s(n) = A164555(n)/A027642(n), the second Bernoulli numbers, yields 0,2,2,3,4,5,6,7,... = A254667(n).",
				"Row sums of T1: A062510(n) = 3*A001045(n).",
				"Antidiagonal sums of T1: A111573(n).",
				"With 0's instead of the spaces, every column, i.e.,",
				"0, 1, 1, 1, 1,  1,  1,  1,  1,  1,   1, ...",
				"0, 2, 2, 3, 4,  5,  6,  7,  8,  9,  10, ... = A001477(n) with 0 instead of 1 = A254667(n)",
				"0, 0, 0, 3, 6, 10, 15, 21, 28, 36,  45, ... = A161680(n) with 0 instead of 1",
				"0, 0, 0, 2, 4, 10, 20, 35, 56, 84, 120, ...",
				"etc., is an autosequence of the first kind.",
				"With T(0,0) = 1, it is (1, 0, -1/2, 1/2, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (2, -2, -1/2, 1/2, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, May 24 2015"
			],
			"formula": [
				"a(n) = Pascal's triangle A007318(n) with main diagonal A010673(n) (= period 2: repeat 0, 2) instead of 1's=A000012(n).",
				"a(n) = reversal abs(A140575(n)).",
				"a(n) = A007318(n) + A197870(n+1).",
				"T(n,k) = T(n-1,k) + T(n-2,k-1) + T(n-2,k-2), T(0,0) = 0, T(1,0) = 1, T(1,1) = 2, T(n,k) = 0 if k\u003en or if k\u003c0 . - _Philippe Deléham_, May 24 2015",
				"G.f.: (-1-2*x*y+x^2*y+x^2*y^2)/((x*y+1)*(x*y+x-1)) - 1. - _R. J. Mathar_, Aug 12 2015"
			],
			"example": [
				"Triangle starts:",
				"0;",
				"1, 2;",
				"1, 2, 0;",
				"1, 3, 3, 2;",
				"1, 4, 6, 4, 0;",
				"1, 5, 10, 10, 5, 2;",
				"1, 6, 15, 20, 15, 6, 0;",
				"..."
			],
			"mathematica": [
				"a[n_, k_] := If[k == n, 2*Mod[n, 2], Binomial[n, k]]; Table[a[n, k], {n, 0, 10}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Mar 23 2015 *)"
			],
			"xref": [
				"Cf. A001045, A001477, A010673, A011973, A054977, A062510, A007318, A197870, A006519, A198631, A140575, A209308, A164555, A027642, A111573, A161680, A254667."
			],
			"keyword": "nonn,tabl",
			"offset": "0,3",
			"author": "_Paul Curtz_, Mar 11 2015",
			"references": 3,
			"revision": 41,
			"time": "2015-08-12T14:45:13-04:00",
			"created": "2015-04-19T22:20:04-04:00"
		}
	]
}