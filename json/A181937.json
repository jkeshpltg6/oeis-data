{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A181937",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 181937,
			"data": "1,1,1,1,1,1,1,1,1,2,1,1,1,1,5,1,1,1,1,3,16,1,1,1,1,1,9,61,1,1,1,1,1,4,19,272,1,1,1,1,1,1,14,99,1385,1,1,1,1,1,1,5,34,477,7936,1,1,1,1,1,1,1,20,69,1513,50521,1,1,1,1,1,1,1,6,55,496,11259,353792",
			"name": "André numbers. Square array A(n,k), n\u003e=2, k\u003e=0, read by antidiagonals upwards, A(n,k) = n-alternating permutations of length k.",
			"comment": [
				"The André numbers were studied by Désiré André in the case n=2 around 1880. The present author suggests that the numbers A(n,k) be named in honor of André. Already in 1877 Ludwig Seidel gave an efficient algorithm for computing the coefficients of secant and tangent which immediately carries over to the general case. Anthony Mendes and Jeffrey Remmel give exponential generating functions for the general case."
			],
			"reference": [
				"Anthony Mendes and Jeffrey Remmel, Generating functions from symmetric functions, Preliminary version of book, available from Jeffrey Remmel's home page."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A181937/b181937.txt\"\u003eAntidiagonals k = 0..140, flattened\u003c/a\u003e",
				"Désiré André, \u003ca href=\"https://gallica.bnf.fr/ark:/12148/bpt6k30457/f961.image\"\u003eDéveloppement de séc x and tang x\u003c/a\u003e, C. R. Math. Acad. Sci. Paris 88 (1879), 965-967.",
				"Désiré André, \u003ca href=\"http://sites.mathdoc.fr/JMPA/PDF/JMPA_1881_3_7_A10_0.pdf\"\u003eSur les permutations alternées\u003c/a\u003e, J. Math. pur. appl., 7 (1881), 167-184.",
				"Peter Luschny, \u003ca href=\"http://oeis.org/wiki/User:Peter_Luschny/SeidelTransform\"\u003eAn old operation on sequences: the Seidel transform\u003c/a\u003e.",
				"Ludwig Seidel, \u003ca href=\"https://babel.hathitrust.org/cgi/pt?id=hvd.32044092897461\u0026amp;view=1up\u0026amp;seq=176\"\u003eÜber eine einfache Entstehungsweise der Bernoulli'schen Zahlen und einiger verwandten Reihen\u003c/a\u003e, Sitzungsberichte der mathematisch-physikalischen Classe der königlich bayerischen Akademie der Wissenschaften zu München, volume 7 (1877), 157-187. [USA access only through the \u003ca href=\"https://www.hathitrust.org/accessibility\"\u003eHATHI TRUST Digital Library\u003c/a\u003e]",
				"Ludwig Seidel, \u003ca href=\"https://www.zobodat.at/pdf/Sitz-Ber-Akad-Muenchen-math-Kl_1877_0157-0187.pdf\"\u003eÜber eine einfache Entstehungsweise der Bernoulli'schen Zahlen und einiger verwandten Reihen\u003c/a\u003e, Sitzungsberichte der mathematisch-physikalischen Classe der königlich bayerischen Akademie der Wissenschaften zu München, volume 7 (1877), 157-187. [Access through \u003ca href=\"https://de.wikipedia.org/wiki/ZOBODAT\"\u003eZOBODAT\u003c/a\u003e]"
			],
			"example": [
				"n\\k [0][1][2][3][4] [5] [6]  [7]   [8]   [9]  [10]    [11]",
				"[1]  1, 1, 1, 1, 1,  1,  1,   1,    1,    1,    1,       1  [A000012]",
				"[2]  1, 1, 1, 2, 5, 16, 61, 272, 1385, 7936, 50521, 353792  [A000111]",
				"[3]  1, 1, 1, 1, 3,  9, 19,  99,  477, 1513, 11259,  74601  [A178963]",
				"[4]  1, 1, 1, 1, 1,  4, 14,  34,   69,  496,  2896,  11056  [A178964]",
				"[5]  1, 1, 1, 1, 1,  1,  5,  20,   55,  125,   251,   2300  [A181936]",
				"[6]  1, 1, 1, 1, 1,  1,  1,   6,   27,   83,   209,    461  [A250283]"
			],
			"maple": [
				"A181937_list := proc(n, len) local E,dim,i,k;  # Seidel's boustrophedon transform",
				"dim := len-1; E := array(0..dim, 0..dim); E[0,0] := 1;",
				"for i from 1 to dim do",
				"if i mod n = 0 then E[i,0] := 0 ;",
				"   for k from i-1 by -1 to 0 do E[k,i-k] := E[k+1,i-k-1] + E[k,i-k-1] od;",
				"else E[0,i] := 0;",
				"   for k from 1 by 1 to i do E[k,i-k] := E[k-1,i-k+1] + E[k-1,i-k] od;",
				"fi od; [E[0,0],seq(E[k,0]+E[0,k],k=1..dim)] end:",
				"for n from 2 to 6 do print(A181937_list(n,12)) od;"
			],
			"mathematica": [
				"dim = 13; e[_][0, 0] = 1; e[m_][n_ /; 0 \u003c= n \u003c= dim, 0] /; Mod[n, m] == 0 = 0; e[m_][k_ /; 0 \u003c= k \u003c= dim, n_ /; 0 \u003c= n \u003c= dim] /; Mod[n+k, m] == 0 := e[m][k, n] = e[m][k, n-1] + e[m][k+1, n-1]; e[m_][0, n_ /; 0 \u003c= n \u003c= dim] /; Mod[n, m] == 0 = 0; e[m_][k_ /; 0 \u003c= k \u003c= dim, n_ /; 0 \u003c= n \u003c= dim] /; Mod[n+k, m] != 0 := e[m][k, n] = e[m][k-1, n] + e[m][k-1, n+1]; e[_][_, _] = 0; a[_, 0] = 1; a[m_, n_] := e[m][n, 0] + e[m][0, n]; Table[a[m-n+1, n], {m, 1, dim-1}, {n, 0, m-1}] // Flatten (* _Jean-François Alcover_, Jul 23 2013, after Maple *)"
			],
			"program": [
				"(Sage)",
				"@cached_function",
				"def A(m, n):",
				"    if n == 0: return 1",
				"    s = -1 if m.divides(n) else 1",
				"    t = [m*k for k in (0..(n-1)//m)]",
				"    return s*add(binomial(n, k)*A(m, k) for k in t)",
				"A181937_row = lambda m, n: (-1)^int(is_odd(n//m))*A(m, n)",
				"for n in (1..6): print([A181937_row(n, k) for k in (0..20)]) # _Peter Luschny_, Feb 06 2017",
				"(Julia) # Signed version.",
				"using Memoize",
				"@memoize function André(m, n)",
				"    n ≤ 0 \u0026\u0026 return 1",
				"    r = range(0, stop=n-1, step=m)",
				"    S = sum(binomial(n, k) * André(m, k) for k in r)",
				"    n % m == 0 ? -S : S",
				"end",
				"for m in 1:8 println([André(m, n) for n in 0:11]) end # _Peter Luschny_, Feb 09 2019"
			],
			"xref": [
				"Cf. A000111, A178963, A178964, A181936, A250283, A250284, A250285, A250286, A250287."
			],
			"keyword": "nonn,tabl",
			"offset": "0,10",
			"author": "_Peter Luschny_, Apr 03 2012",
			"references": 15,
			"revision": 48,
			"time": "2021-07-18T03:04:41-04:00",
			"created": "2012-04-04T13:15:36-04:00"
		}
	]
}