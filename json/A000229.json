{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000229",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 229,
			"id": "M2684 N1074",
			"data": "3,7,23,71,311,479,1559,5711,10559,18191,31391,422231,701399,366791,3818929,9257329,22000801,36415991,48473881,175244281,120293879,427733329,131486759,3389934071,2929911599,7979490791,36504256799,23616331489,89206899239,121560956039",
			"name": "a(n) is the least number m such that the n-th prime is the least quadratic nonresidue modulo m.",
			"comment": [
				"Note that a(n) is always a prime q \u003e prime(n).",
				"For n \u003e 1, a(n) = prime(k), where k is the smallest number such that A053760(k) = prime(n).",
				"One could make a case for setting a(1) = 2, but a(1) = 3 seems more in keeping with the spirit of the sequence.",
				"a(n) is the smallest odd prime q such that prime(n)^((q-1)/2) == -1 (mod q) and b^((q-1)/2) == 1 (mod q) for every natural base b \u003c prime(n). - _Thomas Ordowski_, May 02 2019"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A000229/b000229.txt\"\u003eTable of n, a(n) for n = 1..38\u003c/a\u003e (from the web page of Tomás Oliveira e Silva)",
				"H. J. Godwin, \u003ca href=\"http://dx.doi.org/10.1017/S0305004100039025\"\u003eOn the least quadratic non-residue\u003c/a\u003e, Proc. Camb. Phil. Soc., 61 (3) (1965), 671-672.",
				"A. J. Hanson, G. Ortiz, A. Sabry and Y.-T. Tai, \u003ca href=\"http://arxiv.org/abs/1305.3292\"\u003eDiscrete Quantum Theories\u003c/a\u003e, arXiv preprint arXiv:1305.3292 [quant-ph], 2013.",
				"A. J. Hanson, G. Ortiz, A. Sabry, Y.-T. Tai, \u003ca href=\"http://www.cs.indiana.edu/~sabry/papers/saskatoon-talk.pdf\"\u003eDiscrete quantum theories\u003c/a\u003e, (a different version). (To appear in J. Phys. A: Math. Theor., 2014).",
				"Tomás Oliveira e Silva, \u003ca href=\"http://sweet.ua.pt/tos/p_roots.html\"\u003eLeast primitive root of prime numbers\u003c/a\u003e",
				"Hans Salié, \u003ca href=\"http://dx.doi.org/10.1002/mana.19650290109\"\u003eUber die kleinste Primzahl, die eine gegebene Primzahl als kleinsten positiven quadratischen Nichtrest hat\u003c/a\u003e, Math. Nachr. 29 (1965) 113-114.",
				"Yu-Tsung Tai, \u003ca href=\"https://scholarworks.iu.edu/dspace/bitstream/handle/2022/23178/dissertation.pdf\"\u003eDiscrete Quantum Theories and Computing\u003c/a\u003e, Ph.D. thesis, Indiana University (2019)."
			],
			"example": [
				"a(2) = 7 because the second prime is 3 and 3 is the least quadratic nonresidue modulo 7, 14, 17, 31, 34, ... and 7 is the least of these."
			],
			"mathematica": [
				"leastNonRes[p_] := For[q = 2, True, q = NextPrime[q], If[JacobiSymbol[q, p] != 1, Return[q]]]; a[1] = 3; a[n_] := For[pn = Prime[n]; k = 1, True, k++, an = Prime[k]; If[pn == leastNonRes[an], Print[n, \" \", an];  Return[an]]]; Array[a, 20] (* _Jean-François Alcover_, Nov 28 2015 *)"
			],
			"xref": [
				"Cf. A020649, A025021, A053760, A307809. For records see A133435.",
				"Differs from A002223, A045535 at 12th term."
			],
			"keyword": "nonn,nice",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Definition corrected by Melvin J. Knight (MELVIN.KNIGHT(AT)ITT.COM), Dec 08 2006",
				"Name edited by _Thomas Ordowski_, May 02 2019"
			],
			"references": 12,
			"revision": 77,
			"time": "2019-07-22T17:27:07-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}