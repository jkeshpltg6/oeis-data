{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A186432",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 186432,
			"data": "1,1,1,1,12,1,1,30,30,1,1,56,140,56,1,1,90,420,420,90,1,1,132,990,1848,990,132,1,1,182,2002,6006,6006,2002,182,1,1,240,3640,16016,25740,16016,3640,240,1,1,306,6120,37128,87516,87516,37128,6120,306,1,1,380,9690,77520,251940,369512,251940,77520,9690,380,1",
			"name": "Triangle associated with the set S of squares {0,1,4,9,16,...}.",
			"comment": [
				"Given a subset S of the integers Z, Bhargava [1] has shown how to associate with S a generalized factorial function, denoted n!_S, sharing many properties of the classical factorial function n! (which corresponds to the choice S = Z). In particular, he shows that the generalized binomial coefficients n!_S/(k!_S*(n-k)!_S) are always integral for any choice of S. Here we take S = {0,1,4,9,16,...}, the set of squares.",
				"The associated generalized factorial function n!_S is given by the formula",
				"n!_S = Product_{k=0..n} (n^2 - k^2), with the convention 0!_S = 1. This should be compared with n! = Product_{k=0..n} (n - k).",
				"For n \u003e= 1, n!_S = (2*n)!/2 = A002674(n).",
				"Compare this triangle with A086645 and also A186430 - the generalized binomial coefficients for the set S of prime numbers {2,3,5,7,11,...}."
			],
			"link": [
				"M. Bhargava, \u003ca href=\"http://dx.doi.org/10.2307/2695734\"\u003eThe factorial function and generalizations\u003c/a\u003e, Amer. Math. Monthly, 107 (2000), 783-799."
			],
			"formula": [
				"TABLE ENTRIES",
				"T(n,k) = n!_S/(k!_S*(n-k)!_S),",
				"which simplifies to",
				"T(n,k) = 2*binomial(2*n,2*k) for 1 \u003c= k \u003c n,",
				"with boundary conditions T(n,0) = 1 and T(n,n) = 1 for n \u003e= 0.",
				"RELATIONS WITH OTHER SEQUENCES",
				"Denote this triangle by T. The first column of the inverse T^-1 (see A186433) begins [1, -1, 11, -301, 15371, ...] and, apart from the initial 1, is a signed version of the Glaisher's H' numbers A002114.",
				"The first column of (1/2)*T^2 begins [1/2, 1, 7, 31, 127, ...] and, apart from the initial term, equals A000225(2*n-1), counting the preferential arrangements on (2*n - 1) labeled elements having less than or equal to two ranks.",
				"The first column of (1/3)*T^3 begins [1/3, 1, 13, 181, 1933, ...] and, apart from the initial term, is A101052(2*n-1), which gives the number of preferential arrangements on (2*n-1) labeled elements having less than or equal to three ranks."
			],
			"example": [
				"Triangle begins",
				"n/k.|..0.....1.....2.....3.....4.....5.....6.....7",
				"==================================================",
				".0..|..1",
				".1..|..1.....1",
				".2..|..1....12.....1",
				".3..|..1....30....30.....1",
				".4..|..1....56...140....56.....1",
				".5..|..1....90...420...420....90.....1",
				".6..|..1...132...990..1848...990...132.....1",
				".7..|..1...182..2002..6006..6006..2002...182.....1",
				"..."
			],
			"mathematica": [
				"Table[2 Binomial[2 n, 2 k] - Boole[Or[k == 0, k == n]], {n, 0, 10}, {k, 0, n}] // Flatten (* _Michael De Vlieger_, May 23 2017 *)"
			],
			"xref": [
				"Cf. A002114, A086645, A186430, A186433 (inverse)."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,5",
			"author": "_Peter Bala_, Feb 22 2011",
			"references": 4,
			"revision": 24,
			"time": "2018-06-03T03:49:25-04:00",
			"created": "2011-02-21T10:02:31-05:00"
		}
	]
}