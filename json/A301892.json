{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A301892",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 301892,
			"data": "1,2,3,5,8,11,14,15,26,36,44,49,58,76,131,156,174,206,266,308,339,388,428,460,766,550,568,979,1124,1238,1411,1548,1659,1754,1983,2048,2160,3689,4211,4617,5245,5731,6135,6482,7308,7539,7949,8477,9198,9681,10306",
			"name": "a(n) = A010846(A002182(n)).",
			"comment": [
				"We define an \"n-regular\" number as 1 \u003c= m \u003c= n such that m | n^e with integer e \u003e= 0. The divisor d is a special case of regular number m such that d | n^e with e = 0 or e = 1. Regular numbers m can exceed n; we are concerned only with regulars m \u003c= n herein.",
				"Since highly composite numbers represent those numbers that set records in the divisor counting function A000005, and since the divisor is a special case of regular number, this sequence applies the \"regular counting function\" A010846 to terms in A002182.",
				"Only 13 HCNs less than 36 * 10^6 are also \"highly regular\", i.e., appear in A244052. The largest HCN that is also highly regular is 27720, the 25th HCN and the 47th highly regular number.",
				"Only 2 and 6 set records for the ratio A010846(n)/A000005(n).",
				"Conjectures:",
				"Let \"tier\" t consist of all terms A002110(t) \u003c= m \u003c A002110(t + 1) in A244052, where all such m in tier t have A001221(m) = t. The intersection of A002182 and A244052 is finite, consisting of 13 terms: {1, 2, 4, 6, 12, 24, 60, 120, 180, 840, 1260, 1680, 27720}. All of these terms are also in A060735 and not in A288813, as the latter are squarefree and have \"gaps\" among prime divisors. This intersection has the following number of terms in the \"tiers\" 0 through 5 of A244052: {1, 2, 3, 3, 3, 1}. If we look at A060735 as a number triangle T(n,k) = k * A002110(n) with 1 \u003c= k \u003c prime(n + 1), the terms are:",
				"{0, 1},",
				"{{1,1}, {1,2}},",
				"{{2,1}, {2,2}, {2,4}},",
				"{{3,2}, {3,4}, {3,6}},",
				"{{4,4}, {4,6}, {4,8}},",
				"{5,12}."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A301892/b301892.txt\"\u003eTable of n, a(n) for n = 1..135\u003c/a\u003e"
			],
			"example": [
				"A002182(4) = 6. There are five numbers 1 \u003c= m \u003c= 6 such that m divides an integer power of 6: {1, 2, 3, 4, 6}. Thus, a(4) = 5."
			],
			"mathematica": [
				"With[{s = Array[DivisorSigma[0, #] \u0026, 10^6]}, Map[With[{n = FirstPosition[s, #][[1]]}, Count[Range@ n, _?(PowerMod[n, Floor@ Log2@ n, #] == 0 \u0026)]] \u0026, Union@ FoldList[Max, s]]]"
			],
			"program": [
				"(PARI) a010846(n) = sum(k=1, n, if(gcd(n, k)-1, 0, moebius(k)*(n\\k))) \\\\ after _Benoit Cloitre_ in A010846",
				"r=0; for(x=1, oo, if(numdiv(x) \u003e r, print1(a010846(x), \", \"); r=numdiv(x))) \\\\ _Felix Fröhlich_, Mar 30 2018"
			],
			"xref": [
				"Cf. A000005, A002110, A002182, A010846, A060735, A244052."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Michael De Vlieger_, Mar 28 2018",
			"references": 1,
			"revision": 13,
			"time": "2019-07-07T05:51:56-04:00",
			"created": "2018-04-03T16:08:28-04:00"
		}
	]
}