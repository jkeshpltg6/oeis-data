{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A206487",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 206487,
			"data": "1,1,1,1,1,2,1,1,1,2,1,3,2,2,2,1,1,3,1,3,2,2,1,4,1,4,1,3,2,6,1,1,2,2,2,6,3,2,4,4,2,6,2,3,3,2,2,5,1,3,2,6,1,4,2,4,2,4,1,12,3,2,3,1,4,6,1,3,2,6,3,10,2,6,3,3,2,12,2,5,1,4,1,12,2,4,4,4,4,12,4,3,2,4,2,6,1,3,3,6,4,6,1,8,6,2,3,10,2,6,6,5,6,6,2,6,6,2,2,20,1,6,4,3,1,12,1,1,4,12,1,12,2,2,4,4,2,6,2,12,4,6,4,15,4,4,3,9,2,12,6,4,3,6,2,24,3,4,2,6",
			"name": "Number of ordered trees isomorphic (as rooted trees) to the rooted tree having Matula number n.",
			"comment": [
				"The Matula-Goebel number of a rooted tree is defined in the following recursive manner: to the one-vertex tree there corresponds the number 1; to a tree T with root degree 1 there corresponds the t-th prime number, where t is the Matula-Goebel number of the tree obtained from T by deleting the edge emanating from the root; to a tree T with root degree m\u003e=2 there corresponds the product of the Matula-Goebel numbers of the m branches of T.",
				"a(n) = the number of times n occurs in A127301. - _Antti Karttunen_, Jan 03 2013"
			],
			"link": [
				"E. Deutsch, \u003ca href=\"http://arxiv.org/abs/1111.4288\"\u003e Rooted tree statistics from Matula numbers\u003c/a\u003e, arXiv:1111.4288 [math.CO], 2011.",
				"F. Goebel, \u003ca href=\"http://dx.doi.org/10.1016/0095-8956(80)90049-0\"\u003eOn a 1-1-correspondence between rooted trees and natural numbers\u003c/a\u003e, J. Combin. Theory, B 29 (1980), 141-143.",
				"I. Gutman and A. Ivic, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(95)00182-V\"\u003eOn Matula numbers\u003c/a\u003e, Discrete Math., 150, 1996, 131-142.",
				"I. Gutman and Yeong-Nan Yeh, \u003ca href=\"http://www.emis.de/journals/PIMB/067/3.html\"\u003eDeducing properties of trees from their Matula numbers\u003c/a\u003e, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"D. W. Matula, \u003ca href=\"http://www.jstor.org/stable/2027327\"\u003eA natural rooted tree enumeration by prime factorization\u003c/a\u003e, SIAM Rev. 10 (1968) 273.",
				"P. Schultz, \u003ca href=\"https://doi.org/10.1016/0012-365X(82)90207-2\"\u003eEnumeration of rooted trees with an application to group presentations\u003c/a\u003e, Discrete Math., 41, 1982, 199-214.",
				"\u003ca href=\"/index/Mat#matula\"\u003eIndex entries for sequences related to Matula-Goebel numbers\u003c/a\u003e"
			],
			"formula": [
				"a(1)=1; denoting by p(t) the t-th prime, if n = p(n_1)^{k_1}...p(n_r)^{k_r}, then a(n) = a(n_1)^{k_1}...a(n_r)^{k_r}*(k_1 + ... + k_r)!/[(k_1)!...(k_r)!] (see Theorem 1 in the Schultz reference, where the exponents k_j of N(n_j) have been inadvertently omitted)."
			],
			"example": [
				"a(4)=1 because the rooted tree with Matula number 4 is V and there is no other ordered tree isomorphic to V. a(6)=2 because the rooted tree corresponding to n = 6 is obtained by joining the trees A - B and C - D - E at their roots A and C. Interchanging their order, we obtain another ordered tree, isomorphic (as rooted tree) to the first one."
			],
			"maple": [
				"with(numtheory): F := proc (n) options operator, arrow: factorset(n) end proc: PD := proc (n) local k, m, j: for k to nops(F(n)) do m[k] := 0: for j while is(n/F(n)[k]^j, integer) = true do m[k] := m[k]+1 end do end do: [seq([F(n)[q], m[q]], q = 1 .. nops(F(n)))] end proc: a := proc (n) if n = 1 then 1 elif bigomega(n) = 1 then a(pi(n)) else mul(a(PD(n)[j][1])^PD(n)[j][2], j = 1 .. nops(F(n)))*factorial(add(PD(n)[k][2], k = 1 .. nops(F(n))))/mul(factorial(PD(n)[k][2]), k = 1 .. nops(F(n))) end if end proc: seq(a(n), n = 1 .. 160);"
			],
			"xref": [
				"Cf. A127301."
			],
			"keyword": "nonn",
			"offset": "1,6",
			"author": "_Emeric Deutsch_, Apr 14 2012",
			"references": 5,
			"revision": 19,
			"time": "2017-06-13T03:53:18-04:00",
			"created": "2012-04-15T04:14:33-04:00"
		}
	]
}