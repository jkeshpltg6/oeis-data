{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A335407",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 335407,
			"data": "1,1,1,2,0,2,3,54,0,30,105,6090,1512,133056,816480,127209600,0,10090080,562161600,69864795000,49989139200,29593652088000,382147120555200,41810689605484800,4359985823793600,3025062801079038720,49052072750637116160,25835971971637227375360",
			"name": "Number of anti-run permutations of the prime indices of n!.",
			"comment": [
				"An anti-run is a sequence with no adjacent equal parts.",
				"A prime index of n is a number m such that prime(m) divides n. The multiset of prime indices of n is row n of A112798.",
				"Conjecture: Only vanishes at n = 4 and n = 8.",
				"a(16) = 0. Proof: 16! = 2^15 * m where bigomega(m) = A001222(m) = 13. We can't separate 15 1's with 13 other numbers. - _David A. Corneth_, Jul 04 2020"
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A335407/b335407.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A335452(A000142(n)). - _Andrew Howroyd_, Feb 03 2021"
			],
			"example": [
				"The a(0) = 1 through a(6) = 3 anti-run permutations:",
				"  ()  ()  (1)  (1,2)  .  (1,2,1,3,1)  (1,2,1,2,1,3,1)",
				"               (2,1)     (1,3,1,2,1)  (1,2,1,3,1,2,1)",
				"                                      (1,3,1,2,1,2,1)"
			],
			"mathematica": [
				"primeMS[n_]:=If[n==1,{},Flatten[Cases[FactorInteger[n],{p_,k_}:\u003eTable[PrimePi[p],{k}]]]];",
				"Table[Length[Select[Permutations[primeMS[n!]],!MatchQ[#,{___,x_,x_,___}]\u0026]],{n,0,10}]"
			],
			"program": [
				"(PARI) \\\\ See A335452 for count.",
				"a(n)={count(factor(n!)[,2])} \\\\ _Andrew Howroyd_, Feb 03 2021"
			],
			"xref": [
				"The version for Mersenne numbers is A335432.",
				"Anti-run compositions are A003242.",
				"Anti-run patterns are counted by A005649.",
				"Permutations of prime indices are A008480.",
				"Anti-runs are ranked by A333489.",
				"Separable partitions are ranked by A335433.",
				"Inseparable partitions are ranked by A335448.",
				"Anti-run permutations of prime indices are A335452.",
				"Strict permutations of prime indices are A335489.",
				"Cf. A056239, A106351, A112798, A114938, A278990, A292884, A325535, A335125.",
				"Factorial numbers: A000142, A001222, A002982, A007489, A022559, A027423, A054991, A108731, A181819, A181821, A325272, A325273, A325617."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Gus Wiseman_, Jul 01 2020",
			"ext": [
				"Terms a(14) and beyond from _Andrew Howroyd_, Feb 03 2021"
			],
			"references": 8,
			"revision": 18,
			"time": "2021-02-03T15:55:53-05:00",
			"created": "2020-07-01T22:30:07-04:00"
		}
	]
}