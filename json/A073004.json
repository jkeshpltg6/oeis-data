{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A073004",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 73004,
			"data": "1,7,8,1,0,7,2,4,1,7,9,9,0,1,9,7,9,8,5,2,3,6,5,0,4,1,0,3,1,0,7,1,7,9,5,4,9,1,6,9,6,4,5,2,1,4,3,0,3,4,3,0,2,0,5,3,5,7,6,6,5,8,7,6,5,1,2,8,4,1,0,7,6,8,1,3,5,8,8,2,9,3,7,0,7,5,7,4,2,1,6,4,8,8,4,1,8,2,8,0,3,3,4,8,2",
			"name": "Decimal expansion of exp(gamma).",
			"comment": [
				"See references and additional links in A094644.",
				"The Riemann hypothesis holds if and only if the inequality sigma(n)/(n*log(log(n))) \u003c exp(gamma) is valid for all n \u003e= 5041, (G. Robin, 1984). - _Peter Luschny_, Oct 18 2020"
			],
			"link": [
				"Stanislav Sykora, \u003ca href=\"/A073004/b073004.txt\"\u003eTable of n, a(n) for n = 1..2000\u003c/a\u003e",
				"Paul Erdős and S. K. Zaremba, \u003ca href=\"https://users.renyi.hu/~p_erdos/1974-34.pdf\"\u003eThe arithmetic function Sum_{d|n} log d/d\u003c/a\u003e, Demonstratio Mathematica, Vol. 6 (1973), pp. 575-579.",
				"T. H. Gronwall, \u003ca href=\"https://www.jstor.org/stable/1988773\"\u003eSome Asymptotic Expressions in the Theory of Numbers\u003c/a\u003e, Trans. Amer. Math. Soc., Vol. 14, No. 1 (1913), pp. 113-122.",
				"Jeffrey C. Lagarias, \u003ca href=\"http://arxiv.org/abs/1303.1856\"\u003eEuler's constant: Euler's work and modern developments\u003c/a\u003e, arXiv:1303.1856 [math.NT], 2013.",
				"Jeffrey C. Lagarias, \u003ca href=\"https://doi.org/10.1090/S0273-0979-2013-01423-X\"\u003eEuler's constant: Euler's work and modern developments\u003c/a\u003e, Bull. Amer. Math. Soc., 50 (2013), 527-628.",
				"Simon Plouffe, \u003ca href=\"http://www.worldwideschool.org/library/books/sci/math/MiscellaneousMathematicalConstants/chap24.html\"\u003eThe exp(gamma)\u003c/a\u003e.",
				"G. Robin, \u003ca href=\"http://zakuski.utsa.edu/~jagy/Robin_1984.pdf\"\u003eGrandes valeurs de la fonction somme des diviseurs et hypothèse de Riemann\u003c/a\u003e, J. Math. Pures Appl. 63 (1984), 187-213.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Euler-MascheroniConstant.html\"\u003eEuler-Mascheroni Constant\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GronwallsTheorem.html\"\u003eGronwall's Theorem\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/MertensTheorem.html\"\u003eMertens Theorem\u003c/a\u003e, Equations 2-3.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RobinsTheorem.html\"\u003eRobin's Theorem\u003c/a\u003e."
			],
			"formula": [
				"By Mertens theorem, equals lim(m-\u003einfinity)(1/log(prime(m))*prod(k=1..m, 1/(1-1/prime(k)))). - _Stanislav Sykora_, Nov 14 2014",
				"Equals limsup_{n-\u003eoo} sigma(n)/(n*log(log(n))) (Gronwall, 1913). - _Amiram Eldar_, Nov 07 2020",
				"Equals limsup_{n-\u003eoo} (Sum_{d|n} log(d)/d)/(log(log(n)))^2 (Erdős and Zaremba, 1973). - _Amiram Eldar_, Mar 03 2021"
			],
			"example": [
				"Exp(gamma) = 1.7810724179901979852365041031071795491696452143034302053..."
			],
			"mathematica": [
				"RealDigits[ E^(EulerGamma), 10, 110] [[1]]"
			],
			"program": [
				"(PARI) exp(Euler)",
				"(MAGMA) R:=RealField(100); Exp(EulerGamma(R)); // _G. C. Greubel_, Aug 27 2018"
			],
			"xref": [
				"Gamma is the Euler-Mascheroni constant A001620.",
				"Cf. A001113, A067698, A080130, A091901, A094644 (continued fraction for exp(gamma)), A246499."
			],
			"keyword": "cons,nonn,easy",
			"offset": "1,2",
			"author": "_Robert G. Wilson v_, Aug 03 2002",
			"references": 44,
			"revision": 51,
			"time": "2021-05-20T09:01:51-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}