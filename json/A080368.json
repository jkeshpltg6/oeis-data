{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A080368",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 80368,
			"data": "0,2,3,0,5,2,7,0,0,2,11,3,13,2,3,0,17,2,19,5,3,2,23,3,0,2,0,7,29,2,31,0,3,2,5,0,37,2,3,5,41,2,43,11,5,2,47,3,0,2,3,13,53,2,5,7,3,2,59,3,61,2,7,0,5,2,67,17,3,2,71,0,73,2,3,19,7,2,79,5,0,2,83,3,5,2,3,11,89,2,7,23,3,2",
			"name": "a(n) is the least unitary prime divisor of n, or 0 if no such prime divisor exists.",
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A080368/b080368.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"If A277697(n) = 0, then a(n) = 0, otherwise a(n) = A000040(A277697(n)). - _Antti Karttunen_, Oct 28 2016"
			],
			"example": [
				"n = 252100 = 2*2*3*5*5*7*11*11, unitary prime divisors = {3,7}; smallest is 3, so a(252100)=3."
			],
			"mathematica": [
				"ffi[x_] := Flatten[FactorInteger[x]] lf[x_] := Length[FactorInteger[x]] ba[x_] := Table[Part[ffi[x], 2*w-1], {w, 1, lf[x]}] gb[x_] := GCD[ba[x], x/ba[x]] fpg[x_] := Flatten[Position[gb[x], 1]] upd[x_] := Part[ba[x], fpg[x]] mxu[x_] := Max[upd[x]] miu[x_] := Min[upd[x]] Do[If[Equal[upd[n], {}], Print[0]]; If[ !Equal[upd[n], {}], Print[miu[n]]], {n, 2, 256}]",
				"Table[If[Or[n == 1, Length@ # == 0], 0, First@ #] \u0026@ Select[FactorInteger[n][[All, 1]], GCD[#, n/#] == 1 \u0026], {n, 94}] (* _Michael De Vlieger_, Oct 30 2016 *)"
			],
			"program": [
				"(Haskell)",
				"a080368 n = if null us then 0 else fst $ head us",
				"  where us = filter ((== 1) . snd) $ zip (a027748_row n) (a124010_row n)",
				"-- _Reinhard Zumkeller_, Jul 23 2014",
				"(Scheme) (define (A080368 n) (if (zero? (A277697 n)) 0 (A000040 (A277697 n)))) ;; _Antti Karttunen_, Oct 28 2016",
				"(Python)",
				"from sympy import factorint, prime, primepi, isprime, primefactors",
				"def a049084(n): return primepi(n)*(1*isprime(n))",
				"def a055396(n): return 0 if n==1 else a049084(min(primefactors(n)))",
				"def a028234(n):",
				"    f = factorint(n)",
				"    return 1 if n==1 else n/(min(f)**f[min(f)])",
				"def a067029(n):",
				"    f=factorint(n)",
				"    return 0 if n==1 else f[min(f)]",
				"def a277697(n): return 0 if n==1 else a055396(n) if a067029(n)==1 else a277697(a028234(n))",
				"def a(n): return 0 if a277697(n)==0 else prime(a277697(n)) # _Indranil Ghosh_, May 16 2017"
			],
			"xref": [
				"Cf. A000040, A034444, A056169, A080367, A277697.",
				"Cf. A027748, A124010.",
				"Cf. A001694 (positions of zeros).",
				"Cf. A277698 for a variant which gives 1's instead of 0's for numbers with no unitary prime divisors (A001694)."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Labos Elemer_, Feb 21 2003",
			"ext": [
				"a(1)=0 inserted by _Reinhard Zumkeller_, Jul 23 2014"
			],
			"references": 4,
			"revision": 27,
			"time": "2017-09-05T03:12:37-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}