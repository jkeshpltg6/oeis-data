{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A298882",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 298882,
			"data": "1,2,3,4,5,8,7,16,6,32,11,64,13,128,9,256,17,512,19,1024,12,2048,23,4096,10,8192,18,16384,29,32768,31,65536,24,131072,15,262144,37,524288,27,1048576,41,2097152,43,4194304,36,8388608,47,16777216,14,33554432,48",
			"name": "a(1) = 1, and for any n \u003e 1, if n is the k-th number with least prime factor p, then a(n) is the k-th number with greatest prime factor p.",
			"comment": [
				"This sequence is a permutation of the natural numbers, with inverse A298268.",
				"For any prime p and k \u003e 0:",
				"- if s_p(k) is the k-th p-smooth number and r_p(k) is the k-th p-rough number,",
				"- then a(p * r_p(k)) = p * s_p(k),",
				"- for example: a(11 * A008364(k)) = 11 * A051038(k)."
			],
			"link": [
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(1) = 1.",
				"a(A083140(n, k)) = A125624(n, k) for any n \u003e 0 and k \u003e 0.",
				"a(n) = A125624(A055396(n), A078898(n)) for any n \u003e 1.",
				"Empirically:",
				"- a(n) = n iff n belongs to A046022,",
				"- a(2 * k) = 2^k for any k \u003e 0,",
				"- a(p^2) = 2 * p for any prime p,",
				"- a(p * q) = 3 * p for any pair of consecutive odd primes (p, q)."
			],
			"example": [
				"The first terms, alongside A020639(n), are:",
				"  n     a(n)    lpf(n)",
				"  --    ----    ------",
				"   1       1      1",
				"   2       2      2",
				"   3       3      3",
				"   4       4      2",
				"   5       5      5",
				"   6       8      2",
				"   7       7      7",
				"   8      16      2",
				"   9       6      3",
				"  10      32      2",
				"  11      11     11",
				"  12      64      2",
				"  13      13     13",
				"  14     128      2",
				"  15       9      3",
				"  16     256      2",
				"  17      17     17",
				"  18     512      2",
				"  19      19     19",
				"  20    1024      2"
			],
			"xref": [
				"Cf. A008364, A020639, A046022, A051038, A055396, A078898, A083140, A125624, A298268 (inverse)."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Rémy Sigrist_, Jan 28 2018",
			"references": 1,
			"revision": 10,
			"time": "2018-01-31T06:32:54-05:00",
			"created": "2018-01-31T06:32:54-05:00"
		}
	]
}