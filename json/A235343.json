{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A235343",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 235343,
			"data": "0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,1,2,3,3,2,4,2,2,3,4,4,2,3,0,3,2,3,3,3,3,4,0,2,1,1,2,2,1,2,2,2,1,1,2,4,0,2,1,5,2,2,0,2,3,2,3,4,4,2,2,2,1,3,6,3,3,1,5,2,2,2,4,2,2,2,2,2,3,2,2",
			"name": "a(n) = |{0 \u003c k \u003c n: f(n,k) - 1, f(n,k) + 1 and q(f(n,k)) + 1 are all prime with f(n,k) = phi(k) + phi(n-k)/4}|, where phi(.) is Euler's totient function, and q(.) is the strict partition function (A000009).",
			"comment": [
				"Conjecture: (i) a(n) \u003e 0 for all n \u003e= 60.",
				"(ii) For any integer n \u003e 1234, there is a positive integer k \u003c n such that g(n,k) - 1, g(n,k) + 1 and q(g(n,k)) - 1 are all prime, where g(n,k) = phi(k) + phi(n-k)/8.",
				"Clearly, part (i) implies that there are infinitely many primes of the form q(m) + 1 with m - 1 and m + 1 also prime, and part (ii) implies that there are infinitely many primes of the form q(m) - 1 with m - 1 and m + 1 also prime. As log q(m) is asymptotically equivalent to  pi*sqrt(m/3), the conjecture is much stronger than the twin prime conjecture.",
				"We have verified parts (i) and (ii) for n up to 100000 and 60000 respectively."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A235343/b235343.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Z.-W. Sun, \u003ca href=\"http://arxiv.org/abs/1402.6641\"\u003eProblems on combinatorial properties of primes\u003c/a\u003e, arXiv:1402.6641, 2014"
			],
			"example": [
				"a(50) = 1 since phi(34) + phi(16)/4 = 18 with 18 - 1, 18 + 1 and q(18) + 1 = 47 all prime.",
				"a(215) = 1 since phi(87) + phi(128)/4 = 72 with 72 - 1, 72 + 1 and q(72) + 1 = 36353 all prime.",
				"a(645) = 1 since phi(365) + phi(280)/4 = 312 with 312 - 1, 312 + 1 and q(312) + 1 = 207839472391 all prime."
			],
			"mathematica": [
				"f[n_,k_]:=EulerPhi[k]+EulerPhi[n-k]/4",
				"p[n_,k_]:=PrimeQ[f[n,k]-1]\u0026\u0026PrimeQ[f[n,k]+1]\u0026\u0026PrimeQ[PartitionsQ[f[n,k]]+1]",
				"a[n_]:=Sum[If[p[n,k],1,0],{k,1,n-1}]",
				"Table[a[n],{n,1,100}]"
			],
			"xref": [
				"Cf. A000009, A000010, A000040, A001359, A006512, A014574, A234514, A234567, A234615, A235344, A235346, A235356, A235357, A235358."
			],
			"keyword": "nonn",
			"offset": "1,19",
			"author": "_Zhi-Wei Sun_, Jan 06 2014",
			"references": 6,
			"revision": 19,
			"time": "2014-04-06T04:08:02-04:00",
			"created": "2014-01-06T22:45:53-05:00"
		}
	]
}