{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A096427",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 96427,
			"data": "8,4,7,2,1,3,0,8,4,7,9,3,9,7,9,0,8,6,6,0,6,4,9,9,1,2,3,4,8,2,1,9,1,6,3,6,4,8,1,4,4,5,9,1,0,3,2,6,9,4,2,1,8,5,0,6,0,5,7,9,3,7,2,6,5,9,7,3,4,0,0,4,8,3,4,1,3,4,7,5,9,7,2,3,2,0,0,2,9,3,9,9,4,6,1,1,2,2,9,9,4,2",
			"name": "Decimal expansion of 1/(sqrt(2)*G), where G is Gauss's constant A014549.",
			"comment": [
				"Also, decimal expansion of Product_{n\u003e=1} (1-1/(4n-1)^2)). - _Bruno Berselli_, Apr 02 2013"
			],
			"link": [
				"Harry J. Smith, \u003ca href=\"/A096427/b096427.txt\"\u003eTable of n, a(n) for n = 0..20000\u003c/a\u003e",
				"P. Bala, \u003ca href=\"/A096427/a096427.pdf\"\u003eNotes on the constants A096427 and A224268 \u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GausssConstant.html\"\u003eGauss's Constant\u003c/a\u003e",
				"Eric Weisstein's MathWorld, \u003ca href=\"http://mathworld.wolfram.com/JacobiThetaFunctions.html\"\u003eJacobi Theta Functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/UbiquitousConstant.html\"\u003eUbiquitous Constant\u003c/a\u003e"
			],
			"formula": [
				"Also equals agm(1,1/sqrt(2)) since agm(1,1/b) = (1/b)*agm(1,b). - _Gerald McGarvey_, Sep 22 2008",
				"From _Peter Bala_, Feb 26 2019: (Start)",
				"C = Gamma(3/4)^2/sqrt(Pi).",
				"C = 1/( Sum_{n = -inf..inf} exp(-Pi*n^2) )^2.",
				"C = (1/sqrt(2)) * 1/( Sum_{n = -inf..inf} (-1)^n*exp(-Pi*n^2 ) )^2.",
				"Conjecturally, C = (1/sqrt(2)) * 1/( Sum_{n = -inf..inf} exp(-Pi*(n+1/2)^2 ) )^2.",
				"C = ((-1)^m*4^m/binomial(2*m,m)) * Product_{n \u003e= 0} ( 1 - (4*m + 1)^2/(4*n + 3)^2 ), for m = 0,1,2,....",
				"C = 1 - Integral_{x = 0..1} (sqrt(1 + x^4) - 1)/x^2 dx.",
				"C = 1 - Sum_{n \u003e= 1} binomial(1/2,n)/(4*n - 1) = 1 - Sum_{n \u003e= 0} (-1)^n/(4*n + 3)*Catalan(n)/2^(2*n + 1).",
				"Continued fraction: 1 - 1/(3 + 6/(1 + 12/(3 + ... + (4*n - 1)*(4*n - 2)/(1 + 4*n*(4*n - 1)/(3 + ... ))))). (End)"
			],
			"example": [
				"0.8472130847939790866064991234821916364814459103269... = agm(1, sqrt(1/2)) - _Harry J. Smith_, Apr 15 2009"
			],
			"mathematica": [
				"RealDigits[ArithmeticGeometricMean[1, Sqrt[2]]/Sqrt[2], 10, 110][[1]] (* _Bruno Berselli_, Apr 02 2013 *)",
				"(* From the comment: *) RealDigits[N[Product[1 - 1/(4 n - 1)^2, {n, 1, Infinity}], 110]][[1]] (* _Bruno Berselli_, Apr 02 2013 *)"
			],
			"program": [
				"(PARI) { default(realprecision, 20080); x=agm(1, sqrt(1/2)); d=0; for (n=0, 20000, x=(x-d)*10; d=floor(x); write(\"b096427.txt\", n, \" \", d)); } \\\\ _Harry J. Smith_, Apr 15 2009",
				"(PARI) agm(1, sqrt(1/2)) \\\\ _Michel Marcus_, Jun 09 2019",
				"(MAGMA) SetDefaultRealField(RealField(100)); R:= RealField(); Gamma(3/4)^2/(Sqrt(2)*Sqrt(Pi(R)/2)); // _G. C. Greubel_, Aug 17 2018"
			],
			"xref": [
				"Cf. A014549, A224268, A091670 (1/C^2), A175574 (1/C), A293238 (C^2), A053004 (sqrt(2)*C)."
			],
			"keyword": "nonn,cons,easy",
			"offset": "0,1",
			"author": "_Eric W. Weisstein_, Jul 21 2004",
			"references": 5,
			"revision": 46,
			"time": "2021-07-29T04:48:30-04:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}