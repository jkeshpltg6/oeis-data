{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A128567",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 128567,
			"data": "1,2,1,5,6,1,14,31,14,1,42,133,117,22,1,132,587,813,300,36,1,429,2531,4871,2896,692,52,1,1430,10950,27743,23961,9206,1430,76,1,4862,47185,151208,175734,96418,24598,2798,104,1,16796,203704,804065,1200301,882471,329426,62885,5236,146,1",
			"name": "Matrix square, T(n,k), of Parker's partition triangle A047812, read by rows (n \u003e= 1 and 0 \u003c= k \u003c= n-1).",
			"comment": [
				"Column 0 is the Catalan numbers (A000108). Parker's partition triangle may be defined as: A047812(n,k) = [q^(n*k+k)] in the central q-binomial coefficient [2*n,n] for n \u003e= 1 and 0 \u003c= k \u003c= n-1. [Edited by _Petros Hadjicostas_, May 30 2020]"
			],
			"link": [
				"R. K. Guy, \u003ca href=\"/A007042/a007042_1.pdf\"\u003eParker's permutation problem involves the Catalan numbers\u003c/a\u003e, preprint, 1992. (Annotated scanned copy)",
				"R. K. Guy, \u003ca href=\"http://www.jstor.org/stable/2324467\"\u003eParker's permutation problem involves the Catalan numbers\u003c/a\u003e, Amer. Math. Monthly 100 (1993), 287-289.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/E._T._Parker\"\u003eE. T. Parker\u003c/a\u003e."
			],
			"formula": [
				"T(n,k) = Sum_{s=k..n-1} A047812(n,s)*A047812(s+1,k) for n \u003e= 1 and 0 \u003c= k \u003c= n-1. - _Petros Hadjicostas_, May 31 2020"
			],
			"example": [
				"Triangle T(n,k) (with rows n \u003e= 1 and columns k = 0..n-1) begins:",
				"      1;",
				"      2,      1;",
				"      5,      6,      1;",
				"     14,     31,     14,       1;",
				"     42,    133,    117,      22,      1;",
				"    132,    587,    813,     300,     36,      1;",
				"    429,   2531,   4871,    2896,    692,     52,     1;",
				"   1430,  10950,  27743,   23961,   9206,   1430,    76,    1;",
				"   4862,  47185, 151208,  175734,  96418,  24598,  2798,  104,   1;",
				"  16796, 203704, 804065, 1200301, 882471, 329426, 62885, 5236, 146, 1;",
				"  ..."
			],
			"program": [
				"(PARI) {T(n, k)=local(M);M=matrix(n+1,n+1,r,c,if(r\u003cc,0,if(r==0,1, polcoeff(prod(j=r+1,2*r,1-q^j)/prod(j=1,r,1-q^j),(r+1)*(c-1), q)))); (M^2)[n+1,k+1]}",
				"/* To display the data using the above program: */",
				"vector(10, n, vector(n, k, T(n-1,k-1))) \\\\ _Petros Hadjicostas_, May 31 2020"
			],
			"xref": [
				"Cf. A000108 (column k=0), A047812, A128568 (column k=1), A128569 (column k=2), A128602 (row sums)."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Paul D. Hanna_, Mar 12 2007",
			"ext": [
				"Name edited and offset changed by _Petros Hadjicostas_, May 30 2020"
			],
			"references": 3,
			"revision": 20,
			"time": "2020-05-31T22:20:08-04:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}