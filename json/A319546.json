{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A319546",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 319546,
			"data": "1,2,6,24,120,720,713,664,216,-4320,-54720,-664560,-664547,-664378,-661830,-620880,78000,12701520,12701501,12701140,12693540,12525960,8663640,-84207600,-84207575,-84206950,-84190050,-83716200,-69957000,343310400,343310369",
			"name": "a(n) = 1*2*3*4*5*6 - 7*8*9*10*11*12 + 13*14*15*16*17*18 - ... + (up to n).",
			"comment": [
				"In general, for alternating sequences that multiply the first k natural numbers, and subtract/add the products of the next k natural numbers (preserving the order of operations up to n), we have a(n) = (-1)^floor(n/k) * Sum_{i=1..k-1} (1-sign((n-i) mod k)) * (Product_{j=1..i} (n-j+1)) + Sum_{i=1..n} (-1)^(floor(i/k)+1) * (1-sign(i mod k)) * (Product_{j=1..k} (i-j+1)). Here k=6.",
				"An alternating version of A319207."
			],
			"formula": [
				"a(n) = (-1)^floor(n/6) * Sum_{i=1..5} (1-sign((n-i) mod 6)) * (Product_{j=1..i} (n-j+1)) + Sum_{i=1..n} (-1)^(floor(i/6)+1) * (1-sign(i mod 6)) * (Product_{j=1..6} (i-j+1))."
			],
			"example": [
				"a(1) = 1;",
				"a(2) = 1*2 = 2;",
				"a(3) = 1*2*3 = 6;",
				"a(4) = 1*2*3*4 = 24;",
				"a(5) = 1*2*3*4*5 = 120;",
				"a(6) = 1*2*3*4*5*6 = 720;",
				"a(7) = 1*2*3*4*5*6 - 7 = 713;",
				"a(8) = 1*2*3*4*5*6 - 7*8 = 664;",
				"a(9) = 1*2*3*4*5*6 - 7*8*9 = 216;",
				"a(10) = 1*2*3*4*5*6 - 7*8*9*10 = -4320;",
				"a(11) = 1*2*3*4*5*6 - 7*8*9*10*11 = -54720;",
				"a(12) = 1*2*3*4*5*6 - 7*8*9*10*11*12 = -664560;",
				"a(13) = 1*2*3*4*5*6 - 7*8*9*10*11*12 + 13 = -664547;",
				"a(14) = 1*2*3*4*5*6 - 7*8*9*10*11*12 + 13*14 = -664378;",
				"a(15) = 1*2*3*4*5*6 - 7*8*9*10*11*12 + 13*14*15 = -661830;",
				"a(16) = 1*2*3*4*5*6 - 7*8*9*10*11*12 + 13*14*15*16 = -620880;",
				"a(17) = 1*2*3*4*5*6 - 7*8*9*10*11*12 + 13*14*15*16*17 = 78000; etc."
			],
			"mathematica": [
				"a[n_]:=(-1)^Floor[n/6]*Sum[(1-Sign[Mod[n-i,6]])*Product[n-j+1,{j,1,i}],{i,1,5}]+Sum[(-1)^(Floor[i/6]+1)*(1-Sign[Mod[i,6]])*Product[i-j+1,{j,1,5}],{i,1,n}]; Array[a, 30] (* _Stefano Spezia_, Sep 23 2018 *)"
			],
			"xref": [
				"For similar sequences, see: A001057 (k=1), A319373 (k=2), A319543 (k=3), A319544 (k=4), A319545 (k=5), this sequence (k=6), A319547 (k=7), A319549 (k=8), A319550 (k=9), A319551 (k=10).",
				"Cf. A319207."
			],
			"keyword": "sign,easy",
			"offset": "1,2",
			"author": "_Wesley Ivan Hurt_, Sep 22 2018",
			"references": 8,
			"revision": 13,
			"time": "2018-10-05T08:00:03-04:00",
			"created": "2018-10-05T08:00:03-04:00"
		}
	]
}