{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A286899",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 286899,
			"data": "1,1,2,1,8,3,1,32,21,4,1,128,183,40,5,1,512,1641,544,65,6,1,2048,14763,8320,1205,96,7,1,8192,132861,131584,26465,2256,133,8,1,32768,1195743,2099200,628805,64896,3787,176,9,1,131072,10761681,33562624,15424865",
			"name": "Array read by antidiagonals: A(n, L) is the number of closed walks of length 2L along the edges of an n-cube based at a vertex, for n \u003e= 1 and L \u003e= 1.",
			"reference": [
				"R. P. Stanley, Algebraic Combinatorics: Walks, Trees, Tableaux, and More, Springer, 2013."
			],
			"link": [
				"Melvin Peralta, \u003ca href=\"/A286899/b286899.txt\"\u003eTable of n, a(n) for n = 1..120\u003c/a\u003e"
			],
			"formula": [
				"A(n, L) = (1/2^n)*Sum_{i=0..n} binomial(n, i)*(n - 2*i)^(2*L). (Corrected by _Peter Luschny_, Jul 07 2019.)"
			],
			"example": [
				"A(2, 2) = 8 because at each vertex of a 2-cube (i.e., a square), there are 8 closed walks of length 2(2) = 4.",
				"A(1, k) = 1 because at the vertex of a 1-cube, there is 1 closed walk of any length 2*k.",
				"Array A(n, L) begins:",
				"   1         1         1         1         1         1 ...",
				"   2         8        32       128       512      2048 ...",
				"   3        21       183      1641     14763    132861 ...",
				"   4        40       544      8320    131584   2099200 ...",
				"   5        65      1205     26465    628805  15424865 ...",
				"   6        96      2256     64896   2086656  71172096 ...",
				"   7       133      3787    134953   5501167 243147373 ..."
			],
			"maple": [
				"A286899 := proc(n,L)",
				"    add(binomial(n,i)*(n-2*i)^L, i=0..n) ;",
				"    %/2^n ;",
				"end proc:",
				"for n from 1 to 7 do",
				"    for L from 2 to 12 by 2 do",
				"        printf(\"%9d \",A286899(n,L)) ;",
				"    end do:",
				"    printf(\"\\n\") ;",
				"end do: # _R. J. Mathar_, May 22 2017"
			],
			"mathematica": [
				"f[n_, l_] := 1/2^n*Sum[Binomial[n, i]*(n - 2 i)^l, {i, 0, n}];",
				"Table[f[n - l + 1, 2 l], {n, 1, 15}, {l, n, 1, -1}] // Flatten"
			],
			"xref": [
				"Cf. A054879, A092812, A121822."
			],
			"keyword": "nonn,walk,tabl,easy",
			"offset": "1,3",
			"author": "_Melvin Peralta_, May 15 2017",
			"references": 3,
			"revision": 29,
			"time": "2019-07-08T01:32:01-04:00",
			"created": "2017-05-21T20:48:46-04:00"
		}
	]
}