{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A189920",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 189920,
			"data": "1,1,0,1,0,0,1,0,1,1,0,0,0,1,0,0,1,1,0,1,0,1,0,0,0,0,1,0,0,0,1,1,0,0,1,0,1,0,1,0,0,1,0,1,0,1,1,0,0,0,0,0,1,0,0,0,0,1,1,0,0,0,1,0,1,0,0,1,0,0,1,0,0,1,0,1,1,0,1,0,0,0,1,0,1,0,0,1,1,0,1,0,1,0,1,0,0,0,0,0,0,1,0,0,0,0,0,1,1,0,0,0,0,1,0,1,0,0,0,1,0,0,1,0,0,0,1,0,1",
			"name": "Zeckendorf representation of natural numbers.",
			"comment": [
				"The row lengths sequence of this array is A072649(n), n \u003e= 1.",
				"Note that the Fibonacci numbers F(0)=0 and F(1)=1 are not used in this unique representation of n \u003e= 1. No neighboring Fibonacci numbers are allowed (no 1,1, subsequence in any row n)."
			],
			"reference": [
				"R. L. Graham, D. E. Knuth and O. Patashnik, Concrete Mathematics, 2nd ed., 1994, Addison-Wesley, Reading MA, pp. 295-296.",
				"E. Zeckendorf, Représentation des nombres naturels par une somme des nombres de Fibonacci ou de nombres de Lucas, Bull. Soc. Roy. Sci. Liège 41.3-4 (1972) 179-182 (with the proof from 1939)."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A189920/b189920.txt\"\u003eRows n = 1..1000 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"n = Sum_{m=1..rl(n)} a(n,m)*F(rl(n) + 2 - m), n \u003e= 1, with rl(n):=A072649(n)(row length) and F(n):=A000045(n) (Fibonacci numbers).",
				"T(n,k) = A213676(n, A072649(n, k)-1) for k = 1..A072649(k). - _Reinhard Zumkeller_, Mar 10 2013"
			],
			"example": [
				"n=1:  1;",
				"n=2:  1, 0;",
				"n=3:  1, 0, 0;",
				"n=4:  1, 0, 1;",
				"n=5:  1, 0, 0, 0;",
				"n=6:  1, 0, 0, 1;",
				"n=7:  1, 0, 1, 0;",
				"n=8:  1, 0, 0, 0, 0;",
				"n=9:  1, 0, 0, 0, 1;",
				"n=10: 1, 0, 0, 1, 0;",
				"n=11: 1, 0, 1, 0, 0;",
				"n=12: 1, 0, 1, 0, 1;",
				"n=13: 1, 0, 0, 0, 0, 0;",
				"...",
				"1 = F(2),",
				"6 = F(5) + F(2),",
				"11 = F(6) + F(4)."
			],
			"mathematica": [
				"f[n_] := (k = 1; ff = {}; While[(fi = Fibonacci[k]) \u003c= n, AppendTo[ff, fi]; k++]; Drop[ff, 1]); a[n_] := (fn = f[n]; xx = Array[x, Length[fn]]; r = xx /. {ToRules[ Reduce[ And @@ (0 \u003c= # \u003c= 1 \u0026 ) /@ xx \u0026\u0026 fn . xx == n, xx, Integers]]}; Reverse[ First[ Select[ r, FreeQ[ Split[#], {1, 1, ___}] \u0026 ]]]); Flatten[ Table[ a[n], {n, 1, 25}]] (* _Jean-François Alcover_, Sep 29 2011 *)"
			],
			"program": [
				"(Haskell)",
				"a189920 n k = a189920_row n !! k",
				"a189920_row n = z n $ reverse $ takeWhile (\u003c= n) $ tail a000045_list where",
				"   z x (f:fs'@(_:fs)) | f == 1 = if x == 1 then [1] else []",
				"                      | f == x = 1 : replicate (length fs) 0",
				"                      | f \u003c x  = 1 : 0 : z (x - f) fs",
				"                      | f \u003e x  = 0 : z x fs'",
				"a189920_tabf = map a189920_row [1..]",
				"-- _Reinhard Zumkeller_, Mar 10 2013"
			],
			"xref": [
				"Cf. A035517, A014417."
			],
			"keyword": "nonn,easy,tabf,base",
			"offset": "1",
			"author": "_Wolfdieter Lang_, Jun 12 2011",
			"references": 14,
			"revision": 30,
			"time": "2021-11-15T02:46:52-05:00",
			"created": "2011-06-15T04:25:16-04:00"
		}
	]
}