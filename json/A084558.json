{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A084558",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 84558,
			"data": "0,1,2,2,2,2,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,5",
			"name": "a(0) = 0; for n \u003e= 1: a(n) = largest m such that n \u003e= m!.",
			"comment": [
				"For n \u003e= 1, a(n) = the number of significant digits in n's factorial base representation (A007623).",
				"After zero, which occurs once, each n occurs A001563(n) times.",
				"Number of iterations (...f_4(f_3(f_2(n))))...) such that the result is \u003c 1, where f_j(x):=x/j. - _Hieronymus Fischer_, Apr 30 2012",
				"For n \u003e 0: a(n) = length of row n in table A108731. - _Reinhard Zumkeller_, Jan 05 2014"
			],
			"reference": [
				"F. Smarandache, \"f-Inferior and f-Superior Functions - Generalization of Floor Functions\", Arizona State University, Special Collections."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A084558/b084558.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Yi Yuan and Zhang Wenpeng, \u003ca href=\"http://www.gallup.unm.edu/~smarandache/S-analogue-f.pdf\"\u003eOn the Mean Value of the Analogue of Smarandache Function\u003c/a\u003e, Smarandache Notions J., Vol. 15."
			],
			"formula": [
				"From _Hieronymus Fischer_, Apr 30 2012: (Start)",
				"a(n!) = a((n-1)!))+1, for n\u003e1.",
				"G.f.: 1/(1-x)*Sum_{k\u003e=1} x^(k!).",
				"The explicit first terms of the g.f. are: (x+x^2+x^6+x^24+x^120+x^720...)/(1-x).",
				"(End)",
				"Other identities:",
				"For all n \u003e= 0, a(n) = A090529(n+1) - 1. - _Reinhard Zumkeller_, Jan 05 2014",
				"For all n \u003e= 1, a(n) = A060130(n) + A257510(n). - _Antti Karttunen_, Apr 27 2015"
			],
			"example": [
				"a(4) = 2 because 2! \u003c= 4 \u003c 3!."
			],
			"maple": [
				"0, seq(m$(m*m!),m=1..5); # _Robert Israel_, Apr 27 2015"
			],
			"mathematica": [
				"Table[m = 1; While[m! \u003c= n, m++]; m - 1, {n, 0, 104}] (* _Jayanta Basu_, May 24 2013 *)"
			],
			"program": [
				"(Haskell)",
				"a084558 n = a090529 (n + 1) - 1  -- _Reinhard Zumkeller_, Jan 05 2014",
				"(Python)",
				"def a007623(n, p=2): return n if n\u003cp else a007623(n//p, p+1)*10 + n%p",
				"def a(n): return 0 if n==0 else len(str(a007623(n)))",
				"print([a(n) for n in range(101)]) # _Indranil Ghosh_, Jun 24 2017",
				"(PARI) a(n)={my(m=0);while(n\\=m++,);m-1} \\\\ _R. J. Cano_, Apr 09 2018"
			],
			"xref": [
				"A dual to A090529.",
				"Cf. A084555-A084557.",
				"Cf. A001069, A010096.",
				"Cf. A000142, A001563, A055089, A060130, A111095, A211664, A211670, A108731, A212598, A220656, A220657, A220658, A220659, A231716, A235224, A257510."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Antti Karttunen_, Jun 23 2003",
			"ext": [
				"Name clarified by _Antti Karttunen_, Apr 27 2015"
			],
			"references": 56,
			"revision": 57,
			"time": "2020-04-25T13:05:32-04:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}