{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A266180",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 266180,
			"data": "1,6,16,96,256,1536,4096,24576,65536,393216,1048576,6291456,16777216,100663296,268435456,1610612736,4294967296,25769803776,68719476736,412316860416,1099511627776,6597069766656,17592186044416,105553116266496,281474976710656",
			"name": "Decimal representation of the n-th iteration of the \"Rule 6\" elementary cellular automaton starting with a single ON (black) cell.",
			"comment": [
				"A001025 is a subsequence. - _Altug Alkan_, Dec 23 2015",
				"Rules 38, 134 and 166 also generate this sequence."
			],
			"reference": [
				"S. Wolfram, A New Kind of Science, Wolfram Media, 2002; p. 55."
			],
			"link": [
				"Robert Price, \u003ca href=\"/A266180/b266180.txt\"\u003eTable of n, a(n) for n = 0..999\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ElementaryCellularAutomaton.html\"\u003eElementary Cellular Automaton\u003c/a\u003e",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_Elementary_Cellular_Automata\"\u003eIndex to Elementary Cellular Automata\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,16)."
			],
			"formula": [
				"From _Colin Barker_, Dec 23 2015 and Apr 13 2019: (Start)",
				"a(n) = 4^(n-1)*(5-(-1)^n).",
				"a(n) = 16*a(n-2) for n\u003e1.",
				"G.f.: (1+6*x) / ((1-4*x)*(1+4*x)).",
				"(End)"
			],
			"mathematica": [
				"rule=6; rows=20; ca=CellularAutomaton[rule,{{1},0},rows-1,{All,All}]; (* Start with single black cell *) catri=Table[Take[ca[[k]],{rows-k+1,rows+k-1}],{k,1,rows}]; (* Truncated list of each row *) Table[FromDigits[catri[[k]],2],{k,1,rows}]   (* Decimal Representation of Rows *)",
				"LinearRecurrence[{0,16},{1,6},30] (* _Harvey P. Dale_, May 25 2016 *)"
			],
			"program": [
				"(Python) print([int(4**(n-1)*(5-(-1)**n)) for n in range(30)]) # _Karl V. Keller, Jr._, Jun 03 2021"
			],
			"xref": [
				"Cf. A001025, A266178, A266179, A019590, A003953, A003945, A000034, A032766, A042948, A035608."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Robert Price_, Dec 22 2015",
			"references": 6,
			"revision": 26,
			"time": "2021-06-04T09:50:34-04:00",
			"created": "2015-12-24T11:28:30-05:00"
		}
	]
}