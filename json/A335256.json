{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A335256",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 335256,
			"data": "1,1,1,1,3,1,1,6,4,3,1,1,10,10,15,5,10,1,1,15,20,45,15,60,15,6,15,10,1,1,21,35,105,35,210,105,21,105,70,105,7,21,35,1,1,28,56,210,70,560,420,56,420,280,840,105,28,168,280,210,280,8,28,56,35,1",
			"name": "Irregular triangle read by rows: row n gives the coefficients of the n-th complete exponential Bell polynomial B_n(x_1, x_2, ...,x_n) with monomials sorted into standard order.",
			"comment": [
				"\"Standard order\" means as produced by Maple's \"sort\" command.",
				"According to the Maple help files for the \"sort\" command, polynomials in multiple variables are \"sorted in total degree with ties broken by lexicographic order (this is called graded lexicographic order).\"",
				"Thus, for example, x_1^2*x_3 = x_1*x_1*x_3 \u003e x_1*x_2*x_2 = x_1*x_2^2, while x_1^2*x_4 = x_1*x_1*x_4 \u003e x_1*x_2*x_3.",
				"The number of terms in the n-th row is A000041(n), while the sum of the terms is A000110(n).",
				"The function Bell(n,k) in the PARI program below is a modification of a similar function in the PARI help files and uses the Faà di Bruno formula (cf. A036040)."
			],
			"reference": [
				"L. Comtet, Advanced Combinatorics, Reidel, 1974, pp. 134 and 307-310.",
				"J. Riordan, An Introduction to Combinatorial Analysis, Wiley, 1958, Chapter 2, Section 8 and table on page 49."
			],
			"link": [
				"E. T. Bell, \u003ca href=\"https://www.jstor.org/stable/1967979\"\u003ePartition polynomials\u003c/a\u003e, Ann. Math., 29 (1927-1928), 38-46.",
				"E. T. Bell, \u003ca href=\"https://www.jstor.org/stable/1968431\"\u003eExponential polynomials\u003c/a\u003e, Ann. Math., 35 (1934), 258-277.",
				"Peter Luschny, \u003ca href=\"https://oeis.org/wiki/User:Peter_Luschny/BellTransform\"\u003eThe Bell transform\u003c/a\u003e."
			],
			"formula": [
				"B_n(x[1], ..., x[n]) = Sum_{k=1..n} B_{n,k}(x[1], ..., x[n-k+1]), where B_{n,k} = B_{n,k}(x[1], ..., x[n-k+1]) are the partial exponential Bell polynomials that satisfy B_{n,1} = x[n] for n \u003e= 1 and B_{n,k} = (1/k)*Sum_{m=k-1..n-1} binomial(n,m)*x[n-m]*B_{m,k-1} for n \u003e= 2 and k = 2..n.",
				"E.g.f.: Exp(Sum_{i \u003e= 1} x_i*t^i/i!) = 1 + Sum_{n \u003e= 1} B_n(x_1, x_2,.., x_n))*t^n/n! [Comtet, p. 134, Eq. [3b]]."
			],
			"example": [
				"The first few complete exponential Bell polynomials are:",
				"(1) x[1];",
				"(2) x[1]^2 + x[2];",
				"(3) x[1]^3 + 3*x[1]*x[2] + x[3];",
				"(4) x[1]^4 + 6*x[1]^2*x[2] + 4*x[1]*x[3] + 3*x[2]^2 + x[4];",
				"(5) x[1]^5 + 10*x[1]^3*x[2] + 10*x[1]^2*x[3] + 15*x[1]*x[2]^2 + 5*x[1]*x[4] + 10*x[2]*x[3] + x[5];",
				"(6) x[1]^6 + 15*x[1]^4*x[2] + 20*x[1]^3*x[3] + 45*x[1]^2*x[2]^2 + 15*x[1]^2*x[4] + 60*x[1]*x[2]*x[3] + 15*x[2]^3 + 6*x[1]*x[5] + 15*x[2]*x[4] + 10*x[3]^2 + x[6].",
				"(7) x[1]^7 + 21*x[1]^5*x[2] + 35*x[1]^4*x[3] + 105*x[1]^3*x[2]^2 + 35*x[1]^3*x[4] + 210*x[1]^2*x[2]*x[3] + 105*x[1]*x[2]^3 + 21*x[1]^2*x[5] + 105*x[1]*x[2]*x[4] + 70*x[1]*x[3]^2 + 105*x[2]^2*x[3] + 7*x[1]*x[6] + 21*x[2]*x[5] + 35*x[3]*x[4] + x[7].",
				"...",
				"The first few rows of the triangle are",
				"  1;",
				"  1,  1;",
				"  1,  3,  1;",
				"  1,  6,  4,   3,   1;",
				"  1, 10, 10,  15,   5,  10,   1;",
				"  1, 15, 20,  45,  15,  60,  15,  6,  15, 10,   1;",
				"  1, 21, 35, 105,  35, 210, 105, 21, 105, 70, 105, 7, 21, 35, 1;",
				"  ..."
			],
			"maple": [
				"triangle := proc(numrows) local E, s, Q;",
				"E := add(x[i]*t^i/i!, i=1..numrows);",
				"s := series(exp(E), t, numrows+1);",
				"Q := k -\u003e sort(expand(k!*coeff(s, t, k)));",
				"seq(print(coeffs(Q(k))), k=1..numrows) end:",
				"triangle(8); # _Peter Luschny_, May 30 2020"
			],
			"program": [
				"(PARI) /* It produces the partial exponential Bell polynomials in decreasing degree, but the monomials are not necessarily in standard order. */",
				"Bell(n,k)= { my(x, v, dv, var = i-\u003eeval(Str(\"X\", i))); v = vector(n, i, if (i==1, 'E, var(i-1))); dv = vector(n, i, if (i==1, 'X*var(1)*'E, var(i))); x = diffop('E, v, dv, n) / 'E; if (k \u003c 0, subst(x,'X, 1), polcoeff(x, k, 'X)); };",
				"row(n) = for(k=1, n, print1(\"[\", Bell(n, n+1-k), \"]\", \",\"))"
			],
			"xref": [
				"For different versions, see A178867 and A268441.",
				"Cf. A000041, A000110, A036040."
			],
			"keyword": "nonn,tabf",
			"offset": "1,5",
			"author": "_Petros Hadjicostas_, May 28 2020",
			"references": 0,
			"revision": 43,
			"time": "2020-07-20T13:12:25-04:00",
			"created": "2020-05-29T04:32:32-04:00"
		}
	]
}