{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A273756",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 273756,
			"data": "41,43,47,53,61,71,83,97,113,131,151,173,197,223,251,281,313,347,383,421,461,503,547,593,641,691,743,797,73303,73361,73421,73483,3443897,3071069,3071137,15949847,76553693,365462323,365462399,2204597,9721,1842719,246407633,246407719,246407807,246407897,246407989",
			"name": "Least p for which min { x \u003e= 0 | p + (2n+1)*x + x^2 is composite } reaches the (local) maximum given in A273770.",
			"comment": [
				"All terms are prime, since this is necessary and sufficient to get a prime for x = 0.",
				"The values given in A273770 are the number of consecutive primes obtained for x = 0, 1, 2, ....",
				"Sequence A273595 is the subsequence of terms for which 2n+1 is prime.",
				"For even coefficients of the linear term, the answer would always be q=2, the only choice that yields a prime for x=0 and also for x=1 if (coefficient of the linear term)+3 is prime.",
				"The initial term a(n=0) = 41 corresponds to Euler's famous prime-generating polynomial 41+x+x^2. Some subsequent terms are equal to the primes this polynomial takes for x=1,2,3,.... This stems from the fact that adding 2 to the coefficient of the linear term is equivalent to shifting the x-variable by 1. Since here we require x \u003e= 0, we find a reduced subset of the previous sequence of primes, missing the first one, starting with q equal to the second one. (It is known that there is no better prime-generating polynomial of this form than Euler's, see the MathWorld page and A014556. \"Better\" means a larger p producing p-1 primes in a row. However, the prime k-tuple conjecture suggests that there should be arbitrarily long runs of primes of this form (for much larger p), i.e., longer than 41, but certainly much less than the respective p. Therefore we speak of local maxima.)"
			],
			"link": [
				"Don Reble, \u003ca href=\"/A273756/b273756.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Prime-GeneratingPolynomial.html\"\u003ePrime-Generating Polynomial\u003c/a\u003e",
				"Index to the OEIS, \u003ca href=\"/index/Pri\"\u003eEntries related to primes produced by polynomials\u003c/a\u003e."
			],
			"program": [
				"(PARI) A273756(n,p=2*n+1,L=10^(5+n\\10),m=0,Q)={forprime(q=1,L, for(x=1,oo, ispseudoprime(q+p*x+x^2)\u0026\u0026 next; x\u003em\u0026\u0026 [Q=q,m=x]; break));Q}"
			],
			"xref": [
				"Cf. A273595, A273770.",
				"Cf. also A002837 (n such that n^2-n+41 is prime), A007634 (n such that n^2+n+41 is composite), A005846 (primes of form n^2+n+41), A097823, A144051, A187057 ... A187060, A190800, A191456 ff.",
				"The first line of data coincides with that of A202018, A107448, A155884 (and also A140755, A142719, except for some initial terms), which are all related."
			],
			"keyword": "nonn",
			"offset": "0,1",
			"author": "_M. F. Hasler_, May 26 2016",
			"ext": [
				"Edited, following a remark by _Don Reble_, by _M. F. Hasler_, Jan 23 2018",
				"a(27) corrected and more terms from _Don Reble_, Feb 15 2018"
			],
			"references": 4,
			"revision": 36,
			"time": "2020-02-17T18:37:52-05:00",
			"created": "2016-05-29T13:29:12-04:00"
		}
	]
}