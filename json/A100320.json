{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A100320",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 100320,
			"data": "1,4,12,40,140,504,1848,6864,25740,97240,369512,1410864,5408312,20801200,80233200,310235040,1202160780,4667212440,18150270600,70690527600,275693057640,1076515748880,4208197927440,16466861455200",
			"name": "A Catalan transform of (1 + 2*x)/(1 - 2*x).",
			"comment": [
				"A Catalan transform of (1 + 2*x)/(1 - 2*x) under the mapping g(x) -\u003e g(x*c(x)). (Here c(x) is the g.f. of A000108.) The original sequence can be retrieved by g(x) -\u003e g(x*(1-x)).",
				"Hankel transform is A144704. - _Paul Barry_, Sep 19 2008",
				"Central terms of the triangle in A124927. - _Reinhard Zumkeller_, Mar 04 2012"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A100320/b100320.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/2004.04577\"\u003eOn a Central Transform of Integer Sequences\u003c/a\u003e, arXiv:2004.04577 [math.CO], 2020.",
				"Guo-Niu Han, \u003ca href=\"/A196265/a196265.pdf\"\u003eEnumeration of Standard Puzzles\u003c/a\u003e, 2011. [Cached copy]",
				"Guo-Niu Han, \u003ca href=\"https://arxiv.org/abs/2006.14070\"\u003eEnumeration of Standard Puzzles\u003c/a\u003e, arXiv:2006.14070 [math.CO], 2020."
			],
			"formula": [
				"G.f.: (1 + 2*x*c(x))/(1 - 2*x*c(x)), where c(x) is the g.f. of A000108.",
				"a(n) = 4*binomial(2*n-1, n) - 3*0^n.",
				"a(n) = binomial(2n, n)*(4*2^(n-1) - 0^n)/2^n.",
				"a(n) = Sum_{j=0..n} Sum_{k=0..n} C(2*n, n-k)*((2*k + 1)/(n + k + 1))*C(k, j)*(-1)^(j-k)*(4*2^(j-1) - 0^j).",
				"a(n) = A028329(n), n \u003e 0. - _R. J. Mathar_, Sep 02 2008",
				"a(n) = T(2*n,n), where T(n,k) = A132046(n,k). - _Paul Barry_, Sep 19 2008",
				"a(n) = Sum_{k=0..n} A039599(n,k)*A010684(k). - _Philippe Deléham_, Oct 29 2008",
				"a(n) = A095660(2*n,n) for n \u003e 0. - _Reinhard Zumkeller_, Apr 08 2012",
				"G.f.: G(0) - 1, where G(k) = 1 + 1/(1 - 2*x*(2*k + 1)/(2*x*(2*k + 1) + (k + 1)/G(k+1))); (continued fraction). - _Sergei N. Gladkovskii_, May 24 2013",
				"a(n) = [x^n] (1 + 2*x)/(1 - x)^(n+1). - _Ilya Gutkovskiy_, Oct 12 2017"
			],
			"mathematica": [
				"a[0] = 1; a[n_] := 2 Binomial[2 n, n];",
				"Table[a[n], {n, 0, 30}] (* _Jean-François Alcover_, Jul 31 2018 *)"
			],
			"program": [
				"(Haskell)",
				"a100320 n = a124927 (2 * n) n  -- _Reinhard Zumkeller_, Mar 04 2012"
			],
			"keyword": "easy,nonn",
			"offset": "0,2",
			"author": "_Paul Barry_, Nov 14 2004",
			"ext": [
				"Incorrect connection with A046055 deleted by _N. J. A. Sloane_, Jul 08 2009"
			],
			"references": 12,
			"revision": 51,
			"time": "2020-07-23T20:38:32-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}