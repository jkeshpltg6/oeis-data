{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A146076",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 146076,
			"data": "0,2,0,6,0,8,0,14,0,12,0,24,0,16,0,30,0,26,0,36,0,24,0,56,0,28,0,48,0,48,0,62,0,36,0,78,0,40,0,84,0,64,0,72,0,48,0,120,0,62,0,84,0,80,0,112,0,60,0,144,0,64,0,126,0,96,0,108,0,96,0,182,0,76,0,120,0,112,0,180,0,84,0,192,0,88,0,168,0,156",
			"name": "Sum of even divisors of n.",
			"comment": [
				"The usual OEIS policy is not to include sequences like this where alternate terms are zero; this is an exception. A074400 is the main entry.",
				"a(n) is also the total number of parts in all partitions of n into an even number of equal parts. - _Omar E. Pol_, Jun 04 2017"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A146076/b146076.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(2k-1) = 0, a(2k) = 2*sigma(k) for positive k.",
				"Dirichlet g.f.: zeta(s - 1)*zeta(s)*2^(1 - s). - _Geoffrey Critzer_, Mar 29 2015",
				"a(n) = A000203(n) - A000593(n). - _Omar E. Pol_, Apr 05 2016",
				"L.g.f.: -log(Product_{ k\u003e0 } (1-x^(2*k))) = Sum_{ n\u003e=0 } (a(n)/n)*x^n. - _Benedict W. J. Irwin_, Jul 04 2016",
				"a(n) = A000203(n)*(1 - (1/A038712(n))). - _Omar E. Pol_, Aug 01 2018"
			],
			"maple": [
				"A146076 := proc(n)",
				"    if type(n,'even') then",
				"        2*numtheory[sigma](n/2) ;",
				"    else",
				"        0;",
				"    end if;",
				"end proc: # _R. J. Mathar_, Dec 07 2017"
			],
			"mathematica": [
				"f[n_] := Plus @@ Select[Divisors[n], EvenQ]; Array[f, 150] (* _Vincenzo Librandi_, May 17 2013 *)",
				"a[n_] := DivisorSum[n, Boole[EvenQ[#]]*#\u0026]; Array[a, 100] (* _Jean-François Alcover_, Dec 01 2015 *)",
				"Table[CoefficientList[Series[-Log[QPochhammer[x^2, x^2]], {x, 0, 60}],x][[n + 1]] n, {n, 1, 60}] (* _Benedict W. J. Irwin_, Jul 04 2016 *)"
			],
			"program": [
				"(PARI) vector(80, n, if (n%2, 0, sumdiv(n, d, d*(1-(d%2))))) \\\\ _Michel Marcus_, Mar 30 2015",
				"(PARI) a(n) = if (n%2, 0, 2*sigma(n/2)); \\\\ _Michel Marcus_, Apr 01 2015"
			],
			"xref": [
				"Cf. A000203, A000593, A006128, A038712, A074400, A183063."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Apr 09 2009",
			"ext": [
				"Corrected by _Jaroslav Krizek_, May 07 2011"
			],
			"references": 26,
			"revision": 68,
			"time": "2018-08-02T04:13:19-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}