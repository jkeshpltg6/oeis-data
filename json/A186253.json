{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A186253",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 186253,
			"data": "2,5,11,23,47,79,157,313,619,1237,2473,4909,9817,19603,39199,78193,156019,311347,622669,1244149,2487739,4975111,9950221,19900399,39800797,79601461,159202369,318404629,636788881,1273577761,2547155419,5094310069,10188620041",
			"name": "Indices of zeros of the sequence u(n)=abs(u(n-1)-gcd(u(n-1),n-1)), u(1)=1.",
			"comment": [
				"For any fixed integer m\u003e=1 define u(1)=1 and u(n)=abs(u(n-1)-gcd(u(n-1),m*n-1)). Then (b_m(k))_{k\u003e=1} is the sequence of integers such that u(b_m(k))=0 and we conjecture that for k large enough m*b_m(k)+m-1 is a prime number. Here for m=1 it appears a(n) is prime for n\u003e=1.",
				"See A261301 for the sequence u relevant here (m=1). - _M. F. Hasler_, Aug 14 2015",
				"A261301(a(n)-1) = 1; A261301(a(n)) = 0; A261301(a(n)+1) = a(n). - _Reinhard Zumkeller_, Sep 07 2015"
			],
			"link": [
				"Moritz Firsching, \u003ca href=\"/A186253/b186253.txt\"\u003eTable of n, a(n) for n = 1..315\u003c/a\u003e",
				"B. Cloitre, \u003ca href=\"http://arxiv.org/abs/1101.4274\"\u003e10 conjectures in additive number theory\u003c/a\u003e, arXiv:1101.4274 [math.NT], 2011.",
				"M. F. Hasler, \u003ca href=\"https://oeis.org/wiki/User:M._F._Hasler/Work_in_progress/Rowland-Cloitre_type_prime_generating_sequences\"\u003eRowland-Cloître type prime generating sequences\u003c/a\u003e, OEIS Wiki, August 2015."
			],
			"formula": [
				"Conjecture: a(n) is asymptotic to c*2^n with c = 1.1861..."
			],
			"mathematica": [
				"a = m = 1; Reap[For[n = 2, n \u003c= 10^7, n++, a = Abs[a - GCD[a, m*n - 1]]; If[a == 0, Print[m*n + m - 1]; Sow[m*n + m - 1]]]][[2, 1]] (* _Jean-François Alcover_, Feb 05 2019, from PARI *)"
			],
			"program": [
				"(PARI) a=1;m=1;for(n=2,1e7,a=abs(a-gcd(a,m*n-1));if(a==0,print1(m*n+m-1,\",\")))",
				"(PARI)",
				"next_a(last_a) = {",
				"  local(A=last_a,B=last_a,C=2*last_a+1);",
				"  while(A\u003e0,",
				"    D=divisors(C);",
				"    k1=10*D[2];",
				"    for(j=2,#D, d=D[j];k=((A+1-B+d)/2)%d;",
				"      if(k==0,k=d); if(k\u003c=k1,k1=k;d1=d));",
				"    if(k1-1+d1==A,B=B+1);",
				"    A = max(A-(k1-1)-d1,0);",
				"    B = B + k1;",
				"    C = C - (d1 - 1);",
				"  );",
				"  return(B);",
				"}",
				"a=2",
				"for(n=1,99,print1(a,\", \");a=next_a(a)) \\\\ _Jan Büthe_ and _Moritz Firsching_, Aug 04 2015",
				"(PARI) m=a=k=1; for(n=1, 30, while( a\u003ed=vecmin(apply(p-\u003ea%p, factor(N=m*(k+a)+m-1)[,1])), a-=d+gcd(a-d,N); k+=1+d); k+=a+1; print1(a=N,\",\")) \\\\ _M. F. Hasler_, Aug 22 2015",
				"(Haskell)",
				"a186253 n = a186253_list !! (n-1)",
				"a186253_list = filter ((== 0) . a261301) [1..]",
				"-- _Reinhard Zumkeller_, Sep 07 2015"
			],
			"xref": [
				"Cf. A106108.",
				"Cf. A261301 - A261310; A186254 - A186263."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Benoit Cloitre_, Feb 16 2011",
			"ext": [
				"Definition clarified by _M. F. Hasler_, Aug 14 2015"
			],
			"references": 21,
			"revision": 37,
			"time": "2019-02-05T09:05:11-05:00",
			"created": "2011-02-16T04:57:12-05:00"
		}
	]
}