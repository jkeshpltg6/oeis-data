{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A094954",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 94954,
			"data": "1,1,1,1,2,1,1,3,5,1,1,4,11,13,1,1,5,19,41,34,1,1,6,29,91,153,89,1,1,7,41,169,436,571,233,1,1,8,55,281,985,2089,2131,610,1,1,9,71,433,1926,5741,10009,7953,1597,1,1,10,89,631,3409,13201,33461,47956,29681",
			"name": "Array T(k,n) read by antidiagonals. G.f.: x(1-x)/(1-kx+x^2), k\u003e1.",
			"comment": [
				"Also, values of polynomials with coefficients in A098493 (see Fink et al.). See A098495 for negative k.",
				"Number of dimer tilings of the graph S_{k-1} X P_{2n-2}."
			],
			"link": [
				"Alex Fink, Richard K. Guy, and Mark Krusemeyer, \u003ca href=\"https://doi.org/10.11575/cdm.v3i2.61940\"\u003ePartitions with parts occurring at most thrice\u003c/a\u003e, Contributions to Discrete Mathematics, Vol 3, No 2 (2008), pp. 76-114.",
				"Elizabeth Wilmer, \u003ca href=\"http://www.oberlin.edu/math/faculty/wilmer/OEISconj87.pdf\"\u003eA note on Stephan's conjecture 87\u003c/a\u003e",
				"Elizabeth Wilmer, \u003ca href=\"/A094954/a094954.pdf\"\u003eA note on Stephan's conjecture 87\u003c/a\u003e [cached copy]"
			],
			"formula": [
				"Recurrence: T(k, 1) = 1, T(k, 2) = k-1, T(k, n) = kT(k, n-1) - T(k, n-2).",
				"For n\u003e3, T(k, n) = [k(k-2) + T(k, n-1)T(k, n-2)] / T(k, n-3).",
				"T(k, n+1) = S(n, k) - S(n-1, k) = U(n, k/2) - U(n-1, k/2), with S, U = Chebyshev polynomials of second kind.",
				"T(k+2, n+1) = Sum[i=0..n, k^(n-i) * C(2n-i, i)] (from comments by Benoit Cloitre)."
			],
			"example": [
				"1,1,1,1,1,1,1,1,1,1,1,1,1,1, ...",
				"1,2,5,13,34,89,233,610,1597, ...",
				"1,3,11,41,153,571,2131,7953, ...",
				"1,4,19,91,436,2089,10009,47956, ...",
				"1,5,29,169,985,5741,33461,195025, ...",
				"1,6,41,281,1926,13201,90481,620166, ..."
			],
			"mathematica": [
				"max = 14; row[k_] := Rest[ CoefficientList[ Series[ x*(1-x)/(1-k*x+x^2), {x, 0, max}], x]]; t = Table[ row[k], {k, 2, max+1}]; Flatten[ Table[ t[[k-n+1, n]], {k, 1, max}, {n, 1, k}]] (* _Jean-François Alcover_, Dec 27 2011 *)"
			],
			"program": [
				"(PARI) T(k,n)=polcoeff(x*(1-x)/(1-k*x+x*x),n)"
			],
			"xref": [
				"Rows are first differences of rows in array A073134.",
				"Rows 2-14 are A000012, A001519, A079935/A001835, A004253, A001653, A049685, A070997, A070998, A072256, A078922, A077417, A085260, A001570. Other rows: A007805 (k=18), A075839 (k=20), A077420 (k=34), A078988 (k=66).",
				"Columns include A028387. Diagonals include A094955, A094956. Antidiagonal sums are A094957."
			],
			"keyword": "nonn,tabl",
			"offset": "1,5",
			"author": "_Ralf Stephan_, May 31 2004",
			"references": 29,
			"revision": 25,
			"time": "2019-09-19T07:57:32-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}