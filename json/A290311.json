{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A290311",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 290311,
			"data": "1,1,0,1,3,-1,1,17,-2,-1,1,80,49,-27,2,1,404,733,-153,-49,9,1,2359,7860,1622,-1606,150,9,1,16057,80715,58965,-17840,-3876,1163,-50,1,125656,858706,1150722,47365,-175756,18239,2359,-267,1,1112064,9710898,19571174,7548463,-3175846,-491809,194777,-9884,-413",
			"name": "Triangle T(n, k) read by rows: row n gives the coefficients of the row polynomials of the (n+1)-th diagonal sequence of the Sheffer triangle A094816 (special Poisson-Charlier).",
			"comment": [
				"The o.g.f. of the (n+1)-th diagonal sequence of the Sheffer triangle (e^x, -(log(1-x))) (the product of two Sheffer triangles A007318*A132393 = Pascal*|Stirling1|) is P(n, x)/(1 - x)^{2*n+1}, for n \u003e= 0., with the numerator polynomials P(n, x) = Sum_{k=0..n} T(n, k)*x^k.",
				"O.g.f.'s for diagonal sequences of Sheffer matrices (lower triangular) can be computed via Lagrange's theorem. For the special case of Jabotinsky matrices (1, f(x)) this has been done by P. Bala (see the link under A112007), and the method can be generalized to Sheffer (g(x), f(x)), as shown in the W. Lang link given below."
			],
			"link": [
				"Wolfdieter Lang, \u003ca href=\"http://arxiv.org/abs/1708.01421\"\u003eOn Generating functions of Diagonal Sequences of Sheffer and Riordan Number Triangles\u003c/a\u003e, arXiv:1708.01421 [math.NT], August 2017."
			],
			"formula": [
				"T(n, k) = [x^n] P(n, x) with the numerator polynomials (in rising powers) of the o.g.f. of the (n+1)-th diagonal sequence of the triangle A094816. See the comment above."
			],
			"example": [
				"The triangle T(n, k) begins:",
				"n\\k 0       1       2        3       4        5       6      7     8    9 ...",
				"0:  1",
				"1:  1       0",
				"2:  1       3      -1",
				"3:  1      17      -2       -1",
				"4:  1      80      49      -27       2",
				"5:  1     404     733     -153     -49        9",
				"6:  1    2359    7860     1622   -1606      150       9",
				"7:  1   16057   80715    58965  -17840    -3876    1163    -50",
				"8:  1  125656  858706  1150722   47365  -175756   18239   2359  -267",
				"9:  1 1112064 9710898 19571174 7548463 -3175846 -491809 194777 -9884 -413",
				"...",
				"n = 2: the o.g.f. of the third diagonal of triangle A094816, [1, 8, 29, 75, 160, ...] = A290312 is  (1 + 3*x - x^2)/(1 - x)^5."
			],
			"mathematica": [
				"rows = 10; nmax = 30(*terms to find every gf*);",
				"T = Table[(-1)^(n - k) Sum[Binomial[-j - 1, -n - 1] StirlingS1[j, k], {j, 0, n}], {n, 0, nmax}, {k, 0, nmax}];",
				"row[n_] := FindGeneratingFunction[Diagonal[T, -n], x] // Numerator // CoefficientList[-#, x]\u0026; row[0] = {1}; row[1] = {1, 0};",
				"Table[row[n], {n, 0, rows-1}] // Flatten (* _Jean-François Alcover_, Jan 26 2019 *)"
			],
			"xref": [
				"Cf. A094816, A290312, A290313, A290314."
			],
			"keyword": "sign,tabl",
			"offset": "0,5",
			"author": "_Wolfdieter Lang_, Jul 28 2017",
			"references": 5,
			"revision": 13,
			"time": "2019-01-26T14:27:59-05:00",
			"created": "2017-08-02T15:25:13-04:00"
		}
	]
}