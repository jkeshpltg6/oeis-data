{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A164928",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 164928,
			"data": "3,3,7,3,11,3,7,3,19,10,11,23,3,3,7,31,14,3,19,10,43,11,23,47,3,7,3,7,22,59,31,10,14,67,26,71,3,19,18,79,3,83,10,43,11,23,34,47,3,7,14,103,107,3,7,22,59,11,31,10,127,46,131,14,26,67,26,139,50,71,3,10,151,19,18",
			"name": "Sum of the odd prime divisors of numbers whose odd prime divisors are all of the form 4k+3.",
			"comment": [
				"We define a sequence b(n) = 3, 6, 7, 9, 11, 12, 14, 18, 19, 21, 22, 23, ... to consist of those numbers where all odd prime factors are primes contained in A002145, and which have at least one prime factor in this class; b(n) is basically A004144 without the powers of 2.",
				"a(n) is the sum of the distinct odd prime factors of b(n), where \"distinct\" means that the multiplicity (exponent) in the prime factorization of b(n) is ignored.",
				"Analogous sequence for primes of form 4k+1 is A164927.",
				"Analogous sequence for primes of form 6k+1 is A164929.",
				"Analogous sequence for primes of form 6k+5 is A164930."
			],
			"link": [
				"Harvey P. Dale, \u003ca href=\"/A164928/b164928.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e"
			],
			"example": [
				"a(11) = 10 because b(11) = 21 = 3*7, and 3+7 = 10.",
				"The smallest nonprime number, all of whose prime factors are of form 4n+3, whose sum of distinct prime factors is prime: b(181) = 3*7*19 = 399; 3+7+19 = 29."
			],
			"maple": [
				"isb := proc(n) fs := numtheory[factorset](n) minus {2} ; if fs = {} then RETURN(false); else for f in fs do if op(1,f) mod 4 \u003c\u003e 3 then RETURN(false) ; fi; od: RETURN(true) ; fi; end:",
				"b := proc(n) if n = 1 then 3; else for a from procname(n-1)+1 do if isb(a) then RETURN(a) ; fi; od: fi; end:",
				"A164928 := proc(n) local f; numtheory[factorset]( b(n)) minus {2} ; add(f,f=%) ; end: seq(A164928(n),n=1..120) ; # _R. J. Mathar_, Sep 08 2009"
			],
			"mathematica": [
				"sopd[n_]:=Module[{ff=Select[Transpose[FactorInteger[n]][[1]],OddQ]},If[ And@@ (Mod[#,4]==3\u0026/@ff),Total[ff],0]]; Select[Array[sopd,200],#\u003e0\u0026] (* _Harvey P. Dale_, Dec 16 2013 *)"
			],
			"xref": [
				"Cf. A000040, A002145, A009003, A164927-A164930."
			],
			"keyword": "easy,nonn",
			"offset": "1,1",
			"author": "_Jonathan Vos Post_, Aug 31 2009",
			"ext": [
				"Edited and extended by _R. J. Mathar_, Sep 08 2009"
			],
			"references": 4,
			"revision": 11,
			"time": "2019-04-05T05:32:09-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}