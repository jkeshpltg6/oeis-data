{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A286820",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 286820,
			"data": "1,2,3,8,25,6,7,8,9,30,33,24,26,126,30,32,153,126,152,120,126,726,5888,24,25,26,27,728,145,30,31,32,33,5066,840,144,5883,152,5070,120,123,126,129,5192,720,5888,752,144,147,150,153,728,848,864,46200,728",
			"name": "a(n) = smallest positive multiple of n whose factorial base representation contains only 0's and 1's.",
			"comment": [
				"All terms belong to A059590.",
				"a(n) = n iff n belongs to A059590.",
				"The sequence is well defined: for any n \u003e 0: according to the pigeonhole principle, among the n+1 first repunits in factorial base (A007489), there must be two distinct terms equal modulo n; their absolute difference is a positive multiple of n, and contains only 0's and 1's in factorial base.",
				"This sequence is to factorial base what A004290 is to decimal base."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A286820/b286820.txt\"\u003eTable of n, a(n) for n = 1..2000\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Factorial_number_system\"\u003eFactorial number system\u003c/a\u003e",
				"\u003ca href=\"/index/Fa#facbase\"\u003eIndex entries for sequences related to factorial base representation\u003c/a\u003e"
			],
			"example": [
				"The first terms are:",
				"n   a(n)     a(n) in factorial base",
				"--  ----     ----------------------",
				"1   1        1",
				"2   2        1,0",
				"3   3        1,1",
				"4   8        1,1,0",
				"5   25       1,0,0,1",
				"6   6        1,0,0",
				"7   7        1,0,1",
				"8   8        1,1,0",
				"9   9        1,1,1",
				"10  30       1,1,0,0",
				"11  33       1,1,1,1",
				"12  24       1,0,0,0",
				"13  26       1,0,1,0",
				"14  126      1,0,1,0,0",
				"15  30       1,1,0,0",
				"16  32       1,1,1,0",
				"17  153      1,1,1,1,1",
				"18  126      1,0,1,0,0",
				"19  152      1,1,1,1,0",
				"20  120      1,0,0,0,0"
			],
			"program": [
				"(PARI) isA059590(n) = my (r=2); while (n, if (n%r \u003e 1, return (0), n\\=r; r++)); return (1)",
				"a(n) = forstep (m=n, oo, n, if (isA059590(m), return (m)))"
			],
			"xref": [
				"Cf. A004290, A007489, A059590, A284750."
			],
			"keyword": "nonn,base",
			"offset": "1,2",
			"author": "_Rémy Sigrist_, Jun 24 2017",
			"references": 2,
			"revision": 12,
			"time": "2017-06-26T07:58:35-04:00",
			"created": "2017-06-26T07:58:35-04:00"
		}
	]
}