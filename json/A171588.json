{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A171588",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 171588,
			"data": "0,0,1,0,0,1,0,0,0,1,0,0,1,0,0,0,1,0,0,1,0,0,1,0,0,0,1,0,0,1,0,0,0,1,0,0,1,0,0,1,0,0,0,1,0,0,1,0,0,0,1,0,0,1,0,0,0,1,0,0,1,0,0,1,0,0,0,1,0,0,1,0,0,0,1,0,0,1,0,0,1,0,0,0,1,0,0,1,0,0,0,1,0,0,1,0,0,0,1,0,0,1,0,0,1",
			"name": "The Pell word: Fixed point of the morphism 0-\u003e001, 1-\u003e0.",
			"comment": [
				"From _Peter Bala_, Nov 22 2013: (Start)",
				"Sturmian word: equals the limit word S(infinity) where S(0) = 0, S(1) = 001 and for n \u003e= 1, S(n+1) = S(n)S(n)S(n-1). See the examples below.",
				"This sequence corresponds to the case k = 2 of the Sturmian word S_k(infinity) as defined in A080764. See A159684 for the case k = 1. (End)",
				"Characteristic word with slope 1 - 1/sqrt(2). Since the characteristic word with slope 1-theta is the mirror image of the characteristic word with slope theta, a(n)= 1 - A080764(n) for all n. - _Michel Dekking_, Jan 31 2017",
				"The positions of 0 comprise A001951 (Beatty sequence for sqrt(2)); the positions of 1 comprise A001952 (Beatty sequence for 2+sqrt(2)). - _Clark Kimberling_, May 11 2017"
			],
			"reference": [
				"J.-P. Allouche and J. Shallit, Automatic Sequences, Cambridge Univ. Press, 2003, p. 284.",
				"F. Michel Dekking, Substitution invariant Sturmian words and binary trees, arXiv:1705.08607, 2017."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A171588/b171588.txt\"\u003eTable of n, a(n) for n = 1..5000\u003c/a\u003e",
				"Scott Balchin and Dan Rust, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL20/Rust/rust3.html\"\u003eComputations for Symbolic Substitutions\u003c/a\u003e, Journal of Integer Sequences, Vol. 20 (2017), Article 17.4.1.",
				"Jean Berstel and Juhani Karhumäki, \u003ca href=\"http://www-igm.univ-mlv.fr/~berstel/Articles/2003TutorialCoWdec03.pdf\"\u003eCombinatorics on words-a tutorial\u003c/a\u003e. Bull. Eur. Assoc. Theor. Comput. Sci. EATCS, 79:178-228, 2003.",
				"M. Lothaire, \u003ca href=\"http://www-igm.univ-mlv.fr/~berstel/Lothaire/\"\u003eCombinatorics on Words\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Sturmian_word\"\u003eSturmian word\u003c/a\u003e"
			],
			"formula": [
				"a(n) = floor((n + 1)/(2 + sqrt(2))) - floor(n /(2 + sqrt(2))). - _Peter Bala_, Nov 22 2013",
				"a(n) = floor((n+1)(1 - 1/sqrt(2)) - floor(n (1 - 1/sqrt(2)). - _Michel Dekking_, Jan 31 2017"
			],
			"example": [
				"From _Peter Bala_, Nov 22 2013: (Start)",
				"The sequence of words S(n) begins",
				"S(0) = 0",
				"S(1) = 001",
				"S(2) = 001 001 0",
				"S(3) = 0010010 0010010 001",
				"S(4) = 00100100010010001 00100100010010001 0010010.",
				"The lengths of the words are [1, 3, 7, 17, 41, ...] = A001333 (apart from the initial term).  (End)"
			],
			"maple": [
				"Digits := 50: u := evalf(2 + sqrt(2)): A171588 := n-\u003efloor((n+1)/u) - floor(n/u): seq(A171588(n), n = 1..80); # _Peter Bala_, Nov 22 2013"
			],
			"mathematica": [
				"Table[Floor[(n + 1) (1 - 1/Sqrt[2]) - Floor[n (1 - 1/Sqrt[2])]], {n, 100}] (* _Vincenzo Librandi_, Jan 31 2017 *)",
				"Nest[Flatten[# /. {0 -\u003e {0, 0, 1}, 1 -\u003e {0}}] \u0026, {0}, 6] (* _Clark Kimberling_, May 11 2017 *)"
			],
			"program": [
				"(MAGMA) [Floor((n+1)*(1-1/Sqrt(2))-Floor(n*(1-1/Sqrt(2)))): n in [1..100]]; // _Vincenzo Librandi_, Jan 31 2017"
			],
			"xref": [
				"Cf. A000129, A001333, A001951, A001952, A003849, A080764, A159684."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "Alexis Monnerot-Dumaine (alexis.monnerotdumaine(AT)gmail.com), Dec 12 2009",
			"references": 28,
			"revision": 26,
			"time": "2020-07-03T14:55:05-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}