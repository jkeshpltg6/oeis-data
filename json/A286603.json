{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A286603",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 286603,
			"data": "1,2,3,4,5,6,7,8,9,10,6,11,12,13,13,14,10,15,16,17,18,19,13,20,14,17,21,22,23,24,18,25,26,27,26,28,29,20,22,30,17,31,32,33,34,24,26,35,36,37,24,38,27,39,24,39,40,30,20,41,42,31,43,44,33,45,46,47,31,45,24,48,49,50,35,51,31,41,40,52,53,47,33,54,55,56,39,57,30,58,59,41,60",
			"name": "Restricted growth sequence computed for sigma, A000203.",
			"comment": [
				"When filtering sequences (by equivalence class partitioning), this sequence can be used instead of A000203, because for all i, j it holds that: a(i) = a(j) \u003c=\u003e A000203(i) = A000203(j) \u003c=\u003e A286358(i) = A286358(j).",
				"Note that the latter equivalence indicates that this is also the restricted growth sequence of A286358."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A286603/b286603.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Si#SIGMAN\"\u003eIndex entries for sequences related to sigma(n)\u003c/a\u003e"
			],
			"example": [
				"Construction: we start with a(1)=1 for sigma(1)=1 (where sigma = A000203), and then after, for all n \u003e 1, whenever the value of sigma(n) has not been encountered before, we set a(n) to the least natural number k not already in sequence among a(1) .. a(n-1), otherwise [whenever sigma(n) = sigma(m), for some m \u003c n], we set a(n) = a(m), i.e., to the same value that was assigned to a(m).",
				"For n=2, sigma(2) = 3, not encountered before, thus we allot for a(2) the least so far unused number, which is 2, thus a(2) = 2.",
				"For n=3, sigma(3) = 4, not encountered before, thus we allot for a(3) the least so far unused number, which is 3, thus a(3) = 3.",
				"For n=4, sigma(4) = 7, not encountered before, thus we allot for a(4) the least so far unused number, which is 4, thus a(4) = 4.",
				"For n=5, sigma(5) = 6, not encountered before, thus we allot for a(5) the least so far unused number, which is 5, thus a(5) = 5.",
				"For n=6, sigma(6) = 12, not encountered before, thus we allot for a(6) the least so far unused number, which is 6, thus a(6) = 6.",
				"And this continues for n=7..10 because also for those n sigma obtains fresh new values, so here a(n) = n up to n = 10.",
				"But then comes n=11, where sigma(11) = 12, a value which was already encountered at n=6 for the first time, thus we set a(11) = a(6) = 6."
			],
			"mathematica": [
				"With[{nn = 93}, Function[s, Table[Position[Keys@ s, k_ /; MemberQ[k, n]][[1, 1]], {n, nn}]]@ Map[#1 -\u003e #2 \u0026 @@ # \u0026, Transpose@ {Values@ #, Keys@ #}] \u0026@ PositionIndex@ Array[DivisorSigma[1, #] \u0026, nn]] (* _Michael De Vlieger_, May 12 2017, Version 10 *)"
			],
			"program": [
				"(PARI)",
				"A000203(n) = sigma(n);",
				"rgs_transform(invec) = { my(occurrences = Map(), outvec = vector(length(invec)), u=1); for(i=1, length(invec), if(mapisdefined(occurrences,invec[i]), my(pp = mapget(occurrences, invec[i])); outvec[i] = outvec[pp] , mapput(occurrences,invec[i],i); outvec[i] = u; u++ )); outvec; };",
				"write_to_bfile(start_offset,vec,bfilename) = { for(n=1, length(vec), write(bfilename, (n+start_offset)-1, \" \", vec[n])); }",
				"write_to_bfile(1,rgs_transform(vector(10000,n,A000203(n))),\"b286603.txt\");"
			],
			"xref": [
				"Cf. A000203, A206036, A211656, A286358.",
				"Cf. also A101296, A286605, A286610, A286619, A286621, A286622, A286626, A286378."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Antti Karttunen_, May 11 2017",
			"references": 13,
			"revision": 16,
			"time": "2017-05-12T18:22:01-04:00",
			"created": "2017-05-12T18:22:01-04:00"
		}
	]
}