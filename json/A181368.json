{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A181368",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 181368,
			"data": "1,2,3,4,4,20,5,61,16,6,146,128,7,301,584,64,8,560,1992,704,9,966,5641,4272,256,10,1572,14002,18880,3584,11,2442,31471,67820,27136,1024,12,3652,65428,209820,147200,17408,13,5291,127699,579125,640096,157952",
			"name": "Triangle read by rows: T(n,k) is the number of L-convex polyominoes of semiperimeter n, having k maximal rectangles (n \u003e= 2, 1 \u003c= k \u003c= floor(n/2)). An L-convex polyomino is a convex polyomino in which any two cells can be connected by a path internal to the polyomino and which has at most 1 change of direction (i.e., one of the four orientations of the letter L). A maximal rectangle in an L-convex polyomino P is a rectangle included in P that is maximal with respect to inclusion.",
			"comment": [
				"Row n contains floor(n/2) entries.",
				"Sum of entries in row n is A003480(n-2).",
				"Sum_{k\u003e=1} k*T(n,k) = A181369(n)."
			],
			"reference": [
				"G. Castiglione, A. Frosini, E. Munarini, A. Restivo and S. Rinaldi, Combinatorial aspects of L-convex polyominoes, European Journal of Combinatorics, 28, 2007, 1724-1741 (see Fig. 9).",
				"G. Castiglione and A. Restivo, Reconstruction of L-convex polyominoes, Electronic Notes in Discrete Mathematics, Vol. 12, Elsevier Science, 2003."
			],
			"formula": [
				"T(n+2,k+1) = Sum_{j=0..2k} (-1)^j*2^(2k-j)*binomial(2k, j)*binomial(n+2k-j+1, 4k+1).",
				"T(n+2,k+1) = Sum_{j=0..2k} binomial(2k, j)*binomial(n+j+1, 2k+j+1).",
				"G.f. = G(t,z) = t*z^2*(1-z)^2/((1-z)^4 - t*z^2*(2-z)^2)."
			],
			"example": [
				"T(3,1)=2 because the L-convex polyominoes of semiperimeter 3 are the horizontal and the vertical dominoes, each containing one maximal rectangle.",
				"Triangle starts:",
				"  1;",
				"  2;",
				"  3,   4;",
				"  4,  20;",
				"  5,  61,  16;",
				"  6, 146, 128;"
			],
			"maple": [
				"T := proc (n, k) options operator, arrow: sum(binomial(2*k-2, j)*binomial(n+j-1, 2*k+j-1), j = 0 .. 2*k-2) end proc: for n from 2 to 14 do seq(T(n, k), k = 1 .. floor((1/2)*n)) end do; # yields sequence in triangular form"
			],
			"xref": [
				"Cf. A003480, A181369."
			],
			"keyword": "nonn,tabf",
			"offset": "2,2",
			"author": "_Emeric Deutsch_, Oct 17 2010",
			"references": 1,
			"revision": 8,
			"time": "2019-11-03T19:40:53-05:00",
			"created": "2010-10-20T03:00:00-04:00"
		}
	]
}