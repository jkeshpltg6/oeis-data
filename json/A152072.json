{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A152072",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 152072,
			"data": "1,2,1,3,2,1,4,4,2,1,5,6,4,2,1,6,9,8,4,2,1,7,12,12,8,4,2,1,8,16,18,16,8,4,2,1,9,20,27,24,16,8,4,2,1,10,25,36,36,32,16,8,4,2,1,11,30,48,54,48,32,16,8,4,2,1,12,36,64,81,72,64,32,16,8,4,2,1",
			"name": "Triangle read by rows: T(n,k) = the largest product of a partition of n into k positive integers (1 \u003c= k \u003c= n).",
			"comment": [
				"The optimal partition is P(n,k) = ([(n+i)/k] : 0 \u003c= i \u003c k).",
				"The table also appears in the solution of a maximum problem in arithmetic considered by K. Mahler and J. Popken. - J. van de Lune and Juan Arias-de-Reyna, Jan 05 2012",
				"T(n,k) is the number of ways to select k class representatives from the mod k partitioning of {1,2,...,n}. - _Dennis P. Walsh_, Nov 27 2012",
				"T(n,k) is the maximum number of length-k longest common subsequences of a pair of length-n strings. - _Cees H. Elzinga_, Jun 08 2014"
			],
			"reference": [
				"Cees H. Elzinga, M. Studer, Normalization of Distance and Similarity in Sequence Analysis in G. Ritschard \u0026 M. Studer (eds), Proceedings of the International Conference on Sequence Analysis and Related Methods, Lausanne, June 8-10, 2016, pp 445-468.",
				"K. Mahler and J. Popken, Over een Maximumprobleem uit de Rekenkunde (in Dutch), (On a Maximum Problem in Arithmetic), Nieuw Archief voor Wiskunde (3) 1 (1953), 1-15.",
				"David W. Wilson, Posting to Sequence Fans mailing List, Mar 11 2009"
			],
			"link": [
				"David W. Wilson, \u003ca href=\"/A152072/b152072.txt\"\u003eTable of n, a(n) for n = 1..10011\u003c/a\u003e",
				"Zhiwei Lin, H. Wang, C. H. Elzinga, \u003ca href=\"https://arxiv.org/abs/1609.04722\"\u003eConcordance and the Smallest Covering Set of Preference Orderings\u003c/a\u003e, arXiv preprint arXiv:1609.04722 [cs.AI], 2016."
			],
			"formula": [
				"T(n,k) = PROD(0 \u003c= i \u003c k; [(n+i)/k]).",
				"T(n,n-d) = 2^d = A000079(d) (d \u003c= n/2).",
				"MAX(1 \u003c= k \u003c= n, T(n,k)) = A000792(n).",
				"T(n,k) = (ceiling(n/k))^(n mod k)*(floor(n/k))^(k-n mod k). - _Dennis P. Walsh_, Nov 27 2012",
				"Sum_{k = 1..n} T(n,k) = A152074(n). - _David W. Wilson_, Jul 07 2016"
			],
			"example": [
				"Triangle begins:",
				"1",
				"2,1",
				"3,2,1",
				"4,4,2,1",
				"5,6,4,2,1",
				"6,9,8,4,2,1",
				"7,12,12,8,4,2,1",
				"8,16,18,16,8,4,2,1",
				"9,20,27,24,16,8,4,2,1",
				"10,25,36,36,32,16,8,4,2,1",
				"...",
				"T(7,3)=12 since there are 12 ways to selected class representatives from the mod 3 partitioning of {1,..,7} = {1,4,7} U {2,5} U {3,6}. - _Dennis P. Walsh_, Nov 27 2012"
			],
			"maple": [
				"T:= (n,k)-\u003e mul(floor((n+i)/k), i=0..k-1):",
				"seq(seq(T(n, k), k=1..n), n=1..12);"
			],
			"mathematica": [
				"T[n_, k_] := Product[ Floor[(n + i)/k], {i, 0, k - 1}]; Flatten@ Table[ T[n, k], {n, 12}, {k, n}] (* _Robert G. Wilson v_, Jul 08 2016 *)"
			],
			"program": [
				"(C++)",
				"#include \"boost/multiprecision/cpp_int.hpp\"",
				"using bigint = boost::multiprecision::cpp_int;",
				"using namespace std;",
				"bigint A152072(int n, int k)",
				"{",
				"        bigint v = 1;",
				"        for (int i = 0; i \u003c k; ++i)",
				"                v *= (n + i)/k;",
				"        return v;",
				"}",
				"int main()",
				"{",
				"        for (int i = 1, n = 1; i \u003c 10000; n++)",
				"                for (int k = 1; k \u003c= n; ++k, ++i)",
				"                        cout \u003c\u003c i \u003c\u003c \" \" \u003c\u003c A152072(n, k) \u003c\u003c endl;",
				"}",
				"// _David W. Wilson_, Jul 07 2016"
			],
			"xref": [
				"T(n,1) = n = A000027(n).",
				"T(n,2) = A002620(n-2).",
				"T(n,3) = A006501(n).",
				"T(n,4) = A008233(n).",
				"T(n,5) = A008382(n).",
				"T(n,6) = A008881(n).",
				"T(n,7) = A009641(n).",
				"T(n,8) = A009694(n).",
				"T(n,9) = A009714(n).",
				"T(n,n)=1, T(n,n-1)=A040000(n+1), T(n,n-2)=A113311(n+1).",
				"Cf. A152074 (row sums)."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Sep 16 2009",
			"references": 4,
			"revision": 54,
			"time": "2017-09-04T09:02:00-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}