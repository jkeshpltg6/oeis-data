{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A140995",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 140995,
			"data": "1,1,1,1,2,1,1,2,4,1,1,2,4,8,1,1,2,4,8,16,1,1,2,4,8,17,31,1,1,2,4,8,17,35,60,1,1,2,4,8,17,35,72,116,1,1,2,4,8,17,35,72,148,224,1,1,2,4,8,17,35,72,149,303,432,1,1,2,4,8,17,35,72,149,308,618,833,1,1,2,4,8,17,35,72,149,308,636,1257,1606,1",
			"name": "Triangle G(n, k) read by rows, for 0 \u003c= k \u003c= n, where G(n, n) = G(n+1, 0) = 1, G(n+2, 1) = 2, G(n+3, 2) = 4, G(n+4, 3) = 8, and G(n+5, m) = G(n+1, m-3) + G(n+1, m-4) + G(n+2, m-3) + G(n+3, m-2) + G(n+4, m-1) for n \u003e= 0 and m = 4..(n+4).",
			"comment": [
				"From _Petros Hadjicostas_, Jun 13 2019: (Start)",
				"This is a mirror of image of triangular array A140996. The current array has index of asymmetry s = 3 and index of obliqueness (obliquity) e = 1. Array A140996 has the same index of asymmetry, but has index of obliqueness e = 0. (In other related sequences, the author uses the letter y for the index of asymmetry and the letter z for the index of obliqueness, but in a picture that he posted in those sequences, the letters s and e are used instead. See, for example, the documentation for sequences A140998, A141065, A141066, and A141067.)",
				"Pascal's triangle A007318 has s = 0 and is symmetric, arrays A140998 and A140993 have s = 1 (with e = 0 and e = 1, respectively), and arrays A140997 and A140994 have s = 2 (with e = 0 and e = 1, respectively).",
				"If A(x,y) = Sum_{n,k \u003e= 0} G(n, k)*x^n*y^k is the bivariate g.f. for this array (with G(n, k) = 0 for 0 \u003c= n \u003c k) and B(x, y) = Sum_{n, k} A140996(n, k)*x^n*y^k, then A(x, y) = B(x*y, y^(-1)). This can be proved using formal manipulation of double series expansions and the fact G(n, k) = A140996(n, n-k) for 0 \u003c= k \u003c= n.",
				"If we let b(k) = lim_{n -\u003e infinity} G(n, k) for k \u003e= 0, then b(0) = 1, b(1) = 2, b(2) = 4, b(3) = 8, and b(k) = b(k-1) + b(k-2) + 2*b(k-3) + b(k-4) for k \u003e= 4. (The existence of the limit can be proved by induction on k.) Thus, the limiting sequence is 1, 2, 4, 8, 17, 35, 72, 149, 308, 636, 1314, 2715, 5609, 11588, 23941, 49462, 102188, 211120, 436173, ... (sequence A309462). (End)"
			],
			"link": [
				"Juri-Stepan Gerasimov, \u003ca href=\"/A140998/a140998.jpg\"\u003eStepan's triangles and Pascal's triangle are connected by the recurrence relation ...\u003c/a\u003e"
			],
			"formula": [
				"From _Petros Hadjicostas_, Jun 13 2019: (Start)",
				"G(n, k) = A140996(n, n-k) for 0 \u003c= k \u003c= n.",
				"Bivariate g.f.: Sum_{n,k \u003e= 0} G(n, k)*x^n*y^k = (x^5*y^4 - x^4*y^4 - x^3*y^3 + x^3*y^2 - x^2*y^2 + x^2*y - x*y + 1)/((1- x*y) * (1- x) * (1 - x*y - x^2*y^2 -x^3*y^3 - x^4*y^4 - x^4*y^3)).",
				"Substituting y = 1 in the above bivariate function and simplifying, we get the g.f. of row sums: 1/(1 - 2*x). Hence, the row sums are powers of 2; i.e., A000079.",
				"(End)"
			],
			"example": [
				"Triangle begins:",
				"  1",
				"  1 1",
				"  1 2 1",
				"  1 2 4 1",
				"  1 2 4 8  1",
				"  1 2 4 8 16  1",
				"  1 2 4 8 17 31  1",
				"  1 2 4 8 17 35 60   1",
				"  1 2 4 8 17 35 72 116   1",
				"  1 2 4 8 17 35 72 148 224   1",
				"  1 2 4 8 17 35 72 149 303 432   1",
				"  1 2 4 8 17 35 72 149 308 618 833 1",
				"  ..."
			],
			"xref": [
				"Cf. A000079, A007318, A140993, A140994, A140996, A140997, A140998, A141020, A141021, A141031, A141065, A141066, A141067, A141068, A141069, A141070, A141072, A141073, A309462."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Juri-Stepan Gerasimov_, Jul 08 2008",
			"ext": [
				"Entries checked by _R. J. Mathar_, Apr 14 2010",
				"Name edited by and more terms from _Petros Hadjicostas_, Jun 13 2019"
			],
			"references": 22,
			"revision": 50,
			"time": "2019-08-04T20:00:11-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}