{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A265017",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 265017,
			"data": "1,0,1,0,0,2,0,3,0,3,0,5,0,0,4,0,7,8,0,0,5,0,25,12,0,0,0,6,0,36,16,15,0,0,0,7,0,81,20,21,0,0,0,0,8,0,107,74,27,24,0,0,0,0,9,0,316,102,33,32,0,0,0,0,0,10,0,427,222,39,40,35,0,0,0,0,0,11",
			"name": "Total sum T(n,k) of number of lambda-parking functions of partitions lambda of n into distinct parts with smallest part k; triangle T(n,k), n\u003e=0, 0\u003c=k\u003c=n, read by rows.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A265017/b265017.txt\"\u003eRows n = 0..100, flattened\u003c/a\u003e",
				"Richard P. Stanley, \u003ca href=\"http://math.mit.edu/~rstan/transparencies/parking.pdf\"\u003eParking Functions\u003c/a\u003e, 2011."
			],
			"example": [
				"Triangle T(n,k) begins:",
				"00 :  1;",
				"01 :  0,   1;",
				"02 :  0,   0,   2;",
				"03 :  0,   3,   0,   3;",
				"04 :  0,   5,   0,   0,  4;",
				"05 :  0,   7,   8,   0,  0,  5;",
				"06 :  0,  25,  12,   0,  0,  0, 6;",
				"07 :  0,  36,  16,  15,  0,  0, 0, 7;",
				"08 :  0,  81,  20,  21,  0,  0, 0, 0, 8;",
				"09 :  0, 107,  74,  27, 24,  0, 0, 0, 0, 9;",
				"10 :  0, 316, 102,  33, 32,  0, 0, 0, 0, 0, 10;",
				"11 :  0, 427, 222,  39, 40, 35, 0, 0, 0, 0,  0, 11;",
				"12 :  0, 869, 286, 153, 48, 45, 0, 0, 0, 0,  0,  0, 12;"
			],
			"maple": [
				"p:= l-\u003e (n-\u003e n!*LinearAlgebra[Determinant](Matrix(n, (i, j)",
				"         -\u003e (t-\u003e`if`(t\u003c0, 0, l[i]^t/t!))(j-i+1))))(nops(l)):",
				"g:= (n, i, l)-\u003e `if`(i*(i+1)/2\u003cn, 0, `if`(n=0, p(l)*x^",
				"                `if`(l=[], 0, l[1]), g(n, i-1, l)+",
				"                `if`(i\u003en, 0, g(n-i, i-1, [i, l[]])))):",
				"T:= n-\u003e (f-\u003e seq(coeff(f, x, i), i=0..n))(g(n$2, [])):",
				"seq(T(n), n=0..16);"
			],
			"mathematica": [
				"p[l_] := With[{n = Length[l]}, n!*Det[Table[Function[t,",
				"     If[t \u003c 0, 0, l[[i]]^t/t!]][j - i + 1], {i, n}, {j, n}]]];",
				"g[n_, i_, l_] := If[i(i+1)/2 \u003c n, 0, If[n == 0, p[l]*x^",
				"     If[l == {}, 0, l[[1]]], g[n, i - 1, l] +",
				"     If[i \u003e n, 0, g[n - i, i - 1, Prepend[l, i]]]]];",
				"T[n_] := If[n == 0, {1}, CoefficientList[g[n, n, {}], x]];",
				"Table[T[n], {n, 0, 16}] // Flatten (* _Jean-François Alcover_, Aug 20 2021, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Row sums give A265016.",
				"Column k=0 gives A000007.",
				"Main diagonal gives A028310, first lower diagonal is A000004.",
				"T(2n+1,n) gives A005563.",
				"T(2n+2,n) gives A028347(n+2).",
				"T(2n+3,n) gives A028560."
			],
			"keyword": "nonn,tabl",
			"offset": "0,6",
			"author": "_Alois P. Heinz_, Nov 30 2015",
			"references": 2,
			"revision": 21,
			"time": "2021-08-20T04:27:06-04:00",
			"created": "2015-12-01T19:58:32-05:00"
		}
	]
}