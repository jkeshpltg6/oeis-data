{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A156792",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 156792,
			"data": "1,1,1,6,1,2,7,6,2,9,78,7,12,9,24,420,78,14,54,24,130,6872,420,156,63,144,130,720,17253,6872,840,702,168,780,720,8505,326552,17253,13744,3780,1872,910,4320,8505,35840",
			"name": "Triangle read by rows, T(n,k) = (A156791(n-k+1) * (A006973 * 0^(n-k))).",
			"comment": [
				"This is a convolution triangle: A006973 convolved with A156791.",
				"As a property of eigentriangles, sum of n-th row terms = rightmost term of next row."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A156792/b156792.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"Triangle read by rows, T(n,k) = M*Q =(A156791(n-k+1) * (A006973 * 0^(n-k)))",
				"M = an infinite lower triangular matrix with A156791: (1, 1, 6, 7, 78, ...) in every column.",
				"Q = an infinite lower triangular matrix with A006973 prefaced with a 1 as the main diagonal: (1, 1, 2, 9, 24, 130, 720, 8505, ...) and the rest zeros.",
				"Sum_{k=0..n} T(n, k) = A006973(n+1).",
				"T(n, 0) = T(n+1, 1) = A156791(n).",
				"T(n, k) = A156791(n-k+2)*([k=0] + A006973(k+1)). - _G. C. Greubel_, Jun 11 2021"
			],
			"example": [
				"First few rows of the triangle:",
				"       1,",
				"       1,     1;",
				"       6,     1,     2;",
				"       7,     6,     2,    9;",
				"      78,     7,    12,    9,   24;",
				"     420,    78,    14    54,   24, 130;",
				"    6872,   420,   156,   63,  144, 130,  720;",
				"   17253,  6872,   840,  702,  168, 780,  720, 8505;",
				"  326552, 17253, 13744, 3780, 1872, 910, 4320, 8505, 35840;",
				"  ...",
				"Example: Row 4 = (7, 6, 2, 9) = termwise products of (7, 6, 1, 1) and (1, 1, 2, 9)."
			],
			"mathematica": [
				"A006973[n_]:= A006973[n]= If[n\u003c4, Max[n-1, 0], (n-1)!*(1 + Sum[k*(-A006973[k]/k!)^(n/k), {k, Most[Divisors[n]]}])];",
				"S[n_, x_]:= Sum[A006973[j]*x^j, {j, 0, n+2}];",
				"A156791:= With[{p=100}, CoefficientList[Series[S[p,x]/(x + S[p,x]), {x,0,p}], x]]",
				"A156792[n_, k_]:= A156791[[n-k+2]]*(Boole[k==0] + A006973[k+1]);",
				"Table[A156792[n, k], {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, Jun 11 2021 *)"
			],
			"xref": [
				"Cf. A006973, A156791."
			],
			"keyword": "eigen,nonn,tabl",
			"offset": "0,4",
			"author": "_Gary W. Adamson_, Feb 15 2009",
			"ext": [
				"Typo in last line of triangle corrected by _Olivier Gérard_, Aug 11 2016"
			],
			"references": 3,
			"revision": 25,
			"time": "2021-06-12T23:21:26-04:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}