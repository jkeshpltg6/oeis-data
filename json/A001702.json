{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001702",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1702,
			"id": "M5148 N2234",
			"data": "1,24,154,580,1665,4025,8624,16884,30810,53130,87450,138424,211939,315315,457520,649400,903924,1236444,1664970,2210460,2897125,3752749,4809024,6101900,7671950,9564750,11831274,14528304,17718855,21472615,25866400,30984624",
			"name": "Generalized Stirling numbers.",
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A001702/b001702.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"D. S. Mitrinovic and R. S. Mitrinovic, \u003ca href=\"http://pefmath2.etf.rs/files/47/77.pdf\"\u003eTableaux d'une classe de nombres reliés aux nombres de Stirling\u003c/a\u003e, Univ. Beograd. Publ. Elektrotehn. Fak. Ser. Mat. Fiz., No. 77 (1962), 1-77.",
				"D. S. Mitrinovic and R. S. Mitrinovic, \u003ca href=\"https://www.jstor.org/stable/43667130\"\u003eTableaux d'une classe de nombres reliés aux nombres de Stirling\u003c/a\u003e, Univ. Beograd. Publ. Elektrotehn. Fak. Ser. Mat. Fiz., No. 77 (1962), 1-77 [jstor stable version].",
				"Robert E. Moritz, \u003ca href=\"/A001701/a001701.pdf\"\u003eOn the sum of products of n consecutive integers\u003c/a\u003e, Univ. Washington Publications in Math., 1 (No. 3, 1926), 44-49 [Annotated scanned copy]",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"\u003ca href=\"/index/Rec#order_07\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (7,-21,35,-35,21,-7,1)."
			],
			"formula": [
				"a(n) = (1/48)*(n-1)*n*(n+1)*(n+4)*(n^2+7n+14), n \u003e 1.",
				"G.f.: x + x^2*(x-4)*(x^2-2*x+6)/(x-1)^7. - _Simon Plouffe_ in his 1992 dissertation",
				"If we define f(n,i,a) = Sum_{k=0..n-i} binomial(n,k)*Stirling1(n-k,i)*Product_{j=0..k-1} (-a - j), then a(n-1) = -f(n,n-3,2), for n \u003e= 3. - _Milan Janjic_, Dec 20 2008",
				"a(n) = 7*a(n-1) - 21*a(n-2) + 35*a(n-3) - 35*a(n-4) + 21*a(n-5) - 7*a(n-6) + a(n-7). - _Colin Barker_, Jul 08 2020"
			],
			"maple": [
				"A001702 := proc(n)",
				"    if n = 1 then",
				"        1 ;",
				"    else",
				"        (n-1)*n*(n+1)*(n+4)*(n^2+7*n+14)/48 ;",
				"    end if;",
				"end proc: # _R. J. Mathar_, Sep 23 2016"
			],
			"mathematica": [
				"Join[{1}, Table[(n-1) n (n+1) (n+4) (n^2 + 7 n + 14)/48, {n, 2, 100}]] (* _T. D. Noe_, Aug 09 2012 *)",
				"CoefficientList[Series[1 +x*(x-4)*(x^2-2*x+6)/(x-1)^7, {x, 0, 100}], x] (* _Stefano Spezia_, Sep 30 2018 *)",
				"Join[{1},Table[Coefficient[Product[x + j, {j, 2, k}], x, k - 4], {k, 4, 40}]]  (* or *)  Join[{1}, LinearRecurrence[{7, -21, 35, -35, 21, -7, 1}, {24, 154, 580, 1665, 4025, 8624, 16884}, 40]] (* _Robert A. Russell_, Oct 04 2018 *)"
			],
			"program": [
				"(GAP) Concatenation([1],List([2..35],n-\u003e(n-1)*n*(n+1)*(n+4)*(n^2+7*n+14)/48)); # _Muniru A Asiru_, Sep 29 2018",
				"(MAGMA) [1] cat [n*(n^2-1)*(n+4)*(n^2+7*n+14)/48: n in [2..35]]; // _Vincenzo Librandi_, Sep 30 2018",
				"(PARI) vector(50, n, if(n==1, 1, (1/48)*(n-1)*n* (n+1)* (n+4)*(n^2 +7*n +14))) \\\\_G. C. Greubel_, Oct 06 2018"
			],
			"xref": [
				"For n \u003e 1, a(n) = A145324(n+2,4)."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"references": 2,
			"revision": 61,
			"time": "2021-03-12T22:32:35-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}