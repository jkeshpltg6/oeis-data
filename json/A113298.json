{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A113298",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 113298,
			"data": "1,0,1,0,2,2,3,2,5,4,7,6,11,10,15,14,22,22,30,30,44,44,58,60,81,84,107,112,145,154,190,202,253,270,327,352,429,462,550,594,711,770,904,980,1156,1256,1457,1586,1845,2008,2310,2516,2898,3160,3604,3930,4488,4894",
			"name": "Expansion of q^(1/12) * eta(q^10)^5 / ( eta(q^2) * eta(q^5)^2 * eta(q^20)^2) in powers of q.",
			"comment": [
				"Ramanujan theta functions: f(q) := Prod_{k\u003e=1} (1-(-q)^k) (see A121373), phi(q) := theta_3(q) := Sum_{k=-oo..oo} q^(k^2) (A000122), psi(q) := Sum_{k=0..oo} q^(k*(k+1)/2) (A010054), chi(q) := Prod_{k\u003e=0} (1+q^(2k+1)) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A113298/b113298.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Vaclav Kotesovec, \u003ca href=\"https://arxiv.org/abs/1509.08708\"\u003eA method of finding the asymptotics of q-series based on the convolution of generating functions\u003c/a\u003e, arXiv:1509.08708 [math.CO], 2015-2016.",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Euler transform of period 20 sequence [0, 1, 0, 1, 2, 1, 0, 1, 0, -2, 0, 1, 0, 1, 2, 1, 0, 1, 0, 0, ...].",
				"Expansion of phi(q^5) / f(-q^2) in powers of q where phi(), f() are Ramanujan theta functions.",
				"Expansion of G(x) * G(x^4) - x * H(x) * H(x^4) where G() = g.f. A003114 and H() = g.f. A003106 are the Rogers-Ramanujan functions.",
				"G.f.: (1 + 2 * Sum_{k\u003e0} x^(5*k^2)) / (Product_{k\u003e0} (1 - x^(2*k))).",
				"a(n) ~ exp(Pi*sqrt(n/3)) / (2*3^(1/4)*sqrt(10)*n^(3/4)). - _Vaclav Kotesovec_, Jul 11 2016"
			],
			"example": [
				"1/q + q^23 + 2*q^47 + 2*q^59 + 3*q^71 + 2*q^83 + 5*q^95 + 4*q^107 + ..."
			],
			"mathematica": [
				"nmax = 50; CoefficientList[Series[Product[(1-x^(10*k))^5 / ( (1-x^(2*k)) * (1-x^(5*k))^2 * (1-x^(20*k))^2), {k, 1, nmax}], {x, 0, nmax}], x] (* _Vaclav Kotesovec_, Jul 11 2016 *)",
				"eta[q_]:= q^(1/24)*QPochhammer[q]; CoefficientList[Series[q^(1/12)* eta[q^10]^5/(eta[q^2]*eta[q^5]^2*eta[q^20]^2), {q, 0, 50}], q] (* _G. C. Greubel_, Apr 17 2018 *)"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^10 + A)^5 / eta(x^2 + A) / eta(x^5 + A)^2 / eta(x^20 + A)^2, n))}",
				"(PARI) q='q+O('q^99); Vec(eta(q^10)^5/(eta(q^2)*eta(q^5)^2*eta(q^20)^2)) \\\\ _Altug Alkan_, Apr 18 2018"
			],
			"keyword": "nonn",
			"offset": "0,5",
			"author": "_Michael Somos_, Oct 24 2005",
			"references": 1,
			"revision": 20,
			"time": "2021-03-12T22:24:43-05:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}