{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A122861",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 122861,
			"data": "1,-3,2,0,2,-3,2,0,1,-6,2,0,2,0,2,0,3,-6,0,0,2,-3,2,0,2,-6,2,0,0,0,4,0,2,-3,2,0,2,-6,0,0,1,-6,2,0,4,0,2,0,0,-6,2,0,2,0,2,0,3,-6,2,0,2,0,0,0,2,-9,2,0,0,-6,2,0,4,0,2,0,2,0,0,0,2,-6,4,0,0,-3,4,0,0,-6,2,0,2,0,2,0,1,-6,0,0,4,-6,2,0,2",
			"name": "Expansion of phi(-q)chi(-q)psi(q^3) in powers of q where phi(),chi(),psi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) := Prod_{k\u003e=1} (1-(-q)^k) (see A121373), phi(q) := theta_3(q) := Sum_{k=-oo..oo} q^(k^2) (A000122), psi(q) := Sum_{k=0..oo} q^(k*(k+1)/2) (A010054), chi(q) := Prod_{k\u003e=0} (1+q^(2k+1)) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A122861/b122861.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-1/3)*eta(q)^3*eta(q^6)^2/(eta(q^2)^2*eta(q^3)) in powers of q.",
				"Euler transform of period 6 sequence [ -3, -1, -2, -1, -3, -2, ...].",
				"a(n)=b(3n+1) where b(n) is multiplicative and b(2^e) = -3(1+(-1)^e)/2 if e\u003e0, b(3^e) = 0^e, b(p^e) = e+1 if p == 1 (mod 6), b(p^e) = (1+(-1)^e)/2 if p == 5 (mod 6).",
				"a(4n+3)=0."
			],
			"mathematica": [
				"A122861[n_] := SeriesCoefficient[(QPochhammer[q]^3*QPochhammer[q^6]^2)/(QPochhammer[q^2]^2 *QPochhammer[q^3]), {q, 0, n}]; Table[A122861[n], {n, 0, 50}] (* _G. C. Greubel_, Oct 05 2017 *)"
			],
			"program": [
				"(PARI) {a(n)=local(A); if(n\u003c0, 0, A=x*O(x^n); polcoeff( eta(x+A)^3*eta(x^6+A)^2/eta(x^2+A)^2/eta(x^3+A), n))}",
				"(PARI) {a(n)=local(A, p, e); if(n\u003c0, 0, n=3*n+1; A=factor(n); prod(k=1, matsize(A)[1], if(p=A[k, 1], e=A[k, 2]; if(p==2, 3*(e%2-1), if(p==3, 0, if(p%6==1, e+1, !(e%2)))))))}"
			],
			"xref": [
				"A115979(3n+1)=A097109(3n+1)=a(n). A097195(n)=A033687(2n)=a(2n). -3*A033687(2n+1)=a(2n+1)."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Sep 15 2006",
			"references": 11,
			"revision": 9,
			"time": "2021-03-12T22:24:44-05:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}