{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A029935",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 29935,
			"data": "1,2,4,5,8,8,12,12,16,16,20,20,24,24,32,28,32,32,36,40,48,40,44,48,56,48,60,60,56,64,60,64,80,64,96,80,72,72,96,96,80,96,84,100,128,88,92,112,120,112,128,120,104,120",
			"name": "a(n) = Sum phi(d)*phi(n/d); d divides n.",
			"comment": [
				"Sum_{d|n} a(d) = A018804(n), Mobius transform of A018804. - _Franklin T. Adams-Watters_, Nov 19 2004",
				"Dirichlet convolution of A000010 with itself. - _R. J. Mathar_, Aug 28 2015"
			],
			"link": [
				"Gheorghe Coserea, \u003ca href=\"/A029935/b029935.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e"
			],
			"formula": [
				"Sum_{k=1..n} phi(gcd(n, k)). Multiplicative with a(p^e) = (e+1)*(p^e - p^(e - 1)) - (e - 1)*(p^(e - 1) - p^(e - 2)). - _Vladeta Jovovic_, Oct 30 2001",
				"Dirichlet g.f.: zeta(s-1)^2/zeta(s)^2. - _Franklin T. Adams-Watters_, Nov 19 2004",
				"Equals row sums of triangle A143258. [_Gary W. Adamson_, Aug 02 2008]",
				"a(n) \u003c= A000010(n) * A000005(n), with equality iff n = A005117(k) for some k. - _Gheorghe Coserea_, Oct 23 2016",
				"Sum_{k=1..n} a(k) ~ 9*n^2 * ((2*log(n) + 4*gamma - 1)/Pi^4 - 24*Zeta'(2)/Pi^6), where gamma is the Euler-Mascheroni constant A001620. - _Vaclav Kotesovec_, Jan 31 2019"
			],
			"maple": [
				"with(numtheory): A029935 := proc(n) local i,j; j := 0; for i in divisors(n) do j := j+phi(i)*phi(n/i); od; j; end;"
			],
			"mathematica": [
				"A029935[n_]:=DivisorSum[n,EulerPhi[#]*EulerPhi[n/#]\u0026]; Array[A029935, 50]"
			],
			"program": [
				"(PARI)",
				"a(n) = {",
				"  my(f = factor(n), fsz = matsize(f)[1],",
				"     g = prod(k=1, fsz, f[k,1]),",
				"     h = prod(k=1, fsz, sqr(f[k,1]-1)*f[k,2] + sqr(f[k,1])-1));",
				"  return(h*n\\sqr(g));",
				"};",
				"vector(54, n, a(n))  \\\\ _Gheorghe Coserea_, Oct 23 2016",
				"(PARI) a(n) = sumdiv(n, d, eulerphi(d)*eulerphi(n/d)); \\\\ _Michel Marcus_, Oct 23 2016"
			],
			"xref": [
				"Cf. A029936. Row sums of A159937."
			],
			"keyword": "mult,nonn",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_.",
			"references": 37,
			"revision": 36,
			"time": "2019-01-31T05:14:38-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}