{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A012257",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 12257,
			"data": "2,1,1,1,2,1,1,2,1,1,2,3,1,1,1,2,2,3,4,1,1,1,1,2,2,2,3,3,4,4,5,6,7,1,1,1,1,1,1,1,2,2,2,2,2,2,3,3,3,3,3,4,4,4,4,5,5,5,5,6,6,6,7,7,7,8,8,9,9,10,10,11,12,13,14",
			"name": "Irregular triangle read by rows: row 0 is {2}; if row n is {r_1, ..., r_k} then row n+1 is {r_k 1's, r_{k-1} 2's, r_{k-2} 3's, etc.}.",
			"comment": [
				"I have sometimes referred to this as Lionel Levine's triangle in lectures. - _N. J. A. Sloane_, Mar 21 2021",
				"The shape of each row tends to a limit curve when scaled to a fixed size. It is the same limit curve as this continuous version: start with f_0=x over [0,1]; then repeatedly reverse (1-x), integrate from zero (x-x^2/2), scale to 1 (2x-x^2) and invert (1-sqrt(1-x)). For the limit curve we have f'(0) = F(1) = lim A011784(n+2)/(A011784(n+1)*A011784(n)) ~ 0.27887706 (obtained numerically). - _Martin Fuller_, Aug 07 2006"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A012257/b012257.txt\"\u003eRows n = 0..9 of triangle, flattened\u003c/a\u003e",
				"Neil Sloane and Brady Haran, \u003ca href=\"https://www.youtube.com/watch?v=KNjPPFyEeLo\"\u003eThe Levine Sequence\u003c/a\u003e, Numberphile video (2021)"
			],
			"formula": [
				"Sum of row n = A011784(n+2); e.g. row 5 is {1, 1, 1, 2, 2, 3, 4} and the sum of the elements is 1+1+1+2+2+3+4 = 14 = A011784(7). - _Benoit Cloitre_, Aug 06 2003",
				"T(n,A011784(n+1)) = A011784(n). - _Reinhard Zumkeller_, Aug 11 2014"
			],
			"example": [
				"Initial rows are:",
				"{2},",
				"{1,1},",
				"{1,2},",
				"{1,1,2},",
				"{1,1,2,3},",
				"{1,1,1,2,2,3,4},",
				"{1,1,1,1,2,2,2,3,3,4,4,5,6,7},",
				"..."
			],
			"maple": [
				"T:= proc(n) option remember; `if`(n=0, 2, (h-\u003e",
				"      seq(i$h[-i], i=1..nops(h)))([T(n-1)]))",
				"    end:",
				"seq(T(n), n=0..8);  # _Alois P. Heinz_, Mar 31 2021"
			],
			"mathematica": [
				"row[1] = {1, 1}; row[n_] := row[n] = MapIndexed[ Function[ Table[#2 // First, {#1}]], row[n-1] // Reverse] // Flatten; Array[row, 7] // Flatten (* _Jean-François Alcover_, Feb 10 2015 *)",
				"NestList[Flatten@ MapIndexed[ConstantArray[First@ #2, #1] \u0026, Reverse@ #] \u0026, {1, 1}, 6] // Flatten (* _Michael De Vlieger_, Jul 12 2017 *)"
			],
			"program": [
				"(Haskell)",
				"a012257 n k = a012257_tabf !! (n-1) !! (k-1)",
				"a012257_row n = a012257_tabf !! (n-1)",
				"a012257_tabf = iterate (\\row -\u003e concat $",
				"                        zipWith replicate (reverse row) [1..]) [1, 1]",
				"-- _Reinhard Zumkeller_, Aug 11 2014, May 30 2012"
			],
			"xref": [
				"Cf. A001462, A011784 (row sums), A012257, A014643, A112798, A181819, A182850-A182858, A296150, A304455."
			],
			"keyword": "nonn,tabf,nice,look",
			"offset": "0,1",
			"author": "Lionel Levine (levine(AT)ultranet.com)",
			"ext": [
				"Initial row {2} added by _N. J. A. Sloane_, Mar 21 2021"
			],
			"references": 13,
			"revision": 49,
			"time": "2021-03-31T11:45:12-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}