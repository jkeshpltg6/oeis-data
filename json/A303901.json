{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A303901",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 303901,
			"data": "1,3,-2,9,-12,4,27,-54,36,-8,81,-216,216,-96,16,243,-810,1080,-720,240,-32,729,-2916,4860,-4320,2160,-576,64,2187,-10206,20412,-22680,15120,-6048,1344,-128,6561,-34992,81648,-108864,90720,-48384,16128,-3072,256,19683,-118098,314928,-489888,489888",
			"name": "Triangle read by rows of coefficients in expansion of (3-2x)^n, where n is a nonnegative integer.",
			"comment": [
				"This is a signed version of A038220.",
				"Row n gives coefficients in expansion of (3-2x)^n.",
				"The numbers in rows of triangles in A302747 and A303941 are along skew diagonals pointing top-left and top-right in center-justified triangle of coefficients in expansions of (3-2x)^n (A303901).",
				"This is the lower triangular Riordan matrix (1/(1 - 3*t), -2*t/(1-3*t), hence a convolution matrix.See the g.f.s. - _Wolfdieter Lang_, Jun 28 2018"
			],
			"reference": [
				"Shara Lalo and Zagros Lalo, Polynomial Expansion Theorems and Number Triangles, Zana Publishing, 2018, ISBN: 978-1-9995914-0-3, pp. 394, 396, 398."
			],
			"link": [
				"Zagros Lalo, \u003ca href=\"/A303901/a303901_1.pdf\"\u003eCenter-justified Triangle\u003c/a\u003e",
				"Zagros Lalo, \u003ca href=\"/A303901/a303901_2.pdf\"\u003eSkew Diagonals in center-justified Triangle\u003c/a\u003e"
			],
			"formula": [
				"T(0,0) = 1; T(n,k) = 3*T(n-1,k) -2*T(n-1,k-1) for k = 0,1,...,n; T(n,k)=0 for n or k \u003c 0.",
				"G.f. of row polynomials: 1 / (1 - 3t + 2t x).G.f. of column k: (-2*x)^k/(1-3*x)^(k+1), for k \u003e= 0."
			],
			"example": [
				"Triangle begins:",
				"n \\k 0     1       2       3      4      5       6      7      8    9  ...",
				"--------------------------------------------------------------------------",
				"0 |  1",
				"1 |  3     -2",
				"2 |  9     -12     4",
				"3 |  27    -54     36     -8",
				"4 |  81    -216    216    -96     16",
				"5 |  243   -810    1080   -720    240    -32",
				"6 |  729   -2916   4860   -4320   2160   -576    64",
				"7 |  2187  -10206  20412  -22680  15120  -6048   1344   -128",
				"8 |  6561  -34992  81648  -108864 90720  -48384  16128  -3072  256",
				"9 |  19683 -118098 314928 -489888 489888 -326592 145152 -41472 6912 -512",
				"…"
			],
			"mathematica": [
				"For[i = 0, i \u003c 4, i++, Print[CoefficientList[Expand[(3 - 2 x)^i],x]]]"
			],
			"xref": [
				"Cf. A013620 (unsigned), A000012 (row sums), A000351 (alternating row sums)."
			],
			"keyword": "tabl,easy,sign",
			"offset": "0,2",
			"author": "_Zagros Lalo_, May 02 2018",
			"ext": [
				"Edited - _Wolfdieter Lang_, Jun 28 2018"
			],
			"references": 6,
			"revision": 36,
			"time": "2018-06-28T17:30:31-04:00",
			"created": "2018-06-28T17:29:00-04:00"
		}
	]
}