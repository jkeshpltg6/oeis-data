{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A064325",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 64325,
			"data": "1,1,-2,13,-98,826,-7448,70309,-686090,6865150,-70057772,726325810,-7628741204,81002393668,-868066319108,9376806129493,-101988620430938,1116026661667318,-12277755319108748,135715825209716038,-1506587474535945788,16789107646422189868,-187747069029477151328",
			"name": "Generalized Catalan numbers C(-3; n).",
			"comment": [
				"See triangle A064334 with columns m built from C(-m; n), m \u003e= 0, also for Derrida et al. references."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A064325/b064325.txt\"\u003eTable of n, a(n) for n = 0..850\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{m=0..n-1} (n-m)*binomial(n-1+m, m)*(-3)^m/n.",
				"a(n) = (1/4)^n*(1 + 3*Sum_{k=0..n-1} C(k)*(-3*4)^k), n \u003e= 1, a(0) = 1; with C(n) = A000108(n) (Catalan).",
				"G.f.: (1+3*x*c(-3*x)/4)/(1-x/4) = 1/(1-x*c(-3*x)) with c(x) g.f. of Catalan numbers A000108.",
				"a(n) = hypergeometric([1-n, n], [-n], -3) for n\u003e0. - _Peter Luschny_, Nov 30 2014"
			],
			"mathematica": [
				"a[0] = 1;",
				"a[n_] := Sum[(n-m) Binomial[n+m-1, m] (-3)^m/n, {m, 0, n-1}];",
				"Table[a[n], {n, 0, 22}] (* _Jean-François Alcover_, Jul 30 2018 *)",
				"CoefficientList[Series[(7 +Sqrt[1+12*x])/(2*(4-x)), {x, 0, 30}], x] (* _G. C. Greubel_, May 03 2019 *)"
			],
			"program": [
				"(Sage)",
				"def a(n):",
				"    if n == 0: return 1",
				"    return hypergeometric([1-n, n], [-n], -3).simplify()",
				"[a(n) for n in range(24)] # _Peter Luschny_, Nov 30 2014",
				"(Sage) ((7 +sqrt(1+12*x))/(2*(4-x))).series(x, 30).coefficients(x, sparse=False) # _G. C. Greubel_, May 03 2019",
				"(PARI) a(n) = if (n==0, 1, sum(m=0, n-1, (n-m)*binomial(n-1+m, m)*(-3)^m/n)); \\\\ _Michel Marcus_, Jul 30 2018",
				"(PARI) my(x='x+O('x^30)); Vec((7 +sqrt(1+12*x))/(2*(4-x))) \\\\ _G. C. Greubel_, May 03 2019",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Rationals(), 30); Coefficients(R!( (7 +Sqrt(1+12*x))/(2*(4-x)) )); // _G. C. Greubel_, May 03 2019"
			],
			"xref": [
				"Cf. A064334, A000108."
			],
			"keyword": "sign,easy",
			"offset": "0,3",
			"author": "_Wolfdieter Lang_, Sep 21 2001",
			"references": 7,
			"revision": 18,
			"time": "2019-05-03T21:22:34-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}