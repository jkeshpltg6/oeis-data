{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A164082",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 164082,
			"data": "6,39,124,260,408,513,537,482,379,264,166,95,50,24,11,5,2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0",
			"name": "Rounded value of 2^(n-1) times the surface area of the unit sphere in 2n-dimensional space.",
			"comment": [
				"The floor of this real sequence is A164081, the ceiling is A164083.",
				"The surface area of the n-dimensional sphere of radius r is n*V_n*r^(n-1); see A072478/ A072479.",
				"There are 18 nonzero terms in this sequence. - _G. C. Greubel_, Sep 11 2017"
			],
			"reference": [
				"Conway, J. H. and Sloane, N. J. A. Sphere Packings, Lattices, and Groups, 2nd ed. New York: Springer-Verlag, p. 9, 1993.",
				"Coxeter, H. S. M. Regular Polytopes, 3rd ed. New York: Dover, 1973.",
				"Sommerville, D. M. Y. An Introduction to the Geometry of n Dimensions. New York: Dover, p. 136, 1958."
			],
			"link": [
				"Eric W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/Hypersphere.html\"\u003eHypersphere\u003c/a\u003e,"
			],
			"formula": [
				"a(n) = round(((2*Pi)^n)/(n-1)!)."
			],
			"example": [
				"Table of approximate real values before rounding up or down:",
				"========================",
				"n ((2*pi)^n) / (n-1)!",
				"1 6.28318531 = A019692",
				"2 39.4784176 = 2*A164102",
				"3 124.025107 = 4*A091925",
				"4 259.757576 = 8*A164109",
				"5 408.026246",
				"6 512.740903",
				"7 536.941018",
				"8 481.957131",
				"9 378.528246",
				"10 264.262568",
				"11 166.041068",
				"12 94.8424365",
				"13 49.6593836",
				"14 24.00147",
				"15 10.7718345",
				"16 4.5120955",
				"17 1.77189576",
				"18 0.654891141",
				"19 0.228600133",
				"20 0.075596684",
				"========================"
			],
			"maple": [
				"A164082 := proc(n) (2*Pi)^n/(n-1)! ; round(%) ; end: seq(A164082(n),n=1..80) ; # _R. J. Mathar_, Sep 09 2009"
			],
			"mathematica": [
				"Table[Round[(2*Pi)^n/(n - 1)!], {n, 1, 20}] (* _G. C. Greubel_, Sep 11 2017 *)"
			],
			"program": [
				"(PARI) for(n=1,20, print1(round((2*Pi)^n/(n-1)!), \", \")) \\\\ _G. C. Greubel_, Sep 11 2017"
			],
			"xref": [
				"Cf. A072345, A072346, A072478, A072479, A074457, A122510, A154255, A164081, A164083."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Jonathan Vos Post_, Aug 09 2009",
			"ext": [
				"Definition corrected by _R. J. Mathar_, Sep 09 2009"
			],
			"references": 3,
			"revision": 12,
			"time": "2019-02-24T02:05:10-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}