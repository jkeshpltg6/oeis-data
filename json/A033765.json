{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A033765",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 33765,
			"data": "1,1,1,3,1,2,5,2,3,7,4,4,10,3,3,11,6,4,12,6,5,19,6,8,16,7,10,17,7,8,25,10,9,20,8,8,27,12,11,30,11,14,27,12,14,29,14,12,37,15,11,42,15,14,34,12,16,44,18,16,36,18,17,39,17,20,59,18,19,42,22,24,49",
			"name": "Product t2(q^d); d | 6, where t2 = theta2(q)/(2*q^(1/4)).",
			"comment": [
				"Quadratic AGM theta functions: a(q) (see A004018), b(q) (A104794), c(q) (A005883).",
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Also the number of positive odd solutions to equation a^2 + 2*b^2 + 3*c^2 + 6*d^2 = 8*n + 12. - _Seiichi Manyama_, May 29 2017"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A033765/b033765.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-3) * (a(q) - a(q^3)) * c(q) / 16 in powers of q^2 where a(), c() are quadratic AGM theta functions. - _Michael Somos_, Sep 30 2013",
				"Expansion of (phi(x)^2 - phi(x^3)^2) * psi(x^2)^2 / 4 in powers of x where phi(), psi() are Ramanujan theta functions. - _Michael Somos_, Sep 30 2013"
			],
			"example": [
				"G.f. = 1 + x + x^2 + 3*x^3 + x^4 + 2*x^5 + 5*x^6 + 2*x^7 + 3*x^8 + 7*x^9 + ...",
				"G.f. = q^3 + q^5 + q^7 + 3*q^9 + q^11 + 2*q^13 + 5*q^15 + 2*q^17 + 3*q^19 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 2, 0, q] EllipticTheta[ 2, 0, q^2] EllipticTheta[ 2, 0, q^3] EllipticTheta[ 2, 0, q^6] / 16, {q, 0, 2 n + 3}]; (* _Michael Somos_, Sep 30 2013 *)"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A) * eta(x^4 + A)^2 * eta(x^6 + A) * eta(x^12 + A)^2 / (eta(x + A) * eta(x^3 + A)), n))}; /* _Michael Somos_, Sep 30 2013 */",
				"(MAGMA) A := Basis( ModularForms( Gamma0(24), 2), 105); A[4] + A[6]; /* _Michael Somos_, Aug 24 2014 */"
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Seiichi Manyama_, May 22 2017"
			],
			"references": 2,
			"revision": 22,
			"time": "2021-03-12T22:24:42-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}