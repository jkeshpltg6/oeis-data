{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A334950",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 334950,
			"data": "0,0,-1,1,-1,3,0,6,2,2,-3,7,1,13,6,20,12,12,3,21,11,11,0,22,10,10,-3,23,9,9,-6,24,8,8,-9,25,7,43,24,62,42,42,21,63,41,41,18,18,-6,42,17,17,-9,43,16,16,-12,44,15,15,-15,45,14,14,-18,46,13,79,45,113,78,78,42,114,77,77,39,39,0,78",
			"name": "Pairs (a,b) where \"a\" is the smallest candidate for the n-th term of Recamán's sequence and \"b\" is the n-th term of Recamán's sequence (A005132).",
			"comment": [
				"For n \u003e 0 and after A005132(n-1) the algorithm of Recamán's sequence first explores if A005132(n-1) - n = A334951(n) is a valid number to be its n-th term. If A334951(n) is nonnegative and not already in Recamán's sequence then it is accepted, so A005132(n) = A334951(n), otherwise A334951(n) is rejected and A005132(n) = A005132(n-1) + n, not A334951(n). This sequence lists the pairs [A334951(n), A005132(n)], with a(0) = 0."
			],
			"link": [
				"\u003ca href=\"/index/Rea#Recaman\"\u003eIndex entries for sequences related to Recamán's sequence\u003c/a\u003e"
			],
			"formula": [
				"a(2n) = A334951(n).",
				"a(2n+1) = A005132(n)."
			],
			"example": [
				"Illustration of initial terms:",
				"                                                       23",
				"                                               22       _",
				"                                       21       _      |",
				"                               20       _      | |     |",
				"                                _      | |     | |     |",
				"                               | |     | |     | |     |",
				"                               | |     | |     | |     |",
				"                               | |     | |     | |     |",
				"                               | |     | |     | |     |",
				"                               | |     | |     | |     |",
				"                           13  | |     | |     | |     |",
				"                            _  | | 12  | |     | |     |",
				"                           | | | |_ _  | | 11  | |     |",
				"                           | | |     | | |_ _  | |  10 |",
				"                           | | | 12  | |     | | |_ _  |",
				"                           | | |     | | 11  | |     | |",
				"                        7  | | |     | |     | | 10  | |",
				"                6       _  | | |     | |     | |     | |",
				"                _      | | | |_|     | |     | |     | |",
				"               | |     | | |         | |     | |     | |",
				"            3  | |     | | |  6      | |     | |     | |",
				"            _  | |  2  | | |         |_|     | |     | |",
				"        1  | | | |_ _  | | |                 | |     | |",
				"    0   _  | | |     | | |_|          3      | |     | |",
				"  _ _  | | | |_|  2  | |                     |_|     | |",
				"     |_| |_|         | |  1                          | |",
				"  0           0      | |                      0      | |",
				"     -1  -1          |_|                             |_|",
				".",
				"                     -3                              -3",
				".",
				"In the above diagram the numbers that are written below the path are the terms of A334951 (the candidates for A005132). The numbers that are written above the path are the terms of Recamán's sequence A005132. The length of the n-th vertical-line-segment equals the absolute value of A334952(n).",
				"For n = 4, after A005132(4-1) = 6 the algorithm of Recamán's sequence first explores if A334951(4) = 6 - 4 = 2 is a valid number to be its 4th term. We can see that 2 is nonnegative and not already in Recamán's sequence, then it is accepted, so A005132(4) = A334951(4) = 2.",
				"For n = 5, after A005132(5-1) = 2 the algorithm first explores if A334951(5) = 2 - 5 = -3 is a valid number to be its 5th term. We can see that -3 is negative, so -3 is rejected.",
				"For n = 6, after A005132(6-1) = 7 the algorithm first explores if A334951(6) = 7 - 6 = 1 is a valid number to be its 6th term. We can see that 1 is already in Recamán's sequence, so 1 is rejected."
			],
			"xref": [
				"Cf. A334951 and A005132 interleaved.",
				"Cf. A334952 (first differences)."
			],
			"keyword": "sign",
			"offset": "0,6",
			"author": "_Omar E. Pol_, May 17 2020",
			"references": 2,
			"revision": 47,
			"time": "2020-05-26T06:31:29-04:00",
			"created": "2020-05-26T06:31:29-04:00"
		}
	]
}