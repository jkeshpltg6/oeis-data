{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005195",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5195,
			"id": "M0776",
			"data": "1,1,2,3,6,10,20,37,76,153,329,710,1601,3658,8599,20514,49905,122963,307199,775529,1977878,5086638,13184156,34402932,90328674,238474986,632775648,1686705630,4514955632,12132227370,32717113805,88519867048,240235675303",
			"name": "Number of forests with n unlabeled nodes.",
			"comment": [
				"Same as \"Number of forests with n nodes that are perfect graphs\" [see Hougardy]. - _N. J. A. Sloane_, Dec 04 2015"
			],
			"reference": [
				"F. Harary and E. M. Palmer, Graphical Enumeration, Academic Press, NY, 1973, pp. 58-59.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A005195/b005195.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e (first 201 terms from T. D. Noe)",
				"S. Hougardy, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2006.05.021\"\u003eClasses of perfect graphs\u003c/a\u003e, Discr. Math. 306 (2006), 2529-2571.",
				"E. M. Palmer and A. J. Schwenk, \u003ca href=\"http://dx.doi.org/10.1016/0095-8956(79)90073-X\"\u003eOn the number of trees in a random forest\u003c/a\u003e, J. Combin. Theory, B 27 (1979), 109-121.",
				"N. J. A. Sloane, \u003ca href=\"/transforms.txt\"\u003eTransforms\u003c/a\u003e",
				"Peter Steinbach, \u003ca href=\"/A000055/a000055_12.pdf\"\u003eField Guide to Simple Graphs, Volume 3\u003c/a\u003e, Part 12 (For Volumes 1, 2, 3, 4 of this book see A000088, A008406, A000055, A000664, respectively.)",
				"Peter Steinbach, \u003ca href=\"/A000664/a000664_5.pdf\"\u003eField Guide to Simple Graphs, Volume 4\u003c/a\u003e, Part 5 (For Volumes 1, 2, 3, 4 of this book see A000088, A008406, A000055, A000664, respectively.)",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Forest.html\"\u003eForest.\u003c/a\u003e",
				"\u003ca href=\"/index/Fo#forests\"\u003eIndex entries for sequences related to forests\u003c/a\u003e"
			],
			"formula": [
				"Euler transform of A000055: Product_{n\u003e0} (1-x^n)^(-A000055(n)). a(n) = 1/n*Sum_{k=1..n} b(k)*a(n-k), where b(k) = Sum_{d divides k} d*A000055(d). - _Vladeta Jovovic_, Sep 05 2002",
				"G.f.: exp(sum_{k\u003e0} B(x^k)/k ), where B(x) = x + x^2 + x^3 + 2*x^4 + 3*x^5 + 6*x^6 + 11*x^7 + ... = C(x)-1 and C is the g.f. for A000055.",
				"a(n) ~ c * d^n / n^(5/2), where d = A051491 = 2.9557652856519949747148..., c = 1.023158422... . - _Vaclav Kotesovec_, Nov 16 2014"
			],
			"mathematica": [
				"EulerTransform[ seq_List ] := With[{m = Length[seq]}, CoefficientList[ Series[ Times @@ (1/(1 - x^Range[m])^seq), {x, 0, m}], x]];",
				"b[n_] := b[n] = If[n \u003c= 1, n, Sum[ Sum[ d*b[d], {d, Divisors[j]}]*b[n - j], {j, 1, n - 1}]/(n - 1)];",
				"a55[n_] := a55[n] = If[n == 0, 1, b[n] - (Sum[ b[k]*b[n - k], {k, 0, n}] - If[Mod[n, 2] == 0, b[n/2], 0])/2]; A000055 = Table[ a55[n], {n, 1, 31}]; EulerTransform[ A000055 ] (* _Jean-François Alcover_, Mar 15 2012 *)"
			],
			"xref": [
				"Cf. A000055, A095133 (by number of trees), A136605 (by number of edges), A144958 (first differences).",
				"A diagonal of A144215."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Vladeta Jovovic_, Sep 05 2002"
			],
			"references": 39,
			"revision": 56,
			"time": "2021-08-09T21:29:29-04:00",
			"created": "1991-05-20T03:00:00-04:00"
		}
	]
}