{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A307226",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 307226,
			"data": "0,0,1,0,1,2,0,1,3,2,0,4,1,3,2,0,4,5,1,3,2,0,4,6,5,1,3,2,7,0,4,6,5,1,3,2,7,0,4,6,5,1,3,2,8,7,0,4,6,5,1,3,2,9,8,7,0,4,6,5,1,3,10,2,9,8,7,0,4,6,5,1,11,3,10,2,9,8",
			"name": "Triangle read by rows: drop and bounce.",
			"comment": [
				"Number k starts with 1, 2, 3, ... 'points' and is 'dropped' on the initial value of 0. k then 'bounces' on the numbers already in the sequence according to the following rules:",
				"- k loses 1 point for each bounce.",
				"- After the first bounce (i.e., on 0) k moves to the right. If k bounces on a number already in the sequence which has a higher value than k, then the direction reverses. At lower or equal values, k continues to move in the same direction.",
				"- If k reaches the start or the end of the sequence or has 0 points left, the starting value of k is added to the sequence at that position."
			],
			"example": [
				"For k = 4:",
				"4 points, bounce on 0, move to the right;",
				"3 points, bounce on 1, move to the right;",
				"2 points, bounce on 3, reverse direction;",
				"1 point, bounce on 1, move to the left;",
				"0 points: k = 4 is inserted between 0 and 1. (Although equal to the next value 0, k has no more points left to bounce over it.)",
				"The first iterations:",
				"  [0];",
				"  [0, 1];",
				"  [0, 1, 2];",
				"  [0, 1, 3, 2];",
				"  [0, 4, 1, 3, 2];",
				"  [0, 4, 5, 1, 3, 2];",
				"  [0, 4, 6, 5, 1, 3, 2];",
				"  [7, 0, 4, 6, 5, 1, 3, 2];",
				"  [7, 0, 4, 6, 5, 1, 3, 2, 8];",
				"  ..."
			],
			"program": [
				"(Python)",
				"seq = [0]",
				"for k in range (1, 10):",
				"  points = k",
				"  position = seq.index(0)",
				"  direction = 1",
				"  while points \u003e 0 and position \u003e= 0 and position \u003c len(seq):",
				"    if points \u003c seq[position]: direction *= -1",
				"    points -= 1",
				"    position += direction",
				"  else:",
				"    if position \u003c 0: seq.insert(0, k)",
				"    elif position == len(seq): seq.append(k)",
				"    elif points == 0 and direction == 1: seq.insert(position, k)",
				"    else: seq.insert(position - direction, k)",
				"print(seq)",
				"(PARI) process(row, n) = {my(pos = 1, dir = 1, m = n); while (m \u0026\u0026 (pos \u003e= 1) \u0026\u0026 (pos \u003c= #row), if (m \u003c row[pos], dir = -dir); pos += dir; m--;); if (pos == 0, return (concat(n, row))); if (pos == #row +1, return (concat(row, n))); if (dir == -1, pos ++); my(nrow = vector(#row+1)); for (k=1, pos-1, nrow[k] = row[k];); nrow[pos] = n; for (k = pos+1, #row+1, nrow[k] = row[k-1];); return (nrow);}",
				"tabl(nn) = {row = [0]; print(row); for (n=1, nn, row = process(row, n); print(row););} \\\\ _Michel Marcus_, Apr 13 2019"
			],
			"xref": [
				"Cf. A307326."
			],
			"keyword": "nonn,tabl",
			"offset": "0,6",
			"author": "_Jan Koornstra_, Mar 31 2019",
			"references": 1,
			"revision": 53,
			"time": "2019-06-07T11:13:12-04:00",
			"created": "2019-06-07T11:13:12-04:00"
		}
	]
}