{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A173741",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 173741,
			"data": "1,1,1,1,6,1,1,7,7,1,1,8,10,8,1,1,9,14,14,9,1,1,10,19,24,19,10,1,1,11,25,39,39,25,11,1,1,12,32,60,74,60,32,12,1,1,13,40,88,130,130,88,40,13,1,1,14,49,124,214,256,214,124,49,14,1,1,15,59,169,334,466,466,334,169,59,15,1",
			"name": "T(n,k) = binomial(n,k) + 4 for 1 \u003c= k \u003c= n - 1, n \u003e= 2, and T(n,0) = T(n,n) = 1 for n \u003e= 0, triangle read by rows.",
			"comment": [
				"For n \u003e= 1, row n sums to 2*A100314(n)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A173741/b173741.txt\"\u003eRows n = 0..100 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"From _Franck Maminirina Ramaharo_, Dec 09 2018:(Start)",
				"T(n,k) = A007318(n,k) + 2*(1 - A103451(n,k)).",
				"T(n,k) = 5*A007318(n,k) - 4*A132044(n,k).",
				"n-th row polynomial is 2*(1 - (-1)^(2^n)) + (1 + x)^n + 4*(x - x^n)/(1 - x).",
				"G.f.: (1 - (1 + x)*y + 5*x*y^2 - 4*(x + x^2)*y^3)/((1 - y)*(1 - x*y)*(1 - y - x*y)).",
				"E.g.f.: (4 - 4*x + 4*x*exp(y) - 4*exp(x*y) + (1 - x)*exp((1 + x)*y))/(1 - x). (End)",
				"Sum_{k=0..n} T(n, k) = 2^n + 4*(n - 1 + [n=0]) = 2*A100314(n). - _G. C. Greubel_, Feb 13 2021"
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  1,  1;",
				"  1,  6,  1;",
				"  1,  7,  7,   1;",
				"  1,  8, 10,   8,   1;",
				"  1,  9, 14,  14,   9,   1;",
				"  1, 10, 19,  24,  19,  10,   1;",
				"  1, 11, 25,  39,  39,  25,  11,   1;",
				"  1, 12, 32,  60,  74,  60,  32,  12,  1;",
				"  1, 13, 40,  88, 130, 130,  88,  40, 13,  1;",
				"  1, 14, 49, 124, 214, 256, 214, 124, 49, 14, 1;",
				"  ..."
			],
			"mathematica": [
				"T[n_, m_] = Binomial[n, m] + 4*If[m*(n - m) \u003e 0, 1, 0];",
				"Flatten[Table[T[n, m], {n, 0, 10}, {m, 0, n}]]"
			],
			"program": [
				"(Maxima) T(n,k) := if k = 0 or k = n then 1 else binomial(n, k) + 4$",
				"create_list(T(n, k), n, 0, 12, k, 0, n); /* _Franck Maminirina Ramaharo_, Dec 09 2018 */",
				"(Sage)",
				"def T(n, k): return 1 if (k==0 or k==n) else binomial(n, k) + 4",
				"flatten([[T(n,k) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Feb 13 2021",
				"(Magma)",
				"T:= func\u003c n,k | k eq 0 or k eq n select 1 else Binomial(n,k) + 4 \u003e;",
				"[T(n,k): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Feb 13 2021"
			],
			"xref": [
				"Cf. A100314, A103451, A156050.",
				"Sequences of the form binomial(n, k) + q: A132823 (q=-2), A132044 (q=-1), A007318 (q=0), A132735 (q=1), A173740 (q=2), this sequence (q=4), A173742 (q=6)."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,5",
			"author": "_Roger L. Bagula_, Feb 23 2010",
			"ext": [
				"Edited and name clarified by _Franck Maminirina Ramaharo_, Dec 09 2018"
			],
			"references": 4,
			"revision": 11,
			"time": "2021-02-13T14:50:15-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}