{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A266711",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 266711,
			"data": "-6,2,18,-102,-714,-4826,-33222,-227298,-1558962,-10682534,-73226346,-501882042,-3439999878,-23577981122,-161606223954,-1107664654566,-7592048797962,-52036670543258,-356664661728582,-2444615917773474,-16755646877311986,-114844911923314982",
			"name": "Coefficient of x in the minimal polynomial of the continued fraction [1^n,sqrt(2),1,1,...], where 1^n means n ones.",
			"comment": [
				"See A265762 for a guide to related sequences."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A266711/b266711.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (5,15,-15,-5,1)."
			],
			"formula": [
				"a(n) = 5*a(n-1) + 15*a(n-2) - 15*a(n-3) - 5*a(n-4) + a(n-5).",
				"G.f.:  2*(3 -16*x -49*x^2 +156*x^3 +237*x^4 -280*x^5 -88*x^6 +18*x^7)/(-1 +5*x +15*x^2 -15*x^3 -5*x^4 +x^5)."
			],
			"example": [
				"Let p(n,x) be the minimal polynomial of the number given by the n-th continued fraction:",
				"[sqrt(2),1,1,1,...] has p(0,x) = -1 - 6 x - 5 x^2 + 2 x^3 + x^4, so a(0) = -6;",
				"[1,sqrt(2),1,1,1,...] has p(1,x) = 1 + 2 x - 7 x^2 + 2 x^3 + x^4, so a(1) = 2;",
				"[1,1,sqrt(2),1,1,1...] has p(2,x) = -9 + 18 x - 7 x^2 - 2 x^3 + x^4, so a(2) = 18."
			],
			"mathematica": [
				"u[n_] := Table[1, {k, 1, n}]; t[n_] := Join[u[n], {Sqrt[2]}, {{1}}];",
				"f[n_] := FromContinuedFraction[t[n]];",
				"t = Table[MinimalPolynomial[f[n], x], {n, 0, 40}];",
				"Coefficient[t, x, 0] ; (* A266710 *)",
				"Coefficient[t, x, 1];  (* A266711 *)",
				"Coefficient[t, x, 2];  (* A266712 *)",
				"Coefficient[t, x, 3];  (* A266713 *)",
				"Coefficient[t, x, 4];  (* A266710 *)",
				"LinearRecurrence[{5,15,-15,-5,1}, {-6, 2, 18, -102, -714, -4826, -33222, -227298}, 30] (* _G. C. Greubel_, Jan 26 2018 *)"
			],
			"program": [
				"(PARI) x='x+O('x^30); Vec(2*(3 -16*x -49*x^2 +156*x^3 +237*x^4 -280*x^5 -88*x^6 +18*x^7)/(-1 +5*x +15*x^2 -15*x^3 -5*x^4 +x^5)) \\\\ _G. C. Greubel_, Jan 26 2018",
				"(MAGMA) I:=[-102, -714, -4826, -33222, -227298]; [-6, 2, 18] cat [n le 5 select I[n] else 5*Self(n-1) + 15*Self(n-2) - 15*Self(n-3) - 5*Self(n-4) + Self(n-5): n in [1..30]]; // _G. C. Greubel_, Jan 26 2018"
			],
			"xref": [
				"Cf. A265762, A266710, A266712, A266713."
			],
			"keyword": "sign,easy",
			"offset": "0,1",
			"author": "_Clark Kimberling_, Jan 09 2016",
			"references": 5,
			"revision": 10,
			"time": "2018-01-27T02:25:37-05:00",
			"created": "2016-01-09T15:29:39-05:00"
		}
	]
}