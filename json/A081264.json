{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A081264",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 81264,
			"data": "323,377,1891,3827,4181,5777,6601,6721,8149,10877,11663,13201,13981,15251,17119,17711,18407,19043,23407,25877,27323,30889,34561,34943,35207,39203,40501,50183,51841,51983,52701,53663,60377,64079,64681",
			"name": "Odd Fibonacci pseudoprimes: odd composite numbers k such that either (1) k divides Fibonacci(k-1) if k == +-1 (mod 5) or (2) k divides Fibonacci(k+1) if k == +-2 (mod 5).",
			"comment": [
				"Lehmer shows that there are an infinite number of Fibonacci pseudoprimes (FPPs). In particular, the number Fibonacci(2p) is an FPP for all primes p \u003e 5. Anderson lists over 5000 FPPs, while Jacobsen lists over 170000. The sequences A069106 and A069107 give k such that k divides Fibonacci(k-1) and k divides Fibonacci(k+1), respectively. See A141137 for even FPPs."
			],
			"reference": [
				"R. Crandall and C. Pomerance, Primes Numbers: A Computational Perspective, Springer, 2002, p. 131.",
				"P. Ribenboim, The New Book of Prime Number Records, Springer, 1995, p. 127.",
				"A. Witno, Theory of Numbers, BookSurge, North Charleston, SC; see p. 83."
			],
			"link": [
				"P. G. Anderson and Dana Jacobsen, \u003ca href=\"/A081264/b081264.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (first 5861 terms from P. G. Anderson)",
				"P. G. Anderson, \u003ca href=\"http://www.cs.rit.edu/usr/local/pub/pga/fibonacci_pp\"\u003eFibonacci pseudoprimes under 2,217,967,487 and their factors\u003c/a\u003e",
				"Dorin Andrica and Ovidiu Bagdasar, \u003ca href=\"https://doi.org/10.1007/978-3-030-51502-7\"\u003eRecurrent Sequences: Key Results, Applications, and Problems\u003c/a\u003e, Springer (2020), p. 88.",
				"Dorin Andrica and Ovidiu Bagdasar, \u003ca href=\"https://doi.org/10.3390/math9080838\"\u003eOn Generalized Lucas Pseudoprimality of Level k\u003c/a\u003e, Mathematics (2021) Vol. 9, 838.",
				"Dorin Andrica, Vlad Crişan, and Fawzi Al-Thukair, \u003ca href=\"https://dx.doi.org/10.1016/j.ajmsc.2017.06.002\"\u003eOn Fibonacci and Lucas sequences modulo a prime and primality testing\u003c/a\u003e, Arab Journal of Mathematical Sciences, 2017.",
				"Dana Jacobsen, \u003ca href=\"http://ntheory.org/pseudoprimes.html\"\u003ePseudoprime Statistics, Tables, and Data\u003c/a\u003e (includes terms through 7e12)",
				"E. Lehmer, \u003ca href=\"http://www.fq.math.ca/Scanned/2-3/lehmer.pdf\"\u003eOn the infinitude of Fibonacci pseudoprimes\u003c/a\u003e, Fibonacci Quarterly, 2, 1964, pp. 229-230.",
				"Andrzej Rotkiewicz, \u003ca href=\"http://hdl.handle.net/10338.dmlcz/120560\"\u003eArithmetic progressions formed by pseudoprimes\u003c/a\u003e, Acta Mathematica et Informatica Universitatis Ostraviensis, vol. 8 (2000), issue 1, pp. 61-74.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/FibonacciPseudoprime.html\"\u003eFibonacci Pseudoprime\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Fibonacci_pseudoprime\"\u003eFibonacci pseudoprime\u003c/a\u003e",
				"\u003ca href=\"/index/Ps#pseudoprimes\"\u003eIndex entries for sequences related to pseudoprimes\u003c/a\u003e"
			],
			"maple": [
				"filter:= proc(n) local M,r;",
				"   uses LinearAlgebra:-Modular;",
				"   if isprime(n) then return false fi;",
				"   M:= Mod(n, [[1,1],[1,0]],float[8]);",
				"   if n^2 mod 5 = 1 then r:= n-1 else r:= n+1 fi;",
				"   M:= MatrixPower(n,M,r);",
				"   M[1,2] = 0",
				"end proc:select(filter, [2*i+1 $ i=1..10^5]); # _Robert Israel_, Aug 05 2015"
			],
			"mathematica": [
				"lst={}; f0=0; f1=1; Do[f2=f1+f0; If[n\u003e1\u0026\u0026!PrimeQ[n], If[MemberQ[{1, 4}, Mod[n, 5]], If[Mod[f0, n]==0, AppendTo[lst, n]]]; If[MemberQ[{2, 3}, Mod[n, 5]], If[Mod[f2, n]==0, AppendTo[lst, n]]]]; f0=f1; f1=f2, {n, 100000}]; lst",
				"ocnQ[n_]:=CompositeQ[n]\u0026\u0026Which[Mod[n,5]==1,Divisible[Fibonacci[ n-1], n],Mod[n,5] == 4,Divisible[ Fibonacci[n-1],n],Mod[n,5]==2,Divisible[ Fibonacci[n+1],n], Mod[n,5]==3,Divisible[Fibonacci[n+1],n],True,False]; Select[Range[1,65001,2],ocnQ] (* _Harvey P. Dale_, Aug 23 2017 *)"
			],
			"program": [
				"(Perl) use ntheory \":all\"; foroddcomposites { $e = (0,-1,1,1,-1)[$_%5]; say unless $e==0 || (lucas_sequence($_, 1, -1, $_+$e))[0] } 1e10; # _Dana Jacobsen_, Aug 05 2015"
			],
			"xref": [
				"Cf. A069106, A069107."
			],
			"keyword": "nice,nonn",
			"offset": "1,1",
			"author": "_T. D. Noe_, Mar 15 2003, Jun 09 2008",
			"references": 21,
			"revision": 60,
			"time": "2021-07-17T01:43:36-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}