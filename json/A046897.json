{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A046897",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 46897,
			"data": "1,3,4,3,6,12,8,3,13,18,12,12,14,24,24,3,18,39,20,18,32,36,24,12,31,42,40,24,30,72,32,3,48,54,48,39,38,60,56,18,42,96,44,36,78,72,48,12,57,93,72,42,54,120,72,24,80,90,60,72,62,96,104,3,84,144,68,54,96,144,72",
			"name": "Sum of divisors of n that are not divisible by 4.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"The o.g.f. is (theta_3(0,x)^4 - 1)/8, see the Hardy reference, eqs. 9.2.1, 9.2.3 and 9.2.4 on p. 133 for Sum' m*u_m. Also Hardy-Wright, p. 314. See also the Somos, Jan 25 2008 formula below. - _Wolfdieter Lang_, Dec 11 2016"
			],
			"reference": [
				"J. M. Borwein, D. H. Bailey and R. Girgensohn, Experimentation in Mathematics, A K Peters, Ltd., Natick, MA, 2004. x+357 pp. See p. 194.",
				"G. H. Hardy, Ramanujan: twelve lectures on subjects suggested by his life and work, AMS Chelsea Publishing, Providence, Rhode Island 2002, p. 133.",
				"G. H. Hardy and E. M. Wright, An Introduction to the Theory of Numbers, Clarendon Press, Oxford, Fifth edition, 1979, p. 314.",
				"P. A. MacMahon, Combinatory Analysis, Cambridge Univ. Press, London and New York, Vol. 1, 1915 and Vol. 2, 1916; see vol. 2, p 31, Article 273.",
				"C. J. Moreno and S. S. Wagstaff, Jr., Sums of Squares of Integers, Chapman \u0026 Hall, 2006."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A046897/b046897.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e."
			],
			"formula": [
				"a(n) = (-1)^(n+1)*Sum_{d divides n} (-1)^(n/d+d)*d. Multiplicative with a(2^e) = 3, a(p^e) = (p^(e+1)-1)/(p-1) for an odd prime p. - _Vladeta Jovovic_, Sep 10 2002 [For a proof of the multiplicative property, see for example Moreno and Wagstaff, p. 33. - _N. J. A. Sloane_, Nov 09 2016]",
				"G.f.: Sum_{k\u003e0} x^k/(1+(-x)^k)^2, or Sum_{k\u003e0} k*x^k/(1+(-x)^k). - _Vladeta Jovovic_, Dec 16 2002",
				"Expansion of (1 - phi(q)^4) / 8 in powers of q where phi() is a Ramanujan theta function. - _Michael Somos_, Jan 25 2008",
				"Equals inverse Mobius transform of A190621. - _Gary W. Adamson_, Jul 03 2008",
				"A000118(n) = 8*a(n) for all n\u003e0.",
				"Dirichlet g.f.: (1 - 4^(1-s)) * zeta(s) * zeta(s-1). - _Michael Somos_, Oct 21 2015",
				"L.g.f.: log(Product_{k\u003e=1} (1 - x^(4*k))/(1 - x^k)) = Sum_{n\u003e=1} a(n)*x^n/n. - _Ilya Gutkovskiy_, Mar 14 2018",
				"From _Peter Bala_, Dec 19 2021: (Start)",
				"Logarithmic g.f.: Sum_{n \u003e= 1} a(n)*x^n/n = Sum_{n \u003e= 1} x^n*(1 + x^n + x^(2*n))/( n*(1 - x^(4*n)) )",
				"G.f.: Sum_{n \u003e= 1} x^n*(x^(6*n) + 2*x^(5*n) + 3*x^(4*n) + 3*x^(2*n) + 2*x^n + 1)/(1 - x^(4*n))^2. (End)"
			],
			"example": [
				"G.f. = q + 3*q^2 + 4*q^3 + 3*q^4 + 6*q^5 + 12*q^6 + 8*q^7 + 3*q^8 + 13*q^9 + ..."
			],
			"maple": [
				"A046897 := proc(n) if n mod 4 = 0 then numtheory[sigma](n)-4*numtheory[sigma](n/4) ; else numtheory[sigma](n) ; end if; end proc: # _R. J. Mathar_, Mar 23 2011"
			],
			"mathematica": [
				"a[n_] := Sum[ Boole[ !Divisible[d, 4]]*d, {d, Divisors[n]}]; Table[ a[n], {n, 1, 71}] (* _Jean-François Alcover_, Dec 12 2011 *)",
				"DivisorSum[#1, # \u0026, Mod[#, 4] != 0 \u0026] \u0026 /@ Range[71] (* _Jayanta Basu_, Jun 30 2013 *)",
				"a[ n_] := SeriesCoefficient[ (EllipticTheta[ 3, 0, q]^4 - 1) / 8, {q, 0, n}]; (* _Michael Somos_, Dec 30 2014 *)",
				"f[2, e_] := 3; f[p_, e_] := (p^(e+1)-1)/(p-1); a[1] = 1; a[n_] := Times @@ f @@@ FactorInteger[n]; Array[a, 100] (* _Amiram Eldar_, Sep 15 2020 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, 0, sumdiv(n, d, if(d%4, d)))};",
				"(MAGMA) A := Basis( ModularForms( Gamma0(4), 2), 72); B\u003cq\u003e := (A[1] - 1)/8 + A[2]; B; /* _Michael Somos_, Dec 30 2014 */",
				"(Haskell)",
				"a046897 1 = 1",
				"a046897 n = product $ zipWith",
				"            (\\p e -\u003e if p == 2 then 3 else div (p ^ (e + 1) - 1) (p - 1))",
				"            (a027748_row n) (a124010_row n)",
				"-- _Reinhard Zumkeller_, Aug 12 2015"
			],
			"xref": [
				"Cf. A000203, A000118, A051731, A069733, A027748, A124010, A190621."
			],
			"keyword": "nonn,mult,changed",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"references": 36,
			"revision": 59,
			"time": "2022-01-05T13:53:01-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}