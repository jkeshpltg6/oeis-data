{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A335333",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 335333,
			"data": "1,1,1,1,3,1,1,5,13,1,1,7,37,63,1,1,9,73,305,321,1,1,11,121,847,2641,1683,1,1,13,181,1809,10321,23525,8989,1,1,15,253,3311,28401,129367,213445,48639,1,1,17,337,5473,63601,458649,1651609,1961825,265729,1",
			"name": "Square array T(n,k), n \u003e= 0, k \u003e= 0, read by antidiagonals, where column k is the expansion of 1/sqrt(1 - 2*(2*k+1)*x + x^2).",
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A335333/b335333.txt\"\u003eAntidiagonals n = 0..139, flattened\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/LegendrePolynomial.html\"\u003eLegendre Polynomial\u003c/a\u003e."
			],
			"formula": [
				"T(n,k) is the coefficient of x^n in the expansion of (1 + (2*k+1)*x + k*(k+1)*x^2)^n.",
				"T(n,k) = Sum_{j=0..n} k^j * (k+1)^(n-j) * binomial(n,j)^2.",
				"T(n,k) = Sum_{j=0..n} k^j * binomial(n,j) * binomial(n+j,j).",
				"n * T(n,k) = (2*k+1) * (2*n-1) * T(n-1,k) - (n-1) * T(n-2,k).",
				"T(n,k) = P_n(2*k+1), where P_n is n-th Legendre polynomial."
			],
			"example": [
				"Square array begins:",
				"  1,    1,     1,      1,      1,       1, ...",
				"  1,    3,     5,      7,      9,      11, ...",
				"  1,   13,    37,     73,    121,     181, ...",
				"  1,   63,   305,    847,   1809,    3311, ...",
				"  1,  321,  2641,  10321,  28401,   63601, ...",
				"  1, 1683, 23525, 129367, 458649, 1256651, ..."
			],
			"mathematica": [
				"T[n_, k_] := LegendreP[n, 2*k + 1]; Table[T[k, n - k], {n, 0, 9}, {k, 0, n}] // Flatten (* _Amiram Eldar_, May 03 2021 *)"
			],
			"program": [
				"(PARI) {T(n, k) = pollegendre(n, 2*k+1)}"
			],
			"xref": [
				"Columns k=0..4 give A000012, A001850, A006442, A084768, A084769.",
				"Rows n=0..6 give A000012, A005408, A003154(n+1), A160674, A144124, A335338, A144126.",
				"Main diagonal gives A331656.",
				"T(n,n-1) gives A331657.",
				"Cf. A307883, A307884."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Seiichi Manyama_, Jun 02 2020",
			"references": 2,
			"revision": 37,
			"time": "2021-05-03T02:13:21-04:00",
			"created": "2020-06-02T08:50:02-04:00"
		}
	]
}