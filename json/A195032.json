{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A195032",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 195032,
			"data": "0,5,17,27,51,66,102,122,170,195,255,285,357,392,476,516,612,657,765,815,935,990,1122,1182,1326,1391,1547,1617,1785,1860,2040,2120,2312,2397,2601,2691,2907,3002,3230,3330,3570,3675,3927,4037,4301,4416,4692",
			"name": "Vertex number of a square spiral in which the length of the first two edges are the legs of the primitive Pythagorean triple [5, 12, 13]. The edges of the spiral have length A195031.",
			"comment": [
				"Zero together with partial sums of A195031.",
				"The spiral contains infinitely many Pythagorean triples in which the hypotenuses on the main diagonal are the positives multiples of 13 (cf. A008595). The vertices on the main diagonal are the numbers A195037 = (5+12)*A000217 = 17*A000217, where both 5 and 12 are the first two edges in the spiral. The distance \"a\" between nearest edges that are perpendicular to the initial edge of the spiral is 5, while the distance \"b\" between nearest edges that are parallel to the initial edge is 12, so the distance \"c\" between nearest vertices on the same axis is 13 because from the Pythagorean theorem we can write c = (a^2 + b^2)^(1/2) = sqrt(5^2 + 12^2) = sqrt(25 + 144) = sqrt(169) = 13. - _Omar E. Pol_, Oct 12 2011"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A195032/b195032.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Ron Knott, \u003ca href=\"http://www.maths.surrey.ac.uk/hosted-sites/R.Knott/Pythag/pythag.html#uadgen\"\u003ePythagorean triangles and Triples\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PythagoreanTriple.html\"\u003ePythagorean Triple\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,2,-2,-1,1)."
			],
			"formula": [
				"From _Bruno Berselli_, Oct 13 2011:  (Start)",
				"G.f.: x*(5 + 12*x)/((1 + x)^2*(1 - x)^3).",
				"a(n) = (1/2)*((2*n + (-1)^n + 3)/4)*((34*n - 3*(-1)^n+3)/4) = (2*n*(17*n + 27) + (14*n - 3)*(-1)^n + 3)/16.",
				"a(n) = a(n-1) + 2*a(n-2) - 2*a(n-3) - a(n-4) + a(n-5). (End)",
				"E.g.f.: (1/16)*((3 + 88*x + 34*x^2)*exp(x) - (3 + 14*x)*exp(-x)). - _Franck Maminirina Ramaharo_, Nov 23 2018"
			],
			"mathematica": [
				"a[n_] := (2 n (17 n + 27) + (14 n - 3)*(-1)^n + 3)/16; Array[a, 50, 0] (* _Amiram Eldar_, Nov 23 2018 *)"
			],
			"program": [
				"(MAGMA) [(2*n*(17*n+27)+(14*n-3)*(-1)^n+3)/16: n in [0..50]]; // _Vincenzo Librandi_, Oct 14 2011",
				"(PARI) vector(50, n, n--; (2*n*(17*n+27)+(14*n-3)*(-1)^n+3)/16) \\\\ _G. C. Greubel_, Nov 23 2018",
				"(Sage) [(2*n*(17*n+27)+(14*n-3)*(-1)^n+3)/16 for n in range(50)] # _G. C. Greubel_, Nov 23 2018"
			],
			"xref": [
				"Cf. A195020, A195031, A195034, A195036, A195037."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Omar E. Pol_, Sep 12 2011",
			"references": 6,
			"revision": 39,
			"time": "2018-11-23T03:30:05-05:00",
			"created": "2011-10-12T16:49:33-04:00"
		}
	]
}