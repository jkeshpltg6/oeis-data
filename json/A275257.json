{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A275257",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 275257,
			"data": "1,2,1,3,1,1,4,2,2,1,5,2,2,1,1,6,3,3,2,2,1,7,3,4,2,3,1,1,8,4,4,3,4,1,2,1,9,4,5,3,4,1,3,1,1,10,5,6,4,5,2,4,2,2,1,11,5,6,4,6,2,5,2,2,1,1,12,6,7,5,7,3,6,3,3,2,2,1,13,6",
			"name": "Array read by upwards antidiagonals: LegendrePhi phi(x,n), x,n \u003e=1.",
			"link": [
				"Peter Kagey, \u003ca href=\"/A275257/b275257.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"L. Toth, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL12/Toth2/toth5.html\"\u003eOn the Bi-Unitary Analogues of Euler's Arithmetical Function and the Gcd-Sum Function\u003c/a\u003e, JIS 12 (2009) 09.5.2, function phi(x,n)."
			],
			"formula": [
				"phi(x,n) = Sum_{k=1..x} A054431(k,n).",
				"phi(n,n) = A000010(n)."
			],
			"example": [
				"Upper left corner of array begins",
				"   1 1 1 1 1 1 1 1 1 1 ...",
				"   2 1 2 1 2 1 2 1 2 1 ...",
				"   3 2 2 2 3 1 3 2 2 2 ...",
				"   4 2 3 2 4 1 4 2 3 2 ...",
				"   5 3 4 3 4 2 5 3 4 2 ...",
				"   6 3 4 3 5 2 6 3 4 2 ...",
				"   7 4 5 4 6 3 6 4 5 3 ...",
				"   8 4 6 4 7 3 7 4 6 3 ...",
				"   9 5 6 5 8 3 8 5 6 4 ...",
				"  10 5 7 5 8 3 9 5 7 4 ..."
			],
			"maple": [
				"A275257 := proc(x,n)",
				"    local a,k ;",
				"    a :=0 ;",
				"    for k from 1 to x do",
				"        if igcd(k,n) = 1 then",
				"            a := a+1 ;",
				"        end if;",
				"    end do:",
				"    a ;",
				"end proc:",
				"seq(seq(A275257(d-n,n),n=1..d-1),d=2..15) ;"
			],
			"mathematica": [
				"With[{nn = 14}, Table[#[[k, n - k + 1]], {n, nn - 1}, {k, n}] \u0026@ Map[Accumulate, Table[Boole@ CoprimeQ[k, n], {n, nn}, {k, nn - n}]]] // Flatten (* _Michael De Vlieger_, Jan 09 2018 *)"
			],
			"program": [
				"(Ruby)",
				"def a(x, n); (1..x).count { |k| k.gcd(n) == 1 } end",
				"# _Peter Kagey_, Jan 08 2018"
			],
			"xref": [
				"Partial sums of A054431. Cf. A078401 (upper right triangle)."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "1,2",
			"author": "_R. J. Mathar_, Jul 21 2016",
			"references": 3,
			"revision": 20,
			"time": "2018-01-10T14:22:53-05:00",
			"created": "2016-07-21T13:59:00-04:00"
		}
	]
}