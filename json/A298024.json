{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A298024",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 298024,
			"data": "1,4,10,14,18,24,28,32,38,42,46,52,56,60,66,70,74,80,84,88,94,98,102,108,112,116,122,126,130,136,140,144,150,154,158,164,168,172,178,182,186,192,196,200,206,210,214,220,224,228,234,238,242,248,252,256,262",
			"name": "G.f.: (x^4+3*x^3+6*x^2+3*x+1)/((1-x)*(1-x^3)).",
			"comment": [
				"Coordination sequence for Dual(3^3.4^2) tiling with respect to a tetravalent node. This tiling is also called the prismatic pentagonal tiling, or the cem-d net. It is one of the 11 Laves tilings. (The identification of this coordination sequence with the g.f. in the definition was first conjectured by _Colin Barker_, Jan 22 2018.)",
				"Also, coordination sequence for a tetravalent node in the \"krl\" 2-D tiling (or net).",
				"Both of these identifications are easily established using the \"coloring book\" method - see the Goodman-Strauss \u0026 Sloane link.",
				"For n\u003e0, this is twice A047386 (numbers congruent to 0 or +-2 mod 7)."
			],
			"reference": [
				"Branko Grünbaum and G. C. Shephard, Tilings and Patterns. W. H. Freeman, New York, 1987. See Table 2.2.1, page 66, 3rd row, second tiling. (For the krl tiling.)",
				"B. Gruenbaum and G. C. Shephard, Tilings and Patterns, W. H. Freeman, New York, 1987. See p. 96. (For the Dual(3^3.4^2) tiling.)"
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A298024/b298024.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Chung, Ping Ngai, Miguel A. Fernandez, Yifei Li, Michael Mara, Frank Morgan, Isamar Rosa Plata, Niralee Shah, Luis Sordo Vieira, and Elena Wikner. \"Isoperimetric pentagonal tilings.\" Notices of the AMS 59, no. 5 (2012), pp. 632-640. See Fig. 1 (right).",
				"Brian Galebach, \u003ca href=\"http://probabilitysports.com/tilings.html\"\u003eCollection of n-Uniform Tilings\u003c/a\u003e. See Number 4 from the list of 20 2-uniform tilings.",
				"Brian Galebach, \u003ca href=\"/A250120/a250120.html\"\u003ek-uniform tilings (k \u003c= 6) and their A-numbers\u003c/a\u003e",
				"Chaim Goodman-Strauss and N. J. A. Sloane, \u003ca href=\"https://doi.org/10.1107/S2053273318014481\"\u003eA Coloring Book Approach to Finding Coordination Sequences\u003c/a\u003e, Acta Cryst. A75 (2019), 121-134, also \u003ca href=\"http://NeilSloane.com/doc/Cairo_final.pdf\"\u003eon NJAS's home page\u003c/a\u003e. Also \u003ca href=\"http://arxiv.org/abs/1803.08530\"\u003earXiv:1803.08530\u003c/a\u003e.",
				"Tom Karzes, \u003ca href=\"/A250122/a250122.html\"\u003eTiling Coordination Sequences\u003c/a\u003e",
				"Frank Morgan, \u003ca href=\"https://www.youtube.com/watch?v=PpUx0nnWfKQ\"\u003eOptimal Pentagonal Tilings\u003c/a\u003e, Video, May 2021 [Mentions this tiling",
				"Reticular Chemistry Structure Resource (RCSR), \u003ca href=\"http://rcsr.net/layers/cem-d\"\u003eThe cem-d tiling (or net)\u003c/a\u003e",
				"Reticular Chemistry Structure Resource (RCSR), \u003ca href=\"http://rcsr.net/layers/krl\"\u003eThe krl tiling (or net)\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A298024/a298024.png\"\u003eIllustration of initial terms\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A298024/a298024.gp.txt\"\u003ePARI program for A298024\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/A298024/a298024.pdf\"\u003eIllustration of initial terms\u003c/a\u003e [1 (black), 4 (black), 10 (black), 14 (red)]",
				"N. J. A. Sloane, \u003ca href=\"/A296368/a296368_2.png\"\u003eOverview of coordination sequences of Laves tilings\u003c/a\u003e [Fig. 2.7.1 of Grünbaum-Shephard 1987 with A-numbers added and in some cases the name in the RCSR database]",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,0,1,-1)."
			],
			"formula": [
				"a(n) = a(n-1) + a(n-3) - a(n-4) for n\u003e4. (Conjectured, correctly, by _Colin Barker_, Jan 22 2018.)"
			],
			"mathematica": [
				"CoefficientList[Series[(x^4+3x^3+6x^2+3x+1)/((1-x)(1-x^3)),{x,0,60}],x] (* or *) LinearRecurrence[{1,0,1,-1},{1,4,10,14,18},80] (* _Harvey P. Dale_, Oct 03 2018 *)"
			],
			"program": [
				"(PARI) See Links section."
			],
			"xref": [
				"Cf. A298024.",
				"See A298025 for partial sums, A298022 for a trivalent node.",
				"See also A047486.",
				"List of coordination sequences for Laves tilings (or duals of uniform planar nets): [3,3,3,3,3.3] = A008486; [3.3.3.3.6] = A298014, A298015, A298016; [3.3.3.4.4] = A298022, A298024; [3.3.4.3.4] = A008574, A296368; [3.6.3.6] = A298026, A298028; [3.4.6.4] = A298029, A298031, A298033; [3.12.12] = A019557, A298035; [4.4.4.4] = A008574; [4.6.12] = A298036, A298038, A298040; [4.8.8] = A022144, A234275; [6.6.6] = A008458.",
				"Coordination sequences for the 20 2-uniform tilings in the order in which they appear in the Galebach catalog, together with their names in the RCSR database (two sequences per tiling): #1 krt A265035, A265036; #2 cph A301287, A301289; #3 krm A301291, A301293; #4 krl A301298, A298024; #5 krq A301299, A301301; #6 krs A301674, A301676; #7 krr A301670, A301672; #8 krk A301291, A301293; #9 krn A301678, A301680; #10 krg A301682, A301684; #11 bew A008574, A296910; #12 krh A301686, A301688; #13 krf A301690, A301692; #14 krd A301694, A219529; #15 krc A301708, A301710; #16 usm A301712, A301714; #17 krj A219529, A301697; #18 kre A301716, A301718; #19 krb A301720, A301722; #20 kra A301724, A301726."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Jan 21 2018",
			"ext": [
				"More terms from _Rémy Sigrist_, Jan 21 2018",
				"Entry revised by _N. J. A. Sloane_, Mar 25 2018"
			],
			"references": 57,
			"revision": 59,
			"time": "2021-09-18T16:20:14-04:00",
			"created": "2018-01-21T07:31:41-05:00"
		}
	]
}