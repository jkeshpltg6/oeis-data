{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A237265",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 237265,
			"data": "1,1,2,2,1,1,2,3,2,1,3,3,1,2,1,2,3,4,2,1,3,4,3,1,2,4,4,1,2,3,1,2,3,4,5,2,1,3,4,5,3,1,2,4,5,4,1,2,3,5,5,1,2,3,4,1,2,3,4,5,6,2,1,3,4,5,6,3,1,2,4,5,6,4,1,2,3,5,6,5,1,2,3,4,6,6,1,2,3,4,5",
			"name": "Irregular table: n X n matrices (n=1,2,3,...), read by rows filled with numbers 1..n, with k moved to the front in the k-th row.",
			"comment": [
				"Cases of enumeration in ascending order for the first m positive integers when one of them, j, is previously excluded (m=1,2,3,..., 1 \u003c= j \u003c= m).",
				"Table of the k initial permutations, one per block, when all the k! permutations in lexicographic ascending order are split uniformly into k blocks. Such table read by rows for k=1,2,3,... .",
				"These permutations might be considered the initial inputs for a parallel/distributed variant of the Narayana Pandita's algorithm. Such variant would deliver to each thread/core/host one or more of the mentioned inputs, then the remaining permutations can be obtained with (k-1)!-1 executions of the classic Narayana Pandita's algorithm for the next permutation in lexical order.",
				"The terms of A237450 give the positions of rows of this table among the rows of A030298. The finite n X n square matrices converge towards the infinite square array A237447. Please see further comments there. - _Antti Karttunen_, Feb 10 2014",
				"Alternative way to express this is that each row k=1..n of each n X n matrix contains the lexicographically earliest n-letter permutation beginning with number k, or equally, that each of the n X n square matrices contain in their n rows those n-letter permutations of the symmetric group S_n that correspond to the inverses of cycles (1), (1 2), (1 2 3), ..., (1 2 ... n). Please see the Example section. - _Antti Karttunen_, Feb 12 2014"
			],
			"reference": [
				"Donald Knuth, The Art of Computer Programming, Volume 4: \"Generating All Tuples and Permutations\" Fascicle 2, first printing. Addison-Wesley, 2005. ISBN 0-201-85393-0."
			],
			"link": [
				"R. J. Cano, \u003ca href=\"/A237265/b237265.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"R. J. Cano, \u003ca href=\"http://oeis.org/w/images/d/db/Curiosity_3dot0.pdf\"\u003eAdditional information.\u003c/a\u003e",
				"R. J. Cano, \u003ca href=\"http://oeis.org/w/images/2/2d/Illustration.GIF\"\u003eIllustration of the recursive algorithm defining this sequence\u003c/a\u003e.",
				"R. J. Cano, \u003ca href=\"https://oeis.org/wiki/Additional_information_about_A237265#Recursive_and_iterative_algorithms\"\u003eRecursive and iterative algorithms for A237265\u003c/a\u003e, OEIS wiki."
			],
			"formula": [
				"a(n) = A237447(1 + ((1/2) * ((col+row)^2 + col + 3*row)))[where col = A237451(n) and row = A237452(n)] = A237447bi(A237452(n),A237451(n)) [where A237447bi(row,col) is square array A237447 considered as a bivariate function]. - _Antti Karttunen_, Feb 10-12 2014"
			],
			"example": [
				"By excluding 2, the natural numbers between 1 and 4 are 1,3,4, then the second row of the corresponding matrix must be [2,1,3,4] and a(22)=4; that is, when reading by rows, a(22) must be placed at the 4th matrix since 22 is greater than the sum of elements there in the preceding matrices and it is smaller than the next of such sums: 14 = (1 + 2^2 + 3^2) \u003c= (22) \u003c= (1 + 2^2 + 3^2 + 4^2) = 30. Therefore 14 is subtracted from 22 leaving 8. This means that a(22) is the 8th element in the fourth matrix read by rows, so a(22) = A(4)[2,4] (see formula).",
				"The irregular table starts consists of successively larger squares (beginning with a 1 X 1 square {1}), where each larger (n+1) X (n+1) square contains the previous n X n square in its upper left corner, with the first n rows followed by n+1, and the last row consisting of (n+1) followed by the first row of the previous n X n square (i.e., terms 1, 2, ..., n):",
				"Permutation  In cycle notation.  Inverse in cycle notation",
				"1;           ( )                 ( )    [Note: ( ) stands for identity]",
				"1,2;         ( )                 ( )",
				"2,1;         (1 2)               (1 2)",
				"1,2,3;       ( )                 ( )",
				"2,1,3;       (1 2)               (1 2)",
				"3,1,2;       (1 3 2)             (1 2 3)",
				"1,2,3,4;     ( )                 ( )",
				"2,1,3,4;     (1 2)               (1 2)",
				"3,1,2,4;     (1 3 2)             (1 2 3)",
				"4,1,2,3;     (1 4 3 2)           (1 2 3 4)",
				"1,2,3,4,5;   ( )                 ( )",
				"2,1,3,4,5;   (1 2)               (1 2)",
				"3,1,2,4,5;   (1 3 2)             (1 2 3)",
				"4,1,2,3,5;   (1 4 3 2)           (1 2 3 4)",
				"5,1,2,3,4;   (1 5 4 3 2)         (1 2 3 4 5)",
				"...",
				"The table starts with 1 since the definition must be read in the mathematical sense of its statement. If we have N elements and one of them must be excluded, there are no elements available to exclude when N=1."
			],
			"program": [
				"(Scheme)",
				";; Implemented as a recurrence: (uses memoization macro definec from Antti Karttunen's IntSeq-library)",
				"(definec (A237265 n) (cond ((zero? (A237452 (+ n (A074279 n)))) (+ (A237451 n) (if (zero? (A237451 n)) (A074279 n) 0))) ((zero? (A237451 (+ n 1))) (A074279 n)) (else (A237265 (+ 1 (A000330 (- (A074279 n) 2)) (* (- (A074279 n) 1) (A237452 n)) (A237451 n))))))",
				";; Version which uses the array A237447:",
				"(define (A237265 n) (let ((col (A237451 n)) (row (A237452 n))) (A237447 (+ 1 (/ (+ (expt (+ col row) 2) col (* 3 row)) 2)))))",
				";; _Antti Karttunen_, Feb 08-10 2014",
				"(PARI) a(n,k=0)=if(k,if(k\u003e1,k-(k\u003c=n),n),a(A238013(n),A121997(n))) \\\\ _M. F. Hasler_, Feb 16 2014",
				"(PARI)",
				"A237265_mth_matrix(m,zeroless=1)=my(c:vec=numtoperm(m,0)-!zeroless*vector(m,i,1), M:vec=matrix(m,m,i,j,0)); for(j=1,m, M[j,]=concat([j-!zeroless],concat(c[1..j-1],c[j+1..m]))); M",
				"a(n)=my(p,q,r,s); while(s\u003cn, r=s; s+=(p++)^2); q=divrem(n-r,p); if(!q[2],q[2]=p,q[1]++); A237265_mth_matrix(p,1)[q[1],q[2]] \\\\ _R. J. Cano_, May 08 2017"
			],
			"xref": [
				"Cf. A237447, A030298, A000330, A074279, A237450, A237451, A237452."
			],
			"keyword": "nonn,easy,tabf",
			"offset": "1,3",
			"author": "_R. J. Cano_, Feb 09 2014",
			"ext": [
				"Name changed and more terms added by _Antti Karttunen_, Feb 10 2014",
				"Further edits by _M. F. Hasler_, Mar 09 2014"
			],
			"references": 7,
			"revision": 73,
			"time": "2019-04-22T03:07:54-04:00",
			"created": "2014-02-15T19:23:48-05:00"
		}
	]
}