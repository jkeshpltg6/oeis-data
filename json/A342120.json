{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A342120",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 342120,
			"data": "1,1,0,1,1,0,1,2,2,0,1,3,6,3,0,1,4,12,16,5,0,1,5,20,45,44,8,0,1,6,30,96,171,120,13,0,1,7,42,175,464,648,328,21,0,1,8,56,288,1025,2240,2457,896,34,0,1,9,72,441,1980,6000,10816,9315,2448,55,0",
			"name": "Square array T(n,k), n\u003e=0, k\u003e=0, read by antidiagonals, where column k is the expansion of g.f. 1/(1 - k*x - k*x^2).",
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A342120/b342120.txt\"\u003eAntidiagonals n = 0..139, flattened\u003c/a\u003e",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e"
			],
			"formula": [
				"T(0,k) = 1, T(1,k) = k and T(n,k) = k*(T(n-1,k) + T(n-2,k)) for n \u003e 1.",
				"T(n,k) = Sum_{j=0..floor(n/2)} k^(n-j) * binomial(n-j,j) = Sum_{j=0..n} k^j * binomial(j,n-j).",
				"T(n,k) = (-sqrt(k)*i)^n * S(n, sqrt(k)*i) with S(n, x) := U(n, x/2), Chebyshev's polynomials of the 2nd kind."
			],
			"example": [
				"Square array begins:",
				"  1, 1,   1,   1,    1,    1, ...",
				"  0, 1,   2,   3,    4,    5, ...",
				"  0, 2,   6,  12,   20,   30, ...",
				"  0, 3,  16,  45,   96,  175, ...",
				"  0, 5,  44, 171,  464, 1025, ...",
				"  0, 8, 120, 648, 2240, 6000, ..."
			],
			"maple": [
				"T:= (n, k)-\u003e (\u003c\u003c0|1\u003e, \u003ck|k\u003e\u003e^(n+1))[1, 2]:",
				"seq(seq(T(n, d-n), n=0..d), d=0..12);  # _Alois P. Heinz_, Mar 01 2021"
			],
			"mathematica": [
				"T[n_, k_] := Sum[If[k == j == 0, 1, k^j] * Binomial[j, n - j], {j, 0, n}]; Table[T[k, n - k], {n, 0, 10}, {k, 0, n}] // Flatten (* _Amiram Eldar_, Apr 28 2021 *)"
			],
			"program": [
				"(PARI) T(n, k) = sum(j=0, n\\2, k^(n-j)*binomial(n-j, j));",
				"(PARI) T(n, k) = sum(j=0, n, k^j*binomial(j, n-j));",
				"(PARI) T(n, k) = round((-sqrt(k)*I)^n*polchebyshev(n, 2, sqrt(k)*I/2));"
			],
			"xref": [
				"Columns 0..10 give A000007, A000045(n+1), A002605(n+1), A030195(n+1), A057087, A057088, A057089, A057090, A057091, A057092, A057093.",
				"Rows 0..2 give A000012, A001477, A002378.",
				"Main diagonal gives A109516(n+1).",
				"Cf. A342129, A342133, A342134."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Seiichi Manyama_, Feb 28 2021",
			"references": 4,
			"revision": 37,
			"time": "2021-04-28T02:05:48-04:00",
			"created": "2021-03-01T09:45:29-05:00"
		}
	]
}