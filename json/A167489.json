{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A167489",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 167489,
			"data": "1,1,1,2,2,1,2,3,3,2,1,2,4,2,3,4,4,3,2,4,2,1,2,3,6,4,2,4,6,3,4,5,5,4,3,6,4,2,4,6,3,2,1,2,4,2,3,4,8,6,4,8,4,2,4,6,9,6,3,6,8,4,5,6,6,5,4,8,6,3,6,9,6,4,2,4,8,4,6,8,4,3,2,4,2,1,2,3,6,4,2,4,6,3,4,5,10,8,6,12,8,4,8",
			"name": "Product of run lengths in binary representation of n.",
			"link": [
				"Antti Karttunen, \u003ca href=\"/A167489/b167489.txt\"\u003eTable of n, a(n) for n = 0..8192\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A227349(n) * A227350(n) = A227355(A227352(2n+1)). - _Antti Karttunen_, Jul 25 2013",
				"a(n) = A284558(n) * A284559(n) = A284582(n) * A284583(n). - _Antti Karttunen_, Apr 16 2017"
			],
			"example": [
				"a(56) = 9, because 56 in binary is written 111000 giving the run lengths 3,3 and 3x3 = 9.",
				"a(99) = 12, because 99 in binary is written 1100011 giving the run lengths 2,3,2, and 2x3x2 = 12."
			],
			"mathematica": [
				"Table[ Times @@ (Length /@ Split[IntegerDigits[n, 2]]), {n, 0, 100}](* _Olivier Gérard_, Jul 05 2013 *)"
			],
			"program": [
				"(Scheme)",
				"(define (A167489 n) (apply * (binexp-\u003eruncount1list n)))",
				"(define (binexp-\u003eruncount1list n) (if (zero? n) (list) (let loop ((n n) (rc (list)) (count 0) (prev-bit (modulo n 2))) (if (zero? n) (cons count rc) (if (eq? (modulo n 2) prev-bit) (loop (floor-\u003eexact (/ n 2)) rc (1+ count) (modulo n 2)) (loop (floor-\u003eexact (/ n 2)) (cons count rc) 1 (modulo n 2)))))))",
				";; _Antti Karttunen_, Jul 05 2013",
				"(Haskell)",
				"import Data.List (group)",
				"a167489 = product . map length . group . a030308_row",
				"-- _Reinhard Zumkeller_, Jul 05 2013",
				"(Python)",
				"def A167489(n):",
				"  '''Product of run lengths in binary representation of n.'''",
				"  p = 1",
				"  b = n%2",
				"  i = 0",
				"  while (n != 0):",
				"    n \u003e\u003e= 1",
				"    i += 1",
				"    if ((n%2) != b):",
				"      p *= i",
				"      i = 0",
				"      b = n%2",
				"  return(p)",
				"# _Antti Karttunen_, Jul 24 2013 (Cf. Python program for A227184).",
				"(PARI) a(n) = {my(p=1, b=n%2, i=0); while(n!=0, n=n\u003e\u003e1; i=i+1; if((n%2)!=b, p=p*i; i=0; b=n%2)); p} \\\\ _Indranil Ghosh_, Apr 17 2017, after the Python Program by _Antti Karttunen_"
			],
			"xref": [
				"Row products of A101211 and A227736 (for n \u003e 0).",
				"Cf. A167490 (smallest number with binary run length product = n).",
				"Cf. A167491 (members of A167490 sorted in ascending order).",
				"Cf. A227355, A227184, A227190.",
				"Cf. A227349, A227350, A227352, A246588, A284558, A284559, A284582, A284583.",
				"Differs from similar A284579 for the first time at n=56, where a(56) = 9, while A284579(56) = 5."
			],
			"keyword": "nonn,base",
			"offset": "0,4",
			"author": "_Andrew Weimholt_, Nov 05 2009",
			"references": 18,
			"revision": 36,
			"time": "2017-04-17T09:01:49-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}