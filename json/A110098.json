{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A110098",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 110098,
			"data": "1,2,1,6,6,1,22,30,10,1,90,146,70,14,1,394,714,430,126,18,1,1806,3534,2490,938,198,22,1,8558,17718,14002,6314,1734,286,26,1,41586,89898,77550,40054,13338,2882,390,30,1,206098,461010,426150,244790,94554",
			"name": "Triangle read by rows: T(n,k) (0 \u003c= k \u003c= n) is the number of Delannoy paths of length n, having k return steps to the line y = x from the line y = x+1 (i.e., E steps from the line y=x+1 to the line y = x).",
			"comment": [
				"A Delannoy path of length n is a path from (0,0) to (n,n), consisting of steps E=(1,0), N=(0,1) and D=(1,1).",
				"The row sums are the central Delannoy numbers (A001850).",
				"Column 0 yields the large Schroeder numbers (A006318).",
				"Column 1 yields A006320.",
				"Column k has g.f. z^k*R^(2*k+1), where R = 1 + z*R + z*R^2 is the g.f. of the large Schroeder numbers (A006318)."
			],
			"link": [
				"T.-X. He, L. W. Shapiro, \u003ca href=\"http://dx.doi.org/10.1016/j.laa.2017.06.025\"\u003eFuss-Catalan matrices, their weighted sums, and stabilizer subgroups of the Riordan group\u003c/a\u003e, Lin. Alg. Applic. 532 (2017) 25-41, example p 37.",
				"Robert A. Sulanke, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL6/Sulanke/delannoy.html\"\u003eObjects Counted by the Central Delannoy Numbers\u003c/a\u003e, Journal of Integer Sequences, Volume 6, 2003, Article 03.1.5."
			],
			"formula": [
				"T(n,k) = ((2*k+1)/(n-k))*Sum_{j=0..n-k} binomial(n-k, j)*binomial(n+k+j, n-k-1) for k \u003c n;",
				"T(n,n) = 1;",
				"T(n,k) = 0 for k \u003e n.",
				"G.f.: R/(1 - t*z*R^2), where R = 1 + z*R + z*R^2 is the g.f. of the large Schroeder numbers (A006318).",
				"Sum_{k=0..n} k*T(n,k) = A110099(n).",
				"T(n,k) = A033877(n-k+1, n+k+1). - _Johannes W. Meijer_, Sep 05 2013"
			],
			"example": [
				"T(2, 1) = 6 because we have DN(E), N(E)D, N(E)EN, ND(E), NNE(E) and ENN(E) (the return E steps are shown between parentheses).",
				"Triangle begins:",
				"   1;",
				"   2,   1;",
				"   6,   6,   1;",
				"  22,  30,  10,   1;",
				"  90, 146,  70,  14,   1;"
			],
			"maple": [
				"T := proc(n, k) if k=n then 1 else ((2*k+1)/(n-k))*sum(binomial(n-k,j)*binomial(n+k+j,n-k-1),j=0..n-k) fi end: for n from 0 to 10 do seq(T(n, k), k=0..n) od; # yields sequence in triangular form"
			],
			"xref": [
				"Cf. A001850, A006318, A006320, A033877, A110099, A110107."
			],
			"keyword": "nonn,tabl",
			"offset": "0,2",
			"author": "_Emeric Deutsch_, Jul 11 2005",
			"references": 2,
			"revision": 16,
			"time": "2017-07-23T10:55:33-04:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}