{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008438",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8438,
			"data": "1,4,6,8,13,12,14,24,18,20,32,24,31,40,30,32,48,48,38,56,42,44,78,48,57,72,54,72,80,60,62,104,84,68,96,72,74,124,96,80,121,84,108,120,90,112,128,120,98,156,102,104,192,108,110,152,114,144,182,144,133,168",
			"name": "Sum of divisors of 2*n + 1.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Number of ways of writing n as the sum of 4 triangular numbers.",
				"Bisection of A000203. - _Omar E. Pol_, Mar 14 2012",
				"a(n) is also the total number of parts in all partitions of 2*n + 1 into equal parts. - _Omar E. Pol_, Feb 14 2021"
			],
			"reference": [
				"B. C. Berndt, Ramanujan's Notebooks Part III, Springer-Verlag, see p. 139 Ex. (iii).",
				"J. H. Conway and N. J. A. Sloane, \"Sphere Packings, Lattices and Groups\", Springer-Verlag, p. 102.",
				"L. E. Dickson, History of the Theory of Numbers. Carnegie Institute Public. 256, Washington, DC, Vol. 1, 1919; Vol. 2, 1920; Vol. 3, 1923, see vol. 2, p. 19 eq. (6), and p. 283 eq. (8).",
				"W. Dunham, Euler: The Master of Us All, The Mathematical Association of America Inc., Washington, D.C., 1999, p. 12.",
				"H. M. Farkas, I. Kra, Cosines and triangular numbers, Rev. Roumaine Math. Pures Appl., 46 (2001), 37-43.",
				"N. J. Fine, Basic Hypergeometric Series and Applications, Amer. Math. Soc., 1988; p. 79, Eq. (32.31).",
				"N. Koblitz, Introduction to Elliptic Curves and Modular Forms, Springer-Verlag, 1984, see p. 184, Prop. 4, F(z).",
				"G. Polya, Induction and Analogy in Mathematics, vol. 1 of Mathematics and Plausible Reasoning, Princeton Univ. Press, 1954, page 92 ff."
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A008438/b008438.txt\"\u003eTable of n, a(n) for n = 0..20000\u003c/a\u003e [First 10000 terms from T. D. Noe]",
				"H. Cohen, \u003ca href=\"http://gdz.sub.uni-goettingen.de/dms/resolveppn/?PPN=GDZPPN002311860\"\u003eSums involving the values at negative integers of L-functions of quadratic characters\u003c/a\u003e, Math. Ann. 217 (1975), no. 3, 271-285. MR0382192 (52 #3080)",
				"M. D. Hirschhorn, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2004.08.045\"\u003eThe number of representations of a number by various forms\u003c/a\u003e, Discrete Mathematics 298 (2005), 205-211",
				"Masao Koike, \u003ca href=\"https://oeis.org/A004016/a004016.pdf\"\u003eModular forms on non-compact arithmetic triangle groups\u003c/a\u003e, Unpublished manuscript [Extensively annotated with OEIS A-numbers by N. J. A. Sloane, Feb 14 2021. I wrote 2005 on the first page but the internal evidence suggests 1997.]",
				"K. Ono, S. Robins and P. T. Wahl, \u003ca href=\"http://dx.doi.org/10.1007/BF01831114\"\u003eOn the representation of integers as sums of triangular numbers\u003c/a\u003e, Aequationes mathematicae, August 1995, Volume 50, Issue 1-2, pp 73-94. Theorem 3 [Legendre].",
				"H. Rosengren, \u003ca href=\"http://arXiv.org/abs/math.NT/0504272\"\u003eSums of triangular numbers from the Frobenius determinant\u003c/a\u003e, arXiv:math/0504272 [math.NT], 2005.",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Min Wang, Zhi-Hong Sun, \u003ca href=\"http://arxiv.org/abs/1511.00478\"\u003eOn the number of representations of n as a linear combination of four triangular numbers II\u003c/a\u003e, arXiv:1511.00478 [math.NT], 2015.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"K. S. Williams, \u003ca href=\"http://www.jstor.org/stable/10.4169/amer.math.monthly.120.04.329\"\u003eThe parents of Jacobi's four squares theorem are unique\u003c/a\u003e, Amer. Math. Monthly, 120 (2013), 329-345."
			],
			"formula": [
				"Expansion of q^(-1/2) * (eta(q^2)^2 / eta(q))^4 = psi(q)^4 in powers of q where psi() is a Ramanujan theta function. - _Michael Somos_, Apr 11 2004",
				"Expansion of Jacobi theta_2(q)^4 / (16*q) in powers of q^2. - _Michael Somos_, Apr 11 2004",
				"Euler transform of period 2 sequence [4, -4, 4, -4, ...]. - _Michael Somos_, Apr 11 2004",
				"a(n) = b(2*n + 1) where b() is multiplicative and b(2^e) = 0^n, b(p^e) =(p^(e+1) - 1) / (p - 1) if p\u003e2. - _Michael Somos_, Jul 07 2004",
				"Given g.f. A(x), then B(q) = q * A(q^2) satisfies 0 = f(B(q), B(q^2), B(q^4)) where f(u, v, w) = v^3 + 8*w*v^2 + 16*w^2*v - u^2*w - _Michael Somos_, Apr 08 2005",
				"Given g.f. A(x), then B(q) = q * A(q^2) satisfies 0 = f(B(q), B(q^3), B(q^9)) where f(u, v, w) = v^4 - 30*u*v^2*w + 12*u*v*w*(u + 9*w) - u*w*(u^2 + 9*w*u + 81*w^2).",
				"Given g.f. A(x), then B(q) = q * A(q^2) satisfies 0 = f(B(q), B(q^2), B(q^3), B(q^6) where f(u1, u2, u3, u6) = u2^3 + u1^2*u6 + 3*u2*u3^2 + 27*u6^3 - u1*u2*u3 - 3*u1*u3*u6 - 7*u2^2*u6 - 21*u2*u6^2. - _Michael Somos_, May 30 2005",
				"G.f.: Sum_{k\u003e=0} (2k + 1) * x^k / (1 - x^(2k + 1)).",
				"G.f.: (Product_{k\u003e0} (1 - x^k) * (1 + x^k)^2)^4. - _Michael Somos_, Apr 11 2004",
				"G.f. Sum_{k\u003e=0} a(k) * x^(2k + 1) = x( * Prod_{k\u003e0} (1 - x^(4*k))^2 / (1 - x^(2k)))^ 4 = x * (Sum_{k\u003e0} x^(k^2 - k))^4 = Sum_{k\u003e0} k * (x^k / (1 - x^k) - 3 * x^(2*k) / (1 - x^(2*k)) +2 * x^(4*k) / (1 - x^(4*k))). - _Michael Somos_, Jul 07 2004",
				"Number of solutions of 2*n + 1 = (x^2 + y^2 + z^2 + w^2) / 4 in positive odd integers. - _Michael Somos_, Apr 11 2004",
				"8 * a(n) = A005879(n) = A000118(2*n + 1). 16 * a(n) = A129588(n). a(n) = A000593(2*n + 1) = A115607(2*n + 1).",
				"a(n) = A000203(2*n+1). - _Omar E. Pol_, Mar 14 2012",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (4 t)) = (1/4) (t/i)^2 g(t) where q = exp(2 Pi i t) and g() is the g.f. for A096727. _Michael Somos_, Jun 12 2014",
				"a(0) = 1, a(n) = (4/n)*Sum_{k=1..n} A002129(k)*a(n-k) for n \u003e 0. - _Seiichi Manyama_, May 06 2017",
				"G.f.: exp(Sum_{k\u003e=1} 4*(x^k/k)/(1 + x^k)). - _Ilya Gutkovskiy_, Jul 31 2017",
				"From _Peter Bala_, Jan 10 2021: (Start)",
				"a(n) = A002131(2*n+1).",
				"G.f.: Sum_{n \u003e= 0} x^n*(1 + x^(2*n+1))/(1 - x^(2*n+1))^2. (End)"
			],
			"example": [
				"Divisors of 9 are 1,3,9, so a(4)=1+3+9=13.",
				"F_2(z) = eta(4z)^8/eta(2z)^4 = q + 4q^3 + 6q^5 +8q^7 + 13q^9 + ...",
				"G.f. = 1 + 4*x + 6*x^2 + 8*x^3 + 13*x^4 + 12*x^5 + 14*x^6 + 24*x^7 + 18*x^8 + 20*x^9 + ...",
				"B(q) = q + 4*q^3 + 6*q^5 + 8*q^7 + 13*q^9 + 12*q^11 + 14*q^13 + 24*q^15 + 18*q^17 + ..."
			],
			"maple": [
				"A008438 := proc(n) numtheory[sigma](2*n+1) ; end proc: # _R. J. Mathar_, Mar 23 2011"
			],
			"mathematica": [
				"DivisorSigma[1, 2 # + 1] \u0026 /@ Range[0, 61] (* _Ant King_, Dec 02 2010 *)",
				"a[ n_] := SeriesCoefficient[ D[ Series[ Log[ QPochhammer[ -x] / QPochhammer[ x]], {x, 0, 2 n + 1}], x], {x, 0 , 2n}]; (* _Michael Somos_, Oct 15 2019 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, sigma( 2*n + 1))};",
				"(PARI) {a(n) = if( n\u003c0, 0, n = 2*n; polcoeff( sum( k=1, (sqrtint( 4*n + 1) + 1)\\2, x^(k^2 - k), x * O(x^n))^4, n))}; /* _Michael Somos_, Sep 17 2004 */",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, n = 2*n; A = x * O(x^n); polcoeff( (eta(x^4 + A)^2 / eta(x^2 + A))^4, n))}; /* _Michael Somos_, Sep 17 2004 */",
				"(Sage) ModularForms( Gamma0(4), 2, prec=124).1;  # _Michael Somos_, Jun 12 2014",
				"(MAGMA) Basis( ModularForms( Gamma0(4), 2), 124) [2]; /* _Michael Somos_, Jun 12 2014 */",
				"(Haskell)",
				"a008438 = a000203 . a005408  -- _Reinhard Zumkeller_, Sep 22 2014",
				"(MAGMA) [DivisorSigma(1, 2*n+1): n in [0..70]]; // _Vincenzo Librandi_, Aug 01 2017"
			],
			"xref": [
				"Cf. A000118, A000593, A005879, A096727, A115607, A129588, A225699/A225700.",
				"Number of ways of writing n as a sum of k triangular numbers, for k=1,...: A010054, A008441, A008443, A008438, A008439, A008440, A226252, A007331, A226253, A226254, A226255, A014787, A014809.",
				"Cf. A000203, A002131, A005408, A062731, A099774."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Comments from _Len Smiley_, _Enoch Haga_"
			],
			"references": 70,
			"revision": 122,
			"time": "2021-02-14T13:14:44-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}