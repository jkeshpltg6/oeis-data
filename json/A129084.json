{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A129084",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 129084,
			"data": "1,3,7,25,88,49,219,416,4896,4523,68559,40460,613441,791549,487091,1123701,16678867,4363873,121113412,24252821,5893113,7436454,217867766,306700798,14495108003,11420114688,78503059517,93975842393",
			"name": "a(n) = numerator of b(n): b(n) = the minimum possible value for a continued fraction whose terms are a permutation of the terms of the simple continued fraction for H(n) = sum{k=1 to n} 1/k, the n-th harmonic number.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A129084/b129084.txt\"\u003eTable of n, a(n) for n = 1..750\u003c/a\u003e"
			],
			"example": [
				"The continued fraction for H(5) = 137/60 is [2;3,1,1,8]. The minimum value a continued fraction can have with these same terms in some order is [1;8,1,3,2] = 88/79."
			],
			"maple": [
				"with(numtheory):",
				"H:= proc(n) option remember; `if`(n=1, 1, H(n-1)+1/n) end:",
				"r:= proc(l) local j;",
				"      infinity; for j from nops(l) to 1 by -1 do l[j]+1/% od",
				"    end:",
				"hs:= proc(l) local ll, h, s, m; ll:= []; h:= nops(l); s:= 1; m:= s; while s\u003c=h do ll:= [ll[],l[m]]; if m=h then h:= h-1; m:= s else s:= s+1; m:= h fi od; ll end:",
				"a:= n-\u003e numer(r(hs(sort(cfrac(H(n), 'quotients'))))):",
				"seq(a(n), n=1..40);  # _Alois P. Heinz_, Aug 04 2009"
			],
			"mathematica": [
				"r[l_] := Module[{lj, j}, For[lj = Infinity; j = Length[l], j \u003e= 1, j--, lj = l[[j]] + 1/lj]; lj];",
				"hs[l_] := Module[{ll, h, s, m}, ll = {}; h = Length[l]; s = 1; m = s; While[s \u003c= h, ll = Append[ll, l[[m]]]; If[m == h, h--; m = s, s++; m = h ]]; ll];",
				"a[n_] := Numerator[ r[ hs[ Sort[ ContinuedFraction[ HarmonicNumber[n]]]]]];",
				"Table[a[n], {n, 1, 40}] (* _Jean-François Alcover_, Mar 20 2017, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A129082, A129083, A129085."
			],
			"keyword": "frac,nonn",
			"offset": "1,2",
			"author": "_Leroy Quet_, Mar 28 2007",
			"ext": [
				"More terms from _Diana L. Mecum_, Jun 16 2007",
				"Extended beyond a(12) _Alois P. Heinz_, Aug 04 2009"
			],
			"references": 4,
			"revision": 19,
			"time": "2017-03-21T04:13:16-04:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}