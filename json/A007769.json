{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007769",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7769,
			"data": "1,1,2,5,18,105,902,9749,127072,1915951,32743182,624999093,13176573910,304072048265,7623505722158,206342800616597,5996837126024824,186254702826289089,6156752656678674792,215810382466145354405,7995774669504366055054",
			"name": "Number of chord diagrams with n chords; number of pairings on a necklace.",
			"comment": [
				"Place 2n points equally spaced on a circle. Draw lines to pair up all the points so that each point has exactly one partner."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A007769/b007769.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"D. Bar-Natan, \u003ca href=\"http://dx.doi.org/10.1016/0040-9383(95)93237-2\"\u003eOn the Vassiliev Knot Invariants\u003c/a\u003e, Topology 34 (1995) 423-472.",
				"D. Bar-Natan, \u003ca href=\"http://www.pdmi.ras.ru/~duzhin/VasBib/\"\u003eBibliography of Vassiliev Invariants\u003c/a\u003e.",
				"W. Y.-C. Chen, D. C. Torney, \u003ca href=\"http://dx.doi.org/10.1016/j.dam.2004.02.013\"\u003eEquivalence classes of matchings and lattice-square designs\u003c/a\u003e, Discr. Appl. Math. 145 (3) (2005) 349-357., table of C_2n.",
				"Combinatorial Object Server, \u003ca href=\"http://combos.org/necklace\"\u003eInformation on Chord Diagrams\u003c/a\u003e",
				"Étienne Ghys, \u003ca href=\"http://arxiv.org/abs/1612.06373\"\u003eA Singular Mathematical Promenade\u003c/a\u003e, arXiv:1612.06373 [math.GT], 2016. See p. 252.",
				"A. Khruzin, \u003ca href=\"https://arxiv.org/abs/math/0008209\"\u003eEnumeration of chord diagrams\u003c/a\u003e, arXiv:math/0008209 [math.CO], 2000.",
				"R. J. Mathar, \u003ca href=\"http://vixra.org/abs/1901.0148\"\u003eFeynman diagrams of the QED vacuum polarization\u003c/a\u003e, vixra:1901.0148 (2019), Section V.",
				"Joe Sawada, \u003ca href=\"https://doi.org/10.1137/S0895480100377970\"\u003eA fast algorithm for generating nonisomorphic chord diagrams\u003c/a\u003e, SIAM J. Discrete Math, Vol. 15, No. 4, 2002, pp. 546-561.",
				"Alexander Stoimenow, \u003ca href=\"https://doi.org/10.1016/S0012-365X(99)00347-7\"\u003eOn the number of chord diagrams\u003c/a\u003e, Discr. Math. 218 (2000), 209-233.",
				"\u003ca href=\"/index/Ne#necklaces\"\u003eIndex entries for sequences related to necklaces\u003c/a\u003e"
			],
			"formula": [
				"2n a_n = Sum_{2n=pq} alpha(p, q)phi(q), phi = Euler function, alpha(p, q) = Sum_{k \u003e= 0} binomial(p, 2k) q^k (2k-1)!! if q even, = q^{p/2} (p-1)!! if q odd."
			],
			"maple": [
				"with(numtheory):",
				"alpha:=proc(p,q):if is(q,even) then",
				"add(binomial(p,2*k)*q^k*doublefactorial(2*k-1),k=0..p/2)",
				"else q^(p/2)*doublefactorial(p-1) fi end:",
				"a:=n-\u003eadd(alpha(2*n/p,p)*phi(p),p=divisors(2*n))/2/n:",
				"a(0):=1:seq(a(k),k=0..20); # _Robert FERREOL_, Oct 10 2018"
			],
			"mathematica": [
				"max = 20; alpha[p_, q_?EvenQ] := Sum[Binomial[p, 2k]*q^k*(2k-1)!!, {k, 0, max}]; alpha[p_, q_?OddQ] := q^(p/2)*(p-1)!!; a[0] = 1; a[n_] := Sum[q = 2n/p; alpha[p, q]*EulerPhi[q], {p, Divisors[2n]}]/(2n); Table[a[n], {n, 0, max}] (* _Jean-François Alcover_, May 07 2012, after _R. J. Mathar_ *)",
				"Stoimenow states that a Mma package is available from his website. - _N. J. A. Sloane_, Jul 26 2018"
			],
			"program": [
				"(PARI) doublefactorial(n)={ local(resul) ; resul=1 ; forstep(i=n,2,-2, resul *= i ;) ; return(resul) ; }",
				"alpha(n,q)={ if(q %2, return( q^(p/2)*doublefactorial(p-1)), return( sum(k=0,p/2,binomial(p,2*k)*q^k*doublefactorial(2*k-1)) ) ;) ; }",
				"A007769(n)={ local(resul,q) ; if(n==0, return(1), resul=0 ; fordiv(2*n,p, q=2*n/p ; resul += alpha(p,q)*eulerphi(q) ;); return(resul/(2*n)) ;) ; } { for(n=0,20, print(n,\" \",A007769(n)) ;) ; } \\\\ _R. J. Mathar_, Oct 26 2006"
			],
			"xref": [
				"Cf. A054499, A104255, A279207, A279208."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,3",
			"author": "Jean.Betrema(AT)labri.u-bordeaux.fr",
			"ext": [
				"More terms from _Christian G. Bower_, Apr 06 2000",
				"Corrected and extended by _R. J. Mathar_, Oct 26 2006"
			],
			"references": 13,
			"revision": 71,
			"time": "2019-04-30T09:54:22-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}