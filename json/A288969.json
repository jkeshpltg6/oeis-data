{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A288969",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 288969,
			"data": "0,0,1,0,0,1,2,1,0,0,1,2,0,2,1,0,0,1,2,3,1,3,2,1,0,0,1,2,3,0,2,0,3,2,1,0,0,1,2,3,4,1,3,1,4,3,2,1,0,0,1,2,3,4,0,2,4,2,0,4,3,2,1,0,0,1,2,3,4,5,1,3,0,3,1,5,4,3,2,1,0",
			"name": "Triangular array read by rows: row n is the list of the 2*n-1 successive values taken by the function z = n - floor(x) * floor(y) along the hyperbola with equation y = n/x, for 1 \u003c= x \u003c= n.",
			"comment": [
				"See A288966's links for explanations about the algorithm used to go along an hyperbola of equation y = n/x, with 1 \u003c= x \u003c= n.",
				"When represented as a triangular array, internal zeros \"0\" correspond to factorizations of n.",
				"This array appears to resemble a version of the sieve of Eratosthenes with zeros aligned.",
				"A053186 and A293497 appear to intertwine into this sequence. The following will be denoted \"assumption (1)\": with t indexing columns, t=0 being central: T(n, 2k) = A053186(n+k^2) and T(n, 2k+1) = A293497(n+k(k+1)). - _Luc Rousseau_, Oct 11 2017",
				"It would be nice to have a larger b-file, or an a-file. - _N. J. A. Sloane_, Oct 13 2017"
			],
			"link": [
				"Luc Rousseau, \u003ca href=\"/A288969/a288969.txt\"\u003eThe first 25 lines of the triangle array, formatted\u003c/a\u003e"
			],
			"formula": [
				"From _Luc Rousseau_, Oct 11 2017: (Start)",
				"(All formulas under assumption (1))",
				"With t indexing columns, t=0 being central,",
				"T(n, 2k) = A053186(n+k^2).",
				"T(n, 2k+1) = A293497(n+k(k+1)).",
				"T(n, t) = n - x*(x+t) where x = floor((-t+sqrt(t^2+4n))/2).",
				"With A293578 viewed as a 2D array T',",
				"T'(n,t)=T(n-1,t)-T(n,t)+1 (define T(0,0) as 0).",
				"(End)"
			],
			"example": [
				"Array begins:",
				"                0",
				"              0 1 0",
				"            0 1 2 1 0",
				"          0 1 2 0 2 1 0",
				"        0 1 2 3 1 3 2 1 0",
				"      0 1 2 3 0 2 0 3 2 1 0",
				"    0 1 2 3 4 1 3 1 4 3 2 1 0",
				"  0 1 2 3 4 0 2 4 2 0 4 3 2 1 0"
			],
			"mathematica": [
				"(* Under assumption (1) *)",
				"A288969[n_, t_] := Module[{x},",
				"  x = Floor[(-t + Sqrt[t^2 + 4 n])/2];",
				"  n - x (t + x)",
				"] (* _Luc Rousseau_, Oct 11 2017 *)",
				"(* or *)",
				"FEven[x_] := x^ 2",
				"InvFEven[x_] := Sqrt[x]",
				"GEven[n_] := n - FEven[Floor[InvFEven[n]]]",
				"FOdd[x_] := x*(x + 1)",
				"InvFOdd[x_] := (Sqrt[1 + 4 x] - 1)/2",
				"GOdd[n_] := n - FOdd[Floor[InvFOdd[n]]]",
				"A288969[n_, t_] := Module[",
				"  {e, k, x},",
				"  e = EvenQ[t];",
				"  k = If[e, t/2, (t - 1)/2];",
				"  x = n + If[e, FEven[k], FOdd[k]];",
				"  If[e, GEven[x], GOdd[x]]",
				"] (* _Luc Rousseau_, Oct 11 2017 *)"
			],
			"program": [
				"(Java)",
				"package oeis;",
				"public class B {",
				"public static void main(String[] args) {",
				"for (int n = 1; n \u003c= 8; n ++) {",
				"hyberbolaTiles(n);",
				"}",
				"}",
				"private static void hyberbolaTiles(int n) {",
				"int x = 0, y = 0, p = 0, q = n;",
				"do {",
				"if (p != 0) {",
				"System.out.println(n - p * q);",
				"}",
				"if (y \u003c 0) { x = y + q; q --; }",
				"if (y \u003e 0) { p ++; x = y - p; }",
				"if (y == 0) {",
				"p ++;",
				"x = 0; System.out.println(\"0\");",
				"q --;",
				"}",
				"y = x + p - q;",
				"} while (q \u003e 0);",
				"}",
				"}",
				"(PARI) htrow(n) = {my(x = 0, y = 0, p = 0, q = n); while (q\u003e0, if (p, print1(n-p*q, \", \")); if (y \u003c 0, x = y + q; q --); if (y \u003e 0, p ++; x = y - p); if (y == 0, p++; x = 0; print1(0, \", \"); q --;); y = x + p - q;);}",
				"tabf(nn) = for (n=1, nn, htrow(n); print()); \\\\ _Michel Marcus_, Jun 21 2017"
			],
			"xref": [
				"Cf. A053186, A288966, A293497, A293578."
			],
			"keyword": "nonn,tabf",
			"offset": "1,7",
			"author": "_Luc Rousseau_, Jun 20 2017",
			"ext": [
				"More terms from _Michel Marcus_, Jun 21 2017"
			],
			"references": 3,
			"revision": 36,
			"time": "2017-10-13T05:51:56-04:00",
			"created": "2017-06-21T00:01:44-04:00"
		}
	]
}