{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A212002",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 212002,
			"data": "3,9,4,7,8,4,1,7,6,0,4,3,5,7,4,3,4,4,7,5,3,3,7,9,6,3,9,9,9,5,0,4,6,0,4,5,4,1,2,5,4,7,9,7,6,2,8,9,6,3,1,6,2,5,0,5,6,5,3,3,9,7,5,0,4,8,8,0,1,7,9,2,8,9,6,7,6,8,2,0,9,7,2,0,0,7",
			"name": "Decimal expansion of (2*Pi)^2.",
			"comment": [
				"This constant appears in Kepler's 3rd Law, T^2 = (2*Pi)^2/GM*a^3 where a is the semi-major axis of a planet orbiting the Sun, T is its period, and GM is the standard gravitational parameter. - _Raphie Frank_, Dec 13 2012",
				"García \u0026 Marco give a generalized zeta regularization by which this is the value of the product of the primes. - _Charles R Greathouse IV_, Jun 17 2013"
			],
			"link": [
				"J.-P. Allouche, \u003ca href=\"https://arxiv.org/abs/1906.10532\"\u003eThe zeta-regularized product of odious numbers\u003c/a\u003e, arXiv:1906.10532 [math.NT], 2019.",
				"Hyperphysics, \u003ca href=\"http://hyperphysics.phy-astr.gsu.edu/hbase/kepler.html\"\u003eKepler's Laws\u003c/a\u003e",
				"E. Muñoz García and R. Pérez Marco, \u003ca href=\"https://webusers.imj-prg.fr/~ricardo.perez-marco/publications/articles/CMP2008.pdf\"\u003eThe product over all primes is 4Pi^2\u003c/a\u003e, Communications in Mathematical Physics, Vol. 277, No. 1 (2008), pp. 69-81.",
				"Hengguang Li and Jeffrey S. Ovall, \u003ca href=\"http://www.hli.wayne.edu/research/publications/LO15.pdf\"\u003eA posteriori eigenvalue error estimation for a Schrödinger operator with inverse square potential\u003c/a\u003e, Discrete and Continuous Dynamical Systems Series B, Volume 20, Number 5, July 2015, pp. 1377-1391. Also doi:10.3934/dcdsb.2015.20.1377",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Pendulum\"\u003ePendulum\u003c/a\u003e."
			],
			"formula": [
				"Equals Product_{k=1..10, gcd(k,10)==1} Gamma(k/10) = Gamma(1/10)*Gamma(3/10)*Gamma(7/10)*Gamma(9/10). - _Amiram Eldar_, Jun 12 2021",
				"Equals lim_{n-\u003eoo} |B(2*n)/B(2*n+2)|*(2*n+1)*(2*n+2), where B(n) denotes the n-th Bernoulli number. - _Peter Luschny_, Dec 09 2021"
			],
			"example": [
				"39.4784176043574344753379639995046045412547976289631..."
			],
			"mathematica": [
				"RealDigits[(2*Pi)^2,10,120][[1]] (* _Harvey P. Dale_, Mar 27 2019 *)"
			],
			"program": [
				"(PARI) 4*Pi^2 \\\\ _Charles R Greathouse IV_, Jun 17 2013"
			],
			"xref": [
				"Cf. A000796, A002388, A019692, A212003."
			],
			"keyword": "nonn,cons",
			"offset": "2,1",
			"author": "_Omar E. Pol_, Aug 11 2012",
			"references": 7,
			"revision": 53,
			"time": "2021-12-10T23:00:17-05:00",
			"created": "2012-08-11T22:04:41-04:00"
		}
	]
}