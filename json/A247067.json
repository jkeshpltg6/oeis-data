{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A247067",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 247067,
			"data": "1,-64,0,4096,23506,0,0,-262144,531441,-1504384,0,0,6911282,0,0,16777216,-47295038,-34012224,0,96280576,0,0,0,0,308391411,-442322048,0,0,-173439758,0,0,-1073741824,0,3026882432,0,2176782336,-2050092718,0,0,-6161956864",
			"name": "Glaisher's chi_12(n).",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A247067/b247067.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (terms 1..1000 from G. C. Greubel)",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q * f(-x^2)^24 * phi(q)^2 * (chi(q)^24 - 92*q + 16*q^2 / chi(q)^24) in powers of q where phi(), chi(), f() are Ramanujan theta functions.",
				"a(n) is multiplicative with a(2^e) = (-64)^e, a(p^e) = p^(6*e) * (1 + (-1)^e)/2 if p == 3 (mod 4), a(p^e) = a(p) * a(p^(e-1)) - p^12 * a(p^(e-2)) if p == 1 (mod 4) where a(p) = 2 * Re( (x + i*y)^12 ) and p = x^2 + y^2 with even x. - _Michael Somos_, Nov 18 2014",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (4 t)) = 2^13 (t/i)^13 f(t) where q = exp(2 Pi i t).",
				"G.f.: ( Sum_{j,k in Z} (j + i*k)^12 * x^(j^2 + k^2) ) / 4, where i^2 = -1.",
				"a(2*n) = (-4)^3 * a(n). a(4*n + 3) = 0."
			],
			"example": [
				"G.f. = q - 64*q^2 + 4096*q^4 + 23506*q^5 - 262144*q^8 + 531441*q^9 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ q EllipticTheta[3, 0, q]^2 QPochhammer[ q^2]^24 (QPochhammer[ -q, q^2]^24 - 92 q + 16 q^2/QPochhammer[ -q, q^2]^24), {q, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = my(r); if( n\u003c1, 0, r = sqrtint(n); sum( x=-r, r, sum( y=-r, r, if( x^2 + y^2 == n, (x + I*y)^12) )) / 4 ) };",
				"(PARI) {a(n) = local(A, p, e, x, y, z, a0, a1); if( n\u003c0, 0, A = factor(n); prod( k=1, matsize(A)[1], if( p=A[k, 1], e = A[k, 2]; if( p==2, (-64)^e, if( p%4 == 3, if( e%2, 0, p^(6*e)), forstep( i=0, sqrtint(p), 2, if( issquare( p - i^2, \u0026y), x = i; break)); a0 = 1; a1 = x = real( (x + I*y)^12 ) * 2; for( i=2, e, y = x*a1 - p^12*a0; a0=a1; a1=y); a1))))) }; /* _Michael Somos_, Nov 18 2014 */",
				"(MAGMA) A := Basis( CuspForms( Gamma1(4), 13), 40); A[1] - 64*A[2] + 4096*A[4] + 23506*A[5];"
			],
			"xref": [
				"Cf. A002607, A030212."
			],
			"keyword": "sign,mult",
			"offset": "1,2",
			"author": "_Michael Somos_, Nov 16 2014",
			"references": 3,
			"revision": 22,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2014-11-16T15:15:29-05:00"
		}
	]
}