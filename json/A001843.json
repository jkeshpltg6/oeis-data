{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001843",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1843,
			"id": "M2644 N1052",
			"data": "1,1,3,7,14,18,30,35,51,65,91,105,140,157,198,228,285,315,385,419,498,550,650,702,819,877,1005,1085,1240,1320,1496,1583,1773,1887,2109,2223,2470,2593,2856,3010,3311,3465,3795,3959,4308,4508,4900,5100,5525,5737",
			"name": "The coding-theoretic function A(n,4,4).",
			"comment": [
				"Maximal number of 4-subsets of an n-set such that any two subsets meet in at most 2 points."
			],
			"reference": [
				"CRC Handbook of Combinatorial Designs, 1996, p. 411.",
				"R. K. Guy, A problem of Zarankiewicz, in P. Erdős and G. Katona, editors, Theory of Graphs (Proceedings of the Colloquium, Tihany, Hungary), Academic Press, NY, 1968, pp. 119-150.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Jingjun Bao and Lijun Ji, \u003ca href=\"https://doi.org/10.1007/s10623-014-0001-2\"\u003eThe completion determination of optimal (3,4)-packings\u003c/a\u003e, Des. Codes Cryptogr. 77, 217-229 (2015); arXiv:\u003ca href=\"https://arxiv.org/abs/1401.2022\"\u003e1401.2022\u003c/a\u003e [math.CO], 2014.",
				"A. E. Brouwer, \u003ca href=\"http://www.win.tue.nl/~aeb/codes/Andw.html\"\u003eBounds for constant weight binary codes\u003c/a\u003e",
				"A. E. Brouwer, J. B. Shearer, N. J. A. Sloane and W. D. Smith, \u003ca href=\"http://dx.doi.org/10.1109/18.59932\"\u003eNew table of constant weight codes\u003c/a\u003e, IEEE Trans. Info. Theory 36 (1990), 1334-1380.",
				"R. K. Guy, \u003ca href=\"/A001197/a001197.pdf\"\u003eA problem of Zarankiewicz\u003c/a\u003e, Research Paper No. 12, Dept. of Math., Univ. Calgary, Jan. 1967. [Annotated and scanned copy, with permission]",
				"L. Ji, \u003ca href=\"http://dx.doi.org/10.1007/s10623-004-5662-9\"\u003eAsymptotic Determination of the Last Packing Number of Quadruples\u003c/a\u003e, Designs, Codes and Cryptography 38 (2006) 83-95.",
				"\u003ca href=\"/index/Aa#Andw\"\u003eIndex entries for sequences related to A(n,d,w)\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_21\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, order 21"
			],
			"formula": [
				"See Theorem 1.2 of Bao and Ji, 2015 (Theorem 4.9 in the arXiv preprint, but note the missing parentheses for J(n,4,4) on page 1).",
				"a(n)= +a(n-1) +a(n-2) -a(n-3) +a(n-6) -a(n-7) -a(n-8) +a(n-9) +a(n-12) -a(n-13) -a(n-14) +a(n-15) -a(n-18) +a(n-19) +a(n-20) -a(n-21). - _R. J. Mathar_, Oct 01 2021"
			],
			"example": [
				"For n=7 use all seven cyclic shifts of 1110100."
			],
			"maple": [
				"A001843 :=  proc(n)",
				"    floor((n-1)/3* floor((n-2)/2) ) ;",
				"    if modp(n,6) = 0 then",
				"        floor(n*(%-1)/4) ;",
				"    else",
				"        floor(n*%/4) ;",
				"    end if;",
				"end proc:",
				"seq(A001843(n),n=4..80) ; # _R. J. Mathar_, Oct 01 2021"
			],
			"program": [
				"(Python)",
				"[((n-2)//2*(n-1)//3 - int(n%6 == 0)) * n // 4 for n in range(4, 50)]",
				"# _Andrey Zabolotskiy_, Jan 28 2021"
			],
			"keyword": "nonn,nice,easy",
			"offset": "4,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Revised by _N. J. A. Sloane_ and _Andries E. Brouwer_, May 08 2010",
				"Terms a(23) and beyond added, entry edited by _Andrey Zabolotskiy_, Jan 28 2021"
			],
			"references": 3,
			"revision": 35,
			"time": "2021-10-01T10:29:34-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}