{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A309736",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 309736,
			"data": "1,1,2,1,1,2,3,1,1,1,1,2,2,3,5,1,1,1,1,1,1,1,1,2,2,2,2,3,3,5,10,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,3,3,3,4,5,6,10,19,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1",
			"name": "a(1) = 1, and for any n \u003e 1, a(n) is the least k \u003e 0 such that the binary representation of n^k starts with \"10\".",
			"comment": [
				"The sequence is well defined; for any n \u003e 0:",
				"- if n is a power of 2, then a(n) = 1,",
				"- if n is not a power of 2, then log_2(n) is irrational,",
				"  hence the function k -\u003e frac(k * log_2(n)) is dense in the interval [0, 1]",
				"  according to Weyl's criterion,",
				"  so for some k \u003e 0, k*log_2(n) = m + 1 + e where m is a positive integer",
				"                                              and 0 \u003c= e \u003c log_2(3) - 1 \u003c 1,",
				"- hence 2 * 2^m \u003c= n^k \u003c 3 * 2^m and a(n) \u003c= k, QED."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A309736/a309736.png\"\u003eScatterplot of (x, y) such that the binary representation of x^y starts with \"10\" and x = 2..1024 and y = 1..1024\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/WeylsCriterion.html\"\u003eWeyl's Criterion\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 1 iff n belongs to A004754.",
				"a(2*n) = a(n).",
				"A090996(n^a(n)) = 1."
			],
			"example": [
				"For n = 7:",
				"- the first powers of 7, in decimal as well as in binary, are:",
				"    k  7^k  bin(7^k)",
				"    -  ---  ---------",
				"    1    7        111",
				"    2   49     110001",
				"    3  343  101010111",
				"- hence a(7) = 3."
			],
			"program": [
				"(PARI) a(n) = { my (nk=n); for (k=1, oo, if (binary(2*nk)[2]==0, return (k), nk *= n)) }"
			],
			"xref": [
				"Cf. A004754, A090996, A098174."
			],
			"keyword": "nonn,base",
			"offset": "1,3",
			"author": "_Rémy Sigrist_, Aug 14 2019",
			"references": 1,
			"revision": 13,
			"time": "2019-08-22T14:16:55-04:00",
			"created": "2019-08-22T14:16:55-04:00"
		}
	]
}