{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A342019",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 342019,
			"data": "0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,1",
			"name": "Number of prime power divisors of the form p^p in the arithmetic derivative of A276086(n), the prime product form of primorial base expansion of n.",
			"comment": [
				"When considering the iterations of the arithmetic derivative (i.e., of map x -\u003e A003415(x)) and where such process will eventually end, it is known that any divisor of the form p^p (with p prime) in x guarantees that instead of ever reaching zero, it will either get stuck at a fixed point (of the form p^p) or diverge forever towards infinity (see, e.g., Ufnarovski and Åhlander paper). Because such (new) \"doom divisors\" are born only in the \"wild part\" of the arithmetic derivative (i.e., what is left after A003557(n) has been divided out of the derivative of n), it follows that when the derivative is applied to terms of A276086 (that do not contain any doom divisors), the count of such divisors in the result must be equal to that for A342002(n).",
				"Positions of the records (and the first occurrence of each n) start as: 1, 8, 1164, 18675300, ..."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A342019/b342019.txt\"\u003eTable of n, a(n) for n = 1..65537\u003c/a\u003e",
				"Victor Ufnarovski and Bo Åhlander, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL6/Ufnarovski/ufnarovski.html\"\u003eHow to Differentiate a Number\u003c/a\u003e, J. Integer Seqs., Vol. 6, 2003, #03.3.4.",
				"\u003ca href=\"/index/Pri#primorialbase\"\u003eIndex entries for sequences related to primorial base\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A129251(A327860(n)) = A129251(A342002(n)).",
				"a(n) = A001221(A342017(n))."
			],
			"example": [
				"For n=108, A342002(108) = 36 = 2^2 * 3^2. Only the first prime power divisor is of the form p^p, thus a(108) = 1. Note that A276086(108) = A003415(42875) = 42875 = 5^3 * 7^3, and A327860(108) = 44100 = 2^2 * 3^2 * 5^2 * 7^2. The same \"doom divisors\" are always found both in A327860(n) and in A342002(n).",
				"For n=1164, A342002(1164) = 648 = 2^3 * 3^4. In both prime power divisors the exponent attains its base prime (3 \u003e= 2 and 4 \u003e= 3), thus a(1164) = 2. Note that A276086(1164) = 34525308125 = 5^4 * 7^3 * 11^5, and A327860(1164) = 58110129000 = 2^3 * 3^4 * 5^3 * 7^2 * 11^4.",
				"For n=18675300, A342002(18675300) = 3037500 = 2^2 * 3^5 * 5^5. Here all three prime power divisors are \"doom divisors\" because they reach the p^p limit, thus a(18675300) = 3."
			],
			"program": [
				"(PARI)",
				"A129251(n) = { my(f = factor(n)); sum(k=1, #f~, (f[k, 2]\u003e=f[k, 1])); };",
				"A327860(n) = { my(s=0, m=1, p=2, e); while(n, e = (n%p); m *= (p^e); s += (e/p); n = n\\p; p = nextprime(1+p)); (s*m); };",
				"A342019(n) = A129251(A327860(n));"
			],
			"xref": [
				"Cf. A001221, A003415, A003557, A129251, A276086, A327860, A342002, A342005, A342017, A342018 (positions of nonzero terms), A342026."
			],
			"keyword": "nonn",
			"offset": "1,1164",
			"author": "_Antti Karttunen_, Mar 11 2021",
			"references": 5,
			"revision": 29,
			"time": "2021-03-14T18:44:04-04:00",
			"created": "2021-03-12T23:12:25-05:00"
		}
	]
}