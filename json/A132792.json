{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A132792",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 132792,
			"data": "0,0,0,0,2,0,0,0,6,0,0,0,0,12,0,0,0,0,0,20,0,0,0,0,0,0,30,0,0,0,0,0,0,0,42,0,0,0,0,0,0,0,0,56,0,0,0,0,0,0,0,0,0,72,0,0,0,0,0,0,0,0,0,0,90,0,0,0,0,0,0,0,0,0,0,0,110,0",
			"name": "The infinitesimal Lah matrix: generator of unsigned A111596.",
			"comment": [
				"The matrix T begins",
				"0;",
				"0, 0;",
				"0, 2, 0;",
				"0, 0, 6, 0;",
				"0, 0, 0, 12, 0;",
				"Along the nonvanishing diagonal the n-th term is (n+1)*(n).",
				"Let LM(t) = exp(t*T) = limit [1 + t*T/n]^n as n tends to infinity.",
				"Lah matrix = [ bin(n,k)*(n-1)!/(k-1)! ] = LM(1) = exp(T) = unsigned A111596. Truncating the series gives the n X n principal submatrices. In fact, the principal submatrices of T are nilpotent with [Tsub_n]^n = 0 for n=0,1,2,....",
				"Inverse Lah matrix = LM(-1) = exp(-T)",
				"Umbrally LM[b(.)] = exp(b(.)*T) = [ bin(n,k)*(n-1)!/(k-1)! * b(n-k) ]",
				"A(j) = T^j / j! equals the matrix [ bin(n,k)*(n-1)!/(k-1)! * delta(n-k-j)] where delta(n) = 1 if n=0 and vanishes otherwise (Kronecker delta); i.e. A(j) is a matrix with all the terms 0 except for the j-th lower (or main for j=0) diagonal which equals that of the Lah matrix. Hence the A(j)'s form a linearly independent basis for all matrices of the form [ bin(n,k)*(n-1)!/(k-1)! * d(n-k) ].",
				"For sequences with b(0) = 1, umbrally,",
				"LM[b(.)] = exp(b(.)*T) = [ bin(n,k)*(n-1)!/(k-1)! * b(n-k) ] .",
				"[LM[b(.)]]^(-1) = exp(c(.)*T) = [ bin(n,k)*(n-1)!/(k-1)! * c(n-k) ] where c = LPT(b) with LPT the list partition transform of A133314. Or,",
				"[LM[b(.)]]^(-1) = exp[LPT(b(.))*T] = LPT[LM(b(.))] = LM[LPT(b(.))] = LM[c(.)] .",
				"The matrix operation b = T*a can be characterized in several ways in terms of the coefficients a(n) and b(n), their o.g.f.'s A(x) and B(x), or e.g.f.'s EA(x) and EB(x).",
				"1) b(0) = 0, b(n) = n*(n-1) * a(n-1),",
				"2) B(x) = [ x^2 * D^2 * x ] A(x)",
				"3) B(x) = [ x^2 * 2 * Lag(2,-:xD:,0) x^(-1) ] A(x)",
				"4) EB(x) = [ D^(-1) * x * D^2 * x ] EA(x)",
				"where D is the derivative w.r.t. x, (:xD:)^j = x^j * D^j and Lag(n,x,m) is the associated Laguerre polynomial of order m.",
				"The exponentiated operator can be characterized (with loose notation) as",
				"5) exp(t*T) * a = LM(t) * a = [sum(k=0,...,n) bin(n-1,k-1) * (n! / k!) t^(n-k) * a(k) ] = [ t^n * n! * Lag(n,-a(.)/t,-1) ], a vector array. Note binomial(n-1,k-1) is 1 for n=k=0 and vanishes for n\u003e0 and k=0 .",
				"With t=1 and a(k) = (-x)^k, then LM(1) * a = [ n! * Laguerre(n,x,-1) ], a vector array with index n .",
				"6) exp(t*T) EA(x) = EB(x) = EA[ x / (1-x*t) ]",
				"From the inverse operator (change t to -t), inverting amounts to substituting x/(1+x*t) for x in EB(x) in formula 6.",
				"Compare analogous results in A132710.",
				"T is also a shifted version of the infinitesimal Pascal matrix squared, i.e., T = (A132440^2) * A129185 . The non-vanishing diagonal of T is A002378."
			],
			"link": [
				"M. Janjic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL12/Janjic/janjic22.html\"\u003eSome classes of numbers and derivatives\u003c/a\u003e, JIS 12 (2009) 09.8.3"
			],
			"formula": [
				"Given a polynomial sequence p_n(x) with p_0(x)=1 and the lowering and raising operators L and R defined by L P_n(x) = n * P_(n-1)(x) and",
				"  R P_n(x) = P_(n+1)(x), the matrix T represents the action of R^2*L^2*R",
				"  in the p_n(x) basis. For p_n(x) = x^n, L = D = d/dx and R = x.",
				"  For p_n(x)  = x^n/n!, L = DxD and R = D^(-1). - _Tom Copeland_, Oct 25 2012"
			],
			"mathematica": [
				"Table[PadLeft[{n*(n-1), 0}, n+1], {n, 0, 11}] // Flatten (* _Jean-François Alcover_, Apr 30 2014 *)"
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,5",
			"author": "_Tom Copeland_, Nov 17 2007, Nov 27 2007, Nov 29 2007",
			"references": 3,
			"revision": 16,
			"time": "2016-09-07T14:19:14-04:00",
			"created": "2007-12-09T03:00:00-05:00"
		}
	]
}