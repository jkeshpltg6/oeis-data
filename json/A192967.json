{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A192967",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 192967,
			"data": "1,0,2,4,9,17,31,54,92,154,255,419,685,1116,1814,2944,4773,7733,12523,20274,32816,53110,85947,139079,225049,364152,589226,953404,1542657,2496089,4038775,6534894,10573700,17108626,27682359,44791019,72473413,117264468",
			"name": "Constant term of the reduction by x^2 -\u003e x+1 of the polynomial p(n,x) defined at Comments.",
			"comment": [
				"The titular polynomials are defined recursively: p(n,x) = x*p(n-1,x) + n(n-1)/2, with p(0,x)=1. For an introduction to reductions of polynomials by substitutions such as x^2-\u003ex+1, see A192232 and A192744."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A192967/b192967.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-2,-1,1)."
			],
			"formula": [
				"a(0)=1, a(1)=0, for n \u003e 1, a(n) = a(n-1) + a(n-2) + n - 1. - _Alex Ratushnyak_, May 10 2012",
				"a(n) = 3*a(n-1) - 2*a(n-2) - a(n-3) + a(n-4).",
				"G.f.: (1 -3*x +4*x^2 -x^3)/((1-x-x^2)*(1-x)^2). - _R. J. Mathar_, May 11 2014",
				"a(n) = 3*Fibonacci(n+1) - n - 2. - _G. C. Greubel_, Jul 11 2019"
			],
			"mathematica": [
				"q = x^2; s = x + 1; z = 40;",
				"p[0, x]:= 1;",
				"p[n_, x_]:= x*p[n-1, x] + n*(n-1)/2;",
				"Table[Expand[p[n, x]], {n, 0, 7}]",
				"reduce[{p1_, q_, s_, x_}]:= FixedPoint[(s PolynomialQuotient @@ #1 + PolynomialRemainder @@ #1 \u0026)[{#1, q, x}] \u0026, p1]",
				"t = Table[reduce[{p[n, x], q, s, x}], {n, 0, z}];",
				"u1 = Table[Coefficient[Part[t, n], x, 0], {n, 1, z}] (* A192967 *)",
				"u2 = Table[Coefficient[Part[t, n], x, 1], {n, 1, z}] (* A192968 *)",
				"LinearRecurrence[{3,-2,-1,1}, {1,0,2,4}, 41] (* _Vladimir Joseph Stephan Orlovsky_, Feb 15 2012 *)",
				"Table[3*Fibonacci[n+1] -n-2, {n,0,40}] (* _G. C. Greubel_, Jul 11 2019 *)"
			],
			"program": [
				"(MAGMA) I:=[1, 0, 2, 4]; [n le 4 select I[n] else 3*Self(n-1)-2*Self(n-2)-Self(n-3)+Self(n-4): n in [1..40]]; // _Vincenzo Librandi_, Feb 20 2012",
				"(MAGMA) [3*Fibonacci(n+1) -n-2: n in [0..40]]; // _G. C. Greubel_, Jul 11 2019",
				"(PARI) vector(40, n, n--; f=fibonacci; 3*f(n+1)-n-2) \\\\ _G. C. Greubel_, Jul 11 2019",
				"(Sage) [3*fibonacci(n+1) -n-2 for n in (0..40)] # _G. C. Greubel_, Jul 11 2019",
				"(GAP) List([0..40], n-\u003e 3*Fibonacci(n+1) -n-2); # _G. C. Greubel_, Jul 11 2019"
			],
			"xref": [
				"Cf. A000045, A192232, A192744, A192951."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Clark Kimberling_, Jul 13 2011",
			"references": 4,
			"revision": 29,
			"time": "2019-07-11T21:52:58-04:00",
			"created": "2011-07-13T20:18:55-04:00"
		}
	]
}