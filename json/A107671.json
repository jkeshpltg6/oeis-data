{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A107671",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 107671,
			"data": "1,8,2,513,27,3,81856,2368,64,4,23846125,469625,7625,125,5,10943504136,160767720,1898856,19656,216,6,7250862593527,83548607478,776598305,6081733,43561,343,7,6545029128786432,61068815111168,465690017280,2966844928,16494080,86528,512,8",
			"name": "Triangular matrix T, read by rows, that satisfies: T = D + SHIFT_LEFT(T^3), where SHIFT_LEFT shifts each row 1 place to the left and D is the diagonal matrix {1, 2, 3, ...}.",
			"formula": [
				"Matrix diagonalization method: define the triangular matrix P by P(n, k) = ((n+1)^3)^(n-k)/(n-k)! for n \u003e= k \u003e= 0 and the diagonal matrix D(n, n) = n+1 for n \u003e= 0; then T is given by T = P^-1*D*P.",
				"T(n,k=0) = Sum_{r=1..(n+1)} (-1)^(r-1) * Sum_{s_1, ..., s_r} (s_1^(-1)/(Product_{j=1..r} s_j!)) * Product_{j=1..r} (Sum_{i=1..j} s_i)^(3*s_j)), where the second sum is over lists (s_1, ..., s_r) of positive integers s_i such that Sum_{i=1..r} s_i = n + 1. (Thus, the second sum is over all compositions of n + 1.) - _Petros Hadjicostas_, Mar 11 2021"
			],
			"example": [
				"Triangle T begins:",
				"              1;",
				"              8,           2;",
				"            513,          27,         3;",
				"          81856,        2368,        64,       4;",
				"       23846125,      469625,      7625,     125,     5;",
				"    10943504136,   160767720,   1898856,   19656,   216,   6;",
				"  7250862593527, 83548607478, 776598305, 6081733, 43561, 343, 7;",
				"  ...",
				"The matrix cube T^3 shifts each row to the right 1 place, dropping the diagonal D and putting A006690 in column 0:",
				"             1;",
				"            56,           8;",
				"          7965,         513,        27;",
				"       2128064,       81856,      2368,      64;",
				"     914929500,    23846125,    469625,    7625,  125;",
				"  576689214816, 10943504136, 160767720, 1898856, 19656, 216;",
				"  ...",
				"From _Petros Hadjicostas_, Mar 11 2021: (Start)",
				"We illustrate the above formula for T(n,k=0) with the compositions of n + 1 for n = 2. The compositions of n + 1 = 3 are 3, 1 + 2, 2 + 1, and 1 + 1 + 1.  Thus the above sum has four terms with (r = 1, s_1 = 3), (r = 2, s_1 = 1, s_2 = 2), (r = 2, s_1 = 2, s_2 = 1), and (r = 3, s_1 = s_2 = s_3 = 1).",
				"The value of the denominator Product_{j=1..r} s_j! for these four terms is 6, 2, 2, and 1, respectively.",
				"The value of the numerator s_1^(-1)*Product_{j=1..r} (Sum_{i=1..j} s_i)^(3*s_j) for these four terms is 19683/3, 729/1, 1728/2, and 216/1.",
				"Thus T(2,0) = (19683/3)/6 - (729/1)/2 - (1728/2)/2 + (216/1)/1 = 513. (End)"
			],
			"program": [
				"(PARI) {T(n,k)=local(P=matrix(n+1,n+1,r,c,if(r\u003e=c,(r^3)^(r-c)/(r-c)!)), D=matrix(n+1,n+1,r,c,if(r==c,r)));if(n\u003e=k,(P^-1*D*P)[n+1,k+1])}"
			],
			"xref": [
				"Cf. A107667, A107672 (column 0), A107673, A107674 (matrix square), A107676 (matrix cube), A006690."
			],
			"keyword": "nonn,tabl",
			"offset": "0,2",
			"author": "_Paul D. Hanna_, Jun 07 2005",
			"references": 6,
			"revision": 21,
			"time": "2021-03-11T07:43:25-05:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}