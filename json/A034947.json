{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A034947",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 34947,
			"data": "1,1,-1,1,1,-1,-1,1,1,1,-1,-1,1,-1,-1,1,1,1,-1,1,1,-1,-1,-1,1,1,-1,-1,1,-1,-1,1,1,1,-1,1,1,-1,-1,1,1,1,-1,-1,1,-1,-1,-1,1,1,-1,1,1,-1,-1,-1,1,1,-1,-1,1,-1,-1,1,1,1,-1,1,1,-1,-1,1,1,1,-1,-1,1,-1,-1,1,1",
			"name": "Jacobi (or Kronecker) symbol (-1/n).",
			"comment": [
				"Also the regular paper-folding sequence.",
				"For a proof that a(n) equals the paper-folding sequence, see Allouche and Sondow, arXiv v4. - _Jean-Paul Allouche_ and _Jonathan Sondow_, May 19 2015",
				"It appears that, replacing +1 with 0 and -1 with 1, we obtain A038189. Alternatively, replacing -1 with 0 we obtain (allowing for offset) A014577. - _Jeremy Gardiner_, Nov 08 2004",
				"Partial sums = A005811 starting (1, 2, 1, 2, 3, 2, 1, 2, 3, ...). - _Gary W. Adamson_, Jul 23 2008"
			],
			"reference": [
				"J.-P. Allouche and J. Shallit, Automatic Sequences, Cambridge Univ. Press, 2003, pp. 155, 182.",
				"H. Cohen, Course in Computational Number Theory, p. 28.",
				"Danielle Cox and K. McLellan, A problem on generation sets containing Fibonacci numbers, Fib. Quart., 55 (No. 2, 2017), 105-113."
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A034947/b034947.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"J.-P. Allouche, G.-N. Han, and J. Shallit, \u003ca href=\"https://arxiv.org/abs/2006.08909\"\u003eOn some conjectures of P. Barry\u003c/a\u003e, arXiv:2006.08909 [math.NT], 2020.",
				"J.-P. Allouche and J. Sondow, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v22i1p59\"\u003eSummation of rational series twisted by strongly B-multiplicative coefficients\u003c/a\u003e, Electron. J. Combin., 22 #1 (2015) P1.59; see p. 8.",
				"J.-P. Allouche and J. Sondow, \u003ca href=\"http://arxiv.org/abs/1408.5770\"\u003eSummation of rational series twisted by strongly B-multiplicative coefficients\u003c/a\u003e, arXiv:1408.5770 [math.NT] v4, 2015;  see p. 9.",
				"Jean-Paul Allouche and Leo Goldmakher, \u003ca href=\"http://arxiv.org/abs/1608.03957\"\u003eMock characters and the Kronecker symbol\u003c/a\u003e, arXiv:1608.03957 [math.NT], 2016.",
				"Joerg Arndt, \u003ca href=\"http://www.jjj.de/fxt/#fxtbook\"\u003eMatters Computational (The Fxtbook)\u003c/a\u003e, section 38.8.4 Differences of the sum of Gray code digits, coefficients of polynomials L.",
				"Chandler Davis and Donald E. Knuth, Number Representations and Dragon Curves -- I and II, Journal of Recreational Mathematics, volume 3, number 2, April 1970, pages 66-81, and number 3, July 1970, pages 133-149.  Reprinted with addendum in Donald E. Knuth, \u003ca href=\"http://www-cs-faculty.stanford.edu/~uno/fg.html\"\u003eSelected Papers on Fun and Games\u003c/a\u003e, CSLI Publications, 2010, pages 571-614.  a(n) = d(n) at equation 3.1.",
				"A. Iványi, \u003ca href=\"http://www.emis.de/journals/AUSM/C5-1/math51-5.pdf\"\u003eLeader election in synchronous networks\u003c/a\u003e, Acta Univ. Sapientiae, Mathematica, 5, 2 (2013) 54-82.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/KroneckerSymbol.html\"\u003eKronecker Symbol\u003c/a\u003e",
				"\u003ca href=\"/index/Fo#fold\"\u003eIndex entries for sequences obtained by enumerating foldings\u003c/a\u003e"
			],
			"formula": [
				"Multiplicative with a(2^e) = 1, a(p^e) = (-1)^(e(p-1)/2) if p\u003e2.",
				"a(2*n) = a(n), a(4*n+1) = 1, a(4*n+3) = -1, a(-n) = -a(n). a(n) = 2*A014577(n-1)-1.",
				"a(prime(n)) = A070750(n) for n \u003e 1 - _T. D. Noe_, Nov 08 2004",
				"This sequence can be constructed by starting with w = \"empty string\", and repeatedly applying the map w -\u003e w 1 reverse(-w) [See Allouche and Shallit p. 182). - _N. J. A. Sloane_, Jul 27 2012",
				"a(n) = (-1)^k, where k is number of primes of the form 4*m + 3 dividing n (counted with multiplicity). - _Arkadiusz Wesolowski_, Nov 05 2013",
				"Sum_{n\u003e=1} a(n)/n = Pi/2, due to F. von Haeseler; more generally, Sum_{n\u003e=1} a(n)/n^(2*d+1) = Pi^(2*d+1)*A000364(d)/(2^(2*d+2)-2)(2*d)! for d \u003e= 0; see Allouche and Sondow, 2015. - _Jean-Paul Allouche_ and _Jonathan Sondow_, Mar 20 2015",
				"Dirichlet g.f.: beta(s)/(1-2^(-s)) = L(chi_2(4),s)/(1-2^(-s)). - _Ralf Stephan_, Mar 27 2015",
				"a(n) = A209615(n) * (-1)^(v2(n)), where v2(n) = A007814(n) is the 2-adic valuation of n. - _Jianing Song_, Apr 24 2021"
			],
			"example": [
				"G.f. = x + x^2 - x^3 + x^4 + x^5 - x^6 - x^7 + x^8 + x^9 + x^10 - x^11 - x^12 + ..."
			],
			"maple": [
				"with(numtheory): A034947 := n-\u003ejacobi(-1,n);"
			],
			"mathematica": [
				"Table[KroneckerSymbol[ -1, n], {n, 0, 100}] (* Corrected by _Jean-François Alcover_, Dec 04 2013 *)"
			],
			"program": [
				"(PARI) {a(n) = kronecker(-1, n)};",
				"(PARI) for(n=1, 81, f=factor(n); print1((-1)^sum(s=1, omega(n), f[s, 2]*(Mod(f[s, 1], 4)==3)), \", \")); \\\\ _Arkadiusz Wesolowski_, Nov 05 2013",
				"(PARI) a(n)=direuler(p=1,n,if(p==2,1/(1-kronecker(-4, p)*X)/(1-X),1/(1-kronecker(-4, p)*X))) /* _Ralf Stephan_, Mar 27 2015 */",
				"(MAGMA) [KroneckerSymbol(-1,n): n in [1..100]]; // _Vincenzo Librandi_, Aug 16 2016",
				"(Python)",
				"def A034947(n):",
				"    s = bin(n)[2:]",
				"    m = len(s)",
				"    i = s[::-1].find('1')",
				"    return 1-2*int(s[m-i-2]) if m-i-2 \u003e= 0 else 1 # _Chai Wah Wu_, Apr 08 2021"
			],
			"xref": [
				"Cf. A005811, A000364, A209615.",
				"Moebius transform of A035184.",
				"Indices of 1: A091072; indices of -1: A091067.",
				"The following are all essentially the same sequence: A014577, A014707, A014709, A014710, A034947, A038189, A082410. - _N. J. A. Sloane_, Jul 27 2012"
			],
			"keyword": "sign,nice,easy,mult",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_",
			"references": 27,
			"revision": 103,
			"time": "2021-04-24T03:27:03-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}