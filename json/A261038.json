{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A261038",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 261038,
			"data": "1,3,0,0,0,6,-1,-8,-1,9,-2,-24,-2,12,-3,-48,-3,15,-4,-80,-4,18,-5,-120,-5,21,-6,-168,-6,24,-7,-224,-7,27,-8,-288,-8,30,-9,-360,-9,33,-10,-440,-10,36,-11,-528,-11,39,-12,-624,-12,42,-13,-728,-13,45,-14",
			"name": "a(1)=1; for n\u003e1: a(n) = a(n-1)*n if t=0, a(n) = round(a(n-1)/n) if t=1, a(n) = a(n-1)+n if t=2, a(n) = a(n-1)-n if t=3, where t = n mod 4.",
			"comment": [
				"a(4*n+1) = 1,  0,  -1,  -2,  -3, ...",
				"a(4*n+2) = 3,  6,   9,  12,  15, ...",
				"a(4*n+3) = 0, -1,  -2,  -3,  -4, ...",
				"a(4*n+4) = 0, -8, -24, -48, -80, ... = -A033996(n)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A261038/b261038.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_12\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,0,0,3,0,0,0,-3,0,0,0,1)."
			],
			"formula": [
				"From _Colin Barker_, Aug 09 2015: (Start)",
				"a(n) = 3*a(n-4) - 3*a(n-8) + a(n-12).",
				"G.f.: -x*(x^10+2*x^8-8*x^7-x^6-3*x^5-3*x^4+3*x+1) / ((x-1)^3*(x+1)^3*(x^2+1)^3).",
				"(End)"
			],
			"example": [
				"a(1) =                    1.",
				"a(2) =       a(1) + 2  =  3.",
				"a(3) =       a(2) - 3  =  0.",
				"a(4) =       a(3) * 4  =  0.",
				"a(5) = round(a(4) / 5) =  0.",
				"a(6) =       a(5) + 6  =  6.",
				"a(7) =       a(6) - 7  = -1."
			],
			"maple": [
				"a:= proc(n) option remember; `if`(n=1, 1, (t-\u003e",
				"      `if`(t=0, a(n-1)*n, `if`(t=1, round(a(n-1)/n),",
				"      `if`(t=2, a(n-1)+n, a(n-1)-n))))(irem(n, 4)))",
				"    end:",
				"seq(a(n), n=1..100);  # _Alois P. Heinz_, Aug 08 2015"
			],
			"mathematica": [
				"nxt[{n_,a_}]:=Module[{t=Mod[n+1,4]},{n+1,Which[t==0,a(n+1), t==1,Round[ a/(n+1)], t==2,a+n+1,t==3,a-n-1]}]; NestList[nxt,{1,1},100][[All,2]] (* or *) LinearRecurrence[{0,0,0,3,0,0,0,-3,0,0,0,1},{1,3,0,0,0,6,-1,-8,-1,9,-2,-24},100] (* _Harvey P. Dale_, May 25 2018 *)"
			],
			"program": [
				"(PARI) Vec(-x*(x^10+2*x^8-8*x^7-x^6-3*x^5-3*x^4+3*x+1)/((x-1)^3*(x+1)^3*(x^2+1)^3) + O(x^100)) \\\\ _Colin Barker_, Aug 10 2015",
				"(PARI) first(m)=my(v=vector(m),t);v[1]=1;for(i=2,m,t = i%4;if(t==0,v[i]=v[i-1]*i,if(t==1,v[i]=round(v[i-1]/i),if(t==2,v[i]=v[i-1]+i,v[i]=v[i-1]-i ))));v; \\\\ _Anders Hellström_, Aug 17 2015"
			],
			"xref": [
				"Cf. A033996."
			],
			"keyword": "sign,easy",
			"offset": "1,2",
			"author": "_Peter Woodward_, Aug 07 2015",
			"ext": [
				"More terms from _Alois P. Heinz_, Aug 08 2015",
				"Edited by _Jon E. Schoenfield_, Aug 08 2015",
				"Corrected by _Harvey P. Dale_, May 25 2018"
			],
			"references": 1,
			"revision": 53,
			"time": "2018-05-25T12:11:10-04:00",
			"created": "2015-08-09T08:23:33-04:00"
		}
	]
}