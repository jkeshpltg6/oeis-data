{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A230628",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 230628,
			"data": "4,12,18,24,30,36,42,48,54,60,66,72,78,84,90,96,102,108,114,120,126,132,138,144,150,156,162,168,174,180,186,192,198,204,210,216,222,228,234,240,246,252,258,264,270,276,282,288,294,300,306,312",
			"name": "Maximum number of colors needed to color a planar map of several empires, each empire consisting of n countries.",
			"comment": [
				"The countries in an empire must all have the same color; adjacent empires must have different colors.",
				"The case n=1 is the four-color theorem: any planar map with any number of countries can be colored with 4 colors, so a(1)=4.",
				"Obviously, replacing \"each empire consisting of n countries\" with \"each empire consisting of at most n countries\" yields the same sequence, since we are considering the worst case. - _Daniel Forgues_, Apr 03 2016"
			],
			"reference": [
				"M. Gardner, Mathematical Games Column, Scientific American, Feb. 1980, page 14.",
				"P. J. Heawood, \"Map colour theorem,\" Quart. J. Math. Oxford Ser. 2., 24, 332-338 (1890).",
				"Ian Stewart, The rise and fall of the lunar m-pire, Mathematical Recreations Column, Scientific American, 268 (April 1993), 120-121."
			],
			"link": [
				"Brad Jackson and Gerhard Ringel, \u003ca href=\"http://resolver.sub.uni-goettingen.de/purl?GDZPPN002201143\"\u003eSolution of Heawood's empire problem in the plane\u003c/a\u003e, J. Reine Angew. Math. 347 (1984), 146-153.",
				"Brad Jackson and Gerhard Ringel, \u003ca href=\"http://dx.doi.org/10.1016/0095-8956(85)90082-6\"\u003eHeawood's empire problem\u003c/a\u003e, Journal of Combinatorial Theory, Series B 38.2 (1985): 168-178.",
				"N. J. A. Sloane, \u003ca href=\"/A230628/a230628.pdf\"\u003eIllustrations of a(2)=12 and a(3)=18\u003c/a\u003e [Annotated copy of part of a figure from Ian Stewart's Scientific American article referenced above]",
				"N. J. A. Sloane, \u003ca href=\"/A230628/a230628.png\"\u003eAnother illustration for a(2)=12\u003c/a\u003e [This symmetrical map was drawn by Scott Kim (see Gardner, 1980), the colors were assigned by Jackson and Ringel (1984), and the map was then colored by hand]",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,-1)."
			],
			"formula": [
				"a(1)=4; thereafter a(n) = 6*n (See Jackson \u0026 Ringel 1984).",
				"G.f.: (-2*x)*(x^2-2*x-2)/(x-1)^2. - _Daniel Forgues_, Mar 04 2016",
				"a(n) = 2*a(n-1) - a(n-2) for n \u003e 3. - _Wesley Ivan Hurt_, Mar 07 2016"
			],
			"example": [
				"For n=2, we have a number of empires (12 in the top part of the illustration above), each empire having two countries. The illustration shows an example where all 12 colors are needed."
			],
			"maple": [
				"A230628:=n-\u003e6*n: 4, seq(A230628(n), n=2..100); # _Wesley Ivan Hurt_, Mar 07 2016"
			],
			"mathematica": [
				"Join[{4,12,18},LinearRecurrence[{2,-1},{24,30},50]] (* _Harvey P. Dale_, Dec 05 2014 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003e1,6*n,4) \\\\ _Charles R Greathouse IV_, Dec 11 2013"
			],
			"keyword": "nonn,easy,nice",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_, Oct 29 2013",
			"references": 1,
			"revision": 58,
			"time": "2017-09-12T21:19:15-04:00",
			"created": "2013-10-29T00:09:52-04:00"
		}
	]
}