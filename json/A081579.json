{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A081579",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 81579,
			"data": "1,1,1,1,6,1,1,11,11,1,1,16,46,16,1,1,21,106,106,21,1,1,26,191,396,191,26,1,1,31,301,1011,1011,301,31,1,1,36,436,2076,3606,2076,436,36,1,1,41,596,3716,9726,9726,3716,596,41,1,1,46,781,6056,21746,33876,21746,6056,781,46,1",
			"name": "Pascal-(1,4,1) array.",
			"comment": [
				"One of a family of Pascal-like arrays. A007318 is equivalent to the (1,0,1)-array. A008288 is equivalent to the (1,1,1)-array. Rows include A016861, A081587, A081588. Coefficients of the row polynomials in the Newton basis are given by A013612."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A081579/b081579.txt\"\u003eRows n = 0..100, flattened\u003c/a\u003e",
				"Paul Barry, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL9/Barry/barry91.html\"\u003eOn Integer-Sequence-Based Constructions of Generalized Pascal Triangles\u003c/a\u003e, Journal of Integer Sequences, Vol. 9 (2006), Article 06.2.4."
			],
			"formula": [
				"Square array T(n, k) defined by T(n, 0) = T(0, k) = 1, T(n, k) = T(n, k-1) + 4*T(n-1, k-1) + T(n-1, k).",
				"Rows are the expansions of (1+4*x)^k/(1-x)^(k+1).",
				"From _Philippe Deléham_, Mar 15 2014: (Start)",
				"Riordan array (1/(1-x), x*(1+4*x)/(1-x)).",
				"Sum_{k=0..n} T(n, k) = A063727(n). (End)",
				"E.g.f. for the n-th subdiagonal of the triangle, n = 0,1,2,..., equals exp(x)*P(n,x), where P(n,x) is the polynomial Sum_{k = 0..n} binomial(n,k)*(5*x)^k/k!. For example, the e.g.f. for the second subdiagonal is exp(x)*(1 + 10*x + 25*x^2/2) = 1 + 11*x + 46*x^2/2! + 106*x^3/3! + 191*x^4/4! + 301*x^5/5! + .... - _Peter Bala_, Mar 05 2017",
				"From _G. C. Greubel_, May 26 2021: (Start)",
				"T(n, k, m) = Hypergeometric2F1([-k, k-n], [1], m+1), for m = 4.",
				"T(n, k, m) = Sum_{j=0..n-k} binomial(k,j)*binomial(n-j,k)*m^j, for m = 4. (End)"
			],
			"example": [
				"Square array begins as:",
				"  1,  1,   1,    1,    1, ... A000012;",
				"  1,  6,  11,   16,   21, ... A016861;",
				"  1, 11,  46,  106,  191, ... A081587;",
				"  1, 16, 106,  396, 1011, ... A081588;",
				"  1, 21, 191, 1011, 3606, ...",
				"As triangle this begins:",
				"  1;",
				"  1,  1;",
				"  1,  6,   1;",
				"  1, 11,  11,    1;",
				"  1, 16,  46,   16,     1;",
				"  1, 21, 106,  106,    21,     1;",
				"  1, 26, 191,  396,   191,    26,     1;",
				"  1, 31, 301, 1011,  1011,   301,    31,    1;",
				"  1, 36, 436, 2076,  3606,  2076,   436,   36,   1;",
				"  1, 41, 596, 3716,  9726,  9726,  3716,  596,  41,  1;",
				"  1, 46, 781, 6056, 21746, 33876, 21746, 6056, 781, 46, 1; - _Philippe Deléham_, Mar 15 2014"
			],
			"mathematica": [
				"Table[Hypergeometric2F1[-k, k-n, 1, 5], {n,0,12}, {k,0,n}]//Flatten (* _Jean-François Alcover_, May 24 2013 *)"
			],
			"program": [
				"(MAGMA)",
				"A081579:= func\u003c n,k,q | (\u0026+[Binomial(k, j)*Binomial(n-j, k)*q^j: j in [0..n-k]]) \u003e;",
				"[A081579(n,k,4): k in [0..n], n in [0..12]]; // _G. C. Greubel_, May 26 2021",
				"(Sage) flatten([[hypergeometric([-k, k-n], [1], 5).simplify() for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, May 26 2021"
			],
			"xref": [
				"Cf. Pascal (1,m,1) array: A123562 (m = -3), A098593 (m = -2), A000012 (m = -1), A007318 (m = 0), A008288 (m = 1), A081577 (m = 2), A081578 (m = 3), A081580 (m = 5), A081581 (m = 6), A081582 (m = 7), A143683 (m = 8).",
				"Cf. A016861, A063727, A081587, A081588."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,5",
			"author": "_Paul Barry_, Mar 23 2003",
			"references": 12,
			"revision": 33,
			"time": "2021-05-29T20:05:34-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}