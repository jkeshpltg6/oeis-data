{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A055231",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 55231,
			"data": "1,2,3,1,5,6,7,1,1,10,11,3,13,14,15,1,17,2,19,5,21,22,23,3,1,26,1,7,29,30,31,1,33,34,35,1,37,38,39,5,41,42,43,11,5,46,47,3,1,2,51,13,53,2,55,7,57,58,59,15,61,62,7,1,65,66,67,17,69,70,71,1,73,74,3,19,77,78,79,5",
			"name": "Powerfree part of n: product of primes that divide n only once.",
			"comment": [
				"The previous name was: Write n = K^2*F where F is squarefree and F = g*f where g = gcd(K,F) and f = F/g; then a(n) = f(n) = F(n)/g(n). Thus gcd(K^2,f) = 1.",
				"Differs from A007913; they coincide if and only if g(n) = 1.",
				"a(n) is the powerfree part of n; i.e., if n=Product(pi^ei) over all i [prime factorization) then a(n)=Product(pi^ei) over those i with ei=1; if n=b*c^2*d^3 then a(n) is minimum possible value of b. - _Henry Bottomley_, Sep 01 2000",
				"Also denominator of n/rad(n)^2, where rad is the squarefree kernel of n (A007947), numerator: A062378. - _Reinhard Zumkeller_, Dec 10 2002",
				"Largest unitary squarefree number dividing n (the unitary squarefree kernel of n). - _Steven Finch_, Mar 01 2004",
				"From _Bernard Schott_, Mar 24 2020: (Start)",
				"a(n) = 1 iff n is a perfect power (A001597),",
				"a(n) = n iff n is a squarefree number (A005117), and",
				"(a(n) \u003c\u003e 1 and a(n) \u003c\u003en) iff n is in A059404. (End)"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A055231/b055231.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e (first 1000 terms from T. D. Noe)",
				"Steven R. Finch, \u003ca href=\"/A007947/a007947.pdf\"\u003eUnitarism and Infinitarism\u003c/a\u003e, February 25, 2004. [Cached copy, with permission of the author]",
				"Vaclav Kotesovec, \u003ca href=\"/A055231/a055231.jpg\"\u003ePlot of Sum_{k=1..n} a(k) / n^2 for n = 1..1000000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = n/A057521(n).",
				"Multiplicative with a(p) = p and a(p^e) = 1 for e \u003e 1. - _Vladeta Jovovic_, Nov 01 2001",
				"Dirichlet g.f.: zeta(s)*Product_{primes p} (1 + p^(1-s) - p^(-s) - p^(1-2s) + p^(-2s)). - _R. J. Mathar_, Dec 21 2011",
				"a(n) = A007947(n)/A071773(n). - observed by _Velin Yanev_, Aug 27 2017, confirmed by _Antti Karttunen_, Nov 28 2017",
				"a(1) = 1; for n \u003e 1, a(n) = A020639(n)^A063524(A067029(n)) * a(A028234(n)). - _Antti Karttunen_, Nov 28 2017",
				"a(n*m) = a(n)*a(m)/(gcd(n,a(m))*gcd(m,a(n))) for all n and m \u003e 0 (conjectured). - _Velin Yanev_, Feb 06 2019. [This follows easily from the comment of _Vladeta Jovovic_. - _N. J. A. Sloane_, Mar 14 2019]",
				"From _Vaclav Kotesovec_, Dec 19 2019: (Start)",
				"Dirichlet g.f.: zeta(s-1) * zeta(s) * Product_{primes p} (1 - p^(1-3*s) + p^(2-3*s) - p^(2-2*s) + p^(-2*s) - p^(-s)).",
				"Sum_{k=1..n} a(k) ~ c * Pi^2 * n^2 / 12, where c = Product_{primes p} (1 - 2/p^2 + 2/p^4 - 1/p^5) = 0.394913518073109872954607634745304266741971541072... (End)"
			],
			"example": [
				"If n = 15!, A008833(15!) = 30240*30240, A007913(15!) = 1430, g(15!) = 10, a(15!) = A007913(15!) = 143 and GCD[30240,143] = 1. 15! = (30240*30240)*1430 = (30240^2)*10*143 = K*K*F = (K^2)*g*f."
			],
			"maple": [
				"A055231 := proc(n)",
				"    a := 1 ;",
				"    if n \u003e 1 then",
				"        for f in ifactors(n)[2] do",
				"            if op(2, f) = 1 then",
				"                a := a*op(1, f) ;",
				"            end if;",
				"        end do:",
				"    end if;",
				"    a ;",
				"end proc: # _R. J. Mathar_, Dec 23 2011"
			],
			"mathematica": [
				"rad[n_] := Times @@ First /@ FactorInteger[n]; a[n_] := Denominator[n/rad[n]^2]; Table[a[n], {n, 1, 80}] (* _Jean-François Alcover_, Jun 20 2013, after _Reinhard Zumkeller_ *)",
				"f[p_, e_] := If[e==1, p, 1]; a[n_] := Times @@ (f @@@ FactorInteger[n]); Array[a, 100] (* _Amiram Eldar_, Sep 07 2020 *)"
			],
			"program": [
				"(PARI) A055231(n)={",
				"   local(a=1);",
				"   f=factor(n) ;",
				"   for(i=1,matsize(f)[1],",
				"         if( f[i,2] ==1, a *=  f[i,1]",
				"         )",
				"   ) ;",
				"   a ;",
				"} /* _R. J. Mathar_, Mar 12 2012 */",
				"(PARI) a(n) = {my(f=factor(n)); for (k=1, #f~, if (f[k,2] \u003e 1, f[k,2] = 0);); factorback(f);} \\\\ _Michel Marcus_, Aug 27 2017",
				"(Scheme, with memoization-macro definec) (definec (A055231 n) (if (= 1 n) 1 (* (if (= 1 (A067029 n)) (A020639 n) 1) (A055231 (A028234 n))))) ;; _Antti Karttunen_, Nov 28 2017"
			],
			"xref": [
				"a(n) = A007913(n)/gcd(A008833(n!), A007913(n!)).",
				"Cf. A008833, A007913, A007947, A000188, A057521, A055773 (computed for n!), A056169 (number of prime divisors), A056671 (number of divisors), A092261 (sum of divisors of the n-th term).",
				"Cf. A005117 (subsequence)."
			],
			"keyword": "nonn,mult",
			"offset": "1,2",
			"author": "_Labos Elemer_, Jun 21 2000",
			"ext": [
				"Name replaced with a simpler description (based on _Henry Bottomley_'s comment) by _Antti Karttunen_, Nov 28 2017"
			],
			"references": 42,
			"revision": 98,
			"time": "2020-09-07T06:29:11-04:00",
			"created": "2000-07-22T03:00:00-04:00"
		}
	]
}