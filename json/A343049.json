{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A343049",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 343049,
			"data": "0,1,2,7,0,5,6,31,0,9,10,31,8,29,30,127,0,1,2,23,0,21,22,127,0,25,26,127,24,125,126,511,0,1,2,39,0,37,38,127,0,41,42,127,40,125,126,511,0,33,34,119,32,117,118,511,32,121,122,511,120,509,510,2047,0",
			"name": "The k-th binary digit of a(n) is the most frequent digit among the first k binary digits of n (in case of a tie, take the k-th binary digit of n).",
			"comment": [
				"Leading zeros are taken into account up to the point the number of zeros exceeds the total number of ones.",
				"We scan the binary representation of a number starting from the least significant digit. See A343271 for the other way."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A343049/b343049.txt\"\u003eTable of n, a(n) for n = 0..8192\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 0 iff n belongs to A036993.",
				"a(n) = n iff n = 0 or n belongs to A032925.",
				"a(2^k-1) = 2^(2*k-1)-1 for any k \u003e 1.",
				"A070939(a(n)) \u003c 2*A070939(n)."
			],
			"example": [
				"The first terms, in decimal and in binary, are:",
				"  n   a(n)  bin(n)  bin(a(n))",
				"  --  ----  ------  ---------",
				"   0     0       0          0",
				"   1     1       1          1",
				"   2     2      10         10",
				"   3     7      11        111",
				"   4     0     100          0",
				"   5     5     101        101",
				"   6     6     110        110",
				"   7    31     111      11111",
				"   8     0    1000          0",
				"   9     9    1001       1001",
				"  10    10    1010       1010",
				"  11    31    1011      11111",
				"  12     8    1100       1000",
				"  13    29    1101      11101",
				"  14    30    1110      11110",
				"  15   127    1111    1111111"
			],
			"program": [
				"(PARI) a(n, base=2) = { my (d=digits(n, base), t, f=vector(base)); d=concat(vector(#d), d); forstep (k=#d, 1, -1, f[1+d[k]]++; if (vecmax(f)==f[1+d[k]], t=d[k];); d[k]=t); fromdigits(d, base) }"
			],
			"xref": [
				"Cf. A032925, A036993, A070939, A343271, A342697."
			],
			"keyword": "nonn,base",
			"offset": "0,3",
			"author": "_Rémy Sigrist_, Apr 09 2021",
			"references": 2,
			"revision": 12,
			"time": "2021-04-10T10:35:25-04:00",
			"created": "2021-04-10T08:07:57-04:00"
		}
	]
}