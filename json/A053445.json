{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A053445",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 53445,
			"data": "1,0,1,0,2,0,3,1,4,2,7,3,10,7,14,11,22,17,32,28,45,43,67,63,95,96,134,139,192,199,269,287,373,406,521,566,718,792,983,1092,1346,1496,1827,2045,2465,2772,3323,3733,4449,5016,5929,6696,7882,8897,10426",
			"name": "Second differences of partition numbers A000041.",
			"comment": [
				"First differences of 0 1 1 2 2 4 4 7 8 12 14 21 24 34 41 55... (A002865).",
				"For n\u003e2, a(n-2) is the number of partitions of n with all parts \u003e 1 and with the largest part occurring more than once. The list of partitions counted begins 22 (so a(2) = 1); 33, 222 (so a(4) = 2); 44, 332, 2222 (so a(6) = 3); 333; 55, 442, 3322, 22222; 443, 3332; 66, 552, 444, 4422, 3333, 33222, 222222; 553, 4432, 33322; ...",
				"a(n) is the number of certain level-n quasi-primary states of a quotient space of certain Verma modules. See the Furlan et al. reference p. 67. - _Wolfdieter Lang_, Apr 25 2003"
			],
			"reference": [
				"L. Comtet, Advanced Combinatorics, Reidel, 1974, p. 115, 3."
			],
			"link": [
				"Andrew van den Hoeven, \u003ca href=\"/A053445/b053445.txt\"\u003eTable of n, a(n) for n = 0..9998\u003c/a\u003e (terms up to a(1000) from T. D. Noe)",
				"Gert Almkvist, \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/aa/aa61/aa6126.pdf\"\u003eOn the differences of the partition function\u003c/a\u003e, Acta Arith., 61.2 (1992), 173-181.",
				"P. Furlan, G. M. Sotkov and I. T. Todorov, \u003ca href=\"http://dx.doi.org/10.1007/BF02742979\"\u003eTwo-Dimensional Conformal Quantum Field Theory\u003c/a\u003e, Rivista d. Nuovo Cimento 12, 6 (1989) 1-202.",
				"Cristiano Husu, \u003ca href=\"https://arxiv.org/abs/1804.09883\"\u003eThe butterfly sequence: the second difference sequence of the numbers of integer partitions with distinct parts, its pentagonal number structure, its combinatorial identities and the cyclotomic polynomials 1-x and 1+x+x^2\u003c/a\u003e, arXiv:1804.09883 [math.NT], 2018."
			],
			"formula": [
				"a(n) ~ exp(sqrt(2*n/3)*Pi)*Pi^2/(24*sqrt(3)*n^2) * (1 + (23*Pi/(24*sqrt(6)) - 3*sqrt(6)/Pi)/sqrt(n) + (625*Pi^2/6912 + 45/(2*Pi^2) - 115/24)/n). - _Vaclav Kotesovec_, Nov 04 2016",
				"G.f.: 1/x - 1/x^2 + ((1 - x)/x^2)*Product_{k\u003e=2} 1/(1 - x^k). - _Ilya Gutkovskiy_, Feb 28 2017"
			],
			"example": [
				"a(8) = 7 - 4 = 3; the corresponding partitions are 44, 332 and 2222."
			],
			"maple": [
				"b:= proc(n, i) option remember; `if`(n=0, 1, `if`(i\u003c1, 0,",
				"       b(n, i-1)+`if`(i\u003en, 0, b(n-i, i))))",
				"    end:",
				"a:= n-\u003e b(n$2) -2*b(n+1$2) +b(n+2$2):",
				"seq(a(n), n=0..80);  # _Alois P. Heinz_, May 19 2014",
				"# alternative Maple program:",
				"P:= [seq(combinat:-numbpart(n),n=0..1002)]:",
				"seq(P[i]-2*P[i+1]+P[i+2],i=1..1001); # _Robert Israel_, Dec 15 2014"
			],
			"mathematica": [
				"Table[(PartitionsP[n+2]-PartitionsP[n+1])-(PartitionsP[n+1]-PartitionsP[n]),{n,0,42}] (* _Vladimir Joseph Stephan Orlovsky_, Apr 23 2008 *)",
				"Differences[Table[ PartitionsP[n], {n, 0, 56}], 2] (* _Jean-François Alcover_, Sep 07 2011 *)",
				"Differences[PartitionsP[Range[0,60]],2] (* _Harvey P. Dale_, Jan 29 2016 *)"
			],
			"program": [
				"(MAGMA) m:=58; S:=[ NumberOfPartitions(n): n in [0..m] ]; [ S[n+2]-2*S[n+1]+S[n]: n in [1..m-2] ]; // _Klaus Brockhaus_, Jun 09 2009",
				"(PARI) lista(nn) = {v = vector(nn, n, numbpart(n-1)); dv = vector(#v-1, n, v[n+1] - v[n]); vector(#dv-1, n, dv[n+1] - dv[n]);} \\\\ _Michel Marcus_, Dec 15 2014"
			],
			"xref": [
				"Cf. A000041, A002865, A072380, A081094, A081095."
			],
			"keyword": "easy,nice,nonn",
			"offset": "0,5",
			"author": "_Alford Arnold_, Jan 12 2000",
			"ext": [
				"More terms from _James A. Sellers_, Feb 02 2000",
				"Start of sequence changed, Apr 25 2003"
			],
			"references": 29,
			"revision": 62,
			"time": "2018-07-18T17:49:35-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}