{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A342514",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 342514,
			"data": "1,1,2,2,4,5,6,8,11,14,18,24,28,35,41,52,64,81,93,115,137,157,190,225,268,313,366,430,502,587,683,790,913,1055,1217,1393,1605,1830,2098,2384,2722,3101,3524,4005,4524,5137,5812,6570,7434,8360,9416,10602,11881",
			"name": "Number of integer partitions of n with distinct first quotients.",
			"comment": [
				"Also the number of reversed integer partitions of n with distinct first quotients.",
				"The first quotients of a sequence are defined as if the sequence were an increasing divisor chain, so for example the first quotients of (6,3,1) are (1/2,1/3)."
			],
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/LogarithmicallyConcaveSequence.html\"\u003e Logarithmically Concave Sequence\u003c/a\u003e.",
				"Gus Wiseman, \u003ca href=\"/A325325/a325325.txt\"\u003eSequences counting and ranking integer partitions by the differences of their successive parts\u003c/a\u003e.",
				"Gus Wiseman, \u003ca href=\"/A069916/a069916.txt\"\u003eSequences counting and ranking partitions and compositions by their differences and quotients\u003c/a\u003e."
			],
			"example": [
				"The partition (4,3,3,2,1) has first quotients (3/4,1,2/3,1/2) so is counted under a(13), but it has first differences (-1,0,-1,-1) so is not counted under A325325(13).",
				"The a(1) = 1 through a(9) = 14 partitions:",
				"  (1)  (2)   (3)   (4)    (5)    (6)    (7)     (8)     (9)",
				"       (11)  (21)  (22)   (32)   (33)   (43)    (44)    (54)",
				"                   (31)   (41)   (42)   (52)    (53)    (63)",
				"                   (211)  (221)  (51)   (61)    (62)    (72)",
				"                          (311)  (321)  (322)   (71)    (81)",
				"                                 (411)  (331)   (332)   (432)",
				"                                        (511)   (422)   (441)",
				"                                        (3211)  (431)   (522)",
				"                                                (521)   (531)",
				"                                                (611)   (621)",
				"                                                (3221)  (711)",
				"                                                        (3321)",
				"                                                        (4311)",
				"                                                        (5211)"
			],
			"mathematica": [
				"Table[Length[Select[IntegerPartitions[n],UnsameQ@@Divide@@@Partition[#,2,1]\u0026]],{n,0,30}]"
			],
			"xref": [
				"The version for differences instead of quotients is A325325.",
				"The ordered version is A342529.",
				"The strict case is A342520.",
				"The Heinz numbers of these partitions are A342521.",
				"A000005 counts constant partitions.",
				"A000009 counts strict partitions.",
				"A000041 counts partitions.",
				"A001055 counts factorizations (strict: A045778, ordered: A074206).",
				"A003238 counts chains of divisors summing to n - 1 (strict: A122651).",
				"A167865 counts strict chains of divisors \u003e 1 summing to n.",
				"A342096 counts partitions with all adjacent parts x \u003c 2y (strict: A342097).",
				"A342098 counts partitions with all adjacent parts x \u003e 2y.",
				"Cf. A000837, A002843, A003242, A175342, A318991, A318992, A325557, A342527, A342528, A342529."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Gus Wiseman_, Mar 17 2021",
			"references": 7,
			"revision": 7,
			"time": "2021-03-22T15:00:51-04:00",
			"created": "2021-03-22T15:00:51-04:00"
		}
	]
}