{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A009968",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 9968,
			"data": "1,24,576,13824,331776,7962624,191102976,4586471424,110075314176,2641807540224,63403380965376,1521681143169024,36520347436056576,876488338465357824,21035720123168587776,504857282956046106624,12116574790945106558976,290797794982682557415424,6979147079584381377970176,167499529910025153071284224,4019988717840603673710821376",
			"name": "Powers of 24: a(n) = 24^n.",
			"comment": [
				"If X_1, X_2, ..., X_n is a partition of the set {1, 2, ..., 2*n} into blocks of size 2 then, for n \u003e= 1, a(n) is equal to the number of functions f : {1, 2, ..., 2*n} -\u003e {1, 2, 3, 4, 5} such that for fixed y_1, y_2, ..., y_n in {1, 2, 3, 4, 5} we have f(X_i) \u003c\u003e {y_i}, (i = 1, 2, ..., n). - _Milan Janjic_, May 24 2007",
				"The compositions of n in which each natural number is colored by one of p different colors are called p-colored compositions of n. For n \u003e= 1, a(n) equals the number of 24-colored compositions of n such that no adjacent parts have the same color. - _Milan Janjic_, Nov 17 2011"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A009968/b009968.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e",
				"Milan Janjic, \u003ca href=\"http://www.pmfbl.org/janjic/\"\u003eEnumerative Formulas for Some Functions on Finite Sets\u003c/a\u003e",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_01\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (24)."
			],
			"formula": [
				"G.f.: 1/(1 - 24*x). - _Philippe Deléham_, Nov 23 2008",
				"E.g.f.: exp(24x). - _Zerinvary Lajos_, Apr 29 2009",
				"a(n) = 24^n; a(n) = 24*a(n-1) for n \u003e 0, a(0) = 1. - _Vincenzo Librandi_, Nov 21 2010",
				"a(n) = det(|s(i + 4, j)|, 1 \u003c= i, j \u003c= n), where s(n, k) are Stirling numbers of the first kind. - _Mircea Merca_, Apr 04 2013"
			],
			"mathematica": [
				"NestList[24#\u0026, 1, 20] (* _Harvey P. Dale_, Feb 04 2017 *)"
			],
			"program": [
				"(Sage) [lucas_number1(n,24,0) for n in range(1, 17)]# - _Zerinvary Lajos_, Apr 29 2009",
				"(MAGMA)[24^n: n in [0..100]] - _Vincenzo Librandi_, Nov 21 2010",
				"(PARI) a(n)=24^n \\\\ _Charles R Greathouse IV_, Sep 24 2015",
				"(Scala) LazyList.iterate(1: BigInt)(_ * 24).take(24).toList // _Alonso del Arte_, Apr 24 2020",
				"(Python) [24**n for n in range(21)] # _Michael S. Branicky_, Jan 24 2021"
			],
			"xref": [
				"Column k = 4 of A225816."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 24,
			"revision": 54,
			"time": "2021-12-27T21:36:30-05:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}