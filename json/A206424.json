{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A206424",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 206424,
			"data": "1,2,2,2,4,4,2,4,5,2,4,4,4,8,8,4,8,10,2,4,5,4,8,10,5,10,14,2,4,4,4,8,8,4,8,10,4,8,8,8,16,16,8,16,20,4,8,10,8,16,20,10,20,28,2,4,5,4,8,10,5,10,14,4,8,10,8,16,20,10,20,28,5,10,14,10,20,28",
			"name": "The number of 1's in row n of Pascal's Triangle (mod 3)",
			"comment": [
				"A006047(n) = a(n) + A227428(n).",
				"a(n) = n + 1 - A062296(n) - A227428(n); number of ones in row n of triangle A083093. - _Reinhard Zumkeller_, Jul 11 2013"
			],
			"link": [
				"Reinhard Zumkeller (terms 0..1000) \u0026 Antti Karttunen, \u003ca href=\"/A206424/b206424.txt\"\u003eTable of n, a(n) for n = 0..19683\u003c/a\u003e",
				"R. Garfield, H. S. Wilf, \u003ca href=\"https://dx.doi.org/10.1016/0022-314X(92)90078-4\"\u003eThe distribution of the binomial coefficients modulo p\u003c/a\u003e, J. Numb. Theory 41 (1) (1992) 1-5",
				"Marcus Jaiclin, et al. \u003ca href=\"https://web.archive.org/web/20170823000349/http://pyrrho.wsc.ma.edu/math/faculty/jaiclin/writings/research/pascals_triangle/\"\u003ePascal's Triangle, Mod 2,3,5\u003c/a\u003e",
				"D. L. Wells, \u003ca href=\"http://dx.doi.org/10.1007/978-94-009-0223-7_42\"\u003eResidue counts modulo three for the fibonacci triangle\u003c/a\u003e, Appl. Fib. Numbers, Proc. 6th Int Conf Fib. Numbers, Pullman, 1994 (1996) 521-536.",
				"Avery Wilson, \u003ca href=\"http://www.appliedprobability.org/data/files/MS%20issues/Vol47_No2.pdf\"\u003ePascal's Triangle Modulo 3\u003c/a\u003e, Mathematics Spectrum, 47-2 - January 2015, pp. 72-75."
			],
			"formula": [
				"From _Antti Karttunen_, Jul 27 2017: (Start)",
				"a(n) = (3^k + 1)*2^(y-1), where y = A062756(n) and k = A081603(n). [See e.g. Wells or Wilson references.]",
				"a(n) = A006047(n) - A227428(n).",
				"(End)",
				"From _David A. Corneth_ and _Antti Karttunen_, Jul 27 2017: (Start)",
				"Based on the first formula above, we have following identities:",
				"a(3n) = a(n).",
				"a(3n+1) = 2*a(n).",
				"a(9n+4) = 4*a(n).",
				"(End)",
				"a(n) = (1/2)*Sum_{k = 0..n} mod(C(n,k) + C(n,k)^2, 3). - _Peter Bala_, Dec 17 2020"
			],
			"example": [
				"Example: Rows 0-8 of Pascal's Triangle (mod 3) are:",
				"1                   So a(0) = 1",
				"1 1                 So a(1) = 2",
				"1 2 1               So a(2) = 2",
				"1 0 0 1                 .",
				"1 1 0 1 1               .",
				"1 2 1 1 2 1             .",
				"1 0 0 2 0 0 1",
				"1 1 0 2 2 0 1 1",
				"1 2 1 2 1 2 1 2 1"
			],
			"mathematica": [
				"Table[Count[Mod[Binomial[n, Range[0, n]], 3], 1], {n, 0, 99}] (* _Alonso del Arte_, Feb 07 2012 *)"
			],
			"program": [
				"(Haskell)",
				"a206424 = length . filter (== 1) . a083093_row",
				"-- _Reinhard Zumkeller_, Jul 11 2013",
				"(PARI) A206424(n) = sum(k=0,n,1==(binomial(n,k)%3)); \\\\ (naive way) _Antti Karttunen_, Jul 26 2017",
				"(Scheme) (define (A206424 n) (* (+ (A000244 (A081603 n)) 1) (A000079 (- (A062756 n) 1)))) ;; (fast way) _Antti Karttunen_, Jul 27 2017"
			],
			"xref": [
				"Cf. A083093, A062296, A006047, A062756, A081603, A206427, A227428."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Marcus Jaiclin_, Feb 07 2012",
			"references": 9,
			"revision": 53,
			"time": "2020-12-18T04:06:57-05:00",
			"created": "2012-02-08T12:21:13-05:00"
		}
	]
}