{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097018",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97018,
			"data": "3,2,8,4,43,9,67,37,137,173,16,73,163,257,281,211,353,169,401,283,256,157,331,1024,193,1009,617,641,653,677,64,523,547,277,1489,1811,313,977,1669,691,1789,1447,4201,1543,787,397,421,1783,907,457,3727,1433,3373",
			"name": "a(n) is the least number such that sigma(a(n)) is divisible by the n-th prime.",
			"comment": [
				"Note that a(n) always exists, because sigma(2^(p-2)) = 2^(p-1)-1 is divisible by p for p\u003e2 (by Fermat's little theorem), so there is always a candidate for a(n). Compare A227470, A272349. - _N. J. A. Sloane_, May 01 2016"
			],
			"link": [
				"Donovan Johnson, \u003ca href=\"/A097018/b097018.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"n=11: a(11)=16 is the least number x such that sigma(x) is divisible by the 11th prime, 31."
			],
			"mathematica": [
				"ln[n_]:=Module[{x=1,p=Prime[n]},While[!Divisible[DivisorSigma[ 1,x], p],x++];x]; Array[ln,60] (* _Harvey P. Dale_, Sep 07 2014 *)",
				"Module[{nn=5000,ds},ds=DivisorSigma[1,Range[nn]];Table[Position[ds,_?(Divisible[#,n]\u0026),1,1],{n,Prime[Range[60]]}]]//Flatten (* Much faster than the first program *) (* _Harvey P. Dale_, May 18 2018 *)"
			],
			"program": [
				"(PARI) sigma_hunt(x)=local(n=0,g);while(n++,g=sigma(n);if(g%x,,return(n)));",
				"for(x=1,50,print1(sigma_hunt(prime(x))\", \")) /* _Phil Carmody_, Mar 01 2013 */",
				"(MAGMA) sol:=[]; p:=PrimesUpTo(10000); for n in [1..53] do k:=2; while Max(PrimeDivisors(SumOfDivisors(k))) ne p[n] do k:=k+1; end while; sol[n]:=k; end for; sol; // _Marius A. Burtea_, Jun 05 2019"
			],
			"xref": [
				"Cf. A000203, A000040, A272349, A227470."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Labos Elemer_, Aug 23 2004",
			"references": 4,
			"revision": 31,
			"time": "2019-06-05T11:28:59-04:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}