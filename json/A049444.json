{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A049444",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 49444,
			"data": "1,-2,1,6,-5,1,-24,26,-9,1,120,-154,71,-14,1,-720,1044,-580,155,-20,1,5040,-8028,5104,-1665,295,-27,1,-40320,69264,-48860,18424,-4025,511,-35,1,362880,-663696,509004,-214676,54649,-8624,826,-44,1,-3628800,6999840,-5753736",
			"name": "Generalized Stirling number triangle of first kind.",
			"comment": [
				"a(n,m) = ^2P_n^m in the notation of the given reference with a(0,0) := 1. The monic row polynomials s(n,x) := Sum_{m=0..n} a(n,m)*x^m which are s(n,x) = Product_{k=0..n-1} (x-(2+k)), n \u003e= 1 and s(0,x)=1 satisfy s(n,x+y) = Sum_{k=0..n} binomial(n,k)*s(k,x)*S1(n-k,y), with the Stirling1 polynomials S1(n,x) = Sum_{m=1..n} (A008275(n,m)*x^m) and S1(0,x)=1.",
				"In the umbral calculus (see the S. Roman reference given in A048854) the s(n,x) polynomials are called Sheffer polynomials for (exp(2*t), exp(t)-1).",
				"See A143491 for the unsigned version of this array and A143494 for the inverse. - _Peter Bala_, Aug 25 2008",
				"Corresponding to the generalized Stirling number triangle of second kind A137650. - _Peter Luschny_, Sep 18 2011",
				"Unsigned, reversed rows (cf. A145324, A136124) are the dimensions of the cohomology of a complex manifold with a symmetric group (S_n) action. See p. 17 of the Hyde and Lagarias link. See also the Murri link for an interpretation as the Betti numbers of the moduli space M(0,n) of smooth Riemann surfaces. - _Tom Copeland_, Dec 09 2016"
			],
			"reference": [
				"Mitrinovic, D. S.; Mitrinovic, R. S.; Tableaux d'une classe de nombres relies aux nombres de Stirling. Univ. Beograd. Pubi. Elektrotehn. Fak. Ser. Mat. Fiz. No. 77 1962, 77 pp.",
				"Y. Manin, Frobenius Manifolds, Quantum Cohomology and Moduli Spaces, American Math. Soc. Colloquium Publications Vol. 47, 1999 [From _Tom Copeland_, Jun 29 2008]"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A049444/b049444.txt\"\u003eRows n = 0..125 of triangle, flattened\u003c/a\u003e",
				"E. Getzler, \u003ca href=\"http://arxiv.org/abs/alg-geom/9411004\"\u003eOperads and moduli spaces of genus 0 Riemann surfaces\u003c/a\u003e, arXiv:alg-geom/9411004, 1994, (see p. 23, g(x,t)). [From _Tom Copeland_, Dec 11 2011]",
				"T. Hyde and J. Lagarias \u003ca href=\"http://arxiv.org/abs/1604.05359\"\u003ePolynomial splitting measures and cohomology of the pure braid group\u003c/a\u003e, arXiv preprint arXiv:1604.05359 [math.RT], 2016.",
				"Y. Manin, \u003ca href=\"http://arxiv.org/abs/alg-geom/9407005\"\u003eGenerating functions in algebraic geometry and sums over trees\u003c/a\u003e, arXiv:alg-geom/9407005, 1994, (Eqn. 0.7 and 1.7). [From _Tom Copeland_, Dec 10 2011]",
				"R. Murri, \u003ca href=\"http://arxiv.org/abs/1202.1820\"\u003eFatgraph Algorithms and the Homology of the Kontsevich Complex\u003c/a\u003e, arXiv:1202.1820 [math.AG], 2012, (see Table 1, p. 3). [From _Tom Copeland_, Sep 18 2012]"
			],
			"formula": [
				"a(n, m) = a(n-1, m-1) -(n+1)*a(n-1, m), n \u003e= m \u003e= 0; a(n, m) = 0, n \u003c m; a(n, -1) = 0, a(0, 0) = 1.",
				"E.g.f. for m-th column of signed triangle: ((log(1+x))^m)/(m!*(1+x)^2).",
				"Triangle (signed) = [-2, -1, -3, -2, -4, -3, -5, -4, -6, -5, ...] DELTA [1, 0, 1, 0, 1, 0, 1, 0, ...]; triangle (unsigned) = [2, 1, 3, 2, 4, 3, 5, 4, 6, 5, ...] DELTA [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, ...], where DELTA is Deléham's operator defined in A084938 (unsigned version in A143491).",
				"E.g.f.: (1+y)^(x-2). - _Vladeta Jovovic_, May 17 2004",
				"With P(n,t) = Sum_{j=0..n-2} a(n-2,j) * t^j and P(1,t) = -1 and P(0,t) = 1, then G(x,t) = -1 + exp[P(.,t)*x] = [(1+x)^t - 1 - t^2 * x] / [t(t-1)], whose compositional inverse in x about 0 is given in A074060. G(x,0) = -log(1+x) and G(x,1) = (1+x) log(1+x) - 2x. G(x,q^2) occurs in formulas on pages 194-196 of the Manin reference. - _Tom Copeland_, Feb 17 2008",
				"If we define f(n,i,a) = Sum_{k=0..n-i} binomial(n,k)*Stirling1(n-k,i)*Product_{j=0..k-1} (-a-j), then T(n,i) = f(n,i,2), for n=1,2,...; i=0..n. - _Milan Janjic_, Dec 21 2008"
			],
			"example": [
				"First few rows of the triangle are:",
				"              1;",
				"          -2,    1;",
				"        6,   -5,    1;",
				"   -24,   26,   -9,    1;",
				"120, -154,   71,  -14,    1;"
			],
			"maple": [
				"A049444_row := proc(n) local k,i;",
				"add(add(combinat[stirling1](n, n-i), i=0..k)*x^(n-k-1),k=0..n-1);",
				"seq(coeff(%,x,k),k=1..n-1) end:",
				"seq(print(A049444_row(n)),n=1..7); # _Peter Luschny_, Sep 18 2011"
			],
			"mathematica": [
				"t[n_, i_] = Sum[(-1)^k*Binomial[n, k]*(k+1)!*StirlingS1[n-k, i], {k, 0, n-i}]; Flatten[Table[t[n, i], {n, 0, 9}, {i, 0, n}]] [[1 ;; 48]]",
				"(* _Jean-François Alcover_, Apr 29 2011, after _Milan Janjic_ *)"
			],
			"program": [
				"(Haskell)",
				"a049444 n k = a049444_tabl !! n !! k",
				"a049444_row n = a049444_tabl !! n",
				"a049444_tabl = map fst $ iterate (\\(row, i) -\u003e",
				"   (zipWith (-) ([0] ++ row) $ map (* i) (row ++ [0]), i + 1)) ([1], 2)",
				"-- _Reinhard Zumkeller_, Mar 11 2014"
			],
			"xref": [
				"Unsigned column sequences are A000142(n+1), A001705-A001709. Row sums (signed triangle): n!*(-1)^n, row sums (unsigned triangle): A001710(n-2). Cf. A008275 (Stirling1 triangle).",
				"Cf. A000035 A084938, A094645, A094646.",
				"Cf. A143491, A143494. - _Peter Bala_, Aug 25 2008",
				"Cf. A137650.",
				"Cf. A136124."
			],
			"keyword": "sign,easy,tabl,nice",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_",
			"ext": [
				"Second formula corrected by _Philippe Deléham_, Nov 09 2008"
			],
			"references": 15,
			"revision": 73,
			"time": "2017-10-08T17:40:20-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}