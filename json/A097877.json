{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097877",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97877,
			"data": "1,1,1,1,1,4,1,12,1,1,34,7,1,98,32,1,1,294,124,10,1,919,448,61,1,1,2974,1576,298,13,1,9891,5510,1294,99,1,1,33604,19322,5260,583,16,1,116103,68206,20595,2960,146,1,1,406614,242602,78954,13704,1006,19,1,1440025",
			"name": "Triangle read by rows: T(n,k) is the number of Dyck n-paths with k large components, 0 \u003c= k \u003c= n/2.",
			"comment": [
				"A prime Dyck path is one with exactly one return to ground level. Every nonempty Dyck path decomposes uniquely as a concatenation of prime Dyck paths, called its components. For example, UUDDUD has 2 components: UUDD and UD, of semilength 2 and 1 respectively. A large component is one of semilength \u003e= 2."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A097877/b097877.txt\"\u003eRows n = 0..200, flattened\u003c/a\u003e"
			],
			"formula": [
				"G.f.: 2/(1 + t*(1 - 4*z)^(1/2) + (1 - 2*z)(1-t)) = Sum_{n\u003e=0, k\u003e=0} T(n, k) z^n t^k satisfies (1-z)*G = 1 + z*t*(CatalanGF[z]-1)*G. The gf for Dyck paths (A000108) with z marking semilength is CatalanGF[z]:=(1 - sqrt[1 - 4*z])/(2*z). Hence the gf for prime Dyck paths is z*CatalanGF[z] and the gf for non-UD prime Dyck paths is S(z):= z*CatalanGF[z]-z. For fixed k, the gf for (T(n, k))_{n\u003e=0} is S(z)^k/(1-z)^(k+1). This is clear because 1/(1-z) is the gf for all-UD Dyck paths (including the empty one) and a Dyck path with k large components is a product (uniquely) of an all-UD, a non-UD prime, an all-UD, a non-UD prime, ... with k non-UD primes and k+1 all-UDs."
			],
			"example": [
				"T(3,1) = 4 because each of the 5 Dyck paths of semilength 3 has one large component except for UDUDUD, which has none.",
				"Table begins:",
				"  \\ k 0, 1, 2, ...",
				"  n\\ ____________________",
				"  0 | 1;",
				"  1 | 1;",
				"  2 | 1,   1;",
				"  3 | 1,   4;",
				"  4 | 1,  12,   1;",
				"  5 | 1,  34,   7;",
				"  6 | 1,  98,  32,  1;",
				"  7 | 1, 294, 124, 10;",
				"  8 | 1, 919, 448, 61, 1;",
				"  ..."
			],
			"xref": [
				"The Fine distribution (A065600) counts Dyck paths by number of small components (= number of low peaks)."
			],
			"keyword": "nonn,tabf",
			"offset": "0,6",
			"author": "_David Callan_, Sep 21 2004",
			"references": 1,
			"revision": 8,
			"time": "2021-09-23T14:08:04-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}