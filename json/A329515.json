{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A329515",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 329515,
			"data": "1,1,2,1,3,2,1,3,6,2,1,3,6,7,2,1,3,6,9,6,2,1,3,6,9,12,6,2,1,3,6,9,12,12,6,2,1,3,6,9,12,15,10,6,2,1,3,6,9,12,15,18,10,6,2,1,3,6,9,12,15,18,17,10,6,2,1,3,6,9,12,15,18,21,14,10,6,2",
			"name": "Array read by upward antidiagonals: row n = coordination sequence for cylinder formed by rolling up a strip of width 2*n+1 hexagons cut from the hexagonal grid by cuts perpendicular to grid lines.",
			"comment": [
				"The width of the strip is a little harder to define here. In the illustration for n=2, the strip is five hexagons wide if measured along hexagons that touch edge-to-edge. A path joining two vertices to be identified when the cylinder is formed has length 4n+2 edges (10 edges in the illustration for n=2).",
				"Because the width 2*n+1 is odd, one edge of the cut has to be displaced sideways (by half the width of a hexagon) in order for the two edges to mesh properly.  The arrows in the figures indicate pairs of points which will coalesce.",
				"For the case when the strip is 2*n hexagons wide see A329512.",
				"For the case when the cuts are parallel to the grid lines, see A329508.",
				"See A329501 and A329504 for coordination sequences for cylinders formed by rolling up the square grid."
			],
			"link": [
				"Chaim Goodman-Strauss and N. J. A. Sloane, \u003ca href=\"https://doi.org/10.1107/S2053273318014481\"\u003eA Coloring Book Approach to Finding Coordination Sequences\u003c/a\u003e, Acta Cryst. A75 (2019), 121-134, also \u003ca href=\"http://NeilSloane.com/doc/Cairo_final.pdf\"\u003eon NJAS's home page\u003c/a\u003e. Also \u003ca href=\"http://arxiv.org/abs/1803.08530\"\u003earXiv:1803.08530\u003c/a\u003e.",
				"N. J. A. Sloane, \u003ca href=\"/A329515/a329515.pdf\"\u003eIllustration for row n = 0\u003c/a\u003e, showing vertices of cylinder of width (or circumference) 1 labeled with distance from base point 0. The cylinder is formed by identifying the black lines. Arrows indicate two points which will coalesce.",
				"N. J. A. Sloane, \u003ca href=\"/A329515/a329515_1.pdf\"\u003eIllustration for row n = 1\u003c/a\u003e, showing vertices of cylinder of width (or circumference) 3 labeled with distance from base point 0. The cylinder is formed by identifying the black lines. Arrows indicate two points which will coalesce.",
				"N. J. A. Sloane, \u003ca href=\"/A329515/a329515_2.pdf\"\u003eIllustration for row n = 2\u003c/a\u003e, showing vertices of cylinder of width (or circumference) 5 labeled with distance from base point 0. The cylinder is formed by identifying the black lines. Arrows indicate two points which will coalesce.",
				"\u003ca href=\"/index/Con#coordination_sequences\"\u003eIndex entries for coordination sequences\u003c/a\u003e"
			],
			"formula": [
				"The g.f.s for the rows could be found using the \"trunks and branches\" method (see Goodman-Strauss and Sloane), as was done in A329508. This step has not yet been carried out, so the following g.f. is at present only conjectural.",
				"The g.f. G(n) for row n (n\u003e=0) is (strongly) conjectured to be",
				"(1/(1-x))*(1 + 2*x + 3*x^2*(1-x^(2*n-1))/(1-x) - (n-2)*x^(2*n+1) - n*x^(2*n+2)).",
				"The values of G(0) through G(6) (certified by MAGMA) are:",
				"(1 + x)/(1 - x),",
				"(x^4 - x^3 - 3*x^2 - 2*x - 1)/(x - 1),",
				"(2*x^6 - 3*x^4 - 3*x^3 - 3*x^2 - 2*x - 1)/(x - 1),",
				"(3*x^8 + x^7 - 3*x^6 - 3*x^5 - 3*x^4 - 3*x^3 - 3*x^2 - 2*x - 1)/(x - 1),",
				"(4*x^10 + 2*x^9 - 3*x^8 - 3*x^7 - 3*x^6 - 3*x^5 - 3*x^4 - 3*x^3 - 3*x^2 - 2*x - 1)/(x - 1),",
				"(5*x^12 + 3*x^11 - 3*x^10 - 3*x^9 - 3*x^8 - 3*x^7 - 3*x^6 - 3*x^5 - 3*x^4 - 3*x^3 - 3*x^2 - 2*x - 1)/(x - 1),",
				"(6*x^14 + 4*x^13 - 3*x^12 - 3*x^11 - 3*x^10 - 3*x^9 - 3*x^8 - 3*x^7 - 3*x^6 - 3*x^5 - 3*x^4 - 3*x^3 - 3*x^2 - 2*x - 1)/(x - 1).",
				"Note that row n is equal to 4*n+2 once the (2*n+2)-nd term has been reached.",
				"The g.f.s for the rows can also be obtained by regarding the 1-skeleton of the cylinder as the Cayley diagram for an appropriate group H, and computing the growth function for H (see the MAGMA code)."
			],
			"example": [
				"Array begins:",
				"1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, ...",
				"1, 3, 6, 7, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, ...",
				"1, 3, 6, 9, 12, 12, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, ...",
				"1, 3, 6, 9, 12, 15, 18, 17, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, ...",
				"1, 3, 6, 9, 12, 15, 18, 21, 24, 22, 18, 18, 18, 18, 18, 18, 18, 18, ...",
				"1, 3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 27, 22, 22, 22, 22, 22, 22, ...",
				"1, 3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 32, 26, 26, 26, 26, ...",
				"1, 3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 39, 42, 37, 30, 30, 30, 30, ...",
				"1, 3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 39, 42, 45, 48, 42, 34, 34, 34, 34, 34, 34, 34, ...",
				"1, 3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 39, 42, 45, 48, 51, 54, 47, 38, 38, 38, 38, 38, ...",
				"1, 3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 39, 42, 45, 48, 51, 54, 57, 60, 52, 42, 42, 42, ...",
				"...",
				"The initial antidiagonals are:",
				"1,",
				"1, 2,",
				"1, 3, 2,",
				"1, 3, 6, 2,",
				"1, 3, 6, 7, 2,",
				"1, 3, 6, 9, 6, 2,",
				"1, 3, 6, 9, 12, 6, 2,",
				"1, 3, 6, 9, 12, 12, 6, 2,",
				"1, 3, 6, 9, 12, 15, 10, 6, 2,",
				"1, 3, 6, 9, 12, 15, 18, 10, 6, 2,",
				"1, 3, 6, 9, 12, 15, 18, 17, 10, 6, 2,",
				"1, 3, 6, 9, 12, 15, 18, 21, 14, 10, 6, 2,",
				"1, 3, 6, 9, 12, 15, 18, 21, 24, 14, 10, 6, 2,",
				"..."
			],
			"program": [
				"(MAGMA)",
				"n := 2; \\\\ set n",
				"R\u003cx\u003e := RationalFunctionField(Integers());",
				"FG3\u003cR, S, T\u003e := FreeGroup(3);",
				"Q3 := quo\u003cFG3| R^2, S^2, T^2, R*S*T = T*S*R, (S*T*S*R)^n*S*R \u003e;",
				"H := AutomaticGroup(Q3);",
				"f3 := GrowthFunction(H);",
				"PSR := PowerSeriesRing(Integers():Precision := 60);",
				"Coefficients(PSR!f3);",
				"// 1, 3, 6, 9, 12, 12, 10, 10, 10, 10, 10, 10, ... (row n)",
				"f3; // G(n)",
				"// (2*x^6 - 3*x^4 - 3*x^3 - 3*x^2 - 2*x - 1)/(x - 1)"
			],
			"xref": [
				"Rows 0,1,2,3 are A040000, A329516, A329517, A329771.",
				"Cf. A008486, A329501-A329514."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_, Nov 25 2019",
			"references": 7,
			"revision": 27,
			"time": "2020-01-19T18:13:17-05:00",
			"created": "2019-11-25T18:20:16-05:00"
		}
	]
}