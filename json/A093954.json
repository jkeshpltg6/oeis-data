{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A093954",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 93954,
			"data": "1,1,1,0,7,2,0,7,3,4,5,3,9,5,9,1,5,6,1,7,5,3,9,7,0,2,4,7,5,1,5,1,7,3,4,2,4,6,5,3,6,5,5,4,2,2,3,4,3,9,2,2,5,5,5,7,7,1,3,4,8,9,0,1,7,3,9,1,0,8,6,9,8,2,7,4,8,6,8,4,7,7,6,4,3,8,3,1,7,3,3,6,9,1,1,9,1,3,0,9,3,4",
			"name": "Decimal expansion of Pi/(2*sqrt(2)).",
			"comment": [
				"The value is the length Pi*sqrt(2)/4 of the diagonal in the square with side length Pi/4 = Sum_{n\u003e=0} (-1)^n/(2n+1) = A003881. The area of the circumcircle of this square is Pi*(Pi*sqrt(2)/8)^2 = Pi^3/32 = A153071. - _Eric Desbiaux_, Jan 18 2009",
				"This is the value of the Dirichlet L-function of modulus m=8 at argument s=1 for the non-principal character (1,0,1,0,-1,0,-1,0). See arXiv:1008.2547. - _R. J. Mathar_, Mar 22 2011",
				"Also equals integral_{0, infinity} 1/(x^4+1) dx. - _Jean-François Alcover_, Apr 29 2013",
				"Archimedes's-like scheme: set p(0) = sqrt(2), q(0) = 1; p(n+1) = 2*p(n)*q(n)/(p(n)+q(n)) (harmonic mean, i.e., 1/p(n+1) = (1/p(n) + 1/q(n))/2), q(n+1) = sqrt(p(n+1)*q(n)) (geometric mean, i.e., log(q(n+1)) = (log(p(n+1)) + log(q(n)))/2), for n \u003e= 0. The error of p(n) and q(n) decreases by a factor of approximately 4 each iteration, i.e., approximately 2 bits are gained by each iteration. Set r(n) = (2*q(n) + p(n))/3, the error decreases by a factor of approximately 16 for each iteration, i.e., approximately 4 bits are gained by each iteration. For a similar scheme see also A244644. - _A.H.M. Smeets_, Jul 12 2018",
				"The area of a circle circumscribing a unit-area regular octagon. - _Amiram Eldar_, Nov 05 2020"
			],
			"reference": [
				"George Boros and Victor H. Moll, Irresistible integrals, Cambridge University Press (2006), p. 149.",
				"L. B. W. Jolley, Summation of Series, Dover (1961), eq 76 page 16."
			],
			"link": [
				"Harry J. Smith, \u003ca href=\"/A093954/b093954.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e",
				"J. M. Borwein, P. B. Borwein, and K. Dilcher, \u003ca href=\"http://www.jstor.org/stable/2324715\"\u003ePi, Euler numbers and asymptotic expansions\u003c/a\u003e, Amer. Math. Monthly, 96 (1989), 681-687.",
				"R. J. Mathar, \u003ca href=\"http://arxiv.org/abs/1008.2547\"\u003eTable of Dirichlet L-series and prime zeta modulo functions for small moduli\u003c/a\u003e, arXiv:1008.2547 [math.NT], 2010-2015, table 7 and section 2.2, value of L(m=8,r=4,s=1).",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Bifoliate.html\"\u003eBifoliate\u003c/a\u003e.",
				"\u003ca href=\"/index/Tra#transcendental\"\u003eIndex entries for transcendental numbers\u003c/a\u003e"
			],
			"formula": [
				"From _Peter Bala_, Feb 05 2015: (Start)",
				"Pi/(2*sqrt(2)) = Sum_{k \u003e= 0} binomial(2*k,k)*1/(2*k + 1)*(1/8)^k.",
				"The integer sequences A(n) := 2^n*(2*n + 1)! and B(n) := A(n)*( Sum {k = 0..n} binomial(2*k,k)*1/(2*k + 1)*(1/8)^k ) both satisfy the second order recurrence equation u(n) = (12*n^2 + 1)*u(n-1) - 4*(n - 1)*(2*n - 1)^3*u(n-2). From this observation we can obtain the continued fraction expansion Pi/(2*sqrt(2)) = 1 + 1/(12 - 4*3^3/(49 - 4*2*5^3/(109 - 4*3*7^3/(193 - ... - 4*(n - 1)*(2*n - 1)^3/((12*n^2 + 1) - ... ))))). Cf. A002388 and A019670. (End)",
				"From _Peter Bala_, Mar 03 2015: (Start)",
				"Pi/(2*sqrt(2)) = Sum_{k \u003e= 0} (-1)^floor(k/2)/(2*k + 1) = limit (n -\u003e infinity) Sum_{k = -n .. n - 1} (-1)^k/(4*k + 1).",
				"We conjecture the asymptotic expansion Pi/(2*sqrt(2)) - Sum {k = 0..n - 1} (-1)^floor(k/2)/(2*k + 1) ~ 1/(2*n) - 3/(2*n)^3 + 57/(2*n)^5 - 2763/(2*n)^7 + ..., where n is a multiple of 4 and the sequence of unsigned coefficients [1, 3, 57, 2763, ...] is A000281. An example with n = 5000 is given below. (End)",
				"From _Peter Bala_, Sep 21 2016: (Start)",
				"c = 2 * Sum_{k \u003e= 0} (-1)^k * (4*k + 2)/((4*k + 1)*(4*k + 3)) = A181048 + A181049. The asymptotic expansion conjectured above follows from the asymptotic expansions given in A181048 and A181049.",
				"c = 1/2 * Integral_{x = 0..Pi/2} sqrt(tan(x)) dx. (End)",
				"From _Peter Bala_, Nov 24 2016: (Start)",
				"Let m be an odd integer and n a nonnegative integer. Then Pi/(2*sqrt(2)) = 2^n*m^(2*n)*(2*n)!*Sum_{k \u003e= 0} (-1)^(n+floor(k/2)) * 1/Product_{j = -n..n} (2*k + 1 + 2*m*j). Cf. A003881.",
				"In the particular case m = 1 the result has the equivalent form: for n a nonnegative integer, Pi/(2*sqrt(2)) = 2^n*(2*n)!*Sum_{k \u003e= 0} (-1)^(n+k)*(8*k + 4)* 1/Product_{j = -n..n+1} (4*k + 2*j + 1). The case m = 1, n = 1 is considered in the Example section below.",
				"Let m be an odd integer and n a nonnegative integer. Then Pi/(2*sqrt(2)) = 4^n*m^(2*n)*(2*n)!*Sum_{k \u003e= 0} (-1)^(n+floor(k/2)) * 1/Product_{j = -n..n} (2*k + 1 + 4*m*j). (End)",
				"Equals Integral_{x = 0..inf} cosh(x)/cosh(2*x) dx. - _Peter Bala_, Nov 01 2019",
				"Equals Sum_{k\u003e=1} A188510(k)/k = Sum_{k\u003e=1} Kronecker(-8,k)/k = 1 + 1/3 - 1/5 - 1/7 + 1/9 + 1/11 - 1/13 - 1/15 + ... - _Jianing Song_, Nov 16 2019",
				"From _Amiram Eldar_, Jul 16 2020: (Start)",
				"Equals Product_{k\u003e=1} (1 - (-1)^k/(2*k+1)).",
				"Equals Integral_{x=0..oo} dx/(x^2 + 2).",
				"Equals Integral_{x=0..Pi/2} dx/(sin(x)^2 + 1). (End)"
			],
			"example": [
				"1.11072073... = 1/A112628.",
				"From _Peter Bala_, Mar 03 2015: (Start)",
				"Asymptotic expansion at n = 5000.",
				"The truncated series Sum_{k = 0..5000 - 1} (-1)^floor(k/2)/(2*k + 1) = 1.110(6)207345(42)591561(18)3970(5238)1.... The bracketed digits show where this decimal expansion differs from that of Pi/(2*sqrt(2)). The numbers 1, -3, 57, -2763 must be added to the bracketed numbers to give the correct decimal expansion to 30 digits: Pi/(2*sqrt(2)) = 1.110(7)207345(39)591561(75)3970 (2475)1.... (End)",
				"From _Peter Bala_, Nov 24 2016: (Start)",
				"Case m = 1, n = 1:",
				"Pi/(2*sqrt(2)) = 4*Sum_{k \u003e= 0} (-1)^(1 + floor(k/2))/((2*k - 1)*(2*k + 1)*(2*k + 3)).",
				"We appear to have the following asymptotic expansion for the tails of this series: for N divisible by 4, Sum_{k \u003e= N/2} (-1)^floor(k/2)/((2*k - 1)*(2*k + 1)*(2*k + 3)) ~ 1/N^3 - 14/N^5 + 691/N^7 - 62684/N^9 - ..., where the coefficient sequence [1, 0, -14, 0, 691, 0, -62684, ...] appears to come from the e.g.f. (1/2!)*cosh(x)/cosh(2*x)*sinh(x)^2 = x^2/2! - 14*x^4/4! + 691*x^6/6! - 62684*x^8/8! + .... Cf. A019670.",
				"For example, take N = 10^5. The truncated series Sum_{k = 0..N/2 -1} (-1)^(1+floor(k/2))/((2*k - 1)*(2*k + 1)*(2*k + 3)) = 0.27768018363489(8)89043849(11)61878(80026)6163(351171)58.... The bracketed digits show where this decimal expansion differs from that of (1/4)*Pi/(2*sqrt(2)). The numbers -1, 14, -691, 62684 must be added to the bracketed numbers to give the correct decimal expansion: (1/4)*Pi/(2*sqrt(2)) = 0.27768018363489(7) 89043849(25)61878(79335)6163(413855)58... (End)"
			],
			"maple": [
				"simplify( sum((cos((1/2)*k*Pi)+sin((1/2)*k*Pi))/(2*k+1), k = 0 .. infinity) );  # _Peter Bala_, Mar 09 2015"
			],
			"mathematica": [
				"RealDigits[Pi/Sqrt@8, 10, 111][[1]] (* _Michael De Vlieger_, Sep 23 2016 and slightly modified by _Robert G. Wilson v_, Jul 23 2018 *)"
			],
			"program": [
				"(PARI) default(realprecision, 20080); x=Pi*sqrt(2)/4; for (n=1, 20000, d=floor(x); x=(x-d)*10; write(\"b093954.txt\", n, \" \", d)); \\\\ _Harry J. Smith_, Jun 17 2009"
			],
			"xref": [
				"Cf. A161684 (continued fraction).",
				"Cf. A002388, A019670.",
				"Cf. A000281, A063448, A247719, A193887, A244976, A181048, A181049.",
				"Cf. A003881, A251809, A188510."
			],
			"keyword": "nonn,cons,easy",
			"offset": "1,5",
			"author": "_Eric W. Weisstein_, Apr 19 2004",
			"references": 23,
			"revision": 115,
			"time": "2020-11-05T15:27:09-05:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}