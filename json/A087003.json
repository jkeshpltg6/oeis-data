{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A087003",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 87003,
			"data": "1,0,-1,0,-1,0,-1,0,0,0,-1,0,-1,0,1,0,-1,0,-1,0,1,0,-1,0,0,0,0,0,-1,0,-1,0,1,0,1,0,-1,0,1,0,-1,0,-1,0,0,0,-1,0,0,0,1,0,-1,0,1,0,1,0,-1,0,-1,0,0,0,1,0,-1,0,1,0,-1,0,-1,0,0,0,1,0,-1,0,0,0,-1,0,1,0,1,0,-1,0,1,0,1,0,1,0,-1,0,0,0,-1,0,-1,0,-1,0,-1,0,-1,0,1,0,-1",
			"name": "a(2n) = 0 and a(2n+1) = mu(2n+1); also the sum of Mobius function values computed for terms of 3x+1 trajectory started at n, provided that Collatz conjecture is true.",
			"comment": [
				"Observe that (these summatory) terms are from {-1,0,1}, so behave like Mobius function values, not like Mertens function values. Moreover, empirically: a(n) deviates from mu(initial-value) = mu(n) only if iv = n is an even squarefree number (i.e., it is from A039956). - This comment, like also the next one, concerns the original Collatz-related definition of this sequence. - _Antti Karttunen_, Sep 18 2017",
				"From _Marc LeBrun_, Feb 19 2004: (Start)",
				"Absolute values are the same as those of A091069. First consider the descending parts of Collatz (or 3x+1) trajectories, those that begin with even numbers 2^p k, with k odd. These go 2^p*k, 2^(p-1)*k, ... 2k, k. All but 2k and k are divisible by 4, a (rational) square, hence their mu values are all 0 and so they contribute nothing to the sum.",
				"Then at the end, since mu(2k) = -mu(k), the last two steps cancel each other out. So every descending chain in a trajectory contributes 0. Of course the full trajectory of every even number consists entirely of descending chains, so A087003 is 0 for all even n.",
				"On the other hand, the trajectory of every odd number consists of just that number followed by the trajectory of an even number (which contributes nothing) so A087003 is indeed equal to mu(n) for odd n.",
				"(End)",
				"The sequence is multiplicative; it may be defined as the Dirichlet inverse of the integers modulo 2 (A000035). - _Gerard P. Michon_, Apr 29 2007",
				"a(n) appears in the second column of A156241 at every second row. - _Mats Granvik_, Feb 07 2009"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A087003/b087003.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"G. P. Michon, \u003ca href=\"http://www.numericana.com/answer/open.htm#collatz\"\u003eThe Collatz problem\u003c/a\u003e.",
				"G. P. Michon, \u003ca href=\"http://www.numericana.com/answer/numbers.htm#multiplicative\"\u003eMultiplicative functions\u003c/a\u003e.",
				"\u003ca href=\"/index/3#3x1\"\u003eIndex entries for sequences related to 3x+1 (or Collatz) problem\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A008683(n) + A292273(n). - _Antti Karttunen_, Sep 14 2017",
				"Moebius transform of A209229. - _Andrew Howroyd_, Aug 04 2018",
				"From _Jianing Song_, Aug 04 2018: (Start)",
				"Multiplicative with a(2^e) = 0, a(p^e) = (-1 + (-1)^e)/2 for odd primes p.",
				"Dirichlet g.f.: 1/((1 - 2^(-s))*zeta(s)).",
				"(End)",
				"From _Antti Karttunen_, Sep 01 2018: (Start)",
				"a(n) = A000035(n)*A008683(n).",
				"Dirichlet convolution of A318657/A046644 with itself.",
				"(End)"
			],
			"mathematica": [
				"c[x_] := (1-Mod[x, 2])*(x/2)+Mod[x, 2]*(3*x+1); c[1]=1; fpl[x_] := Delete[FixedPointList[c, x], -1] lf[x_] := Length[fpl[x]] Table[Apply[Plus, Table[MoebiusMu[Part[fpl[w], j]], {j, 1, lf[w]}]], {w, 1, 256}]"
			],
			"program": [
				"(PARI)",
				"A006370(n) = if(n%2, 3*n+1, n/2); \\\\ This function from _Michael B. Porter_, May 29 2010",
				"A087003(n) = { my(s=1); while(n\u003e1, s += moebius(n); n = A006370(n)); (s); }; \\\\ _Antti Karttunen_, Sep 14 2017",
				"(PARI) a(n)={sumdiv(n, d,  my(e=valuation(d, 2)); if(d==1\u003c\u003ce, moebius(n/d), 0))} \\\\ _Andrew Howroyd_, Aug 04 2018",
				"(PARI) A087003(n) = ((n%2)*moebius(n)); \\\\ _Antti Karttunen_, Sep 01 2018"
			],
			"xref": [
				"Cf. A006370, A008683, A039956, A292273, A099990, A209229.",
				"Cf. A000035 (the Dirichlet inverse), A318657/A318658 (the \"Dirichlet Square Root\")."
			],
			"keyword": "sign,mult",
			"offset": "1,1",
			"author": "_Labos Elemer_, Oct 02 2003",
			"ext": [
				"a(2n) = 0, a(2n+1) = mu(2n+1) added to the name as the new primary definition by _Antti Karttunen_, Sep 18 2017"
			],
			"references": 12,
			"revision": 61,
			"time": "2018-09-01T22:26:24-04:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}