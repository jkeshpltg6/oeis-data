{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A325558",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 325558,
			"data": "1,2,4,5,6,10,8,16,13,16,18,32,20,30,30,57,34,52,46,96,74,86,84,174,119,170,192,306,244,332,372,628,560,694,812,1259,1228,1566,1852,2696,2806,3538,4260,5894,6482,8098,9890,13392,15049,18706,23018,30298,35198",
			"name": "Number of compositions of n with equal circular differences up to sign.",
			"comment": [
				"A composition of n is a finite sequence of positive integers summing to n.",
				"The circular differences of a composition c of length k are c_{i + 1} - c_i for i \u003c k and c_1 - c_i for i = k. For example, the circular differences of (1,2,1,3) are (1,-1,2,-2)."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A325558/b325558.txt\"\u003eTable of n, a(n) for n = 1..200\u003c/a\u003e",
				"Gus Wiseman, \u003ca href=\"/A325325/a325325.txt\"\u003eSequences counting and ranking integer partitions by the differences of their successive parts.\u003c/a\u003e"
			],
			"example": [
				"The a(1) = 1 through a(8) = 16 compositions:",
				"  (1)  (2)   (3)    (4)     (5)      (6)       (7)        (8)",
				"       (11)  (12)   (13)    (14)     (15)      (16)       (17)",
				"             (21)   (22)    (23)     (24)      (25)       (26)",
				"             (111)  (31)    (32)     (33)      (34)       (35)",
				"                    (1111)  (41)     (42)      (43)       (44)",
				"                            (11111)  (51)      (52)       (53)",
				"                                     (222)     (61)       (62)",
				"                                     (1212)    (1111111)  (71)",
				"                                     (2121)               (1232)",
				"                                     (111111)             (1313)",
				"                                                          (2123)",
				"                                                          (2222)",
				"                                                          (2321)",
				"                                                          (3131)",
				"                                                          (3212)",
				"                                                          (11111111)"
			],
			"mathematica": [
				"Table[Length[Select[Join@@Permutations/@IntegerPartitions[n],SameQ@@Abs[Differences[Append[#,First[#]]]]\u0026]],{n,15}]"
			],
			"program": [
				"(PARI)",
				"step(R,n,s)={matrix(n, n, i, j, if(i\u003ej, if(j\u003es, R[i-j, j-s]) + if(j+s\u003c=n, R[i-j, j+s])) )}",
				"w(n,k,s)={my(R=matrix(n,n,i,j,i==j\u0026\u0026abs(i-k)==s), t=0); while(R, R=step(R,n,s); t+=R[n,k]); t}",
				"a(n) = {numdiv(max(1,n)) + sum(s=1, n-1, sum(k=1, n, w(n,k,s)))} \\\\ _Andrew Howroyd_, Aug 22 2019"
			],
			"xref": [
				"Cf. A000079, A008965, A047966, A049988, A098504, A173258, A175342, A325553, A325557, A325588, A325589."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Gus Wiseman_, May 11 2019",
			"ext": [
				"a(26)-a(42) from _Lars Blomberg_, May 30 2019",
				"Terms a(43) and beyond from _Andrew Howroyd_, Aug 22 2019"
			],
			"references": 9,
			"revision": 14,
			"time": "2019-08-24T11:51:15-04:00",
			"created": "2019-05-12T08:21:38-04:00"
		}
	]
}