{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A121646",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 121646,
			"data": "-1,0,-3,-5,-16,-39,-105,-272,-715,-1869,-4896,-12815,-33553,-87840,-229971,-602069,-1576240,-4126647,-10803705,-28284464,-74049691,-193864605,-507544128,-1328767775,-3478759201,-9107509824,-23843770275,-62423800997,-163427632720",
			"name": "a(n) = Fibonacci(n-1)^2 - Fibonacci(n)^2.",
			"comment": [
				"Negated first differences of A007598.",
				"Real part of (F(n-1) + i*F(n))^2. Corresponding imaginary part = A079472(n); e.g., (3 + 5i)^2 = (-16 + 30i) where 30 = A079472(5). Consider a(n) and A079472(n) as legs of a Pythagorean triangle; then hypotenuse = corresponding n-th term in the sequence (1, 2, 5, 13, ...; i.e., odd-indexed Fibonacci terms). a(n)/a(n-1) tends to Phi^2.",
				"3*A001654(n) - A001654(n+1) = A121646(n). - _Vladimir Joseph Stephan Orlovsky_, Nov 17 2009"
			],
			"reference": [
				"Daniele Corradetti, La Metafisica del Numero, 2008"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A121646/b121646.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,2,-1)."
			],
			"formula": [
				"a(n) = Re(F(n-1) + F(n)*i)^2 = (F(n-1))^2 - (F(n))^2.",
				"G.f.: (1-3*x)/((1+x)*(1 - 3*x + x^2)). - _Paul Barry_, Oct 13 2006",
				"a(n) = -F(n+1)*F(n-2) where F=A000045. - _Ron Knott_, Jan 24 2009",
				"a(n) = (4*(-1)^n - |A098149(n)|)/5. - _R. J. Mathar_, Jan 13 2011"
			],
			"example": [
				"a(5) = -16 since Re(3 + 5i)^2 = (-16 + 30i).",
				"a(5) = -16 = 3^2 - 5^2."
			],
			"maple": [
				"A121646 := proc(n)",
				"    combinat[fibonacci](n+1)*combinat[fibonacci](n-2) ;",
				"    -% ;",
				"end proc:",
				"seq(A121646(n),n=1..10) ; # _R. J. Mathar_, Jun 22 2017"
			],
			"mathematica": [
				"f[n_] := Re[(Fibonacci[n - 1] + I*Fibonacci[n])^2]; Array[f, 29] (* _Robert G. Wilson v_, Aug 16 2006 *)",
				"lst={};Do[a1=Fibonacci[n]*Fibonacci[n+1];a2=Fibonacci[n+1]*Fibonacci[n+2];AppendTo[lst,3*a1-a2],{n,0,60}];lst (* _Vladimir Joseph Stephan Orlovsky_, Nov 17 2009 *)",
				"Table[-Fibonacci[n-2]*Fibonacci[n+1],{n,1,40}] (* _Vladimir Joseph Stephan Orlovsky_, Nov 17 2009 *)"
			],
			"program": [
				"(PARI) a(n) = fibonacci(n-1)^2 - fibonacci(n)^2 \\\\ _Charles R Greathouse IV_, Jun 11 2015",
				"(MAGMA) [-Fibonacci(n-2)*Fibonacci(n+1): n in [1..40]]; // _G. C. Greubel_, Jan 07 2019",
				"(Sage) [-fibonacci(n-2)*fibonacci(n+1) for n in (1..40)] # _G. C. Greubel_, Jan 07 2019",
				"(GAP) List([1..40], n -\u003e -Fibonacci(n-2)*Fibonacci(n+1)); # _G. C. Greubel_, Jan 07 2019"
			],
			"xref": [
				"Cf. A079472."
			],
			"keyword": "sign,easy",
			"offset": "1,3",
			"author": "_Gary W. Adamson_, Aug 13 2006",
			"references": 8,
			"revision": 33,
			"time": "2019-12-11T20:27:02-05:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}