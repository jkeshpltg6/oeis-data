{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A065919",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 65919,
			"data": "1,5,61,1225,34361,1238221,54516085,2836074641,170218994545,11577727703701,880077524475821,73938089783672665,6803184337622361001,680392371852019772765,73489179344355757819621,8525425196317119926848801,1057226213522667226687070945",
			"name": "Bessel polynomial y_n(4).",
			"comment": [
				"Main diagonal of A143411. - _Peter Bala_, Aug 14 2008"
			],
			"reference": [
				"J. Riordan, Combinatorial Identities, Wiley, 1968, p. 77."
			],
			"link": [
				"Harry J. Smith, \u003ca href=\"/A065919/b065919.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e",
				"W. Mlotkowski, A. Romanowicz, \u003ca href=\"http://www.math.uni.wroc.pl/~pms/files/33.2/Article/33.2.19.pdf\"\u003eA family of sequences of binomial type\u003c/a\u003e, Probability and Mathematical Statistics, Vol. 33, Fasc. 2 (2013), pp. 401-408.",
				"\u003ca href=\"/index/Be#Bessel\"\u003eIndex entries for sequences related to Bessel functions or polynomials\u003c/a\u003e"
			],
			"formula": [
				"y_n(x) = Sum_{k=0..n} (n+k)!*(x/2)^k/((n-k)!*k!).",
				"From _Peter Bala_, Aug 14 2008: (Start)",
				"Recurrence relation: a(0) = 1, a(1) = 5, a(n) = 4*(2*n-1)*a(n-1) + a(n-2) for n \u003e= 2. Sequence A143412(n) satisfies the same recurrence relation.",
				"1/sqrt(e) = 1 - 2*Sum_{n = 0..inf} (-1)^n/(a(n)*a(n+1)) = 1 - 2*( 1/(1*5) - 1/(5*61) + 1/(61*1225) - ... ). (End)",
				"G.f.: 1/Q(0), where Q(k)= 1 - x - 4*x*(k+1)/Q(k+1); (continued fraction). - _Sergei N. Gladkovskii_, May 17 2013",
				"a(n) = exp(1/4)/sqrt(2*Pi)*BesselK(n+1/2,1/4). - _Gerry Martens_, Jul 22 2015",
				"a(n) ~ 2^(3*n+1/2) * n^n / exp(n-1/4). - _Vaclav Kotesovec_, Jul 22 2015",
				"From _Peter Bala_, Apr 12 2017: (Start)",
				"a(n) = 1/n!*Integral_{x = 0..inf} x^n*(1 + 2*x)^n dx.",
				"E.g.f.: d/dx( exp(x*c(2*x)) ) = 1 + 5*x + 61*x^2/2! + 1225*x^3/3! + ..., where c(x) = (1 - sqrt(1 - 4*x))/(2*x) is the g.f. of the Catalan numbers A000108. (End)",
				"G.f.: (1/(1-x))*hypergeometric2f0(1,1/2; - ; 8*x/(1-x)^2). - _G. C. Greubel_, Aug 16 2017"
			],
			"mathematica": [
				"Table[Sum[(n+k)!*2^k/((n-k)!*k!), {k,0,n}], {n,0,20}] (* _Vaclav Kotesovec_, Jul 22 2015 *)"
			],
			"program": [
				"(PARI) for (n=0, 100, if (n\u003e1, a=4*(2*n - 1)*a1 + a2; a2=a1; a1=a, if (n, a=a1=5, a=a2=1)); write(\"b065919.txt\", n, \" \", a) ) \\\\ _Harry J. Smith_, Nov 04 2009",
				"(PARI) a(n) = sum(k=0,n, (n+k)!*2^k/((n-k)!*k!) ); \\\\ _Joerg Arndt_, May 17 2013"
			],
			"xref": [
				"Cf. A001515, A001517, A001518.",
				"Polynomial coefficients are in A001498.",
				"CF. A143411 (main diagonal), A143412."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Dec 08 2001",
			"references": 6,
			"revision": 42,
			"time": "2017-08-16T14:57:17-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}