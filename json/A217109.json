{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A217109",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 217109,
			"data": "2,1,12,9,83,84,81,748,740,731,729,6653,6581,6563,6564,6561,59222,59069,59068,59051,59052,59049,531614,531569,531464,531460,531452,531443,531441,4784122,4783142,4783147,4783070,4782989,4782971,4782972,4782969,43048283",
			"name": "Minimal number (in decimal representation) with n nonprime substrings in base-9 representation (substrings with leading zeros are considered to be nonprime).",
			"comment": [
				"The sequence is well-defined in that for each n the set of numbers with n nonprime substrings is not empty. Proof: Define m(n):=2*sum_{j=i..k} 9^j, where k:=floor((sqrt(8*n+1)-1)/2), i:= n-A000217(k). For n=0,1,2,3,... the m(n) in base-9 representation are 2, 22, 20, 222, 220, 200, 2222, 2220, 2200, 2000, 22222, 22220, .... m(n) has k+1 digits and (k-i+1) 2’s, thus, the number of nonprime substrings of m(n) is ((k+1)*(k+2)/2)-k-1+i = (k*(k+1)/2)+i = n, which proves the statement.",
				"If p is a number with k prime substrings and d digits (in base-9 representation), m\u003e=d, than b := p*9^(m-d) has m*(m+1)/2 - k nonprime substrings, and a(A000217(n)-k) \u003c= b."
			],
			"link": [
				"Hieronymus Fischer, \u003ca href=\"/A217109/b217109.txt\"\u003eTable of n, a(n) for n = 0..210\u003c/a\u003e"
			],
			"formula": [
				"a(n) \u003e= 9^floor((sqrt(8*n-7)-1)/2) for n\u003e0, equality holds if n is a triangular number (cf. A000217).",
				"a(A000217(n)) = 9^(n-1), n\u003e0.",
				"a(A000217(n)-k) \u003e= 9^(n-1) + k, 0\u003c=k\u003cn, n\u003e0.",
				"a(A000217(n)-k) = 9^(n-1) + p, where p is the minimal number \u003e= 0 such that 9^(n-1) + p, has k prime substrings in base-9 representation, 0\u003c=k\u003cn, n\u003e0."
			],
			"example": [
				"a(0) = 2, since 2 = 2_9 is the least number with zero nonprime substrings in base-9 representation.",
				"a(1) = 1, since 1 = 1_9 is the least number with 1 nonprime substring in base-9 representation.",
				"a(2) = 12, since 12 = 13_9 is the least number with 2 nonprime substrings in base-9 representation (1 and 13).",
				"a(3) = 9, since 9 = 10_9 is the least number with 3 nonprime substrings in base-9 representation (0, 1 and 10).",
				"a(4) = 83, since 83 = 102_9 is the least number with 4 nonprime substrings in base-9 representation, these are 0, 1, 10, and 02 (remember, that substrings with leading zeros are considered to be nonprime)."
			],
			"xref": [
				"Cf. A019546, A035232, A039996, A046034, A069489, A085823, A211681, A211682, A211684, A211685.",
				"Cf. A035244, A079397, A213300-A213321.",
				"Cf. A217102-A217109.",
				"Cf. A217302-A217309."
			],
			"keyword": "nonn,base",
			"offset": "0,1",
			"author": "_Hieronymus Fischer_, Dec 12 2012",
			"references": 16,
			"revision": 13,
			"time": "2015-07-16T22:24:46-04:00",
			"created": "2012-12-19T13:21:53-05:00"
		}
	]
}