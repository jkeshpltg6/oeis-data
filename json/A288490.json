{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A288490",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 288490,
			"data": "4,52,108144,967067994163264,691513106932053164262669026747190128930258944",
			"name": "Number of independent vertex sets and vertex covers in the n-Hanoi graph.",
			"comment": [
				"Term a(6) has 135 decimal digits and a(7) has 404 decimal digits. - _Andrew Howroyd_, Jun 19 2017"
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A288490/b288490.txt\"\u003eTable of n, a(n) for n = 1..7\u003c/a\u003e",
				"Hanlin Chen, Renfang Wu, Guihua Huang, and Hanyuan Deng, \u003ca href=\"https://doi.org/10.26493/1855-3974.783.9b5\"\u003eIndependent Sets on the Towers of Hanoi Graphs\u003c/a\u003e, Ars Mathematica Contemporanea, volume 12, number 2, 2017, pages 247-260.  Section 3, i_n = a(n).",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HanoiGraph.html\"\u003eHanoi Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/IndependentVertexSet.html\"\u003eIndependent Vertex Set\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/VertexCover.html\"\u003eVertex Cover\u003c/a\u003e"
			],
			"mathematica": [
				"{1, 3, 3, 1} . # \u0026 /@ NestList[Function[{h, i, j, k}, {h^3 + 6 h^2 i + 9 h i^2 + 3 h^2 j + 2 i^3 + 6 h i j, h^2 i + 4 h i^2 + 2 h^2 j + h^2 k + 8 h i j + 3 i^3 + 4 i^2 j + 2 h j^2 + 2 h i k, h i^2 + 4 h i j + 2 i^3 + 7 i^2 j + 2 h i k + 3 h j^2 + 4 i j^2 + 2 i^2 k + 2 h j k, i^3 + 6 i^2 j + 9 i j^2 + 3 i^2 k + 2 j^3 + 6 i j k}] @@ # \u0026, {1, 1, 0, 0}, 4]"
			],
			"program": [
				"(PARI)",
				"\\\\ Here h0..h3 is independent sets with 0..3 of the 3 apex vertices occupied.",
				"Next(h0,h1,h2,h3) = {[h0^3 + 6*h0^2*h1 + 9*h0*h1^2 + 3*h0^2*h2 + 2*h1^3 + 6*h0*h1*h2, h0^2*h1 + 4*h0*h1^2 + 2*h0^2*h2 + h0^2*h3 + 8*h0*h1*h2 + 3*h1^3 + 4*h1^2*h2 + 2*h0*h2^2 + 2*h0*h1*h3, h0*h1^2 + 4*h0*h1*h2 + 2*h1^3 + 7*h1^2*h2 + 2*h0*h1*h3 + 3*h0*h2^2 + 4*h1*h2^2 + 2*h1^2*h3 + 2*h0*h2*h3, h1^3 + 6*h1^2*h2 + 9*h1*h2^2 + 3*h1^2*h3 + 2*h2^3 + 6*h1*h2*h3]}",
				"a(n) = {my(v);v=[1,1,0,0]; for(i=2,n,v=Next(v[1],v[2],v[3],v[4])); v[1]+v[4]+3*(v[2]+v[3])} \\\\ _Andrew Howroyd_, Jun 20 2017"
			],
			"xref": [
				"Cf. A297536 (maximum independent vertex sets in the n-Hanoi graph).",
				"Cf. A321249 (maximal independent vertex sets in the n-Hanoi graph).",
				"Cf. A288839 (chromatic polynomials of the n-Hanoi graph).",
				"Cf. A193233 (chromatic polynomial with highest coefficients first).",
				"Cf. A137889 (directed Hamiltonian paths in the n-Hanoi graph).",
				"Cf. A286017 (matchings in the n-Hanoi graph).",
				"Cf. A193136 (spanning trees of the n-Hanoi graph).",
				"Cf. A288796 (undirected paths in the n-Hanoi graph)."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Eric W. Weisstein_, Jun 16 2017",
			"ext": [
				"a(5) from _Andrew Howroyd_, Jun 19 2017"
			],
			"references": 9,
			"revision": 38,
			"time": "2020-12-23T17:24:10-05:00",
			"created": "2017-06-18T19:50:01-04:00"
		}
	]
}