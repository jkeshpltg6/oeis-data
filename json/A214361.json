{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A214361",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 214361,
			"data": "1,3,3,3,6,9,8,3,9,18,12,9,14,24,18,3,18,27,20,18,24,36,24,9,31,42,27,24,30,54,32,3,36,54,48,27,38,60,42,18,42,72,44,36,54,72,48,9,57,93,54,42,54,81,72,24,60,90,60,54,62,96,72,3,84,108,68,54",
			"name": "Expansion of c(q^2) * (c(q) + 2 * c(q^4)) / 9 in powers of q where c() is a cubic AGM theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"b(n) = 6*a(n) is the number of solutions in integers to n = x^2 + y^2 + z^2 + w^2 where x + y + z is not divisible by 3. - _Michael Somos_, Jun 23 2018"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A214361/b214361.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of (phi(q)^4 - phi(q^3)^4) / 8 = q * phi(q^3) * (chi(q) * psi(-q^3))^3 = q * psi(-q^3)^4 * (chi(q) * chi(q^3))^3 in powers of q where phi(), psi(), chi() are Ramanujan theta functions.",
				"Expansion of eta(q^2)^6 * eta(q^3) * eta(q^6)^2 * eta(q^12) / ( eta(q) * eta(q^4))^3 in powers of q.",
				"Euler transform of period 12 sequence [3, -3, 2, 0, 3, -6, 3, 0, 2, -3, 3, -4, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (12 t)) = 4 (t/i)^2 g(t) where q = exp(2 Pi i t) and g() is the g.f. for A214456.",
				"a(n) is multiplicative with a(2^e) = 3 if e\u003e0, a(3^e) = 3^e, a(p^e) = (p^(e+1) - 1) / (p - 1) if p\u003e3.",
				"G.f.: x * Product_{k\u003e0} (1 + (-x)^(3*k)) * (1 - x^(6*k))^4 / ( 1 + (-x)^k)^3.",
				"a(n) = -(-1)^n * A124449(n). a(3*n) = 3*a(n). a(2*n) = a(4*n) = 3 * A121443(n). a(2*n + 1) = A185717(n)."
			],
			"example": [
				"G.f. = q + 3*q^2 + 3*q^3 + 3*q^4 + 6*q^5 + 9*q^6 + 8*q^7 + 3*q^8 + 9*q^9 + 18*q^10 + ...",
				"a(1) = 1, b(1) = 6 with solutions (w, x, y, z) = {(0, 0, 1, 0), {0, 1, 0, 0), (1, 0, 0, 0)} and their negatives. - _Michael Somos_, Jun 23 2018"
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (EllipticTheta[ 3, 0, q]^4 - EllipticTheta[ 3, 0, q^3]^4) / 8, {q, 0, n}];",
				"a[ n_] := SeriesCoefficient[ (QPochhammer[ -q, q^2] QPochhammer[ -q^3, q^6])^3 EllipticTheta[ 2, 0, (-q)^(3/2)]^4 / (-16 (-q)^(1/2)), {q, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c1, 0, n--; A = x * O(x^n); polcoeff( eta(x^2 + A)^6 * eta(x^3 + A) * eta(x^6 + A)^2 * eta(x^12 + A) / ( eta(x + A) * eta(x^4 + A))^3, n))};",
				"(PARI) {a(n) = if( n\u003c1, 0, sigma(n) + if( n%3==0, -1 * sigma(n/3)) + if( n%4==0, -4 * sigma(n/4)) + if( n%12==0, +4 * sigma(n/12)))};",
				"(PARI) {a(n) = my(A, p, e); if( n\u003c1, 0, A = factor(n); prod(k=1, matsize(A)[1], [p, e] = A[k, ]; if(p==2, 3, p==3, 3^e, (p^(e+1) - 1) / (p - 1))))};"
			],
			"xref": [
				"Cf. A121443, A124449, A185717, A214456."
			],
			"keyword": "nonn,mult",
			"offset": "1,2",
			"author": "_Michael Somos_, Jul 18 2012",
			"references": 2,
			"revision": 33,
			"time": "2021-03-12T22:24:46-05:00",
			"created": "2012-07-19T05:20:26-04:00"
		}
	]
}