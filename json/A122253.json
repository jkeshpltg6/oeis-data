{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A122253",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 122253,
			"data": "12,12,360,60,280,168,5040,180,11880,264,240240,10920,13104,720,367200,3060,813960,15960,1053360,27720,3825360,16560,5023200,163800,982800,3024,2630880,6960,33227040,229152,116867520,235620,282744,2520,1612119600,7676760,46060560",
			"name": "Binet's factorial series. Denominators of the coefficients of a convergent series for the logarithm of the Gamma function.",
			"comment": [
				"See A122252 for references and formulas."
			],
			"link": [
				"Raphael Schumacher, \u003ca href=\"http://arxiv.org/abs/1602.00336\"\u003eRapidly Convergent Summation Formulas involving Stirling Series\u003c/a\u003e, arXiv preprint arXiv:1602.00336 [math.NT], 2016.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Stirling%27s_approximation\"\u003eStirling's approximation\u003c/a\u003e"
			],
			"formula": [
				"c(n) = (1/n)*Integral_{x=0..1} x^n*(x - 1/2) dx."
			],
			"example": [
				"c(1) = (1/1)*Integral_{x=0..1} x*(x - 1/2) dx = Integral_{x=0..1} (x^2 - x/2) dx = (x^3/3 - x^2/4)|{x=1} - (x^3/3 - x^2/4)|{x=0} = 1/12."
			],
			"maple": [
				"r := n -\u003e add((-1)^(n-j)*Stirling1(n,j)*j/((j+1)*(j+2)), j = 1..n)/(2*n):",
				"a := n -\u003e denom(r(n)); seq(a(n), n = 1..37); # _Peter Luschny_, Sep 22 2021"
			],
			"mathematica": [
				"Rising[z_, n_Integer/;n\u003e0] := z Rising[z + 1, n - 1]; Rising[z_, 0] := 1; c[n_Integer/;n\u003e0] := Integrate[Rising[x, n] (x - 1/2), {x, 0, 1}] / n;"
			],
			"program": [
				"(PARI) a(n) = denominator(sum(j=1, n, (-1)^(n-j)*stirling(n,j,1)*j/((j+1)*(j+2)))/(2*n)); \\\\ _Michel Marcus_, Sep 22 2021"
			],
			"xref": [
				"Cf. A122252 (numerators), A001163, A001164."
			],
			"keyword": "easy,frac,nonn",
			"offset": "1,1",
			"author": "Paul Drees (zemyla(AT)gmail.com), Aug 27 2006",
			"references": 1,
			"revision": 29,
			"time": "2021-09-22T10:29:08-04:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}