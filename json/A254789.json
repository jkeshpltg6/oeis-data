{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A254789",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 254789,
			"data": "1,1,3,15,111,1119,14487,230943,4395855,97608831,2482988079,71321533887,2286179073663,80984105660415,3144251526824991,132867034410319359,6073991827274809407,298815244349875677183,15746949613850439270975,885279424331353488224511,52902213099156326247243519",
			"name": "The number of unlabeled binary trees that contain n distinct subtrees, where a subtree means take any node and everything below that node.",
			"comment": [
				"a(n) is also the number of compacted binary trees of size n. A compacted binary tree is a directed acyclic graph computed by the UID procedure (see Flajolet et al.) from a given full binary tree. Every edge leading to a subtree that has already been seen during the traversal is replaced by a new kind of edge, a pointer, to the already existing subtree. The size of the compacted binary tree is defined by the number of its internal nodes. See Genitrini et al. - _Michael Wallner_, Apr 21 2017"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A254789/b254789.txt\"\u003eTable of n, a(n) for n = 0..371\u003c/a\u003e (terms n = 1..100 from Andrew Szymczak)",
				"P. Flajolet, P. Sipala, and J.-M. Steyaert, \u003ca href=\"http://dx.doi.org/10.1007/BFb0032034\"\u003eAnalytic variations on the common subexpression problem\u003c/a\u003e, in Automata, Languages and Programming (Coventry, 1990), volume 443 of Lecture Notes in Comput. Sci., pages 220-234. Springer, New York, 1990.",
				"Antoine Genitrini, Bernhard Gittenberger, Manuel Kauers and Michael Wallner, \u003ca href=\"https://arxiv.org/abs/1703.10031\"\u003eAsymptotic Enumeration of Compacted Binary Trees\u003c/a\u003e, arXiv:1703.10031 [math.CO], 2017.",
				"Project Euler.chat, \u003ca href=\"https://projecteuler.chat/viewtopic.php?t=3987\"\u003eCounting Subtrees\u003c/a\u003e, (2015).",
				"Marko Riedel, Eric Noeth et al., \u003ca href=\"http://math.stackexchange.com/questions/1132147/\"\u003eDistinct subtrees in binary trees\u003c/a\u003e, Math StackExchange, February 2015.",
				"Michael Wallner, \u003ca href=\"http://dmg.tuwien.ac.at/mwallner/files/Dissertation_Wallner.pdf\"\u003e Combinatorics of lattice paths and tree-like structures\u003c/a\u003e (Dissertation, Institut für Diskrete Mathematik und Geometrie, Technische Universität Wien), 2016.",
				"Christoph Wernhard and Wolfgang Bibel, \u003ca href=\"https://arxiv.org/abs/2104.13645\"\u003eLearning from Łukasiewicz and Meredith: Investigations into Proof Structures (Extended Version)\u003c/a\u003e, arXiv:2104.13645 [cs.AI], 2021."
			],
			"formula": [
				"a(n) := b(n,0) where b(0,p) := p+1, b(1,p) := p^2+p+1 and b(n+1,p) = sum(b(i,p)*b(n-i,p+i), i=0..n) for n\u003e=2, see Genitrini et al. - _Michael Wallner_, Apr 21 2017"
			],
			"example": [
				"Consider n = 2. For two different subtrees there are (i) the left path on two nodes, (ii) the right path on two nodes, and (iii) the complete tree on three nodes, giving three trees total.",
				"a(3) = 15, as follows:",
				"** First type:",
				"4 paths on 3 nodes:",
				"      o",
				"     /",
				"    o",
				"   /",
				"  o",
				"** Second type:",
				"complete tree on three nodes with a singleton attached, 4 trees on 4 nodes:",
				"      o",
				"     / \\",
				"    o   o",
				"   /",
				"  o",
				"** Third type:",
				"complete tree on three nodes attached to a singleton at the root, 2 trees on 4 nodes",
				"      o",
				"     /",
				"    o",
				"   / \\",
				"  o   o",
				"** Fourth type:",
				"complete tree on three nodes with two singletons attached in parallel, 2 trees on 5 nodes",
				"      o",
				"     / \\",
				"    o   o",
				"   /   /",
				"  o    o",
				"** Fifth type:",
				"complete tree on three nodes with two singletons attached to the same terminal, 2 trees on 5 nodes",
				"      o",
				"     / \\",
				"    o   o",
				"   / \\",
				"  o   o",
				"** Sixth type:",
				"complete tree on seven nodes",
				"      o",
				"     / \\",
				"    o   o",
				"   / \\  |\\",
				"  o   o o o",
				"Total is 4+4+2+2+2+1 = 15."
			],
			"mathematica": [
				"b[n_ /; n \u003e= 2, p_] := b[n, p] = Sum[b[i, p] b[n-i-1, p+i], {i, 0, n-1}];",
				"b[0, p_] := p + 1;",
				"b[1, p_] := p^2 + p + 1;",
				"a[n_] := b[n, 0];",
				"Table[a[n], {n, 0, 20}] (* _Jean-François Alcover_, Jul 29 2018  *)"
			],
			"xref": [
				"Cf. A082161."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Marko Riedel_, Feb 07 2015",
			"ext": [
				"a(9)-a(15) computed by Johannes Waldmann",
				"a(16)-a(20) computed by _Andrew Szymczak_, added by _Eric Noeth_, Mar 04 2015"
			],
			"references": 3,
			"revision": 81,
			"time": "2021-08-02T17:43:42-04:00",
			"created": "2015-02-21T12:07:51-05:00"
		}
	]
}