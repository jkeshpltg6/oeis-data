{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A181741",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 181741,
			"data": "3,5,7,11,13,23,29,31,47,59,61,127,191,223,239,251,383,479,503,509,991,1019,1021,2039,3583,3967,4079,4091,4093,6143,8191,15359,16127,16319,16381,63487,65407,65519,129023,131063,131071,245759,253951,261631,261887,262079,262111,262127,262139",
			"name": "Primes of the form 2^t-2^k-1, k\u003e=1.",
			"comment": [
				"All Mersenne primes A000668(i) are in the sequence, parametrized by t=A000043(i)+1 and k=A000043(i).",
				"If p is in the sequence, then the exponents t and k are unique.",
				"For given k, the smallest value of t defines sequence A181692.",
				"Every term p=2^t-2^k-1 in this sequence here generates an entry 2^(t-1)*p in A181595 (cf. A181701)."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A181741/b181741.txt\"\u003eTable of n, a(n) for n = 1..1000, probable primes for n \u003e 150\u003c/a\u003e",
				"Paul Pollack and Vladimir Shevelev, \u003ca href=\"https://doi.org/10.1016/j.jnt.2012.06.008\"\u003eOn perfect and near-perfect numbers\u003c/a\u003e, J. Number Theory 132 (2012), pp. 3037-3046. - _N. J. A. Sloane_, Sep 04 2012",
				"V. Shevelev,\u003ca href=\"http://arxiv.org/abs/1011.6160\"\u003ePerfect and near-perfect numbers\u003c/a\u003e, arXiv:1011.6160 [math.NT], 2010-2012."
			],
			"formula": [
				"Conjecture: equals the intersection of A000040 and A081118 or the intersection of A000040 and A089633. [_R. J. Mathar_, Nov 18 2010]"
			],
			"maple": [
				"isA000079 := proc(n) if n = 1 then true; elif type(n,'odd') then false; else if nops( numtheory[factorset](n) ) = 1 then  true;  else",
				"false; end if; end if; end proc:",
				"isA181741 := proc(p) if isprime(p) then k := A007814(p+1) ; (p+1)/2^k+1 ; return ( isA000079(%) and k \u003e=1 ) ; else",
				"false;  end if; end proc:",
				"for i from 1 to 1000 do p := ithprime(i) ; if isA181741(p) then printf(\"%d,\",p) ; end if; end do: # _R. J. Mathar_, Nov 18 2010"
			],
			"mathematica": [
				"Select[Table[2^t-2^k-1, {t, 1, 20}, {k, 1, t-1}] // Flatten // Union, PrimeQ] (* _Jean-François Alcover_, Nov 16 2017 *)"
			],
			"program": [
				"(Haskell)",
				"a181741 n = a181741_list !! (n-1)",
				"a181741_list = filter ((== 1) . a010051) a081118_list",
				"-- _Reinhard Zumkeller_, Feb 23 2012",
				"(PARI) lista(nn) = {for (n=3, nn, forstep(k=n-1, 1, -1, if (isprime(p=2^n-2^k-1), print1(p, \", \"));););} \\\\ _Michel Marcus_, Dec 17 2018"
			],
			"xref": [
				"Cf. A181595, A181692, A181701, A000043.",
				"Cf. A010051, primes in A081118, see also A208083."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Vladimir Shevelev_, Nov 08 2010",
			"ext": [
				"Corrected (251 and 1019 inserted) and extended by _R. J. Mathar_, Nov 18 2010"
			],
			"references": 6,
			"revision": 32,
			"time": "2018-12-18T00:02:14-05:00",
			"created": "2010-11-10T03:00:00-05:00"
		}
	]
}