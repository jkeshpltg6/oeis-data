{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A323723",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 323723,
			"data": "0,0,4,14,32,64,108,174,256,368,500,670,864,1104,1372,1694,2048,2464,2916,3438,4000,4640,5324,6094,6912,7824,8788,9854,10976,12208,13500,14910,16384,17984,19652,21454,23328,25344,27436,29678,32000,34480,37044,39774",
			"name": "a(n) = (-2 - (-1)^n*(-2 + n) + n + 2*n^3)/4.",
			"comment": [
				"For n \u003e 1, a(n) is the subdiagonal sum of the matrix M(n) whose permanent is A322277(n).",
				"All the terms of this sequence are even numbers (A005843)."
			],
			"link": [
				"Stefano Spezia, \u003ca href=\"/A323723/b323723.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Christian Krause, \u003ca href=\"https://github.com/ckrause/loda\"\u003eLODA, an assembly language, a computational model and a tool for mining integer sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,1,-4,1,2,-1)."
			],
			"formula": [
				"O.g.f.: 2*x^2*(2 + 3*x + x^3)/((1 - x)^4*(1 + x)^2).",
				"E.g.f.: (1/4)*exp(-x)*(2 + x)*(1 + exp(2*x)*(-1 + 2*x + 2* x^2)).",
				"a(n) = 2*a(n-1) + a(n-2) - 4*a(n-3) + a(n-4) + 2*a(n-5) - a(n-6) for n \u003e 5.",
				"a(n) = (6 + n + n^3 + 12*floor((n - 3)/2) + 4*floor((n - 3)/2)^2 - 2*(1 + n)*floor((n - 1)/2)/2.",
				"a(n) = (-2 - A033999(n)*(-2 + n) + n + A033431(n))/4.",
				"a(n) = n^3/2 for even n; a(n) = (n - 1)*(n^2 + n + 2)/2 otherwise. - _Bruno Berselli_, Feb 06 2019",
				"a(n) = 2*A004526(n*A000982(n)). [Found by _Christian Krause_'s LODA miner] - _Stefano Spezia_, Dec 12 2021"
			],
			"maple": [
				"a:=n-\u003e(-2 - (-1)^n*(-2 + n) + n + 2*n^3)/4: seq(a(n), n=0..50);"
			],
			"mathematica": [
				"a[n_]:=(6 + n + n^3 + 12 Floor[1/2 (-3 + n)] + 4 Floor[1/2 (-3 + n)]^2 - 2 (1 + n) Floor[1/2 (-1 + n)])/2; Array[a,50,0]"
			],
			"program": [
				"(GAP) Flat(List([0..50], n -\u003e (-2-(-1)^n*(-2+n)+n+2*n^3)/4));",
				"(MAGMA) [(-2-(-1)^n*(-2+n)+n+2*n^3)/4: n in [0..50]];",
				"(Maxima) makelist((-2-(-1)^n*(-2+n)+n+2*n^3)/4, n, 0, 50);",
				"(PARI) a(n) = (-2-(-1)^n*(-2+n)+n+2*n^3)/4;",
				"(Python) [(-2-(-1)**n*(-2+n)+n+2*n**3)/4 for n in range(50)]"
			],
			"xref": [
				"Cf. A000982, A004526, A005843, A033431, A033999, A317614, A322277, A323724, A325655."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Stefano Spezia_, Jan 25 2019",
			"references": 6,
			"revision": 45,
			"time": "2021-12-13T16:26:17-05:00",
			"created": "2019-02-06T03:23:15-05:00"
		}
	]
}