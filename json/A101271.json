{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A101271",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 101271,
			"data": "1,1,2,3,4,5,6,8,9,12,12,16,15,21,20,26,25,33,28,40,36,45,42,56,44,65,56,70,64,84,66,96,81,100,88,120,90,133,110,132,121,161,120,175,140,176,156,208,153,220,180,222,196,261,184,280,225,270,240,312,230,341,272",
			"name": "Number of partitions of n into 3 distinct and relatively prime parts.",
			"comment": [
				"The Heinz numbers of these partitions are the intersection of A289509 (relatively prime), A005117 (strict), and A014612 (triple). - _Gus Wiseman_, Oct 15 2020"
			],
			"link": [
				"Fausto A. C. Cariboni, \u003ca href=\"/A101271/b101271.txt\"\u003eTable of n, a(n) for n = 6..10000\u003c/a\u003e"
			],
			"formula": [
				"G.f. for the number of partitions of n into m distinct and relatively prime parts is Sum(moebius(k)*x^(m*(m+1)/2*k)/Product(1-x^(i*k), i=1..m), k=1..infinity)."
			],
			"example": [
				"For n=10 we have 4 such partitions: 1+2+7, 1+3+6, 1+4+5 and 2+3+5.",
				"From _Gus Wiseman_, Oct 13 2020: (Start)",
				"The a(6) = 1 through a(18) = 15 triples (A..F = 10..15):",
				"  321  421  431  432  532  542  543  643  653  654  754  764  765",
				"            521  531  541  632  651  652  743  753  763  854  873",
				"                 621  631  641  732  742  752  762  853  863  954",
				"                      721  731  741  751  761  843  871  872  972",
				"                           821  831  832  851  852  943  953  981",
				"                                921  841  932  861  952  962  A53",
				"                                     931  941  942  961  971  A71",
				"                                     A21  A31  951  A51  A43  B43",
				"                                          B21  A32  B32  A52  B52",
				"                                               A41  B41  A61  B61",
				"                                               B31  C31  B42  C51",
				"                                               C21  D21  B51  D32",
				"                                                         C32  D41",
				"                                                         C41  E31",
				"                                                         D31  F21",
				"                                                         E21",
				"(End)"
			],
			"maple": [
				"m:=3: with(numtheory): g:=sum(mobius(k)*x^(m*(m+1)/2*k)/Product(1-x^(i*k),i=1..m),k=1..20): gser:=series(g,x=0,80): seq(coeff(gser,x^n),n=6..77); # _Emeric Deutsch_, May 31 2005"
			],
			"mathematica": [
				"Table[Length[Select[IntegerPartitions[n,{3}],UnsameQ@@#\u0026\u0026GCD@@#==1\u0026]],{n,6,50}] (* _Gus Wiseman_, Oct 13 2020 *)"
			],
			"xref": [
				"Cf. A023024-A023030, A000742-A000743, A023031-A023035.",
				"A000741 is the ordered non-strict version.",
				"A001399(n-6) does not require relative primality.",
				"A023022 counts pairs instead of triples.",
				"A023023 is the not necessarily strict version.",
				"A078374 counts these partitions of any length, with Heinz numbers A302796.",
				"A101271*6 is the ordered version.",
				"A220377 is the pairwise coprime instead of relatively prime version.",
				"A284825 counts the case that is pairwise non-coprime also.",
				"A337605 is the pairwise non-coprime instead of relatively prime version.",
				"A008289 counts strict partitions by sum and length.",
				"A007304 gives the Heinz numbers of 3-part strict partitions.",
				"A307719 counts 3-part pairwise coprime partitions.",
				"A337601 counts 3-part partitions whose distinct parts are pairwise coprime.",
				"Cf. A000010, A000217, A000837, A007360, A014612, A055684, A289509, A332004, A337452, A337563."
			],
			"keyword": "easy,nonn",
			"offset": "6,3",
			"author": "_Vladeta Jovovic_, Dec 19 2004",
			"ext": [
				"More terms from _Emeric Deutsch_, May 31 2005"
			],
			"references": 21,
			"revision": 24,
			"time": "2021-01-24T06:43:42-05:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}