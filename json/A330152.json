{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A330152",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 330152,
			"data": "0,2,8,23,52,127,218,412,542,692,1471,2064,2327,4739,13025,16213,20388,45407,82605,123706,207778,323382,605338,905670,1033731,2041995,3325970,4282238,7638962,9840138,10364329",
			"name": "Absolute multiplicative persistence: a(n) is the least number with multiplicative persistence n for some base b \u003e 1.",
			"link": [
				"Edson de Faria and Charles Tresser, \u003ca href=\"http://arxiv.org/abs/1307.1188\"\u003eOn Sloane's persistence problem\u003c/a\u003e, arXiv preprint arXiv:1307.1188 [math.DS], 2013.",
				"Edson de Faria and Charles Tresser, \u003ca href=\"http://dx.doi.org/10.1080/10586458.2014.910849\"\u003eOn Sloane's persistence problem\u003c/a\u003e, Experimental Math., 23 (No. 4, 2014), 363-382.",
				"Brady Haran and Matt Parker, \u003ca href=\"https://www.youtube.com/watch?v=Wim9WJeDTHQ\"\u003eWhat's special about 277777788888899?\u003c/a\u003e, Numberphile video, 2019.",
				"Tim Lamont-Smith, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL24/Lamont/lamont5.html\"\u003eMultiplicative Persistence and Absolute Multiplicative Persistence\u003c/a\u003e, J. Int. Seq., Vol. 24 (2021), Article 21.6.7.",
				"Stephanie Perez and Robert Styer, \u003ca href=\"http://dx.doi.org/10.2140/involve.2015.8.439\"\u003ePersistence: A Digit Problem\u003c/a\u003e, Involve, Vol. 8 (2015), No. 3, 439-446.",
				"N. J. A. Sloane, \u003ca href=\"http://neilsloane.com/doc/persistence.html\"\u003eThe persistence of a number\u003c/a\u003e, J. Recreational Math., 6 (1973), 97-98.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/MultiplicativePersistence.html\"\u003eMultiplicative Persistence\u003c/a\u003e"
			],
			"example": [
				"2 when represented in base 2 goes 10 -\u003e 0 and has an absolute persistence of 1, so a(1) = 2.",
				"8 when represented in base 3 goes 22 -\u003e 11 -\u003e 1 and has an absolute persistence of 2, so a(2) = 8.",
				"23 when represented in base 6 goes 35 -\u003e 23 -\u003e 10 -\u003e 1 and has absolute persistence of 3, so a(3) = 23 (Cf. A064867).",
				"52 when represented in base 9 goes 57 -\u003e 38 -\u003e 26 -\u003e 13 -\u003e 3 and has absolute persistence of 4, so a(4) = 52 (Cf. A064868)."
			],
			"program": [
				"(Python)",
				"from math import prod",
				"from sympy.ntheory.digits import digits",
				"def mp(n, b): # multiplicative persistence of n in base b",
				"    c = 0",
				"    while n \u003e= b:",
				"        n, c = prod(digits(n, b)[1:]), c+1",
				"    return c",
				"def a(n):",
				"    k = 0",
				"    while True:",
				"        if any(mp(k, b)==n for b in range(2, max(3, k))): return k",
				"        k += 1",
				"print([a(n) for n in range(11)]) # _Michael S. Branicky_, Sep 17 2021"
			],
			"xref": [
				"Cf. A003001, A064867, A064868, A064869, A064870, A064871, A064872."
			],
			"keyword": "nonn,base,more",
			"offset": "0,2",
			"author": "_Tim Lamont-Smith_, Nov 29 2019",
			"ext": [
				"a(19)-a(27) from _Giovanni Resta_, Jan 20 2020",
				"a(28)-a(30) from _Michael S. Branicky_, Sep 17 2021"
			],
			"references": 0,
			"revision": 43,
			"time": "2021-09-18T08:27:16-04:00",
			"created": "2020-01-17T23:33:43-05:00"
		}
	]
}