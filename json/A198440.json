{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A198440",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 198440,
			"data": "5,13,17,25,29,37,41,61,53,65,65,85,73,85,89,101,113,97,109,125,145,145,149,137,181,157,173,197,185,169,221,185,193,205,229,257,265,205,221,233,241,269,313,265,293,325,277,317,281,365,289,305,305,365,401",
			"name": "Square root of second term of a triple of squares in arithmetic progression that is not a multiple of another triple in (A198384, A198385, A198386).",
			"comment": [
				"This sequence gives the hypotenuses of primitive Pythagorean triangles (with multiplicities) ordered according to nondecreasing values of the leg sums x+y (called w in the Zumkeller link, given by A198441). See the comment on the equivalence to primitive Pythagorean triangles in A198441. For the values of these hypotenuses ordered nondecreasingly see A020882. See also the triangle version A222946. - _Wolfdieter Lang_, May 23 2013"
			],
			"link": [
				"Ray Chandler, \u003ca href=\"/A198440/b198440.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Keith Conrad, \u003ca href=\"http://www.math.uconn.edu/~kconrad/blurbs/ugradnumthy/3squarearithprog.pdf\"\u003eArithmetic progressions of three squares\u003c/a\u003e",
				"Reinhard Zumkeller, \u003ca href=\"/A198435/a198435.txt\"\u003eTable of initial values\u003c/a\u003e"
			],
			"formula": [
				"A198436(n) = a(n)^2; a(n) = A198389(A198409(n))."
			],
			"example": [
				"From _Wolfdieter Lang_, May 22 2013: (Start)",
				"Primitive Pythagorean triangle (x,y,z), even y, connection:",
				"a(8) = 61 because the leg sum x+y = A198441(8) = 71 and due to A198439(8) = 49 one has y = (71+49)/2 = 60 is even, hence x = (71-49)/2 = 11 and z = sqrt(11^2 + 60^2) = 61. (End)"
			],
			"mathematica": [
				"wmax = 1000;",
				"triples[w_] := Reap[Module[{u, v}, For[u = 1, u \u003c w, u++, If[IntegerQ[v = Sqrt[(u^2 + w^2)/2]], Sow[{u, v, w}]]]]][[2]];",
				"tt = Flatten[DeleteCases[triples /@ Range[wmax], {}], 2];",
				"DeleteCases[tt, t_List /; GCD@@t \u003e 1 \u0026\u0026 MemberQ[tt, t/GCD@@t]][[All, 2]] (* _Jean-François Alcover_, Oct 22 2021 *)"
			],
			"program": [
				"(Haskell)",
				"a198440 n = a198440_list !! (n-1)",
				"a198440_list = map a198389 a198409_list"
			],
			"xref": [
				"Cf. A020882, A222946."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Reinhard Zumkeller_, Oct 25 2011",
			"references": 5,
			"revision": 24,
			"time": "2021-10-22T11:34:15-04:00",
			"created": "2011-10-25T17:35:44-04:00"
		}
	]
}