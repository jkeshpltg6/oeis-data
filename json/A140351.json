{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A140351",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 140351,
			"data": "1,0,-1,-1,-1,1,1,-1,-3,3,5,-5,-691,691,35,-35,-3617,3617,43867,-43867,-1222277,1222277,854513,-854513,-1181820455,1181820455,76977927,-76977927,-23749461029,23749461029,8615841276005,-8615841276005,-84802531453387,84802531453387",
			"name": "Numerator of the coefficient [x^1] of the Bernoulli twin number polynomial C(n,x).",
			"comment": [
				"The Bernoulli twin number polynomials C(n,x) are defined in A129378.",
				"I call the full fraction [x^1]C(n,x) the secondary Bernoulli twin numbers."
			],
			"formula": [
				"a(n) = numerator(Sum_{i=0..n} binomial(n,i)*(i+1)*bernoulli(i)). - _Vladimir Kruchinin_, Oct 05 2016"
			],
			"example": [
				"The coefficients [x^m]C(n,x) are a table of fractions:",
				"1 ;",
				"-1/2, 1;",
				"-1/3, 0, 1;",
				"-1/6, -1/2, 1/2, 1;",
				"-1/30,-1/2, -1/2, 1, 1;",
				"1/30, -1/6, -1,-1/3, 3/2, 1;",
				"1/42, 1/6, -1/2, -5/3, 0, 2, 1;",
				"-1/42, 1/6, 1/2, -7/6, -5/2, 1/2, 5/2, 1;",
				"-1/30, -1/6, 2/3, 7/6, -7/3, -7/2, 7/6, 3, 1;",
				"1/30, -3/10, -2/3, 2, 7/3, -21/5, -14/3, 2, 7/2, 1;",
				"5/66, 3/10, -3/2, -2, 5, 21/5, -7, -6, 3, 4, 1; ...",
				"This sequence here contains the numerators of the second column."
			],
			"maple": [
				"C := proc(n,x) if n = 0 then 1; else add(binomial(n-1,j-1)* bernoulli(j,x),j=1..n) ; expand(%) ; end if ; end proc:",
				"A140351 := proc(n) coeff(C(n,x),x,1) ; numer(%) ; end proc: seq(A140351(n),n=1..80) ; # _R. J. Mathar_, Nov 22 2009"
			],
			"mathematica": [
				"b[n_, x_] := Coefficient[ Series[ t*E^(x*t)/(E^t - 1), {t, 0, n}], t, n]*n!; c[n_, x_] := Sum[ Binomial[n-1, j-1]*b[j, x], {j, 1, n}]; t[n_, m_] := Coefficient[c[n, x], x, m]; Table[t[n, 1] // Numerator, {n, 1, 34} ] (* _Jean-François Alcover_, Mar 04 2013 *)",
				"Table[Sum[Binomial[n, k]*(k+1)*BernoulliB[k], {k, 0, n}], {n, 0, 30}] // Numerator (* _Vaclav Kotesovec_, Oct 05 2016 *)"
			],
			"program": [
				"(Maxima) makelist(num(sum((binomial(n,i)*(i+1)*bern(i)),i,0,n)),n,0,20); /* _Vladimir Kruchinin_, Oct 05 2016 */",
				"(PARI) a(n) = numerator(sum(i=0, n, binomial(n,i)*(i+1)*bernfrac(i))); \\\\ _Michel Marcus_, Oct 05 2016"
			],
			"xref": [
				"Cf. A129378, A129826."
			],
			"keyword": "frac,sign",
			"offset": "1,9",
			"author": "_Paul Curtz_, May 30 2008, Jun 23 2008",
			"ext": [
				"Edited and extended by _R. J. Mathar_, Nov 22 2009"
			],
			"references": 7,
			"revision": 23,
			"time": "2016-10-06T05:13:25-04:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}