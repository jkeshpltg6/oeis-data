{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A273904",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 273904,
			"data": "0,1,4,13,44,149,498,1656,5498,18236,60456,200409,664464,2203755,7311894,24271290,80605250,267821525,890305418,2961015981,9852481830,32798011430,109229396466,363927233758,1213012655490,4044684629394,13491663770344",
			"name": "Number of even-length columns in all bargraphs having semiperimeter n (n\u003e=2).",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A273904/b273904.txt\"\u003eTable of n, a(n) for n = 2..1000\u003c/a\u003e",
				"M. Bousquet-Mélou and A. Rechnitzer, \u003ca href=\"http://dx.doi.org/10.1016/S0196-8858(02)00553-5\"\u003eThe site-perimeter of bargraphs\u003c/a\u003e, Adv. in Appl. Math. 31 (2003), 86-112.",
				"Emeric Deutsch, S Elizalde, \u003ca href=\"http://arxiv.org/abs/1609.00088\"\u003eStatistics on bargraphs viewed as cornerless Motzkin paths\u003c/a\u003e, arXiv preprint arXiv:1609.00088, 2016"
			],
			"formula": [
				"G.f.:  g(z)=((1-z)(1-3z+z^2-z^3)-(1-z)^2*Q)/(2z(1+z^2)*Q), where Q = sqrt((1-z)(1-3z-z^2-z^3)).",
				"a(n) = Sum(k*A273903(n,k), k\u003e=0)."
			],
			"example": [
				"a(4) = 4 because the 5 (=A082582(4)) bargraphs of semiperimeter 4 correspond to the compositions [1,1,1],[1,2],[2,1],[2,2],[3] and, clearly, they have 0,1,1,2,0 columns of even length."
			],
			"maple": [
				"Q := sqrt((1-z)*(1-3*z-z^2-z^3)): g := (((1-z)*(1-3*z+z^2-z^3)-(1-z)^2*Q)*(1/2))/(z*(1+z^2)*Q): gser := series(g, z = 0, 40): seq(coeff(gser, z, m), m = 2 .. 35);",
				"# second Maple program:",
				"a:= proc(n) option remember; `if`(n\u003c7, [0$3, 1, 4, 13, 44]",
				"      [n+1], ((7*n-22)*(n-6)*a(n-7) -(5*n^2-21*n+6)*a(n-6)+",
				"      (21*n^2-180*n+404)*a(n-5) -(43*n^2-265*n+332)*a(n-4)",
				"      +(41*n^2-226*n+308)*a(n-3) -(43*n^2-257*n+308)*a(n-2)",
				"      +(27*n^2-110*n+36)*a(n-1))/ ((n+1)*(5*n-18)))",
				"    end:",
				"seq(a(n), n=2..40);  # _Alois P. Heinz_, Jun 24 2016"
			],
			"mathematica": [
				"Q = Sqrt[(1-z)*(1-3*z-z^2-z^3)]; g = (((1-z)*(1-3*z+z^2-z^3) - (1-z)^2 * Q)*(1/2))/(z*(1+z^2)*Q); gser = g + O[z]^40; CoefficientList[gser, z][[3 ;; -1]] (* _Jean-François Alcover_, Oct 04 2016, adapted from Maple *)"
			],
			"xref": [
				"Cf. A082582, A273901, A273902, A273903."
			],
			"keyword": "nonn",
			"offset": "2,3",
			"author": "_Emeric Deutsch_, Jun 23 2016",
			"references": 4,
			"revision": 15,
			"time": "2017-08-19T23:20:42-04:00",
			"created": "2016-06-24T09:03:21-04:00"
		}
	]
}