{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A128724",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 128724,
			"data": "1,1,3,9,1,30,5,1,107,23,6,1,399,105,31,7,1,1537,477,156,40,8,1,6069,2166,771,219,50,9,1,24434,9849,3772,1165,295,61,10,1,99924,44869,18347,6083,1676,385,73,11,1,413943,204820,88908,31376,9278,2322,490,86,12,1",
			"name": "Triangle read by rows: T(n,k) is the number of skew Dyck paths of semilength n and having k LL's (n \u003e= 0; 0 \u003c= k \u003c= n-2 for n \u003e= 2).",
			"comment": [
				"A skew Dyck path is a path in the first quadrant which begins at the origin, ends on the x-axis, consists of steps U=(1,1)(up), D=(1,-1)(down) and L=(-1,-1)(left) so that up and left steps do not overlap. The length of a path is defined to be the number of steps in it.",
				"Row 0 and row 1 have one term each; row n has n-1 terms (n \u003e= 2).",
				"Row sums yield A002212."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A128724/b128724.txt\"\u003eRows n = 0..150, flattened\u003c/a\u003e",
				"E. Deutsch, E. Munarini, S. Rinaldi, \u003ca href=\"http://dx.doi.org/10.1016/j.jspi.2010.01.015\"\u003eSkew Dyck paths\u003c/a\u003e, J. Stat. Plann. Infer. 140 (8) (2010) 2191-2203"
			],
			"formula": [
				"T(n,0) = A128725(n).",
				"Sum_{k=0..n-2} k*T(n,k) = A128726(n).",
				"G.f.: G = G(t,z) satisfies z^2*G^3 - z(2 - tz)G^2 + (1 + z - z^2 - tzG + tz - 1 = 0."
			],
			"example": [
				"T(4,1)=5 because we have UDUUUDLL, UUUUDLLD, UUDUUDLL, UUUDUDLL and UUUUDDLL.",
				"Triangle starts:",
				"    1;",
				"    1;",
				"    3;",
				"    9,  1;",
				"   30,  5,  1;",
				"  107, 23,  6,  1;"
			],
			"maple": [
				"eq:=z^2*G^3-z*(2-t*z)*G^2+(1+z-z^2-t*z)*G+t*z-1=0: G:=RootOf(eq,G): Gser:=simplify(series(G,z=0,15)): for n from 0 to 12 do P[n]:=sort(coeff(Gser,z,n)) od: 1; 1; for n from 2 to 12 do seq(coeff(P[n],t,j),j=0..n-2) od; # yields sequence in triangular form",
				"# second Maple program:",
				"b:= proc(n, y, t) option remember; expand(`if`(y\u003en, 0, `if`(n=0, 1,",
				"      `if`(t\u003c0, 0, b(n-1, y+1, 1))+`if`(y\u003c1, 0, b(n-1, y-1, 0))+",
				"      `if`(t\u003e0 or y\u003c1, 0, b(n-1, y-1, -1)*`if`(t\u003c0, z, 1)))))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, z, i), i=0..degree(p)))(b(2*n, 0$2)):",
				"seq(T(n), n=0..14);  # _Alois P. Heinz_, Jun 19 2016"
			],
			"mathematica": [
				"b[n_, y_, t_] := b[n, y, t] = Expand[If[y \u003e n, 0, If[n == 0, 1, If[t \u003c 0, 0, b[n - 1, y + 1, 1]] + If[y \u003c 1, 0, b[n - 1, y - 1, 0]] + If[t \u003e 0 || y \u003c 1, 0, b[n - 1, y - 1, -1]*If[t \u003c 0, z, 1]]]]]; T[n_] := Function[p, Table[Coefficient[p, z, i], {i, 0, Exponent[p, z]}]][b[2*n, 0, 0]]; Table[T[n], {n, 0, 14}] // Flatten (* _Jean-François Alcover_, Oct 24 2016, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A002212, A128725, A128726."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_Emeric Deutsch_, Mar 31 2007",
			"references": 3,
			"revision": 21,
			"time": "2017-07-23T02:41:03-04:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}