{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A111932",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 111932,
			"data": "1,2,1,4,6,2,8,8,1,12,12,4,14,16,6,16,18,2,20,24,8,24,24,8,31,28,1,32,30,12,32,32,12,36,48,4,38,40,14,48,42,16,44,48,6,48,48,16,57,62,18,56,54,2,72,64,20,60,60,24,62,64,8,64,84,24,68,72,24,96,72,8,74,76,31",
			"name": "Expansion of q * (psi(q) * psi(q^3))^2 in powers of q where psi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Cubic AGM theta functions: a(q) (see A004016), b(q) (A005928), c(q) (A005882)."
			],
			"reference": [
				"B. C. Berndt, Ramanujan's Notebooks Part III, Springer-Verlag, see p. 223 Entry 3(iii).",
				"N. J. Fine, Basic Hypergeometric Series and Applications, Amer. Math. Soc., 1988; p. 87, Eq. (33.2)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A111932/b111932.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of (1/3) * (b(q^2)^2 / b(q))* (c(q^2)^2 / c(q)) in powers of q where b(), c() are cubic AGM theta functions.",
				"Expansion of (eta(q^2) * eta(q^6))^4 / (eta(q) * eta(q^3))^2 in powers of q.",
				"Euler transform of period 6 sequence [ 2, -2, 4, -2, 2, -4, ...].",
				"Multiplicative with a(2^e) = 2^e, a(3^e) = 1, a(p^e) = (p^(e+1) - 1) / (p - 1) if p\u003e3.",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2), A(x^4)) where f(u, v, w) = u*w * (u - 4*v) - v * (v - 4*w)^2.",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (6 t)) = (3/4) (t/i)^2 g(t) where q = exp(2 Pi i t) and g() is the g.f. for A131946. - _Michael Somos_, Sep 19 2013",
				"G.f.: Sum_{k\u003e0} k * x^k * (1 - x^(2*k))^2 / (1 - x^(6*k)) = x * Product_{k\u003e0} ((1 + x^k) * (1 + x^(3*k)))^4 * ((1 - x^k) * (1 - x^(3*k)))^2.",
				"a(3*n) = a(n), a(2*n) = 2 * a(n).",
				"Convolution square of A033762. - _Michael Somos_, Sep 19 2013"
			],
			"example": [
				"G.f. = q + 2*q^2 + q^3 + 4*q^4 + 6*q^5 + 2*q^6 + 8*q^7 + 8*q^8 + q^9 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 1, 0, Sum[ Mod[n/d, 2] d KroneckerSymbol[ 9, d], { d, Divisors[ n]}]]; (* _Michael Somos_, Sep 19 2013 *)",
				"a[ n_] := SeriesCoefficient[ q (QPochhammer[ q^2] QPochhammer[ q^6])^4 / (QPochhammer[ q] QPochhammer[ q^3])^2, {q, 0, n}]; (* _Michael Somos_, Sep 19 2013 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, 0, sumdiv(n, d, (n/d % 2) * d * (d%3\u003e0)))};",
				"(PARI) {a(n) = local(A, p, e); if( n\u003c1, 0, A = factor(n); prod(k=1, matsize(A)[1], if( p=A[k,1], e=A[k,2]; if( p==2, p^e, if( p==3, 1, (p^(e+1) - 1) / (p-1)))))) };",
				"(PARI) {a(n) = local(A); if( n\u003c1, 0, n--; A = x * O(x^n); polcoeff( (eta(x^2 + A) * eta(x^6 + A))^4 / (eta(x + A) * eta(x^3 + A))^2, n))};",
				"(Sage) A = ModularForms( Gamma0(6), 2, prec=50) . basis();  A[1] + 2*A[2]; # _Michael Somos_, Sep 19 2013"
			],
			"xref": [
				"Cf. A033762, A131946."
			],
			"keyword": "nonn,mult",
			"offset": "1,2",
			"author": "_Michael Somos_, Aug 21 2005, Apr 18 2007",
			"references": 6,
			"revision": 19,
			"time": "2021-03-12T22:24:43-05:00",
			"created": "2005-09-21T03:00:00-04:00"
		}
	]
}