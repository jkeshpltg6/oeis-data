{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A322620",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 322620,
			"data": "1,1,1,1,2,1,1,6,6,1,1,16,30,16,1,1,40,140,140,40,1,1,96,615,1040,615,96,1,1,224,2562,7000,7000,2562,224,1,1,512,10220,43904,68390,43904,10220,512,1,1,1152,39384,260736,605808,605808,260736,39384,1152,1,1,2560,147645,1482240,4998210,7322112,4998210,1482240,147645,2560,1,1,5632,541310,8131200,39032400,80735424,80735424,39032400,8131200,541310,5632,1,1,12288,1948650,43310080,291662415,831080448,1161583500,831080448,291662415,43310080,1948650,12288,1",
			"name": "E.g.f.: A(x,y) = (cosh(x)*cosh(y) + sinh(x) + sinh(y)) / (1 - sinh(x)*sinh(y)), where A(x,y) = Sum_{n\u003e=0} Sum_{k\u003e=0} T(n,k) * x^n*y^k/(n+k)!, as a square table of coefficients T(n,k) read by antidiagonals.",
			"comment": [
				"Compare to the addition theorem of Jacobi's elliptic functions: cn(x+y) + i*sn(x+y) = (cn(x) + i*sn(x)*dn(y)) * (cn(y) + i*sn(y)*dn(x)) / (1 - k^2*sn(x)^2*sn(y)^2), where the modulus k is implicit.",
				"See A322190 for another description of the e.g.f. of this sequence."
			],
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A322620/b322620.txt\"\u003eTable of n, a(n) for n = 0..1325 terms of this square table read by antidiagonals across rows 0..50.\u003c/a\u003e"
			],
			"formula": [
				"E.g.f.: A(x,y) = (cosh(x) + sinh(x)*cosh(y)) * (cosh(y) + sinh(y)*cosh(x)) / (1 - sinh(x)^2*sinh(y)^2).",
				"E.g.f.: A(x,y) = (cosh(x) + sinh(x)*cosh(y)) / (cosh(y) - sinh(y)*cosh(x)).",
				"E.g.f.: A(x,y) = (cosh(y) + sinh(y)*cosh(x)) / (cosh(x) - sinh(x)*cosh(y)).",
				"E.g.f.: A(x,y) = C(x,y) + S(x,y) such that the following identities hold.",
				"(1) C(x,y)^2 - S(x,y)^2 = 1.",
				"(2a) C(x,y) = cosh(x) * cosh(y) / (1 - sinh(x)*sinh(y)).",
				"(2b) S(x,y) = (sinh(x) + sinh(y)) / (1 - sinh(x)*sinh(y)).",
				"(3a) cosh(x) = C(x,y) * cosh(y) / (1 + sinh(y)*S(x,y)).",
				"(3b) sinh(x) = (S(x,y) - sinh(y)) / (1 + sinh(y)*S(x,y)).",
				"(3c) cosh(y) = C(x,y) * cosh(x) / (1 + sinh(x)*S(x,y)).",
				"(3d) sinh(y) = (S(x,y) - sinh(x)) / (1 + sinh(x)*S(x,y)).",
				"(4a) exp(x) = (C(x,y)*cosh(y) + S(x,y) - sinh(y)) / (1 + sinh(y)*S(x,y)).",
				"(4b) exp(y) = (C(x,y)*cosh(x) + S(x,y) - sinh(x)) / (1 + sinh(x)*S(x,y)).",
				"(5a) exp(x) = (C(x,y) + S(x,y)*cosh(y)) * (cosh(y) - sinh(y)*C(x,y)) / (1 - sinh(y)^2*S(x,y)^2).",
				"(5b) exp(y) = (C(x,y) + S(x,y)*cosh(x)) * (cosh(x) - sinh(x)*C(x,y)) / (1 - sinh(x)^2*S(x,y)^2).",
				"(6a) exp(x) = (C(x,y) + S(x,y)*cosh(y)) / (cosh(y) + sinh(y)*C(x,y)).",
				"(6b) exp(y) = (C(x,y) + S(x,y)*cosh(x)) / (cosh(x) + sinh(x)*C(x,y)).",
				"SPECIAL ARGUMENTS.",
				"A(x, y=0) = exp(x).",
				"A(x, y=x) = (1 + sinh(x)) / (1 - sinh(x)).",
				"A(x, y=-x) = 1.",
				"FORMULAS FOR TERMS.",
				"a(n) = binomial(n,k) * A322190(n,k).",
				"Sum_{k=0..n} 2^k * T(n,k) = A245140(n).",
				"Sum_{k=0..n} 3^k * T(n,k) = A245155(n).",
				"Sum_{k=0..n} 2^(n-k) * 3^k * T(n,k) = A245166(n)."
			],
			"example": [
				"E.g.f.: A(x,y) = 1 + (1*x + 1*y) + (1*x^2 + 2*x*y + 1*y^2)/2! + (1*x^3 + 6*x^2*y + 6*x*y^2 + 1*y^3)/3! + (1*x^4 + 16*x^3*y + 30*x^2*y^2 + 16*x*y^3 + 1*y^4)/4! + (1*x^5 + 40*x^4*y + 140*x^3*y^2 + 140*x^2*y^3 + 40*x*y^4 + 1*y^5)/5! + (1*x^6 + 96*x^5*y + 615*x^4*y^2 + 1040*x^3*y^3 + 615*x^2*y^4 + 96*x*y^5 + 1*y^6)/6! + (1*x^7 + 224*x^6*y + 2562*x^5*y^2 + 7000*x^4*y^3 + 7000*x^3*y^4 + 2562*x^2*y^5 + 224*x*y^6 + 1*y^7)/7! + (1*x^8 + 512*x^7*y + 10220*x^6*y^2 + 43904*x^5*y^3 + 68390*x^4*y^4 + 43904*x^3*y^5 + 10220*x^2*y^6 + 512*x*y^7 + 1*y^8)/8! + ...",
				"where A(x,y) = (cosh(x)*cosh(y) + sinh(x) + sinh(y)) / (1 - sinh(x)*sinh(y)).",
				"This square table of coefficients of x^n*y^k/(n+k)! in A(x,y) begins",
				"1, 1, 1, 1, 1, 1, 1, 1, 1, ...;",
				"1, 2, 6, 16, 40, 96, 224, 512, 1152, ...;",
				"1, 6, 30, 140, 615, 2562, 10220, 39384, 147645, ...;",
				"1, 16, 140, 1040, 7000, 43904, 260736, 1482240, 8131200, ...;",
				"1, 40, 615, 7000, 68390, 605808, 4998210, 39032400, 291662415, ...;",
				"1, 96, 2562, 43904, 605808, 7322112, 80735424, 831080448, 8105175936, ...;",
				"1, 224, 10220, 260736, 4998210, 80735424, 1161583500, 15355426944, ...;",
				"1, 512, 39384, 1482240, 39032400, 831080448, 15355426944, 256124504064, ...; ...",
				"This sequence may be written as a triangle, starting as",
				"1,",
				"1, 1,",
				"1, 2, 1,",
				"1, 6, 6, 1;",
				"1, 16, 30, 16, 1;",
				"1, 40, 140, 140, 40, 1;",
				"1, 96, 615, 1040, 615, 96, 1;",
				"1, 224, 2562, 7000, 7000, 2562, 224, 1;",
				"1, 512, 10220, 43904, 68390, 43904, 10220, 512, 1;",
				"1, 1152, 39384, 260736, 605808, 605808, 260736, 39384, 1152, 1;",
				"1, 2560, 147645, 1482240, 4998210, 7322112, 4998210, 1482240, 147645, 2560, 1; ...",
				"RELATED SERIES.",
				"The series expansions for C(x,y) and S(x,y) are given by",
				"C(x,y) = 1 + (1*x^2 + 2*x*y + 1*y^2)/2! + (1*x^4 + 16*x^3*y + 30*x^2*y^2 + 16*x*y^3 + 1*y^4)/4! + (1*x^6 + 96*x^5*y + 615*x^4*y^2 + 1040*x^3*y^3 + 615*x^2*y^4 + 96*x*y^5 + 1*y^6)/6! + (1*x^8 + 512*x^7*y + 10220*x^6*y^2 + 43904*x^5*y^3 + 68390*x^4*y^4 + 43904*x^3*y^5 + 10220*x^2*y^6 + 512*x*y^7 + 1*y^8)/8! + ...",
				"S(x,y) = (1*x + 1*y) + (1*x^3 + 6*x^2*y + 6*x*y^2 + 1*y^3)/3! + (1*x^5 + 40*x^4*y + 140*x^3*y^2 + 140*x^2*y^3 + 40*x*y^4 + 1*y^5)/5! + (1*x^7 + 224*x^6*y + 2562*x^5*y^2 + 7000*x^4*y^3 + 7000*x^3*y^4 + 2562*x^2*y^5 + 224*x*y^6 + 1*y^7)/7! + ...",
				"where A(x,y) = C(x,y) + S(x,y) such that C(x,y)^2 - S(x,y)^2 = 1.",
				"The e.g.f. may be written with coefficients of x^n*y^k/(n!*k!), as follows:",
				"A(x,y) = 1 + (1*x + 1*y) + (1*x^2/2! + 1*x*y + 1*y^2/2!) + (1*x^3/3! + 2*x^2*y/2! + 2*x*y^2/2! + 1*y^3/3!) + (1*x^4/4! + 4*x^3*y/3! + 5*x^2*y^2/(2!*2!) + 4*x*y^3/3! + 1*y^4/4!) + (1*x^5/5! + 8*x^4*y/4! + 14*x^3*y^2/(3!*2!) + 14*x^2*y^3/(2!*3!) + 8*x*y^4/4! + 1*y^5/5!) + (1*x^6/6! + 16*x^5*y/5! + 41*x^4*y^2/(4!*2!) + 52*x^3*y^3/(3!*3!) + 41*x^2*y^4/(2!*4!) + 16*x*y^5/5! + 1*y^6/6!) + (1*x^7/7! + 32*x^6*y/6! + 122*x^5*y^2/(5!*2!) + 200*x^4*y^3/(4!*3!) + 200*x^3*y^4/(3!*4!) + 122*x^2*y^5/(2!*5!) + 32*x*y^6/6! + 1*y^7/7!) + (1*x^8/8! + 64*x^7*y/7! + 365*x^6*y^2/(6!*2!) + 784*x^5*y^3/(5!*3!) + 977*x^4*y^4/(4!*4!) + 784*x^3*y^5/(3!*5!) + 365*x^2*y^6/(2!*6!) + 64*x*y^7/7! + 1*y^8/8!) + ...",
				"these coefficients are described by table A322190."
			],
			"mathematica": [
				"nmax = 12;",
				"t[n_, k_] := SeriesCoefficient[(Cosh[x] Cosh[y] + Sinh[x] + Sinh[y])/(1 - Sinh[x] Sinh[y]), {x, 0, n}, {y, 0, k}] (n + k)!;",
				"tt = Table[t[n, k], {n, 0, nmax}, {k, 0, nmax}];",
				"T[n_, k_] := tt[[n+1, k+1]];",
				"Table[T[n-k, k], {n, 0, nmax}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Dec 26 2018 *)"
			],
			"program": [
				"(PARI) {T(n,k) = my(X=x+x*O(x^n),Y=y+y*O(y^k));",
				"C = cosh(X)*cosh(Y)/(1 - sinh(X)*sinh(Y));",
				"S = (sinh(X) + sinh(Y))/(1 - sinh(X)*sinh(Y));",
				"(n+k)!*polcoeff(polcoeff(C + S,n,x),k,y)}",
				"/* Print as a square table */",
				"for(n=0,10,for(k=0,10,print1( T(n,k),\", \"));print(\"\"))",
				"/* Print as a triangle */",
				"for(n=0,15,for(k=0,n,print1( T(n-k,k),\", \"));print(\"\"))"
			],
			"xref": [
				"Cf. A322621 (C(x,y)), A322622 (S(x,y)), A322623 (antidiagonal sums), A322624 (main diagonal), A322625, A057711 (column 1).",
				"Cf. A322190, A245140, A245155, A245166."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Paul D. Hanna_, Dec 20 2018",
			"references": 10,
			"revision": 37,
			"time": "2018-12-29T17:40:09-05:00",
			"created": "2018-12-20T19:25:39-05:00"
		}
	]
}