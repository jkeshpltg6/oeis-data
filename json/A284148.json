{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A284148",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 284148,
			"data": "3,0,1,0,3,28,45,276,595,1128,1953,3160,4851,264540,190333,254268,18915,3366496,32385,125391168,199588483,64620,174673821,5039370820,1784859363,16908230328,165025,34420237176,58409997075,1367074573228,2294838551853,15289788305820",
			"name": "Lexicographically earliest sequence of nonnegative integers such that a(1)=3 and the sequence is p-periodic mod p for any p \u003e 0.",
			"comment": [
				"The initial term a(1)=3 seems to be the least one that leads to a sequence that does not have a polynomial closed form.",
				"The first cycles mod p of this sequence are:",
				"p  Cycle of a mod p",
				"-  ----------------",
				"1  0",
				"2  1, 0",
				"3  0, 0, 1",
				"4  3, 0, 1, 0",
				"5  3, 0, 1, 0, 3",
				"6  3, 0, 1, 0, 3, 4",
				"7  3, 0, 1, 0, 3, 0, 3",
				"8  3, 0, 1, 0, 3, 4, 5, 4",
				"9  3, 0, 1, 0, 3, 1, 0, 6, 1",
				"For k\u003e=0, let c_k denote the variant with initial term k.",
				"Naturaly, we have a=c_3.",
				"For some values of k, c_k has a polynomial closed form.",
				"The first such values to be known are:",
				"- k=0: c_0(n) = 0 = A000004(n),",
				"- k=1: c_1(n) = (n-2)^2 = A000290(n-2),",
				"- k=2: c_2(n) = (n-2)*(n-3) = A002378(n-3),",
				"- k=19: c_19(n) = (n-2)*(n^3 - 14*n^2 + 63*n - 88)/2,",
				"- k=20: c_20(n) = (n-2)*(n-3)*(n-5)*(n-6)/2,",
				"- k=22: c_22(n) = (n-2)*(n-3)*(n^2 - 11*n + 32)/2,",
				"- k=40: c_40(n) = (n-2)*(n-3)*(n-5)*(n-6),",
				"- k=172: c_172(n) = (n-2)*(n-3)*(n-5)*(n^3 - 23*n^2 + 172*n - 408)/12.",
				"We notice that c_40 = 2*c_20.",
				"As for A281409, this sequence is the first of a family (of sequences parametrized by their initial term) showing some kind of irregularity.",
				"For k\u003e=0 and n\u003e0, let d_n(k)=c_k(n):",
				"- In particular: d_1(k)=k, and a(n)=d_n(3),",
				"- For any n\u003e1, d_n is periodic.",
				"The cycles for the first d_n (with n\u003e1) are:",
				"n  Cycle of d_n",
				"-  ------------",
				"2  0",
				"3  0, 1",
				"4  0, 4, 2",
				"5  0, 9, 6, 3",
				"6  0, 16, 12, 28, 24, 40, 36, 52, 48, 4",
				"7  0, 25, 20, 45, 40, 5"
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A284148/b284148.txt\"\u003eTable of n, a(n) for n = 1..2000\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A284148/a284148.gp.txt\"\u003ePARI program for A284148\u003c/a\u003e"
			],
			"example": [
				"By definition, a(1)=3.",
				"a(2) must equal 3 mod 1; a(2)=0 is suitable.",
				"a(3) must equal 3 mod 2 and 0 mod 1; a(3)=1 is suitable.",
				"a(4) must equal 3 mod 3 and 0 mod 2 and 1 mod 1; a(4)=0 is suitable.",
				"a(5) must equal 3 mod 4 and 0 mod 3 and 1 mod 2 and 0 mod 1; a(5)=3 is suitable."
			],
			"xref": [
				"Cf. A000004, A000290, A002378, A281409."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Rémy Sigrist_, Mar 21 2017",
			"references": 1,
			"revision": 19,
			"time": "2017-03-22T21:55:08-04:00",
			"created": "2017-03-22T21:55:08-04:00"
		}
	]
}