{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A294019",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 294019,
			"data": "0,1,1,1,1,0,1,1,1,0,1,2,1,0,0,2,1,0,1,0,0,0,1,0,1,0,1,0,1,0,1,1,0,0,0,3,1,0,0,2,1,0,1,0,0,0,1,3,1,0,0,0,1,0,0,0,0,0,1,0,1,0,2,3,0,0,1,0,0,0,1,0,1,0,0,0,0,0,1,0,2,0,1,4,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,1,0,1,0,0,0,1,8",
			"name": "Number of same-trees whose leaves are the parts of the integer partition with Heinz number n.",
			"comment": [
				"By convention a(1) = 0.",
				"The Heinz number of an integer partition (y_1,...,y_k) is prime(y_1)*...*prime(y_k)."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A294019/b294019.txt\"\u003eTable of n, a(n) for n = 1..65537\u003c/a\u003e"
			],
			"formula": [
				"A281145(n) = Sum_{i=1..A000041(n)} a(A215366(n,i)).",
				"a(p^n) = A006241(n) for any prime p and exponent n \u003e= 1. - _Antti Karttunen_, Sep 22 2018"
			],
			"example": [
				"The a(108) = 8 same-trees: ((22)(2(11))), ((22)((11)2)), ((2(11))(22)), (((11)2)(22)), (222(11)), (22(11)2), (2(11)22), ((11)222).",
				"From _Antti Karttunen_, Sep 22 2018: (Start)",
				"For 12 = prime(1)^2 * prime(2)^1, we have the following two cases: 2(11) and (11)2, thus a(12) = 2.",
				"For 36 = prime(1)^2 * prime(2)^2, we have the following cases: (11)22, 2(11)2, 22(11), thus a(36) = 3.",
				"For 144  = prime(1)^4 * prime(2)^2, we have the following 14 cases: (1111)(22), (22)(1111); ((11)(11))(22), (22)((11)(11)); (11)(11)22, (11)2(11)2, (11)22(11), 2(11)2(11), 2(11)(11)2, 22(11)(11); ((11)2)(11(2)), ((11)2)(2(11)), (2(11))((11)2), (2(11))(2(11)), thus a(144) = 14.",
				"For n = 8775 = 3^3 * 5^2 * 13^1 = prime(2)^3 * prime(3)^2 * prime(6)^1, we have the following six cases: (222)(33)6, (222)6(33), (33)(222)6, (33)6(222), 6(222)(33), 6(33)(222), thus a(8775) = 6.",
				"(End)"
			],
			"mathematica": [
				"nn=120;",
				"ptns=Table[If[n===1,{},Join@@Cases[FactorInteger[n]//Reverse,{p_,k_}:\u003eTable[PrimePi[p],{k}]]],{n,nn}];",
				"tris=Join@@Map[Tuples[IntegerPartitions/@#]\u0026,ptns];",
				"qci[y_]:=qci[y]=If[Length[y]===1,1,Sum[Times@@qci/@t,{t,Select[tris,And[Length[#]\u003e1,Sort[Join@@#,Greater]===y,SameQ@@Total/@#]\u0026]}]];",
				"qci/@ptns"
			],
			"program": [
				"(PARI)",
				"A056239(n) = { my(f); if(1==n, 0, f=factor(n); sum(i=1, #f~, f[i,2] * primepi(f[i,1]))); }",
				"productifbalancedfactorization(v) = if(!#v, 1, my(pw=A056239(v[1]), m=1); for(i=1,#v,if(A056239(v[i])!=pw,return(0), m *= A294019(v[i]))); (m));",
				"A294019aux(n, m, facs) = if(1==n, productifbalancedfactorization(Vec(facs)), my(s=0, newfacs); fordiv(n, d, if((d\u003e1)\u0026\u0026(d\u003c=m), newfacs = List(facs); listput(newfacs,d); s += A294019aux(n/d, m, newfacs))); (s));",
				"A294019(n) = if(1==n,0,if(isprime(n),1,A294019aux(n, n-1, List([]))));",
				"\\\\ A memoized implementation:",
				"map294019 = Map();",
				"A294019(n) = if(1==n,0,if(isprime(n),1,if(mapisdefined(map294019,n), mapget(map294019,n), my(v=A294019aux(n, n-1, List([]))); mapput(map294019,n,v); (v)))); \\\\ _Antti Karttunen_, Sep 22 2018"
			],
			"xref": [
				"Cf. A000005, A000041, A000720, A001222, A006241, A056239, A063834, A196545, A215366, A273873, A281145, A289501, A296150, A299200, A299201, A299202, A299203, A294018, A294080."
			],
			"keyword": "nonn",
			"offset": "1,12",
			"author": "_Gus Wiseman_, Feb 07 2018",
			"references": 3,
			"revision": 14,
			"time": "2018-09-24T08:54:07-04:00",
			"created": "2018-02-23T11:08:24-05:00"
		}
	]
}