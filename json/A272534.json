{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A272534",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 272534,
			"data": "4,1,5,8,2,3,3,8,1,6,3,5,5,1,8,6,7,4,2,0,3,4,8,4,5,6,8,8,1,0,2,5,0,3,3,2,4,3,3,1,6,9,5,2,1,2,5,5,4,4,7,6,7,2,8,1,4,3,6,3,9,4,7,7,6,4,7,6,5,6,5,1,3,2,8,1,4,8,7,5,2,6,0,9,2,5,7,5,1,3,4,4,5,4,5,5,1,4,6,1,1,5,7,3,0",
			"name": "Decimal expansion of the edge length of a regular 15-gon with unit circumradius.",
			"comment": [
				"15-gon is the first m-gon with odd composite m which is constructible (see A003401) in virtue of the fact that 15 is the product of two distinct Fermat primes (A019434). The next such case is 51-gon (m=3*17), followed by 85-gon (m=5*17), 771-gon (m=3*257), etc.",
				"From _Wolfdieter Lang_, Apr 29 2018: (Start)",
				"This constant appears in a historic problem posed by Adriaan van Roomen (Adrianus Romanus) in his Ideae mathematicae from 1593, solved by Viète. See the Havil reference, problem 4, pp. 69-74. See also the comments in A302711 with a link to Romanus' book, Exemplum quaesitum.",
				"This problem is equivalent to R(45, 2*sin(Pi/675)) = 2*sin(Pi/15), with a special case of monic Chebyshev polynomials of the first kind, named R, given in A127672. For the constant 2*sin(Pi/675) see A302716. (End)"
			],
			"reference": [
				"Julian Havil, The Irrationals, A Story of the Numbers You Can't Count On, Princeton University Press, Princeton and Oxford, 2012, pp. 69-74."
			],
			"link": [
				"Stanislav Sykora, \u003ca href=\"/A272534/b272534.txt\"\u003eTable of n, a(n) for n = 0..2000\u003c/a\u003e",
				"Mauro Fiorentini, \u003ca href=\"http://www.bitman.name/math/article/264\"\u003eConstruibili (numeri)\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ConstructibleNumber.html\"\u003eConstructible Number\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Constructible_number\"\u003eConstructible number\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Regular_polygon\"\u003eRegular polygon\u003c/a\u003e",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e"
			],
			"formula": [
				"Equals 2*sin(Pi/m) for m=15.",
				"Also equals (sqrt(3) - sqrt(15) + sqrt(10 + 2*sqrt(5)))/4.",
				"Also equals sqrt(7 - sqrt(5) - sqrt(30 - 6*sqrt(5)))/2. This is the rewritten expression of the Havil reference on top of p. 70. - _Wolfdieter Lang_, Apr 29 2018"
			],
			"example": [
				"0.415823381635518674203484568810250332433169521255447672814363947..."
			],
			"mathematica": [
				"RealDigits[N[2Sin[Pi/15], 100]][[1]] (* _Robert Price_, May 02 2016*)"
			],
			"program": [
				"(PARI) 2*sin(Pi/15)"
			],
			"xref": [
				"Cf. A003401, A019434, A127672, A302711, A302716.",
				"Edge lengths of other constructible m-gons: A002194 (m=3), A002193 (4), A182007 (5), A101464 (8), A094214 (10), A101263 (12), A272535 (16), A228787 (17), A272536 (20)."
			],
			"keyword": "nonn,cons,easy",
			"offset": "0,1",
			"author": "_Stanislav Sykora_, May 02 2016",
			"references": 7,
			"revision": 19,
			"time": "2018-05-03T17:37:17-04:00",
			"created": "2016-05-07T00:28:13-04:00"
		}
	]
}