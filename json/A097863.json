{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097863",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97863,
			"data": "1,3,4,7,6,12,8,15,13,18,12,28,14,24,24,31,18,39,20,42,32,36,24,60,31,42,40,56,30,72,32,33,48,54,48,91,38,60,56,90,42,96,44,84,78,72,48,124,57,93,72,98,54,120,72,120,80,90,60,168,62,96,104,99,84,144,68,126,96,144,72,195,74,114,124",
			"name": "Sum of 5-infinitary divisors of n.",
			"comment": [
				"If n=Product p_i^r_i and d=Product p_i^s_i, each s_i has a digit a\u003c=b in its 5-ary expansion everywhere that the corresponding r_i has a digit b, then d is a 5-infinitary-divisor of n."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A097863/b097863.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"Denote by P_5={p^5^k} the two-parameter set when k=0,1,... and p runs prime values. Then every n has a unique representation of the form n=prod q_i prod (r_j)^2 prod (s_k)^3 prod (t_m)^4, where q_i, r_j, s_k, t_m are distinct elements of P_5. Using this representation, we have a(n)=prod (q_i+1)prod ((r_j)^2+r_j+1)prod ((s_k)^3+(s_k)^2+s_k+1) prod ((t_m)^4+(t_m)^3+(t_m)^2+t_m+1). - _Vladimir Shevelev_, May 08 2013"
			],
			"example": [
				"a(32) = a(2^10) = 2^10 + 2^0 = 32 + 1 = 33, in 5-ary expansion. This is the first term which is different from sigma(n)."
			],
			"maple": [
				"A097863 := proc(n) option remember; local ifa, a, p, e, d, k ; ifa := ifactors(n)[2] ; a := 1 ; if nops(ifa) = 1 then p := op(1, op(1, ifa)) ; e := op(2, op(1, ifa)) ; d := convert(e, base, 5) ; for k from 0 to nops(d)-1 do a := a*(p^((1+op(k+1, d))*5^k)-1)/(p^(5^k)-1) ; end do: else for d in ifa do a := a*procname( op(1, d)^op(2, d)) ; end do: return a; end if; end proc:"
			],
			"mathematica": [
				"f[p_, e_] := Module[{d = IntegerDigits[e, 5]}, m = Length[d]; Product[(p^((d[[j]] + 1)*5^(m - j)) - 1)/(p^(5^(m - j)) - 1), {j, 1, m}]]; a[1] = 1; a[n_] := Times @@ f @@@ FactorInteger[n]; Array[a, 100] (* _Amiram Eldar_, Sep 09 2020 *)"
			],
			"program": [
				"(Haskell)  following Bower and Harris, cf. A049418:",
				"a097863 1 = 1",
				"a097863 n = product $ zipWith f (a027748_row n) (a124010_row n) where",
				"   f p e = product $ zipWith div",
				"           (map (subtract 1 . (p ^)) $",
				"                zipWith (*) a000351_list $ map (+ 1) $ a031235_row e)",
				"           (map (subtract 1 . (p ^)) a000351_list)",
				"-- _Reinhard Zumkeller_, Sep 18 2015"
			],
			"xref": [
				"Cf. A049417, A049418, A074847, A097464.",
				"Cf. A000351, A031235, A027748, A124010."
			],
			"keyword": "nonn,mult",
			"offset": "1,2",
			"author": "_Yasutoshi Kohmoto_",
			"references": 7,
			"revision": 24,
			"time": "2020-09-09T03:40:14-04:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}