{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A257479",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 257479,
			"data": "2,6,12,24",
			"name": "Maximal kissing number in n dimensions: maximal number of unit spheres that can touch another unit sphere.",
			"comment": [
				"Two additional dimensions have been solved: a(8) = 240, a(24) = 196560 [Odlyzko and Sloane; Levenstein].",
				"Lower bounds for a(5) onwards are 40, 72, 126, 240 (exact), 306, 500, ... - _N. J. A. Sloane_, May 15 2015",
				"It seems that, while n is even, a lower bound for a(n) can be written as f(n) = (2n + 2^n) - k(n)^2, where k(2) = 2, for n \u003e 2, k(n) = 2^(n/2) - q, q = {2^t, 3*2^t}, t is integer, t \u003e 0, 2 \u003c= q \u003c= n, and f(n) \u003c= a(n) (Note: for n \u003c= 24, q = n at n = {4, 6, 8, 24}, q = 0 at n = 2). - _Sergey Pavlov_, Mar 17 2017",
				"It also seems that, while n is even, an upper bound for a(n) can be written as f(n) = (2n + 2^n) - k(n)^2, where k(2) = 0, for n \u003e 2, k(n) = 2^(n/2) - q, q = {2^t, 3*2^t}, t is integer, t \u003e 0, n \u003c= q \u003c= 2n, and f(n) \u003e= a(n) (Note: for n \u003c= 24, q = n at n = {2, 4, 12, 16}). - _Sergey Pavlov_, Mar 19 2017"
			],
			"reference": [
				"J. H. Conway and N. J. A. Sloane, \"Sphere Packings, Lattices and Groups\", Springer-Verlag, 3rd. ed., Chap. 3, esp. pp. xxi, 23, etc.",
				"Musin, Oleg Rustumovich. \"The problem of the twenty-five spheres.\" Russian Mathematical Surveys 58.4 (2003): 794-795."
			],
			"link": [
				"Eiichi Bannai and N. J. A. Sloane, \u003ca href=\"http://dx.doi.org/10.4153/CJM-1981-038-7\"\u003eUniqueness of certain spherical codes\u003c/a\u003e, Canad. J. Math. 33 (1981), no. 2, 437-449.",
				"J. Leech, \u003ca href=\"http://www.jstor.org/stable/3610264\"\u003eThe problem of the thirteen spheres\u003c/a\u003e, Math. Gaz., 40 (1956), 22-23.",
				"V. I. Levenshtein, \u003ca href=\"https://doi.org/10.4213/rm651\"\u003eOn bounds for packings in n-dimensional Euclidean space\u003c/a\u003e, Dokl. Akad. Nauk., 245 (1979), 1299-1303; English translation in Soviet Math. Doklady, 20 (1979), 417-421.",
				"Mittelmann, Hans D.; Vallentin, Frank, \u003ca href=\"https://arxiv.org/abs/0902.1105\"\u003e\"High accuracy semidefinite programming bounds for kissing numbers\"\u003c/a\u003e, arXiv:0902.1105 [math.OC], 2009; Exp. Math. (2009), no. 19, 174-178.",
				"G. Nebe and N. J. A. Sloane, \u003ca href=\"http://www.math.rwth-aachen.de/~Gabriele.Nebe/LATTICES/kiss.html\"\u003eTable of highest kissing numbers known\u003c/a\u003e",
				"A. M. Odlyzko and N. J. A. Sloane, \u003ca href=\"https://doi.org/10.1016/0097-3165(79)90074-8\"\u003eNew bounds on the number of unit spheres that can touch a unit sphere in n dimensions\u003c/a\u003e, J. Combin. Theory Ser. A 26 (1979), no. 2, 210-214.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Kissing_number_problem\"\u003eKissing number problem\u003c/a\u003e"
			],
			"example": [
				"For a(2), the maximal number of pennies that can touch one penny is 6.",
				"For a(3), the most spheres that can simultaneously touch a central sphere of the same radius is 12."
			],
			"xref": [
				"Cf. A001116 (n-dimensional lattice), A002336 (n-dimensional laminated lattice), A028923 (n-dimensional lattice Kappa_n).",
				"Cf. A008408."
			],
			"keyword": "nonn,bref",
			"offset": "1,1",
			"author": "_Peter Woodward_, Apr 25 2015",
			"ext": [
				"Entry revised by _N. J. A. Sloane_, May 08 2015"
			],
			"references": 4,
			"revision": 52,
			"time": "2019-12-11T02:29:52-05:00",
			"created": "2015-05-08T11:41:04-04:00"
		}
	]
}