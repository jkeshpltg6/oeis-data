{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A052307",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 52307,
			"data": "1,1,1,1,1,1,1,1,1,1,1,1,2,1,1,1,1,2,2,1,1,1,1,3,3,3,1,1,1,1,3,4,4,3,1,1,1,1,4,5,8,5,4,1,1,1,1,4,7,10,10,7,4,1,1,1,1,5,8,16,16,16,8,5,1,1,1,1,5,10,20,26,26,20,10,5,1,1,1,1,6,12,29,38,50,38,29,12,6,1,1,1,1,6,14,35,57,76,76,57,35,14,6,1,1",
			"name": "Triangle read by rows: T(n,k) = number of bracelets (reversible necklaces) with n beads, k of which are black and n - k are white.",
			"comment": [
				"Equivalently, T(n,k) is the number of orbits of k-element subsets of the vertices of a regular n-gon under the usual action of the dihedral group D_n, or under the action of Euclidean plane isometries. Note that each row of the table is symmetric and unimodal. - _Austin Shapiro_, Apr 20 2009",
				"Also, the number of k-chords in n-tone equal temperament, up to (musical) transposition and inversion. Example: there are 29 tetrachords, 38 pentachords, 50 hexachords in the familiar 12-tone equal temperament. Called \"Forte set-classes,\" after Allen Forte who first catalogued them. - _Jon Wild_, May 21 2004"
			],
			"reference": [
				"Martin Gardner, \"New Mathematical Diversions from Scientific American\" (Simon and Schuster, New York, 1966), pages 245-246.",
				"N. Zagaglia Salvi, Ordered partitions and colourings of cycles and necklaces, Bull. Inst. Combin. Appl., 27 (1999), 37-40."
			],
			"link": [
				"Washington Bomfim, \u003ca href=\"/A052307/b052307.txt\"\u003eRows n = 0..130, flattened\u003c/a\u003e",
				"N. J. Fine, \u003ca href=\"http://projecteuclid.org/euclid.ijm/1255381350\"\u003eClasses of periodic sequences\u003c/a\u003e, Illinois J. Math., 2 (1958), 285-302.",
				"E. N. Gilbert and J. Riordan, \u003ca href=\"http://projecteuclid.org/euclid.ijm/1255631587\"\u003eSymmetry types of periodic sequences\u003c/a\u003e, Illinois J. Math., 5 (1961), 657-665.",
				"G. Gori, S. Paganelli, A. Sharma, P. Sodano, and A. Trombettoni, \u003ca href=\"http://arxiv.org/abs/1405.3616\"\u003eBell-Paired States Inducing Volume Law for Entanglement Entropy in Fermionic Lattices\u003c/a\u003e, arXiv:1405.3616 [cond-mat.stat-mech], 2014. See Section V.",
				"H. Gupta, \u003ca href=\"https://web.archive.org/web/20200806162943/https://www.insa.nic.in/writereaddata/UpLoadedFiles/IJPAM/20005a66_964.pdf\"\u003eEnumeration of incongruent cyclic k-gons\u003c/a\u003e, Indian J. Pure and Appl. Math., 10(8) (1979), 964-999.",
				"S. Karim, J. Sawada, Z. Alamgirz, and S. M. Husnine, \u003ca href=\"https://doi.org/10.1016/j.tcs.2012.11.024\"\u003eGenerating bracelets with fixed content\u003c/a\u003e, Theoretical Computer Science, 475 (2013), 103-112.",
				"John P. McSorley and Alan H. Schoen, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2012.08.021\"\u003eRhombic tilings of (n, k)-ovals, (n, k, lambda)-cyclic difference sets, and related topics\u003c/a\u003e, Discrete Math., 313 (2013), 129-154. - From _N. J. A. Sloane_, Nov 26 2012",
				"A. L. Patterson, \u003ca href=\"http://dx.doi.org/10.1103/PhysRev.65.195\"\u003eAmbiguities in the X-Ray Analysis of Crystal Structures\u003c/a\u003e, Phys. Rev. 65 (1944), 195 - 201 (see Table I). [From _N. J. A. Sloane_, Mar 14 2009]",
				"Richard H. Reis, \u003ca href=\"https://web.archive.org/web/20200803213425/https://www.insa.nic.in/writereaddata/UpLoadedFiles/IJPAM/20005a66_1000.pdf\"\u003eA formula for C(T) in Gupta's paper\u003c/a\u003e, Indian J. Pure and Appl. Math., 10(8) (1979), 1000-1001.",
				"V. Shevelev, \u003ca href=\"http://www.math.bgu.ac.il/~shevelev/Shevelev_Neclaces.pdf\"\u003eNecklaces and convex k-gons\u003c/a\u003e, Indian J. Pure and Appl. Math., 35(5) (2004), 629-638.",
				"V. Shevelev, \u003ca href=\"https://web.archive.org/web/20200722171019/http://www.insa.nic.in/writereaddata/UpLoadedFiles/IJPAM/2000c4e8_629.pdf\"\u003eNecklaces and convex k-gons\u003c/a\u003e, Indian J. Pure and Appl. Math., 35(5) (2004), 629-638.",
				"\u003ca href=\"/index/Br#bracelets\"\u003eIndex entries for sequences related to bracelets\u003c/a\u003e"
			],
			"formula": [
				"T(0,0) = 1. If n \u003e 0, T(n,k) = binomial(floor(n/2) - (k mod 2) * (1 - (n mod 2)), floor(k/2)) / 2 + Sum_{d|n, d|k} (phi(d)*binomial(n/d, k/d)) / (2*n). - _Washington Bomfim_, Jun 30 2012 [edited by _Petros Hadjicostas_, May 29 2019]",
				"From _Freddy Barrera_, Apr 21 2019: (Start)",
				"T(n,k) = (1/2) * (A119963(n,k) + A047996(n,k)).",
				"T(n,k) = T(n, n-k) for each k \u003c n (Theorem 2 of H. Gupta). (End)",
				"G.f. for column k \u003e= 1: (x^k/2) * ((1/k) * Sum_{m|k} phi(m)/(1 - x^m)^(k/m) + (1 + x)/(1 - x^2)^floor((k/2) + 1)). (This formula is due to Herbert Kociemba.) - _Petros Hadjicostas_, May 25 2019",
				"Bivariate o.g.f.: Sum_{n,k \u003e= 0} T(n, k)*x^n*y^k = (1/2) * ((x + 1) * (x*y + 1) / (1 - x^2 * (y^2 + 1)) + 1 - Sum_{d \u003e= 1} (phi(d)/d) * log(1 - x^d * (1 + y^d))). - _Petros Hadjicostas_, Jun 13 2019"
			],
			"example": [
				"Triangle T(n,k) (with rows n \u003e= 0 and columns k = 0..n) begins:",
				"   1;",
				"   1,  1;",
				"   1,  1,  1;",
				"   1,  1,  1,  1;",
				"   1,  1,  2,  1,  1;",
				"   1,  1,  2,  2,  1,  1;",
				"   1,  1,  3,  3,  3,  1,  1;",
				"   1,  1,  3,  4,  4,  3,  1,  1;",
				"   1,  1,  4,  5,  8,  5,  4,  1,  1;",
				"   1,  1,  4,  7, 10, 10,  7,  4,  1,  1;",
				"   1,  1,  5,  8, 16, 16, 16,  8,  5,  1,  1;",
				"   1,  1,  5, 10, 20, 26, 26, 20, 10,  5,  1,  1;",
				"   1,  1,  6, 12, 29, 38, 50, 38, 29, 12,  6,  1,  1;",
				"   ..."
			],
			"maple": [
				"A052307 := proc(n,k)",
				"        local hk,a,d;",
				"        if k = 0 then",
				"                return 1 ;",
				"        end if;",
				"        hk := k mod 2 ;",
				"        a := 0 ;",
				"        for d in numtheory[divisors](igcd(k,n)) do",
				"                a := a+ numtheory[phi](d)*binomial(n/d-1,k/d-1) ;",
				"        end do:",
				"        %/k + binomial(floor((n-hk)/2),floor(k/2)) ;",
				"        %/2 ;",
				"end proc: # _R. J. Mathar_, Sep 04 2011"
			],
			"mathematica": [
				"Table[If[m*n===0,1,1/2*If[EvenQ[n], If[EvenQ[m], Binomial[n/2, m/2], Binomial[(n-2)/2, (m-1)/2 ]], If[EvenQ[m], Binomial[(n-1)/2, m/2], Binomial[(n-1)/2, (m-1)/2]]] + 1/2*Fold[ #1 +(EulerPhi[ #2]*Binomial[n/#2, m/#2])/n \u0026, 0, Intersection[Divisors[n], Divisors[m]]]], {n,0,12}, {m,0,n}] (* _Wouter Meeussen_, Aug 05 2002, Jan 19 2009 *)"
			],
			"program": [
				"(PARI)",
				"B(n,k)={ if(n==0, return(1)); GCD = gcd(n, k); S = 0;",
				"for(d = 1, GCD, if((k%d==0)\u0026\u0026(n%d==0), S+=eulerphi(d)*binomial(n/d,k/d)));",
				"return (binomial(floor(n/2)- k%2*(1-n%2), floor(k/2))/2 + S/(2*n)); }",
				"n=0;k=0; for(L=0, 8645, print(L, \" \", B(n,k)); k++; if(k\u003en, k=0; n++))",
				"/* _Washington Bomfim_, Jun 30 2012 */",
				"(Python)",
				"from sympy import binomial as C, totient, divisors, gcd",
				"def T(n, k): return 1 if n==0 else C((n//2) - k%2 * (1 - n%2), (k//2))/2 + sum(totient(d)*C(n//d, k//d) for d in divisors(gcd(n, k)))/(2*n)",
				"for n in range(11): print([T(n, k) for k in range(n + 1)]) # _Indranil Ghosh_, Apr 23 2017"
			],
			"xref": [
				"Row sums: A000029. Columns 0-12: A000012, A000012, A008619, A001399, A005232, A032279, A005513, A032280, A005514, A032281, A005515, A032282, A005516.",
				"Cf. A047996, A051168, A052308, A052309, A052310."
			],
			"keyword": "nonn,tabl,nice",
			"offset": "0,13",
			"author": "_Christian G. Bower_, Nov 15 1999",
			"references": 27,
			"revision": 122,
			"time": "2021-03-10T17:28:51-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}