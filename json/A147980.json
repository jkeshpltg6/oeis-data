{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A147980",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 147980,
			"data": "1,0,2,0,4,4,8,12,28,44,84,156,288,540,1020,1904,3616,6860,13024,24836,47448,90772,174072,334348,643112,1238928,2389956,4615916,8925808,17278680,33482196,64944060,126083448,244989096,476416560,927167752,1805691728,3519062820",
			"name": "Given a set of positive integers A={1,2,...,n-1,n}, n\u003e=2. Take subsets of A of the form {1,...,n} so only subsets containing numbers 1 and n are allowed. Then a(1)=1 and a(n) is the number of subsets where arithmetic mean of the subset is an integer.",
			"comment": [
				"For n odd the value of the arithmetic mean for each possible subset equals (n+1)/2. For n even this value is n/2 or (n+2)/2. If looking after RootMeanSquare for the subset we obtain a sequence [1,0,0,0,0,0,2,...]. We see for example for n=7, A={1,2,3,4,5,6,7} and the only 2 subsets with an integer RootMeanSquare are {1,7}, {1,5,7}. Interestingly the value of RootMeanSquare is 5 for both subsets. So the sequence A140480 RMS numbers is a subsequence of it as a set of divisors of n is clearly a subset of n of the form {1,...,n}."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A147980/b147980.txt\"\u003eTable of n, a(n) for n = 1..100\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ArithmeticMean.html\"\u003eArithmetic mean\u003c/a\u003e"
			],
			"example": [
				"n=5, A={1,2,3,4,5}. Subsets of A starting with 1 and ending with 5 are : {1,5}, {1,2,5}, {1,3,5}, {1,4,5}, {1,2,3,5}, {1,2,4,5}, {1,3,4,5}, {1,2,3,4,5}. Arithmetic mean of the subset is an integer for subsets : {1,5}, {1,3,5}, {1,2,4,5}, {1,2,3,4,5}. Thus a(5) = 4. The value of the arithmetic mean is 3 for all 4 subsets."
			],
			"maple": [
				"b:= proc(i,s,c) option remember; `if` (i=1, `if` (irem (s, c)=0, 1, 0), b(i-1, s, c)+ b(i-1, s+i, c+1)) end: a:= n-\u003e `if` (n=1, 1, b (n-1, n+1, 2)): seq (a(n), n=1..40);  # _Alois P. Heinz_, May 06 2010"
			],
			"mathematica": [
				"b[i_, s_, c_] := b[i, s, c] = If[i==1, If[Mod[s, c]==0, 1, 0], b[i-1, s, c] + b[i-1, s+i, c+1]];",
				"a[n_] := If[n==1, 1, b[n-1, n+1, 2]];",
				"Array[a, 40] (* _Jean-François Alcover_, Nov 20 2020, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A140480."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Ctibor O. Zizka_, Nov 18 2008",
			"ext": [
				"More terms from _Alois P. Heinz_, May 06 2010"
			],
			"references": 1,
			"revision": 14,
			"time": "2020-11-20T04:52:31-05:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}