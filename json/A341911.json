{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A341911",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 341911,
			"data": "0,1,3,2,7,4,6,5,15,8,12,9,14,11,13,10,31,16,24,17,28,19,23,18,30,25,27,20,29,22,26,21,63,32,48,33,56,35,39,34,60,47,49,36,51,38,40,37,62,55,57,44,59,46,50,41,61,52,54,43,58,45,53,42,127,64,96",
			"name": "Lexicographically earliest sequence of distinct nonnegative integers such that for any n \u003e= 0, the number of ones in the binary expansion of n equals the number of runs in the binary expansion of a(n).",
			"comment": [
				"This sequence is a permutation of the nonnegative integers with inverse A341910."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A341911/b341911.txt\"\u003eTable of n, a(n) for n = 0..8191\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A341911/a341911.png\"\u003eColored scatterplot of the first 2^16 terms\u003c/a\u003e (where the color is function of A000120(n))",
				"Rémy Sigrist, \u003ca href=\"/A341911/a341911.gp.txt\"\u003ePARI program for A341911\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"A000120(n) = A005811(a(n)).",
				"a(n) \u003c 2^k for any n \u003c 2^k."
			],
			"example": [
				"The first terms, in decimal and in binary, are:",
				"  n   a(n)  bin(n)   bin(a(n))",
				"  --  ----  -------  ---------",
				"   0     0        0          0",
				"   1     1        1          1",
				"   2     3       10         11",
				"   3     2       11         10",
				"   4     7      100        111",
				"   5     4      101        100",
				"   6     6      110        110",
				"   7     5      111        101",
				"   8    15     1000       1111",
				"   9     8     1001       1000",
				"  10    12     1010       1100"
			],
			"mathematica": [
				"Block[{a = {0}, k}, Do[k = 1; While[Nand[FreeQ[a, k], Length[Split@ IntegerDigits[k, 2]] == #], k++] \u0026@ DigitCount[i, 2, 1]; AppendTo[a, k], {i, 66}]; a] (* _Michael De Vlieger_, Feb 24 2021 *)"
			],
			"program": [
				"(PARI) See Links section."
			],
			"xref": [
				"Cf. A000120, A005811, A298847, A341910 (inverse)."
			],
			"keyword": "nonn,look,base",
			"offset": "0,3",
			"author": "_Rémy Sigrist_, Feb 23 2021",
			"references": 2,
			"revision": 15,
			"time": "2021-02-24T16:25:31-05:00",
			"created": "2021-02-24T08:20:58-05:00"
		}
	]
}