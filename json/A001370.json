{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001370",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1370,
			"id": "M1085 N0414",
			"data": "1,2,4,8,7,5,10,11,13,8,7,14,19,20,22,26,25,14,19,29,31,26,25,41,37,29,40,35,43,41,37,47,58,62,61,59,64,56,67,71,61,50,46,56,58,62,70,68,73,65,76,80,79,77,82,92,85,80,70,77",
			"name": "Sum of digits of 2^n.",
			"comment": [
				"Same digital roots as A065075 (sum of digits of the sum of the preceding numbers) and A004207 (sum of digits of all previous terms); they enter into the cycle {1 2 4 8 7 5}. - _Alexandre Wajnberg_, Dec 11 2005",
				"It is believed that a(n) ~ n*9*log_10(2)/2, but this is an open problem.",
				"The Radcliffe preprint shows that a(n) \u003e log_4(n). - _M. F. Hasler_, May 18 2017"
			],
			"reference": [
				"Archimedeans Problems Drive, Eureka, 26 (1963), 12.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Zak Seidov, \u003ca href=\"/A001370/b001370.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"David Radcliffe, \u003ca href=\"/A001370/a001370_1.pdf\"\u003eThe growth of digital sums of powers of two\u003c/a\u003e. Preprint, 2015.",
				"David G. Radcliffe, \u003ca href=\"http://arxiv.org/abs/1605.02839\"\u003eThe growth of digital sums of powers of two\u003c/a\u003e, arXiv:1605.02839 [math.NT], 2016.",
				"C. L. Stewart, \u003ca href=\"http://gdz.sub.uni-goettingen.de/dms/resolveppn/?PPN=GDZPPN002197707\"\u003eOn the representation of an integer in two different bases\u003c/a\u003e, Journal für die reine und angewandte Mathematik 319 (1980): 63-72."
			],
			"formula": [
				"a(n) = A007953(A000079(n)). - _Michel Marcus_, Nov 01 2013"
			],
			"maple": [
				"seq(convert(convert(2^n,base,10),`+`),n=0..1000); # _Robert Israel_, Mar 29 2015"
			],
			"mathematica": [
				"Table[Total[IntegerDigits[2^n]], {n, 0, 55}] (* _Vincenzo Librandi_, Oct 08 2013 *)"
			],
			"program": [
				"(PARI) a(n)=sumdigits(2^n); \\\\ _Michel Marcus_, Nov 01 2013",
				"(Python) [sum(map(int, str(2**n))) for n in range(56)] # _David Radcliffe_, Mar 29 2015",
				"(Haskell)",
				"a001370 = a007953 . a000079  -- _Reinhard Zumkeller_, Aug 14 2015"
			],
			"xref": [
				"Cf. sum of digits of k^n: A004166 (k=3), A065713 (k=4), A066001(k=5), A066002 (k=6), A066003(k=7), A066004 (k=8), A065999 (k=9), A066005 (k=11), A066006 (k=12).",
				"Cf. A007953, A000079, A261009, A011754."
			],
			"keyword": "base,easy,nonn",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, _Simon Plouffe_",
			"references": 40,
			"revision": 68,
			"time": "2017-05-19T06:02:43-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}