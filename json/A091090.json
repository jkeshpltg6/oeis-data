{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A091090",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 91090,
			"data": "1,1,1,2,1,2,1,3,1,2,1,3,1,2,1,4,1,2,1,3,1,2,1,4,1,2,1,3,1,2,1,5,1,2,1,3,1,2,1,4,1,2,1,3,1,2,1,5,1,2,1,3,1,2,1,4,1,2,1,3,1,2,1,6,1,2,1,3,1,2,1,4,1,2,1,3,1,2,1,5,1,2,1,3,1,2,1,4,1,2,1,3,1,2,1,6,1,2,1,3,1,2",
			"name": "In binary representation: number of editing steps (delete, insert, or substitute) to transform n into n + 1.",
			"comment": [
				"Apparently, one less than the number of cyclotomic factors of the polynomial x^n - 1. - _Ralf Stephan_, Aug 27 2013",
				"Let the binary expansion of n \u003e= 1 end with m \u003e= 0 1's. Then a(n) = m if n = 2^m-1 and a(n) = m+1 if n \u003e 2^m-1. - _Vladimir Shevelev_, Aug 14 2017"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A091090/b091090.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Michael Gilleland, \u003ca href=\"http://www.merriampark.com/ld.htm\"\u003eLevenshtein Distance\u003c/a\u003e [broken link] [It has been suggested that this algorithm gives incorrect results sometimes. - _N. J. A. Sloane_]",
				"F. Ruskey, C. Deugau, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL12/Ruskey/ruskey6.html\"\u003eThe Combinatorics of Certain k-ary Meta-Fibonacci Sequences\u003c/a\u003e, JIS 12 (2009) 09.4.3",
				"Vladimir Shevelev, \u003ca href=\"https://arxiv.org/abs/1708.08096\"\u003eOn a Luschny question\u003c/a\u003e, arXiv:1708.08096 [math.NT], 2017.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Binary.html\"\u003eBinary\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/BinaryCarrySequence.html\"\u003eBinary Carry Sequence\u003c/a\u003e",
				"WikiBooks: Algorithm Implementation, \u003ca href=\"http://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance\"\u003eLevenshtein distance\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"formula": [
				"LevenshteinDistance(A007088(n), A007088(n + 1)).",
				"a(n) = A007814(n + 1) + 1 - A036987(n).",
				"a(n) = A152487(n + 1, n). - _Reinhard Zumkeller_, Dec 06 2008",
				"a(A004275(n)) = 1. - _Reinhard Zumkeller_, Mar 13 2011",
				"From _Vladeta Jovovic_, Aug 25 2004, fixed by _Reinhard Zumkeller_, Jun 09 2015: (Start)",
				"a(2*n) = 1, a(2*n + 1) = a(n) + 1 for n \u003e 0.",
				"G.f.: 1 + Sum_{k \u003e 0} x^(2^k - 1)/(1 - x^(2^(k - 1))). (End)",
				"Let T(x) be the g.f., then T(x) - x*T(x^2) = x/(1 - x). - _Joerg Arndt_, May 11 2010"
			],
			"maple": [
				"A091090 := proc(n)",
				"    if n = 0 then",
				"        1;",
				"    else",
				"        A007814(n+1)+1-A036987(n) ;",
				"    end if;",
				"end proc:",
				"seq(A091090(n),n=0..100); # _R. J. Mathar_, Sep 07 2016",
				"# Alternatively, explaining the connection with A135517:",
				"a := proc(n) local count, k; count := 1; k := n;",
				"while k \u003c\u003e 1 and k mod 2 \u003c\u003e 0 do count := count + 1; k := iquo(k, 2) od:",
				"count end: seq(a(n), n=0..101); # _Peter Luschny_, Aug 10 2017"
			],
			"mathematica": [
				"a[n_] := a[n] = Which[n==0, 1, n==1, 1, EvenQ[n], 1, True, a[(n-1)/2] + 1]; Array[a, 102, 0] (* _Jean-François Alcover_, Aug 12 2017 *)"
			],
			"program": [
				"(Haskell)",
				"a091090 n = a091090_list !! n",
				"a091090_list = 1 : f [1,1] where f (x:y:xs) = y : f (x:xs ++ [x,x+y])",
				"-- Same list generator function as for a036987_list, cf. A036987.",
				"-- _Reinhard Zumkeller_, Mar 13 2011",
				"(Haskell)",
				"a091090' n = levenshtein (show $ a007088 (n + 1)) (show $ a007088 n) where",
				"  levenshtein :: (Eq t) =\u003e [t] -\u003e [t] -\u003e Int",
				"  levenshtein us vs = last $ foldl transform [0..length us] vs where",
				"    transform xs@(x:xs') c = scanl compute (x+1) (zip3 us xs xs') where",
				"      compute z (c', x, y) = minimum [y+1, z+1, x + fromEnum (c' /= c)]",
				"-- _Reinhard Zumkeller_, Jun 09 2015",
				"(Haskell) -- following Vladeta Jovovic's formula",
				"import Data.List (transpose)",
				"a091090'' n = vjs !! n where",
				"   vjs = 1 : 1 : concat (transpose [[1, 1 ..], map (+ 1) $ tail vjs])",
				"-- _Reinhard Zumkeller_, Jun 09 2015",
				"(PARI) a(n)=my(m=valuation(n+1,2)); if(n\u003e\u003em, m+1, max(m, 1)) \\\\ _Charles R Greathouse IV_, Aug 15 2017"
			],
			"xref": [
				"Cf. A007088, A135517.",
				"This is Guy Steele's sequence GS(2, 4) (see A135416)."
			],
			"keyword": "nonn,base,easy",
			"offset": "0,4",
			"author": "_Reinhard Zumkeller_, Dec 19 2003",
			"references": 13,
			"revision": 65,
			"time": "2017-08-31T14:42:48-04:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}