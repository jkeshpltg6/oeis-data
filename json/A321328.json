{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A321328",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 321328,
			"data": "6,20,14,4,10,26,1012,8,1442,68,376,38,1660,14,506,574,352,117,590,22,254,1292,460,82,26108,416,266,10,3496,15,124,32,470,5176,658,362,104696,152,19305,51,12782,62,618770,232,15561,1136,4136,1006,8588,49166,154,13988",
			"name": "a(n) is the smallest number k such that k = (sigma(n*(sigma(k)-k)) - n*(sigma(k)-k))/n.",
			"comment": [
				"A sort of generalization of amicable numbers where x = n*(sigma(k)-k), y = (sigma(x)-x)/n = k and x \u003e= y.",
				"All the numbers that satisfy the equation for n=1 are listed in A206708.",
				"a(n) = n for n = 4, 8, 14, 32, 128, 2366, 8193, 131072, etc.",
				"In particular a(n) = n if n = 2^p where p is a Mersenne exponent (A000043)."
			],
			"link": [
				"Paolo P. Lava, \u003ca href=\"/A321328/b321328.txt\"\u003eTable of n, a(n) for n = 1..200\u003c/a\u003e"
			],
			"example": [
				"a(7) = 1012 because (sigma(7*(sigma(1012)-1012)) - 7*(sigma(1012)-1012))/7 = (sigma(7*1004) - 7*1004)/7 = (14112-7028)/7 = 7084/7 = 1012 and this is the least number to have this property."
			],
			"maple": [
				"with(numtheory): P:=proc(q) local k,n; for n from 1 to q do",
				"for k from 1 to q do if (sigma(n*(sigma(k)-k))-n*(sigma(k)-k))/n=k",
				"then print(k); break; fi; od; od; end: P(10^6);"
			],
			"mathematica": [
				"s[n_] := DivisorSigma[1,n]-n; a[n_] := Module[{k=2}, While[k != s[n*s[k]]/n, k++];k]; Array[a, 52] (* _Amiram Eldar_, Nov 06 2018 *)"
			],
			"program": [
				"(PARI) f(n,k) = {my(sk = sigma(k)-k); iferr((sigma(n*sk)-n*sk)/n, E, 0);}",
				"a(n) = {my(k=1); while (k != f(n,k), k++); k;} \\\\ _Michel Marcus_, Nov 06 2018"
			],
			"xref": [
				"Cf. A000040, A000043, A000203, A206708."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Paolo P. Lava_, Nov 05 2018",
			"references": 1,
			"revision": 32,
			"time": "2018-12-08T17:09:07-05:00",
			"created": "2018-11-14T13:52:08-05:00"
		}
	]
}