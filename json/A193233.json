{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A193233",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 193233,
			"data": "1,-3,2,0,1,-12,63,-190,363,-455,370,-180,40,0,1,-39,732,-8806,76293,-507084,2689452,-11689056,42424338,-130362394,342624075,-776022242,1522861581,-2598606825,3863562996,-5007519752,5652058863,-5541107684,4697231261",
			"name": "Triangle T(n,k), n\u003e=1, 0\u003c=k\u003c=3^n, read by rows: row n gives the coefficients of the chromatic polynomial of the Hanoi graph H_n, highest powers first.",
			"comment": [
				"The Hanoi graph H_n has 3^n vertices and 3*(3^n-1)/2 edges. It represents the states and allowed moves in the Towers of Hanoi problem with n disks. The chromatic polynomial of H_n has 3^n+1 coefficients."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A193233/b193233.txt\"\u003eRows n = 1..6, flattened\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ChromaticPolynomial.html\"\u003eChromatic Polynomial\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HanoiGraph.html\"\u003eHanoi Graph\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Chromatic_polynomial\"\u003eChromatic Polynomial\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Tower_of_Hanoi\"\u003eTower of Hanoi\u003c/a\u003e"
			],
			"example": [
				"2 example graphs:          o",
				".                         / \\",
				".                        o---o",
				".                       /     \\",
				".             o        o       o",
				".            / \\      / \\     / \\",
				".           o---o    o---o---o---o",
				"Graph:       H_1          H_2",
				"Vertices:     3            9",
				"Edges:        3           12",
				"The Hanoi graph H_1 equals the cycle graph C_3 with chromatic polynomial",
				"   q^3 -3*q^2 +2*q =\u003e [1, -3, 2, 0].",
				"Triangle T(n,k) begins:",
				"  1,    -3,      2,          0;",
				"  1,   -12,     63,       -190,         363,            -455,  ...",
				"  1,   -39,    732,      -8806,       76293,         -507084,  ...",
				"  1,  -120,   7113,    -277654,     8028540,      -183411999,  ...",
				"  1,  -363,  65622,   -7877020,   706303350,    -50461570575,  ...",
				"  1, -1092, 595443, -216167710, 58779577593, -12769539913071,  ...",
				"  ..."
			],
			"xref": [
				"Cf. A000244, A029858.",
				"Cf. A288839 (chromatic polynomials of the n-Hanoi graph).",
				"Cf. A137889 (directed Hamiltonian paths in the n-Hanoi graph).",
				"Cf. A288490 (independent vertex sets in the n-Hanoi graph).",
				"Cf. A286017 (matchings in the n-Hanoi graph).",
				"Cf. A193136 (spanning trees of the n-Hanoi graph).",
				"Cf. A288796 (undirected paths in the n-Hanoi graph)."
			],
			"keyword": "sign,tabf,look,hard",
			"offset": "1,2",
			"author": "_Alois P. Heinz_, Jul 18 2011",
			"references": 13,
			"revision": 21,
			"time": "2021-11-02T22:11:48-04:00",
			"created": "2011-07-19T03:02:43-04:00"
		}
	]
}