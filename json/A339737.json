{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A339737",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 339737,
			"data": "1,1,0,1,1,0,2,0,1,0,2,1,1,1,0,3,1,1,1,1,0,4,1,2,2,1,1,0,5,1,3,2,2,1,1,0,6,2,3,4,3,2,1,1,0,8,2,4,5,4,3,2,1,1,0,10,2,5,7,6,5,3,2,1,1,0,12,3,6,8,9,6,5,3,2,1,1,0,15,3,8,11,11,10,7,5,3,2,1,1,0",
			"name": "Triangle read by rows where T(n,k) is the number of integer partitions of n with greatest gap k.",
			"comment": [
				"We define the greatest gap of a partition to be the greatest nonnegative integer less than the greatest part and not in the partition."
			],
			"link": [
				"George E. Andrews and David Newman, \u003ca href=\"https://doi.org/10.1007/s00026-019-00427-w\"\u003ePartitions and the Minimal Excludant\u003c/a\u003e, Annals of Combinatorics, Volume 23, May 2019, Pages 249-254.",
				"Brian Hopkins, James A. Sellers, and Dennis Stanton, \u003ca href=\"https://arxiv.org/abs/2009.10873\"\u003eDyson's Crank and the Mex of Integer Partitions\u003c/a\u003e, arXiv:2009.10873 [math.CO], 2020.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Mex_(mathematics)\"\u003eMex (mathematics)\u003c/a\u003e"
			],
			"example": [
				"Triangle begins:",
				"   1",
				"   1   0",
				"   1   1   0",
				"   2   0   1   0",
				"   2   1   1   1   0",
				"   3   1   1   1   1   0",
				"   4   1   2   2   1   1   0",
				"   5   1   3   2   2   1   1   0",
				"   6   2   3   4   3   2   1   1   0",
				"   8   2   4   5   4   3   2   1   1   0",
				"  10   2   5   7   6   5   3   2   1   1   0",
				"  12   3   6   8   9   6   5   3   2   1   1   0",
				"  15   3   8  11  11  10   7   5   3   2   1   1   0",
				"  18   4   9  13  15  13  10   7   5   3   2   1   1   0",
				"  22   5  10  17  19  18  14  11   7   5   3   2   1   1   0",
				"  27   5  13  20  24  23  20  14  11   7   5   3   2   1   1   0",
				"For example, row n = 9 counts the following partitions:",
				"  (3321)       (432)   (333)      (54)      (522)    (63)    (72)   (81)  (9)",
				"  (22221)      (3222)  (4311)     (441)     (531)    (621)   (711)",
				"  (32211)              (33111)    (4221)    (5211)   (6111)",
				"  (222111)             (3111111)  (42111)   (51111)",
				"  (321111)                        (411111)",
				"  (2211111)",
				"  (21111111)",
				"  (111111111)"
			],
			"mathematica": [
				"maxgap[q_]:=Max@@Complement[Range[0,If[q=={},0,Max[q]]],q];",
				"Table[Length[Select[IntegerPartitions[n],maxgap[#]==k\u0026]],{n,0,15},{k,0,n}]"
			],
			"xref": [
				"Column k = 0 is A000009.",
				"Row sums are A000041.",
				"Central diagonal is A000041.",
				"Column k = 1 is A087897.",
				"The version for least gap is A264401, with Heinz number encoding A257993.",
				"The version for greatest difference is A286469 or A286470.",
				"An encoding (of greatest gap) using Heinz numbers is A339662.",
				"A000070 counts partitions with a selected part.",
				"A006128 counts partitions with a selected position.",
				"A015723 counts strict partitions with a selected part.",
				"A048004 counts compositions by greatest part.",
				"A056239 adds up prime indices, row sums of A112798.",
				"A064391 is the version for crank.",
				"A064428 counts partitions of nonnegative crank.",
				"A073491 list numbers with gap-free prime indices.",
				"A107428 counts gap-free compositions.",
				"A238709/A238710 counts partitions by least/greatest difference.",
				"A342050/A342051 have prime indices with odd/even least gap.",
				"Cf. A001223, A002110, A018818, A063250, A088860, A098743, A279945."
			],
			"keyword": "nonn,tabl",
			"offset": "1,7",
			"author": "_Gus Wiseman_, Apr 20 2021",
			"references": 5,
			"revision": 11,
			"time": "2021-04-22T01:42:52-04:00",
			"created": "2021-04-22T01:42:52-04:00"
		}
	]
}