{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A086449",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 86449,
			"data": "1,1,2,1,4,2,4,1,8,4,8,2,12,4,8,1,18,8,16,4,26,8,16,2,34,12,24,4,36,8,16,1,48,18,36,8,60,16,32,4,80,26,52,8,78,16,32,2,104,34,68,12,110,24,48,4,136,36,72,8,108,16,32,1,154,48,96,18,160,36,72,8",
			"name": "a(0) = 1, a(2n+1) = a(n), a(2n) = a(n) + a(n-1) + ... + a(n-2^m) + ... where a(n) = 0 for n \u003c 0.",
			"comment": [
				"Conjecture: all a(n) are even except a(2^k-1) = 1. Also a(2^k-2) = 2^(k-1). [For proof see link.]",
				"Setting m=0 gives Stern-Brocot sequence (A002487).",
				"a(n) is the number of ways of writing n as a sum of powers of 2, where each power appears p times, with p itself a power of 2."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A086449/b086449.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Lambert Herrgesell, \u003ca href=\"/A086449/a086449.txt\"\u003eProof of conjecture\u003c/a\u003e",
				"Peter Luschny, \u003ca href=\"http://www.oeis.org/wiki/User:Peter_Luschny/SternsDiatomic\"\u003e Rational Trees and Binary Partitions\u003c/a\u003e."
			],
			"formula": [
				"G.f.: Product_{k\u003e=0} (1 + Sum_{j\u003e=0} x^(2^(k+j)). [Corrected by Herbert S. Wilf, May 31 2006]"
			],
			"example": [
				"From _Peter Luschny_, Sep 01 2019: (Start)",
				"The sequence splits into rows of length 2^k:",
				"1",
				"1,  2",
				"1,  4, 2,  4",
				"1,  8, 4,  8, 2, 12, 4,  8",
				"1, 18, 8, 16, 4, 26, 8, 16, 2, 34, 12, 24, 4, 36, 8, 16",
				".",
				"The first few partitions counted are (compare with the list in A174980):",
				"[ 0]  [[]]",
				"[ 1]  [[1]]",
				"[ 2]  [[2], [1, 1]]",
				"[ 3]  [[2, 1]]",
				"[ 4]  [[4], [2, 2], [2, 1, 1], [1, 1, 1, 1]]",
				"[ 5]  [[4, 1], [2, 2, 1]]",
				"[ 6]  [[4, 2], [4, 1, 1], [2, 2, 1, 1], [2, 1, 1, 1, 1]]",
				"[ 7]  [[4, 2, 1]]",
				"[ 8]  [[8], [4, 4], [4, 2, 2], [4, 2, 1, 1], [4, 1, 1, 1, 1], [2, 2, 2, 2],",
				"      [2, 2, 1, 1, 1, 1], [1, 1, 1, 1, 1, 1, 1, 1]]",
				"[ 9]  [[8, 1], [4, 4, 1], [4, 2, 2, 1], [2, 2, 2, 2, 1]]",
				"[10]  [[8, 2], [8, 1, 1], [4, 4, 2], [4, 4, 1, 1], [4, 2, 2, 1, 1],",
				"      [4, 2, 1, 1, 1, 1], [2, 2, 2, 2, 1, 1], [2, 1, 1, 1, 1, 1, 1, 1, 1]]",
				"[11]  [[8, 2, 1], [4, 4, 2, 1]]",
				"[12]  [[8, 4], [8, 2, 2], [8, 2, 1, 1], [8, 1, 1, 1, 1], [4, 4, 2, 2],",
				"      [4, 4, 2, 1, 1], [4, 4, 1, 1, 1, 1], [4, 2, 2, 2, 2], [4, 2, 2, 1, 1, 1, 1],",
				"      [4, 1, 1, 1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 1, 1, 1, 1],",
				"      [2, 2, 1, 1, 1, 1, 1, 1, 1, 1]]",
				"[13]  [[8, 4, 1], [8, 2, 2, 1], [4, 4, 2, 2, 1], [4, 2, 2, 2, 2, 1]]",
				"[14]  [[8, 4, 2], [8, 4, 1, 1], [8, 2, 2, 1, 1], [8, 2, 1, 1, 1, 1],",
				"      [4, 4, 2, 2, 1, 1], [4, 4, 2, 1, 1, 1, 1], [4, 2, 2, 2, 2, 1, 1],",
				"      [4, 2, 1, 1, 1, 1, 1, 1, 1, 1]]",
				"[15]  [[8, 4, 2, 1]]",
				"(End)"
			],
			"maple": [
				"A086449 := proc(n) option remember;",
				"local IndexSet, k; IndexSet := proc(n) local i, j, z;",
				"i := iquo(n,2); j := i; if odd::n then i := i-1; z := 1;",
				"while 0 \u003c= i do j := j,i; i := i-z; z := z+z od fi; j end:",
				"if n \u003c 2 then 1 else add(A086449(k),k=IndexSet(n)) fi end:",
				"seq(A086449(i),i=0..71); # _Peter Luschny_, May 06 2011",
				"# second Maple program:",
				"a:= proc(n) option remember; local r; `if`(n=0, 1,",
				"      `if`(irem(n, 2, 'r')=1, a(r),",
				"       a(r) +add(a(r-2^m), m=0..ilog2(r))))",
				"    end:",
				"seq(a(n), n=0..80);  # _Alois P. Heinz_, May 30 2014"
			],
			"mathematica": [
				"nn=30;CoefficientList[Series[Product[1+Sum[x^(2^(k+j)),{j,0,nn}],{k,0,nn}],{x,0,nn}],x] (* _Geoffrey Critzer_, May 30 2014 *)"
			],
			"program": [
				"a(n)=local(k): if(n\u003c1,n\u003e=0,if(n%2==0,a(n/2)+sum(k=0,n,a((n-2^(k+1))/2)),a((n-1)/2)))",
				"(MAGMA) m:=80; R\u003cx\u003e:=PowerSeriesRing(Integers(), m); Coefficients(R!( (\u0026*[1 + (\u0026+[x^(2^(k+j)): j in [0..m/4]]): k in [0..m/4]]) )); // _G. C. Greubel_, Feb 11 2019"
			],
			"xref": [
				"Cf. A002487, A086450, A174980."
			],
			"keyword": "nonn,easy,look,tabf",
			"offset": "0,3",
			"author": "_Ralf Stephan_, Jul 20 2003",
			"references": 5,
			"revision": 43,
			"time": "2019-09-01T17:04:10-04:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}