{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A217107",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 217107,
			"data": "2,1,7,8,51,49,57,353,345,343,400,2417,2411,2403,2401,9604,16880,16823,16829,16809,16807,67228,117763,117721,117666,117659,117651,117649,470596,823709,823664,823615,823560,823553,823545,823543,3294172,5765310,5765063",
			"name": "Minimal number (in decimal representation) with n nonprime substrings in base-7 representation (substrings with leading zeros are considered to be nonprime).",
			"comment": [
				"The sequence is well-defined in that for each n the set of numbers with n nonprime substrings is not empty. Proof: Define m(n):=2*sum_{j=i..k} 7^j, where k:=floor((sqrt(8*n+1)-1)/2), i:= n-A000217(k). For n=0,1,2,3,... the m(n) in base-7 representation are 2, 22, 20, 222, 220, 200, 2222, 2220, 2200, 2000, 22222, 22220, .... m(n) has k+1 digits and (k-i+1) 2’s, thus, the number of nonprime substrings of m(n) is ((k+1)*(k+2)/2)-k-1+i = (k*(k+1)/2)+i = n, which proves the statement.",
				"If p is a number with k prime substrings and d digits (in base-7 representation), p != 1 (mod 7), m\u003e=d, than b := p*7^(m-d) has m*(m+1)/2 - k nonprime substrings, and a(A000217(n)-k) \u003c= b."
			],
			"link": [
				"Hieronymus Fischer, \u003ca href=\"/A217107/b217107.txt\"\u003eTable of n, a(n) for n = 0..210\u003c/a\u003e"
			],
			"formula": [
				"a(n) \u003e= 7^floor((sqrt(8*n-7)-1)/2) for n\u003e0, equality holds if n=1 or n+1 is a triangular number (cf. A000217).",
				"a(A000217(n)-1) = 7^(n-1), n\u003e1.",
				"a(A000217(n)) = floor(400 * 7^(n-4)), n\u003e0.",
				"a(A000217(n)) = 111…111_7 (with n digits), n\u003e0.",
				"a(A000217(n)-k) \u003e= 7^(n-1) + k-1, 1\u003c=k\u003c=n, n\u003e1.",
				"a(A000217(n)-k) = 7^(n-1) + p, where p is the minimal number \u003e= 0 such that 7^(n-1) + p, has k prime substrings in base-7 representation, 1\u003c=k\u003c=n, n\u003e1."
			],
			"example": [
				"a(0) = 2, since 2 = 2_7 is the least number with zero nonprime substrings in base-7 representation.",
				"a(1) = 1, since 1 = 1_7 is the least number with 1 nonprime substring in base-7 representation.",
				"a(2) = 7, since 7 = 10_7 is the least number with 2 nonprime substrings in base-7 representation (these are 0 and 1).",
				"a(3) = 8, since 8 = 11_7 is the least number with 3 nonprime substrings in base-7 representation (1, 1 and 11).",
				"a(4) = 51, since 51 = 102_7 is the least number with 4 nonprime substrings in base-7 representation, these are 0, 1, 02, and 102 (remember, that substrings with leading zeros are considered to be nonprime)."
			],
			"xref": [
				"Cf. A019546, A035232, A039996, A046034, A069489, A085823, A211681, A211682, A211684, A211685.",
				"Cf. A035244, A079397, A213300-A213321.",
				"Cf. A217102-A217109.",
				"Cf. A217302-A217309."
			],
			"keyword": "nonn,base",
			"offset": "0,1",
			"author": "_Hieronymus Fischer_, Dec 12 2012",
			"references": 2,
			"revision": 16,
			"time": "2015-07-16T22:23:40-04:00",
			"created": "2012-12-19T13:19:44-05:00"
		}
	]
}