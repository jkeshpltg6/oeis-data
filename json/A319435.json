{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A319435",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 319435,
			"data": "1,1,0,1,1,1,4,4,9,16,24,52,83,152,305,515,959,1773,3105,5724,10255,18056,32584,58082,101719,179306,317610,552730,962134,1683435,2899064,4995588,8638919,14746755,25196684,43082429,72959433,123554195,209017908,351164162",
			"name": "Number of partitions of n^2 into exactly n nonzero squares.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A319435/b319435.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A243148(n^2,n)."
			],
			"example": [
				"a(0) = 1: the empty partition.",
				"a(1) = 1: 1.",
				"a(2) = 0: there is no partition of 4 into exactly 2 nonzero squares.",
				"a(3) = 1: 441.",
				"a(4) = 1: 4444.",
				"a(5) = 1: 94444.",
				"a(6) = 4: (25)44111, (16)(16)1111, (16)44444, 999441.",
				"a(7) = 4: (25)(16)41111, (25)444444, (16)(16)44441, (16)999411.",
				"a(8) = 9: (49)9111111, (36)(16)441111, (36)4444444, (25)(25)911111, (25)(16)944411, (25)9999111, (16)(16)(16)94111, (16)9999444, 99999991."
			],
			"maple": [
				"h:= proc(n) option remember; `if`(n\u003c1, 0,",
				"      `if`(issqr(n), n, h(n-1)))",
				"    end:",
				"b:= proc(n, i, t) option remember; `if`(n=0, 1, `if`(i\u003c1 or",
				"      t\u003c1, 0, b(n, h(i-1), t)+b(n-i, h(min(n-i, i)), t-1)))",
				"    end:",
				"a:= n-\u003e (s-\u003e b(s$2, n)-`if`(n=0, 0, b(s$2, n-1)))(n^2):",
				"seq(a(n), n=0..40);"
			],
			"mathematica": [
				"h[n_] := h[n] = If[n \u003c 1, 0, If[Sqrt[n] // IntegerQ, n, h[n - 1]]];",
				"b[n_, i_, t_] := b[n, i, t] = If[n == 0, 1, If[i \u003c 1 || t \u003c 1, 0, b[n, h[i - 1], t] + b[n - i, h[Min[n - i, i]], t - 1]]];",
				"a[n_] := Function[s, b[s, s, n] - If[n == 0, 0, b[s, s, n - 1]]][n^2];",
				"a /@ Range[0, 40] (* _Jean-François Alcover_, Nov 06 2020, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(SageMath) # uses[GeneralizedEulerTransform(n, a) from A338585], slow.",
				"def A319435List(n): return GeneralizedEulerTransform(n, lambda n: n^2)",
				"print(A319435List(10)) # _Peter Luschny_, Nov 12 2020"
			],
			"xref": [
				"Cf. A243148, A259254, A319503."
			],
			"keyword": "nonn",
			"offset": "0,7",
			"author": "_Alois P. Heinz_, Sep 18 2018",
			"references": 6,
			"revision": 19,
			"time": "2020-11-12T23:21:39-05:00",
			"created": "2018-09-19T10:06:02-04:00"
		}
	]
}