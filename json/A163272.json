{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A163272",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 163272,
			"data": "0,1,48,1280,2496,28672,29808,454656,2342912,11534336,57409536,218103808,34753216512,73014444032,583041810432,1305670057984,2624225017856,404620279021568,467515780104192,1014849232437248,4446425022726144,5806013294837760,46545625738641408",
			"name": "Numbers k such that k = A074206(k), the number of ordered factorizations of k.",
			"comment": [
				"From _Mauro Fiorentini_, Jul 15 2018: (Start)",
				"If p is an odd prime, 2^(2*p - 2)*p belongs to the sequence, so the sequence is infinite.",
				"If n^2 + 6*n + 6 = 2*p*q is twice the product of two distinct odd primes, 2^n*p*q belongs to the sequence.",
				"No number of the form 2^n*p^2, with p odd prime, belongs to the sequence. (End)",
				"For every possible prime signature (see A025487) there can be at most one number having it in this sequence. - _David A. Corneth_, Jul 15 2018",
				"2*10^14 \u003c a(18) \u003c= 404620279021568. Also terms: 467515780104192, 1014849232437248, 4446425022726144, 5806013294837760, and 46545625738641408. - _Giovanni Resta_, Jul 16 2018",
				"These numbers are named \"super-perfect numbers\" (Miller), \"gamma-perfect numbers\" (Sandor \u0026 Crstici), \"factor-perfect numbers\" (Knopfmacher \u0026 Mays) and \"balanced numbers\" (Brown). - _Amiram Eldar_, Aug 22 2018",
				"From _David A. Corneth_, Aug 23 2018: (Start)",
				"Suppose one searches terms below u. We have A074206(m * t) \u003e A074206(m) for m, t \u003e 1 so if A074206(m) \u003e u we needn't check any value A074206(m * t) where m * t \u003c u.",
				"All terms \u003c 10^25 except 29809 are of the form 4^e * s where s is a squarefree odd number. (End)"
			],
			"reference": [
				"J. Sandor and B. Crstici, Handbook of Number Theory II, Springer, 2004, pp. 54-55."
			],
			"link": [
				"David A. Corneth, \u003ca href=\"/A163272/b163272.txt\"\u003eTable of n, a(n) for n = 1..49\u003c/a\u003e (terms \u003c 10^30)",
				"Peter Brown, \u003ca href=\"http://www.mountainman.com.au/harmonics_01.htm\"\u003eNumber of Ordered Factorizations\u003c/a\u003e, 2004.",
				"Martin Klazar and Florian Luca, \u003ca href=\"https://doi.org/10.1016/j.jnt.2006.10.003\"\u003eOn the maximal order of numbers in the \"factorisatio numerorum\" problem\u003c/a\u003e, Journal of Number Theory, Vol. 124, No. 2 (2007), pp. 470-490.",
				"Arnold Knopfmacher and M. E. Mays, \u003ca href=\"https://pdfs.semanticscholar.org/d7ed/31ad7c11cad37442838d6614f658af539ef5.pdf\"\u003eA survey of factorization counting functions\u003c/a\u003e, International Journal of Number Theory, Vol. 1, No. 4 (2005), pp. 563-581, DOI: 10.1142/S1793042105000315.",
				"Michael D. Miller, \u003ca href=\"https://fq.math.ca/Scanned/13-3/miller.pdf\"\u003eA recursively defined divisor function\u003c/a\u003e, The Fibonacci Quarterly, Vol. 13 (1975), pp. 199-204.",
				"Project Euler, \u003ca href=\"https://projecteuler.net/problem=548\"\u003eProblem 548: Gozinta Chains\u003c/a\u003e."
			],
			"maple": [
				"A074206 := proc(n) option remember; if n \u003c= 1 then n; else add(procname(d), d=numtheory[divisors](n) minus {n}) ; end if; end proc: for n from 1 do if n = A074206(n) then printf(\"%d,\\n\",n) ; end if; end do: \\\\ _R. J. Mathar_, Aug 01 2009"
			],
			"program": [
				"(PARI) term(n) = {my(f = A074206(n)); if(factor(n)[, 2] == factor(f)[, 2], f, 0) \\\\ returns 0 if there is no term in the sequence with prime signature of n, or if there is, returns that term. - _David A. Corneth_, Jul 15 2018"
			],
			"xref": [
				"Cf. A025487, A074206."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Mats Granvik_, Jul 24 2009",
			"ext": [
				"a(6)-a(7) from _R. J. Mathar_, Aug 01 2009",
				"a(8)-a(9) from _Nathaniel Johnston_, Dec 04 2010",
				"a(10)-a(12) from _Mauro Fiorentini_, Dec 07 2015",
				"a(13)-a(17) from _Giovanni Resta_, Jul 16 2018, following a suggestion from _David A. Corneth_",
				"a(18)-a(23) from _Amiram Eldar_, Aug 22 2018, following the same suggestion with an extended list of terms of A025487."
			],
			"references": 10,
			"revision": 72,
			"time": "2021-08-02T06:33:47-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}