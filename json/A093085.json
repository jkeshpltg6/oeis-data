{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A093085",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 93085,
			"data": "1,-2,0,0,1,2,0,0,-1,-4,0,0,0,6,0,0,1,-8,0,0,0,12,0,0,-1,-18,0,0,-1,24,0,0,2,-32,0,0,1,44,0,0,-2,-58,0,0,-1,76,0,0,2,-100,0,0,1,128,0,0,-3,-164,0,0,-1,210,0,0,4,-264,0,0,2,332,0,0,-5,-416,0,0,-2,516,0,0,5,-640,0,0,2,790,0,0,-6,-968",
			"name": "Expansion of phi(-x) / psi(x^4) in powers of x where phi(), psi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"eta(q^2) * eta(q^8)^6 = eta(q)^2 * eta(q^4)^2 * eta(q^8) * eta(q^16)^2 + 2 * eta(q^2) * eta(q^4)^2 * eta(q^16)^4 is equivalent to the a(4*n), ..., a(4*n + 3) results."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A093085/b093085.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(1/2) * eta(q)^2 * eta(q^4) / (eta(q^2) * eta(q^8)^2) in powers of q.",
				"Euler transform of period 8 sequence [ -2, -1, -2, -2, -2, -1, -2, 0, ...].",
				"Given g.f. A(x), then B(q) = A(q)^2 / q satisfies 0 = f(B(q), B(q^2)) where f(u, v) = u * (u + 8) * (v + 4) - v^2.",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (32 t)) = 32^(1/2) g(t) where q = exp(2 Pi i t) and g() is the g.f. for A187154.",
				"G.f.: Product_{k\u003e0} (1 - x^k)^2 / ((1 - x^(4*k - 2)) * (1 - x^(8*k))^2).",
				"a(4*n + 2) = a(4*n + 3) = 0. a(4*n) = A029838(n). a(4*n + 1) =  -2 * A083365(n). Convolution square is A131124. Convolution inverse is A187154."
			],
			"example": [
				"G.f. = 1 - 2*x + x^4 + 2*x^5 - x^8 - 4*x^9 + 6*x^13 + x^16 - 8*x^17 + 12*x^21 - ...",
				"G.f. = 1/q - 2*q + q^7 + 2*q^9 - q^15 - 4*q^17 + 6*q^25 + q^31 - 8*q^33 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ 2 x^(1/2) EllipticTheta[ 4, 0, x] / EllipticTheta[ 2, 0, x^2], {x, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, polcoeff( prod( k=1, n, (1 - x^k)^[ 0, 2, 1, 2, 2, 2, 1, 2][1 + k%8], 1 + x * O(x^n)), n))}",
				"(PARI) {a(n) = my(A, A2, m); if( n\u003c0, 0, A = x + O(x^2); m=1; while( m\u003c=n, m*=2; A = subst(A, x, x^2); A = 4*A + 16*A^2 + (1 + 8*A) * sqrt(A + 4*A^2)); polcoeff( sqrt(x / A), n))}",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A)^2 * eta(x^4 + A) / (eta(x^2 + A) * eta(x^8 + A)^2), n))}"
			],
			"xref": [
				"Cf. A029838, A029839, A083365, A131124, A187154."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Mar 20 2004, Oct 22 2007",
			"references": 2,
			"revision": 20,
			"time": "2021-03-12T22:24:42-05:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}