{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A027926",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 27926,
			"data": "1,1,1,1,1,1,2,2,1,1,1,2,3,4,3,1,1,1,2,3,5,7,7,4,1,1,1,2,3,5,8,12,14,11,5,1,1,1,2,3,5,8,13,20,26,25,16,6,1,1,1,2,3,5,8,13,21,33,46,51,41,22,7,1,1,1,2,3,5,8,13,21,34,54,79,97,92,63,29,8,1",
			"name": "Triangular array T read by rows: T(n,0) = T(n,2n) = 1 for n \u003e= 0; T(n,1) = 1 for n \u003e= 1; T(n,k) = T(n-1,k-2) + T(n-1,k-1) for k = 2..2n-1, n \u003e= 2.",
			"comment": [
				"T(n,k) = number of strings s(0),...,s(n) such that s(0)=0, s(n)=n-k and for 1\u003c=i\u003c=n, s(i)=s(i-1)+d, with d in {0,1,2} if i=0, in {0,2} if s(i)=2i, in {0,1,2} if s(i)=2i-1, in {0,1} if 0\u003c=s(i)\u003c=2i-2.",
				"Can be seen as concatenation of triangles A104763 and A105809, with identifying column of Fibonacci numbers, see example. - _Reinhard Zumkeller_, Aug 15 2013"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A027926/b027926.txt\"\u003eRows n = 0..100 of table, flattened\u003c/a\u003e",
				"\u003ca href=\"/index/Pas#Pascal\"\u003eIndex entries for triangles and arrays related to Pascal's triangle\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = Sum_{j=0..floor((2*n-k+1)/2)} binomial(n-j, 2*n-k-2*j). - _Len Smiley_, Oct 21 2001"
			],
			"example": [
				".   0:                           1",
				".   1:                        1  1   1",
				".   2:                     1  1  2   2   1",
				".   3:                  1  1  2  3   4   3   1",
				".   4:               1  1  2  3  5   7   7   4   1",
				".   5:            1  1  2  3  5  8  12  14  11   5   1",
				".   6:          1 1  2  3  5  8 13  20  26  25  16   6   1",
				".   7:        1 1 2  3  5  8 13 21  33  46  51  41  22   7   1",
				".   8:      1 1 2 3  5  8 13 21 34  54  79  97  92  63  29   8  1",
				".   9:    1 1 2 3 5  8 13 21 34 55  88 133 176 189 155  92  37  9  1",
				".  10:  1 1 2 3 5 8 13 21 34 55 89 143 221 309 365 344 247 129 46 10  1",
				".",
				".   1:                           1",
				".   2:                        1  1",
				".   3:                     1  1  2",
				".   4:                  1  1  2  3",
				".   5:               1  1  2  3  5      columns = A000045, \u003e 0",
				".   6:            1  1  2  3  5  8     +---------+",
				".   7:          1 1  2  3  5  8 13     | A104763 |",
				".   8:        1 1 2  3  5  8 13 21     +---------+",
				".   9:      1 1 2 3  5  8 13 21 34",
				".  10:    1 1 2 3 5  8 13 21 34 55",
				".  11:  1 1 2 3 5 8 13 21 34 55 89",
				".",
				".   0:                           1",
				".   1:                           1   1                +---------+",
				".   2:                           2   2   1            | A105809 |",
				".   3:                           3   4   3   1        +---------+",
				".   4:                           5   7   7   4   1",
				".   5:                           8  12  14  11   5   1",
				".   6:                          13  20  26  25  16   6   1",
				".   7:                          21  33  46  51  41  22   7   1",
				".   8:                          34  54  79  97  92  63  29   8  1",
				".   9:                          55  88 133 176 189 155  92  37  9  1",
				".  10:                          89 143 221 309 365 344 247 129 46 10  1"
			],
			"maple": [
				"A027926 := proc(n,k)",
				"    add(binomial(n-j,2*n-k-2*j),j=0..(2*n-k+1)/2) ;",
				"end proc: # _R. J. Mathar_, Apr 11 2016"
			],
			"mathematica": [
				"z = 15; t[n_, 0] := 1; t[n_, k_] := 1 /; k == 2 n; t[n_, 1] := 1;",
				"t[n_, k_] := t[n, k] = t[n - 1, k - 2] + t[n - 1, k - 1];",
				"u = Table[t[n, k], {n, 0, z}, {k, 0, 2 n}];",
				"TableForm[u] (* A027926 array *)",
				"v = Flatten[u] (* A027926 sequence *)",
				"(* _Clark Kimberling_, Aug 31 2014 *)",
				"Table[Sum[Binomial[n-j, 2*n-k-2*j], {j, 0, Floor[(2*n-k+1)/2]}], {n, 0, 10}, {k, 0, 2*n}]//Flatten (* _G. C. Greubel_, Sep 05 2019 *)"
			],
			"program": [
				"(PARI) {T(n, k) = if( k\u003c0 || k\u003e2*n, 0, if( k\u003c=1 || k==2*n, 1, T(n-1, k-2) + T(n-1, k-1)))}; /* __Michael Somos_, Feb 26 1999 */",
				"(PARI) {T(n, k) = if( k\u003c0 || k\u003e2*n, 0, sum( j=max(0, k-n), k\\2, binomial(k-j, j)))}; /* _Michael Somos_ */",
				"(Haskell)",
				"a027926 n k = a027926_tabf !! n !! k",
				"a027926_row n = a027926_tabf !! n",
				"a027926_tabf = iterate (\\xs -\u003e zipWith (+)",
				"                               ([0] ++ xs ++ [0]) ([1,0] ++ xs)) [1]",
				"-- Variant, cf. example:",
				"a027926_tabf' = zipWith (++) a104763_tabl (map tail a105809_tabl)",
				"-- _Reinhard Zumkeller_, Aug 15 2013",
				"(MAGMA) [\u0026+[Binomial(n-j, 2*n-k-2*j): j in [0..Floor((2*n-k+1)/2)]]: k in [0..2*n], n in [0..10]]; // _G. C. Greubel_, Sep 05 2019",
				"(Sage) [[sum(binomial(n-j, 2*n-k-2*j) for j in (0..floor((2*n-k+1)/2))) for k in (0..2*n)] for n in (0..10)] # _G. C. Greubel_, Sep 05 2019",
				"(GAP) Flat(List([0..10], n-\u003e List([0..2*n], k-\u003e Sum([0..Int((2*n-k+1)/2) ], j-\u003e Binomial(n-j, 2*n-k-2*j) )))); # _G. C. Greubel_, Sep 05 2019"
			],
			"xref": [
				"Many columns of T are A000045 (Fibonacci sequence), also in T: A001924, A004006, A000071, A000124, A014162, A014166, A027927-A027933.",
				"Some other Fibonacci-Pascal triangles: A036355, A037027, A074829, A105809, A109906, A111006, A114197, A162741, A228074."
			],
			"keyword": "nonn,tabf",
			"offset": "0,7",
			"author": "_Clark Kimberling_",
			"ext": [
				"Incorporates comments from _Michael Somos_.",
				"Example extended by _Reinhard Zumkeller_, Aug 15 2013"
			],
			"references": 44,
			"revision": 34,
			"time": "2019-09-06T01:59:32-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}