{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A051288",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 51288,
			"data": "1,2,4,2,8,12,16,48,6,32,160,60,64,480,360,20,128,1344,1680,280,256,3584,6720,2240,70,512,9216,24192,13440,1260,1024,23040,80640,67200,12600,252,2048,56320,253440,295680,92400,5544,4096,135168,760320",
			"name": "Triangle read by rows: T(n,k) = number of paths of n upsteps U and n downsteps D that contain k UUDs.",
			"comment": [
				"By reading paths backward, the UUD in the name could be replaced by DDU.",
				"Or, triangular array T read by rows: T(n,k)=P(2n,n,4k), where P(n,k,c)=number of vectors (x(1),x(2,),...,x(n)) of k 1's and n-k 0's such that x(i)=x(n+1-i) for exactly c values of i. P(n,k,n) counts palindromes.",
				"In nuclear magnetic resonance of n coupled spin-1/2 nuclides, T(n,k) is the number of zero-quantum transitions with combination index k. See the [Sykora (2007)] link, containing also yet another interpretation in terms of pairs of binary n-tuples. - _Stanislav Sykora_, Apr 27 2012",
				"Let u - (u_1, u_2, u_3, ..., u_{2n}) be a binary vector containing n 0's and n 1's. Define a mismatch to be an adjacent pair (u_{2i-1}, u_{2i}) which is neither 0,1 nor 1,0 (think \"socks\"). Then T(n,k) = number of u's with k mismatches. - _N. J. A. Sloane_, Nov 03 2017 following an email from _Bill Gosper_",
				"From _Colin Defant_, Sep 16 2018: (Start)",
				"Let s denote West's stack-sorting map. T(n,k) is the number of permutations pi of [n] with k valleys such that s(pi) avoids the patterns 132, 231, and 321. T(n,k) is also the number of permutations pi of [n] with k valleys such that s(pi) avoids the patterns 132, 312, and 321.",
				"T(n,k) is the number of permutations of [n] with k valleys that avoid the patterns 1342, 3142, 3412, and 3421. (End)"
			],
			"link": [
				"Stanislav Sykora, \u003ca href=\"/A051288/b051288.txt\"\u003eTable of n, a(n) for n = 0..2600\u003c/a\u003e",
				"Colin Defant, \u003ca href=\"https://arxiv.org/abs/1809.03123\"\u003eStack-sorting preimages of permutation classes\u003c/a\u003e, arXiv:1809.03123 [math.CO], 2018.",
				"Rui Duarte and António Guedes de Oliveira, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Duarte/duarte7.html\"\u003eA Famous Identity of Hajós in Terms of Sets\u003c/a\u003e, Journal of Integer Sequences, Vol. 17 (2014), Article 14.9.1.",
				"Aristidis Sapounakis, Panagiotis Tsikouras, Ioannis Tasoulas, Kostas Manes, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v19i2p2\"\u003eStrings of Length 3 in Grand-Dyck Paths and the Chung-Feller Property\u003c/a\u003e, Electr. J. Combinatorics, 19 (2012), #P2.",
				"Stanislav Sykora, \u003ca href=\"/A051288/a051288.txt\"\u003eTriangle T(n,k) for rows n = 0..100\u003c/a\u003e",
				"Stanislav Sykora, \u003ca href=\"http://www.ebyte.it/library/educards/math/CombiTransitions.html\"\u003ep-Quantum Transitions and a Combinatorial Identity\u003c/a\u003e, Stan's Library II, 2007, Identity (1) for p=0.",
				"Stanislav Sýkora, \u003ca href=\"http://www.ebyte.it/stan/blog12to14.html#14Dec31\"\u003eMagnetic Resonance on OEIS\u003c/a\u003e, Stan's NMR Blog (Dec 31, 2014), Retrieved Nov 12, 2019."
			],
			"formula": [
				"T(n, k) = binomial(n, 2*k)*2^(n-2*k)*binomial(2*k, k).",
				"G.f.: (1-4*x+4*x^2*(1-y))^(-1/2) = Sum_{n\u003e=0, k\u003e=0} a(n, k)*x^n*y^k."
			],
			"example": [
				"Table begins",
				"n | k=0    1    2    3",
				"--+-------------------",
				"0 |   1",
				"1 |   2",
				"2 |   4    2",
				"3 |   8   12",
				"4 |  16   48    6",
				"5 |  32  160   60",
				"6 |  64  480  360   20",
				"7 | 128 1344 1680  280",
				"...",
				"a(2,1)=2 because UUDD, DUUD each have one UUD."
			],
			"mathematica": [
				"Table[Binomial[n, 2k]2^(n-2k)Binomial[2k, k], {n, 0, 15}, {k, 0, n/2}]"
			],
			"xref": [
				"Row sums are the (even) central binomial coefficients A000984. A091894 gives the distribution of the parameter \"number of DDUs\" on Dyck paths."
			],
			"keyword": "nonn,tabf",
			"offset": "0,2",
			"author": "_Clark Kimberling_",
			"ext": [
				"Additional comments from _David Callan_, Aug 28 2004"
			],
			"references": 11,
			"revision": 52,
			"time": "2019-11-18T22:05:10-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}