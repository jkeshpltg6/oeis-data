{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A340591",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 340591,
			"data": "1,1,1,1,1,1,1,2,2,1,1,6,16,5,1,1,24,288,192,14,1,1,120,9216,24444,2816,42,1,1,720,460800,7303104,2738592,46592,132,1,1,5040,33177600,4234233600,8204167296,361998432,835584,429,1,1,40320,3251404800,4223111040000,59027412643200,11332298092032,53414223552,15876096,1430,1",
			"name": "Number A(n,k) of n*(k+1)-step k-dimensional nonnegative closed lattice walks starting at the origin and using steps that increment all components or decrement one component by 1; square array A(n,k), n\u003e=0, k\u003e=0, read by antidiagonals.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A340591/b340591.txt\"\u003eAntidiagonals n = 0..24, flattened\u003c/a\u003e"
			],
			"example": [
				"Square array A(n,k) begins:",
				"  1,  1,     1,         1,              1,                   1, ...",
				"  1,  1,     2,         6,             24,                 120, ...",
				"  1,  2,    16,       288,           9216,              460800, ...",
				"  1,  5,   192,     24444,        7303104,          4234233600, ...",
				"  1, 14,  2816,   2738592,     8204167296,      59027412643200, ...",
				"  1, 42, 46592, 361998432, 11332298092032, 1052109889288796160, ..."
			],
			"maple": [
				"b:= proc(n, l) option remember; `if`(n=0, 1, (k-\u003e add(",
				"     `if`(l[i]\u003e0, b(n-1, sort(subsop(i=l[i]-1, l))), 0), i=1..k)+",
				"     `if`(add(i, i=l)+k\u003cn, b(n-1, map(x-\u003e x+1, l)), 0))(nops(l)))",
				"    end:",
				"A:= (n, k)-\u003e b(k*n+n, [0$k]):",
				"seq(seq(A(n, d-n), n=0..d), d=0..10);"
			],
			"mathematica": [
				"b[n_, l_] := b[n, l] = If[n == 0, 1, Function[k, Sum[",
				"  If[l[[i]]\u003e0, b[n-1, Sort[ReplacePart[l, i -\u003e l[[i]]-1]]], 0], {i, 1, k}]+",
				"  If[Sum[i, {i, l}] + k \u003c n, b[n - 1, Map[#+1\u0026, l]], 0]][Length[l]]];",
				"A[n_, k_] := b[k*n + n, Table[0, {k}]];",
				"Table[Table[A[n, d - n], {n, 0, d}], {d, 0, 10}] // Flatten (* _Jean-François Alcover_, Jan 26 2021, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Columns k=0-3 give: A000012, A000108, A006335, A340540.",
				"Rows n=0-2 give: A000012, A000142, |A055546|.",
				"Main diagonal gives A340590.",
				"Cf. A335570."
			],
			"keyword": "nonn,tabl,walk",
			"offset": "0,8",
			"author": "_Alois P. Heinz_, Jan 12 2021",
			"references": 6,
			"revision": 18,
			"time": "2021-01-26T21:01:00-05:00",
			"created": "2021-01-13T17:31:16-05:00"
		}
	]
}