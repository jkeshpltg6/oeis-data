{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A052121",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 52121,
			"data": "1,1,2,1,6,6,3,1,24,36,30,20,10,4,1,120,240,270,240,180,120,70,35,15,5,1,720,1800,2520,2730,2520,2100,1610,1140,750,455,252,126,56,21,6,1,5040,15120,25200,31920,34230,32970,29400,24640,19600,14840,10696,7336",
			"name": "Triangle of coefficients of polynomials enumerating trees with n labeled nodes by inversions.",
			"comment": [
				"Specialization of Tutte polynomials for complete graphs. See the Gessel and Sagan paper. - _Tom Copeland_, Jan 17 2017"
			],
			"reference": [
				"I. P. Goulden and D. M. Jackson, Combinatorial Enumeration, Wiley, N.Y., 1983.",
				"J. W. Moon, Counting labelled trees, Canad. Math. Monographs No 1 (1970) Section 4.5.",
				"R. P. Stanley, Enumerative Combinatorics, Cambridge, Vol. 2, 1999; see Problem 5.48."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A052121/b052121.txt\"\u003eRows n = 1..50, flattened\u003c/a\u003e",
				"I. Gessel and B. Sagan, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v3i2r9\"\u003eThe Tutte polynomial of a graph, depth-first search, and simplicial complex partitions\u003c/a\u003e, The Elect. Jrn. of Comb., Vol. 3, Issue 2, 1996.",
				"I. M. Gessel, B. E. Sagan, Y.-N. Yeh, \u003ca href=\"http://doi.org/10.1002/jgt.3190190402\"\u003eEnumeration of trees by inversions\u003c/a\u003e, J. Graph Theory 19 (4) (1995) 435-459",
				"C. L. Mallows, J. Riordan, \u003ca href=\"https://projecteuclid.org/euclid.bams/1183529387\"\u003eThe inversion enumerator for labeled trees\u003c/a\u003e, Bull. Am. Math. Soc. 74 (1) (1968) 92-94, eq. (5)"
			],
			"formula": [
				"Sum_{k=0..binomial(n-1,2)} T(n,k) = A000272(n).",
				"Sum_{k=0..binomial(n-1,2)} (-1)^k*T(n,k) = A000111(n-1).",
				"E.g.f.: (y-1)*log(Sum_{n\u003e=0} (y-1)^(-n)*y^binomial(n, 2)*x^n/n!).",
				"Sum_{k=0..binomial(n-1,2)} k*T(n,k) = A057500(n). - _Alois P. Heinz_, Nov 29 2015",
				"Equals the coefficient [x^t] of the polynomial J_n(x) which satisfies sum_{\u003e=0} J_{n+1}(x)*y^n/n! = exp[ sum_{n\u003e=1} J_n(x) (x^n-1)/(x-1)*y^n/n!]. - _R. J. Mathar_, Jul 02 2018"
			],
			"example": [
				"1 :   1;",
				"2 :   1;",
				"3 :   2,    1;",
				"4 :   6,    6,    3,    1;",
				"5 :  24,   36,   30,   20,   10,    4,    1;",
				"6 : 120,  240,  270,  240,  180,  120,   70,   35,  15,   5,   1;",
				"7 : 720, 1800, 2520, 2730, 2520, 2100, 1610, 1140, 750, 455, 252, 126, 56, 21, 6, 1;",
				"..."
			],
			"maple": [
				"for n from 2 to 10 do",
				"    add( J[i]*(x^i-1)/(x-1)*y^i/i! ,i=1..n-1) ;",
				"    exp(%) ;",
				"    coeftayl(%,y=0,n-1)*(n-1)! ;",
				"    expand(%) ;",
				"    J[n] := factor(convert(%,polynom)) ;",
				"    for t from 0 to (n-1)*(n-2)/2 do",
				"        printf(\"%d,\",coeff(J[n],x,t)) ;",
				"    end do:",
				"    printf(\"\\n\") ;",
				"end do: # _R. J. Mathar_, Jul 02 2018"
			],
			"mathematica": [
				"rows = 8; egf = (y - 1)*Log[Sum[(y^Binomial[n, 2]*(x^n/n!))/(y - 1)^n, {n, 0, rows + 1}]]; t = CoefficientList[ Series[egf, {x, 0, rows}, {y, 0, 3*rows}], {x, y}] ; Table[(n - 1)!*t[[n, k]], {n, 2, rows + 1}, {k, 1, Binomial[n - 2, 2] + 1}] // Flatten (* _Jean-François Alcover_, Dec 10 2012, after _Vladeta Jovovic_ *)"
			],
			"xref": [
				"Cf. A000272, A000111, A057500."
			],
			"keyword": "nonn,easy,nice,tabf",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_, Jan 23 2000",
			"ext": [
				"Formulae and more terms from _Vladeta Jovovic_, Apr 06 2001"
			],
			"references": 2,
			"revision": 27,
			"time": "2018-08-04T11:22:18-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}