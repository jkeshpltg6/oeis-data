{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A004012",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 4012,
			"id": "M4817",
			"data": "1,0,0,12,0,0,6,0,2,18,0,12,6,0,0,12,0,12,6,6,12,24,6,0,0,12,0,12,0,24,12,12,2,12,6,24,6,12,0,24,0,12,0,6,24,12,12,24,6,12,0,24,0,24,18,12,12,24,0,12,0,12,0,36,0,24,12,18,12,24,12,48,2,0,0,36,0,0,24,12,12",
			"name": "Theta series of hexagonal close-packing.",
			"comment": [
				"The theta series of even layers is a(q^3) * theta_3(q^8) and of odd layers is c(q^3) * theta_2(q^8). - _Michael Somos_, Aug 15 2006",
				"The Cartesian coordinates of the points in the packing are given by HCP(i, j, k) =",
				"(i + (j + m)/2, (3*j + m)/sqrt(12), sqrt(2/3)*k) where, m=mod(k, 2) and i, j, k are integers. - _Michael Somos_, Feb 04 2019",
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Cubic AGM theta functions: a(q) (see A004016), b(q) (A005928), c(q) (A005882)."
			],
			"reference": [
				"J. H. Conway and N. J. A. Sloane, \"Sphere Packings, Lattices and Groups\", Springer-Verlag, p. 114.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A004012/b004012.txt\"\u003eTable of n, a(n) for n = 0..5000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"L. V. Woodcock, \u003ca href=\"http://dx.doi.org/10.1038/385141a0\"\u003eEntropy difference between the face-centred cubic and hexagonal close-packed crystal structures\u003c/a\u003e, Nature, Jan 09 1997, pp. 141-143, esp. p. 143."
			],
			"formula": [
				"{t3(8z/3) - t2(8z/3)/2} * {t3(z)t3(3z) + t2(z)t2(3z)} + (1/2)*t2(8z/3) * {t3(z/3)t3(z) + t2(z/3)t2(z)}, where t3=theta_3, t2=theta_2.",
				"Expansion of a(x^3) * phi(x^8) + 2*x^2 * c(x^3) * psi(x^16) in powers of x where a(), c() are cubic AGM theta functions and phi(), psi() are Ramanujan theta functions.",
				"a(n) is the number of integer solutions [i, j, k] to n = 2*i^2 + (j^2 + j*k + k^2) / 3 where j, k == mod(i, 2) (mod 3). - _Michael Somos_, Jul 19 2014",
				"G.f.: Sum_{i, j, k in Z} x^(8*i^2 + 3*(j^2 + j*k + k^2)) * (1 + x^(3 + 8*i + 3*j + 3*k)). - _Michael Somos_, Jul 19 2014"
			],
			"example": [
				"G.f. = 1 + 12*x^3 + 6*x^6 + 2*x^8 + 18*x^9 + 12*x^11 + 6*x^12 + 12*x^15 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (QPochhammer[ x^3]^3 + 9 x^3 QPochhammer[ x^27]^3) / QPochhammer[ x^9] EllipticTheta[ 3, 0, x^8] + 3 x QPochhammer[ x^9]^3 / QPochhammer[ x^3]  EllipticTheta[ 2, 0, x^8], {x, 0, n}]; (* _Michael Somos_, Jul 19 2014 *)",
				"a[ n_] := SeriesCoefficient[ 6 x^3 QPochhammer[ x^32]^2 / ( QPochhammer[ x^3] QPochhammer[ x^16]) + 2 EllipticTheta[ 3, 0, x^3]  EllipticTheta[ 3, 0, x^8] EllipticTheta[ 3, 0, x^9] - EllipticTheta[ 4, 0, x^3]   EllipticTheta[ 4, 0, x^8] EllipticTheta[ 4, 0, x^9], {x, 0, n}]; (* _Michael Somos_, Jul 19 2014 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A, A0, A1); if( n\u003c0, 0, A = x * O(x^n); A1 = x^3 * eta(x^9 + A)^3 * eta(x^32 + A)^2 / (eta(x^3 + A) * eta(x^16 + A)); A0 = sum(k=1, sqrtint(n\\3), 2 * x^(3*k^2), 1 + A) * sum(k=1, sqrtint(n\\8), 2 * x^(8*k^2), 1 + A) * sum(k=1, sqrtint(n\\9), 2 * x^(9*k^2), 1 + A); polcoeff( 2*A0 + 6*A1 - subst(A0, x, -x), n))}; /* _Michael Somos_, Aug 03 2006 */"
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_",
			"references": 3,
			"revision": 26,
			"time": "2021-03-12T22:24:41-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}