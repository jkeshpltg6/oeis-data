{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A132356",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 132356,
			"data": "0,8,12,36,44,84,96,152,168,240,260,348,372,476,504,624,656,792,828,980,1020,1188,1232,1416,1464,1664,1716,1932,1988,2220,2280,2528,2592,2856,2924,3204,3276,3572,3648,3960,4040,4368,4452,4796,4884,5244,5336,5712",
			"name": "a(2*k) = k*(10*k+2), a(2*k+1) = 10*k^2 + 18*k + 8, with k \u003e= 0.",
			"comment": [
				"X values of solutions to the equation 10*X^3 + X^2 = Y^2.",
				"Polygonal number connection: 2*H_n + 6S_n, where H_n is the n-th hexagonal number and S_n is the n-th square number. This is the base formula that is expanded upon to achieve the full series. See contributing formula below. - _William A. Tedeschi_, Sep 12 2010",
				"Equivalently, numbers of the form 2*h*(5*h+1), where h = 0, -1, 1, -2, 2, -3, 3, -4, 4, ... . - _Bruno Berselli_, Feb 02 2017"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A132356/b132356.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,2,-2,-1,1)."
			],
			"formula": [
				"G.f.: 4*x*(2*x^2+x+2)/((1-x)^3*(1+x)^2). - _R. J. Mathar_, Apr 07 2008",
				"a(n) = 10*x^2 - 2*x, where x = floor(n/2)*(-1)^n for n \u003e= 1. - _William A. Tedeschi_, Sep 12 2010",
				"a(n) = ((2*n+1-(-1)^n)*(10*(2*n+1)-2*(-1)^n))/16. - _Luce ETIENNE_, Sep 13 2014",
				"a(n) = a(n-1) + 2*a(n-2) - 2*a(n-3) - a(n-4) + a(n-5) for n \u003e 4. - _Chai Wah Wu_, May 24 2016"
			],
			"mathematica": [
				"s=0;lst={};Do[s+=(4*n/5);If[IntegerQ[s],AppendTo[lst,s]],{n,0,6!}];lst (* _Vladimir Joseph Stephan Orlovsky_, Sep 26 2009 *)",
				"CoefficientList[Series[4*x*(2*x^2 + x + 2)/((1 - x)^3*(1 + x)^2), {x, 0, 50}], x] (* _G. C. Greubel_, Jun 12 2017 *)"
			],
			"program": [
				"(PARI) x='x+O('x^50); concat([0], Vec(4*x*(2*x^2+x+2)/((1-x)^3*(1+x)^2))) \\\\ _G. C. Greubel_, Jun 12 2017"
			],
			"xref": [
				"Cf. A054000, A056220, A087475, A028347, A046092, A132209.",
				"Cf. numbers m such that k*m+1 is a square: A005563 (k=1), A046092 (k=2), A001082 (k=3), A002378 (k=4), A036666 (k=5), A062717 (k=6), A132354 (k=7), A000217 (k=8), A132355 (k=9), A219257 (k=11), A152749 (k=12), A219389 (k=13), A219390 (k=14), A204221 (k=15), A074378 (k=16), A219394 (k=17), A219395 (k=18), A219396 (k=19), A219190 (k=20), A219391 (k=21), A219392 (k=22), A219393 (k=23), A001318 (k=24), A219259 (k=25), A217441 (k=26), A219258 (k=27), A219191 (k=28).",
				"Cf. A220082 (numbers k such that 10*k-1 is a square)."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Mohamed Bouhamida_, Nov 08 2007",
			"ext": [
				"More terms from _Max Alekseyev_, Nov 13 2009"
			],
			"references": 11,
			"revision": 48,
			"time": "2020-02-15T10:52:27-05:00",
			"created": "2007-12-09T03:00:00-05:00"
		}
	]
}