{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A179118",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 179118,
			"data": "1,7,5,19,12,26,27,121,122,35,36,156,113,52,53,98,99,100,101,102,72,166,167,168,169,170,171,247,173,187,188,251,252,178,179,317,243,195,196,153,154,155,156,400,326,495,496,161,162,331,332,408,471,410,411,337,338,339,340,553",
			"name": "Number of Collatz steps to reach 1 starting with 2^n + 1.",
			"comment": [
				"There are many long runs of consecutive terms that increase by 1 (see second conjecture in A277109). For n \u003c 40000, the longest run has 1030 terms starting from a(33237) = 244868 and ending with a(34266) = 245897. - _Dmitry Kamenetsky_, Sep 30 2016"
			],
			"link": [
				"Dmitry Kamenetsky, \u003ca href=\"/A179118/b179118.txt\"\u003eTable of n, a(n) for n = 0..9999\u003c/a\u003e (terms n = 1...1000 from Mitch Harris)",
				"MathOverflow, \u003ca href=\"http://mathoverflow.net/questions/51124/collatz-conjecture-for-numbers-of-th-form-2n-1\"\u003eIntroduced on Mathoverflow by Henrik Rüping\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A006577(2^n+1) = A006577(A000051(n)).",
				"a(n) = A075486(n) - 1. - _T. D. Noe_, Jan 17 2013"
			],
			"example": [
				"a(1)=7 because the trajectory of 2^1+1=3 is (3,10,5,16,8,4,2,1)."
			],
			"mathematica": [
				"CollatzNext[n_] := If[Mod[n, 2] == 0, n/2, 3 n + 1]; CollatzPath[n_] := CollatzPath[n] = Module[{k = n, l = {}}, While[k != 1, k = CollatzNext[k]; l = Append[l, k]]; l]; Collatz[n_] := Length[CollatzPath[n]]; Table[Collatz[2^n+1],{n,1,50}]",
				"f[n_] := Length@ NestWhileList[If[OddQ@ #, 3 # + 1, #/2] \u0026, 2^n + 1, # \u003e 1 \u0026] - 1; Array[f, 60] (* _Robert G. Wilson v_, Jan 05 2011 *)",
				"Array[-1 + Length@ NestWhileList[If[EvenQ@ #, #/2, 3 # + 1] \u0026, 2^# + 1, # \u003e 1 \u0026] \u0026, 60, 0] (* _Michael De Vlieger_, Nov 25 2018 *)"
			],
			"program": [
				"(Python)",
				"def steps(a):",
				"  if a==1:     return 0",
				"  elif a%2==0: return 1+steps(a//2)",
				"  else:        return 1+steps(a*3+1)",
				"for n in range(60):",
				"  print(n, steps((1\u003c\u003cn)+1))",
				"(PARI) nbsteps(n)= s=n; c=0; while(s\u003e1, s=if(s%2, 3*s+1, s/2); c++); c;",
				"a(n) = nbsteps(2^n+1); \\\\ _Michel Marcus_, Oct 28 2018"
			],
			"xref": [
				"Cf. A000051, A006577, A070976, A074472, A075486, A193688 (starting with 2^n-1), , A179118, A277109."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Mitch Harris_, Jan 04 2011",
			"ext": [
				"a(0)=1 prepended by _Alois P. Heinz_, Dec 12 2018"
			],
			"references": 4,
			"revision": 30,
			"time": "2021-05-20T08:20:02-04:00",
			"created": "2010-11-12T14:28:36-05:00"
		}
	]
}