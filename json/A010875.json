{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A010875",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 10875,
			"data": "0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5,0",
			"name": "a(n) = n mod 6.",
			"comment": [
				"Period 6: repeat [0, 1, 2, 3, 4, 5].",
				"The rightmost digit in the base-6 representation of n. - _Hieronymus Fischer_, Jun 11 2007",
				"[a(n) * a(m)] mod 6 == a(n*m mod 6) == a(n*m). - _Jon Perry_, Nov 11 2014",
				"If n \u003e 3 and (a(n) is in {0,2,3,4}), then n is not prime. - _Jean-Marc Rebert_, Jul 22 2015, corrected by _M. F. Hasler_, Jul 24 2015"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A010875/b010875.txt\"\u003eTable of n, a(n) for n = 0..65538\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,0,0,0,0,1)."
			],
			"formula": [
				"Complex representation: a(n) = (1/6) * (1 - r^n) * Sum_{k = 1..6} k * Product_{1 \u003c= m \u003c 6, m \u003c\u003e k} (1-r^(n-m)), where r = exp((Pi/3)*i) = (1 + sqrt(3)*i)/2 and i = sqrt(-1).",
				"Trigonometric representation: a(n) = (16/3)^2 * (sin(n*Pi/6))^2 * Sum_{k = 1..6} k * Product_{1 \u003c= m \u003c 6, m\u003c\u003ek} (sin((n-m)*Pi/6))^2.",
				"G.f.: g(x) = (Sum_{k = 1..6} k*x^k)/(1-x^6).",
				"Also: g(x) = x*(5*x^6 - 6*x^5 + 1)/((1 - x^6)*(1 - x)^2). - _Hieronymus Fischer_, May 31 2007",
				"a(n) = (n mod 2) + 2(floor(n/2) mod 3) = A000035(n) +2*A010872(A004526(n));",
				"a(n) = (n mod 3) + 3(floor(n/3) mod 2) = A010872(n) +3*A000035(A002264(n)). - _Hieronymus Fischer_, Jun 11 2007",
				"a(n) = 2.5 - 0.5*(-1)^n - cos(Pi*n/3) - 3^0.5*sin(Pi*n/3) -cos(2*Pi*n/3) - 3^0.5/3*sin(2*Pi*n/3). - _Richard Choulet_, Dec 11 2008",
				"a(n) = n^3 mod 6. - _Zerinvary Lajos_, Oct 29 2009",
				"a(n) = floor(12345/999999*10^(n+1)) mod 10. - _Hieronymus Fischer_, Jan 03 2013",
				"a(n) = floor(373/9331*6^(n+1)) mod 6. - _Hieronymus Fischer_, Jan 04 2013",
				"a(n) = 5/2 - (-1)^n/2 - 2*0^((-1)^(n/6 - 1/12 + (-1)^n/12) - (-1)^(n/2 - 1/4 +(-1)^n/4)) + 2*0^((-1)^(n/6 + 1/4 + (-1)^n/12) + (-1)^(n/2 - 1/4 + (-1)^n/4)). - _Wesley Ivan Hurt_, Jun 23 2015",
				"E.g.f.: -sqrt(3)*exp(x/2)*sin(sqrt(3)*x/2) - 2*cosh(x/2)*cos(sqrt(3)*x/2). - _Robert Israel_, Jul 22 2015"
			],
			"maple": [
				"A010875:=n-\u003en mod 6; seq(A010875(n), n=0..100); # _Wesley Ivan Hurt_, Jul 06 2014"
			],
			"mathematica": [
				"Mod[Range[0, 100], 6] (* _Wesley Ivan Hurt_, Jul 06 2014 *)"
			],
			"program": [
				"(Sage) [power_mod(n,3,6 )for n in range(0, 81)] # _Zerinvary Lajos_, Oct 29 2009",
				"(PARI) a(n)=n%6 \\\\ _Charles R Greathouse IV_, Dec 05 2011",
				"(MAGMA) [n mod 6: n in [0..100]]; // _Wesley Ivan Hurt_, Jul 06 2014",
				"(Scheme) (define (A010875 n) (modulo n 6)) ;; _Antti Karttunen_, Dec 22 2017"
			],
			"xref": [
				"Partial sums: A130484. Other related sequences A130481, A130482, A130483, A130485.",
				"Cf. also A079979, A097325, A122841."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Formulas 1 to 6 re-edited for better readability by _Hieronymus Fischer_, Dec 05 2011",
				"More terms from _Antti Karttunen_, Dec 22 2017"
			],
			"references": 45,
			"revision": 81,
			"time": "2020-08-12T19:45:40-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}