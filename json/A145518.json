{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A145518",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 145518,
			"data": "2,3,4,5,6,8,7,19,12,16,11,29,38,24,32,13,68,85,76,48,64,17,94,181,170,152,96,128,19,177,326,443,340,304,192,256,23,231,683,787,886,680,608,384,512,29,400,1066,1780,1817,1772,1360,1216,768,1024,31,484,1899,3119",
			"name": "Triangle read by rows: T1[n,k;x] := Sum_{partitions with k parts p(n, k; m_1, m_2, m_3, ..., m_n)} x_1^m_1 * x_2^m_2 * ... x^n*m_n, for x_i = A000040(i).",
			"comment": [
				"Let p(n; m_1, m_2, m_3, ..., m_n) denote a partition of integer n in exponential representation, i.e., the m_i are the counts of parts i and satisfy 1*m_1 + 2*m_2 + 3*m_3 + ... + n*m_n = n.",
				"Let p(n, k; m_1, m_2, m_3, ..., m_n) be the partitions of n into exactly k parts; these are further constrained by m_1 + m_2 + m_3 + ... + m_n = k.",
				"Then the triangle is given by T1[n,k;x] := Sum_{all p(n, k; m_1, m_2, m_3, ..., m_n)} x_1^m_1 * x_2^m_2 * ... x^n*m_n, where x_i is the i-th prime number (A000040).",
				"2nd column (4, 6, 19, 29, 68, 94, 177, ...) is A024697.",
				"Row sums give A145519."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A145518/b145518.txt\"\u003eRows n = 1..141, flattened\u003c/a\u003e",
				"Tilman Neumann, \u003ca href=\"http://www.tilman-neumann.de/science.html\"\u003eMore terms, partition generator and transform implementation\u003c/a\u003e."
			],
			"example": [
				"Triangle starts:",
				"   2;",
				"   3,   4;",
				"   5,   6,   8;",
				"   7,  19,  12,  16;",
				"  11,  29,  38,  24,  32;",
				"  13,  68,  85,  76,  48,  64;",
				"  ..."
			],
			"maple": [
				"g:= proc(n, i) option remember; `if`(n=0 or i=1, (2*x)^n,",
				"      expand(add(g(n-i*j, i-1)*(ithprime(i)*x)^j, j=0..n/i)))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=1..n))(g(n$2)):",
				"seq(T(n), n=1..12);  # _Alois P. Heinz_, May 25 2015"
			],
			"mathematica": [
				"g[n_, i_] := g[n, i] = If[n==0 || i==1, (2 x)^n, Expand[Sum[g[n-i*j, i-1]*(Prime[i]*x)^j, {j, 0, n/i}]]]; T[n_] := Function[{p}, Table[Coefficient[p, x, i], {i, 1, n}]][g[n, n]]; Table[T[n], {n, 1, 12}] // Flatten (* _Jean-François Alcover_, Jul 15 2015, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A000040, A024697, A145519, A145520, A258323."
			],
			"keyword": "nonn,tabl,changed",
			"offset": "1,1",
			"author": "_Tilman Neumann_, Oct 12 2008",
			"ext": [
				"Reference to more terms etc. changed to make it version independent by _Tilman Neumann_, Sep 02 2009"
			],
			"references": 5,
			"revision": 23,
			"time": "2022-01-03T21:12:45-05:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}