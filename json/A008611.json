{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008611",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8611,
			"data": "1,0,1,2,1,2,3,2,3,4,3,4,5,4,5,6,5,6,7,6,7,8,7,8,9,8,9,10,9,10,11,10,11,12,11,12,13,12,13,14,13,14,15,14,15,16,15,16,17,16,17,18,17,18,19,18,19,20,19,20,21,20,21,22,21,22,23,22,23,24,23,24,25,24,25,26,25,26,27,26,27,28",
			"name": "a(n) = a(n-3) + 1, with a(0)=a(2)=1, a(1)=0.",
			"comment": [
				"Molien series of 2-dimensional representation of cyclic group of order 3 over GF(2).",
				"One step back, two steps forward.",
				"The crossing number of the graph C(n, {1,3}), n \u003e= 8, is [n/3] + n mod 3, which gives this sequence starting at the first 4. [Yang Yuansheng et al.]",
				"A Chebyshev transform of A078008. The g.f. is the image of (1-x)/(1-x-2x^2) (g.f. of A078008) under the Chebyshev transform A(x)-\u003e 1/(1+x^2))A(x/(1+x^2)). - _Paul Barry_, Oct 15 2004",
				"A047878 is an essentially identical sequence. - Anton Chupin, Oct 24 2009",
				"Rhyme scheme of Dante Alighieri's \"Divine Comedy.\" - David Gaita, Feb 11 2011",
				"A194960 results from deleting the first four terms of A008611. Note that deleting the first term or first four terms of A008611 leaves a concatenation of segments (n, n+1, n+2); for related concatenations, see",
				"  A008619, (n,n+1) after deletion of first term;",
				"  A053737, (n,n+1,n+2,n+3) beginning with n=0;",
				"  A053824, (n to n+4) beginning with n=0. - _Clark Kimberling_, Sep 07 2011",
				"It appears that a(n) is the number of roots of x^(n+1) + x + 1 inside the unit circle. - _Michel Lagneau_, Nov 02 2012",
				"Also apparently for n \u003e= 2: a(n) is the largest remainder r that results from dividing n+2 by 1..n+2 more than once, i.e., a(n) = max(i, A072528(n+2,i)\u003e1). - _Ralf Stephan_, Oct 21 2013",
				"Number of n-element subsets of [n+1] whose sum is a multiple of 3. a(4) = 1: {1,2,4,5}. - _Alois P. Heinz_, Feb 06 2017",
				"It appears that a(n) is the number of roots of the Fibonacci polynomial F(n+2,x) strictly inside the unit circle of the complex plane. - _Michel Lagneau_, Apr 07 2017",
				"For the proof of the preceding conjecture see my comments under A008615 and A049310. Chebyshev S(n,x) = i^n*F(n+1,-i*x), with i = sqrt(-1). - _Wolfdieter Lang_, May 06 2017",
				"The sequence is the interleaving of three sequences: the positive integers (A000027), the nonnegative integers (A001477), and the positive integers, in that order. - _Guenther Schrack_, Nov 07 2020"
			],
			"reference": [
				"D. J. Benson, Polynomial Invariants of Finite Groups, Cambridge, 1993, p. 103."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A008611/b008611.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=447\"\u003eEncyclopedia of Combinatorial Structures 447\u003c/a\u003e",
				"G. P. Michon, \u003ca href=\"http://www.numericana.com/data/polyhedra.htm\"\u003eCounting Polyhedra\u003c/a\u003e",
				"Yang Yuansheng et al., \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2004.08.014\"\u003eThe crossing number of C(n; {1,3})\u003c/a\u003e, Discr. Math. 289 (2004), 107-118.",
				"\u003ca href=\"/index/Mo#Molien\"\u003eIndex entries for Molien series\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,0,1,-1)."
			],
			"formula": [
				"a(n) = a(n-3) + 1.",
				"a(n) = (n-1) - 2*floor((n-1)/3).",
				"G.f.: (1 + x^2 + x^4)/(1 - x^3)^2.",
				"After the initial term, has form {n, n+1, n+2} for n=0, 1, 2, ...",
				"From _Paul Barry_, Mar 18 2004: (Start)",
				"a(n) = Sum_{k=0..n} (-1)^floor(2*(k-2)/3);",
				"a(n) = 4*sqrt(3)*cos(2*Pi*n/3 + Pi/6)/9 + (n+1)/3. (End)",
				"From _Paul Barry_, Oct 15 2004: (Start)",
				"G.f.: (1 - x + x^2)/((1 + x + x^2)*(x-1)^2);",
				"a(n) = Sum_{k=0..floor(n/2)} binomial(n-k, k)*A078008(n-2k)*(-1)^k. (End)",
				"a(n) = -a(-2-n) for all n in Z.",
				"Euler transform of length 6 sequence [0, 1, 2, 0, 0, -1]. - _Michael Somos_, Jan 23 2014",
				"a(n) = ((n-1) mod 3) + floor((n-1)/3). - _Wesley Ivan Hurt_, May 18 2014",
				"PSUM transform of A257075. - _Michael Somos_, Apr 15 2015",
				"a(n) = A194960(n-3), n \u003e= 0, with extended A194960. See the a(n) formula two lines above. - _Wolfdieter Lang_, May 06 2017",
				"From _Guenther Schrack_, Nov 07 2020: (Start)",
				"a(n) = (3*n + 3 + 2*(w^(2*n)*(1 - w) + w^n*(2 + w)))/9, where w = (-1 + sqrt(-3))/2, a primitive third root of unity;",
				"a(n) = (n + 1 + 2*A049347(n))/3;",
				"a(n) = (2*n - A330396(n-1))/3. (End)"
			],
			"example": [
				"G.f. = 1 + x^2 + 2*x^3 + x^4 + 2*x^5 + 3*x^6 + 2*x^7 + 3*x^8 + 4*x^9 + ..."
			],
			"maple": [
				"with(numtheory): for n from 1 to 70 do:it:=0:",
				"y:=[fsolve(x^n+x+1, x, complex)] : for m from 1 to nops(y) do : if abs(y[m])\u003c 1 then it:=it+1:else fi:od: printf(`%d, `,it):od:",
				"A008611:=n-\u003e(n-1)-2*floor((n-1)/3); seq(A008611(n), n=0..50); # _Wesley Ivan Hurt_, May 18 2014"
			],
			"mathematica": [
				"With[{nn=30},Riffle[Riffle[Range[nn],Range[0,nn-1]],Range[nn],3]] (* or *) RecurrenceTable[{a[0]==a[2]==1,a[1]==0,a[n]==a[n-3]+1},a,{n,90}] (* _Harvey P. Dale_, Nov 06 2011 *)",
				"LinearRecurrence[{1, 0, 1, -1}, {1, 0, 1, 2}, 100] (* _Vladimir Joseph Stephan Orlovsky_, Feb 23 2012 *)",
				"a[ n_] := Quotient[n - 1, 3] + Mod[n + 2, 3]; (* _Michael Somos_, Jan 23 2014 *)"
			],
			"program": [
				"(MAGMA) [(n-1)-2*Floor((n-1)/3): n in [0..90]]; // _Vincenzo Librandi_, Aug 21 2011",
				"(Haskell)",
				"a008611 n = n' + mod r 2 where (n', r) = divMod (n + 1) 3",
				"a008611_list = f [1,0,1] where f xs = xs ++ f (map (+ 1) xs)",
				"-- _Reinhard Zumkeller_, Nov 25 2013",
				"(PARI) {a(n) = (n-1) \\ 3 + (n+2) % 3}; /* _Michael Somos_, Jan 23 2014 */"
			],
			"xref": [
				"Cf. A047878, A049347, A058207, A058788, A194960, A257075, A330396."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_, Mar 15 1996",
			"references": 42,
			"revision": 105,
			"time": "2020-12-23T07:40:20-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}