{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A176282",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 176282,
			"data": "1,1,1,1,4,1,1,9,9,1,1,16,21,16,1,1,25,37,37,25,1,1,36,57,64,57,36,1,1,49,81,97,97,81,49,1,1,64,109,136,145,136,109,64,1,1,81,141,181,201,201,181,141,81,1,1,100,177,232,265,276,265,232,177,100,1",
			"name": "Triangle T(n,k) = 1 + A000330(n) - A000330(k) - A000330(n-k), read by rows.",
			"comment": [
				"Not summing squares but summing integers implied by the definition (i.e., not using A000330 but A000217) gives A077028.",
				"Row sums = {1, 2, 6, 20, 55, 126, 252, 456, 765, 1210, 1826, ...} = (n+1)*(n+2)*(n^2-2*n+3)/6."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A176282/b176282.txt\"\u003eRows n = 0..100 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = T(n,n-k).",
				"T(n,k) = 1 + k*(n+1)*(n-k). - _G. C. Greubel_, Nov 24 2019"
			],
			"example": [
				"Triangle begins as:",
				"  1;",
				"  1,   1;",
				"  1,   4,   1;",
				"  1,   9,   9,   1;",
				"  1,  16,  21,  16,   1;",
				"  1,  25,  37,  37,  25,   1;",
				"  1,  36,  57,  64,  57,  36,   1;",
				"  1,  49,  81,  97,  97,  81,  49,   1;",
				"  1,  64, 109, 136, 145, 136, 109,  64,   1;",
				"  1,  81, 141, 181, 201, 201, 181, 141,  81,   1;",
				"  1, 100, 177, 232, 265, 276, 265, 232, 177, 100, 1;"
			],
			"maple": [
				"seq(seq(1 + k*(n+1)*(n-k), k=0..n), n=0..12); # _G. C. Greubel_, Nov 24 2019"
			],
			"mathematica": [
				"(* Sequence for q=1..10 *)",
				"f[n_, k_, q_]:= f[n, k, q] = 1 +Sum[i^q, {i,0,n}] -Sum[i^q, {i,0,k}] + Sum[i^q, {i,0,n-k}]; Table[Flatten[Table[f[n, k, q],{n,0,12}, {k,0,n}]], {q,1,10}]",
				"(* Second program *)",
				"Table[1 + k*(n+1)*(n-k), {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, Nov 24 2019 *)"
			],
			"program": [
				"(PARI) T(n,k) = 1 + k*(n+1)*(n-k); \\\\ _G. C. Greubel_, Nov 24 2019",
				"(MAGMA) [1 + k*(n+1)*(n-k): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Nov 24 2019",
				"(Sage) [[1 + k*(n+1)*(n-k) for k in (0..n)] for n in (0..12)] # _G. C. Greubel_, Nov 24 2019",
				"(GAP) Flat(List([0..12], n-\u003e List([0..n], k-\u003e 1 + k*(n+1)*(n-k) ))); # _G. C. Greubel_, Nov 24 2019"
			],
			"xref": [
				"Cf. A077028."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,5",
			"author": "_Roger L. Bagula_, Apr 14 2010",
			"ext": [
				"Edited by _R. J. Mathar_, May 03 2013"
			],
			"references": 2,
			"revision": 13,
			"time": "2019-11-25T01:02:59-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}