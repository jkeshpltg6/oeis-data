{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A106852",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 106852,
			"data": "1,1,-2,-5,1,16,13,-35,-74,31,253,160,-599,-1079,718,3955,1801,-10064,-15467,14725,61126,16951,-166427,-217280,282001,933841,87838,-2713685,-2977199,5163856,14095453,-1396115,-43682474,-39494129,91553293,210035680,-64624199,-694731239,-500858642",
			"name": "Expansion of 1/(1-x*(1-3*x)).",
			"comment": [
				"Row sums of Riordan array (1, x*(1-3*x)). In general, Sum_{k=0..n} (-1)^(n-k)*binomial(k,n-k)*r^(n-k) yields the row sums of the Riordan array (1, x(1-kx)).",
				"Row sums of Riordan array (1/(1+3*x^2), x/(1+3*x^2)). - _Paul Barry_, Sep 10 2005",
				"See A214733 for a differently signed version of this sequence. - _Peter Bala_, Nov 21 2016"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A106852/b106852.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,-3)."
			],
			"formula": [
				"From _Paul Barry_, Sep 10 2005: (Start)",
				"G.f.: 1/(1-x+3*x^2).",
				"a(n) = 2*sqrt(33)*3^(n/2)*cos((n+1)*arctan(sqrt(11)/11)-pi*n/2)/11.",
				"a(n) = 3^(n/2)(cos(-n*arccot(sqrt(11)/11))-sqrt(11)*sin(-n*arccot(sqrt(11)/11))/11).",
				"a(n) = ((1+sqrt(-11))^(n+1)-(1-sqrt(-11))^(n+1))/(2^(n+1)sqrt(-11)).",
				"a(n) = Sum_{k=0..n} (-1)^(n-k)*binomial(k, n-k)*3^(n-k) = Sum_{k=0..n} A109466(n,k)*3^(n-k).",
				"a(n) = Sum_{k=0..n} C((n+k)/2, k)*(-3)^((n-k)/2)*(1+(-1)^(n-k))/2.",
				"a(n) = Sum_{k=0..floor(n/2)} C(n-k, k)(-3)^k. (End)",
				"a(n) = a(n-1) - 3*a(n-2), a(0)=1, a(1)=1. - _Philippe Deléham_, Oct 21 2008",
				"G.f.: Q(0)/x -1/x, where Q(k) = 1 - 3*x^2 + (k+2)*x - x*(k+1 - 3*x)/Q(k+1); (continued fraction). - _Sergei N. Gladkovskii_, Oct 07 2013"
			],
			"mathematica": [
				"CoefficientList[Series[1/(1 - x (1 - 3 x)), {x, 0, 40}], x] (* _Vincenzo Librandi_, Oct 07 2013 *)",
				"LinearRecurrence[{1,-3},{1,1},40] (* _Harvey P. Dale_, Apr 02 2016 *)"
			],
			"program": [
				"(Sage) [lucas_number1(n,1,+3) for n in range(1, 40)] # _Zerinvary Lajos_, Apr 22 2009",
				"(PARI) a(n)=([0,1; -3,1]^n*[1;1])[1,1] \\\\ _Charles R Greathouse IV_, Nov 21 2016",
				"(PARI) x='x+O('x^30); Vec(1/(1-x+3*x^2)) \\\\ _G. C. Greubel_, Jan 14 2018",
				"(MAGMA) I:=[1,1]; [n le 2 select I[n] else Self(n-1) - 3*Self(n-2): n in [1..30]]; // _G. C. Greubel_, Jan 14 2018"
			],
			"xref": [
				"Cf. A214733."
			],
			"keyword": "sign,easy",
			"offset": "0,3",
			"author": "_Paul Barry_, May 08 2005",
			"references": 18,
			"revision": 55,
			"time": "2020-02-28T08:18:29-05:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}