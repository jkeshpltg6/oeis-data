{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A134531",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 134531,
			"data": "0,1,-1,5,-79,3377,-362431,93473345,-56272471039,77442176448257,-239804482525402111,1650172344732021412865,-24981899010711376986398719,825164608171793476724052668417,-59053816996641612758331731690504191,9102696765174239045811746247171452452865",
			"name": "G.f.: Sum_{n\u003e=0} a(n)*x^n/(n!*2^(n*(n-1)/2)) = log( Sum_{n\u003e=0} x^n/(n!*2^(n*(n-1)/2)) ).",
			"link": [
				"Kimmo Berg, \u003ca href=\"https://doi.org/10.1007/s10479-013-1334-3\"\u003eComplexity of solution structures in nonlinear pricing\u003c/a\u003e, Ann. Oper. Res. 206, 23-37 (2013).",
				"Sergei Chmutov, Maxim Kazarian and Sergey Lando, \u003ca href=\"https://arxiv.org/abs/1803.09800\"\u003ePolynomial graph invariants and the KP hierarchy\u003c/a\u003e, arXiv:1803.09800 [math.CO], 2018."
			],
			"formula": [
				"Equals column 0 of triangle A134530, which is the matrix log of triangle A111636, where A111636(n,k) = (2^k)^(n-k)*C(n,k).",
				"From _Peter Bala_, Apr 01 2013: (Start)",
				"Let E(x) = Sum_{n \u003e= 0} x^n/(n!*2^C(n,2)). Then a generating function for this sequence (but with a different offset) is E(x)/E(2*x) = Sum_{n \u003e= 0} a(n-1)*x^n/(n!*2^C(n,2)) = 1 - x + 5*x^2/(2!*2) - 79*x^3/(3!*2^3) + 3377*x^4/(4!*2^6) - ....",
				"Recurrence equation:",
				"a(n) = 1 - Sum_{k = 1..n-1} 2^(k*(n-k))*C(n-1,k-1)*a(k) with a(1) = 1. (End)",
				"a(n) = (-1)^(n-1)*A003025(n)/n. - _Andrew Howroyd_, Jan 07 2022"
			],
			"example": [
				"Let g.f. G(x) = Sum_{n\u003e=0} a(n)*x^n/[ n! * 2^(n*(n-1)/2) ]",
				"then exp(G(x)) = Sum_{n\u003e=0} x^n/[ n! * 2^(n*(n-1)/2) ];",
				"G.f.: G(x) = x - x^2/4 + 5x^3/48 - 79x^4/1536 + 3377x^5/122880 + ...",
				"exp(G(x)) = 1 + x + x^2/4 + x^3/48 + x^4/1536 + x^5/122880 + ..."
			],
			"mathematica": [
				"a[0] = 0;",
				"a[n_] := a[n] = 1 - Sum[2^(k(n-k)) Binomial[n-1, k-1] a[k], {k, 1, n-1}];",
				"Table[a[n], {n, 0, 15}] (* _Jean-François Alcover_, Jul 26 2018 *)"
			],
			"program": [
				"(PARI) {a(n)=n!*2^(n*(n-1)/2)*polcoeff(log(sum(k=0,n,x^k/(k!*2^(k*(k-1)/2)))+x*O(x^n)),n)}"
			],
			"xref": [
				"Cf. related triangles: A134530, A111636.",
				"Cf. A003025, A011266, A118197 (variant)."
			],
			"keyword": "sign,easy,changed",
			"offset": "0,4",
			"author": "_Paul D. Hanna_, Oct 30 2007",
			"references": 5,
			"revision": 24,
			"time": "2022-01-08T10:11:56-05:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}