{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A046932",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 46932,
			"data": "1,3,7,15,21,63,127,63,73,889,1533,3255,7905,11811,32767,255,273,253921,413385,761763,5461,4194303,2088705,2097151,10961685,298935,125829105,17895697,402653181,10845877,2097151,1023,1057,255652815,3681400539",
			"name": "a(n) = period of x^n + x + 1 over GF(2), i.e., the smallest integer m\u003e0 such that x^n + x + 1 divides x^m + 1 over GF(2).",
			"comment": [
				"Also, the multiplicative order of x modulo the polynomial x^n + x + 1 (or its reciprocal x^n + x^(n-1) + 1) over GF(2).",
				"For n\u003e1, let S_0 = 11...1 (n times) and S_{i+1} be formed by applying D to last n bits of S_i and appending result to S_i, where D is the first difference modulo 2 (e.g., a,b,c,d,e -\u003e a+b,b+c,c+d,d+e). The period of the resulting infinite string is a(n). E.g., n=4 produces 1111000100110101111..., so a(4) = 15.",
				"Also, the sequence can be constructed in the same way as A112683, but using the recurrence x(i) = 2*x(i-1)^2 + 2*x(i-1) + 2*x(i-n)^2 + 2*x(i-n) mod 3.",
				"From _Ben Branman_, Aug 12 2010: (Start)",
				"Additionally, the pseudorandom binary sequence determined by the recursion",
				"If x\u003cn+1, then f(x)=1. If x\u003en, f(x)=f(x-1) XOR f(x-n).",
				"The resulting sequence f(x) has period a(n).",
				"For example, if n=4, then the sequence f(x) is has period 15: 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0",
				"so a(4)=15. (End)"
			],
			"link": [
				"Max Alekseyev, \u003ca href=\"/A046932/b046932.txt\"\u003eTable of n, a(n) for n = 1..1223\u003c/a\u003e",
				"Tej Bade, Kelly Cui, Antoine Labelle, Deyuan Li, \u003ca href=\"https://arxiv.org/abs/2008.02762\"\u003eUlam Sets in New Settings\u003c/a\u003e, arXiv:2008.02762 [math.CO], 2020. See also \u003ca href=\"http://math.colgate.edu/~integers/u102/u102.pdf\"\u003eIntegers\u003c/a\u003e (2020) Vol. 20, #A102",
				"L. Bartholdi, \u003ca href=\"http://arXiv.org/abs/math.CO/9910056\"\u003eLamps, Factorizations and Finite Fields\u003c/a\u003e, Amer. Math. Monthly (2000), 107(5), 429-436.",
				"Steven R. Finch, \u003ca href=\"http://www.people.fas.harvard.edu/~sfinch/csolve/seqmod3.pdf\"\u003ePeriodicity in Sequences Mod 3\u003c/a\u003e [Broken link]",
				"Steven R. Finch, \u003ca href=\"https://web.archive.org/web/20150911081920/http://www.people.fas.harvard.edu/~sfinch/csolve/seqmod3.pdf\"\u003ePeriodicity in Sequences Mod 3\u003c/a\u003e [From the Wayback machine]",
				"International Math Olympiad, \u003ca href=\"http://www.artofproblemsolving.com/Forum/viewtopic.php?t=62190\"\u003eProblem 6, 1993\u003c/a\u003e.",
				"\u003ca href=\"/index/Tri#trinomial\"\u003eIndex entries for sequences related to trinomials over GF(2)\u003c/a\u003e"
			],
			"formula": [
				"a(2^k) = 2^(2*k) - 1.",
				"a(2^k + 1) = 2^(2*k) + 2^k + 1.",
				"Conjecture: a(2^k - 1) = 2^a(k) - 1. [See Bartholdi, 2000]",
				"More general conjecture: a( (2^(k*m) - 1) / (2^m-1) ) = (2^(a(k)*m) - 1) / (2^m-1). For m=1, it would imply Bartholdi conjecture. - _Max Alekseyev_, Oct 21 2011"
			],
			"mathematica": [
				"(* This program is not suitable to compute a large number of terms. *)",
				"a[n_] := Module[{f, ff}, f[x_] := f[x] = If[x\u003cn+1, 1, f[x-1] ~BitXor~ f[x-n]]; ff = Array[f, 10^5]; FindTransientRepeat[ff, 2] // Last // Length]; Array[a, 15] (* _Jean-François Alcover_, Sep 10 2018, after _Ben Branman_ *)"
			],
			"program": [
				"(PARI) a(n) = {pola = Mod(1,2)*(x^n+x+1); m=1; ok = 0; until (ok, polb = Mod(1,2)*(x^m+1); if (type(polb/pola) == \"t_POL\", ok = 1; break;); if (!ok, m++);); return (m);} \\\\ _Michel Marcus_, May 20 2013"
			],
			"xref": [
				"Cf. A010760, A055061, A073639, A100730, A112683."
			],
			"keyword": "nonn,easy,nice",
			"offset": "1,2",
			"author": "_Russell Walsmith_",
			"ext": [
				"More terms from _Dean Hickerson_",
				"Entry revised and b-file supplied by _Max Alekseyev_, Mar 14 2008",
				"b-file extended by _Max Alekseyev_, Nov 15 2014; Aug 17 2015"
			],
			"references": 7,
			"revision": 71,
			"time": "2020-12-04T20:42:28-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}