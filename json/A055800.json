{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A055800",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 55800,
			"data": "1,1,0,1,1,0,1,1,0,0,1,1,1,1,0,1,1,1,1,0,0,1,1,1,2,2,1,0,1,1,1,2,2,1,0,0,1,1,1,2,3,4,3,1,0,1,1,1,2,3,4,3,1,0,0,1,1,1,2,3,5,7,7,4,1,0,1,1,1,2,3,5,7,7,4,1,0,0,1,1,1,2,3,5,8,12,14,11,5,1,0",
			"name": "Triangle T read by rows: T(i,0)=1 for i \u003e= 0; T(i,i)=0 for i \u003e= 1; T(i,j) = Sum_{k=1..floor(i/2)} T(i-2k,j-2k+1) for 1 \u003c= j \u003c= i-1, where T(m,n) := 0 if m \u003c 0 or n \u003c 0.",
			"comment": [
				"T(i+j,j) is the number of strings (s(1),...,s(m)) of nonnegative integers s(k) such that m \u003c= i+1, s(m)=j and s(k)-s(k-1) is an odd positive integer for k=2,3,...,m.",
				"T(i+j,j) is the number of compositions of j consisting of at most i parts, all positive odd integers."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A055800/b055800.txt\"\u003eRows n = 0..100 of triangle, flattened\u003c/a\u003e",
				"C. Kimberling, \u003ca href=\"https://www.fq.math.ca/Scanned/40-4/kimberling.pdf\"\u003ePath-counting and Fibonacci numbers\u003c/a\u003e, Fib. Quart. 40 (4) (2002) 328-338, Example 2A."
			],
			"formula": [
				"G.f. for k-th diagonal: (1-x^2-x*(x/(1-x^2))^k)/(1-x-x^2). - _Vladeta Jovovic_, Mar 10 2005"
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  1,0;",
				"  1,1,0;",
				"  1,1,0,0;",
				"  1,1,1,1,0;",
				"  ...",
				"T(10,5) counts the strings 012345, 0125, 0145, 0345, 05.",
				"T(10,5) counts the compositions 11111, 113, 131, 311, 5."
			],
			"maple": [
				"T:= proc(n,k) option remember;",
				"    if n\u003c0 or k\u003c0 then 0;",
				"    elif k=0 then 1;",
				"    elif k=n then 0;",
				"    else add(T(n-2*j, k-2*j+1), j=1..floor(n/2)) ;",
				"    end if; end proc:",
				"seq(seq(T(n,k), k=0..n), n=0..15); # _G. C. Greubel_, Jan 24 2020"
			],
			"mathematica": [
				"T[n_, k_]:= T[n, k]= If[n\u003c0 || k\u003c0, 0, If[k==0, 1, If[k==n, 0, Sum[T[n-2*j, k- 2*j+1], {j, Floor[n/2]}]]]]; Table[T[n, k], {n,0,15}, {k,0,n}]//Flatten (* _G. C. Greubel_, Jan 24 2020 *)"
			],
			"program": [
				"(PARI) T(n,k) = if(n\u003c0 || k\u003c0, 0, if(k==0, 1, if(k==n, 0, sum(j=1, n\\2, T(n-2*j, k-2*j+1) ))));",
				"for(n=0,15, for(k=0,n, print1(T(n,k), \", \"))) \\\\ _G. C. Greubel_, Jan 23 2020",
				"(MAGMA)",
				"function T(n,k)",
				"  if n lt 0 or k lt 0 then return 0;",
				"  elif k eq 0 then return 1;",
				"  elif k eq n then return 0;",
				"  else return (\u0026+[T(n-2*j, k-2*j+1): j in [1..Floor(n/2)]]);",
				"  end if; return T; end function;",
				"[T(n,k): k in [0..n], n in [0..15]]; // _G. C. Greubel_, Jan 23 2020",
				"(Sage)",
				"@CachedFunction",
				"def T(n, k):",
				"    if (n\u003c0 or k\u003c0): return 0",
				"    elif (k==0): return 1",
				"    elif (k==n): return 0",
				"    else: return sum(T(n-2*j, k-2*j+1) for j in (1..floor(n/2)))",
				"[[T(n, k) for k in (0..n)] for n in (0..15)] # _G. C. Greubel_, Jan 23 2020",
				"(GAP)",
				"T:= function(n,k)",
				"    if n\u003c0 or k\u003c0 then return 0;",
				"    elif k=0 then return 1;",
				"    elif k=n then return 0;",
				"    else return Sum([1..Int(n/2)], j-\u003e T(n-2*j, k-2*j+1));",
				"    fi; end;",
				"Flat(List([0..15], n-\u003e List([0..n], k-\u003e T(n,k) ))); # _G. C. Greubel_, Jan 23 2020"
			],
			"xref": [
				"Row sums are powers of 2: A016116.",
				"T(2n, n)=A000045(n) for n \u003e= 1 (Fibonacci numbers).",
				"Cf. A027926."
			],
			"keyword": "nonn,tabl",
			"offset": "0,25",
			"author": "_Clark Kimberling_, May 28 2000",
			"ext": [
				"a(88)-a(90) from _Michel Marcus_, Jan 21 2019"
			],
			"references": 1,
			"revision": 26,
			"time": "2020-01-24T20:56:16-05:00",
			"created": "2000-06-15T03:00:00-04:00"
		}
	]
}