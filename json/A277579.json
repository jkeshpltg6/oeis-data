{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A277579",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 277579,
			"data": "1,0,1,1,1,2,3,3,4,6,7,9,13,15,19,25,31,38,48,59,74,90,111,136,166,201,246,297,357,431,522,621,745,892,1063,1263,1503,1780,2109,2491,2941,3463,4077,4783,5616,6576,7689,8981,10486,12207,14209,16516,19178,22231",
			"name": "Number of partitions of n for which the number of even parts is equal to the positive alternating sum of the parts.",
			"comment": [
				"In the first Maple program (improvable) AS gives the positive alternating sum of a finite sequence s, EP gives the number of even terms of a finite sequence of positive integers.",
				"For the specified value of n, the second Maple program lists the partitions of n counted by a(n).",
				"Also the number of integer partitions of n with as many even parts as odd parts in the conjugate partition. - _Gus Wiseman_, Jul 26 2021"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A277579/b277579.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e"
			],
			"example": [
				"a(9) = 6: [2,1,1,1,1,1,1,1], [3,2,1,1,1,1], [3,3,2,1], [4,2,2,1], [4,3,1,1], [5,4].",
				"a(10) = 7: [1,1,1,1,1,1,1,1,1,1], [3,2,2,1,1,1], [3,3,1,1,1,1], [4,2,1,1,1,1], [4,3,2,1], [5,5], [6,4].",
				"a(11) = 9: [2,1,1,1,1,1,1,1,1,1], [3,2,1,1,1,1,1,1], [3,3,2,1,1,1], [3,3,3,2], [4,2,2,1,1,1], [4,3,1,1,1,1], [5,2,2,2], [5,4,1,1], [6,5]."
			],
			"maple": [
				"with(combinat): AS := proc (s) options operator, arrow: abs(add((-1)^(i-1)*s[i], i = 1 .. nops(s))) end proc: EP := proc (s) local ct, j: ct := 0: for j to nops(s) do if `mod`(s[j], 2) = 0 then ct := ct+1 else  end if end do: ct end proc: a := proc (n) local P, c, k: P := partition(n): c := 0: for k to nops(P) do if AS(P[k]) = EP(P[k]) then c := c+1 else  end if end do: c end proc: seq(a(n), n = 0 .. 30);",
				"n := 8: with(combinat): AS := proc (s) options operator, arrow: abs(add((-1)^(i-1)*s[i], i = 1 .. nops(s))) end proc: EP := proc (s) local ct, j: ct := 0: for j to nops(s) do if `mod`(s[j], 2) = 0 then ct := ct+1 else  end if end do: ct end proc: P := partition(n): C := {}: for k to nops(P) do if AS(P[k]) = EP(P[k]) then C := `union`(C, {P[k]}) else  end if end do: C;",
				"# alternative Maple program:",
				"b:= proc(n, i, s, t) option remember; `if`(n=0,",
				"      `if`(s=0, 1, 0), `if`(i\u003c1, 0, b(n, i-1, s, t)+",
				"      `if`(i\u003en, 0, b(n-i, i, s+t*i-irem(i+1, 2), -t))))",
				"    end:",
				"a:= n-\u003e b(n$2, 0, 1):",
				"seq(a(n), n=0..60);"
			],
			"mathematica": [
				"b[n_, i_, s_, t_] := b[n, i, s, t] = If[n == 0, If[s == 0, 1, 0], If[i\u003c1, 0, b[n, i-1, s, t] + If[i\u003en, 0, b[n-i, i, s + t*i - Mod[i+1, 2], -t]]]]; a[n_] := b[n, n, 0, 1]; Table[a[n], {n, 0, 60}] (* _Jean-François Alcover_, Dec 21 2016, translated from Maple *)",
				"conj[y_]:=If[Length[y]==0,y,Table[Length[Select[y,#\u003e=k\u0026]],{k,1,Max[y]}]]; Table[Length[Select[IntegerPartitions[n],Count[#,_?EvenQ]==Count[conj[#],_?OddQ]\u0026]],{n,0,15}] (* _Gus Wiseman_, Jul 26 2021 *)"
			],
			"program": [
				"(Sage)",
				"def a(n):",
				"    AS = lambda s: abs(sum((-1)^i*t for i,t in enumerate(s)))",
				"    EP = lambda s: sum((t+1)%2 for t in s)",
				"    return sum(AS(p) == EP(p) for p in Partitions(n))",
				"print([a(n) for n in (0..30)]) # _Peter Luschny_, Oct 21 2016"
			],
			"xref": [
				"The sign-sensitive version is A035457 (aerated version of A000009).",
				"Comparing odd parts to odd conjugate parts gives A277103.",
				"Comparing product of parts to product of conjugate parts gives A325039.",
				"Comparing the rev-alt sum to that of the conjugate gives A345196.",
				"A000041 counts partitions of 2n with alternating sum 0, ranked by A000290.",
				"A103919 counts partitions by sum and alternating sum (reverse: A344612).",
				"A120452 counts partitions of 2n with rev-alt sum 2 (negative: A344741).",
				"A124754 gives alternating sums of standard compositions (reverse: A344618).",
				"A316524 is the alternating sum of the prime indices of n (reverse: A344616).",
				"A344610 counts partitions by sum and positive reverse-alternating sum.",
				"A344611 counts partitions of 2n with reverse-alternating sum \u003e= 0.",
				"Cf. A000070, A000097, A000700, A006330, A027187, A027193, A236559, A257991, A325534, A325535, A344607, A344651."
			],
			"keyword": "nonn",
			"offset": "0,6",
			"author": "_Emeric Deutsch_ and _Alois P. Heinz_, Oct 20 2016",
			"references": 7,
			"revision": 24,
			"time": "2021-08-01T16:40:44-04:00",
			"created": "2016-10-22T02:06:00-04:00"
		}
	]
}