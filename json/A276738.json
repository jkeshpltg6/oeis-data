{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A276738",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 276738,
			"data": "-1,-1,5,-1,12,-32,-1,14,7,-126,231,-1,16,16,-160,-160,1280,-1792,-1,18,18,-198,9,-396,1716,-66,2574,-12870,14586,-1,20,20,-240,20,-480,2240,-240,-240,6720,-17920,2240,-35840,129024,-122880,-1,22,22,-286,22,-572,2860,11,-572,-286,8580,-24310,-286,4290,8580,-97240,184756,715",
			"name": "Irregular triangle read by rows: T(n,m) = coefficients in a power/Fourier series expansion of an arbitrary anharmonic oscillator's exact phase space trajectory.",
			"comment": [
				"Irregular triangle read by rows (see examples). Consider an arbitrary anharmonic oscillator with Hamiltonian energy: H=(1/2)*b^2=(1/2)*(p^2+q^2) + Sum_{i=3} 2*v_i*q^i, and a stable minimum at (p,q)=(0,0). The phase space trajectory can be written in polar phase space coordinates as (q,p) = (R(x)cos(x),R(x)sin(x))=(R(Q)Q,R(Q)P). The present triangle determines a power / Fourier series of R(Q): R(Q) = b * (1 + sum b^n*T(n,m)*f(n,m) ); where the sum runs over n = 1,2,3 ... and m = 1,2,3...A000041(n). The basis functions f(n,m) are constructed from partitions of \"n\" listed in reverse lexicographic order. Partition n=(z_1+z_2+...z_j) becomes 2*Q^((z_1+2)+(z_2+2)+...(z_j+2))*v_{z_1+2}*v_{z_2+2}*...*v_{z_j+2} (see examples). This sequence transforms into A273506/A273507 by setting v_i=0 for odd i, v_i:=(-1)^(i/2-1)/2/(i!) otherwise, and (1/2)*b^2 = 2*k. For more details read \"Plane Pendulum and Beyond by Phase Space Geometry\" (Klee, 2016)."
			],
			"link": [
				"Bradley Klee, \u003ca href=\"http://arxiv.org/abs/1605.09102\"\u003ePlane Pendulum and Beyond by Phase Space Geometry\u003c/a\u003e, arXiv:1605.09102 [physics.class-ph], 2016."
			],
			"example": [
				"n/m  1    2     3     4     5     6      7",
				"--------------------------------------------",
				"1  | -1",
				"2  | -1   5",
				"3  | -1   12   -32",
				"4  | -1   14    7   -126   231",
				"5  | -1   16    16  -160  -160   1280  -1792",
				"--------------------------------------------",
				"R[1,Q] = -2*v_3*Q^3",
				"R[2,Q] = -2*v_4*Q^4 + 10*v_3^2*Q^6",
				"R[Q]   = b*(1+b*(-2*v_3*Q^3)+b^2*(-2*v_4*Q^4 + 10*v_3^2*Q^6 ))+O(b^4)",
				"Construct basis for R[4,Q]; List partitions: {{4}, {3, 1}, {2, 2}, {2, 1, 1}, {1, 1, 1, 1}}; Transform Plus 2: {{v_6}, {v_5, v_3}, {v_4, v_4}, {v_4, v_3, v_3}, {v_3, v_3, v_3, v_3}}; Multiply: {v_6, v_5*v_3, v_4^2, v_4*v_3^2, v_3^4}; don't forget power of Q and factor of 2: {2*v_6*Q^6, 2*v_5*v_3*Q^8, 2*v_4^2*Q^8, 2*v_4*v_3^2*Q^10, 2*v_3^4*Q^12}."
			],
			"mathematica": [
				"R[n_] := b Plus[1, Total[b^# R[#, q] \u0026 /@ Range[n]]]",
				"Vp[n_] := Total[2 v[# + 2] q^(# + 2) \u0026 /@ Range[n]]",
				"H[n_] := Expand[1/2*r^2 + Vp[n]]",
				"RRules[n_] :=  With[{H = Series[ReplaceAll[H[n], {q -\u003e R[n] Q, r -\u003e R[n]}], {b, 0, n + 2}]},  Function[{rules},",
				"    Nest[Rule[#[[1]], ReplaceAll[#[[2]], rules]] \u0026 /@ # \u0026, rules, n]][",
				"   Flatten[R[#, q] -\u003e Expand[-ReplaceAll[ Coefficient[H, b^(# + 2)], {R[#, q] -\u003e 0}]] \u0026 /@ Range[n]]]]",
				"basis[n_] :=  Times[Times @@ (v /@ #), Q^Total[#],2] \u0026 /@ (IntegerPartitions[n] /. x_Integer :\u003e x + 2)",
				"TriangleRow[n_, rules_] := With[{term = Expand[rules[[n, 2]]]},",
				"  Coefficient[term, #] \u0026 /@ basis[n]]",
				"With[{rules = RRules[10]}, TriangleRow[#, rules] \u0026 /@ Range[10]]"
			],
			"xref": [
				"Arbitrary Oscillator: A276814, A276815, A276816, A276817.",
				"Pendulum: A273506, A273507, A274076, A274078, A274130, A274131, A038534, A056982, A000984, A001790, A038533, A046161, A273496."
			],
			"keyword": "sign,tabf",
			"offset": "1,3",
			"author": "_Bradley Klee_, Sep 16 2016",
			"references": 5,
			"revision": 23,
			"time": "2018-01-10T20:27:02-05:00",
			"created": "2016-10-08T21:42:12-04:00"
		}
	]
}