{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A347681",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 347681,
			"data": "0,5,0,9,11,0,13,13,29,0,21,23,21,43,0,25,25,51,27,131,0,33,35,69,69,67,103,0,37,37,39,113,153,77,305,0,45,47,91,139,45,183,137,229,0,57,59,59,57,175,233,407,115,231,0,61,61,61,125,309,311,373,495,185,869,0,73,73,149,223,221,443,443,75,369,813,371,0",
			"name": "Triangle read by rows: T(n,k) (1\u003c=k\u003c=n) = f(prime(n),prime(k)), where f(x,y) = x*red_inv(x,y) + y*(red_inv(y,x) if gcd(x,y)=1, or 0 if gcd(x,y)\u003e1, and red_inv is defined in the comments.",
			"comment": [
				"If u, v are positive integers with gcd(u,v) = 1, the \"reduced inverse\" red_inv(u,v) of u mod v is u^(-1) mod v if u^(-1) mod v \u003c= v/2, otherwise it is v - u^(-1) mod v.",
				"That is, we map u to whichever of +-u has a representative mod v in the range 0 to v/2. Stated another way, red_inv(u,v) is a number r in the range 0 to v/2 such that r*u == +-1 mod v.",
				"For example, red_inv(3,11) = 4, since 3^(-1) mod 11 = 4. But red_inv(2,11) = 5 = 11-6, since red_inv(2,11) = 6.",
				"Arises in the study of A344005."
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A347681/b347681.txt\"\u003eTable of n, a(n) for n = 1..5050\u003c/a\u003e [First 100 rows, flattened]"
			],
			"example": [
				"Triangle begins:",
				"0,",
				"5, 0,",
				"9, 11, 0,",
				"13, 13, 29, 0,",
				"21, 23, 21, 43, 0,",
				"25, 25, 51, 27, 131, 0,",
				"33, 35, 69, 69, 67, 103, 0,",
				"37, 37, 39, 113, 153, 77, 305, 0,",
				"45, 47, 91, 139, 45, 183, 137, 229, 0,",
				"57, 59, 59, 57, 175, 233, 407, 115, 231, 0,",
				"..."
			],
			"maple": [
				"myfun1 := proc(A,B) local Ar,Br;",
				"if igcd(A,B) \u003e 1 then return(0); fi;",
				"  Ar:=(A)^(-1) mod B;",
				"   if 2*Ar \u003e B then Ar:=B-Ar; fi;",
				"  Br:=(B)^(-1) mod A;",
				"   if 2*Br \u003e A then Br:=A-Br; fi;",
				"A*Ar+B*Br;",
				"end;",
				"myfun2:=(i,j)-\u003emyfun1(ithprime(i),ithprime(j));",
				"for i from 1 to 20 do lprint([seq(myfun2(i,j),j=1..i)]); od:"
			],
			"xref": [
				"Cf. A344005, A347682, A347683, A347684."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Sep 18 2021",
			"references": 5,
			"revision": 19,
			"time": "2021-09-30T06:39:47-04:00",
			"created": "2021-09-18T18:19:34-04:00"
		}
	]
}