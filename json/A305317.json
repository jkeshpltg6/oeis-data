{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A305317",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 305317,
			"data": "1,1,4,6,6,8,10,8,10,12,10,14,10,14,16,14,18,12,14,16,18,20,14,22,14,16,18,20,22,24,18,22,16,26,22,26,18,28,22,26",
			"name": "a(n) gives the length of the period of the regular continued fraction of the quadratic irrational of any Markoff form representative Mf(n), n \u003e= 1 (assuming the uniqueness conjecture).",
			"comment": [
				"The index n enumerates the Markoff triples with largest member m from A002559 in increasing order. If the Markoff-Frobenius uniqueness conjecture (see, e.g. the book of Aigner) is true then the triples can be numbered by n if the largest member is m(n) = A00255(n). In the other (unlikely) case there may be more than one triple (hence forms) for some Markoff numbers m from A002559, and then one orders these triples lexicographically.",
				"The indefinite binary quadratic Markoff form Mf(n) = Mf(n;x,y) for the given Markoff number m(n) = A002559(n), n \u003e= 1, (assuming that the mentioned uniqueness conjecture is true) is m(n)*x^2 + (3*m(n) - 2*k(n))*x*y + (l(n) - 3*k(n))y^2 with l(n) = (k(n)^2 +1)/m(n), and k(n) is defined for the representative form (of the unimodualar equvivalence class), e.g., in Cassels as k(n) = k_C(n) = A305310(n). The qudadratic irrational xi(n) is the solution of  Mf(n;x,1) = 0 with the positive root. For the representative forms used by Cassels the regular continued fractions for xi(n) = xi_C(n) are not purely periodic. The smallest preperiod is -1 for n = 1 and 0 for n \u003e= 2.",
				"For the representative Mf(n) with k(n) = A305311(n) = k_C(n) + 2*m(n) one obtains purely periodic regular continued fractions for the  quadratic irrationals xi(n). They were considered by Perron, pp. 5-6, for n=1..11. See the examples below, and in the W. Lang link, Table 2."
			],
			"reference": [
				"Aigner, Martin. Markov's theorem and 100 years of the uniqueness conjecture. A mathematical journey from irrational numbers to perfect matchings. Springer, 2013.",
				"Oskar Perron, Über die Approximation irrationaler Zahlen durch rationale, II, pp. 1-12, Sitzungsber. Heidelberger Akademie der Wiss., 1921, 8. Abhandlung, Carl Winters Universitätsbuchhandlung."
			],
			"link": [
				"Wolfdieter Lang, \u003ca href=\"/A305310/a305310.pdf\"\u003eA Note on Markoff Forms Determining Quadratic Irrationals with Purely Periodic Continued Fractions\u003c/a\u003e"
			],
			"example": [
				"The periods for the representative form Mf(n) with k(n) = A305311(n) are given for n=1..40 in the W. Lang link in Table 2.",
				"The first 11 examples (given by Perron) are:",
				"n     periods             length  quadratic irrationals xi  Markoff form coeffs.",
				"1:    (1)                    1    (1 + sqrt(5)/2            [1, -1, -1]",
				"2:    (2)                    1     1 + sqrt(2)              [2, -4 ,-2]",
				"3:    (2_2, 1_2)             4    (9 + sqrt(221))/10        [5, -9, -7]",
				"4:    (2_2, 1_4)             6    (23 + sqrt(1517))/26      [13, -23,-19]",
				"5:    (2_4, 1_2)             6    (53 + sqrt(7565))/58      [29, -53, -4]",
				"6:    (2_2, 1_6)             8    (15 + 5*sqrt(26))/17      [34, -60, -50]",
				"7:    (2_2, 1_8)            10    (157 + sqrt(71285))/178   [89, -157, -131]",
				"8:    (2_6, 1_2)             8    (309 + sqrt(257045)/338   [169, -309, -239]",
				"9:    (2_2, 1_2, 2_2, 1_4)  10    (86 + sqrt(21170))/ 97    [194, -344, -284]",
				"10:   (2_2, 1_10)           12    (411 + sqrt(488597))/466  [233, -411, -343]",
				"11:   (2_4, 1_2, 2_2, 1_2)  10    (791 + sqrt(1687397))/866 [433, -791, -613]",
				"..."
			],
			"xref": [
				"Cf. A002559, A305310, A305311."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Wolfdieter Lang_, Jul 30 2018",
			"references": 0,
			"revision": 9,
			"time": "2019-01-19T04:15:43-05:00",
			"created": "2018-07-31T10:04:34-04:00"
		}
	]
}