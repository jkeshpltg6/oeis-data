{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A335418",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 335418,
			"data": "19,31,41,59,71,101,109,79,149,121,139,211,209,271,191,229,179,269,341,361,241,251,419,341,311,449,319,439,451,551,389,319,421,599,541,401,659,701,509,649,569,491,781,479,631,811,671,589,499,761,929,571,859,739",
			"name": "Largest odd side of primitive triples for integer-sided triangles that have two perpendicular medians, the triples being ordered by increasing perimeter.",
			"comment": [
				"If medians at A and B are perpendicular at the centroid G, then a^2 + b^2 = 5 * c^2 (see picture in Maths Challenge link).",
				"For the corresponding primitive triples and miscellaneous properties, see A335034.",
				"In each increasing triple (c,a,b), c is always the smallest odd side (A335036), but the largest odd side b' can be either the middle side a (A335347) or the largest side b (A335348) (see formulas and examples for explanations).",
				"The largest odd side b' is not divisible by 3 or 5, and the odd prime factors of this odd side b' are all of the form 10*k +- 1.",
				"The repetitions for 319, 341 ... correspond to largest odd sides for triangles with distinct perimeters (see examples).",
				"This sequence is not increasing: a(7) = 109 for triangle with perimeter = 252 and a(8) = 79 for triangle with perimeter = 266; hence the largest odd side is not an increasing function of the perimeter of these triangles."
			],
			"link": [
				"Annales Concours Général, \u003ca href=\"https://www.freemaths.fr/annales-composition-mathematiques-concours-general/concours-general-mathematiques-2007-sujet.pdf\"\u003eSujet Concours Général 2007\u003c/a\u003e.",
				"Maths Challenge, \u003ca href=\"https://mathschallenge.net/view/perpendicular_medians\"\u003ePerpendicular medians\u003c/a\u003e, Problem with picture.",
				"\u003ca href=\"/index/O#Olympiads\"\u003eIndex to sequences related to Olympiads and other Mathematical competitions\u003c/a\u003e."
			],
			"formula": [
				"There exist two disjoint classes of such triangles, obtained with two distinct families of formulas: let u \u003e v \u003e 0, u and v with different parities, gcd(u,v) = 1; a' is the even side and b' the largest odd side.",
				"--\u003e 1st class of triangles: (a',b',c) = (2*(u^2-uv-v^2), u^2+4*u*v-v^2, u^2+v^2) with u/v \u003e 3 and 5 doesn't divide u-3v.",
				"If 3 \u003c u/v \u003c 3+sqrt(10) then a' \u003c b' (largest odd) and the triple in increasing order is (c, a = a', b = b'),",
				"if u/v \u003e 3+sqrt(10) then a' \u003e b' (largest odd) and the triple in increasing order is (c, a = b', b = a').",
				"--\u003e 2nd class of triangles: (a',b',c) = (2*(u^2+uv-v^2), -u^2+4*u*v+v^2, u^2+v^2) with 1 \u003c u/v \u003c 2 and 5 doesn't divide u-2v.",
				"If 1 \u003c u/v \u003c (1+sqrt(10))/3 then a' \u003c b' (largest odd) and the triple in increasing order is (c, a = a', b=b').",
				"If (1+sqrt(10))/3 \u003c u/v \u003c 2 then a' \u003e b' (largest odd) and the triple in increasing order is (c, a = b', b = a')."
			],
			"example": [
				"The triples (257, 319, 478) and (289, 319, 562) correspond to triangles with respective perimeters equal to 1054 and 1170, so a(27) = a(32) = 319.",
				"-\u003e For 1st class of triangles, u/v \u003e 3:",
				"(u,v) = (4,1), then 3 \u003c u/v \u003c 3+sqrt(10) and (c,a,b) = (c, a', b') = (17,22,31); the relation is 22^2 + 31^2 = 5 * 17^2 = 1445 with a(2) = 31 = b' = b.",
				"(u,v) = (10,1), then u/v \u003e 3+sqrt(10) and (c,a,b) = (c, b' ,a') = (101, 139, 178), the relation is 139^2 + 178^2 = 5 * 101^2 = 51005 with a(11) = 139 = b' = a.",
				"-\u003e For 2nd class, 1 \u003c u/v \u003c 2:",
				"(u,v) = (3,2), then (1+sqrt(10))/3 \u003c u/v \u003c 2 and (c,a,b) = (c, b', a') = (13,19,22), the relation is 19^2 + 22^2 = 5 * 13^2 = 845 with a(1) = 19 = b' = a.",
				"(u,v) = (4,3), then 1 \u003c u/v \u003c (1+sqrt(10))/3 and (c,a,b) = (c, a', b') = (25,38,41); the relation is 38^2 + 41^2 = 5 * 25^2 = 3125 with a(3) = 41 = b' = b."
			],
			"program": [
				"(PARI) mycmp(x, y) = {my(xp = vecsum(x), yp = vecsum(y)); if (xp!=yp, return (xp-yp)); return (x[1] - y[1]); }",
				"triples(nn) = {my(vm = List(), vt); for (u=1, nn, for (v=1, nn, if (gcd(u, v) == 1, vt = 0; if ((u/v \u003e 3) \u0026\u0026 ((u-3*v) % 5), vt = [2*(u^2-u*v-v^2), u^2+4*u*v-v^2, u^2+v^2]); if ((u/v \u003e 1) \u0026\u0026 (u/v \u003c 2) \u0026\u0026 ((u-2*v) % 5), vt = [2*(u^2+u*v-v^2), -u^2+4*u*v+v^2, u^2+v^2]); if (gcd(vt) == 1, listput(vm, vt)); ); ); ); vecsort(apply(vecsort, Vec(vm)), mycmp); } \\\\ A335034",
				"lista(nn) = my(w=triples(nn)); vector(#w, k, vecmax(select(x-\u003e(x%2), w[k]))); \\\\ _Michel Marcus_, Jun 11 2020"
			],
			"xref": [
				"Cf. A335034 (primitive triples), A335035 (corresponding perimeters), A335036 (smallest side), A335347 (middle side), A335348 (largest side), A335273 (even side), this sequence (largest odd side)."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Bernard Schott_, Jun 11 2020",
			"ext": [
				"More terms from _Michel Marcus_, Jun 11 2020"
			],
			"references": 1,
			"revision": 20,
			"time": "2020-10-01T06:00:12-04:00",
			"created": "2020-06-26T01:53:16-04:00"
		}
	]
}