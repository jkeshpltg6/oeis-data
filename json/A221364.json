{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A221364",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 221364,
			"data": "1,1,1,5,1,16,1,45,1,121,1,320,1,841,1,2205,1,5776,1,15125,1,39601,1,103680,1,271441,1,710645,1,1860496,1,4870845,1,12752041,1,33385280,1,87403801,1,228826125,1",
			"name": "The simple continued fraction expansion of F(x) := product {n = 0..inf} (1 - x^(4*n+3))/(1 - x^(4*n+1)) when x = 1/2*(3 - sqrt(5)).",
			"comment": [
				"The function F(x) := product {n = 0..inf} (1 - x^(4*n+3))/(1 - x^(4*n+1)) is analytic for |x| \u003c 1. When x is a quadratic irrational of the form x = 1/2*(N - sqrt(N^2 - 4)), N an integer greater than 2, the real number F(x) has a predictable simple continued fraction expansion. The first examples of these expansions, for N = 2, 4, 6 and 8, are due to Hanna. See A174500 through A175503. The present sequence is the case N = 3. See also A221365 (N = 5), A221366 (N = 7), A221369 (N = 9).",
				"If we denote the present sequence by [1, c(1), 1, c(2), 1, c(3), ...] then for k = 1, 2, ..., the simple continued fraction expansion of F({1/2*(3 - sqrt(5)}^k) is given by the sequence [1; c(k), 1, c(2*k), 1, c(3*k), 1, ...]. Examples are given below."
			],
			"link": [
				"P. Bala, \u003ca href=\"/A174500/a174500_2.pdf\"\u003eSome simple continued fraction expansions for an infinite product, Part 1\u003c/a\u003e",
				"\u003ca href=\"/index/Rec\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,4,0,-4,0,1)."
			],
			"formula": [
				"a(2*n-1) = (1/2*(3 + sqrt(5)))^n + (1/2*(3 - sqrt(5)))^n - 2 = A004146(n); a(2*n) = 1.",
				"a(4n+1) = A081071(n) = A002878(n)^2;",
				"a(4*n-1) = A081070(n) = 5*A049684(n) = 5*(A001906(n))^2.",
				"a(n) = 4*a(n-2)-4*a(n-4)+a(n-6). G.f.: -(x^4+x^3-3*x^2+x+1) / ((x-1)*(x+1)*(x^2-x-1)*(x^2+x-1)). [_Colin Barker_, Jan 20 2013]"
			],
			"example": [
				"F(1/2*(3 - sqrt(5)) = 1.53879 34992 88095 08323 ... = 1 + 1/(1 + 1/(1 + 1/(5 + 1/(1 + 1/(16 + 1/(1 + 1/(45 + ...))))))).",
				"F({1/2*(3 - sqrt(5)}^2) = 1.16725 98258 10214 95210 ... = 1 + 1/(5 + 1/(1 + 1/(45 + 1/(1 + 1/(320 + 1/(1 + 1/(2205 + ...))))))).",
				"F({1/2*(3 - sqrt(5)}^3) = 1.05883 42773 67371 19975 ... = 1 + 1/(16 + 1/(1 + 1/(320 + 1/(1 + 1/(5776 + 1/(1 + 1/(103680 + ...)))))))."
			],
			"xref": [
				"A001906, A002878, A004146, A049684, A081070, A081071, A174500 (N = 4), A221365 (N = 5), A221366 (N = 7), A221369 (N = 9)."
			],
			"keyword": "nonn,easy,cofr",
			"offset": "0,4",
			"author": "_Peter Bala_, Jan 15 2013",
			"references": 3,
			"revision": 13,
			"time": "2015-06-13T00:54:37-04:00",
			"created": "2013-01-16T09:38:29-05:00"
		}
	]
}