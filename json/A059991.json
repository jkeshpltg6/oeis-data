{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A059991",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 59991,
			"data": "1,1,4,1,16,16,64,1,256,256,1024,256,4096,4096,16384,1,65536,65536,262144,65536,1048576,1048576,4194304,65536,16777216,16777216,67108864,16777216,268435456,268435456,1073741824,1,4294967296,4294967296",
			"name": "a(n) = 2^(n-2^ord_2(n)) (or 2^(n-A006519(n))).",
			"comment": [
				"Number of points of period n in the simplest nontrivial disconnected S-integer dynamical system.",
				"This sequence comes from the simplest disconnected S-integer system that is not hyperbolic. In the terminology of the papers referred to, it is constructed by choosing the under- lying field to be F_2(t), the element to be t and the nontrivial valuation to correspond to the polynomial 1+t. Since it counts periodic points, it satisfies the nontrivial congruence sum_{d|n}mu(d)a(n/d) = 0 mod n for all n and since it comes from a group automorphism it is a divisibility sequence."
			],
			"link": [
				"R. Brown and J. L. Merzel, \u003ca href=\"http://www.fq.math.ca/Papers1/45-2/brown.pdf\"\u003eThe number of Ducci sequences with a given period\u003c/a\u003e, Fib. Quart., 45 (2007), 115-121.",
				"Vijay Chothi, \u003ca href=\"http://www.mth.uea.ac.uk/admissions/graduate/phds.html\"\u003ePeriodic Points in S-integer dynamical systems\u003c/a\u003e, PhD thesis, University of East Anglia, 1996. [Broken link]",
				"Vijay Chothi, Graham Everest, Thomas Ward, \u003ca href=\"http://gdz.sub.uni-goettingen.de/dms/load/img/?PID=GDZPPN00221492X\"\u003eS-integer dynamical systems: periodic points\u003c/a\u003e, J. Reine Angew Math. 489 (1997), 99-132.",
				"Y. Puri and T. Ward, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL4/WARD/short.html\"\u003eArithmetic and growth of periodic orbits\u003c/a\u003e, J. Integer Seqs., Vol. 4 (2001), #01.2.1.",
				"T. Ward, \u003ca href=\"http://nyjm.albany.edu/Conf/Ergodic/Ward.ps\"\u003eAlmost all S-integer dynamical systems have many periodic points\u003c/a\u003e, Ergodic Th. Dynam. Sys. 18 (1998), 471-486.",
				"\u003ca href=\"/index/Di#divseq\"\u003eIndex to divisibility sequences\u003c/a\u003e"
			],
			"example": [
				"a(24) = 2^16 = 65536 because ord_2(24)=3, so 24-2^ord_2(24)=16."
			],
			"maple": [
				"readlib(ifactors): for n from 1 to 100 do if n mod 2 = 1 then ord2 := 0 else ord2 := ifactors(n)[2][1][2] fi: printf(`%d,`, 2^(n-2^ord2)) od:"
			],
			"mathematica": [
				"ord2[n_?OddQ] = 0; ord2[n_?EvenQ] := FactorInteger[n][[1, 2]]; a[n_] := 2^(n-2^ord2[n]); a /@ Range[34]",
				"(* _Jean-François Alcover_, May 19 2011, after Maple prog. *)"
			],
			"xref": [
				"Cf. A000079, A006519, A129760."
			],
			"keyword": "easy,nonn",
			"offset": "1,3",
			"author": "_Thomas Ward_, Mar 08 2001",
			"ext": [
				"More terms from _James A. Sellers_, Mar 15 2001"
			],
			"references": 3,
			"revision": 19,
			"time": "2021-02-19T20:10:00-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}