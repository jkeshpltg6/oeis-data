{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A123948",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 123948,
			"data": "1,1,-1,-1,1,1,-2,3,3,-1,9,-15,-22,7,1,96,-184,-314,139,19,-1,-2500,5250,10575,-5375,-1026,51,1,-162000,369900,842310,-498171,-111179,7644,141,-1,26471025,-64790985,-164634169,109325076,28870212,-2322404,-59193,393,1",
			"name": "Triangle read by rows: first row is 1, and n-th row (n \u003e 0) gives the coefficients in the expansion of the characteristic polynomial of the (n - 1)-th Bernstein basis matrix, horizontal flipped.",
			"comment": [
				"The Bernstein basis matrix of order n - 1 is an n X n matrix whose m-th row represents the coefficients in the expansion of the Bernstein basis polynomial defined as binomial(n, m)*x^m*(1 - x)^(n - m), 0 \u003c= m \u003c= n - 1. For n = 0, we obtain the 0 X 0 matrix. The convention is that the characteristic polynomial of the empty matrix is identically 1 (see [de Boor] and [Johnson et al.]). Row n of the present sequence is obtained by taking the characteristic polynomial of the matrix represented by the polynomials binomial(n, m)*x^(n - m)*(1 - x)^m. The resulting matrix is, in fact, the horizontal flipped version of the Bernstein basis matrix of order n (see example). - _Franck Maminirina Ramaharo_, Oct 19 2018"
			],
			"reference": [
				"Gengzhe Chang and Thomas W. Sederberg, Over and Over Again, The Mathematical Association of America, 1997, Chap. 30."
			],
			"link": [
				"Carl de Boor, \u003ca href=\"ftp://ftp.cs.wisc.edu/Approx/empty.pdf\"\u003eAn empty exercise\u003c/a\u003e",
				"John Burkardt, \u003ca href=\"https://people.sc.fsu.edu/~jburkardt/m_src/bernstein_polynomial/bernstein_polynomial.html\"\u003eBERNSTEIN_POLYNOMIAL - The Bernstein Polynomials\u003c/a\u003e",
				"Charles R. Johnson and Carlos M. Saiago, \u003ca href=\"https://doi.org/10.1017/9781316155158\"\u003eEigenvalues, Multiplicities and Graphs\u003c/a\u003e, Cambridge University Press, 2018, p. 8.",
				"Kenneth I. Joy, \u003ca href=\"http://graphics.cs.ucdavis.edu/education/CAGDNotes/Bernstein-Polynomials/Bernstein-Polynomials.html\"\u003eOn-Line Geometric Modeling Notes\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Bernstein_polynomial\"\u003eBernstein polynomial\u003c/a\u003e"
			],
			"example": [
				"Triangle begins:",
				"        1;",
				"        1,     -1;",
				"       -1,      1,      1;",
				"       -2,      3,      3,      -1;",
				"        9,    -15,    -22,       7,       1;",
				"       96,   -184,   -314,     139,      19,   -1;",
				"    -2500,   5250,  10575,   -5375,   -1026,   51,   1;",
				"  -162000, 369900, 842310, -498171, -111179, 7644, 141, -1;",
				"      ...",
				"From _Franck Maminirina Ramaharo_, Oct 19 2018: (Start)",
				"Let n = 6 (i.e., order 5). The corresponding Bernstein basis matrix has the form",
				"   1, -5,  10, -10,   5,  -1",
				"   0,  5, -20,  30, -20,   5",
				"   0,  0,  10, -30,  30, -10",
				"   0,  0,   0,  10, -20,  10",
				"   0,  0,   0,   0,   5,  -5",
				"   0,  0,   0,   0,   0,   1.",
				"Flipping this matrix horizontally gives the matrix for the polynomials binomial(5, m)*x^(5 - m)*(1 - x)^m, 0 \u003c= m \u003c= 5,",
				"   0,  0,   0,   0,   0,   1",
				"   0,  0,   0,   0,   5,  -5",
				"   0,  0,   0,  10, -20,  10",
				"   0,  0,  10, -30,  30, -10",
				"   0,  5, -20,  30, -20,   5",
				"   1, -5,  10, -10,   5,  -1",
				"whose characteristic polynomial is -2500 + 5250*x + 10575*x^2 - 5375*x^3 - 1026*x^4 + 51*x^5 + x^6. (End)"
			],
			"mathematica": [
				"M[n_] := Table[CoefficientList[Binomial[n - 1, n - i - 1]*(1 - x)^i*x^(n - i - 1), x], {i, 0, n - 1}];",
				"Join[{1}, Table[CoefficientList[CharacteristicPolynomial[M[d], x], x], {d, 1, 10}]]//Flatten"
			],
			"xref": [
				"Cf. A045912, A136678."
			],
			"keyword": "tabl,sign",
			"offset": "0,7",
			"author": "_Roger L. Bagula_ and _Gary W. Adamson_, Oct 26 2006",
			"ext": [
				"Edited, new name, offset corrected by _Franck Maminirina Ramaharo_, Oct 19 2018"
			],
			"references": 0,
			"revision": 36,
			"time": "2019-05-26T04:40:45-04:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}