{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002653",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2653,
			"data": "1,6,24,56,114,168,280,294,444,390,840,636,1176,1176,1512,1008,1782,2016,1896,2520,3528,2408,3216,2796,4760,3174,5880,4592,6258,4380,5040,6720,7200,6832,10080,7224,8082,7164,12600,7056,14280,11760,12040,9756",
			"name": "Expansion of (theta_3(z)*theta_3(7z)+theta_2(z)*theta_2(7z))^3.",
			"comment": [
				"Theta series of Kleinian lattice (Z[ (-1+sqrt(-7))/2 ])^3 in 3 complex (or 6 real) dimensions."
			],
			"reference": [
				"N. Elkies, The Klein quartic in number theory, pp. 51-101 of S. Levy, ed., The Eightfold Way, Cambridge Univ. Press, 1999. MR1722413 (2001a:11103)",
				"Srinivasa Ramanujan, The Lost Notebook and Other Unpublished Papers, Narosa Publishing House, New Delhi, 1988, p. 53."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A002653/b002653.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"H. H. Chan and Y. L. Ong, \u003ca href=\"https://doi.org/10.1090/S0002-9939-99-04832-7\"\u003eOn Eisenstein series and Sum_{m,n} q^(m^2+mn+2n^2)\u003c/a\u003e, Proc. Amer. Math. Soc. 127 (1999), no. 6, 1735-1744, See page 1737. MR1600120 (99i:11029).",
				"N. Elkies, \u003ca href=\"http://www.msri.org/publications/books/Book35/files/elkies.pdf\"\u003eThe Klein quartic in number theory\u003c/a\u003e"
			],
			"formula": [
				"G.f.: (theta_3(z)*theta_3(7*z) + theta_2(z)*theta_2(7*z))^3.",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2), A(x^4)) where f(u, v, w) is a homogeneous degree 6 polynomial with 28 terms. - _Michael Somos_, Jun 03 2005",
				"Expansion of (eta(q)^8 + 13 * eta(q)^4 * eta(q^7)^4 + 49 * eta(q^7)^8) / ( eta(q) * eta(q^7) ) in power of q. - _Michael Somos_, Mar 11 2008",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (7*t)) = 7^(3/2)*(t/i)^3*f(t) where q = exp(2*Pi*i*t)."
			],
			"example": [
				"G.f. = 1 + 6*q + 24*q^2 + 56*q^3 + 114*q^4 + 168*q^5 + 280*q^6 + 294*q^7 + ..."
			],
			"maple": [
				"g:= (JacobiTheta3(0,z) * JacobiTheta3(0,z^7) + JacobiTheta2(0,z) * JacobiTheta2(0,z^7))^3:",
				"S:= series(g,z,101):",
				"seq(coeff(S,z,i),i=0..100); # _Robert Israel_, Aug 12 2020"
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (EllipticTheta[ 3, 0, q] EllipticTheta[ 3, 0, q^7] + EllipticTheta[ 2, 0, q] EllipticTheta[ 2, 0, q^7])^3, {q, 0, n}]; (* _Michael Somos_, Nov 09 2014 *)"
			],
			"program": [
				"(PARI) {a(n) = local(A, t2, t3); if( n\u003c1, n==0, A = x * O(x^n); t2 = sum(k=1, (sqrtint(4*n + 1) + 1)\\2, 2*x^(k*k - k), A); t3 = sum(k=1, sqrtint(n), 2*x^(k*k), 1 + A); A = x * O(x^(n\\7)); polcoeff( (t3 * subst(t3 + A, x, x^7) + x^2 * t2 * subst(t2 + A, x, x^7))^3, n))}; /* _Michael Somos_, Jun 03 2005 */",
				"(PARI) {a(n) = local(A, t1, t7); if( n\u003c0, 0, A = x * O(x^n); t1 = eta(x + A)^4; t7 = eta(x^7 + A)^4; polcoeff( (t1^2 + 13 * x * t1 * t7 + 49 * x^2 * t7^2) / (t1 * t7)^(1/4), n))}; /* _Michael Somos_, Mar 11 2008 */",
				"(Sage) A = ModularForms( Gamma1(7), 3, prec=60) . basis(); (3*A[0] + 4*A[1] + 21*A[2] + 105*A[3] + 224*A[4] + 441*A[5] + 672*A[6])/4 # _Michael Somos, May 25 2014",
				"(MAGMA) A := Basis( ModularForms( Gamma1(7), 3), 44); A[1] + 6*A[2] + 24*A[3] + 56*A[4] +114*A[5] + 168*A[6] + 280*A[7];  /* _Michael Somos_, Nov 09 2014 */"
			],
			"xref": [
				"Cf. A002652."
			],
			"keyword": "nonn,look",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 4,
			"revision": 29,
			"time": "2020-08-13T02:05:44-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}