{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A154411",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 154411,
			"data": "8,56,996,1680,5236,280,28,224,448,749196,728,980,420,112,1708,56,112,56,308,252,532,84,2156,28,84,280,29260,140,6454336,3220,868,6916,532,168,28,28,28,56,1568,28,56,376236,28,2380,112,56,28,3640,84,84,84,168,224,56,4732,56,84,28,364,8232",
			"name": "a(n) = period of the sequence [ x(k+1) := floor(2*cos(2*Pi/7)*x(k)) - x(k-1), with x(0) := cosh(n)^2, x(1) := sinh(n)^2 ].",
			"comment": [
				"The Pi*(rational) suppresses the natural tendency to grow like x0^2 + y0^2.",
				"All terms of the sequence x considered for a(n) are of the form k +- sinh(n)^2, where k is an integer; in particular, x(0) = x(1) + 1. Are all terms except for a(0) and a(2) a multiple of 28? - _M. F. Hasler_, Apr 12 2021"
			],
			"link": [
				"M. F. Hasler, \u003ca href=\"/A154411/b154411.txt\"\u003eTable of n, a(n) for n = 0..130\u003c/a\u003e"
			],
			"example": [
				"From Neil Bickford, Apr 12 2021: (Start)",
				"This prints the cycle for a(23)=28:",
				"Block[{x0=Cosh[23]^2,x1=Sinh[23]^2,xPrev,x,k=1},(",
				"  xPrev=x0;",
				"  x=x1;",
				"  Do[",
				"    (Print[{k,xPrev,x}];",
				"    {xPrev,x}={x,Floor[2 Cos[2 Pi/7]x]-xPrev};",
				"    k++),",
				"  29]",
				")]",
				"This produces:",
				"{1,Cosh[23]^2,Sinh[23]^2}",
				"{2,Sinh[23]^2,29603668079891461525-Cosh[23]^2}",
				"{3,29603668079891461525-Cosh[23]^2,7311502210955022182-Sinh[23]^2}",
				"{4,7311502210955022182-Sinh[23]^2,-50090042030186845236+Cosh[23]^2}",
				"(...several terms omitted...)",
				"{26,-50090042030186845236+Sinh[23]^2,7311502210955022184-Cosh[23]^2}",
				"{27,7311502210955022184-Cosh[23]^2,29603668079891461526-Sinh[23]^2}",
				"{28,29603668079891461526-Sinh[23]^2,Cosh[23]^2}",
				"{29,Cosh[23]^2,Sinh[23]^2}",
				"(End)"
			],
			"mathematica": [
				"(* Mma code from Neil Bickford, Apr 12 2021 *)",
				"func[n_] :=Block[{x0=Cosh[n]^2,x1=Sinh[n]^2,xPrev,x,k=1},(",
				"  xPrev=x0;",
				"  x=x1;",
				"  While[True,",
				"    ({xPrev,x}={x,Floor[2 Cos[2 Pi/7]x]-xPrev};",
				"    If[x0==xPrev \u0026\u0026 x1==x, Return[k]];",
				"    k++;",
				"  )]",
				")]",
				"In[20]:= Table[func[n],{n,0,10}]",
				"Out[20]= {8,56,996,1680,5236,280,28,224,448,749196,728}"
			],
			"program": [
				"(PARI) A154411(n)={my(a1='s, a0='s+1, s=sinh(n)^2, c=1/2/cos(2*Pi/7), L=Map([a0,[0]; a1,[1]]), t, i); for(n=2,oo, [a0, a1]=[a1, eval(a1)\\c-a0]; if( !mapisdefined(L, a1), mapput(L, a1, [n]), i=setintersect(t=mapget(L, a1), [j+1| j\u003c-mapget(L,a0)]), return(n-i[1]), mapput(L, a1, concat(t,n))))} \\\\ Precision of 115 digits is enough for n \u003c= 130, not beyond. - _M. F. Hasler_, Apr 12 2021"
			],
			"keyword": "nonn,more",
			"offset": "0,1",
			"author": "_Bill Gosper_, Jun 24 2010",
			"ext": [
				"Definition corrected by _N. J. A. Sloane_, Apr 12 2021",
				"More terms from _M. F. Hasler_, Apr 12 2021"
			],
			"references": 1,
			"revision": 29,
			"time": "2021-04-22T21:55:55-04:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}