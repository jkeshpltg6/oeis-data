{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A078437",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 78437,
			"data": "2,6,9,6,0,6,3,5,1,9,7,1,6,7",
			"name": "Decimal expansion of sum of alternating series of reciprocals of primes.",
			"comment": [
				"Verified and extended by _Chris K. Caldwell_ and _Jud McCranie_.",
				"Next two terms are most likely 4 and 5. - _Robert Price_, Sep 13 2011",
				"From _Jon E. Schoenfield_, Nov 25 2018: (Start)",
				"Let f(k) be the k-th partial sum of the alternating series, i.e., f(k) = Sum_{j=1..k} ((-1)^(j+1))/prime(j). At large values of k, successive first differences f(k) - f(k-1) = ((-1)^(k+1))/prime(k) are alternatingly positive and negative and are nearly the same in absolute value, so f(k) is alternatingly above (for odd k) or below (for even k) the value of the much smoother function g(k) = (f(k-1) + f(k))/2 (a two-point moving average of the function f()).",
				"Additionally, since the first differences f(k) - f(k-1) are decreasing in absolute value, g(k) will be less than both g(k-1) and g(k+1) for odd k, and greater than both for even k; i.e., g(), although much smoother than f(), is also alternatingly below or above the value of the still smoother function h(k) = (g(k-1) + g(k))/2 = ((f(k-2) + f(k-1))/2 + (f(k-1) + f(k))/2)/2 = (f(k-2) + 2*f(k-1) + f(k))/4. Evaluated at k = 2^m for m = 1, 2, 3, ..., the values of h(k) converge fairly quickly toward the limit of the alternating series:",
				"                         h(k) =",
				"           k  (f(k-2) + 2*f(k-1) + f(k))/4",
				"  ==========  ============================",
				"           2     0.29166666666666666...",
				"           4     0.28095238095238095...",
				"           8     0.26875529011751921...",
				"          16     0.27058892362329746...",
				"          32     0.27009944617052797...",
				"          64     0.26963971020080367...",
				"         128     0.26959147218377685...",
				"         256     0.26959653902072193...",
				"         512     0.26960402179695026...",
				"        1024     0.26960568606633210...",
				"        2048     0.26960649673621509...",
				"        4096     0.26960645080540929...",
				"        8192     0.26960627432070023...",
				"       16384     0.26960633643086948...",
				"       32768     0.26960634835658329...",
				"       65536     0.26960635083481533...",
				"      131072     0.26960635144743392...",
				"      262144     0.26960635199009778...",
				"      524288     0.26960635199971603...",
				"     1048576     0.26960635195886861...",
				"     2097152     0.26960635197214933...",
				"     4194304     0.26960635197019215...",
				"     8388608     0.26960635197186919...",
				"    16777216     0.26960635197171149...",
				"    33554432     0.26960635197146884...",
				"    67108864     0.26960635197167534...",
				"   134217728     0.26960635197167145...",
				"   268435456     0.26960635197166927...",
				"   536870912     0.26960635197167200...",
				"  1073741824     0.26960635197167416...",
				"  2147483648     0.26960635197167454...",
				"  4294967296     0.26960635197167462... (End)"
			],
			"reference": [
				"S. R. Finch, Mathematical Constants, Encyclopedia of Mathematics and its Applications, vol. 94, Cambridge University Press, pp. 94-98."
			],
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PrimeSums.html\"\u003ePrime Sums\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PrimeZetaFunction.html\"\u003ePrime Zeta Function\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Prime_zeta_function\"\u003ePrime Zeta Function\u003c/a\u003e"
			],
			"example": [
				"1/2 - 1/3 + 1/5 - 1/7 + 1/11 - 1/13 + ... = 0.26960635197167..."
			],
			"mathematica": [
				"s = NSum[ p=Prime[k//Round]; (-1)^k/p, {k, 1, Infinity}, WorkingPrecision -\u003e 30, NSumTerms -\u003e 5*10^7, Method -\u003e \"AlternatingSigns\"]; RealDigits[s, 10, 14] // First (* _Jean-François Alcover_, Sep 02 2015 *)"
			],
			"xref": [
				"Cf. A242301, A242302, A242303, A242304."
			],
			"keyword": "cons,hard,more,nonn",
			"offset": "0,1",
			"author": "_G. L. Honaker, Jr._, Dec 31 2002",
			"ext": [
				"Values of a(11)-a(14) = 7,1,6,7 from _Robert Price_, Sep 13 2011"
			],
			"references": 13,
			"revision": 48,
			"time": "2018-11-25T07:44:29-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}