{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A076512",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 76512,
			"data": "1,1,2,1,4,1,6,1,2,2,10,1,12,3,8,1,16,1,18,2,4,5,22,1,4,6,2,3,28,4,30,1,20,8,24,1,36,9,8,2,40,2,42,5,8,11,46,1,6,2,32,6,52,1,8,3,12,14,58,4,60,15,4,1,48,10,66,8,44,12,70,1,72,18,8,9,60,4,78,2,2,20,82,2,64,21",
			"name": "Denominator of cototient(n)/totient(n).",
			"comment": [
				"a(n)=1 iff n=A007694(k) for some k.",
				"Numerator of phi(n)/n=Prod_{p|n} (1-1/p). - _Franz Vrabec_, Aug 26 2005",
				"From _Wolfdieter Lang_, May 12 2011: (Start)",
				"For n\u003e=2, a(n)/A109395(n) = sum(((-1)^r)*sigma_r,r=0..M(n)) with the elementary symmetric functions (polynomials) sigma_r of the indeterminates {1/p_1,...,1/p_M(n)} if n = prod((p_j)^e(j),j=1..M(n)) where M(n)=A001221(n) and sigma_0=1.",
				"This follows by expanding the above given product for phi(n)/n.",
				"The n-th member of this rational sequence 1/2, 2/3, 1/2, 4/5, 1/3, 6/7, 1/2, 2/3, 2/5,... is also (2/n^2)*sum(k,with 1\u003c=k\u003cn and gcd(k,n)=1), n\u003e=2.",
				"Therefore, this scaled sum depends only on the distinct prime factors of n.",
				"See also A023896. Proof via PIE (principle of inclusion and exclusion). (End)",
				"In the sequence of rationals r(n)=eulerphi(n)/n: 1, 1/2, 2/3, 1/2, 4/5, 1/3, 6/7, 1/2, 2/3, 2/5, 10/11, 1/3, ... one can observe that new values are obtained for squarefree indices (A005117); while for a nonsquarefree number n (A013929), r(n) = r(A007947(n)), where A007947(n) is the squarefree kernel of n. - _Michel Marcus_, Jul 04 2015"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A076512/b076512.txt\"\u003eTable of n, a(n) for n=1..1000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A000010(n)/A009195(n)."
			],
			"mathematica": [
				"Table[Denominator[(n - EulerPhi[n])/EulerPhi[n]], {n, 80}] (* _Alonso del Arte_, May 12 2011 *)"
			],
			"program": [
				"(PARI) vector(80, n, numerator(eulerphi(n)/n)) \\\\ _Michel Marcus_, Jul 04 2015",
				"(MAGMA) [Numerator(EulerPhi(n)/n): n in [1..100]]; // _Vincenzo Librandi_, Jul 04 2015"
			],
			"xref": [
				"Cf. A076511 (numerator of cototient(n)/totient(n)), A051953.",
				"Phi(m)/m = k: A000079 \\ {1} (k=1/2), A033845 (k=1/3), A000244 \\ {1} (k=2/3), A033846 (k=2/5), A000351 \\ {1} (k=4/5), A033847 (k=3/7), A033850 (k=4/7), A000420 \\ {1} (k=6/7), A033848 (k=5/11), A001020 \\ {1} (k=10/11), A288162 (k=6/13), A001022 \\ {1} (12/13), A143207 (k=4/15), A033849 (k=8/15), A033851 (k=24/35)."
			],
			"keyword": "nonn,frac",
			"offset": "1,3",
			"author": "_Reinhard Zumkeller_, Oct 15 2002",
			"references": 17,
			"revision": 41,
			"time": "2020-11-22T12:18:40-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}