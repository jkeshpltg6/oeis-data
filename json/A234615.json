{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A234615",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 234615,
			"data": "0,0,0,0,1,1,0,2,2,2,4,3,5,4,2,6,6,6,5,4,5,6,4,6,5,5,2,4,5,6,5,7,4,6,6,8,3,3,6,7,7,4,4,4,4,7,7,3,3,4,4,6,5,4,5,5,7,1,3,4,7,5,5,6,3,7,11,5,4,5,4,7,6,4,2,7,9,7,5,5,6,5,10,7,4,3,4,6,3,4,9,5,3,5,6,5,3,6,2,7",
			"name": "Number of ways to write n = k + m with k \u003e 0 and m \u003e 0 such that p = prime(k) + phi(m) and q(p) - 1 are both prime, where phi(.) is Euler's totient function and q(.) is the strict partition function (A000009).",
			"comment": [
				"Conjecture: (i) a(n) \u003e 0 for all n \u003e 7.",
				"(ii) Any integer n \u003e 7 not equal to 15 can be written as k + m with k \u003e 0 and m \u003e 0 such that p = prime(k) + phi(m) and q(p) + 1 are both prime.",
				"(iii) Any integer n \u003e 83 can be written as k + m with k \u003e 0 and m \u003e 0 such that prime(k) + phi(m)/2 is a square. Also, each integer n \u003e 45 can be written as k + m with k \u003e 0 and m \u003e 0 such that prime(k) + phi(m)/2 is a triangular number.",
				"Clearly, part (i) of this conjecture implies that there are infinitely many primes p with q(p) - 1 also prime (cf. A234644)."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A234615/b234615.txt\"\u003eTable of n, a(n) for n = 1..8000\u003c/a\u003e",
				"Z.-W. Sun, \u003ca href=\"http://arxiv.org/abs/1402.6641\"\u003eProblems on combinatorial properties of primes\u003c/a\u003e, arXiv:1402.6641, 2014"
			],
			"example": [
				"a(6) = 1 since 6 = 2 + 4 with prime(2) + phi(4) = 5 and q(5) - 1 = 2 both prime.",
				"a(58) = 1 since 58 = 12 + 46 with prime(12) + phi(46) = 59 and q(59) - 1 = 9791 both prime.",
				"a(526) = 1 since 526 = 389 + 137 with prime(389) + phi(137) = 2819 and q(2819) - 1 = 326033386646595458662191828888146112979 both prime."
			],
			"mathematica": [
				"f[n_,k_]:=Prime[k]+EulerPhi[n-k]",
				"q[n_,k_]:=PrimeQ[f[n,k]]\u0026\u0026PrimeQ[PartitionsQ[f[n,k]]-1]",
				"a[n_]:=Sum[If[q[n,k],1,0],{k,1,n-1}]",
				"Table[a[n],{n,1,100}]"
			],
			"xref": [
				"Cf. A000009, A000010, A000040, A234475, A234514, A234530, A234567, A234569, A234572, A234644"
			],
			"keyword": "nonn",
			"offset": "1,8",
			"author": "_Zhi-Wei Sun_, Dec 28 2013",
			"references": 12,
			"revision": 16,
			"time": "2014-04-05T22:30:48-04:00",
			"created": "2013-12-29T01:30:12-05:00"
		}
	]
}