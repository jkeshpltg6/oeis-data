{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A341356",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 341356,
			"data": "0,1,1,1,2,2,1,1,1,1,1,1,2,2,2,2,2,2,3,3,3,3,3,3,4,4,4,4,4,4,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3",
			"name": "The most significant digit in A097801-base.",
			"comment": [
				"A097801-base uses values 1, 2, 2*3, 2*3*5, 2*3*5*7, 2*3*5*7*9, 2*3*5*7*9*11, 2*3*5*7*9*11*13, 2*3*5*7*9*11*13*15, ..., for its digit-positions, instead of primorials (A002110), thus up to 1889 = 2*3*5*7*9 - 1 = 9*A002110(4) - 1 its representation is identical with the primorial base A049345. Therefore this sequence differs from A276153 for the first time at n=1890, where a(1890)=1, while A276153(1890)=9, as 1890 = 9*A002110(4).",
				"Therefore this sequence might be produced as a rough approximation of A276153 by naive machine learning/mining algorithms. - _Antti Karttunen_, Mar 09 2021"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A341356/b341356.txt\"\u003eTable of n, a(n) for n = 0..65537\u003c/a\u003e"
			],
			"example": [
				"In A097801-base, where the digit-positions are given by 1 and the terms of A097801 from its term a(1) onward: 2, 6, 30, 210, 1890, 20790, 270270, 4054050, ..., number 29 is expressed as \"421\" as 29 = 4*6 + 2*2 + 1*1, thus a(29) = 4. In the same base, number 30 is expressed as \"1000\" as 30 = 1*30, thus a(30) = 1.",
				"Number 1890 = 2*3*5*7*9 is expressed as \"100000\", thus a(1890) = 1."
			],
			"mathematica": [
				"Block[{nn = 105, b}, b = MixedRadix@ NestWhile[Prepend[#1, 2 #2 - 1] \u0026 @@ {#, Length[#] + 1} \u0026, {2}, Times @@ # \u003c nn \u0026]; Array[First@ IntegerDigits[#, b] \u0026, nn + 1, 0]] (* _Michael De Vlieger_, Feb 23 2021 *)"
			],
			"program": [
				"(PARI) A341356(n) = { my(m=2, k=3); while(n\u003e=m, n \\= m; m = k; k += 2); (n); }; \\\\ _Antti Karttunen_ \u0026 _Kevin Ryde_, Feb 24 2021"
			],
			"xref": [
				"Cf. A097801.",
				"Cf. A341513 (sum of digits in the same base), A341514 (number of trailing zeros).",
				"Cf. also A002110, A049345.",
				"Differs from similarly constructed A276153 for the first time at n=1890, where a(1890)=1, while A276153(1890)=9.",
				"Differs from similarly constructed A099564 for the first time at n=210, where a(210)=1, while A099564(210)=7."
			],
			"keyword": "nonn,base",
			"offset": "0,5",
			"author": "_Antti Karttunen_, Feb 23 2021",
			"references": 3,
			"revision": 25,
			"time": "2021-03-11T21:07:13-05:00",
			"created": "2021-02-24T08:18:46-05:00"
		}
	]
}