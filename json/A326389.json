{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A326389",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 326389,
			"data": "32767,65535,67053,2097151,4381419,7174453,9808617,13938267,14348906,19617234,21523360,29425851,39234468,43046720,48686547,49043085,58851702,68660319,71270178,78468936,88277553,98086170,107894787,115174101,117703404,134217727,142540356",
			"name": "Non-oblong numbers that are repdigits with length \u003e 2 in exactly three bases.",
			"comment": [
				"The number of Brazilian representations of a non-oblong number m with repdigits of length = 2 is beta'(m) = tau(m)/2 - 1. So, as here beta\"(m) = 3, beta(m) = tau(m)/2 + 2 where beta(m) is the number of Brazilian representations of m. So, this sequence is the first subsequence of A326382.",
				"As tau(m) = 2 * (beta(m) - 2) is even, the terms of this sequence are not squares.",
				"Some Mersenne numbers belong to this sequence: M_15 = a(1), M_16 = a(2), M_21 = a(4), M_27 = a(26), ..."
			],
			"link": [
				"Bernard Schott, \u003ca href=\"/A326389/a326389.pdf\"\u003eArray of relations beta = f(tau)\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_OEIS:_Section_Br#Brazilian_numbers\"\u003eIndex entries for sequences related to Brazilian numbers\u003c/a\u003e"
			],
			"example": [
				"tau(m) = 8 and beta(m) = 6 for m = 32767 with 32767 = R(15)_2 = 77777_8 = (31,31,31)_32.",
				"tau(m) = 12 and beta(m) = 8 for m = 2097151 with 2097151 = R(21)_2 = 7777777_8 = (127,127,127)_128.",
				"tau(m) = 16 and beta(m) = 10 with m = 67053 = (31,31,31)_46 = (21,21,21)_56 = 333_149."
			],
			"program": [
				"(PARI) isoblong(n) = my(m=sqrtint(n)); m*(m+1)==n; \\\\ A002378",
				"beta(n) = sum(i=2, n-2, #vecsort(digits(n, i), , 8)==1); \\\\ A220136",
				"isok(m) = !isprime(m) \u0026\u0026 !isoblong(m) \u0026\u0026 (beta(m) == numdiv(m)/2 + 2); \\\\ _Jinyuan Wang_, Aug 02 2019"
			],
			"xref": [
				"Cf. A000005 (tau), A220136 (beta).",
				"Subsequence of A167782, A167783, A290869, A308874 and A326382.",
				"Cf. A326386 (non-oblongs with tau(m)/2 - 1), A326387 (non-oblongs with tau(m)/2), A326388 (non-oblongs with tau(m)/2 + 1), this sequence (non-oblongs with tau(m)/2 + 2), A326705 (non-oblongs with tau(m)/2 + k, k \u003e=3)."
			],
			"keyword": "nonn,base,more",
			"offset": "1,1",
			"author": "_Bernard Schott_, Jul 20 2019",
			"references": 7,
			"revision": 28,
			"time": "2019-08-03T21:41:28-04:00",
			"created": "2019-08-03T21:41:28-04:00"
		}
	]
}