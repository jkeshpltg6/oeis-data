{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A258092",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 258092,
			"data": "1,-2,4,-10,20,-36,64,-112,189,-308,492,-778,1210,-1844,2776,-4144,6114,-8914,12884,-18484,26302,-37124,52040,-72512,100415,-138196,189160,-257648,349184,-470932,632312,-845472,1125853,-1493222,1973060,-2597892,3408754",
			"name": "Expansion of f(x, x^2) / psi(x)^3 in powers of x where psi(), f(, ) are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A258092/b258092.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(1/3) * eta(q)^2 * eta(q^3)^2 / (eta(q^2)^5 * eta(q^6)) in powers of q.",
				"Euler transform of period 6 sequence [-2, 3, -4, 3, -2, 2, ...].",
				"G.f.: Product_{k\u003e0} (1 - x^(3*k)) / ((1 + x^(3*k)) * (1 + x^k)^2 * (1 - x^(2*k))^3).",
				"a(n) = A258093(3*n - 1)."
			],
			"example": [
				"G.f. = 1 - 2*x + 4*x^2 - 10*x^3 + 20*x^4 - 36*x^5 + 64*x^6 - 112*x^7 + ...",
				"G.f. = 1/q - 2*q^2 + 4*q^5 - 10*q^8 + 20*q^11 - 36*q^14 + 64*q^17 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x]^2 QPochhammer[ x^3]^2 / ( QPochhammer[ x^2]^5 QPochhammer[ x^6]), {x, 0, n}]; (* _Michael Somos_, May 25 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A)^2 * eta(x^3 + A)^2 / (eta(x^2 + A)^5 * eta(x^6 + A)), n))};"
			],
			"xref": [
				"Cf. A258093."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, May 19 2015",
			"references": 2,
			"revision": 18,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2015-05-19T18:02:24-04:00"
		}
	]
}