{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A026374",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 26374,
			"data": "1,1,1,1,3,1,1,4,4,1,1,6,11,6,1,1,7,17,17,7,1,1,9,30,45,30,9,1,1,10,39,75,75,39,10,1,1,12,58,144,195,144,58,12,1,1,13,70,202,339,339,202,70,13,1,1,15,95,330,685,873,685,330,95,15,1",
			"name": "Triangular array T read by rows: T(n,0) = T(n,n) = 1 for all n \u003e= 0, T(n,k) = T(n-1,k-1) + T(n-1,k) for odd n and 1\u003c = k \u003c= n-1, T(n,k) = T(n-1,k-1) + T(n-1,k) + T(n-2,k-1) for even n and 1 \u003c= k \u003c= n-1.",
			"comment": [
				"T(n,k) is number of lattice paths from (0,0) to (n,n-2k) using steps U=(1,1), D=(1,-1) and, at levels ...,-4,-2,0,2,4,..., also H=(2,0). Example: T(4,1)=6 because we have the following paths from (0,0) to (4,2): UUUD, UUH, UUDU, UDUU, HUU and DUUU. Row sums yield A026383. Column 1 is A032766, column 2 is A026381, column 3 is A026382. - _Emeric Deutsch_, Jan 25 2004"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A026374/b026374.txt\"\u003eRows n = 0..125 of triangle, flattened\u003c/a\u003e",
				"Paul Barry, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL19/Barry/barry321.html\"\u003eJacobsthal Decompositions of Pascal's Triangle, Ternary Trees, and Alternating Sign Matrices\u003c/a\u003e, Journal of Integer Sequences, 19, 2016, #16.3.5."
			],
			"formula": [
				"T(n, k) = number of integer strings s(0), ..., s(n) such that s(0)=0, s(n) = n-2k, where, for 1 \u003c= i \u003c= n, s(i) is even if i is even and |s(i) - s(i-1)| \u003c= 1.",
				"From _Emeric Deutsch_, Jan 25 2004: (Start)",
				"T(2n, k) = Sum_{j=ceiling(k/2)..k} 3^(2j-k)*binomial(n, j)*binomial(j, k-j);",
				"T(2n+1, k) = T(2n, k-1) + T(2n, k).",
				"G.f.: (1 + z + t*z)/(1 - (1+3*t+t^2)*z^2) = 1 + (1+t)*z + (1+3*t+t^2)*z^2+ ... .",
				"Generating polynomial for row 2n is (1 + 3*t + t^2)^n;",
				"Generating polynomial for row 2n+1 it is (1+t)*(1 + 3*t + t^2)^n. (End)",
				"From _Emeric Deutsch_, Jan 30 2004: (Start)",
				"T(2n, k) = Sum_{j=ceiling(k/2)..k} 3^(2j-k)*binomial(n, j)*binomial(j, k-j);",
				"T(2n+1, k) = T(2n, k-1) + T(2n, k). (End)"
			],
			"example": [
				"Triangle starts:",
				"  1;",
				"  1,  1;",
				"  1,  3,   1;",
				"  1,  4,   4,   1;",
				"  1,  6,  11,   6,    1;",
				"  1,  7,  17,  17,    7,    1;",
				"  1,  9,  30,  45,   30,    9,    1;",
				"  1, 10,  39,  75,   75,   39,   10,    1;",
				"  1, 12,  58, 144,  195,  144,   58,   12,   1;",
				"  1, 13,  70, 202,  339,  339,  202,   70,  13,   1;",
				"  1, 15,  95, 330,  685,  873,  685,  330,  95,  15,  1;",
				"  1, 16, 110, 425, 1015, 1558, 1558, 1015, 425, 110, 16, 1;",
				"(End)"
			],
			"mathematica": [
				"p[x, 1] := 1;",
				"p[x_, n_] := p[x, n] = If[Mod[n, 2] == 0, (x + 1)*p[x, n - 1], (x^2 + 1)^Floor[n/2]];",
				"a = Table[CoefficientList[p[x, n], x], {n, 1, 12}];",
				"Flatten[a] (* _Roger L. Bagula_ and _Gary W. Adamson_, Dec 04 2009 *)"
			],
			"program": [
				"(Haskell)",
				"a026374 n k = a026374_tabl !! n !! k",
				"a026374_row n = a026374_tabl !! n",
				"a026374_tabl = [1] : map fst (map snd $ iterate f (1, ([1, 1], [1]))) where",
				"   f (0, (us, vs)) = (1, (zipWith (+) ([0] ++ us) (us ++ [0]), us))",
				"   f (1, (us, vs)) = (0, (zipWith (+) ([0] ++ vs ++ [0]) $",
				"                             zipWith (+) ([0] ++ us) (us ++ [0]), us))",
				"-- _Reinhard Zumkeller_, Feb 22 2014"
			],
			"xref": [
				"Cf. A026383, A051159,A169623, A007318",
				"Cf. A026375 (central terms)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Clark Kimberling_",
			"references": 18,
			"revision": 35,
			"time": "2019-06-13T04:15:02-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}