{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A300396",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 300396,
			"data": "0,1,2,1,1,1,2,1,2,1,2,3,1,1,3,1,3,4,2,2,4,1,2,1,1,2,4,3,1,1,2,1,6,2,2,2,5,1,4,1,2,6,3,3,3,1,2,3,4,3,3,2,4,2,2,1,7,3,1,4,1,2,8,1,3,7,3,4,6,3,4,4,6,4,3,2,4,3,1,2",
			"name": "Number of ways to write n^2 as x^2 + y^2 + z^2 + w^2 with x,y,z,w nonnegative integers and z \u003c= w such that 2*x or y is a power of 4 (including 4^0 = 1) and x + 63*y = 2^(2k+1) for some k = 0,1,2,....",
			"comment": [
				"Conjecture: a(n) \u003e 0 for all n \u003e 1, and a(n) = 1 only for n = 5, 13, 25, 29, 59, 61, 79, 91, 95, 101, 103, 1315, 2^k (k = 1,2,3,...), 2^(2k+1)*m (k = 0,1,2,... and m = 3, 5, 7, 11, 15, 19, 23, 887).",
				"This is stronger than the conjecture that A300360(n) \u003e 0 for all n \u003e 1. Note that a(387) = 3 \u003c A300360(387) = 4 and a(1774) = 1 \u003c A300360(1774) = 2.",
				"We have verified that a(n) \u003e 0 for all n = 2..10^7.",
				"See also A299537, A299794 and A300219 for similar conjectures."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A300396/b300396.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2016.11.008\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, J. Number Theory 175(2017), 167-190.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1701.05868\"\u003eRestricted sums of four squares\u003c/a\u003e, arXiv:1701.05868 [math.NT], 2017-2018."
			],
			"example": [
				"a(29) = 1 since 29^2 = 2^2 + 2^2 + 7^2 + 28^2 with 2*2 = 4^1 and 2 + 63*2 = 2^7.",
				"a(86) = 2 since 65^2 + 1^2 + 19^2 + 53^2 = 65^2 + 1^2 + 31^2 + 47^2 with 1 = 4^0 and 65 + 63*1 = 2^7.",
				"a(1774) = 1 since 1774^2 = 8^2 + 520^2 + 14^2 + 1696^2 with 2*8 = 4^2 and 8 + 63*520 = 2^15."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]];",
				"Pow[n_]:=Pow[n]=IntegerQ[Log[4,n]];",
				"tab={};Do[r=0;Do[If[Pow[y]||Pow[(2*4^k-63y)/2],Do[If[SQ[n^2-y^2-(2*4^k-63y)^2-z^2],r=r+1],{z,0,Sqrt[Max[0,(n^2-y^2-(2*4^k-63y)^2)/2]]}]],{k,0,Log[4,Sqrt[63^2+1]*n/2]},{y,0,Min[n,2*4^k/63]}];tab=Append[tab,r],{n,1,80}];Print[tab]"
			],
			"xref": [
				"Cf. A000118, A000290, A000302, A271518, A279612, A281976, A299924, A299537, A299794, A300219, A300356, A300360, A300362."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Zhi-Wei Sun_, Mar 05 2018",
			"references": 29,
			"revision": 10,
			"time": "2018-03-05T08:27:00-05:00",
			"created": "2018-03-05T08:27:00-05:00"
		}
	]
}