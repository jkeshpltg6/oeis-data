{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002001",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2001,
			"data": "1,3,12,48,192,768,3072,12288,49152,196608,786432,3145728,12582912,50331648,201326592,805306368,3221225472,12884901888,51539607552,206158430208,824633720832,3298534883328,13194139533312,52776558133248,211106232532992,844424930131968",
			"name": "a(n) = 3*4^(n-1), n\u003e0; a(0)=1.",
			"comment": [
				"Second binomial transform of (1,1,4,4,16,16,...) = (3*2^n+(-2)^n)/4. - _Paul Barry_, Jul 16 2003",
				"Number of vertices (or sides) formed after the (n-1)-th iterate towards building a Koch's snowflake. - _Lekraj Beedassy_, Jan 24 2005",
				"For n\u003e=1, a(n) is equal to the number of functions f:{1,2...,n}-\u003e{1,2,3,4} such that for a fixed x in {1,2,...,n} and a fixed y in {1,2,3,4} we have f(x)\u003c\u003ey. - Aleksandar M. Janjic and _Milan Janjic_, Mar 27 2007",
				"a(n) = (n+1) terms in the sequence (1, 2, 3, 3, 3,...) dot (n+1) terms in the sequence (1, 1, 3, 12, 48,...). Example: a(4) = 192 = (1, 2, 3, 3, 3) dot (1, 1, 3, 12, 48) = (1 + 2 + 9 + 36 + 144). - _Gary W. Adamson_, Aug 03 2010",
				"a(n) is the number of compositions of n when there are 3 types of each natural number. - _Milan Janjic_, Aug 13 2010",
				"See A178789 for the number of acute (= exterior) angles of the Koch snowflake referred to in the above comment by L. Beedassy. - _M. F. Hasler_, Dec 17 2013",
				"After 1, subsequence of A033428. - _Vincenzo Librandi_, May 26 2014",
				"a(n) counts walks (closed) on the graph G(1-vertex;1-loop x3,2-loop x3,3-loop x3,4-loop x3,...). - _David Neil McGrath_, Jan 01 2015",
				"For n \u003e 1, a(n) are numbers k such that (2^(k-1) mod k)/(2^k mod k) = 2; 2^(a(n)-1) == 2^(2n-1) (mod a(n)) and 2^a(n) == 2^(2n-2) (mod a(n)). - _Thomas Ordowski_, Apr 22 2020"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A002001/b002001.txt\"\u003eTable of n, a(n) for n = 0..300\u003c/a\u003e",
				"Milan Janjic, \u003ca href=\"http://www.pmfbl.org/janjic/\"\u003eEnumerative Formulas for Some Functions on Finite Sets\u003c/a\u003e",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=456\"\u003eEncyclopedia of Combinatorial Structures 456\u003c/a\u003e",
				"P. Kernan, \u003ca href=\"http://theory2.phys.cwru.edu/~pete/java_chaos/KochApplet.html\"\u003eKoch Snowflake\u003c/a\u003e [Broken link]",
				"C. Lanius, \u003ca href=\"http://math.rice.edu/~lanius/frac/koch/koch.html\"\u003eThe Koch Snowflake\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/KochSnowflake.html\"\u003eKoch Snowflake\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Koch_snowflake\"\u003eKoch snowflake\u003c/a\u003e",
				"\u003ca href=\"/index/Di#divseq\"\u003eIndex to divisibility sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_01\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4)."
			],
			"formula": [
				"From _Paul Barry_, Apr 20 2003: (Start)",
				"a(n) = (3*4^n + 0^n)/4 (with 0^0=1).",
				"E.g.f.: (3*exp(4*x) + 1)/4. (End)",
				"With interpolated zeros, this has e.g.f. (3*cosh(2*x) + 1)/4 and binomial transform A006342. - _Paul Barry_, Sep 03 2003",
				"a(n) = Sum_{j=0..1} Sum_{k=0..n} C(2n+j, 2k). - _Paul Barry_, Nov 29 2003",
				"G.f.: (1-x)/(1-4*x). The sequence 1, 3, -12, 48, -192... has g.f. (1+7*x)/(1+4*x). - _Paul Barry_, Feb 12 2004",
				"a(n) = 3*Sum_{k=0..n-1} a(k). - _Adi Dani_, Jun 24 2011",
				"G.f.: 1/(1-3*Sum_{k\u003e=1} x^k). - _Joerg Arndt_, Jun 24 2011",
				"Row sums of triangle A134316. - _Gary W. Adamson_, Oct 19 2007",
				"a(n) = A011782(n) * A003945(n). - _R. J. Mathar_, Jul 08 2009",
				"If p[1]=3, p[i]=3, (i\u003e1), and if A is Hessenberg matrix of order n defined by: A[i,j]=p[j-i+1], (i\u003c=j), A[i,j]=-1, (i=j+1), and A[i,j]=0 otherwise. Then, for n\u003e=1, a(n)=det A. - _Milan Janjic_, Apr 29 2010",
				"a(n) = 4*a(n-1), a(0)=1, a(1)=3. - _Vincenzo Librandi_, Dec 31 2010",
				"G.f.: 1 - G(0) where G(k) = 1 - 1/(1-3*x)/(1-x/(x-1/G(k+1) )); (recursively defined continued fraction). - _Sergei N. Gladkovskii_, Jan 25 2013",
				"G.f.: x+2*x/(G(0)-2), where G(k) = 1 + 1/(1 - x*(3*k+1)/(x*(3*k+4) + 1/G(k+1))); (continued fraction). - _Sergei N. Gladkovskii_, May 26 2013",
				"a(n) = ceiling(3*4^(n-1)). - _Wesley Ivan Hurt_, Dec 17 2013",
				"Construct the power matrix T(n,j)=[A(n)^*j]*[S(n)^*(j-1)] where A(n)=(3,3,3...) and S(n)=(0,1,0,0...). (* is convolution operation). Then T(n,j) counts n-walks containing j loops on the single vertex graph above and a(n) = Sum_{j=1...n} T(n,j). (S(n)^*0=I.) - _David Neil McGrath_, Jan 01 2015"
			],
			"maple": [
				"A002001:=n-\u003eceil(3*4^(n-1)); seq(A002001(n), n=0..30); # _Wesley Ivan Hurt_, Dec 17 2013"
			],
			"mathematica": [
				"Table[Ceiling[3*4^(n - 1)], {n, 0, 30}] (* _Wesley Ivan Hurt_, May 26 2014 *)"
			],
			"program": [
				"(MAGMA) [ (3*4^n+0^n)/4: n in [0..22] ]; // _Klaus Brockhaus_, Aug 15 2009",
				"(PARI) v=vector(100,n,3*4^(n-2));v[1]=1;v \\\\ _Charles R Greathouse IV_, May 19, 2011",
				"(PARI) A002001=n-\u003eif(n,3*4^(n-1),1) \\\\ _M. F. Hasler_, Dec 17 2013"
			],
			"xref": [
				"First difference of 4^n (A000302).",
				"Cf. A134316."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Dec 11 1996",
			"references": 45,
			"revision": 117,
			"time": "2021-06-01T08:01:47-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}