{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A325007",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 325007,
			"data": "1,2,1,3,6,1,4,18,10,1,5,40,55,15,1,6,75,200,126,21,1,7,126,560,700,252,28,1,8,196,1316,2850,1996,462,36,1,9,288,2730,9261,11376,5004,792,45,1,10,405,5160,25480,50127,38550,11440,1287,55,1,11,550,9075,61776,181027,225225,116160,24310,2002,66,1",
			"name": "Array read by descending antidiagonals: A(n,k) is the number of achiral colorings of the facets of a regular n-dimensional orthotope using up to k colors.",
			"comment": [
				"Also called hypercube, n-dimensional cube, and measure polytope. For n=1, the figure is a line segment with two vertices. For n=2 the figure is a square with four edges. For n=3 the figure is a cube with six square faces. For n=4, the figure is a tesseract with eight cubic facets. The Schläfli symbol, {4,3,...,3}, of the regular n-dimensional orthotope (n\u003e1) consists of a four followed by n-2 threes. Each of its 2n facets is an (n-1)-dimensional orthotope. An achiral coloring is identical to its reflection.",
				"Also the number of achiral colorings of the vertices of a regular n-dimensional orthoplex using up to k colors."
			],
			"link": [
				"Robert A. Russell, \u003ca href=\"/A325007/b325007.txt\"\u003eTable of n, a(n) for n = 1..325\u003c/a\u003e",
				"Robin Chapman, answer to \u003ca href=\"https://math.stackexchange.com/q/5732/\"\u003eColoring the faces of a hypercube\u003c/a\u003e, Math StackExchange, September 30, 2010."
			],
			"formula": [
				"A(n,k) = binomial(binomial(k+1,2) + n-1, n) - binomial(binomial(k,2),n).",
				"A(n,k) = Sum_{j=1..2*n} A325011(n,j) * binomial(k,j).",
				"A(n,k) = 2*A325005(n,k) - A325004(n,k) = (A325004(n,k) - 2*A325006(n,k)) / 2 = A325005(n,k) + A325006(n,k).",
				"G.f. for row n: Sum{j=1..2*n} A325011(n,j) * x^j / (1-x)^(j+1).",
				"Linear recurrence for row n: T(n,k) = Sum_{j=0..2*n} binomial(-2-j,2*n-j) * T(n,k-1-j).",
				"G.f. for column k: 1/(1-x)^binomial(k+1,2) - (1+x)^binomial(k,2)."
			],
			"example": [
				"Array begins with A(1,1):",
				"1  2   3     4      5      6       7        8         9        10 ...",
				"1  6  18    40     75    126     196      288       405       550 ...",
				"1 10  55   200    560   1316    2730     5160      9075     15070 ...",
				"1 15 126   700   2850   9261   25480    61776    135675    275275 ...",
				"1 21 252  1996  11376  50127  181027   559728   1529892   3784627 ...",
				"1 28 462  5004  38550 225225 1053304  4119648  13942908  41918800 ...",
				"1 36 792 11440 116160 881595 5263336 25794288 107427420 390891160 ...",
				"For a(2,2)=6, all colorings are achiral: two with just one of the colors, two with one color on just one edge, one with opposite colors the same, and one with opposite colors different."
			],
			"mathematica": [
				"Table[Binomial[Binomial[d-n+2,2]+n-1,n]-Binomial[Binomial[d-n+1,2],n],{d,1,11},{n,1,d}] // Flatten"
			],
			"program": [
				"(PARI) a(n, k) = binomial(binomial(k+1, 2)+n-1, n) - binomial(binomial(k, 2), n)",
				"array(rows, cols) = for(x=1, rows, for(y=1, cols, print1(a(x, y), \", \")); print(\"\"))",
				"/* Print initial 6 rows and 8 columns of array as follows: */",
				"array(6, 8) \\\\ _Felix Fröhlich_, May 30 2019"
			],
			"xref": [
				"Cf. A325004 (oriented), A325005 (unoriented), A325006 (chiral), A325011 (exactly k colors).",
				"Other n-dimensional polytopes: A325001 (simplex), A325015 (orthoplex).",
				"Rows 1-2 are A000027, A002411; column 2 is A186783(n+2)."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "1,2",
			"author": "_Robert A. Russell_, May 27 2019",
			"references": 11,
			"revision": 12,
			"time": "2019-10-01T07:16:06-04:00",
			"created": "2019-06-11T06:06:53-04:00"
		}
	]
}