{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002527",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2527,
			"id": "M1626 N0637",
			"data": "0,1,2,6,18,60,184,560,1695,5200,15956,48916,149664,458048,1402360,4294417,13149210,40259178,123260854,377395940,1155508592,3537919648,10832298239,33165996032,101546731816,310913195800,951945967120,2914642812096,8923975209168",
			"name": "Number of permutations p on the set [n] with the properties that abs(p(i)-i) \u003c= 3 for all i and p(1) \u003c= 3.",
			"comment": [
				"a(n) is also the permanent of the n X n matrix that has ones on its diagonal, ones on its three superdiagonals, ones on its three subdiagonals (with the exception of a single zero in the (4,1)-entry), and is zero elsewhere.",
				"This is the second row of Kløve's Table 3."
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Nathaniel Johnston and Alois P. Heinz, \u003ca href=\"/A002527/b002527.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e (first 92 terms from Nathaniel Johnston)",
				"Torleiv Kløve, \u003ca href=\"http://www.ii.uib.no/publikasjoner/texrap/pdf/2008-376.pdf\"\u003e Spheres of Permutations under the Infinity Norm - Permutations with limited displacement. \u003c/a\u003e Reports in Informatics, Department of Informatics, University of Bergen, Norway, no. 376, November 2008.",
				"R. Lagrange, \u003ca href=\"http://archive.numdam.org/article/ASENS_1962_3_79_3_199_0.pdf\"\u003eQuelques résultats dans la métrique des permutations\u003c/a\u003e, Annales Scientifiques de l'École Normale Supérieure, Paris, 79 (1962), 199-241."
			],
			"formula": [
				"From _Nathaniel Johnston_, Apr 03 2011 (Start):",
				"a(n) = A002526(n) - A188379(n-1).",
				"a(n) = a(n-1) + A002526(n-1) + A002529(n-1).",
				"(End)",
				"G.f.: x*(x^7+2*x^6-2*x^4-2*x^3-1) / (x^14 +2*x^13 +2*x^11 +4*x^10 -2*x^9 -10*x^8 -16*x^7 -2*x^6 +8*x^5 +10*x^4 +2*x^2 +2*x-1). - _Alois P. Heinz_, Apr 07 2011"
			],
			"maple": [
				"with(LinearAlgebra):",
				"A002527:= n-\u003e `if`(n=0, 0, Permanent(Matrix(n, (i, j)-\u003e",
				"              `if`(abs(j-i)\u003c4 and [i, j]\u003c\u003e[4, 1], 1, 0)))):",
				"seq(A002527(n), n=0..20);"
			],
			"mathematica": [
				"A002527[n_] := If [n == 0, 0, Permanent[Table[If [Abs[j-i]\u003c4 \u0026\u0026 {i, j} != {4, 1}, 1, 0], {i, 1, n}, {j, 1, n}]]]; Table [A002527[n], {n, 0, 25}] (* _Jean-François Alcover_, Mar 11 2014, after Maple *)"
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Name and comments edited, and terms after a(11) added by _Nathaniel Johnston_, Apr 03 2011"
			],
			"references": 10,
			"revision": 42,
			"time": "2016-01-06T11:49:55-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}