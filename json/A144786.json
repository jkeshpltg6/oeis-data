{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A144786",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 144786,
			"data": "1,1,3,4,5,1,7,8,9,10,11,3,13,14,15,16,17,18,19,4,21,22,23,24,25,26,27,28,29,5,31,32,33,34,35,36,37,38,39,40,41,1,43,44,45,46,47,48,49,50,51,52,53,54,55,7,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,8,73,74,75,76",
			"name": "If n is an oblong number A002378, then a(n)=a(j) where j is the number of oblong numbers in (0,n], otherwise a(n)=n.",
			"comment": [
				"As a motivation, consider the greedy decomposition of fractions 1/n into Egyptian fractions,",
				"n=1: 2,3,7,43,1807,3263443,.. A000058",
				"n=2: 3,7,43,1807,3263443,10650056950807,.. A000058",
				"n=3: 4,13,157,24493,599882557,359859081592975693,.. A082732",
				"n=4: 5,21,421,176821,31265489221,977530816197201697621,.. A144779",
				"n=5: 6,31,931,865831,749662454731,561993796032558961827631,.. A144780",
				"n=6: 7,43,1807,3263443,10650056950807,.. A000058",
				"n=7: 8,57,3193,10192057,103878015699193,.. A144781",
				"n=8: 9,73,5257,27630793,763460694178057,.. A144782",
				"n=9: 10,91,8191,67084291,4500302031888391,.. A144783",
				"n=10: 11,111,12211,149096311,22229709804712411,.. A144784",
				"n=11: 12,133,17557,308230693,95006159799029557,.. A144785",
				"n=12: 13,157,24493,599882557,.. A082732",
				"k=13: 14,183,33307,1109322943,..",
				"where the first few denominators of 1/n = 1/b(1)+1/b(2)+... have been tabulated.",
				"For some sets of n, the list b(i) of denominators is essentially the same: consider for example A000058, which represents primarily n=1, then in truncated form also n=2, and then n=6, n=42 etc. Or consider A082732 which represents n=3, then in truncated form n=12, n=156 etc.",
				"The OEIS sequence assigns the primary n to a(n). The interpretation of a(n) with ascending n is: n=1 is primary, a(1)=1.",
				"Decomposition of n=2 is equivalent to n=1, a(2)=1. Cases n=3 to 5 are primary (\"original\", \"new\"), and a(n)=n in these cases. n=6 is not new but essentially the same Egyptian series as seen for n=1, so a(6)=1. Cases n=7 to n=11 are \"new\" sequences, again a(n)=n in these cases, but then n=12 is represented by A082732 as already seen for n=3, so a(12)=3.",
				"Because the first denominator for the decomposition of 1/n is 1/(n+1), n+1 belongs to the sequence of denominators of the expansion of 1/a(n).",
				"The sequences b(.) have recurrences which are essentially 1+b(n-1)*(b(n-1)-1), looking up the oblong number at the position of the previous b(.). This is the reason why reverse look-up of the n via A000194 (number of oblong numbers up to n) as used in the definition is equivalent to the assignment described above."
			],
			"formula": [
				"a(n) = a(A000194(n+1)) if n in A002378. a(n) = n if n in A078358."
			],
			"example": [
				"n=1 is not in A002378, so a(1)=1.",
				"n=2 = A000058(2), so a(2)=1 because there is 1 oblong number \u003c=2 and \u003e0.",
				"n=3 is not in A002378, so a(3)=3.",
				"n=6 = A000058(3), so a(6)=a(2) because there are 2 oblong numbers \u003c=6 and \u003e0."
			],
			"xref": [
				"Cf. A000058, A002378, A078358, A144779, A144780, A144781, A144782, A144783, A144784, A144785, A144786."
			],
			"keyword": "nonn,easy,frac",
			"offset": "1,3",
			"author": "_Artur Jasinski_, Sep 22 2008, Sep 26 2008",
			"ext": [
				"a(57)=57 inserted, a(61)=61 corrected and better definition provided by _Omar E. Pol_, Dec 29 2008",
				"I did some further editing of this entry, but many of the lines are still obscure. - _N. J. A. Sloane_, Dec 29 2008",
				"Comments that connect to Egyptian fractions rephrased by _R. J. Mathar_, Oct 01 2009"
			],
			"references": 10,
			"revision": 22,
			"time": "2020-03-30T06:40:59-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}