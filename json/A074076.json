{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A074076",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 74076,
			"data": "60,4620,2024,5984,11480,22960,41580,8096,45920,521640,226884,392920,438944,803320,6725544,207900,37966500,1544620,6846840,2295200,2785484,9009000,4016600,188375200,3383500,149240,5738000,875124,12013456,8848840",
			"name": "One-sixth of the area of some primitive Heronian triangles with a distance of 2n+1 between the median and altitude points on the longest side.",
			"comment": [
				"With N=2n+1, such a triangle has sides N*u +/- M, 2*M*u (the latter being cut into M*u +/- N by the corresponding altitude) and inradius M*(N - M)*v. The first entry, in particular, is associated with sequence A023039."
			],
			"link": [
				"Ray Chandler, \u003ca href=\"/A074076/b074076.txt\"\u003eTable of n, a(n) for n = 1..499\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HeronianTriangle.html\"\u003eHeronian Triangle.\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Heronian_triangle\"\u003eHeronian Triangle\u003c/a\u003e."
			],
			"formula": [
				"a(n) = M(n)*D(n)*u(n)*v(n)/6, where (u, v) is the fundamental solution to x^2 - D*y^2 = 1, with M = 2*A074075(n); D = A074074(n) = N^2 - M^2."
			],
			"maple": [
				"A033313 := proc(Dcap) local c,i,fr,nu,de ; if issqr(Dcap) then -1; else c := numtheory[cfrac](sqrt(Dcap)) ; for i from 1 do try fr := numtheory[nthconver](c,i) ; nu := numer(fr) ; de := denom(fr) ; if nu^2-Dcap*de^2=1 then RETURN(nu) ; fi; catch: RETURN(-1) ; end try; od: fi: end:",
				"A074076 := proc(n) local Dmin,xmin,Dcap ; Dmin := -1; xmin := -1; mmin := -1; ymin := -1; for m from 1 to n do Dcap := (2*n+1+2*m)*(2*n+1-2*m) ; x := A033313(Dcap) ; if xmin = -1 or (x \u003e0 and x\u003cxmin ) then mmin := m ; xmin := x ; ymin := sqrt((xmin^2-1)/Dcap) ; Dmin := Dcap ; fi; od: Mmin := 2*mmin ; Mmin*Dmin*xmin*ymin/6 ; end:",
				"seq(A074076(n),n=1..80) ; # _R. J. Mathar_, Sep 21 2009"
			],
			"xref": [
				"Cf. A074074, A074075, A023039, A002349, A002350."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Lekraj Beedassy_, Aug 28 2002",
			"ext": [
				"Removed assertion that these are the minimum areas - _R. J. Mathar_, Sep 21 2009"
			],
			"references": 2,
			"revision": 16,
			"time": "2017-07-30T16:32:58-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}