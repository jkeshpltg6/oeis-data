{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A252505",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 252505,
			"data": "1,2,2,3,2,4,2,4,3,4,2,6,2,4,4,4,2,6,2,6,4,4,2,8,3,4,4,6,2,8,2,4,4,4,4,9,2,4,4,8,2,8,2,6,6,4,2,8,3,6,4,6,2,8,4,8,4,4,2,12,2,4,6,4,4,8,2,6,4,8,2,12,2,4,6,6,4,8,2,8,4,4,2,12,4,4,4,8,2,12,4,6,4,4,4,8,2,6,6,9",
			"name": "Number of biquadratefree (4th power free) divisors of n.",
			"comment": [
				"Equivalently, a(n) is the number of divisors of n that are in A046100.",
				"a(n) is also the number of divisors d such that the greatest common square divisor of d and n/d is 1."
			],
			"reference": [
				"Paul J. McCarthy, Introduction to Arithmetical Functions, Springer Verlag, 1986, page 37, Exercise 1.27"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A252505/b252505.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Biquadratefree.html\"\u003eBiquadratefree\u003c/a\u003e."
			],
			"formula": [
				"Dirichlet g.f.: zeta(s)^2/zeta(4*s).",
				"Sum_{k=1..n} a(k) ~ 90*n/Pi^4 * (log(n) - 1 + 2*gamma - 360*Zeta'(4)/Pi^4), where gamma is the Euler-Mascheroni constant A001620. - _Vaclav Kotesovec_, Feb 02 2019",
				"a(n) = Sum_{d|n} mu(gcd(d, n/d))^2. - _Ilya Gutkovskiy_, Feb 21 2020",
				"Multiplicative with a(p^e) = min(e, 3) + 1. - _Amiram Eldar_, Sep 19 2020"
			],
			"example": [
				"a(16) = 4 because there are 4 divisors of 16 that are 4th power free: 1,2,4,8.",
				"a(16) = 4 because there are 4 divisors d of 16 such that the greatest common square divisor of d and 16/d is 1: 1,2,8,16."
			],
			"mathematica": [
				"Prepend[Table[Apply[Times, (FactorInteger[n][[All, 2]] /. x_ /; x \u003e 3 -\u003e 3) + 1], {n, 2, 100}], 1]"
			],
			"program": [
				"(PARI) isA046100(n) = (n==1) || vecmax(factor(n)[, 2])\u003c4;",
				"a(n) = {d = divisors(n); sum(i=1, #d, isA046100(d[i]));} \\\\ _Michel Marcus_, Mar 22 2015"
			],
			"xref": [
				"Cf. A046100 (biquadratefree numbers).",
				"Cf. A034444 (squarefree divisors), A073184 (cubefree divisors)."
			],
			"keyword": "nonn,mult",
			"offset": "1,2",
			"author": "_Geoffrey Critzer_, Mar 21 2015",
			"references": 2,
			"revision": 34,
			"time": "2020-09-19T10:29:45-04:00",
			"created": "2015-03-25T14:18:32-04:00"
		}
	]
}