{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A235459",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 235459,
			"data": "2,4,16,56,368,116764,217093472",
			"name": "Number of facets of the correlation polytope of degree n.",
			"comment": [
				"The correlation polytope of degree n is the set of symmetric n X n matrices, P such that P[i,j] = Prob(X[i] = 1 and X[j] = 1) where (X[1],...,X[n]) is a sequence of 0/1 valued random variables (not necessarily independent). It is the convex hull of all n X n symmetric 0/1 matrices of rank 1.",
				"The correlation polytope COR(n) is affinely equivalent to CUT(n+1), where CUT(n) is the cut polytope of complete graph on n vertices -- the convex hull of indicator vectors of a cut delta(S) -- where S is a subset of the vertices. The cut delta(S) is the set of edges with one end point in S and one endpoint not in S.",
				"According to the SMAPO database it is conjectured that",
				"a(8) = 12,246,651,158,320. This database also says that the above value of a(7) is conjectural, but Ziegler lists it as known."
			],
			"reference": [
				"G. Kalai and G. Ziegler, ed. \"Polytopes: Combinatorics and Computation\", Springer, 2000, Chapter 1, pp 1-41.",
				"M. M. Deza, and M. Laurent, Geometry of Cuts and Metrics, Springer, 1997, pp. 52-54"
			],
			"link": [
				"T. Christof, \u003ca href=\"http://www.iwr.uni-heidelberg.de/groups/comopt/software/SMAPO/cut/cut.html\"\u003eThe SMAPO database about the CUT polytope\u003c/a\u003e",
				"G. Ziegler, \u003ca href=\"http://arxiv.org/abs/math/9909177\"\u003eLectures on 0/1 Polytopes\u003c/a\u003e, arXiv:math/9909177v1 (1999), p 22-28."
			],
			"example": [
				"a(2) corresponds to 0 \u003c= p[1,2] \u003c= p[1,1],p[2,2] and p[1,1] + p[2,2] - p[1,2] \u003c= 1."
			],
			"program": [
				"(sage)",
				"def Correlation(n):",
				"   if n == 0:",
				"      yield (tuple([]),tuple([]))",
				"      return",
				"   for x,y in Correlation(n-1):",
				"      yield (x + (0,),y + (n-1)*(0,))",
				"      yield (x + (1,),y + x)",
				"def CorrelationPolytope(n):",
				"   return Polyhedron(vertices=[x + y for x,y in Correlation(n)])",
				"def a(n):",
				"   return len(CorrelationPolytope(n).Hrepresentation())"
			],
			"keyword": "nonn,hard,more",
			"offset": "1,1",
			"author": "_Victor S. Miller_, Jan 10 2014",
			"references": 1,
			"revision": 23,
			"time": "2020-03-04T16:54:38-05:00",
			"created": "2014-01-14T00:37:19-05:00"
		}
	]
}