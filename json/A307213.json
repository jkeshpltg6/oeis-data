{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A307213",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 307213,
			"data": "1,2,5,9,10,11,16,17,23,30,31,39,40,41,50,51,52,43,53,64,65,66,67,79,92,93,94,95,96,97,111,112,113,128,129,130,131,147,148,149,166,167,185,186,187,169,170,171,172,173,192,193,213,214,215,216,217,218,219,220,221,222,223,244,245,246,247,248,249,250,229",
			"name": "Divide the natural numbers into sets of successive sizes 3,4,5,6,7,...,, starting with {1,2,3}. Cycle through each set until you reach a prime; if the prime was the n-th element in its set, jump to the n-th element of the next set.",
			"comment": [
				"\"Cycle\" in the definition, means that if no prime is found, go back to the start of the set.",
				"If a set does not contain a prime, the sequence goes into an infinite loop, but it is conjectured that this does not happen since the sets are of increasing length.",
				"The sets (rather intervals) are I_j = [(j^2 + 3*j - 2)/2, j*(j + 5)/2] =[A034856(j), A095998(j)], for j \u003e= 1. For the  number of primes in these intervals see A309121. - _Wolfdieter Lang_, Jul 13 2019"
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A307213/b307213.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A307213/a307213_1.gp.txt\"\u003ePARI program for A307213\u003c/a\u003e"
			],
			"example": [
				"The first set is {1,2,3}. We look at 1 then 2. 2 is prime, and it is the second number in the set. The next set is {4,5,6,7}. So we jump to the second element, 5. 5 is also prime, so we jump to the second element of the next set, {8,9,10,11,12}, which is 9, etc. If we reach the end of a set without reaching a prime, we loop back to the first element, which is the only way for a(n) \u003c a(n-1) to happen."
			],
			"mathematica": [
				"Nest[Append[#1, {If[#3 \u003c= Length@ #4, #3, #3 - Length@ #4], If[#2 == #3, {#4[[#3]]}, Join[#4, #4][[#2 ;; #3]]], #4}] \u0026 @@ {#1, #2, If[PrimeQ[#3[[#2]] ], #2, #2 + FirstPosition[RotateLeft[#3, #2], _?PrimeQ][[1]] ], #3} \u0026 @@ {#1, #2, Range[#3, #3 + #4]} \u0026 @@ {#, #[[-1, 1]], 1 + Max@ #[[-1, -1]], Length@ # + 2} \u0026, {{#1, #2[[1 ;; #1]], #2} \u0026 @@ {FirstPosition[#, _?PrimeQ][[1]], #}} \u0026@ Range@ 3, 19][[All, 2]] // Flatten (* _Michael De Vlieger_, Mar 31 2019 *)"
			],
			"program": [
				"(PARI) See Links section."
			],
			"xref": [
				"Cf. A034856, A095998, A309121."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Christopher Cormier_, Mar 28 2019",
			"ext": [
				"Edited by _N. J. A. Sloane_, Jul 13 2019"
			],
			"references": 2,
			"revision": 30,
			"time": "2019-07-14T15:23:17-04:00",
			"created": "2019-07-13T00:41:04-04:00"
		}
	]
}