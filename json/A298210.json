{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A298210",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 298210,
			"data": "1,1,2,2,3,2,3,4,6,3,6,2,7,3,6,8,4,6,10,6,6,6,11,4,15,7,18,6,5,6,15,16,6,4,3,6,19,10,14,12,5,6,22,6,6,11,23,8,21,15,4,14,27,18,6,12,10,5,10,6,31,15,6,32,21,6,34,4,22,3,35,12,18,19,30",
			"name": "Smallest n such that A001542(a(n)) == 0 (mod n), i.e., x=A001541(a(n)) and y=A001542(a(n)) is the fundamental solution of the Pell equation x^2 - 2*(n*y)^2 = 1.",
			"comment": [
				"The fundamental solution of the Pell equation x^2 - 2*(n*y)^2 = 1, is the smallest solution of x^2 - 2*y^2 = 1 satisfying y == 0 (mod n).",
				"If n is prime (i.e., n in A000040) then a(n) divides (n - Legendre symbol (n/2)); the Legendre symbol (n/2), or more general Kronecker symbol (n/2) is A091337(n). - _A.H.M. Smeets_, Jan 23 2018",
				"From _A.H.M. Smeets_, Jan 23 2018: (Start)",
				"Stronger, but conjectured:",
				"If n is prime (i.e., in A000040) and n in {2,3,5,7,11,13,19,23} (mod 24) then (n - Legendre symbol (n/2)) / a(n) == 2 (mod 4).",
				"If n is a safe prime (i.e., in A005385) and n in {7,23} (mod 24) then (n - Legendre symbol (n/2)) / a(n) = 2, i.e., a(n) is a Sophie Germain prime (A005384).",
				"If n is prime (i.e., in A000040) and n in {1,17} (mod 24) then (n - Legendre symbol (n/2)) / a(n) == 0 (mod 4). (End)"
			],
			"reference": [
				"Michael J. Jacobson, Jr. and Hugh C. Williams, Solving the Pell Equation, Springer, 2009, pages 1-17."
			],
			"link": [
				"A.H.M. Smeets, \u003ca href=\"/A298210/b298210.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e",
				"H. W. Lenstra Jr., \u003ca href=\"http://www.ams.org/notices/200202/fea-lenstra.pdf\"\u003eSolving the Pell Equation\u003c/a\u003e, Notices of the AMS, Vol.49, No. 2, Feb. 2002, pp. 182-192."
			],
			"formula": [
				"a(n) \u003c= A000010(n) \u003c n. - _A.H.M. Smeets_, Jan 23 2018",
				"A001541(a(n)) = A002350(2*n^2).",
				"A001542(a(n)) = A002349(2*n^2).",
				"if n | m then a(n) | a(m).",
				"a(2^(m+1)) = 2^m for m\u003e=0."
			],
			"mathematica": [
				"b[n_] := b[n] = Switch[n, 0, 0, 1, 2, _, 6 b[n - 1] - b[n - 2]];",
				"a[n_] := For[k = 1, True, k++, If[Mod[b[k], n] == 0, Return[k]]];",
				"a /@ Range[100] (* _Jean-François Alcover_, Nov 16 2019 *)"
			],
			"program": [
				"Python:",
				"xf,yf = 3,2",
				"x,n = 2*xf,0",
				"while n \u003c 20000:",
				"....n = n+1",
				"....y1,y0,i = 0,yf,1",
				"....while y0%n != 0:",
				"........y1,y0,i = y0,x*y0-y1,i+1",
				"....print(n,i)"
			],
			"xref": [
				"Cf. A298211, A298212."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_A.H.M. Smeets_, Jan 15 2018",
			"references": 5,
			"revision": 34,
			"time": "2019-11-16T03:23:17-05:00",
			"created": "2018-01-22T18:43:35-05:00"
		}
	]
}