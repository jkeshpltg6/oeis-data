{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A113446",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 113446,
			"data": "1,1,-1,1,2,-1,0,1,1,2,0,-1,2,0,-2,1,2,1,0,2,0,0,0,-1,3,2,-1,0,2,-2,0,1,0,2,0,1,2,0,-2,2,2,0,0,0,2,0,0,-1,1,3,-2,2,2,-1,0,0,0,2,0,-2,2,0,0,1,4,0,0,2,0,0,0,1,2,2,-3,0,0,-2,0,2,1,2,0,0,4,0,-2,0,2,2,0,0,0,0,0,-1,2,1,0,3,2,-2,0,2,0",
			"name": "Expansion of (phi(q)^2 - phi(q^3)^2) / 4 in powers of q where phi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A113446/b113446.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of (eta(q^2)^3 * eta(q^6) * eta(q^12)^2) / (eta(q) * eta(q^3) * eta(q^4)^2) in powers of q.",
				"Euler transform of period 12 sequence [1, -2, 2, 0, 1, -2, 1, 0, 2, -2, 1, -2, ...].",
				"Moebius transform is period 12 sequence [1, 0, -2, 0, 1, 0, -1, 0, 2, 0, -1, 0, ...].",
				"a(n) is multiplicative and a(2^e) = 1, a(3^e) = (-1)^e, a(p^e) = e+1 if p == 1 (mod 4), a(p^e) = (1 + (-1)^e)/2 if p == 3 (mod 4).",
				"G.f.: ((Sum_{k} x^(k^2))^2 - (Sum_{k} x^(3*k^2))^2) / 4.",
				"G.f.: Sum_{k\u003e0} x^(3*k-1) / (1 + x^(6*k-2)) + x^(3*k-2)/(1 + x^(6*k-4)).",
				"G.f.: Sum_{k\u003e0} x^k * (1 - x^(2*k))^2 / (1 + x^(6*k)).",
				"G.f.: x * Product_{k\u003e0} (1 - x^k)^2 * (1 + x^k)^3 * (1 + x^(3*k)) * (1 + x^(4*k) + x^(8*k))^2.",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (12 t)) = (t/i) g(t) where q = exp(2 Pi i t) and g() is the g.f. for A138949.",
				"a(n) = (-1)^e * A035154(n) where 3^e is the highest power of 3 dividing n.",
				"a(4*n + 1) = A008441(n).",
				"Expansion of q * f(-q, -q^5) * f(q, q^5)^2 / phi(-q^3) in powers of q where phi(), f(,) are Ramanujan theta functions. - _Michael Somos_, Jan 31 2015",
				"Expansion of q * (psi(q^3)^3 / psi(q)) * (phi(q) / phi(q^3)) in powers of q where phi(), psi() are Ramanujan theta functions."
			],
			"example": [
				"G.f. = q + q^2 - q^3 + q^4 + 2*q^5 - q^6 + q^8 + q^9 + 2*q^10 - q^12 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 1, 0, (-1)^IntegerExponent[ n, 3] Sum[ KroneckerSymbol[ -36, d], { d, Divisors[ n]}]]; (* _Michael Somos_, Jan 31 2015 *)",
				"a[ n_] := SeriesCoefficient[ (1/4) EllipticTheta[ 2, 0, q^(3/2)]^3 / EllipticTheta[ 2, 0, q^(1/2)] (EllipticTheta[ 3, 0, q] / EllipticTheta[ 3, 0, q^3]), {q, 0, n}]; (* _Michael Somos_, Jan 31 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, 0, (-1)^valuation(n, 3) * sumdiv(n, d, kronecker(-36, d)))};",
				"(PARI) {a(n) = if( n\u003c1, 0, direuler(p=2, n, if( p==3, 1 / (1 + X), 1 / (1 - X) / (1 - kronecker(-36, p) * X)))[n])};",
				"(PARI) {a(n) = local(A); if( n\u003c1, 0, n--; A = x * O(x^n); polcoeff( eta(x^2 + A)^3 * eta(x^6 + A) * eta(x^12 + A)^2 / (eta(x + A) * eta(x^3 + A) * eta(x^4 + A)^2), n))};",
				"(MAGMA) A := Basis( ModularForms( Gamma1(12), 1), 106); A[2] + A[3] - A[4] + A[5]; /* _Michael Somos_, Jan 31 2015 */"
			],
			"xref": [
				"Cf. A008441, A035154."
			],
			"keyword": "sign,mult",
			"offset": "1,5",
			"author": "_Michael Somos_, Nov 02 2005",
			"references": 11,
			"revision": 18,
			"time": "2021-03-12T22:24:43-05:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}