{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000294",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 294,
			"id": "M3393 N1372",
			"data": "1,1,4,10,26,59,141,310,692,1483,3162,6583,13602,27613,55579,110445,217554,424148,820294,1572647,2992892,5652954,10605608,19765082,36609945,67405569,123412204,224728451,407119735,733878402,1316631730,2351322765,4180714647,7401898452,13051476707,22922301583,40105025130,69909106888,121427077241,210179991927,362583131144",
			"name": "Expansion of g.f. Product_{k \u003e= 1} (1 - x^k)^(-k*(k+1)/2).",
			"comment": [
				"Number of partitions of n if there are k(k+1)/2 kinds of k (k=1,2,...). E.g., a(3)=10 because we have six kinds of 3, three kinds of 2+1 because there are three kinds of 2 and 1+1+1+1. - _Emeric Deutsch_, Mar 23 2005",
				"Euler transform of the triangular numbers 1,3,6,10,...",
				"Equals A028377: [1, 1, 3, 9, 19, 46, 100, ...] convolved with the aerated version of A000294: [1, 0, 1, 0, 4, 0, 10, 0, 26, 0, 59, ...]. - _Gary W. Adamson_, Jun 13 2009",
				"The formula for p3(n) in the article by S. Finch (page 2) is incomplete, terms with n^(1/2) and n^(1/4) are also needed. These terms are in the article by Mustonen and Rajesh (page 2) and agree with my results, but in both articles the multiplicative constant is marked only as C, resp. c3(m). The following is a closed form of this constant: Pi^(1/24) * exp(1/24 - Zeta(3) / (8*Pi^2) + 75*Zeta(3)^3 / (2*Pi^8)) / (A^(1/2) * 2^(157/96) * 15^(13/96)) = A255939 = 0.213595160470..., where A = A074962 is the Glaisher-Kinkelin constant and Zeta(3) = A002117. - _Vaclav Kotesovec_, Mar 11 2015 [The new version of \"Integer Partitions\" by S. Finch contains the missing terms, see pages 2 and 5. - _Vaclav Kotesovec_, May 12 2015]",
				"Number of solid partitions of corner-hook volume n. E.g., a(2) = 1 because there is only one solid partition [[[2]]] with cohook volume 2; a(3) = 4 because we have three solid partitions with two 1's (entry at (1,1,1) contributes 1, another entry at (2,1,1) or (1,2,1) or (1,1,2) contributes 2 to corner-hook volume) and one solid partition with single entry 3 (which contributes 3 to the corner-hook volume). Namely as 3D arrays [[[1],[1]]],[[[1]],[[1]]],[[[1]],[[1]]], [[[3]]]. - _Alimzhan Amanov_, Jul 13 2021"
			],
			"reference": [
				"R. Chandra, Tables of solid partitions, Proceedings of the Indian National Science Academy, 26 (1960), 134-139.",
				"V. S. Nanda, Tables of solid partitions, Proceedings of the Indian National Science Academy, 19 (1953), 313-314.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A000294/b000294.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Alimzhan Amanov and Damir Yeliussizov, \u003ca href=\"https://arxiv.org/abs/2009.00592\"\u003eMacMahon's statistics on higher-dimensional partitions\u003c/a\u003e, arXiv:2009.00592 [math.CO], 2020. Mentions this sequence.",
				"A. O. L. Atkin, P. Bratley, I. G. McDonald and J. K. S. McKay, \u003ca href=\"http://boltzmann.wdfiles.com/local--files/refined-counting/ABMM.pdf\"\u003eSome computations for m-dimensional partitions\u003c/a\u003e, Proc. Camb. Phil. Soc., 63 (1967), 1097-1100.",
				"A. O. L. Atkin, P. Bratley, I. G. McDonald and J. K. S. McKay, \u003ca href=\"/A000219/a000219.pdf\"\u003eSome computations for m-dimensional partitions\u003c/a\u003e, Proc. Camb. Phil. Soc., 63 (1967), 1097-1100. [Annotated scanned copy]",
				"R. Chandra, \u003ca href=\"/A000294/a000294_1.pdf\"\u003eTables of solid partitions\u003c/a\u003e, Proceedings of the Indian National Science Academy, 26 (1960), 134-139. [Annotated scanned copy]",
				"Nicolas Destainville and Suresh Govindarajan, \u003ca href=\"http://arxiv.org/abs/1406.5605\"\u003eEstimating the asymptotics of solid partitions\u003c/a\u003e, J. Stat. Phys. 158 (2015) 950-967; arXiv:1406.5605 [cond-mat.stat-mech], 2014.",
				"Steven Finch, \u003ca href=\"/A000219/a000219_1.pdf\"\u003eInteger Partitions\u003c/a\u003e, September 22, 2004, page 2. [Cached copy, with permission of the author]",
				"Vaclav Kotesovec, \u003ca href=\"/A000294/a000294.jpg\"\u003eGraph - The asymptotic ratio\u003c/a\u003e",
				"Ville Mustonen and R. Rajesh, \u003ca href=\"http://arXiv.org/abs/cond-mat/0303607\"\u003eNumerical Estimation of the Asymptotic Behaviour of Solid Partitions of an Integer\u003c/a\u003e, J. Phys. A 36 (2003), no. 24, 6651-6659; arXiv:cond-mat/0303607 [cond-mat.stat-mech], 2003.",
				"V. S. Nanda, \u003ca href=\"/A000294/a000294.pdf\"\u003eTables of solid partitions\u003c/a\u003e, Proceedings of the Indian National Science Academy, 19 (1953), 313-314. [Annotated scanned copy]"
			],
			"formula": [
				"a(n) = (1/(2*n))*Sum_{k=1..n} (sigma[2](k)+sigma[3](k))*a(n-k). - _Vladeta Jovovic_, Sep 17 2002",
				"a(n) ~ Pi^(1/24) * exp(1/24 - Zeta(3) / (8*Pi^2) + 75*Zeta(3)^3 / (2*Pi^8) - 15^(5/4) * Zeta(3)^2 * n^(1/4) / (2^(7/4)*Pi^5) + 15^(1/2) * Zeta(3) * n^(1/2) / (2^(1/2)*Pi^2) + 2^(7/4) * Pi * n^(3/4) / (3*15^(1/4))) / (A^(1/2) * 2^(157/96) * 15^(13/96) * n^(61/96)), where A = A074962 = 1.2824271291... is the Glaisher-Kinkelin constant and Zeta(3) = A002117 = 1.202056903... . - _Vaclav Kotesovec_, Mar 11 2015",
				"G.f.: exp(Sum_{k\u003e=1} (sigma_2(k) + sigma_3(k))*x^k/(2*k)). - _Ilya Gutkovskiy_, Aug 21 2018"
			],
			"maple": [
				"with(numtheory): etr:= proc(p) local b; b:=proc(n) option remember; local d,j; if n=0 then 1 else add(add(d*p(d), d=divisors(j)) *b(n-j), j=1..n)/n fi end end: a:= etr(n-\u003e n*(n+1)/2): seq(a(n), n=0..30);  # _Alois P. Heinz_, Sep 08 2008"
			],
			"mathematica": [
				"a[0] = 1; a[n_] := a[n] = 1/(2*n)*Sum[(DivisorSigma[2, k]+DivisorSigma[3, k])*a[n-k], {k, 1, n}]; Table[a[n], {n, 0, 40}] (* _Jean-François Alcover_, Mar 05 2014, after _Vladeta Jovovic_ *)",
				"nmax=50; CoefficientList[Series[Product[1/(1-x^k)^(k*(k+1)/2),{k,1,nmax}],{x,0,nmax}],x] (* _Vaclav Kotesovec_, Mar 11 2015 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c0, 0, polcoeff(exp(sum(k=1, n, x^k/(1-x^k)^3/k, x*O(x^n))), n)) \\\\ _Joerg Arndt_, Apr 16 2010",
				"(SageMath) # uses[EulerTransform from A166861]",
				"b = EulerTransform(lambda n: binomial(n+1, 2))",
				"print([b(n) for n in range(37)]) # _Peter Luschny_, Nov 11 2020"
			],
			"xref": [
				"Cf. A000293, A007294, A007326, A255939, A028377.",
				"Cf. also A000041, A000219, A000335, A000391, A000417, A000428, A255965.",
				"Cf. also A278403 (log of o.g.f.)."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Sascha Kurz_, Aug 15 2002"
			],
			"references": 46,
			"revision": 116,
			"time": "2021-08-31T06:26:47-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}