{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A128720",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 128720,
			"data": "1,1,3,6,16,40,109,297,836,2377,6869,20042,59071,175453,524881,1579752,4780656,14536878,44394980,136107872,418757483,1292505121,4001039563,12418772656,38641790001,120510911885,376628460529,1179376013552,3699860515924,11626784875214",
			"name": "Number of paths in the first quadrant from (0,0) to (n,0) using steps U=(1,1), D=(1,-1), h=(1,0) and H=(2,0).",
			"comment": [
				"Points of two kinds are placed on a line: light points having weight 1 and heavy points having weight 2. Number of configurations of points of total weight n, with some of the light points being paired off by nonintersecting arcs.",
				"Number of skew Dyck paths of semilength n having no UUU's. A skew Dyck path is a path in the first quadrant which begins at the origin, ends on the x-axis, consists of steps U=(1,1)(up), D=(1,-1)(down) and L=(-1,-1)(left) so that up and left steps do not overlap. The length of a path is defined to be the number of its steps. Example: a(3)=6 because we have UDUDUD, UDUUDD, UDUUDL, UUDDUD, UUDUDD and UUDUDL. a(n)=A128719(n,0). a(n)=A059397(n,n). a(n)=A132276(n,0).",
				"Hankel transform is the (1,3) Somos-4 sequence A174168. - _Paul Barry_, Mar 10 2010",
				"First column of the Riordan matrix A132276. - _Emanuele Munarini_, May 05 2011"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A128720/b128720.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e (first 101 terms from Vincenzo Librandi)",
				"Paul Barry, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL13/Barry1/barry95r.html\"\u003eGeneralized Catalan Numbers, Hankel Transforms and Somos-4 Sequences \u003c/a\u003e, J. Int. Seq. 13 (2010) #10.7.2.",
				"Paul Barry, \u003ca href=\"http://arxiv.org/abs/1107.5490\"\u003eInvariant number triangles, eigentriangles and Somos-4 sequences\u003c/a\u003e, arXiv:1107.5490 [math.CO], 2011.",
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/1910.00875\"\u003eGeneralized Catalan recurrences, Riordan arrays, elliptic curves, and orthogonal polynomials\u003c/a\u003e, arXiv:1910.00875 [math.CO], 2019.",
				"E. Deutsch, E. Munarini, S. Rinaldi, \u003ca href=\"http://dx.doi.org/10.1016/j.jspi.2010.01.015\"\u003eSkew Dyck paths\u003c/a\u003e, J. Stat. Plann. Infer. 140 (8) (2010) 2191-2203.",
				"M. Dziemianczuk, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2014.07.024\"\u003eEnumerations of plane trees with multiple edges and Raney lattice paths\u003c/a\u003e, Discrete Mathematics 337 (2014): 9-24.",
				"W. F. Klostermeyer, M. E. Mays, L. Soltes and G. Trapp, \u003ca href=\"http://www.fq.math.ca/Scanned/35-4/klostermeyer.pdf\"\u003eA Pascal rhombus\u003c/a\u003e, Fibonacci Quarterly, 35 (1997), 318-328.",
				"P. Rajkovic, P. Barry, and N. Savic, \u003ca href=\"http://www.math.bas.bg/infres/MathBalk/MB-26/MB-26-219-228.pdf\"\u003eNumber Sequences in an Integral Form with a Generalized Convolution Property and Somos-4 Hankel Determinants\u003c/a\u003e, Math. Balkanica, Vol. 26 (2012), Fasc. 1-2."
			],
			"formula": [
				"a(n) = Sum_{j=0..floor(n/2)} binomial(n-j, j)*m(n-2j), where m(k)=A001006(k) are the Motzkin numbers.",
				"G.f. = G satisfies z^2*G^2 - (1-z-z^2)*G + 1 = 0.",
				"G.f. = c(z^2/(1-z-z^2)^2)/(1-z-z^2), where c(z) = (1-sqrt(1-4z))/(2z) is the Catalan function.",
				"a(n) = a(n-1) + a(n-2) + Sum_{j=0..n-2} a(j)*a(n-2-j), a(0) = a(1) = 1.",
				"G.f.: (1/(1-x-x^2))*c(x^2/(1-x-x^2)^2) = (1/(1-x^2))*m(x/(1-x^2)), c(x) the g.f. of A000108, m(x) the g.f. of A001006. - _Paul Barry_, Mar 18 2010",
				"Let A(x) be the g.f., then B(x) = 1 + x*A(x) = 1 + 1*x + 1*x^2 + 3*x^3 + 6*x^4 + ... = 1/(1-z/(1-z/(1-z/(...)))) where z=x/(1+x-x^2) (continued fraction); more generally B(x)=C(x/(1+x-x^2)) where C(x) is the g.f. for the Catalan numbers (A000108). - _Joerg Arndt_, Mar 18 2011",
				"a(n) = Sum_{k=0..floor(n/2)} (binomial(2*k,k)/(k+1))*Sum_{j=0..floor(n/2)} binomial(n-j, 2*k)*binomial(n-j-2*k, j). - _Emanuele Munarini_, May 05 2011",
				"D-finite with recurrence: (n+2)*a(n) + (-2*n-1)*a(n-1) + 5*(-n+1)*a(n-2) + (2*n-5)*a(n-3) + (n-4)*a(n-4) = 0. - _R. J. Mathar_, Dec 03 2012",
				"G.f.: (1 - x - x^2 - sqrt(1 - 2*x - 5*x^2 + 2*x^3 + x^4))/(2*x^2) = 1/Q(0), where Q(k) = 1 - x - x^2 - x^2/Q(k+1); (continued fraction). - _Sergei N. Gladkovskii_, Oct 04 2013",
				"a(n) ~ sqrt(78+22*sqrt(13)) * ((3+sqrt(13))/2)^n / (4 * sqrt(Pi) * n^(3/2)). - _Vaclav Kotesovec_, Feb 13 2014"
			],
			"example": [
				"a(3)=6 because we have hhh, hH, Hh, hUD, UhD and UDh.",
				"G.f. = 1 + x + 3*x^2 + 6*x^3 + 16*x^4 + 40*x^5 + 109*x^6 + 297*x^7 + ..."
			],
			"maple": [
				"a[0]:=1: a[1]:=1: for n from 2 to 30 do a[n]:=a[n-1]+a[n-2]+add(a[j]*a[n-2-j], j=0..n-2) end do: seq(a[n],n=0..30); G:=((1-z-z^2-sqrt((1+z-z^2)*(1-3*z-z^2)))*1/2)/z^2: Gser:=series(G,z=0,33): seq(coeff(Gser,z,n),n=0..30);"
			],
			"mathematica": [
				"Table[Sum[Binomial[2k,k]/(k+1)Sum[Binomial[n-j,2k]Binomial[n-j-2k,j],{j,0,n/2}],{k,0,n/2}],{n,0,12}] (* _Emanuele Munarini_, May 05 2011 *)"
			],
			"program": [
				"(Maxima) makelist(sum(binomial(2*k,k)/(k+1)*sum(binomial(n-j,2*k)*binomial(n-j-2*k,j),j,0,n/2),k,0,n/2),n,0,12);  // _Emanuele Munarini_, May 05 2011"
			],
			"xref": [
				"Cf. A001006, A128719, A059397, A132276."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Emeric Deutsch_, Mar 30 2007, revised Sep 03 2007",
			"references": 18,
			"revision": 66,
			"time": "2021-11-30T11:07:18-05:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}