{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A095802",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 95802,
			"data": "1,-2,9,3,-6,25,-4,15,-10,49,5,-12,35,-14,81,-6,21,-20,63,-18,121,7,-18,45,-28,99,-22,169,-8,27,-30,77,-36,143,-26,225,9,-24,55,-42,117,-44,195,-30,289,-10,33,-40,91,-54,165,-52,255,-34,361,11,-30,65,-56,135,-66,221,-60,323,-38,441,-12,39,-50,105,-72,187,-78,285,-68,399,-42,529",
			"name": "Upper right triangular matrix T^2, where T(i,j) = (-1)^i*(1-2*i) for 1 \u003c= i \u003c= j.",
			"comment": [
				"Equivalently, (lower left) triangle M^2 = transpose(T)^2. The following description refers to the lower triangular version, but OEIS's \"TABL\" link displays the values more appropriately as an upper right triangle. - _M. F. Hasler_, Apr 18 2009",
				"For n rows, use matrices in each row from the sequence 1, -3, 5, -7, ... (filling in with zeros except for the n-th row). Let the matrix = M, then square and delete the zeros. For example, the 3-row generator would be [1 0 0 / 1 -3 0 / 1 -3 5] = M. The nonzero elements of M^2 give the first 6 terms of the sequence."
			],
			"formula": [
				"Diagonal elements are the odd squares: a(k(k+1)/2)=(2k+1)^2. First element in row k is (-1)^k*k. - _M. F. Hasler_, Apr 18 2009"
			],
			"example": [
				"The matrix",
				"  [  1   0   0   0 ...]",
				"  [  1  -3   0   0 ...]",
				"  [  1  -3   5   0 ...]",
				"  [  1  -3   5  -7 ...]",
				"squared yields",
				"  [ +1   0   0   0 ...]",
				"  [ -2  +9   0   0 ...]",
				"  [ +3  -6  25   0 ...]",
				"  [ -4  15 -10  49 ...]; the lower left triangle gives this sequence: 1; -2, 9; 3, -6, 25; ..."
			],
			"program": [
				"(PARI) T=matrix(12,12,i,j,if(j\u003e=i,(-1)^i*(1-2*i)))^2; concat(vector(#T,i,vecextract(T[,i],2^i-1))) \\\\ _M. F. Hasler_, Apr 18 2009"
			],
			"xref": [
				"Row sums with signs as shown = A002412, Hexagonal pyramidal numbers: (1, 7, 22, 50, 95, ...).",
				"Cf. A002412."
			],
			"keyword": "sign,tabl",
			"offset": "1,2",
			"author": "_Gary W. Adamson_, Jun 07 2004",
			"ext": [
				"Edited and extended by _M. F. Hasler_, Apr 18 2009"
			],
			"references": 1,
			"revision": 19,
			"time": "2020-01-09T03:57:01-05:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}