{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A153151",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 153151,
			"data": "0,1,3,2,7,4,5,6,15,8,9,10,11,12,13,14,31,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,63,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,127,64,65,66,67,68,69",
			"name": "Rotated binary decrementing: For n\u003c2 a(n) = n, if n=2^k, a(n) = 2*n-1, otherwise a(n) = n-1.",
			"comment": [
				"Without the initial 0, a(n) is the lexicographically minimal sequence of distinct positive integers such that all values of a(n) mod n are distinct and nonnegative. - _Ivan Neretin_, Apr 27 2015",
				"A002487(n)/A002487(n+1), n \u003e 0, runs through all the reduced nonnegative rationals exactly once. A002487 is the Stern's sequence. Permutation from denominators (A002487(n+1))",
				"1   2 1   3 2 3 1   4  3  5  2  5  3  4  1",
				"where labels  are",
				"1   2 3   4 5 6 7   8  9 10 11 12 13 14 15",
				"to numerators (A002487(n))",
				"1   1 2   1 3 2 3   1  4  3  5  2  5  3  4",
				"where changed labels  are",
				"1   3 2   7 4 5 6  15  8  9 10 11 12 13 14",
				"Thus, b(n) = A002487(n+1), b(a(n)) = A002487(n), n\u003e0. - _Yosu Yurramendi_, Jul 07 2016"
			],
			"link": [
				"A. Karttunen, \u003ca href=\"/A153151/b153151.txt\"\u003eTable of n, a(n) for n = 0..2047\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A059893(A153141(A059893(n))) = A059894(A153142(A059894(n)))."
			],
			"maple": [
				"a := n -\u003e if n \u003c 2 then n elif convert(convert(n, base, 2), `+`) = 1 then 2*n-1 else n-1 fi: seq(a(n), n=0..70); # _Peter Luschny_, Jul 16 2016"
			],
			"mathematica": [
				"Table[Which[n \u003c 2, n, IntegerQ[Log[2, n]], 2 n - 1, True, n - 1], {n, 0, 70}] (* _Michael De Vlieger_, Apr 27 2015 *)"
			],
			"program": [
				"(MIT Scheme:) (define (A153151 n) (cond ((\u003c n 2) n) ((pow2? n) (- (* 2 n) 1)) (else (- n 1))))",
				"(define (pow2? n) (and (\u003e n 0) (zero? (A004198bi n (- n 1)))))",
				"(Python)",
				"def ok(n): return n\u0026(n - 1)==0",
				"def a(n): return n if n\u003c2 else 2*n - 1 if ok(n) else n - 1 # _Indranil Ghosh_, Jun 09 2017",
				"(R)",
				"nmax \u003c- 126 # by choice",
				"a \u003c- c(1,3,2)",
				"for(n in 3:nmax) a[n+1] \u003c- n",
				"for(m in 0:floor(log2(nmax))) a[2^m] \u003c- 2^(m+1) - 1",
				"a \u003c- c(0, a)",
				"# _Yosu Yurramendi_, Sep 05 2020"
			],
			"xref": [
				"Inverse: A153152.",
				"Cf. A059893, A059894, A153141, A153142."
			],
			"keyword": "nonn,base",
			"offset": "0,3",
			"author": "_Antti Karttunen_, Dec 20 2008",
			"references": 8,
			"revision": 40,
			"time": "2020-10-05T12:49:49-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}