{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A054655",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 54655,
			"data": "1,1,-1,1,-5,6,1,-12,47,-60,1,-22,179,-638,840,1,-35,485,-3325,11274,-15120,1,-51,1075,-11985,74524,-245004,332640,1,-70,2086,-34300,336049,-1961470,6314664,-8648640,1,-92,3682,-83720,1182769",
			"name": "Triangle T(n,k) giving coefficients in expansion of n!*binomial(x-n,n) in powers of x.",
			"link": [
				"Robert Israel, \u003ca href=\"/A054655/b054655.txt\"\u003eTable of n, a(n) for n = 0..10010\u003c/a\u003e (rows 0 to 140, flattened)."
			],
			"formula": [
				"n!*binomial(x-n, n) = Sum_{k=0..n} T(n, k)*x^(n-k).",
				"From _Robert Israel_, Jul 12 2016: (Start)",
				"G.f.: Sum_{n\u003e=0} Sum_{k=0..n} T(n,k)*x^n*y^k  = hypergeom([1, -1/(2*y), (1/2)*(-1+y)/y], [-1/y], -4*x*y).",
				"E.g.f.: Sum_{n\u003e=0} Sum_{k=0..n} T(n,k)*x^n*y^k/n! = (1+4*x*y)^(-1/2)*((1+sqrt(1+4*x*y))/2)^(1+1/y). (End)"
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  1,  -1;",
				"  1,  -5,    6;",
				"  1, -12,   47,    -60;",
				"  1, -22,  179,   -638,    840;",
				"  1, -35,  485,  -3325,  11274,   -15120;",
				"  1, -51, 1075, -11985,  74524,  -245004,  332640;",
				"  1, -70, 2086, -34300, 336049, -1961470, 6314664, -8648640;",
				"  ..."
			],
			"maple": [
				"a054655_row := proc(n) local k; seq(coeff(expand((-1)^n*pochhammer (n-x,n)),x,n-k),k=0..n) end: # _Peter Luschny_, Nov 28 2010"
			],
			"mathematica": [
				"row[n_] := Table[ Coefficient[(-1)^n*Pochhammer[n - x, n], x, n - k], {k, 0, n}]; A054655 = Flatten[ Table[ row[n], {n, 0, 8}]] (* _Jean-François Alcover_, Apr 06 2012, after Maple *)"
			],
			"program": [
				"(PARI) T(n,k)=polcoef(n!*binomial(x-n,n), n-k);"
			],
			"xref": [
				"Cf. A000407, A054649, A054651, A054654, A008276."
			],
			"keyword": "sign,tabl,easy,nice",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_, Apr 18 2000",
			"references": 6,
			"revision": 34,
			"time": "2021-09-24T13:42:46-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}