{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A089942",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 89942,
			"data": "1,0,1,1,1,1,1,3,2,1,3,6,6,3,1,6,15,15,10,4,1,15,36,40,29,15,5,1,36,91,105,84,49,21,6,1,91,232,280,238,154,76,28,7,1,232,603,750,672,468,258,111,36,8,1,603,1585,2025,1890,1398,837,405,155,45,9,1,1585,4213,5500",
			"name": "Inverse binomial matrix applied to A039599.",
			"comment": [
				"Reverse of A071947 - related to lattice paths. First column is A005043.",
				"Triangle T(n,k), 0 \u003c= k \u003c= n, defined by: T(0,0)=1, T(n,k)=0 if k \u003c 0 or if k \u003e n, T(n,0) = T(n-1,1), T(n,k) = T(n-1,k-1) + T(n-1,k) + T(n-1,k+1) for k \u003e= 1. - _Philippe Deléham_, Feb 27 2007",
				"This triangle belongs to the family of triangles defined by: T(0,0)=1, T(n,k)=0 if k \u003c 0 or if k \u003e n, T(n,0) = x*T(n-1,0) + T(n-1,1), T(n,k) = T(n-1,k-1) + y*T(n-1,k) + T(n-1,k+1) for k \u003e= 1. Other triangles arise from choosing different values for (x,y): (0,0) -\u003e A053121; (0,1) -\u003e A089942; (0,2) -\u003e A126093; (0,3) -\u003e A126970; (1,0)-\u003e A061554; (1,1) -\u003e A064189; (1,2) -\u003e A039599; (1,3) -\u003e A110877; (1,4) -\u003e A124576; (2,0) -\u003e A126075; (2,1) -\u003e A038622; (2,2) -\u003e A039598; (2,3) -\u003e A124733; (2,4) -\u003e A124575; (3,0) -\u003e A126953; (3,1) -\u003e A126954; (3,2) -\u003e A111418; (3,3) -\u003e A091965; (3,4) -\u003e A124574; (4,3) -\u003e A126791; (4,4) -\u003e A052179; (4,5) -\u003e A126331; (5,5) -\u003e A125906. - _Philippe Deléham_, Sep 25 2007",
				"Riordan array (f(x),x*g(x)), where f(x)is the o.g.f. of A005043 and g(x)is the o.g.f. of A001006. - _Philippe Deléham_, Nov 22 2009",
				"Riordan array ((1+x-sqrt(1-2x-3x^2))/(2x(1+x)), (1-x-sqrt(1-2x-3x^2))/(2x)). Inverse of Riordan array ((1+x)/(1+x+x^2),x/(1+x+x^2)). E.g.f. of column k is exp(x)*(Bessel_I(k,2x)-Bessel_I(k+1,2x)).",
				"Diagonal sums are A187306.",
				"Simultaneous equations using the first n rows solve for diagonal lengths of odd N = (2n+1) regular polygons, with constants c^0, c^1, c^2,...; where c = 1 + 2*cos( 2*Pi/N) = sin(3*Pi/N)/sin(Pi/N) = the third longest diagonal of N\u003e5.  By way of example, take the first 4 rows relating to the 9-gon (nonagon), N=(2*4 + 1), with c = 1 + 2*cos(2*Pi/9) = 2.5320888.... The simultaneous equations are (1,0,0,0) = 1; (0,1,0,0) = c; (1,1,1,0) = c^2, (1,3,2,1) = c^3. The answers are 1, 2.532..., 2.879..., and 1.879...; the four distinct diagonal lengths of the 9-gon (nonagon) with edge = 1. - _Gary W. Adamson_, Sep 07 2011"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A089942/b089942.txt\"\u003eTable of n, a(n) for the first 50 rows, flattened\u003c/a\u003e",
				"P. Barry and A. Hennessy, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL15/Barry1/barry202.html\"\u003eFour-term Recurrences, Orthogonal Polynomials and Riordan Arrays\u003c/a\u003e, Journal of Integer Sequences, 2012, article 12.4.2. - From _N. J. A. Sloane_, Sep 21 2012",
				"E. Deutsch, L. Ferrari and S. Rinaldi, \u003ca href=\"http://dx.doi.org/10.1016/j.aam.2004.05.002\"\u003eProduction Matrices\u003c/a\u003e, Advances in Applied Mathematics, 34 (2005) pp. 101-122.",
				"D. Merlini, D. G. Rogers, R. Sprugnoli and M. C. Verri, \u003ca href=\"http://dx.doi.org/10.4153/CJM-1997-015-x\"\u003eOn some alternative characterizations of Riordan arrays\u003c/a\u003e, Canad J. Math., 49 (1997), 301-320.",
				"Sun, Yidong; Ma, Luping \u003ca href=\"https://doi.org/10.1016/j.ejc.2014.01.004\"\u003eMinors of a class of Riordan arrays related to weighted partial Motzkin paths\u003c/a\u003e.  Eur. J. Comb. 39, 157-169 (2014) Table 2.2"
			],
			"formula": [
				"G.f.: (1+z-q)/[(1+z)(2z-t+tz+tq)], where q = sqrt(1-2z-3z^2).",
				"Sum_{k\u003e=0} T(m,k)*T(n,k) = T(m+n,0) = A005043(m+n). - _Philippe Deléham_, Mar 22 2007",
				"Sum_{k=0..n} T(n,k)*(2k+1) = 3^n. - _Philippe Deléham_, Mar 22 2007",
				"Sum_{k=0..n} T(n,k)*2^k = A112657(n). - _Philippe Deléham_, Apr 01 2007",
				"T(n,2k) + T(n,2k+1) = A109195(n,k). - _Philippe Deléham_, Nov 11 2008",
				"T(n,k) = GegenbauerC(n-k,-n+1,-1/2) - GegenbauerC(n-k-1,-n+1,-1/2) for 1 \u003c= k \u003c= n. - _Peter Luschny_, May 12 2016"
			],
			"example": [
				"Triangle begins",
				"   1,",
				"   0,   1,",
				"   1,   1,   1,",
				"   1,   3,   2,   1,",
				"   3,   6,   6,   3,   1,",
				"   6,  15,  15,  10,   4,  1,",
				"  15,  36,  40,  29,  15,  5,  1,",
				"  36,  91, 105,  84,  49, 21,  6, 1,",
				"  91, 232, 280, 238, 154, 76, 28, 7, 1",
				"Production matrix is",
				"  0, 1,",
				"  1, 1, 1,",
				"  0, 1, 1, 1,",
				"  0, 0, 1, 1, 1,",
				"  0, 0, 0, 1, 1, 1,",
				"  0, 0, 0, 0, 1, 1, 1,",
				"  0, 0, 0, 0, 0, 1, 1, 1,",
				"  0, 0, 0, 0, 0, 0, 1, 1, 1,",
				"  0, 0, 0, 0, 0, 0, 0, 1, 1, 1"
			],
			"maple": [
				"T:= (n,k) -\u003e simplify(GegenbauerC(n-k,-n+1,-1/2)-GegenbauerC(n-k-1,-n+1,-1/2)): for n from 1 to 9 do seq(T(n,k), k=1..n) od; # _Peter Luschny_, May 12 2016",
				"# Or by recurrence:",
				"T := proc(n, k) option remember;",
				"if n = k then 1 elif k \u003c 0 or n \u003c 0 or k \u003e n then 0",
				"elif k = 0 then T(n-1, 1) else T(n-1, k-1) + T(n-1, k) + T(n-1, k+1) fi end:",
				"for n from 0 to 9 do seq(T(n, k), k = 0..n) od; # _Peter Luschny_, May 25 2021"
			],
			"mathematica": [
				"T[n_, k_] := GegenbauerC[n - k, -n + 1, -1/2] - GegenbauerC[n - k - 1, -n + 1, -1/2]; Table[T[n, k], {n,1,10}, {k,1,n}] // Flatten (* _G. C. Greubel_, Feb 28 2017 *)"
			],
			"xref": [
				"Row sums give A002426 (central trinomial coefficients)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Paul Barry_, Nov 16 2003",
			"ext": [
				"Edited by _Emeric Deutsch_, Mar 04 2004"
			],
			"references": 32,
			"revision": 45,
			"time": "2021-05-25T15:50:11-04:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}