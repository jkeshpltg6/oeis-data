{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A300751",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 300751,
			"data": "1,2,2,1,1,1,1,1,2,3,2,2,2,4,2,1,5,5,2,1,2,2,2,1,4,5,2,1,4,7,1,2,5,3,2,1,3,6,5,2,8,6,1,3,5,6,2,2,4,8,5,4,2,4,3,2,6,4,5,2,1,6,4,1,8,9,6,2,3,3,1,3,7,9,5,5,4,7,1,1",
			"name": "Number of ways to write n as x^2 + y^2 + z^2 + w^2 with x + 3*y + 5*z a positive square, where x,y,z,w are nonnegative integers such that 2*x or y or z is a square.",
			"comment": [
				"Conjecture: a(n) \u003e 0 for all n = 1,2,3,..., and a(n) = 1 only for n = 16^k*m with k = 0,1,2,... and m = 0, 1, 4, 5, 6, 7, 8, 20, 24, 28, 31, 36, 43, 61, 71, 79, 100, 116, 157, 188, 200, 344, 351, 388, 632.",
				"This is stronger than the author's 1-3-5 conjecture in A271518. See also A300752 for a similar conjecture stronger than the 1-3-5 conjecture.",
				"a(n) \u003e 0 for all n = 1..3*10^6. - _Zhi-Wei Sun_, Oct 06 2020"
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A300751/b300751.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2016.11.008\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, J. Number Theory 175(2017), 167-190.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1701.05868\"\u003eRestricted sums of four squares\u003c/a\u003e, arXiv:1701.05868 [math.NT], 2017-2018."
			],
			"example": [
				"a(8) = 1 since 8 = 0^2 + 2^2 + 2^2 + 0^2 with 2*0 = 0^2 and 0 + 3*2 + 5*2 = 4^2.",
				"a(61) = 1 since 61 = 0^2 + 0^2 + 5^2 + 6^2 with 0 = 0^2 and 0 + 3*0 + 5*5 = 5^2.",
				"a(79) = 1 since 79 = 5^2 + 2^2 + 1^2 + 7^2 with 1 = 1^2 and 5 + 3*2 + 5*1 = 4^2.",
				"a(188) = 1 since 188 = 7^2 + 9^2 + 3^2 + 7^2 with 9 = 3^2 and 7 + 3*9 + 5*3 = 7^2.",
				"a(200) = 0 since 200 = 6^2 + 10^2 + 0^2 + 8^2 with 0 = 0^2 and 6 + 3*10 + 5*0 = 6^2.",
				"a(632) = 1 since 632 = 6^2 + 16^2 + 18^2 + 4^2 with 16 = 4^2 and 6 + 3*16 + 5*18 = 12^2.",
				"a(808) = 3 since 808 = 8^2 + 2^2 + 26^2 + 8^2 = 8^2 + 22^2 + 14^2 + 8^2 = 18^2 + 12^2 + 18^2 + 4^2  with 2*8 = 4^2, 2*18 = 6^2 and 8 + 3*2 + 5*26 = 8 + 3*22 + 5*14 = 18 + 3*12 + 5*18 = 12^2."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]];",
				"tab={};Do[r=0;Do[If[(SQ[2(m^2-3y-5z)]||SQ[y]||SQ[z])\u0026\u0026SQ[n-(m^2-3y-5z)^2-y^2-z^2],r=r+1],{m,1,(35n)^(1/4)},{y,0,Min[m^2/3,Sqrt[n]]},{z,0,Min[(m^2-3y)/5,Sqrt[n-y^2]]}];tab=Append[tab,r],{n,1,80}];Print[tab]"
			],
			"xref": [
				"Cf. A000118, A000290, A271518, A281976, A282542, A300666, A300667, A300708, A300712, A300752."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Zhi-Wei Sun_, Mar 11 2018",
			"references": 11,
			"revision": 14,
			"time": "2020-10-06T12:23:02-04:00",
			"created": "2018-03-12T11:59:16-04:00"
		}
	]
}