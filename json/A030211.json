{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A030211",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 30211,
			"data": "1,-4,-2,24,-11,-44,22,8,50,44,-96,-56,-121,152,198,-160,176,-48,-162,-88,-198,52,22,528,233,-200,-242,88,-176,-668,550,-264,-44,188,224,728,154,484,-1056,-656,-311,236,-100,-792,714,528,640,-88,-478,484,1566,-968,192,-780,-1994,648,-942",
			"name": "Expansion of q^(-1/2) * (eta(q) * eta(q^2))^4 in powers of q.",
			"comment": [
				"This is Glaisher's P(n). - _N. J. A. Sloane_, Nov 24 2018",
				"Number 16 of the 74 eta-quotients listed in Table I of Martin (1996)."
			],
			"reference": [
				"J. W. L. Glaisher, On the representations of a number as a sum of four squares, and on some allied arithmetical functions, Quarterly Journal of Pure and Applied Mathematics, 36 (1905), 305-358. See p. 340.",
				"Glaisher, J. W. L. (1906). The arithmetical functions P(m), Q(m), Omega{m). Quart. J. Math, 37, 36-48."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A030211/b030211.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"M. Boylan, \u003ca href=\"http://dx.doi.org/10.1016/S0022-314X(02)00037-9\"\u003eExceptional congruences for the coefficients of certain eta-product newforms\u003c/a\u003e, J. Number Theory 98 (2003), no. 2, 377-389. MR1955423 (2003k:11071)",
				"J. W. L. Glaisher, \u003ca href=\"https://books.google.com/books?id=bLs9AQAAMAAJ\u0026amp;pg=RA1-PA1\"\u003eOn the representations of a number as the sum of two, four, six, eight, ten, and twelve squares\u003c/a\u003e, Quart. J. Math. 38 (1907), 1-62 (see p. 5).",
				"M. Koike, \u003ca href=\"http://projecteuclid.org/euclid.nmj/1118787564\"\u003eOn McKay's conjecture\u003c/a\u003e, Nagoya Math. J., 95 (1984), 85-89.",
				"Y. Martin, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-96-01743-6\"\u003eMultiplicative eta-quotients\u003c/a\u003e, Trans. Amer. Math. Soc. 348 (1996), no. 12, 4825-4856, see page 4852 Table I.",
				"Michael Somos, \u003ca href=\"/A030203/a030203.txt\"\u003eIndex to Yves Martin's list of 74 multiplicative eta-quotients and their A-numbers\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/AperyNumber.html\"\u003eApery Number.\u003c/a\u003e",
				"\u003ca href=\"/index/Ge#Glaisher\"\u003eIndex entries for sequences mentioned by Glaisher\u003c/a\u003e"
			],
			"formula": [
				"G.f.: (Product_{k\u003e0} (1 - x^k) * (1 - x^(2*k)))^4.",
				"Euler transform of period 2 sequence [ -4, -8, ...]. - _Michael Somos_, Apr 14 2004",
				"Given g.f. A(x), then B(q) = q * A(q^2) satisfies 0 = f(B(q), B(q^2), B(q^3), B(q^6)) where f(u1, u2, u3, u6) = (81*u6*u3 + u1*u2) * (u2*u3 + u1*u6) + 30 * u1*u2*u3*u6 - 256 * u2^2*u6^2 - 5 * u2^2*u3^2 - 5 * u1^2*u6^2 - u1^2*u3^2. - _Michael Somos_, Mar 08 2006",
				"Given A = A0 + A1 + A2 + A3 is the 4-section, then 0 = 8 * A0*A2 * (A0^2 + A2^2) + (A1^2 - A3^2) * (A0^2 - A2^2). - _Michael Somos_, Mar 08 2006",
				"a(n) = b(2*n + 1) where b(n) is multiplicative with b(2^e) = 0^e, b(p^e) = b(p) * b(p^(e-1)) - p^3 * b(p^(e-2)) if p\u003e2. - _Michael Somos_, Mar 08 2006",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (8 t)) = 64 (t/i)^4 f(t) where q = exp(2 Pi i t). - _Michael Somos_, May 28 2013",
				"a(n) = (-1)^n * A134461(n). Convolution square of A002171.",
				"G.f.: exp(4*Sum_{k\u003e=1} (sigma(2*k) - 4*sigma(k))*x^k/k). - _Ilya Gutkovskiy_, Sep 19 2018"
			],
			"example": [
				"G.f. = 1 - 4*x - 2*x^2 + 24*x^3 - 11*x^4 - 44*x^5 + 22*x^6 + 8*x^7 + 50*x^8 + ...",
				"G.f. = q - 4*q^3 - 2*q^5 + 24*q^7 - 11*q^9 - 44*q^11 + 22*q^13 + 8*q^15 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (QPochhammer[ x] QPochhammer[ x^2])^4, {x, 0, n}]; (* _Michael Somos_, May 28 2013 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, polcoeff( (eta(x + x * O(x^n)) * eta(x^2 + x * O(x^n)))^4, n))}; /* _Michael Somos_, Apr 14 2004 */",
				"(PARI) q='q+O('q^99); Vec((eta(q)*eta(q^2))^4) \\\\ _Altug Alkan_, Sep 19 2018",
				"(Sage) CuspForms( Gamma0(8), 4, prec=115).0; # _Michael Somos_, May 28 2013",
				"(MAGMA) Basis( CuspForms( Gamma0(8), 4), 115) [1]; /* _Michael Somos_, May 27 2014 */"
			],
			"xref": [
				"Cf. A002171, A134461 (the same except for signs)."
			],
			"keyword": "sign,look",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_.",
			"references": 8,
			"revision": 51,
			"time": "2018-11-26T09:56:52-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}