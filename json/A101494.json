{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A101494",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 101494,
			"data": "1,1,1,2,1,1,4,3,1,1,9,8,4,1,1,23,23,13,5,1,1,66,73,44,19,6,1,1,210,253,162,73,26,7,1,1,733,948,643,302,111,34,8,1,1,2781,3817,2724,1337,506,159,43,9,1,1,11378,16433,12259,6266,2457,788,218,53,10,1,1,49864,75295",
			"name": "Triangle, read by rows, where T(n,k) = Sum_{j=0..n-k-1} C(j+k,j)*T(n-1,j+k) for n\u003ek\u003e=0 with T(n,n)=1.",
			"comment": [
				"Column 0 equals row sums (A026898) shift right.",
				"T(n,k) is the number of m-tuples of nonnegative integers satisfying these two criteria: (i) there are exactly k 0’s, and (ii) the remaining m-k elements are positive integers less than or equal to n-m. - _Mathew Englander_, Feb 25 2021"
			],
			"link": [
				"Muniru A Asiru, \u003ca href=\"/A101494/b101494.txt\"\u003eRows n=0..150 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,0) = A026898(n-1).",
				"T(n,k) = Sum_{j=0..n-k} binomial(j+k,j)*(n-k-j)^j. - _Vladeta Jovovic_, Sep 07 2006",
				"G.f.: A(x,y) = Sum_{n\u003e=0} Sum_{k\u003e=0} x^(n+k)*y^k / (1 - n*x)^(k+1). - _Paul D. Hanna_, Mar 06 2013",
				"From _Mathew Englander_, Feb 25 2021: (Start)",
				"G.f. of row n: Sum_{i=0..n} (x+n-i)^i.",
				"T(n,k) = Sum_{j=k..n} A089246(j,k).",
				"Antidiagonal sums: Sum_{j = 0..n} Sum_{i = j..floor((n+j)/2)} binomial(i,j)*(n+j-2*i)^j. (End)"
			],
			"example": [
				"4th row sum = 23 = (5-0)^0+(5-1)^1+(5-2)^2+(5-3)^3+(5-4)^4.",
				"5th row sum = 66 = (6-0)^0+(6-1)^1+(6-2)^2+(6-3)^3+(6-4)^4+(6-5)^5.",
				"T(6,0) = 66 = 1*23 + 1*23 + 1*13 + 1*5 + 1*1 + 1*1.",
				"T(6,1) = 73 = 1*23 + 2*13 + 3*5 + 4*1 + 5*1.",
				"T(6,2) = 44 = 1*13 + 3*5 + 6*1 + 10*1.",
				"Rows begin:",
				"1;",
				"1, 1;",
				"2, 1, 1;",
				"4, 3, 1, 1;",
				"9, 8, 4, 1, 1;",
				"23, 23, 13, 5, 1, 1;",
				"66, 73, 44, 19, 6, 1, 1;",
				"210, 253, 162, 73, 26, 7, 1, 1;",
				"733, 948, 643, 302, 111, 34, 8, 1, 1;",
				"2781, 3817, 2724, 1337, 506, 159, 43, 9, 1, 1;",
				"11378, 16433, 12259, 6266, 2457, 788, 218, 53, 10, 1, 1;",
				"49864, 75295, 58423, 30953, 12558, 4147, 1163, 289, 64, 11, 1, 1;",
				"232769, 365600, 293902, 160823, 67259, 22878, 6574, 1647, 373, 76, 12, 1, 1; ..."
			],
			"program": [
				"(PARI) T(n,k)=if(n\u003ck || k\u003c0,0,if(n==k,1, sum(j=0,n-k-1,binomial(j+k,j)*T(n-1,j+k));))",
				"(PARI) T(n,k)=polcoeff(sum(m=0,n-k, x^m/(1-m*x +x*O(x^(n-k)))^(k+1)),n-k)",
				"for(n=0,12,for(k=0,n,print1(T(n,k),\", \"));print()) \\\\ _Paul D. Hanna_, Mar 06 2013",
				"(GAP) Flat(List([0..10],n-\u003eList([0..n],k-\u003eSum([0..n-k],j-\u003eBinomial(j+k,j)*(n-k-j)^j)))); # _Muniru A Asiru_, Mar 07 2019"
			],
			"xref": [
				"Cf. A101495, A026898, A089246 (first differences by column), A304357 (antidiagonal sums, empirically), A034856 (fourth diagonal)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Paul D. Hanna_, Jan 21 2005",
			"references": 9,
			"revision": 35,
			"time": "2021-02-25T10:55:54-05:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}