{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A234319",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 234319,
			"data": "0,3,25,-3,-4,-5,-6,-7,-8,-9,-10,-11,-12,-13,-14,-15,-16,-17,-18,-19,-20,-21,-22,-23,-24,-25,-26,-27,-28,-29,-30,-31,-32,-33,-34,-35,-36,-37,-38,-39,-40,-41,-42,-43,-44,-45,-46,-47,-48,-49,-50,-51,-52,-53,-54",
			"name": "Smallest sum of n-th powers of k+1 consecutive positive integers that equals the sum of n-th powers of the next k consecutive integers, or -n if none.",
			"comment": [
				"a(n) is the smallest solution to m^n + (m+1)^n + ... + (m+k)^n = (m+k+1)^n + (m+k+2)^n + ... + (m+2*k)^n, or -n if no solution.",
				"In 1879 Dostor gave all solutions for n = 2. In particular, a(2) = 25.",
				"In 1906 Collignon proved that no solution exists for n = 3 and 4, so a(3) = -3 and a(4) = -4.",
				"In 2013 Felten and Müller-Stach claimed to prove that no solution exists when n \u003e 2, so if their proof is correct, a(n) = -n for n \u003e= 3."
			],
			"reference": [
				"Edouard Collignon, Note sur la résolution en entiers de m^2 + (m-r)^2 + ... + (m-kr)^2 = (m+r)^2 + ... + (m+kr)^2, Sphinx-Oedipe, 1 (1906-1907), 129-133."
			],
			"link": [
				"L. E. Dickson, \u003ca href=\"http://books.google.com/books?id=9LQqAwAAQBAJ\u0026amp;pg=PA564\u0026amp;lpg=PA564\u0026amp;dq=dickson+%22noted+that+there+is+no+positive+integral+solution+of%22%22\u0026amp;source=bl\u0026amp;ots=rZeMQFzLta\u0026amp;sig=FOsVXdiPjunRzxcDFP819lUoYEY\u0026amp;hl=en\u0026amp;sa=X\u0026amp;ei=2qGlU5HWA6ic8gGYvYCABQ\u0026amp;ved=0CB8Q6AEwAA#v=onepage\u0026amp;q=dickson%20%22noted%20that%20there%20is%20no%20positive%20integral%20solution%20of%22%22\u0026amp;f=false\"\u003eHistory of the Theory of Numbers, II\u003c/a\u003e, p. 564.",
				"Georges Dostor, \u003ca href=\"https://www.digitale-sammlungen.de/en/view/bsb11390652?page=372\"\u003eQuestion sur les nombres\u003c/a\u003e, Archiv der Mathematik und Physik, 64 (1879), 350-352.",
				"Simon Felten and Stefan Müller-Stach, \u003ca href=\"http://arxiv.org/abs/1312.5943\"\u003eA diophantine equation for sums of consecutive like powers\u003c/a\u003e, arXiv:1312.5943 [math.NT], 2013-2015; Elem. Math., 70 (2015), 117-124. doi: 10.4171/EM/284",
				"Greg Frederickson, \u003ca href=\"http://www.jstor.org/stable/27765930\"\u003eCasting Light on Cube Dissections\u003c/a\u003e, Math. Mag., 82 (2009), 323-331.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,-1)."
			],
			"formula": [
				"a(0) = A059270(0) = A059255(0).",
				"a(1) = A059270(1) = A230718(1).",
				"a(2) = A059255(2) = A230718(2).",
				"a(n) = -n for n \u003e 2.",
				"G.f.: x*(27*x^3-50*x^2+19*x+3) / (x-1)^2. - _Colin Barker_, Apr 23 2014"
			],
			"example": [
				"m^0 + (m+1)^0 + ... + (m+k)^0 = k+1 \u003e k = (m+k+1)^0 + (m+k+2)^0 + ... + (m+2*k)^0 for m \u003e 0, so a(0) = -0 = 0.",
				"1^1 + 2^1 = 3 = 3^1 is minimal for n = 1, so a(1) = 3.",
				"3^2 + 4^2 = 25 = 5^2 is minimal for n = 2, so a(2) = 25."
			],
			"mathematica": [
				"CoefficientList[Series[x*(27*x^3 - 50*x^2 + 19*x + 3)/(x - 1)^2, {x, 0, 50}], x] (* _Wesley Ivan Hurt_, Jun 21 2014 *)"
			],
			"program": [
				"(PARI) Vec(x*(27*x^3-50*x^2+19*x+3)/(x-1)^2 + O(x^100)) \\\\ _Colin Barker_, Apr 23 2014"
			],
			"xref": [
				"Cf. A059255, A059270, A222716, A230718."
			],
			"keyword": "sign,easy",
			"offset": "0,2",
			"author": "_Jonathan Sondow_, Dec 23 2013",
			"references": 2,
			"revision": 58,
			"time": "2021-09-04T14:52:41-04:00",
			"created": "2013-12-28T02:43:19-05:00"
		}
	]
}