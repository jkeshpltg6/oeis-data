{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A349426",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 349426,
			"data": "3,8,30,144,90,840,840,5760,7280,45360,66528,7560,403200,657720,151200,3991680,7064640,2356200,43545600,82285632,34890240,1247400,518918400,1035365760,521080560,43243200,6706022400,14013679680,8034586560,1059458400",
			"name": "Irregular triangle read by rows: T(n,k) is the number of arrangements of n labeled children with exactly k nontrivial rounds; n \u003e= 3, 1 \u003c= k \u003c= floor(n/3).",
			"comment": [
				"A nontrivial round means the same as a ring or circle consisting of more than one child."
			],
			"reference": [
				"R. P. Stanley, Enumerative Combinatorics, Cambridge, Vol. 2, 1999 (Sec. 5.2)"
			],
			"link": [
				"Steven Finch, \u003ca href=\"https://arxiv.org/abs/2111.14487\"\u003eRounds, Color, Parity, Squares\u003c/a\u003e, arXiv:2111.14487 [math.CO], 2021."
			],
			"formula": [
				"E.g.f.: (1 - x)^(-x*t) * exp(-x^2*t)."
			],
			"example": [
				"Triangle starts:",
				"[3]           3;",
				"[4]           8;",
				"[5]          30;",
				"[6]         144,          90;",
				"[7]         840,         840;",
				"[8]        5760,        7280;",
				"[9]       45360,       66528,       7560;",
				"[10]     403200,      657720,     151200;",
				"[11]    3991680,     7064640,    2356200;",
				"[12]   43545600,    82285632,   34890240,    1247400;",
				"[13]  518918400,  1035365760,  521080560,   43243200;",
				"[14] 6706022400, 14013679680, 8034586560, 1059458400;",
				"...",
				"For n = 6, there are 144 ways to make one round and 90 ways to make two rounds."
			],
			"mathematica": [
				"f[k_, n_] := n! SeriesCoefficient[(1 - x)^(-x t) Exp[-x^2 t], {x, 0, n}, {t, 0, k}]",
				"Table[f[k, n], {n, 2, 14}, {k, 1, Floor[n/3]}]"
			],
			"xref": [
				"Row sums give A066165 (variant of Stanley's children's game).",
				"Column 1 gives A001048.",
				"Right border element of row n is A166334(n/3) for each n divisible by 3.",
				"Cf. A066166, A349280 (correspond to Stanley's original game)."
			],
			"keyword": "nonn,tabf",
			"offset": "3,1",
			"author": "_Steven Finch_, Nov 17 2021",
			"references": 1,
			"revision": 20,
			"time": "2021-12-10T05:55:14-05:00",
			"created": "2021-11-18T03:58:50-05:00"
		}
	]
}