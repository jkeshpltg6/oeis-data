{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A114656",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 114656,
			"data": "1,2,1,4,6,1,8,24,12,1,16,80,80,20,1,32,240,400,200,30,1,64,672,1680,1400,420,42,1,128,1792,6272,7840,3920,784,56,1,256,4608,21504,37632,28224,9408,1344,72,1,512,11520,69120,161280,169344,84672,20160,2160,90",
			"name": "Triangle read by rows: T(n,k) is the number of double rise-bicolored Dyck paths (double rises come in two colors; also called marked Dyck paths) of semilength n and having k peaks (1 \u003c= k \u003c= n).",
			"comment": [
				"Row sums are the little Schroeder numbers (A001003). Sum_{k=1..n} k*T(n,k) = A047781(n). T(n,k) = (1/2)A114655(n,k).",
				"Triangle T(n,k), 1 \u003c= k \u003c= n, given by [0,2,0,2,0,2,0,2,0,2,0,2,...] DELTA [1,0,1,0,1,0,1,0,1,0,1,0,1,0,...] where DELTA is the operator defined in A084938. - _Philippe Deléham_, Jan 02 2009"
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A114656/b114656.txt\"\u003eTable of n, a(n) for n = 1..11325\u003c/a\u003e (rows 1 \u003c= n \u003c= 150, flattened).",
				"D. Callan, \u003ca href=\"http://www.stat.wisc.edu/~callan/papers/polygon_dissections/\"\u003ePolygon Dissections and Marked Dyck Paths\u003c/a\u003e",
				"Samuele Giraudo, \u003ca href=\"https://arxiv.org/abs/1903.00677\"\u003eTree series and pattern avoidance in syntax trees\u003c/a\u003e, arXiv:1903.00677 [math.CO], 2019."
			],
			"formula": [
				"T(n, k) = 2^(n-k)*binomial(n, k)*binomial(n, k-1)/n.",
				"G.f.: G = G(t, z) satisfies G = z(2G+t)(G+1).",
				"T(n,k) = A001263(n,k)*2^(n-k). - _Philippe Deléham_, Apr 11 2007",
				"G.f.: 1/(1-xy/(1-2x/(1-xy/(1-2x/(1-xy/(1-2x/(1-..... (continued fraction). - _Paul Barry_, Feb 06 2009"
			],
			"example": [
				"T(3,2)=6 because we have (UD)Ub(UD)D, (UD)Ur(UD)D, Ub(UD)D(UD), Ur(UD)D(UD), Ub(UD)(UD)D and Ur(UD)(UD)D, where U=(1,1), D=(1,-1) and b (r) indicates a blue (red) double rise (the peaks are shown between parentheses).",
				"Triangle begins:",
				"   1;",
				"   2,  1;",
				"   4,  6,  1;",
				"   8, 24, 12,  1;",
				"  16, 80, 80, 20,  1;",
				"  ....",
				"Triangle T(n,k), 0 \u003c= k \u003c= n, given by [0,2,0,2,0,2,0,2,...] DELTA [1,0,1,0,1,0,1,0,1,0,...] begins: 1; 0,1; 0,2,1; 0,4,6,1; 0,8,24,12,1; 0,16,80,80,20,1; ... - _Philippe Deléham_, Jan 02 2009"
			],
			"maple": [
				"T:=(n,k)-\u003e2^(n-k)*binomial(n,k)*binomial(n,k-1)/n: for n from 1 to 11 do seq(T(n,k),k=1..n) od; # yields sequence in triangular form"
			],
			"mathematica": [
				"Table[2^(n - k) Binomial[n, k] Binomial[n, k - 1]/n, {n, 10}, {k, n}] // Flatten (* _Michael De Vlieger_, Apr 23 2019 *)"
			],
			"xref": [
				"Cf. A001003, A047781, A114655."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Emeric Deutsch_, Dec 23 2005",
			"references": 4,
			"revision": 20,
			"time": "2019-04-23T17:38:42-04:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}