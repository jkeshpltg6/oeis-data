{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007438",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7438,
			"id": "M1339",
			"data": "1,2,5,7,14,13,27,26,39,38,65,50,90,75,100,100,152,111,189,148,198,185,275,196,310,258,333,294,434,292,495,392,490,440,588,438,702,549,684,584,860,582,945,730,876,803,1127,776,1197,910,1168,1020,1430",
			"name": "Moebius transform of triangular numbers.",
			"comment": [
				"a(n)=|{(x,y):1\u003c=x\u003c=y\u003c=n, gcd(x,y,n)=1}|. E.g. a(4)=7 because of the pairs (1,1), (1,2), (1,3), (1,4), (2,3), (3,3), (3,4). - _Steve Butler_, Apr 18 2006",
				"Partial sums of a(n) give A015631(n). - _Steve Butler_, Apr 18 2006",
				"Equals row sums of triangle A159905. - _Gary W. Adamson_, Apr 25 2009; corrected by _Mats Granvik_, Apr 24 2010"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A007438/b007438.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/transforms.txt\"\u003eTransforms\u003c/a\u003e"
			],
			"formula": [
				"a(n) = (A007434(n)+A000010(n))/2, half the sum of the Mobius transforms of n^2 and n. Dirichlet g.f. (zeta(s-2)+zeta(s-1))/(2*zeta(s)). - _R. J. Mathar_, Feb 09 2011",
				"G.f.: Sum_{n\u003e=1} a(n)*x^n/(1 - x^n) = x/(1 - x)^3. - _Ilya Gutkovskiy_, Apr 25 2017"
			],
			"maple": [
				"with(numtheory):",
				"a:= proc(n) option remember;",
				"       add(mobius(n/d)*d*(d+1)/2, d=divisors(n))",
				"    end:",
				"seq(a(n), n=1..60);  # _Alois P. Heinz_, Feb 09 2011"
			],
			"mathematica": [
				"a[n_] := Sum[MoebiusMu[n/d]*d*(d+1)/2, {d, Divisors[n]}]; Array[a, 60] (* _Jean-François Alcover_, Apr 17 2014 *)"
			],
			"program": [
				"(PARI) a(n) = sumdiv(n, d, moebius(n/d)*d*(d+1)/2); \\\\ _Michel Marcus_, Nov 05 2018"
			],
			"xref": [
				"Cf. A000217.",
				"Cf. A159905. - _Gary W. Adamson_, Apr 25 2009"
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_.",
			"references": 12,
			"revision": 44,
			"time": "2018-11-05T06:32:23-05:00",
			"created": "1994-05-24T03:00:00-04:00"
		}
	]
}