{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A257098",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 257098,
			"data": "1,-1,-1,-1,-1,1,-1,-1,-1,1,-1,1,-1,1,1,-5,-1,1,-1,1,1,1,-1,1,-1,1,-1,1,-1,-1,-1,-7,1,1,1,1,-1,1,1,1,-1,-1,-1,1,1,1,-1,5,-1,1,1,1,-1,1,1,1,1,1,-1,-1,-1,1,1,-21,1,-1,-1,1,1,-1,-1,1,-1,1,1,1,1,-1,-1,5,-5,1,-1,-1,1,1,1,1,-1,-1,1,1,1,1,1,7,-1,1,1,1",
			"name": "From square root of the inverse of Riemann zeta function: form Dirichlet series Sum b(n)/n^x whose square is 1/zeta; sequence gives numerator of b(n).",
			"comment": [
				"Dirichlet g.f. of b(n) = A257098(n)/A046644(n) is (zeta (x))^(-1/2).",
				"Denominator is the same as for Dirichlet g.f. (zeta(x))^(+1/2).",
				"Formula holds for general Dirichlet g.f. zeta(x)^(-1/k) with k = 1, 2, ...",
				"The sequence of rationals a(n)/A046644(n) is the Moebius transform of A046643/A046644 which is multiplicative. This sequence is then also multiplicative. - _Andrew Howroyd_, Aug 08 2018"
			],
			"link": [
				"Wolfgang Hintze, \u003ca href=\"/A257098/b257098.txt\"\u003eTable of n, a(n) for n = 1..500\u003c/a\u003e"
			],
			"formula": [
				"with k = 2;",
				"zeta(x)^(-1/k) = Sum_{n\u003e=1} b(n)/n^x;",
				"c(1,n)=b(n); c(k,n) = Sum_{d|n} c(1,d)*c(k-1,n/d), k\u003e1;",
				"Then solve c(k,n) = mu(n) for b(m);",
				"a(n) = numerator(b(n))."
			],
			"mathematica": [
				"k = 2;",
				"c[1, n_] = b[n];",
				"c[k_, n_] := DivisorSum[n, c[1, #1]*c[k - 1, n/#1] \u0026 ]",
				"nn = 100; eqs = Table[c[k, n]==MoebiusMu[n], {n, 1, nn}];",
				"sol = Solve[Join[{b[1] 1}, eqs], Table[b[i], {i, 1, nn}], Reals];",
				"t = Table[b[n], {n, 1, nn}] /. sol[[1]];",
				"num = Numerator[t] (* A257098 *)",
				"den = Denominator[t] (* A046644 *)"
			],
			"program": [
				"(PARI) \\\\ DirSqrt(v) finds u such that v = v[1]*dirmul(u, u).",
				"DirSqrt(v)={my(n=#v, u=vector(n)); u[1]=1; for(n=2, n, u[n]=(v[n]/v[1] - sumdiv(n, d, if(d\u003e1\u0026\u0026d\u003cn, u[d]*u[n/d], 0)))/2); u}",
				"apply(numerator, DirSqrt(vector(100, n, moebius(n)))) \\\\ _Andrew Howroyd_, Aug 08 2018"
			],
			"xref": [
				"Cf. family zeta^(-1/k): A257098/A046644 (k=2), A257099/A256689 (k=3), A257100/A256691 (k=4), A257101/A256693 (k=5).",
				"Cf. family zeta^(+1/k): A046643/A046644 (k=2), A256688/A256689 (k=3), A256690/A256691 (k=4), A256692/A256693 (k=5)."
			],
			"keyword": "sign,mult",
			"offset": "1,16",
			"author": "_Wolfgang Hintze_, Apr 16 2015",
			"references": 12,
			"revision": 12,
			"time": "2018-08-08T22:36:28-04:00",
			"created": "2015-04-16T15:27:37-04:00"
		}
	]
}