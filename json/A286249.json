{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A286249",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 286249,
			"data": "1,3,2,3,0,4,10,5,0,7,3,0,0,0,11,21,5,8,0,0,16,3,0,0,0,0,0,22,36,14,0,12,0,0,0,29,10,0,8,0,0,0,0,0,37,21,5,0,0,17,0,0,0,0,46,3,0,0,0,0,0,0,0,0,0,56,78,27,19,12,0,23,0,0,0,0,0,67,3,0,0,0,0,0,0,0,0,0,0,0,79,21,5,0,0,0,0,30,0,0,0,0,0,0,92,21,0,8,0,17,0,0,0,0,0,0,0,0,0,106",
			"name": "Triangular table: T(n,k) = 0 if k does not divide n, otherwise T(n,k) = P(A046523(n/k), k), where P is sequence A000027 used as a pairing function N x N -\u003e N. Table is read by rows as T(1,1), T(2,1), T(2,2), etc.",
			"comment": [
				"This sequence packs the values of A046523(n/k) and k (whenever k divides n) to a single term with the pairing function A000027. The two \"components\" can be accessed with functions A002260 \u0026 A004736, which allows us to generate from this sequence (among other things) various sums related to the enumeration of aperiodic necklaces, because Moebius mu (A008683) obtains the same value on any representative of the same prime signature.",
				"For example, we have:",
				"  Sum_{i=A000217(n-1) .. A000217(n)} [a(i) \u003e 0] * mu(A002260(a(i))) * 2^(A004736(a(i))) = A027375(n).",
				"and",
				"  Sum_{i=A000217(n-1) .. A000217(n)} [a(i) \u003e 0] * mu(A002260(a(i))) * 3^(A004736(a(i))) = A054718(n).",
				"Triangle A286247 has the same property."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A286249/b286249.txt\"\u003eTable of n, a(n) for n = 1..10585; the first 145 rows of triangle/antidiagonals of array\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PairingFunction.html\"\u003ePairing Function\u003c/a\u003e"
			],
			"formula": [
				"As a triangle (with n \u003e= 1, 1 \u003c= k \u003c= n):",
				"T(n,k) = 0 if k does not divide n, otherwise T(n,k) = (1/2)*(2 + ((A046523(n/k)+k)^2) - A046523(n/k) - 3*k)."
			],
			"example": [
				"The first fifteen rows of triangle:",
				"   1,",
				"   3,  2,",
				"   3,  0,  4,",
				"  10,  5,  0,  7,",
				"   3,  0,  0,  0, 11,",
				"  21,  5,  8,  0,  0, 16,",
				"   3,  0,  0,  0,  0,  0, 22,",
				"  36, 14,  0, 12,  0,  0,  0, 29,",
				"  10,  0,  8,  0,  0,  0,  0,  0, 37,",
				"  21,  5,  0,  0, 17,  0,  0,  0,  0, 46,",
				"   3,  0,  0,  0,  0,  0,  0,  0,  0,  0, 56,",
				"  78, 27, 19, 12,  0, 23,  0,  0,  0,  0,  0, 67,",
				"   3,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 79,",
				"  21,  5,  0,  0,  0,  0, 30,  0,  0,  0,  0,  0,  0, 92,",
				"  21,  0,  8,  0, 17,  0,  0,  0,  0,  0,  0,  0,  0,  0, 106",
				"  -------------------------------------------------------------",
				"Note how triangle A286247 contains on each row the same numbers in the same \"divisibility-allotted\" positions, but in reverse order."
			],
			"program": [
				"(Scheme)",
				"(define (A286249 n) (A286249tr (A002024 n) (A002260 n)))",
				"(define (A286249tr n k) (if (not (zero? (modulo n k))) 0 (let ((a (A046523 (/ n k))) (b k)) (* (/ 1 2) (+ (expt (+ a b) 2) (- a) (- (* 3 b)) 2)))))",
				"(Python)",
				"from sympy import factorint",
				"import math",
				"def T(n, m): return ((n + m)**2 - n - 3*m + 2)/2",
				"def P(n):",
				"    f = factorint(n)",
				"    return sorted([f[i] for i in f])",
				"def a046523(n):",
				"    x=1",
				"    while True:",
				"        if P(n) == P(x): return x",
				"        else: x+=1",
				"def t(n, k): return 0 if n%k!=0 else T(a046523(n/k), k)",
				"for n in range(1, 21): print [t(n, k) for k in range(1, n + 1)] # _Indranil Ghosh_, May 08 2017"
			],
			"xref": [
				"Transpose: A286248 (triangle reversed).",
				"Cf. A000027, A008683, A027375, A046523, A054718, A054525, A286156, A286247, A286239.",
				"Cf. A000124 (the right edge of the triangle)."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Antti Karttunen_, May 06 2017",
			"references": 4,
			"revision": 26,
			"time": "2019-12-07T12:18:29-05:00",
			"created": "2017-05-06T19:52:01-04:00"
		}
	]
}