{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A229111",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 229111,
			"data": "1,-5,35,-275,2275,-19255,163925,-1385725,11483875,-91781375,688658785,-4581861025,22550427925,8852899375,-2431720493125,47471706909725,-699843878180125,9141002535744625,-111232778205154375,1288777160650004375,-14372445132730778975",
			"name": "Expansion of the g.f. of A053723 in powers of the g.f. of A121591.",
			"comment": [
				"In Verrill (1999) section 2.1, t = (eta(q^5) / eta(q))^6 the g.f. of A121591 and f = eta(q^5)^5 / eta(q) the g.f. of A053723.",
				"Apart from signs, this is one of the Apery-like sequences - see Cross-references. - _Hugo Pfoertner_, Aug 06 2017"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A229111/b229111.txt\"\u003eTable of n, a(n) for n = 1..958\u003c/a\u003e",
				"Amita Malik and Armin Straub, \u003ca href=\"https://doi.org/10.1007/s40993-016-0036-8\"\u003eDivisibility properties of sporadic Apéry-like numbers\u003c/a\u003e, Research in Number Theory, 2016, 2:5.",
				"Ofir Gorodetsky, \u003ca href=\"https://arxiv.org/abs/2102.11839\"\u003eNew representations for all sporadic Apéry-like sequences, with applications to congruences\u003c/a\u003e, arXiv:2102.11839 [math.NT], 2021. See eta p. 3.",
				"L. O'Brien, \u003ca href=\"https://doi.org/10.13140/RG.2.2.33912.03843\"\u003eModular forms and two new integer sequences at level 7\u003c/a\u003e, Massey University, 2016.",
				"H. Verrill, \u003ca href=\"http://webdoc.sub.gwdg.de/ebook/serien/e/mpi_mathematik/1999/26.ps\"\u003eSome Congruences related to modular forms\u003c/a\u003e, Max Planck Institute, 1999."
			],
			"formula": [
				"n^3 * a(n+1) = -(2*n - 1)*(11*n*(n - 1) + 5) * a(n) - 125 * (n - 1)^3 * a(n-1).",
				"a(n*p^k) == (p^3 + Kronecker(p, 5)) * a(n*p^(k-1)) - Kronecker(p, 5) * p^3*a(n*p^(-2)) (mod p^k). [Verrill, 1999]",
				"a(n) = Sum_{k=0..n-1} (-1)^k * binomial(n-1,k)^3 * binomial(5*k-(n-1),3*(n-1)). - _Seiichi Manyama_, Sep 02 2020"
			],
			"example": [
				"G.f. = x - 5*x^2 + 35*x^3 - 275*x^4 + 2275*x^5 - 19255*x^6 + 163925*x^7 + ..."
			],
			"mathematica": [
				"a[n_] := a[n] = Switch[n, 1, 1, 2, -5, _, (1/(n-1)^3) ((1-2(n-1)) (11(n-2) (n-1)+5) a[n-1] - 125 (n-2)^3 a[n-2])];",
				"a /@ Range[21] (* _Jean-François Alcover_, Jan 13 2020 *)"
			],
			"program": [
				"(PARI) {a(n) = my(m = n-1); if( n\u003c1, 0, if( n\u003c3, [1, -5][n], -( (5*(m - 1))^3*a(n-2) + (2*m - 1)*(11*(m^2 - m) +5)*a(n-1) )/ m^3))};",
				"(PARI) {a(n) = sum(k=0, n-1, (-1)^k*binomial(n-1, k)^3*binomial(5*k-(n-1), 3*(n-1)))} \\\\ _Seiichi Manyama_, Sep 02 2020"
			],
			"xref": [
				"Cf. A053723, A109064, A121591.",
				"The Apéry-like numbers [or Apéry-like sequences, Apery-like numbers, Apery-like sequences] include A000172, A000984, A002893, A002895, A005258, A005259, A005260, A006077, A036917, A063007, A081085, A093388, A125143 (apart from signs), A143003, A143007, A143413, A143414, A143415, A143583, A183204, A214262, A219692, A226535, A227216, A227454, A229111 (apart from signs), A260667, A260832, A262177, A264541, A264542, A279619, A290575, A290576. (The term \"Apery-like\" is not well-defined.)",
				"For primes that do not divide the terms of the sequences A000172, A005258, A002893, A081085, A006077, A093388, A125143, A229111, A002895, A290575, A290576, A005259 see A260793, A291275-A291284 and A133370 respectively."
			],
			"keyword": "sign",
			"offset": "1,2",
			"author": "_Michael Somos_, Sep 30 2013",
			"references": 47,
			"revision": 58,
			"time": "2021-02-25T07:26:58-05:00",
			"created": "2013-09-30T13:18:38-04:00"
		}
	]
}