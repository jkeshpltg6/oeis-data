{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A292932",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 292932,
			"data": "1,1,4,20,138,1182,12166,146050,2003882,30930734,530477310,10007736906,205965058162,4592120925862,110259944144486,2836517343551714,77836238876829882,2269379773783175454,70057736432648552782,2282895953541692345722",
			"name": "Number of quasitrivial semigroups on an arbitrary n-element set.",
			"comment": [
				"Number of associative and quasitrivial binary operations on {1,...,n}. Convention a(0)=1."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A292932/b292932.txt\"\u003eTable of n, a(n) for n = 0..410\u003c/a\u003e",
				"M. Couceiro, J. Devillet, and J.-L. Marichal, \u003ca href=\"http://arxiv.org/abs/1709.09162\"\u003eQuasitrivial semigroups: characterizations and enumerations\u003c/a\u003e, arXiv:1709.09162 [math.RA], 2017.",
				"Jimmy Devillet, \u003ca href=\"https://arxiv.org/abs/1712.07856\"\u003eBisymmetric and quasitrivial operations: characterizations and enumerations\u003c/a\u003e, arXiv:1712.07856 [math.RA], 2017.",
				"Jimmy Devillet, Miguel Couceiro, \u003ca href=\"http://orbilu.uni.lu/handle/10993/39720\"\u003eCharacterizations and enumerations of classes of quasitrivial n-ary semigroups\u003c/a\u003e, 98th Workshop on General Algebra (AAA98, Dresden, Germany 2019).",
				"Jimmy Devillet, Jean-Luc Marichal, Bruno Teheux, \u003ca href=\"https://arxiv.org/abs/1811.11113\"\u003eClassifications of quasitrivial semigroups\u003c/a\u003e, arXiv:1811.11113 [math.RA], 2018."
			],
			"formula": [
				"E.g.f.: 1/(3 + x - 2*exp(x)).",
				"Recurrence: a(0) = 1, a(n+1) = (n+1)*a(n) + 2*Sum_{k=0...n-1} binomial(n+1,k)*a(k).",
				"Explicit form: a(n) = Sum_{i=0...n} Sum_{k=0...n-i} 2^i * (-1)^k * binomial(n,k) * S2(n-k,i) * (i+k)!, where S2(n,k) are the Stirling numbers of the second kind.",
				"a(n) ~ n! / ((r-1) * (r-3)^(n+1)), where r = -LambertW(-1, -2*exp(-3)) = 3.5830738760366909976807989989303134394318270218566... - _Vaclav Kotesovec_, Sep 27 2017"
			],
			"mathematica": [
				"With[{m=30}, CoefficientList[Series[1/(3+x-2*Exp[x]), {x,0,m}], x]*Range[0,m]!] (* _G. C. Greubel_, May 21 2019 *)"
			],
			"program": [
				"(PARI) my(x='x + O('x^30)); Vec(serlaplace(1/(x+3-2*exp(x)))) \\\\ _Michel Marcus_, May 21 2019",
				"(MAGMA) m:=30; R\u003cx\u003e:=PowerSeriesRing(Rationals(), m); b:=Coefficients(R!( 1/(3+x-2*Exp(x)) )); [Factorial(n-1)*b[n]: n in [1..m]]; // _G. C. Greubel_, May 21 2019",
				"(Sage) m = 30; T = taylor(1/(3+x-2*exp(x)), x, 0, m); [factorial(n)*T.coefficient(x, n) for n in (0..m)] # _G. C. Greubel_, May 21 2019"
			],
			"xref": [
				"Cf. A292933, A292934."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Jean-Luc Marichal_, Sep 27 2017",
			"references": 8,
			"revision": 32,
			"time": "2019-08-19T16:50:20-04:00",
			"created": "2017-09-27T09:19:04-04:00"
		}
	]
}