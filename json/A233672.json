{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A233672",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 233672,
			"data": "1,1,0,-2,-3,0,6,8,0,-14,-18,0,30,38,0,-60,-75,0,114,140,0,-208,-252,0,366,439,0,-626,-744,0,1044,1232,0,-1704,-1998,0,2730,3182,0,-4300,-4986,0,6672,7700,0,-10212,-11736,0,15438,17673,0,-23076,-26322,0",
			"name": "Expansion of psi(q) * phi(-q^18) * f(-q^6) / f(q^3)^3 in powers of q where phi(), psi(), f() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A233672/b233672.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of eta(q^2)^2 * eta(q^3)^3 * eta(q^12)^3 * eta(q^18)^2 / (eta(q) * eta(q^6)^8 * eta(q^36)) in powers of q.",
				"Euler transform of period 36 sequence [ 1, -1, -2, -1, 1, 4, 1, -1, -2, -1, 1, 1, 1, -1, -2, -1, 1, 2, 1, -1, -2, -1, 1, 1, 1, -1, -2, -1, 1, 4, 1, -1, -2, -1, 1, 0, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (36 t)) = f(t) where q = exp(2 Pi i t).",
				"a(n) = A223670(n) unless n=0."
			],
			"example": [
				"G.f. = 1 + q - 2*q^3 - 3*q^4 + 6*q^6 + 8*q^7 - 14*q^9 - 18*q^10 + ..."
			],
			"mathematica": [
				"eta[q_] := q^(1/24)*QPochhammer[q]; CoefficientList[Series[eta[q^2]^2 *eta[q^3]^3*eta[q^12]^3*eta[q^18]^2/(eta[q]*eta[q^6]^8*eta[q^36]), {q, 0, 50}], q] (* _G. C. Greubel_, Aug 08 2018 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^2 * eta(x^3 + A)^3 * eta(x^12 + A)^3 * eta(x^18 + A)^2 / (eta(x + A) * eta(x^6 + A)^8 * eta(x^36 + A)), n))};"
			],
			"xref": [
				"Cf. A233670."
			],
			"keyword": "sign",
			"offset": "0,4",
			"author": "_Michael Somos_, Dec 14 2013",
			"references": 2,
			"revision": 14,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2013-12-16T03:04:23-05:00"
		}
	]
}