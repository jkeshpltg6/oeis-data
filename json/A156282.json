{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A156282",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 156282,
			"data": "1,1,1,1,2,2,1,1,2,3,3,2,1,1,3,6,9,11,11,9,6,3,1,1,2,4,6,8,9,9,8,6,4,2,1,1,3,7,13,21,30,39,46,50,50,46,39,30,21,13,7,3,1,1,3,7,13,22,33,46,59,71,80,85,85,80,71,59,46,33,22,13,7,3,1,1,3,7,14,25,40,60,84,111,139",
			"name": "A triangle sequence of cyclotomic product polynomials: p(x,n)=Product[Cyclotomic[k + 1, x], {k, 1, n}].",
			"comment": [
				"The row sums are:{1, 2, 6, 12, 60, 60, 420, 840, 2520, 2520, 27720,...}.",
				"This idea for these products come from the q-Eulerian exponential generalization:",
				"Here are the definitions of the q-exponentials in John Shareshian and Michelle Wachs paper:",
				"Clear[Q, e, p, n, x];",
				"p[x_, n_] := Product[(1 - x^k)/(1 - x), {k, 1, n}];",
				"e[x_, q_] = Sum[x^n/p[q, n], {n, 0, Infinity}];",
				"f[x_, t_, q_] = (1 - t)/(e[x*(t - 1), q] - t);",
				"Where the expansion is: (called the Stanley q-analog of the Eulerian type);",
				"(1 - t)/(e[x*(t - 1), q] - t)= Sum[A[n,q,t]*x^n/p[q, n], {n, 0, Infinity}] ;",
				"The idea here is to substitute the new Cyclotomic product where it will work for the q-product."
			],
			"link": [
				"L. Carlitz, \u003ca href=\"https://projecteuclid.org/euclid.dmj/1077475200\"\u003eq-Bernoulli numbers and polynomials\u003c/a\u003e, Duke Math. J. Volume 15, Number 4 (1948), 987-1000.",
				"L. Carlitz and J. Riordan, \u003ca href=\"https://projecteuclid.org/euclid.dmj/1077375351\"\u003eTwo element lattice permutation numbers and their q-generalization\u003c/a\u003e, Duke Math. J. Volume 31, Number 3 (1964), 371-388",
				"Y.-H. He, C. Matti, C. Sun, \u003ca href=\"http://arxiv.org/abs/1404.6833\"\u003eThe Scattering Variety\u003c/a\u003e, arXiv preprint arXiv:1403.6833 [cs.SE], 2014. See Table 2, central column. - _N. J. A. Sloane_, Jun 28 2014",
				"John Shareshian, Michelle L. Wachs, \u003ca href=\"http://arxiv.org/abs/math/0608274\"\u003eq-Eulerian Polynomials: Excedance Number ans Major Index\u003c/a\u003e, arXiv:math/0608274 [math.CO], 2006, page 3."
			],
			"formula": [
				"p(x,n)=Product[Cyclotomic[k + 1, x], {k, 1, n}]."
			],
			"example": [
				"1;",
				"1, 1;",
				"1, 2, 2, 1;",
				"1, 2, 3, 3, 2, 1;",
				"1, 3, 6, 9, 11, 11, 9, 6, 3, 1;",
				"1, 2, 4, 6, 8, 9, 9, 8, 6, 4, 2, 1;",
				"1, 3, 7, 13, 21, 30, 39, 46, 50, 50, 46, 39, 30, 21, 13, 7, 3, 1;",
				"1, 3, 7, 13, 22, 33, 46, 59, 71, 80, 85, 85, 80, 71, 59, 46, 33, 22, 13, 7, 3, 1;",
				"1, 3, 7, 14, 25, 40, 60, 84, 111, 139, 166, 189, 206, 215, 215, 206, 189, 166, 139, 111, 84, 60, 40, 25, 14, 7, 3, 1"
			],
			"mathematica": [
				"Clear[p, n, x]; p[x_, n_] = Product[Cyclotomic[k + 1, x], {k, 1, n}]; Table[FullSimplify[ExpandAll[p[x, n]]], {n, 0, 10}]; Table[CoefficientList[FullSimplify[ExpandAll[p[x, n]]], x], {n, 0, 10}]; Flatten[%]"
			],
			"program": [
				"(PARI) row(n) = Vec(prod(k=1, n, polcyclo(k+1))); \\\\ _Michel Marcus_, Dec 12 2017"
			],
			"keyword": "nonn,tabf,uned",
			"offset": "0,5",
			"author": "_Roger L. Bagula_, Feb 07 2009",
			"references": 0,
			"revision": 15,
			"time": "2017-12-12T03:44:59-05:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}