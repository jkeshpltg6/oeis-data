{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A126224",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 126224,
			"data": "1,-5,-48,660,11760,-257040,-6652800,198918720,6745939200,-255826771200,-10727081164800,492775291008000,24610605962342400,-1327677426915840000,-76940526008586240000,4766815315895592960000,314406967644177408000000,-21995911456386651463680000",
			"name": "Determinant of the n X n matrix in which the entries are 1 through n^2, spiraling inward starting with 1 in the (1,1)-entry.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A126224/b126224.txt\"\u003eTable of n, a(n) for n = 1..200\u003c/a\u003e",
				"Sergey Sadov, \u003ca href=\"http://www.jstor.org/stable/27642128\"\u003eProblem 11270\u003c/a\u003e, American Mathematical Monthly, Vol. 114, No. 1, 2007, p. 78."
			],
			"formula": [
				"a(n) = (-1)^[n*(n-1)/2]*2^(2*n-3)*(3*n-1)*Product_{k=0..n-2} (1/2+k) for n\u003e=2.",
				"E.g.f.: (((-16*x^2-1)*sqrt(2*sqrt(16*x^2+1)+2)-8*sqrt(16*x^2+1)*x^2+16*x^2 + sqrt(16*x^2+1)+1)*sqrt(2*sqrt(16*x^2+1)-2)+(8*(sqrt(16*x^2+1)*x^2+2*x^2-(1/8) * sqrt(16*x^2+1)+1/8))*sqrt(2*sqrt(16*x^2+1)+2))/(512*x^3+32*x). - _Robert Israel_, Apr 20 2017"
			],
			"example": [
				"For n = 2, the 2 X 2 (spiral) matrix A is",
				"      [1, 2]",
				"      [4, 3]",
				"Then a(2) = -5 because det(A) = 1*3 - 2*4 = -5."
			],
			"maple": [
				"a:=n-\u003e(-1)^(n*(n-1)/2)*2^(2*n-3)*(3*n-1)*product(1/2+k,k=0..n-2): seq(a(n),n=1..20);",
				"# second Maple program:",
				"a:= proc(n) option remember; `if`(n\u003c2, (3*n+1)/4,",
				"      4*(1-3*n)*(2*n-5)*(2*n-3) *a(n-2) /(3*n-7))",
				"    end:",
				"seq(a(n), n=1..20);  # _Alois P. Heinz_, Jan 21 2014"
			],
			"mathematica": [
				"a[n_] := (-1)^(n*(n-1)/2)*2^(2n-3)*(3n-1)*Pochhammer[1/2, n-1]; Table[a[n], {n, 1, 20}] (* _Jean-François Alcover_, May 26 2015 *)"
			],
			"xref": [
				"Cf. A023999 (absolute values). - _Alois P. Heinz_, Jan 21 2014"
			],
			"keyword": "sign",
			"offset": "1,2",
			"author": "_Emeric Deutsch_, Dec 31 2006",
			"references": 6,
			"revision": 26,
			"time": "2021-04-07T20:05:33-04:00",
			"created": "2007-01-12T03:00:00-05:00"
		}
	]
}