{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A290478",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 290478,
			"data": "1,1,3,1,4,1,3,7,1,6,1,3,4,12,1,8,1,3,7,15,1,4,13,1,3,6,18,1,12,1,3,4,7,12,28,1,14,1,3,8,24,1,4,6,24,1,3,7,15,31,1,18,1,3,4,12,13,39,1,20,1,3,7,6,18,42,1,4,8,32,1,3,12,36,1,24,1,3,4,7",
			"name": "Triangle read by rows in which row n lists the sum of the divisors of each divisor of n.",
			"comment": [
				"Or, in the triangle A027750(n), replace each element with the sum of its divisors.",
				"The row whose index x is a prime power p^m (p prime and m \u003e= 0) is equal to (1, sigma(p), sigma(p^2), ..., sigma(p^(m-1))).",
				"We observe the following properties of row n when n is the product of k distinct primes, k = 1,2,...:",
				"when n = prime(m), row n = (1, prime(m)+1);",
				"when n is the product of two distinct primes p \u003c q, row n = (1, p+1, q+1,(p+1)(q+1));",
				"when n is the product of three distinct primes p \u003c q \u003c r, row n = (1, p+1, q+1, r+1, (p+1)(q+1), (p+1)(r+1), (q+1)(r+1), sigma(p*q*r));"
			],
			"link": [
				"Michel Marcus, \u003ca href=\"/A290478/b290478.txt\"\u003eTable of n, a(n) for n = 1..10006\u003c/a\u003e (rows 1 to 1358, flattened)."
			],
			"formula": [
				"a(n) = sigma(A027750(n))."
			],
			"example": [
				"Row 6 is (a(11), a(12), a(13), a(14)) = (1, 3, 4, 12) because sigma(A027750(11))= sigma(1) = 1, sigma(A027750(12))= sigma(2) = 3, sigma(A027750(13))= sigma(3) = 4 and sigma(A027750(14)) = sigma(6) = 12.",
				"Triangle begins:",
				"  1;",
				"  1,  3;",
				"  1,  4;",
				"  1,  3,  7;",
				"  1,  6;",
				"  1,  3,  4, 12;",
				"  1,  8;",
				"  1,  3,  7, 15;",
				"  1,  4, 13;",
				"  1,  3,  6, 18;",
				"  ..."
			],
			"maple": [
				"with(numtheory):nn:=100:",
				"for n from 1 to nn do:",
				"  d1:=divisors(n):n1:=nops(d1):",
				"   for i from 1 to n1 do:",
				"     s:=sigma(d1[i]):",
				"     printf(`%d, `,s):",
				"   od:",
				"od:"
			],
			"mathematica": [
				"Array[DivisorSigma[1, Divisors@ #] \u0026, 24] // Flatten (* _Michael De Vlieger_, Aug 07 2017 *)"
			],
			"program": [
				"(PARI) row(n) = apply(sigma, divisors(n)); \\\\ _Michel Marcus_, Dec 27 2021",
				"(MAGMA) [[SumOfDivisors(d): d in Divisors(n)]: n in [1..20]]; // _Vincenzo Librandi_, Sep 08 2017"
			],
			"xref": [
				"Cf. A000203, A027750, A290532.",
				"Cf. A007429 (row sums), A206032 (row products)."
			],
			"keyword": "nonn,tabf",
			"offset": "1,3",
			"author": "_Michel Lagneau_, Aug 03 2017",
			"references": 2,
			"revision": 44,
			"time": "2021-12-28T04:17:46-05:00",
			"created": "2017-08-21T05:55:48-04:00"
		}
	]
}