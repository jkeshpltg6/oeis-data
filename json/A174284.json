{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A174284",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 174284,
			"data": "0,1,3,7,15,35,79,193,493,1299,3429,9049,23699,62271,163997,433433,1147659,3040899",
			"name": "Number of distinct finite resistances that can be produced using at most n equal resistors (n or fewer resistors) in series, parallel and/or bridge configurations.",
			"comment": [
				"This sequence is a variation on A153588, which uses only series and parallel combinations. The circuits with exactly n unit resistors are counted by A174283, so this sequence counts the union of the sets, which are counted by A174283(k), k \u003c= n. - _Rainer Rosenthal_, Oct 27 2020",
				"For n = 0 the resistance is infinite, therefore the number of finite resistances is a(0) = 0. Sequence A180414 counts all resistances (including infinity) and so has A180414(0) = 1 and A180414(n) = a(n) + 1 for all n up to n = 7. For n \u003e 7 the networks get more complex, producing more resistance values, so A180414(n) \u003e a(n) + 1. - _Rainer Rosenthal_, Feb 13 2021"
			],
			"link": [
				"Antoni Amengual, \u003ca href=\"http://dx.doi.org/10.1119/1.19396\"\u003eThe intriguing properties of the equivalent resistances of n equal resistors combined in series and in parallel\u003c/a\u003e, American Journal of Physics, 68(2), 175-179 (February 2000).",
				"Sameen Ahmed Khan, \u003ca href=\"http://arxiv.org/abs/1004.3346/\"\u003eThe bounds of the set of equivalent resistances of n equal resistors combined in series and in parallel\u003c/a\u003e, arXiv:1004.3346v1 [physics.gen-ph], (20 April 2010).",
				"Sameen Ahmed Khan, \u003ca href=\"https://www.ias.ac.in/describe/article/pmsc/122/02/0153-0162\"\u003eFarey sequences and resistor networks\u003c/a\u003e, Proc. Indian Acad. Sci. (Math. Sci.) Vol. 122, No. 2, May 2012, pp. 153-162.",
				"Sameen Ahmed Khan, \u003ca href=\"https://dx.doi.org/10.17485/ijst/2016/v9i44/88086\"\u003eBeginning to Count the Number of Equivalent Resistances\u003c/a\u003e, Indian Journal of Science and Technology, Vol. 9, Issue 44, pp. 1-7, 2016.",
				"Rainer Rosenthal, \u003ca href=\"/A174284/a174284.txt\"\u003eMaple program SetA174283 used for A174284\u003c/a\u003e",
				"\u003ca href=\"/index/Res#resistances\"\u003eIndex to sequences related to resistances\u003c/a\u003e."
			],
			"formula": [
				"a(n) = #(union of all S(k), k \u003c= n), where S(k) is the set which is counted by A174283(k). - _Rainer Rosenthal_, Oct 27 2020"
			],
			"example": [
				"Since a bridge circuit requires a minimum of five resistors, the first four terms coincide with A153588. The fifth term also coincides since the set corresponding to five resistors for the bridge, i.e. {1}, is already obtained in the fourth set corresponding to the fourth term in A153588. [Edited by _Rainer Rosenthal_, Oct 27 2020]"
			],
			"maple": [
				"# SetA174283(n) is the set of resistances counted by A174283(n) (see Maple link).",
				"AccumulatedSetsA174283 := proc(n) option remember;",
				"if n=1 then {1} else `union`(AccumulatedSetsA174283(n-1), SetA174283(n)) fi end:",
				"A174284 := n -\u003e nops(AccumulatedSetsA174283(n)):",
				"seq(A174284(n), n=1..9); # _Rainer Rosenthal_, Oct 27 2020"
			],
			"xref": [
				"Cf. A048211, A153588, A174283, A174285, A174286, A176499, A176500, A176501, A176502, A180414."
			],
			"keyword": "more,nonn",
			"offset": "0,3",
			"author": "_Sameen Ahmed Khan_, Mar 15 2010",
			"ext": [
				"a(8) corrected, a(9)-a(17) from _Rainer Rosenthal_, Oct 27 2020",
				"Title changed and a(0) added by _Rainer Rosenthal_, Feb 13 2021"
			],
			"references": 14,
			"revision": 50,
			"time": "2021-10-21T06:32:32-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}