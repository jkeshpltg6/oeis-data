{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A093328",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 93328,
			"data": "3,5,11,21,35,53,75,101,131,165,203,245,291,341,395,453,515,581,651,725,803,885,971,1061,1155,1253,1355,1461,1571,1685,1803,1925,2051,2181,2315,2453,2595,2741,2891,3045,3203,3365,3531,3701,3875,4053,4235",
			"name": "a(n) = 2*n^2 + 3.",
			"comment": [
				"Number of 132-avoiding two-stack sortable permutations which also avoid 4321.",
				"Conjecture: no perfect powers. - _Zak Seidov_, Sep 27 2015",
				"Numbers k such that 2*k - 6 is a square. - _Bruno Berselli_, Nov 08 2017"
			],
			"reference": [
				"Steven Edwards and William Griffiths, Generalizations of Delannoy and cross polytope numbers, The Fibonacci Quarterly, Vol. 55, No. 4 (2017), pp. 357-366."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A093328/b093328.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Steven Edwards and William Griffiths, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL23/Griffiths/griffiths51.html\"\u003eOn Generalized Delannoy Numbers\u003c/a\u003e, J. Int. Seq., Vol. 23 (2020), Article 20.3.6.",
				"Eric S. Egge and Toufik Mansour, \u003ca href=\"https://doi.org/10.1016/j.dam.2003.12.007\"\u003e132-avoiding two-stack sortable permutations, Fibonacci numbers, and Pell numbers\u003c/a\u003e, Discrete Applied Mathematics, Vol. 143, No. 1-3 (2004), pp. 72-83; \u003ca href=\"https://arxiv.org/abs/math/0205206\"\u003earXiv preprint\u003c/a\u003e, arXiv:math/0205206 [math.CO], 2002.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1)."
			],
			"formula": [
				"From _Vincenzo Librandi_, Jul 08 2012: (Start)",
				"G.f.: (3 - 4*x + 5*x^2)/(1 - x)^3.",
				"a(n) = 3*a(n-1) - 3*a(n-2) + a(n-3). (End)",
				"Sum_{n\u003e=0} 1/a(n) = (1 + sqrt(3/2)*Pi*coth(sqrt(3/2)*Pi))/6. - _Amiram Eldar_, Nov 25 2020"
			],
			"mathematica": [
				"Table[2 n^2 + 3, {n, 0, 100}] (* _Vladimir Joseph Stephan Orlovsky_, Feb 15 2011*)",
				"CoefficientList[Series[(3 - 4 x + 5 x^2)/(1 - x)^3, {x, 0, 50}], x] (* _Vincenzo Librandi_, Jul 08 2012 *)",
				"LinearRecurrence[{3, -3, 1}, {3, 5, 11}, 50] (* _Harvey P. Dale_, Apr 03 2016 *)"
			],
			"program": [
				"(PARI) a(n)=2*n^2+3; \\\\ _Zak Seidov_, Sep 27 2015",
				"(MAGMA) [2*n^2+3: n in [0..50]]; // _Vincenzo Librandi_, Jul 08 2012"
			],
			"xref": [
				"a(n) = A005893(n)+1 = A058331(n)+2 = A001105(n)+3.",
				"a(n+2) = A154685(n+1,n+2)."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_Ralf Stephan_, Apr 25 2004",
			"ext": [
				"Simpler definition and new offset from Paul F. Brewbaker, Jun 23 2009",
				"Edited by _N. J. A. Sloane_, Jun 27 2009"
			],
			"references": 16,
			"revision": 58,
			"time": "2020-11-25T06:45:21-05:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}