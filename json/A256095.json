{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A256095",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 256095,
			"data": "0,1,1,3,1,3,6,1,3,6,10,1,1,2,10,15,1,3,3,5,15,21,1,3,3,1,3,21,28,1,1,2,2,1,7,28,36,1,3,6,2,3,3,4,36,45,1,3,3,5,15,3,1,9,45,55,1,1,1,5,5,1,1,1,5,55,66,1,3,6,2,3,3,2,6,3,11,66,78,1,3,6,2,3,3,2,6,3,1,6,78,91,1,1,1,1,1,7,7,1,1,1,1,13,91,105,1,3,3,5,15,21,7,3,15,5,3,3,7,105",
			"name": "Triangle of greatest common divisors of two triangular numbers (A000217).",
			"link": [
				"Robert Israel, \u003ca href=\"/A256095/b256095.txt\"\u003eTable of n, a(n) for n = 0..10010\u003c/a\u003e (rows 0 to 140, flattened)"
			],
			"formula": [
				"T(n, m) = gcd(Tri(n), Tri(m)), 0 \u003c= m \u003c= n, with the triangular numbers Tri = A000217.",
				"T(n, 0) = Tri(n) = T(n, n). T(n, 1) = 1, n \u003e= 0.",
				"Columns m=2: A144437(n-1), m=3: repeat(6, 2, 3, 3, 2, 6, 3, 1, 6, 6, 1, 3) (guess), m=4: repeat(10, 5, 1, 2, 2, 5, 5, 2, 2, 1, 5, 10, 2, 1, 1, 10, 10, 1, 1, 2) (guess), m=5 repeat(15, 3, 1, 3, 15, 5, 3, 3, 1, 15, 15, 1, 3, 3, 5) (guess), ...",
				"From _Robert Israel_, Jan 21 2020: (Start) The guesses are correct. More generally, for each k\u003e=1, T(n,k) is periodic in n with period 2*A000217(k) if k == 0 or 3 (mod 4), A000217(k) if k == 1 or 2 (mod 4). (End)"
			],
			"example": [
				"The triangle T(n, m) begins:",
				"n\\m   0 1 2 3  4  5  6  7  8  9 10 11 12 13  14",
				"0:    0",
				"1:    1 1",
				"2:    3 1 3",
				"3:    6 1 3 6",
				"4:   10 1 1 2 10",
				"5:   15 1 3 3  5 15",
				"6:   21 1 3 3  1  3 21",
				"7:   28 1 1 2  2  1  7 28",
				"8:   36 1 3 6  2  3  3  4 36",
				"9:   45 1 3 3  5 15  3  1  9 45",
				"10:  55 1 1 1  5  5  1  1  1  5 55",
				"11:  66 1 3 6  2  3  3  2  6  3 11 66",
				"12:  78 1 3 6  2  3  3  2  6  3  1  6 78",
				"13:  91 1 1 1  1  1  7  7  1  1  1  1 13 91",
				"14: 105 1 3 3  5 15 21  7  3 15  5  3  3  7 105",
				"..."
			],
			"maple": [
				"T:= (i,j) -\u003e igcd(i*(i+1)/2,j*(j+1)/2):",
				"seq(seq(T(i,j),j=0..i),i=0..20); # _Robert Israel_, Jan 20 2020"
			],
			"program": [
				"(PARI) tabl(nn) = {for (n=0, nn, trn = n*(n+1)/2; for (k=0, n, print1(gcd(trn, k*(k+1)/2), \", \");); print(););} \\\\ _Michel Marcus_, Mar 17 2015"
			],
			"xref": [
				"Cf. A000217, A144437.",
				"T(2n,n) gives A026741."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,4",
			"author": "_Wolfdieter Lang_, Mar 17 2015",
			"references": 3,
			"revision": 17,
			"time": "2020-01-21T09:13:38-05:00",
			"created": "2015-03-18T06:03:43-04:00"
		}
	]
}