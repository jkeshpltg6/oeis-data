{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A336885",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 336885,
			"data": "4,5,1,5,5,8,2,4,4,10,8,10,4,5,1,8,12,15,5,5,2,10,5,13,13,13,4,5,5,8,8,13,4,17,4,10,10,16,4,8,5,9,2,10,1,13,5,13,1,17,9,17,13,17,8,20,16,20,3,15,5,8,4,13,8,13,4,17,16,17,1,20,20,25",
			"name": "Irregular triangle read by rows: row n gives the sequence of pairs ((x(n)_j, y(n)_j))_{j=1..A336886(n)}, for all non-Pythagorean triples ((X(n)_j = sqrt(x(n)_j), Y(n)_j = sqrt(y(n)_j), Z(n) = sqrt(z(n))), with integers 1 \u003c= x(n)_j \u003c= y(n)_j \u003c= z(n) = A334818(n), which correspond to non-right triangles with positive integer area A(n)_j with leg pairs (X(n)_j, Y(n)_j) and base Z(n), for j = 1, 2, ..., A336886(n). The order of the pairs in row n is by nondecreasing y(n)_j.",
			"comment": [
				"This entry, A336886 and A336887 are inspired by A334818.",
				"The length of row n is 2*A336886(n).",
				"The positive integer areas are A(n)_j = (1/4)*sqrt(2*(z(n)*y(n)_j + z(n)*x(n)_j + y(n)_j*x(n)_j) - ((x(n)_j)^2 + (y(n)_j)^2 + z(n)^2)), for j = 1, 2, ..., A336886(n). The corresponding irregular triangle of these areas is given in A336887. (Degenerate triangles are not considered, hence X(n)_j + Y(n)_j \u003e Z(n).)",
				"The triples (x(n))j, y(n)_j, z(n)) may be non-primitive.",
				"The positive or negative defect to Pythagorean triples is deltaPT(n)_j := (X(n)_j)^2 +  (Y(n)_j)^2 -  Z(n)^2 \u003e -2*X(n)_j*Y(n)_j, for  j = 1, 2, ..., A336886(n)."
			],
			"formula": [
				"T(n, 2*j-1) = x(n)_j, and T(n, 2*j) = y(n)_j, for j = 1, 2, ..., A336886(n)."
			],
			"example": [
				"The irregular triangle T(n, k) with pairs (x(n)_j, y(n)_j), j= 1, 2, ..., A336886(n), begins:",
				"n, z(n) \\ k 1  2  3  4  5 6   7  8  9 10 11 12 13 14 15 16 17 18 18 20 ...",
				"--------------------------------------------------------------------------",
				"1,   5:     4  5",
				"2,   8:     1  5",
				"3,   9:     5  8",
				"4,  10:     2  4  4 10  8 10",
				"5,  13:     4  5  1  8",
				"6,  15:    12 15",
				"7,  16:     5  5  2 10  5 13 13 13",
				"8,  17:     4  5  5  8  8 13  4 17",
				"9,  18:     4 10 10 16",
				"10, 20:     4  8  5  9  2 10  1 13  5 13  1 17  9 17 13 17  8 20 16 20",
				"11, 24:     3 15",
				"12, 25:     5  8  4 13  8 13  4 17 16 17  1 20 20 25",
				"13, 26:     4 10  8 10  2 16  2 20 10 20 18 20  4 26",
				"14, 27:    15 24",
				"15, 29:     8  9  4 13  5 16  1 20 13 20 17 20  8 25 16 29",
				"16, 30:     6 12 12 30 24 30",
				"17, 32:    10 10  5 13  9 17  4 20  1 25 17 25 10 26 26 26  5 29",
				"...",
				"T(4, 1) = 2 and T(4, 2) = 4 because for n = 4, z(4) = 10, k = 1: (X(10)_1 = sqrt(2), Y(10)_1 = sqrt(4)) and Z(n) = sqrt(10). This is not a Pythagorean triangle because 2 + 4 is not 10, and the area is integer: A(4, 1) =  A336887(4, 1) = 1 because A(4, 1) = (1/4)*sqrt(2*(10*4 + 10*2 + 4*2) - (2^2 + 4^2 + 10^2)) = 1."
			],
			"xref": [
				"Cf. A334818, A336886, A336887."
			],
			"keyword": "nonn,tabf",
			"offset": "1,1",
			"author": "_Wolfdieter Lang_, Aug 10 2020",
			"references": 4,
			"revision": 9,
			"time": "2021-01-08T22:16:01-05:00",
			"created": "2020-08-11T01:44:24-04:00"
		}
	]
}