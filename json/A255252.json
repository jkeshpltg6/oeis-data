{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A255252",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 255252,
			"data": "1,-1,-1,0,-2,3,2,1,-1,-1,1,-2,1,-3,-2,-2,3,1,-1,4,3,-1,-1,2,-4,4,1,0,-1,-2,-3,-3,-4,2,3,-3,0,0,5,2,0,-3,2,-1,4,1,0,1,3,0,-2,2,-1,-2,-4,-5,2,0,-7,3,-4,3,1,5,2,-5,-1,-1,-3,4,-1,3,4,1,4",
			"name": "Expansion of psi(x) * psi(-x)^2 in powers of x where psi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A255252/b255252.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of f(-x) * f(-x^4)^2 in powers of x where f() is a Ramanujan theta function.",
				"Expansion of q^(-3/8) * eta(q) * eta(q^4)^2 in powers of q.",
				"Euler transform of period 4 sequence [ -1, -1, -1, -3, ...].",
				"G.f.: Product_{k\u003e0} (1 - x^k) * (1 - x^(4*k))^2.",
				"2 * a(n) = A034950(4*n + 1)."
			],
			"example": [
				"G.f. = 1 - x - x^2 - 2*x^4 + 3*x^5 + 2*x^6 + x^7 - x^8 - x^9 + x^10 + ...",
				"G.f. = q^3 - q^11 - q^19 - 2*q^35 + 3*q^43 + 2*q^51 + q^59 - q^67 - q^75 + ..."
			],
			"maple": [
				"A255252 := proc(n)",
				"    local psi,x,i ;",
				"    psi := add( A010054(i)*x^i,i=0..n) ;",
				"    psi*subs(x=-x,psi)^2 ;",
				"    coeftayl(%,x=0,n) ;",
				"end proc:",
				"seq(A255252(n),n=0..20) ; # _R. J. Mathar_, Feb 22 2021"
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x] QPochhammer[ x^4]^2, {x, 0, n}];",
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 2, 0, x^(1/2)] EllipticTheta[ 2, Pi/4, x^(1/2)]^2 / (4 x^(3/8)), {x, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A) * eta(x^4 + A)^2, n))};"
			],
			"xref": [
				"Cf. A034950."
			],
			"keyword": "sign",
			"offset": "0,5",
			"author": "_Michael Somos_, Feb 18 2015",
			"references": 2,
			"revision": 11,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2015-02-18T22:17:56-05:00"
		}
	]
}