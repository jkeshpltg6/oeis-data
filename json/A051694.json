{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A051694",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 51694,
			"data": "2,3,5,21,55,13,34,2584,46368,377,832040,4181,6765,701408733,987,196418,591286729879,610,72723460248141,190392490709135,24157817,8944394323791464,160500643816367088,89,7778742049,12586269025",
			"name": "Smallest Fibonacci number that is divisible by n-th prime.",
			"comment": [
				"It is conjectured that a(n) is not divisible by prime(n)^2. See Remark on p. 528 of Wall and Conjectures in CNRS links. - _Michel Marcus_, Feb 24 2016"
			],
			"link": [
				"Zak Seidov and Alois P. Heinz, \u003ca href=\"/A051694/b051694.txt\"\u003eTable of n, a(n) for n = 1..650\u003c/a\u003e (first 100 terms from Zak Seidov)",
				"Shalom Eliahou, \u003ca href=\"http://images.math.cnrs.fr/Mysteres-arithmetiques-de-la-suite-de-Fibonacci.html\"\u003eMystères Arithmétiques de la Suite de Fibonacci\u003c/a\u003e, (in French), Images des Mathématiques, CNRS, 2014.",
				"Ron Knott, \u003ca href=\"http://www.maths.surrey.ac.uk/hosted-sites/R.Knott/Fibonacci/fib.html\"\u003eFibonacci numbers with tables of F(0)-F(500)\u003c/a\u003e",
				"D. D. Wall, \u003ca href=\"http://www.jstor.org/stable/2309169\"\u003eFibonacci series modulo m\u003c/a\u003e, Amer. Math. Monthly, 67 (1960), 525-532."
			],
			"formula": [
				"a(n) = A000045(A001602(n)). - _Max Alekseyev_, Dec 12 2007",
				"log a(n) \u003c\u003c (n log n)^2. - _Charles R Greathouse IV_, Jul 17 2012"
			],
			"example": [
				"55 is first Fibonacci number that is divisible by 11, the 5th prime, so a(5) = 55."
			],
			"maple": [
				"F:= proc(n) option remember; `if`(n\u003c2, n, F(n-1)+F(n-2)) end:",
				"a:= proc(n) option remember; local p, k; p:=ithprime(n);",
				"      for k while irem(F(k), p)\u003e0 do od; F(k)",
				"    end:",
				"seq(a(n), n=1..30);  # _Alois P. Heinz_, Sep 28 2015"
			],
			"mathematica": [
				"f[n_] := Block[{fib = Fibonacci /@ Range[n^2]}, Reap@ For[k = 1, k \u003c= n, k++, Sow@ SelectFirst[fib, Mod[#, Prime@ k] == 0 \u0026]] // Flatten //",
				"Rest]; f@ 26 (* _Michael De Vlieger_, Mar 28 2015, Version 10 *)"
			],
			"program": [
				"(PARI) a(n)=if(n==3,5,my(p=prime(n));fordiv(p^2-1,d,if(fibonacci(d)%p==0, return(fibonacci(d))))) \\\\ _Charles R Greathouse IV_, Jul 17 2012"
			],
			"xref": [
				"Cf. A000045, A001602, A001605, A005478."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Jud McCranie_",
				"More terms from _James A. Sellers_, Dec 08 1999"
			],
			"references": 12,
			"revision": 39,
			"time": "2016-07-07T23:54:46-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}