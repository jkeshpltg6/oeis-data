{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A340129",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 340129,
			"data": "1,1,2,4,2,2,4,4,2,2,4,4,4,4,4,2,4,2,2,2,16,4,4,4,2,4,2,4,4,8,8,8,4,4,4,2,8,4,16,4,16,4,2,4,16,4,4,16,4,8,8,8,4,4,8,4,8,4,4,4,16,4,4,8,2,16,2,32,2,16,4,4,2,4,8,16,4,8,4,8,4,4,8,4,16",
			"name": "a(n) is the number of solutions of the Diophantine equation x^2 + y^2 = z^5 + z, gcd(x, y, z) = 1, x \u003c= y, where z = A008784(n).",
			"comment": [
				"The idea of this sequence comes from the 6th problem of the 21st British Mathematical Olympiad in 1985 where it is asked to show that this equation has infinitely many solutions (see link Olympiads and reference Gardiner).",
				"Indeed, this Diophantine equation x^2 + y^2 = z^5 + z with gcd(x, y, z) = 1 has solutions iff z is in A008784.",
				"When z is in A008784, there exist (u, v), gcd(u, v) = 1 such that z = u^2 + v^2; then, (u*z^2-v)^2 + (u+v*z^2)^2 = z^5 + z. Hence, with x = min(u*z^2-v, u+v*z^2) and y = max(u*z^2-v, u+v*z^2), the equation x^2 + y^2 = z^5 + z is satisfied. So this equation has infinitely many solutions since it has at least one solution for each term of A008784.",
				"For instance, for z = 10 we have:",
				"  with (u,v) = (1,3), then x = 1*10^2-3 = 97 and y = 1+3*10^2 = 301,",
				"  with (u,v) = (3,1), then x = 3+1*10^2 = 103 and y = 3*10^2-1 = 299,",
				"  so finally 97^2 + 301^2 = 103^2 + 299^2 = 10^5 + 10.",
				"Note that some z, among them 10, have other solutions not of this form."
			],
			"reference": [
				"A. Gardiner, The Mathematical Olympiad Handbook: An Introduction to Problem Solving, Oxford University Press, 1997, reprinted 2011, Problem 6, pp. 63 and 167-168 (1985)."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A340129/b340129.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"British Mathematical Olympiad, \u003ca href=\"https://bmos.ukmt.org.uk/home/bmo-1985.pdf\"\u003e1985 - Problem 6\u003c/a\u003e.",
				"\u003ca href=\"/index/O#Olympiads\"\u003eIndex to sequences related to Olympiads\u003c/a\u003e."
			],
			"example": [
				"For z = A008784(1) = 1, 1^2 + 1^2 = 1^5 + 1 is the only solution, so a(1) = 1.",
				"For z = A008784(3) = 5, 23^2 + 51^2 = 27^2 + 49^2 = 5^5 + 5 so a(3) = 2.",
				"For z = A008784(4) = 10, (97, 301, 10), (103, 299, 10), (119, 293, 10) and (163, 271, 10) are solutions, so a(4) = 4."
			],
			"mathematica": [
				"f[n_] := Length @ Solve[x^2 + y^2 == n^5 + n \u0026\u0026 GCD @@ {x, y, n} == 1 \u0026\u0026 0 \u003c= x \u003c= y, {x, y}, Integers]; f /@ Select[Range[500], IntegerExponent[#, 2] \u003c 2 \u0026\u0026 AllTrue[FactorInteger[#][[;; , 1]], Mod[#1, 4] \u003c 3 \u0026] \u0026] (* _Amiram Eldar_, Jan 22 2021 *)"
			],
			"program": [
				"(PARI) f(z) = {if (issquare(Mod(-1, z)), my(nb = 0, s = z^5+z, d, j); for (i=1, sqrtint(s), if (issquare(d = s - i^2), j = sqrtint(d); if ((j\u003c=i) \u0026\u0026 gcd([i, j, z]) == 1, nb++););); nb;);}",
				"lista(nn) = {for (n=1, nn, if (issquare(Mod(-1, n)), print1(f(n), \", \")););} \\\\ _Michel Marcus_, Jan 20 2021"
			],
			"xref": [
				"Cf. A008784, A048147, A131471."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Bernard Schott_, Jan 17 2021",
			"ext": [
				"More terms from _Michel Marcus_, Jan 20 2021"
			],
			"references": 2,
			"revision": 55,
			"time": "2021-01-22T07:34:44-05:00",
			"created": "2021-01-21T13:57:29-05:00"
		}
	]
}