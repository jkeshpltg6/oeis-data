{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A081706",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 81706,
			"data": "2,3,10,11,14,15,18,19,26,27,34,35,42,43,46,47,50,51,58,59,62,63,66,67,74,75,78,79,82,83,90,91,98,99,106,107,110,111,114,115,122,123,130,131,138,139,142,143,146,147,154,155,162,163,170,171,174,175,178,179,186",
			"name": "Numbers n such that binary representation ends either in an odd number of ones followed by one zero or in an even number of ones.",
			"comment": [
				"Values of k such that the Motzkin number A001006(k) is even. Values of k such that the number of restricted hexagonal polyominoes with k+1 cells (A002212) is even.",
				"Or union of sequences {2*A079523(n)+k}, k=0,1. A generalization see in comment to A161639. - _Vladimir Shevelev_, Jun 15 2009",
				"Or intersection of sequences A121539 and {A121539(n)-1}. A generalization see in comment to A161890. - _Vladimir Shevelev_, Jul 03 2009",
				"Also numbers n for which A010060(n+2) = A010060(n). - _Vladimir Shevelev_, Jul 06 2009",
				"The asymptotic density of this sequence is 1/3 (Rowland and Yassawi, 2015; Burns, 2016). - _Amiram Eldar_, Jan 30 2021",
				"Numbers of the form 4^k*(2*n-1)-2 and 4^k*(2*n-1)-1 where n and k are positive integers. - _Michael Somos_, Oct 22 2021"
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A081706/b081706.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (terms 1..1000 from G. C. Greubel)",
				"Jean-Paul Allouche, \u003ca href=\"https://jtnb.centre-mersenne.org/item/?id=JTNB_2015__27_2_375_0\"\u003eThue, Combinatorics on words, and conjectures inspired by the Thue-Morse sequence\u003c/a\u003e, Journal de théorie des nombres de Bordeaux, Vol. 27, No. 2 (2015), pp. 375-388; \u003ca href=\"https://arxiv.org/abs/1401.3727\"\u003earXiv preprint\u003c/a\u003e, arXiv:1401.3727 [math.NT], 2014.",
				"Jean-Paul Allouche, André Arnold, Jean Berstel, Srećko Brlek, William Jockusch, Simon Plouffe and Bruce E. Sagan, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(93)00147-W\"\u003eA sequence related to that of Thue-Morse\u003c/a\u003e, Discrete Math., Vol. 139, No. 1-3 (1995), pp. 455-461.",
				"Rob Burns, \u003ca href=\"https://arxiv.org/abs/1611.04910\"\u003eAsymptotic density of Motzkin numbers modulo small primes\u003c/a\u003e, arXiv:1611.04910 [math.NT], 2016.",
				"Eric Rowland and Reem Yassawi, \u003ca href=\"http://www.numdam.org/item/JTNB_2015__27_1_245_0/\"\u003eAutomatic congruences for diagonals of rational functions\u003c/a\u003e, Journal de Théorie des Nombres de Bordeaux, Vol. 27, No. 1 (2015), pp. 245-288.",
				"\u003ca href=\"/index/Ar#2-automatic\"\u003eIndex entries for 2-automatic sequences\u003c/a\u003e."
			],
			"formula": [
				"a(2n-1) = 2*A079523(n) = 4*A003159(n)-2; a(2n) = 4*A003159(n)-1.",
				"Note that a(2n) = 1+a(2n-1)."
			],
			"mathematica": [
				"(* m = MotzkinNumber *) m[0] = 1; m[n_] := m[n] = m[n - 1] + Sum[m[k]*m[n - 2 - k], {k, 0, n - 2}]; Select[Range[200], Mod[m[#], 2] == 0 \u0026] (* _Jean-François Alcover_, Jul 10 2013 *)",
				"Select[Range[200], EvenQ@Hypergeometric2F1[3/2, -#, 3, 4]\u0026] (* _Vladimir Reshetnikov_, Nov 02 2015 *)"
			],
			"program": [
				"(PARI) is(n)=valuation(bitor(n,1)+1,2)%2==0 \\\\ _Charles R Greathouse IV_, Mar 07 2013"
			],
			"xref": [
				"Cf. A001006, A002212, A003159, A010060, A079523, A121539, A161639, A161890."
			],
			"keyword": "nonn,base,easy",
			"offset": "1,1",
			"author": "_Emeric Deutsch_ and _Bruce E. Sagan_, Apr 02 2003",
			"references": 23,
			"revision": 51,
			"time": "2021-10-22T23:56:06-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}