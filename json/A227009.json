{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A227009",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 227009,
			"data": "1,1,1,1,1,0,0,1,1,1,1,1,2,0,0,0,0,1,1,1,1,1,2,1,1,1,0,1,0,0,0,0,0,0,1,1,1,1,1,2,2,2,2,3,4,2,2,2,2,1,0,2,0,0,0,0,0,0,0,0,1,1,1,1,1,2,2,2,2,3,4,3,3,4,4,4,3,4,3,2,2,2,2,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1",
			"name": "Irregular triangle read by rows: T(n,k) is the number of partitions of an n X n square lattice into squares that contain k nodes unconnected to any of their neighbors, considering only the number of parts.",
			"comment": [
				"The n-th row contains (n-1)^2 + 1 elements.",
				"The irregular triangle is shown below.",
				"\\ k 0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 ...",
				"n",
				"1   1",
				"2   1  1",
				"3   1  1  0  0  1",
				"4   1  1  1  1  2  0  0  0  0  1",
				"5   1  1  1  1  2  1  1  1  0  1  0  0  0  0  0  0  1",
				"6   1  1  1  1  2  2  2  2  3  4  2  2  2  2  1  0  2  0  0 ...",
				"7   1  1  1  1  2  2  2  2  3  4  3  3  4  4  4  3  4  3  2 ..."
			],
			"link": [
				"Christopher Hunt Gribble and Alois P. Heinz, \u003ca href=\"/A227009/b227009.txt\"\u003eRows n = 1..13, flattened\u003c/a\u003e (Rows n = 1..7 from Christopher Hunt Gribble)"
			],
			"formula": [
				"It appears that T(n,k) = T(n-1,k), n odd, n \u003e 1 and k = 0..(n-1)^2/4.",
				"Sum_{k=0..(n-1)^2} T(n,k) = A034295(n)."
			],
			"example": [
				"For n = 6, there are 3 partitions that contain 8 isolated nodes, so T(6,8) = 3.",
				"An m X m square contains (m-1)^2 isolated nodes.",
				"Consider that each partition is composed of ones and zeros where a one represents a node with one or more links to its neighbors and a zero represents a node with no links to its neighbors.  Then the 3 partitions are:",
				"1 1 1 1 1 1 1    1 1 1 1 1 1 1    1 1 1 1 1 1 1",
				"1 0 1 0 1 0 1    1 0 0 1 1 0 1    1 0 0 1 0 0 1",
				"1 1 1 1 1 1 1    1 0 0 1 1 1 1    1 0 0 1 0 0 1",
				"1 0 1 0 1 0 1    1 1 1 1 1 0 1    1 1 1 1 1 1 1",
				"1 1 1 1 1 1 1    1 1 1 1 1 1 1    1 1 1 1 1 1 1",
				"1 0 1 0 1 1 1    1 1 1 0 1 0 1    1 1 1 1 1 1 1",
				"1 1 1 1 1 1 1    1 1 1 1 1 1 1    1 1 1 1 1 1 1"
			],
			"maple": [
				"b:= proc(n, l) option remember; local i, k, s, t;",
				"      if max(l[])\u003en then {} elif n=0 or l=[] then {0}",
				"    elif min(l[])\u003e0 then t:=min(l[]); b(n-t, map(h-\u003eh-t, l))",
				"    else for k do if l[k]=0 then break fi od; s:={};",
				"         for i from k to nops(l) while l[i]=0 do s:=s union",
				"             map(v-\u003ev+x^(1+i-k), b(n, [l[j]$j=1..k-1,",
				"                 1+i-k$j=k..i, l[j]$j=i+1..nops(l)]))",
				"         od; s",
				"      fi",
				"    end:",
				"T:= n-\u003e (w-\u003eseq(coeff(w, z, h), h=0..(n-1)^2))(add(z^add(",
				"    coeff(p, x, i)*(i-1)^2, i=2..degree(p)), p=b(n, [0$n]))):",
				"seq(T(n), n=1..9);  # _Alois P. Heinz_, Jun 27 2013"
			],
			"mathematica": [
				"b[n_, l_List] := b[n, l] = Module[{i, k , s, t}, Which[Max[l] \u003e n, {}, n == 0 || l == {}, {0}, Min[l] \u003e 0, t = Min[l]; b[n-t, l-t], True, For[k = 1, k \u003c= Length[l], k++, If[l[[k]] == 0, Break[]]]; s = {}; For[i = k, i \u003c= Length[l] \u0026\u0026 l[[i]] == 0, i++, s = s ~Union~ Map[# + x^(1+i-k)\u0026, b[n, Join[l[[1 ;; k-1]], Array[1+i-k\u0026, i-k+1], l[[i+1 ;; Length[l]]]]]]]; s]]; T[n_] := Function[w, Table[Coefficient[w, z, h], {h, 0, (n-1)^2}]][Sum[ z^Sum[Coefficient[p, x, i]*(i-1)^2, {i, 2, Exponent[p, x]}], {p, b[n, Array[0\u0026, n]]}]]; Table[T[n], {n, 1, 9}] // Flatten (* _Jean-François Alcover_, Jan 24 2016, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A034295."
			],
			"keyword": "nonn,tabf",
			"offset": "1,13",
			"author": "_Christopher Hunt Gribble_, Jun 27 2013",
			"references": 2,
			"revision": 22,
			"time": "2016-01-24T05:33:09-05:00",
			"created": "2013-06-27T14:57:56-04:00"
		}
	]
}