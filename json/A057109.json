{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A057109",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 57109,
			"data": "4,8,9,12,16,18,24,25,27,32,36,45,48,49,50,54,64,72,75,80,81,90,96,98,100,108,121,125,128,135,144,147,150,160,162,169,175,180,189,192,196,200,216,224,225,240,242,243,245,250,256,270,288,289,294,300,320,324",
			"name": "Numbers n that are not factors of P(n)!, where P(n) is the largest prime factor of n.",
			"comment": [
				"These are also the numbers for which the Kempner function A002034 is composite. Their density approaches zero as they go to infinity. - _Jud McCranie_, Dec 08 2001",
				"n is a member if and only if P(n) \u003c A002034(n). The members are the exceptions to the rule that P(n) = A002034(n) for almost all n (Erdős and Kastanas 1994, Ivic 2004). - _Jonathan Sondow_, Jan 10 2005",
				"Same as numbers n such that |e - m/n| \u003c 1/(P(n)+1)! for some integer m. - _Jonathan Sondow_, Dec 29 2007"
			],
			"reference": [
				"Steven R. Finch, Mathematical Constants, Cambridge, 2003, pp. 284-292."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A057109/b057109.txt\"\u003eTable of n, a(n) for n = 1..50000\u003c/a\u003e (first 1750 terms from Vincenzo Librandi)",
				"Paul Erdős and Ilias Kastanas, \u003ca href=\"http://www.jstor.org/stable/2324376\"\u003eSolution 6674: The smallest factorial that is a multiple of n\u003c/a\u003e, Amer. Math. Monthly 101 (1994) 179.",
				"Steven R. Finch, \u003ca href=\"http://www.people.fas.harvard.edu/~sfinch/constant/golomb/smarand/smarand1.html\"\u003eThe Average Value of the Smarandache Function\u003c/a\u003e [Broken link]",
				"Steven R. Finch, \u003ca href=\"http://fs.unm.edu/SF/TheAverageValue.pdf\"\u003eThe Average Value of the Smarandache Function\u003c/a\u003e",
				"Kevin Ford, \u003ca href=\"https://faculty.math.illinois.edu/~ford/wwwpapers/nPn.pdf\"\u003eOn integers n for which n does not divide P(n)!\u003c/a\u003e, University of Illinois at Urbana-Champaign (2019).",
				"A. Ivic (2004), \u003ca href=\"http://arXiv.org/abs/math/0311056\"\u003eOn a problem of Erdos involving the largest prime factor of n\u003c/a\u003e, arXiv:math/0311056 [math.NT], 2003-2004.",
				"C. Rivera, \u003ca href=\"http://www.primepuzzles.net/conjectures/conj_027.htm\"\u003eConjecture about their density\u003c/a\u003e",
				"J. Sondow, \u003ca href=\"http://www.jstor.org/stable/27642006\"\u003eA geometric proof that e is irrational and a new measure of its irrationality\u003c/a\u003e, Amer. Math. Monthly 113 (2006) 637-641.",
				"J. Sondow, \u003ca href=\"http://arXiv.org/abs/0704.1282\"\u003eA geometric proof that e is irrational and a new measure of its irrationality\u003c/a\u003e, arXiv:0704.1282 [math.HO], 2007-2010.",
				"J. Sondow and E. W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/SmarandacheFunction.html\"\u003eMathWorld: Smarandache Function\u003c/a\u003e"
			],
			"example": [
				"12 is in the sequence since 3 is the largest prime factor of 12, but 12 is not a factor of 3! = 6."
			],
			"maple": [
				"with(numtheory): for n from 2 to 800 do if ifactors(n)[2][nops(ifactors(n)[2])][1]! mod n \u003c\u003e 0 then printf(`%d,`,n) fi; od:"
			],
			"mathematica": [
				"Select[Range[330],Mod[FactorInteger[#][[-1,1]]!,#] != 0 \u0026] (* _Jean-François Alcover_, May 19 2011 *)"
			],
			"program": [
				"(PARI) is(n)=my(s=factor(n)[, 1]); s[#s]!%n\u003e0 \\\\ _Charles R Greathouse IV_, Sep 20 2012"
			],
			"xref": [
				"Subsequence of A122145.",
				"Cf. A002034, A006530, A057108."
			],
			"keyword": "easy,nonn",
			"offset": "1,1",
			"author": "_Henry Bottomley_, Aug 08 2000",
			"ext": [
				"More terms from _James A. Sellers_, Aug 22 2000"
			],
			"references": 14,
			"revision": 50,
			"time": "2020-02-25T01:02:07-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}