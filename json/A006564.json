{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006564",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6564,
			"id": "M4837",
			"data": "1,12,48,124,255,456,742,1128,1629,2260,3036,3972,5083,6384,7890,9616,11577,13788,16264,19020,22071,25432,29118,33144,37525,42276,47412,52948,58899,65280,72106,79392,87153,95404,104160,113436,123247,133608",
			"name": "Icosahedral numbers: a(n) = n*(5*n^2 - 5*n + 2)/2.",
			"comment": [
				"Schlaefli symbol for this polyhedron: {3,5}.",
				"One of the 5 Platonic polyhedral (tetrahedral, cube, octahedral, dodecahedral and icosahedral) numbers (cf. A053012). - _Daniel Forgues_, May 14 2010"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A006564/b006564.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Hyun Kwang Kim, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9939-02-06710-2\"\u003eOn Regular Polytope Numbers\u003c/a\u003e, Proc. Amer. Math. Soc., Vol. 131, No. 1 (2002), pp. 65-75.",
				"Victor Meally, \u003ca href=\"/A006556/a006556.pdf\"\u003eLetter to N. J. A. Sloane\u003c/a\u003e, no date.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992; arXiv:0911.4975 [math.NT], 2009.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992.",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-6,4,-1)."
			],
			"formula": [
				"a(n) = C(n+2,3) + 8*C(n+1,3) + 6*C(n,3).",
				"a(n) = 4*a(n-1) - 6*a(n-2) + 4*a(n-3) - a(n-4) with a(0)=1, a(1)=12, a(2)=48, a(3)=124. - _Harvey P. Dale_, May 26 2011",
				"G.f.: x*(6*x^2 + 8*x + 1)/(x-1)^4. - _Harvey P. Dale_, May 26 2011",
				"a(n) = A006566(n) - A035006(n). - _Peter M. Chema_, May 04 2016",
				"E.g.f.: x*(2 + 10*x + 5*x^2)*exp(x)/2. - _Ilya Gutkovskiy_, May 04 2016",
				"Sum_{n\u003e=1} 1/a(n) = A175578. - _Amiram Eldar_, Jan 03 2022"
			],
			"maple": [
				"A006564:=(1+8*z+6*z**2)/(z-1)**4; # conjectured by _Simon Plouffe_ in his 1992 dissertation"
			],
			"mathematica": [
				"Table[n (5n^2-5n+2)/2,{n,40}] (* or *) LinearRecurrence[{4,-6,4,-1}, {1,12,48,124},40] (* _Harvey P. Dale_, May 26 2011 *)"
			],
			"program": [
				"(MAGMA) [(5*n^3-5*n^2+2*n)/2: n in [1..100]] // _Vincenzo Librandi_, Nov 21 2010",
				"(Haskell)",
				"a006564 n = n * (5 * n * (n - 1) + 2) `div` 2",
				"-- _Reinhard Zumkeller_, Jun 16 2013",
				"(PARI) a(n)=5*n^2*(n-1)/2+n \\\\ _Charles R Greathouse IV_, Oct 07 2015"
			],
			"xref": [
				"Cf. A000566, A053012, A175578.",
				"Cf. A000292 (tetrahedral numbers), A000578 (cubes), A005900 (octahedral numbers), A006566 (dodecahedral numbers)."
			],
			"keyword": "nonn,nice,easy,changed",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"references": 22,
			"revision": 89,
			"time": "2022-01-04T02:43:25-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}