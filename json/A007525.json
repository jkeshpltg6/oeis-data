{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007525",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7525,
			"id": "M3221",
			"data": "1,4,4,2,6,9,5,0,4,0,8,8,8,9,6,3,4,0,7,3,5,9,9,2,4,6,8,1,0,0,1,8,9,2,1,3,7,4,2,6,6,4,5,9,5,4,1,5,2,9,8,5,9,3,4,1,3,5,4,4,9,4,0,6,9,3,1,1,0,9,2,1,9,1,8,1,1,8,5,0,7,9,8,8,5,5,2,6,6,2,2,8,9,3,5,0,6,3,4,4,4,9,6,9,9",
			"name": "Decimal expansion of log_2 e.",
			"comment": [
				"Around 1670, James Gregory discovered by inversion of 1 - 1/2 + 1/3 - 1/4 + 1/5 - ... = log(2) that 1 + 1/2 - 1/12 + 1/24 - 19/720 + (27/1440 = 3/160) - 863/60480 + ... = 1/log(2). See formula with A002206 and A002207. See also A141417 signed /A091137; case i = 0 in A165313. First row in array p. 36 of the reference. - _Paul Curtz_, Sep 12 2011",
				"This constant 1/log(2) is also related to the asymptotic evaluation of the maximum number of subtraction steps required to compute gcd(m, n) by the binary Euclidean algorithm, m and n being odd and chosen at random. - _Jean-François Alcover_, Jun 23 2014, after _Steven Finch_"
			],
			"reference": [
				"Paul Curtz, Intégration numérique des systèmes différentiels .. , note n° 12, Centre de Calcul Scientifique de l'Armement, Arcueil, 1969.",
				"Steven R. Finch, Mathematical Constants, Cambridge University Press, 2003, Section 2.18 Porter-Hensley constants, p. 159.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A007525/b007525.txt\"\u003eTable of n, a(n) for n = 1..5000\u003c/a\u003e",
				"Simon Plouffe, \u003ca href=\"http://www.worldwideschool.org/library/books/sci/math/MiscellaneousMathematicalConstants/chap3.html\"\u003e1/log(2) the inverse of the natural logarithm of 2\u003c/a\u003e",
				"Srinivasa Ramanujan, \u003ca href=\"http://www.imsc.res.in/~rao/ramanujan/CamUnivCpapers/question/q769.htm\"\u003eQuestion 769\u003c/a\u003e, J. Ind. Math. Soc."
			],
			"formula": [
				"Equals lim_{n-\u003einfinity} A000670(n)/A052882(n). - _Mats Granvik_, Aug 10 2009",
				"Equals Sum_{k\u003e=-1} A002206(k)/A002207(k). - _Paul Curtz_, Sep 12 2011",
				"Also equals integral_{x\u003e=2} 1/(x*log(x)^2). - _Jean-François Alcover_, May 24 2013",
				"1/log(2) = Sum_{n = -infinity..infinity} (2^n / (1 + 2^2^n)). - _Nicolas Nagel_, Mar 16 2018",
				"More generally: 1/log(2) = Sum_{n = -infinity..infinity} (2^(n+x) / (1 + 2^2^(n+x))) for all real x. - _Nicolas Nagel_, Jul 02 2019"
			],
			"example": [
				"1.442695040888963407359924681..."
			],
			"mathematica": [
				"RealDigits[N[1/Log[2], 105]][[1]] (* _Jean-François Alcover_, Oct 30 2012 *)"
			],
			"program": [
				"(PARI) 1/log(2) \\\\ _Charles R Greathouse IV_, Jan 04 2016"
			],
			"keyword": "nonn,cons,easy",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"references": 6,
			"revision": 64,
			"time": "2019-07-03T04:06:56-04:00",
			"created": "1994-09-19T03:00:00-04:00"
		}
	]
}