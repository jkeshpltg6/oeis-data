{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A231190",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 231190,
			"data": "7,3,5,1,3,1,1,0,1,1,3,1,5,3,7,1,9,5,11,3,13,7,15,1,17,9,19,5,21,11,23,3,25,13,27,7,29,15,31,2,33,17,35,9,37,19,39,5,41,21,43,11,45,23,47,3,49,25,51,13,53,27,55,7,57,29,59,15,61,31,63,4,65,33,67,17",
			"name": "Numerator of abs(n-8)/(2*n), n \u003e= 1.",
			"comment": [
				"Because 2*sin(Pi*4/n) = 2*cos(Pi*abs(n-8)/(2*n)) = 2*cos(Pi*a(n)/b(n)) with gcd(a(n),b(n)) = 1, one has",
				"  2*sin(Pi*4/n) = R(a(n), x) (mod C(b(n), x)), with x = 2*cos(Pi/b(n)) =: rho(b(n)). The integer Chebyshev R and C polynomials are found in A127672 and A187360, respectively.",
				"  b(n) = A232625(n). This shows that 2*sin(Pi*4/n) is an integer in the algebraic number field Q(rho(b(n))) of degree delta(b(n)), with delta(k) = A055034(k). This degree delta(b(n)) is given in A231193(n), and if gcd(n,2) = 1 it coincides with the one for sin(2*Pi/n) given by A093819(n). See Theorem 3.9 of the I. Niven reference, pp. 37-38, which uses gcd(k, n) = 1. See also the Jan 09 2011 comment on A093819.",
				"a(n) and b(n) = A232625(n) are the k=2 members of a family of pair of sequences p(k,n) and q(k,n), n \u003e= 1, k \u003e= 1, relevant to determine the algebraic degree of 2*sin(Pi*2*k/n) from the trigonometric identity (used in the D. H. Lehmer and I. Niven references) 2*sin(Pi*2*k/n) = 2*cos(Pi*abs(n-4*k)/(2*n)) = 2*cos(Pi*p(k,n)/q(k,n)). This is R(p(k,n), x) (mod C(q(k,n), x)), with x = 2*cos(Pi/q(k,n)) =: rho(q(k,n)). The polynomials R and C have been used above. C(q(k,n), x) is the minimal polynomial of rho(q(k,n)) with degree delta(q(k,n)), which is then the degree, call it deg(k,n), of the integer 2*sin(Pi*2*k/n) in the number field Q(rho(q(k,n))). From Theorem 3.9 of the I. Niven reference deg(k,n) is, for given k, for those n with gcd(k, n) = 1 determined by A093819(n). In general deg(k,n) = A093819(n/gcd(k,n)). For the k=1 instance p(1,n) and q(1,n) see comments on A106609 and A225975."
			],
			"reference": [
				"I. Niven, Irrational Numbers, The Math. Assoc. of America, second printing, 1963, distributed by John Wiley and Sons."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A231190/b231190.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"D. H. Lehmer, \u003ca href=\"http://www.jstor.org/stable/2301023\"\u003eA Note on Trigonometric Algebraic Numbers\u003c/a\u003e, Am. Math. Monthly 40,3 (1933) 165-6."
			],
			"formula": [
				"a(n) = numerator(abs(n-8)/(2*n)), n \u003e= 1.",
				"a(n) = abs(n-8)/gcd(n-8, 16).",
				"a(n) = abs(n-8) if n is odd; if n is even then a(n) = abs(n-8)/2 if n/2 == 1, 3, 5, 7 (mod 8), a(n) = abs(n-8)/4  if n/2 == 2, 6 (mod 8), a(n) = abs(n-8)/8 if n/2 == 0 (mod 8) and a(n) = abs(n-8)/16 if n == 4 (mod 8).",
				"O.g.f.: 1+ x*(7 + 3*x + 5*x^2 + 1*x^3 + 3*x^4 + 1*x^5 + 1*x^6) + N(x)/(1-x^16)^2 , with N(x) = x^9*((1+x^30) + x*(1+x^28)  + 3*x^2*(1+x^26) + x^3*(1+x^24) + 5*x^4*(1+x^22) + 3*x^5*(1+x^20) + 7*x^6*(1+x^18) + x^7*(1+x^16) + 9*x^8*(1+x^14) + 5*x^9*(1+x^12) + 11*x^10*(1+x^10) + 3*x^11*(1+x^8) + 13*x^12*(1+x^6) + 7*x^13*(1+x^4) + 15*x^14*(1+x^2)+x^15).",
				"a(n+32)-2*a(n+16)+a(n) = 0 for n \u003e= 8.",
				"a(n+8) = A106617(n). - _Peter Bala_, Feb 28 2019"
			],
			"maple": [
				"f:= n -\u003e numer(abs(n-8)/(2*n)):",
				"map(f, [$1..100]); # _Robert Israel_, Dec 06 2018"
			],
			"mathematica": [
				"a[n_] := Numerator[Abs[n-8]/(2n)]; Array[a, 50] (* _Amiram Eldar_, Dec 06 2018 *)"
			],
			"xref": [
				"Cf. A127672 (R), A187360 (C), A232625 (b), A055034 (delta), A093819 (degree if k=1), A232626(degree if k=2), A106609 (k=1, p), A225975 (k=1, q), A106617."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_Wolfdieter Lang_, Dec 12 2013",
			"references": 4,
			"revision": 22,
			"time": "2019-02-28T08:20:33-05:00",
			"created": "2013-12-12T10:01:15-05:00"
		}
	]
}