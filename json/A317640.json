{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A317640",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 317640,
			"data": "0,8,1,20,2,36,3,48,4,64,5,76,6,92,7,104,8,120,9,132,10,148,11,160,12,176,13,188,14,204,15,216,16,232,17,244,18,260,19,272,20,288,21,300,22,316,23,328,24,344,25,356,26,372,27,384,28,400,29,412,30,428,31,440,32,456,33",
			"name": "The 7x+-1 function: a(n) = 7n+1 if n == +1 (mod 4), a(n) = 7n-1 if n == -1 (mod 4), otherwise a(n) = n/2.",
			"comment": [
				"The 7x+-1 problem is as follows. Start with any natural number n. If 4 divides n-1, multiply it by 7 and add 1; if 4 divides n+1, multiply it by 7 and subtract 1; otherwise divide it by 2. The 7x+-1 problem concerns the question whether we always reach 1."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A317640/b317640.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"D. Barina, \u003ca href=\"https://arxiv.org/abs/1807.00908\"\u003e7x+-1: Close Relative of Collatz Problem\u003c/a\u003e, arXiv:1807.00908 [math.NT], 2018.",
				"K. Matthews, \u003ca href=\"http://www.numbertheory.org/php/barina.html\"\u003eDavid Barina's 7x+1 conjecture\u003c/a\u003e.",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,1,0,1,0,-1)."
			],
			"formula": [
				"a(n) = a(a(2*n)).",
				"From _Colin Barker_, Aug 03 2018: (Start)",
				"G.f.: x*(8 + x + 12*x^2 + x^3 + 8*x^4) / ((1 - x)^2*(1 + x)^2*(1 + x^2)).",
				"a(n) = a(n-2) + a(n-4) - a(n-6) for n\u003e5.",
				"(End)"
			],
			"example": [
				"a(3)=20 because 3 == -1 (mod 4), and thus 7*3 - 1 results in 20.",
				"a(5)=36 because 5 == +1 (mod 4), and thus 7*5 + 1 results in 36."
			],
			"mathematica": [
				"Array[Which[#2 == 1, 7 #1 + 1, #2 == 3, 7 #1 - 1, True, #1/2] \u0026 @@ {#, Mod[#, 4]} \u0026, 67, 0] (* _Michael De Vlieger_, Aug 02 2018 *)"
			],
			"program": [
				"(C)",
				"int a(int n) {",
				"....switch(n%4) {",
				"........case 1: return 7*n+1;",
				"........case 3: return 7*n-1;",
				"........default: return n/2;",
				"....}",
				"}",
				"(PARI) a(n)={my(m=(n+2)%4-2); if(m%2, 7*n + m, n/2)} \\\\ _Andrew Howroyd_, Aug 02 2018",
				"(PARI) concat(0, Vec(x*(8 + x + 12*x^2 + x^3 + 8*x^4) / ((1 - x)^2*(1 + x)^2*(1 + x^2)) + O(x^70))) \\\\ _Colin Barker_, Aug 03 2018"
			],
			"xref": [
				"Cf. A006370."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_David Barina_, Aug 02 2018",
			"references": 4,
			"revision": 23,
			"time": "2018-08-06T14:04:00-04:00",
			"created": "2018-08-06T04:18:09-04:00"
		}
	]
}