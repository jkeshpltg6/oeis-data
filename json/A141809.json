{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A141809",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 141809,
			"data": "1,2,3,4,5,2,3,7,8,9,2,5,11,4,3,13,2,7,3,5,16,17,2,9,19,4,5,3,7,2,11,23,8,3,25,2,13,27,4,7,29,2,3,5,31,32,3,11,2,17,5,7,4,9,37,2,19,3,13,8,5,41,2,3,7,43,4,11,9,5,2,23,47,16,3,49,2,25,3,17,4,13,53,2,27,5,11,8,7,3",
			"name": "Irregular table: Row n (of A001221(n) terms, for n\u003e=2) consists of the largest powers that divides n of each distinct prime that divides n. Terms are arranged by the size of the distinct primes. Row 1 = (1).",
			"comment": [
				"In other words, except for row 1, row n contains the unitary prime power divisors of n, sorted by the prime. - _Franklin T. Adams-Watters_, May 05 2011",
				"A034684(n) = smallest term of n-th row; A028233(n) = T(n,1); A053585(n) = T(n,A001221(n)); A008475(n) = sum of n-th row for n \u003e 1. - _Reinhard Zumkeller_, Jan 29 2013"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A141809/b141809.txt\"\u003eRows n=1..10000 of triangle, flattened\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PrimeFactorization.html\"\u003ePrime Factorization\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = A027748(n,k)^A124010(n,k) for n \u003e 1, k = 1 .. A001221(n). - _Reinhard Zumkeller_, Mar 15 2012"
			],
			"example": [
				"60 has the prime factorization 2^2 * 3^1 * 5^1, so row 60 is (4,3,5).",
				"From _M. F. Hasler_, Oct 12 2018: (Start)",
				"The table starts:",
				"   n : largest prime powers dividing n",
				"   1 : 1",
				"   2 : 2",
				"   3 : 3",
				"   4 : 4",
				"   5 : 5",
				"   6 : 2, 3",
				"   7 : 7",
				"   8 : 8",
				"   9 : 9",
				"   10 : 2, 5",
				"   11 : 11",
				"   12 : 4, 3",
				"   etc. (End)"
			],
			"mathematica": [
				"f[{x_, y_}] := x^y; Table[Map[f, FactorInteger[n]], {n, 1, 50}] // Grid (* _Geoffrey Critzer_, Apr 03 2015 *)"
			],
			"program": [
				"(Haskell)",
				"a141809 n k = a141809_row n !! (k-1)",
				"a141809_row 1 = [1]",
				"a141809_row n = zipWith (^) (a027748_row n) (a124010_row n)",
				"a141809_tabf = map a141809_row [1..]",
				"-- _Reinhard Zumkeller_, Mar 18 2012",
				"(PARI) A141809_row(n)=if(n\u003e1,apply(factorback,Col(factor(n)))~,[1]) \\\\ _M. F. Hasler_, Oct 12 2018"
			],
			"xref": [
				"A027748, A124010 are used in a formula defining this sequence.",
				"Cf. A001221 (row lengths), A008475 (row sums), A028233 (column 1), A034684 (row minima), A053585 (right edge).",
				"Cf. A060175, A141810, A213925."
			],
			"keyword": "nonn,tabf",
			"offset": "1,2",
			"author": "_Leroy Quet_, Jul 07 2008",
			"references": 21,
			"revision": 33,
			"time": "2020-10-24T11:55:26-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}