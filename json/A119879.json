{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A119879",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 119879,
			"data": "1,0,1,-1,0,1,0,-3,0,1,5,0,-6,0,1,0,25,0,-10,0,1,-61,0,75,0,-15,0,1,0,-427,0,175,0,-21,0,1,1385,0,-1708,0,350,0,-28,0,1,0,12465,0,-5124,0,630,0,-36,0,1,-50521,0,62325,0,-12810,0,1050,0,-45,0,1",
			"name": "Exponential Riordan array (sech(x),x).",
			"comment": [
				"Row sums have e.g.f. exp(x)*sech(x) (signed version of A009006). Inverse of masked Pascal triangle A119467. Transforms the sequence with e.g.f. g(x) to the sequence with e.g.f. g(x)*sech(x).",
				"Coefficients of the Swiss-Knife polynomials for the computation of Euler, tangent and Bernoulli number (triangle read by rows). Another version in A153641. - _Philippe Deléham_, Oct 26 2013",
				"Relations to Green functions and raising/creation and lowering/annihilation/destruction operators are presented in Hodges and Sukumar and in Copeland's discussion of this sequence and 2020 pdf. - _Tom Copeland_, Jul 24 2020"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A119879/b119879.txt\"\u003eRows n=0..100 of triangle, flattened\u003c/a\u003e",
				"P. Barry, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL14/Barry1/barry97r2.html\"\u003eRiordan Arrays, Orthogonal Polynomials as Moments, and Hankel Transforms\u003c/a\u003e, J. Int. Seq. 14 (2011) # 11.2.2, example 28.",
				"T. Copeland, \u003ca href=\"https://tcjpn.wordpress.com/2015/10/12/the-elliptic-lie-triad-kdv-and-ricattt-equations-infinigens-and-elliptic-genera/\"\u003eThe Elliptic Lie Triad: Ricatti and KdV Equations, Infinigens, and Elliptic Genera\u003c/a\u003e, 2015.",
				"T. Copeland, \u003ca href=\"/A119879/a119879.txt\"\u003eDiscussion of this sequence\u003c/a\u003e",
				"T. Copeland, \u003ca href=\"https://tcjpn.wordpress.com/2020/07/11/skipping-over-dimensions-juggling-zeros-in-the-matrix/\"\u003eSKipping over Dimensions, Juggling Zeros in the Matrix\u003c/a\u003e, 2020.",
				"A. Hodges and C. V. Sukumar, \u003ca href=\"http://dx.doi.org/10.1098/rspa.2007.0001\"\u003eBernoulli, Euler, permutations and quantum algebras\u003c/a\u003e, Proc. R. Soc. A Oct. 2007 vol 463 no. 463 2086 2401-2414.",
				"Peter Luschny, \u003ca href=\"http://www.luschny.de/math/seq/SwissKnifeDecompositions.html\"\u003eAdditive decompositions of classical numbers via the Swiss-Knife polynomials\u003c/a\u003e [_Tom Copeland_, Oct 20 2015]",
				"Miguel Méndez, Rafael Sánchez, \u003ca href=\"https://arxiv.org/abs/1707.00336\"\u003eOn the combinatorics of Riordan arrays and Sheffer polynomials: monoids, operads and monops\u003c/a\u003e, arXiv:1707.00336 [math.CO}, 2017, Section 4.3, Example 4.",
				"Miguel A. Méndez, Rafael Sánchez Lamoneda, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v25i3p25\"\u003eMonops, Monoids and Operads: The Combinatorics of Sheffer Polynomials\u003c/a\u003e, The Electronic Journal of Combinatorics 25(3) (2018), #P3.25."
			],
			"formula": [
				"Number triangle whose k-th column has e.g.f. sech(x)*x^k/k!.",
				"T(n,k) = C(n,k)*2^(n-k)*E_{n-k}(1/2) where C(n,k) is the binomial coefficient and E_{m}(x) are the Euler polynomials. - _Peter Luschny_, Jan 25 2009",
				"The coefficients in ascending order of x^i of the polynomials p{0}(x) = 1 and p{n}(x) = Sum_{k=0..n-1; k even} binomial(n,k)*p{k}(0)*((n mod 2) - 1 + x^(n-k)). - _Peter Luschny_, Jul 16 2012",
				"E.g.f.: exp(x*z)/cosh(x). - _Peter Luschny_, Aug 01 2012",
				"Sum_{k=0..n} T(n,k)*x^k = A122045(n), A155585(n), A119880(n), A119881(n) for x = 0, 1, 2, 3 respectively. - _Philippe Deléham_, Oct 27 2013",
				"With all offsets 0, let A_n(x;y) = (y + E.(x))^n, an Appell sequence in y where E.(x)^k = E_k(x) are the Eulerian polynomials of A123125. Then the row polynomials of A046802 (the h-polynomials of the stellahedra) are given by h_n(x) = A_n(x;1); the row polynomials of A248727 (the face polynomials of the stellahedra), by f_n(x) = A_n(1 + x;1); the Swiss-knife polynomials of this entry, A119879, by Sw_n(x) = A_n(-1;1 + x); and the row polynomials of the Worpitsky triangle (A130850), by w_n(x) = A(1 + x;0). Other specializations of A_n(x;y) give A090582 (the f-polynomials of the permutohedra, cf. also A019538) and A028246 (another version of the Worpitsky triangle). - _Tom Copeland_, Jan 24 2020"
			],
			"example": [
				"Triangle begins:",
				"     1;",
				"     0,    1;",
				"    -1,    0,     1;",
				"     0,   -3,     0,   1;",
				"     5,    0,    -6,   0,   1;",
				"     0,   25,     0, -10,   0,   1;",
				"   -61,    0,    75,   0, -15,   0,   1;",
				"     0, -427,     0, 175,   0, -21,   0,  1;",
				"  1385,    0, -1708,   0, 350,   0, -28,  0,  1;"
			],
			"maple": [
				"T := (n,k) -\u003e binomial(n,k)*2^(n-k)*euler(n-k,1/2): # _Peter Luschny_, Jan 25 2009"
			],
			"mathematica": [
				"T[n_, k_] := Binomial[n, k] 2^(n-k) EulerE[n-k, 1/2];",
				"Table[T[n, k], {n, 0, 10}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Jun 20 2018, after _Peter Luschny_ *)"
			],
			"program": [
				"(Sage)",
				"@CachedFunction",
				"def A119879_poly(n, x) :",
				"    return 1 if n == 0  else add(A119879_poly(k, 0)*binomial(n, k)*(x^(n-k)-1+n%2) for k in range(n)[::2])",
				"def A119879_row(n) :",
				"    R = PolynomialRing(ZZ, 'x')",
				"    return R(A119879_poly(n,x)).coeffs()  # _Peter Luschny_, Jul 16 2012",
				"# Alternatively:",
				"(Sage) # uses[riordan_array from A256893]",
				"riordan_array(sech(x), x, 9, exp=true) # _Peter Luschny_, Apr 19 2015",
				"(PARI)",
				"{T(n,k) = binomial(n,k)*2^(n-k)*(2/(n-k+1))*(subst(bernpol(n-k+1, x), x, 1/2) - 2^(n-k+1)*subst(bernpol(n-k+1, x), x, 1/4))};",
				"for(n=0,5, for(k=0,n, print1(T(n,k), \", \"))) \\\\ _G. C. Greubel_, Feb 25 2019"
			],
			"xref": [
				"Row sums are A155585. - _Johannes W. Meijer_, Apr 20 2011",
				"Rows reversed: A081658.",
				"Cf. A109449, A153641, A162660. - _Philippe Deléham_, Oct 26 2013",
				"Cf. A000182, A046802, A119467, A133314.",
				"Cf. A019538, A028246, A090582, A123125, A130850, A248727."
			],
			"keyword": "easy,sign,tabl",
			"offset": "0,8",
			"author": "_Paul Barry_, May 26 2006",
			"references": 17,
			"revision": 119,
			"time": "2020-09-04T15:47:24-04:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}