{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A344855",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 344855,
			"data": "1,0,1,0,1,1,0,2,3,1,0,4,11,6,1,0,8,40,35,10,1,0,16,148,195,85,15,1,0,32,560,1078,665,175,21,1,0,64,2160,5992,5033,1820,322,28,1,0,128,8448,33632,37632,17913,4284,546,36,1,0,256,33344,190800,280760,171465,52941,9030,870,45,1",
			"name": "Number T(n,k) of permutations of [n] having k cycles of the form (c1, c2, ..., c_m) where c1 = min_{i\u003e=1} c_i and c_j = min_{i\u003e=j} c_i or c_j = max_{i\u003e=j} c_i; triangle T(n,k), n\u003e=0, 0\u003c=k\u003c=n, read by rows.",
			"comment": [
				"The sequence of column k satisfies a linear recurrence with constant coefficients of order k*(k+1)/2 = A000217(k)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A344855/b344855.txt\"\u003eRows n = 0..140, flattened\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Permutation\"\u003ePermutation\u003c/a\u003e"
			],
			"formula": [
				"Sum_{k=1..n} k * T(n,k) = A345341(n).",
				"For fixed k, T(n,k) ~ (2*k)^n / (4^k * k!). - _Vaclav Kotesovec_, Jul 15 2021"
			],
			"example": [
				"T(4,1) = 4: (1234), (1243), (1423), (1432).",
				"Triangle T(n,k) begins:",
				"  1;",
				"  0,  1;",
				"  0,  1,    1;",
				"  0,  2,    3,    1;",
				"  0,  4,   11,    6,    1;",
				"  0,  8,   40,   35,   10,    1;",
				"  0, 16,  148,  195,   85,   15,   1;",
				"  0, 32,  560, 1078,  665,  175,  21,  1;",
				"  0, 64, 2160, 5992, 5033, 1820, 322, 28, 1;",
				"  ..."
			],
			"maple": [
				"b:= proc(n) option remember; `if`(n=0, 1, add(expand(x*",
				"      b(n-j)*binomial(n-1, j-1)*ceil(2^(j-2))), j=1..n))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=0..n))(b(n)):",
				"seq(T(n), n=0..12);"
			],
			"mathematica": [
				"b[n_] := b[n] = If[n == 0, 1, Sum[Expand[x*b[n-j]*",
				"     Binomial[n-1, j-1]*Ceiling[2^(j-2)]], {j, n}]];",
				"T[n_] := CoefficientList[b[n], x];",
				"Table[T[n], {n, 0, 12}] // Flatten (* _Jean-François Alcover_, Aug 23 2021, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Columns k=0-10 give: A000007, A166444, A346317, A346318, A346319, A346320, A346321, A346322, A346323, A346324, A346325.",
				"Row sums give A187251.",
				"Main diagonal gives A000012, lower diagonal gives A000217, second lower diagonal gives A000914.",
				"T(n+1,n) gives A000217.",
				"T(n+2,n) gives A000914.",
				"T(2n,n) gives A345342.",
				"Cf. A060281, A132393, A186366, A345341."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Alois P. Heinz_, May 30 2021",
			"references": 13,
			"revision": 42,
			"time": "2021-12-19T12:21:10-05:00",
			"created": "2021-06-20T20:14:44-04:00"
		}
	]
}