{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A282197",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 282197,
			"data": "1,2,7,15,52,102,296,371,455,929,1853,2034,4517,4797,5829,6146,6948,17577,19818,18915,60349,78369,113010,110185,91650,85171,311321,123788,823049,128596,1650408,1136865,415355,906771,2897535",
			"name": "a(n) is the smallest number d if the point (d,d) is shared by exactly n different Dyck paths in the main diagonal of the diagram of the symmetries of sigma described in A237593.",
			"comment": [
				"This sequence is not monotone since a(19) = 19818 \u003e 18915 = a(20).",
				"Additional values smaller than 5000000 are a(37) = 1751785, a(38) = 1786732, a(39) = 1645139, a(41) = 1308771 and a(44) = 3329668.",
				"Sequence A128605 of first occurrences of gaps between adjacent Dyck paths appears to be unrelated to this sequence.",
				"First differs from A279286 (which is monotone) at a(19). - _Omar E. Pol_, Feb 08 2017",
				"a(n) = d if the point (d,d) belongs to the first vertical-line-segment of exactly length n found in the main diagonal of the pyramid described in A245092 (starting from the top). The diagram of the symmetries of sigma is also the top view of the pyramid. - _Omar E. Pol_, Feb 09 2017"
			],
			"example": [
				"The four examples listed in A279286 are also examples for this sequences.",
				"a(20) = 18915 is in the sequence since it is the first time that exactly 20 Dyck paths meet on the diagonal though a concurrence of exactly 19 paths on the diagonal happens only later at a(19) = 19818."
			],
			"mathematica": [
				"a240542[n_] := Sum[(-1)^(k+1)*Ceiling[(n+1)/k - (k+1)/2], {k, 1, Floor[(Sqrt[8n+1]-1)/2]}]",
				"(* parameter cL must be sufficiently large for bound b *)",
				"a282197[cL_, b_] := Module[{centers=Map[0\u0026, Range[cL]], acc={1}, k=2, cPrev=1, cCur, len}, While[k\u003c=b, cCur=a240542[k]; If[Last[acc]==cCur, AppendTo[acc,cCur], len=Length[acc]; If[centers[[len]]==0, centers[[len]]=cPrev]; acc={cCur}; cPrev=cCur]; k++]; centers]",
				"a282197[50, 5000000] (* data *)",
				"(* list processing implementation useful for \"small\" arguments only *)",
				"a282197F[n_] := Map[Last, Sort[Normal[Map[First[First[#]]\u0026, GroupBy[Split[Map[a240542, Range[n]]], Length[#]\u0026]]]]]",
				"a282197F[50000] (* computes a(1) .. a(20) *)"
			],
			"xref": [
				"Cf. A128605, A237593, A240542, A245092, A279286."
			],
			"keyword": "nonn,hard,more",
			"offset": "1,2",
			"author": "_Hartmut F. W. Hoft_, Feb 08 2017",
			"references": 4,
			"revision": 29,
			"time": "2017-02-10T01:13:39-05:00",
			"created": "2017-02-10T01:13:39-05:00"
		}
	]
}