{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A086019",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 86019,
			"data": "0,0,0,31,0,257,73,683,113,331,109,61681,5419,2796203,1613,3033169,1321,599479,122921,38737,22366891,8831418697,2931542417,22253377,268501,131071,28059810762433,279073,54410972897,77158673929,145295143558111",
			"name": "For p = prime(n), a(n) is the largest prime q such that pq is a base-2 pseudoprime; that is, 2^(pq-1) = 1 mod pq; a(n) is 0 if no such prime exists.",
			"comment": [
				"Using a theorem of Lehmer, it can be shown that the possible values of q are among the prime factors of 2^(p-1)-1. Sequence A085012 gives the smallest prime q such that 2^(pq-1) = 1 mod pq. Sequence A085014 gives the number of 2-factor pseudoprimes that have prime(n) as a factor."
			],
			"reference": [
				"Paulo Ribenboim, The New Book of Prime Number Records, Springer, 1996, p. 105-112."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A086019/b086019.txt\"\u003eTable of n, a(n) for n = 2..337\u003c/a\u003e",
				"Paul Erdős, \u003ca href=\"http://www.jstor.org/stable/2304732\"\u003eOn the converse of Fermat's theorem\u003c/a\u003e, Amer. Math. Monthly 56 (1949), p. 623-624.",
				"D. H. Lehmer, \u003ca href=\"http://www.jstor.org/stable/2301798\"\u003eOn the converse of Fermat's theorem\u003c/a\u003e, Amer. Math. Monthly 43 (1936), p. 347-354."
			],
			"example": [
				"a(9) = 683 because prime(9) = 23 and 683 is the largest factor of 2^22-1 that yields a pseudoprime when multiplied by 23."
			],
			"mathematica": [
				"Table[p=Prime[n]; q=Reverse[Transpose[FactorInteger[2^(p-1)-1]][[1]]]; i=1; While[i\u003c=Length[q]\u0026\u0026(PowerMod[2, p*q[[i]]-1, p*q[[i]]]\u003e1), i++ ]; If[i\u003eLength[q], 0, q[[i]]], {n, 2, 40}]"
			],
			"xref": [
				"Cf. A001567 (base 2 pseudoprimes), A085012, A085014, A180471."
			],
			"keyword": "hard,nonn",
			"offset": "2,4",
			"author": "_T. D. Noe_, Jul 08 2003",
			"references": 3,
			"revision": 14,
			"time": "2021-03-27T08:00:04-04:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}