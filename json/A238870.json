{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A238870",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 238870,
			"data": "1,1,0,1,1,0,2,2,1,4,4,4,9,10,11,21,25,30,51,62,80,125,157,208,309,399,536,772,1013,1373,1938,2574,3503,4882,6540,8918,12329,16611,22672,31183,42182,57588,78952,107092,146202,200037,271831,371057,507053,689885,941558,1285655,1750672,2388951,3260459,4442179,6060948",
			"name": "Number of compositions of n with c(1) = 1, c(i+1) - c(i) \u003c= 1, and c(i+1) - c(i) != 0.",
			"comment": [
				"Number of fountains of n coins with at most two successive coins on the same level."
			],
			"link": [
				"Joerg Arndt and Alois P. Heinz, \u003ca href=\"/A238870/b238870.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e"
			],
			"formula": [
				"a(n) ~ c / r^n, where r = 0.733216317061133379740342579187365700397652443391231594... and c = 0.172010618097928709454463097802313209201440229976513439... . - _Vaclav Kotesovec_, Feb 17 2017"
			],
			"example": [
				"The a(10) = 4 such compositions are:",
				":",
				":   1:  [ 1 2 1 2 1 2 1 ]  (composition)",
				":",
				":  o o o",
				": ooooooo   (rendering as composition)",
				":",
				":     O   O   O",
				":    O O O O O O O  (rendering as fountain of coins)",
				":",
				":",
				":   2:  [ 1 2 1 2 3 1 ]",
				":",
				":     o",
				":  o oo",
				": oooooo",
				":",
				":           O",
				":      O   O O",
				":     O O O O O O",
				":",
				":",
				":   3:  [ 1 2 3 1 2 1 ]",
				":",
				":   o",
				":  oo o",
				": oooooo",
				":",
				":       O",
				":      O O   O",
				":     O O O O O O",
				":",
				":",
				":   4:  [ 1 2 3 4 ]",
				":",
				":    o",
				":   oo",
				":  ooo",
				": oooo",
				":",
				":         O",
				":        O O",
				":       O O O",
				":      O O O O",
				":"
			],
			"maple": [
				"b:= proc(n, i) option remember; `if`(n=0, 1, add(",
				"      `if`(i=j, 0, b(n-j, j)), j=1..min(n, i+1)))",
				"    end:",
				"a:= n-\u003e b(n, 0):",
				"seq(a(n), n=0..60);  # _Alois P. Heinz_, Mar 11 2014"
			],
			"mathematica": [
				"b[n_, i_] := b[n, i] = If[n == 0, 1, Sum[If[i == j, 0, b[n-j, j]], {j, 1, Min[n, i+1]}]];",
				"a[n_] := b[n, 0];",
				"a /@ Range[0, 60] (* _Jean-François Alcover_, Nov 07 2020, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(Sage) # translation of the Maple program by _Alois P. Heinz_",
				"@CachedFunction",
				"def F(n, i):",
				"    if n == 0: return 1",
				"    return sum( (i!=j) * F(n-j, j) for j in [1..min(n,i+1)] ) # A238870",
				"#    return sum( F(n-j, j) for j in [1, min(n,i+1)] ) # A005169",
				"def a(n): return F(n, 0)",
				"print([a(n) for n in [0..50]])",
				"# _Joerg Arndt_, Mar 20 2014"
			],
			"xref": [
				"Cf. A005169 (fountains of coins), A001524 (weakly unimodal fountains of coins).",
				"Cf. A186085 (1-dimensional sand piles), A227310 (rough sand piles).",
				"Cf. A023361 (fountains of coins with all valleys at lowest level)."
			],
			"keyword": "nonn",
			"offset": "0,7",
			"author": "_Joerg Arndt_, Mar 09 2014",
			"references": 3,
			"revision": 27,
			"time": "2020-11-07T14:08:48-05:00",
			"created": "2014-03-10T08:38:02-04:00"
		}
	]
}