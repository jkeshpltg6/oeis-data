{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A327759",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 327759,
			"data": "1,2,2,3,1,2,1,4,5,1,2,4,1,5,1,2,5,1,2,8,4,5,3,4,3,6,1,8,3,2,5,4,7,6,2,3,9,1,2,12,4,5,3,4,8,9,7,8,3,10,1,12,3,2,5,4,7,6,9,8,11,10,2,3,13,1,2,16,4,5,3,4,8,9,7,8,12,13,11,12,3",
			"name": "a(1) = 1; a(2) = 2; a(n) = n - max{k\u003cn | a(k) = m(n)}, where m(n) is the largest term so far if a(n-1) is odd, the second largest term so far if a(n-1) is even.",
			"comment": [
				"From _M. F. Hasler_, Sep 29 2019:",
				"The sequence may be seen as a table with rows of length |2n-1|, n = 0, 1, ...",
				"Then from n = 5, a(18) = 1 on, the rows are of the form",
				"row(n) = (1, 2, 2n-2, ((4k, 4k+1, 4k-1, 4k), k=1..(n-3)/2), 3, 2n-4) for odd n,",
				"row(n) = (1, 2n-4, ((2k+1, 2k), k=1..n-3), 2, 3, 2n-3) for even n.",
				"All rows n \u003e= 5 start with a((n-1)^2 + 2) = 1, and there are no other '1's beyond a(15).",
				"(End)"
			],
			"formula": [
				"a(n) = 1 iff n is in {1, 5, 7, 10, 13, 15} union A059100 \\ { 2, 3, 6, 11 }."
			],
			"example": [
				"a(9) is odd. The largest term up to that point is 5. The largest index of 5 is 9. a(10) = 10 - 9 = 1.",
				"a(16) is even. The second largest term up to that point is 4. The largest index of 4 is 12. a(17) = 17 - 12 = 5.",
				"From _M. F. Hasler_, Sep 29 2019: (Start)",
				"Written as a table with rows of length |2n-1|, n = 0, 1, ...:",
				"   1,  /* row n=0 */",
				"   2,  /* row n=1; from here on, length = 2n-1 */",
				"   2, 3, 1,  /* row n=2 */",
				"   2, 1, 4, 5, 1,  /* row n=3 */",
				"   2, 4, 1, 5, 1, 2, 5,  /* n=4 */",
				"   1, 2, 8, 4, 5, 3, 4, 3, 6,  /* n=5. Here starts the regular pattern. */",
				"   1, 8, 3, 2, 5, 4, 7, 6, 2, 3, 9,  /* n=6 */",
				"   1, 2, 12, 4, 5, 3, 4, 8, 9, 7, 8, 3, 10,  /* n=7 */",
				"   1, 12, 3, 2, 5, 4, 7, 6, 9, 8, 11, 10, 2, 3, 13,  /* n=8 */",
				"   1, 2, 16, 4, 5, 3, 4, 8, 9, 7, 8, 12, 13, 11, 12, 3, 14,  /* n=9 */",
				"   1, 16, 3, 2, 5, 4, 7, 6, 9, 8, 11, 10, 13, 12, 15, 14, 2, 3, 17,  /* n=10 */",
				"   ...",
				"(End)"
			],
			"mathematica": [
				"s={1, 2}; sm = 2; sm2 = 1; Do[a = Length[s] + 1 - If[OddQ[s[[-1]]], Position[s, _?(# == sm \u0026)], Position[s, _?(# == sm2 \u0026)]][[-1, 1]]; AppendTo[s, a]; If[a \u003e sm, sm2 = sm; sm = a,If[a \u003c sm \u0026\u0026 a \u003e sm2, sm2 = a]], {100}]; s (* _Amiram Eldar_, Sep 28 2019 *)"
			],
			"program": [
				"(VBA/Excel)",
				"Sub A327759()",
				"Cells(1, 1) = 1",
				"Cells(2, 1) = 2",
				"For n = 3 To 1000",
				"   max1 = 0",
				"   For m = 1 To n - 1",
				"      If Cells(m, 1) \u003e= max1 Then",
				"          max1 = Cells(m, 1)",
				"          m1 = m",
				"      End If",
				"   Next m",
				"   max2 = 0",
				"   For m = 1 To n - 1",
				"      If Cells(m, 1) \u003c\u003e max1 And Cells(m, 1) \u003e= max2 Then",
				"         max2 = Cells(m, 1)",
				"         m2 = m",
				"      End If",
				"   Next m",
				"   If Cells(n - 1, 1) Mod 2 = 1 Then",
				"      Cells(n, 1) = n - m1",
				"   Else",
				"      Cells(n, 1) = n - m2",
				"   End If",
				"Next n",
				"End Sub",
				"(PARI) A327759_upto(N=99, idx=[0,0], L, S, a)=vector(N,n,a=n-if(n\u003e2,idx[2-a%2]); L\u003ca \u0026\u0026 [S,idx[2]]=[L,idx[1]]; if(L\u003c=a, L=a; idx[1]=n, S\u003c=a, S=a; idx[2]=n); a) \\\\ _M. F. Hasler_, Sep 29 2019",
				"(PARI) A327759(n)={my(r=sqrtint(abs(n-2))+1,c=n-(r-1)^2-1); if(n\u003c17, digits(1223121451241512)[n], c==1, 1, c==2*r-2, 3,c==2*r-1, 2*r-3-r%2, r%2, if(c==3, 2*r-2, c\u003e2, c\\4*4+[0,1,-1,0][c-c\\4*4+1], 2), c==2, 2*r-4, c\u003c2*r-3, c\\/2*2+(c%2)-2,2)} \\\\ _M. F. Hasler_, Sep 30 2019"
			],
			"xref": [
				"Cf. A059100 (indices of '1's, starting with 18), A141044 (col.1, starting at row 3)."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Ali Sada_, Sep 24 2019",
			"references": 1,
			"revision": 31,
			"time": "2019-10-16T13:29:27-04:00",
			"created": "2019-10-16T13:29:27-04:00"
		}
	]
}