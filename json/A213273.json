{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A213273",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 213273,
			"data": "2,4,7,9,11,15,17,21,23,27,29,32,37,40,43,46,49,53,57,61,63,67,71,73,77,81,83,88,92,97,100,103,107,111,113,118,122,125,128,133,135,139,143,147,149,153,157,163,165,167,171,173,178,181,188,191,194,197,202",
			"name": "The smallest m such that the complete bipartite graph K_{n,n} has a coprime labeling using labels from {1,...,m}.",
			"comment": [
				"A prime labeling of a graph G is a labeling of the vertices with the integers 1, 2, ..., v (where v is the number of vertices) such that any two adjacent vertices have labels that are relatively prime. Here we are allowing the largest label m \u003e= v and calling that a coprime labeling. Our goal is to find the smallest m that makes the labeling possible for K_{n,n} (which clearly does not have a prime labeling for n\u003e2)."
			],
			"link": [
				"Kevin Cuadrado, \u003ca href=\"/A213273/b213273.txt\"\u003eTable of n, a(n) for n = 1..2000\u003c/a\u003e (terms 1..14 from Adam H. Berliner, N. Dean, J. Hook, A. Marr, A. Mbirika \u0026 ​C. McBee, terms 14..23 from Alois P. Heinz, terms 24..96 from Paul Tabatabai).",
				"Adam H. Berliner, N. Dean, J. Hook, A. Marr, A. Mbirika, ​and C. McBee, \u003ca href=\"https://arxiv.org/abs/1604.07698\"\u003eCoprime and prime labelings of graphs\u003c/a\u003e, arXiv preprint arXiv:1604.07698 [math.CO], 2016.",
				"Gary Chartrand, Cooroo Egan, and Ping Zhang, \u003ca href=\"https://doi.org/10.1007/978-3-030-16863-6_3\"\u003e\"Harmonious Labelings\"\u003c/a\u003e, How to Label a Graph (2019), Springer Briefs in Mathematics, Springer, Cham, 21-28.",
				"Catherine Lee, \u003ca href=\"https://arxiv.org/abs/1907.12670\"\u003eMinimum coprime graph labelings\u003c/a\u003e, arXiv:1907.12670 [math.CO], 2019."
			],
			"example": [
				"For n=12 and K_{12,12} the two independent sets would be labeled {1,3,5,9,15,17,19,23,25,27,29,31} and {2,4,7,8,11,13,14,16,22,26,28,32}."
			],
			"maple": [
				"b:= proc(n, k, t, s) option remember;",
				"      nops(s)\u003e=t and (k\u003e=t or n\u003e1 and (b(n-1, k, t, s) or",
				"      b(n-1, k+1, t, select(x-\u003e igcd(n, x)=1, s))))",
				"    end:",
				"a:= proc(n) option remember; local m; forget(b);",
				"      for m from `if`(n=1, 1, a(n-1))",
				"      while not b(m, 1, n, {$2..m}) do od; m",
				"    end:",
				"seq(a(n), n=1..14);  # _Alois P. Heinz_, Jun 16 2012"
			],
			"mathematica": [
				"b[n_, k_, t_, s_] := b[n, k, t, s] = Length[s] \u003e= t \u0026\u0026 (k \u003e= t || n \u003e 1 \u0026\u0026 (b[n - 1, k, t, s] || b[n - 1, k + 1, t, Select[s, GCD[n, #] == 1 \u0026]]));",
				"a[n_] := a[n] = Module[{m}, m = If[n == 1, 1, a[n - 1]]; While[!b[m, 1, n, Range[2, m]], m++]; m];",
				"Table[Print[\"a(\", n, \") = \", a[n]]; a[n], {n, 1, 23}] (* _Jean-François Alcover_, Nov 06 2017, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A213806, A284875, A291465."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "Adam Berliner, Nate Dean, Jonelle Hook, _Alison Marr_, _Aba Mbirika_, Cayla McBee, Jun 08 2012",
			"ext": [
				"More terms from _Alois P. Heinz_, Jun 16 2012",
				"a(24) and beyond from _Paul Tabatabai_, Apr 29 2019"
			],
			"references": 4,
			"revision": 79,
			"time": "2020-12-04T10:20:06-05:00",
			"created": "2012-06-17T08:39:24-04:00"
		}
	]
}