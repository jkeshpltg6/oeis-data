{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005585",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5585,
			"id": "M4387",
			"data": "1,7,27,77,182,378,714,1254,2079,3289,5005,7371,10556,14756,20196,27132,35853,46683,59983,76153,95634,118910,146510,179010,217035,261261,312417,371287,438712,515592,602888,701624,812889,937839,1077699,1233765,1407406",
			"name": "5-dimensional pyramidal numbers: n(n+1)(n+2)(n+3)(2n+3)/5!.",
			"comment": [
				"Convolution of triangular numbers (A000217) and squares (A000290) (n\u003e=1). - _Graeme McRae_, Jun 07 2006",
				"p^k divides a(p^k-3), a(p^k-2), a(p^k-1) and a(p^k) for prime p \u003e 5 and integer k \u003e 0. p^k divides a((p^k-3)/2)) for prime p \u003e 5 and integer k \u003e 0. - _Alexander Adamchuk_, May 08 2007",
				"If a 2-set Y and an (n-3)-set Z are disjoint subsets of an n-set X then a(n-5) is the number of 6-subsets of X intersecting both Y and Z. - _Milan Janjic_, Sep 08 2007",
				"5-dimensional square numbers, fourth partial sums of binomial transform of [1,2,0,0,0,...]. a(n) = Sum_{i=0..n} binomial(n+4, i+4)*b(i), where b(i)=[1,2,0,0,0,...]. - Borislav St. Borisov (b.st.borisov(AT)abv.bg), Mar 05 2009",
				"Antidiagonal sums of the convolution array A213550. - _Clark Kimberling_, Jun 17 2012",
				"Binomial transform of (1, 6, 14, 16, 9, 2, 0, 0, 0, ...). - _Gary W. Adamson_, Jul 28 2015",
				"2*a(n) is number of ways to place 4 queens on an (n+3) X (n+3) chessboard so that they diagonally attack each other exactly 6 times. The maximal possible attack number, p=binomial(k,2)=6 for k=4 queens, is achievable only when all queens are on the same diagonal. In graph-theory representation they thus form a corresponding complete graph. - _Antal Pinter_, Dec 27 2015",
				"While adjusting for offsets, add A000389 to find the next in series A000389, A005585, A051836, A034263, A027800, A051843, A051877, A051878, A051879, A051880, A056118, A271567. (See _Bruno Berselli_'s comments in A271567.) - _Bruce J. Nicholson_, Jun 21 2018",
				"Coefficients in the terminating series identity 1 - 7*n/(n + 6) + 27*n*(n - 1)/((n + 6)*(n + 7)) - 77*n*(n - 1)*(n - 2)/((n + 6)*(n + 7)*(n + 8)) + ... = 0 for n = 1,2,3,.... Cf. A002415 and A040977. - _Peter Bala_, Feb 18 2019"
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), p. 797.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Alexander Adamchuk and Vincenzo Librandi, \u003ca href=\"/A005585/b005585.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e (first 121 terms from Alexander Adamchuk)",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/2104.05593\"\u003eOn the Gap-sum and Gap-product Sequences of Integer Sequences\u003c/a\u003e, arXiv:2104.05593 [math.CO], 2021.",
				"R. K. Guy, \u003ca href=\"/A005581/a005581_1.pdf\"\u003eLetter to N. J. A. Sloane, Feb 1988\u003c/a\u003e",
				"Milan Janjic, \u003ca href=\"http://www.pmfbl.org/janjic/\"\u003eTwo Enumerative Functions\u003c/a\u003e",
				"C. H. Karlson and N. J. A. Sloane, \u003ca href=\"/A002790/a002790.pdf\"\u003eCorrespondence, 1974\u003c/a\u003e",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"R. P. Stanley and F. Zanello, \u003ca href=\"http://arxiv.org/abs/1312.4352\"\u003eThe Catalan case of Armstrong's conjecture on core partitions\u003c/a\u003e, arXiv preprint arXiv:1312.4352 [math.CO], 2013.",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Ps#pyramidal_numbers\"\u003eIndex to sequences related to pyramidal numbers\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (6,-15,20,-15,6,-1)."
			],
			"formula": [
				"G.f.: x*(1+x)/(1-x)^6.",
				"a(n) = 2*C(n+4, 5) - C(n+3, 4). - _Paul Barry_, Mar 04 2003",
				"a(n) = C(n+3, 5) + C(n+4, 5). - _Paul Barry_, Mar 17 2003",
				"a(n) = C(n+2, 6) - C(n, 6), n \u003e= 4. - _Zerinvary Lajos_, Jul 21 2006",
				"a(n) = Sum_{k=1..n} T(k)*T(k+1)/3, where T(n) = n(n+1)/2 is a triangular number. - _Alexander Adamchuk_, May 08 2007",
				"a(n-1) = (1/4)*Sum_{1 \u003c= x_1, x_2 \u003c= n} |x_1*x_2*det V(x_1,x_2)| = (1/4)*Sum_{1 \u003c= i,j \u003c= n} i*j*|i-j|, where V(x_1,x_2} is the Vandermonde matrix of order 2. First differences of A040977. - _Peter Bala_, Sep 21 2007",
				"a(n) = C(n+4,4) + 2*C(n+4,5). - Borislav St. Borisov (b.st.borisov(AT)abv.bg), Mar 05 2009",
				"a(n) = 6*a(n-1) - 15*a(n-2) + 20*a(n-3) - 15*a(n-4) + 6*a(n-5) - a(n-6), a(1)=1, a(2)=7, a(3)=27, a(4)=77, a(5)=182, a(6)=378. - _Harvey P. Dale_, Oct 04 2011",
				"a(n) = (1/6)*Sum_{i=1..n+1} (i*Sum_{k=1..i} (i-1)*k). - _Wesley Ivan Hurt_, Nov 19 2014",
				"E.g.f.: x*(2*x^4 + 35*x^3 + 180*x^2 + 300*x + 120)*exp(x)/120. - _Robert Israel_, Nov 19 2014",
				"a(n) = A000389(n+3) + A000389(n+4). - _Bruce J. Nicholson_, Jun 21 2018",
				"a(n) = -a(-3-n) for all n in Z. - _Michael Somos_, Jun 24 2018",
				"From _Amiram Eldar_, Jun 28 2020: (Start)",
				"Sum_{n\u003e=1} 1/a(n) = 40*(16*log(2) - 11)/3.",
				"Sum_{n\u003e=1} (-1)^(n+1)/a(n) = 20*(8*Pi - 25)/3. (End)"
			],
			"example": [
				"G.f. = x + 7*x^2 + 27*x^3 + 77*x^4 + 182*x^5 + 378*x^6 + 714*x^7 + 1254*x^8 + ... - _Michael Somos_, Jun 24 2018"
			],
			"maple": [
				"[seq(binomial(n+2,6)-binomial(n,6), n=4..45)]; # _Zerinvary Lajos_, Jul 21 2006",
				"A005585:=(1+z)/(z-1)**6; # _Simon Plouffe_ in his 1992 dissertation"
			],
			"mathematica": [
				"s1=s2=s3=0; lst={}; Do[s1+=n^2; s2+=s1; s3+=s2; AppendTo[lst,s3],{n,0,6!}]; lst (* _Vladimir Joseph Stephan Orlovsky_, Jan 15 2009 *)",
				"With[{c=5!},Table[n(n+1)(n+2)(n+3)(2n+3)/c,{n,40}]] (* or *) LinearRecurrence[ {6,-15,20,-15,6,-1},{1,7,27,77,182,378},40] (* _Harvey P. Dale_, Oct 04 2011 *)",
				"CoefficientList[Series[(1 + x) / (1 - x)^6, {x, 0, 50}], x] (* _Vincenzo Librandi_, Jun 09 2013 *)"
			],
			"program": [
				"(MAGMA) I:=[1, 7, 27, 77, 182, 378]; [n le 6 select I[n] else 6*Self(n-1)-15*Self(n-2)+20*Self(n-3)-15*Self(n-4)+6*Self(n-5)-Self(n-6): n in [1..40]]; // _Vincenzo Librandi_, Jun 09 2013",
				"(PARI) a(n)=binomial(n+3,4)*(2*n+3)/5 \\\\ _Charles R Greathouse IV_, Jul 28 2015"
			],
			"xref": [
				"a(n) = ((-1)^(n+1))*A053120(2*n+3, 5)/16, (1/16 of sixth unsigned column of Chebyshev T-triangle, zeros omitted).",
				"Partial sums of A002415.",
				"Cf. A006542, A040977, A047819, A111125 (third column).",
				"Cf. a(n) = ((-1)^(n+1))*A084960(n+1, 2)/16 (compare with the first line). - _Wolfdieter Lang_, Aug 04 2014",
				"Cf. A000389, A051836, A034263, A027800, A051843, A051877, A051878, A051879."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"references": 38,
			"revision": 119,
			"time": "2021-07-09T01:07:34-04:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}