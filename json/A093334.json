{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A093334",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 93334,
			"data": "12,120,630,1680,2310,360360,30030,1166880,17459442,193993800,223092870,486748080,579462,180970440,231415950150,493687360320,3085546002,15714504285480,62359143990,5382578744400,15465127383342,162015620206440,173062139765970,6139943741262240,77311562676150",
			"name": "Denominators of the coefficients of Euler-Ramanujan's harmonic number expansion into negative powers of a triangular number.",
			"comment": [
				"Previous name was: Coefficients in Ramanujan's Euler-MacLaurin asymptotic expansion.",
				"Explicitly, H_k = Sum_{i=1..k} 1/i = log(2*m)/2 + gamma + Sum_{n\u003e=1} R_n/m^n, where m = k(k+1)/2 is the k-th triangular number. This sequence lists the denominators of R_n (numerators are listed in A238813). A few starting numerical terms were given by Euler and Ramanujan; the form of the general term and the behavior of the series were determined by Villarino. - _Stanislav Sykora_, Mar 05 2014"
			],
			"link": [
				"Stanislav Sykora, \u003ca href=\"/A093334/b093334.txt\"\u003eTable of n, a(n) for n = 1..296\u003c/a\u003e",
				"Chao-Ping Chen, \u003ca href=\"http://dx.doi.org/10.1007/s11139-015-9670-3\"\u003eOn the coefficients of asymptotic expansion for the harmonic number by Ramanujan\u003c/a\u003e, The Ramanujan Journal, (2016) 40: 279-290.",
				"Xavier Gourdon and Pascal Sebah, \u003ca href=\"http://numbers.computation.free.fr/Constants/Gamma/gammaFormulas.html\"\u003eCollection of formulas for Euler's constant gamma\u003c/a\u003e (see paragraph 2.1.1).",
				"M. B. Villarino, \u003ca href=\"http://arxiv.org/abs/0707.3950\"\u003eRamanujan's Harmonic Number Expansion into Negative Powers of a Triangular Number\u003c/a\u003e, arXiv:0707.3950 [math.CA], 2007."
			],
			"formula": [
				"R_n = ((-1)^(n-1)/(2*n*8^n))*(1 + Sum_{i=1..n} (-4)^i*binomial(n,i)*B_2i(1/2));",
				"a(n) = denominator(R_n), and B_2i(x) is the (2i)-th Bernoulli polynomial. - _Stanislav Sykora_, Mar 05 2014"
			],
			"example": [
				"R_9 = 140051/17459442 = A238813(9)/a(9)."
			],
			"mathematica": [
				"Table[Denominator[((-1)^(n-1)/(2*n*8^n))*(1 + Sum[(-4)^j*Binomial[n,j]* BernoulliB[2*j,1/2], {j,1,n}])], {n,1,30}] (* _G. C. Greubel_, Aug 30 2018 *)"
			],
			"program": [
				"(PARI) Rn(nmax)= {local(n,k,v,R);v=vector(nmax);x=1/2;",
				"for(n=1,nmax,R=1;for(k=1,n,R+=(-4)^k*binomial(n,k)*eval(bernpol(2*k)));",
				"R*=(-1)^(n-1)/(2*n*8^n);v[n]=R);(apply(x-\u003edenominator(x), v));}",
				"// _Stanislav Sykora_, Mar 05 2014; improved by _Michel Marcus_, Aug 30 2018"
			],
			"xref": [
				"Cf. A000217 (triangular numbers), A001620 (gamma), A238813 (numerators)."
			],
			"keyword": "nonn,frac",
			"offset": "1,1",
			"author": "Kent Wigstrom (jijiw(AT)speedsurf.pacific.net.ph), Apr 25 2004",
			"ext": [
				"Title changed, terms a(5) onward added by _Stanislav Sykora_, Mar 05 2014"
			],
			"references": 4,
			"revision": 52,
			"time": "2020-02-09T12:39:57-05:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}