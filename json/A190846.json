{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A190846",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 190846,
			"data": "1,2,2,2,5,6,2,1,3,10,6,6,13,14,2,2,6,6,10,10,21,22,6,1,5,3,2,14,29,30,2,2,33,34,6,6,37,38,10,10,41,42,22,7,15,46,6,1,1,10,26,26,6,6,14,14,57,58,30,30,61,21,1,2,65,66,34,34,69,70,6,6,73,15,8,38,77,78,10,0,3,82,42",
			"name": "(Squarefree part of (ABC))/C for A=1, C=A+B, as a function of B, rounded to the nearest integer.",
			"comment": [
				"Given A, B natural numbers, and C=A+B, the ABC conjecture deals with the ratio of the squarefree part of the product A*B*C, divided through C. Here, B plays the role of the OEIS index n."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A190846/b190846.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Abderrahmane Nitaj, \u003ca href=\"https://nitaj.users.lmno.cnrs.fr/abc.html\"\u003eThe ABC conjecture homepage\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/abcConjecture.html\"\u003eabc Conjecture\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/ABC_conjecture\"\u003eABC conjecture\u003c/a\u003e"
			],
			"example": [
				"For B=14, we have C=15 so SQP(ABC)=SQP(210)=2*3*5*7=210, so SQP(ABC)/C=210/15=14.",
				"For B=19, we have C=20, so SQP(ABC)=SQP(380)=2*5*19=190, so SQP(ABC)/C=190/20=9.5, which rounds to 10."
			],
			"maple": [
				"A190846 := proc(n) c := 1+n ; round(A007947(n*c)/c) ; end proc:",
				"seq(A190846(n),n=1..80) ; # _R. J. Mathar_, Jun 10 2011"
			],
			"mathematica": [
				"Array[Round[SelectFirst[Reverse@ Divisors[#1 #2], SquareFreeQ]/#2] \u0026 @@ {#, # + 1} \u0026, 83] (* _Michael De Vlieger_, Feb 19 2019 *)"
			],
			"program": [
				"(MAGMA) SQP:=func\u003c n | \u0026*[ f[j, 1]: j in [1..#f] ] where f is Factorization(n) \u003e; A190846:=func\u003c n | Round(SQP(a*n*c)/c) where c is a+n where a is 1 \u003e; [ A190846(n): n in [1..85] ]; // _Klaus Brockhaus_, May 27 2011",
				"(PARI) rad(n)=my(f=factor(n)[,1]); prod(i=1,#f,f[i])",
				"a(n)=rad(n^2+n)\\/(n+1) \\\\ _Charles R Greathouse IV_, Mar 11 2014",
				"(Python)",
				"from operator import mul",
				"from sympy import primefactors",
				"def rad(n): return 1 if n\u003c2 else reduce(mul, primefactors(n))",
				"def a(n): return int(round(rad(n**2 + n)/(n + 1))) # _Indranil Ghosh_, May 24 2017"
			],
			"xref": [
				"Cf. A191093, A191100, A120498."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Darrell Minor_, May 25 2011",
			"references": 3,
			"revision": 31,
			"time": "2019-02-19T23:35:06-05:00",
			"created": "2011-06-10T17:38:41-04:00"
		}
	]
}