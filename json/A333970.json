{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A333970",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 333970,
			"data": "2,2,3,2,4,2,5,2,3,6,2,3,7,2,3,4,8,2,3,9,2,5,10,2,11,2,3,4,6,12,2,4,13,2,4,7,14,2,3,4,5,15,2,3,4,8,16,2,3,17,2,3,6,9,18,2,3,19,2,3,4,5,10,20,2,3,5,7,21,2,3,5,11,22,2,3,5,23,2,3,4,5,6,8,12,24",
			"name": "Irregular triangle read by rows where the n-th row lists the bases 2\u003c=b\u003c=n+1 where n in base b contains the digit b-1.",
			"comment": [
				"If a number n has base 'b' representation = (... (b-1) A(j-1) ...A(3) A(2) A(1) A(0)) contains digit b-1, where b = q*(k+1)/k, k\u003e=1 , and Sum_{i\u003e=0} ((A(i)(mod b-q))*((b-q)^i)) \u003e 0 then there exists  n' \u003c n such that that n' in base b-q = b' contains digit b'-1 at the same place as n in base b and 0 \u003c= (A(i)-A'(i))/b' \u003c= (k+1)-((A'(i)+1)/b') (A'(i) is digit of n' in base b')for all i\u003e=0.*",
				"This condition is necessary and sufficient.",
				"Proof that Condition is Necessary:",
				"Since b-1 = b-q+q-1 and b' = q/k (as b = q*(k+1)/k). Therefore (b-1) (mod b') = (b'+q-1) (mod b') = (q-1) (mod b') = b'-1 :-(1).",
				"n in base 'b' representation = (... (b-1) A(j-1) ...A(3) A(2) A(1) A(0)).Then n = Sum_{i\u003e=0} (A(i)*(b^i)) = Sum_{i\u003e=0} (A(i)*((b-q+q)^i)).",
				"n = Sum_{i\u003e=0} (A(i)*(b'^i)) +",
				"    Sum_{i\u003e=1} (A(i)*(b^i - b'^i))",
				"   = Sum_{i\u003e=0} (A'(i)*(b'^i)) + Sum_{i\u003e=0} ((A(i)-A'(i))* (b'^i)) + Sum_{i\u003e=1} (A(i)*(b^i - b'^i)),",
				"         where A'(i) = A(i) (mod b').",
				"Now n-Sum_{i\u003e=0} ((A(i)-A'(i))*(b'^i))",
				"       - Sum_{i\u003e=1} (A(i)*(b^i - b'^i))",
				"       = Sum_{i\u003e=0} (A'(i)*(b'^i)).",
				"Since A'(j) = A(j) (mod b') =  (b-1) (mod b') = b'-1(due to equation (1) above and A(j) = b-1.",
				"Hence there exists n'  = Sum_{i\u003e=0} (A'(i)*(b'^i)) \u003e 0 containing digit b'-1 in base b'.",
				"Table of n/b with cell containing T(n, b) = (n', b') for q = b/2. n' = Sum_{i\u003e=0} (A'(i)*(b'^i))",
				"n/b|  4  |  6  |  8  |  10 | 12",
				"3  |(1,2)|     |     |     |",
				"4  |     |     |     |     |",
				"5  |     |(2,3)|     |     |",
				"6  |     |     |     |     |",
				"7  |(3,2)|     |(3,4)|     |",
				"8  |     |     |     |     |",
				"9  |     |     |     |(4,5)|",
				"10 |     |     |     |     |",
				"11 |(1,2)|(5,3)|     |     |(5,6)",
				"Example: For table n/b in comments containing (n',b') in its cells.",
				"For n = 7:",
				"In base b = 4, n = 13 :- q = b' = 4/2 = 2, and n' = (3 mod (2))*(2)^0 + (1 mod(2))*(2)^1 = 1+2 = 3.",
				"In base b = 8, n = 7 :- q = b' = 8/2 = 4, and n' = (7 mod (4))*(4)^0 = 3.",
				"There are no other bases b \u003e= 4 except 4, 8 for n = 7.",
				"(n, b) maps to (0, 1) if b is prime. Following this and comment in A337536 we can say that all of the terms of A337536 will map to (0, 1) only, except A337536(2).",
				"For above (n, b) -\u003e (n', b') one possible (n, b) pair for (n', b') is { Sum_{i\u003e=0} ((A'(i)+b') *((2*b')^i)), 2*b'}."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A333970/b333970.txt\"\u003eTable of n, a(n) for n = 1..10027\u003c/a\u003e (rows for n = 1..1014, flattened)"
			],
			"example": [
				"Triangle begins",
				"  Row    Bases",
				"  n=1:   2",
				"  n=2:   2  3",
				"  n=3:   2  4",
				"  n=4:   2  5",
				"  n=5:   2  3  6",
				"  n=6:   2  3  7",
				"  n=7:   2  3  4  8",
				"  n=8:   2  3  9",
				"  n=9:   2  5  10",
				"  n=10:  2  11"
			],
			"program": [
				"(PARI) row(n) = {my(list = List()); for (b=2, n+1, if (vecmax(digits(n, b)) == b-1, listput(list, b));); Vec(list);} \\\\ _Michel Marcus_, Sep 11 2020"
			],
			"xref": [
				"Cf. A337535 (second column), A338295 (penultimate column), A337496 (row widths), A337536 (width 2), A337143 (width 3).",
				"Rows containing bases 3..11 respectively: A074940, A337250, A337572, A333656, A337141, A337239, A338090, A011539, A095778."
			],
			"keyword": "nonn,base,tabf",
			"offset": "1,1",
			"author": "_Devansh Singh_, Sep 03 2020",
			"ext": [
				"More terms from _Michel Marcus_, Sep 11 2020"
			],
			"references": 1,
			"revision": 96,
			"time": "2022-01-02T07:25:37-05:00",
			"created": "2020-09-20T01:49:01-04:00"
		}
	]
}