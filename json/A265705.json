{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A265705",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 265705,
			"data": "0,1,1,3,2,3,3,3,3,3,7,6,5,4,7,7,7,5,5,7,7,7,6,7,6,7,6,7,7,7,7,7,7,7,7,7,15,14,13,12,11,10,9,8,15,15,15,13,13,11,11,9,9,15,15,15,14,15,14,11,10,11,10,15,14,15,15,15,15,15,11,11,11,11,15",
			"name": "Triangle read by rows: T(n,k) = k IMPL n, 0 \u003c= k \u003c= n, bitwise logical IMPL.",
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A265705/b265705.txt\"\u003eRows n = 0..255 of triangle, flattened\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Implies.html\"\u003eImplies\u003c/a\u003e"
			],
			"formula": [
				"T(n,0) = T(n,n) = A003817(n).",
				"T(2*n,n) = A265716(n).",
				"Let m = A089633(n): T(m,k) = T(m,m-k), k = 0..m.",
				"Let m = A158582(n): T(m,k) != T(m,m-k) for at least one k \u003c= n.",
				"Let m = A247648(n): T(2*m,m) = 2*m.",
				"For n \u003e 0: A029578(n+2) = number of odd terms in row n; no even terms in odd-indexed rows.",
				"A265885(n) = T(prime(n),n).",
				"A053644(n) = smallest k such that row k contains n."
			],
			"example": [
				".          10 | 1010                            12 | 1100",
				".           4 |  100                             6 |  110",
				".   ----------+-----                     ----------+-----",
				".   4 IMPL 10 | 1011 -\u003e T(10,4)=11       6 IMPL 12 | 1101 -\u003e T(12,6)=13",
				".",
				"First 16 rows of the triangle, where non-symmetrical rows are marked, see comment concerning A158582 and A089633:",
				".   0:                                 0",
				".   1:                               1   1",
				".   2:                             3   2   3",
				".   3:                           3   3   3   3",
				".   4:                         7   6   5   4   7    X",
				".   5:                       7   7   5   5   7   7",
				".   6:                     7   6   7   6   7   6   7",
				".   7:                   7   7   7   7   7   7   7   7",
				".   8:                15  14  13  12  11  10   9   8  15    X",
				".   9:              15  15  13  13  11  11   9   9  15  15    X",
				".  10:            15  14  15  14  11  10  11  10  15  14  15    X",
				".  11:          15  15  15  15  11  11  11  11  15  15  15  15",
				".  12:        15  14  13  12  15  14  13  12  15  14  13  12  15    X",
				".  13:      15  15  13  13  15  15  13  13  15  15  13  13  15  15",
				".  14:    15  14  15  14  15  14  15  14  15  14  15  14  15  14  15",
				".  15:  15  15  15  15  15  15  15  15  15  15  15  15  15  15  15  15 ."
			],
			"maple": [
				"A265705 := (n, k) -\u003e Bits:-Implies(k, n):",
				"seq(seq(A265705(n, k), k=0..n), n=0..11); # _Peter Luschny_, Sep 23 2019"
			],
			"mathematica": [
				"T[n_, k_] := If[n == 0, 0, BitOr[2^Length[IntegerDigits[n, 2]]-1-k, n]];",
				"Table[T[n, k], {n, 0, 11}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Sep 25 2021, after _David A. Corneth_'s PARI code *)"
			],
			"program": [
				"(Haskell)",
				"a265705_tabl = map a265705_row [0..]",
				"a265705_row n = map (a265705 n) [0..n]",
				"a265705 n k = k `bimpl` n where",
				"   bimpl 0 0 = 0",
				"   bimpl p q = 2 * bimpl p' q' + if u \u003c= v then 1 else 0",
				"               where (p', u) = divMod p 2; (q', v) = divMod q 2",
				"(PARI) T(n, k) = if(n==0,return(0)); bitor((2\u003c\u003clogint(n,2))-1-k,n) \\\\ _David A. Corneth_, Sep 24 2021",
				"(Julia)",
				"using IntegerSequences",
				"for n in 0:15 println(n == 0 ? [0] : [Bits(\"IMP\", k, n) for k in 0:n]) end  # _Peter Luschny_, Sep 25 2021"
			],
			"xref": [
				"Cf. A003817, A007088, A029578, A089633, A158582, A247648, A265716 (central terms), A265736 (row sums).",
				"Cf. A053644, A265885, A327490.",
				"Other triangles: A080099 (AND), A080098 (OR), A051933 (XOR), A102037 (CNIMPL)."
			],
			"keyword": "nonn,easy,tabl,look",
			"offset": "0,4",
			"author": "_Reinhard Zumkeller_, Dec 15 2015",
			"references": 14,
			"revision": 49,
			"time": "2021-12-17T01:40:48-05:00",
			"created": "2015-12-15T14:32:49-05:00"
		}
	]
}