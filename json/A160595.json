{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A160595",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 160595,
			"data": "1,1,1,2,1,2,1,4,3,4,1,4,1,6,4,8,1,6,1,8,3,10,1,8,5,12,9,4,1,8,1,16,5,16,12,12,1,18,12,16,1,12,1,20,6,22,1,16,7,20,16,8,1,18,20,24,9,28,1,16,1,30,18,32,3,4,1,32,11,8,1,24,1,36,20,12,15,24,1,32,27,40,1,24,16,42,28",
			"name": "Numerator of resilience R(n) = phi(n)/(n-1), with a(1) = 1 by convention.",
			"comment": [
				"The resilience of a denominator, R(d), is the ratio of proper fractions n/d, 0 \u003c n \u003c d, that are resilient, i.e., such that gcd(n, d) = 1. Obviously this is the case for phi(d) proper fractions among the d - 1 possible ones.",
				"a(n) = 1 if n is prime. It is unknown whether there exist composite n with a(n) = 1 (see Wikipedia link). - _Robert Israel_, Dec 26 2016",
				"Conjecture: a(n) \u003e 2 for every composite n \u003e 6. Slightly stronger than the Lehmer's totient conjecture (1932). - _Thomas Ordowski_, Mar 13 2019"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A160595/b160595.txt\"\u003eTable of n, a(n) for n = 1..65537\u003c/a\u003e (terms 2..10000 from Robert Israel)",
				"Project Euler, \u003ca href=\"http://projecteuler.net/index.php?section=problems\u0026amp;id=245\"\u003eProblem 245: resilient fractions\u003c/a\u003e, May 2009",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Lehmer\u0026#39;s_totient_problem\"\u003eLehmer's totient problem\u003c/a\u003e."
			],
			"formula": [
				"a(n) = phi(n)/gcd(phi(n),n-1) = A000010(n) / A049559(n) = A247074(n) * A318829(n). - _Antti Karttunen_, Sep 09 2018"
			],
			"example": [
				"a(9) = 3 since for the denominator d = 9, among the 8 proper fractions n/9 (n = 1, ..., 8), six cannot be canceled down by a common factor (namely 1/9, 2/9, 4/9, 5/9, 7/9, 8/9), thus R(9) = 6/8 = 3/4."
			],
			"maple": [
				"seq(numer(numtheory:-phi(n)/(n-1)),n=2..100); # _Robert Israel_, Dec 26 2016"
			],
			"mathematica": [
				"Numerator[Table[EulerPhi[n]/(n - 1), {n, 2, 87}]] (* _Alonso del Arte_, Sep 19 2011 *)"
			],
			"program": [
				"(PARI) A160595(n) = if(1==n,n,numerator(eulerphi(n)/(n-1)));",
				"(MAGMA) [Numerator(EulerPhi(n)/(n-1)): n in [2..90]]; // _Vincenzo Librandi_, Jan 02 2017"
			],
			"xref": [
				"Cf. A000010, A049559, A247074, A318829."
			],
			"keyword": "nonn,look",
			"offset": "1,4",
			"author": "_M. F. Hasler_, May 23 2009",
			"ext": [
				"Term a(1) = 1 prepended by _Antti Karttunen_, Sep 09 2018"
			],
			"references": 21,
			"revision": 47,
			"time": "2019-04-10T10:20:41-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}