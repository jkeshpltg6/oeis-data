{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A192914",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 192914,
			"data": "1,0,5,9,28,69,185,480,1261,3297,8636,22605,59185,154944,405653,1062009,2780380,7279125,19057001,49891872,130618621,341963985,895273340,2343856029,6136294753,16065028224,42058789925,110111341545,288275234716,754714362597",
			"name": "Constant term in the reduction by (x^2 -\u003e x + 1) of the polynomial C(n)*x^n, where C=A000285.",
			"comment": [
				"See A192872."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A192914/b192914.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,2,-1)."
			],
			"formula": [
				"a(n) = 2*a(n-1) + 2*a(n-2) - a(n-3).",
				"G.f.: (1 + 3*x^2 - 2*x)/((1 + x)*(x^2 - 3*x + 1)). - _R. J. Mathar_, May 08 2014",
				"a(n) = (2^(-1-n)*(3*(-1)^n*2^(2+n) + (3 + sqrt(5))^n*(-1 + 3*sqrt(5)) - (3-sqrt(5))^n*(1 + 3*sqrt(5))))/5. - _Colin Barker_, Sep 29 2016",
				"a(n) = F(n+1)^2 + F(n)*F(n-3). - _Bruno Berselli_, Feb 15 2017"
			],
			"mathematica": [
				"q = x^2; s = x + 1; z = 28;",
				"p[0, x_]:= 1; p[1, x_]:= 4 x;",
				"p[n_, x_] := p[n-1, x]*x + p[n-2, x]*x^2;",
				"Table[Expand[p[n, x]], {n, 0, 7}]",
				"reduce[{p1_, q_, s_, x_}]:= FixedPoint[(s PolynomialQuotient @@ #1 + PolynomialRemainder @@ #1 \u0026)[{#1, q, x}] \u0026, p1]",
				"t = Table[reduce[{p[n, x], q, s, x}], {n, 0, z}];",
				"u1 = Table[Coefficient[Part[t, n], x, 0], {n, 1, z}] (* A192914 *)",
				"u2 = Table[Coefficient[Part[t, n], x, 1], {n, 1, z}] (* see A192878 *)",
				"LinearRecurrence[{2,2,-1}, {1,0,5}, 30] (* or *) With[{F:= Fibonacci}, Table[F[n+1]^2 +F[n]*F[n-3], {n, 0, 30}]] (* _G. C. Greubel_, Jan 12 2019 *)"
			],
			"program": [
				"(PARI) a(n) = round((2^(-1-n)*(3*(-1)^n*2^(2+n)+(3+sqrt(5))^n*(-1+3*sqrt(5))-(3-sqrt(5))^n*(1+3*sqrt(5))))/5) \\\\ _Colin Barker_, Sep 29 2016",
				"(PARI) Vec((1+3*x^2-2*x)/((1+x)*(x^2-3*x+1)) + O(x^30)) \\\\ _Colin Barker_, Sep 29 2016",
				"(PARI) {f=fibonacci}; vector(30, n, n--; f(n+1)^2 +f(n)*f(n-3)) \\\\ _G. C. Greubel_, Jan 12 2019",
				"(MAGMA) F:=Fibonacci; [F(n+1)^2+F(n)*F(n-3): n in [0..30]]; // _Bruno Berselli_, Feb 15 2017",
				"(Sage) f=fibonacci; [f(n+1)^2 +f(n)*f(n-3) for n in (0..30)] # _G. C. Greubel_, Jan 12 2019",
				"(GAP) F:=Fibonacci; List([0..30], n -\u003e F(n+1)^2 +F(n)*F(n-3)); # _G. C. Greubel_, Jan 12 2019"
			],
			"xref": [
				"Cf. A192232, A192744, A192872."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Clark Kimberling_, Jul 12 2011",
			"references": 5,
			"revision": 28,
			"time": "2019-01-12T20:47:45-05:00",
			"created": "2011-07-13T12:20:08-04:00"
		}
	]
}