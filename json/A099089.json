{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A099089",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 99089,
			"data": "1,0,2,0,1,4,0,0,4,8,0,0,1,12,16,0,0,0,6,32,32,0,0,0,1,24,80,64,0,0,0,0,8,80,192,128,0,0,0,0,1,40,240,448,256,0,0,0,0,0,10,160,672,1024,512,0,0,0,0,0,1,60,560,1792,2304,1024,0,0,0,0,0,0,12,280,1792,4608,5120,2048",
			"name": "Riordan array (1, 2+x).",
			"comment": [
				"Row sums are A000129. Diagonal sums are A008346. The Riordan array (1, s+tx) defines T(n,k) = binomial(k,n-k)*s^k*(t/s)^(n-k). The row sums satisfy a(n) = s*a(n-1) + t*a(n-2) and the diagonal sums satisfy a(n) = s*a(n-2) + t*a(n-3).",
				"Triangle T(n,k), 0 \u003c= k \u003c= n, read by rows given by [0, 1/2, -1/2, 0, 0, 0, 0, ...] DELTA [2, 0, 0, 0, 0, 0, ...] where DELTA is the operator defined in A084938. - _Philippe Deléham_, Nov 10 2008",
				"As an upper right triangle (in the example), table rows give number of points, edges, faces, cubes, 4D hypercubes etc. in hypercubes of increasing dimension by column. - _Henry Bottomley_, Apr 14 2000. More precisely, the (i,j)-th entry is the number of j-dimensional subspaces of an i-dimensional hypercube (see the Coxeter reference). - _Christof Weber_, May 08 2009"
			],
			"reference": [
				"H. S. M. Coxeter, Regular Polytopes, Dover Publications, New York (1973), p. 122."
			],
			"link": [
				"Eric W. Weisstein's Mathworld, \u003ca href=\"http://mathworld.wolfram.com/Hypercube.html\"\u003eHypercube\u003c/a\u003e."
			],
			"formula": [
				"Number triangle T(n,k) = binomial(k, n-k)*2^k*(1/2)^(n-k); columns have g.f. (2*x+x^2)^k.",
				"G.f.: 1/(1-2y*x-y*x^2). - _Philippe Deléham_, Nov 20 2011",
				"Sum_ {k=0..n} T(n,k)*x^k = A000007(n), A000129(n+1), A090017(n+1), A090018(n), A190510(n+1), A190955(n+1) for x = 0,1,2,3,4,5 respectively. - _Philippe Deléham_, Nov 20 2011",
				"T(n,k) = 2*T(n-1,k-1) + T(n-2,k-1), T(0,0) = 1, T(1,0) = T(2,0) = 0, T(1,1) = 2, T(2,1) = 1, T(2,2) = 4, T(n,k) = 0 if k \u003e n or if k \u003c 0. - _Philippe Deléham_, Oct 30 2013"
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  0,  2;",
				"  0,  1,  4;",
				"  0,  0,  4,  8;",
				"  0,  0,  1, 12, 16;",
				"  0,  0,  0,  6, 32, 32;",
				"  0,  0,  0,  1, 24, 80, 64;",
				"The entries can also be interpreted as the antidiagonal reading of the following array:",
				"  1,    2,    4,    8,   16,   32,   64,  128,  256,  512, 1024,... A000079",
				"  0,    1,    4,   12,   32,   80,  192,  448, 1024, 2304, 5120,... A001787",
				"  0,    0,    1,    6,   24,   80,  240,  672, 1792, 4608,11520,... A001788",
				"  0,    0,    0,    1,    8,   40,  160,  560, 1792, 5376,15360,... A001789",
				"  0,    0,    0,    0,    1,   10,   60,  280, 1120, 4032,13440,...",
				"  0,    0,    0,    0,    0,    1,   12,   84,  448, 2016, 8064,...",
				"  0,    0,    0,    0,    0,    0,    1,   14,  112,  672, 3360,...",
				"  0,    0,    0,    0,    0,    0,    0,    1,   16,  144,  960,...",
				"  0,    0,    0,    0,    0,    0,    0,    0,    1,   18,  180,...",
				"  0,    0,    0,    0,    0,    0,    0,    0,    0,    1,   20,...",
				"  0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    1,..."
			],
			"xref": [
				"Cf. A053118, A008312, A062715, A038207."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,3",
			"author": "_Paul Barry_, Sep 25 2004",
			"references": 9,
			"revision": 30,
			"time": "2020-04-10T02:07:51-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}