{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005187",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5187,
			"id": "M2330",
			"data": "0,1,3,4,7,8,10,11,15,16,18,19,22,23,25,26,31,32,34,35,38,39,41,42,46,47,49,50,53,54,56,57,63,64,66,67,70,71,73,74,78,79,81,82,85,86,88,89,94,95,97,98,101,102,104,105,109,110,112,113,116,117,119,120,127,128",
			"name": "a(n) = a(floor(n/2)) + n; also denominators in expansion of 1/sqrt(1-x) are 2^a(n); also 2n - number of 1's in binary expansion of 2n.",
			"comment": [
				"Also exponent of the largest power of 2 dividing (2n)! and (2n)!!.",
				"Write n in binary: 1ab..yz, then a(n) = 1ab..yz + ... + 1ab + 1a + 1. - _Ralf Stephan_, Aug 27 2003",
				"Also numbers having a partition into distinct Mersenne numbers \u003e 0; A079559(a(n))=1; complement of A055938. - _Reinhard Zumkeller_, Mar 18 2009",
				"Wikipedia's article on the \"Whitney Immersion theorem\" mentions that the quantity 2n - a(n) is involved in the differential topology of manifolds, notably the Immersion Conjecture proved by Ralph Cohen in 1985.\" - _Jonathan Vos Post_, Jan 25 2010",
				"For n \u003e 0, denominators for consecutive pairs of integral numerator polynomials L(n+1,x) for the Legendre polynomials with o.g.f. 1 / sqrt(1-tx+x^2). - _Tom Copeland_, Feb 04 2016",
				"a(n) is the total number of pointers in the first n elements of a perfect skip list. - _Alois P. Heinz_, Dec 14 2017",
				"a(n) is the position of the n'th a (indexing from 0) in the fixed point of the morphism a -\u003e aab, b -\u003e b. - _Jeffrey Shallit_, Dec 24 2020"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"N. J. A. Sloane and T. D. Noe, \u003ca href=\"/A005187/b005187.txt\"\u003eTable of n, a(n) for n = 0..20000\u003c/a\u003e (first 1000 terms from T. D. Noe)",
				"J.-P. Allouche, J. Betrema, and J. Shallit, \u003ca href=\"https://doi.org/10.1051/ita/1989230302351\"\u003eSur des points fixes de morphismes d'un monoïde libre\u003c/a\u003e, RAIRO-Theor. Inf. Appl. 23 (1989), 235-249.",
				"Laurent Alonso, Edward M. Reingold, and René Schott, \u003ca href=\"http://dx.doi.org/10.1016/0020-0190(93)90135-V\"\u003eDetermining the majority\u003c/a\u003e, Inform. Process. Lett. 47 (1993), no. 5, 253-255.",
				"Laurent Alonso, Edward M. Reingold, and René Schott, \u003ca href=\"http://dx.doi.org/10.1137/S0097539794275914\"\u003eThe average-case complexity of determining the majority\u003c/a\u003e, SIAM J. Comput. 26 (1997), no. 1, 1-14.",
				"Sung-Hyuk Cha, \u003ca href=\"http://www.wseas.us/e-library/conferences/2012/CambridgeUSA/MATHCC/MATHCC-60.pdf\"\u003eOn Integer Sequences Derived from Balanced k-ary Trees\u003c/a\u003e, Applied Mathematics in Electrical and Computer Engineering, 2012.",
				"Sung-Hyuk Cha, \u003ca href=\"http://naun.org/multimedia/UPress/ami/16-125.pdf\"\u003eOn Complete and Size Balanced k-ary Tree Integer Sequences\u003c/a\u003e, International Journal of Applied Mathematics and Informatics, Issue 2, Volume 6, 2012, pp. 67-75. - From _N. J. A. Sloane_, Dec 24 2012",
				"Ralph L. Cohen, \u003ca href=\"http://www.jstor.org/stable/1971304\"\u003eThe Immersion Conjecture for Differentiable Manifolds\u003c/a\u003e, The Annals of Mathematics, 1985: 237-328. [From _Jonathan Vos Post_, Jan 25 2010]",
				"Hsien-Kuei Hwang, S. Janson, and T.-H. Tsai, \u003ca href=\"http://140.109.74.92/hk/wp-content/files/2016/12/aat-hhrr-1.pdf\"\u003eExact and asymptotic solutions of the recurrence f(n) = f(floor(n/2)) + f(ceiling(n/2)) + g(n): theory and applications\u003c/a\u003e, Preprint, 2016.",
				"Hsien-Kuei Hwang, S. Janson, and T.-H. Tsai, \u003ca href=\"https://doi.org/10.1145/3127585\"\u003eExact and Asymptotic Solutions of a Divide-and-Conquer Recurrence Dividing at Half: Theory and Applications\u003c/a\u003e, ACM Transactions on Algorithms, 13:4 (2017), #47; DOI: 10.1145/3127585.",
				"Keith Johnson and Kira Scheibelhut, \u003ca href=\"http://www.jstor.org/stable/10.4169/amer.math.monthly.123.4.338\"\u003eRational Polynomials That Take Integer Values at the Fibonacci Numbers\u003c/a\u003e, American Mathematical Monthly 123.4 (2016): 338-346. See p. 340.",
				"Tanya Khovanova, \u003ca href=\"http://arxiv.org/abs/1410.2193\"\u003eThere are no coincidences\u003c/a\u003e, arXiv preprint 1410.2193 [math.CO], 2014.",
				"A. Kulshrestha, \u003ca href=\"http://arxiv.org/abs/1203.4547\"\u003eOn Hamming Distance between base-n representations of whole numbers\u003c/a\u003e, arXiv:1203.4547 [cs.DM], 2012.",
				"Michael E. Saks and Michael Werman, \u003ca href=\"http://dx.doi.org/10.1007/BF01275672\"\u003eOn computing majority by comparisons\u003c/a\u003e, Combinatorica 11 (1991), no. 4, 383-387.",
				"Ralf Stephan, \u003ca href=\"/somedcgf.html\"\u003eSome divide-and-conquer sequences ...\u003c/a\u003e",
				"Ralf Stephan, \u003ca href=\"/A079944/a079944.ps\"\u003eTable of generating functions\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Whitney_immersion_theorem\"\u003eWhitney Immersion Theorem\u003c/a\u003e.",
				"Allan Wilks, \u003ca href=\"/A005187/a005187.pdf\"\u003eEmail to N. J. A. Sloane\u003c/a\u003e, Jul 7 1988."
			],
			"formula": [
				"a(n) = A011371(2n+1) = A011371(n) + n, n \u003e= 0.",
				"A046161(n) = 2^a(n).",
				"For m\u003e0, let q = floor(log_2(m)); a(2m+1) = 2^q + 3m + Sum_{k\u003e=1} floor((m-2^q)/2^k); a(2m) = a(2m+1) - 1. - _Len Smiley_",
				"a(n) = Sum_{k \u003e= 0} floor(n/2^k) = n + A011371(n). - _Henry Bottomley_, Jul 03 2001",
				"G.f.: A(x) = Sum_{k\u003e=0} x^(2^k)/((1-x)*(1-x^(2^k))). - _Ralf Stephan_, Dec 24 2002",
				"a(n) = Sum_{k=1..n} A001511(k), sum of binary Hamming distances between consecutive integers up to n. - _Gary W. Adamson_, Jun 15 2003",
				"Conjecture: a(n) = 2n + O(log(n)). - _Benoit Cloitre_, Oct 07 2003 [true as a(n) = 2*n - hamming_weight(2*n). _Joerg Arndt_, Jun 10 2019]",
				"Sum_{n=2^k..2^(k+1)-1} a(n) = 3*4^k - (k+4)*2^(k-1) = A085354(k). - _Philippe Deléham_, Feb 19 2004",
				"From _Hieronymus Fischer_, Aug 14 2007: (Start)",
				"Recurrence: a(n) = n + a(floor(n/2)); a(2n) = 2n + a(n); a(n*2^m) = 2*n*(2^m-1) + a(n).",
				"  a(2^m) = 2^(m+1) - 1, m \u003e= 0.",
				"Asymptotic behavior: a(n) = 2n + O(log(n)), a(n+1) - a(n) = O(log(n)); this follows from the inequalities below.",
				"a(n) \u003c= 2n-1; equality holds for powers of 2.",
				"a(n) \u003e= 2n-1-floor(log_2(n)); equality holds for n = 2^m-1, m \u003e 0.",
				"lim inf (2n - a(n)) = 1, for n--\u003eoo.",
				"lim sup (2n - log_2(n) - a(n)) = 0, for n--\u003eoo.",
				"lim sup (a(n+1) - a(n) - log_2(n)) = 1, for n--\u003eoo. (End)",
				"a(n) = 2n - A000120(n). - _Paul Barry_, Oct 26 2007",
				"PURRS demo results: Bounds for a(n) = n + a(n/2) with initial conditions a(1) = 1: a(n) \u003e= -2 + 2*n - log(n)*log(2)^(-1), a(n) \u003c= 1 + 2*n for each n \u003e= 1. - _Alexander R. Povolotsky_, Apr 06 2008",
				"If n = 2^a_1 + 2^a_2 + ... + 2^a_k, then a(n) = n-k. This can be used to prove that 2^n maximally divides (2n!)/n!. - _Jon Perry_, Jul 16 2009",
				"a(n) = Sum_{k\u003e=0} A030308(n,k)*A000225(k+1). - _Philippe Deléham_, Oct 16 2011",
				"a(n) = log_2(denominator(binomial(-1/2,n))). - _Peter Luschny_, Nov 25 2011",
				"a(2n+1) = a(2n) + 1. - _M. F. Hasler_, Jan 24 2015",
				"a(n) = A004134(n) - n. - _Cyril Damamme_, Aug 04 2015",
				"G.f.: (1/(1 - x))*Sum_{k\u003e=0} (2^(k+1) - 1)*x^(2^k)/(1 + x^(2^k)). - _Ilya Gutkovskiy_, Jul 23 2017"
			],
			"example": [
				"G.f. = x + 3*x^2 + 4*x^3 + 7*x^4 + 8*x^5 + 10*x^6 + 11*x^7 + 15*x^8 + ..."
			],
			"maple": [
				"A005187 := n -\u003e 2*n - add(i, i=convert(n, base, 2)):",
				"seq(A005187(n), n=0..65); # _Peter Luschny_, Apr 08 2014"
			],
			"mathematica": [
				"a[0] = 0; a[n_] := a[n] = a[Floor[n/2]] + n; Table[ a[n], {n, 0, 50}] (* or *)",
				"Table[IntegerExponent[(2n)!, 2], {n, 0, 65}] (* _Robert G. Wilson v_, Apr 19 2006 *)",
				"Table[2n-DigitCount[2n,2,1],{n,0,70}] (* _Harvey P. Dale_, May 24 2014 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, valuation((2*n)!, 2))}; /* _Michael Somos_, Oct 24 2002 */",
				"(PARI) {a(n) = if( n\u003c0, 0, sum(k=1, n, (2*n)\\2^k))}; /* _Michael Somos_, Oct 24 2002 */",
				"(PARI) {a(n) = if( n\u003c0, 0, 2*n - subst( Pol( binary( n ) ), x, 1) ) }; /* _Michael Somos_, Aug 28 2007 */",
				"(PARI) a(n)=my(s=n);while(n\u003e\u003e=1,s+=n);s \\\\ _Charles R Greathouse IV_, Apr 07 2012",
				"(PARI) a(n)=2*n-hammingweight(n) \\\\ _Charles R Greathouse IV_, Jan 07 2013",
				"(Haskell)",
				"a005187 n = a005187_list !! n",
				"a005187_list = 0 : zipWith (+) [1..] (map (a005187 . (`div` 2)) [1..])",
				"-- _Reinhard Zumkeller_, Nov 07 2011, Oct 05 2011",
				"(Sage)",
				"@CachedFunction",
				"def A005187(n): return A005187(n//2) + n if n \u003e 0 else 0",
				"[A005187(n) for n in range(66)]  # _Peter Luschny_, Dec 13 2012",
				"(MAGMA) [n + Valuation(Factorial(n), 2): n in [0..70]]; // _Vincenzo Librandi_, Jun 11 2019",
				"(Python)",
				"def A005187(n): return 2*n-bin(n).count('1') # _Chai Wah Wu_, Jun 03 2021"
			],
			"xref": [
				"Cf. A001790, A011371, A067080, A098844, A132027, A004128, A054899, A046161.",
				"Cf. A001511 (first differences), A122247 (partial sums).",
				"Cf. A004134."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_, May 20 1991; _Allan Wilks_, Dec 11 1999",
			"references": 228,
			"revision": 196,
			"time": "2021-11-01T12:45:00-04:00",
			"created": "1991-05-20T03:00:00-04:00"
		}
	]
}