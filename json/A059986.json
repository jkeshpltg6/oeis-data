{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A059986",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 59986,
			"data": "0,12,54,144,300,540,882,1344,1944,2700,3630,4752,6084,7644,9450,11520,13872,16524,19494,22800,26460,30492,34914,39744,45000,50700,56862,63504,70644,78300,86490,95232,104544,114444,124950,136080,147852,160284,173394",
			"name": "Number of rods required to make a 3-D cube of side length n.",
			"comment": [
				"Equals number of rods making a cube of side length n+1 minus the number of line segments illustrating the isometric projection of a cube of side length n+1 (i.e., the hexagonal matchstick numbers). See the illustration in the links and formula below. - _Peter M. Chema_, Mar 14 2017"
			],
			"link": [
				"Peter M. Chema, \u003ca href=\"/A059986/a059986.pdf\"\u003eFirst difference are the hexagonal matchstick numbers or isometric projection of a cube\u003c/a\u003e.",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-6,4,-1)."
			],
			"formula": [
				"a(n) = 3*n*(n+1)^2. - Neven Juric (neven.juric(AT)apis-it.hr), Sep 28 2005",
				"From _Geoffrey Critzer_, May 17 2009: (Start)",
				"a(n) = a(n-1) + 9*n^2 + 3*n.",
				"O.g.f.: 6*x*(2 + x)/(1 - x)^4.",
				"E.g.f.: 3*x*exp(x)*(x^2 + 5*x + 4). (End)",
				"a(n) = A117227(n^3). - _Michel Marcus_, Jun 19 2013",
				"For n \u003e 0, a(n) = Sum_{k=1..n} 2*(n+1)(k+n+1), which is the sum of all perimeters of Pythagorean triangles with arms 2*k*(n+1) and (n+1)^2 - k^2 with hypotenuse k^2 + (n+1)^2. - _J. M. Bergot_, May 12 2014",
				"a(n) = a(n+1) - A045945(n+1). - _Peter M. Chema_, Mar 14 2017",
				"a(n) = (n-1)*t(n+1) + n*(t(n)+t(n+1)) + (n+1)*(t(n-1)+t(n)+t(n+1)), where t = A000217. - _J. M. Bergot_, May 30 2017",
				"From _Amiram Eldar_, Jan 14 2021: (Start)",
				"Sum_{n\u003e=1} 1/a(n) = 2/3 - Pi^2/18.",
				"Sum_{n\u003e=1} (-1)^(n+1)/a(n) = -2/3 + Pi^2/36 + 2*log(2)/3. (End)"
			],
			"example": [
				"A 1 X 1 X 1 cube requires 12 rods."
			],
			"maple": [
				"A059986:=n-\u003e3*n*(n+1)^2; seq(A059986(n), n=0..50); # _Wesley Ivan Hurt_, May 13 2014"
			],
			"mathematica": [
				"Table[M[GridGraph[n, n, n]], {n, 39}] (* _Geoffrey Critzer_, May 17 2009 *)",
				"Table[3 n (n + 1)^2, {n, 0, 50}] (* _Wesley Ivan Hurt_, May 13 2014 *)"
			],
			"program": [
				"(MAGMA) [3*n*(n+1)^2: n in [0..50]]; // _Wesley Ivan Hurt_, May 13 2014",
				"(PARI) a(n) = 3*n*(n+1)^2 \\\\ _Charles R Greathouse IV_, May 14 2014"
			],
			"xref": [
				"Cf. A045945, A117227."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "Laura Twomey (sxe15(AT)hotmail.com), Mar 07 2001",
			"ext": [
				"More terms from Neven Juric (neven.juric(AT)apis-it.hr), Sep 28 2005"
			],
			"references": 4,
			"revision": 46,
			"time": "2021-12-19T07:39:06-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}