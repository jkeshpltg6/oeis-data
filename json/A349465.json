{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A349465",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 349465,
			"data": "1,0,2,0,-1,1,1,2,2,1,2,2,3,2,3,4,5,3,4,3,5,6,5,4,4,6,7,6,7,6,8,8,8,9,10,10,10,11,11,13,13,14,14,14,15,15,16,16,16,19,19,19,20,20,20,21,21,22,22,23,24,24,25,25,25,26,26,27,27,27,29,30,31,31,31,32,32,33,33",
			"name": "The d_i coefficients of the standard form decomposition of U(1,X), the Ulam sequence starting with 1, X in the ordered abelian group of linear integer polynomials in X, where the ordering is lexigraphical.",
			"comment": [
				"The set U(1,X) can be defined as follows: it is the unique set of linear integer polynomials such that 1,X are the two smallest elements; for any interval [P,Q] where P,Q are linear integer polynomials, U(1,X) intersect [P,Q] has a minimum and a maximum; and finally, an element u \u003e X is in U(1,X) if and only if it is the smallest element larger than the maximum of U(1,X) intersect [1,u - 1] and which can be written as a sum of two distinct elements in U(1,X) in exactly one way.",
				"U(1,X) can be written uniquely as a union of intervals [a_i X + b_i, c_i X + d_i], where c_i X + d_i + 1 \u003c a_{i + 1} X + b_{i + 1} for all indices i. Here, we give just the coefficients d_i.",
				"This set is related to Ulam sequences in an odd way. Let U(1,n) be the sequence of integers starting with 1,n such that every subsequent term is the next smallest element that can be written as the sum of two distinct prior terms in exactly one way. Then, for all integers k, for all sufficiently large n, U(1,n) intersect [1,c_k n + d_k + 1] = eval_n(U(1,X) intersect [1,c_k X + d_k + 1]), where eval_n is the evaluation map sending X to n.",
				"It is conjectured that for all k and n \u003e 3, U(1,n) intersect [1,c_k n + d_k + 1] = eval_n(U(1,X) intersect [1,c_k X + d_k + 1]). At the time of writing, this had been checked up to k = 217529.",
				"U(1,X) can also be defined model-theoretically, as follows. Let *Z denote the hyper-integers. The sequence of sets U(1,n) indexed over positive integers has a unique extension to a sequence of sets indexed over positive hyper-integers. Take any positive non-standard integer H, and consider the corresponding set U(1,H) in this sequence. Then the intersection of U(1,H) with the set of integer polynomials in H will be isomorphic to U(1,X)."
			],
			"reference": [
				"J. Hinman, B. Kuca, A. Schlesinger, and A. Sheydvasser, The Unreasonable Rigidity of Ulam Sequences, J. Number Theory, 194 (2019), 409-425.",
				"A. Sheydvasser, The Ulam Sequence of the Integer Polynomial Ring, J. Integer Seq., accepted."
			],
			"link": [
				"J. Hinman, B. Kuca, A. Schlesinger, and A. Sheydvasser, \u003ca href=\"https://arxiv.org/abs/1711.00145\"\u003eThe Unreasonable Rigidity of Ulam Sequences\u003c/a\u003e, arXiv:1711.00145 [math.NT], 2017."
			],
			"example": [
				"The first four intervals of U(1,X) are [1,1], [X,2X], [2X + 2, 2X + 2], [4X, 4X] hence the corresponding d_i coefficients are 1,0,2,0."
			],
			"xref": [
				"Cf. A349462, A349463, A349464 for the other coefficients. The original Ulam sequence U(1,2) is A002858."
			],
			"keyword": "sign",
			"offset": "0,3",
			"author": "_Arseniy (Senia) Sheydvasser_, Nov 19 2021",
			"references": 3,
			"revision": 15,
			"time": "2021-12-27T11:04:01-05:00",
			"created": "2021-12-27T11:04:01-05:00"
		}
	]
}