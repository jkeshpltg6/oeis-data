{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007013",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7013,
			"id": "M0866",
			"data": "2,3,7,127,170141183460469231731687303715884105727",
			"name": "Catalan-Mersenne numbers: a(0) = 2; for n \u003e= 0, a(n+1) = 2^a(n) - 1.",
			"comment": [
				"The next term is too large to include.",
				"Orbit of 2 under iteration of the \"Mersenne operator\" M: n -\u003e 2^n-1 (0 and 1 are fixed points of M). - _M. F. Hasler_, Nov 15 2006",
				"Also called the Catalan sequence. - _Artur Jasinski_, Nov 25 2007",
				"a(n) divides a(n+1)-1 for every n. - _Thomas Ordowski_, Apr 03 2016",
				"Proof: if 2^a == 2 (mod a), then 2^a = 2 + ka for some k, and 2^(2^a-1) = 2^(1 + ka) = 2*(2^a)^k == 2 (mod 2^a-1). Given that a(1) = 3 satisfies 2^a == 2 (mod a), that gives you all 2^a(n) == 2 (mod a(n)), and since a(n+1) - 1 = 2^a(n) - 2 that says a(n) | a(n+1) - 1. - _Robert Israel_, Apr 05 2016",
				"All terms shown are primes, the status of the next term is currently unknown. - _Joerg Arndt_, Apr 03 2016",
				"The next term is a prime or a Fermat pseudoprime to base 2 (i.e., a member of A001567). If it is a pseudoprime, then all succeeding terms are pseudoprimes. - _Thomas Ordowski_, Apr 04 2016",
				"a(n) is the least positive integer that requires n+1 steps to reach 1 under iteration of the binary weight function A000120. - _David Radcliffe_, Jun 25 2018"
			],
			"reference": [
				"P. Ribenboim, The Book of Prime Number Records. Springer-Verlag, NY, 2nd ed., 1989, p. 81.",
				"W. Sierpiński, A Selection of Problems in the Theory of Numbers. Macmillan, NY, 1964, p. 91.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Chris K. Caldwell, \u003ca href=\"http://primes.utm.edu/mersenne/index.html#c\"\u003eMersenne Primes\u003c/a\u003e.",
				"Alex Kritov, \u003ca href=\"https://doi.org/10.13140/RG.2.2.21603.48165/7\"\u003eExplicit Values for Gravitational and Hubble Constants from Cosmological Entropy Bound and Alpha-Quantization of Particle Masses\u003c/a\u003e, 2021, see p. 8.",
				"Double Mersennes Prime Search \u003ca href=\"http://www.doublemersennes.org/history.php\"\u003eStatus of M(M(p)) where M(p) is a Mersenne prime\u003c/a\u003e [outdated link of Will Edgington replaced by _Georg Fischer_, Jan 18 2019].",
				"W. Sierpiński, \u003ca href=\"/A007013/a007013.pdf\"\u003eA Selection of Problems in the Theory of Numbers\u003c/a\u003e, Macmillan, NY, 1964, p. 91-92. (Annotated scanned copy)",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Catalan-MersenneNumber.html\"\u003eCatalan-Mersenne Number\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DoubleMersenneNumber.html\"\u003eDouble Mersenne Number\u003c/a\u003e."
			],
			"formula": [
				"a(n) = M(a(n-1)) = M^n(2) with M: n-\u003e 2^n-1. - _M. F. Hasler_, Nov 15 2006",
				"A180094(a(n)) = n + 1."
			],
			"maple": [
				"M:=n-\u003e2^n-1; '(M@@i)(2)'$i=0..4; # _M. F. Hasler_, Nov 15 2006"
			],
			"mathematica": [
				"NestList[2^#-1\u0026,2,4] (* _Harvey P. Dale_, Jul 18 2011 *)"
			],
			"program": [
				"(PARI) a(n)=if(n,2^a(n-1)-1,2) \\\\ _Charles R Greathouse IV_, Sep 07 2016"
			],
			"xref": [
				"Cf. A000668, A001567, A014221."
			],
			"keyword": "nonn",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_, Nik Lygeros (webmaster(AT)lygeros.org)",
			"ext": [
				"Edited by _Henry Bottomley_, Nov 07 2002",
				"Amended title name by _Marc Morgenegg_, Apr 14 2016"
			],
			"references": 15,
			"revision": 107,
			"time": "2021-11-17T07:18:08-05:00",
			"created": "1994-04-28T03:00:00-04:00"
		}
	]
}