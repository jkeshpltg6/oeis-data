{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A341121",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 341121,
			"data": "0,1,1,1,0,0,1,2,2,2,3,4,4,3,3,3,4,4,5,6,6,7,7,7,6,7,7,7,6,6,5,4,4,4,5,6,6,7,7,7,6,7,7,7,6,6,5,4,4,3,3,3,4,4,3,2,2,2,1,0,0,1,1,1,0,0,1,2,2,3,3,3,2,3,3,3,2,2,1,0,0,1,1,1,0,0,1",
			"name": "a(n) is the Y-coordinate of the n-th point of the space filling curve C defined in Comments section; A341120 gives X-coordinates.",
			"comment": [
				"We define the family {C_n, n \u003e= 0}, as follows:",
				"- C_0 corresponds to the points (0, 0), (0, 1), (1, 1), (2, 1) and (2, 0), in that order:",
				"       +---+---+",
				"       |       |",
				"       +       +",
				"      O",
				"- for any n \u003e= 0, C_{n+1} is obtained by arranging 4 copies of C_n as follows:",
				"                           + . . . + . . . +",
				"                           .   B   .   B   .",
				"       + . . . +           .       .       .",
				"       .   B   .           .A     C.A     C.",
				"       .       .    --\u003e    + . . . + . . . +",
				"       .A     C.           .C      .      A.",
				"       + . . . +           .      B.B      .",
				"      O                    .A      .      C.",
				"                           + . . . + . . . +",
				"                          O",
				"- the space filling curve C is the limit of C_{2*n} as n tends to infinity."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A341121/b341121.txt\"\u003eTable of n, a(n) for n = 0..16384\u003c/a\u003e",
				"F. M. Dekking, \u003ca href=\"http://dx.doi.org/10.1016/0001-8708(82)90066-4\"\u003eRecurrent Sets\u003c/a\u003e, Advances in Mathematics, vol. 44, no. 1, 1982.",
				"Rémy Sigrist, \u003ca href=\"/A341121/a341121.gp.txt\"\u003ePARI program for A341121\u003c/a\u003e",
				"\u003ca href=\"/index/Con#coordinates_2D_curves\"\u003eIndex entries for sequences related to coordinates of 2D curves\u003c/a\u003e"
			],
			"formula": [
				"a(2*n) = A341019(n).",
				"a(4*n) = 2*A341120(n).",
				"a(16*n) = 4*a(n).",
				"a(n) = A059253(n) + A283316(n+1).",
				"A059252(n) = (a(4*n+2)-1)/2."
			],
			"example": [
				"Points n and their locations X=A341120(n), Y=a(n) begin as follows.  n=7 and n=9 are both at X=3,Y=2, and n=11,n=31 both at X=3,Y=4.",
				"      |       |",
				"    4 | 16---17   12--11,31",
				"      |  |         |    |",
				"    3 | 15---14---13   10",
				"      |                 |",
				"    2 |            8---7,9",
				"      |                 |",
				"    1 |  1----2----3    6",
				"      |  |         |    |",
				"  Y=0 |  0         4----5",
				"      +--------------------",
				"       X=0    1    2    3"
			],
			"program": [
				"(PARI) See Links section."
			],
			"xref": [
				"Cf. A059252, A341019, A341120."
			],
			"keyword": "nonn",
			"offset": "0,8",
			"author": "_Kevin Ryde_ and _Rémy Sigrist_, Feb 05 2021",
			"references": 2,
			"revision": 10,
			"time": "2021-02-09T11:01:06-05:00",
			"created": "2021-02-06T14:01:31-05:00"
		}
	]
}