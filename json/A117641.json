{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A117641",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 117641,
			"data": "1,0,1,3,11,42,167,684,2867,12240,53043,232731,1031829,4615542,20805081,94410363,430945739,1977366192,9115261211,42195093993,196060049129,914110333422,4275222950221,20051858039718,94294269673861",
			"name": "Number of 3-Motzkin paths of length n with no level steps at height 0.",
			"comment": [
				"Hankel transform of this sequence forms A000012 = [1,1,1,1,1,...]. - _Philippe Deléham_, Oct 24 2007"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A117641/b117641.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"Isaac DeJager, Madeleine Naquin, Frank Seidl, \u003ca href=\"https://www.valpo.edu/mathematics-statistics/files/2019/08/Drube2019.pdf\"\u003eColored Motzkin Paths of Higher Order\u003c/a\u003e, VERUM 2019.",
				"L. W. Shapiro, C. J. Wang, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL12/Shapiro/shapiro7.html\"\u003eA bijection between 3-Motzkin paths and Schroder paths with no peak at odd height\u003c/a\u003e, JIS 12 (2009) 09.3.2."
			],
			"formula": [
				"G.f.: (1 +3*x -sqrt(1 -6*x +5*x^2))/(2*x*(3+x)).",
				"G.f. as continued fraction is 1/(1-0*x-x^2/(1-3*x-x^2/(1-3*x-x^2/(1-3*x-x^2/(.....))))). - _Paul Barry_, Dec 02 2008",
				"a(n) = A126970(n,0). - _Philippe Deléham_, Nov 24 2009",
				"a(n) = Sum_{k=0..n} A091965(n,k)*(-3)^k. - _Philippe Deléham_, Nov 28 2009",
				"a(n) = Sum_{k=1..n} Sum_{j=0..floor((n-2*k)/2)} 3^(n-2*k-2*j)*(k/(k+2*j))*binomial(k+2*j,j)*binomial(n-k-1,n-2*k-2*j). - _José Luis Ramírez Ramírez_, Mar 22 2012",
				"D-finite with recurrence: 3*(n+1)*a(n) +(-17*n+10)*a(n-1) +9*(n-3)*a(n-2) +5*(n-2)*a(n-3)=0. - _R. J. Mathar_, Dec 02 2012",
				"a(n) ~ 5^(n+3/2) / (32 * sqrt(Pi) * n^(3/2)). - _Vaclav Kotesovec_, Feb 13 2014",
				"a(n) = 1/(n+1)*Sum_{j=0..floor(n/2)} 3^(n-2*j)*C(n+1,j)*C(n-j-1,n-2*j). - _Vladimir Kruchinin_, Apr 04 2019"
			],
			"example": [
				"The a(4) = 11 paths are UUDD, UDUD and 9 of the form UXYD where each of X and Y are level steps in any of three colors."
			],
			"mathematica": [
				"CoefficientList[ Series[(1 + 3x - Sqrt[1 - 6x + 5x^2])/(2x^2 + 6x), {x, 0, 25}], x] (* _Robert G. Wilson v_ *)"
			],
			"program": [
				"(Maxima)",
				"a(n):=sum(3^(n-2*j)*binomial(n+1,j)*binomial(n-j-1,n-2*j),j,0,floor(n/2))/(n+1); /*  _Vladimir Kruchinin_, Apr 04 2019 */",
				"(PARI) my(x='x+O('x^30)); Vec( (1+3*x-sqrt(1-6*x+5*x^2))/(2*x*(3+x)) ) \\\\ _G. C. Greubel_, Apr 04 2019",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Rationals(), 30); Coefficients(R!( (1+3*x-Sqrt(1-6*x+5*x^2))/(2*x*(3+x)) )); // _G. C. Greubel_, Apr 04 2019",
				"(Sage) ((1+3*x-sqrt(1-6*x+5*x^2))/(2*x*(3+x))).series(x, 30).coefficients(x, sparse=False) # _G. C. Greubel_, Apr 04 2019"
			],
			"xref": [
				"Cf. A000957, A001006, A002212, A005043, A097331, A000108."
			],
			"keyword": "easy,nonn",
			"offset": "0,4",
			"author": "_Louis Shapiro_, Apr 10 2006",
			"references": 13,
			"revision": 43,
			"time": "2020-02-21T06:44:09-05:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}