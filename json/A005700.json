{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005700",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5700,
			"id": "M2975",
			"data": "1,1,3,14,84,594,4719,40898,379236,3711916,37975756,403127256,4415203280,49671036900,571947380775,6721316278650,80419959684900,977737404590100,12058761323277900,150656212896017400",
			"name": "a(n) = C(n)*C(n+2)-C(n+1)^2 where C() are the Catalan numbers A000108.",
			"comment": [
				"The old name was: Number of closed walks of 2n unit steps north, east, south, or west starting and ending at the origin and confined to the first octant.",
				"Image of Catalan numbers (A000108) under \"little Hankel\" transform that sends [c_0, c_1, ...] to [d_0, d_1, ...] where d_n = c_n^2 - c_{n+1}*c_{n-1}.",
				"The Niederhausen reference counts various classes of first octant paths by number of contacts with the line y=x. - _David Callan_, Sep 18 2007",
				"In Sloane and Plouffe (1995) this was incorrectly described as \"Dyck paths\".",
				"Also matchings avoiding a certain pattern (see J. Bloom and S. Elizalde). - _N. J. A. Sloane_, Jan 02 2013",
				"From _Bruce Westbury_, Aug 22 2013: (Start)",
				"a(n) is also the number of nested pairs of Dyck paths of length n starting and ending at the origin;",
				"a(n) is also the number of 3-noncrossing perfect matchings on 2n points;",
				"a(n) is also the number of 2-triangulations on n-gon;",
				"a(n) is also the dimension of the invariant subspace of 2n-th tensor power of the spin representation of Spin(5);",
				"a(n) is also the dimension of the invariant subspace of 2n-th tensor power of the defining representation of Sp(4). (End)",
				"a(-1) = -3/2, a(-2) = -1/4 makes some formulas true for all n in Z. - _Michael Somos_, Oct 02 2014",
				"a(n) is the number of uniquely sorted permutations of length 2n+1 that avoid the pattern 312. - _Colin Defant_, Jun 08 2019"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A005700/b005700.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"Jonathan Bloom and Sergi Elizalde, \u003ca href=\"http://arxiv.org/abs/1211.3442\"\u003ePattern avoidance in matchings and partitions\u003c/a\u003e, arXiv preprint arXiv:1211.3442 [math.CO], 2012. See Table 1.",
				"N. Bonichon, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2004.01.021\"\u003e A bijection between realizers of maximal plane graphs and pairs of non-crossing Dyck paths\u003c/a\u003e, Discr. Math., 298 (2005), 104-114.",
				"Matteo Cervetti and Luca Ferrari, \u003ca href=\"https://arxiv.org/abs/2009.01024\"\u003ePattern avoidance in the matching pattern poset\u003c/a\u003e, arXiv:2009.01024 [math.CO], 2020. See Section 2.",
				"W. Y. C. Cheng, E. Y. P. Deng, R. R. X. Du, R. P. Stanley and C. H. Yan, \u003ca href=\"https://arxiv.org/abs/math/0501230\"\u003eCrossings and nestings of matchings and partitions\u003c/a\u003e, arXiv:math/0501230 [math.CO], 2005.",
				"S. J. Cyvin and I. Gutman, \u003ca href=\"https://doi.org/10.1007/978-3-662-00892-8_11\"\u003eKekulé structures in benzenoid hydrocarbons\u003c/a\u003e, Lecture Notes in Chemistry, No. 46, Springer, New York, 1988 (see p. 183).",
				"C. P. Davis-Stober, \u003ca href=\"http://dx.doi.org/10.1016/j.jmp.2010.09.001\"\u003e A bijection between a set of lexicographic semiorders and pairs of non-crossing Dyck paths\u003c/a\u003e, Journal of Mathematical Psychology, 54, 471-474.",
				"M. de Sainte-Catherine, \u003ca href=\"/A006149/a006149.pdf\"\u003eCouplages et Pfaffiens en Combinatoire. Physique et Informatique.\u003c/a\u003e, Ph.D Dissertation, Université Bordeaux I, 1983. (Annotated scanned copy)",
				"Colin Defant, \u003ca href=\"https://arxiv.org/abs/1904.02627\"\u003eCatalan Intervals and Uniquely Sorted Permutations\u003c/a\u003e, arXiv:1904.02627 [math.CO], 2019.",
				"Stefan Felsner, Eric Fusy, Marc Noy, and David Orden, \u003ca href=\"https://doi.org/10.1016/j.jcta.2010.03.017\"\u003eBijections for Baxter families and related objects\u003c/a\u003e, J. Combin. Theory Ser. A, 118(3):993-1020, 2011. See Theorem 8.4",
				"E. Fusy, D. Poulalhon, G. Schaeffer, \u003ca href=\"https://doi.org/10.1016/j.ejc.2009.03.001\"\u003eBijective counting of plane bipolar orientations and Schnyder words\u003c/a\u003e, Eur. J. Combinat. 30 (2009) 1646-1658, eq (2)",
				"Juan B. Gil, Peter R. W. McNamara, Jordan O. Tirrell, Michael D. Weiner, \u003ca href=\"https://arxiv.org/abs/1708.00513\"\u003eFrom Dyck paths to standard Young tableaux\u003c/a\u003e, arXiv:1708.00513 [math.CO], 2017.",
				"D. Gouyou-Beauchamps, \u003ca href=\"http://dx.doi.org/10.1007/BFb0072513\"\u003eChemins sous-diagonaux et tableaux de Young\u003c/a\u003e, pp. 112-125 of \"Combinatoire Enumerative (Montreal 1985)\", Lect. Notes Math. 1234, Springer 1986.",
				"D. Gouyou-Beauchamps, \u003ca href=\"/A005700/a005700.pdf\"\u003eChemins sous-diagonaux et tableaux de Young\u003c/a\u003e, pp. 112-125 of \"Combinatoire Enumerative (Montreal 1985)\", Lect. Notes Math. 1234, Springer, 1986. (Annotated scanned copy)",
				"D. Gouyou-Beauchamps. \u003ca href=\"https://doi.org/10.1016/S0195-6698(89)80034-4\"\u003eStandard Young tableaux of height 4 and 5\u003c/a\u003e, European J. Combin. 10 (1989) 69 - 82.",
				"Nicholas M. Katz, \u003ca href=\"https://web.math.princeton.edu/~nmk/catalan11.pdf\"\u003eA Note on Random Matrix Integrals, Moment Identities, and Catalan Numbers\u003c/a\u003e, preprint, 2015; Mathemtika, Volume 62, Issue 3 2016 , pp. 811-817.",
				"Kiran S. Kedlaya and Andrew V. Sutherland, \u003ca href=\"http://arXiv.org/abs/0803.4462\"\u003eHyperelliptic curves, L-polynomials and random matrices\u003c/a\u003e, arXiv:0803.4462 [math.NT], 2008-2010.",
				"Alec Mihailovs, \u003ca href=\"https://arxiv.org/abs/math/9803128\"\u003eEnumeration of walks on lattices\u003c/a\u003e, arXiv:math/9803128 (1998).",
				"Heinrich Niederhausen, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL8/Niederhausen/niederhausen10.html\"\u003eA Note on the Enumeration of Diffusion Walks in the First Octant by Their Number of Contacts with the Diagonal\u003c/a\u003e, Journal of Integer Sequences, Vol. 8 (2005), Article 05.4.3.",
				"Maya Sankar, \u003ca href=\"https://arxiv.org/abs/1910.08895\"\u003eFurther Bijections to Pattern-Avoiding Valid Hook Configurations\u003c/a\u003e, arXiv:1910.08895 [math.CO], 2019.",
				"Robert Scherer, \u003ca href=\"https://www.math.ucdavis.edu/~tdenena/dissertations/202101_Scherer_Dissertation.pdf\"\u003eTopics in Number Theory and Combinatorics\u003c/a\u003e, Ph. D. Dissertation, Univ. of California Davis (2021).",
				"\u003ca href=\"/index/Y#Young\"\u003eIndex entries for sequences related to Young tableaux.\u003c/a\u003e"
			],
			"formula": [
				"G.f.: 3F2( [ 1, 1/2, 3/2 ]; [ 3, 4 ]; 16 x ).",
				"a(n) = 6*(2*n)!*(2*n+2)!/(n!*(n+1)!*(n+2)!*(n+3)!) (Mihailovs).",
				"a(n) = Det[Table[binomial[i+1, j-i+2], {i, 1, n}, {j, 1, n}]] - _David Callan_, Jul 20 2005",
				"a(n) = b(n)b(n+1)/6 where b(n) is the superballot number A007054. - _David Callan_, Feb 01 2007",
				"a(n) = A000108(n)*A000108(n+2)-A000108(n+1)^2. - _Philippe Deléham_, Apr 11 2007",
				"G.f.: (1 + 6*x - hypergeom([-1/2,-3/2],[2],16*x))/(4*x^2). - _Mark van Hoeij_, Nov 02 2009",
				"a(n) = 12 * 4^n * (2*n-1)!! * (2*n+1)!! / ((n+2)! * (n+3)!). - _Michael Somos_, Oct 02 2014",
				"D-finite with recurrence 0 = a(n) * 4*(2*n+1)*(2*n+3) - a(n+1) * (n+3)*(n+4) for all n in Z. - _Michael Somos_, Oct 02 2014",
				"0 = a(n)*(+65536*a(n+2) - 72192*a(n+3) + 10296*a(n+4)) + a(n+1)*(-1536*a(n+2) - 1632*a(n+3) - 282*a(n+4)) + a(n+2)*(+40*a(n+2) - 6*a(n+3) + a(n+4)) for all n in Z. - _Michael Somos_, Oct 02 2014",
				"0 = a(n)^2*a(n+2)*(+1792*a(n+1) - 882*a(n+2)) + a(n)*a(n+1)^2*(+768*a(n+1) + 580*a(n+2)) + 7*a(n)*a(n+1)*a(n+2)^2 +a(n+1)^3*(-18*a(n+1) + 3*a(n+2)) for all n in Z. - _Michael Somos_, Oct 02 2014",
				"a(n) ~ 3 * 2^(4*n+3) / (Pi * n^5). - _Vaclav Kotesovec_, Feb 10 2015"
			],
			"example": [
				"Example: a(2)=3 counts EWEW, EEWW, ENSW.",
				"G.f. = 1 + x + 3*x^2 + 14*x^3 + 84*x^4 + 594*x^5 + 4719*x^6 + 40898*x^7 + ..."
			],
			"mathematica": [
				"CoefficientList[ Series[ HypergeometricPFQ[ {1, 1/2, 3/2}, {3, 4}, 16 x], {x, 0, 19}], x]",
				"a[ n_] := If[ n \u003c 1, Boole[n == 0], Det[ Table[ Binomial[i + 1, j - i + 2], {i, n}, {j, n}]]]; (* _Michael Somos_, Feb 25 2014 *) (* slight modification of David Callan formula *)",
				"a[ n_] := 12 * 4^n * (2*n-1)!! * (2*n+1)!! / ((n+2)! * (n+3)!); (* _Michael Somos_, Oct 02 2014 *)"
			],
			"program": [
				"(MAGMA) [6*Factorial(2*n)*Factorial(2*n+2)/(Factorial(n)*Factorial(n+1)* Factorial(n+2)*Factorial(n+3)): n in [0..25]]; // _Vincenzo Librandi_, Aug 04 2011",
				"(PARI) a(n)=6*binomial(2*n+2,n)*(2*n)!/(n+1)!/(n+3)! \\\\ _Charles R Greathouse IV_, Aug 04 2011",
				"(LiE) p_tensor(2*n,[0,1],B2)|[0,0]",
				"(LiE) p_tensor(2*n,[1,0],C2)|[0,0]",
				"(PARI) {a(n) = if( n\u003c0, if( n\u003c-2, 0, [-3/2, -1/4][-n]), 6 * (2*n)! * (2*n+2)! / (n! * (n+1)! * (n+2)! * (n+3)!))}; /* _Michael Somos_, Oct 02 2014 */"
			],
			"xref": [
				"A column of the triangle in A179898. A diagonal of the triangle in A185249.",
				"Row sums of A193691, A193692. - _Alois P. Heinz_, Aug 03 2011",
				"See A138349 for another version.",
				"Cf. A000108, A248152."
			],
			"keyword": "nonn,walk,easy",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _James A. Sellers_, Dec 24 1999",
				"Corrected by _Vladeta Jovovic_, May 23 2004",
				"Better definition from _David Callan_, Sep 18 2007",
				"Definition simplified by _N. J. A. Sloane_, Nov 30 2020"
			],
			"references": 20,
			"revision": 136,
			"time": "2021-05-24T23:30:34-04:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}