{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A322366",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 322366,
			"data": "1,0,2,2,3,2,5,2,5,4,7,2,11,2,9,8,9,2,17,2,17,10,13,2,23,6,15,10,23,2,29,2,17,14,19,12,35,2,21,16,37,2,41,2,35,38,25,2,47,8,47,20,41,2,53,16,51,22,31,2,59,2,33,52,33,18,65,2,53,26,67,2,71,2,39,68,59,18,77,2,77,28,43,2,83,22,45,32,79",
			"name": "Number of integers k in {0,1,...,n} such that k identical test tubes can be balanced in a centrifuge with n equally spaced holes.",
			"comment": [
				"Numbers where a(n) + A000010(n) != n + 1: A102467. - _Robert G. Wilson v_, Aug 23 2021"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A322366/b322366.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Holly Krieger and Brady Haran, \u003ca href=\"https://www.youtube.com/watch?v=7DHE8RnsCQ8\"\u003eThe Centrifuge Problem\u003c/a\u003e, Numberphile video (2018)",
				"T. Y. Lam and K. H. Leung, \u003ca href=\"http://arXiv.org/abs/math.NT/9511209\"\u003eOn vanishing sums for roots of unity\u003c/a\u003e, arXiv:math/9511209 [math.NT], 1995.",
				"Matt Parker, \u003ca href=\"https://mattbaker.blog/2018/06/25/the-balanced-centrifuge-problem/\"\u003eThe Balanced Centrifuge Problem\u003c/a\u003e, Math Blog, 2018.",
				"Gary Sivek, \u003ca href=\"http://www.emis.de/journals/INTEGERS/papers/k31/k31.Abstract.html\"\u003eOn vanishing sums of distinct roots of unity\u003c/a\u003e, #A31, Integers 10 (2010), 365-368.",
				"Robert G. Wilson v, \u003ca href=\"/A322366/a322366_3.pdf\"\u003eGraph of the first 100001 terms.\u003c/a\u003e"
			],
			"formula": [
				"a(n) = |{ k : k and n-k can be written as a sum of prime factors of n }|.",
				"a(n) = 2 \u003c=\u003e n is prime (A000040).",
				"a(n) \u003e= n-1 \u003c=\u003e n in {1,2,3,4} union { A008588 }.",
				"a(n) = (n+4)/2 \u003c=\u003e n in { A100484 } minus { 4 }.",
				"a(n) = (n+9)/3 \u003c=\u003e n in { A001748 } minus { 9 }.",
				"a(n) = (n+25)/5 \u003c=\u003e n in { A001750 } minus { 25 }.",
				"a(n) = (n+49)/7 \u003c=\u003e n in { A272470 } minus { 49 }.",
				"a(n^2) = n+1 \u003c=\u003e n = 0 or n is prime \u003c=\u003e n in { A182986 }.",
				"a(A001248(n)) = A008864(n).",
				"a(n) is odd \u003c=\u003e n in { A163300 }.",
				"a(n) is even \u003c=\u003e n in { A004280 }."
			],
			"example": [
				"a(6) = |{0,2,3,4,6}| = 5.",
				"a(9) = |{0,3,6,9}| = 4.",
				"a(10) = |{0,2,4,5,6,8,10}| = 7."
			],
			"maple": [
				"a:= proc(n) option remember; local f, b; f, b:=",
				"       map(i-\u003e i[1], ifactors(n)[2]),",
				"       proc(m, i) option remember; m=0 or i\u003e0 and",
				"        (b(m, i-1) or f[i]\u003c=m and b(m-f[i], i))",
				"       end; forget(b); (t-\u003e add(",
				"      `if`(b(j, t) and b(n-j, t), 1, 0), j=0..n))(nops(f))",
				"    end:",
				"seq(a(n), n=0..100);"
			],
			"mathematica": [
				"$RecursionLimit = 4096;",
				"a[1] = 0;",
				"a[n_] := a[n] = Module[{f, b}, f = FactorInteger[n][[All, 1]];",
				"     b[m_, i_] := b[m, i] = m == 0 || i \u003e 0 \u0026\u0026",
				"     (b[m, i - 1] || f[[i]] \u003c= m \u0026\u0026 b[m - f[[i]], i]);",
				"     With[{t = Length[f]}, Sum[",
				"     If[b[j, t] \u0026\u0026 b[n - j, t], 1, 0], {j, 0, n}]]];",
				"Table[a[n], {n, 0, 1000}] (* _Jean-François Alcover_, Dec 13 2018, after _Alois P. Heinz_, corrected and updated Aug 07 2021 *)",
				"f[n_] := Block[{c = 2, k = 2, p = First@# \u0026 /@ FactorInteger@ n}, While[k \u003c n, If[ IntegerPartitions[k, All, p, 1] != {} \u0026\u0026 IntegerPartitions[n - k, All, p, 1] != {}, c++]; k++]; c]; f[0] = 1; f[1] = 0; Array[f, 75] (* _Robert G. Wilson v_, Aug 22 2021 *)"
			],
			"xref": [
				"Cf. A000040, A001248, A001748, A001750, A004280, A008588, A008864, A100484, A103306, A103314, A163300, A182986, A272470, A306275."
			],
			"keyword": "nonn,look",
			"offset": "0,3",
			"author": "_Alois P. Heinz_, Dec 04 2018",
			"references": 4,
			"revision": 69,
			"time": "2021-10-06T18:24:01-04:00",
			"created": "2018-12-04T23:29:40-05:00"
		}
	]
}