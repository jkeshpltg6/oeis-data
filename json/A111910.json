{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A111910",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 111910,
			"data": "1,1,1,1,5,1,1,14,14,1,1,30,84,30,1,1,55,330,330,55,1,1,91,1001,2145,1001,91,1,1,140,2548,10010,10010,2548,140,1,1,204,5712,37128,68068,37128,5712,204,1,1,285,11628,116280,352716,352716,116280,11628,285,1",
			"name": "Square array read by antidiagonals: S(p,q) = (p+q+1)!(2p+2q+1)!/((p+1)!(2p+1)!(q+1)!(2q+1)!) (p,q\u003e=0).",
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A111910/b111910.txt\"\u003eTable of n, a(n) for n = 0..11475\u003c/a\u003e (rows 0 \u003c= n \u003c= 150, flattened).",
				"G. Kreweras and H. Niederhausen, \u003ca href=\"https://doi.org/10.1016/S0195-6698(81)80020-0\"\u003eSolution of an enumerative problem connected with lattice paths\u003c/a\u003e, European J. Combin. 2 (1981), 55-60.",
				"Anthony J. Wood, Richard A. Blythe, and Martin R. Evans, \u003ca href=\"https://arxiv.org/abs/1908.00942\"\u003eCombinatorial mappings of exclusion processes\u003c/a\u003e, arXiv:1908.00942 [cond-mat.stat-mech], 2019."
			],
			"formula": [
				"S(n,n) = A111911(n).",
				"From _Peter Bala_, Oct 13 2011: (Start)",
				"Define a(n) = n!*(n+1/2)!*(n+1)!/(1/2)!.",
				"S(n,k) = a(n+k)/(a(n)*a(k)) gives the sequence as a square array while T(n,k) = a(n)/(a(n-k)*a(k)) gives the sequence as a triangle.",
				"S(n-1,k)*S(n,k+1)*S(n+1,k-1) = S(n-1,k+1)*S(n,k-1)*S(n+1,k). Cf. A091044.",
				"(End)",
				"From _G. C. Greubel_, Feb 12 2021: (Start)",
				"As a number triangle:",
				"T(n, k) = binomial(n+1, k)*binomial(2*n+1, 2*k)/((k+1)*(2*k+1)).",
				"T(n, k) = binomial(2*n+1, 2*k)/((2*k+1)*binomial(n, k)) * A001263(n+1, k+1). (End)",
				"From _Peter Bala_, Sep 19 2021: (Start)",
				"As a triangle: T(n,k) = a(n)/(a(n-k)*a(k)), where a(n) = Product_{j = 1..n} s(p,j) with s(p,j) = Sum_{j = 1..n} j^p and p = 2. Note, p = 0 gives Pascal's triangle A007318, p = 1 gives the triangle of Narayana numbers A001263 and p = 3 gives the triangle A174158 whose entries are the squares of the Narayana numbers.",
				"Let E(y) = Sum_{n \u003e= 0} y^n/((n+1)!*(2*n+1)!). Then as a triangle this is the generalized Riordan array (E(y), y) as defined in Wang and Wang with respect to the sequence c_n = (n+1)!*(2*n+1)!.  Cf. A001263.",
				"Generating function: E(y)*E(x*y) = 1 + (1 + x)*y/(2!*3!) + (1 + 5*x + x^2)*y^2/(3!*5!) + (1 + 14*x + 14*x^2 + x^3)*y^3/(4!*7!) + ....",
				"The n-th power of this array has a generating function E(y)^n*E(x*y). In particular, the matrix inverse has a generating function E(x*y)/E(y).",
				"exp(y)*E(y) = 1 + 13*y/(2!*3!) + 421*y^2/(3!*5!) + 25368*y^3/(4!*7!) + ... is essentially a generating function for A081442. (End)"
			],
			"example": [
				"Array S(n,k) in rectangular form (n, k \u003e= 0):",
				"  1,  1,    1,     1,    1,       1,       1,       1,        1, ...",
				"  1,  5,   14,    30,   55,      91,     140,     204,      285, ...",
				"  1, 14,   84,   330,  1001,   2548,    5712,   11628,    21945, ...",
				"  1, 30,  330,  2145, 10010,  37128,  116280,  319770,   793155, ...",
				"  1, 55, 1001, 10010, 68068, 352716, 1492260, 5393454, 17185025, ...",
				"  ...",
				"Array T(n,k) in triangular form (n \u003e= 0 and 0 \u003c= k \u003c= n):",
				"  1,",
				"  1,  1,",
				"  1,  5,    1,",
				"  1, 14,   14,   1,",
				"  1, 30,   84,  30,     1,",
				"  1, 55,  330, 330,    55,  1,",
				"  1, 91, 1001, 2145, 1001, 91, 1,",
				"  ..."
			],
			"maple": [
				"a:=(p,q)-\u003e(p+q+1)!*(2*p+2*q+1)!/(p+1)!/(2*p+1)!/(q+1)!/(2*q+1)!: for n from 0 to 10 do seq(a(j,n-j),j=0..n) od; # yields sequence in triangular form"
			],
			"mathematica": [
				"Table[(# + q + 1)! (2 # + 2 q + 1)!/((# + 1)! (2 # + 1)! (q + 1)! (2 q + 1)!) \u0026[r - q], {r, 0, 9}, {q, 0, r}] // Flatten (* _Michael De Vlieger_, Oct 21 2019 *)",
				"Table[Binomial[n+1, k]*Binomial[2*n+1, 2*k]/((k+1)*(2*k+1)), {n, 0, 12}, {k, 0,",
				"n}]//Flatten (* _G. C. Greubel_, Feb 12 2021 *)"
			],
			"program": [
				"(Sage)",
				"def A111910(n,k): return binomial(n+1, k)*binomial(2*n+1, 2*k)/((k+1)*(2*k+1))",
				"flatten([[A111910(n,k) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Feb 12 2021",
				"(Magma)",
				"T:= func\u003c n,k | Binomial(n+1, k)*Binomial(2*n+1, 2*k)/((k+1)*(2*k+1)) \u003e;",
				"[T(n,k): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Feb 12 2021"
			],
			"xref": [
				"Cf. A091044, A111911 (main diagonal), A196148 (row sums of triangle).",
				"Cf. A001263, A174158."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Emeric Deutsch_, Aug 19 2005",
			"ext": [
				"Example section edited by _Petros Hadjicostas_, Sep 03 2019"
			],
			"references": 6,
			"revision": 45,
			"time": "2021-10-06T12:56:37-04:00",
			"created": "2005-09-21T03:00:00-04:00"
		}
	]
}