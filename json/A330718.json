{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A330718",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 330718,
			"data": "0,1,3,13,25,137,245,871,517,4629,8349,45517,83317,1074679,1992127,7424789,13901189,78403447,147940327,280060651,531718651,11133725681,21243819521,40621501691,15565330735,388375065019,248882304985,479199924517,923951191477,2973006070891",
			"name": "a(n) = numerator(Sum_{k=1..n} (2^k-2)/k).",
			"comment": [
				"If p \u003e 3 is prime, then p^2 | a(p).",
				"Note the similarity to Wolstenholme's theorem.",
				"Conjecture: for n \u003e 3, if n^2 | a(n), then n is prime.",
				"Are there the weak pseudoprimes m such that m | a(m)?",
				"Primes p such that p^3 | a(p) are probably A088164.",
				"If p is an odd prime, then a(p+1) == A330719(p+1) (mod p).",
				"If p \u003e 3 is a prime, then p^2 | numerator(Sum_{k=1..p+1} F(k)), where F(n) = Sum_{k=1..n} (2^(k-1)-1)/k. Cf. A027612 (a weaker divisibility)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A330718/b330718.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/WolstenholmesTheorem.html\"\u003eWolstenholme's Theorem\u003c/a\u003e"
			],
			"formula": [
				"a(n) = numerator(Sum_{k=1..n} (2^(k-1)-1)/k).",
				"a(n+1) = numerator(a(n)/A330719(n) + A225101(n+1)/(2*A159353(n+1))).",
				"a(p) = a(p-1) + A007663(n)*A330719(p-1) for p = prime(n) \u003e 2.",
				"a(n) = numerator(-(2^(n+1)*LerchPhi(2,1,n+1) + Pi*i + 2*HarmonicNumber(n))). - _G. C. Greubel_, Dec 28 2019",
				"a(n) = numerator(A279683(n)/n!) for n \u003e 0. - _Amiram Eldar_ and _Thomas Ordowski_, Jan 15 2020"
			],
			"maple": [
				"seq(numer(add((2^k -2)/k, k = 1..n)), n = 1..30); # _G. C. Greubel_, Dec 28 2019"
			],
			"mathematica": [
				"Numerator @ Accumulate @ Array[(2^# - 2)/# \u0026, 30]",
				"Table[Numerator[Simplify[-(2^(n+1)*LerchPhi[2,1,n+1] +Pi*I +2*HarmonicNumber[n])]], {n,30}] (* _G. C. Greubel_, Dec 28 2019 *)"
			],
			"program": [
				"(PARI) a(n) = numerator(sum(k=1, n, (2^k-2)/k)); \\\\ _Michel Marcus_, Dec 28 2019",
				"(MAGMA) [Numerator( \u0026+[(2^k -2)/k: k in [1..n]] ): n in [1..30]]; // _G. C. Greubel_, Dec 28 2019",
				"(Sage) [numerator( sum((2^k -2)/k for k in (1..n)) ) for n in (1..30)] # _G. C. Greubel_, Dec 28 2019"
			],
			"xref": [
				"Cf. A001008, A007663, A088164, A159353, A225101, A279683, A330719."
			],
			"keyword": "nonn,frac",
			"offset": "1,3",
			"author": "_Amiram Eldar_ and _Thomas Ordowski_, Dec 28 2019",
			"references": 6,
			"revision": 52,
			"time": "2020-01-15T10:05:42-05:00",
			"created": "2019-12-28T07:48:09-05:00"
		}
	]
}