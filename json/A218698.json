{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A218698",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 218698,
			"data": "1,1,1,3,2,2,6,3,2,2,14,5,4,3,3,27,7,4,3,2,2,60,11,8,6,5,4,4,117,15,8,6,4,3,2,2,246,22,13,9,8,6,5,4,4,490,30,15,12,8,7,5,4,3,3,1002,42,22,14,12,9,8,6,5,4,4,1998,56,24,16,12,10,7,6,4,3,2,2",
			"name": "Number T(n,k) of ways to divide the partitions of n into nonempty consecutive subsequences each of which contains only equal parts and parts from distinct subsequences differ by at least k; triangle T(n,k), n\u003e=0, 0\u003c=k\u003c=n, read by rows.",
			"comment": [
				"T(n,k) is defined for n,k \u003e= 0. The triangle contains terms with k \u003c= n. T(n,k) = T(n,n) = A000005(n) for k \u003e= n.  For k\u003e0: T(n,k) = number of partitions of n in which any two distinct parts differ by at least k, or, equivalently, T(n,k) = number of partitions of n in which each part, with the possible exception of the largest, occurs at least k times."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A218698/b218698.txt\"\u003eRows n = 0..140, flattened\u003c/a\u003e"
			],
			"formula": [
				"G.f. of column k: 1 + Sum_{j\u003e=1} x^j/(1-x^j) * Product_{i=1..j-1} (1+x^(k*i)/(1-x^i))."
			],
			"example": [
				"T(4,0) = 14: [[1],[1],[1],[1]], [[1,1],[1],[1]], [[1],[1,1],[1]], [[1,1,1],[1]], [[1],[1],[1,1]], [[1,1],[1,1]], [[1],[1,1,1]], [[1,1,1,1]], [[1],[1],[2]], [[1,1],[2]], [[2],[2]], [[2,2]], [[1],[3]], [[4]].",
				"T(4,1) = 5: [[1,1,1,1]], [[1,1],[2]], [[2,2]], [[1],[3]], [[4]].",
				"T(4,2) = 4: [[1,1,1,1]], [[2,2]], [[1],[3]], [[4]].",
				"T(4,3) = T(4,4) = A000005(4) = 3: [[1,1,1,1]], [[2,2]], [[4]].",
				"Triangle T(n,k) begins:",
				"    1;",
				"    1,  1;",
				"    3,  2,  2;",
				"    6,  3,  2,  2;",
				"   14,  5,  4,  3,  3;",
				"   27,  7,  4,  3,  2,  2;",
				"   60, 11,  8,  6,  5,  4,  4;",
				"  117, 15,  8,  6,  4,  3,  2,  2;",
				"  ..."
			],
			"maple": [
				"b:= proc(n, i, k) option remember; `if`(n=0, 1, `if`(i\u003c1, 0,",
				"       b(n, i-1, k) +add(b(n-i*j, i-k, k), j=1..n/i)))",
				"    end:",
				"T:= (n, k)-\u003e b(n, n, k):",
				"seq(seq(T(n,k), k=0..n), n=0..12);"
			],
			"mathematica": [
				"b[n_, i_, k_] :=  b[n, i, k] =  If[n == 0, 1, If[i \u003c 1, 0,  b[n, i - 1, k] + Sum[b[n - i*j, i - k, k], {j, 1, n/i}]]]; T[n_, k_] := b[n, n, k]; Table[Table[T[n, k], {k, 0, n}], {n, 0, 12}] // Flatten (* _Jean-François Alcover_, Dec 27 2013, translated from Maple *)"
			],
			"xref": [
				"Columns k=0-10 give: A006951, A000041, A116931, A116932, A218699, A218700, A218701, A218702, A218703, A218704, A218705.",
				"Main diagonal gives: A000005.",
				"T(2n,n) gives A319776."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Alois P. Heinz_, Nov 04 2012",
			"references": 12,
			"revision": 25,
			"time": "2020-12-10T17:42:11-05:00",
			"created": "2012-11-05T20:36:45-05:00"
		}
	]
}