{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A201203",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 201203,
			"data": "1,-5,29,-201,1631,-15173,159093,-1854893,23788271,-332613321,5033396573,-81929955953,1426898945343,-26468817431501,520884561854501,-10836674357638293,237603001692915983,-5475288709200573713,132276033079845108621",
			"name": "Alternating row sums of triangle A201201: first associated monic Laguerre-Sonin(e) polynomials with parameter alpha=1 evaluated at x=-1.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A201203/b201203.txt\"\u003eTable of n, a(n) for n = 0..440\u003c/a\u003e",
				"Michael Wallner, \u003ca href=\"https://arxiv.org/abs/1706.07163\"\u003eA bijection of plane increasing trees with relaxed binary trees of right height at most one\u003c/a\u003e, arXiv:1706.07163 [math.CO], 2017, Table 2 on p. 13."
			],
			"formula": [
				"a(n) = Sum_{k=0..n} ((-1)^k)*A201201(n,k), n\u003e=0.",
				"a(n)+(2*n+3)*a(n-1)+n*(n+1)*a(n-2)=0, a(-1)=0, a(0)=1. - _R. J. Mathar_, Dec 07 2011",
				"From _Wolfdieter Lang_, Dec 11 2011: (Start)",
				"E.g.f. from A201201 with x=-1, z-\u003ex: g(x) = exp(1/(1+x))*(3+2*x)*(exp(-1) + (Ei(1,1/(1+x))-Ei(1,1)))/(1+x)^4-(2+x)/(1+x)^3, with the exponential integral Ei.",
				"This e.g.f. satisfies the homogeneous ordinary second-order differential equation (1+x)^2*(d^2(g(x))/dx^2) + (7+6*x)*(d(g(x))/dx)+6*g(x), with g(0)=1 and (d(g(x))/dx)_{x=0} = -5. This is equivalent to the recurrence conjectured above by _R. J. Mathar_, which is thus proved.",
				"(End)",
				"Let G denote Gompertz's constant A073003. The unsigned sequence is the sequence of numerators in the convergents coming from the infinite continued fraction expansion 1 - G = 1/(3 - 2/(5 - 6/(7 - ... - n*(n+1)/((2*n+3) - ...)))). The sequence of convergents begins [1/3, 5/13, 29/73, 201/501, ...]. The denominators are in A000262. - _Peter Bala_, Aug 19 2013",
				"a(n) ~ (-1)^n * 2^(-1/2)*(exp(-1)-Ei(1,1)) * exp(2*sqrt(n)-n+1/2) * n^(n+7/4) * (1+91/(48*sqrt(n))), where Ei(1,1) = 0.21938393439552... = G / exp(1), where G = 0.596347362323194... is the Gompertz constant (see A073003). - _Vaclav Kotesovec_, Oct 19 2013"
			],
			"maple": [
				"A201203 := proc(n)",
				"    add((-1)^k*A201201(n,k),k=0..n) ;",
				"end proc:",
				"seq(A201203(n),n=0..20) ; # _R. J. Mathar_, Dec 07 2011"
			],
			"mathematica": [
				"Flatten[{1,RecurrenceTable[{n*(1+n)*a[-2+n]+(3+2*n)*a[-1+n] +a[n]==0, a[1]==-5,a[2]==29}, a, {n, 20}]}] (* _Vaclav Kotesovec_, Oct 19 2013 *)"
			],
			"xref": [
				"Cf. A201201, A201202 (row sums), A073003, A002793."
			],
			"keyword": "sign,easy",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Dec 06 2011",
			"ext": [
				"_R. J. Mathar_ conjecture corrected and proved by _Wolfdieter Lang_, Dec 11 2011"
			],
			"references": 10,
			"revision": 37,
			"time": "2018-02-08T14:16:08-05:00",
			"created": "2011-12-07T19:46:36-05:00"
		}
	]
}