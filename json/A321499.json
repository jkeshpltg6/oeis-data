{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A321499",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 321499,
			"data": "3,5,7,9,11,13,15,16,17,19,21,23,24,25,27,29,31,32,33,35,37,39,40,41,43,45,47,48,49,51,53,55,56,57,59,61,63,64,65,67,69,71,72,73,75,77,79,80,81,83,85,87,88,89,91,93,95,96,97,99,101,103,104,105,107,109,111,112,113,115",
			"name": "Numbers of the form (x - y)(x^2 - y^2) with x \u003e y \u003e 0.",
			"comment": [
				"Equivalently, numbers of the form (x - y)^2*(x + y) or d^2*(2m + d), for (x, y) = (m+d, m). This shows that this consists of all squares d^2 \u003e 0 times all numbers of the same parity and larger than d. In particular, for d=1, all odd numbers \u003e 1, and for d=2, 4*(even numbers \u003e 2) = 8*(any number \u003e 1). Larger d can't yield additional terms, neither odd nor even: The sequence consists exactly of all odd numbers \u003e 2 and multiples of 8 larger than 8."
			],
			"link": [
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,0,0,0,1,-1)."
			],
			"formula": [
				"Asymptotic density is 5/8. Complement is A321501.",
				"a(5k-2) = 8k for all k \u003e 1, a(n) = floor((n+2)*4/5)*2 + 1 for all other n \u003e 3.",
				"a(n + 5) = a(n) + 8 for n \u003e 3. - _David A. Corneth_, Nov 23 2018",
				"O.g.f. 3*x+5*x^2+7*x^3 -x^4*(-9-2*x-2*x^2-2*x^3-x^4+8*x^5) / ( (x^4+x^3+x^2+x+1) *(x-1)^2 ). - _R. J. Mathar_, Nov 29 2018"
			],
			"example": [
				"a(1) = 3 = 1*3 = (2 - 1)*(2^2 - 1^2). Similarly any larger odd number 2k+1 = (k+1 - k)((k+1)^2 - k^2) is in this sequence.",
				"a(8) = 16 = 2*8 = (3 - 1)*(3^2 - 1^2). Similarly, any larger multiple of 8, 8*(1 + k) = 2*(4k + 4) = (k+2 - k)((k+2)^2 - k^2) is in this sequence."
			],
			"program": [
				"(PARI) is(n)={n\u0026\u0026fordiv(n,d, d^2*(d+2)\u003en \u0026\u0026 break; n%d^2\u0026\u0026next; bittest(n\\d^2-d,0)||return(1))} \\\\ This uses the definition. More efficient variant below.",
				"(PARI) select( is_A321499(n)=if(bittest(n,0),n\u003e1,n%8,0,n\u003e8), [0..99]) \\\\ Defines the function is_A321499(). The select() command is just an illustration and check.",
				"(PARI) A321499_list(M)=setunion(vector(M\\2-1,k,2*k+1),[2..M\\8]*8) \\\\ list all terms up to M; more efficient than select() above.",
				"(PARI) apply( A321499(n)=if(n\u003c8, 2*n+1, n%5!=3, (n+2)*4\\5*2+1, n\\5*8+8), [1..30]) \\\\ Defines A321499(n). The apply() command provides a check \u0026 illustration."
			],
			"xref": [
				"See A321491 for numbers of the form (x+y)(x^2+y^2).",
				"Cf. A321501 (complement).",
				"See A321498 for numbers that have two representations of the form (x-y)(x^2-y^2).",
				"Cf. A106505 (conjectured to be the sequence without the 3)."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_M. F. Hasler_, Nov 22 2018",
			"references": 4,
			"revision": 19,
			"time": "2019-11-11T00:39:09-05:00",
			"created": "2018-11-24T01:14:19-05:00"
		}
	]
}