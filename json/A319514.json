{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A319514",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 319514,
			"data": "0,0,0,1,1,1,1,0,2,0,2,1,2,2,1,2,0,2,0,3,1,3,2,3,3,3,3,2,3,1,3,0,4,0,4,1,4,2,4,3,4,4,3,4,2,4,1,4,0,4,0,5,1,5,2,5,3,5,4,5,5,5,5,4,5,3,5,2,5,1,5,0,6,0,6,1,6,2,6,3,6,4,6,5,6,6,5",
			"name": "The shell enumeration of N X N where N = {0, 1, 2, ...}, also called boustrophedonic Rosenberg-Strong function. Terms are interleaved x and y coordinates.",
			"comment": [
				"If (x, y) and (x', y') are adjacent points on the trajectory of the map then for the boustrophedonic Rosenberg-Strong function max(|x - x'|, |y - y'|) is always 1 whereas for the Rosenberg-Strong function this quantity can become arbitrarily large. In this sense the boustrophedonic variant is continuous in contrast to the original Rosenberg-Strong function.",
				"We implemented the enumeration also as a state machine to avoid the evaluation of the square root function.",
				"The inverse function, computing n for given (x, y), is m*(m + 1) + (-1)^(m mod 2)*(y - x) where m = max(x, y)."
			],
			"reference": [
				"A. L. Rosenberg, H. R. Strong, Addressing arrays by shells, IBM Technical Disclosure Bulletin, vol 14(10), 1972, p. 3026-3028."
			],
			"link": [
				"Peter Luschny, \u003ca href=\"/A319514/b319514.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Georg Cantor, \u003ca href=\"http://gdz.sub.uni-goettingen.de/dms/resolveppn/?PPN=GDZPPN002156806\"\u003eEin Beitrag zur Mannigfaltigkeitslehre\u003c/a\u003e, Journal für die reine und angewandte Mathematik 84 (1878), 242-258.",
				"Steven Pigeon, \u003ca href=\"https://hbfs.wordpress.com/2018/08/07/moeud-deux/\"\u003eMœud deux\u003c/a\u003e, 2018.",
				"A. L. Rosenberg, \u003ca href=\"https://doi.org/10.1145/321850.321861\"\u003eAllocating storage for extendible Arrays\u003c/a\u003e, J. ACM, vol 21(4), 1974, p. 652-670.",
				"M. P. Szudzik, \u003ca href=\"http://arxiv.org/abs/1706.04129\"\u003eThe Rosenberg-Strong Pairing Function\u003c/a\u003e, arXiv:1706.04129 [cs.DM], 2017."
			],
			"example": [
				"The map starts, for n = 0, 1, 2, ...",
				"(0, 0), (0, 1), (1, 1), (1, 0), (2, 0), (2, 1), (2, 2), (1, 2), (0, 2), (0, 3),",
				"(1, 3), (2, 3), (3, 3), (3, 2), (3, 1), (3, 0), (4, 0), (4, 1), (4, 2), (4, 3),",
				"(4, 4), (3, 4), (2, 4), (1, 4), (0, 4), (0, 5), (1, 5), (2, 5), (3, 5), (4, 5),",
				"(5, 5), (5, 4), (5, 3), (5, 2), (5, 1), (5, 0), (6, 0), (6, 1), (6, 2), (6, 3),",
				"(6, 4), (6, 5), (6, 6), (5, 6), (4, 6), (3, 6), (2, 6), (1, 6), (0, 6), ...",
				"The enumeration can be seen as shells growing around the origin:",
				"(0, 0);",
				"(0, 1), (1, 1), (1, 0);",
				"(2, 0), (2, 1), (2, 2), (1, 2), (0, 2);",
				"(0, 3), (1, 3), (2, 3), (3, 3), (3, 2), (3, 1), (3, 0);",
				"(4, 0), (4, 1), (4, 2), (4, 3), (4, 4), (3, 4), (2, 4), (1, 4), (0, 4);",
				"(0, 5), (1, 5), (2, 5), (3, 5), (4, 5), (5, 5), (5, 4), (5, 3), (5, 2),(5,1),(5,0);"
			],
			"program": [
				"(Julia)",
				"function A319514(n)",
				"    k, r = divrem(n, 2)",
				"    m = x = isqrt(k)",
				"    y = k - x^2",
				"    x \u003c= y \u0026\u0026 ((x, y) = (2x - y, x))",
				"    isodd(m) ? (y, x)[r+1] : (x, y)[r+1]",
				"end",
				"[A319514(n) for n in 0:52] |\u003e println",
				"# The enumeration of N X N with a state machine:",
				"# PigeonRosenbergStrong(n)",
				"function PRS(x, y, state)",
				"    x == 0 \u0026\u0026 state == 0 \u0026\u0026 return x, y+1, 1",
				"    y == 0 \u0026\u0026 state == 2 \u0026\u0026 return x+1, y, 3",
				"    x == y \u0026\u0026 state == 1 \u0026\u0026 return x, y-1, 2",
				"    x == y \u0026\u0026 return x-1, y, 0",
				"    state == 0 \u0026\u0026 return x-1, y, 0",
				"    state == 1 \u0026\u0026 return x+1, y, 1",
				"    state == 2 \u0026\u0026 return x, y-1, 2",
				"    return x, y+1, 3",
				"end",
				"function ShellEnumeration(len)",
				"    x, y, state = 0, 0, 0",
				"    for n in 0:len",
				"        println(\"$n -\u003e ($x, $y)\")",
				"        x, y, state = PRS(x, y, state)",
				"    end",
				"end",
				"# Computes n for given (x, y).",
				"function Pairing(x::Int, y::Int)",
				"    m = max(x, y)",
				"    d = isodd(m) ? x - y : y - x",
				"    m*(m + 1) + d",
				"end",
				"ShellEnumeration(20)"
			],
			"xref": [
				"Cf. A319289 (x coordinates), A319290 (y coordinates).",
				"Cf. A319571 (stripe enumeration), A319572 (stripe x), A319573 (stripe y).",
				"A319513 uses the encoding 2^x*3*y.",
				"Cf. A038566/A038567, A157807/A157813."
			],
			"keyword": "nonn",
			"offset": "0,9",
			"author": "_Peter Luschny_, Sep 22 2018",
			"references": 8,
			"revision": 29,
			"time": "2018-09-24T09:25:03-04:00",
			"created": "2018-09-22T05:33:51-04:00"
		}
	]
}