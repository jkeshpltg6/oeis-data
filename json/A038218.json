{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A038218",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 38218,
			"data": "1,2,12,4,48,144,8,144,864,1728,16,384,3456,13824,20736,32,960,11520,69120,207360,248832,64,2304,34560,276480,1244160,2985984,2985984,128,5376,96768,967680,5806080,20901888,41803776,35831808",
			"name": "Triangle whose (i,j)-th entry is binomial(i,j)*2^(i-j)*12^j (with i, j \u003e= 0).",
			"comment": [
				"Using the transfer matrix method, Cyvin et al. (1996) derive the equation a(x,y)_{i,j} = binomial(i-1, j-1) * x^{i-j} * y^{j-1}. See Eq. (4) on p. 111 of the paper. If we replace i-1 with i, j-1 with j, x with 2, and y with 12, we get the current triangular array. - _Petros Hadjicostas_, Jul 23 2019"
			],
			"link": [
				"B. N. Cyvin, J. Brunvoll, and S. J. Cyvin, \u003ca href=\"http://match.pmf.kg.ac.rs/electronic_versions/Match34/match34_109-121.pdf\"\u003eIsomer enumeration of unbranched catacondensed polygonal systems with pentagons and heptagons\u003c/a\u003e, Match, No. 34 (Oct 1996), pp. 109-121.",
				"Gábor Kallós, \u003ca href=\"http://www.numdam.org/article/AMBP_2006__13_1_1_0.pdf\"\u003eA generalization of Pascal's triangle using powers of base numbers\u003c/a\u003e, Ann. Math. Blaise Pascal 13(1) (2006), 1-15. [See Section 2 of the paper with title \"ab-based triangles\". Apparently, this is a 2(12)-based triangle; i.e., a = 2 and b = 12 even though b = 12 \u003e 9. - _Petros Hadjicostas_, Jul 30 2019]"
			],
			"formula": [
				"From _Petros Hadjicostas_, Jul 23 2019: (Start)",
				"Bivariate g.f.: Sum_{i,j \u003e= 0} T(i,j)*x^i*y^j = 1/(1 - 2*x * (1 + 6*y)).",
				"G.f. for row i \u003e= 0: 2^i * (1 + 6*y)^i.",
				"G.f. for column j \u003e= 0: (12*x)^j/(1 - 2*x)^(j+1).",
				"(End)"
			],
			"example": [
				"From _Petros Hadjicostas_, Jul 23 2019: (Start)",
				"Triangle T(i,j) (with rows i \u003e= 0 and columns j \u003e= 0) begins as follows:",
				"    1;",
				"    2,   12;",
				"    4,   48,   144;",
				"    8,  144,   864,   1728;",
				"   16,  384,  3456,  13824,   20736;",
				"   32,  960, 11520,  69120,  207360,   248832;",
				"   64, 2304, 34560, 276480, 1244160,  2985984, 2985984;",
				"  128, 5376, 96768, 967680, 5806080, 20901888, 41803776, 35831808;",
				"  ... (End)"
			],
			"mathematica": [
				"Flatten[Table[Binomial[i, j] 2^(i - j) 12^j, {i, 0, 8}, {j, 0, i}]] (* _Vincenzo Librandi_, Jul 24 2019 *)"
			],
			"program": [
				"(MAGMA) /* As triangle */ [[Binomial(i,j)*2^(i-j)*12^j: j in [0..i]]: i in [0.. 15]]; // _Vincenzo Librandi_, Jul 24 2019"
			],
			"xref": [
				"Cf. A001021 (main diagonal), A001023 (row sums)."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_.",
			"ext": [
				"Name edited by _Petros Hadjicostas_, Jul 23 2019"
			],
			"references": 0,
			"revision": 37,
			"time": "2019-07-31T03:52:54-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}