{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A317414",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 317414,
			"data": "0,2,4,8,1,3,2,531440,1,1,3,1,8,4,2,22528399544939174411840147874772640,1,1,4,8,1,3,1,1,531440,2,3,1,8,4,2",
			"name": "Continued fraction for ternary expansion of Liouville's number interpreted in base 3 (A012245).",
			"comment": [
				"The continued fraction of the number obtained by reading A012245 as a ternary fraction.",
				"Except for the first term, the only values that occur in this sequence are 1,2,3,4, and values 3^((m-1)*m!)-1 for m \u003e 1. The probability of occurrence P(a(n) = k) are given by:",
				"P(a(n) = 1) = 3/8,",
				"P(a(n) = 2) = 1/8,",
				"P(a(n) = 3) = 1/8,",
				"P(a(n) = 4) = 1/8 and",
				"P(a(n) = 3^((m-1)*m!)-1) = 2^-(m+1) for m \u003e 1.",
				"More generally it seems that for any base \u003e 2, P(a(n) \u003c= base+1) = 3/4, P(a(n) \u003e base+1) = 1/4, and P(a(n) = base^((m-1)*m!)-1) = 2^-(m+1) for m \u003e 1."
			],
			"link": [
				"A.H.M. Smeets, \u003ca href=\"/A317414/b317414.txt\"\u003eTable of n, a(n) for n = 0..62\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 1 if and only if n in {floor(8*n/3) + A317627(n) | n \u003e 0}.",
				"a(n) = 2 if and only if n in {8*n - 10 + 3*A089013(n-1) | n \u003e 0}.",
				"a(n) = 3 if and only if n in {16*n - 11 | n \u003e 0} union {16*n - 6 | n \u003e 0}.",
				"a(n) = 4 if and only if n in {16*n - 14 | n \u003e 0} union {16*n - 3 | n \u003e 0}.",
				"a(n) = 3^((m-1)*m!)-1 iff n in {2^m*(1+k*4) - 1 | k \u003e= 0} union {2^m*(3+k*4) | k \u003e= 0} for m \u003e 1."
			],
			"maple": [
				"with(numtheory): cfrac(add(1/3^factorial(n),n=1..7),30,'quotients'); # _Muniru A Asiru_, Aug 11 2018"
			],
			"mathematica": [
				"ContinuedFraction[ FromDigits[ RealDigits[ Sum[1/10^n!, {n, 8}], 10, 10000], 3], 60] (* _Robert G. Wilson v_, Aug 09 2018 *)"
			],
			"program": [
				"(Python)",
				"n,f,i,p,q,base = 1,1,0,0,1,3",
				"while i \u003c 100000:",
				"....i,p,q = i+1,p*base,q*base",
				"....if i == f:",
				"........p,n = p+1,n+1",
				"........f = f*n",
				"n,a,j = 0,0,0",
				"while p%q \u003e 0:",
				"....a,f,p,q = a+1,p//q,q,p%q",
				"....print(a-1,f)"
			],
			"xref": [
				"Cf. A012245, A089013, A317627.",
				"Cf. A058304 (in base 10), A317413 (in base 2), A317661 (in base 4)."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_A.H.M. Smeets_, Jul 27 2018",
			"references": 6,
			"revision": 29,
			"time": "2020-02-22T20:54:24-05:00",
			"created": "2018-09-10T04:59:25-04:00"
		}
	]
}