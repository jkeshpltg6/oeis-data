{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A319443",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 319443,
			"data": "0,1,1,1,1,2,2,1,1,2,1,2,2,3,2,1,1,2,2,2,3,2,1,2,1,3,1,3,1,3,2,1,2,2,3,2,2,3,3,2,1,4,2,2,2,2,1,2,2,2,2,3,1,2,2,3,3,2,1,3,2,3,3,1,3,3,2,2,2,4,1,2,2,3,2,3,3,4,2,2,1,2,1,4,2,3,2",
			"name": "Number of distinct Eisenstein primes in the factorization of n.",
			"comment": [
				"Equivalent of omega (A001221) in the ring of Eisenstein integers.",
				"z is an Eisenstein prime iff z has prime norm or z is the product of a rational prime congruent to 2 modulo 3 and an Eisenstein unit (one of +-1 or (+-1 +- sqrt(3)*i)/2).",
				"Associated Eisenstein prime divisors are counted only once.",
				"Let s(n) be the smallest k with a(k) = n, then we have: s(0) = 1, s(1) = 2, s(2) = 6, s(2n-1) = 2*A121940(n-1), s(2n) = 6*A121940(n-1)."
			],
			"link": [
				"Jianing Song, \u003ca href=\"/A319443/b319443.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Eisenstein_integer\"\u003eEisenstein integer\u003c/a\u003e"
			],
			"formula": [
				"Additive with a(p^e) = 2 if p == 1 (mod 3), 1 otherwise."
			],
			"example": [
				"Let w = (1 + sqrt(3)*i)/2, w' = (1 - sqrt(3)*i)/2.",
				"Over the Gaussian integers, 5187 = 3*7*13*19 is factored as w'*(1 + w)^2*(2 + w)*(2 + w')*(3 + w)*(3 + w')*(3 + 2w)*(3 + 2w'), the distinct Eisenstein prime factors are 1 + w, 2 + w, 2 + w', 3 + w, 3 + w', 3 + 2w and 3 + 2w', so a(5187) = 7.",
				"Over the Gaussian integers, 1006655265000 = 2^3*3^2*5^4*7^5*11^3 is factored as w'^2*(1 + w)^4*2^3*(2 + w)*(2 + w')*5^4*11^3, the distinct Eisenstein prime factors are 1 + w, 2, 2 + w, 2 + w', 5 and 11, so a(1006655265000) = 6."
			],
			"mathematica": [
				"f[p_, e_] := If[Mod[p, 3] == 1, 2, 1]; eisOmega[1] = 0; eisOmega[n_] := Plus @@ f @@@ FactorInteger[n]; Array[eisOmega, 100] (* _Amiram Eldar_, Feb 10 2020 *)"
			],
			"program": [
				"(PARI) a(n)=my(f=factor(n)[, 1]); sum(i=1, #f, if(f[i]%3==1, 2, 1))"
			],
			"xref": [
				"Cf. A121940.",
				"Equivalent of arithmetic functions in the ring of Eisenstein integers (the corresponding functions in the ring of integers are in the parentheses): A319442 (\"d\", A000005), A319449 (\"sigma\", A000203), A319445 (\"phi\", A000010), A319446 (\"psi\", A002322), this sequence (\"omega\", A001221), A319444 (\"Omega\", A001222), A319448 (\"mu\", A008683).",
				"Equivalent in the ring of Gaussian integers: A086275."
			],
			"keyword": "nonn",
			"offset": "1,6",
			"author": "_Jianing Song_, Sep 19 2018",
			"references": 7,
			"revision": 19,
			"time": "2020-02-10T17:40:35-05:00",
			"created": "2018-09-24T08:50:28-04:00"
		}
	]
}