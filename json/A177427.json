{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A177427",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 177427,
			"data": "1,1,13,7,149,157,383,199,7409,7633,86231,88331,1173713,1197473,1219781,620401,42862943,43503583,279379879,283055551,57313183,19328341,449489867,1362695813,34409471059,34738962067,315510823603,45467560829,9307359944587,9382319148907,293103346860157,147643434162641,594812856101039,54448301591149",
			"name": "Numerators of the Inverse Akiyama-Tanigawa transform of the aerated even-indexed Bernoulli numbers 1, 0, 1/6, 0, -1/30, 0, 1/42,...",
			"comment": [
				"These are the numerators of the first row of a Table T(n,k) which contains the even-indexed Bernoulli numbers in the first column: T(2n,0) = A000367(n)/A002445(n), T(2n+1,0)=0, and which generates rows with the Akiyama-Tanigawa transform. (Because the first column is given, the algorithm is an inverse Akiyama-Tanigawa transform.)",
				"These are the absolute values of the numerators of the Taylor expansion of sinh(log(x+1))*log(x+1)at x=0. - _Gary Detlefs_, Aug 31 2011"
			],
			"link": [
				"L. A. Medina, V. H. Moll, E. S. Rowland, \u003ca href=\"http://arxiv.org/abs/0911.1325\"\u003eIterated primitives of logarithmic powers\u003c/a\u003e, arXiv:0911.1325, arXiv:0911.1325 [math.NT], 2009-2010.",
				"D. Merlini, R. Sprugnoli, M. C. Verri, \u003ca href=\"http://www.emis.de/journals/INTEGERS/papers/f5/f5.Abstract.html\"\u003eThe Akiyama-Tanigawa Transformation\u003c/a\u003e, Integers, 5 (1) (2005) #A05."
			],
			"formula": [
				"T(0,k) = H(k)/2+1/(k+1) with H(k) harmonic number of order k. - Roland Groux, Jan 07 2011",
				"T(0,k)= -(1/2)*(k+1)*int(x^n*log(x*(1-x)),x=0..1). - Roland Groux, Jan 07 2011",
				"G.f.: sum_{k\u003e=0} T(0,k) x^k = (x-2)*(log(1-x))/(2*x*(1-x)). - Roland Groux, Jan 07 2011",
				"T(1,n) = -A191567(n)/A061038(n+2) = -A060819(n)/A145979(n). - _Paul Curtz_, Jul 19 2011",
				"(T(1,n))^2 = A181318(n)/A061038(n+2). - _Paul Curtz_, Jul 19 2011, index corrected by _R. J. Mathar_, Sep 09 2011"
			],
			"example": [
				"The table T(n,k) of fractions generated by the Akiyama-Tanigawa transform, with the column T(n,0) equal to bernoulli(n) for even n and equal to 0 for odd n, starts in row n=0 as:",
				"1, 1, 13/12, 7/6, 149/120, 157/120, 383/280, 199/140,...",
				"0, -1/6, -1/4, -3/10, -1/3, -5/14, -3/8, -7/18, -2/5, -9/22,...",
				"1/6, 1/6, 3/20, 2/15, 5/42, 3/28, 7/72, 4/45, 9/110, 5/66,..",
				"0, 1/30, 1/20, 2/35, 5/84, 5/84, 7/120, 28/495, 3/55, 15/286, ...",
				"-1/30, -1/30, -3/140, -1/105, 0, 1/140, 49/3960, 8/495,..",
				"0, -1/42, -1/28, -4/105, -1/28, -29/924, -7/264, -28/1287, -87/5005, ...",
				"1/42, 1/42, 1/140, -1/105, -5/231, -9/308, -343/10296, -1576/45045, ..."
			],
			"mathematica": [
				"t[n_, 0] := BernoulliB[n]; t[1, 0]=0; t[n_, k_] := t[n, k] = (t[n, k-1] + (k-1)*t[n, k-1] - t[n+1, k-1])/k; Table[t[0, k], {k, 0, 33}] // Numerator (* _Jean-François Alcover_, Aug 09 2012 *)"
			],
			"xref": [
				"Cf. A177690 (denominators)."
			],
			"keyword": "nonn,frac",
			"offset": "0,3",
			"author": "_Paul Curtz_, May 07 2010",
			"references": 6,
			"revision": 29,
			"time": "2015-06-23T23:02:55-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}