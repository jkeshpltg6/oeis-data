{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A266701",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 266701,
			"data": "9,11,5,41,81,239,599,1595,4149,10889,28481,74591,195255,511211,1338341,3503849,9173169,24015695,62873879,164605979,430944021,1128226121,2953734305,7732976831,20245196151,53002611659,138762638789,363285304745,951093275409",
			"name": "Coefficient of x^2 in minimal polynomial of the continued fraction [1^n,1/3,1,1,1,...], where 1^n means n ones.",
			"comment": [
				"See A265762 for a guide to related sequences."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A266701/b266701.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,2,-1)."
			],
			"formula": [
				"a(n) = 2*a(n-1) - 2*a(n-2) + a(n-3).",
				"G.f.:  (9 - 7 x - 35 x^2 + 18 x^3)/(1 - 2 x - 2 x^2 + x^3).",
				"a(n) = (2^(-n)*(-37*(-2)^n-2*(3-sqrt(5))^n*(2+3*sqrt(5))+(3+sqrt(5))^n*(-4+6*sqrt(5))))/5. - _Colin Barker_, Sep 29 2016"
			],
			"example": [
				"Let p(n,x) be the minimal polynomial of the number given by the n-th continued fraction:",
				"[1/3,1,1,1,...] = (-1 + 3 sqrt(5))/6 has p(0,x) = -11 + 3 x + 9 x^2, so a(0) = 9;",
				"[1,1/3,1,1,...] = (25 + 9 sqrt(5))/22 has p(1,x) = 5 - 25 x + 11 x^2, so a(1) = 11;",
				"[1,1,1/3,1,...] = (35 - 9 sqrt(5))/10 has p(2,x) = 41 - 35 x + 5 x^2, so a(2) = 5."
			],
			"mathematica": [
				"u[n_] := Table[1, {k, 1, n}]; t[n_] := Join[u[n], {1/3}, {{1}}];",
				"f[n_] := FromContinuedFraction[t[n]];",
				"t = Table[MinimalPolynomial[f[n], x], {n, 0, 20}]",
				"Coefficient[t, x, 0] (* A266701 *)",
				"Coefficient[t, x, 1] (* A266702 *)",
				"Coefficient[t, x, 2] (* A266701 *)"
			],
			"program": [
				"(PARI) a(n) = round((2^(-n)*(-37*(-2)^n-2*(3-sqrt(5))^n*(2+3*sqrt(5))+(3+sqrt(5))^n*(-4+6*sqrt(5))))/5) \\\\ _Colin Barker_, Sep 29 2016",
				"(PARI) Vec((9-7*x-35*x^2+18*x^3)/((1+x)*(1-3*x+x^2)) + O(x^30)) \\\\ _Colin Barker_, Sep 29 2016"
			],
			"xref": [
				"Cf. A265762, A266702."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_Clark Kimberling_, Jan 09 2016",
			"ext": [
				"Three typos in data fixed by _Colin Barker_, Sep 29 2016"
			],
			"references": 3,
			"revision": 13,
			"time": "2016-09-29T16:47:37-04:00",
			"created": "2016-01-09T19:57:23-05:00"
		}
	]
}