{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A185164",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 185164,
			"data": "1,1,2,3,6,10,24,40,15,120,196,105,720,1148,700,105,5040,7848,5068,1260,40320,61416,40740,12600,945,362880,541728,363660,126280,17325,3628800,5319072,3584856,1332100,242550,10395,39916800,57545280,38764440,15020720,3213210,270270",
			"name": "Coefficients of a set of polynomials associated with the derivatives of x^x.",
			"comment": [
				"Gould shows that the derivatives of x^x are given by (d/dx)^n(x^x) = x^x*sum {k = 0..n} (-1)^k*binomial(n,k)*(1+log(x))^(n-k)*x^(-k)* R(k,x), where R(n,x) is a polynomial in x of degree floor(n/2). The first few values are R(0,x) = 1, R(1,x) = 0, R(2,x) = x, R(3,x) = x, R(4,x) = 2*x+3*x^2. The coefficients of these polynomials are listed in the table for n \u003e= 2. Gould gives an explicit formula for R(n,x) as a triple sum and also an expression in terms of the Comtet numbers A008296.",
				"This table read by diagonals gives A075856."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A185164/b185164.txt\"\u003eTable of n, a(n) for n = 2..10001\u003c/a\u003e (rows 2 to 200, flattened)",
				"Peter Bala, \u003ca href=\"/A112007/a112007.txt_Bala\"\u003eDiagonals of triangles with generating function exp(t*F(x)).\u003c/a\u003e",
				"H. W. Gould, \u003ca href=\"http://dx.doi.org/10.1216/rmjm/1181072076\"\u003eA Set of Polynomials Associated with the Higher Derivatives of y = x^x\u003c/a\u003e, Rocky Mountain J. Math. Volume 26, Number 2 (1996), 615-625."
			],
			"formula": [
				"Recurrence relation: T(n+1,k) = (n-k)*T(n,k) + n*T(n-1,k-1).",
				"The diagonal entries D(n,k) := T(n+k,k) satisfy the recurrence D(n+1,k) = n*D(n,k) + (n+k)*D(n,k-1) so this table read by diagonals is A075856.",
				"E.g.f.:  F(x,t) = exp(t*(x + (1-x)*log(1-x))) = sum {n = 0..inf} R(n,t)*x^n/n! = 1 + t*x^2/2! + t*x^3/3! + (2*t+3*t^2)*x^4/4! + .... The e.g.f. F(x,t) satisfies the partial differential equation (1-x)*dF/dx + t*dF/dt = x*t*F.",
				"This gives the recurrence relation for the row generating polynomials: R(n+1,x) = n*R(n,x) - x*d/dx(R(n,x)) + n*x*R(n-1,x) for n \u003e= 1, with initial conditions R(0,x) = 1, R(1,x) = 0.",
				"The e.g.f. for the triangle read by diagonals is given by the series reversion (x - t*(x + (1-x)*log(1-x)))^(-1) = x + t*x^2/2! + (t+3*t^2)x^3/3! + (2*t+10*t^2+15*t^3)*x^4/4! + ....",
				"Diagonal sums: sum {k = 1..n} T(n+k,k) = n^n , n \u003e= 1.",
				"Row sums A203852.",
				"Also the Bell transform of the sequence g(k) = (k-1)! if k\u003e0 else 0. For the definition of the Bell transform see A264428. - _Peter Luschny_, Jan 13 2016"
			],
			"example": [
				"Triangle begins",
				"n\\k.|.....1.....2.....3.....4",
				"= = = = = = = = = = = = = = =",
				"..2.|.....1",
				"..3.|.....1",
				"..4.|.....2.....3",
				"..5.|.....6....10",
				"..6.|....24....40....15",
				"..7.|...120...196...105",
				"..8.|...720..1148...700...105",
				"..9.|..5040..7848..5068..1260",
				"...",
				"Fourth derivative of x^x:",
				"x^(-x)*(d/dx)^4(x^x) = (1+log(x))^4 + C(4,2)/x^2*(1+log(x))^2*x - C(4,3)/x^3*(1+log(x)) + C(4,4)/x^4*(2*x+3*x^2).",
				"Example of recurrence relation for table entries:",
				"T(7,2) = 4*T(6,2) + 6*T(5,1) = 4*40 + 6*6 = 196."
			],
			"maple": [
				"T[2,1]:= 1:",
				"for n from 3 to 15 do",
				"  for k from 1 to floor(n/2) do",
				"    T[n,k]:= (n-1-k)*`if`(k\u003c= floor((n-1)/2),T[n-1,k],0) + `if`(k\u003e=2 and k-1 \u003c= floor((n-2)/2),(n-1)*T[n-2,k-1],0)",
				"od od:",
				"seq(seq(T[n,k],k=1..floor(n/2)),n=2..15); # _Robert Israel_, Jan 13 2016"
			],
			"mathematica": [
				"m = 14; F = Exp[t (x + (1-x) Log[1-x])];",
				"cc = CoefficientList[# + O[t]^m, t]\u0026 /@ CoefficientList[F + O[x]^m, x]* Range[0, m - 1]!;",
				"Rest /@ Drop[cc, 2] (* _Jean-François Alcover_, Jun 26 2019 *)"
			],
			"program": [
				"(Sage) # uses[bell_transform from A264428]",
				"# Computes the full triangle for n\u003e=0 and 0\u003c=k\u003c=n.",
				"def A185164_row(n):",
				"    g = lambda k: factorial(k-1) if k\u003e0 else 0",
				"    s = [g(k) for k in (0..n)]",
				"    return bell_transform(n, s)",
				"[A185164_row(n) for n in (0..10)] # _Peter Luschny_, Jan 13 2016"
			],
			"xref": [
				"Cf. A008296, A075856, A203852 (row sums)."
			],
			"keyword": "nonn,easy,tabf",
			"offset": "2,3",
			"author": "_Peter Bala_, Mar 12 2012",
			"ext": [
				"More terms from _Jean-François Alcover_, Jun 26 2019"
			],
			"references": 5,
			"revision": 40,
			"time": "2020-03-28T16:30:26-04:00",
			"created": "2012-03-13T14:21:50-04:00"
		}
	]
}