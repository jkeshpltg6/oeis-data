{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A280275",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 280275,
			"data": "1,1,2,5,12,37,118,387,1312,4445,17034,73339,342532,1616721,7299100,31195418,129179184,578924785,3057167242,18723356715,120613872016,738703713245,4080301444740,20353638923275,95273007634552,443132388701107,2149933834972928",
			"name": "Number of set partitions of [n] where sizes of distinct blocks are coprime.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A280275/b280275.txt\"\u003eTable of n, a(n) for n = 0..500\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Coprime_integers\"\u003eCoprime integers\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Partition_of_a_set\"\u003ePartition of a set\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{k=0..n} A280880(n,k)."
			],
			"example": [
				"a(n) = A000110(n) for n\u003c=3.",
				"a(4) = 12: 1234, 123|4, 124|3, 12|3|4, 134|2, 13|2|4, 1|234, 1|23|4, 14|2|3, 1|24|3, 1|2|34, 1|2|3|4.",
				"a(5) = 37: 12345, 1234|5, 1235|4, 123|45, 123|4|5, 1245|3, 124|35, 124|3|5, 125|34, 12|345, 125|3|4, 12|3|4|5, 1345|2, 134|25, 134|2|5, 135|24, 13|245, 135|2|4, 13|2|4|5, 145|23, 14|235, 15|234, 1|2345, 1|234|5, 1|235|4, 1|23|4|5, 145|2|3, 14|2|3|5, 1|245|3, 1|24|3|5, 1|2|345, 1|2|34|5, 15|2|3|4, 1|25|3|4, 1|2|35|4, 1|2|3|45, 1|2|3|4|5."
			],
			"maple": [
				"with(numtheory):",
				"b:= proc(n, i, s) option remember;",
				"      `if`(n=0 or i=1, 1, b(n, i-1, select(x-\u003ex\u003c=i-1, s))+",
				"      `if`(i\u003en or factorset(i) intersect s\u003c\u003e{}, 0, b(n-i, i-1,",
				"      select(x-\u003ex\u003c=i-1, s union factorset(i)))*binomial(n, i)))",
				"    end:",
				"a:= n-\u003e b(n$2, {}):",
				"seq(a(n), n=0..30);"
			],
			"mathematica": [
				"b[n_, i_, s_] := b[n, i, s] = Expand[If[n==0 || i==1, x^n, b[n, i-1, Select[s, # \u003c= i-1\u0026]] + If[i\u003en || FactorInteger[i][[All, 1]] ~Intersection~ s != {}, 0, x*b[n-i, i-1, Select[s ~Union~ FactorInteger[i][[All, 1]], # \u003c= i-1\u0026]]*Binomial[n, i]]]];",
				"a[n_] := b[n, n, {}] // CoefficientList[#, x]\u0026 // Total;",
				"Table[a[n], {n, 0, 30}] (* _Jean-François Alcover_, Mar 23 2017, translated from Maple *)"
			],
			"xref": [
				"Cf. A000110, A007837, A275313.",
				"Row sums of A280880."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Alois P. Heinz_, Dec 30 2016",
			"references": 3,
			"revision": 21,
			"time": "2017-03-23T04:15:51-04:00",
			"created": "2016-12-31T06:52:09-05:00"
		}
	]
}