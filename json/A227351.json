{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A227351",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 227351,
			"data": "0,1,3,7,2,15,6,4,31,14,12,8,5,63,30,28,24,13,16,9,11,127,62,60,56,29,48,25,27,32,17,19,23,10,255,126,124,120,61,112,57,59,96,49,51,55,26,64,33,35,39,18,47,22,20,511,254,252,248,125,240,121,123,224",
			"name": "Permutation of nonnegative integers: map each number by lengths of runs of zeros in its Zeckendorf expansion shifted once left to the number which has the same lengths of runs (in the same order, but alternatively of runs of 0's and 1's) in its binary representation.",
			"comment": [
				"This permutation is based on the fact that by appending one extra zero to the right of Fibonacci number representation of n (aka \"Zeckendorf expansion\") and then counting the lengths of blocks of consecutive (nonleading) zeros we get bijective correspondence with compositions, and thus also with the binary representation of a unique n. See the chart below:",
				"   n   A014417(n) A014417(A022342(n+1)) Runs of    Binary number   In dec.",
				"                  [shifted once left]   zeros      with same runs  = a(n)",
				"   0:   ......0              ......0    []         .....0           0",
				"   1:   ......1              .....10    [1]        .....1           1",
				"   2:   .....10              ....100    [2]        ....11           3",
				"   3:   ....100              ...1000    [3]        ...111           7",
				"   4:   ....101              ...1010    [1,1]      ....10           2",
				"   5:   ...1000              ..10000    [4]        ..1111          15",
				"   6:   ...1001              ..10010    [2,1]      ...110           6",
				"   7:   ...1010              ..10100    [1,2]      ...100           4",
				"   8:   ..10000              .100000    [5]        .11111          31",
				"   9:   ..10001              .100010    [3,1]      ..1110          14",
				"  10:   ..10010              .100100    [2,2]      ..1100          12",
				"  11:   ..10100              .101000    [1,3]      ..1000           8",
				"  12:   ..10101              .101010    [1,1,1]    ...101           5",
				"  13:   .100000              1000000    [6]        111111          63",
				"Are there any other fixed points after 0, 1, 6, 803, 407483 ?"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A227351/b227351.txt\"\u003eTable of n, a(n) for n = 0..10946\u003c/a\u003e",
				"OEIS Wiki, \u003ca href=\"https://oeis.org/wiki/Run-length_encoding\"\u003eRun-length encoding\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A006068(A048679(n)) = A006068(A106151(2*A003714(n))).",
				"This permutation effects following correspondences:",
				"a(A000045(n)) = A000225(n-1).",
				"a(A027941(n)) = A000975(n).",
				"For n \u003e=3, a(A000204(n)) = A000079(n-2)."
			],
			"program": [
				"(Scheme): (define (A227351 n) (A006068 (A048679 n)))",
				"(define (A227351v2 n) (A006068 (A106151 (* 2 (A003714 n))))) ;; Alternative definition."
			],
			"xref": [
				"Inverse permutation: A227352. Cf. also A003714, A014417, A006068, A048679.",
				"Could be further composed with A075157 or A075159, also A129594."
			],
			"keyword": "nonn,base",
			"offset": "0,3",
			"author": "_Antti Karttunen_, Jul 08 2013",
			"references": 3,
			"revision": 30,
			"time": "2013-07-29T04:59:42-04:00",
			"created": "2013-07-29T04:59:42-04:00"
		}
	]
}