{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A048793",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 48793,
			"data": "0,1,2,1,2,3,1,3,2,3,1,2,3,4,1,4,2,4,1,2,4,3,4,1,3,4,2,3,4,1,2,3,4,5,1,5,2,5,1,2,5,3,5,1,3,5,2,3,5,1,2,3,5,4,5,1,4,5,2,4,5,1,2,4,5,3,4,5,1,3,4,5,2,3,4,5,1,2,3,4,5,6,1,6,2,6,1,2,6,3,6,1,3,6,2,3,6,1,2,3,6,4,6,1,4",
			"name": "List giving all subsets of natural numbers arranged in standard statistical (or Yates) order.",
			"comment": [
				"For n\u003e0: first occurrence of n in row 2^(n-1), and when the table is seen as a flattened list at position n*2^(n-1)+1, cf. A005183. - _Reinhard Zumkeller_, Nov 16 2013",
				"Row n lists the positions of 1's in the reversed binary expansion of n. Compare to triangles A112798 and A213925. - _Gus Wiseman_, Jul 22 2019"
			],
			"reference": [
				"S. Hedayat, N. J. A. Sloane and J. Stufken, Orthogonal Arrays, Springer-Verlag, NY, 1999, p. 249."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A048793/b048793.txt\"\u003eRows n = 0..1000 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"Constructed recursively: subsets that include n are obtained by appending n to all earlier subsets."
			],
			"example": [
				"From _Gus Wiseman_, Jul 22 2019: (Start)",
				"Triangle begins:",
				"  {}",
				"  1",
				"  2",
				"  1  2",
				"  3",
				"  1  3",
				"  2  3",
				"  1  2  3",
				"  4",
				"  1  4",
				"  2  4",
				"  1  2  4",
				"  3  4",
				"  1  3  4",
				"  2  3  4",
				"  1  2  3  4",
				"  5",
				"  1  5",
				"  2  5",
				"  1  2  5",
				"  3  5",
				"(End)"
			],
			"maple": [
				"T:= proc(n) local i, l, m; l:= NULL; m:= n;",
				"      if n=0 then return 0 fi; for i while m\u003e0 do",
				"      if irem(m, 2, 'm')=1 then l:=l, i fi od; l",
				"    end:",
				"seq(T(n), n=1..50);  # _Alois P. Heinz_, Sep 06 2014"
			],
			"mathematica": [
				"s[0] = {{}}; s[n_] := s[n] = Join[s[n - 1], Append[#, n]\u0026 /@ s[n - 1]]; Join[{0}, Flatten[s[6]]] (* _Jean-François Alcover_, May 24 2012 *)",
				"Table[Join@@Position[Reverse[IntegerDigits[n,2]],1],{n,30}] (* _Gus Wiseman_, Jul 22 2019 *)"
			],
			"program": [
				"(C)",
				"#include \u003cstdio.h\u003e",
				"#include \u003cstdlib.h\u003e",
				"#define USAGE \"Usage: 'A048793 num' where num is the largest number to use creating sets.\\n\"",
				"#define MAX_NUM 10",
				"#define MAX_ROW 1024",
				"int main(int argc, char *argv[]) {",
				"  unsigned short a[MAX_ROW][MAX_NUM]; signed short old_row, new_row, i, j, end;",
				"  if (argc \u003c 2) { fprintf(stderr, USAGE); return EXIT_FAILURE; }",
				"  end = atoi(argv[1]); end = (end \u003e MAX_NUM) ? MAX_NUM: end;",
				"  for (i = 0; i \u003c MAX_ROW; i++) for ( j = 0; j \u003c MAX_NUM; j++) a[i][j] = 0;",
				"  a[1][0] = 1; new_row = 2;",
				"  for (i = 2; i \u003c= end; i++) {",
				"    a[new_row++ ][0] = i;",
				"    for (old_row = 1; a[old_row][0] != i; old_row++) {",
				"      for (j = 0; a[old_row][j] != 0; j++) { a[new_row][j] = a[old_row][j]; }",
				"      a[new_row++ ][j] = i;",
				"    }",
				"  }",
				"  fprintf(stdout, \"Values: 0\");",
				"  for (i = 1; a[i][0] != 0; i++) for (j = 0; a[i][j] != 0; j++) fprintf(stdout, \",%d\", a[i][j]);",
				"  fprintf(stdout, \"\\n\"); return EXIT_SUCCESS",
				"}",
				"(Haskell)",
				"a048793 n k = a048793_tabf !! n !! k",
				"a048793_row n = a048793_tabf !! n",
				"a048793_tabf = [0] : [1] : f [[1]] where",
				"   f xss = yss ++ f (xss ++ yss) where",
				"     yss = [y] : map (++ [y]) xss",
				"     y = last (last xss) + 1",
				"-- _Reinhard Zumkeller_, Nov 16 2013"
			],
			"xref": [
				"Cf. A048794.",
				"Row lengths are A000120.",
				"First column is A001511.",
				"Row sums are A029931.",
				"Reversing rows gives A272020.",
				"Subtracting 1 from each term gives A133457; subtracting 1 and reversing rows gives A272011.",
				"Indices of relatively prime rows are A291166 (see also A326674); arithmetic progressions are A295235; rows with integer average are A326669 (see also A326699/A326700); pairwise coprime rows are A326675.",
				"Cf. A035327, A070939."
			],
			"keyword": "nonn,tabf,easy,nice",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from Larry Reeves (larryr(AT)acm.org), Apr 11 2000"
			],
			"references": 190,
			"revision": 27,
			"time": "2020-11-03T09:48:23-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}