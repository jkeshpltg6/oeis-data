{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A229194",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 229194,
			"data": "1,1,1,2,3,5,8,13,21,35,58,97,163,275,466,793,1353,2315,3969,6817,11726,20195,34816,60073,103724,179195,309724,535537,926275,1602515,2773034,4799353,8307516,14381675,24899377,43112257,74651790,129271235,223862687,387682633,671402698,1162785755,2013837368,3487832977,6040770648,10462450355,18120829034,31385253913,54359521280,94151567435,163072632198",
			"name": "Integers nearest to (2^((n-3)/2) + 3^((n-3)/2)).",
			"comment": [
				"This sequence illustrates the second law of small numbers because it is a coincidence that the terms for 1 \u003c= n \u003c= 8 are the same as the Fibonacci numbers F(n) (A000045): a(n) = F(n) for 1 \u003c= n \u003c= 8.",
				"Furthermore, the following terms are the sum of two Fibonacci numbers: a(9) = F(9) + F(2), a(10) = F(10) + F(4), a(11) = F(11) + F(6), a(14) = F(14) + F(11); or the algebraic sum of three Fibonacci numbers: a(12) = F(12) + F(8) - F(3), a(13) = F(13) + F(10) - F(7), a(14) = F(14) + F(12) - F(10), a(18) = F(19) - F(13) - F(8), a(19) = F(20) + F(10) - F(4); or the algebraic sum of four Fibonacci numbers: a(15) = F(15) + F(12) + F(9) + F(5), a(16) = F(16) + F(14) - F(6) - F(4), a(17) = F(18) - F(13) - F(9) - F(3), a(18) = F(18) + F(16) + F(14) + F(8), a(19) = F(19) + F(18) + F(10) - F(3).",
				"Note that, for following values of n, a(n) \u003e F(n+1) for n \u003e= 20.",
				"Remark as well that (2^(1/2) + 3^(1/2)) = 3.14626437... ~=  Pi (see A135611)."
			],
			"reference": [
				"T. Koshy, Fibonacci and Lucas Numbers with Applications, New York, Wiley-Interscience, 2001",
				"I. Stewart, L'univers des nombres, Belin-Pour La Science, Paris 2000."
			],
			"link": [
				"Vladimir Pletser, \u003ca href=\"/A229194/b229194.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"R. K. Guy, \u003ca href=\"http://www.jstor.org/stable/2691503\"\u003eThe Second Strong Law of Small Numbers\u003c/a\u003e, Math. Mag, 63 (1990), no. 1, 3-20.",
				"I. Stewart, \u003ca href=\"http://www.whydomath.org/Reading_Room_Material/ian_stewart/9505.html\"\u003eFibonacci Forgeries\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/FibonacciNumber.html\"\u003eFibonacci Number.\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/StrongLawofSmallNumbers.html\"\u003eStrong Law of Small Numbers.\u003c/a\u003e"
			],
			"formula": [
				"a(n) = round(2^((n-3)/2) + 3^((n-3)/2))."
			],
			"maple": [
				"seq(round(2^((n-3)/2)+3^((n-3)/2)), n=0..50);"
			],
			"mathematica": [
				"Table[Round[2^((n - 3)/2) + 3^((n - 3)/2)], {n, 0, 50}] (* _Vincenzo Librandi_, Sep 20 2013 *)"
			],
			"program": [
				"(MAGMA) [Round(2^((n-3)/2) + 3^((n-3)/2)): n in [0..50]]; // _Vincenzo Librandi_, Sep 20 2013"
			],
			"xref": [
				"Cf. A000045, A005181."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Vladimir Pletser_, Sep 15 2013",
			"references": 3,
			"revision": 22,
			"time": "2017-09-20T02:51:57-04:00",
			"created": "2013-09-20T04:26:29-04:00"
		}
	]
}