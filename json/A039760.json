{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A039760",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 39760,
			"data": "1,0,1,1,2,1,1,7,6,1,1,24,34,12,1,1,81,190,110,20,1,1,268,1051,920,275,30,1,1,869,5747,7371,3255,581,42,1,1,2768,31060,57568,35686,9296,1092,56,1,1,8689,166068,441652,373926,134022,22764,1884,72,1",
			"name": "Triangle of D-analogs of Stirling numbers of the 2nd kind.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A039760/b039760.txt\"\u003eRows n=0..100 of triangle, flattened\u003c/a\u003e",
				"Eli Bagno, Riccardo Biagioli, and David Garber, \u003ca href=\"https://arxiv.org/abs/1901.07830\"\u003eSome identities involving second kind Stirling numbers of types B and D\u003c/a\u003e, arXiv:1901.07830 [math.CO], 2019.",
				"Ruedi Suter, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL3/SUTER/sut1.html\"\u003eTwo analogues of a classical sequence\u003c/a\u003e, J. Integer Sequences, Vol. 3 (2000), #P00.1.8."
			],
			"formula": [
				"Bivariate e.g.f.-o.g.f.: (exp(x) - x)*exp(y/2*(exp(2*x) - 1)). [See Theorem 4 in Suter (2000).]",
				"T(n,k) = Sum_{j=k..n} 2^(j-k)*binomial(n,j)*Stirling2(j,k) - 2^(n-1-k)*n*Stirling2(n-1,k). [See Proposition 3 in Suter (2000).] - _Petros Hadjicostas_, Jul 11 2020"
			],
			"example": [
				"Triangle T(n,k) (with rows n \u003e= 0 and columns k=0..n) begins:",
				"  1;",
				"  0,   1;",
				"  1,   2,    1;",
				"  1,   7,    6,   1;",
				"  1,  24,   34,  12,   1;",
				"  1,  81,  190, 110,  20,  1;",
				"  1, 268, 1051, 920, 275, 30, 1;",
				"  ..."
			],
			"mathematica": [
				"With[{m = 10}, CoefficientList[CoefficientList[Series[(Exp[x]-x)* Exp[y/2*(Exp[2*x]-1)], {y, 0, m}, {x, 0, m}], x], y]*(Range[0, m]!)] (* _G. C. Greubel_, Mar 07 2019 *)"
			],
			"program": [
				"(PARI) T(n, k)=if(k\u003c0||k\u003en, 0, n!*polcoeff(polcoeff((exp(x)-x)*exp(y/2*(exp(2*x)-1)), n), k));",
				"tabl(nn) = {x = 'x + O('x^nn); for (n=0, nn, for (m=0, n, print1(T(n, m), \", \");); print(););} \\\\ _Michel Marcus_, May 03 2015"
			],
			"xref": [
				"Cf. A039761 (transposed triangle)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "Ruedi Suter (suter(AT)math.ethz.ch)",
			"references": 3,
			"revision": 32,
			"time": "2020-07-13T02:18:58-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}