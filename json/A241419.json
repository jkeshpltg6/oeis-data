{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A241419",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 241419,
			"data": "0,1,2,1,2,3,4,4,2,3,4,4,5,6,7,7,8,8,9,10,11,12,13,13,9,10,10,11,12,12,13,13,14,15,16,16,17,18,19,19,20,21,22,23,23,24,25,25,19,19,20,21,22,22,23,23,24,25,26,26,27,28,28,28,29,30,31,32,33,33,34,34,35,36,36,37,38",
			"name": "Number of numbers m \u003c= n that have a prime divisor greater than sqrt(n) (i.e., A006530(m)\u003esqrt(n)).",
			"comment": [
				"Values of n that are squares of primes p^2 seem to reduce the value of a(p^2) from the value a(p^2 - 1). Example, a(24) = 13, a(25) = 9; a(120) = 70, a(121) = 60.",
				"a(p^2) = a(p^2-1) - p + 1 if p is prime. If n is not the square of a prime, a(n) \u003e= a(n-1).- _Robert Israel_, Aug 11 2014"
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A241419/b241419.txt\"\u003eTable of n, a(n) for n = 1..5000\u003c/a\u003e",
				"E. Naslund, \u003ca href=\"http://www.emis.de/journals/INTEGERS/papers/n81/n81.Abstract.html\"\u003eThe Average Largest Prime Factor\u003c/a\u003e, Integers, Vol. 13 (2013), A81. See \"2. The Main Theorem.\""
			],
			"formula": [
				"a(n) = Sum_{prime p \u003e sqrt(n)} floor(n/p). - _Max Alekseyev_, Nov 14 2017"
			],
			"example": [
				"a(12) = 4, because there are four values of m = {5, 7, 10, 11} that have prime divisors that exceed sqrt(12) = 3.464... These prime divisors are {5, 7, 5, 11} respectively."
			],
			"maple": [
				"N:= 1000: # to get a(1) to a(N)",
				"MF:= map(m -\u003e max(numtheory:-factorset(m))^2,\u003c($1..N)\u003e): MF[1]:= 0:",
				"seq(nops(select(m -\u003e MF[m]\u003en, [$1..n])),n=1..N); # _Robert Israel_, Aug 11 2014"
			],
			"mathematica": [
				"a241419[n_Integer] :=",
				"Module[{f},",
				"  f = Reap[For[m = 1, m \u003c= n, m++,",
				"     If[Max[First[Transpose[FactorInteger[m]]]] \u003e Sqrt[n], Sow[m],",
				"      False]]];",
				"  If[Length[f[[2]]] == 0, Length[f[[2]]], Length[f[[2, 1]]]]]; a241419[120]"
			],
			"program": [
				"(PARI) isok(i, n) = {my(f = factor(i)); my(sqrn = sqrt(n)); for (k=1, #f~, if ((p=f[k, 1]) \u0026\u0026 (p\u003esqrn) , return (1)););}",
				"a(n) = sum(i=1, n, isok(i, n)); \\\\ _Michel Marcus_, Aug 11 2014",
				"(PARI) A241419(n) = my(r=0); forprime(p=sqrtint(n)+1,n, r+=n\\p); r; \\\\ _Max Alekseyev_, Nov 14 2017"
			],
			"xref": [
				"Cf. A013939, A295084."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Michael De Vlieger_, Aug 08 2014",
			"references": 3,
			"revision": 22,
			"time": "2017-11-14T00:12:28-05:00",
			"created": "2014-08-11T23:33:36-04:00"
		}
	]
}