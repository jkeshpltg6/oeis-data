{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008282",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8282,
			"data": "1,1,1,1,2,2,2,4,5,5,5,10,14,16,16,16,32,46,56,61,61,61,122,178,224,256,272,272,272,544,800,1024,1202,1324,1385,1385,1385,2770,4094,5296,6320,7120,7664,7936,7936,7936,15872,23536,30656,36976,42272,46366,49136,50521,50521",
			"name": "Triangle of Euler-Bernoulli or Entringer numbers read by rows: T(n,k) is the number of down-up permutations of n+1 starting with k+1.",
			"reference": [
				"R. C. Entringer, A combinatorial interpretation of the Euler and Bernoulli numbers, Nieuw Archief voor Wiskunde, 14 (1966), 241-246."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A008282/b008282.txt\"\u003eRows n=1..120 of triangle, flattened\u003c/a\u003e",
				"V. I. Arnold, \u003ca href=\"http://mi.mathnet.ru/eng/umn4470\"\u003eThe calculus of snakes and the combinatorics of Bernoulli, Euler and Springer numbers of Coxeter groups\u003c/a\u003e, Uspekhi Mat. nauk., 47 (#1, 1992), 3-45 = Russian Math. Surveys, Vol. 47 (1992), 1-51.",
				"J. L. Arregui, \u003ca href=\"http://arXiv.org/abs/math.NT/0109108\"\u003eTangent and Bernoulli numbers related to Motzkin and Catalan numbers by means of numerical triangles\u003c/a\u003e, arXiv:math/0109108 [math.NT], 2001.",
				"B. Bauslaugh and F. Ruskey, \u003ca href=\"http://dx.doi.org/10.1007/BF01932127\"\u003eGenerating alternating permutations lexicographically\u003c/a\u003e, Nordisk Tidskr. Informationsbehandling (BIT) 30 16-26 1990.",
				"Carolina Benedetti, Rafael S. González D’León, Christopher R. H. Hanusa, Pamela E. Harris, Apoorva Khare, Alejandro H. Morales, and Martha Yip, \u003ca href=\"https://www.cs.ox.ac.uk/people/dan.olteanu/papers/mo-amw18.pdf\"\u003eThe volume of the caracol polytope\u003c/a\u003e, Séminaire Lotharingien de Combinatoire XX (2018), Article #YY, Proceedings of the 30th Conference on Formal Power, Series and Algebraic Combinatorics (Hanover), 2018.",
				"Beáta Bényi and Péter Hajnal, \u003ca href=\"https://arxiv.org/abs/1804.01868\"\u003ePoly-Bernoulli Numbers and Eulerian Numbers\u003c/a\u003e, arXiv:1804.01868 [math.CO], 2018.",
				"Neil J. Y. Fan and Liao He, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v22i2p45\"\u003eThe Complete cd-Index of Boolean Lattices\u003c/a\u003e, Electron. J. Combin., 22 (2015), #P2.45.",
				"Dominique Foata and Guo-Niu Han, \u003ca href=\"http://www-irma.u-strasbg.fr/~foata/paper/pub123Seidel.pdf\"\u003eSeidel Triangle Sequences and Bi-Entringer Numbers\u003c/a\u003e, November 20, 2013.",
				"Dominique Foata and Guo-Niu Han, \u003ca href=\"https://doi.org/10.1016/j.ejc.2014.06.007\"\u003eSeidel Triangle Sequences and Bi-Entringer Numbers\u003c/a\u003e, European Journal of Combinatorics, 42 (2014), 243-260. [See Corollary 1.3. In Eq. (1.10), the power of x should be k-1 rather than k.]",
				"Dominique Foata and Guo-Niu Han, \u003ca href=\"http://arxiv.org/abs/1601.04371\"\u003eAndré Permutation Calculus; a Twin Seidel Matrix Sequence\u003c/a\u003e, arXiv:1601.04371 [math.CO], 2016.",
				"B. Gourevitch, \u003ca href=\"http://www.pi314.net\"\u003eL'univers de Pi\u003c/a\u003e.",
				"G. Kreweras, \u003ca href=\"http://archive.numdam.org/article/MSH_1976__53__5_0.pdf\"\u003eLes préordres totaux compatibles avec un ordre partiel\u003c/a\u003e, Math. Sci. Humaines No. 53 (1976), 5-30.",
				"J. Millar, N. J. A. Sloane and N. E. Young, A new operation on sequences: the Boustrophedon transform, J. Combin. Theory, 17A (1996) 44-54 (\u003ca href=\"http://neilsloane.com/doc/bous.txt\"\u003eAbstract\u003c/a\u003e, \u003ca href=\"http://neilsloane.com/doc/bous.pdf\"\u003epdf\u003c/a\u003e, \u003ca href=\"http://neilsloane.com/doc/bous.ps\"\u003eps\u003c/a\u003e).",
				"C. Poupard, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(82)90293-X\"\u003eDe nouvelles significations énumeratives des nombres d'Entringer\u003c/a\u003e, Discrete Math., 38 (1982), 265-271.",
				"C. Poupard, \u003ca href=\"http://dx.doi.org/10.1006/eujc.1997.0147\"\u003eTwo other interpretations of the Entringer numbers\u003c/a\u003e, Eur. J. Combinat. 18 (1997) 939-943.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Boustrophedon_transform\"\u003eBoustrophedon transform\u003c/a\u003e.",
				"\u003ca href=\"/index/Bo#boustrophedon\"\u003eIndex entries for sequences related to boustrophedon transform\u003c/a\u003e"
			],
			"formula": [
				"From  _Emeric Deutsch_, May 15 2004: (Start)",
				"Let E[j] = A000111(j) = j! * [x^j](sec(x) + tan(x)) be the up/down or Euler numbers. For 1 \u003c= k \u003c n,",
				"T(n, k) = Sum_{i=0..floor((k-1)/2)} (-1)^i * binomial(k, 2*i+1) * E[n-2*i-1];",
				"T(n,k) = Sum_{i=0..floor((n-k)/2)} (-1)^i * binomial(n-k, 2*i) * E[n-2*i];",
				"T(n, k) = Sum_{i=0..floor((n-k)/2)} (-1)^i * binomial(n-k, 2*i) * E[n-2*i]; and",
				"T(n, n) = E[n] for n \u003e= 1. (End)",
				"From _Petros Hadjicostas_, Feb 17 2021: (Start)",
				"If n is even, then T(n,k) = k!*(n-k)!*[x^(n-k),y^k] cos(x)/cos(x + y).",
				"If n is odd, then T(n,k) = k!*(n-k)!*[x^k,y^(n-k)] sin(x)/cos(x + y).",
				"(These were adapted and corrected from the formulas in Corollary 1.3 in Foata and Guo-Niu Han (2014).) (End)"
			],
			"example": [
				"Triangle T(n,k) (with rows n \u003e= 1 and columns k = 1..n) begins",
				"   1",
				"   1  1",
				"   1  2  2",
				"   2  4  5  5",
				"   5 10 14 16 16",
				"  16 32 46 56 61 61",
				"  ...",
				"Each row is constructed by forming the partial sums of the previous row, reading from the right and repeating the final term.",
				"T(4,3) = 5 because we have 41325, 41523, 42314, 42513 and 43512. All these permutations have length n+1 = 5, start with k+1 = 4, and they are down-up permutations."
			],
			"maple": [
				"f:=series(sec(x)+tan(x),x=0,25): E[0]:=1: for n from 1 to 20 do E[n]:=n!*coeff(f,x^n) od: T:=proc(n,k) if k\u003cn then sum((-1)^i*binomial(k,2*i+1)*E[n-2*i-1],i=0..floor((k-1)/2)) elif k=n then E[n] else 0 fi end: seq(seq(T(n,k),k=1..n),n=1..10);",
				"# Alternatively:",
				"T := proc(n, k) option remember; if k = 0 then `if`(n = 0, 1, 0) else",
				"T(n, k - 1) + T(n - 1, n - k) fi end:",
				"for n from 1 to 6 do seq(T(n,k), k=1..n) od; # _Peter Luschny_, Aug 03 2017",
				"# Third program:",
				"T := proc(n, k) local w: if 0 = n mod 2 then w := coeftayl(cos(x)/cos(x + y), [x, y] = [0, 0], [n - k, k]): end if: if 1 = n mod 2 then w := coeftayl(sin(x)/cos(x + y), [x, y] = [0, 0], [k, n - k]): end if: w*(n - k)!*k!: end proc:",
				"for n from 1 to 6 do seq(T(n,k), k=1..n) od; # _Petros Hadjicostas_, Feb 17 2021"
			],
			"mathematica": [
				"ro[1] = {1}; ro[n_] := ro[n] = (s = Accumulate[ Reverse[ ro[n-1]]]; Append[ s, Last[s]]); Flatten[ Table[ ro[n], {n, 1, 10}]] (* _Jean-François Alcover_, Oct 03 2011 *)",
				"nxt[lst_]:=Module[{lst2=Accumulate[Reverse[lst]]},Flatten[Join[ {lst2,Last[ lst2]}]]]; Flatten[NestList[nxt,{1},10]] (* _Harvey P. Dale_, Aug 17 2014 *)"
			],
			"program": [
				"(Haskell)",
				"a008282 n k = a008282_tabl !! (n-1) !! (k-1)",
				"a008282_row n = a008282_tabl !! (n-1)",
				"a008282_tabl = iterate f [1] where",
				"   f xs = zs ++ [last zs] where zs = scanl1 (+) (reverse xs)",
				"-- _Reinhard Zumkeller_, Dec 28 2011"
			],
			"xref": [
				"Cf. A010094, A000111, A099959, A009766, A236935."
			],
			"keyword": "nonn,tabl,easy,nice",
			"offset": "1,5",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Example and Formula sections edited by _Petros Hadjicostas_, Feb 17 2021"
			],
			"references": 22,
			"revision": 86,
			"time": "2021-02-17T09:23:30-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}