{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A104477",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 104477,
			"data": "1,0,1,0,1,0,2,0,1,0,3,0,2,0,3,0,2,0,4,0,3,0,4,0,4,0,3,0,5,0,6,0,4,0,5,0,5,0,6,0,6,0,6,0,5,0,8,0,7,0,6,0,7,0,8,0,7,0,7,0,9,0,8,0,9,0,8,0,9,0,8,0,8,0,11,0,10,0,11,0,10,0,8,0,11,0,10,0,12,0,9,0,12,0,14,0,9,0,10,0",
			"name": "Number of successive squarefree intervals between primes.",
			"comment": [
				"Find the number (the \"run length\") of successive intervals [p, p'=nextprime(p)] (followed by [p', p''], then [p'', p'''] etc.) which do not contain a square. When a square (n+1)^2 is found in such an interval, this will result in a term a(2n) = 0, preceded by a(2n-1) = the number of intervals of primes counted before reaching that square, i.e., between n^2 and (n+1)^2. - _M. F. Hasler_, Oct 01 2018"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A104477/b104477.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(2n) = 0: this is the interval from the greatest prime less than the (n+1)th square, through that square and up to the least prime greater than that square. - _Robert G. Wilson v_, Apr 23 2005",
				"a(2n-1) = the difference between the indices of the greatest prime less than (n+1)^2 and the least prime greater than n^2. - _Robert G. Wilson v_, Apr 23 2005",
				"a(2n-1) = A014085(n) - 1 = primepi((n+1)^2 - primepi(n^2) - 1. - _M. F. Hasler_, Oct 01 2018"
			],
			"example": [
				"a(1)=1 because the first interval between primes (2 to 3) is free of squares.",
				"a(2)=0 because there is a square between 3 and 5.",
				"a(7)=2 because there are two successive squarefree intervals: 17 to 19; and 19 to 23.",
				"a(8)=0 because between 23 and 29 there is a square: 25."
			],
			"mathematica": [
				"NextPrim[n_] := Block[{k = n + 1}, While[ !PrimeQ[k], k++ ]; k]; PrevPrim[n_] := Block[{k = n - 1}, While[ !PrimeQ[k], k-- ]; k]; f[n_] := If[ EvenQ[n], 0, PrimePi[ PrevPrim[(n + 3)^2/4]] - PrimePi[ NextPrim[(n + 1)^2/4]]]; Table[ f[n], {n, 100}] (* _Robert G. Wilson v_, Apr 23 2005 *)"
			],
			"program": [
				"(PARI) p=2; c=0; forprime(np=p+1, 1e4, if( sqrtint(p) \u003c sqrtint(np), print1(c\",\",c=0,\",\"), c++); p=np) \\\\ For illustrative purpose. Better:",
				"A104477(n)=if(bittest(n,0),primepi((1+n\\/=2)^2)-primepi(n^2)-1,0) \\\\ _M. F. Hasler_, Oct 01 2018"
			],
			"xref": [
				"Cf. A061265, A031265, A104481.",
				"Equals A014085 - 1 without the initial term, interleaved with 0's."
			],
			"keyword": "easy,nonn",
			"offset": "1,7",
			"author": "_Alexandre Wajnberg_, Apr 18 2005",
			"ext": [
				"More terms from _Robert G. Wilson v_, Apr 23 2005",
				"Offset corrected by _M. F. Hasler_, Oct 01 2018"
			],
			"references": 2,
			"revision": 15,
			"time": "2018-10-10T09:22:58-04:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}