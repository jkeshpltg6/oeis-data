{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A132013",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 132013,
			"data": "1,-1,1,0,-2,1,0,0,-3,1,0,0,0,-4,1,0,0,0,0,-5,1,0,0,0,0,0,-6,1,0,0,0,0,0,0,-7,1,0,0,0,0,0,0,0,-8,1,0,0,0,0,0,0,0,0,-9,1,0,0,0,0,0,0,0,0,0,-10,1,0,0,0,0,0,0,0,0,0,0,-11,1,0,0,0,0,0,0,0,0,0,0,0,-12,1",
			"name": "T(n,j) for an iterated mixed order Laguerre transform. Coefficients of the normalized generalized Laguerre polynomials (-1)^n*n!*L(n,1-n,x).",
			"comment": [
				"The matrix operation b = T*a can be characterized in several ways in terms of the coefficients a(n) and b(n), their o.g.f.s A(x) and B(x), or e.g.f.s EA(x) and EB(x).",
				"1) b(0) = a(0), b(n) = a(n) - n*a(n-1) for n \u003e 0",
				"2) b(n) = n! Lag{n,(.)!*Lag[.,a(.),0],-1}, umbrally",
				"3) b(n) = n! Sum_{j=0..min(1,n)} (-1)^j * binomial(n,j)*a(n-j)/(n-j)!",
				"4) b(n) = (-1)^n n! Lag(n,a(.),1-n)",
				"5) B(x) = (1-xDx) A(x) = [1-x*Lag(1,-xD:,0)] A(x)",
				"6) EB(x) = (1-x) EA(x),",
				"where D is the derivative w.r.t. x and Lag(n,x,m) is the associated Laguerre polynomial of order m. These formulas are easily generalized for repeated applications of the operator.",
				"c = (1,-1,0,0,0,...) is the sequence associated to T under the list partition transform and the associated operations described in A133314. The reciprocal sequence is d = (0!,1!,2!,3!,4!,...).",
				"Consequently, the inverse of T is TI(n,k) = binomial(n,k)*d(n-k) = A094587, which has the property that the terms at and below TI(m,m) are the associated sequence under the list partition transform for the inverse for T^(m+1) for m=0,1,2,3,... .",
				"Row sums of T = [formula 3 with all a(n) = 1] = [binomial transform of c] = [coefficients of B(x) with A(x) = 1/(1-x)] = A024000 = (1,0,-1,-2,-3,...), with e.g.f. = [EB(x) with EA(x) = exp(x)] = (1-x) * exp(x) = exp(x)*exp(c(.)*x) = exp[(1+c(.))*x].",
				"Alternating row sums of T = [formula 3 with all a(n) = (-1)^n] = [finite differences of c] = [coefficients of B(x) with A(x) = 1/(1+x)] = (1,-2,3,-4,...), with e.g.f. = [EB(x) with EA(x) = exp(-x)] = (1-x) * exp(-x) = exp(-x)*exp(c(.)*x) = exp[-(1-c(.))*x].",
				"An e.g.f. for the o.g.f.s for repeated applications of T on A(x) is given by",
				"exp[t*(1-xDx)] A(x) = e^t * Sum_{n=0,1,...} (-t*x)^n * Lag(n,-:xD:,0) A(x)",
				"= e^t * exp{[-t*u/(1+t*u)]*:xD:} / (1+t*u) A(x) (eval. at u=x)",
				"= e^t * A[x/(1+t*x)]/(1+t*x) .",
				"See A132014 for more notes on repeated applications."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A132013/b132013.txt\"\u003eRows n=0..100 of triangle, flattened\u003c/a\u003e",
				"T. Copeland, \u003ca href=\"http://tcjpn.wordpress.com/2016/11/06/compositional-inverse-operators-and-sheffer-sequences/\"\u003eCompositional inverse operators and Sheffer sequences\u003c/a\u003e, 2016.",
				"M. Janjic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL12/Janjic/janjic22.html\"\u003eSome classes of numbers and derivatives\u003c/a\u003e, JIS 12 (2009) #09.8.3.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Appell_sequence\"\u003eAppell sequence\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = binomial(n,k)*c(n-k), with the sequence c defined in the comments.",
				"E.g.f.: exp(x*y)(1-x), which implies the row polynomials form an Appell sequence. More relations can be found in A132382. - _Tom Copeland_, Dec 03 2013",
				"From _Tom Copeland_, Apr 21 2014: (Start)",
				"Change notation letting L(n,m,x) = Lag(n,x,m).",
				"Row polynomials: (-1)^n*n!*L(n,1-n,x) = -x^(n-1)*L(1,n-1,x) =",
				"(-1)^n*(1/(1-n)!)*K(-n,1-n+1,x) where K is Kummer's confluent hypergeometric function (as a limit of n+s as s tends to zero).",
				"For the row polynomials, the lowering operator = d/dx and the raising operator = x - 1/(1-D).",
				"T = I - A132440 = 2*I - exp[A238385-I] = signed exp[A238385-I], where I = identity matrix.",
				"Operationally, (-1)^n*n!*L(n,1-n,-:xD:) = -x^(n-1)*:Dx:^n*x^(1-n) = (-1)^n*x^(-1)*:xD:^n*x = (-1)^n*n!*binomial(xD+1,n) = (-1)^n*n!*binomial(1,n)*K(-n,1-n+1,-:xD:) where :AB:^n = A^n*B^n for any two operators. Cf. A235706. (End)",
				"The unsigned row polynomials have e.g.f. (1+t)e^(xt) = exp(t*p.(x)), umbrally, and p_n(x) = (1+D) x^n. With q_n(x) the row polynomials of A094587, p_n(x) = u_n(q.(v.(x))), umbrally, where u_n(x) = (-1)^n v_n(-x) = (-1)^n Lah_n(x), the Lah polynomials with e.g.f. exp[x*t/(t-1)]. This has the matrix form unsigned [T] = [p] = [u]*[q]*[v]. Conversely, q_n(x) = v_n (p.(u.(x))). - _Tom Copeland_, Nov 10 2016",
				"n-th row polynomial: n!*Sum_{k = 0..n} (-1)^k*binomial(n,k)*Lag(k,1,x). - _Peter Bala_, Jul 25 2021"
			],
			"example": [
				"First few rows of the triangle are",
				"   1;",
				"  -1,  1;",
				"   0, -2,  1;",
				"   0,  0, -3,  1;",
				"   0,  0,  0, -4,  1;",
				"   0,  0,  0,  0, -5,  1;",
				"   0,  0,  0,  0,  0, -6,  1;",
				"   0,  0,  0,  0,  0,  0, -7,  1;"
			],
			"maple": [
				"c := n -\u003e `if`(n=0,1,`if`(n=1,-1,0)):",
				"T := (n,k) -\u003e binomial(n,k)*c(n-k); # _Peter Luschny_, Nov 14 2016"
			],
			"mathematica": [
				"Table[PadLeft[{-n, 1}, n+1], {n, 0, 13}] // Flatten (* _Jean-François Alcover_, Apr 29 2014 *)"
			],
			"program": [
				"(PARI) row(n) = Vecrev((-1)^n*n!*pollaguerre(n, 1-n)); \\\\ _Michel Marcus_, Jul 26 2021"
			],
			"xref": [
				"Cf. A235706, A132382, A132159, A094587, A132014, A154955, A235706."
			],
			"keyword": "easy,sign,tabl",
			"offset": "0,5",
			"author": "_Tom Copeland_, Oct 30 2007",
			"ext": [
				"Title modified by _Tom Copeland_, Apr 21 2014"
			],
			"references": 10,
			"revision": 72,
			"time": "2021-07-27T21:21:35-04:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}