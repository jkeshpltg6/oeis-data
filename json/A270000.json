{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A270000",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 270000,
			"data": "1,1,1,2,1,1,1,1,2,3,1,3,1,2,1,1,1,3,2,1,3,3,1,3,3,1,1,3,2,1,1,3,1,3,1,2,3,3,2,4,1,2,3,2,3,3,1,1,3,1,3,3,1,5,1,3,3,2,2,2,1,1,1,5,3,3,1,3,3,4,1,2,2,3,3,6,3,3,2,1,4,3,1,2,2,3,3",
			"name": "Harmonic fractility of n.",
			"comment": [
				"In order to define (harmonic) fractility of an integer m \u003e 1, we first define nested interval sequences. Suppose that r = (r(n)) is a sequence satisfying (i) 1 = r(1) \u003e r(2) \u003e r(3) \u003e ... and (ii) r(n) -\u003e 0. For x in (0,1], let n(1) be the index n such that r(n+1) \u003c x \u003c= r(n), and let L(1) = r(n(1)) - r(n(1)+1). Let n(2) be the index n such that r(n(1)+1) + L(1)*r(n+1) \u003c x \u003c= r(n(1)+1) + L(1)*r(n), and let L(2) = (r(n(2))-r(n(2)+1))*L(1). Continue inductively to obtain the sequence (n(1), n(2), n(3), ...) =: NI(x), the r-nested interval sequence of x.",
				"For fixed r, call x and y equivalent if NI(x) and NI(y) are eventually identical. For m \u003e 1, the r-fractility of m is the number of equivalence classes of sequences NI(k/m) for 0 \u003c k \u003c m. Taking r = (1/1, 1/2, 1/3, 1/4, ... ) gives harmonic fractility.",
				"For harmonic fractility, r(n) = 1/n, n(j+1) = floor(L(j)/(x - Sum_{i=1..j} L(i-1)/(n(i)+1))) for all j \u003e= 0, L(0) = 1. - _M. F. Hasler_, Nov 05 2018"
			],
			"link": [
				"Jack W Grahl, \u003ca href=\"/A270000/b270000.txt\"\u003eTable of n, a(n) for n = 2..999\u003c/a\u003e",
				"Jack W Grahl, \u003ca href=\"/A270000/a270000.py.txt\"\u003ePython code to generate this sequence\u003c/a\u003e"
			],
			"example": [
				"NI(1/11) = (11, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...),",
				"NI(2/11) = (5, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, ...),",
				"NI(3/11) = (3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, ...),",
				"NI(4/11) = (2, 5, 2, 1, 2, 1, 2, 1, 2, 1, 2, ...),",
				"NI(5/11) = (2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, ...),",
				"NI(6/11) = (1, 11, 1, 1, 1, 1, 1, 1, 1, 1, ...),",
				"NI(7/11) = (1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, ...),",
				"NI(8/11) = (1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, ...),",
				"NI(9/11) = (1, 1, 3, 3, 3, 3, 3, 3, 3, 3, 3, ...),",
				"NI(10/11) = (1, 1, 1, 3, 3, 3, 3, 3, 3, 3, ...),",
				"so that there are 3 equivalence classes for n = 11, and that the harmonic fractility of 11 is 3."
			],
			"mathematica": [
				"A270000[n_] := CountDistinct[With[{l = NestWhileList[Rescale[#, {1/(Floor[1/#] + 1), 1/Floor[1/#]}] \u0026, #, UnsameQ, All]}, Min@l[[First@FirstPosition[l, Last@l] ;;]]] \u0026 /@ Range[1/n, 1 - 1/n, 1/n]] (* _Davin Park_, Nov 09 2016 *)"
			],
			"program": [
				"From _M. F. Hasler_, Nov 05 2018: (Start)",
				"(PARI) A270000(n)=#Set(vector(n-1,k,NIR(k/n))) \\\\ where:",
				"NIR(x, n, L=1, S=[], c=0)={for(i=2, oo, n=L\\x; S=setunion(S, [x/L]); x-=L/(n+1); L/=n*(n+1); setsearch(S, x/L)\u0026\u0026 if(c, break, c=!S=[])); S[1]} \\\\ variant of the function NI() below; returns just a unique representative (smallest x/L occurring within the period) of the equivalence class.",
				"NI(x, n=[], L=1, S=[], c=0)={for(i=2, oo, n=concat(n, L\\x); c|| S=setunion(S, [x/L]); x-=L/(n[#n]+1); L/=n[#n]*(n[#n]+1); if(!c, setsearch(S, x/L)\u0026\u0026 [c,S]=[i,x/L], x/L==S, c-=i; break)); [n[1..2*c-1], n[c..-1]]} \\\\ Returns the harmonic nested interval sequence for x in the form [transition, period]. (End)"
			],
			"xref": [
				"Guide to related sequences:",
				"  k - numbers with harmonic fractility k:",
				"  1 - A269804",
				"  2 - A269805",
				"  3 - A269806",
				"  4 - A269807",
				"  5 - A269808",
				"  6 - A269809",
				"Cf. A269570 (binary fractility), A269982 (factorial fractility)."
			],
			"keyword": "nonn,easy",
			"offset": "2,4",
			"author": "_Clark Kimberling_ and _Peter J. C. Moses_, Mar 11 2016",
			"ext": [
				"Definition corrected by _Jack W Grahl_, Jun 27 2018",
				"Edited by _M. F. Hasler_, Nov 05 2018"
			],
			"references": 16,
			"revision": 43,
			"time": "2018-11-06T06:47:16-05:00",
			"created": "2016-03-12T21:56:01-05:00"
		}
	]
}