{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A144406",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 144406,
			"data": "1,1,1,1,1,1,1,1,2,1,1,1,2,3,1,1,1,2,4,5,1,1,1,2,4,7,8,1,1,1,2,4,8,13,13,1,1,1,2,4,8,15,24,21,1,1,1,2,4,8,16,29,44,34,1,1,1,2,4,8,16,31,56,81,55,1,1,1,2,4,8,16,32,61,108,149,89,1,1,1,2,4,8,16,32,63,120,208,274",
			"name": "Rectangular array A read by upward antidiagonals: entry A(n,k) in row n and column k gives the number of compositions of k in which no part exceeds n, n\u003e=1, k\u003e=0.",
			"comment": [
				"Polynomial expansion as antidiagonal of p(x,n) = (x-1)/(x^n*(-x+(2*x-1)/x^n). Based on the Pisot general polynomial type q(x,n) = x^n - (x^n-1)/(x-1) (the original name of the sequence).",
				"Row sums are 1, 2, 3, 5, 8, 14, ... (A079500).",
				"Conjecture: Since the array row sequences successively tend to A000079, the absolute values of nonzero differences between two successive row sequences tend to A045623 = {1,2,5,12,28,64,144,320,704,1536,...}, as k -\u003e infinity. - _L. Edson Jeffery_, Dec 26 2013"
			],
			"formula": [
				"p(x,n) = (x-1)/(x^n*(-x+(2*x-1)/x^n);",
				"t(n,m) = antidiagonal_expansion(p(x,n)).",
				"G.f. for array A: (1-x)/(1 - 2*x + x^(n+1)), n\u003e=1. - _L. Edson Jeffery_, Dec 26 2013"
			],
			"example": [
				"Array A begins:",
				"{1, 1, 1, 1, 1,  1,  1,  1,   1,   1,   1, ...}",
				"{1, 1, 2, 3, 5,  8, 13, 21,  34,  55,  89, ...}",
				"{1, 1, 2, 4, 7, 13, 24, 44,  81, 149, 274, ...}",
				"{1, 1, 2, 4, 8, 15, 29, 56, 108, 208, 401, ...}",
				"{1, 1, 2, 4, 8, 16, 31, 61, 120, 236, 464, ...}",
				"{1, 1, 2, 4, 8, 16, 32, 63, 125, 248, 492, ...}",
				"{1, 1, 2, 4, 8, 16, 32, 64, 127, 253, 504, ...}",
				"{1, 1, 2, 4, 8, 16, 32, 64, 128, 255, 509, ...}",
				"{1, 1, 2, 4, 8, 16, 32, 64, 128, 256, 511, ...}",
				"... - _L. Edson Jeffery_, Dec 26 2013",
				"As a triangle:",
				"{1},",
				"{1, 1},",
				"{1, 1, 1},",
				"{1, 1, 2, 1},",
				"{1, 1, 2, 3, 1},",
				"{1, 1, 2, 4, 5, 1},",
				"{1, 1, 2, 4, 7, 8, 1},",
				"{1, 1, 2, 4, 8, 13, 13, 1},",
				"{1, 1, 2, 4, 8, 15, 24, 21, 1},",
				"{1, 1, 2, 4, 8, 16, 29, 44, 34, 1},",
				"{1, 1, 2, 4, 8, 16, 31, 56, 81, 55, 1},",
				"{1, 1, 2, 4, 8, 16, 32, 61, 108, 149, 89, 1},",
				"{1, 1, 2, 4, 8, 16, 32, 63, 120, 208, 274, 144, 1},",
				"{1, 1, 2, 4, 8, 16, 32, 64, 125, 236, 401, 504, 233, 1},",
				"{1, 1, 2, 4, 8, 16, 32, 64, 127, 248, 464, 773, 927, 377, 1}"
			],
			"mathematica": [
				"Clear[f, b, a, g, h, n, t]; g[x_, n_] = x^(n) - (x^n - 1)/(x - 1); h[x_, n_] = FullSimplify[ExpandAll[x^(n)*g[1/x, n]]]; f[t_, n_] := 1/h[t, n];Series[f[t, m], {t, 0, 30}], n], {n, 0, 30}], {m, 1, 31}]; b = Table[Table[a[[n - m + 1]][[m]], {m, 1, n }], {n, 1, 15}]; Flatten[b] (* Triangle version *)",
				"Grid[Table[CoefficientList[Series[(1 - x)/(1 - 2 x + x^(n + 1)), {x, 0, 10}], x], {n, 1, 10}]] (* Array version - _L. Edson Jeffery_, Jul 18 2014 *)"
			],
			"xref": [
				"Cf. A000079, A045623, A092921, A175331.",
				"Same as A048887 but with a column of 1's added on the left (the number of compositions of 0 is defined to be equal to 1).",
				"Array rows (with appropriate offsets) are A000012, A000045, A000073, A000078, A001591, A001592, etc."
			],
			"keyword": "nonn,tabl",
			"offset": "1,9",
			"author": "_Roger L. Bagula_ and _Gary W. Adamson_, Sep 29 2008",
			"ext": [
				"Definition changed by _L. Edson Jeffery_, Jul 18 2014"
			],
			"references": 1,
			"revision": 35,
			"time": "2016-12-09T06:20:53-05:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}