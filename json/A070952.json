{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A070952",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 70952,
			"data": "1,3,3,6,4,9,5,12,7,12,11,14,12,19,13,22,15,19,20,24,21,23,23,28,26,27,26,33,30,34,31,39,26,39,29,46,32,44,38,45,47,41,45,49,38,55,42,51,44,53,43,59,52,60,49,65,57,60,56,69,61,70,59,78,64,56,65,69,69",
			"name": "Number of 1's in n-th generation of 1-D CA using Rule 30, started with a single 1.",
			"comment": [
				"Number of 1's in n-th row of triangle in A070950.",
				"Row sums in A070950; a(n) = 2*n + 1 - A070951(n). - _Reinhard Zumkeller_, Jun 07 2013"
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A070952/b070952.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/A070952/a070952.png\"\u003eIllustration of first 20 generations\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"http://arxiv.org/abs/1503.01168\"\u003eOn the Number of ON Cells in Cellular Automata\u003c/a\u003e, arXiv:1503.01168, 2015",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Rule30.html\"\u003eRule 30\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Rule_30\"\u003eRule 30\u003c/a\u003e",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e"
			],
			"example": [
				"May be arranged into blocks of length 1,1,2,4,8,16,...:",
				"1,",
				"3,",
				"3, 6,",
				"4, 9, 5, 12,",
				"7, 12, 11, 14, 12, 19, 13, 22,",
				"15, 19, 20, 24, 21, 23, 23, 28, 26, 27, 26, 33, 30, 34, 31, 39,",
				"26, 39, 29, 46, 32, 44, 38, 45, 47, 41, 45, 49, 38, 55, 42, 51,",
				"    44, 53, 43, 59, 52, 60, 49, 65, 57, 60, 56, 69, 61, 70, 59, 78,",
				"64, 56, 65, 69, 69, ..."
			],
			"mathematica": [
				"Map[Function[Apply[Plus,Flatten[ #1]]], CellularAutomaton[30,{{1},0},100]] (* _N. J. A. Sloane_, Aug 10 2009 *)",
				"SequenceCount[s, {1,0}] + 2 SequenceCount[s, {0,0,1}] (* gives a(n) where s is the sequence for row n-1 *) (* _Trevor Cappallo_, May 01 2021 *)"
			],
			"program": [
				"(Haskell)",
				"a070952 = sum . a070950_row  -- _Reinhard Zumkeller_, Jun 07 2013"
			],
			"xref": [
				"This sequence, A110240, and A245549 all describe the same sequence of successive states. See also A269160.",
				"Cf. A071049, A070950, A070951, A151929, A051023.",
				"Cf. A110267 (partial sums), A246023, A246024, A246025, A246026, A246597.",
				"A265703 is an essentially identical sequence."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, May 19 2002, Aug 10 2009",
			"ext": [
				"More terms from _Hans Havermann_, May 26 2002",
				"Corrected offset and initial term - _N. J. A. Sloane_, Jun 07 2013"
			],
			"references": 24,
			"revision": 57,
			"time": "2021-05-26T02:51:52-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}