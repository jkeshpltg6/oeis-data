{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A279941",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 279941,
			"data": "1,0,111,10,11001,10000,11111,10100010,111001,1101010000,1101011111,11100100010,101100111001,111011010000,111010110011111,10001100100010,10001011110111001,11100100111010000,101100111011011111,11101011110100010,110101001100110111001",
			"name": "Binary representation of the x-axis, from the origin to the right edge, of the n-th stage of growth of the two-dimensional cellular automaton defined by \"Rule 217\", based on the 5-celled von Neumann neighborhood.",
			"comment": [
				"Initialized with a single black (ON) cell at stage zero."
			],
			"reference": [
				"S. Wolfram, A New Kind of Science, Wolfram Media, 2002; p. 170."
			],
			"link": [
				"Robert Price, \u003ca href=\"/A279941/b279941.txt\"\u003eTable of n, a(n) for n = 0..126\u003c/a\u003e",
				"Robert Price, \u003ca href=\"/A279941/a279941.tmp.txt\"\u003eDiagrams of first 20 stages\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"http://arxiv.org/abs/1503.01168\"\u003eOn the Number of ON Cells in Cellular Automata\u003c/a\u003e, arXiv:1503.01168 [math.CO], 2015",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ElementaryCellularAutomaton.html\"\u003eElementary Cellular Automaton\u003c/a\u003e",
				"S. Wolfram, \u003ca href=\"http://wolframscience.com/\"\u003eA New Kind of Science\u003c/a\u003e",
				"Wolfram Research, \u003ca href=\"http://atlas.wolfram.com/\"\u003eWolfram Atlas of Simple Programs\u003c/a\u003e",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_2D_5-Neighbor_Cellular_Automata\"\u003eIndex to 2D 5-Neighbor Cellular Automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_Elementary_Cellular_Automata\"\u003eIndex to Elementary Cellular Automata\u003c/a\u003e"
			],
			"mathematica": [
				"CAStep[rule_, a_] := Map[rule[[10 - #]] \u0026, ListConvolve[{{0, 2, 0},{2, 1, 2}, {0, 2, 0}}, a, 2],{2}];",
				"code = 217; stages = 128;",
				"rule = IntegerDigits[code, 2, 10];",
				"g = 2 * stages + 1; (* Maximum size of grid *)",
				"a = PadLeft[{{1}}, {g, g}, 0,Floor[{g, g}/2]]; (* Initial ON cell on grid *)",
				"ca = a;",
				"ca = Table[ca = CAStep[rule, ca], {n, 1, stages + 1}];",
				"PrependTo[ca, a];",
				"(* Trim full grid to reflect growth by one cell at each stage *)",
				"k = (Length[ca[[1]]] + 1)/2;",
				"ca = Table[Table[Part[ca[[n]] [[j]],Range[k + 1 - n, k - 1 + n]], {j, k + 1 - n, k - 1 + n}], {n, 1, k}];",
				"Table[FromDigits[Part[ca[[i]] [[i]], Range[i, 2 * i - 1]], 10], {i, 1, stages - 1}]"
			],
			"xref": [
				"Cf. A279940, A279942, A279943."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Robert Price_, Dec 23 2016",
			"references": 4,
			"revision": 7,
			"time": "2016-12-23T21:45:21-05:00",
			"created": "2016-12-23T21:45:21-05:00"
		}
	]
}