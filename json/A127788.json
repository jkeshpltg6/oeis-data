{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A127788",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 127788,
			"data": "0,0,0,0,0,0,0,0,0,0,1,0,0,1,1,0,1,0,1,1,1,0,2,1,0,2,1,0,2,1,2,1,1,1,3,1,2,2,3,1,3,1,3,1,1,1,4,1,1,2,3,1,4,2,3,2,3,2,5,0,4,3,3,1,5,3,5,2,3,1,6,1,5,4,3,1,5,1,6,2,2,3,7,2,5,4,5,3,7,3,7,2,5,3,7,2,7,3,4,1,8,3",
			"name": "Dimension of the space of newforms of weight 2 and level n.",
			"comment": [
				"\"Newform\" is meant in the sense of Atkin-Lehner, that is, a primitive Hecke eigenform relative to the subgroup Gamma_0(n)."
			],
			"reference": [
				"H. Cohen, Number Theory. Vol. II. Analytic and Modern Tools. Springer, 2007, pp. 496-497.",
				"Toshitsune Miyake, Modular Forms, Springer-Verlag, 1989. See Table A."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A127788/b127788.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"E. Halberstadt and A. Kraus, \u003ca href=\"https://www.researchgate.net/publication/243111985_Courbes_de_Fermat_resultats_et_problemes\"\u003eCourbes de Fermat: résultats et problèmes\u003c/a\u003e, J. Reine Angew. Math. 548 (2002) 167-234. [_Steven Finch_, Mar 27 2009}",
				"Xian-Jin Li, \u003ca href=\"http://arXiv.org/abs/math/0403148\"\u003eAn arithmetic formula for certain coefficients of the Euler product of Hecke polynomials\u003c/a\u003e, arXiv:math/0403148 [math.NT], 2004; J. Number Theory 113 (2005) 175-200. See Formula (5.8).",
				"G. Martin, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2004.10.009\"\u003eDimensions of the spaces of cusp forms and newforms on Gamma_0(N) and Gamma_1(N)\u003c/a\u003e, J. Numb. Theory 112 (2005) 298-331. [_Steven Finch_, Mar 27 2009]"
			],
			"formula": [
				"a(n) = A001617(n) - sum a(m)*d(n/m), where the summation is over all divisors 1 \u003c m \u003c n of n and d is the divisor function."
			],
			"example": [
				"a(p) = A001617(p) for any prime p.",
				"G.f. = x^11 + x^14 + x^15 + x^17 + x^19 + x^20 + x^21 + 2*x^23 + x^24 + ..."
			],
			"maple": [
				"seq( g0star(2,N),N=1..80); # using the source in A063195 - _R. J. Mathar_, Jul 15 2015"
			],
			"mathematica": [
				"A001617[n_] := If[n \u003c 1, 0, 1 + Sum[MoebiusMu[d]^2 n/d/12 - EulerPhi[GCD[d, n/d]]/2, {d, Divisors@n}] - Count[(#^2 - # + 1)/n \u0026 /@ Range[n], _?IntegerQ]/3 - Count[(#^2 + 1)/n \u0026 /@ Range[n], _?IntegerQ]/4]; a[n_ /; n \u003c 10] = 0; a[n_] := a[n] =  A001617[n] - Sum[a[m]*DivisorSigma[0, n/m], {m, Divisors[n][[2 ;; -2]]}]; Table[a[n], {n, 1, 102}] (* _Jean-François Alcover_, Sep 07 2015, A001617 code due to _Michael Somos_ *)"
			],
			"program": [
				"(PARI) {a(n) = my(v = [1, 3, 4, 6], A, p, e); if( n\u003c1, 0, A = factor(n); for( k=1, matsize(A)[1], [p, e] = A[k,]; v[1] *= if( e==1, p-1, e==2, p^2-p-1, p^(e-3) * (p+1) * (p-1)^2); v[2] *= if( p==2, (e==3) - (e\u003c3), e==1, kronecker(-4, p) - 1, e==2, -kronecker(-4, p)); v[3] *= if( p==3, (e==3) - (e\u003c3), e==1, kronecker(-3, p) - 1, e==2, -kronecker(-3, p)); v[4] *= if( e%2, 0, e==2, p-2, p^(e/2-2) * (p-1)^2)); moebius(n) + (v[1] - v[2] - v[3] - v[4]) / 12 )}; /* _Michael Somos_, Jun 06 2015 */"
			],
			"xref": [
				"Cf. A001617, A116563."
			],
			"keyword": "nonn",
			"offset": "1,23",
			"author": "_Steven Finch_, Apr 04 2007",
			"references": 3,
			"revision": 22,
			"time": "2019-08-22T12:24:02-04:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}