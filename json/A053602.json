{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A053602",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 53602,
			"data": "0,1,1,2,1,3,2,5,3,8,5,13,8,21,13,34,21,55,34,89,55,144,89,233,144,377,233,610,377,987,610,1597,987,2584,1597,4181,2584,6765,4181,10946,6765,17711,10946,28657,17711,46368,28657,75025,46368,121393,75025",
			"name": "a(n) = a(n-1) - (-1)^n*a(n-2), a(0)=0, a(1)=1.",
			"comment": [
				"If b(0)=0, b(1)=1 and b(n) = b(n-1) + (-1)^n*b(n-2), then a(n) = b(n+3). - _Jaume Oliver Lafont_, Oct 03 2009",
				"a(n) is the number of palindromic compositions of n-1 into parts of 1 and 2. a(7) = 5 because we have 2+2+2, 2+1+1+2, 1+2+2+1, 1+1+2+1+1, 1+1+1+1+1+1. - _Geoffrey Critzer_, Mar 17 2014",
				"a(n) is the number of palindromic compositions of n into odd parts (the corresponding generating function follows easily from Theorem 1.2 of the Hoggatt et al. reference). Example: a(7) = 5 because we have 7, 1+5+1, 3+1+3, 1+1+3+1+1, 1+1+1+1+1+1+1. - _Emeric Deutsch_, Aug 16 2016",
				"The ratio of a(n)/a(n-1) oscillates between phi-1 and phi+1 as n tends to infinity, where phi is golden ratio (A001622). - _Waldemar Puszkarz_, Oct 10 2017"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A053602/b053602.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Krithnaswami Alladi and V. E. Hoggatt, Jr. \u003ca href=\"http://www.fq.math.ca/Scanned/13-3/alladi1.pdf\"\u003eCompositions with Ones and Twos\u003c/a\u003e, Fibonacci Quarterly, 13 (1975), 233-239. - _Ron Knott_, Oct 29 2010",
				"A. R. Ashrafi, J. Azarija, K. Fathalikhani, S. Klavzar, et al., \u003ca href=\"http://www.fmf.uni-lj.si/~klavzar/preprints/Fib-Luc-orbits-August-11-2014.pdf\"\u003eOrbits of Fibonacci and Lucas cubes, dihedral transformations, and asymmetric strings\u003c/a\u003e, 2014.",
				"V. E. Hoggatt, Jr., and Marjorie Bicknell, \u003ca href=\"http://www.fq.math.ca/Scanned/13-4/hoggatt1.pdf\"\u003ePalindromic compositions\u003c/a\u003e, Fibonacci Quart., Vol. 13(4), 1975, pp. 350-356.",
				"M. A. Nyblom, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL16/Nyblom/nyblom13.html\"\u003eCounting Palindromic Binary Strings Without r-Runs of Ones\u003c/a\u003e, J. Int. Seq. 16 (2013) #13.8.7",
				"\u003ca href=\"/index/Tu#2wis\"\u003eIndex entries for two-way infinite sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,1,0,1)"
			],
			"formula": [
				"G.f.: x*(1 + x + x^2)/(1 - x^2 - x^4).",
				"a(n) = a(n-2) + a(n-4).",
				"a(2n) = F(n), a(2n-1) = F(n+1) where F() is Fibonacci sequence.",
				"a(3)=1, a(4)=2, a(n+2) = a(n+1) + sign(a(n) - a(n+1))*a(n), n \u003e 4. - _Benoit Cloitre_, Apr 08 2002",
				"a(n) = A079977(n-1) + A079977(n-2) + A079977(n-3), n \u003e 2. - _Ralf Stephan_, Apr 26 2003",
				"a(0) = 0, a(1) = 1; a(2n) = a(2n-1) - a(2n-2); a(2n+1) = a(2n) + a(2n-1). - _Amarnath Murthy_, Jul 21 2005"
			],
			"maple": [
				"a[0] := 0: a[1] := 1: for n from 2 to 60 do a[n] := a[n-1]-(-1)^n*a[n-2] end do: seq(a[n], n = 0 .. 50); # _Emeric Deutsch_, Oct 09 2017"
			],
			"mathematica": [
				"nn=50;CoefficientList[Series[x (1+x+x^2)/(1-x^2-x^4),{x,0,nn}],x] (* _Geoffrey Critzer_, Mar 17 2014 *)",
				"LinearRecurrence[{0,1,0,1},{0,1,1,2},60] (* _Harvey P. Dale_, Nov 07 2016 *)",
				"RecurrenceTable[{a[0]==0, a[1]==1, a[n]==a[n-1]-(-1)^n a[n-2]}, a, {n, 50}] (* _Vincenzo Librandi_, Oct 10 2017 *)",
				"a={0,1}; Do[AppendTo[a, a[[-1]]-(-1)^(Length[a])a[[-2]]], {49}];a (* _Waldemar Puszkarz_, Oct 10 2017 *)"
			],
			"program": [
				"(PARI) a(n)=fibonacci(n\\2+n%2*2)",
				"(MAGMA) I:=[0,1,1,2]; [n le 4 select I[n] else Self(n-2)+Self(n-4): n in [1..50]]; // _Vincenzo Librandi_ Oct 10 2017"
			],
			"xref": [
				"a(3-n) = A051792(n). Cf. A000045."
			],
			"keyword": "nonn,easy",
			"offset": "0,4",
			"author": "_Michael Somos_, Jan 17 2000",
			"references": 15,
			"revision": 61,
			"time": "2021-06-10T20:15:48-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}