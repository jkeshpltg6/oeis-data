{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A235491",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 235491,
			"data": "0,1,4,9,2,16,7,6,25,3,61,26,17,14,13,115,5,12,359,119,67,47,43,36,791,8,11,41,3017,81,811,407,247,227,179,7525,23,38,37,221,34015,27,503,22,7765,3509,1943,21,1777,1333,93625,97,193,146,181,1717,486721,121,4493,91,96839,10,40217,20813,89",
			"name": "Self-inverse permutation of natural numbers: complementary pair ludic/nonludic numbers (A003309/A192607) entangled with the same pair in the opposite order, nonludic/ludic. See Formula.",
			"comment": [
				"The permutation is self-inverse (an involution), meaning that a(a(n)) = n for all n."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A235491/b235491.txt\"\u003eTable of n, a(n) for n = 0..74\u003c/a\u003e (The terms after a(32) were computed with the help of 100000 term b-file uploaded by Donovan Johnson for A003309.)",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(0)=0, a(1)=1, and for n \u003e 1, if n is k-th ludic number (i.e., n = A003309(k)), then a(n) = nonludic(a(k-1)); otherwise, when n is k-th nonludic number (i.e., n = A192607(k)), then a(n) = ludic(a(k)+1), where ludic numbers are given by A003309, and nonludic numbers by A192607.",
				"a(0)=0, a(1)=1, and for n \u003e 1, if A192490(n)=1 (n is ludic) a(n) = A192607(a(A192512(n)-1)); otherwise (n is nonludic), a(n) = A003309(1+(a(A236863(n))))."
			],
			"example": [
				"For n=2, with 2 being the second ludic number (= A003309(4)), the value is computed as nonludic(a(2-1)) = nonludic(a(1)) = 4, the first nonludic number, thus a(2) = 4.",
				"For n=5, with 5 being the fourth ludic number (= A003309(4)), the value is computed as nonludic(a(4-1)) = nonludic(a(3)) = nonludic(9) = 16, thus a(5) = 16.",
				"For n=6, with 6 being the second nonludic number (= A192607(2)), the value is computed as ludic(a(2)+1) = ludic(4+1) = ludic(5) = 7, thus a(6) = 7."
			],
			"program": [
				"(Scheme, with memoization-macro definec from _Antti Karttunen_'s IntSeq-library)",
				"(definec (A235491 n) (cond ((\u003c n 2) n) ((= 1 (A192490 n)) (A192607 (A235491 (- (A192512 n) 1)))) (else (A003309 (+ 1 (A235491 (A236863 n)))))))"
			],
			"xref": [
				"Cf. A236854 (a similar permutation constructed from prime and composite numbers).",
				"Cf. A237126/A237427 (entanglement permutations between ludic/nonludic \u003c-\u003e odd/even numbers).",
				"Cf. also A003309, A192607, A192490, A192512, A236863."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Antti Karttunen_, Feb 07 2014",
			"references": 7,
			"revision": 17,
			"time": "2014-02-10T08:16:18-05:00",
			"created": "2014-02-10T08:16:18-05:00"
		}
	]
}