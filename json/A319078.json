{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A319078",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 319078,
			"data": "1,2,-4,-8,6,8,-8,0,12,10,-8,-24,8,8,-16,0,6,16,-12,-24,24,16,-8,0,24,10,-24,-32,0,24,-16,0,12,16,-16,-48,30,8,-24,0,24,32,-16,-24,24,24,-16,0,8,18,-28,-48,24,24,-32,0,48,16,-8,-72,0,24,-32,0,6,32",
			"name": "Expansion of phi(-q) * phi(q)^2 in powers of q where phi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of eta(q^2)^9 / (eta(q)^2 * eta(q^4)^4) in powers of q.",
				"Expansion of phi(q) * phi(-q^2)^2 = phi(-q^2)^4 / phi(-q) in powers of q.",
				"Euler transform of period 4 sequence [2, -7, 2, -3, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (16 t)) = 2^(11/2) (t/i)^(3/2) g(t) where q = exp(2 Pi i t) and g() is the g.f. for A045834.",
				"G.f. Product_{k\u003e0}  (1 - x^k)^3 * (1 + x^k)^5 / (1 + x^(2*k))^4.",
				"a(n) = (-1)^n * A212885(n) = A083703(2*n) = A080965(2*n).",
				"a(4*n) = a(n) * -A132429(n + 2) where A132429 is a period 4 sequence.",
				"a(4*n) = A005875(n). a(4*n + 1) = 2 * A045834(n). a(4*n + 2) = -4 * A045828(n).",
				"a(8*n) = A004015(n). a(8*n + 1) = 2 * A213022(n). a(8*n + 2) = -4 * A213625(n). a(8*n + 3) = -8 * A008443(n). a(8*n + 4) = A005887(n). a(8*n + 5) = 2 * A004024(n). a(8*n + 6) = -8 * A213624(n). a(8*n + 7) = 0."
			],
			"example": [
				"G.f. = 1 + 2*x - 4*x^2 - 8*x^3 + 6*x^4 + 8*x^5 - 8*x^6 + 12*x^8 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 4, 0, q] EllipticTheta[ 3, 0, q]^2, {q, 0, n}];",
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 3, 0, q] EllipticTheta[ 4, 0, q^2]^2, {q, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^9 / (eta(x + A)^2 * eta(x^4 + A)^4), n))};",
				"(MAGMA) A := Basis( ModularForms( Gamma0(16), 3/2), 66); A[1] + 2*A[2] - 4*A[3] - 8*A[4];"
			],
			"xref": [
				"Cf. A004015, A004024, A005887, A008443, A045834, A083703, A080965, A132429, A212885, A213624."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Sep 09 2018",
			"references": 0,
			"revision": 5,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2018-09-09T13:30:02-04:00"
		}
	]
}