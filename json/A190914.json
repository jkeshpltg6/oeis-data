{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A190914",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 190914,
			"data": "5,0,6,3,18,10,57,42,178,165,566,616,1821,2236,5914,7963,19362,27982,63813,97394,211458,336633,703786,1157544,2350597,3964960,7872702,13541691,26425522,46147178,88853297,156994354,299165378,533410837,1008343310,1810544592,3401446413,6140811708,11481472994,20815538227",
			"name": "Expansion of ( 5-9*x^2-2*x^3 ) / ( (1+x-x^2)*(1-x-x^2-x^3) ).",
			"comment": [
				"The sequence ..., 14, 29, 10, 2, 9, 2, 0, [5], 0, 6, 3, 18, 10, 57, 42, ...",
				"(the number in square brackets at index 0) equals the trace of:",
				"[ 0 0 0 0-1 ]",
				"[ 1 0 0 0 0 ]",
				"[ 0 1 0 0 1 ]^(+n)",
				"[ 0 0 1 0 3 ]",
				"[ 0 0 0 1 0 ]",
				"or",
				"[ 0 0 0 0-1 ]",
				"[ 1 0 0 0 0 ]",
				"[ 0 1 0 0 3 ]^(-n)",
				"[ 0 0 1 0 1 ]",
				"[ 0 0 0 1 0 ]",
				"Its characteristic polynomial is (x^2 +/- x - 1) * (x^3 -/+ x^2 -/+ x - 1); these factors are fibonacci and tribonacci polynomials.  The ratio of negative terms approaches the golden ratio; the ratio of positive terms approaches the tribonacci constant.",
				"Prime numbers p divide a(+p) and a(-p), as the trace of a matrix M^p (mod p) is constant.",
				"Nonprimes c very rarely divide a(+c) and a(-c) simultaneously.  The only known dual pseudoprime in the sequence is 1.",
				"The distribution of residues induces gaps between pseudoprimes having roughly the size of c.  For example, after 1034881 there is a gap of more than one million terms without either variety of pseudoprime.",
				"Pseudoprimes appear limited to squared primes and squarefree numbers with three or more prime factors.  11 and 13 are more common than other factors.",
				"Positive pseudoprimes: c | a(+c)",
				"----------------------------------------------",
				"1",
				"3481. . . . 59^2",
				"17143 . . . 7 31 79",
				"105589. . . 11 29 331",
				"635335. . . 5 283 449",
				"2992191 . . 3 29 163 211",
				"3659569 . . 1913^2",
				"Negative pseudoprimes: c | a(-c)",
				"----------------------------------------------",
				"1",
				"9 . . . . . 3^2",
				"806 . . . . 2 13 31",
				"1419. . . . 3 11 43",
				"6241. . . . 79^2",
				"6721. . . . 11 13 47",
				"12749 . . . 11 19 61",
				"21106 . . . 2 61 173",
				"38714 . . . 2 13 1489",
				"146689. . . 383^2",
				"649621. . . 7 17 53 103",
				"1034881 . . 41 43 587"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A190914/b190914.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,3,1,0,-1)."
			],
			"formula": [
				"a(n) = A061084(n+1) + A001644(n). - _R. J. Mathar_, Jun 06 2011"
			],
			"mathematica": [
				"LinearRecurrence[{0, 3, 1, 0, -1}, {5, 0, 6, 3, 18}, 40] (* _G. C. Greubel_, Apr 23 2019 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^40)); Vec((5-9*x^2-2*x^3)/((1+x-x^2)*(1-x-x^2-x^3))) \\\\ _G. C. Greubel_, Apr 23 2019",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Integers(), 40); Coefficients(R!( (5-9*x^2 -2*x^3)/((1+x-x^2)*(1-x-x^2-x^3)) )); // _G. C. Greubel_, Apr 23 2019",
				"(Sage) ((5-9*x^2-2*x^3)/((1+x-x^2)*(1-x-x^2-x^3))).series(x, 40).coefficients(x, sparse=False) # _G. C. Greubel_, Apr 23 2019"
			],
			"xref": [
				"Cf. A190913 (extended to negative indices), A000045, A000073, A001608, A000040, A005117, A125666."
			],
			"keyword": "nonn",
			"offset": "0,1",
			"author": "_Reikku Kulon_, May 23 2011",
			"references": 2,
			"revision": 12,
			"time": "2019-04-23T23:59:41-04:00",
			"created": "2011-06-07T17:19:08-04:00"
		}
	]
}