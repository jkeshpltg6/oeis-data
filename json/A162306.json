{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A162306",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 162306,
			"data": "1,1,2,1,3,1,2,4,1,5,1,2,3,4,6,1,7,1,2,4,8,1,3,9,1,2,4,5,8,10,1,11,1,2,3,4,6,8,9,12,1,13,1,2,4,7,8,14,1,3,5,9,15,1,2,4,8,16,1,17,1,2,3,4,6,8,9,12,16,18,1,19,1,2,4,5,8,10,16,20,1,3,7,9,21,1,2,4,8,11,16,22,1,23",
			"name": "Irregular triangle in which row n contains the numbers \u003c= n whose prime factors are a subset of prime factors of n.",
			"comment": [
				"Row n begins with 1, ends with n, and has A010846(n) terms.",
				"From _Michael De Vlieger_, Jul 08 2014: (Start)",
				"Prime p has {1, p} and A010846(p) = 2.",
				"Prime power p^e has {1, p, ..., p^e} and A010846(p^e) = A000005(p^e) = e + 1.",
				"Composite c that are not prime powers have A010846(c) = A000005(c) + A243822(c), where A243822(c) is nonzero positive, since the minimum prime divisor p of c produces at least one semidivisor (e.g., p^2 \u003c c). Thus these have the set of divisors of c and at least one semidivisor p^2. For squareful c that are not prime powers, p^2 may divide c, but p^3 does not. The minimum squareful c = 12, 2^3 does not divide 12 yet is less than 12 and is a product of the minimum prime divisor of 12. All other even squareful c admit a power of 2 that does not divide c, since there must be another prime divisor q \u003e 2. (end)",
				"Numbers 1 \u003c= k \u003c= n such that (floor(n^k/k) - floor((n^k - 1)/k)) = 1. - _Michael De Vlieger_, May 26 2016",
				"Numbers 1 \u003c= k \u003c= n such that k | n^e with e \u003e= 0. - _Michael De Vlieger_, May 29 2018"
			],
			"link": [
				"T. D. Noe and Michael De Vlieger, \u003ca href=\"/A162306/b162306.txt\"\u003eRows n = 1..1000 of triangle, flattened\u003c/a\u003e  (first rows n=1..200 from T. D. Noe)"
			],
			"formula": [
				"Union of A027750 and nonzero terms of A272618."
			],
			"example": [
				"n =  6: {1, 2, 3, 4, 6}.",
				"n =  7: {1, 7}.",
				"n =  8: {1, 2, 4, 8}.",
				"n =  9: {1, 3, 9}.",
				"n = 10: {1, 2, 4, 5, 8, 10}.",
				"n = 11: {1, 11}.",
				"n = 12: {1, 2, 3, 4, 6, 8, 9, 12}."
			],
			"maple": [
				"A:= proc(n) local F, S, s, j, p;",
				"  F:= numtheory:-factorset(n);",
				"  S:= {1};",
				"  for p in F do",
				"    S:= {seq(seq(s*p^j, j=0..floor(log[p](n/s))), s=S)}",
				"  od;",
				"  S",
				"end proc; map(op,[seq(A(n), n=1..100)]); # _Robert Israel_, Jul 15 2014"
			],
			"mathematica": [
				"pf[n_] := If[n==1, {}, Transpose[FactorInteger[n]][[1]]]; SubsetQ[lst1_, lst2_] := Intersection[lst1,lst2]==lst1; Flatten[Table[pfn=pf[n]; Select[Range[n], SubsetQ[pf[ # ],pfn] \u0026], {n,27}]]",
				"(* Second program: *)",
				"Table[Select[Range@ n, PowerMod[n, Floor@ Log2@ n, #] == 0 \u0026], {n,",
				"   23}] // Flatten (* _Michael De Vlieger_, May 29 2018 *)"
			],
			"xref": [
				"Cf. A010846 (number of terms in row n), A027750 (terms k that divide n), A243103 (product of terms in row n), A244974 (sum of terms in row n), A272618 (terms k that do not divide n)."
			],
			"keyword": "nonn,tabf,look",
			"offset": "1,3",
			"author": "_T. D. Noe_, Jun 30 2009",
			"references": 22,
			"revision": 36,
			"time": "2020-03-30T20:59:20-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}