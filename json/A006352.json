{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006352",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6352,
			"id": "M5145",
			"data": "1,-24,-72,-96,-168,-144,-288,-192,-360,-312,-432,-288,-672,-336,-576,-576,-744,-432,-936,-480,-1008,-768,-864,-576,-1440,-744,-1008,-960,-1344,-720,-1728,-768,-1512,-1152,-1296,-1152,-2184,-912,-1440,-1344,-2160,-1008,-2304,-1056,-2016,-1872,-1728",
			"name": "Coefficients in expansion of Eisenstein series E_2 (also called E_1 or G_2).",
			"comment": [
				"Ramanujan's Eisenstein series: P(q) (see A006352), Q(q) (A004009), R(q) (A013973).",
				"The series Q(q), R(q) are modular forms, but P(q) is not. - _Michael Somos_, May 18 2017"
			],
			"reference": [
				"R. C. Gunning, Lectures on Modular Forms. Princeton Univ. Press, Princeton, NJ, 1962, p. 53.",
				"N. Koblitz, Introduction to Elliptic Curves and Modular Forms, Springer-Verlag, 1984, see pp. 111 and 113.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"Zagier, Don. \"Elliptic modular forms and their applications.\" The 1-2-3 of modular forms. Springer Berlin Heidelberg, 2008. 1-103. See p. 19, Eq. (17)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A006352/b006352.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"F. Beukers, \u003ca href=\"http://dx.doi.org/10.1016/0022-314X(87)90025-4\"\u003eAnother congruence for the Apéry numbers\u003c/a\u003e, J. Number Theory 25 (1987), no. 2, 201-210.",
				"J. H. Bruinier and K. Ono, \u003ca href=\"http://www.aimath.org/news/partition/brunier-ono.pdf\"\u003eAlgebraic formulas for the coefficients of half-integral weight harmonic weak Maass forms\u003c/a\u003e",
				"Heng Huat Chan, Shaun Cooper, and Pee Choon Toh, \u003ca href=\"http://unimodular.net/archive/RamEisenstein.pdf\"\u003eRamanujan's Eisenstein series and powers of Dedekind's eta-function\u003c/a\u003e, Journal of the London Mathematical Society 75.1 (2007): 225-242. See P(q).",
				"M. Kaneko and D. Zagier, \u003ca href=\"http://www2.math.kyushu-u.ac.jp/~mkaneko/papers/atkin.pdf\"\u003eSupersingular j-invariants, hypergeometric series and Atkin's orthogonal polynomials\u003c/a\u003e, pp. 97-126 of D. A. Buell and J. T. Teitelbaum, eds., Computational Perspectives on Number Theory, Amer. Math. Soc., 1998",
				"H. Ochiai, \u003ca href=\"http://arXiv.org/abs/math-ph/9909023\"\u003eCounting functions for branched covers of elliptic curves and quasi-modular forms\u003c/a\u003e, arXiv:math-ph/9909023, 1999.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/EisensteinSeries.html\"\u003eEisenstein Series.\u003c/a\u003e",
				"\u003ca href=\"/index/Ed#Eisen\"\u003eIndex entries for sequences related to Eisenstein series\u003c/a\u003e"
			],
			"formula": [
				"a(n) = -24*sigma(n) = -24*A000203(n), for n\u003e0.",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2), A(x^3), A(x^6)) where f(u1, u2, u3, u6) = u1^2 + 4*u2^2 + 9*u3^2 + 36*u6^2 - 8*u1*u2 + 6*u1*u3 + 24*u2*u6 - 72*u3*u6. - _Michael Somos_, May 29 2005",
				"G.f.: 1 - 24*sum(k\u003e=1, k*x^k/(1 - x^k)).",
				"G.f.: 1 + 24 *x*deriv(eta(x))/eta(x) where eta(x) = prod(n\u003e=1, 1-x^n); (cf. A000203). - _Joerg Arndt_, Sep 28 2012",
				"G.f.: 1 - 24*x/(1-x) + 48*x^2/(Q(0) -  2*x^2 + 2*x), where Q(k)= (2*x^(k+2) - x - 1)*k - 1 - 2*x + 3*x^(k+2) - x*(k+1)*(k+3)*(1-x^(k+2))^2/Q(k+1); (continued fraction). - _Sergei N. Gladkovskii_, May 16 2013",
				"G.f.: q*Delta'/Delta where Delta is the generating function of Ramanujan's tau function (A000594). - _Seiichi Manyama_, Jul 15 2017"
			],
			"example": [
				"G.f. = 1 - 24*x - 72*x^2 - 96*x^3 - 168*x^4 - 144*x^5 - 288*x^6 + ..."
			],
			"maple": [
				"E := proc(k) local n,t1; t1 := 1-(2*k/bernoulli(k))*add(sigma[k-1](n)*q^n,n=1..60); series(t1,q,60); end; E(2);"
			],
			"mathematica": [
				"a[n_] := -24*DivisorSigma[1, n]; a[0] = 1; Table[a[n], {n, 0, 46}] (* _Jean-François Alcover_, Dec 12 2012 *)",
				"a[ n_] := If[ n \u003c 1, Boole[n == 0], -24 DivisorSigma[ 1, n]]; (* _Michael Somos_, Apr 08 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n==0, -24 * sigma(n))}; /* _Michael Somos_, Apr 09 2003 */",
				"(Python)",
				"from sympy import divisor_sigma",
				"def a(n): return 1 if n == 0 else -24 * divisor_sigma(n)",
				"[a(n) for n in range(51)]  # _Indranil Ghosh_, Jul 15 2017"
			],
			"xref": [
				"Cf. A006352 (E_2), A004009 (E_4), A013973 (E_6), A008410 (E_8), A013974 (E_10), A029828 (E_12), A058550 (E_14), A029829 (E_16), A029830 (E_20), A029831 (E_24).",
				"Cf. A000594 (Delta), A076835, A145155 (Delta')."
			],
			"keyword": "sign,easy,nice",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 108,
			"revision": 72,
			"time": "2019-11-15T07:46:40-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}