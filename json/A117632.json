{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A117632",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 117632,
			"data": "1,2,2,3,4,2,3,4,4,3,4,4,5,6,4,5,6,6,7,6,2,3,4,4,5,6,4,3,4,5,5,6,6,5,6,4,5,6,6,7,8,4,5,6,4,5,6,6,5,6,6,7,8,8,3,4,5,5,6,7,5,6,6,7,6,4,5,6,6,7,8,6,7,8,8,5,6,4,5,6,6,7,6,6,7,8,6,7,8,8,5,6,7,7,8,9,7,8,6,7,8,8",
			"name": "Number of 1's required to build n using {+,T} and parentheses, where T(i) = i*(i+1)/2.",
			"comment": [
				"This problem has the optimal substructure property."
			],
			"reference": [
				"W. A. Beyer, M. L. Stein and S. M. Ulam, The Notion of Complexity. Report LA-4822, Los Alamos Scientific Laboratory of the University of California, Los Alamos, NM, 1971.",
				"R. K. Guy, Unsolved Problems Number Theory, Sect. F26."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A117632/b117632.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"W. A. Beyer, M. L. Stein and S. M. Ulam, \u003ca href=\"/A003037/a003037.pdf\"\u003eThe Notion of Complexity\u003c/a\u003e. Report LA-4822, Los Alamos Scientific Laboratory of the University of California, Los Alamos, NM, December 1971. [Annotated scanned copy]",
				"R. K. Guy, \u003ca href=\"http://www.jstor.org/stable/2323338\"\u003eSome suspiciously simple sequences\u003c/a\u003e, Amer. Math. Monthly 93 (1986), 186-190; 94 (1987), 965; 96 (1989), 905.",
				"Ed Pegg, Jr., \u003ca href=\"http://library.wolfram.com/infocenter/MathSource/5175/\"\u003eInteger Complexity.\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/IntegerComplexity.html\"\u003eInteger Complexity.\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Optimal_substructure\"\u003eOptimal substructure.\u003c/a\u003e"
			],
			"example": [
				"a(1) = 1 because \"1\" has a single 1.",
				"a(2) = 2 because \"1+1\" has two 1's.",
				"a(3) = 2 because 3 = T(1+1) has two 1's.",
				"a(6) = 2 because 6 = T(T(1+1)).",
				"a(10) = 3 because 10 = T(T(1+1)+1).",
				"a(12) = 4 because 12 = T(T(1+1)) + T(T(1+1)).",
				"a(15) = 4 because 15 = T(T(1+1)+1+1)).",
				"a(21) = 2 because 21 = T(T(T(1+1))).",
				"a(28) = 3 because 28 = T(T(T(1+1))+1).",
				"a(55) = 3 because 55 = T(T(T(1+1)+1))."
			],
			"maple": [
				"a:= proc(n) option remember; local m; m:= floor (sqrt (n*2));",
				"      if n\u003c3 then n",
				"    elif n=m*(m+1)/2 then a(m)",
				"    else min (seq (a(i)+a(n-i), i=1..floor(n/2)))",
				"      fi",
				"    end:",
				"seq (a(n), n=1..110);  # _Alois P. Heinz_, Jan 05 2011"
			],
			"mathematica": [
				"a[n_] := a[n] = Module[{m = Floor[Sqrt[n*2]]}, If[n \u003c 3, n, If[n == m*(m + 1)/2, a[m], Min[Table[a[i] + a[n - i], {i, 1, Floor[n/2]}]]]]];",
				"Array[a, 110] (* _Jean-François Alcover_, Jun 02 2018, from Maple *)"
			],
			"xref": [
				"See also A023361 = number of compositions into sums of triangular numbers, A053614 = numbers that are not the sum of triangular numbers. Iterated triangular numbers: A050536, A050542, A050548, A050909, A007501.",
				"Cf. A000217, A005245, A005520, A003313, A076142, A076091, A061373, A005421, A023361, A053614, A064097, A025280, A003037, A099129, A050536, A050542, A050548, A050909, A007501."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Jonathan Vos Post_, Apr 08 2006",
			"ext": [
				"I do not know how many of these entries have been proved to be minimal. - _N. J. A. Sloane_, Apr 15 2006",
				"Corrected and extended by _Alois P. Heinz_, Jan 05 2011"
			],
			"references": 1,
			"revision": 27,
			"time": "2018-06-02T14:18:13-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}