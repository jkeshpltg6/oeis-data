{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A231692",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 231692,
			"data": "0,1,1,1,5,13,1,27,19,451,199,4709,2399,3467,29207,5183,55411,221267,300649,1628251,5508127,259001,762881,6460903,5694791,11476403,27820203,326206681,5151783667,69088293143,146724611903,2219373406193,8951357840311,4575492601111,328329280711,4454145077671",
			"name": "Define a sequence of rationals by f(0)=0, thereafter f(n)=f(n-1)-1/n if that is \u003e= 0, otherwise f(n)=f(n-1)+1/n; a(n) = numerator of f(n).",
			"comment": [
				"It is conjectured that the terms of the {f(n)} sequence are distinct.",
				"If that is true, then the {f(n)} sequence is a fractional analog of Recamán's sequence A005132.",
				"The denominators of {f(n)} form A231693 (a non-monotonic sequence).",
				"From _Don Reble_, Nov 16 2013: (Start)",
				"Here is a proof that the f(n) are distinct: Suppose not. Then the difference between the terms (which is zero) is a number of the form +- 1/(a+1) +- 1/(a+2) +- 1/(a+3) +- ... +- 1/(a+n).",
				"Consider any harmonic sum",
				"   S = +- 1/(a+1) +- 1/(a+2) +- 1/(a+3) +- ... +- 1/(a+n)",
				"  where one puts any sign on any term, and there is at least one term. Let G be the LCM of the denominator(s). Then for any denominator D, G/D is an integer, and G*S = sum(+- G/D_i) is an integer. Let E be the highest power of two that divides G. Then there is only one multiple of E among the denominators. (If there were two, they would be consecutive multiples of E, and one would be divisible by 2*E.) Call that denominator F. So (+- G/F) is an odd integer, and for all other denominators D, (+- G/D) is an even integer. Therefore G*S is odd, therefore not zero, so S is not zero. (End)"
			],
			"reference": [
				"David Wilson, Posting to Sequence Fans Mailing List, Nov 14 2013."
			],
			"link": [
				"David W. Wilson and R. Zumkeller, \u003ca href=\"/A231692/b231692.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e"
			],
			"example": [
				"0, 1, 1/2, 1/6, 5/12, 13/60, 1/20, 27/140, 19/280, 451/2520, 199/2520, 4709/27720, ..."
			],
			"maple": [
				"f:=proc(n) option remember;",
				"if n=0 then 0 elif",
				"f(n-1) \u003e= 1/n then f(n-1)-1/n else f(n-1)+1/n; fi; end;"
			],
			"program": [
				"(PARI) s=0;vector(30,n,numerator(s-=(-1)^(n*s\u003c1)/n)) \\\\ - _M. F. Hasler_, Nov 15 2013",
				"(Haskell)",
				"a231692_list = map numerator $ 0 : wilson 1 0 where",
				"   wilson x y = y' : wilson (x + 1) y'",
				"                where y' = y + (if y \u003c 1 % x then 1 else -1) % x",
				"-- _Reinhard Zumkeller_, Nov 16 2013"
			],
			"xref": [
				"Cf. A231693, A005132."
			],
			"keyword": "nonn,frac",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_, Nov 15 2013, Nov 16 2013",
			"references": 3,
			"revision": 37,
			"time": "2017-10-19T10:41:58-04:00",
			"created": "2013-11-15T05:45:59-05:00"
		}
	]
}