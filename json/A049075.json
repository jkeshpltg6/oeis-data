{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A049075",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 49075,
			"data": "1,1,2,4,8,18,43,102,247,617,1564,4003,10355,27051,71225,188743,503111,1348301,3630294,9815159,26637436,72540432,198162708,542875096,1491126550,4105602719,11329408543,31328137525,86795258650,240898943969,669730499207,1864855943748",
			"name": "Eigensequence of a power series transformation.",
			"comment": [
				"Euler transform of a(n) - if( n%4, 0, a(n/2)) is sequence itself with offset 0."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A049075/b049075.txt\"\u003eTable of n, a(n) for n = 1..650\u003c/a\u003e"
			],
			"formula": [
				"G.f.: A(x) = x exp(A(x) - A(-x^2)/2 + A(x^3)/3 - A(-x^4)/4 + ...). Also A(x) = Sum_{n \u003e= 1} a(n)*x^n = x * Product_{n \u003e= 1} (1+(-x)^n)^((-1)^n*a(n)).",
				"G.f.: x prod_{n\u003e0} (1-x^(4n))^a(2n)/(1-x^n)^a(n).",
				"a(n) ~ c * d^n / n^(3/2), where d = 2.92045137601697174071599643..., c = 0.4299447159290328896620383... . - _Vaclav Kotesovec_, Aug 25 2014"
			],
			"example": [
				"x + x^2 + 2*x^3 + 4*x^4 + 8*x^5 + 18*x^6 + 43*x^7 + 102*x^8 + 247*x^9 + 617*x^10 + ..."
			],
			"maple": [
				"with(numtheory): etr:= proc(p) local b; b:= proc(n) option remember; if n=0 then 1 else (add(d*p(d), d=divisors(n)) +add(add(d*p(d), d=divisors(j)) *b(n-j), j=1..n-1))/n fi end end: b:= etr(n-\u003e a(n) -`if`(modp(n,4)\u003c\u003e0, 0,a(n/2))): a:= n-\u003e b(n-1): seq(a(n), n=1..40);  # _Alois P. Heinz_, Sep 06 2008"
			],
			"mathematica": [
				"s[ n_, k_ ] := s[ n, k ]=a[ n+1-k ]+If[ n\u003c2k, 0, -s[ n-k, k ](-1)^k ]; a[ 1 ]=1; a[ n_ ] := a[ n ]=Sum[ a[ i ]s[ n-1, i ]i, {i, 1, n-1} ]/(n-1); Table[ a[ i ], {i, 1, 30} ]"
			],
			"program": [
				"(PARI) {a(n) = local(A=x); if( n\u003c1, 0, for( k=1, n-1, A *= (1 + (-x)^k + x*O(x^n))^((-1)^k * polcoeff(A, k))); polcoeff(A, n))}"
			],
			"xref": [
				"Cf. A045648, A000081, A004111."
			],
			"keyword": "nonn,eigen",
			"offset": "1,3",
			"author": "_Michael Somos_, Aug 08 1999",
			"references": 11,
			"revision": 20,
			"time": "2014-12-19T20:36:19-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}