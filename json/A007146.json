{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007146",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7146,
			"id": "M2909",
			"data": "1,0,1,3,11,60,502,7403,197442,9804368,902818087,153721215608,48443044675155,28363687700395422,30996524108446916915,63502033750022111383196,244852545022627009655180986,1783161611023802810566806448531,24603891215865809635944516464394339",
			"name": "Number of unlabeled simple connected bridgeless graphs with n nodes.",
			"comment": [
				"Also unlabeled simple graphs with spanning edge-connectivity \u003e= 2. The spanning edge-connectivity of a set-system is the minimum number of edges that must be removed (without removing incident vertices) to obtain a set-system that is disconnected or covers fewer vertices. - _Gus Wiseman_, Sep 02 2019"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A007146/b007146.txt\"\u003eTable of n, a(n) for n = 1..40\u003c/a\u003e (terms 1..22 from R. J. Mathar)",
				"P. Hanlon and R. W. Robinson, \u003ca href=\"http://dx.doi.org/10.1016/0095-8956(82)90048-X\"\u003eCounting bridgeless graphs\u003c/a\u003e, J. Combin. Theory, B 33 (1982), 276-305, Table III.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/BridgelessGraph.html\"\u003eBridgeless Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ConnectedGraph.html\"\u003eConnected Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/SimpleGraph.html\"\u003eSimple Graph\u003c/a\u003e",
				"Gus Wiseman, \u003ca href=\"/A007146/a007146.png\"\u003eThe a(3) = 1 through a(5) = 11 connected bridgeless graphs.\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A001349(n) - A052446(n). - _Gus Wiseman_, Sep 02 2019"
			],
			"program": [
				"(PARI) \\\\ Translation of theorem 3.2 in Hanlon and Robinson reference. See A004115 for graphsSeries and A339645 for combinatorial species functions.",
				"cycleIndexSeries(n)={my(gc=sLog(graphsSeries(n)), gcr=sPoint(gc)); sSolve( gc + gcr^2/2 - sRaise(gcr,2)/2, x*sv(1)*sExp(gcr) )}",
				"NumUnlabeledObjsSeq(cycleIndexSeries(15)) \\\\ _Andrew Howroyd_, Dec 31 2020"
			],
			"xref": [
				"Cf. A005470 (number of simple graphs).",
				"Cf. A007145 (number of simple connected rooted bridgeless graphs).",
				"Cf. A052446 (number of simple connected bridged graphs).",
				"Cf. A263914 (number of simple bridgeless graphs).",
				"Cf. A263915 (number of simple bridged graphs).",
				"The labeled version is A095983.",
				"Row sums of A263296 if the first two columns are removed.",
				"BII-numbers of set-systems with spanning edge-connectivity \u003e= 2 are A327109.",
				"Graphs with non-spanning edge-connectivity \u003e= 2 are A327200.",
				"2-vertex-connected graphs are A013922.",
				"Cf. A000719, A001349, A002494, A261919, A327069, A327071, A327074, A327075, A327077, A327109, A327144, A327146."
			],
			"keyword": "nonn,nice",
			"offset": "1,4",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Reference gives first 22 terms."
			],
			"references": 29,
			"revision": 44,
			"time": "2021-11-07T15:28:01-05:00",
			"created": "1994-04-28T03:00:00-04:00"
		}
	]
}