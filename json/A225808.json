{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A225808",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 225808,
			"data": "1,9,16,36,81,81,100,144,256,169,225,324,361,625,144,256,324,441,324,361,441,625,256,576,729,784,576,729,900,961,1089,1296,484,625,784,900,484,441,576,729,784,900,1089,1089,1156,1369,625,784,729,900,1089,1369,1296,1600,900,961,1089",
			"name": "Values (Sum_{1\u003c=i\u003c=k} x_i)^2 = Sum_{1\u003c=i\u003c=k} x_i^3 for 1 \u003c= x_1 \u003c= x_2 \u003c=...\u003c= x_k ordered lexicographically according to (x1, x2,..., xk).",
			"comment": [
				"a(n) \u003c= k^4 where k is the size of the ordered tuple (x_1, x_2,..., x_k).",
				"This sequence is closed under multiplication, that is, if m and n are in this sequence, so is m*n."
			],
			"link": [
				"Balarka Sen, \u003ca href=\"/A225808/b225808.txt\"\u003eRows n = 1..10 of irregular triangle, flattened\u003c/a\u003e",
				"Edward Barbeau and Samer Seraj, \u003ca href=\"http://arxiv.org/abs/1306.5257\"\u003eSum of cubes is square of sum\u003c/a\u003e, arXiv:1306.5257 [math.NT], 2013.",
				"John Mason, \u003ca href=\"http://www.jstor.org/stable/3620469\"\u003eGeneralising 'sums of cubes equal to squares of sums'\u003c/a\u003e, The Mathematical Gazette 85:502 (2001), pp. 50-58.",
				"Alasdair McAndrew, \u003ca href=\"http://amca01.wordpress.com/2011/01/10/a-cute-result-relating-to-sums-of-cubes/\"\u003eA cute result relating to sums of cubes\u003c/a\u003e",
				"David Pagni, \u003ca href=\"http://www.jstor.org/stable/3620410\"\u003e82.27 An interesting number fact\u003c/a\u003e, The Mathematical Gazette 82:494 (1998), pp. 271-273.",
				"Balarka Sen, \u003ca href=\"/A225808/a225808.txt\"\u003eTable of rows, n = 1..10\u003c/a\u003e",
				"W. R. Utz, The Diophantine Equation (x_1 + x_2 + ... + x_n)^2 = x_1^3 + x_2^3 + ... + x_n^3, Fibonacci Quarterly 15:1 (1977), pp. 14, 16. \u003ca href=\"http://www.fq.math.ca/Scanned/15-1/utz-a.pdf\"\u003ePart 1\u003c/a\u003e, \u003ca href=\"http://www.fq.math.ca/Scanned/15-1/utz-b.pdf\"\u003epart 2\u003c/a\u003e."
			],
			"example": [
				"1;",
				"9, 16;",
				"36, 81;",
				"81, 100, 144, 256;",
				"169, 225, 324, 361, 625;",
				"144, 256, 324, 441, 324, 361, 441, 625, 256, 576, 729, 784, 576, 729, 900, 961, 1089, 1296;",
				"484, 625, 784, 900, 484, 441, 576, 729, 784, 900, 1089, 1089, 1156, 1369, 625, 784, 729, 900, 1089, 1369, 1296, 1600, 900, 961, 1089, 1600, 1296, 1600, 2025, 2401;"
			],
			"mathematica": [
				"row[n_] := Reap[Module[{v, m}, v = Table[1, {n}]; m = n^(4/3); While[ v[[-1]] \u003c m, v[[1]]++; If[v[[1]] \u003e m, For[i = 2, i \u003c= m, i++, If[v[[i]] \u003c m, v[[i]]++; For[j = 1, j \u003c= i - 1, j++, v[[j]] = v[[i]]]; Break[]]]]; If[Total[v^3] == Total[v]^2, Sow[Total[v]^2]]]]][[2, 1]];",
				"Array[row, 7] // Flatten (* _Jean-François Alcover_, Feb 23 2019, from PARI *)"
			],
			"program": [
				"(PARI) row(n)=my(v=vector(n,i,1),N=n^(4/3)); while(v[#v]\u003cN, v[1]++; if(v[1]\u003eN,for(i=2, N,if(v[i]\u003cN,v[i]++;for(j=1,i-1, v[j]=v[i]); break))); if(sum(i=1,n,v[i]^3)==sum(i=1,n,v[i])^2,print1(sum(i=1,n,v[i])^2\", \")))"
			],
			"xref": [
				"Cf. A158649, A055012, A118881."
			],
			"keyword": "nonn,tabf",
			"offset": "1,2",
			"author": "_Charles R Greathouse IV_, _Jimmy Zotos_, and _Balarka Sen_, Jul 29 2013",
			"references": 2,
			"revision": 36,
			"time": "2019-02-23T02:20:32-05:00",
			"created": "2013-08-02T15:29:49-04:00"
		}
	]
}