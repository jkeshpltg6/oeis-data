{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A061783",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 61783,
			"data": "229,239,241,257,269,271,277,281,439,443,463,467,479,499,613,641,653,661,673,677,683,691,811,823,839,863,881,20011,20029,20047,20051,20101,20161,20201,20249,20269,20347,20389,20399,20441,20477,20479,20507",
			"name": "Luhn primes: primes p such that p + (p reversed) is also a prime.",
			"comment": [
				"a(n) has an odd number of digits, as otherwise a(n) + reverse(a(n)) is a multiple of 11. For a(n) \u003e 10, a(n) is prime and thus odd, and therefore the first digit of a(n) is even as otherwise a(n) + reverse(a(n)) is even and composite. - _Chai Wah Wu_, Aug 19 2015",
				"See A072385 for the resulting primes p + reverse(p) = A056964(p). - _M. F. Hasler_, Sep 26 2019",
				"Named by Cira and Smarandache (2014) after Norman Luhn who noted the property of the prime 229 on the Prime Curios! website. - _Amiram Eldar_, Jun 05 2021"
			],
			"link": [
				"Harry J. Smith and Chai Wah Wu, \u003ca href=\"/A061783/b061783.txt\"\u003eTable of n, a(n) for n = 1..50598\u003c/a\u003e, giving all terms below 9*10^6 (The first 1000 terms from Harry J. Smith)",
				"Octavian Cira and Florian Smarandache, \u003ca href=\"https://uav.ro/applications/se/journal/index.php/TAMCS/article/view/112/90\"\u003eLuhn prime numbers\u003c/a\u003e, Theory and Applications of Mathematics \u0026 Computer Science, Vol. 5, No. 1 (2015), pp. 29-36; \u003ca href=\"http://fs.unm.edu/ScArt/LuhnPrimeNumbers.pdf\"\u003epreprint\u003c/a\u003e, 2014.",
				"G. L. Honaker, Jr. and Chris Caldwell, eds., \u003ca href=\"https://primes.utm.edu/curios/page.php?curio_id=2522\"\u003e229\u003c/a\u003e, Prime Curios!, November 19, 2001.",
				"Chai Wah Wu, \u003ca href=\"/A061783/a061783-big.zip\"\u003e3010506 terms\u003c/a\u003e, 11MB zipped file of all terms below 10^9."
			],
			"example": [
				"229 is a term since 229 is a prime and so is 229 + 922 = 1151."
			],
			"mathematica": [
				"Select[Prime[Range[3000]],PrimeQ[#+FromDigits[Reverse[IntegerDigits[#]]]]\u0026] (* _Harvey P. Dale_, Nov 27 2010 *)"
			],
			"program": [
				"(PARI) { n=0; forprime (p=2, 86843, x=p; r=0; while (x\u003e0, d=x-10*(x\\10); x\\=10; r=r*10 + d); if (isprime(p + r), write(\"b061783.txt\", n++, \" \", p)) ) } \\\\ _Harry J. Smith_, Jul 28 2009",
				"(PARI) select( is_A061783(p)=isprime(A056964(p)) \u0026\u0026 isprime(p), primes(8713)) \\\\  A056964(p)=p+fromdigits(Vecrev(digits(p))). There is no term with 4 digits or starting with an odd digit, i.e., no candidate between prime(168) = 997 and prime(2263) = 20011. Using primes up to prime(8713) = 89989 ensures the list of 5-digit terms is complete. - _M. F. Hasler_, Sep 26 2019",
				"(MAGMA) [NthPrime(n): n in [1..2400] | IsPrime(s) where s is NthPrime(n)+Seqint(Reverse(Intseq(NthPrime(n))))]; // _Bruno Berselli_, Aug 05 2013",
				"(Python)",
				"from sympy import isprime, prime",
				"A061783 = [prime(n) for n in range(1,10**5) if isprime(prime(n)+int(str(prime(n))[::-1]))] # _Chai Wah Wu_, Aug 14 2014"
			],
			"xref": [
				"Cf. A004086 (reverse), A004087 (primes reversed), A056964 (reverse \u0026 add), A072385, A086002 (similar, using \"rotate\" instead of \"reverse\")."
			],
			"keyword": "nonn,base,easy",
			"offset": "1,1",
			"author": "_Amarnath Murthy_, May 24 2001",
			"ext": [
				"Corrected and extended by _Patrick De Geest_, May 26 2001",
				"Cross-references added by _M. F. Hasler_, Sep 26 2019"
			],
			"references": 11,
			"revision": 53,
			"time": "2021-06-05T06:21:31-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}