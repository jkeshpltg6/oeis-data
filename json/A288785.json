{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A288785",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 288785,
			"data": "1,5,26,137,750,4307,25996,164825,1096217,7633650,55549664,421599778,3331027887,27349472297,232967157736,2055635993935,18762063976810,176896220650029,1720762736285790,17249873608817569,178010337967774511,1889129778601708612",
			"name": "Number of blocks of size \u003e= three in all set partitions of n.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A288785/b288785.txt\"\u003eTable of n, a(n) for n = 3..575\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Partition_of_a_set\"\u003ePartition of a set\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Bell(n+1) - Sum_{j=0..2} binomial(n,j) * Bell(n-j).",
				"a(n) = Sum_{j=0..n-3} binomial(n,j) * Bell(j)."
			],
			"example": [
				"a(4) = 5: 1234, 123|4, 124|3, 134|2, 1|234.",
				"a(5) = 26: 12345, 1234|5, 1235|4, 123|45, 123|4|5, 1245|3, 124|35, 124|3|5, 125|34, 12|345, 125|3|4, 1345|2, 134|25, 134|2|5, 135|24, 13|245, 135|2|4, 145|23, 14|235, 15|234, 1|2345, 1|234|5, 1|235|4, 145|2|3, 1|245|3, 1|2|345.",
				"a(6) = 137: 123456, 12345|6, 12346|5, ..., 123|456, 124|356, 125|346, 126|345, 134|256, 135|246, 136|245, 145|236, 146|235, 156|234, ..., 1|256|3|4, 1|2|356|4, 1|2|3|456."
			],
			"maple": [
				"b:= proc(n) option remember; `if`(n=0, 1, add(",
				"      b(n-j)*binomial(n-1, j-1), j=1..n))",
				"    end:",
				"g:= proc(n, k) option remember; `if`(n\u003ck, 0,",
				"      g(n, k+1) +binomial(n, k)*b(n-k))",
				"    end:",
				"a:= n-\u003e g(n, 3):",
				"seq(a(n), n=3..30);",
				"# second Maple program:",
				"b:= proc(n) option remember; `if`(n=0, [1, 0], add((p-\u003e p+[0,",
				"     `if`(j\u003e2, p[1], 0)])(b(n-j)*binomial(n-1, j-1)), j=1..n))",
				"    end:",
				"a:= n-\u003e b(n)[2]:",
				"seq(a(n), n=3..30);  # _Alois P. Heinz_, Jan 06 2022"
			],
			"mathematica": [
				"b[n_] := b[n] = If[n == 0, 1, Sum[b[n-j]*Binomial[n-1, j-1], {j, 1, n}]];",
				"g[n_, k_] := g[n, k] = If[n \u003c k, 0, g[n, k+1] + Binomial[n, k]*b[n - k]];",
				"a[n_] := g[n, 3];",
				"Table[a[n], {n, 3, 30}] (* _Jean-François Alcover_, May 28 2018, from Maple *)"
			],
			"xref": [
				"Column k=3 of A283424.",
				"Cf. A000110."
			],
			"keyword": "nonn,changed",
			"offset": "3,2",
			"author": "_Alois P. Heinz_, Jun 15 2017",
			"references": 2,
			"revision": 17,
			"time": "2022-01-06T14:56:08-05:00",
			"created": "2017-06-15T20:04:15-04:00"
		}
	]
}