{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A160891",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 160891,
			"data": "1,15,40,120,156,600,400,960,1080,2340,1464,4800,2380,6000,6240,7680,5220,16200,7240,18720,16000,21960,12720,38400,19500,35700,29160,48000,25260,93600,30784,61440,58560,78300,62400,129600,52060,108600,95200",
			"name": "a(n) = Sum_{d|n} Moebius(n/d)*d^(b-1)/phi(n) for b = 5.",
			"comment": [
				"a(n) is the number of lattices L in Z^4 such that the quotient group Z^4 / L is C_nm x (C_m)^3 (and also (C_nm)^3 x C_m), for every m\u003e=1. - _Álvar Ibeas_, Oct 30 2015"
			],
			"reference": [
				"J. H. Kwak and J. Lee, Enumeration of graph coverings, surface branched coverings and related group theory, in Combinatorial and Computational Mathematics (Pohang, 2000), ed. S. Hong et al., World Scientific, Singapore 2001, pp. 97-161. See p. 134."
			],
			"link": [
				"Enrique Pérez Herrero, \u003ca href=\"/A160891/b160891.txt\"\u003eTable of n, a(n) for n = 1..5000\u003c/a\u003e",
				"\u003ca href=\"/index/J#nome\"\u003eIndex to Jordan function ratios J_k/J_1\u003c/a\u003e"
			],
			"formula": [
				"a(n) = J_4(n)/J_1(n) = J_4(n)/phi(n) = A059377(n)/A000010(n), where J_k is the k-th Jordan Totient Function. - _Enrique Pérez Herrero_, Oct 19 2010",
				"Multiplicative with a(p^e) = p^(3e-3)*(1+p+p^2+p^3). - _R. J. Mathar_, Jul 10 2011",
				"For squarefree n, a(n) = A000203(n^3). - _Álvar Ibeas_, Oct 30 2015",
				"Sum_{k\u003e=1} 1/a(k) = Product_{primes p} (1 + p^3/((p^3-1)*(p^3+p^2+p+1))) = 1.115923965261131974852254388404911045036763705978837384729819264463715993... - _Vaclav Kotesovec_, Sep 20 2020"
			],
			"example": [
				"There are 1395 = A160870(8,4) lattices of volume 8 in Z^4. Among them, a(8) = 960 give the quotient group C_8 and a(2) = 15 give C_2 x C_2 x C_2.",
				"Among the lattices of volume 64 in Z^4, there are a(4) = 120 such that the quotient group is C_4 x C_4 x C_4 and other 120 with quotient group C_8 x (C_2)^3."
			],
			"maple": [
				"A160891 := proc(n) a := 1 ; for f in ifactors(n)[2] do p := op(1,f) ; e := op(2,f) ; a := a*p^(3*e-3)*(1+p+p^2+p^3) ; end do; a; end proc:",
				"seq(A160891(n),n=1..20) ; # _R. J. Mathar_, Jul 10 2011"
			],
			"mathematica": [
				"A160891[n_]:=DivisorSum[n,MoebiusMu[n/#]*#^(5-1)/EulerPhi[n]\u0026] (* _Enrique Pérez Herrero_, Oct 19 2010 *)"
			],
			"program": [
				"(PARI) vector(50, n, sumdiv(n^3, d, if(ispower(d, 4), moebius(sqrtnint(d, 4))*sigma(n^3/d), 0))) \\\\ _Altug Alkan_, Oct 30 2014",
				"(PARI) a(n) = {f = factor(n); for (i=1, #f~, p = f[i, 1]; f[i,1] = p^(3*f[i,2]-3)*(1+p+p^2+p^3); f[i,2] = 1;); factorback(f);} \\\\ _Michel Marcus_, Nov 12 2015"
			],
			"keyword": "nonn,mult",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Nov 19 2009",
			"ext": [
				"Definition corrected by _Enrique Pérez Herrero_, Aug 22 2010"
			],
			"references": 5,
			"revision": 33,
			"time": "2020-09-20T03:42:14-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}