{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A077416",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 77416,
			"data": "1,13,155,1847,22009,262261,3125123,37239215,443745457,5287706269,63008729771,750817050983,8946795882025,106610733533317,1270382006517779,15137973344680031,180385298129642593",
			"name": "Chebyshev S-sequence with Diophantine property.",
			"comment": [
				"7*b(n)^2 - 5*a(n)^2 = 2 with companion sequence b(n) = A077417(n), n\u003e=0.",
				"a(n) = L(n,-12)*(-1)^n, where L is defined as in A108299; see also A077417 for L(n,+12). - _Reinhard Zumkeller_, Jun 01 2005"
			],
			"link": [
				"Ivan Panchenko, \u003ca href=\"/A077416/b077416.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"Andersen, K., Carbone, L. and Penta, D., \u003ca href=\"https://pdfs.semanticscholar.org/8f0c/c3e68d388185129a56ed73b5d21224659300.pdf\"\u003eKac-Moody Fibonacci sequences, hyperbolic golden ratios, and real quadratic fields\u003c/a\u003e, Journal of Number Theory and Combinatorics, Vol 2, No. 3 pp 245-278, 2011. See Section 9.",
				"Alex Fink, Richard K. Guy, and Mark Krusemeyer, \u003ca href=\"https://doi.org/10.11575/cdm.v3i2.61940\"\u003ePartitions with parts occurring at most thrice\u003c/a\u003e, Contributions to Discrete Mathematics, Vol 3, No 2 (2008), pp. 76-114. See Section 13.",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (12,-1)."
			],
			"formula": [
				"a(n) = 12*a(n-1) - a(n-2), a(-1)=-1, a(0)=1.",
				"a(n) = S(n, 12) + S(n-1, 12) = S(2*n, sqrt(14)) with S(n, x) := U(n, x/2) Chebyshev's polynomials of the second kind. See A049310. S(-1, x)=0, S(n, 12) = A004191(n).",
				"G.f.: (1+x)/(1-12*x+x^2).",
				"a(n) = (ap^(2*n+1) - am^(2*n+1))/(ap - am) with ap := (sqrt(7)+sqrt(5))/sqrt(2) and am := (sqrt(7)-sqrt(5))/sqrt(2).",
				"a(n) = Sum_{k=0..n} (-1)^k * binomial(2*n-k,k) * 14^(n-k).",
				"a(n) = sqrt((7*A077417(n)^2 - 2)/5)."
			],
			"mathematica": [
				"LinearRecurrence[{12,-1},{1,13},30] (* _Harvey P. Dale_, Apr 03 2013 *)"
			],
			"program": [
				"(Sage) [(lucas_number2(n,12,1)-lucas_number2(n-1,12,1))/10 for n in range(1, 18)] # _Zerinvary Lajos_, Nov 10 2009",
				"(PARI) x='x+O('x^30); Vec((1+x)/(1-12*x+x^2)) \\\\ _G. C. Greubel_, Jan 18 2018",
				"(MAGMA) I:=[1, 13]; [n le 2 select I[n] else 12*Self(n-1) - Self(n-2): n in [1..30]]; // _G. C. Greubel_, Jan 18 2018"
			],
			"xref": [
				"Cf. A054320(n-1) with companion A072256(n), n\u003e=1."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Nov 29 2002",
			"references": 11,
			"revision": 40,
			"time": "2019-12-07T12:18:24-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}