{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005581",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5581,
			"id": "M1744",
			"data": "0,0,2,7,16,30,50,77,112,156,210,275,352,442,546,665,800,952,1122,1311,1520,1750,2002,2277,2576,2900,3250,3627,4032,4466,4930,5425,5952,6512,7106,7735,8400,9102,9842,10621,11440,12300,13202,14147,15136,16170",
			"name": "a(n) = (n-1)*n*(n+4)/6.",
			"comment": [
				"A class of Boolean functions of n variables and rank 2.",
				"Also, number of inscribable triangles within a (n+4)-gon sharing with them its vertices but not its sides. - _Lekraj Beedassy_, Nov 14 2003",
				"a(n) = A111808(n,3) for n \u003e 2. - _Reinhard Zumkeller_, Aug 17 2005",
				"If X is an n-set and Y a fixed 2-subset of X then a(n-2) is equal to the number of (n-3)-subsets of X intersecting Y. - _Milan Janjic_, Jul 30 2007",
				"The sequence starting with offset 2 = binomial transform of [2, 5, 4, 1, 0, 0, 0, ...]. - _Gary W. Adamson_, Mar 20 2009",
				"Let I=I_n be the n X n identity matrix and P=P_n be the incidence matrix of the cycle (1,2,3,...,n). Then, for n \u003e= 4, a(n-4) is the number of (0,1) n X n matrices A \u003c= P^(-1) + I + P having exactly two 1's in every row and column with perA=8. - _Vladimir Shevelev_, Apr 12 2010",
				"Also arises as the number of triples of edges which can be chosen as the cut-points in the \"three-opt\" heuristic for a traveling salesman problem on (n+4) nodes. - _James McDermott_, Jul 10 2015",
				"a(n) = risefac(n, 3)/3! - n is for n \u003e= 1 also the number of independent components of a symmetric traceless tensor of rank 3 and dimension n. Here risefac is the rising factorial. - _Wolfdieter Lang_, Dec 10 2015",
				"For n \u003e= 2, a(n) is the number of characters in a word Q formed by concatenating all 'directed' ( left to right or vice versa), unrearranged subwords, from length 1 to (n-1), of a length (n-1) word q- allowing for the appearance of repeated subwords- and simply inserting an extra character for all subwords thus concatenated. - _Christopher Hohl_, May 30 2019"
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), Table 22.7, p. 797.",
				"Joseph D. Konhauser, Dan Velleman and Stan Wagon,, Which Way Did the Bicycle Go?, MAA, 1996, p. 177.",
				"V. S. Shevelyov (Shevelev), Extension of the Moser class of four-line Latin rectangles, DAN Ukrainy, Vol. 3 (1992), pp. 15-19. - _Vladimir Shevelev_, Apr 12 2010",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"A. M. Yaglom and I. M. Yaglom, Challenging Mathematical Problems with Elementary Solutions. Vol. I. Combinatorial Analysis and Probability Theory. New York: Dover Publications, Inc., 1987, p. 13, #51 (the case k=3) (First published: San Francisco: Holden-Day, Inc., 1964)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A005581/b005581.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972. [Alternative scanned copy]",
				"Armen G. Bagdasaryan and Ovidiu Bagdasar, \u003ca href=\"https://doi.org/10.1016/j.endm.2018.05.012\"\u003eOn some results concerning generalized arithmetic triangles\u003c/a\u003e, Electronic Notes in Discrete Mathematics, Vol. 67 (2018), pp. 71-77.",
				"Beáta Bényi, Miguel Méndez, José L. Ramírez and Tanay Wakhare, \u003ca href=\"https://arxiv.org/abs/1811.12897\"\u003eRestricted r-Stirling Numbers and their Combinatorial Applications\u003c/a\u003e, arXiv:1811.12897 [math.CO], 2018.",
				"Richard K. Guy, \u003ca href=\"/A005712/a005712.pdf\"\u003eLetter to N. J. A. Sloane/a\u003e, 1987.",
				"Richard K. Guy, \u003ca href=\"/A005581/a005581_1.pdf\"\u003eLetter to N. J. A. Sloane\u003c/a\u003e, Feb 1988.",
				"F. T. Howard and Curtis Cooper, \u003ca href=\"http://www.fq.math.ca/Papers1/49-3/HowardCooper.pdf\"\u003eSome identities for r-Fibonacci numbers\u003c/a\u003e, Fibonacci Quart., Vol. 49, No. 3 (2011), pp. 231-243.",
				"Milan Janjic, \u003ca href=\"https://pmf.unibl.org/wp-content/uploads/2017/10/enumfor.pdf\"\u003eTwo Enumerative Functions\u003c/a\u003e.",
				"V. Jovovic and G. Kilibarda, \u003ca href=\"http://dx.doi.org/10.4213/dm398\"\u003eOn the number of Boolean functions in the Post classes F^{mu}_8\u003c/a\u003e, Diskretnaya Matematika, Vol. 11, No. 4 (1999), pp. 127-138.",
				"V. Jovovic and G. Kilibarda, \u003ca href=\"http://dx.doi.org/10.1515/dma.1999.9.6.593\"\u003eOn the number of Boolean functions in the Post classes F^{mu}_8\u003c/a\u003e, (English translation), Discrete Mathematics and Applications, Vol. 9, No. 6 (1999), pp. 593-605.",
				"Kyu-Hwan Lee and Se-jin Oh, \u003ca href=\"http://arxiv.org/abs/1601.06685\"\u003eCatalan triangle numbers and binomial coefficients\u003c/a\u003e, arXiv:1601.06685 [math.CO], 2016.",
				"Alice McLeod and William Moser, \u003ca href=\"http://www.jstor.org/stable/27642988\"\u003eCounting cyclic binary strings\u003c/a\u003e, Math. Mag., Vol. 80, No. 1 (2007), pp. 29-37.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992, arXiv:0911.4975 [math.NT], 2009.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992.",
				"C. Rossiter, \u003ca href=\"http://noticingnumbers.net/300SeriesCube.htm\"\u003eDepictions, Explorations and Formulas of the Euler/Pascal Cube\u003c/a\u003e. [Dead link]",
				"C. Rossiter, \u003ca href=\"/A005581/a005581.pdf\"\u003eDepictions, Explorations and Formulas of the Euler/Pascal Cube\u003c/a\u003e. [Cached copy, May 15 2013]",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/TrinomialCoefficient.html\"\u003eTrinomial Coefficient\u003c/a\u003e.",
				"\u003ca href=\"/index/Bo#Boolean\"\u003eIndex entries for sequences related to Boolean functions\u003c/a\u003e.",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-6,4,-1).",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials\u003c/a\u003e."
			],
			"formula": [
				"G.f.: (x^2)*(2-x)/(1-x)^4.",
				"a(n) = binomial(n+1, n-2) + binomial(n, n-2).",
				"a(n) = A027907(n, 3), n \u003e= 0 (fourth column of trinomial coefficients). - _N. J. A. Sloane_, May 16 2003",
				"Convolution of {1, 2, 3, ...} with {2, 3, 4, ...}. - _Jon Perry_, Jun 25 2003",
				"a(n+2) = 2*te(n) - te(n-1), e.g., a(5) = 2*te(3) - te(2) = 2*20 - 10 = 30, where te(n) are the tetrahedral numbers A000292. - _Jon Perry_, Jul 23 2003",
				"a(n) is the coefficient of x^3 in the expansion of (1+x+x^2)^n. For example, a(1)=0 since (1+x+x^2)^1=1+x+x^2. - Peter C. Heinig (algorithms(AT)gmx.de), Apr 09 2007",
				"E.g.f.: (x^2 + x^3/6) * exp(x). - _Michael Somos_, Apr 13 2007",
				"a(n) = - A005586(-4-n) for all n in Z. - _Michael Somos_, Apr 13 2007",
				"a(n) = C(4+n,3)-(n+4)*(n+1), since C(4+n,3) = number of all triangles in (n+4)-gon, and (n+4)*(n+1)=number of triangles with at least one of the edges included. Example: n=0,in a square, all 4 possible triangles include some of the square's edges and C(4+n,3)-(n+4)*(n+1)=4-4*1=0 = number of other triangles = a(0). - _Toby Gottfried_, Nov 12 2011",
				"a(n) = 2*binomial(n,2) + binomial(n,3). - _Vladimir Shevelev_ and _Peter J. C. Moses_, Jun 22 2012",
				"a(0)=0, a(1)=0, a(2)=2, a(3)=7, a(n)=4*a(n-1)-6*a(n-2)+4*a(n-3)-a(n-4). - _Harvey P. Dale_, Sep 22 2012",
				"a(n) = A000292(n-1) + A000217(n-1) for all n in Z. - _Michael Somos_, Jul 29 2015",
				"a(n+2) = -A127672(6+n, n), n \u003e= 0, with A127672 giving the coefficients of Chebyshev's C polynomials. See the Abramowitz-Stegun reference. - _Wolfdieter Lang_, Dec 10 2015",
				"a(n) = GegenbauerC(N, -n, -1/2) where N = 3 if 3\u003cn else 2*n-3. - _Peter Luschny_, May 10 2016",
				"From _Amiram Eldar_, Jan 09 2022: (Start)",
				"Sum_{n\u003e=2} 1/a(n) = 163/200.",
				"Sum_{n\u003e=2} (-1)^n/a(n) = 12*log(2)/5 - 253/200. (End)"
			],
			"example": [
				"In hexagon ABCDEF, the \"interior\" triangles are ACE and BDF, and a(6-4)=a(2)=2. - _Toby Gottfried_, Nov 12 2011",
				"G.f. = 2*x^2 + 7*x^3 + 16*x^4 + 30*x^5 + 50*x^6 + 77*x^7 + 112*x^8 + ..."
			],
			"maple": [
				"A005581 := n-\u003e(n-1)*n*(n+4)/6: seq(A005581(n), n=0..50);",
				"a:=n-\u003esum ((j+3)*j/2,j=0..n): seq(a(n),n=-1..49); # _Zerinvary Lajos_, Dec 17 2006",
				"seq((n+3)*binomial(n,3)/n, n=1..46); # _Zerinvary Lajos_, Feb 28 2007",
				"A005581:=-(-2+z)/(z-1)**4; # _Simon Plouffe_ in his 1992 dissertation",
				"seq(sum(binomial(n,m), m=1..3)+n^2,n=-1..44); # _Zerinvary Lajos_, Jun 19 2008",
				"A005581 := n -\u003e GegenbauerC(`if`(3\u003cn,3,2*n-3), -n, -1/2):",
				"seq(simplify(A005581(n)), n=0..50); # _Peter Luschny_, May 10 2016"
			],
			"mathematica": [
				"Table[(n-1)*n*(n+4)/6, {n,0,50}] (* _Stefan Steinerberger_, Apr 10 2006 *)",
				"LinearRecurrence[{4,-6,4,-1},{0,0,2,7},50] (* _Harvey P. Dale_, Sep 22 2012 *)"
			],
			"program": [
				"(PARI) {a(n) = n * (n+4) * (n-1) / 6}; /* _Michael Somos_, Apr 13 2007 */",
				"(PARI) concat([0, 0], Vec((x^2)*(2-x)/(1-x)^4 + O(x^50))) \\\\ _Altug Alkan_, Dec 10 2015",
				"(Maxima) A005581(n):=(n-1)*n*(n+4)/6$ makelist(A005581(n),n,0,50); /* _Martin Ettl_, Dec 18 2012 */",
				"(Sage) [(n-1)*n*(n+4)/6 for n in range(50)] # _Danny Rorabaugh_, Apr 20 2015",
				"(MAGMA) [(n-1)*n*(n+4)/6 : n in [0..50]]; // _Wesley Ivan Hurt_, Jul 10 2015"
			],
			"xref": [
				"Cf. A000217, A000292, A005582, A005586, A111808.",
				"Cf. A176222, A000211, A052928, A128209. - _Vladimir Shevelev_, Apr 12 2010",
				"Cf. A127672 (Chebyshev C)."
			],
			"keyword": "nonn,easy,nice,changed",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from Larry Reeves (larryr(AT)acm.org), Jun 01 2000"
			],
			"references": 47,
			"revision": 158,
			"time": "2022-01-09T03:12:57-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}