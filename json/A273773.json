{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A273773",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 273773,
			"data": "43,67,101,137,163,181,229,241,281,313,353,421,433,487,563,601,617,641,653,673,769,821,823,853,883,907,937,941,1009,1061,1093,1277,1303,1361,1423,1429,1433,1447,1489,1549,1571,1579,1601,1607,1609,1613,1657,1667,1697,1741,1747",
			"name": "Primes p such that no practical number (A005153) exists between p and its successor.",
			"comment": [
				"According to Margenstern and proved by Weingartner (see links) the density of practical numbers is greater than the density of primes. Margenstern calculated that the density of practical numbers was approx 1.2767 (1.3411/1.059) times greater than the density of primes in the interval 1 to 10^12. This sequence shows that the set of places where no practical number exists between successive primes has a degree of regularity and appears to be infinite."
			],
			"link": [
				"Frank M Jackson, \u003ca href=\"/A273773/b273773.txt\"\u003eTable of n, a(n) for n = 1..100000\u003c/a\u003e",
				"Maurice Margenstern, \u003ca href=\"http://dx.doi.org/10.1016/S0022-314X(05)80022-8\"\u003eLes nombres pratiques: théorie, observations et conjectures\u003c/a\u003e, Journal of Number Theory 37 (1): 1-36, 1991.",
				"A. Weingartner, \u003ca href=\"http://qjmath.oxfordjournals.org/content/66/2/743\"\u003ePractical numbers and the distribution of divisors\u003c/a\u003e, The Quarterly Journal of Mathematics 66 (2): 743-758, 2015.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Practical_number\"\u003ePractical number\u003c/a\u003e"
			],
			"example": [
				"a(6) = 181, the next prime is 191. In the integer interval [181, 191] there are no practical numbers. It is the 6th such occurrence."
			],
			"mathematica": [
				"PracticalQ[n_] := Module[{f, p, e, prod=1, ok=True}, If[n\u003c1||(n\u003e1\u0026\u0026OddQ[n]), False, If[n==1, True, f=FactorInteger[n]; {p, e}=Transpose[f]; Do[If[p[[i]]\u003e1+DivisorSigma[1, prod], ok=False; Break[]];     prod = prod*p[[i]]^e[[i]], {i, Length[p]}]; ok]]]; count[n_Integer] := Module[{t=0, m}, Do[If[PracticalQ[m], t++], {m, Prime[n], Prime[n + 1] - 1}]; t]; lst = {}; Do[If[count[n]==0, AppendTo[lst, Prime[n]]], {n, 1, 1000}];lst"
			],
			"xref": [
				"Cf. A005153."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Frank M Jackson_, May 29 2016",
			"references": 3,
			"revision": 11,
			"time": "2016-06-01T21:40:23-04:00",
			"created": "2016-05-30T23:53:10-04:00"
		}
	]
}