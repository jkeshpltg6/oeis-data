{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A038719",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 38719,
			"data": "1,2,1,4,5,2,8,19,18,6,16,65,110,84,24,32,211,570,750,480,120,64,665,2702,5460,5880,3240,720,128,2059,12138,35406,57120,52080,25200,5040,256,6305,52670,213444,484344,650160,514080,221760,40320,512,19171",
			"name": "Triangle T(n,k) (0 \u003c= k \u003c= n) giving number of chains of length k in partially ordered set formed from subsets of n-set by inclusion.",
			"comment": [
				"The relation of this triangle to A143494 given in the Formula section leads to the following combinatorial interpretation: T(n,k) gives the number of partitions of the set {1,2,...,n+2} into k + 2 blocks where 1 and 2 belong to two distinct blocks and the remaining k blocks are labeled from a fixed set of k labels. - _Peter Bala_, Jul 10 2014",
				"Also, the number of distinct k-level fuzzy subsets of a set consisting of n elements ordered by set inclusion. - _Rajesh Kumar Mohapatra_, Mar 16 2020"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A038719/b038719.txt\"\u003eRows n = 0..140, flattened\u003c/a\u003e",
				"Peter Bala, \u003ca href=\"/A131689/a131689.pdf\"\u003eDeformations of the Hadamard product of power series\u003c/a\u003e",
				"L. Bartlomiejczyk and J. Drewniak, \u003ca href=\"http://dx.doi.org/10.1007/s00010-004-2737-7\"\u003eA characterization of sets and operations invariant under bijections\u003c/a\u003e, Aequationes Mathematicae 68 (2004), pp. 1-9.",
				"M. Dukes and C. D. White, \u003ca href=\"http://arxiv.org/abs/1603.01589\"\u003eWeb Matrices: Structural Properties and Generating Combinatorial Identities\u003c/a\u003e, arXiv:1603.01589 [math.CO], 2016.",
				"R. B. Nelsen and H. Schmidt, Jr., \u003ca href=\"http://www.jstor.org/stable/2690450\"\u003eChains in power sets\u003c/a\u003e, Math. Mag., 64 (1991), 23-31.",
				"\u003ca href=\"/index/Pos#posets\"\u003eIndex entries for sequences related to posets\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = Sum_{j=0..k} (-1)^j*C(k, j)*(k+2-j)^n.",
				"T(n+1, k) = k*T(n, k-1) + (k+2)*T(n, k), T(0,0) = 1, T(0,k) = 0 for k\u003e0.",
				"E.g.f.: exp(2*x)/(1+y*(1-exp(x))). - _Vladeta Jovovic_, Jul 21 2003",
				"A038719 as a lower triangular matrix is the binomial transform of A028246. - _Gary W. Adamson_, May 15 2005",
				"Binomial transform of n-th row = 2^n + 3^n + 4^n + ...; e.g., binomial transform of [8, 19, 18, 6] = 2^3 + 3^3 + 4^3 + 5^3 + ... = 8, 27, 64, 125, ... - _Gary W. Adamson_, May 15 2005",
				"From _Peter Bala_, Jul 09 2014: (Start)",
				"T(n,k) = k!*( Stirling2(n+2,k+2) - Stirling2(n+1,k+2) ).",
				"T(n,k) = k!*A143494(n+2,k+2).",
				"n-th row polynomial = 1/(1 + x)*( sum {k \u003e= 0} (k + 2)^n*(x/(1 + x))^k ). Cf. A028246. (End)",
				"The row polynomials have the form (2 + x) o (2 + x) o ... o (2 + x), where o denotes the black diamond multiplication operator of Dukes and White. See example E12 in the Bala link. - _Peter Bala_, Jan 18 2018",
				"Z(P,m) = Sum_{k=0..n} T(n,k)Binomial(m-2,k) = m^n, the zeta polynomial of the poset B_n. Each length m multichain from 0 to 1 in B_n corresponds to a function from [n] into [m]. - _Geoffrey Critzer_, Dec 25 2020"
			],
			"example": [
				"Triangle begins",
				"   1;",
				"   2,   1;",
				"   4,   5,   2;",
				"   8,  19,  18,   6;",
				"  16,  65, 110,  84,  24;",
				"  ..."
			],
			"maple": [
				"T:= proc(n, k) option remember;",
				"      `if` (n=0, `if`(k=0, 1, 0), k*T(n-1, k-1) +(k+2)*T(n-1, k))",
				"    end:",
				"seq(seq(T(n, k), k=0..n), n=0..10); # _Alois P. Heinz_, Aug 02 2011"
			],
			"mathematica": [
				"t[n_, k_] := Sum[ (-1)^(k-i)*Binomial[k, i]*(2+i)^n, {i, 0, k}]; Flatten[ Table[ t[n, k], {n, 0, 9}, {k, 0, n}]] (* _Jean-François Alcover_, after Pari *)"
			],
			"program": [
				"(PARI) T(n,k)=sum(i=0,k,(-1)^(k-i)*binomial(k,i)*(2+i)^n)",
				"(Haskell)",
				"a038719 n k = a038719_tabl !! n !! k",
				"a038719_row n = a038719_tabl !! n",
				"a038719_tabl = iterate f [1] where",
				"   f row = zipWith (+) (zipWith (*) [0..] $ [0] ++ row)",
				"                       (zipWith (*) [2..] $ row ++ [0])",
				"-- _Reinhard Zumkeller_, Jul 08 2012"
			],
			"xref": [
				"Row sums give A007047. Columns give A000079, A001047, A038721. Next-to-last diagonal gives A038720.",
				"Diagonal gives A000142. - _Rajesh Kumar Mohapatra_, Mar 16 2020",
				"Cf. A028246. A019538, A143494."
			],
			"keyword": "nonn,easy,nice,tabl",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, May 02 2000",
			"ext": [
				"More terms from Larry Reeves (larryr(AT)acm.org), May 09 2000"
			],
			"references": 14,
			"revision": 66,
			"time": "2021-06-04T22:52:04-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}