{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A088192",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 88192,
			"data": "1,2,1,3,2,1,1,2,5,1,3,1,1,2,5,1,2,1,2,7,1,3,2,1,1,1,3,2,1,1,3,2,1,2,1,3,1,2,5,1,2,1,7,1,1,3,2,3,2,1,1,7,1,2,1,5,1,3,1,1,2,1,2,11,1,1,2,1,2,1,1,7,3,1,2,5,1,1,1,1,2,1,7,1,3,2,1,1,1,3,2,13,3,2,2,5,1,1,2,1",
			"name": "Distance between prime(n) and the largest quadratic residue modulo prime(n).",
			"comment": [
				"a(n) = smallest m\u003e0 such that -m is a quadratic residue modulo prime(n).",
				"a(n) = smallest m\u003e0 such that prime(n) either splits or ramifies in the imaginary quadratic field Q(sqrt(-m)). Equals -A220862(n) except when n = 1. Cf. A220861, A220863. - _N. J. A. Sloane_, Dec 26 2012",
				"The values are 1 or a prime number (easily provable!). The maximum occurring prime values increase very slowly: up to 10^5 terms the largest prime is 43. The primes do not appear in order."
			],
			"reference": [
				"David A. Cox, \"Primes of the Form x^2 + n y^2\", Wiley, 1989, Cor. 5.17, p. 105. - From _N. J. A. Sloane_, Dec 26 2012"
			],
			"link": [
				"Charles R Greathouse IV, \u003ca href=\"/A088192/b088192.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Ferenc Adorjan, \u003ca href=\"http://web.axelero.hu/fadorjan/qrp.pdf\"\u003eThe sequence of largest quadratic residues modulo the primes\u003c/a\u003e.",
				"J. A. Bergstra, I. Bethke, \u003ca href=\"http://arxiv.org/abs/1507.00548\"\u003eA negative result on algebraic specifications of the meadow of rational numbers\u003c/a\u003e, arXiv preprint arXiv:1507.00548 [math.RA], 2015-2016."
			],
			"formula": [
				"a(n) = A053760(n) unless -1 is a quadratic residue mod prime(n). - _Charles R Greathouse IV_, Oct 31 2012"
			],
			"mathematica": [
				"a[n_] := With[{p = Prime[n]}, If[JacobiSymbol[-1, p] \u003e 0, 1, For[d = 2, True, d = NextPrime[d], If[JacobiSymbol[-d, p] \u003e= 0, Return[d]]]]]; Array[a, 100] (* _Jean-François Alcover_, Feb 16 2018, after _Charles R Greathouse IV_ *)"
			],
			"program": [
				"(PARI) qrp_pm(fr,to)= {/* The distance of largest QR modulo the primes from the primes */ local(m,p,v=[]); for(i=fr,to,m=1; p=prime(i); j=2; while((j\u003c=(p-1)/2)\u0026\u0026(m\u003cp-1),m=max(m,(j^2)%p); j++); v=concat(v,p-m)); print(v) }",
				"(PARI) do(p)=if(kronecker(-1,p)\u003e0, 1, forprime(d=2, p, if(kronecker(-d, p) \u003e= 0, return(d))))",
				"apply(do, primes(100)) \\\\ _Charles R Greathouse IV_, Oct 31 2012"
			],
			"xref": [
				"Records are (essentially) given by A147971.",
				"Cf. A088190, A088191, A088193, A088194, A088195, A220861, A220862, A220863."
			],
			"keyword": "easy,nonn",
			"offset": "1,2",
			"author": "Ferenc Adorjan (fadorjan(AT)freemail.hu), Sep 22 2003",
			"ext": [
				"Edited by _Max Alekseyev_, Oct 29 2012"
			],
			"references": 14,
			"revision": 32,
			"time": "2018-02-16T08:59:15-05:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}