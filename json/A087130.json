{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A087130",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 87130,
			"data": "2,5,27,140,727,3775,19602,101785,528527,2744420,14250627,73997555,384238402,1995189565,10360186227,53796120700,279340789727,1450500069335,7531841136402,39109705751345,203080369893127",
			"name": "a(n) = 5*a(n-1)+a(n-2) for n\u003e1, a(0)=2, a(1)=5.",
			"comment": [
				"Sequence is related to the fifth metallic mean [5;5,5,5,5,...] (see A098318).",
				"The solution to the general recurrence b(n) = (2*k+1)*b(n-1)+b(n-2) with b(0)=2, b(1) = 2*k+1 is b(n) = ((2*k+1)+sqrt(4*k^2+4*k+5))^n+(2*k+1)-sqrt(4*k^2+4*k+5))^n)/2; b(n) = 2^(1-n)*Sum_{j=0..n} C(n, 2*j)*(4*k^2+4*k+5)^j*(2*k+1)^(n-2*j); b(n) = 2*T(n, (2*k+1)*x/2)(-1)^i with T(n, x) Chebyshev's polynomials of the first kind (see A053120) and i^2=-1. - _Paul Barry_, Nov 15 2003",
				"Primes in this sequence include a(0) = 2; a(1) = 5; a(4) = 727; a(8) = 528527 (3) semiprimes in this sequence include a(7) = 101785; a(13) = 1995189565; a(16) = 279340789727; a(19) = 39109705751345; a(20) = 203080369893127 - _Jonathan Vos Post_, Feb 09 2005",
				"a(n)^2 - 29*A052918(n-1)^2 = 4*(-1)^n, with n\u003e0 - _Gary W. Adamson_, Oct 07 2008",
				"For more information about this type of recurrence follow the Khovanova link and see A054413 and A086902. - _Johannes W. Meijer_, Jun 12 2010",
				"Binomial transform of A072263. - _Johannes W. Meijer_, Aug 01 2010"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A087130/b087130.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"P. Bhadouria, D. Jhala, B. Singh, \u003ca href=\"http://dx.doi.org/10.22436/jmcs.08.01.07\"\u003eBinomial Transforms of the k-Lucas Sequences and its Properties\u003c/a\u003e, The Journal of Mathematics and Computer Science (JMCS), Volume 8, Issue 1, Pages 81-92; sequence L_{5,n}.",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Metallic_mean\"\u003eMetallic mean\u003c/a\u003e",
				"\u003ca href=\"/index/Rea#recur1\"\u003eIndex entries for recurrences a(n) = k*a(n - 1) +/- a(n - 2)\u003c/a\u003e",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (5,1)."
			],
			"formula": [
				"a(n) = ((5+sqrt(29))/2)^n+((5-sqrt(29))/2)^n.",
				"a(n) = A100236(n) + 1.",
				"E.g.f. : 2*exp(5*x/2)*cosh(sqrt(29)*x/2); a(n) = 2^(1-n)*Sum_{k=0..floor(n/2)} C(n, 2k)*29^k*5^(n-2*k). a(n) = 2T(n, 5i/2)(-i)^n with T(n, x) Chebyshev's polynomials of the first kind (see A053120) and i^2=-1. - _Paul Barry_, Nov 15 2003",
				"O.g.f.: (-2+5*x)/(-1+5*x+x^2). - _R. J. Mathar_, Dec 02 2007",
				"a(-n) = (-1)^n * a(n). - _Michael Somos_, Nov 01 2008",
				"A090248(n) = a(2*n). 5 * A097834(n) = a(2*n + 1). - _Michael Somos_, Nov 01 2008",
				"Limit(a(n+k)/a(k), k=infinity) = (A087130(n) + A052918(n-1)*sqrt(29))/2. Limit(A087130(n)/A052918(n-1), n= infinity) = sqrt(29). - _Johannes W. Meijer_, Jun 12 2010",
				"a(3n+1) = A041046(5n), a(3n+2) = A041046(5n+3) and a(3n+3) = 2*A041046 (5n+4). - _Johannes W. Meijer_, Jun 12 2010",
				"a(n) = 2*A052918(n) - 5*A052918(n-1). - _R. J. Mathar_, Oct 02 2020"
			],
			"mathematica": [
				"RecurrenceTable[{a[0] == 2, a[1] == 5, a[n] == 5 a[n-1] + a[n-2]}, a, {n, 30}] (* _Vincenzo Librandi_, Sep 19 2016 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, (-1)^n * a(-n), polsym(x^2 - 5*x -1, n) [n + 1])} /* _Michael Somos_, Nov 04 2008 */",
				"(Sage) [lucas_number2(n,5,-1) for n in range(0, 21)] # _Zerinvary Lajos_, May 14 2009",
				"(MAGMA) I:=[2,5]; [n le 2 select I[n] else 5*Self(n-1)+Self(n-2): n in [1..30]]; // _Vincenzo Librandi_, Sep 19 2016"
			],
			"xref": [
				"Cf. A006497, A014448, A085447.",
				"Cf. A086902, A000032, A052918."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_Paul Barry_, Aug 16 2003",
			"references": 30,
			"revision": 54,
			"time": "2020-10-02T06:22:34-04:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}