{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A173729",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 173729,
			"data": "1,4,10,24,53,106,191,328,528,822,1230,1794,2542,3534,4802,6428,8460,10996,14087,17870,22405,27850,34286,41896,50773,61148,73116,86942,102751,120840,141343,164618,190808,220306,253292,290202,331226,376872",
			"name": "Number of symmetry classes of 3 X 3 magilatin squares with positive values \u003c n.",
			"comment": [
				"A magilatin square has equal row and column sums and no number repeated in any row or column. The symmetries are row and column permutations and diagonal flip.",
				"a(n) is given by a quasipolynomial of degree 5 and period 60."
			],
			"link": [
				"T. Zaslavsky, \u003ca href=\"/A173729/b173729.txt\"\u003eTable of n, a(n) for n = 4..10000\u003c/a\u003e.",
				"M. Beck and T. Zaslavsky, \u003ca href=\"http://arXiv.org/abs/math.CO/0506315\"\u003eAn enumerative geometry for magic and magilatin labellings\u003c/a\u003e, Ann. Combinatorics, 10 (2006), no. 4, 395-413. MR 2007m:05010. Zbl 1116.05071.",
				"M. Beck, T. Zaslavsky, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL13/Zaslavsky/sls.html\"\u003eSix Little Squares and How Their Numbers Grow \u003c/a\u003e, J. Int. Seq. 13 (2010), 10.6.2.",
				"Matthias Beck and Thomas Zaslavsky, \u003ca href=\"http://www.math.binghamton.edu/zaslav/Tmath/SLSfiles/\"\u003e\"Six Little Squares and How their Numbers Grow\" Web Site\u003c/a\u003e: Maple worksheets and supporting documentation."
			],
			"formula": [
				"G.f.: x^2/(1-x)^2 * { x^2/(x-1)^2 - x^3/(x-1)^3 - 2x^3/[(x-1)*(x^2-1)] - x^3/(x^3-1) - 2x^4/[(x-1)^2*(x^2-1)] - x^4/[(x-1)*(x^3-1)] - 2x^4/(x^2-1)^2 + x^5/[(x-1)^3*(x^2-1)] + x^5/[(x-1)^2*(x^3-1)] + 2x^5/[(x-1)*(x^2-1)^2] + x^5/[(x-1)*(x^4-1)] + x^5/[(x^2-1)*(x^3-1)] + x^5/(x^5-1) + 2x^6/[(x-1)*(x^2-1)*(x^3-1)] + 2x^6/[(x^2-1)*(x^4-1)] + x^6/(x^2-1)^3 + x^6/(x^3-1)^2 + x^7/[(x^3-1)*(x^4-1)] + x^7/[(x^2-1)*(x^5-1)] + x^7/[(x^2-1)^2*(x^3-1)] + x^8/[(x^3-1)*(x^5-1)] }",
				"G.f.: x^4*(1 + 4*x + 8*x^2 + 14*x^3 + 25*x^4 + 41*x^5 + 52*x^6 + 54*x^7 + 43*x^8 + 27*x^9 + 13*x^10 + 10*x^11 + 16*x^12 + 23*x^13 + 20*x^14 + 9*x^15)/((1 + x^2)*(1 + x)^3*(1 + x + x^2)^2*(1 + x + x^2 + x^3 + x^4)*(1 - x)^6). - _L. Edson Jeffery_, Sep 10 2017"
			],
			"mathematica": [
				"CoefficientList[Series[x^4*(1 + 4*x + 8*x^2 + 14*x^3 + 25*x^4 + 41*x^5 + 52*x^6 + 54*x^7 + 43*x^8 + 27*x^9 + 13*x^10 + 10*x^11 + 16*x^12 + 23*x^13 + 20*x^14 + 9*x^15)/((1 + x^2)*(1 + x)^3*(1 + x + x^2)^2*(1 + x + x^2 + x^3 + x^4)*(1 - x)^6), {x, 0, 41}], x] (* _L. Edson Jeffery_, Sep 10 2017 *)"
			],
			"xref": [
				"Cf. A173548 (total number of squares, A173549 (squares counted by magic sum), A173730 (symmetry types by magic sum)."
			],
			"keyword": "nonn",
			"offset": "4,2",
			"author": "_Thomas Zaslavsky_, Mar 04 2010, Apr 24 2010",
			"references": 4,
			"revision": 13,
			"time": "2017-09-10T04:03:04-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}