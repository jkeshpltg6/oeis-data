{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A061502",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 61502,
			"data": "1,5,9,18,22,38,42,58,67,83,87,123,127,143,159,184,188,224,228,264,280,296,300,364,373,389,405,441,445,509,513,549,565,581,597,678,682,698,714,778,782,846,850,886,922,938,942,1042,1051,1087",
			"name": "a(n) = Sum_{k\u003c=n} tau(k)^2, where tau = number of divisors function A000005.",
			"reference": [
				"R. Ayoub, An Introduction to the Analytic Theory of Numbers, Amer. Math. Soc., 1963; Chapter II, Problem 56."
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A061502/b061502.txt\"\u003eTable of n, a(n) for n = 1..1024\u003c/a\u003e",
				"Adrian Dudek, \u003ca href=\"http://arxiv.org/abs/1602.03555\"\u003eOn the Success of Mishandling Euclid's Lemma\u003c/a\u003e, arXiv:1602.03555 [math.HO], 2016. See B(n) p. 2.",
				"Chaohua Jia and Ayyadurai Sankaranarayanan, \u003ca href=\"http://doi.org/10.4064/aa164-2-7\"\u003eThe mean square of the divisor function\u003c/a\u003e, Acta Arithmetica 164 (2014), 181-208.",
				"Michaela Cully-Hugill and Timothy Trudgian, \u003ca href=\"https://arxiv.org/abs/1911.07369\"\u003eTwo explicit divisor sums\u003c/a\u003e, arXiv:1911.07369 [math.NT], Nov 19 2019",
				"Vaclav Kotesovec, \u003ca href=\"/A061502/a061502_1.jpg\"\u003eGraph - The asymptotic ratio (1000000 terms)\u003c/a\u003e",
				"Florian Luca and László Tóth, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL20/Luca/luca42.html\"\u003eThe r-th moment of the divisor function: an elementary approach\u003c/a\u003e, Journal of Integer Sequences 20 (2017), Article 17.7.4, 8 pp.",
				"Adolf Piltz, \u003ca href=\"https://gdz.sub.uni-goettingen.de/id/PPN271032898\"\u003eÜber das Gesetz, nach welchem die mittlere Darstellbarkeit der natürlichen Zahlen als Produkte einer gegebenen Anzahl Faktoren mit der Grösse der Zahlen wächst\u003c/a\u003e, 1881.",
				"Ramanujan's Papers, \u003ca href=\"http://ramanujan.sirinudi.org/Volumes/published/ram17.html\"\u003eSome formulas in the analytic theory of numbers\u003c/a\u003e, Messenger of Mathematics, XLV, 1916, 81-84, Formula (3).",
				"D. Suryanarayana and R. Rama Chandra Rao, \u003ca href=\"https://doi.org/10.7146/math.scand.a-11460\"\u003eOn an Asymptotic Formula of Ramanujan\u003c/a\u003e, Mathematica Scandinavica, 32, 258-264, 1973.",
				"B. M. Wilson, \u003ca href=\"http://plms.oxfordjournals.org/content/s2-21/1/235.extract\"\u003eProofs of some formulas enunciated by Ramanujan\u003c/a\u003e, Proc. London Math. Soc. (2) 21 (1922) 235-255."
			],
			"formula": [
				"a(n) = Sum_{k=1..n} tau(k^2)*floor(n/k).",
				"Asymptotic to A*n*log(n)^3 + B*n*log(n)^2 + C*n*log(n) + D*n + O(n^(1/2+eps)) where A = 1/Pi^2 and B = (12*gamma-3)/Pi^2 - 36*zeta'(2)/Pi^4. [corrected by _Vaclav Kotesovec_, Aug 30 2018]",
				"C = 36*gamma^2/Pi^2 - (288*z1/Pi^4 + 24/Pi^2)*gamma + (864*z1^2/Pi^6 + 72*z1/Pi^4 - 72/Pi^4*z2 + 6/Pi^2) - 24*g1/Pi^2 and D = 24*gamma^3/Pi^2 - (432*z1 /Pi^4+ 36/Pi^2)*gamma^2 + (3456*z1^2/Pi^6 + 288*(z1-z2)/Pi^4 + 24/Pi^2 - 72*g1/Pi^2)*gamma + g1*(288*z1/Pi^4 + 24/Pi^2)-10368*z1^3/Pi^8 - 864*z1^2/Pi^6 + 1728*z2*z1/Pi^6 + 72*(z2-z1)/Pi^4- 48*z3/Pi^4 + (12*g2-6)/Pi^2, where gamma is the Euler-Mascheroni constant A001620, z1 = Zeta'(2) = A073002, z2 = Zeta''(2) = A201994, z3 = Zeta'''(2) = A201995 and g1, g2 are the Stieltjes constants, see A082633 and A086279. - _Vaclav Kotesovec_, Sep 10 2018",
				"See Cully-Hugill \u0026 Trudgian, Theorem 2, for an explicit version of the asymptotic given above. - _Charles R Greathouse IV_, Nov 19 2019"
			],
			"mathematica": [
				"Table[Sum[DivisorSigma[0, k^2]*Floor[n/k], {k, 1, n}], {n, 1, 50}] (* _Vaclav Kotesovec_, Aug 30 2018 *)",
				"Accumulate[Table[DivisorSigma[0, n]^2, {n, 1, 50}]] (* _Vaclav Kotesovec_, Sep 10 2018 *)"
			],
			"program": [
				"(PARI) for (n=1, 1024, write(\"b061502.txt\", n, \" \", sum(k=1, n, numdiv(k)^2)) ) \\\\ _Harry J. Smith_, Jul 23 2009",
				"(PARI) vector(60, n, sum(k=1, n, numdiv(k)^2)) \\\\ _Michel Marcus_, Jul 23 2015",
				"(PARI) first(n)=my(v=vector(n),s); forfactored(k=1,n, v[k[1]] = s += numdiv(k)^2); v; \\\\ _Charles R Greathouse IV_, Nov 28 2018",
				"(MAGMA) [\u0026+[NumberOfDivisors(k^2)*Floor(n/k): k in [1..n]]: n in [1..60]]; // _Vincenzo Librandi_, Sep 10 2016"
			],
			"xref": [
				"Cf. A000005, A035116, A061503, A318755.",
				"Cf. A092742 (A), A245074 (B), A319090 (C), A319091 (D).",
				"Cf. A057434, A072379, A074789."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Jun 14 2001",
			"ext": [
				"Definition corrected by _N. J. A. Sloane_, May 25 2008"
			],
			"references": 14,
			"revision": 64,
			"time": "2019-11-19T02:35:46-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}