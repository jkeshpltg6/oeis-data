{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006022",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6022,
			"id": "M2219",
			"data": "0,1,1,3,1,4,1,7,4,6,1,10,1,8,6,15,1,13,1,16,8,12,1,22,6,14,13,22,1,21,1,31,12,18,8,31,1,20,14,36,1,29,1,34,21,24,1,46,8,31,18,40,1,40,12,50,20,30,1,51,1,32,29,63,14,45,1,52,24,43,1,67,1,38,31,58,12,53,1",
			"name": "Sprague-Grundy (or Nim) values for the game of Maundy cake on an n X 1 sheet.",
			"comment": [
				"There are three equivalent formulas for a(n). Suppose n \u003e= 2, and let p1 \u003c= p2 \u003c= ... \u003c= pk be the prime factors of n, with repetition.",
				"Theorem 1: a(1) = 0. For n \u003e= 2, a(n) = n*s(n), where",
				"s(n) = 1/p1 + 1/(p1*p2) + 1/(p1*p2*p3) + ... + 1/(p1*p2*...*pk).",
				"This is implicit in Berlekamp, Conway and Guy, Winning Ways, 2 vols., 1982, pp. 28, 53.",
				"Note that s(n) = A322034(n) / A322035(n).",
				"_David James Sycamore_ observed on Nov 24 2018 that Theorem 1 implies a(n) \u003c n for all n (see comments in A322034), and also leads to a simple recurrence for a(n):",
				"Theorem 2: a(1) = 0. For n \u003e= 2, a(n) = p*a(n/p) + 1, where p is the largest prime factor of n.",
				"Proof. (Th. 1 implies Th. 2) If n is a prime, Theorem 1 gives a(n) = 1 = n*a(1)+1. For a nonprime n, let n = m*p where p is the largest prime factor of n and m \u003e= 2. From Theorem 1, a(m) = m*s(m), a(n) = q*m*(s(m) + 1/n) = q*a(m) + 1.",
				"(Th. 2 implies Th. 1) The reverse implication is equally easy.",
				"Theorem 2 is equivalent to the following more complicated recurrence:",
				"Theorem 3: a(1) = 0. For n \u003e= 2, a(n) = max_{p|n, p prime} (p*a(n/p)+1)."
			],
			"reference": [
				"E. R. Berlekamp, J. H. Conway and R. K. Guy, Winning Ways, Academic Press, NY, 2 vols., 1982, see p. 28, 53.",
				"E. R. Berlekamp, J. H. Conway and R. K. Guy, Winning Ways, Second Edition, Vol. 1, A K Peters, 2001, pp. 27, 51.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A006022/b006022.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Jonathan Blanchette, Robert Laganière, \u003ca href=\"https://arxiv.org/abs/1910.11749\"\u003eA Curious Link Between Prime Numbers, the Maundy Cake Problem and Parallel Sorting\u003c/a\u003e, arXiv:1910.11749 [cs.DS], 2019."
			],
			"formula": [
				"a(n) = n * Sum_{k=1..N} (1/(p1^m1*p2^m2*...*pk^mk)) * (pk^mk-1)/(pk-1) for n\u003e=2, where pk is the k-th distinct prime factor of n, N is the number of distinct prime factors of n, and mk is the multiplicity of pk occurring in n. To prove this, expand the factors in Theorem 1 and use the geometrical series identity. - _Jonathan Blanchette_, Nov 01 2019",
				"From _Antti Karttunen_, Apr 12 2020: (Start)",
				"a(n) = A322382(n) + A333791(n).",
				"a(n) = A332993(n) - n = A001065(n) - A333783(n). (End)"
			],
			"example": [
				"For n=24, s(24) = 1/2 + 1/4 + 1/8 + 1/24 = 11/12, so a(24) = 24*11/12 = 22."
			],
			"maple": [
				"P:=proc(n) local FM: FM:=ifactors(n)[2]: seq(seq(FM[j][1], k=1..FM[j][2]), j=1..nops(FM)) end: # A027746",
				"s:=proc(n) local i,t,b; global P;t:=0; b:=1; for i in [P(n)] do b:=b*i; t:=t+1/b; od; t; end; # A322034/A0322035",
				"A006022 := n -\u003e if n = 1 then 0 else n*s(n); fi;",
				"# _N. J. A. Sloane_, Nov 28 2018"
			],
			"mathematica": [
				"Nest[Function[{a, n}, Append[a, Max@ Map[# a[[n/#]] + 1 \u0026, Rest@ Divisors@ n]]] @@ {#, Length@ # + 1} \u0026, {0, 1}, 77] (* _Michael De Vlieger_, Nov 23 2018 *)"
			],
			"program": [
				"(Haskell)",
				"a006022 1 = 0",
				"a006022 n = (+ 1) $ sum $ takeWhile (\u003e 1) $",
				"          iterate (\\x -\u003e x `div` a020639 x) (a032742 n)",
				"-- _Reinhard Zumkeller_, Jun 03 2012",
				"(PARI) lista(nn) = {my(v = vector(nn)); for (n=1, nn, if (n\u003e1, my(m = 0); fordiv (n, d, if (d\u003e1, m = max(m, d*v[n/d]+1))); v[n] = m;); print1(v[n], \", \"););} \\\\ _Michel Marcus_, Nov 25 2018"
			],
			"xref": [
				"Cf. A020639, A032742, A001348, A002182, A008864.",
				"Cf. also A027746, A306264, A306363, A322034, A322035, A322036, A322382, A328898, A333783, A332993, A333791.",
				"Row sums of A328969."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Edited and extended by _Christian G. Bower_, Oct 18 2002",
				"Entry revised by _N. J. A. Sloane_, Nov 28 2018"
			],
			"references": 12,
			"revision": 138,
			"time": "2020-04-12T21:44:25-04:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}