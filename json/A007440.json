{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007440",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7440,
			"id": "M0413",
			"data": "1,-1,0,2,-3,-1,11,-15,-13,77,-86,-144,595,-495,-1520,4810,-2485,-15675,39560,-6290,-159105,324805,87075,-1592843,2616757,2136539,-15726114,20247800,32296693,-152909577,145139491,417959049,-1460704685,885536173,4997618808,-13658704994",
			"name": "Reversion of g.f. for Fibonacci numbers 1, 1, 2, 3, 5, ....",
			"comment": [
				"Binomial transform of A104565 (reversion of Pell numbers). - _Paul Barry_, Mar 15 2005",
				"From _Paul Barry_, Nov 03 2008: (Start)",
				"Hankel transform of a(n) (starting 0,1,-1,..) is F(n)*(-1)^C(n+1,2).",
				"Hankel transform of a(n+1) is (-1)^C(n+1,2).",
				"Hankel transform of a(n+2) is F(n+2)*(-1)^C(n+2,2).",
				"(End)",
				"The sequence 1,1,-1,0,2,... given by 0^n + Sum_{k=0..floor((n-1)/2)} binomial(n-1,2k)*A000108(k)*(-1)^(n-k-1) has Hankel transform F(n+2)*(-1)^binomial(n+1,2). - _Paul Barry_, Jan 13 2009",
				"Apart from signs, essentially the same as A343773. For odd terms, a(n) = A343773(n-1), while a(n) = -A343773(n-1) if n is even. - _Gennady Eremin_, May 19 2021"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Gennady Eremin, \u003ca href=\"/A007440/b007440.txt\"\u003eTable of n, a(n) for n = 1..800\u003c/a\u003e (first 300 terms from Vincenzo Librandi)",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.pdmi.ras.ru/~lowdimma/BSD/abramowitz_and_stegun.pdf\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards Applied Math. Series 55, Tenth Printing, 1972 (p. 16, Reversion of Series 3.6.25).",
				"Paul Barry, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL13/Barry1/barry95r.html\"\u003eGeneralized Catalan Numbers, Hankel Transforms and Somos-4 Sequences \u003c/a\u003e, J. Int. Seq. 13 (2010) #10.7.2.",
				"Paul Barry, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL14/Barry3/barry132.html\"\u003eOn the Central Coefficients of Bell Matrices\u003c/a\u003e, J. Int. Seq. 14 (2011) # 11.4.3, page 7.",
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/2104.01644\"\u003eCentered polygon numbers, heptagons and nonagons, and the Robbins numbers\u003c/a\u003e, arXiv:2104.01644 [math.CO], 2021.",
				"Gennady Eremin, \u003ca href=\"https://arxiv.org/abs/2108.10676\"\u003eWalking in the OEIS: From Motzkin numbers to Fibonacci numbers. The \"shadows\" of Motzkin numbers\u003c/a\u003e, arXiv:2108.10676 [math.CO], 2021.",
				"\u003ca href=\"/index/Res#revert\"\u003eIndex entries for reversions of series\u003c/a\u003e"
			],
			"formula": [
				"D-finite with recurrence (n+3)*a(n+2) = -(2*n + 3)*a(n+1) - 5*n*a(n), a(1) = 1, a(2) = -1.",
				"G.f.: A(x) = (-1 - x + sqrt(1 + 2*x + 5*x^2))/(2*x).",
				"a(n) = Sum_{k=0..floor(n/2)} binomial(n, 2k)*C(k)*(-1)^(n-k), where C(n) is A000108(n). - _Paul Barry_, May 16 2005",
				"a(n) = (5^((n+1)/2)*LegendreP(n-1,-1/sqrt(5)) + 5^(n/2)*LegendreP(n,-1/sqrt(5)))/(2*n+2). - _Mark van Hoeij_, Jul 02 2010",
				"a(n) = 2^(-n-1)*Sum_{k=floor((n-1)/2)..n} binomial(k+1,n-k)*5^(n-k)*(-1)^k*C(k), n \u003e 0, where C(k) is A000108. - _Vladimir Kruchinin_, Sep 21 2010",
				"G.f.: (G(0)-x-1)/(x^2) = 1/G(0) where G(k) = 1 + x + x^2/G(k+1); (continued fraction). - _Sergei N. Gladkovskii_, Dec 25 2011",
				"From _Peter Bala_, Jun 23 2015: (Start)",
				"Lucas(n) = [x^n] (x/A(x))^n for n \u003e= 1.",
				"-1/A(-x) = 1/x - 1 + x + x^2 - 2*x^4 - 3*x^5 + x^6 + 11*x^7 + 15*x^8 - 13*x^9 + ... is the Laurent series generating function for A214649. (End)",
				"a(n) = (-1)^n*hypergeom([1/2 - n/2, -n/2], [2], -4). - _Peter Luschny_, Mar 19 2018",
				"From _Gennady Eremin_, May 09 2021: (Start)",
				"a(n) = -(-1)^n * A343773(n-1), n \u003e 0.",
				"G.f.: A(x) = x*B(-x), where B(x) is the g.f. of A343773.",
				"Limit_{n-\u003einfinity} a(n)/A001006(n) = 0. (End)",
				"G.f. A(x) satisfies A(x) + 1 + x^-1 = 1/A(x). - _Gennady Eremin_, May 29 2021"
			],
			"example": [
				"G.f. = x - x^2 + 2*x^4 - 3*x^5 - x^6 + 11*x^7 - 15*x^8 - 13*x^9 + 77*x^10 - 86*x^11 - 144*x^12 + ..."
			],
			"maple": [
				"a := n -\u003e (-1)^n*hypergeom([1/2 - n/2, -n/2], [2], -4):",
				"seq(simplify(a(n)), n=0..35); # _Peter Luschny_, Mar 19 2018"
			],
			"mathematica": [
				"a[1] = 1; a[2] = -1; a[n_] := a[n] = (-5*(n-2)*a[n-2] + (1-2*n)*a[n-1])/(n+1); Array[a, 36] (* _Jean-François Alcover_, Apr 18 2014 *)",
				"Rest[CoefficientList[Series[(-1-x+Sqrt[1+2*x+5*x^2])/(2*x),{x,0,20}],x]] (* _Vaclav Kotesovec_, Apr 25 2015 *)"
			],
			"program": [
				"(PARI) a(n)=polcoeff((-1-x+sqrt(1+2*x+5*x^2+x^2*O(x^n)))/(2*x),n)",
				"(PARI) Vec(serreverse(x/(1-x-x^2)+O(x^66))) /* _Joerg Arndt_, Aug 19 2012 */",
				"(Sage)",
				"def A007440_list(len):",
				"    T = [0]*(len+1); T[1] = 1; R = [1]",
				"    for n in (1..len-1):",
				"        a,b,c = 1,0,0",
				"        for k in range(n,-1,-1):",
				"            r = a - b - c",
				"            if k \u003c n : T[k+2] = u;",
				"            a,b,c = T[k-1],a,b",
				"            u = r",
				"        T[1] = u; R.append(u)",
				"    return R",
				"A007440_list(36) # _Peter Luschny_, Nov 01 2012",
				"(Python)",
				"A007440 = [0, 1, -1]",
				"for n in range(3, 801):",
				"    A007440.append( (-(2*n-1)*A007440[-1]",
				"      - 5*(n-2)*A007440[-2])//(n+1) )",
				"for n in range(1, 801):",
				"    print(n, A007440[n])  # _Gennady Eremin_, May 10 2021"
			],
			"xref": [
				"Cf. A000045, A000032, A214649, A291535, A343773."
			],
			"keyword": "sign,changed",
			"offset": "1,4",
			"author": "_N. J. A. Sloane_, May 24 1994",
			"ext": [
				"Extended and signs added by _Olivier Gérard_",
				"Second formula adapted to offset by _Vaclav Kotesovec_, Apr 25 2015"
			],
			"references": 20,
			"revision": 124,
			"time": "2022-01-15T00:51:53-05:00",
			"created": "1994-05-24T03:00:00-04:00"
		}
	]
}