{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001918",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1918,
			"id": "M0242 N0083",
			"data": "1,2,2,3,2,2,3,2,5,2,3,2,6,3,5,2,2,2,2,7,5,3,2,3,5,2,5,2,6,3,3,2,3,2,2,6,5,2,5,2,2,2,19,5,2,3,2,3,2,6,3,7,7,6,3,5,2,6,5,3,3,2,5,17,10,2,3,10,2,2,3,7,6,2,2,5,2,5,3,21,2,2,7,5,15,2,3,13,2,3,2,13,3,2,7,5,2,3,2,2,2,2,2,3",
			"name": "Least positive primitive root of n-th prime.",
			"comment": [
				"If k is a primitive root of p=4m+1, then p-k is too. If k is a primitive root of p=4m+3, then p-k isn't, but has order 2m+1. - _Jon Perry_, Sep 07 2014"
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), p. 864.",
				"T. M. Apostol, Introduction to Analytic Number Theory, Springer-Verlag, 1976, page 213.",
				"CRC Handbook of Combinatorial Designs, 1996, p. 615.",
				"P. Fan and M. Darnell, Sequence Design for Communications Applications, Wiley, NY, 1996, Table A.1.",
				"G. H. Hardy and E. M. Wright, An Introduction to the Theory of Numbers, 5th ed., Oxford Univ. Press, 1979, th. 111",
				"Hua Loo Keng, Introduction To Number Theory, 'Table of least primitive roots for primes less than 50000', pp. 52-6, Springer NY 1982.",
				"R. Osborn, Tables of All Primitive Roots of Odd Primes Less Than 1000, Univ. Texas Press, 1961.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A001918/b001918.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"Loo-keng Hua, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9904-1942-07767-6\"\u003eOn the least primitive root of a prime\u003c/a\u003e, Bull. Amer. Math. Soc. 48 (1942), 726-730.",
				"K. Matthews, \u003ca href=\"http://www.numbertheory.org/php/lprimroot.html\"\u003eFinding the least primitive root (mod p), p an odd prime\u003c/a\u003e",
				"T. Oliveira e Silva, \u003ca href=\"http://sweet.ua.pt/tos/p_roots.html\"\u003eLeast primitive root of prime numbers\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PrimitiveRoot.html\"\u003ePrimitive Root.\u003c/a\u003e"
			],
			"example": [
				"modulo 7: 3^6=1, 3^2=2, 3^7=3, 3^4=4, 3^5=5, 3^3=6, 7=p(4), 3=a(4)."
			],
			"maple": [
				"A001918 := proc(n)",
				"        numtheory[primroot](ithprime(n)) ;",
				"end proc:"
			],
			"mathematica": [
				"Table[PrimitiveRoot@Prime@n, {n, 101}] (* _Robert G. Wilson v_, Dec 15 2005 *)",
				"PrimitiveRoot[Prime[Range[110]]] (* _Harvey P. Dale_, Jan 13 2013 *)"
			],
			"program": [
				"(PARI) for(x=1, 1000, print1(lift(znprimroot(prime(x))), \", \"))",
				"(Sage) [primitive_root(p) for p in primes(570)] # _Zerinvary Lajos_, May 24 2009"
			],
			"xref": [
				"A column of A060749. Cf. A002233."
			],
			"keyword": "nonn,nice,easy",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"references": 94,
			"revision": 70,
			"time": "2021-12-23T22:44:39-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}