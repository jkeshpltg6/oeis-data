{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A289827",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 289827,
			"data": "0,2,2,4,2,2,1,1,4,10,2,2,1,1,4,4,2,2,1,1,2,2,1,1,1,1,4,4,2,2,1,1,1,1,2,2,1,1,4,4,2,2,1,1,2,2,1,1,1,1,2,2,1,1,1,1,4,4,2,2,1,1,1,1,2,2,1,1,4,4,2,2,1,1,1,1,2,2,1,1,2,2,1,1,1,1,2,2,1,1,1,1,1,1,2,2,1,1,10,10",
			"name": "a(n) = largest m \u003c= n such that pi(m + n) = pi(m) + pi(n), where pi function is A000720 (with pi(0) = 0).",
			"comment": [
				"It seems that the sequence is bounded, namely a(n) \u003c= 10.",
				"We have a(n) = 10 for n = 10, 99, 100, 189, 190, 819, 820, ...",
				"For n \u003e 9; a(n) = a(n+1) = 10 if and only if n+2 is in A007530.",
				"First conjecture: for n \u003e 1, all a(n) belong to the set {1, 2, 4, 10}.",
				"Second Hardy-Littlewood conjecture: pi(x+y) \u003c= pi(x) + pi(y) for x,y \u003e= 2.",
				"Third conjecture (T. Ordowski): pi(x+y) \u003c pi(x) + pi(y) for x,y \u003e= 11.",
				"Carl Pomerance (in a letter to the author) wrote: I believe if correct, your conjecture would disprove the Hardy-Littlewood prime k-tuples conjecture, as shown by Hensley and Richards over 30 years ago. They showed that prime k-tuples implies that there are pairs y \u003c x with pi(x+y) \u003e= pi(x) + pi(y) and pi(y) arbitrarily large. Since pi(2x) \u003c 2*pi(x), by increasing y in a y,x example, one would come on a new pair y' \u003c x with pi(x+y') = pi(x) + pi(y'). - _Thomas Ordowski_, Aug 14 2017",
				"By the k-tuple conjecture, the smallest a(n) \u003e 10 is 1418 for some n \u003e 10^100. - _Nathan McNew_, Aug 17 2017",
				"a(n) \u003e 0 for n \u003e 1."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A289827/b289827.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Douglas Hensley and Ian Richards, \u003ca href=\"http://pldml.icm.edu.pl/pldml/element/bwmeta1.element.bwnjournal-article-aav25i4p375bwm?q=bwmeta1.element.bwnjournal-number-aa-1973-1974-25-4;7\u0026amp;qt=CHILDREN-STATELESS\"\u003ePrimes in Intervals\u003c/a\u003e. Acta Mathematica 25,4 (1973/1974) 375-391.",
				"Ian Richards, \u003ca href=\"http://www.ams.org/journals/bull/1974-80-03/home.html\"\u003e On the Incompatibility of Two Conjectures Concerning Primes;...\u003c/a\u003e, Bull. Amer. Math. Soc. 80,3 (1974) 419-438.",
				"Eric Weisstein's MathWorld, \u003ca href=\"http://mathworld.wolfram.com/Hardy-LittlewoodConjectures.html\"\u003eHardy-Littlewood conjectures\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Second_Hardy%E2%80%93Littlewood_conjecture\"\u003eSecond Hardy-Littlewood conjecture\u003c/a\u003e"
			],
			"maple": [
				"f:= proc(n) local m;",
				"  for m from n by -1 do",
				"    if numtheory:-pi(m+n)=numtheory:-pi(m)+numtheory:-pi(n)",
				"        then return m",
				"      fi",
				"  od",
				"end proc:",
				"map(f, [$1..100]); # _Robert Israel_, Aug 14 2017"
			],
			"mathematica": [
				"Table[SelectFirst[Range[n, 0, -1], PrimePi[# + n] == PrimePi[#] + PrimePi[n] \u0026], {n, 100}] (* _Michael De Vlieger_, Aug 15 2017 *)",
				"f[n_] := Block[{m = n, p = PrimePi@ n}, While[ PrimePi[m + n] != PrimePi[m] + p, m--]; m]; Array[f, 103] (* _Robert G. Wilson v_, Aug 30 2017 *)"
			],
			"program": [
				"(PARI) a(n) = my(m=n); while(1, if(primepi(m+n)==primepi(m)+primepi(n), return(m)); m--) \\\\ _Felix Fröhlich_, Aug 13 2017"
			],
			"xref": [
				"Cf. A000720, A007530."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Thomas Ordowski_, Aug 13 2017",
			"ext": [
				"More terms from _Altug Alkan_ and _Robert Israel_, Aug 13 2017"
			],
			"references": 1,
			"revision": 73,
			"time": "2017-10-05T15:55:57-04:00",
			"created": "2017-10-05T15:55:57-04:00"
		}
	]
}