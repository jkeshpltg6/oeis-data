{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A228781",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 228781,
			"data": "-3,1,5,-10,1,-7,35,-21,1,-3,27,-33,1,-11,165,-462,330,-55,1,13,-286,1287,-1716,715,-78,1,1,-28,134,-92,1,17,-680,6188,-19448,24310,-12376,2380,-136,1,-19,969,-11628,50388,-92378,75582,-27132,3876,-171,1,1,-58,655,-1772,1423,-186,1",
			"name": "  Irregular triangle read by rows: coefficients of minimal polynomial of a certain algebraic number S2(2*k+1) from Q(2*cos(Pi/n)) related to the regular (2*k+1)-gon, k \u003e= 1.",
			"comment": [
				"The row length sequence of this table is delta(2*k+1), with the degree delta(n) = A055034(n) of the algebraic number rho(n):= 2*cos(Pi/n), k \u003e= 1.",
				"The numbers S2(n) have been given in A228780 in the power basis of the degree delta(n) number field Q(rho(n)), with rho(n):= 2*cos(Pi/n), n \u003e= 2. Here the odd n case, n = 2*k + 1 is considered. S2(n) is the square of the sum of the distinct length ratios side/radius or diagonal/radius with the radius of the circle in which a regular n-gon is inscribed. For two formulas for S2(n) in terms of powers of rho(n) see the comment section of A228780.",
				"The minimal (monic) polynomial of S2(2*k+1) has degree delta(2*k+1) and is given by",
				"  p(2*k+1,x) = Product_{j=1..delta(2*k+1)} (x - S2(2*k+1)^{(j-1)} (mod C(2*k+1,delta(n))) = sum(a(k, m)*x^m, m = 0..delta(2*k+1)), where S2(2*k+1)^{(0)} = S2(2*k+1) and S2(2*k+1)^{(j-1)} is the (j-1)-th conjugate of S2(2*k+1). The conjugate of a number alpha(n) = Sum_{j=0..(delta(n)-1)} b(n, j)*rho(n)^j in Q(rho(n)) is obtained from the conjugates of rho(n), given in turn by the zeros x(n, j) of the minimal polynomial C(n, x) (see A187360 and the link to the W. Lang Galois paper, tables 2 and 3) as rho(n)^{(j-1)} = x(n, j), j = 1..delta(n), with rho(n)^{(0)} = rho(n).",
				"The motivation to look into this problem originated from emails by _Seppo Mustonen_ who found experimentally polynomials which had as one zero the square of the total length/radius of all chords (sides and diagonals) in the regular n-gon. See his paper given as a link below. The author thanks _Seppo Mustonen_ for sending his paper.",
				"If the minimal polynomial of the algebraic number S2(n) in the n-gon with n = 2*k+1 is p(n, x) then the minimal polynomial of the square of the sum of the length of all n sides and n*(n-3)/2 diagonals is P(n, x) = n^(2*delta(n))*p(n, x/n^2)."
			],
			"link": [
				"Wolfdieter Lang, \u003ca href=\"http://arxiv.org/abs/1210.1018\"\u003eThe field Q(2cos(pi/n)), its Galois group and length ratios in the regular n-gon\u003c/a\u003e, arXiv:1210.1018 [math.GR], 2012-2017.",
				"Seppo Mustonen, \u003ca href=\"http://www.survo.fi/papers/Roots2013.pdf\"\u003eLengths of edges and diagonals and sums of them in regular polygons as roots of algebraic equations\u003c/a\u003e.",
				"Seppo Mustonen, \u003ca href=\"/A108716/a108716.pdf\"\u003eLengths of edges and diagonals and sums of them in regular polygons as roots of algebraic equations\u003c/a\u003e [Local copy]"
			],
			"formula": [
				"a(k, m) = [x^m] p(2*k+1, x), with the minimal polynomial p(2*k+1, x) of S2(2*k+1) given in the power basis in A228780. p(2*k+1, x) is given in a comment above in terms of the S2(2*k+1) and its conjugates S2(2*k+1)^{(j-1)}, j=2, ..., delta(2*k+1), where delta(n) = A055034(n).",
				"Conjecture from _Seppo Mustonen_, rewritten for the p(n, x) coefficients for odd primes: p(prime(j), x) =  Sum_{i=0..imax(j)} (-1)^(imax(j - i))* binomial(prime(j), 2*i+1)*x^i, with imax(j) = (prime(j)-1)/2. See the adapted eq. (5) of the S. Mustonen paper."
			],
			"example": [
				"The irregular triangle a(k, m) begins:",
				"n   k /m 0     1     2       3      4       5     6    7   8",
				"3   1:  -3     1",
				"5   2:   5   -10     1",
				"7   3:  -7    35   -21       1",
				"9   4:  -3    27   -33       1",
				"11  5: -11   165  -462     330    -55      1",
				"13  6:  13  -286  1287   -1716    715    -78      1",
				"15  7:   1   -28   134     -92      1",
				"17  8:  17  -680  6188  -19448  24310  -12376  2380 -136   1",
				"...",
				"n = 19, L = 9: -19, 969, -11628, 50388, -92378, 75582, -27132, 3876, -171, 1.",
				"n = 21, L = 10: 1, -58, 655, -1772, 1423, -186, 1.",
				"p(5, x) = (x - S2(5))*(x - S2(5)^{(1)}), with S2(5) = 3 + 4*rho(5), where rho(5)=phi, the golden section. C(5, x) = x^2 - x - 1 = (x - rho(5))*(x - (1-rho(5))), hence rho(5)^{(1)} = 1-rho(5), and S2(5)^{(1)} = 3 + 4*(1 - rho(5)) = 7 - 4*rho(5). Thus p(5, x) = -16*rho^2 + 21 + 16*rho -10*x + x^2 which becomes modulo C(5,rho(5)), i.e., using rho(5)^2 = rho(5) + 1, finally p(n, 5) = 5 - 10*x + x^2.",
				"Conjecture (_Seppo Mustonen_): p(5, x) = binomial(5, 1) - binomial(5, 3)*x + binomial(5, 5)* x^2 = 5 - 10*x + x^2."
			],
			"xref": [
				"Cf. A055034, A187360, A228780, A228782 (even case)."
			],
			"keyword": "sign,tabf",
			"offset": "1,1",
			"author": "_Wolfdieter Lang_, Oct 01 2013",
			"references": 3,
			"revision": 28,
			"time": "2021-04-13T11:24:26-04:00",
			"created": "2013-10-04T11:24:20-04:00"
		}
	]
}