{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A262990",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 262990,
			"data": "1,1,2,3,5,9,10,20,22,40,51,67,114,126,203,230,354,468,571,885,908,1486,1674,2250,3045,3586,5418,5322,8186,9560,12234,16341,17976,27912,26970,38435,46383,57024,76794,80805,125205,116376,165914,201580,232352,322980,332388,508710,456154,645661,800495,886018,1241190,1226382,1916682,1693454,2290704,2935492",
			"name": "G.f. A(x) satisfies: a([n/r^2]) = [x^n] A(x)^2/x and a([n/r^3]) = [x^n] A(x)^3/x^2, for n\u003e=1, where r^2 + r^3 = 1.",
			"comment": [
				"The integer floor values, [n/r^2] and [n/r^3] where r^2 + r^3 = 1, form Beatty sequences and thus together contain all the positive integers without repetition.",
				"Here r = 6 / ( (108 + 12*sqrt(69))^(1/3) + (108 - 12*sqrt(69))^(1/3) ) = 0.75487766624669276.... satisfies r^2 + r^3 = 1.",
				"Not equal to A090845.",
				"What is the rate of growth of this sequence?"
			],
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A262990/b262990.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e"
			],
			"example": [
				"G.f.: A(x) = x + x^2 + 2*x^3 + 3*x^4 + 5*x^5 + 9*x^6 + 10*x^7 + 20*x^8 + 22*x^9 + 40*x^10 + 51*x^11 + 67*x^12 + 114*x^13 + 126*x^14 + 203*x^15 +...",
				"where the terms are formed from the union of coefficients in A(x)^2 and A(x)^3.",
				"The coefficients of A(x)^2 begin:",
				"A^2 = [1, 2, 5, 10, 20, 40, 67, 126, 203, 354, 571, 908, 1486, 2250, 3586, 5322, 8186, 12234, 17976, 26970, 38435, 57024, 80805, 116376, 165914, 232352,...]",
				"and form the terms of this sequence at positions [n/r^2] for n\u003e=1:",
				"{[n/r^2]} = [1, 3, 5, 7, 8, 10, 12, 14, 15, 17, 19, 21, 22, 24, 26, 28, 29, 31, 33, 35, 36, 38, 40, 42, 43, 45, 47, 49, 50, 52, 54, 56, 57, 59, ...].",
				"The coefficients of A(x)^3 begin:",
				"A^3 = [1, 3, 9, 22, 51, 114, 230, 468, 885, 1674, 3045, 5418, 9560, 16341, 27912, 46383, 76794, 125205, 201580, 322980, 508710, 800495, 1241190, ...]",
				"and form the terms of this sequence at positions [n/r^3] for n\u003e=1:",
				"{[n/r^3]} = [2, 4, 6, 9, 11, 13, 16, 18, 20, 23, 25, 27, 30, 32, 34, 37, 39, 41, 44, 46, 48, 51, 53, 55, 58, 60, 62, 65, 67, 69, 72, ...]."
			],
			"program": [
				"(PARI) {a(n) = local(A=vector(n+1),B=A,C=A,r=6/((108+12*sqrt(69))^(1/3)+(108-12*sqrt(69))^(1/3))); A[1]=1;A[2]=1;",
				"for(i=1,ceil(log(#A)/log(1/r)),",
				"B=vector(floor(#A/r^2));for(n=1,#A,m=floor(n/r^2);if(m\u003c=#B,B[m]=Vec(Ser(A)^2)[n]));",
				"C=vector(floor(#A/r^3));for(n=1,#A,m=floor(n/r^3);if(m\u003c=#C,C[m]=Vec(Ser(A)^3)[n]));",
				"A=vector(#A,n,if(C[n]==0,B[n],C[n]));); A[n]}",
				"for(n=1,80,print1(a(n),\", \"))"
			],
			"xref": [
				"Cf. A075778."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Paul D. Hanna_, Oct 06 2015",
			"references": 2,
			"revision": 23,
			"time": "2015-10-06T22:33:14-04:00",
			"created": "2015-10-06T17:53:42-04:00"
		}
	]
}