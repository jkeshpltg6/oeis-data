{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A035506",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 35506,
			"data": "1,2,4,3,6,7,5,10,11,9,8,16,18,15,12,13,26,29,24,19,14,21,42,47,39,31,23,17,34,68,76,63,50,37,28,20,55,110,123,102,81,60,45,32,22,89,178,199,165,131,97,73,52,36,25,144,288,322,267,212,157,118,84,58,40,27,233,466,521,432,343,254,191,136,94,65,44,30",
			"name": "Stolarsky array read by antidiagonals.",
			"comment": [
				"Inverse of sequence A064357 considered as a permutation of the positive integers. - _Howard A. Landman_, Sep 25 2001",
				"PARI/GP script gives general solution for the Stolarsky array in square array form by row,column. Increase the default precision to compute large values in the array. - Randall L. Rathbun (randallr(AT)abac.com), Jan 25 2002",
				"The Stolarsky array is the dispersion of the sequence s given by s(n)=(integer nearest n*x), where x=(golden ratio).  For a discussion of dispersions, see A191426.",
				"See A098861 for the row in which is a given number. - _M. F. Hasler_, Nov 05 2014",
				"Named after the American mathematician Kenneth Barry Stolarsky. - _Amiram Eldar_, Jun 11 2021"
			],
			"reference": [
				"C. Kimberling, \"Stolarsky interspersions,\" Ars Combinatoria 39 (1995) 129-138."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A035506/b035506.txt\"\u003eAntidiagonals n = 0..140, flattened\u003c/a\u003e",
				"Clark Kimberling, \u003ca href=\"http://faculty.evansville.edu/ck6/integer/intersp.html\"\u003eInterspersions\u003c/a\u003e.",
				"Clark Kimberling, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9939-1993-1111434-0\"\u003eInterspersions and dispersions\u003c/a\u003e, Proceedings of the American Mathematical Society, Vol. 117 (1993), pp. 313-321.",
				"David R. Morrison, \u003ca href=\"https://web.math.ucsb.edu/~drm/papers/stolarsky.pdf\"\u003eA Stolarsky array of Wythoff pairs\u003c/a\u003e, A collection of manuscripts related to the Fibonacci sequence, Santa Clara, CA: Fibonacci Association, 1980, pp. 134-136.",
				"N. J. A. Sloane, \u003ca href=\"/classic.html#WYTH\"\u003eClassic Sequences\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/StolarskyArray.html\"\u003eStolarsky arrays\u003c/a\u003e.",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"T(1,k) = 2*T(0,k+1); T(3,k) = 3*T(0,k+2). - _M. F. Hasler_, Nov 05 2014"
			],
			"example": [
				"Top left corner of the array is:",
				"   1    2    3    5    8   13   21   34   55",
				"   4    6   10   16   26   42   68  110  178",
				"   7   11   18   29   47   76  123  119  322",
				"   9   15   24   39   63  102  165  267  432",
				"  12   19   31   50   81  131  212  343  555",
				"  14   23   37   60   97  157  254  411  665"
			],
			"maple": [
				"A:= proc(n, k) local t, a, b; t:= (1+sqrt(5))/2; a:= floor(n*(t+1)+1 +t/2); b:= round(a*t); (Matrix([[b, a]]). Matrix([[1, 1], [1, 0]])^k) [1, 2] end: seq(seq(A (n, d-n), n=0..d), d=0..10); # _Alois P. Heinz_, Aug 17 2008"
			],
			"mathematica": [
				"(* program generates the dispersion array T of the complement of increasing sequence f[n] *)",
				"r = 40; r1 = 12; (* r=# rows of T, r1=# rows to show *)",
				"c = 40; c1 = 12; (* c=# cols of T, c1=# cols to show *)",
				"x = GoldenRatio; f[n_] := Floor[n*x + 1/2]",
				"(* f(n) is complement of column 1 *)",
				"mex[list_] := NestWhile[#1 + 1 \u0026, 1, Union[list][[#1]] \u003c= #1 \u0026, 1, Length[Union[list]]]",
				"rows = {NestList[f, 1, c]};",
				"Do[rows = Append[rows, NestList[f, mex[Flatten[rows]], r]], {r}];",
				"t[i_, j_] := rows[[i, j]];",
				"TableForm[Table[t[i, j], {i, 1, 10}, {j, 1, 10}]]",
				"(* t=Stolarsky array, A035506 *)",
				"Flatten[Table[t[k, n - k + 1], {n, 1, c1}, {k, 1, n}]]",
				"(* Stolarsky array as a sequence *)",
				"(* Program by _Peter J. C. Moses_, Jun 01 2011 *)"
			],
			"program": [
				"(PARI) {Stolarsky(r,c)= tau=(1+sqrt(5))/2; a=floor(r*(1+tau)-tau/2); b=round(a*tau); if(c==1,a, if(c==2,b, for(i=1,c-2,d=a+b; a=b; b=d; ); d))}"
			],
			"xref": [
				"Cf. A035513 (Wythoff array), A035507 (inverse Stolarksy array), A191426."
			],
			"keyword": "nonn,tabl,easy,nice",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from Larry Reeves (larryr(AT)acm.org), Sep 27 2000",
				"Extended (terms, Mathematica, example) by _Clark Kimberling_, Jun 03 2011",
				"Example corrected by _M. F. Hasler_, Nov 05 2014"
			],
			"references": 52,
			"revision": 46,
			"time": "2021-06-25T23:32:59-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}