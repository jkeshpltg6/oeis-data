{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000757",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 757,
			"id": "M4521 N1915",
			"data": "1,0,0,1,1,8,36,229,1625,13208,120288,1214673,13469897,162744944,2128047988,29943053061,451123462673,7245940789072,123604151490592,2231697509543361,42519034050101745,852495597142800376",
			"name": "Number of cyclic permutations of [n] with no i-\u003ei+1 (mod n)",
			"reference": [
				"Ch. A. Charalambides, Enumerative Combinatorics, Chapman \u0026 Hall/CRC, Boca Raton, Florida, 2002, p. 182 and p. 183, Table 5.6.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"R. P. Stanley, Space Programs Summary. Jet Propulsion Laboratory, California Institute of Technology, Pasadena, California, Vol. 37-40-4 (1966), pp. 208-214.",
				"R. P. Stanley, Enumerative Combinatorics I, Chap. 2, Exercise 8, p. 88."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A000757/b000757.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"Roland Bacher, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v19i3p7\"\u003eCounting Packings of Generic Subsets in Finite Groups\u003c/a\u003e, Electr. J. Combinatorics, 19 (2012), #P7. - _N. J. A. Sloane_, Feb 06 2013",
				"Bhadrachalam Chitturi and Krishnaveni K S, \u003ca href=\"https://arxiv.org/abs/1601.04469\"\u003eAdjacencies in Permutations\u003c/a\u003e, arXiv preprint arXiv:1601.04469 [cs.DM], 2016.",
				"S. M. Jacob, \u003ca href=\"http://dx.doi.org/10.1112/plms/s2-31.1.329\"\u003eThe enumeration of the Latin rectangle of depth three...\u003c/a\u003e, Proc. London Math. Soc., 31 (1928), 329-336.",
				"R. Moreno, L. M. Rivera, \u003ca href=\"http://arxiv.org/abs/1306.5708\"\u003eBlocks in Cycles and k-commuting Permutations\u003c/a\u003e, arXiv preprint arXiv:1306.5708 [math.CO], 2013.",
				"Luis Manuel Rivera, \u003ca href=\"http://arxiv.org/abs/1406.3081\"\u003eInteger sequences and k-commuting permutations\u003c/a\u003e, arXiv preprint arXiv:1406.3081 [math.CO], 2014.",
				"R. P. Stanley, \u003ca href=\"/A000757/a000757.pdf\"\u003ePermutations with no runs of length 2\u003c/a\u003e, Space Programs Summary. Jet Propulsion Laboratory, California Institute of Technology, Pasadena, California, Vol. 37-40-4 (1966), pp. 208-214. [Annotated scanned copy]"
			],
			"formula": [
				"a(n) = (-1)^n + sum((-1)^k*binomial(n, k)*(n-k-1)!, k=0..n-1); e.g.f.: (1 - log(1 - x)) / e^x; a(n) = (n-3) * a(n-1) + (n-2) * (2*a(n-2) + a(n-3)). - _Michael Somos_, Jun 21 2002",
				"a(n) = (n-2) * a(n-1) + (n-1) * a(n-2) - (-1)^n, if n\u003e0. a(n) = (-1)^n + A002741(n). - _Michael Somos_, Jun 21 2002",
				"a(n) = n-th forward difference of [1, 1, 1, 2, 6, 24, ...] (factorials A000142 with 1 prepended). - _Michael Somos_, Mar 28 2011",
				"a(n) = sum(((-1)^(n-j))*D(j-1),j=3..n), n\u003e=3, with the derangements numbers (subfactorials) D(n)=A000166(n).",
				"a(n) + a(n+1) = A000166(n). - _Aaron Meyerowitz_, Feb 08 2014",
				"a(n) ~ exp(-1)*(n-1)! * (1 - 1/n + 1/n^3 + 1/n^4 - 2/n^5 - 9/n^6 - 9/n^7 + 50/n^8 + 267/n^9 + 413/n^10 + ...), numerators are A000587. - _Vaclav Kotesovec_, Jul 03 2016"
			],
			"example": [
				"a(4)=1 because from the 4!/4=6 circular permutations of n=4 elements (1,2,3,4), (1,4,3,2), (1,3,4,2),(1,2,4,3), (1,4,2,3) and (1,3,2,4) only (1,4,3,2) has no successor pair (i,i+1). Note that (4,1) is also a successor pair. - _Wolfdieter Lang_, Jan 21 2008",
				"a(3) = 1 = 2! - 3*1! + 3*0! - 1. a(4) = 1 = 3! - 4*2! + 6*1! - 4*0! + 1. - _Michael Somos_, Mar 28 2011",
				"G.f. = 1 + x^3 + x^4 + 8*x^5 + 36*x^6 + 229*x^7 + 1625*x^8 + 13208*x^9 + ..."
			],
			"mathematica": [
				"a[n_] := (-1)^n + Sum[(-1)^k*n!/((n-k)*k!), {k, 0, n-1}]; Table[a[n], {n, 0, 21}] (* _Jean-François Alcover_, Aug 30 2011, after _Michael Somos_ *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, (-1)^n + sum( k=0, n-1, (-1)^k * binomial( n, k) * (n - k - 1)!))}; /* _Michael Somos_, Jun 21 2002 */"
			],
			"xref": [
				"Cf. A000142, A002741."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,6",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Better description from _Len Smiley_",
				"Additional comments from _Michael Somos_, Jun 21 2002"
			],
			"references": 25,
			"revision": 67,
			"time": "2017-08-20T11:53:06-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}