{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A271106",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 271106,
			"data": "0,0,1,1,1,2,1,2,2,1,2,3,3,1,3,3,1,2,2,2,1,1,2,2,1,1,3,2,2,4,3,3,4,5,3,2,4,4,3,2,4,3,2,2,1,2,3,4,3,2,1,1,2,4,4,2,3,3,2,2,2,3,2,2,2,1,5,5,5,3,4",
			"name": "Number of ordered ways to write n as x^6 + 3*y^3 + z^3 + w*(w+1)/2, where x and y are nonnegative integers, and z and w are positive integers.",
			"comment": [
				"Conjecture: a(n) \u003e 0 for all n \u003e 1, and a(n) = 1 only for n = 2, 3, 4, 6, 9, 13, 16, 20, 21, 24, 25, 44, 50, 51, 65, 84, 189, 290, 484, 616, 664, 680, 917, 1501, 1639, 3013.",
				"Based on our computation, we also formulate the following general conjecture.",
				"General Conjecture: Let T(w) = w*(w+1)/2. We have {P(x,y,z,w): x,y,z,w = 0,1,2,...} = {0,1,2,...} for any of the following polynomials P(x,y,z,w): x^3+y^3+c*z^3+T(w) (c = 2,3,4,6), x^3+y^3+c*z^3+2*T(w) (c = 2,3), x^3+b*y^3+3z^3+3*T(w) (b = 1,2), x^3+2y^3+3z^3+w(5w-1)/2, x^3+2y^3+3z^3+w(5w-3)/2, x^3+2y^3+c*z^3+T(w) (c = 2,3,4,5,6,7,12,20,21,34,35,40), x^3+2y^3+c*z^3+2*T(w) (c = 3,4,5,6,11), x^3+2y^3+c*z^3+w^2 (c = 3,4,5,6), x^3+2y^3+4z^3+w(3w-1)/2, x^3+2y^3+4z^3+w(3w+1)/2, x^3+2y^3+4z^3+w(2w-1), x^3+2y^3+6z^3+w(3w-1)/2, x^3+3y^3+c*z^3+T(w) (c = 3,4,5,6,10,11,13,15,16,18,20), x^3+3y^3+c*z^3+2*T(w) (c = 5,6,11), x^3+4y^3+c*z^3+T(w) (c = 5,10,12,16), x^3+4y^3+5z^3+2*T(w), x^3+5y^3+10z^3+T(w), 2x^3+3y^3+c*z^3+T(w) (c = 4,6), 2x^3+4y^3+8z^3+T(w), x^4+y^3+3z^3+w(3w-1)/2, x^4+y^3+c*z^3+T(w) (c = 2,3,4,5,7,12,13), x^4+y^3+c*z^3+2*T(w) (c = 2,3,4,5), x^4+y^3+2z^3+w^2, x^4+y^3+4z^3+2w^2, x^4+2y^3+c*z^3+T(w) (c = 4,5,12), x^4+2y^3+3z^3+2*T(w), 2x^4+y^3+2z^3+w(3w-1)/2, 2x^4+y^3+c*z^3+T(w) (c = 1,2,3,4,5,6,10,11), 2x^4+y^3+c*z^3+2*T(w) (c = 2,3,4), 2x^4+2y^3+c*z^3+T(w) (c = 3,5), 3x^4+y^3+c*z^3+T(w) (c = 1,2,3,4,5,11), 3x^4+y^3+2z^3+2*T(w), 3x^4+y^3+2z^3+w^2, 3x^4+y^3+2z^3+w(3w-1)/2, 4x^4+y^3+c*z^3+T(w) (c = 2,3,4,6), 4x^4+y^3+2z^3+2*T(w), 5x^4+y^3+c*z^3+T(w) (c = 2,4), a*x^4+y^3+2z^3+T(w) (a = 6,20,28,40), 6x^4+y^3+2z^3+2*T(w), 6x^4+y^3+2z^3+w^2, a*x^4+y^3+3z^3+T(w) (a = 6,8,11), 8x^4+2y^3+4z^3+T(w), x^5+y^3+c*z^3+T(w) (c = 2,3,4), x^5+2y^3+c*z^3+T(w) (c = 3,6,8), 2x^5+y^3+4z^3+T(w), 3x^5+y^3+2z^3+T(w), 5x^5+y^3+c*z^3+T(w) (c = 2,4), x^6+y^3+3z^3+T(w), x^7+y^3+4z^3+T(w), x^4+2y^4+z^3+w^2, x^4+2y^4+2z^3+T(w),  x^4+b*y^4+z^3+T(w) (b = 2,3,4), 2x^4+3y^4+z^3+T(w), a*x^5+y^4+z^3+T(w) (a = 1,2), x^5+2y^4+z^3+T(w).",
				"The polynomials listed in the general conjecture should exhaust all those polynomials P(x,y,z,w) = a*x^i+b*y^j+c*z^k+w*(s*w+/-t)/2 with {P(x,y,z,w): x,y,z,w = 0,1,2,...} = {0,1,2,...}, where a,b,c,s \u003e 0, 0 \u003c= t \u003c= s, s == t (mod 2), i \u003e= j \u003e= k \u003e= 3, a \u003c= b if i = j, and b \u003c= c if j = k."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A271106/b271106.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Z.-W. Sun, \u003ca href=\"http://dx.doi.org/10.4064/aa127-2-1\"\u003eMixed sums of squares and triangular numbers\u003c/a\u003e, Acta Arith. 127(2007), 103-113.",
				"Z.-W. Sun, \u003ca href=\"http://math.scichina.com:8081/sciAe/EN/abstract/abstract517007.shtml\"\u003eOn universal sums of polygonal numbers\u003c/a\u003e, Sci. China Math. 58(2015), 1367-1396."
			],
			"example": [
				"a(9) = 1 since 9 = 0^6 + 3*0^6 + 2^3 + 1*2/2.",
				"a(24) = 1 since 24 = 1^6 + 3*0^6 + 2^3 + 5*6/2.",
				"a(1501) = 1 since 1501 = 2^6 + 3*5^3 + 3^3 + 45*46/2.",
				"a(1639) = 1 since 1639 = 0^6 + 3*6^3 + 1^3 + 44*45/2.",
				"a(3013) = 1 since 3013 = 3^6 + 3*3^3 + 13^3 + 3*4/2."
			],
			"mathematica": [
				"TQ[n_]:=TQ[n]=n\u003e0\u0026\u0026IntegerQ[Sqrt[8n+1]]",
				"Do[r=0;Do[If[TQ[n-x^6-3*y^3-z^3],r=r+1],{x,0,n^(1/6)},{y,0,((n-x^6)/3)^(1/3)},{z,1,(n-x^6-3y^3)^(1/3)}];Print[n,\" \",r];Continue,{n,0,70}]"
			],
			"xref": [
				"Cf. A000217, A000290, A000326, A000578, A000583, A000584, A001014, A005449, A262813, A262824, A262827, A262857, A262880, A266968, A270469, A270488, A270516, A270533, A270559, A270566, A270920, A271026."
			],
			"keyword": "nonn",
			"offset": "0,6",
			"author": "_Zhi-Wei Sun_, Mar 30 2016",
			"references": 4,
			"revision": 35,
			"time": "2016-04-05T22:16:26-04:00",
			"created": "2016-03-30T22:59:07-04:00"
		}
	]
}