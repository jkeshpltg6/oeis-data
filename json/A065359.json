{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A065359",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 65359,
			"data": "0,1,-1,0,1,2,0,1,-1,0,-2,-1,0,1,-1,0,1,2,0,1,2,3,1,2,0,1,-1,0,1,2,0,1,-1,0,-2,-1,0,1,-1,0,-2,-1,-3,-2,-1,0,-2,-1,0,1,-1,0,1,2,0,1,-1,0,-2,-1,0,1,-1,0,1,2,0,1,2,3,1,2,0,1,-1,0,1,2,0,1,2,3,1,2,3,4,2,3,1,2,0,1,2,3,1,2,0,1,-1,0,1,2,0,1,-1,0,-2",
			"name": "Alternating bit sum for n: replace 2^k with (-1)^k in binary expansion of n.",
			"comment": [
				"Notation: (2)[n](-1)",
				"From _David W. Wilson_ and _Ralf Stephan_, Jan 09 2007: (Start)",
				"a(n) is even iff n in A001969; a(n) is odd iff n in A000069.",
				"a(n) == 0 (mod 3) iff n == 0 (mod 3).",
				"a(n) == 0 (mod 6) iff (n == 0 (mod 3) and n/3 not in A036556).",
				"a(n) == 3 (mod 6) iff (n == 0 (mod 3) and n/3 in A036556). (End)",
				"a(n) = A030300(n) - A083905(n). - _Ralf Stephan_, Jul 12 2003",
				"From _Robert G. Wilson v_, Feb 15 2011: (Start)",
				"First occurrence of k and -k: 0, 1, 2, 5, 10, 21, 42, 85, ..., (A000975); i.e., first 0 occurs for 0, first 1 occurs for 1, first -1 occurs at 2, first 2 occurs for 5, etc.;",
				"  a(n)=-3 only if n mod 3 = 0,",
				"  a(n)=-2 only if n mod 3 = 1,",
				"  a(n)=-1 only if n mod 3 = 2,",
				"  a(n)= 0 only if n mod 3 = 0,",
				"  a(n)= 1 only if n mod 3 = 1,",
				"  a(n)= 2 only if n mod 3 = 2,",
				"  a(n)= 3 only if n mod 3 = 0, ..., . (End)",
				"a(n) modulo 2 is the Prouhet-Thue-Morse sequence A010060. - _Philippe Deléham_, Oct 20 2011",
				"In the Koch curve, number the segments starting with n=0 for the first segment. The net direction (i.e., the sum of the preceding turns) of segment n is a(n)*60 degrees. This is since in the curve each base-4 digit 0,1,2,3 of n is a sub-curve directed respectively 0, +60, -60, 0 degrees, which is the net 0,+1,-1,0 of two bits in the sum here. - _Kevin Ryde_, Jan 24 2020"
			],
			"link": [
				"Harry J. Smith, \u003ca href=\"/A065359/b065359.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"J.-P. Allouche and J. Shallit, \u003ca href=\"http://dx.doi.org/10.1016/0304-3975(92)90001-V\"\u003eThe ring of k-regular sequences\u003c/a\u003e, Theoretical Computer Sci., 98 (1992), 163-197. [\u003ca href=\"http://www.math.jussieu.fr/~allouche/kreg2.ps\"\u003ePreprint\u003c/a\u003e.]",
				"J.-P. Allouche and J. Shallit, \u003ca href=\"http://dx.doi.org/10.1016/S0304-3975(03)00090-2\"\u003eThe ring of k-regular sequences, II\u003c/a\u003e, Theoret. Computer Sci., 307 (2003), 3-29.",
				"H.-K. Hwang, S. Janson and T.-H. Tsai. \u003ca href=\"http://algo.stat.sinica.edu.tw/hk/wp-content/files/2016/12/aat-hhrr-1.pdf\"\u003eExact and asymptotic solutions of the recurrence f(n) = f(floor(n/2)) + f(ceiling(n/2)) + g(n): theory and applications\u003c/a\u003e. Preprint, 2016.",
				"H.-K. Hwang, S. Janson and T.-H. Tsai. \u003ca href=\"http://doi.org/10.1145/3127585\"\u003eExact and Asymptotic Solutions of a Divide-and-Conquer Recurrence Dividing at Half: Theory and Applications\u003c/a\u003e. ACM Transactions on Algorithms, 13:4 (2017), #47. DOI:10.1145/3127585.",
				"Helge von Koch, \u003ca href=\"http://archive.org/details/actamathematica11lefgoog\"\u003eUne Méthode Géométrique Élémentaire pour l'Étude de Certaines Questions de la Théorie des Courbes Planes\u003c/a\u003e, Acta Arithmetica, volume 30, 1906, pages 145-174.",
				"William Paulsen, wpaulsen(AT)csm.astate.edu, \u003ca href=\"http://myweb.astate.edu/wpaulsen/primemaze/mazepart.html\"\u003ePartitioning the [prime] maze\u003c/a\u003e",
				"Ralf Stephan, \u003ca href=\"https://arxiv.org/abs/math/0307027\"\u003eDivide-and-conquer generating functions. I. Elementary sequences\u003c/a\u003e, arXiv:math/0307027 [math.CO], 2003.",
				"Ralf Stephan, \u003ca href=\"/somedcgf.html\"\u003eSome divide-and-conquer sequences ...\u003c/a\u003e",
				"Ralf Stephan, \u003ca href=\"/A079944/a079944.ps\"\u003eTable of generating functions\u003c/a\u003e"
			],
			"formula": [
				"G.f.: (1/(1-x)) * Sum_{k\u003e=0} (-1)^k*x^2^k/(1+x^2^k). - _Ralf Stephan_, Mar 07 2003",
				"a(0) = 0, a(2n) = -a(n), a(2n+1) = 1-a(n). - _Ralf Stephan_, Mar 07 2003",
				"a(n) = Sum_{k\u003e=0} A030308(n,k)*(-1)^k. - _Philippe Deléham_, Oct 20 2011",
				"a(n) = -a(floor(n/2)) + n mod 2. - _Reinhard Zumkeller_, Mar 20 2015",
				"a(n) = A139351(n) - A139352(n). - _Kevin Ryde_, Jan 24 2020",
				"G.f. A(x) satisfies: A(x) = x / (1 - x^2) - (1 + x) * A(x^2). - _Ilya Gutkovskiy_, Jul 28 2021"
			],
			"example": [
				"Alternating bit sum for 11 = 1011 in binary is 1 - 1 + 0 - 1 = -1, so a(11) = -1."
			],
			"maple": [
				"A065359 := proc(n) local dgs ; dgs := convert(n,base,2) ; add( -op(i,dgs)*(-1)^i,i=1..nops(dgs)) ; end proc: # _R. J. Mathar_, Feb 04 2011"
			],
			"mathematica": [
				"f[0]=0; f[n_] := Plus @@ (-(-1)^Range[ Floor[ Log2@ n + 1]] Reverse@ IntegerDigits[n, 2]); Array[ f, 107, 0]"
			],
			"program": [
				"(PARI)",
				"SumAD(x)= { local(a=1, s=0); while (x\u003e9, s+=a*(x-10*(x\\10)); x\\=10; a=-a); return(s + a*x) }",
				"baseE(x, b)= { local(d, e=0, f=1); while (x\u003e0, d=x-b*(x\\b); x\\=b; e+=d*f; f*=10); return(e) }",
				"{ for (n=0, 1000, b=baseE(n, 2); write(\"b065359.txt\", n, \" \", SumAD(b)) ) } \\\\ _Harry J. Smith_, Oct 17 2009",
				"(PARI) for(n=0,106,s=0;u=1;for(k=0,#binary(n)-1,s+=bittest(n,k)*u;u=-u);print1(s,\", \")) /* _Washington Bomfim_, Jan 18 2011 */",
				"(Haskell)",
				"a065359 0 = 0",
				"a065359 n = - a065359 n' + m where (n', m) = divMod n 2",
				"-- _Reinhard Zumkeller_, Mar 20 2015",
				"(Python)",
				"def a(n):",
				"    return sum((-1)**k for k, bi in enumerate(bin(n)[2:][::-1]) if bi=='1')",
				"print([a(n) for n in range(107)]) # _Michael S. Branicky_, Jul 13 2021"
			],
			"xref": [
				"Cf. A005536 (partial sums), A056832 (abs first differences), A010060 (mod 2), A039004 (indices of 0's).",
				"Cf. A000120, A065360, A065368, A065081.",
				"Cf. A004718."
			],
			"keyword": "base,easy,sign",
			"offset": "0,6",
			"author": "_Marc LeBrun_, Oct 31 2001",
			"ext": [
				"More terms from _Ralf Stephan_, Jul 12 2003"
			],
			"references": 39,
			"revision": 82,
			"time": "2021-07-28T22:38:19-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}