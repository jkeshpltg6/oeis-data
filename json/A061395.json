{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A061395",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 61395,
			"data": "0,1,2,1,3,2,4,1,2,3,5,2,6,4,3,1,7,2,8,3,4,5,9,2,3,6,2,4,10,3,11,1,5,7,4,2,12,8,6,3,13,4,14,5,3,9,15,2,4,3,7,6,16,2,5,4,8,10,17,3,18,11,4,1,6,5,19,7,9,4,20,2,21,12,3,8,5,6,22,3,2,13,23,4,7,14,10,5,24,3,6,9,11,15",
			"name": "Let p be the largest prime factor of n; if p is the k-th prime then set a(n) = k; a(1) = 0 by convention.",
			"comment": [
				"Records occur at the primes. - _Robert G. Wilson v_, Dec 30 2007.",
				"For n \u003e 1: length of n-th row in A067255. - _Reinhard Zumkeller_, Jun 11 2013",
				"a(n) = the largest part of the partition having Heinz number n. We define the Heinz number of a partition p = [p_1, p_2, ..., p_r] as Product(p_j-th prime, j=1...r) (concept used by _Alois P. Heinz_ in A215366 as an \"encoding\" of a partition). For example, for the partition [1, 1, 2, 4, 10] we get 2*2*3*7*29 = 2436. Example: a(20) = 3; indeed, the partition having Heinz number 20 = 2*2*5 is [1,1,3]. - _Emeric Deutsch_, Jun 04 2015"
			],
			"link": [
				"Álvar Ibeas, \u003ca href=\"/A061395/b061395.txt\"\u003eTable of n, a(n) for n = 1..100000\u003c/a\u003e (first 1000 terms from Harry J. Smith)",
				"\u003ca href=\"/index/Pri#prime_indices\"\u003eIndex entries for sequences computed from indices in prime factorization\u003c/a\u003e"
			],
			"formula": [
				"A000040(a(n)) = A006530(n); a(n) = A049084(A006530(n)). - _Reinhard Zumkeller_, May 22 2003",
				"A243055(n) = a(n) - A055396(n). - _Antti Karttunen_, Mar 07 2017",
				"a(n) = A000720(A006530(n)). - _Alois P. Heinz_, Mar 05 2020"
			],
			"example": [
				"a(20) = 3 since the largest prime factor of 20 is 5, which is the 3rd prime."
			],
			"maple": [
				"with(numtheory):",
				"a:= n-\u003e pi(max(1, factorset(n)[])):",
				"seq(a(n), n=1..100);  # _Alois P. Heinz_, Aug 03 2013"
			],
			"mathematica": [
				"Insert[Table[PrimePi[FactorInteger[n][[ -1]][[1]]], {n, 2, 120}], 0, 1] (* _Stefan Steinerberger_, Apr 11 2006 *)",
				"f[n_] := PrimePi[ FactorInteger@n][[ -1, 1]]; Array[f, 94] (* _Robert G. Wilson v_, Dec 30 2007 *)"
			],
			"program": [
				"(PARI) { for (n=1, 1000, if (n==1, a=0, f=factor(n)~; p=f[1, length(f)]; a=primepi(p)); write(\"b061395.txt\", n, \" \", a) ) } \\\\ _Harry J. Smith_, Jul 22 2009",
				"(Haskell)",
				"a061395 = a049084 . a006530  -- _Reinhard Zumkeller_, Jun 11 2013",
				"(Python)",
				"from sympy import primepi, primefactors",
				"def a(n): return 0 if n==1 else primepi(primefactors(n)[-1])",
				"print([a(n) for n in range(1, 101)]) # _Indranil Ghosh_, May 14 2017"
			],
			"xref": [
				"Cf. A000720, A006530, A055396, A061394, A133674, A243055."
			],
			"keyword": "easy,nice,nonn",
			"offset": "1,3",
			"author": "_Henry Bottomley_, Apr 30 2001",
			"ext": [
				"Definition reworded by _N. J. A. Sloane_, Jul 01 2008"
			],
			"references": 266,
			"revision": 60,
			"time": "2021-05-15T05:45:52-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}