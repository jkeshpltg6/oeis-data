{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A340263",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 340263,
			"data": "1,1,-1,1,1,-3,6,-3,1,1,-7,28,-49,70,-49,28,-7,1,1,-15,120,-525,1820,-4095,8008,-10725,12870,-10725,8008,-4095,1820,-525,120,-15,1",
			"name": "T(n, k) = [x^k] ((1-x)^(2^n) + 2^(-n)*((2^n-1)*(x-1)^(2^n) + (x+1)^(2^n)))/2. Irregular triangle read by rows, for n \u003e= 0 and 0 \u003c= k \u003c= 2^n.",
			"comment": [
				"Conjecture: for n \u003e= 1 the polynomials are irreducible."
			],
			"link": [
				"Peter Luschny, \u003ca href=\"/A340263/b340263.txt\"\u003eTable of n, a(n) for n = 0..1031\u003c/a\u003e"
			],
			"formula": [
				"Let p_n(x) = b(n) - (2^n-1)*a(n-1), b(n) = Sum_{k=0..2^n} binomial(2^n, 2*k)* x^(2*k), and a(n) = x*Product_{k=0..n} b(k). Then T(n, k) = [x^k] p_n(x)."
			],
			"example": [
				"Polynomials begin:",
				"[0] 1;",
				"[1] x^2 - x + 1;",
				"[2] x^4 - 3*x^3 + 6*x^2 - 3*x + 1;",
				"[3] x^8 - 7*x^7 + 28*x^6 - 49*x^5 + 70*x^4 - 49*x^3 + 28*x^2 - 7*x + 1;",
				"Triangle begins:",
				"[0] [1]",
				"[1] [1, -1, 1]",
				"[2] [1, -3, 6, -3, 1]",
				"[3] [1, -7, 28, -49, 70, -49, 28, -7, 1]",
				"[4] [1, -15, 120, -525, 1820, -4095, 8008, -10725, 12870, -10725, 8008, -4095, 1820, -525, 120, -15, 1]"
			],
			"maple": [
				"A340263_row := proc(n) local a, b;",
				"if n = 0 then return [1] fi;",
				"b := n -\u003e add(binomial(2^n, 2*k)*x^(2*k), k = 0..2^n);",
				"a := n -\u003e x*mul(b(k), k = 0..n);",
				"expand(b(n) - (2^n-1)*a(n-1));",
				"[seq(coeff(%, x, j), j = 0..2^n)] end:",
				"for n from 0 to 5 do A340263_row(n) od;",
				"# Alternatively:",
				"CoeffList := p -\u003e [op(PolynomialTools:-CoefficientList(p, x))]:",
				"Tpoly := n -\u003e ((1-x)^(2^n) + 2^(-n)*((2^n-1)*(x-1)^(2^n) + (x + 1)^(2^n)))/2:",
				"seq(print(CoeffList(Tpoly(n))), n=0..5); # _Peter Luschny_, Feb 03 2021"
			],
			"program": [
				"(SageMath)",
				"def A340263():",
				"    a, b, c = 1, 1, 1",
				"    yield [1]",
				"    while True:",
				"        c *= 2",
				"        a *= b",
				"        b = sum(binomial(c, 2 * k) * x ^ (2 * k) for k in range(c + 1))",
				"        yield ((b - (c - 1) * x * a)).list()",
				"A340263_row = A340263()",
				"for _ in range(6):",
				"    print(next(A340263_row))"
			],
			"xref": [
				"Row sums are 2^(2^n - n - 1) = A016031(n-1).",
				"Central terms of the rows are A037293(n) for n \u003e= 2.",
				"Cf. A340312."
			],
			"keyword": "sign,tabf,look",
			"offset": "0,6",
			"author": "_Peter Luschny_, Jan 06 2021",
			"ext": [
				"Shorter name by _Peter Luschny_, Feb 03 2021"
			],
			"references": 5,
			"revision": 30,
			"time": "2021-04-09T10:37:12-04:00",
			"created": "2021-01-06T20:49:28-05:00"
		}
	]
}