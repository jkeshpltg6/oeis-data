{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A073028",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 73028,
			"data": "1,1,1,2,3,4,6,10,15,21,35,56,84,126,210,330,495,792,1287,2002,3003,5005,8008,12376,19448,31824,50388,77520,125970,203490,319770,497420,817190,1307504,2042975,3268760,5311735,8436285,13123110,21474180,34597290",
			"name": "a(n) = max{ C(n,0), C(n-1,1), C(n-2,2), ..., C(n-n,n) }.",
			"comment": [
				"lim a(n)/a(n-1) = (1+sqrt(5))/2.",
				"a(n-1) is the max coefficient in n-th Fibonacci polynomial (the polynomial F_0(x) is constant zero, and is not included in this sequence). - _Vladimir Reshetnikov_, Oct 09 2016"
			],
			"reference": [
				"Peter Boros (borospet(AT)freemail.hu): Lectures on Fibonacci's World at the SOTERIA Foundation, 1999."
			],
			"link": [
				"Charles R Greathouse IV, \u003ca href=\"/A073028/b073028.txt\"\u003eTable of n, a(n) for n = 0..4793\u003c/a\u003e",
				"Benjamin Aram Berendsohn, László Kozma, Dániel Marx, \u003ca href=\"https://arxiv.org/abs/1908.04673\"\u003eFinding and counting permutations via CSPs\u003c/a\u003e, arXiv:1908.04673 [cs.DS], 2019.",
				"S. M. Tanny and M. Zuker, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(74)90073-9\"\u003eOn a unimodal sequence of binomial coefficients\u003c/a\u003e, Discrete Math. 9 (1974), 79-89.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/FibonacciPolynomial.html\"\u003eFibonacci Polynomial\u003c/a\u003e."
			],
			"formula": [
				"a(n) = binomial(n-A060065(n), A060065(n)). - _Vladeta Jovovic_, Jun 16 2004",
				"a(n) ~ 5^(1/4) * phi^(n+1) / sqrt(2*Pi*n), where phi = A001622 = (1+sqrt(5))/2 is the golden ratio. - _Vaclav Kotesovec_, Oct 09 2016"
			],
			"example": [
				"For n = 6, C(6,0) = 1, C(5,1) = 5, C(4,2) = 6, C(3,3) = 1. These binomial coefficients are the coefficients in the Fibonacci polynomial F_7(x) = x^6 + 5*x^4 + 6*x^2 + 1. The max coefficient is 6, so a(6) = 6."
			],
			"mathematica": [
				"Table[Max[CoefficientList[Fibonacci[n + 1, x], x]], {n, 1, 30}] (* _Vladimir Reshetnikov_, Oct 07 2016 *)"
			],
			"program": [
				"(PARI) a(n)=my(k=(5*n-sqrtint(5*n^2+10*n+9)+6)\\10); binomial(n-k,k) \\\\ _Charles R Greathouse IV_, Sep 22 2016"
			],
			"xref": [
				"Cf. A060065, A277282, A168561."
			],
			"keyword": "easy,nonn",
			"offset": "0,4",
			"author": "_Miklos Kristof_, Aug 22 2002",
			"ext": [
				"a(0) = 1 prepended by _Vladimir Reshetnikov_, Oct 09 2016"
			],
			"references": 4,
			"revision": 34,
			"time": "2019-11-04T20:13:00-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}