{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A343262",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 343262,
			"data": "3,4,5,3,6,7,4,3,5,6,6,7,3,4,4,6,6,4,3",
			"name": "a(n) is the number of edges of a regular polygon P with the property that packing n nonoverlapping equal circles inside P, arranged in a configuration with dihedral symmetry D_{2m} with m \u003e= 3, maximizes the packing density.",
			"comment": [
				"Numbers of dihedral symmetries D_{2m} (m \u003e= 3) that n nonoverlapping equal circles possess are given in A343005. The regular polygon is a circle for n=1 and a square for n=2. However, as the symmetry types, O(2) for one circle and D_{4} for two circles, are not D_{2m} with m \u003e= 3, the index of the sequence starts at n = 3.",
				"It can be shown that a(n) \u003c= n and a(n) = k*m/2, where m is the order of a dihedral symmetry of n-circle packing configurations and k is a positive integer."
			],
			"link": [
				"Erich Friedman, \u003ca href=\"https://erich-friedman.github.io/packing/\"\u003ePacking Equal Copies\u003c/a\u003e",
				"Ya-Ping Lu, \u003ca href=\"/A343262/a343262_1.pdf\"\u003eIllustration of packing configurations\u003c/a\u003e",
				"Eckard Specht, Packomania, \u003ca href=\"http://www.packomania.com\"\u003ePackings of equal and unequal circles in fixed-sized containers with maximum packing density\u003c/a\u003e"
			],
			"example": [
				"For n=3, 3-circle configurations possess one dihedral symmetry D_{6}, or m = 3. Since a(n) must be \u003c= 3 and also a multiple of m, a(n) = 3.",
				"For n = 16, 16-circle configurations have 6 D_{2m} symmetries with m \u003e= 3.",
				"Packing densities are for",
				"m = 16: Pi/(2+2*csc(Pi/8)) = 0.43474+,",
				"m = 15: (8*Pi/15)/(1+csc(2*Pi/15)) = 0.48445+,",
				"m =  8: 4*sqrt(2)*Pi/(1+sqrt(2)+sqrt(3)+sqrt(4-2*sqrt(2)))^2 = 0.65004+,",
				"m =  5: (16*Pi/5)*(7-3*sqrt(5))/sqrt(10+2*sqrt(5)) = 0.77110+,",
				"m =  4: Pi/4 = 0.78539+,",
				"m =  3: 8*Pi/(12+13*sqrt(3)) = 0.72813+.",
				"The highest packing density is achieved at m = 4, or a(16) = 4.",
				"Symmetry type (S) of n-circle configuration giving the highest packing density and the corresponding number of edges (N) of the regular polygon and packing density are given below. The packing configurations are illustrated in the Links.",
				"   n       S      N      Packing density",
				"------  --------  --  -------------------------------------------------------------",
				"   3      D_{6}   3   Pi/(2+4/sqrt(3))                                   = 0.72900+",
				"  4,9,16  D_{8}   4   Pi/4                                               = 0.78539+",
				"   5      D_{10}  5   Pi/(2+8/sqrt(10+2*sqrt(5)))                        = 0.76569+",
				"   6      D_{6}   3   6*Pi/(12+7*sqrt(3))                                = 0.78134+",
				"   7      D_{12}  6   7*Pi/(12+8*sqrt(3))                                = 0.85051+",
				"   8      D_{14}  7   4*Pi/(7+7/sin(2*Pi/7))                             = 0.78769+",
				"  10      D_{6}   3   5*Pi/(9+6*sqrt(3))                                 = 0.81001+",
				"  11      D_{10}  5   (22*Pi/25)/sqrt(10+2*sqrt(5))                      = 0.72671+",
				"  12      D_{6}   6   6*Pi/(12+7*sqrt(3))                                = 0.78134+",
				"  13      D_{12}  6   13*sqrt(3)*Pi/96                                   = 0.73685+",
				"  14      D_{14}  7   4*Pi/(sin(2*Pi/7)*(sqrt(3)+cot(Pi/7)+sec(Pi/7))^2) = 0.66440+",
				"  15      D_{6}   3   15*Pi/(24+19*sqrt(3))                              = 0.82805+",
				"  17      D_{8}   4   (17*Pi/4)/(7+3*sqrt(2)+3*sqrt(3)+sqrt(6))          = 0.70688+",
				"  18      D_{12}  6   9*Pi/(12+13*sqrt(3))                               = 0.81915+",
				"  19      D_{12}  6   19*Pi/(24+26*sqrt(3))                              = 0.86465+",
				"  20      D_{8}   4   20*Pi/(2+sqrt(2)+2*sqrt(3)+sqrt(6))^2              = 0.72213+",
				"  21      D_{6}   3   21*Pi/(30+28*sqrt(3))                              = 0.84045+"
			],
			"xref": [
				"Cf. A023393, A051657, A084616, A084617, A084618, A084644, A133587, A227405, A247397, A253570, A257594, A269110, A308578, A337019, A337020, A343005."
			],
			"keyword": "nonn,more",
			"offset": "3,1",
			"author": "_Ya-Ping Lu_, Apr 09 2021",
			"references": 2,
			"revision": 25,
			"time": "2021-05-20T12:48:40-04:00",
			"created": "2021-05-20T12:48:40-04:00"
		}
	]
}