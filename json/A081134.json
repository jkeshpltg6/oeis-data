{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A081134",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 81134,
			"data": "0,1,0,1,2,3,2,1,0,1,2,3,4,5,6,7,8,9,8,7,6,5,4,3,2,1,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,26,25,24,23,22,21,20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0,1,2,3,4,5,6,7",
			"name": "Distance to nearest power of 3.",
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A081134/b081134.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Klaus Brockhaus, \u003ca href=\"/A081134/a081134.gif\"\u003eIllustration for A081134, A081249, A081250 and A081251\u003c/a\u003e",
				"Hsien-Kuei Hwang, S. Janson, T.-H. Tsai, \u003ca href=\"http://140.109.74.92/hk/wp-content/files/2016/12/aat-hhrr-1.pdf\"\u003eExact and asymptotic solutions of the recurrence f(n) = f(floor(n/2)) + f(ceiling(n/2)) + g(n): theory and applications\u003c/a\u003e, Preprint 2016.",
				"Hsien-Kuei Hwang, S. Janson, T.-H. Tsai, \u003ca href=\"https://doi.org/10.1145/3127585\"\u003eExact and Asymptotic Solutions of a Divide-and-Conquer Recurrence Dividing at Half: Theory and Applications\u003c/a\u003e, ACM Transactions on Algorithms, 13:4 (2017), #47; DOI: 10.1145/3127585.",
				"\u003ca href=\"/index/Di#distance_to_the_nearest\"\u003eIndex entries for sequences related to distance to nearest element of some set\u003c/a\u003e"
			],
			"formula": [
				"a(n) = min(n-3^floor(log(n)/log(3)), 3*3^floor(log(n)/log(3))-n)."
			],
			"example": [
				"a(7) = 2 since 9 is closest power of 3 to 7 and 9 - 7 = 2."
			],
			"maple": [
				"a:= n-\u003e (h-\u003e min(n-h, 3*h-n))(3^ilog[3](n)):",
				"seq(a(n), n=1..100);  # _Alois P. Heinz_, Mar 28 2021"
			],
			"mathematica": [
				"Flatten[Table[Join[Range[0,3^n],Range[3^n-1,1,-1]],{n,0,4}]] (* _Harvey P. Dale_, Dec 31 2013 *)"
			],
			"program": [
				"(PARI) a(n) = my (p=#digits(n,3)); return (min(n-3^(p-1), 3^p-n)) \\\\ _Rémy Sigrist_, Mar 24 2018",
				"(Python)",
				"def A081134(n):",
				"    kmin, kmax = 0,1",
				"    while 3**kmax \u003c= n:",
				"        kmax *= 2",
				"    while True:",
				"        kmid = (kmax+kmin)//2",
				"        if 3**kmid \u003e n:",
				"            kmax = kmid",
				"        else:",
				"            kmin = kmid",
				"        if kmax-kmin \u003c= 1:",
				"            break",
				"    return min(n-3**kmin, 3*3**kmin-n) # _Chai Wah Wu_, Mar 31 2021"
			],
			"xref": [
				"Cf. A053646, A062153, A002452, A081249, A081250, A081251."
			],
			"keyword": "easy,nonn",
			"offset": "1,5",
			"author": "_Klaus Brockhaus_, Mar 08 2003",
			"references": 9,
			"revision": 32,
			"time": "2021-03-31T22:28:10-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}