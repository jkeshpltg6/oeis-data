{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A345401",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 345401,
			"data": "1,3,7,5,15,11,13,9,31,23,27,19,29,21,25,17,63,47,55,39,59,43,51,35,61,45,53,37,57,41,49,33,127,95,111,79,119,87,103,71,123,91,107,75,115,83,99,67,125,93,109,77,117,85,101,69,121,89,105,73,113,81,97,65,255,191",
			"name": "a(n) is the unique odd number h such that BCR(h*2^m-1) = 2n (except for BCR(0) = 1) where BCR is bit complement and reverse per A036044.",
			"comment": [
				"This sequence is a permutation of the odd numbers.",
				"We have BCR(a(n)*2^m-1) = 2n when n = 0 for m \u003e= 1, and BCR(a(n)*2^m-1) = 2n when n \u003e= 1 for m \u003e= 0.",
				"Why this exception when n = 0? As a(0) = 1, we have BCR(1*2^m-1) = 2*0 = 0 only for m \u003e= 1, because, for m = 0, we have BCR(1*2^0-1) = BCR(0) = 1 \u003c\u003e 2*0 = 0."
			],
			"formula": [
				"a(n) = BCR(2*n) + 1 for n \u003e= 1.",
				"a(n) = 2*A059894(n) + 1 for n \u003e= 1. - _Hugo Pfoertner_, Jun 18 2021"
			],
			"example": [
				"a(0) = 1 because BCR(1*2^m-1) = 2*0 = 0 for m \u003e= 1 (A000225).",
				"a(1) = 3 because BCR(3*2^m-1) = 2*1 = 2 for m \u003e= 0 (A153893).",
				"a(2) = 7 because BCR(7*2^m-1) = 2*2 = 4 for m \u003e= 0 (A086224).",
				"Indeed, a(1) = 3 because 3*2^m-1 = 1011..11_2 (i.e., 10 followed by m 1's), whose bit complement is 0100..00, which reverses to 10_2 = 2 = 2*1.",
				"Also, a(43) = 75 because 75*2^m-1 = 100101011..11_2 (i.e., 1001010 followed by m 1's), whose bit complement is 011010100..00, which reverses to 1010110_2 = 86 = 2*43."
			],
			"xref": [
				"Cf. A036044 (BCR), A059894.",
				"When BCR(n) = 0, 2, 4, 6, 8, 10, 12, then corresponding a(n) = h = 1, 3, 7, 5, 15, 11, 13 and numbers h*2^m-1 are respectively in A000225, A153893, A086224, A153894, A196305, A086225, A198274."
			],
			"keyword": "nonn,base,easy",
			"offset": "0,2",
			"author": "_Bernard Schott_, Jun 18 2021",
			"references": 1,
			"revision": 58,
			"time": "2021-07-20T03:59:42-04:00",
			"created": "2021-07-20T03:59:42-04:00"
		}
	]
}