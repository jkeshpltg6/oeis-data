{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A290694",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 290694,
			"data": "0,1,0,0,1,0,0,-1,2,0,0,1,-2,3,0,0,-1,14,-9,24,0,0,1,-10,75,-48,20,0,0,-1,62,-135,312,-300,720,0,0,1,-42,903,-1680,2800,-2160,630,0,0,-1,254,-1449,40824,-21000,27360,-17640,4480",
			"name": "Triangle read by rows, numerators of coefficients (in rising powers) of rational polynomials P(n, x) such that Integral_{x=0..1} P'(n, x) = Bernoulli(n, 1).",
			"comment": [
				"Consider a family of integrals I_m(n) = Integral_{x=0..1} P'(n, x)^m with P'(n,x) = Sum_{k=0..n}(-1)^(n-k)*Stirling2(n, k)*k!*x^k (see A278075 for the coefficients).",
				"I_1(n) are the Bernoulli numbers A164555/A027642, I_2(n) are the Bernoulli median numbers A212196/A181131, I_3(n) are the numbers A291449/A291450.",
				"The coefficients of the polynomials P_n(x)^m are for m = 1 A290694/A290695 and for m = 2 A291447/A291448.",
				"Only omega(Clausen(n)) = A001221(A160014(n,1)) = A067513(n) coefficients are rational numbers if n is even. For odd n \u003e 1 there are two rational coefficients.",
				"Let C_k(n) = [x^k] P_n(x), k \u003e 0 and n even. Conjecture: k is a prime factor of Clausen(n) \u003c=\u003e k = denominator(C_k(n)) \u003c=\u003e k does not divide Stirling2(n, k-1)*(k-1)!. (Note that by a comment in A019538 Stirling2(n, k-1)*(k-1)! is the number of chain topologies on an n-set having k open sets.)"
			],
			"link": [
				"Peter Luschny, \u003ca href=\"/A290694/a290694.jpg\"\u003eIllustrating A290694.\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = Numerator(Stirling2(n, k - 1)*(k - 1)!/k) if k \u003e 0 else 0; for n \u003e= 0 and 0 \u003c= k \u003c= n+1."
			],
			"example": [
				"Triangle starts:",
				"[0, 1]",
				"[0, 0,  1]",
				"[0, 0, -1,   2]",
				"[0, 0,  1,  -2,    3]",
				"[0, 0, -1,  14,   -9,  24]",
				"[0, 0,  1, -10,   75, -48,   20]",
				"[0, 0, -1,  62, -135, 312, -300, 720]",
				"The first few polynomials are:",
				"P_0(x) = x.",
				"P_1(x) =  (1/2)*x^2.",
				"P_2(x) = -(1/2)*x^2 + (2/3)*x^3.",
				"P_3(x) =  (1/2)*x^2 - 2*x^3 + (3/2)*x^4.",
				"P_4(x) = -(1/2)*x^2 + (14/3)*x^3 - 9*x^4 + (24/5)*x^5.",
				"P_5(x) =  (1/2)*x^2 - 10*x^3 + (75/2)*x^4 - 48*x^5 + 20*x^6.",
				"P_6(x) = -(1/2)*x^2 + (62/3)*x^3 - 135*x^4 + 312*x^5 - 300*x^6 + (720/7)*x^7.",
				"Evaluated at x = 1 this gives an additive decomposition of the Bernoulli numbers:",
				"B(0) =     1 =    1.",
				"B(1) =   1/2 =  1/2.",
				"B(2) =   1/6 = -1/2 +  2/3.",
				"B(3) =     0 =  1/2 -    2 + 3/2.",
				"B(4) = -1/30 = -1/2 + 14/3 -    9 + 24/5.",
				"B(5) =     0 =  1/2 -   10 + 75/2 -   48 +  20.",
				"B(6) =  1/42 = -1/2 + 62/3 -  135 +  312 - 300 + 720/7."
			],
			"maple": [
				"BG_row := proc(m, n, frac, val) local F, g, v;",
				"F := (n, x) -\u003e add((-1)^(n-k)*Stirling2(n,k)*k!*x^k, k=0..n):",
				"g := x -\u003e int(F(n,x)^m, x):",
				"`if`(val = \"val\", subs(x=1, g(x)), [seq(coeff(g(x),x,j), j=0..m*n+1)]):",
				"`if`(frac = \"num\", numer(%), denom(%)) end:",
				"seq(BG_row(1, n, \"num\", \"val\"), n=0..16);         # A164555",
				"seq(BG_row(1, n, \"den\", \"val\"), n=0..16);         # A027642",
				"seq(print(BG_row(1, n, \"num\", \"poly\")), n=0..12); # A290694 (this seq.)",
				"seq(print(BG_row(1, n, \"den\", \"poly\")), n=0..12); # A290695",
				"# Alternatively:",
				"T_row := n -\u003e numer(PolynomialTools:-CoefficientList(add((-1)^(n-j+1)*Stirling2(n, j-1)*(j-1)!*x^j/j, j=1..n+1), x)): for n from 0 to 6 do T_row(n) od;"
			],
			"mathematica": [
				"T[n_, k_] := If[k \u003e 0, Numerator[StirlingS2[n, k - 1]*(k - 1)! / k], 0]; Table[T[n, k], {n, 0, 8}, {k, 0, n+1}] // Flatten"
			],
			"xref": [
				"Cf. A164555/A027642, A212196/A181131, A291449/A291450, A290694/A290695, A291447/A291448.",
				"Cf. A160014, A278075."
			],
			"keyword": "sign,tabf,frac",
			"offset": "0,9",
			"author": "_Peter Luschny_, Aug 24 2017",
			"references": 6,
			"revision": 28,
			"time": "2017-08-26T10:29:19-04:00",
			"created": "2017-08-26T08:21:06-04:00"
		}
	]
}