{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097676",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97676,
			"data": "6,3,7,6,6,3,2,4,8,9,4,1,6,6,7,7,8,5,5,0,0,1,7,6,2,5,9,3,8,2,5,1,0,7,9,0,6,2,6,7,4,3,5,3,2,6,7,8,6,4,6,2,1,6,7,6,7,3,0,6,4,1,0,7,4,3,4,2,6,4,5,4,9,1,5,2,5,9,9,9,3,9,0,8,8,3,3,7,3,3,1,6,4,3,8,3,2,7,6,5,5,5,3,4,9",
			"name": "Decimal expansion of the constant 8*exp(psi(7/8) + EulerGamma), where EulerGamma is the Euler-Mascheroni constant (A001620) and psi(x) is the digamma function.",
			"comment": [
				"This constant appears in _Benoit Cloitre_'s generalized Euler-Gauss formula for the Gamma function (see Cloitre link) and is involved in the exact determination of asymptotic limits of certain order-8 linear recursions with varying coefficients (see A097682 for example)."
			],
			"reference": [
				"A. M. Odlyzko, Linear recurrences with varying coefficients, in Handbook of Combinatorics, Vol. 2, R. L. Graham, M. Grotschel and L. Lovasz, eds., Elsevier, Amsterdam, 1995, pp. 1135-1138."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A097676/b097676.txt\"\u003eTable of n, a(n) for n = 1..5000\u003c/a\u003e",
				"Benoit Cloitre, \u003ca href=\"/A097679/a097679.pdf\"\u003eOn a generalization of Euler-Gauss formula for the Gamma function\u003c/a\u003e, preprint 2004.",
				"Xavier Gourdon and Pascal Sebah, \u003ca href=\"http://numbers.computation.free.fr/Constants/Miscellaneous/gammaFunction.html\"\u003eIntroduction to the Gamma Function\u003c/a\u003e.",
				"Andrew Odlyzko, \u003ca href=\"http://www.dtc.umn.edu/~odlyzko/doc/asymptotic.enum.pdf\"\u003eAsymptotic enumeration methods\u003c/a\u003e, in Handbook of Combinatorics, vol. 2, 1995, pp. 1063-1229."
			],
			"formula": [
				"c = (1+sqrt(2))^(-sqrt(2))/2*exp(Pi/2*(1+sqrt(2)))."
			],
			"example": [
				"c = 6.37663248941667785500176259382510790626743532678646216767306..."
			],
			"mathematica": [
				"RealDigits[(1 + Sqrt[2])^(-Sqrt[2])/2E^(Pi/2*(1 + Sqrt[2])), 10, 105][[1]] (* _Robert G. Wilson v_, Aug 27 2004 *)"
			],
			"program": [
				"(PARI) 8*exp(psi(7/8)+Euler)",
				"(MAGMA) SetDefaultRealField(RealField(100)); R:= RealField();",
				"(1+Sqrt(2))^(-Sqrt(2))/2*Exp(Pi(R)/2*(1+Sqrt(2))); // _G. C. Greubel_, Sep 07 2018"
			],
			"xref": [
				"Cf. A097663-A097675."
			],
			"keyword": "cons,nonn",
			"offset": "1,1",
			"author": "_Paul D. Hanna_, Aug 25 2004",
			"ext": [
				"More terms from _Robert G. Wilson v_, Aug 27 2004"
			],
			"references": 15,
			"revision": 22,
			"time": "2021-02-27T13:20:38-05:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}