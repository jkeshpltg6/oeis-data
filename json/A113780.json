{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A113780",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 113780,
			"data": "1,3,3,2,2,3,4,1,2,4,2,4,1,2,2,1,8,2,2,2,0,4,1,4,2,2,5,4,2,0,4,4,2,0,0,3,4,4,4,2,3,4,2,2,4,0,0,2,2,4,2,9,2,0,2,2,4,1,4,0,4,4,2,0,4,4,4,2,0,2,1,8,0,2,2,2,6,1,2,4,0,4,4,2,2,0,8,2,2,2,2,0,1,8,0,2,4,0,0,2,5,6,4,2,4",
			"name": "Number of solutions to 24*n+1 = x^2+24*y^2, x a positive integer, y an integer.",
			"comment": [
				"If 24*n+1 is not a square or if sqrt(24*n+1) == 1 or 11 (mod 12), then A000009(n) == a(n) (mod 4), otherwise A000009(n) == a(n) + 2 (mod 4).",
				"Implied by the arithmetic of Q[sqrt(-6)]: Let 24*n+1 = p_1^e_1 * ... * p_r^e_r * q_1^f_1 * ... * q_s^f_s, where the p_i's are distinct primes == 1, 5, 7, or 11 (mod 24) and the q_i's are distinct primes == 13, 17, 19, or 23 (mod 24). If some f_i is odd, then a(n) = 0. Otherwise, a(n) = (e_1 + 1) * ... * (e_r + 1). a(n) == 2 (mod 4) iff all of the f_i's are even and all but one of the e_i's are even and the one e_i which is odd is == 1 (mod 4). Since A000009(n) and a(n) are both odd if 24*n+1 is a square, we can replace a by A000009 in this.",
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"reference": [
				"Mohammad K. Azarian, Diophantine Pair, Problem B-881, Fibonacci Quarterly, Vol. 37, No. 3, August 1999, pp. 277-278.  Solution published in Vol. 38, No. 2, May 2000, pp. 183-184."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A113780/b113780.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of phi(x) * phi(-x^3) / chi(-x) in powers of x where phi(), chi() are Ramanujan theta functions. - _Michael Somos_, Jun 08 2012",
				"Expansion of f(x, x) * f(x, x^2) in powers of x where f(, ) is Ramanujan's general theta function. - _Michael Somos_, Jun 08 2013",
				"Expansion of eta(q^2)^6 * eta(q^3)^2 / (eta(q)^3 * eta(q^4)^2 * eta(q^6)) in powers of q. - _Michael Somos_, Jun 08 2012",
				"Euler transform of period 12 sequence [ 3, -3, 1, -1, 3, -4, 3, -1, 1, -3, 3, -2, ...]. - _Michael Somos_, Jun 08 2012",
				"a(n) = A128580(12*n) = A129402(12*n) = A134177(12*n) = A190615(12*n). - _Michael Somos_, Jun 08 2012"
			],
			"example": [
				"If n=51, the solutions (x,y) are: (7,+-7), (19,+-6), (25,+-5), (29,+-4), (35,0) so a(51)=9.",
				"G.f. = 1 + 3*x + 3*x^2 + 2*x^3 + 2*x^4 + 3*x^5 + 4*x^6 + x^7 + 2*x^8 + 4*x^9 + ...",
				"G.f. = q + 3*q^25 + 3*q^49 + 2*q^73 + 2*q^97 + 3*q^121 + 4*q^145 + q^169 + 2*q^193 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 0, 0, With[{m = 24 n + 1}, Sum[ KroneckerSymbol[ -12, d] KroneckerSymbol[ 2, m/d], {d, Divisors @ m}]]]; (* _Michael Somos_, Jun 08 2013 *)",
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 4, 0, x^3] EllipticTheta[ 3, 0, x] / QPochhammer[ x, x^2], {x, 0, n}]; (* _Michael Somos_, Jun 08 2013 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, n = 24*n + 1; sumdiv( n, d, kronecker( -12, d) * kronecker( 2, n/d)))}; /* _Michael Somos_, Mar 11 2007 */",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^6 * eta(x^3 + A)^2 / (eta(x + A)^3 * eta(x^4 + A)^2 * eta(x^6 + A)), n))}; /* _Michael Somos_, Jun 08 2012 */"
			],
			"xref": [
				"Cf. A001318 generalized pentagonal numbers, indices of odd values of a(n) and A000009.",
				"Cf. A114913 = values k such that A000009(k) == 2 (mod 4) and such that a(k) == 2 (mod 4).",
				"Cf. A128580, A129402, A134177, A190615."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Christian G. Bower_, Jan 20 2006, based on a message from _Dean Hickerson_.",
			"references": 10,
			"revision": 30,
			"time": "2021-03-12T22:24:43-05:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}