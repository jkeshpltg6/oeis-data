{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A239053",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 239053,
			"data": "4,8,12,24,20,24,40,32,48,56,44,48,72,72,60,104,68,72,124,80,84,120,112,120,156,104,108,152,144,144,168,128,132,240,140,168,228,152,192,216,164,168,260,248,180,248,216,192,336,200,240,312,212,264,296",
			"name": "Sum of divisors of 4*n-1.",
			"comment": [
				"Bisection of A008438.",
				"a(n) is also the total number of cells in the n-th branch of the third quadrant of the spiral formed by the parts of the symmetric representation of sigma(4n-1), see example. For the quadrants 1, 2, 4 see A112610, A239052, A193553. The spiral has been obtained according to the following way: A196020 --\u003e A236104 --\u003e A235791 --\u003e A237591 --\u003e A237593 --\u003e A237270.",
				"We can find the spiral (mentioned above) on the terraces of the pyramid described in A244050. - _Omar E. Pol_, Dec 06 2016"
			],
			"formula": [
				"a(n) = A000203(4n-1) = A000203(A004767(n-1)).",
				"a(n) = 4*A097723(n-1). - _Joerg Arndt_, Mar 09 2014"
			],
			"example": [
				"Illustration of initial terms:",
				"-----------------------------------------------------",
				".        Branches of the spiral",
				".        in the third quadrant             n    a(n)",
				"-----------------------------------------------------",
				".     _       _       _       _",
				".    | |     | |     | |     | |",
				".    | |     | |     | |     |_|_ _",
				".    | |     | |     | |    2  |_ _|       1      4",
				".    | |     | |     |_|_     2",
				".    | |     | |    4    |_",
				".    | |     |_|_ _        |_ _ _ _",
				".    | |    6      |_      |_ _ _ _|       2      8",
				".    |_|_ _ _        |_   4",
				".   8      | |_ _      |",
				".          |_    |     |_ _ _ _ _ _",
				".            |_  |_    |_ _ _ _ _ _|       3     12",
				".           8  |_ _|  6",
				".                  |",
				".                  |_ _ _ _ _ _ _ _",
				".                  |_ _ _ _ _ _ _ _|       4     24",
				".                 8",
				".",
				"For n = 4 the sum of divisors of 4*n-1 is 1 + 3 + 5 + 15 = A000203(15) = 24. On the other hand the parts of the symmetric representation of sigma(15) are [8, 8, 8] and the sum of them is 8 + 8 + 8 = 24, equaling the sum of divisors of 15, so a(4) = 24."
			],
			"maple": [
				"A239053:=n-\u003enumtheory[sigma](4*n-1): seq(A239053(n), n=1..80); # _Wesley Ivan Hurt_, Dec 06 2016"
			],
			"mathematica": [
				"DivisorSigma[1,4*Range[60]-1] (* _Harvey P. Dale_, Dec 06 2016 *)",
				"Table[DivisorSigma[1, 4 n - 1], {n, 100}] (* _Vincenzo Librandi_, Dec 07 2016 *)"
			],
			"program": [
				"(MAGMA) [SumOfDivisors(4*n-1): n in [1..60]]; // _Vincenzo Librandi_, Dec 07 2016",
				"(PARI) a(n) = sigma(4*n-1); \\\\ _Michel Marcus_, Dec 07 2016"
			],
			"xref": [
				"Cf. A000203, A004767, A008438, A062731, A074400, A112610, A193553, A196020, A235791, A236104, A237270, A237591, A237593, A239050, A239052, A244050, A245092, A262626."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_Omar E. Pol_, Mar 09 2014",
			"references": 8,
			"revision": 31,
			"time": "2016-12-07T04:23:09-05:00",
			"created": "2014-03-09T22:39:52-04:00"
		}
	]
}