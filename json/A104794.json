{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A104794",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 104794,
			"data": "1,-4,4,0,4,-8,0,0,4,-4,8,0,0,-8,0,0,4,-8,4,0,8,0,0,0,0,-12,8,0,0,-8,0,0,4,0,8,0,4,-8,0,0,8,-8,0,0,0,-8,0,0,0,-4,12,0,8,-8,0,0,0,0,8,0,0,-8,0,0,4,-16,0,0,8,0,0,0,4,-8,8,0,0,0,0,0,8",
			"name": "Expansion of theta_4(q)^2 in powers of q.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Quadratic AGM theta functions: a(q) (see A004018), b(q) (A104794), c(q) (A005883).",
				"In the Arithmetic-Geometric Mean, if a = theta_3(q)^2, b = theta_4(q)^2 then a' := (a+b)/2 = theta_3(q^2)^2, b' := sqrt(a*b) = theta_4(q^2)^2."
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), p. 576.",
				"J. M. Borwein and P. B. Borwein, Pi and the AGM, Wiley, 1987."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A104794/b104794.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of phi(-q)^2 = 2 * phi(q^2)^2 - phi(q)^2 = (phi(q) - 2*phi(q^4))^2 = f(-q)^3 / psi(q) = phi(-q^2)^4 / phi(q)^2 = psi(-q)^4 / psi(q^2)^2 = psi(q)^2 * chi(-q)^6 in powers of q where phi(), psi(), chi(), f() are Ramanujan theta functions.",
				"Expansion of (1-k^2)^(1/2) K(k^2) / (Pi/2) in powers of q where q is Jacobi's nome, k is the elliptic modulus and K() is the complete elliptic integral of the first kind.",
				"Expansion of  K(k^2) / (Pi/2) in powers of -q where q is Jacobi's nome, k is the elliptic modulus and K() is the complete elliptic integral of the first kind. - _Michael Somos_, Jun 08 2015",
				"Expansion of eta(q)^4 / eta(q^2)^2 in powers of q.",
				"Euler transform of period 2 sequence [ -4, -2, ...].",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2), A(x^4)) where f(u, v, w) = v * (u^2 + v^2) - 2*u*w^2.",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2), A(x^3), A(x^6)) where f(u1, u2, u3, u6) = u1^2 - 2*u1*u3 + 4*u2*u6 - 3*u3^2.",
				"Moebius transform is period 8 sequence [ -4, 8, 4, 0, -4, -8, 4, 0, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (8 t)) = 16 (t/i) g(t) where q = exp(2 Pi i t) and g() is the g.f. for A008441.",
				"G.f.: theta_4(q)^2 = (Sum_{k in Z} (-q)^(k^2))^2 = (Product_{k\u003e0} (1 - q^(2*k)) * (1 - q^(2*k - 1))^2)^2.",
				"G.f.: 1 + 4 * Sum_{k\u003e0} (-x)^k / (1 + x^(2*k)). - _Michael Somos_, Jun 08 2015",
				"a(4*n + 3) = 0. a(n) = (-1)^n * A004018(n) = a(2*n). a(4*n + 1) = -4 * A008441(n). a(n) = -4 * A113652(n) unless n=0. a(6*n + 2) = 4 * A122865(n)). a(6*n + 4) = 4 * A122856(n). a(8*n + 1) = -4 * A113407(n). a(8*n + 5) = -8 * A053692(n).",
				"a(n) = a(9*n) = A204531(8*n) = A246950(8*n) = A256014(9*n) = A258210(n). - _Michael Somos_, Jun 08 2015",
				"Convolution inverse of A001934. Convolution with A000729 is A227695. - _Michael Somos_, Jun 08 2015",
				"G.f.: 2 * Sum_{k in Z} (-1)^k * x^(k*(k + 1)/2) / (1 + x^k). - _Michael Somos_, Nov 05 2015",
				"a(0) = 1, a(n) = -(4/n)*Sum_{k=1..n} A002131(k)*a(n-k) for n \u003e 0. - _Seiichi Manyama_, May 02 2017",
				"G.f.: exp(2*Sum_{k\u003e=1} (sigma(k) - sigma(2*k))*x^k/k). - _Ilya Gutkovskiy_, Sep 19 2018"
			],
			"example": [
				"G.f. = 1 - 4*q + 4*q^2 + 4*q^4 - 8*q^5 + 4*q^8 - 4*q^9 + 8*q^10 - 8*q^13 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 4, 0, q]^2, {q, 0, n}];",
				"a[ n_] := With[ {m = InverseEllipticNomeQ @ q}, SeriesCoefficient[ Sqrt[1 - m] EllipticK[m] / (Pi/2), {q, 0, n}]];",
				"a[ n_] := With[ {m = InverseEllipticNomeQ @ q}, SeriesCoefficient[ (1 - m)^(1/4) EllipticK[m] / (Pi/2), {q, 0, 2 n}]];",
				"a[ n_] := With[ {m = InverseEllipticNomeQ @ -q}, SeriesCoefficient[ EllipticK[ m] / (Pi/2), {q, 0, n}]]; (* _Michael Somos_, Jun 06 2015 *)",
				"a[ n_] := If[ n \u003c 1, Boole[n == 0], (-1)^n 4 DivisorSum[ n, KroneckerSymbol[ -4, #] \u0026]]; (* _Michael Somos_, Jun 06 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n==0, (-1)^n * 4 * sumdiv(n, d, (d%4==1) - (d%4==3)))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A)^4 / eta(x^2 + A)^2, n ))};",
				"(PARI) {a(n) = if( n\u003c0, 0, polcoeff( 1 + 4 * sum( k=1, n, (-x)^k / (1 + x^(2*k)), x * O(x^n)), n))};",
				"(MAGMA) A := Basis( ModularForms( Gamma1(8), 1), 100); A[1] - 4*A[2] + 4*A[3]; /* _Michael Somos_, Jan 31 2015 */",
				"(Julia) # JacobiTheta4 is defined in A002448.",
				"A104794List(len) = JacobiTheta4(len, 2)",
				"A104794List(102) |\u003e println # _Peter Luschny_, Mar 12 2018"
			],
			"xref": [
				"Cf. A000203, A000729, A001934, A002131, A004018, A008441, A053692, A113407, A113652, A122856, A122865, A204531, A227695, A246950, A256014, A258210."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Mar 26 2005",
			"references": 14,
			"revision": 45,
			"time": "2021-03-12T22:24:43-05:00",
			"created": "2005-04-09T03:00:00-04:00"
		}
	]
}