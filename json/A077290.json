{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A077290",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 77290,
			"data": "0,6,36,630,3570,61776,349866,6053460,34283340,593177346,3359417496,58125326490,329188631310,5695688818716,32257126450926,558119378907720,3160869203559480,54690003444137886,309732924822378156,5359062218146605150,30350665763389499850",
			"name": "Triangular numbers that are 6 times other triangular numbers.",
			"link": [
				"Colin Barker, \u003ca href=\"/A077290/b077290.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Vladimir Pletser, \u003ca href=\"https://arxiv.org/abs/2101.00998\"\u003eRecurrent Relations for Multiple of Triangular Numbers being Triangular Numbers\u003c/a\u003e, arXiv:2101.00998 [math.NT], 2021.",
				"Vladimir Pletser, \u003ca href=\"https://arxiv.org/abs/2102.13494\"\u003eTriangular Numbers Multiple of Triangular Numbers and Solutions of Pell Equations\u003c/a\u003e, arXiv:2102.13494 [math.NT], 2021.",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,98,-98,-1,1)."
			],
			"formula": [
				"a(n) = 6*A077289(n).",
				"G.f.: -6*x*(x^2+5*x+1) / ((x-1)*(x^2-10*x+1)*(x^2+10*x+1)). - _Colin Barker_, Jul 02 2013",
				"a(n) = 98*a(n-2) - a(n-1) + 42. - _Vladimir Pletser_, Feb 20 2021"
			],
			"example": [
				"The k-th triangular number is T(k) = k*(k+1)/2, so T(35)/T(14) = (35*36/2)/(14*15/2) = 630/105 = 6, so T(35)=630 is a term. - _Jon E. Schoenfield_, Feb 20 2021"
			],
			"maple": [
				"f := gfun:-rectoproc({a(-2) = 6, a(-1) = 0, a(0) = 0, a(1) = 6, a(n) = 98*a(n-2)-a(n-4)+42}, a(n), remember); map(f, [`$`(0 .. 1000)])[]; # _Vladimir Pletser_, Feb 20 2021"
			],
			"mathematica": [
				"CoefficientList[Series[-6 x (x^2 + 5 x + 1)/((x - 1) (x^2 - 10 x + 1) (x^2 + 10 x + 1)), {x, 0, 20}], x] (* _Michael De Vlieger_, Apr 21 2021 *)"
			],
			"program": [
				"(PARI)",
				"T(n)=n*(n+1)\\2;",
				"istriang(n)=issquare(8*n+1);",
				"for(n=0,10^10, t=T(n); if ( t%6==0 \u0026\u0026 istriang(t\\6), print1(t,\", \") ) );",
				"\\\\ _Joerg Arndt_, Jul 03 2013",
				"(PARI) concat(0, Vec(-6*x*(x^2+5*x+1) / ((x-1)*(x^2-10*x+1)*(x^2+10*x+1)) + O(x^100))) \\\\ _Colin Barker_, May 15 2015"
			],
			"xref": [
				"Cf. A077288, A077289, A077291.",
				"Subsequence of A000217."
			],
			"keyword": "easy,nonn",
			"offset": "0,2",
			"author": "Bruce Corrigan (scentman(AT)myfamily.com), Nov 03 2002",
			"ext": [
				"More terms from _Joerg Arndt_, Jul 03 2013"
			],
			"references": 11,
			"revision": 32,
			"time": "2021-05-15T01:23:48-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}