{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A228039",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 228039,
			"data": "0,1,1,0,1,1,0,1,1,1,1,1,0,0,1,0,1,1,1,1,1,0,1,1,0,1,0,0,1,1,0,1,1,1,1,1,1,0,1,1,1,1,0,1,1,0,1,0,0,1,1,1,0,0,0,1,1,0,1,1,0,0,1,0,1,1,1,1,1,0,1,1,1,0,0,1,1,1,1,1,1,0,1,0,0,1,1,1,1,1,0,1,1,0,0,1,0,1,1,0,1,1,1,1,0",
			"name": "Thue-Morse sequence along the squares: A010060(n^2).",
			"comment": [
				"(Adapted from Drmota, Mauduit, and Rivat) The Thue-Morse sequence T(n) is a 0-1-sequence that can be defined by T(n) = s2(n) mod 2, where s2(n) denotes the binary sum-of-digits function of n (that is, the number of powers of 2). By definition it is clear that 0 and 1 appear with the same asymptotic frequency 1/2. However, there is no consecutive block of the form 000 or 111, so that the Thue-Morse sequence is not normal. (A 0-1-sequence is normal if every finite 0-1-block appears with the asymptotic frequency 1/2^k, where k denotes the length of the block.) Mauduit and Rivat (2009) showed that the subsequence T(n^2) also has the property that both 0 and 1 appear with the same asymptotic frequency 1/2. This solved a long-standing conjecture by Gelfond (1967/1968). Drmota, Mauduit, and Rivat (2013) proved that the subsequence T(n^2) is actually normal."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A228039/b228039.txt\"\u003eTable of n, a(n) for n = 0..65537\u003c/a\u003e",
				"M. Drmota, C. Mauduit, J. Rivat, \u003ca href=\"https://math-oemg-dmv-2013.uibk.ac.at/cms/images/Public_Relations/oemg_dmv_2013.pdf\"\u003eThe Thue-Morse Sequence Along The Squares is Normal\u003c/a\u003e, Abstract, ÖMG-DMV Congress, 2013.",
				"A. O. Gelfond, \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/aa/aa13/aa13115.pdf\"\u003eSur les nombres qui ont des propriétés additives et multiplicatives données\u003c/a\u003e, Acta Arith. 13 (1967/1968) 259-265.",
				"C. Mauduit, J. Rivat, \u003ca href=\"http://dx.doi.org/10.1007/s11511-009-0040-0\"\u003eLa somme des chiffres des carres\u003c/a\u003e, Acta Mathem. 203 (1) (2009) 107-148.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Normal_number\"\u003eNormal number\u003c/a\u003e",
				"\u003ca href=\"/index/Ch#char_fns\"\u003eIndex entries for characteristic functions\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A010060(n^2) = A010060(A000290(n))."
			],
			"mathematica": [
				"a[n_] := If[ n == 0, 0, If[ Mod[n, 2] == 0, a[n/2], 1 - a[(n - 1)/2]]]; Table[ a[n^2], {n, 0, 104}]",
				"(* Second program: *)",
				"ThueMorse[Range[0, 104]^2] (* _Michael De Vlieger_, Dec 22 2017 *)"
			],
			"program": [
				"(PARI) a(n)=hammingweight(n^2)%2 \\\\ _Charles R Greathouse IV_, May 08 2016"
			],
			"xref": [
				"Cf. A000290, A010060, A293162."
			],
			"keyword": "nonn,easy",
			"offset": "0",
			"author": "_Jonathan Sondow_, Sep 02 2013",
			"references": 3,
			"revision": 20,
			"time": "2017-12-26T13:43:51-05:00",
			"created": "2013-09-03T03:34:27-04:00"
		}
	]
}