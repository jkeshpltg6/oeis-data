{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A304030",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 304030,
			"data": "0,0,1,0,1,4,3,0,5,2,3,2,3,8,3,0,3,10,7,0,1,6,1,0,9,2,3,6,7,4,3,0,13,4,1,4,5,8,9,0,7,2,5,2,3,4,5,0,5,8,5,0,1,14,9,0,7,2,3,6,7,12,9,0,5,6,3,0,1,4,7,0,13,2,1,2,3,8,3,0,7,14,11,0,1,8,3,0,3,2,7,4,5,12,9,0,19,4,1,0",
			"name": "a(n) is the number of steps at which the Collatz ('3x+1') trajectory of n crosses its initial value, or -1 if the number of crossings is infinite.",
			"comment": [
				"Some treatments of the Collatz conjecture view trajectories as starting to cycle when they reach 1, continuing with 4, 2, 1, 4, 2, 1, ..., while others view trajectories as terminating as soon as 1 is reached; this sequence treats trajectories as terminating at 1.",
				"If the Collatz conjecture is true, then for n \u003e 1, a(n) == n (mod 2).",
				"If there exists any number n whose Collatz trajectory enters a cycle that includes values above and below n, then the number of crossings would be infinite. If the Collatz conjecture is true, then there exists no such value of n.",
				"If a(k) = 0, then a(2^j * k) = 0, for j\u003e0. Therefore the primitives are 1, 20, 24, 52, 56, 68, 72, 84, 88, 100, 116, ..., . - _Robert G. Wilson v_, May 19 2018"
			],
			"link": [
				"\u003ca href=\"/index/3#3x1\"\u003eIndex entries for sequences related to 3x+1 (or Collatz) problem\u003c/a\u003e"
			],
			"example": [
				"The Collatz trajectory of 6 crosses its initial value (6) a total of 4 times, so a(6) = 4:",
				".",
				"                           16",
				"                           / \\",
				"                          /   \\",
				"              10         /     \\",
				"              / \\       /       \\",
				"             /   \\     /         8",
				"  6---------*-----*---*-----------*--------",
				"   \\       /       \\ /             \\",
				"    \\     /         5               \\",
				"     \\   /                           \\",
				"      \\ /                             4",
				"       3                               \\",
				"                                        ...",
				"(Each \"*\" represents a crossing.)"
			],
			"mathematica": [
				"Collatz[n_] := NestWhileList[ If[ OddQ@#, 3# +1, #/2] \u0026, n, # \u003e 1 \u0026]; f[n_] := Block[{x = Length[ SplitBy[ Collatz@ n, # \u003c n +1 \u0026]] - 1}, If[ OddQ@ n \u0026\u0026 n \u003e 1, x - 1, x]]; Array[f, 100] (* _Robert G. Wilson v_, May 05 2018 *)"
			],
			"xref": [
				"Cf. A006370, A070165."
			],
			"keyword": "nonn",
			"offset": "1,6",
			"author": "_Jon E. Schoenfield_, May 04 2018",
			"references": 3,
			"revision": 18,
			"time": "2018-09-28T23:46:33-04:00",
			"created": "2018-05-26T12:51:45-04:00"
		}
	]
}