{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A094040",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 94040,
			"data": "1,1,1,1,3,3,1,6,14,12,1,10,40,75,55,1,15,90,275,429,273,1,21,175,770,1911,2548,1428,1,28,308,1820,6370,13328,15504,7752,1,36,504,3822,17640,51408,93024,95931,43263,1,45,780,7350,42840,162792,406980,648945,600875,246675",
			"name": "Triangle read by rows: T(n,k) is the number of noncrossing forests with n vertices and k edges.",
			"comment": [
				"T(n,n-1) yields A001764; T(n,n-2) yields A026004."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A094040/b094040.txt\"\u003eTable of n, a(n) for n = 1..1275\u003c/a\u003e",
				"P. Flajolet and M. Noy, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(98)00372-0\"\u003eAnalytic combinatorics of noncrossing configurations\u003c/a\u003e, Discrete Math. 204 (1999), 203-229."
			],
			"formula": [
				"T(n, k)=binomial(n, k+1)*binomial(n+2k-1, k)/(n+k) (0\u003c=k\u003c=n-1)."
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  1,  1;",
				"  1,  3,   3;",
				"  1,  6,  14,  12;",
				"  1, 10,  40,  75,   55;",
				"  1, 15,  90, 275,  429,  273;",
				"  1, 21, 175, 770, 1911, 2548, 1428;",
				"  ...",
				"T(3,1)=3 because the noncrossing forests on 3 vertices A,B,C and having one edge are (A, BC), (B, CA) and (C, AB)."
			],
			"maple": [
				"T:=proc(n,k) if k\u003c=n-1 then binomial(n,k+1)*binomial(n+2*k-1,k)/(n+k) else 0 fi end: seq(seq(T(n,k),k=0..n-1),n=1..11);"
			],
			"mathematica": [
				"T[n_, k_] := Binomial[n, k+1] Binomial[n+2k-1, k]/(n+k);",
				"Table[T[n, k], {n, 1, 11}, {k, 0, n-1}] // Flatten (* _Jean-François Alcover_, Jul 29 2018 *)"
			],
			"program": [
				"(PARI)",
				"T(n,k)=binomial(n, k+1)*binomial(n+2*k-1, k)/(n+k);",
				"for(n=1, 10, for(k=0, n-1, print1(T(n, k), \", \")); print); \\\\ _Andrew Howroyd_, Nov 17 2017"
			],
			"xref": [
				"Cf. A001764, A026004, A045739, A094021."
			],
			"keyword": "nonn,tabl",
			"offset": "1,5",
			"author": "_Emeric Deutsch_, May 31 2004",
			"references": 2,
			"revision": 15,
			"time": "2018-07-29T10:32:27-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}