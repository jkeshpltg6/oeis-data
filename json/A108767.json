{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A108767",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 108767,
			"data": "1,1,2,1,6,5,1,12,28,14,1,20,90,120,42,1,30,220,550,495,132,1,42,455,1820,3003,2002,429,1,56,840,4900,12740,15288,8008,1430,1,72,1428,11424,42840,79968,74256,31824,4862,1,90,2280,23940,122094,325584,465120",
			"name": "Triangle read by rows: T(n,k) is number of paths from (0,0) to (3n,0) that stay in the first quadrant (but may touch the horizontal axis), consisting of steps u=(1,1), d=(1,-2) and have k peaks (i.e., ud's).",
			"comment": [
				"Row sums yield A001764.",
				"From _Peter Bala_, Sep 16 2012: (Start)",
				"The number of 2-Dyck paths of order n with k peaks (Cigler). A 2-Dyck path of order n is a lattice path from (0,0) to (2*n,n) with steps (0,1) (North) and (1,0) (East) that never goes below the diagonal {2*i,i} 0 \u003c= i \u003c= n. A peak is a consecutive North East pair.",
				"Row reverse of A120986. Described in A173020 as generalized Runyon numbers R_{n,k}^(2).",
				"(End)",
				"From _Alexander Burstein_, Jun 15 2020: (Start)",
				"T(n,k) is the number of paths from (0,0) to (3n,0) that stay on or above the horizontal axis, with unit steps u=(1,1) and d=(1,-2), that have n+1-k peaks at even height.",
				"T(n,k) is also the number of paths from (0,0) to (3n,0) that stay on or above the horizontal axis, with unit steps u=(1,1) and d=(1,-2), that have n-k peaks at odd height. (End)",
				"An apparent refinement is A338135. - _Tom Copeland_, Oct 12 2020"
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A108767/b108767.txt\"\u003eTable of n, a(n) for n = 1..1275\u003c/a\u003e",
				"Alexander Burstein, \u003ca href=\"https://arxiv.org/abs/2009.00760\"\u003eDistribution of peak heights modulo k and double descents on k-Dyck paths\u003c/a\u003e, arXiv preprint arXiv:2009.00760 [math.CO], 2020.",
				"Frédéric Chapoton and Philippe Nadeau, \u003ca href=\"http://www.mat.univie.ac.at/~slc/wpapers/FPSAC2017/37%20Chapoton%20Nadeau.pdf\"\u003eCombinatorics of the categories of noncrossing partitions\u003c/a\u003e, Séminaire Lotharingien de Combinatoire 78B (2017), Article #37.",
				"J. Cigler, \u003ca href=\"https://doi.org/10.1016/S0195-6698(87)80030-6\"\u003eSome remarks on Catalan families\u003c/a\u003e, European J. Combin., 8(3): 261-267.",
				"J.-C. Novelli, J.-Y. Thibon, \u003ca href=\"http://arxiv.org/abs/1403.5962\"\u003eHopf Algebras of m-permutations,(m+1)-ary trees, and m-parking functions\u003c/a\u003e, arXiv preprint arXiv:1403.5962 [math.CO], 2014-2020. See Fig. 5.",
				"Chao-Jen Wang, \u003ca href=\"http://people.brandeis.edu/~gessel/homepage/students/wangthesis.pdf\"\u003eApplications of the Goulden-Jackson cluster method to counting Dyck paths by occurrences of subwords\u003c/a\u003e, Thesis, 2011."
			],
			"formula": [
				"T(n, k) = binomial(n, k)*binomial(2*n, k-1)/n.",
				"T(n, n) = A000108(n) (the Catalan numbers).",
				"Sum_{k=1..n} k*T(n,k) = A025174(n).",
				"G.f.: T-1, where T = T(t, z) satisfies T = 1 + z*T^2*(T - 1 + t).",
				"From _Peter Bala_, Oct 22 2008: (Start)",
				"Define a functional I on formal power series of the form f(x) = 1 + ax + bx^2 + ... by the following iterative process. Define inductively f^(1)(x) = f(x) and f^(n+1)(x) = f(x*f^(n)(x)) for n \u003e= 1. Then set I(f(x)) = lim_{n-\u003einfinity} f^(n)(x) in the x-adic topology on the ring of formal power series; the operator I may also be defined by I(f(x)) := 1/x*series reversion of x/f(x).",
				"The o.g.f. for the array of Narayana numbers A001263 is I(1 + t*x + t*x^2 + t*x^3 + ...) = 1 + t*x + (t + t^2)*x^2 + (t + 3*t^2 + t^3)*x^3 + ... . The o.g.f. for the current array is IoI(1 + t*x + t*x^2 + t*x^3 + ...) = 1 + t*x + (t + 2*t^2)*x^2 + (t + 6*t^2 + 5*t^3)*x^3 + ... . Cf. A132081 and A141618. Alternatively, the o.g.f. of this array can be obtained from a single application of I, namely, form I(1 + t*x^2 + t*x^4 + t*x^6 + ...) = 1 + t*x^2 + (t + 2*t^2)*x^4 + (t + 6*t^2 + 5*t^3)*x^6 + ... and then replace x by sqrt(x). This is a particular case of the general result that forming the n-fold composition I^(n)(f(x)) and then replacing x with x^n produces the same result as I(f(x^n)). (End)",
				"O.g.f. is series reversion with respect to x of x/((1+x)*(1+x*u)^2). Cf. A173020. - _Peter Bala_, Sep 12 2012"
			],
			"example": [
				"T(3,2)=6 because we have uuduuuudd, uuuduuudd, uuuuduudd, uuuudduud, uuuuududd and uuuuuddud.",
				"Triangle starts:",
				"  1;",
				"  1,  2;",
				"  1,  6,   5;",
				"  1, 12,  28,   14;",
				"  1, 20,  90,  120,   42;",
				"  1, 30, 220,  550,  495,  132;",
				"  1, 42, 455, 1820, 3003, 2002, 429;",
				"  ..."
			],
			"maple": [
				"T:=(n,k)-\u003ebinomial(n,k)*binomial(2*n,k-1)/n: for n from 1 to 10 do seq(T(n,k),k=1..n) od; # yields sequence in triangular form"
			],
			"mathematica": [
				"T[n_, k_] := Binomial[n, k]*Binomial[2*n, k - 1]/n;",
				"Table[T[n, k], {n, 1, 10}, {k, 1, n}] // Flatten (* _Jean-François Alcover_, Nov 11 2017, from Maple *)"
			],
			"program": [
				"(PARI) T(n,k) = binomial(n, k)*binomial(2*n, k-1)/n; \\\\ _Andrew Howroyd_, Nov 06 2017",
				"(Python)",
				"from sympy import binomial",
				"def T(n, k): return binomial(n, k)*binomial(2*n, k - 1)//n",
				"for n in range(1, 21): print([T(n, k) for k in range(1, n + 1)]) # _Indranil Ghosh_, Nov 07 2017",
				"(R)",
				"T \u003c- function(n, k) {",
				"  choose(n, k)*choose(2*n, k - 1)/n",
				"} # _Indranil Ghosh_, Nov 07 2017",
				"(Sage)",
				"def A108767(n,k,m): return binomial(n,k)*binomial(m*n,k-1)/n",
				"flatten([[A108767(n,k,2) for k in (1..n)] for n in (1..12)]) # _G. C. Greubel_, Feb 20 2021",
				"(Magma)",
				"A108767:= func\u003c n,k,m | Binomial(n,k)*Binomial(m*n,k-1)/n \u003e;",
				"[A108767(n,k,2): k in [1..n], n in [1..12]]; // _G. C. Greubel_, Feb 20 2021"
			],
			"xref": [
				"Cf. A000108, A001764, A025174, A120986.",
				"Runyon numbers R_{n,k}^(m): A010054 (m=0), A001263 (m=1), this sequence (m=2), A173020 (m=3)."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Emeric Deutsch_, Jun 24 2005",
			"references": 7,
			"revision": 68,
			"time": "2021-02-20T23:12:09-05:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}