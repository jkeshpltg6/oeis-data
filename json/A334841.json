{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A334841",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 334841,
			"data": "0,1,-1,1,0,2,0,2,-2,0,-2,0,0,2,0,2,-1,1,-1,1,1,3,1,3,-1,1,-1,1,1,3,1,3,-3,-1,-3,-1,-1,1,-1,1,-3,-1,-3,-1,-1,1,-1,1,-1,1,-1,1,1,3,1,3,-1,1,-1,1,1,3,1,3,-2,0,-2,0,0,2,0,2,-2,0,-2,0,0,2,0,2,0,2,0,2,2,4,2,4,0",
			"name": "a(0) = 0; for n \u003e 0, a(n) = (number of 1's and 3's in base 4 representation of n) - (number of 0's and 2's in base 4 representation of n).",
			"comment": [
				"Values are even for base 4 representations of n with an even number of digits, and odd for base 4 representations of n with an odd number of digits, except for a(0)."
			],
			"formula": [
				"a(n) = 2*A139351(n) - A110592(n), n\u003e0. - _R. J. Mathar_, Sep 02 2020"
			],
			"example": [
				"      n in    #odd    #even",
				"  n  base 4  digits - digits = a(n)",
				"  =  ======  =======================",
				"  0    0        0   -            0",
				"  1    1        1   -    0   =   1",
				"  2    2        0   -    1   =  -1",
				"  3    3        1   -    0   =   1",
				"  4   10        1   -    1   =   0",
				"  5   11        2   -    0   =   2",
				"  6   12        1   -    1   =   0",
				"  7   13        2   -    0   =   2"
			],
			"maple": [
				"a:= n-\u003e `if`(n=0, 0, add(`if`(i in [1, 3], 1, -1), i=convert(n, base, 4))):",
				"seq(a(n), n=0..100);  # _Alois P. Heinz_, May 30 2020"
			],
			"mathematica": [
				"a[0] = 0; a[n_] := Total[-(-1)^(r = Range[0, 3]) * DigitCount[n, 4, r]]; Array[a, 100, 0] (* _Amiram Eldar_, May 13 2020 *)",
				"Join[{0},Table[Total[If[EvenQ[#],-1,1]\u0026/@IntegerDigits[n,4]],{n,90}]] (* _Harvey P. Dale_, Sep 06 2020 *)"
			],
			"program": [
				"(R)",
				"qnary = function(n, e, q){",
				"  e = floor(n/4)",
				"  q = n%%4",
				"  if(n == 0 ){return(0)}",
				"  if(e == 0){return(q)}",
				"  else{return(c(qnary(e), (q)))}",
				"}",
				"m = 400",
				"s = seq(2, m)",
				"v = c(0)",
				"for(i in s){",
				"  x = qnary(i-1)",
				"  x[which(x%%2!=0)] = 1",
				"  x[which(x%%2==0)] = -1",
				"  v[i] = sum(x)",
				"}",
				"(Python)",
				"import numpy as np",
				"def qnary(n):",
				"    e = n//4",
				"    q = n%4",
				"    if n == 0 : return 0",
				"    if e == 0 : return q",
				"    if e != 0 : return np.append(qnary(e), q)",
				"m = 400",
				"v = [0]",
				"for i in range(1, m+1) :",
				"    t = np.array(qnary(i))",
				"    t[t%2 != 0] = 1",
				"    t[t%2 == 0] = -1",
				"    v = np.append(v, np.sum(t))",
				"(PARI) a(n) = my(ret=0); if(n,forstep(i=0,logint(n,2),2, if(bittest(n,i),ret++,ret--))); ret; \\\\ _Kevin Ryde_, May 24 2020",
				"(Python)",
				"def A334841(n):",
				"    return 2*bin(n)[-1:1:-2].count('1')-(len(bin(n))-1)//2 if n \u003e 0 else 0 # _Chai Wah Wu_, Sep 03 2020"
			],
			"xref": [
				"Cf. A053737, A010065, A037863, A007090, A301336, A333596."
			],
			"keyword": "sign,easy,base",
			"offset": "0,6",
			"author": "_Alexander Van Plantinga_, May 13 2020",
			"references": 2,
			"revision": 33,
			"time": "2020-09-06T19:04:54-04:00",
			"created": "2020-09-02T10:14:52-04:00"
		}
	]
}