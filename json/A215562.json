{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A215562",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 215562,
			"data": "1,7,403,40350,5223915,783353872,129141898872,22745605840236,4206489449301315,807660192541534200,159752979289765273698,32371149339259024610992,6692030708288364864188400,1406943391115083641966787200,300084647544974128326709244080",
			"name": "Number of permutations of n indistinguishable copies of 1..4 with every partial sum \u003c= the same partial sum averaged over all permutations.",
			"link": [
				"Alois P. Heinz and Vaclav Kotesovec, \u003ca href=\"/A215562/b215562.txt\"\u003eTable of n, a(n) for n = 0..133\u003c/a\u003e (terms 0..60 from Alois P. Heinz)"
			],
			"formula": [
				"a(n) ~ (phi - sqrt(phi)) * 2^(8*n-1/2) / (Pi^(3/2) * n^(5/2)), where phi = (1+sqrt(5))/2. - _Vaclav Kotesovec_, Jan 31 2015"
			],
			"example": [
				"a(0) = 1: the empty permutation.",
				"a(1) = 7: (1,2,3,4), (1,2,4,3), (1,3,2,4), (1,4,2,3), (2,1,3,4), (2,1,4,3), (2,3,1,4).",
				"a(2) = 403: (1,1,2,2,3,3,4,4), (1,1,2,2,3,4,3,4), ..., (2,3,2,3,1,1,4,4), (2,3,2,3,1,4,1,4)."
			],
			"maple": [
				"b:= proc(l) option remember; local m, n, g;",
				"      m, n:= nops(l), add(i, i=l);",
				"      g:= add(i*l[i], i=1..m)-(m+1)/2*(n-1);",
				"     `if`(n\u003c2, 1, add(`if`(l[i]\u003e0 and i\u003c=g,",
				"        b(subsop(i=l[i]-1, l)), 0), i=1..m))",
				"    end:",
				"a:= n-\u003e b([n$4]):",
				"seq(a(n), n=0..15);"
			],
			"mathematica": [
				"b[l_] := b[l] = Module[{m, n, g}, {m, n} = {Length[l], Total[l]}; g = Sum[i*l[[i]], {i, 1, m}] - (m + 1)/2*(n - 1); If[n \u003c 2, 1, Sum[If[l[[i]] \u003e 0 \u0026\u0026 i \u003c= g, b[ReplacePart[l, i -\u003e l[[i]] - 1]], 0], {i, 1, m}]]];",
				"a[k_] := b[Array[k\u0026, 4]];",
				"a /@ Range[0, 15] (* _Jean-François Alcover_, Dec 18 2020, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Row n=4 of A215561."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Alois P. Heinz_, Aug 16 2012",
			"references": 2,
			"revision": 23,
			"time": "2020-12-18T11:50:52-05:00",
			"created": "2012-08-16T23:19:56-04:00"
		}
	]
}