{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A264051",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 264051,
			"data": "0,1,0,1,1,1,1,2,2,2,1,2,3,0,2,4,2,1,1,1,1,1,4,3,1,0,0,2,2,0,1,0,1,0,0,0,1,7,2,0,0,1,0,3,0,1,0,2,1,0,0,0,1,1,0,1,0,0,0,0,0,0,0,0,0,1,0,1,7,3,1,2,0,0,1,2,0,0,1,0,0,0,0,1,0,0,0,0,0,1,0,1,1,0,0,0,1,0,0,0,0,1,1,1",
			"name": "Triangle read by rows: T(n,k) (n\u003e=0, 0\u003c=k\u003c=A264078(n)) is the number of integer partitions of n having k standard Young tableaux such that no entries i and i+1 appear in the same row.",
			"comment": [
				"Row sums give A000041.",
				"Column k=0 gives A025065(n-2) for n\u003e=2."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A264051/b264051.txt\"\u003eRows n = 0..14, flattened\u003c/a\u003e",
				"S. Dulucq and O. Guibert, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(96)83009-3\"\u003eStack words, standard tableaux and Baxter permutations\u003c/a\u003e, Disc. Math. 157 (1996), 91-106.",
				"FindStat - Combinatorial Statistic Finder, \u003ca href=\"http://www.findstat.org/StatisticsDatabase/St000212\"\u003eNumber of standard Young tableaux of an integer partition such that no k and k+1 appear in the same row\u003c/a\u003e."
			],
			"formula": [
				"Sum_{k=1..A264078(n)} k*T(n,k) = A237770(n). - _Alois P. Heinz_, Nov 02 2015"
			],
			"example": [
				"Triangle begins:",
				"0,1,",
				"0,1,",
				"1,1,",
				"1,2,",
				"2,2,1,",
				"2,3,0,2,",
				"4,2,1,1,1,1,1,",
				"4,3,1,0,0,2,2,0,1,0,1,0,0,0,1,",
				"7,2,0,0,1,0,3,0,1,0,2,1,0,0,0,1,1,0,1,0,0,0,0,0,0,0,0,0,1,0,1,",
				"..."
			],
			"maple": [
				"h:= proc(l, j) option remember; `if`(l=[], 1,",
				"      `if`(l[1]=0, h(subsop(1=[][], l), j-1), add(",
				"      `if`(i\u003c\u003ej and l[i]\u003e0 and (i=1 or l[i]\u003el[i-1]),",
				"       h(subsop(i=l[i]-1, l), i), 0), i=1..nops(l))))",
				"    end:",
				"g:= proc(n, i, l) `if`(n=0 or i=1, x^h([1$n, l[]], 0),",
				"      `if`(i\u003c1, 0, g(n, i-1, l)+ `if`(i\u003en, 0,",
				"       g(n-i, i, [i, l[]]))))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=0..degree(p)))(g(n$2, [])):",
				"seq(T(n), n=0..10);  # _Alois P. Heinz_, Nov 02 2015"
			],
			"mathematica": [
				"h[l_, j_] := h[l, j] = If[l == {}, 1, If[l[[1]] == 0, h[ReplacePart[l, 1 -\u003e Sequence[]], j - 1], Sum[If[i != j \u0026\u0026 l[[i]] \u003e 0 \u0026\u0026 (i == 1 || l[[i]] \u003e l[[i - 1]]), h[ReplacePart[l, i -\u003e l[[i]] - 1], i], 0], {i, 1, Length[l]} ]]]; g[n_, i_, l_] := If[n == 0 || i == 1, x^h[Join[Array[1 \u0026, n], l], 0], If[i \u003c 1, 0, g[n, i - 1, l] + If[i \u003e n, 0, g[n - i, i, Join[{i}, l]]] ]]; T[n_] := Function[p, Table[Coefficient[p, x, i], {i, 0, Exponent[p, x]}]][g[n, n, {}]]; Table[T[n], {n, 0, 10}] // Flatten (* _Jean-François Alcover_, Jan 22 2016, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A000041, A001181, A237770, A264078."
			],
			"keyword": "nonn,tabf",
			"offset": "0,8",
			"author": "_Christian Stump_, Nov 01 2015",
			"references": 3,
			"revision": 24,
			"time": "2016-01-22T08:08:21-05:00",
			"created": "2015-11-03T03:06:48-05:00"
		}
	]
}