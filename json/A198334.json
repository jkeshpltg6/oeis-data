{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A198334",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 198334,
			"data": "1,4,5,5,8,8,10,10,9,9,9,13,13,13,12,17,13,14,17,14,14,12,14,20,13,14,19,20,14,17,12,26,13,14,17,21,20,20,17,21,14,21,20,17,22,19,17,29,21,18,17,21,26,26,16,29,21,17,14,24,21,13,26,37,18,18,20,21,22,24,21,30,21,21,23,29,18,24,17,30",
			"name": "The Balaban centric index of the rooted tree having Matula-Goebel number n.",
			"comment": [
				"The Balaban centric index of a tree is the sum of the squares of the components of the pruning partition of the tree. The pruning partition of a tree is the reverse sequence of the number of vertices of degree 1, deleted at the successive prunings. By pruning we mean the deletion of vertices of degree 1 and of their incident edges. See the Balaban reference (p. 360) and/or the Todeschini-Consonni reference (p. 42).",
				"The Matula-Goebel number of a rooted tree can be defined in the following recursive manner: to the one-vertex tree there corresponds the number 1; to a tree T with root degree 1 there corresponds the t-th prime number, where t is the Matula-Goebel number of the tree obtained from T by deleting the edge emanating from the root; to a tree T with root degree m\u003e=2 there corresponds the product of the Matula-Goebel numbers of the m branches of T."
			],
			"reference": [
				"A. T. Balaban, Chemical graphs, Theoret. Chim. Acta (Berl.) 53, 355-375, 1979.",
				"F. Goebel, On a 1-1-correspondence between rooted trees and natural numbers, J. Combin. Theory, B 29 (1980), 141-143.",
				"I. Gutman and A. Ivic, On Matula numbers, Discrete Math., 150, 1996, 131-142.",
				"I. Gutman and Yeong-Nan Yeh, Deducing properties of trees from their Matula numbers, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"D. W. Matula, A natural rooted tree enumeration by prime factorization, SIAM Review, 10, 1968, 273.",
				"R. Todeschini and V. Consonni, Handbook of Molecular Descriptors, Wiley-VCH, 2000."
			],
			"link": [
				"\u003ca href=\"/index/Mat#matula\"\u003eIndex entries for sequences related to Matula-Goebel numbers\u003c/a\u003e"
			],
			"formula": [
				"A198334(n) = sum of the squares of the components of A198333(n)."
			],
			"example": [
				"a(7)=10 because the rooted tree with Matula-Goebel number 7 is Y; it has 3 vertices of degree 1 and after the first pruning we obtain the 1-vertex tree. Thus, the pruning partition is [1,3] and 1^2 + 3^2 = 10."
			],
			"maple": [
				"with(numtheory): N := proc (n) local r, s: r := proc (n) options operator, arrow: op(1, factorset(n)) end proc: s := proc (n) options operator, arrow: n/r(n) end proc: if n = 1 then 1 elif bigomega(n) = 1 then 1+N(pi(n)) else N(r(n))+N(s(n))-1 end if end proc: c := proc (n) local r, s, b: r := proc (n) options operator, arrow: op(1, factorset(n)) end proc: s := proc (n) options operator, arrow: n/r(n) end proc: b := proc (n) if n = 1 then 1 elif n = 2 then 1 elif bigomega(n) = 1 then ithprime(b(pi(n))) else b(r(n))*b(s(n)) end if end proc: if n = 1 then 1 elif bigomega(n) = 1 then b(pi(n)) else b(r(n))*b(s(n)) end if end proc: MS := proc (m) local A, i: A[m, 1] := m: for i from 2 while 1 \u003c A[m, i-1] do A[m, i] := c(A[m, i-1]) end do: if A[m, i-2] = 2 then [seq(A[m, j], j = 1 .. i-2)] else [seq(A[m, j], j = 1 .. i-1)] end if end proc: PP := proc (n) local NVP, q: q := nops(MS(n)): NVP := map(N, MS(n)): [NVP[q], seq(NVP[q-j]-NVP[q-j+1], j = 1 .. nops(NVP)-1)] end proc: a := proc (n) options operator, arrow: add(PP(n)[k]^2, k = 1 .. nops(PP(n))) end proc: seq(a(n), n = 1 .. 80);"
			],
			"xref": [
				"Cf. A198333"
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Emeric Deutsch_, Nov 27 2011",
			"references": 2,
			"revision": 10,
			"time": "2017-03-07T11:29:57-05:00",
			"created": "2011-11-27T22:14:16-05:00"
		}
	]
}