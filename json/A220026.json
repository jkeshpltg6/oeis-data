{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A220026",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 220026,
			"data": "1,1,12500,50000,6250,16,3125,5000,12500,25000,1,50000,12500,50000,6250,4,3125,12500,2500,50000,1,50000,12500,25000,1250,8,625,50000,12500,50000,1,6250,2500,12500,6250,16,3125,50000,12500,25000,1,25000,12500,10000,6250",
			"name": "The period with which the powers of n repeat mod 1000000.",
			"comment": [
				"a(n) will always be a divisor of Phi(1000000) = 400000.",
				"This sequence is periodic with a period of 1000000 because n^i mod 1000000 = (n + 1000000)^i mod 1000000.",
				"For the odd numbers n ending in {1, 3, 7, 9} which are coprime to 10, we can expect the powers of n mod 1000000 to loop back to 1, with the value of n^a(n) mod 1000000 = 1, but for the other numbers n that are not coprime to 10, they do not loop back to 1.",
				"For the even numbers n ending in {2, 4, 6, 8}, n^a(n) mod 1000000 = 109376.",
				"For the numbers n ending in 5, n^(16*i) mod 1000000 = 890625, for all i \u003e= 1.",
				"For the numbers n ending in 0, n^i mod 1000000 = 0, for all i \u003e= 6."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A220026/b220026.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e"
			],
			"example": [
				"a(2) = 12500 since 2^i mod 1000000 = 2^(i + 12500) mod 1000000, for all i \u003e= 6.",
				"a(3) = 50000 since 3^i mod 1000000 = 3^(i + 50000) mod 1000000, for all i \u003e= 0.",
				"But a(7) = 5000 since 7^i mod 1000000 = 7^(i + 5000) mod 1000000, for all i \u003e= 0."
			],
			"mathematica": [
				"Flatten[Table[s = Table[PowerMod[n, e, 1000000], {e, 2, 1000000}]; Union[Differences[Position[s, s[[5]]]]], {n, 0, 40}]] (* _Vincenzo Librandi_, Jan 26 2013 *)"
			],
			"program": [
				"(PARI) k=1000000; for(n=0, 100, x=(n^6)%k; y=(n^7)%k; z=1; while(x!=y, x=(x*n)%k; y=(y*n*n)%k; z++); print1(z\", \"))"
			],
			"xref": [
				"Cf. A173635 (period with which the powers of n repeat mod 10).",
				"Cf. A220022 (period with which the powers of n repeat mod 100)."
			],
			"keyword": "nonn,base",
			"offset": "0,3",
			"author": "_V. Raman_, Dec 15 2012",
			"references": 2,
			"revision": 21,
			"time": "2017-07-25T02:30:05-04:00",
			"created": "2013-01-02T13:56:43-05:00"
		}
	]
}