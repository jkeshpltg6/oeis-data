{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A296220",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 296220,
			"data": "1,2,10,13,16,19,22,25,29,34,38,43,47,52,56,61,65,70,74,79,82,86,91,94,97,101,106,109,113,118,121,124,128,133,136,140,145,148,151,155,160,163,167,172,175,178,182,187,190,194,199,202,205,209,214,217,221",
			"name": "Solution of the complementary equation a(n) = a(0)*b(n-1) + a(1)*b(n-2), where a(0) = 1, a(1) = 2, b(0) = 3, and (a(n)) and (b(n)) are increasing complementary sequences.",
			"comment": [
				"The increasing complementary sequences a() and b() are uniquely determined by the titular equation and initial values. See A295862 for a guide to related sequences.",
				"P. Majer proved that a(n)/n -\u003e 4, that (a(n) - 4*n) is unbounded, and that a( ) is not a linear recurrence sequence; see the Math Overflow link and A297964. - _Clark Kimberling_, Feb 10 2018"
			],
			"link": [
				"Peter J. C. Moses, \u003ca href=\"/A296220/b296220.txt\"\u003eTable of n, a(n) for n = 0..9999\u003c/a\u003e",
				"Clark Kimberling, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL10/Kimberling/kimberling26.html\"\u003eComplementary equations\u003c/a\u003e, J. Int. Seq. 19 (2007), 1-13.",
				"Pietro Majer, \u003ca href=\"https://mathoverflow.net/questions/290220/limit-associated-with-complementary-sequences\"\u003eLimit associated with complementary sequences\u003c/a\u003e Math Overflow."
			],
			"example": [
				"a(0) = 1, a(1) = 2, b(0) = 3, b(1) = 4;",
				"a(2) = a(0)*b(1) + a(1)*b(0) = 10.",
				"Complement: (b(n)) = (3, 4, 5, 6, 7, 8, 9, 11, 12, 14, 15, 17, 18, 20, 21, 23, 24, 26, ...)."
			],
			"mathematica": [
				"mex[list_] := NestWhile[# + 1 \u0026, 1, MemberQ[list, #] \u0026];",
				"a[0] = 1; a[1] = 2; b[0] = 3;",
				"a[n_] := a[n] = a[0]*b[n - 1] + a[1]*b[n - 2];",
				"b[n_] := b[n] = mex[Flatten[Table[Join[{a[n]}, {a[i], b[i]}], {i, 0, n - 1}]]];",
				"u = Table[a[n], {n, 0, 500}];  (* A296220 *)",
				"Table[b[n], {n, 0, 20}]"
			],
			"xref": [
				"Cf. A296000."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Clark Kimberling_, Dec 08 2017",
			"references": 4,
			"revision": 28,
			"time": "2018-11-06T11:44:11-05:00",
			"created": "2017-12-09T19:28:56-05:00"
		}
	]
}