{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A143414",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 143414,
			"data": "0,2,30,492,9620,222630,5989242,184139480,6377545512,245868202890,10446648201110,485126443539012,24449173476952380,1329144227959100462,77535552689576436210,4831278674685354629040,320262424087652686405712",
			"name": "Apéry-like numbers for the constant 1/e: a(n) = (1/(n-1)!)*Sum_{k = 0..n-1} binomial(n-1,k)*(2*n-k)!.",
			"comment": [
				"This sequence satisfies the recursion (n-1)^2*a(n) - n^2*a(n-2) = (2*n-1)*(2*n^2-2*n+1)*a(n-1), which leads to a rapidly converging series for the constant 1/e: 1/e = 1/2 - 2 * Sum_{n \u003e= 2} (-1)^n * n^2/(a(n)*a(n-1)).",
				"Notice the striking resemblance to the theory of the Apéry numbers A(n) = A005258(n), which satisfy a similar recurrence relation n^2*A(n) - (n-1)^2*A(n-2) = (11*n^2-11*n+3)*A(n-1) and which appear in the series acceleration formula zeta(2) = 5*Sum_{n\u003e=1} 1/(n^2*A(n)*A(n-1)). Compare with A143413 and A143415."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A143414/b143414.txt\"\u003eTable of n, a(n) for n = 0..365\u003c/a\u003e",
				"A. van der Poorten, \u003ca href=\"http://pracownicy.uksw.edu.pl/mwolf/Poorten_MI_195_0.pdf\"\u003e A proof that Euler missed ... Apery's proof of the irrationality of zeta(3). An informal report\u003c/a\u003e, Math. Intelligencer 1 (1978/79), no. 4, 195-203."
			],
			"formula": [
				"a(n) = (1/(n-1)!)*Sum_{k = 0..n-1} binomial(n-1,k)*(2*n-k)!.",
				"Recurrence relation: a(0) = 0, a(1) = 2, (n-1)^2*a(n) - n^2*a(n-2) = (2*n-1)*(2*n^2-2*n+1)*a(n-1), n \u003e= 2.",
				"Let b(n) denote the solution to this recurrence with initial conditions b(0) = -1, b(1) = 1. Then b(n) = A143413(n) = (1/(n-1)!)*Sum_{k = 0..n+1} (-1)^k*binomial(n+1,k)*(2*n-k)!.",
				"The rational number b(n)/a(n) is equal to the Padé approximation to exp(x) of degree (n+1,n-1) evaluated at x = -1 and b(n)/a(n) -\u003e 1/e very rapidly. For example, |b(100)/a(100) - 1/e| is approximately 2.177 * 10^(-437).",
				"The identity a(n)*b(n-1) - a(n-1)*b(n) = (-1)^n *2*n^2 leads to rapidly converging series for the constants 1/e and e: 1/e = 1/2 - 2*Sum_{n \u003e= 2} (-1)^n * n^2/(a(n)*a(n-1)) = 1/2 - 2*(2^2/(2*30) - 3^2/(30*492) + 4^2/(492*9620) - ...); e = 2 * Sum_{n \u003e= 1} (-1)^n * n^2/(b(n)*b(n-1)) = 2*(1 + 2^2/(1*11) - 3^2/(11*181) + 4^2/(181*3539) - ...).",
				"a(n) = (BesselK(n-1/2,1/2)-(1-2*n)*BesselK(n+1/2,1/2)) * exp(1/2)/(2*Pi^(1/2)). - _Mark van Hoeij_, Nov 12 2009",
				"a(n) = ((2*n)!/(n-1)!)*hypergeom([1-n], [-2*n], 1)) for n \u003e 0. - _Peter Luschny_, May 14 2020",
				"a(n) ~ 2^(2*n + 1/2) * n^(n+1) / exp(n - 1/2). - _Vaclav Kotesovec_, Jul 11 2021"
			],
			"maple": [
				"a := n -\u003e 1/(n-1)!*add (binomial(n-1,k)*(2*n-k)!,k = 0..n-1): seq(a(n),n = 0..19);",
				"# Alternative:",
				"A143414 := n -\u003e `if`(n=0, 0, ((2*n)!/(n-1)!)*hypergeom([1-n], [-2*n], 1)):",
				"seq(simplify(A143414(n)), n = 0..16); # _Peter Luschny_, May 14 2020"
			],
			"mathematica": [
				"Table[(1/(n-1)!)*Sum[Binomial[n-1,k]*(2*n-k)!, {k,0,n-1}], {n,0,50}] (* _G. C. Greubel_, Oct 24 2017 *)"
			],
			"program": [
				"(PARI) for(n=0,25, print1((1/(n-1)!)*sum(k=0,n-1, binomial(n-1,k)*(2*n-k)!), \", \")) \\\\ _G. C. Greubel_, Oct 24 2017"
			],
			"xref": [
				"Cf. A143413, A143415.",
				"The Apéry-like numbers [or Apéry-like sequences, Apery-like numbers, Apery-like sequences] include A000172, A000984, A002893, A002895, A005258, A005259, A005260, A006077, A036917, A063007, A081085, A093388, A125143 (apart from signs), A143003, A143007, A143413, A143414, A143415, A143583, A183204, A214262, A219692,A226535, A227216, A227454, A229111 (apart from signs), A260667, A260832, A262177, A264541, A264542, A279619, A290575, A290576. (The term \"Apery-like\" is not well-defined.)"
			],
			"keyword": "easy,nonn",
			"offset": "0,2",
			"author": "_Peter Bala_, Aug 14 2008",
			"references": 34,
			"revision": 26,
			"time": "2021-07-11T02:59:10-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}