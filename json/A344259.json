{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A344259",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 344259,
			"data": "0,1,1,1,2,2,3,3,2,2,2,2,3,3,3,3,4,4,4,4,5,5,5,5,6,6,6,6,7,7,7,7,4,4,4,4,4,4,4,4,5,5,5,5,5,5,5,5,6,6,6,6,6,6,6,6,7,7,7,7,7,7,7,7,8,8,8,8,8,8,8,8,9,9,9,9,9,9,9,9,10,10,10,10,10",
			"name": "For any number n with binary expansion (b(1), ..., b(k)), the binary expansion of a(n) is (b(1), ..., b(ceiling(k/2))).",
			"comment": [
				"Leading zeros are ignored."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A344259/b344259.txt\"\u003eTable of n, a(n) for n = 0..8191\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"formula": [
				"a(A020330(n)) = n.",
				"a(A006995(n+1)) = A162751(n).",
				"a(n XOR A344220(n)) = a(n) (where XOR denotes the bitwise XOR operator)."
			],
			"example": [
				"The first terms, alongside their binary expansion, are:",
				"  n   a(n)  bin(n)  bin(a(n))",
				"  --  ----  ------  ---------",
				"   0     0       0          0",
				"   1     1       1          1",
				"   2     1      10          1",
				"   3     1      11          1",
				"   4     2     100         10",
				"   5     2     101         10",
				"   6     3     110         11",
				"   7     3     111         11",
				"   8     2    1000         10",
				"   9     2    1001         10",
				"  10     2    1010         10",
				"  11     2    1011         10",
				"  12     3    1100         11",
				"  13     3    1101         11",
				"  14     3    1110         11",
				"  15     3    1111         11"
			],
			"mathematica": [
				"Array[FromDigits[First@Partition[l=IntegerDigits[#,2],Ceiling[Length@l/2]],2]\u0026,100,0] (* _Giorgos Kalogeropoulos_, May 14 2021 *)"
			],
			"program": [
				"(PARI) a(n) = n\\2^(#binary(n)\\2)",
				"(Python)",
				"def a(n): b = bin(n)[2:]; return int(b[:(len(b)+1)//2], 2)",
				"print([a(n) for n in range(85)]) # _Michael S. Branicky_, May 14 2021"
			],
			"xref": [
				"Cf. A006995, A020330, A162751, A344220."
			],
			"keyword": "nonn,base,easy",
			"offset": "0,5",
			"author": "_Rémy Sigrist_, May 13 2021",
			"references": 2,
			"revision": 15,
			"time": "2021-05-15T01:43:52-04:00",
			"created": "2021-05-14T16:46:15-04:00"
		}
	]
}