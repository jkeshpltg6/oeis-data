{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A091569",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 91569,
			"data": "1,3,5,7,9,11,13,15,8,6,4,2,12,10,36,34,32,30,28,26,24,22,20,18,16,14,52,50,48,35,33,31,29,27,25,23,21,19,17,64,62,60,40,38,148,146,144,37,39,41,43,45,47,49,51,53,55,57,59,61,63,65,67,69,71,73,75,77,79,81,83",
			"name": "a(1) = 1; for n \u003e 1, a(n) is the smallest positive integer not already used such that a(n)*a(n-1) + 1 is a perfect square.",
			"comment": [
				"Does this sequence contain every positive integer? We could get an equally interesting sequence by choosing a(1) to be any other positive integer.",
				"A sequence with the same condition but without the requirement for a(n) to be distinct would end up repeating (1,3) or (2,4), depending on the initial term. - _Ivan Neretin_, May 26 2015"
			],
			"link": [
				"Ivan Neretin, \u003ca href=\"/A091569/b091569.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"10 is followed by 36 because 10*36+1 = 19^2 and 8 and 12 were already used."
			],
			"maple": [
				"N:= 10^4: Used:= Vector(N,datatype=integer[4]):",
				"a[1]:= 1: blocked:= false: Used[1]:= 1:",
				"for n from 2 to 100 while not(blocked) do",
				"    ndone:= false;",
				"    if n = 2 then T:= [0]",
				"      else T:= select(t -\u003e t^2 mod a[n-1] = 1, [$0..a[n-1]-1])",
				"      fi;",
				"    for s from 0 while not (ndone) do",
				"       for t in T while not (ndone) do",
				"         x:= s * a[n-1] + t;",
				"         if x \u003c= 1 then next fi;",
				"         y:= (x^2-1)/a[n-1];",
				"         if y \u003e N then blocked:= true; ndone:= true",
				"         elif Used[y] = 0 then",
				"            a[n]:= y;",
				"            Used[y]:= 1;",
				"            ndone:= true;",
				"            print(n,y);",
				"         fi",
				"       od",
				"    od",
				"od:",
				"seq(a[n],n=1..100); # _Robert Israel_, May 26 2015"
			],
			"mathematica": [
				"a = {1}; Do[a = Join[a, Select[Complement[Range[(Max[a] + 1)*n], a], IntegerQ[Sqrt[#*a[[-1]] + 1]] \u0026, 1]], {n, 2, 71}]; a (* _Ivan Neretin_, May 26 2015 *)"
			],
			"program": [
				"(MATLAB) program by D. Wasserman A = zeros(1, 100); A(1) = 1; used = zeros(1, 1000); used(1) = 1; for i = 2:100; found = 0; k = 0; while found == 0; k = k + 1; if used(k) == 0; s = sqrt(k*A(i - 1) + 1); if s == floor(s); A(i) = k; used(k) = 1; found = 1; end; end; end; end; A"
			],
			"xref": [
				"Cf. A083203."
			],
			"keyword": "easy,nonn",
			"offset": "1,2",
			"author": "_David Wasserman_, Mar 04 2004",
			"references": 4,
			"revision": 12,
			"time": "2016-04-25T12:05:00-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}