{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A248692",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 248692,
			"data": "1,2,4,4,8,8,16,8,16,16,32,16,64,32,32,16,128,32,256,32,64,64,512,32,64,128,64,64,1024,64,2048,32,128,256,128,64,4096,512,256,64,8192,128,16384,128,128,1024,32768,64,256,128,512,256,65536,128,256,128,1024,2048,131072,128,262144,4096,256,64",
			"name": "Fully multiplicative with a(prime(i)) = 2^i; If n = product_{k \u003e= 1} (p_k)^(c_k) where p_k is k-th prime A000040(k) and c_k \u003e= 0 then a(n) = product_{k \u003e= 1} 2^(k*c_k).",
			"comment": [
				"Equally, if n = p_i * p_j * ... * p_k, where p_i, p_j, ..., p_k are the primes A000040(i), A000040(j), ..., A000040(k) in the prime factorization of n (indices i, j, ..., k not necessarily distinct), then a(n) = 2^i * 2^j * 2^k.",
				"a(1) = 1 (empty product).",
				"Fully multiplicative with a(prime(i)) = 2^i."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A248692/b248692.txt\"\u003eTable of n, a(n) for n = 1..2048\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 2^A056239(n) = A000079(A056239(n)).",
				"Other identities. For all n \u003e= 1:",
				"a(A122111(n)) = a(n).",
				"a(A000040(n)) = A000079(n).",
				"For all n \u003e= 0:",
				"a(A000079(n)) = A000079(n).",
				"a(n) = Product_{d|n} 2^A297109(d). - _Antti Karttunen_, Feb 01 2021"
			],
			"maple": [
				"a:= n-\u003e mul((2^numtheory[pi](i[1]))^i[2], i=ifactors(n)[2]):",
				"seq(a(n), n=1..64);  # _Alois P. Heinz_, Jan 14 2021"
			],
			"mathematica": [
				"a[n_] := Product[{p, e} = pe; (2^PrimePi[p])^e, {pe, FactorInteger[n]}];",
				"Array[a, 100] (* _Jean-François Alcover_, Jan 03 2022 *)"
			],
			"program": [
				"(MIT/GNU Scheme, with Aubrey Jaffer's SLIB Scheme library)",
				"(require 'factor)",
				"(define (A248692v2 n) (apply * (map A000079 (map A049084 (factor n)))))",
				";; Alternatively:",
				"(define (A248692 n) (A000079 (A056239 n)))",
				"(PARI) A248692(n) = if(1==n,n,my(f=factor(n)); for(i=1,#f~,f[i,1] = 2^primepi(f[i,1])); factorback(f)); \\\\ _Antti Karttunen_, Feb 01 2021"
			],
			"xref": [
				"Cf. A000040, A000079, A003961, A003965, A048675, A056239, A061142, A122111, A297109."
			],
			"keyword": "nonn,mult,changed",
			"offset": "1,2",
			"author": "_Antti Karttunen_, Oct 11 2014",
			"references": 4,
			"revision": 31,
			"time": "2022-01-03T11:14:02-05:00",
			"created": "2014-10-12T01:01:59-04:00"
		}
	]
}