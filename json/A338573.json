{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A338573",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 338573,
			"data": "1,2,2,3,1,3,4,3,3,4,5,2,1,2,5,6,4,4,4,4,6,7,3,4,1,4,3,7,8,5,2,5,5,2,5,8,9,4,5,3,1,3,5,4,9,10,6,5,5,5,5,5,5,6,10,11,5,3,2,5,1,5,2,3,5,11,12,7,6,6,5,5,5,5,6,6,7,12,13,6,6,4,6,4,1,4,6,4,6,6,13",
			"name": "Array read by ascending antidiagonals: T(m,n) (m, n \u003e= 1) is the minimum number of unit resistors needed to produce resistance m/n.",
			"comment": [
				"Karnofsky (2004, p. 5): \"[...] if some circuit has resistance m/n then some other circuit likely has n/m. In fact, for 9 or fewer resistors, this symmetry is perfect. However, for 10 resistors the following values are achieved, but not their inverses: 95/106, 101/109, 98/103, 97/98, 103/101, 97/86, 110/91, 103/83, 130/101, 103/80, 115/89, 106/77, 109/77, 98/67, 101/67\". That means, that T(m,n) = T(n,m), if T(m,n) \u003c= 9.",
				"This starts with the values of A113881, but the Karnofsky comment says that T(n,m) is not symmetric, whereas the count of tiles in A113881 is. - _R. J. Mathar_, Nov 06 2020",
				"The first difference where T(m,n) = T(n,m), but differs from the corresponding entry of A113881 occurs for (n,m) = (154,167) and (n,m) = (167,154), both representable by networks with non-planar graphs of 11 resistors, whereas A113881 counts 12 tiles. See Pfoertner link for illustration of more differences. - _Hugo Pfoertner_, Nov 13 2020"
			],
			"reference": [
				"Technology Review's Puzzle Corner, How many different resistances can be obtained by combining 10 one ohm resistors? Oct 3, 2003."
			],
			"link": [
				"Joel Karnofsky, \u003ca href=\"http://cs.nyu.edu/~gottlieb/tr/overflow/2003-dec-2.pdf\"\u003eSolution of problem from Technology Review's Puzzle Corner Oct 3, 2003\u003c/a\u003e, Feb 23 2004.",
				"Hugo Pfoertner, \u003ca href=\"/A338573/a338573.pdf\"\u003eWhere A338573 differs from A113881\u003c/a\u003e, x,y \u003c= 380.",
				"\u003ca href=\"/index/Res#resistances\"\u003eIndex to sequences related to resistances\u003c/a\u003e."
			],
			"example": [
				"T(1,2) = 2: at least 2 unit resistors in parallel are needed for resistance 1/2.",
				"T(2,1) = 2: at least 2 unit resistors in series are needed for resistance 2 = 2/1.",
				"T(11,13) = 6: the following \"bridge\" has resistance Bri(Par(1,1),1,1,1,1) = 11/13 (see A337516 for definitions):",
				".",
				"                  (+)",
				"                  / \\",
				"              ---*   \\",
				"             /  /     \\",
				"           (1)(1)     (1)",
				"             \\ |       |",
				"              \\|       |",
				"               *--(1)--*",
				"                \\     /",
				"                (1) (1)",
				"                  \\ /",
				"                  (-)",
				".",
				"T(13,11) = 6: Bri(Ser(1,1),1,1,1,1) = 13/11.",
				"T(95,106) = 10, but T(106,95) \u003e 10: Karnofsky (2004, p. 5), see comment."
			],
			"xref": [
				"Cf. A048211, A113881, A180414, A174283, A337516, A337517.",
				"Non-reciprocal ratios: A338601/A338602 (10 resistors), A338581/A338591 (11 resistors), A338582/A338592 (12 resistors)."
			],
			"keyword": "tabl,nonn,hard",
			"offset": "1,2",
			"author": "_Rainer Rosenthal_, Nov 05 2020",
			"references": 10,
			"revision": 56,
			"time": "2020-11-15T12:49:13-05:00",
			"created": "2020-11-05T23:01:38-05:00"
		}
	]
}