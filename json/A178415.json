{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A178415",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 178415,
			"data": "1,3,5,9,13,21,7,37,53,85,17,29,149,213,341,11,69,117,597,853,1365,25,45,277,469,2389,3413,5461,15,101,181,1109,1877,9557,13653,21845,33,61,405,725,4437,7509,38229,54613,87381,19,133,245,1621,2901,17749,30037",
			"name": "Array T(n,k) of odd Collatz preimages read by antidiagonals.",
			"comment": [
				"Every odd number occurs uniquely in this array. See A178414."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A178415/b178415.txt\"\u003eT(n,k) for n = 1..50, by antidiagonals\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CollatzProblem.html\"\u003eCollatz Problem\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Collatz_conjecture\"\u003eCollatz conjecture\u003c/a\u003e",
				"\u003ca href=\"/index/3#3x1\"\u003eIndex entries for sequences related to 3x+1 (or Collatz) problem\u003c/a\u003e"
			],
			"formula": [
				"From _Bob Selcoe_, Apr 09 2015 (Start):",
				"T(n,k) = 4*T(n,k-1) + 1.",
				"T(n,k) = T(1,k) + 2^(2k+1)*(n-1)/2 when n is odd;",
				"T(n,k) = T(2,k) + 4^k*(n-2)/2 when n \u003e= 2 and n is even. So equivalently:",
				"T(n,k) = T(n-2,k) + 2^(2k+1) when n is odd; and",
				"T(n,k) = T(n-2,k) + 4^k when n is even.",
				"Let j be the n-th positive odd number coprime with 3. Then:",
				"T(n,k) = (j*4^k - 1)/3 when j == 1 (mod 3); and",
				"T(n,k) = (j*2^(2k-1) - 1)/3 when j == 2 (mod 3).",
				"(End)",
				"From _Wolfdieter Lang_, Sep 18 2021: (Start)",
				"T(n, k) = ((3*n - 1)*4^k - 2)/6 if n is even, and ((3*n - 2)*4^k - 1)/3 if n is odd, for n \u003e= 1 and  k \u003e= 1. Also for n = 0: -A007583(k-1), with A007583(-1) = 1/2, and for k = 0: A022998(n-1)/2, with A022998(-1) = -1.",
				"O.g.f. for array T (with row n = 0 and column k = 0; z for rows and x for columns): G(z, x) = (1/(2*(1-x)*(1-4*x)*(1-z^2)^2)) * ((2*x-4)*z^3 + (3-5*x)*z^2 + 2*x*z + 3*x - 1). (End)"
			],
			"example": [
				"Array T begins:",
				".    1    5   21    85   341   1365    5461   21845    87381   349525",
				".    3   13   53   213   853   3413   13653   54613   218453   873813",
				".    9   37  149   597  2389   9557   38229  152917   611669  2446677",
				".    7   29  117   469  1877   7509   30037  120149   480597  1922389",
				".   17   69  277  1109  4437  17749   70997  283989  1135957  4543829",
				".   11   45  181   725  2901  11605   46421  185685   742741  2970965",
				".   25  101  405  1621  6485  25941  103765  415061  1660245  6640981",
				".   15   61  245   981  3925  15701   62805  251221  1004885  4019541",
				".   33  133  533  2133  8533  34133  136533  546133  2184533  8738133",
				".   19   77  309  1237  4949  19797   79189  316757  1267029  5068117",
				"- _L. Edson Jeffery_, Mar 11 2015",
				"From _Bob Selcoe_, Apr 09 2015 (Start):",
				"n=5, j=13: T(5,3) = 277 = (13*4^3 - 1)/3;",
				"n=6, j=17: T(6,4) = 725 = (17*2^7 - 1)/3.",
				"(End)"
			],
			"mathematica": [
				"t[n_,1] := t[n,1] = If[OddQ[n],4n-3,2n-1]; t[n_,k_] := t[n,k] = 4*t[n,k-1]+1; Flatten[Table[t[n-i+1,i], {n,20}, {i,n}]]"
			],
			"xref": [
				"Rows of array: -A007583(k-1) (n=0), A002450 (n=1), A072197(k-1) (n=2), A206374(n=3), A072261 (n=4), A323824 (n=5), A072262 (n=6), A330246 (n=7), A072201 (n=8), ...",
				"Columns of array: A022998(n-1)/2 (k=0), A178414 (k=1), ...",
				"Cf. A347834 (permuted rows of the array)."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "1,2",
			"author": "_T. D. Noe_, May 28 2010",
			"references": 16,
			"revision": 39,
			"time": "2021-12-10T11:18:14-05:00",
			"created": "2010-07-11T03:00:00-04:00"
		}
	]
}