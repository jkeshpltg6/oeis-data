{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A077443",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 77443,
			"data": "3,5,13,27,75,157,437,915,2547,5333,14845,31083,86523,181165,504293,1055907,2939235,6154277,17131117,35869755,99847467,209064253,581953685,1218515763,3391874643,7102030325,19769294173,41393666187",
			"name": "Numbers k such that (k^2 - 7)/2 is a square.",
			"comment": [
				"Lim_{n -\u003e inf} a(n)/a(n-2) = 3 + 2*sqrt(2) = R1*R2. Lim_{k -\u003e inf} a(2*k-1)/a(2*k) = (9 + 4*sqrt(2))/7 = R1 = A156649 (ratio #1). Lim_{k -\u003e inf} a(2*k)/a(2*k-1) = (11 + 6*sqrt(2))/7 = R2 (ratio #2).",
				"Also gives solutions \u003e 3 to the equation x^2-4 = floor(x*r*floor(x/r)) where r=sqrt(2). - _Benoit Cloitre_, Feb 14 2004",
				"From _Paul Curtz_, Dec 15 2012: (Start)",
				"a(n-1) and A006452(n) are companions. Like A000129 and A001333.",
				"Reduced mod 10 this is a sequence of period 12: 3, 5, 3, 7, 5, 7, 7, 5, 7, 3, 5, 3.",
				"(End)",
				"The Pisano periods (periods of the sequence reducing a(n) modulo m) for m\u003e=1 are 1,  1,  8,  4, 12,  8,  6,  4, 24, 12, 24,  8, 28, ... _R. J. Mathar_, Dec 15 2012",
				"Positive values of x (or y) satisfying x^2 - 6xy + y^2 + 56 = 0. - _Colin Barker_, Feb 08 2014",
				"From _Wolfdieter Lang_, Feb 05 2015: (Start)",
				"a(n+1) gives for n \u003e= 0 all positive x solutions of the (generalized) Pell equation x^2 - 2*y^2 = +7.",
				"The corresponding y solutions are given in A077442(n), n \u003e= 0. The, e.g., the Nagell reference for finding all solutions.",
				"Because the primitive Pythagorean triangle (3,4,5) is the only one with the sum of legs equal to 7 all positive solutions (x(n),y(n)) = (a(n+1),A077442(n)) of the Pell equation x^2 - 2*y^2 = +7 satisfy x(n) - y(n) \u003c y(n) if n \u003e= 1; only the first solution (x(0),y(0)) = (3,2) satisfies 3-1 \u003e 1. Proof: Primitive Pythagorean triangles are characterized by the positive integer pairs [u,v] with  u+v odd, gcd(u,v) = 1 and u \u003e v. See the Niven et al. reference, Theorem 5.5, p. 232. The leg sum is L = (u+v)^2 - 2*v^2. With L = 7, x = u+v and y = v, every solution (x(n),y(n)) with x(n)-y(n) = u(n) \u003e v(n) = y(n) will correspond to a primitive Pythagorean triangle. Note that because of gcd(x,y) = 1 also gcd(u,v) = 1. But there is only one such triangle with L=7, namely the one with  [u(0),v(0)] = [2,1]. All other solutions with n \u003e= 1 must therefore satisfy x(n)-y(n) \u003c y(n). (End)"
			],
			"reference": [
				"A. H. Beiler, \"The Pellian.\" Ch. 22 in Recreations in the Theory of Numbers: The Queen of Mathematics Entertains. Dover, New York, New York, pp. 248-268, 1966.",
				"L. E. Dickson, History of the Theory of Numbers, Vol. II, Diophantine Analysis. AMS Chelsea Publishing, Providence, Rhode Island, 1999, pp. 341-400.",
				"Peter G. L. Dirichlet, Lectures on Number Theory (History of Mathematics Source Series, V. 16); American Mathematical Society, Providence, Rhode Island, 1999, pp. 139-147.",
				"T. Nagell, Introduction to Number Theory, Chelsea Publishing Company, 1964, Theorem 109, pp. 207-208 with Theorem 104, pp. 197-198.",
				"Ivan Niven, Herbert S. Zuckerman and Hugh L. Montgomery, An Introduction to the Theory Of Numbers, Fifth Edition, John Wiley and Sons, Inc., NY 1991."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A077443/b077443.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Jeremiah Bartz, Bruce Dearden, and Joel Iiams, \u003ca href=\"https://arxiv.org/abs/1810.07895\"\u003eClasses of Gap Balancing Numbers\u003c/a\u003e, arXiv:1810.07895 [math.NT], 2018.",
				"Jeremiah Bartz, Bruce Dearden, and Joel Iiams, \u003ca href=\"https://ajc.maths.uq.edu.au/pdf/77/ajc_v77_p318.pdf\"\u003eCounting families of generalized balancing numbers\u003c/a\u003e, The Australasian Journal of Combinatorics (2020) Vol. 77, Part 3, 318-325.",
				"J. J. O'Connor and E. F. Robertson, \u003ca href=\"http://www-gap.dcs.st-and.ac.uk/~history/HistTopics/Pell.html\"\u003eHistory of Pell's Equation\u003c/a\u003e",
				"J. P. Robertson, \u003ca href=\"http://www.jpr2718.org/pell.pdf\"\u003eSolving the Generalized Pell Equation\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PellEquation.html\"\u003ePell Equation.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,6,0,-1)"
			],
			"formula": [
				"a(2n+1) = A038762(n). a(2n) = A101386(n-1).",
				"The same recurrences hold for the odd and the even indices: a(n+2) = 6*a(n) - a(n-2), a(n+1) = 3*a(n) + 2*(2*a(n)^2-14)^0.5 - _Richard Choulet_, Oct 11 2007",
				"O.g.f.: -x*(x-1)*(3*x^2+8*x+3) / ( (x^2+2*x-1)*(x^2-2*x-1) ). - _R. J. Mathar_, Nov 23 2007",
				"If n is even a(n) = (1/2)*(3+sqrt(2))*(3+2*sqrt(2))^-(1/2)*n) +(1/2)*(3-sqrt(2))*(3-2*sqrt(2))^-(1/2)*n); if n is odd a(n) = (1/2)*(3+sqrt(2))*(3+2*sqrt(2))^((1/2)n-1/2)) +(1/2)*(3-sqrt(2))*(3-2*sqrt(2))^((1/2)n-1/2)). - _Antonio Alberto Olivares_, Apr 20 2008",
				"a(n) = A000129(n+1) + (-1)^n*A176981(n-1), n\u003e1. - _R. J. Mathar_, Jul 03 2011",
				"a(n) = A000129(n+1) -(-1)^n*A000129(n-2), rephrasing the formula above. - _Paul Curtz_, Dec 07 2012",
				"a(n) = sqrt(8*A216134(n)^2 + 8*A216134(n) + 9) = 2*A124124(n) + 1. - _Raphie Frank_, May 24 2013"
			],
			"example": [
				"a(3)^2 - 2*A077442(2)^2 = 13^2 - 2*9^2  = +7. - _Wolfdieter Lang_, Feb 05 2015"
			],
			"mathematica": [
				"LinearRecurrence[{0,6,0,-1},{3,5,13,27},50] (* _Sture Sjöstedt_, Oct 09 2012 *)"
			],
			"xref": [
				"Cf. A077442, A038762, A038761, A101386 A253811."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_Gregory V. Richardson_, Nov 06 2002",
			"ext": [
				"More terms from _Richard Choulet_, Oct 11 2007",
				"Edited: replaced n by a(n) in the name. Moved Pell remarks to the comment section. Added cross references. - _Wolfdieter Lang_, Feb 05 2015"
			],
			"references": 10,
			"revision": 74,
			"time": "2020-09-17T06:17:52-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}