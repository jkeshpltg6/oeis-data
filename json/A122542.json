{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A122542",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 122542,
			"data": "1,0,1,0,2,1,0,2,4,1,0,2,8,6,1,0,2,12,18,8,1,0,2,16,38,32,10,1,0,2,20,66,88,50,12,1,0,2,24,102,192,170,72,14,1,0,2,28,146,360,450,292,98,16,1,0,2,32,198,608,1002,912,462,128,18,1",
			"name": "Triangle T(n,k), 0 \u003c= k \u003c= n, read by rows, given by [0, 2, -1, 0, 0, 0, 0, 0, ...] DELTA [1, 0, 0, 0, 0, 0, 0, 0, ...] where DELTA is the operator defined in A084938.",
			"comment": [
				"Riordan array (1, x*(1+x)/(1-x)). Rising and falling diagonals are the tribonacci numbers A000213, A001590."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A122542/b122542.txt\"\u003eRows n = 0..120 of triangle, flattened\u003c/a\u003e",
				"Bela Bajnok, \u003ca href=\"https://arxiv.org/abs/1705.07444\"\u003eAdditive Combinatorics: A Menu of Research Problems\u003c/a\u003e, arXiv:1705.07444 [math.NT], May 2017. See Sect. 2.3."
			],
			"formula": [
				"Sum_{k=0..n} x^k*T(n,k) = A000007(n), A001333(n), A104934(n), A122558(n), A122690(n), A091928(n)  for x = 0, 1, 2, 3, 4, 5. - _Philippe Deléham_, Jan 25 2012",
				"Sum_{k=0..n} 3^(n-k)*T(n,k) = A086901(n).",
				"Sum_{k=0..n} 2^(n-k)*T(n,k) = A007483(n-1), n \u003e= 1. - _Philippe Deléham_, Oct 08 2006",
				"T(2*n,n) = A123164(n).",
				"T(n,k) = T(n-1,k) + T(n-1,k-1) + T(n-2,k-1), n \u003e 1. - _Philippe Deléham_, Jan 25 2012",
				"G.f.: (1-x)/(1-(1+y)*x-y*x^2). - _Philippe Deléham_, Mar 02 2012"
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  0, 1;",
				"  0, 2,  1;",
				"  0, 2,  4,   1;",
				"  0, 2,  8,   6,   1;",
				"  0, 2, 12,  18,   8,    1;",
				"  0, 2, 16,  38,  32,   10,   1;",
				"  0, 2, 20,  66,  88,   50,  12,   1;",
				"  0, 2, 24, 102, 192,  170,  72,  14,   1;",
				"  0, 2, 28, 146, 360,  450, 292,  98,  16,  1;",
				"  0, 2, 32, 198, 608, 1002, 912, 462, 128, 18, 1;"
			],
			"mathematica": [
				"CoefficientList[#, y]\u0026 /@ CoefficientList[(1-x)/(1 - (1+y)x - y x^2) + O[x]^11, x] // Flatten (* _Jean-François Alcover_, Sep 09 2018 *)"
			],
			"program": [
				"(Haskell)",
				"a122542 n k = a122542_tabl !! n !! k",
				"a122542_row n = a122542_tabl !! n",
				"a122542_tabl = map fst $ iterate",
				"   (\\(us, vs) -\u003e (vs, zipWith (+) ([0] ++ us ++ [0]) $",
				"                      zipWith (+) ([0] ++ vs) (vs ++ [0]))) ([1], [0, 1])",
				"-- _Reinhard Zumkeller_, Jul 20 2013, Apr 17 2013",
				"(Sage)",
				"def A122542_row(n):",
				"    @cached_function",
				"    def prec(n, k):",
				"        if k==n: return 1",
				"        if k==0: return 0",
				"        return prec(n-1,k-1)+2*sum(prec(n-i,k-1) for i in (2..n-k+1))",
				"    return [prec(n, k) for k in (0..n)]",
				"for n in (0..10): print(A122542_row(n)) # _Peter Luschny_, Mar 16 2016"
			],
			"xref": [
				"Other versions: A035607, A113413, A119800, A266213.",
				"Diagonals: A000012, A005843, A001105, A035597-A035606.",
				"Columns: A000007, A040000, A008575, A005899, A008412-A008416, A008418, A008420, A035706-A035745.",
				"Cf. A155161, A059283."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Philippe Deléham_, Sep 19 2006, May 28 2007",
			"references": 21,
			"revision": 46,
			"time": "2020-02-26T13:46:35-05:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}