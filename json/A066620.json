{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A066620",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 66620,
			"data": "0,0,0,0,0,1,0,0,0,1,0,2,0,1,1,0,0,2,0,2,1,1,0,3,0,1,0,2,0,7,0,0,1,1,1,4,0,1,1,3,0,7,0,2,2,1,0,4,0,2,1,2,0,3,1,3,1,1,0,13,0,1,2,0,1,7,0,2,1,7,0,6,0,1,2,2,1,7,0,4,0,1,0,13,1,1,1,3,0,13,1,2,1,1,1,5,0,2,2,4,0,7,0",
			"name": "Number of unordered triples of distinct pairwise coprime divisors of n.",
			"comment": [
				"a(m) = a(n) if m and n have same factorization structure."
			],
			"reference": [
				"Amarnath Murthy, Decomposition of the divisors of a natural number into pairwise coprime sets, Smarandache Notions Journal, vol. 12, No. 1-2-3, Spring 2001.pp 303-306."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A066620/b066620.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"In the reference it is shown that if k is a squarefree number with r prime factors and m with (r+1) prime factors then a(m) = 4*a(k) + 2^k - 1.",
				"a(n) = (tau(n^3)-3*tau(n)+2)/6. - _Vladeta Jovovic_, Nov 27 2004"
			],
			"example": [
				"a(24) = 3: the divisors of 24 are 1, 2, 3, 4, 6, 8, 12 and 24. The triples are (1, 2, 3), (1, 2, 9), (1, 3, 4).",
				"a(30) = 7: the triples are (1, 2, 3), (1, 2, 5), (1, 3, 5), (2, 3, 5), (1, 3, 10), (1, 5, 6), (1, 2, 15)."
			],
			"mathematica": [
				"Table[Length[Select[Subsets[Divisors[n],{3}],CoprimeQ@@#\u0026]],{n,100}] (* _Gus Wiseman_, Apr 28 2021 *)"
			],
			"program": [
				"(PARI) A066620(n) = (numdiv(n^3)-3*numdiv(n)+2)/6; \\\\ After Jovovic's formula. - _Antti Karttunen_, May 27 2017",
				"(Python)",
				"from sympy import divisor_count as d",
				"def a(n): return (d(n**3) - 3*d(n) + 2)/6 # _Indranil Ghosh_, May 27 2017"
			],
			"xref": [
				"Positions of zeros are A000961.",
				"Positions of ones are A006881.",
				"The version for subsets of {1..n} instead of divisors is A015617.",
				"The non-strict ordered version is A048785.",
				"The version for pairs of divisors is A063647.",
				"The non-strict version (3-multisets) is A100565.",
				"The version for partitions is A220377 (non-strict: A307719).",
				"A version for sets of divisors of any size is A225520.",
				"A000005 counts divisors.",
				"A001399(n-3) = A069905(n) = A211540(n+2) counts 3-part partitions.",
				"A007304 ranks 3-part strict partitions.",
				"A014311 ranks 3-part compositions.",
				"A014612 ranks 3-part partitions.",
				"A018892 counts unordered pairs of coprime divisors (ordered: A048691).",
				"A051026 counts pairwise indivisible subsets of {1..n}.",
				"A337461 counts 3-part pairwise coprime compositions.",
				"A338331 lists Heinz numbers of pairwise coprime partitions.",
				"Cf. A007360, A023022, A084422, A276187, A282935, A305713, A337563, A337605, A343652, A343655."
			],
			"keyword": "nonn",
			"offset": "1,12",
			"author": "K. B. Subramaniam (kb_subramaniambalu(AT)yahoo.com) and _Amarnath Murthy_, Dec 24 2001",
			"ext": [
				"More terms from _Vladeta Jovovic_, Apr 03 2003",
				"Name corrected by _Andrey Zabolotskiy_, Dec 09 2020",
				"Name corrected by _Gus Wiseman_, Apr 28 2021 (ordered version is 6*a(n))"
			],
			"references": 7,
			"revision": 32,
			"time": "2021-05-02T20:20:35-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}