{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A319513",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 319513,
			"data": "1,3,6,2,4,12,36,18,9,27,54,108,216,72,24,8,16,48,144,432,1296,648,324,162,81,243,486,972,1944,3888,7776,2592,864,288,96,32,64,192,576,1728,5184,15552,46656,23328,11664,5832,2916,1458,729,2187,4374,8748,17496",
			"name": "The boustrophedonic Rosenberg-Strong function maps N onto N X N where N = {0, 1, 2, ...} and n -\u003e factor(a(n)) = 2^x*3^y -\u003e (x, y).",
			"comment": [
				"If (x, y) and (x', y') are adjacent points on the trajectory of the map then for the boustrophedonic Rosenberg-Strong function max(|x - x'|, |y - y'|) is always 1 whereas for the Rosenberg-Strong function this quantity can become arbitrarily large. In this sense the boustrophedonic variant is continuous in contrast to the original Rosenberg-Strong function."
			],
			"reference": [
				"A. L. Rosenberg, H. R. Strong, Addressing arrays by shells, IBM Technical Disclosure Bulletin, vol 14(10), 1972, p. 3026-3028."
			],
			"link": [
				"Georg Cantor, \u003ca href=\"http://gdz.sub.uni-goettingen.de/dms/resolveppn/?PPN=GDZPPN002156806\"\u003eEin Beitrag zur Mannigfaltigkeitslehre\u003c/a\u003e, Journal für die reine und angewandte Mathematik 84 (1878), 242-258.",
				"Steven Pigeon, \u003ca href=\"https://hbfs.wordpress.com/2018/08/07/moeud-deux/\"\u003eMœud deux\u003c/a\u003e, 2018.",
				"A. L. Rosenberg, \u003ca href=\"https://doi.org/10.1145/321850.321861\"\u003eAllocating storage for extendible Arrays\u003c/a\u003e, J. ACM, vol 21(4), 1974, p. 652-670.",
				"M. P. Szudzik, \u003ca href=\"http://arxiv.org/abs/1706.04129\"\u003eThe Rosenberg-Strong Pairing Function\"\u003c/a\u003e, arXiv:1706.04129 [cs.DM], 2017."
			],
			"maple": [
				"A319513 := proc(n) local b, r, p, m;",
				"    b := floor(sqrt(n)); r := n - b^2;",
				"    p := `if`(r \u003c b, [b, r], [2*b-r, b]);",
				"    m := `if`(p[1] \u003e p[2], p[1], p[2]);",
				"    `if`(irem(m,2) = 0, 2^p[1]*3^p[2], 2^p[2]*3^p[1]) end:",
				"seq(A319513(n), n=0..52);"
			],
			"mathematica": [
				"a[n_] := Module[{b, r, p1, p2, m}, b = Floor[Sqrt[n]]; r = n-b^2; {p1, p2} = If[r\u003cb, {b, r}, {2b-r, b}]; m = If[p1\u003ep2, p1, p2]; If[EvenQ[m], 2^p1 3^p2, 2^p2 3^p1]]; Table[a[n], {n, 0, 52}] (* _Jean-François Alcover_, Feb 14 2019, from Maple *)"
			],
			"program": [
				"(Julia)",
				"function bRS(n)",
				"    m = x = isqrt(n)",
				"    y = n - x^2",
				"    x \u003c= y \u0026\u0026 ((x, y) = (2x - y, x))",
				"    isodd(m) ? (y, x) : (x, y)",
				"end",
				"A319513(n) = ((x, y) = bRS(n); 2^x * 3^y)",
				"[A319513(n) for n in 0:52] |\u003e println"
			],
			"xref": [
				"See A319514 for a non-decoded variant with interleaved x and y coordinates.",
				"Cf. A038566/A038567, A157807/A157813."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Peter Luschny_, Sep 21 2018",
			"references": 2,
			"revision": 29,
			"time": "2019-02-14T05:56:47-05:00",
			"created": "2018-09-22T05:33:46-04:00"
		}
	]
}