{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A180363",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 180363,
			"data": "3,4,11,29,199,521,3571,9349,64079,1149851,3010349,54018521,370248451,969323029,6643838879,119218851371,2139295485799,5600748293801,100501350283429,688846502588399,1803423556807921,32361122672259149,221806434537978679",
			"name": "a(n) = Lucas(prime(n)).",
			"comment": [
				"This is to A030426, Fibonacci(prime(n)), as A000032 (Lucas numbers beginning at 2) is to A000045."
			],
			"link": [
				"\u003ca href=\"/A180363/b180363.txt\"\u003eTable of n, a(n) for n = 1..650\u003c/a\u003e",
				"A. Aksenov, \u003ca href=\"http://arxiv.org/abs/1108.5352\"\u003eThe Newman phenomenon and Lucas sequence\u003c/a\u003e, arXiv:1108.5352 [math.NT], 2011. [Gives factorizations of first 88 terms]",
				"Paula Burkhardt et al., \u003ca href=\"http://arxiv.org/abs/1505.00018\"\u003eVisual properties of generalized Kloosterman sums\u003c/a\u003e, arXiv:1505.00018 [math.NT], 2015."
			],
			"formula": [
				"a(n) = A000032(A000040(n)) = Lucas(prime(n)).",
				"a(n) = A032170(A000040(n)) / A064723(n-1) - 1 for n\u003e1. - _Flávio V. Fernandes_, Dec 30 2021"
			],
			"example": [
				"a(1) = 3 because the 1st prime is 2, and the 2nd Lucas number is A000032(2) = 3.",
				"a(2) = 4 because the 2nd prime is 3, and the 3rd Lucas number is A000032(3) = 4.",
				"a(3) = 11 because the 3rd prime is 5, and the 5th Lucas number is A000032(5) = 11."
			],
			"maple": [
				"A180363 := proc(n) A000032(ithprime(n)) ; end proc: seq(A180363(n),n=1..30) ; # _R. J. Mathar_, Sep 01 2010",
				"# second Maple program:",
				"a:= n-\u003e (\u003c\u003c1|1\u003e, \u003c1|0\u003e\u003e^ithprime(n). \u003c\u003c2, -1\u003e\u003e)[1, 1]:",
				"seq(a(n), n=1..50);  # _Alois P. Heinz_, Jan 03 2022"
			],
			"mathematica": [
				"LucasL[Prime[Range[30]]] (* _Vincenzo Librandi_, Dec 01 2015 *)"
			],
			"program": [
				"(MAGMA) [Lucas(NthPrime(n)): n in [1..30]]; // _Vincenzo Librandi_, Dec 01 2015",
				"(Python)",
				"from sympy import lucas, prime",
				"def a(n): return lucas(prime(n))",
				"print([a(n) for n in range(1, 24)]) # _Michael S. Branicky_, Dec 30 2021"
			],
			"xref": [
				"Cf. A000032, A000040, A000045, A030426."
			],
			"keyword": "easy,nonn,changed",
			"offset": "1,1",
			"author": "_Jonathan Vos Post_, Aug 31 2010",
			"ext": [
				"Entries checked by _R. J. Mathar_, Sep 01 2010",
				"Edited by _N. J. A. Sloane_, Nov 28 2011"
			],
			"references": 8,
			"revision": 48,
			"time": "2022-01-03T21:27:41-05:00",
			"created": "2010-09-12T03:00:00-04:00"
		}
	]
}