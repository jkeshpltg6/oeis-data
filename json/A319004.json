{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A319004",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 319004,
			"data": "1,1,1,2,1,2,1,4,2,2,1,4,1,2,2,8,1,5,1,4,2,2,1,8,2,2,4,4,1,5,1,16,2,2,2,11,1,2,2,8,1,5,1,4,4,2,1,16,2,5,2,4,1,12,2,8,2,2,1,11,1,2,4,32,2,5,1,4,2,5,1,23,1,2,4,4,2,5,1,16,8,2,1,11,2,2,2,8,1,12,2,4,2,2,2,32,1,5,4,11,1,5,1,8,5",
			"name": "Number of ordered factorizations of n where the sequence of LCMs of the prime indices (A290103) of each factor is weakly increasing.",
			"comment": [
				"Also the number of ordered multiset partitions of the multiset of prime indices of n where the sequence of LCMs of the parts is weakly increasing. If we form a multiorder by treating integer partitions (a,...,z) as multiarrows LCM(a,...,z) \u003c= {z,...,a}, then a(n) is the number of triangles whose composite ground is the integer partition with Heinz number n."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A319004/b319004.txt\"\u003eTable of n, a(n) for n = 1..65537\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#prime_indices\"\u003eIndex entries for sequences computed from indices in prime factorization\u003c/a\u003e",
				"\u003ca href=\"/index/He#Heinz\"\u003eIndex entries for sequences related to Heinz numbers\u003c/a\u003e"
			],
			"formula": [
				"A001055(n) \u003c= a(n) \u003c= A074206(n). - _Antti Karttunen_, Sep 23 2018"
			],
			"example": [
				"The a(60) = 11 ordered factorizations:",
				"  (2*2*3*5),",
				"  (2*2*15), (2*3*10), (2*6*5), (4*3*5),",
				"  (2*30), (3*20), (4*15), (12*5), (6*10),",
				"  (60).",
				"The a(60) = 11 ordered multiset partitions:",
				"     {{1,1,2,3}}",
				"    {{1},{1,2,3}}",
				"    {{2},{1,1,3}}",
				"    {{1,1,2},{3}}",
				"    {{1,1},{2,3}}",
				"    {{1,2},{1,3}}",
				"   {{1},{1},{2,3}}",
				"   {{1},{2},{1,3}}",
				"   {{1},{1,2},{3}}",
				"   {{1,1},{2},{3}}",
				"  {{1},{1},{2},{3}}"
			],
			"mathematica": [
				"facs[n_]:=If[n\u003c=1,{{}},Join@@Table[(Prepend[#1,d]\u0026)/@Select[facs[n/d],Min@@#1\u003e=d\u0026],{d,Rest[Divisors[n]]}]];",
				"lix[n_]:=LCM@@PrimePi/@If[n==1,{},FactorInteger[n]][[All,1]];",
				"Table[Length[Select[Join@@Permutations/@facs[n],OrderedQ[lix/@#]\u0026]],{n,100}]"
			],
			"program": [
				"(PARI)",
				"is_weakly_increasing(v) = { for(i=2,#v,if(v[i]\u003cv[i-1], return(0))); (1); };",
				"A290103(n) = lcm(apply(p-\u003eprimepi(p),factor(n)[,1]));",
				"A319004aux(n, facs) = if(1==n, is_weakly_increasing(apply(f -\u003e A290103(f),Vec(facs))), my(s=0, newfacs); fordiv(n, d, if((d\u003e1), newfacs = List(facs); listput(newfacs,d); s += A319004aux(n/d, newfacs))); (s));",
				"A319004(n) = if((1==n)||isprime(n),1,A319004aux(n, List([]))); \\\\ _Antti Karttunen_, Sep 23 2018"
			],
			"xref": [
				"Cf. A001055, A056239, A063834, A074206, A290103, A296150, A316223, A317545, A317546, A318559, A319002, A319003."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Gus Wiseman_, Sep 07 2018",
			"ext": [
				"More terms from _Antti Karttunen_, Sep 23 2018"
			],
			"references": 4,
			"revision": 13,
			"time": "2018-09-23T21:32:14-04:00",
			"created": "2018-09-11T17:01:39-04:00"
		}
	]
}