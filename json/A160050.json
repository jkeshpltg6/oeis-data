{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A160050",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 160050,
			"data": "0,1,5,9,7,10,27,35,22,27,65,77,45,52,119,135,76,85,189,209,115,126,275,299,162,175,377,405,217,232,495,527,280,297,629,665,351,370,779,819,430,451,945,989,517,540,1127,1175,612,637,1325,1377,715,742,1539",
			"name": "Numerator of the Harary number for the star graph s_n.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A160050/b160050.txt\"\u003eTable of n, a(n) for n = 1..5000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HararyIndex.html\"\u003eHarary Index\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_09\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-6,10,-12,12,-10,6,-3,1)."
			],
			"formula": [
				"Numerator of (1/4)*(n+2)*(n-1). - _Joerg Arndt_, Jan 04 2011",
				"It appears that a(n + 1) = A060819(n-1) * A060819(n + 2). - _Paul Curtz_, Dec 23 2010 [Corrected by _Joerg Arndt_, Jan 04 2011]",
				"G.f.:  x^2*(-1-2*x-5*x^4+3*x^5-2*x^6+x^7) / ( (x-1)^3*(x^2+1)^3 ). - _R. J. Mathar_, Jan 04 2011",
				"a(1+4*n) = (A000217(n+1)-1)/2, a(2+4*n) = (A000217(n+2)-1)/2, a(3+4*n) = A000217(n+3)-1, a(4+4*n) = A000217(n+4)-1. - _Paul Curtz_, Dec 23 2010.",
				"a(n) = 3*a(n-4) - 3*a(n-8) + a(n-12). This is not the shortest recurrence. -_Paul Curtz_, Mar 27 2011",
				"a(1+3*n) = numerator of 9*n*(n+1)/4 = 9*A064038(1+n). - _Paul Curtz_, Apr 06 2011",
				"a(n) = n*(n+3)*(3-i^(n*(n-1)))/8, where i=sqrt(-1).  - _Bruno Berselli_, Apr 07 2011"
			],
			"example": [
				"0, 1, 5/2, 9/2, 7, 10, 27/2, 35/2, 22, 27, ..."
			],
			"mathematica": [
				"f[n_] := n/GCD[n, 4]; Array[f[#] f[# + 3] \u0026, 58]",
				"Rest[CoefficientList[Series[x^2*(-1 - 2*x - 5*x^4 + 3*x^5 - 2*x^6 + x^7)/((x - 1)^3*(x^2 + 1)^3), {x, 0, 50}], x]] (* _G. C. Greubel_, Sep 21 2018 *)"
			],
			"program": [
				"(PARI) s=vector(40,n,1/4*(n+2)*(n-1)) /* fractions */",
				"vector(#s,n,numerator(s[n])) /* this sequence */ \\\\ _Joerg Arndt_, Jan 04 2011",
				"(PARI) x='x+O('x^50); concat([0], Vec(x^2*(-1 - 2*x - 5*x^4 + 3*x^5 - 2*x^6 + x^7)/((x - 1)^3*(x^2 + 1)^3))) \\\\ _G. C. Greubel_, Sep 21 2018",
				"(MAGMA) m:=50; R\u003cx\u003e:=PowerSeriesRing(Integers(), m); [0] cat Coefficients(R!(x^2*(-1 - 2*x - 5*x^4 + 3*x^5 - 2*x^6 + x^7)/((x - 1)^3*(x^2 + 1)^3))); // _G. C. Greubel_, Sep 21 2018"
			],
			"xref": [
				"Cf. A130658 (denominators), A033954 (quadrisection), A001107 (quadrisection), A181890 (quadrisection)."
			],
			"keyword": "nonn,easy,frac",
			"offset": "1,3",
			"author": "_Eric W. Weisstein_, Apr 30 2009",
			"ext": [
				"Edited by _N. J. A. Sloane_, Dec 23 2010"
			],
			"references": 12,
			"revision": 61,
			"time": "2020-04-29T18:06:06-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}