{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A219208",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 219208,
			"data": "1,1,1,1,1,1,1,1,1,1,1,1,3,1,1,1,1,1,3,1,2,1,1,1,6,1,1,1,2,1,4,1,1,1,1,1,7,1,1,1,4,1,3,1,1,1,1,1,10,1,1,1,1,1,4,1,3,1,1,1,26,1,1,1,1,1,3,1,1,1,1,1,26,1,1,1,1,1,2,1,6,1,1,1,23,1",
			"name": "Number of distinct products of all parts of all partitions of n into distinct divisors of n.",
			"comment": [
				"a(p) = 1 for p in A000040 (prime numbers).",
				"a(n) = 1 for n in A006037 (weird numbers).",
				"a(n) = 1 for n in A005100 (deficient numbers).",
				"a(n) = 1 for n in A125493 (composite deficient numbers).",
				"a(n) \u003c= 2 for n in A000396 (perfect numbers).",
				"a(n) \u003e= 2 for n \u003e 6 and n in A005835 (semiperfect numbers)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A219208/b219208.txt\"\u003eTable of n, a(n) for n = 0..5000\u003c/a\u003e"
			],
			"example": [
				"a(0) = 1: the empty product.",
				"a(p) = 1 for any prime p: [p]-\u003e p.",
				"a(6) = 1: {[1,2,3], [6]}-\u003e 6.",
				"a(12) = 3, because all 3 partitions of 12 into distinct divisors of 12 have different products: [1,2,3,6]-\u003e 36, [2,4,6]-\u003e 48, [12]-\u003e 12. a(18) = 3: [1,2,6,9]-\u003e 108, [3,6,9]-\u003e 162, [18]-\u003e 18.",
				"a(20) = 2: [1,4,5,10]-\u003e 200, [20]-\u003e 20.",
				"a(28) = 2: [1,2,4,7,14]-\u003e 784, [28]-\u003e 28.",
				"a(36) = 7: [2,3,4,6,9,12]-\u003e 15552, [2,3,4,9,18]-\u003e 3888, [1,2,6,9,18]-\u003e 1944, [3,6,9,18]-\u003e 2916, {[1,2,3,12,18], [6,12,18]}-\u003e 1296, [2,4,12,18]-\u003e 1728, [36]-\u003e 36.",
				"a(84) = 23: 84, 16464, 28224, 49392, 65856, 74088, 84672, 86436, 98784, 127008, 148176, 190512, 254016, 444528, 592704, 889056, 1016064, 1185408, 1382976, 1778112, 2370816, 4148928, 7112448."
			],
			"maple": [
				"a:= proc(n) local b, l;",
				"      l:= sort([numtheory[divisors](n)[]]);",
				"      b:= proc(n, i) option remember; `if`(n=0, {1}, `if`(i\u003c1, {},",
				"            {b(n, i-1)[], `if`(l[i]\u003en, {}, map(x-\u003e x*l[i],",
				"            b(n-l[i], i-1)))[]}))",
				"          end; forget(b);",
				"      nops(b(n, nops(l)))",
				"    end:",
				"seq(a(n), n=0..120);"
			],
			"mathematica": [
				"a[n_] := a[n] = Module[{b, l}, l = Divisors[n]; b[m_, i_] := b[m, i] = If[m == 0, {1}, If[i\u003c1, {}, Union[b[m, i-1], If[l[[i]]\u003em, {}, (#*l[[i]]\u0026) /@ b[m-l[[i]], i-1]]]]]; Length[b[n, Length[l]]]]; Table[a[n], {n, 0, 120}] (* _Jean-François Alcover_, Feb 16 2017, translated from Maple *)"
			],
			"xref": [
				"Maximal products are in A219209.",
				"Cf. A033630, A092976."
			],
			"keyword": "nonn",
			"offset": "0,13",
			"author": "_Alois P. Heinz_, Nov 14 2012",
			"references": 2,
			"revision": 36,
			"time": "2017-02-16T11:01:00-05:00",
			"created": "2012-11-14T21:47:01-05:00"
		}
	]
}