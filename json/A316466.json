{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A316466",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 316466,
			"data": "0,8,44,108,200,320,468,644,848,1080,1340,1628,1944,2288,2660,3060,3488,3944,4428,4940,5480,6048,6644,7268,7920,8600,9308,10044,10808,11600,12420,13268,14144,15048,15980,16940,17928,18944,19988,21060,22160,23288,24444,25628,26840",
			"name": "a(n) = 2*n*(7*n - 3).",
			"comment": [
				"This is the case k = 9 of Sum_{i = 2..k} P(i,n) = (k - 1)*n*((k - 2)*n - (k - 6))/4, where P(k,n) = n*((k - 2)*n - (k - 4))/2 (see Crossrefs for similar sequences and \"Square array in A139600\" in Links section).",
				"14*x + 9 is a square for x = a(n) or x = a(-n)."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A316466/b316466.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Bruno Berselli, \u003ca href=\"/A316466/a316466.jpg\"\u003eSquare array in A139600\u003c/a\u003e.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1)."
			],
			"formula": [
				"O.g.f.: 4*x*(2 + 5*x)/(1 - x)^3.",
				"E.g.f.: 2*x*(4 + 7*x)*exp(x).",
				"a(n) = 3*a(n-1) - 3*a(n-2) + a(n-3).",
				"a(n) = 4*A218471(n)."
			],
			"mathematica": [
				"Table[2 n (7 n - 3), {n, 0, 50}]",
				"LinearRecurrence[{3,-3,1},{0,8,44},50] (* _Harvey P. Dale_, Jan 24 2021 *)"
			],
			"program": [
				"(PARI) vector(50, n, n--; 2*n*(7*n-3))",
				"(PARI) concat(0, Vec(4*x*(2 + 5*x)/(1 - x)^3 + O(x^40))) \\\\ _Colin Barker_, Jul 05 2018",
				"(Sage) [2*n*(7*n-3) for n in (0..50)]",
				"(Maxima) makelist(2*n*(7*n-3), n, 0, 50);",
				"(GAP) List([0..50], n -\u003e 2*n*(7*n-3));",
				"(MAGMA) [2*n*(7*n-3): n in [0..50]];",
				"(Python) [2*n*(7*n-3) for n in range(50)]",
				"(Julia) [2*n*(7*n-3) for n in 0:50] |\u003e println"
			],
			"xref": [
				"Cf. A139600, A218471.",
				"Similar sequences (see the first comment): A000096 (k = 3), A045943 (k = 4), A049451 (k = 5), A033429 (k = 6), A167469 (k = 7), A152744 (k = 8), this sequence (k = 9), A152994 (k = 10)."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Bruno Berselli_, Jul 04 2018",
			"references": 9,
			"revision": 27,
			"time": "2021-01-24T17:53:44-05:00",
			"created": "2018-07-05T06:11:26-04:00"
		}
	]
}