{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A117966",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 117966,
			"data": "0,1,-1,3,4,2,-3,-2,-4,9,10,8,12,13,11,6,7,5,-9,-8,-10,-6,-5,-7,-12,-11,-13,27,28,26,30,31,29,24,25,23,36,37,35,39,40,38,33,34,32,18,19,17,21,22,20,15,16,14,-27,-26,-28,-24,-23,-25,-30,-29,-31,-18,-17,-19,-15,-14,-16,-21,-20,-22,-36",
			"name": "Balanced ternary enumeration (based on balanced ternary representation) of integers; write n in ternary and then replace 2's with (-1)'s.",
			"comment": [
				"As the graph demonstrates, there are large discontinuities in the sequence between terms 3^i-1 and 3^i, and between terms 2*3^i-1 and 2*3^i. - _N. J. A. Sloane_, Jul 03 2016"
			],
			"reference": [
				"D. E. Knuth, The Art of Computer Programming. Addison-Wesley, Reading, MA, Vol. 2, pp. 173-175; 2nd. ed. pp. 190-193."
			],
			"link": [
				"Gheorghe Coserea, \u003ca href=\"/A117966/b117966.txt\"\u003eTable of n, a(n) for n = 0..59048\u003c/a\u003e (first 729 terms from A. Karttunen)",
				"Ken Levasseur, \u003ca href=\"http://discretemath.org/ternary_number_system.html\"\u003eThe Balanced Ternary Number System\u003c/a\u003e",
				"Tilman Piesk, \u003ca href=\"http://commons.wikimedia.org/wiki/File:Balanced_ternary_A117966.svg\"\u003eFirst 27 numbers with their ternary representation\u003c/a\u003e",
				"N. J. A. Sloane and Brady Haran, \u003ca href=\"https://www.youtube.com/watch?v=o8c4uYnnNnc\"\u003eAmazing Graphs II (including Star Wars)\u003c/a\u003e, Numberphile video (2019)",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Balanced_ternary\"\u003eBalanced ternary\u003c/a\u003e"
			],
			"formula": [
				"a(0) = 0, a(3n) = 3a(n), a(3n+1) = 3a(n)+1, a(3n+2) = 3a(n)-1.",
				"G.f. satisfies A(x) = 3*A(x^3)*(1+x+x^2) + x/(1+x+x^2). - corrected by _Robert Israel_, Nov 17 2015",
				"A004488(n) = a(n)^{-1}(-a(n)). I.e., if a(n) \u003c= 0, A004488(n) = A117967(-a(n)) and if a(n) \u003e 0, A004488(n) = A117968(a(n)).",
				"a(n) = n - 3 * A005836(A289814(n) + 1). - _Andrey Zabolotskiy_, Nov 11 2019"
			],
			"example": [
				"7 in base 3 is 21; changing the 2 to a (-1) gives (-1)*3+1 = -2, so a(7) = -2. I.e., the number of -2 according to the balanced ternary enumeration is 7, which can be obtained by replacing every -1 by 2 in the balanced ternary representation (or expansion) of -2, which is -1,1."
			],
			"maple": [
				"f:= proc(n) local L,i;",
				"   L:= subs(2=-1,convert(n,base,3));",
				"   add(L[i]*3^(i-1),i=1..nops(L))",
				"end proc:",
				"map(f, [$0..100]);",
				"# alternate:",
				"N:= 100: # to get a(0) to a(N)",
				"g:= 0:",
				"for n from 1 to ceil(log[3](N+1)) do",
				"g:= convert(series(3*subs(x=x^3,g)*(1+x+x^2)+x/(1+x+x^2),x,3^n+1),polynom);",
				"od:",
				"seq(coeff(g,x,j),j=0..N); # _Robert Israel_, Nov 17 2015",
				"# third Maple program:",
				"a:= proc(n) option remember; `if`(n=0, 0,",
				"      3*a(iquo(n, 3, 'r'))+`if`(r=2, -1, r))",
				"    end:",
				"seq(a(n), n=0..3^4-1);  # _Alois P. Heinz_, Aug 14 2019"
			],
			"mathematica": [
				"Map[FromDigits[#, 3] \u0026, IntegerDigits[#, 3] /. 2 -\u003e -1 \u0026 /@ Range@ 80] (* _Michael De Vlieger_, Nov 17 2015 *)"
			],
			"program": [
				"(MIT/GNU Scheme:) (define (A117966 n) (let loop ((z 0) (i 0) (n n)) (if (zero? n) z (loop (+ z (* (expt 3 i) (if (= 2 (modulo n 3)) -1 (modulo n 3)))) (1+ i) (floor-\u003eexact (/ n 3)))))) -- _Antti Karttunen_, May 19 2008",
				"(PARI) a(n) = subst(Pol(apply(x-\u003eif(x == 2, -1, x), digits(n,3)), 'x), 'x, 3)",
				"vector(73, i, a(i-1))  \\\\ _Gheorghe Coserea_, Nov 17 2015",
				"(Python)",
				"def a(n):",
				"    if n==0: return 0",
				"    if n%3==0: return 3*a(n//3)",
				"    elif n%3==1: return 3*a((n - 1)//3) + 1",
				"    else: return 3*a((n - 2)//3) - 1",
				"print([a(n) for n in range(101)]) # _Indranil Ghosh_, Jun 06 2017"
			],
			"xref": [
				"Cf. A117967, A117968, A001057, A004488, A134028, A274107, A059095, A005836, A289814, A244042.",
				"Column k=1 of A319047."
			],
			"keyword": "base,sign,look",
			"offset": "0,4",
			"author": "_Franklin T. Adams-Watters_, Apr 05 2006",
			"ext": [
				"Name corrected by _Andrey Zabolotskiy_, Nov 10 2019"
			],
			"references": 39,
			"revision": 87,
			"time": "2021-03-10T07:23:14-05:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}