{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A334301",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 334301,
			"data": "1,2,1,1,3,2,1,1,1,1,4,2,2,3,1,2,1,1,1,1,1,1,5,3,2,4,1,2,2,1,3,1,1,2,1,1,1,1,1,1,1,1,6,3,3,4,2,5,1,2,2,2,3,2,1,4,1,1,2,2,1,1,3,1,1,1,2,1,1,1,1,1,1,1,1,1,1,7,4,3,5,2,6,1,3,2,2",
			"name": "Irregular triangle read by rows where row k is the k-th integer partition, if partitions are sorted first by sum, then by length, and finally lexicographically.",
			"comment": [
				"This is the Abramowitz-Stegun ordering of integer partitions when they are read in the usual (weakly decreasing) order. The case of reversed (weakly increasing) partitions is A036036."
			],
			"link": [
				"Wikiversity, \u003ca href=\"https://en.wikiversity.org/wiki/Lexicographic_and_colexicographic_order\"\u003eLexicographic and colexicographic order\u003c/a\u003e"
			],
			"example": [
				"The sequence of all partitions in Abramowitz-Stegun order begins:",
				"  ()      (41)     (21111)   (31111)    (3221)",
				"  (1)     (221)    (111111)  (211111)   (3311)",
				"  (2)     (311)    (7)       (1111111)  (4211)",
				"  (11)    (2111)   (43)      (8)        (5111)",
				"  (3)     (11111)  (52)      (44)       (22211)",
				"  (21)    (6)      (61)      (53)       (32111)",
				"  (111)   (33)     (322)     (62)       (41111)",
				"  (4)     (42)     (331)     (71)       (221111)",
				"  (22)    (51)     (421)     (332)      (311111)",
				"  (31)    (222)    (511)     (422)      (2111111)",
				"  (211)   (321)    (2221)    (431)      (11111111)",
				"  (1111)  (411)    (3211)    (521)      (9)",
				"  (5)     (2211)   (4111)    (611)      (54)",
				"  (32)    (3111)   (22111)   (2222)     (63)",
				"This sequence can also be interpreted as the following triangle, whose n-th row is itself a finite triangle with A000041(n) rows.",
				"                            0",
				"                           (1)",
				"                        (2) (1,1)",
				"                    (3) (2,1) (1,1,1)",
				"            (4) (2,2) (3,1) (2,1,1) (1,1,1,1)",
				"  (5) (3,2) (4,1) (2,2,1) (3,1,1) (2,1,1,1) (1,1,1,1,1)",
				"Showing partitions as their Heinz numbers (see A334433) gives:",
				"   1",
				"   2",
				"   3   4",
				"   5   6   8",
				"   7   9  10  12  16",
				"  11  15  14  18  20  24  32",
				"  13  25  21  22  27  30  28  36  40  48  64",
				"  17  35  33  26  45  50  42  44  54  60  56  72  80  96 128"
			],
			"mathematica": [
				"Join@@Table[Sort[IntegerPartitions[n]],{n,0,8}]"
			],
			"xref": [
				"Lexicographically ordered reversed partitions are A026791.",
				"The version for reversed partitions (sum/length/lex) is A036036.",
				"Row lengths are A036043.",
				"Reverse-lexicographically ordered partitions are A080577.",
				"The version for compositions is A124734.",
				"Lexicographically ordered partitions are A193073.",
				"Sorting by Heinz number gives A296150, or A112798 for reversed partitions.",
				"Sorting first by sum, then by Heinz number gives A215366.",
				"Reversed partitions under the dual ordering (sum/length/revlex) are A334302.",
				"Taking Heinz numbers gives A334433.",
				"The reverse-lexicographic version is A334439 (not A036037).",
				"Cf. A000041, A048793, A066099, A162247, A211992, A228100, A228351, A228531."
			],
			"keyword": "nonn,tabf",
			"offset": "0,2",
			"author": "_Gus Wiseman_, Apr 29 2020",
			"references": 41,
			"revision": 13,
			"time": "2020-05-31T07:09:36-04:00",
			"created": "2020-05-01T21:26:56-04:00"
		}
	]
}