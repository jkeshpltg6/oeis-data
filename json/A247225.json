{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A247225",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 247225,
			"data": "1,2,3,5,4,9,25,8,21,55,16,7,11,6,35,121,12,49,143,10,63,13,20,27,91,22,15,119,26,33,17,14,39,85,28,57,65,32,19,45,34,133,69,40,77,23,18,175,253,24,95,161,36,125,203,38,75,29,44,51,145,46,81,155,52",
			"name": "a(n) = n if n \u003c= 3, a(4)=5, otherwise the smallest number not occurring earlier having at least one common factor with a(n-3), but none with a(n-1)*a(n-2).",
			"comment": [
				"Conjecturally the sequence is a permutation of the positive integers. However, to prove this we need more subtle arguments than were used to prove the corresponding property for A098550. - _Vladimir Shevelev_, Jan 14 2015",
				"For n \u003c= 2000, a(3n-1) is even and both a(3n) and a(3n-2) are odd numbers. I conjecture that this is true for all positive integers n. This conjecture is true iff for all positive integers n, a(3n-1) is even. - _Farideh Firoozbakht_, Jan 14 2015",
				"From _Vladimir Shevelev_, Jan 19 2015:  (Start)",
				"A generalization of A098550 and A247225.",
				"Let p_n=prime(n). Define the following sequence",
				"a(1)=1, a(2)=p_1,...,a(k+2)=p_(k+1), otherwise the smallest number not occurring earlier having at least one common factor with a(n-(k+1)), but none with a(n-1)*a(n-2)*...*a(n-k).",
				"The sequence begins",
				"1, p_1, p_2, ..., p_(k+1), p_1^2, p_2^2, ..., p_(k+1)^2, p_1^3, ... (*)",
				"[ p_1^3 is followed by p_2*p_(k+2), k\u003c=2,",
				"p_2^3, k\u003e=3, etc.]",
				"In particular, if k=1, it is A098550, if k=2, it is A247225.",
				"Conjecturally for every k\u003e=2, as in the case k=1, the sequence (*) is a permutation of the positive integers. For k\u003e=3, at first glance, already the appearance of the number 6 seems problematic. However, at the author's request, _Peter J. C. Moses_ found that the positions of 6 are 83, 157, 1190, 206, ... in cases k=3,4,5,6,... respectively (A254003).",
				"Note also that for every k\u003e=2, every even term is followed by k odd terms. This is explained by the minimal growth of even numbers (2n) relatively with one of the numbers with the smallest prime divisor p\u003e=3 (asymptotically 6n, 15n, 105n/4, 385n/8, ... for p = 3,5,7,11,... respectively (cf. A084967 - A084970)).",
				"(End)"
			],
			"link": [
				"Peter J. C. Moses, \u003ca href=\"/A247225/b247225.txt\"\u003eTable of n, a(n) for n = 1..2000\u003c/a\u003e",
				"David L. Applegate, Hans Havermann, Bob Selcoe, Vladimir Shevelev, N. J. A. Sloane, and Reinhard Zumkeller, \u003ca href=\"http://arxiv.org/abs/1501.01669\"\u003eThe Yellowstone Permutation\u003c/a\u003e, arXiv preprint arXiv:1501.01669, 2015."
			],
			"mathematica": [
				"a[n_ /; n \u003c= 3] := n; a[4]=5; a[n_] := a[n] = For[aa = Table[a[j], {j, 1, n-1}]; k=4, True, k++, If[FreeQ[aa, k] \u0026\u0026 !CoprimeQ[k, a[n-3]] \u0026\u0026 CoprimeQ[k, a[n-1]*a[n-2]], Return[k]]]; Table[ a[n], {n, 1, 65}] (* _Jean-François Alcover_, Jan 12 2015 *)"
			],
			"xref": [
				"Cf. A098550, A247942, A249167, A251604, A254003."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Vladimir Shevelev_, Jan 11 2015",
			"ext": [
				"More terms from _Peter J. C. Moses_, Jan 12 2015"
			],
			"references": 6,
			"revision": 90,
			"time": "2021-07-20T23:48:03-04:00",
			"created": "2015-01-15T10:12:12-05:00"
		}
	]
}