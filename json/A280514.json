{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A280514",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 280514,
			"data": "1,2,1,3,2,1,5,4,3,2,1,8,7,6,5,4,3,2,1,13,12,11,10,9,8,7,6,5,4,3,2,1,21,20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,34,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,15,14",
			"name": "Index sequence of the reverse block-fractal sequence A003849.",
			"comment": [
				"The sequence is the concatenation of blocks, the n-th of which, for n \u003e=0, consists of the integers from F(n+1) down to F(2) = 1, where F = A000045, the Fibonacci numbers. See A280511 for the definition of reverse block-fractal sequence. The index sequence (a(n)) of a reverse block-fractal sequence (s(n)) is defined (at A280513) by a(n) = least k \u003e 0 such that (s(k), s(k+1), ..., s(k+n)) = (s(n), s(n-1), ..., s(0)).",
				"Apparently (up to offset) a duplicate of A246105. - _R. J. Mathar_, Jan 10 2017",
				"Let W be the Fibonacci word A003849. Then a(n) is the least k such that the reversal of the first n-block in W occurs in W beginning at the k-th term. Since (a(n)) is unbounded, the reversal of every block in W occurs infinitely many times in W. - _Clark Kimberling_, Dec 19 2020"
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A280514/b280514.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e"
			],
			"example": [
				"A003849 = (0,1,0,0,1,0,1,0,0,1,0,0,1,...) = (s(1), s(2), ... ).",
				"(init. block #1) = (1); reversal (0) first occurs at s(1), so a(1) = 1;",
				"(init. block #2) = (0,1); rev. (1,0) first occurs at s(2), so a(2) = 2;",
				"(init. block #3) = (0,1,0); rev. (0,1,0) first occurs at s(1), so a(3) = 1;",
				"(init. block #4) = (0,1,0,0); rev. (0,0,1,0) first occurs at s(3), so a(4) = 3."
			],
			"mathematica": [
				"r = GoldenRatio; t = Table[Floor[(n + 2) #] - Floor[(n + 1) #], {n, 0, 220}] \u0026[",
				"2 - GoldenRatio]  (* A003849 *)",
				"u = StringJoin[Map[ToString, t]]",
				"breverse[seq_] := Flatten[Last[Reap[NestWhile[# + 1 \u0026, 1, (StringLength[",
				"str = StringTake[seq, Min[StringLength[seq], #]]] == # \u0026\u0026 ! (Sow[StringPosition[seq, StringReverse[str], 1][[1]][[1]]]) === {}) \u0026]]]];",
				"breverse[u]  (* _Peter J. C. Moses_, Jan 02 2017 *)"
			],
			"xref": [
				"Cf. A000045, A003849."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Jan 06 2017",
			"references": 2,
			"revision": 18,
			"time": "2020-12-21T07:24:31-05:00",
			"created": "2017-01-07T11:58:38-05:00"
		}
	]
}