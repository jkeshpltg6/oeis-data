{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006522",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6522,
			"id": "M3413",
			"data": "1,0,0,1,4,11,25,50,91,154,246,375,550,781,1079,1456,1925,2500,3196,4029,5016,6175,7525,9086,10879,12926,15250,17875,20826,24129,27811,31900,36425,41416,46904,52921,59500,66675,74481,82954,92131",
			"name": "4-dimensional analog of centered polygonal numbers. Also number of regions created by sides and diagonals of a convex n-gon in general position.",
			"comment": [
				"Let A be the Hessenberg matrix of order n, defined by: A[1,j]=A[i,i]:=1, A[i,i-1]=-1, and A[i,j]=0 otherwise. Then, for n\u003e=5, a(n)=coeff(charpoly(A,x),x^(n-4)). - _Milan Janjic_, Jan 24 2010",
				"From _Ant King_, Sep 14 2011: (Start)",
				"Consider the array formed by the polygonal numbers of increasing rank A139600",
				"0, 1, 3,  6, 10, 15, 21,  28,  36,  45, ...  A000217(n)",
				"0, 1, 4,  9, 16, 25, 36,  49,  64,  81, ...  A000290(n)",
				"0, 1, 5, 12, 22, 35, 51,  70,  92, 117, ...  A000326(n)",
				"0, 1, 6, 15, 28, 45, 66,  91, 120, 153, ...  A000384(n)",
				"0, 1, 7, 18, 34, 55, 81, 112, 148, 189, ...  A000566(n)",
				"0, 1, 8, 21, 40, 65, 96, 133, 176, 225, ...  A000567(n)",
				"...",
				"Then, for n\u003e=2, a(n) is the diagonal sum of this polygonal grid.",
				"(End)",
				"Binomial transform of (1, -1, 1, 0, 1, 0, 0, 0, ...). - _Gary W. Adamson_, Aug 26 2015"
			],
			"reference": [
				"L. Comtet, Advanced Combinatorics, Reidel, 1974, p. 74, Problem 8.",
				"R. Honsberger, Mathematical Gems, M.A.A., 1973, p. 102.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A006522/b006522.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"J. W. Freeman, \u003ca href=\"http://www.jstor.org/stable/2689875\"\u003eThe number of regions determined by a convex polygon\u003c/a\u003e, Math. Mag., 49 (1976), 23-25.",
				"Math Forum, \u003ca href=\"http://mathforum.org/library/drmath/view/55262.html\"\u003eRegions of a circle Cut by Chords to n points\u003c/a\u003e.",
				"V. Meally, \u003ca href=\"/A006516/a006516.pdf\"\u003eLetter to N. J. A. Sloane, May 1975\u003c/a\u003e",
				"M. Janjic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL13/Janjic/janjic33.html\"\u003eHessenberg Matrices and Integer Sequences \u003c/a\u003e, J. Int. Seq. 13 (2010) # 10.7.8.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PolygonDiagonal.html\"\u003ePolygon Diagonal.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (5,-10,10,-5,1)."
			],
			"formula": [
				"a(n) = binomial(n,4) + binomial(n-1,2) = A000332(n) + A000217(n-2).",
				"a(n) = binomial(n-1,2) + binomial(n-1,3) + binomial(n-1,4). - _Zerinvary Lajos_, Jul 23 2006",
				"a(n) = 5*a(n-1) - 10*a(n-2) + 10*a(n-3) - 5*a(n-4) + a(n-5); a(0)=1, a(1)=0, a(2)=0, a(3)=1, a(4)=4. - _Harvey P. Dale_, Jul 11 2011",
				"G.f.: -((x-1)*x*(x*(4*x-5)+5)+1)/(x-1)^5. - _Harvey P. Dale_, Jul 11 2011",
				"a(n) = (n^4 - 6*n^3 + 23*n^2 - 42*n + 24)/24. - _T. D. Noe_, Oct 16 2013",
				"For odd n, a(n) = A007678(n). - _R. J. Mathar_, Nov 22 2017",
				"a(n) = a(3-n) for all n in Z. - _Michael Somos_, Nov 23 2021"
			],
			"example": [
				"For a pentagon in general position, 11 regions are formed (Comtet, Fig. 20, p. 74)."
			],
			"maple": [
				"A006522 := n-\u003e(1/24)*(n-1)*(n-2)*(n^2-3*n+12):",
				"seq(A006522(n), n=0..40);",
				"A006522:=-(1-z+z**2)/(z-1)**5; # _Simon Plouffe_ in his 1992 dissertation; gives sequence except for three leading terms"
			],
			"mathematica": [
				"a=2;b=3;s=4;lst={1,0,0,1,s};Do[a+=n;b+=a;s+=b;AppendTo[lst,s],{n,2,6!,1}];lst (* _Vladimir Joseph Stephan Orlovsky_, May 24 2009 *)",
				"Table[Binomial[n,4]+Binomial[n-1,2],{n,0,40}] (* or *) LinearRecurrence[ {5,-10,10,-5,1},{1,0,0,1,4},40] (* _Harvey P. Dale_, Jul 11 2011 *)",
				"CoefficientList[Series[-(((x - 1) x (x (4 x - 5) + 5) + 1) / (x - 1)^5), {x, 0, 50}], x] (* _Vincenzo Librandi_, Jun 09 2013 *)",
				"a[n_] := If[n==0, 1, Sum[PolygonalNumber[n-k+1, k], {k, 0, n-2}]];",
				"a /@ Range[0, 40] (* _Jean-François Alcover_, Jan 21 2020 *)"
			],
			"program": [
				"(MAGMA) [Binomial(n, 4)+Binomial(n-1, 2): n in [0..40]]; // _Vincenzo Librandi_, Jun 09 2013",
				"(PARI) a(n)=1/24*n^4 - 1/4*n^3 + 23/24*n^2 - 7/4*n + 1 \\\\ _Charles R Greathouse IV_, Feb 09 2017"
			],
			"xref": [
				"Partial sums of A004006.",
				"Cf. A000332, A000217."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_",
			"references": 19,
			"revision": 104,
			"time": "2021-11-23T15:54:11-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}