{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A020989",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 20989,
			"data": "1,6,26,106,426,1706,6826,27306,109226,436906,1747626,6990506,27962026,111848106,447392426,1789569706,7158278826,28633115306,114532461226,458129844906,1832519379626,7330077518506",
			"name": "a(n) = (5*4^n - 2)/3.",
			"comment": [
				"Let Zb[n](x) = polynomial in x whose coefficients are the corresponding digits of index n in base b. Then Z2[(5*4^k-2)/3](1/tau) = 1. - _Marc LeBrun_, Mar 01 2001",
				"a(n)=number of derangements of [2n+2] with runs consisting of consecutive integers. E.g., a(1)=6 because the derangements of {1,2,3,4} with runs consisting of consecutive integers are 4|123, 34|12, 4|3|12, 4|3|2|1, 234|1 and 34|2|1 (the bars delimit the runs). - _Emeric Deutsch_, May 26 2003",
				"Sum of n-th row of triangle of powers of 4: 1; 1 4 1; 1 4 16 4 1; 1 4 16 64 16 4 1; ... - _Philippe Deléham_, Feb 22 2014"
			],
			"reference": [
				"Clifford A. Pickover, A Passion for Mathematics, John Wiley \u0026 Sons, Inc., 2005, at pp. 104 and 311 (for \"Mr. Zanti's ants\")."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A020989/b020989.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"John Brillhart and Patrick Morton, \u003ca href=\"http://projecteuclid.org/euclid.ijm/1256048841\"\u003eÜber Summen von Rudin-Shapiroschen Koeffizienten\u003c/a\u003e, Illinois Journal of Mathematics, volume 22, issue 1, 1978, pages 126-148.  See Satz 9(a) page 132 and Satz 21 page 144 m_k = a(k).",
				"John Brillhart and Patrick Morton, \u003ca href=\"http://www.maa.org/programs/maa-awards/writing-awards/a-case-study-in-mathematical-research-the-golay-rudin-shapiro-sequence\"\u003eA case study in mathematical research: the Golay-Rudin-Shapiro sequence\u003c/a\u003e, Amer. Math. Monthly, 103 (1996) 854-869, see page 858 m_k = a(k).",
				"Kevin Ryde, \u003ca href=\"http://user42.tuxfamily.org/alternate/index.html\"\u003eIterations of the Alternate Paperfolding Curve\u003c/a\u003e, see index \"m_k\".",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (5,-4)."
			],
			"formula": [
				"a(0) = 1, a(n) = 4*a(n-1) + 2; a(n) = a(n-1)+ 5*{4^(n-1)}; - _Amarnath Murthy_, May 27 2001",
				"G.f.: (1+x)/((1-4*x)*(1-x)). - _Zerinvary Lajos_, Jan 11 2009; _Philippe Deléham_, Feb 22 2014",
				"a(n) = 5*a(n-1) - 4*a(n-2), a(0) = 1, a(1) = 6. - _Philippe Deléham_, Feb 22 2014",
				"a(n) = Sum_{k=0..n} A112468(n,k)*5^k. - _Philippe Deléham_, Feb 22 2014",
				"a(n) = (A020988(n) + A020988(n+1))/2. - _Yosu Yurramendi_, Jan 23 2017",
				"a(n) = A002450(n) + A002450(n+1). - _Yosu Yurramendi_, Jan 24 2017",
				"a(n) = 10*A020988(n-1) + 6. - _Yosu Yurramendi_, Feb 19 2017"
			],
			"example": [
				"a(0) = 1;",
				"a(1) = 1 + 4 + 1 = 6;",
				"a(2) = 1 + 4 + 16 + 4 + 1 = 26;",
				"a(3) = 1 + 4 + 16 + 64 + 16 + 4 + 1 = 106; etc. - _Philippe Deléham_, Feb 22 2014"
			],
			"mathematica": [
				"NestList[4#+2\u0026,1,25] (* _Harvey P. Dale_, Jul 23 2011 *)"
			],
			"program": [
				"(MAGMA) [(5*4^n-2)/3: n in [0..25]]; // _Vincenzo Librandi_, Jul 24 2011",
				"(PARI) a(n)=(5*4^n-2)/3 \\\\ _Charles R Greathouse IV_, Jul 02 2013"
			],
			"xref": [
				"Cf. A002450, A020988, A112468.",
				"A column of A119726."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Divided g.f. by x to match the offset. - _Philippe Deléham_, Feb 22 2014"
			],
			"references": 27,
			"revision": 46,
			"time": "2021-08-28T08:10:02-04:00",
			"created": "1998-06-14T03:00:00-04:00"
		}
	]
}