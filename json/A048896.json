{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A048896",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 48896,
			"data": "1,1,2,1,2,2,4,1,2,2,4,2,4,4,8,1,2,2,4,2,4,4,8,2,4,4,8,4,8,8,16,1,2,2,4,2,4,4,8,2,4,4,8,4,8,8,16,2,4,4,8,4,8,8,16,4,8,8,16,8,16,16,32,1,2,2,4,2,4,4,8,2,4,4,8,4,8,8,16,2,4,4,8,4,8,8,16,4,8,8,16,8,16,16,32,2,4,4",
			"name": "a(n) = 2^(A000120(n+1) - 1), n \u003e= 0.",
			"comment": [
				"a(n) = 2^A048881 = 2^{maximal power of 2 dividing the n-th Catalan number (A000108)}. [Comment corrected by _N. J. A. Sloane_, Apr 30 2018]",
				"Row sums of triangle A128937. - _Philippe Deléham_, May 02 2007",
				"a(n) = sum of (n+1)-th row terms of triangle A167364. - _Gary W. Adamson_, Nov 01 2009",
				"a(n), n \u003e= 1: Numerators of Maclaurin series for 1 - ((sin x)/x)^2, A117972(n), n \u003e= 2: Denominators of Maclaurin series for 1 - ((sin x)/x)^2, the correlation function in Montgomery's pair correlation conjecture. - _Daniel Forgues_, Oct 16 2011",
				"For n \u003e 0: a(n) = A007954(A007931(n)). - _Reinhard Zumkeller_, Oct 26 2012",
				"a(n) = A261363(2*(n+1), n+1). - _Reinhard Zumkeller_, Aug 16 2015"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A048896/b048896.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Neil J. Calkin, Eunice Y. S. Chan, and Robert M. Corless, \u003ca href=\"https://ojs.lib.uwo.ca/index.php/maple/article/view/14037\"\u003eSome Facts and Conjectures about Mandelbrot Polynomials\u003c/a\u003e, Maple Transactions (2021) Vol. 1, No. 1 Article 1.",
				"Neil J. Calkin, Eunice Y. S. Chan, Robert M. Corless, David J. Jeffrey, and Piers W. Lawrence, \u003ca href=\"https://arxiv.org/abs/2104.01116\"\u003eA Fractal Eigenvector\u003c/a\u003e, arXiv:2104.01116 [math.DS], 2021.",
				"E. Deutsch and B. E. Sagan, \u003ca href=\"https://arxiv.org/abs/math/0407326\"\u003eCongruences for Catalan and Motzkin numbers and related sequences\u003c/a\u003e, arXiv:math/0407326 [math.CO], 2004; J. Num. Theory 117 (2006), 191-215.",
				"OEIS Wiki, \u003ca href=\"/wiki/Montgomery%27s_pair_correlation_conjecture\"\u003eMontgomery's pair correlation conjecture\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 2^A048881(n).",
				"a(n) = 2^k if 2^k divides A000108(n) but 2^(k+1) does not divide A000108(n).",
				"It appears that a(n) = Sum_{k=0..n} binomial(2*(n+1), k) mod 2. - Christopher Lenard (c.lenard(AT)bendigo.latrobe.edu.au), Aug 20 2001",
				"a(0) = 1; a(2*n) = 2*a(2*n-1); a(2*n+1) = a(n).",
				"a(n) = (1/2) * A001316(n+1). - Mohammed Bouayoun (bouyao(AT)wanadoo.fr), Mar 26 2004",
				"It appears that a(n) = Sum_{k=0..2n} floor(binomial(2n+2, k+1)/2)(-1)^k = 2^n - Sum_{k=0..n+1} floor(binomial(n+1, k)/2). - _Paul Barry_, Dec 24 2004",
				"a(n) = Sum_{k=0..n} (T(n,k) mod 2) where T = A039598, A053121, A052179, A124575, A126075, A126093. - _Philippe Deléham_, May 02 2007",
				"a(n) = numerator(b(n)), where sin(x)^2/x = Sum_{n\u003e0} b(n)*(-1)^n x^(2*n-1). - _Vladimir Kruchinin_, Feb 06 2013",
				"a((2*n+1)*2^p-1) = A001316(n), p \u003e= 0 and n \u003e= 0. - _Johannes W. Meijer_, Feb 12 2013",
				"a(n) = numerator(2^n / (n+1)!). - _Vincenzo Librandi_, Apr 12 2014",
				"a(2n) = (2n+1)!/(n!n!)/A001803(n). - _Richard Turk_, Aug 23 2017",
				"a(2n-1) = (2n-1)!/(n!(n-1)!)/A001790(n). - _Richard Turk_, Aug 23 2017"
			],
			"example": [
				"From _Omar E. Pol_, Jul 21 2009: (Start)",
				"If written as a triangle:",
				"1;",
				"1,2;",
				"1,2,2,4;",
				"1,2,2,4,2,4,4,8;",
				"1,2,2,4,2,4,4,8,2,4,4,8,4,8,8,16;",
				"1,2,2,4,2,4,4,8,2,4,4,8,4,8,8,16,2,4,4,8,4,8,8,16,4,8,8,16,8,16,16,32;",
				"...,",
				"the first half-rows converge to Gould's sequence A001316.",
				"(End)"
			],
			"maple": [
				"a := n -\u003e 2^(add(i,i=convert(n+1,base,2))-1): seq(a(n), n=0..97); # _Peter Luschny_, May 01 2009"
			],
			"mathematica": [
				"NestList[Flatten[#1 /. a_Integer -\u003e {a, 2 a}] \u0026, {1}, 4] // Flatten (* _Robert G. Wilson v_, Aug 01 2012 *)",
				"Table[Numerator[2^n / (n + 1)!], {n, 0, 200}] (* _Vincenzo Librandi_, Apr 12 2014 *)",
				"Denominator[Table[BernoulliB[2*n] / (Zeta[2*n]/Pi^[2*n]), {n, 1, 100}]] (* _Terry D. Grant_, May 29 2017 *)",
				"Table[Denominator[((2 n)!/2^(2 n + 1)) (-1)^n], {n, 1, 100}]/4 (* _Terry D. Grant_, May 29 2017 *)",
				"2^IntegerExponent[CatalanNumber[Range[0,100]],2] (* _Harvey P. Dale_, Apr 30 2018 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c1,1,if(n%2,a(n/2-1/2),2*a(n-1)))",
				"(Haskell)",
				"a048896 n = a048896_list !! n",
				"a048896_list = f [1] where f (x:xs) = x : f (xs ++ [x,2*x])",
				"-- _Reinhard Zumkeller_, Mar 07 2011",
				"(Haskell)",
				"import Data.List (transpose)",
				"a048896 = a000079 . a000120",
				"a048896_list = 1 : concat (transpose",
				"   [zipWith (-) (map (* 2) a048896_list) a048896_list,",
				"    map (* 2) a048896_list])",
				"-- _Reinhard Zumkeller_, Jun 16 2013",
				"(MAGMA) [Numerator(2^n / Factorial(n+1)): n in [0..100]]; // _Vincenzo Librandi_, Apr 12 2014"
			],
			"xref": [
				"This is Guy Steele's sequence GS(3, 5) (see A135416).",
				"Equals first right hand column of triangle A160468.",
				"Equals A160469(n+1)/A002425(n+1).",
				"Cf. A160476, A000079, A001316, A167364, A220466, A001316, A080100, A261363, A117972."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Wolfdieter Lang_",
			"ext": [
				"New definition from _N. J. A. Sloane_, Mar 01 2008"
			],
			"references": 38,
			"revision": 104,
			"time": "2021-09-14T01:00:42-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}