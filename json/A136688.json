{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A136688",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 136688,
			"data": "1,0,1,2,0,1,0,4,0,1,4,0,6,0,1,0,12,0,8,0,1,8,0,24,0,10,0,1,0,32,0,40,0,12,0,1,16,0,80,0,60,0,14,0,1,0,80,0,160,0,84,0,16,0,1,32,0,240,0,280,0,112,0,18,0,1,0,192,0,560,0,448,0,144,0,20,0,1",
			"name": "Triangular sequence of q-Fibonacci polynomials for s=2: F(x,n) = x*F(x,n-1) + s*F(x,n-2).",
			"comment": [
				"Row sums are: 1, 1, 3, 5, 11, 21, 43, 85, 171, 341, 683, ... = A001045(n).",
				"Riordan array (1/(1-2*x^2), x/(1-2*x^2)). - _Paul Barry_, Jun 18 2008",
				"Diagonal sums are 1,0,3,0,9,... with g.f. 1/(1-3*x^2). - _Paul Barry_, Jun 18 2008"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A136688/b136688.txt\"\u003eRows n = 1..100 of triangle, flattened\u003c/a\u003e (terms 1..500 from Nathaniel Johnston)",
				"J. Cigler, \u003ca href=\"http://www.fq.math.ca/Scanned/41-1/cigler.pdf\"\u003eq-Fibonacci polynomials\u003c/a\u003e, Fibonacci Quarterly 41 (2003) 31-40."
			],
			"formula": [
				"F(x,n) = x*F(x,n-1) + s*F(x,n-2), where F(x,0)=0, F(x,1)=1 and s=2."
			],
			"example": [
				"Triangle begins as:",
				"   1;",
				"   0,  1;",
				"   2,  0,   1;",
				"   0,  4,   0,   1;",
				"   4,  0,   6,   0,   1;",
				"   0, 12,   0,   8,   0,  1;",
				"   8,  0,  24,   0,  10,  0,   1;",
				"   0, 32,   0,  40,   0, 12,   0,  1;",
				"  16,  0,  80,   0,  60,  0,  14,  0,  1;",
				"   0, 80,   0, 160,   0, 84,   0, 16,  0, 1;",
				"  32,  0, 240,   0, 280,  0, 112,  0, 18, 0, 1;",
				"..."
			],
			"maple": [
				"A136688 := proc(n) option remember: if(n\u003c=1)then return n: else return x*A136688(n-1)+2*A136688(n-2): fi: end:",
				"seq(seq(coeff(A136688(n),x,m),m=0..n-1),n=1..10); # _Nathaniel Johnston_, Apr 27 2011"
			],
			"mathematica": [
				"s = 2; F[x_, n_]:= F[x, n]= If[n\u003c2, n, x*F[x, n-1] + s*F[x, n-2]]; Table[CoefficientList[F[x, n], x], {n,12}]//Flatten",
				"F[n_, x_, s_, q_]:= Sum[QBinomial[n-j-1, j, q]*q^Binomial[j+1, 2]*x^(n-2*j-1) *s^j, {j, 0, Floor[(n-1)/2]}]; Table[CoefficientList[F[n,x,2,1], x], {n, 1, 10}]//Flatten (* _G. C. Greubel_, Dec 16 2019 *)"
			],
			"program": [
				"(Sage)",
				"def f(n,x,s,q): return sum( q_binomial(n-j-1, j, q)*q^binomial(j+1,2)*x^(n-2*j-1)*s^j for j in (0..floor((n-1)/2)))",
				"def A136688_list(prec):",
				"    P.\u003cx\u003e = PowerSeriesRing(ZZ, prec)",
				"    return P( f(n,x,2,1) ).list()",
				"[A136688_list(n) for n in (1..10)] # _G. C. Greubel_, Dec 16 2019"
			],
			"xref": [
				"Cf. A136689, A136705."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "1,4",
			"author": "_Roger L. Bagula_, Apr 06 2008",
			"references": 2,
			"revision": 14,
			"time": "2019-12-16T22:10:16-05:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}