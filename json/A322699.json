{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A322699",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 322699,
			"data": "0,0,0,0,1,0,0,8,2,0,0,49,24,3,0,0,288,242,48,4,0,0,1681,2400,675,80,5,0,0,9800,23762,9408,1444,120,6,0,0,57121,235224,131043,25920,2645,168,7,0,0,332928,2328482,1825200,465124,58080,4374,224,8,0",
			"name": "Square array A(n,k), n \u003e= 0, k \u003e= 0, read by antidiagonals, where A(n,k) is 1/2 * (-1 + Sum_{j=0..k} binomial(2*k,2*j)*(n+1)^(k-j)*n^j).",
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A322699/b322699.txt\"\u003eAntidiagonals n = 0..139, flattened\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Chebyshev_polynomials\"\u003eChebyshev polynomials\u003c/a\u003e.",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e"
			],
			"formula": [
				"sqrt(A(n,k)+1) + sqrt(A(n,k)) = (sqrt(n+1) + sqrt(n))^k.",
				"sqrt(A(n,k)+1) - sqrt(A(n,k)) = (sqrt(n+1) - sqrt(n))^k.",
				"A(n,0) = 0, A(n,1) = n and A(n,k) = (4*n+2) * A(n,k-1) - A(n,k-2) + 2*n for k \u003e 1.",
				"A(n,k) = (T_{k}(2*n+1) - 1)/2 where T_{k}(x) is a Chebyshev polynomial of the first kind.",
				"T_1(x) = x. So A(n,1) = (2*n+1-1)/2 = n."
			],
			"example": [
				"Square array begins:",
				"   0, 0,   0,    0,      0,       0,        0, ...",
				"   0, 1,   8,   49,    288,    1681,     9800, ...",
				"   0, 2,  24,  242,   2400,   23762,   235224, ...",
				"   0, 3,  48,  675,   9408,  131043,  1825200, ...",
				"   0, 4,  80, 1444,  25920,  465124,  8346320, ...",
				"   0, 5, 120, 2645,  58080, 1275125, 27994680, ...",
				"   0, 6, 168, 4374, 113568, 2948406, 76545000, ..."
			],
			"mathematica": [
				"Unprotect[Power]; 0^0 := 1; Protect[Power]; Table[(-1 + Sum[Binomial[2 k, 2 j] (# + 1)^(k - j)*#^j, {j, 0, k}])/2 \u0026[n - k], {n, 0, 9}, {k, n, 0, -1}] // Flatten (* _Michael De Vlieger_, Jan 01 2019 *)",
				"nmax = 9; row[n_] := LinearRecurrence[{4n+3, -4n-3, 1}, {0, n, 4n(n+1)}, nmax+1]; T = Array[row, nmax+1, 0]; A[n_, k_] := T[[n+1, k+1]];",
				"Table[A[n-k, k], {n, 0, nmax}, {k, n, 0, -1}] // Flatten (* _Jean-François Alcover_, Jan 06 2019 *)"
			],
			"program": [
				"(Ruby)",
				"def ncr(n, r)",
				"  return 1 if r == 0",
				"  (n - r + 1..n).inject(:*) / (1..r).inject(:*)",
				"end",
				"def A(k, n)",
				"  (0..n).map{|i| (0..k).inject(-1){|s, j| s + ncr(2 * k, 2 * j) * (i + 1) ** (k - j) * i ** j} / 2}",
				"end",
				"def A322699(n)",
				"  a = []",
				"  (0..n).each{|i| a \u003c\u003c A(i, n - i)}",
				"  ary = []",
				"  (0..n).each{|i|",
				"    (0..i).each{|j|",
				"      ary \u003c\u003c a[i - j][j]",
				"    }",
				"  }",
				"  ary",
				"end",
				"p A322699(10)"
			],
			"xref": [
				"Columns 0-5 give A000004, A001477, A033996, A322675, A322677, A322745.",
				"Rows 0-9 give A000004, A001108, A132596, A007654(n+1), A132584, A322707, A322708, A322709, A132592, A132593.",
				"Main diagonal gives A322746.",
				"Cf. A173175 (A(n,2*n)), A322790."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Seiichi Manyama_, Dec 23 2018",
			"references": 9,
			"revision": 66,
			"time": "2019-01-07T04:30:41-05:00",
			"created": "2018-12-25T11:32:29-05:00"
		}
	]
}