{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A128084",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 128084,
			"data": "1,1,1,1,2,2,2,1,1,3,5,7,8,8,7,5,3,1,1,4,9,16,24,32,39,44,46,44,39,32,24,16,9,4,1,1,5,14,30,54,86,125,169,215,259,297,325,340,340,325,297,259,215,169,125,86,54,30,14,5,1,1,6,20,50,104,190,315,484,699,958,1255,1580,1919,2254,2565,2832,3037,3166,3210,3166,3037,2832,2565,2254,1919,1580,1255,958,699,484,315,190,104,50,20,6,1,1,7,27,77,181,371,686,1170,1869,2827,4082,5662,7581,9835,12399,15225,18242,21358,24464,27440,30162,32510,34376,35672,36336",
			"name": "Triangle, read by rows of n^2+1 terms, of coefficients of q in the q-analog of the even double factorials: T(n,k) = [q^k] Product_{j=1..n} (1-q^(2j))/(1-q) for n\u003e0, with T(0,0)=1.",
			"comment": [
				"See A128080 for the triangle of coefficients of q in the q-analog of the odd double factorials."
			],
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A128084/b128084.txt\"\u003eRows n=0..30 of triangle, in flattened form.\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/q-Factorial.html\"\u003eq-Factorial\u003c/a\u003e from MathWorld.",
				"A. V. Yurkin, \u003ca href=\"http://www.mce.biophys.msu.ru/eng/archive/abstracts/mce19/sect1138/doc150220/\"\u003eOn similarity of systems of geometrical and arithmetic triangles\u003c/a\u003e, in Mathematics, Computing, Education Conference XIX, 2012.",
				"A. V. Yurkin, \u003ca href=\"http://arxiv.org/abs/1302.6287\"\u003eNew view on the diffraction discovered by Grimaldi and Gaussian beams\u003c/a\u003e, arXiv preprint arXiv:1302.6287 [physics.optics], 2013.",
				"A. V. Yurkin, \u003ca href=\"https://www.lap-publishing.com/catalog/details//store/gb/book/978-3-659-38404-2/new-binomial-and-new-view-on-light-theory\"\u003eNew binomial and new view on light theory\u003c/a\u003e, LAP Lambert Academic Publishing, 2013, 78 pages."
			],
			"example": [
				"The row sums form A000165, the even double factorial numbers:",
				"[1, 2, 8, 48, 384, 3840, 46080, 645120, ..., (2n)!!, ...].",
				"Triangle begins:",
				"1;",
				"1, 1;",
				"1, 2,  2,  2,  1;",
				"1, 3,  5,  7,  8,  8,   7,   5,   3,   1;",
				"1, 4,  9, 16, 24, 32,  39,  44,  46,  44,  39,  32,  24,  16,   9,   4,   1;",
				"1, 5, 14, 30, 54, 86, 125, 169, 215, 259, 297, 325, 340, 340, 325, 297, 259, 215, 169, 125, 86, 54, 30, 14, 5, 1;",
				"1, 6, 20, 50, 104, 190, 315, 484, 699, 958, 1255, 1580, 1919, 2254, 2565, 2832, 3037, 3166, 3210, 3166, 3037, 2832, 2565, 2254, 1919, 1580, 1255, 958, 699, 484, 315, 190, 104, 50, 20, 6, 1;",
				"1, 7, 27, 77, 181, 371, 686, 1170, 1869, 2827, 4082, 5662, 7581, 9835, 12399, 15225, 18242, 21358, 24464, 27440, 30162, 32510, 34376, 35672, 36336, 36336, ...; ..."
			],
			"mathematica": [
				"t[n_, k_] := If[k \u003c 0 || k \u003e n^2, 0, If[n == 0, 1, Coefficient[ Series[ Product[ (1 - q^(2*j))/(1 - q), {j, 1, n}], {q, 0, n^2}], q, k]]]; Table[t[n, k], {n, 0, 6}, {k, 0, n^2}] // Flatten (* _Jean-François Alcover_, Mar 06 2013, translated from Pari *)"
			],
			"program": [
				"(PARI) {T(n,k) = if(k\u003c0||k\u003en^2,0, if(n==0,1, polcoeff( prod(j=1,n,(1-q^(2*j))/(1-q)), k,q) ))}",
				"for(n=0,8,for(k=0,n^2,print1(T(n,k),\", \"));print(\"\"))"
			],
			"xref": [
				"Cf. A000165 ((2n)!!); A128085 (central terms); A128086 (diagonal), A128087 (row squared sums); A128080, A002522 (row lengths).",
				"The growth series for the affine Coxeter groups B_2 through B_12 are A161696-A161699, A161716, A161717, A161733, A161755, A161776, A161858. These are all rows of A128084."
			],
			"keyword": "nonn,tabf",
			"offset": "0,5",
			"author": "_Paul D. Hanna_, Feb 14 2007",
			"references": 67,
			"revision": 42,
			"time": "2016-08-23T13:39:23-04:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}