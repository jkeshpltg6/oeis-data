{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A243399",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 243399,
			"data": "1,19,362,6897,131405,2503592,47699653,908796999,17314842634,329890807045,6285240176489,119749454160336,2281524869222873,43468721969394923,828187242287726410,15779026325436196713,300629687425575463957,5727743087411370011896",
			"name": "a(0) = 1, a(1) = 19; for n \u003e 1, a(n) = 19*a(n-1) + a(n-2).",
			"comment": [
				"a(n+1)/a(n) tends to (19 + sqrt(365))/2.",
				"a(n) equals the number of words of length n on alphabet {0,1,...,19} avoiding runs of zeros of odd lengths. - _Milan Janjic_, Jan 28 2015"
			],
			"link": [
				"Bruno Berselli, \u003ca href=\"/A243399/b243399.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html#dPlusOne\"\u003eRecursive Sequences\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/FibonacciPolynomial.html\"\u003eFibonacci Polynomial\u003c/a\u003e.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (19,1)."
			],
			"formula": [
				"G.f.: 1/(1 - 19*x - x^2).",
				"a(n) = (-1)^n*a(-n-2) = ((19 + sqrt(365))^(n+1)-(19 - sqrt(365))^(n+1))/(2^(n+1)*sqrt(365)).",
				"a(n) = F(n+1, 19), the (n+1)-th Fibonacci polynomial evaluated at x = 19.",
				"a(n)*a(n-2) - a(n-1)^2 = (-1)^n, with a(-2)=1, a(-1)=0."
			],
			"mathematica": [
				"RecurrenceTable[{a[n] == 19 a[n - 1] + a[n - 2], a[0] == 1, a[1] == 19}, a, {n, 0, 20}]"
			],
			"program": [
				"(PARI) v=vector(20); v[1]=1; v[2]=19; for(i=3, #v, v[i]=19*v[i-1]+v[i-2]); v",
				"(MAGMA) [n le 2 select 19^(n-1) else 19*Self(n-1)+Self(n-2): n in [1..20]];",
				"(Maxima) a[0]:1$ a[1]:19$ a[n]:=19*a[n-1]+a[n-2]$ makelist(a[n], n, 0, 20);",
				"(Sage)",
				"from sage.combinat.sloane_functions import recur_gen2",
				"a = recur_gen2(1,19,19,1)",
				"[next(a) for i in (0..20)]"
			],
			"xref": [
				"Row 19 of A172236.",
				"Sequences with g.f. 1/(1-k*x-x^2) or x/(1-k*x-x^2): A000045 (k=1), A000129 (k=2), A006190 (k=3), A001076 (k=4), A052918 (k=5), A005668 (k=6), A054413 (k=7), A041025 (k=8), A099371 (k=9), A041041 (k=10), A049666 (k=11), A041061 (k=12), A140455 (k=13), A041085 (k=14), A154597 (k=15), A041113 (k=16), A178765 (k=17), A041145 (k=18), this sequence (k=19), A041181 (k=20). Also, many other sequences are in the OEIS with even k greater than 20 (denominators of continued fraction convergents to sqrt((k/2)^2+1))."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Bruno Berselli_, Jun 04 2014",
			"references": 16,
			"revision": 39,
			"time": "2020-02-26T04:15:56-05:00",
			"created": "2014-06-05T12:32:50-04:00"
		}
	]
}