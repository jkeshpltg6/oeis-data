{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A106198",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 106198,
			"data": "1,1,1,2,2,1,3,5,3,1,5,13,10,4,1,8,34,35,17,5,1,13,89,125,75,26,6,1,21,233,450,338,139,37,7,1,34,610,1625,1541,757,233,50,8,1",
			"name": "Triangle, columns = successive binomial transforms of Fibonacci numbers.",
			"comment": [
				"First few rows of the triangle are:",
				"   1;",
				"   1,   1;",
				"   2,   2,   1;",
				"   3,   5,   3,   1;",
				"   5,  13,  10,   4,   1;",
				"   8,  34,  35,  17,   5,   1;",
				"  13,  89, 125,  75,  26,   6,   1;",
				"  21, 233, 450, 338, 139,  37,   7,   1;",
				"  ...",
				"Column 0 = Fibonacci numbers, column 1 = odd-indexed Fibonacci numbers (first binomial transform of 1, 1, 2, 3, 5, ...); column 2 = second binomial transform of Fibonacci numbers, etc."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A106198/b106198.txt\"\u003eRows n = 0..100 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"Offset column k = k-th binomial transform of the Fibonacci numbers, given leftmost column = Fibonacci numbers."
			],
			"example": [
				"Column 2 = A081567, second binomial transform of Fibonacci numbers: 1, 3, 10, 35, 125, ..."
			],
			"maple": [
				"with(combinat);",
				"T:= proc(n, k) option remember;",
				"      if k=0 then fibonacci(n+1)",
				"    else add( binomial(n-k,j)*fibonacci(j+1)*k^(n-k-j), j=0..n-k)",
				"      fi; end:",
				"seq(seq(T(n, k), k=0..n), n=0..10); # _G. C. Greubel_, Dec 11 2019"
			],
			"mathematica": [
				"Table[If[k==0, Fibonacci[n+1], Sum[Binomial[n-k, j]*Fibonacci[j+1]*k^(n-k-j), {j,0,n-k}]], {n,0,10}, {k,0,n}]//Flatten (* _G. C. Greubel_, Dec 11 2019 *)"
			],
			"program": [
				"(PARI) T(n,k) = if(k==0, fibonacci(n+1), sum(j=0,n-k, binomial(n-k,j)*fibonacci( j+1)*k^(n-k-j)) ); \\\\ _G. C. Greubel_, Dec 11 2019",
				"(MAGMA)",
				"function T(n,k)",
				"  if k eq 0 then return Fibonacci(n+1);",
				"  else return (\u0026+[Binomial(n-k,j)*Fibonacci(j+1)*k^(n-k-j): j in [0..n-k]]);",
				"  end if; return T; end function;",
				"[T(n,k): k in [0..n], n in [0..10]]; // _G. C. Greubel_, Dec 11 2019",
				"(Sage)",
				"@CachedFunction",
				"def T(n, k):",
				"    if (k==0): return fibonacci(n+1)",
				"    else: return sum(binomial(n-k,j)*fibonacci(j+1)*k^(n-k-j) for j in (0..n-k))",
				"[[T(n, k) for k in (0..n)] for n in (0..10)] # _G. C. Greubel_, Dec 11 2019",
				"(GAP)",
				"T:= function(n,k)",
				"    if k=0 then return Fibonacci(n+1);",
				"    else return Sum([0..n-k], j-\u003e Binomial(n-k,j)*Fibonacci(j+1)*k^(n-k-j));",
				"    fi; end;",
				"Flat(List([0..10], n-\u003e List([0..n], k-\u003e T(n,k) ))); # _G. C. Greubel_, Dec 11 2019"
			],
			"xref": [
				"Cf. A000045, A081567, A081568, A081569, A081570."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Gary W. Adamson_, Apr 24 2005",
			"references": 1,
			"revision": 8,
			"time": "2019-12-11T23:43:08-05:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}