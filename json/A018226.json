{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A018226",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 18226,
			"data": "2,8,20,28,50,82,126",
			"name": "Magic numbers of nucleons: nuclei with one of these numbers of either protons or neutrons are more stable against nuclear decay.",
			"comment": [
				"In the shell model for the nucleus, magic numbers are the numbers of either protons or neutrons at which a shell is filled.",
				"First seven positive terms of A162626. - _Omar E. Pol_, Jul 07 2009",
				"Steppenbeck: \"The results of the experiment indicate that 54Ca's first excited state lies at a relatively high energy, which is characteristic of a large nuclear shell gap, thus indicating that N = 34 in 54Ca is a new magic number, as predicted theoretically by the University of Tokyo group in 2001. By conducting a more detailed comparison to nuclear theory the researchers were able to show that the N = 34 magic number is equally as significant as some other nuclear shell gaps.\""
			],
			"reference": [
				"A brief description is given under \"Magic numbers\" in the Encyclopedia Britannica.",
				"Dictionary of Science (Simon and Schuster), see the entry for \"Magic number\"."
			],
			"link": [
				"Radoslav Jovanovic, \u003ca href=\"http://milan.milanovic.org/math/english/atom/proton.html\"\u003eMagic Numbers and the Pascal Triangle\u003c/a\u003e",
				"S. Bjornholm, \u003ca href=\"http://dx.doi.org/10.1080/00107519008213781\"\u003eClusters, condensed matter in embryonic form\u003c/a\u003e, Contemp. Phys. 31 1990 pp. 309-324 (p. 312).",
				"J. Fridmann et al., \u003ca href=\"http://dx.doi.org/10.1038/nature03619\"\u003e'Magic' nucleus 42-Si\u003c/a\u003e, Nature, 435 (2005), 922-924 and 897-898.",
				"J. Glanz, \u003ca href=\"http://www.nytimes.com/2004/02/01/science/01ELEM.html\"\u003eUut and Uup Add Their Atomic Mass to Periodic Table\u003c/a\u003e, New York Times, Feb 01, 2003, pages 1 and 26.",
				"R. V. F. Janssens, \u003ca href=\"http://dx.doi.org/10.1038/4591069a\"\u003eUnexpected doubly magic nucleus\u003c/a\u003e, Nature, 459 (Jun 25 2009), 1069-1070. [_Added by N. J. A. Sloane, Jul 05 2009]",
				"Lutvo Kuric, \u003ca href=\"http://www.ilcpa.pl/wp-content/uploads/2013/10/ILCPA-132-2014-160-173.pdf\"\u003eDigital nuclear shell model\u003c/a\u003e, International Letters of Chemistry, Physics and Astronomy, 13(2) (2014) 160-173; ISSN 2299-3843.",
				"V. Ladma, \u003ca href=\"http://www.sweb.cz/vladimir_ladma/english/notes/texts/magicn.htm\"\u003eMagic Numbers\u003c/a\u003e",
				"NAPC Isotope Hydrology Section, \u003ca href=\"http://www.iaea.or.at/programmes/ripc/ih/volumes/vol_one/cht_i_02.pdf\"\u003eChapter 2, Atomic Systematics and Nuclear Structure\u003c/a\u003e [Broken link?]",
				"R. Nave, \u003ca href=\"http://hyperphysics.phy-astr.gsu.edu/hbase/nuclear/shell.html\"\u003eShell Model of Nucleus\u003c/a\u003e",
				"R. Nave, \u003ca href=\"http://hyperphysics.phy-astr.gsu.edu/hbase/nuclear/shell2.html\"\u003eEnhanced Abundance of Magic Number Nuclei\u003c/a\u003e",
				"Rachele Nerattini, Johann S. Brauchart, Michael K.-H. Kiessling, \u003ca href=\"http://arxiv.org/abs/1307.2834\"\u003eMagic numbers in Smale's 7th problem\u003c/a\u003e, arXiv:1307.2834v1 [math-ph], July 10, 2013.",
				"Phys.org, \u003ca href=\"http://phys.org/news/2013-10-evidence-nuclear-magic.html\"\u003eEvidence for a new nuclear 'magic number'\u003c/a\u003e, Oct 9, 2013.",
				"D. Steppenbeck et al., \u003ca href=\"http://dx.doi.org/10.1038/nature12522\"\u003eEvidence for a new nuclear 'magic number' from the level structure of 54Ca\u003c/a\u003e, Nature, 2013 DOI: 10.1038/nature12522.",
				"D. Warner, \u003ca href=\"http://dx.doi.org/10.1038/430517a\"\u003eNot-so-magic numbers\u003c/\u003e, Nature, 430 (Jul 29 2004), 517-519.",
				"D. Weise, \u003ca href=\"http://www.mi.sanu.ac.rs/vismath/weise1/\"\u003eThe Pythagorean Approach to Problems of Periodicity in Chemistry and Nuclear Physics\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Magic_number_(physics)\"\u003eMagic number (physics)\u003c/a\u003e"
			],
			"formula": [
				"If 1 \u003c= n \u003c= 3 then a(n)=n*(n+1)*(n+2)/3, else if 4 \u003c= n \u003c= 7 then a(n)=n(n^2+5)/3. - _Omar E. Pol_, Jul 07 2009 [This needs to be clarified. - _Joerg Arndt_, May 03 2011]",
				"From _Daniel Forgues_, May 03 2011: (Start)",
				"If 1 \u003c= n \u003c= 3 then a(n) = 2 T_n, else",
				"if 4 \u003c= n \u003c= 7 then a(n) = 2 (T_n - t_{n-1}),",
				"where T_n is the n-th tetrahedral number, t_n the n-th triangular number.",
				"G.f.: (2*x*(1 - 6*x^3 + 14*x^4 - 11*x^5 + 3*x^6))/(1 - x)^4, 1 \u003c= n \u003c= 7.",
				"Using those formulas for n \u003e= 0 gives A162626. (End)",
				"a(n) = n*(n^2+5)/3 + (4*n-6)*A171386(n). - _Omar E. Pol_, Aug 14 2013"
			],
			"xref": [
				"Cf. A018227 Number of electrons (which equals number of protons) such that they are arranged into complete shells within the atom.",
				"Cf. A033547, A110856, A130598, A162626, A046939, A046940."
			],
			"keyword": "nonn,fini,full",
			"offset": "1,1",
			"author": "John Raithel (raithel(AT)rahul.net)",
			"references": 36,
			"revision": 71,
			"time": "2015-04-02T06:28:15-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}