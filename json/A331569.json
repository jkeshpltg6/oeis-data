{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A331569",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 331569,
			"data": "1,1,1,0,1,1,0,1,0,1,0,1,3,0,1,0,1,17,0,0,1,0,1,230,184,0,0,1,0,1,3264,16936,840,0,0,1,0,1,60338,2711904,768785,0,0,0,1,0,1,1287062,675457000,1493786233,21770070,0,0,0,1,0,1,31900620,232383728378,5254074934990,585810653616,328149360,0,0,0,1",
			"name": "Array read by antidiagonals: A(n,k) is the number of binary matrices with k distinct columns and any number of distinct nonzero rows with n ones in every column and columns in decreasing lexicographic order.",
			"comment": [
				"The condition that the columns be in decreasing order is equivalent to considering nonequivalent matrices with distinct columns up to permutation of columns.",
				"A(n,k) is the number of k-block n-uniform T_0 set systems without isolated vertices."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A331569/b331569.txt\"\u003eTable of n, a(n) for n = 0..209\u003c/a\u003e"
			],
			"formula": [
				"A(n, k) = Sum_{j=0..k} Stirling1(k, j)*A331567(n, j)/k!.",
				"A(n, k) = Sum_{j=0..k} (-1)^(k-j)*binomial(k-1, k-j)*A331571(n, j).",
				"A331651(n) = Sum_{d|n} A(n/d, d)."
			],
			"example": [
				"Array begins:",
				"===============================================================",
				"n\\k | 0 1 2   3         4               5                 6",
				"----+----------------------------------------------------------",
				"  0 | 1 1 0   0         0               0                 0 ...",
				"  1 | 1 1 1   1         1               1                 1 ...",
				"  2 | 1 0 3  17       230            3264             60338 ...",
				"  3 | 1 0 0 184     16936         2711904         675457000 ...",
				"  4 | 1 0 0 840    768785      1493786233     5254074934990 ...",
				"  5 | 1 0 0   0  21770070    585810653616 30604798810581906 ...",
				"  6 | 1 0 0   0 328149360 161087473081920 ...",
				"  ...",
				"The A(2,2) = 3 matrices are:",
				"   [1 1]  [1 0]  [1 0]",
				"   [1 0]  [1 1]  [0 1]",
				"   [0 1]  [0 1]  [1 1]"
			],
			"program": [
				"(PARI)",
				"WeighT(v)={Vec(exp(x*Ser(dirmul(v, vector(#v, n, (-1)^(n-1)/n))))-1, -#v)}",
				"D(p, n, k)={my(v=vector(n)); for(i=1, #p, v[p[i]]++); binomial(WeighT(v)[n], k)/prod(i=1, #v, i^v[i]*v[i]!)}",
				"T(n, k)={ my(m=n*k+1, q=Vec(exp(intformal(O(x^m) - x^n/(1-x)))), f=Vec(serlaplace(1/(1+x) + O(x*x^m))/(x-1))); if(n==0, k\u003c=1, sum(j=1, m, my(s=0); forpart(p=j, s+=(-1)^#p*D(p, n, k), [1, n]); s*sum(i=j, m, q[i-j+1]*f[i]))); }"
			],
			"xref": [
				"Rows n=1..4 are A000012, A331649, A094631, A331650.",
				"Cf. A330942, A331039, A331567, A331570, A331571, A331572, A331651."
			],
			"keyword": "nonn,tabl",
			"offset": "0,13",
			"author": "_Andrew Howroyd_, Jan 20 2020",
			"references": 11,
			"revision": 10,
			"time": "2020-01-25T17:55:26-05:00",
			"created": "2020-01-22T20:14:59-05:00"
		}
	]
}