{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A152290",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 152290,
			"data": "1,1,2,1,5,5,5,1,14,21,31,30,19,9,1,42,84,154,210,245,217,175,105,49,14,1,132,330,708,1176,1722,2148,2386,2358,2080,1618,1086,644,294,104,20,1,429,1287,3135,6006,10164,15093,20496,25188,28770,30225,29511,26571",
			"name": "Coefficients in a q-analog of the LambertW function, as a triangle read by rows.",
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A152290/b152290.txt\"\u003eRows 0 to 30 of the triangle, flattened.\u003c/a\u003e",
				"Eric Weisstein, \u003ca href=\"http://mathworld.wolfram.com/q-ExponentialFunction.html\"\u003eq-Exponential Function\u003c/a\u003e from MathWorld.",
				"Eric Weisstein, \u003ca href=\"http://mathworld.wolfram.com/q-Factorial.html\"\u003eq-Factorial\u003c/a\u003e from MathWorld."
			],
			"formula": [
				"G.f.: A(x,q) = Sum_{n\u003e=0} Sum_{k=0..n(n-1)/2} T(n,k)*q^k*x^n/faq(n,q), where faq(n,q) is the q-factorial of n.",
				"G.f.: A(x,q) = (1/x)*Series_Reversion( x/e_q(x,q) ) where e_q(x,q) = Sum_{n\u003e=0} x^n/faq(n,q) is the q-exponential function.",
				"G.f. satisfies: A(x,q) = e_q( x*A(x,q), q) and A( x/e_q(x,q), q) = e_q(x,q).",
				"G.f. at q=1: A(x,1) = LambertW(-x)/(-x).",
				"Row sums at q=+1: Sum_{k=0..n(n-1)/2} T(n,k) = (n+1)^(n-1).",
				"Row sums at q=-1: Sum_{k=0..n(n-1)/2} T(n,k)*(-1)^k = (n+1)^[(n-1)/2] (A152291).",
				"Sum_{k=0..n(n-1)/2} T(n,k)*exp(2Pi*I*k/n)) = 1 for n\u003e=1; i.e., the n-th row sum at q = exp(2Pi*I/n), the n-th root of unity, equals 1 for n\u003e=1. [From _Paul D. Hanna_, Dec 04 2008]",
				"Sum_{k=0..n*(n-1)/2} T(n,k)*q^k = Sum_{pi} n!/(n-k+1)!*faq(n,q)/Product_{i=1..n} e(i)!*faq(i,q)^e(i), where pi runs through all nonnegative integer solutions of e(1)+2*e(2)+...+n*e(n) = n and k = e(1)+e(2)+...+e(n). [From _Vladeta Jovovic_, Dec 05 2008]",
				"Sum_{k=0..[n/2]} T(n, n*k) = (1/n)*Sum_{d|n} phi(n/d)*(n+1)^(d-1), for n\u003e0, with a(0)=1. [From _Paul D. Hanna_, Jul 18 2013]",
				"Sum_{k=0..[n/2]} T(n, n*k) = A121774(n), the number of n-bead necklaces with n+1 colors, divided by (n+1). [From _Paul D. Hanna_, Jul 18 2013]"
			],
			"example": [
				"Triangle, with columns k=0..n(n-1)/2 for row n\u003e=0, begins:",
				"1;",
				"1;",
				"2, 1;",
				"5, 5, 5, 1;",
				"14, 21, 31, 30, 19, 9, 1;",
				"42, 84, 154, 210, 245, 217, 175, 105, 49, 14, 1;",
				"132, 330, 708, 1176, 1722, 2148, 2386, 2358, 2080, 1618, 1086, 644, 294, 104, 20, 1;",
				"429, 1287, 3135, 6006, 10164, 15093, 20496, 25188, 28770, 30225, 29511, 26571, 22161, 16926, 11832, 7392, 4089, 1932, 714, 195, 27, 1;...",
				"where row sums = (n+1)^(n-1) and column 0 is A000108 (Catalan numbers).",
				"Row sums at q=-1 = (n+1)^[(n-1)/2] (A152291): [1,1,1,4,5,36,49,512,729,...].",
				"The generating function starts:",
				"A(x,q) = 1 + x + (2 + q)*x^2/faq(2,q) + (5 + 5*q + 5*q^2 + q^3)*x^3/faq(3,q) + (14 + 21*q + 31*q^2 + 30*q^3 + 19*q^4 + 9*q^5 + q^6)*x^4/faq(4,q) + ...",
				"G.f. satisfies: A(x,q) = e_q( x*A(x,q), q), where the q-exponential series e_q(x,q) begins:",
				"e_q(x,q) = 1 + x + x^2/faq(2,q) + x^3/faq(3,q) +...+ x^n/faq(n,q) +...",
				"The q-factorial of n is faq(n,q) = Product_{k=1..n} (q^k-1)/(q-1):",
				"faq(0,q)=1, faq(1,q)=1, faq(2,q)=(1+q), faq(3,q)=(1+q)*(1+q+q^2),",
				"faq(4,q)=(1+q)*(1+q+q^2)*(1+q+q^2+q^3), ...",
				"Special cases of g.f.:",
				"q=0: A(x,0) = 1 + x + 2*x^2 + 5*x^3 + 14*x^4 + 42*x^5 +... (Catalan)",
				"q=1: A(x,1) = 1 + x + 3/2*x^2 + 16/6*x^3 + 125/24*x^4 +...= LambertW(-x)/(-x)",
				"q=2: A(x,2) = 1 + x + 4/3*x^2 + 43/21*x^3 + 1076/315*x^4 + 58746/9765*x^5 +...",
				"q=-1: Can A(x,-1) be defined? See A152291."
			],
			"program": [
				"(PARI) /* G.f.: LambertW_q(x,q) = (1/x)*Series_Reversion( x/e_q(x,q) ): */",
				"{T(n,k)=local(e_q=1+sum(j=1,n,x^j/prod(i=1,j,(q^i-1)/(q-1))),LW_q=serreverse(x/e_q+x^2*O(x^n))/x); polcoeff(polcoeff(LW_q+x*O(x^n),n,x)*prod(i=1,n,(q^i-1)/(q-1))+q*O(q^k),k,q)}",
				"for(n=0,8,for(k=0,n*(n-1)/2,print1(T(n,k),\",\"));print(\"\"))"
			],
			"xref": [
				"Cf. A152291 (q=-1), A000272 (row sums), A000108 (column 0), A002054 (column 1).",
				"Cf. A152282 (q=2), A152283 (q=3).",
				"Cf. A121774."
			],
			"keyword": "eigen,nonn,tabf",
			"offset": "0,3",
			"author": "_Paul D. Hanna_, Dec 02 2008",
			"references": 9,
			"revision": 17,
			"time": "2015-07-10T19:37:35-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}