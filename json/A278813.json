{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A278813",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 278813,
			"data": "5,7,5,8,1,9,5,9,3,9,1,1,0,3,7,4,9,4,1,9,7,4,0,2,8,8,6,5,0,0,9,3,2,9,0,9,2,4,7,4,2,4,2,6,4,7,0,5,5,3,1,5,4,1,5,1,4,1,2,5,9,9,0,6,1,9,7,1,0,7,5,9,8,9,1,5,8,7,2,3,0,8,3,3,3,7,8,7,0,6,9,5,8,7,9,1,1,5,7,2,0,0,5,6,2,9,5,0,5,6,3,2,1,1,0,5,7,1,4,7,1,3,5,9,5,0,6,0,7,7",
			"name": "Decimal expansion of c in the sequence b(n+1) = c^(b(n)/n) A278453, where b(1)=0 and c is chosen such that the sequence neither explodes nor goes to 1.",
			"comment": [
				"There exists a unique value of c for which the sequence b(n) does not converge to 1 and at the same time always satisfies b(n-1)b(n+1)/b(n)^2 \u003c 1.",
				"If c were chosen smaller the sequence b(n) would approach 1, if it were chosen greater it would at some point violate b(n-1)b(n+1)/b(n)^2 \u003c 1 and from there on quickly escalate.",
				"The value of c is found through trial and error. Suppose one starts with c = 5, the sequence b(n) would continue b(2) = 1, b(3) = 2.23..., b(4) = 3.31..., b(5) = 3.80..., b(6) = 3.39..., b(7) = 2.48..., b(8) = 1.77... and from there one can see that such a sequence is tending to 1. One continues by trying a larger value, say c = 6, which gives rise to b(2) = 1, b(3) = 2.44, b(4) = 4.31..., b(5) = 6.92..., b(6) = 11.94..., b(7) = 35.38... and from there one can see that such a sequence is escalating too fast. Therefore, one now knows that the true value of c is between 5 and 6.",
				"c satisfies 2*log_c(3*log_c(4*log_c(...))) = 1. - _Andrey Zabolotskiy_, Dec 02 2016",
				"No closed form expression is known. Probably transcendental but this is unproved. - _Robert G. Wilson v_, Dec 02 2016"
			],
			"link": [
				"Robert G. Wilson v, \u003ca href=\"/A278813/b278813.txt\"\u003eTable of n, a(n) for n = 1..2500\u003c/a\u003e (first 1000 from Rok Cestnik)",
				"Rok Cestnik, \u003ca href=\"/A278813/a278813.pdf\"\u003ePlot of the dependence of b(1) on c\u003c/a\u003e"
			],
			"example": [
				"5.75819593911037494197402886500932909247424264705531..."
			],
			"mathematica": [
				"b1 = 0;",
				"n = 100;",
				"acc = Round[n*1.2];",
				"th = 1000000;",
				"c = 0;",
				"For[p = 0, p \u003c acc, ++p, For[d = 0, d \u003c 9, ++d, c = c + 1/10^p;",
				"    bn = b1;",
				"    For[i = 1, i \u003c Round[n*1.2], ++i, bn = N[c^(bn/i), acc];",
				"     If[bn \u003e th, Break[]];];",
				"    If[bn \u003e th, {c = c - 1/10^p;",
				"      Break[];",
				"      }];",
				"    ];",
				"  ];",
				"N[c, n]"
			],
			"xref": [
				"For sequence round(b(n)) see A278453.",
				"For different values of b(1) see A278808, A278809, A278810, A278811, A278812."
			],
			"keyword": "nonn,cons,nice",
			"offset": "1,1",
			"author": "_Rok Cestnik_, Nov 28 2016",
			"references": 7,
			"revision": 31,
			"time": "2019-03-02T04:21:48-05:00",
			"created": "2016-11-30T06:23:42-05:00"
		}
	]
}