{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001687",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1687,
			"id": "M0147 N0059",
			"data": "0,1,0,1,0,1,1,1,2,1,3,2,4,4,5,7,7,11,11,16,18,23,29,34,45,52,68,81,102,126,154,194,235,296,361,450,555,685,851,1046,1301,1601,1986,2452,3032,3753,4633,5739,7085,8771,10838,13404,16577,20489,25348,31327",
			"name": "a(n) = a(n-2) + a(n-5).",
			"comment": [
				"a(n+1) is the number of compositions of n into parts 2 and 5. [_Joerg Arndt_, Mar 15 2013]"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A001687/b001687.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Dorota Bród, \u003ca href=\"https://doi.org/10.1007/s40590-021-00369-5\"\u003eOn trees with unique locating kernels\u003c/a\u003e, Boletín de la Sociedad Matemática Mexicana (2021) Vol. 27, Art. No. 61.",
				"T. M. Green, \u003ca href=\"http://www.jstor.org/stable/2687953\"\u003eRecurrent sequences and Pascal's triangle\u003c/a\u003e, Math. Mag., 41 (1968), 13-21.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=405\"\u003eEncyclopedia of Combinatorial Structures 405\u003c/a\u003e",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"E. Wilson, \u003ca href=\"http://www.anaphoria.com/meruone.PDF\"\u003eThe Scales of Mt. Meru\u003c/a\u003e",
				"R. Yanco, \u003ca href=\"/A007380/a007380.pdf\"\u003eLetter and Email to N. J. A. Sloane, 1994\u003c/a\u003e",
				"R. Yanco and A. Bagchi, \u003ca href=\"/A007380/a007380_1.pdf\"\u003eK-th order maximal independent sets in path and cycle graphs\u003c/a\u003e, Unpublished manuscript, 1994. (Annotated scanned copy)",
				"\u003ca href=\"/index/Tu#2wis\"\u003eIndex entries for two-way infinite sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0, 1, 0, 0, 1)."
			],
			"formula": [
				"G.f.: x/(1-x^2-x^5).",
				"G.f. A(x) satisfies 1+x^4*A(x) = 1/(1-x^5-x^7-x^9-....). - _Jon Perry_, Jul 04 2004",
				"G.f.: -x/( x^5 - 1 + 5*x^2/Q(0) ) where Q(k) = x + 5 + k*(x+1) - x*(k+1)*(k+6)/Q(k+1); (continued fraction). - _Sergei N. Gladkovskii_, Mar 15 2013"
			],
			"maple": [
				"A001687:=-z/(-1+z**2+z**5); # _Simon Plouffe_ in his 1992 dissertation"
			],
			"mathematica": [
				"CoefficientList[Series[x/(1-x^2-x^5),{x,0,60}],x] (* or *) Nest[ Append[#,#[[-5]]+#[[-2]]]\u0026, {0,1,0,1,0}, 60]  (* _Harvey P. Dale_, Apr 06 2011 *)",
				"LinearRecurrence[{0, 1, 0, 0, 1}, {0, 1, 0, 1, 0}, 100] (* _T. D. Noe_, Aug 09 2012 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c0,polcoeff(x^4/(1+x^3-x^5)+x^-n*O(x),-n),polcoeff(x/(1-x^2-x^5)+x^n*O(x),n)) /* _Michael Somos_, Jul 15 2004 */",
				"(Maxima)",
				"a(n):=sum(if mod(n-5*k,3)=0 then binomial(k,(5*k-n)/3) else 0,k,1,n); /* _Vladimir Kruchinin_, May 24 2011 */"
			],
			"xref": [
				"Cf. A005686."
			],
			"keyword": "nonn",
			"offset": "0,9",
			"author": "_N. J. A. Sloane_, following a suggestion from _Robert G. Wilson v_",
			"references": 5,
			"revision": 62,
			"time": "2021-12-20T18:54:57-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}