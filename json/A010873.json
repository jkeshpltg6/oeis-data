{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A010873",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 10873,
			"data": "0,1,2,3,0,1,2,3,0,1,2,3,0,1,2,3,0,1,2,3,0,1,2,3,0,1,2,3,0,1,2,3,0,1,2,3,0,1,2,3,0,1,2,3,0,1,2,3,0,1,2,3,0,1,2,3,0,1,2,3,0,1,2,3,0,1,2,3,0,1,2,3,0,1,2,3,0,1,2,3,0",
			"name": "a(n) = n mod 4.",
			"comment": [
				"Complement of A002265, since 4*A002265(n)+a(n) = n. - _Hieronymus Fischer_, Jun 01 2007",
				"The rightmost digit in the base-4 representation of n. Also, the equivalent value of the two rightmost digits in the base-2 representation of n. - _Hieronymus Fischer_, Jun 11 2007",
				"Periodic sequences of this type can be also calculated by a(n) = floor(q/(p^m-1)*p^n) mod p, where q is the number representing the periodic digit pattern and m is the period length. p and q can be calculated as follows: Let D be the array representing the number pattern to be repeated, m = size of D, max = maximum value of elements in D. Than p := max + 1 and q := p^m*sum_{i=1..m} D(i)/p^i. Example: D = (0, 1, 2, 3), p = 4 and q = 57 for this sequence. - _Hieronymus Fischer_, Jan 04 2013"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A010873/b010873.txt\"\u003eTable of n, a(n) for n = 0..65536\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,0,0,1)."
			],
			"formula": [
				"a(n) = (1/2)*(3-(-1)^n-2*(-1)^floor(n/2));",
				"also a(n) = (1/2)*(3-(-1)^n-2*(-1)^((2n-1+(-1)^n)/4)));",
				"also a(n) = (1/2)*(3-(-1)^n-2*sin(Pi/4*(2n+1+(-1)^n))).",
				"G.f.: (3x^3+2x^2+x)/(1-x^4). - _Hieronymus Fischer_, May 29 2007",
				"From _Hieronymus Fischer_, Jun 11 2007: (Start)",
				"Trigonometric representation: a(n)=2^2*(sin(n*Pi/4))^2*sum{1\u003c=k\u003c4, k*product{1\u003c=m\u003c4,m\u003c\u003ek, (sin((n-m)*Pi/4))^2}}. Clearly, the squared terms may be replaced by their absolute values '|.|'.",
				"Complex representation: a(n)=1/4*(1-r^n)*sum{1\u003c=k\u003c4, k*product{1\u003c=m\u003c4,m\u003c\u003ek, (1-r^(n-m))}} where r=exp(Pi/2*i)=i=sqrt(-1). All these formulas can be easily adapted to represent any periodic sequence.",
				"a(n) = n mod 2+2*(floor(n/2)mod 2) = A000035(n)+2*A000035(A004526(n)). (End)",
				"a(n) = 6 - a(n-1) - a(n-2) - a(n-3) for n \u003e 2. - _Reinhard Zumkeller_, Apr 13 2008",
				"a(n) = 3/2 + cos((n+1)pi)/2 + sqrt(2)cos((2n+3)Pi/4) [_Jaume Oliver Lafont_, Dec 05 2008]",
				"From _Hieronymus Fischer_, Jan 04 2013: (Start)",
				"a(n) = floor(41/3333*10^(n+1)) mod 10.",
				"a(n) = floor(19/85*4^(n+1)) mod 4. (End)",
				"E.g.f.: 2*sinh(x) - sin(x) + cosh(x) - cos(x). - _Stefano Spezia_, Apr 20 2021"
			],
			"maple": [
				"seq(chrem( [n,n], [1,4] ), n=0..80); # _Zerinvary Lajos_, Mar 25 2009"
			],
			"mathematica": [
				"nn=40; CoefficientList[Series[(x+2x^2+3x^3)/(1-x^4), {x,0,nn}], x] (* _Geoffrey Critzer_, Jul 26 2013 *)",
				"Table[Mod[n,4], {n, 0, 100}] (* _T. D. Noe_, Jul 26 2013 *)",
				"PadRight[{},120,{0,1,2,3}] (* _Harvey P. Dale_, Mar 29 2018 *)"
			],
			"program": [
				"(PARI) a(n)=n%4 \\\\ _Charles R Greathouse IV_, Dec 05 2011",
				"(Haskell)",
				"a010873 n = (`mod` 4)",
				"a010873_list = cycle [0..3]  -- _Reinhard Zumkeller_, Jun 05 2012",
				"(Scheme) (define (A010873 n) (modulo n 4)) ;; _Antti Karttunen_, Nov 07 2017"
			],
			"xref": [
				"Partial sums: A130482. Other related sequences A130481, A130483, A130484, A130485.",
				"Cf. A004526, A002264, A002265, A002266.",
				"Cf. A000035, A010877."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"First to third formulas re-edited for better readability by _Hieronymus Fischer_, Dec 05 2011",
				"Incorrect g.f. removed by _Georg Fischer_, May 18 2019"
			],
			"references": 81,
			"revision": 69,
			"time": "2021-12-27T21:51:35-05:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}