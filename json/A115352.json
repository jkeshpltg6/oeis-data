{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A115352",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 115352,
			"data": "0,0,1,0,2,3,1,0,4,5,6,7,2,3,1,0,8,9,10,11,12,13,14,15,4,5,6,7,2,3,1,0,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,8,9,10,11,12,13,14,15,4,5,6,7,2,3,1,0,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49",
			"name": "Concatenation of finite strings S_0, S_1, S_2, ..., where S_0 = {0} and for k \u003e= 1, S_k is obtained from S_{k-1} by inserting the numbers 2^(k-1) through 2^k-1 after the initial 0.",
			"comment": [
				"For example, for k = 3, take S_2 = {0,2,3,1} and insert 2^2 through 2^3-1 after the 0, so that S_3 = {0,4,5,6,7,2,3,1}. The string S_k has length 2^k.",
				"A self-similar fractal sequence.",
				"This is the sequence g_n at the end of Section 2 of Levine's paper. The paper also continues several other sequences that are probably not in the OEIS at present."
			],
			"reference": [
				"L. Levine, Fractal sequences and restricted Nim, Ars Combin. 80 (2006), 113-127."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A115352/b115352.txt\"\u003eTable of n, a(n) for n = 0..1022\u003c/a\u003e",
				"L. Levine, \u003ca href=\"https://arxiv.org/abs/math/0409408\"\u003eFractal sequences and restricted Nim\u003c/a\u003e, arXiv:math/0409408 [math.CO], 2004.",
				"L. Levine, \u003ca href=\"http://math.berkeley.edu/~levine/\"\u003eHome Page\u003c/a\u003e"
			],
			"formula": [
				"If n=2^m-1, then a(n)=0; for all other terms, write n in binary, collapse the initial segment of 1's to a single 1 and delete the first 0. For example, a(25)=a(11001)=101=5. - Lionel Levine (levine(AT)Math.Berkeley.EDU), May 04 2006",
				"a(n)=0 if n=2^m-1, otherwise A054429(2^ceiling(log_2(n+1))-n-1). - _Peter Ward_, Jan 23 2020",
				"a(0)=0, a(1)=0; for all other terms, write n as 2^(m+1)+k with 0 \u003c= k \u003c 2^(m+1), then a(n)=2^m+k if k \u003c 2^m, otherwise a(k). - _Peter Ward_, Jan 23 2020"
			],
			"example": [
				"The first few strings S_0, S_1, S_2, ... are as follows:",
				"0",
				"0,1",
				"0,2,3,1",
				"0,4,5,6,7,2,3,1",
				"0,8,9,10,11,12,13,14,15,4,5,6,7,2,3,1"
			],
			"mathematica": [
				"Nest[Append[#, Join[{#[[-1, 1]]}, Range[#2, 2 #2 - 1], Rest@ #[[-1]]]] \u0026 @@ {#1,Length@ #[[-1]]} \u0026, {{0}, {0, 1}}, 5] // Flatten (* _Michael De Vlieger_, Jan 25 2020 *)"
			],
			"xref": [
				"See A025480 for a similar sequence."
			],
			"keyword": "nonn,tabf",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_, Mar 10 2006",
			"ext": [
				"Edited by _Robert G. Wilson v_, Apr 11 2006",
				"Further edited by _N. J. A. Sloane_, Jan 16 2009"
			],
			"references": 1,
			"revision": 25,
			"time": "2020-02-23T01:46:09-05:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}