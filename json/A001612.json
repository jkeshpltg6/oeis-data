{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001612",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1612,
			"id": "M0974 N0364",
			"data": "3,2,4,5,8,12,19,30,48,77,124,200,323,522,844,1365,2208,3572,5779,9350,15128,24477,39604,64080,103683,167762,271444,439205,710648,1149852,1860499,3010350,4870848,7881197,12752044,20633240,33385283,54018522",
			"name": "a(n) = a(n-1) + a(n-2) - 1 for n \u003e 1, a(0)=3, a(1)=2.",
			"comment": [
				"a(n+3) = A^(n)B^(2)(1), n \u003e= 0, with compositions of Wythoff's complementary A(n):=A000201(n) and B(n)=A001950(n) sequences. See the W. Lang link under A135817 for the Wythoff representation of numbers (with A as 1 and B as 0 and the argument 1 omitted). E.g., 5=`00`, 8=`100`, 12=`1100`, ..., in Wythoff code.",
				"From _Petros Hadjicostas_, Jan 11 2017: (Start)",
				"a(n) is the number of cyclic sequences consisting of zeros and ones that avoid the pattern 001 (or equivalently, the pattern 110) provided the positions of zeros and ones on a circle are fixed. This can easily be proved by considering that sequence A000071(n+3) is the number of binary zero-one words of length n that avoid the pattern 001 and that a(n) = A000071(n+3) - 2*A000071(n). (From the collection of all zero-one binary sequences that avoid 001 subtract those that start with 1 and end with 00 and those that start with 01 and end with 0.)",
				"For n = 1,2, the number a(n) still gives the number of cyclic sequences consisting of zeros and ones that avoid the pattern 001 (provided the positions of zeros and ones on a circle are fixed) even if we assume that the sequence wraps around itself on the circle. For example, when 01 wraps around itself, it becomes 01010..., and it never contains the pattern 001. (End)",
				"For n \u003e= 3, a(n) is also the number of independent vertex sets and vertex covers in the wheel graph on n+1 nodes. - _Eric W. Weisstein_, Mar 31 2017"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A001612/b001612.txt\"\u003eTable of n, a(n) for n = 0..500\u003c/a\u003e",
				"Nazim Fatès, Biswanath Sethi, Sukanta Das, \u003ca href=\"https://hal.inria.fr/hal-01571847\"\u003eOn the reversibility of ECAs with fully asynchronous updating: the recurrence point of view\u003c/a\u003e, Preprint, 2017.",
				"Martin Griffiths, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL18/Griffiths/gr48.html\"\u003eOn a Matrix Arising from a Family of Iterated Self-Compositions\u003c/a\u003e, Journal of Integer Sequences, 18 (2015), #15.11.8.",
				"Fumio Hazama, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2011.06.008\"\u003eSpectra of graphs attached to the space of melodies\u003c/a\u003e, Discr. Math., 311 (2011), 2368-2383. See Table 5.2.",
				"Martin Herschend, Peter Jorgensen, \u003ca href=\"https://arxiv.org/abs/2002.01778\"\u003eClassification of higher wide subcategories for higher Auslander algebras of type A\u003c/a\u003e, arXiv:2002.01778 [math.RT], 2020.",
				"Dov Jarden, \u003ca href=\"/A001602/a001602.pdf\"\u003eRecurring Sequences\u003c/a\u003e, Riveon Lematematika, Jerusalem, 1966. [Annotated scanned copy] See p. 97.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/IndependentVertexSet.html\"\u003eIndependent Vertex Set\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/VertexCover.html\"\u003eVertex Cover\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/WheelGraph.html\"\u003eWheel Graph\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,0,-1)."
			],
			"formula": [
				"G.f.: (3-4*x)/((1-x)*(1-x-x^2)).",
				"a(n) = a(n-1) + a(n-2) - 1.",
				"a(n) = A000032(n) + 1.",
				"a(n) = A000071(n+3) - 2*A000071(n). - _Petros Hadjicostas_, Jan 11 2017"
			],
			"example": [
				"a(3) = 5 because the following cyclic sequences of length three avoid the pattern 001: 000, 011, 101, 110, 111. - _Petros Hadjicostas_, Jan 11 2017"
			],
			"maple": [
				"A001612:=-(-2+3*z**2)/(z-1)/(z**2+z-1); # conjectured by _Simon Plouffe_ in his 1992 dissertation; gives sequence except for the initial 3"
			],
			"mathematica": [
				"Join[{b=3},a=0;Table[c=a+b-1;a=b;b=c,{n,100}]] (* _Vladimir Joseph Stephan Orlovsky_, Mar 15 2011 *)",
				"Table[Fibonacci[n] + Fibonacci[n - 2] + 1, {n, 20}] (* _Eric W. Weisstein_, Mar 31 2017 *)",
				"LinearRecurrence[{2, 0, -1}, {3, 2, 4}, 20]] (* _Eric W. Weisstein_, Mar 31 2017 *)",
				"CoefficientList[Series[(3 - 4 x)/(1 - 2 x + x^3), {x, 0, 20}], x] (* _Eric W. Weisstein_, Sep 21 2017 *)"
			],
			"program": [
				"(PARI) a(n)=fibonacci(n+1)+fibonacci(n-1)+1",
				"(Haskell)",
				"a001612 n = a001612_list !! n",
				"a001612_list = 3 : 2 : (map (subtract 1) $",
				"   zipWith (+) a001612_list (tail a001612_list))",
				"-- _Reinhard Zumkeller_, May 26 2013"
			],
			"xref": [
				"Cf. A000032, A000071, A274017."
			],
			"keyword": "nonn,easy,hear",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Additional comments from _Michael Somos_, Jun 01 2000"
			],
			"references": 9,
			"revision": 106,
			"time": "2021-03-12T22:32:34-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}