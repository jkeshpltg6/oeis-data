{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A232630",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 232630,
			"data": "0,1,0,1,-3,0,1,0,1,5,0,-5,0,1,-3,0,1,-7,0,14,0,-7,0,1,-2,1,-3,0,9,0,-6,0,1,5,0,-5,0,1,-11,0,55,0,-77,0,44,0,-11,0,1,-3,0,1,13,0,-91,0,182,0,-156,0,65,0,-13,0,1,-7,0,14,0,-7,0,1,1,0,-8,0,14,0,-7,0,1,-2,0,1,17,0,-204,0,714,0,-1122,0,935,0,-442,0,119,0,-17,0,1",
			"name": "Coefficient table for the minimal polynomials of 2*sin(4*Pi/n). Rising powers of x.",
			"comment": [
				"The length of row n is A232626(n) + 1, that is 2, 2, 3, 2, 5, 3, 7, 2, 7, 5, 11, 3, 13, 7, 9, 3, 17, 7, 19, 5,...",
				"In a regular n-gon, n\u003e=2, inscribed in a circle of radius R (in some length units), 2*sin(4*Pi/n) = (S(n)/R)*(D(1,n)/S(n)) = D(1,n)/R, with the side length S(n) and the length of the first (smallest) diagonal D(1,n). For n=2 there is no such diagonal, and one can put D(1,2) = 0. Obviously, D(1,2*m) = S(m), m \u003e= 2.",
				"For the power basis representation of 2*sin(4*Pi/n) in the algebraic number field Q(rho(q(2,n))), with q(2,n)) = A232625(n) and rho(m) := 2*cos(Pi/m), see A232629. Call the  row polynomials of A232629 PB2(n,x) (power basis polynomial for the case k=2 in 2*sin(2*Pi*k/n)).",
				"The minimal polynomial of 2*sin(4*Pi/n), call it MP2(n, x), is obtained from the conjugates rho(q(2,n),j), j= 1, ... , delta(q(2,n)) = A232626(n), which are the zeros of C(q(2,n), x), the minimal polynomial of rho(q(2,n)) = rho(q(2,n),1) (for C see A187360). MP2(n, x) = product(x -   PB2(n, rho(q(2,n),j)), j=1..A232626(n)) (mod C(q(2,n), rho(q(2,n))))."
			],
			"formula": [
				"a(n,m) = [x^m] MP2(n, x), n\u003e=1, m = 0, 1, ..., A232626(n), with the minimal polynomials of 2*sin(4*Pi/n), computed like explained above in a comment.",
				"a(2*l,m) = A231188(l,m), m = 0, 1, ..., A093819(l), l \u003e= 1."
			],
			"example": [
				"The table a(n,m) begins:",
				"--------------------------------------------------------------------------------------",
				"n\\m   0  1    2  3     4  5     6  7      8  9   10 11   12 13   14 15   16 17 18 ...",
				"1:    0  1",
				"2:    0  1",
				"3:   -3  0    1",
				"4:    0  1",
				"5:    5  0   -5  0     1",
				"6:   -3  0    1",
				"7:   -7  0   14  0    -7  0     1",
				"8:   -2  1",
				"9:   -3  0    9  0    -6  0     1",
				"10:   5  0   -5  0     1",
				"11: -11  0   55  0   -77  0    44  0    -11  0   1",
				"12:  -3  0    1",
				"13:  13  0  -91  0   182  0  -156  0     65  0 -13  0  1",
				"14:  -7  0   14  0    -7  0     1",
				"15:   1  0   -8  0    14  0    -7  0      1",
				"16:  -2  0    1",
				"17:  17  0 -204  0   714  0 -1122  0    935  0 -442  0  119  0  -17  0    1",
				"18:  -3  0    9  0    -6  0     1",
				"19: -19  0  285  0 -1254  0  2508  0  -2717  0 1729  0 -665  0  152  0  -19  0  1",
				"20:   5  0   -5  0     1",
				"...",
				"n=1: 2*sin(4*Pi/1) = 0 is rational, therefore MP2(1, x) = x, with coefficients 0, 1, and degree A232626(1) = 1. PB2(1, rho(1,1)) = PB2(1, rho(1)) = 0.",
				"n=3: A232626(2) = 2. PB2(2, x) = -x, C(6, x) = x^2 - 3, with zeros rho(6) and R(5, rho(6)) (for R see A127672), hence rho(6,1) = rho(6) and rho(6,2) = R(5, rho(6))=  5*rho(6) - 5*rho(6)^3 + 1*rho(6)^5, MP2(3, x) = (x - (-rho(6)))*(x - (- R(5, rho(6))) reduced with rho(6)^2 = 3 leading to MP2(3, x) = -3 + x^2, yielding row n=3: -3  0  1.",
				"n=8: this row -2, 1 coincides with row n=4 of A231188.",
				"n=17: coincides with WolframAlpha's MinimalPolynomial[2*sin(4*Pi/17),x] = 17-204 x^2+714 x^4-1122 x^6+935 x^8-442 x^10+119 x^12-17 x^14+x^16."
			],
			"xref": [
				"Cf. A231188 (k=1 case), A187360 (C), A127672(R), A232626 (degree), A232629 (PB2)."
			],
			"keyword": "sign,tabf,easy",
			"offset": "1,5",
			"author": "_Wolfdieter Lang_, Dec 17 2013",
			"references": 1,
			"revision": 9,
			"time": "2013-12-18T19:56:45-05:00",
			"created": "2013-12-18T05:17:21-05:00"
		}
	]
}