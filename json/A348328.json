{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A348328",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 348328,
			"data": "0,1,1,2,1,0,3,2,3,2,2,0,4,3,3,4,4,3,0,5,4,3,4,6,5,2,1,0,7,5,4,5,6,6,4,3,1,0,8,6,5,5,7,7,6,5,3,1,0,9,7,6,5,8,7,8,6,5,3,1,0,10,8,7,5,9,7,10,7,8,5,2,2,0,11,9,7,7,9,7,11,7,12,5,4,2,2,1,0,13,10,8,9,9,8,12,7,13,7,6,3,2,2,2,0,14,11,8,12,10,8,12,8,14,10,6,5,3,4,2,2,0",
			"name": "Records inventory sequence: Record the number of record terms seen thus far, then the number of zero terms, then number of 1's, then 2's, etc until recording another zero term, at which point the count is repeated, as above.",
			"comment": [
				"The sequence proceeds in cycles (rows; see example) between consecutive zero terms. Each cycle starts with the number of records seen so far and then records the cardinality of 1's, 2's, etc, until reaching a number whose cardinality is zero, because it has not yet been observed in the sequence. At this point another zero is recorded, the count is reset, and repeated as above. Records may occur mid cycle (away from a zero). The first of these is 2 (count of 1's) seen in the 2nd cycle, the next is 6 (count of 3's) seen in the 5th. These become more common as the sequence progresses. The term immediately following a zero is the current number of records seen so far. It is the sum of the number of records which have occurred between zeros so far, and the number of zeros.",
				"Initially, the number of records is itself a record, and records over the first few hundred terms are the nonnegative integers. This changes at a(435) = 29, after which the next record is 31. Although records appear at approximately one per cycle, there are cycles later which exhibit no records.",
				"The scatter plot depicts a chaotic tangle of trajectories (cardinalities), in which the only apparent order is the appearance of quasi parallel closely set twin curves recording the cardinalities of 0 (lower), and records (upper). As n increases the upper of these curves separates further from the lower in a number of discrete steps which have not yet been explained."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A348328/a348328.png\"\u003eScatterplot of a(n)\u003c/a\u003e for n = 0..43057 (2^8 cycles), accentuating the trajectory of the records counter in red.",
				"Michael De Vlieger, \u003ca href=\"/A348328/a348328_1.png\"\u003eScatterplot of a(n)\u003c/a\u003e for n = 0..777315 (2^10 cycles), accentuating the trajectory of the records counter in red.",
				"Michael De Vlieger, \u003ca href=\"/A348328/a348328_2.png\"\u003eScatterplot of a(n)\u003c/a\u003e for n = 0..15009105 (2^12 cycles), accentuating the trajectory of the records counter in red.",
				"Michael De Vlieger, \u003ca href=\"/A348328/a348328_3.png\"\u003eScatterplot detail of a(n) \u003c 1152\u003c/a\u003e for n = 0..43057 (2^8 cycles), enlarging the dense part of the scatterplot.",
				"Michael De Vlieger, \u003ca href=\"/A348328/a348328_4.png\"\u003eScatterplot of a(n)\u003c/a\u003e for n = 0..43057 accentuating trajectories of the number of records in black, of 0's in red, 1's in orange, 2's in yellow, 3's in green, 4's in cyan, 5's in blue, and 6's in purple.",
				"\u003ca href=\"/index/In#inventory\"\u003eIndex entries for sequences related to the inventory sequence\u003c/a\u003e"
			],
			"example": [
				"a(0) = 0 because so far no terms have been seen, hence no records.",
				"Following zero term a(0) we reset the count and a(1) = 1 since a(0) = 0 is a record term.",
				"a(2) = 1, the number of 0's seen thus far,",
				"a(3) = 2, the number of 1's,",
				"a(4) = 1 the number of 2's,",
				"a(5) = 0 because no 3's have been seen thus far.",
				"Following a(5) = 0, we reset the count and record a(6) = 3, the number of record terms (0,1,2) seen thus far.",
				"a(7) = 2 since at this point the 2 zero terms have been counted; etc.",
				"As an irregular table the sequence starts:",
				"0;",
				"1, 1, 2, 1, 0;",
				"3, 2, 3, 2, 2, 0;",
				"4, 3, 3, 4, 4, 3, 0;",
				"5, 4, 3, 4, 6, 5, 2, 1, 0;",
				"7, 5, 4, 5, 6, 6, 4, 3, 1, 0;",
				"8, 6, 5, 5, 7, 7, 6, 5, 3, 1, 0;",
				"..."
			],
			"mathematica": [
				"Block[{c, k, m, r = 0}, c[-1] = 1; c[0] = 1; {0}~Join~Reap[Do[k = -1; While[IntegerQ[c[k]], Set[m, c[k]]; Sow[m]; If[IntegerQ@ c[m], c[m]++, c[m] = 1]; If[m \u003e r, Set[r, m]; c[-1]++]; k++]; Sow[0]; c[0]++, 11]][[-1, -1]]] (* _Michael De Vlieger_, Oct 14 2021 *)"
			],
			"xref": [
				"Cf. A342585, A349016, A347564."
			],
			"keyword": "nonn,tabf",
			"offset": "0,4",
			"author": "_David James Sycamore_, Oct 07 2021",
			"references": 1,
			"revision": 34,
			"time": "2021-10-28T07:09:06-04:00",
			"created": "2021-10-28T07:09:06-04:00"
		}
	]
}