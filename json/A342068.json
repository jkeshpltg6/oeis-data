{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A342068",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 342068,
			"data": "2,6,10,5,3,7,5,3,5,5,7,4,11,3,5,5,7,3,4,3,7,4,5,5,5,6,6,9,3,6,8,4,6,5,7,5,5,6,5,5,7,4,9,6,4,10,3,3,4,4,7,4,6,4,5,5,4,5,4,8,6,7,7,5,10,6,3,3,6,4,4,4,4,4,4,9,8,6,6,6,3,5,6,5,6,5",
			"name": "a(n) is the smallest k \u003e 1 such that there are more primes in the interval [(k-1)*n + 1, k*n] than there are in the interval [(k-2)*n + 1, (k-1)*n].",
			"comment": [
				"a(519) is a noteworthy record high value; a(n) \u003c 13 for all n \u003c 519, and a(n) \u003c 19 for all n \u003c 9363 except that a(519)=19."
			],
			"link": [
				"Jon E. Schoenfield, \u003ca href=\"/A342068/b342068.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"The 1st 100 positive integers,   1..100, include 25 primes;",
				"the 2nd 100 positive integers, 101..200, include 21 primes;",
				"the 3rd 100 positive integers, 201..300, include 16 primes;",
				"the 4th 100 positive integers, 301..400, include 16 primes;",
				"the 5th 100 positive integers, 401..500, include 17 primes.",
				"The sequence 25, 21, 16, 16, 17, is nonincreasing until we reach the 5th term, 17, so a(100) = 5.",
				"Considering the positive integers in consecutive intervals of length 519, instead (i.e., [1,519], [2,1038], [3,1557], ...) and counting the primes in each interval, we get a sequence that is nonincreasing until we reach the 19th term, since the 19th interval, [9343,9861], contains more primes than does the 18th, so a(519)=19."
			],
			"maple": [
				"a:= proc(n) uses numtheory; local i, j, k; i:= n;",
				"      for k do j:= pi(k*n)-pi((k-1)*n);",
				"        if j\u003ei then break else i:=j fi",
				"      od; k",
				"    end:",
				"seq(a(n), n=1..100);  # _Alois P. Heinz_, Mar 21 2021"
			],
			"mathematica": [
				"a[n_] := Module[{i = n, j, k},",
				"     For[k = 1, True, k++, j = PrimePi[k*n] - PrimePi[(k-1)*n];",
				"     If[j \u003e i, Break[], i = j]]; k];",
				"Array[a, 100] (* _Jean-François Alcover_, Jul 01 2021, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(Python)",
				"from sympy import primepi",
				"def A342068(n):",
				"    k, a, b, c = 2,0,primepi(n),primepi(2*n)",
				"    while a+c \u003c= 2*b:",
				"        k += 1",
				"        a, b, c = b, c, primepi(k*n)",
				"    return k # _Chai Wah Wu_, Mar 25 2021"
			],
			"xref": [
				"Cf. A000040, A342069, A342070, A342071, A342839, A342852."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Jon E. Schoenfield_, Mar 21 2021",
			"references": 6,
			"revision": 33,
			"time": "2021-07-01T06:24:57-04:00",
			"created": "2021-03-22T02:56:42-04:00"
		}
	]
}