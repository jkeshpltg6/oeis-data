{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A225798",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 225798,
			"data": "1,2,5,12,36,96,311,886,3000,8944,31192,96138,342562,1083028,3923351,12656024,46455770,152325850,565212506,1878551444,7033866580,23645970022,89222991344,302879546290,1150480017950,3938480377496,15047312553918,51892071842570,199274492098480,691680497233180",
			"name": "The number of idempotents in the Jones (or Temperley-Lieb) monoid on the set [1..n].",
			"comment": [
				"The Jones monoid is the set of partitions on [1..2n] with classes of size 2, which can be drawn as a planar graph, and multiplication inherited from the Brauer monoid, which contains the Jones monoid as a subsemigroup. The multiplication is defined in Halverson and Ram.",
				"These numbers were produced using the Semigroups (2.0) package for GAP 4.7.",
				"No general formula is known for the number of idempotents in the Jones monoid."
			],
			"link": [
				"Attila Egri-Nagy, Nick Loughlin, and James Mitchell \u003ca href=\"/A225798/b225798.txt\"\u003eTable of n, a(n) for n = 1..30\u003c/a\u003e (a(1) to a(21) from Attila Egri-Nagy, a(22)-a(24) from Nick Loughlin, a(25)-a(30) from James Mitchell)",
				"I. Dolinka, J. East, A. Evangelou, D. FitzGerald, N. Ham, et al., \u003ca href=\"http://arxiv.org/abs/1408.2021\"\u003eEnumeration of idempotents in diagram semigroups and algebras\u003c/a\u003e, arXiv preprint arXiv:1408.2021 [math.GR], 2014.",
				"I. Dolinka, J. East et al, \u003ca href=\"http://arxiv.org/abs/1507.04838\"\u003eIdempotent Statistics of the Motzkin and Jones Monoids\u003c/a\u003e, arXiv:1507.04838 [math.CO], 2015. Table 4 and 5.",
				"T. Halverson, A. Ram, \u003ca href=\"http://dx.doi.org/10.1016/j.ejc.2004.06.005\"\u003ePartition algebras\u003c/a\u003e, European J. Combin. 26 (6) (2005) 869-921.",
				"J. D. Mitchell et al., \u003ca href=\"https://gap-packages.github.io/Semigroups/\"\u003eSemigroups\u003c/a\u003e package for GAP."
			],
			"program": [
				"(GAP) for i in [1..18] do",
				"Print(NrIdempotents(JonesMonoid(i)), \"\\n\");",
				"od;"
			],
			"xref": [
				"Cf. A000108, A227545, A225797."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_James Mitchell_, Jul 27 2013",
			"ext": [
				"a(20)-a(21) from _Attila Egri-Nagy_, Sep 12 2014",
				"a(22)-a(24) from _Nick Loughlin_, Jan 23 2015",
				"a(25)-a(30) from _James Mitchell_, May 21 2016"
			],
			"references": 4,
			"revision": 50,
			"time": "2016-06-02T03:29:10-04:00",
			"created": "2013-07-30T16:07:37-04:00"
		}
	]
}