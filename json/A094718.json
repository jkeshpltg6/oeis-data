{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A094718",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 94718,
			"data": "0,1,0,1,1,0,1,2,1,0,1,2,2,1,0,1,2,3,4,1,0,1,2,3,5,4,1,0,1,2,3,6,8,8,1,0,1,2,3,6,9,13,8,1,0,1,2,3,6,10,18,21,16,1,0,1,2,3,6,10,19,27,34,16,1,0,1,2,3,6,10,20,33,54,55,32,1,0,1,2,3,6,10,20,34,61,81,89,32,1",
			"name": "Array T read by antidiagonals: T(n,k) is the number of involutions avoiding 132 and 12...k.",
			"comment": [
				"Also, number of paths along a corridor with width k, starting from one side (from H. Bottomley's comment in A061551).",
				"Rows converge to binomial(n,floor(n/2)) (A001405).",
				"Note that the rows and columns start at 1, which for example obscures the fact that the first row refers to A000007 and not to A000004. A better choice is the indexing 0 \u003c= k and 0 \u003c= n. The Maple program below uses this indexing and builds only on the roots of -1. - _Peter Luschny_, Sep 17 2020"
			],
			"link": [
				"Shaun V. Ault and Charles Kicey, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2014.05.020\"\u003eCounting paths in corridors using circular Pascal arrays\u003c/a\u003e, Discrete Mathematics, Volume 332, October 2014, Pages 45-54.",
				"Nachum Dershowitz, \u003ca href=\"https://arxiv.org/abs/2006.06516\"\u003eBetween Broadway and the Hudson\u003c/a\u003e, arXiv:2006.06516 [math.CO], 2020.",
				"Robert Dutton Fray and David Paul Roselle, \u003ca href=\"https://projecteuclid.org/download/pdf_1/euclid.pjm/1102970745\"\u003eWeighted lattice paths\u003c/a\u003e, Pacific Journal of Mathematics, 37(1) (1971), 85-96.",
				"O. Guibert and T. Mansour, \u003ca href=\"http://www.mat.univie.ac.at/~slc/wpapers/s48guimans.html\"\u003eRestricted 132-involutions\u003c/a\u003e, Séminaire Lotharingien de Combinatoire, B48a (2002), 23 pp.",
				"T. Mansour, \u003ca href=\"https://arxiv.org/abs/math/0302014\"\u003eRestricted even permutations and Chebyshev polynomials\u003c/a\u003e, arXiv:math/0302014 [math.CO], 2003."
			],
			"formula": [
				"G.f. for k-th row: 1/(x*U(k, 1/(2*x))) * Sum_{j=0..k-1} U(j, 1/(2*x)), with U(k, x) the Chebyshev polynomials of second kind. [Clarified by _Jean-François Alcover_, Nov 17 2018]",
				"T(n, k) = (1/(n+1))*Sum_{j=1..n, j odd} (2 + [j, n]) * [j, n]^k where [j, n] := (-1)^(j/(n+1)) - (-1)^((n-j+1)/(n+1)). - _Peter Luschny_, Sep 17 2020"
			],
			"example": [
				"Array begins",
				"  0   0   0   0   0   0   0   0   0   0 ...",
				"  1   1   1   1   1   1   1   1   1   1 ...",
				"  1   2   2   4   4   8   8  16  16  32 ...",
				"  1   2   3   5   8  13  21  34  55  89 ...",
				"  1   2   3   6   9  18  27  54  81 162 ...",
				"  1   2   3   6  10  19  33  61 108 197 ...",
				"  1   2   3   6  10  20  34  68 116 232 ...",
				"  1   2   3   6  10  20  35  69 124 241 ...",
				"  1   2   3   6  10  20  35  70 125 250 ...",
				"  1   2   3   6  10  20  35  70 126 251 ...",
				"  ..."
			],
			"maple": [
				"X := (j, n) -\u003e (-1)^(j/(n+1)) - (-1)^((n-j+1)/(n+1)):",
				"R := n -\u003e select(k -\u003e type(k, odd), [$(1..n)]):",
				"T := (n, k) -\u003e add((2 + X(j,n))*X(j,n)^k, j in R(n))/(n+1):",
				"seq(lprint([n], seq(simplify(T(n, k)), k=0..10)), n=0..9); # _Peter Luschny_, Sep 17 2020"
			],
			"mathematica": [
				"U = ChebyshevU;",
				"m = maxExponent = 14;",
				"row[1] = Array[0\u0026, m];",
				"row[k_] := 1/(x U[k, 1/(2x)])*Sum[U[j, 1/(2x)], {j, 0, k-1}] + O[x]^m // CoefficientList[#, x]\u0026 // Rest;",
				"T = Table[row[n], {n, 1, m}];",
				"Table[T[[n-k+1, k]], {n, 1, m-1}, {k, 1, n}] // Flatten (* _Jean-François Alcover_, Nov 17 2018 *)"
			],
			"xref": [
				"Rows 3-8 are A016116, A000045, A038754, A028495, A030436, A061551.",
				"Main diagonal is A014495, antidiagonal sums are in A094719.",
				"Cf. A080934 (permutations)."
			],
			"keyword": "nonn,tabl",
			"offset": "1,8",
			"author": "_Ralf Stephan_, May 23 2004",
			"references": 14,
			"revision": 42,
			"time": "2020-10-09T09:08:30-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}