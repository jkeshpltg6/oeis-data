{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A240130",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 240130,
			"data": "5,13,29,53,137,173,293,397,593,857,977,1373,1697,1913,2213,2909,3517,3821,4493,5077,5333,6257,7213,7937,9413,10301,10613,11549,11897,13093,16193,17417,18773,19421,22397,22817,24749,26573,27893,30029",
			"name": "Least prime of the form prime(n)^2 + k^2, or 0 if none.",
			"comment": [
				"The positive terms form a subsequence of A185086 = Fouvry-Iwaniec primes = primes of the form prime^2 + integer^2.",
				"The values of k are A240131.",
				"Is a(n) \u003c a(n+1) for all n? (I have checked it for n \u003c= 10^6.) Note that A240131 is far from being monotone."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A240130/b240130.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Stephan Baier and Liangyi Zhao, \u003ca href=\"http://arxiv.org/abs/math/0703284\"\u003eOn Primes Represented by Quadratic Polynomials\u003c/a\u003e, Anatomy of Integers, CRM Proc. \u0026 Lecture Notes, Vol. 46, Amer. Math. Soc. 2008, pp. 169 - 166.",
				"Étienne Fouvry and Henryk Iwaniec, \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/aa/aa79/aa7935.pdf\"\u003eGaussian primes\u003c/a\u003e, Acta Arithmetica 79:3 (1997), pp. 249-287.",
				"E.W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/Fermats4nPlus1Theorem.html\"\u003eFermat's 4n+1 Theorem\u003c/a\u003e, MathWorld.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Bunyakovsky_conjecture\"\u003eBunyakovsky's conjecture\u003c/a\u003e"
			],
			"formula": [
				"a(n) == 1 (mod 4) if a(n) \u003e 0.",
				"a(n) \u003e 0 if Bunyakovsky's conjecture is true.",
				"a(n) \u003c\u003e a(m) if n \u003c\u003e m and a(n) \u003e 0, by uniqueness in Fermat's 4n+1 Theorem.",
				"a(n) = prime(n)^2 + A240131(n)^2 if a(n) \u003e 0."
			],
			"example": [
				"Prime(2) = 3 and 3^2 + 1^2 = 10 is not prime but 3^2 + 2^2 = 13 is prime, so a(2) = 13."
			],
			"maple": [
				"g:= proc(p) local k; for k from 2 by 2 do if isprime(p^2 + k^2) then return p^2+k^2 fi od end proc:",
				"g(2):= 5:",
				"seq(g(ithprime(i)),i=1..1000); # _Robert Israel_, Nov 04 2015"
			],
			"mathematica": [
				"Table[First[Select[Prime[n]^2 + Range[20]^2, PrimeQ]], {n, 40}]"
			],
			"program": [
				"(PARI) a(n) = {p = prime(n); k = 1 - p%2; inc = 2; while (!isprime(q=p^2+k^2), k += inc); q;} \\\\ _Michel Marcus_, Nov 04 2015"
			],
			"xref": [
				"Cf. A002144, A185086."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Jonathan Sondow_, Apr 07 2014",
			"references": 9,
			"revision": 16,
			"time": "2015-11-05T02:11:31-05:00",
			"created": "2014-04-07T15:58:08-04:00"
		}
	]
}