{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A154916",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 154916,
			"data": "4,5,5,13,24,13,35,120,120,35,97,546,1008,546,97,275,2310,7200,7200,2310,275,793,9312,44928,77760,44928,9312,793,2315,36300,255780,703080,703080,255780,36300,2315,6817,137982,1372356,5660928,8817984,5660928,1372356,137982,6817",
			"name": "Triangle T(n,m,p,q) = (p^(n-k)*q^k + p^k*q^(n-k))*(StirlingS2(n, k) + StirlingS2(n, n-k)) with p=2 and q=3, read by rows.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A154916/b154916.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/BellPolynomial.html\"\u003eBell Polynomial\u003c/a\u003e"
			],
			"formula": [
				"T(n,m,p,q) = (p^(n-k)*q^k + p^k*q^(n-k))*(StirlingS2(n, k) + StirlingS2(n, n-k)) with p=2 and q=3.",
				"Sum_{k=0..n} T(n,k,p,q) = 2*p^n*( T_{n}(q/p) + (q/p)^n*T_{n}(p/q) ), with p=2 and q=3, where T_{n}(x) are the Touchard polynomials (sometimes named Bell polynomials). - _G. C. Greubel_, Mar 02 2021"
			],
			"example": [
				"Triangle begins as:",
				"     4;",
				"     5,      5;",
				"    13,     24,      13;",
				"    35,    120,     120,      35;",
				"    97,    546,    1008,     546,      97;",
				"   275,   2310,    7200,    7200,    2310,     275;",
				"   793,   9312,   44928,   77760,   44928,    9312,     793;",
				"  2315,  36300,  255780,  703080,  703080,  255780,   36300,   2315;",
				"  6817, 137982, 1372356, 5660928, 8817984, 5660928, 1372356, 137982, 6817;"
			],
			"maple": [
				"A154916:= (n, k, p, q) -\u003e (p^(n-k)*q^k + p^k*q^(n-k))*(Stirling2(n, k) + Stirling2(n, n-k)):",
				"seq(seq(A154916(n,k,2,3), k=0..n), n=0..12); # _G. C. Greubel_, Mar 02 2021"
			],
			"mathematica": [
				"T[n_, k_, p_, q_]:= (p^(n-k)*q^k + p^k*q^(n-k))*(StirlingS2[n, k] + StirlingS2[n, n-k]);",
				"Table[T[n, k, 2, 3], {n,0,12}, {k,0,n}]//Flatten (* modified by _G. C. Greubel_, Mar 02 2021 *)"
			],
			"program": [
				"(Sage)",
				"def A154916(n,k,p,q): return (p^(n-k)*q^k + p^k*q^(n-k))*(stirling_number2(n, k) + stirling_number2(n, n-k))",
				"flatten([[A154916(n,k,2,3) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Mar 02 2021",
				"(Magma)",
				"A154916:= func\u003c n,k,p,q | (p^(n-k)*q^k + p^k*q^(n-k))*(StirlingSecond(n, k) + StirlingSecond(n, n-k)) \u003e;",
				"[A154916(n,k,2,3): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Mar 02 2021"
			],
			"xref": [
				"Cf. A154915 (q=1), this sequence (q=3), A154922 (q=5).",
				"Cf. A008277, A048993, A154913, A154914."
			],
			"keyword": "nonn,tabl,easy,less",
			"offset": "0,1",
			"author": "_Roger L. Bagula_, Jan 17 2009",
			"ext": [
				"Edited by _G. C. Greubel_, Mar 02 2021"
			],
			"references": 5,
			"revision": 13,
			"time": "2021-03-02T18:45:03-05:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}