{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A210565",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 210565,
			"data": "1,2,2,3,5,3,4,9,10,5,5,14,22,20,8,6,20,40,51,38,13,7,27,65,105,111,71,21,8,35,98,190,256,233,130,34,9,44,140,315,511,594,474,235,55,10,54,192,490,924,1295,1324,942,420,89,11,65,255,726,1554,2534,3130,2860,1836,744,144",
			"name": "Triangle of coefficients of polynomials u(n,x) jointly generated with A210595; see the Formula section.",
			"comment": [
				"Row n starts with n and ends with F(n+1), where F=A000045 (Fibonacci numbers).",
				"Row sums: A005409.",
				"Alternating row sums: 1,0,1,0,1,0,1,0,1,0,1,0, ...",
				"For a discussion and guide to related arrays, see A208510."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A210565/b210565.txt\"\u003eRows n = 1..30 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"u(n,x) = x*u(n-1,x) + (x+1)*v(n-1,x) + 1,",
				"v(n,x) = x*u(n-1,x) + v(n-1,x) + 1,",
				"where u(1,x) = 1, v(1,x) = 1.",
				"T(n, k) = [x^k]( u(n, x) ), where u(n, x) = (1+x)*u(n-1,x) + x^2*u(n-2,x) + 1 + x, u(1, x) = 1, and u(2, x) = 2 + 2*x. - _G. C. Greubel_, May 24 2021"
			],
			"example": [
				"First five rows:",
				"  1;",
				"  2,  2;",
				"  3,  5,  3;",
				"  4,  9, 10,  5;",
				"  5, 14, 22, 20, 8;",
				"First three polynomials u(n,x):",
				"u(1, x) = 1;",
				"u(2, x) = 2 + 2*x;",
				"u(3, x) = 3 + 5*x + 3*x^2."
			],
			"mathematica": [
				"(* First program *)",
				"u[1, x_]:= 1; v[1, x_]:= 1; z = 16;",
				"u[n_, x_]:= x*u[n-1, x] + (x+1)*v[n-1, x] + 1;",
				"v[n_, x_]:= x*u[n-1, x] + v[n-1, x] + 1;",
				"Table[Expand[u[n, x]], {n, 1, z/2}]",
				"Table[Expand[v[n, x]], {n, 1, z/2}]",
				"cu = Table[CoefficientList[u[n, x], x], {n, 1, z}];",
				"TableForm[cu]",
				"Flatten[%]    (* A210565 *)",
				"Table[Expand[v[n, x]], {n, 1, z}]",
				"cv = Table[CoefficientList[v[n, x], x], {n, 1, z}];",
				"TableForm[cv]",
				"Flatten[%]    (* A210595 *)",
				"(* Second program *)",
				"u[n_, x_]:= u[n, x]= If[n\u003c2, (n+1)*(1+x)^n, (1+x)*u[n-1, x] +x^2*u[n-2, x] +1+x];",
				"T[n_]:= CoefficientList[Series[u[n, x], {x, 0, n}], x];",
				"Table[T[n-1], {n,12}] (* _G. C. Greubel_, May 23 2021 *)"
			],
			"program": [
				"(Sage)",
				"@CachedFunction",
				"def u(n,x): return (n+1)*(1+x)^n if (n\u003c2) else (1+x)*u(n-1,x) + x^2*u(n-2,x) +1+x",
				"def T(n): return taylor( u(n,x) , x,0,n).coefficients(x, sparse=False)",
				"flatten([T(n-1) for n in (1..12)]) # _G. C. Greubel_, May 23 2021"
			],
			"xref": [
				"Cf. A208510, A210595."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Mar 23 2012",
			"references": 3,
			"revision": 12,
			"time": "2021-05-25T01:37:47-04:00",
			"created": "2012-03-24T18:19:29-04:00"
		}
	]
}