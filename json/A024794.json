{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A024794",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 24794,
			"data": "0,0,0,0,0,0,0,0,0,1,1,2,3,5,7,11,15,22,30,43,57,79,104,140,183,242,312,407,520,670,849,1081,1359,1715,2141,2678,3322,4125,5085,6274,7691,9430,11502,14025,17024,20655,24959,30140,36270,43612,52274,62604,74763",
			"name": "Number of 10's in all partitions of n.",
			"comment": [
				"The sums of ten successive terms give A000070. - _Omar E. Pol_, Jul 12 2012",
				"a(n) is also the difference between the sum of 10th largest and the sum of 11th largest elements in all partitions of n. - _Omar E. Pol_, Oct 25 2012",
				"In general, if m\u003e0 and a(n+m)-a(n) = A000041(n), then a(n) ~ exp(sqrt(2*n/3)*Pi) / (2*Pi*m*sqrt(2*n)) * (1 - Pi*(1/24 + m/2)/sqrt(6*n) + (1/48 + Pi^2/6912 + m/4 + m*Pi^2/288 + m^2*Pi^2/72)/n). - _Vaclav Kotesovec_, Nov 05 2016"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A024794/b024794.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A181187(n,10) - A181187(n,11). - _Omar E. Pol_, Oct 25 2012",
				"From _Peter Bala_, Dec 26 2013: (Start)",
				"a(n+10) - a(n) = A000041(n). a(n) + a(n+5) = A024789(n).",
				"a(n) + a(n+2) + a(n+4) + a(n+6) + a(n+8) = A024786(n).",
				"O.g.f.: x^10/(1 - x^10) * product {k \u003e= 1} 1/(1 - x^k) = x^10 + x^11 + 2*x^12 + 3*x^13 + ....",
				"Asymptotic result: log(a(n)) ~ 2*sqrt(Pi^2/6)*sqrt(n) as n -\u003e inf. (End)",
				"a(n) ~ exp(Pi*sqrt(2*n/3)) / (20*Pi*sqrt(2*n)) * (1 - 121*Pi/(24*sqrt(6*n)) + (121/48 + 9841*Pi^2/6912)/n). - _Vaclav Kotesovec_, Nov 05 2016"
			],
			"maple": [
				"b:= proc(n, i) option remember; local g;",
				"      if n=0 or i=1 then [1, 0]",
				"    else g:= `if`(i\u003en, [0$2], b(n-i, i));",
				"         b(n, i-1) +g +[0, `if`(i=10, g[1], 0)]",
				"      fi",
				"    end:",
				"a:= n-\u003e b(n, n)[2]:",
				"seq(a(n), n=1..100);  # _Alois P. Heinz_, Oct 27 2012"
			],
			"mathematica": [
				"Table[ Count[ Flatten[ IntegerPartitions[n]], 10], {n, 1, 55} ]",
				"b[n_, i_] := b[n, i] = Module[{g}, If[n == 0 || i == 1, {1, 0}, g = If[i \u003e n, {0, 0}, b[n - i, i]]; b[n, i - 1] + g + {0, If[i == 10, g[[1]], 0]}]]; a[n_] := b[n, n][[2]]; Table[a[n], {n, 1, 100}] (* _Jean-François Alcover_, Oct 09 2015, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A066633, A000070(n-1), A024786, A024787, A024788, A024789, A024790, A024791, A024792, A024793, A000041."
			],
			"keyword": "nonn,easy",
			"offset": "1,12",
			"author": "_Clark Kimberling_",
			"references": 13,
			"revision": 37,
			"time": "2016-11-05T10:25:50-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}