{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A112329",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 112329,
			"data": "1,0,2,1,2,0,2,2,3,0,2,2,2,0,4,3,2,0,2,2,4,0,2,4,3,0,4,2,2,0,2,4,4,0,4,3,2,0,4,4,2,0,2,2,6,0,2,6,3,0,4,2,2,0,4,4,4,0,2,4,2,0,6,5,4,0,2,2,4,0,2,6,2,0,6,2,4,0,2,6,5,0,2,4,4,0,4,4,2,0,4,2,4,0,4,8,2,0,6,3,2,0,2,4,8",
			"name": "Number of divisors of n if n odd, number of divisors of n/4 if n divisible by 4, otherwise 0.",
			"comment": [
				"First occurrence of k: 2, 1, 3, 9, 15, 64, 45, 256, 96, 144, 192, 4096, 240, ????, 768, 576, 480, ????, 720, ..., . See A246063. (* _Robert G. Wilson v_, Oct 31 2013 *)",
				"a(n) is the number of pairs (u, v) in NxZ satisfying u^2-v^2=n. See Kühleitner. - _Michel Marcus_, Jul 30 2017"
			],
			"reference": [
				"G. H. Hardy, Ramanujan: twelve lectures on subjects suggested by his life and work, AMS Chelsea Publishing, Providence, Rhode Island, 2002, p. 142."
			],
			"link": [
				"Ray Chandler, \u003ca href=\"/A112329/b112329.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"M. Kühleitner, \u003ca href=\"https://www.emis.de/journals/AMUC/_vol-61/_no_1/_kuhleit/kuhleitn.html\"\u003eAn Omega Theorem on Differences of Two Squares\u003c/a\u003e, Acta Mathematica Universitatis Comenianae, Vol. 61, 1 (1992) pp. 117-123. See Lemma 1 p. 2.",
				"N. J. A. Sloane et al., \u003ca href=\"https://oeis.org/wiki/Binary_Quadratic_Forms_and_OEIS\"\u003eBinary Quadratic Forms and OEIS\u003c/a\u003e (Index to related sequences, programs, references)"
			],
			"formula": [
				"Multiplicative with a(2^e) = e-1 if e\u003e0, a(p^e) = 1+e if p\u003e2.",
				"G.f.: Sum_{k\u003e0} x^k / (1 - (-x)^k) = Sum_{k\u003e0} -(-x)^k / (1 + (-x)^k).",
				"Möbius transform is period 4 sequence [ 1, -1, 1, 1, ...].",
				"sum(k\u003e=1, x^(k^2) * (1+x^(2*k))/(1-x^(2*k)) ). - _Joerg Arndt_, Nov 08 2010",
				"a(4*n + 2) = 0. a(n) = -(-1)^n * A048272(n). a(2*n - 1) = A099774(n). a(4*n) = A000005(n). a(4*n + 1) = A000005(4*n + 1). a(4*n - 1) = 2 * A078703(n).",
				"a(n) = A094572(n) / 2. - _Ray Chandler_, Aug 23 2014",
				"Bisection: a(2*k-1) = A000005(2*k-1), a(2*k) = A183063(2*k) - A001227(2*k), k \u003e= 1. See the Hardy reference, p. 142 where a(n) = sigma^*_0(n). - _Wolfdieter Lang_, Jan 07 2017",
				"a(n) = d(n) - 2*d(n/2) + 2*d(n/4) where d(n) = 0 if n is not an integer. See Kühleitner."
			],
			"example": [
				"x + 2*x^3 + x^4 + 2*x^5 + 2*x^7 + 2*x^8 + 3*x^9 + 2*x^11 + 2*x^12 + ..."
			],
			"maple": [
				"f:= proc(n) if n::odd then numtheory:-tau(n) elif n mod 4 = 0 then numtheory:-tau(n/4) else 0 fi end proc;",
				"seq(f(i),i=1..100); # _Robert Israel_, Aug 24 2014"
			],
			"mathematica": [
				"Rest[ CoefficientList[ Series[ Sum[x^k/(1 - (-x)^k), {k, 111}], {x, 0, 110}], x]] (* _Robert G. Wilson v_, Sep 20 2005 *)",
				"Table[If[OddQ[n],DivisorSigma[0,n],If[OddQ[n/2],0,DivisorSigma[0,n/4]]],{n,100} ] (* _Ray Chandler_, Aug 23 2014 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, 0, (-1)^n * sumdiv( n, d, (-1)^d))}",
				"(PARI) {a(n) = if( n\u003c1, 0, if( n%2, numdiv(n), if( n%4, 0, numdiv(n/4))))} /* _Michael Somos_, Sep 02 2006 */",
				"(PARI) d(n) = if (denominator(n)==1, numdiv(n), 0);",
				"a(n) = numdiv(n) - 2*d(n/2) + 2*d(n/4); \\\\ _Michel Marcus_, Jul 30 2017"
			],
			"xref": [
				"Cf. A000005, A001227, A048272, A078703, A094572, A099774, A183063."
			],
			"keyword": "nonn,mult",
			"offset": "1,3",
			"author": "_Michael Somos_, Sep 04 2005",
			"references": 7,
			"revision": 44,
			"time": "2017-08-02T04:55:24-04:00",
			"created": "2005-09-21T03:00:00-04:00"
		}
	]
}