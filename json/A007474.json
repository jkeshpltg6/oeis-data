{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007474",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7474,
			"id": "M1800",
			"data": "1,0,1,2,7,36,300,3218,42335,644808,11119515,213865382,4537496680,105270612952,2651295555949,72042968876506,2100886276796969,65446290562491916,2169090198219290966,76211647261082309466,2829612806029873399561",
			"name": "Number of circular chord diagrams with n chords, up to rotational symmetry.",
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Gheorghe Coserea, \u003ca href=\"/A007474/b007474.txt\"\u003eTable of n, a(n) for n = 0..300\u003c/a\u003e",
				"Dror Bar-Natan, \u003ca href=\"https://www.math.toronto.edu/drorbn/papers/OnVassiliev/\"\u003eOn the Vassiliev Knot Invariants\u003c/a\u003e, Topology 34 (1995) 423-472.",
				"D. Bar-Natan, \u003ca href=\"http://www.pdmi.ras.ru/~duzhin/VasBib/\"\u003eBibliography of Vassiliev Invariants\u003c/a\u003e",
				"E. Krasko, A. Omelchenko, \u003ca href=\"http://arxiv.org/abs/1601.05073\"\u003eEnumeration of Chord Diagrams without Loops and Parallel Chords\u003c/a\u003e, arXiv preprint arXiv:1601.05073 [math.CO], 2016.",
				"E. Krasko, A. Omelchenko, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v24i3p43\"\u003eEnumeration of Chord Diagrams without Loops and Parallel Chords\u003c/a\u003e, The Electronic Journal of Combinatorics, 24(3) (2017), #P3.43."
			],
			"mathematica": [
				"m = 20; Clear[M]; M[_, _] = 0; Mget[n_, k_] := Which[n \u003c 0, 0, n == 0, 1, n == 1, 1 - Mod[k, 2], n == 2, k - Mod[k, 2], True, M[n, k]]; Mset[n_, k_, v_] := (M[n, k] = v); Minit[] = (tmp = 0; For[n = 3, n \u003c= 2*m, n++, For[k = 1, k \u003c= 2*m, k++, tmp = If[Mod[k, 2] == 1, k*(n-1)*Mget[n-2, k] + Mget[n-4, k], Mget[n-1, k] + k*(n-1) * Mget[n-2, k] - Mget[n-3, k] + Mget[n-4, k]]; Mset[n, k, tmp]]];); a[n_] := DivisorSum[2*n, EulerPhi[#] * (Mget[2*n/#, #] - Mget[2*n/# - 2, #])\u0026] / (2*n); Minit[]; Prepend[ Array[a, m], 1] (* _Jean-François Alcover_, Apr 24 2017, after _Gheorghe Coserea_ *)"
			],
			"program": [
				"(PARI)",
				"N = 20; M = matrix(2*N, 2*N);",
				"Mget(n,k) = { if (n\u003c0, 0, n==0, 1, n==1, 1-(k%2), n==2, k-(k%2), M[n,k]) };",
				"Mset(n,k,v) = { M[n,k] = v;};",
				"Minit() = {",
				"  my(tmp = 0);",
				"  for (n=3, 2*N, for(k=1, 2*N,",
				"    tmp = if (k%2, k*(n-1) * Mget(n-2, k) + Mget(n-4, k),",
				"    Mget(n-1, k) + k*(n-1) * Mget(n-2, k) - Mget(n-3, k) + Mget(n-4, k));",
				"    Mset(n, k, tmp)));",
				"};",
				"a(n) = sumdiv(2*n, d, eulerphi(d) * (Mget(2*n/d, d) - Mget(2*n/d-2, d))) / (2*n);",
				"Minit();",
				"concat(1, vector(N, n, a(n)))  \\\\ _Gheorghe Coserea_, Dec 10 2016"
			],
			"keyword": "nonn,nice",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_",
			"references": 10,
			"revision": 40,
			"time": "2021-12-17T11:14:16-05:00",
			"created": "1994-09-19T03:00:00-04:00"
		}
	]
}