{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A196667",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 196667,
			"data": "109,113,139,181,197,199,241,271,281,283,293,313,317,443,449,461,463,467,479,491,503,509,523,619,643,647,653,659,661,673,677,683,691,701,761,769,773,829,859,863,883,887,1033,1039,1049,1051,1061,1063,1069,1091,1093,1097",
			"name": "The Chebyshev primes of index 1.",
			"comment": [
				"The sequence consists of the odd prime numbers p that satisfy li[psi(p)]-li[psi(p-1)]\u003c1, where li(x) is the logarithmic integral and psi(x) is the Chebyshev's psi function."
			],
			"link": [
				"Dana Jacobsen, \u003ca href=\"/A196667/b196667.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"M. Planat and P. Solé, \u003ca href=\"http://arxiv.org/abs/1109.6489\"\u003eEfficient prime counting and the Chebyshev primes\u003c/a\u003e arXiv:1109.6489 [math.NT], 2011.",
				"L. Schoenfeld, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1976-0457374-X\"\u003eSharper bounds for the Chebyshev functions theta(x) and psi(x). II\u003c/a\u003e, Math. Comp. 30 (1975) 337-360."
			],
			"maple": [
				"PlanatSole := proc(n,r) local j, p, pr, psi, L; L := NULL;",
				"psi := n -\u003e add(log(i/ilcm(op(numtheory[divisors](i) minus {1,i}))),i=1..n);",
				"for j in [$3..n] do p := ithprime(j); pr := p^r;",
				"if evalf(Li(psi(pr))-Li(psi(pr-1))) \u003c 1/r then L:= L,p fi od; L end:",
				"A196667 := n -\u003e PlanatSole(n,1); # _Peter Luschny_, Oct 23 2011"
			],
			"mathematica": [
				"ChebyshevPsi[n_] := Log[LCM @@ Range[n]];",
				"Reap[Do[If[LogIntegral[ChebyshevPsi[p]] - LogIntegral[ChebyshevPsi[p - 1]] \u003c 1, Sow[p]], {p, Prime[Range[2, 200]]}]][[2, 1]] (* _Jean-François Alcover_, Nov 17 2017, updated Dec 06 2018 *)"
			],
			"program": [
				"(MAGMA)",
				"Mangoldt:=function(n);",
				"if #Factorization(n) eq 1 then return Log(Factorization(n)[1][1]); else return 0; end if;",
				"end function;",
				"tcheb:=function(n);",
				"x:=0;",
				"for i in [1..n] do",
				"x:=x+Mangoldt(i);",
				"end for;",
				"return(x);",
				"end function;",
				"jump1:=function(n);",
				"x:=LogIntegral(tcheb(NthPrime(n)))-LogIntegral(tcheb(NthPrime(n)-1));",
				"return x;",
				"end function;",
				"Set1:=[];",
				"for i in [2..1000] do",
				"if jump1(i)-1 lt 0 then Set1:=Append(Set1,NthPrime(i)); NthPrime(i); end if;",
				"end for;",
				"Set1;",
				"(Sage)",
				"from mpmath import mp, mangoldt",
				"mp.dps = 25;",
				"def psi(n) :",
				"    return sum(mangoldt(i) for i in (1..n))",
				"def PlanatSole(n,r) :",
				"    P = Primes(); L = []",
				"    for j in (2..n):",
				"        p = P.unrank(j)",
				"        pr = p^r",
				"        if Li(psi(pr)) - Li(psi(pr-1)) \u003c 1/r :",
				"           L.append(p)",
				"    return L",
				"def A196667List(n) : return PlanatSole(n,1)",
				"A196667List(100) # _Peter Luschny_, Oct 23 2011",
				"(Perl)",
				"use ntheory \":all\"; forprimes { say if LogarithmicIntegral(chebyshev_psi($_))-LogarithmicIntegral(chebyshev_psi($_-1)) \u003c 1 } 3,1000; # _Dana Jacobsen_, Dec 29 2015"
			],
			"xref": [
				"Cf. A196668-A196675."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Michel Planat_, Oct 05 2011",
			"references": 10,
			"revision": 36,
			"time": "2020-03-20T02:38:38-04:00",
			"created": "2011-10-06T12:08:57-04:00"
		}
	]
}