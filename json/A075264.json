{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A075264",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 75264,
			"data": "1,5,3,6,5,1,502,485,150,15,760,802,305,50,3,152696,171150,73801,15435,1575,63,252336,295748,139020,33817,4515,315,9,51360816,62333204,31231500,8437975,1334760,124110,6300,135,88864128,110941776,58415444",
			"name": "Triangle of numerators of coefficients, where the n-th row forms the polynomial in z, P(n,z), that is the coefficient of x^n in {-log(1-x)/x}^z, for n \u003e 0. The denominator for all the terms in the n-th row is A053657(n).",
			"comment": [
				"Each n-th row polynomial, P(n,z), has a trivial zero at z = 0; for odd rows, P(2n+1,z) also has zeros at z = -2n, z = -(2n+1), for n \u003e 0."
			],
			"formula": [
				"The n-th row polynomials, P(n, z), satisfy 1 + Sum_{n\u003e=1} P(n, z) x^n = (Sum_{k\u003e=1} x^(k-1)/k)^z."
			],
			"example": [
				"P(1,z) = z/2,",
				"P(2,z) = (5z + 3z^2)/24,",
				"P(3,z) = (6z + 5z^2 + z^3)/48,",
				"P(4,z) = (502z + 485z^2 + 150z^3 + 15z^4)/5760,",
				"P(5,z) = (760z + 802z^2 + 305z^3 + 50z^4 +3z^5)/11520,",
				"P(6,z) = (152696z + 171150z^2 + 73801z^3 + 15435z^4 + 1575z^5",
				"+ 63z^6)/2903040,",
				"P(7,z) = (252336z + 295748z^2 + 139020z^3 + 33817z^4 + 4515z^5",
				"+ 315z^6 + 9z^7)/5806080,",
				"P(8,z) = (51360816z + 62333204z^2 + 31231500z^3 + 8437975z^4",
				"+ 1334760z^5 + 124110z^6 + 6300z^7 + 135z^8)/1393459200."
			],
			"maple": [
				"nmax:=8; A053657 := proc(n) local P, p, q, s, r; P := select(isprime, [$2..n]); r:=1; for p in P do s := 0; q := p-1; do if q \u003e (n-1) then break fi; s := s + iquo(n-1, q); q := q*p; od; r := r * p^s; od; r end: f(z) := convert(series((-ln(1-x)/x)^z, x, nmax+2), polynom): for n from 1 to nmax do f(n) := A053657(n+1)*coeff(f(z), x, n) od: for n from 1 to nmax do for m from 1 to n do a(n, m) := coeff(f(n), z, m) od: od: seq(seq(a(n, m), m=1..n), n=1..nmax);  # _Johannes W. Meijer_, Jun 08 2009, revised Nov 25 2012"
			],
			"mathematica": [
				"rows = 9; A053657[n_] := Product[p^Sum[Floor[(n-1)/((p-1) p^k)], {k, 0, n}], {p, Prime[Range[n]]}]; (Rest[CoefficientList[#, z]]\u0026 /@ Rest @ CoefficientList[(-Log[1-x]/x)^z + O[x]^(rows+1), x]) * Array[A053657, rows, 2] // Flatten (* _Jean-François Alcover_, Nov 22 2016 *)"
			],
			"program": [
				"(PARI) {T(n,k)=local(X=x+x^2*O(x^n)); D=1;for(j=0,n,D=lcm(D,denominator( polcoeff(polcoeff((-log(1-X)/x)^z+z*O(z^j),j,z),n,x)))); return(D*polcoeff(polcoeff((-log(1-X)/x)^z+z*O(z^k),k,z),n,x))}"
			],
			"xref": [
				"Cf. A053657.",
				"Cf. A163972 (MC polynomials)."
			],
			"keyword": "frac,nonn,tabl",
			"offset": "1,2",
			"author": "_Paul D. Hanna_, Sep 15 2002; revised Jun 27 2005",
			"references": 7,
			"revision": 21,
			"time": "2017-08-14T02:41:46-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}