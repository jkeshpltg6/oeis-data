{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A293496",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 293496,
			"data": "0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,4,3,0,0,0,0,10,15,12,1,0,0,0,20,45,72,38,2,0,0,0,35,105,252,270,117,6,0,0,0,56,210,672,1130,1044,336,14,0,0,0,84,378,1512,3535,5270,3795,976,30,0",
			"name": "Array read by antidiagonals: T(n,k) = number of chiral pairs of necklaces with n beads using a maximum of k colors.",
			"comment": [
				"An orientable necklace when turned over does not leave it unchanged. Only one necklace in each pair is included in the count.",
				"The number of chiral bracelets. An achiral bracelet is the same as its reverse, while a chiral bracelet is equivalent to its reverse. - _Robert A. Russell_, Sep 28 2018"
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A293496/b293496.txt\"\u003eTable of n, a(n) for n = 1..1275\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = (A075195(n,k) - A284855(n,k)) / 2.",
				"From _Robert A. Russell_, Sep 28 2018: (Start)",
				"T(n, k) = -(k^floor((n+1)/2) + k^ceiling((n+1)/2)) / 4 + (1/2n) * Sum_{d|n} phi(d) * k^(n/d)",
				"G.f. for column k: -(kx/4)*(kx+x+2)/(1-kx^2) - Sum_{d\u003e0} phi(d)*log(1-kx^d)/2d. (End)"
			],
			"example": [
				"Array begins:",
				"  ==========================================================",
				"  n\\k | 1  2    3     4      5       6        7        8",
				"  ----+-----------------------------------------------------",
				"   1  | 0  0    0     0      0       0        0        0 ...",
				"   2  | 0  0    0     0      0       0        0        0 ...",
				"   3  | 0  0    1     4     10      20       35       56 ...",
				"   4  | 0  0    3    15     45     105      210      378 ...",
				"   5  | 0  0   12    72    252     672     1512     3024 ...",
				"   6  | 0  1   38   270   1130    3535     9156    20748 ...",
				"   7  | 0  2  117  1044   5270   19350    57627   147752 ...",
				"   8  | 0  6  336  3795  23520  102795   355656  1039626 ...",
				"   9  | 0 14  976 14060 106960  556010  2233504  7440216 ...",
				"  10  | 0 30 2724 51204 483756 3010098 14091000 53615016 ...",
				"  ...",
				"For T(3,4)=4, the chiral pairs are ABC-ACB, ABD-ADB, ACD-ADC, and BCD-BDC.",
				"For T(4,3)=3, the chiral pairs are AABC-AACB, ABBC-ACBB, and ABCC-ACCB. - _Robert A. Russell_, Sep 28 2018"
			],
			"mathematica": [
				"b[n_, k_] := (1/n)*DivisorSum[n, EulerPhi[#]*k^(n/#) \u0026];",
				"c[n_, k_] := If[EvenQ[n], (k^(n/2) + k^(n/2 + 1))/2, k^((n + 1)/2)];",
				"T[_, 1] = T[1, _] = 0; T[n_, k_] := (b[n, k] - c[n, k])/2;",
				"Table[T[n - k + 1, k], {n, 1, 11}, {k, n, 1, -1}] // Flatten (* _Jean-François Alcover_, Oct 11 2017, translated from PARI *)"
			],
			"program": [
				"(PARI) \\\\ here b(n,k) is A075195 and c(n,k) is A284855",
				"b(n, k) = (1/n) * sumdiv(n, d, eulerphi(d)*k^(n/d));",
				"c(n, k) = if(n % 2 == 0, (k^(n/2) + k^(n/2+1))/2, k^((n+1)/2));",
				"T(n, k) = (b(n, k) - c(n, k)) / 2;"
			],
			"xref": [
				"Columns 2..6 are A059076, A278639, A278640, A278641, A278642.",
				"Cf. A075195, A081720, A284855."
			],
			"keyword": "nonn,tabl",
			"offset": "1,18",
			"author": "_Andrew Howroyd_, Oct 10 2017",
			"references": 11,
			"revision": 26,
			"time": "2018-09-28T23:34:51-04:00",
			"created": "2017-10-13T05:43:36-04:00"
		}
	]
}