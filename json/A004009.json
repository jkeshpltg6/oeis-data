{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A004009",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 4009,
			"id": "M5416",
			"data": "1,240,2160,6720,17520,30240,60480,82560,140400,181680,272160,319680,490560,527520,743040,846720,1123440,1179360,1635120,1646400,2207520,2311680,2877120,2920320,3931200,3780240,4747680,4905600,6026880",
			"name": "Expansion of Eisenstein series E_4(q) (alternate convention E_2(q)); theta series of E_8 lattice.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"E_8 is also the Barnes-Wall lattice in 8 dimensions.",
				"Cubic AGM theta functions: a(q) (see A004016), b(q) (A005928), c(q) (A005882).",
				"Ramanujan Lambert series: P(q) (see A006352), Q(q) (A004009), R(q) (A013973).",
				"The E_8 lattice is integral, unimodular, and even. The 240 shortest nonzero vectors in the lattice have norm squared 2. Of these vectors, 128 are all half-integer, and 112 are all integer. - _Michael Somos_, Jun 10 2019"
			],
			"reference": [
				"J. H. Conway and N. J. A. Sloane, \"Sphere Packings, Lattices and Groups\", Springer-Verlag, p. 123.",
				"W. Ebeling, Lattices and Codes, Vieweg; 2nd ed., 2002, see p. 53.",
				"R. C. Gunning, Lectures on Modular Forms. Princeton Univ. Press, Princeton, NJ, 1962, p. 53.",
				"N. Koblitz, Introduction to Elliptic Curves and Modular Forms, Springer-Verlag, 1984, see p. 111.",
				"S. Ramanujan, On Certain Arithmetical Functions, Messenger Math., 45 (1916), 11-15 (Eq. (25)). Collected Papers of Srinivasa Ramanujan, Chap. 16, Ed. G. H. Hardy et al., Chelsea, NY, 1962.",
				"S. Ramanujan, On Certain Arithmetical Functions, Messenger Math., 45 (1916), 11-15 (Eq. (25)). Ramanujan's Papers, p. 196, Ed. B. J. Venkatachala et al., Prism Books, Bangalore 2000.",
				"Jean-Pierre Serre, \"A Course in Arithmetic\", Springer, 1978",
				"Joseph H. Silverman, \"Advanced Topics in the Arithmetic of Elliptic Curves\", Springer, 1994",
				"N. J. A. Sloane, Seven Staggering Sequences, in Homage to a Pied Puzzler, E. Pegg Jr., A. H. Schoen and T. Rodgers (editors), A. K. Peters, Wellesley, MA, 2009, pp. 93-110.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A004009/b004009.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e (terms 0..1000 from N. J. A. Sloane)",
				"D. Bump, \u003ca href=\"https://doi.org/10.1017/CBO9780511609572\"\u003eAutomorphic Forms and Representations\u003c/a\u003e, Cambr. Univ. Press, 1997, p. 29.",
				"H. H. Chan and C. Krattenthaler, \u003ca href=\"http://arXiv.org/abs/math.NT/0407061\"\u003eRecent progress in the study of representations of integers as sums of squares\u003c/a\u003e, arXiv:math/0407061 [math.NT], 2004.",
				"Heng Huat Chan, Shaun Cooper, and Pee Choon Toh, \u003ca href=\"http://unimodular.net/archive/RamEisenstein.pdf\"\u003eRamanujan's Eisenstein series and powers of Dedekind's eta-function\u003c/a\u003e, Journal of the London Mathematical Society 75.1 (2007): 225-242. See Q(q).",
				"Henry Cohn and Stephen D. Miller, \u003ca href=\"http://arxiv.org/abs/1603.04759\"\u003eSome properties of optimal functions for sphere packing in dimensions 8 and 24\u003c/a\u003e, arXiv:1603.04759 [math.MG], 2016.",
				"H. S. M. Coxeter, \u003ca href=\"http://dx.doi.org/10.1215/S0012-7094-46-01347-6\"\u003eIntegral Cayley numbers\u003c/a\u003e, Duke Math. J. 13 (1946), 561-578; reprinted in \"Twelve Geometric Essays\", pp. 20-39.",
				"D. de Laat, F. Vallentin, \u003ca href=\"http://arxiv.org/abs/1607.02111\"\u003eA Breakthrough in Sphere Packing: The Search for Magic Functions\u003c/a\u003e, arXiv preprint arXiv:1607.02111 [math.MG], 2016.",
				"Yang-Hui He and John McKay, \u003ca href=\"http://arxiv.org/abs/1505.06742\"\u003eSporadic and Exceptional\u003c/a\u003e, arXiv:1505.06742 [math.AG], 2015.",
				"N. Heninger, E. M. Rains and N. J. A. Sloane, \u003ca href=\"http://arXiv.org/abs/math.NT/0509316\"\u003eOn the Integrality of n-th Roots of Generating Functions\u003c/a\u003e, J. Combinatorial Theory, Series A, 113 (2006), 1732-1745.",
				"M. Kaneko and D. Zagier, \u003ca href=\"http://www2.math.kyushu-u.ac.jp/~mkaneko/papers/atkin.pdf\"\u003eSupersingular j-invariants, hypergeometric series and Atkin's orthogonal polynomials\u003c/a\u003e, pp. 97-126 of D. A. Buell and J. T. Teitelbaum, eds., Computational Perspectives on Number Theory, Amer. Math. Soc., 1998",
				"Masao Koike, \u003ca href=\"https://oeis.org/A004016/a004016.pdf\"\u003eModular forms on non-compact arithmetic triangle groups\u003c/a\u003e, Unpublished manuscript [Extensively annotated with OEIS A-numbers by N. J. A. Sloane, Feb 14 2021. I wrote 2005 on the first page but the internal evidence suggests 1997.]",
				"Moody, Robert V., and Jiri Patera, \u003ca href=\"https://doi.org/10.1090/S0273-0979-1982-15021-2\"\u003eFast recursion formula for weight multiplicities\u003c/a\u003e, Bulletin of the American Mathematical Society 7.1 (1982): 237-242.",
				"G. Nebe and N. J. A. Sloane, \u003ca href=\"http://www.math.rwth-aachen.de/~Gabriele.Nebe/LATTICES/E8.html\"\u003eHome page for E_8 lattice\u003c/a\u003e",
				"H. Ochiai, \u003ca href=\"http://arXiv.org/abs/math-ph/9909023\"\u003eCounting functions for branched covers of elliptic curves and quasi-modular forms\u003c/a\u003e, arXiv:math-ph/9909023, 1999.",
				"S. Ramanujan, \u003ca href=\"http://www.imsc.res.in/~rao/ramanujan/CamUnivCpapers/Cpaper37/page1.htm\"\u003eOn the coefficients in the expansions of certain modular functions\u003c/a\u003e, Proc. Royal Soc., A, 95 (1918), 144-155.",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"http://neilsloane.com/doc/sg.txt\"\u003eMy favorite integer sequences\u003c/a\u003e, in Sequences and their Applications (Proceedings of SETA '98).",
				"N. J. A. Sloane, \u003ca href=\"http://neilsloane.com/doc/g4g7.pdf\"\u003eSeven Staggering Sequences\u003c/a\u003e.",
				"M. Viazovska, \u003ca href=\"http://arxiv.org/abs/1603.04246\"\u003eThe sphere packing problem in dimension 8\u003c/a\u003e, arXiv preprint arXiv:1603.04246 [math.NT], 2016.",
				"Martin H. Weissman, \u003ca href=\"http://people.ucsc.edu/~weissman/SAGE13Slides.pdf\"\u003eOctonions, Cubes, Embeddings\u003c/a\u003e, March 2, 2009.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/EisensteinSeries.html\"\u003eEisenstein Series.\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/LeechLattice.html\"\u003eLeech Lattice.\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Barnes-WallLattice.html\"\u003eBarnes-Wall Lattice\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Eisenstein_series\"\u003eEisenstein series\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/E8_lattice\"\u003eE_8 lattice\u003c/a\u003e",
				"\u003ca href=\"/index/Ed#Eisen\"\u003eIndex entries for sequences related to Eisenstein series\u003c/a\u003e",
				"\u003ca href=\"/index/Ba#BW\"\u003eIndex entries for sequences related to Barnes-Wall lattices\u003c/a\u003e"
			],
			"formula": [
				"Can also be expressed as E4(q) = 1 + 240 sum_{i=1}^infinity i^3 q^i/(1-q^i) - _Gene Ward Smith_, Aug 22 2006",
				"Theta series of E_8 lattice = 1 + 240 * Sum ( sigma_3 (m) * q^2m ), m = 1..inf, where sigma_3 (m) is the sum of the cubes of the divisors of m (A001158).",
				"Expansion of (phi(-q)^8 - (2 * phi(-q) * phi(q))^4 + 16 * phi(q)^8) in powers of q where phi() is a Ramanujan theta function.",
				"Expansion of (eta(q)^24 + 256 * eta(q^2)^24) / (eta(q) * eta(q^2))^8 in powers of q. - _Michael Somos_, Dec 30 2008",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2), A(x^4)) where f(u, v, w) = u^2 + 33*v^2 + 256*w^2 - 18*u*v + 16*u*w - 288*v*w . - _Michael Somos_, Jan 05 2006",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2), A(x^3), A(x^6)) where f(u1, u2, u3, u6) = u1^2 + 16*u2^2 + 81*u3^2 + 1296*u6^2 - 14*u1*u2 - 18*u1*u3 + 30*u1*u6 + 30*u2*u3 - 288*u2*u6 - 1134*u3*u6 . - _Michael Somos_, Apr 15 2007",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^3), A(x^9)) where f(u, v, w) = u^3*v + 9*w*u^3 - 84*u^2*v^2 + 246*u*v^3 - 253*v^4 - 675*w*u^2*v + 729*w^2*u^2 - 4590*w*u*v^2 + 19926*w*v^3 - 54675*w^2*u*v + 59049*w^3*u + 531441*w^3*v - 551124*w^2*v^2 . - _Michael Somos_, Apr 15 2007",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / t) = (t/i)^4 * f(t) where q = exp(2 Pi i t). - _Michael Somos_, Dec 30 2008",
				"Convolution square is A008410. A008411 is convolution of this sequence with A008410.",
				"Expansion of Ramanujan's function Q(q^2) = 12 (omega/Pi)^4 g2 (Weierstrass invariant) in powers of q^2.",
				"Expansion of a(q) * (a(q)^3 + 8*c(q)^3) in powers of q where a(), c() are cubic AGM theta functions. - _Michael Somos_, Jan 14 2015",
				"G.f. is (theta_2(q)^8 + theta_3(q)^8 + theta_4(q)^8) / 2 where q = exp(Pi i t). So a(n) = A008430(n) + 128*A007331(n) (= A000143(2*n) + 128*A007331(n) = A035016(2*n) + 128*A007331(n)). - _Seiichi Manyama_, Sep 30 2018",
				"a(n) = 240*A001158(n) if n\u003e0. - _Michael Somos_, Oct 01 2018"
			],
			"example": [
				"G.f. = 1 + 240*x + 2160*x^2 + 6720*x^3 + 17520*x^4 + 30240*x^5 + 60480*x^6 + ...",
				"G.f. = 1 + 240*q^2 + 2160*q^4 + 6720*q^6 + 17520*q^8 + 30240*q^10 + 60480*q^12 + ..."
			],
			"maple": [
				"with(numtheory); E := proc(k) local n,t1; t1 := 1-(2*k/bernoulli(k))*add(sigma[k-1](n)*q^n,n=1..60); series(t1,q,60); end; E(4);"
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 1, Boole[n == 0], 240 DivisorSigma[ 3, n]]; (* _Michael Somos_, Jul 11 2011 *)",
				"a[ n_] := SeriesCoefficient[ With[ {t2 = EllipticTheta[ 2, 0, q]^4, t3 = EllipticTheta[ 3, 0, q]^4}, t2^2 + 14 t2 t3 + t3^2], {q, 0, n}]; (* _Michael Somos_, Jun 04 2014 *)",
				"max = 30; s = 1 + 240*Sum[k^3*(q^k/(1 - q^k)), {k, 1, max}] + O[q]^max; CoefficientList[s, q] (* _Jean-François Alcover_, Nov 27 2015, after _Gene Ward Smith_ *)",
				"a[ n_] := SeriesCoefficient[ With[ {t2 = EllipticTheta[ 2, 0, q]^4, t3 = EllipticTheta[ 3, 0, q]^4}, t2^2 - t2 t3 + t3^2], {q, 0, 2 n}]; (* _Michael Somos_, Jul 31 2016 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n==0, 240 * sigma(n, 3))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( (eta(x + A)^24 + 256 * x * eta(x^2 + A)^24) / (eta(x + A) * eta(x^2 + A))^8, n))}; /* _Michael Somos_, Dec 30 2008 */",
				"(PARI) q='q+O('q^50); Vec((eta(q)^24+256*q*eta(q^2)^24)/(eta(q)*eta(q^2))^8) \\\\ _Altug Alkan_, Sep 30 2018",
				"(Sage) ModularForms(Gamma1(1), 4, prec=30).0 ; # _Michael Somos_, Jun 04 2013",
				"(MAGMA) Basis( ModularForms( Gamma1(1), 4), 29) [1]; /* _Michael Somos_, May 11 2015 */",
				"(MAGMA) L := Lattice(\"E\",8); A\u003cq\u003e := ThetaSeries(L, 57); A; /* _Michael Somos_, Jun 10 2019 */",
				"(Python)",
				"from sympy import divisor_sigma",
				"def a(n): return 1 if n == 0 else 240 * divisor_sigma(n, 3)",
				"[a(n) for n in range(51)]  # _Indranil Ghosh_, Jul 15 2017"
			],
			"xref": [
				"Cf. A046948, A000143, A108091 (eighth root).",
				"Cf. A008410, A008411, A001158.",
				"Cf. A006352 (E_2), A004009 (E_4), A013973 (E_6), A008410 (E_8), A013974 (E_10), A029828 (E_12), A058550 (E_14), A029829 (E_16), A029830 (E_20), A029831 (E_24).",
				"Cf. A007331 (theta_2(q)^8 / 256), A000143 (theta_3(q)^8)), A035016 (theta_4(q)^8)."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 183,
			"revision": 151,
			"time": "2021-02-14T01:32:15-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}