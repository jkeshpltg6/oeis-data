{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003169",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3169,
			"id": "M2973",
			"data": "1,3,14,79,494,3294,22952,165127,1217270,9146746,69799476,539464358,4214095612,33218794236,263908187100,2110912146295,16985386737830,137394914285538,1116622717709012,9113225693455362,74659999210200292",
			"name": "Number of 2-line arrays; or number of P-graphs with 2n edges.",
			"comment": [
				"First column of triangle A100326. - _Paul D. Hanna_, Nov 16 2004"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A003169/b003169.txt\"\u003eTable of n, a(n) for n = 1..200\u003c/a\u003e",
				"M. Bicknell and V. E. Hoggatt, Jr., \u003ca href=\"http://www.fq.math.ca/Scanned/14-3/hoggatt1.pdf\"\u003eSequences of matrix inverses from Pascal, Catalan and related convolution arrays\u003c/a\u003e, Fib. Quart., 14 (1976), 224-232.",
				"L. Carlitz, \u003ca href=\"http://www.fq.math.ca/Scanned/11-2/carlitz.pdf\"\u003eEnumeration of two-line arrays\u003c/a\u003e, Fib. Quart., Vol. 11 Number 2 (1973), 113-130.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=416\"\u003eEncyclopedia of Combinatorial Structures 416\u003c/a\u003e",
				"R. C. Read, \u003ca href=\"http://dx.doi.org/10.1007/BF02188172\"\u003eOn the enumeration of a class of plane multigraphs\u003c/a\u003e, Aequat. Math. 31 (1986) no 1, 47-63.",
				"Anssi Yli-Jyrä and Carlos Gómez-Rodríguez, \u003ca href=\"https://arxiv.org/abs/1706.03357\"\u003eGeneric Axiomatization of Families of Noncrossing Graphs in Dependency Parsing\u003c/a\u003e, arXiv:1706.03357 [cs.CL], 2017."
			],
			"formula": [
				"For formula see Read reference.",
				"D-finite with recurrence a(n) = ( (324*n^2-708*n+360)*a(n-1) - (371*n^2-1831*n+2250)*a(n-2) + (20*n^2-130*n+210)*a(n-3) )/(16*n*(2*n-1)) for n\u003e2, with a(0)=0, a(1)=1, a(2)=3. - _Paul D. Hanna_, Nov 16 2004",
				"G.f. satisfies: A(x) = x*(1+A(x))/(1-A(x))^2 where A(0)=0. G.f. satisfies: (1+A(x))/(1-A(x)) = 2*G003168(x)-1, where G003168 is the g.f. of A003168. - _Paul D. Hanna_, Nov 16 2004",
				"a(n) = (1/n)*Sum_{i=0..n-1} binomial(n,i)*binomial(3*n-i-2,n-i-1). - _Vladeta Jovovic_, Sep 13 2006",
				"Appears to be 1/n*Jacobi_P(n-1,1,n-1,3). If so then a(n) = 1/(2*n-1)*sum {k = 0..n-1} binomial(n-1,k)*binomial(2*n+k-1,k+1) = 1/n*sum {k = 0..n} binomial(n,k)*binomial(2*n-2,n+k-1)*2^k. - _Peter Bala_, Aug 01 2012",
				"a(n) ~ sqrt(33/sqrt(17)-7) * ((71+17*sqrt(17))/16)^n / (4*sqrt(2*Pi)*n^(3/2)). - _Vaclav Kotesovec_, Aug 09 2013",
				"The o.g.f. A(x) = 1 + 3*x + 14*x^2 + ... taken with offset 0, satisfies 1 + x*A'(x)/A(x) = 1 + 3*x + 19*x^2 + 138*x^3 + ..., the o.g.f. for A156894. - _Peter Bala_, Oct 05 2015"
			],
			"maple": [
				"a[0]:=0:a[1]:=1:a[2]:=3:for n from 3 to 30 do a[n]:=((324*n^2-708*n+360)*a[n-1] -(371*n^2-1831*n+2250)*a[n-2]+(20*n^2-130*n+210)*a[n-3])/(16*n*(2*n-1)) od:seq(a[n],n=1..25); # _Emeric Deutsch_, Jan 31 2005"
			],
			"mathematica": [
				"lim = 21; t[0, 0] = 1; t[n_, 0] := t[n, 0] = Sum[(k + 1)*t[n - 1, k], {k, 0, n - 1}]; t[n_, k_] := t[n, k] = Sum[t[j + 1, 0]*t[n - j - 1, k - 1], {j, 0, n - k}]; Table[ t[n, 0], {n, lim}] (* _Jean-François Alcover_, Sep 20 2011, after _Paul D. Hanna_'s comment *)"
			],
			"program": [
				"(PARI) {a(n)=if(n==0,0,if(n==1,1,if(n==2,3,( (324*n^2-708*n+360)*a(n-1) -(371*n^2-1831*n+2250)*a(n-2)+(20*n^2-130*n+210)*a(n-3))/(16*n*(2*n-1)) )))} \\\\ _Paul D. Hanna_, Nov 16 2004",
				"(PARI) {a(n)=local(A=x+x*O(x^n));if(n==1,1, for(i=1,n,A=x*(1+A)/(1-A)^2); polcoeff(A,n))}",
				"(Haskell)",
				"a003169 = flip a100326 0  -- _Reinhard Zumkeller_, Nov 21 2015"
			],
			"xref": [
				"Cf. A003168, A100324, A100326, A156894."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Emeric Deutsch_, Jan 31 2005"
			],
			"references": 14,
			"revision": 61,
			"time": "2020-02-20T03:18:33-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}