{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A233331",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 233331,
			"data": "1,7,21,15,51,66,102,136,75,186,244,274,310,406,462,246,490,631,729,780,735,939,1086,1183,610,1065,1341,1557,1707,1783,1491,1861,2155,2380,2511,1286,2037,2512,2908,3217,3427,3534,2716,3322,3831,4250,4548,4738,2404",
			"name": "Irregular array read by rows: T(n,k) = number of r_{n,k}-cores associated with A233332(n,k), reduced for symmetry, for n\u003e=2, 1\u003c=k\u003c=floor(n/2), explained below.",
			"comment": [
				"For definitions and more details, see the PDF by L. E. Jeffery.",
				"Let n be an integer, n \u003e= 2, and let k in {1,...,floor(n/2)}. Let R_n be the set of floor(n/2) rhombi in which the k-th rhombus r_{n,k} in R_n has interior angles about its vertices (or corners) given by the pair (k*Pi/n, (n-k)*Pi/n). Let T be any tiling of the plane. For any tile t in T, let C_m(t) denote the m-th corona of t, m\u003e=0. Equivalently, starting with any r in R_n fixed in the plane, we can compose a corona C_m(r) of r of any order m by tessellation using tiles of R_n. For any r in R_n fixed in the plane, a disjoint union of r with four tiles t_1,t_2,t_3,t_4 in R_n is called a \"candidate.\" If no tiles overlap in a candidate, then that candidate is called an \"r-core;\" otherwise that candidate is rejected (since no corona of r can be constructed from it). For each r_{n,k} in R_n, and for m\u003e0, every r_{n,k}-core can be extended to an m-th corona of r_{n,k} using tiles of R_n in a number of ways. For the case m=1, the array A233332 gives the number of ways that this can be done for each n and k. In the theory of tiles the general problem of counting these coronas and its reduction for symmetry seem to have not been addressed before in the literature.",
				"The array A233330 gives the number of r_{n,k}-cores associated with the coronas enumerated in A233332. The present array A233331 gives the number of r_{n,k}-cores associated with the coronas enumerated in A233332 when isometries different from the identity are not counted."
			],
			"reference": [
				"Marjorie Senechal, Quasicrystals and Geometry, Cambridge University Press, 1995, p. 145."
			],
			"link": [
				"Dirk Frettlöh, \u003ca href=\"http://tilings.math.uni-bielefeld.de/glossary\"\u003eGlossary of tiling-theoretic terms\u003c/a\u003e, Tilings Encyclopedia.",
				"L. E. Jeffery, \u003ca href=\"/A233332/a233332_5.pdf\"\u003eAlgorithm for constructing A233332\u003c/a\u003e.",
				"Eric W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/Corona.html\"\u003eCorona\u003c/a\u003e, from MathWorld.",
				"Eric W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/Tiling.html\"\u003eTiling\u003c/a\u003e, from MathWorld."
			],
			"formula": [
				"Conjecture: G.f. for column 1 is F_1(x) = x^2*(1+4x+x^2)/((1+x)^2*(1-x)^5).",
				"Conjecture: Column 1 = A233329, up to an offset.",
				"Conjecture: Column k has generating function of the form F_k(x) = G_k(x)/((1+x)^2*(1-x)^5), where G_k(x) is a polynomial in x.",
				"Conjecture: A233331(2*M,1) = A076454(M), M\u003e=1."
			],
			"example": [
				"Array begins: {1}; {7}; {21, 15}; {51, 66}; {102, 136, 75}; ..."
			],
			"xref": [
				"Cf. A076454, A233329-A233333."
			],
			"keyword": "nonn,tabf",
			"offset": "2,2",
			"author": "_L. Edson Jeffery_, Jan 06 2014",
			"references": 3,
			"revision": 14,
			"time": "2017-09-24T12:15:33-04:00",
			"created": "2014-01-21T03:52:39-05:00"
		}
	]
}