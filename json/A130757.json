{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A130757",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 130757,
			"data": "1,3,-1,15,-10,1,105,-105,21,-1,945,-1260,378,-36,1,10395,-17325,6930,-990,55,-1,135135,-270270,135135,-25740,2145,-78,1,2027025,-4729725,2837835,-675675,75075,-4095,105,-1,34459425,-91891800,64324260,-18378360,2552550,-185640,7140,-136",
			"name": "Triangular table of coefficients of Laguerre-Sonin polynomials n!*2^n*Lag(n,x/2,1/2) of order 1/2.",
			"comment": [
				"These polynomials appear in the radial l=0 (s) wave functions of the isotropic three dimensional harmonic quantum oscillator with the dimensionless variable x=(r/L)^2 with r\u003e=0 and L^2=h/(m*f0). h is Planck's constant and m and f0 are the mass and the frequency of the oscillator.",
				"From _Tom Copeland_, Dec 13 2015: (Start)",
				"See A099174 for relations to the Hermite polynomials and the link in A176230 for operator relations. The infinitesimal generator for this matrix contains A014105.",
				"The row polynomials are P(n,x) = 2^n n! Lag(n,x/2,1/2), where Lag(n,x,q) is the associated Laguerre polynomial of order q, with raising operator R = -x^(-2) [x^(3/2) (1 - 2D)]^2 = 3 - x + (4x - 6) D - 4x D^2 with D = d/dx, i.e., R P(n,x) - P(n+1,x). A matrix reresentation of R acting on an o.g.f. (formal power series) is given by the transpose of the production matrix below. The diagonal corresponds to (3 + 4 xD) x^n = (3 + 4n) x^n; the upper diagonal, to -x x^n = -x^(n+1); and the lower diagonal, to (-6 - 4 xD) D x^n = -n (6 + 4(n-1)) x^(n-1), the sequence A002943. See A176230 for a similar relation.",
				"The triangles of Bessel numbers entries A122848, A049403, A096713, A104556 contain these polynomials as even or odd rows. Also the aerated version A099174 and A066325. Reversed, these entries are A100861, A144299, A111924.",
				"(End)",
				"Exponential Riordan array [1/(1-2x)^(3/2), -x/(1-2x)]. - _Paul Barry_, Mar 07 2017"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A130757/b130757.txt\"\u003eTable of n, a(n) for n = 0..10010\u003c/a\u003e (rows 0 to 140, flattened)",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards Applied Math. Series 55, Tenth Printing, 1972, p. 775, 22.3.9.",
				"T. Copeland, \u003ca href=\"https://tcjpn.wordpress.com/2020/07/15/juggling-zeros-in-the-matrix-example-ii/\"\u003eJuggling Zeros in the Matrix: Example II\u003c/a\u003e, 2020.",
				"Wolfdieter Lang, \u003ca href=\"/A130757/a130757.txt\"\u003eFirst ten rows and more\u003c/a\u003e."
			],
			"formula": [
				"a(n,m)= n!*(2^(n-m))*L(1/2,n,m) with L(1/2,n,m)=((-1)^m)*binomial(n+1/2,n-m)/m!, n\u003e=m\u003e=0, else 0.",
				"Let IP be the lower triangular matrix with its first subdiagonal equal to the first subdiagonal (cf. A014105) of this entry's unsigned matrix M and with all other elements equal to zero. Then IP is the infinitesimal generator of M, i.e., M = exp(IP). - _Tom Copeland_, Dec 12 2015",
				"Production matrix is",
				"3, -1,",
				"-6, 7, -1,",
				"0, -20, 11, -1,",
				"0, 0, -42, 15, -1,",
				"0, 0, 0, -72, 19, -1,",
				"0, 0, 0, 0, -110, 23, -1,",
				"0, 0, 0, 0, 0, -156, 27, -1,",
				"0, 0, 0, 0, 0, 0, -210, 31, -1,",
				"0, 0, 0, 0, 0, 0, 0, -272, 35, -1. - _Tom Copeland_, Dec 14 2015"
			],
			"example": [
				"[1]; [3,-1]; [15,-10,1]; [105,-105,21,-1]; [945,-1260,378,-36,1]; ..."
			],
			"maple": [
				"seq(seq(n!*2^(n-m)*(-1)^m*binomial(n+1/2,n-m)/m!,m=0..n),n=0..10); # _Robert Israel_, Dec 25 2015"
			],
			"mathematica": [
				"Table[n! (2^(n - m)) ((-1)^m) Binomial[n + 1/2, n - m]/m!, {n, 0, 8}, {m, 0, n}] // Flatten (* _Michael De Vlieger_, Dec 24 2015 *)"
			],
			"xref": [
				"Cf. A021009 (Coefficient table of n!*L(n, 0, x)).",
				"Row sums (signed) give A131441. Row sums (unsigned) give A066224.",
				"Cf. A001147, A002943, A014105, A049403, A066325, A096713, A099174, A100861, A104556, A111924, A122848, A144299, A176230."
			],
			"keyword": "sign,tabl,easy",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Jul 13 2007",
			"ext": [
				"Title formula corrected by _Tom Copeland_, Dec 12 2015"
			],
			"references": 12,
			"revision": 47,
			"time": "2020-09-07T12:07:56-04:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}