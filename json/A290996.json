{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A290996",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 290996,
			"data": "1,2,4,9,22,55,136,330,789,1872,4433,10510,24968,59409,141470,336935,802340,1910166,4546845,10822176,25758097,61308650,145928764,347350473,826795942,1968018151,4684451824,11150316882,26540849109,63174538224,150372815489",
			"name": "p-INVERT of (1,1,1,1,1,...), where p(S) = 1 - S - S^4.",
			"comment": [
				"Suppose s = (c(0), c(1), c(2),...) is a sequence and p(S) is a polynomial. Let S(x) = c(0)*x + c(1)*x^2 + c(2)*x^3 + ... and T(x) = (-p(0) + 1/p(S(x)))/x. The p-INVERT of s is the sequence t(s) of coefficients in the Maclaurin series for T(x).  Taking p(S) = 1 - S gives the \"INVERT\" transform of s, so that p-INVERT is a generalization of the \"INVERT\" transform (e.g., A033453).",
				"See A291000 for a guide to related sequences."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A290996/b290996.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (5, -9, 7, -1)"
			],
			"formula": [
				"a(n) = 5*a(n-1) - 9*a(n-2) + 7*a(n-3) - a(n-4) for n \u003e= 4.",
				"G.f.: (1 - 3*x + 3*x^2) / (1 - 5*x + 9*x^2 - 7*x^3 + x^4). - _Colin Barker_, Aug 22 2017"
			],
			"mathematica": [
				"z = 60; s = x/(1 - x); p = 1 - s - s^4;",
				"Drop[CoefficientList[Series[s, {x, 0, z}], x], 1]  (* A000012 *)",
				"Drop[CoefficientList[Series[1/p, {x, 0, z}], x], 1]  (* A290996 *)"
			],
			"program": [
				"(PARI) Vec((1 - 3*x + 3*x^2) / (1 - 5*x + 9*x^2 - 7*x^3 + x^4) + O(x^50)) \\\\ _Colin Barker_, Aug 22 2017"
			],
			"xref": [
				"Cf. A000012, A289780, A291000."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Clark Kimberling_, Aug 22 2017",
			"references": 2,
			"revision": 6,
			"time": "2017-08-23T09:46:14-04:00",
			"created": "2017-08-23T09:46:14-04:00"
		}
	]
}