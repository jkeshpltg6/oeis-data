{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A144267",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 144267,
			"data": "1,4,1,36,12,1,504,144,48,24,1,9576,2520,1440,360,240,40,1,229824,57456,30240,12960,7560,8640,960,720,720,60,1,6664896,1608768,804384,635040,201096,211680,90720,60480,17640,30240,6720,1260,1680,84,1,226606464,53319168",
			"name": "Partition number array, called M32(-4), related to A011801(n,m)= |S2(-4;n,m)| ( generalized Stirling triangle).",
			"comment": [
				"Each partition of n, ordered as in Abramowitz-Stegun (A-St order; for the reference see A134278), is mapped to a nonnegative integer a(n,k)=:M32(-4;n,k) with the k-th partition of n in A-St order.",
				"The sequence of row lengths is A000041 (partition numbers) [1, 2, 3, 5, 7, 11, 15, 22, 30, 42, ...].",
				"a(n,k) enumerates special unordered forests related to the k-th partition of n in the A-St order. The k-th partition of n is given by the exponents enk =(e(n,k,1),...,e(n,k,n)) of 1,2,...n. The number of parts is m = sum(e(n,k,j),j=1..n). The special (enk)-forest is composed of m rooted increasing (r+3)-ary trees if the outdegree is r \u003e= 0.",
				"If M32(-4;n,k) is summed over those k with fixed number of parts m one obtains triangle A011801(n,m)= |S2(-4;n,m)|, a generalization of Stirling numbers of the second kind. For S2(K;n,m), K from the integers, see the reference under A035342."
			],
			"link": [
				"W. Lang, \u003ca href=\"/A144267/a144267.txt\"\u003eFirst 10 rows of the array and more.\u003c/a\u003e",
				"W. Lang, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL12/Lang/lang.html\"\u003eCombinatorial Interpretation of Generalized Stirling Numbers\u003c/a\u003e, J. Int. Seqs. Vol. 12 (2009) 09.3.3."
			],
			"formula": [
				"a(n,k) = (n!/product(e(n,k,j)!*j!^(e(n,k,j),j=1..n))*product(|S2(-4,j,1)|^e(n,k,j),j=1..n) = M3(n,k)*product(|S2(-4,j,1)|^e(n,k,j),j=1..n), with |S2(-4,n,1)|= A008546(n-1) = (5*n-6)(!^5) (5-factorials) for n\u003e=2 and 1 if n=1 and the exponent e(n,k,j) of j in the k-th partition of n in the A-St ordering of the partitions of n. Exponents 0 can be omitted due to 0!=1. M3(n,k):= A036040(n,k), k=1..p(n), p(n):= A000041(n)."
			],
			"example": [
				"a(4,3)=48. The relevant partition of 4 is (2^2). The 48 unordered (0,2,0,0)-forests are composed of the following 2 rooted increasing trees 1--2,3--4; 1--3,2--4 and 1--4,2--3. The trees are quaternary because r=1 vertices are quaternary (4-ary) and for the leaves (r=0) the arity does not matter. Each of the three differently labeled forests comes therefore in 4^2=16 versions due to the two quaternary root vertices."
			],
			"xref": [
				"Cf. A143173 (M32(-3) array), A144268 (M32(-5) array)."
			],
			"keyword": "nonn,easy,tabf",
			"offset": "1,2",
			"author": "_Wolfdieter Lang_, Oct 09 2008",
			"references": 5,
			"revision": 14,
			"time": "2019-08-28T18:01:38-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}