{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A342877",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 342877,
			"data": "1,1,1,0,1,0,1,1,0,1,1,0,1,1,1,0,1,1,0,1,1,1,1,1,0,1,0,1,1,0,1,0,1,0,1,1,0,1,1,0,1,0,0,0,1,1,0,0,0,1,0,1,1,1,1,0,1,0,0,1,1,0,0,0,1,1,1,0,0,1,1,1,1,0,1,1,0,1,1,0,1,0,1,0,1,1,0,0,0,1,1,0,1,0,1,1,0,1,1,1,1,1,0,1,1,1",
			"name": "a(n) = 1 if the average distance between consecutive first n primes is greater than that of the first n-1 primes, otherwise a(n) = 0, for n \u003e 2.",
			"comment": [
				"The average distance between consecutive primes among the first n primes tends to increase with n. This average distance always changes when n is increased to n + 1, but it seems that most of the times this distance decreases. See a log-linear scatter plot of (1/n) Sum_{i=1..n} a(i) in Links.",
				"Conjecture: lim_{n-\u003einfinity} (1/n) Sum_{i=1..n} a(i) \u003c 1/2.",
				"In support of the conjecture: If it is assumed, as an approximation, that position of primes follows a Poisson point process then the distance between consecutive primes is a stochastic variable with exponential probability distribution function. The probability that an exponentially distributed stochastic variable takes a value larger than the mean value is about 0.367879."
			],
			"link": [
				"Andres Cicuttin, \u003ca href=\"/A342877/a342877_3.png\"\u003eLog-linear scatter plot of (1/n) Sum_{i=1..n} a(i) for first 2^18 primes\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Poisson_point_process\"\u003ePoisson point process\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Exponential_distribution\"\u003eExponential distribution\u003c/a\u003e",
				"Yamasaki, Yasuo and Yamasaki, Aiichi, \u003ca href=\"https://repository.kulib.kyoto-u.ac.jp/dspace/bitstream/2433/84326/1/0887-10.pdf\"\u003eOn the Gap Distribution of Prime Numbers\u003c/a\u003e, 数理解析研究所講究録 (1994), 887: 151-168.",
				"\u003ca href=\"/index/Ch#char_fns\"\u003eIndex entries for characteristic functions\u003c/a\u003e"
			],
			"example": [
				"a(3) = 1 because the average distance between consecutive first three primes {2,3,5} is (5 - 2)/2 = 3/2 which is greater than the average distance between consecutive first two primes {2,3} which is (3-2)/1 = 1.",
				"a(6)=0 because the average distance between consecutive first six primes {2,3,5,7,11,13} is (13 - 2)/5 = 11/5 which is smaller than the average distance between consecutive first five primes {2,3,5,7,11} which is (11 - 2)/4 = 9/4."
			],
			"mathematica": [
				"a={}; nmax=128;",
				"Do[If[(Prime[n]-2)/(n-1)\u003e(Prime[n-1]-2)/(n-2),AppendTo[a,1],AppendTo[a,0]],{n,3,nmax}];",
				"a",
				"(* Uncomment and run next lines to produce the log-linear plot available in Links *)",
				"(* a={};",
				"nmax=2^18;",
				"Do[If[(Prime[n]-2)/(n-1)\u003e(Prime[n-1]-2)/(n-2),AppendTo[a,{n,1}],AppendTo[a,{n,0}]],{n,3,nmax}];",
				"ListLogLinearPlot[Transpose[{Range[3,nmax],Accumulate[Transpose[a][[2]]]/Range[3,nmax]}],Frame-\u003eTrue,PlotRange-\u003e{All,{0.25,0.75}},PlotLabel-\u003eText[Style[\"Sum_{i=1..n} a(i)/n\",FontSize-\u003e16]],",
				"FrameLabel-\u003e{Text[Style[\"n\",FontSize-\u003e16]],},PlotStyle-\u003e{PointSize-\u003eSmall,Red},GridLines-\u003eAutomatic] *)"
			],
			"program": [
				"(PARI) A342877(n) = (((prime(n)-2)/(n-1)) \u003e ((prime(n-1)-2)/(n-2))); \\\\ _Antti Karttunen_, Mar 28 2021"
			],
			"xref": [
				"Cf. A001223, A079418, A286888."
			],
			"keyword": "nonn",
			"offset": "3,1",
			"author": "_Andres Cicuttin_, Mar 28 2021",
			"references": 1,
			"revision": 38,
			"time": "2021-05-07T16:00:17-04:00",
			"created": "2021-05-07T16:00:17-04:00"
		}
	]
}