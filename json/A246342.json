{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A246342",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 246342,
			"data": "12,23,15,18,38,35,39,43,24,68,86,71,37,21,28,50,74,62,56,149,76,104,230,305,235,186,278,224,1337,1062,2288,8951,4482,16688,67271,33637,16821,66688,571901,338059,181516,258260,455900,1180337,1080207,1817863,1157487,984558,1230848,53764115",
			"name": "a(0) = 12, after which, if a(n-1) = product_{k \u003e= 1} (p_k)^(c_k), then a(n) = (1/2) * (1 + product_{k \u003e= 1} (p_{k+1})^(c_k)), where p_k indicates the k-th prime, A000040(k).",
			"comment": [
				"Iterates of A048673 starting from value 12.",
				"All numbers 1 .. 11 are in finite cycles of A048673/A064216, thus 12 is the smallest number in this cycle, regardless of whether the cycle is infinite or finite.",
				"This sequence soon reaches much larger values than the corresponding A246343 (iterating the same cycle in the other direction). However, with the corresponding sequences starting from 16 (A246344 \u0026 A246345), there is no such pronounced difference, and with them the bias is actually the other way."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A246342/b246342.txt\"\u003eTable of n, a(n) for n = 0..1001\u003c/a\u003e"
			],
			"formula": [
				"a(0) = 12, and for n \u003e= 1, a(n) = A048673(a(n-1))."
			],
			"example": [
				"Start with a(0) = 12; thereafter each new term is obtained by replacing each prime factor of the previous term with the next prime, to whose product 1 is added before it is halved:",
				"12 = 2^2 * 3 = p_1^2 * p_2 -\u003e ((p_2^2 * p_3)+1)/2 = ((9*5)+1)/2 = 23, thus a(1) = 23.",
				"23 = p_9 -\u003e (p_10 + 1)/2 = (29+1)/2 = 15, thus a(2) = 15."
			],
			"program": [
				"(PARI)",
				"default(primelimit, 2^30);",
				"A003961(n) = my(f = factor(n)); for (i=1, #f~, f[i, 1] = nextprime(f[i, 1]+1)); factorback(f); \\\\ Using code of _Michel Marcus_",
				"A048673(n) = (A003961(n)+1)/2;",
				"k = 12; for(n=0, 1001, write(\"b246342.txt\", n, \" \", k) ; k = A048673(k));",
				"(Scheme, with memoization-macro definec)",
				"(definec (A246342 n) (if (zero? n) 12 (A048673 (A246342 (- n 1)))))"
			],
			"xref": [
				"A246343 gives the terms of the same cycle when going in the opposite direction from 12.",
				"Cf. A048673, A064216, A246344, A246345.",
				"Cf. also A246281, A246282, A246351, A246352, A246361, A246362, A246371, A246372."
			],
			"keyword": "nonn",
			"offset": "0,1",
			"author": "_Antti Karttunen_, Aug 24 2014",
			"references": 5,
			"revision": 15,
			"time": "2014-08-26T01:20:27-04:00",
			"created": "2014-08-26T01:20:27-04:00"
		}
	]
}