{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A238326",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 238326,
			"data": "1,2,3,4,1,5,2,6,3,2,7,4,4,8,5,6,3,9,6,8,6,1,10,7,10,9,4,2,11,8,12,12,8,3,2,12,9,14,15,12,5,4,4,2,13,10,16,18,16,10,5,6,3,4,14,11,18,21,20,15,6,6,8,6,6,4,15,12,20,24,24,20,7,12,10,9,8",
			"name": "Array: row n gives the number of occurrences of each possible diagonal partition of n, arranged in reverse Mathematica order.",
			"comment": [
				"Suppose that p is a partition of n, let F(p) be its Ferrers matrix, as defined at A237981, and let mXm be the size of F(p).  The numbers of 1s in each of the 2m-1 diagonals of F(p) form a partition of n.  Any partition which is associated with a partition of n in this manner is introduced here as a diagonal partition of n.  A000041(n) = sum of the numbers in row n; A003114(n) = number of terms in row n.  Every diagonal partition is an antidiagonal partition, as in A238325 (but not conversely)."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A238326/b238326.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e"
			],
			"example": [
				"The Mathematica ordering of the 3 antidiagonal partitions of 6 follows:  2211, 21111, 111111.)  Frequencies of these among the 11 partitions of 6 are given in reverse Mathematica ordering as follows:  111111 occurs 6 times, 21111 occurs 3 times, and 2211 occurs 2 times, so that row 6 of the array is 6 3 2.",
				"...",
				"First 9 rows:",
				"1",
				"2",
				"3",
				"4 1",
				"5 2",
				"6 3 2",
				"7 4 4",
				"8 5 6 3",
				"9 6 8 6 1"
			],
			"mathematica": [
				"z = 20; ferrersMatrix[list_] := PadRight[Map[Table[1, {#}] \u0026, #], {#, #} \u0026[Max[#, Length[#]]]] \u0026[list]; diagPartSE[list_] := Module[{m = ferrersMatrix[list]}, Map[Diagonal[m, #] \u0026, Range[-#, #] \u0026[Length[m] - 1]]]; Tally[Map[  DeleteCases[Reverse[Sort[Map[Count[#, 1] \u0026, diagPartSE[#]]]], 0] \u0026, IntegerPartitions[z]]]; a1[n_] := Last[Transpose[Tally[Map[DeleteCases[Reverse[Sort[Map[Count[#, 1] \u0026, diagPartSE[#]]]], 0] \u0026, IntegerPartitions[n]]]]]; t = Table[a1[n], {n, 1, z}]; u = Flatten[t]",
				"Map[Last[Transpose[Tally[Map[DeleteCases[Reverse[Sort[Map[Count[#, 1] \u0026, diagPartSE[#]]]], 0] \u0026, IntegerPartitions[#]]]]] \u0026, Range[z]] // TableForm",
				"(* _Peter J. C. Moses_, Feb 25 2014 *)"
			],
			"xref": [
				"Cf. A238325, A003114, A000041."
			],
			"keyword": "nonn,tabf,easy",
			"offset": "1,2",
			"author": "_Clark Kimberling_ and _Peter J. C. Moses_, Feb 25 2014",
			"references": 5,
			"revision": 4,
			"time": "2014-02-26T18:38:57-05:00",
			"created": "2014-02-26T18:38:57-05:00"
		}
	]
}