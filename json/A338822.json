{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A338822",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 338822,
			"data": "1,1,1,1,1,1,1,1,1,1,0,2,0,1,0,1,0,1,0,1,0,2,0,2,0,1,0,1,0,1,0,2,0,1,0,2,0,1,0,1,0,2,0,2,0,1,0,2,0,1,0,2,0,1,0,1,0,1,0,2,0,2,0,2,0,2,0,1,0,1,0,3,0,1,0,1,0,1,0,1,0,2,0,3,0,1,0,2,0,1,0",
			"name": "a(n) is the number of divisors of n whose last digits equal the number of digits of n.",
			"comment": [
				"Inspired by Project Euler, Problem 474 (see link).",
				"If in the name, one replaces “whose last digits equal” with “whose last digit equals” then, this sequence is finite and the last term should be a(999999999) = 5 because 999999999 has 9 digits and among the 20 divisors of 999999999, five of them (9, 999, 9009009, 12345679 and 999999999) have the last digit 9.",
				"With this name, the next term is a(10^9) = 1 because 10^9 has 10 digits and among the 100 divisors of 10^9, only one of them (10) ends with 10.",
				"For p prime, p \u003e= 7, the number 10^(p - 1) + p - 1 is divisible by p and the number of digits is p, so it has at least one divisor that ends in p. Thus, there is an infinity of terms a(k) \u003e 0. - _Marius A. Burtea_, Nov 12 2020"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A338822/b338822.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Project Euler, \u003ca href=\"https://projecteuler.net/problem=474\"\u003eProblem 474: Last digits of divisors\u003c/a\u003e."
			],
			"formula": [
				"For 1-digit numbers: a(n) = 1.",
				"For 2-digit numbers: a(n) = 0 iff n is odd, a(n) \u003e= 1 if n is even.",
				"For 4-digit numbers: a(n) = 0 if n is odd.",
				"For 5-digit numbers, a(n) \u003e= 2 if n ends with 5, a(n) \u003e=1 if n ends with 0, otherwise a(n) = 0.",
				"For 8-digit numbers, a(n) = 0 if n is odd."
			],
			"example": [
				"72 has 2 digits, and among the divisors of 72 (1, 2, 3, 4, 6, 12, 18, 24, 36, 72), three of them (2, 12 and 72) have the last digit 2, hence a(72) = 3.",
				"111 has 3 digits, and among the divisors of 111 (1, 3, 37, 111), only one of them (3) has the last digit 3, hence a(111) = 1."
			],
			"maple": [
				"f:= proc(n) local d, dd;",
				"  d:= ilog10(n)+1;",
				"  dd:= ilog10(d)+1;",
				"  nops(select(t -\u003e t mod 10^dd = d, numtheory:-divisors(n)))",
				"end proc:",
				"map(f, [$1..100]); # _Robert Israel_, Nov 12 2020"
			],
			"mathematica": [
				"a[n_] := DivisorSum[n, 1 \u0026, Divisible [# - (ndigit = IntegerLength[n]), 10^IntegerLength[ndigit]] \u0026]; Array[a, 100] (* _Amiram Eldar_, Nov 12 2020 *)"
			],
			"program": [
				"(MAGMA) [#[d:d in Divisors(n) | d mod 10^(#Intseq(#Intseq(n))) eq #Intseq(n)]:n in [1..100]]; // _Marius A. Burtea_, Nov 12 2020",
				"(PARI) a(n) = my(nb = #Str(n), nc = #Str(nb)); sumdiv(n, d, if (d\u003cnb, 0, !((d-nb) % (10^nc)))); \\\\ _Michel Marcus_, Nov 16 2020"
			],
			"xref": [
				"Cf. A000005, A330348."
			],
			"keyword": "nonn,base",
			"offset": "1,12",
			"author": "_Bernard Schott_, Nov 11 2020",
			"ext": [
				"Name generalized following remark of _Marius A. Burtea_, Nov 12 2020"
			],
			"references": 1,
			"revision": 60,
			"time": "2020-11-23T12:24:05-05:00",
			"created": "2020-11-22T05:53:58-05:00"
		}
	]
}