{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A061345",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 61345,
			"data": "1,3,5,7,9,11,13,17,19,23,25,27,29,31,37,41,43,47,49,53,59,61,67,71,73,79,81,83,89,97,101,103,107,109,113,121,125,127,131,137,139,149,151,157,163,167,169,173,179,181,191,193,197,199,211,223,227,229,233,239",
			"name": "Odd prime powers.",
			"comment": [
				"Let a(n)=p^e, then tau(a(n)^2) = tau(p^(2*e)) = 2*e+1 = 2*(e+1)-1 = tau(2*a(n))-1 where tau=A000005. - _Juri-Stepan Gerasimov_, Jul 14 2011"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A061345/b061345.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"L. J. Corwin, \u003ca href=\"/A033948/a033948.pdf\"\u003eIrreducible polynomials over the integers which factor mod p for every p\u003c/a\u003e, Unpublished Bell Labs Memo, Sep 07 1967 [Annotated scanned copy]"
			],
			"formula": [
				"a(n) = A061344(n)-1.",
				"Intersection of A000961 (prime powers) and A005408 (odd numbers). - _Robert Israel_, Jun 11 2014"
			],
			"maple": [
				"select(t -\u003e nops(ifactors(t)[2])\u003c=1, [seq(2*i+1,i=0..1000)]); # _Robert Israel_, Jun 11 2014",
				"# alternative:",
				"A061345 := proc(n)",
				"    option remember;",
				"    local k ;",
				"    if n = 0 then",
				"        1;",
				"    else",
				"        for k from procname(n-1)+2 by 2 do",
				"            if nops(numtheory[factorset](k)) = 1 then",
				"                return k ;",
				"            end if;",
				"        end do:",
				"    end if;",
				"end proc: # _R. J. Mathar_, Jun 25 2016"
			],
			"mathematica": [
				"t={1};k=0;Do[If[k==1,AppendTo[t,a1]];k=0;Do[c=Sqrt[a^2+b^2];If[IntegerQ[c]\u0026\u0026GCD[a,b,c]==1,k++;a1=a;b1=b;c1=c;],{b,4,a^2/2,2}],{a,3,260,2}];t (* _Vladimir Joseph Stephan Orlovsky_, Jan 29 2012 *)",
				"Select[2 Range@ 130 - 1, PrimeNu@# \u003c 2 \u0026] (* _Robert G. Wilson v_, Jun 12 2014 *)"
			],
			"program": [
				"(MAGMA) [1] cat [n: n in [3..300 by 2] | IsPrimePower(n)]; // _Bruno Berselli_, Feb 25 2016",
				"(PARI) is(n)=my(p); if(isprimepower(n,\u0026p), p\u003e2, n==1) \\\\ _Charles R Greathouse IV_, Jun 08 2016"
			],
			"xref": [
				"Cf. A061346, A000961, A005408, A061344, A075109, A075110."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Jun 08 2001",
			"ext": [
				"More terms from Larry Reeves (larryr(AT)acm.org), Jun 12 2001"
			],
			"references": 35,
			"revision": 47,
			"time": "2021-12-25T12:35:48-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}