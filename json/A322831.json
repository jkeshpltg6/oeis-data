{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A322831",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 322831,
			"data": "71,71,40,77,45,51,42,56,49,51,48,54",
			"name": "Average path length to self-trapping, rounded to nearest integer, of self-avoiding two-dimensional random walks using unit steps and direction changes from the set Pi*(2*k/n - 1), k = 1..n-1.",
			"comment": [
				"The cases n = 3, 4, and 6 correspond to the usual self-avoiding random walks on the honeycomb net, the square lattice, and the hexagonal lattice, respectively. The other cases n = 5, 7, ... are a generalization using self-avoiding rooted walks similar to those defined in A306175, A306177, ..., A306182. The walk is trapped if it cannot be continued without either hitting an already visited (lattice) point or crossing or touching any straight line connecting successively visited points on the path up to the current point.",
				"The result 71 for n=4 was established in 1984 by Hemmer \u0026 Hemmer.",
				"The sequence data are based on the following results of at least 10^9 simulated random walks for each n \u003c= 12, with an uncertainty of +- 0.004 for the average walk length:",
				"   n  length",
				"   3  71.132",
				"   4  70.760 (+-0.001)",
				"   5  40.375",
				"   6  77.150",
				"   7  45.297",
				"   8  51.150",
				"   9  42.049",
				"  10  56.189",
				"  11  48.523",
				"  12  51.486",
				"  13  47.9   (+-0.2)",
				"  14  53.9   (+-0.2)"
			],
			"link": [
				"S. Hemmer, P. C. Hemmer, \u003ca href=\"https://doi.org/10.1063/1.447349\"\u003eAn average self‐avoiding random walk on the square lattice lasts 71 steps\u003c/a\u003e, J. Chem. Phys. 81, 584 (1984)",
				"Hugo Pfoertner, \u003ca href=\"http://www.randomwalk.de/sequences/A322831.htm\"\u003eExamples of self-trapping random walks\u003c/a\u003e.",
				"Hugo Pfoertner, \u003ca href=\"/A322831/a322831.pdf\"\u003eProbability density for the number of steps before trapping occurs\u003c/a\u003e, 2018.",
				"Hugo Pfoertner, \u003ca href=\"http://www.randomwalk.de/stw2d.html\"\u003eResults for the 2D Self-Trapping Random Walk.\u003c/a\u003e",
				"Alexander Renner, \u003ca href=\"http://www.tbi.univie.ac.at/papers/Abstracts/alex_dipl.pdf\"\u003eSelf avoiding walks and lattice polymers\u003c/a\u003e, Diplomarbeit, Universität Wien, December 1994."
			],
			"xref": [
				"Cf. A001668, A001411, A001334, A077482, A306175, A306177, A306178, A306179, A306180, A306181, A306182.",
				"Cf. A122223, A122224, A122226, A127399, A127400, A127401, A300665, A323141, A323560, A323562, A323699."
			],
			"keyword": "nonn,more",
			"offset": "3,1",
			"author": "_Hugo Pfoertner_, Dec 27 2018",
			"references": 5,
			"revision": 21,
			"time": "2019-07-02T18:09:55-04:00",
			"created": "2018-12-30T00:01:12-05:00"
		}
	]
}