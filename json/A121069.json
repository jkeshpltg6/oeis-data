{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A121069",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 121069,
			"data": "2,4,6,30,210,2310,30030,510510,9699690,223092870,6469693230,200560490130,7420738134810,304250263527210,13082761331670030,614889782588491410,32589158477190044730,1922760350154212639070",
			"name": "Conjectured sequence for jumping champions greater than 1 (most common prime gaps up to x, for some x).",
			"comment": [
				"If n \u003e 2, then a(n) = product of n-1 consecutive distinct prime divisors. E.g. a(5)=210, the product of 4 consecutive and distinct prime divisors, 2,3,5,7. - _Enoch Haga_, Dec 08 2007",
				"In 1999 A. Odlyzko, M. Rubinstein, and M. Wolf formulated, on the basis of heuristic and empirical evidence, the conjecture that the numbers greater than 1 that are jumping champions are 4 and the sequence of primorials 2, 6, 30, 210, 2310, .... The authors pointed out that this conjecture is not a direct consequence of other deep conjectures concerning primes. Therefore they made a weaker and possibly more accessible conjecture, that any fixed prime p divides all sufficiently large jumping champions. In the present paper we shall extend the work of P. Erdos and E. G. Straus from 1980 to prove that this second conjecture follows directly from the prime pair conjecture of G. H. Hardy and J. E. Littlewood. [From _Jonathan Vos Post_, Oct 17 2009]"
			],
			"reference": [
				"I. Stewart, \"Jumping Champions\" in 'Scientific American' pp. 80-1 December 2000."
			],
			"link": [
				"R. P. Brent, \u003ca href=\"http://wwwmaths.anu.edu.au/~brent/pub/pub019.html\"\u003eThe First Occurrence of Large Gaps Between Successive Primes\u003c/a\u003e",
				"R. P. Brent, \u003ca href=\"http://wwwmaths.anu.edu.au/~brent/pub/pub021.html\"\u003eThe distribution of small gaps between successive primes\u003c/a\u003e",
				"R. P. Brent, \u003ca href=\"http://wwwmaths.anu.edu.au/~brent/pub/pub057.html\"\u003eThe first occurrence of certain large prime gaps\u003c/a\u003e",
				"C. K. Caldwell, The Prime Glossary, \u003ca href=\"http://primes.utm.edu/glossary/page.php/PrimeGaps.html\"\u003egaps between primes\u003c/a\u003e",
				"C. K. Caldwell, The Prime Glossary, \u003ca href=\"http://primes.utm.edu/glossary/page.php/JumpingChampion.html\"\u003eJumping champion\u003c/a\u003e",
				"S. Funkhouser, D. A. Goldston, D. Sengupta, J. Sengupta, \u003ca href=\"https://arxiv.org/abs/1612.02938\"\u003ePrime Difference Champions\u003c/a\u003e, arXiv:1612.02938 [math.NT], 2016.",
				"D. A. Goldston and A. H. Ledoan, \u003ca href=\"http://arxiv.org/abs/0910.2960\"\u003eJumping champions and gaps between consecutive primes\u003c/a\u003e, Oct 15, 2009. [From _Jonathan Vos Post_, Oct 17 2009]",
				"A. M. Odlyzko, M. Rubinstein \u0026 M. Wolf, \u003ca href=\"http://www.math.uwaterloo.ca/~mrubinst/publications/jumping.pdf\"\u003eJumping Champions\u003c/a\u003e",
				"A. M. Odlyzko, M. Rubinstein \u0026 M. Wolf, \u003ca href=\"http://citeseer.ist.psu.edu/correct/592237\"\u003eJumping Champions\u003c/a\u003e",
				"A. M. Odlyzko, M. Rubinstein \u0026 M. Wolf, CHANCE News 10.02, \u003ca href=\"http://www.dartmouth.edu/~chance/chance_news/recent_news/chance_news_10.02.html\"\u003e10. Jumping champions in the world of primes\u003c/a\u003e",
				"A. M. Odlyzko, M. Rubinstein and M. Wolf, \u003ca href=\"http://www.emis.de/journals/EM/expmath/volumes/8/8.html\"\u003eJumping Champions\u003c/a\u003e",
				"O. e Silva, \u003ca href=\"http://sweet.ua.pt/tos/gaps.html\"\u003eGaps between consecutive primes\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/JumpingChampion.html\"\u003eJumping Champion\u003c/a\u003e",
				"I. Stewart, \u003ca href=\"http://www.ift.uni.wroc.pl/~mwolf/SA1200MRec5Pd.pdf\"\u003eJumping Champions\u003c/a\u003e"
			],
			"formula": [
				"Consists of 4 and the primorials (A002110).",
				"a(1) = 2, a(2) = 4, a(3) = 6, a(n+1)/a(n) = Prime[n] for n\u003e2."
			],
			"mathematica": [
				"2,4,Table[Product[Prime[k],{k,1,n-1}],{n,3,30}]"
			],
			"program": [
				"(PARI) print1(\"2, 4\");t=2;forprime(p=3,97,print1(\", \",t*=p)) \\\\ _Charles R Greathouse IV_, Jun 11 2011"
			],
			"xref": [
				"Cf. A087103, A087104, A001223, A000230, A001632, A038664, A086977-A086980, A085237, A005250, A053686, A054587, A093737-A093753, A093972-A093984."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_Lekraj Beedassy_, Aug 10 2006",
			"ext": [
				"Corrected and extended by _Alexander Adamchuk_, Aug 11 2006",
				"Definition corrected and clarified by _Jonathan Sondow_, Aug 16 2011"
			],
			"references": 4,
			"revision": 23,
			"time": "2016-12-12T02:38:51-05:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}