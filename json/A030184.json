{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A030184",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 30184,
			"data": "1,-1,-1,-1,1,1,0,3,1,-1,-4,1,-2,0,-1,-1,2,-1,4,-1,0,4,0,-3,1,2,-1,0,-2,1,0,-5,4,-2,0,-1,-10,-4,2,3,10,0,4,4,1,0,8,1,-7,-1,-2,2,-10,1,-4,0,-4,2,-4,1,-2,0,0,7,-2,-4,12,-2,0,0,-8,3,10,10,-1",
			"name": "Expansion of eta(q) * eta(q^3) * eta(q^5) * eta(q^15) in powers of q.",
			"comment": [
				"Unique cusp form of weight 2 for congruence group Gamma_1(15). - _Michael Somos_, Aug 11 2011",
				"Coefficients of L-series for elliptic curve \"15a8\": y^2 + x*y + y = x^3 + x^2 or y^2 + x*y - y = x^3 + x^2 + x. - _Michael Somos_, Feb 01 2004",
				"Number 32 of the 74 eta-quotients listed in Table I of Martin (1996)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A030184/b030184.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"N. D. Elkies, \u003ca href=\"http://www.math.harvard.edu/~elkies/modular.pdf\"\u003eElliptic and modular curves over finite fields and related computational issues\u003c/a\u003e, in AMS/IP Studies in Advanced Math., 7 (1998), 21-76, esp. p. 70.",
				"LMFDB, \u003ca href=\"http://www.lmfdb.org/ModularForm/GL2/Q/holomorphic/15/2/0/\"\u003eNewforms of weight 2 on Gamma_0(15)\u003c/a\u003e.",
				"Y. Martin, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-96-01743-6\"\u003eMultiplicative eta-quotients\u003c/a\u003e, Trans. Amer. Math. Soc. 348 (1996), no. 12, 4825-4856, see page 4852 Table I.",
				"M. D. Rogers, \u003ca href=\"http://arxiv.org/abs/0806.3590\"\u003eHypergeometric formulas for lattice sums and Mahler measures\u003c/a\u003e.",
				"Michael Somos, \u003ca href=\"/A030203/a030203.txt\"\u003eIndex to Yves Martin's list of 74 multiplicative eta-quotients and their A-numbers\u003c/a\u003e"
			],
			"formula": [
				"Euler transform of period 15 sequence [ -1, -1, -2, -1, -2, -2, -1, -1, -2, -2, -1, -2, -1, -1, -4, ...]. - _Michael Somos_, May 02 2005",
				"a(n) is multiplicative with a(5^e) = 1, a(3^e) = (-1)^e, otherwise a(p^e) = a(p) * a(p^(e-1)) - p * a(p^(e-2)) where a(p) = p minus number of points of elliptic curve modulo p. - _Michael Somos_, Aug 13 2006",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2), A(x^4)) where f(u, v, w) = v^3 - u*w* (u + 2*v + 4*w). - _Michael Somos_, May 02 2005",
				"G.f. A(x) satisfies 2 * A(x^2) = -(A(x) + A(-x) + 4*A(x^4)), A(x^2)^3 = -A(x) * A(-x) * A(x^4). - _Michael Somos_, Feb 19 2007",
				"G.f.: x * Product_{k\u003e0} (1 - x^k) * (1 - x^(3*k)) * (1 - x^(5*k)) * (1 - x^(15*k))."
			],
			"example": [
				"G.f. = q - q^2 - q^3 - q^4 + q^5 + q^6 + 3*q^8 + q^9 - q^10 - 4*q^11 + q^12 - 2*q^13 - ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ q QPochhammer[ q] QPochhammer[ q^3] QPochhammer[ q^5] QPochhammer[ q^15], {q, 0, n}]; (* _Michael Somos_, Aug 11 2011 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, 0, ellak( ellinit([ 1, 1, 1, 0, 0], 1), n))}; /* _Michael Somos_, Aug 13 2006 */",
				"(PARI) {a(n) = my(A, p, e, x, y, a0, a1); if( n\u003c1, 0, A = factor(n); prod( k=1, matsize(A)[1], [p, e] = A[k, ]; if( p==3, (-1)^e, p==5, 1, a0=1; a1 = y = -if( p==2, 1, sum(x=0, p-1, kronecker( 4*x^3 + 5*x^2 + 2*x + 1, p))); for(i=2, e, x = y*a1 - p*a0; a0=a1; a1=x); a1)))}; /* _Michael Somos_, Aug 13 2006 */",
				"(PARI) {a(n) = my(A); if( n\u003c1, 0, n--; A = x * O(x^n); polcoeff( eta(x + A) * eta(x^3 + A) * eta(x^5 + A) * eta(x^15 + A), n))}; /* _Michael Somos_, May 02 2005 */",
				"(Sage) CuspForms( Gamma1(15), 2, prec = 76).0; # _Michael Somos_, Aug 11 2011",
				"(MAGMA) Basis( CuspForms( Gamma1(15), 2), 76)[1]; /* _Michael Somos_, Nov 20 2014 */"
			],
			"keyword": "sign,mult",
			"offset": "1,8",
			"author": "_N. J. A. Sloane_.",
			"references": 6,
			"revision": 45,
			"time": "2018-11-22T21:50:11-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}