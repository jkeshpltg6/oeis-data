{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A322230",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 322230,
			"data": "1,1,2,1,28,16,1,270,1032,272,1,2456,36096,52736,7936,1,22138,1035088,4766048,3646208,353792,1,199284,27426960,319830400,704357760,330545664,22368256,1,1793606,702812568,18598875760,93989648000,120536980224,38188155904,1903757312,1,16142512,17753262208,1002968825344,10324483102720,28745874079744,24060789342208,5488365862912,209865342976,1,145282674,445736371872,51882638754240,1013356176688128,5416305638467584,9498855414644736,5590122715250688,961530104709120,29088885112832",
			"name": "E.g.f.: S(x,k) = Integral C(x,k)*D(x,k)^2 dx, such that C(x,k)^2 - S(x,k)^2 = 1, and D(x,k)^2 - k^2*S(x,k)^2 = 1, as a triangle of coefficients read by rows.",
			"comment": [
				"Equals a row reversal of triangle A325220.",
				"Appears to be a row reversal of EG1 triangle A162005, which has other formulas.",
				"Compare to sn(x,k) = Integral cn(x,k)*dn(x,k) dx, where sn(x,k), cn(x,k), and dn(x,k) are Jacobi elliptic functions (see triangle A060628).",
				"Compare also to Michael Pawellek's generalized elliptic functions."
			],
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A322230/b322230.txt\"\u003eTable of n, a(n) for n = 0..860 terms of this triangle read by rows 0..40.\u003c/a\u003e"
			],
			"formula": [
				"E.g.f. S = S(x,k) = Sum_{n\u003e=0} Sum_{j=0..n} T(n,j) * x^(2*n+1) * k^(2*j) / (2*n+1)!, along with related series C = C(x,k) and D = D(x,k), satisfies:",
				"(1a) S = Integral C*D^2 dx.",
				"(1b) C = 1 + Integral S*D^2 dx.",
				"(1c) D = 1 + k^2 * Integral S*C*D dx.",
				"(2a) C^2 - S^2 = 1.",
				"(2b) D^2 - k^2*S^2 = 1.",
				"(3a) C + S = exp( Integral D^2 dx ).",
				"(3b) D + k*S = exp( k * Integral C*D dx ).",
				"(4a) S = sinh( Integral D^2 dx ).",
				"(4b) S = sinh( k * Integral C*D dx ) / k.",
				"(4c) C = cosh( Integral D^2 dx ).",
				"(4d) D = cosh( k * Integral C*D dx ).",
				"(5a) d/dx S = C*D^2.",
				"(5b) d/dx C = S*D^2.",
				"(5c) d/dx D = k^2 * S*C*D.",
				"From _Paul D. Hanna_, Mar 31 2019, Apr 20 2019 (Start):",
				"Given sn(x,k), cn(x,k), and dn(x,k) are Jacobi elliptic functions, with i^2 = -1, k' = sqrt(1-k^2), then",
				"(6a) S = -i * sn( i * Integral D dx, k),",
				"(6b) C = cn( i * Integral D dx, k),",
				"(6c) D = dn( i * Integral D dx, k).",
				"(7a) S = sc( Integral D dx, k') = sn(Integral D dx, k')/cn(Integral D dx, k'),",
				"(7b) C = nc( Integral D dx, k') = 1/cn(Integral D dx, k'),",
				"(7c) D = dc( Integral D dx, k') = dn(Integral D dx, k')/cn(Integral D dx, k'). (End)",
				"Row sums equal (2*n+1)!*(2*n)!/(n!^2*4^n) = A079484(n), the product of two consecutive odd double factorials.",
				"Main diagonal equals A000182, the tangent numbers."
			],
			"example": [
				"E.g.f.: S(x,k) = x + (2*k^2 + 1)*x^3/3! + (16*k^4 + 28*k^2 + 1)*x^5/5! + (272*k^6 + 1032*k^4 + 270*k^2 + 1)*x^7/7! + (7936*k^8 + 52736*k^6 + 36096*k^4 + 2456*k^2 + 1)*x^9/9! + (353792*k^10 + 3646208*k^8 + 4766048*k^6 + 1035088*k^4 + 22138*k^2 + 1)*x^11/11! + (22368256*k^12 + 330545664*k^10 + 704357760*k^8 + 319830400*k^6 + 27426960*k^4 + 199284*k^2 + 1)*x^13/13! + ...",
				"such that C(x,k)^2 - S(x,k)^2 = 1.",
				"This triangle of coefficients T(n,j) of x^(2*n+1)*k^(2*j)/(2*n+1)! in e.g.f. S(x,k) begins:",
				"1;",
				"1, 2;",
				"1, 28, 16;",
				"1, 270, 1032, 272;",
				"1, 2456, 36096, 52736, 7936;",
				"1, 22138, 1035088, 4766048, 3646208, 353792;",
				"1, 199284, 27426960, 319830400, 704357760, 330545664, 22368256;",
				"1, 1793606, 702812568, 18598875760, 93989648000, 120536980224, 38188155904, 1903757312;",
				"1, 16142512, 17753262208, 1002968825344, 10324483102720, 28745874079744, 24060789342208, 5488365862912, 209865342976; ...",
				"RELATED SERIES.",
				"The related series C(x,k), where C(x,k)^2 - S(x,k)^2 = 1, starts",
				"C(x,k) = 1 + x^2/2! + (8*k^2 + 1)*x^4/4! + (136*k^4 + 88*k^2 + 1)*x^6/6! + (3968*k^6 + 6240*k^4 + 816*k^2 + 1)*x^8/8! + (176896*k^8 + 513536*k^6 + 195216*k^4 + 7376*k^2 + 1)*x^10/10! + (11184128*k^10 + 51880064*k^8 + 39572864*k^6 + 5352544*k^4 + 66424*k^2 + 1)*x^12/12! + (951878656*k^12 + 6453433344*k^10 + 8258202240*k^8 + 2458228480*k^6 + 139127640*k^4 + 597864*k^2 + 1)*x^14/14! + ...",
				"The related series D(x,k), where D(x,k)^2 - k^2*S(x,k)^2 = 1, starts",
				"D(x,k) = 1 + k^2*x^2/2! + (5*k^4 + 4*k^2)*x^4/4! + (61*k^6 + 148*k^4 + 16*k^2)*x^6/6! + (1385*k^8 + 6744*k^6 + 2832*k^4 + 64*k^2)*x^8/8! + (50521*k^10 + 410456*k^8 + 383856*k^6 + 47936*k^4 + 256*k^2)*x^10/10! + (2702765*k^12 + 32947964*k^10 + 54480944*k^8 + 17142784*k^6 + 780544*k^4 + 1024*k^2)*x^12/12! + (199360981*k^14 + 3402510924*k^12 + 8760740640*k^10 + 5199585280*k^8 + 686711040*k^6 + 12555264*k^4 + 4096*k^2)*x^14/14! + ..."
			],
			"program": [
				"(PARI) N=10;",
				"{S=x;C=1;D=1; for(i=1,2*N, S = intformal(C*D^2 +O(x^(2*N+1))); C = 1 + intformal(S*D^2); D = 1 + k^2*intformal(S*C*D));}",
				"for(n=0,N, for(j=0,n, print1( (2*n+1)!*polcoeff(polcoeff(S,2*n+1,x),2*j,k),\", \")) ;print(\"\"))"
			],
			"xref": [
				"Cf. A000182 (diagonal), A162006, A162007, A162008, A162009, A162010.",
				"Cf. A322231 (C), A322232 (D).",
				"Cf. A325220 (row reversal), A162005."
			],
			"keyword": "nonn,tabl",
			"offset": "0,3",
			"author": "_Paul D. Hanna_, Dec 14 2018",
			"references": 5,
			"revision": 48,
			"time": "2019-04-26T20:39:49-04:00",
			"created": "2018-12-14T22:00:37-05:00"
		}
	]
}