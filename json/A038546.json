{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A038546",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 38546,
			"data": "0,1,5,43,48,53,3301,48515,348422,406665,1200207,6698641,190821326,2292141445,257125021372,5843866639660,45173327533483,46312809996150,59358981837795,129408997210988,1450344802530203,5710154240910003",
			"name": "Numbers n such that n-th Fibonacci number has initial digits n.",
			"comment": [
				"The Mathematica coding used by _Robert G. Wilson v_ implements Binet's Fibonacci number formula as suggested by _David W. Wilson_ and incorporates _Benoit Cloitre_'s use of logarithms to achieve a further increase in speed."
			],
			"link": [
				"Ron Knott, \u003ca href=\"http://www.maths.surrey.ac.uk/hosted-sites/R.Knott/Fibonacci/fib.html\"\u003eFibonacci Numbers and the Golden Section\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/FibonacciNumber.html\"\u003eFibonacci numbers\u003c/a\u003e"
			],
			"formula": [
				"n\u003e5 is in the sequence if a=(1+sqrt(5))/2 b=1/sqrt(5) and n==floor(b*(a^n)/10^(floor((log(b) +n*log(a))/log(10))-floor(log(n)/log(10))) ). - _Benoit Cloitre_, Feb 27 2002"
			],
			"example": [
				"a(3)=43 since 43rd Fibonacci number starts with 43 -\u003e {43}3494437.",
				"Fibonacci(53) is 53316291173, which begins with 53, so 53 is a term in the sequence."
			],
			"mathematica": [
				"a = N[ Log[10, Sqrt[5]/5], 24]; b = N [Log[10, GoldenRatio], 24]; Do[ If[ IntegerPart[10^FractionalPart[a + n*b]*10^Floor[ Log[10, n]]] == n, Print[n]], {n, 225000000}] (* _Robert G. Wilson v_, May 09 2005 *)",
				"(* confirmed with: *) fQ[n_] := (FromDigits[ Take[ IntegerDigits[ Fibonacci[n]], Floor[ Log[10, n] + 1]]] == n)"
			],
			"program": [
				"(PARI) /* To obtain terms \u003e 5: */ a=(1+sqrt(5))/2; b=1/sqrt(5); for(n=1,3500, if(n==floor(b*(a^n)/10^( floor(log(b *(a^n))/log(10))-floor(log(n)/log(10)))),print1(n,\",\"))) \\\\ _Benoit Cloitre_, Feb 27 2002"
			],
			"xref": [
				"Cf. A000045, A052000, A000350, A050816."
			],
			"keyword": "nonn,base,nice",
			"offset": "1,3",
			"author": "_Jeff Burch_",
			"ext": [
				"Term a(6) from _Patrick De Geest_, Oct 15 1999",
				"a(7) from _Benoit Cloitre_, Feb 27 2002",
				"a(8)-a(11) from _Robert G. Wilson v_, May 09 2005",
				"a(12) from _Robert G. Wilson v_, May 11 2005",
				"More terms from _Robert Gerbicz_, Aug 22 2006"
			],
			"references": 5,
			"revision": 23,
			"time": "2016-07-07T23:54:46-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}