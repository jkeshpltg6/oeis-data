{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001551",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1551,
			"id": "M3397 N1375",
			"data": "4,10,30,100,354,1300,4890,18700,72354,282340,1108650,4373500,17312754,68711380,273234810,1088123500,4338079554,17309140420,69107159370,276040692700,1102999460754,4408508961460,17623571298330,70462895745100,281757423024354",
			"name": "a(n) = 1^n + 2^n + 3^n + 4^n.",
			"comment": [
				"From _Wolfdieter Lang_, Oct 10 2011: (Start)",
				"a(n) = 2*A196836, n \u003e= 0.",
				"a(n)*(-1)^n, n \u003e= 0, gives the z-sequence of the Sheffer triangle A049459 ((signed) 4-restricted Stirling1) which is the inverse Sheffer triangle of A143496 with offset [0,0](4-restricted Stirling2). See the W. Lang link under A006232 for general Sheffer a- and z-sequences. The a-sequence of every (signed) r-restricted Stirling1 number Sheffer triangle is A027641/A027642 (Bernoulli numbers).",
				"(End)"
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), p. 813.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A001551/b001551.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=364\"\u003eEncyclopedia of Combinatorial Structures 364\u003c/a\u003e",
				"C. J. Pita Ruiz V., \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL16/Pita/pita19.html\"\u003eSome Number Arrays Related to Pascal and Lucas Triangles\u003c/a\u003e, J. Int. Seq. 16 (2013) #13.5.7.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992"
			],
			"formula": [
				"From _Wolfdieter Lang_, Oct 10 2011: (Start)",
				"E.g.f.: (1-exp(4*x))/(exp(-x)-1) = Sum_{j=1..4} exp(j*x) (trivial).",
				"O.g.f.: 2*(2-5*x)*(1-5*x+5*x^2)/(product(1-j*x,j=1..4) (via Laplace transformation of the o.g.f., and partial fraction decomposition backwards). See the Maple Program for the o.g.f. conjecture by _Simon Plouffe_. This has now been proved.",
				"(End)"
			],
			"maple": [
				"A001551:=-2*(5*z-2)*(5*z**2-5*z+1)/(z-1)/(3*z-1)/(2*z-1)/(4*z-1); # conjectured by _Simon Plouffe_ in his 1992 dissertation"
			],
			"mathematica": [
				"Table[Total[Range[4]^n], {n, 0, 40}] (* _T. D. Noe_, Oct 10 2011 *)"
			],
			"program": [
				"(Sage) [3**n + sigma(4, n) for n in range(23)]  # _Zerinvary Lajos_, Jun 04 2009"
			],
			"xref": [
				"Column 4 of array A103438."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_",
			"references": 7,
			"revision": 55,
			"time": "2021-03-12T22:32:34-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}