{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A193557",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 193557,
			"data": "1,-5,14,-36,85,-180,360,-684,1246,-2196,3754,-6264,10226,-16380,25804,-40032,61275,-92628,138452,-204804,300040,-435672,627356,-896400,1271525,-1791324,2507426,-3488472,4825531,-6638688,9085888,-12373992",
			"name": "Expansion of (1/q) * chi(-q) * chi(-q^3) * chi(-q^6)^4 / chi(q)^4 in powers of q where chi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A193557/b193557.txt\"\u003eTable of n, a(n) for n = -1..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of - (b(q^2) * c(q^2))^3 / (b(-q)^2 * c(-q) * b(q^4) * c(q^4)^2) in powers of q where b(), c() are cubic AGM functions.",
				"Expansion of eta(q)^5 * eta(q^3) * eta(q^4)^4 * eta(q^6)^3 / (eta(q^2)^9 * eta(q^12)^4) in powers of q.",
				"Euler transform of period 12 sequence [ -5, 4, -6, 0, -5, 0, -5, 0, -6, 4, -5, 0, ...].",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2)) where f(u, v) = (1+u)^2 * v^4 - u^4 * v^2 * (1+v) - 4*u^2 * (1+u) * (1+v) *(4+v) * (4+3*v).",
				"a(n) = -(-1)^n * A187198(n). a(n) = A193522(n) unless n=0. a(2*n) = -4 * A128643(n) unless n=0.",
				"a(n) ~ -(-1)^n * exp(2*Pi*sqrt(n/3)) / (2 * 3^(1/4) * n^(3/4)). - _Vaclav Kotesovec_, Sep 07 2017"
			],
			"example": [
				"1/q - 5 + 14*q - 36*q^2 + 85*q^3 - 180*q^4 + 360*q^5 - 684*q^6 + 1246*q^7 + ..."
			],
			"mathematica": [
				"eta[q_]:= q^(1/24)*QPochhammer[q]; a[n_] := SeriesCoefficient[eta[q]^5* eta[q^3]*eta[q^4]^4*eta[q^6]^3/(eta[q^2]^9*eta[q^12]^4), {q, 0, n}]; Table[a[n], {n, -1, 50}] (* _G. C. Greubel_, Apr 03 2018 *)"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c-1, 0, n++; A = x * O(x^n); polcoeff( eta(x + A)^5 * eta(x^3 + A) * eta(x^4 + A)^4 * eta(x^6 + A)^3 / (eta(x^2 + A)^9 * eta(x^12 + A)^4), n))}"
			],
			"xref": [
				"Cf. A128643, A187198, A193522."
			],
			"keyword": "sign",
			"offset": "-1,2",
			"author": "_Michael Somos_, Jul 30 2011",
			"references": 1,
			"revision": 13,
			"time": "2021-03-12T22:24:46-05:00",
			"created": "2011-07-30T15:16:22-04:00"
		}
	]
}