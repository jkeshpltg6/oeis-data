{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000441",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 441,
			"id": "M4613 N1968",
			"data": "0,1,9,34,95,210,406,740,1161,1920,2695,4116,5369,7868,9690,13640,16116,22419,25365,34160,38640,50622,55154,73320,77225,100100,107730,135576,141085,182340,184760,233616,243408,297738,301420,385110,377511,467210,478842",
			"name": "a(n) = Sum_{k=1..n-1} k*sigma(k)*sigma(n-k).",
			"comment": [
				"Apart from initial zero this is the convolution of A340793 and A143128. - _Omar E. Pol_, Feb 16 2021"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"J. Touchard, On prime numbers and perfect numbers, Scripta Math., 129 (1953), 35-39."
			],
			"link": [
				"Vaclav Kotesovec, \u003ca href=\"/A000441/b000441.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"J. Touchard, \u003ca href=\"/A000385/a000385.pdf\"\u003eOn prime numbers and perfect numbers\u003c/a\u003e, Scripta Math., 129 (1953), 35-39. [Annotated scanned copy]"
			],
			"formula": [
				"Convolution of A000203 with A064987. - _Sean A. Irvine_, Nov 14 2010",
				"G.f.: x*f(x)*f'(x), where f(x) = Sum_{k\u003e=1} k*x^k/(1 - x^k). - _Ilya Gutkovskiy_, Apr 28 2018",
				"a(n) = (n/24 - n^2/4)*sigma_1(n) + (5*n/24)*sigma_3(n). - _Ridouane Oudra_, Sep 17 2020"
			],
			"maple": [
				"S:=(n,e)-\u003eadd(k^e*sigma(k)*sigma(n-k),k=1..n-1);",
				"f:=e-\u003e[seq(S(n,e),n=1..30)];f(1); # _N. J. A. Sloane_, Jul 03 2015"
			],
			"mathematica": [
				"a[n_] := Sum[k*DivisorSigma[1, k]*DivisorSigma[1, n-k], {k, 1, n-1}]; Array[a, 40] (* _Jean-François Alcover_, Feb 08 2016 *)"
			],
			"program": [
				"(PARI) a(n) = sum(k=1, n-1, k*sigma(k)*sigma(n-k)); \\\\ _Michel Marcus_, Feb 02 2014"
			],
			"xref": [
				"Cf. A000499.",
				"Cf. A000203 (sigma_1), A001158 (sigma_3), A064987.",
				"Cf. A143128, A340793."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Sean A. Irvine_, Nov 14 2010",
				"a(1)=0 prepended by _Michel Marcus_, Feb 02 2014"
			],
			"references": 5,
			"revision": 59,
			"time": "2021-07-03T06:09:21-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}