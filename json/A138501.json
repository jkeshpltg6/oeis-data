{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A138501",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 138501,
			"data": "1,-4,8,-16,26,-32,48,-64,73,-104,120,-128,170,-192,208,-256,290,-292,360,-416,384,-480,528,-512,651,-680,656,-768,842,-832,960,-1024,960,-1160,1248,-1168,1370,-1440,1360,-1664,1682,-1536,1848,-1920,1898,-2112,2208,-2048,2353,-2604",
			"name": "Expansion of (eta(q)^2 * eta(q^4)^4 / eta(q^2)^3)^2 in powers of q.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A138501/b138501.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q * (phi(-q) * psi(q^2)^2)^2 in powers of q where phi(), psi() are Ramanujan theta functions.",
				"Euler transform of period 4 sequence [ -4, 2, -4, -6, ...].",
				"a(n) is multiplicative with a(2^e) = -(4^e) if e\u003e0, a(p^e) = ((p^2)^(e+1) - 1) / (p^2 - 1) if p == 1 (mod 4), a(p^e) = ((p^2)^(e+1) - (-1)^(e+1)) / (p^2 + 1) if p == 3 (mod 4).",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (4 t)) = 2 (t/i)^3 g(t) where q = exp(2 Pi i t) and g() is the g.f. for A138502.",
				"G.f.: x * (Product_{k\u003e0} (1 - x^k)^3 * (1 + x^k) * (1 + x^(2*k))^4)^2.",
				"a(n) = -(-1)^n * A050470(n)."
			],
			"example": [
				"G.f. = q - 4*q^2 + 8*q^3 - 16*q^4 + 26*q^5 - 32*q^6 + 48*q^7 - 64*q^8 + 73*q^9 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 1, 0, -(-1)^n DivisorSum[ n, #^2 Mod[n/#, 2] (-1)^Quotient[n/#, 2] \u0026]]; (* _Michael Somos_, Aug 26 2015 *)",
				"a[ n_] := SeriesCoefficient[ q (QPochhammer[ q]^2 QPochhammer[ q^4]^4 / QPochhammer[ q^2]^3)^2, {q, 0, n}]; (* _Michael Somos_, Aug 26 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, 0, -(-1)^n * sumdiv( n, d, d^2 * (n / d % 2) * (-1)^(n / d \\ 2)))};",
				"(PARI) {a(n) = my(A, p, e, f); if( n\u003c0, 0, A = factor(n); prod( k=1, matsize(A)[1], [p, e] = A[k, ]; if( p==2, -4^e, f = (-1)^(p\\2); ((p^2)^(e+1) - f^(e+1)) / (p^2 - f))))};",
				"(PARI) {a(n) = my(A); if( n\u003c1, 0, n--; A = x * O(x^n) ; polcoeff( (eta(x + A)^2 * eta(x^4 + A)^4 / eta(x^2 + A)^3)^2, n))};"
			],
			"xref": [
				"Cf. A050470, A138502."
			],
			"keyword": "sign,mult",
			"offset": "1,2",
			"author": "_Michael Somos_, Mar 20 2008",
			"references": 2,
			"revision": 13,
			"time": "2021-03-12T22:24:45-05:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}