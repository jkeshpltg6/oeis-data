{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A136689",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 136689,
			"data": "1,0,1,3,0,1,0,6,0,1,9,0,9,0,1,0,27,0,12,0,1,27,0,54,0,15,0,1,0,108,0,90,0,18,0,1,81,0,270,0,135,0,21,0,1,0,405,0,540,0,189,0,24,0,1,243,0,1215,0,945,0,252,0,27,0,1,0,1458,0,2835,0,1512,0",
			"name": "Triangular sequence of q-Fibonacci polynomials for s=3: F(x,n) = x*F(x,n-1) + s*F(x,n-2).",
			"comment": [
				"Row sums: 1, 1, 4, 7, 19, 40, 97, 217, 508, 1159, 2683, ... = A006130(n-1)."
			],
			"link": [
				"Nathaniel Johnston, \u003ca href=\"/A136689/b136689.txt\"\u003eRows n=1..36 of triangle, flattened\u003c/a\u003e",
				"J. Cigler, \u003ca href=\"http://www.fq.math.ca/Scanned/41-1/cigler.pdf\"\u003eq-Fibonacci polynomials\u003c/a\u003e, Fibonacci Quarterly 41 (2003) 31-40."
			],
			"formula": [
				"F(x,n) = x*F(x,n-1) + s*F(x,n-2), where F(x,0)=0, F(x,1)=1 and s=3."
			],
			"example": [
				"Triangle begins:",
				"    1;",
				"    0,   1;",
				"    3,   0,    1;",
				"    0,   6,    0,   1;",
				"    9,   0,    9,   0,   1;",
				"    0,  27,    0,  12,   0,   1;",
				"   27,   0,   54,   0,  15,   0,   1;",
				"    0, 108,    0,  90,   0,  18,   0,  1;",
				"   81,   0,  270,   0, 135,   0,  21,  0,  1;",
				"    0, 405,    0, 540,   0, 189,   0, 24,  0, 1;",
				"  243,   0, 1215,   0, 945,   0, 252,  0, 27, 0, 1;",
				"  ..."
			],
			"maple": [
				"A136689 := proc(n) option remember: if(n\u003c=1)then return n: else return x*procname(n-1)+3*procname(n-2): fi: end:",
				"seq(seq(coeff(A136689(n), x, m), m=0..n-1), n=1..10); # Nathaniel Johnston, Apr 27 2011"
			],
			"mathematica": [
				"s=2; F[x_, n_]:= F[x, n]= If[n\u003c2, n, x*F[x, n-1] + s*F[x, n-2]]; Table[",
				"CoefficientList[F[x, n], x], {n,10}]//Flatten",
				"F[n_, x_, s_, q_]:= Sum[QBinomial[n-j-1, j, q]*q^Binomial[j+1, 2]*x^(n-2*j-1) *s^j, {j, 0, Floor[(n-1)/2]}]; Table[CoefficientList[F[n,x,3,1], x], {n, 1, 10}]//Flatten (* _G. C. Greubel_, Dec 16 2019 *)"
			],
			"program": [
				"(Sage)",
				"def f(n,x,s,q): return sum( q_binomial(n-j-1, j, q)*q^binomial(j+1,2)*x^(n-2*j-1)*s^j for j in (0..floor((n-1)/2)))",
				"def A136689_list(prec):",
				"    P.\u003cx\u003e = PowerSeriesRing(ZZ, prec)",
				"    return P( f(n,x,3,1) ).list()",
				"[A136689_list(n) for n in (1..10)] # _G. C. Greubel_, Dec 16 2019"
			],
			"xref": [
				"Cf. A136688, A136705."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "1,4",
			"author": "_Roger L. Bagula_, Apr 06 2008",
			"references": 2,
			"revision": 18,
			"time": "2019-12-16T22:10:22-05:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}