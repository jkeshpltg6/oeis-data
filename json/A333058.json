{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A333058",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 333058,
			"data": "1,1,2,2,1,2,1,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0",
			"name": "0, 1, or 2 primes at primorial(n) +- 1.",
			"comment": [
				"a(n) = 0 marks a prime gap size of at least 2*prime(n+1)-1, e.g., primorial(8) +- prime(9) = {9699667,9699713} are primes, gap 2*23-1.",
				"Mathworld reports that it is not known if there are an infinite number of prime Euclid numbers.",
				"The tables in Ondrejka's collection contain no further primorial twin primes after {2309,2311} = primorial(13) +- 1 up to primorial(15877) +- 1 with 6845 digits."
			],
			"reference": [
				"H. Dubner, A new primorial prime, J. Rec. Math., 21 (No. 4, 1989), 276."
			],
			"link": [
				"Chris K. Caldwell, \u003ca href=\"https://primes.utm.edu/top20/page.php?id=5\"\u003ethe top 20: Primorial\u003c/a\u003e, 2012.",
				"H. Dubner \u0026 N. J. A. Sloane, \u003ca href=\"https://oeis.org/A002981/a002981.pdf\"\u003eCorrespondence, 1991,\u003c/a\u003e on A005234.",
				"G. L. Honaker, Jr. and Chris Caldwell, \u003ca href=\"https://primes.utm.edu/curios/page.php?short=30029\"\u003ePrime Curios! 30029\u003c/a\u003e.",
				"G. L. Honaker, Jr. and Chris Caldwell, \u003ca href=\"https://primes.utm.edu/curios/page.php?short=9699667\"\u003ePrime Curios! 9699667\u003c/a\u003e.",
				"Rudolf Ondrejka, \u003ca href=\"https://primes.utm.edu/lists/top_ten/topten.pdf\"\u003eThe Top Ten: a Catalogue of Primal Configurations\u003c/a\u003e, 2001, tables 20, 20A, 20B.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"https://mathworld.wolfram.com/PrimorialPrime.html\"\u003ePrimorial Prime\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"https://mathworld.wolfram.com/EuclidNumber.html\"\u003eEuclid Number\u003c/a\u003e."
			],
			"formula": [
				"a(n) = [ isprime(primorial(n) - 1) ] + [ isprime(primorial(n) + 1) ].",
				"a(n) = Sum_{i in {-1,1}} A010051(primorial(n) + i)."
			],
			"example": [
				"a(2) = a(3) = a(5) = 2: 2*3 +-1 = {5,7}, 6*5 +-1 = {29,31} and 210*11 +-1 = {2309,2311} are twin primes.",
				"a(1) = a(4) = a(6) = 1: 1, 30*7 - 1 = 209 and 2310*13 + 1 = 30031 are not primes.",
				"a(7) = 0: 510509 = 61 * 8369 and 510511 = 19 * 26869 are not primes."
			],
			"maple": [
				"p:= proc(n) option remember; `if`(n\u003c1, 1, ithprime(n)*p(n-1)) end:",
				"a:= n-\u003e add(`if`(isprime(p(n)+i), 1, 0), i=[-1, 1]):",
				"seq(a(n), n=0..120);  # _Alois P. Heinz_, Mar 18 2020"
			],
			"mathematica": [
				"primorial[n_] := primorial[n] = Times @@ Prime[Range[n]];",
				"a[n_] := Boole@PrimeQ[primorial[n] - 1] + Boole@PrimeQ[primorial[n] + 1];",
				"a /@ Range[0, 105] (* _Jean-François Alcover_, Nov 30 2020 *)"
			],
			"program": [
				"(Rexx)",
				"S = ''                     ;  Q = 1",
				"do N = 1 to 27",
				"   Q = Q * PRIME( N )",
				"   T = ISPRIME( Q - 1 ) + ISPRIME( Q + 1 )",
				"   S = S || ',' T",
				"end N",
				"S = substr( S, 3 )",
				"say S                      ;  return S"
			],
			"xref": [
				"Cf. A000040 (primes), A002110 (primorials, p#), A057706.",
				"Cf. A006862 (Euclid, p#+1), A005234 (prime p#+1), A014545 (index prime p#+1).",
				"Cf. A057588 (Kummer, p#-1), A006794 (prime p#-1), A057704 (index prime p#-1).",
				"Cf. A010051, A088411 (where a(n) is positive), A088257."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Frank Ellermann_, Mar 06 2020",
			"references": 0,
			"revision": 100,
			"time": "2020-11-30T14:49:32-05:00",
			"created": "2020-03-21T16:39:45-04:00"
		}
	]
}