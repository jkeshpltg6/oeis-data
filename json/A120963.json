{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A120963",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 120963,
			"data": "1,2,6,10,24,38,78,118,224,330,584,838,1420,2002,3258,4514,7134,9754,15010,20266,30532,40798,60280,79762,115966,152170,217962,283754,401250,518746,724866,930986,1287306,1643626,2250538,2857450,3878298,4899146,6594822",
			"name": "Number of monic polynomials with integer coefficients of degree n with all roots on the unit circle; number of products of cyclotomic polynomials of degree n.",
			"comment": [
				"Also the number of types of crystallographic rotations and reflection-rotations in n-dimensional Euclidean space. - _Andrey Zabolotskiy_, Jul 08 2017"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A120963/b120963.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e (first 1001 terms from T. D. Noe)",
				"Gaëtan Chenevier, \u003ca href=\"https://arxiv.org/abs/2002.03707\"\u003eThe Characteristic Masses of Niemeier Lattices\u003c/a\u003e, arXiv:2002.03707 [math.NT], 2020.",
				"Peter Engel, Louis Michel and Marjorie Senechal, \u003ca href=\"http://www.ihes.fr/~/vergne/LouisMichel/publications/LatticeGeometry.pdf\"\u003eLattice Geometry\u003c/a\u003e, 2004 (see section 1.4.3).",
				"D. Weigel, R. Veysseyre, T. Phan, J. M. Effantin, and Y. Billiet, \u003ca href=\"https://doi.org/10.1107/S0108767384000702\"\u003eCrystallography, geometry and physics in higher dimensions. I. Point-symmetry operations\u003c/a\u003e, Acta Cryst., A40 (1984), 323-330 (see table 3)."
			],
			"formula": [
				"Euler transform of A014197.",
				"G.f.: Product_{k\u003e=1} 1/(1-x^phi(k)) = Product_{j\u003e=1} (1-x^j)^(-A014197(j)). - _Christopher J. Smyth_, Jan 08 2017",
				"log(a(n)) ~ sqrt(105*zeta(3)*n)/Pi. - _Vaclav Kotesovec_, Sep 02 2021"
			],
			"example": [
				"The six polynomials of degree 2 consist of 3 irreducible cyclotomic polynomials: x^2+1, x^2+x+1 and x^2-x+1 and 3 products of 2 linear cyclotomic polynomials: x^2+2x+1, x^2-1 and x^2-2x+1.",
				"The six plane crystallographic operations are the identity operation, rotations by 2 Pi/k with k = 2,3,4,6, and a reflection."
			],
			"maple": [
				"with(numtheory):",
				"b:= proc(n) option remember; nops(invphi(n)) end:",
				"a:= proc(n) option remember; `if`(n=0, 1, add(",
				"      a(n-j)*add(d*b(d), d=divisors(j)), j=1..n)/n)",
				"    end:",
				"seq(a(n), n=0..40);  # _Alois P. Heinz_, Jul 04 2019"
			],
			"mathematica": [
				"max = 40; CoefficientList[Product[1/(1 - x^EulerPhi[k]), {k, 1, 5max}] + O[x]^max, x] (* _Jean-François Alcover_, Apr 14 2017, after _Christopher J. Smyth_ *)"
			],
			"xref": [
				"Cf. A014197, A051894, A280611 (variant where repeated roots are not allowed).",
				"See also A341710, A341711, A341712."
			],
			"keyword": "easy,nonn",
			"offset": "0,2",
			"author": "_Franklin T. Adams-Watters_, Jul 19 2006",
			"references": 12,
			"revision": 45,
			"time": "2021-09-02T04:26:23-04:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}