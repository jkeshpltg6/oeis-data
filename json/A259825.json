{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A259825",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 259825,
			"data": "-1,0,0,4,6,0,0,12,12,0,0,12,16,0,0,24,18,0,0,12,24,0,0,36,24,0,0,16,24,0,0,36,36,0,0,24,30,0,0,48,24,0,0,12,48,0,0,60,40,0,0,24,24,0,0,48,48,0,0,36,48,0,0,60,42,0,0,12,48,0,0,84,36,0,0",
			"name": "a(n) = 12*H(n) where H() is the Hurwitz class number.",
			"comment": [
				"Coefficients of q-expansion of Eisenstein series G_{3/2}(tau) multiplied by 12. - _N. J. A. Sloane_, Mar 16 2019"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A259825/b259825.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"K. Bringmann and J. Lovejoy, \u003ca href=\"http://arxiv.org/abs/0712.0631\"\u003eOverpartitions and class numbers of binary quadratic forms\u003c/a\u003e, arXiv:0712.0631 [math.NT], 2007. See page 5, equation (1.12).",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Hurwitz_class_number\"\u003eHurwitz class number\u003c/a\u003e.",
				"D. Zagier, \u003ca href=\"https://people.mpim-bonn.mpg.de/zagier/files/tex/UtrechtLectures/UtBook.pdf\"\u003eModular Forms of One Variable\u003c/a\u003e, Notes based on a course given in Utrecht, 1991. See page 50."
			],
			"formula": [
				"a(n) = 12 * A058305(n) / A058306(n). a(4*n + 1) = a(4*n + 2) = 0. a(3*n + 4) = 6 * A259827(n).",
				"a(4*n + 3) = 4 * A130695(n). a(8*n + 3) = A005886(n) = 2 * A005869(n) = 4 * A008443(n). a(12*n + 7) = 12 * A259655(n).",
				"a(16*n + 4) = 6 * A045834(n) = 3 * A005876(n). a(16*n + 8) = 12 * A045828(n) = 6 * A005884(n) = 3 * A005877(n).",
				"a(24*n + 3) = 4 * A213627(n). a(24*n + 7) = 12 * A185220(n). a(24*n + 11) = 12 * A213617(n). a(24*n + 19) = 12 * A181648(n). a(24*n + 23) = 12 * A188569(n+1).",
				"a(32*n + 4) = 6 * A213022(n). a(32*n + 8) = 12 * A213625(n). a(32*n + 12) = 16 * A008443(n) = 8 * A005869(n) = 4 * A005886(n) = 2 * A005878(n). a(32*n + 20) = 24 * A045831(n) = 6 * A004024(n). a(32*n + 24) = 24 * A213624(n).",
				"G.f.: -2 * (Sum_{k in Z} (-1)^k * x^(k*k + k) / (1 + (-x)^k)^2) / (Sum_{k in Z} x^k^2) - 2 * (sum_{k in Z} (-1)^k * x^(k^2 + 2*k) / (1 + x^(2*k))^2) / (Sum_{k in Z} (-x)^k^2)."
			],
			"example": [
				"G.f. = -1 + 4*x^3 + 6*x^4 + 12*x^7 + 12*x^8 + 12*x^11 + 16*x^12 + 24*x^15 + ..."
			],
			"mathematica": [
				"terms = 100; gf[m_] := With[{r = Range[-m, m]}, -2 Sum[(-1)^k*x^(k^2 + k)/(1 + (-x)^k)^2, {k, r}]/EllipticTheta[3, 0, x] - 2 Sum[(-1)^k*x^(k^2 + 2 k)/(1 + x^(2 k))^2, {k, r}]/EllipticTheta[3, 0, -x]]; gf[terms // Sqrt // Ceiling] + O[x]^terms // CoefficientList[#, x]\u0026 (* _Jean-François Alcover_, Apr 02 2017 *)"
			],
			"program": [
				"(PARI) {a(n) = 12 * qfbhclassno(n)};",
				"(PARI) {a(n) = my(D, f); 12 * if( n\u003c1, (n==0)/-12, [D, f] = core(-n, 1); if( D%4\u003e1 \u0026\u0026 !(f%2), D*=4; f/=2); if( D%4\u003c2, qfbclassno(D) / max(1, D+6), 0) * sumdiv(f, d, moebius(d) * kronecker(D, d) * sigma(f/d)))};"
			],
			"xref": [
				"Cf. A004024, A005869, A005876, A005877, A005878, A005884, A005886, A008443.",
				"Cf. A045828, A045831, A045834, A058305, A058306, A130695, A181648, A185220.",
				"Cf. A188569, A213022, A213617, A213624, A213625, A213627, A259827.",
				"See also A306934-A306937."
			],
			"keyword": "sign",
			"offset": "0,4",
			"author": "_Michael Somos_, Jul 05 2015",
			"references": 17,
			"revision": 26,
			"time": "2019-03-16T23:56:41-04:00",
			"created": "2015-07-05T20:53:21-04:00"
		}
	]
}