{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A020703",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 20703,
			"data": "1,4,3,2,9,8,7,6,5,16,15,14,13,12,11,10,25,24,23,22,21,20,19,18,17,36,35,34,33,32,31,30,29,28,27,26,49,48,47,46,45,44,43,42,41,40,39,38,37,64,63,62,61,60,59,58,57,56,55,54,53,52,51,50,81,80,79,78,77",
			"name": "Take the sequence of natural numbers (A000027) and reverse successive subsequences of lengths 1,3,5,7,...",
			"comment": [
				"Arrange A000027, the natural numbers, into a (square) spiral, say clockwise as shown in A068225. Read the numbers from the resulting counterclockwise spiral of the same shape that also begins with 1 and this sequence results. - _Rick L. Shepherd_, Aug 04 2006",
				"Contribution from _Hieronymus Fischer_, Apr 30 2012: (Start)",
				"The sequence may also be defined as follows: a(1)=1, a(n)=m^2 (where m^2 is the least square \u003e a(k) for 1\u003c=k\u003cn), if the minimal natural number not yet in the sequence is greater than a(n-1), else a(n)=a(n-1)-1.",
				"A reordering of the natural numbers.",
				"The sequence is self-inverse in that a(a(n))=n.",
				"(End)"
			],
			"reference": [
				"R. Honsberger, \"Ingenuity in Mathematics\", Table 10.4 on page 87.",
				"Suggested by correspondence with Michael Somos."
			],
			"link": [
				"Hieronymus Fischer, \u003ca href=\"/A020703/b020703.txt\"\u003eTable of n, a(n) for n = 1..11131\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"Contribution from _Hieronymus Fischer_, Apr 30 2012: (Start)",
				"a(n)=a(n-1)-1, if a(n-1)-1 \u003e 0 is not in the set {a(k)| 1\u003c=k\u003cn}, else a(n)=m^2, where m^2 is the least square not yet in the sequence.",
				"a(n)=n for n=k(k+1)+1, k\u003e=0.",
				"a(n+1)=(sqrt(a(n)-1)+2)^2, if a(n)-1 is a square, a(n+1)=a(n)-1, else.",
				"a(n)=2*(floor(sqrt(n-1))+1)*floor(sqrt(n-1))-n+2. (End)"
			],
			"example": [
				"a(2)=4=2^2, since 2^2 is the least square \u003e2=a(1) and the minimal number not yet in the sequence is 2\u003e1=a(1);",
				"a(8)=6=a(7)-1, since the minimal number not yet in the sequence (=5) is \u003c=7=a(7)."
			],
			"mathematica": [
				"Flatten[Table[Range[n^2,(n-1)^2+1,-1],{n,10}]] (* _Harvey P. Dale_, Jan 10 2016 *)",
				"With[{nn=20},Flatten[Reverse/@TakeList[Range[nn^2],Range[1,nn,2]]]] (* Requires Mathematica version 11 or later *) (* _Harvey P. Dale_, Jan 28 2019 *)"
			],
			"program": [
				"(PARI) a(n)=local(t); if(n\u003c1,0,t=sqrtint(n-1); 2*(t^2+t+1)-n)"
			],
			"xref": [
				"A self-inverse permutation of the natural numbers.",
				"Cf. A000027, A038722.",
				"Cf. A132666, A132664, A132665, A132674."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, May 02 2000",
			"references": 10,
			"revision": 24,
			"time": "2019-01-28T19:19:45-05:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}