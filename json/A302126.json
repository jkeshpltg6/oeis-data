{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A302126",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 302126,
			"data": "0,2,1,1,1,3,2,4,3,7,5,11,8,18,13,29,21,47,34,76,55,123,89,199,144,322,233,521,377,843,610,1364,987,2207,1597,3571,2584,5778,4181,9349,6765,15127,10946,24476,17711,39603,28657,64079,46368,103682,75025,167761",
			"name": "Interleaved Fibonacci and Lucas numbers.",
			"link": [
				"Colin Barker, \u003ca href=\"/A302126/b302126.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"T. Crilly, \u003ca href=\"http://www.jstor.org/stable/40378281\"\u003eInterleaving Integer Sequences\u003c/a\u003e, The Mathematical Gazette, Vol. 91, No. 520 (Mar., 2007), pp. 27-33.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Interleave_sequence\"\u003eInterleave sequence\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,1,0,1)."
			],
			"formula": [
				"a(0) = 0; a(1) = 2; a(2) = 1; a(3) = 1; a(n) = a(n-2) + a(n-4), n \u003e= 4.",
				"G.f.: x*(2 - x)*(1 + x) / (1 - x^2 - x^4). - _Colin Barker_, Apr 02 2018",
				"a(0) = 0; a(1) = 2; a(2n) = (a(2n-1) + a(2n-2))/2; a(2n+1) = a(2n) + 2*a(2n-2), n \u003e= 1. - _Daniel Forgues_, Jul 29 2018"
			],
			"example": [
				"a(10) = Fibonacci(5) = 5;",
				"a(11) = Lucas(5) = 11."
			],
			"maple": [
				"a:= n-\u003e (\u003c\u003c0|1\u003e, \u003c1|1\u003e\u003e^iquo(n, 2, 'r'). \u003c\u003c2*r, 1\u003e\u003e)[1, 1]:",
				"seq(a(n), n=0..60);  # _Alois P. Heinz_, Apr 23 2018"
			],
			"mathematica": [
				"Table[{Fibonacci[n], LucasL[n]}, {n, 0, 25}] // Flatten",
				"LinearRecurrence[{0, 1, 0, 1}, {0, 2, 1, 1}, 52]",
				"Flatten@ Array[{LucasL@#, Fibonacci@#} \u0026, 26, 0] (* or *)",
				"CoefficientList[Series[(x^3 - x^2 - 2x)/(x^4 + x^2 - 1), {x, 0, 51}], x] (* _Robert G. Wilson v_, Apr 02 2018 *)"
			],
			"program": [
				"(PARI) concat(0, Vec(x*(2 - x)*(1 + x) / (1 - x^2 - x^4) + O(x^60))) \\\\ _Colin Barker_, Apr 02 2018",
				"(GAP) Flat(List([1..25],n-\u003e[Fibonacci(n),Lucas(1,-1,n)[2]])); # _Muniru A Asiru_, Apr 02 2018"
			],
			"xref": [
				"Interleaves A000045 and A000032."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Patrick D McLean_, Apr 01 2018",
			"ext": [
				"More terms from _Colin Barker_, Apr 02 2018"
			],
			"references": 2,
			"revision": 40,
			"time": "2018-08-08T09:59:05-04:00",
			"created": "2018-04-07T03:57:13-04:00"
		}
	]
}