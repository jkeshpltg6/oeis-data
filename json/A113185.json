{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A113185",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 113185,
			"data": "1,1,-3,-2,1,1,6,-6,-7,7,-3,12,-2,-12,18,-2,9,-16,-21,20,1,12,-36,-22,14,1,36,-20,-6,30,6,32,-23,-24,48,-6,7,-36,-60,24,-7,42,-36,-42,12,7,66,-46,-18,43,-3,32,-12,-52,60,12,42,-40,-90,60,-2,62,-96,-42,41,-12,72,-66,-16,44,18,72,-49,-72,108,-2,20",
			"name": "Expansion of (5*phi(q)*phi^3(q^5) - phi^3(q)*phi(q^5))/4 in powers of q where phi(q) is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) := Prod_{k\u003e=1} (1-(-q)^k) (see A121373), phi(q) := theta_3(q) := Sum_{k=-oo..oo} q^(k^2) (A000122), psi(q) := Sum_{k=0..oo} q^(k*(k+1)/2) (A010054), chi(q) := Prod_{k\u003e=0} (1+q^(2k+1)) (A000700)."
			],
			"reference": [
				"B. C. Berndt, Ramanujan's Notebooks Part III, Springer-Verlag, see p. 249 Entry 8(ii)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A113185/b113185.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of (eta(q^2)^5 * eta(q^10)^7)/(eta(q)*eta(q^4)*eta(q^5)^3 * eta(q^20)^3) in powers of q.",
				"Euler transform of period 20 sequence [1, -4, 1, -3, 4, -4, 1, -3, 1, -8, 1, -3, 1, -4, 4, -3, 1, -4, 1, -4, ...].",
				"a(n) is multiplicative with a(5^e) = 1, a(2^e) = ((-2)^(e+1)-1)/(-2-1)-2 if e\u003e0, a(p^e) = (p^(e+1)-1)/(p-1) if p == 1, 9 (mod 10), a(p^e) = ((-p)^(e+1)-1)/(-p-1) if p == 3, 7 (mod 10).",
				"G.f.: (5phi(q) phi(q^5)^3 - phi(q)^3 phi(q^5))/4 where phi(q)=1+2(q+q^4+q^9+...).",
				"a(n) = (-1)^n * A132069(n)."
			],
			"example": [
				"1 + q - 3*q^2 - 2*q^3 + q^4 + q^5 + 6*q^6 - 6*q^7 - 7*q^8 + 7*q^9 + ..."
			],
			"mathematica": [
				"a[n_]:= SeriesCoefficient[(5*EllipticTheta[3, 0, q]*EllipticTheta[3, 0, q^5]^3 - EllipticTheta[3, 0, q]^3*EllipticTheta[3, 0, q^5])/4, {q, 0, n}]; Table[a[n], {n, 0, 50}] (* _G. C. Greubel_, Dec 16 2017 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c1, n==0, (-1)^n*sumdiv(n,d, kronecker(5,d)*d*(-1)^d))",
				"(PARI) {a(n)=local(A); if(n\u003c0, 0, A=x*O(x^n); polcoeff( eta(x^2+A)^5*eta(x^10+A)^7/(eta(x+A)*eta(x^4+A)*eta(x^5+A)^3*eta(x^20+A)^3), n))}",
				"(PARI) {a(n)=local(A,p,e,a1); if(n\u003c1, n==0, A=factor(n); prod(k=1,matsize(A)[1], if(p=A[k,1], e=A[k,2]; if(p==5, 1, if(p\u003e2, p*=kronecker(5,p); (p^(e+1)-1)/(p-1), a1=-3; for(i=2,e, a1=-2*a1-5);a1)))))}"
			],
			"keyword": "sign,mult",
			"offset": "0,3",
			"author": "_Michael Somos_, Oct 17 2005, Mar 20 2008",
			"references": 4,
			"revision": 13,
			"time": "2021-03-12T22:24:43-05:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}