{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A061002",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 61002,
			"data": "1,1,61,509,8431,39541,36093,375035183,9682292227,40030624861,1236275063173,6657281227331,2690511212793403,5006621632408586951,73077117446662772669,4062642402613316532391,46571842059597941563297,8437878094593961096374353",
			"name": "As p runs through the primes \u003e= 5, sequence gives { numerator of Sum_{k=1..p-1} 1/k } / p^2.",
			"comment": [
				"This is an integer by a theorem of Waring and Wolstenholme.",
				"Conjecture: If p is the n-th prime and H(n) is the n-th harmonic number, then denominator(H(p)/H(p-1))/numerator(H(p-1)/p^2) = p^3. A193758(p)/a(n) = p^3, p \u003e 3. - _Gary Detlefs_, Feb 20 2013",
				"The sequence which gives the numerators of H_{p-1} = Sum_{k=1..p-1} 1/k } for p prime \u003e= 5 is A076637. - _Bernard Schott_, Dec 02 2018"
			],
			"reference": [
				"Z. I. Borevich and I. R. Shafarevich, Number Theory. Academic Press, NY, 1966, p. 388 Problem 5.",
				"G. H. Hardy and E. M. Wright, An Introduction to the Theory of Numbers, 5th ed., Oxford Univ. Press, 1979, th. 115."
			],
			"link": [
				"Muniru A Asiru, \u003ca href=\"/A061002/b061002.txt\"\u003eTable of n, a(n) for n = 3..340\u003c/a\u003e",
				"R. Mestrovic, \u003ca href=\"http://arxiv.org/abs/1111.3057\"\u003eWolstenholme's theorem: Its Generalizations and Extensions in the last hundred and fifty years (1862-2011)\u003c/a\u003e, arXiv:1111.3057 [math.NT], 2011."
			],
			"formula": [
				"a(n) = A001008(p-1)/p^2, p=A000040(n). - _R. J. Mathar_, Jan 09 2017",
				"a(n) = A120285(n)/A001248(n). - _R. J. Mathar_, Jan 09 2017"
			],
			"maple": [
				"A061002:=proc(n) local p;",
				"  p:=ithprime(n);",
				"  (1/p^2)*numer(add(1/i,i=1..p-1));",
				"end proc;",
				"[seq(A061002(n),n=3..20)];"
			],
			"mathematica": [
				"Table[Function[p, Numerator[Sum[1/k, {k, p - 1}]/p^2]]@ Prime@ n, {n, 3, 20}] (* _Michael De Vlieger_, Feb 04 2017 *)"
			],
			"program": [
				"(GAP) List(List(Filtered([5..80],p-\u003eIsPrime(p)),i-\u003eSum([1..i-1],k-\u003e1/k)/i^2),NumeratorRat); # _Muniru A Asiru_, Dec 02 2018",
				"(PARI) a(n) = my(p=prime(n)); numerator(sum(k=1, p-1, 1/k))/p^2; \\\\ _Michel Marcus_, Dec 03 2018"
			],
			"xref": [
				"Cf. A000040, A001008, A001248, A120285, A185399, A193758."
			],
			"keyword": "nonn,easy",
			"offset": "3,3",
			"author": "_N. J. A. Sloane_, May 15 2001",
			"references": 25,
			"revision": 43,
			"time": "2018-12-27T13:54:25-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}