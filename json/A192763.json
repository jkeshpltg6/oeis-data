{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A192763",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 192763,
			"data": "1,2,2,1,-2,1,1,2,2,1,0,-2,-3,-2,0,1,2,1,1,2,1,0,-2,2,0,2,-2,0,0,2,-3,1,1,-3,2,0,0,-2,1,-2,-5,-2,1,-2,0,1,2,2,1,0,0,1,2,2,1,0,-2,-3,0,2,6,2,0,-3,-2,0,0,2,1,1,2,1,1,2,1,1,2,0,-1,-2,2,-2,1,-2,-7,-2,1,-2,2,-2,-1,0,2,-3,1,-5,-3,0,0,-3,-5,1,-3,2,0,1,-2,1,0,0,-2,2,0,2,-2,0,0,1,-2,1",
			"name": "Symmetric square array read by antidiagonals up.",
			"comment": [
				"The main diagonal is the Mobius function times the natural numbers A055615 (conjecture). For k\u003e1 the first row is the Mertens function + 2 = A002321 + 2 (conjecture). There is one recurrence for n=1 and k=1, and another recurrence for n\u003e1 and k\u003e1."
			],
			"link": [
				"M. Granvik, \u003ca href=\"http://math.stackexchange.com/questions/50719/\"\u003eIs this a recurrence for the Mertens function plus 2?\u003c/a\u003e"
			],
			"formula": [
				"T(1,1)=1 or 3, T(1,2)=2, T(2,1)=2, T(1,k)=(-T(n,k-1)-Sum_(i=2)^(k-1) of T(i,k))/(k+1)+T(n,k-1), T(n,1)=(-T(n-1,k)-Sum_(i=2)^(n-1) of T(n,i))/(n+1)+T(n-1,k), n\u003e=k: -Sum_(i=1)^(k-1) of T(n-i,k), n\u003ck: -Sum_(i=1)^(n-1) of T(k-i,n)."
			],
			"example": [
				"The array starts:",
				"1..2..1..1..0..1..0..0..0..1...",
				"2.-2..2.-2..2.-2..2.-2..2.-2...",
				"1..2.-3..1..2.-3..1..2.-3..1...",
				"1.-2..1..0..1.-2..1..0..1.-2...",
				"0..2..2..1.-5..0..2..2..1.-5...",
				"1.-2.-3.-2..0..6..1.-2.-3.-2...",
				"0..2..1..1..2..1.-7..0..2..1...",
				"0.-2..2..0..2.-2..0..0..0.-2...",
				"0..2.-3..1..1.-3..2..0..0..0...",
				"1.-2..1.-2.-5.-2..1.-2..0..10..."
			],
			"mathematica": [
				"Clear[t]; t[1, 1] = 1; t[2, 1] = t[1, 2] = 2; t[n_Integer, k_Integer] := t[n, k] = Which[n == 1, (-t[n, k - 1] - Sum[t[i, k], {i, 2, k - 1}])/(k + 1) +  t[n, k - 1], k == 1, (-t[n - 1, k] - Sum[t[n, i], {i, 2, n - 1}])/(n + 1) + t[n - 1, k], n \u003e= k, -Sum[t[n - i, k], {i, 1, k - 1}], True, -Sum[t[k - i, n], {i, 1, n - 1}]];",
				"nn = 12;",
				"MatrixForm[Array[t, {nn, nn}]];",
				"a = Flatten[Table[Reverse[Range[n]], {n, nn}]];",
				"b = Flatten[Table[Range[n], {n, nn}]];",
				"Table[t[a[[i]], b[[i]]], {i, 1, nn*(nn + 1)/2}]",
				"(* Mats Granvik, _Olivier Gérard_, Jul 10 2011 *)",
				"T[ n_, k_] := If[ n \u003c 1 || k \u003c 1, 0, If[ k \u003e n, T[ k, n], T[n, k] = If[ k == 1, If[ n \u003c 3, n, (-T[ n - 1, 1] - Sum[ T[ n, i], {i, 2, n - 1}]) / (n + 1) + T[ n - 1, 1]], If[ n \u003e k, T[ k, Mod[ n, k, 1]], - Sum[ T[ n, i], {i, n - 1}]]]]]; (* _Michael Somos_, Jul 19 2011 *)"
			],
			"program": [
				"(Excel cell formula, European version, American version uses \",\" instead of \";\")",
				"=if(and(row()=1;column()=1);1;if(or(and(row()=1;column()=2);and(row()=2;column()=1));2;if(row()=1;(-indirect(address(row();column()-1))-sum(indirect(address(2;column())\u0026\":\"\u0026address(column()-1;column()))))/(column()+1)+indirect(address(row();column()-1));if(column()=1;(-indirect(address(row()-1;column()))-sum(indirect(address(row();2)\u0026\":\"\u0026address(row();row()-1))))/(row()+1)+indirect(address(row()-1;column()));if(row()\u003e=column();-sum(indirect(address(row()-column()+1;column())\u0026\":\"\u0026address(row()-1; column())));-sum(indirect(address(column()-row()+1;row())\u0026\":\"\u0026address(column()-1;row()))))))))",
				"(PARI) {T(n, k) = if( n\u003c1 || k\u003c1, 0, if( k\u003en, T(k, n), if( k==1, if( n\u003c3, n, (-T(n-1, 1) -sum( i=2, n-1, T(n, i))) / (n+1) + T(n-1, 1)), if( n\u003ek, T(k, (n-1)%k+1), -sum( i=1,n-1, T(n, i))))))}; /* _Michael Somos_, Jul 19 2011 */"
			],
			"xref": [
				"Cf. A002321, A055615."
			],
			"keyword": "sign,tabl",
			"offset": "1,2",
			"author": "_Mats Granvik_, Jul 09 2011",
			"references": 2,
			"revision": 25,
			"time": "2016-12-10T17:00:02-05:00",
			"created": "2011-07-19T16:28:29-04:00"
		}
	]
}