{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A326306",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 326306,
			"data": "1,2,2,4,2,4,2,8,5,4,2,8,2,4,4,16,2,10,2,8,4,4,2,16,7,4,14,8,2,8,2,32,4,4,4,20,2,4,4,16,2,8,2,8,10,4,2,32,9,14,4,8,2,28,4,16,4,4,2,16,2,4,10,64,4,8,2,8,4,8,2,40,2,4,14,8,4,8,2,32,41,4,2,16,4",
			"name": "Dirichlet g.f.: zeta(s) * zeta(s-1) * Product_{p prime} (1 - p^(1 - s) + p^(-s)).",
			"comment": [
				"Inverse Moebius transform of A003557.",
				"Dirichlet convolution of A000203 with A097945."
			],
			"link": [
				"Vaclav Kotesovec, \u003ca href=\"/A326306/b326306.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"S. R. Finch, \u003ca href=\"https://arxiv.org/abs/math/0605019\"\u003eIdempotents and Nilpotents Modulo n\u003c/a\u003e, arXiv:math/0605019 [math.NT], 2006-2017."
			],
			"formula": [
				"G.f.: Sum_{k\u003e=1} (k / rad(k)) * x^k / (1 - x^k), where rad = A007947.",
				"a(n) = Sum_{d|n} A003557(d).",
				"a(n) = Sum_{d|n} mu(n/d) * phi(n/d) * sigma(d), where mu = A008683, phi = A000010 and sigma = A000203.",
				"a(p) = 2, where p is prime.",
				"From _Vaclav Kotesovec_, Jun 20 2020: (Start)",
				"Dirichlet g.f.: zeta(s) * Product_{primes p} (1 + 1/(p^s - p)).",
				"Dirichlet g.f.: zeta(s) * zeta(2*s-2) * Product_{primes p} (1 + p^(1-2*s) - p^(2-2*s) + p^(-s)). (End)",
				"Conjecture: Sum_{k=1..n} a(k) = O(n * log(n)^2). - _Vaclav Kotesovec_, Jun 22 2020",
				"Multiplicative with a(p^e) = 1 + (p^e-1)/(p-1). - _Amiram Eldar_, Oct 14 2020",
				"From _Richard L. Ollerton_, May 07 2021: (Start)",
				"a(n) = Sum_{k=1..n} mu(n/gcd(n,k))*sigma(gcd(n,k)).",
				"a(n) = Sum_{k=1..n} mu(gcd(n,k))*sigma(n/gcd(n,k))*phi(gcd(n,k))/phi(n/gcd(n,k)). (End)"
			],
			"mathematica": [
				"Table[Sum[d/Last[Select[Divisors[d], SquareFreeQ]], {d, Divisors[n]}], {n, 1, 85}]",
				"Table[Sum[MoebiusMu[n/d] EulerPhi[n/d] DivisorSigma[1, d], {d, Divisors[n]}], {n, 1, 85}]",
				"f[p_, e_] := 1 + (p^e-1)/(p-1); a[1] = 1; a[n_] := Times @@ (f @@@ FactorInteger[n]); Array[a, 100] (* _Amiram Eldar_, Oct 14 2020 *)"
			],
			"program": [
				"(PARI) for(n=1, 100, print1(direuler(p=2, n, (1 - p*X + X)/(1 - X)/(1 - p*X))[n], \", \")) \\\\ _Vaclav Kotesovec_, Jun 14 2020"
			],
			"xref": [
				"Cf. A000010, A000079 (fixed points), A000203, A003557, A007947, A008683, A098108 (parity of a(n)), A191750, A300717, A335032."
			],
			"keyword": "nonn,mult,easy",
			"offset": "1,2",
			"author": "_Ilya Gutkovskiy_, Oct 17 2019",
			"references": 3,
			"revision": 29,
			"time": "2021-05-07T06:56:30-04:00",
			"created": "2019-10-18T04:09:36-04:00"
		}
	]
}