{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A295288",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 295288,
			"data": "1,5,19,62,184,512,1360,3488,8704,21248,50944,120320,280576,647168,1478656,3350528,7536640,16842752,37421056,82706432,181927936,398458880,869269504,1889533952,4093640704,8841592832,19042140160,40902852608",
			"name": "Binomial transform of the centered triangular numbers A005448.",
			"comment": [
				"The sequence is column 3 of triangle in A207630.",
				"First difference is given by A055818(n+3,3) for n \u003e 0."
			],
			"reference": [
				"R. L. Graham, D. E. Knuth and O. Patashnik, Concrete Mathematics: A Foundation for Computer Science, Addison-Wesley, 1994."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A295288/b295288.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Luis Manuel Rivera, \u003ca href=\"https://arxiv.org/abs/1406.3081\"\u003eInteger sequences and k-commuting permutations\u003c/a\u003e, arXiv:1406.3081 [math.CO], 2014.",
				"C. Corsani, D. Merlini, R. Sprugnoli, \u003ca href=\"https://doi.org/10.1016/S0012-365X(97)00110-6\"\u003eLeft-inversion of combinatorial sums\u003c/a\u003e, Discrete Mathematics, 180 (1998) 107-122.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (6,-12,8)."
			],
			"formula": [
				"G.f.: (1 - x + x^2)/(1 - 2*x)^3.",
				"a(n+3) = 8*a(n) - 12*a(n+1) + 6*a(n+2).",
				"a(n+1) = 2*a(n) + 3*(n + 2)*2^(n-1).",
				"a(n+1) = 2*a(n) + 3*A001792(n) = 2*a(n) + A001787(n+2) - A001792(n).",
				"a(n) = (3*n^2 + 9*n + 8)*2^(n - 3).",
				"a(n) = (1/8)*A077588(n+2)*A000079(n)."
			],
			"example": [
				"a(0) = (3*0^2 + 9*0 + 8)*2^(-3) = 8/8 = 1."
			],
			"maple": [
				"A:=n-\u003e(3*n^2+9*n+8)*2^(n-3); seq(A(n), n=0..70);"
			],
			"mathematica": [
				"Table[(3 n^2 + 9 n + 8) 2^(n-3), {n, 0, 70}]",
				"LinearRecurrence[{6,-12,8}, {1,5,19}, 50] (* _G. C. Greubel_, Oct 17 2018 *)"
			],
			"program": [
				"(Maxima) makelist((3*n^2 + 9*n + 8)*2^(n - 3), n, 0, 70);",
				"(PARI) a(n) = (3*n^2 + 9*n + 8)*2^(n - 3) \\\\ _Felix Fröhlich_, Nov 19 2017",
				"(MAGMA) I:=[1,5,19]; [n le 3 select I[n] else 6*Self(n-1) -12*Self(n-2) +8*Self(n-3): n in [1..40]]; // _G. C. Greubel_, Oct 17 2018"
			],
			"xref": [
				"Cf. A000079, A001787, A001792, A005448, A055818, A077588, A207629, A207630."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Franck Maminirina Ramaharo_, Nov 19 2017",
			"references": 2,
			"revision": 20,
			"time": "2018-10-18T03:09:05-04:00",
			"created": "2018-01-11T04:34:53-05:00"
		}
	]
}