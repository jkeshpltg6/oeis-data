{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005704",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5704,
			"id": "M0639",
			"data": "1,2,3,5,7,9,12,15,18,23,28,33,40,47,54,63,72,81,93,105,117,132,147,162,180,198,216,239,262,285,313,341,369,402,435,468,508,548,588,635,682,729,783,837,891,954,1017,1080,1152,1224,1296,1377,1458,1539,1632",
			"name": "Number of partitions of 3n into powers of 3.",
			"comment": [
				"Infinite convolution product of [1,2,3,3,3,3,3,3,3,3] aerated A000244 - 1 times, i.e., [1,2,3,3,3,3,3,3,3,3] * [1,0,0,2,0,0,3,0,0,3] * [1,0,0,0,0,0,0,0,0,2] * ... [_Mats Granvik_, _Gary W. Adamson_, Aug 07 2009]"
			],
			"reference": [
				"R. K. Guy, personal communication.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A005704/b005704.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"G. E. Andrews, \u003ca href=\"http://dx.doi.org/10.1016/0022-314X(71)90051-5\"\u003eCongruence properties of the m-ary partition function\u003c/a\u003e, J. Number Theory 3 (1971), 104-110.",
				"G. E. Andrews and J. A. Sellers, \u003ca href=\"http://www.personal.psu.edu/gea1/pdf/300.pdf\"\u003eCharacterizing the number of p-ary partitions modulo a prime p\u003c/a\u003e, p. 2.",
				"C. Banderier, H.-K. Hwang, V. Ravelomanana and V. Zacharovas, \u003ca href=\"http://140.109.74.92/hk/wp-content/files/2012/07/mis-n-to-the-logn.pdf\"\u003eAnalysis of an exhaustive search algorithm in random graphs and the n^{c logn}-asymptotics\u003c/a\u003e, 2012. - From _N. J. A. Sloane_, Dec 23 2012",
				"R. K. Guy, \u003ca href=\"/A000123/a000123_1.pdf\"\u003eLetters to N. J. A. Sloane and J. W. Moon, 1988\u003c/a\u003e",
				"M. D. Hirschhorn and J. A. Sellers, \u003ca href=\"https://ajc.maths.uq.edu.au/pdf/30/ajc_v30_p193.pdf\"\u003eA different view of m-ary partitions\u003c/a\u003e, Australasian J. Combin., 30 (2004), 193-196.",
				"M. D. Hirschhorn and J. A. Sellers, \u003ca href=\"http://www.math.psu.edu/sellersj/mike-m-ary.pdf\"\u003eA different view of m-ary partitions\u003c/a\u003e",
				"D. Krenn, D. Ralaivaosaona, S. Wagner, \u003ca href=\"http://www.danielkrenn.at/downloads/multi-base-asy/multi-bases-asy-with-appendix-20140601.pdf\"\u003eThe Number of Multi-Base Representations of an Integer\u003c/a\u003e, 2014.",
				"D. Krenn, D. Ralaivaosaona, S. Wagner, \u003ca href=\"http://arxiv.org/abs/1503.08594\"\u003eMulti-Base Representations of Integers: Asymptotic Enumeration and Central Limit Theorems\u003c/a\u003e, arXiv:1503.08594 [math.NT], 2015. Also in \u003ca href=\"https://doi.org/10.2298/AADM150917018K\"\u003eApplicable Analysis and Discrete Mathematics\u003c/a\u003e (2015) Vol. 9, Issue 2, 285-312.",
				"M. Latapy, \u003ca href=\"https://www.dmtcs.org/dmtcs-ojs/index.php/proceedings/article/view/dmAA0116.1.html\"\u003ePartitions of an integer into powers\u003c/a\u003e, DMTCS Proceedings AA (DM-CCG), 2001, 215-228.",
				"M. Latapy, \u003ca href=\"/A005706/a005706.pdf\"\u003ePartitions of an integer into powers\u003c/a\u003e, DMTCS Proceedings AA (DM-CCG), 2001, 215-228. [Cached copy, with permission]",
				"O. J. Rodseth, \u003ca href=\"http://dx.doi.org/10.1017/S0305004100046259\"\u003eSome arithmetical properties of m-ary partitions\u003c/a\u003e, Proc. Camb. Phil. Soc. 68 (1970), 447-453.",
				"O. J. Rodseth and J. A. Sellers, \u003ca href=\"http://dx.doi.org/10.1006/jnth.2000.2594\"\u003eOn m-ary partition function congruences: A fresh look at a past problem\u003c/a\u003e, J. Number Theory 87 (2001), 270-281.",
				"O. J. Rodseth and J. A. Sellers, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL8/Sellers/sellers75.html\"\u003eOn a Restricted m-Non-Squashing Partition Function\u003c/a\u003e, Journal of Integer Sequences, Vol. 8 (2005), Article 05.5.4."
			],
			"formula": [
				"a(n) = a(n-1)+a(floor(n/3)).",
				"Coefficient of x^(3*n) in prod(k\u003e=0, 1/(1-x^(3^k))). Also, coefficient of x^n in prod(k\u003e=0, 1/(1-x^(3^k)))/(1-x). - _Benoit Cloitre_, Nov 28 2002",
				"a(n) mod 3 = binomial(2n, n) mod 3. - _Benoit Cloitre_, Jan 04 2004",
				"Let T(x) be the g.f., then T(x)=(1-x^3)/(1-x)^2*T(x^3). [_Joerg Arndt_, May 12 2010]"
			],
			"mathematica": [
				"Fold[Append[#1, Total[Take[Flatten[Transpose[{#1, #1, #1}]], #2]]] \u0026, {1}, Range[2, 55]] (* _Birkas Gyorgy_, Apr 18 2011 *)",
				"a[n_] := a[n] = If[n \u003c= 2, n + 1, a[n - 1] + a[Floor[n/3]]]; Array[a, 101, 0] (* _T. D. Noe_, Apr 18 2011 *)"
			],
			"xref": [
				"Cf. A000041, A000123, A005705, A005706, A018819.",
				"Cf. A006996."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Formula and more terms from _Henry Bottomley_, Apr 30 2001"
			],
			"references": 22,
			"revision": 65,
			"time": "2021-12-26T21:21:40-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}