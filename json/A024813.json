{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A024813",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 24813,
			"data": "4,7,12,15,18,23,26,29,34,37,40,45,48,51,54,59,62,65,70,73,76,81,84,87,92,95,98,103,106,109,114,117,120,125,128,131,136,139,142,147,150,153,158,161,164,169,172,175,180,183,186,191,194,197,202,205,208,213,216,219,224,227",
			"name": "Positive integers m for which f(m-1) \u003c f(m) \u003c f(m+1), where f(m) = floor(cot(Pi/(2m))).",
			"comment": [
				"Conjecture (verified for m \u003c 10^6 by _M. F. Hasler_): A024813(n) = 2*A024812(n) - n + 1, n=1,2,.... - _L. Edson Jeffery_, Mar 21 2013",
				"The above conjecture follows from the Laurent series for cot(x) = 1/x - x/3 + O(x^3) and the conjecture n/a(n) ~ 4/Pi-1. - _M. F. Hasler_, Mar 25 2013"
			],
			"link": [
				"Harvey P. Dale, \u003ca href=\"/A024813/b024813.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Robert G. Wilson v, \u003ca href=\"/A024813/a024813.pdf\"\u003eGraph of f(n), see Mathematica program\u003c/a\u003e"
			],
			"formula": [
				"n/a(n) ~ 4/Pi - 1 (as n -\u003e oo), or a(n) ~ 3.65979 n. (Conjectured.) - _M. F. Hasler_, Mar 25 2013",
				"Alternate formula: cot(Pi/(2m)) = tan((Pi/2)*(1 - 1/m)).",
				"Conjecture: a(n) = a(n-1) + a(n-3) - a(n-4); g.f.: x*(2*x^15-2*x^14-x^3+5*x^2+3*x+4) / ((x-1)^2*(x^2+x+1)). - _Colin Barker_, Jan 03 2014"
			],
			"mathematica": [
				"f[n_] := Floor[Tan[Pi (1 - 1/n)/2]]; Select[Range[2, 100], f[# - 1] \u003c f[#] \u003c f[# + 1] \u0026] (* _Robert G. Wilson v_, Mar 19 2013 *)",
				"Flatten[Position[Partition[Floor[Cot[Pi/(2Range[250])]],3,1],_?(Min[ Differences[ #]]\u003e0\u0026),{1},Heads-\u003eFalse]]+1 (* _Harvey P. Dale_, Feb 04 2016 *)"
			],
			"program": [
				"(PARI) {my(f(m)=floor(cotan(Pi/2/m))); for(m=2,99,f(m-1)\u003cf(m) \u0026 f(m)\u003cf(m+1) \u0026 print1(m\",\"))} \\\\ See comment in A024812; _M. F. Hasler_, Mar 20 2013"
			],
			"xref": [
				"A024812 yields the corresponding values of f."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Clark Kimberling_",
			"ext": [
				"Definition corrected by _M. F. Hasler_, following posts to the SeqFan list from _Harvey P. Dale_ and _Don Reble_, Mar 19 2013"
			],
			"references": 4,
			"revision": 45,
			"time": "2017-10-19T10:44:20-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}