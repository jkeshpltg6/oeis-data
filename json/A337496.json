{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A337496",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 337496,
			"data": "0,1,2,2,2,3,3,4,3,3,2,5,3,4,5,5,3,5,3,6,5,5,4,8,4,4,4,5,3,8,4,6,5,5,6,8,2,3,4,7,2,7,4,7,8,7,6,11,6,7,5,6,4,8,6,8,6,6,5,12,5,6,8,7,5,7,4,7,5,9,5,12,5,6,7,7,7,9,5,11,5,3,2,11,4,3,4,8,3,11,5",
			"name": "Number of bases b for which the expansion of n in base b contains the largest digit possible (i.e., the digit b-1).",
			"comment": [
				"An integer b \u003e 1 is a main base of n if n in base b contains the largest digit possible (i.e., the digit b-1).",
				"2 is a main base for all nonzero integers because in binary, they start with the digit 1.",
				"10 is a main base for all numbers with a 9 in their decimal expansion.",
				"b = n+1 is a main base of n when n \u003e 0.",
				"A000005(n+1) - 1 \u003c= a(n) \u003c= ceiling(sqrt(n+1)) + floor(A000005(n+1)/2) - 1 for n \u003e 0. Indeed, if b != 1 is a divisor of n+1 then n = (k-1)*b + b-1 so the last digit of n in base b is b-1. On the other side, n = q*b + r with r \u003c b. So if b is a main base of n then either r = b-1 (and b is a divisor of n+1) or b is a main base of q and therefore b-1 \u003c= q which implies (b-1)^2 \u003c n (i.e., b \u003c= floor(sqrt(n))+1 \u003c= ceiling(sqrt(n+1)) ). But if b \u003e sqrt(n+1) is a divisor of (n+1) then (n+1)/b \u003c sqrt(n+1) is another divisors of n+1 and only half of them can be greater than its square root. - _François Marques_, Dec 07 2020"
			],
			"link": [
				"François Marques, \u003ca href=\"/A337496/b337496.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Devansh Singh, \u003ca href=\"http://bit.ly/3cAsrjv\"\u003eLink for Python Program below with comments\u003c/a\u003e"
			],
			"formula": [
				"a(n) \u003c= (n+1)/2 for n \u003e= 3. - _Devansh Singh_, Sep 21 2020"
			],
			"example": [
				"For n = 7, a(7) = 4 because the main bases of 7 are 2, 3, 4 and 8 as shown in the table below:",
				"          Base b |   2 |   3 |   4 |   5 |   6 |   7 |   8",
				"-----------------+-----+-----+-----+-----+-----+-----+-----",
				"     7 in base b | 111 |  21 |  13 |  12 |  11 |  10 |   7",
				"-----------------+-----+-----+-----+-----+-----+-----+-----",
				"b is a main base | yes | yes | yes |  no |  no |  no | yes"
			],
			"maple": [
				"A337496 := proc(n)",
				"local k, r:=0;",
				"for k from 2 to n+1 do",
				"   if max(convert(n, base, k)) = k - 1 then",
				"      r++;",
				"   end if;",
				"end do;",
				"return r;",
				"end proc:",
				"seq(A337496(n), n=0..90);"
			],
			"mathematica": [
				"baseQ[n_, b_] := MemberQ[IntegerDigits[n, b], b - 1]; a[n_] := Count[Range[2, n + 1], _?(baseQ[n, #] \u0026)]; Array[a, 100, 0] (* _Amiram Eldar_, Sep 01 2020 *)"
			],
			"program": [
				"(PARI) a(n) = sum(b=2, n+1, vecmax(digits(n, b)) == b-1); \\\\ _Michel Marcus_, Aug 30 2020",
				"(PARI) a337496(n) = my(last_pos(v,k) = forstep(j=#v, 1, -1, if(v[j]==k, return(#v-j))); return(-1);, s=ceil(sqrt(n+1)), p); (n==0) + 1 + sum(b=2, s, p=last_pos(digits(n,b), b-1); if(p\u003c0,0, p==0,2, 1)) -((n+1)==s^2) -2*((n+1)==s*(s-1)); \\\\ _François Marques_, Dec 07 2020",
				"(Python)",
				"def A337496(N):",
				"    A337496_n=[0,1]",
				"    for j in range(2,N+1):",
				"        A337496_n.append(2)",
				"    for b in range(3,((N+1)//2) +1):",
				"        n=2*b-1",
				"        while n\u003c=N:",
				"            s=0",
				"            m=n//b",
				"            while m%b==b-2:",
				"                s=s+1",
				"                m=m//b",
				"            x=b*((b**s)-1)//(b-1)",
				"            for i in range(n, min(N,x+n)+1):",
				"                A337496_n[i]+=1",
				"            n=n+x+b",
				"    return(A337496_n)",
				"print(A337496(100)) # _Devansh Singh_, Dec 30 2020"
			],
			"xref": [
				"Cf. A077268 (contains digit 0)."
			],
			"keyword": "nonn,base,look",
			"offset": "0,3",
			"author": "_François Marques_, Aug 29 2020",
			"ext": [
				"Minor edits by _M. F. Hasler_, Oct 26 2020"
			],
			"references": 8,
			"revision": 117,
			"time": "2021-03-18T08:15:59-04:00",
			"created": "2020-09-01T16:47:51-04:00"
		}
	]
}