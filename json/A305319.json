{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A305319",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 305319,
			"data": "1,1,-1,1,-1,-1,1,-3,1,1,1,-2,-4,1,1,1,-4,-1,6,1,-1,1,-3,-8,3,9,1,-1,1,-5,-4,15,5,-11,-1,1,1,-4,-13,8,27,-3,-14,1,1,1,-6,-8,29,15,-42,-6,18,-1,-1,1,-5,-19,17,60,-19,-63,9,21,-1,-1,1,-7,-13,49,35,-110,-29,93,6,-25,-1,1,1,-6,-26,31,114,-58,-189,45,129,-10,-30,-1,1",
			"name": "Triangle T(n,k) read by rows: coefficients in order of decreasing exponents of characteristic polynomial P_n(t) of the matrix M(i,j) = [(i+j\u003en) or (i+j)=n-1], 1 \u003c= i,j \u003c= n.",
			"comment": [
				"Related to conjecture from entry A047211."
			],
			"link": [
				"Gheorghe Coserea, \u003ca href=\"/A305319/b305319.txt\"\u003eRows n = 0..200, flattened\u003c/a\u003e"
			],
			"formula": [
				"P(n) = det(t*I - M), where M(i,j) = [(i+j\u003en) or (i+j)=n-1], 1 \u003c= i,j \u003c= n.",
				"P(n) = (2*t + 3*(-1)^n)*P(n-1) - (t^2 - 4)*P(n-2) - (2*t + 3*(-1)^n)*P(n-3) - P(n-4).",
				"G.f.: A(x;t) = Sum_{n\u003e=0} P(n)*x^n = (t*x^8 + (-t^2 + t - 1)*x^7 + (-t^3 + t^2 + 2*t + 1)*x^6 + (t^4 - 2*t^3 + t^2 + 2*t)*x^5 - t^2*x^4 + (-t^3 - t^2 + 2*t)*x^3 + (-t^2 - t)*x^2 + (t - 1)*x + 1)/(x^8 + (-2*t^2 + 1)*x^6 + t^4*x^4 + (-2*t^2 + 1)*x^2 + 1)."
			],
			"example": [
				"P(0) = 1;",
				"P(1) = t - 1;",
				"P(2) = t^2 - t - 1;",
				"P(3) = t^3 - 3*t^2 + t + 1;",
				"P(4) = t^4 - 2*t^3 - 4*t^2 + t + 1;",
				"...",
				"Triangle starts:",
				"n\\k [0]  [1]  [2]  [3]  [4]   [5]   [6]  [7] [8]  [9] [10] [11] [12]",
				"[0]  1",
				"[1]  1,  -1;",
				"[2]  1,  -1,  -1;",
				"[3]  1,  -3,   1,   1;",
				"[4]  1,  -2,  -4,   1,   1;",
				"[5]  1,  -4,  -1,   6,   1,   -1;",
				"[6]  1,  -3,  -8,   3,   9,    1,   -1;",
				"[7]  1,  -5,  -4,  15,   5,  -11,   -1,   1;",
				"[8]  1,  -4, -13,   8,  27,   -3,  -14,   1,  1;",
				"[9]  1,  -6,  -8,  29,  15,  -42,   -6,  18, -1,  -1;",
				"[10  1,  -5, -19,  17,  60,  -19,  -63,   9, 21,  -1,  -1;",
				"[11] 1,  -7, -13,  49,  35, -110,  -29,  93,  6, -25,  -1,  1;",
				"[12] 1,  -6, -26,  31, 114,  -58, -189,  45, 29, -10, -30, -1,  1;",
				"...",
				"For n=7 the n X n matrix M (dots for zeros):",
				"  [. . . . 1 . 1]",
				"  [. . . 1 . 1 1]",
				"  [. . 1 . 1 1 1]",
				"  [. 1 . 1 1 1 1]",
				"  [1 . 1 1 1 1 1]",
				"  [. 1 1 1 1 1 1]",
				"  [1 1 1 1 1 1 1]",
				"has characteristic polynomial P(7) = det(tI-M) = t^7 - 5*t^6 - 4*t^5 + 15*t^4 + 5*t^3 - 11*t^2 - t + 1 (which is irreducible over Q: an elementary check shows that P(7)(25) = 4849680601 is a prime and 25 \u003e= 17 = 2 + max(abs([1,-5,-4,15,5,-11,-1,1])))."
			],
			"program": [
				"(PARI)",
				"P(n, t='t) = charpoly(matrix(n, n, i, j, (i+j \u003e n) || (i+j)==n-1), t);",
				"seq(N, t='t) = {",
				"  my(a=vector(N)); for (n=1, 4, a[n] = subst(P(n,'t), 't, t));",
				"  for (n=5, N,",
				"     a[n] +=  (2*t + 3*(-1)^(n%2))*a[n-1] - (t^2-4)*a[n-2];",
				"     a[n] += -(2*t + 3*(-1)^(n%2))*a[n-3] - a[n-4]);",
				"  a;",
				"};",
				"concat(1, concat(apply(p-\u003eVec(p), seq(12))))",
				"\\\\ test: N=100; vector(N, n, P(n)) == seq(N)"
			],
			"xref": [
				"Cf. A047211."
			],
			"keyword": "sign,tabl",
			"offset": "0,8",
			"author": "_Gheorghe Coserea_, May 30 2018",
			"references": 1,
			"revision": 29,
			"time": "2019-05-04T00:43:27-04:00",
			"created": "2018-05-31T03:43:42-04:00"
		}
	]
}