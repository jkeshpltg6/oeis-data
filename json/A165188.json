{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A165188",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 165188,
			"data": "1,0,3,1,6,3,11,6,18,11,27,18,39,27,54,39,72,54,94,72,120,94,150,120,185,150,225,185,270,225,321,270,378,321,441,378,511,441,588,511,672,588,764,672,864,764,972,864,1089,972,1215,1089,1350,1215,1495,1350",
			"name": "Interleaving of A014125 and zero followed by A014125.",
			"comment": [
				"This sequence convolved with A000217 (without initial term 0) yields A164680.",
				"See A164680 for similar convolutions.",
				"A165188 convolved with A000217 yields sequence A164680. This is to be expected since A000217 can be associated with partition 1+1+1, A164680 with partition 1+1+1+2+2+2+3 and A165188 with partition 2+2+2+3 by observing their unreduced generating functions and verified by generating the sequences by converting the partitions into finite sequences and using Euler's Transform. Thus partition 1+1+1 yields the finite sequence (3); partition 2+2+2+3 yields the finite sequence (0,3,1); and, when combined, partition 1+1+1+2+2+2+3 yields (3,3,1). - _Alford Arnold_, Sep 24 2009"
			],
			"link": [
				"\u003ca href=\"/index/Rec#order_09\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,3,1,-3,-3,1,3,0,-1)."
			],
			"formula": [
				"a(n) = -a(n-1)+2*a(n-2)+3*a(n-3)-3*a(n-5)-2*a(n-6)+a(n-7)+a(n-8)+1 for n \u003e 8; a(1)=1, a(2)=0, a(3)=3, a(4)=1, a(5)=6, a(6)=3, a(7)=11, a(8)=6. - _Klaus Brockhaus_, Sep 15 2009",
				"G.f.: x/((1-x)^4*(1+x)^3*(1+x+x^2)). - _Klaus Brockhaus_, Sep 15 2009",
				"a(n) = (2*n^3+21*n^2+63*n+49)/288-(-1)^n*(9+7*n+n^2)/32+A057078(n)/9. - _R. J. Mathar_, Sep 17 2009",
				"Euler transform of length 3 sequence [ 0, 3, 1]. - _Michael Somos_, Feb 01 2015",
				"G.f.: x / ((1 - x^2)^3 * (1 - x^3)). - _Michael Somos_, Feb 01 2015",
				"a(n) = -a(-7 - n) for all n in Z.",
				"a(n+3) - a(n) = 0 if n odd else (n+6) * (n+4) / 8. - _Michael Somos_, Feb 01 2015",
				"a(2*n + 1) = a(2*n + 4) = A014125(n) for all n in Z. - _Michael Somos_, Feb 01 2015"
			],
			"example": [
				"A014125 begins 1,3,6,11,18,27,..., thus this sequence begins 1,0,3,1,6,3,11,6,18,11,27,18,... .",
				"G.f. = x + 3*x^3 + x^4 + 6*x^5 + 3*x^6 + 11*x^7 + 6*x^8 + 18*x^9 + 11*x^10 + ..."
			],
			"mathematica": [
				"a[ n_] := Module[{s = 1, m = n}, If[ n \u003c 0, s = -1; m = -7 - n]; s SeriesCoefficient[ x / ((1 - x^2)^3 (1 - x^3)), {x, 0, m}]]; (* _Michael Somos_, Feb 01 2015 *)",
				"LinearRecurrence[{0,3,1,-3,-3,1,3,0,-1},{1,0,3,1,6,3,11,6,18},60] (* _Harvey P. Dale_, Apr 14 2018 *)"
			],
			"program": [
				"(PARI) /* first computes u = A014125 as second bisection of A001400, then interleaves */ {m=28; u=vector(m, n, polcoeff(1/((1-x)*(1-x^2)*(1-x^3)*(1-x^4))+O(x^(2*n)), 2*n-1)); vector(2*m, k, if(k%2==1,u[(k+1)/2], if(k==2, 0, u[k/2-1])))} /* _Klaus Brockhaus_, Sep 15 2009 */",
				"(PARI) {a(n) = my(s=1); if( n\u003c0, s=-1; n=-7-n); s * polcoeff( x / ((1 - x^2)^3 * (1 - x^3)) + x * O(x^n), n)}; /* _Michael Somos_, Feb 01 2015 */",
				"(MAGMA) I:=[1,0,3,1,6,3,11,6]; [n le 8 select I[n] else -Self(n-1)+2*Self(n-2)+3*Self(n-3)-3*Self(n-5)-2*Self(n-6)+Self(n-7)+Self(n-8)+1: n in [1..60]]; // _Vincenzo Librandi_, Jun 24 2015"
			],
			"xref": [
				"Cf. A014125, A000217, A164680, A001400."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Alford Arnold_, Sep 13 2009",
			"ext": [
				"Edited and extended by _Klaus Brockhaus_, Sep 15 2009",
				"Linear recurrence link and signature correct by _Michel Marcus_, Jun 25 2015"
			],
			"references": 4,
			"revision": 30,
			"time": "2018-04-14T09:34:48-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}