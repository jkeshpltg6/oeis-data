{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A263878",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 263878,
			"data": "0,-1,1,-5,7,-18,30,-61,107,-199,351,-628,1100,-1929,3349,-5801,9991,-17158,29354,-50085,85215,-144651,244991,-414120,698712,-1176913,1979305,-3323981,5574727,-9337914,15623286,-26111053,43594835,-72716239,121181919,-201779356",
			"name": "a(n) = Sum_{k=0..n} (-1)^k*k*Fibonacci(k), where Fibonacci(k) = A000045(k).",
			"link": [
				"Colin Barker, \u003ca href=\"/A263878/b263878.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Eric Weisstein's MathWorld, \u003ca href=\"http://mathworld.wolfram.com/FibonacciNumber.html\"\u003eFibonacci Number\u003c/a\u003e.",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (-1,3,1,-3,1)."
			],
			"formula": [
				"a(n) = (-1)^n*(F(n-3) + n*F(n-1)) - 2, where F(n) = A000045(n).",
				"G.f.: x*(x^2+1)/((x-1)*(x^2-x-1)^2).",
				"E.g.f.: (exp(x/phi)*(phi^3+x)+exp(-phi*x)*(1/phi^3-x))/sqrt(5)-2*exp(x), where phi=(1+sqrt(5))/2.",
				"Recurrences:",
				"6-term, homogeneous, constant coefficients: a(0) = 0, a(1) = -1, a(2) = 1, a(3) = -5, a(4) = 7, a(n) = -a(n-1) + 3*a(n-2) + a(n-3) - 3*a(n-4) + a(n-5).",
				"5-term, non-homogeneous, constant coefficients: a(0) = 0, a(1) = -1, a(2) = 1, a(3) = -5, a(n) = -2*a(n-1) + a(n-2) + 2*a(n-3) - a(n-4) - 2.",
				"4-term, homogeneous: a(0) = 0, a(1) = -1, a(2) = 1, (n-1)*(n-2)*a(n) = (2-n)*a(n-1) + n*(2*n-3)*a(n-2) + n*(1-n)*a(n-3).",
				"3-term, non-homogeneous: a(0) = 0, a(1) = -1, (n^2-1)*a(n) = -(n^2+n+1)*a(n-1) + n*(n+2)*a(n-2) - 2*n*(n-1).",
				"0 = a(n)*(-2*a(n) + 15*a(n+1) - 9*a(n+2) + a(n+3) - 3*a(n+4)) + a(n+1)*(-25*a(n+1) + 15*a(n+2) + 15*a(n+3) + 5*a(n+4)) + a(n+2)*(18*a(n+2) - 29*a(n+3) - 13*a(n+4)) + a(n+3)*(+3*a(n+3) + 7*a(n+4)) + a(n+4)*(2*a(n+4)) for all n in Z. - _Michael Somos_, Nov 02 2015"
			],
			"example": [
				"G.f. = - x + x^2 - 5*x^3 + 7*x^4 - 18*x^5 + 30*x^6 - 61*x^7 + 107*x^8 - 199*x^9 + ..."
			],
			"mathematica": [
				"Table[Sum[(-1)^k k Fibonacci[k], {k, 0, n}], {n, 0, 20}]",
				"Table[(-1)^n (Fibonacci[n-3] + n Fibonacci[n-1]) - 2, {n, 0, 20}]"
			],
			"program": [
				"(PARI) concat(0, Vec(x*(x^2+1)/((x-1)*(x^2-x-1)^2) + O(x^40))) \\\\ _Colin Barker_, Oct 31 2015",
				"(PARI) a(n) = (-1)^n*(fibonacci(n-3) + n*fibonacci(n-1)) - 2; \\\\ _Michel Marcus_, Nov 02 2015",
				"(MAGMA) [(-1)^n*(Fibonacci(n-3) + n*Fibonacci(n-1)) - 2: n in [0..30]]; // _G. C. Greubel_, Jul 30 2018"
			],
			"xref": [
				"Cf. A000045, A014286."
			],
			"keyword": "sign,easy",
			"offset": "0,4",
			"author": "_Vladimir Reshetnikov_, Oct 28 2015",
			"references": 1,
			"revision": 15,
			"time": "2018-07-31T00:22:16-04:00",
			"created": "2015-11-02T10:11:26-05:00"
		}
	]
}