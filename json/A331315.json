{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A331315",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 331315,
			"data": "1,1,1,1,1,1,1,2,2,1,1,4,14,4,1,1,8,150,128,8,1,1,16,2210,10848,1288,16,1,1,32,41642,1796408,933448,13472,32,1,1,64,956878,491544512,1852183128,85862144,143840,64,1,1,128,25955630,200901557728,7805700498776,2098614254048,8206774496,1556480,128,1",
			"name": "Array read by antidiagonals: A(n,k) is the number of nonnegative integer matrices with k columns and any number of nonzero rows with column sums n and columns in nonincreasing lexicographic order.",
			"comment": [
				"The condition that the columns be in nonincreasing order is equivalent to considering nonequivalent matrices up to permutation of columns.",
				"A(n,k) is the number of n-uniform k-block multisets of multisets."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A331315/b331315.txt\"\u003eTable of n, a(n) for n = 0..1325\u003c/a\u003e"
			],
			"formula": [
				"A(n,k) = Sum_{j=0..n*k} binomial(binomial(j+n-1,n)+k-1, k) * (Sum_{i=j..n*k} (-1)^(i-j)*binomial(i,j)).",
				"A(n, k) = Sum_{j=0..k} abs(Stirling1(k, j))*A316674(n, j)/k!.",
				"A(n, k) = Sum_{j=0..k} binomial(k-1, k-j)*A331278(n, j).",
				"A(n, k) = A011782(n) * A330942(n, k) for k \u003e 0.",
				"A317583(n) = Sum_{d|n} A(n/d, d)."
			],
			"example": [
				"Array begins:",
				"====================================================================",
				"n\\k | 0  1      2          3                4                  5",
				"----+---------------------------------------------------------------",
				"  0 | 1  1      1          1                1                  1 ...",
				"  1 | 1  1      2          4                8                 16 ...",
				"  2 | 1  2     14        150             2210              41642 ...",
				"  3 | 1  4    128      10848          1796408          491544512 ...",
				"  4 | 1  8   1288     933448       1852183128      7805700498776 ...",
				"  5 | 1 16  13472   85862144    2098614254048 140102945876710912 ...",
				"  6 | 1 32 143840 8206774496 2516804131997152 ...",
				"     ...",
				"The A(2,2) = 14 matrices are:",
				"  [1 0]  [1 0]  [1 0]  [2 0]  [1 1]  [1 0]  [1 0]",
				"  [1 0]  [0 1]  [0 1]  [0 1]  [1 0]  [1 1]  [1 0]",
				"  [0 1]  [1 0]  [0 1]  [0 1]  [0 1]  [0 1]  [0 2]",
				"  [0 1]  [0 1]  [1 0]",
				".",
				"  [1 0]  [1 0]  [2 1]  [2 0]  [1 1]  [1 0]  [2 2]",
				"  [0 2]  [0 1]  [0 1]  [0 2]  [1 1]  [1 2]",
				"  [1 0]  [1 1]"
			],
			"program": [
				"(PARI) T(n, k)={my(m=n*k); sum(j=0, m, binomial(binomial(j+n-1, n)+k-1, k)*sum(i=j, m, (-1)^(i-j)*binomial(i, j)))}"
			],
			"xref": [
				"Rows n=1..2 are A000012, A121227.",
				"Columns k=0..2 are A000012, A011782, A331397.",
				"The version with binary entries is A330942.",
				"The version with distinct columns is A331278.",
				"Other variations considering distinct rows and columns and equivalence under different combinations of permutations of rows and columns are:",
				"All solutions: A316674 (all), A331568 (distinct rows).",
				"Up to row permutation: A219727, A219585, A331161, A331160.",
				"Up to column permutation: this sequence, A331572, A331278, A331570.",
				"Nonisomorphic: A331485.",
				"Cf. A317583."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Andrew Howroyd_, Jan 13 2020",
			"references": 14,
			"revision": 10,
			"time": "2020-01-23T16:46:24-05:00",
			"created": "2020-01-14T22:16:32-05:00"
		}
	]
}