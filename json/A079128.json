{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A079128",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 79128,
			"data": "1,1,4,15,96,455,4320,29295,300160,2663199,36288000,348523175,5748019200,68027248575,1116542242816,16813959537375,334764638208000,4954072089341375,115242726703104000,1966765155600364119,45415699475660800000,930312555383281809375",
			"name": "Number of degree-n permutations with (mutually) relatively prime cycle lengths.",
			"comment": [
				"a(p) = p!-(p-1)! for prime p. Conjecture: a(n) is divisible by n^2-1 for n\u003e3.",
				"Conjecture: gcd(a(n),n)=1. - _Vladeta Jovovic_, Jan 25 2003"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A079128/b079128.txt\"\u003eTable of n, a(n) for n = 1..450\u003c/a\u003e"
			],
			"maple": [
				"with(combinat):",
				"b:= proc(n, i, g) option remember; `if`(n=0, `if`(g\u003e=2, 1, 0),",
				"      `if`(i\u003c2, 0, b(n, i-1, g) +`if`(igcd(g, i)\u003c2, 0,",
				"       add((i-1)!^j/j! *multinomial(n, i$j, n-i*j)*",
				"         b(n-i*j, i-1, igcd(i, g)), j=1..n/i))))",
				"    end:",
				"a:= n-\u003e n!-b(n, n, 0):",
				"seq(a(n), n=1..25);  # _Alois P. Heinz_, Jun 06 2013",
				"# second Maple program:",
				"b:= proc(n, g) option remember; `if`(n=0, `if`(g=1, 1, 0), add(",
				"      (j-1)!*b(n-j, igcd(g, j))*binomial(n-1, j-1), j=1..n))",
				"    end:",
				"a:= n-\u003e b(n, 0):",
				"seq(a(n), n=1..25);  # _Alois P. Heinz_, Jul 03 2021"
			],
			"mathematica": [
				"f[list_] :=",
				"Total[list]!/Apply[Times, Table[list[[i]], {i, 1, Length[list]}]]/",
				"  Apply[Times, Select[Table[Count[list, i], {i, 1, Total[list]}], # \u003e 0 \u0026]!];",
				"Table[Total[Map[f, Select[IntegerPartitions[n], Apply[GCD, #] == 1 \u0026]]], {n, 1, 25}] (* _Geoffrey Critzer_, Jun 06 2013 *)",
				"multinomial[n_, k_List] := n!/Times @@ (k!); b[n_, i_, g_] := b[n, i, g] = If[n==0, If[g \u003e= 2, 1, 0], If[i\u003c2, 0, b[n, i-1, g] + If[GCD[g, i]\u003c2, 0, Sum[(i-1)!^j/j!*multinomial[n, Append[Array[i\u0026, j], n-i*j]]*b[n-i*j, i-1, GCD[i, g]], {j, 1, n/i}]]]]; a[n_] := n! - b[n, n, 0]; Table[a[n], {n, 1, 25}] (* _Jean-François Alcover_, Jan 08 2016, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A079129, A000837, A079129, A226388."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Vladeta Jovovic_, _Vladimir Baltic_, Dec 27 2002",
			"references": 5,
			"revision": 26,
			"time": "2021-07-03T17:52:57-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}