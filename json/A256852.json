{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A256852",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 256852,
			"data": "1,0,1,0,0,0,2,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,2,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1,0,0,1,0,0,0,0,0,0,0,1,0,2,0,0,0,1,1,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0",
			"name": "Number of ways to write prime(n) = a^2 + b^4.",
			"comment": [
				"a(A049084(A028916(n))) \u003e 0; a(A049084(A256863(n))) = 0;",
				"Conjecture: a(n) \u003c= 2, empirically checked for the first 10^6 primes.",
				"The conjecture is true, because by the uniqueness part of Fermat's two-squares theorem, at most one duplicate of a^2 + b^4 can exist. Namely, if a is a square, say a = B^2, then a^2 + b^4 = A^2 + B^4 where A = b^2. - _Jonathan Sondow_, Oct 03 2015",
				"Friedlander and Iwaniec proved that a(n) \u003e 0 infinitely often. - _Jonathan Sondow_, Oct 05 2015"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A256852/b256852.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Art of Problem Solving, \u003ca href=\"http://www.artofproblemsolving.com/wiki/index.php/Fermat\u0026#39;s_Two_Squares_Theorem\"\u003eFermat's Two Squares Theorem\u003c/a\u003e",
				"John Friedlander and Henryk Iwaniec, \u003ca href=\"http://www.pnas.org/cgi/content/full/94/4/1054\"\u003eUsing a parity-sensitive sieve to count prime values of a polynomial\u003c/a\u003e, PNAS, vol. 94 no. 4, pp. 1054-1058.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Friedlander%E2%80%93Iwaniec_theorem\"\u003eFriedlander-Iwaniec theorem\u003c/a\u003e"
			],
			"example": [
				"First numbers n, such that a(n) \u003e 0:",
				".   k |  n |   prime(n)                    | a(n)",
				". ----+----+-------------------------------+-----",
				".   1 |  1 |    2 = 1^2 + 1^4              |   1",
				".   2 |  3 |    5 = 2^2 + 1^4              |   1",
				".   3 |  7 |   17 = 1^2 + 2^4 = 4^2 + 1^4  |   2",
				".   4 | 12 |   37 = 6^2 + 1^4              |   1",
				".   5 | 13 |   41 = 5^2 + 2^4              |   1",
				".   6 | 25 |   97 = 4^2 + 3^4 = 9^2 + 2^4  |   2",
				".   7 | 33 |  101 = 10^2 + 1^4             |   1",
				".   8 | 42 |  181 = 10^2 + 3^4             |   1",
				".   9 | 45 |  197 = 14^2 + 1^4             |   1",
				".  10 | 53 |  241 = 15^2 + 2^4             |   1",
				".  11 | 55 |  257 = 1^2 + 4^4 = 16^2 + 1^4 |   2",
				".  12 | 59 |  277 = 14^2 + 3^4             |   1",
				".  13 | 60 |  281 = 5^2 + 4^4              |   1",
				".  14 | 68 |  337 = 9^2 + 4^4 = 16^2 + 3^4 |   2",
				".  15 | 79 |  401 = 20^2 + 1^4             |   1",
				".  16 | 88 |  457 = 21^2 + 2^4             |   1 ."
			],
			"program": [
				"(Haskell)",
				"a256852 n = a256852_list !! (n-1)",
				"a256852_list = f a000040_list [] $ tail a000583_list where",
				"   f ps'@(p:ps) us vs'@(v:vs)",
				"     | p \u003e v     = f ps' (v:us) vs",
				"     | otherwise = (sum $ map (a010052 . (p -)) us) : f ps us vs'"
			],
			"xref": [
				"Cf. A000040, A000290, A000583, A010052, A002645, A028916, A256863."
			],
			"keyword": "nonn",
			"offset": "1,7",
			"author": "_Reinhard Zumkeller_, Apr 11 2015",
			"references": 5,
			"revision": 14,
			"time": "2015-10-05T10:12:13-04:00",
			"created": "2015-04-11T17:05:29-04:00"
		}
	]
}