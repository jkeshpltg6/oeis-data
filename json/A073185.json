{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A073185",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 73185,
			"data": "1,3,4,7,6,12,8,7,13,18,12,28,14,24,24,7,18,39,20,42,32,36,24,28,31,42,13,56,30,72,32,7,48,54,48,91,38,60,56,42,42,96,44,84,78,72,48,28,57,93,72,98,54,39,72,56,80,90,60,168,62,96,104,7,84,144,68,126,96,144,72",
			"name": "Sum of cubefree divisors of n.",
			"comment": [
				"Sum of divisors of the cubefree kernel of n (see first formula)."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A073185/b073185.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A000203(A007948(n)).",
				"a(n) \u003c= A073183(n).",
				"Multiplicative with a(p) = 1+p, a(p^e) = 1 + p + p^2, e\u003e1. - _Christian G. Bower_, May 18 2005",
				"a(n) = sum(A212793(A027750(n,k)) * A027750(n,k): k=1..A000005(n)). - _Reinhard Zumkeller_, May 27 2012",
				"Dirichlet g.f.: zeta(s)*zeta(s-1)/zeta(3s-3). - _R. J. Mathar_, Apr 12 2011",
				"Sum_{k=1..n} a(k) ~ Pi^2 * n^2 / (12*Zeta(3)). - _Vaclav Kotesovec_, Feb 01 2019"
			],
			"example": [
				"The divisors of 56 are {1, 2, 4, 7, 8, 14, 28, 56}, 8=2^3 and 56=7*2^3 are not cubefree, therefore a(56) = 1 + 2 + 4 + 7 + 14 + 28 = 56."
			],
			"maple": [
				"charFfree := proc(n,t) local f; for f in ifactors(n)[2] do if op(2,f) \u003e= t then return 0 ; end if; end do: return 1 ; end proc:",
				"A073185 := proc(n) add( d*charFfree(d,3),d =numtheory[divisors](n) ); end proc: # _R. J. Mathar_, Apr 12 2011"
			],
			"mathematica": [
				"nn = 71;f[list_, i_] := list[[i]]; a =Table[If[Max[FactorInteger[n][[All, 2]]] \u003c= 2, n, 0], {n, 1, nn}]; b = Table[1, {nn}]; Select[Table[DirichletConvolve[f[a, n], f[b, n], n, m], {m, 1, nn}], # \u003e 0 \u0026] (* _Geoffrey Critzer_, Mar 22 2015 *)",
				"f[p_, e_] := 1 + p + If[e \u003e 1, p^2, 0]; a[1] = 1; a[n_] := Times @@ f @@@ FactorInteger[n]; Array[a, 100] (* _Amiram Eldar_, Sep 14 2020 *)"
			],
			"program": [
				"(Haskell)",
				"a073185 = sum . filter ((== 1) . a212793) . a027750_row",
				"-- _Reinhard Zumkeller_, May 27 2012",
				"(PARI) a(n) = {my(f=factor(n)); for (i=1, #f~, p = f[i,1]; if ((e=f[i,2]) == 1, f[i,1] = 1+p, f[i,1] = 1+p+p^2); f[i,2] = 1;); factorback(f);} \\\\ _Michel Marcus_, Feb 06 2015"
			],
			"xref": [
				"Cf. A000203, A073184, A004709, A073182, A073181, A048250."
			],
			"keyword": "nonn,mult",
			"offset": "1,2",
			"author": "_Reinhard Zumkeller_, Jul 19 2002",
			"ext": [
				"Incorrect comment removed by _Álvar Ibeas_, Feb 06 2015"
			],
			"references": 4,
			"revision": 46,
			"time": "2020-09-14T02:58:31-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}