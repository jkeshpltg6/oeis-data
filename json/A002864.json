{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002864",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2864,
			"id": "M0847 N0322",
			"data": "0,0,1,1,2,3,7,18,41,123,367,1288,4878,19536,85263,379799,1769979,8400285,40619385,199631989,990623857,4976016485,25182878921",
			"name": "Number of alternating prime knots with n crossings.",
			"comment": [
				"Ortho Smith and Stuart Rankin, with coding by Peter de Vries, calculated a(21) = 990623857 on a Compaq ES 45 in just under 14 hours on Jul 01 2003 (Canada Day)."
			],
			"reference": [
				"See A002863 for many other references and links.",
				"J. H. Conway, An enumeration of knots and links and some of their algebraic properties. 1970. Computational Problems in Abstract Algebra (Proc. Conf., Oxford, 1967) pp. 329-358 Pergamon, Oxford.",
				"J. Hoste, M. B. Thistlethwaite and J. Weeks, The First 1,701,936 Knots, Math. Intell., 20, 33-48, Fall 1998.",
				"Stuart Rankin, Ortho Smith and John Schermann, Enumerating the Prime Alternating Knots, Part I, Journal of Knot Theory and its Ramifications, 13 (2004), 57-100.",
				"Stuart Rankin, Ortho Smith and John Schermann, Enumerating the Prime Alternating Knots, Part II, Journal of Knot Theory and its Ramifications, 13 (2004), 101-149.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"P. G. Tait, Scientific Papers, Cambridge Univ. Press, Vol. 1, 1898, Vol. 2, 1900, see Vol. 1, p. 345.",
				"M. B. Thistlethwaite, personal communication.",
				"M. B. Thistlethwaite, Knot tabulations and related topics. Aspects of topology, 1-76, London Math. Soc. Lecture Note Ser., 93, Cambridge Univ. Press, Cambridge-New York, 1985."
			],
			"link": [
				"See A002863 for many other references and links.",
				"D. Bar-Natan, \u003ca href=\"http://www.math.toronto.edu/~drorbn/KAtlas/Knots11/index.html\"\u003eThe Hoste-Thistlethwaite Table of 11 Crossing Knots\u003c/a\u003e",
				"S. R. Finch, \u003ca href=\"http://www.people.fas.harvard.edu/~sfinch/\"\u003eKnots, links and tangles\u003c/a\u003e",
				"S. R. Finch, \u003ca href=\"/A002863/a002863_4.pdf\"\u003eKnots, links and tangles\u003c/a\u003e, Aug 08 2003. [Cached copy, with permission of the author]",
				"Steven R. Finch, \u003ca href=\"https://doi.org/10.1017/9781316997741\"\u003eMathematical Constants II\u003c/a\u003e, Encyclopedia of Mathematics and Its Applications, Cambridge University Press, Cambridge, 2018, p. 627.",
				"Abdullah Khan, Alexei Lisitsa, Viktor Lopatkin, and Alexei Vernitski, \u003ca href=\"https://arxiv.org/abs/2108.02873\"\u003eCircle graphs (chord interlacement graphs) of Gauss diagrams: Descriptions of realizable Gauss diagrams, algorithms, enumeration\u003c/a\u003e, arXiv:2108.02873 [math.GT], 2021.",
				"W. B. R. Lickorish and K. C. Millett, \u003ca href=\"http://www.jstor.org/stable/2690324\"\u003eThe new polynomial invariants of knots and links\u003c/a\u003e, Math. Mag. 61 (1988), no. 1, 3-23.",
				"K. A. Perko, Jr., \u003ca href=\"https://doi.org/10.1090/S0002-9939-1974-0353294-X\"\u003eOn the classification of knots\u003c/a\u003e, Proc. Amer. Math. Soc., 45 (1974), 262-266.",
				"K. A. Perko, Jr., \u003ca href=\"/A002863/a002863.pdf\"\u003eCuadron's 1979 Knot Table\u003c/a\u003e, 2015 [Included with permission]",
				"Stuart Rankin, \u003ca href=\"http://www.math.uwo.ca/~srankin/knotprint.html\"\u003eKnot Theory Preprints of Ortho Smith and Stuart Rankin\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/A002863/a002863.gif\"\u003eIllustration of initial terms\u003c/a\u003e",
				"M. B. Thistlethwaite, \u003ca href=\"http://www.math.utk.edu/~morwen/index.html\"\u003eHome Page\u003c/a\u003e",
				"M. B. Thistlethwaite, \u003ca href=\"http://www.math.utk.edu/~morwen/png/link_stats.png\"\u003eNumbers of knots and links with up to 19 crossings\u003c/a\u003e",
				"University of Western Ontario Student Beowulf Initiative, \u003ca href=\"http://baldric.uwo.ca/article.php3?section=baldric\u0026amp;article=knots\"\u003eProject: Prime Knots\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/AlternatingKnot.html\"\u003eAlternating Knot.\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Knot.html\"\u003eKnot.\u003c/a\u003e",
				"\u003ca href=\"/index/K#knots\"\u003eIndex entries for sequences related to knots\u003c/a\u003e"
			],
			"xref": [
				"Cf. A002863. A diagonal of A059739."
			],
			"keyword": "nonn,hard,nice",
			"offset": "1,5",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Terms from Hoste et al. added by _Eric W. Weisstein_; further terms from M. B. Thistlethwaite, Feb 10 2001",
				"a(20) found by Ortho Smith and Stuart Rankin (srankin(AT)uwo.ca), with coding done by Peter De Vries, Jun 26 2003",
				"Ortho Smith and Stuart Rankin, with coding by Peter de Vries, calculated a(22) = 4976016485 on an Intel Xeon 2.8ghz in 41.5 hours on Jul 07 2003",
				"Ortho Flint and Stuart Rankin, with coding by Peter de Vries, calculated a(23) = 25182878921 on a Compaq ES 45 in 228 hours, finishing on Mar 14 2004"
			],
			"references": 10,
			"revision": 49,
			"time": "2021-12-30T00:24:03-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}