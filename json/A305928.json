{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A305928",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 305928,
			"data": "0,1,2,3,5,6,8,9,11,12,13,17,24,27,4,7,10,15,16,19,22,25,28,32,43,14,18,20,21,26,36,37,39,45,47,49,50,55,57,77,23,29,30,31,38,41,44,51,52,58,61,42,53,59,62,65,69,33,40,48,56,60,64,73,76,80,86,114,119,35,46",
			"name": "Irregular table: row n \u003e= 0 lists all k \u003e= 0 such that the decimal representation of 8^k has n digits '0' (conjectured).",
			"comment": [
				"The set of (nonempty) rows forms a partition of the nonnegative integers.",
				"Read as a flattened sequence, a permutation of the nonnegative integers.",
				"In the same way, another choice of (basis, digit, base) = (m, d, b) different from (8, 0, 10) will yield a similar partition of the nonnegative integers, trivial if m is a multiple of b.",
				"It remains an open problem to provide a proof that the rows are complete, in the same way as each of the terms of A020665 is unproved.",
				"We can also decide that the rows are to be truncated as soon as no term is found within a sufficiently large search limit. (For all of the displayed rows, there is no additional term up to many orders of magnitude beyond the last term.) That way the rows are well-defined, but it is no longer guaranteed to have a partition of the integers."
			],
			"link": [
				"M. F. Hasler, \u003ca href=\"https://oeis.org/wiki/Zeroless_powers\"\u003eZeroless powers.\u003c/a\u003e. OEIS Wiki, March 2014"
			],
			"formula": [
				"Row n consists of integers in row n of A305932 divided by 3."
			],
			"example": [
				"The table reads:",
				"n \\ k's",
				"0 : 0, 1, 2, 3, 5, 6, 8, 9, 11, 12, 13, 17, 24, 27 (= A030704)",
				"1 : 4, 7, 10, 15, 16, 19, 22, 25, 28, 32, 43",
				"2 : 14, 18, 20, 21, 26, 36, 37, 39, 45, 47, 49, 50, 55, 57, 77",
				"3 : 23, 29, 30, 31, 38, 41, 44, 51, 52, 58, 61",
				"4 : 42, 53, 59, 62, 65, 69",
				"5 : 33, 40, 48, 56, 60, 64, 73, 76, 80, 86, 114, 119",
				"...",
				"Column 0 is A063596: least k such that 8^k has n digits '0' in base 10.",
				"Row lengths are 14, 11, 15, 11, 6, 12, 10, 7, 14, 21, 9, 9, 15, 8, 6, 10, 8, 13, ... (not in the OEIS).",
				"The inverse permutation is (0, 1, 2, 3, 14, 4, 5, 15, 6, 7, 16, 8, 9, 10, 25, 17, 18, 11, 26, 19, 27, 28, 20, 40, 12, 21, 29, 13, 22, ...), also not in the OEIS."
			],
			"mathematica": [
				"mx = 1000; g[n_] := g[n] = DigitCount[8^n, 10, 0]; f[n_] := Select[Range@mx, g@# == n \u0026]; Table[f@n, {n, 0, 4}] // Flatten (* _Robert G. Wilson v_, Jun 20 2018 *)"
			],
			"program": [
				"(PARI) apply( A305928_row(n,M=50*(n+1))=select(k-\u003e#select(d-\u003e!d,digits(8^k))==n,[0..M]), [0..7])"
			],
			"xref": [
				"Cf. A030704, A063596.",
				"Cf. A305932 (analog for 2^k), A305933 (analog for 3^k), A305924 (analog for 4^k), ..., A305929 (analog for 9^k)."
			],
			"keyword": "nonn,base,tabf",
			"offset": "0,3",
			"author": "_M. F. Hasler_, Jun 19 2018",
			"references": 4,
			"revision": 14,
			"time": "2018-11-21T02:34:46-05:00",
			"created": "2018-06-23T07:24:34-04:00"
		}
	]
}