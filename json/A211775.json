{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A211775",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 211775,
			"data": "5419,5209,5003,4801,4603,4409,4219,4033,3851,3673,3499,3329,3163,3001,2843,2689,2539,2393,2251,2113,1979,1849,1723,1601,1483,1369,1259,1153,1051,953,859,769,683,601,523,449,379,313,251,193,139,89,43,1,-37,-71,-101",
			"name": "a(n) = 2*n^2 - 212*n + 5419.",
			"comment": [
				"A \"prime-generating\" polynomial: This polynomial generates 92 primes (57 being distinct) for n from 0 to 99 (in fact the next seven terms are still primes but we keep the range 0-99, customary for comparisons), just three primes fewer than the record held by Euler's polynomial for n = m-35, which is m^2 - 69*m + 1231 (see the link below).",
				"The nonprime terms in the first 100 are 1, 1369 = 37^2, 1849 = 43^2, 4033 = 37*109 (all taken twice).",
				"Setting n = 2*m+54 we obtain the polynomial 8*m^2 + 8*m - 197, which generates 31 primes in a row starting from m = 0 (the polynomial 8*m^2 - 488*m + 7243 generates the same 31 primes, but in reverse order).",
				"From _Charles Kusniec_, Nov 11 2016: The substitution n = m+53 converts this polynomial to the simpler form 2*m^2-199."
			],
			"reference": [
				"Joe L. Mott and Kermite Rose, Prime-Producing Cubic Polynomials in Lecture Notes in Pure and Applied Mathematics (Vol. 220), Marcel Dekker Inc., 2001, pages 281-317."
			],
			"link": [
				"Bruno Berselli, \u003ca href=\"/A211775/b211775.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Joe L. Mott and Kermite Rose, \u003ca href=\"http://webcache.googleusercontent.com/search?q=cache:TRI9WtS_BIQJ:www.math.fsu.edu/~aluffi/archive/paper134.ps.gz+%2295+primes%22+Boston\u0026amp;cd=2\u0026amp;hl=ro\u0026amp;ct=clnk\u0026amp;gl=ro\"\u003e Prime-Producing Cubic Polynomials\u003c/a\u003e",
				"E. W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/Prime-GeneratingPolynomial.html\"\u003eMathWorld: Prime-Generating Polynomial\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1)."
			],
			"formula": [
				"G.f.: (5419-11048*x+5633*x^2)/(1-x)^3. - _Bruno Berselli_, May 18 2012"
			],
			"maple": [
				"A211775:=n-\u003e2*n^2 - 212*n + 5419: seq(A211775(n), n=0..100); # _Wesley Ivan Hurt_, Jan 20 2017"
			],
			"mathematica": [
				"Table[2*n^2 - 212*n + 5419, {n, 0, 80}] (* _Wesley Ivan Hurt_, Aug 06 2017 *)"
			],
			"program": [
				"(MAGMA) [2*n^2-212*n+5419: n in [0..49]]; // _Bruno Berselli_, May 18 2012",
				"(PARI) Vec((5419-11048*x+5633*x^2)/(1-x)^3+O(x^99)) \\\\ _Charles R Greathouse IV_, Oct 01 2012",
				"(PARI) a(n) = 2*n^2 - 212*n + 5419 \\\\ _Charles R Greathouse IV_, Dec 19 2016"
			],
			"keyword": "sign,easy",
			"offset": "0,1",
			"author": "_Marius Coman_, May 18 2012",
			"ext": [
				"Edited by _N. J. A. Sloane_, Nov 12 2016"
			],
			"references": 2,
			"revision": 44,
			"time": "2017-08-06T17:38:32-04:00",
			"created": "2012-05-18T14:36:07-04:00"
		}
	]
}