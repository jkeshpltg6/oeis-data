{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A126869",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 126869,
			"data": "1,0,2,0,6,0,20,0,70,0,252,0,924,0,3432,0,12870,0,48620,0,184756,0,705432,0,2704156,0,10400600,0,40116600,0,155117520,0,601080390,0,2333606220,0,9075135300,0,35345263800,0,137846528820,0,538257874440,0",
			"name": "a(n) = Sum_{k = 0..n} binomial(n,floor(k/2))*(-1)^(n-k).",
			"comment": [
				"Hankel transform is 2^n. Successive binomial transforms are A002426, A000984, A026375, A081671, A098409, A098410.",
				"From _Andrew V. Sutherland_, Feb 29 2008: (Start)",
				"Counts returning walks of length n on a 1-d integer lattice with step set {-1,+1}.",
				"Moment sequence of the trace of a random matrix in G = SO(2). If X = tr(A) is a random variable (A distributed with Haar measure on G), then a(n) = E[X^n].",
				"Also the moment sequence of the trace of the k-th power of a random matrix in USp(2) = SU(2), for all k \u003e 2.",
				"(End)",
				"From _Paul Barry_, Aug 10 2009: (Start)",
				"The Hankel transform of 0,1,0,2,0,6,... is 0,-1,0,4,0,-16,0,... with general term I*(-4)^(n/2)(1 - (-1)^n)/4, I = sqrt(-1).",
				"The Hankel transform of 1,1,0,2,0,6,... (which has g.f. 1 + x/sqrt(1 - 4*x^2)) is A164111. (End)",
				"a(n) = A204293(2*n,n): central terms of the triangle in A204293. [_Reinhard Zumkeller_, Jan 14 2012]",
				"a(n) is the total number of closed walks (round trips) of length n on the graph P_N (a line with N nodes and N-1 edges), divided by N, in the limit N -\u003e infinity. See a comment on A198632 and a link under A201198. - _Wolfdieter Lang_, Oct 10 2012"
			],
			"reference": [
				"Lin Yang and S.-L. Yang, The parametric Pascal rhombus. Fib. Q., 57:4 (2019), 337-346."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A126869/b126869.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Francesc Fite, Kiran S. Kedlaya, Victor Rotger and Andrew V. Sutherland, \u003ca href=\"http://arxiv.org/abs/1110.6638\"\u003eSato-Tate distributions and Galois endomorphism modules in genus 2\u003c/a\u003e, arXiv preprint arXiv:1110.6638 [math.NT], 2011.",
				"Francesc Fite and Andrew V. Sutherland, \u003ca href=\"http://arxiv.org/abs/1203.1476\"\u003eSato-Tate distributions of twists of y^2= x^5-x and y^2= x^6+1\u003c/a\u003e, arXiv preprint arXiv:1203.1476 [math.NT], 2012. - From _N. J. A. Sloane_, Sep 14 2012",
				"Nikita Gogin and Mika Hirvensalo, \u003ca href=\"https://pca-pdmi.ru/2020/files/10/GoHi2020ExtAbstract.pdf\"\u003eOn the Moments of Squared Binomial Coefficients\u003c/a\u003e, (2020).",
				"Kiran S. Kedlaya and Andrew V. Sutherland, \u003ca href=\"http://arXiv.org/abs/0803.4462\"\u003eHyperelliptic curves, L-polynomials and random matrices\u003c/a\u003e, arXiv:0803.4462 [math.NT], 2008-2010."
			],
			"formula": [
				"From _Andrew V. Sutherland_, Feb 29 2008: (Start)",
				"a(2*n) = binomial(2*n,n) = A000984(n); a(2*n+1) = 0.",
				"a(n) = Sum_{k = 0..n} A107430(n,k)*(-1)^(n-k).",
				"a(n) = Sum_{k = 0..n} A061554(n,k)*(-1)^k.",
				"a(n) = (1/Pi)*Integral_{t = 0..Pi} cos^n(t) dt. (End)",
				"E.g.f.: I_0 (2x) where I_n(x) is the modified Bessel function as a function of x. - Benjamin Phillabaum, Mar 10 2011",
				"G.f.: A(x) = 1/sqrt(1 - 4*x^2) - _Vladimir Kruchinin_, Apr 16 2011",
				"a(n) = (1/Pi)*Integral{x = -2..2} x^n/sqrt((2 - x)*(2 + x)). - _Peter Luschny_, Sep 12 2011",
				"a(n) = (-1)^floor(n/2) Hypergeometric([-n,-n],[1], -1). - _Peter Luschny_, Nov 01 2011",
				"E.g.f.: E(0)/(1 - x) where E(k) = 1 - x/(1 - x/(x - (k+1)^2/E(k+1) )); (continued fraction). - _Sergei N. Gladkovskii_, Apr 05 2013",
				"E.g.f.: 1 + x^2/(Q(0) - x^2), where Q(k)= x^2 + (k+1)^2 - x^2*(k+1)^2/Q(k+1)); (continued fraction). - _Sergei N. Gladkovskii_, Apr 28 2013",
				"G.f.: 1/(1 - 2*x^2*Q(0)), where Q(k)= 1 + (4*k+1)*x^2/(k+1 - x^2*(2*k+2)*(4*k+3)/(2*x^2*(4*k+3) + (2*k+3)/Q(k+1))); (continued fraction). - _Sergei N. Gladkovskii_, May 15 2013",
				"G.f.: G(0)/2, where G(k)= 1 + 1/(1 - 2*x/(2*x + (k+1)/(x*(2*k+1))/G(k+1))); (continued fraction). - _Sergei N. Gladkovskii_, May 24 2013",
				"G.f.: G(0)/(1+x), where G(k) = 1 + x*(2+5*x)*(4*k+1)/((4*k+2)*(1+x)^2 - 2*(2*k+1)*(4*k+3)*x*(2+5*x)*(1+x)^2/((4*k+3)*x*(2+5*x) + 4*(k+1)*(1+x)^2/G(k+1) )); (continued fraction). - _Sergei N. Gladkovskii_, Jan 19 2014",
				"a(n) = 2^n*JacobiP(n,0,-1/2-n,-3)). - _Peter Luschny_, Aug 02 2014",
				"a(n) = (2^(n-1)*((-1)^n+1)*Gamma((n+1)/2))/(sqrt(Pi)*Gamma((n+2)/2)). - _Peter Luschny_, Sep 10 2014",
				"a(n) = n!*[x^n]hypergeom([],[1],x^2). - _Peter Luschny_, Jan 31 2015",
				"a(n) = 2^n*hypergeom([1/2,-n],[1],2). - _Peter Luschny_, Feb 03 2015",
				"From _Peter Bala_, Jul 25 2016: (Start)",
				"a(n) = (-1)^floor(n/2)*Sum_{k = 0..n} (-1)^k*binomial(n,k)^2.",
				"D-finite with recurrence: a(n) = 4*(n - 1)/n * a(n-2) with a(0) = 1, a(1) = 0. (End)",
				"From _Ilya Gutkovskiy_, Jul 25 2016: (Start)",
				"Inverse binomial transform of A002426.",
				"a(n) = Sum_{k=0..n} (-1)^k*A128014(k).",
				"a(n) ~ 2^n*((-1)^n + 1)/sqrt(2*Pi*n). (End)"
			],
			"example": [
				"a(4) = 6 {UUDD,UDUD,UDDU,DUUD,DUDU,DDUU}."
			],
			"maple": [
				"seq((-1)^(n/2)*pochhammer(-n,n/2)/(n/2)!, n=0..43); # _Peter Luschny_, May 17 2013",
				"seq(n!*coeff(series(hypergeom([],[1],x^2),x,n+1),x,n),n=0..42); # _Peter Luschny_, Jan 31 2015"
			],
			"mathematica": [
				"Table[(-1)^Floor[n/2] HypergeometricPFQ[{-n,-n},{1},-1],{n,0,30}] (* _Peter Luschny_, Nov 01 2011 *)"
			],
			"program": [
				"(Haskell)",
				"a126869 n = a204293_row (2*n) !! n  -- _Reinhard Zumkeller_, Jan 14 2012",
				"(Sage)",
				"A126869 = lambda n: (2^(n-1)*((-1)^n+1)*gamma((n+1)/2))/(sqrt(pi)*gamma((n+2)/2))",
				"[A126869(n) for n in range(44)] # _Peter Luschny_, Sep 10 2014"
			],
			"xref": [
				"This is A000984 with interspersed zeros. m-th binomial transforms of A000984: A126869 (m = -2), A002426 (m = -1 and m = -3 for signed version), A000984 (m = 0 and m = -4 for signed version), A026375 (m = 1 and m = -5 for signed version), A081671 (m = 2 and m = -6 for signed version), A098409 (m = 3 and m = -7 for signed version), A098410 (m = 4 and m = -8 for signed version), A104454 (m = 5 and m = -9 for signed version).",
				"Cf. A107430, A061554, A126120."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Philippe Deléham_, Mar 16 2007",
			"references": 51,
			"revision": 116,
			"time": "2021-08-30T08:06:59-04:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}