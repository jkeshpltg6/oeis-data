{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A004119",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 4119,
			"id": "M3308",
			"data": "1,4,7,13,25,49,97,193,385,769,1537,3073,6145,12289,24577,49153,98305,196609,393217,786433,1572865,3145729,6291457,12582913,25165825,50331649,100663297,201326593,402653185,805306369,1610612737,3221225473,6442450945,12884901889",
			"name": "a(0)=1; thereafter a(n) = 3*2^(n-1)+1.",
			"comment": [
				"Also Pisot sequence L(4,7) (cf. A008776).",
				"Alternatively, define the sequence S(a(1),a(2)) by: a(n+2) is the least integer such that a(n+2)/a(n+1) \u003e a(n+1)/a(n) for n \u003e 0. This is S(4,7).",
				"a(n) = number of terms of the arithmetic progression with first term 2^(2n-1) and last term 2^(2n+1). So common difference is 2^n. E.g., a(2)=7 corresponds to (8,12,16,20,24,28,32). - _Augustine O. Munagi_, Feb 21 2007",
				"Equals binomial transform of [1, 3, 0, 3, 0, 3, 0, 3,...]. - _Gary W. Adamson_, Aug 27 2010"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A004119/b004119.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"D. W. Boyd, \u003ca href=\"https://www.researchgate.net/profile/David_Boyd7/publication/262181133_Linear_recurrence_relations_for_some_generalized_Pisot_sequences_-_annotated_with_corrections_and_additions/links/00b7d536d49781037f000000.pdf\"\u003eLinear recurrence relations for some generalized Pisot sequences\u003c/a\u003e, Advances in Number Theory ( Kingston ON, 1991) 333-340, Oxford Sci. Publ., Oxford Univ. Press, New York, 1993",
				"S. W. Golomb, \u003ca href=\"http://www.jstor.org/stable/2005337\"\u003eProperties of the sequence 3.2^n+1\u003c/a\u003e, Math. Comp., 30 (1976), 657-663.",
				"S. W. Golomb, \u003ca href=\"/A004119/a004119.pdf\"\u003eProperties of the sequence 3.2^n+1\u003c/a\u003e, Math. Comp., 30 (1976), 657-663. [Annotated scanned copy]",
				"A. O. Munagi and T. Shonhiwa, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL11/Shonhiwa/shonhiwa13.html\"\u003e On the partitions of a number into arithmetic progressions\u003c/a\u003e, J. Integer Sequences, 11 (2008), #08.5.4.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3, -2)."
			],
			"formula": [
				"a(n) = 3a(n-1) - 2a(n-2).",
				"For n\u003e3, a(3)=13, a(n)= a(n-1)+2*floor(a(n-1)/2). - _Benoit Cloitre_, Aug 14 2002",
				"For n\u003e=1, a(n) = A049775(n+1)/2^(n-2). For n\u003e=2, a(n) = 2a(n-1)-1; see also A000051. - _Philippe Deléham_, Feb 20 2004",
				"O.g.f.: -(-1-x+3*x^2)/((2*x-1)*(x-1)). - _R. J. Mathar_, Nov 23 2007",
				"For n\u003e0, a(n) = 2*a(n-1)-1. - _Vincenzo Librandi_, Dec 16 2015"
			],
			"maple": [
				"A004119:=-(-1-z+3*z**2)/(2*z-1)/(z-1); # _Simon Plouffe_ in his 1992 dissertation"
			],
			"mathematica": [
				"s=4;lst={1,s};Do[s=s+(s-1);AppendTo[lst,s],{n,5!}];lst (* _Vladimir Joseph Stephan Orlovsky_, Nov 30 2009 *)",
				"Prepend[Table[3*2^n + 1, {n, 0, 32}], 1] (* or *)",
				"{1}~Join~LinearRecurrence[{3, -2}, {4, 7}, 33] (* _Michael De Vlieger_, Dec 16 2015 *)"
			],
			"program": [
				"(PARI) a(n)=3\u003c\u003cn+1 \\\\ _Charles R Greathouse IV_, Sep 28 2015",
				"(MAGMA) [1] cat [n le 1 select 4 else 2*Self(n-1)-1: n in [1..40]]; // _Vincenzo Librandi_, Dec 16 2015"
			],
			"xref": [
				"Cf. A049988, A049775, A000051, A008776.",
				"A181565 is an essentially identical sequence.",
				"For primes see A002253 and A039687."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, _R. K. Guy_",
			"ext": [
				"Edited by _N. J. A. Sloane_, Dec 16 2015 at the suggestion of _Bruno Berselli_."
			],
			"references": 18,
			"revision": 78,
			"time": "2021-03-12T22:32:36-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}