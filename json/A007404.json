{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007404",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7404,
			"data": "8,1,6,4,2,1,5,0,9,0,2,1,8,9,3,1,4,3,7,0,8,0,7,9,7,3,7,5,3,0,5,2,5,2,2,1,7,0,3,3,1,1,3,7,5,9,2,0,5,5,2,8,0,4,3,4,1,2,1,0,9,0,3,8,4,3,0,5,5,6,1,4,1,9,4,5,5,5,3,0,0,0,6,0,4,8,5,3,1,3,2,4,8,3,9,7,2,6,5,6,1,7,5,5,8",
			"name": "Decimal expansion of Sum_{n\u003e=0} 1/2^(2^n).",
			"comment": [
				"Kempner shows that numbers of a general form (which includes this constant) are transcendental. - _Charles R Greathouse IV_, Nov 07 2017"
			],
			"reference": [
				"M. J. Knight, An \"oceans of zeros\" proof that a certain non-Liouville number is transcendental, The American Mathematical Monthly, Vol. 98, No. 10 (1991), pp. 947-949."
			],
			"link": [
				"Harry J. Smith, \u003ca href=\"/A007404/b007404.txt\"\u003eTable of n, a(n) for n = 0..20000\u003c/a\u003e",
				"Boris Adamczewski, \u003ca href=\"http://www.emis.de/journals/JIS/VOL16/Adamczewski/adam6.html\"\u003eThe Many Faces of the Kempner Number\u003c/a\u003e, Journal of Integer Sequences, Vol. 16 (2013), #13.2.15.",
				"David H. Bailey, Jonathan M. Borwein, Richard E. Crandall, Carl Pomerance, \u003ca href=\"https://doi.org/10.5802/jtnb.457\"\u003eOn the Binary Expansions of Algebraic Numbers\u003c/a\u003e, Journal de Théorie des Nombres de Bordeaux, volume 16, number 3, 2004, pages 487-518.  Also \u003ca href=\"https://escholarship.org/uc/item/44t5s388\"\u003eLBNL-53854\u003c/a\u003e 2003, and authors' copies \u003ca href=\"http://www.davidhbailey.com/dhbpapers/algebraic.pdf\"\u003eone\u003c/a\u003e, \u003ca href=\"https://math.dartmouth.edu/~carlp/PDF/algebraic13.pdf\"\u003efour\u003c/a\u003e.",
				"D. H. Bailey and H. R. P. Ferguson, \u003ca href=\"/A007404/a007404.pdf\"\u003eNumerical results on relations between fundamental constants using a new algorithm\u003c/a\u003e, Mathematics of Computation, Vol.53 No. 188 (1989), 649-656. (Annotated scanned copy)",
				"F. R. Bernhart \u0026 N. J. A. Sloane, \u003ca href=\"/A006343/a006343.pdf\"\u003eEmails, April-May 1994\u003c/a\u003e",
				"Aubrey J. Kempner, \u003ca href=\"http://www.jstor.org/stable/1988833\"\u003eOn transcendental numbers\u003c/a\u003e, Transactions of the American Mathematical Society 17 (1916), pp. 476-482.",
				"Simon Plouffe, Plouffe's Inverter, \u003ca href=\"http://www.plouffe.fr/simon/constants/2n2n.txt\"\u003esum(1/2^(2^n), n=0..infinity); to 20000 digits\u003c/a\u003e",
				"Simon Plouffe, \u003ca href=\"http://www.worldwideschool.org/library/books/sci/math/MiscellaneousMathematicalConstants/chap5.html\"\u003esum(1/2^(2^n), n=0..infinity to 1024 digits\u003c/a\u003e",
				"Jeffrey Shallit, \u003ca href=\"https://doi.org/10.1016/0022-314X(79)90040-4\"\u003eSimple continued fractions for some irrational numbers\u003c/a\u003e. J. Number Theory 11 (1979), no. 2, 209-217.",
				"\u003ca href=\"/index/Tra#transcendental\"\u003eIndex entries for transcendental numbers\u003c/a\u003e"
			],
			"formula": [
				"Equals -Sum_{k\u003e=1} mu(2*k)/(2^k - 1) = Sum_{k\u003e=1, k odd} mu(k)/(2^k - 1). - _Amiram Eldar_, Jun 22 2020"
			],
			"example": [
				"0.81642150902189314370...."
			],
			"mathematica": [
				"RealDigits[ N[ Sum[1/2^(2^n), {n, 0, Infinity}], 110]] [[1]]"
			],
			"program": [
				"(PARI) default(realprecision, 20080); x=suminf(n=0, 1/2^(2^n)); x*=10; for (n=0, 20000, d=floor(x); x=(x-d)*10; write(\"b007404.txt\", n, \" \", d)); \\\\ _Harry J. Smith_, May 07 2009",
				"(PARI) suminf(k = 0, 1/(2^(2^k))) \\\\ _Michel Marcus_, Mar 26 2017",
				"(PARI) suminf(k=0,1.\u003e\u003e2^k) \\\\ _Charles R Greathouse IV_, Nov 07 2017"
			],
			"xref": [
				"Cf. A007400, A078885, A078585, A078886, A078887, A078888, A078889, A078890, A036987."
			],
			"keyword": "nonn,cons",
			"offset": "0,1",
			"author": "_Simon Plouffe_",
			"ext": [
				"Edited by _Robert G. Wilson v_, Dec 11 2002",
				"Deleted old PARI program _Harry J. Smith_, May 20 2009"
			],
			"references": 19,
			"revision": 51,
			"time": "2021-08-05T11:48:58-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}