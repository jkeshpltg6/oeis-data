{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A057077",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 57077,
			"data": "1,1,-1,-1,1,1,-1,-1,1,1,-1,-1,1,1,-1,-1,1,1,-1,-1,1,1,-1,-1,1,1,-1,-1,1,1,-1,-1,1,1,-1,-1,1,1,-1,-1,1,1,-1,-1,1,1,-1,-1,1,1,-1,-1,1,1,-1,-1,1,1,-1,-1,1,1,-1,-1,1,1,-1,-1,1,1,-1,-1",
			"name": "Periodic sequence 1,1,-1,-1; expansion of (1+x)/(1+x^2).",
			"comment": [
				"Sum_{k\u003e=0} a(k)/(k+1) = Sum_{k\u003e=0} 1/((a(k)*(k+1))) = log(2)/2 + Pi/4. - _Jaume Oliver Lafont_, Apr 30 2010",
				"Abscissa of the image produced after n alternating reflections of (1,1) over the x and y axes respectively.  Similarly, the ordinate of the image produced after n alternating reflections of (1,1) over the y and x axes respectively. - _Wesley Ivan Hurt_, Jul 06 2013"
			],
			"link": [
				"Alex Fink, Richard K. Guy, and Mark Krusemeyer, \u003ca href=\"https://doi.org/10.11575/cdm.v3i2.61940\"\u003ePartitions with parts occurring at most thrice\u003c/a\u003e, Contributions to Discrete Mathematics, Vol 3, No 2 (2008), pp. 76-114. See Section 13.",
				"T.-X. He, L. W. Shapiro, \u003ca href=\"http://dx.doi.org/10.1016/j.laa.2017.06.025\"\u003eFuss-Catalan matrices, their weighted sums, and stabilizer subgroups of the Riordan group\u003c/a\u003e, Lin. Alg. Applic. 532 (2017) 25-41, Theorem 2.5, k=2.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,-1).",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e"
			],
			"formula": [
				"G.f.: (1+x)/(1+x^2).",
				"a(n) = S(n, 0) + S(n-1, 0) = S(2*n, sqrt(2)); S(n, x) := U(n, x/2), Chebyshev polynomials of 2nd kind, A049310. S(n, 0)=A056594.",
				"a(n) = cos(n*Pi/2) + sin(n*Pi/2) with n \u003e= 0. - _Paolo P. Lava_, Jun 12 2006",
				"a(n) = (-1)^binomial(n,2) = (-1)^floor(n/2) = 1/2*((n+2) mod 4 - n mod 4). For fixed r = 0,1,2,..., it appears that (-1)^binomial(n,2^r) gives a periodic sequence of period 2^(r+1), the period consisting of a block of 2^r plus ones followed by a block of 2^r minus ones. See A033999 (r = 0), A143621 (r = 2) and A143622 (r = 3). Define E(k) = sum {n = 0..inf} a(n)*n^k/n! for k = 0,1,2,... . Then E(0) = cos(1) + sin(1), E(1) = cos(1) - sin(1) and E(k) is an integral linear combination of E(0) and E(1) (a Dobinski-type relation). Precisely, E(k) = A121867(k) * E(0) - A121868(k) * E(1). See A143623 and A143624 for the decimal expansions of E(0) and E(1) respectively. For a fixed value of r, similar relations hold between the values of the sums E_r(k) := Sum_{n\u003e=0} (-1)^floor(n/r)*n^k/n!, k = 0,1,2,... . For particular cases see A000587 (r = 1) and A143628 (r = 3). - _Peter Bala_, Aug 28 2008",
				"a(n) = (1/2)*((1-i)*i^n + (1+i)*(-i)^n), with i=sqrt(-1). - _Paolo P. Lava_, May 26 2010",
				"a(n) = (-1)^A180969(1,n), where the first index in A180969(.,.) is the row index. - _Adriano Caroli_, Nov 18 2010",
				"a(n) = (-1)^((2*n+(-1)^n-1)/4) = i^((n-1)*n), with i=sqrt(-1). - _Bruno Berselli_, Dec 27 2010 - Aug 26 2011",
				"Non-simple continued fraction expansion of (3+sqrt(5))/2 = A104457. - _R. J. Mathar_, Mar 08 2012",
				"E.g.f.: cos(x)*(1 + tan(x)). - _Arkadiusz Wesolowski_, Aug 31 2012",
				"From _Ricardo Soares Vieira_, Oct 15 2019: (Start)",
				"E.g.f.: sin(x) + cos(x) = sqrt(2)*sin(x + Pi/4).",
				"a(n) = sqrt(2)*(d^n/dx^n) sin(x)|_x=Pi/4, i.e., a(n) equals sqrt(2) times the n-th derivative of sin(x) evaluated at x=Pi/4. (End)"
			],
			"maple": [
				"seq((-1)^floor(k/2),k=0..70); # _Wesley Ivan Hurt_, Jul 06 2013"
			],
			"mathematica": [
				"a[n_] := {1, 1, -1, -1}[[Mod[n, 4] + 1]] (* _Jean-François Alcover_, Jul 05 2013 *)",
				"PadRight[{},80,{1,1,-1,-1}] (* _Harvey P. Dale_, Jun 21 2015 *)"
			],
			"program": [
				"(Maxima) A057077(n) := block(",
				"        [1,1,-1,-1][1+mod(n,4)]",
				")$ /* _R. J. Mathar_, Mar 19 2012 */",
				"(MAGMA) \u0026cat[[1, 1, -1, -1]^^20]; // _Vincenzo Librandi_, Feb 18 2016",
				"(PARI) a(n)=(-1)^(n\\2) \\\\ _Charles R Greathouse IV_, Nov 07 2016"
			],
			"xref": [
				"|a(n)|=A000012. Cf. A049310.",
				"Cf. A000587, A121867, A121868, A130151, A143621, A143622, A143623, A143624, A143628."
			],
			"keyword": "sign,easy",
			"offset": "0,1",
			"author": "_Wolfdieter Lang_, Aug 04 2000",
			"references": 85,
			"revision": 80,
			"time": "2019-10-30T22:40:18-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}