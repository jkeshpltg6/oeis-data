{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097780",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97780,
			"data": "1,25,624,15575,388751,9703200,242191249,6045078025,150884759376,3766073906375,94000962899999,2346257998593600,58562449001940001,1461714967049906425,36484311727245720624,910646078214093109175",
			"name": "Chebyshev polynomials S(n,25) with Diophantine property.",
			"comment": [
				"All positive integer solutions of Pell equation b(n)^2 - 621*a(n)^2 = +4 together with b(n)=A090733(n+1), n\u003e=0. Note that D=621=69*3^2 is not squarefree.",
				"For positive n, a(n) equals the permanent of the tridiagonal matrix with 25's along the main diagonal, and i's along the superdiagonal and the subdiagonal (i is the imaginary unit). - _John M. Campbell_, Jul 08 2011",
				"For n\u003e=1, a(n) equals the number of 01-avoiding words of length n-1 on alphabet {0,1,...,24}. - _Milan Janjic_, Jan 25 2015"
			],
			"link": [
				"Indranil Ghosh, \u003ca href=\"/A097780/b097780.txt\"\u003eTable of n, a(n) for n = 0..714\u003c/a\u003e",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (25,-1)."
			],
			"formula": [
				"a(n) = S(n, 25)=U(n, 25/2) = S(2*n+1, sqrt(25))/sqrt(25) with S(n, x)=U(n, x/2) Chebyshev's polynomials of the 2nd kind, A049310. S(-1, x)= 0 = U(-1, x).",
				"a(n) = 25*a(n-1)-a(n-2), n \u003e= 1; a(0)=1, a(1)=25; a(-1)=0.",
				"a(n) = (ap^(n+1) - am^(n+1))/(ap-am) with ap := (25+3*sqrt(69))/2 and am := (25-3*sqrt(69))/2.",
				"G.f.: 1/(1-25*x+x^2).",
				"a(n) = Sum_{k, 0\u003c=k\u003c=n} A101950(n,k)*24^k. - _Philippe Deléham_, Feb 10 2012",
				"Product {n \u003e= 0} (1 + 1/a(n)) = 1/23*(23 + 3*sqrt(69)). - _Peter Bala_, Dec 23 2012",
				"Product {n \u003e= 1} (1 - 1/a(n)) = 1/50*(23 + 3*sqrt(69)). - _Peter Bala_, Dec 23 2012"
			],
			"example": [
				"(x,y) = (2,0), (25;1), (623;25), (15550;624), ... give the nonnegative integer solutions to x^2 - 69*(3*y)^2 =+4."
			],
			"mathematica": [
				"LinearRecurrence[{25,-1},{1,25},20] (* _Harvey P. Dale_, Aug 23 2021 *)"
			],
			"program": [
				"(Sage) [lucas_number1(n,25,1) for n in range(1,20)] # _Zerinvary Lajos_, Jun 25 2008"
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Aug 31 2004",
			"references": 3,
			"revision": 42,
			"time": "2021-09-02T04:01:11-04:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}