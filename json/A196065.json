{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A196065",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 196065,
			"data": "0,1,4,4,16,16,9,9,64,64,64,36,36,36,256,16,36,144,16,144,144,256,144,64,1024,144,576,81,144,576,256,25,1024,144,576,256,64,64,576,256,144,324,81,576,2304,576,576,100,324,2304,576,324,25,1024,4096,144,256,576,144,1024,256,1024,1296,36,2304",
			"name": "The 1st multiplicative Zagreb index of the rooted tree with Matula-Goebel number n.",
			"comment": [
				"The 1st multiplicative Zagreb index of a connected graph is the product of the squared degrees of the vertices of the graph. Alternatively, it is the square of the Narumi-Katayama index.",
				"The Matula-Goebel number of a rooted tree can be defined in the following recursive manner: to the one-vertex tree there corresponds the number 1; to a tree T with root degree 1 there corresponds the t-th prime number, where t is the Matula-Goebel number of the tree obtained from T by deleting the edge emanating from the root; to a tree T with root degree m\u003e=2 there corresponds the product of the Matula-Goebel numbers of the m branches of T."
			],
			"link": [
				"E. Deutsch, \u003ca href=\"http://arxiv.org/abs/1111.4288\"\u003eTree statistics from Matula numbers\u003c/a\u003e, arXiv preprint arXiv:1111.4288 [math.CO], 2011.",
				"F. Goebel, \u003ca href=\"http://dx.doi.org/10.1016/0095-8956(80)90049-0\"\u003eOn a 1-1-correspondence between rooted trees and natural numbers\u003c/a\u003e, J. Combin. Theory, B 29 (1980), 141-143.",
				"I. Gutman, \u003ca href=\"http://www.imvibl.org/buletin/bulletin_1_13_19.pdf\"\u003eMultiplicative Zagreb indices of trees\u003c/a\u003e, Bulletin of International Mathematical Virtual Institut ISSN 1840-4367, Vol. 1, 2011, 13-19.",
				"I. Gutman and A. Ivic, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(95)00182-V\"\u003eOn Matula numbers\u003c/a\u003e, Discrete Math., 150, 1996, 131-142.",
				"I. Gutman and Yeong-Nan Yeh, \u003ca href=\"http://www.emis.de/journals/PIMB/067/3.html\"\u003eDeducing properties of trees from their Matula numbers\u003c/a\u003e, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"D. W. Matula, \u003ca href=\"http://www.jstor.org/stable/2027327\"\u003eA natural rooted tree enumeration by prime factorization\u003c/a\u003e, SIAM Rev. 10 (1968) 273.",
				"H. Narumi and M. Katayama, \u003ca href=\"http://hdl.handle.net/2115/38010\"\u003eSimple topological index. A newly devised index characterizing the topological nature of structural isomers of saturated hydrocarbons\u003c/a\u003e, Mem. Fac. Engin. Hokkaido Univ., 16, 1984, 209-214.",
				"Z. Tomovic and I. Gutman, \u003ca href=\"https://www.shd.org.rs/JSCS/Vol66/No4.htm#Narumi\"\u003eNarumi-Katayama index of phenylenes\u003c/a\u003e, J. Serb. Chem. Soc., 66(4), 2001, 243-247.",
				"\u003ca href=\"/index/Mat#matula\"\u003eIndex entries for sequences related to Matula-Goebel numbers\u003c/a\u003e"
			],
			"formula": [
				"a(1)=0; a(2)=1, if n = prime(t) (the t-th prime, t\u003e=2), then a(n)=a(t)*(1+G(t))^2/G(t)^2; if n=rs (r,s\u003e=2), then a(n)=a(r)*a(s)*G(n)^2/[G(r)*G(s)]^2; G(m) denotes the number of prime divisors of m counted with multiplicities. The Maple program is based on this recursive formula.",
				"a(n) = (A196063(n))^2."
			],
			"example": [
				"a(7)=9 because the rooted tree with Matula-Goebel number 7 is the rooted tree Y (1*9*1*1=9).",
				"a(2^m) = m^2 because the rooted tree with Matula-Goebel number 2^m is a star with m edges."
			],
			"maple": [
				"with(numtheory): a := proc (n) local r, s: r := proc (n) options operator, arrow: op(1, factorset(n)) end proc: s := proc (n) options operator, arrow: n/r(n) end proc: if n = 1 then 0 elif n = 2 then 1 elif bigomega(n) = 1 then a(pi(n))*(1+bigomega(pi(n)))^2/bigomega(pi(n))^2 else a(r(n))*a(s(n))*bigomega(n)^2/(bigomega(r(n))^2*bigomega(s(n))^2) end if end proc: seq(a(n), n = 1 .. 65);"
			],
			"xref": [
				"Cf. A196063, A196064."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Emeric Deutsch_, Oct 01 2011",
			"references": 2,
			"revision": 17,
			"time": "2019-04-16T02:50:39-04:00",
			"created": "2011-10-01T20:26:35-04:00"
		}
	]
}