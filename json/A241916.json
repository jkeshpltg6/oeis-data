{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A241916",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 241916,
			"data": "1,2,3,4,5,9,7,8,6,25,11,27,13,49,15,16,17,18,19,125,35,121,23,81,10,169,12,343,29,75,31,32,77,289,21,54,37,361,143,625,41,245,43,1331,45,529,47,243,14,50,221,2197,53,36,55,2401,323,841,59,375,61,961,175,64",
			"name": "a(2^k) = 2^k, and for other numbers, if n = 2^e1 * 3^e2 * 5^e3 * ... p_k^e_k, then a(n) = 2^(e_k - 1) * 3^(e_{k-1}) * ... * p_{k-1}^e2 * p_k^(e1+1). Here p_k is the greatest prime factor of n (A006530), and e_k is its exponent (A071178), and the exponents e1, ..., e_{k-1} \u003e= 0.",
			"comment": [
				"For other numbers than the powers of 2 (that are fixed), this permutation reverses the sequence of exponents in the prime factorization of n from the exponent of 2 to that of the largest prime factor, except that the exponents of 2 and the greatest prime factor present are adjusted by one. Note that some of the exponents might be zeros.",
				"Self-inverse permutation of natural numbers, composition of A122111 \u0026 A241909 in either order: a(n) = A122111(A241909(n)) = A241909(A122111(n)).",
				"This permutation preserves both bigomega and the (index of) largest prime factor: for all n it holds that A001222(a(n)) = A001222(n) and A006530(a(n)) = A006530(n) [equally: A061395(a(n)) = A061395(n)].",
				"From the above it follows, that this fixes both primes (A000040) and powers of two (A000079), among other numbers.",
				"Even positions from n=4 onward contain only terms of A070003, and the odd positions only the terms of A102750, apart from 1 which is at a(1), and 2 which is at a(2)."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A241916/b241916.txt\"\u003eTable of n, a(n) for n = 1..8192\u003c/a\u003e",
				"A. Karttunen, \u003ca href=\"/A122111/a122111.txt\"\u003eA few notes on A122111, A241909 \u0026 A241916.\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(1)=1, and for n\u003e1, a(n) = A006530(n) * A137502(n)/2.",
				"a(n) = A122111(A241909(n)) = A241909(A122111(n))."
			],
			"mathematica": [
				"nn = 65; f[n_] := If[n == 1, {0}, Function[f, ReplacePart[Table[0, {PrimePi[f[[-1, 1]]]}], #] \u0026@ Map[PrimePi@ First@ # -\u003e Last@ # \u0026, f]]@ FactorInteger@ n]; g[w_List] := Times @@ Flatten@ MapIndexed[Prime[#2]^#1 \u0026, w]; Table[If[IntegerQ@ #, n/4, g@ Reverse@(# - Join[{1}, ConstantArray[0, Length@ # - 2], {1}] \u0026@ f@ n)] \u0026@ Log2@ n, {n, 4, 4 nn, 4}] (* _Michael De Vlieger_, Aug 27 2016 *)"
			],
			"program": [
				"(PARI)",
				"A209229(n) = (n \u0026\u0026 !bitand(n,n-1));",
				"A241916(n) = if(1==A209229(n), n, my(f = factor(2*n), nbf = #f~, igp = primepi(f[nbf,1]), g = f); for(i=1,nbf,g[i,1] = prime(1+igp-primepi(f[i,1]))); factorback(g)/2); \\\\ _Antti Karttunen_, Jul 02 2018",
				"(Scheme) (define (A241916 n) (A122111 (A241909 n)))"
			],
			"xref": [
				"A241912 gives the fixed points; A241913 their complement.",
				"Cf. A006530, A137502, A070003, A102750, A278525.",
				"{A000027, A122111, A241909, A241916} form a 4-group."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Antti Karttunen_, May 03 2014",
			"ext": [
				"Description clarified by _Antti Karttunen_, Jul 02 2018"
			],
			"references": 17,
			"revision": 27,
			"time": "2018-07-02T07:25:02-04:00",
			"created": "2014-05-20T23:31:45-04:00"
		}
	]
}