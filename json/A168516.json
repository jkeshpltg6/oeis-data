{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A168516",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 168516,
			"data": "-1,1,-1,-1,2,-1,-1,-1,1,1,1,-1,-8,-1,1,1,1,4,-4,-1,-1,-1,-1,4,8,4,-1,-1,-1,-1,-8,-4,4,8,1,1,5,7,-4,-116,-32,-116,-4,7,5,5,5,32,28,16,-16,-28,-32,-5,-5,-691,-2663,-388,2524,5072,6112,5072,2524,-388,-2663,-691,-691,-691,-10264,-10652,-8128,-3056,3056,8128,10652,10264,691,691,7,1247,556,-4148,-2960,-22928",
			"name": "Table of the numerators of the fractions of Bernoulli twin numbers and their higher order differences, read by antidiagonals.",
			"comment": [
				"Consider the Bernoulli twin numbers C(n) = A051716(n)/A051717(n) in the top row and successive higher order differences in the other rows of an array T(0,k) = C(k), T(n,k) = T(n-1,k+1)-T(n-1,k):",
				"1, -1/2, -1/3, -1/6, -1/30, 1/30, 1/42, -1/42, -1/30, 1/30, 5/66, -5/66,...",
				"-3/2, 1/6, 1/6, 2/15, 1/15, -1/105, -1/21, -1/105, 1/15, 7/165, -5/33,...",
				"5/3, 0, -1/30, -1/15, -8/105, -4/105, 4/105, 8/105, -4/165, -32/165,...",
				"-5/3, -1/30, -1/30, -1/105, 4/105, 8/105, 4/105, -116/1155, -28/165,..",
				"49/30, 0, 1/42, 1/21, 4/105, -4/105, -32/231, -16/231, 5072/15015, 8128/15015,..",
				"-49/30, 1/42, 1/42, -1/105, -8/105, -116/1155, 16/231, 6112/15015,...",
				"Remove the two leftmost columns:",
				"-1/3,  -1/6,  -1/30,   1/30,       1/42, -1/42, -1/30, 1/30, 5/66, -5/66,-691/2730, 691/2730,...",
				"1/6,    2/15,  1/15,  -1/105,     -1/21, -1/105, 1/15, 7/165, -5/33, -2663/15015, 691/1365,..",
				"-1/30, -1/15,-8/105,  -4/105 ,  4/105, 8/105, -4/165, -32/165, -388/15015, 10264/15015,..",
				"-1/30,-1/105, 4/105,  8/105,    4/105, -116/1155, -28/165, 2524/15015,...",
				"1/42,   1/21,  4/105, -4/105,  -32/231, -16/231, 5072/15015, 8128/15015, -2960/3003,..",
				"1/42, -1/105, -8/105,-116/1155, 16/231, 6112/15015, 3056/15015, -22928/15015, -7184/3003",
				"-1/30, -1/15, -4/165,  28/165, 5072/15015, -3056/15015, -3712/2145,...",
				"-1/30, 7/165, 32/165,2524/15015,-8128/15015, -22928/15015,...",
				"and read the numerators upwards along antidiagonals to obtain the current sequence.",
				"The leftmost column (ie, the inverse binomial transform of the top row) in this chopped variant equals the top row up to a sign pattern (-1)^n.",
				"In that sense, the C(n) with n\u003e=2 are an eigensequence of the inverse binomial transform (i.e. an autosequence)."
			],
			"maple": [
				"C := proc(n) if n=0 then 1; elif n mod 2 = 0 then bernoulli(n)+bernoulli(n-1); else -bernoulli(n)-bernoulli(n-1); end if; end proc:",
				"A168516 := proc(n,k) L := [seq(C(i),i=0..n+k+3)] ; for c from 1 to n do L := DIFF(L) ; end do; numer(op(k+3,L)) ; end proc:",
				"for d from 0 to 15 do for k from 0 to d do printf(\"%a,\",A168516(d-k,k)) ; end do: end do: # _R. J. Mathar_, Jul 10 2011"
			],
			"mathematica": [
				"max = 13; c[0] = 1; c[n_?EvenQ] := BernoulliB[n] + BernoulliB[n-1]; c[n_?OddQ] := -BernoulliB[n] - BernoulliB[n-1]; cc = Table[c[n], {n, 0, max+1}]; diff = Drop[#, 2]\u0026 /@ Table[ Differences[cc, n], {n, 0, max-1}]; Flatten[ Table[ diff[[n-k+1, k]], {n, 1, max}, {k, 1, n}]] // Numerator (* _Jean-François Alcover_, Aug 09 2012 *)"
			],
			"xref": [
				"Cf. A168426 (denominators), A085737, A085738."
			],
			"keyword": "frac,tabl,sign",
			"offset": "0,5",
			"author": "_Paul Curtz_, Nov 28 2009",
			"ext": [
				"Edited and extended by _R. J. Mathar_, Jul 10 2011"
			],
			"references": 6,
			"revision": 11,
			"time": "2016-02-22T12:26:47-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}