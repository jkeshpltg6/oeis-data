{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A309797",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 309797,
			"data": "1,1,1,1,2,2,2,1,1,1,1,3,3,2,2,2,4,2,4,5,4,1,1,1,1,3,3,3,1,6,6,6,7,5,3,2,2,2,2,2,2,4,4,4,4,4,7,5,7,5,8,5,8,8,8,8,3,1,1,1,1,9,9,6,1,1,1,1,10,3,3,3,6,6,6,6,6,7,6,3,9,2,2,2,2,2,2",
			"name": "Lexicographically earliest sequence of positive integers such that for any n \u003e 0 there are no more than a(n) numbers k \u003e 0 such that a(n + k) = a(n + 2*k).",
			"comment": [
				"The sequence is well defined as we can always extend the sequence with a number that has not yet appeared.",
				"The number 1 appears infinitely many times in the sequence:",
				"- by contradiction: suppose that m is the index of the last occurrence of 1 in the sequence,",
				"- there is no n \u003e 0 such that n + k = m and n + 2*k = 2*m (with k \u003e 0),",
				"- so we can choose a(2*m) = 1, QED.",
				"This sequence has connections with A003602:",
				"- here we have up to a(n) numbers k such that a(n+k) = a(n+2*k), there we have no such numbers,",
				"- for any v \u003e= 0, let f_v be the lexicographically earliest sequence of positive integers such that there are no more than v numbers k such that f_v(n + k) = f_v(n + 2*k),",
				"- then f_v corresponds to A003602 where all but the first term have been repeated 2*v+1 times."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A309797/b309797.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A309797/a309797.gp.txt\"\u003ePARI program for A309797\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A309797/a309797.png\"\u003eScatterplot of the first 500000 terms\u003c/a\u003e"
			],
			"formula": [
				"a(n) \u003e= #{ k\u003e0 such that a(n+k) = a(n+2*k) }."
			],
			"example": [
				"The first terms, alongside the corresponding k's, are:",
				"  n   a(n)  k's",
				"  --  ----  ----------",
				"   1     1  {1}",
				"   2     1  {1}",
				"   3     1  {2}",
				"   4     1  {1}",
				"   5     2  {1, 3}",
				"   6     2  {2, 14}",
				"   7     2  {1, 2}",
				"   8     1  {1}",
				"   9     1  {1}",
				"  10     1  {4}",
				"  11     1  {1}",
				"  12     3  {2, 3, 68}",
				"  13     3  {1, 4, 22}",
				"  14     2  {1, 2}"
			],
			"program": [
				"(PARI) See Links section."
			],
			"xref": [
				"Cf. A003602, A329268 (positions of 1)."
			],
			"keyword": "nonn,look",
			"offset": "1,5",
			"author": "_Rémy Sigrist_, Nov 11 2019",
			"references": 4,
			"revision": 33,
			"time": "2019-11-14T17:20:08-05:00",
			"created": "2019-11-13T14:57:48-05:00"
		}
	]
}