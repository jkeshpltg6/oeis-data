{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A334232",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 334232,
			"data": "0,1,-1,4,2,-2,5,3,-5,-3,12,6,-6,-4,-12,13,11,7,-7,-11,-13,16,14,10,8,-8,-10,-14,17,15,23,9,-23,-9,-17,-15,48,18,22,24,-24,-22,-18,-16,-48,49,47,19,21,25,-25,-21,-19,-47,-49,52,50,46,20,28,26,-26,-20",
			"name": "T(n, k) is the number of steps from the point (0, 0) to the point (k, n) along the H-order curve; a negative value corresponds to moving backwards; square array T(n, k), n, k \u003e= 0 read by antidiagonals downwards.",
			"comment": [
				"The H-order curve is built as follows:",
				"- we start we a unit square H_0 oriented counterclockwise, the origin being at the left bottom corner:",
				"         +---\u003c---+",
				"         |       |",
				"         v       ^",
				"         |       |",
				"         O---\u003e---+",
				"- the configuration H_{k+1} is obtained by connecting four copies of the configuration H_k as follows:",
				"             |   |                               |   |",
				"         .   +   +   .                       .   +   +   .",
				"     H_k     ^   v     H_k                       ^   v",
				"         .   +   +   .                       .   +   +   .",
				"             |   |                               |   |",
				"    -+-\u003e-+---+   +---+-\u003e-+-             -+-\u003e-+   +-\u003c-+   +-\u003e-+-",
				"                                --\u003e          v           ^",
				"    -+-\u003c-+---+   +---+-\u003c-+-             -+-\u003c-+   +-\u003e-+   +-\u003c-+-",
				"             |   |                               |   |",
				"         .   +   +   .                       .   +   +   .",
				"     H_k     ^   v     H_k                       ^   v",
				"         .   +   +   .                       .   +   +   .",
				"             |   |                               |   |",
				"- the H-order curve corresponds to the limit of H_k as k tends to infinity,",
				"- the H-order curve visits once every lattice points with nonnegative coordinates and has a single connected component."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A334232/b334232.txt\"\u003eTable of n, a(n) for n = 0..5049\u003c/a\u003e",
				"GeoWave Developper Guide, \u003ca href=\"http://locationtech.github.io/geowave/devguide.html#spatial-index\"\u003eSpatial Index\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A334232/a334232.png\"\u003eRepresentation of H_k for k = 0..5\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A334232/a334232.gp.txt\"\u003ePARI program for A334232\u003c/a\u003e"
			],
			"example": [
				"Square array starts:",
				"  n\\k|    0    1    2    3    4    5    6    7",
				"  ---+----------------------------------------",
				"    0|    0....1    4....5   12...13   16...17",
				"     |    |    |    |    |    |    |    |    |",
				"    1|   -1    2....3    6   11   14...15   18",
				"     |    |              |    |              |",
				"    2|   -2   -5...-6    7   10   23...22   19",
				"     |    |    |    |    |    |    |    |    |",
				"    3|   -3...-4   -7    8....9   24   21...20",
				"     |              |              |",
				"    4|  -12..-11   -8  -23..-24   25   28...29",
				"     |    |    |    |    |    |    |    |    |",
				"    5|  -13  -10...-9  -22  -25   26...27   30",
				"     |    |              |    |              |",
				"    6|  -14  -17..-18  -21  -26  -29..-30   31",
				"     |    |    |    |    |    |    |    |    |",
				"    7|  -15..-16  -19..-20  -27..-28  -31   32"
			],
			"program": [
				"(PARI) See Links section."
			],
			"xref": [
				"See A334188 for a similar sequence.",
				"See A334233, A334234, A334235 and A334236 for the coordinates of the curve."
			],
			"keyword": "sign",
			"offset": "0,4",
			"author": "_Rémy Sigrist_, Apr 19 2020",
			"references": 6,
			"revision": 17,
			"time": "2021-02-25T21:24:45-05:00",
			"created": "2020-04-21T09:15:15-04:00"
		}
	]
}