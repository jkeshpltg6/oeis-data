{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A291218",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 291218,
			"data": "0,0,0,0,1,0,5,0,15,1,35,10,70,55,127,220,225,715,450,2003,1175,5025,3775,11650,12630,25850,40150,57475,118425,134883,325075,345090,840725,952195,2083888,2722455,5056055,7765010,12293890,21615771,30591685,58293475",
			"name": "p-INVERT of (0,1,0,1,0,1,...), where p(S) = 1 - S^5.",
			"comment": [
				"Suppose s = (c(0), c(1), c(2), ...) is a sequence and p(S) is a polynomial. Let S(x) = c(0)*x + c(1)*x^2 + c(2)*x^3 + ... and T(x) = (-p(0) + 1/p(S(x)))/x. The p-INVERT of s is the sequence t(s) of coefficients in the Maclaurin series for T(x). Taking p(S) = 1 - S gives the \"INVERT\" transform of s, so that p-INVERT is a generalization of the \"INVERT\" transform (e.g., A033453).",
				"See A291219 for a guide to related sequences."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A291218/b291218.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_10\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,5, 0,-10,1,10,0,-5,0,1)"
			],
			"formula": [
				"G.f.: -(x^4/((-1 + x + x^2) (1 + x - 3 x^2 - 2 x^3 + 5 x^4 + 2 x^5 - 3 x^6 - x^7 + x^8))).",
				"a(n) = 5*a(n-2) - 10*a(n-4) + a(n-5) + 10* a(n-6) - 5*a(n-8) + a(n-10) for n \u003e= 11."
			],
			"mathematica": [
				"z = 60; s = x/(1 - x^2); p = 1 - s^3;",
				"Drop[CoefficientList[Series[s, {x, 0, z}], x], 1]  (* A000035 *)",
				"Drop[CoefficientList[Series[1/p, {x, 0, z}], x], 1]  (* A291218 *)",
				"LinearRecurrence[{0, 5, 0, -10, 1, 10, 0, -5, 0, 1}, {0, 0, 0, 0, 1, 0, 5, 0, 15, 1}, 50] (* _Vincenzo Librandi_, Aug 25 2017 *)"
			],
			"program": [
				"(MAGMA) I:=[0,0,0,0,1,0,5,0,15,1]; [n le 10 select I[n] else 5*Self(n-2)-10*Self(n-4)+Self(n-5)+10*Self(n-6)-5*Self(n-8)+Self(n-10): n in [1..45]]; // _Vincenzo Librandi_, Aug 25 2017"
			],
			"xref": [
				"Cf. A000035, A291000, A291219."
			],
			"keyword": "nonn,easy",
			"offset": "0,7",
			"author": "_Clark Kimberling_, Aug 24 2017",
			"references": 2,
			"revision": 14,
			"time": "2018-02-27T16:02:26-05:00",
			"created": "2017-09-01T23:54:25-04:00"
		}
	]
}