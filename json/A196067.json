{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A196067",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 196067,
			"data": "0,2,2,2,2,2,3,3,2,2,2,3,3,3,2,4,3,3,4,3,3,2,3,4,2,3,3,4,3,3,2,5,2,3,3,4,4,4,3,4,3,4,4,3,3,3,3,5,4,3,3,4,5,4,2,5,4,3,3,4,4,2,4,6,3,3,4,4,3,4,4,5,4,4,3,5,3,4,3,5,4,3,3,5,3,4,3,4,5,4,4,4,2,3,4,6,3,5,3,4,4,4,4,5,4,5,5,5,3,3",
			"name": "Number of pendant vertices in the rooted tree with Matula-Goebel number n.",
			"comment": [
				"A pendant vertex in a tree is a vertex having degree 1.",
				"The Matula-Goebel number of a rooted tree can be defined in the following recursive manner: to the one-vertex tree there corresponds the number 1; to a tree T with root degree 1 there corresponds the t-th prime number, where t is the Matula-Goebel number of the tree obtained from T by deleting the edge emanating from the root; to a tree T with root degree m\u003e=2 there corresponds the product of the Matula-Goebel numbers of the m branches of T."
			],
			"reference": [
				"F. Goebel, On a 1-1-correspondence between rooted trees and natural numbers, J. Combin. Theory, B 29 (1980), 141-143.",
				"I. Gutman and A. Ivic, On Matula numbers, Discrete Math., 150, 1996, 131-142.",
				"I. Gutman and Yeong-Nan Yeh, Deducing properties of trees from their Matula numbers, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"D. W. Matula, A natural rooted tree enumeration by prime factorization, SIAM Review, 10, 1968, 273."
			],
			"link": [
				"E. Deutsch, \u003ca href=\"http://arxiv.org/abs/1111.4288\"\u003eTree statistics from Matula numbers\u003c/a\u003e, arXiv preprint arXiv:1111.4288, 2011",
				"\u003ca href=\"/index/Mat#matula\"\u003eIndex entries for sequences related to Matula-Goebel numbers\u003c/a\u003e"
			],
			"formula": [
				"a(1)=0; a(2)=2; if n=p(t) (=the t-th prime) and t is prime, then a(n)=a(t); if n=p(t) (the t-th prime) and t is not prime, then a(n)=1+a(t); if n=rs (r,s,\u003e=2), then a(n)=a(r)+a(s)-the number of primes in {r,s}. The Maple program is based on this recursive formula."
			],
			"example": [
				"a(7)=3 because the rooted tree with Matula-Goebel number 7 is the rooted tree Y.",
				"a(2^m) = m because the rooted tree with Matula-Goebel number 2^m is a star with m edges."
			],
			"maple": [
				"with(numtheory): a := proc (n) local r, s: r := proc (n) options operator, arrow: op(1, factorset(n)) end proc: s := proc (n) options operator, arrow: n/r(n) end proc: if n = 1 then 0 elif n = 2 then 2 elif bigomega(n) = 1 and bigomega(pi(n)) = 1 then a(pi(n)) elif bigomega(n) = 1 then a(pi(n))+1 elif bigomega(r(n)) = 1 and bigomega(s(n)) = 1 then a(r(n))+a(s(n))-2 elif min(bigomega(r(n)), bigomega(s(n))) = 1 then a(r(n))+a(s(n))-1 else a(r(n))+a(s(n)) end if end proc: seq(a(n), n = 1 .. 110);"
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Emeric Deutsch_, Oct 03 2011",
			"references": 0,
			"revision": 13,
			"time": "2017-03-07T06:16:17-05:00",
			"created": "2011-10-03T18:15:40-04:00"
		}
	]
}