{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A304081",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 304081,
			"data": "0,0,0,0,0,1,0,2,1,3,1,2,2,2,1,3,3,3,2,4,2,3,2,5,2,4,2,3,3,3,2,4,3,5,1,7,4,4,3,7,2,4,3,8,4,7,4,6,3,7,3,6,4,5,3,5,4,5,2,7,3,5,4,8,4,5,3,5,5,8,6,6,6,9,3,9,7,6,6,8,5,6,4,6,8,7,6,8,7,4",
			"name": "Number of ways to write n as p + 2^k + (1+(n mod 2))*5^m, where p is an odd prime, and k and m are nonnegative integers with 2^k + (1+(n mod 2))*5^m squarefree.",
			"comment": [
				"Conjecture: a(n) \u003e 0 for all n \u003e 7.",
				"This has been verified for n up to 2*10^10.",
				"See also A303821, A303934, A303949, A304031 and A304122 for related information, and A304034 for a similar conjecture.",
				"The author would like to offer 2500 US dollars as the prize to the first proof of the conjecture, and 250 US dollars as the prize to the first explicit counterexample. - _Zhi-Wei Sun_, May 08 2018"
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A304081/b304081.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://maths.nju.edu.cn/~zwsun/116f.pdf\"\u003eMixed sums of primes and other terms\u003c/a\u003e, in: Additive Number Theory (edited by D. Chudnovsky and G. Chudnovsky), pp. 341-353, Springer, New York, 2010.",
				"Zhi-Wei Sun, \u003ca href=\"https://doi.org/10.1007/978-3-319-68032-3_20\"\u003eConjectures on representations involving primes\u003c/a\u003e, in: M. Nathanson (ed.), Combinatorial and Additive Number Theory II, Springer Proc. in Math. \u0026 Stat., Vol. 220, Springer, Cham, 2017, pp. 279-310. (See also \u003ca href=\"http://arxiv.org/abs/1211.1588\"\u003earXiv\u003c/a\u003e, arXiv:1211.1588 [math.NT], 2012-2017.)"
			],
			"example": [
				"a(6) = 1 since 6 = 3 + 2^1 + 5^0 with 3 an odd prime and 2^1 + 5^0 = 3 squarefree.",
				"a(15) = 1 since 15 = 5 + 2^3 + 2*5^0 with 5 an odd prime and 2^3 + 2*5^0 = 2*5 squarefree.",
				"a(35) = 1 since 35 = 29 + 2^2 + 2*5^0 with 29 an odd prime and 2^2 + 2*5^0 = 2*3 squarefree.",
				"a(91) = 1 since 91 = 17 + 2^6 + 2*5^1 with 17 an odd prime and 2^6 + 2*5^1 = 2*37 squarefree.",
				"a(9574899) = 1 since 9574899 = 9050609 + 2^19 + 2*5^0 with 9050609 an odd prime and 2^19 + 2*5^0 = 2*5*13*37*109 squarefree.",
				"a(6447154629) = 2 since 6447154629 = 6447121859 + 2^15 + 2*5^0 with 6447121859 prime and 2^15 + 2*5^0 = 2*5*29*113 squarefree, and 6447154629 = 5958840611 + 2^15 + 2*5^12 with 5958840611 prime and 2^15 + 2*5^12 = 2*17*41*433*809 squarefree."
			],
			"mathematica": [
				"PQ[n_]:=n\u003e2\u0026\u0026PrimeQ[n];",
				"tab={};Do[r=0;Do[If[SquareFreeQ[2^k+(1+Mod[n,2])*5^m]\u0026\u0026PQ[n-2^k-(1+Mod[n,2])*5^m],r=r+1],{k,0,Log[2,n]},{m,0,If[2^k==n,-1,Log[5,(n-2^k)/(1+Mod[n,2])]]}];tab=Append[tab,r],{n,1,90}];Print[tab]"
			],
			"xref": [
				"Cf. A000040, A000079, A000351, A005117, A098983, A118955, A156695, A273812, A302982, A302984, A303233, A303234, A303338, A303363, A303389, A303393, A303399, A303428, A303401, A303432, A303434, A303539, A303540, A303541, A303543, A303601, A303637, A303639, A303656, A303660, A303702, A303821, A303932, A303934, A303949, A304031, A304032, A304034, A304122."
			],
			"keyword": "nonn",
			"offset": "1,8",
			"author": "_Zhi-Wei Sun_, May 06 2018",
			"references": 18,
			"revision": 31,
			"time": "2020-01-18T20:12:57-05:00",
			"created": "2018-05-07T03:44:20-04:00"
		}
	]
}