{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A203776",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 203776,
			"data": "1,1,0,0,1,1,1,1,0,1,2,2,1,1,2,3,3,2,2,3,5,5,3,3,5,7,7,6,5,7,11,11,8,8,12,15,15,13,12,16,22,22,18,18,24,30,31,27,26,33,42,43,37,37,47,57,58,53,52,63,78,80,71,72,88,103,106,99,98,116,139,142",
			"name": "Number of partitions of n into distinct parts 5k+1 or 5k+4.",
			"comment": [
				"Convolution of A281243 and A280454. - _Vaclav Kotesovec_, Jan 18 2017"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A203776/b203776.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e (first 251 terms from Reinhard Zumkeller)",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of f( x, x^4) / f(-x^5, -x^10) in powers of x where f() is the Ramanujan two-variable theta function. - _Michael Somos_, Mar 23 2013",
				"Expansion of (-x; x^5)_oo (-x^4; x^5)_oo in powers of x where (x; q)_oo is the q-Pochhammer symbol. - _Michael Somos_, Mar 23 2013",
				"Euler transform of period 10 sequence [ 1, -1, 0, 1, 0, 1, 0, -1, 1, 0, ...]. - _Michael Somos_, Mar 23 2013",
				"G.f.: Product_{k\u003e0} (1 + x^(5*k - 1)) * (1 + x^(5*k - 4)). - _Michael Somos_, Mar 23 2013",
				"a(n) ~ exp(sqrt(2*n/15)*Pi) / (2*30^(1/4)*n^(3/4)) * (1 + (Pi/(60*sqrt(30)) - 3*sqrt(15/2)/(8*Pi)) / sqrt(n)). - _Vaclav Kotesovec_, Jan 18 2017, extended Jan 24 2017"
			],
			"example": [
				"a(10) = #{9+1, 6+4} = 2;",
				"a(20) = #{19+1, 16+4, 14+6, 11+9, 9+6+4+1} = 5.",
				"1 + x + x^4 + x^5 + x^6 + x^7 + x^9 + 2*x^10 + 2*x^11 + x^12 + x^13 + 2*x^14 + ...",
				"q + q^61 + q^241 + q^301 + q^361 + q^421 + q^541 + 2*q^601 + 2*q^661 + q^721 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ Product[ (1 + x^(5 k - 1)) (1 + x^(5 k - 4)), {k, Ceiling[ n / 5]}], {x, 0, n}] (* _Michael Somos_, Mar 23 2013 *)",
				"a[ n_] := SeriesCoefficient[ QPochhammer[ -x, x^5] QPochhammer[ -x^4, x^5], {x, 0, n}] (* _Michael Somos_, Mar 23 2013 *)"
			],
			"program": [
				"(Haskell)",
				"a203776 = p a047209_list where",
				"   p _      0 = 1",
				"   p (k:ks) m = if m \u003c k then 0 else p ks (m - k) + p ks m",
				"(PARI) {a(n) = polcoeff( prod( k=1, ceil(n / 5), (1 + x^(5*k - 1)) * (1 + x^(5*k - 4)), 1 + x * O(x^n)), n)} /* _Michael Somos_, Mar 23 2013 */"
			],
			"xref": [
				"Cf. A047209, A003114."
			],
			"keyword": "nonn",
			"offset": "0,11",
			"author": "_Reinhard Zumkeller_, Jan 05 2012",
			"references": 15,
			"revision": 29,
			"time": "2021-03-12T22:24:46-05:00",
			"created": "2012-01-05T17:27:31-05:00"
		}
	]
}