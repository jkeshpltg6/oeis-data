{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A285872",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 285872,
			"data": "0,1,2,3,4,3,4,5,6,7,8,7,8,9,10,11,12,11,12,13,14,15,16,15,16,17,18,19,20,19,20,21,22,23,24,23,24,25,26,27,28,27,28,29,30,31,32,31,32,33,34,35,36,35,36,37,38,39,40,39,40,41,42,43,44,43,44,45",
			"name": "a(n) is the number of zeros of the Chebyshev S(n, x) polynomial (A049310) in the open interval (-sqrt(3), +sqrt(3)).",
			"comment": [
				"See a May 06 2017 comment on A049310 where these problems are considered which originated in a conjecture by _Michel Lagneau_ (see A008611) on Fibonacci polynomials."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A285872/b285872.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_07\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,0,0,0,0,1,-1)."
			],
			"formula": [
				"a(n) = 2*b(n) if n is even and 1 + 2*b(n) if n is odd with b(n) = floor(n/2) - floor((n+1)/6) = A285870(n). See the g.f. for {b(n)}_{n\u003e=0} there.",
				"From _Colin Barker_, May 18 2017: (Start)",
				"G.f.: x*(1 + x + x^2 + x^3 - x^4 + x^5) / ((1 - x)^2*(1 + x)*(1 - x + x^2)*(1 + x + x^2)).",
				"a(n) = a(n-1) + a(n-6) - a(n-7) for n\u003e6.",
				"(End)"
			],
			"example": [
				"n = 3: S(3, x) = x*(-2 + x^2), with all three zeros (-sqrt(2), 0, +sqrt(2)) in the interval (-sqrt(3), +sqrt(3)).",
				"n = 4: S(4, x) = 1 - 3*x^2 + x^4, all four zeros  (-phi, -1/phi, 1/phi, phi) with phi = (1 + sqrt(5))/2, approximately 1.618, lie in the interval.",
				"n = 6, two zeros of  S(6, x) = -1 + 6*x^2 - 5*x^4 + x^6 are out of the interval (-sqrt(3), +sqrt(3)), namely - 1.8019... and +1.8019... ."
			],
			"mathematica": [
				"CoefficientList[Series[x*(1+x+x^2+x^3-x^4+x^5)/((1-x)^2*(1+x)*(1-x+x^2)*(1+x+x^2)), {x, 0, 50}], x] (* _G. C. Greubel_, Mar 08 2018 *)"
			],
			"program": [
				"(PARI) concat(0, Vec(x*(1 + x + x^2 + x^3 - x^4 + x^5) / ((1 - x)^2*(1 + x)*(1 - x + x^2)*(1 + x + x^2)) + O(x^100))) \\\\ _Colin Barker_, May 18 2017",
				"(MAGMA) m:=80; R\u003cx\u003e:=PowerSeriesRing(Integers(), m); Coefficients(R!(x*(1+x+x^2+x^3-x^4+x^5)/((1-x)^2*(1+x)*(1-x+x^2)*(1+x+x^2)))); // _G. C. Greubel_, Mar 08 2018"
			],
			"xref": [
				"Cf. A008611(n-1) (1), A049310, A285869 (sqrt(2)), A285870."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Wolfdieter Lang_, May 12 2017",
			"references": 4,
			"revision": 23,
			"time": "2018-03-09T17:53:59-05:00",
			"created": "2017-05-15T03:09:30-04:00"
		}
	]
}