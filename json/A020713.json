{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A020713",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 20713,
			"data": "5,9,16,28,49,86,151,265,465,816,1432,2513,4410,7739,13581,23833,41824,73396,128801,226030,396655,696081,1221537,2143648,3761840,6601569,11584946,20330163,35676949,62608681,109870576,192809420,338356945,593775046,1042002567",
			"name": "Pisot sequences E(5,9), P(5,9).",
			"link": [
				"Colin Barker, \u003ca href=\"/A020713/b020713.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Shalosh B. Ekhad, N. J. A. Sloane and Doron Zeilberger, \u003ca href=\"https://arxiv.org/abs/1609.05570\"\u003eAutomated Proof (or Disproof) of Linear Recurrences Satisfied by Pisot Sequences\u003c/a\u003e, arXiv:1609.05570 [math.NT], 2016.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,-1,1)."
			],
			"formula": [
				"a(n) = 2*a(n-1) - a(n-2) + a(n-3) (holds at least up to n = 1000 but is not known to hold in general).",
				"Empirical g.f.: (5-x+3*x^2) / (1-2*x+x^2-x^3). - _Colin Barker_, Jun 05 2016",
				"Theorem: E(5,9) satisfies a(n) = 2 a(n - 1) -  a(n - 2) + a(n - 3) for n\u003e=3. Proved using the PtoRv program of Ekhad-Sloane-Zeilberger, and implies the above conjectures. - _N. J. A. Sloane_, Sep 09 2016",
				"a(n) = (-1)^n * A099529(n+6). - _Jinyuan Wang_, Mar 10 2020"
			],
			"mathematica": [
				"RecurrenceTable[{a[0] == 5, a[1] == 9, a[n] == Ceiling[a[n - 1]^2/a[n - 2]-1/2]}, a, {n, 0, 40}] (* _Bruno Berselli_, Feb 04 2016 *)",
				"LinearRecurrence[{2,-1,1},{5,9,16},40] (* _Harvey P. Dale_, Aug 03 2021 *)"
			],
			"program": [
				"(MAGMA) Iv:=[5, 9]; [n le 2 select Iv[n] else Ceiling(Self(n-1)^2/Self(n-2)-1/2): n in [1..40]]; // _Bruno Berselli_, Feb 04 2016",
				"(PARI) lista(nn) = {print1(x = 5, \", \", y = 9, \", \"); for (n=1, nn, z = ceil(y^2/x -1/2); print1(z, \", \"); x = y; y = z;);} \\\\ _Michel Marcus_, Feb 04 2016"
			],
			"xref": [
				"This is a subsequence of A005314.",
				"See A008776 for definitions of Pisot sequences.",
				"Cf. A099529."
			],
			"keyword": "nonn",
			"offset": "0,1",
			"author": "_David W. Wilson_",
			"references": 1,
			"revision": 34,
			"time": "2021-08-03T12:43:21-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}