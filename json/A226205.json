{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A226205",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 226205,
			"data": "1,0,3,5,16,39,105,272,715,1869,4896,12815,33553,87840,229971,602069,1576240,4126647,10803705,28284464,74049691,193864605,507544128,1328767775,3478759201,9107509824,23843770275,62423800997,163427632720,427859097159,1120149658761",
			"name": "a(n) = F(n)^2 - F(n-1)^2 or F(n+1) * F(n-2) where F(n) = A000045(n), the Fibonacci numbers.",
			"comment": [
				"A001519(n)^2 = A079472(n)^2 + a(n)^2 and (A001519(n), A079472(n), a(n)) is a Pythagorean triple.",
				"INVERT transform is A052156. PSUM transform is A007598. SUMADJ transform is A088305. BINOMIAL transform is A039717. BINOMIAL transform with 0 prepended is A112091 with 0 prepended. BINOMIAL transform inverse is A084179(n+1)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A226205/b226205.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Nurettin Irmak, \u003ca href=\"http://journals.tubitak.gov.tr/math/abstract.htm?id=20961\"\u003eProduct of arbitrary Fibonacci numbers with distance 1 to Fibonomial coefficient\u003c/a\u003e, Turk J Math, (2017) 41: 825-828. See p. 828.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,2,-1)."
			],
			"formula": [
				"G.f.: x * (1 - x)^2 / ((1 + x) * (1 -3*x + x^2)).",
				"a(n) = 2*a(n-1) + 2*a(n-2) - a(n-3).",
				"a(n) = - A121646(n).",
				"a(n) = -a(1-n) for all n in Z.",
				"a(n) = A121801(n+1) / 2. - _Michael Somos_, Jun 17 2014",
				"a(n) = a(n-1) + A000045(n-1)^2 - 2*(-1)^n, for n\u003e1. - _Alexander Samokrutov_, Sep 07 2015",
				"a(n) = F(n-1)*F(n) - (-1)^n. - _Bruno Berselli_, Oct 30 2015",
				"a(n) = 2^(-1-n)*(-(-1)^n*2^(3+n)-(3-sqrt(5))^n*(1+sqrt(5))+(-1+sqrt(5))*(3+sqrt(5))^n)/5. - _Colin Barker_, Sep 28 2016",
				"From _Amiram Eldar_, Oct 06 2020: (Start)",
				"Sum_{n\u003e=3} 1/a(n) = (1/2) * A290565 - 1/4.",
				"Sum_{n\u003e=3} (-1)^(n+1)/a(n) = (3/2) * (1/phi - 1/2), where phi is the golden ratio (A001622). (End)"
			],
			"example": [
				"G.f. = x + 3*x^3 + 5*x^4 + 16*x^5 + 39*x^6 + 105*x^7 + 272*x^8 + 715*x^9 + ..."
			],
			"maple": [
				"a:= n-\u003e (\u003c\u003c0|1|0\u003e, \u003c0|0|1\u003e, \u003c-1|2|2\u003e\u003e^n. \u003c\u003c1,0,3\u003e\u003e)[1, 1]:",
				"seq(a(n), n=0..30);  # _Alois P. Heinz_, Sep 28 2016"
			],
			"mathematica": [
				"a[ n_] := Fibonacci[n + 1] Fibonacci[n - 2]; (* _Michael Somos_, Jun 17 2014 *)",
				"CoefficientList[Series[(1 - x)^2/((1 + x) (1 - 3 x + x^2)), {x, 0, 30}], x] (* _Vincenzo Librandi_, Jun 17 2014 *)"
			],
			"program": [
				"(MAGMA) [Fibonacci(n)^2-Fibonacci(n-1)^2: n in [1..40]]; // _Vincenzo Librandi_, Jun 18 2014",
				"(PARI) {a(n) = fibonacci( n + 1) * fibonacci( n - 2)};",
				"(PARI) a(n) = round(2^(-1-n)*(-(-1)^n*2^(3+n)-(3-sqrt(5))^n*(1+sqrt(5))+(-1+sqrt(5))*(3+sqrt(5))^n)/5) \\\\ _Colin Barker_, Sep 28 2016",
				"(PARI) lista(nn) = {my(p = (3*x-1)/(x^3-2*x^2-2*x+1)); for (n=1, nn, p = deriv(p, x); print1(subst(p, x, 0)/n!, \", \"); ); } \\\\ _Michel Marcus_, May 22 2018"
			],
			"xref": [
				"Cf. A001519, A001622, A001654, A007598, A033999, A039717, A052156, A079472, A084179, A088305, A112091, A121646, A290565.",
				"Cf. similar sequences of the type k*F(n)*F(n+1)+(-1)^n listed in A264080.",
				"Cf. A260259: numbers of the form F(n)*F(n+1)-(-1)^n. - _Bruno Berselli_, Nov 02 2015"
			],
			"keyword": "nonn,easy",
			"offset": "1,3",
			"author": "_Michael Somos_, Jun 06 2013",
			"references": 10,
			"revision": 107,
			"time": "2020-10-06T04:01:29-04:00",
			"created": "2013-06-07T11:08:09-04:00"
		}
	]
}