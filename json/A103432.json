{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A103432",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 103432,
			"data": "1,2,1,0,3,2,4,1,5,2,6,1,5,4,0,7,2,6,5,8,3,8,5,9,4,10,1,10,3,8,7,0,11,4,10,7,11,6,13,2,10,9,12,7,14,1,15,2,13,8,15,4,16,1,13,10,14,9,16,5,17,2,13,12,14,11,16,9,18,5,17,8,0,18,7,17,10,19,6,20,1,20,3,15,14,17",
			"name": "Subsequence of the Gaussian primes, where only Gaussian primes a+bi with a\u003e0, b\u003e=0 are listed. Ordered by the norm N(a+bi)=a^2+b^2 and the size of the real part when the norms are equal. The sequence gives the imaginary parts. See A103431 for the real parts.",
			"comment": [
				"Detailed description in A103431."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A103432/b103432.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Sven Simon, \u003ca href=\"/A103431/a103431_1.txt\"\u003eList with Gaussian primes of A103431/A103432\u003c/a\u003e"
			],
			"maple": [
				"N:= 100: # to get all terms with norm \u003c= N",
				"p1:= select(isprime,[seq(i,i=3..N,4)]):",
				"p2:= select(isprime,[seq(i,i=1..N^2,4)]):",
				"p2:= map(t -\u003e GaussInt:-GIfactors(t)[2][1][1],p2):",
				"p3:= sort( [1+I, op(p1),op(p2)],(a,b) -\u003e Re(a)^2 + Im(a)^2  \u003c Re(b)^2 + Im(b)^2):",
				"h:= proc(z)",
				"    local a,b;",
				"    a:= Re(z); b:= Im(z);",
				"    if b = 0 then 0",
				"    else",
				"      a:= abs(a);",
				"      b:= abs(b);",
				"      if a = b then a",
				"      elif a \u003c b then b,a",
				"      else a,b",
				"      fi",
				"    fi",
				"end proc:",
				"map(h,p3); # _Robert Israel_, Feb 23 2016"
			],
			"mathematica": [
				"maxNorm = 500;",
				"norm[z_] := Re[z]^2 + Im[z]^2;",
				"m = Sqrt[maxNorm] // Ceiling;",
				"gp = Select[Table[a + b I, {a, 1, m}, {b, 0, m}] // Flatten, norm[#] \u003c= maxNorm \u0026\u0026 PrimeQ[#, GaussianIntegers -\u003e True]\u0026];",
				"SortBy[gp, norm[#] maxNorm + Abs[Re[#]]\u0026] // Im (* _Jean-François Alcover_, Feb 26 2019 *)"
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Sven Simon_, Feb 05 2005; corrected Feb 20 2005 and again on Aug 06 2006",
			"ext": [
				"Definition of norm corrected by _Franklin T. Adams-Watters_, Mar 04 2011",
				"a(48) corrected by _Robert Israel_, Feb 23 2016"
			],
			"references": 11,
			"revision": 20,
			"time": "2019-02-26T05:06:12-05:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}