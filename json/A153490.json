{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A153490",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 153490,
			"data": "1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,0,1,1,0,1,1,1,1,0,1,1,1,1,1,1,0,0,1,1,1,1,0,1,0,0,0,1,0,1,1,1,1,1,0,0,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,0,1,1,0,1,1,0,1,1,0,1",
			"name": "Sierpinski carpet, read by antidiagonals.",
			"comment": [
				"The Sierpinski carpet is the fractal obtained by starting with a unit square and at subsequent iterations, subdividing each square into 3 X 3 smaller squares and removing (from nonempty squares) the middle square. After the n-th iteration, the upper-left 3^n X 3^n squares will always remain the same. Therefore this sequence, which reads these by antidiagonals, is well-defined.",
				"Row sums are {1, 2, 2, 4, 5, 4, 6, 6, 4, 8, 10, 8, ...}."
			],
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/SierpinskiCarpet.html\"\u003eSierpinski Carpet\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Sierpinski_carpet\"\u003eSierpinski carpet\u003c/a\u003e."
			],
			"example": [
				"The Sierpinski carpet matrix reads",
				"   1 1 1 1 1 1 1 1 1 ...",
				"   1 0 1 1 0 1 1 0 1 ...",
				"   1 1 1 1 1 1 1 1 1 ...",
				"   1 1 1 0 0 0 1 1 1 ...",
				"   1 0 1 0 0 0 1 0 1 ...",
				"   1 1 1 0 0 0 1 1 1 ...",
				"   1 1 1 1 1 1 1 1 1 ...",
				"   1 0 1 1 0 1 1 0 1 ...",
				"   1 1 1 1 1 1 1 1 1 ...",
				"   (...)",
				"so the antidiagonals are",
				"  {1},",
				"  {1, 1},",
				"  {1, 0, 1},",
				"  {1, 1, 1, 1},",
				"  {1, 1, 1, 1, 1},",
				"  {1, 0, 1, 1, 0, 1},",
				"  {1, 1, 1, 0, 1, 1, 1},",
				"  {1, 1, 1, 0, 0, 1, 1, 1},",
				"  {1, 0, 1, 0, 0, 0, 1, 0, 1},",
				"  {1, 1, 1, 1, 0, 0, 1, 1, 1, 1},",
				"  {1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1},",
				"  {1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1},",
				"  ..."
			],
			"mathematica": [
				"\u003c\u003c MathWorld`Fractal`; fractal = SierpinskiCarpet;",
				"a = fractal[4]; Table[Table[a[[m]][[n - m + 1]], {m, 1, n}], {n, 1, 12}];",
				"Flatten[%]"
			],
			"program": [
				"(PARI) A153490_row(n,A=Mat(1))={while(#A\u003cn,A=matrix(3*#A,3*#A,i,j,if(A[(i+2)\\3,(j+2)\\3],i%3!=2||j%3!=2)));vector(n,k,A[k,n-k+1])} \\\\ _M. F. Hasler_, Oct 23 2017"
			],
			"xref": [
				"Cf. A292688 (n-th antidiagonal concatenated as binary number), A292689 (decimal representation of these binary numbers).",
				"Cf. A293143 (number of vertex points in a Sierpinski Carpet)."
			],
			"keyword": "nonn,tabl",
			"offset": "1,1",
			"author": "_Roger L. Bagula_, Dec 27 2008",
			"ext": [
				"Edited by _M. F. Hasler_, Oct 20 2017"
			],
			"references": 8,
			"revision": 18,
			"time": "2018-04-29T02:12:11-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}