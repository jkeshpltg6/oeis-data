{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A343860",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 343860,
			"data": "8,9,10,18,15,24,45,35,90,90,210,264,117,90,585,136,435,522,1305,1935,306,235,3978,3608,4690,2415,1416,801,615,792,27234,1610,6090,50184,44290,3042,44109,8730,22698,41615,2097,1610,107535,186633,46104,40410,19485",
			"name": "For the numbers k that can be expressed as k = w+x = y*z with w*x = (y+z)^2 where w, x, y, and z are all positive integers, this sequence gives the corresponding values of y+z.",
			"comment": [
				"A057369 lists numbers m such that two quadratic equations of the form t^2-k*t+m = 0 and t^2-m*t+k^2 = 0 have positive integer roots, where k is the coefficient of t and m is the constant in first equation, which has roots p and q (i.e., k, m, p, q are all positive integer, k=p+q and m=p*q). Also m is the coefficient of t and k^2 is the constant in second equation, which has roots u and v (i.e., k, m, u, v are all positive integer, m=u+v and k^2=u*v). Sequence [a(n)] represents corresponding values of k=p+q for A057369(m)."
			],
			"link": [
				"Soumyadeep Dhar, \u003ca href=\"/A343860/b343860.txt\"\u003eTable of n, a(n) for n = 1..112\u003c/a\u003e"
			],
			"example": [
				"t^2 - (3+15)*t + 3*15 = 0 has roots p=3 and q=15, and",
				"t^2 - (9+36)*t + 9*36 = 0 has roots u=9 and v=36, and",
				"3*15 = 9+36 and (3+15)^2 = 9*36, so k = 3+15 = 18 is a term of this sequence.",
				"--",
				"The first 10 values of k listed in A057369 and their corresponding values of w, x, y, z, and y+z are as follows:",
				".",
				"   n    k   w    x  y   z  y+z = a(n)",
				"  --  ---  --  --- --  --  ----------",
				"   1   16   8    8  4   4        8",
				"   2   18   9    9  3   6        9",
				"   3   25   5   20  5   5       10",
				"   4   45   9   36  3  15       18",
				"   5   50   5   45  5  10       15",
				"   6   80   8   72  4  20       24",
				"   7  234   9  225  6  39       45",
				"   8  250   5  245 10  25       35",
				"   9  261  36  225  3  87       90",
				"  10  425  20  405  5  85       90"
			],
			"program": [
				"(PARI) forstep(k=1, 1000, 1, fordiv(k, y, if(issquare(k^2 - 4*(y+k/y)^2), print1(y+k/y, \", \"); break)));"
			],
			"xref": [
				"Cf. A057369, A057442."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Soumyadeep Dhar_, May 01 2021",
			"references": 1,
			"revision": 63,
			"time": "2021-06-19T20:23:49-04:00",
			"created": "2021-06-19T20:23:49-04:00"
		}
	]
}