{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A030199",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 30199,
			"data": "1,-1,-1,0,0,1,0,1,0,0,0,0,-1,0,0,-1,0,0,0,0,0,0,1,-1,1,1,1,0,-1,0,-1,0,0,0,0,0,0,0,1,0,-1,0,0,0,0,-1,-1,1,1,-1,0,0,0,-1,0,0,0,1,2,0,0,1,0,1,0,0,0,0,-1,0,-1,0,-1,0,-1,0,0,-1,0,0,-1",
			"name": "Expansion of x * Product_{k\u003e=1} (1 - x^k) * (1 - x^(23*k)).",
			"comment": [
				"Number 40 of the 74 eta-quotients listed in Table I of Martin (1996)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A030199/b030199.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"H. Darmon, \u003ca href=\"http://www.ams.org/notices/201703/moti-p197.pdf\"\u003eAndrew Wiles's Marvelous Proof\u003c/a\u003e, Notices Amer. Math. Soc., 64 (No. 3, March 2017), 209-216. See p. 211 equ. 23",
				"A. Granville and G. Martin, \u003ca href=\"http://www.arXiv.org/abs/math.NT/0408319\"\u003ePrime number races\u003c/a\u003e, arXiv:math/0408319 [math.NT], 2004.",
				"M. Koike, \u003ca href=\"http://projecteuclid.org/euclid.nmj/1118787564\"\u003eOn McKay's conjecture\u003c/a\u003e, Nagoya Math. J., 95 (1984), 85-89.",
				"Y. Martin, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-96-01743-6\"\u003eMultiplicative eta-quotients\u003c/a\u003e, Trans. Amer. Math. Soc. 348 (1996), no. 12, 4825-4856, see page 4852 Table I.",
				"Y. Martin and K. Ono, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9939-97-03928-2\"\u003eEta-quotients and elliptic curves\u003c/a\u003e, Proc. Amer. Math. Soc. 125 (1997), no. 11, 3169-3176. MR1401749 (97m:11057)",
				"J.-P. Serre, \u003ca href=\"http://dx.doi.org/10.1090/S0273-0979-03-00992-3\"\u003eOn a theorem of Jordan\u003c/a\u003e, Bull. Amer. Math. Soc., 40 (No. 4, 2003), 429-440, see p. 434.",
				"Michael Somos, \u003ca href=\"/A030203/a030203.txt\"\u003eIndex to Yves Martin's list of 74 multiplicative eta-quotients and their A-numbers\u003c/a\u003e"
			],
			"formula": [
				"Expansion of eta(q) * eta(q^23) in powers of q.",
				"Euler transform of period 23 sequence [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -2, ...]. - _Michael Somos_, May 02 2005",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2), A(x^4)) where f(u, v, w) = -v^3 + 2 *u * v * w + 2 * u * w^2 + u^2 * w. - _Michael Somos_, May 02 2005",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2), A(x^3), A(x^6)) where f(u1, u2, u3, u6) = u1^2 * u3 * u6 +2 * u1 * u2 * u3 * u6 - 2 * u1 * u6^3 + 2 * u2^2 * u3 * u6 - u2 * u3^3. - _Michael Somos_, May 02 2005",
				"a(n) is multiplicative with a(23^e) = 1. Let y = number of zeros of x^3 - x - 1 modulo p, then a(p^e) = (1 + (-1)^e)/2 if y = 1, a(p^e) = e+1 if y = 3, a(p^e) = (e-1)%3 - 1 if y = 0. - _Michael Somos_, Oct 19 2005",
				"a(8*n + 4) = a(23*n + 5) = a(23*n + 7) = a(23*n + 10) = a(23*n + 11) = a(23*n + 14) = a(23*n + 15) = a(23*n + 19) = a(23*n + 20) = a(23*n + 21) = a(23*n + 22) = 0. - _Michael Somos_, Oct 19 2005",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (23 t)) = 23^(1/2) (t/i) f(t) where q = exp(2 Pi i t). - _Michael Somos_, Sep 08 2014",
				"2 * a(n) = A028959(n) - A028930(n). - _Michael Somos_, Sep 08 2014"
			],
			"example": [
				"G.f. = q - q^2 - q^3 + q^6 + q^8 -q ^13 - q^16 + q^23 - q^24 + q^25 + q^26 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ q QPochhammer[ q] QPochhammer[ q^23], {q, 0, n}]; (* _Michael Somos_, May 17 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c1, 0, n--; A = x * O(x^n); polcoeff( eta(x + A) * eta(x^23 + A), n))}; /* _Michael Somos_, May 02 2005 */",
				"(PARI) {a(n) = my(A, p, e, y); if( n\u003c1, 0, A = factor(n); prod( k=1, matsize(A)[1], [p, e] = A[k, ]; if( p==23, 1, y = sum( x=1, p-1, (x^3 - x - 1)%p == 0); if( y==1, 1-e%2, y, e+1, (e-1)%3 - 1))))}; /* _Michael Somos_, Oct 19 2005 */",
				"(PARI) {a(n) = if( n\u003c1, 0, qfrep([2, 1; 1, 12], n, 1)[n] - qfrep([4, 1; 1, 6], n, 1)[n])}; /* _Michael Somos_, Sep 08 2014 */",
				"(MAGMA) Basis( CuspForms( Gamma1(23), 1), 82) [1]; /* _Michael Somos_, Sep 08 2014 */"
			],
			"xref": [
				"Cf. A028930, A028959."
			],
			"keyword": "sign,mult",
			"offset": "1,59",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Reference to Martin and Ono added by Chandan Singh Dalawat (dalawat(AT)gmail.com), Jul 23 2010"
			],
			"references": 13,
			"revision": 42,
			"time": "2018-11-22T21:50:11-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}