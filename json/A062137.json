{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A062137",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 62137,
			"data": "1,4,-1,20,-10,1,120,-90,18,-1,840,-840,252,-28,1,6720,-8400,3360,-560,40,-1,60480,-90720,45360,-10080,1080,-54,1,604800,-1058400,635040,-176400,25200,-1890,70,-1,6652800,-13305600,9313920",
			"name": "Coefficient triangle of generalized Laguerre polynomials n!*L(n,3,x) (rising powers of x).",
			"comment": [
				"The row polynomials s(n,x) := n!*L(n,3,x) = Sum_{m=0..n} a(n,m)*x^m have e.g.f. exp(-z*x/(1-z))/(1-z)^4. They are Sheffer polynomials satisfying the binomial convolution identity s(n,x+y) = Sum_{k=0..n} binomial(n,k)*s(k,x)*p(n-k,y), with polynomials p(n,x) = Sum_{m=1..n} |A008297(n,m)|*(-x)^m, n \u003e= 1 and p(0,x)=1 (for Sheffer polynomials see A048854 for S. Roman reference).",
				"These polynomials appear in the radial part of the l=1 (p-wave) eigen functions for the discrete energy levels of the H-atom. See Messiah reference.",
				"The unsigned version of this triangle is the triangle of unsigned 2-Lah numbers A143497. - _Peter Bala_, Aug 25 2008",
				"This matrix (unsigned) is embedded in that for n!*L(n,-3,-x). Introduce 0,0,0 to each unsigned row and then add 1,-2,1,4,2,1 to the beginning of the array as the first three rows to generate n!*L(n,-3,-x). - _Tom Copeland_, Apr 21 2014",
				"From _Wolfdieter Lang_, Jul 07 2014: (Start)",
				"The standard Rodrigues formula for the monic generalized Laguerre polynomials (also called Laguerre-Sonin polynomials) is Lm(n,alpha,x) := (-1)^n*n!*L(n,3,x) is x^(-alpha)*exp(x)*(d/dx)^n(exp(-x)*x^(n+alpha)).",
				"Another Rodrigues type formula is Lm(n,alpha,x) = exp(x)*x^(-alpha+n+1)*(-x^2*d/dx)^n*(exp(-x)*x^(alpha+1)). This is derivable from the differential - difference relation of Lm(n,alpha,x) which yields first the creation operator formula -(x*d/dx + (-x + alpha + n + 1))*Lm(n,alpha,x) = Lm(n+1,alpha,x) or in the variable q = log(x) the operator -(d/dq + alpha + n + 1 - exp(q)).",
				"The identity (due to Christoph Mayer) (d/dq - (d/dq)W(q))*f(q) = exp(W(q)*d/dq(exp(-W(q)*f(q)) is then iterated with W(q) = W(alpha,n,q) = exp(q) - (alpha + n + 1)*q and a further change of variables leads then to the given result. (End)"
			],
			"reference": [
				"A. Messiah, Quantum mechanics, vol. 1, p. 419, eq.(XI.18a), North Holland, 1969."
			],
			"link": [
				"Indranil Ghosh, \u003ca href=\"/A062137/b062137.txt\"\u003eRows 0..125, flattened\u003c/a\u003e",
				"Wolfdieter Lang, \u003ca href=\"/A062137/a062137.pdf\"\u003eFirst eleven rows of the triangle.\u003c/a\u003e",
				"\u003ca href=\"/index/La#Laguerre\"\u003eIndex entries for sequences related to Laguerre polynomials\u003c/a\u003e"
			],
			"formula": [
				"a(n, m) = ((-1)^m)*n!*binomial(n+3, n-m)/m!.",
				"E.g.f. for m-th column sequence: ((-x/(1-x))^m)/(m!*(1-x)^4), m \u003e= 0."
			],
			"example": [
				"The triangle a(n,m) begins:",
				"n\\m       0        1       2     3    4   5 ...",
				"0:        1",
				"1:        4       -1",
				"2:       20      -10      1",
				"3:      120      -90     18     -1",
				"4:      840     -840    252    -28    1",
				"5:     6720    -8400   3360   -560   40  -1",
				"... Formatted by _Wolfdieter Lang_, Jul 07 2014",
				"For more rows see the link.",
				"n = 2: 2!*L(2,3,x) = 20 - 10*x + x^2."
			],
			"mathematica": [
				"Flatten[Table[((-1)^m)*n!*Binomial[n+3,n-m]/m!,{n,0,9},{m,0,n}]] (* _Indranil Ghosh_, Feb 23 2017 *)"
			],
			"program": [
				"(PARI) row(n) = Vecrev(n!*pollaguerre(n, 3)); \\\\ _Michel Marcus_, Feb 06 2021"
			],
			"xref": [
				"For m=0..5 the (unsigned) columns give A001715, A061206, A062141-A062144. The row sums (signed) give A062146, the row sums (unsigned) give A062147.",
				"Cf. A143497. - _Peter Bala_, Aug 25 2008",
				"Cf. A062139, A105278. - _Wolfdieter Lang_, Jul 07 2014"
			],
			"keyword": "sign,easy,tabl",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Jun 19 2001",
			"references": 16,
			"revision": 49,
			"time": "2021-02-06T07:08:49-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}