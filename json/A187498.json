{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A187498",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 187498,
			"data": "0,0,1,0,1,1,1,1,2,1,3,3,4,4,6,5,10,10,14,15,20,20,34,35,48,55,69,75,117,124,165,199,241,274,406,440,571,714,846,988,1417,1560,1988,2548,2977,3536,4965,5525,6953,9061,10490,12597,17443,19551",
			"name": "Let i be in {1,2,3,4} and let r \u003e= 0 be an integer. Let p={p_1, p_2, p_3, p_4} = {-3,0,1,2}, n=3*r+p_i, and define a(-3)=0. Then a(n)=a(3*r+p_i) gives the quantity of H_(9,4,0) tiles in a subdivided H_(9,i,r) tile after linear scaling by the factor Q^r, where Q=sqrt(2*cos(Pi/9)).",
			"comment": [
				"Theory: (Start)",
				"1. Definitions. Let T_(9,j,0) denote the rhombus with sides of unit length (=1), interior angles given by the pair (j*Pi/9,(9-j)*Pi/9) and Area(T_(9,j,0))=sin(j*Pi/9), j in {1,2,3,4}. Associated with T_(9,j,0) are its angle coefficients (j, 9-j) in which one coefficient is even while the other is odd. A half-tile is created by cutting T_(9,j,0) along a line extending between its two corners with even angle coefficient; let H_(9,j,0) denote this half-tile. Similarly, a T_(9,j,r) tile is a linearly scaled version of T_(9,j,0) with sides of length Q^r and Area(T_(9,j,r))=Q^(2*r)*sin(j*Pi/9), r\u003e=0 an integer, where Q is the positive, constant square root Q=sqrt(2*cos(Pi/9)); likewise let H_(9,j,r) denote the corresponding half-tile. Often H_(9,i,r) (i in {1,2,3,4}) can be subdivided into an integral number of each equivalence class H_(9,j,0). But regardless of whether or not H_(9,j,r) subdivides, in theory such a proposed subdivision for each j can be represented by the matrix M=(m_(i,j)), i,j=1,2,3,4, in which the entry m_(i,j) gives the quantity of H_(9,j,0) tiles that should be present in a subdivided H_(9,i,r) tile. The number Q^(2*r) (the square of the scaling factor) is an eigenvalue of M=(U_1)^r, where",
				"U_1=",
				"(0 1 0 0)",
				"(1 0 1 0)",
				"(0 1 0 1)",
				"(0 0 1 1).",
				"2. The sequence. Let r\u003e=0, and let D_r be the r-th \"block\" defined by D_r={a(3*r-3),a(3*r),a(3*r+1),a(3*r+2)} with a(-3)=0. Note that D_r-D_(r-1)-3*D_(r-2)+2*D_(r-3)+D_(r-4)={0,0,0,0}, for r\u003e=4, with initial conditions {D_k}={{0,0,0,1},{0,0,1,1},{0,1,1,2},{1,1,3,3}}, k=0,1,2,3. Let p={p_1,p_2,p_3,p_4}={-3,0,1,2} and n=3*r+p_i. Then a(n)=a(3*r+p_i)=m_(i,4), where M=(m_(i,j))=(U_1)^r was defined above. Hence the block D_r corresponds component-wise to the fourth column of M, and a(3*r+p_i)=m_(i,4) gives the quantity of H_(9,4,0) tiles that should appear in a subdivided H_(9,i,r) tile. (End)",
				"Combining blocks A_r, B_r, C_r and D_r, from A187495, A187496, A187497 and this sequence, respectively, as matrix columns [A_r,B_r,C_r,D_r] generates the matrix (U_1)^r, and a negative index (-1)*r yields the corresponding inverse [A_(-r),B_(-r),C_(-r),D_(-r)]=(U_1)^(-r) of (U_1)^r.. Therefore the four sequences need not be causal.",
				"Since U_1 is symmetric, so is M=(U_1)^r, so the block D_r also corresponds to the fourth row of M. Therefore, alternatively, for j=1,2,3,4, a(3r+p_j)=m_(4,j) gives the quantity of H_(9,j,0) tiles that should be present in a H_(9,4,r) tile.",
				"Since a(3*r)=a(3*(r+1)-3) for all r, this sequence arises by concatenation of fourth-column entries m_(2,4), m_(3,4) and m_(4,4) (or fourth-row entries m_(4,2), m_(4,3) and m_(4,4)) from successive matrices M=(U_1)^r."
			],
			"reference": [
				"L. E. Jeffery, Unit-primitive matrices and rhombus substitution tilings, (in preparation)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A187498/b187498.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e"
			],
			"formula": [
				"Recurrence: a(n) = a(n-3) +3*a(n-6) -2*a(n-9) -a(n-12), for n \u003e= 12, with initial conditions {a(m)} = {0,0,1,0,1,1,1,1,2,1,3,3}, m=0,1,...,11.",
				"G.f.: -x^2*(1+x)*(x^6+3*x^4+2*x^2+1) / ( (1+x+x^2)*(x^9+3*x^6-1) )."
			],
			"maple": [
				"A052931 := proc(n) if n \u003c 0 then 0; else coeftayl(1/(1-3*x^2-x^3),x=0,n) ; end if; end proc:",
				"A052931a := proc(n) if n mod 3 = 0 then A052931(n/3) ; else 0 ; end if; end proc:",
				"A057078 := proc(n) op(1+(n mod 3),[1,0,-1]) ; end proc:",
				"A187498 := proc(n) -A057078(n) +A052931a(n) +2*A052931a(n-2) +A052931a(n-3) +3*A052931a(n-4) +2*A052931a(n-5) +A052931a(n-6) +3*A052931a(n-7) -A052931a(n-8) ; %/3 ; end proc:",
				"seq(A187498(n),n=0..20) ; # _R. J. Mathar_, Mar 22 2011"
			],
			"mathematica": [
				"CoefficientList[Series[-x^2*(1 + x)*(x^6 + 3*x^4 + 2*x^2 + 1)/((1 + x + x^2)*(x^9 + 3*x^6 - 1)), {x, 0, 1000}], x] (* _G. C. Greubel_, Sep 23 2017 *)"
			],
			"program": [
				"(PARI) x='x+O('x^50); Vec(-x^2*(1+x)*(x^6+3*x^4+2*x^2+1)/((1+x+x^2)*(x^9+3*x^6-1))) \\\\ _G. C. Greubel_, Sep 23 2017"
			],
			"xref": [
				"Cf. A187495, A187496, A187497."
			],
			"keyword": "nonn,easy",
			"offset": "0,9",
			"author": "_L. Edson Jeffery_, Mar 16 2011",
			"references": 7,
			"revision": 16,
			"time": "2019-02-03T03:59:03-05:00",
			"created": "2011-03-10T17:29:56-05:00"
		}
	]
}