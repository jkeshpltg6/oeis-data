{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A317301",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 317301,
			"data": "0,1,-2,1,-5,0,-9,-2,-14,-5,-20,-9,-27,-14,-35,-20,-44,-27,-54,-35,-65,-44,-77,-54,-90,-65,-104,-77,-119,-90,-135,-104,-152,-119,-170,-135,-189,-152,-209,-170,-230,-189,-252,-209,-275,-230,-299,-252,-324,-275,-350,-299,-377,-324,-405,-350,-434",
			"name": "Sequence obtained by taking the general formula for generalized k-gonal numbers: m*((k - 2)*m - k + 4)/2, where m = 0, +1, -1, +2, -2, +3, -3, ... and k \u003e= 5. Here k = 1.",
			"comment": [
				"Taking the same formula with k = 0 we have A317300.",
				"Taking the same formula with k = 2 we have A001057 (canonical enumeration of integers).",
				"Taking the same formula with k = 3 we have 0 together with A008795 (Molien series for 3-dimensional representation of dihedral group D_6 of order 6).",
				"Taking the same formula with k = 4 we have A008794 (squares repeated) except the initial zero.",
				"Taking the same formula with k \u003e= 5 we have the generalized k-gonal numbers (see Crossrefs section)."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A317301/b317301.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,2,-2,-1,1)."
			],
			"formula": [
				"From _Bruno Berselli_, Jul 30 2018: (Start)",
				"O.g.f.: x*(1 - 3*x + x^2)/((1 + x)^2*(1 - x)^3).",
				"E.g.f.: (-5*(1 + 2*x) + (5 - 2*x^2)*exp(2*x))*exp(-x)/16.",
				"a(n) = a(-n+1) = a(n-1) + 2*a(n-2) - 2*a(n-3) - a(n-4) + a(n-5).",
				"a(n) = (-2*n*(n + 1) - 5*(2*n + 1)*(-1)^n + 5)/16. Therefore:",
				"a(n) = -n*(n + 6)/8 for even n;",
				"a(n) = -(n - 5)*(n + 1)/8 for odd n. Also:",
				"a(n) = a(n-5) for odd n \u003e 3.",
				"2*(2*n - 1)*a(n) + 2*(2*n + 1)*a(n-1) + n*(n^2 - 3) = 0. (End)"
			],
			"mathematica": [
				"Table[(-2 n (n + 1) - 5 (2 n + 1) (-1)^n + 5)/16, {n, 0, 60}] (* _Bruno Berselli_, Jul 30 2018 *)"
			],
			"program": [
				"(MAGMA) /* By definition: */ k:=1; [0] cat [m*i*((k-2)*m*i-k+4)/2: i in [1,-1], m in [1..30]]; // _Bruno Berselli_, Jul 30 2018",
				"(PARI) concat(0, Vec(x*(1 - 3*x + x^2)/((1 + x)^2*(1 - x)^3) + O(x^50))) \\\\ _Colin Barker_, Aug 01 2018"
			],
			"xref": [
				"Row 1 of A303301.",
				"Cf. A000096, A001057, A008794, A008795, A317300.",
				"Sequences of generalized k-gonal numbers: A001318 (k=5), A000217 (k=6), A085787 (k=7), A001082 (k=8), A118277 (k=9), A074377 (k=10), A195160 (k=11), A195162 (k=12), A195313 (k=13), A195818 (k=14), A277082 (k=15), A274978 (k=16), A303305 (k=17), A274979 (k=18), A303813 (k=19), A218864 (k=20), A303298 (k=21), A303299 (k=22), A303303 (k=23), A303814 (k=24), A303304 (k=25), A316724 (k=26), A316725 (k=27), A303812 (k=28), A303815 (k=29), A316729 (k=30)."
			],
			"keyword": "sign,easy",
			"offset": "0,3",
			"author": "_Omar E. Pol_, Jul 29 2018",
			"references": 3,
			"revision": 38,
			"time": "2018-08-01T10:52:43-04:00",
			"created": "2018-07-31T09:58:52-04:00"
		}
	]
}