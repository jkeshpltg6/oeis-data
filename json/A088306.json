{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A088306",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 88306,
			"data": "1,-2,-11,-33,-52174,260515,-573204,37362253,-42781604,122925461,534483448,3083975227,902209779836,-2685575996367,-65398140378926,74357078147863,214112296674652,642336890023956,-5920787228742393,-12055686754159438,18190586279576483,-48436859313312404",
			"name": "Integers n with tan n \u003e |n|, ordered by |n|.",
			"comment": [
				"Name was \"Positive integers n with |tan n| \u003e n.\" before signs were added. The sign here shows whether tan(|n|) is positive or negative.",
				"That this sequence is infinite was proved by Bellamy, Lagarias and Lazebnik. It seems not to be known whether there are infinitely many n with tan n \u003e n.",
				"At approximately 2.37e154, there is a value of n which has tan(n)/n \u003e 556. - _Phil Carmody_, Mar 04 2007 [This is index 214 in the b-file.]",
				"As n increases, log(|a(n)|)/n seems to approach Pi/2; this is similar to what would be expected if an integer sequence were created by drawing many random numbers independently from a uniform distribution on the interval [-Pi/2,+Pi/2] and including in the sequence only those integers j for which the j-th random number x_j happened to satisfy |x_j| \u003c 1/j (and applying to j the sign of x_j). - _Jon E. Schoenfield_, Aug 19 2014; updated Nov 07 2014 to reflect the change in the sequence's Name)"
			],
			"link": [
				"Jon E. Schoenfield, \u003ca href=\"/A088306/b088306.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"D. Bellamy, J. C. Lagarias and F. Lazebnik, \u003ca href=\"http://www.math.udel.edu/~lazebnik/papers/tan_n.pdf\"\u003eProposed problem: large values of Tan n\u003c/a\u003e",
				"Jon E. Schoenfield, \u003ca href=\"/A088306/a088306_3.txt\"\u003eMagma program\u003c/a\u003e"
			],
			"maple": [
				"a:=proc(n) if abs(evalf(tan(n)))\u003en then n else fi end: seq(a(n),n=1..100000); # _Emeric Deutsch_, Dec 18 2004"
			],
			"mathematica": [
				"Select[Range[600000],Abs[Tan[#]]\u003e#\u0026] (* _Harvey P. Dale_, Nov 30 2012 *)"
			],
			"program": [
				"(PARI) is(n)=tan(n)\u003eabs(n) \\\\ _Charles R Greathouse IV_, Nov 07 2014"
			],
			"xref": [
				"Cf. A000503, A224269, A079330, A088989.",
				"Cf. A249836 (subsequence of positive terms)."
			],
			"keyword": "sign",
			"offset": "1,2",
			"author": "_Paul Boddington_, Nov 05 2003",
			"ext": [
				"More terms from _Jon E. Schoenfield_, Aug 17 2014",
				"Signs added and other edits by _Franklin T. Adams-Watters_, Sep 09 2014"
			],
			"references": 8,
			"revision": 47,
			"time": "2021-03-07T00:11:19-05:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}