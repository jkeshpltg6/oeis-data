{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A061712",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 61712,
			"data": "2,3,7,23,31,311,127,383,991,2039,3583,6143,8191,73727,63487,129023,131071,522239,524287,1966079,4128767,16250879,14680063,33546239,67108351,201064447,260046847,536739839,1073479679,5335154687,2147483647",
			"name": "Smallest prime with Hamming weight n (i.e., with exactly n 1's when written in binary).",
			"comment": [
				"a(n) = 2^n - 1 for n in A000043, so Mersenne primes A000668 is a subsequence of this one. Binary length of a(n) is given by A110699 and the number of zeros in a(n) is given by A110700. - _Max Alekseyev_, Aug 03 2005",
				"Drmota, Mauduit, \u0026 Rivat prove that a(n) exists for n \u003e N for some N. - _Charles R Greathouse IV_, May 17 2010",
				"A000120(a(n)) = A014499(A049084(a(n))) = n. - _Reinhard Zumkeller_, Feb 10 2013"
			],
			"link": [
				"T. D. Noe and Charles R Greathouse IV, \u003ca href=\"/A061712/b061712.txt\"\u003eTable of n, a(n) for n = 1..3320\u003c/a\u003e (first 1024 terms from T. D. Noe)",
				"Michael Drmota, Christian Mauduit, and Joel Rivat, \u003ca href=\"http://www.dmg.tuwien.ac.at/drmota/DMRcomp2.pdf\"\u003ePrimes with an average sum of digits\u003c/a\u003e, Compositio Mathematica 145 (2009), pp. 271-292.",
				"MathOverflow, \u003ca href=\"http://mathoverflow.net/questions/22629\"\u003eAre there primes of every Hamming weight?\u003c/a\u003e",
				"Samuel S. Wagstaff, \u003ca href=\"http://projecteuclid.org/euclid.em/999188636\"\u003ePrime numbers with a fixed number of one bits or zero bits in their binary representation\u003c/a\u003e, Experimental Mathematics 10 (2001), pp. 267-273."
			],
			"formula": [
				"Conjecture: a(n) \u003c 2^(n+3). - _T. D. Noe_, Mar 14 2008"
			],
			"example": [
				"The fourth term is 23 (10111 in binary), since no prime less than 23 has exactly 4 1's in its binary representation."
			],
			"maple": [
				"with(combstruct); a:=proc(n) local m,is,s,t,r; if n=1 then return 2 fi; r:=+infinity; for m from 0 to 100 do is := iterstructs(Combination(n-2+m),size=n-2); while not finished(is) do s := nextstruct(is); t := 2^(n-1+m)+1+add(2^i,i=s); # print(s,t); if isprime(t) then r:=min(t,r) fi; od; if r\u003c+infinity then return r fi; od; return 0; end; seq(a(n),n=1..60); # _Max Alekseyev_, Aug 03 2005"
			],
			"mathematica": [
				"Do[k = 1; While[ Count[ IntegerDigits[ Prime[k], 2], 1] != n, k++ ]; Print[ Prime[k]], {n, 1, 30} ]",
				"(* Second program: *)",
				"a[n_] := Module[{m, s, k, p}, For[m=0, True, m++, s = {1, Sequence @@ #, 1} \u0026 /@ Permutations[Join[Table[1, {n-2}], Table[0, {m}]]] // Sort; For[k=1, k \u003c= Length[ s], k++, p = FromDigits[s[[k]], 2]; If[PrimeQ[p], Print[\"a(\", n, \") = \", p]; Return[p]]]]]; a[1] = 2; Array[a, 100] (* _Jean-François Alcover_, Mar 16 2015 *)",
				"Module[{hw=Table[{n,DigitCount[n,2,1]},{n,Prime[Range[250*10^6]]}]}, Table[ SelectFirst[hw,#[[2]]==k\u0026],{k,31}]][[All,1]] (* Requires Mathematica version 10 or later *) (* _Harvey P. Dale_, Jan 01 2019 *)"
			],
			"program": [
				"(Haskell)",
				"a061712 n = fromJust $ find ((== n) . a000120) a000040_list",
				"-- _Reinhard Zumkeller_, Feb 10 2013",
				"(PARI) a(n)=forprime(p=2, , if (hammingweight(p) == n, return(p));); \\\\ _Michel Marcus_, Mar 16 2015",
				"(Python)",
				"from itertools import combinations",
				"from sympy import isprime",
				"def A061712(n):",
				"    l, k = n-1, 2**n",
				"    while True:",
				"        for d in combinations(range(l-1,-1,-1),l-n+1):",
				"            m = k-1 - sum(2**(e) for e in d)",
				"            if isprime(m):",
				"                return m",
				"        l += 1",
				"        k *= 2 # _Chai Wah Wu_, Sep 02 2021"
			],
			"xref": [
				"Cf. A000043, A000668, A001348, A066195, A110699, A110700."
			],
			"keyword": "nonn,base,nice,changed",
			"offset": "1,1",
			"author": "_Alexander D. Healy_, Jun 19 2001",
			"ext": [
				"Extended to 60 terms by _Max Alekseyev_, Aug 03 2005",
				"Further terms from _T. D. Noe_, Mar 14 2008"
			],
			"references": 23,
			"revision": 55,
			"time": "2022-01-16T12:48:14-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}