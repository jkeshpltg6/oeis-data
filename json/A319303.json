{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A319303",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 319303,
			"data": "1,2,4,8,16,32,5,64,10,128,20,21,3,256,40,42,6,512,80,84,12,85,13,168,24,1024,160,336,48,170,26,672,96,2048,320,1344,192,340,52,2688,384,341,53,5376,768,680,104,10752,1536,4096,640,21504,3072,113,17,43008",
			"name": "a(n) is the value of the node of the Collatz tree encoded by the number n (see Comments for precise definition).",
			"comment": [
				"For any n \u003e= 0: to find the node corresponding to n:",
				"- move to the root of the Collatz tree (that is, to the node with value 1),",
				"- set r = n",
				"- while r \u003e 0",
				"      decrement r",
				"      if the current node is a branching node different from 4",
				"         (that is, the current node has a value v such that v \u003e 4 and v+2 is a multiple of 6)",
				"      then",
				"         if r is even",
				"         then",
				"            move to the child corresponding to a halving step",
				"         else",
				"            move to the child corresponding to a tripling step",
				"         end",
				"         divide r by 2 (and round down)",
				"      else",
				"         move to the only child (this child corresponds to a halving step)",
				"      end",
				"  end",
				"- the value of the ending node corresponds to a(n).",
				"With this procedure, we can uniquely encode with a nonnegative number the position of any node rooted to 1 in the Collatz tree.",
				"If the Collatz conjecture is true, then this sequence contains all positive integers."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A319303/b319303.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A319303/a319303.png\"\u003eIllustration of first terms\u003c/a\u003e",
				"\u003ca href=\"/index/3#3x1\"\u003eIndex entries for sequences related to 3x+1 (or Collatz) problem\u003c/a\u003e"
			],
			"example": [
				"For n = 18, we visit the following nodes:",
				"  r   Node  Is branching node?",
				"  --  ----  ------------------",
				"  18     1  No",
				"  17     2  No",
				"  16     4  No",
				"  15     8  No",
				"  14    16  Yes",
				"   6     5  No",
				"   5    10  Yes",
				"   2    20  No",
				"   1    40  Yes",
				"   0    80  No",
				"Hence, a(18) = 80."
			],
			"mathematica": [
				"a[n_] := Module[{r=n, v=1}, While[r != 0, r--; If[v\u003e4 \u0026\u0026 Mod[(v+2), 6] == 0, v = If[Mod[r, 2] == 0, 2v, (v-1)/3]; r = Quotient[r, 2], v = 2v]]; v];",
				"Table[a[n], {n, 0, 55}] (* _Jean-François Alcover_, Dec 18 2018, translated from PARI *)"
			],
			"program": [
				"(PARI) a(n) = my (r=n, v=1); while (r, r--; if (v\u003e4 \u0026\u0026 (v+2)%6==0, v=if (r%2==0, 2*v, (v-1)/3); r \\= 2, v = 2*v)); v"
			],
			"xref": [
				"Cf. A322521 (inverse)."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Rémy Sigrist_, Dec 10 2018",
			"references": 2,
			"revision": 42,
			"time": "2018-12-18T03:37:56-05:00",
			"created": "2018-12-16T14:59:16-05:00"
		}
	]
}