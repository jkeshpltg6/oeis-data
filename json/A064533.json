{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A064533",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 64533,
			"data": "7,6,4,2,2,3,6,5,3,5,8,9,2,2,0,6,6,2,9,9,0,6,9,8,7,3,1,2,5,0,0,9,2,3,2,8,1,1,6,7,9,0,5,4,1,3,9,3,4,0,9,5,1,4,7,2,1,6,8,6,6,7,3,7,4,9,6,1,4,6,4,1,6,5,8,7,3,2,8,5,8,8,3,8,4,0,1,5,0,5,0,1,3,1,3,1,2,3,3,7,2,1,9,3,7,2,6,9,1,2,0,7,9,2,5,9,2,6,3,4,1,8,7,4,2,0,6,4,6,7,8,0,8,4,3,2,3,0,6,3,3,1,5,4,3,4,6,2,9,3,8,0,5,3,1,6,0,5,1,7,1,1,6,9,6,3,6,1,7,7,5,0,8,8,1,9,9,6,1,2,4,3,8,2,4,9,9,4,2,7,7,6,8,3,4,6,9,0,5,1,6,2,3,5,1,3,9,2,1,8,7,1,9,6,2,0,5,6,9,0,5,3,2,9,5,6,4,4,6,7,0,4",
			"name": "Decimal expansion of Landau-Ramanujan constant.",
			"comment": [
				"Named after the German mathematician Edmund Georg Hermann Landau (1877-1938) and the Indian mathematician Srinivasa Ramanujan (1887-1920). - _Amiram Eldar_, Jun 20 2021"
			],
			"reference": [
				"Bruce C. Berndt, Ramanujan's notebook part IV, Springer-Verlag, 1994, pp. 52,60-66; MR 95e: 11028.",
				"Steven R. Finch, Mathematical Constants, Cambridge, 2003, pp. 98-104.",
				"G. H. Hardy, \"Ramanujan, Twelve lectures on subjects suggested by his life and work\", Chelsea, 1940, pp. 60-63; MR 21 # 4881.",
				"Edmund Landau, Über die Einteilung der positiven ganzen Zahlen in vier Klassen nach der Mindestzahl der zu ihrer additiven Zusammensetzung erforderlichen Quadrate. Arch. Math. Phys., 13, 1908, pp. 305-312."
			],
			"link": [
				"David E. G. Hare, \u003ca href=\"/A064533/b064533.txt\"\u003eTable of n, a(n) for n = 0..125078\u003c/a\u003e",
				"Steven R. Finch, \u003ca href=\"http://www.people.fas.harvard.edu/~sfinch/constant/lr/lr.html\"\u003eLandau-Ramanujan Constant\u003c/a\u003e. [Broken link]",
				"Steven R. Finch, \u003ca href=\"http://web.archive.org/web/20010605004309/http://www.mathsoft.com/asolve/constant/lr/lr.html\"\u003eLandau-Ramanujan Constant\u003c/a\u003e. [From the Wayback machine]",
				"Steven R. Finch, \u003ca href=\"http://web.archive.org/web/20010605004309/http://www.mathsoft.com/asolve/constant/lr/lr.html\"\u003eLandau-Ramanujan Constant\u003c/a\u003e. [From the Wayback Machine]",
				"Steven R. Finch, \u003ca href=\"http://www.people.fas.harvard.edu/~sfinch/csolve/fermat.pdf\"\u003eOn a Generalized Fermat-Wiles Equation\u003c/a\u003e. [Broken link]",
				"Steven R. Finch, \u003ca href=\"http://web.archive.org/web/20010602030546/http://www.mathsoft.com/asolve/fermat/fermat.html\"\u003eOn a Generalized Fermat-Wiles Equation\u003c/a\u003e. [From the Wayback Machine]",
				"Philippe Flajolet and Ilan Vardi, \u003ca href=\"http://algo.inria.fr/flajolet/Publications/landau.ps\"\u003eZeta function expansions of some classical constants\u003c/a\u003e, Feb 18 1996.",
				"Etienne Fouvry, Claude Levesque and Michel Waldschmidt, \u003ca href=\"https://arxiv.org/abs/1712.09019\"\u003eRepresentation of integers by cyclotomic binary forms\u003c/a\u003e, arXiv:1712.09019 [math.NT], 2017.",
				"Xavier Gourdon and Pascal Sebah, \u003ca href=\"http://numbers.computation.free.fr/Constants/Miscellaneous/Records.html\"\u003eConstants and records of computation\u003c/a\u003e.",
				"David E. G. Hare, \u003ca href=\"/A064533/a064533_1.txt\"\u003e125,079 digits of the Landau-Ramanujan constant\u003c/a\u003e.",
				"David E. G. Hare, \u003ca href=\"http://www.plouffe.fr/simon/constants/LandauRamanujan.txt\"\u003eLandau-Ramanujan constant up to 10000 digits\u003c/a\u003e.",
				"Institute of Physics, \u003ca href=\"http://scenta.co.uk/tcaep/science/constant/details/landauramanujanconstant.xml\"\u003eConstants - Landau-Ramanujan Constant\u003c/a\u003e.",
				"Simon Plouffe, \u003ca href=\"http://www.worldwideschool.org/library/books/sci/math/MiscellaneousMathematicalConstants/chap51.html\"\u003eLandau Ramanujan constant\u003c/a\u003e.",
				"Daniel Shanks, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1964-0159174-9\"\u003eThe second-order term in the asymptotic expansion of B(x)\u003c/a\u003e, Mathematics of Computation, Vol. 18, No. 85 (1964), pp. 75-86.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Landau-RamanujanConstant.html\"\u003eRamanujan constant\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Landau-Ramanujan_constant\"\u003eLandau-Ramanujan constant\u003c/a\u003e.",
				"Robert G. Wilson v, \u003ca href=\"/A064533/a064533.txt\"\u003eThe first 15584 digits of the Landau-Ramanujan constant\u003c/a\u003e."
			],
			"example": [
				"0.76422365358922066299069873125009232811679054139340951472168667374..."
			],
			"mathematica": [
				"First@ RealDigits@ N[1/Sqrt@2 Product[((1 - 2^(-2^k)) 4^(2^k) Zeta[2^k]/(Zeta[2^k, 1/4] - Zeta[2^k, 3/4]))^(2^(-k - 1)), {k, 8}], 2^8] (* _Robert G. Wilson v_, Jul 01 2007 *)",
				"(* Victor Adamchik calculated 5100 digits of the Landau-Ramanujan constant using Mathematica (from Mathematica 4 demos): *) LandauRamanujan[n_] := With[{K = Ceiling[Log[2, n*Log[3, 10]]]}, N[Product[(((1 - 2^(-2^k))*4^2^k*Zeta[2^k])/(Zeta[2^k, 1/4] - Zeta[2^k, 3/4]))^2^(-k - 1), {k, 1, K}]/Sqrt[2], n]];",
				"(* The code reported here is the code at https://library.wolfram.com/infocenter/Demos/120/. Looking carefully at the outputs reported there one sees that: the last 8 digits of the 500-digit output (\"74259724\") are not the same as those listed in the 1000-digit output (\"94247095\"); the same happens with the last 18 digits of the 1000-digit output (\"584868265713856413\") and the corresponding ones in the 5100-digit output (\"852514327407923660\"). - _Alessandro Languasco_, May 07 2021 *)"
			],
			"xref": [
				"Cf. A125776 = Continued fraction. - _Harry J. Smith_, May 13 2009",
				"Cf. A000692, A001481, A009003, A075880, A090735, A090736, A227158."
			],
			"keyword": "cons,nonn",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_, Oct 08 2001",
			"ext": [
				"More references needed! Hardy and Wright? Gruber and Lekkerkerker?",
				"More terms from _Vladeta Jovovic_, Oct 08 2001"
			],
			"references": 36,
			"revision": 91,
			"time": "2021-06-20T02:48:40-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}