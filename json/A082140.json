{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A082140",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 82140,
			"data": "1,7,56,336,1680,7392,29568,109824,384384,1281280,4100096,12673024,38019072,111132672,317521920,889061376,2444918784,6615662592,17641766912,46425702400,120706826240,310388981760,790081044480",
			"name": "A transform of binomial(n,6).",
			"comment": [
				"Seventh row of number array A082137. C(n,6) has e.g.f. (x^6/6!)exp(x). The transform averages the binomial and inverse binomial transforms."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A082140/b082140.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_07\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (14,-84,280,-560,672,-448,128)."
			],
			"formula": [
				"a(n) = (2^(n-1) + 0^n/2)*C(n+6,n).",
				"a(n) = Sum_{j=0..n} C(n+6, j+6)*C(j+6, 6)*(1+(-1)^j)/2.",
				"G.f.: (1 - 7*x + 42*x^2 - 140*x^3 + 280*x^4 - 336*x^5 + 224*x^6 - 64*x^7)/ (1-2*x)^7.",
				"E.g.f.: (x^6/6!)*exp(x)*cosh(x) (with 6 leading zeros).",
				"a(n) = ceiling(binomial(n+6,6)*2^(n-1)). - _Zerinvary Lajos_, Nov 01 2006",
				"From _Amiram Eldar_, Jan 07 2022: (Start)",
				"Sum_{n\u003e=0} 1/a(n) = 89/5 - 24*log(2).",
				"Sum_{n\u003e=0} (-1)^n/a(n) = 5832*log(3/2) - 11819/5. (End)"
			],
			"example": [
				"a(0) = (2^(-1) + 0^0/2)*binomial(6,0) = 2*(1/2) = 1 (use 0^0 = 1)."
			],
			"maple": [
				"[seq (ceil(binomial(n+6,6)*2^(n-1)),n=0..22)]; # _Zerinvary Lajos_, Nov 01 2006"
			],
			"mathematica": [
				"Drop[With[{nmax = 56}, CoefficientList[Series[x^6*Exp[x]*Cosh[x]/6!, {x, 0, nmax}], x]*Range[0, nmax]!], 5] (* or *) Join[{1}, Table[2^(n-1)* Binomial[n+6,n], {n,1,30}] (* _G. C. Greubel_, Feb 05 2018 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^30)); Vec(serlaplace(x^6*exp(x)*cosh(x)/6!)) \\\\ _G. C. Greubel_, Feb 05 2018",
				"(MAGMA) [(2^(n-1) + 0^n/2)*Binomial(n+6,n): n in [0..30]]; // _G. C. Greubel_, Feb 05 2018"
			],
			"xref": [
				"Cf. A082137, A082139, A080951.",
				"For n\u003e0, a(n) = 1/2 * A002409(n)."
			],
			"keyword": "easy,nonn,changed",
			"offset": "0,2",
			"author": "_Paul Barry_, Apr 06 2003",
			"references": 11,
			"revision": 24,
			"time": "2022-01-07T06:15:19-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}