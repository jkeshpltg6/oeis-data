{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A244154",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 244154,
			"data": "1,2,3,5,4,8,13,14,6,11,18,23,25,38,63,41,7,17,28,32,39,53,88,68,61,74,123,113,172,188,313,122,9,20,33,50,46,83,138,95,72,116,193,158,270,263,438,203,85,182,303,221,424,368,613,338,666,515,858,563,1201,938,1563,365,10,26,43,59,60",
			"name": "Permutation of natural numbers: a(0) = 1, a(1) = 2, a(2n) = A254049(a(n)), a(2n+1) = 3*a(n)-1; composition of A048673 and A005940.",
			"comment": [
				"Note the indexing: the domain starts from 0, while the range excludes zero.",
				"From _Antti Karttunen_, May 30 2017: (Start)",
				"This sequence can be represented as a binary tree. Each left hand child is obtained by applying A254049(n) when the parent contains n, and each right hand child is obtained by applying A016789(n-1) (i.e., multiply by 3, subtract one) to the parent's contents:",
				"                                     1",
				"                                     |",
				"                  ...................2...................",
				"                 3                                       5",
				"       4......../ \\........8                  13......../ \\........14",
				"      / \\                 / \\                 / \\                 / \\",
				"     /   \\               /   \\               /   \\               /   \\",
				"    /     \\             /     \\             /     \\             /     \\",
				"   6       11         18       23         25       38         63       41",
				"  7 17   28  32     39  53   88  68     61  74  123  113   172  188  313 122",
				"etc.",
				"This is a mirror image of the tree depicted in A245612.",
				"(End)"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A244154/b244154.txt\"\u003eTable of n, a(n) for n = 0..8192\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A048673(A005940(n+1)).",
				"From _Antti Karttunen_, May 30 2017: (Start)",
				"a(0) = 1, a(1) = 2, a(2n) = A254049(a(n)), a(2n+1) = 3*a(n)-1.",
				"a(n) = A245612(A054429(n)).",
				"(End)"
			],
			"program": [
				"(Scheme)",
				"(define (A244154 n) (A048673 (A005940 (+ 1 n))))",
				";; Implementing a new recurrence, with memoization-macro definec:",
				"(definec (A244154 n) (cond ((\u003c= n 1) (+ 1 n)) ((even? n) (A254049 (A244154 (/ n 2)))) (else (+ -1 (* 3 (A244154 (/ (- n 1) 2))))))) ;; _Antti Karttunen_, May 30 2017"
			],
			"xref": [
				"Inverse: A244153.",
				"Cf. A005940, A048673, A054429, A243065-A243066, A243505-A243506, A245608, A245610, A245612, A016789, A254049, A285712, A285714, A286613."
			],
			"keyword": "nonn,tabf",
			"offset": "0,2",
			"author": "_Antti Karttunen_, Jun 27 2014",
			"references": 21,
			"revision": 20,
			"time": "2021-02-27T21:32:21-05:00",
			"created": "2014-07-31T23:49:38-04:00"
		}
	]
}