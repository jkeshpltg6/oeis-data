{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A046787",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 46787,
			"data": "0,0,1,5,17,46,113,254,546,1122,2242,4354,8286,15441,28303,51025,90699,159003,275355,471216,797761,1336686,2218393,3648177,5948503,9620406,15439833,24597942,38916192,61159549,95508014,148241050,228753319,351022425,535760584",
			"name": "Number of partitions of 5n with equal nonzero number of parts congruent to each of 1, 2, 3 and 4 modulo 5.",
			"comment": [
				"Number of partitions of m with equal numbers of parts congruent to each of 1, 2, 3 and 4 (mod 5) is 0 unless m == 0 mod 5."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A046787/b046787.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e (terms n=0..100 from Alois P. Heinz)",
				"\u003ca href=\"/wiki/Partitions_of_5n\"\u003eIndex and properties of sequences related to partitions of 5n\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A046776(n) + A202086(n) + A202088(n) - A000041(n) = A202192(n) - A000041(n). - _Max Alekseyev_",
				"G.f.: (Sum_{k\u003e0} x^(2*k)/(Product_{j=1..k} 1 - x^j)^4)/(Product_{j\u003e0} 1 - x^j). - _Andrew Howroyd_, Sep 16 2019"
			],
			"maple": [
				"mkl:= proc(i,l) local ll, mn, x; ll:= `if`(irem(i, 5)=0, l, applyop(x-\u003ex+1, irem(i,5), l)); mn:= min(l[])-1; `if`(mn\u003c=0, ll, map(x-\u003ex-mn, ll)) end:",
				"g:= proc(n,i,t) local m, mx; if n\u003c0 then 0 elif n=0 then `if`(t[1]\u003e0 and t[1]=t[2] and t[2]=t[3] and t[3]=t[4], 1, 0) elif i=0 then 0 elif i\u003c5 then mx:= max(t[]); m:= n-10*mx +t[1] +t[2]*2 +t[3]*3 +t[4]*4; `if`(m\u003e=0 and irem(m, 10)=0, 1, 0) else g(n,i,t):= g(n, i-1, t) + g(n-i, i, mkl(i, t)) fi end:",
				"a:= n-\u003e g(5*n, 5*n, [0,0,0,0]):",
				"seq(a(n), n=0..20);  # _Alois P. Heinz_, Jul 04 2009"
			],
			"mathematica": [
				"mkl[i_, l_] := Module[{ll, mn, x}, ll = If[Mod[i, 5] == 0, l, MapAt[#+1\u0026, l, Mod[i, 5]]]; mn = Min[l]-1; If[mn \u003c= 0, ll, Map[#-mn\u0026, ll]]];",
				"g[n_, i_, t_] := g[n, i, t] = Module[{m, mx}, If[n\u003c0, 0, If[n==0, If[ t[[1]]\u003e0 \u0026\u0026 Equal @@ t[[1;;4]], 1, 0], If[i==0, 0, If[i\u003c5, mx = Max[t]; m = n - 10 mx + t[[1]] + 2 t[[2]] + 3 t[[3]] + 4 t[[4]]; If[m \u003e= 0 \u0026\u0026 Mod[m, 10]==0, 1, 0], g[n, i-1, t] + g[n-i, i, mkl[i, t]]]]]]];",
				"a[n_] := g[5n, 5n, {0, 0, 0, 0}];",
				"Table[a[n], {n, 0, 34}] (* _Jean-François Alcover_, May 25 2019, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(PARI) seq(n)={Vec(sum(k=1, n\\2, x^(2*k)/prod(j=1, k, 1 - x^j + O(x*x^(n-2*k)))^4)/prod(j=1, n, 1 - x^j + O(x*x^n)), -(n+1))} \\\\ _Andrew Howroyd_, Sep 16 2019"
			],
			"xref": [
				"Other similar sequences include:",
				"  Mod 4: A046778, A046779, A046780, A046781, A046782.",
				"  Mod 5: A046783, A046784, A046785, A046786.",
				"Cf. A046765, A046776, A202192."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_David W. Wilson_",
			"ext": [
				"a(17)-a(32) from _Alois P. Heinz_, Jul 04 2009",
				"a(33)-a(34) from _Alois P. Heinz_, Aug 13 2013"
			],
			"references": 11,
			"revision": 30,
			"time": "2019-09-16T18:14:19-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}