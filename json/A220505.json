{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A220505",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 220505,
			"data": "2,16,88,364,1309,4126,11992,32368,82590,200487,467152,1049224,2283364,4829302,9959035,20069790,39612612,76703340,145945332,273224940,503888206,916373028,1644925432,2916814954,5113148026,8866911378,15220453704",
			"name": "a(n) = spt(5n+4)/5 where spt(n) = A092269(n).",
			"comment": [
				"That spt(5n+4) == 0 (mod 5) is one of the congruences stated by George E. Andrews. See theorem 2 in the Andrews' paper. See also A220507 and A220513."
			],
			"link": [
				"G. E. Andrews, \u003ca href=\"http://www.math.psu.edu/vstein/alg/antheory/preprint/andrews/17.pdf\"\u003eThe number of smallest parts in the partitions of n\u003c/a\u003e",
				"G. E. Andrews, F. G. Garvan, and J. Liang, \u003ca href=\"http://www.math.psu.edu/andrews/pdf/287.pdf\"\u003eCombinatorial interpretation of congruences for the spt-function\u003c/a\u003e",
				"K. C. Garrett, C. McEachern, T. Frederick, O. Hall-Holt, \u003ca href=\"http://www.deepdyve.com/lp/elsevier/fast-computation-of-andrews-smallest-part-statistic-and-conjectured-pV0SFgLi27/1\"\u003eFast computation of Andrews' smallest part statistic and conjectured congruences\u003c/a\u003e, Discrete Applied Mathematics, 159 (2011), 1377-1380.",
				"F. G. Garvan, \u003ca href=\"https://qseries.org/fgarvan/papers/spt.pdf\"\u003eCongruences for Andrews' smallest parts partition function and new congruences for Dyson's rank\u003c/a\u003e",
				"F. G. Garvan, \u003ca href=\"https://qseries.org/fgarvan/papers/spt2.pdf\"\u003eCongruences for Andrews' spt-function modulo powers of 5, 7 and 13\u003c/a\u003e",
				"F. G. Garvan, \u003ca href=\"http://arxiv.org/abs/1011.1957\"\u003eCongruences for Andrews' spt-function modulo 32760 and extension of Atkin's Hecke-type partition congruences\u003c/a\u003e, arXiv:1011.1957 [math.NT], 2010.",
				"K. Ono, \u003ca href=\"http://www.mathcs.emory.edu/~ono/publications-cv/pdfs/131.pdf\"\u003eCongruences for the Andrews spt-function\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A092269(A016897(n))/5 = A220485(n)/5."
			],
			"mathematica": [
				"b[n_, i_] := b[n, i] = If[n==0 || i==1, n, {q, r} = QuotientRemainder[n, i]; If[r == 0, q, 0] + Sum[b[n - i j, i - 1], {j, 0, n/i}]];",
				"spt[n_] := b[n, n];",
				"a[n_] := spt[5n+4]/5;",
				"Table[a[n], {n, 0, 26}] (* _Jean-François Alcover_, Jan 30 2019, after _Alois P. Heinz_ in A092269 *)"
			],
			"xref": [
				"Cf. A016897, A071734, A092269, A220485, A220507, A220513."
			],
			"keyword": "nonn",
			"offset": "0,1",
			"author": "_Omar E. Pol_, Jan 18 2013",
			"references": 3,
			"revision": 28,
			"time": "2020-08-16T10:21:27-04:00",
			"created": "2013-01-27T23:05:08-05:00"
		}
	]
}