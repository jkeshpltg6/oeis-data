{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A274917",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 274917,
			"data": "1,2,3,4,2,3,2,4,3,1,4,1,2,5,1,3,1,4,1,4,1,3,1,2,4,2,3,2,3,4,1,3,4,2,4,2,3,5,2,3,2,3,2,4,2,4,3,1,3,1,4,1,4,1,2,3,2,4,2,1,3,1,5,1,2,4,1,4,1,4,1,4,1,3,1,3,1,2,4,2,4,2,3,2,3,2,3,4,1,4,1,3,1,3,4,2,4,2,3,4,1,3,5,2,3",
			"name": "Square spiral in which each new term is the least positive integer distinct from its (already assigned) eight neighbors.",
			"comment": [
				"The largest element is 5 and it is also the element with lower density in the spiral.",
				"See A275609 for proof that 5 is maximal and for further comments. - _N. J. A. Sloane_, Mar 24 2019"
			],
			"link": [
				"F. Michel Dekking, Jeffrey Shallit, and N. J. A. Sloane, \u003ca href=\"https://www.combinatorics.org/ojs/index.php/eljc/article/view/v27i1p52/8039\"\u003eQueens in exile: non-attacking queens on infinite chess boards\u003c/a\u003e, Electronic J. Combin., 27:1 (2020), #P1.52."
			],
			"formula": [
				"a(n) = A275609(n) + 1. - _Omar E. Pol_, Nov 14 2016"
			],
			"example": [
				"Illustration of initial terms as a spiral (n = 0..168):",
				".",
				".     2 - 3 - 2 - 1 - 5 - 1 - 3 - 1 - 2 - 4 - 2 - 4 - 2",
				".     |                                               |",
				".     4   1 - 4 - 3 - 2 - 4 - 2 - 4 - 3 - 1 - 3 - 1   3",
				".     |   |                                       |   |",
				".     2   3   2 - 1 - 5 - 1 - 3 - 1 - 2 - 4 - 2   4   2",
				".     |   |   |                               |   |   |",
				".     1   5   4   3 - 2 - 4 - 2 - 4 - 3 - 1   3   1   3",
				".     |   |   |   |                       |   |   |   |",
				".     4   2   1   5   1 - 3 - 1 - 5 - 2   4   2   4   2",
				".     |   |   |   |   |               |   |   |   |   |",
				".     1   3   4   2   4   2 - 4 - 3   1   3   1   3   1",
				".     |   |   |   |   |   |       |   |   |   |   |   |",
				".     4   2   1   3   1   3   1 - 2   4   2   4   2   4",
				".     |   |   |   |   |   |           |   |   |   |   |",
				".     1   3   4   2   4   2 - 4 - 3 - 1   3   1   3   1",
				".     |   |   |   |   |                   |   |   |   |",
				".     4   2   1   3   1 - 3 - 1 - 2 - 4 - 2   4   2   4",
				".     |   |   |   |                           |   |   |",
				".     1   3   4   2 - 4 - 2 - 4 - 3 - 1 - 3 - 1   3   1",
				".     |   |   |                                   |   |",
				".     4   2   1 - 3 - 1 - 3 - 1 - 2 - 4 - 2 - 4 - 2   4",
				".     |   |                                           |",
				".     1   3 - 4 - 2 - 4 - 2 - 4 - 3 - 1 - 3 - 1 - 3 - 1",
				".     |",
				".     2 - 5 - 1 - 3 - 1 - 3 - 1 - 2 - 4 - 2 - 4 - 2 - 4",
				".",
				"a(13) = 5 is the first \"5\" in the sequence and its four neighbors are 4 (southwest), 3 (south), 1 (southeast) and 2 (east) when a(13) is placed in the spiral.",
				"a(157) = 5 is the 6th \"5\" in the sequence and it is also the first \"5\" that is below the NE-SW main diagonal of the spiral (see the second term in the last row of the above diagram)."
			],
			"xref": [
				"Cf. A274913, A274921, A275609, A278354 (number of neighbors)."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Omar E. Pol_, Jul 11 2016",
			"references": 3,
			"revision": 65,
			"time": "2020-03-07T13:50:20-05:00",
			"created": "2016-11-22T22:08:59-05:00"
		}
	]
}