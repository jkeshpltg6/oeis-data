{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A324300",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 324300,
			"data": "1,0,-2,3,2,0,-2,-14,15,0,-2,22,2,0,-84,33,2,0,-2,38,172,0,-2,-508,323,0,-292,54,2,0,-2,1088,444,0,-2580,1753,2,0,-628,-2396,2,0,-2,86,8142,0,-2,-10366,8991,0,-1092,102,2,0,-16724,-6716,1372,0,-2,44844,2,0,-81846,58495,33284,0,-2,134,2028,0,-2,-127882,2,0,-62326,150,268492,0,-2,-428606,268541,0,-2,249196,100100,0,-3252,-26748,2,0,-738612,182,3724,0,-157780,1133312,2,0,-2517158,1462761,2,0,-2,-44508,2003576,0,-2,897068,2,0,-5332,-4625662,2,0,-344084,230,9352622,0,-3769924,-15721720,8097455",
			"name": "G.f.: Sum_{n\u003e=0} x^n * (x^(n+1) + i)^n / (1 + i*x^(n+1))^(n+1).",
			"comment": [
				"It is remarkable that the generating function results in a power series in x with only real coefficients.",
				"Odd terms occur only at n = k*(k+2) for k \u003e= 0 (conjecture);",
				"a(n*(n+2)) = A324303(n)."
			],
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A324300/b324300.txt\"\u003eTable of n, a(n) for n = 0..10201\u003c/a\u003e"
			],
			"formula": [
				"G.f. A(x) is defined by the following series.",
				"(1) Sum_{n\u003e=0} x^n * (x^(n+1) + i)^n / (1 + i*x^(n+1))^(n+1).",
				"(2) Sum_{n\u003e=0} x^n * (x^(n+1) - i)^n / (1 - i*x^(n+1))^(n+1).",
				"(3) Sum_{n\u003e=0} (i*x)^n * (1 - i*x^(n+1))^(2*n+1) / (1 + x^(2*n+2))^(n+1).",
				"(4) Sum_{n\u003e=0} (-i*x)^n * (1 + i*x^(n+1))^(2*n+1) / (1 + x^(2*n+2))^(n+1).",
				"FORMULAS INVOLVING TERMS.",
				"a(n*(n+2)) = 1 (mod 2) for n \u003e= 0.",
				"a(4*n+1) = 0 for n \u003e= 0.",
				"a(4*n+2) \u003c 0 for n \u003e= 0.",
				"a(p-1) = (-1)^((p-1)/2) * 2 for odd primes p."
			],
			"example": [
				"G.f.: A(x) = 1 - 2*x^2 + 3*x^3 + 2*x^4 - 2*x^6 - 14*x^7 + 15*x^8 - 2*x^10 + 22*x^11 + 2*x^12 - 84*x^14 + 33*x^15 + 2*x^16 - 2*x^18 + 38*x^19 + 172*x^20 - 2*x^22 - 508*x^23 + 323*x^24 - 292*x^26 + 54*x^27 + 2*x^28 - 2*x^30 + 1088*x^31 + 444*x^32 - 2580*x^34 + 1753*x^35 + ...",
				"such that",
				"A(x) = 1/(1+i*x) + x*(x^2+i)/(1+i*x^2)^2 + x^2*(x^3+i)^2/(1+i*x^3)^3 + x^3*(x^4+i)^3/(1+i*x^4)^4 + x^4*(x^5+i)^4/(1+i*x^5)^5 + x^5*(x^6+i)^5/(1+i*x^6)^6 + x^6*(x^7+i)^6/(1+i*x^7)^7 + x^7*(x^8+i)^7/(1+i*x^8)^8 + ...",
				"also",
				"A(x) = (1-i*x)/(1+x^2) + i*x*(1-i*x^2)^3/(1+x^4)^2 - x^2*(1-i*x^3)^5/(1+x^6)^3 - i*x^3*(1-i*x^4)^7/(1+x^8)^4 + x^4*(1-i*x^5)^9/(1+x^10)^5 + i*x^5*(1-i*x^6)^11/(1+x^12)^6 + i*x^6*(1-i*x^7)^11/(1+x^14)^7 + ...",
				"Note that the imaginary components in the above sums vanish.",
				"TRIANGLE FORM.",
				"This sequence may be written in the form of a triangle like so:",
				"1;",
				"0, -2, 3;",
				"2, 0, -2, -14, 15;,",
				"0, -2, 22, 2, 0, -84, 33;",
				"2, 0, -2, 38, 172, 0, -2, -508, 323;",
				"0, -292, 54, 2, 0, -2, 1088, 444, 0, -2580, 1753;",
				"2, 0, -628, -2396, 2, 0, -2, 86, 8142, 0, -2, -10366, 8991;",
				"0, -1092, 102, 2, 0, -16724, -6716, 1372, 0, -2, 44844, 2, 0, -81846, 58495;",
				"33284, 0, -2, 134, 2028, 0, -2, -127882, 2, 0, -62326, 150, 268492, 0, -2, -428606, 268541;",
				"0, -2, 249196, 100100, 0, -3252, -26748, 2, 0, -738612, 182, 3724, 0, -157780, 1133312, 2, 0, -2517158, 1462761;",
				"2, 0, -2, -44508, 2003576, 0, -2, 897068, 2, 0, -5332, -4625662, 2, 0, -344084, 230, 9352622, 0, -3769924, -15721720, 8097455; ...",
				"in which the right border consists of all the odd numbers in this sequence.",
				"RELATED SEQUENCES.",
				"The left border, a(n^2), in the above triangle begins:",
				"[1, 0, 2, 0, 2, 0, 2, 0, 33284, 0, 2, 0, 883460, 0, 2, ..., A324301(n), ...].",
				"The main diagonal, a(n*(n+1)), in the above triangle begins:",
				"[1, -2, -2, 2, 172, -2, -2, 1372, 2, -738612, -5332, ..., A324302(n), ...].",
				"The right border, a(n*(n+2)), in the above triangle begins:",
				"[1, 3, 15, 33, 323, 1753, 8991, 58495, 268541, 1462761, ..., A324303(n), ...],",
				"and appears to consist of all the odd terms in this sequence."
			],
			"program": [
				"(PARI) {a(n) = my(SUM = sum(m=0, n, x^m*(x^(m+1) + I +x*O(x^n))^m / (1 + I*x^(m+1) +x*O(x^n))^(m+1) ) ); polcoeff(SUM, n)}",
				"for(n=0,120, print1(a(n),\", \"))"
			],
			"xref": [
				"Cf. A324301, A324302, A324303.",
				"Cf. A323689 (variant), A323695."
			],
			"keyword": "sign",
			"offset": "0,3",
			"author": "_Paul D. Hanna_, Feb 21 2019",
			"references": 7,
			"revision": 14,
			"time": "2019-02-23T11:53:31-05:00",
			"created": "2019-02-21T20:28:05-05:00"
		}
	]
}