{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A061142",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 61142,
			"data": "1,2,2,4,2,4,2,8,4,4,2,8,2,4,4,16,2,8,2,8,4,4,2,16,4,4,8,8,2,8,2,32,4,4,4,16,2,4,4,16,2,8,2,8,8,4,2,32,4,8,4,8,2,16,4,16,4,4,2,16,2,4,8,64,4,8,2,8,4,8,2,32,2,4,8,8,4,8,2,32,16,4,2,16,4,4,4,16,2,16,4,8,4,4,4",
			"name": "Replace each prime factor of n with 2: a(n) = 2^bigomega(n), where bigomega = A001222, number of prime factors counted with multiplicity.",
			"comment": [
				"The inverse Möbius transform of A162510. - _R. J. Mathar_, Feb 09 2011"
			],
			"link": [
				"R. Zumkeller, \u003ca href=\"/A061142/b061142.txt\"\u003eTable of n, a(n) for n = 1..10005\u003c/a\u003e",
				"R. J. Mathar, \u003ca href=\"http://arxiv.org/abs/1106.4038\"\u003eSurvey of Dirichlet Series of Multiplicative Arithmetic Functions\u003c/a\u003e, arXiv:1106.4038 [math.NT], 2011-2012. See eq. (2.12).",
				"\u003ca href=\"/index/Eu#epf\"\u003eIndex entries for sequences computed from exponents in factorization of n\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{d divides n} 2^(bigomega(d)-omega(d)) = Sum_{d divides n} 2^(A001222(d) - A001221(d)). - _Benoit Cloitre_, Apr 30 2002",
				"a(n) = A000079(A001222(n)), i.e., a(n)=2^bigomega(n). - _Emeric Deutsch_, Feb 13 2005",
				"Totally multiplicative with a(p) = 2. - _Franklin T. Adams-Watters_, Oct 04 2006",
				"Dirichlet g.f.: Product_{p prime} 1/(1-2*p^(-s)). - _Ralf Stephan_, Mar 28 2015",
				"a(n) = A001316(A156552(n)). - _Antti Karttunen_, May 29 2017"
			],
			"example": [
				"a(100)=16 since 100=2*2*5*5 and so a(100)=2*2*2*2."
			],
			"maple": [
				"with(numtheory): seq(2^bigomega(n),n=1..95);"
			],
			"mathematica": [
				"Table[2^PrimeOmega[n], {n, 1, 95}] (* _Jean-François Alcover_, Jun 08 2013 *)"
			],
			"program": [
				"(PARI) a(n)=direuler(p=1,n,1/(1-2*X))[n] /* _Ralf Stephan_, Mar 28 2015 */",
				"(PARI) a(n) = 2^bigomega(n); \\\\ _Michel Marcus_, Aug 08 2017"
			],
			"xref": [
				"Cf. A000079, A001222, A001316, A034444, A069205, A123667, A124508, A156552."
			],
			"keyword": "easy,nonn,mult",
			"offset": "1,2",
			"author": "_Henry Bottomley_, May 29 2001",
			"references": 40,
			"revision": 49,
			"time": "2021-08-22T06:27:36-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}