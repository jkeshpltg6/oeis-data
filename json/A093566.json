{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A093566",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 93566,
			"data": "0,0,0,0,1,20,120,455,1330,3276,7140,14190,26235,45760,76076,121485,187460,280840,410040,585276,818805,1125180,1521520,2027795,2667126,3466100,4455100,5668650,7145775,8930376,11071620,13624345,16649480,20214480",
			"name": "a(n) = n*(n-1)*(n-2)*(n-3)*(n^2-3*n-2)/48.",
			"comment": [
				"a(n+1) is the number of chiral pairs of colorings of the faces of a cube (vertices of a regular octahedron) using n or fewer colors. - _Robert A. Russell_, Sep 28 2020"
			],
			"link": [
				"Solomon W. Golomb, \u003ca href=\"http://www.jstor.org/stable/2321859\"\u003eIterated binomial coefficients\u003c/a\u003e, Amer. Math. Monthly, 87 (1980), 719-727.",
				"\u003ca href=\"/index/Rec#order_07\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (7,-21,35,-35,21,-7,1)."
			],
			"formula": [
				"a(n) = binomial(binomial(n-1, 2), 3).",
				"G.f.: -x^4*(1+13*x+x^2)/(x-1)^7. - _R. J. Mathar_, Dec 08 2010",
				"a(n+1) = 1*C(n,3) + 16*C(n,4) + 30*C(n,5) + 15*C(n,6), where the coefficient of C(n,k) is the number of chiral pairs of colorings using exactly k colors. - _Robert A. Russell_, Sep 28 2020"
			],
			"example": [
				"For a(3+1) = 1, each of the three colors is applied to a pair of adjacent faces of the cube (vertices of the octahedron). - _Robert A. Russell_, Sep 28 2020"
			],
			"mathematica": [
				"Table[ Binomial[ Binomial[n-1, 2], 3], {n,0,32}]",
				"LinearRecurrence[{7,-21,35,-35,21,-7,1},{0,0,0,0,1,20,120},40] (* _Harvey P. Dale_, Feb 18 2016 *)"
			],
			"program": [
				"(Sage) [(binomial(binomial(n,2),3)) for n in range(-1, 33)] # _Zerinvary Lajos_, Nov 30 2009",
				"(PARI) a(n)=n*(n-1)*(n-2)*(n-3)*(n^2-3*n-2)/48 \\\\ _Charles R Greathouse IV_, Jun 11 2015"
			],
			"xref": [
				"From _Robert A. Russell_, Sep 28 2020: (Start)",
				"Cf. A047780 (oriented), A198833 (unoriented), A337898 (achiral) colorings.",
				"a(n+1) = A325006(3,n) (chiral pairs of colorings of orthotope facets or orthoplex vertices.",
				"a(n+1) = A337889(3,n) (chiral pairs of colorings of orthotope faces or orthoplex peaks).",
				"Other polyhedra: A000332 (tetrahedron), A337896 (cube/octahedron).",
				"(End)"
			],
			"keyword": "nonn,easy",
			"offset": "0,6",
			"author": "_Robert G. Wilson v_ and _Santi Spadaro_, Mar 31 2004",
			"ext": [
				"Edited (with a new definition) by _N. J. A. Sloane_, Jul 02 2008"
			],
			"references": 15,
			"revision": 38,
			"time": "2020-09-28T21:40:14-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}