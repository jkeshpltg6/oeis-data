{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A113652",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 113652,
			"data": "1,-1,0,-1,2,0,0,-1,1,-2,0,0,2,0,0,-1,2,-1,0,-2,0,0,0,0,3,-2,0,0,2,0,0,-1,0,-2,0,-1,2,0,0,-2,2,0,0,0,2,0,0,0,1,-3,0,-2,2,0,0,0,0,-2,0,0,2,0,0,-1,4,0,0,-2,0,0,0,-1,2,-2,0,0,0,0,0,-2,1,-2,0,0,4,0,0,0,2,-2,0,0,0,0,0,0,2,-1,0,-3,2,0,0,-2,0",
			"name": "Expansion of (1 - theta_4(q)^2) / 4 in powers of q.",
			"reference": [
				"B. C. Berndt, Ramanujan's Notebooks Part III, Springer-Verlag, see p. 114 Entry 8(v).",
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), p. 576.",
				"J. M. Borwein and P. B. Borwein, Pi and the AGM, Wiley, 1987.",
				"P. A. MacMahon, Combinatory Analysis, Cambridge Univ. Press, London and New York, Vol. 1, 1915 and Vol. 2, 1916; see vol. 2, p 28, Article 269."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A113652/b113652.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy]."
			],
			"formula": [
				"a(n) is multiplicative with a(2^e) = -1 if e\u003e0, a(p^e) = e+1 if p == 1 (mod 4), (1 + (-1)^e)/2 if p == 3 (mod 4).",
				"Expansion of (1 - eta(q)^4 / eta(q^2)^2) / 4 in powers of q.",
				"Moebius transform is period 8 sequence [ 1, -2, -1, 0, 1, 2, -1, 0, ...].",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2), A(x^3), A(x^6)) where f(u1, u2, u3, u6) = u2 - 2*u3 + u6 - u1^2 + 3*u3^2 + 2*u1*u3 - 4*u2*u6.",
				"G.f.: Sum_{k\u003e0} -(-1)^k * x^((k^2 + k)/2) / (1 + x^k).",
				"G.f.: Sum_{k\u003e0} -(-1)^k * x^k / (1 + x^(2*k)).",
				"G.f.: Sum_{k\u003e0} -(-1)^k * x^(2*k - 1) / (1 + x^(2*k - 1)).",
				"a(n) = -(-1)^n * A002654(n). a(n) = - A104794(n) / 4 unless n = 0.",
				"a(2*n) = - A002564(n). a(3*n + 1) = A258277(n). a(3*n + 2) = - A258278(n). a(4*n + 1) = A008441(n). a(4*n + 3) = 0. a(6*n + 2) = - A122856(n). a(6*n + 4 ) = - A122856(n). - _Michael Somos_, Jun 06 2015",
				"a(8*n + 1) = A113407(n). a(8*n + 5) = 2 * A053692(n). a(9*n + 3) = a(9*n + 6) = 0. - _Michael Somos_, Jun 06 2015"
			],
			"example": [
				"G.f. = x - x^2 - x^4 + 2*x^5 - x^8 + x^9 - 2*x^10 + 2*x^13 - x^16 + 2*x^17 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 1, 0, -(-1)^n DivisorSum[ n, KroneckerSymbol[ -4, #] \u0026]]; (* _Michael Somos_, Jun 06 2015 *)",
				"a[ n_] := SeriesCoefficient[ (1 - EllipticTheta[4, 0, q]^2) / 4, {q, 0, n}]; (* _Michael Somos_, Jun 06 2015 *)",
				"a[ n_] := With[ {m = InverseEllipticNomeQ @ -q}, SeriesCoefficient[(1 - EllipticK[m] / (Pi/2)) / 4, {q, 0, n}]]; (* _Michael Somos_, Jun 06 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, 0, -(-1)^n * sumdiv(n, d, kronecker( -4, d)))};",
				"(PARI) {a(n) = my(A, p, e); if( n\u003c1, 0, A = factor(n); prod(k=1, matsize(A)[1], [p, e] = A[k,]; if( p==2, -1, p%4==1, e+1, !(e%2))))};",
				"(PARI) {a(n) = if( n\u003c1, 0, direuler(p=2, n, if( p==2, 1 - X/(1 - X), 1 / ((1 - X) * (1 - kronecker( -4, p)*X))) )[n])};",
				"(PARI) {a(n) = my(A); if( n\u003c1, 0, A = x * O(x^n); polcoeff( (1 - eta(x + A)^4 / eta(x^2 + A)^2) / 4, n))};"
			],
			"xref": [
				"Cf. A002654, A053692, A008441, A104794, A113407, A122856, A122865, A258277, A258278."
			],
			"keyword": "sign,mult",
			"offset": "1,5",
			"author": "_Michael Somos_, Nov 03 2005",
			"references": 8,
			"revision": 14,
			"time": "2018-05-08T15:11:55-04:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}