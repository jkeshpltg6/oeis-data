{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A193522",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 193522,
			"data": "1,-4,14,-36,85,-180,360,-684,1246,-2196,3754,-6264,10226,-16380,25804,-40032,61275,-92628,138452,-204804,300040,-435672,627356,-896400,1271525,-1791324,2507426,-3488472,4825531,-6638688,9085888,-12373992",
			"name": "Expansion of (1/q) * ((chi(q^3) * chi(-q^6)) / (chi(q) * chi(-q^2)))^4 in powers of q where chi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A193522/b193522.txt\"\u003eTable of n, a(n) for n = -1..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of - c(-q) * b(q^4) / (b(-q) * c(q^4)) in powers of q where b(), c() are cubic AGM functions.",
				"Expansion of (eta(q) * eta(q^4)^2 * eta(q^6)^3 / (eta(q^2)^3 * eta(q^3) * eta(q^12)^2))^4 in powers of q.",
				"Euler transform of period 12 sequence [ -4, 8, 0, 0, -4, 0, -4, 0, 0, 8, -4, 0, ...].",
				"a(n) = -(-1)^n * A187091(n). a(2*n) = -4 * A128643(n).",
				"a(n) ~ -(-1)^n * exp(2*Pi*sqrt(n/3)) / (2 * 3^(1/4) * n^(3/4)). - _Vaclav Kotesovec_, Sep 07 2017"
			],
			"example": [
				"1/q - 4 + 14*q - 36*q^2 + 85*q^3 - 180*q^4 + 360*q^5 - 684*q^6 + 1246*q^7 + ..."
			],
			"mathematica": [
				" QP := QPochhammer; A193522[n_]:= SeriesCoefficient[((QP[q]*QP[q^4]^2 *QP[q^6]^3)/(QP[q^2]^3*QP[q^3]*QP[q^12]^2))^4, {q, 0, n}];Table[ A193522[n], {n, 0, 50}] (* _G. C. Greubel_, Dec 24 2017 *)"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c-1, 0, n++; A = x * O(x^n); polcoeff( (eta(x + A) * eta(x^4 + A)^2 * eta(x^6 + A)^3 / (eta(x^2 + A)^3 * eta(x^3 + A) * eta(x^12 + A)^2))^4, n))}"
			],
			"xref": [
				"Cf. A128643, A187091."
			],
			"keyword": "sign",
			"offset": "-1,2",
			"author": "_Michael Somos_, Jul 29 2011",
			"references": 4,
			"revision": 12,
			"time": "2021-03-12T22:24:46-05:00",
			"created": "2011-07-29T23:51:38-04:00"
		}
	]
}