{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A165959",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 165959,
			"data": "2,3,5,5,5,11,3,7,3,9,5,11,7,9,7,11,15,13,27,25,21,15,13,11,5,17,7,3,11,9,15,9,21,13,3,15,13,7,5,15,11,11,17,15,27,21,15,13,7,21,19,15,9,3,17,15,7,7,7,9,9,17,15,11,9,5,5,21,17,11,7,15,9",
			"name": "Size of the range of the Ramanujan Prime Corollary, 2*A168421(n) - A104272(n).",
			"comment": [
				"All but the first term is odd because A104272 has only one even term, 2. Because of all primes \u003e 2 are odd, 1 can be subtracted from each term.",
				"If this sequence has an infinite number of terms in which a(n) = 3, then the twin prime conjecture can be proved.",
				"R_n is the sequence A104272(n) and k = pi(R_n)= A000720(R_n) with i\u003ek.",
				"By comparing the fractions we can see that (p_(i+1)-p_i)/(2*sqrt(p_i)) and a(n)/(2*sqrt(p_k)) are \u003c 1 for all n \u003e 0, in fact a(n)/(1.8*sqrt(p_k)) \u003c 1 for all n \u003e 0. When taking into account numbers in A182873(n) and A190874(n) to sqrt(R_n) we see that A182873(n)/(A190874(n)*sqrt(R_n)) \u003c 1 for all n \u003e 1."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A165959/b165959.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"J. Sondow, \u003ca href=\"http://arxiv.org/abs/0907.5232\"\u003e Ramanujan primes and Bertrand's postulate\u003c/a\u003e, arXiv:0907.5232 [math.NT], 2009-201; Amer. Math. Monthly 116 (2009) 630-635.",
				"J. Sondow, J. W. Nicholson, and T. D. Noe, \u003ca href=\"http://arxiv.org/abs/1105.2249\"\u003eRamanujan Primes: Bounds, Runs, Twins, and Gaps\u003c/a\u003e, arXiv:1105.2249 [math.NT], 2011; J. Integer Seq. 14 (2011) Article 11.6.2.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Ramanujan_prime\"\u003eRamanujan Prime\u003c/a\u003e",
				"Marek Wolf, \u003ca href=\"http://arxiv.org/abs/1010.3945\"\u003eA Note on the Andrica Conjecture\u003c/a\u003e, arXiv:1010.3945 [math.NT], 2010."
			],
			"formula": [
				"a(n) = 2*A168421(n) - A104272(n)."
			],
			"example": [
				"A168421(19) = 127, A104272(19) = 227; so a(19) = 2*A168421(19) - A104272(19) = 254 - 227 = 27. Note: for n = 20, 21, 22, 23 A168421(n) = 127. Because A168421 remains the same for these n and A104272 increases, the size of the range for a(n) for these n decreases. Note: a(18) = 2*97 - 181 = 194 - 181 = 13. This is nearly half a(19). The actual gap betweens A104272(19) and the next prime, 229, is 2."
			],
			"mathematica": [
				"nn = 100; R = Table[0, {nn}]; s = 0;",
				"Do[If[PrimeQ[k], s++]; If[PrimeQ[k/2], s--]; If[s \u003c nn, R[[s + 1]] = k], {k, Prime[3 nn]}];",
				"A104272 = R + 1; t = Table[0, {nn}];",
				"Do[m = PrimePi[2 n] - PrimePi[n]; If[0 \u003c m \u003c= nn, t[[m]] = n], {n, 15 nn}];",
				"A168421 = NextPrime[Join[{1}, t]] // Most;",
				"A165979 = 2 A168421 - A104272 (* _Jean-François Alcover_, Nov 07 2018, after _T. D. Noe_ in A104272 *)"
			],
			"xref": [
				"Cf. A168421, A104272, A182873, A190874."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_John W. Nicholson_, Sep 12 2011",
			"references": 6,
			"revision": 48,
			"time": "2019-10-27T12:00:08-04:00",
			"created": "2011-09-20T17:48:55-04:00"
		}
	]
}