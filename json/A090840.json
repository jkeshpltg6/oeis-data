{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A090840",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 90840,
			"data": "11,5,11551,15551,1551551,15551551,1155555151,1555551551,11555555551,1155155555551,555555515551,555555555551,5555555555551,555155555555551,51555555551555551,51555555555555551,1155555555555555551,15551555555555555551,1155515555555555555551",
			"name": "Smallest prime whose product of digits is 5^n.",
			"link": [
				"Michael S. Branicky, \u003ca href=\"/A090840/b090840.txt\"\u003eTable of n, a(n) for n = 0..998\u003c/a\u003e"
			],
			"example": [
				"a(4) = 1551551 because its digital product is 5^4, and it is prime."
			],
			"maple": [
				"a:= proc(n) local k, t; for k from 0 do t:= min(select(isprime,",
				"      map(x-\u003e parse(cat(x[])), combinat[permute]([1$k, 5$n]))));",
				"      if t\u003cinfinity then return t fi od",
				"    end:",
				"seq(a(n), n=0..18);  # _Alois P. Heinz_, Nov 05 2021"
			],
			"mathematica": [
				"NextPrim[n_] := Block[{k = n + 1}, While[ !PrimeQ[k], k++ ]; k]; a = Table[0, {18}]; p = 2; Do[q = Log[5, Times @@ IntegerDigits[p]]; If[q != 0 \u0026\u0026 IntegerQ[q] \u0026\u0026 a[[q]] == 0, a[[q]] = p; Print[q, \" = \", p]]; p = NextPrim[p], {n, 1, 10^9}]",
				"For a(13); a = Map[ FromDigits, Permutations[{1, 1, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5}]]; Min[ Select[a, PrimeQ[ # ] \u0026]]"
			],
			"program": [
				"(Python)",
				"from sympy import isprime",
				"from sympy.utilities.iterables import multiset_permutations as mp",
				"def a(n):",
				"    if n \u003c 2: return [11, 5][n]",
				"    digits = n + 1",
				"    while True:",
				"        for p in mp(\"1\"*(digits-n-1) + \"5\"*n, digits-1):",
				"            t = int(\"\".join(p) + \"1\")",
				"            if isprime(t): return t",
				"        digits += 1",
				"print([a(n) for n in range(19)]) # _Michael S. Branicky_, Nov 05 2021"
			],
			"xref": [
				"Cf. A089365, A088653, A091465, A090841, A089298."
			],
			"keyword": "base,nonn",
			"offset": "0,1",
			"author": "_Robert G. Wilson v_, Dec 09 2003",
			"ext": [
				"a(17) and beyond from _Michael S. Branicky_, Nov 05 2021"
			],
			"references": 6,
			"revision": 7,
			"time": "2021-11-05T09:13:09-04:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}