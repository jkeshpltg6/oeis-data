{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A223903",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 223903,
			"data": "1,-1,1,-2,2,2,-1,0,-4,2,5,-2,0,-8,2,8,-3,2,-14,6,14,-6,4,-24,12,24,-11,4,-40,16,38,-16,5,-62,24,60,-24,10,-94,40,91,-38,18,-144,62,136,-57,24,-214,88,201,-82,30,-308,122,288,-117,48,-440,180,410",
			"name": "McKay-Thompson series of class 20C for the Monster group with a(0) = -1.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A223903/b223903.txt\"\u003eTable of n, a(n) for n = -1..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-1) * chi(q^5)^5 / chi(q) in powers of q where chi() is a Ramanujan theta function.",
				"Expansion of eta(q) * eta(q^4) * eta(q^10)^10 / (eta(q^2)^2 * eta(q^5)^5 * eta(q^20)^5) in powers of q.",
				"Euler transform of period 20 sequence [ -1, 1, -1, 0, 4, 1, -1, 0, -1, -4, -1, 0, -1, 1, 4, 0, -1, 1, -1, 0, ...].",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^3)) where f(u, v) = (u - v)^4 - u * (u - 1) * (u + 4) * v * (v - 1) * (v + 4).",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (20 t)) = g(t) where q = exp(2 Pi i t) and g() is the g.f. of A225701. - _Michael Somos_, Sep 04 2013",
				"G.f.: (1/x) * Product_{k\u003e0} (1 + x^(10*k - 5))^5 / (1 + x^(2*k - 1)).",
				"a(n) = A112159(n) = A145740(n) unless n=0. a(n) = -(-1)^n * A132980(n)."
			],
			"example": [
				"G.f. = 1/q - 1 + q - 2*q^2 + 2*q^3 + 2*q^4 - q^5 - 4*q^7 + 2*q^8 + 5*q^9 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (1/q) QPochhammer[ -q^5, q^10]^5 / QPochhammer[ -q, q^2], {q, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c-1, 0, n++; A = x * O(x^n); polcoeff( eta(x + A) * eta(x^4 + A) * eta(x^10 + A)^10 / (eta(x^2 + A)^2 * eta(x^5 + A)^5 * eta(x^20 + A)^5), n))};"
			],
			"xref": [
				"Cf. A112159, A132980, A145740, A225701."
			],
			"keyword": "sign",
			"offset": "-1,4",
			"author": "_Michael Somos_, Mar 29 2013",
			"references": 3,
			"revision": 20,
			"time": "2021-03-12T22:24:46-05:00",
			"created": "2013-03-29T14:37:53-04:00"
		}
	]
}