{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A209131",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 209131,
			"data": "1,2,1,2,4,3,2,8,12,5,2,12,28,28,11,2,16,52,84,68,21,2,20,84,188,236,156,43,2,24,124,356,612,628,356,85,2,28,172,604,1324,1852,1612,796,171,2,32,228,948,2532,4500,5316,4020,1764,341,2,36,292,1404",
			"name": "Triangle of coefficients of polynomials u(n,x) jointly generated with A209132; see the Formula section.",
			"comment": [
				"For a discussion and guide to related arrays, see A208510.",
				"Subtriangle of the triangle given by (1, 1, -2, 1, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (0, 1, 2, -2, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Mar 21 2012"
			],
			"formula": [
				"u(n,x) = u(n-1,x) + (x+1)*v(n-1,x),",
				"v(n,x) = 2x*u(n-1,x) + x*v(n-1,x),",
				"where u(1,x)=1, v(1,x)=1.",
				"From _Philippe Deléham_, Mar 21 2012: (Start)",
				"As DELTA-triangle with 0 \u003c= k \u003c= n:",
				"G.f.: (1-y*x+x^2-y*x^2-2*y^2*x^2)/(1-x-y*x-y*x^2-2*y^2*x^2).",
				"T(n,k) = T(n-1,k) + T(n-1,k-1) + T(n-2,k-1) + 2*T(n-2,k-2), T(0,0) = T(1,0) = T(2,1) = 1, T(2,0) = 2, T(1,1) = T(2,2) = 0 and T(n,k) = 0 if k \u003c 0 or if k \u003e n. (End)"
			],
			"example": [
				"First five rows:",
				"  1;",
				"  2,  1;",
				"  2,  4,  3;",
				"  2,  8, 12,  5;",
				"  2, 12, 28, 28, 11;",
				"First three polynomials u(n,x):",
				"  1",
				"  2 + x",
				"  2 + 4x + 3x^2",
				"From _Philippe Deléham_, Mar 21 2012: (Start)",
				"(1, 1, -2, 1, 0, 0, ...) DELTA (0, 1, 2, -2, 0, 0, ...) begins:",
				"  1;",
				"  1,   0;",
				"  2,   1,   0;",
				"  2,   4,   3,   0;",
				"  2,   8,  12,   5,   0;",
				"  2,  12,  28,  28,  11,   0;",
				"  2,  16,  52,  84,  68,  21,   0;",
				"  2,  20,  84, 188, 236, 156,  43,   0; (End)"
			],
			"mathematica": [
				"u[1, x_] := 1; v[1, x_] := 1; z = 16;",
				"u[n_, x_] := u[n - 1, x] + (x + 1)*v[n - 1, x];",
				"v[n_, x_] := 2 x*u[n - 1, x] + x*v[n - 1, x];",
				"Table[Expand[u[n, x]], {n, 1, z/2}]",
				"Table[Expand[v[n, x]], {n, 1, z/2}]",
				"cu = Table[CoefficientList[u[n, x], x], {n, 1, z}];",
				"TableForm[cu]",
				"Flatten[%]    (* A209131 *)",
				"Table[Expand[v[n, x]], {n, 1, z}]",
				"cv = Table[CoefficientList[v[n, x], x], {n, 1, z}];",
				"TableForm[cv]",
				"Flatten[%]    (* A209132 *)"
			],
			"xref": [
				"Cf. A209132, A208510."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Mar 05 2012",
			"references": 3,
			"revision": 13,
			"time": "2020-01-24T03:26:35-05:00",
			"created": "2012-03-06T11:51:02-05:00"
		}
	]
}