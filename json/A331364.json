{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A331364",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 331364,
			"data": "0,1,2,3,4,5,9,13,8,6,10,14,12,7,11,15,16,17,33,49,65,81,41,61,36,38,37,177,52,55,225,53,32,18,34,50,24,26,25,114,130,22,162,62,56,210,59,58,48,19,35,51,28,31,99,29,44,147,47,46,195,23,43,243,64",
			"name": "If the set of nonzero digits of n in some base of the form 2^2^k (with k \u003e= 0) has exactly two elements, let b be the least such base and u and v the corresponding two nonzero digits; the base b representation of a(n) is obtained by replacing the u's by v's and vice versa in the base b representation of n; otherwise a(n) = n.",
			"comment": [
				"This sequence is a self-inverse permutation of the nonnegative integers. See A332520 for the corresponding fixed points.",
				"For any m \u003e 1, we can devise a similar sequence by considering bases of the form m^2^k (with k \u003e= 0)."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A331364/b331364.txt\"\u003eTable of n, a(n) for n = 0..65536\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A331364/a331364.png\"\u003eScatterplot of the first 2^2^4 terms\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A331364/a331364_1.png\"\u003eColored scatterplot of the first 2^2^4 terms\u003c/a\u003e (where the color denotes the base b if any)",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(n) \u003c 2^2^k iff n \u003c 2^2^k for any n, k \u003e= 0.",
				"a(2^k) = 2^k for any k \u003e= 0.",
				"a(2^2^k-1) = 2^2^k-1 for any k \u003e= 0."
			],
			"example": [
				"For n = 73:",
				"- the base 2^2^0 representation of 73 is \"1001001\" which has only one kind of nonzero digits,",
				"- the base 2^2^1 representation of 73 is \"1021\" which has exactly two kinds of nonzero digits, \"1\" and \"2\",",
				"- so the base 2^2^1 representation of a(73) is \"2012\",",
				"- and a(73) = 134."
			],
			"program": [
				"(PARI) a(n) = { for (x=0, oo, my (b=2^2^x, d=if (n, digits(n, b), [0])); if (#d==1, return (n), my (uv=select(sign, Set(d))); if (#uv==2, return (",
				"fromdigits(apply (t -\u003e if (t==0, 0, t==uv[1], uv[2], uv[1]), d), b))))) }"
			],
			"xref": [
				"Cf. A001146, A332520 (fixed points)."
			],
			"keyword": "nonn,base",
			"offset": "0,3",
			"author": "_Rémy Sigrist_, Jun 24 2020",
			"references": 2,
			"revision": 52,
			"time": "2020-06-27T14:21:56-04:00",
			"created": "2020-06-27T09:06:52-04:00"
		}
	]
}