{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A216486",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 216486,
			"data": "0,1,-1,4,-3,14,-10,48,-37,166,-144,582,-570,2067,-2260,7421,-8923,26878,-35020,98039,-136612,359649,-529990,1325491,-2046310,4903786,-7868991,18199354,-30157768,67720279,-115255425,252540383,-439456837,943488036",
			"name": "a(n) is equal to the rational part (considering of the field Q(sqrt(13))) of the numbers A(n)/sqrt(13), where we have  A(n) = ((sqrt(13) - 1)/2)*A(n-1) + A(n-2) + ((3-sqrt(13))/2)*A(n-3), with A(0) = 6, A(1) = sqrt(13) - 1, and A(2) = 11 - sqrt(13).",
			"comment": [
				"The Berndt-type sequence number 2 for the argument 2*Pi/13 defined by the following relation: A216605(n) + a(n)*sqrt(13) = A(n) = 2*(c(1)^n + c(3)^n + c(4)^n), where c(j) := 2*cos(2*Pi*j/13), j=1..6. The numbers a(n), n=0,1,..., are all positive integers. We note that we also have A216605(n) - a(n)*sqrt(13) = B(n) = 2*(c(2)^n + c(5)^n + c(6)^n) and the following recurrence relation holds: B(n) = -((sqrt(13)+1)/2)*B(n-1) + B(n-2) + ((3+sqrt(13))/2)*B(n-3), with B(0) = 6, B(1) = -sqrt(13) - 1, and B(2) = 11 + sqrt(13).",
				"We note that the sums a(2*n+1) + a(2*n+2) are nonnegative only for n = 0..5."
			],
			"reference": [
				"R. Witula and D. Slota, Quasi-Fibonacci numbers of order 13 on the occasion  the Thirteenth International Conference on Fibonacci Numbers and Their Applications, Congressus Numerantium, 201 (2010), 89-107.",
				"R. Witula, On some applications of formulas for sums of the unimodular complex numbers, Wyd. Pracowni Komputerowej Jacka Skalmierskiego, Gliwice 2011 (in Polish)."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A216486/b216486.txt\"\u003eTable of n, a(n) for n = 0..500\u003c/a\u003e",
				"\u003ca href=\"/index/Rec\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (-1,5,4,-6,-3,1)."
			],
			"formula": [
				"G.f.: x*(1 - 2*x^2 + 2*x^3 + x^4)/(1 + x - 5*x^2 - 4*x^3 + 6*x^4 + 3*x^5 - x^6).",
				"a(n) = - a(n-1) + 5*a(n-2) + 4*a(n-3) - 6*a(n-4) - 3*a(n-5) + a(n-6), which from the respective polynomial-type formula follows given by Witula in section \"Formula\" in A216605."
			],
			"example": [
				"We have a(5) + a(6) + a(4) + a(2) = a(7) + a(8) + a(6) + a(2) = a(9) + a(5) + a(1) + a(10) + a(8) = 0 and",
				"  a(6) + a(9) + a(10) = a(11) + a(12) = 12.",
				"Moreover, the following relations hold: A(3) = 4*A(1), B(3) = 4*B(1), A(5) = 4*A(3) + 2*sqrt(13), B(5) = 4*B(3)-2*sqrt(13), A(7) = 4*A(5) + 8*sqrt(13), B(7) = 4*B(5)-8*sqrt(13), A(4) = 3*A(2) - 2, B(4) = 3*B(2) + 2, 6 + A(6) = 3*A(4) + A(2), and A(8) - 3*A(6) = 25 - A(5)/2."
			],
			"mathematica": [
				"LinearRecurrence[{-1, 5, 4, -6, -3, 1}, {0, 1, -1, 4, -3, 14}, 30]"
			],
			"program": [
				"(PARI) concat([0],Vec((1-2*x^2+2*x^3+x^4)/(1+x-5*x^2-4*x^3+6*x^4+3*x^5-x^6) + O(x^30))) \\\\ _Andrew Howroyd_, Feb 25 2018"
			],
			"xref": [
				"Cf. A216605."
			],
			"keyword": "sign,easy",
			"offset": "0,4",
			"author": "_Roman Witula_, Sep 11 2012",
			"references": 10,
			"revision": 34,
			"time": "2019-03-16T21:43:19-04:00",
			"created": "2012-09-12T10:17:28-04:00"
		}
	]
}