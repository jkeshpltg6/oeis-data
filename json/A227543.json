{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A227543",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 227543,
			"data": "1,1,1,1,1,2,1,1,1,3,3,3,2,1,1,1,4,6,7,7,5,5,3,2,1,1,1,5,10,14,17,16,16,14,11,9,7,5,3,2,1,1,1,6,15,25,35,40,43,44,40,37,32,28,22,18,13,11,7,5,3,2,1,1,1,7,21,41,65,86,102,115,118,118,113,106,96,85,73,63,53,42,34,26,20,15,11,7,5,3,2,1,1",
			"name": "Triangle defined by g.f. A(x,q) such that: A(x,q) = 1 + x*A(q*x,q)*A(x,q), as read by terms k=0..n*(n-1)/2 in rows n\u003e=0.",
			"comment": [
				"See related triangle A138158.",
				"Row sums are the Catalan numbers (A000108), set q=1 in the g.f. to see this.",
				"Antidiagonal sums equal A005169, the number of fountains of n coins.",
				"The maximum in each row of the triangle is A274291. - _Torsten Muetze_, Nov 28 2018",
				"The area between a Dyck path and the x-axis may be decomposed into unit area triangles of two types - up-triangles with vertices at the integer lattice points (x, y), (x+1, y+1) and (x+2, y) and down-triangles with vertices at the integer lattice points (x, y), (x-1, y+1) and (x+1, y+1). The table entry T(n,k) equals the number of Dyck paths of semilength n containing k down triangles. See the illustration in the Links section. Cf. A239927.  - _Peter Bala_, Jul 11 2019"
			],
			"link": [
				"Paul D. Hanna and Seiichi Manyama, \u003ca href=\"/A227543/b227543.txt\"\u003eTable of n, a(n) for n = 0..9919 (rows n=0..39 of triangle, flattened).\u003c/a\u003e (first 1351 terms from Paul D. Hanna)",
				"P. Bala, \u003ca href=\"/A227543/a227543.pdf\"\u003eIllustration of triangular decomposition of area beneath Dyck paths\u003c/a\u003e",
				"P. Bala, \u003ca href=\"/A224704/a224704.pdf\"\u003eThe area beneath small Schröder paths: Notes on A224704, A326453 and A326454\u003c/a\u003e, Section 4",
				"Luca Ferrari, \u003ca href=\"http://arxiv.org/abs/1207.7295\"\u003eUnimodality and Dyck paths\u003c/a\u003e, arXiv:1207.7295 [math.CO], 2012.",
				"FindStat - Combinatorial Statistic Finder, \u003ca href=\"http://www.findstat.org/StatisticsDatabase/St000005\"\u003eThe bounce statistic of a Dyck path\u003c/a\u003e, \u003ca href=\"http://www.findstat.org/StatisticsDatabase/St000006\"\u003eThe diagonal inversion statistic of a Dyck path\u003c/a\u003e, \u003ca href=\"http://www.findstat.org/StatisticsDatabase/St000012\"\u003eThe area of a Dyck path\u003c/a\u003e.",
				"Stéphane Ouvry and Alexios P. Polychronakos, \u003ca href=\"https://arxiv.org/abs/2105.14042\"\u003eExclusion statistics for particles with a discrete spectrum\u003c/a\u003e, arXiv:2105.14042 [cond-mat.stat-mech], 2021.",
				"Thomas Prellberg, \u003ca href=\"http://www.maths.qmul.ac.uk/~tp/talks/asymptotics.pdf\"\u003e Area-perimeter generating functions of lattice walks: q-series and their asymptotics\u003c/a\u003e, Slides, School of Mathematical Sciences, Queen Mary, University of London, July 1, 2009.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Rogers-RamanujanContinuedFraction.html\"\u003eRogers-Ramanujan Continued Fraction\u003c/a\u003e."
			],
			"formula": [
				"G.f.: A(x,q) = 1/(1 - x/(1 - q*x/(1 - q^2*x/(1 - q^3*x/(1 - q^4*x/(1 -...)))))), a continued fraction.",
				"G.f. satisfies: A(x,q) = P(x,q)/Q(x,q), where",
				"  P(x,q) = Sum_{n\u003e=0} q^(n^2) * (-x)^n / Product_{k=1..n} (1-q^k),",
				"  Q(x,q) = Sum_{n\u003e=0} q^(n*(n-1)) * (-x)^n / Product_{k=1..n} (1-q^k),",
				"due to Ramanujan's continued fraction identity.",
				"...",
				"Sum_{k=0..n*(n-1)/2} T(n,k)*k = 2^(2*n-1) - C(2*n+1,n) + C(2*n-1,n-1) = A006419(n-1) for n\u003e=1.",
				"Logarithmic derivative of the g.f. A(x,q), wrt x, yields triangle A227532.",
				"From _Peter Bala_, Jul 11 2019: (Start)",
				"(n+1)th row polynomial R(n+1,q) = Sum_{k = 0..n} q^k*R(k,x)*R(n-k,q), with R(0,q) = 1.",
				"1/A(q*x,q) is the generating function for the triangle A047998. (End)"
			],
			"example": [
				"G.f.: A(x,q) = 1 + x*(1) + x^2*(1 + q) + x^3*(1 + 2*q + q^2 + q^3)",
				"+ x^4*(1 + 3*q + 3*q^2 + 3*q^3 + 2*q^4 + q^5 + q^6)",
				"+ x^5*(1 + 4*q + 6*q^2 + 7*q^3 + 7*q^4 + 5*q^5 + 5*q^6 + 3*q^7 + 2*q^8 + q^9 + q^10)",
				"+ x^6*(1 + 5*q + 10*q^2 + 14*q^3 + 17*q^4 + 16*q^5 + 16*q^6 + 14*q^7 + 11*q^8 + 9*q^9 + 7*q^10 + 5*q^11 + 3*q^12 + 2*q^13 + q^14 + q^15) +...",
				"where g.f.:",
				"A(x,q) = Sum_{k=0..n*(n-1)/2, n\u003e=0} T(n,k)*x^n*q^k",
				"satisfies:",
				"A(x,q) = 1 + x*A(q*x,q)*A(x,q).",
				"This triangle of coefficients T(n,k) in A(x,q) begins:",
				"1;",
				"1;",
				"1, 1;",
				"1, 2, 1, 1;",
				"1, 3, 3, 3, 2, 1, 1;",
				"1, 4, 6, 7, 7, 5, 5, 3, 2, 1, 1;",
				"1, 5, 10, 14, 17, 16, 16, 14, 11, 9, 7, 5, 3, 2, 1, 1;",
				"1, 6, 15, 25, 35, 40, 43, 44, 40, 37, 32, 28, 22, 18, 13, 11, 7, 5, 3, 2, 1, 1;",
				"1, 7, 21, 41, 65, 86, 102, 115, 118, 118, 113, 106, 96, 85, 73, 63, 53, 42, 34, 26, 20, 15, 11, 7, 5, 3, 2, 1, 1;",
				"1, 8, 28, 63, 112, 167, 219, 268, 303, 326, 338, 338, 331, 314, 293, 268, 245, 215, 190, 162, 139, 116, 97, 77, 63, 48, 38, 28, 22, 15, 11, 7, 5, 3, 2, 1, 1; ..."
			],
			"mathematica": [
				"T[n_, k_] := Module[{P, Q},",
				"P = Sum[q^(m^2) (-x)^m/Product[1-q^j, {j, 1, m}] + x O[x]^n, {m, 0, n}];",
				"Q = Sum[q^(m(m-1)) (-x)^m/Product[1-q^j, {j, 1, m}] + x O[x]^n, {m, 0, n}];",
				"SeriesCoefficient[P/Q, {x, 0, n}, {q, 0, k}]",
				"];",
				"Table[T[n, k], {n, 0, 10}, {k, 0, n(n-1)/2}] // Flatten (* _Jean-François Alcover_, Jul 27 2018, from PARI *)"
			],
			"program": [
				"(PARI) /* From g.f. A(x,q) = 1 + x*A(q*x,q)*A(x,q): */",
				"{T(n, k)=local(A=1); for(i=1, n, A=1+x*subst(A, x, q*x)*A +x*O(x^n)); polcoeff(polcoeff(A, n, x), k, q)}",
				"for(n=0, 10, for(k=0, n*(n-1)/2, print1(T(n, k), \", \")); print(\"\"))",
				"(PARI) /* By Ramanujan's continued fraction identity: */",
				"{T(n,k)=local(P=1,Q=1);",
				"P=sum(m=0,n,q^(m^2)*(-x)^m/prod(k=1,m,1-q^k)+x*O(x^n));",
				"Q=sum(m=0,n,q^(m*(m-1))*(-x)^m/prod(k=1,m,1-q^k)+x*O(x^n));",
				"polcoeff(polcoeff(P/Q,n,x),k,q)}",
				"for(n=0, 10, for(k=0, n*(n-1)/2, print1(T(n, k), \", \")); print(\"\"))"
			],
			"xref": [
				"Cf. A138158, A227532, A005169, A000108, A274291, A047998, A224704, A239927."
			],
			"keyword": "nonn,tabf",
			"offset": "0,6",
			"author": "_Paul D. Hanna_, Jul 15 2013",
			"references": 32,
			"revision": 66,
			"time": "2021-09-10T21:59:44-04:00",
			"created": "2013-07-15T21:04:46-04:00"
		}
	]
}