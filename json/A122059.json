{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A122059",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 122059,
			"data": "1,0,0,1,1,2,3,0,4",
			"name": "Number of different polygonal knots with n straight line segments.",
			"comment": [
				"A spatial polygon is a finite set of straight line segments in R3 which intersect only at their endpoints; the lines are called edges and their endpoints are called vertices; exactly two edges meet at every vertex. There must be at least 3 edges to make a triangle (the trivial knot) and it is not hard to show that a knotted polygon must have at least 6 edges. \"Enumerating these polygons soon becomes impracticable because the number of cases explodes as n increases.\"",
				"Hong et al. prove: \"The lattice stick number s_L(K) of a knot K is defined to be the minimal number of straight line segments required to construct a stick presentation of K in the cubic lattice. In this paper, we find an upper bound on the lattice stick number of a nontrivial knot K, except trefoil knot, in terms of the minimal crossing number c(K) which is s_L(K) \u003c= 3 c(K) + 2. Moreover if K is a non-alternating prime knot, then s_L(K) \u003c= 3 c(K) - 4\". [Jonathan Vos Post, Sep 04 2012]"
			],
			"reference": [
				"Peter Cromwell, Knots and Links, Cambridge University Press, 2004, Sec. 1.3 (pp. 5-8), Appendix E."
			],
			"link": [
				"KyungPyo Hong, SungJong No, SeungSang Oh, \u003ca href=\"http://arxiv.org/abs/1209.0048\"\u003eUpper bound on lattice stick number of knots\u003c/a\u003e, arXiv:1209.0048v1 [math.GT], Sep 01 2012",
				"Bryson R. Payne, \u003ca href=\"http://www.oglethorpe.edu/faculty/~j_nardo/knots/advanced.htm\"\u003eAdvanced Knot Theory Topics\u003c/a\u003e, Knot Theory Online",
				"Robert G. Scharein, \u003ca href=\"http://www.colab.sfu.ca/KnotPlot/sticknumbers/\"\u003eStick numbers for minimal stick knots\u003c/a\u003e, Feb 15 2004"
			],
			"example": [
				"a(3) = 1 because the unique polygonal knot of 3 edges can be drawn with vertex coordinates (4,9,5), (7,-9,5), (-9,-3,5).",
				"a(6) = 1 because the unique polygonal knot of 6 edges can be drawn with vertex coordinates (4,9,5), (-7,-7,-5), (7,-9,5), (-1,9,-5), (-9,-3,5), (9,-5,-5).",
				"a(7) = 1 because the unique polygonal knot of 7 edges can be drawn with vertex coordinates (9,-6,3), (-4,-7,3), (1,7,2), (-9,2,-10), (4,-5,10), (2,2,-2), (-5,2,5)."
			],
			"xref": [
				"Cf. A002863 (number of prime knots with n crossings)."
			],
			"keyword": "hard,more,nonn",
			"offset": "3,6",
			"author": "_Jonathan Vos Post_, Sep 14 2006",
			"references": 0,
			"revision": 17,
			"time": "2013-01-06T02:07:40-05:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}