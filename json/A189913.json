{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A189913",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 189913,
			"data": "1,1,1,1,2,1,1,3,3,3,1,4,6,12,2,1,5,10,30,10,10,1,6,15,60,30,60,5,1,7,21,105,70,210,35,35,1,8,28,168,140,560,140,280,14,1,9,36,252,252,1260,420,1260,126,126,1,10,45,360,420,2520,1050,4200,630,1260,42",
			"name": "Triangle read by rows: T(n,k) = binomial(n, k) * k! / (floor(k/2)! * floor((k+2)/2)!).",
			"comment": [
				"The triangle may be regarded a generalization of the triangle A097610:",
				"A097610(n,k) = binomial(n,k)*(2*k)$/(k+1);",
				"T(n,k) = binomial(n,k)*(k)$/(floor(k/2)+1).",
				"Here n$ denotes the swinging factorial A056040(n). As A097610 is a decomposition of the Motzkin numbers A001006, a combinatorial interpretation of T(n,k) in terms of lattice paths can be expected.",
				"T(n,n) = A057977(n) which can be seen as extended Catalan numbers."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A189913/b189913.txt\"\u003eTable of n, a(n) for the first 100 rows, flattened\u003c/a\u003e",
				"Peter Luschny, \u003ca href=\"http://oeis.org/wiki/User:Peter_Luschny/TheLostCatalanNumbers\"\u003eThe lost Catalan numbers.\u003c/a\u003e"
			],
			"formula": [
				"From _R. J. Mathar_, Jun 07 2011: (Start)",
				"T(n,1) = n.",
				"T(n,2) = A000217(n-1).",
				"T(n,3) = A027480(n-2).",
				"T(n,4) = A034827(n). (End)"
			],
			"example": [
				"[0]  1",
				"[1]  1, 1",
				"[2]  1, 2,  1",
				"[3]  1, 3,  3,   3",
				"[4]  1, 4,  6,  12,  2",
				"[5]  1, 5, 10,  30, 10,  10",
				"[6]  1, 6, 15,  60, 30,  60,  5",
				"[7]  1, 7, 21, 105, 70, 210, 35, 35"
			],
			"maple": [
				"A189913 := (n,k) -\u003e binomial(n,k)*(k!/iquo(k,2)!^2)/(iquo(k,2)+1):",
				"seq(print(seq(A189913(n,k),k=0..n)),n=0..7);"
			],
			"mathematica": [
				"T[n_, k_] := Binomial[n, k]*k!/((Floor[k/2])!*(Floor[(k + 2)/2])!); Table[T[n, k], {n, 0, 10}, {k, 0, n}]// Flatten (* _G. C. Greubel_, Jan 13 2018 *)"
			],
			"program": [
				"(PARI) {T(n,k) = binomial(n,k)*k!/((floor(k/2))!*(floor((k+2)/2))!) };",
				"for(n=0,10, for(k=0,n, print1(T(n,k), \", \"))) \\\\ _G. C. Greubel_, Jan 13 2018",
				"(MAGMA) /* As triangle */ [[Binomial(n,k)*Factorial(k)/(Factorial(Floor(k/2))*Factorial(Floor((k + 2)/2))): k in [0..n]]: n in [0..10]]; // _G. C. Greubel_, Jan 13 2018"
			],
			"xref": [
				"Row sums are A189912.",
				"Cf. A097610, A057977, A001006."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,5",
			"author": "_Peter Luschny_, May 24 2011",
			"references": 1,
			"revision": 15,
			"time": "2018-01-14T07:20:23-05:00",
			"created": "2011-06-07T21:42:22-04:00"
		}
	]
}