{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A120406",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 120406,
			"data": "1,2,2,5,6,5,14,18,18,14,42,56,60,56,42,132,180,200,200,180,132,429,594,675,700,675,594,429,1430,2002,2310,2450,2450,2310,2002,1430,4862,6864,8008,8624,8820,8624,8008,6864,4862",
			"name": "Triangle read by rows: related to series expansion of the square root of 2 linear factors.",
			"comment": [
				"The numbers T(n,k) arise in the expansion of the square root of 2 generic linear factors: 1 - sqrt((1-a*x)*(1-b*x)) = (a+b)*x/2 + (1/8)*(b-a)^2*x^2*Sum_{n\u003e=0} (Sum_{k=0..n} T(n,k)*a^k*b^(n-k))*(x/4)^n. (The g.f. below simply reformulates this fact.) A combinatorial interpretation of T(n,k) would be very interesting."
			],
			"formula": [
				"T(n,k) = 2*binomial(n,k)^2*binomial(2n+2,n)/binomial(2n+2,2k+1). This shows that T(n,k) is positive and the rows are symmetric. T(n,k) = (k+1)*CatalanNumber(n+1) - 2*Sum_{j=0..k-1} (k-j)*CatalanNumber(j)*CatalanNumber(n-j). This shows that T(n,k) is an integer. Generating function F(x,y):=Sum_{n\u003e=0, k=0..n} T(n,k) x^n y^k is given by F(x,y) = ( 1-2x-2x*y-sqrt(1-4x)*sqrt(1-4x*y) )/( 2x^2*(1-y)^2 ). This shows that the row sums are the powers of 4 (A000302) because lim_{y-\u003e1} F(x,y) = 1/(1-4x).",
				"1 + x*(d/dx)(log(F(x,y))) = 1 + (2 + 2*y)*x + (6 + 4*y + 6*y^2)*x^2 + ... is the o.g.f. for A067804. - _Peter Bala_, Jul 17 2015",
				"G.f. A(x,y) = -G(-x,y), G(x,y) satisfies G(x,y) = x/A008459(G(x,y))^2. - _Vladimir Kruchinin_, Oct 24 2020"
			],
			"example": [
				"Table begins",
				"\\ k..0....1....2....3....4....5....6",
				"n",
				"0 |..1",
				"1 |..2....2",
				"2 |..5....6....5",
				"3 |.14...18...18...14",
				"4 |.42...56...60...56...42",
				"5 |132..180..200..200..180..132",
				"6 |429..594..675..700..675..594..429"
			],
			"mathematica": [
				"Table[2 Binomial[n,k]^2 Binomial[2n+2,n]/ Binomial[2n+2,2k+1],{n,0,9},{k,0,n}]"
			],
			"program": [
				"(Maxima)",
				"solve(A=x*(A^2*y^2-2*A^2*y-2*A*y+A^2-2*A+1),A); /* _Vladimir Kruchinin_, oct 24 2020 */"
			],
			"xref": [
				"Column k=0 is the Catalan numbers A000108 (offset). The middle-of-row entries form A005566. Cf. A067804."
			],
			"keyword": "nonn,tabl",
			"offset": "0,2",
			"author": "_David Callan_, Jul 03 2006",
			"references": 2,
			"revision": 26,
			"time": "2020-10-24T16:59:58-04:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}