{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A204922",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 204922,
			"data": "1,2,1,4,3,2,7,6,5,3,12,11,10,8,5,20,19,18,16,13,8,33,32,31,29,26,21,13,54,53,52,50,47,42,34,21,88,87,86,84,81,76,68,55,34,143,142,141,139,136,131,123,110,89,55,232,231,230,228,225,220,212,199,178",
			"name": "Ordered differences of Fibonacci numbers.",
			"comment": [
				"For a guide to related sequences, see A204892. For numbers not in A204922, see A050939.",
				"From _Emanuele Munarini_, Mar 29 2012: (Start)",
				"Triangle begins:",
				"   1;",
				"   2,  1;",
				"   4,  3,  2;",
				"   7,  6,  5,  3;",
				"  12, 11, 10,  8,  5;",
				"  20, 19, 18, 16, 13,  8;",
				"  33, 32, 31, 29, 26, 21, 13;",
				"  54, 53, 52, 50, 47, 42, 34, 21;",
				"  88, 87, 86, 84, 81, 76, 68, 55, 34;",
				"Diagonal elements = Fibonacci numbers F(n+1) (A000045)",
				"First column = Fibonacci numbers - 1 (A000071);",
				"Second column = Fibonacci numbers - 2 (A001911);",
				"Row sums = n*F(n+3) - F(n+2) + 2 (A014286);",
				"Central coefficients = F(2*n+1) - F(n+1) (A096140).",
				"(End)"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A204922/b204922.txt\"\u003eRows n=1..100 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"From _Emanuele Munarini_, Mar 29 2012: (Start)",
				"T(n,k) = Fibonacci(n+2) - Fibonacci(k+1).",
				"T(n,k) = Sum_{i=k..n} Fibonacci(i+1). (End)"
			],
			"example": [
				"a(1) = s(2) - s(1) = F(3) - F(2) = 2-1 = 1, where F=A000045;",
				"a(2) = s(3) - s(1) = F(4) - F(2) = 3-1 = 2;",
				"a(3) = s(3) - s(2) = F(4) - F(3) = 3-2 = 1;",
				"a(4) = s(4) - s(1) = F(5) - F(2) = 5-1 = 4."
			],
			"mathematica": [
				"(See the program at A204924.)"
			],
			"program": [
				"(Maxima) create_list(fib(n+3)-fib(k+2),n,0,20,k,0,n); /* _Emanuele Munarini_ */",
				"(MAGMA) /* As triangle */ [[Fibonacci(n+2)-Fibonacci(k+1) : k in [1..n]]: n in [1.. 15]]; // _Vincenzo Librandi_, Aug 04 2015",
				"(PARI) {T(n,k) = fibonacci(n+2) - fibonacci(k+1)};",
				"for(n=1,15, for(k=1,n, print1(T(n,k), \", \"))) \\\\ _G. C. Greubel_, Feb 03 2019",
				"(Sage) [[fibonacci(n+2) - fibonacci(k+1) for k in (1..n)] for n in (1..15)] # _G. C. Greubel_, Feb 03 2019"
			],
			"xref": [
				"Cf. A204924, A204892."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Jan 21 2012",
			"references": 32,
			"revision": 33,
			"time": "2019-02-04T03:00:25-05:00",
			"created": "2012-01-21T21:23:33-05:00"
		}
	]
}