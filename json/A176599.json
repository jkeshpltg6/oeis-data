{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A176599",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 176599,
			"data": "1,-1,-1,-5,-7,-1631,-96547,-40291823,-16870575007,-7075000252463,-2969301738826267,-13713149169712887583,-10557203537780702505907",
			"name": "Numerators of the first column of a table with top row the nonnegative integers and successive rows defined by a reverted Akiyama-Tanigawa procedure.",
			"comment": [
				"Define a reverted Akiyama-Tanigawa procedure which takes a sequence s(1), s(2), s(3), ..., as input and constructs the sequence of (s(k)-s(k+1))/k as output. (The difference from the standard algorithm is that the differences are divided through k, not multiplied by k.)",
				"Starting from a top row with nonnegative integers, the following table is constructed row after row by applying the reverted algorithm in succession:",
				"1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17,...",
				"-1, -1/2, -1/3, -1/4, -1/5, -1/6, -1/7, -1/8, -1/9, -1/10, -1/11,...",
				"-1/2, -1/12, -1/36, -1/80, -1/150, -1/252, -1/392, -1/576, -1/810,...",
				"-5/12, -1/36, -11/2160, -7/4800, -17/31500, -5/21168, -23/197568,...",
				"-7/18, -49/4320, -157/129600, -463/2016000, -803/13230000,...",
				"-1631/4320, -1313/259200, -17813/54432000, -35767/846720000,...",
				"-96547/259200, -257917/108864000, -2171917/22861440000,...",
				"The numerators of the left column define the current sequence.",
				"The denominators of the third row are in A011379."
			],
			"link": [
				"Craig A. Tracy, H Widom, \u003ca href=\"http://arxiv.org/abs/1601.04677\"\u003eOn the ground state energy of the delta-function Bose gas\u003c/a\u003e, arXiv preprint arXiv:1601.04677, 2016"
			],
			"formula": [
				"From _Peter Bala_, Aug 14 2012: (Start)",
				"The o.g.f. for the rational numbers in the first column of the above table is sum {n \u003e= 0} x^n/(product {k = 1..n} (x-k)) = 1 - x - 1/2*x^2 - 5/12*x^3 - 7/18*x^4 - .... This yields the formula |a(n)| = numerator of sum {k = 0..n-1} 1/k!*sum {i = 0..k} (-1)^i*binomial(k,i)*(k-i+1)^(k-n). Cf. A024427.",
				"More generally, the o.g.f. for the rational numbers in the r-th column of the above table (excluding the first entry of r) is sum {n \u003e= r} x^(n+1-r)/(product {k = r..n} (x-k)).",
				"(End)",
				"The first column of the above table are the coefficients of the expansion of b(1)x/(1+b(2)x/(1+b(3)x/(1+b(4)x/(...)))), a continued fraction, where b(n) are -1,-1/2,-1/3,-1/4,... i.e. the second row of the table above. - _Benedict W. J. Irwin_, May 10 2016"
			],
			"example": [
				"Column 2: sum {n \u003e= 2} x^(n-1)/(product {k = 2..n} (x-k)) = -1/2*x - 1/12*x^2 - 1/36*x^3 - 49/4320*x^4 - ...",
				"Column 3: sum {n \u003e= 3} x^(n-2)/(product {k = 3..n} (x-k)) = -1/3*x - 1/36*x^2 - 11/2160*x^3 - 157/129600*x^4 - .... - _Peter Bala_, Aug 14 2012"
			],
			"mathematica": [
				"a[1, k_] := k; a[n_, k_] := a[n, k] = (a[n-1, k] - a[n-1, k+1])/k; a[n_] := Numerator[a[n, 1]]; Table[a[n], {n, 1, 13}] (* _Jean-François Alcover_, Aug 02 2012 *)"
			],
			"xref": [
				"Cf. A024427."
			],
			"keyword": "frac,sign",
			"offset": "0,4",
			"author": "_Paul Curtz_, Apr 21 2010",
			"references": 0,
			"revision": 25,
			"time": "2017-04-07T11:29:28-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}