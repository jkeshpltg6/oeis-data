{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A067603",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 67603,
			"data": "2,4,9,72,34,91,62,478,205,2016,522,909,1440,5375,2149,6610,7604,2976,5229,7488,11251,7499,8805,20179,18526,70885,28193,40985,33847,17625,27069,77199,66156,90764,26186,141235,70317,856719,110769,50523,217229",
			"name": "Least k such that gcd(prime(k)+1, prime(k+1)+1) = 2n.",
			"comment": [
				"Since all consecutive primes, 2 \u003c p \u003c q, are odd, therefore gcd(p+1, q+1) must be even."
			],
			"link": [
				"Zak Seidov, Robert G. Wilson v, and Charles R Greathouse IV, \u003ca href=\"/A067603/b067603.txt\"\u003eTable of n, a(n) for n = 1..200\u003c/a\u003e (1..100 terms from Seidov, 101..140 from Wilson, 141..200 from Greathouse)"
			],
			"formula": [
				"Conjecture: a(n) = least k such that A001223(k) = 2n and A000040(k) == -1 (mod 2n). - _Zak Seidov_, Aug 16 2015"
			],
			"example": [
				"a(1) = 2, the first entry in A066940,",
				"a(2) = 4, the first entry in A066941,",
				"a(3) = 9, the first entry in A066942,",
				"a(4) = 72, the first entry in A066943,",
				"a(5) = 34, the first entry in A066944.",
				"That is to say that the first k-th prime that has gcd(prime(k+1)+1, prime(k)+1)) of 2, 4, 6, 8, 10, ..., are k = 2, 4, 9, 72, 34, ..., and the prime_k = 3, 7, 23, 359, 139, 467, 293, ... (A067604).",
				"If the floor of GCD is used, then a(0) equals 1."
			],
			"mathematica": [
				"t = 0*Range@ 70; p = 3; q = 5; While[p \u003c 15*10^6, d = GCD[p + 1, q + 1]/2; If[ t[[d]] == 0, t[[d]] = PrimePi@ p]; p = q; q = NextPrime@ q]; t"
			],
			"program": [
				"(PARI) a(n) = p=2; q=3; k=1; while(gcd(p+1, q+1) != 2*n, k++; p=q; q = nextprime(p+1);); k; \\\\ _Michel Marcus_, Aug 16 2015",
				"(PARI) a(n)=my(p=2,k=2*n,t); forprime(q=3,, t++; if((q-p)%k==0 \u0026\u0026 (p+1)%k==0, return(t)); p=q) \\\\ _Charles R Greathouse IV_, Aug 17 2015",
				"(PARI) a(n)=my(k=2*n); forstep(p=k-1,oo,k, if(isprime(p) \u0026\u0026 (nextprime(p+1)-p)%k==0, return(primepi(p)))) \\\\ _Charles R Greathouse IV_, Aug 17 2015",
				"(MATLAB)",
				"P = primes(10^8);",
				"G = gcd(P(1:end-1)+1,P(2:end)+1);",
				"A = zeros(1,66);",
				"for n = 1:66",
				"    A(n) = find(G == 2*n, 1, 'first');",
				"end",
				"A   % _Robert Israel_, Aug 16 2015"
			],
			"xref": [
				"Main entry is A067604. Cf. A066940, A066941, A066942, A066943, A066944, A000040, A001223."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Robert G. Wilson v_, Jan 31 2002",
			"ext": [
				"Edited by _Robert G. Wilson v_, Aug 17 2015 at the direction of _Zak Seidov_"
			],
			"references": 7,
			"revision": 36,
			"time": "2019-10-18T03:59:51-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}