{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A347171",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 347171,
			"data": "1,1,1,1,2,-1,1,3,-1,1,1,4,0,0,-1,1,5,2,-2,1,1,1,6,5,-4,3,-2,-1,1,7,9,-5,3,-3,3,1,1,8,14,-4,0,0,2,-4,-1,1,9,20,0,-6,6,-4,0,5,1,1,10,27,8,-14,12,-10,8,-3,-6,-1,1,11,35,21,-22,14,-10,10,-11,7,7,1",
			"name": "Triangle read by rows where T(n,k) is the sum of Golay-Rudin-Shapiro terms GRS(j) (A020985) for j in the range 0 \u003c= j \u003c 2^n and having binary weight wt(j) = A000120(j) = k.",
			"comment": [
				"Doche and Mendès France form polynomials P_n(y) = Sum_{j=0..2^n-1} GRS(j) * y^wt(j) and here row n is the coefficients of P_n starting from the constant term, so P_n(y) = Sum_{k=0..n} T(n,k)*y^k.  They conjecture that the number of real roots of P_n is A285869(n).",
				"Row sum n is the sum of GRS terms from j = 0 to 2^n-1 inclusive, which Brillhart and Morton (Beispiel 6 page 129) show is A020986(2^n-1) = 2^ceiling(n/2) = A060546(n).  The same follows by substituting y=1 in the P_n recurrence or the generating function."
			],
			"link": [
				"Kevin Ryde, \u003ca href=\"/A347171/b347171.txt\"\u003eTable of n, a(n) for rows 0 to 100, flattened\u003c/a\u003e",
				"John Brillhart and Patrick Morton, \u003ca href=\"http://projecteuclid.org/euclid.ijm/1256048841\"\u003eÜber Summen von Rudin-Shapiroschen Koeffizienten\u003c/a\u003e, Illinois Journal of Mathematics, volume 22, issue 1, 1978, pages 126-148.",
				"Christophe Doche and Michel Mendès France, \u003ca href=\"http://web.science.mq.edu.au/~doche/finf.ps\"\u003eAn Exercise on the Average Number of Real Zeros of Random Real Polynomials\u003c/a\u003e, Finite and Infinite Combinatorics conference, Budapest, 2001, pages 1-14, see Rudin-Shapiro example page 9."
			],
			"formula": [
				"T(n,k) = T(n-1,k) - T(n-1,k-1) + 2*T(n-2,k-1) for n\u003e=2, and taking T(n,k)=0 if k\u003c0 or k\u003en.",
				"T(n,k) = (-1)^k * A104967(n,n-k).",
				"Row polynomial P_n(y) = (1-y)*P_{n-1}(y) + 2*y*P_{n-2}(y) for n\u003e=2. [Doche and Mendès France]",
				"G.f.: (1 + 2*x*y)/(1 + x*(y-1) - 2*x^2*y).",
				"Column g.f.: C_k(x) = 1/(1-x) for k=0 and C_k(x) = x^k * (2*x-1)^(k-1) / (1-x)^(k+1) for k\u003e=1."
			],
			"example": [
				"Triangle begins",
				"        k=0 k=1 k=2 k=3 k=4 k=5 k=6 k=7",
				"  n=0:   1",
				"  n=1:   1,  1",
				"  n=2:   1,  2, -1",
				"  n=3:   1,  3, -1,  1",
				"  n=4:   1,  4,  0,  0, -1",
				"  n=5:   1,  5,  2, -2,  1,  1",
				"  n=6:   1,  6,  5, -4,  3, -2, -1",
				"  n=7:   1,  7,  9, -5,  3, -3,  3,  1",
				"For T(5,3), those j in the range 0 \u003c= j \u003c 2^5 with wt(j) = 3 are",
				"  j      =  7 11 13 14 19 21 22 25 26 28",
				"  GRS(j) = +1 -1 -1 +1 -1 +1 -1 -1 -1 +1 total -2 = T(5,3)"
			],
			"program": [
				"(PARI) my(M=Mod('x, 'x^2-(1-'y)*'x-2*'y)); row(n) = Vecrev(subst(lift(M^n),'x,'y+1));"
			],
			"xref": [
				"Cf. A020985 (GRS), A020986 (GRS partial sums), A000120 (binary weight), A285869.",
				"Columns k=0..3: A000012, A001477, A000096, A275874.",
				"Cf. A165326 (main diagonal), A248157 (second diagonal negated).",
				"Cf. A060546 (row sums), A104969 (row sums squared terms).",
				"Cf. A329301 (antidiagonal sums).",
				"Cf. A104967 (rows reversed, up to signs)."
			],
			"keyword": "sign,look,tabl",
			"offset": "0,5",
			"author": "_Kevin Ryde_, Aug 21 2021",
			"references": 2,
			"revision": 15,
			"time": "2021-09-06T04:50:43-04:00",
			"created": "2021-08-25T06:55:24-04:00"
		}
	]
}