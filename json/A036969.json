{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A036969",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 36969,
			"data": "1,1,1,1,5,1,1,21,14,1,1,85,147,30,1,1,341,1408,627,55,1,1,1365,13013,11440,2002,91,1,1,5461,118482,196053,61490,5278,140,1,1,21845,1071799,3255330,1733303,251498,12138,204,1,1,87381,9668036,53157079,46587905",
			"name": "Triangle read by rows: T(n,k) = T(n-1,k-1) + k^2*T(n-1,k), 1 \u003c k \u003c= n, T(n,1) = 1.",
			"comment": [
				"Or, triangle central factorial numbers T(2n,2k) (in Riordan's notation).",
				"Can be used to calculate the Bernoulli numbers via the formula B_2n = (1/2)*Sum{k= 1..n, (-1)^(k+1)*(k-1)!*k!*T(n,k)/(2*k+1)}. E.g., n = 1: B_2 = (1/2)*1/3 = 1/6. n = 2: B_4 = (1/2)*(1/3 - 2/5) = -1/30. n = 3: B_6 = (1/2)*(1/3 - 2*5/5 + 2*6/7) = 1/42. - _Philippe Deléham_, Nov 13 2003",
				"From _Peter Bala_, Sep 27 2012: (Start)",
				"Generalized Stirling numbers of the second kind. T(n,k) is equal to the number of partitions of the set {1,1',2,2',...,n,n'} into k disjoint nonempty subsets V1,...,Vk such that, for each 1 \u003c= j \u003c= k, if i is the least integer such that either i or i' belongs to Vj then {i,i'} is a subset of Vj. An example is given below.",
				"Thus T(n,k) may be thought of as a two-colored Stirling number of the second kind. See Matsumoto and Novak, who also give another combinatorial interpretation of these numbers.",
				"(End)"
			],
			"reference": [
				"L. Carlitz, A conjecture concerning Genocchi numbers. Norske Vid. Selsk. Skr. (Trondheim) 1971, no. 9, 4 pp.  [The triangle appears on page 2.]",
				"J. Riordan, Combinatorial Identities, Wiley, 1968, p. 217.",
				"R. P. Stanley, Enumerative Combinatorics, Cambridge, Vol. 2, 1999; see Problem 5.8."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A036969/b036969.txt\"\u003e Rows n = 1..100 of triangle, flattened\u003c/a\u003e",
				"Thomas Browning, \u003ca href=\"https://arxiv.org/abs/2010.13256\"\u003eCounting Parabolic Double Cosets in Symmetric Groups\u003c/a\u003e, arXiv:2010.13256 [math.CO], 2020.",
				"P. L. Butzer, M. Schmidt, E. L. Stark and L. Vogt. \u003ca href=\"http://dx.doi.org/10.1080/01630568908816313\"\u003eCentral factorial numbers; their main properties and some applications\u003c/a\u003e, Num. Funct. Anal. Optim., 10 (1989) 419-488.",
				"M. W. Coffey and M. C. Lettington, \u003ca href=\"http://arxiv.org/abs/1510.05402\"\u003eOn Fibonacci Polynomial Expressions for Sums of mth Powers, their implications for Faulhaber's Formula and some Theorems of Fermat\u003c/a\u003e, arXiv:1510.05402 [math.NT], 2015.",
				"D. Dumont, \u003ca href=\"http://dx.doi.org/10.1215/S0012-7094-74-04134-9\"\u003eInterprétations combinatoires des nombres de Genocchi\u003c/a\u003e, Duke Math. J., 41 (1974), 305-318.",
				"D. Dumont, \u003ca href=\"/A001469/a001469_3.pdf\"\u003eInterprétations combinatoires des nombres de Genocchi\u003c/a\u003e, Duke Math. J., 41 (1974), 305-318. (Annotated scanned copy)",
				"F. G. Garvan, \u003ca href=\"http://qseries.org/fgarvan/papers/hspt.pdf\"\u003eHigher-order spt functions\u003c/a\u003e, Adv. Math. 228 (2011), no. 1, 241-265. - From _N. J. A. Sloane_, Jan 02 2013",
				"P. A. MacMahon, \u003ca href=\"https://doi.org/10.1112/plms/s2-19.1.75\"\u003eDivisors of numbers and their continuations in the theory of partitions\u003c/a\u003e, Proc. London Math. Soc., (2) 19 (1919), 75-113; Coll. Papers II, pp. 303-341.",
				"S. Matsumoto and J. Novak, \u003ca href=\"http://arxiv.org/abs/0905.1992\"\u003eJucys-Murphy Elements and Unitary Matrix Integrals\u003c/a\u003e arXiv.0905.1992 [math.CO], 2009-2012.",
				"B. K. Miceli, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL14/Miceli/miceli4.html\"\u003eTwo q-Analogues of Poly-Stirling Numbers\u003c/a\u003e, J. Integer Seq., 14 (2011), 11.9.6.",
				"John Riordan, \u003ca href=\"/A002720/a002720_2.pdf\"\u003eLetter, Apr 28 1976.\u003c/a\u003e",
				"John Riordan, \u003ca href=\"/A001850/a001850_2.pdf\"\u003eLetter, Jul 06 1978\u003c/a\u003e",
				"Richard P. Stanley, \u003ca href=\"http://www-math.mit.edu/~rstan/transparencies/hooks.pdf\"\u003eHook Lengths and Contents\u003c/a\u003e."
			],
			"formula": [
				"T(n,k) = A156289(n,k)/A001147(k). - _Peter Bala_, Feb 21 2011",
				"O.g.f.: sum {n\u003e=1} x^n*t^n/Product {k = 1..n} (1-k^2*t^2) = x*t + (x+x^2)*t^2 + (x+5*x^2+x^3)*t^3 + .... Define polynomials x^[2*n] = product {k = 0..n-1} (x^2-k^2). This triangle gives the coefficients in the expansion of the monomials x^(2*n) as a linear combination of x^[2*m], 1 \u003c= m \u003c= n. For example, row 4 gives x^8 = x^[2] + 21*x^[4] + 14*x^[6] + x^[8]. A008955 is a signed version of the inverse. n-th row sum = A135920(n). - _Peter Bala_, Oct 14 2011",
				"T(n,k) = (2/(2*k)!)*Sum_{j=0..k-1} (-1)^(j+k+1) * binomial(2*k,j+k+1) * (j+1)^(2*n). This formula is valid for n \u003e= 0 and 0 \u003c= k \u003c= n. - _Peter Luschny_, Feb 03 2012",
				"From _Peter Bala_, Sep 27 2012: (Start)",
				"Let E(x) = cosh(sqrt(2*x)) = sum {n \u003e= 0} x^n/{(2*n)!/2^n}. A generating function for the triangle is E(t*(E(x)-1)) = 1 + t*x + t*(1+t)*x^2/6 + t*(1+5*t+t^2)*x^3/90 + ..., where the sequence of denominators [1,1,6,90,...] is given by (2*n)!/2^n. Cf. A008277 which has generating function exp(t*(exp(x)-1)). An e.g.f. is E(t*(E(x^2/2)-1)) = 1 + t*x^2/2! + t*(1+t)*x^4/4! + t*(1+5*t+t^2)*x^6/6! + ....",
				"Put c(n) := (2*n)!/2^n. Column k generating function is 1/c(k)*(E(x)-1)^k = sum {n = k..inf} T(n,k)*x^n/c(n). Inverse array is A204579.",
				"Production array begins",
				"1...1",
				"0...4...1",
				"0...0...9...1",
				"0...0...0..16...1",
				"...",
				"(End)",
				"x^n = T(n,k)*Product_{i=0..k} (x-i^2), see Stanley link. - _Michel Marcus_, Nov 19 2014"
			],
			"example": [
				"Triangle begins:",
				"  1",
				"  1   1",
				"  1   5   1",
				"  1  21  14   1",
				"  1  85 147  30   1",
				"  ...",
				"T(3,2) = 5: The five set partitions into two sets are {1,1',2,2'}{3,3'}, {1,1',3,3'}{2,2'}, {1,1'}{2,2',3,3'}, {1,1',3}{2,2',3'} and {1,1',3'}{2,2',3}."
			],
			"maple": [
				"A036969 := proc(n,k) local j; 2*add(j^(2*n)*(-1)^(k-j)/((k-j)!*(k+j)!),j=1..k); end;"
			],
			"mathematica": [
				"t[n_, k_] := 2*Sum[j^(2*n)*(-1)^(k-j)/((k-j)!*(k+j)!), {j, 1, k}]; Flatten[ Table[t[n, k], {n, 1, 10}, {k, 1, n}]] (* _Jean-François Alcover_, Oct 11 2011 *)"
			],
			"program": [
				"(PARI) T(n,k)=if(1\u003ck \u0026\u0026 k\u003c=n, T(n-1,k-1) + k^2*T(n-1,k),k==1) \\\\ for illustrative purpose, not efficient ; _M. F. Hasler_, Feb 03 2012",
				"(PARI) T(n,k)=2*sum(j=1,k,(-1)^(k-j)*j^(2*n)/(k-j)!/(k+j)!)  \\\\ _M. F. Hasler_, Feb 03 2012",
				"(Sage)",
				"def A036969(n,k) : return (2/factorial(2*k))*add((-1)^j*binomial(2*k,j)*(k-j)^(2*n) for j in (0..k))",
				"for n in (1..7) : print([A036969(n,k) for k in (1..n)]) # Peter Luschny, Feb 03 2012",
				"(Haskell)",
				"a036969 n k = a036969_tabl !! (n-1) (k-1)",
				"a036969_row n = a036969_tabl !! (n-1)",
				"a036969_tabl = iterate f [1] where",
				"   f row = zipWith (+)",
				"     ([0] ++ row) (zipWith (*) (tail a000290_list) (row ++ [0]))",
				"-- _Reinhard Zumkeller_, Feb 18 2013"
			],
			"xref": [
				"Diagonals are A002450, A002451, A000330 and A060493.",
				"Transpose of A008957. Cf. A008955, A008956, A156289, A135920 (row sums), A204579 (inverse), A000290."
			],
			"keyword": "nonn,easy,nice,tabl",
			"offset": "1,5",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Vladeta Jovovic_, Apr 16 2000"
			],
			"references": 14,
			"revision": 102,
			"time": "2021-12-19T12:39:48-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}