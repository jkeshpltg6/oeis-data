{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A109187",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 109187,
			"data": "1,0,1,2,0,1,0,6,0,1,6,0,12,0,1,0,30,0,20,0,1,20,0,90,0,30,0,1,0,140,0,210,0,42,0,1,70,0,560,0,420,0,56,0,1,0,630,0,1680,0,756,0,72,0,1,252,0,3150,0,4200,0,1260,0,90,0,1,0,2772,0,11550,0,9240,0,1980,0,110,0,1",
			"name": "Triangle read by rows: T(n,k) is number of Grand Motzkin paths of length n having k (1,0)-steps.",
			"comment": [
				"A Grand Motzkin path is a path in the half-plane x \u003e= 0, starting at (0,0), ending at (n,0) and consisting of steps u=(1,1), d=(1,-1) and h=(1,0).",
				"From _Peter Bala_, Feb 11 2017: (Start)",
				"Consider an infinite 1-dimensional integer lattice with an oriented self-loop at each vertex. Then T(n,k) equals the number of walks of length n from a vertex to itself having k loops. There is a bijection between such walks and Grand Motzkin paths which takes a right step and a left step on the lattice to an up step U and a down step D of a Grand Motzkin path respectively, and takes traversing a loop on the lattice to the horizontal step H. See A282252 for the corresponding triangle of walks on a 2-dimensional lattice with self-loops. (End)"
			],
			"link": [
				"Gheorghe Coserea, \u003ca href=\"/A109187/b109187.txt\"\u003eRows n = 0..100, flattened\u003c/a\u003e",
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/2101.10218\"\u003eOn the duals of the Fibonacci and Catalan-Fibonacci polynomials and Motzkin paths\u003c/a\u003e, arXiv:2101.10218 [math.CO], 2021."
			],
			"formula": [
				"G.f.: 1/sqrt((1-tz)^2-4z^2).",
				"Row sums yield the central trinomial coefficients (A002426).",
				"T(2n+1, 0) = 0.",
				"T(2n, 0) = binomial(2n,n) (A000984).",
				"Sum_{k=0..n} k*T(n,k) = A109188(n).",
				"Except for the order, same rows as those of A105868.",
				"Column k has e.g.f. (x^k/k!)*Bessel_I(0,2x). - _Paul Barry_, Mar 11 2006",
				"T(n,k) = binomial((n+k)/2,k)*binomial(n,(n+k)/2)*(1+(-1)^(n-k))/2. - _Paul Barry_, Sep 18 2007",
				"Coefficient array of the polynomials P(n,x) = x^n*hypergeom([1/2-n/2,-n/2], [1], 4/x^2). - _Paul Barry_, Oct 04 2008",
				"G.f.: 1/(1-xy-2x^2/(1-xy-x^2/(1-xy-x^2/(1-xy-x^2/(1-.... (continued fraction). - _Paul Barry_, Jan 28 2009",
				"From _Paul Barry_, Apr 21 2010: (Start)",
				"Exponential Riordan array [Bessel_I(0,2x), x].",
				"Coefficient array of the polynomials P(n,x) = Sum_{k=0..floor(n/2)} C(n,2k)*C(2k, k)*x^(n - 2k).",
				"Diagonal sums are the aerated central Delannoy numbers (A001850 with interpolated zeros). (End)",
				"From _Peter Bala_, Feb 11 2017: (Start)",
				"T(n,k) = binomial(n,k)*binomial(n-k,floor((n-k)/2))*(1 + (-1)^(n-k))/2.",
				"T(n,k) = (n/k) * T(n-1,k-1).",
				"T(n,k) = the coefficient of H^k in the expansion of (H + U + 1/U)^n.",
				"n-th row polynomial R(n,t) = Sum_{k = 0..floor(n/2)} binomial(n,2*k) * binomial(2*k,k) * t^(n-2*k) = coefficient of x^n in the expansion of (1 + t*x + x^2)^n.",
				"R(n,t) = Sum_{k = 0..n} binomial(n,k)*binomial(2*k,k)*(t - 2)^(n-k).",
				"d/dt(R(n,t)) = n*R(n-1,t).",
				"R(n,t) = (1/Pi) * Integral_{x = 0..Pi} (t + 2*cos(x))^n dx.",
				"Moment representation on a finite interval: R(n,t) = 1/Pi * Integral_{x = t-2 .. t+2} x^n/sqrt((t + 2 - x)*(x - t + 2)) dx.",
				"Recurrence: n*R(n,t) = t*(2*n - 1)*R(n-1,t) - (t^2 - 4)*(n - 1)*R(n-2,t) with R(0,t) = 1 and R(1,t) = t.",
				"R(n,t) = A002426 (t = 1), A000984 (t = 2), A026375 (t = 3), A081671 (t = 4), A098409 (t = 5), A098410 (t = 6) and A104454(t = 7).",
				"The zeros of the row polynomials appear to lie on the imaginary axis in the complex plane. Also, the zeros of R(n,t) and R(n+1,t) appear to interlace on the imaginary axis.",
				"The polynomials R(n,1 + t) are the row polynomials of A171128. (End)",
				"From _Peter Luschny_, Jan 23 2018: (Start)",
				"These are the coefficients of the polynomials G(n, -n , -x/2) where G(n, a, x) denotes the n-th Gegenbauer polynomial.",
				"These polynomials can also be expressed as C(n, x) = binomial(2*n,n)*hypergeom([-n, -n], [-n+1/2], 1/2-x/4). (End)"
			],
			"example": [
				"T(3,1)=6 because we have hud,hdu,udh,duh,uhd,dhu, where u=(1,1),d=(1,-1), h=(1,0).",
				"Triangle begins:",
				"n\\k   [0]  [1]   [2]   [3]   [4]   [5]   [6]  [7]  [8]  [9] [10]",
				"[0]    1;",
				"[1]    0,   1;",
				"[2]    2,   0,    1;",
				"[3]    0,   6,    0,    1;",
				"[4]    6,   0,   12,    0,    1;",
				"[5]    0,  30,    0,   20,    0,    1;",
				"[6]   20,   0,   90,    0,   30,    0,    1;",
				"[7]    0, 140,    0,  210,    0,   42,    0,   1;",
				"[8]   70,   0,  560,    0,  420,    0,   56,   0,   1;",
				"[9]    0, 630,    0, 1680,    0,  756,    0,  72,   0,   1;",
				"[10] 252,   0, 3150,    0, 4200,    0, 1260,   0,  90,   0,   1;",
				"[11] ...",
				"From _Peter Bala_, Feb 11 2017: (Start)",
				"The infinitesimal generator begins",
				"      0",
				"      0    0",
				"      2    0     0",
				"      0    6     0     0",
				"     -6    0    12     0     0",
				"      0  -30     0    20     0   0",
				"     80    0   -90     0    30   0   0",
				"      0  560     0  -210     0  42   0  0",
				"  -2310    0  2240     0  -420   0  56  0  0",
				"  ....",
				"and equals the generalized exponential Riordan array [log(Bessel_I(0,2x)),x], and so has integer entries. (End)"
			],
			"maple": [
				"G:=1/sqrt((1-t*z)^2-4*z^2):Gser:=simplify(series(G,z=0,15)): P[0]:=1: for n from 1 to 13 do P[n]:=coeff(Gser,z^n) od: for n from 0 to 13 do seq(coeff(t*P[n],t^k),k=1..n+1) od;",
				"with(PolynomialTools): CL := p -\u003e CoefficientList(simplify(p), x):",
				"C := (n,x) -\u003e binomial(2*n,n)*hypergeom([-n,-n],[-n+1/2],1/2-x/4):",
				"seq(print(CL(C(n,x))), n=0..11); # _Peter Luschny_, Jan 23 2018"
			],
			"mathematica": [
				"p[0] := 1; p[n_] := GegenbauerC[n, -n , -x/2];",
				"Flatten[Table[CoefficientList[p[n], x], {n, 0, 11}]] (* _Peter Luschny_, Jan 23 2018 *)"
			],
			"program": [
				"(PARI)",
				"T(n,k) = if ((n-k)%2, 0, binomial(n,k)*binomial(n-k, (n-k)/2));",
				"concat(vector(12, n, vector(n, k, T(n-1, k-1)))) \\\\ _Gheorghe Coserea_, Sep 06 2018"
			],
			"xref": [
				"Diagonal of rational function R(x, y, t) = 1/(1 - (x^2 + t*x*y + y^2)) with respect to x,y, i.e., T(n,k) = [(xy)^n*t^k] R(x,y,t). For t=0..7 we have the diagonals: A126869(t=0, column 0), A002426(t=1, row sums), A000984(t=2), A026375(t=3), A081671(t=4), A098409(t=5), A098410(t=6), A104454(t=7).",
				"Cf. A089627, A109188, A105868, A171128, A108666, A282252, A298608."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Emeric Deutsch_, Jun 21 2005",
			"references": 8,
			"revision": 80,
			"time": "2021-05-04T19:00:55-04:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}