{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A091787",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 91787,
			"data": "2,2,2,3,2,2,2,3,2,2,2,3,3,2,2,2,3,2,2,2,3,2,2,2,3,3,2,2,2,3,2,2,2,3,2,2,2,3,3,3,3,4,2,2,2,3,2,2,2,3,2,2,2,3,3,2,2,2,3,2,2,2,3,2,2,2,3,3,2,2,2,3,2,2,2,3,2,2,2,3,3,3,3,4,2,2,2,3,2,2,2,3,2,2,2,3,3,2,2",
			"name": "a(1) = 2. To get a(n+1), write the string a(1)a(2)...a(n) as xy^k for words x and y (where y has positive length) and k is maximized, i.e., k = the maximal number of repeating blocks at the end of the sequence so far. Then a(n+1) = max(k,2).",
			"comment": [
				"Here xy^k means the concatenation of the words x and k copies of y.",
				"a(77709404388415370160829246932345692180) = 5 is the first time 5 appears.",
				"This is also the concatenation of the glue strings of A090822, whose respective lengths are given in A091579. - _M. F. Hasler_, Oct 04 2018"
			],
			"reference": [
				"N. J. A. Sloane, Seven Staggering Sequences, in Homage to a Pied Puzzler, E. Pegg Jr., A. H. Schoen and T. Rodgers (editors), A. K. Peters, Wellesley, MA, 2009, pp. 93-110."
			],
			"link": [
				"Giovanni Resta, \u003ca href=\"/A091787/b091787.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"F. J. van de Bult, D. C. Gijswijt, J. P. Linderman, N. J. A. Sloane and Allan Wilks, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL10/Sloane/sloane55.html\"\u003eA Slow-Growing Sequence Defined by an Unusual Recurrence\u003c/a\u003e, J. Integer Sequences, Vol. 10 (2007), #07.1.2.",
				"B. Chaffin, J. P. Linderman, N. J. A. Sloane and Allan Wilks, \u003ca href=\"http://arxiv.org/abs/1212.6102\"\u003eOn Curling Numbers of Integer Sequences\u003c/a\u003e, arXiv:1212.6102 [math.CO], Dec 25 2012.",
				"B. Chaffin, J. P. Linderman, N. J. A. Sloane and Allan Wilks, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL16/Sloane/sloane3.html\"\u003eOn Curling Numbers of Integer Sequences\u003c/a\u003e, Journal of Integer Sequences, Vol. 16 (2013), Article 13.4.3.",
				"N. J. A. Sloane, \u003ca href=\"http://neilsloane.com/doc/g4g7.pdf\"\u003eSeven Staggering Sequences\u003c/a\u003e.",
				"\u003ca href=\"/index/Ge#Gijswijt\"\u003eIndex entries for sequences related to Gijswijt's sequence\u003c/a\u003e"
			],
			"example": [
				"To get a(2): a(1) = 2 = (2)^1, so k = 1, a(2) = 2.",
				"To get a(3): a(1)a(2) = 22 = (2)^2, so a(3) = k = 2.",
				"To get a(4): a(1)a(2)a(3) = 222 = (2)^3, so a(3) = k = 3."
			],
			"program": [
				"(PARI) A091787(n, A=[])={while(#A\u003cn, my(k=2, L=0, m=k); while((k+1)*(L+1)\u003c=#A, for(N=L+1, #A/(m+1), A[-m*N..-1]==A[-(m+1)*N..-N-1]\u0026\u0026(m+=1)\u0026\u0026break); m\u003ek||break; k=m); A=concat(A, k)); A} \\\\ _M. F. Hasler_, Oct 04 2018"
			],
			"xref": [
				"Cf. A090822, A091799."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_, Mar 07 2004",
			"references": 21,
			"revision": 31,
			"time": "2018-10-13T12:30:56-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}