{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A257921",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 257921,
			"data": "1,-2,2,-2,0,0,1,-2,4,0,0,0,0,-4,2,-2,0,0,3,-2,2,-2,0,0,2,-2,2,0,0,0,0,-2,2,-2,0,0,3,-2,4,-2,0,0,0,-6,2,0,0,0,0,-2,4,0,0,0,2,-2,2,-4,0,0,1,0,2,0,0,0,0,-2,6,-2,0,0,2,-4,0,-2,0,0,4,-4",
			"name": "Expansion of f(x^2, -x^4) * f(-x, -x^5)^2 / f(-x^12, -x^12) in powers of x where f(, ) is Ramanujan's general theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A257921/b257921.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of psi(-x)^2 * psi(x^3)^2 / (psi(-x^2) * phi(-x^12)) in powers of x where phi(), psi() are Ramanujan theta functions.",
				"Expansion of q^(-3/4) * eta(q)^2 * eta(q^4)^3 * eta(q^6)^4 * eta(q^24) / (eta(q^2)^3 * eta(q^3)^2 *eta(q^8) * eta(q^12)^2) in powers of q.",
				"Euler transform of period 24 sequence [ -2, 1, 0, -2, -2, -1, -2, -1, 0, 1, -2, -2, -2, 1, 0, -1, -2, -1, -2, -2, 0, 1, -2, -2, ...].",
				"a(n) = b(4*n + 3) where b() is multiplicative with b(2^e) = 0^e, b(3^e) = 1, b(p^e) = e+1 if p == 1, 11 (mod 24), b(p^e) = (e+1) * (-1)^e if p == 5, 7 (mod 24), b(p^e) = (1 + (-1)^e) / 2 if p == 13, 17, 19, 23 (mod 24).",
				"a(n) = (-1)^n * A261119(n) = A134177(2*n + 1) = - A128580(2*n + 1) = - A115660(4*n+3).",
				"a(6*n + 4) = a(6*n + 5) = 0.",
				"a(2*n) = A257920(n). a(2*n + 1) = -2 * A259896(n). a(3*n) = A259668(n). a(6*n + 2) = 2 * A128591(n)."
			],
			"example": [
				"G.f. = 1 - 2*x + 2*x^2 - 2*x^3 + x^6 - 2*x^7 + 4*x^8 - 4*x^13 + 2*x^14 + ...",
				"G.f. = q^3 - 2*q^7 + 2*q^11 - 2*q^15 + q^27 - 2*q^31 + 4*q^35 - 4*q^55 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 0, 0, With[ {m = 4 n + 3}, DivisorSum[ m, KroneckerSymbol[ 12, #] KroneckerSymbol[ -2, m/#] \u0026]]];",
				"a[ n_] := SeriesCoefficient[ (EllipticTheta[ 2, Pi/4, x^(1/2)] EllipticTheta[ 2, 0, x^(3/2)])^2 / (2^(5/2) x^(3/4) EllipticTheta[ 2, Pi/4, x] EllipticTheta[ 4, 0, x^12]), {x, 0, n};"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, n = 4*n + 3; sumdiv(n, d, kronecker( 12, d) * kronecker( -2, n/d)))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A)^2 * eta(x^4 + A)^3 * eta(x^6 + A)^4 * eta(x^24 + A) / (eta(x^2 + A)^3 * eta(x^3 + A)^2 *eta(x^8 + A) * eta(x^12 + A)^2), n))};"
			],
			"xref": [
				"Cf. A115660, A128580, A128591, A134177, A257920, A259668, A259896, A261119."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Jul 12 2015",
			"references": 4,
			"revision": 16,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2015-07-12T21:53:31-04:00"
		}
	]
}