{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007956",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7956,
			"data": "1,1,1,2,1,6,1,8,3,10,1,144,1,14,15,64,1,324,1,400,21,22,1,13824,5,26,27,784,1,27000,1,1024,33,34,35,279936,1,38,39,64000,1,74088,1,1936,2025,46,1,5308416,7,2500,51,2704,1,157464,55,175616,57,58,1,777600000,1,62,3969,32768,65",
			"name": "Product of proper divisors of n.",
			"comment": [
				"From _Bernard Schott_, Feb 01 2019: (Start)",
				"a(n) = 1 iff n = 1 or n is prime.",
				"a(n) = n when n \u003e 1 iff n has exactly four divisors, equally, iff n is either the cube of a prime or the product of two different primes, so iff n belongs to A030513 (very nice proof in Sierpiński).",
				"a(p^3) = 1 * p * p^2 = p^3; a(p*q) = 1 * p * q = p*q.",
				"As a(1) = 1, {1} Union A030513 = A007422, fixed points of this sequence. (End)"
			],
			"reference": [
				"Wacław Sierpiński, Elementary Theory of Numbers, Ex. 2 p. 174, Warsaw, 1964."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A007956/b007956.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (terms 1..1000 from T. D. Noe)"
			],
			"formula": [
				"a(n) = A007955(n)/n = n^(A000005(n)/2-1) = sqrt(n^(number of factors of n other than 1 and n)).",
				"a(n) = Product_{k=1..A000005(n)-1} A027751(n,k). - _Reinhard Zumkeller_, Feb 04 2013",
				"a(n) = A240694(n, A000005(n)-1) for n \u003e 1. - _Reinhard Zumkeller_, Apr 10 2014"
			],
			"example": [
				"a(18) = 1 * 2 * 3 * 6 * 9 = 324. - _Bernard Schott_, Jan 31 2019"
			],
			"maple": [
				"A007956 := n -\u003e mul(i,i=op(numtheory[divisors](n) minus {1,n}));",
				"seq(A007956(i), i=1..79); # _Peter Luschny_, Mar 22 2011"
			],
			"mathematica": [
				"Table[Times@@Most[Divisors[n]], {n, 65}] (* _Alonso del Arte_, Apr 18 2011 *)",
				"a[n_] := n^(DivisorSigma[0, n]/2 - 1); Table[a[n], {n, 1, 65}] (* _Jean-François Alcover_, Oct 07 2013 *)"
			],
			"program": [
				"(PARI) A007956(n) = local(a);a=1;fordiv(n,d,a=a*d);a/n \\\\ _Michael B. Porter_, Dec 01 2009",
				"(PARI) a(n)=my(k); if(issquare(n, \u0026k), k^(numdiv(n)-2), n^(numdiv(n)/2-1)) \\\\ _Charles R Greathouse IV_, Oct 15 2015",
				"(Haskell)",
				"a007956 = product . a027751_row",
				"-- _Reinhard Zumkeller_, Feb 04 2013, Nov 02 2011"
			],
			"xref": [
				"Cf. A000005, A007955, A048671, A182936, A027751, A066729.",
				"Cf. A007422 (fixed points). A030513 (subsequence).",
				"Cf. A001065 (sums of proper divisors)."
			],
			"keyword": "nonn,easy,nice",
			"offset": "1,4",
			"author": "R. Muller",
			"ext": [
				"More terms from Scott Lindhurst (ScottL(AT)alumni.princeton.edu)"
			],
			"references": 37,
			"revision": 81,
			"time": "2019-12-23T06:02:40-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}