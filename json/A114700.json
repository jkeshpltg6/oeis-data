{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A114700",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 114700,
			"data": "1,1,1,1,0,1,1,-1,1,1,1,0,0,0,1,1,-1,0,0,1,1,1,0,1,0,-1,0,1,1,-1,-1,-1,1,1,1,1,1,0,2,2,0,-2,-2,0,1,1,-1,-2,-4,-2,2,4,2,1,1,1,0,3,6,6,0,-6,-6,-3,0,1,1,-1,-3,-9,-12,-6,6,12,9,3,1,1,1,0,4,12,21,18,0,-18,-21,-12,-4,0,1",
			"name": "Triangle T, read by rows, such that the m-th matrix power satisfies T^m = I + m*(T - I), where T(n,k) = [T^-1](n-1,k) + [T^-1](n-1,k-1) for n\u003ek\u003e0, with T(n,0)=T(n,n)=1 for n\u003e=0 and I is the identity matrix.",
			"comment": [
				"The rows of this triangle are symmetric up to sign. Row sums = 2 after row 0. Unsigned row sums = A116466. Row squared sums = A116467. Central terms of odd rows: T(2*n+1,n+1) = |A064310(n)|."
			],
			"formula": [
				"G.f.: A(x,y) = 1/(1-x*y)+ x*(1+x-2*x^2*y)/(1-x)/(1+x+x*y)/(1-x*y). G.f. of matrix power T^m: 1/(1-x*y)+ m*x*(1+x-2*x^2*y)/(1-x)/(1+x+x*y)/(1-x*y)."
			],
			"example": [
				"Matrix inverse is: T^-1 = 2*I - T.",
				"Matrix log is: log(T) = T - I.",
				"Triangle T begins:",
				"1;",
				"1, 1;",
				"1, 0, 1;",
				"1,-1, 1, 1;",
				"1, 0, 0, 0, 1;",
				"1,-1, 0, 0, 1, 1;",
				"1, 0, 1, 0,-1, 0, 1;",
				"1,-1,-1,-1, 1, 1, 1, 1;",
				"1, 0, 2, 2, 0,-2,-2, 0, 1;",
				"1,-1,-2,-4,-2, 2, 4, 2, 1, 1;",
				"1, 0, 3, 6, 6, 0,-6,-6,-3, 0, 1;",
				"1,-1,-3,-9,-12,-6, 6, 12, 9, 3, 1, 1;",
				"1, 0, 4, 12, 21, 18, 0,-18,-21,-12,-4, 0, 1; ...",
				"The g.f. of column k, C_k(x), obeys the recurrence:",
				"C_k = C_{k-1} + (-1)^k*x*(1+2*x)/(1-x)/(1+x)^k with C_0 = 1/(1-x);",
				"so that column g.f.s continue as:",
				"C_1 = C_0 - x*(1+2*x)/(1-x)/(1+x),",
				"C_2 = C_1 + x*(1+2*x)/(1-x)/(1+x)^2,",
				"C_3 = C_2 - x*(1+2*x)/(1-x)/(1+x)^3, ..."
			],
			"program": [
				"(PARI) T(n,k)=local(x=X+X*O(X^n),y=Y+Y*O(Y^k));polcoeff(polcoeff( 1/(1-x*y)+ x*(1+x-2*x^2*y)/(1-x)/(1+x+x*y)/(1-x*y),n,X),k,Y)",
				"(PARI) T(n,k)=local(M=matrix(n+1,n+1));for(r=1,n+1,for(c=1,r, M[r,c]=if(r==c,1,if(c==1,1,if(c\u003e1, (2*M^0-M)[r-1,c-1])+(2*M^0-M)[r-1,c]))));return(M[n+1,k+1])"
			],
			"xref": [
				"Cf. A116466 (unsigned row sums), A116467 (row squared sums), A064310 (central terms); A112555 (variant)."
			],
			"keyword": "sign,tabl",
			"offset": "0,39",
			"author": "_Paul D. Hanna_, Feb 19 2006",
			"references": 2,
			"revision": 7,
			"time": "2015-06-14T00:32:43-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}