{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A004730",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 4730,
			"data": "1,1,2,3,8,5,16,35,128,63,256,231,1024,429,2048,6435,32768,12155,65536,46189,262144,88179,524288,676039,4194304,1300075,8388608,5014575,33554432,9694845,67108864,300540195,2147483648,583401555,4294967296,2268783825",
			"name": "Numerator of n!!/(n+1)!! (cf. A006882).",
			"link": [
				"T. D. Noe, \u003ca href=\"/A004730/b004730.txt\"\u003eTable of n, a(n) for n=0..300\u003c/a\u003e",
				"Joseph E. Cooper III, \u003ca href=\"http://arxiv.org/abs/1510.00399\"\u003eA recurrence for an expression involving double factorials\u003c/a\u003e, arXiv:1510.00399 [math.CO], 2015.",
				"Svante Janson, \u003ca href=\"http://www2.math.uu.se/~svante/papers/sj114.pdf\"\u003eOn the traveling fly problem\u003c/a\u003e, Graph Theory Notes of New York Vol. XXXI, 17, 1996."
			],
			"formula": [
				"Let y(m) = y(m-2) + 1/y(m-1) for m \u003e= 2, with y(0)=y(1)=1. Then the denominator of y(n+1) equals the numerator of n!!/(n+1)!! for n \u003e= 0, where the double factorials are given by A006882. [_Reinhard Zumkeller_, Dec 08 2011, as corrected in Cooper (2015)]"
			],
			"mathematica": [
				"Numerator[#[[1]]/#[[2]]\u0026/@Partition[Range[0,40]!!,2,1]] (* _Harvey P. Dale_, Jan 22 2013 *)",
				"Numerator[CoefficientList[Series[(1 - Sqrt[1 - c^2] + ArcSin[c])/(c Sqrt[1 - c^2]),{c, 0, 39}], c]] (* _Eugene d'Eon_, Nov 01 2018 *)"
			],
			"program": [
				"(Haskell)",
				"import Data.Ratio ((%), denominator)",
				"a004730 n = a004730_list !! n",
				"a004730_list = map denominator ggs where",
				"   ggs = 1 : 2 : zipWith (+) ggs (map (1 /) $ tail ggs) :: [Rational]",
				"-- _Reinhard Zumkeller_, Dec 08 2011",
				"(MAGMA) DoubleFactorial:=func\u003c n | \u0026*[n..2 by -2] \u003e; [ Numerator(DoubleFactorial(n) / DoubleFactorial(n+1)): n in [0..35]]; // _Vincenzo Librandi_, Dec 03 2018",
				"(Python)",
				"from sympy import gcd, factorial2",
				"def A004730(n):",
				"    a, b = factorial2(n), factorial2(n+1)",
				"    return a//gcd(a,b) # _Chai Wah Wu_, Apr 03 2021"
			],
			"xref": [
				"Cf. A004731 (denominator), A006882 (double factorials)."
			],
			"keyword": "nonn,frac",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"references": 7,
			"revision": 46,
			"time": "2021-04-03T15:36:20-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}