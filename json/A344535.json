{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A344535",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 344535,
			"data": "1,2,3,6,4,8,12,24,5,10,15,30,20,40,60,120,9,18,27,54,36,72,108,216,45,90,135,270,180,360,540,1080,16,32,48,96,64,128,192,384,80,160,240,480,320,640,960,1920,144,288,432,864,576,1152,1728,3456,720,1440",
			"name": "For any number n with binary expansion Sum_{k = 1..m} 2^e_k (where 0 \u003c= e_1 \u003c ... \u003c e_m), a(n) = Product_{k = 1..m} prime(1+A025581(e_k))^2^A002262(e_k) (where prime(k) denotes the k-th prime number).",
			"comment": [
				"The ones in the binary expansion of n encode the Fermi-Dirac factors of a(n).",
				"The following table gives the rank of the bit corresponding to the Fermi-Dirac factor p^2^k:",
				"    ...",
				"      7| 6",
				"      5| 3 7",
				"      3| 1 4 8",
				"      2| 0 2 5 9",
				"    ---+--------",
				"    p/k| 0 1 2 3 ...",
				"This sequence is a bijection from the nonnegative integers to the positive integers with inverse A344537.",
				"This sequence establishes a bijection from A261195 to A225547."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A344535/b344535.txt\"\u003eTable of n, a(n) for n = 0..8192\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A344534(A344531(n)).",
				"a(n) = A344534(n) iff n belongs to A261195.",
				"A064547(a(n)) = A000120(n).",
				"a(A006125(n)) = prime(n) for any n \u003e 0.",
				"a(A036442(n+1)) = 2^2^n for any n \u003e= 0.",
				"a(m + n) = a(m) * a(n) when m AND n = 0 (where AND denotes the bitwise AND operator)."
			],
			"example": [
				"For n = 42:",
				"- 42 = 2^5 + 2^3 + 2^1,",
				"- so we have the following Fermi-Dirac factors p^2^k:",
				"      5| X",
				"      3| X",
				"      2|     X",
				"    ---+------",
				"    p/k| 0 1 2",
				"- a(42) = 3^2^0 * 5^2^0 * 2^2^2 = 240."
			],
			"program": [
				"(PARI) A002262(n)=n-binomial(round(sqrt(2+2*n)), 2)",
				"A025581(n)=binomial(1+floor(1/2+sqrt(2+2*n)), 2)-(n+1)",
				"a(n) = { my (v=1, e); while (n, n-=2^e=valuation(n, 2); v *= prime(1 + A025581(e))^2^A002262(e)); v }"
			],
			"xref": [
				"Cf. A000120, A002262, A006125, A025581, A036442, A052330, A064547, A225547, A261195, A344531, A344534, A344537."
			],
			"keyword": "nonn,base",
			"offset": "0,2",
			"author": "_Rémy Sigrist_, May 22 2021",
			"references": 3,
			"revision": 14,
			"time": "2021-05-24T10:14:00-04:00",
			"created": "2021-05-24T08:41:06-04:00"
		}
	]
}