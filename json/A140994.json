{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A140994",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 140994,
			"data": "1,1,1,1,2,1,1,2,4,1,1,2,4,8,1,1,2,4,9,15,1,1,2,4,9,19,28,1,1,2,4,9,19,40,52,1,1,2,4,9,19,41,83,96,1,1,2,4,9,19,41,88,170,177,1,1,2,4,9,19,41,88,188,345,326,1,1,2,4,9,19,41,88,189,400,694,600,1,1,2,4,9,19,41,88,189,406,846,1386,1104,1",
			"name": "Triangle G(n, k), for 0 \u003c= k \u003c= n, read by rows, where G(n, n) = G(n+1, 0) = 1, G(n+2, 1) = 2, G(n+3, 2) = 4, G(n+4, m) = G(n+1, m-2) + G(n+1, m-3) + G(n+2, m-2) + G(n+3, m-1) for n \u003e= 0 and m = 3..(n+3).",
			"comment": [
				"From _Petros Hadjicostas_, Jun 12 2019: (Start)",
				"This is a mirror of image of triangular array A140997. The current array has index of asymmetry s = 2 and index of obliqueness (obliquity) e = 1. Array A140997 has the same index of asymmetry, but has index of obliqueness e = 0. (In other related sequences, the author uses the letter y for the index of asymmetry and the letter z for the index of obliqueness, but the stone slab that appears over a tomb in a picture that he posted in those sequences, the letters s and e are used instead. See, for example, the documentation for sequences A140998, A141065, A141066, and A141067.)",
				"In general, if the index of asymmetry (from the Pascal triangle A007318) is s, then the order of the recurrence is s + 2 (because the recurrence of the Pascal triangle has order 2). There are also s + 2 infinite sets of initial conditions (as opposed to the Pascal triangle that has only 2 infinite sets of initial conditions, namely, G(n, 0) = G(n+1, n+1) = 1 for n \u003e= 0).",
				"Pascal's triangle A007318 has s = 0 and is symmetric, arrays A140998 and A140993 have s = 1 (with e = 0 and e = 1, respectively), and arrays A140996 and A140995 have s = 3 (with e = 0 and e = 1, respectively).",
				"If A(x,y) = Sum_{n,k \u003e= 0} G(n, k)*x^n*y^k is the bivariate g.f. for this array (with G(n, k) = 0 for 0 \u003c= n \u003c k) and B(x, y) = Sum_{n, k} A140997(n, k)*x^n*y^k, then A(x, y) = B(x*y, y^(-1)). This can be proved using formal manipulation of double series expansions and the fact G(n, k) = A140997(n, n-k) for 0 \u003c= k \u003c= n.",
				"If we let b(k) = lim_{n -\u003e infinity} G(n, k) for k \u003e= 0, then b(0) = 1, b(1) = 2, b(2) = 4, and b(k) = b(k-1) + 2*b(k-2) + b(k-3) for k \u003e= 3. (The existence of the limit can be proved by induction on k.) It follows that b(k) = A141015(k) for k \u003e= 0.",
				"(End)"
			],
			"link": [
				"Robert Price, \u003ca href=\"/A140994/b140994.txt\"\u003eTable of n, a(n) for n = 0..1325\u003c/a\u003e",
				"Juri-Stepan Gerasimov, \u003ca href=\"/A140998/a140998.jpg\"\u003eStepan's triangles and Pascal's triangle are connected by the recurrence relation ...\u003c/a\u003e"
			],
			"formula": [
				"From _Petros Hadjicostas_, Jun 12 2019: (Start)",
				"G(n, k) = A140997(n, n-k) for 0 \u003c= k \u003c= n.",
				"Bivariate g.f.: Sum_{n,k \u003e= 0} G(n, k)*x^n*y^k = (x^4*y^3 - x^3*y^3 - x^2*y^2 + x^2*y - x*y + 1)/((1- x*y)*(1 - x)*(1- x*y - x^2*y^2 - x^3*y^3 - x^3*y^2)).",
				"(End)"
			],
			"example": [
				"Triangle begins:",
				"  1",
				"  1 1",
				"  1 2 1",
				"  1 2 4 1",
				"  1 2 4 8 1",
				"  1 2 4 9 15  1",
				"  1 2 4 9 19 28  1",
				"  1 2 4 9 19 40 52   1",
				"  1 2 4 9 19 41 83  96   1",
				"  1 2 4 9 19 41 88 170 177    1",
				"  1 2 4 9 19 41 88 188 345  326    1",
				"  1 2 4 9 19 41 88 189 400  694  600    1",
				"  1 2 4 9 19 41 88 189 406  846 1386 1104 1",
				"... [corrected by _Petros Hadjicostas_, Jun 12 2019]",
				"E.g., G(12, 9) = G(9, 7) + G(9, 6) + G(10, 7) + G(11, 8) = 170 + 88 + 188 + 400 = 846."
			],
			"maple": [
				"G := proc(n,k) if k=0 or n =k then 1; elif k= 1 then 2 ; elif k =2 then 4; elif k \u003e n or k \u003c 0 then 0 ; else procname(n-3,k-2)+procname(n-3,k-3)+procname(n-2,k-2)+procname(n-1,k-1) ; end if; end proc: seq(seq(G(n,k),k=0..n),n=0..15) ; # _R. J. Mathar_, Apr 14 2010"
			],
			"mathematica": [
				"nlim = 50;",
				"Do[G[n, 0] = 1, {n, 0, nlim}];",
				"Do[G[n, n] = 1, {n, 1, nlim}];",
				"Do[G[n + 2, 1] = 2, {n, 0, nlim}];",
				"Do[G[n + 3, 2] = 4, {n, 0, nlim}];",
				"Do[G[n + 4, m] =",
				"   G[n + 1, m - 2] + G[n + 1, m - 3] + G[n + 2, m - 2] +",
				"    G[n + 3, m - 1], {n, 0, nlim}, {m, 3, n + 3}];",
				"A140994 = {}; For[n = 0, n \u003c= nlim, n++,",
				" For[k = 0, k \u003c= n, k++, AppendTo[A140994, G[n, k]]]];",
				"A140994 (* _Robert Price_, Aug 19 2019 *)"
			],
			"xref": [
				"Cf. A007318, A008937, A140993, A140995, A140996, A140997, A140998, A141015, A141018, A141020, A141021, A141031, A141065, A141066, A141067."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Juri-Stepan Gerasimov_, Jul 08 2008",
			"ext": [
				"Entries checked by _R. J. Mathar_, Apr 14 2010"
			],
			"references": 24,
			"revision": 47,
			"time": "2019-08-20T05:17:02-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}