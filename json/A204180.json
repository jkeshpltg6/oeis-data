{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A204180",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 204180,
			"data": "1,-1,1,-3,1,1,-9,6,-1,-2,-32,32,-10,1,-34,-132,183,-81,15,-1,-324,-604,1159,-655,170,-21,1,-2988,-2860,8137,-5589,1825,-316,28,-1,-28944,-11864,62852,-51184,19894,-4326,539,-36,1,-300816,-8568",
			"name": "Array:  row n shows the coefficients of the characteristic polynomial of the n-th principal submatrix of (f(i,j)), where f(i,1)=f(1,j)=1, f(i,i)= i; f(i,j)=0 otherwise; as in A204179.",
			"comment": [
				"Let p(n)=p(n,x) be the characteristic polynomial of the n-th principal submatrix. The zeros of p(n) are real, and they interlace the zeros of p(n+1). See A202605 and A204016 for guides to related sequences."
			],
			"reference": [
				"(For references regarding interlacing roots, see A202605.)"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A204180/b204180.txt\"\u003eTable of n, a(n) for n = 1..10010\u003c/a\u003e (rows 1 to 140, flattened)"
			],
			"formula": [
				"From _Robert Israel_, Jun 26 2018: (Start)",
				"p(n,x) = (1 - Sum_{k=2..n} 1/((1-x)*(k-x)))*Product_{k=1..n) (k - x).",
				"p(n+1,x) = (n+1-x)*p(n,x) - Gamma(n+1-x)/Gamma(2-x). (End)"
			],
			"example": [
				"Top of the array:",
				"   1,  -1;",
				"   1,  -3,   1;",
				"   1,  -9,   6,  -1;",
				"  -2, -32,  32, -10,   1;"
			],
			"maple": [
				"f:= proc(n) local P;",
				"  P:= normal(mul(i-lambda,i=1..n)*(1 - add(1/(lambda-1)/(lambda-i),i=2..n)));",
				"  seq(coeff(P,lambda,i),i=0..n);",
				"end proc:",
				"seq(f(n),n=1..20); # _Robert Israel_, Jun 26 2018"
			],
			"mathematica": [
				"f[i_, j_] := 0; f[1, j_] := 1; f[i_, 1] := 1; f[i_, i_] := i;",
				"m[n_] := Table[f[i, j], {i, 1, n}, {j, 1, n}]",
				"TableForm[m[8]] (* 8x8 principal submatrix *)",
				"Flatten[Table[f[i, n + 1 - i],",
				"  {n, 1, 15}, {i, 1, n}]]  (* A204179 *)",
				"p[n_] := CharacteristicPolynomial[m[n], x];",
				"c[n_] := CoefficientList[p[n], x]",
				"TableForm[Flatten[Table[p[n], {n, 1, 10}]]]",
				"Table[c[n], {n, 1, 12}]",
				"Flatten[%]                 (* A204180 *)",
				"TableForm[Table[c[n], {n, 1, 10}]]"
			],
			"xref": [
				"Cf. A204179, A202605, A204016."
			],
			"keyword": "tabf,sign",
			"offset": "1,4",
			"author": "_Clark Kimberling_, Jan 12 2012",
			"references": 2,
			"revision": 15,
			"time": "2018-06-27T07:10:53-04:00",
			"created": "2012-01-12T20:16:24-05:00"
		}
	]
}