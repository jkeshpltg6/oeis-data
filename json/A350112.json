{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A350112",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 350112,
			"data": "1,1,0,1,0,0,1,0,0,0,1,0,0,0,0,1,1,1,1,1,1,1,2,3,4,5,2,0,1,3,6,10,9,4,0,0,1,4,10,16,16,8,0,0,0,1,5,14,25,28,16,0,0,0,0,1,6,19,38,48,32,16,8,4,2,1,1,7,25,56,80,80,60,40,25,15,3,0,1,8,32,80,136,166,157,128,95,40,9,0,0",
			"name": "Triangle read by rows: T(n,k) is the number of tilings of an (n+k)-board using k (1,4)-fences and n-k squares.",
			"comment": [
				"This is the m=5 member in the sequence of triangles A007318, A059259, A350110, A350111, A350112 which give the number of tilings of an (n+k) X 1 board using k (1,m-1)-fences and n-k unit square tiles. A (1,g)-fence is composed of two unit square tiles separated by a gap of width g.",
				"T(5*j+r-k,k) is the coefficient of x^k in (f(j,x))^(5-r)*(f(j+1,x))^r for r=0,1,2,3,4 where f(n,x) is one form of a Fibonacci polynomial defined by f(n+1,x)=f(n,x)+x*f(n-1,x) where f(0,x)=1 and f(n\u003c0,x)=0.",
				"T(n+5-k,k) is the number of subsets of {1,2,...,n} of size k such that no two elements in a subset differ by 5.",
				"Sum of (5j+r)-th antidiagonal (counting initial 1 as the 0th) is f(j)^(5-r)*f(j+1)^r where j=0,1,..., r=0,1,2,3,4, and f(n) is the Fibonacci number A000045(n+1)."
			],
			"formula": [
				"T(n,0) = 1.",
				"T(n,n) = n mod 5.",
				"T(n,1) = n-4 for n\u003e3.",
				"T(5*j-r,5*j-p) = 0 for j\u003e0, p=0,1,2,3,4, and r=1,...,p.",
				"T(5*(j-1)+p,5*(j-1)) = T(5*j,5*j-p) = j^p for j\u003e0 and p=0,1,...,5.",
				"T(5*j+1,5*j-1) = 5*j(j+1)/2 for j\u003e0.",
				"T(5*j+2,5*j-2) = 5*C(j+2,4) + 10*C(j+1,2)^2 for j\u003e1.",
				"T(n,k) = T(n-1,k) + T(n-1,k-1) for n \u003e= 4*k+1 if k \u003e= 0 (conjectured)."
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  1,   0;",
				"  1,   0,   0;",
				"  1,   0,   0,   0;",
				"  1,   0,   0,   0,   0;",
				"  1,   1,   1,   1,   1,   1;",
				"  1,   2,   3,   4,   5,   2,   0;",
				"  1,   3,   6,  10,   9,   4,   0,   0;",
				"  1,   4,  10,  16,  16,   8,   0,   0,   0;",
				"  1,   5,  14,  25,  28,  16,   0,   0,   0,   0;",
				"  1,   6,  19,  38,  48,  32,  16,   8,   4,   2,   1;",
				"  1,   7,  25,  56,  80,  80,  60,  40,  25,  15,   3,   0;",
				"  1,   8,  32,  80, 136, 166, 157, 128,  95,  40,   9,   0,   0;",
				"  1,   9,  40, 112, 217, 309, 346, 330, 223, 105,  27,   0,   0,   0;"
			],
			"mathematica": [
				"f[n_]:=If[n\u003c0,0,f[n-1]+x*f[n-2]+KroneckerDelta[n,0]];",
				"T[n_, k_]:=Module[{j=Floor[(n+k)/5], r=Mod[n+k,5]},",
				"  Coefficient[f[j]^(5-r)*f[j+1]^r,x,k]];",
				"Flatten@Table[T[n,k], {n, 0, 13}, {k, 0, n}]"
			],
			"xref": [
				"Other members of sequence of triangles: A007318, A059259, A350110, A350111.",
				"Other triangles related to tiling using fences: A123521, A157897, A335964."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,23",
			"author": "_Michael A. Allen_, Dec 22 2021",
			"references": 3,
			"revision": 6,
			"time": "2021-12-26T14:20:13-05:00",
			"created": "2021-12-26T14:20:13-05:00"
		}
	]
}