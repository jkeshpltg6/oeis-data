{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A323608",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 323608,
			"data": "1,1,1,2,2,3,3,5,4,6,5,8,6,9,7,12,8,12,9,15,10,15,11,19,12,18,13,22,14,21,15,27,16,24,17,29,18,27,19,34,20,30,21,36,22,33,23,42,24,36,25,43,26,39,27,49,28,42,29,50,30,45,31,58,32,48,33,57,34,51,35,64,36,54,37,64,38,57,39,73",
			"name": "The position function the fractalization of which yields A323607.",
			"comment": [
				"For a definition of the fractalization process, see comments in A194959. The sequence A323607, triangular array where row n is the list of the numbers from 1 to n sorted in Sharkovsky order, is clearly the result of a fractalization. Let {a(n)} (this sequence) be its position function."
			],
			"formula": [
				"Empirical observations: (Start)",
				"For all odd numbers x \u003e= 3,",
				"a(x) = (1/2)*x - 1/2,",
				"a(2x) = (3/4)*(2x) - 3/2,",
				"a(4x) = (7/8)*(4x) - 5/2,",
				"a(8x) = (15/16)*(8x) - 7/2,",
				"etc.",
				"For all c, a(2^c) = A000325(c) = 2^c-c.",
				"Summarized by:",
				"a((2^c)*(2k+1)) = A126646(c)*k + A000295(c) + A000007(k) = (2^(c+1)-1)*k + (2^c-1-c) + [k==0].",
				"(End)",
				"From _Luc Rousseau_, Apr 01 2019: (Start)",
				"It appears that for all k \u003e 0,",
				"a(4k + 0) = 3k - 2 + a(k),",
				"a(4k + 1) = 2k,",
				"a(4k + 2) = 3k,",
				"a(4k + 3) = 2k + 1.",
				"(End)"
			],
			"example": [
				"In A323607 in triangular form,",
				"- row 5 is:  3  5  4  2  1",
				"- row 6 is:  3  5  6  4  2  1",
				"Row 6 is row 5 in which 6 has been inserted in position 3, so a(6) = 3."
			],
			"mathematica": [
				"lt[x_, y_] := Module[",
				"  {c, d, xx, yy, u, v},",
				"  {c, d} = IntegerExponent[#, 2] \u0026 /@ {x, y};",
				"  xx = x/2^c;",
				"  yy = y/2^d;",
				"  u = If[xx == 1, \\[Infinity], c];",
				"  v = If[yy == 1, \\[Infinity], d];",
				"  If[u != v, u \u003c v, If[u == \\[Infinity], c \u003e d, xx \u003c yy]]]",
				"row[n_] := Sort[Range[n], lt]",
				"a[n_] := First[FirstPosition[row[n], n]]",
				"Table[a[n], {n, 1, 80}]"
			],
			"xref": [
				"Cf. A194959 (introducing fractalization).",
				"Cf. A323607 (fractalization of this sequence).",
				"Cf. A126646, A000295, A000007.",
				"Cf. A000325."
			],
			"keyword": "nonn,look",
			"offset": "1,4",
			"author": "_Luc Rousseau_, Jan 19 2019",
			"references": 1,
			"revision": 24,
			"time": "2019-04-02T05:54:45-04:00",
			"created": "2019-01-24T02:02:29-05:00"
		}
	]
}