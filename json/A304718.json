{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A304718",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 304718,
			"data": "1,1,1,2,2,2,3,5,5,3,5,9,14,9,5,7,18,28,28,18,7,11,29,63,62,63,29,11,15,51,109,150,150,109,51,15,22,79,206,293,380,293,206,79,22,30,126,342,590,787,787,590,342,126,30,42,189,584,1061,1675,1760,1675,1061,584,189,42",
			"name": "Number T(n,k) of domino tilings of Ferrers-Young diagrams of partitions of 2n using exactly k horizontally oriented dominoes; triangle T(n,k), n\u003e=0, 0\u003c=k\u003c=n, read by rows.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A304718/b304718.txt\"\u003eRows n = 0..25, flattened\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/FerrersDiagram.html\"\u003eFerrers Diagram\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Domino_(mathematics)\"\u003eDomino\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Domino_tiling\"\u003eDomino tiling\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Ferrers_diagram\"\u003eFerrers diagram\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Mutilated_chessboard_problem\"\u003eMutilated chessboard problem\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Partition_(number_theory)\"\u003ePartition (number theory)\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Young_tableau#Diagrams\"\u003eYoung tableau, Diagrams\u003c/a\u003e",
				"\u003ca href=\"/index/Do#domino\"\u003eIndex entries for sequences related to dominoes\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = T(n,n-k)."
			],
			"example": [
				":   T(2,0) = 2   :   T(2,1) = 2       :   T(2,2) = 2         :",
				":   ._.  ._._.   :   .___.  ._.___.   :   .___.  .___.___.   :",
				":   | |  | | |   :   |___|  | |___|   :   |___|  |___|___|   :",
				":   |_|  |_|_|   :   | |    |_|       :   |___|              :",
				":   | |          :   |_|              :                      :",
				":   |_|          :                    :                      :",
				":                :                    :                      :",
				"Triangle T(n,k) begins:",
				"   1;",
				"   1,   1;",
				"   2,   2,   2;",
				"   3,   5,   5,    3;",
				"   5,   9,  14,    9,    5;",
				"   7,  18,  28,   28,   18,    7;",
				"  11,  29,  63,   62,   63,   29,   11;",
				"  15,  51, 109,  150,  150,  109,   51,   15;",
				"  22,  79, 206,  293,  380,  293,  206,   79,  22;",
				"  30, 126, 342,  590,  787,  787,  590,  342, 126,  30;",
				"  42, 189, 584, 1061, 1675, 1760, 1675, 1061, 584, 189, 42;",
				"  ..."
			],
			"maple": [
				"h:= proc(l, f) option remember; local k; if min(l[])\u003e0 then",
				"     `if`(nops(f)=0, 1, h(map(u-\u003e u-1, l[1..f[1]]), subsop(1=[][], f)))",
				"    else for k from nops(l) while l[k]\u003e0 by -1 do od; expand(",
				"        `if`(nops(f)\u003e0 and f[1]\u003e=k, x*h(subsop(k=2, l), f), 0)+",
				"        `if`(k\u003e1 and l[k-1]=0, h(subsop(k=1, k-1=1, l), f), 0))",
				"      fi",
				"    end:",
				"g:= l-\u003e `if`(add(`if`(l[i]::odd, (-1)^i, 0), i=1..nops(l))=0,",
				"        `if`(l=[], 1, h([0$l[1]], subsop(1=[][], l))), 0):",
				"b:= (n, i, l)-\u003e `if`(n=0 or i=1, g([l[], 1$n]), b(n, i-1, l)",
				"                  +b(n-i, min(n-i, i), [l[], i])):",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=0..degree(p)))(b(2*n$2, [])):",
				"seq(T(n), n=0..12);"
			],
			"mathematica": [
				"h[l_, f_] := h[l, f] = Module[{k}, If[Min[l] \u003e 0, If[Length[f] == 0, 1, h[l[[1 ;; f[[1]] ]] - 1, ReplacePart[f, 1 -\u003e Nothing]]], For[k = Length[l], l[[k]] \u003e 0, k--]; If[Length[f] \u003e 0 \u0026\u0026 f[[1]] \u003e= k, x*h[ReplacePart[l, k -\u003e 2], f], 0] + If[k \u003e 1 \u0026\u0026 l[[k - 1]] == 0, h[ReplacePart[l, {k -\u003e 1, k - 1 -\u003e 1}], f], 0]]];",
				"g[l_] := If[Sum[If[OddQ[l[[i]]], (-1)^i, 0], {i, 1, Length[l]}] == 0, If[l == {}, 1, h[Table[0, {l[[1]]}], ReplacePart[l, 1 -\u003e Nothing]]], 0];",
				"b[n_, i_, l_] := If[n == 0 || i == 1, g[Join[l, Table[1, {n}]]], b[n, i - 1, l] + b[n - i, Min[n - i, i], Append[l, i]]];",
				"T[n_] := CoefficientList[b[2n, 2n, {}], x];",
				"Table[T[n], {n, 0, 12}] // Flatten (* _Jean-François Alcover_, Aug 29 2021, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Row sums give A304662.",
				"Main diagonal and column k=0 give A000041.",
				"T(n,floor(n/2)) gives A304719."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Alois P. Heinz_, May 17 2018",
			"references": 3,
			"revision": 23,
			"time": "2021-08-29T12:01:53-04:00",
			"created": "2018-05-19T14:46:42-04:00"
		}
	]
}