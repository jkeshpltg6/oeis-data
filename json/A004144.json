{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A004144",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 4144,
			"id": "M0542",
			"data": "1,2,3,4,6,7,8,9,11,12,14,16,18,19,21,22,23,24,27,28,31,32,33,36,38,42,43,44,46,47,48,49,54,56,57,59,62,63,64,66,67,69,71,72,76,77,79,81,83,84,86,88,92,93,94,96,98,99,103,107,108,112,114,118,121,124,126,127",
			"name": "Nonhypotenuse numbers (indices of positive squares that are not the sums of 2 distinct nonzero squares).",
			"comment": [
				"Also numbers with no prime factors of form 4*k+1.",
				"m is a term iff A072438(m) = m.",
				"Density 0. - _Charles R Greathouse IV_, Apr 16 2012",
				"A005089(a(n)) = 0. - _Reinhard Zumkeller_, Jan 07 2013",
				"Closed under multiplication. Primitive elements are A045326, 2 and the primes of form 4*k+3. - _Jean-Christophe Hervé_, Nov 17 2013"
			],
			"reference": [
				"Steven R. Finch, Mathematical Constants, Cambridge, 2003, pp. 98-104.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Evan M. Bailey, \u003ca href=\"/A004144/b004144.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e (Terms 1..1000 from T. D. Noe)",
				"Evan M. Bailey, \u003ca href=\"/A004144/a004144.cpp.txt\"\u003ea004114.cpp\u003c/a\u003e",
				"Steven R. Finch, \u003ca href=\"http://www.people.fas.harvard.edu/~sfinch/constant/lr/lr.html\"\u003eLandau-Ramanujan Constant\u003c/a\u003e [Broken link]",
				"Steven R. Finch, \u003ca href=\"http://web.archive.org/web/20010605004309/http://www.mathsoft.com/asolve/constant/lr/lr.html\"\u003eLandau-Ramanujan Constant\u003c/a\u003e [From the Wayback machine]",
				"D. Shanks, \u003ca href=\"http://www.fq.math.ca/Scanned/13-4/shanks.pdf\"\u003eNon-hypotenuse numbers\u003c/a\u003e, Fib. Quart., 13:4 (1975), pp. 319-321.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PythagoreanTriple.html\"\u003ePythagorean Triple\u003c/a\u003e",
				"\u003ca href=\"/index/Su#ssq\"\u003eIndex entries for sequences related to sums of squares\u003c/a\u003e"
			],
			"mathematica": [
				"fQ[n_] := If[n \u003e 1, First@ Union@ Mod[ First@# \u0026 /@ FactorInteger@ n, 4] != 1, True]; Select[ Range@ 127, fQ]",
				"A004144 = Select[Range[127],Length@Reduce[s^2 + t^2 == s # \u0026\u0026 s \u003e t \u003e 0, Integers] == 0 \u0026] (* _Gerry Martens_, Jun 09 2020 *)"
			],
			"program": [
				"(PARI) is(n)=n==1||vecmin(factor(n)[,1]%4)\u003e1 \\\\ _Charles R Greathouse IV_, Apr 16 2012",
				"(PARI) list(lim)=my(v=List(),u=vectorsmall(lim\\=1)); forprimestep(p=5,lim,4, forstep(n=p,lim,p, u[n]=1)); for(i=1,lim, if(u[i]==0, listput(v,i))); u=0; Vec(v) \\\\ _Charles R Greathouse IV_, Jan 13 2022",
				"(Haskell)",
				"import Data.List (elemIndices)",
				"a004144 n = a004144_list !! (n-1)",
				"a004144_list = map (+ 1) $ elemIndices 0 a005089_list",
				"-- _Reinhard Zumkeller_, Jan 07 2013"
			],
			"xref": [
				"Complement of A009003.",
				"Cf. A000290, A002145, A072437.",
				"The subsequence of primes is A045326."
			],
			"keyword": "nonn,changed",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Reinhard Zumkeller_, Jun 17 2002",
				"Name clarified by _Evan M. Bailey_, Sep 17 2019"
			],
			"references": 43,
			"revision": 71,
			"time": "2022-01-13T12:05:57-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}