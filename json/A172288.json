{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A172288",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 172288,
			"data": "1,2,1,2,3,1,2,4,9,1,2,4,25,129,1,2,4,35,4225,32769,1,2,4,36,47905,268468225,2147483649,1,2,4,36,222241,733276217345,1152921506754330625,9223372036854775809,1",
			"name": "Square array A(n,k), n\u003e=0, k\u003e=0, read by antidiagonals: A(n,k) is the number of partitions of 2^2^n into powers of 2 less than or equal to 2^k.",
			"comment": [
				"A(18,18) = 2797884726...4715787265 has 1420371 decimal digits and was computed by the algorithm given below."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A172288/b172288.txt\"\u003eTable of n, a(n) for n = 0..77\u003c/a\u003e"
			],
			"formula": [
				"A(n,k) = [x^2^(2^n-1)] 1/(1-x) * 1/Product_{j=0..k-1} (1-x^(2^j))."
			],
			"example": [
				"A(2,1) = 9, because there are 9 partitions of 2^2^2=16 into powers of 2 less than or equal to 2^1=2: [2,2,2,2,2,2,2,2], [2,2,2,2,2,2,2,1,1], [2,2,2,2,2,2,1,1,1,1], [2,2,2,2,2,1,1,1,1,1,1], [2,2,2,2,1,1,1,1,1,1,1,1], [2,2,2,1,1,1,1,1,1,1,1,1,1], [2,2,1,1,1,1,1,1,1,1,1,1,1,1], [2,1,1,1,1,1,1,1,1,1,1,1,1,1,1], [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1].",
				"Square array A(n,k) begins:",
				"  1,     2,         2,            2,               2,  ...",
				"  1,     3,         4,            4,               4,  ...",
				"  1,     9,        25,           35,              36,  ...",
				"  1,   129,      4225,        47905,          222241,  ...",
				"  1, 32769, 268468225, 733276217345, 751333186150401,  ..."
			],
			"maple": [
				"b:= proc(n,j) option remember; local nn, r;",
				"      if n\u003c0 then 0",
				"    elif j=0 then 1",
				"    elif j=1 then n+1",
				"    elif n\u003cj then b(n,j):= b(n-1, j) +b(2*n, j-1)",
				"             else nn:= 1 +floor(n);",
				"                  r:= n-nn;",
				"                  (nn-j) *binomial(nn, j) *add(binomial(j, h)",
				"                  /(nn-j+h) *b(j-h+r, j) *(-1)^h, h=0..j-1)",
				"      fi",
				"    end:",
				"A:= (n,k)-\u003e b(2^(2^n-k), k):",
				"seq(seq(A(n, d-n), n=0..d), d=0..8);"
			],
			"mathematica": [
				"b[n_, j_] := b[n, j] = Module[{nn, r}, Which[n \u003c 0, 0, j == 0, 1, j == 1, n+1, n \u003c j , b[n, j] = b[n-1, j] + b[2*n, j-1] , True, nn = 1 + Floor[n]; r := n - nn; (nn-j)*Binomial[nn, j] * Sum [Binomial[j, h] /(nn - j + h) * b[j - h + r, j] *(-1)^h, {h, 0, j-1}] ] ]; a[n_, k_] := b[2^(2^n-k), k]; Table[Table[a[n, d-n] // FullSimplify, {n, 0, d}], {d, 0, 8}] // Flatten (* _Jean-François Alcover_, Dec 11 2013, translated from Maple *)"
			],
			"xref": [
				"Cf. A002577, A000123, A181322, A145515.",
				"Main diagonal gives: A182135."
			],
			"keyword": "nonn,tabl",
			"offset": "0,2",
			"author": "_Alois P. Heinz_, Jan 26 2011",
			"references": 3,
			"revision": 36,
			"time": "2018-09-21T22:41:10-04:00",
			"created": "2010-11-12T14:22:02-05:00"
		}
	]
}