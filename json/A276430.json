{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A276430",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 276430,
			"data": "1,1,1,1,2,1,2,2,1,3,3,1,5,3,2,1,6,5,3,1,7,8,4,2,1,10,10,6,3,1,13,13,9,4,2,1,16,18,12,6,3,1,22,22,16,10,4,2,1,27,29,22,13,6,3,1,33,40,28,17,10,4,2,1,43,49,37,24,13,6,3,1,52,63,50,31,18,10,4,2,1",
			"name": "Triangle read by rows: T(n,k) is the number of partitions of n having k parts that are powers of 2 with positive exponent (n\u003e=0).",
			"comment": [
				"Sum of entries in row n is A000041(n) (the partition numbers).",
				"T(n,0) = A276431(n).",
				"Sum(k*T(n,k), k\u003e=0) = A073119(n)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A276430/b276430.txt\"\u003eRows n = 0..300, flattened\u003c/a\u003e"
			],
			"formula": [
				"G.f.:  G(t,x) = Product_{i\u003e=1} (1-x^{h(i)})/((1-x^i)*(1-t*x^{h(i)})), where h(i) = 2^i."
			],
			"example": [
				"T(6,1) = 3, counting [1,1,4], [1,2,3], [1,1,1,1,2];",
				"T(6,2) = 2, counting [2,4], [1,1,2,2];",
				"T(6,3) = 1, counting [2,2,2];",
				"Triangle starts:",
				"1;",
				"1;",
				"1,1;",
				"2,1;",
				"2,2,1;",
				"3,3,1;",
				"..."
			],
			"maple": [
				"h := proc (i) options operator, arrow: 2^i end proc: G := product((1-x^h(i))/((1-x^i)*(1-t*x^h(i))), i = 1 .. 30): Gser := simplify(series(G, x = 0, 25)): for n from 0 to 20 do P[n] := sort(coeff(Gser, x, n)) end do: for n from 0 to 20 do seq(coeff(P[n], t, k), k = 0 .. degree(P[n])) end do; # yields sequence in triangular form",
				"# second Maple program:",
				"p2:= proc(n) p2(n):= is(n=2^ilog2(n)) end:",
				"b:= proc(n, i) option remember; expand(`if`(n=0 or i=1, 1,",
				"      b(n, i-1)+`if`(i\u003en, 0, b(n-i, i)*`if`(p2(i), x, 1))))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=0..degree(p)))(b(n$2)):",
				"seq(T(n), n=0..30);  # _Alois P. Heinz_, Sep 20 2016"
			],
			"mathematica": [
				"p2[n_] := n == 2^Floor[Log[2, n]]; b[n_, i_] := b[n, i] = Expand[If[n == 0 || i == 1, 1, b[n, i-1] + If[i\u003en, 0, b[n-i, i]*If[p2[i], x, 1]]]]; T[n_] := Function [p, Table[Coefficient[p, x, i], {i, 0, Exponent[p, x]}]][b[n, n]]; Table[T[n], {n, 0, 30}] // Flatten (* _Jean-François Alcover_, Dec 21 2016, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A000041, A073119, A276431."
			],
			"keyword": "nonn,tabf",
			"offset": "0,5",
			"author": "_Emeric Deutsch_, Sep 19 2016",
			"references": 2,
			"revision": 16,
			"time": "2016-12-21T13:48:16-05:00",
			"created": "2016-09-20T03:07:09-04:00"
		}
	]
}