{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A184165",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 184165,
			"data": "2,3,5,5,8,8,9,9,13,13,13,14,14,14,21,17,14,22,17,23,23,21,22,26,34,22,35,24,23,36,21,33,34,23,37,40,26,26,36,43,22,38,24,37,57,35,36,50,41,59,37,38,33,62,55,44,43,36,23,66,40,34,61,65,58,58,26,41,57,62,43,76,38,40,93,44,60,60,37,83",
			"name": "Number of independent (vertex) subsets in the rooted tree with Matula-Goebel number n.",
			"comment": [
				"A vertex subset in a tree is said to be independent if no pair of vertices is connected by an edge. The empty set is considered to be independent. For example, the 1-edge tree AB has 3 independent subsets: the empty set, {A}, and {B}.",
				"The Matula-Goebel number of a rooted tree can be defined in the following recursive manner: to the one-vertex tree there corresponds the number 1; to a tree T with root degree 1 there corresponds the t-th prime number, where t is the Matula-Goebel number of the tree obtained from T by deleting the edge emanating from the root; to a tree T with root degree m\u003e=2 there corresponds the product of the Matula-Goebel numbers of the m branches of T.",
				"The number of independent subsets of a graph G is called the Merrifield-Simmons index of G."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A184165/b184165.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"M. B. Ahmadi and M. Seif, \u003ca href=\"https://www.researchgate.net/publication/266169272_The_merrifield-simmons_index_of_an_infinite_class_of_dendrimers\"\u003eThe Merrifield-Simmons index of an infinite class of dendrimers\u003c/a\u003e, Digest J. of Nanomaterials and Biostructures, 5, 2010, 335-338.",
				"É. Czabarka, L. Székely, and S. Wagner, \u003ca href=\"http://dx.doi.org/10.1016/j.dam.2009.07.004\"\u003eThe inverse problem for certain tree parameters\u003c/a\u003e, Discrete Appl. Math., 157, 2009, 3314-3319.",
				"F. Goebel, \u003ca href=\"http://dx.doi.org/10.1016/0095-8956(80)90049-0\"\u003eOn a 1-1-correspondence between rooted trees and natural numbers\u003c/a\u003e, J. Combin. Theory, B 29 (1980), 141-143.",
				"I. Gutman and A. Ivic, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(95)00182-V\"\u003eOn Matula numbers\u003c/a\u003e, Discrete Math., 150, 1996, 131-142.",
				"I. Gutman and Yeong-Nan Yeh, \u003ca href=\"http://www.emis.de/journals/PIMB/067/3.html\"\u003eDeducing properties of trees from their Matula numbers\u003c/a\u003e, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"D. W. Matula, \u003ca href=\"http://www.jstor.org/stable/2027327\"\u003eA natural rooted tree enumeration by prime factorization\u003c/a\u003e, SIAM Rev. 10 (1968) 273.",
				"H. Prodinger and R. F. Tichy, \u003ca href=\"http://www.fq.math.ca/Scanned/20-1/prodinger.pdf\"\u003eFibonacci numbers of graphs\u003c/a\u003e, Fibonacci Quarterly, 20, 1982, 16-21.",
				"\u003ca href=\"/index/Mat#matula\"\u003eIndex entries for sequences related to Matula-Goebel numbers\u003c/a\u003e"
			],
			"formula": [
				"Define b(n) (c(n)) to be the number of independent subsets of the rooted tree with Matula-Goebel number n that contain (do not contain) the root. We have the following recurrence for the pair A(n)=[b(n),c(n)]. A(1)=[1,1]; if n=p(t) (=the t-th prime), then A(n)=[c(t),b(t)+c(t)]; if n=rs (r,s,\u003e=2), then A(n)=[b(r)b(s), c(r)c(s)]. Clearly, a(n)=b(n)+c(n). See the Czabarka et al. reference (p. 3315, (3)). The Maple program is based on this recursive formula.",
				"a(n) = A228731(n) + A228732(n). - _Reinhard Zumkeller_, Sep 01 2013"
			],
			"example": [
				"a(2)=3 because the tree with the Matula number 2 is the 1-edge tree AB with 3 independent subsets: (empty, {A}, {B}).",
				"a(2655237841)=3216386; the tree D[3] in Fig. 1 of the Ahmadi et al. reference has Merrifield-Simmons index 3216386 (see Table 1). The Matula-Goebel number of D[3] can be found to be 227^4=2655237841."
			],
			"maple": [
				"with(numtheory): A := proc (n) local r, s: r := proc (n) options operator, arrow: op(1, factorset(n)) end proc: s := proc (n) options operator, arrow: n/r(n) end proc: if n = 1 then [1, 1] elif bigomega(n) = 1 then [A(pi(n))[2], A(pi(n))[1]+A(pi(n))[2]] else [A(r(n))[1]*A(s(n))[1], A(r(n))[2]*A(s(n))[2]] end if end proc: a := proc (n) options operator, arrow: A(n)[1]+A(n)[2] end proc: seq(a(n), n = 1 .. 80);"
			],
			"mathematica": [
				"r[n_] := FactorInteger[n][[1, 1]];",
				"s[n_] := n/r[n];",
				"A[n_] := A[n] = If[n==1, {1, 1}, If[PrimeOmega[n]==1, {A[PrimePi[n]][[2]], A[PrimePi[n]] // Total}, A[r[n]] * A[s[n]]]];",
				"a[n_] := A[n] // Total;",
				"a /@ Range[1, 80] (* _Jean-François Alcover_, Sep 20 2019, from Maple *)"
			],
			"program": [
				"(Haskell)",
				"import Data.List (genericIndex)",
				"a184165 n = a228731 n + a228732 n",
				"a228731 n = genericIndex a228731_list (n - 1)",
				"a228732 n = genericIndex a228732_list (n - 1)",
				"(a228731_list, a228732_list) = unzip $ (1, 1) : map f [2..] where",
				"   f x | i \u003e 0     = (a228732 i, a228731 i + a228732 i)",
				"       | otherwise = (a228731 u * a228731 v, a228732 u * a228732 v)",
				"       where i = a049084 x",
				"             u = a020639 x; v = x `div` u",
				"-- _Reinhard Zumkeller_, Sep 01 2013",
				"(PARI)",
				"R(n)={my(f=factor(n), g=f); for(i=1, #f~, my([b,c]=R(primepi(f[i,1]))); f[i,1]=c; g[i,1]=b+c); [factorback(f), factorback(g)]}",
				"a(n)=vecsum(R(n)); \\\\ _Andrew Howroyd_, Aug 01 2018"
			],
			"xref": [
				"Cf. A049084, A020639, A228731, A228732."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Emeric Deutsch_, Oct 19 2011",
			"references": 5,
			"revision": 23,
			"time": "2019-09-20T10:10:11-04:00",
			"created": "2011-10-19T21:46:00-04:00"
		}
	]
}