{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A242527",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 242527,
			"data": "0,0,0,0,1,1,2,6,6,22,80,504,840,6048,3888,37524,72976,961776,661016,11533030,7544366,133552142,208815294,5469236592,6429567323,153819905698,182409170334,4874589558919,7508950009102,209534365631599",
			"name": "Number of cyclic arrangements (up to direction) of {0,1,...,n-1} such that the sum of any two neighbors is a prime.",
			"comment": [
				"a(n)=NPC(n;S;P) is the count of all neighbor-property cycles for a specific set S={0,1,...,n-1} of n elements and a specific pair-property P. For more details, see the link and A242519.",
				"For the same pair-property P but the set {1 through n}, see A051252. Using for pair-property the difference, rather than the sum, one obtains A228626."
			],
			"link": [
				"S. Sykora, \u003ca href=\"http://dx.doi.org/10.3247/SL5Math14.002\"\u003eOn Neighbor-Property Cycles\u003c/a\u003e, \u003ca href=\"http://ebyte.it/library/Library.html#math\"\u003eStan's Library\u003c/a\u003e, Volume V, 2014."
			],
			"example": [
				"The first such cycle is of length n=5: {0,2,1,4,3}.",
				"The first case with 2 solutions is for cycle length n=7:",
				"C_1={0,2,3,4,1,6,5}, C_2={0,2,5,6,1,4,3}.",
				"The first and the last of the 22 such cycles of length n=10 are:",
				"C_1={0,3,2,1,4,9,8,5,6,7}, C_22={0,5,8,9,4,3,2,1,6,7}."
			],
			"mathematica": [
				"A242527[n_] := Count[Map[lpf, Map[j0f, Permutations[Range[n - 1]]]], 0]/2;",
				"j0f[x_] := Join[{0}, x, {0}];",
				"lpf[x_] := Length[Select[asf[x], ! PrimeQ[#] \u0026]];",
				"asf[x_] := Module[{i}, Table[x[[i]] + x[[i + 1]], {i, Length[x] - 1}]];",
				"Table[A242527[n], {n, 1, 10}]",
				"(* OR, a less simple, but more efficient implementation. *)",
				"A242527[n_, perm_, remain_] := Module[{opt, lr, i, new},",
				"   If[remain == {},",
				"     If[PrimeQ[First[perm] + Last[perm]], ct++];",
				"     Return[ct],",
				"     opt = remain; lr = Length[remain];",
				"     For[i = 1, i \u003c= lr, i++,",
				"      new = First[opt]; opt = Rest[opt];",
				"      If[! PrimeQ[Last[perm] + new], Continue[]];",
				"      A242527[n, Join[perm, {new}],",
				"       Complement[Range[n - 1], perm, {new}]];",
				"      ];",
				"     Return[ct];",
				"     ];",
				"   ];",
				"Table[ct = 0; A242527[n, {0}, Range[n - 1]]/2, {n, 1, 15}]",
				"(* _Robert Price_, Oct 18 2018 *)"
			],
			"program": [
				"(C++) See the link."
			],
			"xref": [
				"Cf. A051252, A228626, A242519, A242520, A242521, A242522, A242523, A242524, A242525, A242526, A242528, A242529, A242530, A242531, A242532, A242533, A242534."
			],
			"keyword": "nonn,hard",
			"offset": "1,7",
			"author": "_Stanislav Sykora_, May 30 2014",
			"ext": [
				"a(23)-a(30) from _Max Alekseyev_, Jul 09 2014"
			],
			"references": 20,
			"revision": 12,
			"time": "2018-10-19T16:58:53-04:00",
			"created": "2014-05-30T12:45:41-04:00"
		}
	]
}