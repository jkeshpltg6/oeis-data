{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A080855",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 80855,
			"data": "1,4,16,37,67,106,154,211,277,352,436,529,631,742,862,991,1129,1276,1432,1597,1771,1954,2146,2347,2557,2776,3004,3241,3487,3742,4006,4279,4561,4852,5152,5461,5779,6106,6442,6787,7141,7504,7876,8257,8647,9046",
			"name": "a(n) = (9*n^2 - 3*n + 2)/2.",
			"comment": [
				"The old definition of this sequence was \"Generalized polygonal numbers\".",
				"Row T(3,n) of A080853.",
				"Equals binomial transform of [1, 3, 9, 0, 0, 0, ...] - _Gary W. Adamson_, Apr 30 2008",
				"a(n) is also the least weight of self-conjugate partitions having n different parts such that each part is congruent to 2 modulo 3. The first such self-conjugate partitions, corresponding to a(n)=1,2,3,4, are 2+2, 5+5+2+2+2, 8+8+5+5+5+2+2+2, 11+11+8+8+8+5+5+5+2+2+2. - _Augustine O. Munagi_, Dec 18 2008",
				"Let A be the Hessenberg matrix of order n, defined by: A[1,j]=1, A[i,i]:=3, (i\u003e1), A[i,i-1]=-1, and A[i,j]=0 otherwise. Then, for n \u003e= 3, a(n-1) = -coeff(charpoly(A,x), x^(n-2)). - _Milan Janjic_, Jan 27 2010"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A080855/b080855.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"M. Janjic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL13/Janjic/janjic33.html\"\u003eHessenberg Matrices and Integer Sequences \u003c/a\u003e, J. Int. Seq. 13 (2010) # 10.7.8",
				"A. O. Munagi, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2007.05.022\"\u003ePairing conjugate partitions by residue classes\u003c/a\u003e, Discrete Math., 308 (2008), 2492-2501.",
				"Franck Ramaharo, \u003ca href=\"https://arxiv.org/abs/1802.07701\"\u003eStatistics on some classes of knot shadows\u003c/a\u003e, arXiv:1802.07701 [math.CO], 2018.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1)."
			],
			"formula": [
				"G.f.: (1 + x + 7*x^2)/(1 - x)^3.",
				"a(n) = 9*n + a(n-1) - 6 with n \u003e 0, a(0)=1. - _Vincenzo Librandi_, Aug 08 2010",
				"a(n) = n*A005448(n+1) - (n-1)*A005448(n), with A005448(0)=1. - _Bruno Berselli_, Jan 15 2013",
				"a(0)=1, a(1)=4, a(2)=16; for n \u003e 2, a(n) = 3*a(n-1) - 3*a(n-2) + a(n-3). - _Harvey P. Dale_, Jul 24 2013",
				"a(n) = A152947(3*n+1). - _Franck Maminirina Ramaharo_, Jan 10 2018",
				"E.g.f.: (2 + 6*x + 9*x^2)*exp(x)/2. - _G. C. Greubel_, Nov 02 2018"
			],
			"maple": [
				"seq((9*n^2-3*n+2)/2,n=0..50); # _Muniru A Asiru_, Nov 02 2018"
			],
			"mathematica": [
				"s = 1; lst = {s}; Do[s += n + 2; AppendTo[lst, s], {n, 1, 500, 9}]; lst (* _Zerinvary Lajos_, Jul 11 2009 *)",
				"Table[(9n^2-3n+2)/2,{n,0,50}] (* or *) LinearRecurrence[{3,-3,1}, {1,4,16}, 50] (* _Harvey P. Dale_, Jul 24 2013 *)"
			],
			"program": [
				"(PARI) a(n)=binomial(3*n,2)+1 \\\\ _Charles R Greathouse IV_, Oct 07 2015",
				"(MAGMA) [(9*n^2 - 3*n +2)/2: n in [0..50]]; // _G. C. Greubel_, Nov 02 2018",
				"(GAP) List([0..50],n-\u003e(9*n^2-3*n+2)/2); # _Muniru A Asiru_, Nov 02 2018"
			],
			"xref": [
				"Cf. A027468, A038764.",
				"Cf. A283394 (see Crossrefs section)."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Paul Barry_, Feb 23 2003",
			"ext": [
				"Definition replaced with the closed form by _Bruno Berselli_, Jan 15 2013"
			],
			"references": 10,
			"revision": 62,
			"time": "2018-11-02T12:36:45-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}