{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A306275",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 306275,
			"data": "1,1,2,2,4,2,6,4,6,4,10,2,12,6,8,8,16,2,18,4,12,10,22,2,20,12,18,6,28,2,30,16,20,16,24,2,36,18,24,4,40,2,42,10,8,22,46,2,42,4,32,12,52,2,40,6,36,28,58,2,60,30,12,32,48,2,66,16,44,4,70,2,72",
			"name": "Number of values 0 \u003c k \u003c= n for which there are no k distinct n-th roots of unity that sum to zero.",
			"comment": [
				"In the first 17 terms a(n) = phi(n) except for n=12. For primes a(p) = p - 1.",
				"Also the number of 0's in the n-th row of A103306. - _Alois P. Heinz_, Feb 03 2019"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A306275/b306275.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Holly Krieger and Brady Haran, \u003ca href=\"https://www.youtube.com/watch?v=7DHE8RnsCQ8\"\u003eThe Centrifuge Problem\u003c/a\u003e, Numberphile video (2018)",
				"T. Y. Lam and K. H. Leung, \u003ca href=\"http://arXiv.org/abs/math.NT/9511209\"\u003eOn vanishing sums for roots of unity\u003c/a\u003e, arXiv:math/9511209 [math.NT], 1995.",
				"Matt Parker, \u003ca href=\"https://mattbaker.blog/2018/06/25/the-balanced-centrifuge-problem/\"\u003eThe Balanced Centrifuge Problem\u003c/a\u003e, Math Blog, 2018.",
				"Gary Sivek, \u003ca href=\"http://www.emis.de/journals/INTEGERS/papers/k31/k31.Abstract.html\"\u003eOn vanishing sums of distinct roots of unity\u003c/a\u003e, #A31, Integers 10 (2010), 365-368."
			],
			"formula": [
				"a(n) = #{k in {1,2,...,n} | for all subsets U of {exp(2*Pi*i*m/n)|m=0,1,...,n-1} of size #U=k we have sum(U) != 0 }.",
				"a(n) = 1 + n - A322366(n) for n \u003e 1, a(1) = 1. - _Alois P. Heinz_, Feb 03 2019",
				"a(n) is even for n \u003e= 3. - _Alois P. Heinz_, Feb 05 2019"
			],
			"maple": [
				"a:= proc(n) option remember; local f, b; f, b:=",
				"       map(i-\u003e i[1], ifactors(n)[2]),",
				"       proc(m, i) option remember; m=0 or i\u003e0 and",
				"        (b(m, i-1) or f[i]\u003c=m and b(m-f[i], i))",
				"       end; forget(b); (t-\u003e add(",
				"      `if`(b(j, t) and b(n-j, t), 0, 1), j=1..n))(nops(f))",
				"    end:",
				"seq(a(n), n=1..100);  # _Alois P. Heinz_, Feb 03 2019"
			],
			"mathematica": [
				"a := Function[{n}, Count[Function[{k}, Fold[And, (#!=0)\u0026 /@ RootReduce @* Total /@ Subsets[Exp[2*Pi*I*#/n]\u0026 /@ Range[0,n-1], {k}]]] /@ Range[1,n],True] ]",
				"(* Second program: *)",
				"A322366[n_] := A322366[n] = Module[{f, b}, f = FactorInteger[n][[All, 1]]; b[m_, i_] := b[m, i] = m == 0 || i \u003e 0 \u0026\u0026 (b[m, i - 1] || f[[i]] \u003c= m \u0026\u0026 b[m - f[[i]], i]); Function[t, Sum[If[b[j, t] \u0026\u0026 b[n - j, t], 1, 0], {j, 0, n}]][Length[f]]];",
				"a[n_] := If[n == 1, 1, 1 + n - A322366[n]];",
				"Array[a, 100] (* _Jean-François Alcover_, May 23 2020, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A103306, A322366."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Florentin Bieder_, Feb 03 2019",
			"ext": [
				"More terms from _Alois P. Heinz_, Feb 03 2019"
			],
			"references": 2,
			"revision": 34,
			"time": "2020-05-23T06:55:11-04:00",
			"created": "2019-02-03T21:00:03-05:00"
		}
	]
}