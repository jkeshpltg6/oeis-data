{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A346787",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 346787,
			"data": "1,0,1,1,2,3,6,10,19,35,68,128,253,489,981,1930,3899,7771,15858,31915,65503,133070,274631,561371,1164240,2393652,4983614,10299238,21511537,44637483,93552858,194809152,409270569,855199845,1800958182,3773297872,7963655481",
			"name": "Ordered lone-child-avoiding trees where vertices have decreasing subtree sizes.",
			"comment": [
				"a(n) is the number of size-n, rooted, ordered, lone-child-avoiding trees in which the subtrees of each non-leaf vertex, taken left to right, have weakly decreasing sizes, where size is measured by number of vertices.",
				"The analogous trees when size is measured by number of leaves are counted by A196545."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A346787/b346787.txt\"\u003eTable of n, a(n) for n = 1..2948\u003c/a\u003e",
				"David Callan, \u003ca href=\"/A346787/a346787.pdf\"\u003eTrees of size up to 7 for A346787\u003c/a\u003e",
				"David Callan, \u003ca href=\"https://arxiv.org/abs/2108.04969\"\u003eA Combinatorial Interpretation for Sequence A345973 in OEIS\u003c/a\u003e, arXiv:2108.04969 [math.CO], 2021."
			],
			"formula": [
				"Counting by sizes of subtrees of the root, a(n) is the sum, over all non-singleton partitions i_1,i_2,...,i_k of n-1, of the product a(i_1)a(i_2) ... a(i_k).",
				"G.f. satisfies A(x)=x/((1+x)*Product_{n\u003e=1} (1 - a(n)*x^n))."
			],
			"example": [
				"See Link."
			],
			"maple": [
				"b:= proc(n, i) option remember; `if`(n=0, 1, `if`(i\u003c1, 0,",
				"       b(n, i-1)+a(i)*b(n-i, min(n-i, i))))",
				"    end:",
				"a:= n-\u003e b(n-1, n-2):",
				"seq(a(n), n=1..40);  # _Alois P. Heinz_, Aug 05 2021"
			],
			"mathematica": [
				"a[1] = 1; a[2] = 0;",
				"a[n_] /; n \u003e= 3 := a[n] = Apply[Plus, Map[Apply[Times, Map[a, #]] \u0026, Rest[IntegerPartitions[n - 1]]]]",
				"Table[a[n], {n, 20}]"
			],
			"xref": [
				"Cf. A196545."
			],
			"keyword": "nonn,changed",
			"offset": "1,5",
			"author": "_David Callan_, Aug 03 2021",
			"references": 1,
			"revision": 19,
			"time": "2022-01-10T11:03:24-05:00",
			"created": "2021-08-05T02:34:49-04:00"
		}
	]
}