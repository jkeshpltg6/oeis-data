{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A295339",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 295339,
			"data": "15,52,108,184,279,394,530,684,859,1053,1267,1501,1755,2028,2321,2634,2966,3318,3690,4082,4493,4925,5375,5846,6336,6847,7376,7926,8495,9085,9693,10322,10970,11638,12326,13034,13761,14508,15275",
			"name": "Least k for the inner Theodorus spiral to complete n revolutions.",
			"comment": [
				"Here the points of the inner discrete Theodurus spiral in the complex plane are zhat(k) = rho(k)*exp(i*phihat(k)) with rho(k) = sqrt(k) and phihat(k) starts with phihat(1) = Pi/2 and is not restricted to be \u003c= 2*Pi, it is phihat(k) = Sum_{j=0..k-1} (2*alpha(j+1) - alpha(j)) with alpha(j) = arctan(1/sqrt(j)), for k \u003e= 1. The formula is phihat(k) = phi(k) + alpha(k), with the recurrence for the arguments of the outer spiral phi(k) = phi(k-1) + alpha(k-1), k \u003e= 2, with phi(1) = 0.",
				"If one considers punctured sheets S_n = rho*exp(i*phi_n), with rho \u003e 0 and 2*Pi*(n-1) \u003c= phi_n \u003c 2*Pi*n, for n \u003e= 1, then on sheet S_n there are a(n) - a(n-1) = A296179(n) points zhat, where a(0) = 0.",
				"An analytic continuation of Davis's interpolation of the outer spiral is given in the Waldvogel link (see Figure 2 there). The point zhat(k) (called G_k on Figure 1 there) on the inner spiral is obtained from mirroring the point z(k) (called F_k there) of the outer spiral on the hypotenuse O,z(k+1), for k \u003e= 1. In the present case the arguments phihat(k) of zhat(k) are taken positive.",
				"Conjecture: a(n) = A072895(n) - 2, n \u003e= 1. This follows from the conjecture that the sequences K := {floor(phi(k)/(2*Pi)}_{k \u003e= 1} with phi given above, and Khat:= {floor(phihat(k)/(2*Pi)}_{k \u003e= 1} with phihat given above satisfy Khat(k-2) = K(k), for k \u003e= 3. Note that phihat(k-2) - phi(k) = alpha(k-2) - alpha(k-1) =: delta(k) = arctan((sqrt(k-1) - sqrt(k-2))/(1 + sqrt((k-1)*(k-2)))) \u003e 0, for k \u003e= 3. Therefore the conjecture is that delta(k) \u003c 2*Pi*(1 - frac(phi(k)/(2*Pi))), for k \u003e= 3, or, equivalently, phihat(k-2) \u003c 2*Pi*(K(k) + 1), for k \u003e= 3."
			],
			"reference": [
				"P. J. Davis, Spirals from Theodorus to Chaos, A K Peters, Wellesley, MA, 1993."
			],
			"link": [
				"Joerg Waldvogel, \u003ca href=\"http://www.sam.math.ethz.ch/~joergw/Papers/theopaper.pdf\"\u003eAnalytic Continuation of the Theodorus Spiral\u003c/a\u003e."
			],
			"formula": [
				"a(n) = -1 + first position of n in the sequence",
				"  Khat:= {floor(phihat(k)/(2*Pi))}_{ k\u003e= 1}, with phihat given in a comment above in terms of phi.",
				"Conjecture: a(n) = A072895(n) - 2, n \u003e= 1 (see the comment above)."
			],
			"xref": [
				"Cf. A072895 (outer spiral), A296179."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Wolfdieter Lang_, Dec 13 2017",
			"references": 2,
			"revision": 14,
			"time": "2017-12-18T04:31:58-05:00",
			"created": "2017-12-18T04:31:58-05:00"
		}
	]
}