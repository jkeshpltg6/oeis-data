{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A208475",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 208475,
			"data": "1,2,2,7,2,3,10,10,3,4,23,12,11,4,5,36,30,17,14,5,6,65,40,35,18,17,6,7,94,82,49,44,22,20,7,8,160,110,93,58,48,26,23,8,9,230,190,133,108,70,56,30,26,9,10,356,260,217,148,124,76,64,34,29,10,11",
			"name": "Triangle read by rows: T(n,k) = total sum of odd/even parts \u003e= k in all partitions of n, if k is odd/even.",
			"comment": [
				"Essentially this sequence is related to A206561 in the same way as A206563 is related to A181187. See the calculation in the example section of A206563."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A208475/b208475.txt\"\u003eRows n = 1..141, flattened\u003c/a\u003e"
			],
			"example": [
				"Triangle begins:",
				"1;",
				"2,   2;",
				"7,   2,  3;",
				"10, 10,  3,  4;",
				"23, 12, 11,  4,  5;",
				"36, 30, 17, 14,  5,  6;"
			],
			"maple": [
				"p:= (f, g)-\u003e zip((x, y)-\u003e x+y, f, g, 0):",
				"b:= proc(n, i) option remember; local f, g;",
				"      if n=0 then [1]",
				"    elif i=1 then [1, n]",
				"    else f:= b(n, i-1); g:= `if`(i\u003en, [0], b(n-i, i));",
				"         p (p (f, g), [0$i, g[1]])",
				"      fi",
				"    end:",
				"T:= proc(n) local l;",
				"      l:= b(n, n);",
				"      seq (add (l[i+2*j+1]*(i+2*j), j=0..(n-i)/2), i=1..n)",
				"    end:",
				"seq (T(n), n=1..14);  # _Alois P. Heinz_, Mar 21 2012"
			],
			"mathematica": [
				"p[f_, g_] := With[{m = Max[Length[f], Length[g]]}, PadRight[f, m, 0] + PadRight[g, m, 0]]; b[n_, i_] := b[n, i] = Module[{f, g}, Which[n == 0, {1}, i == 1, {1, n}, True, f = b[n, i-1]; g = If[i\u003en, {0}, b[n-i, i]]; p[p[f, g], Append[Array[0\u0026, i], g[[1]]]]]]; T[n_] := Module[{l}, l = b[n, n]; Table[Sum[l[[i+2j+1]]*(i+2j), {j, 0, (n-i)/2}], {i, 1, n}]]; Table[T[n], {n, 1, 14}] // Flatten (* _Jean-François Alcover_, Mar 11 2015, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Column 1-2: A066967, A066966. Right border is A000027.",
				"Cf. A138785, A181187, A206561, A206563, A208476."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Omar E. Pol_, Feb 28 2012",
			"ext": [
				"More terms from _Alois P. Heinz_, Mar 21 2012"
			],
			"references": 3,
			"revision": 21,
			"time": "2015-03-11T07:27:17-04:00",
			"created": "2012-03-21T21:56:51-04:00"
		}
	]
}