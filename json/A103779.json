{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A103779",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 103779,
			"data": "0,1,-1,1,0,-4,14,-30,33,55,-429,1365,-2652,1428,12920,-64600,178296,-277932,-152950,2770350,-10785390,25312650,-26053020,-84847620,576753450,-1856900682,3566658438,-843350102,-24973594296,117328602840,-317641049880,455822225496",
			"name": "Expansion of real root of y + y^2 + y^3 = x.",
			"comment": [
				"Second column of A103778 (inverse of trinomial triangle A071675)."
			],
			"link": [
				"R. J. Mathar, \u003ca href=\"/A103779/b103779.txt\"\u003eTable of n, a(n) for n = 0..103\u003c/a\u003e",
				"Elżbieta Liszewska, Wojciech Młotkowski, \u003ca href=\"https://arxiv.org/abs/1907.10725\"\u003eSome relatives of the Catalan sequence\u003c/a\u003e, arXiv:1907.10725 [math.CO], 2019."
			],
			"formula": [
				"G.f.: -2^(2/3) * ((3*sqrt(3)*sqrt(27*x^2+14*x+3)-27*x-7)^(1/3) -(3*sqrt(3) * sqrt(27*x^2+14*x+3)+27*x+7)^(1/3) +2^(1/3))/6.",
				"a(0)=0, a(1)=1, and for n\u003e=2 a(n) = -sum(k=1..n-1, sum(j=0..k, C(j,n-k-j) * C(k,j)) * a(k)). - _Vladimir Kruchinin_, Apr 08 2011",
				"a(n) = 1/n*sum(k=1..n-1, C(k,n-1-k)*(-1)^k*C(n+k-1,n-1)), a(1)=1. - _Vladimir Kruchinin_, May 12 2012",
				"3*n*(n-1)*a(n) +7*(n-1)*(2*n-3)*a(n-1) +3*(3*n-5)*(3*n-7)*a(n-2)=0. - _R. J. Mathar_, Oct 06 2012",
				"G.f. A(x) satisfies: A(x)^2 = A( x^2 - 2*x*A(x)^2 ). - _Paul D. Hanna_, Apr 17 2016"
			],
			"example": [
				"G.f.: A(x) = x - x^2 + x^3 - 4*x^5 + 14*x^6 - 30*x^7 + 33*x^8 + 55*x^9 - 429*x^10 + 1365*x^11 - 2652*x^12 + 1428*x^13 + 12920*x^14 + ... where A(x + x^2 + x^3) = x."
			],
			"mathematica": [
				"CoefficientList[ InverseSeries[ Series[y + y^2 + y^3, {y, 0, 28}], x], x] (* _Robert G. Wilson v_ *)"
			],
			"program": [
				"(Maxima) a(n):=if n=1 then 1 else -sum(sum(binomial(j,n-k-j) *binomial(k,j),j,0,k)*a(k),k,1,n-1); [_Vladimir Kruchinin_, Apr 08 2011]",
				"(Maxima) a(n):=if n=1 then 1 else 1/n*sum(binomial(k,n-1-k)*(-1)^k *binomial(n+k-1,n-1),k,1,n-1); [_Vladimir Kruchinin_, May 12 2012]",
				"(PARI) Vec(serreverse(x*(1+x+x^2)+O(x^66))) /* _Joerg Arndt_, Aug 19 2012 */",
				"(PARI) /* G.f. A(x) satisfies: A(x)^2 = A( x^2 - 2*x*A(x)^2 ) */",
				"{a(n) = my(A=x+x^2, X=x+x*O(x^n)); for(i=1, n, A = subst(A, x, x^2 - 2*X*A^2)^(1/2) ); polcoeff(A, n)}",
				"for(n=1, 40, print1(a(n), \", \")) \\\\ _Paul D. Hanna_, Apr 17 2016"
			],
			"keyword": "easy,sign",
			"offset": "0,6",
			"author": "_Paul Barry_, Feb 15 2005",
			"references": 4,
			"revision": 44,
			"time": "2019-10-14T00:24:40-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}