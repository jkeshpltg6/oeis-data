{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A300322",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 300322,
			"data": "1,1,2,1,3,1,1,3,6,3,1,2,5,8,12,8,5,2,1,4,9,16,22,28,22,16,9,4,1,1,4,11,21,34,49,60,69,60,49,34,21,11,4,1,2,7,15,31,53,82,114,147,171,186,171,147,114,82,53,31,15,7,2,1,5,13,30,56,95,150,216,293,371,445,495,522,495,445,371,293,216,150,95,56,30,13,5,1",
			"name": "Number T(n,k) of Dyck paths of semilength n such that 2*k is the difference between the area under the right half of the path and the area under the left half of the path; triangle T(n,k), n\u003e=0, -floor(n*(n-1)/6) \u003c= k \u003c= floor(n*(n-1)/6), read by rows.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A300322/b300322.txt\"\u003eRows n = 0..60, flattened\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Lattice_path#Counting_lattice_paths\"\u003eCounting lattice paths\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = T(n,-k).",
				"T(n,A130518(n)) = A177702(n)."
			],
			"example": [
				"               /\\",
				"T(3,-1) = 1:  /  \\/\\",
				".",
				"                /\\",
				"               /  \\     /\\/\\",
				"T(3,0) = 3:   /    \\   /    \\   /\\/\\/\\",
				".",
				"                 /\\",
				"T(3,1) = 1:   /\\/  \\",
				".",
				"Triangle T(n,k) begins:",
				":                             1                            ;",
				":                             1                            ;",
				":                             2                            ;",
				":                         1,  3,  1                        ;",
				":                     1,  3,  6,  3,  1                    ;",
				":                 2,  5,  8, 12,  8,  5,  2                ;",
				":         1,  4,  9, 16, 22, 28, 22, 16,  9,  4,  1        ;",
				":  1, 4, 11, 21, 34, 49, 60, 69, 60, 49, 34, 21, 11, 4, 1  ;"
			],
			"maple": [
				"b:= proc(x, y, v) option remember; expand(",
				"     `if`(min(y, v, x-max(y, v))\u003c0, 0, `if`(x=0, 1, (l-\u003e add(add(",
				"      b(x-1, y+i, v+j)*z^((y-v)/2+(i-j)/4), i=l), j=l))([-1, 1]))))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, z, i), i=ldegree(p)..degree(p)))(",
				"             add(b(n, (n-2*j)$2), j=0..n/2)):",
				"seq(T(n), n=0..12);"
			],
			"mathematica": [
				"b[x_, y_, v_] := b[x, y, v] = Expand[If[Min[y, v, x - Max[y, v]] \u003c 0, 0, If[x == 0, 1, Function[l, Sum[Sum[b[x - 1, y + i, v + j] z^((y - v)/2 + (i - j)/4), {i, l}], {j, l}]][{-1, 1}]]]];",
				"T[n_] := Function[p, Table[Coefficient[p, z, i], {i, Range[Exponent[p, z, Reverse @@ # \u0026], Exponent[p, z]]}]][Sum[b[n, n-2j, n-2j], {j, 0, n/2}]];",
				"Table[T[n], {n, 0, 12}] // Flatten (* _Jean-François Alcover_, May 31 2018, from Maple *)"
			],
			"xref": [
				"Row sums give A000108.",
				"Column k=0 gives A300323.",
				"Cf. A130518, A129182, A177702, A239927, A298645, A300953."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_Alois P. Heinz_, Mar 02 2018",
			"references": 3,
			"revision": 24,
			"time": "2018-05-31T10:09:06-04:00",
			"created": "2018-03-15T08:56:41-04:00"
		}
	]
}