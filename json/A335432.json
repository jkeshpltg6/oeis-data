{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A335432",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 335432,
			"data": "1,1,1,2,1,1,1,6,2,6,2,36,1,6,6,24,1,24,1,240,6,24,2,1800,6,6,6,720,6,1800,1,120,24,6,24,282240,2,6,24,15120,2,5760,6,5040,720,24,6,1451520,2,5040,120,5040,6,1800,720,40320,24,720,2,1117670400,1,6,1800,5040,6",
			"name": "Number of anti-run permutations of the prime indices of Mersenne numbers A000225(n) = 2^n - 1.",
			"comment": [
				"An anti-run is a sequence with no adjacent equal parts.",
				"A prime index of n is a number m such that prime(m) divides n. The multiset of prime indices of n is row n of A112798."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A335432/b335432.txt\"\u003eTable of n, a(n) for n = 1..200\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A335452(A000225(n))."
			],
			"example": [
				"The a(1) = 1 through a(10) = 6 permutations:",
				"  ()  (2)  (4)  (2,3)  (11)  (2,4,2)  (31)  (2,3,7)  (21,4)  (11,2,5)",
				"                (3,2)                       (2,7,3)  (4,21)  (11,5,2)",
				"                                            (3,2,7)          (2,11,5)",
				"                                            (3,7,2)          (2,5,11)",
				"                                            (7,2,3)          (5,11,2)",
				"                                            (7,3,2)          (5,2,11)"
			],
			"mathematica": [
				"primeMS[n_]:=If[n==1,{},Flatten[Cases[FactorInteger[n],{p_,k_}:\u003eTable[PrimePi[p],{k}]]]];",
				"Table[Length[Select[Permutations[primeMS[2^n-1]],!MatchQ[#,{___,x_,x_,___}]\u0026]],{n,0,30}]"
			],
			"program": [
				"(PARI) \\\\ See A335452 for count.",
				"a(n) = {count(factor(2^n-1)[,2])} \\\\ _Andrew Howroyd_, Feb 03 2021"
			],
			"xref": [
				"The version for factorial numbers is A335407.",
				"Anti-run compositions are A003242.",
				"Anti-run patterns are A005649.",
				"Permutations of prime indices are A008480.",
				"Anti-runs are ranked by A333489.",
				"Separable partitions are ranked by A335433.",
				"Inseparable partitions are ranked by A335448.",
				"Anti-run permutations of prime indices are A335452.",
				"Strict permutations of prime indices are A335489.",
				"Cf. A056239, A106351, A112798, A114938, A278990, A292884, A325535, A335125.",
				"Mersenne numbers: A000225, A046051, A046800, A046801, A049093, A059305, A159611, A325610, A325611, A325612, A325625."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Gus Wiseman_, Jul 02 2020",
			"ext": [
				"Terms a(51) and beyond from _Andrew Howroyd_, Feb 03 2021"
			],
			"references": 4,
			"revision": 13,
			"time": "2021-02-03T13:49:25-05:00",
			"created": "2020-07-03T06:58:29-04:00"
		}
	]
}