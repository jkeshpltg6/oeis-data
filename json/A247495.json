{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A247495",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 247495,
			"data": "1,0,1,1,1,1,0,2,2,1,2,4,5,3,1,0,9,14,10,4,1,5,21,42,36,17,5,1,0,51,132,137,76,26,6,1,14,127,429,543,354,140,37,7,1,0,323,1430,2219,1704,777,234,50,8,1,42,835,4862,9285,8421,4425,1514,364,65,9,1",
			"name": "Generalized Motzkin numbers: Square array read by descending antidiagonals, T(n, k) = k!*[x^k](exp(n*x)* BesselI_{1}(2*x)/x), n\u003e=0, k\u003e=0.",
			"comment": [
				"This two-dimensional array of numbers can be seen as a generalization of the Motzkin numbers A001006 for two reasons: The case n=1 reduces to the Motzkin numbers and the columns are the values of the Motzkin polynomials M_{k}(x) = sum_{j=0..k} A097610(k,j)*x^j evaluated at the nonnegative integers."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A247495/b247495.txt\"\u003eAntidiagonals n = 0..139, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = (n*(2*k+1)*T(n,k-1)-(n-2)*(n+2)*(k-1)*T(n,k-2))/(k+2) for k\u003e=2.",
				"T(n,k) = sum_{j=0..floor(k/2)}(n^(k-2*j)*binomial(k,2*j)* binomial(2*j,j)/(j+1).",
				"T(n,k) = n^k*hypergeom([(1-k)/2,-k/2], [2], 4/n^2) for n\u003e0.",
				"T(n,n) = A247496(n).",
				"O.g.f. for row n: (1-n*x-sqrt(((n-2)*x-1)*((n+2)*x-1)))/(2*x^2).",
				"O.g.f. for row n: R(x)/x where R(x) is series reversion of x/(1+n*x+x^2).",
				"E.g.f. for row n: exp(n*x)*hypergeom([],[2],x^2).",
				"O.g.f. for column k: the k-th column consists of the values of the k-th Motzkin polynomial M_{k}(x) evaluated at x = 0,1,2,...; M_{k}(x) = sum_{j=0..k} A097610(k,j)*x^j = sum_{j=0..k} (-1)^j*binomial(k,j)*A001006(j)*(x+1)^(k-j).",
				"O.g.f. for column k: sum_{j=0..k} (-1)^(k+1)*A247497(k,j)/(x-1)^(j+1). - _Peter Luschny_, Dec 14 2014",
				"O.g.f. for row n: 1/(1 - n*x - x^2/(1 - n*x - x^2/(1 - n*x - x^2/(1 - n*x - x^2/(1 - ...))))), a continued fraction. - _Ilya Gutkovskiy_, Sep 21 2017",
				"T(n,k) is the coefficient of x^k in the expansion of 1/(k+1) * (1 + n*x + x^2)^(k+1). - _Seiichi Manyama_, May 07 2019"
			],
			"example": [
				"Square array starts:",
				"[n\\k][0][1] [2]  [3]   [4]   [5]    [6]     [7]      [8]",
				"[0]   1, 0,  1,   0,    2,    0,     5,      0,      14, ...  A126120",
				"[1]   1, 1,  2,   4,    9,   21,    51,    127,     323, ...  A001006",
				"[2]   1, 2,  5,  14,   42,  132,   429,   1430,    4862, ...  A000108",
				"[3]   1, 3, 10,  36,  137,  543,  2219,   9285,   39587, ...  A002212",
				"[4]   1, 4, 17,  76,  354, 1704,  8421,  42508,  218318, ...  A005572",
				"[5]   1, 5, 26, 140,  777, 4425, 25755, 152675,  919139, ...  A182401",
				"[6]   1, 6, 37, 234, 1514, 9996, 67181, 458562, 3172478, ...  A025230",
				"A000012,A001477,A002522,A079908, ...",
				".",
				"Triangular array starts:",
				"              1,",
				"             0, 1,",
				"           1, 1, 1,",
				"          0, 2, 2, 1,",
				"        2, 4, 5, 3, 1,",
				"      0, 9, 14, 10, 4, 1,",
				"   5, 21, 42, 36, 17, 5, 1,",
				"0, 51, 132, 137, 76, 26, 6, 1."
			],
			"maple": [
				"# RECURRENCE",
				"T := proc(n,k) option remember; if k=0 then 1 elif k=1 then n else",
				"(n*(2*k+1)*T(n,k-1)-(n-2)*(n+2)*(k-1)*T(n,k-2))/(k+2) fi end:",
				"seq(print(seq(T(n,k),k=0..9)),n=0..6);",
				"# OGF (row)",
				"ogf := n -\u003e (1-n*x-sqrt(((n-2)*x-1)*((n+2)*x-1)))/(2*x^2):",
				"seq(print(seq(coeff(series(ogf(n),x,12),x,k),k=0..9)),n=0..6);",
				"# EGF (row)",
				"egf := n -\u003e exp(n*x)*hypergeom([],[2],x^2):",
				"seq(print(seq(k!*coeff(series(egf(n),x,k+2),x,k),k=0..9)),n=0..6);",
				"# MOTZKIN polynomial (column)",
				"A097610 := proc(n,k) if type(n-k,odd) then 0 else n!/(k!*((n-k)/2)!^2* ((n-k)/2+1)) fi end: M := (k,x) -\u003e add(A097610(k,j)*x^j,j=0..k):",
				"seq(print(seq(M(k,n),n=0..9)),k=0..6);",
				"# OGF (column)",
				"col := proc(n, len) local G; G := A247497_row(n); (-1)^(n+1)* add(G[k+1]/(x-1)^(k+1), k=0..n); seq(coeff(series(%, x, len+1),x,j), j=0..len) end: seq(print(col(n,8)), n=0..6); # _Peter Luschny_, Dec 14 2014"
			],
			"mathematica": [
				"T[0, k_] := If[EvenQ[k], CatalanNumber[k/2], 0];",
				"T[n_, k_] := n^k*Hypergeometric2F1[(1 - k)/2, -k/2, 2, 4/n^2];",
				"Table[T[n - k, k], {n, 0, 10}, {k, n, 0, -1}] // Flatten (* _Jean-François Alcover_, Nov 03 2017 *)"
			],
			"program": [
				"(Sage)",
				"def A247495(n,k):",
				"    if n==0: return(k//2+1)*factorial(k)/factorial(k//2+1)^2 if is_even(k) else 0",
				"    return n^k*hypergeometric([(1-k)/2,-k/2],[2],4/n^2).simplify()",
				"for n in (0..7): print([A247495(n,k) for k in range(11)])"
			],
			"xref": [
				"Main diagonal gives A247496.",
				"Cf. A126120, A001006, A000108, A002212, A005572, A182401, A025230, A002522, A079908, A055151, A097610, A247497, A306684."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Peter Luschny_, Dec 11 2014",
			"references": 5,
			"revision": 42,
			"time": "2020-03-07T06:44:44-05:00",
			"created": "2014-12-12T21:22:54-05:00"
		}
	]
}