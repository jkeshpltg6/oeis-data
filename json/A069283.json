{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A069283",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 69283,
			"data": "0,0,0,1,0,1,1,1,0,2,1,1,1,1,1,3,0,1,2,1,1,3,1,1,1,2,1,3,1,1,3,1,0,3,1,3,2,1,1,3,1,1,3,1,1,5,1,1,1,2,2,3,1,1,3,3,1,3,1,1,3,1,1,5,0,3,3,1,1,3,3,1,2,1,1,5,1,3,3,1,1,4,1,1,3,3,1,3,1,1,5,3,1,3,1,3,1,1,2,5,2",
			"name": "a(n) = -1 + number of odd divisors of n.",
			"comment": [
				"Number of nontrivial ways to write n as sum of at least 2 consecutive integers. That is, we are not counting the trivial solution n=n. E.g., a(9)=2 because 9 = 4 + 5 and 9 = 2 + 3 + 4. a(8)=0 because there are no integers m and k such that m + (m+1) + ... + (m+k-1) = 8 apart from k=1, m=8. - _Alfred Heiligenbrunner_, Jun 07 2004",
				"Also number of sums of sequences of consecutive positive integers excluding sequences of length 1 (e.g., 9 = 2+3+4 or 4+5 so a(9)=2). (Useful for cribbage players.) - Michael Gilleland, Dec 29 2002",
				"Let M be any positive integer. Then a(n) = number of proper divisors of M^n + 1 of the form M^k + 1.",
				"This sequence gives the distinct differences of triangular numbers Ti giving n : n = Ti - Tj; none if n = 2^k. If factor a = n or a \u003e (n/a - 1)/2 : i = n/a + (a - 1)/2; j = n/a - (a+1)/2. Else : i = n/2a + (2a - 1)/2; j = n/2a - (2a - 1)/2. Examples: 7 is prime; 7 = T4 - T2 = (1 + 2 + 3 + 4) - (1 + 2) (a = 7; n/a = 1). The odd factors of 35 are 35, 7 and 5; 35 = T18 - T16 (a = 35) = T8 - T1 (a = 7) = T5 - T7 (a = 5). 144 = T20 - T11 (a = 9) = T49 - T46 (a = 3). - M. Dauchez (mdzzdm(AT)yahoo.fr), Oct 31 2005",
				"Also number of partitions of n into the form 1 + 2 + ...( k - 1) + k + k + ... + k for some k \u003e= 2. Example: a(9) = 2 because we have [2, 2, 2, 2, 1] and [3, 3, 2, 1]. - _Emeric Deutsch_, Mar 04 2006",
				"a(n) is the number of nontrivial runsum representations of n, and is also known as the politeness of n. - _Ant King_, Nov 20 2010",
				"Also number of nonpowers of 2 dividing n, divided by the number of powers of 2 dividing n, n \u003e 0. - _Omar E. Pol_, Aug 24 2019",
				"a(n) only depends on the prime signature of A000265(n). - _David A. Corneth_, May 30 2020, corrected by _Charles R Greathouse IV_, Oct 31 2021"
			],
			"reference": [
				"Graham, Knuth and Patashnik, Concrete Mathematics, 2nd ed. (Addison-Wesley, 1994), see exercise 2.30 on p. 65."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A069283/b069283.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Tom M. Apostol, \u003ca href=\"http://www.jstor.org/stable/3620570\"\u003eSums of Consecutive Positive Integers\u003c/a\u003e, The Mathematical Gazette, Vol. 87, No. 508, (March 2003), pp. 98-101.",
				"A. Heiligenbrunner, \u003ca href=\"http://ah9.at/ahsummen.htm\"\u003eSum of adjacent numbers\u003c/a\u003e (in German).",
				"Henri Picciotto, \u003ca href=\"http://www.mathedpage.org/teachers/staircases.pdf\"\u003eStaircases\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Polite_number\"\u003ePolite Number\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 0 if and only if n = 2^k.",
				"a(n) = A001227(n)-1.",
				"a(n) = 1 if and only if n = 2^k * p where k \u003e= 0 and p is an odd prime. - _Ant King_, Nov 20 2010",
				"G.f.: sum(k\u003e=2, x^(k(k + 1)/2)/(1 - x^k) ). - _Emeric Deutsch_, Mar 04 2006",
				"If n = 2^k p1^b1 p2^b2 ... pr^br, then a(n) = (1 + b1)(1 + b2) ... (1 + br) - 1. - _Ant King_, Nov 20 2010",
				"Dirichlet g.f.: (zeta(s)*(1-1/2^s) - 1)*zeta(s). - _Geoffrey Critzer_, Feb 15 2015",
				"a(n) = (A000005(n) - A001511(n))/A001511(n) = A326987(n)/A001511(n), with n \u003e 0 in both formulas. - _Omar E. Pol_, Aug 24 2019",
				"G.f.: Sum_{k\u003e=1} x^(3*k) / (1 - x^(2*k)). - _Ilya Gutkovskiy_, May 30 2020",
				"From _David A. Corneth_, May 30 2020: (Start)",
				"a(2*n) = a(n).",
				"a(n) = A001227(A000265(n)) - 1. (End)"
			],
			"example": [
				"a(14) = 1 because the divisors of 14 are 1, 2, 7, 14, and of these, two are odd, 1 and 7, and -1 + 2 = 1.",
				"a(15) = 3 because the divisors of 15 are 1, 3, 5, 15, and of these, all four are odd, and -1 + 4 = 3.",
				"a(16) = 0 because 16 has only one odd divisor, and -1 + 1 = 0.",
				"Using Ant King's formula: a(90) = 5 as 90 = 2^1 * 3^2 * 5^1, so a(90) = (1 + 2) * (1 + 1) - 1 = 5. - _Giovanni Ciriani_, Jan 12 2013",
				"x^3 + x^5 + x^6 + x^7 + 2*x^9 + x^10 + x^11 + x^12 + x^13 + x^14 + ...",
				"a(120) = 3 as the odd divisors of 120 are the odd divisors of 15 as 120 = 15*2^3. 15 has 4 odd divisors so that gives a(120) = 4 - 1 = 3. - _David A. Corneth_, May 30 2020"
			],
			"maple": [
				"g:=sum(x^(k*(k+1)/2)/(1-x^k),k=2..20): gser:=series(g,x=0,115): seq(coeff(gser,x,n),n=0..100); # _Emeric Deutsch_, Mar 04 2006",
				"A069283 := proc(n)",
				"    A001227(n)-1 ;",
				"end proc: # _R. J. Mathar_, Jun 18 2015"
			],
			"mathematica": [
				"g[n_] := Module[{dL = Divisors[2n], dP}, dP = Transpose[{dL, 2n/dL}]; Select[dP, ((1 \u003c #[[1]] \u003c #[[2]]) \u0026\u0026 (Mod[ #[[1]] - #[[2]], 2] == 1)) \u0026] ]; Table[Length[g[n]], {n, 1, 100}]",
				"Table[Length[Select[Divisors[k], OddQ[#] \u0026]] - 1, {k, 100}] (* _Ant King_, Nov 20 2010 *)",
				"Join[{0}, Times @@@ (#[[All, 2]] \u0026 /@ Replace[FactorInteger[Range[2, 50]], {2, a_} -\u003e {2, 0}, Infinity] + 1) - 1] (* _Horst H. Manninger_, Oct 30 2021 *)"
			],
			"program": [
				"(Haskell)",
				"a069283 0 = 0",
				"a069283 n = length $ tail $ a182469_row n",
				"-- _Reinhard Zumkeller_, May 01 2012",
				"(PARI) {a(n) = if( n\u003c1, 0, sumdiv( n, d, d%2) - 1)} /* _Michael Somos_, Aug 07 2013 */",
				"(PARI) a(n) = numdiv(n \u003e\u003e valuation(n, 2)) - 1 \\\\ _David A. Corneth_, May 30 2020",
				"(MAGMA) [0] cat [-1 + #[d:d in Divisors(n)| IsOdd(d)]:n in [1..100]]; // _Marius A. Burtea_, Aug 24 2019"
			],
			"xref": [
				"Cf. A000265, A001227, A062397, A057934, A138591, A182469.",
				"Cf. A095808 (sums of ascending and descending consecutive integers)."
			],
			"keyword": "nonn,easy",
			"offset": "0,10",
			"author": "_Reinhard Zumkeller_, Mar 13 2002",
			"ext": [
				"Edited by _Vladeta Jovovic_, Mar 25 2002"
			],
			"references": 17,
			"revision": 110,
			"time": "2021-10-31T22:52:50-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}