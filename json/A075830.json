{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A075830",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 75830,
			"data": "0,1,1,5,7,47,37,319,533,1879,1627,20417,18107,263111,237371,52279,95549,1768477,1632341,33464927,155685007,166770367,156188887,3825136961,3602044091,19081066231,18051406831,57128792093,7751493599",
			"name": "Let u(1) = x and u(n+1) = (n^2/u(n)) + 1 for n \u003e= 1; then a(n) is such that u(n) = (b(n)*x + c(n))/(a(n)*x + d(n)) (in lowest terms) and a(n), b(n), c(n), d(n) are positive integers.",
			"comment": [
				"For x real \u003c\u003e 1 - 1/log(2), Lim_{n -\u003e infinity} abs(u(n)-n) = abs((x - 1)/(1 + (x - 1)*log(2))). [Corrected by _Petros Hadjicostas_, May 18 2020]",
				"From _Petros Hadjicostas_, May 05 2020: (Start)",
				"Given x \u003e 0, u(n) = (A075827(n)*x + A075828(n))/(a(n)*x + A075829(n)) = (b(n)*x + c(n))/(a(n)*x + d(n)) with gcd(gcd(b(n), c(n)), gcd(a(n), d(n))) = 1 for each n \u003e= 1.",
				"Conjecture 1: Define the sequences (A(n): n \u003e= 1) and (B(n): n \u003e= 1) by A(n+1) = n^2/A(n) + 1 for n \u003e= 2 with A(1) = infinity and A(2) = 1, and B(n+1) = n^2/B(n) + 1 for n \u003e= 3 with B(1) = 0, B(2) = infinity, and B(3) = 1. Then a(n) = denominator(A(n)), b(n) = numerator(A(n)), c(n) = numerator(B(n)), and d(n) = denominator(B(n)) (assuming infinity = 1/0). Also, gcd(a(n), d(n)) = 1.",
				"In 2002, _Michael Somos_ claimed that d(n) = A024168(n-1)/gcd(A024168(n-1), A024168(n)) for n \u003e= 2. In 2006, _N. J. A. Sloane_ claimed that a(n) = A058313(n-1) for n \u003e= 2 while _Alexander Adamchuk_ claimed that d(n) = A058312(n-1) - A058313(n-1) for n \u003e= 2.",
				"Conjecture 2: a(n) = A024167(n-1)/gcd(A024167(n-1), A024167(n)).",
				"Conjecture 3: b(p) = a(p+1) for p = 1 or prime. In general, it seems that b(n) = A048671(n)*a(n+1) for all n for which A048671(n) \u003c n.",
				"Conjecture 4: c(n) = n*(a(n) + d(n)) - b(n) for n \u003e= 1. (End)",
				"All conjectures are proved in the link below except for the second part of Conjecture 3. - _Petros Hadjicostas_, May 21 2020"
			],
			"link": [
				"Petros Hadjicostas, \u003ca href=\"/A075829/a075829.pdf\"\u003eProofs of various results about the sequence u(n)\u003c/a\u003e, 2020."
			],
			"program": [
				"(PARI) u(n)=if(n\u003c2,x,(n-1)^2/u(n-1)+1);",
				"a(n)=polcoeff(denominator(u(n)),1,x);"
			],
			"xref": [
				"Apart from the leading term, same as A058313.",
				"Cf. A075827 (= b), A075828 (= c), A075829 (= d).",
				"Cf. A024167, A024168, A048671, A058312."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Benoit Cloitre_, Oct 14 2002",
			"ext": [
				"Name edited by _Petros Hadjicostas_, May 04 2020"
			],
			"references": 6,
			"revision": 63,
			"time": "2020-05-21T10:15:29-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}