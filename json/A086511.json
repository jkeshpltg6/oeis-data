{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A086511",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 86511,
			"data": "2,9,28,121,336,1081,3060,8409,23527,64541,175198,480865,1304499,3523885,9557956,25874753,70115413,189961183,514272412,1394193581,3779849620,10246935645,27788566030,75370121161,204475052376,554805820453,1505578023622,4086199301997",
			"name": "a(n) is the smallest integer k \u003e 1 such that k \u003e n * pi(k), where pi() denotes the prime counting function.",
			"comment": [
				"a(n) is bounded above by the sequence A038623, in which k is required to be prime. In addition, the sequence pi(a(n)) = {1, 4, 9, 30, 67, 180, 437, 1051, ...} closely resembles the sequence A038624, in which the n-th term is the minimal t such that k \u003e= n * pi(k) for every k satisfying pi(k) = t. If we were to make the inequality in A038624 strict, the resulting sequence would provide an upper bound for pi(a(n)). Sequences A038625, A038626 and A038627 focus on the equality k = n * pi(k): as we would expect, a(n) follows A038625 very closely for large n."
			],
			"link": [
				"Giovanni Resta, \u003ca href=\"/A086511/b086511.txt\"\u003eTable of n, a(n) for n = 1..50\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PrimeCountingFunction.html\"\u003ePrime Counting Function\u003c/a\u003e."
			],
			"formula": [
				"Heuristically, for large n, a(n) ~= 3.0787*(2.70888^n) [error \u003c 0.05% for 15 \u003c= n \u003c= 20].",
				"From _Nathaniel Johnston_, Apr 10 2011: (Start)",
				"a(n) \u003e= exp(n/2 + sqrt(n^2 + 4n)/2), n \u003e= 6.",
				"a(n) = A038625(n) + m(n)*n + 1 for some m(n) \u003e= 0. For n = 2, 3, 4, ..., m(n) = 3, 0, 6, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, ...",
				"(End)"
			],
			"example": [
				"Consider the pairs (k, pi(k)) for k \u003e 1. The inequality k \u003e 1 * pi(k) is first satisfied at k = 2 and so a(1) = 2. Similarly, the inequality k \u003e 2 * pi(k) is first satisfied at k = 9 and so a(2) = 9."
			],
			"program": [
				"(PARI) a(n) = { k = 2; while (k \u003c= n*primepi(k), k++); return (k);} \\\\ _Michel Marcus_, Jun 19 2013"
			],
			"xref": [
				"Cf. A038623, A038624, A038625, A038626, A038627."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "Tim Paulden (timmy(AT)cantab.net), Sep 09 2003",
			"ext": [
				"a(21)-a(26) from _Nathaniel Johnston_, Apr 10 2011",
				"Corrected a(26) and a(27)-a(28) from _Giovanni Resta_, Sep 01 2018",
				"a(29)-a(50) obtained from the values of A038625 computed by _Jan Büthe_. - _Giovanni Resta_, Sep 01 2018"
			],
			"references": 1,
			"revision": 21,
			"time": "2020-05-30T04:13:55-04:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}