{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A058690",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 58690,
			"data": "1,0,1,2,3,3,5,5,8,9,12,14,19,22,28,33,41,48,60,69,84,98,117,136,164,188,222,256,301,345,404,462,537,614,709,807,931,1056,1211,1374,1569,1774,2021,2280,2588,2916,3299,3708,4189,4697,5290,5926,6656,7442,8344",
			"name": "McKay-Thompson series of class 47A for the Monster group.",
			"comment": [
				"Also McKay-Thompson series of class 47B for Monster. - _Michel Marcus_, Feb 24 2014",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (47 t)) = f(t) where q = exp(2 Pi i t). - _Michael Somos_, Sep 06 2018"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A058690/b058690.txt\"\u003eTable of n, a(n) for n = -1..1500\u003c/a\u003e",
				"D. Ford, J. McKay and S. P. Norton, \u003ca href=\"http://dx.doi.org/10.1080/00927879408825127\"\u003eMore on replicable functions\u003c/a\u003e, Commun. Algebra 22, No. 13, 5175-5193 (1994).",
				"David A. Madore, \u003ca href=\"http://mathforum.org/kb/thread.jspa?forumID=253\u0026amp;threadID=1602206\u0026amp;messageID=5836094\"\u003eCoefficients of Moonshine (McKay-Thompson) series\u003c/a\u003e, The Math Forum",
				"\u003ca href=\"/index/Mat#McKay_Thompson\"\u003eIndex entries for McKay-Thompson series for Monster simple group\u003c/a\u003e"
			],
			"formula": [
				"a(n) ~ exp(4*Pi*sqrt(n/47)) / (sqrt(2) * 47^(1/4) * n^(3/4)). - _Vaclav Kotesovec_, Jul 06 2018"
			],
			"example": [
				"T47A = 1/q + q + 2*q^2 + 3*q^3 + 3*q^4 + 5*q^5 + 5*q^6 + 8*q^7 + 9*q^8 + ..."
			],
			"mathematica": [
				"eta[q_]:= q^(1/24)*QPochhammer[q]; Theta[a_, b_, c_]:= Sum[q^((a*n^2 + b*n*m + c*m^2)/2), {n, -50, 50}, {m, -50, 50}]; a:= CoefficientList[ Series[q*(Theta[2, 2, 24] - Theta[4, 2, 12])/(2*eta[q]*eta[q^47]), {q, 0, 100}], q]; Table[a[[n]], {n,1,80}] (* _G. C. Greubel_, Jul 05 2018 *)",
				"a[ n_] := With[ {T1 = QPochhammer[ q^#] QPochhammer[ q^(47 #)] \u0026, T2 = EllipticTheta[ 2, 0, q^#] EllipticTheta[ 2, 0, q^(47 #)] \u0026, T3 = EllipticTheta[ 3, 0, q^#] EllipticTheta[ 3, 0, q^(47 #)] \u0026}, SeriesCoefficient[ (T3[1] + T2[1]- T3[2] - T2[2] - T2[1/2]/2) / (2 q^2 T1[1]), {q, 0, n}]]; (* _Michael Somos_, Sep 07 2018 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c-1, 0, n++; A = x * O(x^n); polcoeff( Ser( Vec( qfrep([2, 1; 1, 24], n+1, 1)) - Vec(qfrep([4, 1; 1, 12], n+1, 1))) / (eta(x + A) * eta(x^47 + A)), n))}; /* _Michael Somos_, Sep 06 2018 */"
			],
			"xref": [
				"Cf. A000521, A007240, A014708, A007241, A007267, A045478, etc."
			],
			"keyword": "nonn",
			"offset": "-1,4",
			"author": "_N. J. A. Sloane_, Nov 27 2000",
			"ext": [
				"More terms from _Michel Marcus_, Feb 24 2014"
			],
			"references": 1,
			"revision": 25,
			"time": "2018-09-07T22:09:27-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}