{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A328029",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 328029,
			"data": "1,2,1,1,2,3,2,1,4,3,1,2,4,3,5,2,1,6,3,5,4,1,2,4,6,5,3,7,2,1,5,4,8,3,6,7,1,2,4,8,6,7,5,3,9,1,2,10,7,8,3,9,5,4,6,1,2,6,11,7,9,4,8,5,3,10,2,1,7,3,12,5,9,10,4,6,11,8,1,2,12,13,5,10,6,11,3,9,8,4,7",
			"name": "Lexicographically earliest permutation of [1,2,...,n] maximizing the determinant of an n X n circulant matrix that uses this permutation as first row, written as triangle T(n,k), k \u003c= n.",
			"comment": [
				"For n \u003c= 9 the corresponding circulant matrices are n X n Latin squares with maximum determinant A309985(n). It is conjectured that this also holds for n \u003e 9. See Mathematics Stack Exchange link."
			],
			"link": [
				"Hugo Pfoertner, \u003ca href=\"/A328029/b328029.txt\"\u003eTable of n, a(n) for n = 1..120\u003c/a\u003e, rows 1..15 of triangle, flattened",
				"Mathematics Stack Exchange, \u003ca href=\"https://math.stackexchange.com/questions/885481/maximum-determinant-of-latin-squares\"\u003eMaximum determinant of Latin squares\u003c/a\u003e, (2014), (2016).",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Circulant_matrix\"\u003eCirculant matrix\u003c/a\u003e."
			],
			"example": [
				"The triangle starts",
				"  1;",
				"  2,  1;",
				"  1,  2,  3;",
				"  2,  1,  4,  3;",
				"  1,  2,  4,  3,  5;",
				"  2,  1,  6,  3,  5,  4;",
				"  1,  2,  4,  6,  5,  3,  7;",
				"  2,  1,  5,  4,  8,  3,  6,  7;",
				"  1,  2,  4,  8,  6,  7,  5,  3,  9;",
				"  1,  2, 10,  7,  8,  3,  9,  5,  4,  6;",
				".",
				"The 4th row of the triangle T(4,1)..T(4,4) = a(7)..a(10) is [2,1,4,3] because this is the lexicographically earliest permutation of [1,2,3,4] producing a circulant 4 X 4 matrix with maximum determinant A328030(4) = 160.",
				"  [2, 1, 4, 3;",
				"   3, 2, 1, 4;",
				"   4, 3, 2, 1;",
				"   1, 4, 3, 2].",
				"All lexicographically earlier permutations lead to smaller determinants, with [1,2,3,4] and [1,4,3,2] producing determinants = -160."
			],
			"mathematica": [
				"f[n_] := (p = Permutations[Table[i, {i, n}]]; L = Length[p];",
				"  det = Max[",
				"    Table[Det[Reverse /@ Partition[p[[i]], n, 1, {1, 1}]], {i, 1,",
				"      L}]]; mat =",
				"   Table[Reverse /@ Partition[p[[i]], n, 1, {1, 1}], {i, 1,",
				"     L}]); n = 1; While[n \u003c= 10, ClearSystemCache[[]]; f[n];",
				"triangle = Parallelize[Select[mat, Max[Det[#]] == det \u0026]];",
				"Print[SortBy[triangle, Less][[1]][[1]]]; n++]; (* _Kebbaj Mohamed Reda_, Dec 03 2019 *)",
				"(* alternate program *)",
				"n1 = DialogInput[{name = \"\"},",
				"  Column[{\"Input n :\", InputField[Dynamic[name], String],",
				"    Button[\"Proceed\", DialogReturn[name], ImageSize -\u003e Automatic]}]];",
				"f[n_] := (p = Permutations[Table[i, {i, n}]]; L = Length[p];",
				"  det = Max[",
				"    Table[Det[Reverse /@ Partition[p[[i]], n, 1, {1, 1}]], {i, 1,",
				"      L}]]; mat =",
				"   Table[Reverse /@ Partition[p[[i]], n, 1, {1, 1}], {i, 1,",
				"     L}]); n = 1; sequance = {}; While[n \u003c= ToExpression[n1], ClearSystemCache[[]];",
				"f[n]; triangle = Parallelize[Select[mat, Max[Det[#]] == det \u0026]];",
				"AppendTo[sequance, SortBy[triangle, Less][[1]][[1]]];",
				"n++]; Flatten[sequance] (* _Kebbaj Mohamed Reda_, Dec 03 2019 *)"
			],
			"xref": [
				"Cf. A301371, A309985, A328030, A328031, A328062."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Hugo Pfoertner_, Oct 02 2019",
			"references": 4,
			"revision": 33,
			"time": "2019-12-05T05:32:41-05:00",
			"created": "2019-10-02T20:08:51-04:00"
		}
	]
}