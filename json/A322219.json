{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A322219",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 322219,
			"data": "1,1,1,1,10,1,4,1,35,91,85,20,16,24,1,84,966,1324,2737,632,1200,288,384,128,192,1,165,5082,26818,50941,64329,69816,49872,27568,19776,16448,10176,5760,3840,1280,1920,1,286,18447,279136,954239,2550054,2455233,4013788,2929104,3264864,1176640,1815552,834752,731136,394752,491264,141056,164352,69120,46080,15360,23040,1,455,53053,1780207,15627183,51699869,128611679,187372653,213804652,257006976,245800968,195109120,161177792,123750592,83792000,69316224,52893696,35140992,28215808,18433536,12687360,8411648,5945856,2673664,2300928,967680,645120,215040,322560",
			"name": "E.g.f.: S(x,q) = Integral C(x,q) * C(q*x,q) dx, such that C(x,q)^2 - S(x,q)^2 = 1, where S(x,q) = Sum_{n\u003e=0} sum_{k=0..n*(n+1)/2} T(n,k)*x^n*y^k/n!, as an irregular triangle of coefficients T(n,k) read by rows.",
			"comment": [
				"Compare to Jacobi's elliptic function sn(x,k) = Integral cn(x,k)*dn(x,k) dx such that cn(x,k)^2 + sn(x,k)^2 = 1 and dn(x,k)^2 + k^2*sn(x,k)^2 = 1.",
				"Right border equals A002866.",
				"Row sums equal the tangent numbers (A000182).",
				"Last n terms in row n of this triangle and of triangle A322218 are equal for n\u003e0."
			],
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A322219/b322219.txt\"\u003eTable of n, a(n) for n = 0..4990, as a flattened irregular triangle read by rows 0..30.\u003c/a\u003e"
			],
			"formula": [
				"E.g.f. S(x,q) and related series C(x,q) satisfy:",
				"(1) C(x,q)^2 - S(x,q)^2 = 1.",
				"(2) C(x,q) = 1 + Integral S(x,q) * C(q*x,q) dx.",
				"(3) S(x,q) = Integral C(x,q) * C(q*x,q) dx.",
				"(4a) C(x,q) + S(x,q) = exp( Integral C(q*x,q) dx ).",
				"(4b) C(x,q) = cosh( Integral C(q*x,q) dx ).",
				"(4c) S(x,q) = sinh( Integral C(q*x,q) dx ).",
				"(5) C(q*x,q) = 1 + q * Integral S(q*x,q) * C(q^2*x,q) dx.",
				"(6) S(q*x,q) = q * Integral C(q*x,q) * C(q^2*x,q) dx.",
				"(7a) C(q*x,q) + S(q*x,q) = exp( q * Integral C(q^2*x,q) dx ).",
				"(7b) C(q*x,q) = cosh( q * Integral C(q^2*x,q) dx ).",
				"(7c) S(q*x,q) = sinh( q * Integral C(q^2*x,q) dx ).",
				"PARTICULAR ARGUMENTS.",
				"S(x,q=0) = sinh(x).",
				"S(x,q=1) = tan(x).",
				"S(x,q=i) = -i * sl(i*x), where sl(x) is the sine lemniscate function (A104203).",
				"FORMULAS FOR TERMS.",
				"T(n, n*(n+1)/2) = 2^(n-1)*n! for n \u003e= 1.",
				"T(n, n*(n+1)/2 - k) = A322218(n, n*(n-1)/2 - k) for k = 0..n-1, n \u003e 0.",
				"Sum_{k=0..n*(n+1)/2} T(n,k) = A000182(n+1) for n \u003e= 0.",
				"Sum_{k=0..n*(n+1)/2} T(n,k)*(-1)^k = A104203(2*n+1) for n \u003e= 0."
			],
			"example": [
				"E.g.f. S(x,q) = Sum_{n\u003e=0} sum_{k=0..n*(n+1)/2} T(n,k)*x^(2*n+1)*q^(2*k)/(2*n+1)! starts",
				"S(x,q) = x + (q^2 + 1)*x^3/3! + (4*q^6 + q^4 + 10*q^2 + 1)*x^5/5! + (24*q^12 + 16*q^10 + 20*q^8 + 85*q^6 + 91*q^4 + 35*q^2 + 1)*x^7/7! + (192*q^20 + 128*q^18 + 384*q^16 + 288*q^14 + 1200*q^12 + 632*q^10 + 2737*q^8 + 1324*q^6 + 966*q^4 + 84*q^2 + 1)*x^9/9! + (1920*q^30 + 1280*q^28 + 3840*q^26 + 5760*q^24 + 10176*q^22 + 16448*q^20 + 19776*q^18 + 27568*q^16 + 49872*q^14 + 69816*q^12 + 64329*q^10 + 50941*q^8 + 26818*q^6 + 5082*q^4 + 165*q^2 + 1)*x^11/11! + ...",
				"such that S(x,q) = sinh( Integral C(q*x,q) dx ) and C(x,q)^2 = 1 + S(x,q)^2.",
				"This irregular triangle of coefficients T(n,k) of x^(2*n+1)*q^(2*k)/(2*n+1)! in S(x,q) begins:",
				"1;",
				"1, 1;",
				"1, 10, 1, 4;",
				"1, 35, 91, 85, 20, 16, 24;",
				"1, 84, 966, 1324, 2737, 632, 1200, 288, 384, 128, 192;",
				"1, 165, 5082, 26818, 50941, 64329, 69816, 49872, 27568, 19776, 16448, 10176, 5760, 3840, 1280, 1920;",
				"1, 286, 18447, 279136, 954239, 2550054, 2455233, 4013788, 2929104, 3264864, 1176640, 1815552, 834752, 731136, 394752, 491264, 141056, 164352, 69120, 46080, 15360, 23040;",
				"1, 455, 53053, 1780207, 15627183, 51699869, 128611679, 187372653, 213804652, 257006976, 245800968, 195109120, 161177792, 123750592, 83792000, 69316224, 52893696, 35140992, 28215808, 18433536, 12687360, 8411648, 5945856, 2673664, 2300928, 967680, 645120, 215040, 322560;",
				"1, 680, 129948, 8212360, 163115238, 1001312104, 3705217660, 7815443320, 15434182497, 17298854576, 23429393056, 21144463040, 25624143104, 18454639872, 18756800128, 12036914176, 12076688384, 7122865152, 7609525248, 4420732928, 4042876928, 2553473024, 2465701888, 1353586688, 1234018304, 619528192, 587358208, 311279616, 255467520, 117383168, 108036096, 42778624, 36814848, 15482880, 10321920, 3440640, 5160960; ...",
				"RELATED SERIES.",
				"C(x,q) = 1 + x^2/2! + (4*q^2 + 1)*x^4/4! + (24*q^6 + 16*q^4 + 20*q^2 + 1)*x^6/6! + (192*q^12 + 128*q^10 + 384*q^8 + 288*q^6 + 336*q^4 + 56*q^2 + 1)*x^8/8! + (1920*q^20 + 1280*q^18 + 3840*q^16 + 5760*q^14 + 10176*q^12 + 5888*q^10 + 12736*q^8 + 6448*q^6 + 2352*q^4 + 120*q^2 + 1)*x^10/10! + (23040*q^30 + 15360*q^28 + 46080*q^26 + 69120*q^24 + 164352*q^22 + 141056*q^20 + 341504*q^18 + 294912*q^16 + 431616*q^14 + 385472*q^12 + 472704*q^10 + 214016*q^8 + 93280*q^6 + 10032*q^4 + 220*q^2 + 1)*x^12/12! + ...",
				"such that C(x,q) = cosh( Integral C(q*x,q) dx )."
			],
			"mathematica": [
				"rows = 8; m = 2 rows; s[x_, _] = x; c[_, _] = 1; Do[s[x_, q_] = Integrate[c[x, q] c[q x, q] + O[x]^m // Normal, x]; c[x_, q_] = 1 + Integrate[s[x, q] c[q x, q] + O[x]^m // Normal, x], {m}];",
				"CoefficientList[#, q^2]\u0026 /@ (CoefficientList[s[x, q], x] Range[0, m-1]!) // DeleteCases[#, {}]\u0026 // Flatten (* _Jean-François Alcover_, Dec 17 2018 *)"
			],
			"program": [
				"(PARI) {T(n,k) = my(S=x,C=1); for(i=1,2*n,",
				"S = intformal(C*subst(C,x,q*x) +O(x^(2*n+1)));",
				"C = 1 + intformal(S*subst(C,x,q*x)));",
				"(2*n+1)!*polcoeff( polcoeff(S,2*n+1,x),2*k,q)}",
				"for(n=0,10, for(k=0,n*(n+1)/2, print1( T(n,k),\", \"));print(\"\"))"
			],
			"xref": [
				"Cf. A322218 (C(x,q)), A000182 (row sums), A104203, A002866."
			],
			"keyword": "nonn,tabf",
			"offset": "0,5",
			"author": "_Paul D. Hanna_, Dec 16 2018",
			"references": 2,
			"revision": 49,
			"time": "2019-03-24T19:42:51-04:00",
			"created": "2018-12-16T23:48:18-05:00"
		}
	]
}