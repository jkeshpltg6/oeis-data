{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A050931",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 50931,
			"data": "7,13,14,19,21,26,28,31,35,37,38,39,42,43,49,52,56,57,61,62,63,65,67,70,73,74,76,77,78,79,84,86,91,93,95,97,98,103,104,105,109,111,112,114,117,119,122,124,126,127,129,130,133,134,139,140,143,146,147,148,151",
			"name": "Numbers having a prime factor congruent to 1 mod 6.",
			"comment": [
				"Original definition: Solutions c of cot(2*Pi/3)*(-(a+b+c)*(-a+b+c)*(-a+b-c)*(a+b-c))^(1/2)=a^2+b^2-c^2, c\u003ea,b integers.",
				"Note cot(2*Pi/3) = -1/sqrt(3).",
				"Also the c-values for solutions to c^2 = a^2 + ab + b^2 in positive integers. Also the numbers which occur as the longest side of some triangle with integer sides and a 120-degree angle. - _Paul Boddington_, Nov 05 2007",
				"The sequence can also be defined as the numbers w which are Heronian means of two distinct positive integers u and v, i.e., w = [u+sqrt(uv)+v]/3. E.g., 28 is the Heronian mean of 4 and 64 (and also of 12 and 48). - _Pahikkala Jussi_, Feb 16 2008",
				"From _Jean-Christophe Hervé_, Nov 24 2013: (Start)",
				"This sequence is the analog of hypotenuse numbers A009003 for triangles with integer sides and a 120-degree angle. There are two integers a and b \u003e 0 such that a(n)^2 = a^2 + ab + b^2, and a, b and a(n) are the sides of the triangle: a(n) is the sequence of lengths of the longest side of these triangles. A004611 is the same for primitive triangles.",
				"a and b cannot be equal because sqrt(3) is not rational. Then the values a(n) are such that a(n)^2 is in A024606. It follows that a(n) is the sequence of multiples of primes of form 6k+1 A002476.",
				"The sequence is closed under multiplication. The primitive elements are those with exactly one prime divisor of the form 6k+1 with multiplicity one, which are also those for which there exists a unique 120-degree integer triangle with its longest side equals to a(n).",
				"(End)"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A050931/b050931.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Bojan Mohar, \u003ca href=\"http://arxiv.org/abs/1505.03373\"\u003eHermitian adjacency spectrum and switching equivalence of mixed graphs\u003c/a\u003e, arXiv preprint arXiv:1505.03373 [math.CO], 2015.",
				"Planet Math, \u003ca href=\"http://planetmath.org/TruncatedCone\"\u003eTruncated cone\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Triangle.html\"\u003eTriangle\u003c/a\u003e - see especially (19)",
				"\u003ca href=\"/index/Aa#A2\"\u003eIndex entries for sequences related to A2 = hexagonal = triangular lattice\u003c/a\u003e"
			],
			"formula": [
				"A005088(a(n)) \u003e 0. Terms are obtained by the products A230780(k)*A004611(p) for k, p \u003e 0, ordered by increasing values. - _Jean-Christophe Hervé_, Nov 24 2013",
				"cot(2*Pi/3) = -1/sqrt(3) = -0.57735... = - A020760. - _M. F. Hasler_, Aug 18 2016"
			],
			"mathematica": [
				"Select[Range[2,200],MemberQ[Union[Mod[#,6]\u0026/@FactorInteger[#][[All,1]]],1]\u0026] (* _Harvey P. Dale_, Aug 24 2019 *)"
			],
			"program": [
				"(Haskell)",
				"a050931 n = a050931_list !! (n-1)",
				"a050931_list = filter (any (== 1) . map (flip mod 6) . a027748_row) [1..]",
				"-- _Reinhard Zumkeller_, Apr 09 2014",
				"(PARI) is_A050931(n)=n\u003e6\u0026\u0026Set(factor(n)[,1]%6)[1]==1 \\\\ _M. F. Hasler_, Mar 04 2018"
			],
			"xref": [
				"Cf. A002476, A004611, A024606, A230780 (complement), A009003.",
				"Cf. A027748."
			],
			"keyword": "easy,nonn",
			"offset": "1,1",
			"author": "Klaus Strassburger (strass(AT)ddfi.uni-duesseldorf.de), Dec 30 1999",
			"ext": [
				"Simpler definition from _M. F. Hasler_, Mar 04 2018"
			],
			"references": 9,
			"revision": 35,
			"time": "2020-08-16T10:45:18-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}