{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A257145",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 257145,
			"data": "1,-1,-2,2,1,0,-1,-2,2,1,0,-1,-2,2,1,0,-1,-2,2,1,0,-1,-2,2,1,0,-1,-2,2,1,0,-1,-2,2,1,0,-1,-2,2,1,0,-1,-2,2,1,0,-1,-2,2,1,0,-1,-2,2,1,0,-1,-2,2,1,0,-1,-2,2,1,0,-1,-2,2,1,0,-1,-2,2,1,0",
			"name": "a(n) = 5 * floor( (n+2) / 5) - n with a(0) = 1.",
			"comment": [
				"Cycle period is 5, {0, -1, -2, 2, 1} after the first five terms. - _Robert G. Wilson v_, Aug 02 2018"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A257145/b257145.txt\"\u003eTable of n, a(n) for n = 0..2500\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (-1,-1,-1,-1)."
			],
			"formula": [
				"Euler transform of length 5 sequence [-1, -2, 0, 0, 1].",
				"a(5*n) = 0 for all n in Z except n=0.",
				"a(n) = -a(-n) for all n in Z except n=0.",
				"a(n) = a(n+5) for all n in Z except n=-5 or n=0.",
				"Convolution inverse is A257143.",
				"G.f.: (1 - x) * (1 - x^2)^2 / (1 - x^5).",
				"G.f.: (1 - 2*x^2 + x^4) / (1 + x + x^2 + x^3 + x^4).",
				"a(n) = -A117444(n), n\u003e0. - _R. J. Mathar_, Oct 05 2017"
			],
			"example": [
				"G.f. = 1 - x - 2*x^2 + 2*x^3 + x^4 - x^6 - 2*x^7 + 2*x^8 + x^9 - x^11 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n==0, 1, -Mod[ n, 5, -2]];",
				"a[ n_] := If[ n==0, 1, Sign[n] SeriesCoefficient[ (1 - x) * (1 - x^2)^2 / (1 - x^5), {x, 0, Abs@n}]];",
				"CoefficientList[Series[(1-x)*(1-x^2)^2/(1-x^5), {x,0,60}], x] (* _G. C. Greubel_, Aug 02 2018 *)",
				"a[n_] := 5 Floor[(n + 2)/5] - n; Array[a, 77, 0] (* or *)",
				"CoefficientList[ Series[(x - 1)^2 (x + 1)^2/(x^4 + x^3 + x^2 + x + 1), {x, 0, 76}], x] (* or *)",
				"LinearRecurrence[{-1, -1, -1, -1}, {1, -1, -2, 2, 1, 0}, 76] (* _Robert G. Wilson v_, Aug 02 2018*)"
			],
			"program": [
				"(PARI) {a(n) = if( n==0, 1, (n+2) \\ 5 * 5 - n)};",
				"(PARI) {a(n) = if( n==0, 1, [0, -1, -2, 2, 1][n%5 + 1])};",
				"(PARI) {a(n) = if( n==0, 1, sign(n) * polcoeff( (1 - x) * (1 - x^2)^2 / (1 - x^5) + x * O(x^abs(n)), abs(n)))};",
				"(PARI) x='x+O('x^60); Vec((1-x)*(1-x^2)^2/(1-x^5)) \\\\ _G. C. Greubel_, Aug 02 2018",
				"(Haskell)",
				"a257145 0 = 1",
				"a257145 n = div (n + 2) 5 * 5 - n  -- _Reinhard Zumkeller_, Apr 17 2015",
				"(MAGMA) m:=60; R\u003cx\u003e:=PowerSeriesRing(Integers(), m); Coefficients(R!((1-x)*(1-x^2)^2/(1-x^5))); // _G. C. Greubel_, Aug 02 2018"
			],
			"xref": [
				"Cf. A257143, A253262, A117444."
			],
			"keyword": "sign,easy",
			"offset": "0,3",
			"author": "_Michael Somos_, Apr 16 2015",
			"references": 3,
			"revision": 19,
			"time": "2018-08-03T04:22:14-04:00",
			"created": "2015-04-16T17:26:16-04:00"
		}
	]
}