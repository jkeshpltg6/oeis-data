{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A143225",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 143225,
			"data": "0,3,9,9,10,10,16,20,19,21,23,23,24,25,28,31,32,36,38,56,57,59,59,62,65,71,75,84,88,88,96,102,107,115,116,119,120,126,125,129,132,132,163,168,168,182,189,189,192,197,198,213,236",
			"name": "Number of primes between n^2 and (n+1)^2, if equal to the number of primes between n and 2n.",
			"comment": [
				"Legendre's conjecture (still open) says there is always a prime between n^2 and (n+1)^2. Bertrand's postulate (actually a theorem due to Chebyshev) says there is always a prime between n and 2n.",
				"See the additional reference and link to Ramanujan's work mentioned in A143223. [_Jonathan Sondow_, Aug 03 2008]"
			],
			"reference": [
				"M. Aigner and C. M. Ziegler, Proofs from The Book, Chapter 2, Springer, NY, 2001.",
				"G. H. Hardy and E. M. Wright, An Introduction to the Theory of Numbers. 5th ed., Oxford Univ. Press, 1989, p. 19."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A143225/b143225.txt\"\u003eTable of n, a(n) for n=1..97\u003c/a\u003e (no other n \u003c 10^6)",
				"T. Hashimoto, \u003ca href=\"http://arxiv.org/abs/0807.3690\"\u003eOn a certain relation between Legendre's conjecture and Bertrand's postulate\u003c/a\u003e, arXiv:0807.3690 [math.GM], 2008.",
				"M. Hassani, \u003ca href=\"http://arXiv.org/abs/math/0607096\"\u003eCounting primes in the interval (n^2,(n+1)^2)\u003c/a\u003e, arXiv:math/0607096 [math.NT], 2006.",
				"J. Pintz, \u003ca href=\"http://www.renyi.hu/~pintz/\"\u003eLandau's problems on primes\u003c/a\u003e",
				"S. Ramanujan, \u003ca href=\"http://ramanujan.sirinudi.org/Volumes/published/ram24.html\"\u003eA proof of Bertrand's postulate\u003c/a\u003e, J. Indian Math. Soc., 11 (1919), 181-182.",
				"J. Sondow, \u003ca href=\"http://mathworld.wolfram.com/RamanujanPrime.html\"\u003eRamanujan Prime in MathWorld\u003c/a\u003e",
				"J. Sondow and E. W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/BertrandsPostulate.html\"\u003eBertrand's Postulate in MathWorld\u003c/a\u003e",
				"E. W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/LegendresConjecture.html\"\u003eLegendre's Conjecture in MathWorld\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A014085(A143224(n)) = A060715(A143224(n)) for n \u003e 0."
			],
			"example": [
				"There are 3 primes between 9^2 and 10^2 and 3 primes between 9 and 2*9, so 3 is a member."
			],
			"mathematica": [
				"L={}; Do[If[PrimePi[(n+1)^2]-PrimePi[n^2] == PrimePi[2n]-PrimePi[n], L=Append[L,PrimePi[2n]-PrimePi[n]]], {n,0,2000}]; L"
			],
			"xref": [
				"See A000720, A014085, A060715, A143223, A143224, A143226.",
				"Cf. A104272, A143227. [_Jonathan Sondow_, Aug 03 2008]"
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Jonathan Sondow_, Jul 31 2008",
			"references": 9,
			"revision": 15,
			"time": "2021-09-26T11:09:31-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}