{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A101113",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 101113,
			"data": "1,2,2,4,2,4,2,6,4,4,2,8,2,4,4,10,2,8,2,8,4,4,2,12,4,4,6,8,2,8,2,14,4,4,4,16,2,4,4,12,2,8,2,8,8,4,2,20,4,8,4,8,2,12,4,12,4,4,2,16,2,4,8,22,4,8,2,8,4,8,2,24,2,4,8,8,4,8,2,20,10,4,2,16,4,4,4,12,2,16,4,8,4,4,4,28,2,8",
			"name": "Let t(G) = number of unitary factors of the Abelian group G. Then a(n) = sum t(G) over all Abelian groups G of order exactly n.",
			"comment": [
				"From Schmidt paper: Let A denote the set of all Abelian groups. Under the operation of direct product, A is a semigroup with identity element E, the group with one element. G_1 and G_2 are relatively prime if the only common direct factor of G_1 and G_2 is E. We say that G_1 and G_2 are unitary factors of G if G=G_1 X G_2 and G_1, G_2 are relatively prime. Let t(G) denote the number of unitary factors of G. This sequence is a(n) = sum_{G in A, |G| = n} t(n)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A101113/b101113.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Peter Georg Schmidt, \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/aa/aa64/aa6433.pdf\"\u003eZur Anzahl unitärer Faktoren abelscher Gruppen\u003c/a\u003e, [On the number of unitary factors in Abelian groups] Acta Arith., 64 (1993), 237-248.",
				"J. Wu, \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/aa/aa84/aa8412.pdf\"\u003eOn the average number of unitary factors of finite Abelian groups\u003c/a\u003e, Acta Arith. 84 (1998), 17-29."
			],
			"formula": [
				"a(n) = 2^(number of distinct prime factors of n) * product of prime powers in factorization of n.",
				"a(n) = A034444(n) * A000688(n)."
			],
			"example": [
				"The only finite Abelian group of order 6 is C6=C2xC3. The unitary divisors are C1, C2, C3 and C6. So a(6) = 4."
			],
			"mathematica": [
				"Apply[Times, 2*Map[PartitionsP, Map[Last, FactorInteger[n]]]]",
				"Table[2^(PrimeNu[n])*(Times @@ PartitionsP /@ Last /@ FactorInteger@n), {n, 1, 100}] (* _G. C. Greubel_, Dec 27 2015 *)"
			],
			"program": [
				"(PARI) a(n)=my(f=factor(n)[, 2]); prod(i=1, #f, numbpart(f[i]))*2^#f; \\\\ _Michel Marcus_, Dec 27 2015"
			],
			"xref": [
				"Cf. A101114.",
				"Cf. A034444, A000688."
			],
			"keyword": "mult,easy,nonn",
			"offset": "1,2",
			"author": "_Russ Cox_, Dec 01 2004",
			"references": 5,
			"revision": 13,
			"time": "2015-12-29T10:14:35-05:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}