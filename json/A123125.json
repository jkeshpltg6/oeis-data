{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A123125",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 123125,
			"data": "1,0,1,0,1,1,0,1,4,1,0,1,11,11,1,0,1,26,66,26,1,0,1,57,302,302,57,1,0,1,120,1191,2416,1191,120,1,0,1,247,4293,15619,15619,4293,247,1,0,1,502,14608,88234,156190,88234,14608,502,1,0,1,1013,47840,455192,1310354",
			"name": "Triangle of Eulerian numbers T(n,k), 0 \u003c= k \u003c= n, read by rows.",
			"comment": [
				"The beginning of this sequence does not quite agree with the usual version, which is A173018. - _N. J. A. Sloane_, Nov 21 2010",
				"Each row of A123125 is the reverse of the corresponding row in A173018. - _Michael Somos_, Mar 17 2011",
				"A008292 (subtriangle for k\u003e=1 and n\u003e=1 is the main entry for these numbers.",
				"Triangle T(n,k), 0 \u003c= k \u003c= n, read by rows given by [0,1,0,2,0,3,0,4,0,5,0,...] DELTA [1,0,2,0,3,0,4,0,5,0,6,...] where DELTA is the operator defined in A084938.",
				"Row sums are the factorials. - _Roger L. Bagula_ and _Gary W. Adamson_, Aug 14 2008",
				"If the initial zero column is deleted, the result is A008292. - _Roger L. Bagula_ and _Gary W. Adamson_, Aug 14 2008",
				"This result gives an alternative method of calculating the Eulerian numbers by an Umbral Calculus expansion from Comtet. - _Roger L. Bagula_, Nov 21 2009",
				"This function seems to be equivalent to the PolyLog expansion. - _Roger L. Bagula_, Nov 21 2009",
				"A raising operator formed from the e.g.f. of this entry is the generator of a sequence of polynomials p(n,x;t) defined in A046802 that specialize to those for A119879 as p(n,x;-1), A007318 as p(n,x;0), A073107 as p(n,x;1), and A046802 as p(n,0;t). See Copeland link for more associations. - _Tom Copeland_, Oct 20 2015",
				"The Eulerian numbers in this setup count the permutation trees of power n and width k (see the Luschny link). For the associated combinatorial statistic over permutations see the Sage program below and the example section. - _Peter Luschny_, Dec 09 2015",
				"From _Wolfdieter Lang_, Apr 03 2017: (Start)",
				"The row polynomials R(n, x) = Sum_{k=0..n} T(n, k)*x^k are the numerator polynomials of the o.g.f. G(n, x) of n-powers {m^n}_{m\u003e=0} (with 0^0 = 1): G(n, x) = R(n, x)/(1-x)^(n+1). See the Aug 14 2008 formula, where f(x,n) = R(n, x). The e.g.f. of R(n, t) is given in Copeland's Oct 14 2015 formula below.",
				"The first nine column sequences are A000007, A000012, A000295, A000460, A000498, A000505, A000514, A001243, A001244. (End)",
				"With all offsets 0, let A_n(x;y) = (y + E.(x))^n, an Appell sequence in y where E.(x)^k = E_k(x) are the Eulerian polynomials of this entry, A123125. Then the row polynomials of A046802 (the h-polynomials of the stellahedra) are given by h_n(x) = A_n(x;1); the row polynomials of A248727 (the face polynomials of the stellahedra), by f_n(x) = A_n(1 + x;1); the Swiss-knife polynomials of A119879, by Sw_n(x) = A_n(-1;1 + x); and the row polynomials of the Worpitsky triangle (A130850), by w_n(x) = A(1 + x;0). Other specializations of A_n(x;y) give A090582 (the f-polynomials of the permutohedra, cf. also A019538) and A028246 (another version of the Worpitsky triangle). - _Tom Copeland_, Jan 24 2020",
				"Let b(n) = (1/(n+1))*Sum_{k=0..n-1} (-1)^(n-k+1)*T(n, k+1) / binomial(n, k+1). Then b(n) = Bernoulli(n, 1) = -n*Zeta(1 - n) = Integral_{x=0..1} F_n(x) for n \u003e= 1. Here F_n(x) are the signed Fubini polynomials (A278075). (See also Rzadkowski and Urlinska, example 1.) - _Peter Luschny_, Feb 15 2021"
			],
			"reference": [
				"L. Comtet, Advanced Combinatorics, Reidel, Holland, 1978, page 245. [_Roger L. Bagula_, Nov 21 2009]",
				"Ronald L. Graham, Donald E. Knuth and Oren Patashnik, Concrete Mathematics, 2nd ed.; Addison-Wesley, 1994, p. 268, Row reversed table 268. - _Wolfdieter Lang_, Apr 03 2017",
				"Douglas C. Montgomery and Lynwood A. Johnson, Forecasting and Time Series Analysis, MaGraw-Hill, New York, 1976, page 91. - _Roger L. Bagula_ and _Gary W. Adamson_, Aug 14 2008"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A123125/b123125.txt\"\u003eRows n = 0..125 of triangle, flattened\u003c/a\u003e",
				"Paul Barry, \u003ca href=\"http://arxiv.org/abs/1105.3043\"\u003eEulerian polynomials as moments, via exponential Riordan arrays\u003c/a\u003e, arXiv preprint arXiv:1105.3043 [math.CO], 2011, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL14/Barry7/barry172.html\"\u003eJ. Int. Seq. 14 (2011) # 11.9.5\u003c/a\u003e",
				"Paul Barry, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Barry1/barry263.html\"\u003eGeneralized Stirling Numbers, Exponential Riordan Arrays, and Toda Chain Equations\u003c/a\u003e, Journal of Integer Sequences, 17 (2014), #14.2.3.",
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/1802.03443\"\u003eOn a transformation of Riordan moment sequences\u003c/a\u003e, arXiv:1802.03443 [math.CO], 2018.",
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/1803.10297\"\u003eGeneralized Eulerian Triangles and Some Special Production Matrices\u003c/a\u003e, arXiv:1803.10297 [math.CO], 2018.",
				"V. Batyrev and M. Blume, \u003ca href=\"http://arxiv.org/abs/0911.3607\"\u003eThe functor of toric varieties associated with Weyl chambers and Losev-Manin moduli spaces\u003c/a\u003e, p. 11, arXiv:/0911.3607 [math.AG], 2009. [_Tom Copeland_, Oct 16 2015]",
				"Anna Borowiec, Wojciech Mlotkowski, \u003ca href=\"http://arxiv.org/abs/1509.03758\"\u003eNew Eulerian numbers of type D\u003c/a\u003e, arXiv:1509.03758 [math.CO], 2015.",
				"A. Cohen, \u003ca href=\"http://repository.tue.nl/651203\"\u003eEulerian polynomials of spherical type\u003c/a\u003e, Münster J. of Math. 1 (2008). [_Tom Copeland, Oct 16 2015]",
				"Tom Copeland, \u003ca href=\"https://tcjpn.wordpress.com/2015/10/12/the-elliptic-lie-triad-kdv-and-ricattt-equations-infinigens-and-elliptic-genera/\"\u003eThe Elliptic Lie Triad: Ricatti and KdV Equations, Infinigens, and Elliptic Genera\u003c/a\u003e",
				"FindStat - Combinatorial Statistic Finder, \u003ca href=\"http://www.findstat.org/StatisticsDatabase/St000021\"\u003eThe number of descents of a permutation.\u003c/a\u003e",
				"F. Hirzebruch, \u003ca href=\"https://www.uni-muenster.de/FB10/mjm/vol_1/mjm_vol_1_02.pdf\"\u003eEulerian polynomials\u003c/a\u003e, Münster J. of Math. 1 (2008), pp. 9-12.",
				"P. Hitczenko and S. Janson, \u003ca href=\"http://arxiv.org/abs/1212.5498\"\u003eWeighted random staircase tableaux\u003c/a\u003e, arXiv preprint arXiv:1212.5498 [math.CO], 2012.",
				"Hsien-Kuei Hwang, Hua-Huai Chern, Guan-Huei Duh, \u003ca href=\"https://arxiv.org/abs/1807.01412\"\u003eAn asymptotic distribution theory for Eulerian recurrences with applications\u003c/a\u003e, arXiv:1807.01412 [math.CO], 2018.",
				"Svante Janson, \u003ca href=\"http://arxiv.org/abs/1305.3512\"\u003eEuler-Frobenius numbers and rounding\u003c/a\u003e, arXiv preprint arXiv:1305.3512 [math.PR], 2013.",
				"Katarzyna Kril, Wojciech Mlotkowski, \u003ca href=\"https://www.combinatorics.org/ojs/index.php/eljc/article/view/v26i1p27\"\u003ePermutations of Type B with Fixed Number of Descents and Minus Signs\u003c/a\u003e, Volume 26(1) of The Electronic Journal of Combinatorics, 2019.",
				"Wolfdieter Lang, \u003ca href=\"https://arxiv.org/abs/1707.04451\"\u003eOn Sums of Powers of Arithmetic Progressions, and Generalized Stirling, Eulerian and Bernoulli numbers\u003c/a\u003e, arXiv:1707.04451 [math.NT], 2017.",
				"A. Losev and Y. Manin, \u003ca href=\"http://arxiv.org/abs/math/0001003\"\u003eNew moduli spaces of pointed curves and pencils of flat connections\u003c/a\u003e, arXiv preprint arXiv:math/0001003 [math.AG], 2000 (p. 8). - _Tom Copeland_, Oct 16 2015",
				"Peter Luschny, \u003ca href=\"http://www.oeis.org/wiki/User:Peter_Luschny/PermutationTrees\"\u003ePermutation Trees\u003c/a\u003e",
				"G. Rzadkowski, M. Urlinska, \u003ca href=\"http://arxiv.org/abs/1612.06635\"\u003eA Generalization of the Eulerian Numbers\u003c/a\u003e, arXiv:1612.06635 [math.CO], 2016"
			],
			"formula": [
				"Sum_{k=0..n} T(n,k) = n! = A000142(n).",
				"Sum_{k=0..n} 2^k*T(n,k) = A000629(n).",
				"Sum_{k=0..n} 3^k*T(n,k) = abs(A009362(n+1)).",
				"Sum_{k=0..n} 2^(n-k)*T(n,k) = A000670(n).",
				"Sum_{k=0..n} T(n,k)*3^(n-k) = A122704(n). - _Philippe Deléham_, Nov 07 2007",
				"G.f.: f(x,n) = (1 - x)^(n + 1)*Sum_{k\u003e=0} k^n*x^k. - _Roger L. Bagula_ and _Gary W. Adamson_, Aug 14 2008. f is not the g.f. of the triangle, it is the polynomial of row n. See an Apr 03 2017 comment above - _Wolfdieter Lang_, Apr 03 2017",
				"Sum_{k=0..n} T(n,k)*x^k = A000007(n), A000142(n), A000629(n), A123227(n), A201355(n), A201368(n) for x = 0, 1, 2, 3, 4, 5 respectively. - _Philippe Deléham_, Dec 01 2011",
				"E.g.f. (1-t)/(1-t*exp((1-t)x)). A123125 * A007318 = A130850 = unsigned A075263, related to reversed A028246. A007318 * A123125 = A046802. Evaluating the row polynomials at -1, giving the alternating-sign row sum, generates A009006. - _Tom Copeland_, Oct 14 2015",
				"From _Wolfdieter Lang_, Apr 03 2017: (Start)",
				"T(n, k) = A173018(n, n-k), 0 \u003c= k \u003c= n. Row reversed Euler's triangle. See Graham et al., p. 268.",
				"Recurrence (from A173018): T(n, 0) = 1 if n=0 else 0; T(n, k) = 0 if n \u003c k and T(n, k) = (n+1-k)*T(n-1, k-1) + k*T(n-1, k) else.",
				"T(n, k) = Sum_{j=0..k} (-1)^(k-j)*binomial(n-j, k-j)*S2(n, j)*j!, 0 \u003c= k \u003c= n, else 0. For S2(n, k)*k! see A131689.",
				"The recurrence for the o.g.f. of the sequence of column k is",
				"  G(k, x) = (x/(1 - k*x))*(E_x - (k-2))*G(k-1, x), with the Euler operator E_x  = x*d_x, for k \u003e= 1, with G(0, x) = 1. (Proof from the recurrence of T(n, k)).",
				"The e.g.f of the sequence of column k is found from E(k, x) = (1 + int(A(k, x),x)*exp(-k*x))*exp(k*x), k \u003e= 1, with the recurrence",
				"  A(k, x) = x*A(k-1, x) +(1 + (1-k)*(1-x))*E(k-1, x) for k \u003e= 1, with  A(0,x)= 0. (Proof from the recurrence of T(n, k)). (End)"
			],
			"example": [
				"The triangle T(n, k) begins:",
				"n\\k 0 1    2     3      4       5       6      7     8    9 10...",
				"0:  1",
				"1:  0 1",
				"2:  0 1    1",
				"3:  0 1    4     1",
				"4:  0 1   11    11      1",
				"5:  0 1   26    66     26       1",
				"6:  0 1   57   302    302      57       1",
				"7:  0 1  120  1191   2416    1191     120      1",
				"8:  0 1  247  4293  15619   15619    4293    247     1",
				"9:  0 1  502 14608  88234  156190   88234  14608   502    1",
				"10: 0 1 1013 47840 455192 1310354 1310354 455192 47840 1013  1",
				"...  Reformatted. - _Wolfdieter Lang_, Feb 14 2015",
				"------------------------------------------------------------------",
				"The width statistic over permutations, n=4.",
				"[1, 2, 3, 4] =\u003e 3; [1, 2, 4, 3] =\u003e 2; [1, 3, 2, 4] =\u003e 2; [1, 3, 4, 2] =\u003e 2;",
				"[1, 4, 2, 3] =\u003e 2; [1, 4, 3, 2] =\u003e 1; [2, 1, 3, 4] =\u003e 3; [2, 1, 4, 3] =\u003e 2;",
				"[2, 3, 1, 4] =\u003e 2; [2, 3, 4, 1] =\u003e 3; [2, 4, 1, 3] =\u003e 2; [2, 4, 3, 1] =\u003e 2;",
				"[3, 1, 2, 4] =\u003e 3; [3, 1, 4, 2] =\u003e 3; [3, 2, 1, 4] =\u003e 2; [3, 2, 4, 1] =\u003e 3;",
				"[3, 4, 1, 2] =\u003e 3; [3, 4, 2, 1] =\u003e 2; [4, 1, 2, 3] =\u003e 4; [4, 1, 3, 2] =\u003e 3;",
				"[4, 2, 1, 3] =\u003e 3; [4, 2, 3, 1] =\u003e 3; [4, 3, 1, 2] =\u003e 3; [4, 3, 2, 1] =\u003e 2;",
				"Gives row(4) = [0, 1, 11, 11, 1]. - _Peter Luschny_, Dec 09 2015",
				"------------------------------------------------------------------",
				"From _Wolfdieter Lang_, Apr 03 2017: (Start)",
				"Recurrence: T(5, 3) = (6-3)*T(4, 2) + 3*T(4, 3) = 3*11 + 3*11 = 66.",
				"O.g.f. column k=2: (x/(1 - 2*x))*E_x*(x/(1-x) = (x/1-x)^2/(1-2*x).",
				"E.g.f. column k=2: A(2, x) = x*A(1, x) + x*E(1, x) = x*1 + x*(exp(x)-1) = x*exp(x), hence E(2, x) = (1 + int(x*exp(-x),x ))*exp(2*x) = exp(x)*(exp(x) - (1+x)). See A000295. (End)"
			],
			"maple": [
				"gf := 1/(1 - t*exp(x)): ser := series(gf, x, 12):",
				"cx := n -\u003e (-1)^(n + 1)*factor(n!*coeff(ser, x, n)*(t - 1)^(n + 1)):",
				"seq(print(seq(coeff(cx(n), t, k), k = 0..n)), n = 0..9); # _Peter Luschny_, Feb 11 2021",
				"A123125 := proc(n, k) option remember; if k = n then 1 elif k \u003c= 0 or k \u003e n then 0",
				"  else k*procname(n-1, k) + (n-k+1)*procname(n-1, k-1) fi end:",
				"seq(print(seq(A123125(n, k), k=0..n)), n=0..10); # _Peter Luschny_, Mar 28 2021"
			],
			"mathematica": [
				"f[x_, n_] := f[x, n] = (1 - x)^(n + 1)*Sum[k^n*x^k, {k, 0, Infinity}];",
				"Table[CoefficientList[f[x, n], x], {n, 0, 9}] // Flatten (* Roger L. Bagula, Aug 14 2008 *)",
				"t[n_ /; n \u003e= 0, 0] = 1; t[n_, k_] /; k\u003c0 || k\u003en = 0; t[n_, k_] := t[n, k] = (n-k) t[n-1, k-1] + (k+1) t[n-1, k]; T[n_, k_] := t[n, n-k];",
				"Table[T[n, k], {n, 0, 10}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, May 26 2019 *)"
			],
			"program": [
				"(Haskell)",
				"a123125 n k = a123125_tabl !! n !! k",
				"a123125_row n = a123125_tabl !! n",
				"a123125_tabl = [1] : zipWith (:) [0, 0 ..] a008292_tabl",
				"-- _Reinhard Zumkeller_, Nov 06 2013",
				"(Sage)",
				"def statistic_eulerian(pi):",
				"    if not pi: return 0",
				"    h, i, branch, next = 0, len(pi), [0], pi[0]",
				"    while True:",
				"        while next \u003c branch[len(branch)-1]:",
				"            del(branch[len(branch)-1])",
				"        current = 0",
				"        h += 1",
				"        while next \u003e current:",
				"            i -= 1",
				"            if i == 0: return h",
				"            branch.append(next)",
				"            current, next = next, pi[i]",
				"def A123125_row(n):",
				"    L = [0]*(n+1)",
				"    for p in Permutations(n):",
				"        L[statistic_eulerian(p)] += 1",
				"    return L",
				"[A123125_row(n) for n in range(7)] # _Peter Luschny_, Dec 09 2015"
			],
			"xref": [
				"See A008292 (subtriangle for k\u003e=1 and n\u003e=1), which is the main entry for these numbers. Another version has the zeros at the ends of the rows, as in Concrete Mathematics: see A173018.",
				"Cf. A007318, A130850, A028246, A046802, A009006, A019538, A090582, A119879, A248727."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,9",
			"author": "_Philippe Deléham_, Sep 30 2006",
			"references": 83,
			"revision": 175,
			"time": "2021-03-28T08:30:11-04:00",
			"created": "2006-10-09T03:00:00-04:00"
		}
	]
}