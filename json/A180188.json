{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A180188",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 180188,
			"data": "1,0,2,3,0,3,8,12,0,4,45,40,30,0,5,264,270,120,60,0,6,1855,1848,945,280,105,0,7,14832,14840,7392,2520,560,168,0,8,133497,133488,66780,22176,5670,1008,252,0,9,1334960,1334970,667440,222600,55440,11340,1680,360,0",
			"name": "Triangle read by rows: T(n,k) is the number of permutations of [n] with k circular successions (0\u003c=k\u003c=n-1). A circular succession in a permutation p of [n] is either a pair p(i), p(i+1), where p(i+1)=p(i)+1 or the pair p(n), p(1) if p(1)=p(n)+1.",
			"comment": [
				"For example, p=(4,1,2,5,3) has 2 circular successions: (1,2) and (3,4).",
				"Sum of entries in row n = n! = A000142(n).",
				"T(n,0)=nd(n-1)=A000240(n).",
				"T(n,1)=n(n-1)d(n-2)=A180189(n).",
				"Sum(k*T(n,k), k\u003e=0)=n! = A000142(n) if n\u003e=2."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A180188/b180188.txt\"\u003eRows n = 1..141, flattened\u003c/a\u003e",
				"S. M. Tanny, \u003ca href=\"http://dx.doi.org/10.1016/0097-3165(76)90063-7\"\u003ePermutations and successions\u003c/a\u003e, J. Combinatorial Theory, Series A, 21 (1976), 196-202."
			],
			"formula": [
				"T(n,k) = n*C(n-1,k)*d(n-1-k), where d(j) = A000166(j) are the derangement numbers (see Prop. 1 of the Tanny reference).",
				"T(n,k) = n*A008290(n-1,k), 0\u003c=k\u003cn, n\u003e=1. - _R. J. Mathar_, Sep 08 2013"
			],
			"example": [
				"T(3,2) = 3 because we have 123, 312, and 231.",
				"The triangle starts:",
				"1;",
				"0,   2;",
				"3,   0,  3;",
				"8,  12,  0, 4;",
				"45, 40, 30, 0, 5;"
			],
			"maple": [
				"A180188 := proc (n, k) n*binomial(n-1, k)*A000166(n-1-k) end proc:",
				"for n to 10 do seq(A180188(n, k), k = 0 .. n-1) end do; # yields sequence in triangular form"
			],
			"mathematica": [
				"T[n_, k_] := n*Binomial[n-1, k]*Subfactorial[n-1-k]; Table[T[n, k], {n, 0, 10}, {k, 0, n-1}] // Flatten (* _Jean-François Alcover_, Feb 19 2017 *)"
			],
			"xref": [
				"Cf. A000142, A000166, A000240, A180189."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Emeric Deutsch_, Sep 06 2010",
			"references": 4,
			"revision": 18,
			"time": "2017-02-19T06:55:19-05:00",
			"created": "2010-09-12T03:00:00-04:00"
		}
	]
}