{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A286156",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 286156,
			"data": "1,2,3,2,1,6,2,5,4,10,2,5,1,3,15,2,5,9,4,7,21,2,5,9,1,8,6,28,2,5,9,14,4,3,11,36,2,5,9,14,1,8,7,10,45,2,5,9,14,20,4,13,12,16,55,2,5,9,14,20,1,8,3,6,15,66,2,5,9,14,20,27,4,13,7,11,22,78,2,5,9,14,20,27,1,8,19,12,17,21,91,2,5,9,14,20,27,35,4,13,3,18,10,29,105",
			"name": " A(n,k) = T(remainder(n,k), quotient(n,k)), where T(n,k) is sequence A001477 considered as a two-dimensional table, square array read by descending antidiagonals.",
			"link": [
				"Antti Karttunen, \u003ca href=\"/A286156/b286156.txt\"\u003eTable of n, a(n) for n = 1..10585; the first 145 antidiagonals of array\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PairingFunction.html\"\u003ePairing Function\u003c/a\u003e"
			],
			"formula": [
				"A(n,k) = T(remainder(n,k), quotient(n,k)), where T(n,k) is sequence A001477 considered as a two-dimensional table, that is, as a pairing function from [0, 1, 2, 3, ...] x [0, 1, 2, 3, ...] to [0, 1, 2, 3, ...]. This sequence lists only values for indices n \u003e= 1, k \u003e= 1."
			],
			"example": [
				"The top left 15 X 15 corner of the array:",
				"    1,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,   2,   2",
				"    3,  1,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,  5,   5,   5",
				"    6,  4,  1,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,   9,   9",
				"   10,  3,  4,  1, 14, 14, 14, 14, 14, 14, 14, 14, 14,  14,  14",
				"   15,  7,  8,  4,  1, 20, 20, 20, 20, 20, 20, 20, 20,  20,  20",
				"   21,  6,  3,  8,  4,  1, 27, 27, 27, 27, 27, 27, 27,  27,  27",
				"   28, 11,  7, 13,  8,  4,  1, 35, 35, 35, 35, 35, 35,  35,  35",
				"   36, 10, 12,  3, 13,  8,  4,  1, 44, 44, 44, 44, 44,  44,  44",
				"   45, 16,  6,  7, 19, 13,  8,  4,  1, 54, 54, 54, 54,  54,  54",
				"   55, 15, 11, 12,  3, 19, 13,  8,  4,  1, 65, 65, 65,  65,  65",
				"   66, 22, 17, 18,  7, 26, 19, 13,  8,  4,  1, 77, 77,  77,  77",
				"   78, 21, 10,  6, 12,  3, 26, 19, 13,  8,  4,  1, 90,  90,  90",
				"   91, 29, 16, 11, 18,  7, 34, 26, 19, 13,  8,  4,  1, 104, 104",
				"  105, 28, 23, 17, 25, 12,  3, 34, 26, 19, 13,  8,  4,   1, 119",
				"  120, 37, 15, 24,  6, 18,  7, 43, 34, 26, 19, 13,  8,   4,   1"
			],
			"mathematica": [
				"Map[((#1 + #2)^2 + 3 #1 + #2)/2 \u0026 @@ # \u0026 /@ Reverse@ # \u0026, Table[Function[m, Reverse@ QuotientRemainder[m, k]][n - k + 1], {n, 14}, {k, n}]] // Flatten (* _Michael De Vlieger_, May 20 2017 *)"
			],
			"program": [
				"(Scheme)",
				"(define (A286156 n) (A286156bi (A002260 n) (A004736 n)))",
				"(define (A286156bi row col) (if (zero? col) -1 (let ((a (remainder row col)) (b (quotient row col))) (/ (+ (expt (+ a b) 2) (* 3 a) b) 2))))",
				"(Python)",
				"def T(a, b): return ((a + b)**2 + 3*a + b)//2",
				"def A(n, k): return T(n%k, n//k)",
				"for n in range(1, 21): print([A(k, n - k + 1) for k in range(1, n + 1)])  # _Indranil Ghosh_, May 20 2017"
			],
			"xref": [
				"Cf. A286157 (transpose),  A286158 (lower triangular region), A286159 (lower triangular region transposed).",
				"Cf. A000217 (column 1), A000012 (the main diagonal), A000096 (superdiagonal), A034856.",
				"Cf. A001477, A285722, A286101, A286102."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Antti Karttunen_, May 04 2017",
			"references": 13,
			"revision": 27,
			"time": "2021-03-18T23:43:46-04:00",
			"created": "2017-05-04T09:39:03-04:00"
		}
	]
}