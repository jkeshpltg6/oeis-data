{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A246209",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 246209,
			"data": "0,1,5,2,15,22,3,7,52,66,35,71,4,6,11,23,137,194,148,213,36,73,99,172,17,8,16,21,12,25,33,58,462,601,447,643,431,620,304,516,37,72,104,173,127,225,419,587,45,64,9,19,47,68,49,69,13,24,29,59,43,75,152,197,1273,1734,1334,1940,1294,1740,899,1556,1404,1837,945,1567,389,698,1246,1761,41",
			"name": "Permutation of nonnegative integers: a(0) = 0, a(1) = 1, a(2n) = A117967(1+a(n)), a(2n+1) = A117968(a(n)).",
			"comment": [
				"This is an instance of entanglement permutation, where complementary pair A005843/A005408 (even and odd numbers respectively) is entangled with complementary pair A117967/A117968 (positive and negative part of inverse of balanced ternary enumeration of integers, respectively), with a(0) set to 0 and a(1) set to 1.",
				"This implies that the even positions contain only terms of A117967 and apart from a(1) = 1, the odd positions contain only terms of A117968."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A246209/b246209.txt\"\u003eTable of n, a(n) for n = 0..2047\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(0) = 0, a(1) = 1, a(2n) = A117967(1+a(n)), a(2n+1) = A117968(a(n)).",
				"As a composition of related permutations:",
				"a(n) = A246207(A054429(n)).",
				"a(n) = A246211(A246207(n))."
			],
			"program": [
				"(Scheme, with memoizing definec-macro from _Antti Karttunen_'s IntSeq-library)",
				"(definec (A246209 n) (cond ((\u003c= n 1) n) ((odd? n) (A117968 (A246209 (/ (- n 1) 2)))) ((even? n) (A117967 (+ 1 (A246209 (/ n 2)))))))",
				"(Python)",
				"from sympy.ntheory.factor_ import digits",
				"def a004488(n): return int(\"\".join(str((3 - i)%3) for i in digits(n, 3)[1:]), 3)",
				"def a117968(n):",
				"    if n==1: return 2",
				"    if n%3==0: return 3*a117968(n//3)",
				"    elif n%3==1: return 3*a117968((n - 1)//3) + 2",
				"    else: return 3*a117968((n + 1)//3) + 1",
				"def a117967(n): return 0 if n==0 else a117968(-n) if n\u003c0 else a004488(a117968(n))",
				"def a(n): return n if n\u003c2 else a117967(1 + a(n//2)) if n%2==0 else a117968(a((n - 1)//2))",
				"print([a(n) for n in range(101)]) # _Indranil Ghosh_, Jun 07 2017"
			],
			"xref": [
				"Inverse: A246210.",
				"Related permutations: A054429, A246207, A246211.",
				"Cf. A117967, A117968."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Antti Karttunen_, Aug 19 2014",
			"references": 5,
			"revision": 24,
			"time": "2021-05-02T11:09:09-04:00",
			"created": "2014-08-22T16:48:00-04:00"
		}
	]
}