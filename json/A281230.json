{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A281230",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 281230,
			"data": "1,3,4,3,10,12,8,6,12,30,5,12,14,24,20,12,18,12,9,30,8,15,24,12,50,42,36,24,7,60,15,24,20,18,40,12,38,9,28,30,20,24,44,15,60,24,16,12,56,150,36,42,54,36,10,24,36,21,29,60,30,15,24,48,70,60,68,18,24,120,35,12,74,114,100,9,40,84,39,60",
			"name": "Period of the discrete Arnold cat map on an n X n array.",
			"comment": [
				"The discrete Arnold's cat map on an n X n array is defined as (x, y) -\u003e (x + y, x + 2y) mod n, where x and y are integers with 0 \u003c= x, y \u003c n. For a fixed n, iterating this map classifies all points into distinct cycles; a(n) is then the LCM of the cycle lengths.",
				"Dyson and Falk show that log(n)/phi \u003c a(n) \u003c= 3*n, where phi is the golden ratio A001622.",
				"From _Jianing Song_, Jan 05 2019: (Start)",
				"Pisano periods for A001906.",
				"a(n) is the multiplicative order of (3 + sqrt(5))/2 modulo n over the ring Z[(1 + sqrt(5))/2].",
				"a(n) is the smallest k \u003e 0 such that [3, -1; 1, 0]^k == I (mod n), where I is the identity matrix.",
				"a(m*n) = a(m)*a(n) if gcd(m, n) = 1.",
				"For primes p == +-1 (mod 10), a(p) divides (p - 1)/2.",
				"For primes p == +-3 (mod 10), a(p) divides p + 1, but a(p) does not divide (p + 1)/2.",
				"For odd primes p, a(p^e) = p^(e-1)*a(p) if p^2 does not divide a(p). Any counterexample would be a Wall-Sun-Sun prime. No such primes are known.",
				"a(2^e) = 3 if e = 1 and 3*2^(e-2) if e \u003e= 2. a(5^e) = 2*5^e, e \u003e= 1.",
				"As is mentioned above, a(n) \u003c= 3*n for all n, where the equality holds if and only if n = 2*5^e, e \u003e= 1. (End)",
				"In general, the Pisano period of Fibonacci(k*n) mod m is lcm(A001175(m), k)/k, see link. - _Jon Maiga_, Mar 22 2019"
			],
			"link": [
				"Jeremy Tan, \u003ca href=\"/A281230/b281230.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Freeman Dyson and Harold Falk, \u003ca href=\"https://dx.doi.org/10.2307%2F2324989\"\u003ePeriod of a Discrete Cat Mapping\u003c/a\u003e, The American Mathematical Monthly 99 (7), 603-614.",
				"Holger Kantz, \u003ca href=\"http://www.mpipks-dresden.mpg.de/mpi-doc/kantzgruppe/wiki/projects/Recurrence.html\"\u003ePoincaré Recurrences\u003c/a\u003e",
				"J. Maiga, \u003ca href=\"http://jonkagstrom.com/articles/the_pisano_period_of_fibonacci_k_sections.pdf\"\u003eThe Pisano period of Fibonacci k-sections\u003c/a\u003e, 2019.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Arnold%27s_cat_map\"\u003eArnold's cat map\u003c/a\u003e"
			],
			"formula": [
				"a(n) is the smallest positive integer t such that F(2*t) == 0 (mod n) and F(2*t-1) == 1 (mod n), where F(n) is the Fibonacci sequence A000045.",
				"a(n) = A001175(n)/2 for n \u003e= 3 (half the Pisano period for the Fibonacci sequence). Proof: the map sends (F(m-1), F(m)) to (F(m+1), F(m+2)) mod n. F(0) = 0 and F(-1) = 1, so the smallest value of 2*t that makes the two defining congruences valid is A001175(n). For n \u003e= 3, all Pisano periods for the Fibonacci sequence are even, which completes the proof.",
				"a(n) = lcm(A001175(n), 2)/2. - _Jon Maiga_, Mar 22 2019"
			],
			"example": [
				"The cycles for n = 3 are (0,0), (1,0)-\u003e(1,1)-\u003e(2,0)-\u003e(2,2) and (0,1)-\u003e(1,2)-\u003e(0,2)-\u003e(2,1). Since there is one cycle of length 1 and two of length 4, a(3) = 4."
			],
			"program": [
				"(PARI) a(n) = {if(n \u003c 2, 1, t = 1; while(Mod(fibonacci(2 * t), n) != 0 || Mod(fibonacci(2 * t - 1), n) != 1, t += 1); t)}"
			],
			"xref": [
				"Cf. A000045, A001175, A001622, A001906."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Jeremy Tan_, Jan 18 2017",
			"references": 1,
			"revision": 39,
			"time": "2021-04-25T02:34:54-04:00",
			"created": "2017-01-18T10:05:14-05:00"
		}
	]
}