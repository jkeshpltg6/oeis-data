{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A079267",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 79267,
			"data": "1,0,1,1,1,1,5,6,3,1,36,41,21,6,1,329,365,185,55,10,1,3655,3984,2010,610,120,15,1,47844,51499,25914,7980,1645,231,21,1,721315,769159,386407,120274,25585,3850,406,28,1,12310199,13031514,6539679,2052309,446544,70371,8106,666,36,1",
			"name": "d(n,s) = number of perfect matchings on {1, 2, ..., n} with s short pairs.",
			"comment": [
				"Read backwards, the n-th row of the triangle gives the Hilbert series of the variety of slopes determined by n points in the plane.",
				"From _Paul Barry_, Nov 25 2009: (Start)",
				"Reversal of coefficient array for the polynomials P(n,x)= Sum_{k=0..n} (C(n+k,2k)(2k)!/(2^k*k!))*x^k*(1-x)^(n-k).",
				"Note that P(n,x) = Sum_{k=0..n} A001498(n,k)*x^k*(1-x)^(n-k). (End)",
				"Equivalent to the original definition: Triangle of fixed-point free involutions on [1..2n] (=A001147) by number of cycles with adjacent integers. - _Olivier Gérard_, Mar 23 2011",
				"Conjecture: Asymptotically, the n-th row has a Poisson distribution with mean 1. - _David Callan_, Nov 11 2012",
				"This is also the number of configurations of n indistinguishable pairs placed on the vertices of the ladder graph P_1 X P_2n (i.e. a path of length 2n) such that s such pairs are joined by an edge; equivalently the number of \"s-domino\" configurations in the game of memory played on a 1 X 2n rectangular array, see [Young]. - _Donovan Young_, Oct 23 2018"
			],
			"reference": [
				"G. Kreweras and Y. Poupard, Sur les partitions en paires d'un ensemble fini totalement ordonné, Publications de l'Institut de Statistique de l'Université de Paris, 23 (1978), 57-74."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A079267/b079267.txt\"\u003eTable of n, a(n) for n = 0..11475\u003c/a\u003e (rows 0 \u003c= n \u003c= 150, flattened).",
				"Naiomi T. Cameron, Kendra Killpatrick, \u003ca href=\"https://arxiv.org/abs/1902.09021\"\u003eStatistics on Linear Chord Diagrams\u003c/a\u003e, arXiv:1902.09021 [math.CO], 2019.",
				"G. Kreweras and Y. Poupard, \u003ca href=\"/A000806/a000806.pdf\"\u003eSur les partitions en paires d'un ensemble fini totalement ordonné\u003c/a\u003e, Publications de l'Institut de Statistique de l'Université de Paris, 23 (1978), 57-74. (Annotated scanned copy)",
				"J. L. Martin, \u003ca href=\"http://arxiv.org/abs/math/0302106\"\u003eThe slopes determined by n points in the plane\u003c/a\u003e, arXiv:math/0302106 [math.AG], 2003-2006.",
				"J. L. Martin, \u003ca href=\"http://dx.doi.org/10.1215/S0012-7094-05-13114-3\"\u003eThe slopes determined by n points in the plane\u003c/a\u003e, Duke Math. J., Volume 131, Number 1 (2006), 119-165.",
				"D. Young, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL21/Young/young2.html\"\u003eThe Number of Domino Matchings in the Game of Memory\u003c/a\u003e, Journal of Integer Sequences, Vol. 21 (2018), Article 18.8.1.",
				"Donovan Young, \u003ca href=\"https://arxiv.org/abs/1905.13165\"\u003eGenerating Functions for Domino Matchings in the 2 * k Game of Memory\u003c/a\u003e, arXiv:1905.13165 [math.CO], 2019. Also in \u003ca href=\"https://www.emis.de/journals/JIS/VOL22/Young/young13.html\"\u003eJ. Int. Seq.\u003c/a\u003e, Vol. 22 (2019), Article 19.8.7."
			],
			"formula": [
				"d(n, s) = 1/s! * Sum_{h=s..n}(((-1)^(h-s)*(2*n-h)!/(2^(n-h)*(n-h)!*(h-s)!))).",
				"E.g.f.: exp((x-1)*(1-sqrt(1-2*y)))/sqrt(1-2*y). - _Vladeta Jovovic_, Dec 15 2008"
			],
			"example": [
				"Triangle begins:",
				"   1",
				"   0  1",
				"   1  1  1",
				"   5  6  3 1",
				"  36 41 21 6 1",
				"From _Paul Barry_, Nov 25 2009: (Start)",
				"Production matrix begins",
				"       0,      1,",
				"       1,      1,      1,",
				"       4,      4,      2,     1,",
				"      18,     18,      9,     3,     1,",
				"      96,     96,     48,    16,     4,    1,",
				"     600,    600,    300,   100,    25,    5,   1,",
				"    4320,   4320,   2160,   720,   180,   36,   6,  1,",
				"   35280,  35280,  17640,  5880,  1470,  294,  49,  7, 1,",
				"  322560, 322560, 161280, 53760, 13440, 2688, 448, 64, 8, 1",
				"Complete this by adding top row (1,0,0,0,....) and take inverse: we obtain",
				"   1,",
				"   0,  1,",
				"  -1, -1,  1,",
				"  -2, -2, -2,  1,",
				"  -3, -3, -3, -3,  1,",
				"  -4, -4, -4, -4, -4,  1,",
				"  -5, -5, -5, -5, -5, -5,  1,",
				"  -6, -6, -6, -6, -6, -6, -6,  1,",
				"  -7, -7, -7, -7, -7, -7, -7, -7,  1,",
				"  -8, -8, -8, -8, -8, -8, -8, -8, -8,  1 (End)",
				"The 6 involutions with no fixed point on [1..6] with only one 2-cycle with adjacent integers are ((1, 2), (3, 5), (4, 6)), ((1, 3), (2, 4), (5, 6)), ((1, 3), (2, 6), (4, 5)), ((1, 5), (2, 3), (4, 6)), ((1, 5), (2, 6), (3, 4)), and ((1, 6), (2, 5), (3, 4))."
			],
			"maple": [
				"d := (n,s) -\u003e 1/s! * sum('((-1)^(h-s)*(2*n-h)!/(2^(n-h)*(n-h)!*(h-s)!))','h'=s..n):"
			],
			"mathematica": [
				"nmax = 9; d[n_, s_] := (2^(s-n)*(2n-s)!* Hypergeometric1F1[s-n, s-2n, -2])/ (s!*(n-s)!); Flatten[ Table[d[n, s], {n, 0, nmax}, {s, 0, n}]] (* _Jean-François Alcover_, Oct 19 2011, after Maple *)"
			],
			"program": [
				"(PARI) {T(n, k) = 2^(k-n)*binomial(n,k)*hyperu(k-n, k-2*n, -2)};",
				"for(n=0,10, for(k=0,n, print1(round(T(n,k)), \", \"))) \\\\ _G. C. Greubel_, Apr 10 2019",
				"(Sage) [[2^(k-n)*binomial(n,k)*hypergeometric_U(k-n,k-2*n,-2).simplify_hypergeometric() for k in (0..n)] for n in (0..10)] # _G. C. Greubel_, Apr 10 2019"
			],
			"xref": [
				"Columns are A000806, A006198, A006199, A006200.",
				"Row sums are A001147."
			],
			"keyword": "easy,nice,nonn,tabl",
			"offset": "0,7",
			"author": "Jeremy Martin (martin(AT)math.umn.edu), Feb 05 2003",
			"ext": [
				"Extra terms added by _Paul Barry_, Nov 25 2009"
			],
			"references": 12,
			"revision": 53,
			"time": "2020-03-18T11:11:32-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}