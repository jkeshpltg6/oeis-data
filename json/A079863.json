{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A079863",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 79863,
			"data": "34,70,144,296,608,1248,2560,5248,10752,22016,45056,92160,188416,385024,786432,1605632,3276800,6684672,13631488,27787264,56623104,115343360,234881024,478150656,973078528,1979711488,4026531840,8187281408,16642998272,33822867456",
			"name": "a(n) is the number of occurrences of 11s in the palindromic compositions of m=2*n-1 = the number of occurrences of 12s in the palindromic compositions of m=2*n.",
			"comment": [
				"This sequence is part of a family of sequences, namely R(n,k), the number of ks in palindromic compositions of n. See also A057711, A001792, A078836, A079861, A079862. General formula: R(n,k)=2^(floor(n/2) - k) * (2 + floor(n/2) - k) if n and k have different parity and R(n,k)=2^(floor(n/2) - k) * (2 + floor(n/2) - k + 2^(floor((k+1)/2 - 1)) otherwise, for n \u003e= 2k."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A079863/b079863.txt\"\u003eTable of n, a(n) for n = 12..1000\u003c/a\u003e",
				"P. Chinn, R. Grimaldi and S. Heubach, \u003ca href=\"https://www.calstatela.edu/sites/default/files/users/u1231/Papers/freqs.pdf\"\u003eThe frequency of summands of a particular size in Palindromic Compositions\u003c/a\u003e, Ars Combin. 69 (2003), 65-78.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-4)."
			],
			"formula": [
				"a(n) = (n+22)*2^(n-12).",
				"From _Colin Barker_, Sep 29 2015: (Start)",
				"a(n) = 4*a(n-1) - 4*a(n-2) for n\u003e13.",
				"G.f.: -2*x^12*(33*x-17) / (2*x-1)^2.",
				"(End)"
			],
			"example": [
				"a(12) = 34 since the palindromic compositions of 23 that contain a 11 are 11+1+11 and the 32 compositions of the form c+11+(reverse of c), where c represents a composition of 6."
			],
			"mathematica": [
				"Table[(22 + i)*2^(i - 12), {i, 12, 50}]",
				"LinearRecurrence[{4,-4},{34,70},30] (* _Harvey P. Dale_, Jan 30 2017 *)"
			],
			"program": [
				"(PARI) Vec(-2*x^12*(33*x-17)/(2*x-1)^2 + O(x^100)) \\\\ _Colin Barker_, Sep 29 2015",
				"(PARI) a(n)=(n+22)\u003c\u003c(n-12) \\\\ _Charles R Greathouse IV_, Sep 29 2015"
			],
			"xref": [
				"Cf. A057711, A001792, A079859, A078836, A079861, A079862."
			],
			"keyword": "easy,nonn",
			"offset": "12,1",
			"author": "Silvia Heubach (sheubac(AT)calstatela.edu), Jan 11 2003",
			"references": 7,
			"revision": 20,
			"time": "2020-11-05T13:26:28-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}