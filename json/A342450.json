{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A342450",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 342450,
			"data": "53,157,145,3055,6165,234331,584879,2599496,48785015,292856489,854612603,12206236915,8392400925,183100803621,1296977891119,15258697717317,2997253335821,79472769236347,556309528064071,5960463317677243,25033951904190895,46938653648975843,3099441423652148001",
			"name": "a(n) is the numerator of the Schnirelmann density of the n-free numbers.",
			"comment": [
				"k-free numbers are numbers whose exponents in their prime factorization are all less than k. E.g., the squarefree numbers (k=2, A005117), the cubefree numbers (k=3, A004709) and the biquadratefree numbers (k=4, A046100).",
				"Let Q_k(m) be the number of k-free numbers not exceeding m. The Schnirelmann density for k-free numbers is d(k) = inf_{m\u003e=1} Q_k(m)/m.",
				"a(2) was found by Rogers (1964).",
				"a(3)-a(6) were found by Orr (1969).",
				"a(7)-a(75) were found by Hardy (1979)."
			],
			"reference": [
				"József Sándor, Dragoslav S. Mitrinovic and Borislav Crstici, Handbook of Number Theory I, Springer Science \u0026 Business Media, 2005, Chapter VI, p. 217."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A342450/b342450.txt\"\u003eTable of n, a(n) for n = 2..75\u003c/a\u003e (from Hardy, 1979)",
				"P. H. Diananda and M. V. Subbarao, \u003ca href=\"https://doi.org/10.1090/S0002-9939-1977-0435024-9\"\u003eOn the Schnirelmann density of the k-free integers\u003c/a\u003e, Proceedings of the American Mathematical Society, Vol. 62, No. 1 (1977), pp. 7-10.",
				"R. L. Duncan, \u003ca href=\"https://doi.org/10.1090/S0002-9939-1965-0186652-1\"\u003eThe Schnirelmann density of the k-free integers\u003c/a\u003e, Proceedings of the American Mathematical Society, Vol. 16, No. 5 (1965), pp. 1090-1091.",
				"R. L. Duncan, \u003ca href=\"https://www.fq.math.ca/Scanned/7-2/duncan.pdf\"\u003eOn the density of the k-free integers\u003c/a\u003e, Fibonacci Quarterly, Vol. 7, No. 2 (1969), pp. 140-142.",
				"Paul Erdős, G. E. Hardy and M. V. Subbarao, \u003ca href=\"https://users.renyi.hu/~p_erdos/1978-32.pdf\"\u003eOn the Schnirelmann density of k-free integers\u003c/a\u003e, Indian J. Math., Vol. 20 (1978), pp. 45-56.",
				"George Eugene Hardy, \u003ca href=\"https://archive.org/details/Hardy1979\"\u003eOn the Schnirelmann density of the k-free and (k,r)-free integers\u003c/a\u003e, Ph.D. thesis, University of Alberta, 1979.",
				"Richard C. Orr, \u003ca href=\"https://doi.org/10.1112/jlms/s1-44.1.313\"\u003eOn the Schnirelmann density of the sequence of k-free integers\u003c/a\u003e, Journal of the London Mathematical Society, Vol. 1, No. 1 (1969), pp. 313-319.",
				"Kenneth Rogers, \u003ca href=\"https://doi.org/10.1090/S0002-9939-1964-0163893-X\"\u003eThe Schnirelmann density of the squarefree integers\u003c/a\u003e, Proceedings of the American Mathematical Society, Vol. 15, No. 4 (1964), pp. 515-516.",
				"Harold M. Stark, \u003ca href=\"https://doi.org/10.1090/S0002-9939-1966-0199161-1\"\u003eOn the asymptotic density of the k-free integers\u003c/a\u003e, Proceedings of the American Mathematical Society, Vol. 17, No. 5 (1966), pp. 1211-1214.",
				"M. V. Subbarao, \u003ca href=\"https://core.ac.uk/download/pdf/39229699.pdf\"\u003eOn the Schnirelman density of the K-free integers\u003c/a\u003e, Distribution of values of arithmetic functions, Vol. 517 (1984), pp. 47-61; \u003ca href=\"https://repository.kulib.kyoto-u.ac.jp/dspace/handle/2433/98401\"\u003ealternative link\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"https://mathworld.wolfram.com/SchnirelmannDensity.html\"\u003eSchnirelmann Density\u003c/a\u003e.",
				"Wikiepdia, \u003ca href=\"https://en.wikipedia.org/wiki/Schnirelmann_density\"\u003eSchnirelmann density\u003c/a\u003e."
			],
			"formula": [
				"Let d(n) = a(n)/A342451(n), and let D(n) = 1/zeta(n), the asymptotic density of the n-free numbers. Then:",
				"Lim_{n-\u003eoo} d(n) = 1.",
				"d(n) \u003c D(n) (Stark, 1966).",
				"d(n) \u003c D(n) \u003c d(n+1) \u003c D(n+1) (Duncan, 1965; Erdős et al., 1978).",
				"d(n) \u003e 1 - Sum_{p prime} 1/p^n (Duncan, 1969).",
				"(D(n+1)-d(n+1))/(D(n)-d(n)) \u003c 1/2^n (Duncan, 1969).",
				"d(n) \u003e 1 - 1/2^n - 1/3^n - 1/5^n (Diananda and Subbarao, 1977)."
			],
			"example": [
				"The fractions begin with 53/88, 157/189, 145/157, 3055/3168, 6165/6272, 234331/236288, 584879/587264, 2599496/2604717, 48785015/48833536, 292856489/293001216, ..."
			],
			"xref": [
				"Cf. A013928, A336025, A342451 (denominators), A342452.",
				"Cf. A005117, A004709, A046100."
			],
			"keyword": "nonn,frac",
			"offset": "2,1",
			"author": "_Amiram Eldar_, Mar 12 2021",
			"references": 3,
			"revision": 13,
			"time": "2021-03-13T10:05:00-05:00",
			"created": "2021-03-12T23:51:21-05:00"
		}
	]
}