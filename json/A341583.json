{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A341583",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 341583,
			"data": "0,1,3,8,18,42,94,208,450,966,2052,4330,9074,18920,39266,81182,167268,343634,704122,1439496,2936906,5981174,12161332,24691514,50066690,101400616,205150098,414653998,837377988,1689714242,3407154474,6865700808,13826659450,27829885126",
			"name": "Geometric length of the solution to the Towers of Hanoi exchanging disks puzzle with 3 pegs and n disks.",
			"comment": [
				"Scorer, Grundy and Smith define a variation of the towers of Hanoi puzzle where the smallest disk moves freely and two disks can exchange positions when they differ in size by 1, are on different pegs, and each is top-most on its peg.  The puzzle is to move a stack of n disks from one peg to another.",
				"Stockmeyer et al. determine the shortest solution to the puzzle.  a(n) is their d(n) which is the geometric length of the solution when the state graph is embedded in a grid of unit triangles in the manner of the sample drawing by Scorer et al.",
				"Graph n comprises 3 copies of graph n-1.  The embedding arranges these 3 copies as a large triangle with a unit gap between the corners.  The edges connecting these subgraphs are in the middle of the inner sides.  A move of the smallest disk is length 1.  An exchange of disks s and s+1 is length 2^s, where the smallest disk is s=0."
			],
			"link": [
				"Kevin Ryde, \u003ca href=\"/A341583/b341583.txt\"\u003eTable of n, a(n) for n = 0..700\u003c/a\u003e",
				"Paul K. Stockmeyer et al., \u003ca href=\"https://doi.org/10.1080/00207169508804452\"\u003eExchanging Disks in the Tower of Hanoi\u003c/a\u003e, International Journal of Computer Mathematics, volume 59, number 1-2, pages 37-47, 1995.  Also \u003ca href=\"http://www.cs.wm.edu/~pkstoc/gov.pdf\"\u003eauthor's copy\u003c/a\u003e.  a(n) = d(n) in section 5 exercise 5.",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-1,-2,2,-4).",
				"\u003ca href=\"/index/To#Hanoi\"\u003eIndex entries for sequences related to Towers of Hanoi\u003c/a\u003e"
			],
			"formula": [
				"a(n) = (7*2^n - A341579(n+3) + A341579(n))/2.",
				"a(n) = 3*a(n-1) - a(n-2) - 2*a(n-3) + 2*a(n-4) - 4*a(n-5).",
				"G.f.: x * (1 - x) * (1 + x + x^2) / ( (1 - 2*x) * (1 - x - x^2 - 2*x^4) ).",
				"G.f.: (7/2)/(1 - 2*x) - (1/2)*(7 + 5*x + 3*x^2 + 6*x^3)/(1 - x - x^2 - 2*x^4)."
			],
			"example": [
				"The graph embedding in a triangular grid is (as drawn by Scorer et al.),",
				"                A",
				"               / \\              n=3 disks",
				"              *---*              A to D",
				"             /     \\            geometric",
				"            *       *         length along",
				"           / \\     / \\          the path",
				"          *---B---*---*         a(3) = 8",
				"             /     \\",
				"        *   .       \\   *",
				"       / \\ /         \\ / \\",
				"      *---C           *---*",
				"     /     \\         /     \\",
				"    *       *-------*       *",
				"   / \\     / \\     / \\     / \\",
				"  D---*---*---*   *---*---*---*",
				"B to C is where disks s=1 and s+1=2 exchange which is geometric length 2^s = 2."
			],
			"program": [
				"(PARI) my(p=Mod('x,'x^4-'x^3-'x^2-2), f=7*'x^3+5*'x^2+3*'x+6); a(n) = (7\u003c\u003cn - polcoeff(lift(f*p^n),3))/2;"
			],
			"xref": [
				"Cf. A341579."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Kevin Ryde_, Feb 16 2021",
			"references": 2,
			"revision": 8,
			"time": "2021-02-25T21:18:35-05:00",
			"created": "2021-02-20T00:36:02-05:00"
		}
	]
}