{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A283273",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 283273,
			"data": "1,2,4,3,2,6,3,0,4,6,4,5,3,5,7,0,2,3,4,6,6,7,5,0,3,4,9,5,0,15,4,0,4,3,6,9,6,4,7,0,4,7,7,4,5,9,3,0,3,2,6,9,5,11,12,0,7,5,4,13,0,9,6,0,2,9,11,2,3,6,5,0,4,5,12,6,6,11,5,0,6",
			"name": "Number of ways to write n as x^2 + y^2 + z^2 with x,y integers and z a nonnegative integer such that x + 2*y + 3*z is a square or twice a square.",
			"comment": [
				"Conjecture: (i) For any nonnegative integer n not of the form 4^k*(8*m+7) (k,m = 0,1,2,...), we have a(n) \u003e 0.",
				"(ii) Any nonnegative integer not of the form 4^k*(8*m+7) (k,m = 0,1,2,...) can be written as x^2 + y^2 + z^2 with x,y,z integers such that a*x + b*y + c*z is a square or twice a square, whenever (a,b,c) is among the triples (1,2,5), (1,3,4), (1,4,7), (1,6,7), (2,3,4), (2,3,9), (3,4,7), (3,4,9).",
				"The Gauss-Legendre theorem states that a nonnegative integer can be written as the sum of three squares if and only if it is not of the form 4^k*(8*m+7) with k and m nonnegative integers. Thus a(4^k*(8*m+7)) = 0 for all k,m = 0,1,2,..., and part (i) of our conjecture (verified for n up to 1.6*10^6) is stronger than the Gauss-Legendre theorem.",
				"See also A283269 for a similar conjecture."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A283273/b283273.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2016.11.008\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, J. Number Theory 175(2017), 167-190.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1701.05868\"\u003eRestricted sums of four squares\u003c/a\u003e, arXiv:1701.05868 [math.NT], 2017."
			],
			"example": [
				"a(82) = 1 since 82 = 0^2 + (-1)^2 + 9^2 with 0 + 2*(-1) + 3*9 = 5^2.",
				"a(328) = 1 since 328 = 0^2 + (-2)^2 + 18^2 with 0 + 2*(-2) + 3*18 = 2*5^2.",
				"a(330) = 1 since 330 = 5^2 + 4^2 + 17^2 with 5 + 2*4 + 3*17 = 8^2.",
				"a(466) = 1 since 466 = 21^2 + 0^2 + 5^2 with 21 + 2*0 + 3*5 = 6^2.",
				"a(1320) = 1 since 1320 = 10^2 + 8^2 + 34^2 with 10 + 2*8 + 3*34 = 2*8^2.",
				"a(1387) = 1 since 1387 = 33^2  + (-17)^2 + 3^2 with 33 + 2*(-17) + 3*3 = 2*2^2.",
				"a(1857) = 1 since 1857 = (-37)^2 + (-2)^2 + 22^2 with (-37) + 2*(-2) + 3*22 = 5^2.",
				"a(1864) = 1 since 1864 = 42^2 + 0^2 + 10^2 with 42 + 2*0 + 3*10 = 2*6^2.",
				"a(2386) = 1 since 2386 = (-44)^2 + 3^2 + 21^2 with (-44) + 2*3 + 3*21 = 5^2.",
				"a(5548) = 1 since 5548 = 66^2 + (-34)^2 + 6^2 with 66 + 2*(-34) + 3*6 = 4^2.",
				"a(7428) = 1 since 7428 = (-74)^2 + (-4)^2 + 44^2 with (-74) + 2*(-4) + 3*44 = 2*5^2.",
				"a(9544) = 1 since 9544 = (-88)^2 + 6^2 + 42^2 with (-88) + 2*6 + 3*42 = 2*5^2."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]];",
				"TQ[n_]:=TQ[n]=SQ[n]||SQ[2n];",
				"Do[r=0;Do[If[SQ[n-x^2-y^2]\u0026\u0026TQ[(-1)^i*x+(-1)^j*2y+3*Sqrt[n-x^2-y^2]],r=r+1],{x,0,Sqrt[n]},{y,0,Sqrt[n-x^2]},{i,0,Min[x,1]},{j,0,Min[y,1]}];Print[n,\" \",r];Continue,{n,0,80}]"
			],
			"xref": [
				"Cf. A000290, A005875, A271518, A275344, A283170, A283196, A283204, A283205, A283239, A283269."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Zhi-Wei Sun_, Mar 04 2017",
			"references": 4,
			"revision": 21,
			"time": "2017-03-05T00:57:21-05:00",
			"created": "2017-03-04T13:51:29-05:00"
		}
	]
}