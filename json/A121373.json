{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A121373",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 121373,
			"data": "1,1,-1,0,0,-1,0,-1,0,0,0,0,-1,0,0,1,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,1,0,0,0,0,-1,0,0,0,0,0,0,0,0,0,0,-1,0,0,0,0,0,-1,0,0,0,0,0,0,0,0,0,0,0,0,-1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0",
			"name": "Expansion of f(x) = f(x, -x^2) in powers of x where f(, ) is Ramanujan's general theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"This is an example of the quintuple product identity in the form f(a*b^4, a^2/b) - (a/b) * f(a^4*b, b^2/a) = f(-a*b, -a^2*b^2) * f(-a/b, -b^2) / f(a, b) where a = -x^3, b = -x. - _Michael Somos_, Jul 11 2012",
				"Number 5 of the 14 primitive eta-products which are holomorphic modular forms of weight 1/2 listed by D. Zagier on page 30 of \"The 1-2-3 of Modular Forms\". - _Michael Somos_, May 04 2016"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A121373/b121373.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/QuintupleProductIdentity.html\"\u003eQuintuple Product Identity\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-1/4) * (theta_1( Pi/12, q) + theta_2( Pi/12, q)) / sqrt(6) in powers of q^6. - _Michael Somos_, Jul 06 2013",
				"Expansion of q^(-1/24) * eta(q^2)^3 / (eta(q) * eta(q^4)) in powers of q.",
				"Euler transform of period 4 sequence [1, -2, 1, -1, ...].",
				"a(n) = b(24*n + 1) where b() is multiplicative with b(p^2e) = (-1)^e if p == 7, 11, 13, 17 (mod 24), b(p^2e) = +1 if p == 1, 5, 19, 23 (mod 24) and b(p^(2e-1)) = b(2^e) = b(3^e) = 0 if e\u003e0.",
				"G.f.: (1 + x) * (1 - x^2) * (1 + x^3) * (1 - x^4) * ...",
				"G.f.: 1 + x - x^2*(1 + x) + x^3*(1 + x)*(1 - x^2) - x^4*(1 + x)*(1 - x^2)*(1 + x^3) + ...",
				"a(5*n + 3) = a(5*n + 4) = 0. a(25*n + 1) = a(n).",
				"G.f.: Sum_{k\u003e=0} a(k) * x^(24*k + 1) = Sum_{k in Z} (-1)^floor((k+1)/2) * x^(6*k + 1)^2.",
				"a(n) = (-1)^n * A010815(n). |a(n)| = A080995(n).",
				"Expansion of f(-x^5, -x^7) + x * f(-x, -x^11) in powers of x. - _Michael Somos_, Jan 10 2015",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (2304 t)) = 48^(1/2) (t/i)^(1/2) f(t) where q = exp(2 Pi i t). - _Michael Somos_, May 05 2016",
				"G.f.: exp(Sum_{k\u003e=1} (-1)^(k+1)*x^k/(k*(1 - (-x)^k))). - _Ilya Gutkovskiy_, Jun 08 2018"
			],
			"example": [
				"G.f. = 1 + x - x^2 - x^5 - x^7 - x^12 + x^15 + x^22 + x^26 + x^35 + ...",
				"G.f. = q + q^25 - q^49 - q^121 - q^169 - q^289 + q^361 + q^529 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ Product[ 1 - (-x)^k, {k, n}], {x, 0, n}]; (* _Michael Somos_, Nov 14 2011 *)",
				"a[ n_] := SeriesCoefficient[ QPochhammer[ -x], {x, 0, n}]; (* _Michael Somos_, Jul 06 2013 *)",
				"a[ n_] := SeriesCoefficient[ (EllipticTheta[ 1, Pi/12, x^4] + EllipticTheta[ 2, Pi/12, x^4]) / Sqrt[6], {x, 0, 24 n + 1}] // Simplify; (* _Michael Somos_, Mar 20 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( issquare( 24*n + 1, \u0026n), kronecker( 6, n))};",
				"(PARI) {a(n) = if( n\u003c0, 0, polcoeff( eta( -x + x * O(x^n)), n))};"
			],
			"xref": [
				"Cf. A010815, A080995, A247133, A247223."
			],
			"keyword": "sign",
			"offset": "0,1",
			"author": "_Michael Somos_, Jul 24 2006",
			"references": 1360,
			"revision": 44,
			"time": "2021-03-12T22:24:44-05:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}