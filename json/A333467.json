{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A333467",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 333467,
			"data": "1,1,1,1,0,1,1,1,1,1,1,0,2,0,1,1,1,2,5,3,1,1,0,3,0,17,0,1,1,1,3,15,47,73,15,1,1,0,4,0,138,0,388,0,1,1,1,4,34,306,2021,4720,2461,105,1,1,0,5,0,670,0,43581,0,18155,0,1,1,1,5,65,1270,25050,291001,1295493,1256395,152531,945,1",
			"name": "Array read by antidiagonals: T(n,k) is the number of k-regular multigraphs on n labeled nodes, loops allowed, n \u003e= 0, k \u003e= 0.",
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A333467/b333467.txt\"\u003eTable of n, a(n) for n = 0..405\u003c/a\u003e (diagonals 0..27)"
			],
			"example": [
				"Array begins:",
				"=============================================================",
				"n\\k | 0   1     2       3        4          5           6",
				"----+--------------------------------------------------------",
				"  0 | 1   1     1       1        1          1           1 ...",
				"  1 | 1   0     1       0        1          0           1 ...",
				"  2 | 1   1     2       2        3          3           4 ...",
				"  3 | 1   0     5       0       15          0          34 ...",
				"  4 | 1   3    17      47      138        306         670 ...",
				"  5 | 1   0    73       0     2021          0       25050 ...",
				"  6 | 1  15   388    4720    43581     291001     1594340 ...",
				"  7 | 1   0  2461       0  1295493          0   159207201 ...",
				"  8 | 1 105 18155 1256395 50752145 1296334697 23544232991 ...",
				"  ..."
			],
			"maple": [
				"b:= proc(l, i) option remember; (n-\u003e `if`(n=0, 1,",
				"     `if`(l[n]=0, b(sort(subsop(n=[][], l)), n-1),",
				"     `if`(i\u003c1, 0, b(l, i-1)+`if`(i=n, `if`(l[n]\u003e1,",
				"      b(subsop(n=l[n]-2, l), i), 0), `if`(l[i]\u003e0,",
				"      b(subsop(i=l[i]-1, n=l[n]-1, l), i), 0))))))(nops(l))",
				"    end:",
				"A:= (n, k)-\u003e b([k$n], n):",
				"seq(seq(A(n, d-n), n=0..d), d=0..12);  # _Alois P. Heinz_, Mar 23 2020"
			],
			"mathematica": [
				"b[l_, i_] := b[l, i] = Function[n, If[n == 0, 1, If[l[[n]] == 0, b[Sort[ ReplacePart[l, n -\u003e Nothing]], n-1], If[i \u003c 1, 0, b[l, i-1] + If[i == n, If[l[[n]] \u003e 1, b[ReplacePart[l, n -\u003e l[[n]]-2], i], 0], If[l[[i]] \u003e 0, b[ReplacePart[l, {i -\u003e l[[i]]-1, n -\u003e l[[n]]-1}], i], 0]]]]]][Length[l]];",
				"A[n_, k_] := b[Table[k, {n}], n];",
				"Table[A[n, d-n], {d, 0, 12}, {n, 0, d}] // Flatten (* _Jean-François Alcover_, Apr 07 2020, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(PARI)",
				"MultigraphsWLByDegreeSeq(n, limit, ok)={",
				"  local(M=Map(Mat([0, 1])));",
				"  my(acc(p, v)=my(z); mapput(M, p, if(mapisdefined(M, p, \u0026z), z+v, v)));",
				"  my(recurse(r, h, p, q, v, e) = if(!p, if(ok(x^e+q, r), acc(x^e+q, v)), my(i=poldegree(p), t=pollead(p)); self()(r, limit, p-t*x^i, q+t*x^i, v, e); for(m=1, h-i, for(k=1, min(t, (limit-e)\\m), self()(r, if(k==t, limit, i+m-1), p-k*x^i, q+k*x^(i+m), binomial(t, k)*v, e+k*m)))));",
				"  for(r=1, n, my(src=Mat(M)); M=Map(); for(i=1, matsize(src)[1], forstep(e=0, limit, 2, recurse(n-r, limit, src[i, 1], 0, src[i, 2], e)))); Mat(M);",
				"}",
				"T(n, k)={if(n%2\u0026\u0026k%2, 0, vecsum(MultigraphsWLByDegreeSeq(n, k, (p, r)-\u003esubst(deriv(p), x, 1)\u003e=(n-2*r)*k)[, 2]))}",
				"{ for(n=0, 8, for(k=0, 6, print1(T(n, k), \", \")); print) }"
			],
			"xref": [
				"Rows n=0..3 are A000012, A059841, A008619, A006003.",
				"Columns k=0..4 are A000012, A123023, A002135, A005814, A005816.",
				"Cf. A059441 (graphs), A167625 (unlabeled nodes), A333351 (without loops)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,13",
			"author": "_Andrew Howroyd_, Mar 23 2020",
			"references": 6,
			"revision": 16,
			"time": "2020-04-07T15:27:43-04:00",
			"created": "2020-03-23T21:07:26-04:00"
		}
	]
}