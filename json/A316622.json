{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A316622",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 316622,
			"data": "1,1,1,1,1,1,1,2,6,1,1,2,48,168,1,1,4,96,11232,20160,1,1,2,480,86016,24261120,9999360,1,1,6,288,1488000,1321205760,475566474240,20158709760,1,1,4,2016,1886976,116064000000,335522845163520,84129611558952960,163849992929280,1",
			"name": "Array read by antidiagonals: T(n,k) is the order of the group GL(n,Z_k).",
			"comment": [
				"All rows are multiplicative.",
				"Equivalently, the number of invertible n X n matrices mod k.",
				"Also, for k prime (but not higher prime powers) the number of nonsingular n X n matrices over GF(k)."
			],
			"link": [
				"R. P. Brent and B. D. McKay, \u003ca href=\"https://doi.org/10.1016/0012-365X(87)90117-8\"\u003eDeterminants and ranks of random matrices over Zm\u003c/a\u003e, Discrete Mathematics 66 (1987) pp. 35-49.",
				"J. M. Lockhart and W. P. Wardlaw, \u003ca href=\"https://www.jstor.org/stable/27643029\"\u003eDeterminants of Matrices over the Integers Modulo m\u003c/a\u003e, Mathematics Magazine, Vol. 80, No. 3 (Jun., 2007), pp. 207-214.",
				"The Group Properties Wiki, \u003ca href=\"https://groupprops.subwiki.org/wiki/Order_formulas_for_linear_groups\"\u003eOrder formulas for linear groups\u003c/a\u003e"
			],
			"formula": [
				"T(n,p^e) = (p^e)^(n^2) * Product_{j=1..n} (1 - 1/p^j) for prime p."
			],
			"example": [
				"Array begins:",
				"=================================================================",
				"n\\k| 1       2         3          4             5           6",
				"---+-------------------------------------------------------------",
				"0  | 1       1         1          1            1            1 ...",
				"1  | 1       1         2          2            4            2 ...",
				"2  | 1       6        48         96          480          288 ...",
				"3  | 1     168     11232      86016      1488000      1886976 ...",
				"4  | 1   20160  24261120 1321205760 116064000000 489104179200 ...",
				"5  | 1 9999360  ...",
				"..."
			],
			"mathematica": [
				"T[_, 1] = T[0, _] = 1; T[n_, k_] := T[n, k] = Module[{f = FactorInteger[k], p, e}, If[Length[f] == 1, {p, e} = f[[1]]; (p^e)^(n^2)* Product[(1 - 1/p^j), {j, 1, n}], Times @@ (T[n, Power @@ #]\u0026 /@ f)]];",
				"Table[T[n - k + 1, k], {n, 0, 8}, {k, n + 1, 1, -1}] // Flatten (* _Jean-François Alcover_, Jul 25 2019 *)"
			],
			"program": [
				"(GAP)",
				"T:=function(n,k) if k=1 or n=0 then return 1; else return Order(GL(n, Integers mod k)); fi; end;",
				"for n in [0..5] do Print(List([1..6], k-\u003eT(n,k)), \"\\n\"); od;",
				"(PARI) T(n,k)={my(f=factor(k)); k^(n^2) * prod(i=1, #f~, my(p=f[i,1]); prod(j=1, n, (1 - p^(-j))))}"
			],
			"xref": [
				"Rows n=2..4 are A000252, A064767, A305186.",
				"Columns k=2..7 are A002884, A053290, A065128, A053292, A065498, A053293.",
				"Cf. A053291 (GF(4)), A052496 (GF(8)), A052497 (GF(9)).",
				"Cf. A316623."
			],
			"keyword": "nonn,mult,tabl",
			"offset": "0,8",
			"author": "_Andrew Howroyd_, Jul 08 2018",
			"references": 11,
			"revision": 9,
			"time": "2019-07-25T16:57:29-04:00",
			"created": "2018-07-08T20:00:06-04:00"
		}
	]
}