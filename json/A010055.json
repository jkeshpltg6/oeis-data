{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A010055",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 10055,
			"data": "1,1,1,1,1,0,1,1,1,0,1,0,1,0,0,1,1,0,1,0,0,0,1,0,1,0,1,0,1,0,1,1,0,0,0,0,1,0,0,0,1,0,1,0,0,0,1,0,1,0,0,0,1,0,0,0,0,0,1,0,1,0,0,1,0,0,1,0,0,0,1,0,1,0,0,0,0,0,1,0",
			"name": "1 if n is a prime power p^k (k \u003e= 0), otherwise 0.",
			"comment": [
				"Characteristic function of unit or prime powers p^k (k \u003e= 1). Characteristic function of prime powers p^k (k \u003e= 0). - _Daniel Forgues_, Mar 03 2009",
				"See A065515 for partial sums. - _Reinhard Zumkeller_, Nov 22 2009"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A010055/b010055.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Ch#char_fns\"\u003eIndex entries for characteristic functions\u003c/a\u003e"
			],
			"formula": [
				"Dirichlet generating function: 1 + ppzeta(s). Here ppzeta(s) = Sum_{p prime} Sum_{k\u003e=1} 1/(p^k)^s. Note that ppzeta(s) = Sum_{p prime} 1/(p^s-1) = Sum_{k\u003e=1} primezeta(k*s). - _Franklin T. Adams-Watters_, Sep 11 2005",
				"a(n) = 0^(A119288(n)-1). - _Reinhard Zumkeller_, May 13 2006",
				"a(A000961(n)) = 1; a(A024619(n)) = 0. - _Reinhard Zumkeller_, Nov 17 2011",
				"a(n) = if A001221(n) \u003c= 1 then 1, otherwise 0. - _Reinhard Zumkeller_, Nov 28 2015",
				"If n \u003e= 2, a(n) = A069513(n). - _Jeppe Stig Nielsen_, Feb 02 2016",
				"Conjecture: a(n) = (n - A048671(n))/A000010(n) for all n \u003e 1. - _Velin Yanev_, Mar 10 2021 [The conjecture is true. - _Andrey Zabolotskiy_, Mar 11 2021]"
			],
			"maple": [
				"A010055 := proc(n)",
				"    if n =1 then",
				"        1;",
				"    else",
				"        if nops(ifactors(n)[2]) = 1 then",
				"            1;",
				"        else",
				"            0 ;",
				"        end if;",
				"    end if;",
				"end proc: # _R. J. Mathar_, May 25 2017"
			],
			"mathematica": [
				"A010055[n_]:=Boole[PrimeNu[n]\u003c=1]; A010055/@Range[20] (* _Enrique Pérez Herrero_, May 30 2011 *)",
				"{1}~Join~Table[Boole@ PrimePowerQ@ n, {n, 2, 105}] (* _Michael De Vlieger_, Feb 02 2016 *)"
			],
			"program": [
				"(PARI) for(n=1,120,print1(omega(n)\u003c=1,\",\"))",
				"(Haskell)",
				"a010055 n = if a001221 n \u003c= 1 then 1 else 0",
				"-- _Reinhard Zumkeller_, Nov 28 2015, Mar 19 2013, Nov 17 2011"
			],
			"xref": [
				"Cf. A069513 (1 if n is a prime power p^k (k \u003e= 1), else 0.)",
				"Cf. A268340.",
				"Cf. A100995.",
				"Cf. A001221, A000961, A024619, A065515,"
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Charles R Greathouse IV_, Mar 12 2008",
				"Edited by _Daniel Forgues_, Mar 02 2009",
				"Comment re Galois fields moved to A069513 by _Franklin T. Adams-Watters_, Nov 02 2009"
			],
			"references": 82,
			"revision": 64,
			"time": "2021-03-13T00:09:47-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}