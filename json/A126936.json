{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A126936",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 126936,
			"data": "1,6,4,42,60,24,308,688,560,160,2310,7080,8760,5040,1120,17556,68712,114576,99456,44352,8064,134596,642824,1351840,1572480,1055040,384384,59136,1038312,5864640,14912064,21778560,19536000,10695168,3294720",
			"name": "Coefficients of a polynomial representation of the integral of 1/(x^4 + 2*a*x^2 + 1)^(n+1) from x = 0 to infinity.",
			"comment": [
				"The integral N(a;n) = Integral_{x=0..infinity} 1/(x^4 + 2*a*x^2 + 1)^(n+1) has a polynomial representation P_n(a) = 2^(n + 3/2) * (a+1)^(n + 1/2) * N(a;n) / Pi (known as the Boros-Moll polynomial). The table contains the coefficients T(n,l) of P_n(a) = 2^(-2*n)*Sum_{l=0..n} T(n,l)*a^l in row n and column l (with n \u003e= 0 and 0 \u003c= l \u003c= n)."
			],
			"link": [
				"Tewodros Amdeberhan and Victor H. Moll, \u003ca href=\"http://arxiv.org/abs/0707.2118\"\u003e A formula for a quartic integral: a survey of old proofs and some new ones\u003c/a\u003e, arXiv:0707.2118 [math.CA], 2007.",
				"George Boros and Victor H. Moll, \u003ca href=\"http://dx.doi.org/10.1016/S0377-0427(99)00081-3\"\u003eAn integral hidden in Gradshteyn and Ryzhik\u003c/a\u003e, Journal of Computational and Applied Mathematics, 106(2) (1999), 361-368.",
				"William Y. C. Chen and Ernest X. W. Xia, \u003ca href=\"http://arxiv.org/abs/0806.4333\"\u003e The Ratio Monotonicity of the Boros-Moll Polynomials\u003c/a\u003e, arXiv:0806.4333 [math.CO], 2009.",
				"William Y. C. Chen and Ernest X. W. Xia, \u003ca href=\"https://doi.org/10.1090/S0025-5718-09-02223-6\"\u003e The Ratio Monotonicity of the Boros-Moll Polynomials\u003c/a\u003e, Mathematics of Computation, 78(268) (2009), 2269-2282.",
				"Victor H. Moll, \u003ca href=\"http://www.ams.org/notices/200203/fea-moll.pdf\"\u003eThe evaluation of integrals: a personal story\u003c/a\u003e, Notices Amer. Math. Soc., 49 (No. 3, March 2002), 311-317.",
				"Victor H. Moll, \u003ca href=\"https://web.math.rochester.edu/misc/ojac/vol2/Moll_2007.pdf\"\u003eCombinatorial sequences arising from a rational integral\u003c/a\u003e, Onl. J. Anal. Combin., no 2 (2007), #4."
			],
			"formula": [
				"From _Petros Hadjicostas_, May 25 2020: (Start)",
				"T(n,l) = A067001(n, n-l) = 2^(2*n) * A223549(n,l)/A223550(n,l).",
				"Sum_{l=0..n} T(n,l) = A002458(n) = A334907(n)*2^n/n!.",
				"Bivariate o.g.f.: Sum_{n,l \u003e= 0} T(n,l)*x^n*y^l = sqrt((1 + y)/(1 - 8*x*(1 + y))/(y + sqrt(1 - 8*x*(1 + y)))). (End)"
			],
			"example": [
				"The table T(n,l) (with rows n \u003e= 0 and columns l = 0..n) starts:",
				"      1;",
				"      6,     4;",
				"     42,    60,     24;",
				"    308,   688,    560,   160;",
				"   2310,  7080,   8760,  5040,  1120;",
				"  17556, 68712, 114576, 99456, 44352, 8064;",
				"  ...",
				"For n = 2, N(a;2) = Integral_{x=0..oo} dx/(x^4 + 2*a*x + 1)^3 = 2^(-2*2)*(Sum_{l=0..2} T(2,l)*a^l) * Pi/(2^(2 + 3/2) * (a + 1)^(2 + 1/2) = (42 + 60*a + 24*a^2) * Pi/(32 * (2*(a+1))^(5/2)) for a \u003e -1. - _Petros Hadjicostas_, May 25 2020"
			],
			"maple": [
				"A126936 := proc(m, l)",
				"    add(2^k*binomial(2*m-2*k, m-k)*binomial(m+k, m)*binomial(k, l), k=l..m):",
				"end:",
				"seq(seq(A126936(m,l), l=0..m), m=0..12); # _R. J. Mathar_, May 25 2020"
			],
			"mathematica": [
				"t[m_, l_] := Sum[2^k*Binomial[2*m-2*k, m-k]*Binomial[m+k, m]*Binomial[k, l], {k, l, m}]; Table[t[m, l], {m, 0, 11}, {l, 0, m}] // Flatten (* _Jean-François Alcover_, Jan 09 2014, after Maple, adapted May 2020 *)"
			],
			"xref": [
				"Cf. A002458 (row sums), A004982 (column l=0), A059304 (main diagonal), A067001 (rows reversed), A223549, A223550, A334907."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,2",
			"author": "_R. J. Mathar_, Mar 17 2007",
			"ext": [
				"Corrected by _Petros Hadjicostas_, May 23 2020"
			],
			"references": 2,
			"revision": 38,
			"time": "2020-05-27T02:09:40-04:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}