{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A241909",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 241909,
			"data": "1,2,4,3,8,9,16,5,6,27,32,25,64,81,18,7,128,15,256,125,54,243,512,49,12,729,10,625,1024,75,2048,11,162,2187,36,35,4096,6561,486,343,8192,375,16384,3125,50,19683,32768,121,24,45,1458,15625,65536,21,108,2401",
			"name": "Self-inverse permutation of natural numbers: a(1)=1, a(p_i) = 2^i, and if n = p_i1 * p_i2 * p_i3 * ... * p_{ik-1} * p_ik, where p's are primes, with their indexes are sorted into nondescending order: i1 \u003c= i2 \u003c= i3 \u003c= ... \u003c= i_{k-1} \u003c= ik, then a(n) = 2^(i1-1) * 3^(i2-i1) * 5^(i3-i2) * ... * p_k^(1+(ik-i_{k-1})). Here k = A001222(n) and ik = A061395(n).",
			"comment": [
				"This permutation maps between the partitions as ordered in A112798 and A241918 (the original motivation for this sequence).",
				"For all n \u003e 2, A007814(a(n)) = A055396(n)-1, which implies that this self-inverse permutation maps between primes (A000040) and the powers of two larger than one (A000079(n\u003e=1)), and apart from a(1) \u0026 a(2), this also maps each even number to some odd number, and vice versa, which means there are no fixed points after 2.",
				"A122111 commutes with this one, that is, a(n) = A122111(a(A122111(n))).",
				"Conjugates between A243051 and A242424 and other rows of A243060 and A243070."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A241909/b241909.txt\"\u003eTable of n, a(n) for n = 1..1024\u003c/a\u003e",
				"A. Karttunen, \u003ca href=\"/A122111/a122111.txt\"\u003eA few notes on A122111, A241909 \u0026 A241916.\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"If n is a prime with index i (p_i), then a(n) = 2^i, otherwise when n = p_i1 * p_i2 * p_i3 * ... p_ik, where p_i1, p_i2, p_i3, ..., p_ik are the primes present (not necessarily all distinct) in the prime factorization of n, sorted into nondescending order, a(n) = 2^(i1-1) * 3^(i2-i1) * 5^(i3-i2) * ... * p_k^(1+(ik-i_{k-1})).",
				"Equally, if n = 2^k, then a(n) = p_k, otherwise, when n = 2^e1 * 3^e2 * 5^e3 * ... * p_k^{e_k}, i.e., where e1 ... e_k are the exponents (some of them possibly zero, except the last) of the primes 2, 3, 5, ... in the prime factorization of n, a(n) = p_{1+e1} * p_{1+e1+e2} * p_{1+e1+e2+e3} * ... * p_{e1+e2+e3+...+e_k}.",
				"From the equivalence of the above two formulas (which are inverses of each other) it follows that a(a(n)) = n, i.e., that this permutation is an involution. For a proof, please see the attached notes.",
				"The first formula corresponds to this recurrence:",
				"a(1) = 1, a(p_k) = 2^k for primes with index k, otherwise a(n) = (A000040(A001222(n))^(A241917(n)+1)) * A052126(a(A052126(n))).",
				"And the latter formula with this recurrence:",
				"a(1) = 1, and for n\u003e1, if n = 2^k, a(n) = A000040(k), otherwise a(n) = A000040(A001511(n)) * A242378(A007814(n), a(A064989(n))).",
				"[Here A242378(k,n) changes each prime p(i) in the prime factorization of n to p(i+k), i.e., it's the result of A003961 iterated k times starting from n.]",
				"We also have:",
				"a(1)=1, and for n\u003e1, a(n) = Product_{i=A203623(n-1)+2..A203623(n)+1} A000040(A241918(i)).",
				"For all n \u003e= 1, A001222(a(n)) = A061395(n), and vice versa, A061395(a(n)) = A001222(n).",
				"For all n \u003e 1, a(2n-1) = 2*a(A064216(n))."
			],
			"example": [
				"For n = 12 = 2 * 2 * 3 = p_1 * p_1 * p_2, we obtain by the first formula 2^(1-1) * 3^(1-1) * 5^(1+(2-1)) = 5^2 = 25. By the second formula, as n = 2^2 * 3^1, we obtain the same result, p_{1+2} * p_{2+1} = p_3 * p_3 = 25, thus a(12) = 25.",
				"Using the product formula over the terms of row n of table A241918, we see, because 9450 = 2*3*3*3*5*5*7 = p_1^1 * p_2^3 * p_3^2 * p_4^1, that the corresponding row in A241918 is {2,5,7,7}, and multiplying p_2 * p_5 * p_7^2 yields 3 * 11 * 17 * 17 = 9537, thus a(9450) = 9537.",
				"Similarly, for 9537, the corresponding row in A241918 is {1,2,2,2,3,3,4}, and multiplying p_1^1 * p_2^3 * p_3^2 * p_4^1, we obtain 9450 back."
			],
			"mathematica": [
				"Array[If[# == 1, 1, Function[t, Times @@ Prime@ Accumulate[If[Length@ t \u003c 2, {0}, Join[{1}, ConstantArray[0, Length@ t - 2], {-1}]] + ReplacePart[t, Map[#1 -\u003e #2 \u0026 @@ # \u0026, #]]]]@ ConstantArray[0, Transpose[#][[1, -1]]] \u0026[FactorInteger[#] /. {p_, e_} /; p \u003e 0 :\u003e {PrimePi@ p, e}]] \u0026, 56] (* _Michael De Vlieger_, Jan 23 2020 *)"
			],
			"program": [
				"(Scheme, with _Antti Karttunen_'s IntSeq-library)",
				"(definec (A241909 n) (cond ((\u003c= n 1) n) ((prime? n) (A000079 (A049084 n))) (else (* (A052126 (A241909 (A052126 n))) (expt (A000040 (A001222 n)) (+ 1 (A241917 n)))))))",
				";; Another recursive version:",
				"(definec (A241909 n) (cond ((\u003c= n 2) n) ((pow2? n) (A000040 (A007814 n))) (else (* (A000040 (A001511 n)) (A242378bi (A007814 n) (A241909 (A064989 n))))))) ;; The function A242378bi is given in A242378.",
				"(define (pow2? n) (let loop ((n n) (i 0)) (cond ((zero? n) #f) ((odd? n) (and (= 1 n) i)) (else (loop (/ n 2) (1+ i)))))) ;; Gives non-false only when n is a power of two.",
				"(Haskell)",
				"a241909 1 = 1",
				"a241909 n = product $ zipWith (^) a000040_list $ zipWith (-) is (1 : is)",
				"            where is = reverse ((j + 1) : js)",
				"                  (j:js) = reverse $ map a049084 $ a027746_row n",
				"-- _Reinhard Zumkeller_, Aug 04 2014",
				"(PARI) A241909(n) = if(1==n||isprime(n),2^primepi(n),my(f=factor(n),h=1,i,m=1,p=1,k=1); while(k\u003c=#f~, p = nextprime(1+p); i = primepi(f[k,1]); m *= p^(i-h); h = i; if(f[k,2]\u003e1, f[k,2]--, k++)); (p*m)); \\\\ _Antti Karttunen_, Jan 17 2020"
			],
			"xref": [
				"Cf. A203623, A241918, A242378, A007814, A064989, A064216, A001222, A061395, A125976, A243060, A243070.",
				"Cf. also A278220 (= A046523(a(n)), A331280 (its rgs_transform), A331299 (= min(n,a(n))).",
				"{A000027, A122111, A241909, A241916} form a 4-group.",
				"Cf. A049084, A027746."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Antti Karttunen_, May 03 2014, partly inspired by _Marc LeBrun_'s Jan 11 2006 message on SeqFan mailing list.",
			"ext": [
				"Typos in the name corrected May 31 2014 by _Antti Karttunen_, Jan 17 2020"
			],
			"references": 52,
			"revision": 51,
			"time": "2020-01-23T21:32:05-05:00",
			"created": "2014-05-20T23:29:09-04:00"
		}
	]
}