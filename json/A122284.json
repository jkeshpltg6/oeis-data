{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A122284",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 122284,
			"data": "0,1,0,2,1,0,3,3,1,0,4,2,2,1,0,5,7,3,2,1,0,6,8,4,3,2,1,0,7,6,6,5,3,2,1,0,8,5,5,4,5,3,2,1,0,9,4,7,6,6,6,3,2,1,0,10,17,8,7,4,5,6,3,2,1,0,11,18,9,8,7,4,4,4,3,2,1,0,12,20,10,12,8,7,5,5,4,3,2,1,0,13,22,14,13,12",
			"name": "Signature permutations of NEPEED-transformations of non-recursive Catalan automorphisms in table A089840.",
			"comment": [
				"Row n is the signature permutation of the Catalan automorphism which is obtained from the n-th nonrecursive automorphism in the table A089840 with the recursion scheme \"NEPEED\". In this recursion scheme the algorithm first recurses down to all subtrees, before the given automorphism is applied at the root of general tree. I.e., this corresponds to the post-order (postfix) traversal of a Catalan structure, when it is interpreted as a general tree. The associated Scheme-procedures NEPEED and !NEPEED can be used to obtain such a transformed automorphism from any constructively or destructively implemented automorphism. Each row occurs only once in this table. Inverses of these permutations can be found in table A122283.",
				"The recursion scheme KROF (described in A122202) is equivalent to a composition of recursion schemes ENIPS (described in A122204) and NEPEED, i.e., KROF(f) = NEPEED(ENIPS(f)) holds for all Catalan automorphisms f. Because of the \"universal property of folds\", these recursion schemes have well-defined inverses, that is, they are bijective mappings on the set of all Catalan automorphisms. Thus we can equivalently define that NEPEED(f) = KROF(ENIPS^{-1}(f)). Specifically, if g = ENIPS(f), then (f s) = (g (cons (car s) (g^{-1} (cdr s)))), that is, to obtain an automorphism f which gives g when subjected to recursion scheme ENIPS, we compose g with its own inverse applied to the cdr-branch of a S-expression (i.e., the right subtree in the context of binary trees). This implies that for any non-recursive automorphism f in the table A089840, ENIPS^{-1}(f) is also in A089840, which in turn implies that the rows of table A122284 form a (proper) subset of the rows of table A122202. E.g., row 1 of A122284 is row 15 of A122202, row 2 of A122284 is row 3617 of A122202, row 12 of A122284 is row 65167 of A122202, row 15 of A122284 is row 169 of A122202. - _Antti Karttunen_, May 25 2007",
				"The recursion scheme FORK (described in A122201) is equivalent to a composition of recursion schemes SPINE (described in A122203) and DEEPEN, i.e., FORK(f) = DEEPEN(SPINE(f)) holds for all Catalan automorphisms f. These recursion schemes have well-defined inverses, that is, they are bijective mappings on the set of all Catalan automorphisms. Thus we can equivalently define that DEEPEN(f) = FORK(SPINE^{-1}(f)). Specifically, if g = SPINE(f), then (f s) = (cond ((pair? s) (let ((t (g s))) (cons (car t) (g^{-1} (cdr t))))) (else s)) that is, to obtain an automorphism f which gives g when subjected to recursion scheme SPINE, we compose g with its own inverse applied to the cdr-branch of a S-expression. This implies that for any non-recursive automorphism f in the table A089840, SPINE^{-1}(f) is also in A089840, which in turn implies that the rows of table A122283 form a (proper) subset of the rows of table A122201. E.g., row 1 of A122283 is row 21 of A122201, row 2 of A122283 is row 3613 of A122201, row 17 of A122283 is row 65352 of A122201, row 21 of A122283 is row 251 of A122201. - _Antti Karttunen_, May 25 2007"
			],
			"reference": [
				"A. Karttunen, paper in preparation, draft available by e-mail."
			],
			"link": [
				"\u003ca href=\"/index/Per#IntegerPermutationCatAuto\"\u003eIndex entries for signature-permutations of Catalan automorphisms\u003c/a\u003e"
			],
			"program": [
				"(Scheme:) (define (NEPEED foo) (letrec ((bar (lambda (s) (foo (map bar s))))) bar))",
				"(define (!NEPEED foo!) (letrec ((bar! (lambda (s) (for-each bar! s) (foo! s) s))) bar!))"
			],
			"xref": [
				"The first 22 rows of this table: row 0 (identity permutation): A001477, 1: A122302, 2: A122300, 3: A122304, 4: A122310, 5: A122308, 6: A122306, 7: A122312, 8: A122314, 9: A122320, 10: A122318, 11: A122316, 12: A122332, 13: A122334, 14: A122336, 15: A122340, 16: A122338, 17: A122322, 18: A122324, 19: A122326, 20: A122330, 21: A122328. See also tables A089840, A122200, A122201-A122204, A122285-A122288, A122289-A122290."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Antti Karttunen_, Sep 01 2006",
			"references": 38,
			"revision": 11,
			"time": "2014-03-17T01:53:28-04:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}