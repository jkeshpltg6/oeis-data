{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A236076",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 236076,
			"data": "1,0,2,0,1,3,0,0,3,5,0,0,1,7,8,0,0,0,4,15,13,0,0,0,1,12,30,21,0,0,0,0,5,31,58,34,0,0,0,0,1,18,73,109,55,0,0,0,0,0,6,54,162,201,89,0,0,0,0,0,1,25,145,344,365,144,0,0,0,0,0,0,7,85,361",
			"name": "A skewed version of triangular array A122075.",
			"comment": [
				"Triangle T(n,k), 0 \u003c= k \u003c= n, read by rows, given by (0, 1/2, -1/2, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (2, -1/2, -1/2, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938.",
				"Subtriangle of the triangle A122950."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A236076/b236076.txt\"\u003eRows n = 0..125 of triangle, flattened\u003c/a\u003e",
				"H. Fuks and J.M.G. Soto, \u003ca href=\"http://arxiv.org/abs/1306.1189\"\u003eExponential convergence to equilibrium in cellular automata asymptotically emulating identity\u003c/a\u003e, arXiv:1306.1189 [nlin.CG], 2013."
			],
			"formula": [
				"G.f.: (1+x*y)/(1 - x*y - x^2*y - x^2*y^2).",
				"T(n,k) = T(n-1,k-1) + T(n-2,k-1) + T(n-2,k-2), T(0,0)=1, T(1,0) = 0, T(1,1) = 2, T(n,k) = 0 if k \u003c 0 or if k \u003e n.",
				"Sum_{k=0..n} T(n,k) = 2^n = A000079(n).",
				"Sum_{n\u003e=k} T(n,k) = A078057(k) = A001333(k+1).",
				"T(n,n) = Fibonacci(n+2) = A000045(n+2).",
				"T(n+1,n) = A023610(n-1), n \u003e= 1.",
				"T(n+2,n) = A129707(n)."
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  0,  2;",
				"  0,  1,  3;",
				"  0,  0,  3,  5;",
				"  0,  0,  1,  7,  8;",
				"  0,  0,  0,  4, 15, 13;",
				"  0,  0,  0,  1, 12, 30, 21;",
				"  0,  0,  0,  0,  5, 31, 58, 34;"
			],
			"mathematica": [
				"T[n_, k_]:= If[k\u003c0 || k\u003en, 0, If[n==0 \u0026\u0026 k==0, 1, If[k==0, 0, If[n==1 \u0026\u0026 k==1, 2, T[n-1, k-1] + T[n-2, k-1] + T[n-2, k-2]]]]]; Table[T[n,k], {n,0,10}, {k,0,n}]//Flatten (* _G. C. Greubel_, May 21 2019 *)"
			],
			"program": [
				"(Haskell)",
				"a236076 n k = a236076_tabl !! n !! k",
				"a236076_row n = a236076_tabl !! n",
				"a236076_tabl = [1] : [0, 2] : f [1] [0, 2] where",
				"   f us vs = ws : f vs ws where",
				"     ws = [0] ++ zipWith (+) (zipWith (+) ([0] ++ us) (us ++ [0])) vs",
				"-- _Reinhard Zumkeller_, Jan 19 2014",
				"(PARI)",
				"{T(n,k) = if(k\u003c0 || k\u003en, 0, if(n==0 \u0026\u0026 k==0, 1, if(k==0, 0, if(n==1 \u0026\u0026 k==1, 2, T(n-1,k-1) + T(n-2,k-1) + T(n-2,k-2) ))))}; \\\\ _G. C. Greubel_, May 21 2019",
				"(Sage)",
				"def T(n, k):",
				"    if (k\u003c0 or k\u003en): return 0",
				"    elif (n==0 and k==0): return 1",
				"    elif (k==0): return 0",
				"    elif (n==1 and k==1): return 2",
				"    else: return T(n-1,k-1) + T(n-2,k-1) + T(n-2,k-2)",
				"[[T(n, k) for k in (0..n)] for n in (0..10)] # _G. C. Greubel_, May 21 2019"
			],
			"xref": [
				"Cf. variant: A055830, A122075, A122950, A208337.",
				"Cf. A167704 (diagonal sums), A000079 (row sums).",
				"Cf. A111006."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,3",
			"author": "_Philippe Deléham_, Jan 19 2014",
			"references": 1,
			"revision": 18,
			"time": "2019-05-21T03:48:37-04:00",
			"created": "2014-01-19T04:45:56-05:00"
		}
	]
}