{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A333855",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 333855,
			"data": "17,31,33,41,43,51,57,63,65,73,85,89,91,93,97,99,105,109,113,117,119,123,127,129,133,137,145,151,153,155,157,161,165,171,177,185,187,189,193,195,201,205,209,215,217,219,221,223,229,231,233,241,247,249,251,255",
			"name": "Numbers 2*k + 1 with A135303(k) \u003e= 2, for k \u003e= 1, sorted increasingly.",
			"comment": [
				"These are the numbers a(n) for which there is more than one periodic Schick sequence. In his notation B(a(n)) \u003e= 2, for n \u003e= 1.",
				"These are also the numbers a(n) for which there is more than one coach in the complete coach system Sigma(b = a(n)) of Hilton and Pedersen, for n \u003e= 1",
				"These are the numbers a(n) for which there is more than one cycle in the complete system MDS(a(n)) (Modified Doubling Sequence) proposed in the comment by _Gary W. Adamson_, Aug 20 2019, in A003558.",
				"The complement relative to the odd numbers \u003e= 3 is given in A333854.",
				"The subsequence for odd primes is identical with A268923."
			],
			"reference": [
				"Peter Hilton and Jean Pedersen, A Mathematical Tapestry: Demonstrating the Beautiful Unity of Mathematics, Cambridge University Press, 2010, pp. 261-264.",
				"Carl Schick, Trigonometrie und unterhaltsame Zahlentheorie, Bokos Druck, Zürich, 2003 (ISBN 3-9522917-0-6). Tables 3.1 to 3.10, for odd p = 3..113 (with gaps), pp. 158-166."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A333855/b333855.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Wolfdieter Lang, \u003ca href=\"https://arxiv.org/abs/2008.04300\"\u003eOn the Equivalence of Three Complete Cyclic Systems of Integers\u003c/a\u003e, arXiv:2008.04300 [math.NT], 2020"
			],
			"formula": [
				"Sequence {a(n)}_{n\u003e=1} of numbers 2*k + 1 satisfying A135303(k) \u003e= 2, for k \u003e= 1, ordered increasingly."
			],
			"mathematica": [
				"1 + 2 Select[Range[2, 127], 2 \u003c= EulerPhi[#2]/(2 If[#2 \u003e 1 \u0026\u0026 GCD[#1, #2] == 1, Min[MultiplicativeOrder[#1, #2, {-1, 1}]], 0]) \u0026 @@ {2, 2 # + 1} \u0026] (* _Michael De Vlieger_, Oct 15 2020 *)"
			],
			"program": [
				"(PARI) isok8(m, n) = my(md = Mod(2, 2*n+1)^m); (md==1) || (md==-1);",
				"A003558(n) = my(m=1); while(!isok8(m, n) , m++); m;",
				"isok(m) = (m%2) \u0026\u0026 eulerphi(m)/(2*A003558((m-1)/2)) \u003e= 2; \\\\ _Michel Marcus_, Jun 09 2020"
			],
			"xref": [
				"Cf. A003558, A135303, A216371, A268923, A333854."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Wolfdieter Lang_, May 12 2020",
			"references": 6,
			"revision": 20,
			"time": "2020-10-15T16:34:53-04:00",
			"created": "2020-06-12T06:17:03-04:00"
		}
	]
}