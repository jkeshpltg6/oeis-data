{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A143514",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 143514,
			"data": "1,2,3,5,4,6,13,7,9,8,34,10,12,11,16,89,18,15,14,19,21,233,26,23,17,22,24,29,610,47,31,20,25,27,32,37,1597,68,39,28,33,30,35,40,42,4181,123,60,36,41,38,43,48,45,50,10946,178,81,44,49,46,51,56,53,58",
			"name": "Array D of denominators of Best Remaining Lower Approximates of x=(1+sqrt(5))/2, by antidiagonals.",
			"comment": [
				"(1) Row 1 of R consists of lower principal convergents to x.",
				"(2) (row limits of R) = x; (column limits of R) = 0.",
				"(3) Every positive integer occurs exactly once in D, so that as a sequence, A143514 is a permutation of the positive integers.",
				"(4) p=floor(q*r) for every p/q in R. Consequently, the terms of N are distinct and their ordered union is the lower Wythoff sequence, A000201.",
				"(5) Conjecture: Every (N(n,k+1)-N(n,k))/(D(n,k+1)-D(n,k)) is a principal convergent to x.",
				"(6) Suppose n\u003e=1 and p/q and s/t are consecutive terms in row n of R. Then (conjecture) q*s-p*t=n.",
				"In general, for irrational r, let {n*r} denote the fractional part of n*r. Define t(1,1) = 1, and t(1,n) = least k such that {k*r} \u003c {t(1,n-1)*x} for n \u003e= 2. Inductively, for m \u003e= 2 and n \u003e= 1, let t(m,1) be the least k not already defined as a term in the array, and for n \u003e= 2, define t(m,n) = least k such that {k*r} \u003c {t(m,n-1)*x and k has not previously been defined as a term. Thus every row of (t(m,n)) is strictly decreasing. For r = (1+sqrt(5))/2, the array (t(m,n)) is D. - _Clark Kimberling_, Feb 21 2021"
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"http://dx.doi.org/10.1007/s000170050020\"\u003eBest lower and upper approximates to irrational numbers\u003c/a\u003e, Elemente der Mathematik, 52 (1997) 122-126."
			],
			"formula": [
				"For any positive irrational number x, define an array D by successive rows as follows: D(n,k) = least positive integer q not already in D such that there exists an integer p such that 0 \u003c x - p/q \u003c x - c/d for every positive rational number c/d that has 0 \u003c d \u003c q. Thus p/q is the \"best remaining lower approximate\" of x when all better lower approximates are unavailable. For each q, define N(n,k)=p and R(n,k)=p/q. Then R is the \"array of best remaining lower approximates of x,\" D is the corresponding array of denominators and N, of numerators."
			],
			"example": [
				"Northwest corner of D:",
				"  1 2 5 13",
				"  3 4 7 10",
				"  6 9 12 15",
				"  8 11 14 17",
				"Northwest corner of R:",
				"  1/1 3/2 8/5 21/13",
				"  4/3 6/4 11/7 16/10",
				"  9/6 14/9 19/12 24/15",
				"  12/8 17/11 22/14 27/17"
			],
			"mathematica": [
				"r = N[(1 + Sqrt[5])/2, 100]; Table[d = 1; t[k] = {};",
				"Do[a = FractionalPart[n*r];",
				"  If[a \u003c d \u0026\u0026 ! MemberQ[Apply[Union, Map[t[#] \u0026, Range[k - 1]]], n],",
				"   d = a; AppendTo[t[k], n]], {n, 10000}]; t[k], {k, 12}];",
				"Column[Table[t[k], {k, 1, 12}]]",
				"(* _Peter J.C.Moses_, Feb 18 2021 *)"
			],
			"xref": [
				"Cf. A000045, A000201, A143515, A143516."
			],
			"keyword": "nonn,tabl,frac",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Aug 22 2008, Aug 25 2008",
			"references": 4,
			"revision": 13,
			"time": "2021-03-11T21:08:54-05:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}