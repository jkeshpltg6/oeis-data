{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000831",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 831,
			"data": "1,2,4,16,80,512,3904,34816,354560,4063232,51733504,724566016,11070525440,183240753152,3266330312704,62382319599616,1270842139934720,27507470234550272,630424777638805504,15250953398036463616,388362339077351014400,10384044045105304174592",
			"name": "Expansion of e.g.f. (1 + tan(x))/(1 - tan(x)).",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A000831/b000831.txt\"\u003eTable of n, a(n) for n = 0..432\u003c/a\u003e (first 84 terms from R. J. Mathar)",
				"D. Dumont, \u003ca href=\"http://dx.doi.org/10.1006/aama.1995.1014\"\u003eFurther triangles of Seidel-Arnold type and continued fractions related to Euler and Springer numbers\u003c/a\u003e, Adv. Appl. Math., 16 (1995), 275-296.",
				"M. S. Tokmachev, \u003ca href=\"https://vestnik.susu.ru/mmph/article/viewFile/8337/6806\"\u003eCorrelations Between Elements and Sequences in a Numerical Prism\u003c/a\u003e, Bulletin of the South Ural State University, Ser. Mathematics. Mechanics. Physics, 2019, Vol. 11, No. 1, 24-33."
			],
			"formula": [
				"E.g.f.: tan(x+Pi/4).",
				"a(n) = Sum_{k=1..n} (if even(n+k) ( (-1)^((n+k)/2)*Sum_{j=k..n} (j!*stirling2(n,j)*2^(n-j+1)*(-1)^(j)*binomial(j-1,k-1) ), n\u003e0. - _Vladimir Kruchinin_, Aug 19 2010",
				"a(n) = 4^n*(E_{n}(1/2)+E_{n}(1))*(-1)^((n^2-n)/2) for n \u003e 0, where E_{n}(x) is an Euler polynomial. - _Peter Luschny_, Nov 24 2010",
				"a(n) = 2^n A000111(n). - _Gerard P. Michon_, Feb 24 2011",
				"From _Sergei N. Gladkovskii_, Dec 01 2011 - Jan 24 2014: (Start)",
				"Continued fractions:",
				"E.g.f.: tan(x+Pi/4) = -1 + 2/(1-x*G(0)); G(k) = 1 - (x^2)/((x^2) - (2*k + 1)*(2*k + 3)/G(k+1)).",
				"E.g.f.: (1 + tan(x))/(1 - tan(x)) = 1 + 2*x/(U(0)-2*x) where U(k) = 4*k+1 + x/(1+x/ (4*k+3 - x/(1- x/U(k+1)))).",
				"E.g.f.: 1 + 2*x/(G(0)-x) where G(k) =  2*k+1 - x^2/G(k+1).",
				"G.f.: 1 + 2*x/Q(0), where Q(k) = 1 - 2*x*(2*k+1) - 2*x^2*(2*k+1)*(2*k+2)/( 1 - 2*x*(2*k+2) - 2*x^2*(2*k+2)*(2*k+3)/Q(k+1)).",
				"E.g.f.: tan(2*x) + sec(2*x) = (x-1)/(x+1) - 2*(2*x^2+3)/( T(0)*3*x*(1+x)- 2*x^2-3)/(x+1), where T(k) = 1 - x^4*(4*k-1)*(4*k+7)/( x^4*(4*k-1)*(4*k+7) - (4*k+1)*(4*k+5)*(16*k^2 + 8*k - 2*x^2 - 3)*(16*k^2 + 40*k - 2*x^2 + 21)/T(k+1)).",
				"E.g.f.: 1 + 2*x/Q(0), where Q(k) = 4*k+1 -x/(1 - x/( 4*k+3 + x/(1 + x/Q(k+1)))).",
				"E.g.f.: tan(2*x) + sec(2*x) = 2*R(0)-1, where R(k) = 1 + x/( 4*k+1 - x/(1 - x/( 4*k+3 + x/R(k+1)))).",
				"G.f.: 1+ G(0)*2*x/(1-2*x), where G(k) = 1 - 2*x^2*(k+1)*(k+2)/(2*x^2*(k+1)*(k+2) - (1-2*x*(k+1))*(1-2*x*(k+2))/G(k+1)). (End)",
				"a(n) ~ n! * (4/Pi)^(n+1). - _Vaclav Kotesovec_, Jun 15 2015",
				"a(0) = 1; a(n) = 2 * Sum_{k=0..n-1} binomial(n-2,k) * a(k) * a(n-k-1). - _Ilya Gutkovskiy_, Jun 11 2020"
			],
			"example": [
				"(1+tan x)/(1-tan x) = 1 +2*x/1! +4*x^2/2! +16*x^3/3! +80*x^4/4! +512*x^5/5! + ..."
			],
			"maple": [
				"A000831 := (1+tan(x))/(1-tan(x)) : for n from 0 to 200 do printf(\"%d %d \",n,n!*coeftayl(A000831,x=0,n)) ; end: # _R. J. Mathar_, Nov 19 2006",
				"A000831 := n -\u003e `if`(n=0,1,(-1)^((n^2-n)/2)*4^n*(euler(n,1/2)+euler( n,1))): # _Peter Luschny_, Nov 24 2010",
				"# third Maple program:",
				"b:= proc(u, o) option remember;",
				"      `if`(u+o=0, 1, 2*add(b(o-1+j, u-j), j=1..u))",
				"    end:",
				"a:= n-\u003e b(n, 0):",
				"seq(a(n), n=0..25);  # _Alois P. Heinz_, Sep 02 2020"
			],
			"mathematica": [
				"Range[0, 18]! CoefficientList[Series[(1+Tan[x])/(1-Tan[x]), {x,0,18}], x] (* _Robert G. Wilson v_, Apr 16 2011 *)"
			],
			"program": [
				"(PARI) a(n) = if( n\u003c1, n==0, n! * polcoeff( 1 + 2 / ( 1 / tan( x + x * O(x^n)) - 1), n)) /* _Michael Somos_, Apr 16 2011 */",
				"(PARI) a(n) = local(A); if( n\u003c0, 0, A = x * O(x^n); n! * polcoeff( (cos(x + A) + sin(x + A)) / (cos(x + A) - sin(x + A)), n)) /* _Michael Somos_, Apr 16 2011 */",
				"(Maxima) a(n):=sum(if evenp(n+k) then ((-1)^((n+k)/2)*sum(j!*stirling2(n,j)*2^(n-j+1)*(-1)^(j)*binomial(j-1,k-1),j,k,n)) else 0,k,1,n); /* _Vladimir Kruchinin_, Aug 19 2010 */",
				"(Sage)",
				"@CachedFunction",
				"def sp(n,x) :",
				"    if n == 0 : return 1",
				"    return -add(2^(n-k)*sp(k,1/2)*binomial(n,k) for k in range(n)[::2])",
				"A000831 = lambda n : abs(sp(n,x))",
				"[A000831(n) for n in (0..21)]     # _Peter Luschny_, Jul 30 2012",
				"(Sage) m = 30; T = taylor((1+tan(x))/(1-tan(x)), x, 0, m); [factorial(n)*T.coefficient(x, n) for n in (0..m)] # _G. C. Greubel_, Mar 21 2019",
				"(MAGMA) m:=30; R\u003cx\u003e:=PowerSeriesRing(Rationals(), m); b:=Coefficients(R!( (1+Tan(x))/(1-Tan(x)) )); [Factorial(n-1)*b[n]: n in [1..m]]; // _G. C. Greubel_, Mar 21 2019"
			],
			"xref": [
				"Bisections: A002436 and A012393.",
				"Cf. A000111, A000182, A155100, A258880, A258901, A258994."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 19,
			"revision": 92,
			"time": "2021-05-06T08:58:01-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}