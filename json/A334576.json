{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A334576",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 334576,
			"data": "0,1,2,2,2,3,3,3,4,5,6,6,5,4,4,4,4,5,6,6,6,7,7,7,7,6,5,5,6,7,7,7,8,9,10,10,10,11,11,11,12,13,14,14,13,12,12,12,11,10,9,9,9,8,8,8,8,9,10,10,9,8,8,8,8,9,10,10,10,11,11,11,12,13,14,14,13",
			"name": "a(n) is the X-coordinate of the n-th point of the space filling curve P defined in Comments section; sequence A334577 gives Y-coordinates.",
			"comment": [
				"The space filling curve P corresponds to the midpoint curve of the alternate paperfolding curve and can be built as follows:",
				"- we define the family {P_k, k \u003e 0}:",
				"    - P_1 corresponds to the points (0, 0), (1, 0), (2, 0) and (2, 1), in that order:",
				"                    +",
				"                    |",
				"                    |",
				"          +----+----+",
				"         O",
				"    - for any k \u003e 0, P_{n+1} is built from four copies of P_n as follows:",
				"                                            +",
				"                                            |A",
				"                +                           |",
				"               C|                   +----+  |",
				"         A     B|    ---\u003e           |C  B|  |B  C",
				"        +-------+                   +    |  +----+-+",
				"       O                           C|    |        C|",
				"                             A     B|   A|  A     B|",
				"                            +-------+    +-+-------+",
				"                           O",
				"- the space filling curve P is the limit of P_k as k tends to infinity.",
				"We can also describe the space filling curve P by mean of an L-system (see Links section)."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A334576/b334576.txt\"\u003eTable of n, a(n) for n = 0..4095\u003c/a\u003e",
				"Joerg Arndt, \u003ca href=\"/A334576/a334576.pdf\"\u003eL-system corresponding to P\u003c/a\u003e",
				"Robert Ferréol (MathCurve), \u003ca href=\"https://mathcurve.com/fractals/polya/polya.shtml\"\u003eCourbe de Polya\u003c/a\u003e [in French]",
				"Kevin Ryde, \u003ca href=\"https://user42.tuxfamily.org/alternate/index.html\"\u003eIterations of the Alternate Paperfolding Curve\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A334576/a334576.png\"\u003eColored line plot of the first 2^14 points of the space filling curve P\u003c/a\u003e (where the hue is function of the number of steps from the origin)",
				"Rémy Sigrist, \u003ca href=\"/A334576/a334576_1.png\"\u003eColored scatterplot of the first 2^20 points of the space filling curve P\u003c/a\u003e (where the hue is function of the number of steps from the origin)",
				"Rémy Sigrist, \u003ca href=\"/A334576/a334576.gp.txt\"\u003ePARI program for A334576\u003c/a\u003e",
				"\u003ca href=\"/index/Con#coordinates_2D_curves\"\u003eIndex entries for sequences related to coordinates of 2D curves\u003c/a\u003e"
			],
			"formula": [
				"a(n+1) = (A020986(n) + A020986(n+1) - 1)/2 for any n \u003e= 0."
			],
			"example": [
				"The first points of the space filling curve P are as follows:",
				"      6|                                  20...21",
				"       |                                  |    |",
				"      5|                                  19   22",
				"       |                                  |    |",
				"      4|                        16...17...18   23",
				"       |                        |              |",
				"      3|                        15   26...25...24",
				"       |                        |    |",
				"      2|              4....5    14   27...28...29",
				"       |              |    |    |              |",
				"      1|              3    6    13...12...11   30",
				"       |              |    |              |    |",
				"      0|    0....1....2    7....8....9....10   31..",
				"       |",
				"    ---+----------------------------------------",
				"    y/x|    0    1    2    3    4    5    6    7",
				"- hence a(9) = a(12) = a(17) = a(26) = a(27) = 5."
			],
			"program": [
				"(PARI) See Links section."
			],
			"xref": [
				"Cf. A020986, A334577."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Rémy Sigrist_, May 06 2020",
			"references": 2,
			"revision": 23,
			"time": "2020-05-09T02:31:55-04:00",
			"created": "2020-05-08T15:16:58-04:00"
		}
	]
}