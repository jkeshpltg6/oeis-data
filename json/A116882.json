{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A116882",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 116882,
			"data": "1,2,4,8,12,16,24,32,40,48,56,64,80,96,112,128,144,160,176,192,208,224,240,256,288,320,352,384,416,448,480,512,544,576,608,640,672,704,736,768,800,832,864,896,928,960,992,1024,1088,1152,1216,1280,1344,1408",
			"name": "A number n is included if (highest odd divisor of n)^2 \u003c= n.",
			"comment": [
				"Also n is included if (and only if ) the greatest power of 2 dividing n is \u003e= the highest odd divisor of n. All terms of the sequence are even besides the 1.",
				"Equivalently, positive integers of the form k*2^m, where odd k \u003c= 2^m. - _Thomas Ordowski_, Oct 19 2014",
				"If we define a divisor d|n to be superior if d \u003e= n/d, then superior divisors are counted by A038548 and listed by A161908. This sequence consists of 1 and all numbers without a superior odd divisor. - _Gus Wiseman_, Feb 18 2021"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A116882/b116882.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"M. Janjic and B. Petkovic, \u003ca href=\"http://arxiv.org/abs/1301.4550\"\u003eA Counting Function\u003c/a\u003e, arXiv 1301.4550 [math.CO], 2013.",
				"Amelia Carolina Sparavigna, \u003ca href=\"https://doi.org/10.5281/zenodo.3339313\"\u003eDiscussion of the groupoid of Proth numbers (OEIS A080075)\u003c/a\u003e, Politecnico di Torino, Italy (2019)."
			],
			"formula": [
				"a(n) = A080075(n-1)-1. - _Klaus Brockhaus_, _Georgi Guninski_ and _M. F. Hasler_, Aug 16 2010",
				"a(n) ~ n^2/2. - _Thomas Ordowski_, Oct 19 2014"
			],
			"example": [
				"40 = 8 * 5, where 8 is highest power of 2 dividing 40 and 5 is the highest odd dividing 40. 8 is \u003e= 5 (and, uncoincidently, 5^2 \u003c= 40), so 40 is in the sequence."
			],
			"mathematica": [
				"f[n_] := Select[Divisors[n], OddQ[ # ] \u0026][[ -1]]; Insert[Select[Range[2, 1500], 2^FactorInteger[ # ][[1]][[2]] \u003e f[ # ] \u0026], 1, 1] (* _Stefan Steinerberger_, Apr 10 2006 *)"
			],
			"program": [
				"(PARI) isok(n) = vecmax(select(x-\u003e((x % 2)==1), divisors(n)))^2 \u003c= n; \\\\ _Michel Marcus_, Sep 06 2016",
				"(PARI) isok(n) = 2^(valuation(n,2)*2) \u003e= n \\\\ _Jeppe Stig Nielsen_, Feb 19 2019"
			],
			"xref": [
				"Cf. A000265, A006519, A080075, A112714.",
				"The complement is A116883.",
				"Positions of zeros (and 1) in A341675.",
				"A051283 = numbers without a superior prime-power divisor (zeros of A341593).",
				"A059172 = numbers without a superior squarefree divisor (zeros of A341592).",
				"A063539 = numbers without a superior prime divisor (zeros of A341591).",
				"A333805 counts strictly inferior odd divisors.",
				"A341594 counts strictly superior odd divisors.",
				"- Inferior: A033676, A038548, A063962, A066839, A069288, A161906, A217581.",
				"- Superior: A033677, A063538, A070038, A072500, A161908, A341676.",
				"- Strictly Inferior: A056924, A060775, A070039, A333806, A341596, A341674.",
				"- Strictly Superior: A056924, A064052/A048098, A140271, A238535, A341642, A341643, A341673.",
				"Cf. A000005, A000203, A001248, A006530, A020639, A026804, A027193, A340101, A340854 (zeros of A340832)."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Leroy Quet_, Feb 24 2006",
			"ext": [
				"More terms from _Stefan Steinerberger_, Apr 10 2006"
			],
			"references": 34,
			"revision": 51,
			"time": "2021-04-07T12:43:43-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}