{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A330292",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 330292,
			"data": "0,1,2,3,4,5,5,6,7,9,8,11,9,13,14,10,11,17,12,19,20,21,13,23,14,25,15,27,16,29,17,18,31,32,33,34,19,36,37,38,20,41,21,41,42,43,22,45,23,47,48,49,24,51,52,53,54,55,25,59,26,58,59,27,61,65,28,63,64",
			"name": "a(n) = number of integers 1 \u003c= k \u003c n such that omega(k) \u003c= omega(n), where omega = A001221.",
			"comment": [
				"Any natural number n can be represented as n = (k_1)^p_1 * (k_2)^p_2 * ... * (k_h)^p_h, where k_i is prime for any i from 1 to h. Let us consider the function omega(n) = h, which represents the number of distinct prime factors of n. Then a(k) is the number of positive integers j less than k for which the value of function omega(j) is \u003c= omega(k).",
				"a(P) = A025528(P) for P a prime power in A246655.",
				"a(Q) = Q - 1 for Q a primorial number in A002110.",
				"Let us consider n \u003e k such that omega(n) = omega(k) = omega and there is no w such that n \u003e w \u003e k and omega(w) \u003e omega. Hence a(n) - a(k) = n - k."
			],
			"link": [
				"Felix Fröhlich, \u003ca href=\"/A330292/b330292.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"a(1) = 0: 1 has no predecessor, omega(1) = 0 by convention;",
				"a(2) = 1 because omega(2) = 1, 1 \u003e= omega(0);",
				"a(3) = 2 because omega(3) = 1 and none of omega(1), omega(2) \u003e= 1;",
				"a(4) = 3 because omega(4) = 1 and none of omega(1), omega(2), omega(3) \u003e= 1."
			],
			"mathematica": [
				"a[n_] := Block[{t = PrimeNu[n]}, Length@ Select[Range[n - 1], PrimeNu[#] \u003c= t \u0026]]; Array[a, 70] (* _Giovanni Resta_, Dec 10 2019 *)"
			],
			"program": [
				"(Python)",
				"def primes(n):",
				"    divisors = [ d for d in range(2,n//2+1) if n % d == 0 ]",
				"    return [ d for d in divisors if \\",
				"             all( d % od != 0 for od in divisors if od != d ) ]",
				"pprimes = {}",
				"for i in range(1, 10000):",
				"    res = len(primes(i))",
				"    if res == 0:",
				"        res = 1",
				"    pprimes[i] = res",
				"for k in range(1, 10000):",
				"    s = 0",
				"    for i in range(1, k):",
				"        if pprimes[i] \u003c= pprimes[k]:",
				"            s+=1",
				"    print(s)",
				"(PARI) for(n=1,70,my(omn=omega(n),m=0);for(k=1,n-1,if(omega(k)\u003c=omn,m++));print1(m,\", \")) \\\\ _Hugo Pfoertner_, Dec 10 2019",
				"(PARI) a(n) = my(omn=omega(n)); sum(k=1, n-1, omega(k) \u003c= omn); \\\\ _Michel Marcus_, Dec 11 2019"
			],
			"xref": [
				"Cf. A001221 (omega), A025528, A246655, A002110."
			],
			"keyword": "nonn,easy",
			"offset": "1,3",
			"author": "_Dilshod Urazov_, Dec 10 2019",
			"ext": [
				"More terms from _Giovanni Resta_, Dec 10 2019"
			],
			"references": 1,
			"revision": 56,
			"time": "2019-12-12T11:40:36-05:00",
			"created": "2019-12-10T16:07:00-05:00"
		}
	]
}