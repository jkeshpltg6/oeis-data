{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A060594",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 60594,
			"data": "1,1,2,2,2,2,2,4,2,2,2,4,2,2,4,4,2,2,2,4,4,2,2,8,2,2,2,4,2,4,2,4,4,2,4,4,2,2,4,8,2,4,2,4,4,2,2,8,2,2,4,4,2,2,4,8,4,2,2,8,2,2,4,4,4,4,2,4,4,4,2,8,2,2,4,4,4,4,2,8,2,2,2,8,4,2,4,8,2,4,4,4,4,2,4,8,2,2,4,4,2,4,2",
			"name": "Number of solutions to x^2 == 1 (mod n), that is, square roots of unity modulo n.",
			"comment": [
				"Sum_{k=1..n} a(k) appears to be asymptotic to C*n*log(n) with C = 0.6... - _Benoit Cloitre_, Aug 19 2002",
				"a(q) is the number of real characters modulo q. - _Benoit Cloitre_, Feb 02 2003",
				"Also number of real Dirichlet characters modulo n and Sum_{k=1..n}a(k) is asymptotic to (6/Pi^2)*n*log(n). - _Steven Finch_, Feb 16 2006",
				"Let P(n) be the product of the numbers less than and coprime to n. By theorem 59 in Nagell (which is Gauss's generalization of Wilson's theorem): for n \u003e 2, P == (-1)^(a(n)/2) (mod n). - _T. D. Noe_, May 22 2009",
				"Shadow transform of A005563. - _Michel Marcus_, Jun 06 2013",
				"For n \u003e 2, a(n) = 2 iff n is in A033948. - _Max Alekseyev_, Jan 07 2015",
				"For n \u003e 1, number of square numbers on the main diagonal of an (n-1) X (n-1) square array whose elements are the numbers from 1..n^2, listed in increasing order by rows. - _Wesley Ivan Hurt_, May 19 2021"
			],
			"reference": [
				"Trygve Nagell, Introduction to Number Theory, AMS Chelsea, 1981, p. 100. [From _T. D. Noe_, May 22 2009]",
				"G. Tenenbaum, Introduction à la théorie analytique et probabiliste des nombres, Cours spécialisé, 1995, Collection SMF, p. 260.",
				"J. V. Uspensky and M. A. Heaslet, Elementary Number Theory, McGraw-Hill, NY, 1939, pp. 196-197."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A060594/b060594.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"S. R. Finch and Pascal Sebah, \u003ca href=\"https://arxiv.org/abs/math/0604465\"\u003eSquares and Cubes Modulo n\u003c/a\u003e, arXiv:math/0604465 [math.NT], 2006-2016.",
				"K. Matthews, \u003ca href=\"http://www.numbertheory.org/php/squareroot.html\"\u003eSolving the congruence x^2=a(mod m)\u003c/a\u003e",
				"Emilia Mezzetti, RM Miró-Roig, \u003ca href=\"http://arxiv.org/abs/1611.05620\"\u003eTogliatti systems and Galois coverings\u003c/a\u003e, arXiv preprint arXiv:1611.05620 [math.AG], 2016-2018. See Lemma 6.1.",
				"John S. Rutherford, \u003ca href=\"http://dx.doi.org/10.1107/S010876730804333X\"\u003eSublattice enumeration. IV. Equivalence classes of plane sublattices by parent Patterson symmetry and colour lattice group type\u003c/a\u003e, Acta Cryst. (2009). A65, 156-163. [See Table 4].",
				"N. J. A. Sloane, \u003ca href=\"/transforms.txt\"\u003eTransforms\u003c/a\u003e",
				"L. Toth, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Toth/toth12.html\"\u003eCounting Solutions of Quadratic Congruences in Several Variables Revisited\u003c/a\u003e, J. Int. Seq. 17 (2014) # 14.11.6."
			],
			"formula": [
				"If n == 0 (mod 8), a(n) = 2^(A005087(n) + 2); if n == 4 (mod 8), a(n) = 2^(A005087(n) + 1); otherwise a(n) = 2^(A005087(n)). - Ahmed Fares (ahmedfares(AT)my-deja.com), Apr 29 2001",
				"a(n) = 2^omega(n)/2 if n == +/-2 (mod 8), a(n) = 2^omega(n) if n== +/-1, +/-3, 4 (mod 8), a(n) = 2*2^omega(n) if n == 0 (mod 8), where omega(n) = A001221(n). - _Benoit Cloitre_, Feb 02 2003",
				"For n \u003e= 2 A046073(n) * a(n) = A000010(n) = phi(n). This gives a formula for A046073(n) using the one in A060594(n). - Sharon Sela (sharonsela(AT)hotmail.com), Mar 09 2002",
				"Multiplicative with a(2) = 1; a(2^2) = 2; a(2^e) = 4 for e \u003e 2; a(q^e) = 2 for q an odd prime. - _Eric M. Schmidt_, Jul 09 2013",
				"a(n) = 2^A046072(n) for n\u003e2, in accordance with the above formulas by Ahmed Fares. - _Geoffrey Critzer_, Jan 05 2015",
				"a(n) = Sum_{k=1..n} floor(sqrt(1+n*(k-1)))-floor(sqrt(n*(k-1))). - _Wesley Ivan Hurt_, May 19 2021"
			],
			"example": [
				"The four numbers 1^2, 3^2, 5^2 and 7^2 are congruent to 1 modulo 8, so a(8) = 4."
			],
			"maple": [
				"A060594 := proc(n)",
				"   option remember;",
				"   local a,b,c;",
				"   if type(n,even) then",
				"     a:= padic:-ordp(n,2);",
				"     b:= 2^a;",
				"     c:= n/b;",
				"     min(b/2, 4) * procname(c)",
				"   else",
				"     2^nops(numtheory:-factorset(n))",
				"   fi",
				"end proc:",
				"map(A060594, [$1 .. 100]); # _Robert Israel_, Jan 05 2015"
			],
			"mathematica": [
				"a[n_] := Sum[ Boole[ Mod[k^2 , n] == 1], {k, 1, n}]; a[1] = 1; Table[a[n], {n, 1, 103}] (* _Jean-François Alcover_, Oct 21 2011 *)",
				"a[n_] := Switch[Mod[n, 8], 2|6, 2^(PrimeNu[n]-1), 1|3|4|5|7, 2^PrimeNu[n], 0, 2^(PrimeNu[n]+1)]; Array[a, 103] (* _Jean-François Alcover_, Apr 09 2016 *)"
			],
			"program": [
				"(PARI) a(n)=sum(i=1,n,if((i^2-1)%n,0,1))",
				"(PARI) a(n)=my(o=valuation(n,2));2^(omega(n\u003e\u003eo)+max(min(o-1,2),0)) \\\\ _Charles R Greathouse IV_, Jun 06 2013",
				"(PARI) a(n)=if(n\u003c=2, 1, 2^#znstar(n)[3] ); \\\\ _Joerg Arndt_, Jan 06 2015",
				"(Sage) print([len(Integers(n).square_roots_of_one()) for n in range(1,100)]) # _Ralf Stephan_, Mar 30 2014",
				"(Python)",
				"from sympy import primefactors",
				"def a007814(n): return 1 + bin(n - 1).count('1') - bin(n).count('1')",
				"def a(n):",
				"    if n%2==0:",
				"        A=a007814(n)",
				"        b=2**A",
				"        c=n//b",
				"        return min(b//2, 4)*a(c)",
				"    else: return 2**len(primefactors(n))",
				"print([a(n) for n in range(1, 101)]) # _Indranil Ghosh_, Jul 18 2017, after the Maple program"
			],
			"xref": [
				"Cf. A000010, A005087, A046073, A073103 (x^4 == 1 (mod n))."
			],
			"keyword": "nonn,mult",
			"offset": "1,3",
			"author": "_Jud McCranie_, Apr 11 2001",
			"references": 51,
			"revision": 116,
			"time": "2021-05-19T23:52:17-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}