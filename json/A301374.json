{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A301374",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 301374,
			"data": "-1,-2,-4,-6,-10,-16,-18,-22,-28,-30,-42,-46,-52,-58,-66,-70,-78,-82,-100,-102,-106,-126,-130,-136,-138,-148,-150,-162,-166,-172,-178,-190,-196,-198,-210,-222,-226,-228,-238,-250,-256,-262,-268,-270,-282,-292,-306",
			"name": "Values of A023900 which occur only at indices which are powers of a prime.",
			"comment": [
				"Terms are equal to A023900(p) = A023900(p^2) = A023900(p^3) = ... with p prime, but is never equal to A023900(m*p) with m not a power of p. [Corrected by _M. F. Hasler_, Sep 01 2021]",
				"abs(a(n)) + 1 is prime (A301590).",
				"For n \u003e 1, if and only if n can't be factored into 2*m factors, m \u003e 0, distinct factors f \u003e 1 where f + 1 is prime then -n is a term. - _David A. Corneth_, Mar 25 2018",
				"The values are of the form a(n) = 1 - p with prime p = A301590(n). These are exactly the values A023900(x) = 1 - p occurring only if x = p^j for some j \u003e= 1. (See counterexample for p = 13 in EXAMPLE section.) - _M. F. Hasler_, Sep 01 2021"
			],
			"example": [
				"a(1) = -1 = A023900(2^m), m \u003e 0.",
				"a(2) = -2 = A023900(3^m), m \u003e 0.",
				"a(3) = -4 = A023900(5^m), m \u003e 0.",
				"a(4) = -6 = A023900(7^m), m \u003e 0.",
				"a(5) = -10 = A023900(11^m), m \u003e 0.",
				"a(6) = -16 = A023900(17^m), m \u003e 0.",
				"A023900(13) = -12 is not a term as A023900(42) = -12, and 42 is the product of three prime factors.",
				"From _David A. Corneth_, Mar 25 2018: (Start)",
				"10 can't be factored in an even number of distinct factors f \u003e 1 such that f + 1 is prime, so -10 is in the sequence.",
				"12 can be factored in an even number of distinct factors f \u003e 1; 12 = 2 * 6 and both 2 + 1 and 6 + 1 are prime, hence -12 is not a term. (End)"
			],
			"mathematica": [
				"Keys@ Select[Union /@ PrimeNu@ PositionIndex@ Array[DivisorSum[#, # MoebiusMu[#] \u0026] \u0026, 310], # == {1} \u0026] (* _Michael De Vlieger_, Mar 26 2018 *)"
			],
			"program": [
				"(PARI) f(n) = sumdivmult(n, d, d*moebius(d));",
				"isok(p, vp) = {for (k=p+1, p^2-1, if (f(k) == vp, return (0));); return (1);}",
				"lista(nn) = {forprime(p=2, nn, vp = f(p); if (isok(p, vp), print1(vp, \", \")););} \\\\ _Michel Marcus_, Mar 23 2018"
			],
			"xref": [
				"Cf. A000040, A001055, A023900, A301590, A301591."
			],
			"keyword": "sign,easy",
			"offset": "1,2",
			"author": "_Torlach Rush_, Mar 19 2018",
			"references": 3,
			"revision": 66,
			"time": "2021-10-22T23:43:37-04:00",
			"created": "2018-03-29T10:10:41-04:00"
		}
	]
}