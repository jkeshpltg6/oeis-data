{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A246259",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 246259,
			"data": "1,3,2,4,7,5,9,8,6,66,10,15,17,70,91,11,18,20,94,197,55,12,19,24,186,259,155,21,13,22,26,187,364,220,84,46,14,28,41,199,377,238,87,184,1362,16,29,45,237,413,467,189,414,1981,1654,23,32,54,262,479,495,309,445,2378,3055,1419",
			"name": "Square array: row n contains in ascending order all natural numbers k for which A246271(k)+1 = n, i.e., numbers m for which we need n-1 additional iterations of A003961, starting from A003961(m), before the result is 1 modulo 4.",
			"comment": [
				"The array is read by antidiagonals: A(1,1), A(1,2), A(2,1), A(1,3), A(2,2), A(3,1), etc.",
				"The topmost row, which has row number 1, is the sequence A246261 (numbers n such that A003961(n) is of the form 4k+1), as these are exactly the numbers that do not require any additional iterations of A003961 for the result to be of the form 4k+1 (because it already is of that form).",
				"If a number k occurs in any particular row, then 4k, 9k, 16k, etc. also occur on the same row, because multiplying a number by a perfect square does not affect the result when it is reduced modulo 4.",
				"The array has an infinite number of rows, provided that A246271 is not bounded, because if A246271(n) = k \u003e 0 for some n, then A246271(A003961(n)) = k-1, thus all the preceding rows would also have terms. However, if A246271 really had an absolute maximum m, then we can consider the array to have just m rows. In either case, all the natural numbers occur in it, each just once, thus the sequence forms a permutation of natural numbers.",
				"Applying A003961 to the terms of any row below the first row gives a subsequence of the immediately preceding row."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A246259/b246259.txt\"\u003eTable of n, a(n) for n = 1..120; the first 15 antidiagonals of the array\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"example": [
				"The top left corner of the array:",
				"   1,   3,   4,   9,  10,  11,  12,  13,  14,  16,  23,  25, ...",
				"   2,   7,   8,  15,  18,  19,  22,  28,  29,  32,  43,  50, ...",
				"   5,   6,  17,  20,  24,  26,  41,  45,  54,  57,  61,  68, ...",
				"  66,  70,  94, 186, 187, 199, 237, 262, 264, 278, 280, 286, ...",
				"  91  197, 259, 364, 377, 413, 479, 627, 665, 669, 705, 763, ...",
				"  55, 155, 220, 238, 467, 495, 497, 526, 535, 543, 620, 880, ...",
				"  21,  84,  87, 189, 309, 336, 348, 358, 463, 525, 679, 756, ...",
				"  ...",
				"2 is the least number k such that A003961(k) = 3 modulo 4, but A003961(A003961(k)) = 1 modulo 4 (as indeed A003961(2) = 3, and A003961(3) = 5). Thus A(2,1) = 2.",
				"7 is the second smallest number k satisfying the same property, as A003961(7) = 11 (= 3 mod 4) while A003961(11) = 13 (= 1 mod 4). Thus A(2,2) = 7.",
				"8 is the third smallest number k satisfying the same property, as A003961(8)=27 ( = 3 mod 4) while A003961(27) = 125 = 1 mod 4. Thus A(2,3) = 8.",
				"5 is the least number k such that both A003961(k) and A003961(A003961(k)) = 3 mod 4 but A003961(A003961(A003961(k))) = 1 mod 4. Indeed A003961(5) = 7, A003961(7) = 11 and only at A003961(11) = 13 = 1 mod 4. Thus A(3,1) = 5."
			],
			"program": [
				"(Scheme, with _Antti Karttunen_'s IntSeq-library)",
				"(define (A246259 n) (A246259bi (A002260 n) (A004736 n)))",
				"(define (A246259bi row col) ((rowfun-for-A246259 row) col))",
				"(definec (rowfun-for-A246259 row) (MATCHING-POS 1 1 (lambda (k) (= (- row 1) (A246271 k)))))"
			],
			"xref": [
				"Transpose: A246258.",
				"First row: A246261.",
				"First column: A246280.",
				"Cf. A246278, A246271, A003961, A002260, A004736."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Antti Karttunen_, Aug 22 2014",
			"references": 8,
			"revision": 28,
			"time": "2017-07-17T02:17:48-04:00",
			"created": "2014-08-26T01:24:30-04:00"
		}
	]
}