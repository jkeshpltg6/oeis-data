{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A262721",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 262721,
			"data": "1,11,14,1215,1811,111211,1419,2215,1120,1116,1811,111211,1419,2215,1120,1116,1811,111211,1419,2215,1120,1116,1811,111211,1419,2215,1120,1116,1811,111211,1419,2215,1120,1116,1811,111211,1419,2215,1120,1116",
			"name": "Modified Look and Say sequence: compute sum of digits of previous term, square it, and apply the \"Say What You See\" process.",
			"comment": [
				"1. Generated with the help of sequence generated as follows:",
				"c(0)=b, c(n)=k-th power of sum of digits of c(n-1).",
				"Example: c(0)=1, c(n)=1 for all k hence convergent.",
				"Example: c(0)=2, k=2, c(1)=4, c(2)=16, c(3)=49 as (1+6)^2=49.",
				"Example: c(0)=3, k=2, c(n)=81 for all n, hence convergent.",
				"In fact, for c(0)=3 and any k, a sequence generated by using this method converges.",
				"(Methods G, V1 and V2 are explain in Link attached, namely \"Generalization of A262721\")",
				"2. Every sequence generated by c(0)=b and any k, by using G, V1 or V2 has at least two convergent subsequences or in other words sequence generated by method G, V1 or V2 never converges for any b and k.",
				"(2.1) For a(0)=1, k=2, and method G has 6 convergent subsequences with initial terms 1, 11, 14 and 1215 and converging to 1811, 111211, 1419, 2215, 1120, or 1116.",
				"(2.2) For c(0)=1, k=2, and method V1 has 2 convergent subsequences with initial terms 1, 11, 14, 1215, 1118, 2112 and converging to 1316 and 2112.",
				"(2.3) For c(0)=1, k=2, and method V2 has 2 convergent subsequences with initial terms 1, 11, 14, 15125, 1811, 1221 and converging to 1613 and 1221.",
				"3. For any b and k, the least 'g-th' term of the sequence generated by methods G, V1 or V2 reaches a point at which one of the convergent subsequence of generated sequence,converges. That is, c(g) will be the converging point of one of the subsequence of generated sequence, but there is no subsequence which converges to the term c(m),m=0,...,g-1,with initial term read as c(0).",
				"(3.1) For a(0)=1, k=2, method G we have g=4 with converging point 1811 by refering (2.1).",
				"(3.2) For c(0)=1, k=2, method V1 we have g=5, with converging point 2112.",
				"(3.3) For c(0)=1, k=2, method V2 we have g=5, with converging point 1221.",
				"(3.4) For any b and k, the value of g for methods V1 and V2 is the same.",
				"4. For method G, V1 or V2 with c(0)=b and k chosen randomly, the following holds:",
				"(I) g\u003c=k*23, for b=1 and for k\u003c=100",
				"(II) g\u003c=k*23*b for k\u003c=100",
				"(III) g\u003c=k*(23^(b+1)) for large values of k.",
				"5. In the manner of A083671, sequence become periodic from 5th row with period of 6."
			],
			"link": [
				"Abdul Gaffar Khan, \u003ca href=\"/A262721/a262721.txt\"\u003eGeneralization of A262721\u003c/a\u003e"
			],
			"formula": [
				"1. a(0) = 1, a(n) = 'frequency' of digits in the square of the sum of digits of a(n-1) followed by 'digit'-indication.",
				"2. a(0) = 1, a(n) = A005150(A118881(a(n-1))). Here first deal with the type of operations of A118881 on a(n-1)-th term and then deal with the operation of A005150 on obtained value from A118881(a(n-1)) in last step, instead of following a(n-1) term of A118881 and A118881(a(n-1)) as a member of sequences A118881 and A005150 respectively.",
				"a(0) = 1, a(n) = A045918(A118881(a(n-1))."
			],
			"example": [
				"a(0) = 1 has 1 digit, and the sum of digits is 1, and the square of the sum of digits is 1. So a(1) = 11, that is, one times 1.",
				"a(1) = 11 has 2 digits, and the sum of digits is 1+1=2 and the square of the sum of digits is 4. So a(2) = 14, that is, one times 4.",
				"Since a(2)=14, we compute 1+4=5, 5^2 = 25, where we see one 2 and one 5, so a(3)=1215."
			],
			"mathematica": [
				"A262721[0] := 1;",
				"A262721[n_] :=",
				"A262721[n] =",
				"  FromDigits[",
				"   Flatten[{Length[#], First[#]} \u0026 /@",
				"     Split[IntegerDigits[",
				"       Total[IntegerDigits[A262721[n - 1]]]^2]]]]; Table[",
				"A262721[n], {n, 0, 100}]"
			],
			"program": [
				"(PARI) say(n) = {d = digits(n); da = d[1]; na = 1; s = \"\"; for (k=2, #d, if (d[k] == da, na++, s = concat(s, Str(na, da)); na = 1; da = d[k]);); s = concat(s, Str(na, da)); eval(s);}",
				"lista(nn) = {print1(a=1, \", \"); for (k=2, nn, a = say(sumdigits(a)^2); print1(a, \", \"););} \\\\ _Michel Marcus_, Sep 29 2015"
			],
			"xref": [
				"Cf. A005150 (Look and Say).",
				"Cf. A118881 (square of sum of digits of n).",
				"Cf. A005151 (Summarize the previous term! (in increasing order)).",
				"Cf. A007890 (Summarize the previous term! (in decreasing order)).",
				"Cf. A045918 (Describe n. Also called the \"Say What You See\" or \"Look and Say\" sequence LS(n).)"
			],
			"keyword": "nonn,base",
			"offset": "0,2",
			"author": "_Abdul Gaffar Khan_, Sep 28 2015",
			"references": 1,
			"revision": 102,
			"time": "2016-11-13T16:00:22-05:00",
			"created": "2015-11-28T13:08:32-05:00"
		}
	]
}