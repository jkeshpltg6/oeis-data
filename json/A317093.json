{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A317093",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 317093,
			"data": "0,3,4,6,7,8,9,0,2,2,6,8,0,3,3,3,9,0,4,4,0,5,0,6,6,6,6,0,7,7,0,8,8,8,0,9,9,9,9,9,0,0,0,3,0,0,6,7,8,9,0,3,4,6,7,8,9,0,2,2,2,27,29,30,3,33,34,36,37,8,39,0,4,4,43,4,46,4,8,49,0,5,5,5,54,5,57,58,59,60,6,63,6,6,66,67,69,70,7,7,73,74,76,77,78,79,80,8,8,8,8,8,87,88,8,90,9,9,93,94,9,96,97,8,99,0,20",
			"name": "Terms resulting from application of a divisor sieve to the digits of the decimal expansions of the positive integers.",
			"comment": [
				"Define the \"sieve\" of a number k to be the list of divisors of k: {1, d_2, d_3, ..., k}.",
				"Definition of sieving over the digits of k: Erase each digit 1 in the decimal expansion of k, then consolidate the remaining digits. Erase each digit d_2 in what remains from the previous step, then consolidate the remaining digits. Repeat the procedure with d_3, ..., largest divisor of k \u003c= last consolidated remainder. What remains then becomes a term of the sequence. If there are no remaining digits after the procedure, this number disappears and is not a term.",
				"Consolidation means the removal of all empty places at each step of the sieving process. Example: k = 1225; the divisors of k : {1,5,7,25,35,49,175,245,1225}; erasing all 1's in 1225 results in _225, which consolidates to 225; erasing all 5's in 225 results in 22_, which consolidates to 22; erasing all 7's in 22 results in 22. As there is no other divisor of 1225 \u003c= 22 (and \u003e 7) to sieve with, the result for k = 1225 after three sieving steps is 22.",
				"The ratio c of disappearing numbers (\"contraction\"): for k in [1..99], c = 2/3; for k in [100..199], c = 3/20. What is c for k in [1,oo] ?",
				"The number of sieving steps s(k) of the number k: 1 \u003c= s(k) \u003c= A000005(k)."
			],
			"example": [
				"k = 68; divisors of 68: {1,2,4,17,34,68}.",
				"d_1 = 1, no occurrence of 1 in 68",
				"d_2 = 2, no occurrence of 2 in 68",
				"d_3 = 4, no occurrence of 4 in 68",
				"d_4 = 17, no occurrence of 17 in 68",
				"d_5 = 34, no occurrence of 34 in 68",
				"d_6 = 68, 1 occurrence of 68 in 68, no remainder.",
				"The number 68 disappears after 6 sieving steps and is not a member of the sequence.",
				"k = 84; divisors of 84: {1,2,3,4,6,7,12,14,21,28,42,84}.",
				"d_1 = 1, no occurrence of 1 in 84",
				"d_2 = 2, no occurrence of 2 in 84",
				"d_3 = 3, no occurrence of 3 in 84",
				"d_4 = 4, 1 occurrence of 4 in 84, erase 4, remains 8",
				"d_5 = 6, no occurrence of 6 in 8",
				"d_6 = 7, no occurrence of 7 in 8",
				"As there is no other divisor of 84 \u003c= 8 (and \u003e 7) to sieve with, the result for k = 84 after six sieving steps is 8. Number 8 is thus a member of the sequence.",
				"k = 106; divisors of 106: {1,2,53,106}.",
				"d_1 = 1, 1 occurrence of 1 in 106, erase 1, remains 06 which equals to 6",
				"d_2 = 2, no occurrence of 2 in 6",
				"As there is no other divisor of 106 \u003c= 6 (and \u003e 2) to sieve with, the result for k = 106 after two sieving steps is 6. Number 6 is thus a member of the sequence."
			],
			"xref": [
				"Cf. A000005, A000027, A027750, A318708."
			],
			"keyword": "nonn,base",
			"offset": "1,2",
			"author": "_Ctibor O. Zizka_, Sep 10 2018",
			"references": 0,
			"revision": 58,
			"time": "2018-09-15T15:42:37-04:00",
			"created": "2018-09-13T07:55:35-04:00"
		}
	]
}