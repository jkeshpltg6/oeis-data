{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A039683",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 39683,
			"data": "1,-2,1,8,-6,1,-48,44,-12,1,384,-400,140,-20,1,-3840,4384,-1800,340,-30,1,46080,-56448,25984,-5880,700,-42,1,-645120,836352,-420224,108304,-15680,1288,-56,1,10321920,-14026752,7559936,-2153088,359184,-36288,2184,-72,1",
			"name": "Signed double Pochhammer triangle: expansion of x(x-2)(x-4)..(x-2n+2).",
			"comment": [
				"T(n,m) = R_n^m(a=0,b=2) in the notation of the given reference.",
				"Exponential Riordan array [1/(1+2x),log(1+2x)/2]. The unsigned triangle is [1/(1-2x),log(1/sqrt(1-2x))]. - Paul Barry_, Apr 29 2009",
				"The n-th row is related to the expansion of z^(-2n)*(z^3 d/dz)^n in polynomials of the Euler operator D=(z d/dz). E.g., z^(-6)(z^3 d/dz)^3 = D^3 + 6 D^2 + 8 D. See Copeland link for relations to Bell / Exponential / Touchard polynomial operators. - _Tom Copeland_, Nov 14 2013",
				"A refinement of this array is given by A231846. - _Tom Copeland_, Nov 15 2013",
				"Also the Bell transform of the double factorial of even numbers A000165 except that the values are unsigned and in addition a first column (1,0,0 ...) is added on the left side of the triangle. For the Bell transform of the double factorial of odd numbers A001147 see A132062. For the definition of the Bell transform see A264428. - _Peter Luschny_, Dec 20 2015",
				"The signed triangle is also the inverse Bell transform of A000079 (see Luschny link). - _John Keith_, Nov 24 2020"
			],
			"link": [
				"Richell O. Celeste, Roberto B. Corcino, Ken Joffaniel M. Gonzales. \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL20/Celeste/celeste3.html\"\u003e Two Approaches to Normal Order Coefficients\u003c/a\u003e. Journal of Integer Sequences, Vol. 20 (2017), Article 17.3.5.",
				"Tom Copeland, \u003ca href=\"https://tcjpn.files.wordpress.com/2011/06/addendum-to-mathemagical-forests-v2.pdf\"\u003eAddendum to Mathemagical Forests\u003c/a\u003e.",
				"P. Feijão, F. V. Martinez, A. Thévenin, \u003ca href=\"http://www.biomedcentral.com/1471-2105/16/S19/S1/\"\u003eOn the distribution of cycles and paths in multichromosomal breakpoint graphs and the expected value of rearrangement distance\u003c/a\u003e, BMC Bioinformatics 16:Suppl19 (2015), S1. doi:\u003ca href=\"http://dx.doi.org/10.1186/1471-2105-16-S19-S1\"\u003e10.1186/1471-2105-16-S19-S1\u003c/a\u003e",
				"Lisa Glaser, \u003ca href=\"http://dx.doi.org/10.1088/1742-6596/306/1/012041\"\u003eCausal set actions in various dimensions\u003c/a\u003e, J. Phys.: Conf. Ser. 306 (2011), 012041.",
				"Wolfdieter Lang, \u003ca href=\"/A039683/a039683.txt\"\u003eFirst 9 rows and comment\u003c/a\u003e.",
				"Peter Luschny, \u003ca href=\"https://oeis.org/wiki/User:Peter_Luschny/BellTransform\"\u003eThe Bell transform\u003c/a\u003e",
				"D. S. Mitrinovic, M. S. Mitrinovic, \u003ca href=\"http://pefmath2.etf.rs/files/47/77.pdf\"\u003eTableaux d'une classe de nombres relies aux nombres de Stirling\u003c/a\u003e, Univ. Beograd. Pubi. Elektrotehn. Fak. Ser. Mat. Fiz. 77 (1962)."
			],
			"formula": [
				"T(n, m) = T(n-1, m-1) - 2*(n-1)*T(n-1, m), n \u003e= m \u003e= 1; T(n, m) := 0, n\u003cm; T(n, 0) := 0, T(1, 1)=1.",
				"E.g.f. for m-th column of signed triangle: (((log(1+2*x))/2)^m)/m!.",
				"E.g.f.: (1+2*x)^(y/2). O.g.f. for n-th row of signed triangle: Sum_{m=0..n} Stirling1(n, m)*2^(n-m)*x^m. - _Vladeta Jovovic_, Feb 11 2003",
				"T(n, m) = S1(n, m)*2^(n-m), with S1(n, m) := A008275(n, m) (signed Stirling1 triangle).",
				"The production matrix below is A038207 with the first row removed. With the initial index n = 0, the associated differential raising operator is R = e^(2D)*x = (2+x)*e^(2D) with D = d/dx, i.e., R p_n(x) = p_(n+1)(x) where p_n(x) is the n-th unsigned row polynomial and p_0(x) = 1, so p_(n+1)(x) = (2+x) * p_n(2+x). - _Tom Copeland_, Oct 11 2016"
			],
			"example": [
				"Triangle starts:",
				"  {1},",
				"  {2,1},",
				"  {8,6,1},",
				"  {48,44,12,1},",
				"  ...",
				"From _Paul Barry_, Apr 29 2009: (Start)",
				"The unsigned triangle [1/(1-2x),log(1/sqrt(1-2x))] has production matrix:",
				"  2, 1,",
				"  4, 4, 1,",
				"  8, 12, 6, 1,",
				"  16, 32, 24, 8, 1,",
				"  32, 80, 80, 40, 10, 1,",
				"  64, 192, 240, 160, 60, 12, 1",
				"which is A007318^{2} beheaded. (End)"
			],
			"mathematica": [
				"Table[ Rest@ CoefficientList[ Product[ z-k, {k, 0, 2p-2, 2} ], z ], {p, 6} ]"
			],
			"program": [
				"(Sage) # uses[bell_transform from A264428]",
				"# Unsigned values and an additional first column (1,0,0,...).",
				"def A039683_unsigned_row(n):",
				"    a = sloane.A000165",
				"    dblfact = a.list(n)",
				"    return bell_transform(n, dblfact)",
				"[A039683_unsigned_row(n) for n in (0..9)] # _Peter Luschny_, Dec 20 2015"
			],
			"xref": [
				"First column (unsigned triangle) is (2(n-1))!! = 1, 2, 8, 48, 384...= A000165(n-1) and the row sums (unsigned) are (2n-1)!! = 1, 3, 15, 105, 945... = A001147(n-1).",
				"Cf. A051141, A051142.",
				"Cf. A000165, A132062, A264428.",
				"Cf. A038207."
			],
			"keyword": "sign,tabl",
			"offset": "1,2",
			"author": "_Wouter Meeussen_",
			"ext": [
				"Additional comments from _Wolfdieter Lang_",
				"Title revised by _Tom Copeland_, Dec 21 2013"
			],
			"references": 21,
			"revision": 74,
			"time": "2020-12-21T17:29:10-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}