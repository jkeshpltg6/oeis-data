{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A049670",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 49670,
			"data": "0,1,123,15128,1860621,228841255,28145613744,3461681649257,425758697244867,52364858079469384,6440451785077489365,792123204706451722511,97424713727108484379488,11982447665229637126954513,1473743638109518258131025611,181258485039805516112989195640",
			"name": "a(n) = Fibonacci(10*n)/55.",
			"comment": [
				"Chebyshev polynomials S(n-1,123).",
				"Used for all positive integer solutions of Pell equation x^2 - 5*(5*y)^2 = -4. See A097842 with A097843.",
				"This is the k = 10 member of the k-family of sequences {F(k*n)/F(k)}, n \u003e= 0 for k \u003e= 1, with o.g.f. x/(1 - L(k)*x + (-1)^k*x^2). Proof: Binet-de Moivre formula for F and L. See also A028412. - _Wolfdieter Lang_, Aug 26 2012"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A049670/b049670.txt\"\u003eTable of n, a(n) for n = 0..383\u003c/a\u003e",
				"Hacène Belbachir, Soumeya Merwa Tebtoub, and László Németh, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL23/Nemeth/nemeth7.html\"\u003eEllipse Chains and Associated Sequences\u003c/a\u003e, J. Int. Seq., Vol. 23 (2020), Article 20.8.5.",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (123,-1)."
			],
			"formula": [
				"G.f.: x/(1-123*x+x^2), 123=L(10)=A000032(10) (Lucas).",
				"a(n+1) = S(n, 123) = U(n, 123/2) = S(2*n+1, 5*sqrt(5))/(5*sqrt(5)), n\u003e=0, with S(n, x) = U(n, x/2) Chebyshev's polynomials of the second kind, A049310. S(-1, x)= 0 = U(-1, x).",
				"a(n) = 123*a(n-1) - a(n-2), n \u003e= 2; a(0)=0, a(1)=1.",
				"a(n) = (ap^n - am^n)/(ap-am) with ap := (123+55*sqrt(5))/2 and am := (123-55*sqrt(5))/2 = 1/ap.",
				"From _Peter Bala_, Nov 29 2013: (Start)",
				"a(n) = 1/(11*55)*(F(10*n + 5) - F(10*n - 5)).",
				"Sum_{n \u003e= 1} 1/( 11*a(n) + 1/(11*a(n)) ) = 1/11. Compare with A001906 and A049660. (End)",
				"From _Peter Bala_, Apr 03 2015: (Start)",
				"For integer k, 1 + k*(22 - k)*Sum_{n \u003e= 1} a(n)*x^(2*n) = ( 1 + k/5*Sum_{n \u003e= 1} Fibonacci(5*n)*x^n )*( 1 + k/5*Sum_{n \u003e= 1} Fibonacci(5*n)*(-x)^n ).",
				"1 + 4*Sum_{n \u003e= 1} a(n)*x^(2*n) = ( 1 + 2/5*Sum_{n \u003e= 1} Fibonacci(5*n+5)*x^n )*( 1 + 2/5*Sum_{n \u003e= 1} Fibonacci(5*n+5)*(-x)^n ) = ( 1 + 2/5*Sum_{n \u003e= 1} Fibonacci(5*n-5)*x^n )*( 1 + 2/5*Sum_{n \u003e= 1} Fibonacci(5*n-5)*(-x)^n ).",
				"1 + 25*Sum_{n \u003e= 1} a(n)*x^(2*n) = ( 1 + Sum_{n \u003e= 1} Fibonacci(5*n+3)*x^n )*( 1 + Sum_{n \u003e= 1} Fibonacci(5*n+3)*(-x)^n ) = ( 1 + Sum_{n \u003e= 1} Fibonacci(5*n-3)*x^n )*( 1 + Sum_{n \u003e= 1} Fibonacci(5*n-3)*(-x)^n ).",
				"1 + 100*Sum_{n \u003e= 1} a(n)*x^(2*n) = ( 1 + 2*Sum_{n \u003e= 1} Fibonacci(5*n+1)*x^n )*( 1 + 2*Sum_{n \u003e= 1} Fibonacci(5*n+1)*(-x)^n ) = ( 1 + 2*Sum_{n \u003e= 1} Fibonacci(5*n-1)*x^n )*( 1 + 2*Sum_{n \u003e= 1} Fibonacci(5*n-1)*(-x)^n ).",
				"1 + 125*Sum_{n \u003e= 1} a(n)*x^(2*n) = ( 1 + Sum_{n \u003e= 1} Lucas(5*n)*x^n )*( 1 + Sum_{n \u003e= 1} Lucas(5*n)*(-x)^n ). (End)"
			],
			"maple": [
				"seq(combinat:-fibonacci(10*n)/55, n=0..20); # _Robert Israel_, Apr 03 2015"
			],
			"mathematica": [
				"Table[Fibonacci[10 n]/55, {n, 12}] (* _Michael De Vlieger_, Apr 03 2015 *)",
				"LinearRecurrence[{123,-1},{0,1},20] (* _Harvey P. Dale_, Dec 03 2019 *)"
			],
			"program": [
				"(MuPAD) numlib::fibonacci(10*n)/55 $ n = 0..25; // _Zerinvary Lajos_, May 09 2008",
				"(PARI) a(n)=fibonacci(10*n)/55 \\\\ _Charles R Greathouse IV_, Oct 07 2016",
				"(MAGMA) [ Fibonacci(10*n)/55: n in [0..30]]; // _G. C. Greubel_, Dec 02 2017"
			],
			"xref": [
				"A column of array A028412.",
				"Cf. A000045."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Clark Kimberling_",
			"ext": [
				"More terms from _James A. Sellers_, Jan 20 2000",
				"Chebyshev and Pell comments from _Wolfdieter Lang_, Sep 10 2004"
			],
			"references": 11,
			"revision": 62,
			"time": "2020-12-29T20:38:50-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}