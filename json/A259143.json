{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A259143",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 259143,
			"data": "1,2,2,2,2,2,2,2,2,2,1,2,2,2,2,2,2,2,2,3,2,2,3,3,3,3,3,3,3,3,2,2,1,2,2,2,2,2,2,2,3,2,2,3,3,3,3,3,3,3,2,2,1,2,2,2,2,2,2,2,2,1,2,2,2,2,2,2,3,2,3,2,3,3,3,3,3,3,3,3,2,2,3,3,3,3",
			"name": "Number of distinct digits needed to write the calendar date of n-th day of a non-leap year in decimal representation, without leading zeros.",
			"comment": [
				"a(n) \u003c= 4."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A259143/b259143.txt\"\u003eTable of n, a(n) for n = 1..365\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Calendar_date\"\u003eCalendar date\u003c/a\u003e",
				"\u003ca href=\"/index/Ca#calendar\"\u003eIndex entries for sequences related to calendars\u003c/a\u003e"
			],
			"example": [
				".      | Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec",
				".      |   1   2   3   4   5   6   7   8   9  10  11  12",
				".  ----+-------------------------------------------------",
				".    1 |   1   2   2   2   2   2   2   2   2   2   1   2",
				".    2 |   2   1   2   2   2   2   2   2   2   3   2   2",
				".    3 |   2   2   1   2   2   2   2   2   2   3   2   3",
				".    4 |   2   2   2   1   2   2   2   2   2   3   2   3",
				".    5 |   2   2   2   2   1   2   2   2   2   3   2   3",
				".    6 |   2   2   2   2   2   1   2   2   2   3   2   3",
				".    7 |   2   2   2   2   2   2   1   2   2   3   2   3",
				".    8 |   2   2   2   2   2   2   2   1   2   3   2   3",
				".    9 |   2   2   2   2   2   2   2   2   1   3   2   3",
				".   10 |   2   3   3   3   3   3   3   3   3   2   2   3",
				".   11 |   1   2   2   2   2   2   2   2   2   2   1   2",
				".   12 |   2   2   3   3   3   3   3   3   3   3   2   2",
				".   13 |   2   3   2   3   3   3   3   3   3   3   2   3",
				".   14 |   2   3   3   2   3   3   3   3   3   3   2   3",
				".   15 |   2   3   3   3   2   3   3   3   3   3   2   3",
				".   16 |   2   3   3   3   3   2   3   3   3   3   2   3",
				".   17 |   2   3   3   3   3   3   2   3   3   3   2   3",
				".   18 |   2   3   3   3   3   3   3   2   3   3   2   3",
				".   19 |   2   3   3   3   3   3   3   3   2   3   2   3",
				".   20 |   3   2   3   3   3   3   3   3   3   3   3   3",
				".   21 |   2   2   3   3   3   3   3   3   3   3   2   2",
				".   22 |   2   1   2   2   2   2   2   2   2   3   2   2",
				".   23 |   3   2   2   3   3   3   3   3   3   4   3   3",
				".   24 |   3   2   3   2   3   3   3   3   3   4   3   3",
				".   25 |   3   2   3   3   2   3   3   3   3   4   3   3",
				".   26 |   3   2   3   3   3   2   3   3   3   4   3   3",
				".   27 |   3   2   3   3   3   3   2   3   3   4   3   3",
				".   28 |   3   2   3   3   3   3   3   2   3   4   3   3",
				".   29 |   3   _   3   3   3   3   3   3   2   4   3   3",
				".   30 |   3   _   2   3   3   3   3   3   3   3   3   4",
				".   31 |   2   _   2   _   3   _   3   3   _   3   _   3  .",
				"13 days need just 1 digit: {1, 11, 33, 53, 62, 94, 125, 157, 188, 220, 252, 305, 315} = {Jan 1, Jan 11, Feb 2, Feb 22, Mar 3, Apr 4, May 5, Jun 6, Jul 7, Aug 8, Sep 9, Nov 1, Nov 11};",
				"152 days need 2 digits: the first is the 2nd day = Jan 2, the last one is the 356th day = Dec 22;",
				"192 days need 3 digits: the first is the 20th day = Jan 20, the last one is the last day of the year = Dec 31;",
				"8 days need 4 digits: {296 .. 302, 364} = Oct 23 through Oct 29, and also the penultimate day of the year = Dec 30."
			],
			"program": [
				"(Haskell)",
				"import Data.List (nub)",
				"a259143 n = a259143_list !! (n-1)",
				"a259143_list = [length $ nub $ show m ++ show d |",
				"                m \u003c- [1 .. 12], d \u003c- [1 .. a008685 m]]"
			],
			"xref": [
				"Cf. A008685."
			],
			"keyword": "nonn,base,fini,full",
			"offset": "1,2",
			"author": "_Reinhard Zumkeller_, Jun 19 2015",
			"references": 1,
			"revision": 13,
			"time": "2018-05-04T08:11:38-04:00",
			"created": "2015-06-19T03:18:17-04:00"
		}
	]
}