{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002851",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2851,
			"id": "M1521 N0595",
			"data": "1,0,1,2,5,19,85,509,4060,41301,510489,7319447,117940535,2094480864,40497138011,845480228069,18941522184590,453090162062723,11523392072541432,310467244165539782,8832736318937756165",
			"name": "Number of unlabeled trivalent (or cubic) connected graphs with 2n nodes.",
			"reference": [
				"CRC Handbook of Combinatorial Designs, 1996, p. 647.",
				"F. Harary, Graph Theory. Addison-Wesley, Reading, MA, 1969, p. 195.",
				"R. C. Read, Some applications of computers in graph theory, in L. W. Beineke and R. J. Wilson, editors, Selected Topics in Graph Theory, Academic Press, NY, 1978, pp. 417-444.",
				"R. C. Read and G. F. Royle, Chromatic roots of families of graphs, pp. 1009-1029 of Y. Alavi et al., eds., Graph Theory, Combinatorics and Applications. Wiley, NY, 2 vols., 1991.",
				"R. C. Read and R. J. Wilson, An Atlas of Graphs, Oxford, 1998.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)"
			],
			"link": [
				"Peter Adams, Ryan C. Bunge, Roger B. Eggleton, Saad I. El-Zanati, Uğur Odabaşi, and Wannasiri Wannasit, \u003ca href=\"http://www.the-ica.org/Volumes/92/Reprints/BICA2020-26-Reprint.pdf\"\u003eDecompositions of complete graphs and complete bipartite graphs into bipartite cubic graphs of order at most 12\u003c/a\u003e, Bull. Inst. Combinatorics and Applications (2021) Vol. 92, 50-61.",
				"G. Brinkmann, J. Goedgebeur and B. D. McKay, \u003ca href=\"http://www.dmtcs.org/dmtcs-ojs/index.php/dmtcs/article/viewArticle/1801\"\u003eGeneration of cubic graphs\u003c/a\u003e, Discr. Math. Theor. Comp. Sci. 13 (2) (2011) 69-80",
				"G. Brinkmann, J. Goedgebeur, N. Van Cleemput, \u003ca href=\"http://caagt.ugent.be/jgoedgeb/paper-cubic_graphs_survey.pdf\"\u003eThe history of the generation of cubic graphs\u003c/a\u003e, Int. J. Chem. Modeling 5 (2-3) (2013) 67-89",
				"F. C. Bussemaker, S. Cobeljic, L. M. Cvetkovic and J. J. Seidel, \u003ca href=\"http://alexandria.tue.nl/repository/books/252909.pdf\"\u003eComputer investigations of cubic graphs\u003c/a\u003e, T.H.-Report 76-WSK-01, Technological University Eindhoven, Dept. Mathematics, 1976.",
				"F. C. Bussemaker, S. Cobeljic, D. M. Cvetkovic, J. J. Seidel, \u003ca href=\"http://dx.doi.org/10.1016/0095-8956(77)90034-X\"\u003eCubic graphs on \u003c= 14 vertices\u003c/a\u003e J. Combinatorial Theory Ser. B 23(1977), no. 2-3, 234--235. MR0485524 (58 #5354).",
				"Timothy B. P. Clark, Adrian Del Maestro, \u003ca href=\"http://arxiv.org/abs/1506.02048\"\u003eMoments of the inverse participation ratio for the Laplacian on finite regular graphs\u003c/a\u003e, arXiv:1506.02048 [math-ph], 2015.",
				"H. Gropp, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(92)90592-4\"\u003eEnumeration of regular graphs 100 years ago\u003c/a\u003e, Discrete Math., 101 (1992), 73-85.",
				"House of Graphs, \u003ca href=\"http://hog.grinvin.org/Cubic\"\u003eCubic graphs\u003c/a\u003e",
				"Jason Kimberley, \u003ca href=\"/wiki/User:Jason_Kimberley/C_k-reg_girth_ge_g_index\"\u003eIndex of sequences counting connected k-regular simple graphs with girth at least g\u003c/a\u003e",
				"M. Klin, M. Rücker, Ch. Rücker and G. Tinhofer, \u003ca href=\"http://www-lit.ma.tum.de/veroeff/html/950.05003.html\"\u003eAlgebraic Combinatorics\u003c/a\u003e [broken link]",
				"M. Klin, M. Rücker, Ch. Rücker, G. Tinhofer, \u003ca href=\"ftp://ftp.mathe2.uni-bayreuth.de/axel/papers/klin:algebraic_combinatorics_in_mathematical_chemistry_methods_and_applications_1_permutation_groups_and_coherent_algebras.ps.gz\"\u003eAlgebraic Combinatorics\u003c/a\u003e (1997)",
				"Denis S. Krotov, Konstantin V. Vorob'ev, \u003ca href=\"https://arxiv.org/abs/1812.02166\"\u003eOn unbalanced Boolean functions attaining the bound 2n/3-1 on the correlation immunity\u003c/a\u003e, arXiv:1812.02166 [math.CO], 2018.",
				"R. J. Mathar/Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Table_of_simple_cubic_graphs\"\u003eTable of simple cubic graphs\u003c/a\u003e [From _N. J. A. Sloane_, Feb 28 2012]",
				"M. Meringer, \u003ca href=\"http://www.mathe2.uni-bayreuth.de/markus/reggraphs.html\"\u003eTables of Regular Graphs\u003c/a\u003e",
				"R. W. Robinson and N. C. Wormald, \u003ca href=\"http://dx.doi.org/10.1002/jgt.3190070412\"\u003eNumbers of cubic graphs\u003c/a\u003e. J. Graph Theory 7 (1983), no. 4, 463-467.",
				"Sage, \u003ca href=\"http://www.sagemath.org/doc/reference/graphs/sage/graphs/graph_generators.html\"\u003eCommon Graphs (Graph Generators)\u003c/a\u003e",
				"J. J. Seidel, R. R. Korfhage, \u0026 N. J. A. Sloane, \u003ca href=\"/A002851/a002851.pdf\"\u003eCorrespondence 1975\u003c/a\u003e",
				"H. M. Sultan, \u003ca href=\"http://www.math.columbia.edu/~hsultan/papers/separatingpantscomplex_v3.pdf\"\u003eSeparating pants decompositions in the pants complex\u003c/a\u003e.",
				"H. M. Sultan, \u003ca href=\"http://arxiv.org/abs/1106.1472\"\u003eNet of Pants Decompositions Containing a non-trivial Separating Curve in the Pants Complex\u003c/a\u003e, arXiv:1106.1472 [math.GT], 2011.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ConnectedGraph.html\"\u003eConnected Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CubicGraph.html\"\u003eCubic Graph\u003c/a\u003e"
			],
			"example": [
				"G.f. = 1 + x^2 + 2*x^3 + 5*x^4 + 19*x^5 + 85*x^6 + 509*x^7 + 4060*x^8 + 41302*x^9 + 510489*x^10 + 7319447*x^11 + ...",
				"a(0) = 1 because the null graph (with no vertices) is vacuously 3-regular.",
				"a(1) = 0 because there are no simple connected cubic graphs with 2 nodes.",
				"a(2) = 1 because the tetrahedron is the only cubic graph with 4 nodes."
			],
			"xref": [
				"Cf. A004109 (labeled connected cubic), A321304 (rooted connected cubic), A321305 (signed connected cubic), A000421 (connected cubic multigraphs), A275744 (multisets).",
				"Contribution (almost all) from _Jason Kimberley_, Feb 10 2011: (Start)",
				"3-regular simple graphs: this sequence (connected), A165653 (disconnected), A005638 (not necessarily connected), A005964 (planar).",
				"Connected regular graphs A005177 (any degree), A068934 (triangular array), specified degree k: this sequence (k=3), A006820 (k=4), A006821 (k=5), A006822 (k=6), A014377 (k=7), A014378 (k=8), A014381 (k=9), A014382 (k=10), A014384 (k=11).",
				"Connected 3-regular simple graphs with girth at least g: A185131 (triangle); chosen g: this sequence (g=3), A014371 (g=4), A014372 (g=5), A014374 (g=6), A014375 (g=7), A014376 (g=8).",
				"Connected 3-regular simple graphs with girth exactly g: A198303 (triangle); chosen g: A006923 (g=3), A006924 (g=4), A006925 (g=5), A006926 (g=6), A006927 (g=7). (End)"
			],
			"keyword": "nonn,nice",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from R. C. Read (rcread(AT)math.uwaterloo.ca)"
			],
			"references": 52,
			"revision": 101,
			"time": "2021-09-22T18:49:27-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}