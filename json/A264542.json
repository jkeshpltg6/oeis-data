{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A264542",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 264542,
			"data": "1,2,96,17280,860160,774144000,408748032000,347163328512000,266621436297216000,163172319013896192000,67488959156767948800,14865958099336613068800,785345441564243189248819200,530893518497428395932201779200,2831432098652951444971742822400,221701133324526098141287462993920000",
			"name": "a(n) = denominator(Jtilde3(n)).",
			"comment": [
				"Jtilde3(n) are Apéry-like rational numbers that arise in the calculation of zetaQ(3), the spectral zeta function for the non-commutative harmonic oscillator using a Gaussian hypergeometric function."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A264542/b264542.txt\"\u003eTable of n, a(n) for n = 0..345\u003c/a\u003e",
				"Takashi Ichinose, Masato Wakayama, \u003ca href=\"http://doi.org/10.2206/kyushujm.59.39\"\u003eSpecial values of the spectral zeta function of the non-commutative harmonic oscillator and confluent Heun equations\u003c/a\u003e, Kyushu Journal of Mathematics, Vol. 59 (2005) No. 1 p. 39-100.",
				"Kazufumi Kimoto, Masato Wakayama, \u003ca href=\"http://doi.org/10.2206/kyushujm.60.383\"\u003eApéry-like numbers arising from special values of spectral zeta functions for non-commutative harmonic oscillators\u003c/a\u003e, Kyushu Journal of Mathematics, Vol. 60 (2006) No. 2 p. 383-404 (see Table 2)."
			],
			"formula": [
				"Jtilde3(n) = J3(n) - J3(0)*Jtilde2(n) (normalization).",
				"4n^2*J3(n) - (8n^2-8n+3)*J3(n-1) + 4(n-1)^2*J3(n-2) = 2^n*(n-1)!/(2n-1)!! with J3(0)=7*zeta(3) and J3(1)=21*zeta(3)/4 + 1/2."
			],
			"mathematica": [
				"Denominator[Table[-2*Sum[(-1)^k*Binomial[-1/2, k]^2*Binomial[n, k]* Sum[1/(Binomial[-1/2, j]^2*(2*j + 1)^3), {j, 0, k - 1}], {k, 0, n}], {n, 0, 50}]] (* _G. C. Greubel_, Oct 23 2017 *)"
			],
			"program": [
				"(PARI) a(n) = denominator(-2*sum(k=0, n, (-1)^k*binomial(-1/2, k)^2*binomial(n, k)*sum(j=0, k-1, 1/(binomial(-1/2,j)^2*(2*j+1)^3))));"
			],
			"xref": [
				"Cf. A002117 (zeta(3)), A260832 (Jtilde2), A264541 (numerators).",
				"The Apéry-like numbers [or Apéry-like sequences, Apery-like numbers, Apery-like sequences] include A000172, A000984, A002893, A002895, A005258, A005259, A005260, A006077, A036917, A063007, A081085, A093388, A125143 (apart from signs), A143003, A143007, A143413, A143414, A143415, A143583, A183204, A214262, A219692, A226535, A227216, A227454, A229111 (apart from signs), A260667, A260832, A262177, A264541, A264542, A279619, A290575, A290576. (The term \"Apery-like\" is not well-defined.)"
			],
			"keyword": "nonn,frac",
			"offset": "0,2",
			"author": "_Michel Marcus_, Nov 17 2015",
			"references": 35,
			"revision": 21,
			"time": "2017-10-24T08:38:58-04:00",
			"created": "2015-11-18T03:42:45-05:00"
		}
	]
}