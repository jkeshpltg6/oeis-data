{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A274967",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 274967,
			"data": "77,119,143,161,187,203,209,221,299,319,323,329,371,377,391,407,413,437,473,493,497,517,527,533,539,551,581,583,589,611,623,629,649,667,689,707,713,731,737,749,767,779,791,799,803,817,851,869,893,899,901,913",
			"name": "Odd composite numbers n which are not m-gonal number for 3 \u003c= m \u003c n.",
			"comment": [
				"An m-gonal number, m \u003e= 3, i.e. of form n = (k/2)*[(m-2)*k - (m-4)], yields a nontrivial factorization of n if and only if of order k \u003e= 3.",
				"Odd composite numbers n for which A176948(n) = n.",
				"All odd composite n are coprime to 30 (see next comment) and have smallest prime factor \u003e= 7, e.g.",
				"   77 =   7·11, 119 =  7·17, 143 = 11·13, 161 =  7·23,",
				"  187 =  11·17, 203 =  7·29, 209 = 11·19, 221 = 13·17,",
				"  299 =  13·23, 319 = 11·29, 323 = 17·19, 329 =  7·47,",
				"  371 =   7·53, 377 = 13·29, 391 = 17·23, 407 = 11·37,",
				"  413 =   7·59, 437 = 19·23, 473 = 11·43, 493 = 17·29,",
				"  497 =   7·71, 517 = 11·47, 527 = 17·31, 533 = 13·41,",
				"  539 = 7·7·11, 551 = 19·29, 581 =  7·83, 583 = 11·53,",
				"  589 =  19·31, 611 = 13·47, 623 =  7·89, 629 = 17·37,",
				"  649 =  11·59, 667 = 23·29, 689 = 13·53, 707 = 7·101,",
				"  713 =  23·31, 731 = 17·43, 737 = 11·67, 749 = 7·107,",
				"  767 =  13·59, 779 = 19·41, 791 = 7·113, 799 = 17·47,",
				"  803 =  11·73, 817 = 19·43, 851 = 23·37, 869 = 11·79,",
				"  893 =  19·47, 899 = 29·31, 901 = 17·53, 913 = 11·83.",
				"Composite numbers n which are divisible by 3 are m-gonal numbers of order 3, with m = (n + 3)/3. Thus all a(n) are coprime to 3.",
				"Odd composite numbers n which are divisible by 5 are m-gonal numbers of order 5, with m = (n + 15)/10. Thus all a(n) are coprime to 5.",
				"Since we are looking for solutions of (m-2)*k^2 - (m-4)*k - 2*n = 0, with m \u003e= 3 and k \u003e= 3, the largest k we need to consider is",
				"    k = {(m-4) + sqrt[(m-4)^2 + 8*(m-2)*n]}/[2*(m-2)] with m = 3, thus",
				"    k \u003c= (1/2)*{-1 + sqrt[1 + 8*n]}.",
				"Or, since we are looking for solutions of 2n = m*k*(k-1) - 2*k*(k-2), with m \u003e= 3 and k \u003e= 3, the largest m we need to consider is",
				"    m = [2n + 2*k*(k-2)]/[k*(k-1)] with k = 3, thus m \u003c= (n+3)/3."
			],
			"link": [
				"Chai Wah Wu, \u003ca href=\"/A274967/b274967.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"OEIS Wiki, \u003ca href=\"/wiki/Polygonal_numbers\"\u003ePolygonal numbers\u003c/a\u003e"
			],
			"example": [
				"77 is in this sequence because 77 is trivially a 77-gonal number of order k = 2, but not an m-gonal number for 3 \u003c= k \u003c= (1/2)*{-1 + sqrt[1 + 8*77]}."
			],
			"mathematica": [
				"Select[Range[500]2+1, ! PrimeQ[#] \u0026\u0026 FindInstance[n*(4 + n*(s-2)-s)/2 == # \u0026\u0026 s \u003e= 3 \u0026\u0026 n \u003e= 3, {s, n}, Integers] == {} \u0026] (* _Giovanni Resta_, Jul 13 2016 *)"
			],
			"program": [
				"(Sage)",
				"def is_a(n):",
				"    if is_even(n): return False",
				"    if is_prime(n): return False",
				"    for m in (3..(n+3)//3):",
				"        if pari('ispolygonal')(n, m):",
				"            return False",
				"    return True",
				"print([n for n in (3..913) if is_a(n)]) # _Peter Luschny_, Jul 28 2016",
				"(Python)",
				"from sympy import isprime",
				"A274967_list = []",
				"for n in range(3,10**6,2):",
				"    if not isprime(n):",
				"        k = 3",
				"        while k*(k+1) \u003c= 2*n:",
				"            if not (2*(k*(k-2)+n)) % (k*(k - 1)):",
				"                break",
				"            k += 1",
				"        else:",
				"            A274967_list.append(n) # _Chai Wah Wu_, Jul 28 2016"
			],
			"xref": [
				"Cf. A176774, A176948, A176949, A274968."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_Daniel Forgues_, Jul 12 2016",
			"ext": [
				"a(10)-a(52) from _Giovanni Resta_, Jul 13 2016"
			],
			"references": 3,
			"revision": 65,
			"time": "2020-03-13T17:44:17-04:00",
			"created": "2016-07-16T10:32:51-04:00"
		}
	]
}