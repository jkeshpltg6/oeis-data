{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A332878",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 332878,
			"data": "1,2,4,3,6,9,12,8,16,5,10,15,18,20,24,25,21,27,28,32,30,36,35,33,39,42,40,45,44,48,54,50,52,56,55,51,57,60,65,63,64,7,14,66,68,72,78,76,80,84,70,88,75,92,96,49,69,81,85,87,90,95,93,99,100,105,104",
			"name": "a(1)=1; a(n+1) is the smallest number not yet seen in the sequence which is divisible by 1 + Omega(a(n)).",
			"comment": [
				"Here Omega = A001222, the number of prime factors counted with multiplicity.",
				"Conjecture: This is a permutation of the natural numbers in which the primes appear in their natural order. Prime p \u003e 2 arises as a(k) if and only if a(k-1) = 2^(p-1), in which case a(k+1) = 2*p. The sequence of numbers k such that a(k) is prime starts 2, 4, 10, 42, ... How does it continue?",
				"a(636) = 11, a(2530) = 13, a(39731) = 17. It appears that the prime p occurs roughly at index 2^(p-2)*(1 + O(1/log p)). It is followed by 2p and then a multiple of 3. The graph of the sequence has several \"branches\" which can be labeled by odd primes: Most numbers occur on the main (p=3) branch which has an initial slope of about 1.61 increasing to 1.65 in the range 1e4 .. 4e4. A smaller fraction of the numbers lie on a second (p=5) and third (p=7) branch with slope of roughly 1.25 resp. 1.11 around n ~ 4e4, and a very small fraction lies on the branches with even lower slope (about 0.15 for the p=11 and 0.035 for the p=13 branch). - _M. F. Hasler_, Mar 04 2020"
			],
			"link": [
				"M. F. Hasler, \u003ca href=\"/A332878/b332878.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"M. F. Hasler, \u003ca href=\"/A332878/a332878.gif\"\u003eScatterplot of a(1..40000)\u003c/a\u003e."
			],
			"example": [
				"a(1) = 1 =\u003e 1 + Omega(a(1)) = 1, so a(2) must be 2.",
				"Then 1 + Omega(a(2)) = 2, so a(3) = 4.",
				"Since 1 + Omega(4) = 3, a(4) = 3; then 1 + Omega(3) = 2 and a(5) = 6, etc."
			],
			"mathematica": [
				"a[1]=1; a[n_] := a[n] = Block[{s = Array[a, n-1], k, o = 1 + PrimeOmega@ a[n-1]}, k = o; While[ MemberQ[s, k], k += o]; k]; Array[a, 56] (* _Giovanni Resta_, Mar 04 2020 *)"
			],
			"program": [
				"(PARI) {A332878_vec(N,a=1,u=1)=vector(N,n, a=n=bigomega(a)+1; while(bittest(u,a), a+=n); u+=1\u003c\u003ca; a)} \\\\ _M. F. Hasler_, Mar 04 2020"
			],
			"xref": [
				"Cf. A000040, A001222, A061286."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_David James Sycamore_, Feb 28 2020",
			"references": 1,
			"revision": 33,
			"time": "2021-11-25T12:41:02-05:00",
			"created": "2020-03-07T17:24:06-05:00"
		}
	]
}