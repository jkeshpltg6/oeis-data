{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A188615",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 188615,
			"data": "3,3,9,8,3,6,9,0,9,4,5,4,1,2,1,9,3,7,0,9,6,3,9,2,5,1,3,3,9,1,7,6,4,0,6,6,3,8,8,2,4,4,6,9,0,3,3,2,4,5,8,0,7,1,4,3,1,9,2,3,9,6,2,4,8,9,9,1,5,8,8,8,6,6,4,8,4,8,4,1,1,4,6,0,7,6,5,7,9,2,5,0,0,1,9,7,6,1,2,8,5,2,1,2,9,7,6,3,8,0,7,4,0,2,2,9,4,4,7,4,1,5,2,3,9,3,5,7,5,6",
			"name": "Decimal expansion of Brocard angle of side-silver right triangle.",
			"comment": [
				"The Brocard angle is invariant of the size of the side-silver right triangle ABC.  The shape of ABC is given by sidelengths a,b,c, where a=r*b, and c=sqrt(a^2+b^2), where r=(silver ratio)=(1+sqrt(2)).  This is the unique right triangle matching the continued fraction [2,2,2,...] of r; i.e, under the side-partitioning procedure described in the 2007 reference, there are exactly 2 removable subtriangles at each stage.  (This is analogous to the removal of 2 squares at each stage of the partitioning of the silver rectangle as a nest of squares.)",
				"Archimedes's-like scheme: set p(0) = 1/(2*sqrt(2)), q(0) = 1/3; p(n+1) = 2*p(n)*q(n)/(p(n)+q(n)) (harmonic mean, i.e., 1/p(n+1) = (1/p(n) + 1/q(n))/2), q(n+1) = sqrt(p(n+1)*q(n)) (geometric mean, i.e., log(q(n+1)) = (log(p(n+1)) + log(q(n)))/2), for n \u003e= 0. The error of p(n) and q(n) decreases by a factor of approximately 4 each iteration, i.e., approximately 2 bits are gained by each iteration. Set r(n) = (2*q(n) + p(n))/3, the error decreases by a factor of approximately 16 for each iteration, i.e., approximately 4 bits are gained by each iteration. For a similar scheme see also A244644. - _A.H.M. Smeets_, Jul 12 2018",
				"This angle is also the half-angle at the summit of the Kelvin wake pattern traced by a boat. - _Robert FERREOL_, Sep 27 2019"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A188615/b188615.txt\"\u003eTable of n, a(n) for n = 0..5000\u003c/a\u003e",
				"Clark Kimberling, \u003ca href=\"http://www.heldermann.de/JGG/JGG11/JGG112/jgg11014.htm\"\u003eTwo kinds of golden triangles, generalized to match continued fractions\u003c/a\u003e, Journal for Geometry and Graphics, 11 (2007) 165-171.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Wake#Kelvin_wake_pattern\"\u003eKelvin wake pattern\u003c/a\u003e"
			],
			"formula": [
				"(Brocard angle) = arccot((a^2+b^2+c^2)/(4*area(ABC))) = arccot(sqrt(8)).",
				"Also equals arcsin(1/3) or arccsc(3). - _Jean-François Alcover_, May 29 2013"
			],
			"example": [
				"Brocard angle: 0.3398369094541219370963925133917640663882 approx.",
				"Brocard angle: 19.471220634490691369245999 degrees, approx."
			],
			"mathematica": [
				"r=1+2^(1/2);",
				"b=1; a=r*b; c=(a^2+b^2)^(1/2);",
				"area=(1/4)((a+b+c)(b+c-a)(c+a-b)(a+b-c))^(1/2);",
				"brocard=ArcCot[(a^2+b^2+c^2)/(4area)];",
				"N[brocard, 130]",
				"RealDigits[N[brocard,130]][[1]]",
				"N[180 brocard/Pi,130] (* degrees *)",
				"RealDigits[ArcCos[Sqrt[8/9]], 10, 50][[1]] (* _G. C. Greubel_, Nov 18 2017 *)"
			],
			"program": [
				"(PARI) acos(sqrt(8/9)) \\\\ _Charles R Greathouse IV_, May 02 2013",
				"(MAGMA) [Arccos(Sqrt(8/9))]; // _G. C. Greubel_, Nov 18 2017"
			],
			"xref": [
				"Cf. A188614, A188543, A152149."
			],
			"keyword": "nonn,cons",
			"offset": "0,1",
			"author": "_Clark Kimberling_, Apr 05 2011",
			"references": 9,
			"revision": 36,
			"time": "2019-09-29T14:36:07-04:00",
			"created": "2011-04-05T12:33:50-04:00"
		}
	]
}