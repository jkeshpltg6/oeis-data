{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A338874",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 338874,
			"data": "1,1,1,1,2,1,1,6,6,1,1,1,36,24,1,1,30,180,1440,120,1,1,1,1080,11520,7200,720,1,1,42,9072,2419200,672000,1814400,5040,1,1,1,90720,2322432,60480000,435456000,12700800,40320,1,1,30,38880,232243200,207360000,548674560000,21337344000,270950400,362880,1",
			"name": "Array T(n, m) read by ascending antidiagonals: denominators of shifted Bernoulli numbers B(n, m) where m \u003e= 0.",
			"link": [
				"Stefano Spezia, \u003ca href=\"/A338874/b338874.txt\"\u003eFirst 30 antidiagonals of the array, flattened\u003c/a\u003e",
				"Takao Komatsu, \u003ca href=\"https://www.researchgate.net/publication/344595540_SHIFTED_BERNOULLI_NUMBERS_AND_SHIFTED_FUBINI_NUMBERS\"\u003eShifted Bernoulli numbers and shifted Fubini numbers\u003c/a\u003e, Linear and Nonlinear Analysis, Volume 6, Number 2, 2020, 245-263."
			],
			"formula": [
				"T(n, m) = denominator(B(n, m)).",
				"B(n, m) = [x^n] n!*x^m/(exp(x) - E_m(x) + x^m), where E_m(x) = Sum_{n=0..m} x^n/n! (see Equation 2.1 in Komatsu).",
				"B(n, m) = - Sum_{k=0..n-1} n!*B(k, m)/((n - k + m)!*k!) for n \u003e 0 (see Lemma 2.1 in Komatsu).",
				"B(n, m) = n!*Sum_{k=1..n} (-1)^k*Sum_{i_1+...+i_k=n; i_1,...,i_k\u003e=1} Product_{j=1..k} 1/(i_j + m)! for n \u003e 0 (see Theorem 2.2 in Komatsu).",
				"B(n, m) = (-1)^n*n!*det(M(n, m)) where M(n, m) is the n X n Toeplitz matrix whose first row consists in 1/(m + 1)!, 1, 0, ..., 0 and whose first column consists in 1/(m + 1)!, 1/(m + 2)!, ..., 1/(m + n)! (see Theorem 2.3 in Komatsu).",
				"B(1, m) = -1/(m + 1)! (see Theorem 2.4 in Komatsu).",
				"B(n, m) = n!*Sum_{t_1+2*t_2+...+n*t_n=n} (t_1,...,t_n)!*(-1)^(t_1+…+t_n)*Product_{j=1..n} (1/(m + j)!)^t_j for n \u003e= m \u003e= 1 (see Theorem 2.7 in Komatsu).",
				"(-1)^n/(n + m)! = det(M(n, m)) where M(n, m) is the n X n Toeplitz matrix whose first row consists in B(1, m), 1, 0, ..., 0 and whose first column consists in B(1, m), B(2, m)/2!, ..., B(n, m)/n! (see Theorem 2.8 in Komatsu).",
				"Sum_{k=0..n} binomial(n, k)*B(k, m)*B(n-k, m) = - n!/(m^2*m!)*Sum_{l=0..n-1} ((m! - 1)/(m*m!))^(n-l-1)*(l*(m! - 1) + m)/l!*B(l, m) - (n - m)/m*B(n, m) for m \u003e 0 (see Theorem 4.1 in Komatsu)."
			],
			"example": [
				"Array T(n, m):",
				"n\\m|   0         1         2         3         4 ...",
				"---+--------------------------------------------",
				"0  |   1         1         1         1         1 ...",
				"1  |   1         2         6        24       120 ...",
				"2  |   1         6        36      1440      7200 ...",
				"3  |   1         1       180     11520    672000 ...",
				"4  |   1        30      1080   2419200  60480000 ...",
				"...",
				"Related table of shifted Bernoulli numbers B(n, m):",
				"   1      1        1              1                1 ...",
				"  -1   -1/2     -1/6          -1/24           -1/120 ...",
				"   1    1/6    -1/36       -19/1440         -19/7200 ...",
				"  -1      0    1/180      -53/11520      -709/672000 ...",
				"   1  -1/30  11/1080  -3113/2419200  -28813/60480000 ...",
				"  ..."
			],
			"mathematica": [
				"B[n_, m_]:=n!Coefficient[Series[x^m/(Exp[x]-Sum[x^k/k!, {k, 0, m}]+x^m), {x, 0, n}], x, n]; Table[Denominator[B[n-m,m]],{n,0,9},{m,0,n}]//Flatten"
			],
			"xref": [
				"Cf. A000012 (1st column and 1st row), A000142 (2nd row), A027641, A027642 (2nd column), A141056, A164555, A176327, A226513 (high-order Fubini numbers), A338875, A338876.",
				"Cf. A338873 (numerators)."
			],
			"keyword": "nonn,frac,tabl",
			"offset": "0,5",
			"author": "_Stefano Spezia_, Nov 13 2020",
			"references": 4,
			"revision": 38,
			"time": "2021-01-20T18:50:03-05:00",
			"created": "2020-12-23T07:38:24-05:00"
		}
	]
}