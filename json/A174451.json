{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A174451",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 174451,
			"data": "1,1,1,1,18,1,1,36,36,1,1,60,2160,60,1,1,90,5400,5400,90,1,1,126,11340,680400,11340,126,1,1,168,21168,1905120,1905120,21168,168,1,1,216,36288,4572288,411505920,4572288,36288,216,1,1,270,58320,9797760,1234517760,1234517760,9797760,58320,270,1",
			"name": "Triangle T(n, k, q) = n!*(n+1)!*q^k/((n-k)!(n-k+1)!) if floor(n/2) \u003e k-1, otherwise n!*(n+1)!*q^(n-k)/(k!*(k+1)!) for q = 3, read by rows.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A174451/b174451.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k, q) = n!*(n+1)!*q^k/((n-k)!(n-k+1)!) if floor(n/2) \u003e k-1, otherwise n!*(n+1)!*q^(n-k)/(k!*(k+1)!) for q = 3.",
				"T(n, n-k, q) = T(n, k, q).",
				"From _G. C. Greubel_, Nov 29 2021: (Start)",
				"T(2*n, n, q) = q^n*(2*n+1)!*Catalan(n) for q = 3.",
				"T(n, k, q) = binomial(n, k)*binomial(n+1, k+1) * ( k!*(k+1)!*q^k/(n-k+1) if (floor(n/2) \u003e= k), otherwise ((n-k)!)^2*q^(n-k) ), for q = 3. (End)"
			],
			"example": [
				"Triangle begins as:",
				"  1;",
				"  1,   1;",
				"  1,  18,     1;",
				"  1,  36,    36,       1;",
				"  1,  60,  2160,      60,          1;",
				"  1,  90,  5400,    5400,         90,          1;",
				"  1, 126, 11340,  680400,      11340,        126,       1;",
				"  1, 168, 21168, 1905120,    1905120,      21168,     168,     1;",
				"  1, 216, 36288, 4572288,  411505920,    4572288,   36288,   216,   1;",
				"  1, 270, 58320, 9797760, 1234517760, 1234517760, 9797760, 58320, 270, 1;"
			],
			"mathematica": [
				"T[n_, k_, q_]:= If[Floor[n/2]\u003ek-1, n!*(n+1)!*q^k/((n-k)!*(n-k+1)!), n!*(n+1)!*q^(n-k)/(k!*(k+1)!)];",
				"Table[T[n, k, 3], {n,0,12}, {k,0,n}]//Flatten"
			],
			"program": [
				"(MAGMA)",
				"F:= Factorial; // T = A174451",
				"T:= func\u003c n,k,q | Floor(n/2) gt k-1 select F(n)*F(n+1)*q^k/(F(n-k)*F(n-k+1)) else F(n)*F(n+1)*q^(n-k)/(F(k)*F(k+1)) \u003e;",
				"[T(n,k,3): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Nov 29 2021",
				"(Sage)",
				"f=factorial",
				"def A174451(n,k,q):",
				"    if ((n//2)\u003ek-1): return f(n)*f(n+1)*q^k/(f(n-k)*f(n-k+1))",
				"    else: return f(n)*f(n+1)*q^(n-k)/(f(k)*f(k+1))",
				"flatten([[A174451(n,k,3) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Nov 29 2021"
			],
			"xref": [
				"Cf. A174449 (q=1), A174450 (q=2), this sequence (q=3).",
				"Cf. A000108."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,5",
			"author": "_Roger L. Bagula_, Mar 20 2010",
			"ext": [
				"Edited by _G. C. Greubel_, Nov 29 2021"
			],
			"references": 3,
			"revision": 8,
			"time": "2021-11-30T05:00:23-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}