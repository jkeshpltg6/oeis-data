{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A336684",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 336684,
			"data": "0,0,1,0,1,2,0,1,2,3,1,2,3,4,0,1,2,3,4,5,0,1,2,3,4,5,6,1,2,3,4,5,7,0,1,2,3,4,5,6,7,8,1,2,3,4,6,7,8,9,0,1,2,3,4,7,10,1,2,3,4,5,6,7,8,10,11,1,2,3,4,5,6,7,8,9,10,11,12,0,1,2,3,4,5",
			"name": "Irregular triangle in which row n lists residues k found in the sequence Lucas(i) mod n.",
			"comment": [
				"For row n, it is sufficient to take the union of A000032(i) mod n for 0 \u003c= i \u003c= A106291(n - 1), since the Lucas numbers are cyclical mod n.",
				"Row n contains the Lucas number k \u003c n, and k such that (n + k) is a Lucas number.",
				"Row n for n in A224482 is complete, i.e., it contains all residues k (mod n). This includes n that is a perfect power of 3."
			],
			"formula": [
				"A066981(n) = length of row n.",
				"A223487(n) = n - A066981(n) = number of residues missing from row n.",
				"A224482(n) = rows n that have complete residue coverage, i.e., A066981(n) = n and A223487(n) = 0."
			],
			"example": [
				"Row 1 contains 0 by convention.",
				"Row 2 contains (0, 1) since the Lucas sequence contains both even and odd numbers.",
				"Row 5 contains (1, 2, 3, 4) since the Lucas numbers mod 5 is {2,1,3,4,2,1} repeated; we are missing the residue 0.",
				"Table begins as shown below, with residue k shown arranged in columns.",
				"n    k (mod n)",
				"--------------",
				"1:   0",
				"2:   0  1",
				"3:   0  1  2",
				"4:   0  1  2  3",
				"5:      1  2  3  4",
				"6:   0  1  2  3  4  5",
				"7:   0  1  2  3  4  5  6",
				"8:      1  2  3  4  5     7",
				"9:   0  1  2  3  4  5  6  7  8",
				"10:     1  2  3  4     6  7  8  9",
				"11:  0  1  2  3  4        7       10",
				"12:     1  2  3  4  5  6  7  8    10 11",
				"13:     1  2  3  4  5  6  7  8  9 10 11 12",
				"14:  0  1  2  3  4  5  6  7  8  9 10 11 12 13",
				"15:     1  2  3  4        7          11       14",
				"16:     1  2  3  4  5     7     9    11 12 13    15",
				"..."
			],
			"mathematica": [
				"{Most@ #, #} \u0026[Range[0, 1]]~Join~Array[Block[{w = {2, 1}}, Do[If[SequenceCount[w, {2, 1}] == 1, AppendTo[w, Mod[Total@ w[[-2 ;; -1]], #]], Break[]], {i, 2, Infinity}]; Union@ w] \u0026, 12, 3] // Flatten"
			],
			"xref": [
				"Cf. A000032, A066981, A106291, A223487. Analogous to A189768."
			],
			"keyword": "nonn,tabf,easy",
			"offset": "1,6",
			"author": "_Michael De Vlieger_, Oct 07 2020",
			"references": 1,
			"revision": 7,
			"time": "2021-02-21T10:56:07-05:00",
			"created": "2021-02-21T10:56:07-05:00"
		}
	]
}