{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A026317",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 26317,
			"data": "0,2,3,5,6,9,12,15,18,19,21,22,24,25,27,28,31,34,37,40,41,43,44,46,47,49,50,53,56,59,62,63,65,66,68,69,71,72,75,78,81,84,85,87,88,90,91,93,94,97,100,103,106,107,109,110,112,113,115",
			"name": "Nonnegative integers k such that |cos(k)| \u003e |sin(k+1)|.",
			"comment": [
				"The sequences A026317, A327136 and A327137 partition the nonnegative integers. - _Clark Kimberling_, Aug 23 2019",
				"Requirement can be rewritten cos^2(k) \u003e sin^2(k+1) =\u003e cos^2(k) \u003e 1-cos^2(k+1) =\u003e cos^2(k+1) \u003e 1-cos^2(k) =\u003e |cos(k+1)| \u003e |sin(k)|. - _R. J. Mathar_, Sep 03 2019",
				"These are also the numbers k such that sin(2k) \u003c sin(2k+2).",
				"Proof (Jean-Paul Allouche, Nov 14 2019):",
				"cos^2(n) \u003e sin^2(n+1) ;",
				"Formulas for squares Abramowitz-Stegun 4.3.31 and 4.3.32:",
				"1/2 + cos(2n)/2 \u003e 1/2 - cos(2n+2) ;",
				"cos(2n+2) + cos(2n) \u003e 0 ;",
				"Formulas for sums Abramowitz-Stegun 4.3.16 and 4.3.17:",
				"cos(2n)*cos(2) - sin(2n)*sin(2) + cos(2n) \u003e 0 ;",
				"(1+cos(2))*cos(2n) \u003e sin(2n)*sin 2;",
				"Multiply both sides by 1-cos(2) which is \u003e0:",
				"(1-cos^2(2))*cos(2n) \u003e (1-cos(2))*sin(2)*sin(2n) ;",
				"sin^2(2)*cos(2n) \u003e (1-cos(2))*sin(2)*sin(2n) ;",
				"sin(2)*cos(2n) \u003e (1-cos(2))*sin(2n) ;",
				"(1-cos(2))*sin(2n) \u003c cos(2n)*sin 2 ;",
				"sin(2n) - sin(2n)*cos(2) \u003c cos(2n)*sin(2);",
				"sin(2n) \u003c sin(2n)*cos(2)+cos(2n)*sin(2);",
				"And backward application of Abramowitz-Stegun 4.3.16",
				"sin(2n) \u003c sin(2n+2) q.e.d.",
				"Also nonnegative integers k such that cos(2k+1) \u003e 0. Note that sin(2k+2) - sin(2k) = 2*cos(2k+1)*sin(1). - _Jianing Song_, Nov 16 2019"
			],
			"mathematica": [
				"Select[Range[0,120],Abs[Cos[#]]\u003eAbs[Sin[#+1]]\u0026] (* _Harvey P. Dale_, Mar 04 2013 *)"
			],
			"program": [
				"(MAGMA) [k:k in [0..120]|Abs(Cos(k)) gt Abs(Sin(k+1))]; // _Marius A. Burtea_, Nov 14 2019"
			],
			"xref": [
				"Cf. A026309, A246303, A327138."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Clark Kimberling_",
			"references": 6,
			"revision": 22,
			"time": "2019-11-18T22:20:59-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}