{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A256282",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 256282,
			"data": "1,-1,1,0,1,-2,0,0,1,-4,2,0,0,-2,0,0,1,-2,4,0,2,0,0,0,0,-3,2,0,0,-2,0,0,1,0,2,0,4,-2,0,0,2,-2,0,0,0,-8,0,0,0,-1,3,0,2,-2,0,0,0,0,2,0,0,-2,0,0,1,-4,0,0,2,0,0,0,4,-2,2,0,0,0,0,0,2",
			"name": "Expansion of f(-q^3) * psi(q^3)^3 / (psi(q) * psi(q^9)) in powers of q where psi(), f() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A256282/b256282.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of eta(q) * eta(q^6)^6 * eta(q^9) / (eta(q^2)^2 * eta(q^3)^2 * eta(q^18)^2) in powers of q.",
				"Euler transform of period 18 sequence [ -1, 1, 1, 1, -1, -3, -1, 1, 0, 1, -1, -3, -1, 1, 1, 1, -1, -2, ...].",
				"Moebius transform is a period 72 sequence.",
				"a(n) = (-1)^n * A258256(n). a(2*n) = A258256(n). a(3*n + 1) = - A258277(n). a(3*n + 2) = A258278(n). a(4*n + 3) = 0.",
				"a(n) = -b(n) where b() is multiplicative with a(0) = 1,  b(2^e) = -1 if e\u003e0, b(3^e) = 2 * (1 + (-1)^e), a(p^e) = (1 + (-1)^e) / 2 if p == 3 (mod 4), a(p^e) = e+1 if p == 1 (mod 4). - _Michael Somos_, Jun 06 2015"
			],
			"example": [
				"G.f. = 1 - q + q^2 + q^4 - 2*q^5 + q^8 - 4*q^9 + 2*q^10 - 2*q^13 + q^16 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ q^(1/8) QPochhammer[ q^3] EllipticTheta[ 2, 0, q^(3/2)]^3 / (2 EllipticTheta[ 2, 0, q^(1/2)] EllipticTheta[ 2, 0, q^(9/2)]), {q, 0, n}];",
				"a[ n_] := If[ n \u003c 1, Boole[n == 0], DivisorSum[ n, {1, 2, -1, 0}[[Mod[#, 4, 1]]] If[ Divisible[#, 9], 4, 1] (-1)^(Boole[Mod[#, 8] == 6] + #) \u0026]]; (* _Michael Somos_, Jun 06 2015 *)",
				"a[ n_] := If[ n \u003c 1, Boole[ n==0 ], -Times @@ (Which[ # == 1, 1, # == 2, -1, Mod[#, 4] == 1, #2 + 1, True, If[# == 3, 4, 1] Mod[#2 + 1, 2]] \u0026 @@@ FactorInteger[n])]; (* _Michael Somos_, Jun 06 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n==0, sumdiv(n, d, [0, 1, 2, -1][d%4 + 1] * if(d%9, 1, 4) * (-1)^((d%8==6) + d)))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A) * eta(x^6 + A)^6 * eta(x^9 + A) / (eta(x^2 + A)^2 * eta(x^3 + A)^2 * eta(x^18 + A)^2), n))};"
			],
			"xref": [
				"Cf. A258256, A258277, A258278."
			],
			"keyword": "sign",
			"offset": "0,6",
			"author": "_Michael Somos_, Jun 02 2015",
			"references": 2,
			"revision": 20,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2015-06-02T23:02:23-04:00"
		}
	]
}