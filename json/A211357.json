{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A211357",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 211357,
			"data": "1,0,1,1,0,1,1,1,0,1,2,1,2,0,1,2,3,2,2,0,1,5,6,9,4,3,0,1,6,15,18,15,5,3,0,1,15,36,56,42,29,7,4,0,1,28,91,144,142,84,42,10,4,0,1,67,232,419,432,322,152,66,12,5,0,1,145,603,1160,1365,1080,630,252,90,15,5,0,1",
			"name": "Triangle read by rows: T(n,k) is the number of noncrossing partitions up to rotation of an n-set that contain k singleton blocks.",
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A211357/b211357.txt\"\u003eTable of n, a(n) for n = 0..1274\u003c/a\u003e (terms 0..90 from Tilman Piesk)"
			],
			"formula": [
				"T(n,k) = (1/n)*(A091867(n,k) - A171128(n,k) + Sum_{d|gcd(n,k)} phi(d) * A171128(n/d, k/d)) for n \u003e 0. - _Andrew Howroyd_, Nov 16 2017"
			],
			"example": [
				"From _Andrew Howroyd_, Nov 16 2017: (Start)",
				"Triangle begins: (n \u003e= 0, 0 \u003c= k \u003c= n)",
				"   1;",
				"   0,   1;",
				"   1,   0,   1;",
				"   1,   1,   0,   1;",
				"   2,   1,   2,   0,   1;",
				"   2,   3,   2,   2,   0,   1;",
				"   5,   6,   9,   4,   3,   0,  1;",
				"   6,  15,  18,  15,   5,   3,  0,  1;",
				"  15,  36,  56,  42,  29,   7,  4,  0, 1;",
				"  28,  91, 144, 142,  84,  42, 10,  4, 0, 1;",
				"  67, 232, 419, 432, 322, 152, 66, 12, 5, 0, 1;",
				"(End)"
			],
			"mathematica": [
				"a91867[n_, k_] := If[k == n, 1, (Binomial[n + 1, k]/(n + 1)) Sum[Binomial[n + 1 - k, j] Binomial[n - k - j - 1, j - 1], {j, 1, (n - k)/2}]];",
				"a2426[n_] := Sum[Binomial[n, 2*k]*Binomial[2*k, k], {k, 0, Floor[n/2]}];",
				"a171128[n_, k_] := Binomial[n, k]*a2426[n - k];",
				"T[0, 0] = 1;",
				"T[n_, k_] := (1/n)*(a91867[n, k] - a171128[n, k] + Sum[EulerPhi[d]* a171128[n/d, k/d], {d, Divisors[GCD[n, k]]}]);",
				"Table[T[n, k], {n, 0, 11}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Jul 03 2018, after _Andrew Howroyd_ *)"
			],
			"program": [
				"(PARI)",
				"g(x,y) = {1/sqrt((1 - (1 + y)*x)^2 - 4*x^2) - 1}",
				"S(n)={my(A=(1-sqrt(1-4*x/(1-(y-1)*x) + O(x^(n+2))))/(2*x)-1); Vec(1+intformal((A + sum(k=2, n, eulerphi(k)*g(x^k + O(x*x^n), y^k)))/x))}",
				"my(v=S(10)); for(n=1, #v, my(p=v[n]); for(k=0, n-1, print1(polcoeff(p,k), \", \")); print) \\\\ _Andrew Howroyd_, Nov 16 2017"
			],
			"xref": [
				"Column k=0 is A295198.",
				"Row sums are A054357.",
				"Cf. A091867 (noncrossing partitions of an n-set with k singleton blocks), A211359 (up to rotations and reflections).",
				"Cf. A171128."
			],
			"keyword": "nonn,tabl",
			"offset": "0,11",
			"author": "_Tilman Piesk_, Apr 12 2012",
			"references": 4,
			"revision": 19,
			"time": "2018-07-03T05:28:28-04:00",
			"created": "2012-04-18T15:50:53-04:00"
		}
	]
}