{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A258120",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 258120,
			"data": "0,1,1,2,1,2,0,3,2,2,1,3,0,1,2,4,0,3,1,3,1,2,0,4,2,1,3,2,0,3,0,5,2,1,1,4,0,2,1,4,1,2,0,3,3,1,0,5,0,3,1,2,0,4,2,3,2,1,0,4,0,1,2,6,1,3,0,2,1,2,0,5,1,1,3,3,1,2,0,5,4",
			"name": "Number of Fibonacci numbers in the partition having Heinz number n.",
			"comment": [
				"We define the Heinz number of a partition p = [p_1, p_2, ..., p_r] as Product(p_j-th prime, j=1...r) (concept used by _Alois P. Heinz_ in A215366 as an \"encoding\" of a partition). For example, for the partition [1, 1, 2, 4, 10] we get 2*2*3*7*29 = 2436; consequently, a(2436) = 3.",
				"The subprogram B of the Maple program gives the partition having Heinz number n.",
				"a(m*n) = a(m)+a(n)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A258120/b258120.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"a(2)=1 because B(2)=[1]; a(3)=1 because B(3)=[2]; a(4)=2 because B(4)=[1,1]; a(28)=2 because B(28)=[1,1,4]."
			],
			"maple": [
				"with(numtheory): a := proc (n) local B, F, ct, q: B := proc (n) local nn, j, m; nn := op(2, ifactors(n)): for j to nops(nn) do m[j] := op(j, nn) end do: [seq(seq(pi(op(1, m[i])), q = 1 .. op(2, m[i])), i = 1 .. nops(nn))] end proc; F := {seq(combinat['fibonacci'](1+i), i = 1 .. max(B(n)))}: ct := 0; for q to nops(B(n)) do if member(B(n)[q], F) = true then ct := ct+1 else  end if end do: ct end proc: seq(a(n), n = 1 .. 150);"
			],
			"mathematica": [
				"B[n_] := Module[{nn, j, m}, nn = FactorInteger[n]; For[j = 1, j \u003c= Length[nn], j++, m[j] = nn[[j]]]; Flatten[ Table[ Table[ PrimePi[ m[i][[1]]], {q, 1, m[i][[2]]}], {i, 1, Length[nn]}]]];",
				"a[n_] := Module[{F, ct, q}, F = Union @ Table[Fibonacci[1 + i], {i, 1, Max[ B[n]]}]; ct = 0; For[q = 1, q \u003c= Length[B[n]], q++, If[MemberQ[F, B[n][[q]]], ct++]]; ct];",
				"Table[a[n], {n, 1, 150}] (* _Jean-François Alcover_, Apr 25 2017, translated from Maple *)"
			],
			"xref": [
				"Cf. A000045, A215366."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Emeric Deutsch_, Jun 14 2015",
			"references": 1,
			"revision": 13,
			"time": "2020-02-14T16:39:16-05:00",
			"created": "2015-06-14T15:26:56-04:00"
		}
	]
}