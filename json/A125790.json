{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A125790",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 125790,
			"data": "1,1,1,1,2,1,1,4,3,1,1,10,9,4,1,1,36,35,16,5,1,1,202,201,84,25,6,1,1,1828,1827,656,165,36,7,1,1,27338,27337,8148,1625,286,49,8,1,1,692004,692003,167568,25509,3396,455,64,9,1,1,30251722,30251721,5866452,664665",
			"name": "Rectangular table where column k equals row sums of matrix power A078121^k, read by antidiagonals.",
			"comment": [
				"Determinant of n X n upper left submatrix is 2^[n(n-1)(n-2)/6] (see A125791). Related to partitions of numbers into powers of 2 (see A078121). Triangle A078121 shifts left one column under matrix square."
			],
			"link": [
				"G. Blom and C.-E. Froeberg, \u003ca href=\"/A002575/a002575.pdf\"\u003eOm myntvaexling (On money-changing) [Swedish]\u003c/a\u003e, Nordisk Matematisk Tidskrift, 10 (1962), 55-69, 103. [Annotated scanned copy] See Table 5."
			],
			"formula": [
				"T(n,k) = T(n,k-1) + T(n-1,2*k) for n\u003e0, k\u003e0, with T(0,n)=T(n,0)=1 for n\u003e=0."
			],
			"example": [
				"Recurrence T(n,k) = T(n,k-1) + T(n-1,2*k) is illustrated by:",
				"T(4,3) = T(4,2) + T(3,6) = 201 + 455 = 656;",
				"T(5,3) = T(5,2) + T(4,6) = 1827 + 6321 = 8148;",
				"T(6,3) = T(6,2) + T(5,6) = 27337 + 140231 = 167568.",
				"Rows of this table begin:",
				"1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...;",
				"1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, ...;",
				"1, 4, 9, 16, 25, 36, 49, 64, 81, 100, 121, 144, 169, ...;",
				"1, 10, 35, 84, 165, 286, 455, 680, 969, 1330, 1771, 2300, ...;",
				"1, 36, 201, 656, 1625, 3396, 6321, 10816, 17361, 26500, 38841, ...;",
				"1, 202, 1827, 8148, 25509, 64350, 140231, 274856, 497097, ...;",
				"1, 1828, 27337, 167568, 664665, 2026564, 5174449, 11622976, ...;",
				"1, 27338, 692003, 5866452, 29559717, 109082974, 326603719, ...;",
				"1, 692004, 30251721, 356855440, 2290267225, 10243585092, ...; ...",
				"Triangle A078121 begins:",
				"1;",
				"1, 1;",
				"1, 2, 1;",
				"1, 4, 4, 1;",
				"1, 10, 16, 8, 1;",
				"1, 36, 84, 64, 16, 1;",
				"1, 202, 656, 680, 256, 32, 1; ...",
				"where row sums form column 1 of this table A125790,",
				"and column k of A078121 equals column 2^k-1 of this table A125790.",
				"Matrix cube A078121^3 begins:",
				"1;",
				"3, 1;",
				"9, 6, 1;",
				"35, 36, 12, 1;",
				"201, 286, 144, 24, 1;",
				"1827, 3396, 2300, 576, 48, 1; ...",
				"where row sums form column 3 of this table A125790,",
				"and column 0 of A078121^3 forms column 2 of this table A125790."
			],
			"mathematica": [
				"T[n_, k_] := T[n, k] = T[n, k-1] + T[n-1, 2*k]; T[0, _] = T[_, 0] = 1; Table[T[n-k, k], {n, 0, 10}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Jun 15 2015 *)"
			],
			"program": [
				"(PARI) {T(n,k,p=0,q=2)=local(A=Mat(1), B); if(n\u003cp||p\u003c0, 0, for(m=1, n+1, B=matrix(m, m); for(i=1, m, for(j=1, i, if(j==i||j==1, B[i, j]=1, B[i, j]=(A^q)[i-1, j-1]); )); A=B); return((A^(k+1))[n+1, p+1]))}",
				"for(n=0,10,for(k=0,10,print1(T(n,k),\", \")); print(\"\"))"
			],
			"xref": [
				"Cf. A078121; A002577; A125791; columns: A002577, A125792, A125793, A125794, A125795, A125796; diagonals: A125797, A125798; A125799 (antidiagonal sums); related table: A125800 (q=3)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Paul D. Hanna_, Dec 10 2006, corrected Dec 12 2006",
			"references": 13,
			"revision": 12,
			"time": "2015-10-09T08:59:44-04:00",
			"created": "2007-01-12T03:00:00-05:00"
		}
	]
}