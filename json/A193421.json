{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A193421",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 193421,
			"data": "1,1,4,33,436,8185,206046,6622945,263313688,12627149265,716160702970,47284266221401,3587061106583604,309251317536586633,30017652739792964806,3254137305364883664945,391238883136463492841136,51846176797206158144925985",
			"name": "E.g.f.: Sum_{n\u003e=0} x^n * exp(n^2*x).",
			"formula": [
				"E.g.f.: A(x) = Sum_{n\u003e=0} x^n*exp(n*x)*Product_{k=1..n} (1 - x*exp((4*k-3)*x)) / (1 - x*exp((4*k-1)*x)), due to a q-series identity.",
				"Let q = exp(x), then the e.g.f. equals the continued fraction:",
				"A(x) = 1/(1- q*x/(1- q*(q^2-1)*x/(1- q^5*x/(1- q^3*(q^4-1)*x/(1- q^9*x/(1- q^5*(q^6-1)*x/(1- q^13*x/(1- q^7*(q^8-1)*x/(1- ...))))))))), due to a partial elliptic theta function identity.",
				"a(n) = n! * Sum_{k=0..n} (n-k)^(2*k)/k!. - _Paul D. Hanna_, Jan 19 2013",
				"O.g.f.: Sum_{k\u003e=0} k! * x^k / (1 - k^2*x)^(k+1). - _Ilya Gutkovskiy_, Jul 02 2019"
			],
			"example": [
				"E.g.f.: A(x) = 1 + x + 4*x^2/2! + 33*x^3/3! + 436*x^4/4! + 8185*x^5/5! + 206046*x^6/6! +...",
				"where",
				"A(x) = 1 + x*exp(x) + x^2*exp(4*x) + x^3*exp(9*x) + x^4*exp(16*x) +...",
				"By a q-series identity:",
				"A(x) = 1 + x*exp(x)*(1-x*exp(x))/(1-x*exp(3*x)) + x^2*exp(2*x)*(1-x*exp(x))*(1-x*exp(5*x))/((1-x*exp(3*x))*(1-x*exp(7*x))) + x^3*exp(3*x)*(1-x*exp(x))*(1-x*exp(5*x))*(1-x*exp(9*x))/((1-x*exp(3*x))*(1-x*exp(7*x))*(1-x*exp(11*x))) +..."
			],
			"mathematica": [
				"Flatten[{1,Table[n! * Sum[(n-k)^(2*k)/k!,{k,0,n}],{n,1,20}]}] (* _Vaclav Kotesovec_, Oct 21 2014 *)"
			],
			"program": [
				"(PARI) {a(n)=local(Egf); Egf=sum(m=0, n, x^m*exp(m^2*x+x*O(x^n))); n!*polcoeff(Egf, n)}",
				"(PARI) /* q-series identity: */",
				"{a(n)=local(A=1+x);for(i=1, n, A=sum(m=0, n, x^m*exp(m*x+x*O(x^n))*prod(k=1, m, (1-x*exp((4*k-3)*x+x*O(x^n)))/(1-x*exp((4*k-1)*x+x*O(x^n)))))); n!*polcoeff(A, n)}",
				"(PARI) {a(n) = n!*sum(k=0,n, (n-k)^(2*k)/k!)}",
				"for(n=0,20,print1(a(n),\", \"))"
			],
			"xref": [
				"Cf. A193467."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Paul D. Hanna_, Jul 27 2011",
			"references": 12,
			"revision": 17,
			"time": "2019-07-02T18:40:13-04:00",
			"created": "2011-07-27T04:02:22-04:00"
		}
	]
}