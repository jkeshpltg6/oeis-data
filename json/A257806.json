{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A257806",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 257806,
			"data": "0,-1,0,1,0,1,2,1,2,1,2,3,2,3,4,5,6,5,4,5,6,5,6,5,4,3,4,3,4,5,4,5,6,7,6,5,6,7,6,7,8,7,6,7,8,9,10,11,12,11,12,13,12,11,10,9,10,9,10,11,10,11,12,13,12,11,12,13,12,13,12,13,14,13,12,11,10,9,10,11,12,11,10,9,10,11,12,13,14,15,14,15,16,15,16,15,14",
			"name": "a(n) = A257808(n) - A257807(n).",
			"comment": [
				"Alternative description: Start with a(0) = 0, and then to obtain each a(n), look at each successive term in the infinite trunk of inverted binary beanstalk, from A233271(1) onward, subtracting one from a(n-1) if A233271(n) is odd, and adding one to a(n-1) if A233271(n) is even.",
				"In other words, starting from zero, iterate the map x -\u003e {x + 1 + number of nonleading zeros in the binary representation of x}, and note each time whether the result is odd or even: With odd results go one step down, and even results go one step up.﻿",
				"After the zeros at a(0), a(2) and a(4) and -1 at a(1), the terms stay strictly positive for a long time, although from the terms of A257805 it can be seen that the sequence must again fall to the negative side somewhere between n = 541110611 and n = 1051158027 (i.e., A218600(33) .. A218600(34)). Indeed the fourth zero occurs at n = 671605896, and the second negative term right after that as a(671605897) = -1.",
				"The maximum positive value reached prior to the slide into negative territory is 2614822 for a(278998626) and a(278998628). - _Hans Havermann_, May 23 2015"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A257806/b257806.txt\"\u003eTable of n, a(n) for n = 0..8727\u003c/a\u003e",
				"Hans Havermann, \u003ca href=\"http://chesswanks.com/num/a257806.png\"\u003eGraph of 2*10^9 terms\u003c/a\u003e",
				"Hans Havermann, \u003ca href=\"http://chesswanks.com/num/a257806zeros.png\"\u003eDetail graph of the eventual crossover to negative terms and a listing of its associated zeros\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Blancmange_curve\"\u003eBlancmange curve\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A257808(n) - A257807(n).",
				"a(0) = 0; and for n \u003e= 1, a(n) = a(n-1) + (-1)^A233271(n).",
				"Other identities. For all n \u003e= 0:",
				"a(A218600(n+1)) = -A257805(n)."
			],
			"example": [
				"We consider 0 to have no nonleading zeros, so first we get to 0 -\u003e 0+1+0 = 1, and 1 is odd, so we go one step down from the starting value a(0)=0, and thus a(1) = -1.",
				"1 has no nonleading zeros, so we get 1 -\u003e 1+1+0 = 2, and 2 is even, so we go one step up, and thus a(2) = 0.",
				"2 has one nonleading zero in binary \"10\", so we get 2 -\u003e 2+1+1 = 4, and 4 is also even, so we go one step up, and thus a(3) = 1.",
				"4 has two nonleading zeros in binary \"100\", so we get 4 -\u003e 4+2+1 = 7, 7 is odd, so we go one step down, and thus a(4) = 0."
			],
			"program": [
				"(PARI)",
				"A070939 = n-\u003e#binary(n)+!n; \\\\ From _M. F. Hasler_",
				"A080791 = n-\u003eif(n\u003c1,0,(A070939(n)-hammingweight(n)));",
				"A233272 = n-\u003e(n + A080791(n) + 1);",
				"A257806_write_bfile(up_to_n) = { my(n,a_n=0,b_n=0); for(n=0, up_to_n, write(\"b257806.txt\", n, \" \", a_n); b_n = A233272(b_n); a_n += ((-1)^b_n)); };",
				"A257806_write_bfile(8727);",
				"(Python)",
				"def A257806_print_upto(n):",
				"  a = 0",
				"  b = 0",
				"  for n in range(n):",
				"     print(b, end=\", \")",
				"     ta = a",
				"     c0 = 0",
				"     while ta\u003e0:",
				"         c0 += 1-(ta\u00261)",
				"         ta \u003e\u003e= 1",
				"     a += 1 + c0",
				"     b += ((2*(1-(a\u00261))) - 1)",
				"# By _Antti Karttunen_ after _Alex Ratushnyak_'s Python-code for A216431.",
				"(Scheme, two alternatives, the latter using memoizing definec-macro)",
				"(define (A257806 n) (- (A257808 n) (A257807 n)))",
				"(definec (A257806 n) (if (zero? n) n (+  (expt -1 (A233271 n)) (A257806 (- n 1)))))"
			],
			"xref": [
				"Cf. A218600, A233271, A233272, A257807, A257808, A257803, A257804, A257805.",
				"Cf. also A218542, A218543, A218789 and A233270 (compare the scatter plots)."
			],
			"keyword": "sign,base,look",
			"offset": "0,7",
			"author": "_Antti Karttunen_, May 12 2015",
			"references": 12,
			"revision": 42,
			"time": "2021-03-19T10:44:42-04:00",
			"created": "2015-05-13T15:44:04-04:00"
		}
	]
}