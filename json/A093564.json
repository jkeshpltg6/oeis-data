{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A093564",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 93564,
			"data": "1,7,1,7,8,1,7,15,9,1,7,22,24,10,1,7,29,46,34,11,1,7,36,75,80,45,12,1,7,43,111,155,125,57,13,1,7,50,154,266,280,182,70,14,1,7,57,204,420,546,462,252,84,15,1,7,64,261,624,966,1008,714,336,99,16,1,7,71,325,885",
			"name": "(7,1) Pascal triangle.",
			"comment": [
				"The array F(7;n,m) gives in the columns m\u003e=1 the figurate numbers based on A016993, including the 9-gonal numbers A001106, (see the W. Lang link).",
				"This is the seventh member, d=7, in the family of triangles of figurate numbers, called (d,1) Pascal triangles: A007318 (Pascal), A029653, A093560-3, for d=1..6.",
				"This is an example of a Riordan triangle (see A093560 for a comment and A053121 for a comment and the 1991 Shapiro et al. reference on the Riordan group). Therefore the o.g.f. for the row polynomials p(n,x):=Sum_{m=0..n} a(n,m)*x^m is G(z,x)=(1+6*z)/(1-(1+x)*z).",
				"The SW-NE diagonals give A022097(n-1) = Sum_{k=0..ceiling((n-1)/2)} a(n-1-k,k), n \u003e= 1, with n=0 value 6. Observation by _Paul Barry_, Apr 29 2004. Proof via recursion relations and comparison of inputs."
			],
			"reference": [
				"Kurt Hawlitschek, Johann Faulhaber 1580-1635, Veroeffentlichung der Stadtbibliothek Ulm, Band 18, Ulm, Germany, 1995, Ch. 2.1.4. Figurierte Zahlen.",
				"Ivo Schneider: Johannes Faulhaber 1580-1635, Birkhäuser, Basel, Boston, Berlin, 1993, ch. 5, pp. 109-122."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A093564/b093564.txt\"\u003eRows n = 0..125 of triangle, flattened\u003c/a\u003e",
				"W. Lang, \u003ca href=\"/A093564/a093564.txt\"\u003eFirst 10 rows and array of figurate numbers \u003c/a\u003e."
			],
			"formula": [
				"a(n, m)=F(7;n-m, m) for 0\u003c= m \u003c= n, otherwise 0, with F(7;0, 0)=1, F(7;n, 0)=7 if n\u003e=1 and F(7;n, m):=(7*n+m)*binomial(n+m-1, m-1)/m if m\u003e=1.",
				"Recursion: a(n, m)=0 if m\u003en, a(0, 0)= 1; a(n, 0)=7 if n\u003e=1; a(n, m)= a(n-1, m) + a(n-1, m-1).",
				"G.f. column m (without leading zeros): (1+6*x)/(1-x)^(m+1), m\u003e=0.",
				"T(n, k) = C(n, k) + 6*C(n-1, k). - _Philippe Deléham_, Aug 28 2005",
				"exp(x) * e.g.f. for row n = e.g.f. for diagonal n. For example, for n = 3 we have exp(x)*(7 + 15*x + 9*x^2/2! + x^3/3!) = 7 + 22*x + 46*x^2/2! + 80*x^3/3! + 125*x^4/4! + .... The same property holds more generally for Riordan arrays of the form ( f(x), x/(1 - x) ). - _Peter Bala_, Dec 22 2014"
			],
			"example": [
				"Triangle begins",
				"  [1];",
				"  [7,  1];",
				"  [7,  8,  1];",
				"  [7, 15,  9,  1];",
				"  ..."
			],
			"maple": [
				"N:= 20: # to get the first N rows",
				"T:=Matrix(N,N):",
				"T[1,1]:= 1:",
				"for m from 2 to N do",
				"T[m,1]:= 7:",
				"T[m,2..m]:= T[m-1,1..m-1] + T[m-1,2..m];",
				"od:",
				"for m from 1 to N do",
				"convert(T[m,1..m],list)",
				"od; # _Robert Israel_, Dec 28 2014"
			],
			"program": [
				"(Haskell)",
				"a093564 n k = a093564_tabl !! n !! k",
				"a093564_row n = a093564_tabl !! n",
				"a093564_tabl = [1] : iterate",
				"               (\\row -\u003e zipWith (+) ([0] ++ row) (row ++ [0])) [7, 1]",
				"-- _Reinhard Zumkeller_, Sep 01 2014"
			],
			"xref": [
				"Row sums: A000079(n+2), n\u003e=1, 1 for n=0, alternating row sums are 1 for n=0, 6 for n=2 and 0 otherwise.",
				"The column sequences give for m=1..9: A016993, A001106 (9-gonal), A007584, A051740, A051877, A050403, A027818, A034266, A055994.",
				"Cf. A093565 (d=8)."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Apr 22 2004",
			"references": 14,
			"revision": 37,
			"time": "2019-08-28T16:11:38-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}