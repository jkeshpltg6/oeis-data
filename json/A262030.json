{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A262030",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 262030,
			"data": "1,3,1,10,8,1,35,45,20,15,1,126,224,175,126,75,24,1,462,1050,1134,490,840,896,175,280,189,35,1,1716,4752,6468,4704,4950,7350,3528,2646,2400,2940,784,540,392,48,1,6435,21021,34320,33264,13860,27027,50688,41580,25872,15876,17325,29700,15120,14700,1764,5775,7680,2352,945,720,63,1",
			"name": "Partition array in Abramowitz-Stegun order: Schur functions evaluated at 1.",
			"comment": [
				"The length of row n \u003e= 1 of this irregular triangle is A000041(n) (partition numbers).",
				"The Abramowitz-Stegun (A-St) order of the partitions is used.",
				"For Schur functions (or polynomials) s^(lambda) = s(lambda(n, k);x[1], ..., x[n]) defined for the k-th partition of n (here in A-St order) see, e.g., the Macdonald reference, ch. I, 3. S-functions, p. 23, and Wikipedia reference. For the combinatorial interpretation of Schur functions see the Stanley reference.",
				"The partition lambda(n,k) has m = m(n,k) nonincreasing parts lamda_j, j = 1..m, (the reverse of the partitions given in A-St) and n-m 0's are appended to obtain a length n partition. E.g., lambda(4, 3) = (2, 2, 0, 0) with m = 2.",
				"The Schur function s(lambda(n, k),1,...,1) (n 1's) gives the number of semistandard Young tableaux (SSYT) for the Young (Ferrers) diagram of lambda(n, k) (forgetting about trailing 0's) with the box numbers taken out of the set {1, 2, ..., n} where the rows increase weakly and the columns increase strictly. See the Stanley reference pp. 309 and 310, and the example below.",
				"The sum of the row numbers give A209673: 1, 4, 19, 116, 751, 5552, 43219, 366088, ...",
				"Conjecture: The sum of the squares of row numbers give A054688: 1, 10, 165, 3876, ... = binomial(n^2+n-1, n). - _Wouter Meeussen_, Sep 25 2016"
			],
			"reference": [
				"Macdonald, I. G., Symmetric functions and Hall polynomials, Oxford University Press, 1979.",
				"Stanley, R. P., Enumerative Combinatorics, Vol. 2, Cambridge University Press 1999, sect. 7.30, pp. 308-316."
			],
			"link": [
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards Applied Math. Series 55, Tenth Printing, 1972, pp. 831-2.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Schur_polynomial\"\u003eSchur functions\u003c/a\u003e."
			],
			"formula": [
				"a(n, k) = Det_{i,j=1..n} x[i]^(lambda_j + n-j) / Det_{i,j=1..n} x[i]^(n-j), evaluated at x[i] = 1 for i = 1..n (after division). The denominator is the Vandermonde determinant, the numerator an alternant. See, e.g., the Macdonald reference p. 24."
			],
			"example": [
				"The irregular triangle begins (commas separate entries for partitions of like numbers of parts in A-St order):",
				"n\\k  1     2    3    4    5   6   7    8   9 10 11",
				"1:   1",
				"2:   3,    1",
				"3:  10,    8,   1",
				"4:  35,   45   20,  15,   1",
				"5: 126,  224  175, 126   75, 24,  1",
				"6: 462, 1050 1134  490, 840 896 175, 280 189,35, 1",
				"...",
				"Row 7: 1716, 4752 6468 4704, 4950 7350 3528 2646, 2400 2940 784, 540 392, 48, 1;",
				"Row 8: 6435, 21021 34320 33264 13860, 27027 50688 41580 25872 15876, 17325 29700 15120 14700 1764, 5775 7680 2352, 945 720, 63, 1.",
				"...",
				"n = 4, k = 4: lambda(4, 4) = (2,1,1,0) (m=3), SSYT (we use semicolons to separate the three rows): [1,1;2;3], [1,1;2;4], [1,1;3;4],",
				"  [1,2;2;3], [1,2;2;4], [1,2;3;4],",
				"  [1,3;2;3], [1,3;2;4], [1,3;3;4],",
				"  [1,4;2;3], [1,4;2;4], [1,4;3;4],",
				"  [2,2;3;4], [2,3;3;4], [2,4;3;4], hence a(4, 4) = 15. The three tableaux with distinct numbers are standard Young tableaux and give A117506(4, 4) = 3."
			],
			"xref": [
				"Cf. A054688, A117506, A209673, A210391 (in the diagonal)."
			],
			"keyword": "nonn,tabf",
			"offset": "1,2",
			"author": "_Wolfdieter Lang_, Oct 15 2015",
			"references": 0,
			"revision": 28,
			"time": "2020-04-07T23:22:34-04:00",
			"created": "2015-10-31T14:28:56-04:00"
		}
	]
}