{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A224345",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 224345,
			"data": "1,3,11,53,323,2359,19877,188591,1981963,22795849,284285351,3815293199,54762206985,836280215979,13527449608779,230894574439485,4144741143359355,78017419806432567,1535903379571939981,31550210953904250759",
			"name": "Number of closed normal forms of size n in lambda calculus with size 0 for the variables.",
			"link": [
				"Pierre Lescanne, \u003ca href=\"/A224345/b224345.txt\"\u003eTable of n, a(n) for n = 1..500\u003c/a\u003e",
				"Maciej Bendkowski, K Grygiel, P Tarau, \u003ca href=\"http://arxiv.org/abs/1612.07682\"\u003eRandom generation of closed simply-typed lambda-terms: a synergy between logic programming and Boltzmann samplers\u003c/a\u003e, arXiv preprint arXiv:1612.07682, 2016",
				"K. Grygiel and P. Lescanne, \u003ca href=\"http://arxiv.org/abs/1210.2610\"\u003e Counting and generating lambda-terms\u003c/a\u003e, arXiv preprint arXiv:1210.2610 [cs.LO], 2012-2013.",
				"Paul Tarau, \u003ca href=\"http://www.cse.unt.edu/~tarau/research/2015/dbx.pdf\"\u003eOn logic programming representations of lambda terms: de Bruijn indices, compression, type inference, combinatorial generation, normalization\u003c/a\u003e, 2015.",
				"P. Tarau, \u003ca href=\"http://arxiv.org/abs/1507.06944\"\u003eA Logic Programming Playground for Lambda Terms, Combinators, Types and Tree-based Arithmetic Computations\u003c/a\u003e, arXiv preprint arXiv:1507.06944 [cs.LO], 2015.",
				"Paul Tarau, \u003ca href=\"https://arxiv.org/abs/1608.03912\"\u003eA Hiking Trip Through the Orders of Magnitude: Deriving Efficient Generators for Closed Simply-Typed Lambda Terms and Normal Forms\u003c/a\u003e, arXiv preprint arXiv:1608.03912 [cs.PL], 2016."
			],
			"formula": [
				"a(n) = F(n,0) where F(0,m) = m, F(n+1,m) = F(n,m+1) + G(n+1,m), and G(0,m) = m, G(n+1,m) = sum(k=0..n, G(n-k,m)*F(k,m)*d(n,0) ) where d(0,i) = [i = 1], d(n+1,i) = sum(j=i..n+1, binomial(j,i)*d(n,j) + g(n+1,j) ) and g(0,i) = [i = 1], g(n+1,i) = sum(j=0..i, sum(k=0..n, g(k,j)*d(n-k,i-j) ) )."
			],
			"mathematica": [
				"F[0, m_] := m; F[n_, m_] := F[n, m] = F[n-1, m+1] + G[n, m]; G[0, m_] := m; G[n_, m_] := G[n, m] = Sum[G[n-k-1, m]*F[k, m], {k, 0, n-1}]; a[n_] := F[n, 0]; Array[a, 20] (* _Jean-François Alcover_, May 23 2017 *)"
			],
			"program": [
				"(Haskell)",
				"gtab :: [[Integer]]",
				"gtab = [0..] : [[s n m |  m \u003c- [0..]] | n \u003c- [1..]]",
				"  where s n m  = let fi =  [ftab !! i !! m | i \u003c- [0..(n-1)]]",
				"                     gi =  [gtab !! i !! m | i \u003c- [0..(n-1)]]",
				"                 in foldl (+) 0 (map (uncurry (*)) (zip fi (reverse gi)))",
				"ftab :: [[Integer]]",
				"ftab = [0..] : [[ftab !! (n-1) !! (m+1) + gtab !! n !! m | m\u003c-[0..]] | n\u003c-[1..]]",
				"f(n,m) = ftab !! n !! m"
			],
			"xref": [
				"Cf. A220894, A135501, A220895, A220896, A220897.",
				"Cf. A195691 for another size of the terms."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Pierre Lescanne_, Apr 04 2013",
			"references": 2,
			"revision": 39,
			"time": "2018-01-20T11:51:36-05:00",
			"created": "2013-04-05T11:59:32-04:00"
		}
	]
}