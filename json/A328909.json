{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A328909",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 328909,
			"data": "0,9,2,5,8,3,6,1,4,7,10,13,28,31,14,11,26,23,44,19,22,43,40,17,34,37,18,15,32,29,52,25,46,21,42,69,20,39,16,33,12,27,24,45,74,41,68,103,38,35,60,93,30,53,84,49,78,115,160,75,116,47,76,113,72,107,150,67,36,61,94,57,54,85,50,79,82",
			"name": "Knight tour on spirally numbered infinite board, when the knight always jumps on the unvisited square closest to the origin, first according to the sup-norm, then 2-norm, then number of the square: a(n) = number of square visited at move n.",
			"comment": [
				"Differs from A326924 (where only the 2-norm is considered) from a(34) = 42 on, and from A316328 (which considers only the number of the square) from a(48) = 38 on.",
				"When the knight lands on square number a(25108) = 21040 of coordinates (73, -57), there is no unvisited square within reach. The sequence then stops, or can be extended by specifying that the knight has to go back on his path until an unvisited square comes within reach, as in A323809.",
				"The least unvisited square at move 25108 is square number 17822 at (67,67). It is however close to the border of the visited region and the knight will visit it in the infinite extension of the sequence shortly after, at move n = 25358. Is there a square that will never be visited in that infinite extension? (Cf. comments in A323809.) - _M. F. Hasler_, Nov 04 2019"
			],
			"link": [
				"M. F. Hasler, \u003ca href=\"/A328909/b328909.txt\"\u003eTable of n, a(n) for n = 0..25108\u003c/a\u003e",
				"M. F. Hasler, \u003ca href=\"/wiki/Knight_tours\"\u003eKnight tours\u003c/a\u003e, OEIS wiki, Nov. 2019"
			],
			"formula": [
				"A328929(n) = max(|A174344(a(n))|, |A274923(a(n))|) = sup norm of the coordinates of square a(n)."
			],
			"example": [
				"The squares are numbered as in the spiral given in A174344 (upside down to get a counterclockwise spiral, but this is irrelevant here).",
				"The knight starts at a(0) = 0 with coordinates (0, 0).",
				"It jumps to a(1) = 9 with coordinates (2, -1): all 8 available squares (+-2, +-1) and (+-1, +-2) are at the same sup-norm and Euclidean distance from the origin, but square number 9 has the smallest number."
			],
			"program": [
				"{local(coords(n, m=sqrtint(n), k=m\\/2)=if(m\u003c=n-=4*k^2, [n-3*k, -k], n\u003e=0, [-k, k-n], n\u003e=-m, [-k-n, k], [k, 3*k+n]), U=[]/*used squares*/, K=vector(8, i, [(-1)^(i\\2)\u003c\u003c(i\u003e4), (-1)^i\u003c\u003c(i\u003c5)])/* knight moves*/, pos(x,y)=if(y\u003e=abs(x), 4*y^2-y-x, -x\u003e=abs(y),4*x^2-x-y, -y\u003e=abs(x),(4*y-3)*y+x, (4*x-3)*x+y), t(x, p=pos(x[1],x[2]))=if(p\u003c=U[1]||setsearch(U, p), oo, [vecmax(abs(x)), norml2(x), p]), nxt(p, x=coords(p))=vecsort(apply(K-\u003et(x+K), K))[1][3]); my(A=List(0)/*list of positions*/); for(n=1, oo, U=setunion(U, [A[n]]); while(#U\u003e1\u0026\u0026U[2]==U[1]+1, U=U[^1]); iferr(listput(A, nxt(A[n])), E, break)); print(\"Index of last term: \", #A-1); A328909(n)=A[n+1];} \\\\ To compute the infinite extension of the sequence, set en upper limit to the for() loop and replace \"break\" by listput(A, A[n-1])"
			],
			"xref": [
				"Cf. A328929 for the value on the visited square, sup norm of coordinates of a(n).",
				"Cf. A323809 ~ A316328 ~ A316667, A326924, A328908 (variants).",
				"Cf. A174344, A274923, A296030 (coordinates of square number n)."
			],
			"keyword": "nonn,fini,full",
			"offset": "0,2",
			"author": "_M. F. Hasler_, Oct 31 2019",
			"references": 11,
			"revision": 20,
			"time": "2021-02-25T21:23:44-05:00",
			"created": "2019-10-31T21:33:53-04:00"
		}
	]
}