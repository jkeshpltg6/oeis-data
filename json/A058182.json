{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A058182",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 58182,
			"data": "1,0,1,1,2,5,27,734,538783,290287121823,84266613096281243382112,7100862082718357559748563880517486086728702367,50422242317787290639189291009890702507917377925161079229314384058371278254659634544914784801",
			"name": "a(n) = a(n-1)^2 + a(n-2) for n \u003e= 2 with a(0) = 1 and a(1) = 0.",
			"comment": [
				"Has property that CONTINUANT([1, 1, 2, 5, 27, 734, 538783, ...]) = [1, 2, 5, 27, 734, 538783, ...]. - _N. J. A. Sloane_ Jul 19 2002",
				"For n \u003e 2, a(n) is the numerator of the simplified continued fraction resulting from [a(2), a(3), ..., a(n)]. Therefore, for n \u003e 2, a(n) represents the number of ways to tile a (n-2)-board with dominoes and stackable squares, where nothing can be stacked on a domino but otherwise for 2 \u003c i \u003c n, the i-th cell may be stacked by as many as a(i) squares (see Benjamin, A. and Quinn, J.). - _Melvin Peralta_, Feb 22 2016"
			],
			"reference": [
				"Arthur Benjamin and Jennifer Quinn, Proofs that Really Count, Mathematical Association of America, 2003, see pages 49-51."
			],
			"link": [
				"Melvin Peralta, \u003ca href=\"/A058182/b058182.txt\"\u003eTable of n, a(n) for n = 0..15\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/transforms.txt\"\u003eTransforms\u003c/a\u003e",
				"\u003ca href=\"/index/Aa#AHSL\"\u003eIndex entries for sequences of form a(n+1)=a(n)^2 + ...\u003c/a\u003e"
			],
			"formula": [
				"a(n)^2 = a(n+1) - a(n-1), a(-1-n) = -a(n).",
				"For n \u003e 1, a(n+1) = floor(c^(2^n)) where c=1.108604586393628626769904017539.... - _Benoit Cloitre_, Nov 30 2002",
				"a(n+1) = a(n)^2 + floor(sqrt(a(n))) = A000290(a(n)) + A000196(a(n)) for n \u003e 2. - _Reinhard Zumkeller_, May 16 2006"
			],
			"example": [
				"a(6) = a(5)^2 + a(4) = 5^2 + 2 = 27."
			],
			"mathematica": [
				"Join[{a=1,b=0},Table[c=a+b^2;a=b;b=c,{n,12}]] (* _Vladimir Joseph Stephan Orlovsky_, Jan 22 2011 *)",
				"Join[{1},Transpose[NestList[{Last[#],Last[#]^2+First[#]}\u0026,{0,1},12]][[1]]] (* _Harvey P. Dale_, May 15 2011 *)",
				"RecurrenceTable[{a[0] == 1, a[1] == 0, a[n] == a[n-1]^2 + a[n-2]}, a, {n, 13}] (* _Vincenzo Librandi_, Feb 23 2016 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c0, -a(-1-n), if(n\u003c2, 1-n, a(n-1)^2+a(n-2))) /* _Michael Somos_, May 05 2005 */",
				"(MAGMA) I:=[1,0]; [n le 2 select I[n] else Self(n-1)^2+Self(n-2): n in [1..13]]; // _Vincenzo Librandi_, Feb 23 2016"
			],
			"xref": [
				"Cf. A000278, A005605, A058181."
			],
			"keyword": "nonn,nice,eigen",
			"offset": "0,5",
			"author": "_Henry Bottomley_, Nov 15 2000",
			"ext": [
				"More terms from _Reinhard Zumkeller_, May 16 2006"
			],
			"references": 8,
			"revision": 46,
			"time": "2019-11-03T22:37:39-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}