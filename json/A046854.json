{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A046854",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 46854,
			"data": "1,1,1,1,1,1,1,2,1,1,1,2,3,1,1,1,3,3,4,1,1,1,3,6,4,5,1,1,1,4,6,10,5,6,1,1,1,4,10,10,15,6,7,1,1,1,5,10,20,15,21,7,8,1,1,1,5,15,20,35,21,28,8,9,1,1,1,6,15,35,35,56,28,36,9,10,1,1,1,6,21,35,70,56,84,36,45,10,11,1,1",
			"name": "Triangle T(n, k) = binomial(floor((n+k)/2), k), n\u003e=0, n \u003e= k \u003e= 0.",
			"comment": [
				"Row sums are Fibonacci(n+2). Diagonal sums are A016116. - _Paul Barry_, Jul 07 2004",
				"Riordan array (1/(1-x), x/(1-x^2)). Matrix inverse is A106180. - _Paul Barry_, Apr 24 2005",
				"As an infinite lower triangular matrix * [1,2,3,...] = A055244. - _Gary W. Adamson_, Dec 23 2008",
				"From _Emeric Deutsch_, Jun 18 2010: (Start)",
				"T(n,k) is the number of alternating parity increasing subsequences of {1,2,...,n} of size k, starting with an odd number (Terquem's problem, see the Riordan reference, p. 17). Example: T(8,5)=6 because we have 12345, 12347, 12367, 12567, 14567, and 34567.",
				"T(n,k) is the number of alternating parity increasing subsequences of {1,2,...,n,n+1} of size k, starting with an even number. Example: T(7,4)=5 because we have 2345, 2347, 2367, 2567, and 4567. (End)",
				"From _L. Edson Jeffery_, Mar 01 2011: (Start)",
				"This triangle can be constructed as follows. Interlace two copies of the table of binomial coefficients to get the preliminary table",
				"  1",
				"  1",
				"  1  1",
				"  1  1",
				"  1  2  1",
				"  1  2  1",
				"  1  3  3  1",
				"  1  3  3  1",
				"...,",
				"then shift each entire r-th column up r rows, r=0,1,2,.... Also, a signed version of this sequence (A187660 in tabular form) begins with",
				"  1;",
				"  1, -1;",
				"  1, -1, -1;",
				"  1, -2, -1, 1;",
				"  1, -2, -3, 1, 1; ...",
				"(compare with A066170, A130777). Let T(N,k) denote the k-th entry in row N of the signed table. Then, for N\u003e1, row N gives the coefficients of the characteristic function p_N(x)=Sum[k=0..N, T(N,k)x^(N-k)]=0 of the N X N matrix U_N=[(0 ... 0 1);(0 ... 0 1 1);...;(0 1 ... 1);(1 ... 1)]. Now let Q_r(t) be a polynomial with recurrence relation Q_r(t)=t*Q_(r-1)(t)-Q_(r-2)(t)  (r\u003e1), with Q_0(t)=1 and Q_1(t)=t. Then p_N(x)=0 has solutions Q_(N-1)(phi_j), where phi_j=2*(-1)^(j-1)*cos(j*Pi/(2*N+1)), j=1,2,...,N.",
				"For example, row N=3 is {1,-2,-1,1}, giving the coefficients of the characteristic function p_3(x)=x^3-2*x^2-x+1=0 for the 3 X 3 matrix U_3=[(0 0 1);(0 1 1);(1 1 1)], with eigenvalues Q_2(phi_j)=[2*(-1)^(j-1)*cos(j*Pi/7)]^2-1, j=1,2,3. (End)",
				"Given the signed polynomials (+--++--,...) of the triangle, the largest root of the n-th row polynomial is the longest (2n+1) regular polygon diagonal length, with edge = 1. Example: the largest root to x^3 - 2x^2 - x + 1 = 0 is 2.24697...; the longest heptagon diagonal, sin(3*Pi/7)/sin(Pi/7). - _Gary W. Adamson_, Sep 06 2011",
				"Given the signed polynomials from _Gary W. Adamson_'s comment, the largest root of the n-th polynomial also equals the length from the center to a corner (vertex) of a regular 2*(2*n+1)-sided polygon with side (edge) length = 1. - _L. Edson Jeffery_, Jan 01 2012",
				"Put f(x,0) = 1 and f(x,n) = x + 1/f(x,n-1). Then f(x,n) = u(x,n)/v(x,n), where u(x,n) and v(x,n) are polynomials. The flattened triangles of coefficients of u and v are both essentially A046854, as indicated by the Mathematica program headed \"Polynomials\". - _Clark Kimberling_, Oct 12 2014",
				"From _Jeremy Dover_, Jun 07 2016: (Start)",
				"T(n,k) is the number of binary strings of length n+1 starting with 0 that have exactly k pairs of consecutive 0's and no pairs of consecutive 1's.",
				"T(n,k) is the number of binary strings of length n+2 starting with 1 that have exactly k pairs of consecutive 0's and no pairs of consecutive 1's. (End)"
			],
			"reference": [
				"J. Riordan, An Introduction to Combinatorial Analysis, Princeton University Press, 1978. [_Emeric Deutsch_, Jun 18 2010]"
			],
			"link": [
				"Nathaniel Johnston, \u003ca href=\"/A046854/b046854.txt\"\u003eRows 0..100, flattened\u003c/a\u003e",
				"Jeremy M. Dover, \u003ca href=\"http://arxiv.org/abs/1609.00980\"\u003eSome Notes on Pairs in Binary Strings\u003c/a\u003e, arXiv:1609.00980 [math.CO], 2016.",
				"Dominique Foata and Guo-Niu Han, \u003ca href=\"https://arxiv.org/abs/1304.2486\"\u003eMultivariable Tangent and Secant q-derivative Polynomials\u003c/a\u003e, see Fig. 10.1.  arXiv:1304.2486 [math.CO]",
				"\u003ca href=\"/index/Pas#Pascal\"\u003eIndex entries for triangles and arrays related to Pascal's triangle\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = binomial(floor((n+k)/2), k).",
				"G.f.: (1+x)/(1-x*y-x^2). - _Ralf Stephan_, Feb 13 2005",
				"Triangle = A097806 * A049310, as infinite lower triangular matrices. - _Gary W. Adamson_, Oct 28 2007",
				"T(n,k) = A065941(n,n-k) = abs(A130777(n,k)) = abs(A066170(n,k)) = abs(A187660(n,k)). - _Johannes W. Meijer_, Aug 08 2011",
				"For n \u003e 1: T(n, k) = T(n-1, k-1) + T(n-2, k), 0 \u003c k \u003c n-1. - _Reinhard Zumkeller_, Apr 24 2013"
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  1 1;",
				"  1 1 1;",
				"  1 2 1 1;",
				"  1 2 3 1 1;",
				"  1 3 3 4 1 1; ..."
			],
			"maple": [
				"A046854:= proc(n,k): binomial(floor(n/2+k/2), k) end: seq(seq(A046854(n,k),k=0..n),n=0..16); # _Nathaniel Johnston_, Jun 30 2011"
			],
			"mathematica": [
				"Table[Binomial[Floor[(n+k)/2], k], {n,0,16}, {k,0,n}]//Flatten",
				"(* next program: Polynomials *)",
				"z = 12; f[x_, n_] := x + 1/f[x, n - 1]; f[x_, 1] = 1;",
				"t = Table[Factor[f[x, n]], {n, 1, z}]",
				"u = Flatten[CoefficientList[Numerator[t], x]] (* A046854 *)",
				"v = Flatten[CoefficientList[Denominator[t], x]]",
				"(* _Clark Kimberling_, Oct 13 2014 *)"
			],
			"program": [
				"(Haskell)",
				"a046854 n k = a046854_tabl !! n !! k",
				"a046854_row n = a046854_tabl !! n",
				"a046854_tabl = [1] : f [1] [1,1] where",
				"   f us vs = vs : f vs  (zipWith (+) (us ++ [0,0]) ([0] ++ vs))",
				"-- _Reinhard Zumkeller_, Apr 24 2013",
				"(PARI) T(n,k) = binomial((n+k)\\2, k); \\\\ _G. C. Greubel_, Jul 13 2019",
				"(MAGMA) [Binomial(Floor((n+k)/2), k): k in [0..n], n in [0..16]]; // _G. C. Greubel_, Jul 13 2019",
				"(Sage) [[binomial(floor((n+k)/2), k) for k in (0..n)] for n in (0..16)] # _G. C. Greubel_, Jul 13 2019",
				"(GAP) Flat(List([0..16], n-\u003e List([0..n], k-\u003e Binomial(Int((n+k)/2), k) ))); # _G. C. Greubel_, Jul 13 2019"
			],
			"xref": [
				"Reflected version of A065941, which is considered the main entry. A deficient version is in A030111.",
				"Cf. A066170, A097806, A049310, A187660.",
				"Cf. A055244. - _Gary W. Adamson_, Dec 23 2008"
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,8",
			"author": "_Wouter Meeussen_",
			"references": 52,
			"revision": 83,
			"time": "2021-06-05T06:03:04-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}