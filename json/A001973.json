{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001973",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1973,
			"id": "M2441 N0969",
			"data": "1,1,3,5,8,12,18,24,33,43,55,69,86,104,126,150,177,207,241,277,318,362,410,462,519,579,645,715,790,870,956,1046,1143,1245,1353,1467,1588,1714,1848,1988",
			"name": "Expansion of (1+x^3)/((1-x)*(1-x^2)^2*(1-x^3)).",
			"comment": [
				"a(1..3)=0; a(n) is the number of partitions of 2*(n+1) with 4 different numbers from the set {1,...,n}; the number of partitions of 2*n + 2 - C and 2*n + 2 + C are equal; example: n=6; 2*n + 2 = 14; a(6)=3; (10,1), (11,1), (12,2), (13,2), (14,3), (15,2), (16,2), (17,1), (18,1). - _Paul Weisenhorn_, Jun 01 2009"
			],
			"reference": [
				"A. Cayley, Numerical tables supplementary to second memoir on quantics, Collected Mathematical Papers. Vols. 1-13, Cambridge Univ. Press, London, 1889-1897, Vol. 2, pp. 276-281.",
				"M. Jeger, Einfuehrung in die Kombinatorik, Klett, 1975, pages 110ff. [From _Paul Weisenhorn_, Jun 01 2009]",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A001973/b001973.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"A. Cayley, \u003ca href=\"/A001971/a001971.pdf\"\u003eNumerical tables supplementary to second memoir on quantics\u003c/a\u003e, Collected Mathematical Papers. Vols. 1-13, Cambridge Univ. Press, London, 1889-1897, Vol. 2, pp. 276-281. [Annotated scanned copy]",
				"Shalosh B. Ekhad, Doron Zeilberger, \u003ca href=\"https://arxiv.org/abs/1901.08172\"\u003eIn How many ways can I carry a total of n coins in my two pockets, and have the same amount in both pockets?\u003c/a\u003e, arXiv:1901.08172 [math.CO], 2019.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"\u003ca href=\"/index/Rec#order_07\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,0,-1,-1,0,2,-1)"
			],
			"formula": [
				"a(n) is the coefficient of x^(2*n+2) in the g.f. Product_{s=1..4} (x^s - x^(n+1))/(1-x^s). - _Paul Weisenhorn_, Jun 01 2009",
				"a(n) = 2*a(n-1) - a(n-3) - a(n-4) + 2*a(n-6) - a(n-7). _Vincenzo Librandi_, Jun 11 2012"
			],
			"maple": [
				"A001973:=(1-z+z**2)/(z+1)/(z**2+z+1)/(z-1)**4; # _Simon Plouffe_ in his 1992 dissertation",
				"with(combstruct):ZL:=[st, {st=Prod(left, right), left=Set(U, card=r+1), right=Set(U, card\u003cr), U=Sequence(Z, card\u003e=2)}, unlabeled]: subs(r=2, stack): seq(count(subs(r=2, ZL), size=m), m=6..45) ; # _Zerinvary Lajos_, Feb 07 2008"
			],
			"mathematica": [
				"CoefficientList[Series[(1+x^3)/((1-x)*(1-x^2)^2*(1-x^3)),{x,0,40}],x] (* _Vincenzo Librandi_, Jun 11 2012 *)"
			],
			"program": [
				"(PARI) Vec((1+x^3)/((1-x)*(1-x^2)^2*(1-x^3))+O(x^99)) \\\\ _Charles R Greathouse IV_, Sep 23 2012"
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"references": 7,
			"revision": 53,
			"time": "2021-03-12T22:32:35-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}