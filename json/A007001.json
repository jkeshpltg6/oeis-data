{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007001",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7001,
			"id": "M0108",
			"data": "1,2,1,2,3,1,2,1,2,3,1,2,3,4,1,2,1,2,3,1,2,1,2,3,1,2,3,4,1,2,1,2,3,1,2,3,4,1,2,3,4,5,1,2,1,2,3,1,2,1,2,3,1,2,3,4,1,2,1,2,3,1,2,1,2,3,1,2,3,4,1,2,1,2,3,1,2,3,4,1,2,3,4,5,1,2,1,2,3,1,2,1,2,3,1,2,3,4,1,2,1,2,3,1,2",
			"name": "Trajectory of 1 under the morphism 1 -\u003e 12, 2 -\u003e 123, 3 -\u003e 1234, etc.",
			"comment": [
				"Records in this sequence occur at positions: 1, 2, 5, 14, 42, 132, 429, 1430, ... (which appear to be the Catalan numbers A000108). - _Robert G. Wilson v_, May 07 2005",
				"The records do occur at Catalan numbers. Of the first C(n) numbers, the number that are equal to k is A033184(n,k), with the one n last. - _Franklin T. Adams-Watters_, Mar 29 2009",
				"Let (T(1) \u003c T(2) \u003c ... \u003c T(A000108(m))) denote the sequence of Young tableaux of shape (2^m) ordered lexicographically with respect to their columns, and let f(T(i), T(j)) denote the first label of disagreement among T(i) and T(j). Then, empirically, if we take away the zeros from (f(T(1), T(A000108(m) - i + 1)) - f(T(A000108(m) - i), T(A000108(m) - i + 1)), i=1..A000108(m)-1), we obtain the first A000108(m - 1) - 1 terms in this sequence. This is illustrated in the below example. - _John M. Campbell_, Sep 07 2018",
				"The average of the first k terms tends to 3 as k tends to infinity. - _Andrew Slattery_, Jan 19 2021"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"J. West, Generating trees and forbidden subsequences, Proc. 6th FPSAC [ Conference on Formal Power Series and Algebraic Combinatorics ] (1994), pp. 441-450 (see p. 443)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A007001/b007001.txt\"\u003eTable of n, a(n) for n = 1..4862\u003c/a\u003e (8 iterations)",
				"C. Banderier, A. Denise, P. Flajolet, M. Bousquet-Mélou et al., \u003ca href=\"https://doi.org/10.1016/S0012-365X(01)00250-3\"\u003eGenerating Functions for Generating Trees\u003c/a\u003e, Discrete Mathematics 246(1-3), March 2002, pp. 29-55.",
				"Antti Karttunen, \u003ca href=\"/A080237/a080237tree.txt\"\u003eNotes concerning A080237-tree and related sequences.\u003c/a\u003e",
				"S. Lehr, J. Shallit and J. Tromp, \u003ca href=\"https://doi.org/10.1016/0304-3975(95)00234-0\"\u003eOn the vector space of the automatic reals\u003c/a\u003e, Theoret. Comput. Sci. 163 (1996), no. 1-2, 193-210.",
				"James Propp and N. J. A. Sloane, \u003ca href=\"/A006997/a006997.pdf\"\u003eEmail, March 1994\u003c/a\u003e",
				"\u003ca href=\"/index/Fi#FIXEDPOINTS\"\u003eIndex entries for sequences that are fixed points of mappings\u003c/a\u003e"
			],
			"formula": [
				"From n \u003e 1 onward a(n) = A080237(A081291(n-1)). - _Antti Karttunen_, Jul 31 2003"
			],
			"example": [
				"From _John M. Campbell_, Sep 07 2018: (Start)",
				"Letting m = 5, as above let (T(1) \u003c T(2) \u003c ... \u003c T(42)) denote the lexicographic sequence of Young tableaux of shape (2, 2, 2, 2, 2). In this case, the sequence (f(T(1), T(43 - i)) - f(T(42 - i), T(43 - i)), i=1..41) is equal to (0, 1, 0, 0, 2, 0, 1, 0, 0, 2, 0, 0, 0, 3, 0, 1, 0, 0, 2, 0, 1, 0, 0, 2, 0, 0, 0, 3, 0, 1, 0, 0, 2, 0, 0, 0, 3, 0, 0, 0, 0). Removing the zeroes from this tuple, we obtain (1, 2, 1, 2, 3, 1, 2, 1, 2, 3, 1, 2, 3), which gives us the first 13 = A000108(m - 1) - 1 terms in this sequence. For example, the first term in the preceding tuple is 0 since T(1) and T(42) are respectively",
				"   [ 5 10] [ 9 10]",
				"   [ 4 9 ] [ 7 8 ]",
				"   [ 3 8 ] [ 5 6 ]",
				"   [ 2 7 ] [ 3 4 ]",
				"   [ 1 6 ] [ 1 2 ]",
				"and T(41) is equal to",
				"   [ 9 10]",
				"   [ 7 8 ]",
				"   [ 5 6 ]",
				"   [ 2 4 ]",
				"   [ 1 3 ]",
				"so that the first letter of disagreement between T(1) and T(42) is 2, and that between T(41) and T(42) is also 2. (End)"
			],
			"mathematica": [
				"Nest[ Flatten[ # /. a_Integer -\u003e Range[a + 1]] \u0026, {1}, 6] (* _Robert G. Wilson v_, Jan 24 2006 *)"
			],
			"program": [
				"(PARI) a(n)=local(v,w); if(n\u003c1,0,v=[1]; while(#v\u003cn,w=[]; for(i=1,#v,w=concat(w,vector(v[i]+1,j,j))); v=w); v[n])"
			],
			"xref": [
				"Cf. A000245, A085182. a(n)=A076050(n)-1. Partial sums: A080336. Positions of ones: A085197. The first occurrence of each n is at A000108(n). See A085180."
			],
			"keyword": "easy,nonn,nice",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, _Mira Bernstein_, _James Propp_",
			"ext": [
				"More terms from Larry Reeves (larryr(AT)acm.org), Sep 22 2000"
			],
			"references": 15,
			"revision": 56,
			"time": "2021-02-21T14:13:10-05:00",
			"created": "1994-04-28T03:00:00-04:00"
		}
	]
}