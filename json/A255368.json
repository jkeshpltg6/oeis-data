{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A255368",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 255368,
			"data": "0,1,-2,2,-4,5,-4,7,-8,6,-10,11,-8,13,-14,10,-16,17,-12,19,-20,14,-22,23,-16,25,-26,18,-28,29,-20,31,-32,22,-34,35,-24,37,-38,26,-40,41,-28,43,-44,30,-46,47,-32,49,-50,34,-52,53,-36,55,-56,38,-58,59",
			"name": "a(n) = -(-1)^n * 2 * n / 3 if n divisible by 3, a(n) = -(-1)^n * n otherwise.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A255368/b255368.txt\"\u003eTable of n, a(n) for n = 0..2500\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"http://grail.eecs.csuohio.edu/~somos/rfmc.txt\"\u003eRational function multiplicative coefficients\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,0,-2,0,0,-1)."
			],
			"formula": [
				"Euler transform of length 6 sequence [-2, 1, -2, -1, 0, 2].",
				"a(n) is multiplicative with a(2^e) = -(2^e) if e\u003e0, a(3^e) = 2 * 3^(e-1) if e\u003e0, otherwise a(p^e) = p^e.",
				"G.f.: f(x) - f(x^3) where f(x) := x / (1 + x)^2.",
				"G.f.: x * (1 - x)^2 * (1 + x^2) / (1 + x^3)^2.",
				"G.f.: x * (1 - x)^2 * (1 - x^3)^2 * (1 - x^4) / ((1 - x^2) * (1 - x^6)^2).",
				"a(n) = -a(-n) = -(-1)^n * A186101(n) for all n in Z."
			],
			"example": [
				"G.f. = x - 2*x^2 + 2*x^3 - 4*x^4 + 5*x^5 - 4*x^6 + 7*x^7 - 8*x^8 + 6*x^9 + ..."
			],
			"mathematica": [
				"a[ n_] := -(-1)^n If[ Divisible[ n, 3], 2 n/3, n];",
				"a[ n_] := n {1, -1, 2/3, -1, 1, -2/3}[[Mod[n, 6, 1]]];",
				"CoefficientList[Series[x*(1-x)^2*(1+x^2)/(1+x^3)^2, {x,0,60}], x] (* _G. C. Greubel_, Aug 02 2018 *)"
			],
			"program": [
				"(PARI) {a(n) = -(-1)^n * if( n%3, n, 2*n/3)};",
				"(PARI) x='x+O('x^60); concat([0], Vec(x*(1-x)^2*(1+x^2)/(1+x^3)^2)) \\\\ _G. C. Greubel_, Aug 02 2018",
				"(MAGMA) m:=60; R\u003cx\u003e:=PowerSeriesRing(Integers(), m); [0] cat Coefficients(R!(x*(1-x)^2*(1+x^2)/(1+x^3)^2)); // _G. C. Greubel_, Aug 02 2018"
			],
			"xref": [
				"Cf. A186101."
			],
			"keyword": "sign,mult,easy",
			"offset": "0,3",
			"author": "_Michael Somos_, May 04 2015",
			"references": 1,
			"revision": 23,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2015-05-04T20:38:39-04:00"
		}
	]
}