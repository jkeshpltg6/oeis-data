{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A342910",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 342910,
			"data": "0,1,0,0,0,1,1,0,0,0,0,0,0,1,1,1,0,1,0,0,0,0,0,1,0,0,0,0,0,1,0,1,1,1,0,1,0,0,1,0,0,0,0,0,0,1,1,0,0,1,0,0,0,0,0,0,1,1,0,0,0,0,0,1,1,0,1,0,1,1,0,0,1,0,0,0,1,0,0,0,0,0,0,0,1,1",
			"name": "Concatenation of all 01-words, in the order induced by A032766; see Comments.",
			"comment": [
				"Let s = (s(n)) be a strictly increasing sequence of positive integers with infinite complement, t = (t(n)).",
				"For n \u003e= 1, let s'(n) be the number of s(i) that are \u003c= n-1 and let t'(n) be the number of t(i) that are \u003c= n-1.",
				"Define w(1) = 0, w(t(1)) = 1, and w(n) = 0w(s'(n)) if n is in s, and w(n) = 1w(t'(n)) if n is in t. Then (w(n)) is the \"s-induced ordering\" of all 01-words.",
				"s = A032766; t = A016789; s' = A004396; t' = A002264;",
				"In the following list, W represents the sequence of words w(n) induced by A032766. The list includes five partitions and a self-inverse permutation of the positive integers.",
				"length of w(n): A344150;",
				"positions in W of words w(n) such that # 0's = # 1's: A344151;",
				"positions in W of words w(n) such that # 0's \u003c # 1's: A344152;",
				"positions in W of words w(n) such that # 0's \u003e # 1's: A344153;",
				"positions in W of words w(n) that end with 0: A344154;",
				"positions in W of words w(n) that end with 1: A344155;",
				"positions in W of words w(n) such that first digit = last digit: A344156;",
				"positions in W of words w(n) such that first digit != last digit: A344157;",
				"positions in W of words w(n) such that 1st digit  = 0 and last digit 0: A344158;",
				"positions in W of words w(n) such that 1st digit  = 0 and last digit 1: A344159;",
				"positions in W of words w(n) such that 1st digit  = 1 and last digit 0: A344160;",
				"positions in W of words w(n) such that 1st digit  = 1 and last digit 1: A344161;",
				"position in W of n-th positive integer (base 2):  A344162;",
				"positions in W of binary complement of w(n):  A344163;",
				"sum of digits in w(n): A344164;",
				"number of runs in w(n): A344165;",
				"positions in W of palindromes: A344166;",
				"positions in W of words such that #0's - #1's is odd: A344167;",
				"positions in W of words such that #0's - #1's is even: A344168;",
				"positions in W of the reversal of the n-th word in W: A344169.",
				"For a guide to related sequences, see A341256."
			],
			"example": [
				"The first twenty words w(n): 0, 1, 00, 01, 10, 000, 001, 11, 010, 0000, 100, 0001, 011, 101, 0010, 00000, 110, 0100, 00001, 1000."
			],
			"mathematica": [
				"z = 250;",
				"\"The sequence s:\"  (* A001651, (3n/2) *)",
				"s = Table[Floor[3 n/2], {n, 1, z}]",
				"\"The sequence t:\" (* A016789; congr to 0 or 1 mod 3;  *)",
				"t = Complement[Range[Max[s]], s]",
				"s1[n_] := Length[Intersection[Range[n - 1], s]];",
				"t1[n_] := n - 1 - s1[n];",
				"\"The sequence s1:\"",
				"Table[s1[n], {n, 1, z}] (* A004396 *)",
				"\"The sequence t1:\"",
				"Table[t1[n], {n, 1, z}] (* A002264 *)",
				"w[1] = {0}; w[t[[1]]] = {1};",
				"w[n_] := If[MemberQ[s, n], Join[{0}, w[s1[n]]], Join[{1}, w[t1[n]]]]",
				"\"List tt of all binary words:\"",
				"tt = Table[w[n], {n, 1, z}] (*  all the binary words *)",
				"\"All the words, concatenated:\"",
				"Flatten[tt] (* words, concatenated, A344150 *)",
				"\"Positions of words in which #0's = #1's:\"  (* A344151 *)",
				"Select[Range[Length[tt]], Count[tt[[#]], 0] == Count[tt[[#]], 1] \u0026]",
				"\"Positions of words in which #0's \u003c #1's:\"  (* A344152 *)",
				"Select[Range[Length[tt]], Count[tt[[#]], 0] \u003c Count[tt[[#]], 1] \u0026]",
				"\"Positions of words in which #0's \u003e #1's:\"  (* A344153 *)",
				"Select[Range[Length[tt]], Count[tt[[#]], 0] \u003e Count[tt[[#]], 1] \u0026]",
				"\"Positions of words ending with 0:\" (* A344154 *)",
				"Select[Range[Length[tt]], Last[tt[[#]]] == 0 \u0026]",
				"\"Positions of words ending with 1:\" (* A344155 *)",
				"Select[Range[Length[tt]], Last[tt[[#]]] == 1 \u0026]",
				"\"Positions of words starting and ending with same digit:\" (* A344156 *)",
				"Select[Range[Length[tt]], First[tt[[#]]] == Last[tt[[#]]] \u0026]",
				"\"Positions of words starting and ending with opposite digits:\" (* A344157 *)",
				"Select[Range[Length[tt]], First[tt[[#]]] != Last[tt[[#]]] \u0026]",
				"\"Positions of words starting with 0 and ending with 0:\" (* A344158 *)",
				"Select[Range[Length[tt]], First[tt[[#]]] == 0 \u0026\u0026 Last[tt[[#]]] == 0 \u0026]",
				"\"Positions of words starting with 0 and ending with 1:\" (* A344159 *)",
				"Select[Range[Length[tt]], First[tt[[#]]] == 0 \u0026\u0026 Last[tt[[#]]] == 1 \u0026]",
				"\"Positions of words starting with 1 and ending with 0:\" (* A344160 *)",
				"Select[Range[Length[tt]], First[tt[[#]]] == 1 \u0026\u0026 Last[tt[[#]]] == 0 \u0026]",
				"\"Positions of words starting with 1 and ending with 1:\" (* A344161 *)",
				"Select[Range[Length[tt]], First[tt[[#]]] == 1 \u0026\u0026 Last[tt[[#]]] == 1 \u0026]",
				"\"Position of n-th positive integer (base 2) in tt:  A344162 \"",
				"d[n_] := If[First[w[n]] == 1, FromDigits[w[n], 2]];",
				"Flatten[Table[Position[Table[d[n], {n, 1, 200}], n], {n, 1, 200}]]",
				"\"Position of binary complement of w(n): A344163\"",
				"comp = Flatten[Table[Position[tt, 1 - w[n]], {n, 1, 50}]]",
				"\"Sum of digits of w(n):  A344164\"",
				"Table[Total[w[n]], {n, 1, 100}]",
				"\"Number of runs in w(n):  A344165\"",
				"Map[Length, Table[Map[Length, Split[w[n]]], {n, 1, 100}]]",
				"\"Palindromes:\"",
				"Select[tt, # == Reverse[#] \u0026]",
				"\"Positions of palindromes:  A344166\"",
				"Select[Range[Length[tt]], tt[[#]] == Reverse[tt[[#]]] \u0026]",
				"\"Positions of words in which #0's - #1's is odd:  A344167\"",
				"Select[Range[Length[tt]], OddQ[Count[w[#], 0] - Count[w[#], 1]] \u0026]",
				"\"Positions of words in which #0's - #1's is even: A344168\"",
				"Select[Range[Length[tt]], EvenQ[Count[w[#], 0] - Count[w[#], 1]] \u0026]",
				"\"Position of the reversal of the n-th word:  A344169\"",
				"Flatten[Table[Position[tt, Reverse[w[n]]], {n, 1, 150}]]"
			],
			"xref": [
				"Cf. A032766, A016789, A004396, A002264, A341256."
			],
			"keyword": "nonn,base,changed",
			"offset": "1",
			"author": "_Clark Kimberling_, May 11 2021",
			"references": 37,
			"revision": 18,
			"time": "2022-01-05T00:57:53-05:00",
			"created": "2021-05-26T02:43:29-04:00"
		}
	]
}