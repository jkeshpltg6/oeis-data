{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A342707",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 342707,
			"data": "0,1,0,0,1,0,1,1,1,0,1,2,2,1,0,2,1,3,3,1,0,1,2,4,4,4,1,0,2,2,5,27,5,5,1,0,0,3,6,28,256,6,6,1,0,1,1,7,30,257,3125,7,7,1,0,0,2,8,31,260,3126,46656,8,8,1,0,1,2,9,81,261,3130,46657,823543,9,9,1,0",
			"name": "T(n, k) is the result of replacing 2's by k's in the hereditary base-2 expansion of n; square array T(n, k) read by antidiagonals upwards, n, k \u003e= 0.",
			"link": [
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Goodstein\u0026#39;s_theorem#Hereditary_base-n_notation\"\u003eHereditary base-n notation\u003c/a\u003e"
			],
			"formula": [
				"T(n, n) = A343255(n).",
				"T(n, 0) = A345021(n).",
				"T(n, 1) = A000120(n).",
				"T(n, 2) = n.",
				"T(n, 3) = A222112(n-1).",
				"T(0, k) = 0.",
				"T(1, k) = 1.",
				"T(2, k) = k.",
				"T(3, k) = k + 1.",
				"T(4, k) = k^k = A000312(k).",
				"T(5, k) = k^k + 1 = A014566(k).",
				"T(6, k) = k^k + k = A066068(k).",
				"T(7, k) = k^k + k + 1 = A066279(k).",
				"T(16, k) = k^k^k = A002488(k).",
				"T(m + n, k) = T(m, k) + T(n, k) when m AND n = 0 (where AND denotes the bitwise AND operator)."
			],
			"example": [
				"Array T(n, k) begins:",
				"  n\\k|  0  1   2   3     4      5       6        7          8           9",
				"  ---+-------------------------------------------------------------------",
				"    0|  0  0   0   0     0      0       0        0          0           0",
				"    1|  1  1   1   1     1      1       1        1          1           1",
				"    2|  0  1   2   3     4      5       6        7          8           9",
				"    3|  1  2   3   4     5      6       7        8          9          10",
				"    4|  1  1   4  27   256   3125   46656   823543   16777216   387420489",
				"    5|  2  2   5  28   257   3126   46657   823544   16777217   387420490",
				"    6|  1  2   6  30   260   3130   46662   823550   16777224   387420498",
				"    7|  2  3   7  31   261   3131   46663   823551   16777225   387420499",
				"    8|  0  1   8  81  1024  15625  279936  5764801  134217728  3486784401",
				"    9|  1  2   9  82  1025  15626  279937  5764802  134217729  3486784402",
				"   10|  0  2  10  84  1028  15630  279942  5764808  134217736  3486784410"
			],
			"program": [
				"(PARI) T(n,k) = { my (v=0, e); while (n, n-=2^e=valuation(n,2); v+=k^T(e,k)); v }"
			],
			"xref": [
				"See A341907 for a similar sequence.",
				"Cf. A000120, A000312, A002488, A014566, A066068, A066279, A222112, A343255, A345021."
			],
			"keyword": "nonn,tabl,base",
			"offset": "0,12",
			"author": "_Rémy Sigrist_, Jun 04 2021",
			"references": 3,
			"revision": 12,
			"time": "2021-06-07T14:54:46-04:00",
			"created": "2021-06-07T14:54:46-04:00"
		}
	]
}