{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003442",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3442,
			"id": "M2002",
			"data": "1,2,11,48,208,858,3507,14144,56698,226100,898942,3565920,14124496,55887930,220985795,873396480,3450940830,13633173180,53855628554,212750148000,840496068160,3320817060132,13122294166126,51860761615488",
			"name": "Number of nonequivalent dissections of an n-gon into (n-3) polygons by nonintersecting diagonals rooted at a cell up to rotation.",
			"comment": [
				"Number of dissections of regular n-gon into n-3 polygons without reflection and rooted at a cell. - _Sean A. Irvine_, May 05 2015",
				"The conditions imposed mean that the dissection will always be composed of one quadrilateral and n-4 triangles. - _Andrew Howroyd_, Nov 23 2017"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A003442/b003442.txt\"\u003eTable of n, a(n) for n = 4..200\u003c/a\u003e",
				"P. Lisonek, \u003ca href=\"http://dx.doi.org/10.1006/jsco.1995.1066\"\u003eClosed forms for the number of polygon dissections\u003c/a\u003e, Journal of Symbolic Computation 20 (1995), 595-601.",
				"Ronald C. Read, \u003ca href=\"http://dx.doi.org/10.1007/BF03031688\"\u003eOn general dissections of a polygon\u003c/a\u003e, Aequat. math. 18 (1978) 370-388.",
				"Andrey Zabolotskiy, \u003ca href=\"/A003442/a003442.png\"\u003eIllustration for n = 4,5,6\u003c/a\u003e"
			],
			"example": [
				"Case n=5: A pentagon can be dissected into 1 quadrilateral and 1 triangle. Either one of these can be chosen as the root cell so a(n)=2. - _Andrew Howroyd_, Nov 23 2017"
			],
			"program": [
				"(PARI)",
				"DissectionsModCyclicRooted(v)={my(n=#v);",
				"my(q=vector(n)); q[1]=serreverse(x-sum(i=3,#v,x^i*v[i])/x + O(x*x^n));",
				"for(i=2, n, q[i]=q[i-1]*q[1]);",
				"my(vars=variables(q[1]));",
				"my(u(m,r)=substvec(q[r]+O(x^(n\\m+1)), vars, apply(t-\u003et^m,vars)));",
				"my(p=O(x*x^n) + sum(i=3, #v, my(c=v[i]); if(c, c*sumdiv(i, d, eulerphi(d)*u(d,i/d))/i)));",
				"vector(n,i,polcoeff(p,i))}",
				"{ my(v=DissectionsModCyclicRooted(apply(i-\u003eif(i\u003e=3\u0026\u0026i\u003c=4,y^(i-3) + O(y^2)), [1..25]))); apply(p-\u003epolcoeff(p,1), v[4..#v]) } \\\\ _Andrew Howroyd_, Nov 22 2017"
			],
			"xref": [
				"Cf. A003443, A003454, A220881, A295622."
			],
			"keyword": "nonn",
			"offset": "4,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Sean A. Irvine_, May 05 2015",
				"Name clarified by _Andrew Howroyd_, Nov 22 2017"
			],
			"references": 5,
			"revision": 39,
			"time": "2021-01-19T11:48:00-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}