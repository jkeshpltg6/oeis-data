{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A245564",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 245564,
			"data": "1,2,2,3,2,4,3,5,2,4,4,6,3,6,5,8,2,4,4,6,4,8,6,10,3,6,6,9,5,10,8,13,2,4,4,6,4,8,6,10,4,8,8,12,6,12,10,16,3,6,6,9,6,12,9,15,5,10,10,15,8,16,13,21,2,4,4,6,4,8,6,10,4,8,8,12,6,12,10,16,4,8,8,12,8,16,12,20,6,12,12,18",
			"name": "a(n) = Product_{i in row n of A245562} Fibonacci(i+2).",
			"comment": [
				"This is the Run Length Transform of S(n) = Fibonacci(n+2).",
				"The Run Length Transform of a sequence {S(n), n\u003e=0} is defined to be the sequence {T(n), n\u003e=0} given by T(n) = Product_i S(i), where i runs through the lengths of runs of 1's in the binary expansion of n. E.g. 19 is 10011 in binary, which has two runs of 1's, of lengths 1 and 2. So T(19) = S(1)*S(2). T(0)=1 (the empty product).",
				"a(n) = Sum_{k=0..n} ({binomial(3k,k)*binomial(n,k)} mod 2). - _Chai Wah Wu_, Oct 19 2016"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A245564/b245564.txt\"\u003eTable of n, a(n) for n = 0..8191\u003c/a\u003e",
				"Chai Wah Wu, \u003ca href=\"https://arxiv.org/abs/1610.06166\"\u003eSums of products of binomial coefficients mod 2 and run length transforms of sequences\u003c/a\u003e, arXiv:1610.06166 [math.CO], 2016."
			],
			"maple": [
				"with(combinat); ans:=[];",
				"for n from 0 to 100 do lis:=[]; t1:=convert(n,base,2); L1:=nops(t1); out1:=1; c:=0;",
				"for i from 1 to L1 do",
				"   if out1 = 1 and t1[i] = 1 then out1:=0; c:=c+1;",
				"   elif out1 = 0 and t1[i] = 1 then c:=c+1;",
				"   elif out1 = 1 and t1[i] = 0 then c:=c;",
				"   elif out1 = 0 and t1[i] = 0 then lis:=[c,op(lis)]; out1:=1; c:=0;",
				"   fi;",
				"   if i = L1 and c\u003e0 then lis:=[c,op(lis)]; fi;",
				"                   od:",
				"a:=mul(fibonacci(i+2), i in lis);",
				"ans:=[op(ans),a];",
				"od:",
				"ans;"
			],
			"mathematica": [
				"a[n_] := Sum[Mod[Binomial[3k, k] Binomial[n, k], 2], {k, 0, n}];",
				"a /@ Range[0, 100] (* _Jean-François Alcover_, Feb 29 2020, after _Chai Wah Wu_ *)"
			],
			"program": [
				"(PARI) a(n)=my(s=1,k); while(n, n\u003e\u003e=valuation(n,2); k=valuation(n+1,2); s*=fibonacci(k+2); n\u003e\u003e=k); s \\\\ _Charles R Greathouse IV_, Oct 21 2016"
			],
			"xref": [
				"Cf. A245562, A000045, A001045, A071053, A245565, A246028."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Aug 10 2014; revised Sep 05 2014",
			"references": 3,
			"revision": 33,
			"time": "2020-02-29T03:36:22-05:00",
			"created": "2014-08-10T14:02:40-04:00"
		}
	]
}