{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A265285",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 265285,
			"data": "46657,2433601,67371265,351596817937,422240040001,18677955240001,458631349862401,286245437364810001",
			"name": "Carmichael numbers (A002997) n such that n-1 is a square.",
			"comment": [
				"This sequence contains all Carmichael numbers n such that for all primes p dividing n, p-1 divides n-1 and furthermore, n-1 is a square.",
				"a(9) \u003e 2^64.",
				"a(9) \u003c= 20717489165917230086401. - _Daniel Suteu_, Dec 25 2020"
			],
			"link": [
				"G. Tarry, I. Franel, A. Korselt, and G. Vacca, \u003ca href=\"https://oeis.org/wiki/File:Probl%C3%A8me_chinois.pdf\"\u003eProblème chinois\u003c/a\u003e, L'intermédiaire des mathématiciens 6 (1899), pp. 142-144.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CarmichaelNumber.html\"\u003eCarmichael Number\u003c/a\u003e",
				"\u003ca href=\"/index/Ca#Carmichael\"\u003eIndex entries for sequences related to Carmichael numbers\u003c/a\u003e"
			],
			"example": [
				"46657 is a term because 46657 - 1 = 46656 = 216^2.",
				"2433601 is a term because 2433601 - 1 = 2433600 = 1560^2."
			],
			"maple": [
				"isA002997:= proc(n) local F,p;",
				"         if n::even or isprime(n)  then return false fi;",
				"         F:= ifactors(n)[2];",
				"         if max(seq(f[2],f=F)) \u003e 1 then return false fi;",
				"         andmap(f -\u003e (n-1) mod (f[1]-1) = 0,  F)",
				"end proc:",
				"select(isA002997, [seq(4*i^2+1,i=1..10^6)]); # _Robert Israel_, Dec 08 2015"
			],
			"program": [
				"(PARI) is_c(n) = { my(f); bittest(n, 0) \u0026\u0026 !for(i=1, #f=factor(n)~, (f[2, i]==1 \u0026\u0026 n%(f[1, i]-1)==1)||return) \u0026\u0026 #f\u003e1 }",
				"for(n=1, 1e10, if(is_c(n) \u0026\u0026 issquare(n-1), print1(n, \", \")))"
			],
			"xref": [
				"Subsequence of A265237 and of A265328.",
				"Cf. A002997, A265237, A303791."
			],
			"keyword": "nonn,hard,more",
			"offset": "1,1",
			"author": "_Altug Alkan_, Dec 06 2015",
			"ext": [
				"a(4)-a(5), using A002997 b-file, from _Michel Marcus_, Dec 07 2015",
				"a(6) and a(7) from _Robert Israel_, Dec 08 2015",
				"a(8) from _Max Alekseyev_, Apr 30 2018"
			],
			"references": 2,
			"revision": 37,
			"time": "2020-12-25T15:58:56-05:00",
			"created": "2015-12-20T14:01:14-05:00"
		}
	]
}