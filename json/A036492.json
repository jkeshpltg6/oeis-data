{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A036492",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 36492,
			"data": "4,5,6,24,19,47,39,61,116,99,124,194,149,243,369,292,479,599,600,474,1174,721,974,929,1524,2301,1909,2899,2474,2987,2294,3099,5682,4849,4714,3724,6074,7376,9224,9504,7299,14031,11974,14974,11905,18079,14999,11849,14306,23469,29349,18024,24349",
			"name": "Offsets for the Atkin Partition Congruence theorem.",
			"comment": [
				"The Atkin Theorem, inspired by a famous conjecture of Ramanujan, gives congruences properties of certain partition numbers, generalizing many previous results.",
				"Let T = 5^a*7^b*11^c (A036490) and Q = 5^a*7^(floor[(b+2)/2])*11^c (A036491).",
				"If 24*g = 1 mod T, then p(g) = 0 mod Q, where p(g) is the number of integer partitions of g.",
				"In fact, for k \u003e= 0, p(g + k*T) = 0 mod Q, since 24*(g + k*T) = 24*g = 1 mod T.",
				"A036492(n) lists the smallest g for basis T = A036490(n) and modulus Q = A036491(n).",
				"The first case using the full force of the theorem is for n = 46 corresponding to T = 5*7^3*11 = 18865, giving Q = 2695 and g = 18079."
			],
			"reference": [
				"G. E. Andrews, The Theory of Partitions, Addison-Wesley, 1976, pp. 159-161.",
				"G. H. Hardy, P. V. Seshu Aiyar and B. M. Wilson, Collected Papers of S. Ramanujan, CUP, 1927, #25 (1919), pp. 210-213, and #28 (1919), p. 230."
			],
			"link": [
				"A. O. L. Atkin, \u003ca href=\"https://doi.org/10.1017/S0017089500000045\"\u003eProof of a conjecture of Ramanujan\u003c/a\u003e, Glasgow Math. J., 8 (1967), 14-32."
			],
			"formula": [
				"24 * a(n) == 1 (mod A036490(n)). - _Sean A. Irvine_, Nov 04 2020"
			],
			"mathematica": [
				"Map[Function[df, First@Select[Range[3, df], Mod[24 #, df] == 1 \u0026, 1]],  Select[Range[40000], DeleteCases[FactorInteger[#], {5|7|11, _}] == {} \u0026]] (* From _Olivier Gérard_, Nov 12 2016 *)"
			],
			"xref": [
				"Cf. A000041, A036490, A036491."
			],
			"keyword": "nonn,easy,nice",
			"offset": "1,1",
			"author": "_Olivier Gérard_",
			"ext": [
				"Offset corrected by _Reinhard Zumkeller_, Feb 19 2013"
			],
			"references": 4,
			"revision": 23,
			"time": "2020-11-05T09:40:35-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}