{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A219360",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 219360,
			"data": "1,2,3,4,5,8,6,10,7,12,9,11,18,16,13,22,15,14,17,23,19,20,34,21,24,25,26,33,27,40,32,42,29,28,30,46,31,37,35,39,36,38,41,43,45,44,47,48,65,49,52,50,54,51,53,66,72,55,56,57,73,68,63,58,59,61,60,82",
			"name": "Starting from a(1)=1, for any n the sum of the next a(n) numbers is a prime. No repetition of numbers is allowed. At any step the minimum integer not yet used and not leading to a contradiction is chosen.",
			"comment": [
				"Permutation of natural numbers."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A219360/b219360.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (terms 1..133 from Paolo P. Lava)",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"example": [
				"a(1)=1 -\u003e next term is 2, prime.",
				"a(2)=2 -\u003e the sum of the next two terms 3 + 4 = 7, prime.",
				"a(3)=3 -\u003e 4 + 5 + 8 = 17, prime.",
				"a(4)=4 -\u003e 5 + 8 + 6 + 10 = 29, prime.",
				"a(5)=5 -\u003e 8 + 6 + 10 + 7 + 12 = 43, prime.",
				"a(6)=8 but we also have a(7)=6 that must be covered before a(6).",
				"Therefore the sequence became: 1, 2, 3, 4, 5, 8, 6, 10, 7, 12, 9, 11, 18, ... with 10 + 7 + 12 + 9 + 11 + 18 = 67, prime.",
				"Then coming back to a(6)=8:  1, 2, 3, 4, 5, 8, 6, 10, 7, 12, 9, 11, 18, 16, ... with 6 + 10 + 7 + 12 + 9 + 11 + 18 + 16 = 89.",
				"It could happen that two or more sums must be satisfied at the same step. If it is not possible we must change the most recent entries. For example, the sequence up to a(30) is: 1, 2, 3, 4, 5, 8, 6, 10, 7, 12, 9, 11, 18, 16, 13, 22, 14, 15, 17, 23, 19, 20, 34, 21, 24, 25, 26, 33, 27, 40.",
				"Now a(13)=18 and a(17)=14 must be satisfied in a(31) but 16 + 13 + 22 + 14 + 15 + 17 + 23 + 19 + 20 + 34 + 21 + 24 + 25 + 26 + 33 + 27 + 40 = 389 (odd) and 15 + 17 + 23 + 19 + 20 + 34 + 21 + 24 + 25 + 26 + 33 + 27 + 40 = 324 (even) and no integer a(31) can satisfy the system 389 + a(31) = p1 and 324 + a(31) = p2, with p1 and p2 both prime. Therefore we must change a(17)=14 into a new minimum value, in this case a(17)=15, and restart the sequence from that point."
			],
			"program": [
				"(PARI)",
				"PTest(t,u)={for(i=1, #u, if(!isprime(t-u[i]), return(0))); 1}",
				"XTest(u)={forprime(p=2, #u, if(#Set(u%p) \u003e= p, return(0))); 1}",
				"seq(n)={",
				"  my(v=vector(n), f=vector(2*n), M=Map(), p=1, t=0);",
				"  for(k=1, #v, my(S, X);",
				"     while(f[p], p++);",
				"     if(mapisdefined(M, k, \u0026S), mapdelete(M,k), S=[]);",
				"     for(q=p, oo, if(!f[q] \u0026\u0026 PTest(t+q, S),",
				"        X = if(mapisdefined(M, q+k, \u0026X), concat(X,[t+q]), [t+q]);",
				"        if(XTest(X), mapput(M, q+k, X); t += q; f[q]=1; v[k]=q; break);",
				"  )));",
				"  v",
				"} \\\\ _Andrew Howroyd_, Jun 05 2021"
			],
			"xref": [
				"Cf. A000040, A171007."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Paolo P. Lava_, Nov 19 2012",
			"references": 2,
			"revision": 50,
			"time": "2021-06-06T03:33:03-04:00",
			"created": "2012-12-05T03:20:44-05:00"
		}
	]
}