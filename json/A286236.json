{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A286236",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 286236,
			"data": "1,1,2,3,0,4,3,0,2,7,10,0,0,0,11,3,0,0,5,4,16,21,0,0,0,0,0,22,10,0,0,0,5,0,7,29,21,0,0,0,0,0,8,0,37,10,0,0,0,0,14,0,0,11,46,55,0,0,0,0,0,0,0,0,0,56,10,0,0,0,0,0,5,0,8,12,16,67,78,0,0,0,0,0,0,0,0,0,0,0,79,21,0,0,0,0,0,0,27,0,0,0,0,22,92,36,0,0,0,0,0,0,0,0,0,19,0,17,0,106",
			"name": "Square array A(n,k) = P(A000010(k), (n+k-1)/k) if k divides (n+k-1), 0 otherwise, read by descending antidiagonals as A(1,1), A(1,2), A(2,1), etc. Here P is a two-argument form of sequence A000027 used as a pairing function N x N -\u003e N.",
			"comment": [
				"This is transpose of A286237, see comments there."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A286236/b286236.txt\"\u003eTable of n, a(n) for n = 1..10585; the first 145 rows of triangle/antidiagonals of array\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = A113998(n,k) * A286234(n,k)."
			],
			"example": [
				"The top left 12 X 12 corner of the array:",
				"   1,  1,  3,  3, 10, 3, 21, 10, 21, 10, 55, 10",
				"   2,  0,  0,  0,  0, 0,  0,  0,  0,  0,  0,  0",
				"   4,  2,  0,  0,  0, 0,  0,  0,  0,  0,  0,  0",
				"   7,  0,  5,  0,  0, 0,  0,  0,  0,  0,  0,  0",
				"  11,  4,  0,  5,  0, 0,  0,  0,  0,  0,  0,  0",
				"  16,  0,  0,  0, 14, 0,  0,  0,  0,  0,  0,  0",
				"  22,  7,  8,  0,  0, 5,  0,  0,  0,  0,  0,  0",
				"  29,  0,  0,  0,  0, 0, 27,  0,  0,  0,  0,  0",
				"  37, 11,  0,  8,  0, 0,  0, 14,  0,  0,  0,  0",
				"  46,  0, 12,  0,  0, 0,  0,  0, 27,  0,  0,  0",
				"  56, 16,  0,  0, 19, 0,  0,  0,  0, 14,  0,  0",
				"  67,  0,  0,  0,  0, 0,  0,  0,  0,  0, 65,  0",
				"The first 15 rows when viewed as a triangle:",
				"   1,",
				"   1, 2,",
				"   3, 0, 4,",
				"   3, 0, 2, 7,",
				"  10, 0, 0, 0, 11,",
				"   3, 0, 0, 5,  4, 16,",
				"  21, 0, 0, 0,  0,  0, 22,",
				"  10, 0, 0, 0,  5,  0,  7, 29,",
				"  21, 0, 0, 0,  0,  0,  8,  0, 37,",
				"  10, 0, 0, 0,  0, 14,  0,  0, 11, 46,",
				"  55, 0, 0, 0,  0,  0,  0,  0,  0,  0, 56,",
				"  10, 0, 0, 0,  0,  0,  5,  0,  8, 12, 16, 67,",
				"  78, 0, 0, 0,  0,  0,  0,  0,  0,  0,  0,  0, 79,",
				"  21, 0, 0, 0,  0,  0,  0, 27,  0,  0,  0,  0, 22, 92,",
				"  36, 0, 0, 0,  0,  0,  0,  0,  0,  0, 19,  0, 17,  0, 106"
			],
			"program": [
				"(Scheme)",
				"(define (A286236 n) (A286236bi (A002260 n) (A004736 n)))",
				"(define (A286236bi row col) (if (not (zero? (modulo (+ row col -1) col))) 0 (let ((a (A000010 col)) (b (/ (+ row col -1) col))) (* (/ 1 2) (+ (expt (+ a b) 2) (- a) (- (* 3 b)) 2)))))",
				";; Alternatively, with triangular indexing:",
				"(define (A286236 n) (A286236tr (A002024 n) (A002260 n)))",
				"(define (A286236tr n k) (A286236bi k (+ 1 (- n k))))",
				"(Python)",
				"from sympy import totient",
				"def T(n, m): return ((n + m)**2 - n - 3*m + 2)/2",
				"def t(n, k): return 0 if n%k!=0 else T(totient(k), n/k)",
				"for n in range(1, 21): print [t(n, k) for k in range(1, n + 1)][::-1] # _Indranil Ghosh_, May 10 2017"
			],
			"xref": [
				"Transpose: A286237.",
				"Cf. A000010, A000027, A113998, A286156, A286234, A286246."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Antti Karttunen_, May 05 2017",
			"references": 4,
			"revision": 25,
			"time": "2019-12-07T12:18:29-05:00",
			"created": "2017-05-06T19:50:52-04:00"
		}
	]
}