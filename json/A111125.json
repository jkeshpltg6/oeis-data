{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A111125",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 111125,
			"data": "1,3,1,5,5,1,7,14,7,1,9,30,27,9,1,11,55,77,44,11,1,13,91,182,156,65,13,1,15,140,378,450,275,90,15,1,17,204,714,1122,935,442,119,17,1,19,285,1254,2508,2717,1729,665,152,19,1,21,385,2079,5148,7007,5733,2940,952",
			"name": "Triangle read by rows: T(k,s) = ((2*k+1)/(2*s+1))*binomial(k+s,2*s), 0 \u003c= s \u003c= k.",
			"comment": [
				"Riordan array ((1+x)/(1-x)^2,x/(1-x)^2). Row sums are A002878. Diagonal sums are A003945. Inverse is A113187. An interesting factorization is (1/(1-x),x/(1-x))(1+2x,x(1+x)). - _Paul Barry_, Oct 17 2005",
				"Central coefficients of rows with odd numbers of term are A052227.",
				"From _Wolfdieter Lang_, Jun 26 2011: (Start)",
				"T(k,s) appears as T_s(k) in the Knuth reference, p. 285.",
				"This triangle is related to triangle A156308(n,m), appearing in this reference as U_m(n) on p. 285, by T(k,s)-T(k-1,s) = A156308(k,s), k\u003e=s\u003e=1 (identity on p. 286).",
				"  T(k,s) = A156308(k+1,s+1) - A156308(k,s+1), k\u003e=s\u003e=0 (identity on p. 286).",
				"(End)",
				"A111125 is jointly generated with A208513 as an array of coefficients of polynomials v(n,x): initially, u(1,x)=v(1,x)=1; for n\u003e1, u(n,x)=u(n-1,x)+(x+1)*v(n-1)x and v(n,x)=u(n-1,x)+x*v(n-1,x)+1. See the Mathematica section. The columns of A111125 are identical to those of A208508. Here, however, the alternating row sums are periodic (with period 1,2,1,-1,-2,-1). - _Clark Kimberling_, Feb 28 2012",
				"This triangle T(k,s) (with signs and columns scaled with powers of 5) appears in the expansion of Fibonacci numbers F=A000045 with multiples of odd numbers as indices in terms of odd powers of F-numbers. See the Jennings reference, p. 108, Theorem 1. Quoted as Lemma 3 in the Ozeki reference given in A111418. The formula is: F_{(2*k+1)*n} = sum(T(k,s)*(-1)^((k+s)*n)*5^s*F_{n}^(2*s+1),s=0..k), k \u003e= 0, n \u003e= 0. - _Wolfdieter Lang_, Aug 24 2012",
				"From _Wolfdieter Lang_, Oct 18 2012: (Start)",
				"This triangle T(k,s) appears in the formula x^(2*k+1) - x^(-(2*k+1)) = sum(T(k,s)*(x-x^(-1))^(2*s+1),s=0..k), k\u003e=0. Prove the inverse formula (due to the Riordan property this will suffice) with the binomial theorem. Motivated to look into this by the quoted paper of Wang and Zhang, eq. (1.4).",
				"Alternating row sums are A057079.",
				"The Z-sequence of this Riordan array is A217477, and the A-sequence is (-1)^n*A115141(n). For the notion of A- and Z-sequences for Riordan triangles see a W. Lang link under A006232. (End)",
				"The signed triangle ((-1)^(k-s))*T(k,s) gives the coefficients of (x^2)^s of the polynomials C(2*k+1,x)/x, with C the monic integer Chebyshev T-polynomials whose coefficients are given in A127672 (C is there called R). See the odd numbered rows there. This signed triangle is the Riordan array ((1-x)/(1+x)^2, x/(1+x)^2). Proof by comparing the o.g.f. of the row polynomials where x is replaced by x^2 with the odd part of the bisection of the o.g.f. for C(n,x)/x. - _Wolfdieter Lang_, Oct 23 2012",
				"From _Wolfdieter Lang_, Oct 04 2013: (Start)",
				"The signed triangle S(k,s) := ((-1)^(k-s))*T(k,s) (see the preceding comment) is used to express in a (4*(k+1))-gon the length ratio s(4*(k+1)) = 2*sin(Pi/4*(k+1)) =  2*cos((2*k+1)*Pi/(4*(k+1))) of a side/radius as a polynomial in rho(4*(k+1)) = 2*cos(Pi/4*(k+1)), the length ratio (smallest diagonal)/side:",
				"  s(4*(k+1)) = sum(S(k,s)*rho(4*(k+1))^(2*s+1), s=0..k).",
				"This is to be computed modulo C(4*(k+1),rho(4*(k+1)) = 0, the minimal polynomial (see A187360) in order to obtain  s(4*(k+1)) as an integer in the algebraic number field Q(rho(4*(k+1))) of degree delta(4*(k+1)) (see A055034). Thanks go to Seppo Mustonen for asking me to look into the problem of the square of the total length in a regular n-gon, where this formula is used in the even n case. See A127677 for the formula in the (4*k+2)-gon. (End)",
				"From _Wolfdieter Lang_, Aug 14 2014: (Start)",
				"The row polynomials for the signed triangle (see the Oct 23 2012 comment above), call them todd(k,x) = sum(((-1)^(k-s))*T(k,s)*x^s) = S(k, x-2) - S(k-1, x-2), k \u003e= 0, with the Chebyshev S-polynomials (see their coefficient triangle (A049310) and S(-1, x) = 0), satisfy the recurrence todd(k, x) = (-1)^(k-1)*((x-4)/2)*todd(k-1, 4-x) + ((x-2)/2)*todd(k-1, x), k \u003e= 1, todd(0, x) = 1. From the Aug 03 2014 comment on A130777.",
				"This leads to a recurrence for the signed triangle, call it S(k,s) as in the Oct 04 2013 comment: S(k,s) = (1/2)*(1 + (-1)^(k-s))*S(k-1,s-1) + (2*(s+1)*(-1)^(k-s) - 1)*S(k-1,s) + (1/2)*(-1)^(k-s)*sum(binomial(j+s+2,s)*4^(j+2)* S(k-1,s+1+j), j=0..k-s-2) for k \u003e= s \u003e= 1, and S(k,s) = 0 if k \u003c s and S(k,0) = (-1)^k*(2*k+1). Note that the recurrence derived from the Riordan A-sequence A115141 is similar but has simpler coefficients: S(k,s) = sum(A115141(j)*S(k-1,s-1+j), j=0..k-s), k \u003e= s \u003e=1.",
				"(End)",
				"From _Tom Copeland_, Nov 07 2015: (Start)",
				"Rephrasing notes here: Append an initial column of zeros, except for a 1 at the top, to A111125 here. Then the partial sums of the columns of this modified entry are contained in A208513. Append an initial row of zeros to A208513. Then the difference of consecutive pairs of rows of the modified A208513 generates the modified A111125. Cf. A034807 and A127677.",
				"For relations among the characteristic polynomials of Cartan matrices of the Coxeter root groups, Chebyshev polynomials, cyclotomic polynomials, and the polynomials of this entry, see Damianou (p. 20 and 21) and Damianou and Evripidou (p. 7).",
				"As suggested by the equations on p. 7 of Damainou and Evripidou, the signed row polynomials of this entry are given by (p(n,x))^2 = [A(2n+1,x)+2]/x = [F(2n+1,(2-x),1,0,0,..)+2]/x = F(2n+1,-x,2x,-3x,..,(-1)^n n*x)/x=  -F(2n+1,x,2x,3x,..,n*x)/x, where A(n,x) are the polynomials of A127677 and F(n,..) are the Faber polynomials of A263196. Cf. A127672 and A127677.",
				"(End)",
				"The row polynomials P(k, x) of the signed triangle S(k, s) = ((-1)^(k-s))*T(k, s) are given from the row polynomials R(2*k+1, x) of triangle A127672 by",
				"P(k, x) =  R(2*k+1, sqrt(x))/sqrt(x). - _Wolfdieter Lang_, May 02 2021"
			],
			"link": [
				"R. Andre-Jeannin, \u003ca href=\"http://www.fq.math.ca/Scanned/32-3/andre-jeannin.pdf\"\u003eA generalization of Morgan-Voyce polynomials\u003c/a\u003e, The Fibonacci Quarterly 32.3 (1994): 228-31.",
				"Alexander Burstein and Louis W. Shapiro, \u003ca href=\"https://arxiv.org/abs/2112.11595\"\u003ePseudo-involutions in the Riordan group\u003c/a\u003e, arXiv:2112.11595 [math.CO], 2021.",
				"K. Dilcher and K. B. Stolarsky, \u003ca href=\"http://www.maa.org/sites/default/files/pdf/upload_library/22/Ford/dilcher673.pdf\"\u003eA Pascal-type triangle characterizing twin primes\u003c/a\u003e, Amer. Math. Monthly, 112 (2005), 673-681.",
				"P. Damianou , \u003ca href=\"http://arxiv.org/abs/1110.6620\"\u003eOn the characteristic polynomials of Cartan matrices and Chebyshev polynomials\u003c/a\u003e, arXiv preprint arXiv:1110.6620 [math.RT], 2014.",
				"P. Damianou and C. Evripidou, \u003ca href=\"http://arxiv.org/abs/1409.3956\"\u003eCharacteristic and Coxeter polynomials for affine Lie algebras\u003c/a\u003e, arXiv preprint arXiv:1409.3956 [math.RT], 2014.",
				"D. Jennings, \u003ca href=\"http://www.fq.math.ca/Scanned/31-2/jennings.pdf\"\u003eSome Polynomial Identities for the Fibonacci and Lucas Numbers\u003c/a\u003e, Fib. Quart., 31(2) (1993), 134-137.",
				"D. E. Knuth, \u003ca href=\"http://arxiv.org/abs/math/9207222//\"\u003eJohann Faulhaber and sums of powers\u003c/a\u003e, Math. Comp. 61 (1993), no. 203, 277-294.",
				"Yidong Sun, \u003ca href=\"http://www.fq.math.ca/Papers1/43-4/paper43-4-10b.pdf\"\u003eNumerical triangles and several classical sequences\u003c/a\u003e, Fib. Quart., Nov. 2005, pp. 359-370.",
				"T. Wang and W. Zhang, \u003ca href=\"http://rms.unibuc.ro/bulletin/volumes/55-1/node9.html\"\u003e Some identities involving Fibonacci, Lucas polynomials and their applications\u003c/a\u003e, Bull. Math. Soc. Sci. Math. Roumanie, Tome 55(103), No.1, (2012) 95-103.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Morgan-VoycePolynomials.html\"\u003eMorgan-Voyce polynomials\u003c/a\u003e"
			],
			"formula": [
				"T(k,s) = ((2*k+1)/(2*s+1))*binomial(k+s,2*s), 0 \u003c= s \u003c= k.",
				"From _Peter Bala_, Apr 30 2012: (Start)",
				"T(n,k) = binomial(n+k,2*k) + 2*binomial(n+k,2*k+1).",
				"The row generating polynomials P(n,x) are a generalization of the Morgan-Voyce polynomials b(n,x) and B(n,x). They satisfy the recurrence equation P(n,x) = (x+2)*P(n-1,x) - P(n-2,x) for n \u003e= 2, with initial conditions P(0,x) = 1, P(1,x) = x+r+1 and with r = 2. The cases r = 0 and r = 1 give the Morgan-Voyce polynomials A085478 and A078812 respectively. Andre-Jeannin has considered the case of general r.",
				"P(n,x) = U(n+1,1+x/2) + U(n,1+x/2), where U(n,x) denotes the Chebyshev polynomial of the second kind - see A053117. P(n,x) = 2/x*{T(2*n+2,u)-T(2*n,u)), where u = sqrt((x+4)/4) and T(n,x) denotes the Chebyshev polynomial of the first kind - see A053120. P(n,x) = product {k = 1..n} ( x + 4*(sin(k*Pi/(2*n+1)))^2 ). P(n,x) = 1/x*(b(n+1,x) - b(n-1,x)) and P(n,x) = 1/x*{(b(2*n+2,x)+1)/b(n+1,x) - (b(2*n,x)+1)/b(n,x)}, where b(n,x) := sum {k = 0..n} binomial(n+k,2*k)*x^k are the Morgan-Voyce polynomials of A085478. Cf. A211957.",
				"(End)",
				"From _Wolfdieter Lang_, Oct 18, 2012 (Start)",
				"O.g.f. column No. s: ((1+x)/(1-x)^2)*(x/(1-x)^2)^s, s \u003e= 0. (from the Riordan data given in a comment above).",
				"O.g.f. of the row polynomials R(k,x):=sum(T(k,s)*x^s,s=0..k), k\u003e=0: (1+z)/(1-(2+x)*z+z^2) (from the Riordan property).",
				"(End)",
				"T(n,k) = 2*T(n-1,k) + T(n-1,k-1) - T(n-2,k), T(0,0) = 1, T(1,0) = 3, T(1,1) = 1, T(n,k) = 0 if k\u003c0 or if k\u003en. - _Philippe Deléham_, Nov 12 2013"
			],
			"example": [
				"Triangle T(k,s) begins:",
				"k\\s  0    1     2     3     4     5     6    7    8   9 10",
				"0:   1",
				"1:   3    1",
				"2:   5    5     1",
				"3:   7   14     7     1",
				"4:   9   30    27     9     1",
				"5:  11   55    77    44    11     1",
				"6:  13   91   182   156    65    13     1",
				"7:  15  140   378   450   275    90    15    1",
				"8:  17  204   714  1122   935   442   119   17    1",
				"9:  19  285  1254  2508  2717  1729   665  152   19   1",
				"10: 21  385  2079  5148  7007  5733  2940  952  189  21  1",
				"... Extended and reformatted by _Wolfdieter Lang_, Oct 18 2012",
				"Application for Fibonacci numbers F_{(2*k+1)*n}, row k=3:",
				"F_{7*n} = 7*(-1)^(3*n)*F_n + 14*(-1)^(4*n)*5*F_n^3 + 7*(-1)^(5*n)*5^2*F_n^5 + 1*(-1)^(6*n)*5^3*F_n^7, n\u003e=0. - _Wolfdieter Lang_, Aug 24 2012",
				"Example for the  Z- and A-sequence recurrences  of this Riordan triangle: Z = A217477 = [3,-4,12,-40,...]; T(4,0) = 3*7 -4*14 +12*7 -40*1 = 9. A =  [1, 2, -1, 2, -5, 14, ..]; T(5,2) = 1*30 + 2*27 - 1*9 + 2*1= 77. _Wolfdieter Lang_, Oct 18 2012",
				"Example for the (4*(k+1))-gon length ratio s(4*(k+1))(side/radius) as polynomial in the ratio rho(4*(k+1)) ((smallest diagonal)/side): k=0, s(4) = 1*rho(4) = sqrt(2); k=1, s(8) = -3*rho(8) + rho(8)^3 = sqrt(2-sqrt(2)); k=2, s(12) = 5*rho(12) - 5*rho(12)^3 + rho(12)^5, and C(12,x) = x^4 - 4*x^2 + 1, hence rho(12)^5 = 4*rho(12)^3 - rho(12), and s(12) = 4*rho(12) - rho(12)^3 = sqrt(2 - sqrt(3)). - _Wolfdieter Lang_, Oct 04 2013",
				"Example for the recurrence for the signed triangle S(k,s)= ((-1)^(k-s))*T(k,s) (see the Aug 14 2014 comment above):",
				"S(4,1) = 0 + (-2*2 - 1)*S(3,1) - (1/2)*(3*4^2*S(3,2) + 4*4^3*S(3,3)) = - 5*14 - 3*8*(-7) - 128*1 = -30. The recurrence from the Riordan A-sequence A115141 is S(4,1) = -7 -2*14 -(-7) -2*1 = -30. - _Wolfdieter Lang_, Aug 14 2014"
			],
			"mathematica": [
				"u[1, x_] := 1; v[1, x_] := 1; z = 16;",
				"u[n_, x_] := u[n - 1, x] + x*v[n - 1, x];",
				"v[n_, x_] := u[n - 1, x] + (x + 1)*v[n - 1, x] + 1;",
				"Table[Expand[u[n, x]], {n, 1, z/2}]",
				"Table[Expand[v[n, x]], {n, 1, z/2}]",
				"cu = Table[CoefficientList[u[n, x], x], {n, 1, z}];",
				"TableForm[cu]",
				"Flatten[%]  (* A208513 *)",
				"Table[Expand[v[n, x]], {n, 1, z}]",
				"cv = Table[CoefficientList[v[n, x], x], {n, 1, z}];",
				"TableForm[cv]",
				"Flatten[%]  (* A111125 *)",
				"(* _Clark Kimberling_, Feb 28 2012 *)"
			],
			"program": [
				"(Sage)",
				"@CachedFunction",
				"def T(n,k):",
				"    if n\u003c 0: return 0",
				"    if n==0: return 1 if k == 0 else 0",
				"    h = 3*T(n-1,k) if n==1 else 2*T(n-1,k)",
				"    return T(n-1,k-1) - T(n-2,k) - h",
				"A111125 = lambda n,k: (-1)^(n-k)*T(n,k)",
				"for n in (0..9): [A111125(n,k) for k in (0..n)] # _Peter Luschny_, Nov 20 2012"
			],
			"xref": [
				"Mirror image of A082985, which see for further references, etc.",
				"Also closely related to triangles in A098599 and A100218.",
				"A052227, A208513, A208508. A085478, A211957",
				"Cf. A034807, A127672, A127677, A263196."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Oct 16 2005",
			"ext": [
				"More terms from _Paul Barry_, Oct 17 2005"
			],
			"references": 43,
			"revision": 112,
			"time": "2021-12-23T02:58:00-05:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}