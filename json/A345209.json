{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A345209",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 345209,
			"data": "1,1,3,4,6,6,21,16,27,12,66,24,78,42,36,64,136,162,190,48,252,132,253,192,150,156,243,168,870,72,496,256,396,816,252,648,666,1140,468,384,1722,504,903,1056,324,1518,3243,1536,1029,300,816,624,1378,1458,3960,1344,1140,1740,1770,576",
			"name": "Number of Petrie polygons on the regular triangular map corresponding to the principal congruence subgroup Gamma(n) of the modular group.",
			"comment": [
				"To each principal congruence subgroup Gamma(n) of the modular group Gamma = PSL(2,Z) there corresponds a regular triangular map (it is the quotient of the Farey map by Gamma(n)). A Petrie polygon is a closed left-right zig-zagging path on the map. a(n) is the number of such paths."
			],
			"link": [
				"Tom Harris, \u003ca href=\"/A345209/b345209.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"F. Klein, \u003ca href=\"https://doi.org/10.1007/BF01677143\"\u003eUeber die Transformation siebenter Ordnungder elliptischen Funktionen\u003c/a\u003e, Mathematische Annalen, 14 (1878), 428-471.",
				"D. Singerman and J. Strudwick, \u003ca href=\"https://doi.org/10.26493/1855-3974.864.e9b\"\u003ePetrie polygons, Fibonacci sequences and Farey maps\u003c/a\u003e, Ars Mathematica Contemporanea, 10 (2016), 349-357."
			],
			"formula": [
				"a(n) = A001766(n)/A301759(n), n \u003e= 3 (Corollary 7.3 of Singerman \u0026 Strudwick)"
			],
			"example": [
				"The regular triangular map corresponding to Gamma(3) is the tetrahedron; one can easily check by hand that there are 3 distinct closed left-right zigzag paths (Petrie polygons) along the edges of the tetrahedron, so a(3) = 3.",
				"Similarly, there are a(4) = 4 and a(5) = 6 such paths on the octahedron and the icosahedron, the maps corresponding to Gamma(4), and Gamma(5) respectively.",
				"The map corresponding to Gamma(7) is the Klein map on his quartic curve. There are 21 Petrie polygons on this map; Klein drew 3 of them in his 1878 paper on the quartic, and the others can be found by rotating these through 2*Pi*k/7, k=1,...,6."
			],
			"mathematica": [
				"b[n_] := (n^3/2) Times @@ (1-1/Select[Range[n], Mod[n, #] == 0 \u0026\u0026 PrimeQ[#]\u0026]^2);",
				"c[n_] := With[{F = Fibonacci}, For[k = 1, True, k++, If[Mod[F[k], n] == 0 \u0026\u0026 (Mod[F[k+1], n] == 1 || Mod[F[k+1], n] == n-1), Return[k]]]];",
				"a[n_] := If[n\u003c3, 1, b[n]/c[n]];",
				"Array[a, 60] (* _Jean-François Alcover_, Jun 11 2021 *)",
				"Table[((n^3/2^Boole[n \u003e 1]) Product[1 - 1/k^2, {k, Select[Divisors[n], PrimeQ]}])/NestWhile[# + 1 \u0026, 1, ! (Mod[Fibonacci[#], n] == 0 \u0026\u0026 With[{f = Mod[Fibonacci[# + 1], n]}, f == 1 || f == n - 1]) \u0026], {n, 60}] (* _Jan Mangaldan_, Sep 12 2021 *)"
			],
			"program": [
				"(Python)",
				"from sympy import primedivisors",
				"def a(n):",
				"    # degenerate cases",
				"    if n == 1 or n == 2:",
				"        return 1",
				"    # calculate index of Γ(n) in Γ",
				"    index = n**3",
				"    for p in primefactors(n):",
				"        index *= (p**2 - 1)",
				"        index //= p**2",
				"    index //= 2",
				"    # calculate pisano semiperiod",
				"    sigma = 1",
				"    a, b = 1, 1",
				"    while (a,b) != (0,1) and (a,b) != (0, n - 1):",
				"        a, b = b, (a + b) % n",
				"        sigma += 1",
				"    # number of petrie polygons = index / sigma",
				"    return index // sigma"
			],
			"xref": [
				"A301759 gives the lengths of the Petrie polygons on the map in question."
			],
			"keyword": "nonn,easy,walk",
			"offset": "1,3",
			"author": "_Tom Harris_, Jun 10 2021",
			"references": 1,
			"revision": 26,
			"time": "2021-09-12T22:35:28-04:00",
			"created": "2021-06-11T00:24:30-04:00"
		}
	]
}