{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A067747",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 67747,
			"data": "2,4,3,6,5,8,7,9,11,10,13,12,17,14,19,15,23,16,29,18,31,20,37,21,41,22,43,24,47,25,53,26,59,27,61,28,67,30,71,32,73,33,79,34,83,35,89,36,97,38,101,39,103,40,107,42,109,44,113,45,127,46,131,48,137,49,139",
			"name": "Primes interleaved between composite numbers: n-th prime followed by the n-th composite number.",
			"comment": [
				"a(2*n-1) = A000040(n); a(2*n) = A002808(n). - _Reinhard Zumkeller_, Jan 29 2014"
			],
			"link": [
				"Zak Seidov, \u003ca href=\"/A067747/b067747.txt\"\u003eTable of n, a(n) for n = 1..1000.\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(2*n-1) = A000040(n); a(2*n) = A002808(n). - _Reinhard Zumkeller_, Jan 29 2014",
				"a(n) = A000040(ceiling(n/2))*A000035(n) + A002808(ceiling(n/2))*A059841(n), equivalent to the Zumkeller formula. - _Chayim Lowen_, Jul 29 2015"
			],
			"example": [
				"For n=4, the index is even. Therefore a(4)=A002808(4/2)=A002808(2)=6."
			],
			"maple": [
				"P,C:= selectremove(isprime,[$2..1000]):",
				"seq(op([P[i],C[i]]),i=1..min(nops(P),nops(C))); # _Robert Israel_, Jul 24 2015"
			],
			"mathematica": [
				"Array[c,1000];pc=-1;nc=0;Do[If[PrimeQ[n],If[pc==999,Break[],pc+=2;c[pc]=n],If[nc\u003c=998,nc+=2;c[nc]=n,Goto[ne]]];Label[ne],{n,2,20000}];Table[c[i],{i,1000}] (* _Zak Seidov_, Mar 22 2008 *)",
				"Composite[n_Integer] := FixedPoint[n + PrimePi@ # + 1 \u0026, n + PrimePi@ n + 1]; Table[{Prime@ n, Composite@ n}, {n, 35}] // Flatten (* _Robert G. Wilson v_, Jun 08 2008 *)"
			],
			"program": [
				"(Haskell)",
				"import Data.List (transpose)",
				"a067747 n = a067747_list !! (n-1)",
				"a067747_list = concat $ transpose [a000040_list, a002808_list]",
				"-- _Reinhard Zumkeller_, Jan 29 2014",
				"(PARI) c(n) = for(k=0, primepi(n), isprime(n++)\u0026\u0026k--); n; \\\\ A002808",
				"a(n) = if (n%2, prime((n+1)/2), c((n+1)\\2)); \\\\ _Michel Marcus_, Mar 06 2021"
			],
			"xref": [
				"Cf. A073846, A002808, A000040."
			],
			"keyword": "easy,nonn",
			"offset": "1,1",
			"author": "_Amarnath Murthy_, Feb 26 2002",
			"references": 3,
			"revision": 48,
			"time": "2021-03-07T01:16:56-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}