{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000346",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 346,
			"id": "M3920 N1611",
			"data": "1,5,22,93,386,1586,6476,26333,106762,431910,1744436,7036530,28354132,114159428,459312152,1846943453,7423131482,29822170718,119766321572,480832549478,1929894318332,7744043540348,31067656725032,124613686513778,499744650202436",
			"name": "a(n) = 2^(2*n+1) - binomial(2*n+1, n+1).",
			"comment": [
				"Also a(n) = 2nd elementary symmetric function of binomial(n,0), binomial(n,1), ..., binomial(n,n).",
				"Also a(n) = one half the sum of the heights, over all Dyck (n+2)-paths, of the vertices that are at even height and terminate an upstep. For example with n=1, these vertices are indicated by asterisks in the 5 Dyck 3-paths: UU*UDDD, UU*DU*DD, UDUU*DD, UDUDUD, UU*DDUD, yielding a(1)=(2+4+2+0+2)/2=5. - _David Callan_, Jul 14 2006",
				"Hankel transform is (-1)^n*(2n+1); the Hankel transform of sum(k=0..n, C(2*n,k) ) - C(2n,n) is (-1)^n*n. - _Paul Barry_, Jan 21 2007",
				"Row sums of the Riordan matrix (1/(1-4x),(1-sqrt(1-4x))/2) (A187926). - _Emanuele Munarini_, Mar 16 2011",
				"From _Gus Wiseman_, Jul 19 2021: (Start)",
				"For n \u003e 0, a(n-1) is also the number of integer compositions of 2n with nonzero alternating sum, where the alternating sum of a sequence (y_1,...,y_k) is Sum_i (-1)^(i-1) y_i. These compositions are ranked by A053754 /\\ A345921. For example, the a(3-1) = 22 compositions of 6 are:",
				"  (6)  (1,5)  (1,1,4)  (1,1,1,3)  (1,1,1,1,2)",
				"       (2,4)  (1,2,3)  (1,1,3,1)  (1,1,2,1,1)",
				"       (4,2)  (1,4,1)  (1,2,1,2)  (2,1,1,1,1)",
				"       (5,1)  (2,1,3)  (1,3,1,1)",
				"              (2,2,2)  (2,1,2,1)",
				"              (3,1,2)  (3,1,1,1)",
				"              (3,2,1)",
				"              (4,1,1)",
				"(End)"
			],
			"reference": [
				"T. Myers and L. Shapiro, Some applications of the sequence 1, 5, 22, 93, 386, ... to Dyck paths and ordered trees, Congressus Numerant., 204 (2010), 93-104.",
				"D. Phulara and L. W. Shapiro, Descendants in ordered trees with a marked vertex, Congressus Numerantium, 205 (2011), 121-128.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A000346/b000346.txt\"\u003eTable of n, a(n) for n = 0..500\u003c/a\u003e",
				"R. Bacher, \u003ca href=\"http://arxiv.org/abs/math/0409050\"\u003eOn generating series of complementary plane trees\u003c/a\u003e arXiv:math/0409050 [math.CO], 2004.",
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/2004.04577\"\u003eOn a Central Transform of Integer Sequences\u003c/a\u003e, arXiv:2004.04577 [math.CO], 2020.",
				"E. A. Bender, E. R. Canfield and R. W. Robinson, \u003ca href=\"http://dx.doi.org/10.4153/CMB-1988-039-4\"\u003eThe enumeration of maps on the torus and the projective plane\u003c/a\u003e, Canad. Math. Bull., 31 (1988), 257-271; see p. 270.",
				"D. E. Davenport, L. K. Pudwell, L. W. Shapiro and L. C. Woodson, \u003ca href=\"http://faculty.valpo.edu/lpudwell/papers/treeboundary.pdf\"\u003eThe Boundary of Ordered Trees\u003c/a\u003e, 2014.",
				"P. Flajolet and R. Sedgewick, \u003ca href=\"http://algo.inria.fr/flajolet/Publications/books.html\"\u003eAnalytic Combinatorics\u003c/a\u003e, 2009; see page 185",
				"R. K. Guy, \u003ca href=\"/A000346/a000346.pdf\"\u003eLetter to N. J. A. Sloane\u003c/a\u003e",
				"Mircea Merca, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL15/Merca1/merca6.html\"\u003e A Special Case of the Generalized Girard-Waring Formula\u003c/a\u003e J. Integer Sequences, Vol. 15 (2012), Article 12.5.7. - From _N. J. A. Sloane_, Nov 25 2012",
				"D. Merlini, R. Sprugnoli and M. C. Verri, \u003ca href=\"http://www.dsi.unifi.it/~merlini/fun01.ps\"\u003eWaiting patterns for a printer\u003c/a\u003e, FUN with algorithm'01, Isola d'Elba, 2001.",
				"D. Merlini, R. Sprugnoli and M. C. Verri, \u003ca href=\"http://dx.doi.org/10.1006/jcta.2002.3273\"\u003eThe tennis ball problem\u003c/a\u003e, J. Combin. Theory, A 99 (2002), 307-344 (A_n for s=2).",
				"John Riordan, \u003ca href=\"/A002720/a002720_3.pdf\"\u003eLetter to N. J. A. Sloane, Sep 26 1980 with notes on the 1973 Handbook of Integer Sequences\u003c/a\u003e. Note that the sequences are identified by their N-numbers, not their A-numbers.",
				"W. T. Tutte, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9904-1968-11877-4\"\u003eOn the enumeration of planar maps\u003c/a\u003e. Bull. Amer. Math. Soc. 74 1968 64-74.",
				"T. R. S. Walsh and A. B. Lehman, \u003ca href=\"http://dx.doi.org/10.1016/0095-8956(72)90056-1\"\u003eCounting rooted maps by genus\u003c/a\u003e, J. Comb. Thy B13 (1972), 122-141 and 192-218 (eq. 5, b=1).",
				"N. J. A. Sloane, \u003ca href=\"/A007401/a007401_1.pdf\"\u003eNotes\u003c/a\u003e"
			],
			"formula": [
				"G.f.: c(x)/(1-4x), c(x) = g.f. of Catalan numbers.",
				"Convolution of Catalan numbers and powers of 4.",
				"Also one half of convolution of central binomial coeffs. A000984(n), n=0, 1, 2, ... with shifted central binomial coeffs. A000984(n), n=1, 2, 3, ...",
				"a(n) = A045621(2n+1) = (1/2)*A068551(n+1).",
				"a(n) = Sum_{k=0..n} A000984(k)*A001700(n-k). - _Philippe Deléham_, Jan 22 2004",
				"a(n) = Sum_{k=0..n+1} binomial(n+k, k-1)2^(n-k+1). - _Paul Barry_, Nov 13 2004",
				"a(n) = Sum_{i=0..n} binomial(2n+2, i). See A008949. - Ed Catmur (ed(AT)catmur.co.uk), Dec 09 2006",
				"a(n) = Sum_{k=0..n+1, C(2n+2,k)} - C(2n+2,n+1). - _Paul Barry_, Jan 21 2007",
				"Logarithm g.f. log(1/(2-C(x)))=sum(n\u003e0, a(n)/n*x^n), C(x)=(1-sqrt(1-4*x))/2x (A000108). - _Vladimir Kruchinin_, Aug 10 2010",
				"D-finite with recurrence: (n+3) a(n+2) - 2(4n+9) a(n+1) + 8(2n+3) a(n) = 0. - _Emanuele Munarini_, Mar 16 2011",
				"E.g.f.:exp(2*x)*(2*exp(2*x) - BesselI(0,2*x) - BesselI(1,2*x)).",
				"This is the first derivative of exp(2*x)*(exp(2*x) - BesselI(0,2*x))/2. See the e.g.f. of A032443 (which has a plus sign) and the remarks given there. - _Wolfdieter Lang_, Jan 16 2012",
				"a(n) = Sum_{0\u003c=i\u003cj\u003c=n+1} binomial(n+1, i)*binomial(n+1, j). - _Mircea Merca_, Apr 05 2012",
				"A000346 = A004171 - A001700. See A032443 for the sum. - _M. F. Hasler_, Jan 02 2014",
				"0 = a(n) * (256*a(n+1) - 224*a(n+2) + 40*a(n+3)) + a(n+1) * (-32*a(n+1) + 56*a(n+2) - 14*a(n+3)) + a(n+2) * (-2*a(n+2) + a(n+3)) if n\u003e-5. - _Michael Somos_, Jan 25 2014",
				"REVERT transform is [1,-5,28,-168,1056,...] = alternating signed version of A069731. - _Michael Somos_, Jan 25 2014",
				"Convolution square is A038806. - _Michael Somos_, Jan 25 2014",
				"BINOMIAL transform of A055217(n-1) is a(n-1). - _Michael Somos_, Jan 25 2014",
				"(n+1) * a(n) = A033504(n). - _Michael Somos_, Jan 25 2014",
				"Recurrence: (n+1)*a(n) = 512*(2*n-7)*a(n-5) + 256*(13-5*n)*a(n-4) + 64*(10*n-17)*a(n-3) + 32*(4-5*n)*a(n-2) + 2*(10*n+1)*a(n-1), n\u003e=5. - _Fung Lam_, Mar 21 2014",
				"Asymptotic approximation: a(n) ~ 2^(2n+1)*(1-1/sqrt(n*Pi)). - _Fung Lam_, Mar 21 2014",
				"a(n) = Sum_{m = n+2..2*(n+1)} binomial(2*(n+1), m), n \u003e= 0. - _Wolfdieter Lang_, May 22 2015",
				"a(n) = A000302(n) + A008549(n). - _Gus Wiseman_, Jul 19 2021"
			],
			"example": [
				"G.f. = 1 + 5*x + 22*x^2 + 93*x^3 + 386*x^4 + 1586*x^5 + 6476*x^6 + ..."
			],
			"maple": [
				"seq(2^(2*n+1)-binomial(2*n,n)*(2*n+1)/(n+1), n=0..12); # _Emanuele Munarini_, Mar 16 2011"
			],
			"mathematica": [
				"Table[2^(2n+1)-Binomial[2n,n](2n+1)/(n+1),{n,0,20}] (* _Emanuele Munarini_, Mar 16 2011 *)",
				"a[ n_] := If[ n\u003c-4, 0, (4^(n + 1) - Binomial[2 n + 2, n + 1]) / 2]; (* _Michael Somos_, Jan 25 2014 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c-4, 0, n++; (2^(2*n) - binomial(2*n, n)) / 2)}; /* _Michael Somos_, Jan 25 2014 */",
				"(Maxima) makelist(2^(2*n+1)-binomial(2*n,n)*(2*n+1)/(n+1),n,0,12); /* _Emanuele Munarini_, Mar 16 2011 */",
				"(MAGMA) [2^(2*n+1) - Binomial(2*n+1,n+1): n in [0..30]]; // _Vincenzo Librandi_, Jun 07 2011"
			],
			"xref": [
				"Cf. A000108, A014137, A014318. A column of A058893. Subdiagonal of A053979.",
				"Bisection of A058622 and (possibly) A007008.",
				"Cf. A045621, A068551.",
				"Cf. A033504, A038806, A055217, A069731.",
				"Even bisection of A294175 (without the first two terms).",
				"The following relate to compositions of 2n with alternating sum k.",
				"- The k \u003e 0 case is counted by A000302.",
				"- The k \u003c= 0 case is counted by A000302.",
				"- The k != 0 case is counted by A000346 (this sequence).",
				"- The k = 0 case is counted by A001700 or A088218.",
				"- The k \u003c 0 case is counted by A008549.",
				"- The k \u003e= 0 case is counted by A114121.",
				"A011782 counts compositions.",
				"A086543 counts partitions with nonzero alternating sum (bisection: A182616).",
				"A097805 counts compositions by alternating (or reverse-alternating) sum.",
				"A103919 counts partitions by sum and alternating sum (reverse: A344612).",
				"A345197 counts compositions by length and alternating sum.",
				"Cf. A000070, A001791, A007318, A025047, A027306, A032443, A053754, A120452, A163493, A239830, A344611, A345921."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, _Wolfdieter Lang_",
			"ext": [
				"Corrected by _Christian G. Bower_"
			],
			"references": 84,
			"revision": 131,
			"time": "2021-07-20T03:28:25-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}