{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A332436",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 332436,
			"data": "1,0,1,1,2,2,3,2,4,4,4,5,5,4,7,7,6,6,9,6,10,10,6,11,11,8,13,10,10,14,15,8,12,16,12,17,18,10,16,19,14,20,16,14,22,18,16,18,24,14,25,25,12,26,27,18,28,22,18,24,28,20,25,31,22,32,28,18,34,34,24",
			"name": "The number of even numbers \u003c= n of the smallest nonnegative reduced residue system modulo 2*n + 1, for n \u003e= 0.",
			"comment": [
				"For the smallest positive reduced residue system modulo N see the array A038566. Here the nonnegative residue system [0, 1, ..., N-1] is considered, differing only for N = 1 from A038566, with [0] (instead of [1]).",
				"This sequence gives the complement of A332435 (with 0 for n = 0 included) relative to the number of positive numbers \u003c= n of the smallest nonnegative reduced residue system modulo (2*n+1). Thus a(n) + A332435(n) = phi(n)/2, for n \u003e= 1, with phi = A000010. For n = 0 one has 1 + 0 = 1.",
				"a(n) gives also the number of even numbers appearing in the complete modified doubling sequence system (name it MDS(b)), for b = 2*n + 1, with n \u003e= 1, proposed in a comment from _Gary W. Adamson_, Aug 24 2019, in the example section of A135303 for prime b."
			],
			"formula": [
				"a(n) = A000010(n)/2 - A332435(n), for n \u003e= 1, and a(0) = 1."
			],
			"example": [
				"n = 4, b = 9: the even numbers \u003c= 4 in RRS(9) := [1, 2, 4, 5, 7, 8] are {2, 4}, hence a(4) = 2.",
				"The complete MDS(9) system has one cycles of length 3: Cy*(9, 1) = (2, 4, 1), with the even numbers {2, 4}.",
				"n = 8, b = 17: the even numbers \u003c= 8 in RRS(17) := [1, 2, ..., 16] are {2, 4, 6 ,8}, hence a(8) = 4.",
				"The complete MDS(17) system has two cycles of length 4: Cy*(17, 1) = (2, 4, 8, 1) and Cy*(17, 2) = (6, 5, 7, 3) and the even numbers are {2, 4, 6 ,8}."
			],
			"xref": [
				"Cf. A000010, A038566, A135303, A332435."
			],
			"keyword": "nonn,easy",
			"offset": "0,5",
			"author": "_Wolfdieter Lang_, Feb 29 2020",
			"references": 2,
			"revision": 10,
			"time": "2020-03-13T12:56:00-04:00",
			"created": "2020-03-13T12:56:00-04:00"
		}
	]
}