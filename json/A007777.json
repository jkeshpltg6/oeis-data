{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007777",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7777,
			"data": "1,2,4,6,10,14,20,24,30,36,44,48,60,60,62,72,82,88,96,112,120,120,136,148,164,152,154,148,162,176,190,196,210,216,224,228,248,272,284,296,300,296,320,332,356,356,376,400,416,380,382,376,382,356,374,392,410",
			"name": "Number of overlap-free binary words of length n.",
			"reference": [
				"J. Cassaigne, Counting overlap-free binary words, pp. 216-225 of STACS '93, Lect. Notes Comput. Sci., Springer-Verlag, 1993.",
				"J. Cassaigne, Motifs évitables et régularités dans les mots (Thèse de Doctorat), Tech. Rep. LITP-TH 94-04, Institut Blaise Pascal, Paris, 1994.",
				"Raphael M. Jungers, Vladimir Yu. Protasov and Vincent D. Blondel, Computing the Growth of the Number of Overlap-Free Words with Spectra of Matrices, in LATIN 2008: Theoretical Informatics, Lecture Notes in Computer Science, Volume 4957/2008, [From _N. J. A. Sloane_, Jul 10 2009]"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A007777/b007777.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"J.-P. Allouche and J. Shallit, \u003ca href=\"http://dx.doi.org/10.1016/S0304-3975(03)00090-2\"\u003eThe ring of k-regular sequences, II\u003c/a\u003e, Theoret. Computer Sci., 307 (2003), 3-29.",
				"Franz-Josef Brandenburg, \u003ca href=\"https://doi.org/10.1016/0304-3975(88)90009-6\"\u003eUniformly Growing k-th Power-Free Homomorphisms\u003c/a\u003e, Theoretical Computer Science, volume 23, 1983, pages 69-82.  See density of two letter weakly square-free strings (as overlap-free is called in this paper) after theorem 12 page 80.  A misprint omits a(14) = 62 from the list of values.",
				"J. Cassaigne, \u003ca href=\"/A007777/a007777.pdf\"\u003eCounting overlap-free binary words\u003c/a\u003e, Preprint. (Annotated scanned copy)",
				"J. Cassaigne, \u003ca href=\"http://iml.univ-mrs.fr/~cassaign/publis/overlap.ps.gz\"\u003eCounting overlap-free binary words\u003c/a\u003e",
				"K. Jarhumaki and J. Shallit, \u003ca href=\"https://arxiv.org/abs/math/0304095\"\u003ePolynomial vs Exponential Growth in Repetition-Free Binary Words\u003c/a\u003e, arXiv:math/0304095 [math.CO], 2003.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/OverlapfreeWord.html\"\u003eOverlapfree Word\u003c/a\u003e."
			],
			"maple": [
				"delta:=linalg[matrix](4,4,[0,0,1,1,0,0,0,0,0,1,0,0,1,0,0,0]); iota:=linalg[matrix](4,4,[0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0]); kappa:=linalg[matrix](4,4,[0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0]);",
				"V:=proc(n) options remember: if n\u003e7 and n mod 2 =1 then RETURN(evalm(kappa \u0026* V((n+1)/2) \u0026* transpose(delta) + delta \u0026* V((n+1)/2) \u0026* transpose(kappa))) elif n=5 then RETURN(linalg[matrix](4,4,[2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0])) elif n=7 then RETURN(linalg[matrix](4,4,[0,0,2,0,0,2,0,0,2,0,0,0,0,0,0,0])) else RETURN(linalg[matrix](4,4,0)) fi: end;",
				"U:=proc(n) options remember: if n\u003e7 and n mod 2 =1 then RETURN(evalm(iota \u0026* V((n+1)/2) \u0026* transpose(delta) + delta \u0026* V((n+1)/2) \u0026* transpose(iota) + (kappa+iota) \u0026* U((n+1)/2) \u0026* transpose(delta) + delta \u0026* U((n+1)/2) \u0026* transpose(kappa+iota))) elif n\u003e7 and n mod 2 =0 then RETURN(evalm(iota \u0026* V(n/2) \u0026* transpose(iota) + delta \u0026* V(n/2+1) \u0026* transpose(delta) + (kappa+iota) \u0026* U(n/2) \u0026* transpose(kappa+iota) + delta \u0026* U(n/2+1) \u0026* transpose(delta))) elif n=4 then RETURN(linalg[matrix](4,4,[2,0,2,0,0,2,0,0,2,0,0,0,0,0,0,2])) elif n=5 then RETURN(linalg[matrix](4,4,[0,2,2,0,2,0,0,2,2,0,2,0,0,2,0,0])) elif n=6 then RETURN(linalg[matrix](4,4,[2,2,0,2,2,2,2,0,0,2,2,0,2,0,0,2])) elif n=7 then RETURN(linalg[matrix](4,4,[4,2,0,2,2,0,2,2,0,2,0,2,2,2,2,0])) fi: end;",
				"a:=proc(n) if n\u003c4 then RETURN([1,2,4,6][n+1]) else RETURN(add(add(U(n)[i,j],i=1..4),j=1..4)) fi: end; seq(a(n),n=0..100); # Pab Ter (pabrlos2(AT)yahoo.com), Nov 09 2005"
			],
			"mathematica": [
				"delta = {{0, 0, 1, 1}, {0, 0, 0, 0}, {0, 1, 0, 0}, {1, 0, 0, 0}}; iota = {{0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 0}}; kappa = {{0, 0, 1, 1}, {1, 1, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}}; V[n_] := V[n] = Which[n \u003e 7 \u0026\u0026 Mod[n, 2] == 1, delta . V[(n+1)/2] . Transpose[kappa] + kappa . V[(n+1)/2] . Transpose[delta], n == 5, {{2, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}}, n == 7, {{0, 0, 2, 0}, {0, 2, 0, 0}, {2, 0, 0, 0}, {0, 0, 0, 0}}, True, Array[0\u0026 , {4, 4}]]; U[n_] := U[n] = Which[n \u003e 7 \u0026\u0026 Mod[n, 2] == 1, delta . U[(n+1)/2] . Transpose[iota + kappa] + delta . V[(n+1)/2] . Transpose[iota] + iota . V[(n+1)/2] . Transpose[delta] + (iota + kappa) . U[(n+1)/2] . Transpose[delta], n \u003e 7 \u0026\u0026 Mod[n, 2] == 0, delta.U[n/2+1] . Transpose[delta] + delta . V[n/2+1] . Transpose[delta] + iota . V[n/2] . Transpose[iota] + (iota + kappa) . U[n/2] . Transpose[iota + kappa], n == 4, {{2, 0, 2, 0}, {0, 2, 0, 0}, {2, 0, 0, 0}, {0, 0, 0, 2}}, n == 5, {{0, 2, 2, 0}, {2, 0, 0, 2}, {2, 0, 2, 0}, {0, 2, 0, 0}}, n == 6, {{2, 2, 0, 2}, {2, 2, 2, 0}, {0, 2, 2, 0}, {2, 0, 0, 2}}, n == 7, {{4, 2, 0, 2}, {2, 0, 2, 2}, {0, 2, 0, 2}, {2, 2, 2, 0}}]; a[n_] := If[n \u003c 4, {1, 2, 4, 6}[[n+1]], Sum[U[n][[i, j]], {i, 1, 4}, {j, 1, 4}]]; Table[a[n], {n, 0, 56}] (* _Jean-François Alcover_, Jan 02 2013, translated from Pab Ter's Maple program *)"
			],
			"xref": [
				"Cf. A028445, A038952, A082379, A082380."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,2",
			"author": "cassaign(AT)clipper.ens.fr (Julien Cassaigne)",
			"ext": [
				"More terms from Pab Ter (pabrlos2(AT)yahoo.com), Nov 09 2005"
			],
			"references": 6,
			"revision": 38,
			"time": "2020-08-15T09:58:29-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}