{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A035105",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 35105,
			"data": "1,1,2,6,30,120,1560,10920,185640,2042040,181741560,1090449360,254074700880,7368166325520,449458145856720,21124532855265840,33735878969859546480,640981700427331383120,2679944489486672512824720,109877724068953573025813520",
			"name": "a(n) = LCM of Fibonacci sequence {F_1,...,F_n}.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A035105/b035105.txt\"\u003eTable of n, a(n) for n = 1..124\u003c/a\u003e",
				"Yuri V. Matiyasevich and Richard K. Guy, \u003ca href=\"https://doi.org/10.1080/00029890.1986.11971904\"\u003eA new formula for Pi\u003c/a\u003e, The American Mathematical Monthly, Vol 93, No. 8 (1986), pp. 631-635.",
				"Carlo Sanna, \u003ca href=\"https://arxiv.org/abs/2007.13330\"\u003eOn the l.c.m. of shifted Fibonacci numbers\u003c/a\u003e, arXiv:2007.13330 [math.NT], 2020.",
				"\u003ca href=\"/index/Lc#lcm\"\u003eIndex entries for sequences related to LCM's\u003c/a\u003e"
			],
			"formula": [
				"log(a(n)) ~ 3*n^2*log(phi)/Pi^2, where phi is the golden ratio, or equivalently lim_{n-\u003eoo} sqrt(6*log(A003266(n))/log(a(n))) = Pi. - _Amiram Eldar_, Jan 30 2019"
			],
			"maple": [
				"a:= proc(n) option remember; `if`(n=1, 1,",
				"      ilcm(a(n-1), combinat[fibonacci](n)))",
				"    end:",
				"seq(a(n), n=1..25);  # _Alois P. Heinz_, Feb 12 2018"
			],
			"mathematica": [
				"a[ n_ ] := LCM@@Table[ Fibonacci[ k ], {k, 1, n} ]",
				"With[{fibs=Fibonacci[Range[20]]},Table[LCM@@Take[fibs,n],{n, Length[ fibs]}]] (* _Harvey P. Dale_, Apr 29 2019 *)"
			],
			"program": [
				"(PARI) a(n)=lcm(apply(fibonacci,[1..n])) \\\\ _Charles R Greathouse IV_, Oct 07 2016"
			],
			"xref": [
				"Cf. A000045, A001622, A003266, A059248."
			],
			"keyword": "nonn,easy",
			"offset": "1,3",
			"author": "Fred Schwab (fschwab(AT)nrao.edu)",
			"references": 13,
			"revision": 37,
			"time": "2020-07-28T11:19:18-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}