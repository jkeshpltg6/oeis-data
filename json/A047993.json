{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A047993",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 47993,
			"data": "1,0,1,1,1,1,3,2,4,4,6,7,11,11,16,19,25,29,40,45,60,70,89,105,134,156,196,232,285,336,414,485,591,696,839,987,1187,1389,1661,1946,2311,2702,3201,3731,4400,5126,6018,6997,8195,9502,11093,12849,14949,17281,20062",
			"name": "Number of balanced partitions of n: the largest part equals the number of parts.",
			"comment": [
				"Useful in the creation of plane partitions with C3 or C3v symmetry.",
				"The function T[m,a,b] used here gives the partitions of m whose Ferrers plot fits within an a X b box.",
				"Central terms of triangle in A063995: a(n) = A063995(n,0). - _Reinhard Zumkeller_, Jul 24 2013",
				"Sequence enumerates the collection of partitions of size n that are in the semigroup of Dyson rank=0, or balanced partitions, under the binary operation A*B = (a1,a2,...,a[k-1],k)*(b1,...,b[n-1,n) = (a1*b1,...,a1*n,a2*b1,...,a2*n,...,k*b1,...,k*n), where A is a partition with k parts and B is a partition with n parts, and A*B is a partition with k*n parts. Note that the rank of A*B is 0, as required. For example, the product of the rank 0 partitions (1,2,3) of 6 and (1,1,3) of 5 is the rank 0 partition (1,1,2,2,3,3,3,6,9) of 30. There is no rank zero partition of 2, as shown in the sequence. It can be seen that any element of the semigroup that partitions an odd prime p or a composite number of form 2p cannot be a product of smaller nontrivial partitions, whether in this semigroup or not. - _Richard Locke Peterson_, Jul 15 2018",
				"The Heinz numbers of these integer partitions are given by A106529. - _Gus Wiseman_, Mar 09 2019"
			],
			"link": [
				"Vaclav Kotesovec, \u003ca href=\"/A047993/b047993.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (terms 1..1000 from T. D. Noe)",
				"Erich Friedman, \u003ca href=\"/A047993/a047993.gif\"\u003eIllustration of initial terms\u003c/a\u003e"
			],
			"formula": [
				"a(n) = p(n-1) - p(n-2) - p(n-5) + p(n-7) + ... + (-1)^k*(p(n-(3*k^2-k)/2) - p(n-(3*k^2+k)/2)) + ..., where p() is A000041(). E.g., A047993 a(20) = p(19) - p(18) - p(15) + p(13) + p(8) - p(5) = 490 - 385 - 176 + 101 + 22 - 7 = 45. - _Vladeta Jovovic_, Aug 04 2004",
				"G.f.: Sum_{k\u003e=1} (-1)^k * ( x^((3*k^2+k)/2) - x^((3*k^2-k)/2) ) ) / Product_{k\u003e=1} (1-x^k). - _Vladeta Jovovic_, Aug 05 2004",
				"a(n) ~ exp(Pi*sqrt(2*n/3))*Pi / (48*sqrt(2)*n^(3/2)) ~ p(n) * Pi / (4*sqrt(6*n)), where p(n) is the partition function A000041. - _Vaclav Kotesovec_, Oct 06 2016",
				"G.f.: Sum_{n\u003e=0} [2n,n]_q q^(2*n), where [m,n]_q are the q-binomial coefficients. - _Mamuka Jibladze_, Aug 12 2021"
			],
			"example": [
				"From _Joerg Arndt_, Oct 08 2012: (Start)",
				"a(12) = 7 because the partitions of 12 where the largest part equals the number of parts are",
				"   2 + 3 + 3 + 4,",
				"   2 + 2 + 4 + 4,",
				"   1 + 3 + 4 + 4,",
				"   1 + 2 + 2 + 2 + 5,",
				"   1 + 1 + 2 + 3 + 5,",
				"   1 + 1 + 1 + 4 + 5, and",
				"   1 + 1 + 1 + 1 + 2 + 6.",
				"(End)",
				"From _Gus Wiseman_, Mar 09 2019: (Start)",
				"The a(1) = 1 through a(13) = 11 integer partitions:",
				"  1  21  22  311  321  322   332   333    4222   4322    4332    4333",
				"                       331   4211  4221   4321   4331    4422    4432",
				"                       4111        4311   4411   4421    4431    4441",
				"                                   51111  52111  52211   52221   52222",
				"                                                 53111   53211   53221",
				"                                                 611111  54111   53311",
				"                                                         621111  54211",
				"                                                                 55111",
				"                                                                 622111",
				"                                                                 631111",
				"                                                                 7111111",
				"(End)"
			],
			"maple": [
				"with(combinat): for n from 1 to 36 do P:=partition(n): c:=0: for j from 1 to nops(P) do if P[j][nops(P[j])]=nops(P[j]) then c:=c+1 else c:=c fi od: a[n]:=c: od: seq(a[n],n=1..36); # _Emeric Deutsch_, Dec 11 2004"
			],
			"mathematica": [
				"Table[ Count[Partitions[n], par_List/; First[par]===Length[par]], {n, 12}] or recur: Sum[T[n-(2m-1), m-1, m-1], {m, Ceiling[Sqrt[n]], Floor[(n+1)/2]}] with T[m_, a_, b_]/; b \u003c a := T[m, b, a]; T[m_, a_, b_]/; m \u003e a*b := 0; T[m_, a_, b_]/; (2m \u003e a*b) := T[a*b-m, a, b]; T[m_, 1, b_] := If[b \u003c m, 0, 1]; T[0, _, _] := 1; T[m_, a_, b_] := T[m, a, b]=Sum[T[m-a*i, a-1, b-i], {i, 0, Floor[m/a]}];",
				"Table[Sum[ -(-1)^k*(p[n-(3*k^2-k)/2] - p[n-(3*k^2+k)/2]), {k, 1, Floor[(1+Sqrt[1+24*n])/6]}] /. p -\u003e PartitionsP, {n, 1, 64}] (* _Wouter Meeussen_ *)",
				"(* also *)",
				"Table[Count[IntegerPartitions[n], q_ /; Max[q] == Length[q]], {n, 24}]",
				"(* _Clark Kimberling_, Feb 13 2014 *)"
			],
			"program": [
				"(PARI)",
				"N=66;  q='q + O('q^N );",
				"S=2+2*ceil(sqrt(N));",
				"gf= sum(k=1, S,  (-1)^k * ( q^((3*k^2+k)/2) - q^((3*k^2-k)/2) ) ) / prod(k=1,N, 1-q^k );",
				"/* _Joerg Arndt_, Oct 08 2012 */",
				"(Haskell)",
				"a047993 = flip a063995 0  -- _Reinhard Zumkeller_, Jul 24 2013"
			],
			"xref": [
				"Cf. A000700, A063995, A064173, A064174.",
				"Cf. A003114, A006141, A039900, A090858, A106529, A324516, A324518, A324520."
			],
			"keyword": "nice,nonn",
			"offset": "1,7",
			"author": "_Wouter Meeussen_",
			"references": 100,
			"revision": 80,
			"time": "2021-08-24T20:03:36-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}