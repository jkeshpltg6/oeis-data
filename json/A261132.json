{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A261132",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 261132,
			"data": "1,1,2,3,4,5,7,8,10,12,13,15,16,17,17,18,17,17,16,15,13,12,11,10,9,8,7,7,6,6,6,6,5,7,6,6,6,6,6,6,6,6,6,5,8,7,7,7,7,7,7,7,7,7,5,9,7,7,7,7,7,7,7,7,7,5,11,8,8,8,8,8,8,8,8,8,5,12,8,8,8",
			"name": "Number of ways to write n as the sum u+v+w of three palindromes (from A002113) with 0 \u003c= u \u003c= v \u003c= w.",
			"comment": [
				"It is known that a(n) \u003e 0 for every n, i.e., every number can be written as the sum of 3 palindromes.",
				"The graph has a kind of self-similarity: looking at the first 100 values, there is a Gaussian-shaped peak centered at the first local maximum a(15) = 18. Looking at the first 10000 values, one sees just one Gaussian-shaped peak centered around the record and local maximum a(1453) = 766, but to both sides of this value there are smaller peaks, roughly at distances which are multiples of 10. In the range [1..10^6], one sees a Gaussian-shaped peak centered around the record a(164445) = 57714. In the range [1..3*10^7], there is a similar peak of height ~ 4.3*10^6 at 1.65*10^7, with smaller neighbor peaks at distances which are multiples of 10^6, etc. - _M. F. Hasler_, Sep 09 2018"
			],
			"link": [
				"Giovanni Resta, \u003ca href=\"/A261132/b261132.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Javier Cilleruelo and Florian Luca, \u003ca href=\"http://arxiv.org/abs/1602.06208\"\u003eEvery positive integer is a sum of three palindromes\u003c/a\u003e, arXiv: 1602.06208 [math.NT], 2016-2017.",
				"Erich Friedman, \u003ca href=\"https://erich-friedman.github.io/mathmagic/0699.html\"\u003eProblem of the Month (June 1999)\u003c/a\u003e",
				"James Grime and Brady Haran, \u003ca href=\"https://www.youtube.com/watch?v=OKhacWQ2fCs\"\u003eEvery Number is the Sum of Three Palindromes\u003c/a\u003e, Numberphile video (2018)",
				"Hugo Pfoertner, \u003ca href=\"/A261132/a261132.png\"\u003ePlot of first 10^6 terms\u003c/a\u003e",
				"Hugo Pfoertner, \u003ca href=\"/A261132/a261132_1.png\"\u003ePlot of first 3*10^7 terms\u003c/a\u003e",
				"Hugo Pfoertner, \u003ca href=\"/A261132/a261132_2.png\"\u003ePlot of low values in range 7*10^6 ... 10^7 \u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{k=0..3} A319453(n,k). - _Alois P. Heinz_, Sep 19 2018"
			],
			"example": [
				"a(0)=1 because 0 = 0+0+0;",
				"a(1)=1 because 1 = 0+0+1;",
				"a(2)=2 because 2 = 0+1+1 = 0+0+2;",
				"a(3)=3 because 3 = 1+1+1 = 0+1+2 = 0+0+3.",
				"a(28) = 6 since 28 can be expressed in 6 ways as the sum of 3 palindromes, namely, 28 = 0+6+22 = 1+5+22 = 2+4+22 = 3+3+22 = 6+11+11 = 8+9+11."
			],
			"maple": [
				"A261132 := proc(n)",
				"    local xi,yi,x,y,z,a ;",
				"    a := 0 ;",
				"    for xi from 1 do",
				"        x := A002113(xi) ;",
				"        if 3*x \u003e n then",
				"            return a;",
				"        end if;",
				"        for yi from xi do",
				"            y := A002113(yi) ;",
				"            if x+2*y \u003e n then",
				"                break;",
				"            else",
				"                z := n-x-y ;",
				"                if z \u003e= y and isA002113(z) then",
				"                    a := a+1 ;",
				"                end if;",
				"            end if;",
				"        end do:",
				"    end do:",
				"    return a;",
				"end proc:",
				"seq(A261132(n),n=0..80) ; # _R. J. Mathar_, Sep 09 2015"
			],
			"mathematica": [
				"pal=Select[Range[0, 1000], (d = IntegerDigits@ #; d == Reverse@ d)\u0026]; a[n_] := Length@ IntegerPartitions[n, {3}, pal]; a /@ Range[0, 1000]"
			],
			"program": [
				"(PARI) A261132(n)=n||return(1); my(c=0, i=inv_A002113(n)); A2113[i] \u003e n \u0026\u0026 i--; until( A2113[i--]*3 \u003c n, j = inv_A002113(D = n-A2113[i]); if( j\u003ei, j=i, A2113[j] \u003e D \u0026\u0026 j--); while( j \u003e= k = inv_A002113(D - A2113[j]), A2113[k] == D - A2113[j] \u0026\u0026 c++; j--||break));c \\\\ For efficiency, this uses an array A2113 precomputed at least up to n. - _M. F. Hasler_, Sep 10 2018"
			],
			"xref": [
				"Cf. A002113, A035137, A260254, A261131, A319453.",
				"See A261422 for another version."
			],
			"keyword": "nonn,base,look",
			"offset": "0,3",
			"author": "_Giovanni Resta_, Aug 10 2015",
			"ext": [
				"Examples revised and plots for large n added by _Hugo Pfoertner_, Aug 11 2015"
			],
			"references": 19,
			"revision": 55,
			"time": "2020-08-13T14:02:28-04:00",
			"created": "2015-08-10T05:38:00-04:00"
		}
	]
}