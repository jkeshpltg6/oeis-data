{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A282626",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 282626,
			"data": "1,1,0,-2,8,0,-320,2800,0,-344960,4659200,0,-1172864000,21423001600,0,-9117844736000,209945415680000,0,-135381758640128000,3761801958154240000,0,-3421097040836034560000,111349337961365504000000,0,-135776499356700539617280000",
			"name": "Exponential expansion of the real root y = y(x) of y^3 - 3*x*y - 1.",
			"comment": [
				"This is an example of an application of Ramanujan's Master theorem for definite integrals; see eq. (B) on p. 186 of the Hardy reference. This application is given under (ii) on pp. 194-195; here with  r = 1, p = 1, q = 2, and x and a there are y and x here, respectively.",
				"The general formula for the exponential expansion of the r-th power of the solution y=y(x) of y^q - q*x*y - 1 = 0 which starts with y(0) = 1 is  y(x)^r  = Sum_{n\u003e=0} lambda(n;r,q,p)*x^n/n! with lambda(0;r,q,p) = 1, lambda(1;r,q,p) = r and lambda(n;r,q,p) = r*Product_{j=1..n-1} (r + n*p - q*j) for n \u003e= 2. Hardy gives a convergence condition for theorem (B) on p. 189: the class K(A,P,delta) for phi(u) = lambda(u) / Gamma(1+u), u complex, here for lambda(u) = lambda(u;r,q,p)."
			],
			"reference": [
				"G. H. Hardy, Ramanujan: twelve lectures on subjects suggested by his life and work, AMS Chelsea Publishing, Providence, Rhode Island, 2002, ch, XI, pp. 186-211."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A282626/b282626.txt\"\u003eTable of n, a(n) for n = 0..400\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Product_{j=1..n-1} (n + 1 - 3*j), n \u003e=0 (empty product = 1).",
				"E.g.f.: ((1 + sqrt(1-4*x^3))/2)^(1/3) + x/((1 + sqrt(1-4*x^3))/2)^(1/3).",
				"E.g.f.: ((1 + sqrt(1-4*x^3))/2)^(1/3) + ((1 - sqrt(1-4*x^3))/2)^(1/3)."
			],
			"mathematica": [
				"Table[Product[n+1-3*j, {j,1,n-1}], {n,0,25}] (* _G. C. Greubel_, Mar 29 2019 *)"
			],
			"program": [
				"(PARI) vector(25, n, n--; prod(j=1,n-1, (n+1-3*j))) \\\\ _G. C. Greubel_, Mar 29 2019",
				"(MAGMA) [1,1] cat [(\u0026*[n+1-3*j: j in [1..(n-1)]]): n in [2..25]]; // _G. C. Greubel_, Mar 29 2019",
				"(Sage) [1] + [product(n+1-3*j for j in (1..(n-1))) for n in (1..25)] # _G. C. Greubel_, Mar 29 2019"
			],
			"xref": [
				"Cf. A282627."
			],
			"keyword": "sign,easy",
			"offset": "0,4",
			"author": "_Wolfdieter Lang_, Mar 04 2017",
			"ext": [
				"More terms from _G. C. Greubel_, Mar 29 2019"
			],
			"references": 2,
			"revision": 20,
			"time": "2019-04-01T00:51:20-04:00",
			"created": "2017-03-05T12:19:59-05:00"
		}
	]
}