{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A288871",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 288871,
			"data": "5,9,14,15,22,36,25,34,52,88,43,54,76,120,208,77,90,116,168,272,480,143,158,188,248,368,608,1088,273,290,324,392,528,800,1344,2432,531,550,588,664,816,1120,1728,2944,5376,1045,1066,1108,1192,1360,1696,2368,3712,6400,11776",
			"name": "Triangle t needed for the e.g.f.s of the column sequences of A288870 with leading zeros.",
			"comment": [
				"See the triangle T = A288870. The e.g.f. of the sequence of column k (k \u003e= 0) without the leading k zeros is E(k, x) = (2*k+1)*exp(2*x) + exp(x). In order to get the e.g.f. for the column k sequence with the leading k zeros one has to integrate k times for k \u003e=1; but this will first generate unwanted fractional numbers for the first k entries (when no integration constants are taken into account). These rational polynomials of degree k to be subtracted are S(k, x) = 2^(-k)* Sum_{m=1..k} t(k,m)*x^(m-1)/(m-1)! if k \u003e=1."
			],
			"formula": [
				"t(k, m) = 2^k + k*2^m + 2^(m-1), k \u003e= m \u003e= 1, otherwise 0.",
				"O.g.f. column m: G(m, x) =x*(2*x)^(m-1)*(3 - 5*x + 2*(1 - 3*x + 2*x^2)*m)/((1-x)^2*(1-2*x)).",
				"O.g.f. G(m, x) = 1/(1-2*x) + 2^m*x/(1-x)^2 + 2^(m-1)/(1-x) - Subt(m ,x), with",
				"  Subt(m, x) = Sum_{k=0..m-1} A288870(m-1, k)*(2*x)^k."
			],
			"example": [
				"The triangle t begins:",
				"k\\m     1    2    3    4    5    6    7    8    9    10 ...",
				"1:      5",
				"2:      9   14",
				"3:     15   22   36",
				"4:     25   34   52   88",
				"5:     43   54   76  120  208",
				"6:     77   90  116  168  272  480",
				"7:    143  158  188  248  368  608  108",
				"8:    273  290  324  392  528  800 1344 2432",
				"9:    531  550  588  664  816 1120 1728 2944 5376",
				"10:  1045 1066 1108 1192 1360 1696 2368 3712 6400 11776",
				"...",
				"k = 1: E(1, x) = 3*exp(2*x) + exp(x) generates exponentially: 4, 7, 13, 25, 49, ..., the column k = 1 of T = A288870 without leading zero. Integration gives (without integration constant) (3/2)*exp(2*x) + exp(x), generating 5/2, 4, 7, 13, 25, 49, ..., therefore 5/2 = 2^(-1)* t(1,1)*x^(1-1)/(1-1)!= 2^(-1)*5*x^0 = 5/2.",
				"Column o.g.f. for m=2: G(2, x) = 1/(1-2*x) + 4*x/(1-x)^2 + 2/(1-x) - (3 + 2^1*4*x) = 2*x^2*(7-17*x+8*x^2)/((1 - 2*x)*( 1 - x)^2)."
			],
			"xref": [
				"Cf. A288870."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "1,1",
			"author": "_Wolfdieter Lang_, Jun 21 2017",
			"references": 1,
			"revision": 7,
			"time": "2017-07-10T04:18:30-04:00",
			"created": "2017-07-10T04:18:30-04:00"
		}
	]
}