{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A122141",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 122141,
			"data": "1,1,2,1,4,0,1,6,4,0,1,8,12,0,2,1,10,24,8,4,0,1,12,40,32,6,8,0,1,14,60,80,24,24,0,0,1,16,84,160,90,48,24,0,0,1,18,112,280,252,112,96,0,4,2,1,20,144,448,574,312,240,64,12,4,0,1,22,180,672,1136,840,544,320,24,30,8,0",
			"name": "Array: T(d,n) = number of ways of writing n as a sum of d squares, read along diagonals.",
			"comment": [
				"This is the transpose of the array in A286815.",
				"T(d,n) is divisible by 2d for any n != 0 iff d is a power of 2. - _Jianing Song_, Sep 05 2018"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A122141/b122141.txt\"\u003eAntidiagonals d = 1..141, flattened\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Sum_of_squares_function\"\u003eSum of squares function\u003c/a\u003e (transposed)",
				"\u003ca href=\"/index/Su#ssq\"\u003eIndex entries for sequences related to sums of squares\u003c/a\u003e"
			],
			"formula": [
				"T(n,n) = A066535(n). - _Alois P. Heinz_, Jul 16 2014"
			],
			"example": [
				"Array T(d,n) with rows d = 1,2,3... and columns n = 0,1,2,3.. reads",
				"1 2 0 0 2 0 0 0 0 2 0 ...",
				"1 4 4 0 4 8 0 0 4 4 8 ...",
				"1 6 12 8 6 24 24 0 12 30 24 ...",
				"1 8 24 32 24 48 96 64 24 104 144 ...",
				"1 10 40 80 90 112 240 320 200 250 560 ...",
				"1 12 60 160 252 312 544 960 1020 876 1560 ...",
				"1 14 84 280 574 840 1288 2368 3444 3542 4424 ...",
				"1 16 112 448 1136 2016 3136 5504 9328 12112 14112 ...",
				"1 18 144 672 2034 4320 7392 12672 22608 34802 44640 ...",
				"1 20 180 960 3380 8424 16320 28800 52020 88660 129064"
			],
			"maple": [
				"A122141 := proc(d,n) local i,cnts ; cnts := 0 ; for i from -trunc(sqrt(n)) to trunc(sqrt(n)) do if n-i^2 \u003e= 0 then if d \u003e 1 then cnts := cnts+procname(d-1,n-i^2) ; elif n-i^2 = 0 then cnts := cnts+1 ; fi ; fi ; od ; cnts ;",
				"end:",
				"for diag from 1 to 14 do for n from 0 to diag-1 do d := diag-n ; printf(\"%d,\",A122141(d,n)) ; od ; od;",
				"# second Maple program:",
				"A:= proc(d, n) option remember; `if`(n=0, 1, `if`(n\u003c0 or d\u003c1, 0,",
				"      A(d-1, n) +2*add(A(d-1, n-j^2), j=1..isqrt(n))))",
				"    end:",
				"seq(seq(A(h-n, n), n=0..h-1), h=1..14); # _Alois P. Heinz_, Jul 16 2014"
			],
			"mathematica": [
				"Table[ SquaresR[d - n, n], {d, 1, 12}, {n, 0, d - 1}] // Flatten (* _Jean-François Alcover_, Jun 13 2013 *)",
				"A[d_, n_] := A[d, n] = If[n==0, 1, If[n\u003c0 || d\u003c1, 0, A[d-1, n] + 2*Sum[A[d-1, n-j^2], {j, 1, Sqrt[n]}]]]; Table[A[h-n, n], {h, 1, 14}, {n, 0, h-1}] // Flatten (* _Jean-François Alcover_, Feb 28 2018, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A066535, A286815.",
				"Cf. A000122 (1st row), A004018 (2nd row), A005875 (3rd row), A000118 (4th row), A000132 (5th row), A000141 (6th row), A008451 (7th row), A000143 (8th row), A008452 (9th row), A000144 (10th row), A008453 (11th row), A000145 (12th row), A276285 (13th row), A276286 (14th row), A276287 (15th row), A000152 (16th row).",
				"Cf. A005843 (2nd column), A046092 (3rd column), A130809 (4th column).",
				"Cf. A010052 (1st row divides 2), A002654 (2nd row divides 4), A046897 (4th row divides 8), A008457 (8th row divides 16), A302855 (16th row divides 32), A302857 (32nd row divides 64)."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_R. J. Mathar_, Oct 29 2006",
			"references": 26,
			"revision": 40,
			"time": "2020-07-17T12:18:24-04:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}