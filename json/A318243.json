{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A318243",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 318243,
			"data": "1,4,4,7,22,9,10,58,78,20,13,112,282,224,40,16,184,702,1052,570,78,19,274,1419,3260,3335,1338,147,22,382,2514,7928,12520,9462,2968,272,25,508,4068,16460,35955,42108,24766,6312,495,28,652,6162,30584,86330,140586,128352,60976,12996,890,31,814,8877,52352",
			"name": "Triangle read by rows giving the sum of the number of k-matchings of the graphs obtained by deleting one edge and its two vertices from the ladder graph L_n = P_2 X P_n in all possible ways.",
			"comment": [
				"T(n,k) is useful for computing the number of configurations of n indistinguishable pairs placed on the vertices of P_2 X P_n such that only one such pair is joined by an edge. The g.f. given below can be proven using the calculus of the rook polynomial associated with A046741."
			],
			"link": [
				"D. Young, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL21/Young/young2.html\"\u003eThe Number of Domino Matchings in the Game of Memory\u003c/a\u003e, Journal of Integer Sequences, Vol. 21 (2018), Article 18.8.1.",
				"Donovan Young, \u003ca href=\"https://arxiv.org/abs/1905.13165\"\u003eGenerating Functions for Domino Matchings in the 2 * k Game of Memory\u003c/a\u003e, arXiv:1905.13165 [math.CO], 2019. Also in \u003ca href=\"https://www.emis.de/journals/JIS/VOL22/Young/young13.html\"\u003eJ. Int. Seq.\u003c/a\u003e, Vol. 22 (2019), Article 19.8.7."
			],
			"formula": [
				"G.f.: (1 + 2*t*z + 2*z/(1-t*z)^2)*(1-t*z)^2/(1 - z - 2*t*z - t*z^2 + t^3*z^3)^2."
			],
			"example": [
				"The first few rows of T(n,k) are",
				"   1;",
				"   4,   4;",
				"   7,  22,   9;",
				"  10,  58,  78,  20;",
				"  13, 112, 282, 224,  40;",
				"For n = 2, the four ways of deleting an edge and its vertices from P_2 X P_2 all yield a graph with two vertices joined by an edge. This graph has 1 0-matching and 1 1-matching, thus T(2,k) = 4, 4."
			],
			"mathematica": [
				"CoefficientList[Normal[Series[(1 + 2*t*z + 2*z/(1-t*z)^2)*(1-t*z)^2/(1 - z - 2*t*z - t*z^2 + t^3*z^3)^2,{z,0,10}]],{z,t}]//MatrixForm"
			],
			"xref": [
				"Cf. A046741, A318244, A318267, A318268, A318269, A318270."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Donovan Young_, Aug 22 2018",
			"references": 7,
			"revision": 33,
			"time": "2020-03-18T11:07:17-04:00",
			"created": "2018-09-19T12:27:45-04:00"
		}
	]
}