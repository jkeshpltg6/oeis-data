{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A215413",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 215413,
			"data": "1,1,3,-2,3,-6,10,-12,15,-22,30,-36,44,-60,78,-96,117,-150,190,-228,276,-340,420,-504,603,-732,885,-1052,1245,-1488,1770,-2088,2454,-2902,3420,-3996,4666,-5460,6378,-7400,8583,-9972,11566,-13344,15378,-17752,20448",
			"name": "McKay-Thompson series of class 18C for the Monster group with a(0) = 1.",
			"comment": [
				"A058533, A123676, A215412, A058644, A215413 are all essentially the same sequence. - _N. J. A. Sloane_, Aug 09 2012",
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A215413/b215413.txt\"\u003eTable of n, a(n) for n = -1..1000\u003c/a\u003e",
				"D. Ford, J. McKay and S. P. Norton, \u003ca href=\"http://dx.doi.org/10.1080/00927879408825127\"\u003eMore on replicable functions\u003c/a\u003e, Commun. Algebra 22, No. 13, 5175-5193 (1994). See Table 4 18C.",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of psi(q) / (q * psi(q^9)) + 3 * q * psi(q^9) / psi(q) in powers of q where psi() is a Ramanujan theta function.",
				"Expansion of c(q) * b(q^3) / (c(q^2) * b(q^2) * c(q^6) * b(q^6))^(1/2) in powers of q where b(), c() are cubic AGM theta functions.",
				"Expansion of eta(q^3)^6 / (eta(q) * eta(q^2) * eta(q^6)^2 * eta(q^9) * eta(q^18)) in powers of q.",
				"Euler transform of period 18 sequence [ 1, 2, -5, 2, 1, -2, 1, 2, -4, 2, 1, -2, 1, 2, -5, 2, 1, 0, ...].",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2)) where f(u, v) = (v - u)^2 - u * (u - 4) * (v - 3).",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (108 t)) = 4 g(t) where q = exp(2 Pi i t) and g() is g.f. for A123629.",
				"a(n) = A058533(n) = A123676(n) = A215412(n) unless n=0.",
				"a(n) ~ -(-1)^n * exp(2*Pi*sqrt(n)/3) / (2*sqrt(3)*n^(3/4)). - _Vaclav Kotesovec_, Sep 08 2017"
			],
			"example": [
				"1/q + 1 + 3*q - 2*q^2 + 3*q^3 - 6*q^4 + 10*q^5 - 12*q^6 + 15*q^7 - 22*q^8 + ..."
			],
			"mathematica": [
				"QP = QPochhammer; s = QP[q^3]^6 / (QP[q] * QP[q^2] * QP[q^6]^2 * QP[q^9] * QP[q^18]) + O[q]^50; CoefficientList[s, q] (* _Jean-François Alcover_, Nov 14 2015, adapted from PARI *)"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c-1, 0, n++; A = x * O(x^n); polcoeff( eta(x^3 + A)^6 / (eta(x + A) * eta(x^2 + A) * eta(x^6 + A)^2 * eta(x^9 + A) * eta(x^18 + A)), n))}"
			],
			"xref": [
				"Cf. A058533, A123629, A123676, A215412."
			],
			"keyword": "sign",
			"offset": "-1,3",
			"author": "_Michael Somos_, Aug 09 2012",
			"references": 6,
			"revision": 30,
			"time": "2021-03-12T22:24:46-05:00",
			"created": "2012-08-09T13:35:43-04:00"
		}
	]
}