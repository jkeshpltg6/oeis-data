{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A348226",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 348226,
			"data": "2,2,43,2,45481,2,65484343,186914543201,50006393431,2",
			"name": "a(n) is the smallest positive integer that when expressed in bases 2 to n, but read in base n, is always prime.",
			"comment": [
				"a(n)=2 whenever n is prime.",
				"Proof:",
				"Let n be a prime number.",
				"2 expressed in any base larger than 2 is still 2, which is prime.",
				"2 expressed in base 2 is 10. And 10 read in base n is 1*n + 0 = n, which is prime.",
				"The sequence, even when prime indexes are omitted, is not necessarily increasing.",
				"Proof: a(9) \u003e a(10)."
			],
			"example": [
				"a(4) = 43, because",
				"  43 is prime",
				"  43 in base 3 is 1121 = 1*3^3 + 1*3^2 + 2*3 + 1 and",
				"                         1*4^3 + 1*4^2 + 2*4 + 1 = 89, which is prime;",
				"  43 in base 2 is 101011 = 1*2^5 + 0*2^4 + 1*2^3 + 0*2^2 + 1*2^1 + 1 and",
				"                           1*4^5 + 0*4^4 + 1*4^3 + 0*4^2 + 1*4^1 + 1 = 1093, which is prime;",
				"and 43 is the smallest positive integer with this property.",
				"a(10) = 50006393431",
				"      = 153060758677_9",
				"      = 564447201127_8",
				"      = 3420130221331_7",
				"      = 34550030320411_6",
				"      = 1304403114042211_5",
				"      = 232210213100021113_4",
				"      = 11210002000211222202121_3",
				"      = 101110100100100111010000001001010111_2;",
				"if we read these numbers as base-10 numbers, they are all prime. And 50006393431 is the smallest positive integer with this property."
			],
			"program": [
				"(PARI) isok(k, n) = {for (b=2, n, if (! ispseudoprime(fromdigits(digits(k, b), n)), return (0));); return (1);}",
				"a(n) = my(k=1); while (!isok(k, n), k++); k; \\\\ _Michel Marcus_, Oct 09 2021",
				"(Python)",
				"from gmpy2 import digits, is_prime, next_prime",
				"def A348226(n): # code assumes n \u003c= 63 or n is prime",
				"    if is_prime(n):",
				"        return 2",
				"    p = 2",
				"    while True:",
				"        for i in range(n-1,1,-1):",
				"            s = digits(p,i)",
				"            if not is_prime(int(s,n)):",
				"                break",
				"        else:",
				"            return p",
				"        p = next_prime(p) # _Chai Wah Wu_, Nov 19 2021"
			],
			"keyword": "nonn,base,more",
			"offset": "2,1",
			"author": "_Jesús Bellver Arnau_, Oct 09 2021",
			"references": 1,
			"revision": 31,
			"time": "2021-11-22T22:51:07-05:00",
			"created": "2021-11-19T08:18:14-05:00"
		}
	]
}