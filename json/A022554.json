{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A022554",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 22554,
			"data": "0,1,2,3,5,7,9,11,13,16,19,22,25,28,31,34,38,42,46,50,54,58,62,66,70,75,80,85,90,95,100,105,110,115,120,125,131,137,143,149,155,161,167,173,179,185,191,197,203,210,217,224,231,238,245,252,259,266,273,280",
			"name": "a(n) = Sum_{k=0..n} floor(sqrt(k)).",
			"comment": [
				"Partial sums of A000196. - _Michel Marcus_, Mar 01 2016",
				"It seems that 197 is the largest prime in this sequence. Tested for n \u003c= 10^11. - _Hugo Pfoertner_, Oct 26 2020"
			],
			"reference": [
				"R. L. Graham, D. E. Knuth, and O. Patashnik, Concrete Mathematics, 2nd Edition, Addison-Wesley, 1994, Eq. 3.27 on page 87.",
				"D. E. Knuth, The Art of Computer Programming, Vol. 1, 3rd Edition, Addison-Wesley, 1997, Ex. 43 of section 1.2.4.",
				"K. H. Rosen, Discrete Mathematics and Its Application, 6th Edition, McGraw-Hill, 2007, Ex. 25 of section 2.4."
			],
			"link": [
				"David A. Corneth, \u003ca href=\"/A022554/b022554.txt\"\u003eTable of n, a(n) for n = 0..9999\u003c/a\u003e (first 1001 terms from G. C. Greubel)",
				"M. Griffiths, \u003ca href=\"http://www.jstor.org/stable/3621862\"\u003eMore sums involving the floor function\u003c/a\u003e, Math. Gaz., 86 (2002), 285-287.",
				"Michael Penn, \u003ca href=\"https://www.youtube.com/watch?v=kpW2V0hD0Ro\"\u003eWringing out one more result.\u003c/a\u003e, YouTube video, 2021."
			],
			"formula": [
				"a(0)=0, a(1)=1; a(n) = 2*a(n-1) - a(n-2) if n is not a perfect square; a(n) = 2*a(n-1) - a(n-2) + 1 if n is a perfect square.",
				"a(n) = floor(sqrt(n)) * (n-1/6*(2*floor(sqrt(n))+5)*(floor(sqrt(n))-1)). - Yong Kong (ykong(AT)curagen.com), Mar 10 2001",
				"a(n) = 2/3 n^(3/2) - 1/2 n + O(sqrt(n)). - _Charles R Greathouse IV_, Jan 12 2012",
				"G.f.: Sum_{k\u003e=1} x^(k^2)/(1 - x)^2. - _Ilya Gutkovskiy_, Dec 22 2016"
			],
			"example": [
				"G.f. = x + 2*x^2 + 3*x^3 + 5*x^4 + 7*x^5 + 9*x^6 + 11*x^7 + 13*x^8 + 16*x^9 + ..."
			],
			"maple": [
				"Sum(floor(sqrt(k)),k=0..n)"
			],
			"mathematica": [
				"Accumulate[Floor[Sqrt[Range[0,60]]]] (* _Harvey P. Dale_, Feb 16 2011 *)",
				"Table[Sum[Floor[Sqrt[i]], {i,0,n}], {n,0,50}] (* _G. C. Greubel_, Dec 22 2016 *)"
			],
			"program": [
				"(PARI) a(n)=sum(k=1,n,sqrtint(k)) \\\\ _Charles R Greathouse IV_, Jan 12 2012",
				"(PARI) a(n)=my(k=sqrtint(n));k*(n-(2*k+5)/6*(k-1)) \\\\ _Charles R Greathouse IV_, Jan 12 2012",
				"(MAGMA) [\u0026+[Floor(Sqrt(k)): k in [0..n]]: n in [0..50]]; // _G. C. Greubel_, Feb 26 2018"
			],
			"xref": [
				"Cf. A000196."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "Michel Tixier (tixier(AT)dyadel.net)",
			"ext": [
				"More terms from Yong Kong (ykong(AT)curagen.com), Mar 10 2001"
			],
			"references": 14,
			"revision": 57,
			"time": "2021-02-03T23:41:15-05:00",
			"created": "1998-06-14T03:00:00-04:00"
		}
	]
}