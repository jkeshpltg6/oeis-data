{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A343684",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 343684,
			"data": "1,1,2,1,6,2,1,8,162,2,1,12,548,17538,2,1,14,980,33704228,2341874005255914498,2,1,18,1386,139300292,2661014892563136514212,87117602843745867025899669792849494278146,2",
			"name": "Triangle read by rows: T(n,k). Row n lists the numerators of a finite sum of fractions which results from Sum_{j\u003e=1} 1/2^(A014682^n(j)).",
			"comment": [
				"The denominators are given by 2^(3^k)-1.",
				"The fact that A014682^n(m+k*2^n) is of the form r+k*3^s for m = 1..k^2-1 allows an easy development as a sum of geometric series.",
				"We define A014682^0(n) = n and A014682^1(n) = A014682(n), A014682^2(n) = A014682(A014682(n))."
			],
			"example": [
				"Let f(n) = Sum_{j\u003e=1} 1/2^(A014682^n(j)).",
				"f(0) = 1, f(1) = 9/7, f(2) = 951/511, f(3) = 47165693/19173961, ...",
				"So triangle begins:",
				"  1;                    f(0) = 1/1",
				"  1, 2;                 f(1) = 1/1 + 2/7",
				"  1, 6,  2;             f(2) = 1/1 + 6/7 + 2/511",
				"  1, 8,  162, 2;        f(3) = 1/1 + 8/7 + 162/511 + 2/(2^27-1)",
				"  1, 12, 548, 17538, 2; f(4) = 1/1 + 12/7 + 548/511 + 17538/(2^27-1) + 2/(2^81-1)",
				"  ...",
				"Sum_{j\u003e=1} 1/2^(A014682^2(j)) = Sum_{j\u003e=1} 1/2^j + Sum_{j\u003e=1} 1/2^(1+j*(3^1)) + Sum_{j\u003e=1} 1/2^(2+j*(3^1)) + Sum_{j\u003e=1} 1/2^(8+j*(3^2))."
			],
			"program": [
				"(MATLAB)",
				"function a = A343684( row_n )",
				"    a = zeros(1,1+row_n);",
				"    for k = 1:2^row_n",
				"        first = A014682_exp(k,row_n);",
				"        second = A014682_exp(k+2^row_n,row_n);",
				"        e = round(log2(second-first)/log2(3));",
				"        a(e+1) = a(e+1)+2^(3^e-first);",
				"    end",
				"end",
				"function [ out ] = A014682_exp( in, num )",
				"    out = in;",
				"    for n = 1:num",
				"        out = A014682( out );",
				"    end",
				"end",
				"function [ out ] = A014682( in )",
				"    if mod(in,2) == 0",
				"        out = in/2;",
				"    else",
				"        out = ((in*3) + 1)/2;",
				"    end",
				"end"
			],
			"xref": [
				"Cf. A014682."
			],
			"keyword": "nonn,tabl,frac",
			"offset": "0,3",
			"author": "_Thomas Scheuerle_, Apr 26 2021",
			"references": 1,
			"revision": 50,
			"time": "2021-08-21T16:29:00-04:00",
			"created": "2021-05-31T22:00:32-04:00"
		}
	]
}