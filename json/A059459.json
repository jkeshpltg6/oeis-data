{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A059459",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 59459,
			"data": "2,3,7,5,13,29,31,23,19,17,8209,8273,10321,2129,2131,83,67,71,79,1103,1039,1031,1063,1061,1069,263213,263209,263201,265249,265313,264289,280673,280681,280697,280699,280703,280639,280607,280603,280859,280843,281867,265483,265547,265579,265571,266083,266081,266089,266093,266029",
			"name": "a(1) = 2; a(n+1) is obtained by writing a(n) in binary and trying to complement just one bit, starting with the least significant bit, until a new prime is reached.",
			"comment": [
				"This is the lexicographically least (in positions of the flipped bits) such sequence.",
				"It is not known if the sequence is infinite.",
				"\"The prime maze - consider the prime numbers in base 2, starting with the smallest prime (10)2. One can move to another prime number by either changing only one digit of the number, or adding a 1 to the front of the number. Can we reach 11 = (1011)2.? 3331? The Mersennes?\" (See 'Prime Links + +'.) If we start at 11 and exclude terms 2 and 3 we get terms 11, 43, 41, and so on. This is the opposite parity sequence.",
				"a(130), if it exists, is greater than 2^130000. - _Charles R Greathouse IV_, Jan 02 2014",
				"a(130) is equal to a(129) + 2^400092. - _Giovanni Resta_, Jul 19 2017"
			],
			"link": [
				"T. D. Noe and Charles R Greathouse IV, \u003ca href=\"/A059459/b059459.txt\"\u003eTable of n, a(n) for n = 1..129\u003c/a\u003e (first 104 terms from Noe)",
				"Chris K. Caldwell, \u003ca href=\"http://primes.utm.edu/links/curiosities/problems_and_puzzles/\"\u003ePrime Links + +\u003c/a\u003e",
				"W. Paulsen, \u003ca href=\"https://web.archive.org/web/20090116121117/http://www.csm.astate.edu/~wpaulsen/primemaze/pmaze.html\"\u003eThe Prime Number Maze\u003c/a\u003e, Web Pages.",
				"W. Paulsen, \u003ca href=\"http://www.fq.math.ca/Scanned/40-3/paulsen.pdf\"\u003eThe Prime Number Maze\u003c/a\u003e, Fib. Quart., 40 (2002), 272-279.",
				"Carlos Rivera, \u003ca href=\"http://www.primepuzzles.net/problems/prob_025.htm\"\u003eProblem 25.- William Paulsen's Prime Numbers Maze\u003c/a\u003e"
			],
			"maple": [
				"A059459search := proc(a,upto_bit,upto_length) local i,n,t; if(nops(a) \u003e= upto_length) then RETURN(a); fi; t := a[nops(a)]; for i from 0 to upto_bit do n := XORnos(t,(2^i)); if(isprime(n) and (not member(n,a))) then print([op(a), n]); RETURN(A059459search([op(a), n],upto_bit,upto_length)); fi; od; RETURN([op(a),`and no more`]); end;",
				"E.g., call as: A059459search([2],128,200);"
			],
			"mathematica": [
				"maxBits = 2^11; ClearAll[a]; a[1] = 2; a[n_] := a[n] = If[ PrimeQ[ a[n-1] ], bits = PadLeft[ IntegerDigits[ a[n-1], 2], maxBits]; For[i = 1, i \u003c= maxBits, i++, bits2 = bits; bits2[[-i]] = 1 - bits[[-i]]; If[ i == maxBits, Print[ \"maxBits reached\" ]; Break[], If[ PrimeQ[an = FromDigits[ bits2, 2]] \u0026\u0026 FreeQ[ Table[ a[k], {k, 1, n-1}], an], Return[an] ] ] ], 0]; Table[ a[n], {n, 129}] (* _Jean-François Alcover_, Jan 17 2012 *)",
				"f[lst_List] := Block[{db2 = IntegerDigits[lst[[-1]], 2]}, exp = Length@ db2; While[pp = db2; pp[[exp]] = If[OddQ@db2[[exp]], 0, 1]; pp = FromDigits[pp, 2]; !PrimeQ[pp] || MemberQ[lst, pp], exp--; If[exp == 0, exp++; PrependTo[db2, 0]]]; Append[lst, pp]]; Nest[f, {2}, 128] (* _Robert G. Wilson v_, Jul 17 2017 *)"
			],
			"program": [
				"(PARI) step(n)=my(k,t); while(vecsearch(v, t=bitxor(n,1\u003c\u003ck)) || !ispseudoprime(t=bitxor(n, 1\u003c\u003ck)), k++); v=Set(concat(v,t)); t",
				"u=v=[2]; u=concat(u,step(2)); for(i=3,129,u=concat(u,step(u[#u]));print(#u\" \"u[#u])) \\\\ _Charles R Greathouse IV_, Jan 02 2014"
			],
			"xref": [
				"Cf. A059458 (for this sequence written in binary), A059471. A strictly ascending analog: A059661, positions of the flipped bits: A059663."
			],
			"keyword": "nonn,base,nice",
			"offset": "1,1",
			"author": "_Gregory Allen_, Feb 02 2001",
			"ext": [
				"More terms and Maple program from _Antti Karttunen_, Feb 03 2001, who remarks that he was able to extend the sequence to the 104th term 151115727453207491916143 using the bit-flip-limit 128."
			],
			"references": 9,
			"revision": 51,
			"time": "2017-07-19T06:22:20-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}