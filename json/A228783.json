{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A228783",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 228783,
			"data": "2,0,1,1,0,-3,0,1,-1,1,0,4,0,-1,-1,-1,1,0,-7,0,14,0,-7,0,1,2,1,-1,0,8,0,-18,0,8,0,-1,1,2,-3,-1,1,0,-8,0,6,0,-1,0,0,-1,3,3,-4,-1,1,0,12,0,-67,0,96,0,-52,0,12,0,-1,-2,3,1,-1,0,-15,0,140,0,-378,0,450,0,-275,0,90,0,-15,0,1",
			"name": "Table of coefficients of the algebraic number s(2*l) = 2*sin(Pi/2*l) as a polynomial in powers of rho(2*l) = 2*cos(Pi/(2*l)) if l is even and of rho(l) = 2*cos(Pi/l) if l is odd (reduced version).",
			"comment": [
				"In the regular (2*l)-gon inscribed in a circle of radius R the length ratio side/R is s(2*l) = 2*sin(Pi/(2*l)). This can be written as a polynomial in the length ratio (smallest diagonal)/side given by rho(2*l) = 2*cos(Pi/(2*l)). (For the 2-gon there is no such diagonal and rho(2) = 0). This leads, in a first step, to the triangle A127672 (see the Oct 05 2013 comment there referring also to the bisections signed A111125 and A127677). Because the minimal polynomial of the algebraic number rho(2*l) of degree delta(2*l) = A055034(2*l), called C(2*l,x) (with coefficients given in A187360) one can eliminate all powers rho(2*l)^k with k \u003e= delta(2*l) by using C(2*l,rho(2*l)) = 0. Furthermore, because for odd l only even powers of rho(2*l) appear, but rho(2*l)^2 = 2 + rho(l), one will obtain a reduced table for s(2*l) with powers rho(2*l)^(2*k+1), k= 0, ..., (delta(2*l)-2)/2 if l is even, and with powers rho(l)^m, m=0, ... , delta(l)-1 if l is odd.",
				"This leads to a reduction of the triangle A127672, when applied for the s(2*l) computation, giving the present table with row length delta(4*L) = A055034(4*L) = phi(8*L)/2 if l =2*L, if L \u003e= 1, and  phi(2*L+1)/2 = A055035(4*L+2), if l = 2*L + 1,  L \u003e= 1, where phi(n) = A000010(n) (Euler totient).",
				"This table gives the coefficients of s(2*l) in the power basis of the algebraic number field Q(rho(2*l)) of degree delta(2*l) = A055034(2*l) if l is even, and in Q(rho(l)) of degree delta(2*l)/2 if l is odd. s(2) and s(6) are rational integers of degree 1.",
				"Thanks go to Seppo Mustonen whose question about the square of the sum of all length in a regular n-gon, led me to this computation.",
				"If l = 2*L+1, L \u003e= 0, that is n == 2 (mod 4), one can obtain s(2*l) more directly in powers of rho(l) by s(2*l) = R(l-1, rho(l)) (mod C(l,rho(l))), with the monic (except for l=1) Chebyshev T-polynomials, called R, in A127672, and the C polynomials from A187360. - _Wolfdieter Lang_, Oct 10 2013"
			],
			"formula": [
				"a(2*L,m) = [x^m](s(4*L,x)(mod C(4*L,x))), with s(4*L,x) = sum((-1)^(L-1-s)*A111125(L-1,s)*x^(2*s+1),s=0..L-1), L \u003e= 1, m =0, ..., delta(4*L)-1, and",
				"a(2*L+1,m) = [x^m](s(4*L+2,x)(mod C(2*L+1,x))), with s(4*L+2,x) = sum(A127677(L,s)*(2+x)^(L-s)),s=0..L) (with s(2,x) = 2 for L = 0), L \u003e= 0,  m = 0, ..., delta(4*L+2)/2, with delta(n) = A055034(2*l)."
			],
			"example": [
				"The table a(l,m), with n = 2*l, begins:",
				"n,  l \\m  0   1   2    3   4   5   6    7   8   9  10  11 ...",
				"2   1:    2",
				"4   2:    0   1",
				"6   3:    1",
				"8   4:    0  -3   0    1",
				"10  5:   -1   1",
				"12  6:    0   4   0   -1",
				"14  7:   -1  -1   1",
				"16  8:    0  -7   0   14   0  -7   0    1",
				"18  9:    2   1  -1",
				"20 10:    0   8   0  -18   0   8   0   -1",
				"22 11:    1   2  -3   -1   1",
				"24 12:    0  -8   0    6   0  -1   0    0",
				"26 13:   -1   3   3   -4  -1   1",
				"28 14:    0  12   0  -67   0  96   0  -52  0  12  0  -1",
				"30 15:   -2   3   1   -1",
				"...",
				"n = 8, l = 4:  s(8)  = -3*rho(8) + rho(8)^3 = -3*sqrt(2 + sqrt(2)) + (sqrt(2 + sqrt(2)))^3 = (sqrt(2) - 1)*sqrt(2 + sqrt(2)).",
				"n = 10, l = 5:  s(10) =  -1 + rho(5), where rho(5) = tau = (1 + sqrt(5))/2, the golden section."
			],
			"xref": [
				"Cf. A127672, A111125, A127677, A055034, A187360, A228785 (odd n case), A228786 (minimal polynomials)."
			],
			"keyword": "sign,tabf",
			"offset": "1,1",
			"author": "_Wolfdieter Lang_, Oct 06 2013",
			"references": 4,
			"revision": 11,
			"time": "2013-10-19T02:03:42-04:00",
			"created": "2013-10-08T02:47:12-04:00"
		}
	]
}