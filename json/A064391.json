{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A064391",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 64391,
			"data": "1,1,0,0,1,0,0,0,1,1,0,0,1,0,0,1,1,0,1,0,1,0,1,0,1,1,0,1,0,1,1,1,0,1,0,1,1,0,1,1,1,1,1,1,1,1,1,0,1,1,0,1,1,1,1,2,1,2,1,1,1,1,0,1,1,0,1,1,2,1,2,2,2,2,2,1,2,1,1,0,1,1,0,1,1,2,1,3,2,3,2,3,2,3,1,2,1,1,0,1,1,0,1,1,2",
			"name": "Triangle T(n,k) with zeroth row {1} and row n for n \u003e= 1 giving number of partitions of n with crank k, for -n \u003c= k \u003c= n.",
			"comment": [
				"For a partition p, let l(p) = largest part of p, w(p) = number of 1's in p, m(p) = number of parts of p larger than w(p). The crank of p is given by l(p) if w(p) = 0, otherwise m(p)-w(p).",
				"n-th row contains 2n+1 terms."
			],
			"link": [
				"G. E. Andrews and F. Garvan, \u003ca href=\"http://dx.doi.org/10.1090/S0273-0979-1988-15637-6\"\u003eDyson's crank of a partition\u003c/a\u003e, Bull. Amer. Math. Soc., 18 (1988), 167-171.",
				"F. Garvan, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-1988-0920146-8\"\u003eNew combinatorial interpretations of Ramanujan's partition congruences mod 5, 7 and 11\u003c/a\u003e, Trans. Amer. Math. Soc., 305 (1988), 47-77."
			],
			"formula": [
				"G.f. for k-th column is Sum(m\u003e=1, (-1)^m*x^(k*m)*(x^((m^2+m)/2)-x^((m^2-m)/2)))/Product(m\u003e=1, 1-x^m). - _Vladeta Jovovic_, Dec 22 2004"
			],
			"example": [
				"{T(20, k), -20 \u003c= k \u003c=20} = {1, 0, 1, 1, 2, 2, 4, 4, 7, 8, 12, 13, 19, 20, 26, 28, 34, 34, 39, 38, 41, 38, 39, 34, 34, 28, 26, 20, 19, 13, 12, 8, 7, 4, 4, 2, 2, 1, 1, 0, 1}.",
				"From _Omar E. Pol_, Mar 04 2012: (Start)",
				"Triangle begins:",
				".                          1;",
				".                       1, 0, 0;",
				".                    1, 0, 0, 0, 1;",
				".                 1, 0, 0, 1, 0, 0, 1;",
				".              1, 0, 1, 0, 1, 0, 1, 0, 1;",
				".           1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1;",
				".        1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1;",
				".     1, 0, 1, 1, 1, 1, 2, 1, 2, 1, 1, 1, 1, 0, 1;",
				".  1, 0, 1, 1, 2, 1, 2, 2, 2, 2, 2, 1, 2, 1, 1, 0, 1;",
				"1, 0, 1, 1, 2, 1, 3, 2, 3, 2, 3, 2, 3, 1, 2, 1, 1, 0, 1;",
				"(End)"
			],
			"mathematica": [
				"max = 12; f[k_ /; k \u003c 0] := f[-k]; f[k_] := Sum[(-1)^m*x^(k*m)*(x^((m^2 + m)/2) - x^((m^2 - m)/2)), {m, 1, max}]/Product[1 - x^m, {m, 1, max}]; t = Table[ Series[f[k], {x, 0, max}] // CoefficientList[#, x]\u0026, {k, -(max-2), max-2}] // Transpose; Table[If[n == 2, {1, 0, 0}, Table[t[[n, k]], {k, max-n, max+n-2}]], {n, 1, max-1}] // Flatten (* _Jean-François Alcover_, Apr 11 2013, after _Vladeta Jovovic_ *)"
			],
			"program": [
				"(Sage)",
				"for n in (0..9): # computes the sequence as a triangle",
				"    a = [p.crank() for p in Partitions(n)]",
				"    [a.count(k) for k in (-n..n)] # _Peter Luschny_, Sep 15 2014"
			],
			"xref": [
				"Cf. A001522, A064410, A064428.",
				"Row sums give A000041. - _Omar E. Pol_, Mar 04 2012"
			],
			"keyword": "nonn,tabf,nice,easy",
			"offset": "0,56",
			"author": "_N. J. A. Sloane_, Sep 29 2001",
			"ext": [
				"More terms from _Vladeta Jovovic_, Sep 29 2001"
			],
			"references": 10,
			"revision": 41,
			"time": "2014-09-16T05:48:04-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}