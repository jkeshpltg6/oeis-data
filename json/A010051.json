{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A010051",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 10051,
			"data": "0,1,1,0,1,0,1,0,0,0,1,0,1,0,0,0,1,0,1,0,0,0,1,0,0,0,0,0,1,0,1,0,0,0,0,0,1,0,0,0,1,0,1,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,1,0,1,0,0,0,0,0,1,0,0,0,1,0,1,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,1,0,1,0",
			"name": "Characteristic function of primes: 1 if n is prime, else 0.",
			"comment": [
				"The following sequences all have the same parity (with an extra zero term at the start of A010051): A010051, A061007, A035026, A069754, A071574. - _Jeremy Gardiner_, Aug 09 2002",
				"Hardy and Wright prove that the real number 0.011010100010... is irrational. See Nasehpour link. - _Michel Marcus_, Jun 21 2018",
				"The spectral components (excluding the zero frequency) of the Fourier transform of the partial sequences {a(j)} with j=1..n and n an even number, exhibit a remarkable symmetry with respect to the central frequency component at position 1 + n/4. See the Fourier spectrum of the first 2^20 terms in Links, Comments in A289777, and Conjectures in A001223 of Sep 01 2019. It also appears that the symmetry grows with n. - _Andres Cicuttin_, Aug 23 2020"
			],
			"reference": [
				"J.-P. Allouche and J. Shallit, Automatic Sequences, Cambridge Univ. Press, 2003, p. 3.",
				"V. Brun, Über das Goldbachsche Gesetz und die Anzahl der Primzahlpaare, Arch. Mat. Natur. B, 34, no. 8, 1915.",
				"G. H. Hardy and E. M. Wright, An Introduction to the Theory of Numbers, Oxford University Press, London, 1975.",
				"Clifford A. Pickover, A Passion for Mathematics, Wiley, 2005; see p. 65."
			],
			"link": [
				"N. J. A. Sloane and Daniel Forgues, \u003ca href=\"/A010051/b010051.txt\"\u003eTable of n, a(n) for n = 1..100000\u003c/a\u003e (first 10000 terms from N. J. A. Sloane)",
				"Andres Cicuttin, \u003ca href=\"/A010051/a010051.png\"\u003eFourier spectrum of the first 2^20 terms of the characteristic function of primes\u003c/a\u003e",
				"Y. Motohashi, \u003ca href=\"https://arxiv.org/abs/math/0505521\"\u003eAn overview of the Sieve Method and its History\u003c/a\u003e, arXiv:math/0505521 [math.NT], 2005-2006.",
				"Peyman Nasehpour, \u003ca href=\"https://jac.ut.ac.ir/article_76471.html\"\u003eA Simple Criterion for Irrationality of Some Real Numbers\u003c/a\u003e, Journal of Algorithms and Computation, Vol. 52, No. 1 (2020), pp. 97-104, \u003ca href=\"https://arxiv.org/abs/1806.07560\"\u003epreprint\u003c/a\u003e, arXiv:1806.07560 [math.AC], 2018.",
				"J. L. Ramírez, G. N. Rubiano, \u003ca href=\"http://www.mathematica-journal.com/2014/02/properties-and-generalizations-of-the-fibonacci-word-fractal/\"\u003eProperties and Generalizations of the Fibonacci Word Fractal\u003c/a\u003e, The Mathematica Journal, Vol. 16 (2014).",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PrimeNumber.html\"\u003ePrime Number\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PrimeConstant.html\"\u003ePrime Constant\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PrimeZetaFunction.html\"\u003ePrime zeta function primezeta(s)\u003c/a\u003e.",
				"\u003ca href=\"/index/Ch#char_fns\"\u003eIndex entries for characteristic functions\u003c/a\u003e"
			],
			"formula": [
				"a(n) = floor(cos(Pi*((n-1)! + 1)/n)^2) for n \u003e= 2. - Antonio G. Astudillo (afg_astudillo(AT)hotmail.com), Nov 07 2002",
				"Let M(n) be the n X n matrix m(i, j) = 0 if n divides ij + 1, m(i, j) = 1 otherwise; then for n \u003e 0 a(n) = -det(M(n)). - _Benoit Cloitre_, Jan 17 2003",
				"n \u003e= 2, a(n) = floor(phi(n)/(n - 1)) = floor(A000010(n)/(n - 1)). - _Benoit Cloitre_, Apr 11 2003",
				"a(n) = Sum_{d|gcd(n, A034386(n))} mu(d). [Brun]",
				"a(m*n) = a(m)*0^(n - 1) + a(n)*0^(m - 1). - _Reinhard Zumkeller_, Nov 25 2004",
				"a(n) = 1 if n has no divisors other than 1 and n, and 0 otherwise. - _Jon Perry_, Jul 02 2005",
				"Dirichlet generating function: Sum_{n \u003e= 1} a(n)/n^s = primezeta(s), where primezeta is the prime zeta function. - _Franklin T. Adams-Watters_, Sep 11 2005",
				"a(n) = (n-1)!^2 mod n. - _Franz Vrabec_, Jun 24 2006",
				"a(n) = A047886(n, 1). - _Reinhard Zumkeller_, Apr 15 2008",
				"Equals A051731 (the inverse Möbius transform) * A143519. - _Gary W. Adamson_, Aug 22 2008",
				"a(n) = A051731((n + 1)! + 1, n) from Wilson's theorem: n is prime if and only if (n + 1)! is congruent to -1 mod n. - _N-E. Fahssi_, Jan 20 2009, Jan 29 2009",
				"a(n) = A166260/A001477. - _Mats Granvik_, Oct 10 2009",
				"a(n) = 0^A070824, where 0^0=1. - _Mats Granvik_, _Gary W. Adamson_, Feb 21 2010",
				"It appears that a(n) = (H(n)*H(n + 1)) mod n, where H(n) = n!*Sum_{k=1..n} 1/k = A000254(n). - _Gary Detlefs_, Sep 12 2010",
				"Dirichlet generating function: log( Sum_{n \u003e= 1} 1/(A112624(n)*n^s) ). - _Mats Granvik_, Apr 13 2011",
				"a(n) = A100995(n) - sqrt(A100995(n)*A193056(n)). - _Mats Granvik_, Jul 15 2011",
				"a(n) * (2 - n mod 4) = A151763(n). - _Reinhard Zumkeller_, Oct 06 2011",
				"(n - 1)*a(n) = ( (2*n + 1)!! * Sum_{k=1..n}(1/(2*k + 1))) mod n, n \u003e 2. - _Gary Detlefs_, Oct 07 2011",
				"For n \u003e 1, a(n) = floor(1/A001222(n)). - _Enrique Pérez Herrero_, Feb 23 2012",
				"a(n) = mu(n) * Sum_{d|n} mu(d)*omega(d), where mu is A008683 and omega A001222 or A001221 indistinctly. - _Enrique Pérez Herrero_, Jun 06 2012",
				"a(n) = A003418(n+1)/A003418(n) - A217863(n+1)/A217863(n) = A014963(n) - A072211(n). - _Eric Desbiaux_, Nov 25 2012",
				"For n \u003e 1, a(n) = floor(A014963(n)/n). - _Eric Desbiaux_, Jan 08 2013",
				"a(n) = ((abs(n-2))! mod n) mod 2. - _Timothy Hopper_, May 25 2015",
				"a(n) = abs(F(n)) - abs(F(n)-1/2) - abs(F(n)-1) + abs(f(n)-3/2), where F(n) = Sum_{m=2..n+1} (abs(1-(n)mod m) - abs(1/2-(n)mod m) + 1/2), n \u003e 0. F(n) = 1 if n is prime, \u003e 1 otherwise, except F(1) = 0. a(n) = 1 if F(n) = 1, 0 otherwise. - _Timothy Hopper_, Jun 16 2015",
				"For n \u003e 4, a(n) = (n-2)! mod n. - _Thomas Ordowski_, Jul 24 2016",
				"From _Ilya Gutkovskiy_, Jul 24 2016: (Start)",
				"G.f.: A(x) = Sum_{n\u003e=1} x^A000040(n) = B(x)*(1 - x), where B(x) is the g.f. for A000720.",
				"a(n) = floor(2/A000005(n)), for n\u003e1. (End)",
				"a(n) = pi(n) - pi(n-1) = A000720(n) - A000720(n-1), for n\u003e=1. - _G. C. Greubel_, Jan 05 2017",
				"Decimal expansion of Sum_{k\u003e=1} (1/10)^prime(k) = 9 * Sum_{k\u003e=1} pi(k)/10^(k+1), where pi(k) = A000720(k). - _Amiram Eldar_, Aug 11 2020"
			],
			"maple": [
				"A010051:= n -\u003e if isprime(n) then 1 else 0 fi;"
			],
			"mathematica": [
				"Table[ If[ PrimeQ[n], 1, 0], {n, 105}] (* _Robert G. Wilson v_, Jan 15 2005 *)",
				"Table[Boole[PrimeQ[n]], {n, 105}] (* _Alonso del Arte_, Aug 09 2011 *)",
				"Table[PrimePi[n] - PrimePi[n-1], {n,50}] (* _G. C. Greubel_, Jan 05 2017 *)"
			],
			"program": [
				"(MAGMA) s:=[]; for n in [1..100] do if IsPrime(n) then s:=Append(s,1); else s:=Append(s,0); end if; end for; s;",
				"(MAGMA) [IsPrime(n) select 1 else 0: n in [1..100]];  // _Bruno Berselli_, Mar 02 2011",
				"(PARI) { for (n=1, 20000, if (isprime(n), a=1, a=0); write(\"b010051.txt\", n, \" \", a); ) } \\\\ _Harry J. Smith_, Jun 15 2009",
				"(PARI) a(n)=isprime(n) \\\\ _Charles R Greathouse IV_, Apr 16, 2011",
				"(Haskell)",
				"import Data.List (unfoldr)",
				"a010051 :: Integer -\u003e Int",
				"a010051 n = a010051_list !! (fromInteger n-1)",
				"a010051_list = unfoldr ch (1, a000040_list) where",
				"   ch (i, ps'@(p:ps)) = Just (fromEnum (i == p),",
				"                              (i + 1, if i == p then ps else ps'))",
				"-- _Reinhard Zumkeller_, Apr 17 2012, Sep 15 2011"
			],
			"xref": [
				"Cf. A051006 (constant 0.4146825... (base 10) = 0.01101010001010001010... (base 2)), A001221 (inverse Moebius transform), A143519, A156660, A156659, A156657, A059500, A053176, A059456, A072762.",
				"First differences of A000720, so A000720 gives partial sums.",
				"Column k=1 of A117278.",
				"Characteristic function of A000040."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_",
			"references": 1084,
			"revision": 227,
			"time": "2020-10-05T01:11:53-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}