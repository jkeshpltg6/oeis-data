{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A306246",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 306246,
			"data": "1,2,2,3,3,3,5,4,2,4,5,4,5,6,4,5,8,5,6,7,3,5,10,7,3,7,8,5,9,8,4,8,9,6,5,11,9,4,9,10,6,6,9,10,8,8,11,8,9,13,7,5,13,11,5,13,13,7,9,12,8,9,16,9,10,13,9,15,11,5,15,13,8,15,12,5,14,13,8",
			"name": "a(1) = 1, a(2) = 2, and for any n \u003e 2, a(n) = o(n-1) + o(n-2) where o(k) is the number of occurrences of a(k) among a(1), ..., a(k).",
			"comment": [
				"The sequence o corresponds to the ordinal transform of the sequence a.",
				"This sequence has a taste of Fibonacci, as each pair of consecutive terms gives rise to the next term.",
				"This sequence is unbounded:",
				"- if the sequence was bounded, say a(n) \u003c= M for any n \u003e 0, then some value in the range 1..M, say t, would appear at least M times among the first M^2 terms, and we would have a value \u003e M after the M-th occurrence of t, a contradiction.",
				"This sequence has interesting graphical features (see Links section)."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A306246/b306246.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A306246/a306246.png\"\u003eDensity plot of the first 10000000 terms\u003c/a\u003e"
			],
			"example": [
				"The first terms, alongside o, are:",
				"  n   a(n)  o(n)",
				"  --  ----  ----",
				"   1     1     1",
				"   2     2     1",
				"   3     2     2",
				"   4     3     1",
				"   5     3     2",
				"   6     3     3",
				"   7     5     1",
				"   8     4     1",
				"   9     2     3",
				"  10     4     2",
				"  11     5     2",
				"  12     4     3",
				"  13     5     3",
				"  14     6     1",
				"  15     4     4",
				"  16     5     4",
				"  17     8     1",
				"  18     5     5",
				"  19     6     2",
				"  20     7     1"
			],
			"program": [
				"(PARI) o = vector(18); for (n=1, 79, if (n\u003c=2, v=n, v=pp+p); print1 (v \", \"); [pp,p]=[p,o[1+v]++])"
			],
			"xref": [
				"See A306251 for the ordinal transform of this sequence."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Rémy Sigrist_, Jan 31 2019",
			"references": 4,
			"revision": 18,
			"time": "2019-06-28T21:15:57-04:00",
			"created": "2019-02-02T05:55:19-05:00"
		}
	]
}