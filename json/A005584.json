{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005584",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5584,
			"id": "M2059",
			"data": "2,13,49,140,336,714,1386,2508,4290,7007,11011,16744,24752,35700,50388,69768,94962,127281,168245,219604,283360,361790,457470,573300,712530,878787,1076103,1308944,1582240,1901416",
			"name": "Coefficients of Chebyshev polynomials.",
			"comment": [
				"If X is an n-set and Y a fixed 2-subset of X then a(n-6) is equal to the number of (n-6)-subsets of X intersecting Y. - _Milan Janjic_, Jul 30 2007",
				"a(n-1) = risefac(n+1,6)/6! - risefac(n+1,4)/4! is for n \u003e=1 also the number of independent components of a symmetric traceless tensor of rank 6 and dimension n. Here risefac is the rising factorial. Put a(0) = 0. - _Wolfdieter Lang_, Dec 10 2015"
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), p. 797.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A005584/b005584.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Milan Janjic, \u003ca href=\"http://www.pmfbl.org/janjic/\"\u003eTwo Enumerative Functions\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"R. K. Guy, \u003ca href=\"/A005581/a005581_1.pdf\"\u003eLetter to N. J. A. Sloane, Feb 1988\u003c/a\u003e",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"C. Rossiter, \u003ca href=\"http://noticingnumbers.net/300SeriesCube.htm\"\u003eDepictions, Explorations and Formulas of the Euler/Pascal Cube\u003c/a\u003e.",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_07\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (7,-21,35,-35,21,-7,1)."
			],
			"formula": [
				"G.f.: x*(2-x) / (1-x)^7.",
				"a(n) = binomial(n+5, n-1) + binomial(n+4, n-1) = 1/720*n*(n+11)*(n+4)*(n+3)*(n+2)*(n+1).",
				"a(n) = binomial(n,6) + 2*binomial(n,5), n \u003e= 5. - _Zerinvary Lajos_, Jul 26 2006",
				"a(n+1) = A127672(12+n, n), n \u003e= 0, where A127672 gives the coefficients of Chebyshev's C polynomials. See the Abramowitz-Stegun reference. - _Wolfdieter Lang_, Dec 10 2015",
				"From _G. C. Greubel_, Aug 27 2019: (Start)",
				"a(n) = (n+11)*Pochhammer(n, 5)/6!.",
				"E.g.f.: x*(1440 +3240*x +1920*x^2 +420*x^3 +36*x^4 +x^5)*exp(x)/6!. (End)"
			],
			"maple": [
				"[seq(binomial(n,6)+2*binomial(n,5), n=5..35)]; # _Zerinvary Lajos_, Jul 26 2006",
				"A005584:=(-2+z)/(z-1)**7; # _Simon Plouffe_ in his 1992 dissertation"
			],
			"mathematica": [
				"Table[Binomial[n,5] + Binomial[n,6], {n,40}] (* _Vladimir Joseph Stephan Orlovsky_, Jun 14 2011, modified by _G. C. Greubel_, Aug 27 2019 *)",
				"Table[(n+11)*Pochhammer[5, n]/30, {n,40}] (* _G. C. Greubel_, Aug 27 2019 *)"
			],
			"program": [
				"(PARI) a(n)=n*(n+11)*(n+4)*(n+3)*(n+2)*(n+1)/720 \\\\ _Charles R Greathouse IV_, Jun 14 2011",
				"(MAGMA) [n*(n+11)*(n+4)*(n+3)*(n+2)*(n+1)/720: n in [1..40]]; // _Vincenzo Librandi_, Jun 15 2011",
				"(Sage) [(n+11)*rising_factorial(n,5)/factorial(6) for n in (1..40)] # _G. C. Greubel_, Aug 27 2019",
				"(GAP) List([1..40], n-\u003e (n+11)*Binomial(n+4,5)/6); # _G. C. Greubel_, Aug 27 2019"
			],
			"xref": [
				"Cf. A127672, A005581, A005582, A005583."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from Klaus Strassburger (strass(AT)ddfi.uni-duesseldorf.de), Dec 07 1999"
			],
			"references": 7,
			"revision": 60,
			"time": "2021-03-12T22:32:36-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}