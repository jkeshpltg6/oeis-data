{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A265221",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 265221,
			"data": "8,9,16,10,5,17,5,11,36,5,5,18,24,5,42,12,25,37,12,5,10,5,11,19,19,25,31,5,37,43,17,13,173,26,32,34,38,13,44,5,44,11,50,6,17,12,56,20,85,20,20,26,26,32,32,6,32,38,12,44,15,18,18,14,21,174,20,27",
			"name": "Number of steps needed to enter the cycle in the double '5x+1' and '3x+1' problem.",
			"comment": [
				"See A264811 for the double '3x+1' and '5x+1' problem.",
				"The double '5x+1' and '3x+1' problem is as follows: start with any number n.",
				"Step 1: if n is even, divide it by 2 until the result becomes odd, otherwise multiply it by 5 and add 1. Let r1 denote the result after respective computation.",
				"Step 2: if the result r1 of step 1 is even, divide it by 2 until the result becomes odd, and then multiply it by 3 and add 1. If the result of step 1 is odd, multiply it by 3 and add 1. Let r2 denote the result after respective computation.",
				"Step 3: go to step 1 with n=r2 and continue until stabilization.",
				"It is conjectured that the process always stabilize.",
				"Starting from any positive n, we call the sequence of iterates (n, T(n,1), T(n,2), ..., T(n,k), ...) the trajectory of n. There are three possible behaviors for such trajectories when n\u003e0:",
				"(i) Convergent trajectory. Some T(n,k) = 1.",
				"(ii) Nontrivial cyclic trajectory. The sequence T(n,k) eventually becomes periodic and T(n,k) is different from 1 for any k\u003e=1.",
				"(iii) Divergent trajectory (it is conjectured that this behavior cannot occur)."
			],
			"link": [
				"Michel Lagneau, \u003ca href=\"/A265221/b265221.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"a(1)=8 because the number 1 generates the trajectory with 8 steps 1 -\u003e 6 -\u003e 3 -\u003e 10 -\u003e 5 -\u003e 26 -\u003e 13 -\u003e 40 -\u003e 20  where the number 20 is the last term of the cycle because the following terms 20/2 = 10 -\u003e 5 -\u003e 26 -\u003e 13 -\u003e 40 are already in the trajectory."
			],
			"maple": [
				"for n from 1 to 100 do:nn:=1000:T:=array(1..nn):kk:=1:",
				"lst:={n}:m:=n:it:=0:indic:=0:m0:=n:T[1]:=n:",
				"   for i from 2 to nn do:",
				"    if irem(m,2)=0",
				"    then m:=m/2:kk:=kk+1:T[kk]:=m:lst:=lst union {m}:it:=it+1:",
				"    else",
				"    if indic=0",
				"    then m:=5*m+1:kk:=kk+1:T[kk]:=m:lst:=lst union {m}:it:=it+1:indic:=1:",
				"    else",
				"    m:=3*m+1:it:=it+1:kk:=kk+1:T[kk]:=m:lst:=lst union {m}:indic:=0:",
				"   fi:",
				"  fi:",
				"od:",
				"n0:=nops(lst):ii:=0:",
				"  for a from n0 to nn while (ii=0) do:",
				"    q:=T[a]:it:=0:",
				"      for b from a-1 by -1 to 1 while (ii=0)do:",
				"       it:=it+1:",
				"        if q=T[b] and q=T[b+it]",
				"        then",
				"        ii:=1: printf(`%d, `,a-2):",
				"        else",
				"        fi:",
				"       od:",
				"     od:",
				"     od:"
			],
			"xref": [
				"Cf. A006577, A264811."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Michel Lagneau_, Dec 05 2015",
			"references": 1,
			"revision": 9,
			"time": "2016-12-10T19:41:15-05:00",
			"created": "2015-12-13T07:57:18-05:00"
		}
	]
}