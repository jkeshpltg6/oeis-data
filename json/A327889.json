{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A327889",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 327889,
			"data": "0,0,-1,0,0,3,0,0,0,-6,0,0,0,0,10,0,0,3,-6,0,-15,0,0,3,0,10,0,21,0,0,0,0,0,0,0,-28,0,0,0,0,0,0,0,0,36,0,0,0,0,10,-15,0,0,0,-45,0,-1,0,-6,0,0,0,-28,0,-45,55,0,-1,0,-6,0,0,0,0,36,0,55,-66,0,-1,0,0,0,0,0,0,0,0,0,-66,78",
			"name": "Triangle read by rows T(n, k) = (-1)^k * A000217(k) * F(binomial(n,k)), where F(x) = 1 if the largest decimal digit of x is 1, and 0 otherwise.",
			"comment": [
				"Represents alternating, normalized (linear) modification of A327853, transformed by first (decimal) digit indicator function F(x).",
				"The scatterplot of the sequence represents a fractal-like structure, made out of arc-like structures. For comparison, the A327853 represents a Sierpinski's gasket (triangle), bounded by a function of A003056 (the positive inverse of triangular numbers).",
				"If a number base other than decimal is used, then for a larger value of the base, the scatterplot structure will appear to be \"zoomed in\". The smallest base that will still represent the structure is ternary, since in binary we have F(x)=1 for all x, and the scatterplot will degrade to a simple triangle structure.",
				"If we modify F(x) to look at other digits than the largest digit, then the structure appears to lose \"density\".",
				"Why does Pascal's triangle (Sierpinski's gasket) converge to such arc-like structure when the digit indicator function F(x) is applied (in some number base)? Are there sequences other than those related to binomial coefficients, that can replicate this structure?"
			],
			"link": [
				"Matej Veselovac, \u003ca href=\"/A327889/b327889.txt\"\u003eTable of n, a(n) for n = 1..100000\u003c/a\u003e",
				"Math StackExchange, \u003ca href=\"https://math.stackexchange.com/q/2585840/318073\"\u003ePattern in Pascal's triangle \u003c/a\u003e.",
				"Matej Veselovac, \u003ca href=\"https://i.stack.imgur.com/BMsgg.png\"\u003eScatterplot of the sequence, for terms a(n), n=1...10^5.\u003c/a\u003e."
			],
			"formula": [
				"The entries of the triangle are given by T(n, k) = (-1)^k * A000217(k) * F(binomial(n,k)), then it is read by rows, where F(x) = 1 if the largest decimal digit of x is 1, and 0 otherwise."
			],
			"example": [
				"First 16 rows of the T(n, k):",
				"0;",
				"0, -1;",
				"0,  0, 3;",
				"0,  0, 0, -6;",
				"0,  0, 0,  0, 10;",
				"0,  0, 3, -6,  0, -15;",
				"0,  0, 3,  0, 10,   0, 21;",
				"0,  0, 0,  0,  0,   0,  0, -28;",
				"0,  0, 0,  0,  0,   0,  0,   0, 36;",
				"0,  0, 0,  0, 10, -15,  0,   0,  0,-45;",
				"0, -1, 0, -6,  0,   0,  0, -28,  0,-45, 55;",
				"0, -1, 0, -6,  0,   0,  0,   0, 36,  0, 55, -66;",
				"0, -1, 0,  0,  0,   0,  0,   0,  0,  0,  0, -66, 78;",
				"0, -1, 0,  0,  0, -15, 21, -28, 36,  0,  0,   0, 78, -91;",
				"0, -1, 0,  0, 10,   0,  0,   0,  0,  0, 55,   0,  0, -91, 105;",
				"0, -1, 3,  0, 10,   0,  0,   0,  0,  0,  0, -66,  0, -91, 105, -120;"
			],
			"mathematica": [
				"d[n_, b_: 10] := Mod[Floor[n/b^(Floor[Log[b, n]])], b]; t[n_] := n (n + 1)/2; f[x_] := x ; r[n0_, b_: 10] := Flatten[Table[(-1)^k  Floor[f[t[k]]]*If[d[Binomial[n, k], b] == 1, 1, 0], {n, 0, n0}, {k, 0, n}]]; r[20] (* _Matej Veselovac_, Sep 29 2019 *)"
			],
			"xref": [
				"Cf. A001317, A007318, A003056, A000217.",
				"Cf. A327853 (original sequence, before applying the transformation)."
			],
			"keyword": "sign,base,tabl,look",
			"offset": "1,6",
			"author": "_Matej Veselovac_, Sep 29 2019",
			"references": 2,
			"revision": 13,
			"time": "2019-10-27T14:17:20-04:00",
			"created": "2019-10-23T15:48:11-04:00"
		}
	]
}