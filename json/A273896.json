{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A273896",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 273896,
			"data": "1,2,4,1,9,4,22,12,1,56,35,6,146,104,24,1,388,312,86,8,1048,938,300,40,1,2869,2824,1032,170,10,7942,8520,3502,680,60,1,22192,25763,11748,2632,295,12,62510,78064,39072,9926,1330,84,1,177308,236976,129100,36640,5712,469,14,506008,720574,424344,132960,23660,2352,112,1",
			"name": "Triangle read by rows: T(n,k) is the number of bargraphs of semiperimeter n having k UHU configurations, where U=(0,1), H(1,0); (n\u003e=2, k\u003e=0).",
			"comment": [
				"Sum of entries in row n = A082582(n).",
				"T(n,0) = A091561(n-1).",
				"Sum(k*T(n,k), k\u003e=0) = A273714(n-1). This implies that the number of UHUs in all bargraphs of semiperimeter n is equal to the number of doublerises in all bargraphs of semiperimeter n-1."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A273896/b273896.txt\"\u003eRows n = 2..200, flattened\u003c/a\u003e",
				"M. Bousquet-Mélou and A. Rechnitzer, \u003ca href=\"http://dx.doi.org/10.1016/S0196-8858(02)00553-5\"\u003eThe site-perimeter of bargraphs\u003c/a\u003e, Adv. in Appl. Math. 31 (2003), 86-112.",
				"Emeric Deutsch, S Elizalde, \u003ca href=\"http://arxiv.org/abs/1609.00088\"\u003eStatistics on bargraphs viewed as cornerless Motzkin paths\u003c/a\u003e, arXiv preprint arXiv:1609.00088, 2016"
			],
			"formula": [
				"G.f.: G=G(t,z), where t marks number of UHU's and z marks semiperimeter, satisfies zG^2-(1-2z-tz^2)G+z^2 = 0."
			],
			"example": [
				"Row 4 is [4,1] because the 5 (=A082582(4)) bargraphs of semiperimeter 4 correspond to the compositions [1,1,1], [1,2], [2,1], [2,2], [3] and the corresponding drawings show that they have 0,1,0,0,0 UHU's.",
				"Triangle starts",
				"1;",
				"2;",
				"4,1;",
				"9,4;",
				"22,12,1;",
				"56,35,6."
			],
			"maple": [
				"eq := z*G^2-(1-2*z-t*z^2)*G+z^2 = 0: G := RootOf(eq, G): Gser := simplify(series(G, z = 0, 20)): for n from 2 to 18 do P[n] := sort(coeff(Gser, z, n)) end do: for n from 2 to 18 do seq(coeff(P[n], t, j), j = 0 .. degree(P[n])) end do; # yields sequence in triangular form",
				"# second Maple program:",
				"b:= proc(n, y, t, h) option remember; expand(",
				"      `if`(n=0, (1-t), `if`(t\u003c0, 0, b(n-1, y+1, 1, 0)*z^h)+",
				"      `if`(t\u003e0 or y\u003c2, 0, b(n, y-1, -1, 0))+",
				"      `if`(y\u003c1, 0, b(n-1, y, 0, `if`(t\u003e0, 1, 0)))))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, z, i), i=0..degree(p)))(b(n, 0$3)):",
				"seq(T(n), n=2..22); # _Alois P. Heinz_, Jun 06 2016"
			],
			"mathematica": [
				"b[n_, y_, t_, h_] := b[n, y, t, h] = Expand[If[n==0, 1-t, If[t\u003c0, 0, b[n-1, y+1, 1, 0]*z^h] + If[t\u003e0 || y\u003c2, 0, b[n, y-1, -1, 0]] + If[y\u003c1, 0, b[n-1, y, 0, If[t\u003e0, 1, 0]]]]]; T[n_] := Function[p, Table[Coefficient[p, z, i], {i, 0, Exponent[p, z]}]][b[n, 0, 0, 0]]; Table[T[n], {n, 2, 22}] // Flatten (* _Jean-François Alcover_, Dec 02 2016 after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A091561, A082582, A273714."
			],
			"keyword": "nonn,tabf",
			"offset": "2,2",
			"author": "_Emeric Deutsch_, Jun 02 2016",
			"references": 1,
			"revision": 23,
			"time": "2017-08-19T23:18:20-04:00",
			"created": "2016-06-03T02:44:57-04:00"
		}
	]
}