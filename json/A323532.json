{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A323532",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 323532,
			"data": "10,586,2219",
			"name": "Numbers k such that the decimal concatenation of the numbers from 1 up to k followed by digit reversals of the numbers from (k-1) down to 1 is a prime.",
			"comment": [
				"The definition is related to the sequence discussed by N. J. A. Sloane (in Notices of the AMS (2018), Vol. 65, No. 9, pp. 1070-71) for which only a(1)-a(2) are known.",
				"a(1) corresponds to a memorable prime (12345678910987654321); a(4) \u003e 10000 (if it exists)."
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"https://www.ams.org/journals/notices/201809/rnoti-p1062.pdf\"\u003eThe On-Line Encyclopedia of Integer Sequences\u003c/a\u003e, Notices, Amer. Math. Soc., 65 (No. 9, Oct. 2018), 1062-1074."
			],
			"example": [
				"10 is a term because 12345678910987654321 is a prime.",
				"2219 is a term because 1...22172218221981227122...1 is a 15534-digit probable prime (where 8122 following 2219 corresponds to the digit reversal of 2218, 7122 to that of 2217, etc. down to 1)."
			],
			"mathematica": [
				"a[n_]:=Block[{cn=Drop[FoldList[Append, {}, ToString/@Range@n], 2]}, ParallelMap[If[PrimeQ[FromDigits@@{#\u003c\u003eReverse@StringReverse@Most@#}], Length@#, Nothing]\u0026, cn]]; a[2300]"
			],
			"program": [
				"(PARI) f(n) = eval(concat(vector(2*n-1, k, if(k\u003c=n, Str(k), concat(apply(x-\u003eStr(x), Vecrev(digits(2*n-k))))))));",
				"isok(n) = ispseudoprime(f(n)); \\\\ _Michel Marcus_, Jan 20 2019"
			],
			"xref": [
				"Cf. A173426 (similar but different concatenation scheme)."
			],
			"keyword": "nonn,base,more",
			"offset": "1,1",
			"author": "_Mikk Heidemaa_, Jan 17 2019",
			"references": 1,
			"revision": 31,
			"time": "2019-09-26T11:07:51-04:00",
			"created": "2019-02-11T21:29:45-05:00"
		}
	]
}