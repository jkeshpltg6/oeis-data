{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A082011",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 82011,
			"data": "2,3,5,7,13,19,11,17,23,29,31,37,41,43,53,47,59,61,67,71,73,79,83,89,97,101,103,113,107,109,127,131,137,139,149,157,151,163,167,173,179,181,191,197,227,193,199,211,223,229,233,239,241,251,271,257,263,269,277",
			"name": "Square array read by antidiagonals, alternating upwards and downwards: T(1,1) = 2 and every other entry is the smallest prime not already used such that the n-th antidiagonal sum is a multiple of n.",
			"comment": [
				"This is the boustrophedon method of filling an array."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A082011/b082011.txt\"\u003eAntidiagonals n = 1..45, flattened\u003c/a\u003e"
			],
			"example": [
				"Square array begins:",
				"2,   3,  19,  11,  53,  47, ...",
				"5,  13,  17,  43,  59, 103, ...",
				"7,  23,  41,  61, 101, 127, ...",
				"29, 37,  67,  97, 131, 181, ...",
				"31, 71,  89, 137, 179, 229, ...",
				"73, 83, 139, 173, 233, 283, ...",
				"T(2,2) = 13 and not 11, because otherwise T(1,3)+7+11 = 0 (mod 3) would not be satisfied for any prime."
			],
			"maple": [
				"b:= proc(t) false end: T:= proc(n, k) local h, t, l, m; if n\u003c1 or k\u003c1 then t:=0 else h:= 1- 2* irem(n+k, 2); m:= n+k-1; l:= add(T(n+h*t, k-h*t), t=1..m-1); t:=3; while b(t) or (h=1 and (n=2 and igcd(t+l, m)\u003e1 or n=1 and irem(t+l, m)\u003c\u003e0)) or (h=-1 and (k=2 and igcd(t+l, m)\u003e1 or k=1 and irem(t+l, m)\u003c\u003e0)) do t:= nextprime(t) od fi; b(t):= true; T(n, k):=t end: T(1, 1):=2: seq(`if`(irem(d, 2)=1, seq(T(1+d-k, k), k=1..d), seq(T(n, 1+d-n), n=1..d)), d=1..15);  # _Alois P. Heinz_, Oct 10 2009"
			],
			"mathematica": [
				"Clear[b]; b[_] = False; T[n_, k_] := Module[{h, t, l, m}, If[n\u003c1 || k\u003c1, t = 0, h = 1 - 2*Mod[n+k, 2]; m = n+k-1; l = Sum[T[n + h*t, k - h*t], {t, 1, m-1}]; t = 3; While[b[t] || (h == 1 \u0026\u0026 (n == 2 \u0026\u0026 GCD[t+l, m]\u003e1 || n == 1 \u0026\u0026 Mod[t+l, m] != 0)) || (h == -1 \u0026\u0026 (k == 2 \u0026\u0026 GCD[t+l, m]\u003e1 || k == 1 \u0026\u0026 Mod[t+l, m] != 0)), t = NextPrime[t]]]; b[t] = True; T[n, k] = t];T[1, 1] = 2; Table[If[Mod[d, 2] == 1, Table[T[1+d-k, k], {k, 1, d}], Table [T[n, 1+d-n], {n, 1, d}]], {d, 1, 15}] // Flatten (* _Jean-François Alcover_, Jun 10 2015, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A082012, A082013, A082014, A082015."
			],
			"keyword": "nonn,tabl",
			"offset": "1,1",
			"author": "_Amarnath Murthy_, Apr 05 2003",
			"ext": [
				"Edited with more terms by _Alois P. Heinz_, Oct 10 2009"
			],
			"references": 5,
			"revision": 15,
			"time": "2015-06-10T10:01:11-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}