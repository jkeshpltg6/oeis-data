{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A270398",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 270398,
			"data": "2,5,19,409,110469,11663878545,142556979966838173805,52663280147046053953610628211699561262739,3562483323353729594120027219074361805521197466091774321103882341800358039125668071",
			"name": "Denominators of r-Egyptian fraction expansion for golden ratio - 1, where r(k) = 1/Fibonacci(k+1).",
			"comment": [
				"Suppose that r is a sequence of rational numbers r(k) \u003c= 1 for k \u003e= 1, and that x is an irrational number in (0,1).  Let f(0) = x, n(k) = floor(r(k)/f(k-1)), and f(k) = f(k-1) - r(k)/n(k).  Then x = r(1)/n(1) + r(2)/n(2) + r(3)/n(3) + ..., the r-Egyptian fraction for x.",
				"See A269993 for a guide to related sequences."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A270398/b270398.txt\"\u003eTable of n, a(n) for n = 1..12\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/EgyptianFraction.html\"\u003eEgyptian Fraction\u003c/a\u003e",
				"\u003ca href=\"/index/Ed#Egypt\"\u003eIndex entries for sequences related to Egyptian fractions\u003c/a\u003e"
			],
			"example": [
				"tau - 1 = 1/3 + 1/(2*5) + 1/(3*19) + 1/(5*409) + ..."
			],
			"mathematica": [
				"r[k_] := 1/Fibonacci[k+1]; f[x_, 0] = x; z = 10;",
				"n[x_, k_] := n[x, k] = Ceiling[r[k]/f[x, k - 1]]",
				"f[x_, k_] := f[x, k] = f[x, k - 1] - r[k]/n[x, k]",
				"x = GoldenRatio - 1; Table[n[x, k], {k, 1, z}]"
			],
			"program": [
				"(PARI) r(k) = 1/fibonacci(k+1);",
				"f(k,x) = if (k==0, x, f(k-1, x) - r(k)/a(k, x););",
				"a(k, x=(sqrt(5)-1)/2) = ceil(r(k)/f(k-1, x)); \\\\ _Michel Marcus_, Mar 22 2016"
			],
			"xref": [
				"Cf. A269993, A000045, A094214."
			],
			"keyword": "nonn,frac,easy",
			"offset": "1,1",
			"author": "_Clark Kimberling_, Mar 22 2016",
			"references": 1,
			"revision": 14,
			"time": "2018-02-24T10:10:13-05:00",
			"created": "2016-03-30T03:04:34-04:00"
		}
	]
}