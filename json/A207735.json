{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A207735",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 207735,
			"data": "1,-1,0,1,0,0,0,1,-1,0,0,0,0,0,-1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,-1,0,-1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,-1,0,0,0,0,0,0,0,0,0,0,0,0,0,-1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0",
			"name": "Expansion of f(-x^2, x^3)^2 / f(x, -x^2) in powers of x where f() is Ramanujan's two-variable theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"This is an example of the quintuple product identity in the form f(a*b^4, a^2/b) - (a/b) * f(a^4*b, b^2/a) = f(-a*b, -a^2*b^2) * f(-a/b, -b^2) / f(a, b) where a = -x^4, b = x."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A207735/b207735.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/QuintupleProductIdentity.html\"\u003eQuintuple Product Identity\u003c/a\u003e"
			],
			"formula": [
				"Expansion of f(x^7, -x^8) - x * f(-x^2, x^13) = f(x^5, -x^10) * f(-x^2, x^3) / f(x, -x^4) where f() is Ramanujan's two-variable theta function.",
				"Euler transform of period 20 sequence [ -1, 0, 1, 1, 1, 0, 1, -1, -1, -2, -1, -1, 1, 0, 1, 1, 1, 0, -1, -1, ...].",
				"G.f.: Sum_{k} (-1)^[k/2] * x^(5*k * (3*k + 1)/2) * (x^(-3*k) - x^(3*k + 1)).",
				"|a(n)| is the characteristic function of A093722.",
				"The exponents in the q-series q * A(q^120) are the squares of the numbers in A057538.",
				"a(7*n + 2) = a(7*n + 4) = a(7*n + 5) = 0. a(n) * (-1)^n = A113681(n)."
			],
			"example": [
				"1 - x + x^3 + x^7 - x^8 - x^14 + x^20 - x^29 - x^31 + x^42 - x^52 - x^66 + ...",
				"q - q^121 + q^361 + q^841 - q^961 - q^1681 + q^2401 - q^3481 - q^3721 + ..."
			],
			"mathematica": [
				"f[x_, y_] := QPochhammer[-x, x*y]*QPochhammer[-y, x*y]*QPochhammer[x*y, x*y]; A207735[n_] := SeriesCoefficient[f[x^5, -x^10]*f[-x^2, x^3]/f[x, -x^4], {x, 0, n}]; Table[A207735[n], {n,0,50}] (* _G. C. Greubel_, Jun 18 2017 *)"
			],
			"program": [
				"(PARI) {a(n) = local(m); if( issquare( 120*n + 1, \u0026m), (-1)^n * kronecker( 12, m), 0)}"
			],
			"xref": [
				"Cf. A057538, A093722, A113681."
			],
			"keyword": "sign",
			"offset": "0,1",
			"author": "_Michael Somos_, Feb 19 2012",
			"references": 1,
			"revision": 13,
			"time": "2021-03-12T22:24:46-05:00",
			"created": "2012-02-24T13:41:23-05:00"
		}
	]
}