{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A214997",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 214997,
			"data": "4,13,45,153,523,1785,6095,20809,71047,242569,828183,2827593,9654007,32960841,112535351,384219721,1311808183,4478793289,15291556791,52208640585,178251448759,608588513865,2077851157943,7094227604041,24221208100279,82696377193033",
			"name": "Power ceiling-floor sequence of 2+sqrt(2).",
			"comment": [
				"See A214992 for a discussion of power ceiling-floor sequence and power ceiling-floor function, p3(x) = limit of a(n,x)/x^n. The present sequence is a(n,r), where r = 2+sqrt(2), and the limit p3(r) = 3.8478612632206289..."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A214997/b214997.txt\"\u003eTable of n, a(n) for n = 0..250\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,2,-2)."
			],
			"formula": [
				"a(n) = floor(x*a(n-1)) if n is odd, a(n) = ceiling(x*a(n-1) if n is even, where x = 2+sqrt(2) and a(0) = ceiling(x).",
				"a(n) = 3*a(n-1) + 2*a(n-2) - 2*a(n-3).",
				"G.f.: (4 + x - 2*x^2)/(1 - 3*x - 2*x^2 + 2*x^3).",
				"a(n) = (1/14)*(2*(-1)^n + (27-19*sqrt(2))*(2-sqrt(2))^n + (2+sqrt(2))^n*(27+19*sqrt(2))). - _Colin Barker_, Nov 13 2017"
			],
			"example": [
				"a(0) = ceiling(r) = 4, where r = 2+sqrt(2);",
				"a(1) = floor(4*r) = 13; a(2) = ceiling(13*r) = 45."
			],
			"mathematica": [
				"(See A214996.)",
				"CoefficientList[Series[(4+x-2*x^2)/(1-3*x-2*x^2+2*x^3), {x,0,50}], x] (* _G. C. Greubel_, Feb 01 2018 *)"
			],
			"program": [
				"(PARI) Vec((4 + x - 2*x^2) / ((1 + x)*(1 - 4*x + 2*x^2)) + O(x^40)) \\\\ _Colin Barker_, Nov 13 2017",
				"(MAGMA) Q:=Rationals(); R\u003cx\u003e:=PowerSeriesRing(Q, 40); Coefficients(R!((4 +x-2*x^2)/(1-3*x-2*x^2+2*x^3))) // _G. C. Greubel_, Feb 01 2018"
			],
			"xref": [
				"Cf. A214992, A007052, A214996, A007070."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_Clark Kimberling_, Nov 10 2012",
			"references": 4,
			"revision": 17,
			"time": "2018-02-02T04:36:58-05:00",
			"created": "2012-11-15T21:24:07-05:00"
		}
	]
}