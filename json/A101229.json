{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A101229",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 101229,
			"data": "1,2,4,1,2,4,8,16,5,10,3,6,12,24,48,96,192,384,768,1536,3072,6144,12288,24576,49152,98304,196608,393216,786432,1572864,3145728,6291456,12582912,25165824,50331648,100663296,201326592,402653184,805306368",
			"name": "Perfect inverse \"3x+1 conjecture\" (See comments for rules).",
			"comment": [
				"Perfect inverse \"3x+1 conjecture\": rule 1: multiply n by 2 to give n' = 2n. rule 2: when n'=(3x+1), do n\"= (n'-1)/3 (n\" integer) Additional rule: rule 2 is applied once for any number n' (otherwise, the sequence beginning with 1 would be the cycle \"1 2 4 1 2 4 1 2 4 1...\"); then apply rule 1.",
				"This gives a particular sequence of hailstone numbers which may be considered as a central axis for all the hailstone number sequences. The perfect inverse \"3x+1 conjecture\" falls rapidly into the sequence 3 6 12 24 48 96... which will never give a number to which apply the 2nd rule.",
				"a(n) for n \u003e= 11 written in base 2: 11, 110, 11000, 110000, ..., i.e.: 2 times 1, (n-11) times 0 (see A003953(n-10). - _Jaroslav Krizek_, Aug 17 2009"
			],
			"reference": [
				"R. K. Guy, Collatz's Sequence, Section E16 in Unsolved Problems in Number Theory, 2nd ed. New York: Springer-Verlag, pp. 215-218, 1994."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A101229/b101229.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CollatzProblem.html\"\u003eCollatz Problem\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_01\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2)."
			],
			"formula": [
				"a(n) = 3*2^(n-11) = 2^(n-11) + 2^(n-10) for n \u003e= 11. - _Jaroslav Krizek_, Aug 17 2009",
				"From _Colin Barker_, Apr 28 2013: (Start)",
				"a(n) = 2*a(n-1) for n\u003e11.",
				"G.f.: x*(17*x^10+27*x^8+7*x^3-1) / (2*x-1). (End)"
			],
			"example": [
				"The first 4 is followed by 1 because 4 = 3*1 + 1, so rule 2: (4-1)/3 = 1;",
				"the second 4 is followed by 8 because the 2nd rule has already been applied, so rule 1: 4*2 = 8."
			],
			"mathematica": [
				"Rest[CoefficientList[Series[x*(17*x^10+27*x^8+7*x^3-1)/(2*x-1), {x, 0, 45}], x]] (* _G. C. Greubel_, Mar 20 2019 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^45)); Vec(x*(17*x^10+27*x^8+7*x^3-1)/(2*x-1)) \\\\ _G. C. Greubel_, Mar 20 2019",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Integers(), 45); Coefficients(R!( x*(17*x^10+27*x^8+7*x^3-1)/(2*x-1) )); // _G. C. Greubel_, Mar 20 2019",
				"(Sage) a=(x*(17*x^10+27*x^8+7*x^3-1)/(2*x-1)).series(x, 45).coefficients(x, sparse=False); a[1:] # _G. C. Greubel_, Mar 20 2019"
			],
			"xref": [
				"Cf. A070165, A006577, A006667, A006666, A070167."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Alexandre Wajnberg_, Jan 22 2005",
			"ext": [
				"More terms from _Joshua Zucker_, May 18 2006",
				"Edited by _G. C. Greubel_, Mar 20 2019"
			],
			"references": 3,
			"revision": 21,
			"time": "2019-03-22T00:29:13-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}