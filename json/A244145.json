{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A244145",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 244145,
			"data": "0,1,1,1,1,2,1,2,1,2,1,2,1,2,2,2,1,2,1,2,1,2,1,3,1,2,3,3,1,2,1,2,1,2,3,3,1,2,2,3,1,2,1,2,2,2,1,3,1,2,2,2,1,3,1,2,1,2,1,4,1,2,2,3,1,2,1,2,1,3,1,3,1,2,2,2,3,2,1,2,1,2,1,4,1,2,3",
			"name": "Number of positive integers k less than n such that the symmetric representation of sigma(k) is contiguous (shares a line border) with the symmetric representation of sigma(n).",
			"comment": [
				"For more information about the symmetric representation of sigma(n) see A237593 and A237270."
			],
			"link": [
				"Michel Marcus, \u003ca href=\"/A244145/a244145_3.pdf\"\u003eA colored version of the symmetric representation of sigma(n), multipage, n = 1..85\u003c/a\u003e"
			],
			"example": [
				"For n = 6 the symmetric representation of sigma(6) (the outer one in the figure below) touches those for n = 4 and n = 5, so a(6) = 2.",
				"   _ _ _ _",
				"  |_ _ _  |_",
				"  |_ _ _|   |_",
				"  |_ _  |_ _  |",
				"  |_ _|_  | | |",
				"  |_  | | | | |",
				"  |_|_|_|_|_|_|",
				"   1 2 3 4 5 6"
			],
			"mathematica": [
				"(* path[n] computing the n-th Dyck path is defined in A237270 *)",
				"(* canvasNamed[] creates a matrix with the labeled symmetric regions *)",
				"(* adjacentPos[] computes list of bounding regions *)",
				"extents[n_] := Map[Transpose[#] + {{1, 0}, {1, 0}}\u0026, Transpose[{path[n-1], Most[Rest[path[n]]]}]]",
				"squaresPos[n_] := DeleteDuplicates[Flatten[Map[Flatten[Outer[List, First[#], Last[#]], 1]\u0026, Map[Apply[Range, #]\u0026, extents[n], {2}]], 1]]",
				"squaresNamed[n_] := Map[#-\u003en\u0026, squaresPos[n]]",
				"canvasNamed[n_] := Module[{canvas = Table[0, {n}, {n}]}, ReplacePart[canvas, Flatten[Map[squaresNamed, Range[n]], 1]]]",
				"adjacentPos[n_, matrix_] := Drop[DeleteDuplicates[Flatten[Map[{matrix[[Apply[Sequence, # + {-1, 0}]]], matrix[[Apply[Sequence, # + {0, -1}]]]}\u0026, Drop[Drop[squaresPos[n], 1], -1]], 1]], 1]",
				"a244145[n_] := Module[{c = canvasNamed[n]}, Join[{0, 1, 1}, Map[Length[adjacentPos[#, c]]\u0026, Range[4, n]]]]",
				"a244145[87] (* computes the first 87 values *)",
				"(* _Hartmut F. W. Hoft_, Jul 23 2014 *)"
			],
			"xref": [
				"Cf. A237270, A237271, A237593."
			],
			"keyword": "nonn",
			"offset": "1,6",
			"author": "_Michel Marcus_, Jun 21 2014",
			"ext": [
				"a(85) corrected by _Hartmut F. W. Hoft_, Jul 23 2014"
			],
			"references": 5,
			"revision": 35,
			"time": "2014-07-23T10:12:59-04:00",
			"created": "2014-07-12T18:06:33-04:00"
		}
	]
}