{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A277789",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 277789,
			"data": "1,-1,4,-10,23,-59,138,-340,813,-1973,4752,-11486,27715,-66927,161558,-390056,941657,-2273385,5488412,-13250226,31988847,-77227939,186444706,-450117372,1086679429,-2623476253,6333631912,-15290740102,36915112091,-89120964311,215157040686,-519435045712,1254027132081",
			"name": "a(n) = Sum_{k=0..n} (-1)^k*floor((1 + sqrt(2))^k).",
			"comment": [
				"Alternating sum of A080039."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A277789/b277789.txt\"\u003eTable of n, a(n) for n = 0..2610\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/SilverRatio.html\"\u003eSilver Ratio\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (-1,4,0,-3,1)."
			],
			"formula": [
				"O.g.f.: (1 - x^2 - 2*x^3)/((1 - x)^2*(1 + x)*(1 + 2*x - x^2)).",
				"E.g.f.: ((-4*sqrt(2)*sinh(sqrt(2)*x) - 1)*exp(-x) + (5 - 2*x)*exp(x))/4.",
				"a(n) = -a(n-1) + 4*a(n-2) - 3*a(n-4) + a(n-5).",
				"a(n) = (2*sqrt(2)*(-1 - sqrt(2))^n - 2*sqrt(2)*(sqrt(2) - 1)^n - (-1)^n - 2*n + 5)/4.",
				"a(n) ~ (-1)^n*s^(n+1)/(s + 1), where s is the silver ratio (A014176)."
			],
			"maple": [
				"f:= gfun:-rectoproc({a(n) = -a(n-1) + 4*a(n-2) - 3*a(n-4) + a(n-5),seq(a(i)=[ 1, -1, 4, -10, 23][i+1],i=0..4)},a(n),remember):",
				"map(f, [$0..40]); # _Robert Israel_, Oct 31 2016"
			],
			"mathematica": [
				"Accumulate[Table[(-1)^n Floor[(1 + Sqrt[2])^n], {n, 0, 32}]]",
				"LinearRecurrence[{-1, 4, 0, -3, 1}, {1, -1, 4, -10, 23}, 33]"
			],
			"program": [
				"(MAGMA) I:=[1,-1,4,-10,23]; [n le 5 select I[n] else -Self(n-1)+4*Self(n-2)-3*Self(n-4)+Self(n-5): n in [1..35]]; // _Vincenzo Librandi_, Nov 01 2016",
				"(PARI) x='x+O('x^30); Vec((1-x^2-2*x^3)/((1-x)^2*(1+x)*(1+2*x-x^2))) \\\\ _G. C. Greubel_, Sep 30 2018"
			],
			"xref": [
				"Cf. A000129, A014176, A020962, A080039."
			],
			"keyword": "easy,sign",
			"offset": "0,3",
			"author": "_Ilya Gutkovskiy_, Oct 31 2016",
			"references": 1,
			"revision": 16,
			"time": "2018-10-01T03:32:46-04:00",
			"created": "2016-11-02T03:48:44-04:00"
		}
	]
}