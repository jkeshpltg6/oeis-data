{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A176129",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 176129,
			"data": "1,1,0,1,2,0,1,6,16,0,1,12,174,192,0,1,20,690,7020,2816,0,1,30,1876,52808,325590,46592,0,1,42,4140,229680,4558410,16290708,835584,0,1,56,7986,738192,31497284,420421056,854630476,15876096,0",
			"name": "Number A(n,k) of solid standard Young tableaux of shape [[n*k,n],[n]]; square array A(n,k), n\u003e=0, k\u003e=0, read by antidiagonals.",
			"comment": [
				"In general, column k is (for k \u003e 1) asymptotic to sqrt((k+2)*(k^2 - 20*k - 8 + sqrt(k*(k+8)^3)) / (8*k^3)) * ((k+2)^(k+2)/k^k)^n / (Pi*n). - _Vaclav Kotesovec_, Aug 31 2014"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A176129/b176129.txt\"\u003eAntidiagonals n = 0..140, flattened\u003c/a\u003e",
				"S. B. Ekhad, D. Zeilberger, \u003ca href=\"https://arxiv.org/abs/1202.6229\"\u003eComputational and Theoretical Challenges on Counting Solid Standard Young Tableaux\u003c/a\u003e, arXiv:1202.6229v1 [math.CO], 2012",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Young_tableau\"\u003eYoung tableau\u003c/a\u003e"
			],
			"example": [
				"Square array A(n,k) begins:",
				"  1,      1,        1,         1,          1,           1, ...",
				"  0,      2,        6,        12,         20,          30, ...",
				"  0,     16,      174,       690,       1876,        4140, ...",
				"  0,    192,     7020,     52808,     229680,      738192, ...",
				"  0,   2816,   325590,   4558410,   31497284,   146955276, ...",
				"  0,  46592, 16290708, 420421056, 4600393936, 31113230148, ..."
			],
			"maple": [
				"b:= proc(x, y, z) option remember; `if`(z\u003ey, b(x, z, y), `if`(z\u003ex, 0,",
				"      `if`({x, y, z}={0}, 1, `if`(x\u003ey and x\u003ez, b(x-1, y, z), 0)+",
				"      `if`(y\u003e0, b(x, y-1, z), 0)+ `if`(z\u003e0, b(x, y, z-1), 0))))",
				"    end:",
				"A:= (n, k)-\u003e b(n*k, n, n):",
				"seq(seq(A(n, d-n), n=0..d), d=0..8);"
			],
			"mathematica": [
				"b [x_, y_, z_] := b[x, y, z] = If[z \u003e y, b[x, z, y], If[z \u003e x, 0, If[Union[{x, y, z}] == {0}, 1, If[x \u003e y \u0026\u0026 x \u003e z, b[x-1, y, z], 0] + If[y \u003e 0, b[x, y-1, z], 0] + If[z \u003e 0, b[x, y, z-1], 0]]]]; a[n_, k_] := b[n*k, n, n]; Table[Table[a[n, d-n], {n, 0, d}], {d, 0, 8}] // Flatten (* _Jean-François Alcover_, Dec 11 2013, translated from Maple *)"
			],
			"xref": [
				"Columns k=0-10 give: A000007, A006335, A214801, A215686, A246619, A246620, A246621, A246632, A246633, A246634, A246635.",
				"Rows n=0-3 give: A000012, A002378, A215687, A215688.",
				"Main diagonal gives: A215123."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Alois P. Heinz_, Jul 29 2012",
			"references": 14,
			"revision": 49,
			"time": "2018-09-21T22:17:49-04:00",
			"created": "2012-07-30T13:01:17-04:00"
		}
	]
}