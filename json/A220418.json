{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A220418",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 220418,
			"data": "1,1,2,3,6,8,18,27,54,84,186,296,630,1008,2106,3711,7710,12924,27594,48528,97902,173352,364722,647504,1340622,2382660,4918482,9052392,18512790,33361776,69273666,127198287,258155910,475568220,981288906,1814542704,3714566310",
			"name": "Express 1 - x - x^2 - x^3 - x^4 - ... as product (1 + g(1)*x) * (1 + g(2)*x^2) *(1 + g(3)*x^3) * ... and use a(n) = - g(n).",
			"comment": [
				"This is the PPE (power product expansion) of A153881 (with offset 0).",
				"When p is prime, a(p) = (2^p-2)/p (A064535).",
				"From _Petros Hadjicostas_, Oct 04 2019: (Start)",
				"This sequence appears as an example in Gingold and Knopfmacher (1995) starting at p. 1223.",
				"In Section 3 of Gingold and Knopfmacher (1995), it is proved that, if f(z) = Product_{n \u003e= 1} (1 + g(n))*z^n = 1/(Product_{n \u003e= 1} (1 - h(n))*z^n), then g(2*n - 1) = h(2*n - 1) and Sum_{d|n} (1/d)*h(n/d)^d = -Sum_{d|n} (1/d)*(-g(n/d))^d. The same results were proved more than ten years later by Alkauskas (2008, 2009). [If we let a(n) = -g(n), then Alkauskas works with f(z) = Product_{n \u003e= 1} (1 - a(n))*z^n; i.e., a(2*n - 1) = -h(2*n - 1) etc.]",
				"The PPE of 1/(1 - x - x^2 - x^3 - x^4 - ...) is given in A290261, which is also studied in Gingold and Knopfmacher (1995, p. 1234).",
				"(End)"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A220418/b220418.txt\"\u003eTable of n, a(n) for n = 1..2000\u003c/a\u003e",
				"Giedrius Alkauskas, \u003ca href=\"http://arxiv.org/abs/0801.0805\"\u003eOne curious proof of Fermat's little theorem\u003c/a\u003e, arXiv:0801.0805 [math.NT], 2008.",
				"Giedrius Alkauskas, \u003ca href=\"https://www.jstor.org/stable/40391097\"\u003eA curious proof of Fermat's little theorem\u003c/a\u003e, Amer. Math. Monthly 116(4) (2009), 362-364.",
				"H. Gingold, H. W. Gould, and Michael E. Mays, \u003ca href=\"https://www.researchgate.net/publication/268023169_Power_product_expansions\"\u003ePower Product Expansions\u003c/a\u003e, Utilitas Mathematica 34 (1988), 143-161.",
				"H. Gingold and A. Knopfmacher, \u003ca href=\"http://dx.doi.org/10.4153/CJM-1995-062-9\"\u003eAnalytic properties of power product expansions\u003c/a\u003e, Canad. J. Math. 47 (1995), 1219-1239.",
				"W. Lang, \u003ca href=\"/A157162/a157162.txt\"\u003eRecurrences for the general problem\u003c/a\u003e."
			],
			"formula": [
				"g(1) = -1 and for k \u003e 1, g(k) satisfies Sum_{d|k} (1/d)*(-g(k/d))^d = (2^k - 1)/k, where a(k) = -g(k). - _Gevorg Hmayakyan_, Jun 05 2016 [Corrected by _Petros Hadjicostas_, Oct 04 2019. See p. 1224 in Gingold and Knopfmacher (1995).]",
				"From _Petros Hadjicostas_, Oct 04 2019: (Start)",
				"a(2*n - 1) = A290261(2*n - 1) for n \u003e= 1 because A290261 gives the PPE of 1/(1 - x - x^2 - x^3 - ...) = (1 - x)/(1 - 2*x).",
				"Define (A(m,n): n,m \u003e= 1) by A(m=1,n) = -1 for n \u003e= 1, A(m,n) = 0 for m \u003e n \u003e= 1 (upper triangular), and A(m,n) = A(m-1,n) - A(m-1,m-1) * A(m,n-m+1) for n \u003e= m \u003e= 2. Then a(n) = A(n,n). [Theorem 3 in Gingold et al. (1988).]",
				"(End)"
			],
			"maple": [
				"b:= proc(n, i) option remember; `if`(n=0 or i\u003c1, 1,",
				"      b(n, i-1)+a(i)*b(n-i, min(n-i, i)))",
				"    end:",
				"a:= proc(n) option remember; 2^n-b(n, n-1) end:",
				"seq(a(n), n=1..40);  # _Alois P. Heinz_, Jun 22 2018"
			],
			"mathematica": [
				"b[n_, i_] := b[n, i] = If[n == 0 || i \u003c 1, 1, b[n, i - 1] + a[i]*b[n - i, Min[n - i, i]]];",
				"a[n_] := a[n] = 2^n - b[n, n - 1] ;",
				"Array[a, 40] (* _Jean-François Alcover_, Jul 09 2018, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(PARI) a(m) = {default(seriesprecision, m+1); gk = vector(m); pol = 1 + sum(n=1, m, -x^n); gk[1] = polcoeff( pol, 1); for (k=2, m, pol = taylor(pol/(1+gk[k-1]*x^(k-1)), x); gk[k] = polcoeff(pol, k, x);); for (k=1, m, print1(-gk[k], \", \"););}"
			],
			"xref": [
				"Cf. A064535, A147541, A153881, A157162, A170908, A170909, A170910, A170911, A170912, A170913, A170914, A170915, A170916, A170917, A220420, A273866, A290261."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Michel Marcus_, Dec 14 2012",
			"ext": [
				"Name edited by _Petros Hadjicostas_, Oct 04 2019"
			],
			"references": 27,
			"revision": 61,
			"time": "2019-10-06T02:38:34-04:00",
			"created": "2012-12-14T18:53:50-05:00"
		}
	]
}