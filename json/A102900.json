{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A102900",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 102900,
			"data": "1,1,7,25,103,409,1639,6553,26215,104857,419431,1677721,6710887,26843545,107374183,429496729,1717986919,6871947673,27487790695,109951162777,439804651111,1759218604441,7036874417767,28147497671065",
			"name": "a(n) = 3*a(n-1) + 4*a(n-2), a(0)=a(1)=1.",
			"comment": [
				"Binomial transform of A102901.",
				"Hankel transform is := 1,6,0,0,0,0,0,0,0,0,0,0,... - _Philippe Deléham_, Nov 02 2008",
				"a(n) + a(n+1) = 2^(2*n+1) = A004171(n)."
			],
			"reference": [
				"Maria Paola Bonacina and Nachum Dershowitz, Canonical Inference for Implicational Systems, in Automated Reasoning, Lecture Notes in Computer Science, Volume 5195/2008, Springer-Verlag."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A102900/b102900.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"A. Abdurrahman, \u003ca href=\"https://arxiv.org/abs/1909.10889\"\u003eCM Method and Expansion of Numbers\u003c/a\u003e, arXiv:1909.10889 [math.NT], 2019.",
				"Shalosh B. Ekhad, N. J. A. Sloane, and  Doron Zeilberger, \u003ca href=\"http://arxiv.org/abs/1503.01796\"\u003eA Meta-Algorithm for Creating Fast Algorithms for Counting ON Cells in Odd-Rule Cellular Automata\u003c/a\u003e, arXiv:1503.01796 [math.CO], 2015; see also the \u003ca href=\"http://www.math.rutgers.edu/~zeilberg/mamarim/mamarimhtml/CAcount.html\"\u003eAccompanying Maple Package\u003c/a\u003e.",
				"Shalosh B. Ekhad, N. J. A. Sloane, and  Doron Zeilberger, \u003ca href=\"http://arxiv.org/abs/1503.04249\"\u003eOdd-Rule Cellular Automata on the Square Grid\u003c/a\u003e, arXiv:1503.04249 [math.CO], 2015.",
				"N. J. A. Sloane, \u003ca href=\"http://arxiv.org/abs/1503.01168\"\u003eOn the Number of ON Cells in Cellular Automata\u003c/a\u003e, arXiv:1503.01168 [math.CO], 2015.",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,4)."
			],
			"formula": [
				"G.f.: (1-2*x)/(1-3*x-4*x^2).",
				"a(n) = (2*4^n + 3*(-1)^n)/5.",
				"a(n) = ceiling(4^n/5) + floor(4^n/5) = (ceiling(4^n/5))^2 - (floor(4^n/5))^2.",
				"a(n) = Sum_{k=0..n} binomial(2*n-k, 2*k)*2^k. - _Paul Barry_, Jan 20 2005",
				"a(n) = upper left term in the 2 X 2 matrix [1,3; 2,2]. - _Gary W. Adamson_, Mar 14 2008",
				"G.f.: G(0)/2, where G(k) = 1 + 1/(1 - x*(8*4^k-3*(-1)^k)/(x*(8*4^k-3*(-1)^k) + (2*4^k+3*(-1)^k)/G(k+1))); (continued fraction). - _Sergei N. Gladkovskii_, May 28 2013",
				"a(n) = 2^(2*n-1) - a(n-1), a(1)=1. - _Ben Paul Thurston_, Dec 27 2015; corrected by _Klaus Purath_, Aug 02 2020",
				"From _Klaus Purath_, Aug 02 2020: (Start)",
				"a(n) = 4*a(n-1) + (-1)^n*3.",
				"a(n) = 6*4^(n-2) + a(n-2), n\u003e=2.",
				"(End)"
			],
			"mathematica": [
				"a[n_]:=(MatrixPower[{{2,2},{3,1}},n].{{2},{1}})[[2,1]]; Table[a[n],{n,0,40}] (* _Vladimir Joseph Stephan Orlovsky_, Feb 20 2010 *)",
				"LinearRecurrence[{3, 4}, {1, 1}, 30] (* _Vincenzo Librandi_, Dec 28 2015 *)"
			],
			"program": [
				"(Haskell)",
				"a102900 n = a102900_list !! n",
				"a102900_list = 1 : 1 : zipWith (+)",
				"               (map (* 4) a102900_list) (map (* 3) $ tail a102900_list)",
				"-- _Reinhard Zumkeller_, Feb 13 2015",
				"(MAGMA) [n le 2 select 1 else 3*Self(n-1)+4*Self(n-2): n in [1..30]]; // _Vincenzo Librandi_, Dec 28 2015",
				"(PARI) a(n)=([0,1; 4,3]^n*[1;1])[1,1] \\\\ _Charles R Greathouse IV_, Mar 28 2016"
			],
			"xref": [
				"Cf. A001045, A046717, A004171, A086901, A247666 (which appears to be the run length transform of this sequence)."
			],
			"keyword": "easy,nonn",
			"offset": "0,3",
			"author": "_Paul Barry_, Jan 17 2005",
			"references": 11,
			"revision": 65,
			"time": "2020-08-29T22:08:17-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}