{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A316674",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 316674,
			"data": "1,1,1,1,1,1,1,3,2,1,1,13,26,4,1,1,75,818,252,8,1,1,541,47834,64324,2568,16,1,1,4683,4488722,42725052,5592968,26928,32,1,1,47293,617364026,58555826884,44418808968,515092048,287648,64,1",
			"name": "Number A(n,k) of paths from {0}^k to {n}^k that always move closer to {n}^k; square array A(n,k), n\u003e=0, k\u003e=0, read by antidiagonals.",
			"comment": [
				"A(n,k) is the number of nonnegative integer matrices with k columns and any number of nonzero rows with column sums n. - _Andrew Howroyd_, Jan 23 2020"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A316674/b316674.txt\"\u003eAntidiagonals n = 0..48, flattened\u003c/a\u003e"
			],
			"formula": [
				"A(n,k) = A262809(n,k) * A011782(n) for k\u003e0, A(n,0) = 1.",
				"A(n,k) = Sum_{j=0..n*k} binomial(j+n-1,n)^k * Sum_{i=j..n*k} (-1)^(i-j) * binomial(i,j). - _Andrew Howroyd_, Jan 23 2020"
			],
			"example": [
				"Square array A(n,k) begins:",
				"  1,  1,     1,         1,              1,                    1, ...",
				"  1,  1,     3,        13,             75,                  541, ...",
				"  1,  2,    26,       818,          47834,              4488722, ...",
				"  1,  4,   252,     64324,       42725052,          58555826884, ...",
				"  1,  8,  2568,   5592968,    44418808968,      936239675880968, ...",
				"  1, 16, 26928, 515092048, 50363651248560, 16811849850663255376, ..."
			],
			"maple": [
				"A:= (n, k)-\u003e `if`(k=0, 1, ceil(2^(n-1))*add(add((-1)^i*",
				"     binomial(j, i)*binomial(j-i, n)^k, i=0..j), j=0..k*n)):",
				"seq(seq(A(n, d-n), n=0..d), d=0..10);"
			],
			"mathematica": [
				"A[n_, k_] := Sum[If[k == 0, 1, Binomial[j+n-1, n]^k] Sum[(-1)^(i-j)* Binomial[i, j], {i, j, n k}], {j, 0, n k}];",
				"Table[Table[A[n, d-n], {n, 0, d}], {d, 0, 10}] // Flatten (* _Jean-François Alcover_, Nov 04 2021 *)"
			],
			"program": [
				"(PARI) T(n,k)={my(m=n*k); sum(j=0, m, binomial(j+n-1,n)^k*sum(i=j, m, (-1)^(i-j)*binomial(i, j)))} \\\\ _Andrew Howroyd_, Jan 23 2020"
			],
			"xref": [
				"Columns k=0..3 give: A000012, A011782, A052141, A316673.",
				"Rows n=0..2 give: A000012, A000670, A059516.",
				"Main diagonal gives A316677.",
				"Cf. A011782, A219727, A262809, A331315, A331485, A331636."
			],
			"keyword": "nonn,tabl,walk",
			"offset": "0,8",
			"author": "_Alois P. Heinz_, Jul 10 2018",
			"references": 11,
			"revision": 27,
			"time": "2021-11-04T06:01:56-04:00",
			"created": "2018-07-10T14:37:05-04:00"
		}
	]
}