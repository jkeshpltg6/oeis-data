{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A135308",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 135308,
			"data": "1,1,2,5,13,1,35,7,96,36,267,159,3,750,645,35,2123,2475,264,6046,9136,1602,12,17303,32773,8515,195,49721,115017,41349,1925,143365,396730,188010,14740,55,414584,1349440,813072,96200,1144,1201917,4537368",
			"name": "Triangle read by rows: T(n,k) = the number of Dyck paths of semilength n with k DUUU's.",
			"comment": [
				"Row n has floor((n+2)/3) terms (n\u003e=1). Row sums yield the Catalan numbers (A000108). Column 0 yields A005773. - _Emeric Deutsch_, Dec 13 2007"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A135308/b135308.txt\"\u003eRows n = 0..250, flattened\u003c/a\u003e",
				"FindStat - Combinatorial Statistic Finder, \u003ca href=\"http://www.findstat.org/StatisticsDatabase/St000125\"\u003eThe number of occurrences of the contiguous pattern [.,[[[.,.],.],.\u003c/a\u003e",
				"A. Sapounakis, I. Tasoulas and P. Tsikouras, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2007.03.005\"\u003eCounting strings in Dyck paths\u003c/a\u003e, Discrete Math., 307 (2007), 2909-2924."
			],
			"formula": [
				"T(n,k) = (1/n)*C(n,k)*Sum[(-1)^(j-k+1)*3^(n-j)*C(n-k,j-k)*C(2j-2-3k,j-1), j=3k+1..n) (n\u003e=1). G.f.: F=F(t,z) satisfies tzF^3 + [3(1-t)z-1]F^2 - [3(1-t)z-1]F + (1-t)z = 0. - _Emeric Deutsch_, Dec 13 2007"
			],
			"example": [
				"Triangle begins:",
				"1",
				"1",
				"2",
				"5",
				"13 1",
				"35 7",
				"96 36",
				"267 159 3",
				"...",
				"T(5,1)=7 because we have UDUUUUDDDD, UDUUUDUDD, UDUUUDDUDD, UDUUUDDDUD, UDUDUUUDDD, UUDUUUDDDD and UUDDUUUDDD."
			],
			"maple": [
				"T:=proc(n,k) options operator, arrow: binomial(n,k)*(sum((-1)^(j-k+1)*3^(n-j)*binomial(n-k,j-k)*binomial(2*j-2-3*k,j-1),j=3*k+1..n))/n end proc: 1; for n to 15 do seq(T(n,k),k=0..floor((n-1)*1/3)) end do; # yields sequence in triangular form; _Emeric Deutsch_, Dec 13 2007",
				"# second Maple program:",
				"b:= proc(x, y, t) option remember; `if`(y\u003c0 or y\u003ex, 0,",
				"     `if`(x=0, 1, expand(b(x-1, y+1, [1, 3, 4, 1][t])",
				"      * `if`(t=4, z, 1) +b(x-1, y-1, [2, 2, 2, 2][t]))))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, z, i), i=0..degree(p)))(b(2*n, 0, 1)):",
				"seq(T(n), n=0..20);  # _Alois P. Heinz_, Jun 10 2014"
			],
			"mathematica": [
				"T[n_, k_] := (1/n)*Binomial[n, k]*Sum[(-1)^(j-k+1)*3^(n-j)*Binomial[n-k, j-k]*Binomial[2j-2-3k, j-1], {j, 3k+1, n}]; T[0, 0] = 1; Table[T[n, k], {n, 0, 15}, {k, 0, If[n == 0, 0, Quotient[n-1, 3]]}] // Flatten (* _Jean-François Alcover_, Nov 27 2014, after _Emeric Deutsch_ *)"
			],
			"xref": [
				"Cf. A000108, A005773."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_, Dec 07 2007",
			"ext": [
				"Edited and extended by _Emeric Deutsch_, Dec 13 2007"
			],
			"references": 1,
			"revision": 21,
			"time": "2015-10-15T15:24:12-04:00",
			"created": "2007-12-09T03:00:00-05:00"
		}
	]
}