{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A324164",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 324164,
			"data": "1,2,29523,1431655764,119209289550780,204698073815493849906,1288498953284574087356182400,23736214210444926301853697505006152,1090995446010964053236424684934590917505180,1111111111111111111111111111111111111111111111111110",
			"name": "Number of primes \u003c= A324154(n).",
			"comment": [
				"Also the number of zerofree numbers \u003c= A324154(n).",
				"Expressed in base n - 1 and starting with n = 3, the sequence is 10, 1111111110, 1111111111111110, 111111111111111111110, 111111111111111111111111110, 111111111111111111111111111111110, 111111111111111111111111111111111111110, 111111111111111111111111111111111111111111110, 1111111111111111111111111111111111111111111111111110, ....",
				"Ostensibly, the reason for that is the calculation formula (see Formula section) for the number of zerofree numbers \u003c= x^m + y, with y \u003c (x^(m+1)-1)/(x-1) - x^m. But the deeper reason is the definition of sequence A324154. Each term A324154(n) marks a point of intersection between the curve numOfZerofreeNum_n(x) [the number of base-n zerofree numbers \u003c= x] and the curve pi(x) [the number of prime numbers \u003c= x]. Since numOfZerofreeNum_n(x) doesn't change for relatively large intervals at x = k*n^m (approx. a portion of \u003e 1/(k*n)), but grows similar to pi(x) for regions outside, it is likely, that the point of intersection lies between x = k*n^m and x = n^m*(k + 1/n + 1/n^2 + 1/n^3 + ... + 1/n^m). The chance is maximal for k = 1, since the density of primes becomes smaller for greater x."
			],
			"formula": [
				"a(n) = pi(A324154(n)).",
				"a(n) = numOfZerofreeNum_n(A324154(n)), where numOfZerofreeNum_n(x) is the number of base-n zerofree numbers \u003c= x (cf. A324161).",
				"a(n) = k*(n-1)^m + ((n-1)^m - 1)/(n-2) - 1,",
				"where m = floor(log_n(A324154(n))), k = floor(A324154(n)/n^m), and provided A324154(n) - k*n^m \u003c (n^(m+1)-1)/(n-1) - n^m.",
				"With d := log(n-1)/log(n):",
				"a(n) \u003c= ((n - 1)*(A324154(n) + 1)^d - 1)/(n - 2) - 1,",
				"a(n) \u003e= (((n - 1)*A324154(n) + n)^d - 1)/(n - 2) - 1.",
				"a(n) \u003c A324154(n) / (log(A324154(n)) - 1.1), for n \u003e 3.",
				"a(n) \u003e A324154(n) / (log(A324154(n)) - 1), for n \u003e 3."
			],
			"example": [
				"a(2) = 1, since there is only one prime \u003c= A324154(2) = 2.",
				"a(3) = 2, since there are 2 primes \u003c= A324154(3) = 3."
			],
			"xref": [
				"Cf. A011540, A052382, A324154, A324155, A324160, A324161, A324165."
			],
			"keyword": "nonn",
			"offset": "2,2",
			"author": "_Hieronymus Fischer_, Feb 22 2019",
			"references": 2,
			"revision": 21,
			"time": "2019-03-28T12:03:06-04:00",
			"created": "2019-03-28T12:03:06-04:00"
		}
	]
}