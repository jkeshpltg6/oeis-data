{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A341335",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 341335,
			"data": "0,1,3,2,7,6,5,4,15,14,13,12,10,11,8,9,31,30,29,28,27,26,25,24,21,20,23,22,17,16,19,18,63,62,61,60,59,58,57,56,54,55,52,53,50,51,48,49,42,43,40,41,46,47,44,45,35,34,33,32,39,38,37,36,127,126,125",
			"name": "For any number n with binary expansion (b_1, ..., b_k), the binary expansion of a(n), say (c_1, ..., c_k) satisfies c_m = Sum_{d | m} b_d mod 2 for m = 1..k.",
			"comment": [
				"This sequence is a permutation of the nonnegative integers with inverse A341336.",
				"This sequence operates on binary expansions in the same way as the XOR-Moebius transform described in A295901.",
				"This sequence has only two fixed points: a(0) = 0, a(1) = 1."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A341335/b341335.txt\"\u003eTable of n, a(n) for n = 0..8191\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(n) \u003c 2^k for any n \u003c 2^k.",
				"a(floor(n/2)) = floor(a(n)/2).",
				"a(2^k) = 2^(k+1) - 1 for any k \u003e= 0."
			],
			"example": [
				"For n = 42:",
				"- the binary expansion of 42 is (1, 0, 1, 0, 1, 0),",
				"- the binary expansion of a(42) has 6 digits:",
				"    - the 1st digit = 1                     mod 2 = 1,",
				"    - the 2nd digit = 1 + 0                 mod 2 = 1,",
				"    - the 3rd digit = 1     + 1             mod 2 = 0,",
				"    - the 4th digit = 1 + 0     + 0         mod 2 = 1,",
				"    - the 5th digit = 1             + 1     mod 2 = 0,",
				"    - the 6th digit = 1 + 0 + 1         + 0 mod 2 = 0,",
				"- so the binary expansion of a(42) is \"110100\",",
				"- and a(42) = 52."
			],
			"program": [
				"(PARI) a(n) = { my (b=binary(n), c=vector(#b)); for (m=1, #b, fordiv (m, d, c[m]=(c[m] + b[d])%2)); fromdigits(c, 2) }"
			],
			"xref": [
				"Cf. A295901, A341336."
			],
			"keyword": "nonn,base",
			"offset": "0,3",
			"author": "_Rémy Sigrist_, Apr 25 2021",
			"references": 2,
			"revision": 15,
			"time": "2021-04-25T15:12:57-04:00",
			"created": "2021-04-25T13:09:38-04:00"
		}
	]
}