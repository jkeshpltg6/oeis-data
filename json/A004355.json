{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A004355",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 4355,
			"data": "1,6,66,816,10626,142506,1947792,26978328,377348994,5317936260,75394027566,1074082795968,15363284301456,220495674290430,3173734438530120,45795673964460816,662252084388541314",
			"name": "Binomial coefficient C(6n,n).",
			"comment": [
				"a(n) is asymptotic to c*(46656/3125)^n/sqrt(n), with c = sqrt(3/(5*Pi)) = 0.437019372236831628217354... - _Benoit Cloitre_, Jan 23 2008"
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), p. 828."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A004355/b004355.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy]."
			],
			"formula": [
				"a(n) = C(6*n-1,n-1)*C(36*n^2,2)/(3*n*C(6*n+1,3)), n\u003e0. - _Gary Detlefs_, Jan 02 2014",
				"G.f.: A(x) = x*B'(x)/B(x), where B(x)+1 is g.f. of A002295. - _Vladimir Kruchinin_, Oct 05 2015",
				"a(n) = GegenbauerC(n, -3*n, -1). - _Peter Luschny_, May 07 2016",
				"From _Ilya Gutkovskiy_, Jan 16 2017: (Start)",
				"O.g.f.: 5F4(1/6,1/3,1/2,2/3,5/6; 1/5,2/5,3/5,4/5; 46656*x/3125).",
				"E.g.f.: 5F5(1/6,1/3,1/2,2/3,5/6; 1/5,2/5,3/5,4/5,1; 46656*x/3125). (End)",
				"RHS of identities Sum_{k = 0..n} binomial(3*n, k)*binomial(3*n, n-k) =",
				"Sum_{k = 0..2*n} (-1)^(n+k)*binomial(6*n, k)*binomial(6*n, 2*n-k) = binomial(6*n,n). - _Peter Bala_, Oct 07 2021"
			],
			"mathematica": [
				"Table[Binomial[6 n, n], {n, 0, 16}] (* _Michael De Vlieger_, Oct 05 2015 *)"
			],
			"program": [
				"(MAGMA) [Binomial(6*n,n): n in [0..100]]; // _Vincenzo Librandi_, Apr 13 2011",
				"(Maxima)",
				"B(x):=sum(binomial(6*n,n-1)/n*x^n,n,1,30);",
				"taylor(x*diff(B(x),x)/B(x),x,0,10); /* _Vladimir Kruchinin_, Oct 05 2015 */",
				"(PARI) a(n) = binomial(6*n,n) \\\\ _Altug Alkan_, Oct 05 2015"
			],
			"xref": [
				"Cf. A002295"
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 7,
			"revision": 50,
			"time": "2021-10-09T06:53:43-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}