{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A208614",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 208614,
			"data": "1,0,1,1,0,1,0,1,0,0,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,3,1,4,2,1,1,1,3,2,1,1,3,1,1,2,3,1,2,1,3,4,2,1,5,9,6,5,4,1,6,6,7,4,3,1,4,1,4,9,20,7,3,1,7,8,6,1,15,1,5,19,11,13,9,1,21,52,7,1",
			"name": "Number of partitions of n into distinct primes where all of the prime factors of n are represented in the partition.",
			"comment": [
				"Inspired by web-based discussion started by Rajesh Bhowmick."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A208614/b208614.txt\"\u003eTable of n, a(n) for n = 0..5000\u003c/a\u003e",
				"Rajesh Bhowmick, \u003ca href=\"http://sciforums.com/showthread.php?t=112659\"\u003eA bit different from Goldbach's conjecture\u003c/a\u003e (February 27-28, 2012)"
			],
			"example": [
				"a(p) = 1 for any prime p.",
				"a(n) = 0 for 1, 4, 6, 8, 9, 22.",
				"a(25) = 3 because 25 = 3 + 5 + 17 = 5 + 7 + 13 = 2 + 5 + 7 + 11."
			],
			"maple": [
				"with (numtheory):",
				"a:= proc(n) local b, l, f;",
				"      b:= proc(h, j) option remember;",
				"            `if`(h=0, 1, `if`(j\u003c1, 0,",
				"            `if`(l[j]\u003eh, 0, b(h-l[j], j-1)) +b(h, j-1)))",
				"          end; forget (b);",
				"      f:= factorset(n);",
				"      l:= sort([({seq (ithprime(i), i=1..pi(n))} minus f)[]]);",
				"      b(n-add(i, i=f), nops(l))",
				"    end:",
				"seq (a(n), n=0..300); # _Alois P. Heinz_, Mar 20 2012"
			],
			"mathematica": [
				"restrictedIntegerPartition[ n_Integer, list_List ] := 1 /; n == 0",
				"restrictedIntegerPartition[ n_Integer, list_List ] := 0 /; n \u003c 0 || Total[list] \u003c n || n \u003c Min[list]",
				"restrictedIntegerPartition[ n_Integer, list_List ] := restrictedIntegerPartition[n - First[list], Rest[list]] + restrictedIntegerPartition[n, Rest[list]]",
				"distinctPrimeFactors[ n_Integer ] := distinctPrimeFactors[n] = Map[First, FactorInteger[n]]",
				"oeisA076694[ n_Integer ] := oeisA076694[n] = n - Total[distinctPrimeFactors[n]]",
				"oeisA208614[ n_Integer ] := restrictedIntegerPartition[oeisA076694[n], Sort[Complement[Prime @ Range @ PrimePi @ oeisA076694 @ n, distinctPrimeFactors[n]] , Greater ]]",
				"Table[oeisA208614[n], {n,1,100}]"
			],
			"program": [
				"(* maxima *)",
				"countRestrictedIntegerPartitions(n, L) := if ( n = 0 ) then 1 else if ( ( n \u003c 0 ) or ( lsum(k, k, L) \u003c n ) or ( n \u003c lmin( L ) ) ) then 0 else block( [ m, R ], m : first(L), R : rest(L), countRestrictedIntegerPartitions(n, R) + countRestrictedIntegerPartitions(n - m, R));",
				"distinctPrimeFactors(n) := map(first,ifactors(n));",
				"oeisA076694(n) := n - lsum(k,k,distinctPrimeFactors(n));",
				"listOfPrimesLessThanOrEqualTo (n) := block( [ list : [] , i], for i : 2 step 0 while i \u003c= n do ( list : cons(i, list) , i : next_prime(i) ) , list );",
				"oeisA208614(n) := block([ m, list ], m : oeisA076694(n), list : sort(listify(setdifference(setify(listOfPrimesLessThanOrEqualTo(m)), setify(distinctPrimeFactors(n)))), ordergreatp), countRestrictedIntegerPartitions(m, list));",
				"makelist(oeisA208614(j), j, 1, 100);"
			],
			"xref": [
				"Cf. A000586 (upper bound). A000586(A076694(n)) is a stricter upper bound."
			],
			"keyword": "nonn",
			"offset": "0,26",
			"author": "_Richard Penner_, Feb 29 2012",
			"references": 2,
			"revision": 26,
			"time": "2012-04-05T19:55:56-04:00",
			"created": "2012-03-20T20:38:46-04:00"
		}
	]
}