{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A179409",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 179409,
			"data": "6,6,6,8,10,12,16,18,20,26,26,28,30,22,26,16,20,18,18,20,16,20,10,8,6,6,6,8,10,12,16,18,20,26,26,28,30,22,26,16,20,18,18,20,16,20,10,8,6,6,6,8,10,12,16,18,20,26,26,28,30,22,26,16,20,18,18,20,16,20,10,8",
			"name": "The number of alive cells in Conway's Game of Life on the 8x8 toroidal grid, in a cyclic sequence of 48 patterns containing among other patterns, a \"stairstep hexomino\" and its mirror image, illustrated below.",
			"comment": [
				"Period 24. The sequence begins (from offset 0) with its lexicographically earliest rotation. All terms are even because the initial pattern has an even number of cells and because it has 180-degree rotational symmetry.",
				"The mean value of terms in the whole period of 24 is 16.9167."
			],
			"link": [
				"A. Karttunen, \u003ca href=\"/A179409/a179409.ijs.txt\"\u003eJ-program for computing the terms of this sequence\u003c/a\u003e",
				"Stephen A. Silver, \u003ca href=\"https://conwaylife.com/ref/lexicon/lex_s.htm#stairstephexomino\"\u003eLife Lexicon, \"stairstep hexomino\"\u003c/a\u003e"
			],
			"example": [
				"The generations 0-3 of this cycle of patterns look as follows, thus a(0)=a(1)=a(2)=6 and a(3)=8.",
				". . . . . . . . | . . . . . . . . | . . . . . . . . | . . . . . . . .",
				". . . . . . . . | . . . . . . . . | . . . . . . . . | . . . . . . . .",
				". . o . . . . . | . . . . . . . . | . . o . . . . . | . . o o . . . .",
				". o . . o . . . | . o o o . . . . | . . o o . . . . | . . o . o . . .",
				". . o . . o . . | . . . o o o . . | . . . o o . . . | . . o . o . . .",
				". . . . o . . . | . . . . . . . . | . . . . o . . . | . . . o o . . .",
				". . . . . . . . | . . . . . . . . | . . . . . . . . | . . . . . . . .",
				". . . . . . . . | . . . . . . . . | . . . . . . . . | . . . . . . . .",
				"(generation 0.) | (generation 1.) | (generation 2.) | (generation 3.)",
				"..................................|stairstep hexomino................",
				"generations 4-7 of this cycle of patterns look as follows, thus a(4)=10, a(5)=12, a(6)=16 and a(7)=18.",
				". . . . . . . . | . . . . . . . . | . . . . . . . . | . . . . . . . .",
				". . . . . . . . | . . . . . . . . | . . o . . . . . | . o o . . . . .",
				". . o o . . . . | . o o o . . . . | . o o o o . . . | . o o . o o . .",
				". o o . o . . . | . o . . o o . . | o . . . o o . . | o . . . o o . .",
				". . o . o o . . | . o o . . o . . | . o o . . . o . | . o o . . . o .",
				". . . o o . . . | . . . o o o . . | . . o o o o . . | . o o . o o . .",
				". . . . . . . . | . . . . . . . . | . . . . o . . . | . . . . o o . .",
				". . . . . . . . | . . . . . . . . | . . . . . . . . | . . . . . . . .",
				"In generation 24 we obtain the initial pattern reflected over its central vertical axis, and for the generations 24--47 the patterns repeat the history of the first 24 generations, but reflected over its vertical axis, after which the whole cycle begins from the start again at the generation 48.",
				". . . . . . . . | . . . . . . . .",
				". . . . . . . . | . . . . . . . .",
				". . . . o . . . | . . . . . . . .",
				". . o . . o . . | . . . o o o . .",
				". o . . o . . . | . o o o . . . .",
				". . o . . . . . | . . . . . . . .",
				". . . . . . . . | . . . . . . . .",
				". . . . . . . . | . . . . . . . .",
				"(generation 24) | (generation 25)"
			],
			"xref": [
				"Cf. A179410, A179411.",
				"Cf. also A179412 for a longer cyclic sequence of 132 patterns.",
				"A179415 which traces the history of the same initial pattern on infinite square grid, differs from this one for the first time at n=14, where a(14)=26 while A179415(14)=32."
			],
			"keyword": "nonn",
			"offset": "0,1",
			"author": "_Antti Karttunen_, Jul 27 2010",
			"references": 10,
			"revision": 6,
			"time": "2019-11-27T11:14:52-05:00",
			"created": "2010-07-31T03:00:00-04:00"
		}
	]
}