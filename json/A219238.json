{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A219238",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 219238,
			"data": "1,0,1,1,1,0,0,1,1,2,1,1,0,0,0,1,1,2,2,2,1,1,0,0,0,0,1,1,2,2,3,2,2,1,1,0,0,0,0,0,1,1,2,2,3,3,3,2,2,1,1,0,0,0,0,0,0,1,1,2,2,3,3,4,3,3,2,2,1,1,0,0,0,0,0,0,0,1,1,2,2,3,3,4,4,4,3,3,2,2,1,1,0,0,0,0,0,0,0,0,1,1,2,2,3,3,4,4,5,4,4,3,3,2,2,1,1",
			"name": "Coefficient table for the first differences of table A047971: Coefficients of the difference of Gauss polynomials [n+3,3]_q - [n+2,3]_q.",
			"comment": [
				"The row lengths sequence is A016777 (3*n+1). The sum for row n is A000217(n+1) = binomial(n+2,2).",
				"The coefficients of the Gauss polynomial [n+3,3]_q are given in A047971.",
				"a(n,k) = [q^k]([n+3,3]_q - [n+2,3]_q). One can use the identity [n+3,3]_q - [n+2,3]_q = q^n*[n+2,2]_q (see the Andrews reference given in A047971, p. 35, (3.3.3)). Therefore, the present array is obtained from A008967 after a shift of row n by n units to the right, inserting zeros for the first n entries.",
				"The o.g.f. of the row polynomials in q of degree 3*n is 1/((1-q)*(1-q^2)*(1-q^3)) (multiply the o.g.f. of A047971 by (1-z)). a(n,k) determines therefore the number of partitions of k with precisely n parts, each \u003c= 3. Alternatively, a(n,k) determines the number of partitions of k with at most 3 parts, with each part \u003c= n but not each part \u003c= (n-1), i.e., part n, maybe more than once, is present besides possibly smaller ones."
			],
			"formula": [
				"a(n,k) = [q^k]([n+3,3]_q - [n+2,3]_q),  = [q^(k-n)] [n+2,2]_q , n \u003e= 0, 0 \u003c= k \u003c= 3*n. For the Gauss polynomial (q-binomial) [n+m,m]_q = [m+n,n]_q see a comment on A219237 where also the Andrews reference and a link to Mathworld is found."
			],
			"example": [
				"The table a(n,k) begins:",
				"n\\k 0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18...",
				"0:  1",
				"1:  0  1  1  1",
				"2:  0  0  1  1  2  1  1",
				"3:  0  0  0  1  1  2  2  2  1  1",
				"4:  0  0  0  0  1  1  2  2  3  2  2  1  1",
				"5:  0  0  0  0  0  1  1  2  2  3  3  3  2  2  1  1",
				"6:  0  0  0  0  0  0  1  1  2  2  3  3  4  3  3  2  2  1  1",
				"...",
				"Row n=1 is 0,1,1,1 because [3,2]_q = 1 + q + q^2 and the coefficient of q^{-1} is 0, the one of q^0 is 1, the one of q^1 is 1 and the one of q^2 is 1. A shift of row n=1 of A008967 by one unit to the right.",
				"a(n,k) = 0 if n \u003e k because a partition of k never has more than k parts.",
				"a(n,k) = 0 if k \u003e 3*n because there is no partition of 3*n+m, with m \u003e= 1, and exactly n parts, each \u003c= 3.",
				"a(2,4) = 2 because the partitions of 4 with 2 parts are 1,3 and 2,2, and the parts in both are \u003c= 3.",
				"a(2,4) = 2 because the partitions of 4 with number of parts \u003c= 3, each \u003c= 2, are 2,2 and 1,1,2, and part 2 is present in both of them. Note the conjugacy of partitions 1,3 and 1,1,2."
			],
			"xref": [
				"Cf. A047971, A008967 (with shifted rows)."
			],
			"keyword": "nonn,easy,tabf",
			"offset": "0,10",
			"author": "_Wolfdieter Lang_, Dec 06 2012",
			"references": 1,
			"revision": 10,
			"time": "2017-08-20T23:19:03-04:00",
			"created": "2012-12-06T16:34:34-05:00"
		}
	]
}