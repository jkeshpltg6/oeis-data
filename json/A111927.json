{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A111927",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 111927,
			"data": "0,0,0,1,4,10,21,42,84,169,340,682,1365,2730,5460,10921,21844,43690,87381,174762,349524,699049,1398100,2796202,5592405,11184810,22369620,44739241,89478484,178956970,357913941,715827882,1431655764,2863311529,5726623060",
			"name": "Expansion of x^3 / ((x-1)*(2*x-1)*(x^2-x+1)).",
			"comment": [
				"Binomial transform of sequence (0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0). Note: the binomial transform of the sequence (0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0) is A111926; the binomial transform of the sequence (0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0) is A024495 (disregarding first two terms, which are both zero).",
				"The sequence relates the calculation of the logarithm of the Twin Prime Constants of order 3 to the sequence of prime zeta functions, see definition 7 in arXiv:0903.2514. - _R. J. Mathar_, Mar 28 2009"
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A111927/b111927.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Antoine-Augustin Cournot, \u003ca href=\"https://archive.org/download/bulletindesscie22unkngoog/\"\u003eSolution d'un problème d'analyse combinatoire\u003c/a\u003e, Bulletin des Sciences Mathématiques, Physiques et Chimiques, item 34, volume 11, 1829, pages 93-97.  Also at \u003ca href=\"http://books.google.com.au/books?id=B-v-eXuvoG4C\"\u003eGoogle Books\u003c/a\u003e.  Page 97 case p=3 formula y^(0) = a(n).  (But misprint \"- (2/3)*cos\" should be \"+ (2/3)*cos\".)",
				"R. J. Mathar, \u003ca href=\"http://arxiv.org/abs/0903.2514\"\u003eHardy-Littlewood constants embedded into infinite products over all positive integers\u003c/a\u003e, arXiv:0903.2514 [math.NT], 2009-2011.",
				"Christian Ramus, \u003ca href=\"http://gdz.sub.uni-goettingen.de/en/dms/load/toc/?PPN=PPN243919689_0011\"\u003eSolution générale d'un problème d'analyse combinatoire\u003c/a\u003e, Journal für die Reine und Angewandte Mathematik (Crelle's journal), volume 11, 1834, pages 353-355.  Page 353 case p=3 formula y^(0) = a(n).  (But misprint \"+ (1/3)*cos\" should be \"+ (2/3)*cos\", per the general case equation A page 354.)",
				"Kevin Ryde, \u003ca href=\"http://user42.tuxfamily.org/terdragon/index.html\"\u003eIterations of the Terdragon Curve\u003c/a\u003e, section Lines, quantity Lines_k(2) = a(k+1).",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-6,5,-2)."
			],
			"formula": [
				"a(n+2) - a(n+1) + a(n) = A000225(n).",
				"a(n) - a(n-1) = A024495(n-1).",
				"From _Colin Barker_, Feb 10 2017: (Start)",
				"a(n) = 2^n/3 + 2*cos((Pi*n)/3)/3 - 1. [Cournot]",
				"a(n) = 4*a(n-1) - 6*a(n-2) + 5*a(n-3) - 2*a(n-4) for n \u003e 3.",
				"(End)",
				"a(n) = (2^n+A087204(n))/3 - 1. - _R. J. Mathar_, Aug 07 2017"
			],
			"maple": [
				"seq(sum(binomial(n, k*3), k=1..n), n=0..33); # _Zerinvary Lajos_), Oct 23 2007"
			],
			"mathematica": [
				"LinearRecurrence[{4,-6,5,-2},{0,0,0,1},40] (* _Harvey P. Dale_, Jul 04 2017 *)"
			],
			"program": [
				"(PARI) concat(vector(3), Vec(x^3/((x-1)*(2*x-1)*(x^2-x+1)) + O(x^40))) \\\\ _Colin Barker_, Feb 10 2017"
			],
			"xref": [
				"Cf. A000295, A111926, A024495, A131708."
			],
			"keyword": "easy,nonn",
			"offset": "0,5",
			"author": "_Creighton Dement_, Aug 21 2005",
			"references": 8,
			"revision": 32,
			"time": "2020-09-24T04:06:35-04:00",
			"created": "2005-09-21T03:00:00-04:00"
		}
	]
}