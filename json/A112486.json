{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A112486",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 112486,
			"data": "1,1,1,2,5,3,6,26,35,15,24,154,340,315,105,120,1044,3304,4900,3465,945,720,8028,33740,70532,78750,45045,10395,5040,69264,367884,1008980,1571570,1406790,675675,135135,40320,663696,4302216,14777620,29957620",
			"name": "Coefficient triangle for polynomials used for e.g.f.s for unsigned Stirling1 diagonals.",
			"comment": [
				"The k-th diagonal of |A008275| appears as the k-th column in |A008276| with k-1 leading zeros.",
				"The recurrence, given below, is derived from (d/dx)g1(k,x) - g1(k,x)= x*(d/dx)g1(k-1,x) + g1(k-1,x), k \u003e= 1, with input g(-1,x):=0 and initial condition g1(k,0)=1, k \u003e= 0. This differential recurrence for the e.g.f. g1(k,x) follows from the one for unsigned Stirling1 numbers.",
				"The column sequences start with A000142 (factorials), A001705, A112487- A112491, for m=0,...,5.",
				"The main diagonal gives (2*k-1)!! = A001147(k), k \u003e= 1.",
				"This computation was inspired by the Bender article (see links), where the Stirling polynomials are discussed.",
				"The e.g.f. for the k-th diagonal, k \u003e= 1, of the unsigned Stirling1 triangle |A008275| with k-1 leading zeros is g1(k-1,x) = exp(x)*Sum_{m=0..k-1} a(k,m)*(x^(k-1+m))/(k-1+m)!.",
				"a(k,n) = number of lists with entries from [n] such that (i) each element of [n] occurs at least once and at most twice, (ii) for each i that occurs twice, all entries between the two occurrences of i are \u003e i, and (iii) exactly k elements of [n] occur twice. Example: a(1,2)=5 counts 112, 121, 122, 211, 221, and a(2,2)=3 counts 1122,1221,2211. - _David Callan_, Nov 21 2011"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A112486/b112486.txt\"\u003eTable of n, a(n) for the first 50 rows, flattened\u003c/a\u003e",
				"Roland Bacher, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v19i3p7\"\u003eCounting Packings of Generic Subsets in Finite Groups\u003c/a\u003e, Electr. J. Combinatorics, 19 (2012), #P7. - From _N. J. A. Sloane_, Feb 06 2013",
				"C. M. Bender, D. C. Brody and B. K. Meister, \u003ca href=\"https://arxiv.org/abs/math-ph/0509008\"\u003eBernoulli-like polynomials associated with Stirling Numbers\u003c/a\u003e, arXiv:math-ph/0509008 [math-ph], 2005.",
				"W. Lang, \u003ca href=\"/A112486/a112486.txt\"\u003eFirst 10 rows.\u003c/a\u003e"
			],
			"formula": [
				"a(k, m) = (k+m)*a(k-1, m) + (k+m-1)*a(k-1, m-1) for k \u003e= m \u003e= 0, a(0, 0)=1, a(k, -1):=0, a(k, m)=0 if k \u003c m.",
				"From _Tom Copeland_, Oct 05 2011: (Start)",
				"With polynomials",
				"P(0,t) = 0",
				"P(1,t) = 1",
				"P(2,t) = -(1 + t)",
				"P(3,t) = 2 + 5 t + 3 t^2",
				"P(4,t) = -( 6 + 26 t + 35 t^2 + 15 t^3)",
				"P(5,t) = 24 + 154 t +340 t^2 + 315 t^3 + 105 t^4",
				"Apparently, P(n,t) = (-1)^(n+1) PW[n,-(1+t)] where PW are the Ward polynomials A134991. If so, an e.g.f. for the polynomials is",
				"  A(x,t) = -(x+t+1)/t - LW{-((t+1)/t) exp[-(x+t+1)/t]}, where LW(x) is a suitable branch of the Lambert W Fct. (e.g., see A135338). The comp. inverse in x (about x = 0) is B(x) = x + (t+1) [exp(x) - x - 1]. See A112487 for special case t = 1. These results are a special case of A134685 with u(x) = B(x), i.e., u_1=1 and (u_n)=(1+t) for n\u003e0.",
				"Let h(x,t) = 1/(dB(x)/dx) = 1/[1+(1+t)*(exp(x)-1)], an e.g.f. in x for row polynomials in t of signed A028246 , then P(n,t), is given by",
				"(h(x,t)*d/dx)^n x, evaluated at x=0, i.e., A(x,t)=exp(x*h(u,t)*d/du) u, evaluated at u=0. Also, dA(x,t)/dx = h(A(x,t),t).",
				"The e.g.f. A(x,t) = -v * Sum_{j\u003e=1} D(j-1,u) (-z)^j / j! where u=-(x+t+1)/t, v=1+u, z=(1+t*v)/(t*v^2) and D(j-1,u) are the polynomials of A042977. dA/dx = -1/[t*(v-A)].(End)",
				"A133314 applied to the derivative of A(x,t) implies (a.+b.)^n = 0^n, for (b_n)=P(n+1,t) and (a_0)=1, (a_1)=t+1, and (a_n)=t*P(n,t) otherwise. E.g., umbrally, (a.+b.)^2 = a_2*b_0 + 2 a_1*b_1 + a_0*b_2 =0. - Tom Copeland, Oct 08 2011",
				"The row polynomials R(n,x) may be calculated using R(n,x) = 1/x^(n+1)*D^n(x), where D is the operator (x^2+x^3)*d/dx. - _Peter Bala_, Jul 23 2012",
				"For n\u003e0, Sum_{k=0..n} a(n,k)*(-1/(1+W(t)))^(n+k+1) = (t d/dt)^(n+1) W(t), where W(t) is Lambert W function. For t=-x, this gives Sum_{k\u003e=1} k^(k+n)*x^k/k! = - Sum_{k=0..n} a(n,k)*(-1/(1+W(-x)))^(n+k+1). - _Max Alekseyev_, Nov 21 2019"
			],
			"example": [
				"    1;",
				"    1,    1;",
				"    2,    5,     3;",
				"    6,   26,    35,    15;",
				"   24,  154,   340,   315,   105;",
				"  120, 1044,  3304,  4900,  3465,   945;",
				"  720, 8028, 33740, 70532, 78750, 45045, 10395;",
				"k=3 column of |A008276| is [0,0,2,11,35,85,175,...] (see A000914), its e.g.f. exp(x)*(2*x^2/2! + 5* x^3/3! + 3*x^4/4!)."
			],
			"maple": [
				"A112486 := proc(n,k)",
				"    if n \u003c 0 or k\u003c0 or  k\u003e n then",
				"        0 ;",
				"    elif n = 0 then",
				"        1 ;",
				"    else",
				"        (n+k)*procname(n-1,k)+(n+k-1)*procname(n-1,k-1) ;",
				"    end if;",
				"end proc: # _R. J. Mathar_, Dec 19 2013"
			],
			"mathematica": [
				"A112486 [n_, k_] := A112486[n, k] = Which[n\u003c0 || k\u003c0 || k\u003en, 0, n == 0, 1, True, (n+k)*A112486[n-1, k]+(n+k-1)*A112486[n-1, k-1]]; Table[A112486[n, k], {n, 0, 9}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Mar 05 2014, after _R. J. Mathar_ *)"
			],
			"xref": [
				"Cf. A112007 (triangle for o.g.f.s for unsigned Stirling1 diagonals). A112487 (row sums)."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,4",
			"author": "_Wolfdieter Lang_, Sep 12 2005",
			"references": 12,
			"revision": 61,
			"time": "2019-11-21T12:11:11-05:00",
			"created": "2005-09-21T03:00:00-04:00"
		}
	]
}