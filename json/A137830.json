{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A137830",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 137830,
			"data": "1,-2,0,0,4,-4,0,0,9,-12,0,0,20,-24,0,0,42,-50,0,0,80,-92,0,0,147,-172,0,0,260,-296,0,0,445,-510,0,0,744,-840,0,0,1215,-1372,0,0,1944,-2176,0,0,3059,-3424,0,0,4740,-5268,0,0,7239,-8040,0,0,10920",
			"name": "Expansion of phi(-x) / f(-x^4)^2 in powers of x where phi(), f() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A137830/b137830.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(1/3) * eta(q)^2 / (eta(q^2) * eta(q^4)^2) in powers of q.",
				"Euler transform of period 4 sequence [ -2, -1, -2, 1, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (72 t)) = (4/3)^(1/2) (t/i)^(-1/2) g(t) where q = exp(2 Pi i t) and g(t) is the g.f. for A137829.",
				"G.f.: ( Product_{k\u003e0} (1 - x^(2*k)) * (1 + x^k)^2 * (1 + x^(2*k))^2 )^(-1).",
				"a(4*n + 2) = a(4*n + 3) = 0.",
				"a(n) = (-1)^n * A137828(n). a(4*n) = A051136(n). a(4*n + 1) = -2 * A137829(n)."
			],
			"example": [
				"G.f. = 1 - 2*x + 4*x^4 - 4*x^5 + 9*x^8 - 12*x^9 + 20*x^12 - 24*x^13 + 42*x^16 + ...",
				"G.f. = 1/q - 2*q^2 + 4*q^11 - 4*q^14 + 9*q^23 - 12*q^26 + 20*q^35 - 24*q^38 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 4, 0, x] / QPochhammer[ x^4]^2, {x, 0, n}]; (* _Michael Somos_, Oct 04 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A)^2 / eta(x^2 + A) / eta(x^4 + A)^2, n))};"
			],
			"xref": [
				"Cf. A051136, A137828, A137829."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Feb 12 2008",
			"references": 2,
			"revision": 12,
			"time": "2021-03-12T22:24:45-05:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}