{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A076598",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 76598,
			"data": "1,5,10,17,26,50,50,65,91,130,122,170,170,250,260,257,290,455,362,442,500,610,530,650,651,850,820,850,842,1300,962,1025,1220,1450,1300,1547,1370,1810,1700,1690,1682,2500,1850,2074,2366,2650,2210,2570,2451,3255",
			"name": "Sum of squares of divisors d of n such that d or n/d is odd.",
			"link": [
				"Amiram Eldar, \u003ca href=\"/A076598/b076598.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"Multiplicative with a(2^e) = 4^e+1, a(p^e) = (p^(2*e+2)-1)/(p^2-1) for an odd prime p. G.f.: Sum_{m\u003e0} m^2*x^m*(1+2*x^m+3*x^(2*m))/(1+x^(2*m))/(1+x^m). More generally, if b(n, k) is sum of k-th powers of divisors d of n such that d or n/d is odd then b(n, k) = sigma_k(n)-2^k*sigma_k(n/4) if n mod 4=0, otherwise b(n, k) = sigma_k(n). G.f. for b(n, k): Sum_{m\u003e0} m^k*x^m*(1+x^m+x^(2*m)-(2^k-1)*x^(3*m))/(1-x^(4*m)). b(n, k) is multiplicative and b(2^e, k) = 2^(k*e)+1, b(p^e, k) = (p^(k*e+k)-1)/(p^k-1) for an odd prime p.",
				"a(n) = sigma_2(n)-4*sigma_2(n/4) if n mod 4=0, otherwise a(n) = sigma_2(n)."
			],
			"mathematica": [
				"f[2, e_] := 4^e+1 ; f[p_, e_] := (p^(2*e+2)-1)/(p^2-1) ; a[n_] := Times @@ (f @@@ FactorInteger[n]); Array[a, 50] (* _Amiram Eldar_, Aug 01 2019 *)"
			],
			"xref": [
				"Cf. A069733, A069184, A001157, A050999, A076577."
			],
			"keyword": "mult,nonn",
			"offset": "1,2",
			"author": "_Vladeta Jovovic_, Oct 20 2002",
			"references": 1,
			"revision": 9,
			"time": "2019-08-01T11:31:59-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}