{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A220002",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 220002,
			"data": "1,5,21,715,-162877,19840275,-7176079695,1829885835675,-5009184735027165,2216222559226679575,-2463196751104762933637,1679951011110471133453965,-5519118103058048675551057049,5373485053345792589762994345215,-12239617587594386225052760043303511",
			"name": "Numerators of the coefficients of an asymptotic expansion in even powers of the Catalan numbers.",
			"comment": [
				"Let N = 4*n+3 and A = sum_{k\u003e=0} a(k)/(A123854(k)*N^(2*k)) then",
				"C(n) ~ 8*4^n*A/(N*sqrt(N*Pi)), C(n) = (4^n/sqrt(Pi))*(Gamma(n+1/2)/ Gamma(n+2)) the Catalan numbers A000108.",
				"The asymptotic expansion of the Catalan numbers considered here is based on the Taylor expansion of square root of the sine cardinal. This asymptotic series involves only even powers of N, making it more efficient than the asymptotic series based on Stirling's approximation to the central binomial which involves all powers (see for example: D. E. Knuth, 7.2.1.6 formula (16)). The series is discussed by Kessler and Schiff but is included as a special case in the asymptotic expansion given by J. L. Fields for quotients Gamma(x+a)/Gamma(x+b) and discussed by Y. L. Luke (p. 34-35), apparently overlooked by Kessler and Schiff."
			],
			"reference": [
				"Donald E. Knuth, The Art of Computer Programming, Volume 4, Fascicle 4: Generating All Trees—History of Combinatorial Generation, 2006.",
				"Y. L. Luke, The Special Functions and their Approximations, Vol. 1. Academic Press, 1969."
			],
			"link": [
				"Peter Luschny, \u003ca href=\"/A220002/b220002.txt\"\u003eTable of n, a(n) for n = 0..99\u003c/a\u003e",
				"J. L. Fields, \u003ca href=\"https://doi.org/10.1017/S0013091500013171\"\u003eA note on the asymptotic expansion of a ratio of gamma functions\u003c/a\u003e, Proc. Edinburgh Math. Soc. 15 (1) (1966), 43-45.",
				"D. Kessler and J. Schiff, \u003ca href=\"http://u.math.biu.ac.il/~schiff/Papers/prepap3.pdf\"\u003eThe asymptotics of factorials, binomial coefficients and Catalan numbers\u003c/a\u003e. April 2006."
			],
			"formula": [
				"Let [x^n]T(f(x)) denote the coefficient of x^n in the Taylor expansion of f(x) then r(n) = (-1)^n*prod_{i=1..2n}(2i+1)*[x^(2*n)]T(sqrt(sin(x)/x)) is the rational coefficient of the asymptotic expansion (in N=4*n+3) and a(n) = numerator(r(n)) = r(n)*2^(3*n-bs(n)), where bs(n) is the binary sum of n (A000120).",
				"Also a(n) = numerator([x^(2*n)]T(exp(S))) where S = sum_{k\u003e=1}((4-E(2*k))/ (4*k)*x^(2*k)) and E(n) the Euler numbers A122045.",
				"Also a(n) = sf(4*n+1)*2^(3*n-bs(n))*F_{2*n}(-1/4) where sf(n) is the swinging factorial A056040, bs(n) the binary sum of n and F_{n}(x) J. L. Fields' generalized Bernoulli polynomials A220412.",
				"In terms of sequences this means",
				"r(n) = (-1)^n*A103639(n)*A008991(n)/A008992(n),",
				"a(n) = (-1)^n*A220371(n)*A008991(n)/A008992(n).",
				"Note that a(n) = r(n)*A123854(n) and A123854(n) = 2^A004134(n) = 8^n/2^A000120(n).",
				"Formula from _Johannes W. Meijer_:",
				"a(n) = d(n+1)*A098597(2*n+1)*(A008991(n)/A008992(n)) with d(1) = 1 and",
				"d(n+1) = -4*(2*n+1)*A161151(n)*d(n),",
				"d(n+1) = (-1)^n*2^(-1)*(2*(n+1))!*A060818(n)*A048896(n)."
			],
			"example": [
				"With N = 4*n+3 the first few terms of A are A = 1 + 5/(4*N^2) + 21/(32*N^4) + 715/(128*N^6) - 162877/(2048*N^8) + 19840275/(8192*N^10). With this A C(n) = round(8*4^n*A/(N*sqrt(N*Pi))) for n = 0..39 (if computed with sufficient numerical precision)."
			],
			"maple": [
				"A220002 := proc(n) local s; s := n -\u003e `if`(n \u003e 0, s(iquo(n,2))+n, 0);",
				"(-1)^n*mul(4*i+2, i = 1..2*n)*2^s(iquo(n,2))*coeff(taylor(sqrt(sin(x)/x), x,2*n+2), x, 2*n) end: seq(A220002(n), n = 0..14);",
				"# Second program illustrating J. L. Fields expansion of gamma quotients.",
				"A220002 := proc(n) local recF, binSum, swing;",
				"binSum := n -\u003e add(i,i=convert(n,base,2));",
				"swing := n -\u003e n!/iquo(n, 2)!^2;",
				"recF := proc(n, x) option remember; `if`(n=0, 1, -2*x*add(binomial(n-1,2*k+1)*bernoulli(2*k+2)/(2*k+2)*recF(n-2*k-2,x),k=0..n/2-1)) end: recF(2*n,-1/4)*2^(3*n-binSum(n))*swing(4*n+1) end:"
			],
			"mathematica": [
				"max = 14; CoefficientList[ Series[ Sqrt[ Sinc[x]], {x, 0, 2*max+1}], x^2][[1 ;; max+1]]*Table[ (-1)^n*Product[ (2*k+1), {k, 1, 2*n}], {n, 0, max}] // Numerator (* _Jean-François Alcover_, Jun 26 2013 *)"
			],
			"program": [
				"(Sage)",
				"length = 15; T = taylor(sqrt(sin(x)/x),x,0,2*length+2)",
				"def A005187(n): return A005187(n//2) + n if n \u003e 0 else 0",
				"def A220002(n):",
				"    P = mul(4*i+2 for i in (1..2*n)) \u003c\u003c A005187(n//2)",
				"    return (-1)^n*P*T.coefficient(x, 2*n)",
				"[A220002(n) for n in range(length)]",
				"(Sage) # Second program illustrating the connection with the Euler numbers.",
				"def A220002_list(n):",
				"    S = lambda n: sum((4-euler_number(2*k))/(4*k*x^(2*k)) for k in (1..n))",
				"    T = taylor(exp(S(2*n+1)),x,infinity,2*n-1).coefficients()",
				"    return [t[0].numerator() for t in T][::-1]",
				"A220002_list(15)"
			],
			"xref": [
				"The logarithmic version is A220422. Appears in A193365 and A220466.",
				"Cf. A220412."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Peter Luschny_, Dec 27 2012",
			"references": 8,
			"revision": 50,
			"time": "2020-03-16T14:26:42-04:00",
			"created": "2013-01-05T21:26:02-05:00"
		}
	]
}