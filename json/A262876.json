{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A262876",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 262876,
			"data": "1,0,1,0,1,2,1,2,4,2,7,6,7,12,12,16,26,22,35,44,47,68,84,88,133,146,176,238,267,324,431,468,604,746,842,1068,1296,1470,1884,2202,2579,3220,3753,4418,5483,6294,7541,9144,10554,12644,15191,17480,21057,24896",
			"name": "Expansion of Product_{k\u003e=1} 1/(1-x^(3*k-1))^k.",
			"comment": [
				"a(n) is the number of partitions of n into parts 3*k-1 of k kinds (k\u003e=1).",
				"In general, if s\u003e0, t\u003e0, GCD(s,t)=1 and g.f. = Product_{k\u003e=1} 1/(1 - x^(s*k-t))^k then a(n) ~ s^(t^2/(3*s^2) - 7/18) * n^(t^2/(6*s^2) - 25/36) * exp(d(s,t) - Pi^4 * t^2 / (432*s^2 * Zeta(3)) + Pi^2 * t * 2^(2/3) * s^(2/3) * n^(1/3) / (12 * s^2 * Zeta(3)^(1/3)) + 3*Zeta(3)^(1/3) * n^(2/3) / (2^(2/3)*s^(2/3))) / (2^(t^2/(6*s^2) + 11/36) * sqrt(3*Pi) * Zeta(3)^(t^2/(6*s^2) - 7/36)), where d(s,t) = Integral_{x=0..infinity} 1/x * (exp(-(s-t)*x)/(1 - exp(-s*x))^2 - 1/(s^2*x^2) - t/(s^2*x) + exp(-x)*(1/12 - t^2/(2*s^2))) dx. - _Vaclav Kotesovec_, Oct 12 2015"
			],
			"link": [
				"Vaclav Kotesovec, \u003ca href=\"/A262876/b262876.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Vaclav Kotesovec, \u003ca href=\"http://arxiv.org/abs/1509.08708\"\u003eA method of finding the asymptotics of q-series based on the convolution of generating functions\u003c/a\u003e, arXiv:1509.08708 [math.CO], Sep 30 2015",
				"Vaclav Kotesovec, \u003ca href=\"/A262876/a262876.jpg\"\u003eGraph - The asymptotic ratio (70000 terms)\u003c/a\u003e"
			],
			"formula": [
				"a(n) ~ Zeta(3)^(19/108) * exp(d1 - Pi^4 / (3888*Zeta(3)) + Pi^2 * n^(1/3) / (2^(4/3)*3^(7/3) * Zeta(3)^(1/3)) + 3^(1/3) * Zeta(3)^(1/3) * n^(2/3) / 2^(2/3)) / (2^(35/108) * 3^(23/27) * sqrt(Pi) * n^(73/108)), where d1 = A263030 = Integral_{x=0..infinity} 1/x*(exp(-2*x)/(1 - exp(-3*x))^2 - 1/(9*x^2) - 1/(9*x) + exp(-x)/36) = -0.188708191979528532376410098649207973592114467268429221509... . - _Vaclav Kotesovec_, Oct 08 2015"
			],
			"maple": [
				"with(numtheory):",
				"a:= proc(n) option remember; `if`(n=0, 1, add(add(d*",
				"      `if`(irem(d+3, 3, 'r')=2, r, 0), d=divisors(j))*a(n-j), j=1..n)/n)",
				"    end:",
				"seq(a(n), n=0..60);  # _Alois P. Heinz_, Oct 05 2015"
			],
			"mathematica": [
				"nmax=100; CoefficientList[Series[Product[1/(1-x^(3k-1))^k, {k, 1, nmax}], {x, 0, nmax}], x]",
				"nmax=100; CoefficientList[Series[E^Sum[1/j*x^(2*j)/(1-x^(3j))^2,{j,1,nmax}],{x,0,nmax}],x]"
			],
			"xref": [
				"Cf. A000219, A035528, A255610, A262877, A262878, A262879, A262883, A262946, A263136, A263141."
			],
			"keyword": "nonn",
			"offset": "0,6",
			"author": "_Vaclav Kotesovec_, Oct 04 2015",
			"references": 17,
			"revision": 26,
			"time": "2021-04-22T08:29:47-04:00",
			"created": "2015-10-04T07:08:08-04:00"
		}
	]
}