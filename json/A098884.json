{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A098884",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 98884,
			"data": "1,1,0,0,0,1,1,1,1,0,0,1,2,2,1,0,1,2,3,3,2,1,1,3,5,5,3,1,2,5,7,7,5,3,3,7,11,11,7,4,6,11,15,15,11,7,8,15,22,22,15,10,13,22,30,30,23,16,18,30,42,42,31,22,27,43,56,56,44,33,37,57,77,77,59,45,53,79,101,101,82,64,71",
			"name": "Number of partitions of n into distinct parts in which each part is congruent to 1 or 5 mod 6.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Convolution of A281244 and A280456. - _Vaclav Kotesovec_, Jan 18 2017"
			],
			"link": [
				"Reinhard Zumkeller and Vaclav Kotesovec, \u003ca href=\"/A098884/b098884.txt\"\u003eTable of n, a(n) for n = 0..20000\u003c/a\u003e (terms 0..300 from Reinhard Zumkeller)",
				"Noureddine Chair, \u003ca href=\"http://arxiv.org/abs/hep-th/0409011\"\u003ePartition Identities From Partial Supersymmetry\u003c/a\u003e, arXiv:hep-th/0409011v1, 2004.",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of chi(x) / chi(x^3) in powers of x where chi() is a Ramanujan theta function. - _Michael Somos_, Sep 20 2013",
				"Expansion of f(x^1, x^5) / f(-x^6) in powers of x where f(,) is a Ramanujan theta function. - _Michael Somos_, Sep 20 2013",
				"Expansion of G(x^6) * H(-x) + x * G(-x) * H(x^6) where G() (A003114), H() (A003106) are Rogers-Ramanujan functions.",
				"Expansion of q^(-1/12) * eta(q^2)^2 * eta(q^3) * eta(q^12) / (eta(q) * eta(q^4) * eta(q^6)^2) in powers of q.",
				"Euler transform of period 12 sequence [ 1, -1, 0, 0, 1, 0, 1, 0, 0, -1, 1, 0, ...]. - _Michael Somos_, Jun 26 2005",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (12 t)) = g(t) where q = exp(2 Pi i t) and g() is the g.f. for A227398. - _Michael Somos_, Sep 20 2013",
				"G.f.: Product_{k\u003e0} (1 - (-x)^k + x^(2*k)).",
				"G.f.: 1 / Product_{k\u003e0} (1 - x^(2*k - 1) + x^(4*k - 2)).",
				"G.f.: 1 / Product_{k\u003e0} ((1 + x^(6*k - 3)) / (1 + x^(2*k - 1))).",
				"G.f.: Product_{k\u003e0} ((1 + x^(6*k - 1)) * (1 + x^(6*k - 5))).",
				"G.f.: 1 / Product_{k\u003e0} (1  + (-x)^(3*k - 1)) * (1 + (-x)^(3*k - 2)).",
				"G.f.: (Sum_{k in Z} x^(k * (3*k - 2))) / (Sum_{k in Z} (-1)^k * x^(3*k * (3*k-1))).",
				"A109389(n) = (-1)^n * a(n). Convolution inverse of A227398.",
				"a(n) ~ exp(sqrt(n)*Pi/3)/ (2*sqrt(6)*n^(3/4)) * (1 + (Pi/72 - 9/(8*Pi)) / sqrt(n)). - _Vaclav Kotesovec_, Aug 30 2015, extended Jan 18 2017"
			],
			"example": [
				"E.g. a(25)=5 because 25=19+5+1=17+7+1=13+7+5=13+11+1.",
				"G.f. = 1 + x + x^5 + x^6 + x^7 + x^8 + x^11 + 2*x^12 + 2*x^13 + x^14 + x^16 + ...",
				"G.f. = q + q^13 + q^61 + q^73 + q^85 + q^97 + q^133 + 2*q^145 + 2*q^157 + q^169 + ..."
			],
			"maple": [
				"series(product((1+x^(6*k-1))*(1+x^(6*k-5)),k=1..100),x=0,100);"
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ Product[ 1 - (-x)^k + x^(2 k), {k, n}], {x, 0, n}]; (* _Michael Somos_, Sep 20 2013 *)",
				"a[ n_] := SeriesCoefficient[ 1 / Product[ 1 - x^k + x^(2 k), {k, 1, n, 2}], {x, 0, n}]; (* _Michael Somos_, Sep 20 2013 *)",
				"a[ n_] := SeriesCoefficient[ Product[ 1 + x^k, {k, 1, n, 2}] / Product[ 1 + x^k, {k, 3, n, 6}], {x, 0, n}]; (* _Michael Somos_, Sep 20 2013 *)",
				"a[ n_] := SeriesCoefficient[ Product[ 1 + x^k, {k, 1, n, 6}] Product[ 1 + x^k, {k, 5, n, 6}], {x, 0, n}]; (* _Michael Somos_, Sep 20 2013 *)",
				"a[ n_] := SeriesCoefficient[ QPochhammer[ -x, x^6] QPochhammer[ -x^5, x^6], {x, 0, n}]; (* _Michael Somos_, Sep 20 2013 *)",
				"a[ n_] := SeriesCoefficient[ QPochhammer[ -x, x^2] / QPochhammer[ -x^3, x^6], {x, 0, n}]; (* _Michael Somos_, Sep 20 2013 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^2 * eta(x^3 + A) * eta(x^12 + A) / (eta(x + A) * eta(x^4 + A) * eta(x^6 + A)^2), n))}; /* _Michael Somos_, Jun 26 2005 */",
				"(PARI) {a(n) = my(A, m); if( n\u003c0, 0, A = x * O(x^n); m = sqrtint(3*n + 1); polcoeff( sum(k= -((m-1)\\3), (m+1)\\3, x^(k * (3*k - 2)), A) / eta(x^6 + A), n))}; /* _Michael Somos_, Sep 20 2013 */",
				"(Haskell)",
				"a098884 = p a007310_list where",
				"   p _  0     = 1",
				"   p (k:ks) m = if k \u003e m then 0 else p ks (m - k) + p ks m",
				"-- _Reinhard Zumkeller_, Feb 19 2013"
			],
			"xref": [
				"Cf. A109389, A227398.",
				"Cf. A007310, A056970, A096981, A097451."
			],
			"keyword": "nonn",
			"offset": "0,13",
			"author": "_Noureddine Chair_, Oct 14 2004",
			"ext": [
				"Typo in Maple program fixed by _Vaclav Kotesovec_, Nov 15 2016"
			],
			"references": 10,
			"revision": 33,
			"time": "2021-03-12T22:24:43-05:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}