{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A232696",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 232696,
			"data": "1,1,1,-1,2,6,-264,5370,-93750,1315706,-3543736,-880688376,56549341380,-2612825765748,99009763750128,-2593891139126560,-31860555469490020,12585468136754891100,-1364794494618494224128,114095029934565534862680,-7984695190944325311086112,419424013080533747232201968",
			"name": "E.g.f. A(x) satisfies: A'(x) = A(x/A'(x)) with A(0)=1.",
			"comment": [
				" CONJECTURES.",
				"a(n) == 0 (mod 2) for n\u003e=4.",
				"a(n) == 0 (mod 2^2) for n\u003e=10.",
				"a(n) == 0 (mod 2^3) for n\u003e=18.",
				"a(n) == 0 (mod 2^k) for n\u003e=(8*n-6) for k\u003e1."
			],
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A232696/b232696.txt\"\u003eTable of n, a(n) for n = 0..160\u003c/a\u003e"
			],
			"formula": [
				"E.g.f. satisfies: A(x) = A'(x*A(x)).",
				"E.g.f. satisfies: A(x) = 1/x * Series_Reversion( x/A'(x) ).",
				"a(n) = [x^n/n!] A'(x)^(n+1)/(n+1) for n\u003e=0."
			],
			"example": [
				"E.g.f.: A(x) = 1 + x + x^2/2! - x^3/3! + 2*x^4/4! + 6*x^5/5! - 264*x^6/6! + 5370*x^7/7! +...",
				"such that A(x) = A'(x*A(x)) and",
				"A(x/A'(x)) = A'(x) = 1 + x - x^2/2! + 2*x^3/3! + 6*x^4/4! - 264*x^5/5! +...",
				"To illustrate a(n) = [x^n/n!] A'(x)^(n+1)/(n+1), create a table of coefficients of x^k/k!, k\u003e=0, in A'(x)^n like so:",
				"A'^1: [1, 1, -1,   2,   6, -264,  5370,  -93750,  1315706, ...];",
				"A'^2: [1, 2,  0,  -2,  34, -508,  7472, -100392,   774076, ...];",
				"A'^3: [1, 3,  3,  -6,  48, -522,  6036,  -54030,  -435618, ...];",
				"A'^4: [1, 4,  8,  -4,  36, -336,  2832,    3672, -1469680, ...];",
				"A'^5: [1, 5, 15,  10,  10, -100,  -130,   44490, -1964390, ...];",
				"A'^6: [1, 6, 24,  42,   6,   36, -1680,   59520, -1938564, ...];",
				"A'^7: [1, 7, 35,  98,  84,   42, -1848,   54978, -1605394, ...];",
				"A'^8: [1, 8, 48, 184, 328,  128, -1504,   42960, -1194368, ...];",
				"A'^9: [1, 9, 63, 306, 846,  864, -1278,   32202,  -843750, ...]; ...",
				"then the diagonal in the above table generates this sequence:",
				"[1/1, 2/2, 3/3, -4/4, 10/5, 36/6, -1848/7, 42960/8, -843750/9, ...].",
				"SUMS OF TERM RESIDUES MODULO 2^n.",
				"Given a(k) == 0 (mod 2^n) for k\u003e=(8*n-6) for n\u003e1, then it is interesting to consider the sums of the residues of all terms modulo 2^n for n\u003e=1.",
				"Let b(n) = Sum_{k\u003e=0} a(k) (mod 2^n) for n\u003e=1, then the sequence {b(n)} begins:",
				"[4, 16, 40, 144, 432, 1008, 3184, 6384, 15600, 33520, 75504, 159472, 356080, 798448, 1797872, 3895024, 8089328, 16609008, 37842672, 76639984, 166817520, ...]."
			],
			"program": [
				"(PARI) {a(n)=local(A=1+x); for(i=1, n, A=1+intformal(subst(A, x, x/A' +x*O(x^n)))); n!*polcoeff(A, n)}",
				"for(n=0, 25, print1(a(n), \", \"))",
				"(PARI) {a(n)=local(A=1+x); for(i=1, n, A=1+intformal(x/serreverse(x*A +x*O(x^n)))); n!*polcoeff(A, n)}",
				"for(n=0, 25, print1(a(n), \", \"))"
			],
			"xref": [
				"Cf. A231619, A231866, A231899, A232694, A232695."
			],
			"keyword": "sign",
			"offset": "0,5",
			"author": "_Paul D. Hanna_, Nov 28 2013",
			"references": 7,
			"revision": 11,
			"time": "2013-12-03T20:51:28-05:00",
			"created": "2013-11-28T16:31:43-05:00"
		}
	]
}