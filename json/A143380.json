{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A143380",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 143380,
			"data": "1,2,0,0,1,-2,0,0,-3,0,0,0,-2,-2,0,0,2,-2,0,0,-1,2,0,0,0,2,0,0,1,2,0,0,2,-2,0,0,4,2,0,0,-2,0,0,0,0,2,0,0,-1,0,0,0,-2,0,0,0,2,-4,0,0,-1,-2,0,0,0,0,0,0,-2,0,0,0,-2,-2,0,0,-2,2,0,0,0",
			"name": "Expansion of q^(-1/6) * eta(q^2)^5 / (eta(q)^2 * eta(q^4)) in powers of q.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A143380/b143380.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of psi(x)^2 * chi(-x^2) in powers of x where phi(), chi() are Ramanujan theta functions.",
				"Euler transform of period 4 sequence [ 2, -3, 2, -2, ...].",
				"a(n) = (-1)^(-n / 2) * b(6*n + 1) where b() is multiplicative with b(2^e) = b(3^e) = 0^e, b(p^e) = (1 + (-1)^e) / 2 if p == 5 (mod 8) or p == 23 (mod 24), b(p^e) = (-1)^(e/2) * (1 + (-1)^e) / 2 if p == 3 (mod 8) or p == 17 (mod 24) and p\u003e3, b(p^e) = (e+1) * s^e if p == 1, 7 (mod 24) where p = x^2 + 6*y^2 and s = Kronecker(12, x) * (-1)^((p-1) / 12).",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (576 t)) = 1152^(1/2) (t/i) g(t) where q = exp(2 Pi i t) and g(t) is the g.f. for A143378.",
				"a(4*n + 2) = a(4*n + 3) = 0.",
				"G.f.: Product_{k\u003e0} (1 - (-x)^k)^2 * (1 + x^(2*k)).",
				"a(n) = (-1)^n * A143377(n). a(4*n) = A143378(n). a(4*n + 1) = 2 * A143379(n)."
			],
			"example": [
				"G.f. = 1 + 2*x + x^4 - 2*x^5 - 3*x^8 - 2*x^12 - 2*x^13 + 2*x^16 - 2*x^17 + ...",
				"G.f. = q + 2*q^7 + q^25 - 2*q^31 - 3*q^49 - 2*q^73 - 2*q^79 + 2*q^97 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x^2]^5 / (QPochhammer[ x]^2 QPochhammer[ x^4]), {x, 0, n}]; (* _Michael Somos_, Sep 06 2015 *)"
			],
			"program": [
				"(PARI) {a(n)= my(A, p, e, x); if(n\u003c0, 0, A = factor(6*n + 1); simplify( I^-n * prod(k=1, matsize(A)[1], [p, e] = A[k, ]; if(p\u003c5, 0, p%8==5 || p%24==23, !(e%2), p%8==3 || p%24==17, (-1)^(e\\2)*!(e%2), for(i=1, sqrtint(p\\6), if( issquare(p - 6*i^2, \u0026x), break)); (e+1) * (kronecker(12, x) * I^((p-1) / 6))^e ))))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^5 / (eta(x + A)^2 * eta(x^4 + A)), n))};"
			],
			"xref": [
				"Cf. A143377, A143378, A143379."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Aug 11 2008",
			"references": 4,
			"revision": 14,
			"time": "2021-03-12T22:24:45-05:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}