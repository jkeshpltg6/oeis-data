{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A045778",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 45778,
			"data": "1,1,1,1,1,2,1,2,1,2,1,3,1,2,2,2,1,3,1,3,2,2,1,5,1,2,2,3,1,5,1,3,2,2,2,5,1,2,2,5,1,5,1,3,3,2,1,7,1,3,2,3,1,5,2,5,2,2,1,9,1,2,3,4,2,5,1,3,2,5,1,9,1,2,3,3,2,5,1,7,2,2,1,9,2,2,2,5,1,9,2,3,2,2,2,10,1,3,3,5,1,5,1,5",
			"name": "Number of factorizations of n into distinct factors greater than 1.",
			"comment": [
				"This sequence depends only on the prime signature of n and not on the actual value of n.",
				"Also the number of strict multiset partitions (sets of multisets) of the prime factors of n. - _Gus Wiseman_, Dec 03 2016"
			],
			"link": [
				"David W. Wilson, \u003ca href=\"/A045778/b045778.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Philippe A. J. G. Chevalier, \u003ca href=\"https://biblio.ugent.be/publication/8049761/file/8049762\"\u003eOn the discrete geometry of physical quantities\u003c/a\u003e, Preprint, 2012.",
				"P. A. J. G. Chevalier, \u003ca href=\"https://www.researchgate.net/profile/Philippe_Chevalier2/publication/260598331_On_a_Mathematical_Method_for_Discovering_Relations_Between_Physical_Quantities_a_Photonics_Case_Study/links/00b7d531be7b837626000000.pdf\"\u003eOn a Mathematical Method for Discovering Relations Between Physical Quantities: a Photonics Case Study\u003c/a\u003e, Slides from a talk presented at ICOL2014.",
				"P. A. J. G. Chevalier, \u003ca href=\"http://www.researchgate.net/profile/Philippe_Chevalier2/publication/262067273_A_table_of_Mendeleev_for_physical_quantities/links/0c9605368f6d191478000000.pdf\"\u003eA \"table of Mendeleev\" for physical quantities?\u003c/a\u003e, Slides from a talk, May 14 2014, Leuven, Belgium.",
				"A. Knopfmacher, M. Mays, Ordered and Unordered Factorizations of Integers: \u003ca href=\"http://www.mathematica-journal.com/issue/v10i1/contents/Factorizations/Factorizations_3.html\"\u003eUnordered Factorizations with Distinct Parts\u003c/a\u003e, The Mathematica Journal 10(1), 2006.",
				"R. J. Mathar, \u003ca href=\"/A045778/a045778.txt\"\u003eFactorizations of n =1..1500\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/UnorderedFactorization.html\"\u003eUnordered Factorization\u003c/a\u003e",
				"\u003ca href=\"/index/Eu#epf\"\u003eIndex entries for sequences computed from exponents in factorization of n\u003c/a\u003e"
			],
			"formula": [
				"Dirichlet g.f.: Product_{n\u003e=2}(1 + 1/n^s).",
				"Let p and q be two distinct prime numbers and k a natural number. Then a(p^k) = A000009(k) and a(p^k*q) = A036469(k). - _Alexander Adam_, Dec 28 2012",
				"Let p_i with 1\u003c=i\u003c=k k distinct prime numbers. Then a(Product_{i=1..k} p_i) = A000110(k). - _Alexander Adam_, Dec 28 2012"
			],
			"example": [
				"24 can be factored as 24, 2*12, 3*8, 4*6, or 2*3*4, so a(24) = 5. The factorization 2*2*6 is not permitted because the factor 2 is present twice. a(1) = 1 represents the empty factorization."
			],
			"maple": [
				"with(numtheory):",
				"b:= proc(n, k) option remember;",
				"      `if`(n\u003ek, 0, 1) +`if`(isprime(n), 0,",
				"      add(`if`(d\u003ek, 0, b(n/d, d-1)), d=divisors(n) minus {1, n}))",
				"    end:",
				"a:= n-\u003e b(n$2):",
				"seq(a(n), n=1..120);  # _Alois P. Heinz_, May 26 2013"
			],
			"mathematica": [
				"gd[m_, 1] := 1; gd[1, n_] := 0; gd[1, 1] := 1; gd[0, n_] := 0; gd[m_, n_] := gd[m, n] = Total[gd[# - 1, n/#] \u0026 /@ Select[Divisors[n], # \u003c= m \u0026]]; Array[ gd[#, #] \u0026, 100]  (* _Alexander Adam_, Dec 28 2012 *)"
			],
			"program": [
				"(PARI) v=vector(100,k,k==1); for(n=2,#v, v+=dirmul(v,vector(#v,k,k==n)) ); v /* _Max Alekseyev_, Jul 16 2014 */",
				"(PARI)",
				"A045778aux(n, k) = ((n\u003c=k) + sumdiv(n, d, if(d \u003e 1 \u0026\u0026 d \u003c= k \u0026\u0026 d \u003c n, A045778aux(n/d, d-1))));",
				"A045778(n) = A045778aux(n,n); \\\\ After _Alois P. Heinz_'s Maple-code by _Antti Karttunen_, Jul 23 2017",
				"(Python)",
				"from sympy.core.cache import cacheit",
				"from sympy import divisors, isprime",
				"@cacheit",
				"def b(n, k): return (0 if n\u003ek else 1) + (0 if isprime(n) else sum(0 if d\u003ek else b(n//d, d - 1) for d in divisors(n)[1:-1]))",
				"def a(n): return b(n, n)",
				"print([a(n) for n in range(1, 121)]) # _Indranil Ghosh_, Aug 19 2017, after Maple code"
			],
			"xref": [
				"Cf. A001055, A045779, A045780, A050323. a(p^k)=A000009. a(A002110) = A000110.",
				"Cf. A036469, A114591, A114592, A316441 (Dirichlet inverse)."
			],
			"keyword": "nonn,easy,nice",
			"offset": "1,6",
			"author": "_David W. Wilson_",
			"ext": [
				"Edited by _Franklin T. Adams-Watters_, Jun 04 2009"
			],
			"references": 253,
			"revision": 69,
			"time": "2020-04-23T08:13:20-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}