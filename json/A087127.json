{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A087127",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 87127,
			"data": "1,1,2,1,1,8,19,18,6,1,26,163,432,564,360,90,1,80,1135,6354,18078,28800,26100,12600,2520,1,242,7291,77400,405060,1210680,2211570,2520000,1751400,680400,113400,1,728,45199,862218,7667646,38350080,118848420",
			"name": "This table shows the coefficients of combinatorial formulas needed for generating the sequential sums of p-th powers of triangular numbers. The p-th row (p\u003e=1) contains a(i,p) for i=1 to 2*p-1, where a(i,p) satisfies Sum_{i=1..n} C(i+1,2)^p = 3 * C(n+2,3) * Sum_{i=1..2*p-1} a(i,p) * C(n-1,i-1)/(i+2).",
			"comment": [
				"From _Peter Bala_, Mar 08 2018: (Start)",
				"The table entries T(n,k) are the coefficients when expressing the polynomial C(x+2,2)^p of degree 2*p in terms of falling factorials: C(x+2,2)^p = Sum_{k = 0..2*p} T(p,k)*C(x,k). It follows that Sum_{i = 0..n-1} C(i+2,2)^p = Sum_{k = 0..2*p} T(p,k)*C(n,k+1).",
				"The sum of the p-th powers of the triangular numbers is also given by Sum_{i = 0..n-1} C(i+2,2)^p = Sum_{k = 2..2*p} A122193(p,k)*C(n+2,k+1) for p \u003e= 1. (End)"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A087127/b087127.txt\"\u003eTable of n, a(n) for the first 50 rows, flattened\u003c/a\u003e",
				"M. Dukes, C. D. White, \u003ca href=\"http://arxiv.org/abs/1603.01589\"\u003eWeb Matrices: Structural Properties and Generating Combinatorial Identities\u003c/a\u003e, arXiv:1603.01589 [math.CO], 2016."
			],
			"formula": [
				"a(1, p) = 1, a(2, p) = 3^(p-1)-1, a(3, p) = 3^(p-1)*[2^(p-1)-2]+1, ..., a(2*p-3, p) = [ (6*p^4-20*p^3+21*p^2-7*p)*(2*p-4)! ]/[3*2^(p-1)], a(2*p-2, p) = [ (p^2-p)*(2*p-3)! ]/2^(p-2), a(2*p-1, p) = [ (p-1)*(2*p-3)! ]/2^(p-2).",
				"a(i, p) = Sum_{k=1..[2*i+1+(-1)^(i-1)]/4} [ C(i-1, 2*k-2)*C(i-2*k+3, i-2*k+1)^(p-1) -C(i-1, 2*k-1)*C(i-2*k+2, i-2*k)^(p-1) ]",
				"From _Peter Bala_, Mar 08 2018: (Start)",
				"The following remarks assume row and column indices start at 0.",
				"T(n,k) = Sum_{i = 0..k} (-1)^(k-i)*C(k,i)*C(i+2,2)^n. Equivalently, let v_n denote the sequence (1, 3^n, 6^n, 10^n, ...) regarded as an infinite column vector, where 1, 3, 6, 10, ... is the sequence of triangular numbers A000217. Then the n-th row of this table is determined by the matrix product P^(-1)*v_n, where P denotes Pascal's triangle A007318. Cf. A122193.",
				"T(n+1,k) = C(k+2,2)*T(n,k) + 2*C(k+1,2)*T(n,k-1) + C(k,2)*T(n,k-2), with boundary conditions T(n,0) = 1 for all n and T(n,k) = 0 for k \u003e 2*n.",
				"Let R(n,x) denote the n-th row polynomial.",
				"R(n+1,x) = 1/2!*(1 + x)^2*(d/dx)^2 (x^2*R(n,x)).",
				"R(n,x) = Sum_{i \u003e= 0} binomial(i+2,2)^n*x^i/(1 + x)^(i+1).",
				"R(n,x) = (1 + x)^2 o (1 + x)^2 o ... o (1 + x)^2 (n factors), where o denotes the black diamond product of power series defined in Dukes and White. Note the polynomial x^2 o ... o x^2 (n factors) is the n-th row polynomial of A122193.",
				"x^2*R(n,x) = (1 + x)^2 * the n-th row polynomial of A122193 (End)"
			],
			"example": [
				"Row 3 contains 1,8,19,18,6, so Sum_{i=1..n} C(i+1,2)^3 = (n+2) * C(n+1,2) * [ a(1,3)/3 + a(2,3)*C(n-1,1)/4 + a(3,3)*C(n-1,2)/5 + a(4,3)*C(n-1,3)/6 + a(5,3)*C(n-1,4)/7 ] = [ (n+2)*(n+1)*n/2 ] * [ 1/3 + (8/4)*C(n-1,1) + (19/5)*C(n-1,2) + (18/6)*C(n-1,3) + (6/7)*C(n-1,4). Cf. A085438 for more details.",
				"From _Peter Bala_, Mar 08 2018: (Start)",
				"Table begins",
				"n=0 |1",
				"n=1 |1   2     1",
				"n=2 |1   8    19    18      6",
				"n=3 |1  26   163   432    564    360     90",
				"n=4 |1  80  1135  6354  18078  28800  26100  12600  2520",
				"...",
				"Row 2: C(i+2,2)^2 = C(i,0) + 8*C(i,1) + 19*C(i,2) + 18*C(i,3) + 6*C(i,4). Hence, Sum_{i = 0..n-1} C(i+2,2)^2 =  C(n,1) + 8*C(n,2) + 19*C(n,3) + 18*C(n,4) + 6*C(n,5). (End)"
			],
			"maple": [
				"seq(seq(add( (-1)^(k-i)*binomial(k,i)*binomial(i+2,2)^n, i = 0..k), k = 0..2*n), n = 0..8); # _Peter Bala_, Mar 08 2018"
			],
			"mathematica": [
				"a[i_, p_] := Sum[Binomial[i - 1, 2*k - 2]*Binomial[i - 2*k + 3, i - 2*k + 1]^(p - 1) - Binomial[i - 1, 2*k - 1]*Binomial[i - 2*k + 2, i - 2*k]^(p - 1), {k, 1, (2*i + 1 + (-1)^(i - 1))/4}]; Table[If[p == 1, 1, a[i, p]], {p, 1, 10}, {i, 1, 2*p - 1}]//Flatten (* _G. C. Greubel_, Nov 23 2017 *)",
				"a[i_,p_]:=(-1)^i HypergeometricPFQ[ConstantArray[3,p]~Join~{2-i},ConstantArray[1,p],1];Table[a[i,p],{p,0,10},{i,2,2 p+2}]//Flatten (* _Jonathan Burns_, Mar 20 2018 *)"
			],
			"program": [
				"(PARI) {a(i, p) = sum(k=1, (2*i + 1 + (-1)^(i - 1))/4, binomial(i - 1, 2*k - 2)*binomial(i - 2*k + 3, i - 2*k + 1)^(p - 1) - binomial(i - 1, 2*k - 1)*binomial(i - 2*k + 2, i - 2*k)^(p - 1))}; for(p=1,8, for(i=1, 2*p-1, print1(if(p==1,1,a(i,p)), \", \"))) \\\\ _G. C. Greubel_, Nov 23 2017",
				"(GAP) Flat(List([0..6],n-\u003eList([0..2*n],k-\u003eSum([0..k],i-\u003e(-1)^(k-i)*Binomial(k,i)*Binomial(i+2,2)^n)))); # _Muniru A Asiru_, Mar 22 2018"
			],
			"xref": [
				"Cf. A000292, A024166, A024166, A085438, A085439, A085440, A085441, A085442, A087107, A000332, A086020, A086021, A086022, A087108, A000389, A086023, A086024, A087109, A000579, A086025, A086026, A087110, A000580, A086027, A086028, A087111, A027555, A086029, A086030.",
				"Cf. A000217, A122193."
			],
			"keyword": "easy,nonn,tabf",
			"offset": "1,3",
			"author": "_André F. Labossière_, Aug 11 2003",
			"ext": [
				"Edited by _Dean Hickerson_, Aug 16 2003"
			],
			"references": 38,
			"revision": 28,
			"time": "2018-04-20T01:07:28-04:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}