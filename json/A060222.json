{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A060222",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 60222,
			"data": "19,171,2280,32490,495216,7839780,127695960,2122929090,35854187880,613106378136,10590023536200,184442905990860,3234844881712080,57071906063500860,1012075135324821024,18027588346914850290,322375697516753069760,5784852794310472599780,104127350297911241532840",
			"name": "Number of orbits of length n under the full 19-shift (whose periodic points are counted by A001029).",
			"comment": [
				"Number of monic irreducible polynomials of degree n over GF(19). - _Andrew Howroyd_, Dec 10 2017"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A060222/b060222.txt\"\u003eTable of n, a(n) for n = 1..799\u003c/a\u003e",
				"Y. Puri and T. Ward, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL4/WARD/short.html\"\u003eArithmetic and growth of periodic orbits\u003c/a\u003e, J. Integer Seqs., Vol. 4 (2001), #01.2.1.",
				"Yash Puri and Thomas Ward, \u003ca href=\"http://www.fq.math.ca/Scanned/39-5/puri.pdf\"\u003eA dynamical property unique to the Lucas sequence\u003c/a\u003e, Fibonacci Quarterly, Volume 39, Number 5 (November 2001), pp. 398-402.",
				"T. Ward, \u003ca href=\"https://web.archive.org/web/20110921045528/http://www.uea.ac.uk:80/~h720/research/files/integersequences.html\"\u003eExactly realizable sequences\u003c/a\u003e"
			],
			"formula": [
				"a(n) = (1/n)* Sum_{d|n} mu(d)*A001029(n/d).",
				"G.f.: Sum_{k\u003e=1} mu(k)*log(1/(1 - 19*x^k))/k. - _Ilya Gutkovskiy_, May 20 2019"
			],
			"example": [
				"a(2)=171 since there are 361 points of period 2 in the full 19-shift and 19 fixed points, so there must be (361-19)/2 = 171 orbits of length 2."
			],
			"mathematica": [
				"a[n_]:=(1/n) Sum[MoebiusMu[d] 19^(n/d), {d, Divisors[n]}]; Table[a[n], {n, 20}] (* _Vincenzo Librandi_, Sep 19 2017 *)"
			],
			"program": [
				"(PARI) a001029(n) = 19^n;",
				"a(n) = (1/n)*sumdiv(n, d, moebius(d)*a001029(n/d)); \\\\ _Michel Marcus_, Sep 11 2017"
			],
			"xref": [
				"Column 19 of A074650.",
				"Cf. A001029."
			],
			"keyword": "easy,nonn",
			"offset": "1,1",
			"author": "_Thomas Ward_, Mar 21 2001",
			"ext": [
				"More terms from _Michel Marcus_, Sep 11 2017"
			],
			"references": 2,
			"revision": 22,
			"time": "2021-02-19T20:10:00-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}