{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A350330",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 350330,
			"data": "1,1,2,1,1,2,2,1,2,1,1,2,1,2,3,1,1,2,1,1,2,2,1,2,1,1,2,2,3,2,2,1,1,2,2,3,1,1,2,1,1,2,2,1,2,1,1,2,1,2,3,1,1,2,1,1,2,2,1,2,1,1,2,2,3,2,2,1,1,2,3,1,1,2,1,1,2,2,1,2,1,1,2,1,2,3,1",
			"name": "Lexicographically earliest sequence of positive integers such that the Hankel matrix of any odd number of consecutive terms is invertible.",
			"comment": [
				"No linear relation of the form c_1*a(j) + ... + c_k*a(j+k-1) = 0, with at least one c_i nonzero, holds for k consecutive values of j.",
				"Is a(n) \u003c= 3 for all n? (It is true for n \u003c= 400.) If not, what is the largest term? Or is the sequence unbounded?",
				"There seems to be some regularity in the sequence of values of n for which a(n) \u003e 2: 15, 29, 36, 51, 65, 71, 86, 100, ... . The first differences of these are: 14, 7, 15, 14, 6, 15, 14, 5, 15, 14, 3, 15, 14, 1, 15, 13, 11, 15, 14, 7, 15, 14, 5, 15, 14, 3, 15, 14, 1, ... . The differences are all less than or equal to 15, because A350364(15,2) = 0.",
				"Agrees with A154402 for the first 20 terms, but differs on the 21st."
			],
			"link": [
				"Pontus von Brömssen, \u003ca href=\"/A350330/b350330.txt\"\u003eTable of n, a(n) for n = 1..400\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Hankel_matrix\"\u003eHankel matrix\u003c/a\u003e"
			],
			"example": [
				"a(15) = 3, because the Hankel matrix of (a(11), ..., a(15)) is",
				"  [1  2   1  ]",
				"  [2  1   2  ]",
				"  [1  2 a(15)],",
				"which is singular if a(15) = 1, and the Hankel matrix of (a(5), ..., a(15)) is",
				"  [1  2  2  1  2   1  ]",
				"  [2  2  1  2  1   1  ]",
				"  [2  1  2  1  1   2  ]",
				"  [1  2  1  1  2   1  ]",
				"  [2  1  1  2  1   2  ]",
				"  [1  1  2  1  2 a(15)],",
				"which is singular if a(15) = 2, but if a(15) = 3 the Hankel matrix of (a(k), ..., a(15)) is invertible for all odd k \u003c= 15."
			],
			"program": [
				"(Python)",
				"from sympy import Matrix",
				"from itertools import count",
				"def A350330_list(nmax):",
				"    a=[]",
				"    for n in range(nmax):",
				"        a.append(next(k for k in count(1) if all(Matrix((n-r)//2+1,(n-r)//2+1,lambda i,j:(a[r:]+[k])[i+j]).det()!=0 for r in range(n-2,-1,-2))))",
				"    return a",
				"(Python)",
				"# Faster version using numpy instead of sympy.",
				"# Due to floating point errors, the results may be inaccurate for large n. Correctness verified up to n=400 for numpy 1.20.2.",
				"from numpy import array",
				"from numpy.linalg import det",
				"from itertools import count",
				"def A350330_list(nmax):",
				"    a=[]",
				"    for n in range(nmax):",
				"        a.append(next(k for k in count(1) if all(abs(det(array([[(a[r:]+[k])[i+j] for j in range((n-r)//2+1)] for i in range((n-r)//2+1)])))\u003e0.5 for r in range(n-2,-1,-2))))",
				"    return a"
			],
			"xref": [
				"Cf. A350351, A350348, A350349, A350350, A350364, A154402."
			],
			"keyword": "nonn,changed",
			"offset": "1,3",
			"author": "_Pontus von Brömssen_, Dec 25 2021",
			"references": 7,
			"revision": 20,
			"time": "2022-01-04T18:53:32-05:00",
			"created": "2021-12-25T14:53:24-05:00"
		}
	]
}