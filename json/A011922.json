{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A011922",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 11922,
			"data": "1,3,33,451,6273,87363,1216801,16947843,236052993,3287794051,45793063713,637815097923,8883618307201,123732841202883,1723376158533153,24003533378261251,334326091137124353",
			"name": "a(n) = 15*a(n-1) - 15*a(n-2) + a(n-3).",
			"reference": [
				"Mario Velucchi, Seeing couples, in Recreational and Educational Computing, to appear 1997."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A011922/b011922.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e",
				"Christian Aebi, and Grant Cairns, \u003ca href=\"https://arxiv.org/abs/2006.07566\"\u003eLattice Equable Parallelograms\u003c/a\u003e, arXiv:2006.07566 [math.NT], 2020.",
				"Hacène Belbachir, Soumeya Merwa Tebtoub, László Németh, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL23/Nemeth/nemeth7.html\"\u003eEllipse Chains and Associated Sequences\u003c/a\u003e, J. Int. Seq., Vol. 23 (2020), Article 20.8.5.",
				"Z. Franusic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL13/Franusic/franusic4.html\"\u003eOn the Extension of the Diophantine Pair {1,3} in Z[surd d]\u003c/a\u003e, J. Int. Seq. 13 (2010) # 10.9.6.",
				"Giovanni Lucca, \u003ca href=\"http://forumgeom.fau.edu/FG2016volume16/FG201654.pdf\"\u003eCircle Chains Inscribed in Symmetrical Lenses and Integer Sequences\u003c/a\u003e, Forum Geometricorum, Volume 16 (2016) 419-427.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (15,-15,1)."
			],
			"formula": [
				"a(n) = (2+sqrt(1+((((2+sqrt(3))^(2*n)-(2-sqrt(3))^(2*n))^2)/4)))/3.",
				"a(n) = ((7+4*sqrt(3))^n+(7-4*sqrt(3))^n+4)/6. - _Bruno Berselli_, Jul 09 2011",
				"G.f.: (1-12*x+3*x^2)/ ((1-x) * (x^2-14*x+1)). - _R. J. Mathar_, Apr 15 2010",
				"Sqrt(3) = 1 + sum(n\u003e=1, 2/a(n)) = 1 + 2/3 + 2/33 +... - _Gary W. Adamson_, Jun 12 2003",
				"a(n)^2 = A103974(n+1)^2 - (4*A007655(n+1))^2. - _Paul D. Hanna_, Mar 06 2005",
				"a(n) = (A011943(n+1) + 2)/3. - _Ralf Stephan_, Aug 13 2013",
				"a(n) = A001075(n)^2 - A001353(n)^2. - _Richard R. Forberg_, Aug 24 2013"
			],
			"maple": [
				"a:= gfun:-rectoproc({a(n) = 15*a(n-1) - 15*a(n-2) + a(n-3), a(0)=1,a(1)=3,a(2)=33},a(n),remember):",
				"map(a,[$0..100]); # _Robert Israel_, Jul 02 2015"
			],
			"mathematica": [
				"RecurrenceTable[{a[n] == 15 a[n - 1] - 15 a[n - 2] + a[n - 3], a[0] == 1, a[1] == 3, a[2] == 33}, a, {n, 0, 15}] (* _Michael De Vlieger_, Jul 02 2015 *)",
				"LinearRecurrence[{15,-15,1},{1,3,33},30] (* _Harvey P. Dale_, Dec 04 2018 *)"
			],
			"program": [
				"(Maxima) a[0]:1$ a[1]:3$ a[2]:33$ a[n]:=15*a[n-1]-15*a[n-2]+a[n-3]$ makelist(a[n], n, 0, 16);  \\\\ _Bruno Berselli_, Jul 09 2011",
				"(MAGMA) I:=[1,3,33]; [n le 3 select I[n] else 15*Self(n-1)-15*Self(n-2)+Self(n-3): n in [1..17]];  // _Bruno Berselli_, Jul 09 2011",
				"(PARI) a(n)=([0,1,0; 0,0,1; 1,-15,15]^n*[1;3;33])[1,1] \\\\ _Charles R Greathouse IV_, Jul 02 2015"
			],
			"xref": [
				"Cf. A011916, A011918, A011920, A103974, A007655."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "Mario Velucchi (mathchess(AT)velucchi.it)",
			"ext": [
				"Formula corrected by Francisco Salinas (franciscodesalinas(AT)hotmail.com), Dec 30 2001",
				"Recurrence in definition by _R. J. Mathar_, Apr 15 2010"
			],
			"references": 9,
			"revision": 63,
			"time": "2020-12-28T20:32:24-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}