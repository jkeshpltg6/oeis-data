{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007351",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7351,
			"id": "M1507",
			"data": "2,5,17,41,461,26833,26849,26863,26881,26893,26921,616769,616793,616829,616843,616871,617027,617257,617363,617387,617411,617447,617467,617473,617509,617531,617579,617681,617707,617719,618437,618521,618593,618637",
			"name": "Where prime race 4m-1 vs. 4m+1 is tied.",
			"comment": [
				"Primes p such that the number of primes \u003c= p of the form 4m-1 is equal to the number of primes \u003c= p of the form 4m+1.",
				"Starting from a(27410)=9103362505753 the sequence includes the 8th sign-changing zone predicted by C. Bays et al. The sequence with the first 8 sign-changing zones contains 419467 terms (see a-file) with a(419467)=9543313015351 as its last term. - _Sergei D. Shchebetov_, Oct 15 2017"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Andrey S. Shchebetov and Sergei D. Shchebetov, \u003ca href=\"/A007351/b007351.txt\"\u003eTable of n, a(n) for n = 1..100000\u003c/a\u003e (first 1000 terms from T. D. Noe)",
				"A. Alahmadi, M. Planat, P. Solé, \u003ca href=\"https://hal.archives-ouvertes.fr/hal-00650320\"\u003eChebyshev's bias and generalized Riemann hypothesis\u003c/a\u003e, HAL Id: hal-00650320.",
				"C. Bays and R. H. Hudson, \u003ca href=\"http://dx.doi.org/10.1155/S0161171279000119\"\u003eNumerical and graphical description of all axis crossing regions for moduli 4 and 8 which occur before 10^12\u003c/a\u003e, International Journal of Mathematics and Mathematical Sciences, vol. 2, no. 1, pp. 111-119, 1979.",
				"C. Bays, K. Ford, R. H. Hudson and M. Rubinstein, \u003ca href=\"https://doi.org/10.1006/jnth.2000.2601\"\u003eZeros of Dirichlet L-functions near the real axis and Chebyshev's bias\u003c/a\u003e, J. Number Theory 87 (2001), pp.54-76.",
				"M. Deléglise, P. Dusart, X. Roblot, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-04-01649-7\"\u003eCounting Primes in Residue Classes\u003c/a\u003e, Mathematics of Computation, American Mathematical Society, 2004, 73 (247), pp.1565-1575.",
				"A. Granville and G. Martin, \u003ca href=\"http://www.jstor.org/stable/27641834\"\u003ePrime number races\u003c/a\u003e, Amer. Math. Monthly, 113 (No. 1, 2006), 1-33.",
				"R. K. Guy, \u003ca href=\"/A005165/a005165.pdf\"\u003eThe strong law of small numbers\u003c/a\u003e. Amer. Math. Monthly 95 (1988), no. 8, 697-712. [Annotated scanned copy]",
				"M. Rubinstein, P. Sarnak, \u003ca href=\"https://projecteuclid.org/euclid.em/1048515870\"\u003eChebyshev’s bias\u003c/a\u003e, Experimental Mathematics, Volume 3, Issue 3, 1994, Pages 173-197.",
				"Andrey S. Shchebetov and Sergei D. Shchebetov, \u003ca href=\"/A007351/a007351-419467.zip\"\u003eFirst 419467 terms (zipped file)/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PrimeQuadraticEffect.html\"\u003ePrime Quadratic Effect.\u003c/a\u003e",
				"R. G. Wilson, V, \u003ca href=\"/A005596/a005596.pdf\"\u003eLetter to N. J. A. Sloane, Aug. 1993\u003c/a\u003e"
			],
			"mathematica": [
				"Prime@ Position[Fold[Append[#1, #1[[-1]] + If[Mod[#2, 4] == 3, {1, 0}, {0, 1}]] \u0026, {{0, 0}}, Prime@ Range[2, 10^5]], _?(SameQ @@ # \u0026)][[All, 1]] (* _Michael De Vlieger_, May 27 2018 *)"
			],
			"program": [
				"(PARI) lista(nn) = {nb = 0; forprime(p=2, nn, m = (p % 4); if (m == 1, nb++, if (m == 3, nb--)); if (!nb, print1(p, \", \")););} \\\\ _Michel Marcus_, Oct 05 2017"
			],
			"xref": [
				"Cf. A007350, A038691.",
				"Cf. A156749 Sequence showing Chebyshev bias in prime races (mod 4). [From _Daniel Forgues_, Mar 26 2009]"
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_, _Mira Bernstein_, _Robert G. Wilson v_",
			"ext": [
				"Corrected and extended by _Enoch Haga_, Feb 24 2004"
			],
			"references": 23,
			"revision": 48,
			"time": "2018-05-30T03:34:12-04:00",
			"created": "1994-04-28T03:00:00-04:00"
		}
	]
}