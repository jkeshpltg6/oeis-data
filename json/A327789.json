{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A327789",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 327789,
			"data": "4369,1387,341,3277,2047,8321,31621,104653,280601,13747,2081713,88357,8902741,741751,665333,680627,2008597,1252697,3235699,1293337,513629,8095447,83333,2284453,604117,191981609,1530787,13747361,3568661,769757,6973063,275887,12854437,16705021",
			"name": "a(n) is the smallest Fermat pseudoprime to base 2 such that gpf(p-1) = prime(n) for all prime factors p of a(n).",
			"comment": [
				"Equivalently, a(n) is the smallest composite number k such that 2^(k-1) == 1 (mod k) and gpf(p-1) = prime(n) for all prime factors p of k."
			],
			"link": [
				"Daniel Suteu, \u003ca href=\"/A327789/b327789.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PouletNumber.html\"\u003ePoulet Number\u003c/a\u003e"
			],
			"example": [
				"a(1) = 4369 = (2*2*2*2 + 1)(2*2*2*2*2*2*2*2 + 1).",
				"a(2) = 1387 = (2*3*3 + 1)(2*2*2*3*3 + 1).",
				"a(3) = 341 = (2*5 + 1)(2*3*5 + 1).",
				"a(4) = 3277 = (2*2*7 + 1)(2*2*2*2*7 + 1).",
				"a(5) = 2047 = (2*11 + 1)(2*2*2*11 + 1)."
			],
			"mathematica": [
				"pspQ[n_] := CompositeQ[n] \u0026\u0026 PowerMod[2, (n - 1), n] == 1; gpf[n_] := FactorInteger[n][[-1, 1]]; g[n_] := If[Length[(u = Union[gpf /@ (FactorInteger[n][[;; , 1]] - 1)])] == 1, u[[1]], 1]; m = 10; c = 0; k = 0; v = Table[0, {m}]; While[c \u003c m, k++ If[! pspQ[k], Continue[]]; If[(p = g[k]) \u003e 1, i = PrimePi[p]; If[i \u003c= m \u0026\u0026 v[[i]] == 0, c++; v[[i]] = k]]]; v (* _Amiram Eldar_, Oct 08 2019 *)"
			],
			"program": [
				"(Perl) use ntheory \":all\"; sub a { my $p = nth_prime(shift); for(my $k = 4; ; ++$k) { return $k if (is_pseudoprime($k,2) and !is_prime($k) and vecall { (factor($_-1))[-1] == $p } factor($k)) } }",
				"for my $n (1..25) { print \"a($n) = \", a($n), \"\\n\" }"
			],
			"xref": [
				"Cf. A001567 (Fermat pseudoprimes to base 2), A006530 (gpf)."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Daniel Suteu_, Sep 25 2019",
			"references": 1,
			"revision": 11,
			"time": "2019-10-10T04:24:00-04:00",
			"created": "2019-10-09T03:24:49-04:00"
		}
	]
}