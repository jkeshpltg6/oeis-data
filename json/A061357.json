{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A061357",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 61357,
			"data": "0,0,0,1,1,1,1,2,2,2,2,3,2,2,3,2,3,4,1,3,4,3,3,5,4,3,5,3,3,6,2,5,6,2,5,6,4,5,7,4,4,8,4,4,9,4,4,7,3,6,8,5,5,8,6,7,10,6,5,12,3,5,10,3,7,9,5,5,8,7,7,11,5,5,12,4,8,11,4,8,10,5,5,13,9,6,11,7,6,14,6,8,13,5,8,11,6,9",
			"name": "Number of 0\u003ck\u003cn such that n-k and n+k are both primes.",
			"comment": [
				"Number of prime pairs (p,q) with p \u003c n \u003c q and q-n = n-p.",
				"The same as the number of ways n can be expressed as the mean of two distinct primes.",
				"Conjecture: for n\u003e=4 a(n)\u003e0. - _Benoit Cloitre_, Apr 29 2003",
				"Conjectures from _Rick L. Shepherd_, Jun 24 2003: (Start)",
				"1) For each integer N\u003e=1 there exists a positive integer m(N) such that for n\u003e=m(N) a(n)\u003ea(N). (After the first m(N)-1 terms, a(N) does not reappear). In particular, for N=1 (or 2 or 3), m(N)=4 and a(N)=0, giving Benoit Cloitre's conjecture. (cont.)",
				"(cont.) Conjectures based upon observing a(1),...,a(10000):",
				"m(4)=m(5)=m(6)=m(7)=m(19)=20 for a(4)=a(5)=a(6)=a(7)=a(19)=1,",
				"m(8)=...(7 others)...=m(34)=35 for a(8)=...(7 others)...=a(34)=2,",
				"m(12)=...(10 others)...=m(64)=65 for a(12)=...(10 others)...=a(64)=3,",
				"m(18)=...(10 others)...=m(79)=80 for a(18)=...(10 others)...=a(79)=4,",
				"m(24)=...(14 others)...=m(94)=95 for a(24)=...(14 others)...=a(94)=5,",
				"m(30)=...(17 others)...=m(199)=200 for a(30)=...(17 others)...=a(199)=6, etc.",
				"2) Each nonnegative integer appears at least once in the current sequence.",
				"3) Stronger than 2): A001477 (nonnegative integers) is a subsequence of the current sequence. (Supporting evidence: I've observed that 0,1,2,...,175 is a subsequence of a(1),...,a(10000)).",
				"(End)",
				"a(n) is also the number of k such that 2*k+1=p and 2*(n-k-1)+1=q are both odd primes with p \u003c q with p*q = n^2 - m^2. [_Pierre CAMI_, Sep 01 2008]",
				"Also: Number of ways n^2 can be written as b^2+pq where 0\u003cb\u003cn-1 and p,q are primes. - Erin Noel and George Panos (erin.m.noel(AT)rice.edu), Jun 27 2006",
				"a(n) = sum (A010051(2*n - p): p prime \u003c n). [_Reinhard Zumkeller_, Oct 19 2011]",
				"a(n) is also the number of partitions of 2*n into two distinct primes. See the first formula by T. D. Noe, and the Alois P. Heinz, Nov 14 2012, crossreference. - _Wolfdieter Lang_, May 13 2016",
				"All 0\u003ck\u003cn are coprime to n. - _Jamie Morken_, Jun 02 2017"
			],
			"link": [
				"Pierre CAMI, \u003ca href=\"/A061357/b061357.txt\"\u003eTable of n, a(n) for n = 1..60000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A045917(n) - A010051(n). - _T. D. Noe_, May 08 2007",
				"a(n) = sum(A010051(n-k)*A010051(n+k): 1 \u003c= k \u003c n). - _Reinhard Zumkeller_, Nov 10 2012",
				"a(n) = sum_{i=2..n-1} A010051(i)*A010051(2n-i). [_Wesley Ivan Hurt_, Aug 18 2013]"
			],
			"example": [
				"a(10)= 2: there are two such pairs (3,17) and (7,13), as 10 = (3+17)/2 = (7+13)/2."
			],
			"maple": [
				"P:=proc(i) local a,b,c,n; print(0); print(0); print(0); for n from 4 by 1 to i do a:=0; b:=prevprime(n); while b\u003e2 do c:=2*n-b; if isprime(c) then a:=a+1; fi; b:=prevprime(b); od; print(a); od; end: P(100); # _Paolo P. Lava_, Dec 22 2008"
			],
			"mathematica": [
				"Table[Count[Range[n - 1], k_ /; And[PrimeQ[n - k], PrimeQ[n + k]]], {n, 98}] (* _Michael De Vlieger_, May 14 2016 *)"
			],
			"program": [
				"(Haskell)",
				"a061357 n = sum $",
				"   zipWith (\\u v -\u003e a010051 u * a010051 v) [n+1..] $ reverse [1..n-1]",
				"-- _Reinhard Zumkeller_, Nov 10 2012, Oct 19 2011",
				"(PARI) a(n)=my(s);forprime(p=2,n-1,s+=isprime(2*n-p));s \\\\ _Charles R Greathouse IV_, Mar 08 2013"
			],
			"xref": [
				"Cf. A071681 (subsequence for prime n only).",
				"Cf. A092953.",
				"Bisection of A117929 (even part). - _Alois P. Heinz_, Nov 14 2012"
			],
			"keyword": "nonn,easy",
			"offset": "1,8",
			"author": "_Amarnath Murthy_, Apr 28 2001",
			"ext": [
				"More terms from Larry Reeves (larryr(AT)acm.org), May 15 2001"
			],
			"references": 16,
			"revision": 52,
			"time": "2017-06-03T01:28:05-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}