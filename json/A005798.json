{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005798",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5798,
			"id": "M4528",
			"data": "0,1,-8,44,-192,718,-2400,7352,-20992,56549,-145008,356388,-844032,1934534,-4306368,9337704,-19771392,40965362,-83207976,165944732,-325393024,628092832,-1194744096,2241688744,-4152367104,7599231223,-13749863984",
			"name": "Expansion of (theta_2(q)/theta_3(q))^4/16 in powers of q.",
			"comment": [
				"When multiplied by 16, this is the q-expansion of the automorphic function lambda (see A115977) [see Erdelyi].",
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math.Series 55, Tenth Printing, 1972, p. 591.",
				"J. M. Borwein and P. B. Borwein, Pi and the AGM, Wiley, 1987, p. 121.",
				"A. Erdelyi, Higher Transcendental Functions, McGraw-Hill, 1955, Vol. 3, p. 23, Eq. (37).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A005798/b005798.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e (terms 0..1000 from Alois P. Heinz)",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards Applied Math.Series 55, Tenth Printing, 1972, p. 591.",
				"A. Kneser, \u003ca href=\"https://eudml.org/doc/149645\"\u003eNeue Untersuchung einer Reihe aus der Theorie der elliptischen Funktionen\u003c/a\u003e, J. reine u. angew. Math. 157, 1927, 209 - 218, p.213, second formula.",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/EllipticLambdaFunction.html\"\u003eElliptic Lambda Function\u003c/a\u003e a section of The World of Mathematics."
			],
			"formula": [
				"Expansion of elliptic lambda / 16 = m / 16 = (k / 4)^2 in powers of the nome q.",
				"Expansion of q * (psi(q) / phi(q))^8 = q * (psi(q^2) / psi(q))^8 = q * (psi(-q) / phi(-q^2))^8 = q * (chi(-q) / chi(-q^2)^2)^8 = q / (chi(q) * chi(-q^2))^8 = q / (chi(-q) * chi(q)^2)^8 = q * (psi(q^2) / phi(q))^4 = q * (f(-q^4) / f(q))^8 in powers of q where phi(), psi(), chi(), f() are Ramanujan theta functions. - _Michael Somos_, Jun 13 2011",
				"Expansion of eta(q)^8 * eta(q^4)^16 / eta(q^2)^24 in powers of q.",
				"Euler transform of period 4 sequence [-8, 16, -8, 0, ...].",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2)) where f(u, v) = u^2 - v + 16*u*v - 32*u^2*v + 256*(u*v)^2. - _Michael Somos_, Mar 19 2004",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (4 t)) = (1 / 16) g(t) where q = exp(2 Pi i t) and g() is the g.f. for A128692. - _Michael Somos_, May 10 2014",
				"G.f.: q * Product( (1 + q^(2*n)) / (1 + q^(2*n - 1)), n=1..inf )^8.",
				"a(n) ~ (-1)^(n+1) * exp(2*Pi*sqrt(n))/(512*n^(3/4)). - _Vaclav Kotesovec_, Jul 10 2016",
				"Empirical: Sum_{n\u003e=0} a(n)/exp(2*Pi*n) = 17/16 - 3*sqrt(2)/4, verified to 27000 digits (10000 terms). - _Simon Plouffe_, Mar 01 2021"
			],
			"example": [
				"G.f. = q - 8*q^2 + 44*q^3 - 192*q^4 + 718*q^5 - 2400*q^6 + 7352*q^7 - 20992*q^8 + ..."
			],
			"maple": [
				"with (numtheory): etr:= proc(p) local b; b:=proc(n) option remember; local d,j; if n=0 then 1 else add (add (d*p(d), d=divisors(j)) *b(n-j), j=1..n)/n fi end end: aa:=etr (n-\u003e [ -8,16,-8,0] [modp(n-1,4)+1]): a:= n-\u003eaa(n-1): seq (a(n), n=0..26);  # _Alois P. Heinz_, Sep 08 2008"
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ InverseEllipticNomeQ[ x] / 16, {x, 0, n}]; (* _Michael Somos_, Jun 13 2011 *)",
				"a[ n_] := SeriesCoefficient[ (EllipticTheta[ 2, 0, q] / EllipticTheta[ 2, 0, q^(1/2)])^8, {q, 0, n}]; (* _Michael Somos_, May 10 2014 *)",
				"a[ n_] := SeriesCoefficient[ q (QPochhammer[ q^4] / QPochhammer[ -q])^8, {q, 0, n}]; (* _Michael Somos_, May 10 2014 *)",
				"a[ n_] := SeriesCoefficient[ q (Product[ 1 - q^k, {k, 4, n - 1, 4}] / Product[ 1 - (-q)^k, {k, n - 1}])^8, {q, 0, n}]; (* _Michael Somos_, May 10 2014 *)",
				"etr[p_] := Module[{b}, b[n_] := b[n] = If[n == 0, 1, Sum[Sum[d*p[d], {d, Divisors[ j]}]*b[n-j], {j, 1, n}]/n]; b]; aa = etr[Function[{n}, {-8, 16, -8, 0}[[Mod[n-1, 4]+1]]]]; a[n_] := aa[n-1]; Table[a[n], {n, 0, 26}] (* _Jean-François Alcover_, Mar 05 2015, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(PARI) {a(n) = my(A, m); if( n\u003c1, 0, m=1; A = x + O(x^2); while( m\u003cn, m*=2; A = sqrt( subst(A, x, x^2)); A /= (1 + 4*A)^2); polcoeff(A, n))};",
				"(PARI) {a(n) = my(A); if( n\u003c1, 0, n--; A = x * O(x^n); polcoeff( (eta(x + A) * eta(x^4 + A)^2 / eta(x^2 + A)^3)^8, n))}; /* _Michael Somos_, Jul 16 2005 */"
			],
			"xref": [
				"If initial 0 is omitted and sequence begins with a(0) = 1, then this is the convolution of A001938 with itself. G.f.s are related by a(x)=x*A001938(x)^2. Reversion of A005797.",
				"Cf. A007248, A029845, A115977, A128692."
			],
			"keyword": "sign,easy",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Definition simplified by _N. J. A. Sloane_, Sep 25 2011"
			],
			"references": 8,
			"revision": 83,
			"time": "2021-03-12T14:05:28-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}