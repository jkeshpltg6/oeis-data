{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A124959",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 124959,
			"data": "1,1,2,1,4,5,1,6,15,11,1,8,30,44,26,1,10,50,110,130,59,1,12,75,220,390,354,137,1,14,105,385,910,1239,959,314,1,16,140,616,1820,3304,3836,2512,725,1,18,180,924,3276,7434,11508,11304,6525,1667,1,20,225,1320,5460,14868,28770,37680,32625,16670,3842",
			"name": "Triangle read by rows: T(n,k) = a(k)*binomial(n,k) (0 \u003c= k \u003c= n), where a(0)=1, a(1)=2, a(k) = a(k-1) + 3*a(k-2) for k \u003e= 2 (a(k) = A006138(k)).",
			"comment": [
				"Sum of entries in row n = A006190(n+1)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A124959/b124959.txt\"\u003eRows n = 0..100 of triangle, flattened\u003c/a\u003e"
			],
			"example": [
				"First few rows of the triangle:",
				"  1;",
				"  1,   2;",
				"  1,   4,   5;",
				"  1,   6,  15,  11;",
				"  1,   8,  30,  44,  26;",
				"  1,  10,  50, 110, 130,  59;",
				"  ..."
			],
			"maple": [
				"a:=proc(n) if n=0 then 1 elif n=1 then 2 else a(n-1)+3*a(n-2) fi end: T:=(n,k)-\u003ea(k)*binomial(n,k): for n from 0 to 10 do seq(T(n,k),k=0..n) od; # yields sequence in triangular form"
			],
			"mathematica": [
				"T[n_, k_]:= T[n, k]= Simplify[(I*Sqrt[3])^(k-1)*Binomial[n,k]*(I*Sqrt[3]* ChebyshevU[k, 1/(2*I*Sqrt[3])] + ChebyshevU[k-1, 1/(2*I*Sqrt[3])])];",
				"Table[T[n, k], {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, Nov 19 2019 *)"
			],
			"program": [
				"(PARI)",
				"b(k) = if(k\u003c2, k+1, b(k-1) + 3*b(k-2));",
				"T(n,k) = binomial(n,k)*b(k);",
				"for(n=0,10, for(k=0,n, print1(T(n,k), \", \"))) \\\\ _G. C. Greubel_, Nov 19 2019",
				"(MAGMA)",
				"function b(k)",
				"  if k lt 2 then return k+1;",
				"  else return b(k-1) + 3*b(k-2);",
				"  end if;",
				"  return b;",
				"end function;",
				"[Binomial(n,k)*b(k): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Nov 19 2019",
				"(Sage)",
				"@CachedFunction",
				"def b(k):",
				"    if (k\u003c2): return k+1",
				"    else: return b(k-1) + 3*b(k-2)",
				"[[binomial(n, k)*b(k) for k in (0..n)] for n in (0..12)] # _G. C. Greubel_, Nov 19 2019"
			],
			"xref": [
				"Cf. A006138, A006190."
			],
			"keyword": "nonn,tabl",
			"offset": "0,3",
			"author": "_Gary W. Adamson_, Nov 13 2006",
			"ext": [
				"Edited by _N. J. A. Sloane_, Dec 03 2006"
			],
			"references": 1,
			"revision": 11,
			"time": "2019-11-19T22:34:09-05:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}