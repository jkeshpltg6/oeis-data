{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A160333",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 160333,
			"data": "1,1,1,1,2,3,4,5,7,10,13,17,23,32,44,59,79,107,146,198,267,361,490,665,900,1217,1648,2234,3027,4098,5548,7515,10181,13789,18672,25287,34251,46392,62830,85090,115243,156087,211402,286311,387765,525180,711295,963355,1304728",
			"name": "Number of pairs of rabbits in month n in the dying rabbits problem, if they become mature after 4 months and give birth to exactly 7 pairs, one per month.",
			"comment": [
				"The dying rabbits problem of immortal rabbits and matureness after 1 month defines the Fibonacci sequence.",
				"For 0 \u003c= n \u003c= 9, a(n) = A003269(n+1), but a(10) = A003269(11) - 1 because of the death of the first pair of rabbits. - _Robert FERREOL_, Oct 05 2017"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A160333/b160333.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Antonio M. Oller-Marcén, \u003ca href=\"http://www.emis.de/journals/INTEGERS/papers/j11/j11.Abstract.html\"\u003eThe Dying Rabbit Problem Revisited\u003c/a\u003e, INTEGERS 9 (2009), 129-138",
				"\u003ca href=\"/index/Rec#order_09\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1, -1, 1, 0, 1, 0, 1, 0, 1)."
			],
			"formula": [
				"G.f.: -(1 + x + x^2 + x^3 + x^4)*(x^4 - x^3 + x^2 - x + 1)/(-1 + x - x^2 + x^3 + x^5 + x^7 + x^9). - _R. J. Mathar_, May 12 2009",
				"G.f.: (1 - x^10) / (1 - x - x^4 + x^11) = 1 / (1 - x / (1 - x^3 / (1 + x^3 / (1 - x^3 / (1 + x^3 / (1 - x / (1 + x / (1 - x / (1 + x))))))))). - _Michael Somos_, Jan 03 2013",
				"a(n) = a(n-1) - a(n-2) + a(n-3) + a(n-5) + a(n-7) + a(n-9). - _Joerg Arndt_, Oct 04 2017",
				"a(n)=1 for 0 \u003c= n \u003c= 3, a(n) = a(n-1) + a(n-4) for 4 \u003c= n \u003c= 9, and a(n) = a(n-4) + a(n-5) + ... + a(n-10) for n \u003e= 10. - _Robert FERREOL_, Oct 04 2017"
			],
			"example": [
				"The number of pairs at the 13th month is 32."
			],
			"maple": [
				"Cnh := proc(n,h) option remember ; if n \u003c 0 then 0 ; elif n \u003c h then 1; else procname(n-1,h)+procname(n-h,h) ; fi; end:",
				"C := proc(n,k,h) option remember ; local i; if n \u003e= 0 and n \u003c k+h-1 then Cnh(n,h); else add( procname(n-h-i,k,h),i=0..k-1) ; fi; end:",
				"A160333 := proc(n) C(n,7,4) ; end: seq(A160333(n),n=0..80) ; # _R. J. Mathar_, May 12 2009"
			],
			"mathematica": [
				"LinearRecurrence[{1,-1,1,0,1,0,1,0,1},{1,1,1,1,2,3,4,5,7},50]  (* _Harvey P. Dale_, Apr 23 2011 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, n = -n; polcoeff( (x^6 - x^10) / (1 - x^7 - x^10 + x^11) + x * O(x^n), n), polcoeff( (1 - x^10) / (1 - x - x^4 + x^11) + x * O(x^n), n))} /* _Michael Somos_, Jan 03 2013 */"
			],
			"xref": [
				"Cf. A000045, A000930."
			],
			"keyword": "nonn",
			"offset": "0,5",
			"author": "_Parthasarathy Nambi_, May 09 2009",
			"ext": [
				"Edited and extended by _R. J. Mathar_, May 12 2009",
				"Name corrected by _Robert FERREOL_, Nov 18 2017"
			],
			"references": 1,
			"revision": 48,
			"time": "2017-11-29T04:33:05-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}