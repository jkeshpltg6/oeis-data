{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097968",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97968,
			"data": "2468101,21,41,61,82022242628303,23,43,63,84042444648505,25,45,65,86062646668707,27,47,67,88082848688909,29,49,69,81001021041061081,101,1,211,411,611,81,201,221,241,261,281,301,3,213,413,613,81401,421,441",
			"name": "Consider the succession of single digits of the positive even integers: 2 4 6 8 1 0 1 2 1 4 1 6 1 8 2 0 2 2 2 4 ... (A036211). This sequence is the lexicographically earliest sequence of distinct positive odd integers that produces the same succession of digits.",
			"comment": [
				"Original name: \"Write each odd integer \u003e0 on a single label. Put the labels in numerical order to form an infinite sequence L. Now consider the succession of single digits of A005843 (even numbers): 2 4 6 8 1 0 1 2 1 4 1 6 1 8 2 0 2 2 2 4 2 6 2 8 3 0 3 2 3 4 3 6 3 8... The sequence S gives a rearrangement of the labels that reproduces the same succession of digits, subject to the constraint that the smallest label must be used that does not lead to a contradiction.\"",
				"This could be roughly rephrased like this: Rewrite in the most economical way the \"even numbers pattern\" using only odd numbers, but rearranged. All the numbers of the sequence must be different one from another."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A097968/b097968.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Eric Angelini, \u003ca href=\"http://www.pourlascience.fr/ewb_pages/a/article-jeux-de-suites-19006.php\"\u003eJeux de suites\u003c/a\u003e, in Dossier Pour La Science, pp. 32-35, Volume 59 (Jeux math'), April/June 2008, Paris."
			],
			"example": [
				"We must begin with \"2,4,6...\" and we cannot use \"2\" or \"24\" or \"246\" (only odd terms are available), so the first possibility is \"2468101\". We could not have used \"24681\" since no term begins with a 0."
			],
			"mathematica": [
				"f[lst_List, k_] := Block[{L = lst, g, w, a = {}, m}, g[x_] := First@ FirstPosition[x, i_ /; OddQ@ i]; Do[w = Take[L, g@ L]; L = Drop[L, Length@ w]; m = Take[L, g@ L]; While[Or[MemberQ[a, FromDigits@ w], IntegerLength@ FromDigits@ m \u003c Length@ m], w = Join[w, m]; L = Drop[L, Length@ m]; m = Take[L, g@ L]]; AppendTo[a, FromDigits@ w], {k}]; a]; f[Flatten@ Map[IntegerDigits, 2 Range@ 80], 40] (* _Michael De Vlieger_, Nov 28 2015, Version 10 *)"
			],
			"xref": [
				"Cf. A005408, A005843, A098099."
			],
			"keyword": "base,easy,nonn",
			"offset": "1,1",
			"author": "_Eric Angelini_, Sep 22 2004",
			"ext": [
				"Name and Example edited by _Danny Rorabaugh_, Nov 28 2015"
			],
			"references": 4,
			"revision": 31,
			"time": "2017-07-09T18:28:30-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}