{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A053156",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 53156,
			"data": "1,3,10,33,106,333,1030,3153,9586,29013,87550,263673,793066,2383293,7158070,21490593,64504546,193579173,580868590,1742867913,5229128026,15688432653,47067395110,141206379633,423627527506,1270899359733",
			"name": "Number of 2-element intersecting families (with not necessary distinct sets) whose union is an n-element set.",
			"comment": [
				"Let P(A) be the power set of an n-element set A. Then a(n+1) = the number of pairs of elements {x,y} of P(A) for which either 0) x and y are disjoint and for which either x is a subset of y or y is a subset of x, or 1) x and y are disjoint and for which x is not a subset of y and y is not a subset of x, or 2) x and y are intersecting and for which either x is a proper subset of y or y is a proper subset of x, or 3) x = y. - _Ross La Haye_, Jan 12 2008",
				"From _Paul Barry_, Apr 27 2003: (Start)",
				"With offset 0, this is a(n) = (3*3^n - 2*2^n + 1)/2.",
				"G.f. (1-3*x+3*x^2)/((1-x)*(1-2*x)*(1-3*x)).",
				"E.g.f. (3*exp(3*x) - 2*exp(2*x) + exp(x))/2.",
				"Binomial transform of A083329.",
				"Second binomial transform of A040001. (End)"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A053156/b053156.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"V. Jovovic, G. Kilibarda, \u003ca href=\"http://dx.doi.org/10.4213/dm398\"\u003eOn the number of Boolean functions in the Post classes F^{mu}_8\u003c/a\u003e, in Russian, Diskretnaya Matematika, 11 (1999), no. 4, 127-138.",
				"V. Jovovic, G. Kilibarda, \u003ca href=\"http://dx.doi.org/10.1515/dma.1999.9.6.593\"\u003eOn the number of Boolean functions in the Post classes F^{mu}_8\u003c/a\u003e, English translation, in Discrete Mathematics and Applications, 9, (1999), no. 6.",
				"Ross La Haye, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL12/LaHaye/lahaye5.html\"\u003eBinary Relations on the Power Set of an n-Element Set\u003c/a\u003e, Journal of Integer Sequences, Vol. 12 (2009), Article 09.2.6.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (6,-11,6)."
			],
			"formula": [
				"a(n) = (3^n - 2^n + 1)/2.",
				"a(n) = StirlingS2(n+2,3) + StirlingS2(n+1,2) + 1. - _Ross La Haye_, Jan 12 2008",
				"From _Colin Barker_, Jul 29 2012: (Start)",
				"a(n) = 6*a(n-1) - 11*a(n-2) + 6*a(n-3) for n \u003e 3.",
				"G.f.: x*(1-3*x+3*x^2)/((1-x)*(1-2*x)*(1-3*x)). (End)"
			],
			"maple": [
				"A053156:=n-\u003e(3^n - 2^n + 1)/2: seq(A053156(n), n=1..40); # _Wesley Ivan Hurt_, Oct 06 2017"
			],
			"mathematica": [
				"LinearRecurrence[{6,-11,6}, {1, 3, 10}, 50] (* or *) Table[(3^n - 2^n + 1)/2, {n,1,50}] (* _G. C. Greubel_, Oct 06 2017 *)"
			],
			"program": [
				"(PARI) a(n) = (3^n-2^n+1)/2; \\\\ _Michel Marcus_, Nov 30 2015",
				"(MAGMA) [(3^n-2^n+1)/2: n in [1..30]]; // _G. C. Greubel_, Oct 06 2017"
			],
			"xref": [
				"Cf. A000225, A000392, A028243, A000079.",
				"Cf. A036239.",
				"Column k=2 of A288638.",
				"Third column of A294201."
			],
			"keyword": "easy,nonn",
			"offset": "1,2",
			"author": "_Vladeta Jovovic_ and Goran Kilibarda, Feb 28 2000",
			"references": 6,
			"revision": 30,
			"time": "2018-06-16T16:15:16-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}