{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A186430",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 186430,
			"data": "1,1,1,1,2,1,1,12,12,1,1,2,12,2,1,1,120,120,120,120,1,1,2,120,20,120,2,1,1,252,252,2520,2520,252,252,1,1,2,252,42,2520,42,252,2,1,1,240,240,5040,5040,5040,5040,240,240,1,1,2,240,40,5040,84,5040,40,240,2,1",
			"name": "Generalized Pascal triangle associated with the set of primes.",
			"comment": [
				"Given a subset S of the integers Z, Bhargava has shown how to associate with S a generalized factorial function, denoted n!_S, sharing many properties of the classical factorial function n! (which corresponds to the choice S = Z). In particular, he shows that the generalized binomial coefficients n!_S/(k!_S*(n-k)!_S) are always integral for any choice of S.",
				"Here we take S = {2,3,5,7,...} the set of primes.",
				"The generalized factorial n!_S is given by the formula n!_S = product {primes p} (p^(floor(n/(p-1)) + floor(n/(p^2-p)) + floor(n/(p^3-p^2)) + ...), and appears in the database as n!_S = A053657(n) for n\u003e=1. We make the convention that 0!_S = 1.",
				"See A186432 for the generalized Pascal triangle associated with the set",
				"of squares."
			],
			"link": [
				"M. Bhargava, \u003ca href=\"http://mathdl.maa.org/images/upload_library/22/Hasse/00029890.di021346.02p0064l.pdf\"\u003eThe factorial function and generalizations\u003c/a\u003e, Amer. Math. Monthly, 107(2000), 783-799."
			],
			"formula": [
				"T(n,k) = A053657(n)/(A053657(k)*A053657(n-k)), for n,k \u003e= 0, with the convention that A053657(0) = 1.",
				"Row sums A186431."
			],
			"example": [
				"Triangle begins",
				"n/k.|..0.....1.....2.....3.....4.....5.....6.....7",
				"==================================================",
				".0..|..1",
				".1..|..1.....1",
				".2..|..1.....2.....1",
				".3..|..1....12....12.....1",
				".4..|..1.....2....12.....2.....1",
				".5..|..1...120...120...120...120.....1",
				".6..|..1.....2...120....20...120.....2.....1",
				".7..|..1...252...252..2520..2520...252...252.....1",
				".8..|"
			],
			"maple": [
				"#A186430",
				"#Uses program for A053657 written by _Peter Luschny_",
				"A053657 := proc(n) local P, p, q, s, r;",
				"P := select(isprime, [$2..n]); r:=1;",
				"for p in P do s := 0; q := p-1;",
				"do if q \u003e (n-1) then break fi;",
				"s := s + iquo(n-1, q); q := q*p; od;",
				"r := r * p^s; od; r end:",
				"T := (n,k) -\u003e A053657(n)/(A053657(k)*A053657(n-k)):",
				"for n from 0 to 10 do",
				"seq(T(n,k),k = 0..n)",
				"end do;"
			],
			"mathematica": [
				"b[n_] := Product[p^Sum[Floor[(n - 1)/((p - 1) p^k)], {k, 0, n}], {p, Prime[ Range[n]]}];",
				"T[n_, k_] := b[n]/(b[k] b[n - k]);",
				"Table[T[n, k], {n, 0, 10}, {k, 0, n}] (* _Jean-François Alcover_, Jun 22 2019 *)"
			],
			"xref": [
				"Cf. A053657, A186431, A186432."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,5",
			"author": "_Peter Bala_, Feb 21 2011",
			"references": 7,
			"revision": 21,
			"time": "2019-06-22T11:29:48-04:00",
			"created": "2011-02-21T10:02:31-05:00"
		}
	]
}