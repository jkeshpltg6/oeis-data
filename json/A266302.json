{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A266302",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 266302,
			"data": "1,6,1,126,1,2046,1,32766,1,524286,1,8388606,1,134217726,1,2147483646,1,34359738366,1,549755813886,1,8796093022206,1,140737488355326,1,2251799813685246,1,36028797018963966,1,576460752303423486,1,9223372036854775806,1",
			"name": "Decimal representation of the n-th iteration of the \"Rule 15\" elementary cellular automaton starting with a single ON (black) cell.",
			"reference": [
				"Stephen Wolfram, A New Kind of Science, Wolfram Media, 2002; p. 55."
			],
			"link": [
				"Robert Price, \u003ca href=\"/A266302/b266302.txt\"\u003eTable of n, a(n) for n = 0..999\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ElementaryCellularAutomaton.html\"\u003eElementary Cellular Automaton\u003c/a\u003e",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_Elementary_Cellular_Automata\"\u003eIndex to Elementary Cellular Automata\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,17,0,-16)."
			],
			"formula": [
				"From _Colin Barker_, Dec 28 2015 and Apr 15 2019: (Start)",
				"a(n) = (3*(-1)^n+2^(2*n+1)-(-1)^n*2^(2*n+1)-1)/2.",
				"a(n) = 17*a(n-2)-16*a(n-4) for n\u003e3.",
				"G.f.: (1+6*x-16*x^2+24*x^3) / ((1-x)*(1+x)*(1-4*x)*(1+4*x)).",
				"(End)",
				"a(n) = 2*4^n - 2 for odd n; a(n) = 1 for even n. - _Karl V. Keller, Jr._, Aug 31 2021",
				"E.g.f.: cosh(x) - 2*sinh(x) + 2*sinh(4*x). - _Stefano Spezia_, Sep 01 2021"
			],
			"mathematica": [
				"rule=15; rows=20; ca=CellularAutomaton[rule,{{1},0},rows-1,{All,All}]; (* Start with single black cell *) catri=Table[Take[ca[[k]],{rows-k+1,rows+k-1}],{k,1,rows}]; (* Truncated list of each row *) Table[FromDigits[catri[[k]],2],{k,1,rows}]   (* Decimal Representation of Rows *)"
			],
			"program": [
				"(Python) print([2*4**n - 2 if n%2 else 1 for n in range(50)]) # _Karl V. Keller, Jr._, Aug 31 2021"
			],
			"xref": [
				"Cf. A266300, A266301."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Robert Price_, Dec 26 2015",
			"references": 2,
			"revision": 26,
			"time": "2021-09-02T19:17:32-04:00",
			"created": "2015-12-27T08:59:32-05:00"
		}
	]
}