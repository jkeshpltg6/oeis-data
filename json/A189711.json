{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A189711",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 189711,
			"data": "2,10,8,28,54,22,60,190,204,52,110,490,916,676,114,182,1050,2878,3932,2118,240,280,1988,7278,15210,16148,6474,494,408,3444,15890,45738,77470,65210,19576,1004,570,5580,31192,115808,278358,389640,261708,58920,2026,770,8580,56484,258720,820118,1677048,1951700,1048008,176994,4072,1012,12650,96006,525444,2090296,5758802,10073698,9763628,4193580,531262,8166",
			"name": "Number of non-monotonic functions from [k] to [n-k].",
			"comment": [
				"Triangle T(n,k), 3\u003c=k\u003c=n-2, given by (n-k)^k-2*C(n-1,k)+(n-k) is derived using inclusion/exclusion. The triangle contains several other listed sequences: T(2n,n) is sequence A056174(n), number of monotonic functions from [n] to [n]; T(n+2,n) is sequence A005803(n), second-order Eulerian numbers; and T(n,3) is A006331(n-4), maximum accumulated number of electrons at energy level n."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A189711/b189711.txt\"\u003eRows n=5..100 of triangle, flattened\u003c/a\u003e",
				"Dennis Walsh, \u003ca href=\"http://frank.mtsu.edu/~dwalsh/MONOFUNC.pdf\"\u003eNotes on finite monotonic and non-monotonic functions\u003c/a\u003e"
			],
			"formula": [
				"T(n,k)=(n-k)^k-2*C(n-1,k)+(n-k).",
				"T(n,3) = A006331(n-4) for n\u003e=5.",
				"T(n+2,n) = A005803(n) for n\u003e=3.",
				"T(2n,n) = A056174(n) for n\u003e=3."
			],
			"example": [
				"Triangle T(n,k) begins",
				"  n\\k    3     4     5     6     7     8     9",
				"   5     2",
				"   6    10     8",
				"   7    28    54    22",
				"   8    60   190   204    52",
				"   9   110   490   916   676   114",
				"  10   182  1050  2878  3932  2118   240",
				"  11   280  1988  7278 15210 16148  6474   494",
				"  ...",
				"For n=6 and k=4, T(6,4)=8 since there are 8 non-monotonic functions f from [4] to [2], namely, f = \u003cf(1),f(2),f(3),f(4)\u003e given by \u003c1,1,2,1\u003e, \u003c1,2,1,1\u003e, \u003c1,2,2,1\u003e, \u003c1,2,1,2\u003e, \u003c2,2,1,2\u003e, \u003c2,1,2,2\u003e, \u003c2,1,1,2\u003e, and \u003c2,1,2,1\u003e."
			],
			"maple": [
				"seq(seq((n-k)^k-2*binomial(n-1,k)+(n-k),k=3..(n-2)),n=5..15);"
			],
			"mathematica": [
				"nmax = 15; t[n_, k_] := (n-k)^k-2*Binomial[n-1, k]+(n-k); Flatten[ Table[ t[n, k], {n, 5, nmax}, {k, 3, n-2}]](* _Jean-François Alcover_, Nov 18 2011, after Maple *)"
			],
			"program": [
				"(Haskell)",
				"a189711 n k = (n - k) ^ k - 2 * a007318 (n - 1) k + n - k",
				"a189711_row n = map (a189711 n) [3..n-2]",
				"a189711_tabl = map a189711_row [5..]",
				"-- _Reinhard Zumkeller_, May 16 2014"
			],
			"xref": [
				"Cf. A007318."
			],
			"keyword": "nonn,easy,nice,tabl",
			"offset": "5,1",
			"author": "_Dennis P. Walsh_, Apr 25 2011",
			"references": 1,
			"revision": 28,
			"time": "2017-05-15T14:45:21-04:00",
			"created": "2011-04-26T06:28:07-04:00"
		}
	]
}