{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A160377",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 160377,
			"data": "0,1,2,3,4,5,6,1,8,9,10,1,12,13,1,1,16,17,18,1,1,21,22,1,24,25,26,1,28,1,30,1,1,33,1,1,36,37,1,1,40,1,42,1,1,45,46,1,48,49,1,1,52,53,1,1,1,57,58,1,60,61,1,1,1,1,66,1,1,1,70,1,72,73,1,1,1,1,78,1,80,81,82,1,1,85,1,1",
			"name": "Phi-torial of n (A001783) modulo n.",
			"comment": [
				"Is a(n)\u003c\u003e 1 iff n in A033948, n\u003e2? [_R. J. Mathar_, May 21 2009]",
				"Same as A103131, except there -1 appears instead of n-1. By Gauss's generalization of Wilson's theorem, a(n)=-1 means n has a primitive root (n in A033948) and a(n)=1 means n has no primitive root (n in A033949). [_T. D. Noe_, May 21 2009]"
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A160377/b160377.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"John B. Cosgrave and Karl Dilcher, \u003ca href=\"http://www.emis.de/journals/INTEGERS/papers//i39/i39.Abstract.html\"\u003e Extensions of the Gauss-Wilson Theorem\u003c/a\u003e, Integers: Electronic Journal of Combinatorial Number Theory, Vol. 8 (2008), Article #A39.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/WilsonsTheorem.html\"\u003eWilson's Theorem\u003c/a\u003e."
			],
			"formula": [
				"a(n) = A001783(n) mod n. - _R. J. Mathar_, May 21 2009",
				"For n\u003e2, a(n)=n-1 if A060594(n)=2; otherwise a(n)=1. - _Max Alekseyev_",
				"a(n) = Gauss_factorial(n, n) modulo n. (Definition of the Gauss factorial in A216919.) - _Peter Luschny_, Oct 20 2012"
			],
			"example": [
				"Phi-torial of 12 equals 1*5*7*11=385 which leaves a remainder of 1 when divided by 12.",
				"Phi-torial of 14 equals 1*3*5*9*11*13=19305 which leaves a remainder of 13 when divided by 14."
			],
			"maple": [
				"copr := proc(n) local a,k ; a := {1} ; for k from 2 to n-1 do if gcd(k,n) = 1 then a := a union {k} ; fi; od: a ; end:",
				"A001783 := proc(n) local c; mul(c,c= copr(n)) ; end:",
				"A160377 := proc(n) A001783(n) mod n ; end: seq( A160377(n),n=1..100) ; # _R. J. Mathar_, May 21 2009",
				"A160377 := proc(n) local k, r; r := 1:",
				"for k to n do if igcd(n,k) = 1 then r := modp(r*k, n) fi od;",
				"r end: seq( A160377(i), i=1..88); # _Peter Luschny_, Oct 20 2012"
			],
			"mathematica": [
				"Table[nn = n; a = Select[Range[nn], CoprimeQ[#, nn] \u0026];",
				"Mod[Apply[Times, a], nn], {n, 1, 88}] (* _Geoffrey Critzer_, Jan 03 2015 *)"
			],
			"program": [
				"(Sage)",
				"def A160377(n):",
				"    r = 1",
				"    for k in (1..n):",
				"        if gcd(n, k) == 1: r = mod(r*k, n)",
				"    return r",
				"[A160377(n) for n in (1..88)]  # _Peter Luschny_, Oct 20 2012"
			],
			"xref": [
				"Cf. A124740 (one of just four listing \"product of coprimes\")."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_J. M. Bergot_, May 11 2009",
			"ext": [
				"Edited and extended by _R. J. Mathar_ and _Max Alekseyev_, May 21 2009"
			],
			"references": 4,
			"revision": 26,
			"time": "2021-10-07T02:00:05-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}