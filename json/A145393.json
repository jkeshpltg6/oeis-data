{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A145393",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 145393,
			"data": "1,2,2,4,3,5,3,7,5,7,4,11,5,8,8,12,6,13,6,15,10,11,7,21,10,13,12,18,9,22,9,21,14,16,14,29,11,17,16,29,12,28,12,25,23,20,13,39,16,27,20,29,15,34,20,36,22,25,16,50,17,26,29,38,24,40,18,36,26,40",
			"name": "Number of inequivalent sublattices of index n in square lattice, where two sublattices are considered equivalent if one can be rotated or reflected to give the other, with that rotation or reflection preserving the parent square lattice.",
			"comment": [
				"From _Andrey Zabolotskiy_, Mar 12 2018: (Start)",
				"If reflections are not allowed, we get A145392. If any rotations and reflections are allowed, we get A054346.",
				"The parent lattice of the sublattices under consideration has Patterson symmetry group p4mm, and two sublattices are considered equivalent if they are related via a symmetry from that group [Rutherford]. For other 2D Patterson groups, the analogous sequences are A000203 (p2), A069734 (p2mm), A145391 (c2mm), A145392 (p4), A145394 (p6), A003051 (p6mm).",
				"Rutherford says at p. 161 that a(n) != A054346(n) only when A002654(n) \u003e 2, but actually these two sequence differ at other terms, too, for example, at n = 30 (see illustration). (End)"
			],
			"link": [
				"Andrey Zabolotskiy, \u003ca href=\"/A145393/b145393.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Amihay Hanany, Domenico Orlando, and Susanne Reffert, \u003ca href=\"https://doi.org/10.1007/JHEP06(2010)051\"\u003eSublattice counting and orbifolds\u003c/a\u003e, High Energ. Phys., 2010 (2010), 51, \u003ca href=\"https://arxiv.org/abs/1002.2981\"\u003earXiv.org:1002.2981 [hep-th]\u003c/a\u003e (see table 6 and fig. 2).",
				"John S. Rutherford, \u003ca href=\"http://dx.doi.org/10.1107/S010876730804333X\"\u003eSublattice enumeration. IV. Equivalence classes of plane sublattices by parent Patterson symmetry and colour lattice group type\u003c/a\u003e, Acta Cryst. (2009). A65, 156-163. [See Table 2; beware the typo in a(5).]",
				"Andrey Zabolotskiy, \u003ca href=\"/A145392/a145392.pdf\"\u003eSublattices of the square lattice\u003c/a\u003e (illustrations for n = 1..6, 15, 25)",
				"\u003ca href=\"/index/Su#sublatts\"\u003eIndex entries for sequences related to sublattices\u003c/a\u003e",
				"\u003ca href=\"/index/Sq#sqlatt\"\u003eIndex entries for sequences related to square lattice\u003c/a\u003e"
			],
			"formula": [
				"a(n) = (A000203(n) + A002654(n) + A069735(n) + A145390(n))/4. [Rutherford] - _N. J. A. Sloane_, Mar 13 2009",
				"G.f.: Sum_{ m\u003e=1 } (1/((1-x^m)(1-x^(4m))) - 1). [Hanany, Orlando \u0026 Reffert, eq. (6.8)] - _Andrey Zabolotskiy_, Jul 05 2017",
				"a(n) = Sum_{ m: m^2|n } A019590(n/m^2) + A157228(n/m^2) + A157226(n/m^2) + A157230(n/m^2) + A157231(n/m^2) = A053866(n) + A025441(n) + Sum_{ m: m^2|n } A157226(n/m^2) + A157230(n/m^2) + A157231(n/m^2). [Rutherford] - _Andrey Zabolotskiy_, May 07 2018",
				"a(n) = Sum_{ d|n } A008621(d) = Sum_{ d|n } (1 + floor(d/4)). [From the above-given g.f.] - _Andrey Zabolotskiy_, Jul 17 2019"
			],
			"mathematica": [
				"terms = 70;",
				"CoefficientList[Sum[(1/((1-x^m)(1-x^(4m)))-1), {m, 1, terms}] + O[x]^(terms + 1), x] // Rest (* _Jean-François Alcover_, Aug 05 2018 *)"
			],
			"xref": [
				"Cf. A054345, A054346, A167156, A008621.",
				"Cf. A000203, A069734, A145391, A145392, A145394, A003051, A002324, A002654, A069735, A145390.",
				"Cf. A019590, A157228, A157226, A157230, A157231, A053866, A025441."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Feb 23 2009",
			"ext": [
				"New name from _Andrey Zabolotskiy_, Mar 12 2018"
			],
			"references": 19,
			"revision": 34,
			"time": "2019-07-18T01:14:01-04:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}