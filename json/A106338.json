{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A106338",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 106338,
			"data": "1,1,-1,1,-3,2,1,-9,14,-6,1,-45,110,-90,24,1,-585,1670,-1710,744,-120,1,-21105,61670,-66150,32424,-7560,720,1,-1858185,5439350,-5864670,2925384,-728280,91440,-5040,1,-367958745,1077215510,-1161894510,580489224,-145567800,18961200,-1285200,40320,1",
			"name": "Triangle T, read by rows, equal to the matrix inverse of the triangle defined by [T^-1](n,k) = A075263(n,k)/n!, for n\u003e=k\u003e=0.",
			"comment": [
				"Row sums are zero after the initial row. Absolute row sums equal A106339."
			],
			"formula": [
				"Also, T(n, k) = k!*A106340(n, k), where A106340 is the matrix inverse of the triangle formed from (n-k)!*A008278(n, k), n\u003e=k\u003e=0 and A008278 is the triangle of Stirling numbers of 2nd kind."
			],
			"example": [
				"Triangle begins:",
				"1;",
				"1,-1;",
				"1,-3,2;",
				"1,-9,14,-6;",
				"1,-45,110,-90,24;",
				"1,-585,1670,-1710,744,-120;",
				"1,-21105,61670,-66150,32424,-7560,720;",
				"1,-1858185,5439350,-5864670,2925384,-728280,91440,-5040; ...",
				"The matrix inverse T^-1 begins:",
				"1;",
				"1,1;",
				"1,3/2,1/2;",
				"1,2,7/6,1/6;",
				"1,5/2,25/12,5/8,1/24;",
				"1,3,13/4,3/2,31/120,1/120;",
				"1,7/2,14/3,35/12,301/360,7/80,1/720; ...",
				"where [T^-1](n,k) = A075263(n,k)/n!.",
				"Each row n of the matrix inverse equals the initial",
				"(n+1) fractional coefficients of (x/log(1+x))^n,",
				"which are listed below for n=1,2,3,...,9:",
				"1; 1/2,-1/12,1/24,-19/720,3/160,-863/60480,275/24192,...",
				"1,1; 1/12,0,-1/240,1/240,-221/60480,19/6048,...",
				"1,3/2,1/2; 0,1/240,-1/480,1/945,-11/20160,47/172800,...",
				"1,2,7/6,1/6; -1/720,0,1/3024,-1/3024,199/725760,...",
				"1,5/2,25/12,5/8,1/24; 0,-1/6048,1/12096,-19/725760,...",
				"1,3,13/4,3/2,31/120,1/120; 1/30240,0,-1/57600,1/57600,...",
				"1,7/2,14/3,35/12,301/360,7/80,1/720; 0,1/172800,...",
				"1,4,19/3,5,81/40,23/60,127/5040,1/5040; -1/1209600,0,...",
				"1,9/2,33/4,63/8,331/80,37/32,605/4032,17/2688,1/40320; 0,..."
			],
			"mathematica": [
				"rows = 10; Tinv = Table[(1/n!)*PadRight[CoefficientList[x^(n+1)*Sum[k^n * (1-x)^k, {k, 0, Infinity}], x], rows], {n, 0, rows-1}]; T = Inverse[Tinv ]; Table[T[[n, k]], {n, 1, rows}, {k, 1, n}] // Flatten (* _Jean-François Alcover_, Sep 11 2017 *)"
			],
			"program": [
				"(PARI) T(n,k)=(M=matrix(n+1,n+1,m,j,if(m\u003e=j, polcoeff((-x/log(1-x+x^2*O(x^n)))^m,j-1)))^-1)[n+1,k+1]",
				"(PARI) T(n,k)=(-1)^n*k!*(matrix(n+1,n+1,r,c,if(r\u003e=c,(r-c)!* sum(m=0,r-c+1,(-1)^(r-c+1-m)*m^r/m!/(r-c+1-m)!)))^-1)[n+1,k+1]"
			],
			"xref": [
				"Cf. A075263, A106340, A106339, A008278, A002206."
			],
			"keyword": "sign,tabl",
			"offset": "0,5",
			"author": "_Paul D. Hanna_, May 01 2005",
			"references": 2,
			"revision": 10,
			"time": "2017-09-11T05:49:12-04:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}