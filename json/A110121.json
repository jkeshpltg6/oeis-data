{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A110121",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 110121,
			"data": "1,3,12,1,53,10,247,73,1,1192,474,17,5897,2908,183,1,29723,17290,1602,24,152020,100891,12475,342,1,786733,581814,90205,3780,31,4111295,3329507,620243,35857,550,1,21661168,18956564,4114406,307192,7351,38",
			"name": "Triangle read by rows: T(n,k) (0 \u003c= k \u003c= floor(n/2)) is the number of Delannoy paths of length n, having k EE's crossing the line y = x (i.e., two consecutive E steps from the line y = x+1 to the line y = x-1).",
			"comment": [
				"A Delannoy path of length n is a path from (0,0) to (n,n), consisting of steps E=(1,0), N=(0,1) and D=(1,1).",
				"Row n contains 1 + floor(n/2) terms.",
				"Row sums are the central Delannoy numbers (A001850)."
			],
			"link": [
				"Jinyuan Wang, \u003ca href=\"/A110121/b110121.txt\"\u003eRows n = 0..50 of triangle, flattened\u003c/a\u003e",
				"Andrei Asinowski, Axel Bacher, Cyril Banderier, Bernhard Gittenberger, \u003ca href=\"https://lipn.univ-paris13.fr/~banderier/Papers/patterns2019.pdf\"\u003eAnalytic combinatorics of lattice paths with forbidden patterns, the vectorial kernel method, and generating functions for pushdown automata\u003c/a\u003e, Laboratoire d'Informatique de Paris Nord (LIPN 2019).",
				"R. A. Sulanke, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL6/Sulanke/delannoy.html\"\u003eObjects counted by the central Delannoy numbers\u003c/a\u003e, J. Integer Seq. 6 (2003), no. 1, Article 03.1.5."
			],
			"formula": [
				"T(n,0) = A110122(n).",
				"Sum_{k=0..floor(n/2)} k*T(n,k) = A110127(n).",
				"G.f.: 1/((1 - zR)^2 - z - tz^2*R^2), where R = 1 + zR + zR^2 = (1 - z - sqrt(1 - 6z + z^2))/(2z) is the g.f. of the large Schroeder numbers (A006318)."
			],
			"example": [
				"T(2,0)=12 because, among the 13 (=A001850(2)) Delannoy paths of length 2, only NEEN has an EE crossing the line y=x.",
				"Triangle begins:",
				"    1;",
				"    3;",
				"   12,  1;",
				"   53, 10;",
				"  247, 73,  1;"
			],
			"maple": [
				"R:=(1-z-sqrt(1-6*z+z^2))/2/z: G:=1/((1-z*R)^2-z-t*z^2*R^2): Gser:=simplify(series(G,z=0,15)): P[0]:=1: for n from 1 to 12 do P[n]:=coeff(Gser,z^n) od: for n from 0 to 12 do seq(coeff(t*P[n],t^k),k=1..1+floor(n/2)) od; # yields sequence in triangular form"
			],
			"mathematica": [
				"nmax = 11; r := (1 - z - Sqrt[1 - 6*z + z^2])/2/z; g := 1/((1 - z*r)^2 - z - t*z^2*r^2); gser = Series[g, {z, 0, nmax}]; p[0] = 1; Do[ p[n] = Coefficient[ gser, z, n] , {n, 1, nmax}]; row[n_] := Table[ Coefficient[ t*p[n], t, k], {k, 1, 1 + Floor[n/2]}]; Flatten[ Table[ row[n], {n, 0, nmax}]] (* _Jean-François Alcover_, Dec 07 2011, after Maple *)"
			],
			"xref": [
				"Cf. A001850, A006318, A110122, A110123, A110127."
			],
			"keyword": "nonn,tabf",
			"offset": "0,2",
			"author": "_Emeric Deutsch_, Jul 13 2005",
			"references": 4,
			"revision": 20,
			"time": "2020-02-24T00:38:44-05:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}