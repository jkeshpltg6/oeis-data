{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A115878",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 115878,
			"data": "0,0,1,0,1,1,1,1,2,1,1,1,1,1,4,2,1,2,1,1,4,1,1,4,2,1,3,1,1,4,1,3,4,1,4,2,1,1,4,4,1,4,1,1,7,1,1,7,2,2,4,1,1,3,4,4,4,1,1,4,1,1,7,4,4,4,1,1,4,4,1,7,1,1,7,1,4,4,1,7,4,1,1,4,4,1,4,4,1,7,4,1,4,1,4,10,1,2,7,2,1,4",
			"name": "a(n) is the number of positive solutions of the Diophantine equation x^2 = y(y+n).",
			"comment": [
				"Number of divisors d of n^2 such that d^2 \u003c n^2 and n^2/d == d (mod 4). - _Antti Karttunen_, Oct 06 2018, based on _Robert Israel_'s Jun 27 2014 comment in A115880.",
				"For odd n, a(n) can be computed from the prime signature. - _David A. Corneth_, Oct 07 2018",
				"Number of r X s rectangles with integer side lengths such that r + s = n, r \u003c s and (s-r) | (s*r). - _Wesley Ivan Hurt_, Apr 24 2020"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A115878/b115878.txt\"\u003eTable of n, a(n) for n = 1..18480\u003c/a\u003e",
				"Antti Karttunen, \u003ca href=\"/A115878/a115878.txt\"\u003eData supplement: n, a(n) computed for n = 1..100000\u003c/a\u003e"
			],
			"formula": [
				"From _David A. Corneth_, Oct 07 2018: (Start)",
				"a((2k+1) * 2^m) = floor(tau((2k + 1) ^ 2) / 2) for m \u003c= 2.",
				"a((2k+1) * 2^m) = (2m - 3) * a(2k+1) + (m-2) for m \u003e 2. (End)",
				"a(n) = Sum_{i=1..floor((n-1)/2)} (1 - ceiling(i*(n-i)/(n-2*i)) + floor(i*(n-i)/(n-2*i))). - _Wesley Ivan Hurt_, Apr 24 2020"
			],
			"example": [
				"a(15) = 4 since there are 4 solutions (x,y) to x^2 = y(y+15), namely (4,1), (10,5), (18, 12) and (56, 49).",
				"Note how each x is obtained from each such divisor pair n2/d and d of n2 as (n2/d - d)/4, when their difference is a positive multiple of four, thus in case of n2 = 15^2 = 225 we get (225/1 - 1)/4 = 56, (225/3 - 3)/4 = 18, (225/5 - 5) = 10 and (225/9 - 9)/4 = 4. - _Antti Karttunen_, Oct 06 2018",
				"a(96) = 10. We compute P, the largest power of 2 dividing n = 96. Then compute min(P, 4) and divide n by it. This gives 96/4 = 24. Then find the number of divisors of 24^2, which is 21. Dividing by 2 rounding down to the nearest integer gives 10, the value of a(96). - _David A. Corneth_, Oct 06 2018"
			],
			"mathematica": [
				"a[n_] := Sum[Boole[d^2 \u003c n^2 \u0026\u0026 Mod[n^2/d-d, 4] == 0], {d, Divisors[n^2]}];",
				"Array[a, 102] (* _Jean-François Alcover_, Feb 27 2019, from PARI *)"
			],
			"program": [
				"(PARI) A115878(n) = { my(n2 = n^2); sumdiv(n2,d,((d*d)\u003cn2)\u0026\u0026(0==(((n2/d)-d)%4))); }; \\\\ _Antti Karttunen_, Oct 06 2018",
				"(PARI) a(n) = my(v=min(2, valuation(n,2))); numdiv((n\u003e\u003ev)^2)\u003e\u003e1 \\\\ _David A. Corneth_, Oct 06 2018"
			],
			"xref": [
				"Cf. A067721, A115879, A115880, A115881."
			],
			"keyword": "nonn,easy",
			"offset": "1,9",
			"author": "_Giovanni Resta_, Feb 02 2006",
			"references": 10,
			"revision": 26,
			"time": "2021-02-01T22:04:42-05:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}