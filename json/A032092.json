{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A032092",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 32092,
			"data": "3,9,28,60,126,226,396,636,1001,1491,2184,3080,4284,5796,7752,10152,13167,16797,21252,26532,32890,40326,49140,59332,71253,84903,100688,118608,139128,162248,188496,217872,250971,287793,329004,374604,425334,481194,543004",
			"name": "Number of reversible strings with n-1 beads of 2 colors. 5 beads are black. String is not palindromic.",
			"comment": [
				"If the offset is changed to 3, this is the 2nd Witt transform of A000217 [Moree]. - _R. J. Mathar_, Nov 08 2008",
				"From _Petros Hadjicostas_, May 19 2018: (Start)",
				"Let k be an integer \u003e= 2. The g.f. of the BHK[k] transform of the sequence (c(n): n \u003e= 1), with g.f. C(x) = Sum_{n\u003e=1} c(n)*x^n, is A_k(x) = (C(x)^k - C(x^2)^(k/2))/2 if k is even, and A_k(x) = (C(x)/2)*(C(x)^{k-1} - C(x^2)^{(k-1)/2}) if k is odd. This follows easily from the formulae in C. G. Bower's web link below about transforms.",
				"When k is even and c(n) = 1 for all n \u003e= 1, we get C(x) = x/(1-x) and A_k(x) = (1/2)*((x/(1-x))^k - (x^2/(1-x^2))^{k/2}). If (a_k(n): n \u003e= 1) is the output sequence (with g.f. A_k(x)), then it can be proved (using Taylor expansions) that a_k(n) = (1/2)*(binomial(n-1, n-k) - binomial((n/2)-1, (n-k)/2)) for even n \u003e= k+1 and a_k(n) = (1/2)*binomial(n-1, n-k) for odd n \u003e= k+1. (Clearly, a_k(1) = ... = a_k(k) = 0.)",
				"In this sequence, k = 6, and (according to C. G. Bower) a(n) = a_{k=6}(n) is the number of reversible non-palindromic compositions of n with 6 positive parts. If n = b_1 + b_2 + b_3 + b_4 + b_5 + b_6 is such a composition of n (with b_i \u003e= 1), then it is equivalent to the composition n = b_6 + b_5 + b_4 + b_3 + b_2 + b_1, and each equivalent class has two elements because here linear palindromes are not allowed as compositions of n.",
				"The fact that we are finding the BHK[6] transform of 1, 1, 1, ... means that each part of each composition of n can have exactly one color (see Bower's link below about transforms).",
				"In each such composition replace each b_i with one black (B) ball followed by b_i - 1 white (W) balls. Then drop the first black (B) ball. We then get a reversible non-palindromic string of length n-1 that has 5 black balls and n-6 white balls. This process, applied to the equivalent compositions n = b_1 + b_2 + b_3 + b_4 + b_5 + b_6 = b_6 + b_5 + b_4 + b_3 + b_2 + b_1, gives two strings of length n-1 with 5 black balls and n-6 white balls that are mirror images of each other.",
				"Hence, for n \u003e= 2, a(n) = a_{k=6}(n) is also the number of reversible non-palindromic strings of length n-1 that have k-1 = 5 black balls and n-k = n-6 white balls. (Clearly, a(n) = a_{k=6}(n) \u003e 0 only for n \u003e= 7.)",
				"(End)"
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A032092/b032092.txt\"\u003eTable of n, a(n) for n = 7..1000\u003c/a\u003e",
				"C. G. Bower, \u003ca href=\"/transforms2.html\"\u003eTransforms (2)\u003c/a\u003e",
				"Pieter Moree, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2005.03.004\"\u003eThe formal series Witt transform\u003c/a\u003e, Discr. Math. no. 295 vol. 1-3 (2005) 143-160. - _R. J. Mathar_, Nov 08 2008",
				"\u003ca href=\"/index/Rec#order_09\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,0,-8,6,6,-8,0,3,-1)."
			],
			"formula": [
				"\"BHK[ 6 ]\" (reversible, identity, unlabeled, 6 parts) transform of 1, 1, 1, 1, ...",
				"G.f.: x^7*(3+x^2)/((1-x)^6*(1+x)^3). - _R. J. Mathar_, Nov 08 2008",
				"From _Colin Barker_, Mar 07 2015: (Start)",
				"a(n) = (2*n^5-30*n^4+170*n^3-480*n^2+728*n-480)/480 if n is even.",
				"a(n) = (2*n^5-30*n^4+170*n^3-450*n^2+548*n-240)/480 if n is odd.",
				"(End)",
				"From _Petros Hadjicostas_, May 19 2018: (Start)",
				"a(n) = (1/2)*(binomial(n-1, n-6) - binomial((n/2)-1, (n-6)/2)) if n is even.",
				"a(n) = (1/2)*binomial(n-1, n-6) if n is odd.",
				"G.f.: (1/2)*((x/(1-x))^6 - (x^2/(1-x^2))^3).",
				"These formulae agree with the above formulae by R. J. Mathar and C. Barker.",
				"(End)"
			],
			"example": [
				"From _Petros Hadjicostas_, May 19 2018: (Start)",
				"For n=7, we have the following 3 reversible non-palindromic compositions with 6 parts of n: 1+1+1+1+1+2 (= 2+1+1+1+1+1), 1+1+1+1+2+1 (= 1+2+1+1+1+1), and 1+1+1+2+1+1 (= 1+1+2+1+1+1). Using the process described in the comments, we get the following reversible non-palindromic strings with 5 black balls and n-6 = 1 white balls: BBBBBW (= WBBBBB), BBBBWB (= BWBBBB), and BBBWBB (= BBWBBB).",
				"For n=8, we get the following 9 compositions and 9 corresponding strings:",
				"1+1+1+1+1+3 \u003c-\u003e BBBBBWW",
				"1+1+1+1+3+1 \u003c-\u003e BBBBWWB",
				"1+1+1+3+1+1 \u003c-\u003e BBBWWBB",
				"1+1+1+1+2+2 \u003c-\u003e BBBBWBW",
				"1+1+1+2+1+2 \u003c-\u003e BBBWBBW",
				"1+1+2+1+1+2 \u003c-\u003e BBWBBBW",
				"1+2+1+1+1+2 \u003c-\u003e BWBBBBW",
				"1+1+1+2+2+1 \u003c-\u003e BBBWBWB",
				"1+1+2+1+2+1 \u003c-\u003e BBWBBWB",
				"(End)"
			],
			"mathematica": [
				"LinearRecurrence[{3,0,-8,6,6,-8,0,3,-1},{3,9,28,60,126,226,396,636,1001},50] (* _Harvey P. Dale_, Mar 19 2017 *)",
				"f[n_] := Binomial[n - 1, n - 6]/2 - If[ OddQ@ n, 0, Binomial[(n/2) - 1, (n - 6)/2]/2]; Array[a, 40, 7] (* or *)",
				"CoefficientList[ Series[(x^7 (x^2 + 3))/((x - 1)^6 (x + 1)^3), {x, 0, 46}], x] (* _Robert G. Wilson v_, May 20 2018 *)"
			],
			"program": [
				"(PARI) Vec(x^7*(3+x^2)/((1-x)^6*(1+x)^3) + O(x^100)) \\\\ _Colin Barker_, Mar 07 2015"
			],
			"xref": [
				"Cf. A002620, A006584, A032091, A032093, A032094, A282011."
			],
			"keyword": "nonn,easy",
			"offset": "7,1",
			"author": "_Christian G. Bower_",
			"references": 7,
			"revision": 48,
			"time": "2018-07-01T11:09:55-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}