{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A328401",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 328401,
			"data": "1,2,2,3,2,2,2,4,3,2,2,5,2,2,2,6,2,5,2,5,2,2,2,7,3,2,4,5,2,2,2,8,2,2,2,3,2,2,2,7,2,2,2,5,5,2,2,9,3,5,2,5,2,7,2,7,2,2,2,5,2,2,5,10,2,2,2,5,2,2,2,11,2,2,5,5,2,2,2,9,6,2,2,5,2,2,2,7,2,5,2,5,2,2,2,12,2,5,5,3,2,2,2,7,2",
			"name": "Lexicographically earliest infinite sequence such that a(i) = a(j) =\u003e A328400(i) = A328400(j) for all i, j.",
			"comment": [
				"Restricted growth sequence transform of A328400(n), or equally, of A007947(A181819(n)).",
				"For all i, j:",
				"  A101296(i) = A101296(j) =\u003e a(i) = a(j),",
				"  a(i) = a(j) =\u003e A051903(i) = A051903(j) =\u003e A008966(i) = A008966(j),",
				"  a(i) = a(j) =\u003e A051904(i) = A051904(j),",
				"  a(i) = a(j) =\u003e A052409(i) = A052409(j),",
				"  a(i) = a(j) =\u003e A072411(i) = A072411(j),",
				"  a(i) = a(j) =\u003e A071625(i) = A071625(j),",
				"  a(i) = a(j) =\u003e A267115(i) = A267115(j),",
				"  a(i) = a(j) =\u003e A267116(i) = A267116(j)."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A328401/b328401.txt\"\u003eTable of n, a(n) for n = 1..100000\u003c/a\u003e",
				"\u003ca href=\"/index/Eu#epf\"\u003eIndex entries for sequences computed from exponents in factorization of n\u003c/a\u003e"
			],
			"example": [
				"Numbers 2 (= 2^1), 3 (= 3^1), 6 = (2^1 * 3^1) and 30 (2^1 * 3^1 * 5^1) all have just one distinct exponent, 1, in the multisets of exponents that occur in their prime factorization, thus they all have the same value a(2) = a(3) = a(6) = a(30) = 2 in this sequence.",
				"Number 4 (2^2), 9 (3^2) and 36 (2^2 * 3^2) all have just one distinct exponent, 2, in the multisets of exponents that occur in their prime factorization, thus they all have the same value a(4) = a(9) = a(36) = 3 in this sequence.",
				"Numbers 12 = 2^2 * 3^1, 18 = 2^1 * 3^2, 60 = 2^2 * 3^1 * 5^1 and 300 = 2^2 * 3^1 * 5^2 all have both 1 and 2 and none other values occurring in the multisets of exponents in their prime factorization, thus they all have the value of a(12) = 5 that was allotted to 12 by the restricted growth sequence transform, as 12 is the smallest number with prime signature (1, 2)."
			],
			"program": [
				"(PARI)",
				"up_to = 100000;",
				"rgs_transform(invec) = { my(om = Map(), outvec = vector(length(invec)), u=1); for(i=1, length(invec), if(mapisdefined(om,invec[i]), my(pp = mapget(om, invec[i])); outvec[i] = outvec[pp] , mapput(om,invec[i],i); outvec[i] = u; u++ )); outvec; };",
				"A007947(n) = factorback(factorint(n)[, 1]);",
				"A181819(n) = factorback(apply(e-\u003eprime(e),(factor(n)[,2])));",
				"v328401 = rgs_transform(vector(up_to, n, A007947(A181819(n)))); \\\\ Faster than with A328400(n).",
				"A328401(n) = v328401[n];"
			],
			"xref": [
				"Cf. A007947, A181819, A328400.",
				"Cf. A005117 (gives indices of terms \u003c= 2), A062503 (after its initial 1, gives indices of 3's).",
				"Cf. A008966, A051903, A051904, A052409, A072411, A071625, A267115, A267116."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Antti Karttunen_, Oct 17 2019",
			"references": 2,
			"revision": 20,
			"time": "2019-10-18T11:28:51-04:00",
			"created": "2019-10-18T11:28:51-04:00"
		}
	]
}