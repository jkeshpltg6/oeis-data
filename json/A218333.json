{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A218333",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 218333,
			"data": "5,5,8,10,2,12,7,4,37,23,5,51,3,6,34,23,5,57,9,22,49,66,64,54,5,56,43,28,46,116,56,232,92,170,65,206,181,379,170,511,190,416,187,448,89,143,200,159,434,670,145,1081,213,1011,680,77",
			"name": "The index of the smallest n-isolated prime p such that p/n is not between 2 and 3 and not between the smaller and greater primes of a twin prime pair, or 0 if no such p exists.",
			"comment": [
				"For n\u003e=2, a prime p is called n-isolated (cf. A166251 and the Shevelev link, Section 10) if there is no other prime in the interval (n*prevprime(p/n), n*nextprime(p/n)).",
				"In particular, if a(n)=1, then the smallest n-isolated prime divided by n is not between 2 and 3 and not between the smaller and greater primes of a twin prime pair.",
				"Suppose that for every n there exist infinitely many n-isolated primes.  Then if there exists n_0 such that a(n_0)=0, there are infinitely many twin primes. On the other hand, one can prove that the smallest n-isolated prime divided by n tends to infinity as n goes to infinity. Therefore, if there is not an N such that, for all n \u003e= N, a(n)=1, then we also conclude that there are infinitely many twin primes.",
				"Conjecture: all a(n) \u003e= 2."
			],
			"link": [
				"V. Shevelev, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL15/Shevelev/shevelev19.html\"\u003eRamanujan and Labos Primes, Their Generalizations, and Classifications of Primes\u003c/a\u003e, Journal of Integer Sequences, Vol. 15 (2012), Article 12.5.4"
			],
			"example": [
				"Let n=2. The 2-isolated primes are in A166251.",
				"The first 2-isolated prime is 5, and 5/2 is between 2 and 3.",
				"The second 2-isolated prime is 7, and 7/2 is between 3 and 5.",
				"The third 2-isolated prime is 23, and 23/2 is between 11 and 13.",
				"The fourth 2-isolated prime is 37, and 37/2 is between 17 and 19.",
				"The fifth 2-isolated prime is 79, and 79/2 is between 37 and 41.  Since (37,41) is not (2,3) and is not a twin prime pair, a(2)=5"
			],
			"program": [
				"(PARI) isoki(p, n) = (p==nextprime(n*precprime(p\\n))) \u0026\u0026 (p==precprime(n*nextprime(p/n))); \\\\ A166251",
				"nextp(p, n) = while(! isoki(p, n), p = nextprime(p+1)); p;",
				"isokp(p, n) = {my(diff = nextprime(p/n) - precprime(p/n)); if ((diff == 1) || (diff == 2), return (0)); return (1);}",
				"a(n) = {my(p = nextp(2, n), nb = 1); while (! isokp(p, n), p = nextp(nextprime(p+1), n); nb++;); nb;} \\\\ _Michel Marcus_, Dec 16 2018"
			],
			"xref": [
				"Cf. A166251, A217561, A217566."
			],
			"keyword": "nonn",
			"offset": "2,1",
			"author": "_Vladimir Shevelev_, Oct 26 2012",
			"ext": [
				"a(6)-a(38) were calculated by _Zak Seidov_, Oct 28 2012",
				"More terms from _Michel Marcus_, Dec 16 2018"
			],
			"references": 0,
			"revision": 56,
			"time": "2018-12-16T13:03:36-05:00",
			"created": "2012-12-28T01:40:13-05:00"
		}
	]
}