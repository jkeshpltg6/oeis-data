{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A221076",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 221076,
			"data": "2,16,1,32,1,320,1,608,1,5776,1,10944,1,103680,1,196416,1,1860496,1,3524576,1,33385280,1,63245984,1,599074576,1,1134903168,1,10749957120,1,20365011072,1,192900153616,1,365435296160,1",
			"name": "Continued fraction expansion of product_{n\u003e=0} (1-sqrt(5)*[sqrt(5)-2]^{4n+3})/(1-sqrt(5)*[sqrt(5)-2]^{4n+1}).",
			"comment": [
				"Simple continued fraction expansion of product {n \u003e= 0} {1 - sqrt(m)*[sqrt(m) - sqrt(m-1)]^(4*n+3)}/{1 - sqrt(m)*[sqrt(m) - sqrt(m-1)]^(4*n+1)} at m = 5. For other cases see A221073 (m = 2), A221074 (m = 3) and A221075 (m = 4).",
				"If we denote the present sequence by [2; 16, 1, c(3), 1, c(4), 1, ...] then for k \u003e= 1 the sequence [1; c(2*k+1), 1, c(2*(2*k+1)), 1, c(3*(2*k+1)), 1, ...] gives the simple continued fraction expansion of product {n \u003e= 0} [1-sqrt(5)*{(sqrt(5)-2)^(2*k+1)}^(4*n+3)]/[1 - sqrt(5)*{(sqrt(5)-2)^(2*k+1)}^(4*n+1)]. An example is given below."
			],
			"link": [
				"P. Bala, \u003ca href=\"/A174500/a174500_2.pdf\"\u003eSome simple continued fraction expansions for an infinite product, Part 1\u003c/a\u003e",
				"\u003ca href=\"/index/Rec\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,1,0,18,0,-18,0,-1,0,1)."
			],
			"formula": [
				"a(2*n) = 1 for n \u003e= 1. For n \u003e= 1 we have:",
				"a(4*n - 3) = (sqrt(5) + 2)^(2*n) + (sqrt(5) - 2)^(2*n) - 2;",
				"a(4*n - 1) = 1/sqrt(5)*{(sqrt(5) + 2)^(2*n + 1) + (sqrt(5) - 2)^(2*n + 1)} - 2.",
				"a(4*n - 3) = 16*A049863(n) = 4*A132584(n);",
				"a(4*n - 1) = 32*A049664(n) = 4*A053606(n).",
				"O.g.f.: 2 + x^2/(1 - x^2) + 16*x*(1 + x^2)^2/(1 - 19*x^4 + 19*x^8 - x^12) = 2 + 16*x + x^2 + 32*x^3 + x^4 + 320*x^5 + ....",
				"O.g.f.: (x^10-2*x^8-18*x^6+36*x^4-16*x^3+x^2-16*x-2) / ((x-1)*(x+1)*(x^4-4*x^2-1)*(x^4+4*x^2-1)). - _Colin Barker_, Jan 10 2014"
			],
			"example": [
				"Product {n \u003e= 0} {1 - sqrt(5)*(sqrt(5) - 2)^(4*n+3)}/{1 - sqrt(5)*(sqrt(5) - 2)^(4*n+1)} = 2.05892 54859 32105 82744 ...",
				"= 2 + 1/(16 + 1/(1 + 1/(32 + 1/(1 + 1/(320 + 1/(1 + 1/(608 + ...))))))).",
				"Since (sqrt(5) - 2)^3 = 17*sqrt(5) - 38 we have the following simple continued fraction expansion:",
				"product {n \u003e= 0} {1 - sqrt(5)*(17*sqrt(5) - 38)^(4*n+3)}/{1 - sqrt(5)*(17*sqrt(5) - 38)^(4*n+1)} = 1.03030 31892 29728 52318 ... = 1 + 1/(32 + 1/(1 + 1/(5776 + 1/(1 + 1/(196416 + 1/(1 + 1/(33385280 + ...)))))))."
			],
			"xref": [
				"Cf. A049664, A049863, A053606, A132584, A174500, A221073 (m = 2), A221074 (m = 3), A221075 (m = 4)."
			],
			"keyword": "nonn,easy,cofr",
			"offset": "0,1",
			"author": "_Peter Bala_, Jan 06 2013",
			"references": 3,
			"revision": 17,
			"time": "2015-06-13T00:54:37-04:00",
			"created": "2013-01-06T11:24:34-05:00"
		}
	]
}