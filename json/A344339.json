{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A344339",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 344339,
			"data": "0,1,1,1,1,1,2,2,1,2,1,2,1,2,2,1,1,1,2,2,2,2,3,3,2,2,2,2,2,2,3,2,1,2,1,2,2,2,2,2,2,3,2,3,2,3,2,2,1,2,2,1,2,2,3,2,2,3,2,2,2,3,3,2,1,2,2,2,1,2,2,2,2,3,2,3,2,3,2,2,1,2,2,2,2,1,3",
			"name": "a(n) is the minimal number of terms of A332520 that need to be combined with the bitwise OR operator in order to give n.",
			"comment": [
				"This sequence is related to Karnaugh maps:",
				"- for any number n with up to 2^k binary digits (possibly with leading zeros),",
				"- we can interpret the binary expansion of n as a truth table for a k-ary Boolean function f,",
				"- a(n) gives the optimal number of products in a disjunctive normal form for f."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A344339/a344339.gp.txt\"\u003ePARI program for A344339\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Disjunctive_normal_form\"\u003eDisjunctive normal form\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Karnaugh_map\"\u003eKarnaugh map\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 1 iff n \u003e 0 and n belongs to A332520.",
				"a(n) \u003c= A000120(n).",
				"a(A001196(n)) = a(n)."
			],
			"example": [
				"For n = 32576:",
				"- the binary representation of 13170 is \"111111101000000\",",
				"- it has 15 bits, so we can take k = 4 (15 \u003c= 2^4),",
				"- the corresponding 4-ary Boolean function f has the following truth table:",
				"     CD\\AB|  00  01  11  10",
				"     -----+----------------",
				"        00|   0   0   1   1",
				"        01|   0   0   1   1",
				"        11|   0   0   0   1",
				"        10|   0   1   1   1",
				"- we can express f as AC' + AB' + BCD' in optimal form,",
				"- so a(32576) = 3."
			],
			"program": [
				"(PARI) See Links section."
			],
			"xref": [
				"Cf. A000120, A001196, A332520."
			],
			"keyword": "nonn,base",
			"offset": "0,7",
			"author": "_Rémy Sigrist_, May 15 2021",
			"references": 1,
			"revision": 8,
			"time": "2021-05-21T04:18:04-04:00",
			"created": "2021-05-21T04:18:04-04:00"
		}
	]
}