{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A060121",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 60121,
			"data": "0,2,3,7,8,16,26,5,21,18,38,49,50,16,26,6,81,54,98,70,157,161,58,147,21,86,92,197,50,249,137,184,119,45,45,261,198,61,176,143,51,103,221,72,11,219,35,86,385,384,141,143,225,92,245,533,557,473,170,375,516",
			"name": "First solution mod p of x^3 = 2 for primes p such that only one solution exists.",
			"comment": [
				"Solutions mod p are represented by integers from 0 to p-1. For i \u003e 1, i is a solution mod p of x^3 = 2 iff p is a prime factor of i^3-2 and p \u003e i (cf. comment to A059940). i^3-2 has at most two prime factors \u003e i. Hence i is a solution mod p of x^3 = 2 for at most two different p and therefore no integer occurs more than twice in this sequence. There are integers which do occur twice, e.g. 16, 21, 26 (cf. A060914). Moreover, no integer occurs more than twice in A060121, A060122, A060123 and A060124 taken together."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A060121/b060121.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = first (only) solution mod p of x^3 = 2, where p is the n-th prime such that x^3 = 2 has only one solution mod p, i.e. p is the n-th term of A045309."
			],
			"example": [
				"a(9) = 21, since 47 is the ninth term of A045309 and 21 is the only solution mod 47 of x^3 = 2."
			],
			"maple": [
				"Res:=0,2: count:= 2: p:= 3:",
				"while count \u003c 100 do",
				"p:= nextprime(p);",
				"   if p mod 3 = 2 then",
				"    count:= count+1;",
				"    Res:= Res, numtheory:-mroot(2,3,p);",
				"fi",
				"od:",
				"Res; # _Robert Israel_, Sep 12 2018"
			],
			"mathematica": [
				"terms = 100;",
				"A045309 = Select[Prime[Range[2 terms]], Mod[#, 3] != 1\u0026];",
				"a[n_] := PowerMod[2, 1/3, A045309[[n]]];",
				"Array[a, terms] (* _Jean-François Alcover_, Feb 27 2019 *)"
			],
			"xref": [
				"Cf. A040028, A045309, A059940, A060914, A060122, A060123, A060124."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Klaus Brockhaus_, Mar 02 2001",
			"references": 6,
			"revision": 10,
			"time": "2019-02-27T16:16:24-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}