{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006577",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6577,
			"id": "M4323",
			"data": "0,1,7,2,5,8,16,3,19,6,14,9,9,17,17,4,12,20,20,7,7,15,15,10,23,10,111,18,18,18,106,5,26,13,13,21,21,21,34,8,109,8,29,16,16,16,104,11,24,24,24,11,11,112,112,19,32,19,32,19,19,107,107,6,27,27,27,14,14,14,102,22",
			"name": "Number of halving and tripling steps to reach 1 in '3x+1' problem, or -1 if 1 is never reached.",
			"comment": [
				"The 3x+1 or Collatz problem is as follows: start with any number n. If n is even, divide it by 2, otherwise multiply it by 3 and add 1. Do we always reach 1? This is a famous unsolved problem. It is conjectured that the answer is yes.",
				"It seems that about half of the terms satisfy a(i) = a(i+1). For example, up to 10000000, 4964705 terms satisfy this condition.",
				"n is an element of row a(n) in triangle A127824. - _Reinhard Zumkeller_, Oct 03 2012",
				"The number of terms that satisfy a(i) = a(i+1) for i being a power of ten from 10^1 through 10^10 are: 0, 31, 365, 4161, 45022, 477245, 4964705, 51242281, 526051204, 5378743993. - _John Mason_, Mar 02 2018",
				"5 seems to be the only number whose value matches its total number of steps (checked to n \u003c= 10^9). - _Peter Woodward_, Feb 15 2021"
			],
			"reference": [
				"R. K. Guy, Unsolved Problems in Number Theory, E16.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A006577/b006577.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"David Eisenbud and Brady Haran, \u003ca href=\"https://www.youtube.com/watch?v=5mFpVDpKX70\"\u003eUNCRACKABLE? The Collatz Conjecture\u003c/a\u003e, Numberphile video, 2016.",
				"Geometry.net, \u003ca href=\"http://www.geometry.net/theorems_and_conjectures/collatz_problem.html\"\u003eLinks on Collatz Problem\u003c/a\u003e",
				"Jason Holt, \u003ca href=\"/A006577/a006577_1Blog.png\"\u003eLog-log plot of first billion terms\u003c/a\u003e",
				"Jason Holt, \u003ca href=\"/A006577/a006577_1B.png\"\u003ePlot of 1 billion values of the number of steps to drop below n\u003c/a\u003e (A060445), log scale on x axis",
				"Jason Holt, \u003ca href=\"/A006577/a006577_10B.png\"\u003ePlot of 10 billion values of the number of steps to drop below n\u003c/a\u003e (A060445), log scale on x axis",
				"A. Krowne, \u003ca href=\"https://planetmath.org/collatzproblem\"\u003eCollatz problem\u003c/a\u003e, PlanetMath.org.",
				"J. C. Lagarias, \u003ca href=\"http://www.cecm.sfu.ca/organics/papers/lagarias/paper/html/paper.html\"\u003eThe 3x+1 problem and its generalizations\u003c/a\u003e, Amer. Math. Monthly, 92 (1985), 3-23.",
				"J. C. Lagarias, \u003ca href=\"http://www.cfrd.cl/~moises/DVD/05Bibliografia%20-%20Geometria%20Sagrada/Matematica%20Recreativa/Martin%20Gardner/Martin%20Gardner%20-%20The%20Mathemagician%20and%20Pied%20Puzzler%20-%20A%20Collection%20in%20Tribute%20to%20Martin%20Gardner.pdf\"\u003eHow random are 3x+1 function iterates?\u003c/a\u003e, in The Mathemagician and the Pied Puzzler - A Collection in Tribute to Martin Gardner, Ed. E. R. Berlekamp and T. Rogers, A. K. Peters, 1999, pp. 253-266.",
				"J. C. Lagarias, \u003ca href=\"http://arxiv.org/abs/math/0608208\"\u003eThe 3x+1 Problem: an annotated bibliography, II (2000-2009)\u003c/a\u003e, arXiv:0608208 [math.NT], 2006-2012.",
				"J. C. Lagarias, ed., \u003ca href=\"http://www.ams.org/bookstore-getitem/item=mbk-78\"\u003eThe Ultimate Challenge: The 3x+1 Problem\u003c/a\u003e, Amer. Math. Soc., 2010.",
				"Jeffrey C. Lagarias, \u003ca href=\"https://arxiv.org/abs/2111.02635\"\u003eThe 3x+1 Problem: An Overview\u003c/a\u003e, arXiv:2111.02635 [math.NT], 2021.",
				"M. Le Brun, \u003ca href=\"/A006577/a006577.pdf\"\u003eEmail to N. J. A. Sloane, Jul 1991\u003c/a\u003e",
				"Mathematical BBS, \u003ca href=\"http://felix.unife.it/Root/d-Mathematics/d-Number-theory/b-3x+1\"\u003eBiblography on Collatz Sequence\u003c/a\u003e",
				"P. Picart, \u003ca href=\"http://trucsmaths.free.fr/js_syracuse.htm\"\u003eAlgorithme de Collatz et conjecture de Syracuse\u003c/a\u003e",
				"E. Roosendaal, \u003ca href=\"http://www.ericr.nl/wondrous/index.html\"\u003eOn the 3x+1 problem\u003c/a\u003e",
				"J. L. Simons, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-04-01728-4\"\u003eOn the nonexistence of 2-cycles for the 3x+1 problem\u003c/a\u003e, Math. Comp. 75 (2005), 1565-1572.",
				"G. Villemin's Almanach of Numbers, \u003ca href=\"http://translate.google.com/translate?hl=en\u0026amp;sl=fr\u0026amp;u=http://villemin.gerard.free.fr/Wwwgvmm/Iteration/Syracuse.htm#top\"\u003eCycle of Syracuse\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CollatzProblem.html\"\u003eCollatz Problem\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Collatz_conjecture\"\u003eCollatz Conjecture\u003c/a\u003e",
				"\u003ca href=\"/index/3#3x1\"\u003eIndex entries for sequences related to 3x+1 (or Collatz) problem\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A006666(n) + A006667(n).",
				"a(n) = A112695(n) + 2 for n \u003e 2. - _Reinhard Zumkeller_, Apr 18 2008",
				"a(n) = A008908(n) - 1. - _L. Edson Jeffery_, Jul 21 2014"
			],
			"example": [
				"a(5)=5 because the trajectory of 5 is (5,16,8,4,2,1)."
			],
			"maple": [
				"A006577 := proc(n)",
				"        local a,traj ;",
				"        a := 0 ;",
				"        traj := n ;",
				"        while traj \u003e 1 do",
				"                if type(traj,'even') then",
				"                        traj := traj/2 ;",
				"                else",
				"                        traj := 3*traj+1 ;",
				"                end if;",
				"                a := a+1 ;",
				"        end do:",
				"        return a;",
				"end proc: # _R. J. Mathar_, Jul 08 2012"
			],
			"mathematica": [
				"f[n_] := Module[{a=n,k=0}, While[a!=1, k++; If[EvenQ[a], a=a/2, a=a*3+1]]; k]; Table[f[n],{n,4!}] (* _Vladimir Joseph Stephan Orlovsky_, Jan 08 2011 *)",
				"Table[Length[NestWhileList[If[EvenQ[#],#/2,3#+1]\u0026,n,#!=1\u0026]]-1,{n,80}] (* _Harvey P. Dale_, May 21 2012 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c0,0,s=n; c=0; while(s\u003e1,s=if(s%2,3*s+1,s/2); c++); c)",
				"(PARI) step(n)=if(n%2,3*n+1,n/2);",
				"A006577(n)=if(n==1,0,A006577(step(n))+1); \\\\ _Michael B. Porter_, Jun 05 2010",
				"(Haskell)",
				"import Data.List (findIndex)",
				"import Data.Maybe (fromJust)",
				"a006577 n = fromJust $ findIndex (n `elem`) a127824_tabf",
				"-- _Reinhard Zumkeller_, Oct 04 2012, Aug 30 2012",
				"(Python)",
				"def a(n):",
				"    if n==1: return 0",
				"    x=0",
				"    while True:",
				"        if n%2==0: n//=2",
				"        else: n = 3*n + 1",
				"        x+=1",
				"        if n\u003c2: break",
				"    return x",
				"print([a(n) for n in range(1, 101)]) # _Indranil Ghosh_, Jun 05 2017"
			],
			"xref": [
				"See A070165 for triangle giving trajectories of n = 1, 2, 3, ....",
				"Cf. A006370, A125731, A127885, A127886, A008908, A112695.",
				"See also A008884, A161021, A161022, A161023."
			],
			"keyword": "nonn,nice,easy,hear,look",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_, _Bill Gosper_",
			"ext": [
				"More terms from Larry Reeves (larryr(AT)acm.org), Apr 27 2001",
				"\"Escape clause\" added to definition by _N. J. A. Sloane_, Jun 06 2017"
			],
			"references": 219,
			"revision": 171,
			"time": "2021-11-05T09:36:20-04:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}