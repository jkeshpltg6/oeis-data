{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A118195",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 118195,
			"data": "1,1,3,23,411,15771,1353045,252512065,106798723795,99080638950595,208993838938550873,968425792397232696773,10208662119796586878979989,236472963735267887311598074949,12462692176683507314938059670486683",
			"name": "Self-convolution square-root of A118191, where A118191 is column 0 of the matrix square of triangle A118190 with A118190(n,k) = (5^k)^(n-k).",
			"comment": [
				"In general, sqrt( Sum_{n\u003e=0} x^n/(1 - q^n*x) ) is an integer series whenever q == 1 (mod 4)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A118195/b118195.txt\"\u003eTable of n, a(n) for n = 0..75\u003c/a\u003e"
			],
			"formula": [
				"G.f.: A(x) = sqrt( Sum_{n\u003e=0} x^n/(1-5^n*x) )."
			],
			"example": [
				"A(x) = 1 + x + 3*x^2 + 23*x^3 + 411*x^4 + 15771*x^5 + ...",
				"A(x)^2 = 1 + 2*x + 7*x^2 + 52*x^3 + 877*x^4 + 32502*x^5 + ...",
				"= 1/(1-x) + x/(1-5x) + x^2/(1-25x) + x^3/(1-125x) + ..."
			],
			"mathematica": [
				"With[{m = 30}, CoefficientList[Series[Sqrt[Sum[x^j/(1 - 5^j*x), {j, 0, m + 2}]], {x, 0, m}], x]] (* _G. C. Greubel_, Jun 30 2021 *)"
			],
			"program": [
				"(PARI) a(n)=polcoeff(sqrt(sum(k=0,n,sum(j=0, k, (5^j)^(k-j) )*x^k+x*O(x^n))),n)",
				"(MAGMA)",
				"m:=30;",
				"R\u003cx\u003e:=PowerSeriesRing(Rationals(), m);",
				"Coefficients(R!( Sqrt( (\u0026+[x^j/(1-5^j*x): j in [0..m+2]]) ) )); // _G. C. Greubel_, Jun 30 2021",
				"(Sage)",
				"m=30;",
				"def A118195_list(prec):",
				"    P.\u003cx\u003e = PowerSeriesRing(ZZ, prec)",
				"    return P( sqrt(sum( x^j/(1-5^j*x) for j in (0..m+2))) ).list()",
				"A118195_list(m) # _G. C. Greubel_, Jun 30 2021"
			],
			"xref": [
				"Cf. A118190, A118191."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Paul D. Hanna_, Apr 15 2006",
			"references": 2,
			"revision": 11,
			"time": "2021-06-30T04:32:48-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}