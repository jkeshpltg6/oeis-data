{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A278080",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 278080,
			"data": "0,0,1,-5,126,1490,118151,8256885,808428076,100199284180,15432169163901,2889536106161375,646438926423519626,170294687860735726470,52177485058722877649251,18397662218707151323777465,7396641315814156362154666776",
			"name": "E.g.f. (1/4!)*sin^4(x)/cos(x) (coefficients of even powers only).",
			"comment": [
				"This sequence gives the coefficients in an asymptotic expansion of a series related to the constant Pi. It can be shown that (1/4!)*Pi/4 = Sum_{k \u003e= 1} (-1)^(k-1)/((2*k - 5)*(2*k - 3)*(2*k - 1)*(2*k + 1)*(2*k + 3)). Using Proposition 1 of Borwein et al. it can be shown that the following asymptotic expansion holds for the tails of the series: for N divisible by 4, 2*( (1/4!)*Pi/4 - Sum_{k = 1..N/2} (-1)^(k-1)/((2*k - 5)*(2*k - 3)*(2*k - 1)*(2*k + 1)*(2*k + 3)) ) ~ 1/N^5 - (-5)/N^7 + 126/N^9 - 1490/N^11 + 118151/N^13 - .... An example is given below. Cf. A024235 and A278195."
			],
			"link": [
				"J. M. Borwein, P. B. Borwein, K. Dilcher, \u003ca href=\"http://www.jstor.org/stable/2324715\"\u003ePi, Euler numbers and asymptotic expansions\u003c/a\u003e, Amer. Math. Monthly, 96 (1989), 681-687.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/EulerPolynomial.html\"\u003eMathWorld: Euler Polynomial\u003c/a\u003e"
			],
			"formula": [
				"a(n) = [x^(2*n)/(2*n)!] ( (1/4!)*sin^4(x)/cos(x) ).",
				"a(n) = (1/4!)*( A000364(n) + (-1)^n*(9^(n) - 5)/4 ).",
				"a(n) = (-1)^n/(2^4*4!) * 2^(2*n)*( E(2*n,5/2) - 4*E(2*n,3/2) + 6*E(2*n,1/2) - 4*E(2*n,-1/2) + E(2*n,-3/2) ), where E(n,x) is the Euler polynomial of order n.",
				"E.g.f. (1/4!)*sin^4(x)/cos(x) = x^4/4! - 5*x^6/6! + 126*x^8/8! + 1490*x^10/10! + ....",
				"O.g.f. for a signed version of the sequence: Sum_{n \u003e= 0} ( (1/2^n) * Sum_{k = 0..n} (-1)^k*binomial(n, k)/((1 - (2*k - 3)*x)*(1 - (2*k - 1)*x)*(1 - (2*k + 1)*x)*(1 - (2*k + 3)*x)*(1 - (2*k + 5)*x)) ) = 1 + 5*x^2 + 126*x^4 - 1490*x^6 + 118151*x^8 - ...."
			],
			"example": [
				"Let N = 100000. The truncated series 2*Sum_{k = 1..N/2} (-1)^(k-1)/((2*k - 5)*(2*k - 3)*(2*k - 1)*(2*k + 1)*(2*k + 3)) = 0.065449846949787359134638(3)038183229(2)6754107(569)820314(8536)0364.... The bracketed digits show where this decimal expansion differs from that of Pi/48. The numbers 1, 5, 126, -1490 must be added to the bracketed numbers to give the correct decimal expansion to 60 digits: Pi/48 = 0.065449846949787359134638(4) 038183229(7)6754107(695)820314(7046)0364.. .."
			],
			"maple": [
				"A000364 := n -\u003e abs(euler(2*n)):",
				"seq(1/4!*(A000364(n) + (-1)^n*(9^n - 5)/4), n = 0..20);"
			],
			"xref": [
				"Cf. A000364, A004174, A024235, A166984, A278079, A278194, A278195."
			],
			"keyword": "sign,easy",
			"offset": "0,4",
			"author": "_Peter Bala_, Nov 10 2016",
			"references": 6,
			"revision": 14,
			"time": "2016-11-16T03:47:38-05:00",
			"created": "2016-11-16T03:47:38-05:00"
		}
	]
}