{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A059434",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 59434,
			"data": "1,1,2,1,3,6,1,7,16,26,1,27,69,118,168,1,169,455,810,1192,1575,1,1576,4343,7920,11952,16153,20355,1,20356,56864,105324,161704,222427,284726,347026,1,347027,978779,1832958,2848841,3970048,5148119,6346546,7544974",
			"name": "Triangle formed when cumulative boustrophedon transform is applied to 1, 1, 1, 1, ..., read by rows in natural order.",
			"formula": [
				"From _Petros Hadjicostas_, Feb 16 2021: (Start)",
				"T(i,j) = T(i,j-1) + Sum_{r=1..j} T(i-1,i-r) for i \u003e= 1 and 1 \u003c= j \u003c= i with T(i,0) = b(i+1) for i \u003e= 0, where b(i) = 1 for i \u003e= 1. (The sequence b = (b(i): i \u003e= 1) is the input sequence.)",
				"T(i,j) = 2*T(i,j-1) - T(i,j-2) + T(i-1,i-j) for i \u003e= 2 and 2 \u003c= j \u003c= i.",
				"T(i,i) = A059430(i) = T(i+1,1) - 1 for i \u003e= 0. (End)"
			],
			"example": [
				"Triangle T(i,j) (with rows i \u003e= 0 and columns j = 0..i) begins:",
				"  1;",
				"  1,    2;",
				"  1,    3,    6;",
				"  1,    7,   16,   26;",
				"  1,   27,   69,  118,   168;",
				"  1,  169,  455,  810,  1192,  1575;",
				"  1, 1576, 4343, 7920, 11952, 16153, 20355;",
				"  ... - _Petros Hadjicostas_, Feb 16 2021"
			],
			"maple": [
				"# This is a modification of _N. J. A. Sloane_'s program from A059429:",
				"CBOUS2 := proc(a) local c, i, j, n, r: option remember: if whattype(a) \u003c\u003e list then RETURN([]): end if: n := min(nops(a), 60): for i from 0 to n - 1 do c[i, 0] := a[i + 1]: end do: for i to n - 1 do for j to i do c[i, j] := c[i, j - 1] + add(c[i - 1, i - r], r = 1 .. j): end do: end do: RETURN([seq(seq(c[i, j], j = 0 .. i), i = 0 .. n - 1)]): end proc:",
				"# To get the flattened triangle up to the 9th row, we type",
				"CBOUS2([1, 1, 1, 1, 1, 1, 1, 1, 1, 1]); # _Petros Hadjicostas_, Feb 16 2021"
			],
			"mathematica": [
				"nmax = 9; Clear[CBOUS2, c]; CBOUS2[a_List] := CBOUS2[a] = Module[{i, j, n, r }, n = Min[Length[a], nmax]; For[i = 0, i \u003c= n - 1, i++, c[i, 0] = a[[i + 1]]]; For[i = n - 1, i \u003c= nmax, i++, For[j = 1, j \u003c= i, j++, c[i, j] = c[i, j - 1] + Sum[c[i - 1, i - r], {r, 1, j}]]]; Return[Table[c[i, i], {i, 0, n - 1}]]]; Do[CBOUS2[Table[1, {n}]], {n, 0, nmax}]; Table[c[i, j], {i, 0, nmax - 1}, {j, 0, i}] // Flatten (* _Jean-François Alcover_, Jul 14 2017, adapted from Maple code for A059430 *)"
			],
			"xref": [
				"Cf. A059429, A059430, A059433."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_, Jan 31 2001",
			"ext": [
				"More terms from _Floor van Lamoen_, Oct 08 2001"
			],
			"references": 3,
			"revision": 26,
			"time": "2021-02-17T04:08:27-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}