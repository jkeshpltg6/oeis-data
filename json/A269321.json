{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A269321",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 269321,
			"data": "142097,173944,259653,283673,320785,321053,326945,335229,412277,424236,459964,471713,476152,527068,535441,551384,567473,621749,637820,681276,686977,729293,747496,750376,782737,784997,807937,893029,916181,942961,966053,967928,974157,982049",
			"name": "Discriminants of real quadratic fields with 3-class tower group \u003c81,7\u003e",
			"comment": [
				"The Artin transfer homomorphisms of the assigned 3-class tower group G with SmallGroups identifier \u003c81,7\u003e [Besche, Eick, O'Brien], which is better known as the 3-Sylow subgroup Syl_3(A_9) of the alternating group of degree 9, determine the capitulation type (2,0,0,0) (TKT without fixed point) of the real quadratic field K in its four unramified cyclic cubic extensions N_i|K (i=1,...,4) and the abelian type invariants of the 3-class groups Cl(3,K)=(3,3) (whence A269321 is a subsequence of A269319) and [Cl(3,N_i)]=[(3,3,3),(3,3),(3,3),(3,3)] (TTT or IPAD). Conversely, the group G=\u003c81,7\u003e is determined uniquely not only by its Artin pattern AP(G)=(TTT,TKT) but even by the TTT component alone [Mayer, 2014, Fig.3.1, Tbl.4.1], where TKT, TTT, IPAD are abbreviations for transfer kernel type, transfer target type, index-p abelianization data, respectively [Mayer, 2016]. Consequently, it suffices that the MAGMA program only determines the TTT component of the Artin pattern. This is an instance of the \"Principalization algorithm via class group structure\" [Mayer, 2014] and saves a considerable amount of CPU time, since the determination of the TKT component is very delicate. In fact, G=\u003c81,7\u003e is the unique finite 3-group of coclass cc(G)=1 with a component (3,3,3) in its IPAD. Since the group G=\u003c81,7\u003e has derived length dl(G)=2, the Hilbert 3-class field tower of these real quadratic fields consists of exactly two stages.",
				"The MAGMA program requires A269319 as its input list."
			],
			"link": [
				"H. U. Besche, B. Eick, and E. A. O'Brien, \u003ca href=\"http://www.icm.tu-bs.de/ag_algebra/software/small/small.html\"\u003eThe SmallGroups Library\u003c/a\u003e - a library of groups of small order, 2005, an accepted and refereed GAP package, available also in MAGMA.",
				"M. R. Bush, \u003ca href=\"http://home.wlu.edu/~bushm/Research/research.html\"\u003eprivate communication\u003c/a\u003e, 11 July 2015.",
				"D. C. Mayer, \u003ca href=\"https://www.researchgate.net/publication/234063636\"\u003eList of discriminants less than 200000 of totally real cubic fields\u003c/a\u003e, 1991, ResearchGate.",
				"D. C. Mayer, \u003ca href=\"http://www.algebra.at/Memorial2009Principalization.htm\"\u003eAll known examples for principalization types\u003c/a\u003e, Memorial 2009.",
				"D. C. Mayer, \u003ca href=\"http://dx.doi.org/10.1142/S179304211250025X\"\u003eThe second p-class group of a number field\u003c/a\u003e, Int. J. Number Theory 8 (2012), no. 2, 471-505.",
				"D. C. Mayer, \u003ca href=\"http://dx.doi.org/10.5802/jtnb.874\"\u003ePrincipalization algorithm via class group structure\u003c/a\u003e, J. Thèor. Nombres Bordeaux 26 (2014), no. 2, 415-464.",
				"D. C. Mayer, \u003ca href=\"http://dx.doi.org/10.4236/apm.2016.62008\"\u003eArtin transfer patterns on descendant trees of finite p-groups\u003c/a\u003e, Adv. Pure Math. 6 (2016), no. 2, 66-104."
			],
			"example": [
				"The two leading terms, 142097, 173944, were listed in [Mayer, 1991] (up to 2*10^5) without giving the Artin pattern. The first 34 terms of A269321 up to 10^6 have been published in [Mayer, 2009]. The first 698 terms up to 10^7 have been determined in [Mayer, 2012] and [Mayer, 2014] with erroneous counter 697 corrected by [Bush]. The 10244, resp. 122955, terms up to 10^8, resp. 10^9, have been computed by [Bush]."
			],
			"program": [
				"(MAGMA) SetClassGroupBounds(\"GRH\"); p:=3; dList:=A269319; for d in dList do",
				"ZX\u003cX\u003e:=PolynomialRing(Integers()); K:=NumberField(X^2-d); O:=MaximalOrder(K); C,mC:=ClassGroup(O); sS:=Subgroups(C: Quot:=[p]); sI:=[]; for j in [1..p+1] do Append(~sI,0); end for; n:=Ngens(C); g:=(Order(C.(n-1)) div p)*C.(n-1); h:=(Order(C.n) div p)*C.n; ct:=0; for x in sS do ct:=ct+1; if g in x`subgroup then sI[1]:=ct; end if; if h in x`subgroup then sI[2]:=ct; end if; for e in [1..p-1] do if g+e*h in x`subgroup then sI[e+2]:=ct; end if; end for; end for;",
				"sA:=[AbelianExtension(Inverse(mQ)*mC) where Q,mQ:=quo\u003cC|x`subgroup\u003e: x in sS];",
				"sN:=[NumberField(x): x in sA]; sR:=[MaximalOrder(x): x in sA];",
				"sF:=[AbsoluteField(x): x in sN]; sM:=[MaximalOrder(x): x in sF];",
				"sM:=[OptimizedRepresentation(x): x in sF];",
				"sA:=[NumberField(DefiningPolynomial(x)): x in sM]; sO:=[Simplify(LLL(MaximalOrder(x))): x in sA]; TTT:=[]; epsilon:=0; polarization1:=3; polarization2:=3; for j in [1..#sO] do CO:=ClassGroup(sO[j]); Append(~TTT,pPrimaryInvariants(CO,p));",
				"if (3 eq #pPrimaryInvariants(CO,p)) then epsilon:=epsilon+1; end if;",
				"val:=Valuation(Order(CO),p); if (2 eq val) then polarization2:=val; elif (4 le val) then if (3 eq polarization1) then polarization1:=val; else polarization2:=val; end if; end if; end for; if (2 eq polarization2) and (3 eq polarization1) and (1 eq epsilon) then printf \"%o, \",d; end if; end for;"
			],
			"xref": [
				"Subsequence of A269319"
			],
			"keyword": "nonn,hard",
			"offset": "1,1",
			"author": "_Daniel Constantin Mayer_, Mar 10 2016",
			"references": 1,
			"revision": 10,
			"time": "2016-04-05T01:01:24-04:00",
			"created": "2016-04-05T01:01:24-04:00"
		}
	]
}