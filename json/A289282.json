{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A289282",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 289282,
			"data": "1,1,2,6,24,120,720,5040,40320,362880,3628800,39916800,479001600,1932053504,1278945280,2004310016,2004189184,-288522240,-898433024,109641728,-2102132736,-1195114496,-522715136,862453760,-775946240,2076180480,-1853882368,1484783616,-1375731712,-1241513984,1409286144,738197504,-2147483648,-2147483648,0,0,0",
			"name": "The least significant four bytes of n! interpreted in two's complement.",
			"comment": [
				"a(n) = 0 for n \u003e= 34. - _Georg Fischer_, Mar 12 2019"
			],
			"example": [
				"The hexadecimal column in the following list shows how the bits in the least significant four bytes are shifted to the left by each factor containing some power of 2. With two's complement the terms are negative when the highest bit is one. - _Georg Fischer_, Mar 13 2019",
				"  28 -1375731712 # ae000000",
				"  29 -1241513984 # b6000000",
				"  30  1409286144 # 54000000",
				"  31   738197504 # 2c000000",
				"  32 -2147483648 # 80000000",
				"  33 -2147483648 # 80000000",
				"  34           0 # 00000000"
			],
			"mathematica": [
				"Table[Mod[n!, 2^32] - Boole[Mod[n!, 2^32] \u003e 2^31 - 1] * 2^32, {n, 0, 49}]"
			],
			"program": [
				"(Java) import java.math.BigInteger;",
				"public class a289282 {",
				"    public static void main(String[] args) {",
				"        BigInteger pow32 = BigInteger.valueOf(2).pow(32);",
				"        BigInteger bfact  = BigInteger.ONE;",
				"        for (int i = 0; i \u003c 48; i++) {",
				"            bfact = bfact.multiply(BigInteger.valueOf(i == 0 ? 1 : i));",
				"            System.out.println(String.valueOf(i) + \" \"",
				"                + String.valueOf(bfact.mod(pow32).intValue()));",
				"        }",
				"    } // main",
				"} // _Georg Fischer_, Mar 12 2019",
				"(PARI) Bits = 32; N = Mod(1, 2^Bits); j = 0; until(N == 0, print1(-centerlift(-N), \", \"); j += 1; N *= j); \\\\ _Jack Brennen_, Jun 30 2017",
				"(Scala) (1 to 36).scanLeft(1)(_ * _) // Scala infers 1 and 36 are Int, which become int primitives in the Java Virtual Machine. - _Alonso del Arte_, Mar 02 2019",
				"(Python)",
				"import math",
				"def A289282(n):",
				"    f = math.factorial(n)",
				"    least_four = f \u0026 0xffffffff",
				"    mask = 0x7fffffff",
				"    return (least_four \u0026 mask) - (least_four \u0026 ~mask)",
				"print([A289282(n) for n in range(37)]) # _Peter Luschny_, Mar 13 2019"
			],
			"xref": [
				"Cf. A000142, A036603."
			],
			"keyword": "sign,easy",
			"offset": "0,3",
			"author": "_Alonso del Arte_, Jul 01 2017",
			"references": 1,
			"revision": 38,
			"time": "2021-08-09T21:12:00-04:00",
			"created": "2017-07-20T16:05:08-04:00"
		}
	]
}