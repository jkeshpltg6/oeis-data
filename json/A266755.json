{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A266755",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 266755,
			"data": "1,0,1,1,2,1,3,2,4,3,5,4,7,5,8,7,10,8,12,10,14,12,16,14,19,16,21,19,24,21,27,24,30,27,33,30,37,33,40,37,44,40,48,44,52,48,56,52,61,56,65,61,70,65,75,70,80,75,85,80,91,85,96,91,102,96,108,102,114,108,120,114,127,120,133,127,140,133,147,140,154,147,161,154,169",
			"name": "Expansion of 1/((1-x^2)*(1-x^3)*(1-x^4)).",
			"comment": [
				"This is the same as A005044 but without the three leading zeros. There are so many situations where one wants this sequence rather than A005044 that it seems appropriate for it to have its own entry.",
				"But see A005044 (still the main entry) for numerous applications and references.",
				"Also, Molien series for invariants of finite Coxeter group D_3.",
				"The Molien series for the finite Coxeter group of type D_k (k \u003e= 3) has G.f. = 1/Prod_i (1-x^(1+m_i)) where the m_i are [1,3,5,...,2k-3,k-1]. If k is even only even powers of x appear, and we bisect the sequence.",
				"Also, Molien series for invariants of finite Coxeter group A_3. The Molien series for the finite Coxeter group of type A_k (k \u003e= 1) has G.f. = 1/Prod_{i=2..k+1} (1-x^i). Note that this is the root system A_k not the alternating group Alt_k.",
				"a(n) is the number of partitions of n into parts 2, 3, and 4. - _Joerg Arndt_, Apr 16 2017",
				"From _Gus Wiseman_, May 23 2021: (Start)",
				"Also the number of integer partitions of n into at most n/2 parts, none greater than 3. The case of any maximum is A110618. The case of any length is A001399. The Heinz numbers of these partitions are given by A344293.",
				"For example, the a(2) = 1 through a(13) = 5 partitions are:",
				"    2  3  22  32  33   322  332   333   3322   3332   3333    33322",
				"          31      222  331  2222  3222  3331   32222  33222   33331",
				"                  321       3221  3321  22222  33221  33321   322222",
				"                            3311        32221  33311  222222  332221",
				"                                        33211         322221  333211",
				"                                                      332211",
				"                                                      333111",
				"(End)"
			],
			"reference": [
				"J. E. Humphreys, Reflection Groups and Coxeter Groups, Cambridge, 1990. See Table 3.1, page 59."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A266755/b266755.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Sara C. Billey, Matjaž Konvalinka, Joshua P. Swanson, \u003ca href=\"https://arxiv.org/abs/1905.00975\"\u003eAsymptotic normality of the major index on standard tableaux\u003c/a\u003e, arXiv:1905.00975 [math.CO], 2019.",
				"\u003ca href=\"/index/Mo#Molien\"\u003eIndex entries for Molien series\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_09\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,1,1,1,-1,-1,-1,0,1)."
			],
			"formula": [
				"a(n) = a(n-2) + a(n-3) + a(n-4) - a(n-5) - a(n-6) - a(n-7) + a(n-9) for n\u003e8. - _Vincenzo Librandi_, Jan 11 2016"
			],
			"mathematica": [
				"CoefficientList[Series[1/((1-x^2)(1-x^3)(1-x^4)), {x, 0, 100}], x] (* _JungHwan Min_, Jan 10 2016 *)",
				"LinearRecurrence[{0,1,1,1,-1,-1,-1,0,1}, {1,0,1,1,2,1,3,2,4}, 100] (* _Vincenzo Librandi_, Jan 11 2016 *)",
				"Table[Length[Select[IntegerPartitions[n],Length[#]\u003c=n/2\u0026\u0026Max@@#\u003c=3\u0026]],{n,0,30}] (* _Gus Wiseman_, May 23 2021 *)"
			],
			"program": [
				"(PARI) Vec(1/((1-x^2)*(1-x^3)*(1-x^4)) + O(x^100)) \\\\ _Michel Marcus_, Jan 11 2016",
				"(MAGMA) I:=[1,0,1,1,2,1,3,2,4]; [n le 9 select I[n] else Self(n-2)+ Self(n-3)+Self(n-4)-Self(n-5)-Self(n-6)-Self(n-7)+Self(n-9): n in [1..100]]; // _Vincenzo Librandi_, Jan 11 2016",
				"(Sage) (1/((1-x^2)*(1-x^3)*(1-x^4))).series(x, 100).coefficients(x, sparse=False) # _G. C. Greubel_, Jun 13 2019"
			],
			"xref": [
				"Molien series for finite Coxeter groups A_1 through A_12 are A059841, A103221, A266755, A008667, A037145, A001996, and A266776-A266781.",
				"Molien series for finite Coxeter groups D_3 through D_12 are A266755, A266769, A266768, A003402, and A266770-A266775.",
				"A variant of A005044.",
				"Cf. A001400 (partial sums).",
				"Cf. A308065.",
				"Number of partitions of n whose Heinz number is in A344293.",
				"A001399 counts partitions with all parts \u003c= 3, ranked by A051037.",
				"A025065 counts partitions of n with \u003e= n/2 parts, ranked by A344296.",
				"A035363 counts partitions of n with n/2 parts, ranked by A340387.",
				"A110618 counts partitions of n into at most n/2 parts, ranked by A344291.",
				"Cf. A000041, A008642, A279622, A325691, A344294, A344297."
			],
			"keyword": "nonn,easy",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_, Jan 10 2016",
			"references": 27,
			"revision": 50,
			"time": "2021-05-24T00:48:07-04:00",
			"created": "2016-01-10T15:54:02-05:00"
		}
	]
}