{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097757",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97757,
			"data": "2,3,6,7,8,11,12,15,18,19,22,23,24,27,28,31,32,33,43,44,47,48,60,67,72,76,92,96,108,112,128,0,1,4,5,9,10,13,14,16,17,20,21,34,35,36,37,38,39,40,42,51,52,55,56,57,58,59,63,64,68,71,73,80,83,88,97,124,132,25,26,29",
			"name": "Table read by rows where row n consists of integers that can be expressed as the sum of distinct squares in exactly n ways.",
			"comment": [
				"Row 0 is A001422.",
				"Only positive squares are allowed, not 0. The inclusion of 0 in row 1 is for the empty sum, not for a sum with a single 0. - _Franklin T. Adams-Watters_, Sep 20 2009"
			],
			"link": [
				"R. Sprague, \u003ca href=\"https://dx.doi.org/10.1007/BF01181594\"\u003eÜber Zerlegungen in ungleiche Quadratzahlen\u003c/a\u003e, Math. Z. 51, (1948), 289-290.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/SquareNumber.html\"\u003eSquare Number.\u003c/a\u003e"
			],
			"example": [
				"Table begins:",
				"Row 0: 2, 3, 6, 7, 8, 11, 12, 15, 18, 19, 22, 23, 24, 27, 28, 31, 32, 33, 43, 44, 47, 48, 60, 67, 72, 76, 92, 96, 108, 112, 128;",
				"Row 1: 0, 1, 4, 5, 9, 10, 13, 14, 16, 17, 20, 21, 34, 35, 36, 37, 38, 39, 40, 42, 51, 52, 55, 56, 57, 58, 59, 63, 64, 68, 71, 73, 80, 83, 88, 97, 124, 132;",
				"Row 2: 25, 26, 29, 30, 41, 45, 46, 49, 53, 54, 61, 69, 70, 77, 79, 82, 84, 87, 93, 103, 107, 133, 144, 148, 188;",
				"Row 3: 50, 62, 66, 75, 81, 85, 86, 89, 91, 95, 98, 99, 100, 102, 104, 109, 113, 116, 118, 119, 123, 136, 137, 140, 152, 157, 172, 176, 177, 192;",
				"Row 4: 65, 74, 78, 101, 105, 106, 111, 115, 117, 120, 121, 122, 127, 141, 153, 160, 164, 168, 193;",
				"Row 5: 94, 125, 129, 131, 143, 145, 149, 156, 161, 163, 167, 173, 197, 213;",
				"Row 6: 90, 114, 134, 135, 138, 139, 147, 180, 181, 208, 212, 217, 228;",
				"Row 7: 110, 142, 151, 154, 158, 169, 184, 189, 204, 224;",
				"Row 8: 155, 159, 162, 165, 166, 182, 187, 196, 201, 202, 203, 216, 229, 233, 240, 252, 253;",
				"Row 9: 126, 130, 146, 150, 171, 178, 179, 183, 185, 200, 209, 236, 237, 241, 288;",
				"Row 10: 191, 205, 218, 232, 249, 257;",
				"Row 11: 170, 175, 198, 207, 220, 221, 227, 245, 272, 277, 293;",
				"Row 12: 186, 214, 225, 244, 248, 268, 297;",
				"Row 13: 174, 199, 223, 256, 265, 292;",
				"Row 14: 190, 194, 206, 215, 261, 269, 273, 281, 313, 317;",
				"Row 15: 211, 219, 242, 262, 301;",
				"Row 16: 195, 222, 239, 243, 276, 278, 289, 333;",
				"Row 17: 226, 230, 238, 264, 266, 284;",
				"Row 18: 210, 258, 263, 267, 285, 304, 308, 337;",
				"Row 19: 231, 246, 254, 260, 357;",
				"Row 20: 234, 247, 251, 282, 305, 309, 353;",
				"Row 21: 235, 250, 280, 298, 321, 329;",
				"Row 22: 332.",
				"Denoting r(n) the index of the row of the number n, among n = {350, ..., 1000} the only r-values below 28 are a(357) = 19, a(353) = 20, a(373) = 25, a(397) = 26, a(362) = 27, and all n \u003e= 400 have r(n) \u003e 30. So the above rows appear to be complete, and rows 23 \u0026 24 would be empty, as would be rows 39 \u0026 40 unless they have elements \u003e 1000. - _M. F. Hasler_, May 26 2020"
			],
			"program": [
				"(PARI) {r(n,m=n)=sum(x=1,min(sqrtint(n),m),r(n-x^2,x-1),!n)} \\\\ Gives index of the row in which number n is listed. - _M. F. Hasler_, May 26 2020"
			],
			"xref": [
				"Cf. A001422, A003995, A033461, A097563, A097758, A097759, A097760."
			],
			"keyword": "nonn,tabf",
			"offset": "0,1",
			"author": "_Ray Chandler_, Sep 06 2004",
			"references": 4,
			"revision": 15,
			"time": "2020-06-04T02:06:02-04:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}