{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A270640",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 270640,
			"data": "1,1,0,2,1,0,4,2,0,6,4,0,9,5,0,14,8,0,20,12,0,30,16,0,41,22,0,56,32,0,76,42,0,102,56,0,136,75,0,178,97,0,232,126,0,300,164,0,384,208,0,490,264,0,620,336,0,780,420,0,977,526,0,1218,656,0,1512,810",
			"name": "Expansion of psi(x) * psi(x^6) / psi(-x^3) in powers of x where psi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A270640/b270640.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of psi(x) / (chi(-x^3) * chi(-x^6)) in powers of x where psi(), chi() are Ramanujan theta functions.",
				"Expansion of q^(-1/2) * eta(q^2)^2 * eta(q^12) / (eta(q) * eta(q^3)) in powers of q.",
				"Euler transform of period 12 sequence [ 1, -1, 2, -1, 1, 0, 1, -1, 2, -1, 1, -1, ...].",
				"G.f.: Product_{k\u003e0} (1 + x^k) * (1 - x^(2*k)) * (1 + x^(3*k)) * (1 + x^(6*k))."
			],
			"example": [
				"G.f. = 1 + x + 2*x^3 + x^4 + 4*x^6 + 2*x^7 + 6*x^9 + 4*x^10 + 9*x^12 + ...",
				"G.f. = q + q^3 + 2*q^7 + q^9 + 4*q^13 + 2*q^15 + 6*q^19 + 4*q^21 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ 2^-1 x^(-1/8) EllipticTheta[ 2, 0, x^(1/2)] / (QPochhammer[ x^3, x^6] QPochhammer[ x^6, x^12]), {x, 0, n}];",
				"a[ n_] := SeriesCoefficient[ 2^(-3/2) x^(-1/2) EllipticTheta[ 2, 0, x^(1/2)] EllipticTheta[ 2, 0, x^3] / EllipticTheta[ 2, Pi/4, x^(3/2)], {x, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^2 * eta(x^12 + A) / (eta(x + A) * eta(x^3 + A)), n))};"
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Michael Somos_, Mar 20 2016",
			"references": 1,
			"revision": 10,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2016-03-20T16:14:21-04:00"
		}
	]
}