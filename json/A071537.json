{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A071537",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 71537,
			"data": "1,4,6,8,10,9,14,12,15,16,22,18,26,21,20,24,34,25,38,27,28,33,46,30,32,39,35,36,58,40,62,42,44,51,45,48,74,57,52,49,82,50,86,55,54,69,94,56,60,63,68,65,106,64,66,70,76,87,118,72,122,93,75,77,78,80,134,85,92,84,142,81,146,111,88,95,90,91,158,96,98",
			"name": "Least integer m \u003e= n such that there exist integers k_n, k_n+1, ..., k_m with nonzero k_n, for which n^(k_n) * (n+1)^(k_n+1) * ... * m^(k_m) = 1.",
			"comment": [
				"Conjecture: this is a permutation of the nonprimes A018252.",
				"From _Max Alekseyev_, Oct 22 2008: (Start)",
				"The conjecture is true. It is easy to see that (i) a(n)=m cannot be prime; (ii) k_m is always nonzero; (iii) for any nonprime s, there exist integers k_p,...,k_s, where p is the smallest prime divisor of s, k_p and k_s are nonzero, for which p^(k_p)*...*s^(k_s) = 1. The bijectivity of a(n) (as a function between positive integers and nonprimes) now follows from the lemma.",
				"*Lemma*. If a \u003c b \u003c= m and a^(p_a)*...* m^(p_m) = b^(q_b)*...* m^(q_m) = 1 where p_a, p_m, q_b and q_m are nonzero, then there exists integer s, a \u003c= s \u003c m, such that a^(r_a)*...* m^(r_s) = 1 where r_a and r_s are nonzero. Similarly, if a \u003c= s \u003c m and a^(p_a)*...* s^(p_s) = a^(q_a)*...* m^(q_m) = 1 where p_a, p_s, q_a and q_m are nonzero, then there exists integer b, a \u003c b \u003c= m, such that b^(r_b)*...* m^(r_m) = 1 where r_b and r_m are nonzero. (end)"
			],
			"example": [
				"a(18) = 25: we have 18^4 * 19^0 * 20^10 * 21^0 * 22^0 * 23^0 * 24^-8 * 25^-5 = 1."
			],
			"mathematica": [
				"a[n_] := Module[{m, d, f, M, M2}, If[n == 1, Return[1]]; m = n; While[True, m++; If[PrimeQ[m], Continue[]]; d = PrimePi[m]; M = Array[0 \u0026, {d, m - n + 1}]; For[k = 0, k \u003c= m - n, k++, f = FactorInteger[n + k]; For[j = 1, j \u003c= Length[f], j++, M[[PrimePi[f[[j, 1]]], k + 1]] = f[[j, 2]] ]]; M2 = Table[M[[i, j + 1]], {i, 1, d}, {j, 1, m - n}]; If[MatrixRank[M] == MatrixRank[M2], Return[m]]]];",
				"Array[a, 81] (* _Jean-François Alcover_, Jun 12 2017, translated from PARI *)"
			],
			"program": [
				"(PARI) { a(n) = local(m,d,f,M,M2); if(n==1,return(1)); m=n; while(1, m++; if(isprime(m),next); d=primepi(m); M=matrix(d,m-n+1); for(k=0,m-n, f=factor(n+k); for(j=1,matsize(f)[1], M[primepi(f[j,1]),k+1]=f[j,2] )); M2=matrix(d,m-n,i,j,M[i,j+1]); if(matrank(M)==matrank(M2),return(m)); ) } \\\\ _Max Alekseyev_, Oct 22 2008"
			],
			"keyword": "nice,nonn",
			"offset": "1,2",
			"author": "_Alex Fink_, May 30 2002",
			"ext": [
				"Corrected definition, comments and extra terms from _Max Alekseyev_, Oct 22 2008"
			],
			"references": 1,
			"revision": 12,
			"time": "2020-08-18T15:42:15-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}