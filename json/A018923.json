{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A018923",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 18923,
			"data": "16,32,63,124,244,480,944,1856,3649,7174,14104,27728,54512,107168,210687,414200,814296,1600864,3147216,6187264,12163841,23913482,47012668,92424472,181701728,357216192,702268543,1380623604,2714234540,5336044608,10490387488",
			"name": "Define the generalized Pisot sequence T(a(0),a(1)) by: a(n+2) is the greatest integer such that a(n+2)/a(n+1) \u003c a(n+1)/a(n). This is T(16,32).",
			"comment": [
				"Not to be confused with the Pisot T(16,32) sequence, which is essentially A000079. - _R. J. Mathar_, Feb 13 2016"
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A018923/b018923.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"D. W. Boyd, \u003ca href=\"http://www.researchgate.net/publication/258834801\"\u003eLinear recurrence relations for some generalized Pisot sequences\u003c/a\u003e, Advances in Number Theory ( Kingston ON, 1991) 333-340, Oxford Sci. Publ., Oxford Univ. Press, New York, 1993."
			],
			"formula": [
				"Empirical G.f.: -(8*x^5+4*x^4+2*x^3+x^2-16) / ((x-1)*(x^5+x^4+x^3+x^2+x-1)). - _Colin Barker_, Dec 21 2012",
				"a(n+1) = ceiling(a(n)^2/a(n-1))-1 for n\u003e0. - _Bruno Berselli_, Feb 15 2016"
			],
			"mathematica": [
				"T[a_, b_, n_] := Block[{s = {a, b}, k}, Do[k = 2 Last@ s; While[k/s[[i - 1]] \u003e= s[[i - 1]]/s[[i - 2]], k--]; AppendTo[s, k], {i, 3, n}]; s]; T[16, 32, 23] (* or *)",
				"a = {16, 32}; Do[AppendTo[a, Ceiling[a[[n - 1]]^2/a[[n - 2]]] - 1], {n, 3, 23}]; a (* _Michael De Vlieger_, Feb 15 2016 *)",
				"RecurrenceTable[{a[1] == 16, a[2] == 32, a[n] == Ceiling[a[n-1]^2/a[n-2] - 1]}, a, {n, 40}] (* _Vincenzo Librandi_, Feb 17 2016 *)"
			],
			"program": [
				"(PARI) T(a0, a1, maxn) = a=vector(maxn); a[1]=a0; a[2]=a1; for(n=3, maxn, a[n]=ceil(a[n-1]^2/a[n-2])-1); a",
				"T(16, 32, 30) \\\\ _Colin Barker_, Feb 16 2016"
			],
			"xref": [
				"Is this the same sequence as A001949?"
			],
			"keyword": "nonn",
			"offset": "0,1",
			"author": "_R. K. Guy_",
			"references": 1,
			"revision": 32,
			"time": "2016-02-17T04:56:50-05:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}