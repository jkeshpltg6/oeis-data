{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A318049",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 318049,
			"data": "1,0,1,0,1,1,1,3,2,6,8,11,26,28,67,96,162,316,448,922,1435,2572,4660,7563,14397,23896,43337,77097,133071,244787,423093,767732,1367412,2426612,4408497,7802348,14152342,25365035,45602031,82631362,148246136,269103870,485379304",
			"name": "Number of first/rest balanced rooted plane trees with n nodes.",
			"comment": [
				"A rooted plane tree is first/rest balanced if either (1) it is a single node, or (2a) the number of leaves in the first branch is equal to the number of branches minus one, and (2b) every branch is also first/rest balanced.",
				"Also the number of composable free pure multifunctions (CPMs) with one atom and n positions. A CPM is either (case 1) the leaf symbol \"o\", or (case 2) an expression of the form h[g_1, ..., g_k] where h and each of the g_i for i = 1, ..., k \u003e 0 are CPMs, and the number of leaves in h is equal to k. The number of positions in a CPM is the number of brackets [...] plus the number of o's."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A318049/b318049.txt\"\u003eTable of n, a(n) for n = 1..100\u003c/a\u003e"
			],
			"formula": [
				"G.f.: A(x,1) where A(x,y) satisfies A(x,y) = x*(y + Sum_{k\u003e=1} y^k * ([y^k] A(x,y)) * A(x,y)^k). - _Andrew Howroyd_, Jan 22 2021"
			],
			"example": [
				"The a(12) = 11 first/rest balanced rooted plane trees:",
				"  (o(o(o((oo)oo))))",
				"  (o(o((oo)(oo)o)))",
				"  (o(o((oo)o(oo))))",
				"  (o((oo)(o(oo))o))",
				"  (o((oo)o(o(oo))))",
				"  (o((oo)(oo)(oo)))",
				"  ((oo)(o(o(oo)))o)",
				"  ((oo)o(o(o(oo))))",
				"  ((o(o(oo)))oooo)",
				"  ((oo)(o(oo))(oo))",
				"  ((oo)(oo)(o(oo)))",
				"The a(12) = 11 composable free pure multifunctions:",
				"  o[o[o[o[o][o,o]]]]",
				"  o[o[o[o][o[o],o]]]",
				"  o[o[o[o][o,o[o]]]]",
				"  o[o[o][o[o[o]],o]]",
				"  o[o[o][o,o[o[o]]]]",
				"  o[o[o][o[o],o[o]]]",
				"  o[o][o[o[o[o]]],o]",
				"  o[o][o,o[o[o[o]]]]",
				"  o[o][o[o[o]],o[o]]",
				"  o[o][o[o],o[o[o]]]",
				"  o[o[o[o]]][o,o,o,o]"
			],
			"mathematica": [
				"balplane[n_]:=balplane[n]=If[n===1,{{}},Join@@Function[c,Select[Tuples[balplane/@c],Length[Cases[#[[1]],{},{0,Infinity}]]==Length[#]-1\u0026]]/@Join@@Permutations/@IntegerPartitions[n-1]];",
				"Table[Length[balplane[n]],{n,10}]"
			],
			"program": [
				"(PARI) seq(n)={my(p=x*y+O(x^2)); for(n=1, n\\2, p = x*y + x*sum(k=1, n, y^k * polcoef(p,k,y) * (O(x^(2*n-k+1)) + p)^k )); Vec(subst(p + O(x*x^n), y, 1)) } \\\\ _Andrew Howroyd_, Jan 22 2021"
			],
			"xref": [
				"Cf. A000081, A000108, A001003, A001006, A007853, A126120, A317713, A318046, A318048."
			],
			"keyword": "nonn",
			"offset": "1,8",
			"author": "_Gus Wiseman_, Aug 13 2018",
			"ext": [
				"Terms a(21) and beyond from _Andrew Howroyd_, Jan 22 2021"
			],
			"references": 5,
			"revision": 11,
			"time": "2021-01-22T15:43:57-05:00",
			"created": "2018-08-14T08:56:48-04:00"
		}
	]
}