{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A178749",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 178749,
			"data": "1,-1,-1,1,1,-1,-3,4,8,-13,-23,39,71,-121,-229,400,757,-1354,-2559,4625,8799,-16021,-30671,56316,108166,-200047,-385210,716429,1383331,-2585173,-5003791,9391680,18214565,-34318117,-66674463,126044208,245273927,-465067981",
			"name": "n*a(n) provides the Moebius transform of signed central binomial coefficients.",
			"comment": [
				"This should be related to the Coxeter transformation for the Tamari lattices.",
				"The source sequence is 1, -1, -2, 3, 6, -10, -20, 35, 70, -126, ... (A001405). Its Mobius transform is 1, -2, -3, 4, 5, -6, -21, 32, 72, -130, -253, 468, 923, ... and division of each term through n generates a(n). - _R. J. Mathar_, Jul 23 2012"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A178749/b178749.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"F. Chapoton, \u003ca href=\"http://dx.doi.org/10.5802/aif.2416\"\u003eLe module dendriforme sur le groupe cyclique\u003c/a\u003e, Ann. Inst. Fourier (Grenoble) 58 (2008), no. 7, 2333-2350. In French."
			],
			"example": [
				"G.f. = x - x^2 - x^3 + x^4 + x^5 - x^6 - 3*x^7 + 4*x^8 + 8*x^9 - 13*x^10 + ..."
			],
			"maple": [
				"with(numtheory):",
				"a:= n-\u003e add(mobius(n/d)*[1$2, -1$2][1+irem(d, 4)]*",
				"        binomial(d-1, iquo(d-1, 2)), d=divisors(n))/n:",
				"seq(a(n), n=1..50);  # _Alois P. Heinz_, Apr 05 2013"
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 1, 0, DivisorSum[ n, MoebiusMu[ n/#] (-1)^Quotient[ #, 2] Binomial[ # - 1, Quotient[ # - 1, 2]] \u0026] / n]; (* _Michael Somos_, Sep 14 2015 *)"
			],
			"program": [
				"(Sage)",
				"def lam(n):",
				"    return (-1)**binomial(n, 2) * binomial(n - 1, (n - 1) // 2)",
				"def a(n):",
				"    return sum(moebius(n // d) * lam(d) for d in divisors(n)) // n",
				"[a(n) for n in range(1, 20)]",
				"(PARI) {a(n) = if( n\u003c1, 0, sumdiv( n, d, moebius(n/d) * (-1)^(d\\2) * binomial(d-1, (d-1)\\2)) / n)}; /* _Michael Somos_, Dec 23 2014 */"
			],
			"xref": [
				"Similar to A022553, A131868 and A178738.",
				"Also related to A163210."
			],
			"keyword": "sign",
			"offset": "1,7",
			"author": "_F. Chapoton_, Jun 09 2010",
			"references": 3,
			"revision": 25,
			"time": "2020-05-30T16:19:52-04:00",
			"created": "2010-07-11T03:00:00-04:00"
		}
	]
}