{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A157704",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 157704,
			"data": "1,1,5,32,186,132,10,56,2814,17834,27324,11364,1078,10,48,17988,494720,3324209,7526484,6382271,2004296,203799,4580,5,16,72210,7108338,146595355,1025458635,2957655028,3828236468",
			"name": "G.f.s of the z^p coefficients of the polynomials in the GF3 denominators of A156927.",
			"comment": [
				"The formula for the PDGF3(z;n) polynomials in the GF3 denominators of A156927 can be found below.",
				"The general structure of the GFKT3(z;p) that generate the z^p coefficients of the PDGF3(z; n) polynomials can also be found below. The KT3(z;p) polynomials in the numerators of the GFKT3(z; p) have a nice symmetrical structure.",
				"The sequence of the number of terms of the first few KT3(z;p) polynomials is 1, 2, 4, 7, 10, 13, 14, 17, 20, 23, 26, 29, 32, 34, 36, 39, 42. The differences of this sequence and that of the number of terms of the KT4(z;p), see A157705, follow a simple pattern.",
				"A Maple algorithm that generates relevant GFKT3(z;p) information can be found below."
			],
			"formula": [
				"PDGF3(z;n) = Product_{k=0..n} (1-(k+1)*z)^(1+3*k) with n = 1, 2, 3, ...",
				"GFKT3(z;p) = (-1)^(p)*(z^q3)*KT3(z, p)/(1-z)^(3*p+1) with p = 0, 1, 2, ...",
				"The recurrence relation for the z^p coefficients a(n) is a(n) = Sum_{k=1..3*p+1} (-1)^(k+1)*binomial(3*p + 1, k)*a(n-k) with p = 0, 1, 2, ... ."
			],
			"example": [
				"Some PDGF3 (z;n) are:",
				"  PDGF3(z;n=3) = (1-z)*(1-2*z)^4*(1-3*z)^7*(1-4*z)^10",
				"  PDGF3(z;n=4) = (1-z)*(1-2*z)^4*(1-3*z)^7*(1-4*z)^10*(1-5*z)^13",
				"The first few GFKT3's are:",
				"  GFKT3(z;p=0) = 1/(1-z)",
				"  GFKT3(z;p=1) = -(5*z+1)/(1-z)^4",
				"  GFKT3(z;p=2) = z*(32+186*z+132*z^2+10*z^3)/(1-z)^7",
				"Some KT3(z,p) polynomials are:",
				"  KT3(z;p=2) = 32+186*z+132*z^2+10*z^3",
				"  KT3(z;p=3) = 56+2814*z+17834*z^2+27324*z^3+11364*z^4+1078*z^5+10*z^6"
			],
			"maple": [
				"p:=2; fn:=sum((-1)^(n1+1)*binomial(3*p+1,n1) *a(n-n1),n1=1..3*p+1): fk:=rsolve(a(n) = fn,a(k)): for n2 from 0 to 3*p+1 do fz(n2):=product((1-(k+1)*z)^(1+3*k), k=0..n2): a(n2):= coeff(fz(n2),z,p); end do: b:=n-\u003e a(n): seq(b(n), n=0..3*p+1); a(n)=fn; a(k)=sort(simplify(fk)); GFKT3(p):=sum((fk)*z^k, k=0..infinity); q3:=ldegree((numer(GFKT3(p)))): KT3(p):=sort((-1)^(p)*simplify((GFKT3(p)*(1-z)^(3*p+1))/z^q3),z, ascending);"
			],
			"xref": [
				"Originator sequence A156927.",
				"See A002414 for the z^1 coefficients and A157707 for the z^2 coefficients divided by 2.",
				"Row sums equal A064350 and those of A157705.",
				"Cf. A157702, A157703, A157705."
			],
			"keyword": "easy,nonn,tabf,uned",
			"offset": "0,3",
			"author": "_Johannes W. Meijer_, Mar 07 2009",
			"references": 8,
			"revision": 10,
			"time": "2021-04-15T21:47:24-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}