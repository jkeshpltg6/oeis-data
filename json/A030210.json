{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A030210",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 30210,
			"data": "1,-4,2,8,-5,-8,6,0,-23,20,32,16,-38,-24,-10,-64,26,92,100,-40,12,-128,-78,0,25,152,-100,48,-50,40,-108,256,64,-104,-30,-184,266,-400,-76,0,22,-48,442,256,115,312,-514,-128,-307,-100,52,-304,2,400,-160,0,200,200,500,-80,-518,432,-138,-512",
			"name": "Expansion of (eta(q) * eta(q^5))^4 in powers of q.",
			"comment": [
				"Conjecture: |a(p)| \u003c 2*p^(3/2) for p prime. - _Michael Somos_, Oct 31 2005",
				"Unique cusp form of weight 4 for congruence group Gamma_1(5). - _Michael Somos_, Aug 11 2011",
				"Number 13 of the 74 eta-quotients listed in Table I of Martin (1996)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A030210/b030210.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"M. Koike, \u003ca href=\"http://projecteuclid.org/euclid.nmj/1118787564\"\u003eOn McKay's conjecture\u003c/a\u003e, Nagoya Math. J., 95 (1984), 85-89.",
				"Mathieu Lemire and Kenneth S. Williams, \u003ca href=\"http://people.math.carleton.ca/~williams/papers/pdf/284.pdf\"\u003eEvaluation of two convolution sums involving the sum of divisors function\u003c/a\u003e, Bulletin of the Australian Mathematical Society, Volume 73, Issue 1 February 2006 , pp. 107-115. See function c5 pp. 107-108.",
				"LMFDB, \u003ca href=\"http://www.lmfdb.org/ModularForm/GL2/Q/holomorphic/5/4/0/\"\u003eNewforms of weight 4 on Gamma_0(5)\u003c/a\u003e",
				"Y. Martin, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-96-01743-6\"\u003eMultiplicative eta-quotients\u003c/a\u003e, Trans. Amer. Math. Soc. 348 (1996), no. 12, 4825-4856, see page 4852 Table I.",
				"Michael Somos, \u003ca href=\"/A030203/a030203.txt\"\u003eIndex to Yves Martin's list of 74 multiplicative eta-quotients and their A-numbers\u003c/a\u003e"
			],
			"formula": [
				"Euler transform of period 5 sequence [ -4, -4, -4, -4, -8, ...]. - _Michael Somos_, May 02 2005",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2), A(x^4)) where f(u, v, w) = -v^3 + 8*u*v*w + 16*u*w^2 + u^2*w. - _Michael Somos_, May 02 2005",
				"a(n) is multiplicative with a(5^e) = (-5)^e, a(p^e) = a(p) * a(p^(e-1)) - p^3 * a(p^(e-2)).",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (5 t)) = 25 (t/i)^4 f(t) where q = exp(2 Pi i t). - _Michael Somos_, Aug 11 2011",
				"G.f.: x * (Product_{k\u003e0} (1 - x^k) * (1 - x^(5*k)))^4.",
				"Convolution square of A030205. - _Michael Somos_, Jun 15 2014"
			],
			"example": [
				"G.f. = q - 4*q^2 + 2*q^3 + 8*q^4 - 5*q^5 - 8*q^6 + 6*q^7 - 23*q^9 + 20*q^10 + 32*q^11 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ q (QPochhammer[ q] QPochhammer[ q^5])^4, {q, 0, n}]; (* _Michael Somos_, Aug 11 2011 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c1, 0, n--; A = x^n * O(x); polcoeff( (eta(x + A) * eta(x^5 + A))^4, n))}",
				"(Sage) CuspForms( Gamma1(5), 4, prec = 65).0 # _Michael Somos_, Aug 11 2011",
				"(MAGMA) Basis( CuspForms( Gamma1(5), 4), 65) [1]; /* _Michael Somos_, May 17 2015 */"
			],
			"xref": [
				"Cf. A030205."
			],
			"keyword": "sign,mult",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_.",
			"references": 5,
			"revision": 39,
			"time": "2018-11-22T21:50:11-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}