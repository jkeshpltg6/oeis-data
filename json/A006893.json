{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006893",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6893,
			"id": "M1533",
			"data": "1,2,5,20,230,26795,359026205,64449908476890320,2076895351339769460477611370186680,2156747150208372213435450937462082366919951682912789656986079991220",
			"name": "Smallest number whose representation requires n triangular numbers with greedy algorithm; also number of 1-2 rooted trees of height n.",
			"reference": [
				"M. Abert and P. Diaconis, paper in preparation, 2002.",
				"D. Parisse, The Tower of Hanoi and the Stern-Brocot-Array, Thesis, Munich, 1997.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A006893/b006893.txt\"\u003eTable of n, a(n) for n = 1..13\u003c/a\u003e",
				"Mee Seong Im and Can Ozan Oğuz, \u003ca href=\"https://www.researchgate.net/publication/351823558_Natural_transformations_between_induction_and_restriction_on_iterated_wreath_product_of_symmetric_group_of_order_2\"\u003eNatural transformations between induction and restriction on iterated wreath product of symmetric group of order 2\u003c/a\u003e, (2021).",
				"E. Lemoine, \u003ca href=\"http://gallica.bnf.fr/ark:/12148/bpt6k2011936/f75.image\"\u003eNote sur deux nouvelles décompositions des nombres entiers\u003c/a\u003e, Assoc. française pour l'avancement des sciences. Vol. 29, Tome 2, pp. 72-74, 1900.",
				"Sridhar Narayanan, \u003ca href=\"https://arxiv.org/abs/1712.02507\"\u003eThe Representation Theory of 2-Sylow Subgroups of the Symmetric Group\u003c/a\u003e, arXiv:1712.02507 [math.RT], 2017.",
				"\u003ca href=\"/index/St#Stern\"\u003eIndex entries for sequences related to Stern's sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Ro#rooted\"\u003eIndex entries for sequences related to rooted trees\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A007501(n-1) - 1.",
				"a(n+1) = a(n)*(a(n)+3)/2, a(1)=1.",
				"a(0) = 1, a(n) = sum(i=0..n-1, t(a(i)), where t(n)=n*(n+1)/2. - _Jon Perry_, Feb 14 2004",
				"a(n) ~ 2 * c^(2^n), where c = 1.16007248510653786919452141287945841802404855231102953089... . - _Vaclav Kotesovec_, Dec 17 2014"
			],
			"maple": [
				"A006893 := proc(n) option remember; if n=1 then 1 else A006893(n-1)*(A006893(n-1)+3)/2; fi; end;"
			],
			"mathematica": [
				"RecurrenceTable[{a[1] == 1, a[n] == a[n-1]*(a[n-1] + 3)/2}, a[n], {n, 10}] (* _Vaclav Kotesovec_, Dec 17 2014 *)"
			],
			"program": [
				"(PARI) a=vector(20); a[1]=1; for(n=2, #a, a[n]=a[n-1]*(a[n-1]+3)/2); a \\\\ _Altug Alkan_, Apr 04 2018"
			],
			"xref": [
				"Where records occur in A057945, n \u003e= 1.",
				"Cf. A007501."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Jeffrey Shallit_",
			"ext": [
				"Additional description from _Andreas M. Hinz_ and _Daniele Parisse_"
			],
			"references": 9,
			"revision": 56,
			"time": "2021-09-22T16:58:55-04:00",
			"created": "1994-04-28T03:00:00-04:00"
		}
	]
}