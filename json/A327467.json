{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A327467",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 327467,
			"data": "3,2,1,4,3,2,3,4,5,6,3,4,5,4,5,6,7,4,5,6,7,6,5,6,5,6,7,6,5,8,7,6,7,8,7,6,7,6,7,8,7,6,7,8,7,8,9,8,7,8,9,8,7,8,7,8,9,8,7,8,9,8,9,8,9,10,9,8,9,10,9,8,9,8,9,10,9,8,9,10,9,10,9,10,9,10",
			"name": "a(n) = smallest k such that n can be expressed as a signed sum of the first k primes.",
			"comment": [
				"Smallest k such that n = +- p_1 +- p_2 +- p_3 +- ... +- p_k for a suitable choice of signs, where p_i = i-th prime."
			],
			"reference": [
				"Allan C. Wechsler, Posting to Sequence Fans Mailing List, circa Aug 29 2019."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A327467/b327467.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e (terms 0..1000 from Giovanni Resta)"
			],
			"formula": [
				"a(A007504(n)) = n for n \u003e 0. - _Seiichi Manyama_, Sep 30 2019",
				"Conjecture. Let k be the smallest integer satisfying n\u003c=A007504(k). If n=9 or 16, a(n)=k+3 (so a(9)=6, a(16)=7), else if A007504(k)-n is odd, a(n)=k+1. If A007504(k)-n=2 or 8 or 12, a(n)=k+2, otherwise a(n)=k. - _Kei Fujimoto_, Sep 24 2021"
			],
			"example": [
				"Illustration of initial terms:",
				"0  =   2 + 3 - 5",
				"1  = - 2 + 3",
				"2  =   2",
				"3  = - 2 + 3 - 5 + 7",
				"4  =   2 - 3 + 5",
				"5  =   2 + 3",
				"6  = - 2 + 3 + 5",
				"7  =   2 + 3 - 5 + 7",
				"8  =   2 - 3 + 5 - 7 + 11",
				"9  =   2 - 3 + 5 + 7 + 11 - 13",
				"10 =   2 + 3 + 5",
				"..."
			],
			"mathematica": [
				"(* 1001 terms *) sgn[w_] := Union@ Abs[Total /@ (w # \u0026 /@ Tuples[{1, -1}, Length@w])]; set[n_] := Block[{h = Floor[n/2], p = Prime@ Range@ n, x, y}, x = sgn[Take[p, h]]; y = sgn[Take[p, h - n]]; Union@ Flatten@ Table[{e + f, Abs[e - f]}, {e, x}, {f, y}]]; T = {}; L = 0 Range[1001]; k = 0; While[Length[T] \u003c 1001, k++; s = Select[set[k], # \u003c= 1000 \u0026\u0026 ! MemberQ[T, #] \u0026]; Do[L[[e + 1]] = k, {e, s}]; T = Union[T, s]]; L (* _Giovanni Resta_, Sep 30 2019 *)"
			],
			"xref": [
				"Cf. A007504, A140358."
			],
			"keyword": "nonn",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_, Sep 29 2019",
			"ext": [
				"More terms from _Giovanni Resta_, Sep 30 2019"
			],
			"references": 3,
			"revision": 29,
			"time": "2021-11-11T20:04:54-05:00",
			"created": "2019-09-29T14:59:45-04:00"
		}
	]
}