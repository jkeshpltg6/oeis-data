{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A245500",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 245500,
			"data": "0,1,2,11,21,31,22,41,211,311,221,411,321,421,3111,2211,4111,3211,4211,3311,5211,4311,6211,4221,32111,4411,5221,42111,33111,52111,43111,62111,42211,53111,44111,52211,63111,421111,331111,521111,431111,621111,422111",
			"name": "Concatenation of multiplicities of prime divisors of highly composite numbers A002182(n).",
			"comment": [
				"For prime decomposition of A002182(n) = 2^a * 3^b * 5^c * ..., a(n) = \"abc...\" converted to a decimal number.",
				"In other words, each \"place\" read from left to right represents the n-th prime, starting with 2 at left and increasing to the right. A number in the \"place\" represents the multiplicity of the corresponding prime in A002182(n).",
				"This notation is corrupt when any multiplicity exceeds 9. The smallest instance of this is at n = 221.",
				"Similar to A054841 but multiplicities are in reverse order.",
				"Given that the exponents e(i) (a,b,c... in the above) of the prime factorization are (weakly) decreasing, their concatenation remains unambiguous way beyond n = 221 (first instance where e(1) \u003e= 10) and even beyond n = 8869 (first instance where e(2) \u003e= 10). Only when e2 \u003e= 10 + e(3) for the first time, in principle the first digit of e(2) could be mistaken for the last digit of e(1); yet it is unlikely if not impossible that e(2) \u003c 10 and e(1) \u003e 100. So the first ambiguous decomposition would require concat(e(1),e(2),e(3)) = concat(a',b',c') with a' \u003e b' \u003e= e(3), thus e(2) significantly larger than 10 + e(3) and e(1) much larger than 100. - _M. F. Hasler_, Jan 03 2020"
			],
			"link": [
				"M. F. Hasler, \u003ca href=\"/A245500/b245500.txt\"\u003eTable of n, a(n) for n = 1..5000\u003c/a\u003e (first 220 terms from _Michael De Vlieger_), Jan 02 2020",
				"A. Flammenkamp, \u003ca href=\"http://wwwhomes.uni-bielefeld.de/achim/highly.html\"\u003eHighly composite numbers\u003c/a\u003e.",
				"A. Flammenkamp, \u003ca href=\"http://wwwhomes.uni-bielefeld.de/achim/highly.txt\"\u003eList of the first 1200 highly composite numbers\u003c/a\u003e.",
				"D. B. Siano and J. D. Siano, \"Pwrs of primes\" notation from \u003ca href=\"http://wwwhomes.uni-bielefeld.de/achim/julianmanuscript3.pdf\"\u003eAn Algorithm for Generating Highly Composite Numbers\u003c/a\u003e"
			],
			"example": [
				"A002182(4)   = 12 = 2^2 * 3^1, thus a(4) = 21;",
				"A002182(17)  = 2520 = 2^3 * 3^2 * 5^1 * 7^1, thus a(17) = 3211;",
				"A002182(220) = 2^10 * 3^4 * 5^3 * 7^2 * 11 * ... * 53 (skipping no primes), thus a(220) cannot be represented using a single decimal place for the multiplicity 10."
			],
			"mathematica": [
				"encodePrimeSignature[n_Integer] :=",
				"  Catch[FromDigits[Reverse[IntegerDigits[Apply[Plus,",
				"       Which[n == 0, Throw[\"undefined\"],",
				"          n == 1, 0,",
				"          Max[Last /@ FactorInteger @ n ] \u003e 9, Throw[\"overflow\"],",
				"          True, Power[10, PrimePi[Abs[#]] - 1]] \u0026 /@",
				"       Flatten[ConstantArray @@@ FactorInteger[n]] ]]]]];",
				"lst = FoldList[Max, 1, Table[DivisorSigma[0, n], {n, 2, 100000000}]];",
				"Map[encodePrimeSignature, Flatten[Position[lst, #, 1, 1] \u0026 /@ Union[lst]]]"
			],
			"program": [
				"(PARI) apply( A245500(n)=fromdigits(factor(A002182(n))[,2]~), [1..99]) \\\\ For n \u003e= 8869, fromdigits must be replaced by (s-\u003eif(s,eval(concat([Str(e)|e\u003c-s])))). - _M. F. Hasler_, Jan 03 2020"
			],
			"xref": [
				"Cf. A002182, A054841."
			],
			"keyword": "nonn,base",
			"offset": "1,3",
			"author": "_Michael De Vlieger_, Jul 24 2014",
			"ext": [
				"Offset corrected to 1 by _M. F. Hasler_, Jan 03 2020"
			],
			"references": 1,
			"revision": 29,
			"time": "2020-06-04T18:28:31-04:00",
			"created": "2014-07-27T01:19:33-04:00"
		}
	]
}