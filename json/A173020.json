{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A173020",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 173020,
			"data": "1,1,3,1,9,12,1,18,66,55,1,30,210,455,273,1,45,510,2040,3060,1428,1,63,1050,6650,17955,20349,7752,1,84,1932,17710,74382,148764,134596,43263,1,108,3276,40950,245700,753480,1184040,888030,246675,1,135,5220,85260,690606,2992626,7125300,9161100,5852925,1430715",
			"name": "Triangle of Generalized Runyon numbers R_{n,k}^(3) read by rows.",
			"comment": [
				"The Runyon numbers R_{n,k}^(1) are A001263, R_{n,k}^(2) are A108767.",
				"Row sums are in A002293."
			],
			"reference": [
				"Chunwei Song, The Generalized Schroeder Theory, El. J. Combin. 12 (2005) #R53"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A173020/b173020.txt\"\u003eRows n = 1..100 of the triangle, flattened\u003c/a\u003e",
				"J.-C. Novelli, J.-Y. Thibon, \u003ca href=\"http://arxiv.org/abs/1403.5962\"\u003eHopf Algebras of m-permutations,(m+1)-ary trees, and m-parking functions\u003c/a\u003e, arXiv preprint arXiv:1403.5962, 2014. See Fig. 6."
			],
			"formula": [
				"T(n, k) = R(n,k,3) with R(n,k,m)= binomial(n,k)*binomial(m*n,k-1)/n, 1\u003c=k\u003c=n.",
				"T(n, n) = A001764(n).",
				"T(n, n-1) = A003408(n-2).",
				"T(n, 2) = A045943(n-1).",
				"T(n, 3) = n*(n-1)*(n-2)*(3*n-1)/4 = 3*A052149(n-1).",
				"O.g.f. is series reversion with respect to x of x/((1+x)*(1+x*u)^3). - _Peter Bala_, Sep 12 2012",
				"Sum_{k=1..n} T(n, k, 3) = binomial(4*n, n)/(3*n+1) = A002293(n). - _G. C. Greubel_, Feb 20 2021"
			],
			"example": [
				"The triangle starts in row n=1 as",
				"  1;",
				"  1,  3;",
				"  1,  9,   12;",
				"  1, 18,   66,    55;",
				"  1, 30,  210,   455,   273;",
				"  1, 45,  510,  2040,  3060,   1428;",
				"  1, 63, 1050,  6650, 17955,  20349,   7752;",
				"  1, 84, 1932, 17710, 74382, 148764, 134596, 43263;"
			],
			"mathematica": [
				"T[n_, k_, m_]:= Binomial[n, k]*Binomial[m*n, k-1]/n;",
				"Table[T[n, k, 3], {n, 12}, {k, n}]//Flatten (* _G. C. Greubel_, Feb 20 2021 *)"
			],
			"program": [
				"(Sage)",
				"def A173020(n,k,m): return binomial(n,k)*binomial(m*n,k-1)/n",
				"flatten([[A173020(n,k,3) for k in (1..n)] for n in (1..12)]) # _G. C. Greubel_, Feb 20 2021",
				"(Magma)",
				"A173020:= func\u003c n,k,m | Binomial(n,k)*Binomial(m*n,k-1)/n \u003e;",
				"[A173020(n,k,3): k in [1..n], n in [1..12]]; // _G. C. Greubel_, Feb 20 2021"
			],
			"xref": [
				"Cf. A010054 (m=0), A001263 (m=1), A108767 (m=2), this sequence (m=3).",
				"Cf. A001764, A002293, A003408, A045943, A052149."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "1,3",
			"author": "_R. J. Mathar_, Nov 08 2010",
			"references": 2,
			"revision": 15,
			"time": "2021-02-20T23:12:46-05:00",
			"created": "2010-11-10T03:00:00-05:00"
		}
	]
}