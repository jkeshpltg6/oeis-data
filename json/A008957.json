{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008957",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8957,
			"data": "1,1,1,1,5,1,1,14,21,1,1,30,147,85,1,1,55,627,1408,341,1,1,91,2002,11440,13013,1365,1,1,140,5278,61490,196053,118482,5461,1,1,204,12138,251498,1733303,3255330,1071799,21845,1,1,285,25194,846260,10787231",
			"name": "Triangle of central factorial numbers T(2*n,2*n-2*k), k \u003e= 0, n \u003e= 1 (in Riordan's notation).",
			"comment": [
				"D. E. Knuth [1992] on page 10 gives the formula Sum n^(2m-1) = Sum_{k=1..m} (2k-1)! T(2m,2k) binomial(n+k, 2k) where T(m, k) is the central factorial number of the second kind. - _Michael Somos_, May 08 2018"
			],
			"reference": [
				"J. Riordan, Combinatorial Identities, Wiley, 1968, p. 217, Table 6.2(a).",
				"R. P. Stanley, Enumerative Combinatorics, Cambridge, Vol. 2, 1999; see Problem 5.8."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A008957/b008957.txt\"\u003eRows n = 1..100 of triangle, flattened\u003c/a\u003e",
				"F. Alayont and N. Krzywonos, \u003ca href=\"http://faculty.gvsu.edu/alayontf/notes/rook_polynomials_higher_dimensions_preprint.pdf\"\u003eRook Polynomials in Three and Higher Dimensions\u003c/a\u003e, 2012. [From _N. J. A. Sloane_, Jan 02 2013]",
				"D. Dumont, \u003ca href=\"http://dx.doi.org/10.1215/S0012-7094-74-04134-9\"\u003eInterprétations combinatoires des nombres de Genocchi\u003c/a\u003e, Duke Math. J., 41 (1974), 305-318.",
				"D. E. Knuth, \u003ca href=\"https://arxiv.org/abs/math/9207222\"\u003eJohann Faulhaber and Sums of Powers\u003c/a\u003e, arXiv:math/9207222, Jul 1992. See bottom of page 10. [From _Michael Somos_, May 08 2018]"
			],
			"formula": [
				"From _Michael Somos_, May 08 2018: (Start)",
				"T(n, k) = T(n-1, k-1) + k^2 * T(n-1, k), where T(n, n) = T(n, 1) = 1.",
				"E.g.f.: x^2 * cosh(sinh(y*x/2) / (x/2)) - 1) = (1*x^2)*y^2/2! + (1*x^2 + 1*x^4)*y^4/4! +(1*x^2 + 5*x^4 + x^6)*y^6/6! + (1*x^2 + 14*x^4 + 21*x^6 + 1*x^8)*y^8/8! + ... (End)"
			],
			"example": [
				"The triangle starts:",
				"1;",
				"1,  1;",
				"1,  5,    1;",
				"1, 14,   21,     1;",
				"1, 30,  147,    85,     1;",
				"1, 55,  627,  1408,   341,    1;",
				"1, 91, 2002, 11440, 13013, 1365, 1;"
			],
			"maple": [
				"A036969 := proc(n,k) local j; 2*add(j^(2*n)*(-1)^(k-j)/((k-j)!*(k+j)!),j=1..k); end; # Gives rows of triangle in reversed order"
			],
			"mathematica": [
				"t[n_, n_] = t[n_, 1] = 1;",
				"t[n_, k_] := t[n-1, k-1] + k^2 t[n-1, k];",
				"Flatten[Table[t[n, k], {n, 1, 10}, {k, n, 1, -1}]][[1 ;; 50]] (* _Jean-François Alcover_, Jun 16 2011 *)"
			],
			"program": [
				"(Haskell)",
				"a008957 n k = a008957_tabl !! (n-1) (k-1)",
				"a008957_row n = a008957_tabl !! (n-1)",
				"a008957_tabl = map reverse a036969_tabl",
				"-- _Reinhard Zumkeller_, Feb 18 2013",
				"(PARI) {T(n, k) = if( n\u003c1 || k\u003en, 0, n==k || k==1, 1, T(n-1, k-1) + k^2 * T(n-1, k))}; \\\\ _Michael Somos_, May 08 2018",
				"(Sage)",
				"def A008957(n, k):",
				"    m = n - k",
				"    return 2*sum((-1)^(j+m)*(j+1)^(2*n)/(factorial(j+m+2)*factorial(m-j)) for j in (0..m))",
				"for n in (1..7): print([A008957(n, k) for k in (1..n)]) # _Peter Luschny_, May 10 2018"
			],
			"xref": [
				"Row reversed version of A036969. Cf. A008955."
			],
			"keyword": "nonn,nice,easy,tabl",
			"offset": "1,5",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Vladeta Jovovic_, Apr 16 2000"
			],
			"references": 6,
			"revision": 39,
			"time": "2018-05-10T08:45:06-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}