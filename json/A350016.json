{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A350016",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 350016,
			"data": "1,1,2,5,1,17,1,6,74,11,15,20,394,56,60,120,90,2484,407,525,490,630,504,18108,3235,4725,2240,4620,4032,3360,149904,29143,40509,27440,26460,33264,30240,25920,1389456,291394,398790,319760,163800,302400,277200,259200,226800",
			"name": "Irregular triangle read by rows: T(n,k) is the number of n-permutations whose third-shortest cycle has length exactly k; n \u003e= 0, 0 \u003c= k \u003c= max(0,n-2).",
			"comment": [
				"If the permutation has no third cycle, then its third-longest cycle is defined to have length 0."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A350016/b350016.txt\"\u003eRows n = 0..142, flattened\u003c/a\u003e"
			],
			"formula": [
				"Sum_{k=0..n-2} k * T(n,k) = A332907(n) for n \u003e= 3. - _Alois P. Heinz_, Dec 12 2021"
			],
			"example": [
				"Triangle begins:",
				"[0]      1;",
				"[1]      1;",
				"[2]      2;",
				"[3]      5,     1;",
				"[4]     17,     1,     6;",
				"[5]     74,    11,    15,    20;",
				"[6]    394,    56,    60,   120,    90;",
				"[7]   2484,   407,   525,   490,   630,   504;",
				"[8]  18108,  3235,  4725,  2240,  4620,  4032,  3360;",
				"[9] 149904, 29143, 40509, 27440, 26460, 33264, 30240, 25920;",
				"..."
			],
			"maple": [
				"m:= infinity:",
				"b:= proc(n, l) option remember; `if`(n=0, x^`if`(l[3]=m,",
				"      0, l[3]), add(b(n-j, sort([l[], j])[1..3])",
				"               *binomial(n-1, j-1)*(j-1)!, j=1..n))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=0..degree(p)))(b(n, [m$3])):",
				"seq(T(n), n=0..10);  # _Alois P. Heinz_, Dec 11 2021"
			],
			"mathematica": [
				"m = Infinity;",
				"b[n_, l_] := b[n, l] = If[n == 0, x^If[l[[3]] == m, 0, l[[3]]], Sum[b[n-j, Sort[Append[l, j]][[1;;3]]]*Binomial[n - 1, j - 1]*(j - 1)!, {j, 1, n}]];",
				"T[n_] := With[{p = b[n, {m, m, m}]}, Table[Coefficient[p, x, i], {i, 0, Exponent[p, x]}]];",
				"Table[T[n], {n, 0, 10}] // Flatten (* _Jean-François Alcover_, Dec 28 2021, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Column 0 gives 1 together with A000774.",
				"Column 1 gives the column 3 of A208956.",
				"Row sums give A000142.",
				"Cf. A332907."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_Steven Finch_, Dec 08 2021",
			"references": 2,
			"revision": 28,
			"time": "2021-12-28T14:12:08-05:00",
			"created": "2021-12-12T11:47:58-05:00"
		}
	]
}