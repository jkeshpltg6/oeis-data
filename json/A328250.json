{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A328250",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 328250,
			"data": "4,8,1,12,2,9,16,3,18,50,20,5,25,99,306,24,6,45,125,549,5831,27,7,49,207,1611,6849,20230,28,10,63,343,2662,14225,33026,52283,32,11,75,375,2842,16299,47107,225998,286891,36,13,90,531,2891,19431,49806,1336047,1292750,10820131,40,14,98,686,4575,21231,117649,1422275,2886982,21628098,38452606",
			"name": "Square array A(n,k) read by descending antidiagonals where A(n,k) is the k-th solution x to A328248(x) = n-1.",
			"comment": [
				"Row 1 of the array is reserved for numbers for which no squarefree number is ever reached, and from then on, each row n \u003e 1 of array gives in ascending order all natural numbers that require n-2 iterations of arithmetic derivative (A003415) to reach a squarefree number. Squarefree numbers (A005117) thus occupy the row 2, as they require no iterations."
			],
			"link": [
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"example": [
				"The upper left corner of the array:",
				"         4,        8,       12,       16,       20,       24,       27,       28,",
				"         1,        2,        3,        5,        6,        7,       10,       11,",
				"         9,       18,       25,       45,       49,       63,       75,       90,",
				"        50,       99,      125,      207,      343,      375,      531,      686,",
				"       306,      549,     1611,     2662,     2842,     2891,     4575,     4802,",
				"      5831,     6849,    14225,    16299,    19431,    21231,    22638,    24010,",
				"     20230,    33026,    47107,    49806,   117649,   121671,   145386,   162707,",
				"     52283,   225998,  1336047,  1422275,  1500759,  1576899,  2309503,  3023398,",
				"    286891,  1292750,  2886982,  3137526,  6882453,  8703459, 15358457, 16777114,",
				"  10820131, 21628098, 23934105, 24332763, 46295435, 51320698, 52320191, 56199375,",
				"  38452606, ...",
				"  ..."
			],
			"program": [
				"(PARI)",
				"up_to = 45; \\\\ 10585 = binomial(145+1,2)",
				"A003415checked(n) = if(n\u003c=1, 0, my(f=factor(n), s=0); for(i=1, #f~, if(f[i,2]\u003e=f[i,1],return(0), s += f[i, 2]/f[i, 1])); (n*s));",
				"A328248(n) = { my(k=1); while(n \u0026\u0026 !issquarefree(n), k++; n = A003415checked(n)); (!!n*k); };",
				"memoA328250sq = Map();",
				"A328250sq(n, k) = { my(v=0); if(!mapisdefined(memoA328250sq,[n,k-1],\u0026v),if(1==k, v=0, v = A328250sq(n, k-1))); for(i=1+v,oo,if((1+A328248(i))==n,mapput(memoA328250sq,[n,k],i); return(i))); };",
				"A328250list(up_to) = { my(v = vector(up_to), i=0); for(a=1,oo, for(col=1,a, i++; if(i \u003e up_to, return(v)); v[i] = A328250sq(col,(a-(col-1))))); (v); };",
				"v328250 = A328250list(up_to);",
				"A328250(n) = v328250[n];"
			],
			"xref": [
				"Cf. A003415, A328248.",
				"Column 1: A328302.",
				"Rows 1 - 4 are: A328251, A005117, A328252, A328253."
			],
			"keyword": "nonn,tabl",
			"offset": "1,1",
			"author": "_Antti Karttunen_, Oct 12 2019",
			"references": 6,
			"revision": 17,
			"time": "2019-10-13T18:08:42-04:00",
			"created": "2019-10-13T18:08:42-04:00"
		}
	]
}