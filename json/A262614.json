{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A262614",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 262614,
			"data": "1,-2,2,-5,9,-12,16,-23,36,-47,60,-84,115,-149,188,-245,321,-406,505,-641,813,-1007,1237,-1533,1901,-2321,2816,-3437,4191,-5055,6068,-7307,8792,-10501,12490,-14886,17720,-20975,24755,-29236,34492,-40522,47486,-55666",
			"name": "Expansion of phi(-x^3) * f(-x, -x^5) / psi(x) in powers of x where phi(), psi(), f(, ) are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A262614/b262614.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of f(-x^3)^3 / (f(x, x^2) * psi(x)) in powers of x where psi(), f(, ) are Ramanujan theta functions.",
				"Expansion of q^(-5/24) * eta(q)^2 * eta(q^3) * eta(q^6) / eta(q^2)^3 in powers of q.",
				"Euler transform of period 6 sequence [ -2, 1, -3, 1, -2, -1, ...].",
				"a(n) = A053269(3*n + 1).",
				"a(n) ~ (-1)^n * exp(sqrt(n/2)*Pi) / (6*sqrt(n)). - _Vaclav Kotesovec_, Apr 17 2016"
			],
			"example": [
				"G.f. = 1 - 2*x + 2*x^2 - 5*x^3 + 9*x^4 - 12*x^5 + 16*x^6 - 23*x^7 +",
				"G.f. = q^5 - 2*q^29 + 2*q^53 - 5*q^77 + 9*q^101 - 12*q^125 + 16*q^149 - 23*q^173 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ 2 x^(1/8) QPochhammer[ x^3]^3 QPochhammer[ x, x^2] / (EllipticTheta[ 4, 0, x^3] EllipticTheta[ 2, 0, x^(1/2)]), {x, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A)^2 * eta(x^3 + A) * eta(x^6 + A) / eta(x^2 + A)^3, n))};"
			],
			"xref": [
				"Cf. A053269."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Apr 17 2016",
			"references": 3,
			"revision": 27,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2016-04-17T13:21:32-04:00"
		}
	]
}