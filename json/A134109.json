{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A134109",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 134109,
			"data": "1,1,0,2,0,0,2,1,0,0,2,0,1,0,1,0,0,1,1,1,0,0,1,0,1,2,1,3,0,0,0,0,0,0,1,0,0,0,3,1,0,0,0,1,1,0,3,2,1,0,0,0,2,1,2,1,0,0,0,2,1,0,2,1,0,0,1,0,0,0,1,1,0,1,0,2,0,0,1,0,1,0,1,0,0,0,1,0,1,0,0,0,0,0,1,0,0,0,0,3,0,0,0,3",
			"name": "Number of integral solutions with nonnegative y to Mordell's equation y^2 = x^3 - n.",
			"comment": [
				"a(n) = A081120(n)/2 if A081120(n) is even, (A081120(n)+1)/2 if A081120(n) is odd (i.e. if n is a cubic number).",
				"Comment from T. D. Noe, Oct 12 2007: In sequences A134108 and A134109 (this entry) dealing with the equation y^2 = x^3 + n, one could note that these are Mordell equations. Here are some related sequences: A054504, A081119, A081120, A081121. The link \"Integer points on Mordell curves\" has data on 20000 values of n. A134108 and A134109 count only solutions with y \u003e= 0 and can be derived from A081119 and A081120."
			],
			"link": [
				"Jean-François Alcover, \u003ca href=\"/A134109/b134109.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"J. Gebel, \u003ca href=\"/A001014/a001014.txt\"\u003eInteger points on Mordell curves\u003c/a\u003e [Cached copy, after the original web site tnt.math.se.tmu.ac.jp was shut down in 2017]",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/MordellCurve.html\"\u003eMordell Curve\u003c/a\u003e"
			],
			"example": [
				"y^2 = x^3 - 4 has solutions (y, x) = (2, 2) and (11, 5), hence a(4) = 2.",
				"y^2 = x^3 - 5 has no solutions, hence a(5) = 0.",
				"y^2 = x^3 - 8 has solution (y, x) = (0, 2), hence a(8) = 1.",
				"y^2 = x^3 - 207 has 7 solutions (see A134106, A134107), hence a(207) = 7."
			],
			"mathematica": [
				"A081120 = Cases[Import[\"https://oeis.org/A081120/b081120.txt\", \"Table\"], {_, _}][[All, 2]];",
				"a[n_] := With[{an = A081120[[n]]}, If[EvenQ[an], an/2, (an+1)/2]];",
				"a /@ Range[10000] (* _Jean-François Alcover_, Nov 28 2019 *)"
			],
			"program": [
				"(MAGMA) [ #{ Abs(p[2]) : p in IntegralPoints(EllipticCurve([0, -n])) }: n in [1..104] ];"
			],
			"xref": [
				"Cf. A081120, A134106, A134107, A134108."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Klaus Brockhaus_, Oct 08 2007, Oct 14 2007",
			"references": 4,
			"revision": 10,
			"time": "2019-11-28T07:39:58-05:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}