{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A335447",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 335447,
			"data": "0,0,0,0,0,1,0,0,0,1,0,2,0,1,1,0,0,2,0,2,1,1,0,3,0,1,0,2,0,5,0,0,1,1,1,5,0,1,1,3,0,5,0,2,2,1,0,4,0,2,1,2,0,3,1,3,1,1,0,11,0,1,2,0,1,5,0,2,1,5,0,9,0,1,2,2,1,5,0,4,0,1,0,11,1,1",
			"name": "Number of (1,2)-matching permutations of the prime indices of n.",
			"comment": [
				"Depends only on sorted prime signature (A118914).",
				"Also the number of (2,1)-matching permutations of the prime indices of n.",
				"A prime index of n is a number m such that prime(m) divides n. The multiset of prime indices of n is row n of A112798.",
				"We define a pattern to be a finite sequence covering an initial interval of positive integers. Patterns are counted by A000670. A sequence S is said to match a pattern P if there is a not necessarily contiguous subsequence of S whose parts have the same relative order as P. For example, (3,1,1,3) matches (1,1,2), (2,1,1), and (2,1,2), but avoids (1,2,1), (1,2,2), and (2,2,1)."
			],
			"link": [
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Permutation_pattern\"\u003ePermutation pattern\u003c/a\u003e",
				"Gus Wiseman, \u003ca href=\"/A102726/a102726.txt\"\u003eSequences counting and ranking compositions by the patterns they match or avoid.\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A008480(n) - 1."
			],
			"example": [
				"The a(n) permutations for n = 6, 12, 24, 48, 30, 72, 60:",
				"  (12)  (112)  (1112)  (11112)  (123)  (11122)  (1123)",
				"        (121)  (1121)  (11121)  (132)  (11212)  (1132)",
				"               (1211)  (11211)  (213)  (11221)  (1213)",
				"                       (12111)  (231)  (12112)  (1231)",
				"                                (312)  (12121)  (1312)",
				"                                       (12211)  (1321)",
				"                                       (21112)  (2113)",
				"                                       (21121)  (2131)",
				"                                       (21211)  (2311)",
				"                                                (3112)",
				"                                                (3121)"
			],
			"mathematica": [
				"primeMS[n_]:=If[n==1,{},Flatten[Cases[FactorInteger[n],{p_,k_}:\u003eTable[PrimePi[p],{k}]]]];",
				"Table[Length[Select[Permutations[primeMS[n]],!GreaterEqual@@#\u0026]],{n,100}]"
			],
			"xref": [
				"The avoiding version is A000012.",
				"Patterns are counted by A000670.",
				"Positions of zeros are A000961.",
				"(1,2)-matching patterns are counted by A002051.",
				"Permutations of prime indices are counted by A008480.",
				"(1,2)-matching compositions are counted by A056823.",
				"STC-numbers of permutations of prime indices are A333221.",
				"Patterns matched by standard compositions are counted by A335454.",
				"(1,2,1) or (2,1,2)-matching permutations of prime indices are A335460.",
				"(1,2,1) and (2,1,2)-matching permutations of prime indices are A335462.",
				"Dimensions of downsets of standard compositions are A335465.",
				"(1,2)-matching compositions are ranked by A335485.",
				"Cf. A056239, A056986, A112798, A181796, A333175, A335451, A335452, A335463, A333175."
			],
			"keyword": "nonn",
			"offset": "1,12",
			"author": "_Gus Wiseman_, Jun 14 2020",
			"references": 3,
			"revision": 11,
			"time": "2020-06-29T22:21:07-04:00",
			"created": "2020-06-14T22:46:21-04:00"
		}
	]
}