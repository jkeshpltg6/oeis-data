{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A010061",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 10061,
			"data": "1,4,6,13,15,18,21,23,30,32,37,39,46,48,51,54,56,63,71,78,80,83,86,88,95,97,102,104,111,113,116,119,121,128,130,133,135,142,144,147,150,152,159,161,166,168,175,177,180,183,185,192,200,207,209,212,215,217",
			"name": "Binary self or Colombian numbers: numbers that cannot be expressed as the sum of distinct terms of the form 2^k+1 (k\u003e=0), or equivalently, numbers not of form m + sum of binary digits of m.",
			"comment": [
				"No two consecutive values appear in this sequence (see Links). - _Griffin N. Macris_, May 31 2020",
				"The asymptotic density of this sequence is (1/8) * (2 - Sum_{n\u003e=1} 1/2^a(n))^2 = 0.252660... (A242403). - _Amiram Eldar_, Nov 28 2020"
			],
			"reference": [
				"Steven R. Finch, Mathematical Constants, Cambridge, 2003, Section 2.24, pp. 179-180.",
				"József Sándor and Borislav Crstici, Handbook of Number theory II, Kluwer Academic Publishers, 2004, Chapter 4, pp. 384-386."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A010061/b010061.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Max A. Alekseyev, Donovan Johnson and N. J. A. Sloane, \u003ca href=\"http://neilsloane.com/doc/colombian12302021.pdf\"\u003eOn Kaprekar's Junction Numbers\u003c/a\u003e, Preprint, 2021.",
				"Griffin N. Macris, \u003ca href=\"/A010061/a010061.txt\"\u003eProof that no consecutive self numbers exist\u003c/a\u003e, 2020.",
				"\u003ca href=\"/index/Coi#Colombian\"\u003eIndex entries for Colombian or self numbers and related sequences\u003c/a\u003e"
			],
			"maple": [
				"# For Maple code see A230091. - _N. J. A. Sloane_, Oct 10 2013"
			],
			"mathematica": [
				"Table[n + Total[IntegerDigits[n, 2]], {n, 0, 300}] // Complement[Range[Last[#]], #]\u0026 (* _Jean-François Alcover_, Sep 03 2013 *)"
			],
			"program": [
				"(Scheme, with _Antti Karttunen_'s IntSeq-library)",
				"(define A010061 (ZERO-POS 1 0 A228085))",
				"(Haskell)",
				"a010061 n = a010061_list !! (n-1)",
				"a010061_list = filter ((== 0) . a228085) [1..]",
				"-- _Reinhard Zumkeller_, Oct 13 2013",
				"/* PARI: Gen(n, b) returns a list of the generators of n in base b. Written by _Max Alekseyev_ (see Alekseyev et al., 2021).",
				"For example, Gen(101, 10) returns [91, 101]. - N. J. A. Sloane, Jan 02 2022 */",
				"{ Gen(u, b=10) = my(d, m, k);",
				"  if(u\u003c0 || u==1, return([]); );",
				"  if(u==0, return([0]); );",
				"  d = #digits(u, b)-1;",
				"  m = u\\b^d;",
				"  while( sumdigits(m, b) \u003e u - m*b^d,",
				"    m--;",
				"    if(m==0, m=b-1; d--; );",
				"  );",
				"  k = u - m*b^d - sumdigits(m, b);",
				"  vecsort( concat( apply(x-\u003ex+m*b^d, Gen(k, b)),",
				"                   apply(x-\u003em*b^d-1-x, Gen((b-1)*d-k-2, b)) ) );",
				"}"
			],
			"xref": [
				"Complement of A228082, or equally, numbers which do not occur in A092391. Gives the positions of zeros (those occurring after a(0)) in A228085-A228087 and positions of ones in A227643. Leftmost column of A228083. Base-10 analog: A003052.",
				"Cf. A010062, A055938, A230091, A230092, A230058, A242403.",
				"Cf. A228088, A227915, A232228."
			],
			"keyword": "nonn,base,changed",
			"offset": "1,2",
			"author": "_Leonid Broukhis_",
			"ext": [
				"More terms from _Antti Karttunen_, Aug 17 2013",
				"Better definition from _Matthew C. Russell_, Oct 08 2013"
			],
			"references": 42,
			"revision": 77,
			"time": "2022-01-08T15:03:02-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}