{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A177885",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 177885,
			"data": "1,1,-1,4,-27,256,-3125,46656,-823543,16777216,-387420489,10000000000,-285311670611,8916100448256,-302875106592253,11112006825558016,-437893890380859375,18446744073709551616,-827240261886336764177",
			"name": "a(n) = (1-n)^(n-1).",
			"comment": [
				"A signed version of A000312.",
				"LeClair gives an approximation z(n) for the location of the n-th nontrivial zero of the Riemann zeta function on the critical line, which can be expressed in terms of the exponential generating function of this sequence A(x) = x/LambertW(x) as follows: z(n) = 1/2 + 2*Pi*exp(1)*A((n - 11/8)/exp(1))*i. For example, working to 1 decimal place, z(1) = 1/2 + 14.5*i (the first nontrivial zero is at 1/2 + 14.1*i), z(10) = 1/2 + 50.2*i (the tenth nontrivial zero is at 1/2 + 49.8*i) and z(100) = 1/2 + 236*i (the hundredth nontrivial zero is at 1/2 + 236.5*i). [_Peter Bala_, Jun 12 2013]"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A177885/b177885.txt\"\u003eTable of n, a(n) for n = 0..140\u003c/a\u003e",
				"Vladimir Kruchinin, D. V. Kruchinin, \u003ca href=\"http://arxiv.org/abs/1103.2582\"\u003eComposita and their properties\u003c/a\u003e, arXiv:1103.2582",
				"A. LeClair, \u003ca href=\"http://arxiv.org/abs/1305.2613\"\u003eAn electrostatic depiction of the validity of the Riemann Hypothesis and a formula for the N-th zero at large N\u003c/a\u003e, arXiv:1305.2613v4[math-ph]"
			],
			"formula": [
				"E.g.f. satisfies A(x) = exp(x/A(x)).",
				"E.g.f. A(x) = x/LambertW(x) = exp(LambertW(x)) = 1 + x - x^2/2! + 4*x^3/3! - 27*x^4/4! + .... - _Peter Bala_, Jun 12 2013",
				"E.g.f.: 1 + Series_Reversion( (1+x)*log(1+x) ). - _Paul D. Hanna_, Aug 24 2016",
				"E.g.f.: 1 + Series_Reversion( x + Sum_{n\u003e=2} (-x)^n/(n*(n-1)) ). - _Paul D. Hanna_, Aug 24 2016",
				"a(n) ~ (-1)^(n+1) * exp(-1) * n^(n-1). - _Vaclav Kotesovec_, Sep 22 2016"
			],
			"example": [
				"From _Paul D. Hanna_, Aug 24 2016: (Start)",
				"E.g.f.: A(x) = 1 + x - x^2/2! + 4*x^3/3! - 27*x^4/4! + 256*x^5/5! - 3125*x^6/6! + 46656*x^7/7! - 823543*x^8/8! +...+ (1-n)^(n-1)*x^n/n! +...",
				"Related series.",
				"Series_Reversion(A(x) - 1) = x + x^2/2 - x^3/6 + x^4/12 - x^5/20 + x^6/30 - x^7/42 + x^8/56 - x^9/72 + x^10/90 +...+ (-x)^n/(n*(n-1)) +... (End)"
			],
			"mathematica": [
				"Join[{1,1}, Table[(1-n)^(n-1), {n, 2, 20}]] (* _Harvey P. Dale_, Aug 10 2012 *)",
				"nn = 18; Range[0, nn]! CoefficientList[ Series[ Exp[ ProductLog[ x]], {x, 0, nn}], x] (* _Robert G. Wilson v_, Aug 23 2012 *)"
			],
			"program": [
				"(MAGMA) [(1-n)^(n-1): n in [0..30]]; // _Vincenzo Librandi_, May 15 2011",
				"(PARI) a(n)=(1-n)^(n-1) \\\\ _Charles R Greathouse IV_, May 15 2013",
				"(PARI) {a(n) = my(A = 1 + serreverse( x + sum(m=2,n+2, (-x)^m/(m*(m-1)) +x^2*O(x^n)))); n!*polcoeff(A,n)}",
				"for(n=0,30,print1(a(n),\", \")) \\\\ _Paul D. Hanna_, Aug 24 2016"
			],
			"xref": [
				"Cf. A000312."
			],
			"keyword": "sign,easy",
			"offset": "0,4",
			"author": "_Vladimir Kruchinin_, Dec 28 2010",
			"references": 12,
			"revision": 56,
			"time": "2016-12-10T19:17:33-05:00",
			"created": "2010-11-12T14:26:18-05:00"
		}
	]
}