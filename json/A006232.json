{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006232",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6232,
			"id": "M5067",
			"data": "1,1,-1,1,-19,9,-863,1375,-33953,57281,-3250433,1891755,-13695779093,24466579093,-132282840127,240208245823,-111956703448001,4573423873125,-30342376302478019,56310194579604163",
			"name": "Numerators of Cauchy numbers of first type.",
			"comment": [
				"The corresponding denominators are given in A006233.",
				"-a(n+1), n\u003e=0, also numerators from e.g.f. 1/x-1/log(1+x), with denominators A075178(n). |a(n+1)|, n\u003e=0, numerators from e.g.f. 1/x+1/log(1-x) with denominators A075178(n). For formula of unsigned a(n) see A075178.",
				"The signed rationals a(n)/A006233(n) provide the a-sequence for the Stirling2 Sheffer matrix A048993. See the W. Lang link concerning Sheffer a- and z-sequences.",
				"Cauchy numbers of the first type are also called Bernoulli numbers of the second kind.",
				"Named after the French mathematician, engineer and physicist Augustin-Louis Cauchy (1789-1857). - _Amiram Eldar_, Jun 17 2021"
			],
			"reference": [
				"Louis Comtet, Advanced Combinatorics, Reidel, 1974, p. 294.",
				"Harold Jeffreys and B. S. Jeffreys, Methods of Mathematical Physics, Cambridge, 1946, p. 259.",
				"L. B. W. Jolley, Summation of Series, Chapman and Hall, London, 1925, pp. 14-15 (formula 70).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A006232/b006232.txt\"\u003eTable of n, a(n) for n=0..100\u003c/a\u003e",
				"Arnold Adelberg, \u003ca href=\"http://dx.doi.org/10.1006/jnth.1998.2278\"\u003e2-adic congruences of Norland numbers and of Bernoulli numbers of the second kind\u003c/a\u003e, J. Number Theory, Vol. 73, No. 1 (1998), pp. 47-58.",
				"I. S. Gradsteyn and I. M. Ryzhik, \u003ca href=\"http://mathtable.com/gr/index.html\"\u003eTable of integrals, series and products\u003c/a\u003e, (1980), page 2 (formula 0.131).",
				"Wolfdieter Lang, \u003ca href=\"/A006232/a006232.pdf\"\u003e Sheffer a- and z-sequences\u003c/a\u003e.",
				"Wolfdieter Lang, \u003ca href=\"https://arxiv.org/abs/1708.01421\"\u003eOn Generating functions of Diagonals Sequences of Sheffer and Riordan Number Triangles\u003c/a\u003e, arXiv:1708.01421 [math.NT], August 2017.",
				"Hong-Mei Liu, Shu-Hua Qi and Shu-Yan Ding, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL13/Liu/liu4.html\"\u003eSome Recurrence Relations for Cauchy Numbers of the First Kind\u003c/a\u003e, JIS, Vol. 13 (2010), Article 10.3.8.",
				"Donatella Merlini, Renzo Sprugnoli and M. Cecilia Verri, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2006.03.065\"\u003eThe Cauchy numbers\u003c/a\u003e, Discrete Math., Vol. 306, No. 16 (2006), pp. 1906-1920.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/BernoulliNumberoftheSecondKind.html\"\u003eBernoulli Numbers of the Second Kind\u003c/a\u003e.",
				"Ming Wu and Hao Pan, \u003ca href=\"http://www.fq.math.ca/Papers1/45-2/quartpan02_2007.pdf\"\u003eSums of products of Bernoulli numbers of the second kind\u003c/a\u003e, Fib. Quart., Vol. 45, No. 2 (2007), pp. 146-150.",
				"Feng-Zhen Zhao, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2008.10.013\"\u003eSums of products of Cauchy numbers\u003c/a\u003e, Discrete Math., Vol. 309, No. 12 (2009), pp. 3830-3842."
			],
			"formula": [
				"Numerator of integral of x(x-1)...(x-n+1) from 0 to 1.",
				"E.g.f.: x/log(1+x). (Note: the numerator of the coefficient of x^n/n! is a(n) - _Michael Somos_, Jul 12 2014)",
				"Numerator of Sum_{k=0..n} A048994(n,k)/(k+1). - _Peter Luschny_, Apr 28 2009",
				"Sum_{k=1..n} 1/k = C + log(n) + 1/(2n) + Sum_{k=2..inf} |a(n)|/A075178(n-1) * 1/(n*(n+1)*...*(n+k-1)) (section 0.131 in Gradshteyn and Ryzhik tables). - _Ralf Stephan_, Jul 12 2014",
				"a(n) = numerator(f(n) * n!), where f(0) = 1, f(n) = Sum_{k=0..n-1} (-1)^(n-k+1) * f(k) / (n-k+1). - _Daniel Suteu_, Feb 23 2018",
				"Sum_{k = 1..n} (1/k) = A001620 + log(n) + 1/(2n) - Sum_{k \u003e= 2} abs((a(k)/A006233(k)/k/(Product_{j = 0..k-1} (n-j)))), (see I. S. Gradsteyn, I. M. Ryzhik). - _A.H.M. Smeets_, Nov 14 2018"
			],
			"example": [
				"1, 1/2, -1/6, 1/4, -19/30, 9/4, -863/84, 1375/24, -33953/90, ..."
			],
			"maple": [
				"seq(numer(add(stirling1(n,k)/(k+1),k=0..n)),n=0..20); # _Peter Luschny_, Apr 28 2009"
			],
			"mathematica": [
				"a[n_] := Numerator[ Sum[ StirlingS1[n, k]/(k + 1), {k, 0, n}]]; Table[a[n], {n, 0, 19}] (* _Jean-François Alcover_, Nov 03 2011, after Maple *)",
				"a[n_] := Numerator[ Integrate[ Gamma[x+1]/Gamma[x-n+1], {x, 0, 1}]]; Table[a[n], {n, 0, 19}] (* _Jean-François Alcover_, Jul 29 2013 *)",
				"a[ n_] := If[ n \u003c 0, 0, (-1)^n Numerator @ Integrate[ Pochhammer[ -x, n], {x, 0, 1}]]; (* _Michael Somos_, Jul 12 2014 *)",
				"a[ n_] := If[ n \u003c 0, 0, Numerator [ n! SeriesCoefficient[ x / Log[ 1 + x], {x, 0, n}]]]; (* _Michael Somos_, Jul 12 2014 *)",
				"Join[{1}, Array[Numerator[(1/#) Integrate[Product[(x - k), {k, 0, # - 1}], {x, 0, 1}]] \u0026, 25]] (* _Michael De Vlieger_, Nov 13 2018 *)"
			],
			"program": [
				"(Sage)",
				"def A006232_list(len):",
				"    f, R, C = 1, [1], [1]+[0]*(len-1)",
				"    for n in (1..len-1):",
				"        for k in range(n, 0, -1):",
				"            C[k] = -C[k-1] * k / (k + 1)",
				"        C[0] = -sum(C[k] for k in (1..n))",
				"        R.append((C[0]*f).numerator())",
				"        f *= n",
				"    return R",
				"print(A006232_list(20)) # _Peter Luschny_, Feb 19 2016",
				"(PARI) for(n=0,20, print1(numerator( sum(k=0,n, stirling(n, k, 1)/(k+1)) ), \", \")) \\\\ _G. C. Greubel_, Nov 13 2018",
				"(MAGMA) [Numerator((\u0026+[StirlingFirst(n,k)/(k+1): k in [0..n]])): n in [0..20]]; // _G. C. Greubel_, Nov 13 2018",
				"(Python) # Results are abs values",
				"from fractions import gcd",
				"aa,n,sden = [0,1],1,1",
				"while n \u003c 20:",
				"    j,snom,sden,a = 1,0,(n+1)*sden,0",
				"    while j \u003c len(aa):",
				"        snom,j = snom+aa[j]*(sden//(j+1)),j+1",
				"    nom,den = snom,sden",
				"    print(n,nom//gcd(nom,den))",
				"    aa,j = aa+[-aa[j-1]],j-1",
				"    while j \u003e 0:",
				"        aa[j],j = n*aa[j]-aa[j-1],j-1",
				"    n = n+1 # _A.H.M. Smeets_, Nov 14 2018"
			],
			"xref": [
				"Cf. A006233 (denominators), A002206, A002207, A002208, A002209, A002657, A002790."
			],
			"keyword": "sign,frac,nice",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_",
			"references": 105,
			"revision": 97,
			"time": "2021-06-17T04:37:32-04:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}