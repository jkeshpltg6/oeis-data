{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A140271",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 140271,
			"data": "1,2,3,4,5,3,7,4,9,5,11,4,13,7,5,8,17,6,19,5,7,11,23,6,25,13,9,7,29,6,31,8,11,17,7,9,37,19,13,8,41,7,43,11,9,23,47,8,49,10,17,13,53,9,11,8,19,29,59,10,61,31,9,16,13,11,67,17,23,10,71,9,73,37,15,19,11,13,79,10,27",
			"name": "Least divisor of n that is \u003e sqrt(n), with a(1) = 1.",
			"comment": [
				"If n is not a square, then a(n) = A033677(n).",
				"If we define a divisor d|n to be strictly superior if d \u003e n/d, then strictly superior divisors are counted by A056924 and listed by A341673. This sequence selects the smallest strictly superior divisor of n. - _Gus Wiseman_, Apr 06 2021"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A140271/b140271.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"From _Gus Wiseman_, Apr 06 2021: (Start)",
				"a(n) is the smallest element in the following sets of strictly superior divisors:",
				"   1: {1}       16: {8,16}        31: {31}",
				"   2: {2}       17: {17}          32: {8,16,32}",
				"   3: {3}       18: {6,9,18}      33: {11,33}",
				"   4: {4}       19: {19}          34: {17,34}",
				"   5: {5}       20: {5,10,20}     35: {7,35}",
				"   6: {3,6}     21: {7,21}        36: {9,12,18,36}",
				"   7: {7}       22: {11,22}       37: {37}",
				"   8: {4,8}     23: {23}          38: {19,38}",
				"   9: {9}       24: {6,8,12,24}   39: {13,39}",
				"  10: {5,10}    25: {25}          40: {8,10,20,40}",
				"  11: {11}      26: {13,26}       41: {41}",
				"  12: {4,6,12}  27: {9,27}        42: {7,14,21,42}",
				"  13: {13}      28: {7,14,28}     43: {43}",
				"  14: {7,14}    29: {29}          44: {11,22,44}",
				"  15: {5,15}    30: {6,10,15,30}  45: {9,15,45}",
				"(End)"
			],
			"maple": [
				"with(numtheory):",
				"a:= n-\u003e min(select(d-\u003e is(d=n or d\u003esqrt(n)), divisors(n))):",
				"seq(a(n), n=1..100);  # _Alois P. Heinz_, Jan 29 2018"
			],
			"mathematica": [
				"Table[Select[Divisors[n], # \u003e Sqrt[n] \u0026][[1]], {n, 2, 70}] (* _Stefan Steinerberger_, May 18 2008 *)"
			],
			"program": [
				"(PARI) A140271(n)={local(d,a);d=divisors(n);a=n;for(i=1,length(d),if(d[i]\u003esqrt(n),a=min (d[i],a)));a} \\\\ _Michael B. Porter_, Apr 06 2010"
			],
			"xref": [
				"Cf. A060775, A033676, A033677.",
				"These divisors are counted by A056924.",
				"These divisors add up to A238535.",
				"These divisors that are odd are counted by A341594.",
				"These divisors that are squarefree are counted by A341595",
				"These divisors that are prime are counted by A341642.",
				"These divisors are listed by A341673.",
				"A038548 counts superior (or inferior) divisors.",
				"A161906 lists inferior divisors.",
				"A161908 lists superior divisors.",
				"A207375 list central divisors.",
				"A341674 lists strictly inferior divisors.",
				"- Inferior: A063962, A066839, A069288, A217581, A333749, A333750.",
				"- Superior: A051283, A059172, A063538, A063539, A070038, A072500, A116882, A116883, A341591, A341592, A341593, A341675, A341676.",
				"- Strictly Inferior: A070039, A333805, A333806, A341596, A341677.",
				"- Strictly Superior: A048098, A064052, A341643, A341644, A341646.",
				"Cf. A000005, A000203, A001221, A001222, A001248, A006530, A020639, A112798."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Leroy Quet_, May 16 2008",
			"ext": [
				"More terms from _Stefan Steinerberger_, May 18 2008",
				"a(70)-a(80) from _Ray Chandler_, Jun 25 2009",
				"_Franklin T. Adams-Watters_, Jan 26 2018, added a(1) = 1 to preserve the relation a(n) | n."
			],
			"references": 35,
			"revision": 29,
			"time": "2021-04-08T08:23:13-04:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}