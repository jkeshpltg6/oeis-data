{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A198322",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 198322,
			"data": "1,2,7,8,56,76,107,147,163,292,454,839,1433,4221,5833,6137,7987,8626,16216,17059,17128,17764,23438,25672,36812,41203,45952,46428,51768,60635,83009,86716,86908,88321,91951,93534,94542,99141,100142,108848,120357,124783,133741,136768,137941,140079,142424,145404,145654",
			"name": "The Matula-Goebel numbers of the rooted trees that have palindromic Wiener polynomials.",
			"comment": [
				"The Wiener polynomials are assumed to have zero constant terms.",
				"The Matula-Goebel number of a rooted tree can be defined in the following recursive manner: to the one-vertex tree there corresponds the number 1; to a tree T with root degree 1 there corresponds the t-th prime number, where t is the Matula-Goebel number of the tree obtained from T by deleting the edge emanating from the root; to a tree T with root degree m\u003e=2 there corresponds the product of the Matula-Goebel numbers of the m branches of T."
			],
			"reference": [
				"G. Caporossi, A. A. Dobrynin, I. Gutman, and P. Hansen, Trees with palindromic Hosoya polynomials, Graph Theory Notes of New York, XXXVI, 1999, 10-16."
			],
			"link": [
				"F. Goebel, \u003ca href=\"http://dx.doi.org/10.1016/0095-8956(80)90049-0\"\u003eOn a 1-1-correspondence between rooted trees and natural numbers\u003c/a\u003e, J. Combin. Theory, B 29 (1980), 141-143.",
				"I. Gutman and A. Ivic, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(95)00182-V\"\u003eOn Matula numbers\u003c/a\u003e, Discrete Math., 150, 1996, 131-142.",
				"I. Gutman and Yeong-Nan Yeh, \u003ca href=\"http://www.emis.de/journals/PIMB/067/3.html\"\u003eDeducing properties of trees from their Matula numbers\u003c/a\u003e, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"D. W. Matula, \u003ca href=\"http://www.jstor.org/stable/2027327\"\u003eA natural rooted tree enumeration by prime factorization\u003c/a\u003e, SIAM Rev. 10 (1968) 273.",
				"B. E. Sagan, Y-N. Yeh and P. Zhang, \u003ca href=\"http://users.math.msu.edu/users/sagan/Papers/Old/wpg-pub.pdf\"\u003eThe Wiener Polynomial of a Graph\u003c/a\u003e, Internat. J. of Quantum Chem., 60, 1996, 959-969.",
				"\u003ca href=\"/index/Mat#matula\"\u003eIndex entries for sequences related to Matula-Goebel numbers\u003c/a\u003e"
			],
			"formula": [
				"The Wiener polynomial W(n,x) of the rooted tree corresponding to the Matula-Goebel number n is given in A196059. It is palindromic if and only if x^{1+degree(W(n,x))}*W(n,1/x)=W(n,x)."
			],
			"example": [
				"7 is in the sequence because the rooted tree with Matula-Goebel number 7 is Y; 3 distances are equal to 1 and 3 distances are equal to 2; Wiener polynomial is 3x+3x^2."
			],
			"maple": [
				"with(numtheory): W := proc (n) local r, s, R: r := proc (n) options operator, arrow: op(1, factorset(n)) end proc: s := proc (n) options operator, arrow: n/r(n) end proc: R := proc (n) if n = 1 then 0 elif bigomega(n) = 1 then sort(expand(x*R(pi(n))+x)) else sort(expand(R(r(n))+R(s(n)))) end if end proc: if n = 1 then 0 elif bigomega(n) = 1 then sort(expand(W(pi(n))+x*R(pi(n))+x)) else sort(expand(W(r(n))+W(s(n))+R(r(n))*R(s(n)))) end if end proc: A := {}: for n to 100000 do if expand(x^(1+degree(W(n)))*subs(x = 1/x, W(n))) = W(n) then A := `union`(A, {n}) else  end if end do: A;"
			],
			"xref": [
				"Cf. A196059."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Emeric Deutsch_, Oct 24 2011",
			"references": 0,
			"revision": 13,
			"time": "2020-03-30T08:33:05-04:00",
			"created": "2011-10-24T16:59:11-04:00"
		}
	]
}