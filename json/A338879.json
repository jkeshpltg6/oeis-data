{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A338879",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 338879,
			"data": "1,3,2,5,11,3,7,11,9,4,9,37,31,27,5,11,28,8,7,19,6,13,79,69,61,55,51,7,15,53,47,21,19,35,33,8,17,137,41,37,101,31,29,83,9,19,86,78,71,13,12,56,53,51,10,21,211,193,177,163,151,141,133,127,123,11,23,127,39,18,50,31,29,41,13,25,73,12",
			"name": "Denominators in a set of expansions of the single-term Machin-like formula for Pi.",
			"comment": [
				"Numerators are A338878.",
				"Abrarov et al. give an identity arctan(n*x) = Sum_{m=1..n} arctan(x / (1 + (m-1)*m*x^2)). At x=1/n this identity provides set of expansions of the single-term Machin-like formula for Pi in form Pi/4 = arctan(1) = Sum_{m=1..n} arctan(n/((m-1)*m + n^2)). For m = n - k + 1 at k=1..n the fractions n / ((m-1)*m + n^2) constitute the triangle with rows in ascending order:",
				"    k=  1     2     3     4     5     6",
				"  n=1:  1;",
				"  n=2:  1/3,  1/2;",
				"  n=3:  1/5,  3/11, 1/3;",
				"  n=4:  1/7,  2/11, 2/9,  1/4;",
				"  n=5:  1/9,  5/37, 5/31, 5/27, 1/5;",
				"  n=6:  1/11, 3/28, 1/8,  1/7,  3/19, 1/6;"
			],
			"link": [
				"Sanjar Abrarov, \u003ca href=\"/A338879/b338879.txt\"\u003eTable of n, a(n) for n = 1..120\u003c/a\u003e",
				"Sanjar M. Abrarov, Rehan Siddiqui, Rajinder K. Jagpal, and Brendan M. Quine, \u003ca href=\"https://arxiv.org/abs/2004.11711\"\u003eUnconditional applicability of the Lehmer's measure to the two-term Machin-like formula for pi\u003c/a\u003e, arXiv:2004.11711 [math.GM], 2020."
			],
			"formula": [
				"T(n,k) = denominator of n / ((n-k)*(n-k+1) + n^2), for n\u003e=1 and 1 \u003c= k \u003c= n.",
				"Pi/4 = Sum_{k=1..n} arctan(A338878(n,k) / T(n,k))."
			],
			"example": [
				"The triangle T(n,k) begins:",
				"    k=  1   2   3   4   5   6",
				"  n=1:  1;",
				"  n=2:  3,  2;",
				"  n=3:  5,  11, 3;",
				"  n=4:  7,  11, 9,  4;",
				"  n=5:  9,  37, 31, 27, 5;",
				"  n=6:  11, 28, 8,  7,  19, 6;",
				"For example, at n = 3 the expansion formula is Pi/4 = arctan(1/5) + arctan(3/11) + arctan(1/3) and the corresponding sequence in the denominators is 5,11,3."
			],
			"mathematica": [
				"(*Define variable*)",
				"PiOver4[m_] := Sum[ArcTan[m/((k - 1)*k + m^2)], {k, 1, m}];",
				"(*Expansions*)",
				"m := 1;",
				"While[m \u003c= 10,",
				"  If[m == 1, Print[\"\\[Pi]/4 = ArcTan[1/1]\"],",
				"    Print[\"\\[Pi]/4 = \", PiOver4[m]]]; m = m + 1];",
				"(*Verification*)",
				"m := 1;",
				"While[m \u003c= 10, Print[PiOver4[m] == Pi/4]; m = m + 1];",
				"(*Denominators*)",
				"For[n = 1, n \u003c= 10, n++, {k := 1; sq := {};",
				"  While[n \u003e= k, AppendTo[sq, Denominator[n/((n - k)*(n - k",
				"    + 1) + n^2)]]; k++]}; Print[sq]];"
			],
			"program": [
				"(PARI) T(n, k) = if (n\u003e=k, denominator(n/((n - k)*(n - k + 1) + n^2)))",
				"matrix(10, 10, n, k, T(n, k)) \\\\ _Michel Marcus_, Nov 14 2020"
			],
			"xref": [
				"Cf. A338878 (numerators), A003881 (Pi/4)."
			],
			"keyword": "nonn,frac,tabl",
			"offset": "1,2",
			"author": "_Sanjar Abrarov_, Nov 13 2020",
			"references": 2,
			"revision": 67,
			"time": "2020-12-23T08:42:39-05:00",
			"created": "2020-12-23T08:42:39-05:00"
		}
	]
}