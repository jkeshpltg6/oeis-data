{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A284592",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 284592,
			"data": "1,1,1,2,0,2,3,1,1,3,5,1,2,1,5,7,2,3,3,2,7,11,2,5,4,5,2,11,15,4,6,7,7,6,4,15,22,4,10,8,12,8,10,4,22,30,7,12,14,14,14,14,12,7,30,42,8,18,16,24,16,24,16,18,8,42,56,12,23,25,28,28,28,28,25,23,12,56",
			"name": "Square array read by antidiagonals: T(n,k) is the number of pairs of partitions of n and k respectively, such that the pair of partitions have no part in common.",
			"comment": [
				"Compare with A284593."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A284592/b284592.txt\"\u003eAntidiagonals n = 0..200, flattened\u003c/a\u003e",
				"H. S. Wilf, \u003ca href=\"https://www.math.upenn.edu/~wilf/PIMS/PIMSLectures.pdf\"\u003eLectures on Integer Partitions\u003c/a\u003e"
			],
			"formula": [
				"O.g.f. Product_{j \u003e= 1} (1 + x^j/(1 - x^j) + y^j/(1 - y^j)) = Sum_{n,k \u003e= 0} T(n,k)*x^n*y^k (see Wilf, Example 7).",
				"Antidiagonal sums are A015128."
			],
			"example": [
				"Square array begins",
				"  n\\k|  0  1  2  3  4  5   6   7   8   9  10",
				"- - - - - - - - - - - - - - - - - - - - - - -",
				"  0  |  1  1  2  3  5  7  11  15  22  30  42: A000041",
				"  1  |  1  0  1  1  2  2   4   4   7   8  12: A002865",
				"  2  |  2  1  2  3  5  6  10  12  18  23  32",
				"  3  |  3  1  3  4  7  8  14  16  25  31  44",
				"  4  |  5  2  5  7 12 14  24  28  43  54  76",
				"  5  |  7  2  6  8 14 16  28  31  49  60  85",
				"  6  | 11  4 10 14 24 28  48  55  85 106 149",
				"  7  | 15  4 12 16 28 31  55  60  95 115 163",
				"  8  | 22  7 18 25 43 49  85  95 148 182 256",
				"  9  | 30  8 23 31 54 60 106 115 182 220 311",
				"  10 | 42 12 32 44 76 85 149 163 256 311 438",
				"  ...",
				"T(4,3) = 7: the 7 pairs of partitions of 4 and 3 with no parts in common are (4, 3), (4, 2 + 1), (4, 1 + 1 + 1), (2 + 2, 3), (2 + 2, 1 + 1 + 1), (2 + 1 + 1 , 3) and (1 + 1 + 1 + 1, 3)."
			],
			"maple": [
				"#A284592 as a square array",
				"ser := taylor(taylor(mul(1 + x^j/(1 - x^j) + y^j/(1 - y^j), j = 1..10), x, 11), y, 11):",
				"convert(ser, polynom):",
				"s := convert(%, polynom):",
				"with(PolynomialTools):",
				"for n from 0 to 10 do CoefficientList(coeff(s, y, n), x) end do;",
				"# second Maple program:",
				"b:= proc(n, k, i) option remember; `if`(n=0 and",
				"       (k=0 or i=1), 1, `if`(i\u003c1, 0, b(n, k, i-1)+",
				"       add(b(sort([n-i*j, k])[], i-1), j=1..n/i)+",
				"       add(b(sort([n, k-i*j])[], i-1), j=1..k/i)))",
				"    end:",
				"A:= (n, k)-\u003e (l-\u003e b(l[1], l[2]$2))(sort([n, k])):",
				"seq(seq(A(n, d-n), n=0..d), d=0..12);  # _Alois P. Heinz_, Apr 02 2017"
			],
			"mathematica": [
				"Table[Total@ Boole@ Map[! IntersectingQ @@ Map[Union, #] \u0026, Tuples@ {IntegerPartitions@ #, IntegerPartitions@ k}] \u0026[n - k], {n, 0, 11}, {k, 0, n}] // Flatten (* _Michael De Vlieger_, Apr 02 2017 *)",
				"b[n_, k_, i_] := b[n, k, i] = If[n == 0 \u0026\u0026",
				"     (k == 0 || i == 1), 1, If[i \u003c 1, 0, b[n, k, i - 1] +",
				"     Sum[b[Sequence @@ Sort[{n - i*j, k}], i - 1], {j, 1, n/i}] +",
				"     Sum[b[Sequence @@ Sort[{n, k - i*j}], i - 1], {j, 1, k/i}]]];",
				"A[n_, k_] := Function [l, b[l[[1]], l[[2]], l[[2]]]][Sort[{n, k}]];",
				"Table[Table[A[n, d - n], {n, 0, d}], {d, 0, 12}] // Flatten (* _Jean-François Alcover_, Jun 07 2021, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A000041 (row 0), A002865 (row 1), A015128 (antidiagonal sums), A284593.",
				"Main diagonal gives A054440 or 2*A260669 (for n\u003e0)."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,4",
			"author": "_Peter Bala_, Mar 30 2017",
			"references": 4,
			"revision": 30,
			"time": "2021-06-10T07:41:36-04:00",
			"created": "2017-04-02T13:05:40-04:00"
		}
	]
}