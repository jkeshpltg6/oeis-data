{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A244477",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 244477,
			"data": "3,2,1,3,5,4,3,8,7,3,11,10,3,14,13,3,17,16,3,20,19,3,23,22,3,26,25,3,29,28,3,32,31,3,35,34,3,38,37,3,41,40,3,44,43,3,47,46,3,50,49,3,53,52,3,56,55,3,59,58,3,62,61,3,65,64,3,68,67,3,71,70,3,74,73,3,77,76,3,80",
			"name": "a(1)=3, a(2)=2, a(3)=1; thereafter a(n) = a(n-a(n-1)) + a(n-a(n-2)).",
			"comment": [
				"Similar to Hofstadter's Q-sequence A005185 but with different starting values.",
				"Golomb describes this as \"quasi-periodic sequence with a quasi-period of 3\"."
			],
			"reference": [
				"Higham, J.; Tanny, S. More well-behaved meta-Fibonacci sequences. Proceedings of the Twenty-fourth Southeastern International Conference on Combinatorics, Graph Theory, and Computing (Boca Raton, FL, 1993). Congr. Numer. 98(1993), 3-17."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A244477/b244477.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Altug Alkan, Nathan Fox, and Orhan Ozgur Aybar, \u003ca href=\"https://doi.org/10.1155/2017/2614163\"\u003eOn Hofstadter Heart Sequences\u003c/a\u003e, Complexity, Volume 2017, Article ID 2614163, 8 pages.",
				"Nathan Fox, \u003ca href=\"https://vimeo.com/141111990\"\u003eLinear-Recurrent Solutions to Meta-Fibonacci Recurrences, Part 1 (video)\u003c/a\u003e, Rutgers Experimental Math Seminar, Oct 01 2015. Part 2 is vimeo.com/141111991.",
				"S. W. Golomb, \u003ca href=\"/A005185/a005185_1.pdf\"\u003eDiscrete chaos: sequences satisfying \"strange\" recursions\u003c/a\u003e, unpublished manuscript, circa 1990 [cached copy, with permission (annotated)]",
				"\u003ca href=\"/index/Ho#Hofstadter\"\u003eIndex entries for Hofstadter-type sequences\u003c/a\u003e"
			],
			"formula": [
				"From _Colin Barker_, Nov 23 2015: (Start)",
				"a(n) = 2*a(n-3) - a(n-6) for n\u003e6.",
				"G.f.: x*(2*x^5 + x^4 - 3*x^3 + x^2 + 2*x + 3)/((x - 1)^2*(x^2 + x + 1)^2). (End)",
				"a(3*k) = 3*k-2, a(3*k+1) = 3, a(3*k+2) = 3*k+2. - _Nathan Fox_, Apr 02 2017",
				"a(n) = 3*(m-1)^2*floor(n/3) - (3*m^2-8*m+2), where m = n mod 3. - _Luce ETIENNE_, Oct 17 2018"
			],
			"maple": [
				"f := proc(n) option remember;",
				"    if n\u003c=3 then",
				"        4-n",
				"    elif n \u003e procname(n-1) and n \u003e procname(n-2) then",
				"        RETURN(procname(n-procname(n-1))+procname(n-procname(n-2)));",
				"    else",
				"        ERROR(\" died at n= \", n);",
				"    fi;",
				"end proc;",
				"[seq(f(n),n=0..200)];"
			],
			"mathematica": [
				"a[1] = 3; a[2] = 2; a[3] = 1; a[n_] := a[n] = a[n - a[n - 1]] + a[n - a[n - 2]]; Array[a, 75] (* or *)",
				"Flatten@ Table[{Mod[3n, 3] +3, 3n -1, 3n -2}, {n, 25}] (* _Robert G. Wilson v_, Nov 23 2015 *)"
			],
			"program": [
				"(Haskell)",
				"a244477 n = a244477_list !! (n-1)",
				"a244477_list = 3 : 2 : 1 : zipWith (+)",
				"   (map a244477 $ zipWith (-) [4..] $ tail a244477_list)",
				"   (map a244477 $ zipWith (-) [4..] $ drop 2 a244477_list)",
				"-- _Reinhard Zumkeller_, Jul 05 2014",
				"(MAGMA) [n le 3 select 4-n else Self(n-Self(n-1)) + Self(n-Self(n-2)): n in [1..80]]; // _Vincenzo Librandi_, Nov 24 2015"
			],
			"xref": [
				"Cf. A005185.",
				"Cf. A010872."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_, Jul 02 2014",
			"references": 38,
			"revision": 40,
			"time": "2019-10-05T09:16:43-04:00",
			"created": "2014-07-02T18:34:47-04:00"
		}
	]
}