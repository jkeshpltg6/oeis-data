{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A286575",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 286575,
			"data": "1,2,2,2,2,4,2,4,2,4,4,4,2,4,4,2,2,4,4,4,4,8,4,8,2,4,4,4,4,8,2,4,2,4,4,4,4,8,4,8,4,8,8,8,4,8,8,4,2,4,4,4,4,8,4,8,4,8,8,8,2,4,4,4,2,4,4,4,4,8,4,8,4,8,8,8,4,8,8,4,4,8,8,8,8,16,8,16,4,8,8,8,8,16,4,8,2,4,4,4,4,8,4,8,4,8,8,8,4",
			"name": "Run-length transform of A001316.",
			"link": [
				"Antti Karttunen, \u003ca href=\"/A286575/b286575.txt\"\u003eTable of n, a(n) for n = 0..16384\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e",
				"\u003ca href=\"/index/Ru#rlt\"\u003eIndex entries for sequences computed with run length transform\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A037445(A005940(1+n)).",
				"a(n) = A000079(A286574(n))."
			],
			"example": [
				"For n = 0, there are no 1-runs, and thus a(0) = 1 as an empty product.",
				"For n = 29, \"11101\" in binary, there are two 1-runs, of lengths 1 and 3, thus a(29) = A001316(1) * A001316(3) = 2*4 = 8."
			],
			"mathematica": [
				"Table[Times @@ Map[Sum[Mod[#, 2] \u0026@ Binomial[#, k], {k, 0, #}] \u0026@ Length@ # \u0026, DeleteCases[Split@ IntegerDigits[n, 2], _?(First@ # == 0 \u0026)]], {n, 0, 108}] (* _Michael De Vlieger_, May 29 2017 *)"
			],
			"program": [
				"(Scheme)",
				"(define (A286575 n) (fold-left (lambda (a r) (* a (A001316 r))) 1 (bisect (reverse (binexp-\u003eruncount1list n)) (- 1 (modulo n 2)))))",
				"(define (bisect lista parity) (let loop ((lista lista) (i 0) (z (list))) (cond ((null? lista) (reverse! z)) ((eq? i parity) (loop (cdr lista) (modulo (1+ i) 2) (cons (car lista) z))) (else (loop (cdr lista) (modulo (1+ i) 2) z)))))",
				"(define (binexp-\u003eruncount1list n) (if (zero? n) (list) (let loop ((n n) (rc (list)) (count 0) (prev-bit (modulo n 2))) (if (zero? n) (cons count rc) (if (eq? (modulo n 2) prev-bit) (loop (floor-\u003eexact (/ n 2)) rc (1+ count) (modulo n 2)) (loop (floor-\u003eexact (/ n 2)) (cons count rc) 1 (modulo n 2)))))))",
				"(define (A001316 n) (let loop ((n n) (z 1)) (cond ((zero? n) z) ((even? n) (loop (/ n 2) z)) (else (loop (/ (- n 1) 2) (* z 2))))))",
				"(Python)",
				"from sympy import factorint, prime, log",
				"import math",
				"def wt(n): return bin(n).count(\"1\")",
				"def a037445(n):",
				"    f=factorint(n)",
				"    return 2**sum([wt(f[i]) for i in f])",
				"def A(n): return n - 2**int(math.floor(log(n, 2)))",
				"def b(n): return n + 1 if n\u003c2 else prime(1 + (len(bin(n)[2:]) - bin(n)[2:].count(\"1\"))) * b(A(n))",
				"def a(n): return a037445(b(n)) # _Indranil Ghosh_, May 30 2017"
			],
			"xref": [
				"Cf. A000079, A001316, A005940, A037445, A227349, A247282, A286574."
			],
			"keyword": "nonn,base",
			"offset": "0,2",
			"author": "_Antti Karttunen_, May 28 2017",
			"references": 5,
			"revision": 17,
			"time": "2017-05-30T18:16:26-04:00",
			"created": "2017-05-30T18:16:26-04:00"
		}
	]
}