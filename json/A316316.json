{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A316316",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 316316,
			"data": "1,4,8,8,12,20,20,20,28,32,32,36,40,44,48,48,52,60,60,60,68,72,72,76,80,84,88,88,92,100,100,100,108,112,112,116,120,124,128,128,132,140,140,140,148,152,152,156,160,164,168,168,172,180,180,180,188,192,192",
			"name": "Coordination sequence for tetravalent node in chamfered version of square grid.",
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A316316/b316316.txt\"\u003eTable of n, a(n) for n = 0..5000\u003c/a\u003e",
				"Michel Deza and Mikhail Shtogrin, \u003ca href=\"https://doi.org/10.1016/S0012-365X(01)00074-7\"\u003eIsometric embedding of mosaics into cubic lattices\u003c/a\u003e, Discrete mathematics 244.1-3 (2002): 43-53. See Fig. 2.",
				"Michel Deza and Mikhail Shtogrin, \u003ca href=\"/A316316/a316316.png\"\u003eIsometric embedding of mosaics into cubic lattices\u003c/a\u003e, Discrete mathematics 244.1-3 (2002): 43-53. [Annotated scan of page 52 only]",
				"Michel Deza and Mikhail Shtogrin, \u003ca href=\"/A316316/a316316_5.png\"\u003eEnlargement of figure from previous link\u003c/a\u003e",
				"Chaim Goodman-Strauss and N. J. A. Sloane, \u003ca href=\"https://doi.org/10.1107/S2053273318014481\"\u003eA Coloring Book Approach to Finding Coordination Sequences\u003c/a\u003e, Acta Cryst. A75 (2019), 121-134, also \u003ca href=\"http://NeilSloane.com/doc/Cairo_final.pdf\"\u003eon NJAS's home page\u003c/a\u003e. Also \u003ca href=\"http://arxiv.org/abs/1803.08530\"\u003eon arXiv\u003c/a\u003e, arXiv:1803.08530 [math.CO], 2018-2019.",
				"Rémy Sigrist, \u003ca href=\"/A316316/a316316.gp.txt\"\u003ePARI program for A316316\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A316316/a316316_6.png\"\u003eIllustration of first terms\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/A316316/a316316_1.png\"\u003eInitial terms of coordination sequence for tetravalent node\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/A316316/a316316_3.png\"\u003eTrunks and branches structure of tetravalent node\u003c/a\u003e (First part of proof that a(n+12)=a(n)+40).",
				"N. J. A. Sloane, \u003ca href=\"/A316316/a316316_4.png\"\u003eCalculation of coordination sequence\u003c/a\u003e (Second part of proof that a(n+12)=a(n)+40).",
				"N. J. A. Sloane, \u003ca href=\"/A316316/a316316_7.png\"\u003e\"Basketweave\" tiling by 3X1 rectangles which is equivalent (as far as the graph and coordination sequences are concerned) to this tiling \u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/A316316/a316316.pdf\"\u003eAn equivalent tiling seen on the sidewalk of East 70th St in New York City\u003c/a\u003e. As far as the graph and coordination sequences are concerned, this is the same as the chamfered square grid. The trivalent vertices labeled b and c are equivalent to each other.",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,-1,2,-1,1,-1)."
			],
			"formula": [
				"Apparently, a(n + 12) = a(n) + 40 for any n \u003e 0. - _Rémy Sigrist_, Jun 30 2018",
				"From _N. J. A. Sloane_, Jun 30 2018: This conjecture is true.",
				"Theorem: a(n + 12) = a(n) + 40 for any n \u003e 0.",
				"The proof uses the Coloring Book Method described in the Goodman-Strauss - Sloane article. For details see the two links.",
				"From _Colin Barker_, Dec 13 2018: (Start)",
				"G.f.: (1 + 3*x + 5*x^2 + 2*x^3 + 5*x^4 + 3*x^5 + x^6) / ((1 - x)^2*(1 + x^2)*(1 + x + x^2)).",
				"a(n) = a(n-1) - a(n-2) + 2*a(n-3) - a(n-4) + a(n-5) - a(n-6) for n\u003e6.",
				"(End)",
				"a(n) = (2/9)*(15*n + 9*A056594(n-1) - 6*A102283(n)) for n \u003e 0. - _Stefano Spezia_, Jun 12 2021"
			],
			"mathematica": [
				"Join[{1}, LinearRecurrence[{1, -1, 2, -1, 1, -1}, {4, 8, 8, 12, 20, 20}, 100]] (* _Jean-François Alcover_, Dec 13 2018 *)"
			],
			"program": [
				"(PARI) See Links section."
			],
			"xref": [
				"See A316317 for trivalent node.",
				"See A250120 for links to thousands of other coordination sequences.",
				"Cf. A316357 (partial sums).",
				"Cf. A056594, A102283."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Jun 29 2018",
			"ext": [
				"More terms from _Rémy Sigrist_, Jun 30 2018"
			],
			"references": 4,
			"revision": 74,
			"time": "2021-06-30T19:52:07-04:00",
			"created": "2018-06-29T14:38:38-04:00"
		}
	]
}