{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A131744",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 131744,
			"data": "1,9,9,5,5,9,9,5,5,9,1,3,13,17,1,3,13,17,9,5,5,9,9,5,5,9,1,3,13,17,1,3,13,17,9,5,5,9,10,1,9,15,12,10,13,0,15,12,1,9,2,15,0,9,5,14,17,17,9,6,15,0,9,1,1,9,15,12,10,13,0,15,12,1,9,2,15,0,9,5,14,17,17,9",
			"name": "Eric Angelini's \"1995\" puzzle: the sequence is defined by the property that if one writes the English names for the entries, replaces each letter with its rank in the alphabet and calculates the absolute values of the differences, one recovers the sequence.",
			"comment": [
				"In the first few million terms, the numbers 16, 19, 20 and 22-26 do not occur. Of the numbers that do occur, the number 11 appears with the smallest frequence - see A133152. - _N. J. A. Sloane_, Sep 22 2007",
				"From _David Applegate_, Sep 24 2007: (Start)",
				"The numbers 16, 19-20, 22-25 never occur in the sequence. The following table gives the possible numbers that can occur in the sequence and for each one, the possible numbers that can follow it. The table is complete - when any number and its successor are expanded, the resulting pairs are also in the table. It contains the expansion of 1 and thus describes all possible transitions:",
				"0 -\u003e 0,1,4,5,7,9,10,12,15,21",
				"1 -\u003e 1,3,5,9,12",
				"2 -\u003e 1,3,12,15",
				"3 -\u003e 0,1,2,3,4,5,8,9,11,12,13,14,18",
				"4 -\u003e 2,3,12,14",
				"5 -\u003e 3,5,9,10,12,14,15",
				"6 -\u003e 3,5,12,15,21",
				"7 -\u003e 7,10,17",
				"8 -\u003e 0,3,5,9",
				"9 -\u003e 0,1,2,3,4,5,6,8,9,10,12,14,15,21",
				"10 -\u003e 1,13,15,17",
				"11 -\u003e 21",
				"12 -\u003e 0,1,6,9,10,14,15,21",
				"13 -\u003e 0,3,17",
				"14 -\u003e 3,10,15,17",
				"15 -\u003e 0,3,4,9,12,15,18",
				"17 -\u003e 1,9,10,14,15,17,21",
				"18 -\u003e 3,7,9",
				"21 -\u003e 13,21",
				"(End)",
				"The sequence may also be extended in the reverse direction: ... 0 21 21 13 3 0 [then what we have now] 1 9 9 5 5 ..., corresponding to ... zero twentyone twentyone thirteen three zero one nine nine five ... - _N. J. A. Sloane_, Sep 27 2007",
				"The name of this sequence (\"Eric Angelini's ... puzzle\") was added by _N. J. A. Sloane_ many months after Eric Angelini submitted it.",
				"Begin with 1, map the integer to its name and then map according to A073029, compute the absolute difference, spell out that difference; iterate as necessary. - _Robert G. Wilson v_, Jun 08 2010"
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A131744/b131744.txt\"\u003eTable of n, a(n) for n = 1..40000\u003c/a\u003e"
			],
			"example": [
				"O.N.E...N.I.N.E...N.I.N.E...F.I..V..E...F.I..V..E...",
				".1.9..9..5.5.9..9..5.5.9..1..3.13.17..1..3.13.17....",
				"1 -\u003e \"one\" -\u003e 15,14,5 -\u003e (the difference is) 1,9; iterate. Therefore 1,9 -\u003e \"one,nine\"; -\u003e 15,14,5,14,9,14,5 -\u003e 1,9,9,5,5,9; \"one,nine,nine,five,five,nine\"; etc. - _Robert G. Wilson v_, Jun 08 2010"
			],
			"mathematica": [
				"Nest[Abs@Differences@Flatten[LetterNumber[Characters[IntegerName@#]/.\"-\"-\u003e\"\"]\u0026/@#]\u0026,{1},4] (* _Giorgos Kalogeropoulos_, Apr 11 2021 *)"
			],
			"program": [
				"(Python)",
				"def chrdist(a, b): return abs(ord(a)-ord(b))",
				"def aupto(nn):",
				"  allnames = \"zero,one,two,three,four,five,six,seven,eight,nine,ten,eleven,twelve,thirteen,fourteen,fifteen,sixteen,seventeen,eighteen,nineteen,twenty,twentyone\"",
				"  names = allnames.split(\",\")",
				"  alst, aidx, last, nxt = [1, 9], 1, \"e\", \"one\"",
				"  while len(alst) \u003c nn:",
				"    nxt = names[alst[aidx]]",
				"    alst += [chrdist(a, b) for a, b in zip(last+nxt[:-1], nxt)]",
				"    last, aidx = nxt[-1], aidx + 1",
				"  return alst[:nn]",
				"print(aupto(84)) # _Michael S. Branicky_, Jan 09 2021"
			],
			"xref": [
				"Cf. A131745, A131746, A130316, A133152, A133816, A133817.",
				"Cf. A131285 (ranks of letters), A131286, A131287."
			],
			"keyword": "nonn,word,nice",
			"offset": "1,2",
			"author": "_Eric Angelini_, Sep 20 2007",
			"ext": [
				"More terms from _N. J. A. Sloane_, Sep 20 2007"
			],
			"references": 20,
			"revision": 28,
			"time": "2021-05-01T08:01:39-04:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}