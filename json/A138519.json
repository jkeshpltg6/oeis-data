{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A138519",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 138519,
			"data": "1,-2,3,-6,11,-16,24,-38,57,-82,117,-168,238,-328,448,-614,834,-1114,1480,-1966,2592,-3384,4398,-5704,7361,-9436,12045,-15344,19470,-24576,30922,-38822,48576,-60548,75259,-93342,115454,-142360,175104,-214958,263262",
			"name": "Expansion of q * (psi(q^5) / psi(q))^2 in powers of q where psi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A138519/b138519.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of ((eta(q^10) / eta(q^2))^2 * eta(q) / eta(q^5))^2 in powers of q.",
				"Euler transform of period 10 sequence [ -2, 2, -2, 2, 0, 2, -2, 2, -2, 0, ...].",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2)) where f(u, v) = (u - v)^2 - v * (1 - u) * (1 - 5*u).",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^3)) where f(u, v) = (u - v)^4 - u * (1 - u) * (1 - 5*u) * v * (1 - v) * (1 - 5*v).",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (10 t)) = (1/5) g(t) where q = exp(2 Pi i t) and g() is the g.f. for A138518.",
				"G.f.: x * (Product_{k\u003e0} P(5, x^k) * P(10, x^k)^2)^2 where P(n, x) is the n-th cyclotomic polynomial.",
				"a(n) = - A138520(n) unless n=0. -5 * a(n) = A138521(n) unless n=0.",
				"Convolution inverse of A138516.",
				"a(n) = -(-1)^n * A210458(n). - _Michael Somos_, Sep 16 2015",
				"a(n) ~ -(-1)^n * exp(2*Pi*sqrt(n/5)) / (2 * 5^(5/4) * n^(3/4)). - _Vaclav Kotesovec_, Nov 15 2017"
			],
			"example": [
				"G.f. = q - 2*q^2 + 3*q^3 - 6*q^4 + 11*q^5 - 16*q^6 + 24*q^7 - 38*q^8 + 57*q^9 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (EllipticTheta[ 2, 0, q^(5/2)] / EllipticTheta[ 2, 0, q^(1/2)])^2, {q, 0, n}]; (* _Michael Somos_, Sep 16 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c1, 0, n--; A = x * O(x^n); polcoeff( ( eta(x + A) / eta(x^5 + A) * ( eta(x^10 + A) / eta(x^2 + A) )^2)^2, n))};"
			],
			"xref": [
				"Cf. A138516, A138520, A138521, A210458."
			],
			"keyword": "sign",
			"offset": "1,2",
			"author": "_Michael Somos_, Mar 23 2008",
			"references": 6,
			"revision": 16,
			"time": "2021-03-12T22:24:45-05:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}