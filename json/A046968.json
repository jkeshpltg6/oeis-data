{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A046968",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 46968,
			"data": "1,-1,1,-1,1,-691,1,-3617,43867,-174611,77683,-236364091,657931,-3392780147,1723168255201,-7709321041217,151628697551,-26315271553053477373,154210205991661,-261082718496449122051,1520097643918070802691",
			"name": "Numerators of coefficients in Stirling's expansion for log(Gamma(z)).",
			"comment": [
				"A001067(n) = a(n) if n\u003c574; A001067(574) = 37*a(574). - _Michael Somos_, Feb 01 2004"
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, Tenth Printing, 1972, p. 257, Eq. 6.1.41.",
				"L. V. Ahlfors, Complex Analysis, McGraw-Hill, 1979, p. 205"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A046968/b046968.txt\"\u003eTable of n, a(n) for n = 1..314\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards Applied Math. Series 55, Tenth Printing, 1972, p. 257, Eq. 6.1.41.",
				"R. P. Brent, \u003ca href=\"http://arxiv.org/abs/1608.04834\"\u003e Asymptotic approximation of central binomial coefficients with rigorous error bounds\u003c/a\u003e, arXiv:1608.04834 [math.NA], 2016.",
				"N. Elezovic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Elezovic/elezovic5.html\"\u003eAsymptotic Expansions of Central Binomial Coefficients and Catalan Numbers\u003c/a\u003e, J. Int. Seq. 17 (2014) # 14.2.1.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/StirlingsSeries.html\"\u003eStirling's Series\u003c/a\u003e",
				"\u003ca href=\"/index/Be#Bernoulli\"\u003eIndex entries for sequences related to Bernoulli numbers.\u003c/a\u003e"
			],
			"formula": [
				"From numerator of Jk(z) = (-1)^(k-1)*Bk/(((2k)*(2k-1))*z^(2k-1)), so Gamma(z) = sqrt(2pi)*z^(z-0.5)*exp(-z)*exp(J(z))."
			],
			"maple": [
				"seq(numer(bernoulli(2*n)/(2*n*(2*n-1))), n = 1..25); # _G. C. Greubel_, Sep 19 2019"
			],
			"mathematica": [
				"Table[ Numerator[ BernoulliB[2n]/(2n(2n - 1))], {n, 1, 22}] (* _Robert G. Wilson v_, Feb 03 2004 *)",
				"s = LogGamma[z] + z - (z - 1/2) Log[z] - Log[2 Pi]/2 + O[z, Infinity]^42; DeleteCases[CoefficientList[s, 1/z], 0] // Numerator (* _Jean-François Alcover_, Jun 13 2017 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c1,0,numerator(bernfrac(2*n)/(2*n)/(2*n-1)))",
				"(MAGMA) [Numerator(Bernoulli(2*n)/(2*n*(2*n-1))): n in [1..25]]; // _G. C. Greubel_, Sep 19 2019",
				"(Sage) [numerator(bernoulli(2*n)/(2*n*(2*n-1))) for n in (1..25)] # _G. C. Greubel_, Sep 19 2019",
				"(GAP) List([1..25], n-\u003e NumeratorRat(Bernoulli(2*n)/(2*n*(2*n-1))) ); # _G. C. Greubel_, Sep 19 2019"
			],
			"xref": [
				"Denominators given by A046969.",
				"Similar to but different from A001067. See A090495, A090496."
			],
			"keyword": "frac,sign,nice",
			"offset": "1,6",
			"author": "Douglas Stoll (dougstoll(AT)email.msn.com)",
			"ext": [
				"More terms from _Frank Ellermann_, Jun 13 2001"
			],
			"references": 10,
			"revision": 45,
			"time": "2019-09-20T09:01:23-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}