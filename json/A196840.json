{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A196840",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 196840,
			"data": "1,1,1,1,1,1,0,1,1,1,-1,0,1,1,1,0,-1,0,5,1,1,1,0,-1,0,1,1,1,0,1,0,-7,0,7,1,1,-1,0,2,0,-7,0,2,1,1,0,-3,0,1,0,-7,0,3,1,1,5,0,-1,0,1,0,-1,0,5,1,1",
			"name": "Numerators of coefficients of the polynomials in n of the sums of powers of the first n positive integers.",
			"comment": [
				"Duplicate of A162298. - _Joerg Arndt_, May 27 2019",
				"The sums of the k-th power of each of the first n positive integers, sum(j^k,j=1..n), k\u003e=0, n\u003e=1, abbreviated usually as Sigma n^k, can be written as Sigma n^k = sum(r(k,m)*n^m,m=1..k+1), with the rational number triangle r(n,m)=a(n,m)/A162299(k+1,m). See, e.g., the Graham et al. reference, eq. (6.78), p. 269, where Sigma n^k is S_k(n+1) - delta(k,0), with delta(k,0)=1 if k=0 and 0 else. The formula for r(n,m) given below can be adapted from this reference, and it is found in the given form (for k\u003e0) in the Remmert reference, p. 175.",
				"For sums of powers of integers see the array A103438 with further references and links."
			],
			"reference": [
				"R. L. Graham, D. E. Knuth and O. Patashnik, Concrete Mathematics. Addison-Wesley, Reading, MA, 1991 (Seventh printing).Second ed. 1994.",
				"R. Remmert, Funktionentheorie I, Zweite Auflage, Springer-Verlag, 1989. English version: Classical topics in complex function theory, Springer, 1998."
			],
			"formula": [
				"a(k,m)= numerator(r(k,m)) with",
				"r(k,m)= 1/(k+1) if m=k+1, 1/2 if m=k, and (B(k+1-m)/(k+1-m))*binomial(k,m) if m = 1,...,k-1, with the Bernoulli numbers B(n)=A027641(n)/A027642(n).",
				"Another version, involving Stirling numbers is",
				"r(k,m) = ((-1)^(k+1-m))*sum(S(k,l)*s(l+1,m)/(l+1),l=(m-1),...,k), k\u003e=0, m=1,...,k+1, with the Stirling numbers of the second kind S, given in A048993, and of the first kind s, given in A048994. For this formula see the W. Lang link under A196837, addendum."
			],
			"example": [
				"The triangle a(k,m) starts with",
				"k\\m  1   2  3  4  5  6  7  8  9 10 11 ...",
				"0:   1",
				"1:   1   1",
				"2:   1   1  1",
				"3:   0   1  1  1",
				"4:  -1   0  1  1  1",
				"5:   0  -1  0  5  1  1",
				"6:   1   0 -1  0  1  1  1",
				"7:   0   1  0 -7  0  7  1  1",
				"8:  -1   0  2  0 -7  0  2  1  1",
				"9:   0  -3  0  1  0 -7  0  3  1  1",
				"10:  5   0 -1  0  1  0 -1  0  5  1  1",
				"...",
				"The rational number triangle a(k,m)/A162299(k+1,m) starts with",
				"k\\m  1     2     3     4     5      6    7    8    9 ...",
				"0:   1",
				"1:  1/2   1/2",
				"2:  1/6   1/2   1/3",
				"3:   0    1/4   1/2   1/4",
				"4: -1/30   0    1/3   1/2   1/5",
				"5:   0   -1/12   0   5/12   1/2    1/6",
				"6:  1/42   0   -1/6    0    1/2    1/2  1/7",
				"7:   0    1/12   0   -7/24   0    7/12  1/2  1/8",
				"8: -1/30   0    2/9    0   -7/15    0   2/3  1/2  1/9",
				"...",
				"Sigma n^4 = sum(j^4,j=1..n) =",
				"-(1/30)*n + (1/3)*n^3 +  (1/2)*n^4 + (1/5)*n^5.",
				"For n\u003e=1 this is the sequence A000538(n)."
			],
			"mathematica": [
				"row[k_] := Numerator[ Rest[ CoefficientList[ HarmonicNumber[n, -k], n]]]; Flatten[ Table[ row[k], {k, 0, 10}]] (* _Jean-François Alcover_, Dec 07 2011 *)"
			],
			"xref": [
				"A103438, A196837."
			],
			"keyword": "sign,easy,tabl",
			"offset": "0,19",
			"author": "_Wolfdieter Lang_, Oct 23 2011",
			"references": 0,
			"revision": 19,
			"time": "2019-05-28T19:38:02-04:00",
			"created": "2011-10-24T14:34:54-04:00"
		}
	]
}