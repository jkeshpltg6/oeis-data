{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A326411",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 326411,
			"data": "1,0,1,0,0,1,0,0,0,1,0,0,2,0,1,1,0,5,5,0,1,3,12,15,20,9,0,1,23,70,112,91,49,14,0,1,177,544,740,640,302,96,20,0,1,1553,4500,6003,4725,2439,747,165,27,0,1,14963,41740,53585,41420,20810,7076,1550,260,35,0,1",
			"name": "Triangle T(n,k) read by rows: T(n,k) = the number of ways of seating n people around a table for the second time so that k pairs are maintained. Reflected and rotated sequences are counted as one.",
			"comment": [
				"Definition requires \"pairs\" and for n=0 it is assumed that there is 1 way of seating 0 people around a table for the second time so that 0 pairs are maintained and 1 person forms only one pair with him/herself. Therefore T(0,0)=1, T(1,0)=0 and T(1,1)=1.",
				"Weighted average of each row using k as weights converges to 2 for large n and is given with following formula: (Sum_{k} T(n,k)*k)/n! = 2/(n-1) + 2 (conjectured)."
			],
			"link": [
				"Witold Tatkiewicz, \u003ca href=\"/A326411/b326411.txt\"\u003eRows n = 0..17 of triangle, flattened\u003c/a\u003e",
				"Witold Tatkiewicz, \u003ca href=\"https://pastebin.com/ZrBYiK9g\"\u003elink for java program\u003c/a\u003e."
			],
			"formula": [
				"T(n,n) = 1;",
				"T(n,n-1) = 0 for n \u003e 1;",
				"T(n,n-2) = (1/2)*n^2 + (1/2)n - 1 for n \u003e 3 (conjectured);",
				"T(n,n-3) = (2/3)*n^3 + n^2 - 8/3*n + 1 for n \u003e 4 (conjectured);",
				"T(n,n-4) = (25/24)*n^4 + (23/12)*n^3 - (169/24)*n^2 + (85/12)*n - 3 for n \u003e 5 (conjectured);",
				"T(n,n-5) = (26/15)*n^5 + (25/6)*n^4 - (83/6)*n^3 + (221/6)*n^2 - (299/10)*n + 13 for n \u003e 5 (conjectured);",
				"T(n,n-6) = (707/240)*n^6 + (2037/240)*n^5 - (413/16)*n^4 + (2233/16)*n^3 - (2777/15)*n^2 + (3739/20)*n - 57 for n \u003e 6 (conjectured)."
			],
			"example": [
				"Assuming the initial order was {1,2,3,4,5} (therefore 1 and 5 form a pair as the first and last persons are neighbors in the case of a round table) there are 5 sets of ways of seating them again so that 3 pairs are conserved: {1,2,3,5,4}, {2,3,4,1,5}, {3,4,5,2,1}, {4,5,1,3,2}, {5,1,2,4,3}. Since within each set we do not allow for circular symmetry (e.g., {1,2,3,5,4} and its rotation to form {2,3,5,4,1} are counted as one) nor reflection ({1,2,3,5,4} and {4,5,3,2,1} are also counted as one), the total number of ways is 5 and therefore T(5,3)=5.",
				"Unfolded table with n individuals (rows) forming k pairs (columns):",
				"    0    1    2    3    4    5    6    7",
				"0   1",
				"1   0    1",
				"2   0    0    1",
				"3   0    0    0    1",
				"4   0    0    2    0    1",
				"5   1    0    5    5    0    1",
				"6   3   12   15   20    9    0   1",
				"7  23   70  112   91   49   14   0   1"
			],
			"program": [
				"(Java) See Links section"
			],
			"xref": [
				"Cf. A002816 (column k=0).",
				"Cf. A001710(n-1)=Sum_k{T(n,k)} - sum of row n-th row for n\u003e0",
				"Cf. A326390 (accounting for rotation and reflection symmetry), A326397 (disregards reflection symmetry but allows rotation), A326407 (disregards rotation symmetry but allows reflection)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,13",
			"author": "_Witold Tatkiewicz_, Aug 07 2019",
			"references": 4,
			"revision": 9,
			"time": "2019-09-24T09:18:34-04:00",
			"created": "2019-09-21T15:06:58-04:00"
		}
	]
}