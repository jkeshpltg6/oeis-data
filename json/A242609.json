{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A242609",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 242609,
			"data": "1,-2,0,0,2,0,0,0,2,-6,0,0,4,0,0,0,2,-4,0,0,0,0,0,0,4,-2,0,0,0,0,0,0,2,-8,0,0,6,0,0,0,0,-4,0,0,4,0,0,0,4,-2,0,0,0,0,0,0,0,-8,0,0,0,0,0,0,2,0,0,0,4,0,0,0,6,-4,0,0,4,0,0,0,0,-10,0",
			"name": "Expansion of phi(-q) * phi(q^8) in powers of q where phi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A242609/b242609.txt\"\u003eTable of n, a(n) for n = 0..2500\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of eta(q)^2 * eta(q^16)^5 / (eta(q^2) * eta(q^8)^2 * eta(q^32)^2) in powers of q.",
				"G.f.: (Sum_{k in Z} (-x)^k^2) * (Sum_{k in Z} (x^8)^k^2).",
				"a(4*n + 2) = a(4*n + 3) = a(8*n + 5) = 0.  a(4*n) = a(8*n) = A033715(n). a(8*n + 1) = -2 * A112603(n). a(8*n + 4) = 2 * A113411(n).",
				"a(n) = (-1)^n * A226225(n)."
			],
			"example": [
				"G.f. = 1 - 2*q + 2*q^4 + 2*q^8 - 6*q^9 + 4*q^12 + 2*q^16 - 4*q^17 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 4, 0, q] EllipticTheta[ 3, 0, q^8], {q, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n==0, 2 * (-1)^n * (n%4 \u003c 2) * sumdiv( n, d, kronecker( -2, d)))};",
				"(PARI) {a(n) = local(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A)^2 * eta(x^16 + A)^5 / (eta(x^2 + A) * eta(x^8 + A)^2 * eta(x^32 + A)^2), n))};"
			],
			"xref": [
				"Cf. A033715, A112603, A113411, A226225."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, May 19 2014",
			"references": 3,
			"revision": 12,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2014-05-19T01:47:44-04:00"
		}
	]
}