{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A268438",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 268438,
			"data": "1,0,1,0,8,6,0,180,240,90,0,8064,14560,10080,2520,0,604800,1330560,1285200,604800,113400,0,68428800,173638080,209341440,139708800,49896000,7484400,0,10897286400,30858347520,43770767040,36970053120,18918900000,5448643200,681080400",
			"name": "Triangle read by rows, T(n,k) = (-1)^k*(2*n)!*P[n,k](n/(n+1)) where P is the P-transform, for n\u003e=0 and 0\u003c=k\u003c=n.",
			"comment": [
				"The P-transform is defined in the link. Compare also the Sage and Maple implementations below."
			],
			"link": [
				"Peter Luschny, \u003ca href=\"http://oeis.org/wiki/User:Peter_Luschny/P-Transform\"\u003eThe P-transform\u003c/a\u003e."
			],
			"formula": [
				"T(n,k) = ((2*n)!/FF(n+k,n))*Sum_{m=0..k}(-1)^(m+k)*C(n+k,n+m)*Stirling1(n+m,m) where FF denotes the falling factorial function.",
				"T(n,k) = ((2*n)!/FF(n+k,n))*A269940(n,k).",
				"T(n,1) = (2*n)!/(n+1) = A060593(n) for n\u003e=1.",
				"T(n,n) = (2*n)!/2^n = A000680(n) for n\u003e=0."
			],
			"example": [
				"Triangle starts:",
				"[1],",
				"[0, 1],",
				"[0, 8,        6],",
				"[0, 180,      240,       90],",
				"[0, 8064,     14560,     10080,     2520],",
				"[0, 604800,   1330560,   1285200,   604800,    113400],",
				"[0, 68428800, 173638080, 209341440, 139708800, 49896000, 7484400]."
			],
			"maple": [
				"A268438 := proc(n,k) local F,T;",
				"  F := proc(n,k) option remember;",
				"  `if`(n=0 and k=0, 1,`if`(n=k, (4*n-2)*F(n-1,k-1),",
				"  F(n-1,k)*(n+k))) end;",
				"  T := proc(n, k) option remember;",
				"  `if`(k=0 and n=0, 1,`if`(k\u003c=0 or k\u003en, 0,",
				"  (4*n-2)*n*(n+k-1)*(T(n-1,k)+T(n-1,k-1)))) end:",
				"T(n,k)/F(n,k) end:",
				"for n from 0 to 6 do seq(A268438(n,k), k=0..n) od;",
				"# Alternatively, with the function PTrans defined in A269941:",
				"A268438_row := n -\u003e PTrans(n, n-\u003en/(n+1),(n,k)-\u003e(-1)^k*(2*n)!):",
				"seq(lprint(A268438_row(n)), n=0..8);"
			],
			"mathematica": [
				"T[n_, k_] := (2n)!/FactorialPower[n+k, n] Sum[(-1)^(m+k) Binomial[n+k, n+m] Abs[StirlingS1[n+m, m]], {m, 0, k}];",
				"Table[T[n, k], {n, 0, 7}, {k, 0, n}] (* _Jean-François Alcover_, Jun 15 2019 *)"
			],
			"program": [
				"(Sage)",
				"A268438 = lambda n,k: (factorial(2*n)/falling_factorial(n+k,n))*sum((-1)^(m+k)* binomial(n+k,n+m)*stirling_number1(n+m,m) for m in (0..k))",
				"for n in (0..7): print([A268438(n,m) for m in (0..n)])",
				"(Sage) # uses[PtransMatrix from A269941]",
				"PtransMatrix(7, lambda n: n/(n+1), lambda n,k: (-1)^k*factorial(2*n))"
			],
			"xref": [
				"Cf. A000680, A060593, A268437, A269940, A269941."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Peter Luschny_, Mar 07 2016",
			"references": 6,
			"revision": 27,
			"time": "2020-03-27T08:49:47-04:00",
			"created": "2016-03-29T03:44:44-04:00"
		}
	]
}