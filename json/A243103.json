{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A243103",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 243103,
			"data": "1,2,3,8,5,144,7,64,27,3200,11,124416,13,6272,2025,1024,17,35831808,19,1024000,3969,247808,23,859963392,125,346112,729,2809856,29,261213880320000000,31,32768,264627,18939904,30625,26748301344768,37,23658496,369603,32768000000,41",
			"name": "Product of numbers m with 2 \u003c= m \u003c= n whose prime divisors all divide n.",
			"comment": [
				"This sequence is the product of n-regular numbers.",
				"A number m is said to be \"regular\" to n or \"n-regular\" if all the prime factors p of m also divide n.",
				"The divisor is a special case of a regular m such that m also divides n in addition to all of its prime factors p | n.",
				"Analogous to A007955 (Product of divisors of n).",
				"If n is 1 or prime, a(n) = n.",
				"If n is a prime power, a(n) = A007955(n).",
				"Note: b-file ends at n = 4619, because a(4620) has more than 1000 decimal digits.",
				"Product of the numbers 1 \u003c= k \u003c= n such that (floor(n^k/k) - floor((n^k - 1)/k)) = 1. - _Michael De Vlieger_, May 26 2016"
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A243103/b243103.txt\"\u003eTable of n, a(n) for n = 1..4619\u003c/a\u003e",
				"Encyclopedia Britannica, \u003ca href=\"http://www.britannica.com/EBchecked/topic/496213/regular-number\"\u003eRegular Number\u003c/a\u003e (base-neutral definition)",
				"Eric W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/RegularNumber.html\"\u003eRegular Number\u003c/a\u003e (decimal definition)",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Regular_number\"\u003eRegular Number\u003c/a\u003e (sexagesimal / Hamming number definition)"
			],
			"formula": [
				"a(n) = product of terms of n-th row of irregular triangle A162306(n,k).",
				"a(n) = Product_{k=1..n} k^( floor(n^k/k)-floor((n^k -1)/k) ). - _Anthony Browne_, Jul 06 2016",
				"From _Antti Karttunen_, Mar 22 2017: (Start)",
				"a(n) = Product_{k=2..n, A123275(n,k)=1} k.",
				"For n \u003e= 1, A046523(a(n)) = A283990(n).",
				"(End)"
			],
			"example": [
				"a(12) = 124416 since 1 * 2 * 3 * 4 * 6 * 8 * 9 * 12 = 124416. These numbers are products of prime factors that are the distinct prime divisors of 12 = {2, 3}.",
				"From _David A. Corneth_, Feb 09 2015: (Start)",
				"Let p# be the product of primes up to p, A002110. Then",
				"a(13#) ~= 8.3069582 * 10 ^ 4133",
				"a(17#) ~= 1.3953000 * 10 ^ 22689",
				"a(19#) ~= 3.8258936 * 10 ^ 117373",
				"a(23#) ~= 6.7960327 * 10 ^ 594048",
				"a(29#) ~= 1.3276817 * 10 ^ 2983168",
				"a(31#) ~= 2.8152792 * 10 ^ 14493041",
				"a(37#) ~= 1.9753840 * 10 ^ 69927040",
				"Up to n = 11# already in the table.",
				"(End)"
			],
			"maple": [
				"A:= proc(n) local F, S, s, j, p;",
				"  F:= numtheory:-factorset(n);",
				"  S:= {1};",
				"  for p in F do",
				"    S:= {seq(seq(s*p^j, j=0..floor(log[p](n/s))), s=S)}",
				"  od;",
				"  convert(S,`*`)",
				"end proc:",
				"seq(A(n), n=1..100); # _Robert Israel_, Feb 09 2015"
			],
			"mathematica": [
				"regularQ[m_Integer, n_Integer] := Module[{omega = First /@ FactorInteger @ m }, If[Length[Select[omega, Divisible[n, #] \u0026]] == Length[omega], True, False]]; a20140819[n_Integer] := Times @@ Flatten[Position[Thread[regularQ[Range[1, n], n]], True]]; a20140819 /@ Range[41]",
				"regulars[n_] := Block[{f, a}, f[x_] := First /@ FactorInteger@ x; a = f[n];{1}~Join~Select[Range@ n, SubsetQ[a, f@ #] \u0026]]; Array[Times @@ regulars@ # \u0026, 12] (* _Michael De Vlieger_, Feb 09 2015 *)",
				"Table[Times @@ Select[Range@ n, (Floor[n^#/#] - Floor[(n^# - 1)/#]) == 1 \u0026], {n, 41}] (* _Michael De Vlieger_, May 26 2016 *)"
			],
			"program": [
				"(PARI) lista(nn) = {vf = vector(nn, n, Set(factor(n)[,1])); vector(nn, n, prod(i=1, n, if (setintersect(vf[i], vf[n]) == vf[i], i, 1)));} \\\\ _Michel Marcus_, Aug 23 2014",
				"(PARI) for(n=1, 100, print1(prod(k=1, n, k^(floor(n^k/k) - floor((n^k - 1)/k))),\", \")) \\\\ _Indranil Ghosh_, Mar 22 2017",
				"(Python)",
				"from sympy import primefactors",
				"def A243103(n):",
				"    y, pf = 1, set(primefactors(n))",
				"    for m in range(2,n+1):",
				"        if set(primefactors(m)) \u003c= pf:",
				"            y *= m",
				"    return y # _Chai Wah Wu_, Aug 28 2014",
				"(Scheme)",
				";; A naive implementation, code for A123275bi given under A123275:",
				"(define (A243103 n) (let loop ((k n) (m 1)) (cond ((= 1 k) m) ((= 1 (A123275bi n k)) (loop (- k 1) (* m k))) (else (loop (- k 1) m)))))",
				";; _Antti Karttunen_, Mar 22 2017"
			],
			"xref": [
				"Cf. A162306 (irregular triangle of regular numbers of n), A010846 (number of regular numbers of n), A244974 (sum of regular numbers of n), A007955, A244052 (record transform of regular numbers of n).",
				"Cf. A123275, A283990."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Michael De Vlieger_, Aug 19 2014",
			"references": 8,
			"revision": 68,
			"time": "2017-03-23T11:43:11-04:00",
			"created": "2014-08-28T02:36:20-04:00"
		}
	]
}