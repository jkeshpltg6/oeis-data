{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A258173",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 258173,
			"data": "1,1,3,12,58,321,1975,13265,96073,743753,6113769,53086314,484861924,4641853003,46441475253,484327870652,5252981412262,59132909030463,689642443691329,8319172260103292,103645882500123026,1331832693574410475,17629142345935969713",
			"name": "Sum over all Dyck paths of semilength n of products over all peaks p of y_p, where y_p is the y-coordinate of peak p.",
			"comment": [
				"A Dyck path of semilength n is a (x,y)-lattice path from (0,0) to (2n,0) that does not go below the x-axis and consists of steps U=(1,1) and D=(1,-1). A peak of a Dyck path is any lattice point visited between two consecutive steps UD.",
				"Number of general rooted ordered trees with n edges and \"back edges\", which are additional edges connecting vertices to their ancestors. Every vertex specifies an ordering on the edges to its children and back edges to its ancestors altogether; it may be connected to the same ancestor by multiple back edges, distinguishable only by their relative ordering under that vertex. - _Li-yao Xia_, Mar 06 2017"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A258173/b258173.txt\"\u003eTable of n, a(n) for n = 0..500\u003c/a\u003e",
				"Antti Karttunen, \u003ca href=\"http://list.seqfan.eu/pipermail/seqfan/2017-March/017314.html\"\u003eBijection between rooted trees with back edges and Dyck paths with multiplicity\u003c/a\u003e, SeqFans mailing list, Mar 2 2017.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Lattice_path\"\u003eLattice path\u003c/a\u003e",
				"Li-yao Xia, \u003ca href=\"http://blog.poisson.chat/posts/2017-03-01-enumerating-dfs.html\"\u003eDefinition and enumeration of rooted trees with back edges in Haskell\u003c/a\u003e, blog post, Mar 1 2017."
			],
			"formula": [
				"G.f.: T(0), where T(k) = 1 - x/(k*x + 2*x - 1/T(k+1) ); (continued fraction). - _Sergei N. Gladkovskii_, Aug 20 2015"
			],
			"maple": [
				"b:= proc(x, y, t) option remember; `if`(y\u003ex or y\u003c0, 0,",
				"      `if`(x=0, 1, b(x-1, y-1, false)*`if`(t, y, 1) +",
				"                   b(x-1, y+1, true)  ))",
				"    end:",
				"a:= n-\u003e b(2*n, 0, false):",
				"seq(a(n), n=0..25);"
			],
			"mathematica": [
				"nmax = 25; Clear[g]; g[nmax+1] = 1; g[k_] := g[k] = 1 - x/(k*x + 2*x - 1/g[k+1]); CoefficientList[Series[g[0], {x, 0, nmax}], x] (* _Vaclav Kotesovec_, Aug 20 2015, after _Sergei N. Gladkovskii_ *)"
			],
			"xref": [
				"Cf. A000108, A000698, A005411, A005412, A258172, A258174, A258175, A258176, A258177, A258178, A258179, A258180, A258181."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Alois P. Heinz_, May 22 2015",
			"references": 12,
			"revision": 23,
			"time": "2017-03-12T12:57:52-04:00",
			"created": "2015-05-23T10:33:28-04:00"
		}
	]
}