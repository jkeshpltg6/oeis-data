{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A304972",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 304972,
			"data": "1,1,1,1,1,1,1,3,2,1,1,3,5,2,1,1,7,10,9,3,1,1,7,19,16,12,3,1,1,15,38,53,34,18,4,1,1,15,65,90,95,46,22,4,1,1,31,130,265,261,195,80,30,5,1,1,31,211,440,630,461,295,100,35,5,1,1,63,422,1221,1700,1696,1016,515,155,45,6,1,1,63,665,2002",
			"name": "Triangle read by rows of achiral color patterns (set partitions) for a row or loop of length n. T(n,k) is the number using exactly k colors (sets).",
			"comment": [
				"Two color patterns are equivalent if we permute the colors. Achiral color patterns must be equivalent if we reverse the order of the pattern."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A304972/b304972.txt\"\u003eTable of n, a(n) for n = 1..1275\u003c/a\u003e",
				"Ira Gessel, \u003ca href=\"https://mathoverflow.net/q/291720\"\u003eWhat is the number of achiral color patterns for a row of n colors containing k different colors?\u003c/a\u003e, mathoverflow, Jan 30 2018."
			],
			"formula": [
				"T(n,k) = [n\u003e1] * (k*T(n-2,k) + T(n-2,k-1) + T(n-2,k-2)) + [n\u003c2 \u0026 n==k \u0026 n\u003e=0].",
				"T(2m-1,k) = A140735(m,k).",
				"T(2m,k) = A293181(m,k).",
				"T(n,k) = [k==0 \u0026 n==0] + [k==1 \u0026 n\u003e0]",
				"  + [k\u003e1 \u0026 n==1 mod 2] * Sum_{i=0..(n-1)/2} (C((n-1)/2, i) * T(n-1-2i, k-1))",
				"  + [k\u003e1 \u0026 n==0 mod 2] * Sum_{i=0..(n-2)/2} (C((n-2)/2, i) * (T(n-2-2i, k-1)",
				"  + 2^i * T(n-2-2i, k-2))) where C(n,k) is a binomial coefficient."
			],
			"example": [
				"Triangle begins:",
				"1;",
				"1,   1;",
				"1,   1,    1;",
				"1,   3,    2,    1;",
				"1,   3,    5,    2,     1;",
				"1,   7,   10,    9,     3,     1;",
				"1,   7,   19,   16,    12,     3,     1;",
				"1,  15,   38,   53,    34,    18,     4,    1;",
				"1,  15,   65,   90,    95,    46,    22,    4,    1;",
				"1,  31,  130,  265,   261,   195,    80,   30,    5,    1;",
				"1,  31,  211,  440,   630,   461,   295,  100,   35,    5,   1;",
				"1,  63,  422, 1221,  1700,  1696,  1016,  515,  155,   45,   6,  1",
				"1,  63,  665, 2002,  3801,  3836,  3156, 1556,  710,  185,  51,  6, 1;",
				"1, 127, 1330, 5369, 10143, 13097, 10508, 6832, 2926, 1120, 266, 63, 7, 1;",
				"For T(4,2)=3, the row patterns are AABB, ABAB, and ABBA.  The loop patterns are AAAB, AABB, and ABAB.",
				"For T(5,3)=5, the color patterns for both rows and loops are AABCC, ABACA, ABBBC, ABCAB, and ABCBA."
			],
			"mathematica": [
				"Ach[n_, k_] := Ach[n, k] = If[n \u003c 2, Boole[n == k \u0026\u0026 n \u003e= 0],",
				"  k Ach[n - 2, k] + Ach[n - 2, k - 1] + Ach[n - 2, k - 2]]",
				"Table[Ach[n, k], {n, 1, 15}, {k, 1, n}] // Flatten",
				"Ach[n_, k_] := Ach[n, k] = Which[0==k, Boole[0==n], 1==k, Boole[n\u003e0],",
				"  OddQ[n], Sum[Binomial[(n-1)/2, i] Ach[n-1-2i, k-1], {i, 0, (n-1)/2}],",
				"  True, Sum[Binomial[n/2-1, i] (Ach[n-2-2i, k-1]",
				"  + 2^i Ach[n-2-2i, k-2]), {i, 0, n/2-1}]]",
				"Table[Ach[n, k], {n, 1, 15}, {k, 1, n}] // Flatten"
			],
			"program": [
				"(PARI)",
				"Ach(n)={my(M=matrix(n,n,i,k,i\u003e=k)); for(i=3, n, for(k=2, n, M[i,k]=k*M[i-2,k] + M[i-2,k-1] + if(k\u003e2, M[i-2,k-2]))); M}",
				"{ my(A=Ach(10)); for(n=1, #A, print(A[n,1..n])) } \\\\ _Andrew Howroyd_, Sep 18 2019"
			],
			"xref": [
				"Columns 1-6 are A057427, A052551(n-2), A304973, A304974, A304975, A304976.",
				"A305008 has coefficients that determine the function and generating function for each column.",
				"Row sums are A080107."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "1,8",
			"author": "_Robert A. Russell_, May 22 2018",
			"references": 47,
			"revision": 25,
			"time": "2019-09-19T15:24:12-04:00",
			"created": "2018-06-28T03:14:40-04:00"
		}
	]
}