{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A116217",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 116217,
			"data": "2,3,2,4,7,8,4,7,7,2,8,4,0,4,7,9,0,6,1,2,3,5,2,1,7,6,8,2,8,6,1,3,9,3,0,4,6,0,2,0,9,5,1,3,4,5,2,2,5,4,7,6,0,5,3,6,0,1,4,6,9,4,6,4,4,4,1,9,2,2,0,2,0,0,4,6,3,9,7,7,0,3,1,7,3,6,9,8,8,4,0,1,5,1,2,7,7,2,8,2,6,8,8,3,1",
			"name": "Decimal expansion of constant Sum_{i,j,k=1..inf} 1/2^(i*j*k).",
			"comment": [
				"This constant is a sum of triple series Sum[Sum[Sum[1/2^(i*j*k),{i,1,Infinity}],{j,1,Infinity}],{k,1,Infinity}] = 2.3247847... It is similar to Erdos-Borwein constant Sum[Sum[1/2^(i*j),{i,1,Infinity}],{j,1,Infinity}] = Sum[1/(2^k-1),{k,1,Infinity}] = 1.60669515..."
			],
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/TripleSeries.html\"\u003eTriple Series\u003c/a\u003e."
			],
			"formula": [
				"Equals Sum_{n=1..infinity} A007425(n)/2^n . - _R. J. Mathar_, Jan 23 2008",
				"From _Amiram Eldar_, Aug 10 2020: (Start)",
				"Equals Sum{k\u003e=1} d(k)/(2^k - 1), where d(k) is the number of divisors of k (A000005).",
				"Equals Sum_{i,j=1..oo} 1/(2^(i*j) - 1). (End)"
			],
			"example": [
				"2.32478477284047906123521768286139304602095134522547605..."
			],
			"mathematica": [
				"digits = 105; Clear[s]; s[n_] := s[n] = 2*NSum[1/(2^(j*k) - 1), {j, 1, n}, {k, 1, j-1}, WorkingPrecision -\u003e digits+10, NSumTerms -\u003e 100] + NSum[1/(2^j^2 - 1), {j, 1, n}, WorkingPrecision -\u003e digits+10, NSumTerms -\u003e 100] // RealDigits[#, 10, digits]\u0026 // First; s[n=100]; While[s[n] != s[n-100], n = n+100]; s[n] (* _Jean-François Alcover_, Feb 13 2013 *)"
			],
			"program": [
				"(PARI): /* Using sum(n=1..infinity, A007425(n)/2^n )  */",
				"lambert2ser(L)=",
				"{",
				"    local(n, t);",
				"    n = length(L);",
				"    t = sum(k=1, length(L), O('x^(n+1))+L[k]*'x^k/(1-'x^k) );",
				"    t = Vec(t);",
				"    return( t );",
				"}",
				"N=1000; v=vector(N,n,1); /* roughly 1000 bits precision */",
				"t=lambert2ser(lambert2ser(v)); /* ==[1, 3, 3, 6, 3, 9,...] == A007425 */",
				"default(realprecision,floor(N/3.4)); /* factor approx. log(10)/log(2) */",
				"sum(n=1,#v,1.0*t[n]/2^n)",
				"/* == 2.324784772840479061235217682861... */"
			],
			"xref": [
				"Cf. A065442 = Decimal expansion of Erdos-Borwein constant Sum_{k=1..inf} 1/(2^k-1).",
				"Cf. A000005, A007425."
			],
			"keyword": "cons,nonn",
			"offset": "1,1",
			"author": "_Alexander Adamchuk_, Apr 09 2007",
			"ext": [
				"More terms from _R. J. Mathar_, Jan 23 2008",
				"More terms from _Jean-François Alcover_, Feb 13 2013"
			],
			"references": 0,
			"revision": 20,
			"time": "2020-08-10T09:27:57-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}