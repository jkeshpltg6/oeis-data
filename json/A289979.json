{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A289979",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 289979,
			"data": "1,2,3,4,5,6,7,8,9,86,86,42,86,20,42,53,86,108,20,110,222,110,31,222,310,110,288,31,97,75,154,64,75,692,154,468,64,176,75,389,367,132,187,389,648,367,209,132,211,1772,411,446,1715,828,1772,7150,411,413",
			"name": "Define two sequences n1(i) and n2(i) by the recurrences n1(i) = n1(i-1) + digsum(n2(i-1)), n2(i) = n2(i-1) + digsum(n1(i-1)), with initial values n1(1) = n and n2(1) = 0. Then a(n) is the smallest m such that n1(i) = n2(i) = m for some i, or -1 if no such m exists.",
			"comment": [
				"The function is like a chase that ends when n1(i) = n2(i). For example, when n = 14:",
				"n1(1) = 14, n2(1) = 0",
				"n1(2) = 14 = 14 + digsum(0), n2(2) = 5 = 0 + digsum(14)",
				"n1(3) = 19 = 14 + digsum(5), n2(3) = 10 = 5 + digsum(14)",
				"n1(4) = 20 = 19 + digsum(10), n2(4) = 20 = 10 + digsum(19)",
				"Because n1 = n2 = 20, the chase ends and a(14) = 20. When n = 81, a(81) \u003e 10^8 and the chase may never end. In other bases, some different number first produces a prolonged chase with no result. E.g., in base 9, the number is 64 = 71 (b9); in base 12, the number is 110 = 92 (b12). In base 2, when n = 178, n1 = n2 = 6181 and when n = 179, n1 = n2 = 267684506.",
				"If a(81) exists, it is larger than 5*10^14. - _Giovanni Resta_, Jul 21 2017"
			],
			"link": [
				"Anthony Sand, \u003ca href=\"/A289979/b289979.txt\"\u003eTable of n, a(n) for n = 1..80\u003c/a\u003e"
			],
			"formula": [
				"n1(1) = n, n2(1) = 0, then n1(i) = n1(i-1) + digsum(n2(i-1)), n2(i) = n2(i-1) + digsum(n1(i-1)) until n1(i) = n2(i)."
			],
			"example": [
				"n1(1) = 12, n2(1) = 0",
				"n1(2) = 12 = 12 + digsum(0), n2(2) = 3 = 0 + digsum(12)",
				"n1(3) = 15 = 12 + digsum(3), n2(3) = 6 = 3 + digsum(12)",
				"n1(4) = 21 = 15 + digsum(6), n2(4) = 12 = 6 + digsum(15)",
				"n1(5) = 24 = 21 + 3, n2(5) = 15 = 12 + 3",
				"n1(6) = 30 = 24 + 6, n2(6) = 21 = 15 + 6",
				"n1(7) = 33 = 30 + 3, n2(7) = 24 = 21 + 3",
				"n1(8) = 39 = 33 + 6, n2(8) = 30 = 24 + 6",
				"n1(9) = 42 = 39 + 3, n2(9) = 42 = 30 + 12"
			],
			"mathematica": [
				"Table[NestWhileList[{#1 + Total@ IntegerDigits[#2], #2 + Total@ IntegerDigits[#1]} \u0026 @@ # \u0026, {n, 0}, UnsameQ @@ # \u0026, 1, 10^4][[-1, -1]], {n, 80}] (* _Michael De Vlieger_, Jul 17 2017 *)"
			],
			"xref": [
				"Cf. A004207."
			],
			"keyword": "nonn,base,more",
			"offset": "1,2",
			"author": "_Anthony Sand_, Jul 17 2017",
			"references": 1,
			"revision": 27,
			"time": "2017-07-22T08:30:30-04:00",
			"created": "2017-07-21T09:30:22-04:00"
		}
	]
}