{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A074324",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 74324,
			"data": "1,1,4,3,12,9,36,27,108,81,324,243,972,729,2916,2187,8748,6561,26244,19683,78732,59049,236196,177147,708588,531441,2125764,1594323,6377292,4782969,19131876,14348907,57395628,43046721,172186884,129140163",
			"name": "a(2n+1) = 3^n, a(2n) = 4*3^(n-1) except for a(0) = 1.",
			"comment": [
				"Also: Coefficient of the highest power of q in the expansion of nu(0)=1, nu(1)=b and for n\u003e=2, nu(n)=b*nu(n-1)+lambda*(n-1)_q*nu(n-2) with (b,lambda)=(1,3), where (n)_q=(1+q+...+q^(n-1)) and q is a root of unity.",
				"Instead of listing the coefficients of the highest power of q in each nu(n), if we list the coefficients of the smallest power of q (i.e., constant terms), we get a weighted Fibonacci numbers described by f(0)=1, f(1)=1, for n\u003e=2, f(n)=f(n-1)+3f(n-2).",
				"Sequences A162766, A166552 are essentially the same. - _M. F. Hasler_, Dec 03 2014"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A074324/b074324.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"M. Beattie, S. Dăscălescu and S. Raianu, \u003ca href=\"https://arxiv.org/abs/math/0204075\"\u003eLifting of Nichols Algebras of Type B_2\u003c/a\u003e, arXiv:math/0204075 [math.QA], 2002.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,3)."
			],
			"formula": [
				"For given b and lambda, the recurrence relation is given by; t(0)=1, t(1)=b, t(2)=b^2+lambda and for n\u003e=3, t(n) = lambda*t(n-2).",
				"G.f.: -(1+x+x^2)/(-1+3*x^2). - _R. J. Mathar_, Dec 05 2007",
				"a(n) = 3*a(n-2) for n\u003e2. - _Ralf Stephan_, Jul 19 2013",
				"a(n) = (1/6)*(7+(-1)^n)*3^floor(n/2) for n\u003e0. - _Ralf Stephan_, Jul 19 2013"
			],
			"example": [
				"nu(0)=1;",
				"nu(1)=1;",
				"nu(2)=4;",
				"nu(3)=7+3q;",
				"nu(4)=19+15q+12q^2;",
				"nu(5)=40+45q+42q^2+30q^3+9q^4;",
				"nu(6)=97+147q+180q^2+168q^3+147q^4+81q^5+36q^6;",
				"by listing the coefficients of the highest power in each nu(n), we get, 1,1,4,3,12,9,36,...."
			],
			"mathematica": [
				"CoefficientList[Series[-(1 + x + x^2) / (-1 + 3 x^2), {x, 0, 40}], x] (* _Vincenzo Librandi_, Jul 20 2013 *)",
				"LinearRecurrence[{0,3},{1,1,4},40] (* _Harvey P. Dale_, Mar 13 2016 *)"
			],
			"program": [
				"(MAGMA) [1] cat [(1/6)*(7+(-1)^n)*3^Floor(n/2):n in [1..40]]; // _Vincenzo Librandi_, Jul 20 2013",
				"(PARI) a(n)=3^(n\\2)\\(3/4)^!bittest(n,0) \\\\ _M. F. Hasler_, Dec 03 2014"
			],
			"xref": [
				"Cf. A006130."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "Y. Kelly Itakura (yitkr(AT)mta.ca), Aug 21 2002",
			"ext": [
				"More terms from _R. J. Mathar_, Dec 05 2007",
				"Simpler definition from _M. F. Hasler_, Dec 03 2014"
			],
			"references": 5,
			"revision": 40,
			"time": "2016-10-06T14:17:49-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}