{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A033630",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 33630,
			"data": "1,1,1,1,1,1,2,1,1,1,1,1,3,1,1,1,1,1,3,1,2,1,1,1,6,1,1,1,2,1,4,1,1,1,1,1,8,1,1,1,4,1,3,1,1,1,1,1,11,1,1,1,1,1,4,1,3,1,1,1,35,1,1,1,1,1,3,1,1,1,1,1,32,1,1,1,1,1,2,1,7,1,1,1,26,1,1,1,2,1,24,1,1,1,1,1,22,1,1,1,3",
			"name": "Number of partitions of n into distinct divisors of n.",
			"link": [
				"T. D. Noe and Alois P. Heinz, \u003ca href=\"/A033630/b033630.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e (1000 terms from T. D. Noe)"
			],
			"formula": [
				"a(A005100(n)) = 1; a(A005835(n)) \u003e 1. - _Reinhard Zumkeller_, Mar 02 2007",
				"a(n) = f(n, n, 1) with f(n, m, k) = if k \u003c= m then f(n, m, k + 1) + f(n, m - k, k + 1)*0^(n mod k) else 0^m. - _Reinhard Zumkeller_, Dec 11 2009",
				"a(n) = [x^n] Product_{d|n} (1 + x^d). - _Ilya Gutkovskiy_, Jul 26 2017",
				"a(n) = 1 if n is deficient (A005100) or weird (A006037). a(n) = 2 if n is perfect (A000396). - _Alonso del Arte_, Sep 24 2017"
			],
			"example": [
				"a(12) = 3 because we have the partitions [12], [6, 4, 2], and [6, 3, 2, 1]."
			],
			"maple": [
				"with(numtheory): a:=proc(n) local div, g, gser: div:=divisors(n): g:=product(1+x^div[j],j=1..tau(n)): gser:=series(g,x=0,105): coeff(gser,x^n): end: seq(a(n),n=1..100); # _Emeric Deutsch_, Mar 30 2006",
				"# second Maple program:",
				"with(numtheory):",
				"a:= proc(n) local b, l; l:= sort([(divisors(n))[]]):",
				"      b:= proc(m, i) option remember; `if`(m=0, 1, `if`(i\u003c1, 0,",
				"             b(m, i-1)+`if`(l[i]\u003em, 0, b(m-l[i], i-1))))",
				"          end; forget(b):",
				"      b(n, nops(l))",
				"    end:",
				"seq(a(n), n=0..100); # _Alois P. Heinz_, Feb 05 2014"
			],
			"mathematica": [
				"A033630 = Table[SeriesCoefficient[Series[Times@@((1 + z^#) \u0026 /@ Divisors[n]), {z, 0, n}], n ], {n, 512}] (* _Wouter Meeussen_ *)",
				"A033630[n_] := f[n, n, 1]; f[n_, m_, k_] := f[n, m, k] = If[k \u003c= m, f[n, m, k + 1] + f[n, m - k, k + 1] * Boole[Mod[n, k] == 0], Boole[m == 0]]; Array[A033630, 101, 0] (* _Jean-François Alcover_, Jul 29 2015, after _Reinhard Zumkeller_ *)"
			],
			"program": [
				"(Haskell)",
				"a033630 0 = 1",
				"a033630 n = p (a027750_row n) n where",
				"   p _  0 = 1",
				"   p [] _ = 0",
				"   p (d:ds) m = if d \u003e m then 0 else p ds (m - d) + p ds m",
				"-- _Reinhard Zumkeller_, Feb 23 2014, Apr 04 2012, Oct 27 2011"
			],
			"xref": [
				"Cf. A018818.",
				"a(n) = A065205(n) + 1.",
				"Cf. A083206. - _Reinhard Zumkeller_, Jul 19 2010",
				"Cf. A000009, A005153.",
				"Cf. A211111, A027750.",
				"Cf. A225245."
			],
			"keyword": "nonn",
			"offset": "0,7",
			"author": "_Marc LeBrun_",
			"ext": [
				"More terms from _Reinhard Zumkeller_, Apr 21 2003"
			],
			"references": 64,
			"revision": 51,
			"time": "2017-09-26T09:32:49-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}