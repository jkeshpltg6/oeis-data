{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A052978",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 52978,
			"data": "1,2,10,40,172,728,3096,13152,55888,237472,1009056,4287616,18218688,77413760,328941952,1397720576,5939111168,25236118016,107231812096,455643039744,1936091311104,8226724075520,34956506765312,148535109967872,631146557100032",
			"name": "Expansion of (1-2*x)/(1-4*x-2*x^2+4*x^3).",
			"comment": [
				"a(n) = element(1,3) in A^(n+1), where A is the 5 X 5 matrix:",
				"[1, 1, 1, 1, 1]",
				"[1, 1, 0, 1, 1]",
				"[1, 0, 0, 0, 1]",
				"[1, 1, 0, 1, 1]",
				"[1, 1, 1, 1, 1]. - _Lechoslaw Ratajczak_, May 03 2017",
				"Also the number of matchings in the 2 X n king graph for n \u003e= 1. - _Eric W. Weisstein_, Oct 03 2017"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A052978/b052978.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=1050\"\u003eEncyclopedia of Combinatorial Structures 1050\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GridGraph.html\"\u003eGrid Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/IndependentEdgeSet.html\"\u003eIndependent Edge Set\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Matching.html\"\u003eMatching\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,2,-4)."
			],
			"formula": [
				"G.f.: (1-2*x)/(1-4*x-2*x^2+4*x^3).",
				"Recurrence: {a(0)=1, a(1)=2, a(2)=10, 4*a(n)-2*a(n+1)-4*a(n+2)+a(n+3)=0.}",
				"a(n) = Sum(-1/158*(-11-42*r+24*r^2)*r^(-1-n) where r=RootOf(1-4*_Z-2*_Z^2+4*_Z^3))"
			],
			"maple": [
				"spec := [S,{S=Sequence(Prod(Union(Sequence(Union(Z,Z)),Z),Union(Z,Z)))},unlabeled ]: seq(combstruct[count ](spec,size=n), n=0..20);"
			],
			"mathematica": [
				"LinearRecurrence[{4, 2, -4}, {1, 2, 10}, 40] (* _Vincenzo Librandi_, Jun 23 2012 *)",
				"Table[RootSum[4 - 2 # - 4 #^2 + #^3 \u0026, 30 #^n - 13 #^(n + 1) + 6 #^(n + 2) \u0026]/158, {n, 0, 20}] (* _Eric W. Weisstein_, Oct 03 2017 *)",
				"Table[RootSum[1 - 4 # - 2 #^2 + 4 #^3 \u0026, (11 + 42 # - 24 #^2)/#^(n + 1) \u0026]/158, {n, 0, 20}] (* _Eric W. Weisstein_, Oct 03 2017 *)",
				"CoefficientList[Series[(1 - 2 x)/(1 - 4 x - 2 x^2 + 4 x^3), {x, 0, 20}], x] (* _Eric W. Weisstein_, Oct 03 2017 *)"
			],
			"program": [
				"(MAGMA) I:=[1, 2, 10]; [n le 3 select I[n] else 4*Self(n-1)+2*Self(n-2)-4*Self(n-3): n in [1..30]]; // _Vincenzo Librandi_, Jun 23 2012",
				"(PARI) Vec((1-2*x)/(1-4*x-2*x^2+4*x^3) + O(x^30)) \\\\ _Michel Marcus_, May 06 2017"
			],
			"keyword": "easy,nonn",
			"offset": "0,2",
			"author": "encyclopedia(AT)pommard.inria.fr, Jan 25 2000",
			"references": 2,
			"revision": 38,
			"time": "2017-10-03T20:49:08-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}