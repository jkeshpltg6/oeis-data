{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A301937",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 301937,
			"data": "1,3,10,7,6,9,22,19,14,25,18,83,62,33,54,559,108,109,110,97,188,147,166,221,146,171,292,129,194,257,294,313,342,399,506,609,462,353,398,531,834,471,530,1153,9854,417,470,627,8758,9853,626,9225,18450,20609,23718",
			"name": "a(n) is the smallest number whose Collatz ('3x+1') trajectory crosses its initial value exactly n times.",
			"comment": [
				"Records: 1, 3, 10, 22, 25, 83, 559, 609, 834, 1153, 9854, 18450, 20609, 23718, 31142, 35090, 41586, 80294, 283262, 377681, 427762, 789305, 887954, 887964, 1403202, 1752022, ..., . - _Robert G. Wilson v_, May 06 2018"
			],
			"link": [
				"Jon E. Schoenfield, \u003ca href=\"/A301937/b301937.txt\"\u003eTable of n, a(n) for n = 0..185\u003c/a\u003e",
				"\u003ca href=\"/index/3#3x1\"\u003eIndex entries for sequences related to 3x+1 (or Collatz) problem\u003c/a\u003e"
			],
			"formula": [
				"a(n) = min{k : A304030(k) = n}.",
				"If the Collatz conjecture is true, then a(n) == n (mod 2) for all terms."
			],
			"example": [
				"The Collatz trajectory for k=3 is 3 -\u003e 10 -\u003e 5 -\u003e 16 -\u003e 8 -\u003e 4 -\u003e 2 -\u003e 1 crosses the threshold value of 3 on exactly one iteration: the iteration on which it moves from 4 to 2. No smaller value of k shares this property, so a(1) = 3.",
				"The Collatz trajectory for k=6 (see A304030) is nearly identical, containing, in order of appearance, the values 6, 3, 10, 5, 16, 8, 4, 2, 1; it crosses the threshold value of 6 on exactly 4 iterations (3 -\u003e 10, 10 -\u003e 5, 5 -\u003e 16, and 8 -\u003e 4). No smaller value of k shares this property, so a(4) = 6."
			],
			"mathematica": [
				"Collatz[n_] := NestWhileList[ If[ OddQ@#, 3# +1, #/2] \u0026, n, # \u003e 1 \u0026]; f[n_] := Block[{x = Length[ SplitBy[ Collatz@ n, # \u003c n +1 \u0026]] - 1}, If[ OddQ@ n \u0026\u0026 n \u003e 1, x - 1, x]]; t[_] := 0; k = 1; While[k \u003c 24000, If[ t[f[k]] == 0, t[f[k]] = k]; k++]; t@# \u0026 /@ Range@54 (* _Robert G. Wilson v_, May 05 2018 *)"
			],
			"program": [
				"(Magma) nMax:=54; a:=[0: n in [1..nMax]]; for k in [2..24000] do n:=0; t:=k; while t gt 1 do tPrev:=t; if IsEven(t) then t:=t div 2; else t:=3*t+1; end if; if (t-k)*(tPrev-k) lt 0 then n+:=1; end if; end while; if (n gt 0) and (n le nMax) then if a[n] eq 0 then a[n]:=k; end if; end if; end for; a;"
			],
			"xref": [
				"Cf. A006370, A070165, A304030."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Jon E. Schoenfield_, May 05 2018",
			"references": 2,
			"revision": 51,
			"time": "2020-04-03T07:55:41-04:00",
			"created": "2018-05-26T12:52:48-04:00"
		}
	]
}