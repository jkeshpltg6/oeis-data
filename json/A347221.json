{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A347221",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 347221,
			"data": "1,1,4,1,4,10,1,4,10,19,1,4,10,20,31,1,4,10,20,32,46,1,4,10,20,35,47,64,1,4,10,20,35,50,65,85,1,4,10,20,35,53,68,86,109,1,4,10,20,35,56,71,89,110,136,1,4,10,20,35,56,77,92,113,137,166",
			"name": "Square array T(s,t), s\u003e=0, t\u003e=0, read by downward antidiagonals: T(s,t) is the number of nonnegative ordered triples (a,b,c) satisfying a+b+c \u003c= s and a*b*c \u003c= t.",
			"comment": [
				"Based on the fact that there are only O(sqrt(t)) distinct integers in the set S = {floor(t/1), floor(t/2), ..., floor(t/t)} and using combinatorics by fixing the number (a) as minimum, we can easily calculate this function in O(sigma(a=1-\u003et^(1/3)) (log_2(floor(t/a)) + floor(t/a)^(1/2))).",
				"You can educe the complexity to O(sigma(a=1-\u003et^(1/3)) (log_2(floor(t/a)) + floor(t/a)^(1/3))) = O(t^(5/9)).",
				"There are many methods that are take O(t^(5/9)) steps, but not all are effective; some of them have lower complexity but use too many division and multiplication steps. Some of them use precalculation and caching to speed things up, but are not as effective as the division-free algorithm that Richard Sladkey described.",
				"It is known that for each (a), we only need a for loop of at most K = min((s-a)/2,sqrt(t/a)) iterations. If we can somehow calculate this in O(K^(1/2)) or even O(K^(1/3)) for each (a) then it is possible to calculate in O(t^(1/3)).",
				"We can also calculate T(s, t) in O(t^(1/3)) if this function can be calculated fast enough: g(l, r, a, t) = Sum_{p = l..r} floor(t/(a*p)) for all a such that 1 \u003c= a \u003c= t^(1/3).",
				"When s is small, we can try not fixing a as minimum and get O(s * t^(1/3)) complexity. You can also make the complexity not depend on O(f(t)) and get O(s^2) complexity and this can be further improved.",
				"When s = t, the values on the main diagonal, T(s, s), are approximately 1.5*s^2 (tested with 10^7 numbers s \u003c= 10^9 using a power regression algorithm).",
				"The current best known algorithm O(t^(5/9)) (with modulo for large result) can run for s \u003c= 10^18 and t \u003c= 10^12 under 1 second and constant memory (820ms on GNU C++17 7.3.0 codeforces and 310ms on C++14 ideone).",
				"As far as I know, the best complexity in theory is O(t^(2/3-c+o(1))) for small c \u003e 0. Computing this table faster is as hard as the prime counting function and/or the divisor summatory function."
			],
			"link": [
				"Vo Hoang Anh, \u003ca href=\"https://ideone.com/yMi8tu\"\u003eO(t^(5/9)) C++ Code\u003c/a\u003e",
				"Vo Hoang Anh, \u003ca href=\"https://ideone.com/mmoWxJ\"\u003ePower Regression C++ Code\u003c/a\u003e",
				"Codeforces, \u003ca href=\"https://codeforces.com/blog/entry/93981\"\u003eAlgorithm discussion\u003c/a\u003e",
				"MathStackExchange, \u003ca href=\"https://math.stackexchange.com/questions/4230187/faster-algorithm-for-counting-non-negative-tripplea-b-c-satisfied-a-b-c\"\u003eMath discussion\u003c/a\u003e",
				"D. H. J. Polymath, \u003ca href=\"https://arxiv.org/abs/1009.3956\"\u003eDeterministic methods to find primes\u003c/a\u003e, arXiv:1009.3956 [math.NT], 2012.",
				"Richard Sladkey, \u003ca href=\"https://arxiv.org/abs/1206.3369\"\u003eA Successive Approximation Algorithm for Computing the Divisor Summatory Function\u003c/a\u003e, arXiv:1206.3369 [math.NT], 2012."
			],
			"formula": [
				"T(s,t) = Sum_{a=0..s} Sum_{b=0..s-a} Sum_{c=0..s-a-b} [a*b*c\u003c=t], where [] is the Iverson bracket.",
				"T(n, n) = A347252(n).",
				"T(0, t) = A000012(t).",
				"T(s, 0) = A005448(s).",
				"T(s, A006501(s) + x) = A000292(s + 1), for all x \u003e= 0.",
				"This lead to: T(s, A000578(s) + x) = A000292(s + 1), for all x \u003e= 0.",
				"T(s, t) = (3s^2 + 3s) / 2 + A061201(t) + 1, for s \u003e t + 1 \u003e 1.",
				"T(s, 1) = (3s^2 + 3s) / 2 + 2 = A139482(s + 2), for s \u003e t + 1 \u003e 1.",
				"T(s, 2) = (3s^2 + 3s) / 2 + 5 = A124011(s), for s \u003e t + 1 \u003e 2.",
				"T(s, t) = T(s + k, t) - 3*k*s - 3*A000217(k), for s \u003e t + 1 \u003e 5.",
				"T(t + 2 - k, t) = T(t + 2, t) - 3*k*s - 6k + 3*A000217(k), for 0 \u003c= 2k \u003c= (t-1), for t \u003e 4.",
				"When n \u003e 4, T(n, n) = T(n + 2, n) - 6n - 15 = A347252(n) = Sum_{k=1..n} floor(n/k)*A000005(k) + (3n^2 + 3n - 10) / 2 = A061201(n) + (3n^2 + 3n - 10) / 2 = (1/2)n(6g^2 + (6g-2)log(n) - 6g - 6g1 + log^2(n) + 2) + (3n^2)/2 + (3n)/2 - 5 + O(sqrt(n)) where g is the Euler-Mascheroni number and g1 is the first Stieltjes constant."
			],
			"example": [
				"T(2,0) = 10: (0,0,0), (0,0,1), (0,0,2), (0,1,0), (0,1,1), (0,1,2), (0,2,0), (1,0,0), (1,0,1), (2,0,0).",
				"T(0,1) = 1: (0,0,0).",
				"Array T(s,t) begins:",
				"    1,   1,   1,   1,   1,   1,   1,   1,   1, ...",
				"    4,   4,   4,   4,   4,   4,   4,   4,   4, ...",
				"   10,  10,  10,  10,  10,  10,  10,  10,  10, ...",
				"   19,  20,  20,  20,  20,  20,  20,  20,  20, ...",
				"   31,  32,  35,  35,  35,  35,  35,  35,  35, ...",
				"   46,  47,  50,  53,  56,  56,  56,  56,  56, ...",
				"   64,  65,  68,  71,  77,  77,  83,  83,  84, ...",
				"   85,  86,  89,  92,  98, 101, 107, 107, 114, ...",
				"  109, 110, 113, 116, 122, 125, 134, 134, 141, ..."
			],
			"mathematica": [
				"Table[Function[s, Sum[Boole[a b c \u003c= t], {a, 0, s}, {b, 0, s - a}, {c, 0, s - a - b}]][n - t], {n, 0, 10}, {t, n, 0, -1}] // Flatten (* _Michael De Vlieger_, Aug 25 2021 *)"
			],
			"program": [
				"(C) int T(int s, int t) { int cnt = 0; for (int a = 0; a \u003c= s; ++a) for (int b = 0; b \u003c= s - a; ++b) for (int c = 0; c \u003c= s - a - b; ++c) if (a * b * c \u003c= t) ++cnt; return cnt; }",
				"(PARI) T(s, t) = sum(a=0, s, sum(b=0, s-a, sum(c=0, s-a-b, a*b*c \u003c= t))); \\\\ _Michel Marcus_, Aug 25 2021"
			],
			"xref": [
				"Cf. A000005, A000012, A000217, A000292, A000578, A006501, A061201, A124011, A139482,",
				"Cf. A005448 (column 0), A347252 (main diagonal)."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,3",
			"author": "_Vo Hoang Anh_, Aug 24 2021",
			"references": 1,
			"revision": 112,
			"time": "2021-10-28T11:13:06-04:00",
			"created": "2021-10-28T11:13:06-04:00"
		}
	]
}