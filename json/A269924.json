{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A269924",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 269924,
			"data": "225225,12317877,12317877,351683046,792534015,351683046,7034538511,26225260226,26225260226,7034538511,111159740692,600398249550,993494827480,600398249550,111159740692,1480593013900,10743797911132,25766235457300,25766235457300,10743797911132,1480593013900,17302190625720,160576594766588,517592962672296,750260619502310,517592962672296,160576594766588,17302190625720",
			"name": "Triangle read by rows: T(n,f) is the number of rooted maps with n edges and f faces on an orientable surface of genus 4.",
			"comment": [
				"Row n contains n-7 terms."
			],
			"link": [
				"Gheorghe Coserea, \u003ca href=\"/A269924/b269924.txt\"\u003eRows n = 8..208, flattened\u003c/a\u003e",
				"Sean R. Carrell, Guillaume Chapuy, \u003ca href=\"http://arxiv.org/abs/1402.6300\"\u003eSimple recurrence formulas to count maps on orientable surfaces\u003c/a\u003e, arXiv:1402.6300 [math.CO], 2014."
			],
			"example": [
				"Triangle starts:",
				"n\\f  [1]           [2]           [3]           [4]",
				"[8]  225225;",
				"[9]  12317877,     12317877;",
				"[10] 351683046,    792534015,    351683046;",
				"[11] 7034538511,   26225260226,  26225260226,  7034538511;",
				"[12] ..."
			],
			"mathematica": [
				"Q[0, 1, 0] = 1; Q[n_, f_, g_] /; n\u003c0 || f\u003c0 || g\u003c0 = 0;",
				"Q[n_, f_, g_] := Q[n, f, g] = 6/(n+1)((2n-1)/3 Q[n-1, f, g] + (2n-1)/3 Q[n - 1, f-1, g] + (2n-3)(2n-2)(2n-1)/12 Q[n-2, f, g-1] + 1/2 Sum[l = n-k; Sum[v = f-u; Sum[j = g-i; Boole[l \u003e= 1 \u0026\u0026 v \u003e= 1 \u0026\u0026 j \u003e= 0] (2k-1) (2l-1) Q[k-1, u, i] Q[l-1, v, j], {i, 0, g}], {u, 1, f}], {k, 1, n}]);",
				"Table[Q[n, f, 4], {n, 8, 14}, {f, 1, n-7}] // Flatten (* _Jean-François Alcover_, Aug 10 2018 *)"
			],
			"program": [
				"(PARI)",
				"N = 14; G = 4; gmax(n) = min(n\\2, G);",
				"Q = matrix(N + 1, N + 1);",
				"Qget(n, g) = { if (g \u003c 0 || g \u003e n/2, 0, Q[n+1, g+1]) };",
				"Qset(n, g, v) = { Q[n+1, g+1] = v };",
				"Quadric({x=1}) = {",
				"  Qset(0, 0, x);",
				"  for (n = 1, length(Q)-1, for (g = 0, gmax(n),",
				"    my(t1 = (1+x)*(2*n-1)/3 * Qget(n-1, g),",
				"       t2 = (2*n-3)*(2*n-2)*(2*n-1)/12 * Qget(n-2, g-1),",
				"       t3 = 1/2 * sum(k = 1, n-1, sum(i = 0, g,",
				"       (2*k-1) * (2*(n-k)-1) * Qget(k-1, i) * Qget(n-k-1, g-i))));",
				"    Qset(n, g, (t1 + t2 + t3) * 6/(n+1))));",
				"};",
				"Quadric('x);",
				"concat(apply(p-\u003eVecrev(p/'x), vector(N+1 - 2*G, n, Qget(n-1 + 2*G, G))))"
			],
			"xref": [
				"Columns f=1-10 give: A288271 f=1, A288272 f=2, A288273 f=3, A288274 f=4, A288275 f=5, A288276 f=6, A288277 f=7, A288278 f=8, A288279 f=9, A288280 f=10.",
				"Cf. A035309, A269921, A269922, A269923, A269925, A270406, A270407, A270408, A270409, A270410, A270412.",
				"Row sums give A215402 (column 4 of A269919)."
			],
			"keyword": "nonn,tabl",
			"offset": "8,1",
			"author": "_Gheorghe Coserea_, Mar 15 2016",
			"references": 13,
			"revision": 39,
			"time": "2018-08-10T17:36:35-04:00",
			"created": "2016-03-16T09:12:02-04:00"
		}
	]
}