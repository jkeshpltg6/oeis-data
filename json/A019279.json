{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A019279",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 19279,
			"data": "2,4,16,64,4096,65536,262144,1073741824,1152921504606846976",
			"name": "Superperfect numbers: numbers k such that sigma(sigma(k)) = 2*k where sigma is the sum-of-divisors function (A000203).",
			"comment": [
				"Let sigma_m(n) be result of applying sum-of-divisors function m times to n; call n (m,k)-perfect if sigma_m (n) = k*n; sequence gives (2,2)-perfect numbers.",
				"Even values of these are 2^(p-1) where 2^p-1 is a Mersenne prime (A000043 and A000668). No odd superperfect numbers are known. Hunsucker and Pomerance checked that there are no odd ones below 7 * 10^24. - _Jud McCranie_, Jun 01 2000",
				"The number of divisors of a(n) is equal to A000043(n), if there are no odd superperfect numbers. - _Omar E. Pol_, Feb 29 2008",
				"The sum of divisors of a(n) is the n-th Mersenne prime A000668(n), provided that there are no odd superperfect numbers. - _Omar E. Pol_, Mar 11 2008",
				"Largest proper divisor of A072868(n) if there are no odd superperfect numbers. - _Omar E. Pol_, Apr 25 2008",
				"This sequence is a divisibility sequence if there are no odd superperfect numbers. - _Charles R Greathouse IV_, Mar 14 2012",
				"For n\u003e1, sigma(sigma(a(n))) + phi(phi(a(n))) = (9/4)*a(n). - _Farideh Firoozbakht_, Mar 02 2015",
				"The term \"super perfect number\" was coined by Suryanarayana (1969). He and Kanold (1969) gave the general form of even superperfect numbers. - _Amiram Eldar_, Mar 08 2021"
			],
			"reference": [
				"Dieter Bode, Über eine Verallgemeinerung der vollkommenen Zahlen, Dissertation, Braunschweig, 1971.",
				"Richard K. Guy, Unsolved Problems in Number Theory, 3rd Edition, Springer, 2004, Section B9, pp. 99-100.",
				"József Sándor, Dragoslav S. Mitrinovic and Borislav Crstici, Handbook of Number Theory I, Springer Science \u0026 Business Media, 2005, Chapter III, pp. 110-111.",
				"József Sándor and Borislav Crstici, Handbook of Number theory II, Kluwer Academic Publishers, 2004, Chapter 1, pp. 38-42."
			],
			"link": [
				"P. Bundschuh, \u003ca href=\"https://eudml.org/doc/140933\"\u003eAufgabe 601\u003c/a\u003e, Elem. Math., Vol. 24 (1969), p. 69; \u003ca href=\"https://www.e-periodica.ch/digbib/view?pid=edm-001%3A1969%3A24%3A%3A7#71\"\u003ealternative link\u003c/a\u003e.",
				"G. L. Cohen and H. J. J. te Riele, \u003ca href=\"http://projecteuclid.org/euclid.em/1047565640\"\u003eIterating the sum-of-divisors function\u003c/a\u003e, Experimental Mathematics, 5 (1996), pp. 93-100.",
				"G. G. Dandapat, J. L. Hunsucker, and Carl Pomerance, \u003ca href=\"http://projecteuclid.org/euclid.pjm/1102905990\"\u003eSome new results on odd perfect numbers\u003c/a\u003e, Pacific J. Math. Volume 57, Number 2 (1975), 359-364.",
				"A. Hoque and H. Kalita, \u003ca href=\"http://www.naturalspublishing.com/files/published/1r9c4i46d2gg27.pdf\"\u003eGeneralized perfect numbers connected with arithmetic functions\u003c/a\u003e, Math. Sci. Lett. 3, No. 3, 249-253 (2014).",
				"J. L. Hunsucker and Carl Pomerance, \u003ca href=\"https://math.dartmouth.edu/~carlp/super.pdf\"\u003eThere are no odd superperfect number less than 7*10^24\u003c/a\u003e, Indian J. Math., Vol. 17 (1975), pp. 107-120.",
				"H.-J. Kanold, \u003ca href=\"https://eudml.org/doc/140929\"\u003eÜber \"Super perfect numbers\"\u003c/a\u003e, Elem. Math., Vol. 24 (1969), pp. 61-62; \u003ca href=\"https://www.e-periodica.ch/digbib/view?pid=edm-001%3A1969%3A24%3A%3A7#67\"\u003ealternative link\u003c/a\u003e.",
				"Graham Lord, \u003ca href=\"https://www.e-periodica.ch/digbib/view?pid=edm-001%3A1975%3A30%3A%3A7#90\"\u003eEven Perfect and Superperfect Numbers\u003c/a\u003e, Elem. Math., Vol. 30 (1975), pp. 87-88.",
				"H. G. Niederreiter, \u003ca href=\"https://eudml.org/doc/140973\"\u003eSolution of Aufgabe 601\u003c/a\u003e, Elem. Math., Vol. 25 (1970), pp. 66-67; \u003ca href=\"https://www.e-periodica.ch/digbib/view?pid=edm-001%3A1970%3A25%3A%3A7#69\"\u003ealternative link\u003c/a\u003e.",
				"Paul Shubhankar, \u003ca href=\"https://www.erpublication.org/published_paper/IJETR011954.pdf\"\u003eTen Problems of Number Theory\u003c/a\u003e, International Journal of Engineering and Technical Research (IJETR), ISSN: 2321-0869, Volume-1, Issue-9, November 2013.",
				"D. Suryanarayana, \u003ca href=\"https://eudml.org/doc/140912\"\u003eSuper perfect numbers\u003c/a\u003e, Elem. Math., Vol. 24 (1969), pp. 16-17; \u003ca href=\"https://www.e-periodica.ch/digbib/view?pid=edm-001%3A1969%3A24%3A%3A7#22\"\u003ealternative link\u003c/a\u003e.",
				"D. Suryanarayana, \u003ca href=\"https://eudml.org/doc/141127\"\u003eThere is no superperfect number of the form p^(2*alpha)\u003c/a\u003e, Elem. Math., Vol. 28 (1973), pp. 148-150; \u003ca href=\"https://www.e-periodica.ch/digbib/view?pid=edm-001%3A1973%3A28%3A%3A31#155\"\u003ealternative link\u003c/a\u003e.",
				"László Tóth, \u003ca href=\"http://macs.elte.hu/downloads/abstracts/MaCS_abs_Toth.pdf\"\u003eThe alternating sum-of-divisors function\u003c/a\u003e, 9th Joint Conf. on Math. and Comp. Sci., February 9-12, 2012, Siófok, Hungary.",
				"László Tóth, \u003ca href=\"http://arxiv.org/abs/1111.4842\"\u003eA survey of the alternating sum-of-divisors function\u003c/a\u003e, arXiv:1111.4842 [math.NT], 2011-2014.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/SuperperfectNumber.html\"\u003eSuperperfect Number\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Superperfect_number\"\u003eSuperperfect number\u003c/a\u003e.",
				"Tomohiro Yamada, \u003ca href=\"https://jtnb.centre-mersenne.org/item/JTNB_2020__32_1_259_0/\"\u003eOn finiteness of odd superperfect numbers\u003c/a\u003e, Journal de Théorie des Nombres de Bordeaux, Vol. 32, No. 1 (2020), pp. 259-274."
			],
			"formula": [
				"a(n) = (1 + A000668(n))/2, if there are no odd superperfect numbers. - _Omar E. Pol_, Mar 11 2008",
				"Also, if there are no odd superperfect numbers then a(n) = 2^A000043(n)/2 = A072868(n)/2 = A032742(A072868(n)). - _Omar E. Pol_, Apr 25 2008",
				"a(n) = 2^A090748(n), if there are no odd superperfect numbers. - _Ivan N. Ianakiev_, Sep 04 2013"
			],
			"example": [
				"sigma(sigma(4))=2*4, so 4 is in the sequence."
			],
			"mathematica": [
				"sigma = DivisorSigma[1, #]\u0026;",
				"For[n = 2, True, n++, If[sigma[sigma[n]] == 2 n, Print[n]]] (* _Jean-François Alcover_, Sep 11 2018 *)"
			],
			"program": [
				"(PARI) is(n)=sigma(sigma(n))==2*n \\\\ _Charles R Greathouse IV_, Nov 20 2012"
			],
			"xref": [
				"Cf. A019280, A000203, A000396, A000668, A000043, A034897, A061652, A032742, A072868."
			],
			"keyword": "nonn,more,nice",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"a(8)-a(9) from _Jud McCranie_, Jun 01 2000",
				"Corrected by _Michel Marcus_, Oct 28 2017"
			],
			"references": 86,
			"revision": 108,
			"time": "2021-03-09T16:14:44-05:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}