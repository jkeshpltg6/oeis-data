{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000958",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 958,
			"id": "M2748 N1104",
			"data": "1,1,3,8,24,75,243,808,2742,9458,33062,116868,417022,1500159,5434563,19808976,72596742,267343374,988779258,3671302176,13679542632,51134644014,191703766638,720629997168,2715610275804,10256844598900,38822029694628,147229736485868",
			"name": "Number of ordered rooted trees with n edges having root of odd degree.",
			"comment": [
				"a(n) is the number of Dyck n-paths containing no peak at height 2 before the first return to ground level. Example: a(3)=3 counts UUUDDD, UDUUDD, UDUDUD. - _David Callan_, Jun 07 2006",
				"Also number of order trees with n edges and having no even-length branches starting at the root. - _Emeric Deutsch_, Mar 02 2007",
				"Convolution of the Catalan sequence 1,1,2,5,14,42,... (A000108) and the Fine sequence 1,0,1,2,6,18,... (A000957). a(n) = A127541(n,0). - _Emeric Deutsch_, Mar 02 2007",
				"The Catalan transform of A008619. - _R. J. Mathar_, Nov 06 2008",
				"Hankel transform is F(2n+1). - _Paul Barry_, Dec 01 2008",
				"Starting with offset 2 = iterates of M * [1,1,0,0,0,...] where M = a tridiagonal matrix with [0,2,2,2,...] in the main diagonal and [1,1,1,...] in the super and subdiagonals. - _Gary W. Adamson_, Jan 09 2009",
				"Equals INVERT transform of A032357. - _Gary W. Adamson_, Apr 10 2009",
				"a(n) is the number of Dyck paths of semilength n+1 that have equal length inclines incident with the first return to ground level. For example, for UUDDUUDDUD these inclines are DD and UU (steps 3 through 6), and a(3)=3 counts UDUDUUDD, UDUDUDUD, UUDDUUDD. - _David Callan_, Aug 23 2011",
				"a(n) is the number of imprimitive Dyck paths of semilength n+1 for which the heights of the first and the last peaks coincide, this gives the connection to A193215. - _Volodymyr Mazorchuk_, Aug 27 2011"
			],
			"reference": [
				"Kim, Ki Hang; Rogers, Douglas G.; Roush, Fred W. Similarity relations and semiorders. Proceedings of the Tenth Southeastern Conference on Combinatorics, Graph Theory and Computing (Florida Atlantic Univ., Boca Raton, Fla., 1979), pp. 577--594, Congress. Numer., XXIII-XXIV, Utilitas Math., Winnipeg, Man., 1979. MR0561081 (81i:05013) - _N. J. A. Sloane_, Jun 05 2012",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A000958/b000958.txt\"\u003eTable of n, a(n) for n = 1..200\u003c/a\u003e",
				"Dennis E. Davenport, Louis W. Shapiro, Leon C. Woodson, \u003ca href=\"http://math.colgate.edu/~integers/u8/u8.Abstract.html\"\u003eA bijection between the triangulations of convex polygons and ordered trees\u003c/a\u003e, Integers (2020) Vol. 20, Article #A8.",
				"E. Deutsch and L. Shapiro, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(01)00121-2\"\u003eA survey of the Fine numbers\u003c/a\u003e, Discrete Math., 241 (2001), 241-265.",
				"Sergio Falcon, \u003ca href=\"http://www.mathnet.or.kr/mathnet/thesis_file/CKMS-28-4-827-832.pdf\"\u003eCatalan transform of the K-Fibonacci sequence\u003c/a\u003e, Commun. Korean Math. Soc. 28 (2013), No. 4, pp. 827-832; http://dx.doi.org/10.4134/CKMS.2013.28.4.827.",
				"T. Fine, \u003ca href=\"http://dx.doi.org/10.1016/S0019-9958(70)90177-4\"\u003eExtrapolation when very little is known about the source\u003c/a\u003e Information and Control 16 (1970), 331-359.",
				"D. G. Rogers, \u003ca href=\"http://dx.doi.org/10.1016/0097-3165(77)90082-6\"\u003eSimilarity relations on finite ordered sets\u003c/a\u003e, J. Combin. Theory, A 23 (1977), 88-98. Erratum, loc. cit., 25 (1978), 95-96.",
				"Yidong Sun, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2004.07.002\"\u003eThe statistic \"number of udu's\" in Dyck paths\u003c/a\u003e, Discrete Math., 287 (2004), 177-186. See Table 2.",
				"Murray Tannock, \u003ca href=\"https://skemman.is/bitstream/1946/25589/1/msc-tannock-2016.pdf\"\u003eEquivalence classes of mesh patterns with a dominating pattern\u003c/a\u003e, MSc Thesis, Reykjavik Univ., May 2016.",
				"\u003ca href=\"/index/Ro#rooted\"\u003eIndex entries for sequences related to rooted trees\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A000957(n) + A000957(n+1).",
				"G.f.: (1-x-(1+x)*sqrt(1-4*x))/(2*x*(x+2)). - _Paul Barry_, Jan 26 2007",
				"G.f.: z*C/(1-z^2*C^2), where C=(1-sqrt(1-4*z))/(2*z) is the Catalan function. - _Emeric Deutsch_, Mar 02 2007",
				"a(n+1) = Sum_{0\u003c=k\u003c=[n/2]} A039599(n-k,k). - _Philippe Deléham_, Mar 13 2007",
				"a(n) = (-1/2)^n*(-2-5*Sum((-8)^k*Gamma(1/2+k)*(4/5+k)/(sqrt(Pi)*Gamma(k+3)),k=1.. n-1)). - Mark van Hoeij, Nov 11 2009",
				"a(n) + a(n+1) = A135339(n+1). - _Philippe Deléham_, Dec 02 2009",
				"From _Gary W. Adamson_, Jul 14 2011: (Start)",
				"a(n) = sum of top row terms in M^(n-1), where M = the following infinite square production matrix:",
				"0, 1, 0, 0, 0, 0,...",
				"1, 1, 1, 0, 0, 0,...",
				"1, 1, 1, 1, 0, 0,...",
				"1, 1, 1, 1, 1, 0,...",
				"1, 1, 1, 1, 1, 1,...",
				"... (End)",
				"D-finite with recurrence 2*(n+1)*a(n) + (-5*n+3)*a(n-1) + (-11*n+21)*a(n-2) +2 *(-2*n+5)*a(n-3) = 0. - _R. J. Mathar_, Dec 03 2012",
				"a(n) ~ 5*4^n/(9*sqrt(Pi)*n^(3/2)). - _Vaclav Kotesovec_, Aug 09 2013",
				"a(n) = Catalan(n-1)*h(n-1) for n\u003e=2 where h(n) = hypergeom([1,3/2,-n/2,(1-n)/2],[1/2,-n,-n+1/2], 1). - _Peter Luschny_, Apr 25 2016"
			],
			"maple": [
				"g:=(1-x-(1+x)*sqrt(1-4*x))/2/x/(x+2): gser:=series(g,x=0,30): seq(coeff(gser,x,n),n=1..26); # _Emeric Deutsch_, Mar 02 2007",
				"A958 := n -\u003e add(binomial(2*n-2*k-2, n-1)*(2*k+1)/n, k=0..floor((n-1)/2)): seq(A958(n), n=1..28); # _Johannes W. Meijer_, Jul 26 2013"
			],
			"mathematica": [
				"nn = 30; Rest[CoefficientList[Series[(1-x-(1+x)*Sqrt[1-4*x])/(2*x*(x+2)), {x, 0, nn}], x]] (* _T. D. Noe_, May 09 2012 *)"
			],
			"program": [
				"(Python)",
				"from itertools import accumulate",
				"def A000958_list(size):",
				"    if size \u003c 1: return []",
				"    L, accu = [], [1]",
				"    for n in range(size-1):",
				"        accu = list(accumulate(accu+[-accu[-1]]))",
				"        L.append(accu[n])",
				"    return L",
				"print(A000958_list(29)) # _Peter Luschny_, Apr 25 2016",
				"(PARI) my(x='x+O('x^30)); Vec((1-x-(1+x)*sqrt(1-4*x))/(2*x*(x+2))) \\\\ _G. C. Greubel_, Feb 27 2019",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Rationals(), 30); Coefficients(R!( (1-x-(1+x)*Sqrt(1-4*x))/(2*x*(x+2)) )); // _G. C. Greubel_, Feb 27 2019",
				"(Sage) a=((1-x-(1+x)*sqrt(1-4*x))/(2*x*(x+2))).series(x, 30).coefficients(x, sparse=False); a[1:] # _G. C. Greubel_, Feb 27 2019"
			],
			"xref": [
				"A column of A065602, a column of A098747.",
				"Cf. A127541, A127539, A000108, A000957, A032357."
			],
			"keyword": "nonn,easy",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_",
			"references": 19,
			"revision": 96,
			"time": "2020-04-09T17:35:49-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}