{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097772",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97772,
			"data": "1,679,460361,312124079,211619665201,143477820882199,97277750938465721,65954171658458876639,44716831106684179895521,30317945536160215510286599,20555522356685519431794418601,13936613839887246014541105524879",
			"name": "Pell equation solutions (13*a(n))^2 - 170*b(n)^2 = -1 with b(n):=A097771(n), n \u003e= 0.",
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A097772/b097772.txt\"\u003eTable of n, a(n) for n = 0..353\u003c/a\u003e",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"Giovanni Lucca, \u003ca href=\"http://forumgeom.fau.edu/FG2019volume19/FG201902index.html\"\u003eInteger Sequences and Circle Chains Inside a Hyperbola\u003c/a\u003e, Forum Geometricorum (2019) Vol. 19, 11-16.",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (678,-1)."
			],
			"formula": [
				"G.f.: (1 + x)/(1 - 2*339*x + x^2).",
				"a(n) = S(n, 2*339) + S(n-1, 2*339) = S(2*n, 2*sqrt(170)), with Chebyshev polynomials of the 2nd kind. See A049310 for the triangle of S(n, x)= U(n, x/2) coefficients. S(-1, x) := 0 =: U(-1, x).",
				"a(n) = ((-1)^n)*T(2*n+1, 13*i)/(13*i) with the imaginary unit i and Chebyshev polynomials of the first kind. See the T-triangle A053120.",
				"a(n) = 678*a(n-1) - a(n-2), n \u003e 1; a(0)=1, a(1)=679. - _Philippe Deléham_, Nov 18 2008",
				"a(n) = (1/13)*sinh((2*n + 1)*arcsinh(13)). - _Bruno Berselli_, Apr 05 2018"
			],
			"example": [
				"(x,y) = (13*1=13;1), (8827=13*679;677), (5984693=13*460361;459005), ... give the positive integer solutions to x^2 - 170*y^2 =-1."
			],
			"mathematica": [
				"LinearRecurrence[{678, -1}, {1, 679}, 12] (* _Ray Chandler_, Aug 12 2015 *)"
			],
			"program": [
				"(PARI) x='x+O('x^99); Vec((1+x)/(1-2*339*x+x^2)) \\\\ _Altug Alkan_, Apr 05 2018"
			],
			"xref": [
				"Cf. A097771 for S(n, 2*339).",
				"Cf. similar sequences of the type (1/k)*sinh((2*n+1)*arcsinh(k)) listed in A097775."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Aug 31 2004",
			"references": 4,
			"revision": 29,
			"time": "2020-01-23T03:25:46-05:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}