{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A075181",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 75181,
			"data": "1,2,1,6,6,2,24,36,22,6,120,240,210,100,24,720,1800,2040,1350,548,120,5040,15120,21000,17640,9744,3528,720,40320,141120,231840,235200,162456,78792,26136,5040,362880,1451520,2751840,3265920,2693880,1614816",
			"name": "Coefficients of certain polynomials (rising powers).",
			"comment": [
				"This is the unsigned triangle A048594 with rows read backwards.",
				"The row polynomials p(n,y) := Sum_{m=0..n-1}a(n,m)*y^m, n\u003e=1, are obtained from (log(x)*(-x*log(x))^n)*(d^n/dx^n)(1/log(x)), n\u003e=1, after replacement of log(x) by y.",
				"The gcd of row n is A075182(n). Row sums give A007840(n), n\u003e=1.",
				"The columns give A000142 (factorials), A001286 (Lah), 2* A075183, 2*A075184, 4*A075185, 4!*A075186, 4!*A075187 for m=0..6.",
				"Coefficients T(n,k) of the differential operator expansion",
				"[x^(1+y)D]^n = x^(n*y)[T(n,1)* (xD)^n / n! + y * T(n,2)* (xD)^(n-1) / (n-1)! + ... + y^(n-1) * T(n,n) * (xD)], where D = d/dx. Note that (xD)^n = Bell(n,:xD:), where (:xD:)^n = x^n * D^n and Bell(n,x) are the Bell / Touchard polynomials. See A094638. - _Tom Copeland_, Aug 22 2015"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A075181/b075181.txt\"\u003eRows n = 1..100, flattened\u003c/a\u003e",
				"Y.-Z. Huang, J. Lepowsky and L. Zhang, \u003ca href=\"https://arxiv.org/abs/math/0311235\"\u003eA logarithmic generalization of tensor product theory for modules for a vertex operator algebra\u003c/a\u003e, arXiv:math/0311235 [math.QA], 2003; Internat. J. Math. 17 (2006), no. 8, 975-1012. See page 984 eq. (3.9) MR2261644.",
				"D. Lubell, \u003ca href=\"http://www.jstor.org/stable/3647779\"\u003eProblem 10992, problems and solutions\u003c/a\u003e, Amer. Math. Monthly 110 (2003) p. 155. \u003ca href=\"http://www.jstor.org/stable/4145207\"\u003eEqual Sums of Reciprocal Products: 10992\u003c/a\u003e (2004) pp. 827-829."
			],
			"formula": [
				"a(n, m) = (n-m)!*|S1(n, n-m)|, n\u003e=m+1\u003e=1, else 0, with S1(n, m) := A008275(n, m) (Stirling1).",
				"a(n, m) = (n-m)*a(n-1, m)+(n-1)*a(n-1, m-1), if n\u003e=m+1\u003e=1, a(n, -1) := 0 and a(1, 0)=1, else 0."
			],
			"example": [
				"Triangle starts:",
				"1;",
				"2,1;",
				"6,6,2;",
				"24,36,22,6;",
				"...",
				"n=2: (x^2*log(x)^3)*(d^2/d^x^2)(1/log(x)) = 2 + log(x)."
			],
			"maple": [
				"seq(seq(k!*abs(Stirling1(n,k)),k=n..1,-1),n=1..10); # _Robert Israel_, Jul 12 2015"
			],
			"mathematica": [
				"Table[ Table[ k!*StirlingS1[n, k] // Abs, {k, 1, n}] // Reverse, {n, 1, 9}] // Flatten (* _Jean-François Alcover_, Jun 21 2013 *)"
			],
			"program": [
				"(PARI) {T(n, k)= if(k\u003c0| k\u003e=n, 0, (-1)^k* stirling(n, n-k)* (n-k)!)} /* _Michael Somos_ Apr 11 2007 */"
			],
			"xref": [
				"Cf. A048594, A075178, A007840, A075182.",
				"Cf. A094638."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "1,2",
			"author": "_Wolfdieter Lang_, Sep 19 2002",
			"references": 10,
			"revision": 31,
			"time": "2020-08-22T17:38:22-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}