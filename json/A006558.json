{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006558",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6558,
			"id": "M2155",
			"data": "1,2,33,242,11605,28374,171893,1043710445721,2197379769820,2642166652554075",
			"name": "Start of first run of n consecutive integers with same number of divisors.",
			"comment": [
				"The entry 40311 given by Guy and by Wells is incorrect. - _Jud McCranie_, Jan 20 2002",
				"a(10) \u003c= 14366256627859031643, a(11) \u003c= 193729158984658237901148, a(12) \u003c= 1284696910355238430481207644. - Bilgin Ali and Bruno Mishutka (bruno_mishutka(AT)hotmail.com), Dec 29 2006",
				"a(10) \u003c= 2642166652554075, a(11) \u003c= 17707503256664346, a(12) \u003c= 9827470582657267545. - _David Wasserman_, Feb 22 2008",
				"a(13) \u003c= 58032555961853414629544105797569, a(14) \u003c= 25335305376270095455498383578391968. - _Vladimir Letsko_, Jun 13 2015",
				"a(10) \u003e 10^13. - _Giovanni Resta_, Jul 13 2015",
				"a(15) \u003c= 1956636199634182220409498715768827417. - _Vladimir Letsko_, Mar 01 2017",
				"a(16) \u003c= 37981337212463143311694743672867136611416. - _Vladimir Letsko_, Mar 17 2017",
				"a(17) \u003c= 768369049267672356024049141254832375543516. - _Vladimir Letsko_, Sep 12 2017",
				"a(18) \u003c= 8100239725694207838698666538353341829610974940. - _Vladimir Letsko_, Dec 24 2020"
			],
			"reference": [
				"J.-M. De Koninck, Ces nombres qui nous fascinent, Entry 33, pp 12, Ellipses, Paris 2008.",
				"R. K. Guy, Unsolved Problems in Number Theory, section B18.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"D. Wells, The Penguin Dictionary of Curious and Interesting Numbers, Penguin Books, NY, 1986, pages 147 and 176."
			],
			"link": [
				"Pentti Haukkanen, \u003ca href=\"https://www.researchgate.net/publication/282603270_SOME_COMPUTATIONAL_RESULTS_CONCERNING_THE_DIVISOR_FUNCTIONS_dn_AND_SIGMAn\"\u003eSome computational results concerning the divisor functions d(n) and sigma(n)\u003c/a\u003e, The Mathematics Student, Vol. 62 Nos. 1-4 (1993) pp. 166-168. See p. 167.",
				"Vladimir A. Letsko, \u003ca href=\"http://arxiv.org/abs/1510.07081\"\u003eSome new results on consecutive equidivisible integers\u003c/a\u003e, arXiv:1510.07081 [math.NT], 2015.",
				"Vladimir A. Letsko, Vasilii Dziubenko \u003ca href=\"http://grani.vspu.ru/files/publics/1464684416.pdf\"\u003eOn consecutive equidivisible integers\u003c/a\u003e (in Russian), Boundaries of knowledge, 2 (45) 2016.",
				"Carlos Rivera, \u003ca href=\"http://www.primepuzzles.net/problems/prob_020.htm\"\u003eProblem 20: k consecutive numbers with the same number of divisors\u003c/a\u003e",
				"Carlos Rivera, \u003ca href=\"http://www.primepuzzles.net/problems/prob_061.htm\"\u003eProblem 61: problem 20 revisited\u003c/a\u003e"
			],
			"example": [
				"33 has four divisors (1, 3, 11, and 33), 34 has four divisors (1, 2, 17, and 34), 35 has four divisors (1, 5, 7, and 35).  These are the first three consecutive numbers with the same number of divisors, so a(3)=33."
			],
			"maple": [
				"with(numtheory); A006558:=proc(q,w) local a,k,j,ok,n;",
				"for j from 0 to w do for n from 1 to q do ok:=1; a:=tau(n);",
				"  for k from 1 to j do if a\u003c\u003etau(n+k) then ok:=0; break; fi; od;",
				"  if ok=1 then print(n); break; fi;",
				"od; od; end: A006558(10^10,20); # _Paolo P. Lava_, May 03 2013"
			],
			"mathematica": [
				"tau = DivisorSigma[0, #]\u0026;",
				"A006558[q_, w_] := Module[{a, k, j, ok, n}, For[j = 0, j \u003c= w, j++, For[n = 1, n \u003c= q, n++, ok = 1; a = tau[n]; For[k = 1, k \u003c= j, k++, If[a != tau[n + k], ok = 0; Break[]]]; If [ok == 1, Print[n]; Break[]]]]];",
				"A006558[2*10^5, 7] (* _Jean-François Alcover_, Dec 10 2017, after _Paolo P. Lava_ *)"
			],
			"program": [
				"(PARI) isok(n, k) = {nb = numdiv(k); for (j=k+1, k+n-1, if (numdiv(j) != nb, return(0));); 1;}",
				"a(n) = {k=1; while (!isok(n, k), k++); k;} \\\\ _Michel Marcus_, Feb 17 2016"
			],
			"xref": [
				"Cf. A000005, A005237, A005238, A006601, A049051, A019273, A039665.",
				"Cf. A034173, A115158, A119479."
			],
			"keyword": "nonn,more,hard",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, _Robert G. Wilson v_",
			"ext": [
				"a(8) from _Jud McCranie_, Jan 20 2002",
				"a(9) conjectured by _David Wasserman_, Jan 08 2006",
				"a(9) confirmed by _Jud McCranie_, Jan 14 2006",
				"a(10) by _Jud McCranie_, Nov 27 2018"
			],
			"references": 36,
			"revision": 113,
			"time": "2021-12-11T19:00:02-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}