{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A326323",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 326323,
			"data": "1,1,1,1,1,1,1,1,2,1,1,1,3,6,1,1,1,4,13,24,1,1,1,5,22,75,120,1,1,1,6,33,160,541,720,1,1,1,7,46,285,1456,4683,5040,1,1,1,8,61,456,3081,15904,47293,40320,1,1,1,9,78,679,5656,40005,202672,545835,362880,1",
			"name": "A(n, k) = A_{n}(k) where A_{n}(x) are the Eulerian polynomials, square array read by ascending antidiagonals, for n \u003e= 0 and k \u003e= 0.",
			"link": [
				"OEIS Wiki, \u003ca href=\"http://oeis.org/wiki/Eulerian_polynomials\"\u003eEulerian polynomials\u003c/a\u003e."
			],
			"formula": [
				"A(n, k) = Sum_{j=0..k} a(k, j)*n^j where a(k, j) are the Eulerian numbers.",
				"E.g.f.: (n - 1)/(n - exp((n-1)*x)) for n = 0 and n \u003e= 2, 1/(1 - x) if n = 1.",
				"A(n, 0) = 1; A(n, 1) = n!.",
				"A(n, k) = (k - 1)^(n + 1)/k HurwitzLerchPhi(1/k, -n, 0) for k \u003e= 2.",
				"A(n, k) = Sum_{j=0..n} j! * Stirling2(n, j) * (k - 1)^(n - j) for k \u003e= 2."
			],
			"example": [
				"Array starts:",
				"[0] 1, 1, 1,  1,    1,     1,      1,        1,         1,          1, [A000012]",
				"[1] 1, 1, 2,  6,   24,   120,    720,     5040,     40320,     362880, [A000142]",
				"[2] 1, 1, 3, 13,   75,   541,   4683,    47293,    545835,    7087261, [A000670]",
				"[3] 1, 1, 4, 22,  160,  1456,  15904,   202672,   2951680,   48361216, [A122704]",
				"[4] 1, 1, 5, 33,  285,  3081,  40005,   606033,  10491885,  204343641, [A255927]",
				"[5] 1, 1, 6, 46,  456,  5656,  84336,  1467376,  29175936,  652606336, [A326324]",
				"[6] 1, 1, 7, 61,  679,  9445, 158095,  3088765,  68958295, 1731875605,",
				"[7] 1, 1, 8, 78,  960, 14736, 272448,  5881968, 145105920, 4026744576,",
				"[8] 1, 1, 9, 97, 1305, 21841, 440649, 10386817, 279768825, 8476067761,",
				"Seen as a triangle:",
				"[0], 1",
				"[1], 1, 1",
				"[2], 1, 1, 1",
				"[3], 1, 1, 2,  1",
				"[4], 1, 1, 3,  6,   1",
				"[5], 1, 1, 4, 13,  24,    1",
				"[6], 1, 1, 5, 22,  75,  120,     1",
				"[7], 1, 1, 6, 33, 160,  541,   720,     1",
				"[8], 1, 1, 7, 46, 285, 1456,  4683,  5040,     1",
				"[9], 1, 1, 8, 61, 456, 3081, 15904, 47293, 40320, 1"
			],
			"maple": [
				"A := (n, k) -\u003e add(combinat:-eulerian1(k, j)*n^j, j=0..k):",
				"seq(seq(A(n-k, k), k=0..n), n=0..10);",
				"# Alternative:",
				"egf := n -\u003e `if`(n=1, 1/(1-x), (n-1)/(n  - exp((n-1)*x))):",
				"ser := n -\u003e series(egf(n), x, 21):",
				"for n from 0 to 6 do seq(k!*coeff(ser(n), x, k), k=0..9) od;"
			],
			"mathematica": [
				"a[n_, 0] := 1; a[n_, 1] := n!;",
				"a[n_, k_] := (k - 1)^(n + 1)/k HurwitzLerchPhi[1/k, -n, 0];",
				"(* Alternative: *) a[n_, k_] := Sum[StirlingS2[n, j] (k - 1)^(n - j) j!, {j, 0, n}];",
				"Table[Print[Table[a[n, k], {n, 0, 10}]], {k, 0, 8}]"
			],
			"xref": [
				"Cf. A173018, A000012, A000142, A000670, A122704, A255927, A326324."
			],
			"keyword": "nonn,tabl",
			"offset": "0,9",
			"author": "_Peter Luschny_, Jun 27 2019",
			"references": 4,
			"revision": 13,
			"time": "2019-06-28T01:13:31-04:00",
			"created": "2019-06-27T18:27:57-04:00"
		}
	]
}