{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A344482",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 344482,
			"data": "2,3,2,5,7,3,11,5,13,17,7,19,11,23,13,29,31,17,37,19,41,23,43,29,47,53,31,59,37,61,41,67,43,71,47,73,79,53,83,59,89,61,97,67,101,71,103,73,107,109,79,113,83,127,89,131,97,137,101,139,103,149,107,151",
			"name": "Primes, each occurring twice, such that a(C(n)) = a(4*n-C(n)) = prime(n), where C is the Connell sequence (A001614).",
			"comment": [
				"Terms can be arranged in an irregular triangle read by rows in which row r is a permutation P of the primes in the interval [prime(s), prime(s+rlen-1)], where s = 1+(r-1)*(r-2)/2, rlen = 2*r-1 = A005408(r-1) and r \u003e= 1 (see example).",
				"P is the alternating (first term \u003e second term \u003c third term \u003e fourth term \u003c ...) permutation m -\u003e 1, 1 -\u003e 2, m+1 -\u003e 3, 2 -\u003e 4, m+2 -\u003e 5, 3 -\u003e 6, ..., rlen -\u003e rlen where m = ceiling(rlen/2).",
				"The triangle has the following properties.",
				"Row lengths are the positive odd numbers (A005408).",
				"First column is A078721.",
				"Column 3 is A078722 (for n \u003e= 1).",
				"Column 5 is A078724 (for n \u003e= 2).",
				"Column 7 is A078725 (for n \u003e= 3).",
				"Each even column is equal to the column preceding it.",
				"Row records (A011756) are in the right border.",
				"Indices of row records are the positive terms of A000290.",
				"Each row r contains r terms that are duplicated in the next row.",
				"In each row, the sum of terms which are not already listed in the sequence give A007468.",
				"For rows r \u003e= 2, row sum is A007468(r)+A007468(r-1) and row product is A007467(r)*A007467(r-1)."
			],
			"formula": [
				"a(A001614(n)) = a(4*n-A001614(n)) = prime(n)."
			],
			"example": [
				"Written as an irregular triangle the sequence begins:",
				"   2;",
				"   3,   2,   5;",
				"   7,   3,  11,   5,  13;",
				"  17,   7,  19,  11,  23,  13,  29;",
				"  31,  17,  37,  19,  41,  23,  43,  29,  47;",
				"  53,  31,  59,  37,  61,  41,  67,  43,  71,  47,  73;",
				"  79,  53,  83,  59,  89,  61,  97,  67, 101,  71, 103,  73, 107;",
				"  ...",
				"The triangle can be arranged as shown below so that, in every row, each odd position term is equal to the term immediately below it.",
				"                2",
				"             3  2  5",
				"          7  3 11  5 13",
				"      17  7 19 11 23 13 29",
				"   31 17 37 19 41 23 43 29 47",
				"              ..."
			],
			"mathematica": [
				"nterms=64;a=ConstantArray[0,nterms];For[n=1;p=1,n\u003c=nterms,n++,If[a[[n]]==0,a[[n]]=Prime[p];If[(d=4p-n)\u003c=nterms,a[[d]]=a[[n]]];p++]]; a",
				"(* Second program, triangle rows *)",
				"nrows=8;Table[rlen=2r-1;Permute[Prime[Range[s=1+(r-1)(r-2)/2,s+rlen-1]],Join[Range[2,rlen,2],Range[1,rlen,2]]],{r,nrows}]"
			],
			"xref": [
				"Cf. A000040, A117384, A000290, A001614, A005408, A007467, A007468, A011756.",
				"Cf. A078721, A078722, A078724, A078725."
			],
			"keyword": "nonn,tabf",
			"offset": "1,1",
			"author": "_Paolo Xausa_, Aug 16 2021",
			"references": 1,
			"revision": 26,
			"time": "2021-08-26T10:44:57-04:00",
			"created": "2021-08-26T10:44:57-04:00"
		}
	]
}