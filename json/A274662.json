{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A274662",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 274662,
			"data": "1,0,1,0,-3,1,0,4,-5,1,0,-3,13,-7,1,0,6,-25,26,-9,1,0,-12,43,-70,43,-11,1,0,8,-70,157,-147,64,-13,1,0,-3,109,-315,408,-264,89,-15,1,0,13,-155,582,-984,872,-429,118,-17,1,0,-18,201,-1001,2142,-2464,1641,-650,151,-19,1",
			"name": "Triangle T(n, m) appearing in the expansion of Jacobi's elliptic function sn(u, k) divided by sin(v) in terms of the Jacobi nome q and even powers of 2*cos(v), with v = u/((2/Pi)*K(k)).",
			"comment": [
				"The representation of Jacobi's elliptic sn(u, k) function in terms of quotients of theta functions of the variables q (Jacobi nome) and v = u/((2/Pi)*K(k)) with the real quarter period K is",
				"  sn(u, k) = (theta_3(0, q)/theta_2(0, q)) * (theta_1(v, q)/theta_4(v, q)).",
				"  This can be written either in terms of infinite sums or products. (see e.g., Tricomi, p. 176, eq. (3.87), p. 156, eq. (3.51), p. 167, eq. (3.71) with (3.71'), p. 173, eq. (3.81)).",
				"The sums representation involves sin((2*n+1)*v) and cos(2*n*v) functions. Using Chebyshev S and T polynomial (A049310 and A053120) one can write sn(u, k)/sin(v) = Sum_{n \u003e= 0} q^n*Sum_{m = 0..n} T(n, m) * (2*cos(v))^(2*m).",
				"The product representation involves directly (2*cos(v))^2 powers in the q expansion:",
				"  sn(u, k)/sin(v) = Product_{n \u003e= 1} (1 - (q^(2*n)/(1 + q^(2*n))^2)*(2*cos(v))^2) / (1 - (q^(2*n-1)/(1 + q^(2*n-1))^2)*(2*cos(v))^2) = Sum_{n \u003e=0} q^n * Sum_{m = 1..n} T(n, m)*(2*cos(v))^(2*m).",
				"This sn expansion in the v and q variables is used in the scaled phase space coordinate qhat(v, q) of the plane pendulum. See A275790.",
				"An alternative expansion of sn in the variables v and q is given in A274659.",
				"See also the W. Lang link, equations (52) and (53)."
			],
			"reference": [
				"F. Tricomi, Elliptische Funktionen (German translation by M. Krafft of: Funzioni ellittiche), Akademische Verlagsgesellschaft Geest \u0026 Portig K.-G., Leipzig, 1948."
			],
			"link": [
				"Wolfdieter Lang, \u003ca href=\"/A273506/a273506_5.pdf\"\u003eExpansions for phase space coordinates for the plane pendulum\u003c/a\u003e"
			],
			"formula": [
				"sn(u, k) = sin(v)*Sum_{n \u003e= 0} q^n*Sum_{m = 0..n} T(n, m)*(2*cos(v))^(2*m), becoming an identity when q, the Jacobi nome, is replaced by exp(-Pi*K'(k)/K(k)) and v by u/((2/Pi)*K(k)) with the real and imaginary quarter periods K' and K, respectively. For the expansions of q = q(k) see A005797 or better A002103 for q = q((1-k^2)^(1/4)), and for (2/Pi)*K(k) see A038534 / A056982."
			],
			"example": [
				"The triangle T(n, m) begins:",
				"n\\m 0   1    2    3    4    5    6   7   8 9",
				"0:  1",
				"1:  0   1",
				"2:  0  -3    1",
				"3:  0   4   -5    1",
				"4:  0  -3   13   -7    1",
				"5:  0   6  -25   26   -9    1",
				"6:  0 -12   43  -70   43  -11    1",
				"7:  0   8  -70  157 -147   64  -13   1",
				"8:  0  -3  109 -315  408 -264   89 -15   1",
				"9:  0  13 -155  582 -984  872 -429 118 -17 1",
				"...",
				"row n=10: 0 -18 201 -1001 2142 -2464 1641 -650 151 -19 1",
				"...",
				"n=4: the q^4 term of sn(u, k)/sin(v) is -3*(2*cos(v))^2 + 13*(2*cos(v))^4 - 7*(2*cos(v))^6 + (2*cos(v))^8.",
				"One can check the identity for example for u = 1 and k = sqrt(1/2), belonging to v = 0.8472130848  and q = 0.04321391815 (Maple 10 digits), with the result from Maple's sn function sn(1, sqrt(1/2)) = 0.8030018249 (10 digits). If one takes the expansion up to q^4 inclusive one obtains .8030012888 (10 digits)."
			],
			"xref": [
				"Cf. A002103, A005797, A038534/A056982, A049310, A053120, A274659, A275790."
			],
			"keyword": "sign,tabl,easy",
			"offset": "0,5",
			"author": "_Wolfdieter Lang_, Aug 08 2016",
			"references": 2,
			"revision": 20,
			"time": "2016-08-26T17:38:50-04:00",
			"created": "2016-08-13T12:05:03-04:00"
		}
	]
}