{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005101",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5101,
			"id": "M4825",
			"data": "12,18,20,24,30,36,40,42,48,54,56,60,66,70,72,78,80,84,88,90,96,100,102,104,108,112,114,120,126,132,138,140,144,150,156,160,162,168,174,176,180,186,192,196,198,200,204,208,210,216,220,222,224,228,234,240,246,252,258,260,264,270",
			"name": "Abundant numbers (sum of divisors of m exceeds 2m).",
			"comment": [
				"A number m is abundant if sigma(m) \u003e 2m (this sequence), perfect if sigma(m) = 2m (cf. A000396), or deficient if sigma(m) \u003c 2m (cf. A005100), where sigma(m) is the sum of the divisors of m (A000203).",
				"While the first even abundant number is 12 = 2^2*3, the first odd abundant is 945 = 3^3*5*7, the 232nd abundant number!",
				"It appears that for m abundant and \u003e 23, 2*A001055(m) - A101113(m) is NOT 0. - _Eric Desbiaux_, Jun 01 2009",
				"If m is a term so is every positive multiple of m. \"Primitive\" terms are in A091191.",
				"If m=6k (k\u003e=2), then sigma(m) \u003e= 1 + k + 2*k + 3*k + 6*k \u003e 12*k = 2*m. Thus all such m are in the sequence.",
				"According to Deléglise (1998), the abundant numbers have natural density 0.2474 \u003c A(2) \u003c 0.2480. Thus the n-th abundant number is asymptotic to 4.0322*n \u003c n/A(2) \u003c 4.0421*n. - _Daniel Forgues_, Oct 11 2015",
				"From _Bob Selcoe_, Mar 28 2017 (prompted by correspondence with Peter Seymour): (Start)",
				"Applying similar logic as the proof that all multiples of 6 \u003e= 12 appear in the sequence, for all odd primes p:",
				"i) all numbers of the form j*p*2^k (j \u003e= 1) appear in the sequence when p \u003c 2^(k+1) - 1;",
				"ii) no numbers appear when p \u003e 2^(k+1) - 1 (i.e., are deficient and are in A005100);",
				"iii) when p = 2^(k+1) - 1 (i.e., perfect numbers, A000396), j*p*2^k (j \u003e= 2) appear.",
				"Note that redundancies are eliminated when evaluating p only in the interval [2^k, 2^(k+1)].",
				"The first few even terms not of the forms i or iii are {70, 350, 490, 550, 572, 650, 770, ...}. (End)"
			],
			"reference": [
				"L. E. Dickson, Theorems and tables on the sum of the divisors of a number, Quart. J. Pure Appl. Math., Vol. 44 (1913), pp. 264-296.",
				"Richard K. Guy, Unsolved Problems in Number Theory, 3rd Edition, Springer, 2004, Section B2, pp. 74-84.",
				"Clifford A. Pickover, A Passion for Mathematics, Wiley, 2005; see p. 59.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A005101/b005101.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"J. Britton, \u003ca href=\"http://britton.disted.camosun.bc.ca/perfect/jbperfect.htm\"\u003ePerfect Number Analyser\u003c/a\u003e.",
				"C. K. Caldwell, The Prime Glossary, \u003ca href=\"http://primes.utm.edu/glossary/page.php/AbundantNumber.html\"\u003eabundant number\u003c/a\u003e.",
				"Marc Deléglise, \u003ca href=\"http://projecteuclid.org/euclid.em/1048515661\"\u003eBounds for the density of abundant integers\u003c/a\u003e, Experiment. Math., Volume 7, Issue 2 (1998), pp. 137-143.",
				"Jason Earls, \u003ca href=\"https://pdfs.semanticscholar.org/4559/ac50797ddeda688576630c4d92229440a0a3.pdf#page=251\"\u003eOn Smarandache repunit n numbers\u003c/a\u003e, in Smarandache Notions Journal, Vol. 14, No. 1 (2004), page 243.",
				"Christian Kassel and Christophe Reutenauer, \u003ca href=\"https://arxiv.org/abs/1505.07229v3\"\u003eThe zeta function of the Hilbert scheme of n points on a two-dimensional torus\u003c/a\u003e, arXiv:1505.07229v3 [math.AG], 2015. [A later version of this paper has a different title and different contents, and the number-theoretical part of the paper was moved to the publication below.]",
				"Christian Kassel and Christophe Reutenauer, \u003ca href=\"https://arxiv.org/abs/1610.07793\"\u003eComplete determination of the zeta function of the Hilbert scheme of n points on a two-dimensional torus\u003c/a\u003e, arXiv:1610.07793 [math.NT], 2016.",
				"Walter Nissen, \u003ca href=\"http://upforthecount.com/math/abundance.html\"\u003eAbundancy : Some Resources \u003c/a\u003e.",
				"Paul Pollack and Carl Pomerance, \u003ca href=\"https://doi.org/10.1090/btran/10\"\u003eSome problems of Erdős on the sum-of-divisors function\u003c/a\u003e, For Richard Guy on his 99th birthday: May his sequence be unbounded, Trans. Amer. Math. Soc. Ser. B, Vol. 3 (2016), pp. 1-26; \u003ca href=\"http://pollack.uga.edu/reversal-errata.pdf\"\u003eErrata\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/AbundantNumber.html\"\u003eAbundant Number\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Abundance.html\"\u003eAbundance\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Abundant_number\"\u003eAbundant number\u003c/a\u003e.",
				"\u003ca href=\"/index/Cor#core\"\u003eIndex entries for \"core\" sequences\u003c/a\u003e."
			],
			"formula": [
				"a(n) is asymptotic to C*n with C=4.038... (Deléglise, 1998). - _Benoit Cloitre_, Sep 04 2002",
				"A005101 = { n | A033880(n) \u003e 0 }. - _M. F. Hasler_, Apr 19 2012",
				"A001065(a(n)) \u003e a(n). - _Reinhard Zumkeller_, Nov 01 2015"
			],
			"maple": [
				"with(numtheory): for n from 1 to 270 do if sigma(n)\u003e2*n then printf(`%d,`,n) fi: od:",
				"isA005101 := proc(n)",
				"    simplify(numtheory[sigma](n) \u003e 2*n) ;",
				"end proc: # _R. J. Mathar_, Jun 18 2015",
				"A005101 := proc(n)",
				"    option remember ;",
				"    local a ;",
				"    if n =1 then",
				"        12 ;",
				"    else",
				"        a := procname(n-1)+1 ;",
				"        while numtheory[sigma](a) \u003c= 2*a do",
				"            a := a+1 ;",
				"        end do ;",
				"        a ;",
				"    end if ;",
				"end proc: # _R. J. Mathar_, Oct 11 2017"
			],
			"mathematica": [
				"abQ[n_] := DivisorSigma[1, n] \u003e 2n; A005101 = Select[ Range[270], abQ[ # ] \u0026] (* _Robert G. Wilson v_, Sep 15 2005 *)",
				"Select[Range[300], DivisorSigma[1, #] \u003e 2 # \u0026] (* _Vincenzo Librandi_, Oct 12 2015 *)"
			],
			"program": [
				"(PARI) isA005101(n) = (sigma(n) \u003e 2*n) \\\\ _Michael B. Porter_, Nov 07 2009",
				"(Haskell)",
				"a005101 n = a005101_list !! (n-1)",
				"a005101_list = filter (\\x -\u003e a001065 x \u003e x) [1..]",
				"-- _Reinhard Zumkeller_, Nov 01 2015, Jan 21 2013",
				"(Python)",
				"from sympy import divisors",
				"def ok(n): return sum(divisors(n)) \u003e 2*n",
				"print(list(filter(ok, range(1, 271)))) # _Michael S. Branicky_, Aug 29 2021",
				"(Python)",
				"from sympy import divisor_sigma",
				"from itertools import count, islice",
				"def A005101_gen(startvalue=1): return filter(lambda n:divisor_sigma(n) \u003e 2*n, count(max(startvalue, 1))) # generator of terms \u003e= startvalue",
				"A005101_list = list(islice(A005101_gen(), 20)) # _Chai Wah Wu_, Jan 14 2022"
			],
			"xref": [
				"Cf. A005835, A005100, A091194, A091196, A080224, A091191 (primitive).",
				"Cf. A005231 and A006038 (odd abundant numbers).",
				"Cf. A094268 (n consecutive abundant numbers).",
				"Cf. A173490 (even abundant numbers).",
				"Cf. A001065.",
				"Cf. A000396 (perfect numbers).",
				"Cf. A302991."
			],
			"keyword": "nonn,easy,core,nice,changed",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_",
			"references": 283,
			"revision": 149,
			"time": "2022-01-16T09:15:06-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}