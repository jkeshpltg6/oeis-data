{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A093127",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 93127,
			"data": "1,1,1,2,1,5,1,9,3,1,14,14,1,20,40,4,1,27,90,30,1,35,175,125,5,1,44,308,385,55,1,54,504,980,315,6,1,65,780,2184,1274,91,1,77,1155,4410,4116,686,7,1,90,1650,8250,11340,3528,140,1,104,2288,14520,27720,14112,1344,8",
			"name": "Triangle read by rows: differences of Narayana numbers.",
			"comment": [
				"T(n,k) (0 \u003c= k \u003c= n/2) is the number of dissections of a regular (n+2)-gon using k strictly disjoint diagonals (diagonals join nonconsecutive vertices and strictly disjoint means no two cross or share an endpoint).",
				"Row n contains 1 + floor(n/2) entries. - _Emeric Deutsch_, Sep 18 2014",
				"T(n,k) is the number of paths in B(n+1) that do not start with H and have k steps of weight 2 (i.e., H or u). Example: T(3,1) = 5 because the paths in B(4) that do not start with H are hhhh, hHh, hhH, uhd, hud, and udh; the last five contain exactly 1 step of weight 2. - _Emeric Deutsch_, Sep 18 2014",
				"B(n) is the set of lattice paths of weight n that start in (0,0), end on the horizontal axis and never go below this axis, whose steps are of the following four kinds: h = (1,0) of weight 1, H = (1,0) of weight 2, u = (1,1) of weight 2, and d = (1,-1) of weight 1. The weight of a path is the sum of the weights of its steps. - _Emeric Deutsch_, Sep 18 2014"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A093127/b093127.txt\"\u003eRows n = 0..100 of triangle, flattened\u003c/a\u003e",
				"M. Bona and A. Knopfmacher, \u003ca href=\"http://dx.doi.org/10.1007/s00026-010-0060-7\"\u003eOn the probability that certain compositions have the same number of parts\u003c/a\u003e, Ann. Comb., 14 (2010), 291-306. - _Emeric Deutsch_, Sep 18 2014"
			],
			"formula": [
				"T(n, k) = Narayana(n-k+2, k+1) - Narayana(n-k+1, k) where Narayana(n, k) = binomial(n, k)*binomial(n, k-1)/n is A001263.",
				"G.f.: G=G(t,z) satisfies t*z^4*G^2 - (1 - z - 2*t*z^2 - t*z^3 + t^2*z^4)*G + 1 = 0. - _Emeric Deutsch_, Sep 18 2014"
			],
			"example": [
				"T(3,1) = 5 because there are 5 ways to insert a single diagonal into a pentagon.",
				"Triangle begins:",
				"  1;",
				"  1;",
				"  1,  2;",
				"  1,  5;",
				"  1,  9,  3;",
				"  1, 14, 14;",
				"  1, 20, 40,  4;",
				"  1, 27, 90, 30;"
			],
			"maple": [
				"eq := t*z^4*G^2-(1-z-2*t*z^2-t*z^3+t^2*z^4)*G+1 = 0: G := RootOf(eq, G): Gser := simplify(series(G, z = 0, 18)): for n from 0 to 15 do P[n] := sort(coeff(Gser, z, n)) end do: for n from 0 to 15 do seq(coeff(P[n], t, k), k = 0 .. floor((1/2)*n)) end do; # yields sequence in triangular form # _Emeric Deutsch_, Sep 18 2014",
				"T := proc (n, k) if (1/2)*n+1/2 \u003c k then 0 else binomial(n-k+2, k+1)*binomial(n-k+2, k)/(n-k+2)-binomial(n-k+1, k)*binomial(n-k+1, k-1)/(n-k+1) end if end proc: for n from 0 to 15 do seq(T(n, k), k = 0 .. floor((1/2)*n)) end do; # yields sequence in triangular form # _Emeric Deutsch_, Sep 18 2014"
			],
			"mathematica": [
				"Nara[n_, k_]:= Binomial[n, k]*Binomial[n, k-1]/n; T[n_, k_]:= Nara[n-k+2, k+1] - Nara[n-k+1, k]; Table[T[n, k], {n,0,15}, {k,0,Floor[n/2]}]//Flatten (* _G. C. Greubel_, Dec 28 2019 *)"
			],
			"program": [
				"(PARI) T(n,k) = my(b=binomial); b(n-k+2,k+1)*b(n-k+2,k)/(n-k+2) - b(n-k+1,k)* b(n-k+1,k-1)/(n-k+1);",
				"for(n=0,15, for(k=0, n\\2, print1(T(n,k), \", \"))) \\\\ _G. C. Greubel_, Dec 28 2019",
				"(MAGMA) B:=Binomial; T:= func\u003c n, k | B(n-k+2,k+1)*B(n-k+2,k)/(n-k+2) - B(n-k+1,k)*B(n-k+1,k-1)/(n-k+1) \u003e;",
				"[T(n,k): k in [0..Floor(n/2)], n in [0..15]]; // _G. C. Greubel_, Dec 28 2019",
				"(Sage)",
				"b=binomial;",
				"def T(n,k): return b(n-k+2,k+1)*b(n-k+2,k)/(n-k+2) - b(n-k+1,k)* b(n-k+1,k-1)/(n-k+1)",
				"[[T(n,k) for k in (0..floor(n/2))] for n in (0..15)] # _G. C. Greubel_, Dec 28 2019",
				"(GAP)",
				"B:=Binomial;;",
				"T:= function(n,k)",
				"    return B(n-k+2,k+1)*B(n-k+2,k)/(n-k+2) - B(n-k+1,k)*B(n-k+1,k-1)/(n-k+1);",
				"    end;",
				"Flat(List([0..15], n-\u003e List([0..Int(n/2)], k-\u003e T(n,k) ))); # _G. C. Greubel_, Dec 28 2019"
			],
			"keyword": "easy,nonn,tabf",
			"offset": "0,4",
			"author": "_David Callan_, Mar 23 2004",
			"references": 2,
			"revision": 24,
			"time": "2019-12-29T08:41:23-05:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}