{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A250030",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 250030,
			"data": "0,3,2,1,1,2,3,2,5,4,3,4,3,2,4,3,3,4,3,2,3,4,4,3,7,6,4,3,5,4,5,2,5,4,3,4,5,4,6,5,4,5,4,3,2,3,5,3,3,2,3,6,3,6,6,5,7,6,6,2,4,5,6,5,4,4,4,3,7,6,4,3,4,4,4,3,3,5,6,5,3,5,5,4,6,5,5,4,3,6,5,4,4,3,4,4,5,4,6,5,3,4,4,3,5,4,5,5,6,5,7,5,5",
			"name": "a(n) is the number of iterations of A008474 needed to reach 5, starting at n, where A008474(Product p_i^e_i) = Sum(p_i+e_i).",
			"comment": [
				"If the canonical representation of m is Product(p(i)^k(i)), where p(i) is the i-th prime and k(i) is its exponent, then A008474(m) equals Sum(p(i)+k(i)).",
				"Conjecture 1: for m \u003e 4, by iterating the map m -\u003e A008474(m) one always reaches 5. Tested for m up to 320000.",
				"a(n) = -1 in the cases (if such exist) where the iteration acting on n does not reach 5. - _Ivan N. Ianakiev_, Jun 03 2015",
				"a(A247095(n)) = n and a(m) != n for m \u003c A247095(n). - _Reinhard Zumkeller_, Nov 18 2014",
				"Conjecture 2: Let S(n) be the sum of the first a(n) numbers in the n-to-5 orbit, i.e., the sum of all the numbers in the orbit without 5. Except for n in [8,9], S(n) is not divisible by n. Verified for n up to 10^6. - _Ivan N. Ianakiev_, May 06 2015",
				"From _Ivan N. Ianakiev_, Aug 12 2015: (Start)",
				"Proof outline for Conjecture 1:",
				"1. a(n) \u003c n is true for:",
				"a) all composite numbers n such that A001221(n) = 1 in the cases where the exponent of the prime is greater than 2,",
				"b) all composite numbers n such that A001221(n) = 2 in the cases where one of the exponents of the primes is greater than 1, and",
				"c) all composite numbers n such that A001221(n) \u003e 2, the smallest of which is 30 = 2^1*3^1*5^1.",
				"2. a(a(n)) \u003c n is true for all primes p such that p+1 equals a composite number handled in item 1 above.",
				"3. The only thing needed to complete the proof is to exhaustively confirm the conjecture for all numbers in [5, 29] that were not handled in items 1 and 2 above, i.e., 5, 6, 8, 9, 10, 11, 13, 14, 15, 21, 22, 25, 26. (End)"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A250030/b250030.txt\"\u003eTable of n, a(n) for n = 5..10000\u003c/a\u003e"
			],
			"example": [
				"3 iterations are needed to start at 11 and finish at 5 (11 -\u003e 12 -\u003e 8 -\u003e 5), therefore a(11) is 3.",
				"Conjecture 2: For n = 11, S(n) = 11 + 12 + 8 = 31, which is not divisible by 11. - _Ivan N. Ianakiev_, May 06 2015"
			],
			"maple": [
				"f:= n -\u003e convert(map(convert,ifactors(n)[2],`+`),`+`):",
				"a:= proc(n) option remember;",
				"if n = 5 then 0 else 1+procname(f(n)) fi",
				"end proc:",
				"4, seq(a(n),n=6..200); # _Robert Israel_, May 11 2015"
			],
			"mathematica": [
				"trajLen={0};Do[lst={};g[n_]:=Total[Flatten[FactorInteger[n]]];While[n\u003e5,n=g[n];AppendTo[lst,n]];AppendTo[trajLen,Length[lst]],{n,6,105}];trajLen"
			],
			"program": [
				"(Haskell)",
				"a250030 n = snd $ until ((== 5) . fst)",
				"                  (\\(x, s) -\u003e (a008474 x, s + 1)) (a008474 n, 1)",
				"-- _Reinhard Zumkeller_, Nov 18 2014",
				"(PARI) A008474(n)=my(f=factor(n)); sum(i=1,#f~,f[i,1]+f[i,2])",
				"a(n)=my(k); while(n!=5, n=A008474(n); k++); k \\\\ _Charles R Greathouse IV_, Jun 03 2015"
			],
			"xref": [
				"Cf. A008474, A247095."
			],
			"keyword": "nonn",
			"offset": "5,2",
			"author": "_Ivan N. Ianakiev_, Nov 11 2014",
			"ext": [
				"a(5) in b-file corrected by _Andrew Howroyd_, Feb 22 2018"
			],
			"references": 3,
			"revision": 45,
			"time": "2018-02-22T20:19:09-05:00",
			"created": "2014-11-18T05:24:05-05:00"
		}
	]
}