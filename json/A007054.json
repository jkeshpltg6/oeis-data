{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007054",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7054,
			"id": "M2243",
			"data": "3,2,3,6,14,36,99,286,858,2652,8398,27132,89148,297160,1002915,3421710,11785890,40940460,143291610,504932340,1790214660,6382504440,22870640910,82334307276,297670187844,1080432533656,3935861372604,14386251913656,52749590350072",
			"name": "Super ballot numbers: 6(2n)!/(n!(n+2)!).",
			"comment": [
				"Hankel transform is 2n+3. The Hankel transform of a(n+1) is n+2. The sequence a(n)-2*0^n has Hankel transform A110331(n). - _Paul Barry_, Jul 20 2008",
				"Number of pairs of Dyck paths of total length 2*n with heights differing by at most 1. (Gessel/Xin, p.2). - _Joerg Arndt_, Sep 01 2012"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A007054/b007054.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"E. Allen and I. Gheorghiciuc, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Allen/gheo.html\"\u003eA Weighted Interpretation for the Super Catalan Numbers\u003c/a\u003e, J. Int. Seq. 17 (2014) # 14.10.7.",
				"D. Callan, \u003ca href=\"http://arXiv.org/abs/math.CO/0408117\"\u003eA combinatorial interpretation for a super-Catalan recurrence\u003c/a\u003e, arXiv:math/0408117 [math.CO], 2004.",
				"David Callan, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL8/Callan/callan301.html\"\u003eA Combinatorial Interpretation for a Super-Catalan Recurrence\u003c/a\u003e, Journal of Integer Sequences, Vol. 8 (2005), Article 05.1.8.",
				"D. Callan, \u003ca href=\"http://arxiv.org/abs/1204.5704\"\u003eA variant of Touchard's Catalan number identity\u003c/a\u003e, arXiv preprint arXiv:1204.5704 [math.CO], 2012. - From _N. J. A. Sloane_, Oct 10 2012",
				"Ira M. Gessel, \u003ca href=\"/A007054/a007054.pdf\"\u003eLetter to N. J. A. Sloane, Jul. 1992\u003c/a\u003e",
				"Ira M. Gessel, \u003ca href=\"http://people.brandeis.edu/~gessel/homepage/papers/superballot.pdf\"\u003eSuper ballot numbers\u003c/a\u003e, J. Symbolic Comp., 14 (1992), 179-194",
				"Ira M. Gessel, \u003ca href=\"http://people.brandeis.edu/~gessel/homepage/slides/nonneg.pdf\"\u003eRational Functions With Nonnegative Integer Coefficients\u003c/a\u003e, 50th Séminaire Lotharingien de Combinatoire, 2003.",
				"Ira M. Gessel and Guoce Xin, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL8/Gessel/xin.html\"\u003eA Combinatorial Interpretation of the Numbers 6(2n)!/n!(n+2)!\u003c/a\u003e, Journal of Integer Sequences, Vol. 8 (2005), Article 05.2.3.",
				"Ira M. Gessel and Guoce Xin, \u003ca href=\"http://arxiv.org/abs/math/0401300\"\u003eA Combinatorial Interpretation of The Numbers 6(2n)! /n! (n+2)!\u003c/a\u003e, arXiv:math/0401300v2 [math.CO], 2004.",
				"N. Pippenger and K. Schleich, \u003ca href=\"http://arxiv.org/abs/gr-qc/0306049\"\u003eTopological characteristics of random triangulated surfaces (section 7)\u003c/a\u003e, Random Structures Algorithms 28 (2006) 247-288; arXiv:gr-qc/0306049v1.",
				"G. Schaeffer, \u003ca href=\"http://www.loria.fr/~schaeffe/Pub/Conjugacy/superCat.ps\"\u003eA combinatorial interpretation of super-Catalan numbers of order two, (2001)."
			],
			"formula": [
				"G.f.: c(x)*(4-c(x)), where c(x) = g.f. for Catalan numbers A000108; Convolution of Catalan numbers with negative Catalan numbers but -C(0)=-1 replaced by 3. - _Wolfdieter Lang_",
				"E.g.f. in Maple notation: exp(2*x)*(4*x*(BesselI(0, 2*x)-BesselI(1, 2*x))-BesselI(1, 2*x))/x. Integral representation as n-th moment of a positive function on [0, 4], in Maple notation: a(n)=int(x^n*(4-x)^(3/2)/x^(1/2), x=0..4)/(2*Pi), n=0, 1... This representation is unique. - _Karol A. Penson_, Oct 10 2001",
				"E.g.f.: sum(n\u003e=0, a(n)*x^(2*n) ) = 3*BesselI(2, 2x).",
				"a(n) = A000108(n)*6/(n+2). - _Philippe Deléham_, Oct 30 2007",
				"a(n+1) = 2*(A000108(n+2)-A000108(n+1))/(n+1); - _Paul Barry_, Jul 20 2008",
				"G.f.: ((6-4*sqrt(1-4*x))*x+sqrt(1-4*x)-1)/(2*x^2) - _Harvey P. Dale_, Oct 05 2011",
				"a(n) = 4*A000108(n) - A000108(n+1) (Gessel/Xin, p. 2). - _Joerg Arndt_, Sep 01 2012",
				"D-finite with recurrence (n+2)*a(n) +2*(-2*n+1)*a(n-1)=0. - _R. J. Mathar_, Dec 03 2012",
				"G.f.: 1/(x^2*G(0)) + 3/x -1/2/x^2, where G(k)= 1 + 1/(1 - 2*x*(2*k+3)/(2*x*(2*k+3) + (k+1)/G(k+1))); (continued fraction). - _Sergei N. Gladkovskii_, Jun 06 2013",
				"G.f.: 3/x - 1/(2*x^2) + G(0)/(4*x^2), where G(k)= 1 + 1/(1 - 2*x*(2*k-3)/(2*x*(2*k-3) + (k+1)/G(k+1) )); (continued fraction). - _Sergei N. Gladkovskii_, Jul 18 2013",
				"0 = a(n)*(+16*a(n+1) - 14*a(n+2)) + a(n+1)*(+6*a(n+1) + a(n+2)) for all n in Z. - _Michael Somos_, Sep 18 2014",
				"A002421(n+2) = 2*a(n) for all n in Z. - _Michael Somos_, Sep 18 2014",
				"a(n) = 3*(2*n)!*[x^(2*n)]hypergeometric([],[3],x^2). - _Peter Luschny_, Feb 01 2015",
				"a(n) = 6*4^n*Gamma(1/2+n)/(sqrt(Pi)*Gamma(3+n)). - _Peter Luschny_, Dec 14 2015",
				"a(n) = (-4)^(2 + n)*binomial(3/2, 2 + n)/2. - _Peter Luschny_, Nov 04 2021"
			],
			"maple": [
				"seq(3*(2*n)!/(n!)^2/binomial(n+2,n), n=0..22); # _Zerinvary Lajos_, Jun 28 2007",
				"A007054 := n -\u003e 6*4^n*GAMMA(1/2+n)/(sqrt(Pi)*GAMMA(3+n)):",
				"seq(A007054(n),n=0..28); # _Peter Luschny_, Dec 14 2015"
			],
			"mathematica": [
				"Table[6(2n)!/(n!(n+2)!),{n,0,30}] (* or *) CoefficientList[Series[ (-1+Sqrt[1-4*x]+(6-4*Sqrt[1-4*x])*x)/(2*x^2),{x,0,30}],x] (* _Harvey P. Dale_, Oct 05 2011 *)"
			],
			"program": [
				"(MAGMA) [6*Factorial(2*n)/(Factorial(n)*Factorial(n+2)): n in [0..30]]; // _Vincenzo Librandi_, Aug 20 2011",
				"(PARI) a(n)=6*(2*n)!/(n!*(n+2)!); /* _Joerg Arndt_, Sep 01 2012 */",
				"(Sage)",
				"def A007054(n): return (-4)^(2 + n)*binomial(3/2, 2 + n)/2",
				"print([A007054(n) for n in range(29)])  # _Peter Luschny_, Nov 04 2021"
			],
			"xref": [
				"Cf. A002421, A007272, A091712, A000257."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_, _Mira Bernstein_, _Ira M. Gessel_",
			"ext": [
				"Corrected and extended by _Vincenzo Librandi_, Aug 20 2011"
			],
			"references": 20,
			"revision": 101,
			"time": "2021-11-04T06:25:11-04:00",
			"created": "1994-04-28T03:00:00-04:00"
		}
	]
}