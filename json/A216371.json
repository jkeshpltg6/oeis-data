{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A216371",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 216371,
			"data": "3,5,7,11,13,19,23,29,37,47,53,59,61,67,71,79,83,101,103,107,131,139,149,163,167,173,179,181,191,197,199,211,227,239,263,269,271,293,311,317,347,349,359,367,373,379,383,389,419,421,443,461,463,467,479,487",
			"name": "Odd primes with one coach: primes p such that A135303((p-1)/2) = 1.",
			"comment": [
				"Given that prime p has only one coach, the corresponding value of k in A003558 must be (p-1)/2, and vice versa. Using the Coach theorem of Jean Pedersen et al., phi(b) = 2 * c * k, with b odd. Let b = p, prime. Then phi(p) = (p-1), and k must be (p-1)/2 iff c = 1. Or, phi(p) = (p-1) = 2 * 1 * (p-1)/2.",
				"Conjecture relating to odd integers: iff an integer is in the set A216371 and is either of the form 4q - 1 or 4q + 1, (q\u003e0); then the top row of its coach (cf. A003558) is composed of a permutation of the first q odd integers. Examples: 11 is of the form 4q - 1, q = 3; with the top row of its coach [1, 5, 3]. 13 is of the form 4q + 1, q = 3; so has a coach of [1, 3, 5]. 37 is of the form 4q + 1, q = 9; so has a coach with the top row composed of a permutation of the first 9 odd integers: [1, 9, 7, 15, 11, 13, 3, 17, 5]. - _Gary W. Adamson_, Sep 08 2012",
				"These are also the odd primes a(n) for which there is only one periodic Schick sequence (see the reference, and also the Brändli and Beyne link, eq. (2) for the recurrence but using various inputs. See also a comment in A332439). This sequence has primitive period length (named pes in Schick's book) A003558((a(n)-1)/2) = A005034(a(n)) = A000010(a(n))/2 = (a(n) - 1)/2, for n \u003e= 1. - _Wolfdieter Lang_, Apr 09 2020"
			],
			"reference": [
				"P. Hilton and J. Pedersen, A Mathematical Tapestry, Demonstrating the Beautiful Unity of Mathematics, 2010, Cambridge University Press, pages 260-264.",
				"Carl Schick, Trigonometrie und unterhaltsame Zahlentheorie, Bokos Druck, Zürich, 2003 (ISBN 3-9522917-0-6). Tables 3.1 to 3.10, for odd p = 3..113 (with gaps), pp. 158-166."
			],
			"link": [
				"T. D. Noe and Charles R Greathouse IV, \u003ca href=\"/A216371/b216371.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (first 1000 terms from T. D. Noe)",
				"Gerold Brändli and Tim Beyne, \u003ca href=\"https://arxiv.org/abs/1504.02757\"\u003eModified Congruence Modulo n with Half the Amount of Residues\u003c/a\u003e, arXiv:1504.02757 [math.NT], 2016.",
				"Marcelo E. Coniglio, Francesc Esteva, Tommaso Flaminio, and Lluis Godo, \u003ca href=\"https://arxiv.org/abs/2103.07548\"\u003eOn the expressive power of Lukasiewicz's square operator\u003c/a\u003e, arXiv:2103.07548 [math.LO], 2021."
			],
			"formula": [
				"Odd primes p such that 2^m is not 1 or -1 mod p for 0 \u003c m \u003c (p-1)/2. - _Charles R Greathouse IV_, Sep 15 2012",
				"a(n) = 2*A054639(n) + 1. - _L. Edson Jeffery_, Dec 18 2012"
			],
			"example": [
				"Prime 23 has a k value of 11 = (23 - 1)/2 (Cf. A003558(11). It follows that 23 has only one coach (A135303(11) = 1). 23 is thus in the set. On the other hand 31 is not in the set since A135303(15) shows 3 coaches, with A003558(15) = 5.",
				"13 is in the set since A135303(6) = 1; but 17 isn't since A135303(8) = 2."
			],
			"maple": [
				"isA216371 := proc(n)",
				"    if isprime(n) then",
				"        if A135303((n-1)/2) = 1 then",
				"            true;",
				"        else",
				"            false;",
				"        end if;",
				"    else",
				"        false;",
				"    end if;",
				"end proc:",
				"A216371 := proc(n)",
				"    local p;",
				"    if n = 1 then",
				"        3;",
				"    else",
				"        p := nextprime(procname(n-1)) ;",
				"        while true do",
				"            if isA216371(p) then",
				"                return p;",
				"            end if;",
				"            p := nextprime(p) ;",
				"        end do:",
				"    end if;",
				"end proc:",
				"seq(A216371(n),n=1..40) ; # _R. J. Mathar_, Dec 01 2014"
			],
			"mathematica": [
				"Suborder[a_, n_] := If[n \u003e 1 \u0026\u0026 GCD[a, n] == 1, Min[MultiplicativeOrder[a, n, {-1, 1}]], 0]; nn = 150; Select[Prime[Range[2, nn]], EulerPhi[#]/(2*Suborder[2, #]) == 1 \u0026] (* _T. D. Noe_, Sep 18 2012 *)",
				"f[p_] := Sum[Cos[2^n Pi/((2 p + 1))], {n, p}]; 1 + 2 * Select[Range[500], Reduce[f[#] == -1/2, Rationals] \u0026]; (* _Gerry Martens_, May 01 2016 *)"
			],
			"program": [
				"(PARI) is(p)=for(m=1,p\\2-1, if(abs(centerlift(Mod(2,p)^m))==1, return(0))); p\u003e2 \u0026\u0026 isprime(p) \\\\ _Charles R Greathouse IV_, Sep 18 2012"
			],
			"xref": [
				"Cf. A000010, A000040, A003558, A005034, A054639, A135303."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Gary W. Adamson_, Sep 05 2012",
			"references": 8,
			"revision": 72,
			"time": "2021-05-28T02:46:18-04:00",
			"created": "2012-09-07T14:25:55-04:00"
		}
	]
}