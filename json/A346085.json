{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A346085",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 346085,
			"data": "1,0,1,0,1,1,0,4,0,2,0,15,3,0,6,0,96,0,0,0,24,0,455,105,40,0,0,120,0,4320,0,0,0,0,0,720,0,29295,4725,0,1260,0,0,0,5040,0,300160,0,22400,0,0,0,0,0,40320,0,2663199,530145,0,0,72576,0,0,0,0,362880",
			"name": "Number T(n,k) of permutations of [n] such that k is the GCD of the cycle lengths; triangle T(n,k), n\u003e=0, 0\u003c=k\u003c=n, read by rows.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A346085/b346085.txt\"\u003eRows n = 0..140, flattened\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Permutation\"\u003ePermutation\u003c/a\u003e"
			],
			"formula": [
				"Sum_{k=1..n} k * T(n,k) = A346066(n)."
			],
			"example": [
				"T(3,1) = 4: (1)(23), (13)(2), (12)(3), (1)(2)(3).",
				"T(4,4) = 6: (1234), (1243), (1324), (1342), (1423), (1432).",
				"Triangle T(n,k) begins:",
				"  1;",
				"  0,       1;",
				"  0,       1,      1;",
				"  0,       4,      0,     2;",
				"  0,      15,      3,     0,    6;",
				"  0,      96,      0,     0,    0,    24;",
				"  0,     455,    105,    40,    0,     0, 120;",
				"  0,    4320,      0,     0,    0,     0,   0, 720;",
				"  0,   29295,   4725,     0, 1260,     0,   0,   0, 5040;",
				"  0,  300160,      0, 22400,    0,     0,   0,   0,    0, 40320;",
				"  0, 2663199, 530145,     0,    0, 72576,   0,   0,    0,     0, 362880;",
				"  ..."
			],
			"maple": [
				"b:= proc(n, g) option remember; `if`(n=0, x^g, add((j-1)!",
				"      *b(n-j, igcd(g, j))*binomial(n-1, j-1), j=1..n))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=0..n))(b(n, 0)):",
				"seq(T(n), n=0..12);"
			],
			"mathematica": [
				"b[n_, g_] := b[n, g] = If[n == 0, x^g, Sum[(j - 1)!*",
				"     b[n - j, GCD[g, j]] Binomial[n - 1, j - 1], {j, n}]];",
				"T[n_] := CoefficientList[b[n, 0], x];",
				"Table[T[n], {n, 0, 12}] // Flatten (* _Jean-François Alcover_, Aug 30 2021, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Columns k=0-1 give: A000007, A079128.",
				"Even bisection of column k=2 gives A346086.",
				"Row sums give A000142.",
				"T(2n,n) gives A110468(n-1) for n \u003e= 1.",
				"Cf. A057731, A346066."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Alois P. Heinz_, Jul 04 2021",
			"references": 4,
			"revision": 19,
			"time": "2021-08-30T12:12:36-04:00",
			"created": "2021-07-05T07:15:20-04:00"
		}
	]
}