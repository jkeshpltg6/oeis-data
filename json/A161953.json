{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A161953",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 161953,
			"data": "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,342,371,520,584,645,1189,1456,1457,1547,1611,2240,2241,2458,2729,2755,3240,3689,3744,3745,47314,79225,177922,177954,368764,369788,786656,786657,787680,787681,811239,812263,819424,819425,820448,820449,909360",
			"name": "Base-16 Armstrong or narcissistic numbers (written in base 10).",
			"comment": [
				"Whenever 16|a(n) (n = 22, 26, 33, 41, 43, 47, 49, 51, 53, 61, 116, 149, 157, 196, 198, 204, 206, 243, 247), then a(n+1) = a(n) + 1. Zero also satisfies the definition (n = Sum_{i=1..k} d[i]^k where d[1..k] are the base-16 digits of n), but this sequence only considers positive terms. - _M. F. Hasler_, Nov 22 2019"
			],
			"link": [
				"Joseph Myers, \u003ca href=\"/A161953/b161953.txt\"\u003eTable of n, a(n) for n=1..293\u003c/a\u003e (the full list of terms, from Winter)",
				"Henk Koppelaar and Peyman Nasehpour, \u003ca href=\"https://arxiv.org/abs/2008.08187\"\u003eOn Hardy's Apology Numbers\u003c/a\u003e, arXiv:2008.08187 [math.NT], 2020.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/NarcissisticNumber.html\"\u003eNarcissistic Number\u003c/a\u003e",
				"D. T. Winter, \u003ca href=\"https://deimel.org/rec_math/dik2.htm\"\u003eTable of Armstrong Numbers\u003c/a\u003e"
			],
			"example": [
				"645 is in the sequence because 645 is 285 in hexadecimal and 2^3 + 8^3 + 5^3 = 645. (The exponent 3 is the number of hexadecimal digits.)"
			],
			"mathematica": [
				"Select[Range[10^7], # == Total[IntegerDigits[#, 16]^IntegerLength[#, 16]] \u0026] (* _Michael De Vlieger_, Nov 04 2020 *)"
			],
			"program": [
				"(PARI) isok(n) = {my(b=16, d=digits(n, b), e=#d); sum(k=1, #d, d[k]^e) == n;} \\\\ _Michel Marcus_, Feb 25 2019",
				"(PARI) select( is_A161953(n)={n==vecsum([d^#n|d\u003c-n=digits(n,16)])}, [1..10^5]) \\\\ _M. F. Hasler_, Nov 22 2019"
			],
			"xref": [
				"In other bases: A010344 (base 4), A010346 (base 5), A010348 (base 6), A010350 (base 7), A010354 (base 8), A010353 (base 9), A005188 (base 10), A161948 (base 11), A161949 (base 12), A161950 (base 13), A161951 (base 14), A161952 (base 15)."
			],
			"keyword": "base,fini,full,nonn",
			"offset": "1,2",
			"author": "_Joseph Myers_, Jun 22 2009",
			"ext": [
				"Terms sorted in increasing order by _Pontus von Brömssen_, Mar 03 2019"
			],
			"references": 13,
			"revision": 30,
			"time": "2020-11-05T06:46:16-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}