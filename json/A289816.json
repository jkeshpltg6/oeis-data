{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A289816",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 289816,
			"data": "1,1,2,1,1,2,3,3,6,1,1,2,1,1,2,3,3,6,4,5,10,4,5,10,12,15,30,1,1,2,1,1,2,3,3,6,1,1,2,1,1,2,3,3,6,4,5,10,4,5,10,12,15,30,5,7,14,5,7,14,15,21,42,5,7,14,5,7,14,15,21,42,20,35,70,20,35,70",
			"name": "The second of a pair of coprime numbers whose factorizations depend on the ternary representation of n (See Comments for precise definition).",
			"comment": [
				"For n \u003e= 0, with ternary representation Sum_{i=1..k} t_i * 3^e_i (all t_i in {1, 2} and all e_i distinct and in increasing order):",
				"- let S(0) = A000961 \\ { 1 },",
				"- and S(i) = S(i-1) \\ { p^(f + j), with p^f = the (e_i+1)-th term of S(i-1) and j \u003e 0 } for any i=1..k,",
				"- then a(n) = Product_{i=1..k such that t_i=2} \"the (e_i+1)-th term of S(k)\".",
				"See A289815 for the first coprime number and additional comments.",
				"The number of distinct prime factors of a(n) equals the number of twos in the ternary representation of n."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A289816/b289816.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A289815(A004488(n)) for any n \u003e= 0.",
				"a(A005836(n)) = 1 for any n \u003e 0.",
				"a(2 * A005836(n)) = A289272(n-1) for any n \u003e 0."
			],
			"example": [
				"For n=42:",
				"- 42 = 2*3^1 + 1*3^2 + 1*3^3,",
				"- S(0) = { 2, 3, 4, 5, 7, 8, 9, 11, 13, 16, 17, 19, 23, 25, 27, 29, ... },",
				"- S(1) = S(0) \\ { 3^(1+j) with j \u003e 0 }",
				"       = { 2, 3, 4, 5, 7, 8,    11, 13, 16, 17, 19, 23, 25,     29, ... },",
				"- S(2) = S(1) \\ { 2^(2+j) with j \u003e 0 }",
				"       = { 2, 3, 4, 5, 7,       11, 13,     17, 19, 23, 25,     29, ... },",
				"- S(3) = S(2) \\ { 5^(1+j) with j \u003e 0 }",
				"       = { 2, 3, 4, 5, 7,       11, 13,     17, 19, 23,         29, ... },",
				"- a(42) = 3."
			],
			"program": [
				"(PARI) a(n) = my (v=1, x=1);                   \\",
				"       for (o=2, oo,                           \\",
				"           if (n==0, return (v));              \\",
				"           if (gcd(x,o)==1 \u0026\u0026 omega(o)==1,     \\",
				"               if (n % 3,    x *= o);          \\",
				"               if (n % 3==2, v *= o);          \\",
				"               n \\= 3;                         \\",
				"           );                                  \\",
				"       );",
				"(Python)",
				"from sympy import gcd, primefactors",
				"def omega(n): return 0 if n==1 else len(primefactors(n))",
				"def a(n):",
				"    v, x, o = 1, 1, 2",
				"    while True:",
				"        if n==0: return v",
				"        if gcd(x, o)==1 and omega(o)==1:",
				"            if n%3: x*=o",
				"            if n%3==2:v*=o",
				"            n //= 3",
				"        o+=1",
				"print([a(n) for n in range(101)]) # _Indranil Ghosh_, Aug 02 2017"
			],
			"xref": [
				"Cf. A000961, A004488, A289815."
			],
			"keyword": "nonn,base,look",
			"offset": "0,3",
			"author": "_Rémy Sigrist_, Jul 12 2017",
			"references": 5,
			"revision": 34,
			"time": "2021-04-21T07:14:08-04:00",
			"created": "2017-07-14T10:33:53-04:00"
		}
	]
}