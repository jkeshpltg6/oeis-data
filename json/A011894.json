{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A011894",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 11894,
			"data": "0,0,0,0,2,5,10,17,28,42,60,82,110,143,182,227,280,340,408,484,570,665,770,885,1012,1150,1300,1462,1638,1827,2030,2247,2480,2728,2992,3272,3570,3885,4218,4569,4940",
			"name": "a(n) = floor(n(n-1)(n-2)/12).",
			"comment": [
				"a(n+1) = floor((n^3-n)/12) is an upper bound for the Kirchhoff index of a circulant graph with n vertices [Zhang \u0026 Yang]. - _R. J. Mathar_, Apr 26 2007",
				"Also the matching number of the n-tetrahedral graph. - _Eric W. Weisstein_, Jun 20 2017"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A011894/b011894.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/JohnsonGraph.html\"\u003eJohnson Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/MatchingNumber.html\"\u003eMatching Number\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/TetrahedralGraph.html\"\u003eTetrahedral Graph\u003c/a\u003e",
				"H. Zhang and Y. Yang, \u003ca href=\"http://dx.doi.org/10.1002/qua.21068\"\u003eResistance Distance and Kirchhoff Index in Circulant Graphs\u003c/a\u003e, Int. J. Quant. Chem. 107 (2007) 330-339.",
				"\u003ca href=\"/index/Rec#order_07\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1,1,-3,3,-1)."
			],
			"formula": [
				"a(n) = +3*a(n-1) -3*a(n-2) +a(n-3) +a(n-4) -3*a(n-5) +3*a(n-6) -a(n-7);",
				"G.f.: x^4*(-x+x^2+2) / ( (-1+x)^4*(1+x)*(x^2+1) ). - _R. J. Mathar_, Apr 15 2010",
				"a(n) = (2*n^3-6*n^2+4*n-3*(1-(-1)^n)*(1-(-1)^((2*n-1+(-1)^n)/4)))/24. - _Luce ETIENNE_, Jun 26 2014"
			],
			"maple": [
				"seq(floor(binomial(n,3)/2), n=0..40); # _Zerinvary Lajos_, Jan 12 2009"
			],
			"mathematica": [
				"CoefficientList[Series[x^4*(-x + x^2 + 2)/((-1 + x)^4*(1 + x)*(x^2 + 1)),{x, 0, 50}], x] (* _Vincenzo Librandi_, Jul 07 2012 *)",
				"Table[(3 ((-1)^n - 1) + 2 (n - 2) (n - 1) n + 6 Sin[(n Pi)/2])/24, {n, 20}] (* _Eric W. Weisstein_, Jun 20 2017 *)",
				"LinearRecurrence[{3, -3, 1, 1, -3, 3, -1}, {0, 0, 0, 2, 5, 10, 17}, 20] (* _Eric W. Weisstein_, Jun 20 2017 *)"
			],
			"program": [
				"(Sage) [floor(binomial(n,3)/2) for n in range(0,41)] # [_Zerinvary Lajos_, Dec 01 2009]",
				"(MAGMA) [Floor( n*(n-1)*(n-2)/12): n in [0..50]]; // _Vincenzo Librandi_, Jul 07 2012"
			],
			"keyword": "nonn,easy",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_",
			"references": 1,
			"revision": 41,
			"time": "2019-12-07T12:18:18-05:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}