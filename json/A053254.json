{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A053254",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 53254,
			"data": "1,-1,2,-2,2,-3,4,-4,5,-6,6,-8,10,-10,12,-14,15,-18,20,-22,26,-29,32,-36,40,-44,50,-56,60,-68,76,-82,92,-101,110,-122,134,-146,160,-176,191,-210,230,-248,272,-296,320,-350,380,-410,446,-484,522,-566,612,-660,715,-772,830,-896,966,-1038",
			"name": "Coefficients of the '3rd order' mock theta function nu(q)",
			"comment": [
				"In Watson 1936 the function is denoted by upsilon(q). - _Michael Somos_, Jul 25 2015"
			],
			"reference": [
				"Srinivasa Ramanujan, The Lost Notebook and Other Unpublished Papers, Narosa Publishing House, New Delhi, 1988, p. 31"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A053254/b053254.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Leila A. Dragonette, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-1952-0049927-8\"\u003eSome asymptotic formulas for the mock theta series of Ramanujan\u003c/a\u003e, Trans. Amer. Math. Soc., 72 (1952) 474-500.",
				"George N. Watson, \u003ca href=\"https://doi.org/10.1112/jlms/s1-11.1.55\"\u003eThe final problem: an account of the mock theta functions\u003c/a\u003e, J. London Math. Soc., 11 (1936) 55-80."
			],
			"formula": [
				"G.f.: nu(q) = sum for n \u003e= 0 of q^(n(n+1))/((1+q)(1+q^3)...(1+q^(2n+1)))",
				"(-1)^n a(n) = number of partitions of n in which even parts are distinct and if k occurs then so does every positive even number less than k",
				"G.f.: 1/(1 + x*(1-x)/(1 + x^2*(1-x^2)/(1 + x^3*(1-x^3)/(1 + x^4*(1-x^4)/(1 + x^5*(1-x^5)/(1 + ...)))))), a continued fraction. - _Paul D. Hanna_, Jul 09 2013",
				"a(2*n) = A085140(n). a(2*n + 1) = - A053253(n). - _Michael Somos_, Jul 25 2015",
				"a(n) ~ (-1)^n * exp(Pi*sqrt(n/6)) / (2^(3/2)*sqrt(n)). - _Vaclav Kotesovec_, Jun 15 2019"
			],
			"example": [
				"G.f. = 1 - x + 2*x^2 - 2*x^3 + 2*x^4 - 3*x^5 + 4*x^6 - 4*x^7 + 5*x^8 + ..."
			],
			"mathematica": [
				"Series[Sum[q^(n(n+1))/Product[1+q^(2k+1), {k, 0, n}], {n, 0, 9}], {q, 0, 100}]"
			],
			"program": [
				"(PARI) /* Continued Fraction Expansion: */",
				"{a(n)=local(CF); CF=1+x; for(k=0, n, CF=1/(1 + x^(n-k+1)*(1 - x^(n-k+1))*CF+x*O(x^n))); polcoeff(CF, n)} \\\\ _Paul D. Hanna_, Jul 09 2013"
			],
			"xref": [
				"Other '3rd order' mock theta functions are at A000025, A053250, A053251, A053252, A053253, A053255.",
				"Cf. A058140."
			],
			"keyword": "sign,easy",
			"offset": "0,3",
			"author": "_Dean Hickerson_, Dec 19 1999",
			"references": 12,
			"revision": 25,
			"time": "2019-06-15T03:42:51-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}