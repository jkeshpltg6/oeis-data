{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A132000",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 132000,
			"data": "1,-1,-5,-1,11,24,-5,-50,-53,-1,120,120,11,-170,-250,24,203,288,-5,-362,-264,-50,600,528,-53,-601,-850,-1,550,840,120,-962,-821,120,1440,1200,11,-1370,-1810,-170,1272,1680,-250,-1850,-1320,24,2640,2208,203,-2451",
			"name": "Expansion of (1/3) * b(q) * b(q^2) * c(q)^2 / c(q^2) in powers of q where b(), c() are cubic AGM functions.",
			"comment": [
				"Cubic AGM theta functions: a(q) (see A004016), b(q) (A005928), c(q) (A005882).",
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"reference": [
				"N. J. Fine, Basic Hypergeometric Series and Applications, Amer. Math. Soc., 1988; p. 85, Eq. (32.71)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A132000/b132000.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of phi(-q)^2 * phi(-q^3)^2 * psi(q)^3 / psi(q^3) in powers of q where phi(), psi() are Ramanujan theta functions.",
				"Expansion of eta(q) * eta(q^2)^4 * eta(q^3)^5 / eta(q^6)^4 in powers of q.",
				"Euler transform of period 6 sequence [-1, -5, -6, -5, -1, -6, ...].",
				"a(n) = -b(n) where b() is multiplicative with b(2^e) = 2+((-4)^(e+1)-1)/5, b(3^e) = 1, b(p^e) = (q^(e+1) - 1) / (q-1) where q = p^2*Kronecker(-3, p) if p \u003e 3.",
				"a(3*n) = a(n).",
				"G.f.: 1 - Sum_{k\u003e0} k^2 * Kronecker(-3, k) * x^k / (1 - (-x)^k) = Product_{k\u003e0} (1  - x^(3*k)) * (1 - x^k)^5 / (1 - x^k + x^(2*k))^4.",
				"a(n) = (-1)^n * A113261(n). Convolution of A123330 and A131943.",
				"a(n) = -A132000(n) unless n=0.",
				"Expansion of (9 * phi(-q) * phi(-q^3)^5 - phi(-q)^5 * phi(-q^3)) / 8 in powers of q where phi() is a Ramanujan theta function. - _Michael Somos_, Nov 03 2015",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (6 t)) = 15552^(1/2) (t/i)^3 g(t) where q = exp(2 Pi i t) and g() is the g.f. for A122373. - _Michael Somos_, Nov 03 2015"
			],
			"example": [
				"G.f. = 1 - x - 5*x^2 - x^3 + 11*x^4 + 24*x^5 - 5*x^6 - 50*x^7 - 53*x^8 - x^9 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 1, Boole[n == 0], DivisorSum[ n, #^2 (-1)^# KroneckerSymbol[ -3, #] \u0026]]; (* _Michael Somos_, Nov 03 2015 *)",
				"a[ n_] := SeriesCoefficient[ (1/4) EllipticTheta[ 4, 0, q]^2 EllipticTheta[ 4, 0, q^3]^2 EllipticTheta[ 2, 0, q^(1/2)]^3 / EllipticTheta[ 2, 0, q^(3/2)], {q, 0, n}]; (* _Michael Somos_, Nov 03 2015 *)",
				"a[ n_] := SeriesCoefficient[(9 EllipticTheta[ 4, 0, q] EllipticTheta[ 4, 0, q^3]^5 - EllipticTheta[ 4, 0, q]^5 EllipticTheta[ 4, 0, q^3]) / 8, {q, 0, n}]; (* _Michael Somos_, Nov 03 2015 *)",
				"a[ n_] := SeriesCoefficient[ QPochhammer[ q] QPochhammer[ q^2]^4 QPochhammer[ q^3]^5 / QPochhammer[ q^6]^4, {q, 0, n}]; (* _Michael Somos_, Nov 03 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n==0, sumdiv(n, d, d^2 * (-1)^d * kronecker(-3, d)))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A) * eta(x^2 + A)^4 * eta(x^3 + A)^5 / eta(x^6 + A)^4, n))};",
				"(PARI) {a(n) = my(A, p, e); if( n\u003c1, n==0, A = factor(n); - prod(k=1, matsize(A)[1], [p, e] = A[k, ]; if( p==3, 1, p==2, 2 + ((-4)^(e+1) - 1) / 5, p = p^2 * kronecker(-3, p); (p^(e+1) - 1) / (p-1) )))};",
				"(MAGMA) A := Basis( ModularForms( Gamma1(6), 3), 50); A[1] - A[2] - 5*A[3] - A[4]; /* _Michael Somos_, Nov 03 2015 */"
			],
			"xref": [
				"Cf. A113261, A122373, A123330, A131943."
			],
			"keyword": "sign",
			"offset": "0,3",
			"author": "_Michael Somos_, Aug 06 2007",
			"references": 3,
			"revision": 15,
			"time": "2021-03-12T22:24:44-05:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}