{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A215534",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 215534,
			"data": "1,-1,1,-1,-2,1,-4,-3,-3,1,-27,-16,-6,-4,1,-256,-135,-40,-10,-5,1,-3125,-1536,-405,-80,-15,-6,1,-46656,-21875,-5376,-945,-140,-21,-7,1,-823543,-373248,-87500,-14336,-1890,-224,-28,-8,1,-16777216,-7411887,-1679616,-262500,-32256,-3402,-336,-36,-9,1",
			"name": "Matrix inverse of triangle A088956.",
			"comment": [
				"For commuting lower unitriangular matrices A and B, we define A raised to the matrix power B, denoted A^^B, to be the matrix Exp(B*log(A)). Here Exp is the matrix exponential and the matrix logarithm Log(A) is defined as sum {n \u003e= 1} (-1)^(n+1)*(A-1)^n/n. This triangle, call it M, is related to Pascal's triangle P by M^^M = P^(-1). Also M = P^(-1)^^A088956."
			],
			"link": [
				"T. Copeland, \u003ca href=\"https://tcjpn.wordpress.com/2021/11/20/composition-conjugation-and-the-umbral-calculus-part-i/\"\u003eComposition, Conjugation, and the Umbral Calculus-Part I\u003c/a\u003e, 2021.",
				"Eric W. Weisstein,\u003ca href=\"http://mathworld.wolfram.com/AbelPolynomial.html\"\u003eMathWorld: Abel Polynomial\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = -binomial(n,k)*(n-k-1)^(n-k-1) for n,k \u003e= 0.",
				"E.g.f.: (x/T(x))*exp(t*x) = exp(-T(x))*exp(t*x) = 1 + (-1 + t)*x + (-1 - 2*t + t^2)*x^2/2! + ...., where T(x) := sum {n \u003e= 0} n^(n-1) *x^n/n! denotes the tree function of A000169. The triangle is the exponential Riordan array [x/T(x),x] belonging to the exponential Appell group.",
				"Let A(n,x) = x*(x+n)^(n-1) be an Abel polynomial. This is the triangle of connection constants expressing A(n,x) as a linear combination of the basis polynomials A(k,x+1), 0 \u003c= k \u003c= n. For example, A(4,x) = -27*A(0,x+1) - 16*A(1,x+1) - 6*A(2,x+1) - 4*A(3,x+1) + A(4,x+1) giving row 4 as [-27,-16,-6,-4,1]."
			],
			"example": [
				"Triangle begins",
				".n\\k.|......0......1.....2......3......4......5......6",
				"= = = = = = = = = = = = = = = = = = = = = = = = = = = =",
				"..0..|......1",
				"..1..|.....-1......1",
				"..2..|.....-1.....-2.....1",
				"..3..|.....-4.....-3....-3......1",
				"..4..|....-27....-16....-6.....-4......1",
				"..5..|...-256...-135...-40....-10.....-5......1",
				"..6..|..-3125..-1536..-405....-80....-15.....-6......1",
				"..."
			],
			"mathematica": [
				"(* The function RiordanArray is defined in A256893. *)",
				"rows = 10;",
				"R = RiordanArray[-#/ProductLog[-#]\u0026, #\u0026, rows, True];",
				"R // Flatten (* _Jean-François Alcover_, Jul 20 2019 *)"
			],
			"xref": [
				"Cf. A000169, A007318, A088956."
			],
			"keyword": "sign,easy,tabl",
			"offset": "0,5",
			"author": "_Peter Bala_, Sep 11 2012",
			"references": 1,
			"revision": 13,
			"time": "2021-12-30T14:40:38-05:00",
			"created": "2012-09-11T17:41:16-04:00"
		}
	]
}