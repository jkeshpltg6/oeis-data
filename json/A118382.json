{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A118382",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 118382,
			"data": "1,1,2,1,2,2,1,2,3,1,1,2,3,3,1,2,1,2,4,1,1,1,1,3,2,2,3,1,2,3,4,3,2,1,1,1,1,2,4,1,4,2,1,1,1,3,1,2,1,5,2,2,1,2,3,1,3,3,2,6,1,2,2,1,3,1,3,2,5,1,1,1,1,2,2,1,4,1,4,1,4,1,4,1,2,3,3,1,2,3,3,3,3,3,1,2,1,2,1,1,2,5,1,2,2",
			"name": "Primitive Orloj clock sequences; row n sums to 2n-1.",
			"comment": [
				"An Orloj clock sequence is a finite sequence of positive integers that, when iterated, can be grouped so that the groups sum to successive natural numbers. There is one primitive sequence whose values sum to each odd m; all other sequences can be obtained by repeating and refining these. Refining means splitting one or more terms into values summing to that term. The Orloj clock sequence is the one summing to 15: 1,2,3,4,3,2, with a beautiful up and down pattern.",
				"These are known in some papers as Sindel sequences. It appears that this sequence was submitted prior to the first such publication."
			],
			"link": [
				"Michal Krížek, Alena Šolcová and Lawrence Somer, \u003ca href=\"https://dml.cz/dmlcz/119666\"\u003eConstruction of Šindel sequences\u003c/a\u003e, Comment. Math. Univ. Carolin., 48 (2007), 373-388."
			],
			"formula": [
				"Let b(i),0\u003c=i\u003ck be all the residues of triangular numbers modulo n in order, with b(k)=n. The differences b(i+1)-b(i) are the sequence for n."
			],
			"example": [
				"For a sum of 5, we have 1,2,2, which groups as 1, 2, 2+1, 2+2, 1+2+2, 1+2+2+1, .... This could be refined by splitting the second 2, to give the sequence 1,2,1,1; note that when this is grouped, the two 1's from the refinement always wind up in the same sum.",
				"The array starts:",
				"  1;",
				"  1,2;",
				"  1,2,2;",
				"  1,2,3,1;",
				"  1,2,3,3;",
				"  1,2,1,2,4,1;",
				"  ..."
			],
			"program": [
				"(PARI) {Orloj(n) = local(found,tri,i,last,r); found = vector(n,i,0); found[n] = 1; tri = 0; for(i = 1, if(n%2==0,n-1,n\\2), tri += i; if(tri \u003e= n, tri -= n); found[tri] = 1); last = 0; r = []; for(i = 1, n, if(found[i], r = concat(r, [i-last]); last = i)); r}"
			],
			"xref": [
				"Cf. A028355, A118383. Length of row n is A117484(2n-1) = A000224(2n-1)."
			],
			"keyword": "nonn,tabf",
			"offset": "1,3",
			"author": "_Franklin T. Adams-Watters_, Apr 26 2006",
			"references": 7,
			"revision": 15,
			"time": "2019-09-23T07:03:02-04:00",
			"created": "2006-05-19T03:00:00-04:00"
		}
	]
}