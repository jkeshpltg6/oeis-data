{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A128633",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 128633,
			"data": "1,4,6,4,-3,-12,-8,12,30,20,-30,-72,-46,60,156,96,-117,-300,-188,228,552,344,-420,-1008,-603,732,1770,1048,-1245,-2976,-1776,2088,4908,2900,-3420,-7992,-4658,5460,12756,7408,-8583,-19944,-11564,13344,30756,17744",
			"name": "McKay-Thompson series of class 6E for the Monster group with a(0) = 4.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A128633/b128633.txt\"\u003eTable of n, a(n) for n = -1..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"\u003ca href=\"/index/Mat#McKay_Thompson\"\u003eIndex entries for McKay-Thompson series for Monster simple group\u003c/a\u003e"
			],
			"formula": [
				"Expansion of 3 * (b(q^2)^2 / b(q)) / (c(q^2)^2 / c(q)) in powers of q where b(), c() are cubic AGM theta functions.",
				"Expansion of q^-1 * (psi(q) / psi(q^3))^4 in powers of q where psi() is a Ramanujan theta function.",
				"Expansion of (eta(q^2)^2 * eta(q^3) / (eta(q) * eta(q^6)^2))^4 in powers of q.",
				"Euler transform of period 6 sequence [ 4, -4, 0, -4, 4, 0, ...].",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2)) where f(u, v) = v * (u - 9) * (u - 1) - (u - v)^2.",
				"G.f.: (1/x) * (Product_{k\u003e0} (1 + x^k + x^(2*k)) * (1 - x^k + x^(2*k))^2)^-4.",
				"a(n) = A007258(n) = A105559(n) = A128632(n) unless n = 0."
			],
			"example": [
				"G.f. = 1/q + 4 + 6*q + 4*q^2 - 3*q^3 - 12*q^4 - 8*q^5 + 12*q^6 + 30*q^7 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (EllipticTheta[ 2, 0, q^(1/2)] / EllipticTheta[ 2, 0, q^(3/2)])^4, {q, 0, n}]; (* _Michael Somos_, Nov 12 2015 *)",
				"QP = QPochhammer; s = (QP[q^2]^2*(QP[q^3]/(QP[q]*QP[q^6]^2)))^4 + O[q]^50; CoefficientList[s, q] (* _Jean-François Alcover_, Nov 12 2015, adapted from PARI *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c-1, 0, n++; A = x * O(x^n); polcoeff( (eta(x^2 + A)^2 * eta(x^3 + A) / (eta(x + A) * eta(x^6 + A)^2))^4, n))};"
			],
			"xref": [
				"Cf. A007258, A105559, A128632."
			],
			"keyword": "sign",
			"offset": "-1,2",
			"author": "_Michael Somos_, Mar 15 2007",
			"references": 8,
			"revision": 22,
			"time": "2021-03-12T22:24:44-05:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}