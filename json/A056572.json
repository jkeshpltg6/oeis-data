{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A056572",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 56572,
			"data": "0,1,1,32,243,3125,32768,371293,4084101,45435424,503284375,5584059449,61917364224,686719856393,7615646045657,84459630100000,936668172433707,10387823949447757,115202670521319424,1277617458486664901",
			"name": "Fifth power of Fibonacci numbers A000045.",
			"comment": [
				"Divisibility sequence; that is, if n divides m, then a(n) divides a(m)."
			],
			"reference": [
				"D. E. Knuth, The Art of Computer Programming. Addison-Wesley, Reading, MA, 1969, Vol. 1, p. 85, (exercise 1.2.8. Nr. 30) and p. 492 (solution)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A056572/b056572.txt\"\u003eTable of n, a(n) for n = 0..135\u003c/a\u003e",
				"Mohammad K. Azarian, \u003ca href=\"http://www.m-hikari.com/ijcms/ijcms-2012/37-40-2012/azarianIJCMS37-40-2012.pdf\"\u003eFibonacci Identities as Binomial Sums\u003c/a\u003e, International Journal of Contemporary Mathematical Sciences, Vol. 7, No. 38, 2012, pp. 1871-1876.",
				"Mohammad K. Azarian, \u003ca href=\"http://www.m-hikari.com/ijcms/ijcms-2012/41-44-2012/azarianIJCMS41-44-2012.pdf\"\u003eFibonacci Identities as Binomial Sums II\u003c/a\u003e, International Journal of Contemporary Mathematical Sciences, Vol. 7, No. 42, 2012, pp. 2053-2059.",
				"A. Brousseau, \u003ca href=\"http://www.fq.math.ca/Scanned/6-1/brousseau3.pdf\"\u003eA sequence of power formulas\u003c/a\u003e, Fib. Quart., 6 (1968), 81-83.",
				"Hilary I. Okagbue, Muminu O. Adamu, Sheila A. Bishop, Abiodun A. Opanuga, \u003ca href=\"https://www.ripublication.com/ijaer16/ijaerv11n6_150.pdf\"\u003eDigit and Iterative Digit Sum of Fibonacci numbers, their identities and powers\u003c/a\u003e, International Journal of Applied Engineering Research ISSN 0973-4562 Volume 11, Number 6 (2016) pp 4623-4627.",
				"J. Riordan, \u003ca href=\"http://dx.doi.org/10.1215/S0012-7094-62-02902-2\"\u003eGenerating functions for powers of Fibonacci numbers\u003c/a\u003e, Duke. Math. J. 29 (1962) 5-12.",
				"\u003ca href=\"/index/Di#divseq\"\u003eIndex to divisibility sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (8,40,-60,-40,8,1)."
			],
			"formula": [
				"a(n) = F(n)^5, F(n)=A000045(n).",
				"G.f.: x*p(5, x)/q(5, x) with p(5, x) := sum(A056588(4, m)*x^m, m=0..4)= 1-7*x-16*x^2+7*x^3+x^4 and q(5, x) := sum(A055870(6, m)*x^m, m=0..6)= 1-8*x-40*x^2+60*x^3+40*x^4-8*x^5-x^6 = (1-x-x^2)*(1+4*x-x^2)*(1-11*x-x^2) (factorization deduced from Riordan result).",
				"Recursion (cf. Knuth's exercise): sum(A055870(6, m)*a(n-m), m=0..6) = 0, n \u003e= 6; inputs: a(n), n=0..5. a(n) = +8*a(n-1) +40*a(n-2) -60*a(n-3) -40*a(n-4) +8*a(n-5) +a(n-6).",
				"a(n) = (10*F(n) + 5*(-1)^(n+1)*F(3*n) + F(5*n))/25, n \u003e= 0. See the general comment on A111418 regarding the Ozeki reference; here the row 10, 5, 1 of that triangle applies. - _Wolfdieter Lang_, Aug 25 2012",
				"a(n) = (F(n)^2*(F(3*n)-(-1)^n*3*F(n)))/5. - _Gary Detlefs_, Jan 07 2013",
				"a(n) = F(n-2)*F(n-1)*F(n)*F(n+1)*F(n+2) + F(n). - _Tony Foster III_, Apr 11 2018"
			],
			"mathematica": [
				"Table[f=Fibonacci[n];f^5,{n,0,12}] (* _Vladimir Joseph Stephan Orlovsky_, Jul 22 2008 *)"
			],
			"program": [
				"(MAGMA) [Fibonacci(n)^5: n in [0..30]]; // _Vincenzo Librandi_, Jun 04 2011",
				"(PARI) a(n)=fibonacci(n)^5 \\\\ _Charles R Greathouse IV_, Sep 24 2015"
			],
			"xref": [
				"Cf. A000045, A007598, A056570-1, A056588, A055870.",
				"Fifth row of array A103323."
			],
			"keyword": "nonn,easy",
			"offset": "0,4",
			"author": "_Wolfdieter Lang_, Jul 10 2000",
			"references": 15,
			"revision": 63,
			"time": "2018-04-14T08:23:22-04:00",
			"created": "2000-07-22T03:00:00-04:00"
		}
	]
}