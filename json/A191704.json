{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A191704",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 191704,
			"data": "1,2,3,7,12,4,32,57,17,5,157,282,82,22,6,782,1407,407,107,27,8,3907,7032,2032,532,132,37,9,19532,35157,10157,2657,657,182,42,10,97657,175782,50782,13282,3282,907,207,47,11,488282,878907,253907,66407,16407,4532",
			"name": "Dispersion of A016873, (5k+2), by antidiagonals.",
			"comment": [
				"For a background discussion of dispersions and their fractal sequences, see A191426. For dispersions of congruence sequences mod 3 or mod 4, see A191655, A191663, A191667.",
				"...",
				"Each of the sequences (5n, n\u003e1), (5n+1, n\u003e1), (5n+2, n\u003e=0), (5n+3, n\u003e=0), (5n+4, n\u003e=0), generates a dispersion. Each complement (beginning with its first term \u003e1) also generates a dispersion. The ten sequences and dispersions are listed here:",
				"...",
				"A191702=dispersion of A008587 (5k, k\u003e=1)",
				"A191703=dispersion of A016861 (5k+1, k\u003e=1)",
				"A191704=dispersion of A016873 (5k+2, k\u003e=0)",
				"A191705=dispersion of A016885 (5k+3, k\u003e=0)",
				"A191706=dispersion of A016897 (5k+4, k\u003e=0)",
				"A191707=dispersion of A047201 (1, 2, 3, 4 mod 5 and \u003e1)",
				"A191708=dispersion of A047202 (0, 2, 3, 4 mod 5 and \u003e1)",
				"A191709=dispersion of A047207 (0, 1, 3, 4 mod 5 and \u003e1)",
				"A191710=dispersion of A032763 (0, 1, 2, 4 mod 5 and \u003e1)",
				"A191711=dispersion of A001068 (0, 1, 2, 3 mod 5 and \u003e1)",
				"...",
				"EXCEPT for at most 2 initial terms (so that column 1 always starts with 1):",
				"A191702 has 1st col A047201, all else A008587",
				"A191703 has 1st col A047202, all else A016861",
				"A191704 has 1st col A047207, all else A016873",
				"A191705 has 1st col A032763, all else A016885",
				"A191706 has 1st col A001068, all else A016897",
				"A191707 has 1st col A008587, all else A047201",
				"A191708 has 1st col A042968, all else A047203",
				"A191709 has 1st col A042968, all else A047207",
				"A191710 has 1st col A042968, all else A032763",
				"A191711 has 1st col A042968, all else A001068",
				"...",
				"Regarding the dispersions A191670-A191673, there is a formula for sequences of the type \"(a or b or c or d mod m)\", (as in the relevant Mathematica programs):",
				"...",
				"If f(n)=(n mod 3), then (a,b,c,d,a,b,c,d,a,b,c,d,...) is given by a*f(n+3)+b*f(n+2)+c*f(n+1)+d*f(n); so that for n\u003e=1, \"(a, b, c, d mod m)\" is given by",
				"a*f(n+3)+b*f(n+2)+c*f(n+1)+d*f(n)+m*floor((n-1)/4))."
			],
			"link": [
				"Ivan Neretin, \u003ca href=\"/A191704/b191704.txt\"\u003eTable of n, a(n) for n = 1..5050\u003c/a\u003e (first 100 antidiagonals, flattened)"
			],
			"example": [
				"Northwest corner:",
				"1...2....7.....32....157",
				"3...12...57....282...1407",
				"4...17...82....407...2032",
				"5...22...107...532...2657",
				"6...27...132...657...3282",
				"6...37...182...907...4532"
			],
			"mathematica": [
				"(* Program generates the dispersion array T of the increasing sequence f[n] *)",
				"r = 40; r1 = 12;  c = 40; c1 = 12;",
				"f[n_] := 5n-3",
				"Table[f[n], {n, 1, 30}] (* A016873 *)",
				"mex[list_] := NestWhile[#1 + 1 \u0026, 1, Union[list][[#1]] \u003c= #1 \u0026, 1, Length[Union[list]]]",
				"rows = {NestList[f, 1, c]};",
				"Do[rows = Append[rows, NestList[f, mex[Flatten[rows]], r]], {r}];",
				"t[i_, j_] := rows[[i, j]];",
				"TableForm[Table[t[i, j], {i, 1, 10}, {j, 1, 10}]] (* A191704 *)",
				"Flatten[Table[t[k, n - k + 1], {n, 1, c1}, {k, 1, n}]] (* A191704 *)"
			],
			"xref": [
				"Cf. A047207, A016873, A191709, A191426."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Jun 12 2011",
			"references": 10,
			"revision": 9,
			"time": "2017-10-18T18:18:50-04:00",
			"created": "2011-06-13T13:11:00-04:00"
		}
	]
}