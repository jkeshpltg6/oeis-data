{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A187053",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 187053,
			"data": "1,-3,9,-22,48,-99,194,-363,657,-1155,1977,-3312,5443,-8787,13968,-21894,33873,-51795,78345,-117312,174033,-255945,373353,-540486,776848,-1109040,1573209,-2218198,3109713,-4335840,6014123,-8300811,11402928",
			"name": "Expansion of (psi(x^2) / psi(x))^3 in powers of x where psi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"reference": [
				"A. Cayley, A memoir on the transformation of elliptic functions, Collected Mathematical Papers. Vols. 1-13, Cambridge Univ. Press, London, 1889-1897, Vol. 9, p. 128."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A187053/b187053.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e (terms 0..2500 from G. C. Greubel)",
				"A. Cayley, \u003ca href=\"/A001934/a001934.pdf\"\u003eA memoir on the transformation of elliptic functions\u003c/a\u003e, Philosophical Transactions of the Royal Society of London (1874): 397-456; Collected Mathematical Papers. Vols. 1-13, Cambridge Univ. Press, London, 1889-1897, included in Vol. 9. [Annotated scan of pages 126-129]",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-3/8) * (eta(q) * eta(q^4)^2 / eta(q^2)^3)^3 in powers of q.",
				"Euler transform of period 4 sequence [-3, 6, -3, 0, ...].",
				"G.f.: (Product_{k\u003e0} (1 + x^(2*k)) / (1 + x^(2*k-1)))^3.",
				"Convolution inverse of A029840. Convolution cube of A083365. a(n) = (-1)^n * A001937(n).",
				"a(n) ~ (-1)^n * 3^(1/4) * exp(sqrt(3*n/2)*Pi) / (16*2^(3/4)*n^(3/4)). - _Vaclav Kotesovec_, Nov 15 2017"
			],
			"example": [
				"G.f. = 1 - 3*x + 9*x^2 - 22*x^3 + 48*x^4 - 99*x^5 + 194*x^6 - 363*x^7 + ...",
				"G.f. = q^3 - 3*q^11 + 9*q^19 - 22*q^27 + 48*q^35 - 99*q^43 + 194*q^51 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (QPochhammer[ x^4] / QPochhammer[ -x])^3, {x, 0, n}]; (* _Michael Somos_, Sep 02 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( (eta(x + A) * eta(x^4 + A)^2 / eta(x^2 + A)^3)^3, n))};"
			],
			"xref": [
				"Cf. A001937, A029840, A083365."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Mar 06 2011",
			"references": 6,
			"revision": 35,
			"time": "2021-03-12T22:24:46-05:00",
			"created": "2011-03-02T13:59:09-05:00"
		}
	]
}