{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A253186",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 253186,
			"data": "0,0,1,2,3,4,6,7,9,11,13,15,18,20,23,26,29,32,36,39,43,47,51,55,60,64,69,74,79,84,90,95,101,107,113,119,126,132,139,146,153,160,168,175,183,191,199,207,216,224,233,242,251,260,270,279,289,299,309,319,330",
			"name": "Number of connected unlabeled loopless multigraphs with 3 vertices and n edges.",
			"comment": [
				"a(n) is also the number of ways to partition n into 2 or 3 parts.",
				"a(n) is also the dimension of linear space of three-dimensional 2n-homogeneous polynomial vector fields, which have an octahedral symmetry (for a given representation), which are solenoidal, and which are vector fields on spheres. - _Giedrius Alkauskas_, Sep 30 2017",
				"Apparently a(n) = A244239(n-6) for n \u003e 4. - _Georg Fischer_, Oct 09 2018",
				"a(n) is also the number of loopless connected n-regular multigraphs with 4 nodes. - _Natan Arie Consigli_, Aug 09 2019",
				"a(n) is also the number of inequivalent linear [n, k=2] binary codes without 0 columns (see A034253 for more details). - _Petros Hadjicostas_, Oct 02 2019"
			],
			"link": [
				"Danny Rorabaugh, \u003ca href=\"/A253186/b253186.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Giedrius Alkauskas, \u003ca href=\"https://arxiv.org/abs/1601.06570\"\u003eProjective and polynomial superflows. I\u003c/a\u003e, arxiv.org/1601.06570 [math.AG], 2017; see Section 5.3.",
				"Harald Fripertinger, \u003ca href=\"http://www.mathe2.uni-bayreuth.de/frib/codes/tables.html\"\u003eIsometry Classes of Codes\u003c/a\u003e.",
				"Harald Fripertinger, \u003ca href=\"http://www.mathe2.uni-bayreuth.de/frib/codes/tables_4.html\"\u003eSnk2: Number of the isometry classes of all binary (n,k)-codes without zero-columns\u003c/a\u003e. [See column k = 2.]",
				"H. Fripertinger and A. Kerber, \u003ca href=\"https://doi.org/10.1007/3-540-60114-7_15\"\u003eIsometry classes of indecomposable linear codes\u003c/a\u003e. In: G. Cohen, M. Giusti, T. Mora (eds), Applied Algebra, Algebraic Algorithms and Error-Correcting Codes, 11th International Symposium, AAECC 1995, Lect. Notes Comp. Sci. 948 (1995), pp. 194-204. [Here a(n) = S_{n,2,2}.]",
				"R. J. Mathar, \u003ca href=\"http://arxiv.org/abs/1709.09000\"\u003eStatistics on Small Graphs\u003c/a\u003e, arXiv:1709.09000  [math.CO], 2017; see Eq. (23).",
				"Gordon Royle, \u003ca href=\"http://staffhome.ecm.uwa.edu.au/~00013890/remote/graphs/multigraphs/index.html\"\u003eSmall Multigraphs\u003c/a\u003e.",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,1,0,-1,-1,1)."
			],
			"formula": [
				"a(n) = A004526(n) + A069905(n).",
				"a(n) = floor(n/2) + floor((n^2 + 6)/12).",
				"G.f.: x^2*(x^3 - x - 1)/((x - 1)^2*(x^2 - 1)*(x^2 + x + 1))."
			],
			"example": [
				"On vertex set {a, b, c}, every connected multigraph with n = 5 edges is isomorphic to a multigraph with one of the following a(5) = 4 edge multisets: {ab, ab, ab, ab, ac}, {ab, ab, ab, ac, ac}, {ab, ab, ab, ac, bc}, and {ab, ab, ac, ac, bc}."
			],
			"mathematica": [
				"CoefficientList[Series[- x^2 (x^3 - x - 1) / ((1 - x) (1 - x^2) (1 - x^3)), {x, 0, 70}], x] (* _Vincenzo Librandi_, Mar 24 2015 *)",
				"LinearRecurrence[{1, 1, 0, -1, -1, 1}, {0, 0, 1, 2, 3, 4}, 61] (* _Robert G. Wilson v_, Oct 11 2017 *)",
				"a[n_]:=Floor[n/2] + Floor[(n^2 + 6)/12]; Array[a, 70, 0] (* _Stefano Spezia_, Oct 09 2018 *)"
			],
			"program": [
				"(Sage) [floor(n/2) + floor((n^2 + 6)/12) for n in range(70)]",
				"(MAGMA) [Floor(n/2) + Floor((n^2 + 6)/12): n in [0..70]]; // _Vincenzo Librandi_, Mar 24 2015"
			],
			"xref": [
				"Cf. A001399, A003082, A014395, A014396, A014397, A014398, A050535, A076864.",
				"Column k = 3 of A191646 and column k = 2 of A034253.",
				"First differences of A034198 (excepting the first term)."
			],
			"keyword": "nonn,easy",
			"offset": "0,4",
			"author": "_Danny Rorabaugh_, Mar 23 2015",
			"references": 18,
			"revision": 76,
			"time": "2021-07-20T13:25:00-04:00",
			"created": "2015-03-26T04:33:11-04:00"
		}
	]
}