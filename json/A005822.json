{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005822",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5822,
			"id": "M1243",
			"data": "0,1,1,2,4,11,16,49,72,214,319,947,1408,4187,6223,18502,27504,81769,121552,361379,537196,1597106,2374129,7058377,10492416,31194361,46371025,137862866,204935836,609282227,905709904,2692710841,4002767136,11900382694,17690150767",
			"name": "G.f.: x*(1-x^2)*(x^4+x^3-x^2+x+1) / (x^8-4*x^6-x^4-4*x^2+1).",
			"comment": [
				"This is a rescaled version of the number of spanning trees in the cube of an n-cycle. See A331905 for details. - _N. J. A. Sloane_, Feb 06 2020"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A005822/b005822.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e [Jul 09 2015; a(0) inserted by _Georg Fischer_, Jan 27 2020]",
				"G. Baron et al., \u003ca href=\"http://www.fq.math.ca/Scanned/23-3/baron.pdf\"\u003eThe number of spanning trees in the square of a cycle\u003c/a\u003e, Fib. Quart., 23 (1985), 258-264.",
				"Tsuyoshi Miezaki, \u003ca href=\"/A331905/a331905.pdf\"\u003eA note on spanning trees.\u003c/a\u003e",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"\u003ca href=\"/index/Tra#trees\"\u003eIndex entries for sequences related to trees\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_08\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,4,0,1,0,4,0,-1)."
			],
			"maple": [
				"A005822:=-z*(z-1)*(1+z)*(z**4+z**3-z**2+z+1)/(-4*z**6-z**4-4*z**2+1+z**8); # [Conjectured (correctly) by _Simon Plouffe_ in his 1992 dissertation; adapted to offset 0 by _Georg Fischer_, Jan 27 2020]"
			],
			"mathematica": [
				"CoefficientList[Series[x (1 - x^2) (x^4 + x^3 - x^2 + x + 1) / (x^8 - 4 x^6 - x^4 - 4 x^2 + 1), {x, 0, 35}], x] (* _Vincenzo Librandi_, Jan 28 2020 *)"
			],
			"program": [
				"(PARI) Vec(-x*(x-1)*(x+1)*(x^4+x^3-x^2+x+1)/(x^8-4*x^6-x^4-4*x^2+1) + O(x^50)) \\\\ _Colin Barker_, Jul 09 2015",
				"(MAGMA) m:=40; R\u003cx\u003e:=PowerSeriesRing(Rationals(), m); [0] cat Coefficients(R!( x*(1-x^2)*(x^4+x^3-x^2+x+1) / (x^8-4*x^6-x^4-4*x^2+1))); // _Vincenzo Librandi_, Jan 28 2020"
			],
			"xref": [
				"Cf. A169630, A331905."
			],
			"keyword": "nonn,easy",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"G.f. adapted to the offset from _Colin Barker_, Jul 09 2015",
				"Entry revised by _N. J. A. Sloane_, Jan 25 2020 and Feb 06 2020."
			],
			"references": 2,
			"revision": 43,
			"time": "2021-03-12T22:32:36-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}