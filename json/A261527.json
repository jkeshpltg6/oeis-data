{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A261527",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 261527,
			"data": "1,1,1,1,1,1,1,2,1,1,1,1,1,1,2,4,1,1,1,2,1,1,1,1,2,20,1,1,1,4,1,1,1,2,2,1,2,24,2",
			"name": "Irregular triangular array giving minimum number of reciprocal steps in the boomerang fractions process needed to return to 1 if a returning path exists, otherwise 0.",
			"comment": [
				"The boomerang fractions process is defined as follows. Fix a rational number q, 0\u003cq\u003c1. Starting with 1, on the first step add q and on subsequent steps either add q or take the reciprocal.",
				"Let q(n) be the n-th rational number in the interval (0,1) in the canonical ordering, that is, q(n)=A038566(n+1)/A038567(n+1). Then a(n) is obtained by applying the sequence definition with q=q(n).",
				"The value of a(n) is 1 if and only if q(n) is the difference of two unit fractions.",
				"If q(m) = k q(n) for some positive integer k, then a(n) \u003c= a(m).",
				"The first rational number in the canonical ordering for which it is not known whether a(n) is nonzero is q(40)=9/11. If a(40) is nonzero, then a(40) \u003e= 55."
			],
			"link": [
				"W. P. Orrick, \u003ca href=\"http://pages.iu.edu/~worrick/boomerang/\"\u003eBoomerang fractions: some calculations\u003c/a\u003e"
			],
			"example": [
				"a(1) = 1 since q(1) = 1/2 and there is the returning path 1 --\u003e 1+2*(1/2) = 2 --\u003e 1/2 --\u003e 1/2+1/2 = 1, which uses the reciprocal operation once.",
				"a(8) = 2 since q(8) = 3/5, which cannot be written as the difference of two unit fractions (ruling out a(8) = 1) and because there is the returning path 1 --\u003e 1+15*(3/5) = 10 --\u003e 1/10 --\u003e 1/10+4*(3/5) = 5/2 --\u003e 2/5 --\u003e 2/5+3/5 = 1, which uses the reciprocal operation twice.",
				"Triangle starts:",
				"1;",
				"1, 1;",
				"1, 1;",
				"1, 1, 2, 1;",
				"1, 1;",
				"1, 1, 1, 2, 4, 1;",
				"1, 1, 2, 1;",
				"1, 1, 1, 2, 20, 1;",
				"1, 1, 4, 1;",
				"1, 1, 2, 2, 1, 2, 24, 2,",
				"..."
			],
			"mathematica": [
				"(* In the following code, Alpha is the operation \"add q\" and Beta is the operation \"take the reciprocal and add q\". The set L(j) is defined to be the set of positive rational numbers r such that there is a path from r to 1 that uses Beta exactly j times. The program computes L(1), L(2), and so on, until an L(j) is found that contains 1, in which case it returns j, or until maxIterations is exceeded, in which case it returns 0. The function iterateUntilOne can generate the result for all q up to 6/11 rather quickly, but for q = 7/11, which corresponds to a(38) = 24, it requires considerable time; it is not capable of ruling out the existence of a returning path that uses Beta more than maxIterations times. *)",
				"applyBetaInverse[q_, x_] := 1/(x - q)",
				"applyAlphaPowerInverse[q_, x_] :=",
				"Table[x - q j, {j, 0, Ceiling[x/q] - 1}]",
				"iterateUntilOne[q_, maxIterations_] :=",
				"Module[{list, listOld, oneFound, it, betaInverseResult},",
				"  listOld = Flatten[applyAlphaPowerInverse[q, #] \u0026 /@ {1}];",
				"  oneFound = False;",
				"  For[it = 1, ! oneFound \u0026\u0026 it \u003c= maxIterations, it++,",
				"   betaInverseResult =",
				"    applyBetaInverse[q, #] \u0026 /@ Select[listOld, # \u003e q \u0026];",
				"   list = Flatten[applyAlphaPowerInverse[q, #] \u0026 /@ betaInverseResult];",
				"   oneFound = MemberQ[list, 1];",
				"   Print[\"L(\", it, \") : length \", Length[list],",
				"    If[oneFound, \", contains 1\", \", does not contain 1\"]];",
				"   listOld = list",
				"   ];",
				"  If[oneFound,",
				"   it - 1,",
				"   0",
				"   ]",
				"  ]",
				"iterateUntilOne[#, 20] \u0026 /@Flatten[Join[",
				"  Table[Select[Range[1, d], CoprimeQ[d, #] \u0026]/d, {d, 2, 10}],",
				"  Range[1, 6]/11]]"
			],
			"xref": [
				"Cf. A256174."
			],
			"keyword": "nonn,tabf,more",
			"offset": "1,8",
			"author": "_William P. Orrick_, Aug 21 2015",
			"references": 0,
			"revision": 17,
			"time": "2015-11-16T06:30:20-05:00",
			"created": "2015-10-24T12:09:36-04:00"
		}
	]
}