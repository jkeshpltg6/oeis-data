{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A256691",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 256691,
			"data": "1,4,4,32,4,16,4,128,32,16,4,128,4,16,16,2048,4,128,4,128,16,16,4,512,32,16,128,128,4,64,4,8192,16,16,16,1024,4,16,16,512,4,64,4,128,128,16,4,8192,32,128,16,128,4,512,16,512,16,16,4,512,4,16,128,65536,16,64,4,128,16,64,4,4096,4,16,128,128,16,64,4,8192,2048,16,4,512,16,16,16,512,4,512,16,128,16,16,16,32768,4,128,128,1024",
			"name": "From fourth root of Riemann zeta function: form Dirichlet series Sum b(n)/n^x whose fourth power is zeta function; sequence gives denominator of b(n).",
			"comment": [
				"Dirichlet g.f. of A256690(n)/A256691(n) is (zeta (x))^(1/4).",
				"Formula holds for general Dirichlet g.f. zeta(x)^(1/k) with k = 1, 2, ..."
			],
			"link": [
				"Wolfgang Hintze, \u003ca href=\"/A256691/b256691.txt\"\u003eTable of n, a(n) for n = 1..500\u003c/a\u003e"
			],
			"formula": [
				"with k = 4;",
				"zeta(x)^(1/k) = Sum_{n\u003e=1} b(n)/n^x;",
				"c(1,n)=b(n); c(k,n) = Sum_{d|n} c(1,d)*c(k-1,n/d), k\u003e1;",
				"Then solve c(k,n) = 1 for b(m);",
				"a(n) = denominator(b(n))."
			],
			"example": [
				"b(1), b(2), ... = 1, 1/4, 1/4, 5/32, 1/4, 1/16, 1/4, 15/128, 5/32, 1/16, 1/4, 5/128, 1/4, 1/16, 1/16, 195/2048, ..."
			],
			"mathematica": [
				"k = 4;",
				"c[1, n_] = b[n];",
				"c[k_, n_] := DivisorSum[n, c[1,#1]*c[k - 1, n/#1] \u0026 ]",
				"nn = 100; eqs = Table[c[k, n] == 1, {n, 1, nn}];",
				"sol = Solve[Join[{b[1] == 1}, eqs], Table[b[i], {i, 1, nn}], Reals];",
				"t = Table[b[n], {n, 1, nn}] /. sol[[1]];",
				"num = Numerator[t] (* A256690 *)",
				"den = Denominator[t] (* A256691 *)"
			],
			"xref": [
				"Cf. A046643/A046644 (k=2), A256688/A256689 (k=3), A256690/A256691 (k=4), A256692/A256693 (k=5)."
			],
			"keyword": "nonn,frac,mult",
			"offset": "1,2",
			"author": "_Wolfgang Hintze_, Apr 08 2015",
			"references": 11,
			"revision": 24,
			"time": "2015-04-16T15:26:27-04:00",
			"created": "2015-04-16T15:26:27-04:00"
		}
	]
}