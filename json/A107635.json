{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A107635",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 107635,
			"data": "1,3,3,4,9,12,15,21,30,43,54,69,94,123,153,193,252,318,391,486,609,754,918,1119,1376,1680,2019,2432,2946,3540,4220,5034,6015,7157,8463,9999,11835,13956,16374,19206,22542,26376,30750,35829,41745,48526,56250",
			"name": "McKay-Thompson series of class 32a for the Monster group.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A107635/b107635.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"D. Ford, J. McKay and S. P. Norton, \u003ca href=\"http://dx.doi.org/10.1080/00927879408825127\"\u003eMore on replicable functions\u003c/a\u003e, Commun. Algebra 22, No. 13, 5175-5193 (1994).",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"\u003ca href=\"/index/Mat#McKay_Thompson\"\u003eIndex entries for McKay-Thompson series for Monster simple group\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(1/8) * (eta(q^2)^2 / (eta(q) * eta(q^4)))^3 in powers of q.",
				"Expansion of chi(x)^3 = phi(x) / psi(-x) in powers of x where phi(), psi(), chi() are Ramanujan theta functions.",
				"Given g.f. A(x), then B(q) = A(q^8) / q satisfies 0 = f(B(q), B(q^3)) where f(u, v) = (u^3 - v) * (v^3 - u) - 9*u*v.",
				"Euler transform of period 4 sequence [3, -3, 3, 0, ...].",
				"G.f.: Product_{k\u003e0} (1 + (-x)^k)^-3.",
				"a(n) = (-1)^n * A022598(n).",
				"a(n) ~ exp(Pi*sqrt(n/2)) / (2^(7/4) * n^(3/4)). - _Vaclav Kotesovec_, Aug 27 2015",
				"G.f.: exp(3*Sum_{k\u003e=1} x^k/(k*(1 - (-x)^k))). - _Ilya Gutkovskiy_, Jun 07 2018"
			],
			"example": [
				"G.f. = 1 + 3*x + 3*x^2 + 4*x^3 + 9*x^4 + 12*x^5 + 15*x^6 + 21*x^7 + ...",
				"T32a = 1/q + 3*q^7 + 3*q^15 + 4*q^23 + 9*q^31 + 12*q^39 + 15*q^47 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (QPochhammer[ x^2]^2 / (QPochhammer[ x] QPochhammer[ x^4]))^3, {x, 0, n}]; (* _Michael Somos_, Jun 29 2014 *)",
				"nmax = 50; CoefficientList[Series[Product[(1 + x^(2*k+1))^3, {k, 0, nmax}], {x, 0, nmax}], x] (* _Vaclav Kotesovec_, Aug 27 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( (eta(x^2 + A)^2 / (eta(x + A) * eta(x^4 + A)))^3, n))};"
			],
			"xref": [
				"Cf. A022598."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Michael Somos_, May 18 2005",
			"references": 5,
			"revision": 27,
			"time": "2021-03-12T22:24:43-05:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}