{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A111578",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 111578,
			"data": "1,1,1,1,6,1,1,31,15,1,1,156,166,28,1,1,781,1650,530,45,1,1,3906,15631,8540,1295,66,1,1,19531,144585,126651,30555,2681,91,1,1,97656,1320796,1791048,646086,86856,4956,120,1,1,488281,11984820,24604420,12774510",
			"name": "Triangle T(n, m) = T(n-1, m-1) + (4m-3)*T(n-1, m) read by rows 1\u003c=m\u003c=n.",
			"comment": [
				"From _Peter Bala_, Jan 27 2015: (Start)",
				"Working with an offset of 0, this is the exponential Riordan array [exp(z), (exp(4*z) - 1)/4].",
				"This is the triangle of connection constants between the polynomial basis sequences {x^n}n\u003e=0 and { n!*4^n * binomial((x - 1)/4,n) }n\u003e=0. An example is given below.",
				"Call this array M and let P denote Pascal's triangle A007318 then P^2 * M = A225469; P^(-1) * M is a shifted version of A075499.",
				"This triangle is the particular case a = 4, b = 0, c = 1 of the triangle of generalized Stirling numbers of the second kind S(a,b,c) defined in the Bala link. (End)"
			],
			"link": [
				"P. Bala, \u003ca href=\"/A143395/a143395.pdf\"\u003eA 3 parameter family of generalized Stirling numbers\u003c/a\u003e"
			],
			"formula": [
				"From _Peter Bala_, Jan 27 2015: (Start)",
				"The following formulas assume an offset of 0.",
				"T(n,k) = 1/(4^k*k!)*sum {j = 0..k} (-1)^(k-j)*binomial(k,j)*(4*j + 1)^n.",
				"T(n,k) = sum {i = 0..n-1} 4^(i-k+1)*binomial(n-1,i)*Stirling2(i,k-1).",
				"E.g.f.: exp(z)*exp(x/4*(exp(4*z) - 1)) = 1 + (1 + x)*z + (1 + 6*x + x^2)*z^2/2! + ....",
				"O.g.f. for n-th diagonal: exp(-x/4)*sum {k \u003e= 0} (4*k + 1)^(k + n - 1)*((x/4*exp(-x))^k)/k!.",
				"O.g.f. column k: 1/( (1 - x)*(1 - 5*x)...(1 - (4*k + 1)*x ). (End)"
			],
			"example": [
				"The triangle starts in row n=1 as:",
				"1;",
				"1,1;",
				"1,6,1;",
				"1,31,15,1;",
				"Connection constants: Row 4: [1, 31, 15, 1] so",
				"x^3 = 1 + 31*(x - 1) + 15*(x - 1)*(x - 5) + (x - 1)*(x - 5)*(x - 9). - _Peter Bala_, Jan 27 2015"
			],
			"mathematica": [
				"T[n_, k_] := 1/(4^(k-1)*(k-1)!) * Sum[ (-1)^(k-j-1) * (4*j+1)^(n-1) * Binomial[k-1, j], {j, 0, k-1}]; Table[T[n, k], {n, 1, 10}, {k, 1, n}] // Flatten (* _Jean-François Alcover_, Jan 28 2015, after _Peter Bala_ *)"
			],
			"program": [
				"(Python)",
				"def A096038(n,m):",
				"    if n \u003c 1 or m \u003c 1 or m \u003e n:",
				"        return 0",
				"    elif n \u003c=2:",
				"        return 1",
				"    else:",
				"        return A096038(n-1,m-1)+(4*m-3)*A096038(n-1,m)",
				"print( [A096038(n,m) for n in range(20) for m in range(1,n+1)] )",
				"# _R. J. Mathar_, Oct 11 2009"
			],
			"xref": [
				"Cf. A111577, A008277, A039755, A016234 (3rd column)."
			],
			"keyword": "nonn,tabl",
			"offset": "1,5",
			"author": "_Gary W. Adamson_, Aug 07 2005",
			"ext": [
				"Edited and extended by _R. J. Mathar_, Oct 11 2009"
			],
			"references": 7,
			"revision": 16,
			"time": "2021-05-16T21:59:53-04:00",
			"created": "2005-09-21T03:00:00-04:00"
		}
	]
}