{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002789",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2789,
			"id": "M1039 N0390",
			"data": "2,4,7,11,16,21,28,35,43,52,62,72,84,96,109,123,138,153,170,187,205,224,244,264,286,308,331,355,380,405,432,459,487,516,546,576,608,640,673,707,742,777,814,851,889,928,968,1008,1050,1092,1135,1179,1224,1269",
			"name": "Number of integer points in a certain quadrilateral scaled by a factor of n.",
			"comment": [
				"The quadrilateral is given by four vertices [(1/2, 1/3), (0, 1), (0, 0), (1, 0)] as an example on page 22 of Ehrhart 1967. Here the closed line segment from (1/2, 1/3) to (0, 1) is not included but the rest of the boundary is. The sequence is denoted by d(n). - _Michael Somos_, May 22 2014"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"E. Ehrhart, \u003ca href=\"http://dx.doi.org/10.1515/crll.1967.226.1\"\u003eSur un problème de géométrie diophantienne linéaire I\u003c/a\u003e, (Polyèdres et réseaux), J. Reine Angew. Math. 226 1967 1-29. MR0213320 (35 #4184).",
				"E. Ehrhart, \u003ca href=\"/A002789/a002789.pdf\"\u003eSur un problème de géométrie diophantienne linéaire I, (Polyèdres et réseaux)\u003c/a\u003e, J. Reine Angew. Math. 226 1967 1-29. MR0213320 (35 #4184). [Annotated scanned copy of pages 16 and 22 only]",
				"E. Ehrhart, \u003ca href=\"/A002789/a002789_1.pdf\"\u003eSur un problème de géométrie diophantienne linéaire II. Systemes diophantiens lineaires\u003c/a\u003e, J. Reine Angew. Math. 227 1967 25-49. [Annotated scanned copy of pages 47-49 only]",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Ehrhart_polynomial\"\u003eEhrhart polynomial\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,1,0,-1,-1,1)."
			],
			"formula": [
				"G.f.: x * (2 + 2*x + x^2) / (1 - x - x^2 + x^4 + x^5 - x^6) = (2*x + x^3 + x^4 + x^5) / ((1 - x)^2 * (1 - x^6)). - _Michael Somos_, May 22 2014",
				"a(n) = floor( A168668(n+1) / 12), a(n) = A242771(-n), a(n) - a(n-1) = A242774(n) for all n in Z. - _Michael Somos_, May 22 2014"
			],
			"example": [
				"G.f. = 2*x + 4*x^2 + 7*x^3 + 11*x^4 + 16*x^5 + 21*x^6 + 28*x^7 + 35*x^8 + ..."
			],
			"mathematica": [
				"a[ n_] := Quotient[ 7 + 12 n + 5 n^2, 12]; (* _Michael Somos_, May 22 2014 *)",
				"a[ n_] := Length @ With[{o = Boole[ 0 \u003c n], c = Boole[ 0 \u003e= n], m = Abs@n}, FindInstance[ 0 \u003c o + x \u0026\u0026 0 \u003c o + y \u0026\u0026 (2 x \u003c o + m \u0026\u0026 4 x + 3 y \u003c c + 3 m || m \u003c c + 2 x \u0026\u0026 2 x + 3 y \u003c o + 2 m), {x, y}, Integers, 10^9]]; (* _Michael Somos_, May 22 2014 *)"
			],
			"program": [
				"(PARI) {a(n) = (7 + 12*n + 5*n^2) \\ 12}; /* _Michael Somos_, May 22 2014 */",
				"(PARI) {a(n) = if( n\u003c0, polcoeff( x^3 * (1 + x + x^2 + 2*x^4) / ((1 - x)^2 * (1 - x^6)) + x * O(x^-n), -n), polcoeff( x * (2 + x^2 + x^3 + x^4) / ((1 - x)^2 * (1 - x^6)) + x * O(x^n), n))}; /* _Michael Somos_, May 22 2014 */"
			],
			"xref": [
				"Cf. A168668, A242771, A242774."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_",
			"references": 5,
			"revision": 29,
			"time": "2015-10-23T19:38:40-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}