{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A098602",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 98602,
			"data": "0,12,420,14280,485112,16479540,559819260,19017375312,646030941360,21946034630940,745519146510612,25325704946729880,860328449042305320,29225841562491651012,992818284675673829100,33726595837410418538400,1145711440187278556476512",
			"name": "a(n) = A001652(n) * A046090(n).",
			"comment": [
				"From _Ron Knott_, Nov 25 2013: (Start)",
				"a(n) = 2*r*(r+1) which is also of form s(s+1) where the s is in A053141.",
				"a(n) is an oblong number (A002378) which is twice another oblong number. (End)",
				"2*a(n)+1 and 4*a(n)+1 are both square. - _Paul Cleary_, Jun 23 2014"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A098602/b098602.txt\"\u003eTable of n, a(n) for n = 0..255\u003c/a\u003e",
				"Nikola Adžaga, Andrej Dujella, Dijana Kreso, Petra Tadić, \u003ca href=\"https://www.researchgate.net/publication/323934704_On_Diophantine_m-tuples_and_Dn-sets\"\u003eOn Diophantine m-tuples and D(n)-sets\u003c/a\u003e, 2018.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (35,-35,1)."
			],
			"formula": [
				"a(n) = 2*A029549(n) = 2*A001109(n)*A001109(n+1).",
				"a(n) = (A001653(n)^2 - 1)/2.",
				"a(n) = A053141(n)^2 + A011900(n)^2 - 1.",
				"For n\u003e0, a(n) = A053141(2n) - 2*A001109(n-1)^2.",
				"For n\u003e0, a(n) = 3*(A001542(n)^2 - A001542(n-1)^2).",
				"For n\u003e0, a(n) = A053141(2n-1) + 2*(A001653(2n-1) - A001109(n-1)^2).",
				"a(n+1) + a(n) = 3*A001542(n+1)^2.",
				"a(n+1) - a(n) = A001542(2*n).",
				"a(n+1)*a(n) = 4*(A001109(n)^4 - A001109(n)^2) = 4*A001110(n)*(A001110(n) - 1).",
				"From _Ron Knott_, Nov 25 2013: (Start)",
				"a(n) = 35*a(n-1) - 35*a(n-2) + a(n-3).",
				"G.f.: 12*x / ((1-x)*(x^2-34*x+1)). (End)",
				"a(n) = (-6 + (3-2*sqrt(2))*(17+12*sqrt(2))^(-n)+(3+2*sqrt(2))*(17+12*sqrt(2))^n)/16. -  _Colin Barker_, Mar 02 2016"
			],
			"example": [
				"a(1) = 12 = 2(2*3) = 3*4, a(2) = 420 = 2(14*15) = 20*21."
			],
			"mathematica": [
				"2*Table[ Floor[(Sqrt[2] + 1)^(4n + 2)/32], {n, 0, 20} ] (* _Ray Chandler_, Nov 10 2004, copied incorrect program from A029549, revised Jul 09 2015 *)",
				"RecurrenceTable[{a[n+3] == 35 a[n+2] - 35 a[n+1] + a[n], a[1] == 0, a[2] == 12, a[3] == 420}, a, {n, 1, 10}] (* _Ron Knott_, Nov 25 2013 *)",
				"LinearRecurrence[{35, -35, 1}, {0, 12, 420}, 25] (* _T. D. Noe_, Nov 25 2013 *)",
				"Table[(LucasL[4*n+2, 2] - 6)/16, {n,0,30}] (* _G. C. Greubel_, Jul 15 2018 *)"
			],
			"program": [
				"(PARI) concat(0, Vec(12*x/((1-x)*(1-34*x+x^2)) + O(x^20))) \\\\ _Colin Barker_, Mar 02 2016",
				"(PARI) {a=1+sqrt(2); b=1-sqrt(2); Q(n) = a^n + b^n};",
				"for(n=0, 30, print1(round((Q(4*n+2) - 6)/16), \", \")) \\\\ _G. C. Greubel_, Jul 15 2018",
				"(MAGMA) m:=30; R\u003cx\u003e:=PowerSeriesRing(Integers(), m); [0] cat Coefficients(R!(12*x/((1-x)*(x^2-34*x+1)))); // _G. C. Greubel_, Jul 15 2018"
			],
			"xref": [
				"Cf. A002378, A053141."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Charlie Marion_, Oct 26 2004",
			"ext": [
				"More terms from _Ray Chandler_, Nov 10 2004",
				"Corrected by Bill Lam (bill_lam(AT)myrealbox.com), Feb 27 2006"
			],
			"references": 7,
			"revision": 55,
			"time": "2020-08-03T00:45:42-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}