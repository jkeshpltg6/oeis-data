{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A121757",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 121757,
			"data": "1,1,2,1,4,6,1,6,18,24,1,8,36,96,120,1,10,60,240,600,720,1,12,90,480,1800,4320,5040,1,14,126,840,4200,15120,35280,40320,1,16,168,1344,8400,40320,141120,322560,362880,1,18,216,2016,15120,90720,423360,1451520",
			"name": "Triangle read by rows: multiply Pascal's triangle by 1,2,6,24,120,720,... = A000142.",
			"comment": [
				"Row sums are 1,3,11,49,261,1631,... = A001339",
				"a(n,k) = D(n+1,k+1) Array D in A253938 is part of a conjectured formula for F(n,p,r) that relates Dyck path peaks and returns.  a(n,k) was discovered prior to array D. - _Roger Ford_, May 19 2016"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A121757/b121757.txt\"\u003eRows n = 0..100, flattened\u003c/a\u003e",
				"J. Goldman, J. Haglund, \u003ca href=\"http://dx.doi.org/10.1006/jcta.2000.3113\"\u003eGeneralized rook polynomials\u003c/a\u003e, J. Combin. Theory A91 (2000), 509-530, 2-rook coefficients of k rooks on the 2xn board (all heights 2).",
				"\u003ca href=\"/index/Pas#Pascal\"\u003eIndex entries for triangles and arrays related to Pascal's triangle\u003c/a\u003e"
			],
			"formula": [
				"a(n,k) = A007318(n,k)*A000142(k+1), k=0,1,..,n, n=0,1,2,3... - _R. J. Mathar_, Sep 02 2006",
				"a(n,k) = A008279(n,k) * (k+1). a(n,k) = n!*(k+1)/(n-k)!. - _Franklin T. Adams-Watters_, Sep 20 2006"
			],
			"example": [
				"Row 6 is 1*1 5*2 10*6 10*24 5*120 1*720.",
				"From _Vincenzo Librandi_, Dec 16 2012: (Start)",
				"Triangle begins:",
				"1,",
				"1, 2,",
				"1, 4,  6,",
				"1, 6,  18,  24,",
				"1, 8,  36,  96,   120,",
				"1, 10, 60,  240,  600,  720,",
				"1, 12, 90,  480,  1800, 4320,  5040,",
				"1, 14, 126, 840,  4200, 15120, 35280,  40320,",
				"1, 16, 168, 1344, 8400, 40320, 141120, 322560, 362880 etc.",
				"(End)"
			],
			"mathematica": [
				"Flatten[Table[n!(k+1)/(n-k)!,{n,0,10},{k,0,n}]]  (* _Harvey P. Dale_, Apr 25 2011 *)"
			],
			"program": [
				"(PARI) A000142(n)={ return(n!) ; } A007318(n,k)={ return(binomial(n,k)) ; } A121757(n,k)={ return(A007318(n,k)*A000142(k+1)) ; } { for(n=0,12, for(k=0,n, print1(A121757(n,k),\",\") ; ); ) ; } \\\\ _R. J. Mathar_, Sep 02 2006",
				"(Haskell)",
				"a121757 n k = a121757_tabl !! n !! k",
				"a121757_row n = a121757_tabl !! n",
				"a121757_tabl = iterate",
				"   (\\xs -\u003e zipWith (+) (xs ++ [0]) (zipWith (*) [1..] ([0] ++ xs))) [1]",
				"-- _Reinhard Zumkeller_, Mar 06 2014"
			],
			"xref": [
				"Cf. A007526 A000522, A005843 (2nd column), A028896 (3rd column).",
				"Cf. A008279.",
				"Cf. A008277, A132159 (mirrored)."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,3",
			"author": "_Alford Arnold_, Aug 19 2006",
			"references": 3,
			"revision": 28,
			"time": "2017-05-04T05:54:23-04:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}