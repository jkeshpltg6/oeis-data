{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A214833",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 214833,
			"data": "1,1,2,6,16,52,160,536,1796,6216,21752,77504,278720,1013184,3712128,13701204,50880808,190003808,712975648,2687114976,10167088608,38605365712,147060726688,561853414896,2152382687488,8265949250848,31817041756880,122728993889056",
			"name": "Number of formula representations of n using addition, multiplication and the constant 1.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A214833/b214833.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Shalosh B. Ekhad, \u003ca href=\"http://www.math.rutgers.edu/~zeilberg/tokhniot/oArithFormulas1\"\u003eEverything About Formulas Representing Integers Using Additions and Multiplication for integers from 1 to 8000\u003c/a\u003e",
				"Edinah K. Gnang, Maksym Radziwill, and Carlo Sanna, \u003ca href=\"https://arxiv.org/abs/1406.1704\"\u003eCounting arithmetic formulas\u003c/a\u003e, arXiv:1406.1704 [math.CO], 2014.",
				"Edinah K. Gnang, Maksym Radziwill, and Carlo Sanna, \u003ca href=\"https://dx.doi.org/10.1016/j.ejc.2015.01.007\"\u003eCounting arithmetic formulas\u003c/a\u003e, European Journal of Combinatorics 47 (2015), pp. 40-53.",
				"Edinah K. Ghang and Doron Zeilberger, \u003ca href=\"https://arxiv.org/abs/1303.0885\"\u003eZeroless Arithmetic: Representing Integers ONLY using ONE\u003c/a\u003e, arXiv:1303.0885 [math.CO], 2013.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Postfix_notation\"\u003ePostfix notation\u003c/a\u003e",
				"\u003ca href=\"/index/Com#complexity\"\u003eIndex to sequences related to the complexity of n\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{i=1..n-1} a(i)*a(n-i) + Sum_{d|n, 1\u003cd\u003cn} a(d)*a(n/d) for n\u003e1, a(1)=1.",
				"a(n) ~ c * d^n / n^(3/2), where d = 4.076561785276... = A242970, c = 0.145691854699979... = A242955. - _Vaclav Kotesovec_, Sep 12 2014"
			],
			"example": [
				"a(1) = 1: 1.",
				"a(2) = 1: 11+.",
				"a(3) = 2: 111++, 11+1+.",
				"a(4) = 6: 1111+++, 111+1++, 11+11++, 111++1+, 11+1+1+, 11+11+*.",
				"a(5) = 16: 11111++++, 1111+1+++, 111+11+++, 1111++1++, 111+1+1++, 111+11+*+, 11+111+++, 11+11+1++, 111++11++, 11+1+11++, 1111+++1+, 111+1++1+, 11+11++1+, 111++1+1+, 11+1+1+1+, 11+11+*1+.",
				"All formulas are given in postfix (reverse Polish) notation but other notations would give the same results."
			],
			"maple": [
				"with(numtheory):",
				"a:= proc(n) option remember; `if`(n=1, 1,",
				"       add(a(i)*a(n-i), i=1..n-1)+",
				"       add(a(d)*a(n/d), d=divisors(n) minus {1, n}))",
				"    end:",
				"seq(a(n), n=1..40);"
			],
			"mathematica": [
				"a[n_] := a[n] = If[n == 1, 1, Sum[a[i]*a[n-i], {i, 1, n-1}] + Sum[a[d]*a[n/d], {d, Divisors[n][[2 ;; -2]]}]]; Table[a[n], {n, 1, 40}] (* _Jean-François Alcover_, Feb 05 2015, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(PARI) A214833_vec=[1]; alias(A,A214833_vec); A214833(n)={n\u003e#A\u0026\u0026A=concat(A,vector(n-#A));if(A[n],A[n],A[n]=sum(i=1,n-1,A214833(i)*A214833(n-i))+sumdiv(n,d,if(d\u003e1\u0026\u0026d\u003cn,A214833(d)*A214833(n/d))))} \\\\ _M. F. Hasler_, May 04 2017"
			],
			"xref": [
				"Cf. A213923 (minimal length of formula), A005408(n-1) (maximal length of formula), A214835 (total sum of lengths), A214836, A214843, A242970, A242955."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Alois P. Heinz_, Mar 07 2013",
			"references": 7,
			"revision": 70,
			"time": "2021-02-22T12:43:26-05:00",
			"created": "2013-03-12T19:43:57-04:00"
		}
	]
}