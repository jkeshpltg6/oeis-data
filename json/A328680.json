{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A328680",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 328680,
			"data": "2,2,2,2,2,2,2,2,2,3,2,3,3,3,3,3,3,3,3,3,3,2,3,3,3,3,3,3,3,3,3,3,2,3,3,3,3,3,3,3,3,3,3,2,3,3,3,3,3,3,3,3,3,3,2,3,3,3,3,3,3,3,3,3,3,2,3,3,3,3,3,3,3,3,3,3,2,3,3,3,3,3,3,3,3,3,3,2,3,3,3,3,3,3,3,3,3,3,2,4,4,4,4,4,4,4,4,4,5",
			"name": "The number of iterations before a repeated value appears when starting from n and performing the iterative cycle as described in the comments, which involves setting the next iterative number to either A053392 or A040115 depending on the current numbers' size relative to n.",
			"comment": [
				"This sequence is based on the following iterative cycle. Start with n, set m = A040115(n), the concatenation of the absolute values of differences between adjacent digits, and then repeat the following until the number m has been previously seen: if m is greater than n, let m = A040115(m), otherwise let m = A053392(m), the concatenation of the sums of pairs of adjacent digits.",
				"For all starting values n the iteration eventually converges to 0 or else goes into a cycle of finite length. When the number m gets larger than the iteration's starting value n it will always have its magnitude decreased by the operation m = A040115(m), while m = A053392(m) can either increase or decrease its magnitude, depending on the digit values of m. This has the overall effect of never allowing the iterative values to increase without limit as is seen in the similar iterations A328975 and A329624.",
				"All the values of A053393 are seen as repeating values in this sequence, although this sequence has significantly more; probably an infinite number, although this is unknown. The first nonzero repeating value is not seen until a(9090), which forms the two-member loop of 999 -\u003e 1818 -\u003e 999. The first starting value that leads to an m value greater than the initial starting value is a(10090), see examples below. A330159 lists the starting values which are also the first repeating value.",
				"For the first 20 million terms the longest iterative sequence is seen for a(18505180) which takes 457 steps before reaching 0. See attached link. The longest found looping sequence is for a(14106482) which reaches 1040103 after 5 steps and then again after 116 steps, forming a loop of length 111. The largest number found which starts the repeating loop is for a(9265011) which reaches 1411131715 after 9 iterations and then again after 41 iterations.",
				"From a(12) to a(99) the sequence repeats a pattern of ten 3's followed by a 2. After that, a(100) = 4 and the terms begin to show a slow average increase in value."
			],
			"link": [
				"Scott R. Shannon, \u003ca href=\"/A328680/a328680_1.dat.txt\"\u003eThe iterative sequence for a(18505180)\u003c/a\u003e."
			],
			"example": [
				"a(10) = 3 as A040115(10) = 1, A053392(1) = 0, and A053392(0) = 0, taking three steps to repeat from 10.",
				"a(1060) = 7 as A040115(1060) = 166, A053392(166) = 712, A053392(712) = 83, A053392(83) = 11, A053392(11) = 2, A053392(2) = 0, A053392(0) = 0, taking seven steps to repeat from 1060.",
				"a(10090) = 11 as A040115(10090) = 1099, A053392(1099) = 1918, A053392(1918) = 10109, A040115(10109) = 1119, A053392(1119) = 2210, A053392(2210) = 431, A053392(431) = 74, A053392(74) = 11, A053392(11) = 2, A053392(2) = 0, A053392(0) = 0, taking eleven steps to repeat from 11090."
			],
			"xref": [
				"Cf. A329624, A329623, A053392, A328975, A329200, A040115, A329197."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Scott R. Shannon_, Dec 03 2019",
			"references": 2,
			"revision": 38,
			"time": "2021-09-23T01:27:23-04:00",
			"created": "2019-12-07T00:39:23-05:00"
		}
	]
}