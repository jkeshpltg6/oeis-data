{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A194270",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 194270,
			"data": "0,1,5,13,29,51,75,97,137,177,209,241,297,371,467,517,605,677,709,757,829,933,1061,1173,1317,1461,1613,1709,1861,2039,2279,2401,2585,2721,2753,2801,2873,2981,3125,3269,3453,3641,3841,4017,4289,4563,4979,5229",
			"name": "D-toothpick sequence of the second kind (see Comments lines for definition).",
			"comment": [
				"This is a cellular automaton of forking paths to 135 degrees which uses elements of two sizes: toothpicks of length 1 and D-toothpicks of length 2^(1/2). Toothpicks are placed in horizontal or vertical direction. D-toothpicks are placed in diagonal direction. Toothpicks and D-toothpicks are connected by their endpoints.",
				"On the infinite square grid we start with no elements.",
				"At stage 1, we place anywhere a D-toothpick.",
				"The rule for adding new elements is as follows. Each exposed endpoint of the elements of the old generation must be touched by the two endpoints of two elements of the new generation such that the angle between the old element and each new element is equal to 135 degrees. Intersections and overlapping are prohibited, so some toothpick endpoints can remain exposed forever.",
				"The sequence gives the number of toothpicks and D-toothpicks in the structure after n-th stage. The first differences (A194271) give the number of toothpicks and D-toothpicks added at n-th stage.",
				"It appears that if n \u003e\u003e 1 the structure looks like an almost regular octagon. It appears that this has a like-fractal behavior related to powers of 2 (see formula section in A194271 and A194443). Note that for some values of n we can see an internal growth, similar to A160120. Also there are hidden sub-structures which have a surprising connection with the Sierpinski triangle. The hidden sub-structures are displayed more clearly for large values ​​of n without reducing the scale of the drawing. The main \"wedges\" in the structures are essentially the triangles A194440 and A194442.",
				"Note that this structure is much more complex than A139250 and A160120. The structure contains a large number of distinct polygonal shapes. There are convex polygons and concave polygons, also there are symmetrical and asymmetrical polygons. Several of these polygons are also in the structure of A172310. The number of edges of the known polygons are 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 16, 17, 20, 24. Is not known how many distinct types of polygons there are in the structure if n -\u003e infinite. The sequences related with these polygons are A194276, A194277, A194278 and A194283. Note that the structure is not centered with respect to the axes X, Y. Also, for some polygons the area is not an integer. For symmetric versions of C. A. see A194432 and A194434.",
				"Another representation (Large version): instead toothpicks of length 1 we place toothpicks of length 2. We start with no toothpicks. At stage 1, we place a toothpick of length 2 on the y-axis and centered at the origin. At stage 2 we place four D-toothpicks of length 2^(1/2) = sqrt(2), and so on. In this case the structure is centered with respect to the axes X, Y and the area of the polygons is an integer."
			],
			"link": [
				"David Applegate, \u003ca href=\"/A139250/a139250.anim.html\"\u003eThe movie version\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/wiki/Catalog_of_Toothpick_and_CA_Sequences_in_OEIS\"\u003eCatalog of Toothpick and Cellular Automata Sequences in the OEIS\u003c/a\u003e",
				"\u003ca href=\"/index/To#toothpick\"\u003eIndex entries for sequences related to toothpick sequences\u003c/a\u003e"
			],
			"example": [
				"Illustration of initial terms:",
				".                                            o   o",
				".                                             \\ /",
				".                        o                     o     o",
				".                        |                     |    /",
				".        o               o - o         o       o - o",
				".       /               /               \\     /     \\",
				".      o           o - o                 o - o       o",
				".                      |                /    |",
				".                      o               o     o",
				".                                           / \\",
				".                                          o   o",
				".",
				".       1               5                    13",
				"."
			],
			"xref": [
				"Cf. A139250, A160120, A172310, A182838, A194271, A194276, A194277, A194278, A194440, A194441, A194442, A194443, A194444."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Omar E. Pol_, Aug 23 2011",
			"ext": [
				"More terms from _Omar E. Pol_, Sep 01 2011"
			],
			"references": 37,
			"revision": 77,
			"time": "2019-11-23T04:05:45-05:00",
			"created": "2011-08-24T13:41:26-04:00"
		}
	]
}