{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A156269",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 156269,
			"data": "1,2,6,-20,-24,-56,144,160,352,-832,-896,-1920,4352,4608,9728,-21504,-22528,-47104,102400,106496,221184,-475136,-491520,-1015808,2162688,2228224,4587520,-9699328,-9961472,-20447232,42991616,44040192,90177536",
			"name": "Denominators of a series expansion for Pi/2.",
			"comment": [
				"Numerators are all 1.",
				"Sum_{n \u003e= 0} 1/a(n) = Pi/2.",
				"This sequence is based on Adamchik and Wagon's BBP-type three-term formula for Pi, namely Pi = Sum_{n \u003e= 0} (-1/4)^n*(2/(4*n + 1) + 2/(4*n + 2) + 1/(4*n + 3)).",
				"From _Peter Bala_, Jun 16 2016: (Start)",
				"The reciprocals 1/a(n) appear as coefficients in the Maclaurin series for 2*arctan(z/(2 - z)) = z + z^2/2 + z^3/6 - z^5/20 - z^6/24 - z^7/56 + ... (the radius of convergence is sqrt(2)).",
				"Setting z = 1 gives Pi/2 = Sum_{n \u003e= 0} 1/a(n) as observed above. Setting z = 2 - sqrt(2) gives a series for Pi/4 in terms of a(n). Setting z = +- sqrt(2), and using Abel's theorem on power series, gives two further series for Pi involving a(n). (End)"
			],
			"link": [
				"V. Adamchik and S. Wagon, \u003ca href=\"http://www.cs.cmu.edu/~adamchik/articles/pi/pi.htm\"\u003ePi: A 2000-Year Search Changes Direction\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0, 0, -8, 0, 0, -16)."
			],
			"formula": [
				"G.f.: (1+2*x+6*x^2-12*x^3-8*x^4-8*x^5)/(1+4*x^3)^2.",
				"From _Peter Bala_, Jun 16 2016: (Start)",
				"a(3*n) = (-4)^n*(4*n + 1);",
				"a(3*n + 1) = (-4)^n*(4*n + 2);",
				"a(3*n + 2) = (-4)^n*(8*n + 6). (End)"
			],
			"maple": [
				"A156269 := n -\u003e if `mod`(n, 3) = 0 then (-4)^(n/3)*(4*n/3 + 1) elif `mod`(n, 3) = 1 then (-4)^((n-1)/3)*(4*(n-1)/3 + 2) else (-4)^((n-2)/3)*(8*(n-2)/3 + 6) end if:",
				"seq(A156269(n), n = 1 .. 35); # _Peter Bala_, Jun 16 2016"
			],
			"mathematica": [
				"CoefficientList[Series[(1+2x+6x^2-12x^3-8x^4-8x^5)/(1+4x^3)^2,{x,0,40}],x] (* or *) LinearRecurrence[{0,0,-8,0,0,-16},{1,2,6,-20,-24,-56},40] (* _Harvey P. Dale_, Dec 16 2016 *)"
			],
			"xref": [
				"Cf. A000796, A154925, A154962."
			],
			"keyword": "sign,easy",
			"offset": "0,2",
			"author": "_Jaume Oliver Lafont_, Feb 07 2009, Feb 21 2009",
			"references": 4,
			"revision": 13,
			"time": "2016-12-16T16:52:54-05:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}