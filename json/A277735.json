{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A277735",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 277735,
			"data": "0,1,2,0,0,0,1,0,1,0,1,2,0,0,1,2,0,0,1,2,0,0,0,1,0,1,2,0,0,0,1,0,1,2,0,0,0,1,0,1,0,1,2,0,0,1,2,0,0,0,1,0,1,0,1,2,0,0,1,2,0,0,0,1,0,1,0,1,2,0,0,1,2,0,0,1,2,0,0,0,1,0,1,2,0,0,0,1,0,1,0,1,2,0,0,1,2,0,0,1",
			"name": "Unique fixed point of the morphism 0 -\u003e 01, 1 -\u003e 20, 2 -\u003e 0.",
			"comment": [
				"From _Clark Kimberling_, May 21 2017: (Start)",
				"Let u be the sequence of positions of 0, and likewise, v for 1 and w for 2.  Let U, V, W be the limits of u(n)/n, v(n)/n, w(n)/n, respectively.  Then 1/U + 1/V + 1/W = 1, where",
				"U = 1.8392867552141611325518525646532866...,",
				"V = U^2 = 3.3829757679062374941227085364...,",
				"W = U^3 = 6.2222625231203986266745611011....",
				"If n \u003e=2, then u(n) - u(n-1) is in {1,2,3}, v(n) - v(n-1) is in {2,4,5}, and w(n) - w(n-1) is in {4,7,9}. (u = A277736, v = A277737, w = A277738). (End)",
				"Although I believe the assertions in Kimberling's comment above to be correct, these results are quite tricky to prove, and unless a formal proof is supplied at present these assertions must be regarded as conjectures. - _N. J. A. Sloane_, Aug 20 2018",
				"From _Michel Dekking_, Oct 03 2019: (Start)",
				"Here is a proof of Clark Kimberling's conjectures (and more).",
				"The incidence matrix of the defining morphism",
				"    sigma:  0 -\u003e 01, 1 -\u003e 20, 2 -\u003e 0",
				"is  the same as the incidence matrix of the tribonacci morphism",
				"            0 -\u003e 01, 1 -\u003e 02, 2 -\u003e 0",
				"(see A080843  and/or A092782).",
				"This implies that the frequencies  f0, f1 and f2 of the letters 0,1, and 2 in (a(n)) are the same as the corresponding frequencies in the tribonacci word, which are 1/t, 1/t^2  and 1/t^3 (see, e.g., A092782).",
				"Since   U = 1/f0,  V = 1/f1, and W = 1/f2, we conclude that",
				"    U = t = A058265,  V = t^2 = A276800 and W = t^3 = A276801.",
				"The statements on the difference sequences u, v, and w of the positions of 0,1, and 2 are easily verified by applying sigma to the return words of  these three letters.",
				"Here the return words of an arbitrary word w in a sequence x are all the words occurring in x with prefix w that do not have other occurrences of w in them.",
				"The return words of 0 are 0, 01, and 012, which indeed have length 1, 2",
				"and 3. Since",
				"    sigma(0) = 01, sigma(1) = 0120, and sigma(012) = 01200,",
				"one sees that u is the unique fixed point of the morphism",
				"    1 -\u003e 2, 2-\u003e 31, 3 -\u003e311.",
				"With a little more work, passing to sigma^2, and rotating, one can show that v is the unique fixed point of the morphism",
				"    2-\u003e52, 4-\u003e5224, 5-\u003e52244 .",
				"Similarly, w is the unique fixed point of the morphism",
				"    4-\u003e94, 7-\u003e9447, 9-\u003e94477.",
				"Interestingly, the three morphisms having u,v, and w as fixed point are essentially the same morphism (were we replaced sigma by sigma^2) with standard form",
				"    1-\u003e12, 2-\u003e1223, 3-\u003e12233.",
				"(End)",
				"The kind of phenomenon observed at the end of the previous comment holds in a very strong way for the tribonacci word. See Theorem 5.1. in the paper by Huang and Wen. - _Michel Dekking_, Oct 04 2019"
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A277735/b277735.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e",
				"Y.-K. Huang, Z.-Y. Wen, \u003ca href=\"https://doi.org/10.1016/S0252-9602(15)30086-2\"\u003eKernel words and gap sequence of the Tribonacci sequence\u003c/a\u003e, Acta Mathematica Scientia (Series B). 36.1 (2016) 173-194.",
				"Victor F. Sirvent, \u003ca href=\"https://doi.org/10.1016/S0893-9659(98)00121-9\"\u003eSemigroups and the self-similar structure of the flipped tribonacci substitution\u003c/a\u003e, Applied Math. Letters, 12 (1999), 25-29.",
				"Victor F. Sirvent, \u003ca href=\"https://doi.org/10.36045/bbms/1103055617\"\u003eThe common dynamics of the Tribonacci substitutions\u003c/a\u003e, Bulletin of the Belgian Mathematical Society-Simon Stevin 7.4 (2000): 571-582.",
				"\u003ca href=\"/index/Fi#FIXEDPOINTS\"\u003eIndex entries for sequences that are fixed points of mappings\u003c/a\u003e"
			],
			"maple": [
				"with(ListTools);",
				"T:=proc(S) Flatten(subs( {0=[0,1], 1=[2,0], 2=[0]}, S)); end;",
				"S:=[0];",
				"for n from 1 to 10 do S:=T(S); od:",
				"S;"
			],
			"mathematica": [
				"s = Nest[Flatten[# /. {0 -\u003e {0, 1}, 1 -\u003e {2, 0}, 2 -\u003e 0}] \u0026, {0}, 10] (* A277735 *)",
				"Flatten[Position[s, 0]] (* A277736 *)",
				"Flatten[Position[s, 1]] (* A277737 *)",
				"Flatten[Position[s, 2]] (* A277738 *)",
				"(* _Clark Kimberling_, May 21 2017 *)"
			],
			"xref": [
				"Cf. A277736, A277737, A277738.",
				"Equals A100619(n)-1."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_, Nov 07 2016",
			"ext": [
				"Name clarified by _Michel Dekking_, Oct 03 2019"
			],
			"references": 5,
			"revision": 44,
			"time": "2019-10-04T09:12:32-04:00",
			"created": "2016-11-07T11:07:39-05:00"
		}
	]
}