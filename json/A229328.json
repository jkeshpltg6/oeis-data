{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A229328",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 229328,
			"data": "0,1,66,797,5024,21447,73920,212951,552378,1292410,2838234,5823262,11464628,21488403,39094986,68600554,117628414,196085189,321067770,513857202,810429626,1254814258,1918760856,2889290459,4305268546,6331543700,9226796660,13297146272",
			"name": "Total sum of 6th powers of parts in all partitions of n.",
			"comment": [
				"The bivariate g.f. for the partition statistic \"sum of 6th powers the parts\" is G(t,x) = 1/Product_{k\u003e=1}(1 - t^{k^6}*x^k). The g.f. g at the Formula section has been obtained by evaluating dG/dt at t=1. - _Emeric Deutsch_, Dec 06 2015"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A229328/b229328.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Guo-Niu Han, \u003ca href=\"https://arxiv.org/abs/0804.1849\"\u003eAn explicit expansion formula for the powers of the Euler Product in terms of partition hook lengths\u003c/a\u003e, arXiv:0804.1849 [math.CO], 2008."
			],
			"formula": [
				"a(n) = Sum_{k=1..n} A066633(n,k) * k^6.",
				"G.f.: g(x) = (Sum_{k\u003e=1} k^6*x^k/(1-x^k))/Product_{q\u003e=1} (1-x^q). - _Emeric Deutsch_, Dec 06 2015",
				"a(n) ~ 38880*sqrt(2)*Zeta(7)/Pi^7 * exp(Pi*sqrt(2*n/3)) * n^(5/2). - _Vaclav Kotesovec_, May 28 2018"
			],
			"maple": [
				"b:= proc(n, i) option remember; `if`(n=0, [1, 0],",
				"      `if`(i\u003c1, [0, 0], `if`(i\u003en, b(n, i-1),",
				"      ((g, h)-\u003e g+h+[0, h[1]*i^6])(b(n, i-1), b(n-i, i)))))",
				"    end:",
				"a:= n-\u003e b(n, n)[2]:",
				"seq(a(n), n=0..40);",
				"# second Maple program:",
				"g := (sum(k^6*x^k/(1-x^k), k = 1..100))/(product(1-x^k, k = 1..100)): gser := series(g, x = 0, 45): seq(coeff(gser, x, m), m = 1 .. 40); # _Emeric Deutsch_, Dec 06 2015"
			],
			"mathematica": [
				"(* T = A066633 *) T[n_, n_] = 1; T[n_, k_] /; k \u003c n := T[n, k] = T[n - k, k] + PartitionsP[n - k]; T[_, _] = 0; a[n_] := Sum[T[n, k]*k^6, {k, 1, n}]; Array[a, 40, 0] (* _Jean-François Alcover_, Dec 15 2016 *)"
			],
			"xref": [
				"Column k=6 of A213191."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Alois P. Heinz_, Sep 20 2013",
			"references": 2,
			"revision": 21,
			"time": "2018-05-28T02:53:02-04:00",
			"created": "2013-09-20T07:24:31-04:00"
		}
	]
}