{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A328431",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 328431,
			"data": "1,1,2,5,15,53,214,960,4701,24873,141147,853641,5472642,37024569,263342224,1962835806,15288074104,124120865849,1048092680689,9186689045482,83435365244510,783923558286071,7608398620990535,76177574145052258,785853360840424425",
			"name": "Number of inversion sequences of length n avoiding the consecutive patterns 010, 021, 120, and 210.",
			"comment": [
				"A length n inversion sequence e_1e_2...e_n is a sequence of integers such that 0 \u003c= e_i \u003c i. The term a(n) counts the inversion sequences of length n with no entries e_i, e_{i+1}, e_{i+2} such that e_i != e_{i+1} \u003e e_{i+2}. This is the same as the set of inversion sequences of length n avoiding the consecutive patterns 010, 021, 120, and 210."
			],
			"link": [
				"Juan S. Auli and Sergi Elizalde, \u003ca href=\"https://arxiv.org/abs/1906.07365\"\u003eConsecutive patterns in inversion sequences II: avoiding patterns of relations\u003c/a\u003e, arXiv:1906.07365 [math.CO], 2019."
			],
			"example": [
				"The a(4)=15 length 4 inversion sequences avoiding the consecutive patterns 010, 021, 120, and 210 are 0000, 0110, 0001, 0011, 0111, 0002, 0012, 0112, 0022, 0122, 0003, 0013, 0113, 0023, and 0123."
			],
			"maple": [
				"# after _Alois P. Heinz_ in A328357",
				"b := proc(n, x, t) option remember; `if`(n = 0, 1, add(",
				"       `if`(t and i \u003c\u003e x, 0, b(n - 1, i, x \u003c i)), i = 0 .. n - 1))",
				"     end proc:",
				"a := n -\u003e b(n, n, false):",
				"seq(a(n), n = 0 .. 24);"
			],
			"mathematica": [
				"b[n_, x_, t_] := b[n, x, t] = If[n == 0, 1, Sum[If[t \u0026\u0026 i != x, 0, b[n - 1, i, x \u003c i]], {i, 0, n - 1}]];",
				"a[n_] := b[n, n, False];",
				"a /@ Range[0, 24] (* _Jean-François Alcover_, Mar 02 2020 after _Alois P.Heinz_ in A328357 *)"
			],
			"xref": [
				"Cf. A328357, A328358, A328429, A328430, A328432, A328433, A328434, A328435, A328436, A328437, A328438, A328439, A328440, A328441, A328442."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Juan S. Auli_, Oct 16 2019",
			"references": 15,
			"revision": 13,
			"time": "2020-03-02T09:39:57-05:00",
			"created": "2019-10-16T12:33:54-04:00"
		}
	]
}