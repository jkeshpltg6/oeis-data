{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A239264",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 239264,
			"data": "1,1,1,1,0,1,1,1,1,1,1,0,3,0,1,1,1,5,5,1,1,1,0,11,0,11,0,1,1,1,21,43,43,21,1,1,1,0,43,0,280,0,43,0,1,1,1,85,451,1563,1563,451,85,1,1,1,0,171,0,9415,0,9415,0,171,0,1,1,1,341,4945,55553,162409,162409,55553,4945,341,1,1",
			"name": "Number A(n,k) of domicule tilings of a k X n grid; square array A(n,k), n\u003e=0, k\u003e=0, read by antidiagonals.",
			"comment": [
				"A domicule is either a domino or it is formed by the union of two neighboring unit squares connected via their corners.  In a tiling the connections of two domicules are allowed to cross each other."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A239264/b239264.txt\"\u003eAntidiagonals n = 0..36, flattened\u003c/a\u003e"
			],
			"example": [
				"A(3,2) = 5:",
				"  +-----+ +-----+ +-----+ +-----+ +-----+",
				"  |o o-o| |o o o| |o o o| |o o o| |o-o o|",
				"  ||    | ||  X | || | || | X  || |    ||",
				"  |o o-o| |o o o| |o o o| |o o o| |o-o o|",
				"  +-----+ +-----+ +-----+ +-----+ +-----+",
				"A(4,3) = 43:",
				"  +-------+ +-------+ +-------+ +-------+ +-------+",
				"  |o o o o| |o o o-o| |o o-o o| |o o-o o| |o o-o o|",
				"  ||  X  || | X     | | \\   / | ||     || | \\    ||",
				"  |o o o o| |o o o o| |o o o o| |o o o o| |o o o o|",
				"  |       | |     X | ||     || |   \\ \\ | ||    \\ |",
				"  |o-o o-o| |o-o o o| |o o-o o| |o-o o o| |o o-o o|",
				"  +-------+ +-------+ +-------+ +-------+ +-------+ ...",
				"Square array A(n,k) begins:",
				"  1, 1,  1,   1,    1,      1,       1, ...",
				"  1, 0,  1,   0,    1,      0,       1, ...",
				"  1, 1,  3,   5,   11,     21,      43, ...",
				"  1, 0,  5,   0,   43,      0,     451, ...",
				"  1, 1, 11,  43,  280,   1563,    9415, ...",
				"  1, 0, 21,   0, 1563,      0,  162409, ...",
				"  1, 1, 43, 451, 9415, 162409, 3037561, ..."
			],
			"maple": [
				"b:= proc(n, l) option remember; local d, f, k;",
				"      d:= nops(l)/2; f:=false;",
				"      if n=0 then 1",
				"    elif l[1..d]=[f$d] then b(n-1, [l[d+1..2*d][], true$d])",
				"    else for k to d while not l[k] do od;",
				"         `if`(k\u003cd and n\u003e1 and l[k+d+1],",
				"                              b(n, subsop(k=f, k+d+1=f, l)), 0)+",
				"         `if`(k\u003e1 and n\u003e1 and l[k+d-1],",
				"                              b(n, subsop(k=f, k+d-1=f, l)), 0)+",
				"         `if`(n\u003e1 and l[k+d], b(n, subsop(k=f, k+d=f, l)), 0)+",
				"         `if`(k\u003cd and l[k+1], b(n, subsop(k=f, k+1=f, l)), 0)",
				"      fi",
				"    end:",
				"A:= (n, k)-\u003e `if`(irem(n*k, 2)\u003e0, 0,",
				"    `if`(k\u003en, A(k, n), b(n, [true$(k*2)]))):",
				"seq(seq(A(n, d-n), n=0..d), d=0..14);"
			],
			"mathematica": [
				"b[n_, l_List] := b[n, l] = Module[{d = Length[l]/2, f = False, k}, Which [n == 0, 1, l[[1 ;; d]] == Array[f\u0026, d], b[n-1, Join[l[[d+1 ;; 2*d]], Array[True\u0026, d]]], True, For[k=1, !l[[k]], k++]; If[k\u003cd \u0026\u0026 n\u003e1 \u0026\u0026 l[[k+d+1]], b[n, ReplacePart[l, {k -\u003e f, k+d+1 -\u003e f}]], 0] + If[k\u003e1 \u0026\u0026 n\u003e1 \u0026\u0026 l[[k+d-1]], b[n, ReplacePart[l, {k -\u003e f, k+d-1 -\u003e f}]], 0] + If[n\u003e1 \u0026\u0026 l[[k+d]], b[n, ReplacePart[l, {k -\u003e f, k+d -\u003e f}]], 0] + If[k\u003cd \u0026\u0026 l[[k+1]], b[n, ReplacePart[l, {k -\u003e f, k+1 -\u003e f}]], 0]]]; A[n_, k_] := If[Mod[n*k, 2]\u003e0, 0, If[k\u003en, A[k, n], b[n, Array[True\u0026, k*2]]]]; Table[Table[A[n, d-n], {n, 0, d}], {d, 0, 14}] // Flatten (* _Jean-François Alcover_, Feb 02 2015, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Columns (or rows) k=0-10 give: A000012, A059841, A001045(n+1), A239265, A239266, A239267, A239268, A239269, A239270, A239271, A239272.",
				"Bisection of main diagonal gives: A239273.",
				"Cf. A099390, A187616, A187617, A187596, A220644."
			],
			"keyword": "nonn,tabl",
			"offset": "0,13",
			"author": "_Alois P. Heinz_, Mar 13 2014",
			"references": 12,
			"revision": 25,
			"time": "2018-11-23T06:53:00-05:00",
			"created": "2014-03-19T14:02:45-04:00"
		}
	]
}