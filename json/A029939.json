{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A029939",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 29939,
			"data": "1,2,5,6,17,10,37,22,41,34,101,30,145,74,85,86,257,82,325,102,185,202,485,110,417,290,365,222,785,170,901,342,505,514,629,246,1297,650,725,374,1601,370,1765,606,697,970,2117,430,1801,834,1285,870,2705,730,1717,814,1625",
			"name": "a(n) = Sum_{d|n} phi(d)^2.",
			"comment": [
				"Equals the inverse Mobius transform (A051731) of A127473. [_Gary W. Adamson_, Aug 20 2008]",
				"Number of (i,j) in {1,2,..,n}^2 such that gcd(n,i)=gcd(n,j). [_Benoit Cloitre_, Dec 31 2020]"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A029939/b029939.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"Multiplicative with a(p^e) = (p^(2*e)*(p-1)+2)/(p+1). - _Vladeta Jovovic_, Nov 19 2001",
				"G.f.: Sum_{k\u003e=1} phi(k)^2*x^k/(1 - x^k), where phi(k) is the Euler totient function (A000010). - _Ilya Gutkovskiy_, Jan 16 2017",
				"a(n) = Sum_{k=1..n} phi(n/gcd(n, k)). - _Ridouane Oudra_, Nov 28 2019",
				"Sum_{k\u003e=1} 1/a(k) = 2.3943802654751092440350752246012273573942903149891228695146514601814537713... - _Vaclav Kotesovec_, Sep 20 2020"
			],
			"maple": [
				"with(numtheory): A029939 := proc(n) local i,j; j := 0; for i in divisors(n) do j := j+phi(i)^2; od; j; end;",
				"# alternative",
				"N:= 1000: # to get a(1)..a(N)",
				"A:= Vector(N,1):",
				"for d from 2 to N do",
				"  pd:= numtheory:-phi(d)^2;",
				"  md:= [seq(i,i=d..N,d)];",
				"  A[md]:= map(`+`,A[md],pd);",
				"od:",
				"seq(A[i],i=1..N); # _Robert Israel_, May 30 2016"
			],
			"mathematica": [
				"Table[Total[EulerPhi[Divisors[n]]^2],{n,60}] (* _Harvey P. Dale_, Feb 04 2017 *)",
				"f[p_, e_] := (p^(2*e)*(p-1)+2)/(p+1); a[1] = 1; a[n_] := Times @@ f @@@ FactorInteger[n]; Array[a, 100] (* _Amiram Eldar_, Sep 18 2020 *)"
			],
			"program": [
				"(PARI) a(n) = sumdiv(n, d, eulerphi(d)^2); \\\\ _Michel Marcus_, Jan 17 2017"
			],
			"xref": [
				"Cf. A051731, A062367, A127473."
			],
			"keyword": "nonn,mult,easy",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"references": 15,
			"revision": 42,
			"time": "2020-12-31T07:28:04-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}