{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A227774",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 227774,
			"data": "1,1,1,1,1,2,1,3,3,6,5,1,12,11,2,25,22,5,52,49,12,113,104,28,2,247,232,65,4,548,513,152,13,1226,1159,351,34,2770,2619,818,91,1,6299,5989,1907,225,6,14426,13734,4460,571,18,33209,31729,10453,1403,57,76851",
			"name": "Triangular array read by rows: T(n,k) is the number of rooted identity trees with n nodes having exactly k subtrees from the root.",
			"comment": [
				"Row sums = A004111."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A227774/b227774.txt\"\u003eRows n = 1..400, flattened\u003c/a\u003e"
			],
			"formula": [
				"G.f.: x * Product_{n\u003e=1} (1 + y * x^n)^A004111(n).",
				"From _Alois P. Heinz_, Aug 25 2017: (Start)",
				"T(n,k) = Sum_{h=0..n-k} A291529(n-1,h,k).",
				"Sum_{k\u003e=1} k * T(n,k) = A291532(n-1). (End)"
			],
			"example": [
				"Triangular array T(n,k) begins:",
				"n\\k:   0    1    2   3   4  ...",
				"---+---------------------------",
				"01 :   1;",
				"02 :   .    1;",
				"03 :   .    1;",
				"04 :   .    1,   1;",
				"05 :   .    2,   1;",
				"06 :   .    3,   3;",
				"07 :   .    6,   5,  1;",
				"08 :   .   12,  11,  2;",
				"09 :   .   25,  22,  5;",
				"10 :   .   52,  49, 12;",
				"11 :   .  113, 104, 28,  2;"
			],
			"maple": [
				"b:= proc(n, i) option remember; `if`(n=0, 1, `if`(i\u003c1, 0,",
				"      add(binomial(b((i-1)$2), j)*b(n-i*j, i-1), j=0..n/i)))",
				"    end:",
				"g:= proc(n, i) option remember; `if`(n=0, 1, `if`(i\u003c1, 0, expand(",
				"      add(x^j*binomial(b((i-1)$2), j)*g(n-i*j, i-1), j=0..n/i))))",
				"    end:",
				"T:= n-\u003e `if`(n=1, 1,",
				"    (p-\u003e seq(coeff(p, x, k), k=1..degree(p)))(g((n-1)$2))):",
				"seq(T(n), n=1..25);  # _Alois P. Heinz_, Jul 30 2013"
			],
			"mathematica": [
				"nn=20;f[x_]:=Sum[a[n]x^n,{n,0,nn}];sol=SolveAlways[0==Series[f[x]-x Product[(1+x^i)^a[i],{i,1,nn}],{x,0,nn}],x];A004111=Drop[ Flatten[Table[a[n],{n,0,nn}]/.sol],1];Map[Select[#,#\u003e0\u0026]\u0026, Drop[CoefficientList[Series[x Product[(1 + y x^i)^A004111[[i]],{i,1,nn}],{x,0,nn}],{x,y}],1]]//Grid"
			],
			"program": [
				"(Python)",
				"from sympy import binomial, Poly, Symbol",
				"from sympy.core.cache import cacheit",
				"x=Symbol('x')",
				"@cacheit",
				"def b(n, i):return 1 if n==0 else 0 if i\u003c1 else sum([binomial(b(i - 1, i - 1), j)*b(n - i*j, i - 1) for j in range(n//i + 1)])",
				"@cacheit",
				"def g(n, i):return 1 if n==0 else 0 if i\u003c1 else sum([x**j*binomial(b(i - 1, i - 1), j)*g(n - i*j, i - 1) for j in range(n//i + 1)])",
				"def T(n): return [1] if n==1 else Poly(g(n - 1, n - 1)).all_coeffs()[::-1][1:]",
				"for n in range(1, 26): print(T(n)) # _Indranil Ghosh_, Aug 28 2017"
			],
			"xref": [
				"Columns k=1-10 give: A004111(n-1), A227806, A227807, A227808, A227809, A227810, A227811, A227812, A227813, A227814.",
				"Cf. A291529, A291532."
			],
			"keyword": "nonn,tabf,look",
			"offset": "1,6",
			"author": "_Geoffrey Critzer_, Jul 30 2013",
			"references": 12,
			"revision": 52,
			"time": "2020-03-23T06:21:53-04:00",
			"created": "2013-07-30T20:52:28-04:00"
		}
	]
}