{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A329715",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 329715,
			"data": "9,5,8,3,8,0,4,5,4,5,6,3,0,9,4,5,6,2,0,5,1,6,6,9,4,0,2,8,6,1,5,7,7,8,1,8,8,2,4,8,9,5,3,1,7,9,3,9,7,7,5,3,4,0,7,5,7,5,0,4,5,0,7,0,4,7,0,7,5,6,9,7,4,8,4,2,9,7,9,3,6,4,7,8,2,5,2,6,9,9,7",
			"name": "Decimal expansion of Sum_{k\u003e=1} Kronecker(8,k)/k^3.",
			"comment": [
				"Let Chi() be a primitive character modulo d, the so-called Dirichlet L-series L(s,Chi) is the analytic continuation (see the functional equations involving L(s,Chi) in the MathWorld link entitled Dirichlet L-Series) of the sum Sum_{k\u003e=1} Chi(k)/k^s, Re(s)\u003e0 (if d = 1, the sum converges requires Re(s)\u003e1).",
				"If s != 1, we can represent L(s,Chi) in terms of the Hurwitz zeta function by L(s,Chi) = (Sum_{k=1..d} Chi(k)*zeta(s,k/d))/d^s.",
				"L(s,Chi) can also be represented in terms of the polylog function by L(s,Chi) = (Sum_{k=1..d} Chi'(k)*polylog(s,u^k))/(Sum_{k=1..d} Chi'(k)*u^k), where Chi' is the complex conjugate of Chi, u is any primitive d-th root of unity.",
				"If m is a positive integer, we have L(m,Chi) = (Sum_{k=1..d} Chi(k)*polygamma(m-1,k/d))/((-d)^m*(m-1)!).",
				"In this sequence we have Chi = A091337 and s = 3."
			],
			"link": [
				"Steven R. Finch, \u003ca href=\"https://doi.org/10.1017/9781316997741\"\u003eMathematical Constants II\u003c/a\u003e, Encyclopedia of Mathematics and Its Applications, Cambridge University Press, Cambridge, 2018, p. 99.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DirichletL-Series.html\"\u003eDirichlet L-Series\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PolygammaFunction.html\"\u003ePolygamma Function\u003c/a\u003e"
			],
			"formula": [
				"Equals (zeta(3,1/8) - zeta(3,3/8) - zeta(3,5/8) + zeta(3,7/8))/512, where zeta(s,a) is the Hurwitz zeta function.",
				"Equals (polylog(3,u) - polylog(3,u^3) - polylog(3,-u) + polylog(3,-u^3))/sqrt(8), where u = sqrt(2)/2 + i*sqrt(2)/2 is an 8th primitive root of unity, i = sqrt(-1).",
				"Equals (polygamma(2,1/8) - polygamma(2,3/8) - polygamma(2,5/8) + polygamma(2,7/8))/(-1024)."
			],
			"example": [
				"1 - 1/3^3 - 1/5^3 + 1/7^3 + 1/9^3 - 1/11^3 - 1/13^3 + 1/15^3 + ... = 0.9583804545..."
			],
			"mathematica": [
				"(PolyGamma[2, 1/8] - PolyGamma[2, 3/8] - PolyGamma[2, 5/8] + PolyGamma[2, 7/8])/(-1024) // RealDigits[#, 10, 102] \u0026 // First"
			],
			"xref": [
				"Cf. A091337.",
				"Decimal expansion of Sum_{k\u003e=1} Kronecker(d,k)/k^3, where d is a fundamental discriminant: A251809 (d=-8), A327135 (d=-7), A153071 (d=-4), A129404 (d=-3), A002117 (d=1), A328723 (d=5), this sequence (d=8), A329716 (d=12).",
				"Decimal expansion of Sum_{k\u003e=1} Kronecker(8,k)/k^s: A195625 (s=1), A328895 (s=2), this sequence (s=3)."
			],
			"keyword": "nonn,cons",
			"offset": "0,1",
			"author": "_Jianing Song_, Nov 19 2019",
			"references": 4,
			"revision": 9,
			"time": "2021-06-13T05:31:03-04:00",
			"created": "2019-11-21T07:37:46-05:00"
		}
	]
}