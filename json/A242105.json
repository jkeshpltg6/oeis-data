{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A242105",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 242105,
			"data": "1,2,7,44,428,5802,102322,2239844,58849332,1810039960,63930543419,2553881719348,113979459829296,5625823639358928,304505544257483550,17944306197698666740,1144180970802458374244,78517953136289477587608,5771772521253777092098050",
			"name": "Number of sequences (x(k))_{k=1..n}, of n strictly increasing terms of nonnegative integers {x(1)\u003cx(2)\u003c...\u003cx(n)}, satisfying x(k) \u003c= k^2 for all k.",
			"comment": [
				"Also the first occurring nonzero terms in rows of triangle A261897, without repetitions. - _Reinhard Zumkeller_, Sep 06 2015"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A242105/b242105.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"L. Haddad and C. Helou, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL18/Helou/helou7.html\"\u003eFinite Sequences Dominated by the Squares\u003c/a\u003e, Journal of Integer Sequences, Volume 18, 2015, Issue 1, Article 15.1.8."
			],
			"formula": [
				"a(n) = Sum_{k=1..n} (-1)^{k-1}* C((n-k+1)^2+k-1,k) * a(n-k), for n\u003e1.",
				"a(n) = C(n^2,n-1) + C(n^2-1,n-1) - Sum_{k=2..n-1} C(n^2-k^2,n-k+1) *a(k-1), for n\u003e1.",
				"Conjecture: lim n-\u003einfinity a(n)^(1/n)/n = 2. - _Vaclav Kotesovec_, Feb 26 2017"
			],
			"example": [
				"For n=2 the a(2) = 7 solutions are (0,1), (0,2), (0,3), (0,4), (1,2), (1,3), (1,4)."
			],
			"maple": [
				"a:= proc(n) option remember; `if`(n\u003c2, n+1, add((-1)^(k-1)*",
				"       binomial((n-k+1)^2+k-1, k) * a(n-k), k=1..n))",
				"    end:",
				"seq(a(n), n=0..25);  # _Alois P. Heinz_, Aug 15 2014"
			],
			"mathematica": [
				"a[0] = 1; a[1] = 2; a[n_] := a[n] = Sum[(-1)^(k-1)*Binomial[(n-k+1)^2+k-1, k]*a[n-k], {k, 1, n}]; Table[a[n], {n, 0, 25}] (* _Jean-François Alcover_, Feb 26 2017 *)"
			],
			"xref": [
				"Cf. A261897."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Charles Helou_, Aug 14 2014",
			"references": 2,
			"revision": 33,
			"time": "2020-07-22T11:42:35-04:00",
			"created": "2014-08-16T11:46:41-04:00"
		}
	]
}