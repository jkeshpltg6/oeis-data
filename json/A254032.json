{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A254032",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 254032,
			"data": "0,1,1,2,3,5,8,13,7,20,27,47,37,84,11,19,30,49,79,128,23,151,29,180,209,389,598,987,317,163,480,643,1123,883,17,900,131,1031,83,557,640,1197,167,31,198,229,61,290,351,641,992,71,1063,1134,2197,3331,691",
			"name": "a(0)=0, a(1)=1, and for n\u003e2, a(n) is the smallest prime factor of a(n-1)+a(n-2) not already in the sequence or, if there is no such prime, a(n)=a(n-1)+a(n-2).",
			"comment": [
				"Empirically this sequence grows slower than A000045(n), the Fibonacci sequence, but faster than log(A000045(n)). - _Kellen Myers_ May 10 2015",
				"Note that in the case where no suitable prime divisor exists, a(n) must take the value a(n-1)+a(n-2) regardless of whether it appears previously. This allows for repetition, e.g., a(86)=a(99)=957. Among the first 1000 terms, there are 9 values a(n) takes twice. - _Kellen Myers_, May 10 2015"
			],
			"link": [
				"Kellen Myers, \u003ca href=\"/A254032/b254032.txt\"\u003eTable of n, a(n) for n = 0..999\u003c/a\u003e"
			],
			"example": [
				"The first nonprime Fibonacci number is F(5)=8, and so this is the first place that a(n) could disagree with F(n). However, the only prime factor of 8 is 2, which appears as a(2), and thus a(5) must be 8.",
				"For n=7, a(n-1)+a(n-2)=21. The prime factors of 21 are 3 and 7, and 7 does not yet appear, so a(7)=7."
			],
			"mathematica": [
				"a[n_] := a[n] =",
				"   Module[{set = seq[n - 1], val = a[n - 1] + a[n - 2], p},",
				"    p = 2;",
				"    While[(Mod[val, p] != 0 || MemberQ[set, p]) \u0026\u0026 p \u003c= val,",
				"     p = NextPrime[p]",
				"     ];",
				"    If[p \u003e val, Return[val], Return[p]];",
				"    ];",
				"seq[n_] := seq[n] = Append[seq[n - 1], a[n]]",
				"a[1] = 0; a[2] = 1;",
				"seq[2] = {0, 1};",
				"(* _Kellen Myers_, May 10 2015 *)"
			],
			"xref": [
				"Cf. A000045, A214094."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_David S. Newman_, Jan 22 2015",
			"ext": [
				"First 1000 terms, clarification of definition, examples by _Kellen Myers_, May 10 2015"
			],
			"references": 1,
			"revision": 11,
			"time": "2015-07-16T22:41:44-04:00",
			"created": "2015-05-12T20:30:29-04:00"
		}
	]
}