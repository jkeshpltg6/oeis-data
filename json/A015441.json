{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A015441",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 15441,
			"data": "0,1,1,7,13,55,133,463,1261,4039,11605,35839,105469,320503,953317,2876335,8596237,25854247,77431669,232557151,697147165,2092490071,6275373061,18830313487,56482551853,169464432775,508359743893,1525146340543,4575304803901,13726182847159",
			"name": "Generalized Fibonacci numbers.",
			"comment": [
				"a(n) is the coefficient of x^(n-1) in the bivariate Fibonacci polynomials F(n)(x,y) = xF(n-1)(x,y) + yF(n-2)(x,y), F(0)(x,y)=0, F(1)(x,y)=1, when y=6x^2. - Mario Catalani (mario.catalani(AT)unito.it), Dec 06 2002",
				"For n\u003e=1: number of length-(n-1) strings with letters {0,1,2,3,4,5,6,7} where no two consecutive letters are nonzero, see fxtbook link below. - _Joerg Arndt_, Apr 08 2011",
				"Starting with offset 1 and convolved with (1, 3, 3, 3, ...) = A003462: (1, 4, 13, 40, ...). - _Gary W. Adamson_, May 28 2009",
				"a(n) is identical to its inverse binomial transform signed. Differences: A102901. - _Paul Curtz_, Feb 23 2010",
				"The compositions of n in which each natural number is colored by one of p different colors are called p-colored compositions of n. For n\u003e=2, 7*a(n-2) equals the number of 7-colored compositions of n with all parts \u003e=2, such that no adjacent parts have the same color. - _Milan Janjic_, Nov 26 2011",
				"Pisano period lengths: 1, 1, 1, 2, 20, 1, 6, 2, 3, 20, 5, 2, 12, 6, 20, 4, 16, 3, 18, 20, ... - _R. J. Mathar_, Aug 10 2012",
				"A015441 and A015518 are the only integer sequences (from the family of homogeneous linear recurrence relation of order 2 with positive integer coefficients with initial values a(0)=0 and a(1)=1) whose ratio a(n+1)/a(n) converges to 3 as n approaches infinity. - _Felix P. Muga II_, Mar 14 2014",
				"This is an autosequence of the first kind: the array of successive differences shows a main diagonal of zeros and the inverse binomial transform is identical to the sequence (with alternating signs). - Pointed out by _Paul Curtz_, Dec 05 2016",
				"First two upper diagonals: A000400(n).",
				"This is a variation on the Starhex honeycomb configuration A332243, see illustration in links. It is an alternating pattern of the 2nd iteration of the centered hexagonal numbers A003215 and centered 12-gonal 'Star' numbers A003154. - _John Elias_, Oct 06 2021"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A015441/b015441.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e (terms 0..200 from T. D. Noe)",
				"Joerg Arndt, \u003ca href=\"http://www.jjj.de/fxt/#fxtbook\"\u003eMatters Computational (The Fxtbook)\u003c/a\u003e, p. 317-318",
				"A. Abdurrahman, \u003ca href=\"https://arxiv.org/abs/1909.10889\"\u003eCM Method and Expansion of Numbers\u003c/a\u003e, arXiv:1909.10889 [math.NT], 2019.",
				"John Elias, \u003ca href=\"/A015441/a015441.png\"\u003eIllustration: A Starhex honeycomb variation\u003c/a\u003e",
				"M. Janjic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL18/Janjic/janjic63.html\"\u003eOn Linear Recurrence Equations Arising from Compositions of Positive Integers\u003c/a\u003e, Journal of Integer Sequences, Vol. 18 (2015), Article 15.4.7.",
				"H. Li and T. MacHenry, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL16/MacHenry/machenry7.html\"\u003ePermanents and Determinants, Weighted Isobaric Polynomials, and Integer Sequences\u003c/a\u003e, J. Int. Seq. 16 (2013) #13.3.5, example 48.",
				"F. P. Muga II, \u003ca href=\"https://www.researchgate.net/publication/267327689_Extending_the_Golden_Ratio_and_the_Binet-de_Moivre_Formula\"\u003eExtending the Golden Ratio and the Binet-de Moivre Formula\u003c/a\u003e, March 2014; Preprint on ResearchGate.",
				"A. G. Shannon and J. V. Leyendekkers, \u003ca href=\"http://nntdm.net/volume-21-2015/number-2/35-42/\"\u003eThe Golden Ratio family and the Binet equation\u003c/a\u003e, Notes on Number Theory and Discrete Mathematics, Vol. 21, No. 2, (2015), 35-42.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,6)"
			],
			"formula": [
				"G.f.: x/((1+2*x)*(1-3*x)).",
				"a(n) = a(n-1) + 6*a(n-2).",
				"a(n) = (1/5)*((3^n)-((-2)^n)). - henryk.wicke(AT)stud.uni-hannover.de",
				"E.g.f.: (exp(3*x) - exp(-2*x))/5. - _Paul Barry_, Apr 20 2003",
				"a(n+1) = Sum_{k=0..ceiling(n/2)} 6^k*binomial(n-k, k). - _Benoit Cloitre_, Mar 06 2004",
				"a(n) = (A000244(n) - A001045(n+1)(-1)^n - A001045(n)(-1)^n)/5. - _Paul Barry_, Apr 27 2004",
				"The binomial transform of [1,1,7,13,55,133,463,...] is A122117. - _Philippe Deléham_, Oct 19 2006",
				"a(n+1) = Sum_{k=0..n} A109466(n,k)*(-6)^(n-k). - _Philippe Deléham_, Oct 26 2008",
				"a(n) = 3a(n-1) + (-1)^(n+1)*A000079(n-1). - _Paul Curtz_, Feb 23 2010",
				"G.f.: Q(0) -1, where Q(k) = 1 + 6*x^2 + (k+2)*x - x*(k+1 + 6*x)/Q(k+1); (continued fraction). - _Sergei N. Gladkovskii_, Oct 06 2013",
				"a(n) = (Sum_{1\u003c=k\u003c=n, k odd} binomial(n,k)*5^(k-1))/2^(n-1). - _Vladimir Shevelev_, Feb 05 2014",
				"a(-n) = -(-1)^n * a(n) / 6^n for all n in Z. - _Michael Somos_, Mar 18 2014",
				"From _Peter Bala, Apr 01 2015: (Start)",
				"Sum_{n \u003e= 0} a(n+1)*x^n = exp( Sum_{n \u003e= 1} A087451(n)*x^n/n ).",
				"For k = 0, 1, 2, ... and for n \u003e= 1, 5^k*a(n) | a(5^k*n).",
				"The expansion of exp( Sum_{n \u003e= 1} a(5*n)/(5*a(n))*x^n/n ) has integral coefficients. Cf. A001656. (End)"
			],
			"example": [
				"G.f. = x + x^2 + 7*x^3 + 13*x^4 + 55*x^5 + 133*x^6 + 463*x^7 + 1261*x^8 + ..."
			],
			"maple": [
				"A015441:=n-\u003e(1/5)*((3^n)-((-2)^n)); seq(A015441(n), n=0..30); # _Wesley Ivan Hurt_, Mar 14 2014"
			],
			"mathematica": [
				"a[n_]:=(MatrixPower[{{1,4},{1,-2}},n].{{1},{1}})[[2,1]]; Table[Abs[a[n]], {n,-1,40}] (* _Vladimir Joseph Stephan Orlovsky_, Feb 19 2010 *)",
				"LinearRecurrence[{1,6},{0,1},30] (* _Harvey P. Dale_, Apr 26 2011 *)",
				"CoefficientList[Series[x/((1 + 2 x) (1 - 3 x)), {x, 0, 29}], x] (* _Michael De Vlieger_, Dec 05 2016 *)"
			],
			"program": [
				"(PARI) {a(n) = (3^n - (-2)^n) / 5};",
				"(Sage) [lucas_number1(n,1,-6) for n in range(0, 27)] # _Zerinvary Lajos_, Apr 22 2009",
				"(MAGMA) I:=[0,1]; [n le 2 select I[n] else Self(n-1) + 6*Self(n-2): n in [1..30]]; // _G. C. Greubel_, Jan 24 2018"
			],
			"xref": [
				"Cf. A000079, A003462, A016153, A109466, A122117. Cf. A001656, A087451. Cf. A000400."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,4",
			"author": "_Olivier Gérard_",
			"references": 46,
			"revision": 124,
			"time": "2021-11-27T10:57:30-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}