{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A341846",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 341846,
			"data": "0,0,1,0,2,0,2,2,1,6,0,5,1,4,2,7,0,6,8,0,3,6,4,9,0,5,14,0,3,8,11,1,19,0,6,13,2,22,0,5,14,14,1,11,13,9,22,9,2,12,8,21,2,4,31,0,17,5,18,5,2,8,11,19,31,10,18,8,6,34,0,15,11,10,8,7,60,0,7",
			"name": "a(1) = 0; thereafter, if a(n) has not appeared before, a(n+1) = number of existing terms which are greater than a(n); otherwise, a(n) = n-m where a(m) is the most recent copy of a(n).",
			"comment": [
				"The sequence is nontrivial only with a(1) = 0 or 1. a(1)=0 here for contrast with the Van Eck sequence A181391, with which data agrees up to a(12). For n \u003e 1, a(n)=0 if and only if a(n-1) is a record novel term, whereas every non-record novel term is followed by a term \u003e 0. Definition implies a(n) \u003c n for all n. Open questions: Is it true that a(n) + a(n+1) \u003c n for all n? (This is true for n \u003c= 65000.) Do records ever arise from rule 1? Does every number appear? (If so, 0 appears infinitely many times.)"
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A341846/b341846.txt\"\u003eTable of n, a(n) for n = 1..10001\u003c/a\u003e",
				"Michael De Vlieger, \u003ca href=\"/A341846/a341846.png\"\u003eScatterplot of a(n)\u003c/a\u003e for 1 \u003c= n \u003c= 4096, color coded to show records in red, zeros in blue, terms instigated by novel a(n) in green, and records in the last-mentioned terms in orange. The least unused number is indicated by a yellow step function line.",
				"Michael De Vlieger, \u003ca href=\"/A341846/a341846_1.png\"\u003ePlot of b(n) = a(n) + a(n+1)\u003c/a\u003e for 1 \u003c= n \u003c= 65536 showing a partial bifurcation resulting from conditions involving novel a(n+1) such that, for such conditions, b(n) \u003e n/Pi.",
				"Michael De Vlieger, \u003ca href=\"/A341846/a341846_2.png\"\u003eLogarithmic scatterplot of a(n)\u003c/a\u003e for 1 \u003c= n \u003c= 65536."
			],
			"example": [
				"a(2)=0 because a(1)=0 is a novel term, and there are 0 terms \u003e 0.",
				"a(3)=1 because a(2) has been seen before at a(1), and 2-1=1.",
				"a(13)=1, because a(12)=5, a novel term with 1 earlier term (a(10)=6) greater than it."
			],
			"mathematica": [
				"Block[{nn = 120, a = {0}, c}, Do[If[IntegerQ@ c[#], AppendTo[a, i - c[#] ]; Set[c[#], i], Set[c[#], i]; AppendTo[a, Count[Most@ a, _?(# \u003e a[[-1]] \u0026)]]] \u0026[ a[[-1]] ], {i, nn}]; a] (* _Michael De Vlieger_, Feb 21 2021 *)"
			],
			"program": [
				"(Python)",
				"def aupton(terms):",
				"  alst, an = [], 0",
				"  for n in range(1, terms+1):",
				"    if an not in alst: anp1 = sum(ai \u003e an for ai in alst)",
				"    else: anp1 = alst[::-1].index(an) + 1",
				"    alst, an = alst + [an], anp1",
				"  return alst",
				"print(aupton(79)) # _Michael S. Branicky_, Feb 21 2021"
			],
			"xref": [
				"Cf. A181391."
			],
			"keyword": "nonn",
			"offset": "1,5",
			"author": "_David James Sycamore_, Feb 19 2021",
			"references": 2,
			"revision": 16,
			"time": "2021-03-28T19:34:05-04:00",
			"created": "2021-02-21T14:01:58-05:00"
		}
	]
}