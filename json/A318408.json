{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A318408",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 318408,
			"data": "0,0,1,1,1,1,6,1,1,19,19,1,1,48,142,48,1,1,109,730,730,109,1,1,234,3087,6796,3087,234,1,1,487,11637,48355,48355,11637,487,1,1,996,40804,291484,543030,291484,40804,996,1",
			"name": "Triangle read by rows: T(n,k) is the number of permutations of [n+1] with index in the lexicographic ordering of permutations being congruent to 1 or 5 modulo 6 that have exactly k descents; k \u003e 0.",
			"comment": [
				"Note that we assume the permutations are lexicographically ordered in a zero-indexed list from smallest to largest.",
				"Recall that a descent in a permutation p of [n+1] is an index i in [n] such that p(i) \u003e p(i+1).",
				"The n-th row of the triangle T(n,k) is the coefficient vector of the local h^*-polynomial (i.e., the box polynomial) of the factoradic n-simplex. Each row is known to be symmetric and unimodal. Moreover the local h^*-polynomial of the factoradic n-simplex has only real roots. See the paper by L. Solus below for definitions and proofs of these statements.",
				"The n-th row of T(n,k) is the coefficient sequence of a restriction of the n-th Eulerian polynomial, which is given by the n-th row of A008292."
			],
			"link": [
				"L. Solus. \u003ca href=\"https://arxiv.org/abs/1807.08223\"\u003eLocal h^*-polynomials of some weighted projective spaces\u003c/a\u003e, arXiv:1807.08223 [math.CO], 2018. To appear in the Proceedings of the 2018 Summer Workshop on Lattice Polytopes at Osaka University (2018)."
			],
			"example": [
				"The triangle T(n,k) begins:",
				"  n\\k|  1     2     3       4       5       6     7     8    9",
				"  ---+---------------------------------------------------------",
				"  0  |  0",
				"  1  |  0",
				"  2  |  1",
				"  3  |  1     1",
				"  4  |  1     6     1",
				"  5  |  1    19    19       1",
				"  6  |  1    48   142      48       1",
				"  7  |  1   109   730     730     109       1",
				"  8  |  1   234  3087    6796    3087     234     1",
				"  9  |  1   487 11637   48355   48355   11637   487     1",
				"  10 |  1   996 40804  291484  543030  291484 40804   996    1"
			],
			"program": [
				"(Macaulay2)",
				"R = QQ[z];",
				"factoradicBox = n -\u003e (",
				"L := toList(1..(n!-1));",
				"B := {};",
				"for j in L do",
				"if (j%6!=0 and j%6!=2 and j%6!=3 and j%6!=4) then B = append(B,j);",
				"W := B / (i-\u003ez^(i-sum(1..(n-1),j-\u003efloor(i/((n-j)!+(n-1-j)!)))));",
				"return sum(W);",
				");"
			],
			"xref": [
				"Cf. A008292."
			],
			"keyword": "nonn,tabf,more",
			"offset": "0,7",
			"author": "_Liam Solus_, Aug 26 2018",
			"references": 0,
			"revision": 18,
			"time": "2018-09-07T03:59:48-04:00",
			"created": "2018-09-07T03:59:48-04:00"
		}
	]
}