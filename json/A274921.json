{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A274921",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 274921,
			"data": "1,2,3,2,3,2,3,1,3,1,2,1,3,1,2,1,3,1,2,3,2,1,2,3,1,3,2,1,2,3,1,3,2,1,2,3,1,2,1,3,2,3,1,2,3,2,1,3,2,3,1,2,3,2,1,3,2,3,1,2,3,1,3,2,1,3,1,2,3,1,2,1,3,2,1,3,1,2,3,1,2,1,3,2,1,3,1,2,3,1,2,3,2,1,3,2,1,2,3,1,2,3,1,3,2,1",
			"name": "Spiral constructed on the nodes of the triangular net in which each new term is the least positive integer distinct from its neighbors.",
			"comment": [
				"The structure of the spiral has the following properties:",
				"1) Every 1 is surrounded by three equidistant 2's and three equidistant 3's.",
				"2) Every 2 is surrounded by three equidistant 1's and three equidistant 3's.",
				"3) Every 3 is surrounded by three equidistant 1's and three equidistant 2's.",
				"4) Diagonals are periodic sequences with period 3 (A010882 and A130784).",
				"From _Juan Pablo Herrera P._, Nov 16 2016: (Start)",
				"5) Every hexagon with a 1 in its center is the same hexagon as the one in the middle of the spiral.",
				"6) Every triangle whose number of numbers is divisible by 3 has the same number of 1's, 2's, and 3's. For example, a triangle with 6 numbers, has two 1's, two 2's, and two 3's. (End)",
				"a(n) = a(n-2) if n \u003e 2 is in A014591, otherwise a(n) = 6 - a(n-1)-a(n-2). - _Robert Israel_, Sep 15 2017"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A274921/b274921.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A274920(n) + 1."
			],
			"example": [
				"Illustration of initial terms as a spiral:",
				".",
				".                3 - 1 - 2 - 3 - 1 - 2",
				".               /                     \\",
				".              1   2 - 3 - 1 - 2 - 3   1",
				".             /   /                 \\   \\",
				".            2   3   1 - 2 - 3 - 1   2   3",
				".           /   /   /             \\   \\   \\",
				".          3   1   2   3 - 1 - 2   3   1   2",
				".         /   /   /   /         \\   \\   \\   \\",
				".        1   2   3   1   2 - 3   1   2   3   1",
				".       /   /   /   /   /     \\   \\   \\   \\   \\",
				".      2   3   1   2   3   1 - 2   3   1   2   3",
				".       \\   \\   \\   \\   \\         /   /   /   /",
				".        1   2   3   1   2 - 3 - 1   2   3   1",
				".         \\   \\   \\   \\             /   /   /",
				".          3   1   2   3 - 1 - 2 - 3   1   2",
				".           \\   \\   \\                 /   /",
				".            2   3   1 - 2 - 3 - 1 - 2   3",
				".             \\   \\                     /",
				".              1   2 - 3 - 1 - 2 - 3 - 1",
				".               \\",
				".                3 - 1 - 2 - 3 - 1 - 2",
				"."
			],
			"maple": [
				"A[0]:= 1: A[1]:= 2: A[2]:= 3:",
				"b:= 3: c:= 2: d:= 2: e:= 1: f:= 1:",
				"for n from 3 to 200 do",
				"  if n = b then",
				"     r:= b; b:= c + d - f + 1; f:= e; e:= d; d:= c; c:= r;",
				"     A[n]:= A[n-2];",
				"  else",
				"     A[n]:= 6 - A[n-1] - A[n-2];",
				"  fi",
				"od:",
				"seq(A[i],i=0..200); # _Robert Israel_, Sep 15 2017"
			],
			"xref": [
				"Cf. A001399, A010882, A130784, A253186, A274820, A274821, A274920, A275606, A275610."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Omar E. Pol_, Jul 11 2016",
			"references": 10,
			"revision": 49,
			"time": "2017-09-16T03:45:51-04:00",
			"created": "2016-11-14T13:13:05-05:00"
		}
	]
}