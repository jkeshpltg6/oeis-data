{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A176085",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 176085,
			"data": "0,1,3,11,41,155,591,2267,8735,33775,130965,509015,1982269,7732659,30208749,118167055,462760369,1814091011,7118044023,27952660883,109853552255,432021606103,1700093447847,6694137523051,26372544576331,103950885100775,409928481296331",
			"name": "a(n) = A136431(n,n).",
			"comment": [
				"a(n+1) is also the number of sequences of length 2n obeying the regular expression \"0^* (1 or 2)^* 3^*\" and having sum 3n. For example, a(3)=11 because of the sequences 0033, 0123, 0213, 0222, 1113, 1122, 1212, 1221, 2112, 2121, 2211. - _Don Knuth_, May 11 2016"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A176085/b176085.txt\"\u003eTable of n, a(n) for n = 0..300\u003c/a\u003e"
			],
			"formula": [
				"a(n+1) - 4*a(n) = -A081696(n-1).",
				"From _Vaclav Kotesovec_, Oct 21 2012: (Start)",
				"G.f.: x*(x-sqrt(1-4*x))/(sqrt(1-4*x)*(x^2+4*x-1)).",
				"Recurrence: (n-2)*a(n) = 2*(4*n-9)*a(n-1) - (15*n-38)*a(n-2) - 2*(2*n-5)* a(n-3).",
				"a(n) ~ 4^n/sqrt(Pi*n). (End)",
				"a(n) = Sum_{k=1..n} (F(k)*binomial(2*n-k-1,n-k)), where F(k) = A000045(k). - _Vladimir Kruchinin_, Mar 17 2016",
				"Simpler g.f.: x/sqrt(1-4*x)/(x+sqrt(1-4*x)). - _Don Knuth_, May 11 2016",
				"a(n) = A000045(3*n) - A054441(n). - _Hrishikesh Venkataraman_, May 27 2021",
				"a(n) = 4*a(n-1) + a(n-2) - binomial(2*n-4,n-2) for n\u003e=2. - _Hrishikesh Venkataraman_, Jul 02 2021"
			],
			"maple": [
				"with(combinat); seq( add(binomial(2*n-k-1, n-k)*fibonacci(k), k=0..n), n=0..30); # _G. C. Greubel_, Nov 28 2019",
				"1/(sqrt(1 - 4*x) + 1/x - 4): series(%, x, 27):",
				"seq(coeff(%, x, k), k=0..26); # _Peter Luschny_, May 29 2021"
			],
			"mathematica": [
				"t[n_, k_]:= CoefficientList[ Series[x/(1-x-x^2)/(1-x)^k, {x,0,k}], x][[k+1]]; Array[ t[#, #] \u0026, 20]",
				"Table[Sum[Binomial[2*n-k-1, n-k]*Fibonacci[k], {k,0,n}], {n,0,30}] (* _G. C. Greubel_, Nov 28 2019 *)"
			],
			"program": [
				"(Maxima)",
				"a(n):=sum(fib(k)*binomial(2*n-k-1,n-k),k,1,n); /*  _Vladimir Kruchinin_, Mar 17 2016 */",
				"(PARI) a(n) = sum(k=1, n, fibonacci(k)*binomial(2*n-k-1, n-k)) \\\\ _Michel Marcus_, Mar 17 2016",
				"(MAGMA) [(\u0026+[Binomial(2*n-k-1, n-k)*Fibonacci(k): k in [0..n]]): n in [0..30]]; // _G. C. Greubel_, Nov 28 2019",
				"(Sage) [sum(binomial(2*n-k-1, n-k)*fibonacci(k) for k in (0..n)) for n in (0..30)] # _G. C. Greubel_, Nov 28 2019",
				"(GAP) List([0..30], n-\u003e Sum([0..n], k-\u003e Binomial(2*n-k-1, n-k)*Fibonacci(k) )); # _G. C. Greubel_, Nov 28 2019"
			],
			"xref": [
				"Cf. A000045, A054441."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Paul Curtz_, Apr 08 2010",
			"references": 2,
			"revision": 48,
			"time": "2021-07-24T16:24:35-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}