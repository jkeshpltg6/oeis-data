{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A189640",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 189640,
			"data": "0,0,1,0,0,1,1,0,1,0,0,1,0,0,1,1,0,1,1,0,1,0,0,1,1,0,1,0,0,1,0,0,1,1,0,1,0,0,1,0,0,1,1,0,1,1,0,1,0,0,1,1,0,1,1,0,1,0,0,1,1,0,1,0,0,1,0,0,1,1,0,1,1,0,1,0,0,1,1,0,1,0,0,1,0,0,1,1,0,1,0,0,1,0,0,1,1,0,1,1,0,1,0,0,1,1,0,1,0,0,1,0,0,1,1,0,1,0,0,1,0,0",
			"name": "Fixed point of the morphism 0-\u003e001, 1-\u003e101.",
			"comment": [
				"Are the positions of 0 given by A026138?  Are the positions of 1 given by A026232?",
				"This sequence appears to be: 0, followed by A080846. [R. J. Mathar, May 16 2011]",
				"From _Michel Dekking_, Oct 26 2019: (Start)",
				"Proof of Mathar's third conjecture: Let alpha be the defining morphism for (a(n)): alpha(0)=001, alpha(1)=101. Let beta be the defining morphism for A080846: beta(0)=010, beta(1)=011.",
				"To prove the conjecture, it suffices to prove that",
				"       alpha^n(0) 0 = 0 beta^n(0)   for all n\u003e0.",
				"This holds for n=1. The important property of the morphisms alpha and beta is that",
				"10^{-1}alpha^n(0) = alpha^n(1) and beta^n(0)  = beta^n(1) 1^{-1}0 for all n\u003e0.",
				"Here 0^{-1} and 1^{-1} are the free group inverses of 0 and 1.",
				"We use this in the induction step:",
				"    alpha^{n+1}(0) 0 = alpha^n(00)alpha^n(1) 0 =",
				"      alpha^n(0) alpha^n(0) 10^{-1}alpha^n(0) 0 =",
				"    0 beta^n(0) 0^{-1} 0 beta^n(0) 0^{-1} 10^{-1} 0 beta^n(0) 0^{-1} 0 =",
				"      0 beta^n(0) beta^n(1)1^{-1}0 0^{-1} 1 beta^n(0) =",
				"    0 beta^n(010) =  0 beta^{n+1}(0).",
				"(End)",
				"From _Michel Dekking_, Oct 26 2019: (Start)",
				"Let beta be the defining morphism for this sequence. It is notationally convenient to write the morphism beta on the alphabet {A,B}:",
				"      beta:  A -\u003e AAB, B -\u003e BAB.",
				"Let delta be the 'decoration' morphism defined by delta(A) = 0, delta(B) = 10.",
				"Let x = AABAABBABA... be the fixed point of beta.",
				"CLAIM:  delta(x) = A049320, the non-primitive Chacon sequence.",
				"This is proved in Ferenczi, 1995. Here in an independent, short proof.",
				"Let gamma given by 0-\u003e0010, 1-\u003e1 be the Chacon morphism, and let",
				"c = 0010001010010... be the fixed point of gamma. One verifies that",
				"      delta(AAB) = delta(AABAABBAB) = gamma(0010),",
				"      delta(BAB) = delta(BABAABBAB) = gamma(10010).",
				"So delta(beta(A)) = gamma(delta(A)), and delta(beta(B)) = gamma(delta(B)).",
				"It follows that",
				"      delta(beta^n(A)) = gamma^n(delta(A)) = gamma^n(0) for all n\u003e0.",
				"The left side converges to the delta image of the fixed point x of beta, the right side to c, the non-primitive Chacon sequence.",
				"We have shown that the [10-\u003e1]-transform of A049320 equals (a(n)).",
				"(End)",
				"A generalized choral sequence c(3n+r_0)=0, c(3n+r_1)=1, c(3n+r_c)=c(n), with r_0=1, r_1=2, r_c=0, and c(0)=0. - _Joel Reyes Noche_, Jun 14 2021"
			],
			"reference": [
				"Joel Reyes Noche, Generalized Choral Sequences, Matimyas Matematika, 31(2008), 25-28."
			],
			"link": [
				"S. Ferenczi, \u003ca href=\"https://doi.org/10.24033/bsmf.2260\"\u003eLes transformations de Chacon: combinatoire, structure géométrique, lien avec les systèmes de complexité 2n + 1\u003c/a\u003e, Bull. Soc. Math. France 123 (1995), 271-292.",
				"Joel Reyes Noche, \u003ca href=\"https://www.adnu.edu.ph/urc/download/p051p069.pdf\"\u003eOn generalized choral sequences\u003c/a\u003e, Gibon, IX(2011), 51-69."
			],
			"formula": [
				"a(3k-2)=a(k), a(3k-1)=0, a(3k)=1 for k\u003e=1, a(0)=0."
			],
			"example": [
				"Start: 0",
				"Rules:",
				"  0 --\u003e 001",
				"  1 --\u003e 101",
				"-------------",
				"0:   (#=1)",
				"  0",
				"1:   (#=3)",
				"  001",
				"2:   (#=9)",
				"  001001101",
				"3:   (#=27)",
				"  001001101001001101101001101",
				"4:   (#=81)",
				"  001001101001001101101001101001001101001001101101001101101001101001001101101001101"
			],
			"mathematica": [
				"t = Nest[Flatten[# /. {0-\u003e{0,0,1}, 1-\u003e{1,0,1}}] \u0026, {0}, 5] (*A189640*)",
				"f[n_] := t[[n]]",
				"Flatten[Position[t, 0]] (*A026138*)",
				"Flatten[Position[t, 1]] (*A026232*)",
				"s[n_] := Sum[f[i], {i, 1, n}]; s[0] = 0;",
				"Table[s[n], {n, 1, 120}] (*A189641*)"
			],
			"program": [
				"(PARI) a(n) = my(r); if(n--, until(r, [n,r]=divrem(n,3))); r==2; \\\\ _Kevin Ryde_, Nov 09 2021"
			],
			"xref": [
				"Cf. A080846 (essentially the same), A026138, A026232, A189641.",
				"Cf. A189628 (guide)."
			],
			"keyword": "nonn,easy",
			"offset": "1",
			"author": "_Clark Kimberling_, Apr 24 2011",
			"references": 5,
			"revision": 36,
			"time": "2021-11-09T18:24:00-05:00",
			"created": "2011-04-25T00:59:35-04:00"
		}
	]
}