{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A329454",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 329454,
			"data": "0,1,2,4,5,3,8,6,11,7,10,12,9,19,22,14,15,16,13,18,21,40,43,20,27,46,17,26,33,24,35,38,32,23,29,30,31,28,25,34,36,39,37,64,42,41,67,47,60,49,48,52,45,55,44,58,69,51,50,62,53,77,54,56,83,57,66,74,65,61,102,70,71,79,78,59,68,63,72,95,86,81,76,73,75,82,106",
			"name": "There are exactly three primes among a(n+i) + a(n+j), 0 \u003c= i \u003c j \u003c= 3, for any n \u003e= 0: lexicographically earliest such sequence of distinct nonnegative integers.",
			"comment": [
				"That is, there are exactly three primes (counted with multiplicity) among the 6 pairwise sums of any four consecutive terms.",
				"It remains unknown whether this is a permutation of the nonnegative numbers. There is some hope that this could be the case, and it seems coherent to choose offset 0 not only in view of this. The restriction to positive indices would then be a permutation of the positive integers, but not the smallest one with the given property.",
				"Concerning the existence of the sequence: If the sequence is to be computed in a greedy manner, this means that for given n, we assume given P(n) := {a(n-1), a(n-2), a(n-3)} and thus S(n) := #{ primes x + y with x, y in P(n), x \u003c y} which may equal 0, 1, 2 or 3. We have to find a(n) such that we have exactly 3 - S(n) primes in a(n) + P(n). It is easy to prove that this is always possible when 3 - S(n) is 0 or 1, and similar to the case of A329452 and A329453 when S(n) = 1. When S(n) = 0, however, we must find three primes in the same configuration as the elements of P(n). It is almost surprising that this can always be found up to n = 10^6. However, the sequence does not need to be computable in a greedy manner. That is, if for given P(n) no a(n) would exist such that a(n) + P(n) contains 3 - S(n) primes, this simply means that the considered value of a(n-1) was incorrect, and the next larger choice has to be made. Given this freedom, well-definedness of this sequence up to infinity is far more probable than, for example, the k-tuple conjecture.",
				"Computational results are as follows:",
				"a(10^5) = 99954 and all numbers below 99915 have appeared at that point.",
				"a(10^6) = 1000053 and all numbers below 999845 have appeared at that point."
			],
			"link": [
				"Eric Angelini, \u003ca href=\"http://cinquantesignes.blogspot.com/2019/11/prime-sums-from-neighbouring-terms.html\"\u003ePrime sums from neighbouring terms\u003c/a\u003e, personal blog \"Cinquante signes\" (and post to the SeqFan list), Nov. 11, 2019."
			],
			"example": [
				"We start with a(0) = 0, a(1) = 1, a(2) = 2, the smallest possibilities which do not lead to a contradiction.",
				"Now there are already 2 primes, 0 + 2 and 1 + 2, among the pairwise sums, so the next term must generate exactly one further prime. It appears that a(3) = 4 is the smallest possible choice.",
				"Then there are again two primes among the pairwise sums using {1, 2, 4}, and the next term must again produce one additional prime as sum with these. We find that a(4) = 5 is the smallest possibility."
			],
			"mathematica": [
				"Nest[Block[{k = 3}, While[Nand[FreeQ[#, k], Count[Subsets[Append[Take[#, -3], k], {2}], _?(PrimeQ@ Total@ # \u0026)] == 3], k++]; Append[#, k]] \u0026, {0, 1, 2}, 84] (* _Michael De Vlieger_, Nov 15 2019 *)"
			],
			"program": [
				"(PARI) A329454(n, show=0, o=0, N=3, M=3, p=[], U, u=o)={for(n=o, n-1, show\u0026\u0026 print1(o\", \"); U+=1\u003c\u003c(o-u); U\u003e\u003e=-u+u+=valuation(U+1, 2); p=concat(if(#p\u003e=M, p[^1], p), o); my(c=N-sum(i=2, #p, sum(j=1, i-1, isprime(p[i]+p[j])))); if(#p\u003cM \u0026\u0026 sum(i=1, #p, isprime(p[i]+u))\u003c=c, o=u)|| for(k=u, oo, bittest(U, k-u) || sum(i=1, #p, isprime(p[i]+k))!=c || [o=k, break])); o} \\\\ Optional args: show=1: print a(o..n-1); o=1: start with a(1)=1; N, M: get N primes using M+1 consecutive terms."
			],
			"xref": [
				"Cf. A329452 (2 primes among a(n+i)+a(n+j), 0 \u003c= i \u003c j \u003c 4), A329453 (2 primes among a(n+i)+a(n+j), 0 \u003c= i \u003c j \u003c 5).",
				"Cf. A329333 (1 odd prime among a(n+i)+a(n+j), 0 \u003c= i \u003c j \u003c 3), A329450 (no primes among a(n+i)+a(n+j), 0 \u003c= i \u003c j \u003c 3).",
				"Cf. A329405 ff: variants defined for positive integers."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_M. F. Hasler_, based on an idea from _Eric Angelini_, Nov 15 2019",
			"references": 15,
			"revision": 20,
			"time": "2021-09-03T20:56:29-04:00",
			"created": "2019-11-16T02:32:32-05:00"
		}
	]
}