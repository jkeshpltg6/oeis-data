{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A342020",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 342020,
			"data": "0,2,0,1,2,6,13,32,63,124,244,453,862,1568,2835,5150,9251,16093,27830,48605,84765,145300,245730,417251,721100,1267411,2247106,3997263",
			"name": "Row sums of A342000.",
			"comment": [
				"Question: Will a(n)/(2^n) converge towards zero? After local maximum a(7)/(2^7) = 32/128 = 1/4, the ratio seems to be decreasing, and is already only 0.02978... (\u003c 1/32) at a(27) = 3997263."
			],
			"mathematica": [
				"Block[{a, b, c, f, nn = 12}, b[0] = c[1] = 1; f[n_] := DivisorSigma[1, n]/(n Log[Log[n]]); Do[b[i] = Prime[1 + BitLength[i] - DigitCount[i, 2, 1]]*b[i - 2^Floor@ Log2@ i]; c[i + 1] = Apply[Times, Flatten@ MapIndexed[ConstantArray[Prime[First[#2]], #1] \u0026, Table[LengthWhile[#1, # \u003e= j \u0026], {j, #2}] \u0026 @@ {#, Max[#]} \u0026@ Sort[Flatten[ConstantArray[PrimePi@ #1, #2] \u0026 @@@ FactorInteger[b[i]]], Greater]]]; a[i - 1] = Boole[f[c[i]] \u003e= f[c[Floor[(i + 1)/2]]]], {i, 2^nn}]; Total /@ Table[a[2^n + k], {n, 0, nn - 2}, {k, 0, 2^n - 1}]] (* _Michael De Vlieger_, Mar 07 2021 *)"
			],
			"program": [
				"(PARI)",
				"default(parisizemax,2^31);",
				"default(realprecision, 121);",
				"A283980(n) = {my(f=factor(n)); prod(i=1, #f~, my(p=f[i, 1], e=f[i, 2]); if(p==2, 6, nextprime(p+1))^e)}; \\\\ From A283980",
				"A329886(n) = if(n\u003c2,1+n,if(!(n%2),A283980(A329886(n/2)),2*A329886(n\\2)));",
				"Gi(n) = (log(n)^(n/sigma(n)));",
				"A342000(n) = if(n\u003c=3,!!(n-1),my(p=A329886(n\\2)); if(n%2,Gi(2*p)\u003c=Gi(p),Gi(A283980(p))\u003c=Gi(p))); \\\\ Program less vulnerable to the loss of precision.",
				"s=0; k=0; for(n=2,2^31,if(!bitand(n,n-1), write(\"b342020.txt\", k, \" \", s); print(s, \"/2^\", k, \" = \",(1.0)*(s/(2^k))); k++; s = 0); s += A342000(n));"
			],
			"xref": [
				"Cf. A342000."
			],
			"keyword": "nonn,more",
			"offset": "0,2",
			"author": "_Antti Karttunen_ and _Michael De Vlieger_, Mar 07 2021",
			"references": 1,
			"revision": 14,
			"time": "2021-03-13T18:08:40-05:00",
			"created": "2021-03-13T18:08:40-05:00"
		}
	]
}