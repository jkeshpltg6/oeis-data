{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A257296",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 257296,
			"data": "0,1,2,3,4,5,6,7,8,9,5,10,15,20,25,30,35,40,45,50,10,15,20,25,30,35,40,45,50,55,15,20,25,30,35,40,45,50,55,60,20,25,30,35,40,45,50,55,60,65,25,30,35,40,45,50,55,60,65,70,30,35,40,45,50,55,60,65",
			"name": "Arithmetic mean of the digits of n, multiplied by 10^(d-1) and rounded down, where d is the number of digits of n.",
			"comment": [
				"The reason for the factor 10^(d-1) in the definition is to produce an analog of A257294, i.e., give the first d digits of the mean value, for an \"average\" d-digit number. But since the arithmetic mean of the digits may be between 0 and 1, the situation is slightly different from the case of the geometric mean.",
				"Also motivated by sequence A257829."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A257296/b257296.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = floor(A007953(n)/A055642(n)*10^(A055642(n)-1))"
			],
			"example": [
				"For n = 12, a two-digit number, the average of the digits is 1.50000..., so a(12) = 15."
			],
			"maple": [
				"f:= proc(n) local d;",
				"     d:= ilog10(n);",
				"     floor(convert(convert(n,base,10),`+`)/(d+1)*10^d)",
				"end proc:",
				"map(f, [$0..100]); # _Robert Israel_, May 10 2015"
			],
			"mathematica": [
				"Table[Floor[Mean[IntegerDigits[n]]10^(IntegerLength[n]-1)],{n,0,70}] (* _Harvey P. Dale_, Mar 11 2020 *)"
			],
			"program": [
				"(PARI) a(n)=sum(i=1,#n=digits(n),n[i])*10^(#n-1)\\#n"
			],
			"xref": [
				"Cf. A004426, A004427, A007953, A055642, A257829."
			],
			"keyword": "nonn,base,easy,look",
			"offset": "0,3",
			"author": "_M. F. Hasler_, May 10 2015",
			"references": 1,
			"revision": 21,
			"time": "2020-03-11T16:39:16-04:00",
			"created": "2015-05-11T21:07:24-04:00"
		}
	]
}