{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A073345",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 73345,
			"data": "1,0,0,0,1,0,0,0,0,0,0,0,2,0,0,0,0,1,0,0,0,0,0,0,4,0,0,0,0,0,0,6,0,0,0,0,0,0,0,6,8,0,0,0,0,0,0,0,4,20,0,0,0,0,0,0,0,0,1,40,16,0,0,0,0,0,0,0,0,0,68,56,0,0,0,0,0,0,0,0,0,0,94,152,32,0,0,0,0,0,0,0,0,0,0,114,376,144,0,0,0,0,0,0,0",
			"name": "Table T(n,k) (listed antidiagonalwise in order T(0,0), T(1,0), T(0,1), T(2,0), T(1,1), ...) giving the number of rooted plane binary trees of size n and height k.",
			"reference": [
				"Luo Jian-Jin, Catalan numbers in the history of mathematics in China, in Combinatorics and Graph Theory, (Yap, Ku, Lloyd, Wang, Editors), World Scientific, River Edge, NJ, 1995."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A073345/b073345.txt\"\u003eAntidiagonals n = 0..200, flattened\u003c/a\u003e",
				"H. Bottomley and A. Karttunen, \u003ca href=\"/A073345/a073345.txt\"\u003eNotes concerning diagonals of the square arrays A073345 and A073346.\u003c/a\u003e",
				"Andrew Odlyzko, \u003ca href=\"http://www.dtc.umn.edu/~odlyzko/doc/enumeration.html\"\u003eAnalytic methods in asymptotic enumeration\u003c/a\u003e."
			],
			"formula": [
				"(See the Maple code below. Is there a nicer formula?)",
				"This table was known to the Chinese mathematician Ming An-Tu, who gave the following recurrence in the 1730s. a(0, 0) = 1, a(n, k) = Sum[a(n-1, k-1-i)( 2*Sum[ a(j, i), {j, 0, n-2}]+a(n-1, i) ), {i, 0, k-1}]. - _David Callan_, Aug 17 2004",
				"The generating function for row n, T_n(x):=Sum[T(n, k)x^k, k\u003e=0], is given by T_n = a(n)-a(n-1) where a(n) is defined by the recurrence a(0)=0, a(1)=1, a(n) = 1 + x a(n-1)^2 for n\u003e=2. - _David Callan_, Oct 08 2005"
			],
			"example": [
				"The top-left corner of this square array is",
				"1 0 0 0 0 0 0 0 0 ...",
				"0 1 0 0 0 0 0 0 0 ...",
				"0 0 2 1 0 0 0 0 0 ...",
				"0 0 0 4 6 6 4 1 0 ...",
				"0 0 0 0 8 20 40 68 94 ...",
				"E.g. we have A000108(3) = 5 binary trees built from 3 non-leaf (i.e. branching) nodes:",
				"_______________________________3",
				"___\\/__\\/____\\/__\\/____________2",
				"__\\/____\\/__\\/____\\/____\\/_\\/__1",
				"_\\/____\\/____\\/____\\/____\\./___0",
				"The first four have height 3 and the last one has height 2, thus T(3,3) = 4, T(3,2) = 1 and T(3,any other value of k) = 0."
			],
			"maple": [
				"A073345 := n -\u003e A073345bi(A025581(n), A002262(n));",
				"A073345bi := proc(n,k) option remember; local i,j; if(0 = n) then if(0 = k) then RETURN(1); else RETURN(0); fi; fi; if(0 = k) then RETURN(0); fi; 2 * add(A073345bi(n-i-1,k-1) * add(A073345bi(i,j),j=0..(k-1)),i=0..floor((n-1)/2)) + 2 * add(A073345bi(n-i-1,k-1) * add(A073345bi(i,j),j=0..(k-2)),i=(floor((n-1)/2)+1)..(n-1)) - (`mod`(n,2))*(A073345bi(floor((n-1)/2),k-1)^2); end;",
				"A025581 := n -\u003e binomial(1+floor((1/2)+sqrt(2*(1+n))),2) - (n+1);",
				"A002262 := n -\u003e n - binomial(floor((1/2)+sqrt(2*(1+n))),2);"
			],
			"mathematica": [
				"a[0, 0] = 1; a[n_, k_]/;k\u003cn||k\u003e2^n-1 := 0; a[n_, k_]/;1 \u003c= n \u003c= k \u003c= 2^n-1 := a[n, k] = Sum[a[n-1, k-1-i](2Sum[ a[j, i], {j, 0, n-2}]+a[n-1, i]), {i, 0, k-1}]; Table[a[n, k], {n, 0, 9}, {k, 0, 9}]",
				"(* or *) a[0] = 0; a[1] = 1; a[n_]/;n\u003e=2 := a[n] = Expand[1 + x a[n-1]^2]; gfT[n_] := a[n]-a[n-1]; Map[CoefficientList[ #, x, 8]\u0026, Table[gfT[n], {n, 9}]/.{x^i_/;i\u003e=9 -\u003e0}] (Callan)"
			],
			"xref": [
				"Variant: A073346. Column sums: A000108. Row sums: A001699.",
				"Diagonals: A073345(n, n) = A011782(n), A073345(n+3, n+2) = A014480(n), A073345(n+2, n) = A073773(n), A073345(n+3, n) = A073774(n) - _Henry Bottomley_ and AK, see the attached notes.",
				"A073429 gives the upper triangular region of this array. Cf. also A065329, A001263."
			],
			"keyword": "nonn,tabl",
			"offset": "0,13",
			"author": "_Antti Karttunen_, Jul 31 2002",
			"references": 12,
			"revision": 15,
			"time": "2020-06-29T15:39:13-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}