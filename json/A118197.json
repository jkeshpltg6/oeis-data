{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A118197",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 118197,
			"data": "0,1,0,-1,4,-11,-186,10823,-492536,5125897,10381552650,-6405856963573,3302055158456332,2338316177882689549,-30991279275364493410290,224870687441604081662836751,-1045565401111374322223949545456,-50507259999091315834370754855632879",
			"name": "Column 0 of the matrix log of triangle A117401, after term in row n is multiplied by n: a(n) = n*[log(A117401)](n,0), where A117401(n,k) = 2^(k*(n-k)).",
			"comment": [
				"The entire matrix log of triangle A117401 is determined by column 0 (this sequence): [log(A117401)](n,k) = a(n-k)*2^(k*(n-k))/(n-k) for n\u003ek\u003e=0."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A118197/b118197.txt\"\u003eTable of n, a(n) for n = 0..80\u003c/a\u003e"
			],
			"formula": [
				"G.f.: x/(1-x)^2 = Sum_{n\u003e=0} a(n)*x^n/(1-2^n*x).",
				"By using the inverse transformation: a(n) = Sum_{k=0..n} k*A118196(n-k)*2^(k*(n-k)) for n\u003e=0.",
				"a(2^n) is divisible by 2^n.",
				"G.f.: Sum_{n\u003e=1} a(n)*x^n/[n*2^(n(n-1)/2)] = log(Sum_{n\u003e=0} x^n/2^[n(n-1)/2])."
			],
			"example": [
				"Column 0 of log(A117401) = [0, 1, 0, -1/3, 4/4, -11/5, -186/6, ...] and",
				"consists of terms a(n)/n (n\u003e0); these terms are integers at n = [0, 1, 2, 4, 6, 8, 10, 14, 16, 22, 26, 32, 34, 38, 46, 50, 58, 62, 64, 70, ...].",
				"The g.f. is illustrated by:",
				"x/(1-x)^2 = x + 2*x^2 + 3*x^3 + 4*x^4 + 5*x^5 + 6*x^6 +... = x/(1-2*x) - 0*x^2/(1-4*x) - 1*x^3/(1-8*x) + 4*x^4/(1-16*x) - 11*x^5/(1-32*x) - 186*x^6/(1-64*x) + 10823*x^7/(1-128*x) + ...",
				"Define g.f.: G(x) = Sum_{n\u003e=1} a(n)*x^n/[n * 2^(n(n-1)/2)], then G(x) = x + 0*x^2/4 - x^3/24 + 4*x^4/256 - 11*x^5/5120 - 186*x^6/196608 + ... and exp(G(x)) = 1 + x + x^2/2 + x^3/8 + x^4/64 + x^5/1024 + x^6/32768 + ..."
			],
			"mathematica": [
				"A118196[n_]:= A118196[n]= If[n\u003c2, (-1)^n, -Sum[2^(j*(n-j))*A118196[j], {j, 0, n-1}]];",
				"a[n_]:= a[n]= -Sum[2^(j*(n-j))*j*A118196[j], {j, 0, n}];",
				"Table[a[n], {n, 0, 30}] (* _G. C. Greubel_, Jun 30 2021 *)"
			],
			"program": [
				"(PARI) {a(n) = local(T=matrix(n+1,n+1,r,c,if(r\u003e=c,(2^(c-1))^(r-c))), L=sum(m=1,#T,-(T^0-T)^m/m));return(n*L[n+1,1])};",
				"(PARI) {a(n)=n*2^(n*(n-1)/2)*polcoeff(log(sum(k=0,n,x^k/2^(k*(k-1)/2))+x*O(x^n)),n)}",
				"(Sage)",
				"@CachedFunction",
				"def A118196(n): return (-1)^n if (n\u003c2) else -sum(2^(j*(n-j))*A118196(j) for j in (0..n-1))",
				"def a(n): return (-1)*sum(2^(j*(n-j))*j*A118196(j) for j in (0..n))",
				"[a(n) for n in (0..30)] # _G. C. Greubel_, Jun 30 2021"
			],
			"xref": [
				"Cf. A117401, A118196, A134531."
			],
			"keyword": "sign",
			"offset": "0,5",
			"author": "_Paul D. Hanna_, Apr 15 2006, Oct 30 2007",
			"references": 3,
			"revision": 9,
			"time": "2021-06-30T03:58:19-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}