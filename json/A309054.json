{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A309054",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 309054,
			"data": "1,0,2,1,4,0,1,2,4,1,8,2,1,0,8,1,2,0,1,4,2,1,8,4,1,2,8,1,16,2,1,4,16,1,2,4,1,0,2,1,16,0,1,2,16,1,4,2,1,0,4,1,2,0,1,8,2,1,4,8,1,2,4,1,16,2,1,8,16,1,2,8,1,4,2,1,16,4,1,2,16,1,32,2,1",
			"name": "a(1) = 0; for m \u003e= 0, a(3m) = 1; for m \u003e= 1, a(3m-1) = 2*a(m-1), a(3m+1) = 2*a(m+1).",
			"comment": [
				"The sequence extends to negative n by defining a(n) = a(-n).",
				"Consider a Sierpinski arrowhead curve formed of unit edges numbered consecutively from 0 at its axis of symmetry and aligned with an infinite Sierpinski gasket so that each edge is contained in the boundary of either the plane sector occupied by the gasket or a triangular region of the gasket's complement. If a(n) = 0, the n-th edge is contained in the sector boundary, otherwise the relevant triangular region has side a(n). Every length 3 segment of these boundaries contains exactly one edge of the arrowhead curve. A191108 lists positive n such that edge n is contained in the plane sector boundary. See A307672 for further properties.",
				"See the graphic (in the links) of the arrowhead curve aligned with the gasket. Note the even-indexed edges (colored red) are the edges contained in a triangular region boundary on the left side of the vector. - _Peter Munn_, Jul 29 2019",
				"a(n) = 0 if A307744(n) = 0, otherwise a(n) = 2^(A307744(n) - 1). Thus, this sequence has the symmetries of A307744, which are similar to ruler functions (especially A051064) and described further in A307744.",
				"When listening to this, set pitch modulus to 35 or 36."
			],
			"link": [
				"Peter Munn, \u003ca href=\"/A309054/b309054.txt\"\u003eTable of n, a(n) for n = 0..4374\u003c/a\u003e",
				"Peter Munn, \u003ca href=\"/A309054/a309054.png\"\u003eArrowhead curve aligned with Sierpiński gasket\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Sierpi%C5%84ski_arrowhead_curve\"\u003eSierpiński arrowhead curve\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 0 if A307744(n) = 0, otherwise a(n) = 2^(A307744(n) - 1).",
				"a(A191108(i)) = 0 for i \u003e= 1.",
				"if a(n) = 2^k, a(3^(k+1)+n) = a(3^(k+1)-n) = 2^k.",
				"a((m-1)*3^k + 1) = a((m+1)*3^k - 1) for k \u003e= 1, all integer m.",
				"Upper bound relations: (Start)",
				"for k \u003e= 1, let m_k = A034472(k-1) = 3^(k-1)+1.",
				"a(n) \u003c 2^k, for -m_k \u003c n \u003c m_k.",
				"a(-m_k) = a(m_k) = 2^k.",
				"(End)",
				"Sum_{n=-3^k..3^k-1} (a(n) + 1) = 3 * 4^k.",
				"Sum_{n=-3m..3m-1} (a(n) + 1) = 4 * Sum_{n=-m..m-1} (a(n) + 1) (conjectured)."
			],
			"example": [
				"As 4 is congruent to 1 modulo 3, a(4) = a(3*1 + 1) = 2*a(1+1) = 2*a(2).",
				"As 2 is congruent to -1 modulo 3, a(2) = a(3*1 - 1) = 2*a(1-1) = 2*a(0).",
				"As 0 is congruent to 0 modulo 3, a(0) = 1.  So a(2) = 2*a(0) = 2*1 = 2.  So a(4) = 2*a(2) = 2*2 = 4."
			],
			"xref": [
				"Cf. A034472, A051064, A191108, A307672, A307744."
			],
			"keyword": "nonn,look,hear",
			"offset": "0,3",
			"author": "_Peter Munn_, Jul 09 2019",
			"references": 3,
			"revision": 12,
			"time": "2019-08-13T23:13:38-04:00",
			"created": "2019-07-27T00:41:07-04:00"
		}
	]
}