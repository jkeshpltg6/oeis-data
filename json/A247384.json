{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A247384",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 247384,
			"data": "97,11,3,23,47,167,131,2011,233,23633,34499,1013,9341,90659,521,51749,505049,1391087,2264839,2556713,17123893,2569529,15090641,18246451,6160043,1557431471,43679609,198572029,701575297,5552898499,6639843979,61233611783,9005520203",
			"name": "Find the first (maximal) string of consecutive primes of length exactly n which alternate between 4*k+1 and 4*k+3 or 4*k+3 and 4*k+1 as in A002144(4*n+1) and A002145(4*n+3). The first element is a(n).",
			"link": [
				"Jens Kruse Andersen and Giovanni Resta, \u003ca href=\"/A247384/b247384.txt\"\u003eTable of n, a(n) for n = 1..45\u003c/a\u003e (first 38 terms from Jens Kruse Andersen)",
				"Jens Kruse Andersen, \u003ca href=\"http://primerecords.dk/congruent-primes.htm\"\u003eConsecutive Congruent Primes\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A289118(n) if and only if n \u003e 1 and A289118(n) \u003c A289118(n+1). - _Jonathan Sondow_, Jun 27 2017"
			],
			"example": [
				"a(4)=23 because 23,29,31,37 alternate 4*n+3,4*n+1,4*n+3,4*n+1 for exactly four primes and 23 is the least prime for a string of exactly four."
			],
			"maple": [
				"Primes:= select(isprime,[seq(2*i+1,i=1..10^7)]):",
				"Pm4:= map(`modp`,[seq((-1)^j*Primes[j],j=1..nops(Primes))],4):",
				"Starts:= [1,op(select(t -\u003e Pm4[t-1]\u003c\u003e Pm4[t], [$2..nops(Pm4)]))]:",
				"Lengths:= [seq(Starts[i+1]-Starts[i],i=1..nops(Starts)-1)]:",
				"for i from 1 to max(Lengths) do A[i]:= ListTools:-Search(i,Lengths) od:",
				"R:=[seq(A[i],i=1..max(Lengths))]:",
				"seq(`if`(a=0,0,Primes[Starts[a]]),a=R); # _Robert Israel_, Sep 15 2014"
			],
			"mathematica": [
				"i = 2; While[ Mod[ Prime[i] - Prime[i - 1], 4] != 0 || Mod[ Prime[i + 1] - Prime[i], 4] != 0, i++]; T = {Prime[i]}; Do[j = 2; While[! (Product[ Mod[ Prime[k + 1] - Prime[k], 4], {k, j, j + n}] != 0 \u0026\u0026 (Mod[Prime[j] - Prime[j - 1], 4] == 0 || j == 2) \u0026\u0026 Mod[ Prime[j + n + 2] - Prime[j + n + 1], 4] == 0), j++]; T = Append[T, Prime[j]], {n, 0, 13}]; T (* _Jonathan Sondow_, Jun 28 2017 *)"
			],
			"program": [
				"(PARI) v=vector(100);v[1]=7;cur=1;p=3;forprime(q=5, 1e10, if((q-p)%4==0,if(!v[cur],v[cur]=back(p,cur);print(\"a(\"cur\") = \"v[cur]));cur=1,cur++);p=q) \\\\ _Charles R Greathouse IV_, Sep 15 2014"
			],
			"xref": [
				"Cf. A002144, A002145, A098058, A098059, A289118, A289237."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_J. M. Bergot_, Sep 15 2014",
			"ext": [
				"More terms from _Jens Kruse Andersen_, Oct 01 2014",
				"Definition clarified by _Jonathan Sondow_, Jun 25 2017"
			],
			"references": 3,
			"revision": 48,
			"time": "2017-07-02T13:36:25-04:00",
			"created": "2014-09-30T16:41:18-04:00"
		}
	]
}