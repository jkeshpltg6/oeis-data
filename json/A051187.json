{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A051187",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 51187,
			"data": "1,-8,1,128,-24,1,-3072,704,-48,1,98304,-25600,2240,-80,1,-3932160,1122304,-115200,5440,-120,1,188743680,-57802752,6651904,-376320,11200,-168,1,-10569646080,3425697792,-430309376,27725824,-1003520,20608,-224,1",
			"name": "Generalized Stirling number triangle of the first kind.",
			"comment": [
				"T(n,m)= R_n^m(a=0, b=8) in the notation of the given 1962 reference.",
				"T(n,m) is a Jabotinsky matrix, i.e. the monic row polynomials E(n,x) := Sum_{m=1..n} T(n,m)*x^m = Product_{j=0..n-1} (x - 8*j), n \u003e= 1, and E(0,x) := 1 are exponential convolution polynomials (see A039692 for the definition and a Knuth reference).",
				"From _Petros Hadjicostas_, Jun 07 2020: (Start)",
				"For integers n, m \u003e= 0 and complex numbers a, b (with b \u003c\u003e 0), the numbers R_n^m(a,b) were introduced by Mitrinovic (1961) and further examined by Mitrinovic and Mitrinovic (1962). Such numbers are related to the work of Nörlund (1924).",
				"They are defined via Product_{r=0..n-1} (x - (a + b*r)) = Sum_{m=0..n} R_n^m(a,b)*x^m for n \u003e= 0. As a result, R_n^m(a,b) = R_{n-1}^{m-1}(a,b) - (a + b*(n-1))*R_{n-1}^m(a,b) for n \u003e= m \u003e= 1 with R_1^0(a,b) = a, R_1^1(a,b) = 1, R_n^m(a,b) = 0 for n \u003c m, and R_0^0(a,b) = 1.",
				"With a = 0 and b = 1, we get the Stirling numbers of the first kind S1(n,m) = R_n^m(a=0, b=1) = A048994(n,m).",
				"We have R_n^m(a,b) = Sum_{k=0}^{n-m} (-1)^k * a^k * b^(n-m-k) * binomial(m+k, k) * S1(n, m+k) for n \u003e= m \u003e= 0.",
				"For the current array, T(n,m) = R_n^m(a=0, b=8) but with no zero row or column. (End)"
			],
			"link": [
				"D. S. Mitrinovic, \u003ca href=\"https://gallica.bnf.fr/ark:/12148/bpt6k762d/f996.image.r=1961%20mitrinovic\"\u003eSur une classe de nombres reliés aux nombres de Stirling\u003c/a\u003e, Comptes rendus de l'Académie des sciences de Paris, t. 252 (1961), 2354-2356.",
				"D. S. Mitrinovic and R. S. Mitrinovic, \u003ca href=\"https://gdz.sub.uni-goettingen.de/id/PPN311570321_0010?tify={%22pages%22:[45],%22view%22:%22info%22}\"\u003eSur les polynômes de Stirling\u003c/a\u003e, Bulletin de la Société des mathématiciens et physiciens de la R. P. de Serbie, t. 10 (1958), 43-49.",
				"D. S. Mitrinovic and R. S. Mitrinovic, \u003ca href=\"https://www.jstor.org/stable/43667599\"\u003eSur les nombres de Stirling et les nombres de Bernoulli d'ordre supérieur\u003c/a\u003e, Univ. Beograd. Publ. Elektrotehn. Fak. Ser. Mat. Fiz., No. 43 (1960), 1-63.",
				"D. S. Mitrinovic and R. S. Mitrinovic, \u003ca href=\"https://www.jstor.org/stable/43667490\"\u003eSur une classe de nombres se rattachant aux nombres de Stirling--Appendice: Table des nombres de Stirling\u003c/a\u003e, Univ. Beograd. Publ. Elektrotehn. Fak. Ser. Mat. Fiz., No. 60 (1961), 1-15 and 17-62.",
				"D. S. Mitrinovic and R. S. Mitrinovic, \u003ca href=\"http://pefmath2.etf.rs/files/47/77.pdf\"\u003eTableaux d'une classe de nombres reliés aux nombres de Stirling\u003c/a\u003e, Univ. Beograd. Publ. Elektrotehn. Fak. Ser. Mat. Fiz., No. 77 (1962), 1-77.",
				"D. S. Mitrinovic and R. S. Mitrinovic, \u003ca href=\"https://www.jstor.org/stable/43667130\"\u003eTableaux d'une classe de nombres reliés aux nombres de Stirling\u003c/a\u003e, Univ. Beograd. Publ. Elektrotehn. Fak. Ser. Mat. Fiz., No. 77 (1962), 1-77 [jstor stable version].",
				"Niels Nörlund, \u003ca href=\"https://eudml.org/doc/204170\"\u003eVorlesungen über Differenzenrechnung\u003c/a\u003e, Springer, Berlin, 1924."
			],
			"formula": [
				"T(n, m) = T(n-1, m-1) - 8*(n-1)*T(n-1, m) for n \u003e= m \u003e= 1; T(n, m) := 0 for n \u003c m; T(n, 0) := 0 for n \u003e= 1; T(0, 0) = 1.",
				"E.g.f. for the m-th column of the signed triangle: (log(1 + 8*x)/8)^m/m!.",
				"From _Petros Hadjicostas_, Jun 07 2020: (Start)",
				"T(n,m) = 8^(n-m)*Stirling1(n,m) = 8^(n-m)*A048994(n,m) = 8^(n-m)*A008275(n,m) for n \u003e= m \u003e= 1.",
				"Bivariate e.g.f.-o.g.f.: Sum_{n,m \u003e= 1} T(n,m)*x^n*y^m/n! = exp((y/8)*log(1 + 8*x)) - 1 = (1 + 8*x)^(y/8) - 1. (End)"
			],
			"example": [
				"Triangle T(n,m) (with rows n \u003e= 1 and columns m = 1..n) begins:",
				"          1;",
				"         -8,         1;",
				"        128,       -24,       1;",
				"      -3072,       704,     -48,       1;",
				"      98304,    -25600,    2240,     -80,     1;",
				"   -3932160,   1122304, -115200,    5440,  -120,    1;",
				"  188743680, -57802752, 6651904, -376320, 11200, -168, 1;",
				"  ...",
				"3rd row o.g.f.: E(3,x) = Product_{j=0..2} (x - 8*j) = 128*x - 24*x^2 + x^3."
			],
			"xref": [
				"First (m=1) column sequence is: A051189(n-1).",
				"Row sums (signed triangle): A049210(n-1)*(-1)^(n-1).",
				"Row sums (unsigned triangle): A045755(n).",
				"The b=1..7 triangles are: A008275 (Stirling1 triangle), A039683, A051141, A051142, A051150, A051151, A051186."
			],
			"keyword": "sign,easy,tabl",
			"offset": "1,2",
			"author": "_Wolfdieter Lang_",
			"references": 5,
			"revision": 21,
			"time": "2020-06-08T02:45:14-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}