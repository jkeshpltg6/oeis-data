{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A026800",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 26800,
			"data": "0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,1,1,1,1,1,1,2,2,3,3,4,4,5,6,7,8,10,11,13,15,18,20,24,27,32,36,42,48,56,63,73,83,96,108,125,141,162,183,209,236,270,304,346,390,443,498,565,635,719,807,911,1022,1153,1291,1453,1628,1829,2045",
			"name": "Number of partitions of n in which the least part is 7.",
			"comment": [
				"From _Jason Kimberley_, Feb 03 2011: (Start)",
				"a(n) is also the number of not necessarily connected 2-regular graphs on n-vertices with girth exactly 7 (all such graphs are simple). The integer i corresponds to the i-cycle; the addition of integers corresponds to the disconnected union of cycles.",
				"By removing a single part of size 7, an A026800 partition of n becomes an A185327 partition of n - 7. (End)"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A026800/b026800.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Jason Kimberley, \u003ca href=\"/wiki/User:Jason_Kimberley/E_k-reg_girth_eq_g_index\"\u003eIndex of sequences counting not necessarily connected k-regular simple graphs with girth exactly g\u003c/a\u003e"
			],
			"formula": [
				"G.f.: x^7 * Product_{m\u003e=7} 1/(1-x^m).",
				"a(n) = p(n-7) -p(n-8) -p(n-9) +p(n-12) +2*p(n-14) -p(n-16) -p(n-17) -p(n-18) -p(n-19) +2*p(n-21) +p(n-23) -p(n-26) -p(n-27) +p(n-28) where p(n)=A000041(n) including the implicit p(n)=0 for negative n. - _Shanzhen Gao_, Oct 28 2010; offset corrected / made explicit by _Jason Kimberley_, Feb 03 2011",
				"a(n) ~ exp(Pi*sqrt(2*n/3)) * 5*Pi^6 / (6*sqrt(3)*n^4). - _Vaclav Kotesovec_, Jun 02 2018",
				"G.f.: Sum_{k\u003e=1} x^(7*k) / Product_{j=1..k-1} (1 - x^j). - _Ilya Gutkovskiy_, Nov 25 2020"
			],
			"example": [
				"a(0)=0 because there does not exist a least part of the empty partition.",
				"The  a(7)=1 partition is 7.",
				"The a(14)=1 partition is 7+7.",
				"The a(15)=1 partition is 7+8.",
				".............................",
				"The a(20)=1 partition is 7+13.",
				"The a(21)=2 partitions are 7+7+7 and 7+14."
			],
			"maple": [
				"N:= 100: # for a(0)..a(N)",
				"S:= series(x^7/mul(1-x^i,i=7..N-7),x,N+1):",
				"seq(coeff(S,x,i),i=0..N); # _Robert Israel_, Jul 04 2019"
			],
			"mathematica": [
				"CoefficientList[Series[x^7/QPochhammer[x^7, x], {x, 0, 75}], x] (* _G. C. Greubel_, Nov 03 2019 *)"
			],
			"program": [
				"(MAGMA) p :=  func\u003c n | n lt 0 select 0 else NumberOfPartitions(n) \u003e;",
				"A026800 := func\u003c n | p(n-7)-p(n-8)-p(n-9)+p(n-12)+2*p(n-14)-p(n-16)- p(n-17)-p(n-18)-p(n-19)+2*p(n-21)+p(n-23)-p(n-26)-p(n-27)+p(n-28) \u003e; // _Jason Kimberley_, Feb 03 2011",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Integers(), 75); [0,0,0,0,0,0,0] cat Coefficients(R!( x^7/(\u0026*[1-x^(m+7): m in [0..80]]) )); // _G. C. Greubel_, Nov 03 2019",
				"(PARI) my(x='x+O('x^75)); concat([0,0,0,0,0,0,0], Vec(x^7/prod(m=0,80, 1-x^(m+7)))) \\\\ _G. C. Greubel_, Nov 03 2019",
				"(Sage)",
				"def A026800_list(prec):",
				"    P.\u003cx\u003e = PowerSeriesRing(ZZ, prec)",
				"    return P( x^7/product((1-x^(m+7)) for m in (0..80)) ).list()",
				"A026800_list(75) # _G. C. Greubel_, Nov 03 2019"
			],
			"xref": [
				"Cf. A185327 (Mathematica code)",
				"Not necessarily connected 2-regular graphs with girth at least g [partitions into parts \u003e= g]: A026807 (triangle); chosen g: A000041 (g=1 -- multigraphs with loops allowed), A002865 (g=2 -- multigraphs with loops forbidden), A008483 (g=3), A008484 (g=4), A185325(g=5), A185326 (g=6), A185327 (g=7), A185328 (g=8), A185329 (g=9).",
				"Not necessarily connected 2-regular graphs with girth exactly g [partitions with smallest part g]: A026794 (triangle); chosen g: A002865 (g=2 -- multigraphs with at least one pair of parallel edges, but loops forbidden), A026796 (g=3), A026797 (g=4), A026798 (g=5), A026799 (g=6), this sequence (g=7), A026801 (g=8), A026802 (g=9), A026803 (g=10). - _Jason Kimberley_, Feb 03 2011"
			],
			"keyword": "nonn,easy",
			"offset": "0,22",
			"author": "_Clark Kimberling_",
			"ext": [
				"More terms from Arlin Anderson (starship1(AT)gmail.com), Apr 12 2001"
			],
			"references": 19,
			"revision": 44,
			"time": "2020-11-25T08:29:34-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}