{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A307335",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 307335,
			"data": "4,3,2,1,0,4,4,4,4,4,3,2,1,0,4,4,3,2,1,4,4,0,3,4,4,4,2,4,1,0,4,3,4,2,1,3,1,2,4,4,0,4,4,3,4,4,4,2,0,4,1,0,3,3,4,4,4,2,4,1,3,1,4,2,4,2,0,4,4,4,4,1,3,4,4,4,2,0,0,4,1,4,3,3,0,4,3,4,4,2,4,1,2,4,3,1,1,1,3,4,2,4,2,2,4,4,0,4,4,4,4,0",
			"name": "A fractal quinary sequence: For all n \u003e= 1, underline the term with index n + a(n) + 1; then the two subsequences of underlined terms and of non-underlined terms are both equal to the sequence itself.",
			"comment": [
				"This is defined to be the lexicographically earliest quinary sequence with the following property:",
				"If a(n) = 0, underline a(n+1); if a(n) = 1, underline a(n+2); if a(n) = 2, underline a(n+3); if a(n) = 3, underline a(n+4); if a(n) = 4, underline a(n+5). Now, the subsequence of (once or more) underlined terms must be equal to the original sequence (copy #1), and the subsequence of non-underlined terms must also reproduce the original sequence (copy #2)."
			],
			"link": [
				"Carole Dubois, \u003ca href=\"/A307335/b307335.txt\"\u003eTable of n, a(n) for n = 1..2005\u003c/a\u003e"
			],
			"example": [
				"The sequence starts (4,3,2,1,0,4,4,4,4,4,3,2,1,0,4,4,3,2,1,4,4,...)",
				"Instead of underlining terms, we will put parentheses around the terms we want to emphasize:",
				"a(1) = 4 produces parentheses around a(1 + 5 = 6):",
				"4,3,2,1,0,(4),4,4,4,4,3,2,1,0,4,4,3,2,1,4,4,...",
				"a(2) = 3 produces parentheses around a(2 + 4 = 6), which is now already done. Then,",
				"a(3) = 2 produces parentheses around a(3 + 3 = 6), which is already done. Then,",
				"a(4) = 1 produces parentheses around a(4 + 2 = 6), - already done. Then,",
				"a(5) = 0 produces parentheses around a(5 + 1 = 6) - already done. Then,",
				"a(6) = 4 produces parentheses around a(6 + 5 = 11):",
				"4,3,2,1,0,(4),4,4,4,4,(3),2,1,0,4,4,3,2,1,4,4,...",
				"a(7) = 4 produces parentheses around a(7 + 5 = 12):",
				"4,3,2,1,0,(4),4,4,4,4,(3),(2),1,0,4,4,3,2,1,4,4,...",
				"a(8) = 4 produces parentheses around a(8 + 5 = 13):",
				"4,3,2,1,0,(4),4,4,4,4,(3),(2),(1),0,4,4,3,2,1,4,4,...",
				"a(9) = 4 produces parentheses around a(9 + 5 = 14):",
				"4,3,2,1,0,(4),4,4,4,4,(3),(2),(1),(0),4,4,3,2,1,4,4,...",
				"a(10) = 4 produces parentheses around a(10 + 5 = 15):",
				"4,3,2,1,0,(4),4,4,4,4,(3),(2),(1),(0),(4),4,3,2,1,4,4,...",
				"a(11) = 3 produces parentheses around a(11 + 4 = 15) - already done. Then,",
				"a(12) = 2 produces parentheses around a(12 + 3 = 15) - already done. Then,",
				"a(13) = 1 produces parentheses around a(13 + 2 = 15) - already done. Then,",
				"a(14) = 0 produces parentheses around a(14 + 1 = 15) - already done. Then,",
				"a(15) = 4 produces parentheses around a(15 + 5 = 20):",
				"4,3,2,1,0,(4),4,4,4,4,(3),(2),(1),(0),(4),4,3,2,1,(4),4,...",
				"a(16) = 4 produces parentheses around a(16 + 5 = 21):",
				"4,3,2,1,0,(4),4,4,4,4,(3),(2),(1),(0),(4),4,3,2,1,(4),(4),... Etc.",
				"We see in this small example that the parenthesized terms reproduce the initial sequence:",
				"(4),(3),(2),(1),(0),(4),(4),(4)...",
				"The same is true for the subsequence of non-parenthesized terms:",
				"4, 3, 2, 1, 0, 4, 4, 4, 4, 4, 3, 2, 1,..."
			],
			"xref": [
				"Cf. A307183 (first binary example of such fractal sequences), A307332 (ternary), A307333 (quaternary), A307336 (senary), A307337 (septuary), A307338 (octal), A307339 (nonary), A307340 (decimal)."
			],
			"keyword": "base,nonn",
			"offset": "1,1",
			"author": "_Eric Angelini_ and _Carole Dubois_, Apr 02 2019",
			"references": 9,
			"revision": 12,
			"time": "2019-04-04T22:49:36-04:00",
			"created": "2019-04-04T22:49:36-04:00"
		}
	]
}