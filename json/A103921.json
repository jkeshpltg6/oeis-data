{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A103921",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 103921,
			"data": "0,1,1,1,1,2,1,1,2,1,2,1,1,2,2,2,2,2,1,1,2,2,1,2,3,1,2,2,2,1,1,2,2,2,2,3,2,2,2,3,2,2,2,2,1,1,2,2,2,1,2,3,3,2,2,2,3,2,3,1,2,3,2,2,2,2,1,1,2,2,2,2,2,3,3,2,2,3,1,2,3,3,3,3,2,2,3,2,3,2,2,3,2,2,2,2,1,1,2,2,2,2,1,2,3",
			"name": "Irregular triangle T(n,m) (n \u003e= 0) read by rows: row n lists numbers of distinct parts of partitions of n in Abramowitz-Stegun order.",
			"comment": [
				"T(n, m) is the number of distinct parts of the m-th partition of n in Abramowitz-Stegun order; n \u003e= 0, m = 1..p(n) = A000041(n).",
				"The row length sequence of this table is p(n)=A000041(n) (number of partitions).",
				"In order to count distinct parts of a partition consider the partition as a set instead of a multiset. E.g., n=6: read [1,1,1,3] as {1,3} and count the elements, here 2.",
				"Rows are the same as the rows of A115623, but in reverse order.",
				"From _Wolfdieter Lang_, Mar 17 2011: (Start)",
				"The number of 1s in row number n, n \u003e= 1, is tau(n)=A000005(n), the number of divisors of n.",
				"For the proof read off the divisors d(n,j), j=1..tau(n), from row number n of table A027750, and translate them to the tau(n) partitions d(n,1)^(n/d(n,1)), d(n,2)^(n/d(n,2)),..., d(n,tau(n))^(n/d(n,tau(n))).",
				"See a comment by _Giovanni Resta_ under A000005. (End)",
				"From _Gus Wiseman_, May 20 2020: (Start)",
				"The name is correct if integer partitions are read in reverse, so that the parts are weakly increasing. The non-reversed version is A334440.",
				"Also the number of distinct parts of the n-th integer partition in lexicographic order (A193073).",
				"Differs from the number of distinct parts in the n-th integer partition in (sum/length/revlex) order (A334439). For example, (6,2,2) has two distinct elements, while (1,4,5) has three.",
				"(End)"
			],
			"link": [
				"Robert Price, \u003ca href=\"/A103921/b103921.txt\"\u003eTable of n, a(n) for n = 0..9295\u003c/a\u003e (first 25 rows).",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards Applied Math. Series 55, Tenth Printing, 1972, pp. 831-2.",
				"Wolfdieter Lang, \u003ca href=\"/A103921/a103921.pdf\"\u003eFirst 10 rows.\u003c/a\u003e",
				"OEIS Wiki, \u003ca href=\"http://oeis.org/wiki/Orderings of partitions\"\u003eOrderings of partitions\u003c/a\u003e",
				"Wikiversity, \u003ca href=\"https://en.wikiversity.org/wiki/Lexicographic_and_colexicographic_order\"\u003e Lexicographic and colexicographic order\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A001221(A185974(n)). - _Gus Wiseman_, May 20 2020"
			],
			"example": [
				"Triangle starts:",
				"0,",
				"1,",
				"1, 1,",
				"1, 2, 1,",
				"1, 2, 1, 2, 1,",
				"1, 2, 2, 2, 2, 2, 1,",
				"1, 2, 2, 1, 2, 3, 1, 2, 2, 2, 1,",
				"1, 2, 2, 2, 2, 3, 2, 2, 2, 3, 2, 2, 2, 2, 1,",
				"1, 2, 2, 2, 1, 2, 3, 3, 2, 2, 2, 3, 2, 3, 1, 2, 3, 2, 2, 2, 2, 1,",
				"1, 2, 2, 2, 2, ...",
				"a(5,4)=2 from the fourth partition of 5 in the mentioned order, i.e., (1^2,3), which has two distinct parts, namely 1 and 3."
			],
			"mathematica": [
				"Join@@Table[Length/@Union/@Sort[Reverse/@IntegerPartitions[n]],{n,0,8}] (* _Gus Wiseman_, May 20 2020 *)"
			],
			"xref": [
				"Row sums are A000070.",
				"Row lengths are A000041.",
				"The lengths of these partitions are A036043.",
				"The maxima of these partitions are A049085.",
				"The version for non-reversed partitions is A334440.",
				"The version for colex instead of lex is (also) A334440.",
				"Lexicographically ordered reversed partitions are A026791.",
				"Reversed partitions in Abramowitz-Stegun order are A036036.",
				"Reverse-lexicographically ordered partitions are A080577.",
				"Compositions in Abramowitz-Stegun order are A124734.",
				"Cf. A001221, A036037, A112798, A115621, A115623, A185974, A193073, A228531, A334301, A334302, A334433, A334435, A334441."
			],
			"keyword": "nonn,tabf",
			"offset": "0,6",
			"author": "_Wolfdieter Lang_, Mar 24 2005",
			"ext": [
				"Edited by _Franklin T. Adams-Watters_, May 29 2006"
			],
			"references": 26,
			"revision": 42,
			"time": "2020-06-11T16:48:04-04:00",
			"created": "2005-04-09T03:00:00-04:00"
		}
	]
}