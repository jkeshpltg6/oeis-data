{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A334144",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 334144,
			"data": "1,1,1,1,1,2,2,1,2,2,2,2,2,2,3,1,1,2,2,2,3,2,2,2,2,2,2,3,3,3,3,1,4,2,4,3,3,3,3,2,2,4,4,3,4,3,3,2,4,2,3,3,3,3,3,3,3,3,3,4,4,4,4,1,5,5,5,2,5,5,5,3,3,3,4,3,6,4,4,2,3,2,2,4,3,4,4,3,3,5,5,3,5,3,5,2,2,4,6,3,3,3,3,3,6,3",
			"name": "Consider the mapping k -\u003e (k - (k/p)), where prime p | k. a(n) = maximum distinct terms at any position j among the various paths to 1.",
			"comment": [
				"Let i = A064097(n) be the common path length and let 1 \u003c= j \u003c= i. Given a path P, we find for any j relatively few distinct values. Regarding a common path length i, see A333123 comment 2, and proof at A064097.",
				"Maximum term in row n of A334184."
			],
			"link": [
				"Peter Kagey, \u003ca href=\"/A334144/b334144.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Peter Kagey, Math.StackExchange, \u003ca href=\"https://math.stackexchange.com/questions/3632156/does-a-graded-poset-on-mathbbn-0-generated-from-subtracting-factors-defi\"\u003eDoes a graded poset on the positive integers generated from subtracting factors define a lattice?\u003c/a\u003e",
				"Richard Stanley, \u003ca href=\"https://ocw.mit.edu/courses/mathematics/18-318-topics-in-algebraic-combinatorics-spring-2006/lecture-notes/sperner.pdf\"\u003eMIT OpenCourseWare, Lecture Notes in Algebraic Combinatorics: The Sperner Property\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Sperner_property_of_a_partially_ordered_set\"\u003eSperner property of a partially ordered set\u003c/a\u003e"
			],
			"example": [
				"For n=15, the paths are shown vertically at left, and the graph obtained appears at right:",
				"  15   15   15   15   15  =\u003e         15",
				"   |    |    |    |    |            _/ \\_",
				"   |    |    |    |    |           /     \\",
				"  10   10   12   12   12  =\u003e     10       12",
				"   |    |    |    |    |         | \\_   _/ |",
				"   |    |    |    |    |         |   \\ /   |",
				"   5    8    6    6    8  =\u003e     5    8    6",
				"   |    |    |    |    |          \\_  |  _/|",
				"   |    |    |    |    |            \\_|_/  |",
				"   4    4    3    4    4  =\u003e          4    3",
				"   |    |    |    |    |              |  _/",
				"   |    |    |    |    |              |_/",
				"   2    2    2    2    2  =\u003e          2",
				"   |    |    |    |    |              |",
				"   |    |    |    |    |              |",
				"   1    1    1    1    1  =\u003e          1",
				"Because the maximum number of distinct terms in any row is 3, a(15) = 3."
			],
			"mathematica": [
				"Max[Length@ Union@ # \u0026 /@ Transpose@ #] \u0026 /@ Nest[Function[{a, n}, Append[a, Join @@ Table[Flatten@ Prepend[#, n] \u0026 /@ a[[n - n/p]], {p, FactorInteger[n][[All, 1]]}]]] @@ {#, Length@ # + 1} \u0026, {{{1}}}, 105]",
				"(* Second program: *)",
				"g[n_] := Block[{lst = {{n}}}, While[lst[[-1]] != {1}, lst = Join[lst, {Union@ Flatten[# - #/(First@ # \u0026 /@ FactorInteger@ #) \u0026 /@ lst[[-1]] ]}]]; Max[Length /@ lst]]; Array[g, 105] (* _Robert G. Wilson v_, May 08 2020 *)"
			],
			"xref": [
				"Cf. A064097, A332992, A332999, A333123, A334111, A334184.",
				"Cf. also A096825."
			],
			"keyword": "nonn",
			"offset": "1,6",
			"author": "_Michael De Vlieger_, _Peter Kagey_, _Antti Karttunen_, Apr 15 2020",
			"references": 7,
			"revision": 23,
			"time": "2020-05-08T23:21:01-04:00",
			"created": "2020-04-28T00:43:29-04:00"
		}
	]
}