{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097196",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97196,
			"data": "1,0,1,2,2,2,4,4,6,8,9,12,16,18,22,28,33,40,50,58,70,84,98,116,138,160,188,222,256,298,348,400,463,536,614,706,812,926,1060,1212,1378,1568,1785,2022,2292,2598,2932,3312,3740,4208,4736,5328,5978,6708,7522,8416,9416",
			"name": "Expansion of psi(x^3)^2 / f(-x^2) in powers of x where psi(), f() are Ramanujan theta functions.",
			"comment": [
				"On page 63 of Watson 1936 is an equation with left side 2*rho(q) + omega(q) and the right side is 3 times the g.f. of this sequence. - _Michael Somos_, Jul 14 2015",
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"reference": [
				"N. J. Fine, Basic Hypergeometric Series and Applications, Amer. Math. Soc., 1988; p. 50, Eq. (25.4).",
				"George N. Watson, The final problem: an account of the mock theta functions, J. London Math. Soc., 11 (1936) 55-80."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A097196/b097196.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Vaclav Kotesovec, \u003ca href=\"http://arxiv.org/abs/1509.08708\"\u003eA method of finding the asymptotics of q-series based on the convolution of generating functions\u003c/a\u003e, arXiv:1509.08708 [math.CO], 2015-2016.",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"George N. Watson, \u003ca href=\"http://jlms.oxfordjournals.org/content/s1-11/1/55.extract\"\u003eThe final problem: an account of the mock theta functions\u003c/a\u003e, J. London Math. Soc., 11 (1936) 55-80.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-2/3) * eta(x^6)^4 / (eta(x^2) * eta(x^3)^2) in powers of q. - _Michael Somos_, Jul 14 2015",
				"G.f.: Product_{n \u003e= 1} (1+q^(3*n))^4*(1-q^(3*n))^2/(1-q^(2*n)).",
				"3 * a(n) = A053253(n) + 2 * A053255(n). - _Michael Somos_, Jul 29 2015",
				"a(n) ~ exp(Pi*sqrt(n/3)) / (12*sqrt(n)). - _Vaclav Kotesovec_, Oct 14 2015"
			],
			"example": [
				"G.f. = 1 + x^2 + 2*x^3 + 2*x^4 + 2*x^5 + 4*x^6 + 4*x^7 + 6*x^8 + 8*x^9 + ...",
				"G.f. = q^2 + q^8 + 2*q^11 + 2*q^14 + 2*q^17 + 4*q^20 + 4*q^23 + 6*q^26 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 2, 0, x^(3/2)]^2 / (4 x^(3/4) QPochhammer[ x^2]), {x, 0, n}]; (* _Michael Somos_, Jul 14 2015 *)",
				"nmax=60; CoefficientList[Series[Product[(1+x^(3*k))^4 * (1-x^(3*k))^2 / (1-x^(2*k)),{k,1,nmax}],{x,0,nmax}],x] (* _Vaclav Kotesovec_, Oct 14 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^6 + A)^4 / (eta(x^2 + A) * eta(x^3 + A)^2), n))}; /* _Michael Somos_, Jul 14 2015 */"
			],
			"xref": [
				"Cf. A053253, A053255."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_, Sep 17 2004",
			"references": 3,
			"revision": 17,
			"time": "2021-03-12T22:24:43-05:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}