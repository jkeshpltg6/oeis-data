{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A155998",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 155998,
			"data": "0,1,1,0,4,0,1,3,3,1,0,8,0,8,0,1,5,10,10,5,1,0,12,0,40,0,12,0,1,7,21,35,35,21,7,1,0,16,0,112,0,112,0,16,0,1,9,36,84,126,126,84,36,9,1,0,20,0,240,0,504,0,240,0,20,0",
			"name": "Triangle read by rows: T(n, k) = f(n, k) + f(n, n-k), where f(n, k) = binomial(n, k)*(1 - (-1)^k)/2.",
			"comment": [
				"Row sums are: A155559(n) = {0, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, ...}."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A155998/b155998.txt\"\u003eRows n = 0..100 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = f(n, k) + f(n, n-k), where f(n, k) = binomial(n, k)*(1 - (-1)^k)/2.",
				"From _G. C. Greubel_, Dec 01 2019: (Start)",
				"T(n, k) = binomial(n, k)*(2 - (-1)^k*(1 + (-1)^n))/2.",
				"Sum_{k=0..n} T(n,k) = 2^n = A155559(n) for n \u003e= 1.",
				"Sum_{k=0..n-1} T(n,k) = (2^(n+1) - (1-(-1)^n))/2 = A051049(n), n \u003e= 1. (End)"
			],
			"example": [
				"Triangle begins as:",
				"  0;",
				"  1,  1;",
				"  0,  4,  0;",
				"  1,  3,  3,   1;",
				"  0,  8,  0,   8,   0;",
				"  1,  5, 10,  10,   5,   1;",
				"  0, 12,  0,  40,   0,  12,  0;",
				"  1,  7, 21,  35,  35,  21,  7,   1;",
				"  0, 16,  0, 112,   0, 112,  0,  16, 0;",
				"  1,  9, 36,  84, 126, 126, 84,  36, 9,  1;",
				"  0, 20,  0, 240,   0, 504,  0, 240, 0, 20, 0;"
			],
			"maple": [
				"seq(seq( binomial(n, k)*(2 - (-1)^k*(1+(-1)^n))/2, k=0..n), n=0..12); # _G. C. Greubel_, Dec 01 2019"
			],
			"mathematica": [
				"f[n_, k_]:= Binomial[n, k]*(1 - (-1)^k)/2; Table[f[n,k]+f[n,n-k], {n, 0, 10}, {k, 0, n}]//Flatten",
				"Table[Binomial[n, k]*(2-(-1)^k*(1+(-1)^n))/2, {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, Dec 01 2019 *)"
			],
			"program": [
				"(PARI) T(n,k) = binomial(n, k)*(2 - (-1)^k*(1+(-1)^n))/2; \\\\ _G. C. Greubel_, Dec 01 2019",
				"(MAGMA) [Binomial(n, k)*(2 - (-1)^k*(1+(-1)^n))/2: k in [0..n], n in [0..12]]; // _G. C. Greubel_, Dec 01 2019",
				"(Sage) [[binomial(n, k)*(2 - (-1)^k*(1+(-1)^n))/2 for k in (0..n)] for n in (0..12)] # _G. C. Greubel_, Dec 01 2019",
				"(GAP) Flat(List([0..12], n-\u003e List([0..n], k-\u003e Binomial(n, k)*(2 - (-1)^k*(1 + (-1)^n))/2 ))); # _G. C. Greubel_, Dec 01 2019"
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Roger L. Bagula_, Feb 01 2009",
			"references": 1,
			"revision": 5,
			"time": "2019-12-01T23:16:34-05:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}