{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A320162",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 320162,
			"data": "0,0,4,5,0,7,18,0,12,20,24,25,29,37,0,5,18,19,24,43,78,97,102,103,116,0,2,14,45,70,82,87,99,124,155,167,0,24,38,40,63,83,103,105,184,186,206,226,249,251,265,0,31,37,63,79,100,137,144,150,180,181,211,217,224,261,282,298,324,330",
			"name": "Irregular triangle read by rows: row n lists 0 \u003c= k \u003c p^2 such that p^2 divides A172236(k, p-Kronecker(k^2+4, p)), p = prime(n).",
			"comment": [
				"p always divides A172236(k, p-Kronecker(k^2+4, p)), so it's interesting to see when p^2 also divides A172236(k, p-Kronecker(k^2+4, p)). If this holds, then p is called a k-Wall-Sun-Sun prime (and thus being a (k + s*p^2)-Wall-Sun-Sun prime for all integer s). Specially, there is no Wall-Sun-Sun prime below 10^14 implies that there is no 1 in the first pi(10^14) rows.",
				"Note that A172236(0, m) is not defined, so here k must be understood as a remainder modulo p^2. because A172236(k+s*p^2, m) == A172236(k, m) (mod p^2).",
				"Let p = prime(n). Every row contains 0. For n \u003e= 2, if p == 3 (mod 4), then the n-th row contains p numbers, whose remainders modulo p form a permutation of {0, 1, 2, 3, ..., p - 2, p - 1}. If p == 1 (mod 4), then the n-th row contains p - 2 numbers, whose remainders modulo p form a permutation of {0, 1, 2, 3, ..., p - 2, p - 1} \\ {+-2*((p - 1)/2)! mod p}.",
				"Every row is antisymmetric, that is, k is a member iff p^2 - k is, k \u003e 0. As a result, the sum of the n-th row is prime(n)^2*(prime(n) - 1)/2 if prime(n) == 3 (mod 4) and prime(n)^2*(prime(n) - 3)/2 if prime(n) == 1 (mod 4).",
				"Equivalently, if p = prime(n) == 1 (mod 4), then row n lists 0 \u003c= k \u003c p^2 such that p^2 divides A172236(k, (p-Kronecker(k^2+4, p))/2). - _Jianing Song_, Jul 06 2019"
			],
			"link": [
				"Jianing Song, \u003ca href=\"/A320162/b320162.txt\"\u003eTable of n, a(n) for n = 1..29193\u003c/a\u003e (primes below 600)",
				"Jianing Song, \u003ca href=\"/A320162/a320162.txt\"\u003eTable of n, a(n) for n = 1..75966\u003c/a\u003e (primes below 1000)",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Wall-Sun-Sun_prime\"\u003eWall-Sun-Sun prime\u003c/a\u003e"
			],
			"example": [
				"Table starts",
				"p = 2: 0,",
				"p = 3: 0, 4, 5,",
				"p = 5: 0, 7, 18,",
				"p = 7: 0, 12, 20, 24, 25, 29, 37,",
				"p = 11: 0, 5, 18, 19, 24, 43, 78, 97, 102, 103, 116,",
				"p = 13: 0, 2, 14, 45, 70, 82, 87, 99, 124, 155, 167,",
				"p = 17: 0, 24, 38, 40, 63, 83, 103, 105, 184, 186, 206, 226, 249, 251, 265,",
				"p = 19: 0, 31, 37, 63, 79, 100, 137, 144, 150, 180, 181, 211, 217, 224, 261, 282, 298, 324, 330,",
				"p = 23: 0, 21, 30, 38, 40, 70, 79, 89, 111, 149, 198, 248, 281, 331, 380, 418, 440, 450, 459, 489, 491, 499, 508,",
				"p = 29: 0, 15, 40, 41, 49, 51, 56, 64, 74, 84, 126, 182, 204, 381, 460, 637, 659, 715, 757, 767, 777, 785, 790, 792, 800, 801, 826,",
				"..."
			],
			"program": [
				"(PARI)",
				"B(k, p) = (([k, 1; 1, 0]^(p-kronecker(k^2+4, p)))[1, 2])%(p^2)",
				"forprime(p=2, 50, for(k=0, p^2-1, if(!B(k, p), print1(k, \", \"))); print)"
			],
			"xref": [
				"Cf. A143548, A172236, A320161 (discriminant k^2-4).",
				"Cf. A238736 (primes p such that 2 occurs in the corresponding row)."
			],
			"keyword": "nonn,tabf",
			"offset": "1,3",
			"author": "_Jianing Song_, Oct 06 2018",
			"references": 2,
			"revision": 29,
			"time": "2019-07-06T10:41:24-04:00",
			"created": "2018-10-15T22:19:14-04:00"
		}
	]
}