{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A051254",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 51254,
			"data": "2,11,1361,2521008887,16022236204009818131831320183,4113101149215104800030529537915953170486139623539759933135949994882770404074832568499",
			"name": "Mills primes.",
			"comment": [
				"Mills showed that there is a number A \u003e 1 but not an integer, such that floor( A^(3^n) ) is a prime for all n = 1, 2, 3, ... A is approximately 1.306377883863... (see A051021).",
				"a(1) = 2 and (for n \u003e 1) a(n) is greatest prime \u003c a(n-1)^3. - _Jonathan Vos Post_, May 05 2006",
				"Named after the American mathematician William Harold Mills (1921-2007). - _Amiram Eldar_, Jun 23 2021"
			],
			"reference": [
				"Tom M. Apostol, Introduction to Analytic Number Theory, Springer-Verlag, 1976, page 8."
			],
			"link": [
				"Robert G. Wilson v, \u003ca href=\"/A051254/b051254.txt\"\u003eTable of n, a(n) for n = 1..8\u003c/a\u003e",
				"Chris K. Caldwell, \u003ca href=\"http://www.utm.edu/research/primes/notes/proofs/A3n.html\"\u003eMills' Theorem - a generalization\u003c/a\u003e.",
				"Chris K. Caldwell and Yuanyou Chen, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL8/Caldwell/caldwell78.html\"\u003eDetermining Mills' Constant and a Note on Honaker's Problem\u003c/a\u003e, Journal of Integer Sequences, Vol. 8 (2005), Article 05.4.1.",
				"Steven R. Finch, \u003ca href=\"http://www.people.fas.harvard.edu/~sfinch/constant/mills/mills.html\"\u003eMills' Constant\u003c/a\u003e. [Broken link]",
				"Steven R. Finch, \u003ca href=\"http://web.archive.org/web/20010603070928/http://www.mathsoft.com/asolve/constant/mills/mills.html\"\u003eMills' Constant\u003c/a\u003e. [From the Wayback machine]",
				"Dylan Fridman, Juli Garbulsky, Bruno Glecer, James Grime and Massi Tron Florentin, \u003ca href=\"https://doi.org/10.1080/00029890.2019.1530554\"\u003eA Prime-Representing Constant\u003c/a\u003e, Amer. Math. Monthly, Vol. 126, No. 1 (2019), pp. 72-73; \u003ca href=\"https://www.researchgate.net/publication/330746181_A_Prime-Representing_Constant\"\u003eResearchGate link\u003c/a\u003e, \u003ca href=\"https://arxiv.org/abs/2010.15882\"\u003earXiv preprint\u003c/a\u003e, arXiv:2010.15882 [math.NT], 2020.",
				"James Grime and Brady Haran, \u003ca href=\"https://www.youtube.com/watch?v=6ltrPVPEwfo\"\u003eAwesome Prime Number Constant\u003c/a\u003e, Numberphile video, 2013.",
				"Brian Hayes, \u003ca href=\"http://bit-player.org/2015/pumping-the-primes\"\u003ePumping the Primes\u003c/a\u003e, bit-player, Aug 19 2015.",
				"William H. Mills, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9904-1947-08849-2\"\u003eA prime-representing function\u003c/a\u003e, Bull. Amer. Math. Soc., Vol. 53, No. 6 (1947), p. 604; \u003ca href=\"https://doi.org/10.1090/S0002-9904-1947-08944-8\"\u003eErrata\u003c/a\u003e, ibid., Vol. 53, No 12 (1947), p. 1196.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/2002.12137\"\u003eThe calculation of p(n) and pi(n)\u003c/a\u003e, arXiv:2002.12137 [math.NT], 2020.",
				"László Tóth, \u003ca href=\"https://arxiv.org/abs/1801.08014\"\u003eA Variation on Mills-Like Prime-Representing Functions\u003c/a\u003e, arXiv:1801.08014 [math.NT], 2018.",
				"Juan L. Varona, \u003ca href=\"https://arxiv.org/abs/2012.11750\"\u003eA Couple of Transcendental Prime-Representing Constants\u003c/a\u003e, arXiv:2012.11750 [math.NT], 2020.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/MillsPrime.html\"\u003eMills' Prime\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PrimeFormulas.html\"\u003ePrime Formulas\u003c/a\u003e.",
				"Eric W. Weisstein, \u003ca href=\"/A051254/a051254.txt\"\u003eTable of n, a(n) for n = 1..13\u003c/a\u003e."
			],
			"formula": [
				"a(1) = 2; a(n) is least prime \u003e a(n-1)^3. - _Jonathan Vos Post_, May 05 2006"
			],
			"example": [
				"a(3) = 1361 = 11^3 + 30 = a(2)^3 + 30 and there is no smaller k such that a(2)^3 + k is prime. - _Jonathan Vos Post_, May 05 2006"
			],
			"maple": [
				"floor(A^(3^n), n=1..10); # A is Mills's constant: 1.306377883863080690468614492602605712916784585156713644368053759966434.. (A051021)."
			],
			"mathematica": [
				"p = 1; Table[p = NextPrime[p^3], {6}] (* _T. D. Noe_, Sep 24 2008 *)",
				"NestList[NextPrime[#^3] \u0026, 2, 5] (* _Harvey P. Dale_, Feb 28 2012 *)"
			],
			"program": [
				"(PARI) a(n)=if(n==1, 2, nextprime(a(n-1)^3)) \\\\ _Charles R Greathouse IV_, Jun 23 2017"
			],
			"xref": [
				"Cf. A001358, A055496, A076656, A006992, A005384, A005385, A118908, A118909, A118910, A118911, A118912, A118913.",
				"Cf. A224845 (integer lengths of Mills primes).",
				"Cf. A108739 (sequence of offsets b_n associated with Mills primes).",
				"Cf. A051021 (decimal expansion of Mills constant)."
			],
			"keyword": "nonn,nice",
			"offset": "1,1",
			"author": "_Simon Plouffe_.",
			"ext": [
				"Edited by _N. J. A. Sloane_, May 05 2007"
			],
			"references": 16,
			"revision": 85,
			"time": "2021-06-23T05:04:13-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}