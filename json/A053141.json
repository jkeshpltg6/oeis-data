{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A053141",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 53141,
			"data": "0,2,14,84,492,2870,16730,97512,568344,3312554,19306982,112529340,655869060,3822685022,22280241074,129858761424,756872327472,4411375203410,25711378892990,149856898154532,873430010034204",
			"name": "a(0)=0, a(1)=2 then a(n) = a(n-2) + 2*sqrt(8*a(n-1)^2 + 8*a(n-1) + 1).",
			"comment": [
				"Solution to b(b+1) = 2a(a+1) in natural numbers including 0; a = a(n), b = b(n) = A001652(n).",
				"The solution of a special case of a binomial problem of H. Finner and K. Strassburger (strass(AT)godot.dfi.uni-duesseldorf.de).",
				"Also the indices of triangular numbers that are half other triangular numbers [a of T(a) such that 2T(a)=T(b)]. The T(a)'s are in A075528, the T(b)'s are in A029549 and the b's are in A001652. - Bruce Corrigan (scentman(AT)myfamily.com), Oct 30 2002",
				"Sequences A053141 (this entry), A016278, A077259, A077288 and A077398 are part of an infinite series of sequences. Each depends upon the polynomial p(n) = 4k*n^2 + 4k*n + 1, when 4k is not a perfect square. Equivalently, they each depend on the equation k*t(x)=t(z) where t(n) is the triangular number formula n(n+1)/2. The dependencies are these: they are the sequences of positive integers n such that p(n) is a perfect square and there exists a positive integer m such that k*t(n)=t(m). A053141 is for k=2, A016278 is for k=3, A077259 is for k=5. - Robert Phillips (bobanne(AT)bellsouth.net), Oct 11 2007, Nov 27 2007",
				"Jason Holt observes that a pair drawn from a drawer with A053141(n)+1 red socks and A001652(n) - A053141(n) blue socks will as likely as not be matching reds: (A053141+1)*A053141/((A001652+1)*A001652 = 1/2, n\u003e0. - _Bill Gosper_, Feb 07 2010",
				"The values x(n)=A001652(n), y(n)=A046090(n) and z(n)=A001653(n) form a nearly isosceles Pythagorean triple since y(n)=x(n)+1 and x(n)^2 + y(n)^2 = z(n)^2; e.g., for n=2, 20^2 + 21^2 = 29^2. In a similar fashion, if we define b(n)=A011900(n) and c(n)=A001652(n), a(n), b(n) and c(n) form a nearly isosceles anti-Pythagorean triple since b(n)=a(n)+1 and a(n)^2 + b(n)^2 = c(n)^2 + c(n) + 1; i.e., the value a(n)^2 + b(n)^2 lies almost exactly between two perfect squares; e.g., 2^2 + 3^2 = 13 = 4^2 - 3 = 3^2 + 4; 14^2 + 15^2 = 421 = 21^2 - 20 = 20^2 + 21. - _Charlie Marion_, Jun 12 2009",
				"Behera \u0026 Panda call these the balancers and A001109 are the balancing numbers. - _Michel Marcus_, Nov 07 2017"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A053141/b053141.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Jeremiah Bartz, Bruce Dearden, and Joel Iiams, \u003ca href=\"https://arxiv.org/abs/1810.07895\"\u003eClasses of Gap Balancing Numbers\u003c/a\u003e, arXiv:1810.07895 [math.NT], 2018.",
				"Jeremiah Bartz, Bruce Dearden, and Joel Iiams, \u003ca href=\"https://ajc.maths.uq.edu.au/pdf/77/ajc_v77_p318.pdf\"\u003eCounting families of generalized balancing numbers\u003c/a\u003e, The Australasian Journal of Combinatorics (2020) Vol. 77, Part 3, 318-325.",
				"A. Behera and G. K. Panda, \u003ca href=\"http://www.fq.math.ca/Scanned/37-2/behera.pdf\"\u003eOn the Square Roots of Triangular Numbers\u003c/a\u003e, Fib. Quart., 37 (1999), pp. 98-105.",
				"Martin V. Bonsangue, Gerald E. Gannon and Laura J. Pheifer, \u003ca href=\"https://www.jstor.org/stable/20871097\"\u003eMisinterpretations can sometimes be a good thing\u003c/a\u003e, Math. Teacher, vol. 95, No. 6 (2002) pp. 446-449.",
				"P. Catarino, H. Campos, and P. Vasco, \u003ca href=\"http://ami.ektf.hu/uploads/papers/finalpdf/AMI_45_from11to24.pdf\"\u003eOn some identities for balancing and cobalancing numbers\u003c/a\u003e, Annales Mathematicae et Informaticae, 45 (2015) pp. 11-24.",
				"Refik Keskin and Olcay Karaatli, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL15/Karaatli/karaatli5.html\"\u003eSome New Properties of Balancing Numbers and Square Triangular Numbers\u003c/a\u003e, Journal of Integer Sequences, Vol. 15 (2012), Article #12.1.4.",
				"J. S. Myers, R. Schroeppel, S. R. Shannon, N. J. A. Sloane, and P. Zimmermann, \u003ca href=\"http://arxiv.org/abs/2004.14000\"\u003eThree Cousins of Recaman's Sequence\u003c/a\u003e, arXiv:2004:14000 [math.NT], April 2020.",
				"G. K. Panda, \u003ca href=\"https://www.fq.math.ca/Papers1/45-3/panda.pdf\"\u003eSequence balancing and cobalancing numbers\u003c/a\u003e, Fib. Q., Vol. 45, No. 3 (2007), 265-271. See p. 266.",
				"Robert Phillips, \u003ca href=\"https://web.archive.org/web/20100713033314/http://www.usca.edu/math/~mathdept/bobp/pdf/polgonal.pdf\"\u003ePolynomials of the form 1+4ke+4ke^2\u003c/a\u003e, 2008.",
				"Robert Phillips, \u003ca href=\"https://web.archive.org/web/20100713033404/http://www.usca.edu/math/~mathdept/bobp/pdf/result.pdf\"\u003eA triangular number result\u003c/a\u003e, 2009.",
				"Vladimir Pletser, \u003ca href=\"https://arxiv.org/abs/2101.00998\"\u003eRecurrent Relations for Multiple of Triangular Numbers being Triangular Numbers\u003c/a\u003e, arXiv:2101.00998 [math.NT], 2021.",
				"Vladimir Pletser, \u003ca href=\"https://arxiv.org/abs/2102.12392\"\u003eClosed Form Equations for Triangular Numbers Multiple of Other Triangular Numbers\u003c/a\u003e, arXiv:2102.12392 [math.GM], 2021.",
				"Vladimir Pletser, \u003ca href=\"https://arxiv.org/abs/2102.13494\"\u003eTriangular Numbers Multiple of Triangular Numbers and Solutions of Pell Equations\u003c/a\u003e, arXiv:2102.13494 [math.NT], 2021.",
				"Vladimir Pletser, \u003ca href=\"https://arxiv.org/abs/2103.03019\"\u003eCongruence Properties of Indices of Triangular Numbers Multiple of Other Triangular Numbers\u003c/a\u003e, arXiv:2103.03019 [math.GM], 2021.",
				"Vladimir Pletser, \u003ca href=\"https://doi.org/10.13140/RG.2.2.35428.91527\"\u003eSearching for multiple of triangular numbers being triangular numbers\u003c/a\u003e, 2021.",
				"Burkard Polster, \u003ca href=\"http://youtu.be/rjHFkx6eTL8\"\u003eNice merging together\u003c/a\u003e, Mathologer video (2015).",
				"B. Polster and M. Ross, \u003ca href=\"https://arxiv.org/abs/1503.04658\"\u003eMarching in squares\u003c/a\u003e, arXiv preprint arXiv:1503.04658 [math.HO], 2015.",
				"A. Tekcan, M. Tayat, and M. E. Ozbek, \u003ca href=\"https://doi.org/10.1155/2014/897834\"\u003eThe diophantine equation 8x^2-y^2+8x(1+t)+(2t+1)^2=0 and t-balancing numbers\u003c/a\u003e, ISRN Combinatorics, Volume 2014, Article ID 897834, 5 pages.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (7,-7,1)."
			],
			"formula": [
				"a(n) = (A001653(n)-1)/2 = 2*A053142(n) = A011900(n-1).",
				"a(n) = 6*a(n-1) - a(n-2) + 2, a(0) = 0, a(1) = 2.",
				"G.f.: 2*x/((1-x)*(1-6*x+x^2)).",
				"Let c(n) = A001109(n). Then a(n+1) = a(n)+2*c(n+1), a(0)=0. This gives a generating function (same as existing g.f.) leading to a closed form: a(n) = (1/8)*(-4+(2+sqrt(2))*(3+2*sqrt(2))^n + (2-sqrt(2))*(3-2*sqrt(2))^n). - Bruce Corrigan (scentman(AT)myfamily.com), Oct 30 2002",
				"a(n) = 2*Sum_{k = 0..n} A001109(k). - Mario Catalani (mario.catalani(AT)unito.it), Mar 22 2003",
				"For n\u003e=1, a(n) = 2*Sum_{k=0..n-1} (n-k)*A001653(k). - _Charlie Marion_, Jul 01 2003",
				"For n and j \u003e= 1, A001109(j+1)*A001652(n) - A001109(j)*A001652(n-1) + a(j) = A001652(n+j). - _Charlie Marion_, Jul 07 2003",
				"From _Antonio Alberto Olivares_, Jan 13 2004: (Start)",
				"a(n) = 7*a(n-1) - 7*a(n-2) + a(n-3).",
				"a(n) = -(1/2) - (1-sqrt(2))/(4*sqrt(2))*(3-2*sqrt(2))^n + (1+sqrt(2))/(4*sqrt(2))*(3+2*sqrt(2))^n. (End)",
				"a(n) = sqrt(2)*cosh((2*n+1)*log(1+sqrt(2)))/4 - 1/2 = (sqrt(1+4*A029549)-1)/2. - _Bill Gosper_, Feb 07 2010 [typo corrected by _Vaclav Kotesovec_, Feb 05 2016]",
				"a(n+1) + A055997(n+1) = A001541(n+1) + A001109(n+1). - _Creighton Dement_, Sep 16 2004",
				"From _Charlie Marion_, Oct 18 2004: (Start)",
				"For n\u003ek, a(n-k-1) = A001541(n)*A001653(k)-A011900(n+k); e.g., 2 = 99*5 - 493.",
				"For n\u003c=k, a(k-n) = A001541(n)*A001653(k) - A011900(n+k); e.g., 2 = 3*29 - 85 + 2. (End)",
				"a(n) = A084068(n)*A084068(n+1). - _Kenneth J Ramsey_, Aug 16 2007",
				"Let G(n,m) = (2*m+1)*a(n)+ m and H(n,m) = (2*m+1)*b(n)+m where b(n) is from the sequence A001652 and let T(a) = a*(a+1)/2. Then T(G(n,m)) + T(m) = 2*T(H(n,m)). - _Kenneth J Ramsey_, Aug 16 2007",
				"Let S(n) equal the average of two adjacent terms of G(n,m) as defined immediately above and B(n) be one half the difference of the same adjacent terms. Then for T(i) = triangular number i*(i+1)/2, T(S(n)) - T(m) = B(n)^2 (setting m = 0 gives the square triangular numbers). - _Kenneth J Ramsey_, Aug 16 2007",
				"a(n) = A001108(n+1) - A001109(n+1). - _Dylan Hamilton_, Nov 25 2010",
				"a(n) = (a(n-1)*(a(n-1) - 2))/a(n-2) for n \u003e 2. - _Vladimir Pletser_, Apr 08 2020",
				"a(n) = (ChebyshevU(n, 3) - ChebyshevU(n-1, 3) - 1)/2 = (Pell(2*n+1) - 1)/2. - _G. C. Greubel_, Apr 27 2020"
			],
			"maple": [
				"A053141 := proc(n)",
				"    option remember;",
				"    if n \u003c= 1 then",
				"        op(n+1,[0,2]) ;",
				"    else",
				"        6*procname(n-1)-procname(n-2)+2 ;",
				"    end if;",
				"end proc: # _R. J. Mathar_, Feb 05 2016"
			],
			"mathematica": [
				"Join[{a=0,b=1}, Table[c=6*b-a+1; a=b; b=c, {n,60}]]*2 (* _Vladimir Joseph Stephan Orlovsky_, Jan 18 2011 *)",
				"a[n_] := Floor[1/8*(2+Sqrt[2])*(3+2*Sqrt[2])^n]; Table[a[n], {n, 0, 20}] (* _Jean-François Alcover_, Nov 28 2013 *)",
				"Table[(Fibonacci[2n + 1, 2] - 1)/2, {n, 0, 20}] (* _Vladimir Reshetnikov_, Sep 16 2016 *)"
			],
			"program": [
				"(Haskell)",
				"a053141 n = a053141_list !! n",
				"a053141_list = 0 : 2 : map (+ 2)",
				"   (zipWith (-) (map (* 6) (tail a053141_list)) a053141_list)",
				"-- _Reinhard Zumkeller_, Jan 10 2012",
				"(PARI) concat(0,Vec(2/(1-x)/(1-6*x+x^2)+O(x^30))) \\\\ _Charles R Greathouse IV_, May 14 2012",
				"(PARI) {x=1+sqrt(2); y=1-sqrt(2); P(n) = (x^n - y^n)/(x-y)};",
				"a(n) = round((P(2*n+1) - 1)/2);",
				"for(n=0, 30, print1(a(n), \", \")) \\\\ _G. C. Greubel_, Jul 15 2018",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Integers(), 30); Coefficients(R!(2*x/((1-x)*(1-6*x+x^2)))); // _G. C. Greubel_, Jul 15 2018",
				"(Sage) [(lucas_number1(2*n+1, 2, -1)-1)/2 for n in range(30)] # _G. C. Greubel_, Apr 27 2020"
			],
			"xref": [
				"Cf. A000129, A001108, A001109, A001652, A001653.",
				"Cf. A011900, A029549, A053142, A075528, A103200.",
				"Partial sums of A001542."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_",
			"ext": [
				"Name corrected by _Zak Seidov_, Apr 11 2011"
			],
			"references": 47,
			"revision": 152,
			"time": "2021-05-19T01:45:49-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}