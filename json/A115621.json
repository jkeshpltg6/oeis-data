{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A115621",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 115621,
			"data": "1,1,2,1,1,1,3,1,1,1,2,1,2,4,1,1,1,1,1,1,2,1,2,1,3,5,1,1,1,1,1,2,1,2,1,1,1,3,1,3,2,2,1,4,6,1,1,1,1,1,1,1,1,2,1,1,1,1,2,1,2,1,3,1,1,2,1,3,1,4,2,3,1,5,7,1,1,1,1,1,1,1,2,1,2,1,1,1,1,1,1,1,2,1,2,1,3,1,1,2,2,2,1,1,2",
			"name": "Signature of partitions in Abramowitz and Stegun order.",
			"comment": [
				"The signature of a multiset is a partition consisting of the repetition factors of the original partition. Regarding a partition as a multiset, the signature of a partition is defined. E.g., [1,1,3,4,4] = [1^2,3^1,4^2], so the repetition factors are 2,1,2, making the signature [1,2,2] = [1,2^2]. Partitions are written here in increasing part size, so [1,2^2] is 1,2,2, not 2,2,1. - Edited by _Franklin T. Adams-Watters_, Jul 09 2012",
				"The sum (or order) of the signature is the number of parts of the original partition and the number of parts of the signature is the number of distinct parts of the original partition."
			],
			"link": [
				"Robert Price, \u003ca href=\"/A115621/b115621.txt\"\u003eTable of n, a(n) for n = 1..8266\u003c/a\u003e (first 20 rows).",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy]."
			],
			"example": [
				"[1];",
				"[1], [2];",
				"[1], [1,1], [3];",
				"[1], [1,1], [2], [1,2], [4];",
				"...",
				"From _Hartmut F. W. Hoft_, Apr 25 2015: (Start)",
				"Extending the triangle to rows 5 and 6 where row headings indicate the number of elements in the underlying partitions. Brackets group the multiplicities of a single partition.",
				"    row 5         row 6",
				"1:  [1]           [1]",
				"2:  [1,1] [1,1]   [1,1] [1,1] [2]",
				"3:  [1,2] [1,2]   [1,2] [1,1,1] [3]",
				"4:  [1,3]         [1,3] [2,2]",
				"5:  [5]           [1,4]",
				"6:                [6]",
				"(End)"
			],
			"mathematica": [
				"(* row[] and triangle[] compute structured rows of the triangle as laid out above *)",
				"mL[pL_] := Map[Last[Transpose[Tally[#]]]\u0026, pL]",
				"row[n_] := Map[Map[Sort, mL[#]]\u0026, GatherBy[Map[Sort, IntegerPartitions[n]], Length]]",
				"triangle[n_] := Map[row, Range[n]]",
				"a115621[n_]:= Flatten[triangle[n]]",
				"Take[a115621[8],105] (* data *)  (* _Hartmut F. W. Hoft_, Apr 25 2015 *)",
				"Map[Sort[#, Less] \u0026, Table[Last /@ Transpose /@ Tally /@ Sort[Reverse /@ IntegerPartitions[n]], {n, 8}], 2]"
			],
			"program": [
				"(SageMath) from collections import Counter",
				"def A115621_row(n):",
				"    h = lambda p: sorted(Counter(p).values())",
				"    return flatten([h(p) for k in (0..n) for p in Partitions(n, length=k)])",
				"for n in (1..10): print(A115621_row(n)) # _Peter Luschny_, Nov 02 2019"
			],
			"xref": [
				"Cf. A036036, A113787, A115622, A103921 (part counts), A000070 (row counts)."
			],
			"keyword": "nonn,tabf",
			"offset": "1,3",
			"author": "_Franklin T. Adams-Watters_, Jan 25 2006",
			"references": 15,
			"revision": 45,
			"time": "2020-06-13T00:49:18-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}