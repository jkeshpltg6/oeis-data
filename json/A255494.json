{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A255494",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 255494,
			"data": "1,1,1,1,4,1,1,13,13,1,1,38,130,38,1,1,105,1106,1106,105,1,1,280,8575,26544,8575,280,1,1,729,62475,567203,567203,62475,729,1,1,1866,435576,11179686,32897774,11179686,435576,1866,1,1,4717,2939208,207768576,1736613466,1736613466,207768576,2939208,4717,1",
			"name": "Triangle read by rows: coefficients of numerator of generating functions for powers of Pell numbers.",
			"comment": [
				"Note that Table 8 by Falcon should be labeled with the powers n (not r) and that the labels are off by 1. - _R. J. Mathar_, Jun 14 2015"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A255494/b255494.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e",
				"S. Falcon, \u003ca href=\"http://saspublisher.com/wp-content/uploads/2014/06/SJET24C669-675.pdf\"\u003eOn The Generating Functions of the Powers of the K-Fibonacci Numbers\u003c/a\u003e, Scholars Journal of Engineering and Technology (SJET), 2014; 2 (4C):669-675."
			],
			"formula": [
				"From _G. C. Greubel_, Sep 19 2021: (Start)",
				"T(n, k) = P(n-k+1)*T(n-1, k-1) + P(k+1)*T(n-1, k), where T(n, 0) = T(n, n) = 1 and P(n) = A000129(n).",
				"T(n, k) = T(n, n-k).",
				"T(n, 1) = A094706(n).",
				"T(n, 2) = A255495(n-2).",
				"T(n, 3) = A255496(n-3).",
				"T(n, 4) = A255497(n-4).",
				"T(n, 5) = A255498(n-5). (End)"
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  1,    1; # see A079291",
				"  1,    4,      1; # see A110272",
				"  1,   13,     13,        1;",
				"  1,   38,    130,       38,        1;",
				"  1,  105,   1106,     1106,      105,        1;",
				"  1,  280,   8575,    26544,     8575,      280,      1;",
				"  1,  729,  62475,   567203,   567203,    62475,    729,    1;",
				"  1, 1866, 435576, 11179686, 32897774, 11179686, 435576, 1866, 1;"
			],
			"mathematica": [
				"T[n_, k_]:= T[n,k]= If[k==0 || k==n, 1, Fibonacci[n-k+1, 2]*T[n-1, k-1] + Fibonacci[k+1, 2]*T[n-1, k]]; Table[T[n, k], {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, Sep 19 2021 *)"
			],
			"program": [
				"(MAGMA)",
				"P:= func\u003c n | Round(((1 + Sqrt(2))^n - (1 - Sqrt(2))^n)/(2*Sqrt(2))) \u003e;",
				"function T(n,k)",
				"  if k eq 0 or k eq n then return 1;",
				"  else return P(n-k+1)*T(n-1,k-1) + P(k+1)*T(n-1,k);",
				"  end if; return T;",
				"end function;",
				"[T(n,k): k in [0..n], n in [0..12]];",
				"(Sage)",
				"@CachedFunction",
				"def P(n): return lucas_number1(n, 2, -1)",
				"def T(n,k): return 1 if (k==0 or k==n) else P(n-k+1)*T(n-1, k-1) + P(k+1)*T(n-1, k)",
				"flatten([[T(n,k) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Sep 19 2021"
			],
			"xref": [
				"Cf. A000129, A079291, A094706, A110272.",
				"Diagonals: A094706, A255495, A255496, A255497, A255498."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_, Mar 06 2015",
			"references": 6,
			"revision": 30,
			"time": "2021-09-22T02:38:40-04:00",
			"created": "2015-03-06T18:06:00-05:00"
		}
	]
}