{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A330373",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 330373,
			"data": "0,1,0,3,4,5,6,7,16,18,20,22,36,39,42,60,80,85,90,114,140,168,176,207,264,300,312,378,448,493,540,620,736,825,884,1015,1188,1295,1406,1599,1840,2009,2184,2451,2772,3060,3312,3666,4176,4557,4900,5457,6084,6625,7182,7920,8792,9576,10324,11328,12540",
			"name": "Sum of all parts of all self-conjugate partitions of n.",
			"comment": [
				"a(n) is the sum of all parts of all partitions of n whose Ferrers diagrams are symmetric.",
				"The k-th part of a partition equals the number of parts \u003e= k of its conjugate partition. Hence, the k-th part of a self-conjugate partition equals the number of parts \u003e= k.",
				"The k-th rank of a partition is the k-th part minus the number of parts \u003e= k. Thus all ranks of a conjugate-partitions are zero. Therefore, a(n) is also the sum of all parts of all partitions of n whose n ranks are zero, n \u003e= 1. For more information about the k-th ranks see A208478."
			],
			"link": [
				"Freddy Barrera, \u003ca href=\"/A330373/b330373.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = n*A000700(n).",
				"a(n) = abs(n*A081362(n)).",
				"a(n) = abs(A235324(n)), n \u003e= 1."
			],
			"example": [
				"For n = 10 there are only two partitions of 10 whose Ferrers diagram are symmetric, they are [5, 2, 1, 1, 1] and [4, 3, 2, 1] as shown below:",
				"  * * * * *",
				"  * *",
				"  *",
				"  *",
				"  *",
				"            * * * *",
				"            * * *",
				"            * *",
				"            *",
				"The sum of all parts of these partitions is 5 + 2 + 1 + 1 + 1 + 4 + 3 + 2 + 1 = 20, so a(10) = 20.",
				"Also, in accordance with the first formula; a(10) = 2*10 = 20."
			],
			"program": [
				"(PARI) seq(n)={Vec(deriv(exp(sum(k=1, n, x^k/(k*(1 - (-x)^k)) + O(x*x^n)))), -(n+1))} \\\\ _Andrew Howroyd_, Dec 31 2019"
			],
			"xref": [
				"Row sums of A330372.",
				"For \"k-th rank\" of a partition see also: A181187, A208478, A208479, A208482, A208483, A330370.",
				"Cf. A000700, A066186, A081362, A235324."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Omar E. Pol_, Dec 17 2019",
			"references": 4,
			"revision": 32,
			"time": "2020-01-03T16:20:53-05:00",
			"created": "2019-12-31T08:27:18-05:00"
		}
	]
}