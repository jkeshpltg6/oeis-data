{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A239312",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 239312,
			"data": "1,1,1,2,3,3,5,6,9,10,14,16,23,27,33,41,51,62,75,93,111,134,159,189,226,271,317,376,445,520,609,714,832,972,1129,1304,1520,1753,2023,2326,2692,3077,3540,4050,4642,5298,6054,6887,7854,8926,10133,11501,13044",
			"name": "Number of condensed partitions of n; see Comments.",
			"comment": [
				"Suppose that p is a partition of n.  Let x(1), x(2), ..., x(k) be the distinct parts of p, and let m(i) be the multiplicity of x(i) in p.  Let c(p) be the partition {m(1)*x(1), m(2)*x(2), ... , x(k)*m(k)} of n.  Call a partition q of n a condensed partition of n if q = c(p) for some partition p of n.  Then a(n) is the number of distinct condensed partitions of n.  Note that c(p) = p if and only if p has distinct parts and that condensed partitions can have repeated parts."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A239312/b239312.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e (first 84 terms from Manfred Scheucher)",
				"Manfred Scheucher, \u003ca href=\"/A239312/a239312.py.txt\"\u003ePython Script\u003c/a\u003e"
			],
			"example": [
				"a(5) = 3 gives the number of partitions of 5 that result from condensations as shown here:  5 -\u003e 5, 41 -\u003e 41, 32 -\u003e 32, 311 -\u003e 32, 221 -\u003e 41, 2111 -\u003e 32, 11111 -\u003e 5."
			],
			"maple": [
				"b:= proc(n,i) option remember; `if`(n=0, {[]},",
				"      `if`(i=1, {[n]}, {seq(map(x-\u003e `if`(j=0, x,",
				"       sort([x[], i*j])), b(n-i*j, i-1))[], j=0..n/i)}))",
				"    end:",
				"a:= n-\u003e nops(b(n$2)):",
				"seq(a(n), n=0..50);  # _Alois P. Heinz_, Jul 01 2019"
			],
			"mathematica": [
				"u[n_, k_] := u[n, k] = Map[Total, Split[IntegerPartitions[n][[k]]]]; t[n_] := t[n] = DeleteDuplicates[Table[Sort[u[n, k]], {k, 1, PartitionsP[n]}]]; Table[Length[t[n]], {n, 0,   30}]"
			],
			"xref": [
				"Cf. A237685."
			],
			"keyword": "nonn,easy",
			"offset": "0,4",
			"author": "_Clark Kimberling_, Mar 15 2014",
			"ext": [
				"Typo in definition corrected by _Manfred Scheucher_, May 29 2015"
			],
			"references": 5,
			"revision": 22,
			"time": "2019-07-02T08:23:49-04:00",
			"created": "2014-03-16T12:58:29-04:00"
		}
	]
}