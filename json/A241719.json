{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A241719",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 241719,
			"data": "1,1,1,2,1,2,1,3,2,4,6,1,5,7,1,6,11,2,8,16,3,10,31,15,1,12,36,16,1,15,55,29,2,18,71,41,3,22,101,65,5,27,147,144,32,1,32,188,179,35,1,38,245,269,63,2,46,327,382,93,3,54,421,549,148,5,64,540,739,205,7",
			"name": "Number T(n,k) of compositions of n into distinct parts with exactly k descents; triangle T(n,k), n\u003e=0, 0\u003c=k\u003c=max(floor((sqrt(1+8*n)-3)/2),0), read by rows.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A241719/b241719.txt\"\u003eRows n = 0..500, flattened\u003c/a\u003e"
			],
			"example": [
				"T(6,0) = 4: [6], [2,4], [1,5], [1,2,3].",
				"T(6,1) = 6: [5,1], [4,2], [3,1,2], [1,3,2], [2,1,3], [2,3,1].",
				"T(6,2) = 1: [3,2,1].",
				"T(7,0) = 5: [7], [3,4], [2,5], [1,6], [1,2,4].",
				"T(7,1) = 7: [6,1], [4,3], [5,2], [2,1,4], [1,4,2], [2,4,1], [4,1,2].",
				"T(7,2) = 1: [4,2,1].",
				"Triangle T(n,k) begins:",
				"00:   1;",
				"01:   1;",
				"02:   1;",
				"03:   2,   1;",
				"04:   2,   1;",
				"05:   3,   2;",
				"06:   4,   6,   1;",
				"07:   5,   7,   1;",
				"08:   6,  11,   2;",
				"09:   8,  16,   3;",
				"10:  10,  31,  15,  1;",
				"11:  12,  36,  16,  1;",
				"12:  15,  55,  29,  2;",
				"13:  18,  71,  41,  3;",
				"14:  22, 101,  65,  5;",
				"15:  27, 147, 144, 32, 1;"
			],
			"maple": [
				"g:= proc(u, o) option remember; `if`(u+o=0, 1, expand(",
				"      add(g(u+j-1, o-j)  , j=1..o)+",
				"      add(g(u-j, o+j-1)*x, j=1..u)))",
				"    end:",
				"b:= proc(n, i) option remember; local m; m:= i*(i+1)/2;",
				"      `if`(n\u003em, 0, `if`(n=m, x^i,",
				"      expand(b(n, i-1) +`if`(i\u003en, 0, x*b(n-i, i-1)))))",
				"    end:",
				"T:= n-\u003e (p-\u003e (q-\u003e seq(coeff(q, x, i), i=0..degree(q)))(add(",
				"         coeff(p, x, k)*g(0, k), k=0..degree(p))))(b(n$2)):",
				"seq(T(n), n=0..20);"
			],
			"mathematica": [
				"g[u_, o_] := g[u, o] = If[u+o == 0, 1, Expand[Sum[g[u+j-1, o-j], {j, 1, o}] + Sum[g[u-j, o+j-1]*x, {j, 1, u}]]]; b[n_, i_] := b[n, i] = Module[{m}, m = i*(i+1)/2; If[n\u003em, 0, If[n == m, x^i, Expand[b[n, i-1] + If[i\u003en, 0, x*b[n-i, i-1]]]]]]; T[n_] := Function [p, Function[q, Table[Coefficient[q, x, i], {i, 0, Exponent[q, x]}]][Sum[Coefficient[p, x, k]*g[0, k], {k, 0, Exponent[p, x]}]]][b[n, n]]; Table[T[n], {n, 0, 20}] // Flatten (* _Jean-François Alcover_, Apr 28 2014, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Columns k=0-10 give: A000009, A241720, A241721, A241722, A241723, A241724, A241725, A241726, A241727, A241728, A241729.",
				"Row sums give A032020.",
				"T(A000217(k+1)-1,k-1) = A000041(k) for k\u003e0.",
				"Cf. A052146."
			],
			"keyword": "nonn,tabf,look",
			"offset": "0,4",
			"author": "_Alois P. Heinz_, Apr 27 2014",
			"references": 12,
			"revision": 22,
			"time": "2014-10-30T22:39:36-04:00",
			"created": "2014-04-28T09:38:11-04:00"
		}
	]
}