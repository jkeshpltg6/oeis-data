{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A242193",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 242193,
			"data": "1,1,1,1,5,691,7,3617,43867,283,11,103,13,9349,1721,37,17,26315271553053477373,19,137616929,1520097643918070802691,59,23,653,417202699,577,39409,113161,29,2003,31,1226592271,839,101,688531",
			"name": "Least prime p such that B_{2*n} == 0 (mod p) but there is no k \u003c n with B_{2k} == 0 (mod p), or 1 if such a prime p does not exist, where B_m denotes the m-th Bernoulli number.",
			"comment": [
				"Conjecture: a(n) is prime for any n \u003e 4.",
				"It is known that (-1)^(n-1)*B_{2*n} \u003e 0 for all n \u003e 0.",
				"See also A242194 for a similar conjecture involving Euler numbers."
			],
			"link": [
				"Peter Luschny, \u003ca href=\"/A242193/b242193.txt\"\u003eTable of n, a(n) for n = 1..103\u003c/a\u003e, (a(1)..a(60) from Zhi-Wei Sun)",
				"Z.-W. Sun, \u003ca href=\"http://arxiv.org/abs/1405.0290\"\u003eNew observations on primitive roots modulo primes\u003c/a\u003e, arXiv preprint arXiv:1405.0290 [math.NT], 2014."
			],
			"example": [
				"a(14) = 9349 since the numerator of |B_{28}| is 7*9349*362903 with B_2*B_4*B_6*...*B_{26} not congruent to 0 modulo 9349, but B_{14} == 0 (mod 7)."
			],
			"mathematica": [
				"b[n_]:=Numerator[Abs[BernoulliB[2n]]]",
				"f[n_]:=FactorInteger[b[n]]",
				"p[n_]:=Table[Part[Part[f[n],k],1],{k,1,Length[f[n]]}]",
				"Do[If[b[n]\u003c2,Goto[cc]];Do[Do[If[Mod[b[i],Part[p[n],k]]==0,Goto[aa]],{i,1,n-1}];Print[n,\" \",Part[p[n],k]];Goto[bb];Label[aa];Continue,{k,1,Length[p[n]]}];Label[cc];Print[n,\" \",1];Label[bb];Continue,{n,1,35}]",
				"(* Second program: *)",
				"LPDtransform[n_, fun_] := Module[{d}, d[p_] := AllTrue[Range[n - 1], !Divisible[fun[#], p]\u0026]; SelectFirst[FactorInteger[fun[n]][[All, 1]], d] /. Missing[_] -\u003e 1];",
				"A242193list[sup_] := Table[LPDtransform[n, Function[k, Abs[BernoulliB[2k]] // Numerator]], {n, 1, sup}]",
				"A242193list[35] (* _Jean-François Alcover_, Jul 27 2019, after _Peter Luschny_ *)"
			],
			"program": [
				"(Sage)",
				"def LPDtransform(n, fun):",
				"    d = lambda p: all(not p.divides(fun(k)) for k in (1..n-1))",
				"    return next((p for p in prime_divisors(fun(n)) if d(p)), 1)",
				"A242193list = lambda sup: [LPDtransform(n, lambda k: abs(bernoulli(2*k)).numerator()) for n in (1..sup)]",
				"print(A242193list(35)) # _Peter Luschny_, Jul 26 2019"
			],
			"xref": [
				"Cf. A000040, A027641, A242169, A242170, A242171, A242173, A242194, A242195."
			],
			"keyword": "nonn",
			"offset": "1,5",
			"author": "_Zhi-Wei Sun_, May 07 2014",
			"references": 10,
			"revision": 31,
			"time": "2019-08-05T02:49:51-04:00",
			"created": "2014-05-07T04:27:42-04:00"
		}
	]
}