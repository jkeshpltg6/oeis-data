{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A301305",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 301305,
			"data": "1,-1,1,3,-5,2,-14,33,-25,6,85,-261,292,-140,24,-621,2363,-3516,2546,-892,120,5236,-23872,44537,-43405,23228,-6444,720,-49680,264860,-596396,733983,-532095,226644,-52356,5040,521721,-3193029,8448004,-12605668,11586756,-6707208,2383248,-474144,40320,-5994155,41506739,-126480376,222424796,-248535142,182793154,-88379152,27046632,-4745376,362880,74701055,-578419961,1998774636,-4056699966,5351696394,-4791391134,2945757656,-1226765624,330797184,-52079040,3628800",
			"name": "G.f. L(x,y) satisfies: L(x,y) = x * (1 + y*x*L'(x,y)) / (1 + x*L'(x,y)) where L'(x,y) = d/dx L(x,y), as a triangle read by rows.",
			"comment": [
				"Main diagonal equals the factorials.",
				"Column 0 forms A088716, signed.",
				"Rows sums are zeros after the initial row.",
				"Absolute row sums equal A301388."
			],
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A301305/b301305.txt\"\u003eTable of n, a(n) for n = 1..1275 giving rows 1..50 as a flattened triangle read by rows.\u003c/a\u003e"
			],
			"formula": [
				"G.f. L(x,y) satisfies: [x^n] exp( -n * L(x,y) ) = ((y-1)*(n-1) - 1) * [x^(n-1)] exp( -n * L(x,y) ) for n\u003e=1."
			],
			"example": [
				"G.f. L(x,y) = Sum_{n\u003e=1, k=0..n-1} T(n,k) * x^n*y^k begins:",
				"L(x,y) = x + (-1 + y)*x^2 + (3 - 5*y + 2*y^2)*x^3 + (-14 + 33*y - 25*y^2 + 6*y^3)*x^4 + (85 - 261*y + 292*y^2 - 140*y^3 + 24*y^4)*x^5 + (-621 + 2363*y - 3516*y^2 + 2546*y^3 - 892*y^4 + 120*y^5)*x^6 + (5236 - 23872*y + 44537*y^2 - 43405*y^3 + 23228*y^4 - 6444*y^5 + 720*y^6)*x^7 + (-49680 + 264860*y - 596396*y^2 + 733983*y^3 - 532095*y^4 + 226644*y^5 - 52356*y^6 + 5040*y^7)*x^8 + ...",
				"where L = L(x,y) satisfies:",
				"L = x*(1 + y*x*L') / (1 + x*L').",
				"TRIANGLE.",
				"This triangle of coefficients T(n,k) in L(x,y) begins:",
				"1;",
				"-1, 1;",
				"3, -5, 2;",
				"-14, 33, -25, 6;",
				"85, -261, 292, -140, 24;",
				"-621, 2363, -3516, 2546, -892, 120;",
				"5236, -23872, 44537, -43405, 23228, -6444, 720;",
				"-49680, 264860, -596396, 733983, -532095, 226644, -52356, 5040;",
				"521721, -3193029, 8448004, -12605668, 11586756, -6707208, 2383248, -474144, 40320;",
				"-5994155, 41506739, -126480376, 222424796, -248535142, 182793154, -88379152, 27046632, -4745376, 362880; ...",
				"LIMITS.",
				"In this triangle, the largest real root of the n-th row polynomial in y converges to the constant t = 2.845344903202547217277843362090557097661... (A301389).",
				"RELATED SERIES.",
				"exp(L(x,y)) = 1 + x + (-1 + 2*y)*x^2/2! + (13 - 24*y + 12*y^2)*x^3/3!  +  (-263 + 660*y - 540*y^2 + 144*y^3)*x^4/4!  +  (8381 - 26800*y + 31380*y^2 - 15840*y^3 + 2880*y^4)*x^5/5!  +  (-379409 + 1485870*y - 2280180*y^2 + 1706520*y^3 - 619200*y^4 + 86400*y^5)*x^6/6!  +  (22915369 - 106759128*y + 203726880*y^2 - 203269920*y^3 + 111449520*y^4 - 31691520*y^5 + 3628800*y^6)*x^7/7! + ...",
				"satisfies: [x^n] exp(-n*L(x,y)) = ((y-1)*(n-1) - 1) * [x^(n-1)] exp(-n*L(x,y)) for n\u003e=1."
			],
			"program": [
				"(PARI) {T(n,k) = my(L=x); for(i=0,n, L = x*(1 + y*x*L')/(1 + x*L' +x*O(x^n)) ); polcoeff(polcoeff(L,n,x),k,y)}",
				"for(n=1,12, for(k=0,n-1, print1(T(n,k),\", \"));print(\"\"))"
			],
			"xref": [
				"Cf. A088716, A301388, A301389."
			],
			"keyword": "sign,tabl",
			"offset": "1,4",
			"author": "_Paul D. Hanna_, Mar 20 2018",
			"references": 2,
			"revision": 16,
			"time": "2018-03-26T13:14:30-04:00",
			"created": "2018-03-21T00:00:21-04:00"
		}
	]
}