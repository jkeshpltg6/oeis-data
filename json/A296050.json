{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A296050",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 296050,
			"data": "0,0,1,2,8,40,236,1648,13125,117794,1175224,12903874,154615096,2007498192,28075470833,420753819282,6726830163592,114278495205524,2055782983578788,39039148388975552,780412763620655061,16381683795665956242,360258256118419518680,8283042472303599966974",
			"name": "Number of permutations p of [n] such that min_{j=1..n} |p(j)-j| = 1.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A296050/b296050.txt\"\u003eTable of n, a(n) for n = 0..450\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A000142(n) - A001883(n) - A002467(n).",
				"a(n) = A000166(n) - A001883(n).",
				"a(n) = Sum_{k=1..n} A323671(n,k).",
				"a(n) is odd \u003c=\u003e n in { A016933 }.",
				"a(n) is even \u003c=\u003e n in { A047252 }."
			],
			"example": [
				"a(2) = 1: 21.",
				"a(3) = 2: 231, 312.",
				"a(4) = 8: 2143, 2341, 2413, 3142, 3421, 4123, 4312, 4321.",
				"a(5) = 40: 21453, 21534, 23154, 23451, 23514, 24153, 24513, 24531, 25134, 25413, 25431, 31254, 31452, 31524, 34152, 34251, 35124, 35214, 35412, 35421, 41253, 41523, 41532, 43152, 43251, 43512, 43521, 45132, 45213, 45231, 51234, 51423, 51432, 53124, 53214, 53412, 53421, 54132, 54213, 54231."
			],
			"maple": [
				"b:= proc(s, k) option remember; (n-\u003e `if`(n=0, `if`(k=1, 1, 0), add(",
				"      `if`(n=j, 0, b(s minus {j}, min(k, abs(n-j)))), j=s)))(nops(s))",
				"    end:",
				"a:= n-\u003e b({$1..n}, n):",
				"seq(a(n), n=0..14);",
				"# second Maple program:",
				"a:= n-\u003e (f-\u003e f(1)-f(2))(k-\u003e `if`(n=0, 1, LinearAlgebra[Permanent](",
				"        Matrix(n, (i, j)-\u003e `if`(abs(i-j)\u003e=k, 1, 0))))):",
				"seq(a(n), n=0..14);",
				"# third Maple program:",
				"g:= proc(n) g(n):= `if`(n\u003c2, 1-n, (n-1)*(g(n-1)+g(n-2))) end:",
				"h:= proc(n) h(n):= `if`(n\u003c7, [1, 0$3, 1, 4, 29][n+1], n*h(n-1)+4*h(n-2)",
				"      -3*(n-3)*h(n-3)+(n-4)*h(n-4)+2*(n-5)*h(n-5)-(n-7)*h(n-6)-h(n-7))",
				"    end:",
				"a:= n-\u003e g(n)-h(n):",
				"seq(a(n), n=0..25);"
			],
			"mathematica": [
				"g[n_] := g[n] = If[n \u003c 2, 1-n, (n-1)(g[n-1] + g[n-2])];",
				"h[n_] := h[n] = If[n \u003c 7, {1, 0, 0, 0, 1, 4, 29}[[n+1]],",
				"     n h[n-1] + 4h[n-2] - 3(n-3)h[n-3] + (n-4)h[n-4] +",
				"     2(n-5)h[n-5] - (n-7)h[n-6] - h[n-7]];",
				"a[n_] := g[n] - h[n];",
				"Table[a[n], {n, 0, 25}] (* _Jean-François Alcover_, Aug 30 2021, after third Maple program *)"
			],
			"xref": [
				"Column k=1 of A299789.",
				"Cf. A000142, A000166, A001883, A002467, A016933, A047252, A323671."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Alois P. Heinz_, Jan 21 2019",
			"references": 3,
			"revision": 101,
			"time": "2021-08-30T09:39:39-04:00",
			"created": "2019-01-22T09:37:09-05:00"
		}
	]
}