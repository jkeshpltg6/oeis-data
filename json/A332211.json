{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A332211",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 332211,
			"data": "2,3,7,5,31,11,127,13,17,19,23,29,8191,37,41,43,131071,47,524287,53,59,61,67,71,73,79,83,89,97,101,2147483647,103,107,109,113,131,137,139,149,151,157,163,167,173,179,181,191,193,197,199,211,223,227,229,233,239,241,251,257,263,2305843009213693951,269,271,277,281,283,293,307,311,313",
			"name": "Lexicographically earliest permutation of primes such that a(n) = 2^n - 1 when n is one of the Mersenne prime exponents (in A000043).",
			"comment": [
				"Sequence is well-defined also in case there are only a finite number of Mersenne primes."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A332211/b332211.txt\"\u003eTable of n, a(n) for n = 1..3217\u003c/a\u003e"
			],
			"formula": [
				"For all applicable n \u003e= 1, a(A000043(n)) = A000668(n)."
			],
			"example": [
				"For p in A000043: 2, 3, 5, 7, 13, 17, 19, ..., a(p) = (2^p)-1, thus a(2) = 2^2 - 1 = 3, a(3) = 7, a(5) = 31, a(7) = 127, a(13) = 8191, a(17) = 131071, etc., with the rest of positions filled by the least unused prime:",
				"1, 2, 3, 4,  5,  6,   7,  8,  9, 10, 11, 12,   13, 14, 15, 16, 17, ...",
				"2, 3, 7, 5, 31, 11, 127, 13, 17, 19, 23, 29, 8191, 37, 41, 43, 131071, ..."
			],
			"program": [
				"(PARI)",
				"up_to = 127;",
				"A332211list(up_to) = { my(v=vector(up_to), xs=Map(), i=1, q); for(n=1,up_to, if(isprime(q=((2^n)-1)), v[n] = q, while(mapisdefined(xs,prime(i)), i++); v[n] = prime(i)); mapput(xs,v[n],n)); (v); };",
				"v332211 = A332211list(up_to);",
				"A332211(n) = v332211[n];",
				"\\\\ For faster computing of larger values, use precomputed values of A000043:",
				"v000043 = [2,3,5,7,13,17,19,31,61,89,107,127,521,607,1279,2203,2281,3217];",
				"up_to = v000043[#v000043];",
				"A332211list(up_to) = { my(v=vector(up_to), xs=Map(), i=1, q); for(n=1,up_to, if(vecsearch(v000043,n), q = (2^n)-1, while(mapisdefined(xs,prime(i)), i++); q = prime(i)); v[n] = q; mapput(xs,q,n)); (v); };"
			],
			"xref": [
				"Cf. A000040, A000043, A000668, A332210 (inverse permutation of primes), A332220.",
				"Used to construct permutations A332212, A332214."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Antti Karttunen_, Feb 09 2020",
			"references": 8,
			"revision": 26,
			"time": "2020-02-09T20:12:45-05:00",
			"created": "2020-02-09T20:12:45-05:00"
		}
	]
}