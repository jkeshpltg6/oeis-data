{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A114710",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 114710,
			"data": "1,0,2,6,26,114,526,2502,12194,60570,305526,1560798,8058714,41987106,220470942,1165553718,6198683090,33140219946,178012804678,960232902606,5199384505226,28250295397170,153977094874862,841656387060006",
			"name": "Number of hill-free Schroeder paths of length 2n that have no horizontal steps on the x-axis.",
			"comment": [
				"A Schroeder path of length 2n is a lattice path from (0, 0) to (2n, 0) consisting of U = (1,1), D = (1,-1) and H = (2,0) steps and never going below the x-axis. A hill is a peak at height 1.",
				"Hankel transform is 2^C(n+1,2) (A006125(n+1)). Hankel transform of a(n+1) is (2-2^(n+1))*2^C(n+1,2). - _Paul Barry_, Oct 31 2008"
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A114710/b114710.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Paul Barry, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL16/Barry3/barry252.html\"\u003eOn the Inverses of a Family of Pascal-Like Matrices Defined by Riordan Arrays\u003c/a\u003e, Journal of Integer Sequences, 16 (2013), #13.5.6.",
				"Shishuo Fu, Yaling Wang, \u003ca href=\"https://arxiv.org/abs/1908.03912\"\u003eBijective recurrences concerning two Schröder triangles\u003c/a\u003e, arXiv:1908.03912 [math.CO], 2019."
			],
			"formula": [
				"G.f.: 2/(1+3*x+sqrt(1-6*x+x^2)).",
				"Apparently 3*(n+1)*a(n) +(11-16*n)*a(n-1) -9*n*a(n-2) +2*(n-2)*a(n-3)=0. - _R. J. Mathar_, Nov 07 2012",
				"G.f.: 1/(Q(0) + 2*x) where Q(k) = 1 + k*(1-x) - x - x*(k+1)*(k+2)/Q(k+1); (continued fraction). - _Sergei N. Gladkovskii_, Mar 14 2013",
				"a(n) = (-1)^n*Sum_{k=0..n} binomial(n, k)*hypergeom([k - n, n + 1], [k + 2], 2). - _Peter Luschny_, Jan 08 2018"
			],
			"example": [
				"a(3) = 6 because we have UHHD, UHUDD, UUDHD, UUDUDD, UUHDD and UUUDDD."
			],
			"maple": [
				"G:=2/(1+3*z+sqrt(1-6*z+z^2)): Gser:=series(G,z=0,32):",
				"1,seq(coeff(Gser,z^n),n=1..27);"
			],
			"mathematica": [
				"A114710[n_] := (-1)^n Sum[Binomial[n, k] Hypergeometric2F1[k - n, n + 1, k + 2, 2], {k, 0, n}]; Table[A114710[n], {n, 0, 23}] (* _Peter Luschny_, Jan 08 2018 *)",
				"InverseInvertTransform[ser_, n_] := CoefficientList[Series[ser/(1 + x ser), {x, 0, n}], x]; LittleSchroeder := (1 + x - Sqrt[1 - 6 x + x^2])/(4 x); (* A001003 *)",
				"InverseInvertTransform[LittleSchroeder, 23] (* _Peter Luschny_, Jan 10 2019 *)"
			],
			"xref": [
				"Column 0 of A114709.",
				"Cf. A001003, A104219."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Emeric Deutsch_, Dec 26 2005",
			"references": 3,
			"revision": 30,
			"time": "2019-10-30T22:06:44-04:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}