{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A046057",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 46057,
			"data": "1,4,75,28,8,42,375,510,308,90,140,88,56,16,24,100,675,156,1029,820,1875,6321,294,546,2450,2550,1210,2156,1380,270,11774,630",
			"name": "Smallest order m \u003e 0 for which there are n nonisomorphic finite groups of order m, or 0 if no such order exists.",
			"comment": [
				"R. Keith Dennis conjectures that there are no 0's in this sequence. See A053403 for details.",
				"In (John H. Conway, Heiko Dietrich and E. A. O'Brien, 2008), m is called the \"minimal order attaining n\" and is denoted by moa(n). - _Daniel Forgues_, Feb 15 2017",
				"a(33) \u003e 30500. - _Muniru A Asiru_, Nov 15 2017",
				"From _Jorge R. F. F. Lopes_, Jan 07 2022: (Start)",
				"The following values taken from the Max Horn website are improvements over those given in the Conway-Dietrich-O'Brien table (see Links):",
				"a(58) = 3591, a(59) = 6328, a(63) = 2025, a(73) \u003c= 24003, a(74) \u003c= 25250, a(78) \u003c= 12750, a(90) = 2970, a(91) = 2058, a(92) \u003c= 15092. (End)"
			],
			"reference": [
				"J. H. Conway et al., The Symmetries of Things, Peters, 2008, p. 209."
			],
			"link": [
				"H. U. Besche, \u003ca href=\"https://www.gap-system.org/Packages/sgl.html\"\u003eThe Small Groups library\u003c/a\u003e",
				"Hans Ulrich Besche and Bettina Eick, \u003ca href=\"https://doi.org/10.1006/jsco.1998.0258\"\u003eConstruction of finite groups\u003c/a\u003e, Journal of Symbolic Computation, Vol. 27, No. 4, Apr 15 1999, pp. 387-404.",
				"Hans Ulrich Besche and Bettina Eick, \u003ca href=\"https://doi.org/10.1006/jsco.1998.0259\"\u003eThe groups of order at most 1000 except 512 and 768\u003c/a\u003e, Journal of Symbolic Computation, Vol. 27, No. 4, Apr 15 1999, pp. 405-413.",
				"John H. Conway, Heiko Dietrich and E. A. O'Brien, \u003ca href=\"http://www.math.auckland.ac.nz/~obrien/research/gnu.pdf\"\u003eCounting groups: gnus, moas and other exotica\u003c/a\u003e, The Mathematical Intelligencer, Volume 30, Issue 2 (2008), pp 6-15, DOI:\u003ca href=\"https://doi.org/10.1007/BF02985731\"\u003e10.1007/BF02985731\u003c/a\u003e.",
				"John H. Conway, Heiko Dietrich and E. A. O'Brien, \u003ca href=\"/A046057/a046057_1.txt\"\u003eTable of n, a(n) for n = 1..100\u003c/a\u003e (with some question marks) [contains some errors: see comment from _Jorge R. F. F. Lopes_].",
				"Steven Finch, \u003ca href=\"https://doi.org/10.1007/s00283-021-10060-2\"\u003eThe On-Line Encyclopedia of Integer Sequences, founded in 1964 by N. J. A. Sloane\u003c/a\u003e, A Tribute to John Horton Conway, The Mathematical Intelligencer (2021) Vol. 43, 146-147.",
				"Max Horn, \u003ca href=\"https://groups.quendi.de/\"\u003eNumbers of isomorphism types of finite groups of given order\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/FiniteGroup.html\"\u003eFinite Group.\u003c/a\u003e",
				"\u003ca href=\"/index/Gre#groups\"\u003eIndex entries for sequences related to groups\u003c/a\u003e"
			],
			"xref": [
				"Cf. A000001, A046056, A046058, A046059, A053403."
			],
			"keyword": "nonn,hard,more,nice,changed",
			"offset": "1,2",
			"author": "_Eric W. Weisstein_",
			"ext": [
				"More terms from Victoria A. Sapko (vsapko(AT)canes.gsw.edu), Nov 04 2003",
				"a(20) corrected by _N. J. A. Sloane_, Jan 21 2004",
				"More terms from _N. J. A. Sloane_, Oct 03 2008, from the John H. Conway, Heiko Dietrich and E. A. O'Brien article.",
				"a(31)-a(32) from _Muniru A Asiru_, Nov 15 2017"
			],
			"references": 6,
			"revision": 75,
			"time": "2022-01-11T21:47:18-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}