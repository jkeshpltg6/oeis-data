{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001567",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1567,
			"id": "M5441 N2365",
			"data": "341,561,645,1105,1387,1729,1905,2047,2465,2701,2821,3277,4033,4369,4371,4681,5461,6601,7957,8321,8481,8911,10261,10585,11305,12801,13741,13747,13981,14491,15709,15841,16705,18705,18721,19951,23001,23377,25761,29341",
			"name": "Fermat pseudoprimes to base 2, also called Sarrus numbers or Poulet numbers.",
			"comment": [
				"A composite number n is a Fermat pseudoprime to base b if and only if b^(n-1) == 1 (mod n). Fermat pseudoprimes to base 2 are often simply called pseudoprimes.",
				"Theorem: If both numbers q and 2q - 1 are primes (q is in the sequence A005382) and n = q*(2q-1) then 2^(n-1) == 1 (mod n) (n is in the sequence) if and only if q is of the form 12k + 1. The sequence 2701, 18721, 49141, 104653, 226801, 665281, 721801, ... is related. This subsequence is also a subsequence of the sequences A005937 and A020137. - _Farideh Firoozbakht_, Sep 15 2006",
				"Also, composite odd numbers n such that n divides 2^n - 2 (cf. A006935). It is known that all primes p divide 2^(p-1) - 1. There are only two known numbers n such that n^2 divides 2^(n-1) - 1, A001220(n) = {1093, 3511} Wieferich primes p: p^2 divides 2^(p-1) - 1. 1093^2 and 3511^2 are the terms of a(n). - _Alexander Adamchuk_, Nov 06 2006",
				"An odd composite number 2n + 1 is in the sequence if and only if multiplicative order of 2 (mod 2n+1) divides 2n. - _Ray Chandler_, May 26 2008",
				"The Carmichael numbers A002997 are a subset of this sequence. For the Sarrus numbers which are not Carmichael numbers, see A153508. - _Artur Jasinski_, Dec 28 2008",
				"An odd number n greater than 1 is a Fermat pseudoprime to base b if and only if ((n-1)! - 1)*b^(n-1) == -1 (mod n). - _Arkadiusz Wesolowski_, Aug 20 2012",
				"The name \"Sarrus numbers\" is after Frédéric Sarrus, who, in 1819, discovered that 341 is a counterexample to the \"Chinese hypothesis\" that n is prime if and only if 2^n is congruent to 2 (mod n). - _Alonso del Arte_, Apr 28 2013",
				"The name \"Poulet numbers\" appears in Monografie Matematyczne 42 from 1932, apparently because Poulet in 1928 produced a list of these numbers (cf. Miller, 1975). - _Felix Fröhlich_, Aug 18 2014",
				"Numbers n \u003e 2 such that (n-1)! + 2^(n-1) == 1 (mod n). Composite numbers n such that (n-2)^(n-1) == 1 (mod n). - _Thomas Ordowski_, Feb 20 2016",
				"The only twin pseudoprimes up to 10^13 are 4369, 4371. - _Thomas Ordowski_, Feb 12 2016",
				"Theorem (A. Rotkiewicz, 1965): (2^p-1)*(2^q-1) is a pseudoprime if and only if p*q is a pseudoprime, where p,q are different primes. - _Thomas Ordowski_, Mar 31 2016",
				"Theorem (W. Sierpiński, 1947): if n is a pseudoprime (odd or even), then 2^n-1 is a pseudoprime. - _Thomas Ordowski_, Apr 01 2016",
				"If 2^n-1 is a pseudoprime, then n is a prime or a pseudoprime (odd or even). - _Thomas Ordowski_, Sep 05 2016",
				"From _Amiram Eldar_, Jun 19 2021: (Start)",
				"Ore (1948) called these numbers \"Numbers with the Fermat property\", or, for short, \"F numbers\".",
				"Erdős (1950) called them \"almost primes\".",
				"According to Erdős (1949) and Piza (1954), the term \"pseudoprime\" was coined by D. H. Lehmer.",
				"Named after the French mathematician Pierre de Fermat (1607-1665), or, alternatively, after the Belgian mathematician Paul Poulet (1887-1946), or, the French mathematician Pierre Frédéric Sarrus (1798-1861). (End)"
			],
			"reference": [
				"Richard K. Guy, Unsolved Problems in Number Theory, 3rd Edition, Springer, 2004, Section A12, p. 44-50.",
				"George P. Loweke, The Lore of Prime Numbers. New York: Vantage Press (1982), p. 22.",
				"Øystein Ore, Number Theory and Its History, McGraw-Hill, 1948.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A001567/b001567.txt\"\u003eTable of n, a(n) for n = 1..101629\u003c/a\u003e [The pseudoprimes up to 10^12, from Richard Pinch's web site - see links below]",
				"Jonathan Bayless and Paul Kinlaw, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL20/Bayless/bayless3.html\"\u003eExplicit Bounds for the Sum of Reciprocals of Pseudoprimes and Carmichael Numbers\u003c/a\u003e, Journal of Integer Sequences, Vol. 20 (2017), Article 17.6.4.",
				"Jens Bernheiden, \u003ca href=\"https://web.archive.org/web/20160508153313/http://www.mathe-schule.de/download/pdf/Primzahl/PSP.pdf\"\u003ePseudoprimes\u003c/a\u003e (Text in German).",
				"John Brillhart, N. J. A. Sloane, J. D. Swift, \u003ca href=\"/A001567/a001567_2.pdf\"\u003eCorrespondence, 1972\u003c/a\u003e.",
				"Paul Erdős, \u003ca href=\"https://www.jstor.org/stable/2304732\"\u003eOn the converse of Fermat's theorem\u003c/a\u003e, The American Mathematical Monthly, Vol. 56, No. 9 (1949), pp. 623-624; \u003ca href=\"https://users.renyi.hu/~p_erdos/1949-07.pdf\"\u003ealternative link\u003c/a\u003e.",
				"Paul Erdős, \u003ca href=\"https://www.jstor.org/stable/2307640\"\u003eOn almost primes\u003c/a\u003e, Amer. Math. Monthly, Vol. 57, No. 6 (1950), pp. 404-407; \u003ca href=\"https://users.renyi.hu/~p_erdos/1950-06.pdf\"\u003ealternative link\u003c/a\u003e.",
				"Jan Feitsma, \u003ca href=\"http://www.janfeitsma.nl/math/psp2/index\"\u003eThe pseudoprimes below 2^64\u003c/a\u003e.",
				"William Galway, \u003ca href=\"http://www.cecm.sfu.ca/Pseudoprimes/index-2-to-64.html\"\u003eTables of pseudoprimes and related data\u003c/a\u003e [Includes a file with pseudoprimes up to 2^64.]",
				"Richard K. Guy, \u003ca href=\"/A005165/a005165.pdf\"\u003eThe strong law of small numbers\u003c/a\u003e. Amer. Math. Monthly, Vol. 95, No. 8 (1988), pp. 697-712. [Annotated scanned copy]",
				"D. H. Lehmer, \u003ca href=\"https://www.nap.edu/read/18678/chapter/3#47\"\u003eGuide to Tables in the Theory of Numbers\u003c/a\u003e, Bulletin No. 105, National Research Council, Washington, DC, 1941, p. 48.",
				"D. H. Lehmer, \u003ca href=\"http://www.jstor.org/stable/2004371\"\u003eErrata for Poulet's table\u003c/a\u003e, Math. Comp., Vol. 25, No. 116 (1971), pp. 944-945.",
				"D. H. Lehmer, \u003ca href=\"/A001567/a001567_1.pdf\"\u003eErrata for Poulet's table\u003c/a\u003e.  [annotated scanned copy]",
				"Gérard P. Michon, \u003ca href=\"http://www.numericana.com/answer/pseudo.htm\"\u003ePseudoprimes\u003c/a\u003e.",
				"J. C. P. Miller, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1975-0366796-6\"\u003eOn factorization, with a suggested new approach\u003c/a\u003e, Math. Comp., Vol. 29, No. 129 (1975), pp. 155-172. - _Felix Fröhlich_, Aug 18 2014",
				"Robert Morris, \u003ca href=\"/A001567/a001567.pdf\"\u003eSome observations on the converse of Fermat's theorem\u003c/a\u003e, unpublished memorandum, Oct 03 1973.",
				"Richard Pinch, \u003ca href=\"http://www.chalcedon.demon.co.uk/rgep/carpsp.html\"\u003ePseudoprimes\u003c/a\u003e.",
				"P. A. Piza, \u003ca href=\"https://www.jstor.org/stable/3029103\"\u003eFermat Coefficients\u003c/a\u003e, Mathematics Magazine, Vol. 27, No. 3 (1954), pp. 141-146.",
				"Carl Pomerance \u0026 N. J. A. Sloane, \u003ca href=\"/A001567/a001567_4.pdf\"\u003eCorrespondence, 1991\u003c/a\u003e.",
				"Paul Poulet, \u003ca href=\"/A001567/a001567_3.pdf\"\u003eTables des nombres composés vérifiant le théorème de Fermat pour le module 2 jusqu'à 100.000.000\u003c/a\u003e, Sphinx (Brussels), Vol. 8 (1938), pp. 42-45. [annotated scanned copy]",
				"Fred Richman, \u003ca href=\"http://math.fau.edu/Richman/carm.htm\"\u003ePrimality testing with Fermat's little theorem\u003c/a\u003e.",
				"Andrzej Rotkiewicz, \u003ca href=\"http://gdz.sub.uni-goettingen.de/dms/resolveppn/?PPN=GDZPPN002075660\"\u003eSur les nombres pseudopremiers de la forme MpMq\u003c/a\u003e, Elemente der Mathematik, Vol. 20 (1965), pp. 108-109.",
				"Waclaw Sierpiński, \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/cm/cm1/cm113.pdf\"\u003eRemarque sur une hypothèse des Chinois concernant les nombres (2^n-2)/n\u003c/a\u003e, Colloquium Mathematicum, Vol. 1 (1947), p. 9.",
				"Waclaw Sierpiński, \u003ca href=\"http://matwbn.icm.edu.pl/kstresc.php?tom=42\u0026amp;wyd=10\"\u003eElementary Theory of Numbers\u003c/a\u003e, Państ. Wydaw. Nauk., Warszawa, 1964, p. 215.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ChineseHypothesis.html\"\u003eChinese Hypothesis\u003c/a\u003e, \u003ca href=\"http://mathworld.wolfram.com/FermatPseudoprime.html\"\u003eFermat Pseudoprime\u003c/a\u003e, \u003ca href=\"http://mathworld.wolfram.com/PouletNumber.html\"\u003ePoulet Number\u003c/a\u003e, and \u003ca href=\"http://mathworld.wolfram.com/Pseudoprime.html\"\u003ePseudoprime\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Chinese_hypothesis\"\u003eChinese hypothesis\u003c/a\u003e and \u003ca href=\"http://en.wikipedia.org/wiki/Pseudoprime\"\u003ePseudoprime\u003c/a\u003e.",
				"\u003ca href=\"/index/Ps#pseudoprimes\"\u003eIndex entries for sequences related to pseudoprimes\u003c/a\u003e"
			],
			"formula": [
				"Sum_{n\u003e=1} 1/a(n) is in the interval (0.015260, 33) (Bayless and Kinlaw, 2017). - _Amiram Eldar_, Oct 15 2020"
			],
			"maple": [
				"select(t -\u003e not isprime(t) and 2 \u0026^(t-1) mod t = 1, [seq(i,i=3..10^5,2)]); # _Robert Israel_, Feb 18 2016"
			],
			"mathematica": [
				"Select[Range[3,30000,2], ! PrimeQ[ # ] \u0026\u0026 PowerMod[2, (# - 1), # ] == 1 \u0026] (* _Farideh Firoozbakht_, Sep 15 2006 *)"
			],
			"program": [
				"(PARI) q=1;vector(50,i,until( !isprime(q) \u0026 (1\u003c\u003c(q-1)-1)%q == 0, q+=2);q) \\\\ _M. F. Hasler_, May 04 2007",
				"(PARI) is_A001567(n)={Mod(2,n)^(n-1)==1 \u0026\u0026 !isprime(n) \u0026\u0026 n\u003e1}  \\\\ _M. F. Hasler_, Oct 07 2012, updated to current PARI syntax and to exclude even pseudoprimes on Mar 01 2019",
				"(MAGMA) [n: n in [3..3*10^4 by 2] | IsOne(Modexp(2,n-1,n)) and not IsPrime(n)]; // _Bruno Berselli_, Jan 17 2013"
			],
			"xref": [
				"Cf. A002997, A005382, A020137, A052155, A083737, A084653, A153508.",
				"Cf. A001220 = Wieferich primes p: p^2 divides 2^(p-1) - 1.",
				"Cf. A005935, A005936, A005937, A005938, A005939, A020136-A020228 (pseudoprimes to bases 3 through 100)."
			],
			"keyword": "nonn,nice",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _David W. Wilson_, Aug 15 1996",
				"Replacement of broken geocities link by _Jason G. Wurtzel_, Sep 05 2010",
				"\"Poulet numbers\" added to name by _Joerg Arndt_, Aug 18 2014"
			],
			"references": 343,
			"revision": 213,
			"time": "2021-06-19T03:54:08-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}