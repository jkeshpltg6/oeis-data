{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A285305",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 285305,
			"data": "1,0,0,1,1,0,1,0,1,0,0,1,1,0,0,1,1,0,1,0,0,1,1,0,1,0,0,1,1,0,1,0,1,0,0,1,1,0,0,1,1,0,1,0,1,0,0,1,1,0,0,1,1,0,1,0,0,1,1,0,1,0,1,0,0,1,1,0,0,1,1,0,1,0,0,1,1,0,1,0,1,0,0,1,1,0",
			"name": "Fixed point of the morphism 0 -\u003e 10, 1 -\u003e 1001.",
			"comment": [
				"This is a 3-automatic sequence. See Allouche et al. link. - _Michel Dekking_, Oct 05 2020"
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A285305/b285305.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"J.-P. Allouche, F. M. Dekking, and M. Queffélec, \u003ca href=\"https://arxiv.org/abs/2010.00920\"\u003eHidden automatic sequences\u003c/a\u003e, arXiv:2010.00920 [math.NT], 2020.",
				"\u003ca href=\"/index/Fi#FIXEDPOINTS\"\u003eIndex entries for sequences that are fixed points of mappings\u003c/a\u003e"
			],
			"formula": [
				"Conjecture: a(n) = A284775(n+1). - _R. J. Mathar_, May 08 2017",
				"From _Michel Dekking_, Feb 20 2021: (Start)",
				"Proof of this conjecture. Let mu: 0 -\u003e 10, 1 -\u003e 1001 be the defining morphism of (a(n)), and let tau: 0 -\u003e 01, 1 -\u003e 0011 be the defining morphism of A284775.",
				"Since mu^n(0) starts with 1 for n\u003e1, mu^n(0) tends to (a(n)) as n tends to infinity. So the conjecture will follow directly from the following claim.",
				"CLAIM: 0 mu^n(0) = tau^n(0) 0 for n\u003e0.",
				"Proof: By induction over two levels, exploiting the obvious equality  tau(1) = 0 tau(0) 1 to go from the third to the fourth line below.",
				"For n=1: 0 mu(0)= 010 = tau(0) 0.",
				"For n=2: 0 mu^2(0)= 0100110 = tau^2(0) 0.",
				"Suppose true for n-1 and n. Then",
				"     tau^{n+1}(0) =",
				"     tau^n(tau(0)) =",
				"     tau^n(0) tau^n(1) =",
				"     tau^n(0) tau^{n-1)(0) tau^n(0) tau^{n-1}(1) =",
				"     0 mu^n(0)0^{-1} 0 mu^{n-1}(0)0^{-1}0mu^n(0)0^{-1}tau^{n-1)(1)=",
				"     0 mu^{n-1}(mu(0))mu^{n-1}(0)mu^{n-1}(mu(0))0^{-1}0tau^{n-)(1)=",
				"     0 mu^{n-1}(10) mu^{n-1}(0) mu^{n-1}(10) 0^{-1} tau^{n-1)(1) =",
				"     0 mu^{n-1}(1001) mu^{n-1}(0)  0^{-1} tau^{n-1)(1) =",
				"     0 mu^n(1)  0^{-1} tau^{n-1)(0) 0 0^{-1} tau^{n-1)(1) =",
				"     0 mu^n(1) 0^{-1} tau^{n-1}(01)  =",
				"     0 mu^n(1) 0^{-1} tau^n(0) =",
				"     0 mu^n(1) mu^n(0) 0^{-1} =",
				"     0 mu^n(mu(0)) 0^{-1} =",
				"     0 mu^{n+1}(0) 0^{-1}.",
				"So we proved 0 mu^{n+1}(0) = tau^{n+1}(0) 0.",
				"(End)"
			],
			"example": [
				"0 -\u003e 10-\u003e 1001 -\u003e 100110101001 -\u003e"
			],
			"mathematica": [
				"s = Nest[Flatten[# /. {0 -\u003e {1, 0}, 1 -\u003e {1, 0, 0, 1}}] \u0026, {0}, 10]; (* A285305 *)",
				"u = Flatten[Position[s, 0]];  (* A285306 *)",
				"v = Flatten[Position[s, 1]];  (* A285307 *)"
			],
			"xref": [
				"Cf. A284306, A285307, A284775 ."
			],
			"keyword": "nonn,easy",
			"offset": "1",
			"author": "_Clark Kimberling_, Apr 25 2017",
			"references": 4,
			"revision": 21,
			"time": "2021-02-20T11:55:27-05:00",
			"created": "2017-04-25T11:46:00-04:00"
		}
	]
}