{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000688",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 688,
			"id": "M0064 N0020",
			"data": "1,1,1,2,1,1,1,3,2,1,1,2,1,1,1,5,1,2,1,2,1,1,1,3,2,1,3,2,1,1,1,7,1,1,1,4,1,1,1,3,1,1,1,2,2,1,1,5,2,2,1,2,1,3,1,3,1,1,1,2,1,1,2,11,1,1,1,2,1,1,1,6,1,1,2,2,1,1,1,5,5,1,1,2,1,1,1,3,1,2,1,2,1,1,1,7,1,2,2,4,1,1,1,3,1,1,1",
			"name": "Number of Abelian groups of order n; number of factorizations of n into prime powers.",
			"comment": [
				"Equivalently, number of Abelian groups with n conjugacy classes. - _Michael Somos_, Aug 10 2010",
				"a(n) depends only on prime signature of n (cf. A025487). So a(24) = a(375) since 24 = 2^3*3 and 375 = 3*5^3 both have prime signature (3, 1).",
				"Also number of rings with n elements that are the direct product of fields; these are the commutative rings with n elements having no nilpotents; likewise the commutative rings where for every element x there is a k \u003e 0 such that x^(k+1) = x. - _Franklin T. Adams-Watters_, Oct 20 2006",
				"Range is A033637.",
				"a(n) = 1 if and only if n is from A005117 (squarefree numbers). See the Ahmed Fares comment there, and the formula for n\u003e=2 below. - _Wolfdieter Lang_, Sep 09 2012",
				"Also, from a theorem of Molnár (see [Molnár]), the number of (non-isomorphic) abelian groups of order 2*n + 1 is equal to the number of non-congruent lattice Z-tilings of R^n by crosses, where a \"cross\" is a unit cube in R^n for which at each facet is attached another unit cube (Z, R are the integers and reals, respectively). (Cf. [Horak].) - _L. Edson Jeffery_, Nov 29 2012",
				"Zeta(k*s) is the Dirichlet generating function of the characteristic function of numbers which are k-th powers (k=1 in A000012, k=2 in A010052, k=3 in A010057, see arXiv:1106.4038 Section 3.1). The infinite product over k (here) is the number of representations n=product_i (b_i)^(e_i) where all exponents e_i are distinct and \u003e=1. Examples: a(n=4)=2: 4^1 = 2^2. a(n=8)=3: 8^1 = 2^1*2^2 = 2^3. a(n=9)=2: 9^1 = 3^2. a(n=12)=2: 12^1 = 3*2^2. a(n=16)=5: 16^1 = 2*2^3 = 4^2 = 2^2*4^1 = 2^4. If the e_i are the set {1,2} we get A046951, the number of representations as a product of a number and a square. - _R. J. Mathar_, Nov 05 2016",
				"See A060689 for the number of non-abelian groups of order n. - _M. F. Hasler_, Oct 24 2017"
			],
			"reference": [
				"Steven R. Finch, Mathematical Constants, Cambridge, 2003, pp. 274-278.",
				"D. S. Mitrinovic et al., Handbook of Number Theory, Kluwer, Section XIII.12, p. 468.",
				"J. S. Rose, A Course on Group Theory, Camb. Univ. Press, 1978, see p. 7.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"A. Speiser, Die Theorie der Gruppen von endlicher Ordnung, 4. Auflage, Birkhäuser, 1956."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A000688/b000688.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Tak-Shing T. Chan, Y.-H. Yang, \u003ca href=\"https://doi.org/10.1109/TSP.2016.2612171\"\u003ePolar n-Complex and n-Bicomplex Singular Value Decomposition and Principal Component Pursuit\u003c/a\u003e, IEEE Transactions on Signal Processing ( Volume: 64, Issue: 24, Dec.15, 15 2016 ); DOI: 10.1109/TSP.2016.2612171.",
				"I. G. Connell, \u003ca href=\"http://cms.math.ca/10.4153/CMB-1964-002-1\"\u003eA number theory problem concerning finite groups and rings\u003c/a\u003e, Canad. Math. Bull, 7 (1964), 23-34.",
				"I. G. Connell, \u003ca href=\"/A000688/a000688.pdf\"\u003eLetter to N. J. A. Sloane, no date\u003c/a\u003e",
				"P. Erdős and G. Szekeres, \u003ca href=\"http://acta.bibl.u-szeged.hu/13441/1/math_007_095-102.pdf\"\u003eÜber die Anzahl der Abelschen Gruppen gegebener Ordnung und über ein verwandtes zahlentheoretisches Problem\u003c/a\u003e, Acta Sci. Math. (Szeged), 7 (1935), 95-102.",
				"Steven R. Finch, \u003ca href=\"http://www.people.fas.harvard.edu/~sfinch/constant/abel/abel.html\"\u003eAbelian Group Enumeration Constants\u003c/a\u003e [Broken link]",
				"Steven R. Finch, \u003ca href=\"http://web.archive.org/web/20010603070928/http://www.mathsoft.com/asolve/constant/abel/abel.html\"\u003eAbelian Group Enumeration Constants\u003c/a\u003e [broken link?] [From the Wayback machine]",
				"P. Horak, \u003ca href=\"http://dx.doi.org/10.2478/v10127-010-0004-y\"\u003eError-correcting codes and Minkowski's conjecture\u003c/a\u003e, Tatra Mt. Math. Publ., 45 (2010), p. 40.",
				"B. Horvat, G. Jaklic and T. Pisanski, \u003ca href=\"http://arXiv.org/abs/math.CO/0503183\"\u003eOn the number of Hamiltonian groups\u003c/a\u003e, arXiv:math/0503183 [math.CO], 2005.",
				"D. G. Kendall, R. A. Rankin, \u003ca href=\"http://dx.doi.org/10.1093/qmath/os-18.1.197\"\u003eOn the number of Abelian groups of a given order\u003c/a\u003e, Q. J. Math. 18 (1947) 197-208.",
				"Kurokawa, Nobushige; Wakayama, Masato. \u003ca href=\"http://projecteuclid.org/download/pdf_1/euclid.pja/1148392634\"\u003eZeta extensions\u003c/a\u003e. Proc. Japan Acad. Ser. A Math. Sci. 78 (2002), no. 7, 126--130. \u003ca href=\"http://www.ams.org/mathscinet-getitem?mr=1930216\"\u003eMR1930216 (2003h:11112)\u003c/a\u003e.",
				"E. Molnár, \u003ca href=\"http://www.bdim.eu/item?id=RLINA_1971_8_51_3-4_177_0\"\u003eSui mosaici dello spazio di dimensione n\u003c/a\u003e, Atti Accad. Naz. Lincei, VIII. Ser., Rend., Cl. Sci. Fis. Mat. Nat. 51 (1971), 177-185.",
				"H.-E. Richert, \u003ca href=\"http://gdz.sub.uni-goettingen.de/dms/load/img/?PPN=GDZPPN002382776\u0026amp;IDDOC=157448\"\u003eÜber die Anzahl Abelscher Gruppen gegebener Ordnung I\u003c/a\u003e, Math. Zeitschr. 56 (1952) 21-32.",
				"Marko Riedel, \u003ca href=\"http://math.stackexchange.com/questions/955649/\"\u003eCounting Abelian Groups\u003c/a\u003e, Math StackExchange, October 2014.",
				"Laszlo Toth, \u003ca href=\"http://arxiv.org/abs/1203.6473\"\u003eA note on the number of abelian groups of a given order\u003c/a\u003e, arXiv:1203.6473 [math.NT], (2012).",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/AbelianGroup.html\"\u003eAbelian Group\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/FiniteGroup.html\"\u003eFinite Group\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/KroneckerDecompositionTheorem.html\"\u003eKronecker Decomposition Theorem\u003c/a\u003e",
				"\u003ca href=\"/index/Gre#groups\"\u003eIndex entries for sequences related to groups\u003c/a\u003e",
				"\u003ca href=\"/index/Cor#core\"\u003eIndex entries for \"core\" sequences\u003c/a\u003e"
			],
			"formula": [
				"Multiplicative with a(p^k) = number of partitions of k = A000041(k); a(mn) = a(m)a(n) if (m, n) = 1.",
				"a(2n) = A101872(n).",
				"a(n) = product(A000041(e(j)),j = 1..N(n)), n \u003e= 2, if",
				"  n  = product(prime(j)^e(j), j = 1..N(n)), N(n) = A001221(n). See the Richert reference, quoting A. Speiser's book on finite groups (in German, p. 51 in words). - _Wolfdieter Lang_, Jul 23 2011",
				"In terms of the cycle index of the symmetric group: prod_{q=1}^m [z^{v_q}] Z(S_v) 1/(1-z) where v is the maximum exponent of any prime in the prime factorization of n, v_q are the exponents of the prime factors, and Z(S_v) is the cycle index of the symmetric group on v elements. - _Marko Riedel_, Oct 03 2014",
				"Dirichlet g.f.: sum_{n \u003e= 1} a(n)/n^s = product_{k \u003e= 1} zeta(ks) [Kendall]. - _Álvar Ibeas_, Nov 05 2014",
				"a(n)=2 for all n in A054753 and for all n in A085987.  a(n)=3 for all n in A030078 and for all n in A065036.  a(n)=4 for all n in A085986.  a(n)=5 for all n in A030514 and for all n in A178739.  a(n)=6 for all n in A143610. - _R. J. Mathar_, Nov 05 2016",
				"A050360(n) = a(A025487(n)). a(n) = A050360(A101296(n)). - _R. J. Mathar_, May 26 2017",
				"a(n) = A000001(n) - A060689(n). - _M. F. Hasler_, Oct 24 2017"
			],
			"example": [
				"a(1) = 1 since the trivial group {e} is the only group of order 1, and it is Abelian; alternatively, since the only factorization of 1 into prime powers is the empty product.",
				"a(p) = 1 for any prime p, since the only factorization into prime powers is p = p^1, and (in view of Lagrange's theorem) there is only one group of prime order p; it is isomorphic to (Z/pZ,+) and thus Abelian.",
				"From _Wolfdieter Lang_, Jul 22 2011: (Start)",
				"a(8) = 3 because 8 = 2^3, hence a(8) = pa(3) = A000041(3) = 3 from the partitions (3), (2, 1) and (1, 1, 1), leading to the 3 factorizations of 8: 8, 4*2 and 2*2*2.",
				"a(36) = 4 because 36 = 2^2*3^2, hence a(36) = pa(2)*pa(2) = 4 from the partitions (2) and (1, 1), leading to the 4 factorizations of 36: 2^2*3^2, 2^2*3^1*3^1, 2^1*2^1*3^2 and 2^1*2^1*3^1*3^1.",
				"(End)",
				"From _Amiram Eldar_, Nov 01 2020: (Start)",
				"a(n) = a(A057521(n)).",
				"Asymptotic mean: lim_{n-\u003eoo} (1/n) * Sum_{k=1..n} a(k) = A021002. (End)"
			],
			"maple": [
				"with(combinat): readlib(ifactors): for n from 1 to 120 do ans := 1: for i from 1 to nops(ifactors(n)[2]) do ans := ans*numbpart(ifactors(n)[2][i][2]) od: printf(`%d,`,ans): od: # _James A. Sellers_, Dec 07 2000"
			],
			"mathematica": [
				"f[n_] := Times @@ PartitionsP /@ Last /@ FactorInteger@n; Array[f, 107] (* _Robert G. Wilson v_, Sep 22 2006 *)",
				"Table[FiniteAbelianGroupCount[n], {n, 200}] (* Requires version 7.0 or later. - _Vladimir Joseph Stephan Orlovsky_, Jul 01 2011 *)"
			],
			"program": [
				"(PARI) A000688(n) = {local(f);f=factor(n);prod(i=1,matsize(f)[1],numbpart(f[i,2]))} \\\\ _Michael B. Porter_, Feb 08 2010",
				"(PARI) a(n)=my(f=factor(n)[,2]); prod(i=1,#f,numbpart(f[i])) \\\\ _Charles R Greathouse IV_, Apr 16 2015",
				"(Sage)",
				"def a(n):",
				"    F=factor(n)",
				"    return prod([number_of_partitions(F[i][1]) for i in range(len(F))])",
				"# _Ralf Stephan_, Jun 21 2014",
				"(Haskell)",
				"a000688 = product . map a000041 . a124010_row",
				"-- _Reinhard Zumkeller_, Aug 28 2014"
			],
			"xref": [
				"Cf. A000001, A021002, A060689, A000041, A000961, A001055, A034382, A046054, A046055, A046056, A050360, A055653, A057521, A101872 (bisection), A101876 (quadrisection), A124010, A050361, A051532, A129667 (Dirichlet inverse)."
			],
			"keyword": "nonn,core,easy,nice,mult",
			"offset": "1,4",
			"author": "_N. J. A. Sloane_",
			"references": 85,
			"revision": 163,
			"time": "2020-11-02T02:42:52-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}