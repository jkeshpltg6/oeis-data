{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A257042",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 257042,
			"data": "0,10,52,144,304,550,900,1372,1984,2754,3700,4840,6192,7774,9604,11700,14080,16762,19764,23104,26800,30870,35332,40204,45504,51250,57460,64152,71344,79054,87300,96100,105472,115434,126004,137200,149040,161542,174724",
			"name": "a(n) = (3*n+7)*n^2.",
			"comment": [
				"Consider a natural number r such that r has 15 proper divisors and 5 prime factors (note that these prime factors do not have to be distinct). The difference between these two values, say d(r), is in this case 10. Where n is a positive integer, d(r^n)=(3*n+7)*n^2."
			],
			"link": [
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-6,4,-1)."
			],
			"formula": [
				"G.f.: x*(10+12*x-4*x^2)/(1-x)^4. - _Vincenzo Librandi_, Apr 15 2015",
				"a(n) = 4*a(n-1) - 6*a(n-2) + 4*a(n-3) - a(n-4) for n\u003e3. - _Vincenzo Librandi_, Apr 15 2015"
			],
			"example": [
				"The smallest integer that satisfies this is 120: it has 15 proper divisors (1, 2, 3, 4, 5, 6, 8, 10, 12, 15, 20, 24, 30, 40, 60) and 5 prime factors (2, 2, 2, 3, 5), so d(120)=10. The square of 120, 14400, we would expect to have a difference of 52 between the number of its proper divisors and prime factors, and with respectively 62 and 10, d(120)=52 indeed. Checking this with further integer powers of 120 will continue to generate terms in this sequence.",
				"The integers which satisfy the proper-divisor-prime-factor requirement are those of A189975."
			],
			"maple": [
				"A257042:=n-\u003e(3*n+7)*n^2: seq(A257042(n), n=0..50); # _Wesley Ivan Hurt_, Apr 16 2015"
			],
			"mathematica": [
				"Table[(3 n + 7) n^2, {n, 40}] (* or *) CoefficientList[Series[(10 + 12 x - 4 x^2) / (1 - x)^4, {x, 0, 40}], x] (* _Vincenzo Librandi_, Apr 15 2015 *)"
			],
			"program": [
				"(MAGMA) [(3*n+7)*n^2: n in [0..65]]  // _Vincenzo Librandi_, Apr 15 2015",
				"(PARI) lista(nn) = {v = 1; while(!((numdiv(v)-1 == 15) \u0026\u0026 (bigomega(v) == 5)), v++); for (n=0, nn, vn = v^n; nb = numdiv(vn)-1-bigomega(vn); print1(nb, \", \"););} \\\\ _Michel Marcus_, Apr 16 2015"
			],
			"xref": [
				"Cf. A189975."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Garrett Frandson_, Apr 14 2015",
			"ext": [
				"More terms from _Vincenzo Librandi_, Apr 15 2015"
			],
			"references": 1,
			"revision": 29,
			"time": "2015-06-13T00:55:36-04:00",
			"created": "2015-04-23T13:40:26-04:00"
		}
	]
}