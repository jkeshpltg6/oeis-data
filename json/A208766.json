{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A208766",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 208766,
			"data": "1,1,3,1,6,7,1,9,21,19,1,12,42,76,47,1,15,70,190,235,123,1,18,105,380,705,738,311,1,21,147,665,1645,2583,2177,803,1,24,196,1064,3290,6888,8708,6424,2047,1,27,252,1596,5922,15498,26124,28908,18423",
			"name": "Triangle of coefficients of polynomials v(n,x) jointly generated with A208765; see the Formula section.",
			"comment": [
				"For a discussion and guide to related arrays, see A208510.",
				"Subtriangle of the triangle given by (1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (0, 3, -2/3, -4/3, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Mar 20 2012"
			],
			"formula": [
				"u(n,x) = u(n-1,x) + 2x*v(n-1,x),",
				"v(n,x) = 2x*u(n-1,x) + (x+1)*v(n-1,x),",
				"where u(1,x)=1, v(1,x)=1.",
				"From _Philippe Deléham_, Mar 20 2012: (Start)",
				"As DELTA-triangle with 0 \u003c= k \u003c= n:",
				"G.f.: (1-x-y*x+3*y*x^2-4*y^2*x^2)/(1-2*x-y*x+x^2+y*x^2-4*y^2*x^2).",
				"T(n,k) = 2*T(n-1,k) + T(n-1,k-1) - T(n-2,k) - T(n-2,k-1) + 4*T(n-2,k-2), T(0,0) = T(1,0) = T(2,0) = 1, T(1,1) = T(2,2) = 0, T(2,1) = 3 and T(n,k) = 0 if k \u003c 0 or if k \u003e n. (End)"
			],
			"example": [
				"First five rows:",
				"  1;",
				"  1,  3;",
				"  1,  6,  7;",
				"  1,  9, 21, 19;",
				"  1, 12, 42, 76, 47;",
				"First five polynomials v(n,x):",
				"  1",
				"  1 +  3x",
				"  1 +  6x +  7x^2",
				"  1 +  9x + 21x^2 + 19x^3",
				"  1 + 12x + 42x^2 + 76x^3 + 47x^4",
				"From _Philippe Deléham_, Mar 20 2012: (Start)",
				"(1, 0, 0, 1, 0, 0, ...) DELTA (0, 3, -2/3, -4/3, 0, 0, ...) begins:",
				"  1;",
				"  1,  0;",
				"  1,  3,  0;",
				"  1,  6,  7,  0;",
				"  1,  9, 21, 19,  0;",
				"  1, 12, 42, 76, 47,  0; (End)"
			],
			"mathematica": [
				"u[1, x_] := 1; v[1, x_] := 1; z = 16;",
				"u[n_, x_] := u[n - 1, x] + 2 x*v[n - 1, x];",
				"v[n_, x_] := 2 x*u[n - 1, x] + (x + 1) v[n - 1, x];",
				"Table[Expand[u[n, x]], {n, 1, z/2}]",
				"Table[Expand[v[n, x]], {n, 1, z/2}]",
				"cu = Table[CoefficientList[u[n, x], x], {n, 1, z}];",
				"TableForm[cu]",
				"Flatten[%]     (* A208765 *)",
				"Table[Expand[v[n, x]], {n, 1, z}]",
				"cv = Table[CoefficientList[v[n, x], x], {n, 1, z}];",
				"TableForm[cv]",
				"Flatten[%]     (* A208766 *)"
			],
			"xref": [
				"Cf. A208765, A208510."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Clark Kimberling_, Mar 02 2012",
			"references": 3,
			"revision": 18,
			"time": "2020-01-24T03:27:43-05:00",
			"created": "2012-03-02T14:58:06-05:00"
		}
	]
}