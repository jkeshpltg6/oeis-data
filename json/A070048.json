{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A070048",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 70048,
			"data": "1,1,1,2,1,2,3,3,4,5,6,7,8,9,11,13,16,18,21,24,27,32,36,41,48,54,61,70,78,88,100,112,127,143,159,179,199,222,248,276,308,342,380,421,465,516,570,629,697,767,845,932,1022,1124,1236,1355,1488,1631,1785,1954,2136",
			"name": "Number of partitions of n into odd parts in which no part appears more than thrice.",
			"comment": [
				"Also number of partitions of n into distinct parts in which no part is multiple of 4. - _Vladeta Jovovic_, Jul 31 2004",
				"McKay-Thompson series of class 64a for the Monster group.",
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A070048/b070048.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e (terms 0..220 from Reinhard Zumkeller)",
				"G. E. Andrews and R. P. Lewis, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(00)00295-8\"\u003eAn algebraic identity of F. H. Jackson and its implications for partitions\u003c/a\u003e, Discrete Math., 232 (2001), 77-83.",
				"Cristina Ballantine and Mircea Merca, \u003ca href=\"https://arxiv.org/abs/2111.10702\"\u003e4-Regular partitions and the pod function\u003c/a\u003e, arXiv:2111.10702 [math.CO], 2021.",
				"D. Ford, J. McKay and S. P. Norton, \u003ca href=\"http://dx.doi.org/10.1080/00927879408825127\"\u003eMore on replicable functions\u003c/a\u003e, Commun. Algebra 22, No. 13, 5175-5193 (1994).",
				"M. D. Hirschhorn, J. A. Sellers, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Sellers/sellers32.html\"\u003eA Congruence Modulo 3 for Partitions into Distinct Non-Multiples of Four\u003c/a\u003e, Article 14.9.6, Journal of Integer Sequences, Vol. 17 (2014).",
				"Vaclav Kotesovec, \u003ca href=\"http://arxiv.org/abs/1509.08708\"\u003eA method of finding the asymptotics of q-series based on the convolution of generating functions\u003c/a\u003e, arXiv:1509.08708 [math.CO], Sep 30 2015, p. 12.",
				"\u003ca href=\"/index/Mat#McKay_Thompson\"\u003eIndex entries for McKay-Thompson series for Monster simple group\u003c/a\u003e"
			],
			"formula": [
				"G.f.: Product_{i\u003e0} (1+x^i)/(1+x^(4*i)). - _Vladeta Jovovic_, Jul 31 2004",
				"Expansion of chi(x) * chi(x^2) = psi(x) / psi(-x^2) = phi(-x^4) / psi(-x) = chi(-x^4) / chi(-x) in powers of x where phi(), psi(), chi() are Ramanujan theta functions. - _Michael Somos_, Jul 01 2014",
				"Expansion of q^(1/8) * eta(q^2) * eta(q^4) / (eta(q) * eta(q^8)) in powers of q.",
				"Euler transform of period 8 sequence [1, 0, 1, -1, 1, 0, 1, 0, ...].",
				"Given g.f. A(x), then B(q) = A(q^8) / q satisifes 0 = f(B(q), B(q^3)) where f(u, v) = (u - v^3) * (u^3 - v) + 3*u*v. - _Michael Somos_, Jul 01 2014",
				"G.f.: Product_{k\u003e0} (1 - x^(8*k - 4)) / (1 - x^(2*k - 1)).",
				"a(n) ~ exp(sqrt(n)*Pi/2) / (4*n^(3/4)) * (1 - (3/(4*Pi) + Pi/32) / sqrt(n)). - _Vaclav Kotesovec_, Aug 31 2015, extended Jan 21 2017"
			],
			"example": [
				"G.f. = 1 + x + x^2 + 2*x^3 + x^4 + 2*x^5 + 3*x^6 + 3*x^7 + 4*x^8 + 5*x^9 + ...",
				"T64a = 1/q + q^7 + q^15 + 2*q^23 + q^31 + 2*q^39 + 3*q^47 + 3*q^55 + 4*q^63 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ -x, x^2] QPochhammer[ -x^2, x^4]), {x, 0, n}]; (* _Michael Somos_, Jul 01 2014 *)",
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x^2] QPochhammer[ x^4] / (QPochhammer[ x] QPochhammer[ x^8]), {x, 0, n}]; (* _Michael Somos_, Jul 01 2014 *)"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c0, 0 ,A = x * O(x^n); polcoeff( eta(x^2 + A) * eta(x^4 + A) / (eta(x + A) * eta(x^8 + A)), n))};",
				"(Haskell)",
				"a070048 = p a042968_list where",
				"   p _      0 = 1",
				"   p (k:ks) m = if m \u003c k then 0 else p ks (m - k) + p ks m",
				"-- _Reinhard Zumkeller_, Oct 01 2012"
			],
			"xref": [
				"Cf. A042968, A001935, A261734.",
				"Cf. A000700 (m=2), A003105 (m=3), A096938 (m=5), A261770 (m=6), A097793 (m=7), A261771 (m=8), A112193 (m=9), A261772 (m=10)."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_, May 09 2002",
			"ext": [
				"Additional comments from _Michael Somos_, Dec 04 2002"
			],
			"references": 17,
			"revision": 55,
			"time": "2021-11-23T09:42:45-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}