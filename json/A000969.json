{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000969",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 969,
			"id": "M2630 N1042",
			"data": "1,3,7,12,18,26,35,45,57,70,84,100,117,135,155,176,198,222,247,273,301,330,360,392,425,459,495,532,570,610,651,693,737,782,828,876,925,975,1027,1080,1134,1190,1247,1305,1365,1426,1488,1552,1617,1683,1751,1820,1890",
			"name": "Expansion of (1+x+2*x^2)/((1-x)^2*(1-x^3)).",
			"comment": [
				"From _Paul Curtz_, Oct 07 2018: (Start)",
				"Terms that are on the x-axis of the following spiral (without 0):",
				"     28--29--29--30--31--31--32",
				"      |",
				"     27  13--14--15--15--16--17",
				"      |   |                   |",
				"     27  13   4---5---5---6  17",
				"      |   |   |           |   |",
				"     26  12   3   0---1   7  18",
				"      |   |   |       |   |   |",
				"     25  11   3---2---1   7  19",
				"      |   |               |   |",
				"     25  11--10---9---9---8  19",
				"      |                       |",
				"     24--23--23--22--21--21--20  (End)",
				"Diagonal 1, 4, 8, 13, 20, 28, ... (without 0) is A143978. - _Bruno Berselli_, Oct 08 2018"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A000969/b000969.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"David Applegate, Omar E. Pol and N. J. A. Sloane, \u003ca href=\"/A000695/a000695_1.pdf\"\u003eThe Toothpick Sequence and Other Sequences from Cellular Automata\u003c/a\u003e, Congressus Numerantium, Vol. 206 (2010), 157-191. [There is a typo in Theorem 6: (13) should read u(n) = 4.3^(wt(n-1)-1) for n \u003e= 2.]",
				"R. P. Loh, A. G. Shannon, A. F. Horadam, \u003ca href=\"/A000969/a000969.pdf\"\u003eDivisibility Criteria and Sequence Generators Associated with Fermat Coefficients\u003c/a\u003e, Preprint, 1980.",
				"P. A. Piza, \u003ca href=\"http://www.jstor.org/stable/3029103\"\u003eFermat coefficients\u003c/a\u003e, Math. Mag., 27 (1954), 141-146.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"N. J. A. Sloane, \u003ca href=\"/wiki/Catalog_of_Toothpick_and_CA_Sequences_in_OEIS\"\u003eCatalog of Toothpick and Cellular Automata Sequences in the OEIS\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,-1,1,-2,1)."
			],
			"formula": [
				"a(n) = floor( (2*n+3)*(n+1)/3 ). Or, a(n) = (2*n+3)*(n+1)/3 but subtract 1/3 if n == 1 mod 3. - _N. J. A. Sloane_, May 05 2010.",
				"a(2^k-2) = A139250(2^k-1), k \u003e= 1. - _Omar E. Pol_, Feb 13 2010",
				"a(n) = Sum_{i=0..n} floor(4*i/3). - _Enrique Pérez Herrero_, Apr 21 2012",
				"a(n) = +2*a(n-1) -1*a(n-2) +1*a(n-3) -2*a(n-4) +1*a(n-5). - _Joerg Arndt_, Apr 22 2012",
				"a(n) = A014105(n+1) = A258708(n+3,n). - _Reinhard Zumkeller_, Jun 23 2015"
			],
			"maple": [
				"A000969:=-(1+z+2*z**2)/(z**2+z+1)/(z-1)**3; # _Simon Plouffe_ in his 1992 dissertation"
			],
			"mathematica": [
				"f[x_, y_] := Floor[ Abs[ y/x - x/y]]; Table[ f[3, 2 n^2 + n + 2], {n, 53}] (* _Robert G. Wilson v_, Aug 11 2010 *)",
				"CoefficientList[Series[(1+x+2*x^2)/((1-x)^2*(1-x^3)), {x, 0, 50}], x] (* _Stefano Spezia_, Oct 08 2018 *)"
			],
			"program": [
				"(Haskell)",
				"a000969 = flip div 3 . a014105 . (+ 1)  -- _Reinhard Zumkeller_, Jun 23 2015",
				"(PARI) a(n)=([0,1,0,0,0; 0,0,1,0,0; 0,0,0,1,0; 0,0,0,0,1; 1,-2,1,-1,2]^n*[1;3;7;12;18])[1,1] \\\\ _Charles R Greathouse IV_, May 10 2016"
			],
			"xref": [
				"Cf. A004773 (first differences), A092498 (partial sums).",
				"Cf. A139250, A160165, A258708, A014105."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 27,
			"revision": 77,
			"time": "2021-03-12T22:32:34-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}