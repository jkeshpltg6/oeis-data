{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A053028",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 53028,
			"data": "5,13,17,37,53,61,73,89,97,109,113,137,149,157,173,193,197,233,257,269,277,293,313,317,337,353,373,389,397,421,433,457,557,577,593,613,617,653,661,673,677,701,733,757,761,773,797,821,829,853,857,877,937,953",
			"name": "Odd primes p with 4 zeros in any period of the Fibonacci numbers mod p.",
			"comment": [
				"Also, primes that do not divide any Lucas number. - _T. D. Noe_, Jul 25 2003",
				"Although every prime divides some Fibonacci number, this is not true for the Lucas numbers. In fact, exactly 1/3 of all primes do not divide any Lucas number. See Lagarias and Moree for more details. The Lucas numbers separate the primes into three disjoint sets: (A053028) primes that do not divide any Lucas number, (A053027) primes that divide Lucas numbers of even index and (A053032) primes that divide Lucas numbers of odd index. - _T. D. Noe_, Jul 25 2003; revised by _N. J. A. Sloane_, Feb 21 2004"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A053028/b053028.txt\"\u003eTable of n, a(n) for n=1..1000\u003c/a\u003e",
				"C. Ballot and M. Elia, \u003ca href=\"http://www.fq.math.ca/Papers1/45-1/quartballot01_2007.pdf\"\u003eRank and period of primes in the Fibonacci sequence; a trichotomy\u003c/a\u003e, Fib. Quart., 45 (No. 1, 2007), 56-63 (The sequence B2).",
				"J. C. Lagarias, \u003ca href=\"http://projecteuclid.org/euclid.pjm/1102706452\"\u003eThe set of primes dividing the Lucas numbers has density 2/3\u003c/a\u003e, Pacific J. Math., 118. No. 2, (1985), 449-461.",
				"J. C. Lagarias, \u003ca href=\"http://projecteuclid.org/euclid.pjm/1102622818\"\u003eErrata to: The set of primes dividing the Lucas numbers has density 2/3\u003c/a\u003e, Pacific J. Math., 162, No. 2, (1994), 393-396.",
				"Diego Marques and Pavel Trojovsky, \u003ca href=\"https://doi.org/10.2478/tmmp-2014-0019\"\u003eThe order of appearance of the product of five consecutive Lucas numbers\u003c/a\u003e, Tatra Mountains Math. Publ. 59 (2014), 65-77.",
				"Pieter Moree, \u003ca href=\"http://msp.org/pjm/1998/186-2/p03.xhtml\"\u003eCounting Divisors of Lucas Numbers\u003c/a\u003e, Pacific J. Math, Vol. 186, No. 2, 1998, pp. 267-284.",
				"M. Renault, \u003ca href=\"http://webspace.ship.edu/msrenault/fibonacci/fib.htm\"\u003eFibonacci sequence modulo m\u003c/a\u003e",
				"H. Sedaghat, \u003ca href=\"https://www.fq.math.ca/Papers1/52-1/Sedaghat.pdf\"\u003eZero-Avoiding Solutions of the Fibonacci Recurrence Modulo A Prime\u003c/a\u003e, Fibonacci Quart. 52 (2014), no. 1, 39-45. See p. 45.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/LucasNumber.html\"\u003eLucas Number\u003c/a\u003e"
			],
			"formula": [
				"A prime p = prime(i) is in this sequence if p \u003e 2 and A001602(i) is odd. - _T. D. Noe_, Jul 25 2003"
			],
			"mathematica": [
				"Lucas[n_] := Fibonacci[n+1] + Fibonacci[n-1]; badP={}; Do[p=Prime[n]; k=1; While[k\u003cp\u0026\u0026Mod[Lucas[k], p]\u003e0, k++ ]; If[k==p, AppendTo[badP, p]], {n, 200}]; badP"
			],
			"xref": [
				"Cf. A001176.",
				"Cf. A000204 (Lucas numbers), A001602 (index of the smallest Fibonacci number divisible by prime(n)), A053027, A053032."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Henry Bottomley_, Feb 23 2000",
			"ext": [
				"Edited: Name clarified. Moree and Renault link updated. Ballot and Elia reference linked. - _Wolfdieter Lang_, Jan 20 2015"
			],
			"references": 30,
			"revision": 32,
			"time": "2021-05-14T12:16:38-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}