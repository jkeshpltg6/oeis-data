{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A052463",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 52463,
			"data": "0,47,2301,112747,5524601,270705447,13264566901,649963778147,31848225129201,1560563031330847,76467588535211501,3746911838225363547,183598680073042813801,8996335323579097876247",
			"name": "a(n) is the smallest nonnegative solution k to 24*k == 1 (mod 7^(2*n-2)).",
			"comment": [
				"Related to a Ramanujan congruence for the partition function P = A000041.",
				"In other words, a(n) = k such that 24*k (mod 7^(2*n-2) ) == 1. - _N. J. A. Sloane_, Oct 08 2019",
				"If b(n) = a(n) + 7^(2*n-2)*r, where r is a nonnegative integer, then there is an integer s \u003e= 0 such that 24*b(n) = 24*a(n) + 24*7^(2*n-2)*r = 7^(2*n-2)*s + 1 + 24*7^(2*n-2)*r = 7^(2*n-2)*(24*r+s) + 1 == 1 (mod 7^(2*n-2)). Thus, we insist that a(n) is the smallest k \u003e= 0 such that 24*k == 1 (mod 7^(2*n-2)). - _Petros Hadjicostas_, Oct 09 2019"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A052463/b052463.txt\"\u003eTable of n, a(n) for n = 1..600\u003c/a\u003e",
				"G. K. Patil, \u003ca href=\"https://web.archive.org/web/20180422092157/http://www.ijsres.com/2014/vol-1_issue-6/paper_8.pdf\"\u003eRamanujan's Life And His Contributions In The Field Of Mathematics\u003c/a\u003e, International Journal of Scientific Research and Engineering Studies (IJSRES), 1(6) (2014), ISSN: 2349-8862.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PartitionFunctionPCongruences.html\"\u003ePartition Function P Congruences\u003c/a\u003e.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (50,-49)."
			],
			"formula": [
				"G.f.: x^2*(-49*x + 47)/((1 - x)*(1 - 49*x)).",
				"a(n) = 49*a(n-1) - 2. - _Vincenzo Librandi_, Jul 01 2012",
				"a(n) = 23*49^n/1176 + 1/24, n \u003e 1. - _R. J. Mathar_, Oct 09 2019"
			],
			"mathematica": [
				"Table[PowerMod[24, -1, 7^(2b-2)], {b, 20}]",
				"CoefficientList[Series[(-49x^2+47x)/((1-x)(1-49x)),{x,0,30}],x] (* _Vincenzo Librandi_, Jul 01 2012 *)",
				"LinearRecurrence[{50,-49},{0,47,2301},20] (* _Harvey P. Dale_, Aug 23 2021 *)"
			],
			"program": [
				"(MAGMA) I:=[0, 47]; [n le 2 select I[n] else 49*Self(n-1)-2: n in [1..20]]; // _Vincenzo Librandi_, Jul 01 2012"
			],
			"xref": [
				"Cf. A000041, A052462, A052465, A052466, A327770."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Eric W. Weisstein_",
			"ext": [
				"Name edited by _Petros Hadjicostas_, Oct 09 2019"
			],
			"references": 6,
			"revision": 51,
			"time": "2021-08-23T18:45:44-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}