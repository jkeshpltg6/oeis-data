{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005840",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5840,
			"id": "M1872",
			"data": "1,1,2,8,46,332,2874,29024,334982,4349492,62749906,995818760,17239953438,323335939292,6530652186218,141326092842416,3262247252671414,80009274870905732,2077721713464798210,56952857434896699992,1643312099715631960910",
			"name": "Expansion of (1-x)*e^x/(2-e^x).",
			"comment": [
				"Also number of distinct resistances possible for n arbitrary resistors each connected in series or parallel with previous ones (cf. A051045).",
				"The n-th term of A051045 uses the n different resistances 1, ..., n ohms, whereas the problem corresponding to A005840 allows arbitrary general resistances a1, a2, ..., an, chosen so as to give the maximum possible number of distinct equivalent resistances - Eric Weisstein",
				"Stanley's Problem 5.4(a) involves threshold graphs; Problem 5.4(c) involves hyperplane arrangements.",
				"a(n) is the number of labeled threshold graphs on n vertices. [This is more specific than the reference to Stanley.] [_Svante Janson_, Apr 01 2009]",
				"If circuits were allowed that combine complex subcircuits in series or parallel, rather than requiring that one of them consists of a single resistor, then there are more additional possible resistances. For n = 4, there are additional 6 possible values. See illustration in links. - _Kival Ngaokrajang_, Aug 26 2013 (rephrased by _Dave R.M. Langers_, Nov 13 2013)",
				"Conjecture: A285868 (with offset 1) shows the associated connected threshold graphs. - _R. J. Mathar_, Apr 29 2019",
				"Re: above conjecture - the number of connected threshold graphs on n labeled vertices is A317057 (see also A053525). [_David Galvin_, Oct 18 2021]"
			],
			"reference": [
				"Miklos Bona, editor, Handbook of Enumerative Combinatorics, CRC Press, 2015, p. 417.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"R. P. Stanley, Enumerative Combinatorics, Cambridge, Vol. 2, 1999; see Problem 5.4(a)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A005840/b005840.txt\"\u003eTable of n, a(n) for n=0..100\u003c/a\u003e",
				"J. S. Beissinger and U. N. Peled, \u003ca href=\"http://dx.doi.org/10.1007/BF01788543\"\u003eEnumeration of labelled threshold graphs and a theorem of Frobenius involving Eulerian polynomials\u003c/a\u003e, J Graphs Combin., 3 (1987), 213--219. MR903610 [From _Svante Janson_, Apr 01 2009]",
				"Chao-Ping Chen and Xue-Feng Han, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2016.02.018\"\u003eOn Somos' quadratic recurrence constant\u003c/a\u003e, J. Number Theory, 166 (2016) 31-40. See page 34 equation (2.3).",
				"P. Diaconis, S. Holmes and S. Janson, \u003ca href=\"https://doi.org/10.1080/15427951.2008.10129166\"\u003eThreshold graph limits and random threshold graphs\u003c/a\u003e, Internet. math 5 (3) (2008) 267-320.",
				"D. Galvin, G. Wesley and B. Zacovic, \u003ca href=\"https://arxiv.org/abs/2110.08953\"\u003eEnumerating threshold graphs and some related graph classes\u003c/a\u003e, arXiv:2110.08953 [math.CO], 2021.",
				"Venkatesan Guruswami, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(99)00022-9\"\u003eEnumerative aspects of certain subclasses of perfect graphs\u003c/a\u003e, Discrete Math. 205 (1999), 97-117.",
				"Andrew H. Hoefel and Jeff Mermin, \u003ca href=\"http://projecteuclid.org/euclid.ijm/1385129955\"\u003eGotzmann squarefree ideals\u003c/a\u003e, Ill. J. Math. 56, No. 2, 397-414 (2012), Proposition 3.13.",
				"Ricky I. Liu, K. Mészáros and A. H. Morales, \u003ca href=\"http://arxiv.org/abs/1610.08370\"\u003eFlow polytopes and the space of diagonal harmonics\u003c/a\u003e, arXiv preprint arXiv:1610.08370 [math.CO], 2016.",
				"Kival Ngaokrajang, \u003ca href=\"/A005840/a005840.pdf\"\u003eIllustration for n = 4; [a1, a2, a3, a4] = [3, 5, 7, 9]\u003c/a\u003e",
				"Seunghyun Seo, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL20/Seo/seo2.html\"\u003eThe Catalan Threshold Arrangement\u003c/a\u003e, Journal of Integer Sequences, 2017 Vol. 20, #17.1.1.",
				"Sam Spiro, \u003ca href=\"https://arxiv.org/abs/1909.06518\"\u003eCounting Threshold Graphs with Eulerian Numbers\u003c/a\u003e, arXiv:1909.06518 [math.CO], 2019.",
				"R. P. Stanley, \u003ca href=\"http://dedekind.mit.edu/~rstan/pubs/pubfiles/83.pdf\"\u003eA zonotope associated with graphical degree sequences\u003c/a\u003e, in Applied Geometry and Discrete Combinatorics. DIMACS Series in Discrete Math., Amer. Math. Soc., Vol. 4, pp. 555-570, 1991.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ResistorNetwork.html\"\u003eResistor Network\u003c/a\u003e"
			],
			"formula": [
				"a(n) ~ n! * (1-log(2)) / (log(2))^(n+1). - _Vaclav Kotesovec_, Sep 29 2014",
				"E.g.f.: (1 - x) * e^x / (2 - e^x).",
				"E.g.f. A(x) satisfies (1 - x) * A'(x) = A(x) * (A(x) - x). - _Michael Somos_, Aug 01 2016",
				"a(n+1) = n*(a(n) - a(n-1)) + Sum_{k=0..n} binomial(n, k) * a(k) * a(n-k). - _Michael Somos_, Aug 01 2016",
				"a(n) = (1-n) + Sum_{k=0..n-1} binomial(n, k) * a(k). - _Michael Somos_, Aug 01 2016",
				"BINOMIAL transform of A053525. - _Michael Somos_, Aug 01 2016",
				"a(n) = Sum_{k=1..n-1} (n-k)*A008292(n-1,k-1)*2^k, for n\u003e=2. - _Sam Spiro_, Sep 22 2019"
			],
			"example": [
				"exp(x)*(1-x)/(2-exp(x)) = 1 + x + x^2 + 4/3*x^3 + 23/12*x^4 + 83/30*x^5 + 479/120*x^6 + 1814/315*x^7 + O(x^8); then the coefficients are multiplied by n! to get 1, 1, 2, 8, 46, 332, 2874, 29024, ..."
			],
			"maple": [
				"A005840 := proc(n) option remember;",
				"1 - n + add(binomial(n, k) * A005840(k), k = 0..n-1) end:",
				"seq(A005840(n), n = 0..20); # _Peter Luschny_, Oct 25 2021"
			],
			"mathematica": [
				"nn = 20; Range[0, nn]! CoefficientList[Series[(1 - x) Exp[x]/(2 - Exp[x]), {x, 0, nn}], x] (* _Harvey P. Dale_, Jul 20 2011 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^30)); Vec(serlaplace((1-x)*exp(x)/(2-exp(x)))); \\\\ _Michel Marcus_, Jan 04 2016"
			],
			"xref": [
				"2*A053525(n), n\u003e1."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,3",
			"author": "_Simon Plouffe_",
			"references": 9,
			"revision": 94,
			"time": "2021-10-25T07:14:41-04:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}