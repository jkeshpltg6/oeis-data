{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A177405",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 177405,
			"data": "0,1,0,1,2,1,0,1,2,1,4,5,2,5,4,1,0,1,2,1,4,5,2,5,4,1,2,3,4,13,14,5,4,3,2,9,12,5,14,13,4,9,6,1,0,1,2,1,4,5,2,5,4,1,2,3,4,13,14,5,4,3,2,9,12,5,14,13,4,9,6,1,4,5,2,7,8,3,10,11",
			"name": "Form triangle of weighted Farey fractions; read numerators by rows.",
			"comment": [
				"Start with the list of fractions 0/1, 1/1 and repeatedly insert the weighted mediants (2a+c)/(2b+d) and (a+2c)/(b+2d) between every pair of adjacent elements a/b and c/d of the list. The fractions are to be reduced before the insertion step.",
				"James Propp asks: Does every fraction between 0 and 1 with odd denominator appear in the triangle?"
			],
			"reference": [
				"James Propp, Posting to the Math Fun Mailing List, Dec 10 2010."
			],
			"link": [
				"Nathaniel Johnston, \u003ca href=\"/A177405/b177405.txt\"\u003eTable of n, a(n) for n = 0..29533\u003c/a\u003e (first 10 rows of triangle)",
				"Dhroova Aiylam, Tanya Khovanova, \u003ca href=\"https://arxiv.org/abs/1711.01475\"\u003eWeighted Mediants and Fractals\u003c/a\u003e, arXiv:1711.01475 [math.NT], 2017."
			],
			"example": [
				"Triangle begins:",
				"0 1",
				"- -",
				"1 1",
				"0 1 2 1",
				"- - - -",
				"1 3 3 1",
				"0 1 2 1 4 5 2 5 4 1",
				"- - - - - - - - - -",
				"1 5 7 3 9 9 3 7 5 1",
				"0 1 .2 1 .4 .5 2 .5 .4 1 2 3 4 13 14 5 4 3 2 .9 12 5 14 13 4 .9 6 1",
				"- - -- - -- -- - -- -- - - - - -- -- - - - - -- -- - -- -- - -- - -",
				"1 7 11 5 17 19 7 17 13 3 5 7 9 27 27 9 7 5 3 13 17 7 19 17 5 11 7 1"
			],
			"mathematica": [
				"Mma code from _James Propp_:",
				"        Lengthen[L_] :=",
				"         Module[{i, M}, M = Table[0, {3 Length[L]}];",
				"          M[[1]] = Numerator[L[[1]]]/(2 + Denominator[L[[1]]]);",
				"          M[[2]] = 2*Numerator[L[[1]]]/(1 + 2 Denominator[L[[1]]]);",
				"          For[i = 1, i \u003c Length[L], i++, M[[3 i]] = L[[i]];",
				"           M[[3 i + 1]] = (2 Numerator[L[[i]]] +",
				"               Numerator[L[[i + 1]]])/(2 Denominator[L[[i]]] +",
				"               Denominator[L[[i + 1]]]);",
				"           M[[3 i + 2]] = (Numerator[L[[i]]] +",
				"               2 Numerator[L[[i + 1]]])/(Denominator[L[[i]]] +",
				"               2 Denominator[L[[i + 1]]])]; M[[3 Length[L]]] = L[[Length[L]]];",
				"           Return[M]]",
				"        WF[n_] := WF[n] = If[n == 0, {1}, Lengthen[WF[n - 1]]]"
			],
			"xref": [
				"Cf. A177407, A177903, A006842/A006843."
			],
			"keyword": "nonn,frac,tabf,easy",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_, Dec 10 2010",
			"ext": [
				"a(45)-a(80) and some corrected terms from _Nathaniel Johnston_, Apr 12 2011"
			],
			"references": 6,
			"revision": 34,
			"time": "2018-05-07T03:37:48-04:00",
			"created": "2010-11-12T14:25:51-05:00"
		}
	]
}