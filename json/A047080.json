{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A047080",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 47080,
			"data": "1,1,1,1,1,1,1,2,2,1,1,3,3,3,1,1,4,5,5,4,1,1,5,8,9,8,5,1,1,6,12,15,15,12,6,1,1,7,17,24,27,24,17,7,1,1,8,23,37,46,46,37,23,8,1,1,9,30,55,75,83,75,55,30,9,1,1,10,38,79,118,143,143,118,79,38,10,1",
			"name": "Triangular array T read by rows: T(h,k)=number of paths from (0,0) to (k,h-k) using step-vectors (0,1), (1,0), (1,1) with no right angles between pairs of consecutive steps.",
			"comment": [
				"T(n,k) equals the number of reduced alignments between a string of length n and a string of length k. See Andrade et. al. - _Peter Bala_, Feb 04 2018"
			],
			"link": [
				"Muniru A Asiru, \u003ca href=\"/A047080/b047080.txt\"\u003eTable of n, a(n) for n = 0..1325\u003c/a\u003e",
				"H. Andrade, I. Area, J. J. Nieto, A. Torres, \u003ca href=\"https://doi.org/10.1186/1471-2105-15-94\"\u003eThe number of reduced alignments between two DNA sequences\u003c/a\u003e BMC Bioinformatics (2014) Vol. 15: 94."
			],
			"formula": [
				"T(h, k) = T(h-1, k-1) + T(h-1, k) - T(h-4, k-2);",
				"Writing T(h, k) = F(h-k, k), generating function for F is (1-xy)/(1-x-y+x^2y^2).",
				"From _Peter Bala_, Feb 04 2018: (Start)",
				"T(n,k) = Sum_{i = 0..A} (-1)^i*(n+k-3*i)!/((i!*(n-2*i)!*(k-2*i)!) - Sum_{i = 0..B} (-1)^i*(n+k-3*i-2)!/((i!*(n-2*i-1)!*(k-2*i-1)!), where A = min{floor(n/2), floor(k/2)} and B = min{floor((n-1)/2), floor((k-1)/2)}.",
				"T(2*n,n) appears to be A171155(n). (End)"
			],
			"example": [
				"E.g., row 3 consists of T(3,0)=1; T(3,1)=2; T(3,2)=2; T(3,3)=1.",
				"Triangle begins:",
				"  1;",
				"  1,1;",
				"  1,1,1;",
				"  1,2,2,1;",
				"  1,3,3,3,1;",
				"  ..."
			],
			"maple": [
				"T := proc(n, k) option remember; if n \u003c 0 or k \u003e n then return 0 fi;",
				"if n \u003c 3 then return 1 fi; if k \u003c iquo(n,2) then return T(n, n-k) fi;",
				"T(n-1, k-1) + T(n-1, k) - T(n-4, k-2)  end:",
				"seq(seq(T(n,k), k=0..n), n=0..11); # _Peter Luschny_, Feb 11 2018"
			],
			"mathematica": [
				"T[n_, k_] := T[n, k] = Which[n\u003c0 || k\u003en, 0, n\u003c3, 1, k\u003cQuotient[n, 2],   T[n, n-k], True, T[n-1, k-1] + T[n-1, k] - T[n-4, k-2]];",
				"Table[T[n, k], {n, 0, 11}, { k, 0, n}] // Flatten (* _Jean-François Alcover_, Jul 30 2018 *)"
			],
			"xref": [
				"Cf. A171155."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,8",
			"author": "_Clark Kimberling_",
			"ext": [
				"Sequence recomputed to correct terms from 23rd onward, and recurrence and generating function added by Michael L. Catalano-Johnson (mcj(AT)pa.wagner.com), Jan 14 2000"
			],
			"references": 9,
			"revision": 28,
			"time": "2018-07-30T03:30:59-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}