{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A280026",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 280026,
			"data": "0,1,2,3,3,4,4,5,6,5,6,7,6,7,8,9,7,8,9,10,8,9,10,11,12,9,10,11,12,13,10,11,12,13,14,15,11,12,13,14,15,16,12,13,14,15,16,17,18,13,14,15,16,17,18,19,14,15,16,17,18,19,20,21,15,16,17,18,19,20,21",
			"name": "Fill an infinite square array by following a spiral around the origin; in the n-th cell, enter the number of earlier cells that can be seen from that cell.",
			"comment": [
				"The spiral track being used here is the same as in A274640, except that the starting cell here is numbered 0 (as in A274641).",
				"\"Can be seen from\" means \"are on the same row, column, diagonal, or antidiagonal as\".",
				"The entry in a cell gives the number of earlier cells that are occupied in any of the eight cardinal directions. - _Robert G. Wilson v_, Dec 25 2016",
				"First occurrence of k = 0,1,2,3,...: 0, 1, 2, 3, 5, 7, 8, 11, 14, 15, 19, 23, 24, 29, 34, 35, 41, 47, 48, 55, 62, ... - _Robert G. Wilson v_, Dec 25 2016"
			],
			"link": [
				"Lars Blomberg, \u003ca href=\"/A280026/b280026.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"formula": [
				"Empirically: a(0)=0, a(n+1)=a(n)+d for n\u003e0, when n=k^2 or n=k*(k+1) then d=2-k, else d=1."
			],
			"example": [
				"The central portion of the spiral is:",
				".",
				"    7---9---8---7---6",
				"    |               |",
				"    8   3---3---2   7",
				"    |   |       |   |",
				"    9   4   0---1   6",
				"    |   |           |",
				"   10   4---5---6---5",
				"    |",
				"    8---9--10--11--12 ..."
			],
			"mathematica": [
				"a[n_] := a[n - 1] + If[ IntegerQ@ Sqrt@ n || IntegerQ@ Sqrt[4n +1], 2 - Select[{Sqrt@ n, (Sqrt[4n +1] -1)/2}, IntegerQ][[1]], 1]; a[0] = 0; Array[a, 76, 0] (* _Robert G. Wilson v_, Dec 25 2016 *)"
			],
			"xref": [
				"See A280027 for an additive version.",
				"Cf. A274640, A274641, A278354.",
				"See A279211, A279212 for versions that follow antidiagonals in just one quadrant."
			],
			"keyword": "nonn,tabl",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_, Dec 24 2016",
			"ext": [
				"Corrected a(23) and more terms from _Lars Blomberg_, Dec 25 2016"
			],
			"references": 4,
			"revision": 36,
			"time": "2016-12-28T13:50:29-05:00",
			"created": "2016-12-24T19:52:37-05:00"
		}
	]
}