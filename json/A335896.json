{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A335896",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 335896,
			"data": "8,8,15,15,21,21,35,35,40,40,48,48,55,55,65,65,77,77,80,80,91,91,96,96,99,99,112,112,117,117,119,119,133,133,143,143,153,153,160,160,171,171,168,168,187,187,176,176,209,209,207,207,221,221,224,224,225,225",
			"name": "Largest side of integer-sided primitive triangles whose angles A \u003c B \u003c C are in arithmetic order.",
			"comment": [
				"The triples of sides (a,b,c) with a \u003c b \u003c c are in nondecreasing order of middle side, and if middle sides coincide then by increasing order of the largest side. This sequence lists the c's.",
				"Equivalently, lengths of the largest side c of primitive non-equilateral triangles that have an angle of Pi/3; indeed, this side is opposite to the largest angle C.",
				"Also, solutions c of the Diophantine equation b^2 = a^2 - a*c + c^2 with gcd(a,b) = 1 and a \u003c b.",
				"For the corresponding primitive triples and miscellaneous properties and references, see A335893.",
				"When (a, b, c) is a triple, then (c-a, b, c) is another triple, so every c in the data is twice consecutively present according to the corresponding pair (b, c) (see examples).",
				"As B = Pi/3 and C runs from Pi/3 to 2*Pi/3, sin(C) gets a maximum when C = Pi/2 with sin(C) = 1, hence, from law of sinus, b/sin(B) = c/sin(C), c \u003c b/sin(Pi/3) = b * 2/sqrt(3) \u003c 6*b/5. This bound is used in PARI and Maple programs.",
				"This sequence is not increasing. For example, a(41) = a(42) = 171 for triangle with middle side = 151 while a(43) = a(44) = 168 for triangle with middle side = 157."
			],
			"reference": [
				"V. Lespinard \u0026 R. Pernet, Trigonométrie, Classe de Mathématiques élémentaires, programme 1962, problème B-298 p. 124, André Desvigne."
			],
			"link": [
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Law_of_sines\"\u003eLaw of sines\u003c/a\u003e."
			],
			"formula": [
				"a(n) = A335893(n, 3).",
				"c satisfies c^2 - a*c + a^2 - b^2 = 0 with gcd(a,b) = 1 and a \u003c b."
			],
			"example": [
				"c = 8 appears twice because:",
				"  7^2 = 3^2 - 3*8 + 8^2, with triple (3, 7, 8),",
				"  7^2 = 5^2 - 5*8 + 8^2, with triple (5, 7, 8).",
				"c = 96 and c = 99 each appear twice associated with b = 91 because:",
				"  91^2 = 11^2 - 11*96 + 96^2, with triple (11, 91, 96),",
				"  91^2 = 85^2 - 85*96 + 96^2, with triple (85, 91, 96),",
				"  91^2 = 19^2 - 19*99 + 99^2, with triple (19, 91, 99),",
				"  91^2 = 80^2 - 80*99 + 99^2, with triple (80, 91, 99)."
			],
			"maple": [
				"for b from 3 to 250 by 2 do",
				"for c from b+1 to 6*b/5 do",
				"a := (c - sqrt(4*b^2-3*c^2))/2;",
				"if gcd(a,b)=1 and issqr(4*b^2-3*c^2) then print(c,c); end if;",
				"end do;",
				"end do;"
			],
			"program": [
				"(PARI) lista(nn) = {forstep(b=1, nn, 2, for(c=b+1, 6*b\\5, if (issquare(d=4*b^2 - 3*c^2), my(a = (c - sqrtint(d))/2); if ((denominator(a)==1) \u0026\u0026 (gcd(a, b) == 1), print(c, \", \", c, \", \");););););} \\\\ _Michel Marcus_, Jul 15 2020"
			],
			"xref": [
				"Cf. A089025 (terms in increasing order without repetition).",
				"Cf. A335893 (triples), A335894 (smallest side), A335895 (middle side), this sequence (largest side), A335897 (perimeter)."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Bernard Schott_, Jul 10 2020",
			"references": 7,
			"revision": 31,
			"time": "2021-06-25T03:00:13-04:00",
			"created": "2020-07-15T11:38:08-04:00"
		}
	]
}