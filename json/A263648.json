{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A263648",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 263648,
			"data": "4,9,10,21,22,15,14,25,6,35,26,49,34,77,38,33,46,39,58,51,62,57,74,69,82,87,86,93,94,111,106,123,118,129,122,141,134,159,142,177,146,183,158,201,166,213,178,219,194,237,202,249,206,267,214,291,218,303,226,309",
			"name": "a(1)=4, a(2)=9: a(n) = smallest semiprime not yet appearing in the sequence which is coprime to a(n-1) and not coprime to a(n-2).",
			"comment": [
				"Contrary to what might be expected (see comment after Proof), a(n) is not a permutation of all semiprimes; it is a permutation of even semiprimes {S_2} and semiprimes with smallest factor 3 {S_3}, plus {25, 35, 49, 77}. Proof (Start):",
				"i. The sequence is infinite: we can always consider p*q for a(n+1), where p is the smallest factor in a(n-1) and q is the smallest prime \u003e than the largest factor of any term already appearing in the sequence;",
				"ii. a(15)=38 = 2*19 == 2*prime(8) and a(16)=33 = 3*11 == 3*prime(5);",
				"iii. all {S_2} \u003c= 38 and {S_3} \u003c= 33 have appeared up to a(16), with 38 and 33 being maximum terms in {S_2} and {S_3}, respectively;",
				"iv. all semiprimes with smallest factor \u003e=5 which are \u003c 2*prime(9)=46 and 3*prime(6)=39 have appeared up to a(16). Consequently, the terms starting at a(17)=46 alternate between 2*prime(k) and 3*prime(k-3) k=9..inf.",
				"v. the only other numbers to have appeared are {25, 35, 49, 77}.",
				"(End)",
				"The above behavior is in contrast to A119718 (a permutation of all semiprimes because it lacks the constraint of a(n) being not coprime to a(n-2)). Interestingly, this sequence (A263648) shares the same essential rules as A098550 (the Yellowstone permutation) and many of its variations, while A119718 does not; one therefore might expect the opposite behavior to occur between this sequence and A119718. What observations or generalizations might we draw from this?"
			],
			"link": [
				"David L. Applegate, Hans Havermann, Bob Selcoe, Vladimir Shevelev, N. J. A. Sloane, and Reinhard Zumkeller, \u003ca href=\"http://arxiv.org/abs/1501.01669\"\u003eThe Yellowstone Permutation\u003c/a\u003e, arXiv preprint arXiv:1501.01669 [math.NT], 2015. Also Journal of Integer Sequences, Vol. 18 (2015), Article 15.6.7"
			],
			"formula": [
				"For a(n) n\u003e=15:",
				"a(n) = 2*prime((n+1)/2) when n is odd;",
				"a(n) = 3*prime(n/2-3) when n is even."
			],
			"mathematica": [
				"a[1]=4; a[2]=9; a[n_] := a[n] = Module[{k}, For[k=6, True, k++, If[MatchQ[ FactorInteger[k], {{_, 1}, {_, 1}}|{{_, 2}}] \u0026\u0026 FreeQ[Array[a, n-1], k] \u0026\u0026 CoprimeQ[k, a[n-1]] \u0026\u0026 !CoprimeQ[k, a[n-2]], Return[k]]]]; Array[a, 60] (* _Jean-François Alcover_, Oct 06 2018  *)"
			],
			"xref": [
				"Cf. A098550, A119718, A000040 (primes), A100484 (even semiprimes), A001748 (essentially, semiprimes with smallest factor 3)."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Bob Selcoe_, Oct 22 2015",
			"ext": [
				"More terms from _Jean-François Alcover_, Oct 06 2018"
			],
			"references": 2,
			"revision": 21,
			"time": "2018-10-06T11:46:27-04:00",
			"created": "2015-10-25T16:06:54-04:00"
		}
	]
}