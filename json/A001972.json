{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001972",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1972,
			"id": "M0551 N0199",
			"data": "1,2,3,4,6,8,10,12,15,18,21,24,28,32,36,40,45,50,55,60,66,72,78,84,91,98,105,112,120,128,136,144,153,162,171,180,190,200,210,220,231,242,253,264,276,288,300,312,325,338,351,364,378,392,406,420,435,450,465",
			"name": "Expansion of 1/((1-x)^2*(1-x^4)) = 1/( (1+x)*(1+x^2)*(1-x)^3 ).",
			"comment": [
				"First differences are A008621. - _Amarnath Murthy_, Apr 26 2004",
				"a(n) = least k \u003e a(n-1) such that k + a(n-1) + a(n-2) + a(n-3) is triangular. - _Amarnath Murthy_, Apr 26 2004",
				"Column sums of the following array:",
				"1 2 3 4 5 6 7  8  9...",
				"        1 2 3  4  5...",
				"                  1...",
				"......................",
				"--------------------",
				"1 2 3 4 6 8 10 12 15",
				"...",
				"A001972(n) is the number of 3-tuples (w,x,y) having all terms in {0,...,n} and 2=4x+y. - _Clark Kimberling_, Jun 04 2012",
				"Number of partitions of n into parts 1 (of two sorts) and 4 (of one sort). - _Joerg Arndt_, Aug 08 2013",
				"In the polynomial sequence s(n) = (x*s(n-1)*s(n-4) + y*s(n-2)*s(n-3))/s(n-5), with s(k) = 1 for k = 0..4, the leading term of s(n+5) is x^a(n). See A333260. - _Michael Somos_, Mar 13 2020"
			],
			"reference": [
				"A. Cayley, Numerical tables supplementary to second memoir on quantics, Collected Mathematical Papers. Vols. 1-13, Cambridge Univ. Press, London, 1889-1897, Vol. 2, pp. 276-281.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A001972/b001972.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"A. Cayley, \u003ca href=\"/A001971/a001971.pdf\"\u003eNumerical tables supplementary to second memoir on quantics\u003c/a\u003e, Collected Mathematical Papers. Vols. 1-13, Cambridge Univ. Press, London, 1889-1897, Vol. 2, pp. 276-281. [Annotated scanned copy]",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=208\"\u003e Encyclopedia of Combinatorial Structures 208\u003c/a\u003e",
				"Clark Kimberling and John E. Brown, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL7/Kimberling/kimber67.html\"\u003ePartial Complements and Transposable Dispersions\u003c/a\u003e, J. Integer Seqs., Vol. 7, 2004.",
				"Brian O'Sullivan and Thomas Busch, \u003ca href=\"http://arxiv.org/abs/0810.0231\"\u003eSpontaneous emission in ultra-cold spin-polarised anisotropic Fermi seas\u003c/a\u003e, arXiv 0810.0231v1 [quant-ph], 2008. [Eq 8a, lambda=4]",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2, -1, 0, 1, -2, 1)."
			],
			"formula": [
				"From _Michael Somos_, Apr 21 2000: (Start)",
				"a(n) = a(n-1) + a(n-4) - a(n-5) + 1.",
				"a(n) = floor((n+3)^2/8). (End)",
				"a(n) = Sum_{k=0..n} floor((k+4)/4) = n + 1 + Sum_{k=0..n} floor(k/4). - _Paul Barry_, Aug 19 2003",
				"a(n) = a(n-4) + n + 1. - _Paul Barry_, Jul 14 2004",
				"From _Mitch Harris_, Sep 08 2008: (Start)",
				"a(n) = Sum_{j=0..n+4} floor(j/4);",
				"a(n-4) = (1/2)*floor(n/4)*(2*n - 2 - 4*floor(n/4)). (End)",
				"A002620(n+1) = a(2*n-1)/2.",
				"A000217(n+1) = a(2*n).",
				"a(n)+a(n+1)+a(n+2)+a(n+3) = (n+4)*(n+5)/2. - _Amarnath Murthy_, Apr 26 2004",
				"a(n) = n^2/8 + 3*n/4 + 15/16 + (-1)^n/16 + A056594(n+3)/4. - _Amarnath Murthy_, Apr 26 2004",
				"a(n) = A130519(n+4). - _Franklin T. Adams-Watters_, Jul 10 2009",
				"a(n) = floor((n+1)/(1-e^(-8/(n+1)))). - _Richard R. Forberg_, Aug 07 2013",
				"a(n) = a(-6-n) for all n in Z. - _Michael Somos_, Mar 13 2020"
			],
			"maple": [
				"A001972:=-(2-z+z**3-2*z**4+z**5)/(z+1)/(z**2+1)/(z-1)**3; # conjectured by _Simon Plouffe_ in his 1992 dissertation; gives sequence except for the initial 1"
			],
			"mathematica": [
				"CoefficientList[Series[1/((1-x)^2(1-x^4)),{x,0,80}],x]  (* _Harvey P. Dale_, Mar 27 2011 *)",
				"LinearRecurrence[{2, -1, 0, 1, -2, 1}, {1, 2, 3, 4, 6, 8}, 80] (* _Vladimir Joseph Stephan Orlovsky_, Feb 23 2012 *)"
			],
			"program": [
				"(PARI) a(n)=(n+3)^2\\8;",
				"(MAGMA) [Floor((n+3)^2/8): n in [0..60]]; // _Vincenzo Librandi_, Aug 15 2011"
			],
			"xref": [
				"Bisections are A000217 and A007590. - _Amarnath Murthy_, Apr 26 2004",
				"Cf. A333260."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Partially edited by _R. J. Mathar_, Jul 11 2009"
			],
			"references": 16,
			"revision": 72,
			"time": "2021-03-12T22:32:35-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}