{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A220419",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 220419,
			"data": "0,0,1,0,1,0,1,1,0,1,1,1,3,1,1,0,2,1,2,1,2,1,2,0,1,1,0,2,1,1,2,2,1,1,2,1,1,4,2,1,1,1,3,2,1,0,1,0,1,1,1,1,1,0,3,3,1,4,1,1,0,4,2,2,3,0,1,3,2,2,1,0,5,2,0,0,1,2,2,2,2,1,2,2,3,3,2,0,1,0,2,2,4,3,2,1,3,4,2,3",
			"name": "Number of ways to write n=x+y (x\u003e0, y\u003e0) with 2x+1, 2y-1 and x^3+2y^3 all prime.",
			"comment": [
				"Conjecture: a(n)\u003e0 for all n\u003e527.",
				"This has been verified for n up to 2*10^7. It implies the Goldbach conjecture since 2(x+y)=(2x+1)+(2y-1).",
				"Zhi-Wei Sun also made the following similar conjectures:",
				"(1) Each integer n\u003e1544 can be written as x+y (x\u003e0, y\u003e0) with 2x-1, 2y+1 and x^3+2y^3 all prime.",
				"(2) Any odd number n\u003e2060 can be written as 2p+q with p, q and p^3+2((q-1)/2)^3 all prime.",
				"(3) Every integer n\u003e25537 can be written as p+q (q\u003e0) with p, p-6, p+6 and p^3+2q^3 all prime.",
				"(4) Any even number n\u003e1194 can be written as x+y (x\u003e0, y\u003e0) with x^3+2y^3 and 2x^3+y^3 both prime.",
				"(5) Each integer n\u003e3662 can be written as x+y (x\u003e0, y\u003e0) with 3(xy)^3-1 and 3(xy)^3+1 both prime.",
				"(6) Any integer n\u003e22 can be written as x+y (x\u003e0, y\u003e0) with (xy)^4+1 prime. Also, any integer n\u003e7425 can be written as x+y (x\u003e0, y\u003e0) with 2(xy)^4-1 and 2(xy)^4+1 both prime.",
				"(7) Every odd integer n\u003e1 can be written as x+y (x\u003e0, y\u003e0) with x^4+y^2 prime. Moreover, any odd number n\u003e15050 can be written as p+2q with p, q and p^4+(2q)^2 all prime."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A220419/b220419.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e",
				"D. R. Heath-Brown, \u003ca href=\"https://doi.org/10.1007/BF02392715\"\u003ePrimes represented by x^3 + 2y^3\u003c/a\u003e. Acta Mathematica 186 (2001), pp. 1-84.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1211.1588\"\u003eConjectures involving primes and quadratic forms\u003c/a\u003e, arXiv:1211.1588 [math.NT], 2012-2017."
			],
			"example": [
				"a(25)=1 since 25=3+22 with 2*3+1, 2*22-1 and 3^3+2*22^3=21323 all prime.",
				"a(26)=1 since 26=11+15 with 2*11+1, 2*15-1 and 11^3+2*15^3=8081 all prime."
			],
			"mathematica": [
				"a[n_]:=a[n]=Sum[If[PrimeQ[2k+1]==True\u0026\u0026PrimeQ[2(n-k)-1]==True\u0026\u0026PrimeQ[k^3+2(n-k)^3]==True,1,0],{k,1,n-1}]",
				"Do[Print[n,\" \",a[n]],{n,1,1000}]"
			],
			"xref": [
				"Cf. A220413, A173587, A220272, A219842, A219864, A219923."
			],
			"keyword": "nonn",
			"offset": "1,13",
			"author": "_Zhi-Wei Sun_, Dec 14 2012",
			"references": 6,
			"revision": 15,
			"time": "2021-08-04T02:15:07-04:00",
			"created": "2012-12-14T11:46:41-05:00"
		}
	]
}