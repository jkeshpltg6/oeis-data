{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A268759",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 268759,
			"data": "0,0,0,1,0,0,3,3,0,0,6,9,6,0,0,10,18,18,10,0,0,15,30,36,30,15,0,0,21,45,60,60,45,21,0,0,28,63,90,100,90,63,28,0,0,36,84,126,150,150,126,84,36,0,0,45,108,168,210,225,210,168,108,45,0,0,55,135,216,280,315",
			"name": "Triangle T(n,k) read by rows: T(n,k) = (1/4)*(1 + k)*(2 + k)*(k - n)*(1 + k - n).",
			"comment": [
				"Off-diagonal elements of angular momentum matrices J_1^2 and J_2^2.",
				"Construct the infinite-dimensional matrix representation of angular momentum operators (J_1,J_2,J_3) in the block-diagonal, Jordan-Schwinger form (cf. Harter, Klee, Schwinger). The triangle terms T(n,k) satisfy:(1/2)T(n,k)^(1/2) = \u003cn(n+1)/2+k+1|J_1^2|n(n+1)/2+k+3\u003e = \u003cn(n+1)/2+k+3|J_1^2|n(n+1)/2+k+1\u003e = - \u003cn(n+1)/2+k+1|J_2^2|n(n+1)/2+k+3\u003e = - \u003cn(n+1)/2+k+3|J_2^2|n(n+1)/2+k+1\u003e. In the Dirac notation, we write elements m_{ij} of matrix M as \u003ci|M|j\u003e=m_{ij}. Matrices for J_1^2 and J_2^2 are sparse. These equalities and the central-diagonal equalities of A141387 determine the only nonzero entries.",
				"Notice that a(n) = T(n,k) is always a multiple of the triangular numbers, up to an offset. Conjecture: the triangle tabulating matrix elements \u003cn(n+1)/2+k+1|J_1^p|n(n+1)/2+k+p+1\u003e is determined entirely by the coefficients: binomial(n,p) (cf. A094053). Various sequences along the diagonals of matrix J_1^p lead to other numbers with geometric interpretations (Cf. A000567, A100165)."
			],
			"link": [
				"W. Harter, \u003ca href=\"http://www.uark.edu/ua/modphys/markup/PSDS_Info.html/\"\u003ePrinciples of Symmetry, Dynamics, Spectroscopy\u003c/a\u003e, Wiley, 1993, Ch. 5, page 345-346.",
				"B. Klee, \u003ca href=\"http://demonstrations.wolfram.com/QuantumAngularMomentumMatrices/\"\u003eQuantum Angular Momentum Matrices\u003c/a\u003e, Wolfram Demonstrations Project, 2016.",
				"J. Schwinger, \u003ca href=\"http://www.ifi.unicamp.br/~cabrera/teaching/paper_schwinger.pdf\"\u003e On Angular Momentum \u003c/a\u003e, Cambridge: Harvard University, Nuclear Development Associates, Inc., 1952."
			],
			"formula": [
				"T(n,k) = (1/4)*(1 + k)*(2 + k)*(k - n)*(1 + k - n).",
				"G.f.: x^2/((1-x)^3(1-x*y)^3)"
			],
			"example": [
				"0;",
				"0,  0;",
				"1,  0,  0;",
				"3,  3,  0,  0;",
				"6,  9,  6,  0,  0;",
				"10, 18, 18, 10, 0,  0;",
				"15, 30, 36, 30, 15, 0, 0;",
				"..."
			],
			"mathematica": [
				"Flatten[Table[(1/4) (1 + k) (2 + k) (k - n) (1 + k - n), {n, 0, 10, 1}, {k, 0, n, 1}]]"
			],
			"xref": [
				"Cf. A114327, A094053, A141387."
			],
			"keyword": "nonn,tabl",
			"offset": "0,7",
			"author": "_Bradley Klee_, Feb 20 2016",
			"references": 3,
			"revision": 36,
			"time": "2016-11-08T21:02:47-05:00",
			"created": "2016-04-03T12:49:49-04:00"
		}
	]
}