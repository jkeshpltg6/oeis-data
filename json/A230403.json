{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A230403",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 230403,
			"data": "0,1,0,1,0,2,0,1,0,1,0,2,0,1,0,1,0,2,0,1,0,1,0,3,0,1,0,1,0,2,0,1,0,1,0,2,0,1,0,1,0,2,0,1,0,1,0,3,0,1,0,1,0,2,0,1,0,1,0,2,0,1,0,1,0,2,0,1,0,1,0,3,0,1,0,1,0,2,0,1,0,1,0,2,0,1",
			"name": "a(n) = the largest k such that (k+1)! divides n; the number of trailing zeros in the factorial base representation of n (A007623(n)).",
			"comment": [
				"Many of the comments given in A055881 apply also here.",
				"From _Amiram Eldar_, Mar 10 2021: (Start)",
				"The asymptotic density of the occurrences of k is (k+1)/(k+2)!.",
				"The asymptotic mean of this sequence is e - 2 = 0.718281... (A001113 - 2). (End)"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A230403/b230403.txt\"\u003eTable of n, a(n) for n = 1..10080\u003c/a\u003e",
				"Tyler Ball, Joanne Beckford, Paul Dalenberg, Tom Edgar, and Tina Rajabi, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL23/Edgar/edgar3.html\"\u003eSome Combinatorics of Factorial Base Representations\u003c/a\u003e, J. Int. Seq., Vol. 23 (2020), Article 20.3.3.",
				"\u003ca href=\"/index/Fa#facbase\"\u003eIndex entries for sequences related to factorial base representation\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A055881(n)-1."
			],
			"example": [
				"In factorial number base representation (A007623), the numbers from 1 to 9 are represented as:",
				"n  A007623(n)  a(n) (gives the number of trailing zeros)",
				"1        1       0",
				"2       10       1",
				"3       11       0",
				"4       20       1",
				"5       21       0",
				"6      100       2",
				"7      101       0",
				"8      110       1",
				"9      111       0"
			],
			"mathematica": [
				"With[{b = MixedRadix[Range[12, 2, -1]]}, Array[LengthWhile[Reverse@ IntegerDigits[#, b], # == 0 \u0026] \u0026, 105]] (* _Michael De Vlieger_, Jun 03 2020 *)"
			],
			"program": [
				"(Scheme)",
				"(define (A230403 n) (if (zero? n) 0 (let loop ((n n) (i 2)) (cond ((not (zero? (modulo n i))) (- i 2)) (else (loop (/ n i) (1+ i)))))))"
			],
			"xref": [
				"Cf. A001113, A055881. Bisection: A230404.",
				"A few sequences related to factorial base representation (A007623): A034968, A084558, A099563, A060130, A227130, A227132, A227148, A227149, A153880.",
				"Analogous sequence for binary system: A007814."
			],
			"keyword": "nonn,base,easy",
			"offset": "1,6",
			"author": "_Antti Karttunen_, Oct 31 2013",
			"references": 13,
			"revision": 32,
			"time": "2021-03-10T03:19:16-05:00",
			"created": "2013-11-08T13:44:13-05:00"
		}
	]
}