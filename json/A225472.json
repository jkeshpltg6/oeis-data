{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A225472",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 225472,
			"data": "1,2,3,4,21,18,8,117,270,162,16,609,2862,4212,1944,32,3093,26550,72090,77760,29160,64,15561,230958,1031940,1953720,1662120,524880,128,77997,1941030,13429962,39735360,57561840,40415760,11022480,256,390369,15996222,165198852",
			"name": "Triangle read by rows, k!*S_3(n, k) where S_m(n, k) are the Stirling-Frobenius subset numbers of order m; n \u003e= 0, k \u003e= 0.",
			"comment": [
				"The Stirling-Frobenius subset numbers are defined in A225468 (see also the Sage program)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A225472/b225472.txt\"\u003eRows n = 0..50, flattened\u003c/a\u003e",
				"P. Bala, \u003ca href=\"/A143395/a143395.pdf\"\u003eA 3 parameter family of generalized Stirling numbers\u003c/a\u003e.",
				"Peter Luschny, \u003ca href=\"http://www.luschny.de/math/euler/GeneralizedEulerianPolynomials.html\"\u003eGeneralized Eulerian polynomials.\u003c/a\u003e",
				"Peter Luschny, \u003ca href=\"http://www.luschny.de/math/euler/StirlingFrobeniusNumbers.html\"\u003eThe Stirling-Frobenius numbers.\u003c/a\u003e"
			],
			"formula": [
				"For a recurrence see the Maple program.",
				"T(n, 0) ~ A000079; T(n, 1) ~ A005057; T(n, n) ~ A032031.",
				"From _Wolfdieter Lang_, Apr 10 2017: (Start)",
				"E.g.f. for sequence of column k: exp(2*x)*(exp(3*x) - 1)^k, k \u003e= 0. From the Sheffer triangle S2[3,2] = A225466 with column k multiplied with k!.",
				"O.g.f. for sequence of column k is 3^k*k!*x^k/Product_{j=0..k} (1 - (2+3*j)*x), k \u003e= 0.",
				"T(n, k) = Sum_{j=0..k} (-1)^(k-j)*binomial(k, j)*(2+3*j)^n, 0 \u003c= k \u003c= n.",
				"Three term recurrence (see the Maple program): T(n, k) = 0 if n \u003c k , T(n, -1) = 0, T(0,0) = 1, T(n, k) = 3*k*T(n-1, k-1) + (2 + 3*k)*T(n-1, k) for n \u003e= 1, k=0..n.",
				"For the column scaled triangle (with diagonal 1s) see A225468, and the Bala link with (a,b,c) = (3,0,2), where Sheffer triangles are called exponential Riordan triangles.",
				"(End)",
				"The e.g.f. of the row polynomials R(n, x) = Sum_{k=0..n} T(n, k)*x^k is exp(2*z)/(1 - x*(exp(3*z) - 1)). - _Wolfdieter Lang_, Jul 12 2017"
			],
			"example": [
				"[n\\k][0,     1,      2,       3,       4,       5,      6 ]",
				"[0]   1,",
				"[1]   2,     3,",
				"[2]   4,    21,     18,",
				"[3]   8,   117,    270,     162,",
				"[4]  16,   609,   2862,    4212,    1944,",
				"[5]  32,  3093,  26550,   72090,   77760,   29160,",
				"[6]  64, 15561, 230958, 1031940, 1953720, 1662120, 524880."
			],
			"maple": [
				"SF_SO := proc(n, k, m) option remember;",
				"if n = 0 and k = 0 then return(1) fi;",
				"if k \u003e n or k \u003c 0 then return(0) fi;",
				"m*k*SF_SO(n-1, k-1, m) + (m*(k+1)-1)*SF_SO(n-1, k, m) end:",
				"seq(print(seq(SF_SO(n, k, 3), k=0..n)), n = 0..5);"
			],
			"mathematica": [
				"EulerianNumber[n_, k_, m_] := EulerianNumber[n, k, m] = (If[ n == 0, Return[If[k == 0, 1, 0]]]; Return[(m*(n-k)+m-1)*EulerianNumber[n-1, k-1, m] + (m*k+1)*EulerianNumber[n-1, k, m]]); SFSO[n_, k_, m_] := Sum[ EulerianNumber[n, j, m]*Binomial[j, n-k], {j, 0, n}]; Table[ SFSO[n, k, 3], {n, 0, 8}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, May 29 2013, translated from Sage *)"
			],
			"program": [
				"(Sage)",
				"@CachedFunction",
				"def EulerianNumber(n, k, m) :",
				"    if n == 0: return 1 if k == 0 else 0",
				"    return (m*(n-k)+m-1)*EulerianNumber(n-1, k-1, m)+ (m*k+1)*EulerianNumber(n-1, k, m)",
				"def SF_SO(n, k, m):",
				"    return add(EulerianNumber(n, j, m)*binomial(j, n - k) for j in (0..n))",
				"for n in (0..6): [SF_SO(n, k, 3) for k in (0..n)]"
			],
			"xref": [
				"Cf. A131689 (m=1), A145901 (m=2), A225473 (m=4).",
				"Cf. A225466, A225468, columns: A000079, 3*A016127, 3^2*2!*A016297, 3^3*3!*A025999."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,2",
			"author": "_Peter Luschny_, May 17 2013",
			"references": 5,
			"revision": 21,
			"time": "2017-07-12T05:02:54-04:00",
			"created": "2013-05-21T14:08:34-04:00"
		}
	]
}