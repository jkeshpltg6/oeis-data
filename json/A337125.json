{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A337125",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 337125,
			"data": "1,2,3,4,4,6,6,7,8,9,9,11,11,12,13,14,14,16,16,17,18,19,19,21,21,22,23,24,24,26,26,27,28,28,29,30,30,30,31,32,32,34,34,36,37,37,37,39,39,41,42,43,43,44,45,46,47,47,47,49,49,49,50,51,51,53,53,54",
			"name": "Length of the longest simple path in the divisor graph of {1,...,n}.",
			"comment": [
				"a(n) is the length of the longest simple path in the graph on vertices {1,...,n} in which two vertices are connected by an edge if one divides another.",
				"Saias shows that there exist positive constants b and c such that for sufficiently large n, b*n/log n \u003c a(n) \u003c c*n/log n.",
				"The definition can also be formulated as: a(n) is the length of the longest sequence of distinct numbers between 1 and n such that if k immediately follows m, then either k divides m or m divides k. - _Peter Luschny_, Dec 28 2020",
				"Can be formulated as an optimal subtour problem by introducing a depot node 0 that is adjacent to all other nodes. An integer linear programming formulation is as follows. For {i,j} in E, let binary decision variable x_{i,j} indicate whether edge {i,j} is traversed, and for i in N let binary decision variable y_i indicate whether node i is visited. The objective is to maximize Sum_{i in N \\ {0}} y_i. The constraints are Sum_{{i,j} in E: k in {i,j}} x_{i,j} = 2 y_k for all k in N, y_0 = 1, as well as (dynamically generated) generalized subtour elimination constraints Sum_{i in S, j in S: {i,j} in E} x_{i,j} \u003c= Sum_{i in S \\ {k}} y_i for all S subset N \\ {0} and k in S. - _Rob Pratt_, Dec 28 2020"
			],
			"reference": [
				"Andrew Pollington, There is a long path in the divisor graph, Ars Combinatoria 16 (Jan. 1983), B, 303-304."
			],
			"link": [
				"Rob Pratt, \u003ca href=\"/A337125/b337125.txt\"\u003eTable of n, a(n) for n = 1..200\u003c/a\u003e, the first 75 terms by Nathan McNew.",
				"A. Chadozeau, \u003ca href=\"https://doi.org/10.1007/s10998-008-6227-3\"\u003eSur les partitions en chaînes du graphe divisoriel\u003c/a\u003e, Period. Math. Hungar. 56(2), 227-239, 2008.",
				"G. Chartrand, R. Muntean, V. Saenpholphat, and P. Zhang, \u003ca href=\"http://faculty.nps.edu/rgera/papers/WHICH%20GRAPHS%20ARE%20DIVISOR%20GRAPHS.PDF\"\u003e Which graphs are divisor graphs?\u003c/a\u003e Cong. Numerantium 151, 189-200, 2001.",
				"Paul Erdős and Éric Saias, \u003ca href=\"https://doi.org/10.4064/AA-73-2-189-198\"\u003eSur le graphe divisoriel\u003c/a\u003e, Acta Arith. 73(2), 189-198, 1995.",
				"Erich Friedman, \u003ca href=\"https://erich-friedman.github.io/mathmagic/1198.html\"\u003e Divisor chains (Problem of the Month (November 1998))\u003c/a\u003e.",
				"Nathan McNew, \u003ca href=\"https://arxiv.org/abs/1808.04923\"\u003eCounting primitive subsets and other statistics of the divisor graph of {1,2,...,n}\u003c/a\u003e, arXiv:1808.04923v2 [math.NT], 2020. Published in: \u003ca href=\"https://doi.org/10.1016/j.ejc.2020.103237\"\u003eEuropean Journal of Combinatorics\u003c/a\u003e, Volume 92, February 2021.",
				"Paul Melotti and Éric Saias, \u003ca href=\"https://doi.org/10.4064/aa180711-26-4\"\u003e On path partitions of the divisor graph\u003c/a\u003e, Acta Arith. 192, 329-339, 2020.",
				"Carl Pomerance, \u003ca href=\"https://math.dartmouth.edu/~carlp/divisorgraph.pdf\"\u003eOn the longest simple path in the divisor graph\u003c/a\u003e, Proc. Southeastern Conf. Combinatorics, Graph Theory, and Computing, Boca Raton, Florida, 1983, Cong. Num. 40 (1983), 291-304.",
				"O. Roeder, \u003ca href=\"https://fivethirtyeight.com/features/is-this-bathroom-occupied/\"\u003eSolution to last week’s Riddler Classic\u003c/a\u003e, FiveThirtyEight.",
				"E. Saias, \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/aa/aa83/aa8333.pdf\"\u003eApplications des entiers à diviseurs denses\u003c/a\u003e, Acta Arithmetica, 83, 3 (1998), 225-240.",
				"E. Saias, \u003ca href=\"https://doi.org/10.1007/BF02872766\"\u003eÉtude du graphe divisoriel 3\u003c/a\u003e, Rend. Circ. Mat. Palermo (2) 52 no. 3, 481-488, 2003.",
				"G. Tenenbaum, \u003ca href=\"https://doi.org/10.24033/asens.1710\"\u003eSur un problème de crible et ses applications. II. Corrigendum et étude du graphe divisoriel\u003c/a\u003e, Ann. Sci. École Norm. Sup. (4) 28 (1995) 115-127."
			],
			"formula": [
				"If p prime \u003e= 5, a(p-1) = a(p). - _Bernard Schott_, Dec 28 2020",
				"For 1 \u003c= n \u003c= 33: a(n) = floor(n*5/6) + [(n+1) mod 6 \u003c\u003e 0], where [] are the Iverson brackets. - _Peter Luschny_, Jan 02 2021"
			],
			"example": [
				"For n = 7, the divisor graph has the path 7-1-4-2-6-3, with length 6, but it is not possible to include all 7 integers into a single path, so a(7) = 6.",
				"Other examples for small n (from _N. J. A. Sloane_, Oct 12 2021):",
				"1: 1 (1)",
				"2: 1-2 (2)",
				"3: 2-1-3 (3)",
				"4: 3-1-2-4 (4)",
				"5: 3-1-2-4 (4)",
				"6: 5-1-3-6-2-4 (6)",
				"8: 5-1-3-6-2-4-8 (7)",
				"9: insert 9 between 1 and 3 (8)",
				"10: add 10 to the start (9)"
			],
			"xref": [
				"Cf. A034298 (the smallest possible value of the largest number in a divisor chain of length n).",
				"Cf. A035280 (divisor loops).",
				"Cf. A320536 (least number of paths required to cover the divisor graph).",
				"Cf. A339490 (number of longest paths).",
				"Cf. A339491 (lexicographically earliest longest path).",
				"A347698 gives n - a(n)."
			],
			"keyword": "nonn,hard",
			"offset": "1,2",
			"author": "_Nathan McNew_, Aug 17 2020",
			"ext": [
				"a(74) corrected by _Rob Pratt_, Dec 28 2020"
			],
			"references": 5,
			"revision": 65,
			"time": "2021-10-12T14:17:08-04:00",
			"created": "2020-09-08T03:32:09-04:00"
		}
	]
}