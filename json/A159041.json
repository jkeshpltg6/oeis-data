{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A159041",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 159041,
			"data": "1,1,1,1,-10,1,1,-25,-25,1,1,-56,246,-56,1,1,-119,1072,1072,-119,1,1,-246,4047,-11572,4047,-246,1,1,-501,14107,-74127,-74127,14107,-501,1,1,-1012,46828,-408364,901990,-408364,46828,-1012,1,1,-2035,150602",
			"name": "Triangle read by rows: row n (n\u003e=0) gives the coefficients of the polynomial p(n,x) of degree n defined in comments.",
			"comment": [
				"Let E(n,k) (1 \u003c= k \u003c= n) denote the Eulerian numbers as defined in A008292. Then we define polynomials p(n,x) for n \u003e= 0 as follows.",
				"p(n,x) = (1/(1-x)) * ( Sum_{k=0..floor(n/2)} (-1)^k*E(n+2,k+1)*x^k + Sum_{k=ceiling((n+2)/2)..n+1} (-1)^(n+k)*E(n+2,k+1)*x^k ).",
				"For example,",
				"p(0,x) = (1-x)/(1-x) = 1,",
				"p(1,x) = (1-x^2)/(1-x) = 1 + x,",
				"p(2,x) = (1 - 11*x + 11*x^2 - x^3)/(1-x) = 1 - 10*x + x^2,",
				"p(3,x) = (1 - 26*x + 26*x^3 - x^4)/(1-x) = 1 - 25*x - 25*x^2 + x^3),",
				"p(4,x) = (1 - 57*x + 302*x^2 - 302*x^3 + 57*x^3 + x^5)/(1-x)",
				"       = 1 - 56*x + 246*x^2 - 56*x^3 + x^4.",
				"Row sums are: {1, 2, -8, -48, 136, 1908, -3968, -121040, 176896, 11561820, -11184128, ...}.",
				"More generally, there is a triangle-to-triangle transformation U -\u003e T defined as follows.",
				"Let U(n,k) (1 \u003c= k \u003c= n) be a triangle of nonnegative numbers in which the rows are symmetric about the middle. Define polynomials p(n,x) for n \u003e= 0 by",
				"p(n,x) = (1/(1-x)) * ( Sum_{k=0..floor(n/2)} (-1)^k*U(n+2,k+1)*x^k + Sum_{k=ceiling((n+2)/2)..n+1} (-1)^(n+k)*U(n+2,k+1)*x^k ).",
				"The n-th row of the new triangle T(n,k) (0 \u003c= k \u003c= n) gives the coefficients in the expansion of p(n+2).",
				"The new triangle may be defined recursively by: T(n,0)=1; T(n,k) = T(n,k-1) + (-1)^k*U(n+2,k) for 1 \u003c= k \u003c= floor(n/2); T(n,k) = T(n,n-k).",
				"Note that the central terms in the odd-numbered rows of U(n,k) do not get used.",
				"The following table lists various sequences constructed using this transform:",
				"Parameter Triangle  Triangle  Odd-numbered",
				"m             U         T        rows",
				"0          A007312  A007312   A034870",
				"1          A008292  A159041   A171692",
				"2          A060187  A225356   A225076",
				"3          A142458  A225433   A225398",
				"4          A142459  A225434   A225415"
			],
			"link": [
				"Roger L. Bagula, \u003ca href=\"/A159041/a159041.txt\"\u003eAnother Mathematica program for A159041\u003c/a\u003e."
			],
			"example": [
				"Triangle begins as follows:",
				"  1;",
				"  1,     1;",
				"  1,   -10,      1;",
				"  1,   -25,    -25,        1;",
				"  1,   -56,    246,      -56,       1;",
				"  1,  -119,   1072,     1072,    -119,       1;",
				"  1,  -246,   4047,   -11572,    4047,    -246,        1;",
				"  1,  -501,  14107,   -74127,  -74127,   14107,     -501,      1;",
				"  1, -1012,  46828,  -408364,  901990, -408364,    46828,  -1012,     1;",
				"  1, -2035, 150602, -2052886, 7685228, 7685228, -2052886, 150602, -2035, 1;"
			],
			"maple": [
				"A008292 := proc(n, k) option remember; if k \u003c 1 or k \u003e n then 0; elif k = 1 or k = n then 1; else k*procname(n-1, k)+(n-k+1)*procname(n-1, k-1) ; end if; end proc:",
				"# row n of new triangle T(n,k) in terms of old triangle U(n,k):",
				"p:=proc(n) local k; global U;",
				"simplify( (1/(1-x)) * ( add((-1)^k*U(n+2,k+1)*x^k,k=0..floor(n/2)) + add((-1)^(n+k)*U(n+2,k+1)*x^k, k=ceil((n+2)/2)..n+1 )) );",
				"end;",
				"U:=A008292;",
				"for n from 0 to 6 do lprint(simplify(p(n))); od: # _N. J. A. Sloane_, May 11 2013",
				"A159041 := proc(n, k)",
				"    if k = 0 then",
				"        1;",
				"    elif k \u003c= floor(n/2) then",
				"        A159041(n, k-1)+(-1)^k*A008292(n+2, k+1) ;",
				"    else",
				"        A159041(n, n-k) ;",
				"    end if;",
				"end proc: # _R. J. Mathar_, May 08 2013"
			],
			"mathematica": [
				"A[n_, 1] := 1;",
				"A[n_, n_] := 1;",
				"A[n_, k_] := (n - k + 1)A[n - 1, k - 1] + k A[n - 1, k];",
				"p[x_, n_] = Sum[x^i*If[i == Floor[n/2] \u0026\u0026 Mod[n, 2] == 0, 0, If[i \u003c= Floor[n/2], (-1)^i*A[n, i], -(-1)^(n - i)*A[n, i]]], {i, 0, n}]/(1 - x);",
				"Table[CoefficientList[FullSimplify[p[x, n]], x], {n, 1, 11}];",
				"Flatten[%]"
			],
			"xref": [
				"Cf. A007312, A008292, A034870, A060187, A142458, A142459, A159041, A171692, A225076, A225356, A225398, A225415, A225433, A225434."
			],
			"keyword": "sign,tabl",
			"offset": "0,5",
			"author": "_Roger L. Bagula_, Apr 03 2009",
			"ext": [
				"Edited by _N. J. A. Sloane_, May 07 2013, May 11 2013"
			],
			"references": 11,
			"revision": 49,
			"time": "2020-04-18T01:37:24-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}