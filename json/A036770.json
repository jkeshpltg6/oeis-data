{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A036770",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 36770,
			"data": "1,3,60,3150,317520,52390800,12843230400,4382752374000,1986847742880000,1155153277710432000,838011196011749760000,742058914068404412480000,787724078011075453248000000,987468397792455300321600000000,1443283810213452666950050560000000",
			"name": "Number of labeled rooted trees with a degree constraint: (2*n)!/(2^n) * C(2*n+1, n).",
			"comment": [
				"a(n) is the number of rooted labeled strictly binary trees (each vertex has exactly two children or none) on 2*n + 1 vertices. - _Geoffrey Critzer_, Nov 13 2011"
			],
			"link": [
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=46\"\u003eEncyclopedia of Combinatorial Structures 46\u003c/a\u003e.",
				"Lajos Takacs, \u003ca href=\"http://www.appliedprobability.org/data/files/TMS%20articles/18_1_1.pdf\"\u003eEnumeration of rooted trees and forests\u003c/a\u003e, Math. Scientist 18 (1993), 1-10; see Eqs. (12) and (13) on p. 4.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/StronglyBinaryTree.html\"\u003eStrongly Binary Tree\u003c/a\u003e.",
				"\u003ca href=\"/index/Ro#rooted\"\u003eIndex entries for sequences related to rooted trees\u003c/a\u003e"
			],
			"formula": [
				"Recurrence (with interpolated zeros): Define (b(n): n \u003e= 0) by b(2*m+1) = a(m) and b(2*m) = 0 for m \u003e= 0. Then the sequence (b(n): n \u003e= 0) satisfies the recurrence (-2*n^3 - 6*n^2 - 4*n)*b(n) + (n + 3)*b(n+2) = 0 for n \u003e= 0 with b(0) = 0 and b(1) = 1. [Corrected by _Petros Hadjicostas_, Jun 07 2019]",
				"E.g.f. with interpolated zeros: G(x) = Sum_{n \u003e= 0} b(n)*x^n/n! = Sum_{m \u003e= 0} a(m)*x^(2*m + 1)/(2*m + 1)! = 1/x * (1 - (1 - 2*x^2)^(1/2)) for 0 \u003c |x| \u003c 1/sqrt(2). [Edited by _Petros Hadjicostas_, Jun 07 2019]",
				"E.g.f. with interpolated zeros satisfies G(x)= x*(1 + G(x)^2/2). - _Geoffrey Critzer_, Nov 13 2011",
				"D-finite with recurrence (with no interpolated zeros): -2*a(n)*(n + 1)*(2*n + 1)*(2*n + 3) + (n + 2)*a(n+1) = 0 with a(0) = 1. - _Petros Hadjicostas_, Jun 08 2019",
				"G.f.: 4F1(1,1,1/2,3/2;2;8*x). - _R. J. Mathar_, Jan 28 2020"
			],
			"maple": [
				"spec := [S,{S=Union(Z,Prod(Z,Set(S,card=2)))},labeled]: seq(combstruct[count](spec,size=n)"
			],
			"mathematica": [
				"Range[0, 19]! CoefficientList[Series[(1 - (1 - 2 x^2)^(1/2))/x, {x, 0, 20}], x] (* _Geoffrey Critzer_, Nov 13 2011 *)"
			],
			"program": [
				"(MAGMA) [Factorial(2*n)/(2^n) * Binomial(2*n+1, n): n in [0..15]]; // _Vincenzo Librandi_, Jan 29 2020"
			],
			"xref": [
				"Cf. A001190, A036771, A036772, A036773, A052510."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 8,
			"revision": 68,
			"time": "2020-01-30T22:05:51-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}