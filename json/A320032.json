{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A320032",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 320032,
			"data": "1,1,-1,1,0,1,1,1,1,-1,1,2,5,2,1,1,3,13,29,9,-1,1,4,25,116,233,44,1,1,5,41,299,1393,2329,265,-1,1,6,61,614,4785,20894,27949,1854,1,1,7,85,1097,12281,95699,376093,391285,14833,-1,1,8,113,1784,26329,307024,2296777,7897952,6260561,133496,1",
			"name": "Square array A(n,k), n \u003e= 0, k \u003e= 0, read by antidiagonals, where column k is the expansion of the e.g.f. exp(-x)/(1 - k*x).",
			"comment": [
				"For n \u003e 0 and k \u003e 0, A(n,k) gives the number of derangements of the generalized symmetric group S(k,n), which is the wreath product of Z_k by S_n. - _Peter Kagey_, Apr 07 2020"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A320032/b320032.txt\"\u003eAntidiagonals n = 0..140, flattened\u003c/a\u003e",
				"Sami H. Assaf, \u003ca href=\"https://arxiv.org/abs/1002.3138\"\u003eCyclic derangements\u003c/a\u003e,  arXiv:1002.3138 [math.CO], 2010.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Generalized_symmetric_group\"\u003eGeneralized symmetric group\u003c/a\u003e."
			],
			"formula": [
				"E.g.f. of column k: exp(-x)/(1 - k*x).",
				"A(n,k) = Sum_{j=0..n} (-1)^(n-j)*binomial(n,j)*j!*k^j.",
				"A(n,k) = (-1)^n*2F0(1,-n; ; k)."
			],
			"example": [
				"E.g.f. of column k: A_k(x) = 1 + (k - 1)*x/1! + (2*k^2 - 2*k + 1)*x^2/2! + (6*k^3 - 6*k^2 + 3*k - 1)*x^3/3! + (24*k^4 - 24*k^3 + 12*k^2 - 4*k + 1)*x^4/4! + ...",
				"Square array begins:",
				"   1,   1,     1,      1,      1,       1,  ...",
				"  -1,   0,     1,      2,      3,       4,  ...",
				"   1,   1,     5,     13,     25,      41,  ...",
				"  -1,   2,    29,    116,    299,     614,  ...",
				"   1,   9,   233,   1393,   4785,   12281,  ...",
				"  -1,  44,  2329,  20894,  95699,  307024,  ..."
			],
			"maple": [
				"A:= proc(n, k) option remember;",
				"     `if`(n=0, 1, k*n*A(n-1, k)+(-1)^n)",
				"    end:",
				"seq(seq(A(n, d-n), n=0..d), d=0..12);  # _Alois P. Heinz_, May 07 2020"
			],
			"mathematica": [
				"Table[Function[k, n! SeriesCoefficient[Exp[-x]/(1 - k x), {x, 0, n}]][j - n], {j, 0, 10}, {n, 0, j}] // Flatten",
				"Table[Function[k, (-1)^n HypergeometricPFQ[{1, -n}, {}, k]][j - n], {j, 0, 10}, {n, 0, j}] // Flatten"
			],
			"xref": [
				"Columns k=0..5 give A033999, A000166, A000354, A000180, A001907, A001908.",
				"Main diagonal gives A319392.",
				"Cf. A320031."
			],
			"keyword": "sign,tabl",
			"offset": "0,12",
			"author": "_Ilya Gutkovskiy_, Oct 03 2018",
			"references": 7,
			"revision": 23,
			"time": "2020-07-30T00:37:09-04:00",
			"created": "2018-10-03T17:31:56-04:00"
		}
	]
}