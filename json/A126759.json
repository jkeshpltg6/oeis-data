{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A126759",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 126759,
			"data": "1,2,2,2,2,3,2,4,2,2,3,5,2,6,4,3,2,7,2,8,3,4,5,9,2,10,6,2,4,11,3,12,2,5,7,13,2,14,8,6,3,15,4,16,5,3,9,17,2,18,10,7,6,19,2,20,4,8,11,21,3,22,12,4,2,23,5,24,7,9,13,25,2,26,14,10,8,27,6,28,3,2,15,29,4,30,16,11,5,31,3",
			"name": "a(0) = 1; a(2n) = a(n); a(3n) = a(n); otherwise write n = 6i+j, where j = 1 or 5 and set a(n) = 2i+2 if j = 1, otherwise a(n) = 2i+3.",
			"comment": [
				"Invented by Miles Okazaki, who said: I was trying to write a composition that has the same melody going at several different speeds. If this sequence is mapped onto musical notes and you play every other term, you get the original sequence at half speed. If you play every third term, you again get the same melody. And every 4th term, 6th term, 8th term, 12th term, etc. yields the same result. The pattern generates itself, adding two new increasing integers every six terms.",
				"The formula in the definition encapsulates this verbal description - _N. J. A. Sloane_.",
				"For k\u003e1: a(A007310(k-1))=k and a(m)\u003ck for m \u003c A007310(k-1). - _Reinhard Zumkeller_, Jun 16 2008",
				"For n \u003e 0: a(A007310(n)) = n and a(m) \u003c n for m \u003c A007310(n). - _Reinhard Zumkeller_, May 23 2013"
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A126759/b126759.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"formula": [
				"a(0) = 1, a(2n) = a(n), a(3n) = a(n), a(6n+1) = 2n + 2, a(6n-1) = 2n + 1. [Essentially same as the original description, except the last clause expressed slightly differently.] - _Antti Karttunen_, Jan 28 2015"
			],
			"maple": [
				"a:=proc(n) option remember; local i,j;",
				"if n = 0 then RETURN(1); fi;",
				"if n mod 2 = 0 then RETURN(a(n/2)); fi;",
				"if n mod 3 = 0 then RETURN(a(n/3)); fi;",
				"j := n mod 6; i := (n-j)/6;",
				"if j = 1 then RETURN(2*i+2) else RETURN(2*i+3); fi;",
				"end;",
				"[seq(a(n),n=0..100)];"
			],
			"mathematica": [
				"a[n_] := a[n] = Module[{i, j}, If[n == 0, Return[1]]; If[Mod[n, 2] == 0, Return[a[n/2]]]; If[Mod[n, 3] == 0, Return[a[n/3]]]; j = Mod[n, 6]; i = (n-j)/6; If[j == 1, Return[2*i+2], Return[2*i+3]]]; Table[a[n], {n, 0, 90}] (* _Jean-François Alcover_, Feb 11 2014, after Maple *)"
			],
			"program": [
				"(Haskell)",
				"a126759 n = a126759_list !! n",
				"a126759_list = 1 : f 1 where",
				"   f n = (case mod n 6 of 1 -\u003e 2 * div n 6 + 2",
				"                          5 -\u003e 2 * div n 6 + 3",
				"                          3 -\u003e a126759 $ div n 3",
				"                          _ -\u003e a126759 $ div n 2) : f (n + 1)",
				"-- _Reinhard Zumkeller_, May 23 2013",
				"(Scheme)",
				"(definec (A126759 n) (cond ((zero? n) 1) ((even? n) (A126759 (/ n 2))) ((zero? (modulo n 3)) (A126759 (/ n 3))) ((= 1 (modulo n 6)) (+ 2 (/ (- n 1) 3))) (else (+ 1 (/ (+ n 1) 3)))))",
				";; _Antti Karttunen_, Jan 28 2015"
			],
			"xref": [
				"One more than A126760.",
				"Cf. A007310."
			],
			"keyword": "nonn,nice,hear",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, based on email from Miles Okazaki (milesokazaki(AT)gmail.com), Feb 18 2007",
			"ext": [
				"Typo in definition corrected by _Reinhard Zumkeller_, Jun 16 2008"
			],
			"references": 4,
			"revision": 21,
			"time": "2015-03-13T23:48:39-04:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}