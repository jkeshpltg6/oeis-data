{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A212652",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 212652,
			"data": "1,2,2,4,3,3,4,8,4,4,6,5,7,5,5,16,9,6,10,6,6,7,12,9,7,8,7,7,15,8,16,32,8,10,8,8,19,11,9,10,21,9,22,9,9,13,24,17,10,12,11,10,27,10,10,11,12,16,30,11,31,17,11,64,11,11,34",
			"name": "a(n) = least positive integer M such that n = T(M) - T(k), for k an integer, 0 \u003c= k \u003c= M, where T(r) = A000217(r) is the r-th triangular number.",
			"comment": [
				"n = A000217(a(n)) - A000217(a(n) - A109814(n)).",
				"Conjecture: n appears in row a(n) of A209260.",
				"From _Daniel Forgues_, Jan 06 2016: (Start)",
				"n = Sum_{i=k+1..M} i = T(M) - T(k) = (M-k)*(M+k+1)/2.",
				"n = 2^m, m \u003e= 0, iff M = n = 2^m and k = n - 1 = 2^m - 1. (Points on line with slope 1.) (Powers of 2 can't be the sum of consecutive numbers.)",
				"n is odd prime iff k = M-2. Thus M = (n+1)/2 when n is odd prime. (Points on line with slope 1/2.) (Odd primes can't be the sum of more than 2 consecutive numbers.) (End)",
				"If n = 2^m*p where p is an odd prime, then a(n) = 2^m + (p-1)/2. - _Robert Israel_, Jan 14 2016"
			],
			"reference": [
				"Max Alekseyev, Posting to Sequence Fans Mailing List, Mar 31 2008"
			],
			"link": [
				"David W. Wilson, \u003ca href=\"/A212652/b212652.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Min_{odd d|n} (n/d + (d-1)/2).",
				"a(n) = A218621(n) + (n/A218621(n) - 1)/2.",
				"a(n) = A109814(n) + A118235(n) - 1."
			],
			"example": [
				"For n = 63, we have D(63) = {1,3,7,9,21,63}, B_63 = {11,12,13,22,32,63} and a(63) = min(11,12,13,22,32,63) = 11. Since A109814(63) = 9, T(11) - T(11-9) = T(11) - T(2) = 66 - 3 = 63."
			],
			"maple": [
				"f:= n -\u003e  min(map(t -\u003e n/t + (t-1)/2,",
				"numtheory:-divisors(n/2^padic:-ordp(n,2)))):",
				"map(f, [$1..100]); # _Robert Israel_, Jan 14 2016"
			],
			"mathematica": [
				"Table[Min[n/# + (# - 1)/2 \u0026@ Select[Divisors@ n, OddQ]], {n, 67}] (* _Michael De Vlieger_, Dec 11 2015 *)"
			],
			"program": [
				"(PARI) { A212652(n) = my(m); m=2*n+1; fordiv(n/2^valuation(n,2), d, m=min(m,d+(2*n)\\d)); (m-1)\\2; } \\\\ _Max Alekseyev_, Mar 31 2008"
			],
			"xref": [
				"Cf. A000217, A109814, A118235, A138796, A141419, A209260, A218621."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_L. Edson Jeffery_, Feb 14 2013",
			"ext": [
				"Reference to _Max Alekseyev_'s 2008 proposal of this sequence added by _N. J. A. Sloane_, Nov 01 2014"
			],
			"references": 6,
			"revision": 69,
			"time": "2021-06-10T01:45:32-04:00",
			"created": "2013-02-19T17:53:18-05:00"
		}
	]
}