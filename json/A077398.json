{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A077398",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 77398,
			"data": "0,2,5,39,87,629,1394,10032,22224,159890,354197,2548215,5644935,40611557,89964770,647236704,1433791392,10315175714,22850697509,164395574727,364177368759,2620014019925,5803987202642,41755828744080,92499617873520,665473245885362",
			"name": "First member of the Diophantine pair (m,k) that satisfies 7*(m^2+m) = k^2+k; a(n)=m.",
			"comment": [
				"Equivalently, m such that 28*m*(m+1)+1 is a square. - _Bruno Berselli_, May 19 2014"
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A077398/b077398.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Mohammad K. Azarian, \u003ca href=\"http://www.fq.math.ca/Scanned/37-3/elementary37-3.pdf\"\u003eDiophantine Pair, Problem B-881\u003c/a\u003e, Fibonacci Quarterly, Vol. 37, No. 3, August 1999, pp. 277-278; \u003ca href=\"http://www.fq.math.ca/Scanned/38-2/elementary38-2.pdf\"\u003eSolution to Problem B-881\u003c/a\u003e, Fibonacci Quarterly, Vol. 38, No. 2, May 2000, pp. 183-184.",
				"Vladimir Pletser, \u003ca href=\"https://arxiv.org/abs/2101.00998\"\u003eRecurrent Relations for Multiple of Triangular Numbers being Triangular Numbers\u003c/a\u003e, arXiv:2101.00998 [math.NT], 2021.",
				"Vladimir Pletser, \u003ca href=\"https://arxiv.org/abs/2102.13494\"\u003eTriangular Numbers Multiple of Triangular Numbers and Solutions of Pell Equations\u003c/a\u003e, arXiv:2102.13494 [math.NT], 2021.",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,16,-16,-1,1)."
			],
			"formula": [
				"G.f.: x*(2+3*x+2*x^2)/((1-x)*(1-16*x^2+x^4)).",
				"a(n) = 16*a(n-2) - a(n-4) + 7, n \u003e= 3. [corrected by _Vladimir Pletser_, Feb 29 2020]",
				"Let b(n) be A077397 then a(n+2) = 2*a(n+1) - a(n) + b(n) with a(0)=0 a(1)=2.",
				"a(0)=0, a(1)=2; a(n+2) = (7 + 16*a(n) + 3*sqrt((1+28*a(n)+28*a(n)^2))/2. - _Herbert Kociemba_, May 12 2008",
				"a(n) = a(n-1) + 16*a(n-2) - 16*a(n-3) - a(n-4) + a(n-5). - _Wesley Ivan Hurt_, Jul 24 2020"
			],
			"maple": [
				"f := gfun:-rectoproc({a(-2) = 2, a(-1) = 0, a(0) = 0, a(1) = 2, a(n) = 16*a(n - 2) - a(n - 4) + 7}, a(n), remember): map(f, [$ (0 .. 40)])[]; # _Vladimir Pletser_, Jul 24 2020"
			],
			"mathematica": [
				"LinearRecurrence[{1,16,-16,-1,1},{0,2,5,39,87}, 30] (* _G. C. Greubel_, Jan 18 2018 *)"
			],
			"program": [
				"(PARI) {a(n)=if(n\u003c0,0,polcoeff(x*(2+3*x+2*x^2)/((1-x)*(1-16*x^2+x^4)) + x*O(x^n),n))};",
				"(MAGMA) I:=[0,2,5,39,87]; [n le 5 select I[n] else Self(n-1)+16*Self(n-2) -16*Self(n-3)-Self(n-4)+Self(n-5): n in [1..30]]; // _G. C. Greubel_, Jan 18 2018"
			],
			"xref": [
				"Cf. A077397, A077399, A077400. The k values are in A077401.",
				"Cf. A053141."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "Bruce Corrigan (scentman(AT)myfamily.com), Nov 05 2002",
			"references": 13,
			"revision": 44,
			"time": "2021-05-15T01:24:01-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}