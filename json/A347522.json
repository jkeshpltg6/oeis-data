{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A347522",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 347522,
			"data": "1,11,13,7,3,5,29,23,17,19,2,47,31,37,41,43,83,89,97,53,59,61,67,71,73,79,103,101,107,109,113,131,127,137,139,149,151,157,163,167,173,179,181,191,193,197,199,211,223,229,227,233,239,241,251,257,263,269,271,277,281,283,331,293,307,311",
			"name": "The prime numbers visited on a square spiral when starting at 1 and then stepping to the smallest unvisited prime number that is not visible from the current number.",
			"comment": [
				"A number is not visible from the current number if, given it has coordinates (x,y) relative to the current number, the greatest common divisor of |x| and |y| is greater than 1.",
				"As n increases the vast majority of primes are on the same square ring of numbers as the current prime. However occasionally, especially for primes inside the right side quadrant, the next prime is on an outer or inner ring which causes the step to make a diagonal line. See the linked images. The largest diagonal step after 50000 terms is one at step 43936 between primes 532981 and 531457 which is seen as the long violet diagonal line from the top-left to the bottom-right in the image for these terms. No other such diagonal line is seen up to 10^6 terms."
			],
			"link": [
				"Scott R. Shannon, \u003ca href=\"/A347522/a347522.gif\"\u003eImage showing the path taken when connecting the first 1000 terms\u003c/a\u003e. The colors are graduated across the spectrum to show the relative step order. The white dots show the visited prime numbers.",
				"Scott R. Shannon, \u003ca href=\"/A347522/a347522.jpg\"\u003eImage showing the path taken when connecting the first 50000 terms\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/VisiblePoint.html\"\u003eVisible Point\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Ulam_spiral\"\u003eUlam Spiral\u003c/a\u003e."
			],
			"example": [
				"The square spiral is numbered as follows:",
				".",
				"  17--16--15--14--13   .",
				"   |               |   .",
				"  18   5---4---3  12   29",
				"   |   |       |   |   |",
				"  19   6   1---2  11   28",
				"   |   |           |   |",
				"  20   7---8---9--10   27",
				"   |                   |",
				"  21--22--23--24--25--26",
				".",
				"a(1) = 1. The central starting number.",
				"a(2) = 11 as the smaller prime numbers 2,3,5,7 are all visible from 1, while 11 is hidden by 2.",
				"a(3) = 13 as the smaller prime numbers 2,3,5,7 are all visible from 11, while 13 is hidden by 12.",
				"a(4) = 7 as the smaller prime numbers 2,3,5 are visible from 13, while 7 is hidden by 1 and 3.",
				"a(7) = 29 as the smaller prime numbers 2,17,19,23 are visible from 5, while 29 is hidden by 3,4 and 12."
			],
			"xref": [
				"Cf. A347358 (step to smallest visible), A000040, A063826, A214664, A214665, A331400, A335364, A332767, A330979."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Scott R. Shannon_, Sep 05 2021",
			"references": 2,
			"revision": 17,
			"time": "2021-09-24T10:23:35-04:00",
			"created": "2021-09-24T10:23:35-04:00"
		}
	]
}