{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A165355",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 165355,
			"data": "1,2,7,5,13,8,19,11,25,14,31,17,37,20,43,23,49,26,55,29,61,32,67,35,73,38,79,41,85,44,91,47,97,50,103,53,109,56,115,59,121,62,127,65,133,68,139,71,145,74,151,77,157,80,163,83,169,86,175,89,181,92,187,95,193,98",
			"name": "a(n) = 3n + 1 if n is even, or a(n) = (3n + 1)/2 if n is odd.",
			"comment": [
				"Second trisection of A026741.",
				"A111329(n+1) = A000041(a(n)). - _Reinhard Zumkeller_, Nov 19 2009",
				"We observe that this sequence is a particular case of sequence for which there exists q: a(n+3) = (a(n+2)*a(n+1)+q)/a(n) for every n \u003e= n0. Here q=-9 and n0=0. - _Richard Choulet_, Mar 01 2010",
				"The entries are also encountered via the bilinear transform approximation to the natural log (unit circle). Specifically, evaluating 2(z-1)/(z+1) at z = 2, 3, 4, ..., A165355 entries stem from the pair (sums) seen 2 ahead of each new successive prime. For clarity, the evaluation output is 2, 3, 1, 1, 6, 5, 4, 3, 10, 7, 3, 2, 14, 9, 8, 5, 18, 11, ..., where (1+1), (4+3), (3+2), (8+5), ... generate the A165355 entries (after the first). As an aside, the same mechanism links A165355 to A140777. - _Bill McEachen_, Jan 08 2015",
				"As a follow-up to the previous comment, it appears that the numerators and denominators of 2(z-1)/(z+1) are respectively given by A145979 and A060819, but with different offsets. - _Michel Marcus_, Jan 14 2015",
				"Odd parts of the terms give A067745. E.g.: 1, 2/2, 7, 5, 13, 8/8 .... - _Joe Slater_, Nov 30 2016"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A165355/b165355.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"D. H. Lehmer, \u003ca href=\"/A016825/a016825.pdf\"\u003eContinued fractions containing arithmetic progressions\u003c/a\u003e, Scripta Mathematica, 29 (1973): 17-24. [Annotated copy of offprint]",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,2,0,-1)."
			],
			"formula": [
				"a(n) = A026741(3*n+1).",
				"a(n)*A026741(n) = A005449(n).",
				"a(n)*A022998(n+1) = A000567(n+1).",
				"a(n) = A026741(n+1) + A022998(n).",
				"a(2n) = A016921(n). a(2n+1) = A016789(n).",
				"a(2n+1)*A026741(2n) = A045944(n).",
				"G.f.: (1+2*x+5*x^2+x^3)/((x-1)^2 * (1+x)^2). - _R. J. Mathar_, Sep 26 2009",
				"a(n) = (3+9*n)/4 + (-1)^n*(1+3*n)/4. - _R. J. Mathar_, Sep 26 2009",
				"a(n) = 2*(3n+1)/(4-((2n+2) mod 4)). - _Bill McEachen_, Jan 09 2015",
				"If a(2n-1) = x then a(2n) = 2x+3. - _Robert G. Wilson v_, Jan 26 2015",
				"Let the reduced Collatz procedure be defined as Cr(n) = (3*n+1)/2. For odd n, a(n) = Cr(n). For even n, a(n) = Cr(4*n+1)/2. - _Joe Slater_, Nov 29 2016",
				"a(n) = A067745(n+1) * 2^A007814((3n+1)/2). - _Joe Slater_, Nov 30 2016",
				"a(n) = 2*a(n-2) - a(n-4). - _G. C. Greubel_, Apr 13 2017"
			],
			"mathematica": [
				"f[n_] := If[ OddQ@ n, (3n +1)/2, (3n +1)]; Array[f, 66, 0] (* _Robert G. Wilson v_, Jan 26 2015 *)",
				"f[n_] := (3 (-1)^(2n) + (-1)^(1 + n)) (-2 + 3n)/4; Array[f, 66] (* or *)",
				"CoefficientList[ Series[(x^3 + 5x^2 + 2x + 1)/(x^2 - 1)^2, {x, 0, 65}], x] (* or *)",
				"LinearRecurrence[{0, 2, 0, -1}, {1, 2, 7, 5}, 66] (* _Robert G. Wilson v_, Apr 13 2017 *)"
			],
			"program": [
				"(PARI) a(n)=n+=2*n+1; if(n%2,n,n/2) \\\\ _Charles R Greathouse IV_, Jan 13 2015"
			],
			"xref": [
				"Cf. A165351, A165367, A067745, A007814."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Paul Curtz_, Sep 16 2009",
			"ext": [
				"All comments changed to formulas by _R. J. Mathar_, Sep 26 2009",
				"New name from _Charles R Greathouse IV_, Jan 13 2015",
				"Name corrected by _Joe Slater_, Nov 29 2016"
			],
			"references": 14,
			"revision": 73,
			"time": "2021-07-25T10:51:41-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}