{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A280053",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 280053,
			"data": "1,2,3,4,1,2,3,4,5,2,3,4,5,1,2,3,4,5,2,3,4,5,6,3,4,5,6,2,3,1,2,3,4,5,2,3,4,5,6,3,4,5,6,2,3,4,5,6,3,4,5,6,7,4,1,2,3,4,5,2,3,4,5,6,3,4,5,6,2,3,4,5,6,3,4,5,6,7,4,5,6,7,3,4,2,3,4,5,6,3",
			"name": "\"Nachos\" sequence based on squares.",
			"comment": [
				"The nachos sequence based on a sequence of positive numbers S starting with 1 is defined as follows: To find a(n) we start with a pile of n nachos.",
				"During each phase, we successively remove S(1), then S(2), then S(3), ..., then S(i) nachos from the pile until fewer than S(i+1) remain. Then we start a new phase, successively removing S(1), then S(2), ..., then S(j) nachos from the pile until fewer than S(j+1) remain. Repeat. a(n) is the number of phases required to empty the pile.",
				"Suggested by the Fibonachos sequence A280521, which is the case when S is 1,1,2,3,5,8,13,... (A000045).",
				"If S = 1,2,3,4,5,... we get A057945.",
				"If S = 1,2,3,5,7,11,... (A008578) we get A280055.",
				"If S = triangular numbers we get A281367.",
				"If S = squares we get the present sequence.",
				"If S = powers of 2 we get A100661.",
				"Needs a more professional Maple program.",
				"Comment from _Matthew C. Russell_, Jan 30 2017 (Start):",
				"Theorem: Any nachos sequence based on a sequence S = {1=s1 \u003c s2 \u003c s3 \u003c ...} is unbounded.",
				"Proof: S is the (infinite) set of numbers that we are allowed to subtract. (In the case of Fibonachos, this is the set of Fibonaccis themselves, not the partial sums.)",
				"Suppose that n is a positive integer, with the number of stages of the process denoted by a(n).",
				"Let s_m be the smallest element of S that is greater than n.",
				"Then, if you start the process at N = n + s1 + s2 + s3 + ... + s_(m-1), you will get stuck when you hit n, and will have to start the process over again. Thus you will take a(n) + 1 stages of the process here, so a(N) = a(n) + 1.",
				"(End)"
			],
			"link": [
				"Lars Blomberg, \u003ca href=\"/A280053/b280053.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Reddit user Teblefer, \u003ca href=\"https://www.reddit.com/r/math/comments/5lxh3c\"\u003eFibonachos\u003c/a\u003e"
			],
			"example": [
				"If n = 10, in the first phase we successively remove 1, then 4 nachos, leaving 5 in the pile. The next square is 9, which is bigger than 5, so we start a new phase. We remove 1, then 4 nachos, and now the pile is empty. There were two phases, so a(10)=2."
			],
			"maple": [
				"S:=[seq(i^2,i=1..1000)];",
				"phases := proc(n) global S; local a,h,i,j,ipass;",
				"a:=1; h:=n;",
				"for ipass from 1 to 100 do",
				"   for i from 1 to 100 do",
				"      j:=S[i];",
				"      if j\u003eh then a:=a+1; break; fi;",
				"      h:=h-j;",
				"      if h=0 then return(a); fi;",
				"                       od;",
				"od;",
				"return(-1);",
				"end;",
				"t1:=[seq(phases(i),i=1..1000)];",
				"# 2nd program",
				"A280053 := proc(n)",
				"    local a,nres,i ;",
				"    a := 0 ;",
				"    nres := n;",
				"    while nres \u003e 0 do",
				"        for i from 1 do",
				"            if A000330(i) \u003e nres then",
				"                break;",
				"            end if;",
				"        end do:",
				"        nres := nres-A000330(i-1) ;",
				"        a := a+1 ;",
				"    end do:",
				"    a ;",
				"end proc:",
				"seq(A280053(n),n=1..80) ; # _R. J. Mathar_, Mar 05 2017"
			],
			"xref": [
				"Cf. A000045, A008578, A280521, A057945, A281367, A100661, A280055.",
				"For indices of first occurrences of 1,2,3,4,... see A280054."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Jan 07 2017",
			"references": 6,
			"revision": 32,
			"time": "2017-03-05T15:35:23-05:00",
			"created": "2017-01-07T10:43:42-05:00"
		}
	]
}