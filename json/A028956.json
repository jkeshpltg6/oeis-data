{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A028956",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 28956,
			"data": "1,0,4,2,0,2,0,0,8,0,0,0,6,0,0,0,0,4,4,0,6,0,0,4,0,0,0,2,0,0,4,0,12,0,0,0,0,0,8,0,0,0,0,0,0,2,0,4,10,0,4,0,0,4,0,0,0,4,0,0,0,0,8,0,0,0,0,0,12,0,0,0,8,0,0,2,0,0,0,0,10,0,0,4,0,0,0,0,0,0,0,0,12,4,0,4,0,0,4,0,0,0,8",
			"name": "Theta series of quadratic form (or lattice) with Gram matrix [ 4, 1; 1, 4 ].",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"The number of integer solutions (x, y) to 2*x^2 + x*y + 2*y^2 = n, discriminant -15. - _Ray Chandler_, Jul 12 2014"
			],
			"reference": [
				"R. Barman and N. D. Baruah, Theta function identities associated with Ramanujan's modular equations of degree 15, Proc. Indian Acad. Sci. Math. Sci. 120 (2010), no. 3, 267-284. see p. 271, equ. (3.1)"
			],
			"link": [
				"John Cannon, \u003ca href=\"/A028956/b028956.txt\"\u003eTable of n, a(n) for n = 0..5000\u003c/a\u003e",
				"N. J. A. Sloane et al., \u003ca href=\"https://oeis.org/wiki/Binary_Quadratic_Forms_and_OEIS\"\u003eBinary Quadratic Forms and OEIS\u003c/a\u003e (Index to related sequences, programs, references)",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of phi(q^3) * phi(q^5) + 4 * q^2 * psi(q^6) * psi(q^10) in powers of q where phi(),psi() are Ramanujan theta functions. - _Michael Somos_, Feb 09 2006",
				"Expansion of (phi(q^3) * phi(q^5) + phi(-q^3) * phi(-q^5)) / 2 in powers of q^4 where phi() is a Ramanujan theta function. - _Michael Somos_, Aug 01 2011",
				"Expansion of (eta(q^3) * eta(q^5))^2 / (eta(q) * eta(q^15)) - (eta(q) * eta(q^15))^2 / (eta(q^3) * eta(q^5)) in powers of q. - _Michael Somos_, Aug 26 2006",
				"G.f.: theta_3(q^3) * theta_3(q^5) + theta_2(q^3) * theta_2(q^5) . - _Michael Somos_, Feb 09 2006",
				"a(n) = A192323(4*n)."
			],
			"example": [
				"1 + 4*q^4 + 2*q^6 + 2*q^10 + 8*q^16 + 6*q^24 + 4*q^34 + 4*q^36 + 6*q^40 + 4*q^46 + 2*q^54 + 4*q^60 + 12*q^64 + 8*q^76 + 2*q^90 + 4*q^94 + 10*q^96 + 4*q^100 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 3, 0, q^3] EllipticTheta[ 3, 0, q^5], {q, 0, 4 n}] (* _Michael Somos_, Aug 01 2011 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n==0, qfrep([4, 1;1, 4], n, 1)[n]*2)} /* _Michael Somos_, Aug 26 2006 */",
				"(PARI) {a(n) = if( n\u003c1, n==0, sumdiv( n, d, kronecker( -15, d) - kronecker( -3, d) * kronecker( 5, n/d)))} /* _Michael Somos_, Aug 26 2006 */"
			],
			"xref": [
				"Cf. A192323."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_.",
			"references": 2,
			"revision": 21,
			"time": "2021-03-12T22:24:41-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}