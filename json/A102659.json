{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A102659",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 102659,
			"data": "1,2,12,112,122,1112,1122,1222,11112,11122,11212,11222,12122,12222,111112,111122,111212,111222,112122,112212,112222,121222,122222,1111112,1111122,1111212,1111222,1112112,1112122,1112212,1112222,1121122",
			"name": "List of Lyndon words on {1,2} sorted first by length and then lexicographically.",
			"comment": [
				"A Lyndon word is primitive (not a power of another word) and is earlier in lexicographic order than any of its cyclic shifts."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A102659/b102659.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"F. Bassino, J. Clement and C. Nicaud, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2004.11.002\"\u003eThe standard factorization of Lyndon words: an average point of view\u003c/a\u003e, Discrete Math. 290 (2005), 1-25.",
				"Émilie Charlier, Manon Philibert, Manon Stipulanti, \u003ca href=\"https://arxiv.org/abs/1804.09735\"\u003eNyldon words\u003c/a\u003e, arXiv:1804.09735 [math.CO], 2018. See Table 1.",
				"A. M. Uludag, A. Zeytin and M. Durmus, \u003ca href=\"http://math.gsu.edu.tr/uludag/CHARKSANDDESSINS.pdf\"\u003eBinary Quadratic Forms as Dessins\u003c/a\u003e, 2012. - From _N. J. A. Sloane_, Dec 31 2012",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Lyndon_word\"\u003eLyndon word\u003c/a\u003e",
				"Reinhard Zumkeller, \u003ca href=\"/A210585/a210585.hs.txt\"\u003eHaskell programs for some sequences concerning Lyndon words\u003c/a\u003e",
				"\u003ca href=\"/index/Lu#Lyndon\"\u003eIndex entries for sequences related to Lyndon words\u003c/a\u003e"
			],
			"formula": [
				"A102659 = A102660 intersect A007931 = A213969 intersect A239016. - _M. F. Hasler_, Mar 10 2014"
			],
			"mathematica": [
				"lynQ[q_]:=Array[Union[{q,RotateRight[q,#]}]=={q,RotateRight[q,#]}\u0026,Length[q]-1,1,And];",
				"Join@@Table[FromDigits/@Select[Tuples[{1,2},n],lynQ],{n,5}] (* _Gus Wiseman_, Nov 14 2019 *)"
			],
			"program": [
				"(Haskell) cf. link.",
				"(PARI) is_A102659(n)={ vecsort(d=digits(n))!=d\u0026\u0026for(i=1,#d-1, n\u003e[1,10^(#d-i)]*divrem(n,10^i)\u0026\u0026return); fordiv(#d,L,L\u003c#d \u0026\u0026 d==concat(Col(vector(#d/L,i,1)~*vecextract(d,2^L-1))~)\u0026\u0026return); !setminus(Set(d),[1,2])} \\\\ The last check is the least expensive one, but not useful if we test only numbers with digits {1,2}.",
				"for(n=1,6,p=vector(n,i,10^(n-i))~;forvec(d=vector(n,i,[1,2]),is_A102659(m=d*p)\u0026\u0026print1(m\",\"))) \\\\ One could use is_A102660 instead of is_A102659 here. - _M. F. Hasler_, Mar 08 2014"
			],
			"xref": [
				"Cf. A001037, A074650, A102660, A210584, A210585.",
				"The \"co\" version is A329318.",
				"A triangular version is A296657.",
				"A sequence listing all Lyndon compositions is A294859.",
				"Numbers whose binary expansion is Lyndon are A328596.",
				"Length of the Lyndon factorization of the binary expansion is A211100.",
				"Cf. A059966, A060223, A275692, A281013, A296373, A329131, A329313."
			],
			"keyword": "nonn,easy,nice",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Feb 03 2005",
			"ext": [
				"More terms from _Franklin T. Adams-Watters_, Dec 14 2006",
				"Definition improved by _Reinhard Zumkeller_, Mar 23 2012"
			],
			"references": 48,
			"revision": 33,
			"time": "2019-11-15T21:36:29-05:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}