{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000566",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 566,
			"id": "M4358 N1826",
			"data": "0,1,7,18,34,55,81,112,148,189,235,286,342,403,469,540,616,697,783,874,970,1071,1177,1288,1404,1525,1651,1782,1918,2059,2205,2356,2512,2673,2839,3010,3186,3367,3553,3744,3940,4141,4347,4558,4774,4995,5221,5452,5688",
			"name": "Heptagonal numbers (or 7-gonal numbers): n*(5*n-3)/2.",
			"comment": [
				"Binomial transform of (0, 1, 5, 0, 0, 0, ...). Binomial transform is A084899. - _Paul Barry_, Jun 10 2003",
				"Row sums of triangle A131413. - _Gary W. Adamson_, Jul 08 2007",
				"Sequence starting (1, 7, 18, 34, ...) = binomial transform of (1, 6, 5, 0, 0, 0, ...). Also row sums of triangle A131896. - _Gary W. Adamson_, Jul 24 2007",
				"Also the partial sums of A016861, a zero added in front; therefore a(n) = n (mod 5). - _R. J. Mathar_, Mar 19 2008",
				"Comment from Ken Rosenbaum, Dec 02 2009: if you multiply the terms of this sequence by 40 and add 9, you get A017354, which is the list of squares of all whole numbers ending in 7 (this is easy to prove).",
				"Also sequence found by reading the line from 0, in the direction 0, 7, ..., and the line from 1, in the direction 1, 18, ..., in the square spiral whose edges have length A195013 and whose vertices are the numbers A195014. These parallel lines are the semi-axes perpendicular to the main axis A195015 in the same spiral. - _Omar E. Pol_, Oct 14 2011",
				"Also sequence found by reading the line from 0, in the direction 0, 7, ... and the parallel line from 1, in the direction 1, 18, ..., in the square spiral whose vertices are the generalized heptagonal numbers A085787. - _Omar E. Pol_, Jul 18 2012",
				"Partial sums give A002413. - _Omar E. Pol_, Jan 12 2013"
			],
			"reference": [
				"Albert H. Beiler, Recreations in the Theory of Numbers, Dover, NY, 1964, p. 189.",
				"E. Deza and M. M. Deza, Figurate numbers, World Scientific Publishing (2012), page 6.",
				"Leonard E. Dickson, History of the Theory of Numbers. Carnegie Institute Public. 256, Washington, DC, Vol. 1, 1919; Vol. 2, 1920; Vol. 3, 1923, see vol. 2, p. 2.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A000566/b000566.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"S. Barbero, U. Cerruti, and N. Murru, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL13/Barbero/barbero5.html\"\u003eTransforming Recurrent Sequences by Using the Binomial and Invert Operators\u003c/a\u003e, J. Int. Seq. 13 (2010) # 10.7.7., section 4.4.",
				"C. K. Cook and M. R. Bacon, \u003ca href=\"https://www.fq.math.ca/Papers1/52-4/CookBacon4292014.pdf\"\u003eSome polygonal number summation formulas\u003c/a\u003e, Fib. Q., 52 (2014), 336-343.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=341\"\u003eEncyclopedia of Combinatorial Structures 341\u003c/a\u003e",
				"Bir Kafle, Florian Luca and Alain Togbé, \u003ca href=\"https://doi.org/10.33039/ami.2020.09.002\"\u003ePentagonal and heptagonal repdigits\u003c/a\u003e, Annales Mathematicae et Informaticae. pp. 137-145.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"Omar E. Pol, \u003ca href=\"http://www.polprimos.com/imagenespub/polnum01.jpg\"\u003eIllustration of initial terms of A000217, A000290, A000326, A000384, A000566, A000567\u003c/a\u003e",
				"B. Srinivasa Rao, \u003ca href=\"http://www.fq.math.ca/Papers1/43-3/paper43-3-1.pdf\"\u003eHeptagonal Numbers in the Pell Sequence and Diophantine Equations 2x^2 = y^2(5y - 3)^2 ± 2\u003c/a\u003e, Fib. Quarterly, 43 (2005), 194-201.",
				"B. Srinivasa Rao, \u003ca href=\"http://www.fq.math.ca/Papers1/43-3/paper43-3-1.pdf\"\u003eHeptagonal numbers in the associated Pell sequence and Diophantine equations x^2(5x - 3)^2 = 8y^2 ± 4\u003c/a\u003e, Fib. Quarterly, 43 (2005), 302-306.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HeptagonalNumber.html\"\u003eHeptagonal Number\u003c/a\u003e",
				"\u003ca href=\"/index/Pol#polygonal_numbers\"\u003eIndex to sequences related to polygonal numbers\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1)."
			],
			"formula": [
				"G.f.: x*(1 + 4*x)/(1 - x)^3. _Simon Plouffe_ in his 1992 dissertation.",
				"a(n) = C(n, 1) + 5*C(n, 2). - _Paul Barry_, Jun 10 2003",
				"a(n) = Sum_{k = 1..n} (4*n - 3*k). - _Paul Barry_, Sep 06 2005",
				"a(n) = n + 5*A000217(n-1) - _Floor van Lamoen_, Oct 14 2005",
				"a(n) = 3*a(n-1) - 3*a(n-2) + a(n-3) for a(0) = 0, a(1) = 1, a(2) = 7. -  _Jaume Oliver Lafont_, Dec 02 2008",
				"a(n+1) = A153126(n) + n mod 2; a(2*n+1) = A033571(n); a(2*(n+1)) = A153127(n) + 1. - _Reinhard Zumkeller_, Dec 20 2008",
				"a(n) = 2*a(n-1) - a(n-2) + 5, with a(0) = 0 and a(1) = 1. - _Mohamed Bouhamida_, May 05 2010",
				"a(n) = A000217(n) + 4*A000217(n-1). - _Vincenzo Librandi_, Nov 20 2010",
				"a(n) = a(n-1) + 5*n - 4, with a(0) = 0. - _Vincenzo Librandi_, Nov 20 2010",
				"a(n) = A130520(5*n). - _Philippe Deléham_, Mar 26 2013",
				"a(5*a(n) + 11*n + 1) = a(5*a(n) + 11*n) + a(5*n + 1). - _Vladimir Shevelev_, Jan 24 2014",
				"Sum_{n\u003e=1} 1/a(n) = sqrt(1 - 2/sqrt(5))*Pi/3 + 5*log(5)/6 - sqrt(5)*log((1 + sqrt(5))/2)/3 = 1.32277925312238885674944226131... . See A244639. - _Vaclav Kotesovec_, Apr 27 2016",
				"E.g.f.: x*(2 + 5*x)*exp(x)/2. - _Ilya Gutkovskiy_, Aug 27 2016",
				"From _Charlie Marion_, May 02 2017: (Start)",
				"a(n+m) = a(n) + 5*n*m + a(m);",
				"a(n-m) = a(n) - 5*n*m + a(m) + 3*m;",
				"a(n) - a(m) = (5*(n + m) - 3)*(n - m)/2.",
				"In general, let P(k,n) be the n-th k-gonal number. Then",
				"P(k,n+m) = P(k,n) + (k - 2)*n*m + P(k,m);",
				"P(k,n-m) = P(k,n) - (k - 2)*n*m + P(k,m) + (k - 4)*m;",
				"P(k,n) - P(k,m) = ((k-2)*(n + m) + 4 - k)*(n - m)/2.",
				"(End)",
				"a(n) = A147875(-n) for all n in Z. - _Michael Somos_, Jan 25 2019",
				"a(n) = A000217(n-1) + A000217(2*n-1). - _Charlie Marion_, Dec 19 2019",
				"Product_{n\u003e=2} (1 - 1/a(n)) = 5/7. - _Amiram Eldar_, Jan 21 2021",
				"a(n) + a(n+1) = (2*n+1)^2 + n^2 - 2*n. In general, if we let P(k,n) = the n-th k-gonal number, then P(k^2-k+1,n)+ P(k^2-k+1,n+1) = ((k-1)*n+1)^2 + (k-2)*(n^2-2*n) = ((k-1)*n+1)^2 + (k-2)*A005563(n-2). When k = 2, this formula reduces to the well-known triangular number formula: T(n) + T(n+1) = (n+1)^2. - _Charlie Marion_, Jul 01 2021"
			],
			"example": [
				"G.f. = x + 7*x^2 + 18*x^3 + 34*x^4 + 55*x^5 + 81*x^6 + 112*x^7 + ... - _Michael Somos_, Jan 25 2019"
			],
			"maple": [
				"A000566 := proc(n)",
				"        n*(5*n-3)/2 ;",
				"end proc:",
				"seq(A000566(n),n=0..30); # _R. J. Mathar_, Oct 02 2020"
			],
			"mathematica": [
				"Table[n (5n - 3)/2, {n, 0, 50}] (* or *) LinearRecurrence[{3, -3, 1}, {0, 1, 7}, 50] (* _Harvey P. Dale_, Oct 13 2011 *)",
				"Join[{0},Accumulate[Range[1,315,5]]] (* _Harvey P. Dale_, Mar 26 2016 *)",
				"(* For Mathematica 10.4+ *) Table[PolygonalNumber[RegularPolygon[7], n], {n, 0, 48}] (* _Arkadiusz Wesolowski_, Aug 27 2016 *)",
				"PolygonalNumber[7,Range[0,50]] (* Requires Mathematica version 10 or later *) (* _Harvey P. Dale_, Jan 23 2021 *)"
			],
			"program": [
				"(MAGMA) a000566:=func\u003c n | n*(5*n-3) div 2 \u003e; [ a000566(n): n in [0..50] ];",
				"(PARI) a(n) = n * (5*n - 3) / 2",
				"(Maxima) makelist(n*(5*n-3)/2, n, 0, 20); /* _Martin Ettl_, Dec 11 2012 */",
				"(Haskell)",
				"a000566 n = n * (5 * (n - 1) + 2) `div` 2",
				"a000566_list = scanl (+) 0 a016861_list  -- _Reinhard Zumkeller_, Jun 16 2013",
				"(Python 3) # Intended to compute the initial segment of the sequence, not isolated terms.",
				"def aList():",
				"     x, y = 1, 1",
				"     yield 0",
				"     while True:",
				"         yield x",
				"         x, y = x + y + 5, y + 5",
				"A000566 = aList()",
				"print([next(A000566) for i in range(49)]) # _Peter Luschny_, Aug 04 2019"
			],
			"xref": [
				"Cf. A014637, A014640, A014773, A014792, A069099, A131413, A131896, A134483, A000384.",
				"a(n)= A093562(n+1, 2), (5, 1)-Pascal column.",
				"Cf. A006564, A147875, A244639.",
				"Cf. sequences listed in A254963."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Partially edited by _Joerg Arndt_, Mar 11 2010"
			],
			"references": 234,
			"revision": 182,
			"time": "2021-12-29T09:53:43-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}