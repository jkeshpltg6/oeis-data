{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A342701",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 342701,
			"data": "3,7,5,14,9,34,7,16,15,26,11,68,39,28,15,32,33,72,25,40,35,56,17,101,45,37,45,56,29,152,31,61,39,56,35,144,37,61,39,74,41,128,35,88,45,161,47,192,49,82,51,74,95,216,43,97,75,203,59,304,91,88,63,122",
			"name": "a(n) is the second smallest k such that phi(n+k) = phi(k), or 0 is no such solution exists.",
			"comment": [
				"Sierpiński (1956) proved that there is at least one solution for all n\u003e=1.",
				"Schinzel (1958) proved that there are at least two solutions k to phi(n+k) = phi(k) for all n \u003c= 8*10^47. Schinzel and Wakulicz (1959) increased this bound to 2*10^58.",
				"Schinzel (1958) observed that under the prime k-tuple conjecture there is a second solution for all even n.",
				"Holt (2003) proved that there is a second solution for all even n \u003c= 1.38 * 10^26595411."
			],
			"reference": [
				"Richard K. Guy, Unsolved Problems in Number Theory, 3rd edition, Springer, 2004, section B36, page 138-142.",
				"József Sándor and Borislav Crstici, Handbook of Number theory II, Kluwer Academic Publishers, 2004, Chapter 3, p. 217-219.",
				"Wacław Sierpiński, Sur une propriété de la fonction phi(n), Publ. Math. Debrecen, Vol. 4 (1956), pp. 184-185."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A342701/b342701.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Jeffery J. Holt, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-03-01509-6\"\u003eThe minimal number of solutions to phi(n)=phi(n+k)\u003c/a\u003e, Math. Comp., Vol. 72, No. 244 (2003), pp. 2059-2061.",
				"Andrzej Schinzel, \u003ca href=\"https://eudml.org/doc/206114\"\u003eSur l'équation phi(x + k) = phi(x)\u003c/a\u003e, Acta Arith., Vol. 4, No. 3 (1958), pp. 181-184.",
				"Andrzej Schinzel and Andrzej Wakulicz, \u003ca href=\"https://eudml.org/doc/206421\"\u003eSur l'équation phi(x+k)=phi(x). II\u003c/a\u003e, Acta Arith., Vol. 5, No. 4 (1959), pp. 425-426.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"https://mathworld.wolfram.com/k-TupleConjecture.html\"\u003ek-Tuple Conjecture\u003c/a\u003e."
			],
			"example": [
				"a(1) = 3 since the solutions to the equation phi(1+k) = phi(k) are k = 1, 3, 15, 104, 164, ... (A001274), and 3 is the second solution."
			],
			"mathematica": [
				"f[n_, 0] = 0; f[n_, k0_] := Module[{k = f[n, k0 - 1] + 1}, While[EulerPhi[n + k] != EulerPhi[k], k++]; k]; Array[f[#, 2] \u0026, 100]"
			],
			"program": [
				"(PARI) a(n) = my(k=1, nb=0); while ((nb += (eulerphi(n+k)==eulerphi(k))) != 2, k++); k; \\\\ _Michel Marcus_, Mar 19 2021"
			],
			"xref": [
				"Cf. A000010, A001259, A001274, A007015, A217199."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Amiram Eldar_, Mar 18 2021",
			"references": 3,
			"revision": 12,
			"time": "2021-03-21T11:56:09-04:00",
			"created": "2021-03-18T23:00:49-04:00"
		}
	]
}