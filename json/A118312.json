{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A118312",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 118312,
			"data": "1,8,33,76,129,196,277,372,481,604,741,892,1057,1236,1429,1636,1857,2092,2341,2604,2881,3172,3477,3796,4129,4476,4837,5212,5601,6004,6421,6852,7297,7756,8229,8716,9217,9732,10261,10804,11361,11932,12517,13116,13729,14356,14997,15652",
			"name": "Number of squares on infinite chessboard that a knight can reach in n moves from a fixed square.",
			"comment": [
				"Related to A018842: a(n) = A018842(n) + A018842(n-2) + A018842(n-4) + ... ."
			],
			"reference": [
				"M. Petkovic, Mathematics and Chess, Dover Publications (2003), Problem 3.11."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A118312/b118312.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Mordechai Katzman, \u003ca href=\"http://www.katzman.staff.shef.ac.uk/ComputerAlgebra/knight.ps\"\u003eKnight's moves on an infinite board\u003c/a\u003e",
				"Mordechai Katzman, \u003ca href=\"http://arXiv.org/abs/math.AC/0504113\"\u003eCounting monomials\u003c/a\u003e, arXiv:math/0504113 [math.AC], 2005.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1)."
			],
			"formula": [
				"a(n) = -3 + 4*n + 7*n^2 + 4*Sign[(n - 2)(n - 1)].",
				"G.f.: (1 + 5*x + 12*x^2 - 8*x^4 + 4*x^5)/(1 - x)^3",
				"a(n) = 3*a(n-1) - 3*a(n-2) + a(n-3). - _Vincenzo Librandi_, Jul 09 2012",
				"For n\u003e=3, a(n) = A005892(n)."
			],
			"example": [
				"a(2)=33 because knight in 2 moves from square (0,0) can reach one of the following squares: {{0,0}, {-4,-2}, {-4,0}, {-4,2}, {-3,-3}, {-3,-1}, {-3,1}, {-3,3}, {-2,-4}, {-2,0}, {-2,4}, {-1,-3}, {-1,-1}, {-1,1}, {-1,3}, {0,-4}, {0,-2}, {0,2}, {0,4}, {1,-3}, {1,-1}, {1,1}, {1,3}, {2,-4}, {2,0}, {2,4}, {3,-3}, {3,-1}, {3,1}, {3,3}, {4,-2}, {4,0}, {4,2}}."
			],
			"mathematica": [
				"Table[ -3 + 4*n + 7*n^2 + 4*Sign[(n - 2)(n - 1)], {n, 0, 100}]",
				"CoefficientList[Series[(1+5*x+12*x^2-8*x^4+4*x^5)/(1-x)^3,{x,0,50}],x] (* _Vincenzo Librandi_, Jul 09 2012 *)",
				"Join[{1,8,33},LinearRecurrence[{3,-3,1},{76,129,196},50]] (* _Harvey P. Dale_, Dec 05 2014 *)"
			],
			"program": [
				"(MAGMA) I:=[1, 8, 33, 76, 129, 196, 277]; [n le 7 select I[n] else 3*Self(n-1)-3*Self(n-2)+Self(n-3): n in [1..50]]: // _Vincenzo Librandi_, Jul 09 2012",
				"(PARI) a(n)=7*n^2 + 4*n - 3 + 4*sign((n-2)*(n-1)) \\\\ _Charles R Greathouse IV_, Feb 09 2017"
			],
			"xref": [
				"Cf. A018842 (squares in EXACTLY n moves), A018836 (squares in \u003c=n moves)."
			],
			"keyword": "easy,nice,nonn",
			"offset": "0,2",
			"author": "Anton Chupin (chupin(AT)icmm.ru), May 14 2006",
			"ext": [
				"Link updated by _Tristan Miller_, Jun 13 2013"
			],
			"references": 1,
			"revision": 30,
			"time": "2017-02-09T03:17:44-05:00",
			"created": "2006-05-19T03:00:00-04:00"
		}
	]
}