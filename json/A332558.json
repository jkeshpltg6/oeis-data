{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A332558",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 332558,
			"data": "4,3,2,3,4,5,4,3,5,4,6,5,6,5,4,7,6,5,4,3,6,7,6,5,4,8,7,6,6,5,8,7,6,5,4,8,7,6,5,7,6,5,10,9,8,9,8,7,6,9,8,7,6,5,4,6,12,11,10,9,8,7,6,7,6,5,12,11,10,9,8,7,6,5,8,7,6,11,10,9,8,7,6,5",
			"name": "a(n) is the smallest k such that n*(n+1)*(n+2)*...*(n+k) is divisible by n+k+1.",
			"comment": [
				"This is a multiplicative analog of A332542.",
				"a(n) always exists because one can take k to be 2^m - 1 for m large."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A332558/b332558.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"David A. Corneth, \u003ca href=\"/A332558/a332558.gp.txt\"\u003ePARI program\u003c/a\u003e",
				"J. S. Myers, R. Schroeppel, S. R. Shannon, N. J. A. Sloane, and P. Zimmermann, \u003ca href=\"http://arxiv.org/abs/2004.14000\"\u003eThree Cousins of Recaman's Sequence\u003c/a\u003e, arXiv:2004.14000 [math.NT], April 2020."
			],
			"formula": [
				"a(n) = A061836(n) - 1 for n \u003e= 1.",
				"a(n + 1) \u003e= a(n) - 1. a(n + 1) = a(n) - 1 mostly. - _David A. Corneth_, Apr 14 2020"
			],
			"maple": [
				"f:= proc(n) local k,p;",
				"  p:= n;",
				"  for k from 1 do",
				"    p:= p*(n+k);",
				"    if (p/(n+k+1))::integer then return k fi",
				"  od",
				"end proc:",
				"map(f, [$1..100]); # _Robert Israel_, Feb 25 2020"
			],
			"mathematica": [
				"a[n_] := Module[{k, p = n}, For[k = 1, True, k++, p *= (n+k); If[Divisible[p, n+k+1], Return[k]]]];",
				"Array[a, 100] (* _Jean-François Alcover_, Jun 04 2020, after Maple *)"
			],
			"program": [
				"(PARI) a(n) = {my(r=n*(n+1)); for(k=2, oo, r=r*(n+k); if(r%(n+k+1)==0, return(k))); } \\\\ _Jinyuan Wang_, Feb 25 2020",
				"(PARI) \\\\ See Corneth link",
				"(Python)",
				"def a(n):",
				"    k, p = 1, n*(n+1)",
				"    while p%(n+k+1): k += 1; p *= (n+k)",
				"    return k",
				"print([a(n) for n in range(1, 85)]) # _Michael S. Branicky_, Jun 06 2021"
			],
			"xref": [
				"Cf. A061836 (k+1), A332559 (n+k+1), A332560 (the final product), A332561 (the quotient).",
				"For records, see A333532 and A333533 (and A333537), which give the records in the essentially identical sequence A061836.",
				"Additive version: A332542, A332543, A332544, A081123.",
				"\"Concatenate in base 10\" version: A332580, A332584, A332585."
			],
			"keyword": "nonn,look",
			"offset": "1,1",
			"author": "_Scott R. Shannon_ and _N. J. A. Sloane_, Feb 24 2020",
			"references": 15,
			"revision": 39,
			"time": "2021-06-06T02:53:54-04:00",
			"created": "2020-02-25T11:14:30-05:00"
		}
	]
}