{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A253284",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 253284,
			"data": "1,2,2,6,18,12,24,144,240,120,120,1200,3600,4200,1680,720,10800,50400,100800,90720,30240,5040,105840,705600,2116800,3175200,2328480,665280,40320,1128960,10160640,42336000,93139200,111767040,69189120,17297280",
			"name": "Triangle read by rows, T(n,k) = (k+1)*(n+1)!*(n+k)!/((k+1)!^2*(n-k)!) with n \u003e= 0 and 0 \u003c= k \u003c= n.",
			"comment": [
				"G_n(x) = - Sum_{k=0..n} T(n,k)/(x-1)^(n+k+1) are generating functions, for n=0 of A000012, for n=1 of A002378, for n=2 of A083374 (with offset 0) and for n=3 for A253285. In general G_n(x) is the generating function of the sequence k -\u003e ((n+k)!/k!)*C(n+k-1,k-1). These sequences are associated with the rows of the square array of unsigned Lah numbers (compare A253283 for the columns)."
			],
			"formula": [
				"T(n,k) = (n+1)!*binomial(n+k,n)*binomial(n,k)/(k+1).",
				"T(n,k) = (n+1)!*A088617(n,k).",
				"T(n,0) = n! = A000142(n).",
				"T(n,1) = A001804(n+1) for n\u003e0.",
				"T(n,n) = (2*n)!/n! = A001813(n).",
				"Sum_{k=0..n} T(n,k) = (n+1)!*hypergeom([-n, n+1], [2], -1) = (n+1)!*A006318(n)."
			],
			"example": [
				"Triangle begins:",
				"1;",
				"2, 2;",
				"6, 18, 12;",
				"24, 144, 240, 120;",
				"120, 1200, 3600, 4200, 1680;",
				"720, 10800, 50400, 100800, 90720, 30240;",
				"5040, 105840, 705600, 2116800, 3175200, 2328480, 665280."
			],
			"maple": [
				"T := (n,k) -\u003e ((k+1)*(n+1)!*(n+k)!)/((k+1)!^2*(n-k)!);",
				"for n from 0 to 6 do seq(T(n,k), k=0..n) od;"
			],
			"mathematica": [
				"f[n_] := Rest@ Flatten@ Reap@ Block[{i, k, t}, For[i = 0, i \u003c= n, i++, For[k = 0, k \u003c= i, k++, Sow[(i + 1)!*Binomial[i + k, i]*Binomial[i, k]/(k + 1)]]]]; f@ 7 (* _Michael De Vlieger_, Mar 23 2015 *)"
			],
			"program": [
				"(PARI) tabl(nn) = {for (n=0, nn, for (k=0, n, print1((n+1)!*binomial(n+k,n)*binomial(n,k)/(k+1), \", \");); print(););} \\\\ _Michel Marcus_, Mar 23 2015",
				"(MAGMA) /* As triangle: */ [[(k + 1)*Factorial(n + 1)*Factorial(n + k)/(Factorial(k + 1)^2*Factorial(n - k)): k in [0..n]]: n in [0..10]]; // _Bruno Berselli_, Mar 23 2015"
			],
			"xref": [
				"Cf. A000142, A001804, A001813, A002378, A006318, A083374, A088617, A253283, A253285."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,2",
			"author": "_Peter Luschny_, Mar 23 2015",
			"references": 1,
			"revision": 25,
			"time": "2015-03-23T17:36:17-04:00",
			"created": "2015-03-23T11:15:07-04:00"
		}
	]
}