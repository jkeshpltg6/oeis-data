{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A320470",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 320470,
			"data": "1,1,2,2,3,4,4,5,7,6,8,10,10,11,14,13,16,19,18,20,25,23,27,31,30,34,39,37,42,48,47,50,59,56,63,70,68,74,83,82,89,97,97,104,116,113,123,133,133,142,155,153,166,178,178,189,204,204,218,232,235,247,265,265,283,299",
			"name": "Number of partitions of n such that the successive differences of consecutive parts are strictly decreasing.",
			"comment": [
				"Partitions are usually written with parts in descending order, but the conditions are easier to check \"visually\" if written in ascending order.",
				"Partitions (p(1), p(2), ..., p(m)) such that p(k-1) - p(k-2) \u003e p(k) - p(k-1) for all k \u003e= 3.",
				"The differences of a sequence are defined as if the sequence were increasing, so for example the differences of (6,3,1) are (-3,-2). Then a(n) is the number of integer partitions of n whose differences are strictly decreasing. The Heinz numbers of these partitions are given by A325457. Of course, the number of such integer partitions of n is also the number of reversed integer partitions of n whose differences are strictly decreasing, which is the author's interpretation. - _Gus Wiseman_, May 03 2019"
			],
			"link": [
				"Fausto A. C. Cariboni, \u003ca href=\"/A320470/b320470.txt\"\u003eTable of n, a(n) for n = 0..2000\u003c/a\u003e",
				"Gus Wiseman, \u003ca href=\"/A325325/a325325.txt\"\u003eSequences counting and ranking integer partitions by the differences of their successive parts.\u003c/a\u003e"
			],
			"example": [
				"There are a(10) = 8 such partitions of 10:",
				"01: [10]",
				"02: [1, 9]",
				"03: [2, 8]",
				"04: [3, 7]",
				"05: [4, 6]",
				"06: [5, 5]",
				"07: [1, 4, 5]",
				"08: [2, 4, 4]",
				"There are a(11) = 10 such partitions of 11:",
				"01: [11]",
				"02: [1, 10]",
				"03: [2, 9]",
				"04: [3, 8]",
				"05: [4, 7]",
				"06: [5, 6]",
				"07: [1, 4, 6]",
				"08: [1, 5, 5]",
				"09: [2, 4, 5]",
				"10: [3, 4, 4]"
			],
			"mathematica": [
				"Table[Length[Select[IntegerPartitions[n],Greater@@Differences[#]\u0026]],{n,0,30}] (* _Gus Wiseman_, May 03 2019 *)"
			],
			"program": [
				"(Ruby)",
				"def partition(n, min, max)",
				"  return [[]] if n == 0",
				"  [max, n].min.downto(min).flat_map{|i| partition(n - i, min, i).map{|rest| [i, *rest]}}",
				"end",
				"def f(n)",
				"  return 1 if n == 0",
				"  cnt = 0",
				"  partition(n, 1, n).each{|ary|",
				"    ary0 = (1..ary.size - 1).map{|i| ary[i - 1] - ary[i]}",
				"    cnt += 1 if ary0.sort == ary0 \u0026\u0026 ary0.uniq == ary0",
				"  }",
				"  cnt",
				"end",
				"def A320470(n)",
				"  (0..n).map{|i| f(i)}",
				"end",
				"p A320470(50)"
			],
			"xref": [
				"Cf. A049988, A240026, A240027, A320466, A320510, A325325, A325358, A325393, A325457."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Seiichi Manyama_, Oct 13 2018",
			"references": 12,
			"revision": 27,
			"time": "2021-01-09T07:29:15-05:00",
			"created": "2018-10-14T09:17:31-04:00"
		}
	]
}