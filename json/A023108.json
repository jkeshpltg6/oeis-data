{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A023108",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 23108,
			"data": "196,295,394,493,592,689,691,788,790,879,887,978,986,1495,1497,1585,1587,1675,1677,1765,1767,1855,1857,1945,1947,1997,2494,2496,2584,2586,2674,2676,2764,2766,2854,2856,2944,2946,2996,3493,3495,3583,3585,3673,3675",
			"name": "Positive integers which apparently never result in a palindrome under repeated applications of the function A056964(x) = x + (x with digits reversed).",
			"comment": [
				"196 is conjectured to be smallest initial term which does not lead to a palindrome. John Walker, Tim Irvin and others have extended this to millions of digits without finding one (see A006960).",
				"Also called Lychrel numbers, though the definition of \"Lychrel number\" varies: Purists only call the \"seeds\" or \"root numbers\" Lychrel; the \"related\" or \"extra\" numbers (arising in the former's orbit) have been coined \"Kin numbers\" by Koji Yamashita. There are only 2 \"root\" Lychrels below 1000 and 3 more below 10000, cf. A088753. - _M. F. Hasler_, Dec 04 2007",
				"Question: when do numbers in this sequence start to outnumber numbers that are not in the sequence? - _J. Lowell_, May 15 2014",
				"Answer: according to Doucette's site, 10-digit numbers have 49.61% of Lychrels. So beyond 10 digits, Lychrels start to outnumber non-Lychrels. - _Dmitry Kamenetsky_, Oct 12 2015",
				"From the current definition it is unclear whether palindromes are excluded from this sequence, cf. A088753 vs A063048. 9999 would be the first palindromic term that will never result in a palindrome when the Reverse-then-add function A056964 is repeatedly applied. - _M. F. Hasler_, Apr 13 2019",
				"Empirical observation of the available list shows that every positive integer of the form 99*k-2 such that 1 \u003c k \u003c 9 belongs to the sequence; every positive integer of the form 999*k-1 such that 1 \u003c k \u003c 9 belongs to the sequence except for k=5, which is a palindromic number; every positive integer of the form 9999*k such that 1 \u003c k \u003c 9 belongs to the sequence, and 99999*2+1 belongs to the sequence. Thus, it can be conjectured that every positive integer n in base 10 of the form 999...9*k-m, such that 1 \u003c k \u003c 9, m = 4-j, and j \u003e 1, where j is the number of 9's composing the form of n, is either a palindromic number or belongs to this sequence. - _Juan Moreno Borrallo_, Aug 10 2020",
				"As pointed out by @Mathlove in MathStackExchange (see Links section), the above conjecture can be improved to conjecture that some positive integer n in base 10 such that n = (10^j-1)*k - (4-j) and 2 + (j-5)*floor(j/6) \u003c= k \u003c= j + 6 + floor(j/4) is either a palindromic number or belongs to this sequence. - _Juan Moreno Borrallo_, Mar 15 2021"
			],
			"reference": [
				"F. Gruenberger, Computer Recreations, Scientific American, 250 (No. 4, 1984), 19-26.",
				"Daniel Lignon, Dictionnaire de (presque) tous les nombres entiers, Ellipses, Paris, 2012, 702 pages. See Entry 196."
			],
			"link": [
				"A.H.M. Smeets, \u003ca href=\"/A023108/b023108.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e (tested for 200 iterations; first 249 terms from William Boyles)",
				"DeCode, \u003ca href=\"https://www.dcode.fr/lychrel-number\"\u003eLychrel Number\u003c/a\u003e, dCode.fr 'toolkit' to solve games, riddles, geocaches, 2020.",
				"Jason Doucette, \u003ca href=\"http://www.jasondoucette.com/worldrecords.html\"\u003eWorld Records\u003c/a\u003e",
				"Martianus Frederic Ezerman, Bertrand Meyer and Patrick Sole, \u003ca href=\"https://arxiv.org/abs/1210.7593\"\u003eOn Polynomial Pairs of Integers\u003c/a\u003e, arXiv:1210.7593 [math.NT], 2012-2014.",
				"Patrick De Geest, \u003ca href=\"http://www.worldofnumbers.com/weblinks.htm\"\u003eSome thematic websources\u003c/a\u003e",
				"James Grime and Brady Haran, \u003ca href=\"https://www.youtube.com/watch?v=bN8PE3eljdA\"\u003eWhat's special about 196?\u003c/a\u003e, Numberphile video (2015).",
				"R. K. Guy, \u003ca href=\"https://www.jstor.org/stable/25678158\"\u003eWhat's left?\u003c/a\u003e, Math Horizons, Vol. 5, No. 4 (April 1998), pp. 5-7.",
				"Tim Irvin, \u003ca href=\"http://www.fourmilab.ch/documents/threeyears/two_months_more.html\"\u003eAbout Two Months of Computing, or An Addendum to Mr. Walker's Three Years of Computing\u003c/a\u003e",
				"MathStackExchange, \u003ca href=\"https://math.stackexchange.com/questions/3789543/observation-and-conjecture-on-lychrel-numbers\"\u003eObservation and conjecture on Lychrel numbers\u003c/a\u003e",
				"Niphawan Phoopha and Prapanpong Pongsriiam, \u003ca href=\"http://ijmcs.future-in-tech.net/16.4/R-Phoopha-Pongsriiam.pdf\"\u003eNotes on 1089 and a Variation of the Kaprekar Operator\u003c/a\u003e, Int'l J. Math. Comp. Sci. (2021) Vol. 16, No. 4, 1599-1606.",
				"Project Euler, \u003ca href=\"https://projecteuler.net/problem=55\"\u003eProblem 55: How many Lychrel numbers are there below ten-thousand?\u003c/a\u003e",
				"A.H.M. Smeets, \u003ca href=\"/A023108/a023108.png\"\u003eDistribution of terms \u003c 10000000 (number of terms in interval of length 10000)\u003c/a\u003e",
				"Wade VanLandingham, \u003ca href=\"http://www.p196.org/\"\u003e196 and other Lychrel numbers\u003c/a\u003e",
				"Wade VanLandingham, \u003ca href=\"http://web.archive.org/web/20030828235934/http://home.cfl.rr.com/p196/lychrel.html\"\u003eLargest known Lychrel number\u003c/a\u003e",
				"John Walker, \u003ca href=\"http://www.fourmilab.ch/documents/threeyears/threeyears.html\"\u003eThree Years Of Computing: Final Report On The Palindrome Quest\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/196-Algorithm.html\"\u003e196 Algorithm.\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PalindromicNumberConjecture.html\"\u003ePalindromic Number Conjecture\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/LychrelNumber.html\"\u003eLychrel Number\u003c/a\u003e",
				"\u003ca href=\"/index/Res#RAA\"\u003eIndex entries for sequences related to Reverse and Add!\u003c/a\u003e"
			],
			"example": [
				"From _M. F. Hasler_, Feb 16 2020: (Start)",
				"Under the \"add reverse\" operation, we have:",
				"196 (+ 691) -\u003e 887 (+ 788) -\u003e 1675 (+ 5761) -\u003e 7436 (+ 6347) -\u003e 13783 (+ 38731) -\u003e etc. which apparently never leads to a palindrome.",
				"Similar for 295 (+ 592) -\u003e 887, 394 (+ 493) -\u003e 887, 790 (+ 097) -\u003e 887 and 689 (+ 986) -\u003e 1675, which all merge immediately into the above sequence, and also for the reverse of any of the numbers occurring in these sequences: 493, 592, 691, 788, ...",
				"879 (+ 978) -\u003e 1857 -\u003e 9438 -\u003e 17787 -\u003e 96558 is the only other \"root\" Lychrel below 1000 which yields a sequence distinct from that of 196. (End)"
			],
			"mathematica": [
				"With[{lim = 10^3}, Select[Range@ 4000, Length@ NestWhileList[# + IntegerReverse@ # \u0026, #, ! PalindromeQ@ # \u0026, 1, lim] == lim + 1 \u0026]] (* _Michael De Vlieger_, Dec 23 2017 *)"
			],
			"program": [
				"(PARI) select( {is_A023108(n, L=exponent(n+1)*5)=while(L--\u0026\u0026 n*2!=n+=A004086(n),);!L}, [1..3999]) \\\\ with {A004086(n)=fromdigits(Vecrev(digits(n)))}; default value for search limit L chosen according to known records A065199 and indices A065198. - _M. F. Hasler_, Apr 13 2019, edited Feb 16 2020"
			],
			"xref": [
				"Cf. A006960, A088753, A063048, A089694, A089521, A023109; A075421, A030547.",
				"Cf. A056964 (\"reverse and add\" operation on which this is based)."
			],
			"keyword": "nonn,base,nice",
			"offset": "1,1",
			"author": "_David W. Wilson_",
			"ext": [
				"Edited by _M. F. Hasler_, Dec 04 2007"
			],
			"references": 71,
			"revision": 132,
			"time": "2021-12-03T18:19:13-05:00",
			"created": "1998-06-14T03:00:00-04:00"
		}
	]
}