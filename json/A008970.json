{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008970",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8970,
			"data": "1,1,2,1,6,5,1,14,29,16,1,30,118,150,61,1,62,418,926,841,272,1,126,1383,4788,7311,5166,1385,1,254,4407,22548,51663,59982,34649,7936,1,510,13736,100530,325446,553410,517496,252750,50521,1,1022,42236",
			"name": "Triangle T(n,k) = P(n,k)/2, n \u003e= 2, 1 \u003c= k \u003c n, of one-half of number of permutations of 1..n such that the differences have k runs with the same signs.",
			"reference": [
				"L. Comtet, Advanced Combinatorics, Reidel, 1974, p. 261, #13, P_{n,k}.",
				"F. N. David, M. G. Kendall and D. E. Barton, Symmetric Function and Allied Tables, Cambridge, 1966, p. 260, Table 7.2.1."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A008970/b008970.txt\"\u003eRows n = 2..100, flattened\u003c/a\u003e",
				"Désiré André, \u003ca href=\"http://sites.mathdoc.fr/JMPA/PDF/JMPA_1881_3_7_A10_0.pdf\"\u003eMémoire sur les permutations alternées\u003c/a\u003e, J. Math. Pur. Appl., 7 (1881), 167-184.",
				"Désiré André, \u003ca href=\"https://doi.org/10.24033/asens.235\"\u003eEtude sur les maxima, minima et sequences des permutations\u003c/a\u003e, Ann. Sci. Ecole Norm. Sup., 3, no. 1 (1884), 121-135.",
				"Désiré André, \u003ca href=\"http://sites.mathdoc.fr/JMPA/PDF/JMPA_1895_5_1_A7_0.pdf\"\u003eMémoire sur les permutations quasi-alternées\u003c/a\u003e, Journal de mathématiques pures et appliquées 5e série, tome 1 (1895), 315-350.",
				"Désiré André, \u003ca href=\"https://doi.org/10.24033/bsmf.519\"\u003eMémoire sur les séquences des permutations circulaires\u003c/a\u003e, Bulletin de la S. M. F., tome 23 (1895), pp. 122-184.",
				"M. Bona and R. Ehrenborg, \u003ca href=\"https://arxiv.org/abs/math/9902020\"\u003eA combinatorial proof of the log-concavity of the numbers of permutations with k runs\u003c/a\u003e, arXiv:math/9902020 [math.CO], 1999.",
				"F. Morley, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9904-1897-00451-7\"\u003eA generating function for the number of permutations with an assigned number of sequences\u003c/a\u003e, Bull. Amer. Math. Soc. 4 (1897), 23-28. Shows the transpose of this triangle."
			],
			"formula": [
				"Let P(n, k) = number of permutations of [1..n] with k \"sequences\". Note that A008970 gives P(n, k)/2. Then g.f.: Sum_{n, k} P(n, k) *u^k * t^n/n! = (1 + u)^(-1) * ((1 - u) * (1 - sin(v + t * cos(v))-1) where u = sin(v).",
				"P(n, 1) = 2, P(n, k) = k*P(n-1, k) + 2*P(n-1, k-1) + (n-k)*P(n-1, k-2)."
			],
			"example": [
				"Triangle starts",
				"  1;",
				"  1,  2;",
				"  1,  6,   5;",
				"  1, 14,  29,  16;",
				"  1, 30, 118, 150, 61;",
				"  ..."
			],
			"mathematica": [
				"p[n_ /; n \u003e= 2, 1] = 2; p[n_ /; n \u003e= 2, k_] /; 1 \u003c= k \u003c= n := p[n, k] = k*p[n-1, k] + 2*p[n-1, k-1] + (n-k)*p[n-1, k-2]; p[n_, k_] = 0; t[n_, k_] := p[n, k]/2; A008970 = Flatten[ Table[ t[n, k], {n, 2, 11}, {k, 1, n-1}]] (* _Jean-François Alcover_, Apr 03 2012, after given recurrence *)"
			],
			"xref": [
				"Diagonals give A000352, A000486, A000506, A000111, A000708, A091303.",
				"A059427 gives triangle of P(n, k).",
				"A008303 gives circular version of P(n, k)."
			],
			"keyword": "tabl,nonn,easy,nice",
			"offset": "2,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from Larry Reeves (larryr(AT)acm.org), Feb 01 2001"
			],
			"references": 12,
			"revision": 33,
			"time": "2019-08-08T03:34:37-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}