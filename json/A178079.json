{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A178079",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 178079,
			"data": "0,1,1,1,2,1,-3,-7,-8,-25,-37,47,318,559,2023,7039,-496,-90431,-314775,-1139599,-8007614,-13512079,154788437,1247862041,5097732072,56844671623,290379801907,-1403230649825,-32188159859842,-199066111517153",
			"name": "A (1,-1) Somos-4 sequence.",
			"comment": [
				"Hankel transform of A178078 is a(n+2).",
				"Apparently a(n) = -A174400(n+1). - _R. J. Mathar_, Dec 10 2010",
				"This is a strong elliptic divisibility sequence t_n as given in [Kimberling, p. 16] where x = 1, y = 1, z = 2. - _Michael Somos_, Aug 06 2014",
				"Associated with elliptic curve \"61a1\" y^2 + x*y = x^3 - 2*x + 1 and point (1, -1). - _Michael Somos_, Sep 27 2018"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A178079/b178079.txt\"\u003eTable of n, a(n) for n = 0..150\u003c/a\u003e",
				"C. Kimberling, \u003ca href=\"http://www.fq.math.ca/Scanned/17-1/kimberling1.pdf\"\u003eStrong divisibility sequences and some conjectures\u003c/a\u003e, Fib. Quart., 17 (1979), 13-17.",
				"LMFDB, \u003ca href=\"http://www.lmfdb.org/EllipticCurve/Q/61/a/1\"\u003eElliptic Curve 61.a1 (Cremona label 61a1)\u003c/a\u003e"
			],
			"formula": [
				"a(n) = (a(n-1)*a(n-3) - a(n-2)^2)/a(n-4), n\u003e=4.",
				"a(n) = -a(-n) for all n in Z. - _Michael Somos_, Aug 06 2014",
				"0 = a(n)*a(n+5) - a(n+1)*a(n+4) + 2*a(n+2)*a(n+3) for all n in Z. - _Michael Somos_, Sep 27 2018"
			],
			"mathematica": [
				"Join[{0}, RecurrenceTable[{a[n] == (a[n - 1]*a[n - 3] - a[n - 2]^2)/a[n - 4], a[1] == 1, a[2] == 1, a[3] == 1, a[4] == 2}, a, {n, 1, 50}]] (* _G. C. Greubel_, Sep 22 2018 *)"
			],
			"program": [
				"(MAGMA) I:=[0,1,1,1,2]; [n le 5 select I[n] else (Self(n-1)*Self(n-3)-Self(n-2)^2)/Self(n-4): n in [1..30]]; // _Vincenzo Librandi_, Aug 07 2014",
				"(PARI) m=50; v=concat([1,1,1,2], vector(m-4)); for(n=5, m, v[n] = (v[n-1]*v[n-3] - v[n-2]^2)/v[n-4]); concat([0], v) \\\\ _G. C. Greubel_, Sep 22 2018",
				"(PARI) {a(n) = my(E = ellinit([1, 0, 0, -2, 1]), z =  ellpointtoz(E, [1, -1])); round( ellsigma(E, n*z) / ellsigma(E, z)^n^2)}; /* _Michael Somos_, Sep 27 2018 */"
			],
			"keyword": "easy,sign",
			"offset": "0,5",
			"author": "_Paul Barry_, May 19 2010",
			"ext": [
				"Missing a(0)=0 and a(1)=1 added by _Michael Somos_, Aug 06 2014",
				"More terms from _Vincenzo Librandi_, Aug 07 2014"
			],
			"references": 3,
			"revision": 21,
			"time": "2018-10-17T21:19:17-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}