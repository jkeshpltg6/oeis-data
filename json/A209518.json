{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A209518",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 209518,
			"data": "1,1,3,1,4,6,1,5,10,10,1,6,15,20,15,1,7,21,35,35,21,1,8,28,56,70,56,28,1,9,36,84,126,126,84,36,1,10,45,120,210,252,210,120,45,1,11,55,165,330,462,462,330,165,55",
			"name": "Triangle by rows, reversal of A104712.",
			"comment": [
				"The offset is chosen as \"0\" to match the generalized or compositional Bernoulli numbers.",
				"Following [Blandin and Diaz], we can generalize a subset of Bernoulli numbers to comply with the origin of the triangle (the Pascal matrix A007318 beheaded once: (A074909), twice: (this triangle), and so on...); and a corresponding Bernoulli sequence that equals the inverse of the triangle, extracting the left border. This procedure done with A074909 results in The Bernoulli numbers (A027641/A026642) starting (1, -1/2, 1/6,...). Done with this triangle we obtain A006568/A006569: (1, -1/3, 1/18, 1/90,...).",
				"A generalized algebraic property of the subset of such triangles and compositional Bernoulli numbers is that the triangle M * [corresponding Bernoulli sequence considered as a vector, V] = [1, 0, 0, 0,...].",
				"The infinite set of generalized Bernoulli number sequences thus generated from variants of Pascal's triangle begins: [(1, -1/2, 1/6,...); (1, -1/3, 1/18,...); (1, -1/4, 1/40,...); (1, -1/5, 1/75,...); where the third term denominators = A002411 (1, 6, 18, 40, 75,...) after the \"1\".",
				"Row sums of the triangle = A000295 starting (1, 4, 11, 26, 57,...)."
			],
			"link": [
				"Hector Blandin and Rafael Diaz, \u003ca href=\"http://arxiv.org/abs/0708.0809\"\u003eCompositional Bernoulli numbers\u003c/a\u003e, arXiv:0708.0809 [math.CO], 2007-2008, Page 7, 2nd table is identical to A006569/A006568."
			],
			"formula": [
				"Doubly beheaded variant of Pascal's triangle in which two rightmost diagonals are deleted.",
				"T(n,k)=T(n-1,k)+3*T(n-1,k-1)-2*T(n-2,k-1)-3*T(n-2,k-2)+T(n-3,k-2)+T(n-3,k-3), T(0,0)=1, T(n,k)=0 if k\u003c0 or if k\u003en. - _Philippe Deléham_, Jan 11 2014"
			],
			"example": [
				"First few rows of the triangle =",
				"1;",
				"1, 3;",
				"1, 4, 6;",
				"1, 5, 10, 10;",
				"1, 6, 15, 20, 15;",
				"1, 7, 21, 35, 35, 21;",
				"1, 8, 28, 56, 70, 56, 28;",
				"1, 9, 36, 84, 126, 126, 84, 36;",
				"1, 10, 45, 120, 210, 252, 210, 120, 45;",
				"1, 11, 55, 165, 330, 462, 462, 30, 165, 55;",
				"..."
			],
			"mathematica": [
				"Table[Binomial[n+2, k+2], {n, 0, 9}, {k, n , 0, -1}] // Flatten (* _Jean-François Alcover_, Aug 08 2018 *)"
			],
			"xref": [
				"Cf. A000295, A104721, A074909, A027641, A027642, A006568, A005669, A007318, A002411."
			],
			"keyword": "nonn,tabl",
			"offset": "0,3",
			"author": "_Gary W. Adamson_, Mar 09 2012",
			"references": 1,
			"revision": 20,
			"time": "2018-08-08T07:54:28-04:00",
			"created": "2012-03-12T11:30:15-04:00"
		}
	]
}