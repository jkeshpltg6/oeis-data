{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A282124",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 282124,
			"data": "1,3,3,15,11,63,43,255,171,1023,683,4095,2731,16383,10923,65535,43691,262143,174763,1048575,699051,4194303,2796203,16777215,11184811,67108863,44739243,268435455,178956971,1073741823,715827883,4294967295,2863311531,17179869183",
			"name": "Decimal representation of the x-axis, from the origin to the right edge, of the n-th stage of growth of the two-dimensional cellular automaton defined by \"Rule 430\", based on the 5-celled von Neumann neighborhood.",
			"comment": [
				"Initialized with a single black (ON) cell at stage zero."
			],
			"reference": [
				"S. Wolfram, A New Kind of Science, Wolfram Media, 2002; p. 170."
			],
			"link": [
				"Robert Price, \u003ca href=\"/A282124/b282124.txt\"\u003eTable of n, a(n) for n = 0..126\u003c/a\u003e",
				"Robert Price, \u003ca href=\"/A282124/a282124.tmp.txt\"\u003eDiagrams of first 20 stages\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"http://arxiv.org/abs/1503.01168\"\u003eOn the Number of ON Cells in Cellular Automata\u003c/a\u003e, arXiv:1503.01168 [math.CO], 2015",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ElementaryCellularAutomaton.html\"\u003eElementary Cellular Automaton\u003c/a\u003e",
				"S. Wolfram, \u003ca href=\"http://wolframscience.com/\"\u003eA New Kind of Science\u003c/a\u003e",
				"Wolfram Research, \u003ca href=\"http://atlas.wolfram.com/\"\u003eWolfram Atlas of Simple Programs\u003c/a\u003e",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_2D_5-Neighbor_Cellular_Automata\"\u003eIndex to 2D 5-Neighbor Cellular Automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_Elementary_Cellular_Automata\"\u003eIndex to Elementary Cellular Automata\u003c/a\u003e"
			],
			"formula": [
				"Conjectures from _Colin Barker_, Feb 07 2017: (Start)",
				"a(n) = (-1 + 2*(-1)^n - (-1)^n*2^(1+n) + 2^(2+n)) / 3.",
				"a(n) = 5*a(n-2) - 4*a(n-4) for n\u003e3.",
				"G.f.: (1 + 3*x - 2*x^2) / ((1 - x)*(1 + x)*(1 - 2*x)*(1 + 2*x)).",
				"(End)",
				"Conjectures from _Paul Curtz_, Jun 10 2019: (Start)",
				"a(n) = A001045(n+1)*(period 2: repeat[1, 3]).",
				"a(n+4) = a(n) + 10*A081631(n).",
				"a(2*n+1) = 2^(2*n+2) -1.",
				"a(n+2) = a(n) + A098646(n+1).",
				"(End)"
			],
			"mathematica": [
				"CAStep[rule_, a_] := Map[rule[[10 - #]] \u0026, ListConvolve[{{0, 2, 0},{2, 1, 2}, {0, 2, 0}}, a, 2],{2}];",
				"code = 430; stages = 128;",
				"rule = IntegerDigits[code, 2, 10];",
				"g = 2 * stages + 1; (* Maximum size of grid *)",
				"a = PadLeft[{{1}}, {g, g}, 0,Floor[{g, g}/2]]; (* Initial ON cell on grid *)",
				"ca = a;",
				"ca = Table[ca = CAStep[rule, ca], {n, 1, stages + 1}];",
				"PrependTo[ca, a];",
				"(* Trim full grid to reflect growth by one cell at each stage *)",
				"k = (Length[ca[[1]]] + 1)/2;",
				"ca = Table[Table[Part[ca[[n]] [[j]],Range[k + 1 - n, k - 1 + n]], {j, k + 1 - n, k - 1 + n}], {n, 1, k}];",
				"Table[FromDigits[Part[ca[[i]] [[i]], Range[i, 2 * i - 1]], 2], {i ,1, stages - 1}]"
			],
			"xref": [
				"Cf. A282121, A282122, A282123.",
				"Cf. A001045, A010684, A024036, A081631, A098646."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Robert Price_, Feb 06 2017",
			"references": 4,
			"revision": 19,
			"time": "2019-06-10T22:49:49-04:00",
			"created": "2017-02-07T05:52:07-05:00"
		}
	]
}