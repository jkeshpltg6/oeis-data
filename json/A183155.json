{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A183155",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 183155,
			"data": "1,1,3,9,23,53,115,241,495,1005,2027,4073,8167,16357,32739,65505,131039,262109,524251,1048537,2097111,4194261,8388563,16777169,33554383,67108813,134217675,268435401,536870855,1073741765",
			"name": "The number of order-preserving partial isometries (of an n-chain) of fix zero (fix of alpha = 0). Equivalently, the number of order-preserving partial derangement isometries (of an n-chain).",
			"comment": [
				"a(n) is also the number of dominating sets in the (n+1)-path complement graph. - _Eric W. Weisstein_, Apr 11 2018"
			],
			"link": [
				"Zachary Hamaker, Eric Marberg, Brendan Pawlowski, \u003ca href=\"https://arxiv.org/abs/1706.06665\"\u003eFixed-point-free involutions and Schur P-positivity\u003c/a\u003e, arXiv:1706.06665 [math.CO], 2017.",
				"R. Kehinde, A. Umar, \u003ca href=\"http://arxiv.org/abs/1101.0049\"\u003eOn the semigroup of partial isometries of a finite chain\u003c/a\u003e, arXiv:1101.0049 [math.GR], 2010.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DominatingSet.html\"\u003eDominating Set\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PathComplementGraph.html\"\u003ePath Complement Graph\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-5,2)."
			],
			"formula": [
				"a(n) = A183154(n,0).",
				"a(n) = 2^(n+1) - (2*n+1).",
				"a(0)=1; for n\u003e0, a(n) = 2*a(n-1) + 2*n - 3. - _Vincenzo Librandi_, Feb 05 2011",
				"G.f. ( -1+3*x-4*x^2 ) / ( (2*x-1)*(x-1)^2 ). - _R. J. Mathar_, Feb 06 2011"
			],
			"example": [
				"a(3) = 9 because there are exactly 9 order-preserving partial derangement isometries (on a 3-chain) , namely: empty map; 1--\u003e2; 1--\u003e3; 2--\u003e1; 2--\u003e3; 3--\u003e1; 3--\u003e2; (1,2)--\u003e(2,3); (2,3)--\u003e(1,2) - the mappings are coordinate-wise."
			],
			"mathematica": [
				"Table[1 + 2^(1 + n) - 2 (1 + n), {n, 0, 20}] (* or *)",
				"LinearRecurrence[{4, -5, 2}, {1, 3, 9}, {0, 20}] (* or *)",
				"CoefficientList[Series[(-1 + 3 x - 4 x^2)/((-1 + x)^2 (-1 + 2 x)), {x, 0, 20}], x] (* _Eric W. Weisstein_, Apr 11 2018 *)"
			],
			"program": [
				"(PARI) a(n) = 2^(n+1)-(2*n+1); \\\\ _Altug Alkan_, Apr 12 2018"
			],
			"xref": [
				"Cf. A183154."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Abdullahi Umar_, Dec 28 2010",
			"references": 5,
			"revision": 34,
			"time": "2020-12-04T17:20:33-05:00",
			"created": "2010-12-27T07:54:15-05:00"
		}
	]
}