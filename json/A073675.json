{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A073675",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 73675,
			"data": "2,1,6,8,10,3,14,4,18,5,22,24,26,7,30,32,34,9,38,40,42,11,46,12,50,13,54,56,58,15,62,16,66,17,70,72,74,19,78,20,82,21,86,88,90,23,94,96,98,25,102,104,106,27,110,28,114,29,118,120,122,31,126,128,130,33,134,136",
			"name": "Rearrangement of natural numbers such that a(n) is the smallest proper divisor of n not included earlier but if no such divisor exists then a(n) is the smallest proper multiple of n not included earlier, subject always to the condition that a(n) is not equal to n.",
			"comment": [
				"The parity of the sequence is E,D,E,E,E,D,E,E,E,D,E,E,E,D,E,E,E,D,E,E,E,D,..., that is, an D followed by three E's from the second term onwards.",
				"Closely related to A035263: if A035263(n) = 1, a(n) = 2n; otherwise a(n)=n/2. - _Franklin T. Adams-Watters_, Feb 02 2006",
				"This permutation is self-inverse. This is the case r=2 of sequences where a(n)=floor(n/r) if floor(n/r)\u003e0 and not already in the sequence, a(n) = floor(n*r) otherwise. All such sequences (for r\u003e=1) are permutations of the natural numbers. - _Franklin T. Adams-Watters_, Feb 06 2006"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A073675/b073675.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"If valuation(n,2) is even, a(n) = 2n; otherwise a(n)=n/2, where valuation(n,2) = A007814(n) is the exponent of the highest power of 2 dividing n. - _Franklin T. Adams-Watters_, Feb 06 2006, Jul 31 2009",
				"a(k*2^m) = k*2^(m+(-1)^m), m \u003e= 0, odd k \u003e= 1. - _Carl R. White_, Aug 23 2010"
			],
			"maple": [
				"a:= proc(n) local i, m; m:=n;",
				"      for i from 0 while irem(m, 2, 'r')=0 do m:=r od;",
				"      m*2^`if`(irem(i, 2)=1, i-1, i+1)",
				"    end:",
				"seq(a(n), n=1..80);  # _Alois P. Heinz_, Feb 10 2014"
			],
			"mathematica": [
				"a[n_] := Module[{i, m = n}, For[i = 0, {q, r} = QuotientRemainder[m, 2]; r == 0, i++, m = q]; m*2^If[Mod[i, 2] == 1, i-1, i+1]]; Table[a[n], {n, 1, 80}] (* _Jean-François Alcover_, Jun 10 2015, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(GNU bc) scale=0;for(n=1;n\u003c=100;n++){m=0;for(k=n;!k%2;m++)k/=2;k*2^(m+(-1)^m)} /* _Carl R. White_, Aug 23 2010 */",
				"(PARI) a(n) = if (valuation(n, 2) % 2, n/2, 2*n); \\\\ _Michel Marcus_, Mar 17 2018"
			],
			"xref": [
				"Matches A118967 for all non-powers-of-two. - _Carl R. White_, Aug 23 2010",
				"Row 2 and column 2 of A059897."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Amarnath Murthy_, Aug 11 2002",
			"ext": [
				"More terms and comment from _Franklin T. Adams-Watters_, Feb 06 2006, Jul 31 2009",
				"More terms from _Franklin T. Adams-Watters_, Feb 06 2006",
				"Edited by _N. J. A. Sloane_, Jul 31 2009",
				"Typo fixed by _Charles R Greathouse IV_, Apr 29 2010"
			],
			"references": 13,
			"revision": 34,
			"time": "2019-03-31T03:44:52-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}