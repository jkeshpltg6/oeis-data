{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A128748",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 128748,
			"data": "0,2,11,54,260,1247,5982,28741,138364,667488,3226503,15625476,75802578,368316888,1792203759,8732274312,42598366616,208036945958,1017023261529,4976560342522,24372741339016,119461561111023,585970198529224",
			"name": "Number of peaks at height \u003e1 in all skew Dyck paths of semilength n.",
			"comment": [
				"A skew Dyck path is a path in the first quadrant which begins at the origin, ends on the x-axis, consists of steps U=(1,1)(up), D=(1,-1)(down) and L=(-1,-1)(left) so that up and left steps do not overlap. The length of the path is defined to be the number of its steps."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A128748/b128748.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"E. Deutsch, E. Munarini, S. Rinaldi, \u003ca href=\"http://dx.doi.org/10.1016/j.jspi.2010.01.015\"\u003eSkew Dyck paths\u003c/a\u003e, J. Stat. Plann. Infer. 140 (8) (2010) 2191-2203"
			],
			"formula": [
				"a(n) = Sum_{k=0..n-1} A128747(n,k).",
				"G.f.: (1-4*z+2*z^2+z^3-(1-z+z^2)*sqrt(1-6*z+5*z^2))/(2*z*(2-z)*sqrt(1-6*z+5*z^2)).",
				"a(n) ~ 5^(n-1/2)/sqrt(Pi*n). - _Vaclav Kotesovec_, Mar 20 2014",
				"Conjecture: -2*(n+2)*(9*n^2-13*n-2)*a(n) +(117*n^3-7*n^2-220*n-116)*a(n-1) +8*(-18*n^3+17*n^2+32*n-8)*a(n-2) +5*(n-2)*(9*n^2+5*n-6)*a(n-3)=0. - _R. J. Mathar_, Jun 17 2016",
				"Conjecture: +2*(n+2)*a(n) +(-19*n-18)*a(n-1) +(53*n-12)*a(n-2) +2*(-20*n+19)*a(n-3) +(-n+26)*a(n-4) +5*(n-4)*a(n-5)=0. - _R. J. Mathar_, Jun 17 2016"
			],
			"example": [
				"a(2)=2 because in the paths UDUD, U(UD)D and U(UD)L we have altogether 2 peaks at height \u003e1 (shown between parentheses)."
			],
			"maple": [
				"G:=(1-4*z+2*z^2+z^3-(1-z+z^2)*sqrt(1-6*z+5*z^2))/2/z/(2-z)/sqrt(1-6*z+5*z^2): Gser:=series(G,z=0,30): seq(coeff(Gser,z,n),n=1..27);"
			],
			"mathematica": [
				"Rest[CoefficientList[Series[(1-4*x+2*x^2+x^3-(1-x+x^2)*Sqrt[1-6*x+5*x^2]) /2/x/(2-x)/Sqrt[1-6*x+5*x^2], {x, 0, 20}], x]] (* _Vaclav Kotesovec_, Mar 20 2014 *)"
			],
			"program": [
				"(PARI) z='z+O('z^50); concat([0], Vec((1-4*z+2*z^2+z^3-(1-z+z^2)*sqrt(1-6*z+5*z^2))/(2*z*(2-z)*sqrt(1-6*z+5*z^2)))) \\\\ _G. C. Greubel_, Mar 20 2017"
			],
			"xref": [
				"Cf. A128747."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Emeric Deutsch_, Mar 31 2007",
			"references": 2,
			"revision": 16,
			"time": "2017-03-21T04:16:32-04:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}