{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A153212",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 153212,
			"data": "1,2,4,3,8,6,16,5,9,18,32,15,64,54,12,7,128,10,256,75,36,162,512,35,27,486,25,375,1024,30,2048,11,108,1458,24,21,4096,4374,324,245,8192,150,16384,1875,45,13122,32768,77,81,50,972,9375,65536,14,72,1715,2916,39366,131072,105,262144,118098,225,13",
			"name": "A permutation of the natural numbers: in the prime factorization of n, swap each prime's index difference (from the previous distinct prime that divides n) and the prime's power.",
			"comment": [
				"In order for the \"index difference\" to make sense, we consider the factorization to be sorted with respect to the primes but not the powers to which they are raised; that is, first comes the smallest prime and each subsequent prime is larger than the previous disregarding their powers.",
				"For every n it is true that a(a(n)) = n.",
				"From _Antti Karttunen_, May 29 2014: (Start)",
				"In other words, this is a self-inverse permutation (involution) of natural numbers.",
				"This permutation maps primes (A000040) to the powers of two larger than one (A000079(n\u003e=1)) and vice versa.",
				"The term a(1) = 1 was added on the grounds that as 1 has an empty prime factorization, there is nothing to swap, thus it stays same. It is also needed as a base case for the given recurrence.",
				"Considered as a function on partitions encoded by the indices of primes in the prime factorization of n (as in table A112798), this implements an operation which exchanges the horizontal and vertical line segment of each \"step\" in Young (or Ferrers) diagram of a partition. Please see the last example in the example section.",
				"(End)"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A153212/b153212.txt\"\u003eTable of n, a(n) for n = 1..1024\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Young_diagram\"\u003eYoung diagram\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"Denote the i-th prime with p(i): p(1)=2, p(2)=3, p(3)=5, p(4)=7, etc. Let n = p(a1)^b1 * p(a2)^b2 * ... * p(ak)^bk is the factorization of n where p(i)^j is the i-th prime raised to power j. As mentioned above, we assume that the primes are sorted, i.e., a1 \u003c a2 \u003c a3 ... \u003c ak. Then a(n) = p(c1)^d1 * p(c2)^d2 * ... * p(ck)^dk where c1 = b1 and c(i) = b(i) + c(i-1) for i \u003e 1 d1 = a1 and d(i) = a(i) - a(i-1) for i \u003e 1.",
				"From _Antti Karttunen_, May 16 2014: (Start)",
				"a(1) = 1 and for n\u003e1, let r = a(A051119(n)). Then a(n) = r * (A000040(A061395(r)+A071178(n)) ^ A241919(n)).",
				"a(n) = A122111(A242419(n)) = A242419(A122111(n)).",
				"(End)"
			],
			"example": [
				"For n = 10 we have 10 = 2^1 * 5^1 = p(1)^1 * p(3)^1 then a(10) = p(1)^1 * p(2)^2 = 2^1 * 3^2 = 18.",
				"For n = 18 we have 18 = 2^1 * 3^2 = p(1)^1 * p(2)^2 then a(18) = p(1)^1 * p(3)^1 = 2^1 * 5^1 = 10.",
				"For n = 19 we have 19 = 19^1 = p(8)^1 then a(19) = p(1)^8 = 2^8 = 256.",
				"For n = 2200, we see that it encodes the partition (1,1,1,3,3,5) in A112798 as 2200 = p_1 * p_1 * p_1 * p_3 * p_3 * p_5 = 2^3 * 5^2 * 11. This in turn corresponds to the following Young diagram in French notation:",
				"   _",
				"  | |",
				"  | |",
				"  | |_ _",
				"  |     |",
				"  |     |_ _",
				"  |_ _ _ _ _|",
				"Exchanging the order of the horizontal and vertical line segment of each \"step\", results the following Young diagram:",
				"   _ _ _",
				"  |     |_ _",
				"  |         |",
				"  |         |_",
				"  |           |",
				"  |_ _ _ _ _ _|",
				"which represents the partition (3,5,5,6,6), encoded in A112798 by p_3 * p_5^2 * p_6^2 = 5 * 11^2 * 13^2 = 102245, thus a(2200) = 102245."
			],
			"program": [
				"(Scheme, with _Antti Karttunen_'s IntSeq-library)",
				"(definec (A153212 n) (if (\u003c= n 1) n (let ((r (A153212 (A051119 n)))) (* r (expt (A000040 (+ (A061395 r) (A071178 n))) (A241919 n))))))",
				"(PARI) a(n) = {my(f = factor(n)); my(g = f); for (i=1, #f~, if (i==1, g[i,1] = prime(f[i,2]), g[i,1] = prime(f[i,2]+ primepi(g[i-1,1]))); if (i==1, g[i,2] = primepi(f[i,1]), g[i,2] = primepi(f[i,1]) - primepi(f[i-1,1]));); factorback(g);} \\\\ _Michel Marcus_, Dec 16 2014"
			],
			"xref": [
				"Cf. A000040, A051119, A061395, A071178.",
				"Fixed points: A242421.",
				"{A000027, A122111, A153212, A242419} form a 4-group.",
				"Cf. also A112798, A069799, A242415, A241909, A241916."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "Luchezar Belev (l_belev(AT)yahoo.com), Dec 20 2008",
			"ext": [
				"Term a(1)=1 prepended, and also more terms computed by _Antti Karttunen_, May 16 2014"
			],
			"references": 6,
			"revision": 26,
			"time": "2014-12-18T02:19:53-05:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}