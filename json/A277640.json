{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A277640",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 277640,
			"data": "-2,1,-3,-1,7,-1,6,4,-15,-15,-13,1,-23,1,8,-15,-22,13,-33,27,25,11,-17,24,-32,-53,31,42,-19,18,-35,55,-5,38,-29,76,34,44,-71,-21,-13,16,46,70,92,70,-39,88,-84,-118,-120,64,107,111,-56,124,-13,-23",
			"name": "a(n) is the integer r with |r| \u003c prime(n)/2 such that (T(prime(n)^2)-T(prime(n))/prime(n)^4 == r (mod prime(n)), where T(k) denotes the central trinomial coefficient A002426(k).",
			"comment": [
				"Conjecture: (i) For any prime p \u003e 3 and positive integer n, the number (T(p*n)-T(n))/(p*n)^2 is always a p-adic integer.",
				"(ii) For any prime p \u003e 3 and positive integer k, we have (T(p^k)-T(p^(k-1)))/p^(2k) == 1/6*(p^k/3)*B_{p-2}(1/3) (mod p), where (p^k/3) denotes the Legendre symbol and B_{p-2}(x) is the Bernoulli polynomial of degree p-2.",
				"For any prime p \u003e 3, the author has proved that (T(p*n)-T(n))/(p^2*n) is a p-adic integer for each positive integer n, and that T(p) == 1 + p^2/6*(p/3)*B_{p-2}(1/3) (mod p^3)."
			],
			"reference": [
				"Zhi-Wei Sun, Congruences involving generalized central trinomial coefficients, Sci. China Math. 57(2014), no.7, 1375--1400."
			],
			"link": [
				"Hao Pan and Zhi-Wei Sun, \u003ca href=\"https://arxiv.org/abs/2012.05121\"\u003eSupercongruences for central trinomial coefficients\u003c/a\u003e, arXiv:2012.05121 [math.NT], 2020.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1610.03384\"\u003eSupercongruences involving Lucas sequences\u003c/a\u003e, arXiv:1610.03384 [math.NT], 2016."
			],
			"example": [
				"a(3) = -2 since (T(prime(3)^2)-T(prime(3))/prime(3)^4 = (T(25)-T(5))/5^4 = (82176836301-51)/5^4 = 131482938 is congruent to -2 modulo prime(3) = 5 with |-2| \u003c 5/2."
			],
			"mathematica": [
				"T[n_]:=T[n]=Sum[Binomial[n,2k]Binomial[2k,k],{k,0,n/2}]",
				"rMod[m_,n_]:=Mod[Numerator[m]*PowerMod[Denominator[m],-1,n],n,-n/2]",
				"Do[Print[n,\" \",rMod[(T[Prime[n]^2]-T[Prime[n]])/Prime[n]^4,Prime[n]]],{n,3,60}]"
			],
			"xref": [
				"Cf. A000040, A002426, A245089, A277860."
			],
			"keyword": "sign",
			"offset": "3,1",
			"author": "_Zhi-Wei Sun_, Oct 25 2016",
			"references": 7,
			"revision": 16,
			"time": "2020-12-10T08:10:52-05:00",
			"created": "2016-10-25T14:05:33-04:00"
		}
	]
}