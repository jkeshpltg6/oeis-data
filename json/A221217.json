{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A221217",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 221217,
			"data": "1,6,5,4,3,2,15,14,13,12,11,10,9,8,7,28,27,26,25,24,23,22,21,20,19,18,17,16,45,44,43,42,41,40,39,38,37,36,35,34,33,32,31,30,29,66,65,64,63,62,61,60,59,58,57,56,55,54,53,52,51,50,49,48,47,46,91",
			"name": "T(n,k) = ((n+k)^2-2*n+3-(n+k-1)*(1+2*(-1)^(n+k)))/2; n , k \u003e 0, read by antidiagonals.",
			"comment": [
				"Permutation of the natural numbers.",
				"a(n) is a pairing function: a function that reversibly maps Z^{+} x Z^{+} onto Z^{+}, where Z^{+} is the set of integer positive numbers.",
				"Enumeration table T(n,k). Let m be natural number. The order of the list:",
				"T(1,1)=1;",
				"T(3,1), T(2,2), T(1,3);",
				"T(2,1), T(1,2);",
				". . .",
				"T(2*m+1,1), T(2*m,2), T(2*m-1,3),...T(1,2*m+1);",
				"T(2*m,1), T(2*m-1,2), T(2*m-2,3),...T(1,2*m);",
				". . .",
				"First row  contains elements antidiagonal {T(1,2*m+1), ... T(2*m+1,1)}, read upwards.",
				"Second row contains elements antidiagonal {T(1,2*m), ... T(2*m,1)},  read upwards."
			],
			"link": [
				"Boris Putievskiy, \u003ca href=\"/A221217/b221217.txt\"\u003eRows n = 1..140 of triangle, flattened\u003c/a\u003e",
				"Boris Putievskiy, \u003ca href=\"http://arxiv.org/abs/1212.2732\"\u003eTransformations [of] Integer Sequences And Pairing Functions\u003c/a\u003e arXiv:1212.2732 [math.CO]",
				"Eric W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/PairingFunction.html\"\u003eMathWorld: Pairing functions\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"As table",
				"T(n,k) = ((n+k)^2-2*n+3-(n+k-1)*(1+2*(-1)^(n+k)))/2.",
				"As linear sequence",
				"a(n) = (A003057(n)^2-2*A002260(n)+3-A002024(n)*(1+2*(-1)^A003056(n)))/2;",
				"a(n)=((t+2)^2-2*i+3-(t+1)*(1+2*(-1)**t))/2, where i=n-t*(t+1)/2,",
				"j=(t*t+3*t+4)/2-n, t=floor((-1+sqrt(8*n-7))/2)."
			],
			"example": [
				"The start of the sequence as table:",
				"1....6...4..15..11..28..22...",
				"5....3..14..10..27..21..44...",
				"2...13...9..26..20..43..35...",
				"12...8..25..19..42..34..63...",
				"7...24..18..41..33..62..52...",
				"23..17..40..32..61..51..86...",
				"16..39..31..60..50..85..73...",
				". . .",
				"The start of the sequence as triangle array read by rows:",
				"1;",
				"6,5;",
				"4,3,2;",
				"15,14,13,12;",
				"11,10,9,8,7;",
				"28,27,26,25,24,23;",
				"22,21,20,19,18,17,16;",
				". . .",
				"Row number r consecutive contains r numbers in decreasing order."
			],
			"program": [
				"(Python)",
				"t=int((math.sqrt(8*n-7) - 1)/ 2)",
				"i=n-t*(t+1)/2",
				"j=(t*t+3*t+4)/2-n",
				"result=((t+2)**2-2*i+3-(t+1)*(1+2*(-1)**t))/2"
			],
			"xref": [
				"Cf. A211394, A221215, A002260, A004736, A003057, A002024;",
				"table T(n,k) contains: in rows A084849, A000384, A014106, A014105, A014107, A091823, A071355, A091823, A071355,  A100040, A130861, A100041;",
				"in columns A130883, A096376, A033816, A100037, A100038, A100039;",
				"main diagonal and parallel diagonals are A058331, A051890, A005893, A097080, A093328, A137882, A001844, A001105, A056220, A142463, A054000, A090288, A059993."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Boris Putievskiy_, Feb 22 2013",
			"references": 1,
			"revision": 20,
			"time": "2013-02-27T17:28:12-05:00",
			"created": "2013-02-27T17:28:12-05:00"
		}
	]
}