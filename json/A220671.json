{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A220671",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 220671,
			"data": "-14,15,-20,8,-1,55,-170,221,-153,59,-12,1,115,-670,1773,-2696,2549,-1538,589,-138,18,-1,195,-1850,8215,-21530,36330,-41110,31865,-17080,6314,-1579,255,-24,1,295,-4150,27735,-110795,289540,-518290,654595,-595805,396316,-193906,69641,-18129,3327,-408,30,-1",
			"name": "Coefficient array for powers of x^2 of polynomials appearing in a generalized Melham conjecture on alternating sums of fifth powers of Chebyshev S polynomials with odd indices.",
			"comment": [
				"The row lengths sequence is 3*n + 1 = A016777(n).",
				"For the generalized Melham conjecture and links to the references concerned with the Melham conjecture on sums of fifth powers of even-indexed Fibonacci numbers see a comment under A220670.",
				"Here the conjecture is considered for m=2 (fifth powers): H(2,n,x^2):= product(tau(j,x), j=0..2) * sum(((-1)^k)*(S(2*k-1,x)/x)^5, k=0..n) / (P(n,x^2)^2), with P(n,x^2):= (1 - (-1)^n*S(2*n,x))/x^2. For tau(j,x):= 2*T(2*j+1,x/2)/x, with Chebyshev's T polynomials see a Oct 23 2012 comment on A111125. For the polynomials P see signed A109954. The conjecture is that H(2,n,x^2) is an integer polynomial of degree 3*n:  H(2,n,x^2) = sum(a(n,p)*x^(2*p), p=0..3*n), n \u003e= 1.",
				"If one puts x = i (the imaginary unit) one obtains the original Melham conjecture for Fibonacci numbers F = A000045.",
				"H(2,n,-1) = +44*sum(F(2*k)^5,k=0..n)/(1+F(2*n+1))^2, n\u003e=1, which is conjectured to be -14 - 3*y(n) + 8*y(n)^2 + 4*y(n)^3, with y(n):=F(2*n+1) (see row m=2 of A217475).",
				"It is conjectured that H(2,n,x^2) = h(2,n,x^2) - 3*z(n) + 8*z(n)^2 + 4*z(n)^3, with z(n):= ((-1)^n)*S(2*n,x), with h an integer polynomial of degree 3*n. See A220672 for the coefficients of h(2,n,x^2) for n = 1..5. Because h(2,n,-1) = -14 by the usual Melham conjecture, we put h(2,0,x^2) = -14."
			],
			"formula": [
				"a(n,p) = [x^(2*p)] H(2,n,x^2), n\u003e=1, with H(2,n,x^2) defined in a comment above. a(0,0) has been put to -14 ad hoc."
			],
			"example": [
				"The array a(n,p) begins:",
				"n\\p   0     1     2     3 4    4      5    6     7   8    9",
				"0:  -14",
				"1:   15   -20     8    -1",
				"2:   55  -170   221  -153     59    -12    1",
				"3:  115  -670  1773 -2696   2549  -1538  589  -138  18   -1",
				"...",
				"Row n=4: 195, -1850, 8215, -21530, 36330, -41110, 31865, -17080, 6314, -1579, 255, -24, 1],",
				"Row n=5: [295, -4150, 27735, -110795, 289540, -518290, 654595, -595805, 396316, -193906, 69641, -18129, 3327, -408, 30, -1],",
				"Row n=6: [415, -8120, 76118, -429531, 1599441, -4125672, 7621983, -10350335, 10539787, -8164410, 4853792, -2222153, 781514, -209172, 41823, -6047, 597, -36, 1]."
			],
			"xref": [
				"Cf. A220670, A217475, A220672."
			],
			"keyword": "sign,tabf",
			"offset": "1,1",
			"author": "_Wolfdieter Lang_, Jan 11 2013",
			"references": 2,
			"revision": 20,
			"time": "2019-12-10T00:05:42-05:00",
			"created": "2013-01-15T16:56:25-05:00"
		}
	]
}