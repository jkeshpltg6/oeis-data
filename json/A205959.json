{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A205959",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 205959,
			"data": "1,1,1,2,1,6,1,4,3,10,1,24,1,14,15,8,1,54,1,40,21,22,1,96,5,26,9,56,1,900,1,16,33,34,35,216,1,38,39,160,1,1764,1,88,135,46,1,384,7,250,51,104,1,486,55,224,57,58,1,7200,1,62,189,32,65,4356,1,136",
			"name": "a(n) = n^omega(n)/rad(n).",
			"comment": [
				"a(n) = exp(-Sum_{d in P} moebius(d)*log(n/d)) where P = {d : d divides n and d is prime}. This is a variant of the (exponential of the) von Mangoldt function where the divisors are restricted to prime divisors. The (exponential of the) summatory function is A205957. Apart from n=1 the value is 1 if and only if n is prime; the fixed points are the products of two distinct primes (A006881)."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A205959/b205959.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Peter Luschny, \u003ca href=\"http://oeis.org/wiki/User:Peter_Luschny/VonMangoldtTransformation\"\u003eThe von Mangoldt Transformation\u003c/a\u003e."
			],
			"formula": [
				"a(n) = Product_{p|n} n/p. - _Charles R Greathouse IV_, Jun 27 2013",
				"a(n) = Product_{k=1..A001221(n)} n/A027748(n,k). - _Reinhard Zumkeller_, Dec 15 2013",
				"If n is squarefree, then a(n) = n^(omega(n)-1). - _Wesley Ivan Hurt_, Jun 09 2020",
				"a(p^e) = p^(e-1) for p prime, e \u003e 0. - _Bernard Schott_, Jun 09 2020"
			],
			"maple": [
				"with(numtheory): A205959 := proc(n) select(isprime, divisors(n));",
				"simplify(exp(-add(mobius(d)*log(n/d), d=%))) end:",
				"seq(A205959(i),i=1..60);"
			],
			"mathematica": [
				"a[n_] := Exp[-Sum[ MoebiusMu[d]*Log[n/d], {d, FactorInteger[n][[All, 1]]}]]; Table[a[n], {n, 1, 68}] (* _Jean-François Alcover_, Jan 15 2013 *)"
			],
			"program": [
				"(Sage)",
				"def A205959(n) :",
				"    P = filter(is_prime, divisors(n))",
				"    return simplify(exp(-add(moebius(d)*log(n/d) for d in P)))",
				"[A205959(n) for n in (1..60)]",
				"(PARI) a(n)=my(f=factor(n)[,1]);prod(i=1,#f,n/f[i]) \\\\ _Charles R Greathouse IV_, Jun 27 2013",
				"(Haskell)",
				"a205959 n = product $ map (div n) $ a027748_row n",
				"-- _Reinhard Zumkeller_, Dec 15 2013"
			],
			"xref": [
				"Cf. A003418, A025527, A008578, A102467, A006881, A205957."
			],
			"keyword": "nonn,nice",
			"offset": "1,4",
			"author": "_Peter Luschny_, Feb 03 2012",
			"ext": [
				"New name from _Charles R Greathouse IV_, Jun 30 2013"
			],
			"references": 5,
			"revision": 40,
			"time": "2020-06-15T16:32:54-04:00",
			"created": "2012-02-05T12:23:56-05:00"
		}
	]
}