{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A166363",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 166363,
			"data": "0,2,2,1,3,1,2,3,2,2,3,2,2,4,1,2,3,3,3,3,2,2,5,2,3,4,1,3,3,3,4,3,3,3,4,3,3,4,1,3,3,5,3,4,4,3,3,3,4,3,3,4,4,4,2,3,4,3,3,4,5,3,5,4,2,3,3,6,2,4,5,3,2,2,3,6,3,6,3,4,4,6,3,4,3,4,4,4,2,3,6,3,3,2,6,5,2,6,3,5,3,2,5,4,4",
			"name": "Number of primes in the half-open interval (n*(log(n))^2..(n+1)*(log(n+1))^2].",
			"comment": [
				"The open-closed half-open intervals form a partition of the real line for x \u003e 0, thus each prime appears in a unique interval.",
				"The n-th interval length is ~ (log(n+1/2))^2 + 2*log(n+1/2) ~ (log(n))^2 as n goes to infinity.",
				"The n-th interval prime density is ~ 1/[log(n+1/2)+2*log(log(n+1/2))] ~ 1/log(n) as n goes to infinity.",
				"The expected number of primes in the n-th interval is ~ [(log(n+1/2))^2 + 2*log(n+1/2)] / [log(n+1/2)+2*log(log(n+1/2))] ~ log(n) as n goes to infinity.",
				"For n = 1 there is no prime.",
				"If it can be proved that each interval always contains at least one prime, this would constitute even shorter intervals than A166332(n), let alone A143898(n), as n gets large.",
				"The Shanks Conjecture and the Cramer-Granville Conjecture tell us that the intervals of length (log(n))^2 are of very critical length (the constant M \u003e 1 of the Cramer-Granville Conjecture definitely matters!). There seems to be some risk that one such interval does not contain a prime.",
				"The Wolf Conjecture (which agrees better with numerical evidence) seems more in favor of each interval's containing at least one prime.",
				"From _Charles R Greathouse IV_, May 13 2010: (Start)",
				"Not all intervals \u003e 1 contain primes!",
				"a(n) = 0 for n = 1, 4977, 17512, 147127, 76082969 (and no others up to 10^8).",
				"Higher values include 731197850, 2961721173, 2103052050563, 188781483769833, 1183136231564246 but this list is not exhaustive.",
				"The intervals have length (log n)^2 + 2*log n + o(1). In the Cramer model, the probability that a given integer in the interval would be prime is approximately 1/(log n + 2*log log n). Tedious calculation gives the probability that a(n) = 0 in the Cramer model as 3C(log n)^2/n * (1 + o(1)) with C = exp(-5/2)/3. Thus under that model we would expect to find roughly C*(log N)^3 numbers n up to N with a(n) = 0. In fact, the numbers are not that common since the probabilities are not independent.",
				"(End)"
			],
			"link": [
				"Charles R Greathouse IV, \u003ca href=\"/A166363/b166363.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PrimeGaps.html\"\u003ePrimeGaps\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Cramer-GranvilleConjecture.html\"\u003eCramer-Granville Conjecture\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ShanksConjecture.html\"\u003eShanks Conjecture\u003c/a\u003e (and Wolf Conjecture.)"
			],
			"formula": [
				"a(n) = pi((n+1)*(log(n+1))^2) - pi(n*(log(n))^2) since the intervals are half-open properly."
			],
			"program": [
				"(PARI) a(n)=sum(k=ceil(n*log(n)^2),floor((n+1)*log(n+1)^2), isprime(k)) \\\\ _Charles R Greathouse IV_, Aug 21 2015"
			],
			"xref": [
				"Cf. A166332, A000720, A111943, A143898, A134034, A143935, A144140 (primes between successive n^K, for different K), A014085 (primes between successive squares).",
				"Cf. A182315."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Daniel Forgues_, Oct 12 2009",
			"ext": [
				"Edited by _Daniel Forgues_, Oct 18 2009 and Nov 01 2009",
				"Edited by _Charles R Greathouse IV_, May 13 2010"
			],
			"references": 5,
			"revision": 21,
			"time": "2015-08-21T09:29:17-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}