{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A134058",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 134058,
			"data": "1,2,2,2,4,2,2,6,6,2,2,8,12,8,2,2,10,20,20,10,2,2,12,30,40,30,12,2,2,14,42,70,70,42,14,2,2,16,56,112,140,112,56,16,2,2,18,72,168,252,252,168,72,18,2",
			"name": "Triangle T(n, k) = 2*binomial(n, k) with T(0, 0) = 1, read by rows.",
			"comment": [
				"Triangle T(n,k), 0 \u003c= k \u003c= n, read by rows, given by [2, -1, 0, 0, 0, 0, 0, ...] DELTA [2, -1, 0, 0, 0, 0, 0, ...] where DELTA is the operator defined in A084938. - _Philippe Deléham_, Oct 07 2007",
				"Equals A028326 for all but the first term. - _R. J. Mathar_, Jun 08 2008",
				"Warning: the row sums do not give A046055. - _N. J. A. Sloane_, Jul 08 2009"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A134058/b134058.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"Double Pascal's triangle and replace leftmost column with (1,2,2,2,...).",
				"M*A007318, where M = an infinite lower triangular matrix with (1,2,2,2,...) in the main diagonal and the rest zeros.",
				"Sum_{k=0..n} T(n,k) = A151821(n+1). - _Philippe Deléham_, Sep 17 2009",
				"G.f.: (1+x+y)/(1-x-y). - _Vladimir Kruchinin_, Apr 09 2015",
				"T(n, k) = 2*binomial(n, k) - [n=0]. - _G. C. Greubel_, Apr 26 2021"
			],
			"example": [
				"First few rows of the triangle:",
				"  1",
				"  2,  2;",
				"  2,  4,  2;",
				"  2,  6,  6,  2;",
				"  2,  8, 12,  8,  2;",
				"  2, 10, 20, 20, 10,  2;",
				"  ..."
			],
			"mathematica": [
				"T[n_, k_]:= SeriesCoefficient[(1+x+y)/(1-x-y), {x, 0, n-k}, {y, 0, k}];",
				"Table[T[n, k], {n,0,12}, {k,0,n}]//Flatten (* _Jean-François Alcover_, Apr 09 2015, after _Vladimir Kruchinin_ *)",
				"Table[2*Binomial[n,k] -Boole[n==0], {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, Apr 26 2021 *)"
			],
			"program": [
				"(Magma)",
				"A134058:= func\u003c n,k | n eq 0 select 1 else 2*Binomial(n,k) \u003e;",
				"[A134058(n,k): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Apr 26 2021",
				"(Sage)",
				"def A134058(n,k): return 2*binomial(n,k) - bool(n==0)",
				"flatten([[A134058(n,k) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Apr 26 2021"
			],
			"xref": [
				"Cf. A028326, A134059, A173048, A173049."
			],
			"keyword": "nonn,tabl",
			"offset": "0,2",
			"author": "_Gary W. Adamson_, Oct 05 2007",
			"ext": [
				"Title changed by _G. C. Greubel_, Apr 26 2021"
			],
			"references": 10,
			"revision": 22,
			"time": "2021-04-27T01:05:13-04:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}