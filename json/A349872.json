{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A349872",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 349872,
			"data": "2,3,5,7,11,4,13,17,6,19,23,9,29,10,8,31,37,41,14,15,43,12,47,53,59,21,61,22,25,67,18,16,71,26,20,73,79,83,33,27,35,89,34,97,101,103,24,28,38,39,30,107,109,49,113,127,131,46,137,51,55,139,149,151,32,45,157,36,42,57,163,58,44",
			"name": "a(1) = 2; for n \u003e 1, a(n) is the smallest unused number \u003e 1 such that none of the previous a(n) terms divide a(n).",
			"comment": [
				"Slightly under two-thirds of the terms are between the lines a(n) = n/2 and a(n) = n; in the range studied all of these numbers are composite. The remaining terms, all of which are primes, lie approximately on a curve that starts with a slope near 2 that slowly increases. See the linked image.",
				"There are no fixed points up to 10000 terms so it is likely none exist.",
				"For many even terms a(n) where n \u003e a(n) it is found that a(n - a(n) - 1) = a(n)/2. The first even term where that is not the case is a(113) = 68, as a(113 - 68 - 1) = a(44) = 97, not 34. In this case a(43) = 34.",
				"For a given number k the longest possible sequence of unique numbers that contains a number every k terms that divides k is finite, ~ 2*sqrt(k)*k; this implies all numbers \u003e 1 eventually appear."
			],
			"link": [
				"Scott R. Shannon, \u003ca href=\"/A349872/a349872.png\"\u003eImage of the first 10000 terms\u003c/a\u003e. The green line is a(n) = n."
			],
			"formula": [
				"a(2) = 3 as the previous term 2 does not divide 3.",
				"a(6) = 4 as none of the previous four terms, 3, 5, 7, 11, divide 4.",
				"a(9) = 6 as none of the previous six terms, 5, 7, 11, 4, 13, 17, divide 6."
			],
			"mathematica": [
				"a[1]=2;a[n_]:=a[n]=(k=2;While[MemberQ[s=Array[a,n-1],k]||Or@@(IntegerQ/@(k/s[[-If[k\u003e=n,n-1,k];;]])),k++];k);Array[a,73] (* _Giorgos Kalogeropoulos_, Dec 03 2021 *)"
			],
			"program": [
				"(Python)",
				"def aupton(terms):",
				"    alst, aset = [2], {2}",
				"    for n in range(2, terms+1):",
				"        k = 2",
				"        while k in aset or any(k%j == 0 for j in alst[-k:]): k += 1",
				"        alst.append(k); aset.add(k)",
				"    return alst",
				"print(aupton(73)) # _Michael S. Branicky_, Dec 03 2021"
			],
			"xref": [
				"Cf. A027750, A093714, A064413, A000040."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Scott R. Shannon_, Dec 03 2021",
			"references": 1,
			"revision": 23,
			"time": "2021-12-03T19:27:29-05:00",
			"created": "2021-12-03T15:49:40-05:00"
		}
	]
}