{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A300562",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 300562,
			"data": "0,1,11100,111,111100100,110011100,111110000,1111100,1110011,1100111100100,1111100100100,1111000011100,1110011100100,1100111110000,11100100111100100,11111,1111110000100,11001110011,1111110010000,11110000111100100",
			"name": "Condensed deep factorization of n, in binary. (Remove all trailing 0's and one trailing 1 from A300560.)",
			"comment": [
				"The binary representation of the deep factorization of n, A300560, is obtained by recursively replacing any factor prime(i)^e_i by the expression [i [e_i]], and finally taking '[' and ']' as binary digits 1 and 0.",
				"This always ends in trailing 0's which can be safely removed without loss of information; then there is a final binary digit 1 that can also be dropped. The result is a(n), in binary, equal to A300563(n) when converted to decimal.",
				"The initial a(1) = 0 results from the empty factorization of 1.",
				"To reconstruct the deep factorization of n \u003e 1, append a digit 1 and then as many 0's (namely: A145037(a(n))+1) as to have the same number of as 1's."
			],
			"formula": [
				"a(n) = A007088(A300563(n)), see there for an expression in terms of A300560. - _M. F. Hasler_, Mar 16 2018"
			],
			"example": [
				"The first term a(1) = 0 represents, by convention, the empty factorization of the number 1.",
				"To reconstruct the full deep factorization A300560(n), append a digit 1 and then as many 0's as to balance the number of 1's:",
				"a(2) = 1, append a 1 =\u003e 11; append two 0's =\u003e 1100 = A300560(2).",
				"a(3) = 11100, append a 1 =\u003e 111001; append two 0's =\u003e 11100100 = A300560(3).",
				"a(4) = 111, append a 1 =\u003e 1111; append four 0's =\u003e 11110000 = A300560(4)."
			],
			"program": [
				"(PARI) A300562(n)=(n=eval(A300560(n)))\\10^valuation(10*n+!n,10)"
			],
			"xref": [
				"Cf. A300560, A300561, A300563.",
				"Cf. A061396, A062504, A062860."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_M. F. Hasler_, Mar 08 2018",
			"references": 4,
			"revision": 8,
			"time": "2018-03-17T17:42:01-04:00",
			"created": "2018-03-11T17:16:33-04:00"
		}
	]
}