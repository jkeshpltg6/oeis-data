{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A292581",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 292581,
			"data": "0,3,12,10,12,39,36,46,30,63,48,106,24,93,216,148,24,141,60,196,162,141,120,304,60,111,162,232,42,459,108,394,174,141,372,442,54,183,420,538,42,489,78,352,540,243,198,904,102,303,294,334,78,513",
			"name": "Number of solutions to 4/n = 1/x + 1/y + 1/z in positive integers.",
			"comment": [
				"Corrected version of A192786.",
				"The Erdos-Straus conjecture is that a(n) \u003e 0 for n \u003e 1. Swett verified the conjecture for n \u003c 10^14.",
				"Vaughan shows that the number of n \u003c x with a(n) = 0 is at most x exp(-c * (log x)^(2/3)) for some c \u003e 0.",
				"After a(2) = 3, the values shown are all composite. [_Jonathan Vos Post_, Jul 17 2011]"
			],
			"link": [
				"Hugo Pfoertner, \u003ca href=\"/A292581/b292581.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Christian Elsholtz and Terence Tao, \u003ca href=\"https://arxiv.org/abs/1107.1010\"\u003eCounting the number of solutions to the Erdos-Straus equation on unit fractions\u003c/a\u003e, arXiv:1107.1010 [math.NT], 2011-2015.",
				"Allan Swett, \u003ca href=\"https://web.archive.org/web/20110716110731/http://math.uindy.edu/swett/esc.htm\"\u003eThe Erdos-Strauss Conjecture\u003c/a\u003e, 1999.",
				"R. C. Vaughan, \u003ca href=\"http://dx.doi.org/10.1112/S0025579300002886\"\u003eOn a problem of Erdős, Straus and Schinzel\u003c/a\u003e, Mathematika 17 (1970), pp. 193-198."
			],
			"example": [
				"a(3)=12 because 4/3 can be expressed in 12 ways:",
				"  4/3 =  1/1  + 1/4  + 1/12",
				"  4/3 =  1/1  + 1/6  + 1/6",
				"  4/3 =  1/1  + 1/12 + 1/4",
				"  4/3 =  1/2  + 1/2  + 1/3",
				"  4/3 =  1/2  + 1/3  + 1/2",
				"  4/3 =  1/3  + 1/2  + 1/2",
				"  4/3 =  1/4  + 1/1  + 1/12",
				"  4/3 =  1/4  + 1/12 + 1/1",
				"  4/3 =  1/6  + 1/1  + 1/6",
				"  4/3 =  1/6  + 1/6  + 1/1",
				"  4/3 =  1/12 + 1/1  + 1/4",
				"  4/3 =  1/12 + 1/4  + 1/1",
				"a(4) = 10 because 4/4 = 1 can be expressed in 10 ways:",
				"  4/4=  1/2 + 1/3 + 1/6",
				"  4/4=  1/2 + 1/4 + 1/4",
				"  4/4=  1/2 + 1/6 + 1/3",
				"  4/4=  1/3 + 1/2 + 1/6",
				"  4/4=  1/3 + 1/3 + 1/3",
				"  4/4=  1/3 + 1/6 + 1/2",
				"  4/4=  1/4 + 1/2 + 1/4",
				"  4/4=  1/4 + 1/4 + 1/2",
				"  4/4=  1/6 + 1/2 + 1/3",
				"  4/4=  1/6 + 1/3 + 1/2"
			],
			"mathematica": [
				"checkmult[a_, b_, c_] := If[Denominator[c] == 1, If[a == b \u0026\u0026 a == c \u0026\u0026 b == c, Return[1], If[a != b \u0026\u0026 a != c \u0026\u0026 b != c, Return[6], Return[3]]], Return[0]];",
				"a292581[n_] := Module[{t, t1, s, a, b, c, q = Quotient}, t = 4/n; s = 0; For[a = q[1, t]+1, a \u003c= q[3, t], a++, t1 = t - 1/a; For[b = Max[q[1, t1] + 1, a], b \u003c= q[2, t1], b++, c = 1/(t1 - 1/b); s += checkmult[a, b, c]]]; Return[s]];",
				"Reap[For[n=1, n \u003c= 54, n++, Print[n, \" \", an = a292581[n]]; Sow[an]]][[2, 1]] (* _Jean-François Alcover_, Dec 02 2018, adapted from PARI *)"
			],
			"program": [
				"(PARI) \\\\ modified version of code by _Charles R Greathouse IV_ in A192786",
				"checkmult (a,b,c) =",
				"{",
				"  if(denominator(c)==1,",
				"     if(a==b \u0026\u0026 a==c \u0026\u0026 b==c,",
				"        return(1),",
				"        if(a!=b \u0026\u0026 a!=c \u0026\u0026 b!=c,",
				"           return(6),",
				"           return(3)",
				"          )",
				"       ),",
				"     return(0)",
				"     )",
				"}",
				"a292581(n) =",
				"{",
				"  local(t, t1, s, a, b, c);",
				"  t = 4/n;",
				"  s = 0;",
				"  for (a=1\\t+1, 3\\t,",
				"     t1=t-1/a;",
				"     for (b=max(1\\t1+1,a), 2\\t1,",
				"          c=1/(t1-1/b);",
				"          s+=checkmult(a,b,c);",
				"         )",
				"      );",
				"     return(s);",
				"}",
				"for (n=1,54,print1(a292581(n),\", \"))"
			],
			"xref": [
				"For more references and links see A192787.",
				"Cf. A073101, A192786, A292624, A337432."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Hugo Pfoertner_, Sep 20 2017",
			"references": 7,
			"revision": 30,
			"time": "2020-10-15T16:56:31-04:00",
			"created": "2017-09-20T05:09:53-04:00"
		}
	]
}