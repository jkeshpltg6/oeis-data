{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A128605",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 128605,
			"data": "3,1,6,72,120,1800,840,3600,2520,28800,10080,88200,27720,259200,50400,176400,83160,352800,138600,3484800,277200,1411200,360360,2822400,831600,3175200,720720,6350400,1663200,31363200,1441440,28576800,2162160,12700800,3326400,21344400,4324320",
			"name": "Smallest number m having exactly n divisors d with sqrt(m/2) \u003c= d \u003c sqrt(2*m).",
			"comment": [
				"A067742(a(n)) = n and A067742(m) \u003c\u003e n for m \u003c a(n).",
				"From _Hartmut F. W. Hoft_, Feb 06 2017: (Start)",
				"a(66)=86486400 has the largest index n with a(n) \u003c= 100000000, but there are 12 values from a(38) to a(67) that are larger than 100000000.",
				"Conjecture: a(n) = k where p(k) and p(k-1) are the first pair of Dyck paths for the symmetric representation of sigma(k) and sigma(k-1), as described in A237593, having a gap of exactly n units on the diagonal, i.e., it is the sequence of record gaps in sequence A240542; tested through 2000000 with a variant of function A279286. (End)",
				"The first 37 terms are 13-smooth (see A080197). - _David A. Corneth_, Apr 07 2018"
			],
			"link": [
				"David A. Corneth, \u003ca href=\"/A128605/a128605_4.gp.txt\"\u003eUpper bounds on a(0)..a(376) and some more values\u003c/a\u003e",
				"Christian Kassel and Christophe Reutenauer, \u003ca href=\"https://arxiv.org/abs/1505.07229v3\"\u003eThe zeta function of the Hilbert scheme of n points on a two-dimensional torus\u003c/a\u003e, arXiv:1505.07229v3 [math.AG], 2015, see page 29 Remarks 6.8(b). [Note that a later version of this paper has a different title and different contents, and the number-theoretical part of the paper was moved to the publication which is next in this list.]",
				"Christian Kassel and Christophe Reutenauer, \u003ca href=\"https://arxiv.org/abs/1610.07793\"\u003eComplete determination of the zeta function of the Hilbert scheme of n points on a two-dimensional torus\u003c/a\u003e, arXiv:1610.07793 [math.NT], 2016, see Remark 1.3."
			],
			"example": [
				"A067742(a(5)) = A067742(1800) = #{30,36,40,45,50} = 5;",
				"A067742(a(6)) = A067742(840) = #{21,24,28,30,35,40} = 6;",
				"A067742(a(7)) = A067742(3600) = #{45,48,50,60,72,75,80} = 7.",
				"a(0)=3 since 3 has no middle divisors. - _Hartmut F. W. Hoft_, Feb 06 2017"
			],
			"mathematica": [
				"(* computation based on the function of _Michael Somos_ in A067742 *)",
				"a128605[pL_,b_] := Module[{posL=Map[0\u0026, Range[pL]], k=1, mCur, count}, While[k\u003c=b, mCur=DivisorSum[k, 1\u0026, k/2 \u003c= #^2 \u003c 2k\u0026]; If[posL[[mCur]]==0, posL[[mCur]]=k]; k++]; Prepend[posL, 3]]",
				"a128605[70,100000000] (* computes those a(0) .. a(66) \u003c= 100000000 *)",
				"(* _Hartmut F. W. Hoft_, Feb 06 2017 *)"
			],
			"program": [
				"(PARI) ct(m)=my(lower=if(m%2==0\u0026\u0026issquare(m/2), sqrtint(m/2), sqrtint(m\\2)+1), upper=sqrtint(2*m)); sumdiv(m, d, lower\u003c=d \u0026\u0026 d\u003c=upper)",
				"v=vector(10^3); need=1; for(m=1, 1e9, t=ct(m); if(t\u003e=need \u0026\u0026 v[t]==0, v[t]=m; print(\"a(\"t\") = \"n); while(v[need], need++))) \\\\ _Charles R Greathouse IV_, Feb 06 2017"
			],
			"xref": [
				"Cf. A067742.",
				"Related to Dyck paths: A237593, A240542, A279286."
			],
			"keyword": "nonn",
			"offset": "0,1",
			"author": "_Reinhard Zumkeller_, Mar 14 2007",
			"ext": [
				"a(33)-a(37) from _Hartmut F. W. Hoft_, Feb 06 2017"
			],
			"references": 6,
			"revision": 42,
			"time": "2019-04-27T20:24:03-04:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}