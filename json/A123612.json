{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A123612",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 123612,
			"data": "1,1,2,3,5,8,17,31,68,145,325,728,1685,3891,9140,21565,51311,122666,295037,712477,1728262,4207027,10276693,25178708,61866141,152397945,376309596,931239093,2309219447,5737078442,14278587533,35595622719",
			"name": "Antidiagonal sums of triangle A123610.",
			"comment": [
				"The g.f. was suggested by P. D. Hanna. It can be proved either by letting y=x in the bivariate g.f. for sequence A123610 or by using the formula of A. Howroyd (below) for this sequence and the l.g.f. for sequence A167539. The second proof proceeds as follows: Sum_{n\u003e=1} a(n)*x^n = Sum_{n\u003e=1} (1/n)*Sum_{d|n} phi(n/d)*g(d), where g(d) = A167539(d). Then Sum_{n\u003e=1} a(n)*x^n = Sum_{m\u003e=1} (phi(m)/m)*Sum_{d\u003e=1} g(d)*(x^m)^d/d = Sum_{m\u003e=1} (phi(m)/m)*G(x^m), where G(x) = l.g.f. of sequence g(n) = A167539(n). - _Petros Hadjicostas_, Oct 25 2017"
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A123612/b123612.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e"
			],
			"formula": [
				"a(n) = (1/n) * Sum_{d | n} phi(n/d) * A167539(d) for n\u003e0. - _Andrew Howroyd_, Apr 02 2017",
				"G.f.: 1-Sum_{n\u003e=1} (phi(n)/n)*f(x^n), where f(x) = log((1-x-x^2+sqrt((1+x+x^2)*(1-3*x+x^2))/2) = -log((1-x-x^2-sqrt((1+x+x^2)*(1-3*x+x^2))/(2*x^3)). - _Petros Hadjicostas_, Oct 25 2017"
			],
			"mathematica": [
				"Total /@ Table[Function[m, If[k == 0, 1, 1/m DivisorSum[m, If[GCD[k, #] == #, EulerPhi[#] Binomial[m/#, k/#]^2, 0] \u0026]]][n - k + 1], {n, -1, 30}, {k, 0, Ceiling[n/2]}] (* _Michael De Vlieger_, Apr 03 2017, after _Jean-François Alcover_ at A123610 *)"
			],
			"program": [
				"(PARI) {a(n)=sum(k=0,n\\2,if(k==0,1,(1/(n-k))*sumdiv(n-k,d,if(gcd(k,d)==d, eulerphi(d)*binomial((n-k)/d,k/d)^2,0))))}"
			],
			"xref": [
				"Cf. A123610 (triangle), A123611 (row sums); central terms: A123617, A123618, A167539."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Paul D. Hanna_, Oct 03 2006",
			"references": 3,
			"revision": 20,
			"time": "2017-10-25T12:20:19-04:00",
			"created": "2006-10-09T03:00:00-04:00"
		}
	]
}