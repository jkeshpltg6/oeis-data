{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A237840",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 237840,
			"data": "1,2,2,1,3,3,2,1,1,2,1,2,2,1,2,1,1,2,1,2,3,1,3,4,3,4,4,3,3,4,2,2,4,2,3,2,1,3,2,2,2,1,2,1,2,2,2,4,3,2,2,1,3,4,3,1,3,1,2,4,2,5,2,3,2,3,1,3,2,4,4,1,3,2,4,2,4,4,4,4",
			"name": "a(n) = |{0 \u003c k \u003c= n: the number of twin prime pairs not exceeding k*n is a square}|.",
			"comment": [
				"Conjecture: (i) a(n) \u003e 0 for all n \u003e 0, and a(n) = 1 for no n \u003e 159.",
				"(ii) For every n = 1, 2, 3, ..., there is a positive integer k \u003c= n such that the number |{{p, 2*p+1}: both p and 2*p + 1 are primes not exceeding k*n}| is a square.",
				"We have verified that a(n) \u003e 0 for all n = 1, ..., 22000.",
				"See also A237879 for the least k among 1, ..., n such that the number of twin prime pairs not exceeding k*n is a square."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A237840/b237840.txt\"\u003eTable of n, a(n) for n = 1..9000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;68a81953.1402\"\u003eA surprising conjecture on primes and squares\u003c/a\u003e, a message to the Number Theory List, Feb. 14, 2014.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1402.6641\"\u003eProblems on combinatorial properties of primes\u003c/a\u003e, arXiv:1402.6641, 2014."
			],
			"example": [
				"a(4) = 1 since the number of twin prime pairs not exceeding 1*4 = 4 is 0^2.",
				"a(9) = 1 since there are exactly 2^2 twin prime pairs not exceeding 3*9 = 27 (namely, they are {3, 5}, {5, 7}, {11, 13} and {17, 19}).",
				"a(18055) \u003e 0 since there are exactly 675^2 = 455625 twin prime pairs not exceeding 5758*18055 = 103960690.",
				"a(18120) \u003e 0 since there are exactly 729^2 = 531441 twin prime pairs not exceeding 6827*18120 = 123705240.",
				"a(18307) \u003e 0 since there are exactly 681^2 = 463761 twin prime pairs not exceeding 5792*18307 = 106034144.",
				"a(18670) \u003e 0 since there are exactly 683^2 = 466489 twin prime pairs not exceeding 5716*18670 = 106717720.",
				"a(19022) \u003e 0 since there are exactly 737^2 = 543169 twin prime pairs not exceeding 6666*19022 = 126800652.",
				"a(19030) \u003e 0 since there are exactly 706^2 = 498436 twin prime pairs not exceeding 6045*19030 = 115036350.",
				"a(19805) \u003e 0 since there are exactly 717^2 = 514089 twin prime pairs not exceeding 6015*19805 = 119127075.",
				"a(19939) \u003e 0 since there are exactly 1000^2 = 10^6 twin prime pairs not exceeding 12660*19939 = 252427740.",
				"a(20852) \u003e 0 since there are exactly 747^2 = 558009 twin prime pairs not exceeding 6268*20852 = 130700336.",
				"a(21642) \u003e 0 since there are exactly 724^2 = 524176 twin prime pairs not exceeding 5628*21642 = 121801176."
			],
			"mathematica": [
				"tw[0]:=0",
				"tw[n_]:=tw[n-1]+If[PrimeQ[Prime[n]+2],1,0]",
				"SQ[n_]:=IntegerQ[Sqrt[tw[PrimePi[n]]]]",
				"a[n_]:=Sum[If[SQ[k*n-2],1,0],{k,1,n}]",
				"Table[a[n],{n,1,80}]"
			],
			"xref": [
				"Cf. A000290, A001359, A005384, A006512, A237706, A237710, A237578, A237598, A237612, A237769, A237817, A237839, A237879, A237975."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Zhi-Wei Sun_, Feb 14 2014",
			"references": 5,
			"revision": 46,
			"time": "2014-03-16T11:59:48-04:00",
			"created": "2014-02-14T03:29:52-05:00"
		}
	]
}