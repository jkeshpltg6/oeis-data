{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005129",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5129,
			"id": "M5309",
			"data": "1,0,54,72,0,432,270,0,918,720,0,2160,936,0,2700,2160,0,5184,2214,0,5616,3600,0,9504,4590,0,9180,6552,0,15120,5184,0,14742,10800,0,21600,9360,0,19548,12240,0,30240,13500,0,28080,17712,0,39744,14760,0,32454",
			"name": "Theta series of {E_6}* lattice.",
			"comment": [
				"Cubic AGM theta functions: a(q) (see A004016), b(q) (A005928), c(q) (A005882)."
			],
			"reference": [
				"J. H. Conway and N. J. A. Sloane, \"Sphere Packings, Lattices and Groups\", Springer-Verlag, p. 127.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"N. Heninger, E. M. Rains and N. J. A. Sloane, \u003ca href=\"http://arXiv.org/abs/math.NT/0509316\"\u003eOn the Integrality of n-th Roots of Generating Functions\u003c/a\u003e, J. Combinatorial Theory, Series A, 113 (2006), 1732-1745.",
				"G. Nebe and N. J. A. Sloane, \u003ca href=\"http://www.math.rwth-aachen.de/~Gabriele.Nebe/LATTICES/Es6.html\"\u003eHome page for this lattice\u003c/a\u003e"
			],
			"formula": [
				"Expansion of b(q)^3 + c(q)^3 / 3 in power of q where b(), c() are cubic AGM theta functions.",
				"Expansion of (eta(q)^3 / eta(q^3))^3 + 9 * (eta(q^3)^3 / eta(q))^3 in powers of q."
			],
			"example": [
				"G.f. = 1 + 54*x^2 + 72*x^3 + 432*x^5 + 270*x^6 + 918*x^8 + 720*x^9 + 2160*x^11 + ..."
			],
			"mathematica": [
				"a[n_] := Module[{A = x*O[x]^n}, SeriesCoefficient[(QPochhammer[x+A]^3 / QPochhammer[x^3+A])^3 + 9*x*(QPochhammer[x^3+A]^3 / QPochhammer[x+A])^3, {x, 0, n}]]; Table[a[n], {n, 0, 80}] (* _Jean-François Alcover_, Nov 05 2015, adapted from 1st PARI script *)",
				"a[ n_] := SeriesCoefficient[ (QPochhammer[ q]^3 / QPochhammer[ q^3])^3 + 9 q (QPochhammer[ q^3]^3 /QPochhammer[ q])^3 , {q, 0, n}]; ( _Michael Somos_, Dec 28 2015 )"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( (eta(x + A)^3 / eta(x^3 + A))^3 + 9 * x * (eta(x^3 + A)^3 / eta(x + A))^3, n))}; /* _Michael Somos_, Feb 28 2012 */",
				"(PARI) {a(n) = my(A, a1, p3); if( n\u003c0, 0, A = x * O(x^n); a1 = sum( k=1, n, 6 * sumdiv(k, d, kronecker( d, 3)) * x^k, 1 + A); p3 = sum( k=1, n\\3, -24 * sigma(k) * x^(3*k), 1 + A); polcoeff( (a1^3 + a1 * p3 - 4 * x * a1') / 2, n))}; /* _Michael Somos_, Feb 28 2012 */",
				"(MAGMA) A := Basis( ModularForms( Gamma1(3), 3), 51); A[1]; /* _Michael Somos_, Dec 28 2015 */"
			],
			"xref": [
				"Cf. A004007 (E_6)."
			],
			"keyword": "nonn,nice,easy",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"references": 2,
			"revision": 32,
			"time": "2021-09-06T11:11:45-04:00",
			"created": "1991-05-16T03:00:00-04:00"
		}
	]
}