{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A255551",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 255551,
			"data": "2,4,3,6,5,7,8,11,19,9,10,17,39,27,13,12,23,61,57,45,15,14,29,81,91,97,55,21,16,35,103,121,147,117,85,25,18,41,123,153,199,181,177,109,31,20,47,145,183,253,243,277,225,139,33,22,53,165,217,301,315,369,345,295,157,37,24,59,187,247,351,379,471,465,447,325,175,43",
			"name": "Lucky / Unlucky array, shifted version, read by antidiagonals A(1,1), A(1,2), A(2,1), A(1,3), A(2,2), A(3,1), ...",
			"comment": [
				"Note how in comparison to A255545, the even numbers on the first row have been shifted one step left, \"pushing\" term 1 out of the array proper. This was done to obtain a better alignment with arrays like A083221 and A255127 associated with other sieves, from which one may then induce permutations like A255553 by cross-referencing.",
				"The starting offset of the sequence giving the terms in square array is 2. However, we can tacitly assume that a(1) = 1 when the sequence is used one-dimensionally as a permutation of natural numbers."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A255551/b255551.txt\"\u003eTable of n, a(n) for n = 2..5996; the first 109 antidiagonals of the array, flattened\u003c/a\u003e"
			],
			"formula": [
				"For row = 1, A(row,col) = 2*col; For row \u003e 1 and col = 1, A(row,col) = A000959(row); otherwise, A(row,col) = A255543(row,col-1)."
			],
			"example": [
				"The top left corner of the array:",
				"   2,   4,   6,   8,  10,  12,  14,   16,   18,   20,   22,   24,   26,   28,   30",
				"   3,   5,  11,  17,  23,  29,  35,   41,   47,   53,   59,   65,   71,   77,   83",
				"   7,  19,  39,  61,  81, 103, 123,  145,  165,  187,  207,  229,  249,  271,  291",
				"   9,  27,  57,  91, 121, 153, 183,  217,  247,  279,  309,  343,  373,  405,  435",
				"  13,  45,  97, 147, 199, 253, 301,  351,  403,  453,  507,  555,  609,  661,  709",
				"  15,  55, 117, 181, 243, 315, 379,  441,  505,  571,  633,  697,  759,  825,  889",
				"  21,  85, 177, 277, 369, 471, 567,  663,  757,  853,  949, 1045, 1141, 1239, 1333",
				"  25, 109, 225, 345, 465, 589, 705,  829,  945, 1063, 1185, 1305, 1423, 1549, 1669",
				"  31, 139, 295, 447, 603, 765, 913, 1075, 1227, 1377, 1537, 1689, 1843, 1999, 2155",
				"  33, 157, 325, 493, 667, 835, 999, 1177, 1347, 1513, 1687, 1861, 2029, 2205, 2367",
				"..."
			],
			"program": [
				"(Scheme)",
				"(define (A255551 n) (if (\u003c= n 1) n (A255551bi (A002260 (- n 1)) (A004736 (- n 1)))))",
				"(define (A255551bi row col) (cond ((= 1 row) (+ col col)) ((= 1 col) (A000959 row)) (else (A255543bi row (- col 1)))))",
				";; Other code as in A255543."
			],
			"xref": [
				"Inverse: A255552.",
				"Variant of array A255545. (See also A255543).",
				"Row 1: A005843 (even numbers).",
				"Column 1: 2 followed by A000959(2..) (Lucky numbers from their second term onward).",
				"Main diagonal: A255550.",
				"Similar arrays: A083221, A255127.",
				"Associated permutations: A255553, A255554."
			],
			"keyword": "nonn,tabl",
			"offset": "2,1",
			"author": "_Antti Karttunen_, Feb 26 2015",
			"references": 19,
			"revision": 15,
			"time": "2015-02-28T14:49:23-05:00",
			"created": "2015-02-28T14:49:23-05:00"
		}
	]
}