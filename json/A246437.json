{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A246437",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 246437,
			"data": "1,0,2,3,10,25,71,196,554,1569,4477,12826,36895,106470,308114,893803,2598314,7567465,22076405,64498426,188689685,552675364,1620567764,4756614061,13974168191,41088418150,120906613076,356035078101,1049120176954",
			"name": "Expansion of (1/2)*(1/(x+1)+1/(sqrt(-3*x^2-2*x+1))).",
			"comment": [
				"a(2101) has 1001 decimal digits. - _Michael De Vlieger_, Apr 25 2016",
				"This is the analog for Coxeter type B of Motzkin sums (A005043) for Coxeter type A, see the article by Athanasiadis and Savvidou. - _F. Chapoton_, Jul 20 2017",
				"Number of compositions of n into exactly n nonnegative parts avoiding part 1. a(4) = 10: 0004, 0022, 0040, 0202, 0220, 0400, 2002, 2020, 2200, 4000. - _Alois P. Heinz_, Aug 19 2018",
				"From _Peter Bala_, Jan 07 2022: (Start)",
				"The binary transform is A088218. The inverse binomial transform is a signed version of A027306 and the second inverse binomial transform is a signed version of A027914.",
				"The Gauss congruences a(n*p^k) == a(n^p^(k-1)) (mod p^k) hold for prime p and positive integers n and k.",
				"Conjecture: the stronger congruences a(n*p^k) == a(n^p^(k-1)) (mod p^(2*k)) hold for prime p \u003e= 5 and positive integers n and k. (End)"
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A246437/b246437.txt\"\u003eTable of n, a(n) for n = 0..2100\u003c/a\u003e",
				"Christos A. Athanasiadis and Christina Savvidou, \u003ca href=\"https://www.emis.de/journals/SLC/wpapers/s66athasav.html\"\u003eThe Local h-Vector of the Cluster Subdivision of a Simplex\u003c/a\u003e, Séminaire Lotharingien de Combinatoire 66 (2012), Article B66c.",
				"Eric Marberg, \u003ca href=\"https://arxiv.org/abs/1709.07446\"\u003eOn some actions of the 0-Hecke monoids of affine symmetric groups\u003c/a\u003e, arXiv:1709.07996 [math.CO], 2017."
			],
			"formula": [
				"a(n) = Sum_{k = 0..n/2} binomial(n,k)*binomial(n-k-1,n-2*k).",
				"A(x) = 1 + x*B'(x)/B(x), where B(x) = (1+x-sqrt(1-2*x-3*x^2))/(2*x*(1+x)) is the o.g.f. of A005043.",
				"a(n) = n*hypergeom([1-n, 1-n/2, 3/2-n/2],[2, 2-n], 4) for n\u003e=3. - _Peter Luschny_, Nov 14 2014",
				"a(n) ~ 3^(n+1/2) / (4*sqrt(Pi*n)). - _Vaclav Kotesovec_, Nov 14 2014",
				"a(n) = (-1)^n*(hypergeom([1/2, -n], [1], 4) + 1)/2. - _Vladimir Reshetnikov_, Apr 25 2016",
				"D-finite with recurrence: n*(a(n) - a(n-1)) = (5*n-6)*a(n-2) + 3*(n-2)*a(n-3). - _Vladimir Reshetnikov_, Oct 13 2016",
				"a(n) = [x^n]( (1 - x + x^2)/(1 - x) )^n. - _Peter Bala_, Jan 07 2022"
			],
			"mathematica": [
				"CoefficientList[Series[(1/2) (1 / (x + 1) + 1 / (Sqrt[-3 x^2 - 2 x + 1])), {x, 0, 40}], x] (* _Vincenzo Librandi_, Nov 14 2014 *)",
				"Table[(-1)^n (Hypergeometric2F1[1/2, -n, 1, 4] + 1)/2, {n, 0, 20}] (* _Vladimir Reshetnikov_, Apr 25 2016 *)",
				"Table[Sum[Binomial[n, k] Binomial[n - k - 1, n - 2 k], {k, 0, n/2}], {n, 0, 28}] (* _Michael De Vlieger_, Apr 25 2016 *)"
			],
			"program": [
				"(Maxima)",
				"a(n):=sum(binomial(n,k)*binomial(n-k-1,n-2*k),k,0,n/2);",
				"(Sage)",
				"def a(n):",
				"    if n \u003c 3: return [1,0,2][n]",
				"    return n*hypergeometric([1-n, 1-n/2, 3/2-n/2],[2, 2-n], 4)",
				"[simplify(a(n)) for n in (0..28)] # _Peter Luschny_, Nov 14 2014"
			],
			"xref": [
				"Cf. A002426, A005043, A027306, A027914, A088218."
			],
			"keyword": "nonn,easy,changed",
			"offset": "0,3",
			"author": "_Vladimir Kruchinin_, Nov 14 2014",
			"references": 7,
			"revision": 50,
			"time": "2022-01-08T22:04:38-05:00",
			"created": "2014-11-14T05:04:47-05:00"
		}
	]
}