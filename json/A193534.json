{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A193534",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 193534,
			"data": "3,7,3,5,5,0,7,2,7,8,9,1,4,2,4,1,8,0,3,9,2,2,8,2,0,4,5,3,9,4,6,5,9,7,2,1,4,0,2,8,5,5,3,7,1,2,4,4,1,6,1,7,7,3,8,1,6,4,0,1,6,4,1,9,6,4,9,0,9,8,5,3,0,5,2,2,1,9,7,2,2,6,9,2,7,5,3,8,8,7,0,7,1,8,8,0,4",
			"name": "Decimal expansion of (1/3) * (Pi/sqrt(3) - log(2)).",
			"comment": [
				"The formulas for this number and the constant in A113476 are exactly the same except for one small, crucial detail: the infinite sum has a denominator of 3i + 2 rather than 3i + 1, while in the closed form, log(2)/3 is subtracted from rather than added to (Pi * sqrt(3))/9.",
				"Understandably, the typesetter for Spiegel et al. (2009) set the closed formula for this number incorrectly (as being the same as for A113476, compare equation 21.16 on the same page of that book)."
			],
			"reference": [
				"Jolley, Summation of Series, Dover (1961) eq (80) page 16.",
				"Murray R. Spiegel, Seymour Lipschutz, John Liu. Mathematical Handbook of Formulas and Tables, 3rd Ed. Schaum's Outline Series. New York: McGraw-Hill (2009): p. 135, equation 21.18"
			],
			"link": [
				"Gheorghe Coserea, \u003ca href=\"/A193534/b193534.txt\"\u003eTable of n, a(n) for n = 0..2015\u003c/a\u003e",
				"Eric W. Weisstein, \u003ca href=\"https://mathworld.wolfram.com/EulersSeriesTransformation.html\"\u003eEuler's Series Transformation\u003c/a\u003e."
			],
			"formula": [
				"Equals Sum_{k \u003e= 0} (-1)^k/(3k + 2) = 1/2 - 1/5 + 1/8 - 1/11 + 1/14 - 1/17 + ... (see A016789).",
				"From _Peter Bala_, Feb 20 2015: (Start)",
				"Equals (1/2) * Integral_{x = 0..1} 1/(1 + x^(3/2)) dx.",
				"Generalized continued fraction: 1/(2 + 2^2/(3 + 5^2/(3 + 8^2/(3 + 11^2/(3 + ... ))))) due to Euler. For a sketch proof see A024396. (End)",
				"Equals (Psi(5/6)-Psi(1/3))/6. - _Vaclav Kotesovec_, Jun 16 2015",
				"Equals Integral_{x = 1..infinity} 1/(1 + x^3) dx. - _Robert FERREOL_, Dec 23 2016",
				"Equals (1/2)*Sum_{n \u003e= 0} n!*(3/2)^n/(Product_{k = 0..n} 3*k + 2) = (1/2)*Sum_{n \u003e= 0} n!*(3/2)^n/A008544(n+1) (apply Euler's series transformation to Sum_{k \u003e= 0} (-1)^k/(3*k + 2)). - _Peter Bala_, Dec 01 2021"
			],
			"example": [
				"0.373550727891424180392282045394659721402855371244161773816401641964909853052219..."
			],
			"maple": [
				"evalf((Psi(5/6)-Psi(1/3))/6, 120); # _Vaclav Kotesovec_, Jun 16 2015"
			],
			"mathematica": [
				"RealDigits[(Pi Sqrt[3])/9 - (Log[2]/3), 10, 100][[1]]"
			],
			"program": [
				"(PARI) (Pi/sqrt(3)-log(2))/3 \\\\ _Charles R Greathouse IV_, Jul 29 2011",
				"(PARI)",
				"default(realprecision, 98);",
				"eval(vecextract(Vec(Str(sumalt(n=0, (-1)^(n)/(3*n+2)))), \"3..-2\")) \\\\ _Gheorghe Coserea_, Oct 06 2015"
			],
			"xref": [
				"Cf. A073010, A193535, A024396, A113476, A258969."
			],
			"keyword": "nonn,cons",
			"offset": "0,1",
			"author": "_Alonso del Arte_, Jul 29 2011",
			"references": 5,
			"revision": 59,
			"time": "2021-12-19T07:40:10-05:00",
			"created": "2011-07-31T05:48:57-04:00"
		}
	]
}