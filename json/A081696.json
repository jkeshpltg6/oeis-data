{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A081696",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 81696,
			"data": "1,1,3,9,29,97,333,1165,4135,14845,53791,196417,721887,2667941,9907851,36950465,138320021,519515209,1957091277,7392602917,27992976565,106236268337,404005515873,1539293204549,5875059106769,22459721336977",
			"name": "Expansion of 1/(x + sqrt(1-4x)).",
			"comment": [
				"Number of irreducible ordered pairs of compositions of n. A pair of compositions of n into the same number of (positive) parts, say n = a1 + ... + ak and n = b1 + ... + bk, is irreducible if for all j \u003c k, a1 + ... + aj is not equal to b1 + ... + bj. E.g., a(3)=3 because the irreducible pairs are (1+2,2+1), (2+1,1+2), (3,3). - Herbert S. Wilf, May 22 2004",
				"Hankel transform is 2^n. - _Paul Barry_, Nov 22 2007",
				"Equals left border of triangle A152229. - _Gary W. Adamson_, Nov 29 2008",
				"Equals INVERTi transform of A000984: (1, 2, 6, 20, 70, 252, ...). - _Gary W. Adamson_, May 15 2009",
				"(1 + 3x + 9x^2 + 29x^3 + ...) * 1/(1 + x + 3x^2 + 9x^3 + 29x^4 + ...) = (1 + 2x + 4x^2 + 10x^3 + 28x^4 + ...); where A068875 = (1, 2, 4, 10, 28, ...). - _Gary W. Adamson_, Nov 21 2011",
				"Apparently the number of grand Motzkin paths of length n with two types of flat step (F,f) and avoiding F at level 0. - _David Scambler_, Jul 04 2013",
				"Starting at n=1 p-INVERT of Catalan numbers (A000108, starting at n=0), where p(S) = 1 - S - S^2; see A289780. - _Clark Kimberling_, Aug 12 2017"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A081696/b081696.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e",
				"Paul Barry, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL8/Barry/barry84.html\"\u003eA Catalan Transform and Related Transformations on Integer Sequences\u003c/a\u003e, Journal of Integer Sequences, Vol. 8 (2005), Article 05.4.5.",
				"P. Barry, \u003ca href=\"http://dx.doi.org/10.1016/j.laa.2015.10.032\"\u003eRiordan arrays, generalized Narayana triangles, and series reversion\u003c/a\u003e, Linear Algebra and its Applications, 491 (2016) 343-385.",
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/1912.11845\"\u003eChebyshev moments and Riordan involutions\u003c/a\u003e, arXiv:1912.11845 [math.CO], 2019.",
				"Paul Barry, Arnauld Mesinga Mwafise, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL21/Barry/barry362.html\"\u003eClassical and Semi-Classical Orthogonal Polynomials Defined by Riordan Arrays, and Their Moment Sequences\u003c/a\u003e, Journal of Integer Sequences, Vol. 21 (2018), Article 18.1.5.",
				"Edward A. Bender, Gregory F. Lawler, Robin Pemantle and Herbert S. Wilf, \u003ca href=\"http://arxiv.org/abs/math/0404253\"\u003eIrreducible compositions and the first return to the origin of a random walk\u003c/a\u003e, arXiv:math/0404253 [math.CO], 2004.",
				"Edward A. Bender, Gregory F. Lawler, Robin Pemantle and Herbert S. Wilf, \u003ca href=\"http://www.math.ucsd.edu/~ebender/reprints/110.pdf\"\u003eIrreducible compositions and the first return to the origin of a random walk\u003c/a\u003e, Sem. Lothar. 50 (2004) B50h.",
				"D. Callan, \u003ca href=\"http://arxiv.org/abs/1206.3174\"\u003eAn identity for the central binomial coefficient\u003c/a\u003e, arXiv preprint arXiv:1206.3174 [math.CO], 2012. - From _N. J. A. Sloane_, Nov 25 2012",
				"G. Chatel and V. Pilaud, \u003ca href=\"http://arxiv.org/abs/1411.3704\"\u003eCambrian Hopf Algebras\u003c/a\u003e, arXiv:1411.3704 [math.CO], 2014-2015.",
				"A. Umar, \u003ca href=\"http://www.mathnet.ru/adm33\"\u003eSome combinatorial problems in the theory of symmetric ...\u003c/a\u003e, Algebra Disc. Math. 9 (2010) 115-126."
			],
			"formula": [
				"G.f.: 1/(x + sqrt(1-4*x)).",
				"D-finite with recurrence: n*a(n) + 2*(-4*n+3)*a(n-1) + 3*(5*n-8)*a(n-2) + 2*(2*n-3)*a(n-3) = 0.",
				"a(n) = Sum_{k=0..n} binomial(2n-k,n+k)*(3k+1)/(n+k+1). - _Emanuele Munarini_, Apr 02 2011",
				"From _Paul Barry_, Dec 18 2004: (Start)",
				"A Catalan transform of the Fibonacci numbers F(n+1) under the mapping G(x) -\u003e G(xc(x)), c(x) the g.f. of A000108. The inverse mapping is H(x) -\u003e H(x(1-x)).",
				"a(n) = Sum{k=0..n} (k/(2n-k))binomial(2n-k, n-k)F(k+1). (End)",
				"From _Bill Gosper_, May 14 2011: (Start)",
				"We have (per _Wouter Meeussen_): a(n) = (Sum_{k=1..n} k*Fibonacci(k+1)*(-1)^(n+k)*binomial(-n,n-k))/n = (Sum_{k=1..n} k*Fibonacci(k+1)*binomial(2*n-k-1,n-1))/n.",
				"If we introduce an alternating sign, defining b(n) = (Sum_{k=1..n} k*Fibonacci(k+1)*binomial(-n,n-k))/n = (Sum_{k=1..n} k*Fibonacci(k+1)*(-1)^(n+k)*binomial(2*n-k-1,n-1))/n, then b(n) = 1 for all n. (Not obvious--I proved it satisfies b(n+2) = ((17*n^2 + 37*n + 18)*b(n+1) - 4*(2*n+1)*(2*n+3)*b(n))/((n+2)*(n+3)).) (End)",
				"G.f.: 1/(1-x-2x^2/(1-2x-x^2/(1-2x-x^2/(1-2x-x^2/(1-... (continued fraction). - _Paul Barry_, Aug 03 2009",
				"From _Gary W. Adamson_, Jul 11 2011: (Start)",
				"a(n) = the upper left term in M^n, M = an infinite square production matrix in which a column of (1,2,2,2,...) is prepended to an infinite lower triangular matrix of all 1's and the rest zeros:",
				"  1, 1, 0, 0, 0, 0, ...",
				"  2, 1, 1, 0, 0, 0, ...",
				"  2, 1, 1, 1, 0, 0, ...",
				"  2, 1, 1, 1, 1, 0, ...",
				"  2, 1, 1, 1, 1, 1, ...",
				"  ... (End)"
			],
			"mathematica": [
				"y[n_] := y[n] = (2*(4*n - 3)*y[n - 1] - (15*n - 24)*y[n - 2] - (4*n - 6)*y[n - 3])/n; y[0] = 1; y[1] = 1; y[2] = 3; (* corrected by _Wouter Meeussen_, Apr 30 2011 *)",
				"CoefficientList[Series[1/(x+Sqrt[1-4x] ),{x,0,30}],x] (* _Harvey P. Dale_, May 05 2021 *)"
			],
			"program": [
				"(Maxima) makelist(sum(binomial(2*n-k,n+k)*(3*k+1)/(n+k+1),k,0,n),n,0,12); /* _Emanuele Munarini_, Apr 02 2011 */",
				"(PARI) x='x+O('x^66); Vec(1/(x+sqrt(1-4*x))) \\\\ _Joerg Arndt_, Jul 06 2013"
			],
			"xref": [
				"Cf. A000045, A000984, A081698, A152229, A189675.",
				"Cf. A068875."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Emanuele Munarini_, Apr 02 2003",
			"ext": [
				"More terms from _Paul Barry_, Dec 18 2004",
				"Wouter credited with first sums in Gosper's FORMULA Comment, which were mistyped by NJAS (caught by Julian Ziegler Hunts), May 14 2011"
			],
			"references": 18,
			"revision": 102,
			"time": "2021-05-05T12:11:56-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}