{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A260667",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 260667,
			"data": "1,37,1737,102501,6979833,523680739,42129659113,3572184623653,315561396741609,28807571694394593,2701627814373536601,259121323945378645947,25330657454041707496017,2516984276442279642274311,253667099464270541534450025,25884030861250181046253181349,2670255662315910532447096232073",
			"name": "a(n) = (Sum_{k=0..n-1}(2k+1)*S(k,n)^2)/n^2, where S(k,x) denotes the polynomial Sum_{j=0..k}binomial(k,j)*binomial(x,j)*binomial(x+j,j).",
			"comment": [
				"Conjecture: For k = 0,1,2,... define S(k,x):= Sum_{j=0..k}binomial(k,j)*binomial(x,j)*binomial(x+j,j).",
				"(i) For any integer n \u003e 0, the polynomial (Sum_{k=0..n-1}(2k+1)*S(k,x)^2)/n^2 is integer-valued (and hence a(n) is always integral).",
				"(ii) Let r be 0 or 1, and let x be any integer. Then, for any positive integers m and n, we have the congruence",
				"   sum_{k=0..n-1}(-1)^(k*r)*(2k+1)*S(k,x)^(2m) == 0 (mod n).",
				"(iii) For any odd prime p, we have Sum_{k=0..p-1}S(k,-1/2)^2 == (-1/p)(1-7*p^3*B_{p-3}) (mod p^4), where (a/p) is the Legendre symbol, and B_0,B_1,B_2,... are Bernoulli numbers. Also, for any prime p \u003e 3 we have sum_{k=0..p-1}S(k,-1/3)^2 == p-14/3*(p/3)*p^3*B_{p-2}(1/3) (mod p^4), where B_n(x) denotes the Bernoulli polynomial of degree n; sum_{k=0..p-1}S(k,-1/4)^2 == (2/p)*p-26*(-2/p)*p^3*E_{p-3} (mod p^4), where E_0,E_1,E_2,... are Euler numbers; sum_{k=0..p-1}S(k,-1/6)^2 == (3/p)*p-155/12*(-1/p)*p^3*B_{p-2}(1/3) (mod p^4).",
				"Our conjecture is motivated by a conjecture of Kimoto and Wakayama which states that Sum_{k=0..p-1}S(k,-1/2)^2 == (-1/p) (mod p^3) for any odd prime p. The Kimoto-Wakayama conjecture was confirmed by Long, Osburn and Swisher in 2014.",
				"For more related conjectures, see Sun's paper arXiv.1512.00712. - _Zhi-Wei Sun_, Dec 03 2015"
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A260667/b260667.txt\"\u003eTable of n, a(n) for n = 1..100\u003c/a\u003e",
				"K. Kimoto and M. Wakayama, \u003ca href=\"http://doi.org/10.2206/kyushujm.60.383\"\u003eApéry-like numbers arising from special values of spectral zeta function for non-commutative harmonic oscillators\u003c/a\u003e, Kyushu J. Math. 60(2006), no.2, 383-404. (Cf. the formula (6.19).)",
				"L. Long, R. Osburn and H. Swisher, \u003ca href=\"http://arxiv.org/abs/1404.4723\"\u003eOn a conjecture of Kimoto and Wakayama\u003c/a\u003e, arXiv:1404.4723 [math.NT], 2014.",
				"Z.-W. Sun, \u003ca href=\"https://arxiv.org/abs/1011.6676\"\u003eSupercongruences involving products of two binomial coefficients\u003c/a\u003e, arXiv:1011.6676 [math.NT], 2010-2013; Finite Fields Appl. 22(2013), 24-44.",
				"Z.-W. Sun, \u003ca href=\"http://dx.doi.org/10.1007/s11139-015-9727-3\"\u003eCongruences involving g_n(x)=sum_{k=0..n}binom(n,k)^2*binom(2k,k)*x^k\u003c/a\u003e, Ramanujan J., in press. Doi: 10.1007/s11139-015-9727-3.",
				"Z.-W. Sun, \u003ca href=\"http://arxiv.org/abs/1512.00712\"\u003eSupercongruences involving dual sequences\u003c/a\u003e, arXiv:1502.00712 [math.NT], 2015."
			],
			"formula": [
				"a(n) ~ phi^(10*n + 3) / (10 * Pi^2 * n^3), where phi = A001622 is the golden ratio. - _Vaclav Kotesovec_, Nov 06 2021"
			],
			"example": [
				"a(2) = 37 since (Sum_{k=0,1}(2k+1)*S(k,2)^2)/2^2 = (S(0,2)^2 + 3*S(1,2)^2)/4 = (1^2 + 3*7^2)/4 = 148/4 = 37.",
				"G.f. = x + 37*x^2 + 1737*x^3 + 102501*x^4 + 6979833*x^5 + 523680739*x^6 + ..."
			],
			"mathematica": [
				"S[k_,x_]:=S[k,x]=Sum[Binomial[k,j]Binomial[x,j]Binomial[x+j,j],{j,0,k}]",
				"a[n_]:=a[n]=Sum[(2k+1)*S[k,n]^2,{k,0,n-1}]/n^2",
				"Do[Print[n,\" \",a[n]],{n,1,17}]"
			],
			"xref": [
				"Cf. A000290, A027641, A027642, A122045.",
				"The Apéry-like numbers [or Apéry-like sequences, Apery-like numbers, Apery-like sequences] include A000172, A000984, A002893, A002895, A005258, A005259, A005260, A006077, A036917, A063007, A081085, A093388, A125143 (apart from signs), A143003, A143007, A143413, A143414, A143415, A143583, A183204, A214262, A219692, A226535, A227216, A227454, A229111 (apart from signs), A260667, A260832, A262177, A264541, A264542, A279619, A290575, A290576. (The term \"Apery-like\" is not well-defined.)"
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Zhi-Wei Sun_, Nov 14 2015",
			"references": 34,
			"revision": 39,
			"time": "2021-11-06T09:31:18-04:00",
			"created": "2015-11-14T14:56:16-05:00"
		}
	]
}