{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A316784",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 316784,
			"data": "1,1,1,1,1,2,1,2,1,2,1,4,1,2,2,3,1,4,1,4,2,2,1,10,1,2,2,4,1,8,1,6,2,2,2,13,1,2,2,10,1,8,1,4,4,2,1,26,1,4,2,4,1,10,2,10,2,2,1,28,1,2,4,13,2,8,1,4,2,8,1,46,1,2,4,4,2,8,1,26,3,2,1",
			"name": "Number of orderless identity tree-factorizations of n.",
			"comment": [
				"A factorization of n is a finite nonempty multiset of positive integers greater than 1 with product n. An orderless identity tree-factorization of n is either (case 1) the number n itself or (case 2) a finite set of two or more distinct orderless identity tree-factorizations, one of each factor in a factorization of n.",
				"a(n) depends only on the prime signature of n. - _Andrew Howroyd_, Nov 18 2018"
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A316784/b316784.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(p^n) = A300660(n) for prime p. - _Andrew Howroyd_, Nov 18 2018"
			],
			"example": [
				"The a(24)=10 orderless identity tree-factorizations:",
				"  24",
				"  (4*6)",
				"  (3*8)",
				"  (2*12)",
				"  (2*3*4)",
				"  (4*(2*3))",
				"  (3*(2*4))",
				"  (2*(2*6))",
				"  (2*(3*4))",
				"  (2*(2*(2*3)))"
			],
			"mathematica": [
				"postfacs[n_]:=If[n\u003c=1,{{}},Join@@Table[Map[Prepend[#,d]\u0026,Select[postfacs[n/d],Min@@#\u003e=d\u0026]],{d,Rest[Divisors[n]]}]];",
				"oltsfacs[n_]:=If[n\u003c=1,{{}},Prepend[Select[Union@@Function[q,Sort/@Tuples[oltsfacs/@q]]/@DeleteCases[postfacs[n],{n}],UnsameQ@@#\u0026],n]];",
				"Table[Length[oltsfacs[n]],{n,100}]"
			],
			"program": [
				"(PARI) seq(n)={my(v=vector(n), w=vector(n)); w[1]=v[1]=1; for(k=2, n, w[k]=v[k]+1; forstep(j=n\\k*k, k, -k, my(i=j, e=0); while(i%k==0, i/=k; e++; v[j] += binomial(w[k], e)*v[i]))); w} \\\\ _Andrew Howroyd_, Nov 18 2018"
			],
			"xref": [
				"Cf. A001055, A001678, A004111, A292504, A292505, A295279, A300660, A316782."
			],
			"keyword": "nonn",
			"offset": "1,6",
			"author": "_Gus Wiseman_, Jul 13 2018",
			"references": 2,
			"revision": 7,
			"time": "2018-11-18T19:26:12-05:00",
			"created": "2018-07-14T15:37:50-04:00"
		}
	]
}