{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A059377",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 59377,
			"data": "1,15,80,240,624,1200,2400,3840,6480,9360,14640,19200,28560,36000,49920,61440,83520,97200,130320,149760,192000,219600,279840,307200,390000,428400,524880,576000,707280,748800,923520,983040,1171200",
			"name": "Jordan function J_4(n).",
			"comment": [
				"This sequence is multiplicative. - _Mitch Harris_, Apr 19 2005",
				"For n = 4 or n \u003e= 6, a(n) is divisible by 240. - _Jianing Song_, Apr 06 2019"
			],
			"reference": [
				"L. Comtet, Advanced Combinatorics, Reidel, 1974, p. 199, #3.",
				"R. Sivaramakrishnan, \"The many facets of Euler's totient. II. Generalizations and analogues\", Nieuw Arch. Wisk. (4) 8 (1990), no. 2, 169-187."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A059377/b059377.txt\"\u003eTable of n, a(n) for n=1..1000\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Jordan%27s_totient_function\"\u003eJordan's totient function\u003c/a\u003e."
			],
			"formula": [
				"a(n) = Sum_{d|n} d^4*mu(n/d). - _Benoit Cloitre_, Apr 05 2002",
				"Multiplicative with a(p^e) = p^(4e)-p^(4(e-1)).",
				"Dirichlet generating function: zeta(s-4)/zeta(s). - _Franklin T. Adams-Watters_, Sep 11 2005",
				"a(n) = Sum_{k=1..n} gcd(k,n)^4 * cos(2*Pi*k/n). - _Enrique Pérez Herrero_, Jan 18 2013",
				"a(n) = n^4*Product_{distinct primes p dividing n} (1 - 1/p^4). - _Tom Edgar_, Jan 09 2015",
				"G.f.: Sum_{n\u003e=1} a(n)*x^n/(1 - x^n) = x*(1 + 11*x + 11*x^2 + x^3)/(1 - x)^5. - _Ilya Gutkovskiy_, Apr 25 2017",
				"Sum_{k=1..n} a(k) ~ n^5 / (5*zeta(5)). - _Vaclav Kotesovec_, Feb 07 2019",
				"From _Amiram Eldar_, Oct 12 2020: (Start)",
				"lim_{n-\u003eoo} (1/n) * Sum_{k=1..n} a(k)/k^4 = 1/zeta(5).",
				"Sum_{n\u003e=1} 1/a(n) = Product_{p prime} (1 + p^4/(p^4-1)^2) = 1.0870036174... (End)"
			],
			"maple": [
				"J := proc(n,k) local i,p,t1,t2; t1 := n^k; for p from 1 to n do if isprime(p) and n mod p = 0 then t1 := t1*(1-p^(-k)); fi; od; t1; end; # (with k = 4)"
			],
			"mathematica": [
				"JordanJ[n_, k_: 1] := DivisorSum[n, #^k*MoebiusMu[n/#] \u0026]; f[n_] := JordanJ[n, 4]; Array[f, 38]",
				"f[p_, e_] := p^(4*e) - p^(4*(e-1)); a[1] = 1; a[n_] := Times @@ f @@@ FactorInteger[n]; Array[a, 100] (* _Amiram Eldar_, Oct 12 2020 *)"
			],
			"program": [
				"(PARI) for(n=1,100,print1(sumdiv(n,d,d^4*moebius(n/d)),\",\"))",
				"(PARI) a(n)=if(n\u003c1,0,sumdiv(n,d,d^4*moebius(n/d)))",
				"(PARI) a(n)=if(n\u003c1,0,dirdiv(vector(n,k,k^4),vector(n,k,1))[n])",
				"(PARI) { for (n = 1, 1000, write(\"b059377.txt\", n, \" \", sumdiv(n, d, d^4*moebius(n/d))); ) } \\\\ _Harry J. Smith_, Jun 26 2009"
			],
			"xref": [
				"See A059379 and A059380 (triangle of values of J_k(n)), A000010 (J_1), A007434 (J_2), A059376 (J_3), A059378 (J_5).",
				"Cf. A013663."
			],
			"keyword": "nonn,mult,easy",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Jan 28 2001",
			"references": 26,
			"revision": 44,
			"time": "2020-10-12T02:24:13-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}