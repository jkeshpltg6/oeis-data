{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097057",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97057,
			"data": "1,4,8,16,24,24,32,32,24,52,48,48,96,56,64,96,24,72,104,80,144,128,96,96,96,124,112,160,192,120,192,128,24,192,144,192,312,152,160,224,144,168,256,176,288,312,192,192,96,228,248,288,336,216,320,288,192,320,240,240",
			"name": "Number of integer solutions to a^2 + b^2 + 2*c^2 + 2*d^2 = n.",
			"comment": [
				"a^2 + b^2 + 2*c^2 + 2*d^2 is another (cf. A000118) of Ramanujan's 54 universal quaternary quadratic forms. - _Michael Somos_, Apr 01 2008"
			],
			"reference": [
				"B. C. Berndt, Ramanujan's Notebooks Part V, Springer-Verlag, see p. 373 Entry 31.",
				"Deutsch, Jesse Ira; Bumby's technique and a result of Liouville on a quadratic form. Integers 8 (2008), no. 2, A2, 20 pp. MR2438287 (2009g:11047).",
				"N. J. Fine, Basic Hypergeometric Series and Applications, Amer. Math. Soc., 1988; p. 78, Eq. (32.29).",
				"S. Ramanujan, Collected Papers, Chap. 20, Cambridge Univ. Press 1927 (Proceedings of the Camb. Phil. Soc., 19 (1917) 11-21)"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A097057/b097057.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Jesse Ira Deutsch, \u003ca href=\"https://doi.org/10.1016/j.jnt.2004.08.014\"\u003eA quaternionic proof of the representation formula of a quaternary quadratic form\u003c/a\u003e, J. Number Theory 113 (2005), no. 1, 149-174. MR2141762 (2006b:11033).",
				"Y. Mimura, \u003ca href=\"http://www.kobepharma-u.ac.jp/~math/notes/note01.html\"\u003eAlmost Universal Quadratic Forms\u003c/a\u003e.",
				"Olivia X. M. Yao, Ernest X. W. Xia, \u003ca href=\"https://doi.org/10.1016/j.disc.2013.11.011\"\u003eCombinatorial proofs of five formulas of Liouville\u003c/a\u003e, Discrete Math. 318 (2014), 1--9. MR3141622."
			],
			"formula": [
				"Euler transform of period 8 sequence [4, -2, 4, -8, 4, -2, 4, -4, ...]. - _Michael Somos_, Sep 17 2004",
				"Multiplicative with a(n) = 4*b(n), b(2) = 2, b(2^e) = 6 if e \u003e 1, b(p^e) = (p^(e+1) - 1) / (p - 1) if p \u003e 2. - _Michael Somos_, Sep 17 2004",
				"Expansion of (eta(q^2) * eta(q^4))^6 / (eta(q) * eta(q^8))^4 in powers of q.",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (8 t)) = 8 (t/i)^2 f(t) where q = exp(2 Pi i t). - _Michael Somos_, Jul 05 2011",
				"G.f.: (theta_3(q) * theta_3(q^2))^2.",
				"G.f.: Product_{k\u003e0} ((1-x^(2k))(1-x^(4k)))^6/((1-x^k)(1-x^(8k)))^4.",
				"G.f.: 1 + Sum_{k\u003e0} 8 * x^(4*k) / (1 + x^(4*k))^2 + 4 * x^(2*k-1) / (1 - x^(2*k-1))^2 = 1 + Sum_{k\u003e0} (2 + (-1)^k) * 4*k * x^(2*k) / (1 + x^(2*k)) + 4*(2*k - 1) * x^(2*k-1) / (1 - x^(2*k - 1)). - _Michael Somos_, Oct 22 2005",
				"a(2*n) = A000118(n). a(2*n + 1) = 4 * A008438(n). a(4*n) = A004011(n). a(4*n + 1) = 4 * A112610(n). a(4*n + 2) = 8 * A008438(n). a(4*n + 3) = 16 * A097723(n). - _Michael Somos_, Jul 05 2011"
			],
			"example": [
				"1 + 4*q + 8*q^2 + 16*q^3 + 24*q^4 + 24*q^5 + 32*q^6 + 32*q^7 + 24*q^8 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (EllipticTheta[ 3, 0, q] EllipticTheta[ 3, 0, q^2])^2, {q, 0, n}] (* _Michael Somos_, Jul 05 2011 *)"
			],
			"program": [
				"(PARI) {a(n) = local(t); if( n\u003c1, n\u003e=0, t = 2^valuation( n, 2); 4 * sigma(n/t) * if( t\u003e2, 6, t))} \\\\ _Michael Somos_, Sep 17 2004",
				"(PARI) {a(n) = local(A = x * O(x^n)); polcoeff( (eta(x^2 + A) * eta(x^4 + A))^6 / (eta(x + A) * eta(x^8 + A))^4, n))} \\\\ _Michael Somos_, Sep 17 2004",
				"(PARI) {a(n) = if( n\u003c1, n==0, 2 * qfrep([ 1, 0, 0, 0; 0, 1, 0, 0; 0, 0, 2, 0; 0, 0, 0, 2], n)[n])} \\\\ _Michael Somos_, Oct 29 2005",
				"(PARI) A097057(n)=if(n,sigma(n\u003e\u003en=valuation(n,2))*if(n\u003e1,24,4\u003c\u003cn),1) \\\\ _M. F. Hasler_, May 07 2018"
			],
			"xref": [
				"a^2 + b^2 + 2*c^2 + m*d^2 = n: this sequence (m=2), A320124 (m=3), A320125 (m=4), A320126 (m=5), A320127 (m=6), A320128 (m=7), A320130 (m=8), A320131 (m=9), A320132 (m=10), A320133 (m=11), A320134 (m=12), A320135 (m=13), A320136 (m=14).",
				"Cf. A000118, A004011, A008438, A097723, A112610."
			],
			"keyword": "nonn,mult",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Sep 15 2004",
			"ext": [
				"Added keyword mult and minor edits by _M. F. Hasler_, May 07 2018"
			],
			"references": 20,
			"revision": 37,
			"time": "2018-10-08T03:49:48-04:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}