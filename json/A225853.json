{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A225853",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 225853,
			"data": "1,2,0,0,3,2,0,0,4,6,0,0,7,8,0,0,13,14,0,0,19,20,0,0,29,34,0,0,43,46,0,0,62,70,0,0,90,96,0,0,126,138,0,0,174,186,0,0,239,262,0,0,325,346,0,0,435,472,0,0,580,620,0,0,769,826,0,0,1007,1072,0",
			"name": "Expansion of phi(x) / f(-x^4) in powers of x where phi(), f() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A225853/b225853.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of chi(x)^2 * chi(-x^2) = chi(x)^3 * chi(-x) = chi(-x^2)^3 / chi(-x)^2 in powers of x where chi() is a Ramanujan theta function.",
				"Expansion of q^(1/4) * eta(q^2)^5 / (eta(q)^2 * eta(q^4)^3) in powers of q.",
				"Euler transform of period 4 sequence [ 2, -3, 2, 0, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (576 t)) = 2^(1/2) g(t) where q = exp(2 Pi i t) and g() is the g.f. of A029552.",
				"G.f.: Product_{k\u003e0} (1 - x^(4*k-2))^3 / (1 - x^(2*k-1))^2 = (Sum_{k in Z} x^k^2) / (Product_{k\u003e0} (1 - x^(4*k))).",
				"a(n) = (-1)^n * A143161(n). a(4*n + 2) = a(4*n + 3) = 0."
			],
			"example": [
				"1 + 2*x + 3*x^4 + 2*x^5 + 4*x^8 + 6*x^9 + 7*x^12 + 8*x^13 + 13*x^16 + ...",
				"1/q + 2*q^5 + 3*q^23 + 2*q^29 + 4*q^47 + 6*q^53 + 7*q^71 + 8*q^77 + 13*q^95 + ..."
			],
			"mathematica": [
				"a[n_]:= SeriesCoefficient[EllipticTheta[3,0,q]/QPochhammer[q^4],{q,0,n}];",
				"a[n_]:= SeriesCoefficient[QPochhammer[q^2,q^4]^3/QPochhammer[q,q^2]^2, {q,0,n}];"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^5 / (eta(x + A)^2 * eta(x^4 + A)^3), n))}"
			],
			"xref": [
				"Cf. A029552, A143161."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Michael Somos_, May 17 2013",
			"references": 1,
			"revision": 14,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2013-05-18T01:50:24-04:00"
		}
	]
}