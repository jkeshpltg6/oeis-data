{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A271649",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 271649,
			"data": "4,8,16,28,44,64,88,116,148,184,224,268,316,368,424,484,548,616,688,764,844,928,1016,1108,1204,1304,1408,1516,1628,1744,1864,1988,2116,2248,2384,2524,2668,2816,2968,3124,3284,3448,3616,3788,3964,4144,4328,4516,4708,4904,5104,5308,5516",
			"name": "a(n) = 2*(n^2 - n + 2).",
			"comment": [
				"Numbers n such that 2n - 7 a perfect square.",
				"Galois numbers for three-dimensional vector space, defined as the total number of subspaces in a three-dimensional vector space over GF(n-1), when n-1 is a power of a prime. - _Artur Jasinski_, Aug 31 2016, corrected by _Robert Israel_, Sep 23 2016"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A271649/b271649.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1)."
			],
			"formula": [
				"a(n) = 4*A000124(n).",
				"a(n) = 2*A014206(n).",
				"a(n) = A137882(n), n \u003e 1. - _R. J. Mathar_, Apr 12 2016"
			],
			"example": [
				"a(1) = 2*(1^2 - 1 + 2) = 4."
			],
			"maple": [
				"A271649:=n-\u003e2*(n^2-n+2): seq(A271649(n), n=1..60); # _Wesley Ivan Hurt_, Aug 31 2016"
			],
			"mathematica": [
				"Table[2 (n^2 - n + 2), {n, 53}] (* or *)",
				"Select[Range@ 5516, IntegerQ@ Sqrt[2 # - 7] \u0026] (* or *)",
				"Table[SeriesCoefficient[(-4 (1 - x + x^2))/(-1 + x)^3, {x, 0, n}], {n, 0, 52}] (* _Michael De Vlieger_, Apr 11 2016 *)",
				"GaloisNumber[n_, q_] := Sum[QBinomial[n, m, q], {m, 0, n}]; Table[GaloisNumber[3, n], {n, 0, 50}] (* _Artur Jasinski_, Aug 31 2016 *)"
			],
			"program": [
				"(MAGMA) [ 2*n^2 - 2*n + 4: n in [1..60]];",
				"(MAGMA) [ n: n in [1..6000] | IsSquare(2*n-7)];",
				"(PARI) a(n)=2*(n^2-n+2) \\\\ _Charles R Greathouse IV_, Jun 17 2017"
			],
			"xref": [
				"Cf. A000124, A014206.",
				"Numbers h such that 2*h + k is a perfect square: no sequence (k=-9), A255843 (k=-8), this sequence (k=-7), A093328 (k=-6), A097080 (k=-5), A271624 (k=-4), A051890 (k=-3), A058331 (k=-2), A001844 (k=-1), A001105 (k=0), A046092 (k=1), A056222 (k=2), A142463 (k=3), A054000 (k=4), A090288 (k=5), A268581 (k=6), A059993 (k=7), (-1)*A147973 (k=8), A139570 (k=9), A271625 (k=10), A222182 (k=11), A152811 (k=12), A181510 (k=13), A161532 (k=14), no sequence (k=15)."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_Juri-Stepan Gerasimov_, Apr 11 2016",
			"references": 4,
			"revision": 45,
			"time": "2017-11-09T03:56:25-05:00",
			"created": "2016-04-11T23:38:49-04:00"
		}
	]
}