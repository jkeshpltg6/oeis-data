{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A072478",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 72478,
			"data": "0,2,2,4,2,8,1,16,1,32,1,64,1,128,1,256,1,512,1,1024,1,2048,1,4096,1,8192,1,16384,1,32768,1,65536,1,131072,1,262144,1,524288,1,1048576,1,2097152,1,4194304,1,8388608,1,16777216,1,33554432,1,67108864,1",
			"name": "Surface area of n-dimensional sphere of radius r is n*V_n*r^(n-1) = n*Pi^(n/2)*r^(n-1)/(n/2)! = S_n*Pi^floor(n/2)*r^(n-1); sequence gives numerator of S_n.",
			"comment": [
				"Answer to question of how to extend the sequence 0, 2, 2 Pi r, 4 Pi r^2, 2 Pi^2 r^3, ...",
				"Volume of n-dimensional sphere of radius r is V_n*r^n - see A072345/A072346.",
				"a(2*n-1) = 2^n and for n\u003e2 a(2*n)=1.",
				"Denominator of the rational coefficient of integral_{x\u003e0} exp(-x^2)*x^n. - _Jean-François Alcover_, Apr 23 2013",
				"From _Ilya Gutkovskiy_, Aug 02 2016: (Start)",
				"Numerator of n/Gamma(n/2+1).",
				"More generally, the ordinary generating function for the surface area of the n-dimensional sphere of radius r is 2*x*(1 + exp(Pi*r^2*x^2)*Pi*r*x + exp(Pi*r^2*x^2)*Pi*r*erf(sqrt(Pi)*r*x)*x) =  2*x + 2*Pi*r*x^2 + 4*Pi*r^2*x^3 + 2*Pi^2*r^3*x^4 + (8*Pi^2*r^4/3)*x^5 + Pi^3*r^5*x^6 + ... (End)"
			],
			"reference": [
				"N. Cakic, D. Letic, B. Davidovic, The Hyperspherical functions of a derivative, Abstr. Appl. Anal. (2010) 364292 doi:10.1155/2010/364292",
				"J. H. Conway and N. J. A. Sloane, \"Sphere Packings, Lattices and Groups\", Springer-Verlag, p. 10, Eq. 19."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A072478/b072478.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Dusko Letic, Nenad Cakic, Branko Davidovic and Ivana Berkovic, \u003ca href=\"http://dx.doi.org/10.1186/1687-1847-2012-22\"\u003eOrthogonal and diagonal dimension fluxes of hyperspherical function\u003c/a\u003e, Advances in Difference Equations 2012, 2012:22. - _N. J. A. Sloane_, Sep 04 2012",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Ball.html\"\u003eBall\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Hypersphere.html\"\u003eHypersphere\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Four-DimensionalGeometry.html\"\u003eFour-Dimensional Geometry\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,3,0,-2)."
			],
			"formula": [
				"From _Colin Barker_, Sep 04 2012: (Start)",
				"a(n) = 3*a(n-2)-2*a(n-4) for n\u003e4.",
				"G.f.: x*(2+2*x-2*x^2-4*x^3-x^5+2*x^7) / (1-3*x^2+2*x^4).",
				"(End)",
				"From _Colin Barker_, Aug 01 2016: (Start)",
				"a(n) = (1+(-1)^n-2^((1+n)/2)*(-1+(-1)^n))/2 for n\u003e4.",
				"a(n) = 1 for n\u003e4 and even.",
				"a(n) = 2^((n+1)/2) for n\u003e4 and odd.",
				"(End)"
			],
			"example": [
				"Sequence of S_n's begins 0, 2, 2, 4, 2, 8/3, 1, 16/15, 1/3, 32/105, 1/12, 64/945, ..."
			],
			"mathematica": [
				"f[n_] := Pi^(n/2 - Floor[n/2])*n/(n/2)!; Table[ Numerator[ f[n]], {n, 0, 52}]",
				"CoefficientList[Series[x (2 + 2 x - 2 x^2 - 4 x^3 - x^5 + 2 x^7)/(1 - 3 x^2 + 2 x^4), {x, 0, 52}], x] (* _Michael De Vlieger_, Aug 01 2016 *)",
				"LinearRecurrence[{0,3,0,-2},{0,2,2,4,2,8,1,16,1},60] (* _Harvey P. Dale_, May 30 2018 *)"
			],
			"program": [
				"(PARI) concat(0, Vec(x*(2+2*x-2*x^2-4*x^3-x^5+2*x^7)/(1-3*x^2+2*x^4) + O(x^100))) \\\\ _Colin Barker_, Aug 01 2016"
			],
			"xref": [
				"Cf. A072479. A072478(n)/A072479(n) = n*A072345(n)/A072346(n)."
			],
			"keyword": "nonn,frac,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Aug 02 2002",
			"ext": [
				"More terms from _Robert G. Wilson v_, Aug 18 2002"
			],
			"references": 8,
			"revision": 35,
			"time": "2018-06-21T05:28:23-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}