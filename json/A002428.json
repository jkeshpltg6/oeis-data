{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002428",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2428,
			"id": "M2131 N0844",
			"data": "0,1,-2,23,-44,563,-3254,88069,-11384,1593269,-15518938,31730711,-186088972,3788707301,-5776016314,340028535787,-667903294192,10823198495797,-5476065119726,409741429887649,-103505656241356,17141894231615609",
			"name": "Numerators of coefficients of expansion of arctan(x)^2 = x^2 - 2/3*x^4 + 23/45*x^6 - 44/105*x^8 + 563/1575*x^10 - 3254/10395*x^12 + ...",
			"comment": [
				"|a(n)| = numerator of Sum_{k=1..n} 1/(n*(2*k-1)).",
				"Let f(x) = (1/2)*log((1+sqrt(x))/(1-sqrt(x))) and c(n) = Integral_{x=0..1} f(x)*x^(n-1) dx, then for n\u003e=1, c(n) = |a(n+1)|/A071968(n) and (f(x))^2 = Sum_{n\u003e=1} c(n)*x^n. - _Roland Groux_, Dec 14 2010"
			],
			"reference": [
				"A. Fletcher, J. C. P. Miller, L. Rosenhead and L. J. Comrie, An Index of Mathematical Tables. Vols. 1 and 2, 2nd ed., Blackwell, Oxford and Addison-Wesley, Reading, MA, 1962, Vol. 1, p. 89.",
				"H. A. Rothe, in C. F. Hindenburg, editor, Sammlung Combinatorisch-Analytischer Abhandlungen, Vol. 2, Chap. XI. Fleischer, Leipzig, 1800, p. 313.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A002428/b002428.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = numerator of (-1)^n * Sum_{k=1..n-1} 1/((n-1)*(2*k-1)), for n\u003e=1. - _G. C. Greubel_, Jul 03 2019"
			],
			"mathematica": [
				"a[n_]:= (-1)^n*Sum[1/((n-1)*(2*k-1)), {k,1,n-1}]//Numerator; Table[a[n], {n, 1, 30}] (* _Jean-François Alcover_, Nov 04 2013 *)",
				"a[n_]:= SeriesCoefficient[ArcTan[x]^2, {x, 0, 2*n-2}]//Numerator; Table[a[n], {n, 1, 30}] (* _G. C. Greubel_, Jul 03 2019 *)"
			],
			"program": [
				"(PARI) vector(30, n, numerator((-1)^n*sum(k=1,n-1,1/((n-1)*(2*k-1))))) /* corrected by _G. C. Greubel_, Jul 03 2019 */",
				"(MAGMA) [0] cat [Numerator((-1)^n*(\u0026+[1/((n-1)*(2*k-1)): k in [1..n-1]])): n in [2..30]]; // _G. C. Greubel_, Jul 03 2019",
				"(Sage) [numerator((-1)^n*sum(1/((n-1)*(2*k-1)) for k in (1..n-1))) for n in (1..30)] # _G. C. Greubel_, Jul 03 2019",
				"(GAP) List([1..30], n-\u003e NumeratorRat( (-1)^n*Sum([1..n-1], k-\u003e 1/((n-1)*(2*k-1))) )) # _G. C. Greubel_, Jul 03 2019"
			],
			"xref": [
				"Cf. A071968.",
				"Cf. A002549, A004041, A025550, A035048."
			],
			"keyword": "sign,easy,frac",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Jason Earls_, Apr 09 2002",
				"Additional comments from _Benoit Cloitre_, Apr 06 2002"
			],
			"references": 7,
			"revision": 37,
			"time": "2019-07-03T18:25:14-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}