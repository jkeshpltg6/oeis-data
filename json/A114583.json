{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A114583",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 114583,
			"data": "1,1,2,3,1,7,2,15,6,36,14,1,85,39,3,209,102,12,517,280,37,1,1303,758,123,4,3312,2085,381,20,8510,5730,1194,76,1,22029,15849,3657,295,5,57447,43914,11187,1056,30,150709,122090,33903,3734,135,1,397569,340104",
			"name": "Triangle read by rows: T(n,k) is the number of Motzkin paths of length n and having k UHD's, where U=(1,1),H=(1,0),D=(1,-1) (0\u003c=k\u003c=floor(n/3)).",
			"comment": [
				"Row n contains 1+floor(n/3) terms. Row sums are the Motzkin numbers (A001006). Column 1 yields A114584. Sum(k*T(n,k),k=0..floor(n/3))=A005717(n-2)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A114583/b114583.txt\"\u003eRows n = 0..300, flattened\u003c/a\u003e",
				"Marilena Barnabei, Flavio Bonetti, and Niccolò Castronuovo, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL21/Barnabei/barnabei5.html\"\u003eMotzkin and Catalan Tunnel Polynomials\u003c/a\u003e, J. Int. Seq., Vol. 21 (2018), Article 18.8.8.",
				"Zhuang, Yan. \u003ca href=\"https://arxiv.org/abs/1508.02793\"\u003eA generalized Goulden-Jackson cluster method and lattice path enumeration\u003c/a\u003e, Discrete Mathematics 341.2 (2018): 358-379. Also arXiv: 1508.02793v2."
			],
			"formula": [
				"G.f.=G=G(t, z) satisfies G=1+zG+z^2*G(tz-z+G)."
			],
			"example": [
				"T(5,1)=6 because we have HH(UHD), UD(UHD), (UHD)HH, (UHD)UD, H(UHD)H and U(UHD)D, where U=(1,1),H=(1,0),D=(1,-1) (the UHD's are shown between parentheses).",
				"Triangle begins:",
				"   1;",
				"   1;",
				"   2;",
				"   3,  1;",
				"   7,  2;",
				"  15,  6;",
				"  36, 14, 1;",
				"  ..."
			],
			"maple": [
				"G:=(1-z-t*z^3+z^3-sqrt((1-3*z+z^3-t*z^3)*(1+z+z^3-t*z^3)))/2/z^2: Gser:=simplify(series(G,z=0,20)): P[0]:=1: for n from 1 to 17 do P[n]:=sort(coeff(Gser,z^n)) od: for n from 0 to 17 do seq(coeff(t*P[n],t^j),j=1..1+floor(n/3)) od; # yields sequence in triangular form",
				"# second Maple program:",
				"b:= proc(x, y, t) option remember; expand(`if`(y\u003c0 or y\u003ex, 0,",
				"     `if`(x=0, 1, b(x-1, y, `if`(t=1, 2, 0))+b(x-1, y-1, 0)*",
				"     `if`(t=2, z, 1)+b(x-1, y+1, 1))))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, z, i), i=0..degree(p)))(b(n, 0$2)):",
				"seq(T(n), n=0..15);  # _Alois P. Heinz_, Feb 01 2019"
			],
			"mathematica": [
				"CoefficientList[#, t]\u0026 /@ CoefficientList[(1 - z - t z^3 + z^3 - Sqrt[(1 - 3z + z^3 - t z^3)(1 + z + z^3 - t z^3)])/2/z^2 + O[z]^17, z] // Flatten (* _Jean-François Alcover_, Aug 07 2018 *)"
			],
			"xref": [
				"Cf. A001006, A114584, A115717."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_Emeric Deutsch_, Dec 09 2005",
			"references": 2,
			"revision": 24,
			"time": "2019-02-01T09:42:32-05:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}