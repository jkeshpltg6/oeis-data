{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A025265",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 25265,
			"data": "1,0,1,2,4,9,22,56,146,388,1048,2869,7942,22192,62510,177308,506008,1451866,4185788,12119696,35227748,102753800,300672368,882373261,2596389190,7658677856,22642421206,67081765932,199128719896,592179010350,1764044315540",
			"name": "a(n) = a(1)*a(n-1) + a(2)*a(n-2) + ... + a(n-1)*a(1) for n \u003e= 4.",
			"comment": [
				"With offset 0, a(n) is the number of 021-avoiding ascent sequences of length n with no isolated 0's. For example, a(4)=4 counts 0000, 0001, 0011, 0012. - _David Callan_, Nov 13 2019"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A025265/b025265.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Paul Barry, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL19/Barry/barry321.html\"\u003eJacobsthal Decompositions of Pascal's Triangle, Ternary Trees, and Alternating Sign Matrices\u003c/a\u003e, Journal of Integer Sequences, 19, 2016, #16.3.5.",
				"David Callan, \u003ca href=\"https://arxiv.org/abs/1911.02209\"\u003eOn Ascent, Repetition and Descent Sequences\u003c/a\u003e, arXiv:1911.02209 [math.CO], 2019."
			],
			"formula": [
				"a(n+2) = A091561(n).",
				"G.f.: (1-sqrt(1-4*x+4*x^2-4*x^3))/2. - _Michael Somos_, Jun 08 2000",
				"G.f. A(x) satisfies 0=f(x, A(x)) where f(x, y)=(x-x^2+x^3)-(y-y^2). - _Michael Somos_, May 26 2005",
				"Conjecture: n*a(n) +2*(3-2*n)*a(n-1) +4*(n-3)*a(n-2)+ 2*(9-2*n)*a(n-3)=0. - _R. J. Mathar_, Aug 14 2012",
				"a(n) = Sum_{k=0..n} C(k)*Sum_{j=0..k+1} binomial(j,n-k-j-1)*binomial(k+1,j)*(-1)^(-n+k-1), where C(k) is Catalan numbers (A000108)  - _Vladimir Kruchinin_, May 10 2018"
			],
			"mathematica": [
				"nmax = 30; aa = ConstantArray[0,nmax]; aa[[1]] = 1; aa[[2]] = 0; aa[[3]] = 1; Do[aa[[n]] = Sum[aa[[k]] * aa[[n-k]],{k,1,n-1}],{n,4,nmax}]; aa (* _Vaclav Kotesovec_, Jan 25 2015 *)",
				"CoefficientList[Series[(1-Sqrt[1-4x+4x^2-4x^3])/2,{x,0,40}],x] (* _Harvey P. Dale_, Jun 02 2017 *)"
			],
			"program": [
				"(PARI) a(n)=polcoeff((1-sqrt(1-4*x+4*x^2-4*x^3+x*O(x^n)))/2,n)",
				"(PARI) a(n)=if(n\u003c1,0,polcoeff(subst(serreverse(x-x^2+x*O(x^n)),x,x-x^2+x^3),n))",
				"(Maxima)",
				"a(n):=sum((binomial(2*k,k)*(sum(binomial(j,n-k-j-1)*binomial(k+1,j),j,0,k+1))*(-1)^(-n+k+1))/(k+1),k,0,n) /* _Vladimir Kruchinin_, May 10 2018  */"
			],
			"xref": [
				"Cf. A025247, A091561."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Clark Kimberling_",
			"references": 3,
			"revision": 41,
			"time": "2019-11-14T04:47:48-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}