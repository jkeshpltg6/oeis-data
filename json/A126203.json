{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A126203",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 126203,
			"data": "0,2,3,27,98,120",
			"name": "Middle number of a set of 5 consecutive numbers such that the sum of their cubes is a square.",
			"comment": [
				"That is, numbers n \u003e= 0 such that 5n(n^2+6) is a square.",
				"According to Dickson, Lucas stated that the only terms are 2, 3, 98 and 120 (missing 27). Has this result been checked?",
				"Comment from _Allan Wilks_, Mar 10 2007: the Mordell reference shows that there are only finitely many solutions.",
				"Comments from Max Alekseyev, Mar 10 2007 (Start) It can be shown that all such numbers n can be obtained from elements that are perfect squares in the following 3 recurrent sequence:",
				"1) x(0)=0, x(1)=4, x(k+1) = 98*x(k) - x(k-1). If x(k) is a square then n = 30*x(k). In particular: for k=0, we have n=30*x(0)=0, for k=1, we have n=30*x(1)=120.",
				"2) x(0)=1, x(1)=49, x(k+1) = 38*x(k) - x(k-1). If x(k) is a square then n = 2*x(k). In particular: for k=1, we have n=2*x(1)=2, for k=2, we have n=2*x(1)=98.",
				"3) x(0)=1, x(1)=9, x(k+1) = 8*x(k) - x(k-1). If x(k) is a square then n = 3*x(k). In particular: for k=1, we have n=3*x(1)=3, for k=2, we have n=3*x(1)=27.",
				"It also follows that for any such n one of n/2, n/3, or n/30 is a perfect square. I have tested 10^5 terms of each of the recurrent sequences above and found no new perfect squares. (End)",
				"Comment from _Warut Roonguthai_, Apr 28 2007: The sequence is finite because the number of integral points on an elliptic curve is finite; in this case the curve is m^2 = 5n^3 + 30n. Multiplying the equation by 25 and letting y = 5m and x = 5n, we have y^2 = x^3 + 150x. According to MAGMA, the integral points on this curve are (x, y) = (0, 0), (10, 50), (15, 75), (24, 132), (135, 1575), (490, 10850), (600, 14700). So the list is complete.",
				"This was also confirmed by Jaap Spies, May 27 2007, using Sage."
			],
			"reference": [
				"L. E. Dickson, History of the Theory of Numbers, Volume 2, Chapter 21, page 587.",
				"E. Lucas, Recherches sur l'analyse indeterminee, Moulins, 1873, 92. Extract from Bull. Soc. d'Emulation du Departement de l'Allier, 12, 1873, 532.",
				"L. J. Mordell, Diophantine Equations, Ac. Press; see Th. 1, Chap. 27, p. 255."
			],
			"mathematica": [
				"Select[Partition[Range[-5,130],5,1],IntegerQ[Sqrt[Total[#^3]]]\u0026][[All,3]] (* _Harvey P. Dale_, Jan 31 2017 *)"
			],
			"program": [
				"(PARI) for(n=1, 10^8, if(issquare(5*n*(n*n+6)), print(n)))"
			],
			"keyword": "nonn,fini,full",
			"offset": "1,2",
			"author": "Nick Hobson, Mar 10 2007",
			"references": 12,
			"revision": 10,
			"time": "2017-01-31T12:38:06-05:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}