{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A288942",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 288942,
			"data": "1,1,0,1,1,0,1,1,1,0,1,1,2,1,0,1,1,2,4,1,0,1,1,2,5,9,1,0,1,1,2,5,13,21,1,0,1,1,2,5,14,36,51,1,0,1,1,2,5,14,41,104,127,1,0,1,1,2,5,14,42,125,309,323,1,0,1,1,2,5,14,42,131,393,939,835,1,0",
			"name": "Number A(n,k) of ordered rooted trees with n non-root nodes and all outdegrees \u003c= k; square array A(n,k), n \u003e= 0, k \u003e= 0, read by antidiagonals.",
			"comment": [
				"Also the number of Dyck paths of semilength n with all ascent lengths \u003c= k. A(4,2) = 9: /\\/\\/\\/\\, //\\\\/\\/\\, /\\//\\\\/\\, /\\/\\//\\\\, //\\/\\\\/\\, //\\/\\/\\\\, /\\//\\/\\\\, //\\\\//\\\\, //\\//\\\\\\.",
				"Also the number of permutations p of [n] such that in 0p all up-jumps are \u003c= k and no down-jump is larger than 1. An up-jump j occurs at position i in p if p_{i} \u003e p_{i-1} and j is the index of p_i in the increasingly sorted list of those elements in {p_{i}, ..., p_{n}} that are larger than p_{i-1}. A down-jump j occurs at position i in p if p_{i} \u003c p_{i-1} and j is the index of p_i in the decreasingly sorted list of those elements in {p_{i}, ..., p_{n}} that are smaller than p_{i-1}. First index in the lists is 1 here. A(4,2) = 9: 1234, 1243, 1324, 1342, 2134, 2143, 2314, 2341, 2431."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A288942/b288942.txt\"\u003eRows n = 0..140, flattened\u003c/a\u003e",
				"N. Hein and J. Huang, \u003ca href=\"http://arxiv.org/abs/1508.01688\"\u003eModular Catalan Numbers\u003c/a\u003e, arXiv:1508.01688 [math.CO], 2015",
				"\u003ca href=\"/index/Ro#rooted\"\u003eIndex entries for sequences related to rooted trees\u003c/a\u003e"
			],
			"formula": [
				"A(n,k) = Sum_{j=0..k} A203717(n,j).",
				"G.f. of column k: G(x)/x where G(x) is the reversion of x*(1-x)/(1-x^(k+1)). - _Andrew Howroyd_, Nov 30 2017",
				"G.f. g_k(x) of column k satisfies: g_k(x) = Sum_{j=0..k} (x*g_k(x))^j. - _Alois P. Heinz_, May 05 2019"
			],
			"example": [
				"A(4,2) = 9:",
				".",
				".   o    o      o      o      o      o       o      o       o",
				".   |    |      |      |     / \\    / \\     / \\    / \\     / \\",
				".   o    o      o      o    o   o  o   o   o   o  o   o   o   o",
				".   |    |     / \\    / \\   |          |  ( )        ( )  |   |",
				".   o    o    o   o  o   o  o          o  o o        o o  o   o",
				".   |   / \\   |          |  |          |",
				".   o  o   o  o          o  o          o",
				".   |",
				".   o",
				".",
				"Square array A(n,k) begins:",
				"  1, 1,   1,   1,    1,    1,    1,    1,    1, ...",
				"  0, 1,   1,   1,    1,    1,    1,    1,    1, ...",
				"  0, 1,   2,   2,    2,    2,    2,    2,    2, ...",
				"  0, 1,   4,   5,    5,    5,    5,    5,    5, ...",
				"  0, 1,   9,  13,   14,   14,   14,   14,   14, ...",
				"  0, 1,  21,  36,   41,   42,   42,   42,   42, ...",
				"  0, 1,  51, 104,  125,  131,  132,  132,  132, ...",
				"  0, 1, 127, 309,  393,  421,  428,  429,  429, ...",
				"  0, 1, 323, 939, 1265, 1385, 1421, 1429, 1430, ..."
			],
			"maple": [
				"b:= proc(u, o, k) option remember; `if`(u+o=0, 1,",
				"      add(b(u-j, o+j-1, k), j=1..min(1, u))+",
				"      add(b(u+j-1, o-j, k), j=1..min(k, o)))",
				"    end:",
				"A:= (n, k)-\u003e b(0, n, k):",
				"seq(seq(A(n, d-n), n=0..d), d=0..12);"
			],
			"mathematica": [
				"b[u_, o_, k_] := b[u, o, k] = If[u + o == 0, 1, Sum[b[u - j, o + j - 1, k], {j, 1, Min[1, u]}] + Sum[b[u + j - 1, o - j, k], {j, 1, Min[k, o]}]];",
				"A[n_, k_] := b[0, n, k];",
				"Table[Table[A[n, d-n], {n, 0, d}], {d, 0, 12}] // Flatten (* _Jean-François Alcover_, Oct 27 2017, translated from Maple *)"
			],
			"program": [
				"(PARI)",
				"T(n,k)=polcoeff(serreverse(x*(1-x)/(1-x*x^k) + O(x^2*x^n)), n+1);",
				"for(n=0, 10, for(k=0, 10, print1(T(n, k), \", \")); print); \\\\ _Andrew Howroyd_, Nov 29 2017"
			],
			"xref": [
				"Columns k=0..10 give: A000007, A000012, A001006, A036765, A036766, A036767, A036768, A036769, A291823, A291824, A291825.",
				"Main diagonal (and upper diagonals) give A000108.",
				"First lower diagonal gives A001453.",
				"Cf. A203717."
			],
			"keyword": "nonn,tabl",
			"offset": "0,13",
			"author": "_Alois P. Heinz_, Sep 01 2017",
			"references": 13,
			"revision": 53,
			"time": "2019-05-05T17:47:47-04:00",
			"created": "2017-09-01T19:12:26-04:00"
		}
	]
}