{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000420",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 420,
			"id": "M4431 N1874",
			"data": "1,7,49,343,2401,16807,117649,823543,5764801,40353607,282475249,1977326743,13841287201,96889010407,678223072849,4747561509943,33232930569601,232630513987207,1628413597910449,11398895185373143,79792266297612001,558545864083284007",
			"name": "Powers of 7: a(n) = 7^n.",
			"comment": [
				"Same as Pisot sequences E(1,7), L(1,7), P(1,7), T(1,7). See A008776 for definitions of Pisot sequences.",
				"Sum of coefficients of expansion of (1+x+x^2+x^3+x^4+x^5+x^6)^n.",
				"a(n) is number of compositions of natural numbers into n parts \u003c 7.",
				"The compositions of n in which each natural number is colored by one of p different colors are called p-colored compositions of n. For n\u003e=1, a(n) equals the number of 7-colored compositions of n such that no adjacent parts have the same color. - _Milan Janjic_, Nov 17 2011",
				"Numbers n such that sigma(7n) = 7n + sigma(n). - _Jahangeer Kholdi_, Nov 23 2013"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A000420/b000420.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e",
				"P. J. Cameron, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL3/groups.html\"\u003eSequences realized by oligomorphic permutation groups\u003c/a\u003e, J. Integ. Seqs. Vol. 3 (2000), #00.1.5.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=272\"\u003eEncyclopedia of Combinatorial Structures 272\u003c/a\u003e",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"Y. Puri and T. Ward, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL4/WARD/short.html\"\u003eArithmetic and growth of periodic orbits\u003c/a\u003e, J. Integer Seqs., Vol. 4 (2001), #01.2.1.",
				"\u003ca href=\"/index/Rec#order_01\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (7)."
			],
			"formula": [
				"a(n) = 7^n.",
				"a(0) = 1; a(n) = 7*a(n-1).",
				"G.f.: 1/(1-7*x).",
				"E.g.f.: exp(7*x).",
				"4/7 - 5/7^2 + 4/7^3 - 5/7^4 + ... = 23/48. [Jolley, Summation of Series, Dover, 1961]"
			],
			"example": [
				"a(2)=49 there are 49 compositions of natural numbers into 2 parts \u003c 7."
			],
			"maple": [
				"A000420:=-1/(-1+7*z); # _Simon Plouffe_ in his 1992 dissertation. [This is actually the generating function, so convert(series(...),list) would yield the actual sequence. - _M. F. Hasler_, Apr 19 2015]",
				"A000420 := n -\u003e 7^n; # _M. F. Hasler_, Apr 19 2015"
			],
			"mathematica": [
				"Table[7^n, {n,0,50}] (* _Vladimir Joseph Stephan Orlovsky_, Feb 15 2011 *)"
			],
			"program": [
				"(Maxima) makelist(7^n,n,0,20); /* _Martin Ettl_, Dec 27 2012 */",
				"(Haskell)",
				"a000420 = (7 ^)",
				"a000420_list = iterate (* 7) 1  -- _Reinhard Zumkeller_, Apr 29 2015",
				"(PARI) a(n)=7^n \\\\ _Charles R Greathouse IV_, Jul 28 2015",
				"(MAGMA) [7^n : n in [0..30]]; // _Wesley Ivan Hurt_, Sep 27 2016"
			],
			"xref": [
				"Cf. A000079 (powers of 2), A000244 (powers of 3), A000302 (powers of 4), A000351 (powers of 5), A000400 (powers of 6), A001018 (powers of 8), ..., A001029 (powers of 19), A009964 (powers of 20), ..., A009992 (powers of 48), A087752 (powers of 49)."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 121,
			"revision": 90,
			"time": "2021-03-12T22:32:34-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}