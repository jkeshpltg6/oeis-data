{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A092569",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 92569,
			"data": "0,1,2,3,6,7,4,5,14,15,12,13,10,11,8,9,30,31,28,29,26,27,24,25,22,23,20,21,18,19,16,17,62,63,60,61,58,59,56,57,54,55,52,53,50,51,48,49,46,47,44,45,42,43,40,41,38,39,36,37,34,35,32,33,126,127,124,125,122,123,120",
			"name": "Permutation of integers a(a(n)) = n. In binary representation of n, transformation of inner bits, 1 \u003c-\u003e 0, gives binary representation of a(n).",
			"comment": [
				"Primes which stay primes under transformation \"opposite inner bits\", A092570.",
				"This permutation transforms the enumeration system of positive irreducible fractions A020651/A020650 into the enumeration system A245327/A245326, and vice versa. - _Yosu Yurramendi_, Jun 16 2015",
				"A117120(a(n)) = a(A117120(n)), n \u003e 0.",
				"A258996(a(n)) = a(A258996(n)), n \u003e 0.",
				"A258746(a(n)) = a(A258746(n)), n \u003e 0.",
				"A054429(a(n)) = a(A054429(n)), n \u003e 0.",
				"a(n) = A054429(A065190(n)) = A065190(A054429(n), n \u003e 0. - _Yosu Yurramendi_, Mar 23 2017"
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A092569/b092569.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"formula": [
				"a(0) = 0, a(1) = 1, a(2) = 2, a(3) = 3, a(2^(m+1) +k) = a(2^m+k) + 2^(m+1),",
				"a(2^(m+1)+2^m+k) = a(2^m+k) + 2^m,     m \u003e= 1, 0 \u003c= k \u003c 2^m. - _Yosu Yurramendi_, Apr 02 2017"
			],
			"example": [
				"a(9)=15 because 9_10 = 1001_2, transformation of inner bits gives 1001_2 -\u003e 1111_2 = 15_10."
			],
			"mathematica": [
				"bb={0, 1, 2, 3};Do[id=IntegerDigits[n, 2];Do[id[[i]]=1-id[[i]], {i, 2, Length[id]-1}];bb=Append[bb, FromDigits[id, 2]], {n, 4, 1000}];fla=Flatten[bb]",
				"(* Second program: *)",
				"Table[If[n \u003c 2, n, Function[b, FromDigits[#, 2] \u0026@ Join[{First@ b}, Most[Rest@ b] /. { 0 -\u003e 1, 1 -\u003e 0}, {Last@ b}]]@ IntegerDigits[n, 2]], {n, 0, 70}] (* _Michael De Vlieger_, Apr 03 2017 *)"
			],
			"program": [
				"(PARI)T(n)={pow2=2;v=binary(n);L=#v-1;forstep(k=L,2,-1,if(v[k],n-=pow2,n+=pow2);pow2*=2);return(n)};",
				"for(n=0,70,print1(T(n),\", \")) \\\\ _Washington Bomfim_, Jan 18 2011",
				"(R)",
				"maxrow \u003c- 8 # by choice",
				"a \u003c- 1:3 # If it were c(1, 3, 2), it would be A054429",
				"for(m in 1:maxrow) for(k in 0:(2^m-1)){",
				"a[2^(m+1)+    k] = a[2^m+k] + 2^(m+1)",
				"a[2^(m+1)+2^m+k] = a[2^m+k] + 2^m",
				"}",
				"a",
				"# _Yosu Yurramendi_, Apr 10 2017"
			],
			"xref": [
				"Cf. A092570."
			],
			"keyword": "nonn,base,easy",
			"offset": "0,3",
			"author": "_Zak Seidov_, Feb 28 2004",
			"references": 12,
			"revision": 49,
			"time": "2018-01-27T02:32:32-05:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}