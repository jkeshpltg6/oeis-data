{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A131388",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 131388,
			"data": "1,2,4,3,6,10,8,5,11,7,12,19,14,22,16,9,18,28,20,31,21,33,24,13,26,40,27,15,30,46,32,17,34,52,36,55,38,58,39,60,42,64,44,23,47,25,48,73,50,76,51,78,54,82,56,29,59,88,57,89,61,92,63,96,66,100,68,35,70,106,72,37",
			"name": "Sequence (a(n)) generated by Rule 1 (in Comments) with a(1) = 1 and d(1) = 0.",
			"comment": [
				"Rule 1 follows.  For k \u003e= 1, let  A(k) = {a(1), …, a(k)} and D(k) = {d(1), …, d(k)}.  Begin with k = 1 and nonnegative integers a(1) and d(1).",
				"Step 1:   If there is an integer h such that 1 - a(k) \u003c h \u003c 0 and h is not in D(k) and a(k) + h is not in A(k), let d(k+1) be the greatest such h, let a(k+1) = a(k) + h, replace k by k + 1, and repeat Step 1; otherwise do Step 2.",
				"Step 2:  Let h be the least positive integer not in D(k) such that a(k) + h is not in A(k).  Let a(k+1) = a(k) + h and d(k+1) = h.  Replace k by k+1 and do Step 1.",
				"Conjecture:  if a(1) is an nonnegative integer and d(1) is an integer, then (a(n)) is a permutation of the nonnegative integers (if a(1) = 0) or a permutation of the positive integers (if a(1) \u003e 0).  Moreover, (d(n)) is a permutation of the integers if d(1) = 0, or of the nonzero integers if d(1) \u003e 0.",
				"See A257705 for a guide to related sequences."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A131388/b131388.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e"
			],
			"formula": [
				"a(k+1) - a(k) = d(k+1) for k \u003e= 1."
			],
			"example": [
				"a(2)=1+1, a(3)=a(2)+2, a(4)=a(3)+(-1), a(5)=a(4)+3, a(6)=a(5)+4."
			],
			"mathematica": [
				"(*Program 1 *)",
				"{a, f} = {{1}, {0}}; Do[tmp = {#, # - Last[a]} \u0026[Max[Complement[#, Intersection[a, #]] \u0026[Last[a] + Complement[#, Intersection[f, #]] \u0026[Range[2 - Last[a], -1]]]]];",
				"If[! IntegerQ[tmp[[1]]], tmp = {Last[a] + #, #} \u0026[NestWhile[# + 1 \u0026, 1, ! (! MemberQ[f,#] \u0026\u0026 ! MemberQ[a, Last[a] + #]) \u0026]]];",
				"AppendTo[a, tmp[[1]]]; AppendTo[f, tmp[[2]]], {400}];",
				"{a, f} (*{A131388, A131389}; _Peter J. C. Moses_, May 10 2015*)",
				"(*Program 2 *)",
				"a[1] = 1; d[1] = 0; k = 1; z = 10000; zz = 120;",
				"A[k_] := Table[a[i], {i, 1, k}]; diff[k_] := Table[d[i], {i, 1, k}];",
				"c[k_] := Complement[Range[-z, z], diff[k]];",
				"T[k_] := -a[k] + Complement[Range[z], A[k]];",
				"s[k_] := Intersection[Range[-a[k], -1], c[k], T[k]];",
				"Table[If[Length[s[k]] == 0, {h = Min[Intersection[c[k], T[k]]], a[k + 1] = a[k] + h, d[k + 1] = h, k = k + 1}, {h = Max[s[k]], a[k + 1] = a[k] + h, d[k + 1] = h, k = k + 1}], {i, 1, zz}];",
				"u = Table[a[k], {k, 1, zz}] (* A131388 *)",
				"Table[d[k], {k, 1, zz}]     (* A131389 *)"
			],
			"xref": [
				"Cf. A131389, A131390, A131391, A131392, A131393, A131394, A131395, A131396, A131397, A257705, A175498."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Jul 05 2007",
			"ext": [
				"Revised by _Clark Kimberling_, May 12 2015"
			],
			"references": 17,
			"revision": 12,
			"time": "2015-05-14T12:51:58-04:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}