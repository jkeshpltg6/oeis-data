{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A266703",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 266703,
			"data": "9,11,1,29,45,149,359,971,2511,6605,17261,45221,118359,309899,811295,2124029,5560749,14558261,38113991,99783755,261237231,683927981,1790546669,4687712069,12272589495,32130056459,84117579839,220222683101,576550469421,1509428725205",
			"name": "Coefficient of x^2 in minimal polynomial of the continued fraction [1^n,2/3,1,1,1,...], where 1^n means n ones.",
			"comment": [
				"See A265762 for a guide to related sequences."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A266703/b266703.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,2,-1)."
			],
			"formula": [
				"a(n) = 2*a(n-1) - 2*a(n-2) + a(n-3).",
				"G.f.:  (9 - 7 x - 39 x^2 + 14 x^3 - 4 x^4 + 2 x^5)/(1 - 2 x - 2 x^2 + x^3).",
				"a(n) = 2^(-n)*(-43*(-2)^n+(3+sqrt(5))^n*(-1+3*sqrt(5))-(3-sqrt(5))^n*(1+3*sqrt(5)))/5 for n\u003e2. - _Colin Barker_, Sep 29 2016"
			],
			"example": [
				"Let p(n,x) be the minimal polynomial of the number given by the n-th continued fraction:",
				"[2/3,1,1,1,...] = (1+3*sqrt(5))/6 has p(0,x) = -11 - 3 x + 9 x^2, so a(0) = 9;",
				"[1,2/3,1,1,...] = (19+9*sqrt(5))/22 has p(1,x) = -1 - 19 x + 11 x^2, so a(1) = 11;",
				"[1,1,2/3,1,...] = (-17+9*sqrt(5))/2 has p(2,x) = -29 + 17 x + x^2, so a(2) = 1."
			],
			"mathematica": [
				"u[n_] := Table[1, {k, 1, n}]; t[n_] := Join[u[n], {2/3}, {{1}}];",
				"f[n_] := FromContinuedFraction[t[n]];",
				"t = Table[MinimalPolynomial[f[n], x], {n, 0, 20}]",
				"Coefficient[t, x, 0] (* A266703 *)",
				"Coefficient[t, x, 1] (* A266704 *)",
				"Coefficient[t, x, 2] (* A266703 *)"
			],
			"program": [
				"(PARI) Vec((9-7*x-39*x^2+14*x^3-4*x^4+2*x^5)/((1+x)*(1-3*x+x^2)) + O(x^30)) \\\\ _Colin Barker_, Sep 29 2016"
			],
			"xref": [
				"Cf. A265762, A266704."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_Clark Kimberling_, Jan 09 2016",
			"references": 3,
			"revision": 11,
			"time": "2016-09-30T09:49:42-04:00",
			"created": "2016-01-09T19:58:03-05:00"
		}
	]
}