{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A105809",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 105809,
			"data": "1,1,1,2,2,1,3,4,3,1,5,7,7,4,1,8,12,14,11,5,1,13,20,26,25,16,6,1,21,33,46,51,41,22,7,1,34,54,79,97,92,63,29,8,1,55,88,133,176,189,155,92,37,9,1,89,143,221,309,365,344,247,129,46,10,1,144,232,364,530,674,709,591",
			"name": "Riordan array (1/(1-x-x^2), x/(1-x)).",
			"comment": [
				"Previous name was: A Fibonacci-Pascal matrix.",
				"Row sums are A027934, antidiagonal sums are A010049(n+1). Inverse is A105810.",
				"From _Wolfdieter Lang_, Oct 04 2014: (Start)",
				"In the column k of this triangle (without leading zeros) is the k-fold iterated partial sums of the Fibonacci numbers, starting with 1. A000045(n+1), A000071(n+3), A001924(n+1), A014162(n+1), A014166(n+1), ..., n \u003e= 0. See the Riordan property. - _Wolfdieter Lang_, Oct 03 2014",
				"For a combinatorial interpretation of these iterated partial sums see the H. Belbachir and A. Belkhir link. There table 1 shows in the rows these columns. In their notation (with r=k) f^(k)(n) = T(k,n+k).",
				"The A-sequence of this Riordan triangle is [1, 1] (see the recurrence for T(n,k), k\u003e=1, given in the formula section). The Z-sequence is A165326 = [1, repeat(1,-1)]. See the W. Lang link under A006232 for Riordan A- and Z-sequences.",
				"The alternating row sums are A212804. (End)"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A105809/b105809.txt\"\u003eRows n = 0..120 of table, flattened\u003c/a\u003e",
				"H. Belbachir and A. Belkhir, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Belbachir/belb2.html\"\u003eCombinatorial Expressions Involving Fibonacci, Hyperfibonacci, and Incomplete Fibonacci Numbers\u003c/a\u003e, Journal of Integer Sequences, Vol. 17 (2014), Article 14.4.3.",
				"Hung Viet Chu, \u003ca href=\"https://arxiv.org/abs/2106.03659\"\u003ePartial Sums of the Fibonacci Sequence\u003c/a\u003e, arXiv:2106.03659 [math.CO], 2021.",
				"\u003ca href=\"/index/Pas#Pascal\"\u003eIndex entries for triangles and arrays related to Pascal's triangle\u003c/a\u003e"
			],
			"formula": [
				"Riordan array (1/(1-x-x^2), x/(1-x)).",
				"Number triangle T(n, k) = Sum_{j=0..n} binomial(n-j, k+j); T(n, 0)=A000045(n);",
				"T(n, m) = T(n-1, m-1)+T(n-1, m).",
				"T(n,k) = Sum_{j=0..n} binomial(j,n+k-j). - _Paul Barry_, Oct 23 2006",
				"G.f. of row polynomials Sum_{k=0..n} T(n,k)*x^k is (1-z)/((1-z-z^2)*(1-(1+x)*z)) (Riordan property). - _Wolfdieter Lang_, Oct 04 2014",
				"T(n, k) = binomial(n, k)*hypergeom([1, k/2-n/2, k/2-n/2+1/2],[k+1, -n],-4) for n\u003e0. - _Peter Luschny_, Oct 10 2014"
			],
			"example": [
				"The triangle T(n,k) begins:",
				"n\\k   0   1   2    3    4    5    6    7    8   9  10 11 12 13 ...",
				"0:    1",
				"1:    1   1",
				"2:    2   2   1",
				"3:    3   4   3    1",
				"4:    5   7   7    4    1",
				"5:    8  12  14   11    5    1",
				"6:   13  20  26   25   16    6    1",
				"7:   21  33  46   51   41   22    7    1",
				"8:   34  54  79   97   92   63   29    8    1",
				"9:   55  88 133  176  189  155   92   37    9   1",
				"10:  89 143 221  309  365  344  247  129   46  10   1",
				"11: 144 232 364  530  674  709  591  376  175  56  11  1",
				"12: 233 376 596  894 1204 1383 1300  967  551 231  67 12  1",
				"13: 377 609 972 1490 2098 2587 2683 2267 1518 782 298 79 13  1",
				"... reformatted and extended - _Wolfdieter Lang_, Oct 03 2014",
				"------------------------------------------------------------------",
				"Recurrence from Z-sequence (see a comment above): 8 = T(0,5) = (+1)*5 + (+1)*7 + (-1)*7 + (+1)*4 + (-1)*1 = 8. - _Wolfdieter Lang_, Oct 04 2014"
			],
			"maple": [
				"T := (n,k) -\u003e `if`(n=0,1,binomial(n,k)*hypergeom([1,k/2-n/2,k/2-n/2+1/2], [k+1,-n], -4)); for n from 0 to 13 do seq(simplify(T(n,k)),k=0..n) od; # _Peter Luschny_, Oct 10 2014"
			],
			"mathematica": [
				"T[n_, k_] := Sum[Binomial[n-j, k+j], {j, 0, n}]; Table[T[n, k], {n, 0, 11}, {k, 0, n}] (* _Jean-François Alcover_, Jun 11 2019 *)"
			],
			"program": [
				"(Haskell)",
				"a105809 n k = a105809_tabl !! n !! k",
				"a105809_row n = a105809_tabl !! n",
				"a105809_tabl = map fst $ iterate",
				"   (\\(u:_, vs) -\u003e (vs, zipWith (+) ([u] ++ vs) (vs ++ [0]))) ([1], [1,1])",
				"-- _Reinhard Zumkeller_, Aug 15 2013"
			],
			"xref": [
				"Some other Fibonacci-Pascal triangles: A027926, A036355, A037027, A074829, A109906, A111006, A114197, A162741, A228074.",
				"Cf.A165326 (Z-sequence), A027934 (row sums, see comment above), A212804 (alternating row sums). - _Wolfdieter Lang_, Oct 04 2014"
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,4",
			"author": "_Paul Barry_, May 04 2005",
			"ext": [
				"Use first formula as a more descriptive name, _Joerg Arndt_, Jun 08 2021"
			],
			"references": 17,
			"revision": 37,
			"time": "2021-06-08T10:30:16-04:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}