{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A085695",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 85695,
			"data": "0,1,4,34,216,1525,10336,71149,486864,3339106,22881100,156843721,1074985344,7368157369,50501844796,346145466850,2372514562656,16261461342589,111457702083424,763942486626661,5236139616899400",
			"name": "a(n) = Fibonacci(n)*Fibonacci(3n)/2.",
			"comment": [
				"This is a divisibility sequence, that is, if n | m then a(n) | a(m). However, it is not a strong divisibility sequence. It is the case k = -3 of a 1-parameter family of 4th-order linear divisibility sequences with o.g.f. x*(1 - x^2)/( (1 - k*x + x^2)*(1 - (k^2 - 2)*x + x^2) ). Compare with A000290 (case k = 2) and A215465 (case k = 3). - _Peter Bala_, Jan 17 2014",
				"a(n) + a(n+1) is the numerator of the continued fraction [1,...,1,4,...,4] with n 1's followed by n 4's. - _Greg Dresden_ and _Hexuan Wang_, Aug 16 2021"
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A085695/b085695.txt\"\u003eTable of n, a(n) for n = 0..1197\u003c/a\u003e",
				"H. C. Williams and R. K. Guy, \u003ca href=\"http://dx.doi.org/10.1142/S1793042111004587\"\u003eSome fourth-order linear divisibility sequences\u003c/a\u003e, Intl. J. Number Theory 7 (5) (2011) 1255-1277.",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,19,4,-1)."
			],
			"formula": [
				"G.f.: ( x - x^3 )/( 1 - 4 x - 19 x^2 - 4 x^3 + x^4 ).",
				"Recurrence: a(n+4) = 4*a(n+3) + 19*a(n+2) + 4*a(n+1) - a(n)."
			],
			"mathematica": [
				"Array[Times @@ MapIndexed[Fibonacci[#]/First@ #2 \u0026, {#, 3 #}] \u0026, 21, 0] (* or *) LinearRecurrence[{4, 19, 4, -1}, {0, 1, 4, 34}, 21] (* or *)",
				"CoefficientList[Series[(x - x^3)/(1 - 4 x - 19 x^2 - 4 x^3 + x^4), {x, 0, 20}], x] (* _Michael De Vlieger_, Dec 17 2017 *)"
			],
			"program": [
				"(MuPAD) numlib::fibonacci(3*n)*numlib::fibonacci(n)/2 $ n = 0..35; // _Zerinvary Lajos_, May 13 2008",
				"(PARI) a(n) = fibonacci(n)*fibonacci(3*n)/2 \\\\ _Andrew Howroyd_, Dec 17 2017"
			],
			"xref": [
				"Cf. A215465."
			],
			"keyword": "easy,nonn",
			"offset": "0,3",
			"author": "_Emanuele Munarini_, Jul 18 2003",
			"references": 2,
			"revision": 28,
			"time": "2021-08-17T07:30:08-04:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}