{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A078740",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 78740,
			"data": "1,6,6,1,72,168,96,18,1,1440,5760,6120,2520,456,36,1,43200,259200,424800,285120,92520,15600,1380,60,1,1814400,15120000,34776000,33566400,16304400,4379760,682200,62400,3270,90,1,101606400,1117670400",
			"name": "Triangle of generalized Stirling numbers S_{3,2}(n,k) read by rows (n\u003e=1, 2\u003c=k\u003c=2n).",
			"comment": [
				"The sequence of row lengths of this array is [1,3,5,7,...] = A005408(n-1), n\u003e=1.",
				"For the scaled array s2_{3,2}(n,k) := a(n,k)*k!/((n+1)!*n!) see A090452."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A078740/b078740.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (rows 1 \u003c= n \u003c= 100, flattened).",
				"Paul Barry, \u003ca href=\"https://www.emis.de/journals/JIS/VOL22/Barry3/barry422.html\"\u003eGeneralized Catalan Numbers Associated with a Family of Pascal-like Triangles\u003c/a\u003e, J. Int. Seq., Vol. 22 (2019), Article 19.5.8.",
				"P. Blasiak, K. A. Penson and A. I. Solomon, \u003ca href=\"http://arXiv.org/abs/quant-ph/0212072\"\u003eThe Boson Normal Ordering Problem and Generalized Bell Numbers\u003c/a\u003e, arXiv:quant-ph/0212072, 2002.",
				"P. Blasiak, K. A. Penson and A. I. Solomon, \u003ca href=\"http://www.arXiv.org/abs/quant-ph/0402027\"\u003eThe general boson normal ordering problem\u003c/a\u003e, arXiv:quant-ph/0402027, 2004.",
				"P. Blasiak, K. A. Penson and A. I. Solomon, \u003ca href=\"http://dx.doi.org/10.1016/S0375-9601(03)00194-4\"\u003eThe general boson normal ordering problem\u003c/a\u003e, Phys. Lett. A 309 (2003) 198-205.",
				"A. Dzhumadildaev and D. Yeliussizov, \u003ca href=\"http://arxiv.org/abs/1408.6764v1\"\u003ePath decompositions of digraphs and their applications to Weyl algebra\u003c/a\u003e, arXiv preprint arXiv:1408.6764v1, 2014. [Version 1 contained many references to the OEIS, which were removed in Version 2. - _N. J. A. Sloane_, Mar 28 2015]",
				"Askar Dzhumadil'daev and Damir Yeliussizov, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v22i4p10\"\u003eWalks, partitions, and normal ordering\u003c/a\u003e, Electronic Journal of Combinatorics, 22(4) (2015), #P4.10.",
				"W. Lang, \u003ca href=\"/A078740/a078740.txt\"\u003eFirst 6 rows\u003c/a\u003e.",
				"Toufik Mansour, Matthias Schork and Mark Shattuck, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL15/Schork/schork2.html\"\u003eThe Generalized Stirling and Bell Numbers Revisited\u003c/a\u003e, Journal of Integer Sequences, Vol. 15 (2012), #12.8.3.",
				"M. Schork, \u003ca href=\"http://dx.doi.org/10.1088/0305-4470/36/16/314\"\u003eOn the combinatorics of normal ordering bosonic operators and deforming it\u003c/a\u003e, J. Phys. A 36 (2003) 4651-4665."
			],
			"formula": [
				"Recursion: a(n, k) = Sum(binomial(2, p)*fallfac(n-1-p+k, 2-p)*a(n-1, k-p), p=0..2), n\u003e=2, 2\u003c=k\u003c=2*n, a(1, 1)=1, else 0. Rewritten from eq.(19) of the Schork reference with r=3, s=2. fallfac(n, m) := A008279(n, m) (falling factorials triangle).",
				"a(n, k) = (((-1)^k)/k!)*Sum(((-1)^p)*binomial(k, p)*product(fallfac(p+(j-1)*(3-2), 2), j=1..n), p=2..k), n\u003e=1, 2\u003c=k\u003c=2*n, else 0. From eq. (12) of the Blasiak et al. reference with r=3, s=2.",
				"a(n, k) = (-1)^k n! (n+1)! 3F2(2-k, n+1, n+2; 2, 3; 1) / (2(k-2)!). - _Jean-François Alcover_, Dec 04 2013"
			],
			"example": [
				"1;",
				"6, 6, 1;",
				"72, 168, 96, 18, 1;",
				"..."
			],
			"mathematica": [
				"a[n_, k_] := (-1)^k*n!*(n+1)!*HypergeometricPFQ[{2-k, n+1, n+2}, {2, 3}, 1]/(2*(k-2)!); Table[a[n, k], {n, 1, 7}, {k, 2, 2*n}] // Flatten (* _Jean-François Alcover_, Dec 04 2013 *)"
			],
			"xref": [
				"Row sums give A078738. Cf. A078739.",
				"Cf. A005408, A090452."
			],
			"keyword": "nonn,tabf,easy",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Dec 21 2002",
			"ext": [
				"Edited by _Wolfdieter Lang_, Dec 23 2003"
			],
			"references": 19,
			"revision": 37,
			"time": "2019-11-20T21:52:27-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}