{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A106270",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 106270,
			"data": "1,-1,1,-2,-1,1,-5,-2,-1,1,-14,-5,-2,-1,1,-42,-14,-5,-2,-1,1,-132,-42,-14,-5,-2,-1,1,-429,-132,-42,-14,-5,-2,-1,1,-1430,-429,-132,-42,-14,-5,-2,-1,1,-4862,-1430,-429,-132,-42,-14,-5,-2,-1,1,-16796,-4862,-1430,-429,-132,-42,-14,-5,-2,-1,1,-58786,-16796",
			"name": "Inverse of number triangle A106268; triangle T(n,k), 0 \u003c= k \u003c= n.",
			"comment": [
				"Sequence array for the sequence a(n) = 2*0^n - C(n), where C = A000108 (Catalan numbers). Row sums are A106271. Anti-diagonal sums are A106272.",
				"The lower triangular matrix |T| (unsigned case) gives the Riordan matrix R = (c(x), x), a Toeplitz matrix. It is its own so called L-Eigen-matrix (cf. Bernstein - Sloane for such Eigen-sequences, and Barry for such eigentriangles), that is R*R = L*(R - I), with the infinite matrices I (identity) and L with matrix elements L(i, j) = delta(i,j-1) (Kronecker symbol; first upper diagonal with 1s). Thus R = L*(I - R^{-1}), and R^{-1} = I - L^{tr}*R (tr for transposed) is the Riordan matrix (1 - x*c(x), x) given in A343233. (For finite N X N matrices the R^{-1} equation is also valid, but for the other two ones the last row with only zeros has to be omitted.) - _Gary W. Adamson_ and _Wolfdieter Lang_, Apr 11 2021"
			],
			"link": [
				"Paul Barry, \u003ca href=\"http://arxiv.org/abs/1107.5490\"\u003eInvariant number triangles, eigentriangles and Somos-4 sequences\u003c/a\u003e, arXiv:1107.5490 [math.CO], 2011.",
				"M. Bernstein and N. J. A. Sloane, \u003ca href=\"http://arXiv.org/abs/math.CO/0205301\"\u003eSome canonical sequences of integers\u003c/a\u003e, Linear Alg. Applications, 226-228 (1995), 57-72; erratum 320 (2000), 210. [Link to arXiv version]"
			],
			"formula": [
				"Number triangle T(n, k) = if(k \u003c= n, 2*0^(n-k) - C(n-k), 0); Riordan array (2*sqrt(1-4*x)/(1+sqrt(1-4*x)), x) = (c(x)*sqrt(1-4*x), x), where c(x) is the g.f. of A000108.",
				"Bivariate g.f.: Sum_{n, k \u003e= 0} T(n,k)*x^n*y^k = (1/(1 - x*y)) * (2 - c(x)), where c(x) is the g.f. of A000108. - _Petros Hadjicostas_, Jul 15 2019"
			],
			"example": [
				"Triangle (with rows n \u003e= 0 and columns k \u003e= 0) begins as follows:",
				"    1;",
				"   -1,  1;",
				"   -2, -1,  1;",
				"   -5, -2, -1,  1;",
				"  -14, -5, -2, -1,  1;",
				"  ..."
			],
			"xref": [
				"Cf. A000108, A106268, A106271, A106272, A343233."
			],
			"keyword": "easy,sign,tabl",
			"offset": "0,4",
			"author": "_Paul Barry_, Apr 28 2005",
			"references": 6,
			"revision": 31,
			"time": "2021-05-09T10:02:20-04:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}