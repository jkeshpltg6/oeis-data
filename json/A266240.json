{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A266240",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 266240,
			"data": "1,4,1,32,28,336,664,105,4096,14912,8112,54912,326496,396792,50050,786432,7048192,15663360,6722816,11824384,150820608,544475232,518329776,56581525,184549376,3208396800,17388675072,30117189632,11100235520,2966845440",
			"name": "Triangle read by rows: T(n,g) is the number of rooted 2n-face triangulations in an orientable surface of genus g.",
			"comment": [
				"Row n contains floor((n+3)/2) terms."
			],
			"link": [
				"Gheorghe Coserea, \u003ca href=\"/A266240/b266240.txt\"\u003eRows n = 0..200, flattened\u003c/a\u003e",
				"Edward A. Bender, Zhicheng Gao, L. Bruce Richmond, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v15i1r51\"\u003e The map asymptotics constant tg\u003c/a\u003e, The Electronic Journal of Combinatorics, Volume 15 (2008), Research Paper #R51.",
				"Zhicheng Gao, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v17i1r155\"\u003eA Formula for the Bivariate Map Asymptotics Constants in terms of the Univariate Map Asymptotics Constants\u003c/a\u003e, The Electronic Journal of Combinatorics, Volume 17 (2010), Research Paper #R155.",
				"I. P. Goulden and D. M. Jackson, \u003ca href=\"https://www.math.uwaterloo.ca/~ipgoulde/KPpapermar08.pdf\"\u003eThe KP hierarchy, branched covers, and triangulations\u003c/a\u003e, Advances in Mathematics, Volume 219, Issue 3, 20 October 2008, Pages 932-951."
			],
			"formula": [
				"T(n,g) = f(n,g)/(3*n+2) for all n \u003e= 0 and 0 \u003c= g \u003c= (n+1)/2, where f(n,g) satisfies the quadratic recurrence equation f(n,g) = 4*(3*n+2)/(n+1)*(n*(3*n-2)*f(n-2,g-1) + Sum_{i=-1..n-1} Sum_{h=0..g} f(i,h)*f(n-2-i, g-h)) for n \u003e= 1 and g \u003e= 0 with the initial conditions f(-1,0)=1/2, f(0,0)=2 and f(n,g)=0 for g \u003c 0 or g \u003e (n+1)/2.",
				"For column g, as n goes to infinity we have T(n,g) ~ 3*6^((g-1)/2) * t(g) * n^(5*(g-1)/2) * (12*sqrt(3))^n, where t(g) = (A269418(g)/A269419(g)) / (2^(g-2) * gamma((5*g-1)/2)) and gamma is the Gamma function. - _Gheorghe Coserea_, Feb 26 2016"
			],
			"example": [
				"Triangle starts:",
				"n\\g    [0]          [1]          [2]          [3]          [4]",
				"[0]    1;",
				"[1]    4,           1;",
				"[2]    32,          28;",
				"[3]    336,         664,         105;",
				"[4]    4096,        14912,       8112;",
				"[5]    54912,       326496,      396792,      50050;",
				"[6]    786432,      7048192,     15663360,    6722816;",
				"[7]    11824384,    150820608,   544475232,   518329776,   56581525;",
				"[8]    184549376,   3208396800,  17388675072, 30117189632, 11100235520;",
				"[9]    ..."
			],
			"mathematica": [
				"T[n_ /; n \u003e= 0, g_] /; 0 \u003c= g \u003c= (n+1)/2 := f[n, g]/(3n+2); T[_, _] = 0; f[n_ /; n \u003e= 1, g_ /; g \u003e= 0] := f[n, g] = 4*(3*n+2)/(n+1)*(n*(3*n-2)*f[n - 2, g-1] + Sum[f[i, h]*f[n-2-i, g-h], {i, -1, n-1}, {h, 0, g}]); f[-1, 0] = 1/2; f[0, 0] = 2; f[_, _] = 0; Table[Table[T[n, g], {g, 0, Floor[(n + 1)/2]}], {n, 0, 9}] // Flatten (* _Jean-François Alcover_, Feb 27 2016 *)"
			],
			"program": [
				"(PARI)",
				"N = 10;",
				"m = matrix(N+2, N+2);",
				"mget(n,g) = {",
				"  if (g \u003c 0 || g \u003e (n+1)/2, return(0));",
				"  return(m[n+2,g+1]);",
				"}",
				"mset(n,g,v) = {",
				"  m[n+2,g+1] = v;",
				"}",
				"Cubic() = {",
				"  mset(-1,0,1/2);",
				"  mset(0,0,2);",
				"  for (n = 1, N,",
				"  for (g = 0, (n+1)\\2,",
				"    my(t1 = n * (3*n-2) * mget(n-2, g-1),",
				"       t2 = sum(i = -1, n-1, sum(h = 0, g,",
				"                mget(i,h) * mget(n-2-i, g-h))));",
				"    mset(n, g, 4*(3*n+2)/(n+1) * (t1 + t2))));",
				"  my(a = vector(N+1));",
				"  for (n = 0, N,",
				"    a[n+1] = vector(1 + (n+1)\\2);",
				"    for (g = 0, (n+1)\\2,",
				"         a[n+1][g+1] = mget(n, g));",
				"    a[n+1] = a[n+1]/(3*n+2));",
				"  return(a);",
				"}",
				"concat(Cubic())"
			],
			"xref": [
				"Columns k=0-4 give: A002005, A269473, A269474, A269475, A269476.",
				"Row sums give A062980.",
				"Cf. A269418, A269419."
			],
			"keyword": "nonn,tabf",
			"offset": "0,2",
			"author": "_Gheorghe Coserea_, Dec 25 2015",
			"references": 8,
			"revision": 78,
			"time": "2017-06-15T03:10:21-04:00",
			"created": "2015-12-26T03:08:02-05:00"
		}
	]
}