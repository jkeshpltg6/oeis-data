{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A232635",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 232635,
			"data": "1,1,4,5,4,8,1,4,8,4,13,12,4,8,8,1,12,8,8,12,16,13,4,16,4,12,20,8,5,12,12,12,16,8,8,20,17,16,16,8,20,24,8,8,16,1,24,16,8,12,20,16,12,28,12,33,20,8,16,12,16,20,24,8,16,32,1,12,32,8,16,32,8",
			"name": "Expansion of psi(x) * phi(x^2)^2 in powers of x where phi(), psi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Theta function of cubic lattice with respect to point (1/4, 0, 0)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A232635/b232635.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-1/8) * eta(q^4)^10 / (eta(q) * eta(q^2)^2 * eta(q^8)^4) in powers of q.",
				"Euler transform of period 8 sequence [ 1, 3, 1, -7, 1, 3, 1, -3, ...].",
				"a(3*n + 2) = 4 * A213023(n)."
			],
			"example": [
				"G.f. = 1 + x + 4*x^2 + 5*x^3 + 4*x^4 + 8*x^5 + x^6 + 4*x^7 + 8*x^8 + 4*x^9 + ...",
				"G.f. = q + q^9 + 4*q^17 + 5*q^25 + 4*q^33 + 8*q^41 + q^49 + 4*q^57 + 8*q^65 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ q^4]^10 / (QPochhammer[ q] QPochhammer[ q^2]^2 QPochhammer[ q^8]^4), {q, 0, n}]"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^4 + A)^10 / (eta(x + A) * eta(x^2 + A)^2 * eta(x^8 + A)^4), n))}"
			],
			"xref": [
				"Cf. A213023."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Michael Somos_, Nov 27 2013",
			"references": 1,
			"revision": 10,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2013-11-28T02:51:47-05:00"
		}
	]
}