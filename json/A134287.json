{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A134287",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 134287,
			"data": "1,30,315,1960,8820,31752,97020,261360,637065,1431430,3006003,5962320,11262160,20391840,35581680,60093504,98590905,157608990,246142435,376372920,564559380,832117000,1206913500,1724814000,2431508625",
			"name": "Fifth column of triangle A103371 (without leading zeros).",
			"comment": [
				"Kekulé numbers for certain benzenoids.",
				"a(n) = K(L(n))*K(O(2,4,n)) with the Cyvin and Gutman Kekulé number notation. See p. 62 for the L(n) structure with K(L(n))=n+1 and p. 105 (i) for the O(k,m,n) structure and its Kekulé number. This corresponds to an essentially disconnected 7-tier benzenoid structure similar to the 6-tier structure shown on p. 230, nr. 23 (see A108647).",
				"a(n-5), n \u003e= 5, is the number of ways to put n identical objects into m=5 of altogether n distinguishable boxes (n-5 boxes stay empty)."
			],
			"reference": [
				"S. J. Cyvin and I. Gutman, Kekulé structures in benzenoid hydrocarbons, Lecture Notes in Chemistry, No. 46, Springer, New York, 1988."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A134287/b134287.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A103371(n+4,4), n \u003e= 0.",
				"a(n) = ((n+1)*(n+2)*(n+3)*(n+4))^2*(n+5)/2880, n \u003e= 0. 2880 = 4!*5! = A010790(4).",
				"G.f.: (1+20*x+60*x^2+40*x^3+5*x^4)/(1-x)^10. Numerator polynomial from fifth row of triangle A132813.",
				"a(n) = 5*C(n+5,5)^2/(n+5), n \u003e= 0. - _Zerinvary Lajos_, May 09 2008",
				"a(n) = (C(n+6,6)*C(n+5,4)+5*C(n+5,6)*C(n+5,4))/(n+5). - _Gary Detlefs_, Jan 06 2014"
			],
			"example": [
				"a(2)=315 because n=7 identical balls can be put into m=5 of n=7 distinguishable boxes in binomial(7,5)*(5!/(4!*1!)+ 5!/(3!*2!)) = 21*(5+10) = 315 ways. The m=5 part partitions of 7, namely (1^4,3) and (1^3,2^2) specify the filling of each of the 21 possible five box choices. - _Wolfdieter Lang_, Nov 13 2007"
			],
			"maple": [
				"seq(binomial(n+4,4)^2*(n+5)/5, n=0..24); # _Peter Luschny_, Jan 13 2014"
			],
			"mathematica": [
				"CoefficientList[Series[(1 + 20 x + 60 x^2 + 40 x^3 + 5 x^4)/(1 - x)^10, {x, 0, 24}], x]"
			],
			"program": [
				"(MuPAD) 5*binomial(n+5,5)^2/(n+5) $ n = 0..35; // _Zerinvary Lajos_, May 09 2008",
				"(PARI) a(n) = 5*binomial(n+5, 5)^2/(n+5); \\\\ _Michel Marcus_, Jan 07 2014",
				"(Haskell)",
				"a134287 = flip a103371 4 . (+ 4)  -- _Reinhard Zumkeller_, Apr 04 2014"
			],
			"xref": [
				"Cf. A108647 (fourth column of triangle A103371)."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Nov 13 2007",
			"references": 2,
			"revision": 31,
			"time": "2017-12-18T03:08:11-05:00",
			"created": "2007-12-09T03:00:00-05:00"
		}
	]
}