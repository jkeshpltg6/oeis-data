{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008725",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8725,
			"data": "1,2,3,4,5,6,7,9,11,13,15,17,19,21,24,27,30,33,36,39,42,46,50,54,58,62,66,70,75,80,85,90,95,100,105,111,117,123,129,135,141,147,154,161,168,175,182,189,196,204,212,220,228,236,244,252,261,270,279,288,297,306",
			"name": "Molien series for 3-dimensional group [2,n] = *22n.",
			"comment": [
				"a(n) is the number of partitions of n into parts 1 and 7, where there are two kinds of part 1. - _Joerg Arndt_, Sep 27 2020"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A008725/b008725.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=190\"\u003eEncyclopedia of Combinatorial Structures 190\u003c/a\u003e",
				"\u003ca href=\"/index/Mo#Molien\"\u003eIndex entries for Molien series\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_09\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,-1,0,0,0,0,1,-2,1)."
			],
			"formula": [
				"G.f.: 1/((1-x)^2*(1-x^7)).",
				"From _Mitch Harris_, Sep 08 2008: (Start)",
				"a(n) = Sum_{j=0..n+7} floor(j/7).",
				"a(n-7) = (1/2)*floor(n/7)*(2*n - 5 - 7*floor(n/7)). (End)",
				"a(n) = 2*a(n-1) - a(n-2) + a(n-7) - 2*a(n-8) + a(n-9). - _R. J. Mathar_, Apr 20 2010",
				"a(n) = A011867(n+5). - _Pontus von Brömssen_, Sep 27 2020"
			],
			"maple": [
				"1/((1-x)^2*(1-x^7)): seq(coeff(series(%, x, n+1), x, n), n=0..80);"
			],
			"mathematica": [
				"CoefficientList[Series[1/((1-x)^2*(1-x^7)), {x, 0, 80}], x] (* _Vincenzo Librandi_, Jun 11 2013 *)",
				"LinearRecurrence[{2,-1,0,0,0,0,1,-2,1}, {1,2,3,4,5,6,7,9,11}, 80] (* _Harvey P. Dale_, Sep 27 2014 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^80)); Vec(1/((1-x)^2*(1-x^7))) \\\\ _G. C. Greubel_, Sep 09 2019",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Integers(), 80); Coefficients(R!( 1/((1-x)^2*(1-x^7)) )); // _G. C. Greubel_, Sep 09 2019",
				"(Sage)",
				"def A008725_list(prec):",
				"    P.\u003cx\u003e = PowerSeriesRing(ZZ, prec)",
				"    return P(1/((1-x)^2*(1-x^7))).list()",
				"A008725_list(80) # _G. C. Greubel_, Sep 09 2019",
				"(GAP) a:=[1,2,3,4,5,6,7,9,11];; for n in [10..80] do a[n]:=2*a[n-1] -a[n-2]+a[n-7]-2*a[n-8]+a[n-9]; od; a; # _G. C. Greubel_, Sep 09 2019"
			],
			"xref": [
				"Cf. A011867."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Vladimir Joseph Stephan Orlovsky_, Mar 14 2010"
			],
			"references": 6,
			"revision": 43,
			"time": "2020-09-28T02:36:54-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}