{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A079908",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 79908,
			"data": "1,4,14,36,76,140,234,364,536,756,1030,1364,1764,2236,2786,3420,4144,4964,5886,6916,8060,9324,10714,12236,13896,15700,17654,19764,22036,24476,27090,29884,32864,36036,39406,42980,46764,50764,54986,59436",
			"name": "Solution to the Dancing School Problem with 3 girls and n+3 boys: f(3,n).",
			"comment": [
				"The Dancing School Problem: a line of g girls (g\u003e0) with integer heights ranging from m to m+g-1 cm and a line of g+h boys (h\u003e=0) ranging in height from m to m+g+h-1 cm are facing each other in a dancing school (m is the minimal height of both girls and boys).",
				"A girl of height l can choose a boy of her own height or taller with a maximum of l+h cm. We call the number of possible matchings f(g,h).",
				"This problem is equivalent to a rooks problem: The number of possible placings of g non-attacking rooks on a g X g+h chessboard with the restriction i \u003c= j \u003c= i+h for the placement of a rook on square (i,j): f(g,h) = per(B), the permanent of the corresponding (0,1)-matrix B, b(i, j)=1 if and only if i \u003c= j \u003c= i+h",
				"f(g,h) = per(B), the permanent of the (0,1)-matrix B of size g X g+h with b(i,j)=1 if and only if i \u003c= j \u003c= i+h.",
				"For fixed g, f(g,n) is polynomial in n for n \u003e= g-2. See reference."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A079908/b079908.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Lute Kamstra, \u003ca href=\"http://www.math.leidenuniv.nl/~naw/\"\u003eProblem 29 in Vol. 5/3, No. 1, Mar 2002, p. 96\u003c/a\u003e. See also Vol. 5/3, Nos. 3 and 4.",
				"Jaap Spies, \u003ca href=\"http://www.nieuwarchief.nl/serie5/pdf/naw5-2006-07-4-283.pdf\"\u003eDancing School Problems\u003c/a\u003e, Nieuw Archief voor Wiskunde 5/7 nr. 4, Dec 2006, pp. 283-285.",
				"Jaap Spies, \u003ca href=\"http://www.jaapspies.nl/mathfiles/dancingschool.pdf\"\u003eDancing School Problems, Permanent solutions of Problem 29\u003c/a\u003e.",
				"Jaap Spies, \u003ca href=\"http://www.jaapspies.nl/oeis/ds.sage\"\u003eSage program to compute f(g,h)\u003c/a\u003e.",
				"Jaap Spies, \u003ca href=\"http://www.jaapspies.nl/bookb5.pdf\"\u003eA Bit of Math, The Art of Problem Solving\u003c/a\u003e, Jaap Spies Publishers (2019).",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-6,4,-1)."
			],
			"formula": [
				"a(n) = max(1, n^3 + 3*n), found by elementary counting.",
				"G.f.: 1+2*x*(2-x+2*x^2)/(1-x)^4. - _R. J. Mathar_, Nov 19 2007"
			],
			"mathematica": [
				"Join[{1},Array[#^3+3*#\u0026,5!,1]] (* _Vladimir Joseph Stephan Orlovsky_, Jan 08 2011 *)"
			],
			"program": [
				"(PARI) concat(1,vector(30,n,n^3+3*n)) \\\\ _Derek Orr_, Jun 18 2015"
			],
			"xref": [
				"Cf. A061989, A079909-A079928.",
				"Cf. Essentially the same as A061989."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Jaap Spies_, Jan 28 2003",
			"references": 34,
			"revision": 43,
			"time": "2021-12-03T15:43:27-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}