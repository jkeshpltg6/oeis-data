{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A274888",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 274888,
			"data": "1,1,1,1,1,2,2,1,1,1,2,1,1,1,2,4,5,6,5,4,2,1,1,1,2,3,3,3,3,2,1,1,1,2,4,7,10,13,16,17,17,16,13,10,7,4,2,1,1,1,2,3,5,5,7,7,8,7,7,5,5,3,2,1,1,1,2,4,7,12,17,24,31,39,45,51,54,56,54,51,45,39,31,24,17,12,7,4,2,1",
			"name": "Triangle read by rows: the q-analog of the swinging factorial which is defined as q-multinomial([floor(n/2), n mod 2, floor(n/2)]).",
			"comment": [
				"The q-swing_factorial(n) is a univariate polynomial over the integers with degree floor((n+1)/2)^2 + ((n+1) mod 2) and at least floor(n/2) irreducible factors.",
				"Evaluated at q=1 q-swing_factorial(n) gives the swinging factorial A056040(n).",
				"Combinatorial interpretation: The definition of an orbital system is given in A232500 and in the link 'Orbitals'. The number of orbitals over n sectors is counted by the swinging factorial.",
				"The major index of an orbital is the sum of the positions of steps which are immediately followed by a step with strictly smaller value. This statistic is an extension of the major index statistic given in A063746 which reappears in the even numbered rows here. This reflects the fact that the swinging factorial can be seen as an extension of the central binomial. As in the case of the central binomial also in the case of the swinging factorial the major index coincides with its q-analog."
			],
			"link": [
				"Peter Luschny, \u003ca href=\"https://oeis.org/wiki/User:Peter_Luschny/Orbitals\"\u003eOrbitals\u003c/a\u003e"
			],
			"formula": [
				"q_swing_factorial(n) = q_factorial(n)/q_factorial(floor(n/2))^2.",
				"q_swing_factorial(n) = q_binomial(n-eta(n),floor((n-eta(n))/2))*q_int(n)^eta(n) with eta(n) = (1-(-1)^n)/2.",
				"Recurrence: q_swing_factorial(0,q) = 1 and for n\u003e0 q_swing_factorial(n,q) = r*q_swing_factorial(n-1,q) with r = (1+q^(n/2))/[n/2;q] if n is even else r = [n;q]. Here [a;q] are the q_brackets.",
				"The generating polynomial for row n is P_n(p) = ((p^(floor(n/2)+1)-1)/(p-1))^((1-(-1)^n)/2)*Product_{i=0..floor(n/2)-1}((p^(n-i)-1)/(p^(i+1)-1))."
			],
			"example": [
				"The polynomials start:",
				"[0] 1",
				"[1] 1",
				"[2] q + 1",
				"[3] (q + 1) * (q^2 + q + 1)",
				"[4] (q^2 + 1) * (q^2 + q + 1)",
				"[5] (q^2 + 1) * (q^2 + q + 1) * (q^4 + q^3 + q^2 + q + 1)",
				"[6] (q + 1) * (q^2 - q + 1) * (q^2 + 1) * (q^4 + q^3 + q^2 + q + 1)",
				"The coefficients of the polynomials start:",
				"[n] [k=0,1,2,...] [row sum]",
				"[0] [1] [1]",
				"[1] [1] [1]",
				"[2] [1, 1] [2]",
				"[3] [1, 2, 2, 1] [6]",
				"[4] [1, 1, 2, 1, 1] [6]",
				"[5] [1, 2, 4, 5, 6, 5, 4, 2, 1] [30]",
				"[6] [1, 1, 2, 3, 3, 3, 3, 2, 1, 1] [20]",
				"[7] [1, 2, 4, 7, 10, 13, 16, 17, 17, 16, 13, 10, 7, 4, 2, 1] [140]",
				"[8] [1, 1, 2, 3, 5, 5, 7, 7, 8, 7, 7, 5, 5, 3, 2, 1, 1] [70]",
				"T(5, 4) = 6 because the 2 orbitals [-1,-1,1,1,0] and [-1,0,1,1,-1] have at position 4 and the 4 orbitals [0,-1,1,-1,1], [1,-1,0,-1,1], [1,-1,1,-1,0] and [1,0,1,-1,-1] at positions 1 and 3 a down step."
			],
			"maple": [
				"QSwingFactorial_coeffs := proc(n) local P,a,b;",
				"a := mul((p^(n-i)-1)/(p^(i+1)-1),i=0..iquo(n,2)-1);",
				"b := ((p^(iquo(n,2)+1)-1)/(p-1))^((1-(-1)^n)/2);",
				"P := simplify(a*b); seq(coeff(P,p,j),j=0..degree(P)) end:",
				"for n from 0 to 9 do print(QSwingFactorial_coeffs(n)) od;",
				"# Alternatively (recursive):",
				"with(QDifferenceEquations):",
				"QSwingRec := proc(n,q) local r; if n = 0 then return 1 fi:",
				"if irem(n,2) = 0 then r := (1+q^(n/2))/QBrackets(n/2,q)",
				"else r := QBrackets(n,q) fi; r*QSwingRec(n-1,q) end:",
				"Trow := proc(n) expand(QSimplify(QSwingRec(n,q)));",
				"seq(coeff(%,q,j),j=0..degree(%)) end: seq(Trow(n),n=0..10);"
			],
			"mathematica": [
				"p[n_] := QFactorial[n, q] / QFactorial[Quotient[n, 2], q]^2",
				"Table[CoefficientList[p[n] // FunctionExpand, q], {n,0,9}] // Flatten"
			],
			"program": [
				"(Sage)",
				"from sage.combinat.q_analogues import q_factorial",
				"def q_swing_factorial(n, q=None):",
				"    return q_factorial(n)//q_factorial(n//2)^2",
				"for n in (0..8): print(q_swing_factorial(n).list())",
				"(Sage) # uses[unit_orbitals from A274709]",
				"# Brute force counting",
				"def orbital_major_index(n):",
				"    S = [0]*(((n+1)//2)^2 + ((n+1) % 2))",
				"    for u in unit_orbitals(n):",
				"        L = [i+1 if u[i+1] \u003c u[i] else 0 for i in (0..n-2)]",
				"        # i+1 because u is 0-based whereas convention assumes 1-base.",
				"        S[sum(L)] += 1",
				"    return S",
				"for n in (0..9): print(orbital_major_index(n))"
			],
			"xref": [
				"Cf. A056040 (row sums), A274887 (q-factorial), A063746 (q-central_binomial).",
				"Other orbital statistics: A241477 (first zero crossing), A274706 (absolute integral), A274708 (peaks), A274709 (max. height), A274710 (number of turns), A274878 (span), A274879 (returns), A274880 (restarts), A274881 (ascent)."
			],
			"keyword": "nonn,tabf",
			"offset": "0,6",
			"author": "_Peter Luschny_, Jul 19 2016",
			"references": 6,
			"revision": 38,
			"time": "2020-03-28T19:15:31-04:00",
			"created": "2016-08-01T05:52:16-04:00"
		}
	]
}