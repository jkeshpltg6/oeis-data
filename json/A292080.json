{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A292080",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 292080,
			"data": "1,0,0,0,2,2,14,84,630,6096,55336,672160,7409300,104999520,1366363752,22068387264,331233939624,6005919062528,102144359744192,2054811316442112,39053339674065360,863259240785840640,18132529836143846560,436899062862222484480",
			"name": "Number of nonequivalent ways to place n non-attacking rooks on an n X n board with no rook on 2 main diagonals up to rotations and reflections of the board.",
			"comment": [
				"For odd n, there are no symmetrical configurations of non-attacking rooks without a rook in the main diagonal, so a(2n+1) = A003471(2n+1) / 8. For even n, configurations with rotational and diagonal symmetry are possible."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A292080/b292080.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e",
				"Andrew Howroyd, \u003ca href=\"/A292080/a292080.txt\"\u003eNonequivalent and Symmetric Solutions\u003c/a\u003e"
			],
			"formula": [
				"a(2n+1) = A003471(2n+1) / 8, a(2n) = (A003471(2n) + 2^n * A000166(n) + 2*A037224(2*n) + 2*A053871(n)) / 8."
			],
			"example": [
				"Case n=4: The 2 nonequivalent solutions are:",
				"   _ x _ _     _ x _ _",
				"   x _ _ _     _ _ _ x",
				"   _ _ _ x     x _ _ _",
				"   _ _ x _     _ _ x _",
				"Case n=5: The 2 nonequivalent solutions are:",
				"   _ x _ _ _   _ x _ _ _",
				"   x _ _ _ _   _ _ _ _ x",
				"   _ _ _ x _   x _ _ _ _",
				"   _ _ _ _ x   _ _ x _ _",
				"   _ _ x _ _   _ _ _ x _"
			],
			"mathematica": [
				"sf[n_] := n! * SeriesCoefficient[Exp[-x ] / (1 - x), {x, 0, n}];",
				"F[n_] := (Clear[v]; v[_] = 0; For[m = 4, m \u003c= n, m++, v[m] = (m - 1)*v[m - 1] + 2*If[OddQ[m], (m - 1)*v[m - 2], (m - 2)*If[m == 4, 1, v[m - 4]]]]; v[n]);",
				"d[n_] := Sum[(-1)^(n-k)*Binomial[n, k]*(2k)!/(2^k*k!), {k, 0, n}];",
				"R[n_] := If[OddQ[n], 0, (n - 1)!*2/(n/2 - 1)!];",
				"a[0] = 1; a[n_] := (F[n] + If[OddQ[n], 0, m = n/2; 2^m * sf[m] + 2*R[m] + 2*d[m]])/8;",
				"Table[a[n], {n, 0, 23}] (* _Jean-François Alcover_, Dec 28 2017, after _Andrew Howroyd_ *)"
			],
			"program": [
				"(PARI) \\\\ here sf is A000166, F is A003471, D is A053871, R(n) is A037224(2n).",
				"sf(n) = {n! * polcoeff( exp(-x + x * O(x^n)) / (1 - x), n)}",
				"F(n) = {my(v = vector(n)); for(n=4,length(v),v[n]=(n-1)*v[n-1]+2*if(n%2==1,(n-1)*v[n-2],(n-2)*if(n==4,1,v[n-4]))); v[n]}",
				"D(n) = {sum(k=0, n, (-1)^(n-k) * binomial(n,k) * (2*k)!/(2^k*k!))}",
				"R(n) = {if(n%2==1, 0, (n-1)!*2/(n/2-1)!)}",
				"a(n) = {(F(n) + if(n%2==1, 0, my(m=n/2); 2^m * sf(m) + 2*R(m) + 2*D(m)))/8}"
			],
			"xref": [
				"Cf. A000166, A000903, A003471, A037224, A053871, A064280."
			],
			"keyword": "nonn",
			"offset": "0,5",
			"author": "_Andrew Howroyd_, Sep 12 2017",
			"references": 2,
			"revision": 28,
			"time": "2018-01-14T03:23:26-05:00",
			"created": "2017-09-12T22:57:24-04:00"
		}
	]
}