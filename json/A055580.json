{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A055580",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 55580,
			"data": "1,7,31,111,351,1023,2815,7423,18943,47103,114687,274431,647167,1507327,3473407,7929855,17956863,40370175,90177535,200278015,442499071,973078527,2130706431,4647288831,10099884031,21877489663",
			"name": "Björner-Welker sequence: 2^n*(n^2 + n + 2) - 1.",
			"comment": [
				"a(n) is the d=1 Betti number of the complement of '3-equal' arrangements in n dimensional real space, see Björner-Welker reference, Table I, pp. 308-309, column '1' with k=3 and Th. 5.2, pp. 297-298.",
				"Binomial transform of [1/2, 2/3, 3/4, 4/5, ...] = 1/2, 7/6, 31/12, 111/20, 351/30, 1023/42, ..., where 2, 6, 12, 20, ... = A002378 (deleting the zero). - _Gary W. Adamson_, Apr 28 2005",
				"Number of three-dimensional block structures associated with n joint systems in the construction of stable underground structures. - _Richard M. Green_, Jul 26 2011",
				"Number of monotone mappings from the chain with three points to the complete binary tree of height n (n+1 levels). For example, the seven monotone mappings from the chain with three points (denoted 1,2,3, in order) to the complete binary tree with two levels (with a the root of the tree, and b, c the atoms) are: f(1)=f(2)=f(3)=a; f(1)=f(2)=a, f(3)=b; f(1)=f(2)=a, f(3)=c; f(1)=a, f(2)=f(3)=b; f(1)=a, f(2)=f(3)=c; f(1)=f(2)=f(3)=b; f(1)=f(2)=f(3)=c. - _Pietro Codara_, Mar 26 2015"
			],
			"reference": [
				"H. Barcelo and S. Smith, The discrete fundamental group of the order complex of B_n, Abstract 1020-05-141, 1020th Meeting Amer. Math. Soc., Cincinatti, Ohio, Oct 21-22, 2006."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A055580/b055580.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"H. Barcelo and R. Laubenbacher, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2004.03.016\"\u003ePerspectives on A-homotopy theory and its applications\u003c/a\u003e, Discr. Math., 298 (2005), 39-61.",
				"H. Barcelo and S. Smith, \u003ca href=\"http://arxiv.org/abs/0711.0915\"\u003eThe discrete fundamental group of the order complex of B_n\u003c/a\u003e, arXiv:0711.0915 [math.CO], 2007.",
				"A. Björner and V. Welker, \u003ca href=\"http://dx.doi.org/10.1006/aima.1995.1012\"\u003eThe homology of \"k-equal\" manifolds and related partition lattices\u003c/a\u003e, Adv. Math., 110 (1995), 277-313.",
				"Harry Crane, \u003ca href=\"https://ajc.maths.uq.edu.au/pdf/61/ajc_v61_p057.pdf\"\u003eLeft-right arrangements, set partitions, and pattern avoidance\u003c/a\u003e, Australasian Journal of Combinatorics, 61(1) (2015), 57-72.",
				"Robert Davis, Greg Simay, \u003ca href=\"https://arxiv.org/abs/2001.11089\"\u003eFurther Combinatorics and Applications of Two-Toned Tilings\u003c/a\u003e, arXiv:2001.11089 [math.CO], 2020.",
				"G.G. Kocharyan and A.M. Kulyukin, \u003ca href=\"http://dx.doi.org/10.1007/BF02335014\"\u003eConstruction of a three-dimensional block structure on the basis of jointed rock parameters estimating the stability of underground workings\u003c/a\u003e, Soil Mech. Found. Eng., 31 (1994), 62-66.",
				"A. F. Y. Zhao, \u003ca href=\"http://www.emis.de/journals/JIS/VOL17/Zhao/zhao3.html\"\u003ePattern Popularity in Multiply Restricted Permutations\u003c/a\u003e, Journal of Integer Sequences, 17 (2014), #14.10.3.",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (7,-18,20,-8)."
			],
			"formula": [
				"a(n) = A055252(n+3, 3).",
				"a(n) = Sum_{j=0..n-1} a(j) + A045618(n), n \u003e= 1.",
				"G.f.: 1/((1-2*x)^3*(1-x)).",
				"Partial sums of A001788 (without leading zero). - _Paul Barry_, Jun 26 2003",
				"a(n) = A001788(n) - A000337(n). - _Jon Perry_, Dec 12 2003",
				"a(n) = A119258(n+4,n). - _Reinhard Zumkeller_, May 11 2006",
				"E.g.f.: 2*(1 + 2*x + 2*x^2)*exp(2*x) - exp(x). - _G. C. Greubel_, Oct 28 2016",
				"a(n) = Sum_{k=0..n+1} Sum_{i=0..n+1} i^2 * C(k,i). - _Wesley Ivan Hurt_, Sep 21 2017"
			],
			"mathematica": [
				"Table[ n*(n+1)*2^(n-2), {n, 0, 26}] // Accumulate // Rest (* _Jean-François Alcover_, Jul 09 2013, after _Paul Barry_ *)",
				"LinearRecurrence[{7,-18,20,-8},{1,7,31,111},30] (* _Harvey P. Dale_, Nov 27 2014 *)"
			],
			"program": [
				"(MAGMA) [2^n*(n^2+n+2)-1: n in [0..35]]; // _Vincenzo Librandi_, Jul 28 2011",
				"(PARI) a(n)=(n^2+n+2)\u003c\u003cn-1 \\\\ _Charles R Greathouse IV_, Jul 28 2011"
			],
			"xref": [
				"Fourth column of triangle A055252.",
				"Cf. A055252, A055249, A045618, A000337, A001788, A066185."
			],
			"keyword": "easy,nonn",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, May 26 2000; revised Feb 12 2001",
			"ext": [
				"Edited (for consistency with change of offset) by _M. F. Hasler_, Nov 03 2012"
			],
			"references": 13,
			"revision": 91,
			"time": "2020-04-23T20:44:22-04:00",
			"created": "2000-06-15T03:00:00-04:00"
		}
	]
}