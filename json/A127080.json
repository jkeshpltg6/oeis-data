{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A127080",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 127080,
			"data": "1,1,1,1,1,-2,1,1,-1,-5,1,1,0,-4,12,1,1,1,-3,3,43,1,1,2,-2,-4,28,-120,1,1,3,-1,-9,15,-15,-531,1,1,4,0,-12,4,48,-288,1680,1,1,5,1,-13,-5,75,-105,105,8601,1,1,6,2,-12,-12,72,24,-624,3984,-30240,1,1,7,3,-9,-17,45,105,-735,945,-945,-172965",
			"name": "Infinite square array read by antidiagonals: Q(m, 0) = 1, Q(m, 1) = 1; Q(m, 2k) = (m - 2k + 1)*Q(m+1, 2k-1) - (2k-1)*Q(m+2,2k-2), m*Q(m, 2k+1) = (m - 2k)*Q(m+1, 2k) - 2k(m+1)*Q(m+2, 2k-1).",
			"comment": [
				"Comment from _N. J. A. Sloane_, Jan 29 2020 (Start)",
				"It looks like there was a missing 2 in the definition, which I have now corrected.  The old definition was:",
				"(Wrong!) Infinite square array read by antidiagonals: Q(m, 0) = 1, Q(m, 1) = 1; Q(m, 2k) = (m - 2k + 1)*Q(m+1, 2k-1) - (2k-1)*Q(m+2, k-2), m*Q(m, 2k+1) = (m - 2k)*Q(m+1, 2k) - 2k(m+1)*Q(m+2, 2k-1). (Wrong!) (End)"
			],
			"reference": [
				"V. van der Noort and N. J. A. Sloane, Paper in preparation, 2007."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A127080/b127080.txt\"\u003eAntidiagonals n = 0..100, flattened\u003c/a\u003e"
			],
			"formula": [
				"E.g.f.: Sum_{k \u003e= 0} Q(m,2k) x^k/k! = (1+4x)^((m-1)/2)/(1+2x)^(m/2), Sum_{k \u003e= 0} Q(m,2k+1) x^k/k! = (1+4x)^((m-2)/2)/(1+2x)^((m+1)/2)."
			],
			"example": [
				"Array begins:",
				"     1,    1,    1,    1,    1,   1,   1,    1,    1,    1, ... (A000012)",
				"     1,    1,    1,    1,    1,   1,   1,    1,    1,    1, ... (A000012)",
				"    -2,   -1,    0,    1,    2,   3,   4,    5,    6,    7, ... (A023444)",
				"    -5,   -4,   -3,   -2,   -1,   0,   1,    2,    3,    4, ... (A023447)",
				"    12,    3,   -4,   -9,  -12, -13, -12,   -9,   -4,    3, ... (A127146)",
				"    43,   28,   15,    4,   -5, -12, -17,  -20,  -21,  -20, ... (A127147)",
				"  -120,  -15,   48,   75,   72,  45,   0,  -57, -120, -183, ... (A127148)",
				"  -531, -288, -105,   24,  105, 144, 147,  120,   69,    0, ...",
				"  1680,  105, -624, -735, -432, 105, 720, 1281, 1680, 1833, ..."
			],
			"maple": [
				"f:= proc(k) option remember;",
				"      if `mod`(k,2)=0 then k!/(k/2)!",
				"    else 2^(k-1)*((k-1)/2)!*add(binomial(2*j, j)/8^j, j=0..((k-1)/2))",
				"      fi; end;",
				"Q:= proc(n, k) option remember;",
				"      if n=0 then (-1)^binomial(k, 2)*f(k)",
				"    elif k\u003c2 then 1",
				"    elif `mod`(k,2)=0 then (n-k+1)*Q(n+1,k-1) - (k-1)*Q(n+2,k-2)",
				"    else ( (n-k+1)*Q(n+1,k-1) - (k-1)*(n+1)*Q(n+2,k-2) )/n",
				"      fi; end;",
				"seq(seq(Q(n-k, k), k=0..n), n=0..12); # _G. C. Greubel_, Jan 30 2020"
			],
			"mathematica": [
				"Q[0, k_]:= Q[0,k]= (-1)^Binomial[k, 2]*If[EvenQ[k], k!/(k/2)!, 2^(k-1)*((k-1)/2)!* Sum[Binomial[2*j, j]/8^j, {j, 0, (k-1)/2}] ];",
				"Q[n_, k_]:= Q[n,k]= If[k\u003c2, 1, If[EvenQ[k], (n-k+1)*Q[n+1, k-1] - (k-1)*Q[n+2, k-2], ((n -k+1)*Q[n+1, k-1] - (k-1)*(n+1)*Q[n+2, k-2])/n]];",
				"Table[Q[n-k,k], {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, Jan 30 2020 *)"
			],
			"program": [
				"(Sage)",
				"@CachedFunction",
				"def f(k):",
				"    if (mod(k, 2)==0): return factorial(k)/factorial(k/2)",
				"    else: return 2^(k-1)*factorial((k-1)/2)*sum(binomial(2*j, j)/8^j for j in (0..(k-1)/2))",
				"def Q(n,k):",
				"    if (n==0): return (-1)^binomial(k, 2)*f(k)",
				"    elif (k\u003c2): return 1",
				"    elif (mod(k,2)==0): return (n-k+1)*Q(n+1,k-1) - (k-1)*Q(n+2,k-2)",
				"    else: return ( (n-k+1)*Q(n+1,k-1) - (k-1)*(n+1)*Q(n+2,k-2) )/n",
				"[[Q(n-k,k) for k in (0..n)] for n in (0..12)] # _G. C. Greubel_, Jan 30 2020"
			],
			"xref": [
				"See A105937 for another version.",
				"Columns give A127137, A127138, A127144, A127145;",
				"Rows give A127146, A127147, A127148."
			],
			"keyword": "sign,tabl",
			"offset": "0,6",
			"author": "_N. J. A. Sloane_, Mar 24 2007",
			"ext": [
				"More terms added by _G. C. Greubel_, Jan 30 2020"
			],
			"references": 10,
			"revision": 16,
			"time": "2020-01-30T17:21:33-05:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}