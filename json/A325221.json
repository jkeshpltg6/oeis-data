{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A325221",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 325221,
			"data": "1,1,0,5,4,0,61,148,16,0,1385,6744,2832,64,0,50521,410456,383856,47936,256,0,2702765,32947964,54480944,17142784,780544,1024,0,199360981,3402510924,8760740640,5199585280,686711040,12555264,4096,0,19391512145,441239943664,1632067372896,1569971730560,419867864320,26090711040,201199616,16384,0,2404879675441,70347660061552,353538702361888,502094919789184,227204970315520,30892394850304,965223559168,3220652032,65536,0",
			"name": "E.g.f.: C(x,k) = cn( i * Integral C(x,k) dx, k), where C(x,k) = Sum_{n\u003e=0} Sum_{j=0..n} T(n,j) * x^(2*n)*k^(2*j)/(2*n)!, as a triangle of coefficients T(n,j) read by rows.",
			"comment": [
				"Equals a row reversal of triangle A322232.",
				"Compare to cn(x,k) = 1 - Integral sn(x,k)*dn(x,k) dx, where sn(x,k), cn(x,k), and dn(x,k) are Jacobi elliptic functions (see triangle A060627)."
			],
			"formula": [
				"E.g.f. C = C(x,k) = Sum_{n\u003e=0} Sum_{j=0..n} T(n,j) * x^(2*n)*k^(2*j)/(2*n)!, along with related series S = S(x,k) and D = D(x,k), satisfies:",
				"(1a) S = Integral C^2*D dx.",
				"(1b) C = 1 + Integral S*C*D dx.",
				"(1c) D = 1 + k^2 * Integral S*C^2 dx.",
				"(2a) C^2 - S^2 = 1.",
				"(2b) D^2 - k^2*S^2 = 1.",
				"(3a) C + S = exp( Integral C*D dx ).",
				"(3b) D + k*S = exp( k * Integral C^2 dx ).",
				"(4a) S = sinh( Integral C*D dx ).",
				"(4b) S = sinh( k * Integral C^2 dx ) / k.",
				"(4c) C = cosh( Integral C*D dx ).",
				"(4d) D = cosh( k * Integral C^2 dx ).",
				"(5a) d/dx S = C^2*D.",
				"(5b) d/dx C = S*C*D.",
				"(5c) d/dx D = k^2 * S*C^2.",
				"Given sn(x,k), cn(x,k), and dn(x,k) are Jacobi elliptic functions, with i^2 = -1, k' = sqrt(1-k^2), then",
				"(6a) S = -i * sn( i * Integral C dx, k),",
				"(6b) C = cn( i * Integral C dx, k),",
				"(6c) D = dn( i * Integral C dx, k).",
				"(7a) S = sc( Integral C dx, k') = sn(Integral C dx, k')/cn(Integral C dx, k'),",
				"(7b) C = nc( Integral C dx, k') = 1/cn(Integral C dx, k'),",
				"(7c) D = dc( Integral C dx, k') = dn(Integral C dx, k')/cn(Integral C dx, k').",
				"Row sums equal ( (2*n)!/(n!*2^n) )^2 = A001818(n), the squares of the odd double factorials.",
				"Column T(n,0) = A000364(n), for n\u003e=0, where A000364 is the secant numbers."
			],
			"example": [
				"E.g.f.: C(x,k) = 1 + x^2/2! + (5 + 4*k^2)*x^4/4! + (61 + 148*k^2 + 16*k^4)*x^6/6! + (1385 + 6744*k^2 + 2832*k^4 + 64*k^6)*x^8/8! + (50521 + 410456*k^2 + 383856*k^4 + 47936*k^6 + 256*k^8)*x^10/10! + (2702765 + 32947964*k^2 + 54480944*k^4 + 17142784*k^6 + 780544*k^8 + 1024*k^10)*x^12/12! + (199360981 + 3402510924*k^2 + 8760740640*k^4 + 5199585280*k^6 + 686711040*k^8 + 12555264*k^10 + 4096*k^12)*x^14/14! + ...",
				"such that C(x,k) = cn( i * Integral C(x,k) dx, k).",
				"This triangle of coefficients T(n,j) of x^(2*n)*k^(2*j)/(2*n)! in e.g.f. C(x,k) begins:",
				"1;",
				"1, 0;",
				"5, 4, 0;",
				"61, 148, 16, 0;",
				"1385, 6744, 2832, 64, 0;",
				"50521, 410456, 383856, 47936, 256, 0;",
				"2702765, 32947964, 54480944, 17142784, 780544, 1024, 0;",
				"199360981, 3402510924, 8760740640, 5199585280, 686711040, 12555264, 4096, 0;",
				"19391512145, 441239943664, 1632067372896, 1569971730560, 419867864320, 26090711040, 201199616, 16384, 0;",
				"2404879675441, 70347660061552, 353538702361888, 502094919789184, 227204970315520, 30892394850304, 965223559168, 3220652032, 65536, 0; ...",
				"RELATED SERIES.",
				"The related series S(x,k), where C(x,k)^2 - S(x,k)^2 = 1, starts",
				"S(x,k) = x + (2 + 1*k^2)*x^3/3! + (16 + 28*k^2 + 1*k^4)*x^5/5! + (272 + 1032*k^2 + 270*k^4 + 1*k^6)*x^7/7! + (7936 + 52736*k^2 + 36096*k^4 + 2456*k^6 + 1*k^8)*x^9/9! + (353792 + 3646208*k^2 + 4766048*k^4 + 1035088*k^6 + 22138*k^8 + 1*k^10)*x^11/11! + (22368256 + 330545664*k^2 + 704357760*k^4 + 319830400*k^6 + 27426960*k^8 + 199284*k^10 + 1*k^12)*x^13/13! + (1903757312 + 38188155904*k^2 + 120536980224*k^4 + 93989648000*k^6 + 18598875760*k^8 + 702812568*k^10 + 1793606*k^12 + 1*k^14)*x^15/15! + ...",
				"The related series D(x,k), where D(x,k)^2 - k^2*S(x,k)^2 = 1, starts",
				"D(x,k) = 1 + k^2*x^2/2! + (8*k^2 + 1*k^4)*x^4/4! + (136*k^2 + 88*k^4 + 1*k^6)*x^6/6! + (3968*k^2 + 6240*k^4 + 816*k^6 + 1*k^8)*x^8/8! + (176896*k^2 + 513536*k^4 + 195216*k^6 + 7376*k^8 + 1*k^10)*x^10/10! + (11184128*k^2 + 51880064*k^4 + 39572864*k^6 + 5352544*k^8 + 66424*k^10 + 1*k^12)*x^12/12! + (951878656*k^2 + 6453433344*k^4 + 8258202240*k^6 + 2458228480*k^8 + 139127640*k^10 + 597864*k^12 + 1*k^14)*x^14/14! + ..."
			],
			"program": [
				"(PARI) N=10;",
				"{S=x; C=1; D=1; for(i=1, 2*N, S = intformal(C^2*D +O(x^(2*N+1))); C = 1 + intformal(S*C*D); D = 1 + k^2*intformal(S*C^2)); }",
				"{T(n,j) = (2*n)!*polcoeff(polcoeff(C, 2*n, x), 2*j, k)}",
				"for(n=0, N, for(j=0, n, print1( T(n,j), \", \")) ; print(\"\"))"
			],
			"xref": [
				"Cf. A325220 (S), A325222(D).",
				"Cf. A322232 (row reversal)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Paul D. Hanna_, Apr 13 2019",
			"references": 3,
			"revision": 13,
			"time": "2019-04-26T20:40:57-04:00",
			"created": "2019-04-13T16:08:04-04:00"
		}
	]
}