{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A179072",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 179072,
			"data": "-1,-2,0,0,-32,256,0,0,-8192,0,-262144,5242880,0,0,-33554432,0,-2684354560,0,0,8589934592000,0,0,932385860354048,160159261748363264,-1125899906842624,0,0,-225179981368524800,5260204364768739328,0,0",
			"name": "Chapman's \"evil\" determinants II.",
			"comment": [
				"Determinant of the k-by-k matrix with (i,j)-entry L((i+j)/p), where L(./p) denotes the Legendre symbol modulo p and p = p_n = 2k+1 is the n-th prime.",
				"Guy says \"Chapman has a number of conjectures which concern the distribution of quadratic residues.\" One is that if 3 \u003c p_n == 3 (mod 4), then a(n) = 0.",
				"It appears that a(n) is even, if p_n == 1 (mod 4).",
				"For any odd prime p, (p+1)/2-i+(p+1)/2-j == -(i+j-1) (mod p) and hence we have L(-1/p)*|L((i+j)/p)|_{i,j=1,...,(p-1)/2} = |L((i+j-1)/p)|_{i,j=1,...,(p-1)/2}. Thus the value of a(n) was actually determined in the first reference of R. Chapman. - _Zhi-Wei Sun_, Aug 21 2013"
			],
			"reference": [
				"Richard Guy, Unsolved Problems in Number Theory, 3rd ed., Springer, 2004, Section F5."
			],
			"link": [
				"Robin Chapman, \u003ca href=\"http://dx.doi.org/10.4064/aa115-3-4\"\u003eDeterminants of Legendre symbol matrices\u003c/a\u003e, Acta Arith. 115 (2004), 231-244.",
				"Robin Chapman, \u003ca href=\"http://dx.doi.org/10.1016/j.ejc.2003.02.001\"\u003eSteinitz classes of unimodular lattices\u003c/a\u003e, European J. Combin. 25 (2004), 487-493.",
				"Robin Chapman (2009), \u003ca href=\"http://secamlocal.ex.ac.uk/people/staff/rjchapma/etc/evildet.pdf\"\u003e My evil determinant problem\u003c/a\u003e",
				"Maxim Vsemirnov (2011), \u003ca href=\"http://arxiv.org/abs/1108.4031\"\u003eOn R. Chapman's ``evil determinant'': case p=1 (mod 4)\u003c/a\u003e, arXiv:1108.4031 [math.NT], 2011-2012.",
				"M. Vseminov, \u003ca href=\"http://dx.doi.org/10.1016/j.laa.2011.08.039\"\u003eOn the evaluation of R. Chapman's \"evil determinant\"\u003c/a\u003e, Linear Algebra Appl. 436(2012), 4101-4106.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Legendre_symbol\"\u003e Legendre symbol\u003c/a\u003e"
			],
			"example": [
				"p_4 = 7 = 2*3 + 1 and the 3 X 3 matrix (L((i+j)/7)) is",
				"   1, -1,  1",
				"  -1,  1, -1",
				"   1, -1, -1",
				"which has determinant 0, so a(4) = 0."
			],
			"mathematica": [
				"a[n_] := Module[{p, k}, p = Prime[n]; k = (p-1)/2; Det @ Table[JacobiSymbol[ i + j, p], {i, 1, k}, {j, 1, k}]];",
				"Table[a[n], {n, 2, 32}] (* _Jean-François Alcover_, Nov 18 2018 *)"
			],
			"xref": [
				"Cf. A179071 (Chapman's \"evil\" determinants I), A179073 (A179071 for p == 1 (mod 4)), A179074 (A179072 for p == 1 (mod 4))."
			],
			"keyword": "sign",
			"offset": "2,2",
			"author": "_Jonathan Sondow_ and _Wadim Zudilin_, Jun 29 2010",
			"references": 6,
			"revision": 24,
			"time": "2018-11-18T07:55:47-05:00",
			"created": "2010-07-11T03:00:00-04:00"
		}
	]
}