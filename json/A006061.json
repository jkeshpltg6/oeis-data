{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006061",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6061,
			"id": "M5385",
			"data": "1,121,11881,1164241,114083761,11179044361,1095432263641,107341182792481,10518340481399521,1030690025994360601,100997104206965939401,9896685522256667700721,969774184076946468731281",
			"name": "Star numbers (A003154) that are squares.",
			"reference": [
				"J.-M. De Koninck, Ces nombres qui nous fascinent, Entry 121, p. 42, Ellipses, Paris 2008.",
				"M. Gardner, Time Travel and Other Mathematical Bewilderments. Freeman, NY, 1988, p. 22.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A006061/b006061.txt\"\u003eTable of n, a(n) for n = 1..500\u003c/a\u003e",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/StarNumber.html\"\u003eStar Number\u003c/a\u003e"
			],
			"formula": [
				"A007667 = 3*square star numbers (A006061) + 2.",
				"a(n) = denominator of kappa(sqrt(6)/A054320(n)) where kappa(x) is the sum of successive remainders by computing the Euclidean algorithm for (1, x). - _Thomas Baruchel_, Nov 29 2003",
				"From _Ignacio Larrosa Cañestro_, Feb 27 2000: (Start)",
				"a(n) = 99*(a(n-1) - a(n-2)) + a(n-3).",
				"a(n) = (5 - 2*sqrt(6))/8*(sqrt(3) + sqrt(2))^(4*n) + (5 + 2*sqrt(6))/8*(sqrt(3) - sqrt(2))^(4*n) - 1/4. (End)",
				"a(n) = 98*a(n-1) - a(n-2) + 24. - _Lekraj Beedassy_, Jul 14 2008"
			],
			"example": [
				"a(2)=121 because this is the 2nd star number (A003154) that is a square."
			],
			"maple": [
				"Digits := 1000:q := seq(floor(evalf(( (5+2*sqrt(6))^n*(sqrt(6)-2)-(5-2*sqrt(6))^n*(sqrt(6)+2))^2/16)),n=1..100);",
				"A006061:=-(1+22*z+z**2)/(z-1)/(z**2-98*z+1); # conjectured (correctly) by _Simon Plouffe_ in his 1992 dissertation"
			],
			"mathematica": [
				"CoefficientList[Series[(1+22*x+x^2)/((1-x)*(1-98*x+x^2)), {x,0,20}], x] (* or *) LinearRecurrence[{99,-99,1}, {1,121,11881}, 20] (* _G. C. Greubel_, Jul 23 2019 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^20)); Vec((1+22*x+x^2)/((1-x)*(1-98*x+x^2))) \\\\ _G. C. Greubel_, Jul 23 2019",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Integers(), 20); Coefficients(R!( (1+22*x+x^2)/((1-x)*(1-98*x+x^2)) )); // _G. C. Greubel_, Jul 23 2019",
				"(Sage) ((1+22*x+x^2)/((1-x)*(1-98*x+x^2))).series(x, 20).coefficients(x, sparse=False) # _G. C. Greubel_, Jul 23 2019",
				"(GAP) a:=[1,121,11881];; for n in [4..20] do a[n]:=99*a[n-1]-99*a[n-2]+a[n-3]; od; a; # _G. C. Greubel_, Jul 23 2019"
			],
			"xref": [
				"A007667 is 3*a(n)+2, sqrt(a(n)) is A054320.",
				"Cf. A003154."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Eric W. Weisstein_ and _Sascha Kurz_, Mar 24 2002"
			],
			"references": 8,
			"revision": 46,
			"time": "2021-03-12T22:32:37-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}