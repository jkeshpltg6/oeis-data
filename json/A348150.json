{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A348150",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 348150,
			"data": "1,12,111,1116,11112,111114,1111112,11111112,111111111,1111111125,11111111112,111111111126,1111111111116,11111111111114,111111111111114,1111111111111122,11111111111111112,111111111111111132,1111111111111111119,11111111111111111121,111111111111111111117",
			"name": "a(n) is the smallest Niven (or Harshad) number with exactly n digits and not containing the digit 0.",
			"comment": [
				"This sequence is inspired by a problem, proposed by Argentina during the 39th International Mathematical Olympiad in 1998 at Taipei, Taiwan, but not used for the competition.",
				"The problem asked for a proof that, for each positive integer n, there exists a n-digit number, not containing the digit 0 and that is divisible by the sum of its digits (see links: Diophante in French and Kalva in English).",
				"This sequence proposes the smallest such n-digit integer."
			],
			"link": [
				"Michael S. Branicky, \u003ca href=\"/A348150/b348150.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Diophante, \u003ca href=\"http://www.diophante.fr/problemes-par-themes/arithmetique-et-algebre/a1-pot-pourri/3455-a1960-bon-souvenir-de-buenos-aires\"\u003eBon souvenir de Buenos-Aires\u003c/a\u003e.",
				"Kalva, \u003ca href=\"https://prase.cz/kalva/short/sh98.html\"\u003e39th IMO 1998 shortlisted problems, problem N7\u003c/a\u003e.",
				"\u003ca href=\"/index/O#Olympiads\"\u003eIndex to sequences related to Olympiads\u003c/a\u003e."
			],
			"formula": [
				"a(n) = A002275(n) = R_n iff n is in A014950."
			],
			"example": [
				"111114 has 6 digits, does not contain 0 and is divisible by 1+1+1+1+1+4 = 9 (111114 = 9*12346), while 111111, 111112, 111113 are not respectively divisible by sum of their digits: 6, 7, 8; hence, a(6) = 111114."
			],
			"mathematica": [
				"hQ[n_] := ! MemberQ[(d = IntegerDigits[n]), 0] \u0026\u0026 Divisible[n, Plus @@ d]; a[n_] := Module[{k = (10^n - 1)/9}, While[! hQ[k], k++]; k]; Array[a, 30] (* _Amiram Eldar_, Oct 03 2021 *)"
			],
			"program": [
				"(PARI) a(n) = for(k=(10^n-1)/9, 10^n-1, if (vecmin(digits(k)) \u0026\u0026 !(k % sumdigits(k)), return (k)));  \\\\ _Michel Marcus_, Oct 03 2021",
				"(Python)",
				"def niven(n):",
				"    s = str(n)",
				"    return '0' not in s and n%sum(map(int, s)) == 0",
				"def a(n):",
				"    k = int(\"1\"*n)",
				"    while not niven(k): k += 1",
				"    return k",
				"print([a(n) for n in range(1, 22)]) # _Michael S. Branicky_, Oct 09 2021"
			],
			"xref": [
				"Cf. A002275, A005349, A217973."
			],
			"keyword": "nonn,base",
			"offset": "1,2",
			"author": "_Bernard Schott_, Oct 03 2021",
			"ext": [
				"More terms from _Amiram Eldar_, Oct 03 2021"
			],
			"references": 5,
			"revision": 35,
			"time": "2021-10-11T17:40:52-04:00",
			"created": "2021-10-11T13:22:56-04:00"
		}
	]
}