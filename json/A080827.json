{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A080827",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 80827,
			"data": "1,3,5,9,13,19,25,33,41,51,61,73,85,99,113,129,145,163,181,201,221,243,265,289,313,339,365,393,421,451,481,513,545,579,613,649,685,723,761,801,841,883,925,969,1013,1059,1105,1153,1201,1251,1301,1353,1405,1459",
			"name": "Rounded up staircase on natural numbers.",
			"comment": [
				"Represents the 'rounded up' staircase diagonal on A000027, arranged as a square array. A000982 is the 'rounded down' staircase.",
				"a(1)= 1, a(2n) = a(2n-1) + 2n, a(2n+1) = a(2n) +2n. - _Amarnath Murthy_, May 07 2003",
				"Partial sums of A131055. - _Paul Barry_, Jun 14 2008",
				"The same sequence arises in the triangular array of integers \u003e= 1 according to a simple \"zig zag\" rule for selection of terms. a(n-1) lies in the (n-1)-th row of the array and the second row of that subarray (with apex a(n-1)) contains just two numbers, one odd one even. The one with the same (odd) parity as a(n-1) is a(n). - _David James Sycamore_, Jul 29 2018"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A080827/b080827.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"J. C. F. de Winter, \u003ca href=\"http://pareonline.net/getvn.asp?v=18\u0026amp;n=10\"\u003eUsing the Student's t-test with extremely small sample sizes\u003c/a\u003e, Practical Assessment, Research \u0026 Evaluation, 18(10), 2013.",
				"David James Sycamore, \u003ca href=\"/A080827/a080827.jpg\"\u003eTriangular array\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,0,-2,1)."
			],
			"formula": [
				"a(n) = ceiling((n^2+1)/2).",
				"G.f.: x*(1+x-x^2+x^3)/((1+x)(1-x)^3); a(n) = n*(n+1)/2-floor((n-1)/2). - _Paul Barry_, Apr 12 2008 [corrected by _R. J. Mathar_, Jul 14 2013]",
				"From _Wesley Ivan Hurt_, Sep 08 2015: (Start)",
				"a(n) = 2*a(n-1) - 2*a(n-3) + a(n-4), n \u003e 4.",
				"a(n) = (n^2 + 2 - (1 - (-1)^n)/2)/2.",
				"a(n) = floor(n^2/2) + 1 = A007590(n-1) + 1. (End)"
			],
			"maple": [
				"A080827:=n-\u003e(n^2+2-(1-(-1)^n)/2)/2: seq(A080827(n), n=1..100); # _Wesley Ivan Hurt_, Sep 08 2015"
			],
			"mathematica": [
				"s1=0;lst={};Do[s1+=n;If[EvenQ[s1],s1-=1];AppendTo[lst,s1],{n,6!}];lst (* _Vladimir Joseph Stephan Orlovsky_, Jun 06 2009 *)",
				"CoefficientList[Series[(1 + x - x^2 + x^3) / ((1 + x) (1 - x)^3), {x, 0, 40}], x] (* _Vincenzo Librandi_, Aug 05 2013 *)"
			],
			"program": [
				"(MAGMA) [n*(n+1)/2-Floor((n-1)/2) : n in [1..60]]; // _Vincenzo Librandi_, Aug 05 2013",
				"(GAP) List([1..10],n-\u003eInt(n^2/2)+1); # _Muniru A Asiru_, Aug 02 2018"
			],
			"xref": [
				"Apart from leading term identical to A099392.",
				"Cf. A000027, A000982, A007590, A131055."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Paul Barry_, Feb 28 2003",
			"references": 13,
			"revision": 41,
			"time": "2018-08-23T16:14:26-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}