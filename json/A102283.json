{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A102283",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 102283,
			"data": "0,1,-1,0,1,-1,0,1,-1,0,1,-1,0,1,-1,0,1,-1,0,1,-1,0,1,-1,0,1,-1,0,1,-1,0,1,-1,0,1,-1,0,1,-1,0,1,-1,0,1,-1,0,1,-1,0,1,-1,0,1,-1,0,1,-1,0,1,-1,0,1,-1,0,1,-1,0,1,-1,0,1,-1,0,1,-1,0,1,-1,0,1,-1,0,1,-1,0,1,-1,0,1,-1,0,1,-1,0,1,-1,0,1,-1,0,1,-1,0,1,-1",
			"name": "Period 3: repeat [0, 1, -1].",
			"comment": [
				"The sequence is the non-principal Dirichlet character of the reduced residue system mod 3. (The other is A011655.) Associated Dirichlet L-functions are L(1, chi) = Sum_{n \u003e= 1} a(n)/n = A073010, L(2, chi)= Sum_{n \u003e= 1} a(n)/n^2 = A086724, or L(3, chi)= Sum_{n \u003e= 1} a(n)/n^3 = A129404. [Jolley eq 310] - _R. J. Mathar_, Jul 15 2010",
				"a(n) = 2*D(n) - L(n), where L(n) denotes the n-th Lucas number and D(n) denotes the so-called n-th quadrapell number -- defined and discussed by Dursun Tasci in his paper (see References below). We have D(n) = D(n-2) + 2*D(n-3) + D(n-4), D(0) = D(1) = D(2) = 1, D(3) = 2. G.f. D(x) = (1+x-x^3)/((1-x-x^2)(1+x+x^2)). - _Roman Witula_, Jul 31 2012",
				"This is a strong elliptic divisibility sequence t_n as given in [Kimberling, p. 16] where x = -1, y = 0, z = -1. - _Michael Somos_, Nov 27 2019"
			],
			"reference": [
				"M. N. Huxley, Area, Lattice Points and Exponential Sums, Oxford, 1996; p. 236.",
				"L. B. W. Jolley, Summation of Series, Dover Publications (1961)."
			],
			"link": [
				"C. Kimberling, \u003ca href=\"http://www.fq.math.ca/Scanned/17-1/kimberling1.pdf\"\u003eStrong divisibility sequences and some conjectures\u003c/a\u003e, Fib. Quart., 17 (1979), 13-17.",
				"R. J. Mathar, \u003ca href=\"http://arxiv.org/abs/1008.2547\"\u003eTable of Dirichlet L-series..\u003c/a\u003e, arXiv:1008.2547 [math.NT], 2010-2015, Table 2, Table 22 for m=3, r=2.",
				"D. Tasci, \u003ca href=\"http://dergipark.ulakbim.gov.tr/hujms/article/view/5000017451\"\u003eOn Quadrapell Numbers and Quadrapell Polynomials\u003c/a\u003e, Hacettepe J. Math. Stat., 38 (3) (2009), 265-275.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/KroneckerSymbol.html\"\u003eKronecker Symbol.\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Kronecker_symbol\"\u003eKronecker Symbol.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (-1,-1)."
			],
			"formula": [
				"a(n) = A049347(n-1).",
				"a(n) = -a(n-1) - a(n-2); a(0) = 0, a(1) = 1. G.f.: x/(1+x+x^2). - _Philippe Deléham_, Nov 03 2008",
				"a(n) = -(1/3)*((n mod 3) - 2*((n+1) mod 3) + ((n+2) mod 3)). a(n) = -(1/3)*i*sqrt(3)*(-1/2 + (1/2)*i*sqrt(3))^n + (1/3)*i*sqrt(3)*(-1/2 - (1/2)*i*sqrt(3))^n, with n \u003e= 0 and i = sqrt(-1). - _Paolo P. Lava_, Nov 06 2008",
				"a(n) = -2*sin(4*Pi*n/3)/sqrt(3) = 2*sin(8*Pi*n/3)/sqrt(3). - _Jaume Oliver Lafont_, Dec 05 2008",
				"a(n) = 2*sin(2*Pi*n/3)/sqrt(3), which immediately follows from Paolo Lava's formula. - _Roman Witula_, Jul 31 2012",
				"a(n) = Legendre(n, 3), the Legendre symbol for p = 3. - _Alonso del Arte_, Feb 06 2013",
				"a(n) = (-3/n), where (k/n) is the Kronecker symbol. See the Eric Weisstein and Wikipedia links. - _Wolfdieter Lang_, May 29 2013",
				"Dirichlet g.f.: L(chi_2(3),s), with chi_2(3) the nontrivial Dirichlet character modulo 3. - _Ralf Stephan_, Mar 27 2015",
				"a(n) = a(n-3) for n \u003e 2. - _Wesley Ivan Hurt_, Jul 02 2016",
				"E.g.f.: 2*sin(sqrt(3)*x/2)*exp(-x/2)/sqrt(3). - _Ilya Gutkovskiy_, Jul 02 2016",
				"a(n) = H(2*n, 1, 1/2) for n \u003e 0 where H(n, a, b) = hypergeom([a - n/2, b - n/2], [1 - n], 4). - _Peter Luschny_, Sep 03 2019",
				"Euler transform of length 3 sequence [-1, 0, 1]. - _Michael Somos_, Nov 27 2019",
				"a(n) = n - 3*floor((n+1)/3). - _Wolfdieter Lang_, Oct 07 2021"
			],
			"example": [
				"G.f. = x - x^2 + x^4 - x^5 + x^7 - x^8 + x^10 - x^11 + ... - _Michael Somos_, Nov 27 2019"
			],
			"maple": [
				"ch:=n-\u003e if n mod 3 = 0 then 0; elif n mod 3 = 1 then 1; else -1; fi;",
				"seq(op([0, 1, -1]), n=1..50); # _Wesley Ivan Hurt_, Jul 02 2016"
			],
			"mathematica": [
				"Table[JacobiSymbol[n, 3], {n, 0, 99}] (* _Alonso del Arte_, Feb 06 2013 *)",
				"Table[KroneckerSymbol[-3, n], {n, 0, 99}] (* _Wolfdieter Lang_, May 30 2013 *)",
				"PadRight[{}, 100, {0, 1, -1}] (* _Wesley Ivan Hurt_, Jul 02 2016 *)",
				"a[ n_] := {1, -1, 0}[[Mod[n, 3, 1]]]; (* _Michael Somos_, Nov 27 2019 *)"
			],
			"program": [
				"(Sage)",
				"def A102283():",
				"    x, y = 0, -1",
				"    while True:",
				"        yield -x",
				"        x, y = y, -x -y",
				"a = A102283(); [next(a) for i in range(40)]  # _Peter Luschny_, Jul 11 2013",
				"(MAGMA) \u0026cat [[0, 1, -1]^^30]; // _Wesley Ivan Hurt_, Jul 02 2016",
				"(PARI) a(n)=([0,1; -1,-1]^n*[0;1])[1,1] \\\\ _Charles R Greathouse IV_, Jan 14 2017",
				"(PARI) {a(n) = [0, 1, -1][n%3 + 1]}; /* _Michael Somos_, Nov 27 2019 */"
			],
			"xref": [
				"Cf. A011655, A049347, A073010, A086724, A129404, A002324 (Mobius transform)."
			],
			"keyword": "sign,easy,mult",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_, Nov 02 2008",
			"references": 42,
			"revision": 69,
			"time": "2021-10-07T05:28:24-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}