{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A103372",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 103372,
			"data": "1,1,1,1,1,2,2,2,2,3,4,4,4,5,7,8,8,9,12,15,16,17,21,27,31,33,38,48,58,64,71,86,106,122,135,157,192,228,257,292,349,420,485,549,641,769,905,1034,1190,1410,1674,1939,2224,2600,3084,3613,4163,4824,5684,6697,7776",
			"name": "a(1) = a(2) = a(3) = a(4) = a(5) = 1 and for n\u003e5: a(n) = a(n-4) + a(n-5).",
			"comment": [
				"k=4 case of the family of sequences whose k=1 case is the Fibonacci sequence A000045, k=2 case is the Padovan sequence A000931 (offset so as to begin 1,1,1) and k=3 case is A079398 (offset so as to begin 1,1,1,1).",
				"The general case for integer k\u003e1 is defined: a(1) = a(2) = ... = a(k+1) and for n\u003e(k+1) a(n) = a(n-k) + a(n-[k+1]).",
				"For this k=4 case, the ratio of successive terms a(n)/a(n-1) approaches the unique positive root of the irreducible characteristic polynomial: x^5 - x - 1 = 0, A160155.",
				"The sequence of prime values in this k=4 case is A103382; The sequence of semiprime values in this k=4 case is A103392."
			],
			"reference": [
				"Zanten, A. J. van, The golden ratio in the arts of painting, building and mathematics, Nieuw Archief voor Wiskunde, 4 (17) (1999) 229-245."
			],
			"link": [
				"Indranil Ghosh, \u003ca href=\"/A103372/b103372.txt\"\u003eTable of n, a(n) for n = 1..14857\u003c/a\u003e",
				"J.-P. Allouche and T. Johnson, \u003ca href=\"http://www.math.jussieu.fr/~allouche/johnson2.pdf\"\u003eNarayana's Cows and Delayed Morphisms\u003c/a\u003e",
				"Richard Padovan, \u003ca href=\"http://www.nexusjournal.com/conferences/N2002-Padovan.html\"\u003eDom Hans van der Laan and the Plastic Number\u003c/a\u003e.",
				"E. S. Selmer, \u003ca href=\"http://www.mscand.dk/article/view/10478/8499\"\u003eOn the irreducibility of certain trinomials\u003c/a\u003e, Math. Scand., 4 (1956) 287-302.",
				"J. Shallit, \u003ca href=\"http://dx.doi.org/10.1016/0304-3975(88)90103-X\"\u003eA generalization of automatic sequences\u003c/a\u003e, Theoretical Computer Science, 61 (1988), 1-16.",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,0,0,1,1)."
			],
			"formula": [
				"G.f. -x*(1+x)*(1+x^2) / ( -1+x^4+x^5 ). - _R. J. Mathar_, Aug 26 2011",
				"a(n) = A124789(n-2)+A124798(n-1). - _R. J. Mathar_, Jun 30 2020"
			],
			"example": [
				"a(14) = 5 because a(14) = a(14-4) + a(14-5) = a(10) + a(9) = 3 + 2 = 5."
			],
			"mathematica": [
				"k = 4; Do[a[n] = 1, {n, k + 1}]; a[n_] := a[n] = a[n - k] + a[n - k - 1]; Array[a, 61]",
				"LinearRecurrence[{0,0,0,1,1},{1,1,1,1,1},70] (* _Harvey P. Dale_, Apr 22 2015 *)"
			],
			"program": [
				"(PARI) a(n)=([0,1,0,0,0; 0,0,1,0,0; 0,0,0,1,0; 0,0,0,0,1; 1,1,0,0,0]^(n-1)*[1;1;1;1;1])[1,1] \\\\ _Charles R Greathouse IV_, Oct 03 2016"
			],
			"xref": [
				"Cf. A000931, A079398, A103373-A103380, A103382, A103392."
			],
			"keyword": "nonn,easy",
			"offset": "1,6",
			"author": "_Jonathan Vos Post_, Feb 03 2005",
			"ext": [
				"Edited by _Ray Chandler_ and _Robert G. Wilson v_, Feb 06 2005"
			],
			"references": 23,
			"revision": 34,
			"time": "2021-11-12T16:14:08-05:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}