{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A052331",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 52331,
			"data": "0,1,2,4,8,3,16,5,32,9,64,6,128,17,10,256,512,33,1024,12,18,65,2048,7,4096,129,34,20,8192,11,16384,257,66,513,24,36,32768,1025,130,13,65536,19,131072,68,40,2049,262144,258,524288,4097,514,132,1048576,35",
			"name": "Inverse of A052330; A binary encoding of Fermi-Dirac factorization of n, shown in decimal.",
			"comment": [
				"Every number can be represented uniquely as a product of numbers of the form p^(2^k), sequence A050376. This sequence is a binary representation of this factorization, with a(p^(2^k)) = 2^(i-1), where i is the index (A302778) of p^(2^k) in A050376. Additive with a(p^e) = sum a(p^(2^e_k)) where e = sum(2^e_k) is the binary representation of e and a(p^(2^k)) is as described above. - _Franklin T. Adams-Watters_, Oct 25 2005 - Index offset corrected by _Antti Karttunen_, Apr 17 2018"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A052331/b052331.txt\"\u003eTable of n, a(n) for n = 1..4096\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(1)=0; a(n*A050376(k)) = a(n) + 2^k for a(n) \u003c 2^k, k=0, 1, ... - _Thomas Ordowski_, Mar 23 2005",
				"From _Antti Karttunen_, Apr 13 2018: (Start)",
				"a(1) = 0; for n \u003e 1, a(n) = A000079(A302785(n)-1) + a(A302776(n)).",
				"For n \u003e 1, a(n) = A000079(A302786(n)-1) * A302787(n).",
				"a(n) = A064358(n)-1.",
				"A000120(a(n)) = A064547(n).",
				"A069010(a(n)) = A302790(n).",
				"(End)"
			],
			"example": [
				"n = 84 has Fermi-Dirac factorization A050376(5) * A050376(3) * A050376(2) = 7*4*3. Thus a(84) = 2^(5-1) + 2^(3-1) + 2^(2-1) = 16 + 4 + 2 = 22 (\"10110\" in binary = A182979(84)). - _Antti Karttunen_, Apr 17 2018"
			],
			"program": [
				"(PARI) A052331=a(n)={for(i=1,#n=factor(n)~,n[2,i]\u003e1||next; m=binary(n[2,i]); n=concat(n,Mat(vector(#m-1,j,[n[1,i]^2^(#m-j),m[j]]~)));n[2,i]%=2); n||return(0); m=vecsort(n[1,]); forprime(p=1,m[#m],my(j=0);while(p^2^j\u003cm[#m],setsearch(m,p^2^j)||n=concat(n,[p^2^j,0]~);j++));vector(#n=vecsort(n),i,2^i)*n[2,]~\u003e\u003e1} \\\\ _M. F. Hasler_, Apr 08 2015",
				"(PARI)",
				"up_to_e = 8192;",
				"v050376 = vector(up_to_e);",
				"ispow2(n) = (n \u0026\u0026 !bitand(n,n-1));",
				"i = 0; for(n=1,oo,if(ispow2(isprimepower(n)), i++; v050376[i] = n); if(i == up_to_e,break));",
				"A052331(n) = { my(s=0,e); while(n \u003e 1, fordiv(n, d, if(((n/d)\u003e1)\u0026\u0026ispow2(isprimepower(n/d)), e = vecsearch(v050376, n/d); if(!e, print(\"v050376 too short!\"); return(1/0)); s += 2^(e-1); n = d; break))); (s); }; \\\\ _Antti Karttunen_, Apr 12 2018"
			],
			"xref": [
				"Cf. A050376, A052330, A064547, A302024, A302029, A302776, A302778, A302785, A302786, A302787, A302790, A302784.",
				"Cf. A182979 (same sequence shown in binary).",
				"One less than A064358.",
				"Cf. also A156552."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Christian G. Bower_, Dec 15 1999",
			"references": 42,
			"revision": 44,
			"time": "2018-04-27T17:09:40-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}