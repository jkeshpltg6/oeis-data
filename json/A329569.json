{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A329569",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 329569,
			"data": "0,1,2,5,6,11,12,17,26,35,36,47,24,77,32,65,62,149,74,9,8,39,14,15,4,3,28,33,38,69,10,51,20,21,58,93,16,81,46,13,70,27,76,37,34,97,52,7,30,49,40,31,22,67,82,19,42,25,64,85,18,109,54,43,88,139,84,145,94,79,112,55,48,289,144",
			"name": "For all n \u003e= 0, exactly 9 sums are prime among a(n+i) + a(n+j), 0 \u003c= i \u003c j \u003c 6: lexicographically earliest such sequence of distinct nonnegative numbers.",
			"comment": [
				"That is, there are nine primes, counted with multiplicity, among the 15 pairwise sums of any six consecutive terms. This is the maximum: there can't be more than 9 primes among the pairwise sums of any 6 numbers \u003e 1, cf. wiki page in LINKS.",
				"Conjectured to be a permutation of the nonnegative integers. The restriction to [1,oo) is then a permutation of the positive integers with similar properties, but different from the lexico-smallest one, A329568 = (1, 2, 3, 9, 4, 10, 27, ...).",
				"For n \u003e 5, a(n) is the smallest number not used earlier such that the set a(n) + {a(n-5}, ..., a(n-1)} has the same number of primes as a(n-6) + {a(n-5), ..., a(n-1)}. Such a number always exists, by definition of the sequence. (If it would not exist for a given n, the term a(n-1) (or earlier) \"is wrong and must be corrected\", so to say.) See the wiki page for further considerations about existence and surjectivity.",
				"For a(3) and a(4), one must exclude values 3 \u0026 4 to be able to continue the sequence indefinitely, but in all other cases (at least for several hundred terms), the greedy choice gives the correct solution.",
				"The values 3, 4 and 7 appear quite late at indices 25, 24 resp. 47."
			],
			"link": [
				"Éric Angelini, \u003ca href=\"http://list.seqfan.eu/pipermail/seqfan/2019-November/020145.html\"\u003ePrime sums from neighbouring terms\u003c/a\u003e, SeqFan list, Nov 11 2019.",
				"M. F. Hasler, \u003ca href=\"/wiki/User:M._F._Hasler/Prime_sums_from_neighboring_terms\"\u003ePrime sums from neighboring terms\u003c/a\u003e, OEIS Wiki, Nov 23 2019."
			],
			"program": [
				"(PARI) {A329569(n,show=0,o=0,N=9,M=5,X=[[3,3],[3,4],[4,3],[4,4]],p=[],u=o,U)=for(n=o+1,n, show\u003e0\u0026\u0026 print1(o\",\"); show\u003c0\u0026\u0026 listput(L,o); U+=1\u003c\u003c(o-u); U\u003e\u003e=-u+u+=valuation(U+1,2); p=concat(if(#p\u003e=M,p[^1],p),o); my(c=N-sum(i=2,#p, sum(j=1,i-1, isprime(p[i]+p[j])))); for(k=u,oo,bittest(U,k-u)|| min(c-#[0|x\u003c-p,isprime(x+k)],#p\u003e=M)|| setsearch(X,[n,k])|| [o=k,break])); show\u0026\u0026print([u]);o} \\\\ optional args: show=1: print a(o..n-1), show=-1: append them on global list L, in both cases print [least unused number] at the end. Parameters N,M,o,... allow to get other variants, see the wiki page for more."
			],
			"xref": [
				"Cf. A055265, A128280 (1 prime from 2 terms), A329333 (1 prime from 3 terms), A329405, ..., A329416 (N primes from M terms \u003e= 1), A329425, A329449, ..., A329581 (N primes from M terms \u003e= 0)."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_M. F. Hasler_, Feb 10 2020",
			"references": 4,
			"revision": 13,
			"time": "2020-02-12T21:05:47-05:00",
			"created": "2020-02-12T21:05:47-05:00"
		}
	]
}