{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A346934",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 346934,
			"data": "-1,52,-18660,24446016,-85000104000,647188836814080,-9486416237249952000,244072502056661870592000,-10282514440038927957603532800,671904022157076034864609763328000,-65203712913305114275839483698454528000",
			"name": "Permanent of the 2n X 2n matrix with the (i,j)-entry i-j (i,j=1..2n).",
			"comment": [
				"The author has proved that a((p-1)/2) == 2 (mod p) for any odd prime p.",
				"Conjecture 1: (-1)^n*a(n) \u003e 0 for all n \u003e 0. Also, a(n) == 0 (mod 2n+1) if 2n+1 is composite.",
				"For any permutation f of {1,...,2n+1}, clearly Product_{j=1..2n+1} (j-f(j)) = -Product_{k=1..2n+1} (k-f^{-1}(k)). Thus the permanent of the matrix [i-j]_{1\u003c=i,j\u003c=2n+1} vanishes.",
				"It is easy to see that per[i+j]_{1\u003c=i,j\u003cn} = per[i+(n-j)]_{1\u003c=i,j\u003cn} == per[i-j]_{1\u003c=i,j\u003cn} (mod n). Thus A204249(2n) == a(n) (mod 2n+1).",
				"Let D(2n) be the set of all derangements of {1,...,2n}. Clearly, a(n) is the sum of those Product_{j=1..2n}(j-f(j)) with f in the set D(2n).",
				"Conjecture 2: For any odd prime p, the sum of those 1/Product_{j=1..p-1}(j-f(j)) with f in the set D(p-1) is congruent to (-1)^((p-1)/2) modulo p."
			],
			"link": [
				"Vaclav Kotesovec, \u003ca href=\"/A346934/b346934.txt\"\u003eTable of n, a(n) for n = 1..19\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/2108.07723\"\u003eArithmetic properties of some permanents\u003c/a\u003e, arXiv:2108.07723 [math.GM], 2021."
			],
			"example": [
				"a(1) is the permanent of the matrix [1-1,1-2;2-1,2-2] = [0,-1;1,0], which equals -1."
			],
			"mathematica": [
				"a[n_]:=a[n]=Permanent[Table[i-j,{i,1,2n},{j,1,2n}]];",
				"Table[a[n],{n,1,11}]"
			],
			"program": [
				"(PARI) a(n) = matpermanent(matrix(2*n, 2*n, i, j, i-j)); \\\\ _Michel Marcus_, Aug 08 2021"
			],
			"xref": [
				"Cf. A000166, A204249, A346949."
			],
			"keyword": "sign",
			"offset": "1,2",
			"author": "_Zhi-Wei Sun_, Aug 08 2021",
			"references": 5,
			"revision": 28,
			"time": "2021-08-19T03:29:31-04:00",
			"created": "2021-08-08T11:16:56-04:00"
		}
	]
}