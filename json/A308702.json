{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A308702",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 308702,
			"data": "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,3,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,1",
			"name": "a(n) is the least integer k such that both k^2+2*n-1 and k^2+4*n are squares.",
			"comment": [
				"Motivated by the following original problem: the population of a city is a square number. With 100 more people, it would be a square plus one, with again 100 more people it would be a square. What is the initial population of the city? Answer: 2401. See Images des Mathématiques link.",
				"One remarks that one can solve the problem when the population increase is even and greater than 1. So in the sequence definition, 2*n is the population increase and k^2 is the initial population.",
				"One also notes that a(n) always exists with a(n) \u003c= n-1."
			],
			"link": [
				"Michel Marcus, \u003ca href=\"/A308702/b308702.txt\"\u003eTable of n, a(n) for n = 2..1000\u003c/a\u003e",
				"Ana Rechtman, \u003ca href=\"http://images.math.cnrs.fr/Juin-2019-1er-defi.html\"\u003eJuin 1er défi\u003c/a\u003e, Images des Mathématiques, CNRS, 2019 (in French)."
			],
			"example": [
				"For n=2, that is a population increase of 4, the population is 1 (a square), since we have 1+4-1 = 4 (a square) and 1+8 = 9 (a square).",
				"For n=28, that is a population increase of 56, the population is 9 (a square), since we have 9+56-1 = 64 (a square) and 9+112 = 121 (a square)."
			],
			"mathematica": [
				"okQ[m_, k_] := IntegerQ[Sqrt[m]] \u0026\u0026 IntegerQ[Sqrt[m+k-1]] \u0026\u0026 IntegerQ[ Sqrt[m+2k]];",
				"findm[k_] := Module[{m = 1}, While[!okQ[m^2, k], m++]; m];",
				"a[n_] := findm[2n];",
				"Table[a[n], {n, 2, 72}] (* _Jean-François Alcover_, Jun 18 2019, from PARI *)"
			],
			"program": [
				"(PARI) isok(m, k) = issquare(m) \u0026\u0026 issquare(m+k-1) \u0026\u0026 issquare(m+2*k);",
				"findm(k) = my(m=1); while (!isok(m^2,k), m++); m;",
				"a(n) = findm(2*n);"
			],
			"xref": [
				"Cf. A000290 (squares), A308703."
			],
			"keyword": "nonn",
			"offset": "2,2",
			"author": "_Michel Marcus_, Jun 18 2019",
			"references": 2,
			"revision": 18,
			"time": "2019-06-19T04:16:58-04:00",
			"created": "2019-06-18T09:37:24-04:00"
		}
	]
}