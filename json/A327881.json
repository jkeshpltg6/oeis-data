{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A327881",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 327881,
			"data": "0,0,1,3,0,10,75,126,196,1548,15525,39820,161106,358722,3705884,46623045,142988280,768721504,3560215293,12250746432,144581799790,2542575063630,8955836934660,55657973021431,319349051391228,1983548989621200,7898257536096850",
			"name": "Number of set partitions of [n] with distinct block sizes and one of the block sizes is 2.",
			"comment": [
				"Sum of multinomials M(n; lambda), where lambda ranges over all integer partitions of n into distinct parts and one part is 2."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A327881/b327881.txt\"\u003eTable of n, a(n) for n = 0..697\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Multinomial_theorem#Multinomial_coefficients\"\u003eMultinomial coefficients\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Partition_(number_theory)\"\u003ePartition (number theory)\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Partition_of_a_set\"\u003ePartition of a set\u003c/a\u003e"
			],
			"example": [
				"a(2) = 1: 12.",
				"a(3) = 3: 12|3, 13|2, 1|23.",
				"a(4) = 0.",
				"a(5) = 10: 123|45, 124|35, 125|34, 12|345, 134|25, 135|24, 13|245, 145|23, 14|235, 15|234."
			],
			"maple": [
				"b:= proc(n, i, k) option remember; `if`(i*(i+1)/2\u003cn, 0,",
				"     `if`(n=0, 1, `if`(i\u003c2, 0, b(n, i-1, `if`(i=k, 0, k)))+",
				"     `if`(i=k, 0, b(n-i, min(n-i, i-1), k)*binomial(n, i))))",
				"    end:",
				"a:= n-\u003e b(n$2, 0)-b(n$2, 2):",
				"seq(a(n), n=0..29);"
			],
			"mathematica": [
				"b[n_, i_, k_] := b[n, i, k] = If[i(i+1)/2 \u003c n, 0, If[n == 0, 1, If[i \u003c 2, 0, b[n, i - 1, If[i == k, 0, k]]] + If[i == k, 0, b[n - i, Min[n - i, i - 1], k] Binomial[n, i]]]];",
				"a[n_] := b[n, n, 0] - b[n, n, 2];",
				"a /@ Range[0, 29] (* _Jean-François Alcover_, Dec 18 2020, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Column k=2 of A327869."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Alois P. Heinz_, Sep 28 2019",
			"references": 2,
			"revision": 10,
			"time": "2020-12-18T04:02:16-05:00",
			"created": "2019-09-28T17:46:31-04:00"
		}
	]
}