{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097784",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97784,
			"data": "1,11,110,1090,10791,106821,1057420,10467380,103616381,1025696431,10153347930,100507782870,994924480771,9848737024841,97492445767640,965075720651560,9553264760747961,94567571886828051,936122454107532550,9266656969188497450",
			"name": "Partial sums of Chebyshev sequence S(n,10) = U(n,5) = A004189(n+1).",
			"link": [
				"Colin Barker, \u003ca href=\"/A097784/b097784.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"D. Fortin, \u003ca href=\"http://ijpam.eu/contents/2012-77-1/11/11.pdf\"\u003eB-spline Toeplitz Inverse Under Corner Perturbations\u003c/a\u003e, International Journal of Pure and Applied Mathematics, Volume 77, No. 1, 2012, 107-118. - From _N. J. A. Sloane_, Oct 22 2012",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (11,-11,1)."
			],
			"formula": [
				"a(n) = Sum_{k=0..n} S(k, 10) with S(k, 10) = U(k, 5) = A004189(k+1) Chebyshev's polynomials of the second kind.",
				"G.f.: 1/((1-x)*(1 - 10*x + x^2)) = 1/(1 - 11*x + 11*x^2 - x^3).",
				"a(n) = 11*a(n-1) - 11*a(n-2) + a(n-3) with n \u003e= 2, a(-1)=0, a(0)=1, a(1)=11.",
				"a(n) = 10*a(n-1) - a(n-2) + 1 with n \u003e= 1, a(-1)=0, a(0)=1.",
				"a(n) = (S(n+1, 10) - S(n, 10) - 1)/8.",
				"a(n) = (-6 + (27-11*sqrt(6))*(5 - 2*sqrt(6))^n + (5 + 2*sqrt(6))^n*(27 + 11*sqrt(6)))/48. - _Colin Barker_, Mar 05 2016"
			],
			"mathematica": [
				"LinearRecurrence[{11,-11,1}, {1,11,110}, 30] (* _G. C. Greubel_, May 24 2019 *)",
				"CoefficientList[Series[1/(1-11x+11x^2-x^3),{x,0,30}],x] (* _Harvey P. Dale_, Aug 24 2021 *)"
			],
			"program": [
				"(PARI) Vec(1/((1-x)*(1-10*x+x^2)) + O(x^30)) \\\\ _Colin Barker_, Jun 14 2015",
				"(MAGMA) I:=[1,11,110]; [n le 3 select I[n] else 11*Self(n-1)-11*Self(n-2) +Self(n-3): n in [1..30]]; // _G. C. Greubel_, May 24 2019",
				"(Sage) (1/((1-x)*(1 - 10*x + x^2))).series(x, 30).coefficients(x, sparse=False) # _G. C. Greubel_, May 24 2019",
				"(GAP) a:=[1,11,110];; for n in [4..30] do a[n]:=11*a[n-1]-11*a[n-2]+ a[n-3]; od; a; # _G. C. Greubel_, May 24 2019"
			],
			"xref": [
				"Cf. A098296.",
				"Cf. A212336 for more sequences with g.f. of the type 1/(1-k*x+k*x^2-x^3)."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Aug 31 2004",
			"references": 10,
			"revision": 36,
			"time": "2021-08-24T15:02:10-04:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}