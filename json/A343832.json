{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A343832",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 343832,
			"data": "1,4,31,358,5509,106096,2456299,66471826,2059640713,71920704124,2794938616471,119653108240414,5595650767265101,283841520215780008,15523069639558351459,910529206043204428426,57023540590242398853649,3797750659849704886903156,268025698704886063968108943",
			"name": "a(n) = Sum_{k=0..n} k! * binomial(n,k) * binomial(2*n+1,k).",
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A343832/b343832.txt\"\u003eTable of n, a(n) for n = 0..365\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Laguerre_polynomials\"\u003eLaguerre polynomials\u003c/a\u003e",
				"\u003ca href=\"/index/La#Laguerre\"\u003eIndex entries for sequences related to Laguerre polynomials\u003c/a\u003e"
			],
			"formula": [
				"a(n) = (2*n+1)! * Sum_{k=0..n} binomial(n,k)/(k+n+1)!.",
				"a(n) = n! * Sum_{k=0..n} binomial(2*n+1,k)/(n-k)!.",
				"a(n) = n! * LaguerreL(n, n+1, -1).",
				"a(n) = n! * [x^n] exp(x/(1 - x))/(1 - x)^(n+2).",
				"a(n) == 1 (mod 3).",
				"a(n) ~ 2^(2*n + 3/2) * n^n / exp(n-1). - _Vaclav Kotesovec_, May 02 2021"
			],
			"maple": [
				"a := n -\u003e add(k!*binomial(n, k)*binomial(2*n+1, k), k=0..n):",
				"a := n -\u003e n!*add(binomial(2*n+1, k)/(n-k)!, k=0..n):",
				"a := n -\u003e (-1)^n*KummerU(-n, n+2, -1):",
				"a := n -\u003e n!*LaguerreL(n, n+1, -1): # _Peter Luschny_, May 02 2021"
			],
			"mathematica": [
				"a[n_] := Sum[k! * Binomial[n, k] * Binomial[2*n+1, k], {k, 0, n}]; Array[a, 20, 0] (* _Amiram Eldar_, May 01 2021 *)",
				"Table[(-1)^n * HypergeometricU[-n, 2 + n, -1], {n, 0, 20}] (* _Vaclav Kotesovec_, May 02 2021 *)"
			],
			"program": [
				"(PARI) a(n) = sum(k=0, n, k!*binomial(n, k)*binomial(2*n+1, k));",
				"(PARI) a(n) = (2*n+1)!*sum(k=0, n, binomial(n, k)/(k+n+1)!);",
				"(PARI) a(n) = n!*sum(k=0, n, binomial(2*n+1, k)/(n-k)!);",
				"(PARI) a(n) = n!*pollaguerre(n, n+1, -1);"
			],
			"xref": [
				"Cf. A000522, A045721, A082545, A152059, A248668, A343830, A343831."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Seiichi Manyama_, May 01 2021",
			"references": 7,
			"revision": 54,
			"time": "2021-05-02T12:14:57-04:00",
			"created": "2021-05-01T06:50:56-04:00"
		}
	]
}