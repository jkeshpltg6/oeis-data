{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A171884",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 171884,
			"data": "0,1,3,6,2,7,13,20,12,21,11,22,10,23,9,24,8,25,43,62,42,63,41,64,40,65,39,66,38,67,37,68,36,69,35,70,34,71,33,72,32,73,31,74,30,75,29,76,28,77,27,78,26,79,133,188,132,189,131,190,130,191,129,192,128,193,127,194",
			"name": "Lexicographically earliest injective nonnegative sequence a(n) satisfying |a(n+1) - a(n)| = n for all n.",
			"comment": [
				"The map n -\u003e a(n) is an injective map to the nonnegative integers, i.e., no two terms are identical.",
				"Appears not to contain numbers from the following sets (grouped intentionally): {4, 5}, {14, 15, 16, 17, 18, 19}, {44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61}, etc. The numbers of terms in these groups appears to be A008776. - _Paul Raff_, Mar 15 2010",
				"The first 23 terms are shared with Recamán's sequence A005132, but from then on they are different. - _Philippe Deléham_, Mar 01 2013, _Omar E. Pol_, Jul 01 2013",
				"From _M. F. Hasler_, May 09 2013:",
				"It appears that the starting points of the gaps (4, 14, 44, 134, 404, 1214, ...) are given by A181655(2n) = A198643(n-1), and thus the ending points (5, 19, 61, ...) by A181655(2n) + A048473(n-1).",
				"The first differences have signs (grouped intentionally): +++, -, +++, -+-+-+-+- (5 times \"-\"), +++, -+...+- (17 times \"-\"), +++, ... where the number of minus signs is again given by A048473 = A008776-1. (End)",
				"A correspondent, Dennis Reichard, conjectures that (i) a(n) \u003c= 3.5*n for all n and (ii) the sequence covers 2/3 of all natural numbers. - _N. J. A. Sloane_, Jun 30 2018"
			],
			"link": [
				"R. Munafo, \u003ca href=\"http://mrob.com/pub/math/seq-a171884.html\"\u003eLexicographically earliest injective and unbounded sequence A(n) satisfying |A(n+1)-A(n)|=n for all n\u003c/a\u003e",
				"R. Munafo, \u003ca href=\"http://mrob.com/pub/math/main-A171884.txt\"\u003emain-A171884.c\u003c/a\u003e(C source code to generate the sequence)"
			],
			"formula": [
				"a(n+1) = a(n) +- n with - iff n is even but not n = 2 + 2*3^k. (Cf. comment from May 09 2013.) - _M. F. Hasler_, Apr 05 2019"
			],
			"example": [
				"We begin with 0, 0+1=1, 1+2=3. 3-3=0 cannot be the next term because 0 is already in the sequence so we go to 3+3=6. The next could be 6-4=2 or 6+4=10 but we choose 2 because it is smaller."
			],
			"mathematica": [
				"Contribution from _Paul Raff_, Mar 15 2010: (Start)",
				"A171884[{}, _, _] := {};",
				"A171884[L_List, max_Integer, True] := If[Length[L] == max, L, With[{n = Length[L]},",
				"  If[Last[L] - n \u003c 1 || MemberQ[L, Last[L] - n],",
				"    If[MemberQ[L, Last[L] + n],",
				"       A171884[Drop[L, -1], max, False],",
				"       A171884[Append[L, Last[L] + n], max, True]],",
				"    A171884[Append[L, Last[L] - n], max, True]]]]",
				"A171884[L_List, max_Integer, False] := With[{n = Length[L]},",
				"  If[MemberQ[L, Last[L] + n],",
				"     A171884[Drop[L, -1], max, False],",
				"     A171884[Append[L, Last[L] + n], max, True]]]",
				"A171884[{0}, 200, True]",
				"(End)"
			],
			"program": [
				"(PARI) A171884_upto(N,a=0,t=2)=vector(N,k, a+=if(!bitand(k,1), k-1, t-=1, 1-k, t=k-1)) \\\\ or:",
				"A171884_upto(N,a)=vector(N,k,a+=if(bitand(k,1)\u0026\u0026k\\2!=3^valuation(k-(k\u003e1),3),1-k,k-1)) \\\\ _M. F. Hasler_, Apr 05 2019"
			],
			"xref": [
				"Cf. A005132, which allows duplicate values.",
				"Cf. also A118201, in which every value of a(n) and of |a(n+1)-a(n)| occurs exactly once, but does not ensure that the latter is strictly increasing."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Robert Munafo_, Mar 11 2010",
			"ext": [
				"Definition edited by _M. F. Hasler_, Apr 01 2019"
			],
			"references": 5,
			"revision": 43,
			"time": "2019-05-10T20:10:16-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}