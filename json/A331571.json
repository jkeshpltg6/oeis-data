{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A331571",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 331571,
			"data": "1,1,1,1,1,1,1,2,0,1,1,4,3,0,1,1,8,23,0,0,1,1,16,290,184,0,0,1,1,32,4298,17488,840,0,0,1,1,64,79143,2780752,771305,0,0,0,1,1,128,1702923,689187720,1496866413,21770070,0,0,0,1,1,256,42299820,236477490418,5261551562405,585897733896,328149360,0,0,0,1",
			"name": "Array read by antidiagonals: A(n,k) is the number of binary matrices with k columns and any number of distinct nonzero rows with n ones in every column and columns in nonincreasing lexicographic order.",
			"comment": [
				"The condition that the columns be in nonincreasing order is equivalent to considering nonequivalent matrices up to permutation of columns."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A331571/b331571.txt\"\u003eTable of n, a(n) for n = 0..209\u003c/a\u003e"
			],
			"formula": [
				"A(n, k) = Sum_{j=0..k} abs(Stirling1(k, j))*A331567(n, j)/k!.",
				"A(n, k) = Sum_{j=0..k} binomial(k-1, k-j)*A331569(n, j).",
				"A(n, k) = 0 for k \u003e 0, n \u003e 2^(k-1).",
				"A331653(n) = Sum_{d|n} A(n/d, d)."
			],
			"example": [
				"Array begins:",
				"===============================================================",
				"n\\k | 0 1 2   3         4               5                 6",
				"----+----------------------------------------------------------",
				"  0 | 1 1 1   1         1               1                 1 ...",
				"  1 | 1 1 2   4         8              16                32 ...",
				"  2 | 1 0 3  23       290            4298             79143 ...",
				"  3 | 1 0 0 184     17488         2780752         689187720 ...",
				"  4 | 1 0 0 840    771305      1496866413     5261551562405 ...",
				"  5 | 1 0 0   0  21770070    585897733896 30607728081550686 ...",
				"  6 | 1 0 0   0 328149360 161088785679360 ...",
				"  ...",
				"The A(2,2) = 3 matrices are:",
				"   [1 1]  [1 0]  [1 0]",
				"   [1 0]  [1 1]  [0 1]",
				"   [0 1]  [0 1]  [1 1]"
			],
			"program": [
				"(PARI)",
				"WeighT(v)={Vec(exp(x*Ser(dirmul(v, vector(#v, n, (-1)^(n-1)/n))))-1, -#v)}",
				"D(p, n, k)={my(v=vector(n)); for(i=1, #p, v[p[i]]++); binomial(WeighT(v)[n] + k - 1, k)/prod(i=1, #v, i^v[i]*v[i]!)}",
				"T(n, k)={ my(m=n*k+1, q=Vec(exp(intformal(O(x^m) - x^n/(1-x)))), f=Vec(serlaplace(1/(1+x) + O(x*x^m))/(x-1))); if(n==0, 1, sum(j=1, m, my(s=0); forpart(p=j, s+=(-1)^#p*D(p, n, k), [1, n]); s*sum(i=j, m, q[i-j+1]*f[i]))); }"
			],
			"xref": [
				"Rows n=0..4 are A000012, A011782, A060090, A060491, A331652.",
				"Cf. A330942, A331567, A331569, A331570, A331572, A331653."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Andrew Howroyd_, Jan 20 2020",
			"references": 12,
			"revision": 13,
			"time": "2020-01-25T17:55:35-05:00",
			"created": "2020-01-22T20:15:15-05:00"
		}
	]
}