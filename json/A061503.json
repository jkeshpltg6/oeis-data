{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A061503",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 61503,
			"data": "1,4,7,12,15,24,27,34,39,48,51,66,69,78,87,96,99,114,117,132,141,150,153,174,179,188,195,210,213,240,243,254,263,272,281,306,309,318,327,348,351,378,381,396,411,420,423,450,455,470,479,494,497",
			"name": "a(n) = Sum_{k=1..n} tau(k^2), where tau is the number of divisors function A000005.",
			"comment": [
				"a(n) is the number of pairs of positive integers \u003c= n with their LCM \u003c= n. - _Andrew Howroyd_, Sep 01 2019"
			],
			"reference": [
				"Mentioned by _Steven Finch_ in a posting to the Number Theory List (NMBRTHRY(AT)LISTSERV.NODAK.EDU), Jun 13 2001."
			],
			"link": [
				"Harry J. Smith, \u003ca href=\"/A061503/b061503.txt\"\u003eTable of n, a(n) for n = 1..1024\u003c/a\u003e",
				"Kevin A. Broughan, \u003ca href=\"http://www.math.waikato.ac.nz/~kab/papers/div4.pdf\"\u003eRestricted divisor sums\u003c/a\u003e, Acta Arithmetica, vol. 101, (2002), pp. 105-114.",
				"Steven R. Finch, \u003ca href=\"http://arxiv.org/abs/2001.00578\"\u003eErrata and Addenda to Mathematical Constants\u003c/a\u003e, p. 19.",
				"Vaclav Kotesovec, \u003ca href=\"/A061503/a061503.jpg\"\u003eGraph - the asymptotic ratio\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/StieltjesConstants.html\"\u003eStieltjes Constants\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{j=1..n^2} floor(n/A019554(j)). - Antonio G. Astudillo (afg_astudillo(AT)hotmail.com), Jul 20 2002",
				"a(n) = Sum_{i=1..n} 2^omega(i) * floor(n/i). - _Enrique Pérez Herrero_, Sep 15 2012",
				"a(n) ~ 3/Pi^2 * n log^2 n. - _Charles R Greathouse IV_, Nov 08 2012",
				"a(n) ~ 3*n/Pi^2 * (log(n)^2 + log(n)*(-2 + 6*g - 24*z/Pi^2) + 2 - 6*g + 6*g^2 - 6*sg1 + 288*z^2/Pi^4 - 24*(-z + 3*g*z + z2)/ Pi^2), where g is the Euler-Mascheroni constant A001620, sg1 is the first Stieltjes constant (see A082633), z = Zeta'(2) (see A073002), z2 = Zeta''(2) = A201994. - _Vaclav Kotesovec_, Jan 30 2019",
				"a(n) = Sum_{k=1..n} A064608(floor(n/k)). - _Daniel Suteu_, Mar 09 2019"
			],
			"maple": [
				"with(numtheory): a:=n-\u003eadd(tau(k^2),k=1..n): seq(a(n),n=1..60); # _Muniru A Asiru_, Mar 09 2019"
			],
			"mathematica": [
				"DivisorSigma[0, Range[60]^2] // Accumulate (* _Jean-François Alcover_, Nov 25 2013 *)"
			],
			"program": [
				"(PARI) for (n=1, 1024, write(\"b061503.txt\", n, \" \", sum(k=1, n, numdiv(k^2)))) \\\\ _Harry J. Smith_, Jul 23 2009",
				"(PARI) t=0;v=vector(60,n,t+=numdiv(n^2)) \\\\ _Charles R Greathouse IV_, Nov 08 2012",
				"(Sage) def A061503(n) :",
				"    tau = sloane.A000005",
				"    return add(tau(k^2) for k in (1..n))",
				"[ A061503(i) for i in (1..19)] # _Peter Luschny_, Sep 15 2012",
				"(GAP) List([1..60],n-\u003eSum([1..n],k-\u003eTau(k^2))); # _Muniru A Asiru_, Mar 09 2019"
			],
			"xref": [
				"Cf. A000005, A061502. Partial sums of A048691."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Jun 14 2001",
			"ext": [
				"Name corrected by _Peter Luschny_, Sep 15 2012"
			],
			"references": 6,
			"revision": 57,
			"time": "2020-12-28T07:15:30-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}