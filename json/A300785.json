{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A300785",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 300785,
			"data": "1,1,1,1,127,1,1,1093,1093,1,1,3739,8905,3739,1,1,8905,30157,30157,8905,1,1,17431,71569,101935,71569,17431,1,1,30157,139861,241753,241753,139861,30157,1,1,47923,241753,472291,573217,472291,241753,47923,1,1,71569,383965,816229,1119721,1119721,816229,383965,71569,1",
			"name": "Triangle read by rows: T(n,k) = 140*k^3*(n-k)^3 - 14*k*(n-k) + 1; n \u003e= 0, 0 \u003c= k \u003c= n.",
			"comment": [
				"From _Kolosov Petro_, Apr 12 2020: (Start)",
				"Let be A(m, r) = A302971(m, r) / A304042(m, r).",
				"Let be L(m, n, k) = Sum_{r=0..m} A(m, r) * k^r * (n - k)^r.",
				"Then T(n, k) = L(3, n, k).",
				"T(n, k) is symmetric: T(n, k) = T(n, n-k). (End)"
			],
			"link": [
				"Muniru A Asiru, \u003ca href=\"/A300785/b300785.txt\"\u003eRows n=0..100 of triangle, flattened\u003c/a\u003e.",
				"Kolosov Petro, \u003ca href=\"https://arxiv.org/abs/1603.02468\"\u003eOn the link between Binomial Theorem and Discrete Convolution of Power Function\u003c/a\u003e, arXiv:1603.02468 [math.NT], 2016-2020."
			],
			"formula": [
				"From _Kolosov Petro_, Apr 12 2020: (Start)",
				"T(n, k) = 140*k^3*(n-k)^3 - 14*k*(n-k) + 1.",
				"T(n, k) = 140*A094053(n, k)^3 + 0*A094053(n, k)^2 - 14*A094053(n, k)^1 + 1.",
				"T(n+3, k) = 4*T(n+2, k) - 6*T(n+1, k) + 4*T(n, k) - T(n-1, k), for n \u003e= k.",
				"Sum_{k=1..n} T(n, k) = A001015(n).",
				"Sum_{k=0..n} T(n, k) = A258806(n).",
				"Sum_{k=0..n-1} T(n, k) = A001015(n).",
				"Sum_{k=1..n-1} T(n, k) = A258808(n).",
				"Sum_{k=1..n-1} T(n, k) = -A024005(n).",
				"Sum_{k=1..r} T(n, k) = -A316387(3, r, 0)*n^0 + A316387(3, r ,1)*n^1 - A316387(3, r, 2)*n^2 + A316387(3, r, 3)*n^3. (End)"
			],
			"example": [
				"Triangle begins:",
				"--------------------------------------------------------------------",
				"k=   0      1       2       3       4       5       6      7     8",
				"--------------------------------------------------------------------",
				"n=0: 1;",
				"n=1: 1,     1;",
				"n=2: 1,   127,      1;",
				"n=3: 1,  1093,   1093,      1;",
				"n=4: 1,  3739,   8905,   3739,      1;",
				"n=5: 1,  8905,  30157,  30157,   8905,      1;",
				"n=6: 1, 17431,  71569, 101935,  71569,  17431,      1;",
				"n=7: 1, 30157, 139861, 241753, 241753, 139861,  30157,     1;",
				"n=8: 1, 47923, 241753, 472291, 573217, 472291, 241753, 47923,    1;"
			],
			"maple": [
				"T:=(n,k)-\u003e140*k^3*(n-k)^3-14*k*(n-k)+1: seq(seq(T(n,k),k=0..n),n=0..9); # _Muniru A Asiru_, Dec 14 2018"
			],
			"mathematica": [
				"T[n_, k_] := 140*k^3*(n - k)^3 - 14*k*(n - k) + 1; Column[",
				"Table[T[n, k], {n, 0, 10}, {k, 0, n}], Center] (* From _Kolosov Petro_, Apr 12 2020 *)"
			],
			"program": [
				"(PARI) t(n, k) = 140*k^3*(n-k)^3-14*k*(n-k)+1",
				"trianglerows(n) = for(x=0, n-1, for(y=0, x, print1(t(x, y), \", \")); print(\"\"))",
				"/* Print initial 9 rows of triangle as follows */ trianglerows(9)",
				"(MAGMA) /* As triangle */ [[140*k^3*(n-k)^3-14*k*(n-k)+1: k in [0..n]]: n in [0..10]]; // _Bruno Berselli_, Mar 21 2018",
				"(Sage) [[140*k^3*(n-k)^3 - 14*k*(n-k)+1 for k in range(n+1)] for n in range(12)] # _G. C. Greubel_, Dec 14 2018",
				"(GAP) T:=Flat(List([0..9], n-\u003eList([0..n], k-\u003e140*k^3*(n-k)^3 - 14*k*(n-k)+1))); # _G. C. Greubel_, Dec 14 2018"
			],
			"xref": [
				"Various cases of L(m, n, k): A287326 (m=1), A300656 (m=2), This sequence (m=3). See comments for L(m, n, k).",
				"Row sums give A258806.",
				"Cf. A000584, A287326, A007318, A077028, A294317, A068236, A300656, A302971, A304042, A001015, A094053, A258808, A024005, A316387."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,5",
			"author": "_Kolosov Petro_, Mar 12 2018",
			"references": 10,
			"revision": 92,
			"time": "2020-04-15T03:00:32-04:00",
			"created": "2018-03-21T11:37:23-04:00"
		}
	]
}