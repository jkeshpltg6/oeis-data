{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A273167",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 273167,
			"data": "1,1,1,3,1,1,5,15,3,1,35,7,7,1,1,63,105,15,45,5,1,231,99,495,55,33,3,1,429,3003,1001,1001,91,91,7,1,6435,715,1001,273,455,35,15,1,1,12155,21879,1989,4641,1071,765,51,153,9,1,46189,20995,62985,4845,4845,969,4845,285,95,5,1",
			"name": "Numerators of coefficient triangle for expansion of x^(2*n) in terms of Chebyshev polynomials of the first kind T(2*m, x) (A127674).",
			"comment": [
				"The denominator triangle is given in A273168.",
				"The expansion is x^(2*n) = Sum_{m=0..n} R(n, m)*Tnx(2*m, x), n \u003e= 0, with the rational triangle R(n, m) = a(n, m)/A273168(n, m).",
				"  Compare this with A127673.",
				"This is equivalent to the expansion cos(x)^(2n) = Sum_{m=0..n} R(n, m)*cos(2*m*x), n \u003e= 0. Compare this with the even numbered rows of A273496.",
				"See A244420/A244421 for the expansion of x^(2*n+1) in terms of odd-indexed Chebyshev polynomials of the first kind.",
				"The signed rational triangle S(n, m) = R(n, m) * (-1)^m appears in the expansion sin(x)^(2n) = Sum_{m=0..n} S(n, m) * cos(2*m*x), n \u003e= 0. This is equivalent to the identity (1-x^2)^n =  Sum_{m=0..n} S(n, m) * T(2*m, x)."
			],
			"link": [
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e"
			],
			"formula": [
				"a(n, m) = numerator(R(n, m)), n \u003e= 0, m = 1, ..., n, with the rationals R(n, m) given by R(n, 0) = (1/2^(2*n-1))*binomial(2*n,n)/2 and R(n ,m) = (1/2^(2*n-1))*binomial(2*n, n-m) for m =1..n, n \u003e= 0."
			],
			"example": [
				"The triangle a(n, m) begins:",
				"n\\m     0     1    2    3    4   5  6   7 8 9",
				"0:      1",
				"1:      1     1",
				"2:      3     1    1",
				"3:      5    15    3    1",
				"4:     35     7    7    1    1",
				"5:     63   105   15   45    5   1",
				"6:    231    99  495   55   33   3  1",
				"7:    429  3003 1001 1001   91  91  7   1",
				"8:   6435   715 1001  273  455  35 15   1 1",
				"9:  12155 21879 1989 4641 1071 765 51 153 9 1",
				"...",
				"The rational triangle R(n, m) begins:",
				"n\\m  0       1      2     3      4     5  ...",
				"0:   1",
				"1:  1/2     1/2",
				"2:  3/8     1/2    1/8",
				"3:  5/16   15/32   3/16  1/32",
				"4: 35/128   7/16   7/32  1/16  1/128",
				"5: 63/256 105/256 15/64 45/512 5/256 1/512",
				"...",
				"row 6: 231/1024 99/256 495/2048 55/512 33/1024 3/512 1/2048,",
				"row 7: 429/2048 3003/8192 1001/4096 1001/8192 91/2048 91/8192 7/4096 1/8192,",
				"row 8: 6435/32768 715/2048 1001/4096 273/2048 455/8192 35/2048 15/4096 1/2048 1/32768,",
				"row 9: 12155/65536 21879/65536 1989/8192 4641/32768 1071/16384 765/32768 51/8192 153/131072 9/65536 1/131072,",
				"...",
				"n=3: x^6 = (5/16)*T(0, x) + (15/32)*T(2, x)",
				"  +(3/16)*T(4, x) + (1/32)*T(6,x).",
				"  cos^6(x) = (5/16) + (15/32)*cos(2*x) +",
				"    (3/16)*cos(4*x) + (1/32)*cos(6*x).",
				"  sin^6(x) = (5/16) - (15/32)*cos(2*x) +",
				"    (3/16)*cos(4*x) - (1/32)*cos(6*x)."
			],
			"mathematica": [
				"T[MaxN_] := Function[{n}, With[{exp = Expand[(1/2)^(2 n) (Exp[I x] + Exp[-I x])^(2 n)]}, Prepend[ 2 Coefficient[exp, Exp[I 2 # x]] \u0026 /@ Range[1, n], exp /. {Exp[_] -\u003e 0}]]][#] \u0026 /@ Range[0, MaxN];",
				"T[5] // ColumnForm",
				"T2[MaxN_] := Table[Inverse[Outer[Coefficient[#1, x, #2] \u0026, Prepend[ChebyshevT[#, x] \u0026 /@ Range[2 MaxN], 1], Range[0, 2 MaxN]]][[n, m]], {n, 1, 2 MaxN, 2}, {m, 1, n, 2}]",
				"T2[6] // ColumnForm (* _Bradley Klee_, Jun 14 2016 *)"
			],
			"program": [
				"(PARI) a(n, m) = if (m == 0, numerator((1/2^(2*n-1)) * binomial(2*n,n)/2), numerator((1/2^(2*n-1))*binomial(2*n, n-m)));",
				"tabl(nn) = for (n=0, nn, for (k=0, n, print1(a(n,k), \", \")); print()); \\\\ _Michel Marcus_, Jun 19 2016"
			],
			"xref": [
				"Cf. A127673, A127674, A244420, A244421, A273168."
			],
			"keyword": "nonn,tabl,frac,easy",
			"offset": "0,4",
			"author": "_Wolfdieter Lang_, Jun 12 2016",
			"references": 3,
			"revision": 27,
			"time": "2019-12-11T07:37:09-05:00",
			"created": "2016-06-22T09:55:57-04:00"
		}
	]
}