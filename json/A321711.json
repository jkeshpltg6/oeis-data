{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A321711",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 321711,
			"data": "1,1,0,3,0,0,11,9,0,1,53,120,60,40,9,309,1410,1800,1590,885,216,2119,16560,39960,55120,52065,29016,7570,16687,202755,801780,1696555,2433165,2300403,1326850,357435,148329,2624496,15606360,48387024,99650670,141429456,135382464,79738800,22040361,1468457,36080100,304274880,1323453180,3760709526,7493549868,10570597800,10199809980,6103007505,1721632024",
			"name": "Triangle T(n,k) read by rows: coefficients of polynomials P_n(t) defined in Formula section.",
			"link": [
				"Gheorghe Coserea, \u003ca href=\"/A321711/b321711.txt\"\u003eRows n = 0..13, flattened\u003c/a\u003e",
				"Shmuel Friedland, Giorgio Ottaviani, \u003ca href=\"https://arxiv.org/abs/1210.8316\"\u003eThe number of singular vector tuples and uniqueness of best rank one approximation of tensors\u003c/a\u003e, arXiv:1210.8316 [math.AG], 2013."
			],
			"formula": [
				"Let z1..zn be n variables and s1 = Sum_{k=1..n} zk, s2 = Sum_{k=1..n} zk^2, s12 = (s1^2 - s2)/2, fk = s2 + t*(s12 - zk*(s1 - zk)) + zk*(s1 - zk) for k=1..n; we define P_n(t) = [(z1..zn)^2] Product_{k=1..n} fk.",
				"A000255(n) = T(n,0).",
				"A007107(n) = T(n,n).",
				"A000681(n) = Sum_{k=0..n} T(n,k).",
				"A274308(n) = Sum_{k=0..n} T(n,k)*2^k."
			],
			"example": [
				"For n=3 we have s1 = z1 + z2 + z3, s2 = z1^2 + z2^2 + z3^2, s12 = z1*z2 + z1*z3 + z2*z3, f1 = z1^2 + z2^2 + z3^2 + t*z2*z3 + z1*(z2 + z3), f2 = z1^2 + z2^2 + z3^2 + t*z1*z3 + z2*(z1 + z3), f3 = z1^2 + z2^2 + z3^2 + t*z1*z2 + z3*(z1 + z2), [(z1*z2*z3)^2] f1*f2*f3 = 11 + 9*t + t^3, therefore P_3(t) = 11 + 9*t + t^3.",
				"A(x;t) = 1 + x + 3*x^2 + (11 + 9*t + t^3)*x^3 + (53 + 120*t + 60*t^2 + 40*t^3 + 9*t^4)*x^4 + ...",
				"Triangle starts:",
				"n\\k [0]    [1]     [2]     [3]      [4]      [5]      [6]      [7]",
				"[0] 1;",
				"[1] 1;     0;",
				"[2] 3;     0;      0;",
				"[3] 11,    9,      0,      1;",
				"[4] 53,    120,    60,     40,      9;",
				"[5] 309,   1410,   1800,   1590,    885,     216;",
				"[6] 2119,  16560,  39960,  55120,   52065,   29016,   7570;",
				"[7] 16687, 202755, 801780, 1696555, 2433165, 2300403, 1326850, 357435;",
				"[8] ..."
			],
			"program": [
				"(PARI)",
				"P(n, t='t) = {",
				"  my(z=vector(n, k, eval(Str(\"z\", k))),",
				"     s1=sum(k=1, #z, z[k]), s2=sum(k=1, #z, z[k]^2), s12=(s1^2 - s2)/2,",
				"     f=vector(n, k, s2 + t*(s12 - z[k]*(s1 - z[k])) + z[k]*(s1 - z[k])), g=1);",
				"  for (i=1, n, g *= f[i]; for(j=1, n, g=substpol(g, z[j]^3, 0)));",
				"  for (k=1, n, g=polcoef(g, 2, z[k]));",
				"  g;",
				"};",
				"seq(N) = concat([[1], [1, 0], [3, 0, 0]], apply(n-\u003eVecrev(P(n,'t)), [3..N]));",
				"concat(seq(9))"
			],
			"xref": [
				"Cf. A000255, A000681, A007107, A274308, A284989."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Gheorghe Coserea_, Nov 27 2018",
			"references": 2,
			"revision": 28,
			"time": "2018-12-24T21:31:02-05:00",
			"created": "2018-12-24T21:31:02-05:00"
		}
	]
}