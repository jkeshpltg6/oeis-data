{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A227229",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 227229,
			"data": "1,6,15,24,33,36,33,48,69,78,90,72,51,84,120,144,141,108,87,120,198,192,180,144,87,186,210,240,264,180,198,192,285,288,270,288,105,228,300,336,414,252,264,264,396,468,360,288,159,342,465,432,462,324,249",
			"name": "Expansion of (psi(q)^3 / psi(q^3))^2 in powers of q where psi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Cubic AGM theta functions: a(q) (see A004016), b(q) (A005928), c(q) (A005882).",
				"Number 8 and 32 of the 126 eta-quotients listed in Table 1 of Williams 2012. - _Michael Somos_, Nov 10 2018"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A227229/b227229.txt\"\u003eTable of n, a(n) for n = 0..2500\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"K. S. Williams, \u003ca href=\"http://dx.doi.org/10.1142/S1793042112500595\"\u003eFourier series of a class of eta quotients\u003c/a\u003e, Int. J. Number Theory 8 (2012), no. 4, 993-1004."
			],
			"formula": [
				"Expansion of (a(q) + a(q^2))^2 / 4 in powers of q where a() is a cubic AGM theta function.",
				"Expansion of (b(q^2)^2 / b(q))^2 in powers of q where b() is a cubic AGM theta function.",
				"Expansion of (eta(q^3) * eta(q^2)^6 / (eta(q)^3 * eta(q^6)^2))^2 in powers of q.",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (6 t)) = (27/4) (t/i)^2 g(t) where q = exp(2 Pi i t) and g() is the g.f. for A227226.",
				"Convolution square of A107760.",
				"Euler transform of period 6 sequence [6, -6, 4, -6, 6, -4, ...]."
			],
			"example": [
				"G.f. = 1 + 6*q + 15*q^2 + 24*q^3 + 33*q^4 + 36*q^5 + 33*q^6 + 48*q^7 + 69*q^8 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (QPochhammer[ q^3] QPochhammer[ q^2]^6 / (QPochhammer[q]^3 QPochhammer[q^6]^2))^2, {q, 0, n}];",
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 2, 0, q]^6 / EllipticTheta[ 2, 0, q^3]^2/16, {q, 0, 2 n}];",
				"a[ n_] := If[ n \u003c 1, Boole[ n == 0], 3 Sum[ {2, 3/2, 2, 3/2, 2, 0}[[ Mod[d, 6, 1]]] d, {d, Divisors[n]}]];",
				"a[ n_] := If[ n \u003c 1, Boole[ n == 0], 3 Sum[ {2, 1, 2, 1, 2, -8}[[ Mod[d, 6, 1]]] n/d, {d, Divisors[n]}]];"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( (eta(x^2 + A)^6 * eta(x^3 + A) / (eta(x + A)^3 * eta(x^6 + A)^2))^2, n))};",
				"(Sage) A = ModularForms( Gamma0(6), 2, prec=50) . basis(); A[0] + 6*A[1] + 15*A[2];",
				"(MAGMA) A := Basis( ModularForms( Gamma0(6), 2), 50); A[1] + 6*A[2] + 15*A[3];"
			],
			"xref": [
				"Cf. A107760, A227226."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Michael Somos_, Sep 19 2013",
			"references": 2,
			"revision": 19,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2013-09-20T03:17:51-04:00"
		}
	]
}