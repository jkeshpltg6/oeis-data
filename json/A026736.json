{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A026736",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 26736,
			"data": "1,1,1,1,2,1,1,3,3,1,1,5,6,4,1,1,6,11,10,5,1,1,7,22,21,15,6,1,1,8,29,43,36,21,7,1,1,9,37,94,79,57,28,8,1,1,10,46,131,173,136,85,36,9,1,1,11,56,177,398,309,221,121,45,10,1,1,12,67,233,575,707,530,342,166,55,11,1",
			"name": "Triangular array T read by rows: T(n,0) = T(n,n) = 1 for n \u003e= 0; for n \u003e= 2 and 1 \u003c= k \u003c= n-1, T(n,k) = T(n-1,k-1) + T(n-2,k-1) + T(n-1,k) if n is even and k=(n-2)/2, otherwise T(n,k) = T(n-1,k-1) + T(n-1,k).",
			"comment": [
				"T(n, k) is the number of paths from (0, 0) to (n-k, k) in directed graph having vertices (i, j) and edges (i, j)-to-(i+1, j) and (i, j)-to-(i, j+1) for i, j \u003e= 0 and edges (i, i+2)-to-(i+1, i+3) for i \u003e= 0."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A026736/b026736.txt\"\u003eRows n = 0..100 of triangle, flattened\u003c/a\u003e"
			],
			"example": [
				"Triangle begins",
				"  1;",
				"  1,  1;",
				"  1,  2,  1;",
				"  1,  3,  3,   1;",
				"  1,  5,  6,   4,   1;",
				"  1,  6, 11,  10,   5,   1;",
				"  1,  7, 22,  21,  15,   6,   1;",
				"  1,  8, 29,  43,  36,  21,   7,   1;",
				"  1,  9, 37,  94,  79,  57,  28,   8,   1;",
				"  1, 10, 46, 131, 173, 136,  85,  36,   9,   1;",
				"  1, 11, 56, 177, 398, 309, 221, 121,  45,  10,   1;",
				"  1, 12, 67, 233, 575, 707, 530, 342, 166,  55,  11,  1;",
				"  ..."
			],
			"mathematica": [
				"T[_, 0] = T[n_, n_] = 1; T[n_, k_] := T[n, k] = If[EvenQ[n] \u0026\u0026 k == (n-2)/2, T[n-1, k-1] + T[n-2, k-1] + T[n-1, k], T[n-1, k-1] + T[n-1, k]];",
				"Table[T[n, k], {n, 0, 12}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Jul 22 2018 *)"
			],
			"program": [
				"(PARI)",
				"T(n,k) = if(k==n || k==0, 1, if((n%2)==0 \u0026\u0026 k==(n-2)/2, T(n-1, k-1) + T(n-2, k-1) + T(n-1, k), T(n-1, k-1) + T(n-1, k) ));",
				"for(n=0,12, for(k=0,n, print1(T(n,k), \", \"))) \\\\ _G. C. Greubel_, Jul 16 2019",
				"(Sage)",
				"def T(n, k):",
				"    if (k==0 or k==n): return 1",
				"    elif (mod(n,2)==0 and k==(n-2)/2): return T(n-1, k-1) + T(n-2, k-1) + T(n-1, k)",
				"    else: return T(n-1, k-1) + T(n-1, k)",
				"[[T(n, k) for k in (0..n)] for n in (0..12)] # _G. C. Greubel_, Jul 16 2019",
				"(GAP)",
				"T:= function(n,k)",
				"    if k=0 or k=n then return 1;",
				"    elif (n mod 2)=0 and k=Int((n-2)/2) then return T(n-1, k-1) + T(n-2, k-1) + T(n-1, k);",
				"    else return T(n-1, k-1) + T(n-1, k);",
				"    fi;",
				"  end;",
				"Flat(List([0..12], n-\u003e List([0..n], k-\u003e T(n,k) ))); # _G. C. Greubel_, Jul 16 2019"
			],
			"xref": [
				"Row sums give A026743.",
				"T(2n,n) gives A026737(n) or A111279(n+1)."
			],
			"keyword": "nonn,tabl,walk",
			"offset": "0,5",
			"author": "_Clark Kimberling_",
			"ext": [
				"Offset corrected by _Alois P. Heinz_, Jul 23 2018"
			],
			"references": 31,
			"revision": 30,
			"time": "2019-07-16T21:58:39-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}