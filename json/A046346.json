{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A046346",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 46346,
			"data": "4,16,27,30,60,70,72,84,105,150,180,220,231,240,256,286,288,308,378,440,450,476,528,540,560,576,588,594,624,627,646,648,650,728,800,805,840,884,897,900,945,960,1008,1040,1056,1080,1100,1122,1134,1160,1170,1248",
			"name": "Composite numbers that are divisible by the sum of their prime factors (counted with multiplicity).",
			"comment": [
				"If m is in the sequence and d|m, then m^d is also a term. Note that this sequence contains all infinite subsequences of the form p^(p^k) for k\u003e0, where p is a prime. - _Amiram Eldar_ and _Thomas Ordowski_, Feb 06 2019",
				"If one selects some composite k, k \u003e= 8, and decomposes (k - sopfr(k)) into an additive partition having only prime parts, then those parts, when taken as a product with k, yield an element of this sequence. - _Christopher Hohl_, Jul 30 2019"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A046346/b046346.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"K. Alladi and P. Erdős, \u003ca href=\"http://projecteuclid.org/euclid.pjm/1102811427\"\u003eOn an additive arithmetic function\u003c/a\u003e, Pacific J. Math., Volume 71, Number 2 (1977), 275-294. See \"special numbers\" on page 287."
			],
			"example": [
				"a(38) = 884 = 2 * 2 * 13 * 17 -\u003e 2 + 2 + 13 + 17 = 34 so 884 / 34 = 26."
			],
			"maple": [
				"isA046346 := proc(n)",
				"    if isprime(n) then",
				"        false;",
				"    elif modp(n,A001414(n)) = 0 then",
				"        true;",
				"    else",
				"        false;",
				"    end if;",
				"end proc:",
				"for n from 2 to 1000 do",
				"    if isA046346(n) then",
				"        printf(\"%d,\",n);",
				"    end if;",
				"end do: # _R. J. Mathar_, Jan 12 2016"
			],
			"mathematica": [
				"Select[Range[2,1170],!PrimeQ[#]\u0026\u0026IntegerQ[#/Total[Times@@@FactorInteger[#]]]\u0026] (* _Jayanta Basu_, Jun 02 2013 *)"
			],
			"program": [
				"(PARI) sopfr(n) = {my(f=factor(n)); sum(k=1, #f~, f[k,1]*f[k,2]);}",
				"lista(nn) = forcomposite(n=2, nn, if (! (n % sopfr(n)), print1(n, \", \"));); \\\\ _Michel Marcus_, Jan 06 2016",
				"(MATLAB) m=1;for u=2:1200 if and(isprime(u)==0,mod(u,sum(factor(u)))==0); sol(m)=u; m=m+1; end; end;sol % _Marius A. Burtea_, Jul 31 2019",
				"(MAGMA) [k:k in [2..1200]| not IsPrime(k) and  k mod (\u0026+[m[1]*m[2]: m in Factorization(k)]) eq 0]; // _Marius A. Burtea_, Jul 31 2019",
				"(Python)",
				"from sympy import factorint",
				"def ok(n):",
				"  f = factorint(n)",
				"  return sum(f[p] for p in f) \u003e 1 and n % sum(p*f[p] for p in f) == 0",
				"print(list(filter(ok, range(1250)))) # _Michael S. Branicky_, Apr 16 2021"
			],
			"xref": [
				"Cf. A036844, A046347, A046348, A001414.",
				"Contains A071142."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Patrick De Geest_, Jun 15 1998",
			"ext": [
				"Description corrected by Robert A. Stump (bee_ess107(AT)yahoo.com), Jan 09 2002"
			],
			"references": 16,
			"revision": 45,
			"time": "2021-04-16T11:57:48-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}