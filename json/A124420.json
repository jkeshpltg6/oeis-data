{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A124420",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 124420,
			"data": "1,0,1,1,1,1,3,1,5,8,2,9,26,15,2,52,101,45,5,130,385,287,70,5,855,1889,1143,238,15,2707,8295,7320,2475,335,15,19921,48382,35805,10540,1275,52,75771,240534,240082,100940,19505,1686,52,614866,1609551,1379753,512710",
			"name": "Triangle read by rows: T(n,k) is the number of partitions of the set {1,2,...,n}, having exactly k blocks consisting only odd entries (0\u003c=k\u003c=ceiling(n/2)).",
			"comment": [
				"Row n has 1 + ceiling(n/2) terms. Row sums are the Bell numbers (A000110). T(2n,n) = T(2n+1,n+1) = A000110(n) (the Bell numbers). T(n,0) = A124421(n)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A124420/b124420.txt\"\u003eRows n = 0..150, flattened\u003c/a\u003e"
			],
			"formula": [
				"The generating polynomial of row n is P[n](t)=Q[n](t,1,1), where the polynomials Q[n]=Q[n](t,s,x) are defined by Q[0]=1; Q[n]=t*dQ[n-1]/dt + x*dQ[n-1]/ds + x*dQ[n-1]/dx + t*Q[n-1] if n is odd and Q[n]=x*dQ[n-1]/dt + s*dQ[n-1]/ds + x*dQ[n-1]/dx + s*Q[n-1] if n is even."
			],
			"example": [
				"T(4,1) = 8 because we have 13|24, 1|234, 124|3, 14|2|3, 1|2|34, 13|2|4, 1|23|4 and 12|3|4.",
				"Triangle starts:",
				"   1;",
				"   0,   1;",
				"   1,   1;",
				"   1,   3,   1;",
				"   5,   8,   2;",
				"   9,  26,  15,   2;",
				"  52, 101,  45,   5;"
			],
			"maple": [
				"Q[0]:=1: for n from 1 to 13 do if n mod 2 = 1 then Q[n]:=expand(t*diff(Q[n-1],t)+x*diff(Q[n-1],s)+x*diff(Q[n-1],x)+t*Q[n-1]) else Q[n]:=expand(x*diff(Q[n-1],t)+s*diff(Q[n-1],s)+x*diff(Q[n-1],x)+s*Q[n-1]) fi od: for n from 0 to 13 do P[n]:=sort(subs({s=1,x=1},Q[n])) od: for n from 0 to 13 do seq(coeff(P[n],t,j),j=0..ceil(n/2)) od; # yields sequence in triangular form",
				"# second Maple program:",
				"T:= proc(n, k) local g, u; g:= floor(n/2); u:=ceil(n/2);",
				"      add(Stirling2(i, k)*binomial(u, i)*",
				"      add(Stirling2(g, j)*j^(u-i), j=0..g), i=k..u)",
				"    end:",
				"seq(seq(T(n,k), k=0..ceil(n/2)), n=0..15);  # _Alois P. Heinz_, Oct 23 2013"
			],
			"mathematica": [
				"T[n_, k_] := Module[{g = Floor[n/2], u = Ceiling[n/2]},",
				"    Sum[StirlingS2[i, k]*Binomial[u, i]*",
				"    Sum[StirlingS2[g, j]*If[u == i, 1, j^(u - i)], {j, 0, g}], {i, k, u}]];",
				"Table[Table[T[n, k], {k, 0, Ceiling[n/2]}], {n, 0, 15}] // Flatten (* _Jean-François Alcover_, May 20 2015, after _Alois P. Heinz_, updated Jan 01 2021 *)"
			],
			"xref": [
				"Cf. A000110, A124418, A124419, A124421, A124422, A124423."
			],
			"keyword": "nonn,tabf",
			"offset": "0,7",
			"author": "_Emeric Deutsch_, Oct 31 2006",
			"references": 8,
			"revision": 20,
			"time": "2021-01-01T11:58:44-05:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}