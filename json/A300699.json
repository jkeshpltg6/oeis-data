{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A300699",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 300699,
			"data": "1,1,1,1,2,2,1,1,3,6,6,6,3,1,1,4,12,18,28,24,28,18,12,4,1,1,5,20,40,80,95,150,150,150,150,95,80,40,20,5,1,1,6,30,75,180,270,506,660,840,1080,1035,1035,1080,840,660,506,270,180,75,30,6,1,1,7,42,126,350,630,1337,2107,3192,4760",
			"name": "Irregular triangle read by rows: T(n, k) = number of vertices with rank k in concertina n-cube.",
			"comment": [
				"n-place formulas in first-order logic like Ax Ey P(x, y) ordered by implication form a graded poset, and its Hasse diagram is the concertina n-cube.",
				"Sum of row n is A000629(n), the number of vertices of a concertina n-cube.",
				"The rows are palindromic. Their lengths are the central polygonal numbers A000124 = 1, 2, 4, 7, 11, 16, 22, ... That means after row 0 rows of even and odd length follow each other in pairs.",
				"The central values are 1, (1), (2), 6, 24, (150), (1035), 9030, 88760, (1002204), ... (Values next to the center in rows of even length are in parentheses.)",
				"Maximal values are 1, 1, 2, 6, 28, 150, 1080, 9030, 88760, 1002204, ...",
				"A300695 is a triangle of the same shape that shows the number of ranks in cocoon concertina hypercubes."
			],
			"link": [
				"Tilman Piesk, \u003ca href=\"/A300699/b300699.txt\"\u003eRows 0..9, flattened\u003c/a\u003e",
				"Tilman Piesk, \u003ca href=\"/A300699/a300699.txt\"\u003eRows 0..9\u003c/a\u003e",
				"Tilman Piesk, \u003ca href=\"https://en.wikiversity.org/wiki/Formulas_in_predicate_logic\"\u003eFormulas in predicate logic\u003c/a\u003e (Wikiversity)",
				"Tilman Piesk, Concertina cube Hasse diagram \u003ca href=\"https://commons.wikimedia.org/wiki/File:Concertina_cube_Hasse_diagram.png\"\u003ewith labels\u003c/a\u003e and \u003ca href=\"https://commons.wikimedia.org/wiki/File:Ranks_in_concertina_cube.svg\"\u003ewith highlighted ranks\u003c/a\u003e",
				"Tilman Piesk, \u003ca href=\"https://github.com/watchduck/concertina_hypercubes/tree/master/computed_results/coordinates\"\u003eLists of vertices ordered by rank\u003c/a\u003e for n=2..6",
				"Tilman Piesk, \u003ca href=\"https://github.com/watchduck/concertina_hypercubes/blob/master/ranks_convex.py\"\u003ePython code used to generate the sequence\u003c/a\u003e"
			],
			"example": [
				"First rows of the triangle:",
				"    k   0   1   2   3   4   5    6    7    8    9   10  11  12  13  14  15",
				"  n",
				"  0     1",
				"  1     1   1",
				"  2     1   2   2   1",
				"  3     1   3   6   6   6   3    1",
				"  4     1   4  12  18  28  24   28   18   12    4    1",
				"  5     1   5  20  40  80  95  150  150  150  150   95  80  40  20   5   1",
				"  6     1   6  30  75 180 270  506  660  840 1080 1035 ...",
				"The ranks of vertices of a concertina cube (n=3) can be seen in the linked Hasse diagrams. T(3, 4) = 6, so there are 6 vertices with rank 4.",
				"Ey Ez Ax P(x, y, z) implies Ey Ax Ez P(x, y, z), and their ranks are 3 and 4. As the difference in rank is 1, this implication is an edge in the Hasse diagram."
			],
			"xref": [
				"Cf. A000124, A000629, A300695."
			],
			"keyword": "nonn,tabf,more",
			"offset": "0,5",
			"author": "_Tilman Piesk_, Mar 11 2018",
			"references": 2,
			"revision": 25,
			"time": "2018-04-03T10:14:44-04:00",
			"created": "2018-04-03T10:14:44-04:00"
		}
	]
}