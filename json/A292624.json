{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A292624",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 292624,
			"data": "3,12,12,36,48,24,24,60,120,42,108,54,42,78,198,78,156,66,96,234,42,216,156,60,48,96,156,144,90,78,192,186,102,210,108,180,144,138,384,156,276,102,396,36,138,246,174,342,216,120,114,630,48,300",
			"name": "Number of solutions to 4/p = 1/x + 1/y + 1/z in positive integers, where p is the n-th prime.",
			"comment": [
				"Corrected version of A192788."
			],
			"reference": [
				"For references and links see A192787."
			],
			"link": [
				"Hugo Pfoertner, \u003ca href=\"/A292624/b292624.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Christian Elsholtz, Terence Tao, \u003ca href=\"https://arxiv.org/abs/1107.1010\"\u003eCounting the number of solutions to the Erdos-Straus equation on unit fractions\u003c/a\u003e, arXiv:1107.1010 [math.NT], 2011-2015.",
				"Christian Elsholtz, Terence Tao, \u003ca href=\"https://doi.org/10.1017/S1446788712000468\"\u003eCounting the number of solutions to the Erdos-Straus equation on unit fractions\u003c/a\u003e, Journal of the Australian Mathematical Society, 94(1), 50-105, 2013. doi:10.1017/S1446788712000468."
			],
			"formula": [
				"a(n) = A292581(A000040(n))."
			],
			"example": [
				"a(3) = 12 because 4/(3rd prime) = 4/5 can be expressed in the following 12 ways:",
				"  4/5 =  1/2  + 1/4  + 1/20",
				"  4/5 =  1/2  + 1/5  + 1/10",
				"  4/5 =  1/2  + 1/10 + 1/5",
				"  4/5 =  1/2  + 1/20 + 1/4",
				"  4/5 =  1/4  + 1/2  + 1/20",
				"  4/5 =  1/4  + 1/20 + 1/2",
				"  4/5 =  1/5  + 1/2  + 1/10",
				"  4/5 =  1/5  + 1/10 + 1/2",
				"  4/5 =  1/10 + 1/2  + 1/5",
				"  4/5 =  1/10 + 1/5  + 1/2",
				"  4/5 =  1/20 + 1/2  + 1/4",
				"  4/5 =  1/20 + 1/4  + 1/2"
			],
			"mathematica": [
				"checkmult[a_, b_, c_] := If[Denominator[c] == 1, If[a == b \u0026\u0026 a == c \u0026\u0026 b == c, Return[1], If[a != b \u0026\u0026 a != c \u0026\u0026 b != c, Return[6], Return[3]]], Return[0]];",
				"a292581[n_] := Module[{t, t1, s, a, b, c, q = Quotient}, t = 4/n; s = 0; For[a = q[1, t] + 1, a \u003c= q[3, t], a++, t1 = t - 1/a; For[b = Max[q[1, t1] + 1, a], b \u003c= q[2, t1], b++, c = 1/(t1 - 1/b); s += checkmult[a, b, c]]]; Return[s]];",
				"Reap[For[n = 1, n \u003c= 54, n++, Print[n, \" \", an = a292581[Prime[n]]]; Sow[an]]][[2, 1]] (* _Jean-François Alcover_, Dec 02 2018, adapted from PARI *)"
			],
			"program": [
				"(PARI)",
				"checkmult (a,b,c) =",
				"{",
				"  if(denominator(c)==1,",
				"     if(a==b \u0026\u0026 a==c \u0026\u0026 b==c,",
				"        return(1),",
				"        if(a!=b \u0026\u0026 a!=c \u0026\u0026 b!=c,",
				"           return(6),",
				"           return(3)",
				"          )",
				"       ),",
				"     return(0)",
				"     )",
				"}",
				"a292624(n) =",
				"{",
				"  local(t, t1, s, a, b, c);",
				"  t = 4/prime(n);",
				"  s = 0;",
				"  for (a=1\\t+1, 3\\t,",
				"     t1=t-1/a;",
				"     for (b=max(1\\t1+1,a), 2\\t1,",
				"          c=1/(t1-1/b);",
				"          s+=checkmult(a,b,c);",
				"         )",
				"      );",
				"  return(s);",
				"}",
				"for (n=1,54,print1(a292624(n),\", \"))"
			],
			"xref": [
				"Cf. A073101, A192787, A192789, A292581."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Hugo Pfoertner_, Sep 20 2017",
			"references": 6,
			"revision": 21,
			"time": "2018-12-02T09:06:17-05:00",
			"created": "2017-09-20T17:16:12-04:00"
		}
	]
}