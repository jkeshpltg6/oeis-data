{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A211390",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 211390,
			"data": "2,4,4,6,6,8,8,11,10,12,12,16,14,16,16,20,18,20,20,24,22,24,24,28,26,28,28,32,30,32,32,36,34,36,36,40,38,40,40,44,42,44,44,48,46,48,48,52,50,52,52,56,54,56,56,60,58,60,60,64,62,64,64,68,66,68,68",
			"name": "The minimum cardinality of an n-qubit unextendible product basis.",
			"comment": [
				"An unextendible product basis (UPB) is a set of mutually orthogonal product states such that there is no product state orthogonal to every member of the set. An n-qubit UPB is a UPB on the space C^2 tensored with itself n times, where C is the field of complex numbers."
			],
			"link": [
				"D. P. DiVincenzo, T. Mor, P. W. Shor, J. A. Smolin, and B. M. Terhal, \u003ca href=\"https://arxiv.org/abs/quant-ph/9908070\"\u003eUnextendible product bases, uncompletable product bases and bound entanglement\u003c/a\u003e, arXiv:quant-ph/9908070, 1999-2000; Commun. Math. Phys., 238:379-410, 2003.",
				"K. Feng, \u003ca href=\"https://doi.org/10.1016/j.dam.2005.10.011\"\u003eUnextendible product bases and 1-factorization of complete graphs\u003c/a\u003e, Discrete Appl. Math., 154:942-949, 2006.",
				"N. Johnston, \u003ca href=\"http://arxiv.org/abs/1302.1604\"\u003eThe minimum size of qubit unextendible product bases\u003c/a\u003e. In Proceedings of the 8th Conference on the Theory of Quantum Computation, Communication and Cryptography (TQC), 2013. doi: 10.4230/LIPIcs.TQC.2013.93",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,0,0,1,-1)."
			],
			"formula": [
				"a(n) = n + 1 if n is odd.",
				"a(n) = n + 2 if n = 2 (mod 4) or if n = 4.",
				"a(n) = n + 3 if n = 8.",
				"a(n) = n + 4 otherwise (i.e., if n \u003e= 12 and n = 0 (mod 4)).",
				"G.f.: -x*(x^12-x^11+x^8-x^7+2*x^4-2*x^3-2*x-2) / ((x-1)^2*(x+1)*(x^2+1)). - _Colin Barker_, Feb 16 2013"
			],
			"example": [
				"a(2) = 4 because there is no nontrivial UPB on two qubits -- any UPB spans the entire 2^2 = 4-dimensional space.",
				"a(3) = 4 because there is a 4-state UPB in 3-qubit space. If we use \"ket\" notation from quantum mechanics, then one such UPB is: |0\u003e|0\u003e|0\u003e, |+\u003e|1\u003e|-\u003e, |1\u003e|-\u003e|+\u003e, |-\u003e|+\u003e|1\u003e. This is the \"shifts\" UPB from the DiVincenzo et al. paper."
			],
			"maple": [
				"a := proc(n) if(n mod 2 = 1)then return n + 1;elif(n = 4 or n mod 4 = 2)then return n + 2;elif(n = 8)then return 11;else return n + 4; fi: end: seq(a(n), n=1..67);"
			],
			"mathematica": [
				"Join[{2, 4, 4, 6, 6, 8, 8, 11}, LinearRecurrence[{1, 0, 0, 1, -1}, {10, 12, 12, 16, 14}, 60]] (* _Jean-François Alcover_, Nov 29 2017 *)"
			],
			"xref": [
				"Cf. A229913."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_Nathaniel Johnston_, Feb 07 2013",
			"references": 1,
			"revision": 45,
			"time": "2017-11-30T00:06:08-05:00",
			"created": "2013-02-07T21:02:05-05:00"
		}
	]
}