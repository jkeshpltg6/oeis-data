{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A338162",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 338162,
			"data": "1,2,3,2,4,4,2,3,6,3,7,6,5,6,7,3,8,9,5,6,8,4,8,7,4,8,11,3,7,8,6,8,13,6,6,8,6,9,11,8,10,12,7,7,12,5,14,14,7,7,13,5,13,13,5,8,13,8,10,10,7,13,10,6,9,14,9,10,15,7,10",
			"name": "Number of ways to write 4*n + 1 as x^2 + y^2 + z^2 + w^2 with x^2 + 7*y^2 = 2^k for some k = 0,1,2,..., where x, y, z, w are nonnegative integers with z \u003c= w.",
			"comment": [
				"Conjecture: a(n) \u003e 0 for all n \u003e= 0. Moreover, if m \u003e 1 has the form 2^a*(2*b+1), and either a is positive and even, or b is even,  then m can be written as x^2 + y^2 + z^2 + w^2 with x^2 + 7*y^2 = 2^k for some positive integer k, where x, y, z, w are nonnegative integers.",
				"We have verified the latter assertion in the conjecture for m up to 4*10^8."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A338162/b338162.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2016.11.008\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, J. Number Theory 175(2017), 167-190. See also \u003ca href=\"http://arxiv.org/abs/1604.06723\"\u003earXiv:1604.06723 [math.NT]\u003c/a\u003e.",
				"Zhi-Wei Sun, \u003ca href=\"https://doi.org/10.1142/S1793042119501045\"\u003eRestricted sums of four squares\u003c/a\u003e, Int. J. Number Theory 15(2019), 1863-1893.  See also \u003ca href=\"http://arxiv.org/abs/1701.05868\"\u003earXiv:1701.05868 [math.NT]\u003c/a\u003e.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/2010.05775\"\u003eSums of four squares with certain restrictions\u003c/a\u003e, arXiv:2010.05775 [math.NT], 2020."
			],
			"example": [
				"a(0) = 1, and 4*0 + 1 = 1^2 + 0^2 + 0^2 +0^2 with 1^2 + 7*0^2 = 2^0.",
				"a(25) = 2, and 25 = 2^2 + 2^2 + 1^2 + 4^2 = 4^2 + 0^2 + 0^2 + 3^2",
				"with 2^2 + 7*2^2 = 2^5 and 4^2 + 7*0^2 = 2^4."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]];",
				"PQ[n_]:=PQ[n]=IntegerQ[Log[2,n]];",
				"tab={};Do[r=0;Do[If[SQ[4n+1-x^2-y^2-z^2]\u0026\u0026PQ[x^2+7y^2],r=r+1],{x,1,Sqrt[4n+1]},{y,0,Sqrt[4n+1-x^2]},{z,0,Sqrt[(4n+1-x^2-y^2)/2]}];tab=Append[tab,r],{n,0,70}];tab"
			],
			"xref": [
				"Cf. A000079, A000118, A000290, A020670, A337082, A338094, A338095, A338096, A338139."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Zhi-Wei Sun_, Oct 14 2020",
			"references": 2,
			"revision": 15,
			"time": "2020-10-15T03:40:48-04:00",
			"created": "2020-10-14T23:18:30-04:00"
		}
	]
}