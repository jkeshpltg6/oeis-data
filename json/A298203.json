{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A298203",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 298203,
			"data": "1,-2,0,0,3,0,-4,0,4,0,-4,0,7,0,-12,0,13,0,-16,0,22,0,-28,0,38,0,-44,0,55,0,-72,0,83,0,-104,0,129,0,-156,0,187,0,-220,0,273,0,-328,0,384,0,-452,0,539,0,-652,0,757,0,-880,0,1041,0,-1220,0,1428,0",
			"name": "Expansion of (1/q) * phi(-q) * phi(q^5) / (f(-q^4) * f(-q^20)) in powers of q where phi(), f() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A298203/b298203.txt\"\u003eTable of n, a(n) for n = -1..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of (1/q) * chi(q) * chi(-q)^3 * chi(q^5)^3 * chi(-q^5) in powers of q where chi() is a Ramanujan theta function.",
				"Expansion of eta(q)^2 * eta(q^10)^5 / (eta(q^2) * eta(q^4)* eta(q^5)^2 * eta(q^20)^3) in powers of q.",
				"Euler transform of period 20 sequence [-2, -1, -2, 0, 0, -1, -2, 0, -2, -4, -2, 0, -2, -1, 0, 0, -2, -1, -2, 0, ...].",
				"a(2*n) = 0 except n=0. a(2*n + 1) = A058559(n) for all n in Z."
			],
			"example": [
				"G.f. = q^-1 - 2 + 3*q^3 - 4*q^5 + 4*q^7 - 4*q^9 + 7*q^11 - 12*q^13 + 13*q^15 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ 1/q EllipticTheta[ 4, 0, q] EllipticTheta[ 3, 0, q^5] / (QPochhammer[ q^4] QPochhammer[ q^20]), {q, 0, n}];",
				"a[ n_] := SeriesCoefficient[ 1/q QPochhammer[ -q, q^2] QPochhammer[ q, q^2]^3 QPochhammer[ -q^5, q^10]^3 QPochhammer[ q^5, q^10], {q, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c-1, 0, n++; A = x * O(x^n); polcoeff( eta(x + A)^2 * eta(x^10 + A)^5 / (eta(x^2 + A) * eta(x^4 + A)* eta(x^5 + A)^2 * eta(x^20 + A)^3), n))};"
			],
			"xref": [
				"Cf. A058559."
			],
			"keyword": "sign",
			"offset": "-1,2",
			"author": "_Michael Somos_, Jan 14 2018",
			"references": 2,
			"revision": 12,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2018-01-15T03:27:39-05:00"
		}
	]
}