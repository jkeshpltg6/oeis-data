{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A094646",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 94646,
			"data": "1,-2,1,2,-3,1,0,2,-3,1,0,2,-1,-2,1,0,4,0,-5,0,1,0,12,4,-15,-5,3,1,0,48,28,-56,-35,7,7,1,0,240,188,-252,-231,0,42,12,1,0,1440,1368,-1324,-1638,-231,252,114,18,1,0,10080,11016,-7900,-12790,-3255,1533,1050,240,25,1",
			"name": "Generalized Stirling number triangle of first kind.",
			"comment": [
				"Triangle T(n,k), 0 \u003c= k \u003c= n, read by rows, given by [ -2, 1, -1, 2, 0, 3, 1, 4, 2, 5, ...] DELTA [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, ...] where DELTA is the operator defined in A084938. - _Philippe Deléham_, Aug 23 2006",
				"From _Wolfdieter Lang_, Jun 23 2011: (Start)",
				"The row polynomials s(n,x):=Sum_{k=0..n} T(n,k)*x^k satisfy risefac(x-2,n)=s(n,x), with the rising factorials risefac(x-2,n):=Product_{j=0..n-1} (x-2+j), n \u003e= 1, risefac(x-2,0)=1. Compare this with the formula risefac(x,n)=|S1|(n,x), with the row polynomials |S1|(n,x) of A132393 (unsigned Stirling1).",
				"This is the third triangle of an a-family of Sheffer arrays, call them |S1|(a), with e.g.f. of the row polynomials |S1|(a;x;z) = ((1-z)^a)*exp(-x*log(1-z)). In the notation showing the column e.g.f.s this is Sheffer ((1-z)^a,-log(1-z)). In the umbral notation (see the Roman reference, given under A094645) this is called Sheffer for (exp(a*t),1-exp(-t)). For a=0 this becomes the unsigned Stirling1 triangle |S1|(0) = A132393 with row polynomials |S1|(0;n,x) =: s1(n,x).",
				"E.g.f. column number k (with leading zeros): ((1-x)^a)*((-log(1-x))^k)/k!, k \u003e= 0.",
				"E.g.f. for row sums is (1-x)^(a-1), and the e.g.f. for the alternating row sums is (1-x)^(a+1).",
				"Row polynomial recurrence:",
				"  |S1|(a;n,x)=(x+(n-1-a))*|S1|(a;n-1,m), |S1|(a;0,x)=1.",
				"Meixner identity (see the reference under A060338):",
				"  |S1|(a;n,x) - |S1|(a;n,x-1) = n*|S1|(a;n-1,x), n \u003e= 1,",
				"Also (from the corollary 3.7.2 on p. 50 of the Roman reference): |S1|(a;n,x) = (x-a)*|S1|(a;n-1,x+1), n \u003e= 1.",
				"Recurrence: |S1|(a;n,k) = |S1|(a;n-1,k-1) + (n-(a+1))*|S1|(a;n-1,k); |S1|(a;n,k)=0 if n \u003c m, |S1|(a;n,-1)=0, |S1|(a;0,0)=1.",
				"Connection to |Stirling1|=|S1|(0):",
				"  |S1|(a;n,k) = Sum_{p=0..a} |S1|(a;a,p)*abs(Stirling1(n-a,k-p)), n \u003e= a.",
				"The exponential convolution identity is",
				"  |S1|(a;n,x+y) = Sum_{k=0..n} binomial(n,k)*|S1|(a;k,y)*s1(n-k,x), n \u003e= 0, with symmetry x \u003c-\u003e y.",
				"The Sheffer a- and z-sequences are (see the W. Lang link under A006232): Sha(a;n)=A164555(n)/A027642(n) (independent of a) with e.g.f. x/(1-exp(-x)), and the z-sequence has e.g.f. (exp(a*x)-1)/(exp(-x)-1).",
				"The inverse Sheffer matrix has e.g.f. exp(a*z)*exp(x*(1-exp(-z))), in short notation (exp(a*z),1-exp(-z)),",
				"  (or in umbral notation ((1-t)^a,-log(1-t))).",
				"(End)"
			],
			"formula": [
				"E.g.f.: (1-y)^(2-x).",
				"Sum_{k=0..n} T(n,k)*x^k = A000007(n), A000142(n), A000142(n+1), A001710(n+2), A001715(n+3), A001720(n+4), A001725(n+5), A001730(n+6), A049388(n), A049389(n), A049398(n), A051431(n) for x = 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 respectively. - _Philippe Deléham_, Nov 13 2007",
				"If we define f(n,i,a) = Sum_{k=0..n-i} binomial(n,k)*Stirling1(n-k,i)*Product_{j=0..k-1} (-a-j), then |T(n,i)| = |f(n,i,-2)|, for n=1,2,...; i=0..n. - _Milan Janjic_, Dec 21 2008",
				"From _Wolfdieter Lang_, Jun 23 2011: (Start)",
				"risefac(x-2,n) = Sum_{k=0..n} T(n,k)*x^k, n \u003e= 0, with the rising factorials (see a comment above).",
				"Recurrence: T(n,k) = T(n-1,k-1) + (n-3)*T(n-1,k); T(n,k)=0 if n \u003c m, T(n,-1)=0, T(0,0)=1.",
				"T(n,k) = 2*abs(S1(n-2,k)) - 3*abs(S1(n-2,k-1)) + abs(S1(n-2,k-2)), n \u003e= 2, with S1(n,k) = Stirling1(n,k) = A048994(n,k).",
				"E.g.f. column number k (with leading zeros):",
				"((1-x)^2)*((-log(1-x))^k)/k!, k \u003e= 0.",
				"E.g.f. for row sums is 1-x, i.e., [1,-1,0,0,...],",
				"  and the e.g.f. for the alternating row sums is (1-x)^3. i.e., [1,-3,3,1,0,0,...]. (End)"
			],
			"example": [
				"Triangle begins",
				"   1;",
				"  -2,  1;",
				"   2, -3,  1;",
				"   0,  2, -3,  1;",
				"   0,  2, -1, -2,  1;",
				"   0,  4,  0, -5,  0,  1;",
				"   ...",
				"risefac(x-2,3) = (x-2)*(x-1)*x = 2*x-3*x^2+x^3.",
				"-1 = T(4,2) = T(3,1) + 1*T(3,2) =  2 + (-3).",
				"T(4,3) = 2*abs(S1(2,3)) - 3*abs(S1(2,2)) + 1*abs(S1(2,1)) = 2*0 - 3*1 + 1*1 = -2."
			],
			"maple": [
				"A094646_row := n -\u003e seq((-1)^(n-k)*coeff(expand(pochhammer(x-n+3, n)), x, k), k=0..n): seq(print(A094646_row(n)), n = 0..6); # _Peter Luschny_, May 16 2013"
			],
			"mathematica": [
				"Flatten[ Table[ CoefficientList[ Pochhammer[x-2, n], x], {n, 0, 10}]] (* _Jean-François Alcover_, Sep 26 2011 *)"
			],
			"xref": [
				"Cf. A049444, A049458, A094645."
			],
			"keyword": "easy,sign,tabl",
			"offset": "0,2",
			"author": "_Vladeta Jovovic_, May 17 2004",
			"references": 4,
			"revision": 26,
			"time": "2020-01-22T06:03:45-05:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}