{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A176447",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 176447,
			"data": "0,1,-1,3,-2,5,-3,7,-4,9,-5,11,-6,13,-7,15,-8,17,-9,19,-10,21,-11,23,-12,25,-13,27,-14,29,-15,31,-16,33,-17,35,-18,37,-19,39,-20,41,-21,43,-22,45,-23,47,-24,49,-25,51,-26,53,-27,55,-28,57,-29,59,-30,61,-31,63,-32,65,-33,67,-34,69,-35",
			"name": "a(2n) = -n, a(2n+1) = 2n+1.",
			"comment": [
				"There is more complicated way of defining the sequence: consider the sequence of modified Bernoulli numbers EVB(n) = A176327(n)/A176289(n) and its inverse binomial transform IEVB(n) = A176328(n)/A176591(n). Then a(n) is the numerator of the difference EVB(n)-IEVB(n). The denominator of the difference is 1 if n=0, else A040001(n-1).",
				"A particularity of EVB(n) is: its (forward) binomial transform is 1, 1, 7/6, 3/2, 59/30,.. = (-1)^n*IEVB(n).",
				"Note that A026741 is related to the Rydberg-Ritz spectrum of the hydrogen atom."
			],
			"link": [
				"R. J. Mathar, \u003ca href=\"/A176447/b176447.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,2,0,-1)."
			],
			"formula": [
				"From _R. J. Mathar_, Dec 01 2010:  (Start)",
				"a(n) = (-1)^n*A026741(n) = n*(1-3*(-1)^n)/4.",
				"G.f.: x*(1-x+x^2) / ( (x-1)^2*(1+x)^2 ).",
				"a(n) = +2*a(n-2) -a(n-4).  (End)",
				"a(n) = -a(-n) for all n in Z. - _Michael Somos_, Jun 11 2013",
				"Euler transform of length 6 sequence [ -1, 3, 1, 0, 0, -1]. - _Michael Somos_, Aug 30 2014",
				"0 = - 1 - a(n) - a(n+1) + a(n+2) + a(n+3) for all n in Z. - _Michael Somos_, Aug 30 2014",
				"0 = 1 + a(n)*(-2 -a(n) + a(n+2)) - 2*a(n+1) - a(n+2) for all n in Z. - _Michael Somos_, Aug 30 2014",
				"a(n) is multiplicative with a(2^e) = -(2^(e-1)) if e\u003e0, a(p^e) = p^e otherwise. - _Michael Somos_, May 04 2015",
				"G.f.: (f(x) - 3 * f(-x)) / 4 where f(x) := x / (1 - x)^2. - _Michael Somos_, May 04 2015",
				"G.f.: x * (1 - x) * (1 - x^6) / ((1 - x^2)^3 * (1 - x^3)). - _Michael Somos_, May 04 2015"
			],
			"example": [
				"G.f. = x - x^2 + 3*x^3 - 2*x^4 + 5*x^5 - 3*x^6 + 7*x^7 - 4*x^8 + 9*x^9 - 5*x^10 + ..."
			],
			"mathematica": [
				"a[n_?EvenQ]:=-(n/2); a[n_?OddQ]:=n; Table[a[n], {n, 100}] (* _Alonso del Arte_, Dec 01 2010 *)",
				"a[ n_] := n / If[ Mod[ n, 2] == 1, 1, -2]; (* _Michael Somos_, Jun 11 2013 *)",
				"CoefficientList[Series[x (1 - x + x^2)/((x - 1)^2*(1 + x)^2), {x, 0, 70}], x]  (* _Michael De Vlieger_, Dec 10 2016 *)",
				"LinearRecurrence[{0,2,0,-1},{0,1,-1,3},80] (* _Harvey P. Dale_, Nov 01 2017 *)"
			],
			"program": [
				"(MAGMA) [n*(1-3*(-1)^n)/4: n in [0..60]]; // _Vincenzo Librandi_, Aug 04 2011",
				"(PARI) {a(n) = n / if( n%2, 1, -2)}; /* _Michael Somos_, Jun 11 2013 */"
			],
			"xref": [
				"Cf. A026741."
			],
			"keyword": "sign,easy,mult",
			"offset": "0,4",
			"author": "_Paul Curtz_, Apr 18 2010",
			"references": 2,
			"revision": 40,
			"time": "2017-11-01T11:36:25-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}