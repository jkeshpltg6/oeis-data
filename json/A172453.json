{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A172453",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 172453,
			"data": "1,1,1,1,1,1,1,2,2,1,1,2,4,2,1,1,3,6,6,3,1,1,4,12,12,12,4,1,1,4,16,24,24,16,4,1,1,4,16,32,48,32,16,4,1,1,5,20,40,80,80,40,20,5,1,1,6,30,60,120,160,120,60,30,6,1",
			"name": "Triangle T(n, k) = round( A172452(n)/(A172452(k)*A172452(n-k)) ), read by rows.",
			"comment": [
				"The original definition of this sequence did not produce an integer valued triangular sequence. The application of the \"round\" function was the method chosen to formulate an integer sequence. - _G. C. Greubel_, Apr 27 2021"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A172453/b172453.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = round( A172452(n)/(A172452(k)*A172452(n-k)) ).",
				"T(n, k) = round( c(n)/(c(k)*c(n-k)) ) where c(n) = Product_{j=1..n} A004001(j) with c(0) = 1."
			],
			"example": [
				"Triangle begins as:",
				"  1;",
				"  1, 1;",
				"  1, 1,  1;",
				"  1, 2,  2,  1;",
				"  1, 2,  4,  2,   1;",
				"  1, 3,  6,  6,   3,   1;",
				"  1, 4, 12, 12,  12,   4,   1;",
				"  1, 4, 16, 24,  24,  16,   4,  1;",
				"  1, 4, 16, 32,  48,  32,  16,  4,  1;",
				"  1, 5, 20, 40,  80,  80,  40, 20,  5, 1;",
				"  1, 6, 30, 60, 120, 160, 120, 60, 30, 6, 1;"
			],
			"mathematica": [
				"f[n_]:= f[n]= If[n\u003c3, Fibonacci[n], f[f[n-1]] + f[n-f[n-1]]]; (* f=A004001 *)",
				"c[n_]:= Product[f[j], {j,n}]; (* c=A172452 *)",
				"T[n_, k_]:= Round[c[n]/(c[k]*c[n-k])];",
				"Table[T[n, k], {n,0,10}, {k,0,n}]//Flatten (* modified by _G. C. Greubel_, Apr 27 2021 *)"
			],
			"program": [
				"(Sage)",
				"@CachedFunction",
				"def b(n): return fibonacci(n) if (n\u003c3) else b(b(n-1)) + b(n-b(n-1)) # b=A004001",
				"def c(n): return product(b(j) for j in (1..n)) # c=A172452",
				"def T(n,k): return round(c(n)/(c(k)*c(n-k)))",
				"[[T(n,k) for k in (0..n)] for n in (0..12)] # _G. C. Greubel_, Apr 27 2021"
			],
			"xref": [
				"Cf. A004001, A172452."
			],
			"keyword": "nonn,tabl,easy,less",
			"offset": "0,8",
			"author": "_Roger L. Bagula_, Feb 03 2010",
			"ext": [
				"Definition changed to give integral terms and edited by _G. C. Greubel_, Apr 27 2021"
			],
			"references": 2,
			"revision": 12,
			"time": "2021-04-28T02:09:23-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}