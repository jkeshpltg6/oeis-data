{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A241568",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 241568,
			"data": "0,0,1,1,1,2,2,3,1,3,2,4,2,4,2,5,3,3,5,3,4,2,5,2,4,4,3,4,3,5,3,2,5,4,7,2,6,5,4,4,5,4,3,4,7,4,4,4,5,6,4,3,5,4,3,3,3,3,3,5,6,7,8,2,5,7,6,3,5,7,5,3,4,4,6,3,6,7,4,3",
			"name": "a(n) = |{0 \u003c k \u003c prime(n)/2: k is not only a quadratic nonresidue modulo prime(n) but also a Fibonacci number}|.",
			"comment": [
				"Conjecture: (i) a(n) \u003e 0 for all n \u003e 2. In other words, for any prime p \u003e 3, there is a Fibonacci number among 1, ..., (p-1)/2 which is a quadratic nonresidue modulo p.",
				"(ii) For any n \u003e 2, there is a prime q \u003c prime(n) such that the q-th Fibonacci number is a quadratic nonresidue modulo prime(n).",
				"(iii) For any odd prime p, there is a Lucas number (i.e., a term of A000032) smaller than p which is a quadratic nonresidue modulo p.",
				"We have checked part (i) for all primes p \u003c 3*10^9, part (ii) for n up to 10^8, and part (iii) for the first 10^7 primes.",
				"See also A241604 for a sequence related to part (i) of the conjecture."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A241568/b241568.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Z.-W. Sun, \u003ca href=\"http://arxiv.org/abs/1405.0290\"\u003eNew observations on primitive roots modulo primes\u003c/a\u003e, arXiv preprint arXiv:1405.0290 [math.NT], 2014."
			],
			"example": [
				"a(3) = 1 since F(3) = 2 is a quadratic nonresidue modulo prime(3) = 5, where F(n) denotes the n-th Fibonacci number.",
				"a(4) = 1 since F(4) = 3 is a quadratic nonresidue modulo prime(4) = 7.",
				"a(5) = 1 since F(3) = 2 is a quadratic nonresidue modulo prime(5) = 11.",
				"a(9) = 1 since F(5) = 5 is a quadratic nonresidue modulo prime(9) = 23."
			],
			"mathematica": [
				"f[k_]:=Fibonacci[k]",
				"Do[m=0;Do[If[f[k]\u003ePrime[n]/2,Goto[aa]];If[JacobiSymbol[f[k],Prime[n]]==-1,m=m+1];Continue,{k,2,(Prime[n]+1)/2}]; Label[aa];Print[n,\" \",m];Continue,{n,1,80}]"
			],
			"xref": [
				"Cf. A000040, A000032, A000045, A236966, A239957, A239963, A241492, A241504, A241516, A241604."
			],
			"keyword": "nonn",
			"offset": "1,6",
			"author": "_Zhi-Wei Sun_, Apr 25 2014",
			"references": 6,
			"revision": 29,
			"time": "2019-08-05T02:49:35-04:00",
			"created": "2014-04-26T01:24:44-04:00"
		}
	]
}