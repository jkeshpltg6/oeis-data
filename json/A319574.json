{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A319574",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 319574,
			"data": "1,0,1,0,2,1,0,0,4,1,0,0,4,6,1,0,2,0,12,8,1,0,0,4,8,24,10,1,0,0,8,6,32,40,12,1,0,0,0,24,24,80,60,14,1,0,0,0,24,48,90,160,84,16,1,0,2,4,0,96,112,252,280,112,18,1,0,0,4,12,64,240,312,574,448,144,20,1",
			"name": "A(n, k) = [x^k] JacobiTheta3(x)^n, square array read by descending antidiagonals, A(n, k) for n \u003e= 0 and k \u003e= 0.",
			"comment": [
				"Number of ways of writing k as a sum of n squares."
			],
			"reference": [
				"E. Grosswald, Representations of Integers as Sums of Squares. Springer-Verlag, NY, 1985, p. 121.",
				"G. H. Hardy and E. M. Wright, An Introduction to the Theory of Numbers. 3rd ed., Oxford Univ. Press, 1954.",
				"J. Carlos Moreno and Samuel S. Wagstaff Jr., Sums Of Squares Of Integers, Chapman \u0026 Hall/CRC, (2006)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A319574/b319574.txt\"\u003eDescending antidiagonals n = 0..139, flattened\u003c/a\u003e",
				"L. Carlitz, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9939-1957-0084520-2\"\u003eNote on sums of four and six squares\u003c/a\u003e, Proc. Amer. Math. Soc. 8 (1957), 120-124.",
				"S. H. Chan, \u003ca href=\"http://www.jstor.org/stable/4145192\"\u003eAn elementary proof of Jacobi's six squares theorem\u003c/a\u003e, Amer. Math. Monthly, 111 (2004), 806-811.",
				"H. H. Chan and C. Krattenthaler, \u003ca href=\"http://arXiv.org/abs/math.NT/0407061\"\u003eRecent progress in the study of representations of integers as sums of squares\u003c/a\u003e, arXiv:math/0407061 [math.NT], 2004.",
				"Shi-Chao Chen, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2010.01.011\"\u003eCongruences for rs(n)\u003c/a\u003e, Journal of Number Theory, Volume 130, Issue 9, September 2010, Pages 2028-2032.",
				"S. C. Milne, \u003ca href=\"http://dx.doi.org/10.1023/A:1014865816981\"\u003eInfinite families of exact sums of squares formulas, Jacobi elliptic functions, continued fractions and Schur functions\u003c/a\u003e, Ramanujan J., 6 (2002), 7-149.",
				"\u003ca href=\"/index/Su#ssq\"\u003eIndex entries for sequences related to sums of squares\u003c/a\u003e"
			],
			"example": [
				"[ 0] 1,  0,    0,    0,     0,     0,     0      0,     0,     0, ... A000007",
				"[ 1] 1,  2,    0,    0,     2,     0,     0,     0,     0,     2, ... A000122",
				"[ 2] 1,  4,    4,    0,     4,     8,     0,     0,     4,     4, ... A004018",
				"[ 3] 1,  6,   12,    8,     6,    24,    24,     0,    12,    30, ... A005875",
				"[ 4] 1,  8,   24,   32,    24,    48,    96,    64,    24,   104, ... A000118",
				"[ 5] 1, 10,   40,   80,    90,   112,   240,   320,   200,   250, ... A000132",
				"[ 6] 1, 12,   60,  160,   252,   312,   544,   960,  1020,   876, ... A000141",
				"[ 7] 1, 14,   84,  280,   574,   840,  1288,  2368,  3444,  3542, ... A008451",
				"[ 8] 1, 16,  112,  448,  1136,  2016,  3136,  5504,  9328, 12112, ... A000143",
				"[ 9] 1, 18,  144,  672,  2034,  4320,  7392, 12672, 22608, 34802, ... A008452",
				"[10] 1, 20,  180,  960,  3380,  8424, 16320, 28800, 52020, 88660, ... A000144",
				"   A005843,   v, A130809,  v,  A319576,  v ,   ...      diagonal: A066535",
				"           A046092,    A319575,       A319577,     ..."
			],
			"maple": [
				"A319574row := proc(n, len) series(JacobiTheta3(0, x)^n, x, len+1);",
				"[seq(coeff(%, x, j), j=0..len-1)] end:",
				"seq(print([n], A319574row(n, 10)), n=0..10);"
			],
			"mathematica": [
				"A[n_, k_] := If[n == k == 0, 1, SquaresR[n, k]];",
				"Table[A[n-k, k], {n, 0, 11}, {k, n, 0, -1}] // Flatten (* _Jean-François Alcover_, Nov 03 2018 *)"
			],
			"program": [
				"(Sage)",
				"for n in (0..10):",
				"    Q = DiagonalQuadraticForm(ZZ, [1]*n)",
				"    print(Q.theta_series(10).list())"
			],
			"xref": [
				"Variant starting with row 1 is A122141, transpose of A286815."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Peter Luschny_, Oct 01 2018",
			"references": 18,
			"revision": 28,
			"time": "2020-02-28T08:16:24-05:00",
			"created": "2018-10-02T04:24:28-04:00"
		}
	]
}