{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A059270",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 59270,
			"data": "0,3,15,42,90,165,273,420,612,855,1155,1518,1950,2457,3045,3720,4488,5355,6327,7410,8610,9933,11385,12972,14700,16575,18603,20790,23142,25665,28365,31248,34320,37587,41055,44730,48618,52725,57057,61620",
			"name": "a(n) is both the sum of n+1 consecutive integers and the sum of the n immediately higher consecutive integers.",
			"comment": [
				"Group the non-multiples of n as follows, e.g., for n = 4: (1,2,3), (5,6,7), (9,10,11), (13,14,15), ... Then a(n) is the sum of the members of the n-th group. Or, the sum of (n-1)successive numbers preceding n^2. - _Amarnath Murthy_, Jan 19 2004",
				"Convolution of odds (A005408) and multiples of three (A008585). G.f. is the product of the g.f. of A005408 by the g.f. of A008585. - _Graeme McRae_, Jun 06 2006",
				"Sums of rows of the triangle in A126890. - _Reinhard Zumkeller_, Dec 30 2006",
				"Corresponds to the Wiener indices of C_{2n+1} i.e., the cycle on 2n+1 vertices (n \u003e 0). - _K.V.Iyer_, Mar 16 2009",
				"Also the product of the three numbers from A005843(n) up to A163300(n), divided by 8. - _Juri-Stepan Gerasimov_, Jul 26 2009",
				"Partial sums of A033428. - _Charlie Marion_, Dec 08 2013",
				"For n \u003e 0, sum of multiples of n and (n+1) from 1 to n*(n+1). - _Zak Seidov_, Aug 07 2016"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A059270/b059270.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"M. Janjic and B. Petkovic, \u003ca href=\"http://arxiv.org/abs/1301.4550\"\u003eA Counting Function\u003c/a\u003e, arXiv 1301.4550 [math.CO], 2013",
				"R. Nelsen, \u003ca href=\"http://www.jstor.org/stable/2691505\"\u003eProof Without Words: Consecutive Sums of Consecutive Integers\u003c/a\u003e, Math. Mag., 63 (1990), 25.",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-6,4,-1)."
			],
			"formula": [
				"a(n) = n*(n+1)*(2*n+1)/2.",
				"a(n) = A000330(n)*3 = A006331(n)*3/2 = A055112(n)/2 = A000217(A002378(n)) - A000217(A005563(n-1)) = A000217(A005563(n)) - A000217(A002378(n)).",
				"a(n) = A110449(n+1, n-1) for n \u003e 1.",
				"a(n) = Sum_{k=A000290(n) .. A002378(n)} k = Sum_{k=n^2..n^2+n} k.",
				"a(n) = Sum_{k=n^2+n+1 .. n^2+2*n} k = Sum_{k=A002061(n+1) .. A005563(n)} k.",
				"a(n) = 3*a(n-1) - 3*a(n-2) + a(n-3) + 6 = 4*a(n-1) - 6*a(n-2) + 4*a(n-3) - a(n-4). - _Ant King_, Jan 03 2011",
				"G.f.: 3*x(1+x)/(1-x)^4. - _Ant King_, Jan 03 2011",
				"a(n) = A000578(n+1) - A000326(n+1). - _Ivan N. Ianakiev_, Nov 29 2012",
				"a(n) = A005408(n)*A000217(n) = a(n-1) + 3*A000290(n). -_Ivan N. Ianakiev_, Mar 08 2013",
				"a(n) = n^3 + n^2 + A000217(n). - _Charlie Marion_, Dec 04 2013",
				"From _Ilya Gutkovskiy_, Aug 08 2016: (Start)",
				"E.g.f.: x*(6 + 9*x + 2*x^2)*exp(x)/2.",
				"Sum_{n\u003e=1} 1/a(n) = 2*(3 - 4*log(2)) = 0.4548225555204375246621... (End)",
				"a(n) = Sum_{k=0..2*n} A001318(k). - _Jacob Szlachetka_, Dec 20 2021",
				"a(n) = Sum_{k=0..n} A000326(k) + A005449(k). - _Jacob Szlachetka_, Dec 21 2021"
			],
			"example": [
				"a(5) = 25 + 26 + 27 + 28 + 29 + 30 = 31 + 32 + 33 + 34 + 35 = 165."
			],
			"maple": [
				"A059270 := proc(n) n*(n+1)*(2*n+1)/2 ; end proc: # _R. J. Mathar_, Jul 10 2011"
			],
			"mathematica": [
				"# (#+1)(2#+1)/2 \u0026/@ Range[0,39] (* _Ant King_, Jan 03 2011 *)",
				"CoefficientList[Series[3 x (1 + x)/(x - 1)^4, {x, 0, 39}], x]",
				"LinearRecurrence[{4,-6,4,-1},{0,3,15,42},50] (* _Vincenzo Librandi_, Jun 23 2012 *)"
			],
			"program": [
				"(Sage) [bernoulli_polynomial(n+1,3) for n in range(0, 41)] # _Zerinvary Lajos_, May 17 2009",
				"(MAGMA) I:=[0, 3, 15, 42]; [n le 4 select I[n] else 4*Self(n-1)-6*Self(n-2)+4*Self(n-3)-Self(n-4): n in [1..50]]; // _Vincenzo Librandi_, Jun 23 2012",
				"(PARI) a(n) = n*(n+1)*(2*n+1)/2 \\\\ _Charles R Greathouse IV_, Mar 08 2013"
			],
			"xref": [
				"Cf. A059255 for analog for sum of squares.",
				"Cf. A222716 for the analogous sum of triangular numbers.",
				"Cf. A234319 for nonexistence of analogs for sums of n-th powers, n \u003e 2. - _Jonathan Sondow_, Apr 23 2014",
				"Cf. A098737 (first subdiagonal).",
				"Bisection of A109900."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Henry Bottomley_, Jan 24 2001",
			"references": 21,
			"revision": 101,
			"time": "2021-12-21T21:14:30-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}