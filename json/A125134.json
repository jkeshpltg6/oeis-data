{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A125134",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 125134,
			"data": "7,8,10,12,13,14,15,16,18,20,21,22,24,26,27,28,30,31,32,33,34,35,36,38,39,40,42,43,44,45,46,48,50,51,52,54,55,56,57,58,60,62,63,64,65,66,68,69,70,72,73,74,75,76,77,78,80,81,82,84,85,86,87,88,90",
			"name": "\"Brazilian\" numbers (\"les nombres brésiliens\" in French): numbers n such that there is a natural number b with 1 \u003c b \u003c n-1 such that the representation of n in base b has all equal digits.",
			"comment": [
				"The condition b \u003c n-1 is important because every number n has representation 11 in base n-1. - _Daniel Lignon_, May 22 2015",
				"Every even number \u003e= 8 is Brazilian. Odd Brazilian numbers are in A257521. - _Daniel Lignon_, May 22 2015",
				"Looking at A190300, it seems that asymptotically 100% of composite numbers are Brazilian, while looking at A085104, it seems that asymptotically 0% of prime numbers are Brazilian. The asymptotic density of Brazilian numbers would thus be 100%. - _Daniel Forgues_, Oct 07 2016"
			],
			"reference": [
				"Pierre Bornsztein, \"Hypermath\", Vuibert, Exercise a35, p. 7."
			],
			"link": [
				"Nathaniel Johnston, \u003ca href=\"/A125134/b125134.txt\"\u003eTable of n, a(n) for n = 1..4000\u003c/a\u003e",
				"9th Iberoamerican Mathematical Olympiad, \u003ca href=\"http://imomath.com/othercomp/Ib/IbMO94.pdf\"\u003eProblem 1: sensible numbers\u003c/a\u003e, Fortaleza, Brazil, September 17-25, 1994.",
				"Bernard Schott and others, \u003ca href=\"http://www.les-mathematiques.net/phorum/read.php?5,350062,page=1\"\u003ePostings to the French mathematical forum les-mathematiques.net\u003c/a\u003e",
				"Bernard Schott, \u003ca href=\"/A125134/a125134.pdf\"\u003eLes nombres brésiliens\u003c/a\u003e, Quadrature, no. 76, avril-juin 2010, pages 30-38; included here with permission from the editors of Quadrature."
			],
			"formula": [
				"a(n) ~ n. - _Charles R Greathouse IV_, Aug 09 2017"
			],
			"example": [
				"15 is a member since it is 33 in base 4."
			],
			"maple": [
				"isA125134 := proc(n) local k: for k from 2 to n-2 do if(nops(convert(convert(n,base,k),set))=1)then return true: fi: od: return false: end: A125134 := proc(n) option remember: local k: if(n=1)then return 7: fi: for k from procname(n-1)+1 do if(isA125134(k))then return k: fi: od: end: seq(A125134(n),n=1..65); # _Nathaniel Johnston_, May 24 2011"
			],
			"mathematica": [
				"fQ[n_] := Module[{b = 2, found = False}, While[b \u003c n - 1 \u0026\u0026 Length[Union[IntegerDigits[n, b]]] \u003e 1, b++]; b \u003c n - 1]; Select[Range[4, 90], fQ] (* _T. D. Noe_, May 07 2013 *)"
			],
			"program": [
				"(PARI) for(n=4,100,for(b=2,n-2,d=digits(n,b);if(vecmin(d)==vecmax(d),print1(n,\", \");break))) \\\\ _Derek Orr_, Apr 30 2015",
				"(PARI) is(n)=my(m); if(!isprime(n), return(if(issquare(n,\u0026m), m\u003e3 \u0026\u0026 (!isprime(m) || m==11), n\u003e6))); for(b=2,n-2, m=digits(n,b); for(i=2,#m, if(m[i]!=m[i-1], next(2))); return(1)); 0 \\\\ _Charles R Greathouse IV_, Aug 09 2017"
			],
			"xref": [
				"Cf. A190300 and A257521 (odd Brazilian numbers).",
				"Cf. A085104 (prime Brazilian numbers)."
			],
			"keyword": "nonn,base,easy",
			"offset": "1,1",
			"author": "_Bernard Schott_, Jan 21 2007",
			"ext": [
				"More terms from _Nathaniel Johnston_, May 24 2011"
			],
			"references": 64,
			"revision": 77,
			"time": "2018-11-09T18:25:36-05:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}