{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A050381",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 50381,
			"data": "2,3,10,40,170,785,3770,18805,96180,502381,2667034,14351775,78096654,429025553,2376075922,13252492311,74372374366,419651663108,2379399524742,13549601275893,77460249369658,444389519874841",
			"name": "Number of series-reduced planted trees with n leaves of 2 colors.",
			"comment": [
				"Consider the free algebraic system with two commutative associative operators (x+y) and (x*y) and two generators A,B. The number of elements with n occurrences of the generators is 2*a(n) if n\u003e1, and the number of generators if n=1. - _Michael Somos_, Aug 07 2017",
				"From _Gus Wiseman_, Feb 07 2020: (Start)",
				"Also the number of semi-lone-child-avoiding rooted trees with n leaves. Semi-lone-child-avoiding means there are no vertices with exactly one child unless that child is an endpoint/leaf. For example, the a(1) = 2 through a(3) = 10 trees are:",
				"  o    (oo)      (ooo)",
				"  (o)  (o(o))    (o(oo))",
				"       ((o)(o))  (oo(o))",
				"                 ((o)(oo))",
				"                 (o(o)(o))",
				"                 (o(o(o)))",
				"                 ((o)(o)(o))",
				"                 ((o)(o(o)))",
				"                 (o((o)(o)))",
				"                 ((o)((o)(o)))",
				"(End)"
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A050381/b050381.txt\"\u003eTable of n, a(n) for n = 1..500\u003c/a\u003e",
				"David Callan, \u003ca href=\"http://arxiv.org/abs/1406.7784\"\u003eA sign-reversing involution to count labeled lone-child-avoiding trees\u003c/a\u003e, arXiv:1406.7784 [math.CO], (30-June-2014).",
				"F. Chapoton, F. Hivert, J.-C. Novelli, \u003ca href=\"http://arxiv.org/abs/1307.0092\"\u003eA set-operad of formal fractions and dendriform-like sub-operads\u003c/a\u003e, arXiv preprint arXiv:1307.0092 [math.CO], 2013.",
				"V. P. Johnson, \u003ca href=\"http://people.math.sc.edu/czabarka/Theses/JohnsonThesis.pdf\"\u003eEnumeration Results on Leaf Labeled Trees\u003c/a\u003e, Ph. D. Dissertation, Univ. Southern Calif., 2012. - From _N. J. A. Sloane_, Dec 22 2012",
				"N. J. A. Sloane, \u003ca href=\"/transforms.txt\"\u003eTransforms\u003c/a\u003e",
				"Gus Wiseman, \u003ca href=\"https://docs.google.com/document/d/e/2PACX-1vS1zCO9fgAIe5rGiAhTtlrOTuqsmuPos2zkeFPYB80gNzLb44ufqIqksTB4uM9SIpwlvo-oOHhepywy/pub\"\u003eSequences counting series-reduced and lone-child-avoiding trees by number of vertices.\u003c/a\u003e",
				"\u003ca href=\"/index/Ro#rooted\"\u003eIndex entries for sequences related to rooted trees\u003c/a\u003e"
			],
			"formula": [
				"Doubles (index 2+) under EULER transform.",
				"Product_{k\u003e=1} (1-x^k)^-a(k) = 1 + a(1)*x + Sum_{k\u003e=2} 2*a(k)*x^k. - _Michael Somos_, Aug 07 2017",
				"a(n) ~ c * d^n / n^(3/2), where d = 6.158893517087396289837838459951206775682824030495453326610366016992093939... and c = 0.1914250508201011360729769525164141605187995730026600722369002... - _Vaclav Kotesovec_, Aug 17 2018"
			],
			"example": [
				"For n=2, the 2*a(2) = 6 elements are: A+A, A+B, B+B, A*A, A*B, B*B. - _Michael Somos_, Aug 07 2017"
			],
			"mathematica": [
				"terms = 22;",
				"B[x_] = x O[x]^(terms+1);",
				"A[x_] = 1/(1 - x + B[x])^2;",
				"Do[A[x_] = A[x]/(1 - x^k + B[x])^Coefficient[A[x], x, k] + O[x]^(terms+1) // Normal, {k, 2, terms+1}];",
				"Join[{2}, Drop[CoefficientList[A[x], x]/2, 2]] (* _Jean-François Alcover_, Aug 17 2018, after _Michael Somos_ *)",
				"slaurte[n_]:=If[n==1,{o,{o}},Join@@Table[Union[Sort/@Tuples[slaurte/@ptn]],{ptn,Rest[IntegerPartitions[n]]}]];",
				"Table[Length[slaurte[n]],{n,10}] (* _Gus Wiseman_, Feb 07 2020 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A, B); if( n\u003c2, 2*(n\u003e0), B = x * O(x^n); A = 1 / (1 - x + B)^2; for(k=2, n, A /= (1 - x^k + B)^polcoeff(A, k)); polcoeff(A, n)/2)}; /* _Michael Somos_, Aug 07 2017 */"
			],
			"xref": [
				"Column 2 of A319254.",
				"Cf. A029856, A031148.",
				"Lone-child-avoiding rooted trees with n leaves are A000669.",
				"Lone-child-avoiding rooted trees with n vertices are A001678.",
				"The locally disjoint case is A331874.",
				"Semi-lone-child-avoiding rooted trees with n vertices are A331934.",
				"Matula-Goebel numbers of these trees are A331935.",
				"Cf. A000081, A005804, A141268, A196545, A291636, A316697, A330465, A331872, A331933, A331964."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Christian G. Bower_, Nov 15 1999",
			"references": 19,
			"revision": 36,
			"time": "2020-02-09T04:03:59-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}