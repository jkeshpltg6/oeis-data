{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A243116",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 243116,
			"data": "1,4,28,220,1816,15424,133456,1169872,10354528,92331904,828204928,7464652672,67547774464,613295870464,5584367987968,50974595472640,466307503244800,4273832891668480,39237007284226048,360768875975526400,3321625537178669056,30619908430235828224,282578914501599305728",
			"name": "a(n) = Sum_{k=0..n} C(n + 2*k, 3*k) * C(3*k, 2*k).",
			"comment": [
				"Compare to: Sum_{k=0..n} (-1)^k * C(n+2*k,3*k) * C(3*k,2*k) = (-2)^n for n\u003e=0."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A243116/b243116.txt\"\u003eTable of n, a(n) for n = 0..1027\u003c/a\u003e",
				"Hacène Belbachir, Abdelghani Mehdaoui, \u003ca href=\"https://lrecits.usthb.dz/6.5.pdf\"\u003eDiagonal sums in Pascal pyramid (1, 2, r)\u003c/a\u003e, Les Annales RECITS (2019) Vol. 6, 45-52."
			],
			"formula": [
				"G.f.: Sum_{n\u003e=0} C(3*n, n) * x^n / (1-x)^(3*n+1).  - _Paul D. Hanna_, Aug 30 2014",
				"G.f.: 1/(1-x) / ( 3 / G(x/(1-x)^3) - 2 ), where G(x) = 1 + x*G(x)^3 is the g.f. of A001764.  - _Paul D. Hanna_, Aug 30 2014",
				"G.f. satisfies: A(x) = 1 + (4-3*x)*A(x) - (4 - 39*x + 12*x^2 - 4*x^3)*A(x)^3. - _Paul D. Hanna_, Sep 05 2014",
				"a(n) = Sum_{k=0..n} A109955(n,k) * A005809(k).",
				"a(n) = -(-2)^n + 2*Sum_{k=0..[n/2]} C(n+4*k, 6*k) * C(6*k, 4*k).",
				"Recurrence: 2*n*(2*n-1)*(3*n-4)*a(n) = (3*n-2)*(39*n^2 - 65*n + 18)*a(n-1) - 2*(n-1)*(18*n^2 - 33*n + 10)*a(n-2) + 4*(n-2)*(n-1)*(3*n-1)*a(n-3). - _Vaclav Kotesovec_, Aug 21 2014"
			],
			"example": [
				"G.f.: A(x) = 1 + 4*x + 28*x^2 + 220*x^3 + 1816*x^4 + 15424*x^5 +...",
				"where",
				"A(x) = 1/(1-x) + 3*x/(1-x)^4 + 15*x^2/(1-x)^7 + 84*x^3/(1-x)^10 + 495*x^4/(1-x)^13 + 3003*x^5/(1-x)^16 + 18564*x^6/(1-x)^19 + 116280*x^7/(1-x)^22 + 735471*x^8/(1-x)^25 +...+ C(3*n, n)*x^n/(1-x)^(3*n+1) +...",
				"ILLUSTRATION OF TERMS.",
				"The sequence A005809(k) = C(3*k, 2*k) begins:",
				"[1, 3, 15, 84, 495, 3003, 18564, 116280, 735471, 4686825, ...];",
				"the triangle A109955(n,k) = C(n + 2*k, 3*k) begins:",
				"1;",
				"1, 1;",
				"1, 4, 1;",
				"1, 10, 7, 1;",
				"1, 20, 28, 10, 1;",
				"1, 35, 84, 55, 13, 1;",
				"1, 56, 210, 220, 91, 16, 1;",
				"1, 84, 462, 715, 455, 136, 19, 1; ...",
				"where a(n) = Sum_{k=0..n} A109955(n,k) * A005809(k):",
				"a(1) = 1*1 + 1*3 = 4;",
				"a(2) = 1*1 + 4*3 + 1*15 = 28;",
				"a(3) = 1*1 + 10*3 + 7*15 + 1*84 = 220;",
				"a(4) = 1*1 + 20*3 + 28*15 + 10*84 + 1*495 = 1816; ...",
				"compare to: Sum_{k=0..n} (-1)^k * A109955(n,k) * A005809(k) = (-2)^n."
			],
			"mathematica": [
				"Table[Sum[Binomial[n + 2*k, 3*k] * Binomial[3*k, 2*k],{k,0,n}],{n,0,20}] (* _Vaclav Kotesovec_, Aug 21 2014 *)"
			],
			"program": [
				"(PARI) {a(n)=sum(k=0,n, binomial(n+2*k,3*k) * binomial(3*k,2*k))}",
				"for(n=0,30,print1(a(n),\", \"))",
				"(PARI) {a(n)=-(-2)^n + 2*sum(k=0, n\\2, binomial(n+4*k, 6*k) * binomial(6*k, 4*k))}",
				"for(n=0, 30, print1(a(n), \", \"))",
				"(PARI) {a(n)=local(A=1); A=sum(m=0, n, binomial(3*m, m) * x^m/(1-x +x*O(x^n))^(3*m+1)); polcoeff(A, n)}",
				"for(n=0, 30, print1(a(n), \", \"))"
			],
			"xref": [
				"Cf. A109955, A005809."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Paul D. Hanna_, Aug 20 2014",
			"references": 1,
			"revision": 27,
			"time": "2020-06-26T02:59:50-04:00",
			"created": "2014-08-20T21:49:53-04:00"
		}
	]
}