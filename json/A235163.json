{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A235163",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 235163,
			"data": "9,26,75,217,629,1826,5307,15438,44941,130900,381444,1111926,3242224,9455987,27583372,80472698,234799873,685149328,1999414181,5835044495,17029601028,49702671494,145066398937,423412132499,1235854038791,3607255734629,10529101874491",
			"name": "Number of positive integers with n digits in which adjacent digits differ by at most 1.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A235163/b235163.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (6,-10,1,6,-1)."
			],
			"formula": [
				"a(n) = Sum_{r=0..9} u(n,r) where u(n,r) = 0 if r\u003c0 or r\u003e9, u(1,0) = 0, u(1,r) = 1 for 1\u003c=r\u003c=9, and otherwise u(n,r) = u(n-1,r-1) + u(n-1,r) + u(n-1,r+1).",
				"G.f.: -x*(3*x^4-18*x^3-9*x^2+28*x-9)/(x^5-6*x^4-x^3+10*x^2-6*x+1). - _Alois P. Heinz_, Jan 12 2014"
			],
			"example": [
				"a(2) = 26: 10, 11, 12, 21, 22, 23, 32, 33, 34, 43, 44, 45, 54, 55, 56, 65, 66, 67, 76, 77, 78, 87, 88, 89, 98, 99."
			],
			"maple": [
				"u:= proc(n, r) option remember; `if`(n=1, `if`(r=0, 0, 1),",
				"      add(`if`(r+i in [$0..9], u(n-1, r+i), 0), i=-1..1))",
				"    end:",
				"a:= n-\u003e add(u(n, r), r = 0..9):",
				"seq(a(n), n=1..30);  # _Alois P. Heinz_, Jan 12 2014"
			],
			"mathematica": [
				"CoefficientList[Series[-x*(3*x^4-18*x^3-9*x^2+28*x-9)/(x^5-6*x^4-x^3+10*x^2-6*x+1),{x,0,30}],x]//Rest (* _Harvey P. Dale_, Aug 13 2019 *)"
			],
			"program": [
				"(Python)",
				"from functools import cache",
				"@cache",
				"def u(n, r):",
				"    if r \u003c 0 or r \u003e 9: return 0",
				"    if n == 1: return (r \u003e 0)",
				"    return u(n-1, r-1) + u(n-1, r) + u(n-1, r+1)",
				"def a(n): return sum(u(n, r) for r in range(10))",
				"print([a(n) for n in range(1, 28)]) # _Michael S. Branicky_, Sep 26 2021"
			],
			"xref": [
				"Cf. A032981, A090994, A126364 (allowing leading zeros)."
			],
			"keyword": "nonn,base,easy",
			"offset": "1,1",
			"author": "_Gerry Leversha_, Jan 04 2014",
			"references": 1,
			"revision": 33,
			"time": "2021-09-26T08:11:46-04:00",
			"created": "2014-01-12T12:11:48-05:00"
		}
	]
}