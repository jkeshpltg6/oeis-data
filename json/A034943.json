{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A034943",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 34943,
			"data": "1,1,1,2,5,12,28,65,151,351,816,1897,4410,10252,23833,55405,128801,299426,696081,1618192,3761840,8745217,20330163,47261895,109870576,255418101,593775046,1380359512,3208946545",
			"name": "Binomial transform of Padovan sequence A000931.",
			"comment": [
				"Trisection of the Padovan sequence: a(n) = A000931(3n). - _Paul Barry_, Jul 06 2004",
				"a(n+1) gives diagonal sums of Riordan array (1/(1-x),x/(1-x)^3). - _Paul Barry_, Oct 11 2005",
				"a(n+2) is the sum, over all Boolean n-strings, of the product of the lengths of the runs of 1. For example, the Boolean 7-string (0,1,1,0,1,1,1) has two runs of 1s. Their lengths, 2 and 3, contribute a product of 6 to a(9). The 8 Boolean 3-strings contribute to a(5) as follows: 000 (empty product), 001, 010, 100, 101 all contribute 1, 011 and 110 contribute 2, 111 contributes 3. - _David Callan_, Nov 29 2007",
				"[a(n), a(n+1), a(n+2)], n \u003e 0, = [0,1,0; 0,0,1; 1,-2,3]^n * [1,1,1]. - _Gary W. Adamson_, Mar 27 2008",
				"Without the initial 1 and 1: 1, 2, 5, 12, 28, this is also the transform of 1 by the T_{1,0} transformation; see Choulet link. - _Richard Choulet_, Apr 11 2009",
				"Without the first 1: transform of 1 by T_{0,0} transformation (see Choulet link). - _Richard Choulet_, Apr 11 2009",
				"Starting (1, 2, 5, 12, ...) = INVERT transform of (1, 1, 2, 3, 4, 5, ...) and row sums of triangle A159974. - _Gary W. Adamson_, Apr 28 2009",
				"a(n+1) is also the number of 321-avoiding separable permutations. (A permutation is separable if it avoids both 2413 and 3142.) - Vince Vatter, Sep 21 2009",
				"a(n+1) is an eigensequence of the sequence array for (1,1,2,3,4,5,...). - _Paul Barry_, Nov 03 2010",
				"Equals the INVERTi transform of A055588: (1, 2, 4, 9, 22, 56, ...) - _Gary W. Adamson_, Apr 01 2011",
				"The Ca3 sums, see A180662, of triangle A194005 equal the terms of this sequence without a(0) and a(1). - _Johannes W. Meijer_, Aug 16 2011",
				"Without the initial 1, a(n) = row sums of A182097(n)*A007318(n,k); i.e., a Triangular array T(n,k) multiplying the binomial (Pascal's) triangle by the Padovan sequence where a(0) = 1, a(1) = 0 and a(2) = 1. - _Bob Selcoe_, Jun 28 2013",
				"a(n+1) is the top left entry of the n-th power of any of the 3 X 3 matrices [1, 1, 1; 0, 1, 1; 1, 0, 1] or [1, 1, 0; 1, 1, 1; 1, 0, 1] or [1, 1, 1; 1, 1, 0; 0, 1, 1] or [1, 0, 1; 1, 1, 0; 1, 1, 1]. - _R. J. Mathar_, Feb 03 2014",
				"a(n) is the top left entry of the n-th power of the 3 X 3 matrix [1, 0, 1; 1, 1, 1; 0, 1, 1] or of the 3 X 3 matrix [1, 1, 0; 0, 1, 1; 1, 1, 1]. - _R. J. Mathar_, Feb 03 2014",
				"Number of sequences (e(1), ..., e(n-1)), 0 \u003c= e(i) \u003c i, such that there is no triple i \u003c j \u003c k with e(i) != e(j) \u003c e(k) and e(i) \u003c= e(k). [Martinez and Savage, 2.8] - _Eric M. Schmidt_, Jul 17 2017",
				"a(n+1) is the number of words of length n over the alphabet {0,1,2} that do not contain the substrings 01 or 12 and do not start with a 2 and do not end with a 0. - _Yiseth K. Rodríguez C._, Sep 11 2020"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A034943/b034943.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Miklos Bona, Rebecca Smith, \u003ca href=\"https://arxiv.org/abs/1901.00026\"\u003ePattern avoidance in permutations and their squares\u003c/a\u003e, arXiv:1901.00026 [math.CO], 2018. See H(z), Ex. 4.1.",
				"Richard Choulet, \u003ca href=\"http://www.apmep.fr/IMG/pdf/curtz1.pdf\"\u003eCurtz like Transformation\u003c/a\u003e",
				"Dairyko, Michael; Tyner, Samantha; Pudwell, Lara; Wynn, Casey. \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v19i3p22\"\u003eNon-contiguous pattern avoidance in binary trees\u003c/a\u003e. Electron. J. Combin. 19 (2012), no. 3, Paper 22, 21 pp. MR2967227. - From _N. J. A. Sloane_, Feb 01 2013",
				"Stoyan Dimitrov, \u003ca href=\"https://arxiv.org/abs/2103.04332\"\u003eSorting by shuffling methods and a queue\u003c/a\u003e, arXiv:2103.04332 [math.CO], 2021.",
				"Phan Thuan Do, Thi Thu Huong Tran, Vincent Vajnovszki, \u003ca href=\"https://arxiv.org/abs/1809.00742\"\u003eExhaustive generation for permutations avoiding a (colored) regular sets of patterns\u003c/a\u003e, arXiv:1809.00742 [cs.DM], 2018.",
				"Brian Hopkins, Hua Wang, \u003ca href=\"https://arxiv.org/abs/2003.05291\"\u003eRestricted Color n-color Compositions\u003c/a\u003e, arXiv:2003.05291 [math.CO], 2020.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=904\"\u003eEncyclopedia of Combinatorial Structures 904\u003c/a\u003e",
				"H. Magnusson and H. Ulfarsson, \u003ca href=\"http://arxiv.org/abs/1211.7110\"\u003eAlgorithms for discovering and proving theorems about permutation patterns\u003c/a\u003e, arXiv preprint arXiv:1211.7110 [math.CO], 2012.",
				"Megan A. Martinez and Carla D. Savage, \u003ca href=\"https://arxiv.org/abs/1609.08106\"\u003ePatterns in Inversion Sequences II: Inversion Sequences Avoiding Triples of Relations\u003c/a\u003e, arXiv:1609.08106 [math.CO], 2016",
				"V. Vatter, \u003ca href=\"https://arxiv.org/abs/0911.2683\"\u003eFinding regular insertion encodings for permutation classes\u003c/a\u003e,  arXiv:0911.2683 [math.CO], 2009. [From _Vincent Vatter_, Sep 21 2009]",
				"Chunyan Yan, Zhicong Lin, \u003ca href=\"https://arxiv.org/abs/1912.03674\"\u003eInversion sequences avoiding pairs of patterns\u003c/a\u003e, arXiv:1912.03674 [math.CO], 2019.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-2,1)."
			],
			"formula": [
				"a(n) = 3*a(n-1) - 2*a(n-2) + a(n-3).",
				"a(n) = Sum_{k=0..floor(n/2)} binomial(n+k-1, 3*k). - _Paul Barry_, Jul 06 2004",
				"G.f.: (1 - 2*x)/(1 - 3*x + 2*x^2 - x^3). - _Paul Barry_, Jul 06 2005",
				"G.f.: 1 + x / (1 - x / (1 - x / (1 - x / (1 + x / (1 - x))))). - _Michael Somos_, Mar 31 2012",
				"a(-1 - n) = A185963(n). - _Michael Somos_, Mar 31 2012"
			],
			"example": [
				"1 + x + x^2 + 2*x^3 + 5*x^4 + 12*x^5 + 28*x^6 + 65*x^7 + 151*x^8 + ..."
			],
			"maple": [
				"A034943 := proc(n): add(binomial(n+k-1, 3*k), k=0..floor(n/2)) end: seq(A034943(n), n=0..28); # _Johannes W. Meijer_, Aug 16 2011"
			],
			"mathematica": [
				"a=1;b=0;c=1;lst={};Do[a+=b;AppendTo[lst,a];b+=c;c+=a,{n,5!}];lst (* _Vladimir Joseph Stephan Orlovsky_, Jan 20 2009 *)",
				"LinearRecurrence[{3,-2,1},{1,1,1},30] (* _Harvey P. Dale_, Aug 11 2017 *)"
			],
			"program": [
				"(MAGMA) I:=[1, 1, 1]; [n le 3 select I[n] else 3*Self(n-1)-2*Self(n-2)+Self(n-3): n in [1..40]]; // _Vincenzo Librandi_, Feb 14 2012",
				"(PARI) {a(n) = if( n\u003c1, n = 0-n; polcoeff( (1 - x + x^2) / (1 - 2*x + 3*x^2 - x^3) + x * O(x^n), n), n = n-1; polcoeff( (1 - x + x^2) / (1 - 3*x + 2*x^2 - x^3) + x * O(x^n), n))} /* _Michael Somos_, Mar 31 2012 */"
			],
			"xref": [
				"First differences of A052921.",
				"Cf. A097550, A137531, A052921, A095263, A159974, A055588, A185963."
			],
			"keyword": "nonn,easy",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Edited by _Charles R Greathouse IV_, Apr 20 2010"
			],
			"references": 22,
			"revision": 98,
			"time": "2021-05-20T22:23:18-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}