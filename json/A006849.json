{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006849",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6849,
			"id": "M1947",
			"data": "2,9,69,567,5112,48114,469179,4691115,47849940,495893502,5206886874,55273052646,592211326464,6395881806180,69555215111319,761015877850035,8371174661041500,92523509359662150,1027010953940099238",
			"name": "Number of strongly self-dual planar maps with 2n edges.",
			"comment": [
				"A planar map is called strongly self-dual if it is self-dual with respect to an orientation-preserving duality. - _Valery A. Liskovets_, May 27 2006"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Gheorghe Coserea, \u003ca href=\"/A006849/b006849.txt\"\u003eTable of n, a(n) for n = 1..200\u003c/a\u003e",
				"V. A. Liskovets, \u003ca href=\"https://www.researchgate.net/publication/277008787_Enumeration_of_nonisomorphic_planar_maps\"\u003eEnumeration of nonisomorphic planar maps\u003c/a\u003e, Selecta Math. Sovietica, 4 (No. 4, 1985), 303-323."
			],
			"formula": [
				"a(2k) = 3^(2k)C(2k)/2=A005159(2k)/2 (4k edges, k\u003e0) and a(2k-1) = (3^(2k-1)C(2k-1)+3^(k-1)C(k-1))/2 =(A005159(2k-1)+A005159(k-1))/2 (4k-2 edges, k\u003e0) where C(n) = A000108(n) (Catalan numbers). - _Valery A. Liskovets_, May 27 2006",
				"G.f.: -1/2 + 1/(1 + (1 - 12*x)^(1/2)) + x/(1 + (1 - 12*x^2)^(1/2)). - _Gheorghe Coserea_, Aug 15 2015"
			],
			"mathematica": [
				"With[{nn = 21}, CoefficientList[InverseSeries[Series[2*x/(12*x^2 + 12*x + 3), {x, 0, nn}]] + InverseSeries[Series[2*x/(12*x^2 + 1), {x, 0, nn}]], x]] (* _Gheorghe Coserea_, Aug 15 2015 *)",
				"a[n_] := 3^n*CatalanNumber[n]/2 + If[OddQ[n], 3^((n-1)/2)*CatalanNumber[(n-1)/2]/2, 0]; Array[a, 20] (* _Jean-François Alcover_, Jan 17 2018 *)"
			],
			"program": [
				"(PARI) C = n -\u003e binomial(2*n, n) / (n + 1);",
				"a(n) = if (n%2, ( 3^n*C(n) + 3^((n-1)/2)*C((n-1)/2) )/2, 3^n*C(n)/2);",
				"apply(n -\u003e a(n), vector(30, i, i)) \\\\ _Gheorghe Coserea_, Aug 04 2015",
				"(PARI) x='x + O('x^33); Vec(-1/2 + 1/(1 + (1 - 12*x)^(1/2)) + x/(1 + (1 - 12*x^2)^(1/2))) \\\\ _Gheorghe Coserea_, Aug 15 2015"
			],
			"xref": [
				"Cf. A005159, A000108, A005470."
			],
			"keyword": "nonn,nice",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Valery A. Liskovets_, May 27 2006"
			],
			"references": 2,
			"revision": 35,
			"time": "2021-12-17T11:27:26-05:00",
			"created": "1991-07-25T03:00:00-04:00"
		}
	]
}