{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A068085",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 68085,
			"data": "0,1,21,78,1540,30381,112575,2220778,43809480,162333171,3202360435,63173239878,234084320106,4617801526591,91095768094695,337549427259780,6658866598983886,131360034419310411,486746040024282753,9602081017933237120,189421078536877518066,701887452165588470145",
			"name": "Numbers k such that k and 10*k are both triangular numbers.",
			"comment": [
				"Let y=sqrt(8*k+1) and x=sqrt(80*k+1), which must be integers if k and 10*k are triangular. These quantities satisfy the Pell-like equation x^2 - 10*y^2 = -9. All solutions x+y*sqrt(10) are obtained from 1+sqrt(10), 9+3*sqrt(10) and 41+13*sqrt(10) by multiplying by powers of the fundamental unit 19+6*sqrt(10).",
				"Conjecture: satisfies a linear recurrence having signature (1, 0, 1442, -1442, 0, -1, 1). - _Harvey P. Dale_, Sep 03 2020",
				"This conjecture is true because of the connection between (generalized) Pell equations and continued fractions of quadratic irrationals. - _Georg Fischer_, Feb 23 2021",
				"From _Vladimir Pletser_, Feb 26 2021: (Start)",
				"The triangular numbers T(t) that are one-tenth of other triangular numbers T(u) : T(t)=T(u)/10. The t's are in A341893, and the u's are in A341895.",
				"Can be defined for negative n by setting a(n) = a(1-n) for all n in Z. (End)"
			],
			"link": [
				"Georg Fischer, \u003ca href=\"/A068085/b068085.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_07\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,0,1442,-1442,0,-1,1)."
			],
			"formula": [
				"a(n) = (99 + 1442*a(n-3) + 57*sqrt((1 + 8*a(n-3))*(1 + 88*a(n-3))))/2.",
				"G.f.: -x^2*(x^4+20*x^3+57*x^2+20*x+1) / ((x-1)*(x^6-1442*x^3+1)). - _Colin Barker_, Jun 24 2014",
				"From __Vladimir Pletser_, Feb 26 2021: (Start)",
				"a(n) = 1442 *a(n-3) - a(n-6) + 99, for n \u003e 3, with a(-2) = 21, a(-1) = 1, a(0) = 0, a(1) = 0, a(2) = 1, a(3) = 21.",
				"a(n) = a(n - 1) + 1442 ( a(n - 3) - a(n - 4) ) - ( a(n - 6) - a(n - 7) ) for n \u003e= 4 with a(-2) = 21, a(-1) = 1, a(0) = 0, a(1) = 0, a(2) = 1, a(3) = 21.",
				"a(n) = b(n)*(b(n)+1)/2 where b(n) is A341893(n). (End)"
			],
			"example": [
				"21 and 210 are both triangular numbers."
			],
			"maple": [
				"f := gfun:-rectoproc({a(-3) = 21, a(-2) = 1, a(-1) = 0, a(0) = 0, a(1) = 1, a(2) = 21, a(n) = 1442*a(n-3)-a(n-6)+99}, a(n), remember); map(f, [`$`(0 .. 1000)])[] ; # _Vladimir Pletser_, Feb 26 2021"
			],
			"mathematica": [
				"a[0]=0; a[1]=1; a[2]=21; a[n_] := a[n]=(99+1442a[n-3]+57Sqrt[(1+8a[n-3])(1+80a[n-3])])/2"
			],
			"xref": [
				"Cf. for k and m*k both triangular: A075528 (m=2), A076139 (m=3), 0 (m=4), A077260 (m=5), A077289 (m=6), A07739 (m=7), A336624 (m=8), 0 (m=9), this sequence (m=10)."
			],
			"keyword": "nonn,easy",
			"offset": "1,3",
			"author": "_Amarnath Murthy_, Feb 18 2002",
			"ext": [
				"Edited by _Dean Hickerson_, Feb 20 2002",
				"More terms from _Georg Fischer_, Feb 23 2021"
			],
			"references": 4,
			"revision": 48,
			"time": "2021-02-27T21:18:13-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}