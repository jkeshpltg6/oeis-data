{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007357",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7357,
			"id": "M4267",
			"data": "6,60,90,36720,12646368,22276800,126463680,4201148160,28770487200,287704872000,1446875426304,2548696550400,14468754263040,590325173932032,3291641594841600,8854877608980480,32916415948416000",
			"name": "Infinitary perfect numbers.",
			"comment": [
				"Numbers N whose sum of infinitary divisors equals 2*N: A049417(N)=2*N. - _Joerg Arndt_, Mar 20 2011",
				"6 is the only infinitary perfect number which is also perfect number (A000396). 6 is also the only squarefree infinitary perfect number. - _Vladimir Shevelev_, Mar 02 2011"
			],
			"reference": [
				"G. L. Cohen, personal communication.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"G. L. Cohen, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1990-0993927-5\"\u003eOn an integer's infinitary divisors\u003c/a\u003e, Math. Comp., 54 (1990), 395-411.",
				"A. V. Lelechenko, \u003ca href=\"http://taac.org.ua/files/a2014/proceedings/UA-2-Andrew%20Lelechenko-440.pdf\"\u003eThe Quest for the Generalized Perfect Numbers\u003c/a\u003e, in Theoretical and Applied Aspects of Cybernetics, TAAC 2014, Kiev.",
				"David Moews, \u003ca href=\"http://djm.cc/aliquot-database/aliquot-database-.1.txt\"\u003eA database of aliquot cycles - Known infinitary perfect numbers (together with unitary perfect and e-perfect ones)\u003c/a\u003e.",
				"Jan Munch Pedersen, \u003ca href=\"http://amicable.adsl.dk/aliquot/i1/i1.txt\"\u003eKnown infinitary perfect numbers\u003c/a\u003e. [BROKEN LINK]",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/InfinitaryPerfectNumber.html\"\u003eInfinitary Perfect Number.\u003c/a\u003e"
			],
			"formula": [
				"{n: A049417(n) = 2*n}. - _R. J. Mathar_, Mar 18 2011",
				"a(n)==0 (mod 6). Thus there are no odd infinitary perfect numbers. - _Vladimir Shevelev_, Mar 02 2011"
			],
			"example": [
				"Let n=90. Its unique expansion over distinct terms of A050376 is 90=2*5*9. Thus the infinitary divisors of 90 are 1,2,5,9,10,18,45,90. The number of such divisors is 2^3, i.e., the cardinality of the set of all subsets of the set {2,5,9}. The sum of such divisors is (2+1)*(5+1)*(9+1)=180 and the sum of proper such divisors is 90=n. Thus 90 is in the sequence. - _Vladimir Shevelev_, Mar 02 2011"
			],
			"maple": [
				"isA007357 := proc(n)",
				"    A049417(n) = 2*n ;",
				"    simplify(%) ;",
				"end proc:",
				"for n from 1 do",
				"    if isA007357(n) then",
				"        printf(\"%d,\\n\",n) ;",
				"    end if;",
				"end do: # _R. J. Mathar_, Oct 05 2017"
			],
			"mathematica": [
				"infiPerfQ[n_] := 2n == Total[If[n == 1, 1, Sort @ Flatten @ Outer[ Times, Sequence @@ (FactorInteger[n] /. {p_, m_Integer} :\u003e p^Select[Range[0, m], BitOr[m, #] == m\u0026])]]];",
				"For[n = 6, True, n += 6, If[infiPerfQ[n], Print[n]]] (* _Jean-François Alcover_, Feb 08 2021 *)"
			],
			"xref": [
				"Cf. A129656 (infinitary abundant), A129657 (infinitary deficient)."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Eric W. Weisstein_, Jan 27 2004"
			],
			"references": 26,
			"revision": 50,
			"time": "2021-12-26T21:39:09-05:00",
			"created": "1994-04-28T03:00:00-04:00"
		}
	]
}