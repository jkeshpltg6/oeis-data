{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A253286",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 253286,
			"data": "1,0,1,0,1,1,0,3,2,1,0,13,8,3,1,0,73,44,15,4,1,0,501,304,99,24,5,1,0,4051,2512,801,184,35,6,1,0,37633,24064,7623,1696,305,48,7,1,0,394353,261536,83079,18144,3145,468,63,8,1",
			"name": "Square array read by upward antidiagonals, A(n,k) = Sum_{j=0..n} (n-j)!*C(n,n-j)* C(n-1,n-j)*k^j, for n\u003e=0 and k\u003e=0.",
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A253286/b253286.txt\"\u003eAntidiagonals n = 0..139, flattened\u003c/a\u003e",
				"\u003ca href=\"/index/La#Laguerre\"\u003eIndex entries for sequences related to Laguerre polynomials\u003c/a\u003e"
			],
			"formula": [
				"A(n,k) = k*n!*hypergeom([1-n],[2],-k)) for n\u003e=1 and 1 for n=0.",
				"Row sums of triangle, Sum_{k=0..n} A(n-k, k) = 1 + A256325(n).",
				"From _Seiichi Manyama_, Feb 03 2021: (Start)",
				"E.g.f. of column k: exp(k*x/(1-x)).",
				"T(n,k) = (2*n+k-2) * T(n-1,k) - (n-1) * (n-2) * T(n-2, k) for n \u003e 1. (End)",
				"From _G. C. Greubel_, Feb 23 2021: (Start)",
				"A(n, k) = k*(n-1)!*LaguerreL(n-1, 1, -k) with A(0, k) = 1.",
				"T(n, k) = k*(n-k-1)!*LaguerreL(n-k-1, 1, -k) with T(n, n) = 1.",
				"T(n, 2) = A052897(n) = A086915(n)/2.",
				"Sum_{k=0..n} T(n, k) = 1 + Sum_{k=0..n-1} (n-k-1)*k!*LaguerreL(k, 1, k-n+1). (End)"
			],
			"example": [
				"Square array starts, A(n,k):",
				"      1,       1,       1,       1,      1,      1,      1, ...  A000012",
				"      0,       1,       2,       3,      4,      5,      6, ...  A001477",
				"      0,       3,       8,      15,     24,     35,     48, ...  A005563",
				"      0,      13,      44,      99,    184,    305,    468, ...  A226514",
				"      0,      73,     304,     801,   1696,   3145,   5328, ...",
				"      0,     501,    2512,    7623,  18144,  37225,  68976, ...",
				"      0,    4051,   24064,   83079, 220096, 495475, 997056, ...",
				"A000007, A000262, A052897, A255806, ...",
				"Triangle starts, T(n, k) = A(n-k, k):",
				"  1;",
				"  0,   1;",
				"  0,   1,   1;",
				"  0,   3,   2,  1;",
				"  0,  13,   8,  3,  1;",
				"  0,  73,  44, 15,  4, 1;",
				"  0, 501, 304, 99, 24, 5, 1;"
			],
			"maple": [
				"L := (n, k) -\u003e (n-k)!*binomial(n,n-k)*binomial(n-1,n-k):",
				"A := (n, k) -\u003e add(L(n,j)*k^j, j=0..n):",
				"# Alternatively:",
				"# A := (n, k) -\u003e `if`(n=0,1, simplify(k*n!*hypergeom([1-n],[2],-k))):",
				"for n from 0 to 6 do lprint(seq(A(n,k), k=0..6)) od;"
			],
			"mathematica": [
				"A253286[n_, k_]:= If[k==n, 1, k*(n-k-1)!*LaguerreL[n-k-1, 1, -k]];",
				"Table[A253286[n, k], {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, Feb 23 2021 *)"
			],
			"program": [
				"(PARI) {T(n, k) = if(n==0, 1, n!*sum(j=1, n, k^j*binomial(n-1, j-1)/j!))} \\\\ _Seiichi Manyama_, Feb 03 2021",
				"(PARI) {T(n, k) = if(n\u003c2, (k-1)*n+1, (2*n+k-2)*T(n-1, k)-(n-1)*(n-2)*T(n-2, k))} \\\\ _Seiichi Manyama_, Feb 03 2021",
				"(Sage) flatten([[1 if k==n else k*factorial(n-k-1)*gen_laguerre(n-k-1, 1, -k) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Feb 23 2021",
				"(Magma) [k eq n select 1 else k*Factorial(n-k-1)*Evaluate(LaguerrePolynomial(n-k-1, 1), -k): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Feb 23 2021"
			],
			"xref": [
				"Main diagonal gives A293145.",
				"Cf. A000262, A001477, A005563, A052897, A226514, A255806, A256325."
			],
			"keyword": "tabl,easy,nonn",
			"offset": "0,8",
			"author": "_Peter Luschny_, Mar 24 2015",
			"references": 7,
			"revision": 29,
			"time": "2021-02-25T02:26:56-05:00",
			"created": "2015-03-24T12:12:56-04:00"
		}
	]
}