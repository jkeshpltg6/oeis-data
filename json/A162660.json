{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A162660",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 162660,
			"data": "0,1,0,0,2,0,-2,0,3,0,0,-8,0,4,0,16,0,-20,0,5,0,0,96,0,-40,0,6,0,-272,0,336,0,-70,0,7,0,0,-2176,0,896,0,-112,0,8,0,7936,0,-9792,0,2016,0,-168,0,9,0,0,79360,0,-32640,0,4032,0,-240,0,10,0",
			"name": "Triangle read by rows, the coefficients of the complementary Swiss-Knife polynomials.",
			"comment": [
				"Definition. V_n(x) = (skp(n, x+1) - skp(n, x-1))/2 where skp(n,x) are the Swiss-Knife polynomials A153641. - _Peter Luschny_, Jul 23 2012",
				"Equivalently, let the polynomials V_n(x) (n\u003e=0) defined by V_n(x) = Sum_{k=0..n} Sum_{v=0..k} (-1)^v*C(k,v)*L(k)*(x+v+1)^n; the sequence L(k) = -1 - H(k-1)*(-1)^floor((k-1)/4) / 2^floor(k/2) if k \u003e 0 and L(0)=0; H(k) = 1 if k mod 4 \u003c\u003e 0, otherwise 0.",
				"(1) V_n(0) = 2^n euler(n,1) for n \u003e 0, A155585.",
				"(2) V_n(1) = 1 - euler(n).",
				"(3) V_{n-1}(0) n / (4^n - 2^n) = B_n gives for n \u003e 1 the Bernoulli numbers A027641/A027642.",
				"(4) V_{n-1}(0) n (2/2^n-2)/(2^n-1) = G_n the Genocchi number A036968 for n \u003e 1.",
				"(5) V_n(1/2)2^{n} - 1 is a signed version of the generalized Euler (Springer) numbers, see A001586.",
				"The Swiss-Knife polynomials (A153641) are complementary to the polynomials defined here. Adding both gives polynomials with e.g.f. exp(x*t)*(sech(t)+tanh(t)), the coefficients of which are a signed variant of A109449.",
				"The Swiss-Knife polynomials as well as the complementary Swiss-Knife polynomials are closely related to the Bernoulli and Euler polynomials. Let F be a sequence and",
				"P_{F}[n](x) = Sum_{k=0..n} Sum_{v=0..k} (-1)^v*C(k,v)*F(k)*(x+v+1)^n.",
				"V_n(x) = P_{F}[n](x) with F(k)=L(k) defined above, are the Co-Swiss-Knife polynomials,",
				"W_n(x) = P_{F}[n](x) with F(k)=c(k) the Chen sequence defined in A153641 are the Swiss-Knife polynomials.",
				"B_n(x) = P_{F}[n](x-1) with F(k)=1/(k+1) are the Bernoulli polynomials,",
				"E_n(x) = P_{F}[n](x-1) with F(k)=2^(-k) are the Euler polynomials.",
				"The most striking formal difference between the Swiss-Knife-type polynomials and the Bernoulli-Euler type polynomials is: The SK-type polynomials have integer coefficients whereas the BE-type polynomials have rational coefficients."
			],
			"link": [
				"Leonhard  Euler, (1735), \u003ca href=\"http://arxiv.org/abs/math/0506415\"\u003eDe summis serierum reciprocarum\u003c/a\u003e, Opera Omnia I.14, E 41, 73-86; On the sums of series of reciprocals, arXiv:math/0506415 [math.HO], 2005-2008.",
				"Peter Luschny, \u003ca href=\"http://www.luschny.de/math/seq/SwissKnifePolynomials.html\"\u003eThe Swiss-Knife polynomials.\u003c/a\u003e",
				"Peter Luschny, \u003ca href=\"http://oeis.org/wiki/User:Peter_Luschny/SwissKnifePolynomials\"\u003eSwiss-Knife Polynomials and Euler Numbers.\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Bernoulli_number\"\u003eBernoulli number.\u003c/a\u003e",
				"J. Worpitzky, \u003ca href=\"http://gdz.sub.uni-goettingen.de/dms/resolveppn/?PPN=GDZPPN002158698\"\u003eStudien über die Bernoullischen und Eulerschen Zahlen\u003c/a\u003e, Journal für die reine und angewandte Mathematik, 94 (1883), 203-232."
			],
			"formula": [
				"T(n, k) = [x^(n-k)](skp(n,x+1)-skp(n,x-1))/2) where skp(n,x) are the Swiss-Knife polynomials A153641. - _Peter Luschny_, Jul 23 2012",
				"E.g.f. exp(x*t)tanh(t) = 0*(t^0/0!) + 1*(t^1/1!) + (2*x)*(t^2/2!) + (3*x^2-2)*(t^3/3!) + ...",
				"V_n(x) = -x^n + Sum_{k=0..n} C(n,k)*Euler(k)*(x+1)^(n-k)."
			],
			"maple": [
				"# Polynomials V_n(x):",
				"V := proc(n,x) local k,pow; pow := (n,k) -\u003e `if`(n=0 and k=0,1,n^k); add(binomial(n,k)*euler(k)*pow(x+1,n-k),k=0..n) - pow(x,n) end:",
				"# Coefficients a(n):",
				"seq(print(seq(coeff(n!*coeff(series(exp(x*t)*tanh(t),t,16),t,n),x,n-k),k=0..n)),n=0..8);"
			],
			"mathematica": [
				"skp[n_, x_] := Sum[Binomial[n, k]*EulerE[k]*x^(n-k), {k, 0, n}]; v[n_, x_] := (skp[n, x+1]-skp[n, x-1])/2; t[n_, k_] := Coefficient[v[n, x], x, k]; Table[t[n, k], {n, 0, 10}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Jan 09 2014 *)"
			],
			"program": [
				"(Sage)",
				"R = PolynomialRing(QQ, 'x')",
				"@CachedFunction",
				"def skp(n, x) : # Swiss-Knife polynomials A153641.",
				"    if n == 0 : return 1",
				"    return add(skp(k, 0)*binomial(n, k)*(x^(n-k)-(n+1)%2) for k in range(n)[::2])",
				"def A162660(n,k) : return 0 if k \u003e n else R((skp(n, x+1)-skp(n, x-1))/2)[k]",
				"matrix(ZZ, 9, A162660) # _Peter Luschny_, Jul 23 2012"
			],
			"xref": [
				"V_n(k), n=0, 1, .., k=0: A155585, k=1: A009832,",
				"V_n(k), k=0, 1, .., V_0: A000004, V_1: A000012, V_2: A005843, V_3: A100536.",
				"Cf. A153641, A154341, A154342, A154343, A154344, A154345."
			],
			"keyword": "sign,tabl",
			"offset": "0,5",
			"author": "_Peter Luschny_, Jul 09 2009",
			"references": 6,
			"revision": 18,
			"time": "2018-09-26T03:04:22-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}