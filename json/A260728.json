{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A260728",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 260728,
			"data": "0,0,0,1,0,0,1,1,0,2,0,1,1,0,1,1,0,0,2,1,0,1,1,1,1,0,0,3,1,0,1,1,0,1,0,1,2,0,1,1,0,0,1,1,1,2,1,1,1,2,0,1,0,0,3,1,1,1,0,1,1,0,1,3,0,0,1,1,0,1,1,1,2,0,0,1,1,1,1,1,0,4,0,1,1,0,1,1,1,0,2,1,1,1,1,1,1,0,2,3,0,0,1,1,0,1,0,1,3,0,1,1,1,0,1,1,0,2,1,1,1",
			"name": "Bitwise-OR of the exponents of all 4k+3 primes in the prime factorization of n.",
			"comment": [
				"A001481 (numbers that are the sum of 2 squares) gives the positions of even terms in this sequence, while its complement A022544 (numbers that are not the sum of 2 squares) gives the positions of odd terms.",
				"If instead of bitwise-oring (A003986) we added in ordinary way the exponents of 4k+3 primes together, we would get the sequence A065339. For the positions where these two sequences differ see A260730."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A260728/b260728.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"formula": [
				"If n \u003c 3, a(n) = 0; thereafter, for any even n: a(n) = a(n/2), for any n with its smallest prime factor (A020639) of the form 4k+1: a(n) = a(A032742(n)), otherwise [when A020639(n) is of the form 4k+3] a(n) = A003986(A067029(n),a(A028234(n))).",
				"Other identities. For all n \u003e= 0:",
				"A229062(n) = 1 - A000035(a(n)). [Reduced modulo 2 and complemented, the sequence gives the characteristic function of A001481.]",
				"a(n) = a(A097706(n)). [The result depends only on the prime factors of the form 4k+3.]",
				"a(n) = A267116(A097706(n)).",
				"a(n) = A267113(A267099(n))."
			],
			"example": [
				"For n = 21 = 3^1 * 7^1 we compute A003986(1,1) = 1, thus a(21) = 1.",
				"For n = 63 = 3^2 * 7^1 we compute A003986(2,1) = A003986(1,2) = 3, thus a(63) = 3."
			],
			"mathematica": [
				"Table[BitOr @@ (Map[Last, FactorInteger@ n /. {p_, _} /; MemberQ[{0, 1, 2}, Mod[p, 4]] -\u003e Nothing]), {n, 0, 120}] (* _Michael De Vlieger_, Feb 07 2016 *)"
			],
			"program": [
				"(Scheme) (define (A260728 n) (cond ((\u003c n 3) 0) ((even? n) (A260728 (/ n 2))) ((= 1 (modulo (A020639 n) 4)) (A260728 (A032742 n))) (else (A003986bi (A067029 n) (A260728 (A028234 n)))))) ;; A003986bi implements bitwise-or (see A003986)."
			],
			"xref": [
				"Cf. A000035, A001481, A022544, A003986, A020639, A028234, A032742, A067029, A097706, A229062, A260730.",
				"Cf. also A267113, A267116, A267099.",
				"Differs from A065339 for the first time at n=21, where a(21) = 1, while A065339(21)=2."
			],
			"keyword": "nonn",
			"offset": "0,10",
			"author": "_Antti Karttunen_, Aug 12 2015",
			"references": 8,
			"revision": 27,
			"time": "2016-02-07T20:14:21-05:00",
			"created": "2015-08-14T12:57:43-04:00"
		}
	]
}