{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A082245",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 82245,
			"data": "1,3,10,73,626,8052,117650,2113665,43053283,1001953638,25937424602,743375541244,23298085122482,793811662272744,29192932133689220,1152956690052710401,48661191875666868482,2185928253847184914509",
			"name": "Sum of (n-1)-th powers of divisors of n.",
			"comment": [
				"a(n) = t(n,n-1), t as defined in A082771;",
				"a(1)=A000005(1), a(2)=A000203(2), a(3)=A001157(3), a(4)=A001158(4), a(5)=A001159(5), a(6)=A001160(6), a(7)=A013954(7), a(8)=A013955(8)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A082245/b082245.txt\"\u003eTable of n, a(n) for n = 1..387\u003c/a\u003e",
				"M. Sugunamma, \u003ca href=\"http://doi.org/10.4064/ap-8-2-173-176\"\u003eCertain results concerning sigma_k(n) and phi_k(n)\u003c/a\u003e, Annales Polonici Mathematici, Vol. 8, No. 2 (1960), pp. 173-176.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DivisorFunction.html\"\u003eDivisor Function\u003c/a\u003e."
			],
			"formula": [
				"G.f.: Sum_{k\u003e=1} k^(k-1)*x^k/(1 - (k*x)^k). - _Ilya Gutkovskiy_, Nov 02 2018",
				"L.g.f.: -log(Product_{k\u003e=1} (1 - (k*x)^k)^(1/k^2)) = Sum_{k\u003e=1} a(k)*x^k/k. - _Seiichi Manyama_, Jun 23 2019",
				"Limit_{n-\u003eoo} a(n)/A023887(n-1) = e (A001113) (Sugunamma, 1960). - _Amiram Eldar_, Apr 15 2021"
			],
			"example": [
				"a(6) = 1^5 + 2^5 + 3^5 + 6^5 = 1 + 32 + 243 + 7776 = 8052."
			],
			"mathematica": [
				"Table[Total[Divisors[n]^(n-1)], {n,18}] (* _T. D. Noe_, Oct 25 2006 *)",
				"Table[DivisorSigma[n-1,n], {n,1,20}] (* _G. C. Greubel_, Nov 02 2018 *)"
			],
			"program": [
				"(Sage) [sigma(n,(n-1))for n in range(1,19)] # _Zerinvary Lajos_, Jun 04 2009",
				"(PARI) a(n) = sigma(n, n-1); \\\\ _Michel Marcus_, Nov 07 2017",
				"(PARI) N=20; x='x+O('x^N); Vec(x*deriv(-log(prod(k=1, N, (1-(k*x)^k)^(1/k^2))))) \\\\ _Seiichi Manyama_, Jun 23 2019",
				"(MAGMA) [DivisorSigma(n-1, n): n in [1..20]]; // _G. C. Greubel_, Nov 02 2018"
			],
			"xref": [
				"Cf. A023887, A000005, A000203, A001157, A001158, A001159, A001160, A013954, A013955, A013956, A013957, A013958",
				"Cf. A013959, A013960, A013961, A013962, A013963, A013964, A013965, A013966, A013967, A013968, A013969, A013970",
				"Cf. A001113, A013971, A013972."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Reinhard Zumkeller_, May 22 2003",
			"ext": [
				"Corrected by _T. D. Noe_, Oct 25 2006"
			],
			"references": 6,
			"revision": 33,
			"time": "2021-04-15T05:15:00-04:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}