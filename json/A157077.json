{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A157077",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 157077,
			"data": "1,0,2,-2,0,6,0,-12,0,20,6,0,-60,0,70,0,60,0,-280,0,252,-20,0,420,0,-1260,0,924,0,-280,0,2520,0,-5544,0,3432,70,0,-2520,0,13860,0,-24024,0,12870,0,1260,0,-18480,0,72072,0,-102960,0,48620,-252,0,13860,0,-120120,0,360360,0,-437580,0,184756",
			"name": "Triangle read by rows, coefficients of the Legendre polynomials P(n, x) times 2^n: T(n, k) = 2^n * [x^k] P(n, x), n\u003e=0, 0\u003c=k\u003c=n.",
			"link": [
				"Paul W. Haggard, \u003ca href=\"https://doi.org/10.1155/S0161171288000481\"\u003eSome applications of Legendre numbers\u003c/a\u003e, International Journal of Mathematics and Mathematical Sciences, vol. 11, Article ID 538097, 8 pages, 1988. See Table 3 p. 412.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/LegendrePolynomial.html\"\u003eLegendre Polynomial\u003c/a\u003e"
			],
			"formula": [
				"Row sums are 2^n.",
				"From _Peter Luschny_, Dec 19 2014 (Start):",
				"T(n,0) = A126869(n).",
				"T(n,n) = A000984(n).",
				"T(n,1) = (-1)^floor(n/2)*A005430(floor(n/2)+1) if n is odd else 0.",
				"Let Q(n, x) = 2^n*P(n, x).",
				"Q(n,0) = (-1)^floor(n/2)*A126869(floor(n/2)) if n is even else 0.",
				"Q(n,1) = A000079(n).",
				"Q(n,2) = A069835(n).",
				"Q(n,3) = A084773(n).",
				"Q(n,4) = A098269(n).",
				"Q(n,5) = A098270(n). (End)"
			],
			"example": [
				"The term order is Q(x) = a_0+a_1*x+...+a_n*x^n. The coefficients of the first few polynomials in this order are:",
				"{1},",
				"{0, 2},",
				"{-2, 0, 6},",
				"{0, -12, 0, 20},",
				"{6, 0, -60, 0, 70},",
				"{0, 60, 0, -280, 0, 252},",
				"{-20, 0, 420, 0, -1260, 0, 924},",
				"{0, -280, 0, 2520, 0, -5544, 0, 3432},",
				"{70, 0, -2520, 0, 13860, 0, -24024, 0, 12870},",
				"{0, 1260, 0, -18480, 0, 72072, 0, -102960, 0, 48620},",
				"{-252, 0, 13860, 0, -120120, 0, 360360, 0, -437580, 0, 184756}."
			],
			"maple": [
				"with(orthopoly):with(PolynomialTools): seq(print(CoefficientList (2^n*P(n, x), x,termorder=forward)),n=0..10); # _Peter Luschny_, Dec 18 2014"
			],
			"mathematica": [
				"Table[CoefficientList[2^n*LegendreP[n, x], x], {n, 0, 10}]; Flatten[%]"
			],
			"program": [
				"(PARI) tabl(nn) = for (n=0, nn, print(Vecrev(2^n*pollegendre(n)))); \\\\ _Michel Marcus_, Dec 18 2014",
				"(Sage)",
				"def A157077_row(n):",
				"    if n==0: return [1]",
				"    T = [c[0] for c in (2^n*gen_legendre_P(n, 0, x)).coefficients()]",
				"    return [0 if is_odd(n+k) else T[k//2] for k in (0..n)]",
				"for n in range(9): print(A157077_row(n)) # _Peter Luschny_, Dec 19 2014"
			],
			"xref": [
				"Cf. A100258, A126869, A000984, A005430, A000079, A069835, A084773, A098269, A098270."
			],
			"keyword": "tabl,sign",
			"offset": "0,3",
			"author": "_Roger L. Bagula_, Feb 22 2009",
			"ext": [
				"Name clarified and edited by _Peter Luschny_, Dec 18 2014"
			],
			"references": 0,
			"revision": 28,
			"time": "2021-03-15T08:35:16-04:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}