{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A334930",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 334930,
			"data": "1,11,13,91,109,731,877,5851,7021,46811,56173,374491,449389,2995931,3595117,23967451,28760941,191739611,230087533,1533916891,1840700269,12271335131,14725602157,98170681051,117804817261,785365448411,942438538093,6282923587291,7539508304749",
			"name": "Numbers that generate rotationally symmetrical XOR-triangles featuring singleton zero bits in a hexagonal arrangement.",
			"comment": [
				"Subset of A334556.",
				"No zero appears in the center of the figure, thus a(n) does not intersect A334769.",
				"Numbers m with A070939(m) (mod 3) = 1 involving alternating run lengths of a singleton zero separated by a pair of 1s in the binary expansion, admitting an initial or final singleton 1."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A334930/b334930.txt\"\u003eTable of n, a(n) for n = 1..2215\u003c/a\u003e",
				"Michael De Vlieger, \u003ca href=\"/A334930/a334930.png\"\u003eDiagram montage\u003c/a\u003e of XOR-triangles resulting from a(n) with 2 \u003c= n \u003c= 33.",
				"Michael De Vlieger, \u003ca href=\"http://vincico.com/seq/a334769.html\"\u003eCentral zero-triangles in rotationally symmetrical XOR-Triangles\u003c/a\u003e, 2020.",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e",
				"\u003ca href=\"/index/X#XOR-triangles\"\u003eIndex entries for sequences related to XOR-triangles\u003c/a\u003e"
			],
			"formula": [
				"G.f.: (1 + 11*x + 4*x^2 - 8*x^3)/(1 - 9*x^2 + 8*x^4).",
				"a(n) = - (4/7) - (1/7)*(-1)^(n-1) + ((6 + 10*sqrt(2))/7)*(2*sqrt(2))^(n-1) + ((6 - 10*sqrt(2))/7)*(-2*sqrt(2))^(n-1) - _Alejandro J. Becerra Jr._, May 31 2020"
			],
			"example": [
				"Diagrams of a(2)-a(5), replacing “0” with “.” and “1” with “@” for clarity:",
				"     a(2)=11            a(3)=13",
				"     @ . @ @            @ @ . @",
				"      @ @ .              . @ @",
				"       . @                @ .",
				"        @                  @",
				".",
				"    a(4) = 91          a(5) = 109",
				"  @ . @ @ . @ @      @ @ . @ @ . @",
				"   @ @ . @ @ .        . @ @ . @ @",
				"    . @ @ . @          @ . @ @ .",
				"     @ . @ @            @ @ . @",
				"      @ @ .              . @ @",
				"       . @                @ .",
				"        @                  @"
			],
			"mathematica": [
				"CoefficientList[Series[(1 + 11 x + 4 x^2 - 8 x^3)/(1 - 9 x^2 + 8 x^4), {x, 0, 28}], x]",
				"(* Generate a textual plot of XOR-triangle T(n) *)",
				"xortri[n_Integer] := TableForm@ MapIndexed[StringJoin[ConstantArray[\" \", First@ #2 - 1], StringJoin @@ Riffle[Map[If[# == 0, \".\" (*0*), \"@\" (*1*)] \u0026, #1], \" \"]] \u0026, NestWhileList[Map[BitXor @@ # \u0026, Partition[#, 2, 1]] \u0026, IntegerDigits[n, 2], Length@ # \u003e 1 \u0026]]"
			],
			"xref": [
				"Cf. A334556, A334769, A334931, A334932."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Michael De Vlieger_, May 16 2020",
			"references": 3,
			"revision": 20,
			"time": "2020-07-03T23:34:05-04:00",
			"created": "2020-05-23T23:57:36-04:00"
		}
	]
}