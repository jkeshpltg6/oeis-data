{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A306460",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 306460,
			"data": "1,2,1,1,2,2,3,3,1,2,4,3,2,1,2,6,5,1,3,3,4,6,1,1,4,6,3,3,5,3,6,4,2,3,5,5,4,6,4,2,5,5,3,5,2,6,7,3,5,5,7,5,4,2,5,8,5,3,2,6,6,4,5,5,6,7,5,5,4,6,9,6,6,5,1,7,8,3,2,6,7,5,6,5,7,8,5,3,2,6,10,6,8,7,7,5,4,6,5,5",
			"name": "Number of ways to write n as x*(2x-1) + y*(y+1)/2 + z*(z+1)*(z+2)/6, where x,y,z are nonnegative integers with x \u003e 0.",
			"comment": [
				"Conjecture 1: a(n) \u003e 0 for all n \u003e 0. In other words, any positive integer n can be written as the sum of a positive hexagonal number, a triangular number and a tetrahedral number.",
				"We have verified a(n) \u003e 0 for all n = 1..10^7.",
				"Conjecture 2: Let c be 1 or 3. Then each n = 0,1,... can be written as c*x(x+1) + y*(y+1)/2 + z*(z+1)*(z+2)/6 with x,y,z nonnegative integers.",
				"Conjecture 3: Let t(x) = x*(x+1)*(x+2)/6. Then each n = 0,1,... can be written as 2*t(w) + t(x) + t(y) + t(z) with w,x,y,z nonnegative integers.",
				"We have verified Conjecture 3 for all n = 0..2*10^5. Clearly, Conjecture 3 implies Pollock's conjecture which states that any natural number is the sum of five tetrahedral numbers."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A306460/b306460.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/TetrahedralNumber.html\"\u003eTetrahedral Number\u003c/a\u003e"
			],
			"example": [
				"a(3) = 1 with 3 = 1*(2*1-1) + 1*2/2 + 1*2*3/6.",
				"a(14) = 1 with 14 = 1*(2*1-1) + 2*3/2 + 3*4*5/6.",
				"a(75) = 1 with 75 = 5*(2*5-1) + 4*5/2 + 4*5*6/2.",
				"a(349) = 1 with 349 = 5*(2*5-1) + 24*25/2 + 2*3*4/6.",
				"a(369) = 1 with 369 = 4*(2*4-1) + 10*11/2 + 11*12*13/6.",
				"a(495) = 1 with 495 = 8*(2*8-1) + 20*21/2 + 9*10*11/6.",
				"a(642) = 1 with 642 = 16*(2*16-1) + 16*17/2 + 3*4*5/6."
			],
			"mathematica": [
				"f[n_]:=f[n]=n(n+1)(n+2)/6;",
				"TQ[n_]:=TQ[n]=IntegerQ[Sqrt[8n+1]];",
				"tab={};Do[r=0;Do[If[f[z]\u003e=n, Goto[aa]]; Do[If[TQ[n-f[z]-x(2x-1)],r=r+1],{x,1,(Sqrt[8(n-f[z])+1]+1)/4}];Label[aa],{z,0,n}];tab=Append[tab,r],{n,1,100}];Print[tab]"
			],
			"xref": [
				"Cf. A000217, A000292, A000384, A000797, A104246, A262813, A306462, A306471."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Zhi-Wei Sun_, Feb 17 2019",
			"references": 8,
			"revision": 18,
			"time": "2019-02-18T14:28:39-05:00",
			"created": "2019-02-18T14:28:39-05:00"
		}
	]
}