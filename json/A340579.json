{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A340579",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 340579,
			"data": "1,3,2,4,6,4,7,8,12,7,6,14,16,21,12,12,12,28,28,36,19,8,24,24,49,48,57,30,15,16,48,42,84,76,90,45,13,30,32,84,72,133,120,135,67,18,26,60,56,144,114,210,180,201,97,12,36,52,105,96,228,180,315,268,291,139,28,24,72,91",
			"name": "Triangle read by rows: T(n,k) = A000203(n-k+1)*A000070(k-1), 1 \u003c= k \u003c= n.",
			"comment": [
				"Consider a symmetric tower (a polycube) in which the terraces are the symmetric representation of sigma (n..1) respectively starting from the base (cf. A237270, A237593). The total area of the terraces equals A024916(n), the same as the area of the base.",
				"The levels of the terraces starting from the base are the first n terms of A000070, that is A000070(0)..A000070(n-1), hence the differences between two successive levels give the partition numbers A000041, that is A000041(0)..A000041(n-1).",
				"T(n,k) is the total volume (or total number of cubes) exactly below the symmetric representation of sigma(n-k+1). In other words: T(n,k) is the total volume (the total number of cubes) exactly below the terraces that are in the k-th level that contains terraces starting from the base.",
				"This symmetric tower has the property that its volume (the total number of cubes) equals A182738(n), the sum of all parts of all partitions of all positive integers \u003c= n. That is due to the correspondence between divisors and partitions (cf. A336811).",
				"The growth of the volume represents the convolution of A000203 and A000070.",
				"The symmetric tower is a member of the family of the pyramid described in A245092.",
				"For another symmetric tower of the same family and whose volume equals A066186(n) see A221529 and A339106."
			],
			"example": [
				"Triangle begins:",
				"   1;",
				"   3,   2;",
				"   4,   6,   4;",
				"   7,   8,  12,   7;",
				"   6,  14,  16,  21,  12;",
				"  12,  12,  28,  28,  36,  19;",
				"   8,  24,  24,  49,  48,  57,  30;",
				"  15,  16,  48,  42,  84,  76,  90,  45;",
				"  13,  30,  32,  84,  72, 133, 120, 135,  67;",
				"  18,  26,  60,  56, 144, 114, 210, 180, 201,  97;",
				"  12,  36,  52, 105,  96, 228, 180, 315, 268, 291, 139;",
				"...",
				"For n = 6 the calculation of every term of row 6 is as follows:",
				"-------------------------",
				"k   A000070        T(6,k)",
				"1      1  *  12  =   12",
				"2      2  *  6   =   12",
				"3      4  *  7   =   28",
				"4      7  *  4   =   28",
				"5     12  *  3   =   36",
				"6     19  *  1   =   19",
				".         A000203",
				"-------------------------",
				"The sum of row 6 is 12 + 12 + 28 + 28 + 36 + 19 = 135, equaling A182738(6)."
			],
			"program": [
				"(PARI) row(n) = vector(n, k, sigma(n-k+1)*sum(i=0, k-1, numbpart(i))); \\\\ _Michel Marcus_, Jul 23 2021"
			],
			"xref": [
				"Row sums give A182738.",
				"Cf. A000070, A000203, A024916, A221529, A221531, A237593, A339106, A340424, A340426, A340524, A340525, A340526, A340527, A340531."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Omar E. Pol_, Jan 12 2021",
			"references": 2,
			"revision": 48,
			"time": "2021-07-27T01:39:27-04:00",
			"created": "2021-01-19T05:55:02-05:00"
		}
	]
}