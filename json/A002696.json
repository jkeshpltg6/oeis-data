{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002696",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2696,
			"id": "M4532 N1921",
			"data": "1,8,45,220,1001,4368,18564,77520,319770,1307504,5311735,21474180,86493225,347373600,1391975640,5567902560,22239974430,88732378800,353697121050,1408831480056,5608233007146,22314239266528,88749815264600",
			"name": "Binomial coefficients C(2n,n-3).",
			"comment": [
				"Number of lattice paths from (0,0) to (n,n) with steps E=(1,0) and N=(0,1) which touch or cross the line x-y=3. - _Herbert Kociemba_, May 23 2004"
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), p. 828.",
				"C. Lanczos, Applied Analysis. Prentice-Hall, Englewood Cliffs, NJ, 1956, p. 517.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A002696/b002696.txt\"\u003eTable of n, a(n) for n = 3..1497\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"A. Claesson and T. Mansour, \u003ca href=\"http://arxiv.org/abs/math/0110036\"\u003eCounting patterns of type (1,2) or (2,1)\u003c/a\u003e, arXiv:math/0110036 [math.CO], 2001.",
				"Milan Janjic, \u003ca href=\"http://www.pmfbl.org/janjic/\"\u003eTwo Enumerative Functions\u003c/a\u003e",
				"C. Lanczos, \u003ca href=\"/A002457/a002457.pdf\"\u003eApplied Analysis\u003c/a\u003e (Annotated scans of selected pages)",
				"R. Parviainen, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL9/Parviainen/parviainen3.html\"\u003eLattice Path Enumeration of Permutations with k Occurrences of the Pattern 2-13\u003c/a\u003e, Journal of Integer Sequences, Vol. 9 (2006), Article 06.3.2.",
				"Franck Ramaharo, \u003ca href=\"https://arxiv.org/abs/1802.07701\"\u003eStatistics on some classes of knot shadows\u003c/a\u003e, arXiv:1802.07701 [math.CO], 2018.",
				"Hermann Stamm-Wilbrandt, \u003ca href=\"/A002696/a002696.gif\"\u003eCompute C(2n, n-k) based on C(n,...) animation\u003c/a\u003e",
				"Daniel W. Stasiuk, \u003ca href=\"https://harvest.usask.ca/bitstream/handle/10388/11865/STASIUK-THESIS-2019.pdf\"\u003eAn Enumeration Problem for Sequences of n-ary Trees Arising from Algebraic Operads\u003c/a\u003e, Master's Thesis, University of Saskatchewan-Saskatoon (2018)."
			],
			"formula": [
				"G.f.: (1-sqrt(1-4*z))^6/(64*z^3*sqrt(1-4*z)). - _Emeric Deutsch_, Jan 28 2004",
				"a(n) = Sum_{k=0..n} C(n, k)*C(n, k+3). - _Hermann Stamm-Wilbrandt_, Aug 17 2015",
				"From _Robert Israel_, Aug 19 2015: (Start)",
				"(n-2)*(n+4)*a(n+1) = (2*n+2)*(2*n+1)*a(n).",
				"E.g.f.: I_3(2*x)*exp(2*x) where I_3 is a modified Bessel function. (End)"
			],
			"maple": [
				"A002696:=n-\u003ebinomial(2*n,n-3): seq(A002696(n), n=3..30); # _Wesley Ivan Hurt_, Aug 19 2015"
			],
			"mathematica": [
				"CoefficientList[Series[64/(((Sqrt[1-4x] +1)^6)*Sqrt[1-4x]), {x,0,30}], x] (* _Robert G. Wilson v_, Aug 08 2011 *)"
			],
			"program": [
				"(MAGMA) [ Binomial(2*n,n-3): n in [3..30] ]; // _Vincenzo Librandi_, Apr 13 2011",
				"(PARI) a(n)=binomial(n+n,n-3) \\\\ _Charles R Greathouse IV_, Aug 08 2011",
				"(Sage) [binomial(2*n, n-3) for n in (3..30)] # _G. C. Greubel_, Mar 21 2019",
				"(GAP) List([3..30], n-\u003e Binomial(2*n, n-3)) # _G. C. Greubel_, Mar 21 2019"
			],
			"xref": [
				"Diagonal 7 of triangle A100257.",
				"Column k=1 of A263776."
			],
			"keyword": "nonn,easy",
			"offset": "3,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Emeric Deutsch_, Feb 18 2004"
			],
			"references": 7,
			"revision": 64,
			"time": "2019-03-22T00:30:51-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}