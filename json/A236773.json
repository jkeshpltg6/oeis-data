{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A236773",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 236773,
			"data": "0,1,6,16,33,59,96,145,210,292,393,515,660,829,1026,1252,1509,1799,2124,2485,2886,3328,3813,4343,4920,5545,6222,6952,7737,8579,9480,10441,11466,12556,13713,14939,16236,17605,19050,20572,22173,23855,25620,27469",
			"name": "a(n) = n + floor( n^2/2 + n^3/3 ).",
			"comment": [
				"This sequence follows A074148 and A042965, A236771.",
				"The prime terms are 59, 829, 14939, 35759, 93719, 132409, 155219, 290399, 414179, 487463, ... .",
				"If a(k) is prime then k == 1, 5, 7 or 11 (mod 12).",
				"Third differences: 1, 2, 2, 2, 1, 4 repeated (unsigned terms of A181982).",
				"Fourth differences: 1, 0, 0, -1, 3, -3 repeated (see A131193)."
			],
			"link": [
				"Bruno Berselli, \u003ca href=\"/A236773/b236773.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_09\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1,0,0,1,-3,3,-1)."
			],
			"formula": [
				"G.f.: x*(1+3*x+x^2+2*x^3+2*x^4+2*x^5+x^7) / ((1+x)*(1-x+x^2)*(1+x+x^2)*(1-x)^4).",
				"a(n) = 3*a(n-1) -3*a(n-2) +a(n-3) +a(n-6) -3*a(n-7) +3*a(n-8) -a(n-9).",
				"Also, for h\u003e=0:",
				"a(6h)   = 6*h*( 12*h^2 + 3*h + 1 ),",
				"a(6h+1) = 72*h^3 + 54*h^2 + 18*h + 1,",
				"a(6h+2) = 6*( 4*h + 1 )*( 3*h^2 + 3*h + 1 ),",
				"a(6h+3) = 2*( 36*h^3 + 63*h^2 + 39*h + 8 ),",
				"a(6h+4) = 3*( 24*h^3 + 54*h^2 + 42*h + 11 ),",
				"a(6h+5) = 72*h^3 + 198*h^2 + 186*h + 59."
			],
			"maple": [
				"seq(n+floor(n^2/2+n^3/3),n=0..43); # _Paolo P. Lava_, Aug 24 2018"
			],
			"mathematica": [
				"Table[n + Floor[n^2/2 + n^3/3], {n, 0, 50}]",
				"CoefficientList[Series[x (1 + 3 x + x^2 + 2 x^3 + 2 x^4 + 2 x^5 + x^7)/((1 + x) (1 - x + x^2) (1 + x + x^2) (1 - x)^4), {x, 0, 50}], x] (* _Vincenzo Librandi_, Feb 08 2014 *)"
			],
			"program": [
				"(MAGMA) [n+Floor(n^2/2+n^3/3): n in [0..50]];",
				"(MAGMA) I:=[0,1,6,16,33,59,96,145,210]; [n le 9 select I[n] else 3*Self(n-1)-3*Self(n-2)+Self(n-3)+Self(n-6)-3*Self(n-7)+3*Self(n-8)-Self(n-9): n in [1..50]]; // _Vincenzo Librandi_, Feb 08 2014",
				"(PARI) vector(60, n, n--; n+floor(n^2/2 +n^3/3)) \\\\ _G. C. Greubel_, Aug 12 2018"
			],
			"xref": [
				"Cf. A074148: n+floor(n^2/2).",
				"Cf. A042965: n+floor(1/2+n/3); A236771: n+floor(n/2+n^2/3).",
				"Cf. A236772: floor(sum(i=1..n, n^i/i))."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Bruno Berselli_, Feb 07 2014",
			"references": 2,
			"revision": 30,
			"time": "2018-08-24T06:37:52-04:00",
			"created": "2014-02-07T10:15:34-05:00"
		}
	]
}