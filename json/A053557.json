{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A053557",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 53557,
			"data": "1,0,1,1,3,11,53,103,2119,16687,16481,1468457,16019531,63633137,2467007773,34361893981,15549624751,8178130767479,138547156531409,92079694567171,4282366656425369,72289643288657479,6563440628747948887,39299278806015611311",
			"name": "Numerator of Sum_{k=0..n} (-1)^k/k!.",
			"comment": [
				"Numerator of probability of a derangement of n things (A000166(n)/n! or !n/n!).",
				"Also denominators of successive convergents to e using continued fraction 2 +1/(1 +1/(2 +2/(3 +3/(4 +4/(5 +5/(6 +6/(7 +7/(8 +...)))))))."
			],
			"reference": [
				"L. Lorentzen and H. Waadeland, Continued Fractions with Applications, North-Holland 1992, p. 562.",
				"E. Maor, e: The Story of a Number, Princeton Univ. Press 1994, pp. 151 and 157."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A053557/b053557.txt\"\u003eTable of n, a(n) for n = 0..450\u003c/a\u003e (terms 0..100 from T. D. Noe)",
				"Leonhardo Eulero, \u003ca href=\"http://gallica.bnf.fr/ark:/12148/bpt6k69587\"\u003eIntroductio in analysin infinitorum. Tomus primus\u003c/a\u003e, Lausanne, 1748.",
				"L. Euler, Introduction à l'analyse infinitésimale, \u003ca href=\"http://gallica.bnf.fr/ark:/12148/bpt6k3884z\"\u003eTome premier\u003c/a\u003e, \u003ca href=\"http://gallica.bnf.fr/ark:/12148/bpt6k38858\"\u003eTome second\u003c/a\u003e, trad. du latin en français par J. B. Labey, Paris, 1796-1797.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ContinuedFractionConstants.html\"\u003eContinued Fraction Constants\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GeneralizedContinuedFraction.html\"\u003eGeneralized Continued Fraction\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Subfactorial.html\"\u003eSubfactorial\u003c/a\u003e"
			],
			"formula": [
				"Let exp(-x)/(1-x) = Sum_{n \u003e= 0} (a_n/b_n)*x^n. Then sequence a_n is A053557. - Aleksandar Petojevic (apetoje(AT)ptt.yu), Apr 14 2004"
			],
			"example": [
				"Sum_{k=0..n} (-1)^k/k! = {1, 0, 1/2, 1/3, 3/8, 11/30, 53/144, 103/280, 2119/5760, ...} for n \u003e= 0."
			],
			"mathematica": [
				"Numerator[CoefficientList[Series[Exp[-x]/(1-x), {x, 0, 30}], x]] (* _Jean-François Alcover_, Nov 18 2011 *)",
				"Table[Numerator[Sum[(-1)^k/k!,{k,0,n}]],{n,0,30}] (* _Harvey P. Dale_, Dec 02 2011 *)",
				"Join[{1, 0}, Numerator[RecurrenceTable[{a[n]==a[n-1]+a[n-2]/(n-2), a[1] ==0, a[2]==1}, a, {n,2,30}]]] (* _Terry D. Grant_, May 07 2017; corrected by _G. C. Greubel_, May 16 2019 *)"
			],
			"program": [
				"(PARI) for(n=0, 30, print1(numerator(sum(k=0,n, (-1)^k/k!)), \", \")) \\\\ _G. C. Greubel_, Nov 05 2017",
				"(MAGMA) [Numerator( (\u0026+[(-1)^k/Factorial(k): k in [0..n]]) ): n in [0..30]]; // _G. C. Greubel_, May 16 2019",
				"(Sage) [numerator(sum((-1)^k/factorial(k) for k in (0..n))) for n in (0..30)] # _G. C. Greubel_, May 16 2019"
			],
			"xref": [
				"Cf. A000166/A000142, A053556 (denominators), A053518-A053520. See also A103816.",
				"a(n) = (N(n, n) of A103361), A053557/A053556 = A000166/n! = (N(n, n) of A103361)/(D(n, n) of A103360), Cf. A053518-A053520."
			],
			"keyword": "nonn,frac,nice,easy",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_, Jan 17 2000",
			"ext": [
				"More terms from _Vladeta Jovovic_, Mar 31 2000"
			],
			"references": 13,
			"revision": 48,
			"time": "2019-05-16T21:19:35-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}