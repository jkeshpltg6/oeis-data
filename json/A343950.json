{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A343950",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 343950,
			"data": "0,0,0,1,1,0,0,3,1,2,2,2,3,1,4,3,2,3,3,4,4,2,1,4,6,4,2,3,12,5,3,5,8,4,5,5,8,4,7,4,4,4,7,5,5,1,4,6,5,6,6,10,7,4,9,5,10,16,7,7,9,6,5,5,14,8,6,6,3,7,1,5,4,10,5,7,10,8,13,10,3,4,8,5,12,7,20,9,12,5,8,1,9,4,8,9,8,7,4,10",
			"name": "Number of ways to write n as x + y + z with x^2 + 4*y^2 + 5*z^2 a square, where x,y,z are positive integers with y or z a positive power of two.",
			"comment": [
				"Conjecture 1: a(n) \u003e 0 for all n \u003e 7.",
				"We have verified a(n) \u003e 0 for all n = 8..50000. Clearly, a(2*n) \u003e 0 if a(n) \u003e 0.",
				"Conjecture 2: For any integer n \u003e 7, we can write n as x + y + z with x,y,z positive integers such that x^2 + 2*y^2 + 3*z^2 is a square.",
				"Conjecture 3: For any integer n \u003e 4, we can write n as x + y + z with x,y,z positive integers such that 3*x^2 + 4*y^2 + 5*z^2 (or x^2 + 3*y^2 + 5*z^2) is a square."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A343950/b343950.txt\"\u003eTable of n, a(n) for n = 1..3200\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2016.11.008\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, J. Number Theory 175(2017), 167-190. See also \u003ca href=\"http://arxiv.org/abs/1604.06723\"\u003earXiv:1604.06723 [math.NT]\u003c/a\u003e."
			],
			"example": [
				"a(4) = 1, and 4 = 1 + 1 + 2 with 1^2 + 4*1^2 + 5*2^2 = 5^2.",
				"a(5) = 1, and 5 = 2 + 2 + 1 with 2^2 + 4*2^2 + 5*1^2 = 5^2.",
				"a(9) = 1, and 9 = 4 + 1 + 4 with 4^2 + 4*1^2 + 5*4^2 = 10^2.",
				"a(14) = 1, and 14 = 7 + 5 + 2 with 7^2 + 4*5^2 + 5*2^2 = 13^2.",
				"a(23) = 1, and 23 = 7 + 8 + 8 with 7^2 + 4*8^2 + 5*8^2 = 25^2.",
				"a(46) = 1, and 46 = 14 + 16 + 16 with 14^2 + 4*16^2 + 5*16^2 = 50^2.",
				"a(71) = 1, and 71 = 42 + 8 + 21 with 42^2 + 4*8^2 + 5*21^2 = 65^2.",
				"a(92) = 1, and 92 = 28 + 32 + 32 with 28^2 + 4*32^2 + 5*32^2 = 100^2.",
				"a(142) = 1, and 142 = 84 + 16 + 42 with 84^2 + 4*16^2 + 5*42^2 = 130^2."
			],
			"mathematica": [
				"PowQ[n_]:=PowQ[n]=n\u003e1\u0026\u0026IntegerQ[Log[2,n]];",
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]];",
				"tab={};Do[r=0;Do[If[(PowQ[y]||PowQ[n-x-y])\u0026\u0026SQ[x^2+4*y^2+5*(n-x-y)^2],r=r+1],{x,1,n-3},{y,1,n-1-x}];tab=Append[tab,r],{n,1,100}];Print[tab]"
			],
			"xref": [
				"Cf. A000079, A000290, A271510, A271513, A271518, A230121, A230747."
			],
			"keyword": "nonn",
			"offset": "1,8",
			"author": "_Zhi-Wei Sun_, May 05 2021",
			"references": 3,
			"revision": 15,
			"time": "2021-05-06T04:41:37-04:00",
			"created": "2021-05-06T03:18:35-04:00"
		}
	]
}