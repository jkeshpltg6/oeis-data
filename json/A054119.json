{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A054119",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 54119,
			"data": "1,2,4,9,32,150,864,5880,46080,408240,4032000,43908480,522547200,6745939200,93884313600,1401079680000,22317642547200,377917892352000,6778983923712000,128403161542656000,2560949482291200000,53645489280294912000,1177524571957493760000,27027108408834293760000",
			"name": "a(n) = n! + (n-1)! + (n-2)!.",
			"comment": [
				"In factorial base representation (A007623) the terms are written as: 1, 10, 20, 111, 1110, 11100, 111000, ... From a(3) = 9 = \"111\" onward each term begins always with three consecutive 1's, followed by n-3 zeros. - _Antti Karttunen_, Sep 24 2016"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A054119/b054119.txt\"\u003eTable of n, a(n) for n = 0..300\u003c/a\u003e",
				"\u003ca href=\"/index/Fa#facbase\"\u003eIndex entries for sequences related to factorial base representation\u003c/a\u003e"
			],
			"formula": [
				"For n\u003e2, a(n) = (n-2)! * n^2. [_Gary Detlefs_, Aug 01 2009]",
				"a(n) = (n+1)!*(H(n-1)+H(n+1)-H(n-2)-H(n))/2, n\u003e1, where H(n) is the n-th harmonic number. [_Gary Detlefs_, Oct 04 2011]",
				"E.g.f.: x + 1/(1-x) - x*log(1-x) = x^2/G(0)/2 where G(k) = 1 + (k+2)/(x - x*(k+1)/(x + k + 1 - x^4/(x^3 +(k+2)*(k+3)/G(k+1)))); (continued fraction, 3rd kind, 4-step). - _Sergei N. Gladkovskii_, Jul 06 2012",
				"G.f. G(0) where G(k) = 1 - x/(1 + x/(1 - x - (k+1)/( k+1 - x/Q))); (continued fraction, 3rd kind, 4-step). - _Sergei N. Gladkovskii_, Jul 28 2012",
				"For n \u003e= 1, a(n) = A276940(n)/n. - _Antti Karttunen_, Sep 24 2016",
				"Sum_{n\u003e=2} 1/a(n) = A306770. - _Amiram Eldar_, Nov 19 2020"
			],
			"maple": [
				"seq(n! + (n+1)! + (n+2)!, n=0..30);"
			],
			"mathematica": [
				"Join[{1,2},Table[n!+(n+1)!+(n+2)!,{n,0,30}]] (* _Vladimir Joseph Stephan Orlovsky_, May 19 2011 *)"
			],
			"program": [
				"(MAGMA) [1,2],[Factorial(n)+Factorial(n-1)+Factorial(n-2): n in [2..20]]; // _Vincenzo Librandi_, Oct 05 2011",
				"(Scheme) (define (A054119 n) (if (\u003c= n 1) (+ 1 n) (+ (A000142 n) (A000142 (- n 1)) (A000142 (- n 2))))) ;; _Antti Karttunen_, Sep 24 2016"
			],
			"xref": [
				"Cf. A001048, A030495, A108217, A276940, A306770.",
				"Equals T(n, 3), array T as in A054115.",
				"Row 6 of A276955 (from a(3)=9 onward)."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Clark Kimberling_",
			"ext": [
				"Simpler definition from _Miklos Kristof_, Jun 16 2005",
				"More terms from _Antti Karttunen_, Sep 24 2016"
			],
			"references": 4,
			"revision": 45,
			"time": "2020-11-19T04:38:11-05:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}