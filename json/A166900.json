{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A166900",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 166900,
			"data": "1,1,1,2,4,1,9,21,9,1,64,156,84,16,1,630,1540,935,230,25,1,7916,19160,12480,3564,510,36,1,121023,288813,196623,61845,10465,987,49,1,2179556,5123608,3591560,1207696,228800,25864,1736,64,1,45179508,104657520",
			"name": "Triangle, read by rows, that transforms rows into diagonals in the table of coefficients of successive iterations of x+x^2 (cf. A122888).",
			"comment": [
				" Compare to the triangle A071207 that transforms rows into diagonals in the table of iterations of x/(1-x), where A071207(n,k) gives the number of labeled free trees with n vertices and k children of the root that have a label smaller than the label of the root. Does this triangle have a similar interpretation?"
			],
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A166900/b166900.txt\"\u003eTable of n, a(n) for n=0..495 (rows 0..30)\u003c/a\u003e"
			],
			"example": [
				"Triangle begins:",
				"1;",
				"1, 1;",
				"2, 4, 1;",
				"9, 21, 9, 1;",
				"64, 156, 84, 16, 1;",
				"630, 1540, 935, 230, 25, 1;",
				"7916, 19160, 12480, 3564, 510, 36, 1;",
				"121023, 288813, 196623, 61845, 10465, 987, 49, 1;",
				"2179556, 5123608, 3591560, 1207696, 228800, 25864, 1736, 64, 1;",
				"45179508, 104657520, 74847168, 26415840, 5426949, 695079, 56511, 2844, 81, 1;",
				"1059312264, 2420186616, 1755406674, 642448632, 140247810, 19683060, 1830080, 112520, 4410, 100, 1; ...",
				"Coefficients in self-compositions of (x+x^2) form table A122888:",
				"1;",
				"1, 1;",
				"1, 2, 2, 1;",
				"1, 3, 6, 9, 10, 8, 4, 1;",
				"1, 4, 12, 30, 64, 118, 188, 258, 302, 298, 244, 162, 84, 32, 8, 1;",
				"1, 5, 20, 70, 220, 630, 1656, 4014, 8994, 18654, 35832, 63750,...;",
				"1, 6, 30, 135, 560, 2170, 7916, 27326, 89582, 279622, 832680,...; ...",
				"This triangle T transforms rows of A122888 into diagonals of A122888;",
				"the initial diagonals begin:",
				"A112319: [1, 1, 2, 9, 64, 630, 7916, 121023, 2179556, 45179508, ...];",
				"A112317: [1, 2, 6, 30, 220, 2170, 27076, 409836, 7303164, 149837028,..];",
				"A112320: [1, 3, 12, 70, 560, 5810, 74760, 1153740, 20817588, 430604724, ...].",
				"For example:",
				"T * [1, 0, 0, 0, 0, 0, 0,...]~ = A112319;",
				"T * [1, 1, 0, 0, 0, 0, 0,...]~ = A112317;",
				"T * [1, 2, 2, 1, 0, 0, 0,...]~ = A112320."
			],
			"program": [
				"(PARI) {T(n, k)=local(F=x, M, N, P, m=n); M=matrix(m+2, m+2, r, c, F=x;for(i=1, r+c-2, F=subst(F, x, x+x^2+x*O(x^(m+2)))); polcoeff(F, c)); N=matrix(m+1, m+1, r, c, F=x;for(i=1, r, F=subst(F, x, x+x^2+x*O(x^(m+2)))); polcoeff(F, c)); P=matrix(m+1, m+1, r, c, M[r+1, c]); (P~*N~^-1)[n+1, k+1]}",
				"for(n=0, 10, for(k=0, n, print1(T(n, k), \", \")); print(\"\"))"
			],
			"xref": [
				"Cf. A112319, A166901, A166902, A166903, A122888, A135080."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Paul D. Hanna_, Nov 27 2009",
			"references": 9,
			"revision": 6,
			"time": "2014-01-19T18:54:54-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}