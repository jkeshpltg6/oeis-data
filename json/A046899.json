{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A046899",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 46899,
			"data": "1,1,2,1,3,6,1,4,10,20,1,5,15,35,70,1,6,21,56,126,252,1,7,28,84,210,462,924,1,8,36,120,330,792,1716,3432,1,9,45,165,495,1287,3003,6435,12870,1,10,55,220,715,2002,5005,11440,24310,48620,1,11,66,286,1001",
			"name": "Triangle in which n-th row is {binomial(n+k,k), k=0..n}, n \u003e= 0.",
			"comment": [
				"C(n,k) is the number of lattice paths from (0,0) to (n,k) using steps (1,0) and (0,1). - _Joerg Arndt_, Jul 01 2011",
				"Row sums are A001700.",
				"T(n, k) is also the number of order-preserving full transformations (of an n-chain) of waist k (waist(alpha) = max(Im(alpha))). - _Abdullahi Umar_, Oct 02 2008",
				"If T(r,c), r=0,1,2,..., c=1,2,...,(r+1), are the triangle elements, then for r \u003e 0, T(r,c) = binomial(r+c-1,c-1) = M(r,c) is the number of monotonic mappings from an ordered set of r elements into an ordered set of c elements. For example, there are 15 monotonic mappings from an ordered set of 4 elements into an ordered set of 3 elements. For c \u003e r+1, use the identity M(r,c) = M(c-1,r+1) = T(c-1,r+1). For example, there are 210 monotonic mappings from an ordered set of 4 elements into an ordered set of 7 elements, because M(4,7) = T(6,5) = 210. Number of monotonic endomorphisms in a set of r elements, M(r,r), therefore appear on the second diagonal of the triangle which coincides with A001700. - _Stanislav Sykora_, May 26 2012",
				"Start at the origin. Flip a fair coin to determine steps of (1,0) or (0,1).  Stop when you are a (perpendicular) distance of n steps from the x axis or the y axis.  For k = 0,1,...,n-1, C(n-1,k)/2^(n+k) is the probability that you will stop on the point (n,k). This is equal to the probability that you will stop on the point (k,n).  Hence, Sum_{k=0..n} C(n,k)/2^(n+k) = 1. - _Geoffrey Critzer_, May 13 2017"
			],
			"reference": [
				"H. W. Gould, A class of binomial sums and a series transform, Utilitas Math., 45 (1994), 71-83."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A046899/b046899.txt\"\u003eRows n=0..150 of triangle, flattened\u003c/a\u003e",
				"Karl Dilcher and Maciej Ulas, \u003ca href=\"https://arxiv.org/abs/1909.11222\"\u003eArithmetic properties of polynomial solutions of the Diophantine equation P(x)x^(n+1)+Q(x)(x+1)^(n+1)=1\u003c/a\u003e, arXiv:1909.11222 [math.NT], 2019. See Qn(x) Table 1 p. 2.",
				"H. W. Gould, \u003ca href=\"/A007680/a007680.pdf\"\u003eA class of binomial sums and a series transform\u003c/a\u003e, Utilitas Math., 45 (1994), 71-83. (Annotated scanned copy)",
				"A. Laradji and A. Umar, \u003ca href=\"http://dx.doi.org/10.1016/j.jalgebra.2003.10.023\"\u003eCombinatorial results for semigroups of order-preserving partial transformations\u003c/a\u003e, Journal of Algebra 278, (2004), 342-359.",
				"A. Laradji and A. Umar, \u003ca href=\"http://dx.doi.org/10.1007/s00233-005-0553-6\"\u003eCombinatorial results for semigroups of order-preserving full transformations\u003c/a\u003e, Semigroup Forum 72 (2006), 51-62.",
				"\u003ca href=\"/index/Pas#Pascal\"\u003eIndex entries for triangles and arrays related to Pascal's triangle\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = A092392(n,n-k), k = 0..n. - _Reinhard Zumkeller_, Jul 27 2012",
				"T(n,k) = A178300(n,k), n\u003e0, k = 1..n. - _L. Edson Jeffery_, Jul 23 2014",
				"T(n,k) = (n + 1)*hypergeom([-n, 1 - k], [2], 1)). - _Peter Luschny_, Jan 09 2022"
			],
			"example": [
				"The triangle is the lower triangular part of the square array:",
				"  1|  1,  1,   1,   1,    1,    1,     1,     1,     1, ...",
				"  1,  2|  3,   4,   5,    6,    7,     8,     9,    10, ...",
				"  1,  3,  6|  10,  15,   21,   28,    36,    45,    55, ...",
				"  1,  4, 10,  20|  35,   56,   84,   120,   165,   220, ...",
				"  1,  5, 15,  35,  70|  126,  210,   330,   495,   715, ...",
				"  1,  6, 21,  56, 126,  252|  462,   792,  1287,  2002, ...",
				"  1,  7, 28,  84, 210,  462,  924|  1716,  3003,  5005, ...",
				"  1,  8, 36, 120, 330,  792, 1716,  3432|  6435, 11440, ...",
				"  1,  9, 45, 165, 495, 1287, 3003,  6435, 12870| 24310, ...",
				"  1, 10, 55, 220, 715, 2002, 5005, 11440, 24310, 48620| ...",
				"The array read by antidiagonals gives the binomial triangle.",
				"From _Reinhard Zumkeller_, Jul 27 2012: (Start)",
				"Take the first n elements of the n-th diagonal (NW to SE) of left half of Pascal's triangle and write it as n-th row on the triangle on the right side, see above",
				"  0:                 1                    1",
				"  1:               1   _                  1  2",
				"  2:             1   2  __                1  3  6",
				"  3:           1   3  __  __              1  4 10 20",
				"  4:         1   4   6  __  __            1  5 15 35 70",
				"  5:       1   5  10  __  __  __          1  6 21 56 .. ..",
				"  6:     1   6  15  20  __  __  __        1  7 28 .. .. .. ..",
				"  7:   1   7  21  35  __  __  __  __      1  8 .. .. .. .. .. ..",
				"  8: 1   8  28  56  70  __  __  __  __    1 .. .. .. .. .. .. .. .. (End)"
			],
			"maple": [
				"for n from 0 to 10 do seq( binomial(n+m,n), m = 0 .. n) od; # _Zerinvary Lajos_, Dec 09 2007"
			],
			"mathematica": [
				"t[n_, k_] := Binomial[n + k, n]; Table[t[n, k], {n, 0, 10}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Aug 12 2013 *)"
			],
			"program": [
				"(PARI) /* same as in A092566 but use */",
				"steps=[[1, 0], [1, 0] ];",
				"/* _Joerg Arndt_, Jul 01 2011 */",
				"(Haskell)",
				"import Data.List (transpose)",
				"a046899 n k = a046899_tabl !! n !! k",
				"a046899_row n = a046899_tabl !! n",
				"a046899_tabl = zipWith take [1..] $ transpose a007318_tabl",
				"-- _Reinhard Zumkeller_, Jul 27 2012",
				"(MAGMA) /* As triangle */ [[Binomial(n+k, n): k in [0..n]]: n in [0.. 15]]; // _Vincenzo Librandi_, Aug 18 2015",
				"(SageMath)",
				"for n in (0..9):",
				"    print([multinomial(n, k) for k in (0..n)]) # _Peter Luschny_, Dec 24 2020"
			],
			"xref": [
				"Cf. A046900, A001700, A007318, A034868, A092392, A239103, A178300."
			],
			"keyword": "nonn,tabl,easy,nice,changed",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _James A. Sellers_"
			],
			"references": 22,
			"revision": 81,
			"time": "2022-01-09T10:35:23-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}