{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A337065",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 337065,
			"data": "5,12,24,59,97,84,159,128,144,162,186,420,647,457,503,360,1214,1677,532,548,564,600,624,648,1033,1079,752,772,798,828,852,1315,906,924,1924,3096,1667,3496,1208,1230,3834,1993,1360,2101,1446,1472,2251,1530,3977,2471,1668,2569",
			"name": "Infinite sum of the prime numbers, compacted (see the Comments line for an explanation).",
			"comment": [
				"If the successive terms of the present sequence are expressed as the sum of k\u003e1 consecutive primes in only one way and added, the end result will be 2 + 3 + 5 + 7 + 11 + 13 + 17 + 19 + 23 + ... (conjectured to extend ad infinitum).",
				"This is the lexicographically earliest sequence of distinct positive terms with this property.",
				"a(n) is the smallest term of A084146 such that the set of prime parts of a(n) has (i) no primes in common with the union of the prime parts of a(1), ..., a(n-1) and (ii) contains the smallest prime excluded in the union of the prime parts of a(1), ..., a(n-1). - _R. J. Mathar_, Aug 19 2020"
			],
			"link": [
				"Jean-Marc Falcoz, \u003ca href=\"/A337065/b337065.txt\"\u003eTable of n, a(n) for n = 1..201\u003c/a\u003e"
			],
			"example": [
				"The 1st term is 5 and 5 = 2+3.",
				"The 2nd term is 12 and 12 = 5+7.",
				"The 3rd term is 24 and 24 = 11+13.",
				"The 4th term is 59 and 59 = 17+19+23.",
				"The 5th term is 97 and 97 = 29+31+37.",
				"The 6th term is 84 and 84 = 41+43; etc.",
				"(The 4th term is NOT a(4) = 36 as 36 is the sum of consecutive primes in more than one way: 36 = 17+19 and 36 = 5+7+11+13)."
			],
			"maple": [
				"# the set of prime partitions of A084146(n)",
				"A084146Pset := proc(n::integer)",
				"    option remember;",
				"    local pset,k,i,spr ;",
				"    pset := {} ;",
				"    if isA084146(n) then",
				"        for k from 2 do",
				"            if add(ithprime(i),i=1..k) \u003e n then",
				"                break;",
				"            end if;",
				"            for i from 1 do",
				"                spr := add( ithprime(j),j=i..i+k-1) ;",
				"                if spr \u003e n then",
				"                    break;",
				"                elif spr = n then",
				"                    return {seq(ithprime(j),j=i..i+k-1)} ;",
				"                end if;",
				"            end do:",
				"        end do:",
				"    end if;",
				"    pset ;",
				"end proc:",
				"A337065 := proc(n)",
				"    option remember;",
				"    local pprev,i,pmex,uni, thiss ;",
				"    # the set of all primes needed to represent all previous terms",
				"    pprev := {} ;",
				"    for i from 1 to n-1 do",
				"        pprev := pprev union A084146Pset(procname(i)) ;",
				"    end do ;",
				"    # smallest prime not in the representation of previous terms",
				"    for i from 1 do",
				"        if not ithprime(i) in pprev then",
				"            pmex := ithprime(i) ;",
				"            break;",
				"        end if;",
				"    end do:",
				"    for uni from 1 do",
				"        thiss := A084146Pset(uni) ;",
				"        if pmex in thiss and thiss intersect(pprev) = {} then",
				"            return uni ;",
				"        end if;",
				"    end do:",
				"end proc:",
				"for n from 1 do",
				"    print(A337065(n)) ;",
				"end do: # _R. J. Mathar_, Aug 19 2020"
			],
			"xref": [
				"Cf. A336897, A050936, A084146."
			],
			"keyword": "base,nonn",
			"offset": "1,1",
			"author": "_Eric Angelini_ and _Jean-Marc Falcoz_, Aug 14 2020",
			"references": 1,
			"revision": 25,
			"time": "2020-08-29T03:19:29-04:00",
			"created": "2020-08-16T12:59:15-04:00"
		}
	]
}