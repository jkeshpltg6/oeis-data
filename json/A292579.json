{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A292579",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 292579,
			"data": "0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,60,61,62,63,64,65,66,67,68,69",
			"name": "Inverse to A055643: Consider n as a number written in base 60, using 2 decimal digits for each base-60 digit (which we allow to be in the range 0 .. 99), and write n in base 10.",
			"comment": [
				"For the first 240000 terms, also the conversion of a digital clock display H:MM:SS to seconds, i.e., a(HMMSS) yields the number of seconds after midnight corresponding to that time, with leading zeros allowed: 0 \u003c= H \u003c= 24, 0 \u003c= MM, SS \u003c= 59.",
				"Numbers where SS or MM etc. are \u003e 59 are strictly spoken invalid in base 60. However, in order to \"fill in the gaps\" in a meaningful way, we simply do allow SS, MM, etc. to take values up to 99. (The ISO specification for writing time and date does actually allow SS to take values including 60 in order to account for leap seconds, as it allows H to be equal to 24 to denote midnight at the end of a day.) Therefore, a(60+k) = 60+k for k \u003c 40, but a(100+k) = 60+k, a(200+k) = 120+k etc, for 0 \u003c= k \u003c= 99."
			],
			"formula": [
				"a(n) = n - [n/100]*40 - [n/10^4]*40*60 - [n\\10^6]*40*60^2 - ..., where [.] denotes the floor function.",
				"A292579 o A055643 = A001477 (the identity map on the set of nonnegative integers), i.e., for all n \u003e= 0, A292579(A055643(n)) = n.",
				"Also, A055643(A292579(n)) = n for all n in the range of A055643, which are the \"valid base-60 numbers\", i.e., excluding 60, ..., 99, 160, ..., 199 etc. The latter (60, ...) are \"converted\" by A055643 o A292579 to correct base-60 numbers, i.e., 60 -\u003e 100, 99 = 60 + 39 -\u003e 139, 9999 -\u003e 14039 since 99*60 + 99 = 6039 = 1*60^2 + 40*60 + 39 = 14039[60]."
			],
			"example": [
				"Interpreted in base-60, HMMSS[60] = H*60^2 + MM*60 + SS. Thus, e.g., a(100) = 60, a(123456) = 12*3600 + 34*60 + 56 = 45296.",
				"In contrast to (most) digital clocks, MM and SS may take values up to 99. For example, a(199) = 1*60 + 99 = 159."
			],
			"program": [
				"(PARI) A292579(n)=fromdigits(digits(n,100),60)"
			],
			"xref": [
				"Cf. A055643."
			],
			"keyword": "nonn,base",
			"offset": "0,3",
			"author": "_M. F. Hasler_, Jan 09 2018",
			"references": 6,
			"revision": 29,
			"time": "2018-04-23T02:59:19-04:00",
			"created": "2018-01-14T14:44:53-05:00"
		}
	]
}