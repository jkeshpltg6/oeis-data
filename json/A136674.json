{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A136674",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 136674,
			"data": "1,2,-1,1,-4,1,0,-8,6,-1,-1,-12,19,-8,1,-2,-15,44,-34,10,-1,-3,-16,84,-104,53,-12,1,-4,-14,140,-258,200,-76,14,-1,-5,-8,210,-552,605,-340,103,-16,1,-6,3,288,-1056,1562,-1209,532,-134,18,-1,-7,20,363,-1848,3575,-3640,2170,-784,169,-20,1",
			"name": "Triangle T(n,k) read by rows: coefficient [x^k] of the polynomial p(n,x) with p(0,x) = 1, p(1,x) = 2 - x, p(2,x) = 1 - 4*x + x^2 and p(n,x) = (2-x)*p(n-1,x) - p(n-2,x) if n\u003e2.",
			"comment": [
				"Row sums: A117373(n-1)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A136674/b136674.txt\"\u003eRows n = 0..100 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = 2*T(n-1,k) - T(n-2,k) - T(n-1,k-1). - _R. J. Mathar_, Jan 12 2011"
			],
			"example": [
				"Triangle begins as:",
				"   1;",
				"   2,  -1;",
				"   1,  -4,   1;",
				"   0,  -8,   6,    -1;",
				"  -1, -12,  19,    -8,    1;",
				"  -2, -15,  44,   -34,   10,    -1;",
				"  -3, -16,  84,  -104,   53,   -12,    1;",
				"  -4, -14, 140,  -258,  200,   -76,   14,   -1;",
				"  -5,  -8, 210,  -552,  605,  -340,  103,  -16,   1;",
				"  -6,   3, 288, -1056, 1562, -1209,  532, -134,  18,  -1;",
				"  -7,  20, 363, -1848, 3575, -3640, 2170, -784, 169, -20, 1;"
			],
			"maple": [
				"A136674aux := proc(n) option remember; if n = 0 then 1; elif n= 1 then 2-x ; elif n= 2 then 1-4*x+x^2 ; else (2-x)*procname(n-1)-procname(n-2) ; end if; end proc:",
				"A136674 := proc(n,k) coeftayl(A136674aux(n),x=0,k) ; end proc: # _R. J. Mathar_, Jan 12 2011"
			],
			"mathematica": [
				"(* tridiagonal matrix code*)",
				"T[n_, m_, d_]:= If[n==m, 2, If[n==d \u0026\u0026 m==d-1, -3, If[(n==m-1 || n==m+1), -1, 0]]];",
				"M[d_]:= Table[T[n, m, d], {n,d}, {m,d}];",
				"Join[{{1}}, Table[CoefficientList[Det[M[d] - x*IdentityMatrix[d]], x], {d, 1, 10}]]//Flatten",
				"(* polynomial recursion: three initial terms necessary*)",
				"p[x, 0]:= 1; p[x, 1]:= (2-x); p[x, 2]:= 1 -4*x +x^2;",
				"p[x_, n_]:= p[x, n]= (2-x)*p[x, n-1] - p[x, n-2];",
				"Table[ExpandAll[p[x, n]], {n, 0, Length[g] -1}]",
				"(* Third program *)",
				"T[n_, k_]:= T[n, k]= If[k\u003c0 || k\u003en, 0, If[k==n, (-1)^n, If[k==0, 3-n, 2*T[n-1, k] -T[n-2, k] -T[n-1, k-1] ]]]; Table[T[n, k], {n, 0, 10}, {k, 0, n}]//Flatten (* _G. C. Greubel_, Mar 12 2020 *)"
			],
			"program": [
				"(Sage)",
				"@CachedFunction",
				"def T(n, k):",
				"    if (k\u003c0 or k\u003en): return 0",
				"    elif (k==n): return (-1)^n",
				"    elif (k==0): return 3-n",
				"    else: return 2*T(n-1,k) - T(n-2,k) - T(n-1,k-1)",
				"[[T(n, k) for k in (0..n)] for n in (0..10)] # _G. C. Greubel_, Mar 12 2020"
			],
			"keyword": "easy,tabl,sign",
			"offset": "0,2",
			"author": "_Roger L. Bagula_, Apr 05 2008",
			"references": 9,
			"revision": 11,
			"time": "2020-03-12T18:59:15-04:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}