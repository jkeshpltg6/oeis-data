{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A094440",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 94440,
			"data": "1,1,2,2,3,3,3,8,6,4,5,15,20,10,5,8,30,45,40,15,6,13,56,105,105,70,21,7,21,104,224,280,210,112,28,8,34,189,468,672,630,378,168,36,9,55,340,945,1560,1680,1260,630,240,45,10,89,605,1870,3465,4290,3696,2310,990,330,55,11",
			"name": "Triangular array T(n,k) = Fibonacci(n+1-k)*C(n,k-1), k = 1..n; n \u003e= 1.",
			"comment": [
				"Row sums yield the even-subscripted Fibonacci numbers (A001906).",
				"Row n shows the coefficients of the numerator of the n-th derivative of c(n)/(x^2+x-1), where c(n) = ((-1)^(n + 1))/n!; see the Mathematica program. - _Clark Kimberling_, Oct 22 2019"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A094440/b094440.txt\"\u003eRows n = 1..100 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"From _Peter Bala_, Aug 17 2007: (Start)",
				"With an offset of 0, the row polynomials F(n,x) = Sum_{k = 0..n} C(n,k)*Fibonacci(n-k)*x^k satisfy F(n,x)*L(n,x) = F(2*n,x), where L(n,x) = Sum_{k = 0..n} C(n,k)*Lucas(n-k)*x^k.",
				"Other identities and formulas include:",
				"F(n+1,x)^2 - F(n,x)*F(n+2,x) = (x^2 + x - 1)^n;",
				"Sum_{k = 0..n} C(n,k)*F(n-k,x)*L(k,x) = 2^n F(n,x);",
				"F(n,2*x) = Sum_{k = 0..n} C(n,k)*F(n-k,x)*x^k;",
				"F(n,3*x) = Sum_{k = 0..n} C(n,k)*F(n-k,2*x)*x^k, etc.",
				"Sequence {F(n,r)}n\u003e=1 gives the r th binomial transform of the Fibonacci numbers: r = 1 gives A001906, r = 2 gives A030191, r = 3 gives A099453, r = 4 gives A081574, r = 5 gives A081574.",
				"F(n,1/phi) = (-1)^(n-1)*F(n,-phi) = sqrt(5)^(n-1) for n \u003e= 1, where phi = (1 + sqrt(5))/2.",
				"The polynomials F(n,-x) satisfy a Riemann hypothesis: the zeros of F(n,-x) lie on the vertical line Re x = 1/2 in the complex plane.",
				"G.f.: t/(1 - (2*x + 1)*t + (x^2 + x - 1)*t^2) = t + (1 + 2*x)*t^2 + (2 + 3*x + 3*x^2)*t^3 + (3 + 8*x + 6*x^2 + 4*x^3)*t^4 + ... . (End)",
				"From _Peter Bala_, Jun 29 2016: (Start)",
				"Working with an offset of 0, the n-th row polynomial F(n,x) = 1/sqrt(5)*( (x + phi)^n - (x - 1/phi)^n ), where phi = (1 + sqrt(5))/2.",
				"d/dx(F(n,x)) = n*F(n-1,x).",
				"F(-n,x) = -F(n,x)/(x^2 + x - 1)^n.",
				"F(n,x - 1) = (-1)^(n-1)*F(n,-x).",
				"F(n,x) is a divisibility sequence of polynomials, that is, if n divides m then F(n,x) divides F(m,x) in the polynomial ring Z[x]. (End)",
				"From _G. C. Greubel_, Oct 30 2019: (Start)",
				"Sum_{k=1..n} T(n,k) = Fibonacci(2*n).",
				"Sum_{k=1..n} (-1)^k * T(n,k) = (-1)^n * Fibonacci(n). (End)",
				"From _Clark Kimberling_, Oct 30 2019: (Start)",
				"F(n,x) is a strong divisibility sequence of polynomials; that is,",
				"gcd(F(x,h),F(x,k)) = F(x,gcd(h,k)) for h,k \u003e= 1.  Thus, if x is an integer, then F(n,x) is a strong divisibility sequence of integers; e.g., for x=3, we have A099453. (End)"
			],
			"example": [
				"Triangle starts:",
				"   1;",
				"   1,  2;",
				"   2,  3,   3;",
				"   3,  8,   6,   4;",
				"   5, 15,  20,  10,  5;",
				"   8, 30,  45,  40, 15,  6;",
				"  13, 56, 105, 105, 70, 21, 7;",
				"  ...",
				"T(4,3) = F(2)*C(4,2) = 1*6 = 6."
			],
			"maple": [
				"with(combinat): T:=(n,k)-\u003ebinomial(n,k-1)*fibonacci(n+1-k): for n from 1 to 11 do seq(T(n,k),k=1..n) od; # yields sequence in triangular form # _Emeric Deutsch_"
			],
			"mathematica": [
				"Table[Fibonacci[n+1-k]Binomial[n,k-1],{n,20},{k,n}]//Flatten (* _Harvey P. Dale_, Sep 14 2016 *)",
				"(* Next program outputs polynomials having coefficients T(n,k) *)",
				"g[x_, n_] := Numerator[(-1)^(n + 1) Factor[D[1/(1 - x - x^2), {x, n}]]]",
				"Column[Expand[Table[g[x, n]/n!, {n, 0, 12}]]] (* _Clark Kimberling_, Oct 22 2019 *)"
			],
			"program": [
				"(MAGMA) /* As triangle */ [[Fibonacci(n+1-k)*Binomial(n,k-1): k in [1..n]]: n in [1.. 15]]; // _Vincenzo Librandi_, Aug 15 2017",
				"(PARI) T(n,k) = binomial(n,k-1)*fibonacci(n-k+1);",
				"for(n=1,12, for(k=1,n, print1(T(n,k), \", \"))) \\\\ _G. C. Greubel_, Oct 30 2019",
				"(Sage) [[binomial(n,k-1)*fibonacci(n-k+1) for k in (1..n)] for n in (1..12)] # _G. C. Greubel_, Oct 30 2019",
				"(GAP) Flat(List([1..12], n-\u003e List([1..n], k-\u003e Binomial(n,k-1)* Fibonacci(n-k+1) ))); # _G. C. Greubel_, Oct 30 2019"
			],
			"xref": [
				"Cf. A000032, A000045, A001906, A030191, A081574, A094435, A132148.",
				"Cf. A094435, A094436, A094437, A094438, A094439, A094441, A094442, A094443, A094444."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Clark Kimberling_, May 03 2004",
			"ext": [
				"Error in expansion of generating function corrected by _Peter Bala_, Sep 24 2008"
			],
			"references": 16,
			"revision": 41,
			"time": "2019-10-31T01:33:10-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}