{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A168175",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 168175,
			"data": "1,4,9,8,-31,-180,-503,-752,513,7316,25673,51480,26209,-255524,-1205559,-3033568,-3695359,6453540,51681673,161551912,284435937,6880364,-1963530103,-7902282960,-17864421119,-16141703756,60484132809",
			"name": "Expansion of 1/(1 - 4*x + 7*x^2).",
			"comment": [
				"Also the coefficient of i of Q^(n+1), Q being the quaternion 2+i+j+k. The real part of the quaternion power is A213421, see also A087455, A088138, A128018. - _Stanislav Sykora_, Jun 11 2012",
				"a(n)*(-1)^n gives the coefficient c(7^n) of (eta(z^6))^4, a modular cusp form of weight 2, when expanded in powers of q = exp(2*Pi*i*z), Im(z) \u003e 0, assuming alpha-multiplicativity (but not for primes 2 and 3) with alpha(x) = x (weight 2) and input c(7) = -4. Eta is the Dedekind function. See the Apostol reference, p. 138, eq. (54) for alpha-multiplicativity and p. 130, eq. (39) with k=2. See also A000727(n) = b(n) where c(7^n) = b((7^n-1)/6) = b(A023000(n)), n \u003e= 0. Proof: The alpha-multiplicity with alpha(1) = 1 and c(1) = 1 leads from p^n = p^(n-1)*p to the recurrence c_n = c*c_(n-1) - a*c(n-2), with c_n = c(p^n), c = c(p) and a = alpha(p). Inputs are c_{-1} = 0 and c_0 = c(1) = 1. This gives the polynomial c_n = sqrt(a)^n * S(n,c/sqrt(a)), with Chebyshev's S-polynomials (A049310). See the Apostol reference, Exercise 6., p. 139. Here p = 7, c = -4. - _Wolfdieter Lang_, Apr 27 2016"
			],
			"reference": [
				"Tom M. Apostol, Modular Functions and Dirichlet Series in Number Theory, Second edition, Springer, 1990, pp. 130, 138 - 139."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A168175/b168175.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-7).",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e"
			],
			"formula": [
				"a(n) = (1/2 - i/sqrt(3))*(2 + i*sqrt(3))^n + (1/2 + i/sqrt(3))*(2 - i*sqrt(3))^n (Binet formula), where i is the imaginary unit.",
				"a(n) = 4*a(n-1) - 7*a(n-2).",
				"a(n) = sqrt(7)^n * S(n, 4/sqrt(7)), n \u003e= 0, with Chebyshev's S polynomials (A049310). - _Wolfdieter Lang_, Apr 27 2016",
				"E.g.f.: (2*sqrt(3)*sin(sqrt(3)*x) + 3*cos(sqrt(3)*x))*exp(2*x)/3. - _Ilya Gutkovskiy_, Apr 27 2016",
				"a(n) = (-1) * 7^(n+1) * a(-2-n) for all n in Z. - _Michael Somos_, Feb 23 2020"
			],
			"example": [
				"G.f. = 1 + 4*x + 9*x^2 + 8*x^3 - 31*x^4 - 180*x^5 - 503*x^6 - 752*x^7 + ... - _Michael Somos_, Feb 23 2020"
			],
			"maple": [
				"a:=series(1/(1-4*x+7*x^2),x=0,27): seq(coeff(a,x,n),n=0..26); # _Paolo P. Lava_, Mar 28 2019"
			],
			"mathematica": [
				"CoefficientList[Series[1/(1-4x+7x^2),{x,0,30}],x] (* or *) LinearRecurrence[ {4,-7},{1,4},30] (* _Harvey P. Dale_, Nov 28 2014 *)"
			],
			"program": [
				"(MAGMA) I:=[1,4]; [n le 2 select I[n] else 4*Self(n-1)-7*Self(n-2): n in [1..30]]; // _Vincenzo Librandi_, Jun 25 2012",
				"(PARI) {a(n) = my(s=1, t=1); if( n\u003c0, n=-2-n; s=-1; t=1/7); s * t^(n+1) * polcoeff(1 / (1 - 4*x + 7*x^2) + x * O(x^n), n)}; /* _Michael Somos_, Feb 23 2020 */"
			],
			"xref": [
				"Cf. A023000, A049310."
			],
			"keyword": "sign,easy",
			"offset": "0,2",
			"author": "_Roger L. Bagula_, Nov 19 2009",
			"references": 8,
			"revision": 47,
			"time": "2020-02-23T19:25:15-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}