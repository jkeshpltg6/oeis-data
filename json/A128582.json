{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A128582",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 128582,
			"data": "1,1,0,0,1,2,0,0,1,1,0,0,2,1,0,0,1,1,0,0,2,1,0,0,1,2,0,0,1,1,0,0,1,2,0,0,0,1,0,0,3,1,0,0,1,3,0,0,1,0,0,0,1,0,0,0,2,1,0,0,2,2,0,0,1,2,0,0,2,1,0,0,0,1,0,0,1,1,0,0,2,1,0,0,1,2,0",
			"name": "Expansion of f(x^4, x^12) * f(x, x^5) in powers of x where f(, ) is Ramanujan's general theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A128582/b128582.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of psi(x) * psi(-x^3) / chi(-x^4)^2 in powers of x where psi(), chi() are Ramanujan theta functions.",
				"Expansion of q^(-5/6) * eta(q^2)^2 * eta(q^3) * eta(q^8)^2 * eta(q^12) / (eta(q) * eta(q^4)^2 * eta(q^6)) in powers of q.",
				"Euler transform of period 24 sequence [ 1, -1, 0, 1, 1, -1, 1, -1, 0, -1, 1, 0, 1, -1, 0, -1, 1, -1, 1, 1, 0, -1, 1, -2, ...].",
				"G.f.: Product_{k\u003e0} (1 + x^k) * (1 - x^(2*k)) * (1 + x^(4*k))^2 * (1 - x^(6*k - 3)) * (1 - x^(12*k)).",
				"A128580(3*n + 2) = -2 * a(n). a(4*n) = A128583. a(4*n + 1) = A128591(n). a(4*n + 2) = a(4*n + 3) = 0."
			],
			"example": [
				"G.f. = 1 + x + x^4 + 2*x^5 + x^8 + x^9 + 2*x^12 + x^13 + x^16 + x^17 + 2*x^20 + ...",
				"G.f. = q^5 + q^11 + q^29 + 2*q^35 + q^53 + q^59 + 2*q^77 + q^83 + q^101 + q^107 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 0, 0, With[{m = 6 n + 5}, -1/2 DivisorSum[ m, KroneckerSymbol[ -12, #] KroneckerSymbol[ 2, m/#] \u0026]]]; (* _Michael Somos_, Nov 15 2015 *)",
				"a[ n_] := SeriesCoefficient[ 2^(-3/2) x^(-7/8) EllipticTheta[ 2, Pi/4, x^(3/2)] EllipticTheta[ 2, Pi/4, x]^2 / QPochhammer[ x], {x, 0, n}]; (* _Michael Somos_, Nov 15 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, n = 6*n + 5; -1/2 * sumdiv( n, d, kronecker( -12, d) * kronecker( 2, n/d)))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^2 * eta(x^3 + A) * eta(x^8 + A)^2 * eta(x^12 + A) / (eta(x + A) * eta(x^4 + A)^2 * eta(x^6 + A)), n))};"
			],
			"xref": [
				"Cf. A128580, A128583, A128591."
			],
			"keyword": "nonn",
			"offset": "0,6",
			"author": "_Michael Somos_, Mar 11 2007",
			"references": 11,
			"revision": 22,
			"time": "2021-03-12T22:24:44-05:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}