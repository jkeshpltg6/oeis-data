{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A258718",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 258718,
			"data": "1,8,1,9,0,5,8,9,0,2,0,0,8,0,1,2,1,5,6,7,6,2,0,9,6,7,7,9,0,2,8,7,2,1,2,3,4,0,4,7,9,5,5,0,2,6,4,8,5,2,1,1,5,2,1,7,5,8,8,5,4,2,1,4,3,2,1,8,7,9,9,0,1,4,9,1,4,2,1,1,8,9,2,7,3",
			"name": "Value of DIS (\"Decimal Integer Series\") constant based on sequence of squares.",
			"comment": [
				"The following problem was proposed in Popular Computing in 1973. If m is a k-digit number, let c(m) = m/100^k. For example, if m=16, c(m) = .0016 = 16/100^2. For a sequence S = a(1), a(2), a(3), ..., the \"DIS\" constant based on S is defined to be f(S) = Sum_{n \u003e= 1} c(a(n)).",
				"If S = 1, 4, 9, 16, 25, 36, ... the nonzero squares then f(S) is the sum of the infinite series",
				".01",
				".04",
				".09",
				".0016",
				".0025",
				".0036",
				"...",
				"Problem 22 in Popular Computing asks for the values of f(S) when S is respectively the squares (A000290), the cubes (A259929), the powers of 2 (A259838), powers of 3 (A259930), Fibonacci numbers (A000045), factorials (A259837), and subfactorials (A000166). To this list we might add the triangular numbers (A000217), the Catalan numbers (A000108), and the Motzkin numbers (A001006). But not the primes (A000040), for in that case the series would diverge.",
				"Solution in the case of squares from _Alex Meiburg_, Jun 17, 2015:",
				"(Start)",
				"If we group the sum (for squares) by number of digits -- that is,  A = (.01",
				"+ .04 + .09 + .0016 + .0025 + ...) + (.000100 + .000121 + .000144 )...,",
				"Mathematica gives a closed form for each term. Specifically,",
				"f[n_] := 1/3 2^(-5-4 n) 25^(-2-2 n) (10^(1+n)-Ceiling[10^(1/2+n)])",
				"(1+2^(3+2 n) 5^(2+2 n)-3 10^(1+n)-3 Ceiling[10^(1/2+n)]+2^(2+n) 5^(1+n)",
				"Ceiling[10^(1/2+n)]+2 Ceiling[10^(1/2+n)]^2)-1/3 2^(-3-4 n) 5^(-2-4 n)",
				"(-1+10^n-Floor[10^(1/2+n)]) (2^(1+2 n) 5^(2",
				"n)-10^n+Floor[10^(1/2+n)]+2^(1+n) 5^n Floor[10^(1/2+n)]+2",
				"Floor[10^(1/2+n)]^2)",
				"this is found by breaking into those less than 10^(n+1/2) and those more",
				"than 10^(n+1/2), and each sum can be done exactly. The above expression",
				"is then summed over n from 0 onwards. This allows it to converge very",
				"rapidly, yielding (in 1 second on my computer)",
				"A = 0.",
				"1819058902008012156762096779028721234047955026485211521758854214321879901491421189273371395956634796",
				"1904208362098771470319180456038832179432219577663082261873935853836211627222184657331459477127289143",
				"6276893929762580722032999375515089312368984249626008647664116996102082800886283059357211021253063111",
				"7152305529806492037632632133219059593583028182894120128297404653399814673445759857199248725452332778",
				"2701081771512735871942465499976300270627184777090377542514576075322995673512370243399897097464883624",
				"2192516987267080161614437137250709716131980302821936381137562051251267791554480374065719655469612472",
				"1097775213404047734600505041085860200873512242914767206681950461568207388112576403619589771428626620",
				"4125627257974347552135871228668804281367024808511880657288473617154807673305471279796586999731661629",
				"7031493974952970572666731437316703731493871634266094228909721713279127991559139591293931720140406675",
				"3533810115103080147864906643290214333852271864692146387518404231194138382947749162680518961898521092",
				"... The same technique should work for cubes, and perhaps some sequences that grow exponentially; but almost certainly not with factorials.",
				"(End)"
			],
			"reference": [
				"N. J. A. Sloane, Alex Meiburg, Olivier Gérard, \"A Computational Challenge from 1973\", Postings to Sequence Fans Mailing List, Jun 17 2015."
			],
			"link": [
				"Robert G. Wilson v, \u003ca href=\"/A258718/b258718.txt\"\u003eTable of n, a(n) for n = 0..999\u003c/a\u003e",
				"Popular Computing (Calabasas CA), \u003ca href=\"/A258718/a258718.jpg\"\u003eProblem 22: Decimal Integer Series (DIS)\u003c/a\u003e, Vol. 1 (No. 8, Nov 1973), page PC8-14."
			],
			"xref": [
				"Cf. A000290, A000079, A000142, A000166, A000045, A000217, A000040."
			],
			"keyword": "nonn,cons,base",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Jun 17 2015",
			"references": 5,
			"revision": 52,
			"time": "2016-02-15T00:06:15-05:00",
			"created": "2015-06-17T10:54:19-04:00"
		}
	]
}