{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A340785",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 340785,
			"data": "1,2,1,3,1,2,1,5,1,2,1,4,1,2,1,7,1,3,1,4,1,2,1,7,1,2,1,4,1,3,1,11,1,2,1,6,1,2,1,7,1,3,1,4,1,2,1,12,1,3,1,4,1,3,1,7,1,2,1,7,1,2,1,15,1,3,1,4,1,3,1,12,1,2,1,4,1,3,1,12,1,2,1,7,1",
			"name": "Number of factorizations of 2n into even factors \u003e 1.",
			"link": [
				"Antti Karttunen, \u003ca href=\"/A340785/b340785.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A349906(2*n). - _Antti Karttunen_, Dec 13 2021"
			],
			"example": [
				"The a(n) factorizations for n = 2*2, 2*4, 2*8, 2*12, 2*16, 2*32, 2*36, 2*48 are:",
				"  4    8      16       24     32         64           72      96",
				"  2*2  2*4    2*8      4*6    4*8        8*8          2*36    2*48",
				"       2*2*2  4*4      2*12   2*16       2*32         4*18    4*24",
				"              2*2*4    2*2*6  2*2*8      4*16         6*12    6*16",
				"              2*2*2*2         2*4*4      2*4*8        2*6*6   8*12",
				"                              2*2*2*4    4*4*4        2*2*18  2*6*8",
				"                              2*2*2*2*2  2*2*16               4*4*6",
				"                                         2*2*2*8              2*2*24",
				"                                         2*2*4*4              2*4*12",
				"                                         2*2*2*2*4            2*2*4*6",
				"                                         2*2*2*2*2*2          2*2*2*12",
				"                                                              2*2*2*2*6"
			],
			"mathematica": [
				"facs[n_]:=If[n\u003c=1,{{}},Join@@Table[Map[Prepend[#,d]\u0026,Select[facs[n/d],Min@@#\u003e=d\u0026]],{d,Rest[Divisors[n]]}]];",
				"Table[Length[Select[facs[n],Select[#,OddQ]=={}\u0026]],{n,2,100,2}]"
			],
			"program": [
				"(PARI)",
				"A349906(n, m=n) = if(1==n, 1, my(s=0); fordiv(n, d, if((d\u003e1)\u0026\u0026(d\u003c=m)\u0026\u0026!(d%2), s += A349906(n/d, d))); (s));",
				"A340785(n) = A349906(2*n); \\\\ _Antti Karttunen_, Dec 13 2021"
			],
			"xref": [
				"Note: A-numbers of Heinz-number sequences are in parentheses below.",
				"The version for partitions is A035363 (A066207).",
				"The odd version is A340101.",
				"The even length case is A340786.",
				"- Factorizations -",
				"A001055 counts factorizations, with strict case A045778.",
				"A340653 counts balanced factorizations.",
				"A340831/A340832 count factorizations with odd maximum/minimum.",
				"A316439 counts factorizations by product and length",
				"A340102 counts odd-length factorizations of odd numbers into odd factors.",
				"- Even -",
				"A027187 counts partitions of even length/maximum (A028260/A244990).",
				"A058696 counts partitions of even numbers (A300061).",
				"A067661 counts strict partitions of even length (A030229).",
				"A236913 counts partitions of even length and sum.",
				"A340601 counts partitions of even rank (A340602).",
				"Cf. A001147, A001222, A050320, A066208, A160786, A174725, A320655, A320656, A339890, A340851.",
				"Even bisection of A349906."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Gus Wiseman_, Jan 30 2021",
			"references": 12,
			"revision": 16,
			"time": "2021-12-14T00:24:30-05:00",
			"created": "2021-02-01T09:14:37-05:00"
		}
	]
}