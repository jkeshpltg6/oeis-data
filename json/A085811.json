{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A085811",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 85811,
			"data": "0,0,1,0,1,1,2,2,4,4,7,8,12,14,21,24,34,41,55,66,88,105,137,165,210,253,320,383,478,574,708,847,1039,1238,1507,1794,2167,2573,3094,3660,4378,5170,6153,7245,8591,10087,11914,13959,16424,19196,22519,26252,30701",
			"name": "Number of partitions of n including 3, but not 1.",
			"comment": [
				"Related to the 'number of sums containing k' phenomena reported at link. Define P_k(n,j) to be the number of partitions of n with minimum part j and containing k, P_k(n) as the number of partitions of n that contain k as a part and P(n,j) as the number of partitions of k that have minimum part k, then: P_k(n)=sum{i=1,k-1,P_k(n-i,i)}+P(n-k,k) which (unproved) gives P(n-k). This sequence gives P_3(n,2). E.g. assume P_3(9)=11. P_3(10)=P_3(9,1)+P_3(8,2)+P(7,3)=11+2+2=15, where P(7,3) is given by A008483(7)."
			],
			"link": [
				"Andrew van den Hoeven, \u003ca href=\"/A085811/b085811.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"A002865(n) = a(n+3). - _James A. Sellers_, Dec 06 2005.",
				"G.f.: x^3*(1-x)/prod(n\u003e=1, 1-x^n). [_Joerg Arndt_, Feb 03 2012]",
				"G.f.: x^2 + x^3*(1 - G(0))/(1-x) where G(k) =  1 - x/(1-x^(k+1))/(1-x/(x-1/G(k+1) )); (recursively defined continued fraction). - _Sergei N. Gladkovskii_, Jan 23 2013"
			],
			"example": [
				"a(3): 3",
				"a(5): 2+3",
				"a(6): 3+3",
				"a(7): 2+2+3, 3+4",
				"a(8): 2+3+3, 3+5",
				"a(9): 2+3+4, 2+2+2+3, 3+3+3, 3+6",
				"a(10): 2+3+5, 2+2+3+3, 3+7, 3+3+4",
				"a(11): 2+2+3+4, 2+3+6, 2+2+2+2+3, 2+3+3+3, 3+4+4, 3+8, 3+3+5,",
				"a(12): 2+2+2+3+3, 2+3+3+4, 2+3+7, 2+2+3+5, 3+9, 3+3+6, 3+4+5, 3+3+3+3",
				"a(13): 2+2+2+2+2+3, 2+2+2+3+4, 2+2+3+6, 2+2+3+3+3, 2+3+4+4, 2+3+3+5,",
				"2+3+8, 3+10, 3+3+7, 3+4+6, 3+5+5, 3+3+3+4"
			],
			"mathematica": [
				"f[n_] := Block[{c = 0, k = 1, m = PartitionsP[n], p = IntegerPartitions[n] }, While[k \u003c m, If[ Count[ p[[k]], 3] \u003e 0 \u0026\u0026 Count[ p[[k]], 1] == 0, c++ ]; k++ ]; c]; Table[ f[n], {n, 1, 53}]",
				"(* second program: *)",
				"CoefficientList[x^2*(1-x)/QPochhammer[x] + O[x]^60, x] (* _Jean-François Alcover_, Jan 22 2016, after _Joerg Arndt_ *)"
			],
			"program": [
				"(PARI)  x='x+O('x^66); /* about that many terms */",
				"v=Vec((x^3*(1-x)/eta(x)))  /* _Joerg Arndt_, Feb 03 2012 */"
			],
			"xref": [
				"Cf. A008483, A002865. Essentially the same as A002865."
			],
			"keyword": "nonn",
			"offset": "1,7",
			"author": "_Jon Perry_, Jul 25 2003",
			"ext": [
				"Edited, corrected and extended by _Robert G. Wilson v_",
				"Typo in formula corrected by _Andrew van den Hoeven_, Nov 20 2014"
			],
			"references": 3,
			"revision": 38,
			"time": "2016-01-22T06:43:58-05:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}