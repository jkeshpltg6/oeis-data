{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005202",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5202,
			"id": "M3282",
			"data": "0,1,0,1,1,4,6,14,28,60,125,263,558,1181,2513,5339,11392,24290,51926,111017,237757,509404,1092713,2345256,5038015,10828720,23291759,50126055,107939753,232550011,501270200,1080996244,2332221316,5033764628,10868950676,23476998980,50728408182,109649040738,237081174662,512767906801,1109354495908",
			"name": "Total number of fixed points in planted trees with n nodes.",
			"comment": [
				"From _R. J. Mathar_, Apr 13 2019: (Start)",
				"The associated triangle H_{p,j}, p \u003e= 1, 1 \u003c= j \u003c= p, a(n) = Sum_{j=1..p} j*H_{p,j}, row sums in A001678, starts:",
				"   1;",
				"   0,  0;",
				"   1,  0,  0;",
				"   1,  0,  0,  0;",
				"   1,  0,  1,  0,  0;",
				"   1,  1,  1,  0,  0,  0;",
				"   2,  2,  1,  0,  1,  0,  0;",
				"   1,  4,  2,  2,  1,  0,  0,  0;",
				"   3,  4,  4,  5,  2,  0,  1,  0,  0;",
				"   3,  7,  7,  9,  4,  4,  1,  0,  0,  0;",
				"   5,  9, 15, 14, 11,  9,  3,  0,  1,  0,  0;",
				"   4, 14, 23, 28, 25, 19,  7,  6,  1,  0,  0,  0;",
				"  11, 15, 39, 46, 55, 38, 24, 14,  5,  0,  1,  0,  0;",
				"   6, 32, 54, 86, 97, 86, 64, 36, 11,  9,  1,  0,  0,  0;",
				"(End)"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"F. Harary and E. M. Palmer, \u003ca href=\"https://doi.org/10.1017/S0305004100055857\"\u003eProbability that a point of a tree is fixed\u003c/a\u003e, Math. Proc. Camb. Phil. Soc. 85 (1979) 407-415.",
				"\u003ca href=\"/index/Ro#rooted\"\u003eIndex entries for sequences related to rooted trees\u003c/a\u003e",
				"\u003ca href=\"/index/Tra#trees\"\u003eIndex entries for sequences related to trees\u003c/a\u003e"
			],
			"maple": [
				"Hpj := proc(Hofxy,p,j)",
				"    coeftayl(Hofxy,x=0,p) ;",
				"    coeftayl(%,y=0,j) ;",
				"    simplify(%) ;",
				"end proc:",
				"Hxy := proc(x,y,pmax,hxyinit)",
				"    if pmax = 0 then",
				"        x*y ;",
				"    else",
				"        pp := 1;",
				"        for p from 1 to pmax do",
				"            t :=1 ;",
				"            for j from 1 to p do",
				"                t := t*(1+x^p*y^j+add(x^(k*p),k=2..pmax+1))^Hpj(hxyinit,p,j) ;",
				"            end do:",
				"            pp := pp*t ;",
				"        end do:",
				"        x*y*%/(1+x*y) ;",
				"    end if;",
				"end proc:",
				"hxy := Hxy(x,y,0,0) ;",
				"for pmax from 2 to 20 do",
				"    Hxy(x,y,pmax,hxy) ;",
				"    taylor(%,x=0,pmax+2) ;",
				"    convert(%,polynom) ;",
				"    taylor(%,y=0,pmax+2) ;",
				"    hxy := convert(%,polynom) ;",
				"    for p from 0 to pmax do",
				"        Ap := 0 ;",
				"        for j from 1 to p do",
				"            Ap := Ap+j*Hpj(hxy,p,j) ;",
				"        end do:",
				"        printf(\"%d,\",Ap) ;",
				"    end do:",
				"    print() ;",
				"end do: # _R. J. Mathar_, Apr 13 2019"
			],
			"mathematica": [
				"Hpj[Hofxy_, p_, j_] := SeriesCoefficient[SeriesCoefficient[Hofxy, {x, 0, p}] , {y, 0, j}];",
				"Hxy [x_, y_, pMax_, hxyinit_] := If [pMax == 0, x y, pp = 1; For[p = 1, p \u003c= pMax, p++, t = 1; For[j = 1, j \u003c= p, j++, t = t(1 + x^p y^j + Sum[x^(k*p), {k, 2, pMax + 1}])^Hpj[hxyinit, p, j]]; pp = pp t]; x*y* pp/(1 + x y)];",
				"hxy = Hxy[x, y, 0, 0];",
				"Reap[For[pMax = 2, pMax \u003c= terms - 1, pMax++, Print[\"pMax = \", pMax]; sx = Series[Hxy[x, y, pMax, hxy], {x, 0, pMax + 2}] // Normal; sy = Series[sx, {y, 0, pMax + 2}]; hxy = sy // Normal; For[p = 0, p \u003c= pMax, p++, Ap = 0; For[j = 1, j \u003c= p, j++, Ap = Ap + j Hpj[hxy, p, j]]; If[pMax == terms - 1, Print[Ap]; Sow[Ap]]]]][[2, 1]] (* _Jean-François Alcover_, Mar 22 2020, after _R. J. Mathar_ *)"
			],
			"xref": [
				"Cf. A005200."
			],
			"keyword": "nonn,easy",
			"offset": "1,6",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _R. J. Mathar_, Apr 13 2019"
			],
			"references": 2,
			"revision": 21,
			"time": "2020-03-22T10:01:28-04:00",
			"created": "1991-05-20T03:00:00-04:00"
		}
	]
}