{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A223550",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 223550,
			"data": "1,2,1,8,4,2,16,4,4,2,128,32,32,16,8,256,128,64,8,16,8,1024,512,128,32,64,32,16,2048,256,256,128,128,32,32,16,32768,4096,4096,2048,2048,512,512,256,128,65536,32768,8192,2048,4096,2048,1024,64,256,128",
			"name": "Triangle T(n,k), read by rows, giving the denominator of the coefficient of x^k in the Boros-Moll polynomial P_n(x) for n \u003e= 0 and 0 \u003c= k \u003c= n.",
			"comment": [
				"As Chen and Xia (2009) state, the Boros-Moll polynomial P_n(x) can be viewed as a Jacobi polynomial P_n^{a,b}(x) with a = n + (1/2) and b = -(n + (1/2)). For more information about the relation of this polynomial P_n(x) to the theory in Comtet (1967, pp. 81-83 and 85-86), see my comments for A223549. - _Petros Hadjicostas_, May 22 2020"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A223550/b223550.txt\"\u003eRows n = 0..50, flattened\u003c/a\u003e",
				"Tewodros Amdeberhan and Victor H. Moll, \u003ca href=\"http://arxiv.org/abs/0707.2118\"\u003e A formula for a quartic integral: a survey of old proofs and some new ones\u003c/a\u003e, arXiv:0707.2118 [math.CA], 2007.",
				"George Boros and Victor H. Moll, \u003ca href=\"http://dx.doi.org/10.1016/S0377-0427(99)00081-3\"\u003eAn integral hidden in Gradshteyn and Ryzhik\u003c/a\u003e, Journal of Computational and Applied Mathematics, 106(2) (1999), 361-368.",
				"William Y. C. Chen and Ernest X. W. Xia, \u003ca href=\"http://arxiv.org/abs/0806.4333\"\u003e The Ratio Monotonicity of the Boros-Moll Polynomials\u003c/a\u003e, arXiv:0806.4333 [math.CO], 2009.",
				"William Y. C. Chen and Ernest X. W. Xia, \u003ca href=\"https://doi.org/10.1090/S0025-5718-09-02223-6\"\u003e The Ratio Monotonicity of the Boros-Moll Polynomials\u003c/a\u003e, Mathematics of Computation, 78(268) (2009), 2269-2282.",
				"Louis Comtet, \u003ca href=\"https://www.jstor.org/stable/43667287\"\u003eFonctions génératrices et calcul de certaines intégrales\u003c/a\u003e, Publikacije Elektrotechnickog faculteta - Serija Matematika i Fizika, No. 181/196 (1967), 77-87."
			],
			"formula": [
				"A223549(n,k)/T(n,k) =  2^(-2*n)*Sum_{j=k..n} 2^j*binomial(2*n - 2*j, n - j)*binomial(n + j, j)*binomial(j, k) = 2^(-2*n)*A067001(n,n-k) for n \u003e= 0 and k = 0..n.",
				"P_n(x) = Sum_{k=0..n} (A223549(n,k)/T(n,k))*x^k = ((2*n)!/4^n/(n!)^2)*2F1([-n, n + 1], [1/2 - n], (x + 1)/2).",
				"From _Petros Hadjicostas_, May 22 2020: (Start)",
				"Recurrence for the polynomial: 4*n*(n - 1)*(x - 1)*P_n(x) = 4*(2*n - 1)*(n - 1)*(x^2 - 2)*P_{n-1}(x) + (16*(n - 1)^2 - 1)*(x + 1)*P_{n-2}(x).",
				"P_n(1) = Sum_{k=0..n} A223549(n,k)/T(n,k) = A334907(n)/(2^n*n!). (End)"
			],
			"example": [
				"P_3(x) = 77/16 + 43*x/4 + 35*x^2/4 + 5*x^3/2.",
				"From _Bruno Berselli_, Mar 22 2013: (Start)",
				"Triangle T(n,k) (with rows n \u003e= 0 and columns k=0..n) begins as follows:",
				"      1;",
				"      2,     1;",
				"      8,     4,    2;",
				"     16,     4,    4,    2;",
				"    128,    32,   32,   16,    8;",
				"    256,   128,   64,    8,   16,    8;",
				"   1024,   512,  128,   32,   64,   32,   16;",
				"   2048,   256,  256,  128,  128,   32,   32,  16;",
				"  32768,  4096, 4096, 2048, 2048,  512,  512, 256, 128;",
				"  65536, 32768, 8192, 2048, 4096, 2048, 1024,  64, 256, 128;",
				"  ... (End)"
			],
			"mathematica": [
				"t[n_, k_] := 2^(-2*n)*Sum[ 2^j*Binomial[2*n - 2*j, n-j]*Binomial[n+j, j]*Binomial[j, k], {j, k, n}]; Table[t[n, k] // Denominator, {n, 0, 9}, {k, 0, n}] // Flatten"
			],
			"program": [
				"(MAGMA) /* As triangle: */ [[Denominator(2^(-2*n)*\u0026+[2^j*Binomial(2*n-2*j, n-j)*Binomial(n+j, j)*Binomial(j, k): j in [k..n]]): k in [0..n]]: n in [0..10]]; // _Bruno Berselli_, Mar 22 2013"
			],
			"xref": [
				"Cf. A067001, A223549 (numerators), A334907."
			],
			"keyword": "nonn,easy,frac,tabl",
			"offset": "0,2",
			"author": "_Jean-François Alcover_, Mar 22 2013",
			"ext": [
				"Name edited by _Petros Hadjicostas_, May 22 2020"
			],
			"references": 6,
			"revision": 35,
			"time": "2020-05-25T07:54:49-04:00",
			"created": "2013-03-22T15:29:03-04:00"
		}
	]
}