{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A047235",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 47235,
			"data": "2,4,8,10,14,16,20,22,26,28,32,34,38,40,44,46,50,52,56,58,62,64,68,70,74,76,80,82,86,88,92,94,98,100,104,106,110,112,116,118,122,124,128,130,134,136,140,142,146,148,152,154,158,160,164,166,170,172,176,178,182,184,188,190,194,196,200,202,206",
			"name": "Numbers that are congruent to {2, 4} mod 6.",
			"comment": [
				"Apart from initial term(s), dimension of the space of weight 2n cuspidal newforms for Gamma_0( 19 ).",
				"Complement of A047273; A093719(a(n)) = 0. - _Reinhard Zumkeller_, Oct 01 2008",
				"One could prefix an initial term \"1\" (or not) and define this sequence through a(n+1) = a(n) + (a(n) mod 6). See A001651 for the analog with 3, A235700 (with 5), A047350 (with 7), A007612 (with 9) and A102039 (with 10). Using 4 or 8 yields a constant sequence from that term on. - _M. F. Hasler_, Jan 14 2014",
				"Nonnegative m such that m^2/6 + 1/3 is an integer. - _Bruno Berselli_, Apr 13 2017",
				"Numbers divisible by 2 but not by 3. - _David James Sycamore_, Apr 04 2018"
			],
			"link": [
				"David A. Corneth, \u003ca href=\"/A047235/b047235.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Chunhui Lai, \u003ca href=\"https://arxiv.org/abs/math/0308105\"\u003eA note on potentially K_4-e graphical sequences\u003c/a\u003e, arXiv:math/0308105 [math.CO], 2003.",
				"William A. Stein, \u003ca href=\"http://wstein.org/Tables/\"\u003eThe modular forms database\u003c/a\u003e.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,1,-1)."
			],
			"formula": [
				"a(n) = 2*A001651(n).",
				"n such that phi(3*n) = phi(2*n). - _Benoit Cloitre_, Aug 06 2003",
				"G.f.: 2*x*(1 + x + x^2)/((1 + x)*(1 - x)^2). a(n) = 3*n - 3/2 - (-1)^n/2. - _R. J. Mathar_, Nov 22 2008",
				"a(n) = 3*n + 5..n odd, 3*n + 4..n even a(n) = 6*floor((n+1)/2) + 3 + (-1)^n. - _Gary Detlefs_, Mar 02 2010",
				"a(n) = 6*n - a(n-1) - 6 (with a(1) = 2). - _Vincenzo Librandi_, Aug 05 2010",
				"a(n+1) = a(n) + (a(n) mod 6). - _M. F. Hasler_, Jan 14 2014",
				"Sum_{n\u003e=1} 1/a(n)^2 = Pi^2/27. - _Dimitris Valianatos_, Oct 10 2017",
				"a(n) = (6*n - (-1)^n - 3)/2. - _Ammar Khatab_, Aug 23 2020",
				"Sum_{n\u003e=1} (-1)^(n+1)/a(n) = Pi/(6*sqrt(3)). - _Amiram Eldar_, Dec 11 2021"
			],
			"maple": [
				"seq(6*floor((n+1)/2) + 3 + (-1)^n, n=1..67); # _Gary Detlefs_, Mar 02 2010"
			],
			"mathematica": [
				"Flatten[Table[{6n - 4, 6n - 2}, {n, 40}]] (* _Alonso del Arte_, Oct 27 2014 *)"
			],
			"program": [
				"(MAGMA) [ n eq 1 select 2 else Self(n-1)+2*(1+n mod 2): n in [1..70] ]; // _Klaus Brockhaus_, Dec 13 2008",
				"(PARI) a(n)=(n-1)\\2*6+3+(-1)^n \\\\ _Charles R Greathouse IV_, Jul 01 2013",
				"(PARI) first(n) = my(v = vector(n, i, 3*i - 1)); forstep(i = 2, n, 2, v[i]--); v \\\\ _David A. Corneth_, Oct 20 2017"
			],
			"xref": [
				"Equals 2*A001651.",
				"Cf. A007310 ((6*n+(-1)^n-3)/2). - _Bruno Berselli_, Jun 24 2010"
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_",
			"references": 35,
			"revision": 85,
			"time": "2021-12-11T04:33:01-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}