{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A307884",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 307884,
			"data": "1,1,1,1,0,1,1,-1,-2,1,1,-2,-3,0,1,1,-3,-2,11,6,1,1,-4,1,28,1,0,1,1,-5,6,45,-74,-81,-20,1,1,-6,13,56,-255,-92,141,0,1,1,-7,22,55,-554,477,1324,363,70,1,1,-8,33,36,-959,2376,2689,-3656,-1791,0,1",
			"name": "Square array T(n,k), n \u003e= 0, k \u003e= 0, read by antidiagonals, where column k is the expansion of 1/sqrt(1 + 2*(k-1)*x + ((k+1)*x)^2).",
			"comment": [
				"Column k is the diagonal of the rational function 1 / ((1-x)*(1-y) + k*x*y). - _Seiichi Manyama_, Jul 11 2020",
				"More generally, column k is the diagonal of the rational function r / ((1-r*x)*(1-r*y) + r-1 + (k-r+1)*r*x*y) for any nonzero real number r. - _Seiichi Manyama_, Jul 22 2020"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A307884/b307884.txt\"\u003eAntidiagonals n = 0..139, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) is the coefficient of x^n in the expansion of (1 - (k-1)*x - k*x^2)^n.",
				"T(n,k) = Sum_{j=0..n} (-k)^j * binomial(n,j)^2.",
				"T(n,k) = Sum_{j=0..n} (-k-1)^(n-j) * binomial(n,j) * binomial(n+j,j).",
				"n * T(n,k) = -(k-1) * (2*n-1) * T(n-1,k) - (k+1)^2 * (n-1) * T(n-2,k)."
			],
			"example": [
				"Square array begins:",
				"  1,   1,   1,    1,    1,    1,      1, ...",
				"  1,   0,  -1,   -2,   -3,   -4,     -5, ...",
				"  1,  -2,  -3,   -2,    1,    6,     13, ...",
				"  1,   0,  11,   28,   45,   56,     55, ...",
				"  1,   6,   1,  -74, -255, -554,   -959, ...",
				"  1,   0, -81,  -92,  477, 2376,   6475, ...",
				"  1, -20, 141, 1324, 2689, -804, -20195, ..."
			],
			"mathematica": [
				"T[n_, k_] := Sum[If[k == j == 0, 1, (-k)^j] * Binomial[n, j]^2, {j, 0, n}]; Table[T[k, n - k], {n, 0, 10}, {k, 0, n}] // Flatten (* _Amiram Eldar_, May 13 2021 *)"
			],
			"xref": [
				"Columns k=2..4 give (-1)^n * A098332, A116091, (-1)^n * A098341.",
				"Main diagonal gives A307885.",
				"T(n,n-1) gives A335310.",
				"Cf. A307883, A336179."
			],
			"keyword": "sign,tabl",
			"offset": "0,9",
			"author": "_Seiichi Manyama_, May 02 2019",
			"references": 9,
			"revision": 41,
			"time": "2021-05-13T02:36:05-04:00",
			"created": "2019-05-03T03:06:12-04:00"
		}
	]
}