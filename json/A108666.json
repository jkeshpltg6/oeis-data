{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A108666",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 108666,
			"data": "0,1,8,57,384,2505,16008,100849,628736,3888657,23900040,146146473,889928064,5399971161,32668236552,197123362785,1186790473728,7131032334369,42773183020296,256161548120857,1531966218561920,9150330147133161,54591847064667528,325361790187810257",
			"name": "Number of (1, 1)-steps in all Delannoy paths of length n.",
			"comment": [
				"A Delannoy path of length n is a path from (0,0) to (n,n), consisting of steps E =(1,0), N = (0,1) and D = (1,1)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A108666/b108666.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e(terms 0..200 from Vincenzo Librandi)",
				"Luca Ferrari and Emanuele Munarini, \u003ca href=\"http://arxiv.org/abs/1203.6792\"\u003eEnumeration of edges in some lattices of paths\u003c/a\u003e, arXiv:1203.6792 [math.CO], 2012 and \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Ferrari/ferrari.html\"\u003eJ. Int. Seq. 17 (2014) #14.1.5\u003c/a\u003e",
				"Robert A. Sulanke, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL6/Sulanke/delannoy.html\"\u003eObjects Counted by the Central Delannoy Numbers\u003c/a\u003e, Journal of Integer Sequences, Volume 6, 2003, Article 03.1.5."
			],
			"formula": [
				"a(n) = Sum_{k=0..n} k*A104684(k).",
				"a(n) = Sum_{k=1..n} k*binomial(n, k)*binomial(2*n-k, n).",
				"G.f.: x*(1-x)/(1-6*x+x^2)^(3/2).",
				"D-finite with recurrence (n-1)*(2*n-3)*a(n) = 4*(3*n^2-6*n+2)*a(n-1) - (n-1)*(2*n-1)*a(n-2). - _Vaclav Kotesovec_, Oct 18 2012",
				"a(n) ~ (3+2*sqrt(2))^n*sqrt(n)/(2^(7/4)*sqrt(Pi)). - _Vaclav Kotesovec_, Oct 18 2012",
				"a(n) = n^2*hypergeom([-n+1, -n+1], [2], 2). - _Peter Luschny_, Jan 20 2020"
			],
			"example": [
				"a(2)=8 because in the 13 (=A001850(2)) Delannoy paths of length 2, namely, DD, DNE,DEN,NED,END,NDE,EDN,NENE,NEEN,ENNE,ENEN,NNEE and EENN, we have a total of eight D steps."
			],
			"maple": [
				"a := n -\u003e add(k*binomial(n,k)*binomial(2*n-k,n),k=1..n): seq(a(n),n=0..24);",
				"# Alternative:",
				"a := n -\u003e n^2*hypergeom([-n+1, -n+1], [2], 2):",
				"seq(simplify(a(n)), n=0..24); # _Peter Luschny_, Jan 20 2020"
			],
			"mathematica": [
				"CoefficientList[Series[x*(1-x)/(1-6*x+x^2)^(3/2), {x, 0, 20}], x] (* _Vaclav Kotesovec_, Oct 18 2012 *)"
			],
			"program": [
				"(PARI) for(n=0,25, print1(sum(k=0,n, k*binomial(n,k)*binomial(2*n-k,n)), \", \")) \\\\ _G. C. Greubel_, Jan 31 2017"
			],
			"xref": [
				"a(n)/n = A047781(n) (for n \u003e= 1).",
				"Cf. A001850, A104684."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Emeric Deutsch_, Jul 07 2005",
			"references": 9,
			"revision": 33,
			"time": "2020-05-25T05:07:20-04:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}