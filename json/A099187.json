{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A099187",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 99187,
			"data": "1,20,34220,180318314012420,26383476911029432816173777932463879690054620",
			"name": "Iterated dodecahedral numbers, starting with a(1) = 20.",
			"comment": [
				"This need not start with Dod(2) = 20. For example, if a(1) = Dod(3) = 84, then a(2) = Dod(Dod(3)) = Dod(84) = 84*(9*84^2 - 9*84 + 2)/2 = 2635500; a(3) = Dod(Dod(Dod(3))) = Dod(2635500) = 82376134843569010500. The core sequence is not to be confused with Rhombic dodecahedral numbers."
			],
			"reference": [
				"H. S. M. Coxeter, \"Regular Polytopes\", New York: Dover, 1973.",
				"J. V. Post, \"Iterated Triangular Numbers\", preprint."
			],
			"link": [
				"Michel Marcus, \u003ca href=\"/A099187/b099187.txt\"\u003eTable of n, a(n) for n = 0..6\u003c/a\u003e",
				"Hyun Kwang Kim, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9939-02-06710-2\"\u003eOn Regular Polytope Numbers\u003c/a\u003e, Proc. Amer. Math. Soc., 131 (2003), 65-75.",
				"J. V. Post, \u003ca href=\"http://magicdragon.com/poly.html\"\u003eTable of Polytope Numbers, Sorted, Through 1,000,000\u003c/a\u003e."
			],
			"formula": [
				"From the definition of dodecahedral numbers, for n\u003e1, Dod(n) = n*(9*n^2-9*n+2)/2 we have a(0) = 1, a(1) = Dod(2) = 20; a(k+1) = Dod(a(k))."
			],
			"example": [
				"a(0) = 1;",
				"a(1) = Dod(2) = the 2nd dodecahedral number = 2*(9*2^2-9*2+2)/2 = 20;",
				"a(2) = Dod(Dod(2)) = the 20th dodecahedral number = 20*(9*20^2-9*20+2)/2 = 34220."
			],
			"mathematica": [
				"Dod[n_]:= n*(9*n^2-9*n+2)/2; a[n_]:= If[n==0, Dod[1], If[n==1, Dod[2], Dod[a[n-1]]]]; Table[a[n], {n, 0, 4}] (* _G. C. Greubel_, Mar 22 2019 *)"
			],
			"program": [
				"(PARI) dod(n) = n*(9*n^2-9*n+2)/2;",
				"a(n) = if (n==0, 1, if (n==1, dod(2), dod(a(n-1)))); \\\\ _Michel Marcus_, Dec 14 2015"
			],
			"xref": [
				"Cf. A007501, A006566."
			],
			"keyword": "easy,nonn",
			"offset": "0,2",
			"author": "_Jonathan Vos Post_, Nov 15 2004",
			"references": 1,
			"revision": 15,
			"time": "2019-03-22T09:27:07-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}