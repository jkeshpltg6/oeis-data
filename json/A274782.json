{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A274782",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 274782,
			"data": "1,2,12,80,580,4452,35364,288024,2389860,20117240,171297412,1472316144,12753610324,111204119600,975106366680,8592062844480,76030953610980,675323181536760,6018366674614800,53794511697141600,482124714647540580,4331431183551057960,38999393295271719960",
			"name": "Diagonal of the rational function 1/(1 - x - x y - x z - y z).",
			"comment": [
				"Annihilating differential operator: x*(2*x+5)*(27*x^3+20*x^2+7*x-1)*Dx^2 + (162*x^4+620*x^3+314*x^2+70*x-5)*Dx + 48*x^3+220*x^2+80*x+10."
			],
			"link": [
				"Gheorghe Coserea, \u003ca href=\"/A274782/b274782.txt\"\u003eTable of n, a(n) for n = 0..310\u003c/a\u003e",
				"A. Bostan, S. Boukraa, J.-M. Maillard, J.-A. Weil, \u003ca href=\"http://arxiv.org/abs/1507.03227\"\u003eDiagonals of rational functions and selected differential Galois groups\u003c/a\u003e, arXiv preprint arXiv:1507.03227 [math-ph], 2015.",
				"Steffen Eger, \u003ca href=\"http://arxiv.org/abs/1511.00622\"\u003eOn the Number of Many-to-Many Alignments of N Sequences\u003c/a\u003e, arXiv:1511.00622 [math.CO], 2015.",
				"Jacques-Arthur Weil, \u003ca href=\"http://www.unilim.fr/pages_perso/jacques-arthur.weil/diagonals/\"\u003eSupplementary Material for the Paper \"Diagonals of rational functions and selected differential Galois groups\"\u003c/a\u003e"
			],
			"formula": [
				"G.f.: hypergeom([1/12, 5/12], [1], 1728*x^5*(27*x^3+20*x^2+7*x-1)/(8*x^2+8*x-1)^3)/(1-8*x-8*x^2)^(1/4).",
				"0 = x*(2*x+5)*(27*x^3+20*x^2+7*x-1)*y'' + (162*x^4+620*x^3+314*x^2+70*x-5)*y' + (48*x^3+220*x^2+80*x+10)*y, where y is the g.f.",
				"Recurrence: n^2*(29*n - 49)*a(n) = (203*n^3 - 546*n^2 + 393*n - 90)*a(n-1) + 20*(29*n^3 - 107*n^2 + 123*n - 42)*a(n-2) + 3*(3*n - 7)*(3*n - 5)*(29*n - 20)*a(n-3). - _Vaclav Kotesovec_, Jul 07 2016",
				"a(n) = Sum_{k = 0..n} C(n,k)*C(n+k,k)*C(k,n-k) (apply Eger, Theorem 3 to the set of column vectors S = {[1,0,0], [1,1,0], [1,0,1], [0,1,1]}). - _Peter Bala_, Jan 26 2018",
				"a(n) = [x^n] Legendre_P(n,1+2*x+2*x^2). - _Peter Bala_, Dec 24 2020"
			],
			"maple": [
				"with(combinat):",
				"seq(add(binomial(n,k)*binomial(n+k,k)*binomial(k,n-k), k = 0..n), n = 0..20); # _Peter Bala_, Jan 26 2018"
			],
			"program": [
				"(PARI)",
				"my(x='x, y='y, z='z);",
				"R = 1/(1 - x - x*y - x*z - y*z);",
				"diag(n, expr, var) = {",
				"  my(a = vector(n));",
				"  for (i = 1, #var, expr = taylor(expr, var[#var - i + 1], n));",
				"  for (k = 1, n, a[k] = expr;",
				"       for (i = 1, #var, a[k] = polcoeff(a[k], k-1)));",
				"  return(a);",
				"};",
				"diag(10, R, [x, y, z])",
				"(PARI) \\\\ system(\"wget http://www.jjj.de/pari/hypergeom.gpi\");",
				"read(\"hypergeom.gpi\");",
				"N = 20; x = 'x + O('x^N);",
				"Vec(hypergeom([1/12, 5/12], [1], 1728*x^5*(27*x^3+20*x^2+7*x-1)/(8*x^2+8*x-1)^3, N)/(1-8*x-8*x^2)^(1/4))"
			],
			"xref": [
				"Cf. A268545-A268555."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Gheorghe Coserea_, Jul 06 2016",
			"references": 1,
			"revision": 23,
			"time": "2020-12-27T19:42:21-05:00",
			"created": "2016-07-07T02:35:20-04:00"
		}
	]
}