{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007501",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7501,
			"id": "M0818",
			"data": "2,3,6,21,231,26796,359026206,64449908476890321,2076895351339769460477611370186681,2156747150208372213435450937462082366919951682912789656986079991221",
			"name": "a(0) = 2; for n \u003e= 0, a(n+1) = a(n)*(a(n)+1)/2.",
			"comment": [
				"Number of nonisomorphic complete binary trees with leaves colored using two colors. - _Brendan McKay_, Feb 01 2001",
				"Let {t(k)} be the triangular numbers (A000217). Then a(0) = 2; for n\u003e0, a(n) = t(a(n-1)). - _Jonathan Vos Post_, Nov 13 2004",
				"With a(0) = 2, a(n+1) is the number of possible distinct sums between any number of elements in {1,...,a(n)}. - _Derek Orr_, Dec 13 2014"
			],
			"reference": [
				"W. H. Cutler, Subdividing a Box into Completely Incongruent Boxes, J. Rec. Math., 12 (1979), 104-111.",
				"J. V. Post, \"Iterated Triangular Numbers\", preprint.",
				"J. V. Post, \"Iterated Polygonal Numbers\", preprint.",
				"J. V. Post, \"Triangular Carmichael Numbers: The First 22 Identified\", preprint.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A007501/b007501.txt\"\u003eTable of n, a(n) for n = 0..12\u003c/a\u003e",
				"G. L. Honaker, Jr., \u003ca href=\"http://primes.utm.edu/curios/page.php?number_id=2321\"\u003e41041 (another Prime Pages' Curiosity)\u003c/a\u003e",
				"J. C. Kieffer, \u003ca href=\"http://dx.doi.org/10.1109/CCP.2011.36\"\u003eHierarchical Type Classes and Their Entropy Functions\u003c/a\u003e, in 2011 First International Conference on Data Compression, Communications and Processing, pp. 246-254; Digital Object Identifier: 10.1109/CCP.2011.36.",
				"J. V. Post, \u003ca href=\"http://www.magicdragon.com/math.html\"\u003eMath Pages\u003c/a\u003e",
				"Stephan Wagner, \u003ca href=\"http://www.cs.sun.ac.za/~swagner/balanced.pdf\"\u003eEnumeration of highly balanced trees\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A006893(n+1) + 1.",
				"a(n+1) = A000217(a(n)). - _Reinhard Zumkeller_, Aug 15 2013",
				"a(n) ~ 2 * c^(2^n), where c = 1.34576817070125852633753712522207761954658547520962441996... . - _Vaclav Kotesovec_, Dec 17 2014"
			],
			"example": [
				"Example for depth 2 (the nonisomorphic possibilites are AAAA, AAAB, AABB, ABAB, ABBB, BBBB):",
				".........o",
				"......../.\\",
				"......./...\\",
				"......o.....o",
				"...../.\\.../.\\",
				"..../...\\./...\\",
				"....A...B.B...B"
			],
			"mathematica": [
				"f[n_Integer] := n(n + 1)/2; NestList[f, 2, 10]"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c1,2,a(n-1)*(1+a(n-1))/2)",
				"(Haskell)",
				"a007501 n = a007501_list !! n",
				"a007501_list = iterate a000217 2  -- _Reinhard Zumkeller_, Aug 15 2013"
			],
			"xref": [
				"Cf. A000217, A006893.",
				"Cf. A129440.",
				"Cf. A013589 (start=4), A050542 (start=5), A050548 (start=7), A050536 (start=8), A050909 (start=9)."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_, _Robert G. Wilson v_",
			"references": 35,
			"revision": 47,
			"time": "2015-05-04T21:05:31-04:00",
			"created": "1994-09-19T03:00:00-04:00"
		}
	]
}