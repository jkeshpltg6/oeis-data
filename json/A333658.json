{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A333658",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 333658,
			"data": "0,1,2,3,4,5,6,8,7,9,14,15,12,13,10,11,16,17,18,20,19,21,22,23,24,26,25,27,28,29,30,36,32,38,66,68,31,37,33,39,67,69,62,63,44,45,74,75,96,98,97,99,104,105,126,128,127,129,134,135,60,61,42,43,72,73",
			"name": "a(n) is the greatest number m not yet in the sequence such that the primorial base expansions of n and of m have the same digits (up to order but with multiplicity).",
			"comment": [
				"Leading 0's are ignored.",
				"This sequence is a permutation of the nonnegative integers, which preserves the number of digits (A235224) and the sum of digits (A276150) in primorial base."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A333658/b333658.txt\"\u003eTable of n, a(n) for n = 0..30029\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A333658/a333658.png\"\u003eScatterplot of the first 2*3*5*7*11*13*17 terms\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A333658/a333658.gp.txt\"\u003ePARI program for A333658\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#primorialbase\"\u003eIndex entries for sequences related to primorial base\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(A002110(n)) = A002110(n) for any n \u003e= 0."
			],
			"example": [
				"For n = 42:",
				"- the primorial base representation of 42 is \"1200\",",
				"- there are five numbers m with the same multiset of digits:",
				"    m   prim(m)",
				"    --  -------",
				"    34  \"1020\"",
				"    42  \"1200\"",
				"    61  \"2001\"",
				"    62  \"2010\"",
				"    66  \"2100\"",
				"- so a(34) = 66,",
				"     a(42) = 62,",
				"     a(61) = 61,",
				"     a(62) = 42,",
				"     a(66) = 34."
			],
			"program": [
				"(PARI) See Links section."
			],
			"xref": [
				"See A333659 and A337598 for similar sequences.",
				"Cf. A002110, A235224, A276150."
			],
			"keyword": "nonn,look,base",
			"offset": "0,3",
			"author": "_Rémy Sigrist_, Sep 02 2020",
			"references": 4,
			"revision": 20,
			"time": "2020-09-08T15:17:57-04:00",
			"created": "2020-09-08T02:08:28-04:00"
		}
	]
}