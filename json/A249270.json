{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A249270",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 249270,
			"data": "2,9,2,0,0,5,0,9,7,7,3,1,6,1,3,4,7,1,2,0,9,2,5,6,2,9,1,7,1,1,2,0,1,9,4,6,8,0,0,2,7,2,7,8,9,9,3,2,1,4,2,6,7,1,9,7,7,2,6,8,2,5,3,3,1,0,7,7,3,3,7,7,2,1,2,7,7,6,6,1,2,4,1,9,0,1,7,8,1,1,2,3,1,7,5,8,3,7,4,2,2,9,8,3",
			"name": "Decimal expansion of lim_{n-\u003eoo} (1/n)*Sum_{k=1..n} smallest prime not dividing k.",
			"comment": [
				"The old definition was \"Decimal expansion of the mean value over all positive integers of the least prime not dividing a given integer.\"",
				"The integer parts of the sequence having this constant as starting value and thereafter x[n+1] = (frac(x[n])+1)*floor(x[n]), where floor and frac are integer and fractional part, are exactly the sequence of the prime numbers: see the Grime-Haran Numberphile video for details. - _M. F. Hasler_, Nov 28 2020"
			],
			"reference": [
				"Steven R. Finch, Meissel-Mertens constants: Quadratic residues, Mathematical Constants, Cambridge Univ. Press, 2003, pp. 96—98."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A249270/b249270.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Steven R. Finch, \u003ca href=\"/A232927/a232927.pdf\"\u003eAverage least nonresidues\u003c/a\u003e, December 4, 2013. [Cached copy, with permission of the author]",
				"Steven R. Finch, \u003ca href=\"https://doi.org/10.1017/9781316997741\"\u003eMathematical Constants II\u003c/a\u003e, Encyclopedia of Mathematics and Its Applications, Cambridge University Press, Cambridge, 2018, p. 171.",
				"Dylan Fridman, Juli Garbulsky, Bruno Glecer, James Grime and Massi Tron Florentin, \u003ca href=\"https://doi.org/10.1080/00029890.2019.1530554\"\u003eA Prime-Representing Constant\u003c/a\u003e, The American Mathematical Monthly, Vol. 126, No. 1 (2019), pp. 70-73, \u003ca href=\"https://www.researchgate.net/publication/330746181_A_Prime-Representing_Constant\"\u003eResearchGate link\u003c/a\u003e.",
				"James Grime and Brady Haran, \u003ca href=\"https://youtube.com/watch?v=_gCKX6VMvmU\"\u003e2.920050977316\u003c/a\u003e, Numberphile video, Nov 26 2020.",
				"Paul Pollack, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2011.12.015\"\u003eThe average least quadratic nonresidue modulo m and other variations on a theme of Erdős\u003c/a\u003e, J. Number Theory, Vol. 132, No. 6 (2012), pp. 1185-1202.",
				"Juan L. Varona, \u003ca href=\"https://arxiv.org/abs/2012.11750\"\u003eA Couple of Transcendental Prime-Representing Constants\u003c/a\u003e, arXiv:2012.11750 [math.NT], 2020.",
				"I. A. Weinstein, \u003ca href=\"https://arxiv.org/abs/2101.00094\"\u003eFamily of prime-representing constants: use of the ceiling function\u003c/a\u003e, arXiv:2101.00094 [math.GM], 2021."
			],
			"formula": [
				"Sum_{k \u003e= 1} (p_k - 1)/(p_1 p_2 ... p_{k-1}), where p_k is the k-th prime number.",
				"Sum_{k \u003e= 0} 1/A034386(k). - _Jani Melik_, Jul 22 2015",
				"From _Amiram Eldar_, Oct 29 2020: (Start)",
				"Equals lim_{n-\u003eoo} (1/n) * Sum_{k=1..n} A053669(k).",
				"Equals 2 + Sum_{n\u003e=1} (prime(n+1)-prime(n))/prime(n)# = 2 + Sum_{n\u003e=1} A001223(n)/A002110(n). (End)"
			],
			"example": [
				"2.9200509773161347120925629171120194680027278993214267..."
			],
			"mathematica": [
				"digits = 103; Clear[s]; s[m_] := s[m] = Sum[(Prime[k] - 1)/Product[Prime[j], {j, 1, k - 1}] // N[#, digits + 100]\u0026, {k, 1, m}]; s[10]; s[m = 20]; While[RealDigits[s[m]] != RealDigits[s[m/2]], m = 2*m]; RealDigits[s[m], 10, digits] // First"
			],
			"program": [
				"(Sage)",
				"def sharp_primorial(n): return sloane.A002110(prime_pi(n));",
				"@CachedFunction",
				"def spv(n):",
				"    b = 0",
				"    for i in (0..n):",
				"        b += 1 / sharp_primorial(i)",
				"    return b",
				"N(spv(300), digits=108) # _Jani Melik_, Jul 22 2015"
			],
			"xref": [
				"Cf. A000040, A001223, A002110, A034386, A053669, A098990, A232927."
			],
			"keyword": "nonn,cons",
			"offset": "1,1",
			"author": "_Jean-François Alcover_, Oct 24 2014",
			"ext": [
				"Definition revised by _N. J. A. Sloane_, Nov 29 2020"
			],
			"references": 14,
			"revision": 82,
			"time": "2021-06-13T13:21:46-04:00",
			"created": "2014-10-24T05:16:56-04:00"
		}
	]
}