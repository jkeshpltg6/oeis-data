{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A334231",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 334231,
			"data": "1,2,2,3,3,3,4,4,6,4,5,5,15,5,5,6,6,6,6,15,6,7,7,7,7,35,7,7,8,8,12,8,10,12,14,8,9,9,9,9,45,9,21,18,9,10,10,15,10,10,15,35,10,45,10,11,11,33,11,11,33,77,11,99,11,11,12,12,12,12,15,12,14,12",
			"name": "Triangle read by rows: T(n,k) gives the join of n and k in the graded lattice of the positive integers defined by covering relations \"n covers (n - n/p)\" for all divisors p of n.",
			"comment": [
				"The poset of the positive integers is defined by covering relations \"n covers (n - n/p)\" for all divisors p of n.",
				"n appears A332809(n) times in row n."
			],
			"link": [
				"Peter Kagey, \u003ca href=\"/A334231/b334231.txt\"\u003eTable of n, a(n) for n = 1..10011\u003c/a\u003e (first 141 rows, flattened)",
				"Math Stack Exchange, \u003ca href=\"https://math.stackexchange.com/a/3640072/121988\"\u003eDoes a graded poset on the positive integers generated from subtracting factors define a lattice?\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Semilattice\"\u003eSemilattice\u003c/a\u003e"
			],
			"formula": [
				"T(n,1) = T(n,n) = n. T(n, 2) = n for n \u003e= 2.",
				"T(x,y) \u003c= lcm(x,y) for any x,y because x is in same chain with lcm(x,y), and y is in same chain with lcm(x,y).",
				"Moreover, empirically it looks like T(x,y) divides lcm(x,y)."
			],
			"example": [
				"The interval [1,15] illustrates that, for example, T(12, 10) = T(6, 5) = 15, T(12, 4) = 12, T(8, 5) = 10, T(3, 1) = 3, etc.",
				"      15",
				"     _/ \\_",
				"    /     \\",
				"  10       12",
				"  | \\_   _/ |",
				"  |   \\ /   |",
				"  5    8    6",
				"   \\_  |  _/|",
				"     \\_|_/  |",
				"       4    3",
				"       |  _/",
				"       |_/",
				"       2",
				"       |",
				"       |",
				"       1",
				"Triangle begins:",
				"  n\\k|  1  2  3  4  5  6  7  8  9 10  11 12 13 14",
				"  ---+-------------------------------------------",
				"   1 |  1",
				"   2 |  2  2",
				"   3 |  3  3  3",
				"   4 |  4  4  6  4",
				"   5 |  5  5 15  5  5",
				"   6 |  6  6  6  6 15  6",
				"   7 |  7  7  7  7 35  7  7",
				"   8 |  8  8 12  8 10 12 14  8",
				"   9 |  9  9  9  9 45  9 21 18  9",
				"  10 | 10 10 15 10 10 15 35 10 45 10",
				"  11 | 11 11 33 11 11 33 77 11 99 11  11",
				"  12 | 12 12 12 12 15 12 14 12 18 15  33 12",
				"  13 | 13 13 13 13 65 13 91 13 39 65 143 13 13",
				"  14 | 14 14 14 14 35 14 14 14 21 35  77 14 91 14"
			],
			"program": [
				"(PARI)",
				"\\\\ This just returns the least (in a normal sense) number x such that both n and k are in its set of descendants:",
				"up_to = 105;",
				"buildWdescsets(up_to) = { my(v=vector(up_to)); v[1] = Set([1]); for(n=2,up_to, my(f=factor(n)[, 1]~, s=Set([n])); for(i=1,#f,s = setunion(s,v[n-(n/f[i])])); v[n] = s); (v); }",
				"vdescsets = buildWdescsets(100*up_to); \\\\ XXX - Think about a safe limit here!",
				"A334231tr(n,k) = for(i=max(n,k),oo,if(setsearch(vdescsets[i],n)\u0026\u0026setsearch(vdescsets[i],k),return(i)));",
				"A334231list(up_to) = { my(v = vector(up_to), i=0); for(n=1,oo, for(k=1,n, i++; if(i \u003e up_to, return(v)); v[i] = A334231tr(n,k))); (v); };",
				"v334231 = A334231list(up_to);",
				"A334231(n) = v334231[n]; \\\\ _Antti Karttunen_, Apr 19 2020"
			],
			"xref": [
				"Cf. A051173, A332809, A333123, A334184, A334230."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Peter Kagey_, Apr 19 2020",
			"references": 4,
			"revision": 35,
			"time": "2020-04-28T03:12:29-04:00",
			"created": "2020-04-28T00:45:12-04:00"
		}
	]
}