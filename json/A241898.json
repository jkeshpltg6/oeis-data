{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A241898",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 241898,
			"data": "1,1,1,2,1,1,1,2,3,1,1,2,2,1,1,4,2,3,1,2,2,2,1,2,5,2,3,2,2,1,2,4,2,3,1,6,2,2,1,2,4,2,3,2,3,1,2,4,7,5,1,4,2,3,1,2,4,3,3,2,5,2,3,8,4,4,3,4,2,3,2,6,4,5,5,3,4,2,3,4,9,4,3,4,6,5,2",
			"name": "a(n) is the largest integer such that n = a(n)^2 + ... is a decomposition of n into a sum of at most four nondecreasing squares.",
			"comment": [
				"This differs from A191090 only for n\u003e=30 because 30 cannot be written as a sum of at most four squares without using 1^2, but 30 can be written as a sum of five nondecreasing squares: 2^2 + 2^2 + 2^2 + 3^2 + 3^2, making A191090(30)=2.",
				"By Lagrange's Theorem every number can be written as a sum of four squares.  Can the same be said of the set of {a^2|a is any integer not equal to 7}? From the data that I have, it would seem that a(n) is greater than 7 for all n\u003e599.  If this could be proved, it would only remain to check if all the numbers up to 599 can be written as the sum of 4 squares none of which is 7^2."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A241898/b241898.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"30 can be written as the sum of at most 4 nondecreasing squares in the following ways: 1^2 + 2^2 + 5^2 or 1^2 + 2^2 + 3^2 + 4^2. Therefore, a(30)=1."
			],
			"maple": [
				"b:= proc(n, i, t) option remember; n=0 or t\u003e0 and",
				"       i^2\u003c=n and (b(n, i+1, t) or b(n-i^2, i, t-1))",
				"    end:",
				"a:= proc(n) local k;",
				"      for k from isqrt(n) by -1 do",
				"        if b(n, k, 4) then return k fi",
				"      od",
				"    end:",
				"seq(a(n), n=1..100);  # _Alois P. Heinz_, May 25 2014"
			],
			"mathematica": [
				"For[i=0,i\u003c=7^4,i++,a[i]={}];",
				"For[i1=0,i1\u003c=7,i1++,",
				"For[i2=0,i2\u003c=7,i2++,",
				"For[i3=0,i3\u003c=7,i3++,",
				"For[i4=0,i4\u003c=7,i4++,",
				"sumOfSquares=i1^2+i2^2+i3^2+i4^2;",
				"smallestSquare=Min[DeleteCases[{i1,i2,i3,i4},0]];",
				"a[sumOfSquares]=Union[{smallestSquare},a[sumOfSquares]] ]]]];",
				"Table[Max[a[i]],{i,1,50}]"
			],
			"xref": [
				"Cf. A191090."
			],
			"keyword": "nonn,look",
			"offset": "1,4",
			"author": "_Moshe Shmuel Newman_, May 15 2014",
			"references": 1,
			"revision": 48,
			"time": "2014-05-25T19:13:46-04:00",
			"created": "2014-05-25T19:13:04-04:00"
		}
	]
}