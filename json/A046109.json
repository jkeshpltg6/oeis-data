{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A046109",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 46109,
			"data": "1,4,4,4,4,12,4,4,4,4,12,4,4,12,4,12,4,12,4,4,12,4,4,4,4,20,12,4,4,12,12,4,4,4,12,12,4,12,4,12,12,12,4,4,4,12,4,4,4,4,20,12,12,12,4,12,4,4,12,4,12,12,4,4,4,36,4,4,12,4,12,4,4,12,12,20,4,4,12,4,12,4,12,4,4,36",
			"name": "Number of lattice points (x,y) on the circumference of a circle of radius n with center at (0,0).",
			"comment": [
				"Also number of Gaussian integers x + yi having absolute value n. - _Alonso del Arte_, Feb 11 2012"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A046109/b046109.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Gilleland, \u003ca href=\"/selfsimilar.html\"\u003eSome Self-Similar Integer Sequences\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CircleLatticePoints.html\"\u003eCircle Lattice Points\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 8*A046080(n) + 4 for n \u003e 0.",
				"a(n) = A004018(n^2).",
				"a(A084647(k)) = 28. - _Jean-Christophe Hervé_, Dec 01 2013",
				"a(A084648(k)) = 36. - _Jean-Christophe Hervé_, Dec 01 2013",
				"a(A084649(k)) = 44. - _Jean-Christophe Hervé_, Dec 01 2013",
				"a(n) = 4 * Product_{i=1..k} (2*e_i + 1) for n \u003e 0, given that p_i^e_i is the i-th factor of n with p_i = 1 mod 4. - _Orson R. L. Peters_, Jan 31 2017",
				"a(n) = [x^(n^2)] theta_3(x)^2, where theta_3() is the Jacobi theta function. - _Ilya Gutkovskiy_, Apr 20 2018"
			],
			"example": [
				"a(5) = 12 because the circumference of the circle with radius 5 will pass through the twelve points (5, 0), (4, 3), (3, 4), (0, 5), (-3, 4), (-4, 3), (-5, 0), (-4, -3), (-3, -4), (0, -5), (3, -4) and (4, -3). Alternatively, we can say the twelve Gaussian integers 5, 4 + 3i, ... , 4 - 3i all have absolute value of 5."
			],
			"maple": [
				"N:= 1000: # to get a(0) to a(N)",
				"A:= Array(0..N):",
				"A[0]:= 1:",
				"for x from 1 to N do",
				"  A[x]:= A[x]+4;",
				"  for y from 1 to min(x-1,floor(sqrt(N^2-x^2))) do",
				"     z:= x^2+y^2;",
				"     if issqr(z) then",
				"       t:= sqrt(z);",
				"       A[t]:= A[t]+8;",
				"     fi",
				"  od",
				"od:",
				"seq(A[i],i=0..N); # _Robert Israel_, May 08 2015"
			],
			"mathematica": [
				"Table[Length[Select[Flatten[Table[r + I i, {r, -n, n}, {i, -n, n}]], Abs[#] == n \u0026]], {n, 0, 49}] (* _Alonso del Arte_, Feb 11 2012 *)"
			],
			"program": [
				"(Haskell)",
				"a046109 n = length [(x,y) | x \u003c- [-n..n], y \u003c- [-n..n], x^2 + y^2 == n^2]",
				"-- _Reinhard Zumkeller_, Jan 23 2012",
				"(Python)",
				"from sympy import factorint",
				"def a(n):",
				"    r = 1",
				"    for p, e in factorint(n).items():",
				"        if p%4 == 1: r *= 2*e + 1",
				"    return 4*r if n \u003e 0 else 0",
				"# _Orson R. L. Peters_, Jan 31 2017",
				"(PARI) a(n)=if(n==0, return(1)); my(f=factor(n)); 4*prod(i=1,#f~, if(f[i,1]%4==1, 2*f[i,2]+1, 1)) \\\\ _Charles R Greathouse IV_, Feb 01 2017",
				"(PARI) a(n)=if(n==0, return(1)); t=0; for(x=1, n-1, y=n^2-x^2; if(issquare(y), t++)); return(4*t+4) \\\\ _Arkadiusz Wesolowski_, Nov 14 2017"
			],
			"xref": [
				"Cf. A004018, A046080, A046110, A046111, A046112.",
				"Also A000328=A051132+A046109."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,2",
			"author": "_Eric W. Weisstein_",
			"references": 27,
			"revision": 51,
			"time": "2018-04-20T17:51:34-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}