{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A292108",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 292108,
			"data": "0,0,0,1,0,1,0,1,1,1,0,2,0,3,2,1,0,1,0,2,2,1,0,4,1,2,1,4,0,2,0,1,4,3,2,1,0,3,2,1,0,9,0,2,3,1,0,7,1,1,2,1,0,8,3,2,2,1,0,3,0,8,7,1,3,2,0,1,7,6,0,1,0,3,2,4",
			"name": "Iterate the map k-\u003e(sigma(k)+phi(k))/2 starting at n; a(n) = number of steps to reach either a fixed point or a fraction, or a(n) = -1 if neither of these two events occurs.",
			"comment": [
				"The first unknown value is a(270).",
				"For an alternative version of this sequence, see A291914.",
				"From _Andrew R. Booker_, Sep 19 2017 and Oct 03 2017: (Start)",
				"Let f(n)=(sigma(n)+phi(n))/2. Then f(n)\u003e=n, so the trajectory of n under f either terminates with a half integer, reaches a fixed point, or increases monotonically. The fixed points of f are 1 and the prime numbers, and f(n) is fractional iff n\u003e2 is a square or twice a square.",
				"It seems likely that a(n) = -1 for all but o(x) numbers n\u003c=x. See link for details of the argument. (End)"
			],
			"link": [
				"Hugo Pfoertner, \u003ca href=\"/A292108/b292108.txt\"\u003eTable of n, a(n) for n = 1..269\u003c/a\u003e",
				"Andrew R. Booker, \u003ca href=\"/A292108/a292108.pdf\"\u003eNotes on (sigma + phi)/2\u003c/a\u003e",
				"Sean A. Irvine, \u003ca href=\"/A291790/a291790.png\"\u003eShowing how the initial portions of some of these trajectories merge\u003c/a\u003e",
				"N. J. A. Sloane, Three (No, 8) Lovely Problems from the OEIS, Experimental Mathematics Seminar, Rutgers University, Oct 05 2017, \u003ca href=\"https://vimeo.com/237029685\"\u003ePart I\u003c/a\u003e, \u003ca href=\"https://vimeo.com/237030304\"\u003ePart 2\u003c/a\u003e, \u003ca href=\"https://oeis.org/A290447/a290447_slides.pdf\"\u003eSlides.\u003c/a\u003e (Mentions this sequence)"
			],
			"formula": [
				"a(n) = 0 if n is 1 or a prime (these are fixed points).",
				"a(n) = 1 if n\u003e2 is a square or twice a square, since these reach a fraction in one step."
			],
			"example": [
				"Let f(k) = (sigma(k)+phi(k))/2. Under the action of f:",
				"14 -\u003e 15 -\u003e 16 -\u003e 39/2, taking 3 steps, so a(14) = 3.",
				"21 -\u003e 22 -\u003e 23, a prime, in 2 steps, so a(21) = 2."
			],
			"mathematica": [
				"With[{i = 200}, Table[-1 + Length@ NestWhileList[If[! IntegerQ@ #, -1/2, (DivisorSigma[1, #] + EulerPhi@ #)/2] \u0026, n, Nor[! IntegerQ@ #, SameQ@ ##] \u0026, 2, i, -1] /. k_ /; k \u003e= i - 1 -\u003e -1, {n, 76}]] (* _Michael De Vlieger_, Sep 19 2017 *)"
			],
			"xref": [
				"Cf. A000010, A000203, A289997, A290001, A291790, A291787, A291804, A291805, A291914."
			],
			"keyword": "nonn",
			"offset": "1,12",
			"author": "_Hugo Pfoertner_ and _N. J. A. Sloane_, Sep 18 2017",
			"references": 6,
			"revision": 38,
			"time": "2017-10-13T12:59:34-04:00",
			"created": "2017-09-18T15:25:00-04:00"
		}
	]
}