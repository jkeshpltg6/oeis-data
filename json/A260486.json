{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A260486",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 260486,
			"data": "1,-4,4,2,-4,0,2,0,-4,0,0,8,-2,0,0,0,-4,-8,0,8,0,0,8,0,-2,-4,0,-2,0,0,0,0,-4,-4,8,0,0,0,8,0,0,-8,0,8,-8,0,0,0,-2,-4,4,4,0,0,-2,0,0,-4,0,8,0,0,0,0,-4,0,4,8,-8,0,0,0,0,-8,0,2,-8,0,0,0",
			"name": "Expansion of phi(-x)^2 * phi(-x^6) / phi(-x^3) in powers of x where phi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A260486/b260486.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of eta(q)^4 * eta(q^6)^3 / (eta(q^2)^2 *eta(q^3)^2 *eta(q^12)) in powers of q.",
				"Euler transform of period 12 sequence [ -4, -2, -2, -2, -4, -3, -4, -2, -2, -2, -4, -2, ...].",
				"Convolution of A010815 and A257657."
			],
			"example": [
				"G.f. = 1 - 4*x + 4*x^2 + 2*x^3 - 4*x^4 + 2*x^6 - 4*x^8 + 8*x^11 - 2*x^12 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 4, 0, x]^2 EllipticTheta[ 4, 0, x^6] / EllipticTheta[ 4, 0, x^3], {x, 0, n}];",
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x, x^2]^2 QPochhammer[ x]^2 QPochhammer[ -x^3] / QPochhammer[ x^3], {x, 0, n}];",
				"a[ n_] := If[ n \u003c 1, Boole[n == 0], -4 I^(n-1) Sum[ {1, I, -1/2, I, 1, -I/2}[[Mod[d, 6, 1]]] KroneckerSymbol[ -2, n/d], {d, Divisors[ n]}]];",
				"a[n_]:= SeriesCoefficient[EllipticTheta[3, 0, -x]^2* EllipticTheta[3, 0, -x^6]/EllipticTheta[3, 0, -x^3], {x, 0, n}]; Table[a[n], {n,0,50}] (* _G. C. Greubel_, Mar 17 2018 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n==0, -4 * I^(n-1) * sumdiv(n, d, [-I/2, 1, I, -1/2, I, 1][d%6+1] * kronecker(-2, n/d)))};",
				"(PARI) {a(n) = my(A, p, e); if( n\u003c1, n==0, A = factor(n);  -4 * I^(n-1) * prod(k=1, matsize(A)[1], [p, e] = A[k, ]; if( p==2, I, p==3, 1-e/2, p%8 \u003e 4, !(e%2), e+1)))};",
				"(PARI) {a(n) = if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A)^4 * eta(x^6 + A)^3 / (eta(x^2 + A)^2 * eta(x^3 + A)^2 * eta(x^12 + A)), n))};"
			],
			"xref": [
				"Cf. A010815, A257657."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Jul 26 2015",
			"references": 1,
			"revision": 9,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2015-07-26T23:27:30-04:00"
		}
	]
}