{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A327512",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 327512,
			"data": "1,2,1,4,1,6,1,8,1,10,1,12,1,14,15,16,1,6,1,20,1,22,1,24,1,26,1,28,1,30,1,32,33,34,35,12,1,38,1,40,1,14,1,44,15,46,1,48,1,10,51,52,1,6,55,56,1,58,1,60,1,62,1,64,1,66,1,68,69,70,1,24,1,74",
			"name": "Maximum divisor of n that is 1, 2, or a nonprime number whose prime indices are pairwise coprime.",
			"comment": [
				"A prime index of n is a number m such that prime(m) divides n. The multiset of prime indices of n is row n of A112798. Numbers that are 1, 2, or a nonprime number whose prime indices are pairwise coprime are listed in A302696, which is the union of this sequence.",
				"a(n) is the greatest term of A302696 which divides n. - _Antti Karttunen_, Dec 06 2021"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A327512/b327512.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e",
				"Gus Wiseman, \u003ca href=\"https://docs.google.com/document/d/e/2PACX-1vSX9dPMGJhxB8rOknCGvOs6PiyhupdWNpqLsnphdgU6MEVqFBnWugAXidDhwHeKqZe_YnUqYeGOXsOk/pub\"\u003eSequences counting and encoding certain classes of multisets\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#prime_indices\"\u003eIndex entries for sequences computed from indices in prime factorization\u003c/a\u003e"
			],
			"example": [
				"The divisors of 72 that are 1, 2, or nonprime numbers whose prime indices are pairwise coprime are: {1, 2, 4, 6, 8, 12, 24}, so a(72) = 24."
			],
			"mathematica": [
				"primeMS[n_]:=If[n==1,{},Flatten[Cases[FactorInteger[n],{p_,k_}:\u003eTable[PrimePi[p],{k}]]]];",
				"Table[Max[Select[Divisors[n],#==1||CoprimeQ@@primeMS[#]\u0026]],{n,100}]"
			],
			"program": [
				"(PARI)",
				"isA302696(n) = if(isprimepower(n),!(n%2), if(!issquarefree(n\u003e\u003evaluation(n,2)), 0, my(pis=apply(primepi,factor(n)[,1])); (lcm(pis)==factorback(pis))));",
				"A327512(n) = vecmax(select(isA302696,divisors(n))); \\\\ _Antti Karttunen_, Dec 06 2021"
			],
			"xref": [
				"See link for additional cross-references.",
				"Cf. A000005, A006530, A056239, A112798, A289509, A302569, A302696, A304711."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Gus Wiseman_, Sep 19 2019",
			"references": 3,
			"revision": 11,
			"time": "2021-12-07T11:07:16-05:00",
			"created": "2019-09-20T08:57:31-04:00"
		}
	]
}