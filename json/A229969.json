{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A229969",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 229969,
			"data": "0,0,0,0,0,1,1,1,1,2,1,1,1,2,1,4,4,3,3,3,3,2,3,3,3,3,4,2,7,4,3,5,3,2,6,3,4,3,4,5,3,4,6,6,3,5,4,5,6,9,4,8,4,7,10,2,6,12,9,1,7,7,6,12,10,3,7,8,8,9,9,5,3,7,3,7,3,9,10,8,6,11,11,13,15,6,6,10,15,11,11,13,8,12,12,7,10,8,13,12",
			"name": "Number of ways to write n = x + y + z with 0 \u003c x \u003c= y \u003c= z such that all the six numbers 2*x-1, 2*y-1, 2*z-1, 2*x*y-1, 2*x*z-1, 2*y*z-1 are prime.",
			"comment": [
				"Conjecture: a(n) \u003e 0 for all n \u003e 5. Moreover, any integer n \u003e 6 can be written as x + y + z with x among 3, 4, 6, 10, 15 such that 2*y-1, 2*z-1, 2*x*y-1, 2*x*z-1, 2*y*z-1 are prime.",
				"We have verified this conjecture for n up to 10^6. As (2*x-1)+(2*y-1)+(2*z-1) = 2*(x+y+z)-3, it implies Goldbach's weak conjecture which has been proved.",
				"Zhi-Wei Sun also had some similar conjectures including the following (i)-(iii):",
				"(i) Any integer n \u003e 6 can be written as x + y + z (x, y, z \u003e 0) with 2*x-1, 2*y-1, 2*z-1 and 2*x*y*z-1 all prime and x among 2, 3, 4. Also, each integer n \u003e 2 can be written as x + y + z (x, y, z \u003e 0) with 2*x+1, 2*y+1, 2*z+1 and 2*x*y*z+1 all prime and x among 1, 2, 3.",
				"(ii) Each integer n \u003e 4 can be written as x + y + z with x = 3 or 6 such that 2*y+1, 2*x*y*z-1 and 2*x*y*z+1 are prime.",
				"(iii) Every integer n \u003e 5 can be written as x + y + z (x, y, z \u003e 0) with x*y-1, x*z-1, y*z-1 all prime and x among 2, 6, 10. Also, any integer n \u003e 2 not equal to 16 can be written as x + y + z (x, y, z \u003e 0) with x*y+1, x*z+1, y*z+1 all prime and x among 1, 2, 6.",
				"See also A229974 for a similar conjecture involving three pairs of twin primes."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A229969/b229969.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1211.1588\"\u003eConjectures involving primes and quadratic forms\u003c/a\u003e, preprint, arXiv:1211.1588."
			],
			"example": [
				"a(10) = 2 since 10 = 2+2+6 = 3+3+4 with 2*2-1, 2*6-1, 2*2*2-1, 2*2*6 -1, 2*3-1, 2*4-1, 2*3*3-1, 2*3*4-1 all prime."
			],
			"mathematica": [
				"a[n_]:=Sum[If[PrimeQ[2i-1]\u0026\u0026PrimeQ[2j-1]\u0026\u0026PrimeQ[2(n-i-j)-1]\u0026\u0026PrimeQ[2i*j-1]\u0026\u0026PrimeQ[2i(n-i-j)-1]\u0026\u0026PrimeQ[2j(n-i-j)-1],1,0],{i,1,n/3},{j,i,(n-i)/2}]",
				"Table[a[n],{n,1,100}]"
			],
			"xref": [
				"Cf. A000040, A068307, A219842, A219864, A227923, A229974."
			],
			"keyword": "nonn",
			"offset": "1,10",
			"author": "_Zhi-Wei Sun_, Oct 04 2013",
			"references": 5,
			"revision": 34,
			"time": "2013-10-10T03:50:42-04:00",
			"created": "2013-10-05T10:28:28-04:00"
		}
	]
}