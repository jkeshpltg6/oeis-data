{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A181322",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 181322,
			"data": "1,1,1,1,2,1,1,2,3,1,1,2,4,4,1,1,2,4,6,5,1,1,2,4,6,9,6,1,1,2,4,6,10,12,7,1,1,2,4,6,10,14,16,8,1,1,2,4,6,10,14,20,20,9,1,1,2,4,6,10,14,20,26,25,10,1,1,2,4,6,10,14,20,26,35,30,11,1,1,2,4,6,10,14,20,26,36,44,36,12,1,1,2,4,6,10,14,20,26,36,46,56,42,13,1",
			"name": "Square array A(n,k), n\u003e=0, k\u003e=0, read by antidiagonals: A(n,k) is the number of partitions of 2*n into powers of 2 less than or equal to 2^k.",
			"comment": [
				"Column sequences converge towards A000123."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A181322/b181322.txt\"\u003eAntidiagonals n = 0..140, flattened\u003c/a\u003e",
				"G. Blom and C.-E. Froeberg, \u003ca href=\"/A002575/a002575.pdf\"\u003eOm myntvaexling (On money-changing) [Swedish]\u003c/a\u003e, Nordisk Matematisk Tidskrift, 10 (1962), 55-69, 103. [Annotated scanned copy] See Table 4."
			],
			"formula": [
				"G.f. of column k: 1/(1-x) * 1/Product_{j=0..k-1} (1 - x^(2^j)).",
				"A(n,k) = Sum_{i=0..k} A089177(n,i).",
				"For n \u003c 2^k, T(n,k) = A000123(k). T(n,0) = 1, T(n,1) = n+1. - _M. F. Hasler_, Feb 19 2019"
			],
			"example": [
				"A(3,2) = 6, because there are 6 partitions of 2*3=6 into powers of 2 less than or equal to 2^2=4: [4,2], [4,1,1], [2,2,2], [2,2,1,1], [2,1,1,1,1], [1,1,1,1,1,1].",
				"Square array A(n,k) begins:",
				"  1,  1,  1,  1,  1,  1,  ...",
				"  1,  2,  2,  2,  2,  2,  ...",
				"  1,  3,  4,  4,  4,  4,  ...",
				"  1,  4,  6,  6,  6,  6,  ...",
				"  1,  5,  9, 10, 10, 10,  ...",
				"  1,  6, 12, 14, 14, 14,  ..."
			],
			"maple": [
				"b:= proc(n, j) local nn, r;",
				"      if n\u003c0 then 0",
				"    elif j=0 then 1",
				"    elif j=1 then n+1",
				"    elif n\u003cj then b(n, j):= b(n-1, j) +b(2*n, j-1)",
				"             else nn:= 1 +floor(n);",
				"                  r:= n-nn;",
				"                  (nn-j) *binomial(nn, j) *add(binomial(j, h)",
				"                  /(nn-j+h) *b(j-h+r, j) *(-1)^h, h=0..j-1)",
				"      fi",
				"    end:",
				"A:= (n, k)-\u003e b(n/2^(k-1), k):",
				"seq(seq(A(n, d-n), n=0..d), d=0..13);"
			],
			"mathematica": [
				"b[n_, j_] := b[n, j] = Module[{nn, r}, Which[n\u003c0, 0, j == 0, 1, j == 1, n+1, n\u003cj, b[n, j] = b[n-1, j] + b[2*n, j-1], True, nn = Floor[n]+1; r = n - nn; (nn-j)*Binomial[nn, j]*Sum[(Binomial[j, h]*b[j-h+r, j]*(-1)^h)/(nn-j+h), {h, 0, j-1}]]]; A[n_, k_] := b[n/2^(k-1), k]; Table[Table[A[n, d-n], {n, 0, d}], {d, 0, 13}] // Flatten (* _Jean-François Alcover_, Jan 15 2014, translated from Maple *)"
			],
			"program": [
				"(PARI) A181322(n,k,r=1)={if(n\u003cr,r,!k,1, r\u0026\u0026n/=2^(k-1); k==1, n+1, n\u003ck, A181322(n-1,k,0)+A181322(2*n,k-1,0),n-=r=1+n\\1,(r-k)*binomial(r,k)*sum(i=0,min(k-1,k+n), binomial(k,i)/(r-k+i)*A181322(k-i+n,k,0) *(-1)^i))} \\\\ From Maple. - _M. F. Hasler_, Feb 19 2019"
			],
			"xref": [
				"Columns k=0-5 give: A000012, A000027(n+1), A002620(n+2), A008804, A088932, A088954.",
				"Main diagonal gives A000123.",
				"Cf. A145515.",
				"See A262553 for another version of this array.",
				"See A072170 for a related array (having the same limiting column)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Alois P. Heinz_, Jan 26 2011",
			"references": 10,
			"revision": 42,
			"time": "2019-02-19T13:02:28-05:00",
			"created": "2010-11-12T14:30:23-05:00"
		}
	]
}