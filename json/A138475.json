{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A138475",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 138475,
			"data": "0,1,3,5,5,7,7,105,11,11,11,385,13,429,715,715,165,323323,15015,323323,1062347,1062347,373065,1062347,11305,1062347,1062347,1062347,37182145,2800733,37182145,5107219,40755,40755,275147873,10015005,215656441",
			"name": "Least k such that the x^n coefficient of cyclotomic polynomial Phi(k,x) has the largest possible magnitude.",
			"comment": [
				"The maximum possible magnitude of the x^n coefficient is A138474(n). Note that a(0)=0 because we assume Phi(0,x)=1; another convention has Phi(0,x)=x, which would force a(0) and a(1) to be reversed.",
				"It appears that (1) for n\u003e80, a(n) has an even number of prime factors and (2) for prime n\u003e80, n divides a(n). Terms up to n=128 were found by exhaustive search; subsequent terms were found by a much faster hill-climbing method."
			],
			"reference": [
				"A. Grytczuk and B. Tropak, A numerical method for the determination of the cyclotomic polynomial coefficients, Computational number theory (Debrecen, 1989), 15-19, de Gruyter, Berlin, 1991."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A138475/b138475.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Carlo Sanna, \u003ca href=\"https://arxiv.org/abs/2111.04034\"\u003eA Survey on Coefficients of Cyclotomic Polynomials\u003c/a\u003e, arXiv:2111.04034 [math.NT], 2021."
			],
			"example": [
				"a(7)=105 because the cyclotomic polynomial Phi(105,x) has the term -2x^7."
			],
			"mathematica": [
				"coef[k_,n_] := Module[{t, b=Table[0,{k+1}]}, t=-MoebiusMu[n]*Table[g=GCD[n,k-m]; MoebiusMu[g]*EulerPhi[g], {m,0,k-1}]; b[[1]]=1; Do[b[[j+1]] = Take[b,j].Take[t,-j]/j, {j,k}]; b]; Table[mx=1; r=PrimePi[k]+1; mnN=Prime[r]; ps=Reverse[Prime[Range[r]]]; Do[d=IntegerDigits[i,2,r]; n=Times@@Pick[ps,d,1]; c=Abs[coef[k,n][[ -1]]]; If[c==mx, mnN=Min[mnN,n], If[c\u003emx, mx=c; mnN=n]], {i,2^r-1}]; mnN, {k,2,20}]"
			],
			"xref": [
				"Cf. A013594 (smallest order of cyclotomic polynomial containing n or -n as a coefficient).",
				"Cf. A138474."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_T. D. Noe_, Mar 19 2008, Apr 14 2008, Feb 16 2009",
			"references": 3,
			"revision": 9,
			"time": "2021-11-09T02:49:24-05:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}