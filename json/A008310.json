{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008310",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8310,
			"data": "1,1,-1,2,-3,4,1,-8,8,5,-20,16,-1,18,-48,32,-7,56,-112,64,1,-32,160,-256,128,9,-120,432,-576,256,-1,50,-400,1120,-1280,512,-11,220,-1232,2816,-2816,1024,1,-72,840,-3584,6912,-6144,2048,13,-364,2912,-9984,16640,-13312,4096",
			"name": "Triangle of coefficients of Chebyshev polynomials T_n(x).",
			"comment": [
				"The row length sequence of this irregular array is A008619(n), n \u003e= 0. Even or odd powers appear in increasing order starting with 1 or x for even or odd row numbers n, respectively. This is the standard triangle A053120 with 0 deleted. - _Wolfdieter Lang_, Aug 02 2014"
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), p. 795.",
				"E. A. Guilleman, Synthesis of Passive Networks, Wiley, 1957, p. 593."
			],
			"link": [
				"R. J. Mathar, \u003ca href=\"/A008310/b008310.txt\"\u003eTable of n, a(n) for n = 0..2600\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"D. Foata and G.-N. Han, \u003ca href=\"http://www-irma.u-strasbg.fr/~foata/paper/pub71.html\"\u003eNombres de Fibonacci et polynomes orthogonaux\u003c/a\u003e",
				"C. Lanczos, \u003ca href=\"/A002457/a002457.pdf\"\u003eApplied Analysis\u003c/a\u003e (Annotated scans of selected pages)",
				"I. Rivin, \u003ca href=\"http://arXiv.org/abs/math.CO/9911076\"\u003eGrowth in free groups (and other stories)\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ChebyshevPolynomialoftheFirstKind.html\"\u003eChebyshev Polynomial of the First Kind\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Chebyshev_polynomials\"\u003eChebyshev polynomials\u003c/a\u003e",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e"
			],
			"formula": [
				"a(n,m) = 2^(m-1) * n * (-1)^((n-m)/2) * ((n+m)/2-1)! / (((n-m)/2)! * m!) if n\u003e0. - _R. J. Mathar_, Apr 20 2007",
				"From _Paul Weisenhorn_, Oct 02 2019: (Start)",
				"T_n(x) = 2*x*T_(n-1)(x)-T_(n-2)(x), T_0(x) = 1, T_1(x) = x.",
				"T_n(x) = ((x+sqrt(x^2-1))^n + (x-sqrt(x^2-1))^n)/2.",
				"(End)"
			],
			"example": [
				"Rows are: (1), (1), (-1,2), (-3,4), (1,-8,8), (5,-20,16) etc., since if c = cos(x): cos(0x) = 1, cos(1x) = 1c; cos(2x) = -1+2c^2; cos(3x) = -3c+4c^3, cos(4x) = 1-8c^2+8c^4, cos(5x) = 5c-20c^3+16c^5, etc.",
				"From _Wolfdieter Lang_, Aug 02 2014: (Start)",
				"This irregular triangle a(n,k) begins:",
				"n\\k   0    1     2      3      4      5      6      7 ...",
				"0:    1",
				"1:    1",
				"2:   -1    2",
				"3:   -3    4",
				"4:    1   -8     8",
				"5:    5  -20    16",
				"6:   -1   18   -48     32",
				"7:   -7   56  -112     64",
				"8:    1  -32   160   -256    128",
				"9:    9 -120   432   -576    256",
				"10:  -1   50  -400   1120  -1280    512",
				"11: -11  220 -1232   2816  -2816   1024",
				"12:   1  -72   840  -3584   6912  -6144   2048",
				"13:  13 -364  2912  -9984  16640 -13312   4096",
				"14:  -1   98 -1568   9408 -26880  39424 -28672   8192",
				"15: -15  560 -6048  28800 -70400  92160 -61440  16384",
				"...",
				"T(4,x) = 1 - 8*x^2 + 8*x^4, T(5,x) = 5*x - 20*x^3 +16*x^5.",
				"(End)"
			],
			"maple": [
				"A008310 := proc(n,m) local x ; coeftayl(simplify(ChebyshevT(n,x),'ChebyshevT'),x=0,m) ; end: i := 0 : for n from 0 to 100 do for m from n mod 2 to n by 2 do printf(\"%d %d \",i,A008310(n,m)) ; i := i+1 ; od ; od ; # _R. J. Mathar_, Apr 20 2007",
				"# second Maple program:",
				"b:= proc(n) b(n):= `if`(n\u003c2, 1, expand(2*b(n-1)-x*b(n-2))) end:",
				"T:= n-\u003e (p-\u003e (d-\u003e seq(coeff(p, x, d-i), i=0..d))(degree(p)))(b(n)):",
				"seq(T(n), n=0..15);  # _Alois P. Heinz_, Sep 04 2019"
			],
			"mathematica": [
				"Flatten[{1, Table[CoefficientList[ChebyshevT[n, x], x], {n, 1, 13}]}]//DeleteCases[#, 0, Infinity]\u0026 (* or *) Flatten[{1, Table[Table[((-1)^k*2^(n-2 k-1)*n*Binomial[n-k, k])/(n-k), {k, Floor[n/2], 0, -1}], {n, 1, 13}]}] (* _Eugeniy Sokol_, Sep 04 2019 *)"
			],
			"xref": [
				"A039991 is a row reversed version, but has zeros which enable the triangle to be seen. Columns/diagonals are A011782, A001792, A001793, A001794, A006974, A006975, A006976 etc.",
				"Reflection of A028297. Cf. A008312, A053112.",
				"Row sums are one. Polynomial evaluations include A001075 (x=2), A001541 (x=3), A001091, A001079, A023038, A011943, A001081, A023039, A001085, A077422, A077424, A097308, A097310, A068203.",
				"Cf. A053120."
			],
			"keyword": "sign,tabf,nice,easy",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Additional comments and more terms from _Henry Bottomley_, Dec 13 2000",
				"Edited: Corrected Cf. A039991 statement. Cf. A053120 added. - _Wolfdieter Lang_, Aug 06 2014"
			],
			"references": 16,
			"revision": 109,
			"time": "2019-10-03T10:36:48-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}