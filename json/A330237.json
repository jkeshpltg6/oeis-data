{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A330237",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 330237,
			"data": "0,1,1,2,0,2,3,1,1,3,4,2,0,2,4,5,3,1,1,3,5,6,4,2,0,2,4,6,7,5,3,1,1,3,5,7,8,6,4,2,0,2,4,6,8,11,7,5,3,1,1,3,5,7,11,10,12,6,4,2,0,2,4,6,12,10,11,11,13,5,3,1,1,3,5,13,11,11,12,10,12,14,4,2,0,2,4,14,12,10,12,13,11,11,13,15,3,1,1,3,15,13,11,11,13,14,12,10,12,14,16,2,0,2,16",
			"name": "Square array T(n,k): concatenate the absolute differences of the digits of n and k (the smaller one padded with leading zeros); read by antidiagonals; n, k \u003e= 1.",
			"comment": [
				"A digit-wise analog of A049581.",
				"The binary operator T: N x N -\u003e N is commutative, therefore this table is symmetric and it does not matter in which direction the antidiagonals are read. It would also be sufficient to specify only the lower half of the square table: see A330238 for this variant. The operator is also defined for either argument equal to 0, which is the neutral element: T(x,0) = 0 for all x. Therefore we omit row \u0026 column 0 here, see A330240 for the table including these. Every element is its opposite or inverse, as shown by the zero diagonal T(x,x) = 0."
			],
			"link": [
				"Eric Angelini, \u003ca href=\"https://cinquantesignes.blogspot.com/2019/12/the-box-operation.html\"\u003eThe box ■ operation\u003c/a\u003e, personal blog \"Cinquante signes\", and post to the SeqFan list, Dec 06 2019."
			],
			"example": [
				"The square array starts as follows:",
				"   n | k=1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 ...",
				"  ---+-----------------------------------------------------------",
				"   1 |   0  1  2  3  4  5  6  7  8 11 10 11 12 13 14 15 16 17 ...",
				"   2 |   1  0  1  2  3  4  5  6  7 12 11 10 11 12 13 14 15 16 ...",
				"   3 |   2  1  0  1  2  3  4  5  6 13 12 11 10 11 12 13 14 15 ...",
				"   4 |   3  2  1  0  1  2  3  4  5 14 13 12 11 10 11 12 13 14 ...",
				"   5 |   4  3  2  1  0  1  2  3  4 15 14 13 12 11 10 11 12 13 ...",
				"   6 |   5  4  3  2  1  0  1  2  3 16 15 14 13 12 11 10 11 12 ...",
				"   7 |   6  5  4  3  2  1  0  1  2 17 16 15 14 13 12 11 10 11 ...",
				"   8 |   7  6  5  4  3  2  1  0  1 18 17 16 15 14 13 12 11 10 ...",
				"   9 |   8  7  6  5  4  3  2  1  0 19 18 17 16 15 14 13 12 11 ...",
				"  10 |  11 12 13 14 15 16 17 18 19  0  1  2  3  4  5  6  7  8 ...",
				"  11 |  10 11 12 13 14 15 16 17 18  1  0  1  2  3  4  5  6  7 ...",
				"  12 |  11 10 11 12 13 14 15 16 17  2  1  0  1  2  3  4  5  6 ...",
				"   (...)",
				"It differs from A049581 only if at least one index is \u003e 10."
			],
			"program": [
				"(PARI) T(a,b)=fromdigits(abs(Vec(digits(min(a,b)),-logint(a=max(a,b),10)-1)-digits(a)))"
			],
			"xref": [
				"Cf A330240 (variant including row \u0026 column 0), A330237 (lower left triangle), A049581 (T(n,k) = |n-k|)."
			],
			"keyword": "nonn,base,tabl",
			"offset": "1,4",
			"author": "_M. F. Hasler_, Dec 06 2019",
			"references": 3,
			"revision": 14,
			"time": "2020-07-20T02:05:27-04:00",
			"created": "2019-12-07T01:39:47-05:00"
		}
	]
}