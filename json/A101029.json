{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A101029",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 101029,
			"data": "1,10,70,420,4620,60060,60060,408408,7759752,38798760,892371480,4461857400,13385572200,55454513400,1719089915400,3438179830800,24067258815600,890488576177200,890488576177200,36510031623265200",
			"name": "Denominator of partial sums of a certain series.",
			"comment": [
				"The numerators are given in A101028.",
				"One third of the denominator of the finite differences of the series of sums of all matrix elements of n X n Hilbert matrix M(i,j)=1/(i+j-1) (i,j = 1..n). - _Alexander Adamchuk_, Apr 11 2006"
			],
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HilbertMatrix.html\"\u003eHilbert Matrix\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HarmonicNumber.html\"\u003eHarmonic Number\u003c/a\u003e."
			],
			"formula": [
				"a(n)=denominator(s(n)) with s(n)=3*sum(1/((2*k-1)*k*(2*k+1)), k=1..n). See A101028 for more information.",
				"a(n) = 1/3*Denominator[Sum[Sum[1/(i+j-1),{i,1,n+1}],{j,1,n+1}]-Sum[Sum[1/(i+j-1),{i,1,n}],{j,1,n}]]. a(n) = 1/3*Denominator[H(2n+1) + H(2n) - 2H(n)], where H(n) = Sum[1/k, (k, 1, n}] is a Harmonic number, H[n] = A001008/A002805. - _Alexander Adamchuk_, Apr 11 2006"
			],
			"example": [
				"n=2: HilbertMatrix[n,n]",
				"1 1/2",
				"1/2 1/3",
				"so a(1) = 1/3*Denominator[(1 + 1/2 + 1/2 + 1/3) - 1] = 1/3*Denominator[7/3 -1] = 1/3*Denominator[4/3] = 1.",
				"The n X n Hilbert matrix begins:",
				"1 1/2 1/3 1/4 1/5 1/6 1/7 1/8 ...",
				"1/2 1/3 1/4 1/5 1/6 1/7 1/8 1/9 ...",
				"1/3 1/4 1/5 1/6 1/7 1/8 1/9 1/10 ...",
				"1/4 1/5 1/6 1/7 1/8 1/9 1/10 1/11 ...",
				"1/5 1/6 1/7 1/8 1/9 1/10 1/11 1/12 ...",
				"1/6 1/7 1/8 1/9 1/10 1/11 1/12 1/13 ..."
			],
			"mathematica": [
				"Denominator[Table[Sum[1/(i + j - 1), {i, n}, {j, n}], {n,2, 27}]-Table[Sum[1/(i + j - 1), {i, n}, {j, n}], {n, 26}]]/3 - _Alexander Adamchuk_, Apr 11 2006"
			],
			"xref": [
				"Cf. A098118, A086881, A005249, A001008, A002805."
			],
			"keyword": "nonn,frac,easy",
			"offset": "1,2",
			"author": "_Wolfdieter Lang_, Dec 17 2004",
			"references": 1,
			"revision": 7,
			"time": "2012-12-17T14:16:50-05:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}