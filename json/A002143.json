{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002143",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2143,
			"id": "M2266 N0896",
			"data": "1,1,1,1,3,3,1,5,3,1,7,5,3,5,3,5,5,3,7,1,11,5,13,9,3,7,5,15,7,13,11,3,3,19,3,5,19,9,3,17,9,21,15,5,7,7,25,7,9,3,21,5,3,9,5,7,25,13,5,13,3,23,11,5,5,31,13,5,21,15,5,7,9,7,33,7,21,3,29,3,31,19,5,11,15,27,17,13",
			"name": "Class numbers h(-p) where p runs through the primes p == 3 (mod 4).",
			"comment": [
				"a(n) = h(-A002145(n)).",
				"Same as (1/p)*(sum of quadratic nonresidues mod p in (0,p) - sum of quadratic residues mod p in (0,p)), for prime p == 3 (mod 4) if p \u003e 3. (See Davenport's book and the first Mathematica program.) - _Jonathan Sondow_, Oct 27 2011",
				"Conjecture: For any prime p \u003e 3 with p == 3 (mod 8), we have 2*h(-p)*sqrt(p) = Sum_{k=1..(p-1)/2} csc(2*Pi*k^2/p). - _Zhi-Wei Sun_, Aug 06 2019"
			],
			"reference": [
				"H. Davenport, Multiplicative Number Theory, Graduate Texts in Math. 74, 2nd ed., Springer, 1980, p. 51.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A002143/b002143.txt\"\u003eTable of n, a(n) for n=1..10000\u003c/a\u003e",
				"Christian Aebi, Grant Cairns, \u003ca href=\"http://arxiv.org/abs/1512.00896\"\u003eSums of Quadratic residues and nonresidues\u003c/a\u003e, arXiv:1512.00896 [math.NT], 2015.",
				"Kevin A. Broughan, \u003ca href=\"http://www.math.waikato.ac.nz/~kab/papers/div4.pdf\"\u003eRestricted divisor sums\u003c/a\u003e, Acta Arithmetica, vol. 101, (2002), pp. 105-114.",
				"E. T. Ordman, \u003ca href=\"/A002143/a002143.pdf\"\u003eTables of the class number for negative prime discriminants\u003c/a\u003e, Deposited in Unpublished Mathematical Table file of Math. Comp. [Annotated scanned partial copy with notes]",
				"E. T. Ordman, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-69-99644-6\"\u003eTables of the class number for negative prime discriminants\u003c/a\u003e, Math. Comp., 23 (1969), 458.",
				"A. Pacetti and F. Rodriguez Villegas, \u003ca href=\"https://doi.org/10.1090/S0025-5718-04-01709-0\"\u003eComputing weight two modular forms of level p^2\u003c/a\u003e, Math. Comp. 74 (2004), 1545-1557. See Table 1.",
				"N. Snyder, \u003ca href=\"http://math.berkeley.edu/~nsnyder/tutorial/lecture7.pdf\"\u003eLectures # 7: The Class Number Formula For Positive Definite Binary Quadratic Forms.\u003c/a\u003e [Background information on class numbers, link sent by V. S. Miller, Nov 22 2009]",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Class_number_(number_theory)#Class_numbers_of_quadratic_fields\"\u003eClass numbers of quadratic fields\u003c/a\u003e"
			],
			"formula": [
				"h(-p) = 1 + 2*sum(0 \u003c= n \u003c= (1/2)*sqrt(p/3)-1, d(n^2+n+(p+1)/4, [2*n+1, sqrt(n^2+n+(p+1)/4)])) for prime p=3 mod 4, p\u003e3. d(n, [a, b])=card{d: d|n and a\u003cd\u003cb} for integer n and real a, b. - Antonio G. Astudillo (afg_astudillo(AT)hotmail.com), Jul 19 2002",
				"h(-p) = -(1/p)*sum(n=1..p-1, n*(n|p)) if p \u003e 3, where (n|p) = +/- 1 is the Legendre symbol. - _Jonathan Sondow_, Oct 27 2011",
				"h(-p) = (1/3)*sum(n=1..(p-1)/2, (n|p)) or sum(n=1..(p-1)/2, (n|p)) according as p == 3 or 7 (mod 8). - _Jonathan Sondow_, Feb 27 2012"
			],
			"example": [
				"E.g., a(4) = 1 is the class number of -19, the 4th prime == 3 mod 4.",
				"a(5) = -(1/23)*sum(n=1..22, n*(n|23)) = -(1/23)*(1 + 2 + 3 + 4 - 5 + 6 - 7 + 8 + 9 - 10 - 11 + 12 + 13 - 14 - 15 + 16 - 17 + 18 - 19 - 20 - 21 - 22) = 69/23 = 3. - _Jonathan Sondow_, Oct 27 2011"
			],
			"mathematica": [
				"Cases[ Table[ With[ {p = Prime[n]}, If[ Mod[p, 4] == 3, -(1/p)*Sum[ a*JacobiSymbol[a, p], {a, 1, p - 1}]]], {n, 1, 100}], _Integer] (* _Jonathan Sondow_, Oct 27 2011 *)",
				"p = Prime[n]; If[Mod[p, 4] == 3, NumberFieldClassNumber[Sqrt[-p]]] (* _Jonathan Sondow_, Feb 24 2012 *)"
			],
			"program": [
				"(PARI) forprime(p=3, 1e3, if(p%4==3, print1(qfbclassno(-p)\", \"))) \\\\ _Charles R Greathouse IV_, May 08 2011"
			],
			"xref": [
				"Cf. A002145 (primes p), A002146, A101435."
			],
			"keyword": "nonn",
			"offset": "1,5",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from Antonio G. Astudillo (afg_astudillo(AT)hotmail.com), Jul 19 2002"
			],
			"references": 13,
			"revision": 57,
			"time": "2019-08-06T05:02:26-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}