{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A208333",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 208333,
			"data": "1,0,4,0,2,10,0,2,6,28,0,2,6,24,76,0,2,6,28,80,208,0,2,6,32,100,264,568,0,2,6,36,120,360,840,1552,0,2,6,40,140,464,1232,2624,4240,0,2,6,44,160,576,1680,4128,8064,11584,0,2,6,48,180,696,2184,5952",
			"name": "Triangle of coefficients of polynomials v(n,x) jointly generated with A208332; see the Formula section.",
			"comment": [
				"As triangle T(n,k) with 0 \u003c= k \u003c= n, it is (0, 1/2, 1/2, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (4, -3/2, -1/2, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Feb 28 2012"
			],
			"formula": [
				"u(n,x) = u(n-1,x) + x*v(n-1,x),",
				"v(n,x) = 2x*u(n-1,x) + 2x*v(n-1,x),",
				"where u(1,x)=1, v(1,x)=1.",
				"From _Philippe Deléham_, Feb 28 2012: (Start)",
				"As triangle with 0 \u003c= k \u003c= n:",
				"T(n,k) = T(n-1,k) + 2*T(n-1,k-1) - 2*T(n-2,k-1) + 2*T(n-2,k-2) with T(0,0) = 1, T(1,0) = 0, T(1,1) = 4 and T(n,k) = 0 if k \u003c 0 or if k \u003e n.",
				"G.f.: (1-x+2*y*x)/(1-x-2*y*x+2*y*x^2-2*y^2*x^2).",
				"T(n,n) = A026150(n+1).",
				"Sum_{k=0..n} T(n,k) = A003946(n). (End)"
			],
			"example": [
				"First five rows:",
				"  1;",
				"  0,  4;",
				"  0,  2, 10;",
				"  0,  2,  6, 28;",
				"  0,  2,  6, 24, 76;",
				"First five polynomials u(n,x):",
				"  1",
				"      4x",
				"      2x + 10x^2",
				"      2x +  6x^2 + 28x^3",
				"      2x +  6x^2 + 24x^3 + 76x^4."
			],
			"mathematica": [
				"u[1, x_] := 1; v[1, x_] := 1; z = 13;",
				"u[n_, x_] := u[n - 1, x] + x*v[n - 1, x];",
				"v[n_, x_] := 2 x*u[n - 1, x] + 2 x*v[n - 1, x];",
				"Table[Expand[u[n, x]], {n, 1, z/2}]",
				"Table[Expand[v[n, x]], {n, 1, z/2}]",
				"cu = Table[CoefficientList[u[n, x], x], {n, 1, z}];",
				"TableForm[cu]",
				"Flatten[%]  (* A208332 *)",
				"Table[Expand[v[n, x]], {n, 1, z}]",
				"cv = Table[CoefficientList[v[n, x], x], {n, 1, z}];",
				"TableForm[cv]",
				"Flatten[%]  (* A208333 *)"
			],
			"xref": [
				"Cf. A003946, A026150, A208332."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Clark Kimberling_, Feb 26 2012",
			"references": 3,
			"revision": 19,
			"time": "2020-01-24T03:27:10-05:00",
			"created": "2012-02-27T20:59:08-05:00"
		}
	]
}