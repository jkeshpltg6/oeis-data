{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A103631",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 103631,
			"data": "1,0,1,0,1,1,0,1,1,1,0,1,1,2,1,0,1,1,3,2,1,0,1,1,4,3,3,1,0,1,1,5,4,6,3,1,0,1,1,6,5,10,6,4,1,0,1,1,7,6,15,10,10,4,1,0,1,1,8,7,21,15,20,10,5,1,0,1,1,9,8,28,21,35,20,15,5,1,0,1,1,10,9,36,28,56,35,35,15,6,1",
			"name": "Triangle read by rows: T(n,k) = abs(qStirling2(n,k,q)) for q = -1, with 0 \u003c= k \u003c= n.",
			"comment": [
				"Previous name: An invertible triangle whose row sums are F(n+1).",
				"Triangle inverse has general term (-1)^(n-k)*binomial(floor(n/2),n-k). Diagonal sums are A103632.",
				"Triangle T(n,k), 0 \u003c= k \u003c= n, read by rows, given by [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, ...] DELTA [1, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, ...] where DELTA is the operator defined in A084938. - _Philippe Deléham_, Oct 08 2005",
				"Row sums are Fibonacci numbers (A000045).",
				"Another version of triangle in A065941. - _Philippe Deléham_, Jan 01 2009",
				"From _Johannes W. Meijer_, Aug 11 2011: (Start)",
				"The T(n,k) coefficients appear in appendix 2 of Parks's remarkable article \"A new proof of the Routh-Hurwitz stability criterion using the second method of Liapunov\" if we assume that the b(r) coefficients are all equal to 1; see the second Maple program.",
				"The T(n,k) triangle is related to a linear (n+1)-th order differential equation with coefficients a(n,k), see triangle A194005.",
				"Parks's triangle appears to be an appropriate name for the triangle given above. (End)"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A103631/b103631.txt\"\u003eRows n = 0..150 of triangle, flattened\u003c/a\u003e",
				"Henry W. Gould, \u003ca href=\"http://www.fq.math.ca/Scanned/3-4/gould.pdf\"\u003eA Variant of Pascal's Triangle\u003c/a\u003e, The Fibonacci Quarterly, Vol. 3, Nr. 4, Dec. 1965, pp. 257-271.",
				"P. C. Parks, \u003ca href=\"http://dx.doi.org/10.1017/S030500410004072X\"\u003e A new proof of the Routh-Hurwitz stability criterion using the second method of Liapunov \u003c/a\u003e, Math. Proc. of the Cambridge Philosophical Society, Vol. 58, Issue 04 (1962) pp. 694-702."
			],
			"formula": [
				"T(n,k) = binomial(floor((2*n-k-1)/2), n-k).",
				"A polynomial recursion which produces this triangle: p(x, n) = p(x, n - 1) + x^2*p(x, n - 2). - _Roger L. Bagula_, Apr 27 2008",
				"Sum_{k=0..n} T(n,k)*x^k = A152163(n), A000007(n), A000045(n+1), A026597(n), A122994(n+1), A158608(n), A122995(n+1), A158797(n), A122996(n+1), A158798(n), A158609(n) for x = -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 respectively. - _Philippe Deléham_, Jun 12 2009",
				"G.f.: (1+(y-1)*x)/(1-x-y^2*x^2). - _Philippe Deléham_, Mar 09 2012",
				"T(n,k) = T(n-1,k) + T(n-2,k-2), T(0,0) = 1, T(1,0) = 0, T(1,1) = 1, T(n,k) = 0 if k \u003c 0 or if k \u003e n. - _Philippe Deléham_, Mar 09 2012"
			],
			"example": [
				"From _Paul Barry_, Oct 02 2009: (Start)",
				"Triangle begins:",
				"  1,",
				"  0, 1,",
				"  0, 1, 1,",
				"  0, 1, 1, 1,",
				"  0, 1, 1, 2, 1,",
				"  0, 1, 1, 3, 2,  1,",
				"  0, 1, 1, 4, 3,  3,  1,",
				"  0, 1, 1, 5, 4,  6,  3,  1,",
				"  0, 1, 1, 6, 5, 10,  6,  4, 1,",
				"  0, 1, 1, 7, 6, 15, 10, 10, 4, 1",
				"Production matrix is:",
				"  0, 1,",
				"  0, 1, 1,",
				"  0, 0, 0, 1,",
				"  0, 0, 0, 1, 1,",
				"  0, 0, 0, 0, 0, 1,",
				"  0, 0, 0, 0, 0, 1, 1,",
				"  0, 0, 0, 0, 0, 0, 0, 1,",
				"  0, 0, 0, 0, 0, 0, 0, 1, 1,",
				"  0, 0, 0, 0, 0, 0, 0, 0, 0, 1 (End)"
			],
			"maple": [
				"From _Johannes W. Meijer_, Aug 11 2011: (Start)",
				"A103631 := proc(n,k): binomial(floor((2*n-k-1)/2),n-k) end: seq(seq(A103631(n,k), k=0..n), n=0..12);",
				"nmax:=12: for n from 0 to nmax+1 do b(n):=1 od: A103631 := proc(n,k) option remember: local j: if k=0 and n=0 then b(1) elif k=0 and n\u003e=1 then 0 elif k=1 then b(n+1) elif k=2 then b(1)*b(n+1) elif k\u003e=3 then expand(b(n+1)*add(procname(j,k-2), j=k-2..n-2)) fi: end: for n from 0 to nmax do seq(A103631(n,k), k=0..n) od: seq(seq(A103631(n,k),k=0..n), n=0..nmax); # (End)"
			],
			"mathematica": [
				"p[x, -1] = 0; p[x, 0] = 1; p[x, 1] = x; p[x, 2] = x + x^2; p[x_, n_] := p[x, n] = p[x, n - 1] + x^2*p[x, n - 2]; (* with *) Table[ExpandAll[p[x, n]], {n, 0, 10}]; (* or *) a = Table[CoefficientList[p[x, n], x], {n, 0, 10}]; Flatten[a] (* _Roger L. Bagula_, Apr 27 2008 *)",
				"Table[Binomial[Floor[(2*n - k - 1)/2], n - k], {n, 0, 10}, {k, 0, n}] // Flatten (* _G. C. Greubel_, Aug 27 2016 *)",
				"qStirling2[n_, k_, q_] /; 1 \u003c= k \u003c= n := q^(k - 1) qStirling2[n - 1, k - 1, q] + Sum[q^j, {j, 0, k - 1}] qStirling2[n - 1, k, q];",
				"qStirling2[n_, 0, _] := KroneckerDelta[n, 0];",
				"qStirling2[0, k_, _] := KroneckerDelta[0, k];",
				"qStirling2[_, _, _] = 0;",
				"Table[Abs[qStirling2[n, k, -1]], {n, 0, 12}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Mar 10 2020 *)"
			],
			"program": [
				"(Haskell)",
				"a103631 n k = a103631_tabl !! n !! k",
				"a103631_row n = a103631_tabl !! n",
				"a103631_tabl = [1] : [0,1] : f [1] [0,1] where",
				"   f xs ys = zs : f ys zs where",
				"     zs = zipWith (+)  ([0,0] ++ xs)  (ys ++ [0])",
				"-- _Reinhard Zumkeller_, May 07 2012",
				"(MAGMA) / As triangle: / [[Binomial(Floor((2*n-k-1)/2), n-k): k in [0..n]]: n in [0..15]]; // _Vincenzo Librandi_, Aug 28 2016",
				"(Sage)",
				"from sage.combinat.q_analogues import q_stirling_number2",
				"for n in (0..9):",
				"    print([abs(q_stirling_number2(n,k).substitute(q=-1)) for k in [0..n]])",
				"# _Peter Luschny_, Mar 09 2020"
			],
			"xref": [
				"Cf. A103633 (signed version)."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,14",
			"author": "_Paul Barry_, Feb 11 2005",
			"ext": [
				"New name from _Peter Luschny_, Mar 09 2020"
			],
			"references": 11,
			"revision": 58,
			"time": "2020-03-10T09:00:17-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}