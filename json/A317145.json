{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A317145",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 317145,
			"data": "1,1,1,1,1,1,1,1,1,1,1,2,1,1,1,2,1,2,1,2,1,1,1,5,1,1,1,2,1,3,1,4,1,1,1,7,1,1,1,5,1,3,1,2,2,1,1,15,1,2,1,2,1,5,1,5,1,1,1,11,1,1,2,11,1,3,1,2,1,3,1,26,1,1,2,2,1,3,1,15,2,1,1,11,1,1,1,5,1,11,1,2,1,1,1,52,1,2,2,7,1,3,1,5,3",
			"name": "Number of maximal chains of factorizations of n into factors \u003e 1, ordered by refinement.",
			"comment": [
				"If x and y are factorizations of the same integer and it is possible to produce x by further factoring the factors of y, flattening, and sorting, then x \u003c= y.",
				"a(n) depends only on prime signature of n (cf. A025487). - _Antti Karttunen_, Oct 08 2018"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A317145/b317145.txt\"\u003eTable of n, a(n) for n = 1..11520\u003c/a\u003e",
				"Antti Karttunen, \u003ca href=\"/A317145/a317145.txt\"\u003eData supplement: n, a(n) computed for n = 1..100000\u003c/a\u003e",
				"\u003ca href=\"/index/Eu#epf\"\u003eIndex entries for sequences computed from exponents in factorization of n\u003c/a\u003e"
			],
			"formula": [
				"a(prime^n) = A002846(n).",
				"a(n) = A320105(A064988(n)). - _Antti Karttunen_, Oct 08 2018"
			],
			"example": [
				"The a(36) = 7 maximal chains:",
				"  (2*2*3*3) \u003c (2*2*9) \u003c (2*18) \u003c (36)",
				"  (2*2*3*3) \u003c (2*2*9) \u003c (4*9)  \u003c (36)",
				"  (2*2*3*3) \u003c (2*3*6) \u003c (2*18) \u003c (36)",
				"  (2*2*3*3) \u003c (2*3*6) \u003c (3*12) \u003c (36)",
				"  (2*2*3*3) \u003c (2*3*6) \u003c (6*6)  \u003c (36)",
				"  (2*2*3*3) \u003c (3*3*4) \u003c (3*12) \u003c (36)",
				"  (2*2*3*3) \u003c (3*3*4) \u003c (4*9)  \u003c (36)"
			],
			"program": [
				"(PARI)",
				"A064988(n) = { my(f = factor(n)); for (k=1, #f~, f[k, 1] = prime(f[k, 1]); ); factorback(f); }; \\\\ From A064988",
				"memoA320105 = Map();",
				"A320105(n) = if(bigomega(n)\u003c=2,1,if(mapisdefined(memoA320105,n), mapget(memoA320105,n), my(f=factor(n), u = #f~, s = 0); for(i=1,u,for(j=i+(1==f[i,2]),u, s += A320105(prime(primepi(f[i,1])*primepi(f[j,1]))*(n/(f[i,1]*f[j,1]))))); mapput(memoA320105,n,s); (s)));",
				"A317145(n) = A320105(A064988(n)); \\\\ _Antti Karttunen_, Oct 08 2018"
			],
			"xref": [
				"Cf. A001055, A002846, A007716, A045778, A064988, A162247, A213427, A275024, A281113, A299202, A300385, A317144, A317146, A320105."
			],
			"keyword": "nonn",
			"offset": "1,12",
			"author": "_Gus Wiseman_, Jul 22 2018",
			"ext": [
				"Data section extended to 105 terms by _Antti Karttunen_, Oct 08 2018"
			],
			"references": 24,
			"revision": 17,
			"time": "2018-10-12T04:42:51-04:00",
			"created": "2018-07-23T21:07:20-04:00"
		}
	]
}