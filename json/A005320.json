{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005320",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5320,
			"id": "M2919",
			"data": "0,3,12,45,168,627,2340,8733,32592,121635,453948,1694157,6322680,23596563,88063572,328657725,1226567328,4577611587,17083879020,63757904493,237947738952,888033051315,3314184466308,12368704813917",
			"name": "a(n) = 4*a(n-1) - a(n-2), with a(0) = 0, a(1) = 3.",
			"comment": [
				"For n \u003e 1, a(n-1) is the determinant of the n X n band matrix which has {2,4,4,...,4,4,2} on the diagonal and a 1 on the entire super- and subdiagonal. This matrix appears when constructing a natural cubic spline interpolating n equally spaced data points. - g.degroot(AT)phys.uu.nl, Feb 14 2007",
				"Integer values of x that make 9+3*x^2 a perfect square. - _Lorenz H. Menke, Jr._, Mar 26 2008",
				"The intermediate convergents to 3^(1/2), beginning with 3/2, 12/7, 45/26, 168/97, comprise a strictly increasing sequence whose numerators are the terms of this sequence and denominators are A001075. - _Clark Kimberling_, Aug 27 2008",
				"a(n) also give the altitude to the middle side of a Super-Heronian Triangle. - _Johannes Boot_, Oct 14 2010",
				"a(n) gives values of y satisfying 3*x^2 - 4*y^2 = 12; corresponding x values are given by A003500. - _Sture Sjöstedt_, Dec 19 2017"
			],
			"reference": [
				"Serge Lang, Introduction to Diophantine Approximations, Addison-Wesley, New York, 1966.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A005320/b005320.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"C. Banderier and D. Merlini, \u003ca href=\"http://algo.inria.fr/banderier/Papers/infjumps.ps\"\u003eLattice paths with an infinite set of jumps\u003c/a\u003e, FPSAC02, Melbourne, 2002.",
				"Hacène Belbachir, Soumeya Merwa Tebtoub, László Németh, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL23/Nemeth/nemeth7.html\"\u003eEllipse Chains and Associated Sequences\u003c/a\u003e, J. Int. Seq., Vol. 23 (2020), Article 20.8.5.",
				"I. M. Gessel, Ji Li, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL16/Gessel/gessel6.html\"\u003eCompositions and Fibonacci identities\u003c/a\u003e, J. Int. Seq. 16 (2013) 13.4.5",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"Clark Kimberling, \u003ca href=\"http://dx.doi.org/10.1007/s000170050020\"\u003eBest lower and upper approximates to irrational numbers\u003c/a\u003e, Elemente der Mathematik, 52 (1997) 122-126.",
				"E. Keith Lloyd, \u003ca href=\"http://www.jstor.org/stable/3619201\"\u003eThe Standard Deviation of 1, 2,..., n: Pell's Equation and Rational Triangles\u003c/a\u003e, Math. Gaz. vol 81 (1997), 231-243.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"William H. Richardson, \u003ca href=\"http://www.math.wichita.edu/~richardson/heronian/heronian.html\"\u003e Super-Heronian Triangles\u003c/a\u003e from Johannes Boot, Oct 14 2010",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-1)."
			],
			"formula": [
				"a(n) = (sqrt(3)/2)*(2+sqrt(3))^n-(sqrt(3)/2)*(2-sqrt(3))^n. - _Antonio Alberto Olivares_, Jan 17 2004",
				"G.f.: 3*x/(x^2-4*x+1). - _Harvey P. Dale_, Mar 04 2012",
				"a(n) = 3*A001353(n). - _R. J. Mathar_, Mar 14 2016"
			],
			"maple": [
				"A005320:=3*z/(1-4*z+z**2); # _Simon Plouffe_ in his 1992 dissertation",
				"a:= n-\u003e (Matrix([[3,0]]). Matrix([[4,1],[ -1,0]])^n)[1,2]: seq(a(n), n=0..50); # _Alois P. Heinz_, Aug 14 2008"
			],
			"mathematica": [
				"Det[SparseArray[{{i_, i_} -\u003e If[i == 1 || i == n, 2, 4], {i_, j_} -\u003e If[Abs[i - j] == 1, 1, 0]}, {n, n}]] (* the recurrence relation is faster! g.degroot(AT)phys.uu.nl, Feb 14 2007 *)",
				"Do[If[IntegerQ[Sqrt[(9 + 3 x^2)]], Print[{x, Sqrt[(9 + 3 x^2)]}]], {x, 0, 2000000}] (* _Lorenz H. Menke, Jr._, Mar 26 2008 *)",
				"LinearRecurrence[{4,-1},{0,3},30] (* _Harvey P. Dale_, Mar 04 2012 *)"
			],
			"program": [
				"(PARI) Vec(3/(x^2-4*x+1)+O(x^99)) \\\\ _Charles R Greathouse IV_, Mar 05 2012"
			],
			"xref": [
				"Cf. A001075, A002194, A082841, A003500."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Typo in definition corrected by _Johannes Boot_, Feb 05 2009"
			],
			"references": 14,
			"revision": 90,
			"time": "2021-03-12T22:32:36-05:00",
			"created": "1991-05-20T03:00:00-04:00"
		}
	]
}