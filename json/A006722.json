{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006722",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6722,
			"id": "M2457",
			"data": "1,1,1,1,1,1,3,5,9,23,75,421,1103,5047,41783,281527,2534423,14161887,232663909,3988834875,45788778247,805144998681,14980361322965,620933643034787,16379818848380849,369622905371172929,20278641689337631649,995586066665500470689",
			"name": "Somos-6 sequence: a(n) = (a(n-1) * a(n-5) + a(n-2) * a(n-4) + a(n-3)^2) / a(n-6), a(0) = ... = a(5) = 1.",
			"reference": [
				"C. Pickover, Mazes for the Mind, St. Martin's Press, NY, 1992, p. 350.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A006722/b006722.txt\"\u003eTable of n, a(n) for n=0..100\u003c/a\u003e",
				"R. H. Buchholz and R. L. Rathbun, \u003ca href=\"http://www.jstor.org/stable/2974977\"\u003eAn infinite set of Heron triangles with two rational medians\u003c/a\u003e, Amer. Math. Monthly, 104 (1997), 107-115.",
				"Chang, Xiangke; Hu, Xingbiao, \u003ca href=\"https://doi.org/10.1016/j.laa.2012.01.016\"\u003eA conjecture based on Somos-4 sequence and its extension\u003c/a\u003e, Linear Algebra Appl. 436, No. 11, 4285-4295 (2012).",
				"Yuri N. Fedorov and Andrew N. W. Hone, \u003ca href=\"http://arxiv.org/abs/1512.00056\"\u003eSigma-function solution to the general Somos-6 recurrence via hyperelliptic Prym varieties\u003c/a\u003e, arXiv:1512.00056 [nlin.SI], 2015.",
				"S. Fomin and A. Zelevinsky, \u003ca href=\"http://arXiv.org/abs/math.CO/0104241\"\u003eThe Laurent phenomemon\u003c/a\u003e, arXiv:math/0104241 [math.CO], 2001.",
				"David Gale, \u003ca href=\"http://dx.doi.org/10.1007/BF03024070\"\u003eThe strange and surprising saga of the Somos sequences\u003c/a\u003e, Math. Intelligencer 13(1) (1991), pp. 40-42.",
				"R. W. Gosper and Richard C. Schroeppel, \u003ca href=\"http://arxiv.org/abs/math/0703470\"\u003eSomos Sequence Near-Addition Formulas and Modular Theta Functions\u003c/a\u003e, arXiv:math/0703470 [math.NT], 2007.",
				"A. N. W. Hone, \u003ca href=\"http://dx.doi.org/10.1080/00036810903329977\"\u003eAnalytic solutions and integrability for bilinear recurrences of order six\u003c/a\u003e, Appl. Anal. 89, no.4 (2010), 473-492.",
				"J. L. Malouf, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(92)90714-Q\"\u003eAn integer sequence from a rational recursion\u003c/a\u003e, Discr. Math. 110 (1992), 257-261.",
				"R. M. Robinson, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9939-1992-1140672-5\"\u003ePeriodicity of Somos sequences\u003c/a\u003e, Proc. Amer. Math. Soc., 116 (1992), 613-619.",
				"Vladimir Shevelev and Peter J. C. Moses, \u003ca href=\"http://arxiv.org/abs/1112.5715\"\u003eOn a sequence of polynomials with hypothetically integer coefficients\u003c/a\u003e, arXiv preprint arXiv:1112.5715 [math.NT], 2011.",
				"Michael Somos, \u003ca href=\"http://grail.eecs.csuohio.edu/~somos/somos6.html\"\u003eSomos 6 Sequence\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"http://faculty.uml.edu/jpropp/somos/history.txt\"\u003eBrief history of the Somos sequence problem\u003c/a\u003e",
				"A. van der Poorten, \u003ca href=\"http://arXiv.org/abs/math.NT/0608247\"\u003eHyperelliptic curves, continued fractions and Somos sequences\u003c/a\u003e, arXiv:math/0608247 [math.NT], 2006.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/SomosSequence.html\"\u003eSomos Sequence.\u003c/a\u003e",
				"\u003ca href=\"/index/Tu#2wis\"\u003eIndex entries for two-way infinite sequences\u003c/a\u003e"
			],
			"formula": [
				"a(n) = a(5-n).",
				"Michael Somos found an explicit formula for a(n) in 1993, which is not as widely known as it should be. The following is a quotation from the \"Somos 6 sequence\" document mentioned in the Links section: (Start)",
				"This sequence is one of a large class of sequences of numbers that satisfy a non-linear recurrence relation depending on previous terms. It is also one of the class of sequences which can be computed from a theta series, hence I call them theta sequences. Here are the details:",
				"Fix the following seven constants:",
				"c1 = 0.875782749065950194217251...,",
				"c2 = 1.084125925473763343779968...,",
				"c3 = 0.114986002186402203509006...,",
				"c4 = 0.077115634258697284328024...,",
				"c5 = 1.180397390176742642553759...,",
				"c6 = 1.508030831265086447098989..., and",
				"c7 = 2.551548771413081602906643... .",
				"Consider the doubly indexed series: f(x,y) = c1*c2^(x*y)*sum(k2, (-1)^k2*sum(k1, g(k1,k2,x,y))) , where g(k1,k2,x,y) = c3^(k1*k1)*c4^(k2*k2)*c5^(k1*k2)*cos(c6*k1*x+c7*k2*y) . Here both sums range over all integers.",
				"Then the sequence defined by a(n) = f(n-2.5,n-2.5) is the Somos 6 sequence. I announced this in 1993. (End) - _N. J. A. Sloane_, Dec 06 2015",
				"From _Andrew Hone_ and Yuri Fedorov, Nov 27 2015: (Start)",
				"The following is an exact formula for a(n):",
				"a(n+3) = A*B^n*C^(n^2 -1)*sigma(v_0 + n*v) / sigma(v)^(n^2),",
				"where",
				"A = C / sigma(v_0),",
				"B = A^(-1)*sigma(v) / sigma(v_0+v),",
				"C = i/sqrt(20) (with i the imaginary unit),",
				"sigma is the two-variable Kleinian sigma-function associated with the genus two curve X: y^2 = 4*x^5 - 233*x^4 + 1624*x^3 - 422*x^2 + 36*x - 1, and",
				"v and v_0 are two-component vectors in the Jacobian of X, being the images under the Abel map of the divisors P_1+P_2 - 2*infinity,  Q_1 + Q_2 - 2*infinity, respectively, where points P_j and Q_j on X are given by",
				"P_1 = ( -8 + sqrt(65), 20*i*(129 -16*sqrt(65)) ),",
				"P_2 = ( -8 - sqrt(65), 20*i*(129 +16*sqrt(65)) ),",
				"Q_1 = ( 5 + 2*sqrt(6), 4*i*(71 +sqrt(6)) ),",
				"Q_2 = ( 5 - 2*sqrt{6}, 4*i*(71 -sqrt(6)) ).",
				"The Abel map is based at infinity and calculated with respect to the basis of holomorphic differentials dx/y, x dx/y.",
				"Approximate values from Maple are A =  0.0619-0.0317*i, B = -0.0000973-0.0000158*i, v = (-.341*i, .477*i), v_0 = (-.379-.150*i, -.259+.576*i).",
				"(End)"
			],
			"mathematica": [
				"a[n_ /; 0 \u003c= n \u003c= 5] = 1; a[n_] := a[n] = (a[n-1]*a[n-5] + a[n-2]*a[n-4] + a[n-3]^2) / a[n-6]; Table[a[n], {n, 0, 25}] (* _Jean-François Alcover_, Nov 22 2013 *)",
				"RecurrenceTable[{a[0]==a[1]==a[2]==a[3]==a[4]==a[5]==1,a[n]==(a[n-1]a[n-5]+ a[n-2]a[n-4]+a[n-3]^2)/a[n-6]},a,{n,30}] (* _Harvey P. Dale_, Dec 20 2014 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003e-1 \u0026\u0026 n\u003c6, 1, if( n\u003c0, a(5 - n), (a(n - 1) * a(n - 5) + a(n - 2) * a(n - 4) + a(n-3) * a(n-3)) / a(n - 6)))}; /* _Michael Somos_, Jan 30 2012 */",
				"(Haskell)",
				"a006722 n = a006722_list !! n",
				"a006722_list = [1,1,1,1,1,1] ++",
				"  zipWith div (foldr1 (zipWith (+)) (map b [1..3])) a006722_list",
				"  where b i = zipWith (*) (drop i a006722_list) (drop (6-i) a006722_list)",
				"-- _Reinhard Zumkeller_, Jan 22 2012",
				"(Python)",
				"from gmpy2 import divexact",
				"A006722 = [1,1,1,1,1,1]",
				"for n in range(6,101):",
				"....A006722.append(divexact(A006722[n-1]*A006722[n-5]+A006722[n-2]*A006722[n-4]+A006722[n-3]**2,A006722[n-6]))",
				"# _Chai Wah Wu_, Sep 01 2014",
				"(MAGMA) [n le 6 select 1 else (Self(n-1)*Self(n-5)+Self(n-2)*Self(n-4)+ Self(n-3)^2)/Self(n-6): n in [1..30]]; // _Vincenzo Librandi_, Dec 02 2015"
			],
			"xref": [
				"Cf. A006720, A006721, A006723, A048736."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,7",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _James A. Sellers_, Aug 22 2000"
			],
			"references": 15,
			"revision": 93,
			"time": "2021-03-12T22:24:41-05:00",
			"created": "1991-07-25T03:00:00-04:00"
		}
	]
}