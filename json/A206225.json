{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A206225",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 206225,
			"data": "1,2,6,4,3,10,12,8,5,14,18,9,7,15,20,24,16,30,22,11,21,26,28,36,42,13,34,40,48,32,60,17,38,54,27,19,33,44,50,25,66,46,23,35,39,52,45,56,72,90,84,78,70,58,29,62,31,51,68,80,96,64,120",
			"name": "Numbers j such that the numbers Phi(j, m) are in sorted order for any integer m \u003e= 2, where Phi(k, x) is the k-th cyclotomic polynomial.",
			"comment": [
				"Based on A002202 \"Values taken by totient function phi(m)\", A000010 can only take certain even numbers. So for the worst case, the largest Phi(k,m) with degree d (even positive integer) will be (1-k^(d+1))/(1-k) (or smaller) and the smallest Phi(k,m) with degree d+2 will be (1+k^(d+3))/(1+k) (or larger).",
				"Note that (1+k^(d+3))/(1+k)-(1-k^(d+1))/(1-k) = (k/(k^2-1))*(2+k^d*(k^3-(k^2+k+1))) \u003e= 0 since k^3 \u003e k^2+k+1 when k \u003e= 2.",
				"This means that this sequence can be segmented into sets in which Phi(k,m) shares the same degree of polynomial and it can be generated in this way."
			],
			"link": [
				"S. P. Glasby, \u003ca href=\"https://arxiv.org/abs/1903.02951\"\u003eCyclotomic ordering conjecture\u003c/a\u003e, arXiv:1903.02951 [math.NT], 2019.",
				"Carl Pomerance and Simon Rubinstein-Salzedo, \u003ca href=\"https://arxiv.org/abs/1903.01962\"\u003eCyclotomic Coincidences\u003c/a\u003e, arXiv:1903.01962 [math.NT], 2019."
			],
			"example": [
				"For k such that A000010(k) = 1,",
				"  Phi(1,m) = -1 + m,",
				"  Phi(2,m) = 1 + m,",
				"  Phi(1,m) \u003c Phi(2,m),",
				"  so, a(1)=1, a(2)=2.",
				"For k \u003e 2 such that A000010(k) = 2,",
				"  Phi(3,m) = 1 + m + m^2,",
				"  Phi(4,m) = 1 + m^2,",
				"  Phi(6,m) = 1 - m + m^2.",
				"For m \u003e 1, Phi(6,m) \u003c Phi(4,m) \u003c Phi(3,m), so a(3)=6, a(4)=4, and a(5)=3 (noting that Phi(6,m) \u003e Phi(2,m) when m \u003e 2, and Phi(6,2) = Phi(2,2)).",
				"For k such that A000010(k) = 4,",
				"  Phi(5,m) = 1 + m + m^2 + m^3 + m^4,",
				"  Phi(8,m) = 1 + m^4,",
				"  Phi(10,m) = 1 - m + m^2 - m^3 + m^4,",
				"  Phi(12,m) = 1 - m^2 + m^4.",
				"For m \u003e 1, Phi(10,m) \u003c Phi(12,m) \u003c Phi(8,m) \u003c Phi(5,m), so a(6) = 10, a(7) = 12, a(8) = 8, and a(9) = 5 (noting Phi(10,m) - Phi(3,m) = m((m^2 + m + 2)(m - 2) + 2) \u003e= 4 \u003e 0 when m \u003e= 2)."
			],
			"mathematica": [
				"t = Select[Range[400], EulerPhi[#] \u003c= 40 \u0026]; SortBy[t, Cyclotomic[#, 2] \u0026]"
			],
			"xref": [
				"Cf. A194712, A000010, A002202, A032447."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Lei Zhou_, Feb 13 2012",
			"references": 7,
			"revision": 57,
			"time": "2021-07-13T19:49:37-04:00",
			"created": "2012-02-16T15:48:54-05:00"
		}
	]
}