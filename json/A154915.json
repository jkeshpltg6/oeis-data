{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A154915",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 154915,
			"data": "4,3,3,5,8,5,9,24,24,9,17,70,112,70,17,33,198,480,480,198,33,65,544,1920,2880,1920,544,65,129,1452,7308,15624,15624,7308,1452,129,257,3770,26724,80640,108864,80640,26724,3770,257,513,9546,94644,408312,706608,706608,408312,94644,9546,513",
			"name": "Triangle T(n,m,p,q) = (p^(n-k)*q^k + p^k*q^(n-k))*(StirlingS2(n, k) + StirlingS2(n, n-k)) with p=2 and q=1, read by rows.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A154915/b154915.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/BellPolynomial.html\"\u003eBell Polynomial\u003c/a\u003e"
			],
			"formula": [
				"T(n,m,p,q) = (p^(n-k)*q^k + p^k*q^(n-k))*(StirlingS2(n, k) + StirlingS2(n, n-k)) with p=2 and q=1.",
				"Sum_{k=0..n} T(n,k,p,q) = 2*p^n*( T_{n}(q/p) + (q/p)^n*T_{n}(p/q) ), with p=2 and q=1, where T_{n}(x) are the Touchard polynomials (sometimes named Bell polynomials). - _G. C. Greubel_, Mar 02 2021"
			],
			"example": [
				"Triangle begins as:",
				"    4;",
				"    3,    3;",
				"    5,    8,     5;",
				"    9,   24,    24,      9;",
				"   17,   70,   112,     70,     17;",
				"   33,  198,   480,    480,    198,     33;",
				"   65,  544,  1920,   2880,   1920,    544,     65;",
				"  129, 1452,  7308,  15624,  15624,   7308,   1452,   129;",
				"  257, 3770, 26724,  80640, 108864,  80640,  26724,  3770,  257;",
				"  513, 9546, 94644, 408312, 706608, 706608, 408312, 94644, 9546, 513;"
			],
			"maple": [
				"A154915:= (n,k,p,q) -\u003e (p^(n-k)*q^k + p^k*q^(n-k))*(combinat[stirling2](n, k) + combinat[stirling2](n, n-k));",
				"seq(seq(A154915(n,k,2,1), k=0..n), n=0..12); # _G. C. Greubel_, Mar 02 2021"
			],
			"mathematica": [
				"T[n_, k_, p_, q_]:= (p^(n-k)*q^k + p^k*q^(n-k))*(StirlingS2[n, k] + StirlingS2[n, n-k]);",
				"Table[T[n, k, 2, 1], {n,0,12}, {k,0,n}]//Flatten (* modified by _G. C. Greubel_, Mar 02 2021 *)"
			],
			"program": [
				"(Sage)",
				"def A154915(n,k,p,q): return (p^(n-k)*q^k + p^k*q^(n-k))*(stirling_number2(n, k) + stirling_number2(n, n-k))",
				"flatten([[A154915(n,k,2,1) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Mar 02 2021",
				"(Magma)",
				"A154915:= func\u003c n,k,p,q | (p^(n-k)*q^k + p^k*q^(n-k))*(StirlingSecond(n, k) + StirlingSecond(n, n-k)) \u003e;",
				"[A154915(n,k,2,1): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Mar 02 2021"
			],
			"xref": [
				"Cf. this sequence (q=1), A154916 (q=3), A154922 (q=5).",
				"Cf. A008277, A048993, A154913, A154914."
			],
			"keyword": "nonn,tabl,easy,less",
			"offset": "0,1",
			"author": "_Roger L. Bagula_, Jan 17 2009",
			"references": 5,
			"revision": 15,
			"time": "2021-03-02T09:22:55-05:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}