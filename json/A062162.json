{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A062162",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 62162,
			"data": "1,0,0,1,0,5,10,61,280,1665,10470,73621,561660,4650425,41441530,395757181,4031082640,43626778785,499925138190,6046986040741,76992601769220,1029315335116745,14416214547400450,211085887742964301,3225154787165157400,51329932704636904305",
			"name": "Boustrophedon transform of (-1)^n.",
			"comment": [
				"Inverse binomial transform of Euler numbers A000111. - _Paul Barry_, Jan 21 2005",
				"a(n) = abs(sum of row n in A247453). - _Reinhard Zumkeller_, Sep 17 2014"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A062162/b062162.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"Peter Luschny, \u003ca href=\"http://oeis.org/wiki/User:Peter_Luschny/SeidelTransform\"\u003eAn old operation on sequences: the Seidel transform\u003c/a\u003e.",
				"Ludwig Seidel, \u003ca href=\"https://babel.hathitrust.org/cgi/pt?id=hvd.32044092897461\u0026amp;view=1up\u0026amp;seq=175\"\u003eÜber eine einfache Entstehungsweise der Bernoulli'schen Zahlen und einiger verwandten Reihen\u003c/a\u003e, Sitzungsberichte der mathematisch-physikalischen Classe der königlich bayerischen Akademie der Wissenschaften zu München, volume 7 (1877), 157-187. [USA access only through the \u003ca href=\"https://www.hathitrust.org/accessibility\"\u003eHATHI TRUST Digital Library\u003c/a\u003e]",
				"Ludwig Seidel, \u003ca href=\"https://www.zobodat.at/pdf/Sitz-Ber-Akad-Muenchen-math-Kl_1877_0157-0187.pdf\"\u003eÜber eine einfache Entstehungsweise der Bernoulli'schen Zahlen und einiger verwandten Reihen\u003c/a\u003e, Sitzungsberichte der mathematisch-physikalischen Classe der königlich bayerischen Akademie der Wissenschaften zu München, volume 7 (1877), 157-187. [Access through \u003ca href=\"https://de.wikipedia.org/wiki/ZOBODAT\"\u003eZOBODAT\u003c/a\u003e]",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Boustrophedon_transform\"\u003eBoustrophedon transform\u003c/a\u003e.",
				"\u003ca href=\"/index/Bo#boustrophedon\"\u003eIndex entries for sequences related to boustrophedon transform\u003c/a\u003e"
			],
			"formula": [
				"E.g.f.: exp(-x)*(tan(x) + sec(x)). - _Vladeta Jovovic_, Feb 11 2003",
				"a(n) ~ 4*(2*n/Pi)^(n+1/2)/exp(n+Pi/2). - _Vaclav Kotesovec_, Oct 05 2013",
				"G.f.: E(0)*x/(1+x) + 1/(1+x), where E(k) = 1 - x^2*(k+1)*(k+2)/(x^2*(k+1)*(k+2) - 2*(x*k-1)*(x*(k+1)-1)/E(k+1) ); (continued fraction). - _Sergei N. Gladkovskii_, Jan 16 2014"
			],
			"mathematica": [
				"CoefficientList[Series[E^(-x)*(Tan[x]+1/Cos[x]), {x, 0, 20}], x]* Range[0, 20]! (* _Vaclav Kotesovec_, Oct 05 2013 *)",
				"t[n_, 0] := (-1)^n; t[n_, k_] := t[n, k] = t[n, k-1] + t[n-1, n-k]; a[n_] := t[n, n]; Array[a, 30, 0] (* _Jean-François Alcover_, Feb 12 2016 *)"
			],
			"program": [
				"(Sage) # Generalized algorithm of L. Seidel (1877)",
				"def A062162_list(n) :",
				"    R = []; A = {-1:0, 0:0}",
				"    k = 0; e = 1",
				"    for i in range(n) :",
				"        Am = (-1)^i",
				"        A[k + e] = 0",
				"        e = -e",
				"        for j in (0..i) :",
				"            Am += A[k]",
				"            A[k] = Am",
				"            k += e",
				"        R.append(A[e*i//2])",
				"    return R",
				"A062162_list(22) # _Peter Luschny_, Jun 02 2012",
				"(Haskell)",
				"a062162 = abs . sum . a247453_row -- _Reinhard Zumkeller_, Sep 17 2014"
			],
			"xref": [
				"Cf. A000111 (binomial transform).",
				"Cf. A000667.",
				"Cf. A247453."
			],
			"keyword": "nonn,easy",
			"offset": "0,6",
			"author": "_Frank Ellermann_, Jun 10 2001",
			"references": 8,
			"revision": 36,
			"time": "2019-08-13T02:16:54-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}