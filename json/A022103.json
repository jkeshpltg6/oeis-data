{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A022103",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 22103,
			"data": "1,13,14,27,41,68,109,177,286,463,749,1212,1961,3173,5134,8307,13441,21748,35189,56937,92126,149063,241189,390252,631441,1021693,1653134,2674827,4327961,7002788,11330749,18333537,29664286,47997823,77662109,125659932,203322041,328981973",
			"name": "Fibonacci sequence beginning 1, 13.",
			"comment": [
				"a(n-1) = Sum_{k=0..ceiling((n-1)/2)} P(13;n-1-k,k) for n\u003e=1, a(-1)=12. These are the SW-NE diagonals in P(13;n,k), the (13,1) Pascal triangle. Cf. A093645 for the (10,1) Pascal triangle. Observation by _Paul Barry_, Apr 29 2004. Proof via recursion relations and comparison of inputs.",
				"In general, for b Fibonacci sequence beginning with 1, h, we have:",
				"b(n) = (2^(-1-n)*((1 - sqrt(5))^n*(1 + sqrt(5) - 2*h) + (1 + sqrt(5))^n*(-1 + sqrt(5) + 2*h)))/sqrt(5). - _Herbert Kociemba_, Dec 18 2011",
				"Pisano period lengths: 1, 3, 8, 6, 4, 24, 16, 12, 24, 12, 10, 24, 28, 48, 8, 24, 36, 24, 18, 12, ... (is this A106291?). - _R. J. Mathar_, Aug 10 2012"
			],
			"link": [
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,1)."
			],
			"formula": [
				"a(n) = a(n-1) + a(n-2) for n\u003e=2, a(0)=1, a(1)=13, and a(-1):=12.",
				"G.f.: (1 + 12*x)/(1 - x - x^2).",
				"a(n) = ((1 + sqrt(5))^n-(1 - sqrt(5))^n)/(2^n*sqrt(5))+ 6*((1 + sqrt(5))^(n-1)-(1 - sqrt(5))^(n-1))/(2^(n-2)*sqrt(5)) for n\u003e0. - Al Hakanson (hawkuu(AT)gmail.com), Jan 14 2009",
				"a(n) = 12*A000045(n) + A000045(n+1). - _R. J. Mathar_, Aug 10 2012",
				"a(n) = 13*A000045(n) + A000045(n-1). - _Paolo P. Lava_, May 19 2015",
				"a(n) = 14*A000045(n) - A000045(n-2). - _Bruno Berselli_, Feb 20 2017",
				"a(n) = Lucas(n+5) - 5*Lucas(n). - _Bruno Berselli_, Dec 30 2016"
			],
			"maple": [
				"with(numtheory): with(combinat): P:=proc(q) local n;",
				"for n from 0 to q do print(13*fibonacci(n)+fibonacci(n-1));",
				"od; end: P(30); # _Paolo P. Lava_, May 19 2015"
			],
			"mathematica": [
				"LinearRecurrence[{1, 1}, {1, 13}, 40] (* or *) Table[LucasL[n + 5] - 5 LucasL[n], {n, 0, 40}] (* _Bruno Berselli_, Dec 30 2016 *)"
			],
			"program": [
				"(MAGMA) a0:=1; a1:=13; [GeneralizedFibonacciNumber(a0, a1, n): n in [0..30]]; // _Bruno Berselli_, Feb 12 2013"
			],
			"xref": [
				"a(n) = A109754(12, n+1) = A101220(12, 0, n+1).",
				"Cf. A000032, A000045."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 6,
			"revision": 47,
			"time": "2017-02-20T12:07:40-05:00",
			"created": "1998-06-14T03:00:00-04:00"
		}
	]
}