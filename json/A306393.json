{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A306393",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 306393,
			"data": "1,1,1,1,2,2,2,3,6,6,6,3,8,16,24,24,24,16,8,20,60,100,120,120,120,100,60,20,80,240,480,640,720,720,720,640,480,240,80,210,840,1890,3150,4200,4830,5040,5040,4830,4200,3150,1890,840,210",
			"name": "Number T(n,k) of defective (binary) heaps on n elements where k ancestor-successor pairs do not have the correct order; triangle T(n,k), n\u003e=0, 0\u003c=k\u003c=A061168(n), read by rows.",
			"comment": [
				"T(n,k) is the number of permutations p of [n] having exactly k pairs (i,j) in {1,...,n} X {1,...,floor(log_2(i))} such that p(i) \u003e p(floor(i/2^j)).",
				"T(n,0) counts perfect (binary) heaps on n elements (A056971)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A306393/b306393.txt\"\u003eRows n = 0..100, flattened\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Heap.html\"\u003eHeap\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Binary_heap\"\u003eBinary heap\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Permutation\"\u003ePermutation\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = T(n,A061168(n)-k) for n \u003e 0.",
				"Sum_{k=0..A061168(n)} k * T(n,k) = A324074(n)."
			],
			"example": [
				"T(4,0) = 3: 4231, 4312, 4321.",
				"T(4,1) = 6: 3241, 3412, 3421, 4123, 4132, 4213.",
				"T(4,2) = 6: 2341, 2413, 2431, 3124, 3142, 3214.",
				"T(4,3) = 6: 1342, 1423, 1432, 2134, 2143, 2314.",
				"T(4,4) = 3: 1234, 1243, 1324.",
				"T(5,1) = 16: 43512, 43521, 45123, 45132, 45213, 45231, 45312, 45321, 52314, 52341, 52413, 52431, 53124, 53142, 53214, 53241.",
				"(The examples use max-heaps.)",
				"Triangle T(n,k) begins:",
				"   1;",
				"   1;",
				"   1,   1;",
				"   2,   2,   2;",
				"   3,   6,   6,   6,   3;",
				"   8,  16,  24,  24,  24,  16,   8;",
				"  20,  60, 100, 120, 120, 120, 100,  60,  20;",
				"  80, 240, 480, 640, 720, 720, 720, 640, 480, 240, 80;",
				"  ..."
			],
			"maple": [
				"b:= proc(u, o) option remember; local n, g, l; n:= u+o;",
				"      if n=0 then 1",
				"    else g:= 2^ilog2(n); l:= min(g-1, n-g/2); expand(",
				"         add(x^(n-j)*add(binomial(j-1, i)*binomial(n-j, l-i)*",
				"         b(i, l-i)*b(j-1-i, n-l-j+i), i=0..min(j-1, l)), j=1..u)+",
				"         add(x^(j-1)*add(binomial(j-1, i)*binomial(n-j, l-i)*",
				"         b(l-i, i)*b(n-l-j+i, j-1-i), i=0..min(j-1, l)), j=1..o))",
				"      fi",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=0..degree(p)))(b(n, 0)):",
				"seq(T(n), n=0..10);"
			],
			"mathematica": [
				"b[u_, o_] := b[u, o] = Module[{n, g, l}, n = u + o;",
				"     If[n == 0, 1, g = 2^Floor@Log[2, n]; l = Min[g - 1, n - g/2]; Expand[",
				"     Sum[x^(n-j)*Sum[Binomial[j - 1, i]*Binomial[n - j, l - i]*",
				"     b[i, l-i]*b[j-1-i, n-l-j+i], {i, 0, Min[j - 1, l]}], {j, 1, u}] +",
				"     Sum[x^(j-1)*Sum[Binomial[j - 1, i]*Binomial[n - j, l - i]*",
				"     b[l-i, i]*b[n-l-j+i, j-1-i], {i, 0, Min[j-1, l]}], {j, 1, o}]]]];",
				"T[n_] := CoefficientList[b[n, 0], x];",
				"T /@ Range[0, 10] // Flatten (* _Jean-François Alcover_, Feb 15 2021, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Columns k=0-10 give: A056971, A324062, A324063, A324064, A324065, A324066, A324067, A324068, A324069, A324070, A324071.",
				"Row sums give A000142.",
				"Central terms (also maxima) of rows give A324075.",
				"Cf. A000523, A008302, A061168, A120385, A306343, A324074."
			],
			"keyword": "nonn,tabf",
			"offset": "0,5",
			"author": "_Alois P. Heinz_, Feb 12 2019",
			"references": 15,
			"revision": 40,
			"time": "2021-02-15T08:07:20-05:00",
			"created": "2019-02-13T10:56:46-05:00"
		}
	]
}