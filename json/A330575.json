{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A330575",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 330575,
			"data": "1,3,4,8,6,14,8,20,14,20,12,42,14,26,26,48,18,54,20,58,34,38,24,116,32,44,46,74,30,104,32,112,50,56,50,176,38,62,58,156,42,132,44,106,96,74,48,304,58,112,74,122,54,190,74,196,82,92,60,346,62,98,124,256,86",
			"name": "a(n) = n + Sum_{d|n and d\u003cn} a(d) for n\u003e1; a(1) = 1.",
			"link": [
				"Robert Israel, \u003ca href=\"/A330575/b330575.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Thomas Fink, \u003ca href=\"https://arxiv.org/abs/1912.07979\"\u003eRecursively divisible numbers\u003c/a\u003e, arXiv:1912.07979 [math.NT], 2019. See Table 2 p. 11."
			],
			"formula": [
				"a(p) = p+1 for p prime.",
				"a(n) = n + A255242(n). - _Rémy Sigrist_, Dec 18 2019",
				"G.f. A(x) satisfies: A(x) = x/(1 - x)^2 + Sum_{k\u003e=2} A(x^k). - _Ilya Gutkovskiy_, Dec 18 2019",
				"a(n) = Sum_{d|n} A074206(d) * n/d. - _David A. Corneth_, Apr 13 2020"
			],
			"example": [
				"a(2) = 2 + a(1) = 2 + 1 = 3, since the only proper divisors of 2 is 1.",
				"a(4) = 4 + a(1) + a(2) = 4 + 1 + 3 = 8, since the proper divisors of 4 are 1 and 2.",
				"a(6) = 6 + a(1) + a(2) + a(3) = 6 + 1 + 3 + 4 = 14, since the proper divisors of 6 are 1, 2 and 3."
			],
			"maple": [
				"f:= proc(n) option remember;",
				"n + add(procname(d), d = numtheory:-divisors(n) minus {n})",
				"end proc:",
				"map(f, [$1..100]); # _Robert Israel_, Dec 19 2019"
			],
			"mathematica": [
				"a[1] = 1; a[n_] := a[n] = n + DivisorSum[n, a[#] \u0026, # \u003c n \u0026]; Array[a, 65] (* _Amiram Eldar_, Apr 12 2020 *)"
			],
			"program": [
				"(PARI) a(n) = if (n==1, 1, n + sumdiv(n, d, if (d\u003cn, a(d))));",
				"(MAGMA) a:=[1]; for n in [2..65] do Append(~a,(n+\u0026+[a[d]:d in Set(Divisors(n)) diff {n}])); end for; a; // _Marius A. Burtea_, Dec 18 2019"
			],
			"xref": [
				"Cf. A067824, A074206, A255242."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Michel Marcus_, Dec 18 2019",
			"references": 6,
			"revision": 28,
			"time": "2020-04-14T20:52:34-04:00",
			"created": "2019-12-18T05:46:58-05:00"
		}
	]
}