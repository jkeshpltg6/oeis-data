{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A328000",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 328000,
			"data": "1,2,5,16,28,96,160,512,896,2560,4864,12288,25600,57344,131072,262144,655360,1179648,3211264,5242880,15466496,23068672,73400320,100663296,343932928,436207616,1593835520,1879048192,7314866176,8053063680,33285996544,34359738368",
			"name": "a(n) = Sum_{k=0..n}(k!*(n - k)!)/(floor(k/2)!*floor((n - k)/2)!)^2.",
			"link": [
				"Colin Barker, \u003ca href=\"/A328000/b328000.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,12,0,-48,0,64)."
			],
			"formula": [
				"a(n) = Sum_{k=0..n} s(k)*s(n-k) where s(n) = A056040(n).",
				"a(n) = [x^n] (4*x^2 - x - 1)^2 / (1 - 4*x^2)^3.",
				"a(n) = 2^(n - 5)*(n*(n + 2) + 32) if n even else 2^(n - 1)*(n + 1).",
				"a(2*n) = A327999(n).",
				"a(2*n-1) = A002699(n), (with a(-1) = 0).",
				"a(2^n-1) = 2^(2^n - 2 + n) for n \u003e= 1.",
				"2*a(2*n)/2^n = A081908(n+1).",
				"4*a(2*n)/4^n = A145018(n+1).",
				"2*a(2*n-1)/4^n = A001477(n).",
				"From _Stefano Spezia_, Oct 19 2019: (Start)",
				"a(n) = n! [x^n] (1/32)*exp(-2*x)*(8 + exp(4*x)*(8 + x)*(3 + 2*x) + x*(13 + 2*x)).",
				"a(n) = 12*a(n-2) - 48*a(n-4) + 64*a(n-6) for n \u003e 5. (End)"
			],
			"maple": [
				"swing := n -\u003e n!/iquo(n,2)!^2: a := n -\u003e add(swing(k)*swing(n-k), k=0..n):",
				"seq(`if`(irem(n, 2) = 0, 2 + n*(n + 2)/16, n + 1)*2^(n - 1), n=0..31);"
			],
			"mathematica": [
				"A32800List[len_] := CoefficientList[Series[(4 x^2 - x - 1)^2 / (1 - 4 x^2)^3 , {x, 0, len}], x]; A32800List[31]"
			],
			"program": [
				"(PARI) x='x + O('x^32);",
				"Vec(serlaplace(((3*x + 8)*sinh(2*x) + (2*x^2 + 16*(x + 1))*cosh(2*x))/16))",
				"(PARI) Vec((1 + x - 4*x^2)^2 / ((1 - 2*x)^3*(1 + 2*x)^3) + O(x^30)) \\\\ _Colin Barker_, Feb 05 2020",
				"(MAGMA) [IsOdd(n) select 2^(n - 1)*(n + 1) else 2^(n - 5)*(n*(n + 2) + 32):n in [0..30]]; // _Marius A. Burtea_, Feb 05 2020"
			],
			"xref": [
				"Cf. A327999, A328001.",
				"Cf. A056040, A002699, A001477, A081908, A145018."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Peter Luschny_, Oct 01 2019",
			"references": 3,
			"revision": 30,
			"time": "2020-02-05T16:23:05-05:00",
			"created": "2019-10-19T12:29:12-04:00"
		}
	]
}