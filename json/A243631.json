{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A243631",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 243631,
			"data": "1,1,1,1,1,1,1,1,2,1,1,1,3,5,1,1,1,4,11,14,1,1,1,5,19,45,42,1,1,1,6,29,100,197,132,1,1,1,7,41,185,562,903,429,1,1,1,8,55,306,1257,3304,4279,1430,1,1,1,9,71,469,2426,8925,20071,20793,4862,1",
			"name": "Square array of Narayana polynomials N_n evaluated at the integers, A(n,k) = N_n(k), n\u003e=0, k\u003e=0, read by antidiagonals.",
			"comment": [
				"Mirror image of A008550. - _Philippe Deléham_, Sep 26 2014"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A243631/b243631.txt\"\u003eAntidiagonals n = 0..139, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = 2F1([1-n, -n], [2], k), 2F1 the hypergeometric function.",
				"T(n, k) = P(n,1,-2*n-1,1-2*k)/(n+1), P the Jacobi polynomials.",
				"T(n, k) = sum(j=0..n-1, binomial(n,j)^2*(n-j)/(n*(j+1))*k^j), for n\u003e0.",
				"For a recurrence see the second Maple program.",
				"The o.g.f. of column n is gf(n) = 2/(sqrt((n-1)^2*x^2-2*(n+1)*x+1)+(n-1)*x+1). - _Peter Luschny_, Nov 17 2014",
				"T(n, k) ~ (sqrt(k)+1)^(2*n+1)/(2*sqrt(Pi)*k^(3/4)*n^(3/2)). - _Peter Luschny_, Nov 17 2014",
				"The n-th row can for n\u003e=1 be computed by a linear recurrence, a(x) = sum(k=1..n, (-1)^(k+1)*binomial(n,k)*a(x-k)) with initial values a(k) = p(n,k) for k=0..n and p(n,x) = sum(j=0..n-1, binomial(n-1,j)*binomial(n,j)*x^j/(j+1)) (implemented in the fourth Maple script). - _Peter Luschny_, Nov 19 2014",
				"(n+1) * T(n,k) = (k+1) * (2*n-1) * T(n-1,k) - (k-1)^2 * (n-2) * T(n-2,k) for n\u003e1. - _Seiichi Manyama_, Aug 08 2020",
				"Sum_{k=0..n} T(k, n-k) = Sum_{k=0..n} 2F1([-k, 1-k], [2], n-k) = A132745(n). - _G. C. Greubel_, Feb 16 2021"
			],
			"example": [
				"   [0]  [1]      [2]      [3]      [4]      [5]      [6]     [7]",
				"[0] 1,   1,       1,       1,       1,       1,       1,       1",
				"[1] 1,   1,       1,       1,       1,       1,       1,       1",
				"[2] 1,   2,       3,       4,       5,       6,       7,       8 .. A000027",
				"[3] 1,   5,      11,      19,      29,      41,      55,      71 .. A028387",
				"[4] 1,  14,      45,     100,     185,     306,     469,     680 .. A090197",
				"[5] 1,  42,     197,     562,    1257,    2426,    4237,    6882 .. A090198",
				"[6] 1, 132,     903,    3304,    8925,   20076,   39907,   72528 .. A090199",
				"[7] 1, 429,    4279,   20071,   65445,  171481,  387739,  788019 .. A090200",
				"   A000108, A001003, A007564, A059231, A078009, A078018, A081178",
				"First few rows of the antidiagonal triangle are:",
				"  1;",
				"  1, 1;",
				"  1, 1, 1;",
				"  1, 1, 2,  1;",
				"  1, 1, 3,  5,  1;",
				"  1, 1, 4, 11, 14,  1;",
				"  1, 1, 5, 19, 45, 42, 1; - _G. C. Greubel_, Feb 16 2021"
			],
			"maple": [
				"# Computed with Narayana polynomials:",
				"N := (n,k) -\u003e binomial(n,k)^2*(n-k)/(n*(k+1));",
				"A := (n,x) -\u003e `if`(n=0, 1, add(N(n,k)*x^k, k=0..n-1));",
				"seq(print(seq(A(n,k), k=0..7)), n=0..7);",
				"# Computed by recurrence:",
				"Prec := proc(n,N,k) option remember; local A,B,C,h;",
				"if n = 0 then 1 elif n = 1 then 1+N+(1-N)*(1-2*k)",
				"else h := 2*N-n; A := n*h*(1+N-n); C := n*(h+2)*(N-n);",
				"B := (1+h-n)*(n*(1-2*k)*(1+h)+2*k*N*(1+N));",
				"(B*Prec(n-1,N,k) - C*Prec(n-2,N,k))/A fi end:",
				"T := (n, k) -\u003e Prec(n,n,k)/(n+1);",
				"seq(print(seq(T(n,k), k=0..7)), n=0..7);",
				"# Array by o.g.f. of columns:",
				"gf := n -\u003e 2/(sqrt((n-1)^2*x^2-2*(n+1)*x+1)+(n-1)*x+1):",
				"for n from 0 to 11 do PolynomialTools:-CoefficientList(convert( series(gf(n), x, 12), polynom), x) od; # _Peter Luschny_, Nov 17 2014",
				"# Row n by linear recurrence:",
				"rec := n -\u003e a(x) = add((-1)^(k+1)*binomial(n,k)*a(x-k), k=1..n):",
				"ini := n -\u003e seq(a(k) = A(n,k), k=0..n): # for A see above",
				"row := n -\u003e gfun:-rectoproc({rec(n),ini(n)},a(x),list):",
				"for n from 1 to 7 do row(n)(8) od; # _Peter Luschny_, Nov 19 2014"
			],
			"mathematica": [
				"MatrixForm[Table[JacobiP[n,1,-2*n-1,1-2*x]/(n+1), {n,0,7},{x,0,7}]]",
				"Table[Hypergeometric2F1[1-k, -k, 2, n-k], {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, Feb 16 2021 *)"
			],
			"program": [
				"(Sage)",
				"def NarayanaPolynomial():",
				"    R = PolynomialRing(ZZ, 'x')",
				"    D = [1]",
				"    h = 0",
				"    b = True",
				"    while True:",
				"        if b :",
				"            for k in range(h, 0, -1):",
				"                D[k] += x*D[k-1]",
				"            h += 1",
				"            yield R(expand(D[0]))",
				"            D.append(0)",
				"        else :",
				"            for k in range(0, h, 1):",
				"                D[k] += D[k+1]",
				"        b = not b",
				"NP = NarayanaPolynomial()",
				"for _ in range(8):",
				"    p = next(NP)",
				"    [p(k) for k in range(8)]",
				"(Sage)",
				"def A243631(n,k): return 1 if n==0 else sum( binomial(n,j)^2*k^j*(n-j)/(n*(j+1)) for j in [0..n-1])",
				"flatten([[A243631(k,n-k) for k in [0..n]] for n in [0..12]]) # _G. C. Greubel_, Feb 16 2021",
				"(Magma)",
				"A243631:= func\u003c n,k | n eq 0 select 1 else (\u0026+[ Binomial(n,j)^2*k^j*(n-j)/(n*(j+1)): j in [0..n-1]]) \u003e;",
				"[A243631(k,n-k): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Feb 16 2021"
			],
			"xref": [
				"Cf. A001263, A008550 (mirror), A204057 (another version), A242369 (main diagonal), A099169 (diagonal), A307883, A336727.",
				"Rows[2-7]: A000027, A028387, A090197, A090198, A090199, A090200.",
				"Columns[1-7]: A000108, A001003, A007564, A059231, A078009, A078018, A081178.",
				"Cf. A132745."
			],
			"keyword": "nonn,tabl",
			"offset": "0,9",
			"author": "_Peter Luschny_, Jun 08 2014",
			"references": 9,
			"revision": 45,
			"time": "2021-02-16T08:12:27-05:00",
			"created": "2014-06-10T05:16:32-04:00"
		}
	]
}