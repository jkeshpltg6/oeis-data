{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A137553",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 137553,
			"data": "1,2,5,14,43,146,561,2518,13563,88354,686137,6191526,63330147,720314930,8985750097,121722964822,1777038601387,27792425428418,463361639828329,8200984957695750,153532638260056115,3030783297332577234",
			"name": "Number of permutations in S_n avoiding {bar 5}{bar 4}231 (i.e., every occurrence of 231 is contained in an occurrence of a 54231).",
			"comment": [
				"From _Lara Pudwell_, Oct 23 2008: (Start)",
				"A permutation p avoids a pattern q if it has no subsequence that is order-isomorphic to q. For example, p avoids the pattern 132 if it has no subsequence abc with a \u003c c \u003c b.",
				"Barred pattern avoidance considers permutations that avoid a pattern except in a special case. Given a barred pattern q, we may form two patterns, q1 = the sequence of unbarred letters of q and q2 = the sequence of all letters of q.",
				"A permutation p avoids barred pattern q if every instance of q1 in p is embedded in a copy of q2 in p. In other words, p avoids q1, except in the special case that a copy of q1 is a subsequence of a copy of q2.",
				"For example, if q = 5{bar 1}32{bar 4}, then q1 = 532 and q2 = 51324. p avoids q if every for decreasing subsequence acd of length 3 in p, one can find letters b and e so that the subsequence abcde of p has b \u003c d \u003c c \u003c e \u003c a. (End)"
			],
			"link": [
				"Vaclav Kotesovec, \u003ca href=\"/A137553/b137553.txt\"\u003eTable of n, a(n) for n = 1..300\u003c/a\u003e",
				"Lara Pudwell, \u003ca href=\"http://faculty.valpo.edu/lpudwell/papers/pudwell_thesis.pdf\"\u003eEnumeration Schemes for Pattern-Avoiding Words and Permutations\u003c/a\u003e, Ph. D. Dissertation, Math. Dept., Rutgers University, May 2008.",
				"L. Pudwell, \u003ca href=\"http://www.emis.de/journals/EJC/Volume_17/Abstracts/v17i1r29.html\"\u003eEnumeration schemes for permutations avoiding barred patterns\u003c/a\u003e, El. J. Combinat. 17 (1) (2010) R29."
			],
			"formula": [
				"G.f. A(x) (for offset 0) satisfies: A(x) = (1-x)^2*A(x)^2 - x^2*A'(x). - _Paul D. Hanna_, Aug 02 2008",
				"a(n) ~ (n-2)!. - _Vaclav Kotesovec_, Mar 15 2014",
				"G.f.: (1 + x/((1-x)*S(0) -x))/(1-x), where S(k)= 1 - (k+1)*x/(1 - (k+1)*x/S(k+1)); (continued fraction). - _Sergei N. Gladkovskii_, Feb 05 2015"
			],
			"mathematica": [
				"CoefficientList[Assuming[Element[x, Reals], Series[1/(1 - x - ExpIntegralEi[1/x]/E^(1/x)), {x, 0, 20}]], x] (* _Vaclav Kotesovec_, Mar 15 2014 *)",
				"max = 20; Clear[g]; g[max + 2] = 1; g[k_] := g[k] = 1 - (k+1)*x/(1 - (k+1)*x/g[k+1]); gf = (1 + x/((1-x)*g[0] -x))/(1-x); CoefficientList[Series[gf, {x, 0, max}], x] (* _Vaclav Kotesovec_, Feb 06 2015, after _Sergei N. Gladkovskii_ *)"
			],
			"program": [
				"(PARI) {a(n)=local(A=1+x+x*O(x^n));for(i=1,n,A=(1+x^2*deriv(A)/A)/(1-x)^2);polcoeff(A,n)} \\\\ _Paul D. Hanna_, Aug 02 2008"
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Lara Pudwell_, Apr 25 2008",
			"references": 2,
			"revision": 20,
			"time": "2018-06-10T08:25:32-04:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}