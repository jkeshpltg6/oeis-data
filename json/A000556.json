{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000556",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 556,
			"id": "M3966 N1638",
			"data": "1,1,5,31,257,2671,33305,484471,8054177,150635551,3130337705,71556251911,1784401334897,48205833997231,1402462784186105,43716593539939351,1453550100421124417,51350258701767067711,1920785418183176050505,75839622064482770570791",
			"name": "Expansion of exp(-x) / (1 - exp(x) + exp(-x)).",
			"reference": [
				"Anthony G. Shannon and Richard L. Ollerton. \"A note on Ledin’s summation problem.\" The Fibonacci Quarterly 59:1 (2021), 47-56.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A000556/b000556.txt\"\u003eTable of n, a(n) for n = 0..401\u003c/a\u003e",
				"Paul Kinlaw, Michael Morris, and Samanthak Thiagarajan, \u003ca href=\"https://www.researchgate.net/publication/350886459_SUMS_RELATED_TO_THE_FIBONACCI_SEQUENCE\"\u003eSums related to the Fibonacci sequence\u003c/a\u003e, Husson University (2021).",
				"G. Ledin, Jr., \u003ca href=\"http://www.fq.math.ca/Scanned/5-1/ledin.pdf\"\u003eOn a certain kind of Fibonacci sums\u003c/a\u003e, Fib. Quart., 5 (1967), 45-58.",
				"Eric Weisstein's MathWorld, \u003ca href=\"http://mathworld.wolfram.com/Polylogarithm.html\"\u003ePolylogarithm\u003c/a\u003e.",
				"Eric Weisstein's MathWorld, \u003ca href=\"http://mathworld.wolfram.com/GoldenRatio.html\"\u003eGolden Ratio\u003c/a\u003e.",
				"Eric Weisstein's MathWorld, \u003ca href=\"http://mathworld.wolfram.com/LucasNumber.html\"\u003eLucas Number\u003c/a\u003e."
			],
			"formula": [
				"a(n) = Sum_{k=0..n} k!*Fibonacci(k+1)*Stirling2(n,k).",
				"E.g.f.: 1/(1 + U(0)) where U(k) = 1 - 2^k/(1 - x/(x - (k+1)*2^k/U(k+1) ));(continued fraction 3rd kind, 3-step ). - _Sergei N. Gladkovskii_, Dec 05 2012",
				"a(n) ~ 2*n! / ((5+sqrt(5)) * log((1+sqrt(5))/2)^(n+1)). - _Vaclav Kotesovec_, May 04 2015",
				"a(n) = (-1)^(n+1)*(Li_{-n}(1-phi)*phi + Li_{-n}(phi)/phi)/sqrt(5), where Li_n(x) is the polylogarithm, phi=(1+sqrt(5))/2 is the golden ratio. - _Vladimir Reshetnikov_, Oct 30 2015",
				"_John W. Layman_ observes that this is also Sum (-2)^k*binomial(n, k)*b(n-k), where b() = A005923."
			],
			"maple": [
				"a:= proc(n) option remember; `if`(n=0, 1, add(",
				"      a(n-j)*binomial(n, j)*(2^j-1), j=1..n))",
				"    end:",
				"seq(a(n), n=0..20);  # _Alois P. Heinz_, Oct 05 2019"
			],
			"mathematica": [
				"CoefficientList[Series[E^(-x)/(1-E^x+E^(-x)), {x, 0, 20}], x] * Range[0, 20]! (* _Vaclav Kotesovec_, May 04 2015 *)",
				"Round@Table[(-1)^(n+1) (PolyLog[-n, 1-GoldenRatio] GoldenRatio + PolyLog[-n, GoldenRatio]/GoldenRatio)/Sqrt[5], {n, 0, 20}] (* _Vladimir Reshetnikov_, Oct 30 2015 *)"
			],
			"program": [
				"(PARI) a(n) = sum(k=0, n, k!*fibonacci(k+1)*stirling(n, k, 2)); \\\\ _Michel Marcus_, Oct 30 2015"
			],
			"xref": [
				"Cf. A005923, A216794."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"references": 10,
			"revision": 65,
			"time": "2021-07-26T08:33:22-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}