{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A128581",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 128581,
			"data": "1,1,-1,-1,-2,-1,2,1,1,-2,-2,1,0,2,2,-1,0,1,0,2,-2,-2,0,-1,3,0,-1,-2,-2,2,2,1,2,0,-4,-1,0,0,0,-2,0,-2,0,2,-2,0,0,1,3,3,0,0,-2,-1,4,2,0,-2,-2,-2,0,2,2,-1,0,2,0,0,0,-4,0,1,2,0,-3,0,-4,0",
			"name": "Expansion of (phi(q^2) * phi(-q^3) - phi(-q) * phi(q^6)) / 2 in powers of q where phi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A128581/b128581.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of eta(q^2)^3 * eta(q^3) * eta(q^12) * eta(q^24) / (eta(q) * eta(q^6)^2 * eta(q^8)) in powers of q.",
				"Euler transform of period 24 sequence [ 1, -2, 0, -2, 1, -1, 1, -1, 0, -2, 1, -2, 1, -2, 0, -1, 1, -1, 1, -2, 0, -2, 1, -2, ...].",
				"Multiplicative with a(2^e) = -(-1)^e if e\u003e0, a(3^e) = (-1)^e, a(p^e) = e+1 if p == 1, 7 (mod 24), a(p^e) = (e+1)(-1)^e if p == 5, 11 (mod 24), a(p^e) = (1 + (-1)^e)/2 if p == 13, 17, 19, 23 (mod 24).",
				"a(n) = -(-1)^n * A115660(n). a(2*n) = A115660(n). a(2*n + 1) = A128580(n).",
				"abs(a(n)) = A000377(n) = A192013(n). - _M. F. Hasler_, May 07 2018"
			],
			"example": [
				"G.f. = q + q^2 - q^3 - q^4 - 2*q^5 - q^6 + 2*q^7 + q^8 + q^9 - 2*q^10 - ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 1, 0, -(-1)^n DivisorSum[ n, KroneckerSymbol[ #, 8] KroneckerSymbol[ n/#, 3] \u0026]]; (* _Michael Somos_, Nov 15 2015 *)",
				"a[ n_] := SeriesCoefficient[ (EllipticTheta[ 3, 0, q^2] EllipticTheta[ 4, 0, q^3] - EllipticTheta[ 4, 0, q] EllipticTheta[ 3, 0, q^6])/2, {q, 0, n}]; (* _Michael Somos_, Nov 15 2015 *)",
				"a[ n_] := SeriesCoefficient[ q QPochhammer[ -q] QPochhammer[ q^24] QPochhammer[ q^4, q^8] QPochhammer[ q^3, -q^3], {q, 0, n}]; (* _Michael Somos_, Nov 15 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = -(-1)^n * sumdiv(n, d, kronecker(d, 8) * kronecker(n/d, 3))}",
				"(PARI) {a(n) = my(A =  x * O(x^n)); polcoeff( eta(x^2 + A)^3 * eta(x^3 + A) * eta(x^12 + A) * eta(x^24 + A) / (eta(x + A) * eta(x^6 + A)^2 * eta(x^8 + A)), n)}",
				"(PARI) A128581(n)={prod(i=1,matsize(n=factor(n))[1], if(12\u003en[i,1]%24, if(bittest(12,n[i,1]),(-1)^(n[i,1]+n[i,2]-1), if(bittest(n[i,2],0)\u0026\u0026n[i,1]%6\u003e1,-n[i,2]-1,n[i,2]+1)), !bittest(n[i,2],0)))} \\\\ a(p^e) as given in formula. - _M. F. Hasler_, May 07 2018"
			],
			"xref": [
				"Cf. A115660, A128580."
			],
			"keyword": "sign,mult",
			"offset": "1,5",
			"author": "_Michael Somos_, Mar 11 2007",
			"references": 9,
			"revision": 20,
			"time": "2021-03-12T22:24:44-05:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}