{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006632",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6632,
			"id": "M2997",
			"data": "1,3,15,91,612,4389,32890,254475,2017356,16301164,133767543,1111731933,9338434700,79155435870,676196049060,5815796869995,50318860986108,437662920058980,3824609516638444,33563127932394060,295655735395397520,2613391671568320765",
			"name": "a(n) = 3*binomial(4*n-1,n-1)/(4*n-1).",
			"comment": [
				"a(n) is the number of ordered trees (A000108) with 3n-1 edges in which every non-leaf vertex has exactly two leaf children (no restriction on non-leaf children). For example, a(2) counts the 3 trees",
				"  \\/......\\/......\\/",
				"  .\\|/...\\|/....\\|/  . - _David Callan_, Aug 22 2014",
				"a(n) is the number of lattice paths from (0,0) to (3n,n) using only the steps (1,0) and (0,1) and which are strictly below the line y = x/3 except at the path's endpoints. - _Lucas A. Brown_, Aug 21 2020"
			],
			"reference": [
				"H. M. Finucan, Some decompositions of generalized Catalan numbers, pp. 275-293 of Combinatorial Mathematics IX. Proc. Ninth Australian Conference (Brisbane, August 1981). Ed. E. J. Billington, S. Oates-Williams and A. P. Street. Lecture Notes Math., 952. Springer-Verlag, 1982.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"O. Aichholzer, A. Asinowski and T. Miltzow, \u003ca href=\"http://arxiv.org/abs/1403.5546\"\u003eDisjoint compatibility graph of non-crossing matchings of points in convex position\u003c/a\u003e, arXiv preprint arXiv:1403.5546 [math.CO], 2014.",
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/2001.08799\"\u003eCharacterizations of the Borel triangle and Borel polynomials\u003c/a\u003e, arXiv:2001.08799 [math.CO], 2020.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=438\"\u003eEncyclopedia of Combinatorial Structures 438\u003c/a\u003e",
				"Elżbieta Liszewska and Wojciech Młotkowski, \u003ca href=\"https://arxiv.org/abs/1907.10725\"\u003eSome relatives of the Catalan sequence\u003c/a\u003e, arXiv:1907.10725 [math.CO], 2019."
			],
			"formula": [
				"a(n) = binomial(4*n-1, n)/(4*n-1) = 3*binomial(4*n-2, n-1) - binomial(4*n-2, n). - _David Callan_, Sep 15 2004",
				"G.f.: g^3 where g = 1+x*g^4 is the g.f. of A002293. - _Mark van Hoeij_, Nov 11 2011",
				"a(n) = (3/4)*binomial(4*n,n)/(4*n-1). - _Bruno Berselli_, Jan 17 2014",
				"From _Wolfdieter Lang_, Feb 06 2020: (Start)",
				"G.f.: (3/4)*(1 - hypergeom([-1, 1, 2]/4, [1, 2]/3, (4^4/3^3)*x)).",
				"E.g.f.: (3/4)*(1 - hypergeom([-1, 1, 2]/4, [1, 2, 3]/3, (4^4/3^3)*x)). (End)",
				"D-finite with recurrence 3*n*(3*n-1)*(3*n-2)*a(n) -8*(4*n-5)*(4*n-3)*(2*n-1)*a(n-1)=0. - _R. J. Mathar_, May 07 2021",
				"a(n) = (2n-1)*A000260(n). - _F. Chapoton_, Jul 15 2021",
				"G.f. A(x) satisfies: A(x) = x / (1 - A(x))^3. - _Ilya Gutkovskiy_, Nov 03 2021"
			],
			"maple": [
				"A006632:=n-\u003e3*binomial(4*n-1,n-1)/(4*n-1): seq(A006632(n), n=1..30); # _Wesley Ivan Hurt_, Oct 23 2017"
			],
			"mathematica": [
				"InverseSeries[Series[y*(1-y)^3, {y, 0, 24}], x] (* then A(x)=y(x) *) (* _Len Smiley_, Apr 07 2000 *)",
				"a[ n_] := If[ n \u003c 1, 0, Binomial[4 n - 2, n - 1] / n]; (* _Michael Somos_, Aug 22 2014 *)"
			],
			"program": [
				"(PARI) a(n) = 3*binomial(4*n-1, n-1)/(4*n-1) \\\\ _Felix Fröhlich_, Oct 23 2017"
			],
			"xref": [
				"A112385 divided by 2.",
				"Cf. A000108, A000260, A002293, A006013, A069271, A120588."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Simon Plouffe_",
			"references": 26,
			"revision": 87,
			"time": "2021-11-03T11:38:17-04:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}