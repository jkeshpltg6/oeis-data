{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A238354",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 238354,
			"data": "1,1,0,2,0,0,2,1,0,0,4,0,1,0,0,5,1,0,1,0,0,8,1,1,0,1,0,0,11,2,0,1,0,1,0,0,17,2,1,0,1,0,1,0,0,23,3,1,1,0,1,0,1,0,0,33,4,2,0,1,0,1,0,1,0,0,45,5,2,1,0,1,0,1,0,1,0,0,63,6,3,1,1,0,1,0,1,0,1,0,0,84,8,3,2,0,1,0,1,0,1,0,1,0,0,114,10,4,2,1,0,1,0,1,0,1,0,1,0,0",
			"name": "Triangle T(n,k) read by rows: T(n,k) is the number of partitions of n (as weakly ascending list of parts) with minimal ascent k, n \u003e= 0, 0 \u003c= k \u003c= n.",
			"comment": [
				"Column k=0: T(n,0) = 1 + A047967(n).",
				"Column k=1 is A238708.",
				"Row sums are A000041."
			],
			"link": [
				"Joerg Arndt and Alois P. Heinz, \u003ca href=\"/A238354/b238354.txt\"\u003eRows n = 0..140, flattened\u003c/a\u003e"
			],
			"example": [
				"Triangle starts:",
				"00:    1;",
				"01:    1,  0;",
				"02:    2,  0, 0;",
				"03:    2,  1, 0, 0;",
				"04:    4,  0, 1, 0, 0;",
				"05:    5,  1, 0, 1, 0, 0;",
				"06:    8,  1, 1, 0, 1, 0, 0;",
				"07:   11,  2, 0, 1, 0, 1, 0, 0;",
				"08:   17,  2, 1, 0, 1, 0, 1, 0, 0;",
				"09:   23,  3, 1, 1, 0, 1, 0, 1, 0, 0;",
				"10:   33,  4, 2, 0, 1, 0, 1, 0, 1, 0, 0;",
				"11:   45,  5, 2, 1, 0, 1, 0, 1, 0, 1, 0, 0;",
				"12:   63,  6, 3, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0;",
				"13:   84,  8, 3, 2, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0;",
				"14:  114, 10, 4, 2, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0;",
				"15:  150, 13, 4, 3, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0;",
				"...",
				"The 11 partitions of 6 together with their minimal ascents are:",
				"01:  [ 1 1 1 1 1 1 ]   0",
				"02:  [ 1 1 1 1 2 ]     0",
				"03:  [ 1 1 1 3 ]       0",
				"04:  [ 1 1 2 2 ]       0",
				"05:  [ 1 1 4 ]         0",
				"06:  [ 1 2 3 ]         1",
				"07:  [ 1 5 ]           4",
				"08:  [ 2 2 2 ]         0",
				"09:  [ 2 4 ]           2",
				"10:  [ 3 3 ]           0",
				"11:  [ 6 ]             0",
				"There are 8 partitions of with min ascent 0, 1 with min ascents 1, 2, and 4, giving row 6 of the triangle: 8, 1, 1, 0, 1, 0, 0."
			],
			"maple": [
				"b:= proc(n, i, t) option remember; `if`(n=0, 1/x, `if`(i\u003c1, 0,",
				"      b(n, i-1, t)+`if`(i\u003en, 0, (p-\u003e`if`(t=0, p, add(coeff(",
				"       p, x, j)*x^`if`(j\u003c0, t-i, min(j, t-i)),",
				"       j=-1..degree(p))))(b(n-i, i, i)))))",
				"    end:",
				"T:= n-\u003e(p-\u003eseq(coeff(p, x, k)+`if`(k=0, 1, 0), k=0..n))(b(n$2, 0)):",
				"seq(T(n), n=0..15);"
			],
			"mathematica": [
				"b[n_, i_, t_] := b[n, i, t] = If[n == 0, 1/x, If[i\u003c1, 0, b[n, i-1, t]+If[i\u003en, 0, Function[{p}, If[t == 0, p, Sum[Coefficient[p, x, j]*x^If[j\u003c0, t-i, Min[j, t-i]], {j, -1, Exponent[p, x]}]]][b[n-i, i, i]]]]]; T[n_] := Function[{p}, Table[ Coefficient[p, x, k]+If[k == 0, 1, 0], {k, 0, n}]][b[n, n, 0]]; Table[T[n], {n, 0, 15}] // Flatten (* _Jean-François Alcover_, Jan 12 2015, translated from Maple *)"
			],
			"xref": [
				"Cf. A238353 (partitions by maximal ascent)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Joerg Arndt_ and _Alois P. Heinz_, Feb 26 2014",
			"references": 3,
			"revision": 26,
			"time": "2015-01-12T04:12:55-05:00",
			"created": "2014-03-02T12:38:53-05:00"
		}
	]
}