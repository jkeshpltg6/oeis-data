{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A306024",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 306024,
			"data": "1,1,0,1,1,0,1,2,2,0,1,3,7,5,0,1,4,15,31,15,0,1,5,26,95,164,52,0,1,6,40,214,717,999,203,0,1,7,57,405,2096,6221,6841,877,0,1,8,77,685,4875,23578,60619,51790,4140,0,1,9,100,1071,9780,67354,297692,652595,428131,21147,0",
			"name": "Number A(n,k) of length-n restricted growth strings (RGS) with growth \u003c= k and first element in [k]; square array A(n,k), n\u003e=0, k\u003e=0, read by antidiagonals.",
			"comment": [
				"A(n,k) counts strings [s_1, ..., s_n] with 1 \u003c= s_i \u003c= k + max(0, max_{j\u003ci} s_j)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A306024/b306024.txt\"\u003eAntidiagonals n = 0..150, flattened\u003c/a\u003e"
			],
			"formula": [
				"E.g.f. of column k: exp(Sum_{j=1..k} (exp(j*x)-1)/j)."
			],
			"example": [
				"A(2,3) = 15: 11, 12, 13, 14, 21, 22, 23, 24, 25, 31, 32, 33, 34, 35, 36.",
				"A(4,1) = 15: 1111, 1112, 1121, 1122, 1123, 1211, 1212, 1213, 1221, 1222, 1223, 1231, 1232, 1233, 1234.",
				"Square array A(n,k) begins:",
				"  1,   1,    1,     1,      1,       1,       1,       1, ...",
				"  0,   1,    2,     3,      4,       5,       6,       7, ...",
				"  0,   2,    7,    15,     26,      40,      57,      77, ...",
				"  0,   5,   31,    95,    214,     405,     685,    1071, ...",
				"  0,  15,  164,   717,   2096,    4875,    9780,   17689, ...",
				"  0,  52,  999,  6221,  23578,   67354,  160201,  335083, ...",
				"  0, 203, 6841, 60619, 297692, 1044045, 2943277, 7117789, ..."
			],
			"maple": [
				"b:= proc(n, k, m) option remember; `if`(n=0, 1,",
				"      add(b(n-1, k, max(m, j)), j=1..m+k))",
				"    end:",
				"A:= (n, k)-\u003e b(n, k, 0):",
				"seq(seq(A(n, d-n), n=0..d), d=0..12);",
				"# second Maple program:",
				"A:= (n, k)-\u003e n!*coeff(series(exp(add(",
				"    (exp(j*x)-1)/j, j=1..k)), x, n+1), x, n):",
				"seq(seq(A(n, d-n), n=0..d), d=0..12);"
			],
			"mathematica": [
				"b[n_, k_, m_] := b[n, k, m] = If[n==0, 1, Sum[b[n-1, k, Max[m, j]], {j, 1, m+k}]];",
				"A[n_, k_] := b[n, k, 0];",
				"Table[A[n, d-n], {d, 0, 12}, {n, 0, d}] // Flatten (* _Jean-François Alcover_, May 27 2019, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Columns k=0-10 give: A000007, A000110, A002872, A306027, A306028, A306029, A306030, A306031, A306032, A306033, A306034.",
				"Main diagonal gives A306025.",
				"Antidiagonal sums give A306026.",
				"Cf. A305962."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Alois P. Heinz_, Jun 17 2018",
			"references": 13,
			"revision": 17,
			"time": "2019-05-27T09:08:24-04:00",
			"created": "2018-06-17T12:53:37-04:00"
		}
	]
}