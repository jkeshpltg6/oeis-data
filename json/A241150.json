{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A241150",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 241150,
			"data": "2,2,1,3,1,1,2,3,2,4,2,4,1,2,6,5,1,1,4,5,8,3,2,3,8,10,4,5,4,10,13,5,9,1,2,13,17,8,14,1,1,6,12,22,10,22,3,2,2,19,27,11,32,5,5,4,21,33,15,43,9,10,4,20,44,21,57,10,19,1,5,28,50,20,77,20",
			"name": "Irregular triangle read by rows T(n,k) = number of partitions of degree k in the partition graph G(n), for n \u003e= 2; G(n) is defined in Comments.",
			"comment": [
				"The partition graph G(n) of n has the partitions of n as nodes, and nodes p and q have an edge if one of them can be obtained from the other by a substitution x -\u003e x-1,1 for some part x.  G(n) is nonplanar for n \u003e= 8. Column 1: divisors of n, A000005(n), for n \u003e= 2. A000041(n) = sum of numbers in row n, for n \u003e= 2 (counting the top row as row 2). Number of numbers in row n (i.e., maximal degree in G(n)): A241151(n), n \u003e= 2. Last term in row n (the number of partitions having maximal degree): A241153(n), n \u003e= 2. Maximal number in row n: A241152(n), n \u003e= 2. Let u(n,k) be the array at A029205 (where n \u003e= 0, k=0..n). Then u(n,k) is the number of edges in G(n+2) between partitions of n+2 that having length k+1 and those having length k+2."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A241150/b241150.txt\"\u003eTable of n, a(n) for n = 1..500\u003c/a\u003e"
			],
			"example": [
				"The first 12 rows:",
				"2",
				"2 ... 1",
				"3 ... 1 ... 1",
				"2 ... 3 ... 2",
				"4 ... 2 ... 4 ... 1",
				"2 ... 6 ... 5 ... 1 ... 1",
				"4 ... 5 ... 8 ... 3 ... 2",
				"3 ... 8 ... 10 .. 4 ... 5",
				"4 ... 10 .. 13 .. 5 ... 9 ... 1",
				"2 ... 13 .. 17 .. 8 ... 14 .. 1 ... 1",
				"6 ... 12 .. 22 .. 10 .. 22 .. 3 ... 2",
				"2 ... 19 .. 27 .. 11 .. 32 .. 5 ... 5",
				"The graph can is represented by these transformations:",
				"6 -\u003e 51, 51 -\u003e 411, 42 -\u003e 321, 42 -\u003e 411, 411 -\u003e 3111, 33 -\u003e 321, 321 -\u003e 2211, 321 -\u003e 3111, 3111 -\u003e 21111, 222 -\u003e 2211, 2211 -\u003e 21111, 21111 -\u003e 111111.  These 4 partitions p have degree 1 (i.e., number of arrows to or from p): 6, 33, 222, 111111; these 2 have degree 2: 51, 42; these 4 have degree 3: 411, 3111, 2211, 21111; the remaining partition, 321, has degree 4. So, row 6 of the array is 4 2 4 1."
			],
			"mathematica": [
				"z = 25; spawn[part_] := Map[Reverse[Sort[Flatten[ReplacePart[part, {# - 1, 1}, Position[part, #, 1, 1][[1]][[1]]]]]] \u0026, DeleteCases[DeleteDuplicates[part], 1]];",
				"    unspawn[part_] := If[Length[Cases[part, 1]] \u003e 0, Map[ReplacePart[Most[part], Position[Most[part], #, 1, 1][[1]][[1]] -\u003e # + 1] \u0026, DeleteDuplicates[Most[part]]], {}]; m = Map[Last[Transpose[Tally[Map[#[[2]] \u0026, Tally[Flatten[{Map[unspawn, #], Map[spawn, #]}, 2] \u0026[IntegerPartitions[#]]]]]]] \u0026, 1 + Range[z]];",
				"    Column[m]  (* A241150 as an array *)",
				"    Flatten[m] (* A241150 as a sequence *)",
				"    Table[Length[m[[n]]], {n, 1, z}] (* A241151 *)",
				"    Table[Max[m[[n]]], {n, 1, z}]    (* A241152 *)",
				"    Table[Last[m[[n]]], {n, 1, z}]   (* A241153 *)",
				"    (* Next, show the graph G(k) *)",
				"    k = 8; graph = Flatten[Table[part = IntegerPartitions[k][[n]]; Map[FromDigits[part] -\u003e FromDigits[#] \u0026, spawn[part]], {n, 1, PartitionsP[k]}]]; Graph[graph, VertexLabels -\u003e \"Name\", ImageSize -\u003e 500, ImagePadding -\u003e 20] (* _Peter J. C. Moses_, Apr 15 2014 *)"
			],
			"xref": [
				"Cf. A241151, A241152, A241153, A029205, A000041."
			],
			"keyword": "nonn,easy,tabf",
			"offset": "1,1",
			"author": "_Clark Kimberling_ and _Peter J. C. Moses_, Apr 17 2014",
			"references": 6,
			"revision": 10,
			"time": "2014-04-22T22:18:52-04:00",
			"created": "2014-04-22T22:18:52-04:00"
		}
	]
}