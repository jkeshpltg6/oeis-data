{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A258666",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 258666,
			"data": "0,0,0,0,4,20,117,791,6204,55004,543595,5922925,70518884,910711076,12678337153,189252394275,3015217877068,51067618521276,916176420499159,17355904074255065,346195849623668420,7252654428822549364,159210363264445218829,3654550887654460566191",
			"name": "A total of n married couples, including a mathematician M and his wife, are to be seated at the 2n chairs around a circular table, with no man seated next to his wife. After the ladies are seated at every other chair, M is the first man allowed to choose one of the remaining chairs. The sequence gives the number of ways of seating the other men, with no man seated next to his wife, if M chooses the chair that is 7 seats clockwise from his wife's chair.",
			"comment": [
				"This is a variation of the classic ménage problem (cf. A000179).",
				"It is known [Riordan, ch. 8, ex. 7(b)] that, after the ladies are seated at every other chair, the number U_n of ways of seating the men in the ménage problem has asymptotic expansion U_n ~ e^(-2)*n!*(1 + Sum_{k\u003e=1}(-1)^k/(k!(n-1)_k)), where (n)_k = n*(n-1)*...*(n-k+1).",
				"Therefore, it is natural to conjecture that a(n) ~ e^(-2)*n!/(n-2)*(1 + sum{k\u003e=1}(-1)^k/(k!(n-1)_k))."
			],
			"reference": [
				"I. Kaplansky and J. Riordan, The problème des ménages, Scripta Math. 12, (1946), 113-124.",
				"J. Riordan, An Introduction to Combinatorial Analysis, Wiley, 1958, chs. 7, 8."
			],
			"link": [
				"Peter J. C. Moses, \u003ca href=\"/A258666/a258666_1.pdf\"\u003eSeatings for 5 couples\u003c/a\u003e",
				"I. Kaplansky and J. Riordan, \u003ca href=\"/A000166/a000166_1.pdf\"\u003eThe problème des ménages\u003c/a\u003e, Scripta Math. 12, (1946), 113-124. [Scan of annotated copy]",
				"E. Lucas, \u003ca href=\"https://archive.org/details/thoriedesnombre00lucagoog/page/n495\"\u003eSur le problème des ménages\u003c/a\u003e, Théorie des nombres, Paris, 1891, 491-496.",
				"Vladimir Shevelev, Peter J. C. Moses, \u003ca href=\"http://arxiv.org/abs/1101.5321\"\u003eThe ménage problem with a known mathematician\u003c/a\u003e, arXiv:1101.5321 [math.CO], 2011-2015.",
				"Vladimir Shevelev and Peter J. C. Moses, \u003ca href=\"http://www.emis.de/journals/INTEGERS/papers/q72/q72.Abstract.html\"\u003eAlice and Bob go to dinner: A variation on menage\u003c/a\u003e, INTEGERS, Vol. 16(2016), #A72.",
				"J. Touchard, \u003ca href=\"http://gallica.bnf.fr/ark:/12148/bpt6k31506/f631.image\"\u003eSur un problème de permutations\u003c/a\u003e, C.R. Acad. Sci. Paris, 198 (1934), 631-633."
			],
			"formula": [
				"a(n)=0, n\u003c=4; for n\u003e=5, a(n) = Sum_{k=0..n-1} (-1)^k*(n-k-1)! Sum_{j=max(k-n+4, 0)..min(k,3)} binomial(6-j, j)*binomial(2*n-k+j-8, k-j)."
			],
			"mathematica": [
				"a[n_] := If[n\u003c5, 0, Sum[(-1)^k (n-k-1)! Sum[Binomial[6-j, j] Binomial[2n-k+j-8, k-j], {j, Max[k-n+4, 0], Min[k, 3]}], {k, 0, n-1}]];",
				"Array[a, 24] (* _Jean-François Alcover_, Sep 19 2018 *)"
			],
			"program": [
				"(PARI) vector(30, n, if (n\u003c=4, 0, sum(k=0,n-1,(-1)^k*(n-k-1)!*sum(j=max(k-n+4, 0),min(k,3), binomial(6-j, j)*binomial(2*n-k+j-8, k-j))))) \\\\ _Michel Marcus_, Jun 17 2015"
			],
			"xref": [
				"Cf. A000179, A258664, A258665, A258667, A258673, A259212."
			],
			"keyword": "nonn",
			"offset": "1,5",
			"author": "_Vladimir Shevelev_ and _Peter J. C. Moses_, Jun 07 2015",
			"references": 9,
			"revision": 68,
			"time": "2020-07-20T16:21:17-04:00",
			"created": "2015-07-01T02:23:17-04:00"
		}
	]
}