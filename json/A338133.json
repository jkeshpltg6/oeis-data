{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A338133",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 338133,
			"data": "6,20,28,70,945,1575,2205,88,550,3465,5775,7425,8085,12705,104,572,650,1430,2002,4095,6435,6825,9555,15015,78975,81081,131625,189189,297297,342225,351351,570375,63126063,99198099,117234117,272,748,1870,2210,5355,8415,8925,11492",
			"name": "Primitive nondeficient numbers sorted by largest prime factor then by increasing size. Irregular triangle T(n, k), n \u003e= 2, k \u003e= 1, read by rows, row n listing those with largest prime factor = prime(n).",
			"comment": [
				"For definitions and further references/links, see A006039, the main entry for primitive nondeficient numbers.",
				"Rows are finite: row n is a subset of the divisors of any of the products formed by multiplying 2^(A035100(n)-1) by a member of the first n finite sets described in the Dickson reference.",
				"Column 1 includes the even perfect numbers.",
				"The largest number in rows 2..n (therefore the largest that is prime(n)-smooth) is A338427(n). - _Peter Munn_, Sep 07 2021"
			],
			"link": [
				"L. E. Dickson, \u003ca href=\"http://www.jstor.org/stable/2370405\"\u003eFiniteness of the Odd Perfect and Primitive Abundant Numbers with n Distinct Prime Factors\u003c/a\u003e, Amer. J. Math., 35 (1913), 413-426."
			],
			"formula": [
				"A006530(T(n, k)) = A000040(n).",
				"T(n, 1) = A308710(n-1) [provided there is no least deficient number that is not a power of 2, as described in A000079].",
				"For m \u003e= 1, T(A059305(m), 1) = A000668(m) * 2^(A000043(m)-1) = A000668(m) * A061652(m)."
			],
			"example": [
				"Row 1 is empty as there exists no primitive nondeficient number of the form prime(1)^k = 2^k.",
				"Row 2 is (6) as 6 is the only primitive nondeficient number of the form prime(1)^k * prime(2)^m = 2^k * 3^m that is a multiple of prime(2) = 3.",
				"Irregular triangle T(n, k) begins:",
				"  n   prime(n)  row n",
				"  2      3      6;",
				"  3      5      20;",
				"  4      7      28, 70, 945, 1575, 2205;",
				"  5     11      88, 550, 3465, 5775, 7425, 8085, 12705;",
				"  ...",
				"See also the factorization of initial terms below:",
				"      6 = 2 * 3,",
				"     20 = 2^2 * 5,",
				"     28 = 2^2 * 7,",
				"     70 = 2 * 5 * 7,",
				"    945 = 3^3 * 5 * 7,",
				"   1575 = 3^2 * 5^2 * 7,",
				"   2205 = 3^2 * 5 * 7^2,",
				"     88 = 2^3 * 11,",
				"    550 = 2 * 5^2 * 11,",
				"   3465 = 3^2 * 5 * 7 * 11,",
				"   5775 = 3 * 5^2 * 7 * 11,",
				"   7425 = 3^3 * 5^2 * 11,",
				"   8085 = 3 * 5 * 7^2 * 11,",
				"  12705 = 3 * 5 * 7 * 11^2,",
				"    104 = 2^3 * 13,",
				"    572 = 2^2 * 11 * 13,",
				"    650 = 2 * 5^2 * 13,",
				"   1430 = 2 * 5 * 11 * 13,",
				"   2002 = 2 * 7 * 11 * 13,",
				"   4095 = 3^2 * 5 * 7 * 13,",
				"  ..."
			],
			"program": [
				"(PARI) rownupto(n, u) = { my(res = List(), pr = primes(n), e = vector(n, i, logint(u, pr[i]))); vu = vector(n, i, [0, e[i]]); vu[n][1] = 1; forvec(x = vu, c = prod(i = 1, n, pr[i]^x[i]); if(c \u003c= u \u0026\u0026 isprimitive(c), listput(res, c) ) ); Set(res) }",
				"isprimitive(n) = { my(f = factor(n), c); if(sigma(f) \u003c 2*n, return(0)); for(i = 1, #f~, c = n / f[i,1]; if(sigma(c) \u003e= c * 2, return(0) ) ); 1 }",
				"for(i = 2, 7, print(rownupto(i, 10^9)))"
			],
			"xref": [
				"A000040, A006530 are used to define this sequence.",
				"Permutation of A006039.",
				"A047802\\{12}, A308710 are subsequences.",
				"Cf. A000043, A000079, A000668, A035100, A059305, A061652, A338427."
			],
			"keyword": "nonn,tabf",
			"offset": "2,1",
			"author": "_David A. Corneth_ and _Peter Munn_, Oct 11 2020",
			"references": 2,
			"revision": 30,
			"time": "2021-10-06T19:42:02-04:00",
			"created": "2020-11-07T11:37:32-05:00"
		}
	]
}