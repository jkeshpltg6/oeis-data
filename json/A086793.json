{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A086793",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 86793,
			"data": "0,5,4,3,9,8,2,1,11,12,5,7,10,1,0,13,12,15,6,1,2,12,9,9,11,1,13,9,8,14,10,14,8,16,3,17,6,10,2,14,9,9,2,3,9,16,8,3,3,3,16,2,12,4,16,4,2,14,1,10,2,1,15,7,3,18,2,18,10,18,12,11,6,10,17,10,10,17,13,10,11,16,8,2,14,10,15",
			"name": "Number of iterations of the map A034690 (x -\u003e sum of digits of all divisors of x) required to reach one of the fixed points, 15 or 1.",
			"comment": [
				"Ecker states that every number (larger than 1) eventually reaches 15. \"Take any natural number larger than 1 and write down its divisors, including 1 and the number itself. Now take the sum of the digits of these divisors. Iterate until a number repeats. The black-hole number this time is 15.\" [Ecker]",
				"The only other fixed point of A034690, namely 1, cannot be reached by any other starting value than 1 itself. - _M. F. Hasler_, Nov 08 2015"
			],
			"reference": [
				"Michael W. Ecker, Number play, calculators and card tricks ..., pp. 41-51 of The Mathemagician and the Pied Puzzler, Peters, Boston. [Suggested by a problem in this article.]"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A086793/b086793.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (corrected by _Georg Fischer_, Jan 20 2019)",
				"Eric Angelini et al., \u003ca href=\"http://list.seqfan.eu/pipermail/seqfan/2015-November/015585.html\"\u003eList the dividers, sum the digits\u003c/a\u003e, SeqFan list, Nov. 2015",
				"Michael W. Ecker, \u003ca href=\"http://web.archive.org/web/20080515203911/http://members.aol.com/DrMWEcker/mathhole.html\"\u003eDivisive Number 15\u003c/a\u003e. (Web archive, as of May 2008)"
			],
			"example": [
				"35 requires 3 iterations to reach 15 because 35 -\u003e 1+5+7+3+5 = 21 -\u003e 1+3+7+2+1 = 14 -\u003e 1+2+7+1+4 = 15."
			],
			"maple": [
				"with(numtheory); read transforms; f:=proc(n) local t1,t2,i; t1:=divisors(n); t2:=0; for i from 1 to nops(t1) do t2:=t2+digsum(t1[i]); od: t2; end;",
				"g:=proc(n) global f; local t2,i; t2:=n; for i from 1 to 100 do if t2 = 15 then return(i-1); fi; t2:=f(t2); od; end; # _N. J. A. Sloane_"
			],
			"mathematica": [
				"f[n_] := (i++; Plus @@ Flatten@IntegerDigits@Divisors@n); Table[i = 0; NestWhile[f, n, # != 15 \u0026]; i, {n, 2, 87}] (* _Robert G. Wilson v_, May 16 2006 *)"
			],
			"program": [
				"(Haskell)",
				"a086793 = f 0 where",
				"   f y x = if x == 15 then y else f (y + 1) (a034690 x)",
				"-- _Reinhard Zumkeller_, May 20 2015",
				"(PARI) A086793(n)=n\u003e1\u0026\u0026for(k=0, 9e9, n==15\u0026\u0026return(k); n=A034690(n)) \\\\ _M. F. Hasler_, Nov 08 2015"
			],
			"xref": [
				"Cf. A034690, A114527. For records see A095347, A118358."
			],
			"keyword": "base,easy,nonn",
			"offset": "1,2",
			"author": "_Jason Earls_, Aug 04 2003; revised Jun 03 2004",
			"ext": [
				"Corrected by _N. J. A. Sloane_, May 17 2006 (a(15) changed to 0)",
				"Corrected by _David Applegate_, Jan 23 2007 (reference book title corrected)",
				"Extended to a(1)=0 by _M. F. Hasler_, Nov 08 2015"
			],
			"references": 14,
			"revision": 41,
			"time": "2019-01-20T03:32:34-05:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}