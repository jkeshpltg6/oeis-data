{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A121363",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 121363,
			"data": "1,-2,0,2,-2,0,3,-2,0,2,-2,0,1,-2,0,2,-4,0,2,0,0,4,-2,0,2,-2,0,2,-2,0,1,-4,0,0,-2,0,4,-2,0,2,0,0,3,-2,0,2,-4,0,2,-2,0,4,0,0,0,-4,0,2,-2,0,2,-2,0,0,-2,0,4,-2,0,2,-2,0,3,-2,0,0,-4,0,2,-2,0,6,0,0,2,0,0,2,-2,0,1,-4,0,2,-4,0,0,-2,0,2,-2,0,2,0,0",
			"name": "Expansion of q^(-1/4)(eta(q)*eta(q^6)*eta(q^9)/eta(q^3))^2/(eta(q^2)eta(q^18)) in powers of q.",
			"comment": [
				"Ramanujan theta functions: f(q) := Prod_{k\u003e=1} (1-(-q)^k) (see A121373), phi(q) := theta_3(q) := Sum_{k=-oo..oo} q^(k^2) (A000122), psi(q) := Sum_{k=0..oo} q^(k*(k+1)/2) (A010054), chi(q) := Prod_{k\u003e=0} (1+q^(2k+1)) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A121363/b121363.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Euler transform of period 18 sequence [ -2, -1, 0, -1, -2, -1, -2, -1, -2, -1, -2, -1, -2, -1, 0, -1, -2, -2, ...].",
				"a(n) = b(4n+1) where b(n) is multiplicative and b(2^e) = b(3^e) = 0^e, b(p^e) = (1+(-1)^e)/2 if p == 3 (mod 4), b(p^e) = e+1 if p == 1 (mod 12), b(p^e) = (e+1)(-1)^e if p == 5 (mod 12).",
				"G.f.: Product_{k\u003e0} (1+x^(3n))^2(1-x^n)(1-x^(9n))/((1+x^n)(1+x^9n)).",
				"a(3n+2) = 0.",
				"Expansion of phi(-q)*phi(-q^9)/chi(-q^3)^2 in powers of q where phi(),chi() are Ramanujan theta functions."
			],
			"mathematica": [
				"QP = QPochhammer; s = (QP[q]*QP[q^6]*(QP[q^9]/QP[q^3]))^2/QP[q^2]/QP[q^18]+ O[q]^105; CoefficientList[s, q] (* _Jean-François Alcover_, Nov 30 2015, adapted from PARI *)"
			],
			"program": [
				"(PARI) {a(n)=if(n\u003c0, 0, n=4*n+1; dirmul(vector(n, k, kronecker(12, k)), vector(n, k, kronecker(-12, k)))[n])}",
				"(PARI) {a(n)=local(A, p, e); if(n\u003c0, 0, n=4*n+1; A=factor(n); prod(k=1, matsize(A)[1], if(p=A[k, 1], e=A[k, 2]; if(p\u003c5, 0, if(p%4==3, (1+(-1)^e)/2, (e+1)*if(p%3==2, (-1)^e, 1)))))) }",
				"(PARI) {a(n)=local(A); if(n\u003c0, 0, A=x*O(x^n); polcoeff( (eta(x+A)*eta(x^6+A)*eta(x^9+A)/ eta(x^3+A))^2/ eta(x^2+A)/ eta(x^18+A), n))}"
			],
			"xref": [
				"A002175(n) = a(3n). A121444(n) = -a(3n+1)/2."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Jul 22 2006",
			"references": 5,
			"revision": 16,
			"time": "2021-03-12T22:24:44-05:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}