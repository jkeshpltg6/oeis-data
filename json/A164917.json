{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A164917",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 164917,
			"data": "0,1,2,3,0,4,0,1,5,0,1,2,0,6,0,1,0,2,0,0,3,1,7,1,0,0,2,0,0,1,0,1,0,1,0,0,2,8,0,2,0,0,0,1,0,1,1,2,0,0,0,0,0,1,2,0,0,1,2,0,0,1,0,0,3,9,1,3,0,0,1,1,0,0,1,2,1,2,0,0,0,2,0,0,0,3,1,1,0,1,1,1,0,0,2,0,3,0,1,2,3,1,1,0,0,2",
			"name": "Smallest number of steps to reach prime(n) by applying the map x-\u003eA060308(x) starting from any member of A164368.",
			"comment": [
				"Starting from some prime, iterated application of A060308 (or of the equivalent A059788) generates a chain of increasing prime numbers.",
				"The nature of these chains is to reach higher in the list of primes, sometimes \"over-satisfying\" Bertrand's postulate by skipping some nearer primes, almost doubling of possible. On the other hand, A164368 contains the primes that would be skipped by a chain which contains the prime slightly above half of their value. The sequence shows how far up in chains starting from some member of A164368 we find prime(n), or equivalently, how many inverse applications of the map we need to hit a member of A164368 if starting at prime(n).",
				"Note that by construction A164368(k) starts with the smallest prime that is not member of any chain started from any previous A164368. So each prime exists at some place in one of these chains, and the number of steps a(n) to reach it from the start of its chain is well defined."
			],
			"link": [
				"V. Shevelev, \u003ca href=\"http://arXiv.org/abs/0908.2319\"\u003eOn critical small intervals containing primes\u003c/a\u003e, arXiv:0908.2319 [math.NT], 2009."
			],
			"example": [
				"The first prime chains of the mapping with A060308 initialized with members of A164368 are",
				"2-\u003e3-\u003e5-\u003e7-\u003e13-\u003e23-\u003e43-\u003e83-\u003e163-\u003e317-\u003e631-\u003e1259-\u003e2503-\u003e..",
				"11-\u003e19-\u003e37-\u003e73-\u003e139-\u003e277-\u003e547-\u003e1093-\u003e2179-\u003e4357-\u003e8713-\u003e17419-\u003e..",
				"17-\u003e31-\u003e61-\u003e113-\u003e223-\u003e443-\u003e883-\u003e1759-\u003e3517-\u003e7027-\u003e14051-\u003e28099-\u003e..",
				"29-\u003e53-\u003e103-\u003e199-\u003e397-\u003e787-\u003e1571-\u003e3137-\u003e6271-\u003e12541-\u003e25073-\u003e..",
				"41-\u003e79-\u003e157-\u003e313-\u003e619-\u003e1237-\u003e2473-\u003e4943-\u003e9883-\u003e19763-\u003e39521-\u003e..",
				"47-\u003e89-\u003e173-\u003e337-\u003e673-\u003e1327-\u003e2647-\u003e5281-\u003e10559-\u003e21107-\u003e..",
				"The a(1) to a(4) representing the first 4 primes are all on the first chain, and need 0 to 3 steps to be reached from 2 = A164368(1). a(5) asks for the number of steps for A000040(5)=11 which is on the second chain, and needs 0 steps."
			],
			"maple": [
				"A060308 := proc(n) prevprime(2*n+1) ; end:",
				"isA164368 := proc(p) local q ; q := nextprime(floor(p/2)) ; RETURN(numtheory[pi](2*q) -numtheory[pi](p) \u003e= 1); end:",
				"A164368 := proc(n) option remember; local a; if n = 1 then 2; else a := nextprime( procname(n-1)) ; while not isA164368(a) do a := nextprime(a) ; od: RETURN(a) ; fi; end:",
				"A164917 := proc(n) local p,a,j,q,itr ; p := ithprime(n) ; a := 1000000000000000 ; for j from 1 do q := A164368(j) ; if q \u003e p then break; fi; itr := 0 ; while q \u003c p do q := A060308(q) ; itr := itr+1 ; od; if q = p then if itr \u003c a then a := itr; fi; fi; od: a ; end:",
				"seq(A164917(n),n=1..120) ; # _R. J. Mathar_, Sep 24 2009"
			],
			"mathematica": [
				"A060308[n_] := NextPrime[2*n + 1, -1];",
				"isA164368[p_] := Module[{q}, q = NextPrime[Floor[p/2]]; Return[PrimePi[2*q] - PrimePi[p] \u003e= 1]];",
				"A164368[n_] := A164368[n] = Module[{a}, If[n == 1, 2, a = NextPrime[ A164368[n-1]]; While[Not @ isA164368[a], a = NextPrime[a]]; Return[a]]];",
				"A164917[n_] := Module[{p, a, j, q, itr}, p = Prime[n]; a = 10^15; For[j = 1 , True, j++, q = A164368[j]; If[q \u003e p, Break[]]; itr = 0; While[q \u003c p, q = A060308[q]; itr++]; If[q == p, If[itr \u003c a, a = itr]]]; a];",
				"Table[A164917[n], {n, 1, 120}] (* _Jean-François Alcover_, Dec 14 2017, after _R. J. Mathar_ *)"
			],
			"xref": [
				"Cf. A006992, A104272, A164368, A164288."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Vladimir Shevelev_, Aug 31 2009",
			"ext": [
				"Edited, examples added and extended by _R. J. Mathar_, Sep 24 2009"
			],
			"references": 5,
			"revision": 16,
			"time": "2017-12-14T04:13:31-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}