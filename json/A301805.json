{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A301805",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 301805,
			"data": "0,0,1,1,2,2,3,3,3,2,4,4,4,4,4,4,6,4,4,3,5,4,4,5,7,5,4,4,6,4,7,5,5,7,7,5,5,4,8,5,7,6,11,6,6,5,8,5,6,7,5,7,6,5,5,5,7,7,4,4,8,8,8,6,6,6,9,8,8,7,8,6,10,6,10,6,8,8,8,5",
			"name": "Number of ways to write 3*n^2 as x^2 + 10*y^2 + 2^z, where x, y and z are nonnegative integers with z \u003e 3.",
			"comment": [
				"It might seem that a(n) \u003e 0 for all n \u003e 2. However, we find that a(323525083) = 0, moreover 3*323525083^2 cannot be written as x^2 + 10*y^2 + 2^z with x,y,z nonnegative integers. We note also that a(270035155) = 0 but 3*270035155^2 - 2^0 has the form x^2 + 10*y^2 with x and y integers.",
				"My way to check whether 3*n^2 can be written as x^2 + 10*y^2 + 2^z is to find z such that 3*n^2 - 2^z can be written as x^2 + 10*y^2. I observe that a positive integer n has the form x^2 + 10*y^2 with x and y integers if and only if the p-adic order ord_p(n) of n is even for any prime p == 3, 17, 21, 27, 29, 31, 33, 39 (mod 40) and the sum of those ord_p(n) with p prime and p == 2, 5, 7, 13, 23, 37 (mod 40) is even.",
				"From _David A. Corneth_, Mar 27 2018: (Start)",
				"If a(n) \u003e 0 then a(2*n) \u003e 0; 3*n^2 = x^2 + 10*y^2 + 2^z \u003c=\u003e 3*(2*n)^2 = 4 * 3*n^2 = 4 * (x^2 + 10*y^2 + 2^z) = (2*x)^2 + 10 * (2*y)^2 + 2^(z + 2).",
				"So we just need to check odd n and as z \u003e 0, 2 | 2^z and furthermore 2 | 10 * y^2 so 3*x^2 must be odd, i.e., x must be odd for 3*n^2 to be odd. Also, y must be odd. For odd n, 3*n^2 == 3 (mod 4), for odd x, x^2 == (1 mod 4), for z \u003e= 3, 2^z == 0 (mod 4) so 10 * y^2 must be == 2 (mod 4) which happens if and only if y is odd. (End)"
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A301805/b301805.txt\"\u003eTable of n, a(n) for n = 1..800\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2016.11.008\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, J. Number Theory 175(2017), 167-190.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1701.05868\"\u003eRestricted sums of four squares\u003c/a\u003e, arXiv:1701.05868 [math.NT], 2017-2018."
			],
			"example": [
				"a(1) = a(2) = 0 since 3*1^2 \u003c 3*2^2 \u003c 2^4.",
				"a(3) = 1 since 3*3^2 = 1^2 + 10*1^2 + 2^4.",
				"a(4) = 1 since 3*4^2 = 4^2 + 10*0^2 + 2^5."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]];",
				"tab={};Do[r=0;Do[If[SQ[3*n^2-2^k-10x^2],r=r+1],{k,4,Log[2,3n^2]},{x,0,(3*n^2-2^k)/10}];tab=Append[tab,r],{n,1,80}];Print[tab]"
			],
			"xref": [
				"Cf. A000079, A000290, A020673, A299924, A299537, A299794, A300219, A300362, A300396, A300510, A301376, A301391, A301452, A301471, A301472, A301479, A301579, A301640."
			],
			"keyword": "nonn",
			"offset": "1,5",
			"author": "_Zhi-Wei Sun_, Mar 27 2018",
			"references": 1,
			"revision": 39,
			"time": "2018-04-15T21:53:12-04:00",
			"created": "2018-03-27T09:35:06-04:00"
		}
	]
}