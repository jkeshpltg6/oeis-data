{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A057789",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 57789,
			"data": "0,1,4,6,20,10,56,44,84,60,220,92,364,182,280,344,816,318,1140,520,840,770,2024,760,2100,1300,2196,1540,4060,1240,4960,2736,3520,2992,4760,2580,8436,4218,5928,4240,11480,3612,13244,6380,8040,7590,17296,6128",
			"name": "a(n) = Sum_{k = 1..n, gcd(k,n)=1} k*(n-k).",
			"comment": [
				"Equal to convolution sum over positive integers, k, where k\u003c=n and gcd(k,n)=1, except in first term, where the convolution sum is 1 instead of 0."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A057789/b057789.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"From _Robert Israel_, Sep 29 2019: (Start)",
				"If n is prime, a(n) = A000292(n-1).",
				"If n/2 is an odd prime, a(n) =  A000292(n-2)/2.",
				"If n/3 is a prime other than 3, a(n) = A000292(n-3)*2*n/(3*(n-2)). (End)"
			],
			"example": [
				"Since 1, 3, 5 and 7 are relatively prime to 8 and are \u003c= 8, a(8) = 1*(8-1) +3*(8-3) +5*(8-5) +7*(8-7) = 44."
			],
			"maple": [
				"f:= proc(n) local i;",
				"  2*add(`if`(igcd(i,n)=1, i*(n-i),0),i=1..n/2)",
				"end proc:",
				"f(2):= 1:",
				"map(f, [$1..100]); # _Robert Israel_, Sep 29 2019"
			],
			"mathematica": [
				"a[n_] := 2 Sum[Boole[CoprimeQ[k, n]] k (n - k), {k, 1, n/2}];",
				"a[2] = 1;",
				"Array[a, 100] (* _Jean-François Alcover_, Aug 16 2020, after Maple *)"
			],
			"program": [
				"(PARI) a(n) = sum(k=1, n, if (gcd(n,k)==1, k*(n-k))); \\\\ _Michel Marcus_, Sep 29 2019"
			],
			"xref": [
				"Cf. A000292."
			],
			"keyword": "nonn,look",
			"offset": "1,3",
			"author": "_Leroy Quet_, Nov 04 2000",
			"references": 1,
			"revision": 23,
			"time": "2020-08-16T08:42:28-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}