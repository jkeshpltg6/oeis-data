{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A300390",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 300390,
			"data": "1,5,227,15090,1182187,101527596,9247179818,877362665128,85783306955099,8582893111512001,874542924575207352,90437361732467946334,9467275300762187682554,1001309098267187214993056,106836493655355495755649544,11485688815900189437990930096,1242964338344397490958154292155",
			"name": "The number of paths of length 7*n from the origin to the line y = 3*x/4 with unit east and north steps that stay below the line or touch it.",
			"comment": [
				"Equivalent to nonnegative walks from (0,0) to (7*n,0) with step set [1,3], [1,-4]."
			],
			"link": [
				"M. T. L. Bizley, \u003ca href=\"/A060941/a060941.pdf\"\u003eDerivation of a new formula for the number of minimal lattice paths from (0, 0) to (km, kn) having just t contacts with the line my = nx and having no points above this line; and a proof of Grossman's formula for the number of paths which may touch but do not rise above this line\u003c/a\u003e, Journal of the Institute of Actuaries, Vol. 80, No. 1 (1954): 55-62. [Cached copy]",
				"Bryan Ek, \u003ca href=\"https://arxiv.org/abs/1803.10920\"\u003eLattice Walk Enumeration\u003c/a\u003e, arXiv:1803.10920 [math.CO], 2018.",
				"Bryan Ek, \u003ca href=\"https://arxiv.org/abs/1804.05933\"\u003eUnimodal Polynomials and Lattice Walk Enumeration with Experimental Mathematics\u003c/a\u003e, arXiv:1804.05933 [math.CO], 2018."
			],
			"formula": [
				"G.f. satisfies: f = f^35*t^5 - f^31*t^4 + f^30*t^4 - f^29*t^4 + 5*f^28*t^4 - f^25*t^3 + f^24*t^3 + 3*f^23*t^3 - 4*f^22*t^3 + 10*f^21*t^3 + f^19*t^2 - f^18*t^2 + 5*f^17*t^2 + 3*f^16*t^2 - 6*f^15*t^2 + 10*f^14*t^2 + f^13*t - f^12*t + 3*f^10*t + f^9*t - 4*f^8*t + 5*f^7*t + 1.",
				"From _Peter Bala_, Jan 03 2019: (Start)",
				"O.g.f.: A(x) = exp( Sum_{n \u003e= 1} (1/7)*binomial(7*n, 3*n)*x^n/n ) - Bizley.",
				"Recurrence: a(0) = 1 and a(n) = (1/n) * Sum_{k = 0..n-1} (1/7)*binomial(7*n-7*k, 3*n-3*k)*a(k) for n \u003e= 1. (End)"
			],
			"example": [
				"For n=1, the possible walks are EEEENNN, EEENENN, EENEENN, EEENNEN, EENENEN."
			],
			"mathematica": [
				"m = 17; f = 0; Do[f = f^35*t^5 - f^31*t^4 + f^30*t^4 - f^29*t^4 + 5*f^28*t^4 - f^25*t^3 + f^24*t^3 + 3*f^23*t^3 - 4*f^22*t^3 + 10*f^21*t^3 + f^19*t^2 - f^18*t^2 + 5*f^17*t^2 + 3*f^16*t^2 - 6*f^15*t^2 + 10*f^14*t^2 + f^13*t - f^12*t + 3*f^10*t + f^9*t - 4*f^8*t + 5*f^7*t + 1 + O[t]^m, {m}]; CoefficientList[f, t] (* _Jean-François Alcover_, Feb 18 2019 *)"
			],
			"xref": [
				"Cf. A001764, A060941, A300386, A300387, A300388, A300389, A300391."
			],
			"keyword": "nonn,walk,easy",
			"offset": "0,2",
			"author": "_Bryan T. Ek_, Mar 05 2018",
			"references": 1,
			"revision": 30,
			"time": "2020-01-03T11:32:56-05:00",
			"created": "2018-03-13T12:58:57-04:00"
		}
	]
}