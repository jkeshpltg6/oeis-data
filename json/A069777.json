{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A069777",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 69777,
			"data": "1,1,1,1,1,1,1,2,1,1,1,6,3,1,1,1,24,21,4,1,1,1,120,315,52,5,1,1,1,720,9765,2080,105,6,1,1,1,5040,615195,251680,8925,186,7,1,1,1,40320,78129765,91611520,3043425,29016,301,8,1,1",
			"name": "Array of q-factorial numbers n!_q, read by ascending antidiagonals.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A069777/b069777.txt\"\u003eAntidiagonals n = 0..55, flattened\u003c/a\u003e",
				"Kent E. Morrison, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL9/Morrison/morrison37.html\"\u003eInteger Sequences and Matrices Over Finite Fields\u003c/a\u003e, Journal of Integer Sequences, Vol. 9 (2006), Article 06.2.1.",
				"\u003ca href=\"/index/Fa#factorial\"\u003eIndex entries for sequences related to factorial numbers\u003c/a\u003e"
			],
			"formula": [
				"T(n,q) = Product_{k=1..n} (q^k - 1) / (q - 1).",
				"T(n,k) = Product_{n1=k..n-1} A104878(n1,k). - _Johannes W. Meijer_, Aug 21 2011"
			],
			"example": [
				"Square array begins:",
				"    1,   1,    1,      1,       1,        1,         1, ...",
				"    1,   1,    1,      1,       1,        1,         1, ...",
				"    1,   2,    3,      4,       5,        6,         7, ...",
				"    1,   6,   21,     52,     105,      186,       301, ...",
				"    1,  24,  315,   2080,    8925,    29016,     77959, ...",
				"    1, 120, 9765, 251680, 3043425, 22661496, 121226245, ...",
				"    ..."
			],
			"maple": [
				"A069777 := proc(n,k) local n1: mul(A104878(n1,k), n1=k..n-1) end: A104878 := proc(n,k): if k = 0 then 1 elif k=1 then n elif k\u003e=2 then (k^(n-k+1)-1)/(k-1) fi: end: seq(seq(A069777(n,k), k=0..n), n=0..9); # _Johannes W. Meijer_, Aug 21 2011",
				"nmax:=9: T(0,0):=1: for n from 1 to nmax do T(n,0):=1:  T(n,1):= (n-1)! od: for q from 2 to nmax do for n from 0 to nmax do T(n+q,q) := product((q^k - 1)/(q - 1), k= 1..n) od: od: for n from 0 to nmax do seq(T(n,k), k=0..n) od; seq(seq(T(n,k), k=0..n), n=0..nmax); # _Johannes W. Meijer_, Aug 21 2011",
				"# alternative Maple program:",
				"T:= proc(n, k) option remember; `if`(n\u003c2, 1,",
				"      T(n-1, k)*`if`(k=1, n, (k^n-1)/(k-1)))",
				"    end:",
				"seq(seq(T(d-k, k), k=0..d), d=0..10);  # _Alois P. Heinz_, Sep 08 2021"
			],
			"mathematica": [
				"(* Returns the rectangular array *) Table[Table[QFactorial[n, q], {q, 0, 6}], {n, 0, 6}] (* _Geoffrey Critzer_, May 21 2017 *)"
			],
			"program": [
				"(PARI) T(n,q)=prod(k=1, n, ((q^k - 1) / (q - 1))) \\\\ _Andrew Howroyd_, Feb 19 2018"
			],
			"xref": [
				"Columns q=0..11 are A000012, A000142, A005329, A015001, A015002, A015004, A015005, A015006, A015007, A015008, A015009, A015011.",
				"Rows n=3..5 are A069778, A069779, A218503.",
				"Main diagonal gives A347611.",
				"Cf. A156173."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,8",
			"author": "_Franklin T. Adams-Watters_, Apr 07 2002",
			"ext": [
				"Name edited by _Michel Marcus_, Sep 08 2021"
			],
			"references": 19,
			"revision": 43,
			"time": "2021-09-08T18:13:45-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}