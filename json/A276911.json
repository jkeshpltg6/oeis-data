{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A276911",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 276911,
			"data": "1,2,6,28,180,1446,13888,156472,2034000,29724490,476806176,8502508884,174802753216,3768345692398,63300353418240,1386349221087856,149879079531401472,5097575010920072850,-780487993325688128000,-32524149870689487270260,10927977097616993825596416,490896441869732669067535414,-213936255246865273137807851520,-10450262329586550037066790750808,6047981224337998054714885264691200",
			"name": "E.g.f. A(x) satisfies: A(A( x*exp(-x) )) = x*exp(x).",
			"comment": [
				"Former name was \"Inverse of e.g.f. A(x) equals its conjugate, where A(x) = Sum_{n\u003e=1} a(n)*i^(n-1)*x^n/n! and i=sqrt(-1).\" - _Paul D. Hanna_, Sep 06 2018"
			],
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A276911/b276911.txt\"\u003eTable of n, a(n) for n = 1..301\u003c/a\u003e"
			],
			"formula": [
				"E.g.f. A(x) satisfies: A(A( x*exp(-x) )) = x*exp(x). - _Paul D. Hanna_, Sep 06 2018",
				"E.g.f. A(x) satisfies: A(-A(-x)) = x. - _Paul D. Hanna_, Sep 06 2018",
				"Inverse of F(x) equals its conjugate, where F(x) = Sum_{n\u003e=1} a(n)*i^(n-1)*x^n/n! and i=sqrt(-1).",
				"Let G(x) be the e.g.f. of A276910, then F(x) = Sum_{n\u003e=1} a(n)*i^(n-1)*x^n/n! satisfies:",
				"(1) F(x) = G(x) * exp(i*G(x)).",
				"(2) G( F(x) ) = i*LambertW(-i*x), where LambertW( x*exp(x) ) = x."
			],
			"example": [
				"E.g.f.: A(x) = x + 2*x^2/2! + 6*x^3/3! + 28*x^4/4! + 180*x^5/5! + 1446*x^6/6! + 13888*x^7/7! + 156472*x^8/8! + 2034000*x^9/9! + 29724490*x^10/10! + ...",
				"such that A(A( x*exp(-x) )) = x*exp(x).",
				"RELATED SERIES.",
				"Let F(x) = x + 2*I*x^2/2! - 6*x^3/3! - 28*I*x^4/4! + 180*x^5/5! + 1446*I*x^6/6! - 13888*x^7/7! - 156472*I*x^8/8! + 2034000*x^9/9! + 29724490*I*x^10/10! - 476806176*x^11/11! - 8502508884*I*x^12/12! + 174802753216*x^13/13! + 3768345692398*I*x^14/14! - 63300353418240*x^15/15! - 1386349221087856*I*x^16/16! + 149879079531401472*x^17/17! +...+ a(n)*i^(n-1)*x^n/n! +...",
				"then",
				"(a) Series_Reversion( F(x) ) = conjugate( F(x) ).",
				"(b) F(x) = G(x)*exp(i*G(x)) where G(x) is the e.g.f. of A276910:",
				"(c) G(x) = x - 3*x^3/3! + 85*x^5/5! - 6111*x^7/7! + 872649*x^9/9! - 195062395*x^11/11! + 76208072733*x^13/13! - 12330526252695*x^15/15! + 125980697776559377*x^17/17! + 857710566759117989133*x^19/19! + 11428318296234746748941925*x^21/21! +...+ A276910(n)*x^n/n! +...",
				"where",
				"G( F(x) ) = x + 2*I*x^2/2! - 9*x^3/3! - 64*I*x^4/4! + 625*x^5/5! + 7776*I*x^6/6! - 117649*x^7/7! - 2097152*I*x^8/8! +...+ -n^(n-1)*(-i)^(n-1)*x^n/n! +..."
			],
			"program": [
				"(PARI) {a(n) = my(V=[1],A=x,G=x); for(i=1,n\\2+1, V = concat(V,[0,0]); G = sum(m=1,#V,V[m]*x^m/m!) +x*O(x^#V);",
				"A = G*exp(I*G); V[#V] = -(#V)!/2 * polcoeff( subst( A, x, conj(A) ),#V) ); n!*(-I)^(n-1)*polcoeff(A,n)}",
				"for(n=1,30,print1(a(n),\", \"))"
			],
			"xref": [
				"Cf. A276910, A276912."
			],
			"keyword": "sign",
			"offset": "1,2",
			"author": "_Paul D. Hanna_, Sep 22 2016",
			"ext": [
				"Name replaced with simpler formula by _Paul D. Hanna_, Sep 06 2018"
			],
			"references": 4,
			"revision": 15,
			"time": "2018-09-06T11:22:53-04:00",
			"created": "2016-09-22T21:46:04-04:00"
		}
	]
}