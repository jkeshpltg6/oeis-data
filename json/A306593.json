{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A306593",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 306593,
			"data": "0,1,2,3,4,5,6,7,8,9,334,65,42,76,455,41,40,98,123,667,64,52,778,788,51,50,899,63,86,7787,2025885,8788,62,74,46996,61,60,66898,67997,85,73,78998,88899,88999,335,72,4579975,878888,71,70,10243,5354,355,989999,114",
			"name": "Least number k such that the determinant of the circulant matrix formed by its decimal digits is equal to n.",
			"comment": [
				"Here only the least numbers are listed: e.g., a(75) = 1031, even if 10002110 also produces 75.",
				"The sequence is infinite because any number of the form (91*10^n - 10) / 90 for n \u003e 0 (A267623 or A283508) has the determinant of the circulant matrix equal to n but, in general, it is not the least possible term. - _Giovanni Resta_, Mar 06 2019"
			],
			"link": [
				"Paolo P. Lava, \u003ca href=\"/A306593/b306593.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e"
			],
			"formula": [
				"A177894(a(n)) = n when a(n) \u003e= 0. - _Rémy Sigrist_, Feb 27 2019"
			],
			"example": [
				"                        | 3 3 4 |",
				"a(10) = 334 because det | 4 3 3 | = 10",
				"                        | 3 4 3 |",
				".",
				"and 334 is the least number to have this property.",
				".",
				"                          | 4 6 9 9 6 |",
				"                          | 6 4 6 9 9 |",
				"a(34) = 46996 because det | 9 6 4 6 9 | = 34",
				"                          | 9 9 6 4 6 |",
				"                          | 6 9 9 6 4 |",
				".",
				"and 46996 is the least number to have this property."
			],
			"maple": [
				"with(linalg): P:=proc(q) local a,b,c,d,j,k,i,n,t;",
				"print(0); for i from 1 to q do for n from 1 to q do",
				"d:=ilog10(n)+1; a:=convert(n, base, 10); c:=[];",
				"for k from 1 to nops(a) do c:=[op(c), a[-k]]; od; t:=[op([]), c];",
				"for k from 2 to d do b:=[op([]), c[nops(c)]];",
				"for j from 1 to nops(c)-1 do",
				"b:=[op(b), c[j]]; od;  c:=b; t:=[op(t), c]; od;",
				"if i=det(t) then print(n); break; fi; od; od; end: P(10^7);"
			],
			"program": [
				"(PARI) md(n) = my(d = if (n, digits(n), [0])); matdet(matrix(#d, #d, i, j, d[1+lift(Mod(j-i, #d))]));",
				"a(n) = my(k=0); while(md(k) != n, k++); k; \\\\ _Michel Marcus_, Mar 20 2019"
			],
			"xref": [
				"Cf. A177894, A219324, A267623, A283508, A306662, A323485, A323486."
			],
			"keyword": "nonn,base",
			"offset": "0,3",
			"author": "_Paolo P. Lava_, Feb 27 2019",
			"references": 5,
			"revision": 39,
			"time": "2019-03-21T04:38:58-04:00",
			"created": "2019-03-06T11:08:39-05:00"
		}
	]
}