{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A254524",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 254524,
			"data": "1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,1,3,3,3,3,3,3,3,3,2,1,4,4,4,4,4,4,4,3,2,1,5,5,5,5,5,5,4,3,2,1,6,6,6,6,6,5,4,3,2,1,7,7,7,7,6,5,4,3,2,1,8,8,8,7,6,5,4,3,2,1,9,9,8,7,6,5,4,3,2,1,10,9,8,7,6,5,4,3,2,1,3,4,5,6,7,8,9,10,11,10,5",
			"name": "n is the a(n)-th positive integer having its digitsum.",
			"comment": [
				"a(A051885(n)) = 1. - _Reinhard Zumkeller_, Oct 09 2015",
				"Ordinal transform of A007953. - _Antti Karttunen_, May 20 2017"
			],
			"link": [
				"David A. Corneth, \u003ca href=\"/A254524/b254524.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"35 is the 4th positive integer having digitsum 8 (the others before are 8, 17 and 26) so a(35) = 4."
			],
			"mathematica": [
				"c[n_, k_] := If[n \u003e= k, Binomial[n, k], 0]; b[s_, q_, n_] := (s1 = q; If[s \u003c= q*(n - 1), s1 = s + q; Sum[(-1)^i*c[q, i]*c[s1 - 1 - n*i, q - 1], {i, 0, q - 1}], 0]); a[n_] := (r = 1; v = IntegerDigits[n]; l = v[[-1]]; For[i = Length[v] - 1, i \u003e= 1, i--, For[j = 1, j \u003c= v[[i]], j++, r += b[l + j, Length[v] - i, 10]]; l += v[[i]]]; r); Table[a[n], {n, 1, 110}] (* _Jean-François Alcover_, Nov 14 2016, adapted from PARI *)",
				"With[{nn=400},#[[3]]\u0026/@Sort[Flatten[Table[Flatten[#,1]\u0026/@MapIndexed[ List,Select[ Table[{n,Total[IntegerDigits[n]]},{n,nn}],#[[2]]==k\u0026]],{k,nn}],1]]](* _Harvey P. Dale_, Mar 29 2020 *)"
			],
			"program": [
				"(PARI)",
				"\\\\This algorithm needs a modified binomial.",
				"C(n, k)=if(n\u003e=k, binomial(n, k), 0)",
				"\\\\ways to roll s-q with q dice having sides 0 through n - 1.",
				"b(s, q, n)=if(s\u003c=q*(n-1), s+=q; sum(i=0, q-1, (-1)^i*C(q, i)*C(s-1-n*i, q-1)), 0)",
				"\\\\main algorithm",
				"a(n)={r = 1; v=digits(n); l=v[#v]; forstep(i = #v-1, 1, -1, for(j=1,v[i], r+=b(l+j, #v-i,10)); l+=v[i]);r}",
				"(Haskell)",
				"import Data.IntMap (empty, findWithDefault, insert)",
				"a254524 n = a254524_list !! (n-1)",
				"a254524_list = f 1 empty where",
				"   f x m = y : f (x + 1) (insert q (y + 1) m) where",
				"           y = findWithDefault 1 q m; q = a007953 x",
				"-- _Reinhard Zumkeller_, Oct 09 2015"
			],
			"xref": [
				"Cf. A007953, A051885, A069877, A143164, A263017, A263109, A263110.",
				"Cf. A286478 (analogous sequence for factorial base)."
			],
			"keyword": "nonn,base,look,nice",
			"offset": "1,10",
			"author": "_David A. Corneth_, Jan 31 2015",
			"references": 17,
			"revision": 35,
			"time": "2020-03-29T15:58:01-04:00",
			"created": "2015-02-01T18:33:23-05:00"
		}
	]
}