{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A286234",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 286234,
			"data": "1,1,2,3,1,4,3,3,2,7,10,3,3,2,11,3,10,3,5,4,16,21,3,10,3,5,4,22,10,21,3,10,5,5,7,29,21,10,21,3,10,5,8,7,37,10,21,10,21,3,14,5,8,11,46,55,10,21,10,21,3,14,5,8,11,56,10,55,10,21,10,21,5,14,8,12,16,67,78,10,55,10,21,10,21,5,14,8,12,16,79,21,78,10,55,10,21,10,27,5,14,8,12,22,92",
			"name": "Square array A(n,k) = P(A000010(k), floor((n+k-1)/k)), read by descending antidiagonals as A(1,1), A(1,2), A(2,1), etc. Here P is a two-argument form of sequence A000027 used as a pairing function N x N -\u003e N.",
			"comment": [
				"Transpose of A286235."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A286234/b286234.txt\"\u003eTable of n, a(n) for n = 1..10585; the first 145 antidiagonals of array\u003c/a\u003e",
				"MathWorld, \u003ca href=\"http://mathworld.wolfram.com/PairingFunction.html\"\u003ePairing Function\u003c/a\u003e"
			],
			"example": [
				"The top left 12 X 12 corner of the array:",
				"   1,  1,  3, 3, 10, 3, 21, 10, 21, 10, 55, 10",
				"   2,  1,  3, 3, 10, 3, 21, 10, 21, 10, 55, 10",
				"   4,  2,  3, 3, 10, 3, 21, 10, 21, 10, 55, 10",
				"   7,  2,  5, 3, 10, 3, 21, 10, 21, 10, 55, 10",
				"  11,  4,  5, 5, 10, 3, 21, 10, 21, 10, 55, 10",
				"  16,  4,  5, 5, 14, 3, 21, 10, 21, 10, 55, 10",
				"  22,  7,  8, 5, 14, 5, 21, 10, 21, 10, 55, 10",
				"  29,  7,  8, 5, 14, 5, 27, 10, 21, 10, 55, 10",
				"  37, 11,  8, 8, 14, 5, 27, 14, 21, 10, 55, 10",
				"  46, 11, 12, 8, 14, 5, 27, 14, 27, 10, 55, 10",
				"  56, 16, 12, 8, 19, 5, 27, 14, 27, 14, 55, 10",
				"  67, 16, 12, 8, 19, 5, 27, 14, 27, 14, 65, 10",
				"The first fifteen rows when viewed as a triangle:",
				"   1",
				"   1  2",
				"   3  1  4",
				"   3  3  2  7",
				"  10  3  3  2 11",
				"   3 10  3  5  4 16",
				"  21  3 10  3  5  4 22",
				"  10 21  3 10  5  5  7 29",
				"  21 10 21  3 10  5  8  7 37",
				"  10 21 10 21  3 14  5  8 11 46",
				"  55 10 21 10 21  3 14  5  8 11 56",
				"  10 55 10 21 10 21  5 14  8 12 16 67",
				"  78 10 55 10 21 10 21  5 14  8 12 16 79",
				"  21 78 10 55 10 21 10 27  5 14  8 12 22 92",
				"  36 21 78 10 55 10 21 10 27  5 19  8 17 22 106"
			],
			"mathematica": [
				"Map[(2 + (#1 + #2)^2 - #1 - 3 #2)/2 \u0026 @@ # \u0026 /@ Reverse@ # \u0026, Table[{EulerPhi@ k, Floor[n/k]}, {n, 14}, {k, n}]] // Flatten (* _Michael De Vlieger_, May 06 2017 *)"
			],
			"program": [
				"(Scheme)",
				"(define (A286234 n) (A286234bi (A002260 n) (A004736 n)))",
				"(define (A286234bi row col) (let ((a (A000010 col)) (b (quotient (+ row col -1) col))) (* (/ 1 2) (+ (expt (+ a b) 2) (- a) (- (* 3 b)) 2))))",
				"(Python)",
				"from sympy import totient",
				"def T(n, m): return ((n + m)**2 - n - 3*m + 2)/2",
				"def t(n, k): return T(totient(k), int(n/k))",
				"for n in range(1, 21): print [t(n, k) for k in range(1, n + 1)][::-1] # _Indranil Ghosh_, May 11 2017"
			],
			"xref": [
				"Transpose: A286235.",
				"Cf. A000010, A000027, A286156, A286236, A286244."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Antti Karttunen_, May 05 2017",
			"references": 4,
			"revision": 28,
			"time": "2019-12-07T12:18:29-05:00",
			"created": "2017-05-06T19:50:35-04:00"
		}
	]
}