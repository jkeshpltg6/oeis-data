{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A234943",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 234943,
			"data": "1,2,3,3,7,3,4,10,13,11,5,12,19,22,17,6,15,24,32,34,25,7,19,30,41,49,51,34,8,22,36,51,64,72,70,44,9,24,42,60,79,94,100,94,55,10,27,48,71",
			"name": "Array read by antidiagonals: T(i,j) = size of optimal difference triangle set M(i,j).",
			"comment": [
				"An (n,k) difference triangle set is a set of n blocks of k integers such that the difference sets of the blocks are all disjoint. The \"scope\" of such a set is defined as the maximal element, if all blocks are translated such that their least elements are all 0. T(n,k) lists the minimal scope for which an (n,k) difference triangle set exists. - _Charlie Neder_, Jun 14 2019"
			],
			"reference": [
				"CRC Handbook of Combinatorial Designs, 1996, p. 315. (But beware errors!)"
			],
			"link": [
				"Yeow Meng Chee and Charles J. Colbourn, \u003ca href=\"https://arxiv.org/abs/0712.2553\"\u003eConstructions for Difference Triangle Sets\u003c/a\u003e, arXiv:0712.2553 [cs.IT], 2007.",
				"J. B. Shearer, \u003ca href=\"https://web.archive.org/web/20180228224432/http://www.research.ibm.com/people/s/shearer/dtsopt.html\"\u003eDifference Triangle Sets: Known optimal solutions\u003c/a\u003e.",
				"J. B. Shearer, \u003ca href=\"https://web.archive.org/web/20171118235702/http://www.research.ibm.com/people/s/shearer/dtslb.html\"\u003eDifference Triangle Sets: Discoverers\u003c/a\u003e"
			],
			"example": [
				"Array begins:",
				"j\\i| 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,",
				"-------------------------------------------------------",
				"1  | 1,  2,   3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15,  (A000027)",
				"2  | 3,  7,  10, 12, 15, 19, 22, 24, 27, 31, 34, 36, 39, 43, 46,  (A013574)",
				"3  | 6,  13, 19, 24, 30, 36, 42, 48, 54, 60, 66, 72, 78, 84, 90,  (A013575)",
				"4  | 11, 22, 32, 41, 51, 60, 71, 80, 91, 100, 111, 120, 131, 140, 151, (A013576)",
				"5  | 17, 34, 49, 64, 79,  (A013577)",
				"6  | 25, 51, 72, 94,",
				"7  | 34, 70, 100,",
				"8  | 44, 94,",
				"9  | 55, 121,",
				"10 | 72,",
				"11 | 85",
				"12 | 106",
				"13 | 127",
				"14 | 151",
				"15 | 177",
				"  A003022 A010896 A010898",
				"       A010895 A010897 A010899"
			],
			"xref": [
				"Rows and columns give A000027, A013574, A013575, A013576, A013577; A003022, A010895, A010896, A010897, A010898, A010899.",
				"For the number of different optimal triangle difference sets see the corresponding array in A234947."
			],
			"keyword": "nonn,tabl,more",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Jan 08 2014",
			"references": 12,
			"revision": 19,
			"time": "2019-06-15T10:27:22-04:00",
			"created": "2014-01-08T22:31:09-05:00"
		}
	]
}