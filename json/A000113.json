{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000113",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 113,
			"data": "1,3,4,3,6,12,8,6,4,18,12,12,14,24,24,6,18,12,20,18,32,36,24,24,30,42,12,24,30,72,32,12,48,54,48,12,38,60,56,36,42,96,44,36,24,72,48,24,56,90,72,42,54,36,72,48,80,90,60,72,62,96,32,12,84,144,68",
			"name": "Number of transformation groups of order n.",
			"comment": [
				"A strong divisibility sequence, that is, gcd(a(n), a(m)) = a(gcd(n, m)) for all positive integers n and m. - _Michael Somos_, Jan 03 2017"
			],
			"reference": [
				"B. Schoeneberg, Elliptic Modular Functions, Springer-Verlag, NY, 1974, p. 139."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A000113/b000113.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Di#divseq\"\u003eIndex to divisibility sequences\u003c/a\u003e"
			],
			"formula": [
				"Let psi(m) = A001615(m) (Dedekind's psi function). Write n = 2^i*3^j*k, where (6,k) = 1 and let i' = floor(i/2) for i \u003c 6, i' = 3 for i \u003e= 6; let j' = 0 for j = 0 or 1, j' = 1 for j \u003e= 2. Then a(n) = psi(n/(2^i'*3^j')) = psi(n)/(2^i'*3^j').",
				"Multiplicative with a(2^e)=3*2^Floor[(e-1)/2] for 0\u003ce\u003c7, a(2^e)=3*2^(e-4) for e\u003e=7, a(3^e)=4 for 0\u003ce\u003c3, a(3^e)=4*3^(e-2) for e\u003e=3 and a(p^e)=(p+1)*p^(e-1) for p\u003e3. - _T. D. Noe_, Nov 14 2006"
			],
			"example": [
				"G.f. = x + 3*x^2 + 4*x^3 + 3*x^4 + 6*x^5 + 12*x^6 + 8*x^7 + 6*x^8 + 4*x^9 + ..."
			],
			"mathematica": [
				"psi[n_] := n*DivisorSum[n, MoebiusMu[#]^2/#\u0026]; a[n_] := (i=IntegerExponent[ n, 2]; j=IntegerExponent[n, 3]; ip = If[i\u003c6, Floor[i/2], 3]; jp = If[j\u003c2, 0, 1]; psi[n]/(2^ip*3^jp)); Array[a, 60] (* _Jean-François Alcover_, Feb 04 2016 *)",
				"a[ n_] := If[ n \u003c 1, 0, n Sum[ MoebiusMu[d]^2/d, {d, Divisors @ n}] / (2^Min[3, Quotient[IntegerExponent[n, 2], 2]] 3^Boole[1 \u003c IntegerExponent[n, 3]]) ]; (* _Michael Somos_, Jan 03 2017 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, 0, n * sumdiv(n, d, moebius(d)^2 / d) / (2^min(3, valuation(n, 2)\\2) * 3^(1 \u003c valuation(n, 3))))}; /* _Michael Somos_, Jan 03 2017 */"
			],
			"keyword": "nonn,easy,mult",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"references": 1,
			"revision": 22,
			"time": "2017-01-03T23:10:05-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}