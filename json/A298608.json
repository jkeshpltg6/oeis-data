{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A298608",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 298608,
			"data": "1,0,1,2,1,1,2,6,2,1,6,9,12,3,1,8,30,24,20,4,1,20,50,90,50,30,5,1,30,140,180,210,90,42,6,1,70,245,560,490,420,147,56,7,1,112,630,1120,1680,1120,756,224,72,8,1",
			"name": "Polynomials related to the Motzkin numbers for Coxeter type D, T(n, k) for n \u003e= 0 and 0 \u003c= k \u003c= n.",
			"comment": [
				"The polynomials evaluated at x = 1 give the analog of the Motzkin numbers for Coxeter type D (see A298300 (with a shift in the indexing))."
			],
			"formula": [
				"T(n,k) = A109187(n,k) + A298609(n,k).",
				"The polynomials are defined by p(0, x) = 1 and for n \u003e= 1 by p(n, x) = G(n,-n,-x/2) + G(n-1,-n,-x/2)*(n-1)/n where G(n, a, x) denotes the n-th Gegenbauer polynomial.",
				"p(n, x) = binomial(2*n,n)*(hypergeom([-n,-n], [-n+1/2], 1/2-x/4) + ((n-1)/(n+1))* hypergeom([-n+1,-n-1], [-n+1/2], 1/2-x/4))) for n \u003e= 1."
			],
			"example": [
				"The first few polynomials are:",
				"p0(x) =  1;",
				"p1(x) =  0 +     x;",
				"p2(x) =  2 +     x +     x^2;",
				"p3(x) =  2 +   6*x +   2*x^2 +     x^3;",
				"p4(x) =  6 +   9*x +  12*x^2 +   3*x^3 +    x^4;",
				"p5(x) =  8 +  30*x +  24*x^2 +  20*x^3 +   4*x^4 +     x^5;",
				"p6(x) = 20 +  50*x +  90*x^2 +  50*x^3 +  30*x^4 +   5*x^5 +    x^6;",
				"p7(x) = 30 + 140*x + 180*x^2 + 210*x^3 +  90*x^4 +  42*x^5 +  6*x^6 +   x^7;",
				"The triangle starts:",
				"[0][  1]",
				"[1][  0,   1]",
				"[2][  2,   1,    1]",
				"[3][  2,   6,    2,    1]",
				"[4][  6,   9,   12,    3,    1]",
				"[5][  8,  30,   24,   20,    4,   1]",
				"[6][ 20,  50,   90,   50,   30,   5,   1]",
				"[7][ 30, 140,  180,  210,   90,  42,   6,  1]",
				"[8][ 70, 245,  560,  490,  420, 147,  56,  7, 1]",
				"[9][112, 630, 1120, 1680, 1120, 756, 224, 72, 8, 1]"
			],
			"maple": [
				"A298608Poly := n -\u003e `if`(n=0, 1, binomial(2*n, n)*(hypergeom([-n, -n], [-n+1/2], 1/2-x/4) + ((n-1)/(n+1))*hypergeom([-n+1, -n-1], [-n+1/2], 1/2-x/4))):",
				"A298608Row := n -\u003e op(PolynomialTools:-CoefficientList(simplify(A298608Poly(n)), x)): seq(A298608Row(n), n=0..9);"
			],
			"mathematica": [
				"p[0] := 1;",
				"p[n_] := GegenbauerC[n, -n , -x/2] + GegenbauerC[n - 1, -n , -x/2] (n - 1) / n;",
				"Table[CoefficientList[p[n], x], {n, 0, 9}] // Flatten"
			],
			"xref": [
				"Row sums are A298300(n+1) for n \u003e= 1.",
				"Cf. A109187, A298609."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Peter Luschny_, Jan 23 2018",
			"references": 2,
			"revision": 17,
			"time": "2018-01-25T03:36:15-05:00",
			"created": "2018-01-25T03:36:15-05:00"
		}
	]
}