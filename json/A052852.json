{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A052852",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 52852,
			"data": "0,1,4,21,136,1045,9276,93289,1047376,12975561,175721140,2581284541,40864292184,693347907421,12548540320876,241253367679185,4909234733857696,105394372192969489,2380337795595885156",
			"name": "Expansion of e.g.f.: (x/(1-x))*exp(x/(1-x)).",
			"comment": [
				"A simple grammar.",
				"Number of {121,212}-avoiding n-ary words of length n. - _Ralf Stephan_, Apr 20 2004",
				"The infinite continued fraction (1+n)/(1+(2+n)/(2+(3+n)/(3+...))) converges to the rational number A052852(n)/A000262(n) when n is a positive integer. - David Angell (angell(AT)maths.unsw.edu.au), Dec 18 2008"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A052852/b052852.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"David Angell, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2009.12.003\"\u003eA family of continued fractions\u003c/a\u003e, Journal of Number Theory, Volume 130, Issue 4, April 2010, Pages 904-911, Section 2.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=820\"\u003eEncyclopedia of Combinatorial Structures 820\u003c/a\u003e",
				"Florent Hivert, Jean-Christophe Novelli and Jean-Yves Thibon, \u003ca href=\"https://arxiv.org/abs/math/0605262\"\u003eCommutative combinatorial Hopf algebras\u003c/a\u003e, arXiv:math/0605262 [math.CO], 2006.",
				"John Riordan, \u003ca href=\"/A002720/a002720_3.pdf\"\u003eLetter to N. J. A. Sloane, Sep 26 1980 with notes on the 1973 Handbook of Integer Sequences\u003c/a\u003e. Note that the sequences are identified by their N-numbers, not their A-numbers.",
				"Luis Verde-Star \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL24/Verde/verde4.html\"\u003eA Matrix Approach to Generalized Delannoy and Schröder Arrays\u003c/a\u003e, J. Int. Seq., Vol. 24 (2021), Article 21.4.1.",
				"Michael Wallner, \u003ca href=\"https://arxiv.org/abs/1706.07163\"\u003eA bijection of plane increasing trees with relaxed binary trees of right height at most one\u003c/a\u003e, arXiv:1706.07163 [math.CO], 2017, Table 2 on p. 13.",
				"\u003ca href=\"/index/La#Laguerre\"\u003eIndex entries for sequences related to Laguerre polynomials\u003c/a\u003e"
			],
			"formula": [
				"D-finite with recurrence: a(1)=1, a(0)=0, (n^2+2*n)*a(n)+(-4-2*n)*a(n+1)+ a(n+2)=0.",
				"a(n) = Sum_{m=0..n} n!*binomial(n+2, n-m)/m!. - _Wolfdieter Lang_, Jun 19 2001",
				"a(n) = n*A002720(n-1). [Riordan] - _Vladeta Jovovic_, Mar 18 2005",
				"Related to an n-dimensional series : for n\u003e=1, a(n)=(n!/e)* sum_{k_n\u003e=k_{n-1}\u003e=...\u003e=k_1\u003e=0}1/(k_n)!). - _Benoit Cloitre_, Sep 30 2006",
				"E.g.f.: (x/(1-x))*exp((x/(1-x)))  =(x/(1-x))*G(0); G(k)=1+x/((2*k+1)*(1-x)-x*(1-x)*(2*k+1)/(x+(1-x)*(2*k+2)/G(k+1))); (continued fraction). - _Sergei N. Gladkovskii_, Nov 24 2011",
				"a(n) = D^n(x*exp(x)) evaluated at x = 0, where D is the operator (1+x)^2*d/dx. Cf. A000262 and A005493. - _Peter Bala_, Nov 25 2011",
				"a(n) ~ exp(2*sqrt(n)-n-1/2)*n^(n+1/4)/sqrt(2). - _Vaclav Kotesovec_, Oct 09 2012",
				"a(n) = (n+1)!*hypergeom([-n+1], [3], -1)/2 for n\u003e=1. - _Peter Luschny_, Oct 18 2014",
				"a(n) = Sum_{k=0..n} L(n,k)*k; L(n,k) the unsigned Lah numbers. - _Peter Luschny_, Oct 18 2014",
				"a(n) = (n-1)!*LaguerreL(n-1, 2, -1) for n\u003e=1. - _Peter Luschny_, Apr 08 2015",
				"The series reversion of the e.g.f. equals W(x)/(1 + W(x)) = x - 2^2*x^2/2! + 3^3*x^3/3! - 4^4*x^4/4! + ..., essentially the e.g.f. for a signed version of A000312, where W(x) is Lambert's W-function (see A000169). - _Peter Bala_, Jun 14 2016",
				"Equals column A059114(n, 2) for n \u003e= 1. - _G. C. Greubel_, Feb 23 2021"
			],
			"maple": [
				"spec := [S,{B=Set(C),C=Sequence(Z,1 \u003c= card),S=Prod(B,C)},labeled]: seq(combstruct[count](spec,size=n), n=0..20);",
				"a := n -\u003e `if`(n=0, 0, (n+1)!*hypergeom([-n+1], [3], -1)/2); seq(simplify(a(n)), n=0..18); # _Peter Luschny_, Oct 18 2014"
			],
			"mathematica": [
				"Table[n!*SeriesCoefficient[(x/(1-x))*E^(x/(1-x)),{x,0,n}],{n,0,20}] (* _Vaclav Kotesovec_, Oct 09 2012 *)",
				"Table[If[n==0, 0, n!*LaguerreL[n-1, 0, -1]], {n, 0, 30}] (* _G. C. Greubel_, Feb 23 2021 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^30)); concat([0], Vec(serlaplace((x/(1-x))*exp(x/(1-x))))) \\\\ _G. C. Greubel_, May 15 2018",
				"(Sage) [0 if n==0 else factorial(n)*gen_laguerre(n-1, 0, -1) for n in (0..30)] # _G. C. Greubel_, Feb 23 2021",
				"(Magma) [n eq 0 select 0 else Factorial(n)*Evaluate(LaguerrePolynomial(n-1, 0), -1): n in [0..30]]; // _G. C. Greubel_, Feb 23 2021"
			],
			"xref": [
				"Row sums of unsigned triangle A062139 (generalized a=2 Laguerre).",
				"Cf. A000262. A000169, A000312.",
				"Cf. A059114."
			],
			"keyword": "easy,nonn",
			"offset": "0,3",
			"author": "encyclopedia(AT)pommard.inria.fr, Jan 25 2000",
			"references": 34,
			"revision": 80,
			"time": "2021-06-11T18:24:33-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}