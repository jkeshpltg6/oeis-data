{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A230037",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 230037,
			"data": "0,0,1,1,2,1,2,1,2,1,2,2,2,3,3,2,2,2,2,4,3,2,3,2,5,2,4,3,4,4,4,3,3,4,5,7,4,5,2,5,4,5,7,5,5,4,4,4,6,6,8,4,5,3,4,5,6,7,4,6,2,5,3,7,8,4,4,1,4,2,7,6,3,5,3,5,4,6,6,5,4,3,5,4,5,3,3,3,6,7,5,2,4,4,5,3,6,4,3,5",
			"name": "Number of ways to write n = x + y + z (0 \u003c x \u003c= y \u003c= z) such that the four pairs {6*x-1, 6*x+1}, {6*y-1, 6*y+1}, {6*z-1, 6*z+1} and {6*x*y-1, 6*x*y+1} are twin prime pairs.",
			"comment": [
				"Conjecture: a(n) \u003e 0 for all n \u003e 2. Moreover, any integer n \u003e 2 can be written as x + y + z with x = 1 or 5 such that {6*y-1, 6*y+1}, {6*z-1, 6*z+1} and {6*x*y-1, 6*x*y+1} are twin prime pairs.",
				"We have verified this for n up to 5*10^7. It implies the twin prime conjecture.",
				"Zhi-Wei Sun also made the following similar conjectures:",
				"(i) Any integer n \u003e 2 can be written as x + y + z (x, y, z \u003e 0) with the 8 numbers 6*x-1, 6*x+1, 6*y-1, 6*y+1, 6*z-1, 6*z+1, 6*x*y-1 and 6*x*y*z-1 (or 12*x*y-1) all prime.",
				"(ii) Each integer n \u003e 2 can be written as x + y + z (x, y, z \u003e 0) with the 8 numbers 6*x-1, 6*x+1, 6*y-1, 12*y-1, 6*z-1 (or 6*x*y-1), 2*(x^2+y^2)+1, 2*(x^2+z^2)+1, 2*(y^2+z^2)+1 all prime.",
				"(iii) Any integer n \u003e 8 can be written as x + y + z (x, y, z \u003e 0) with x-1, x+1, y-1, y+1, x*z-1 and y*z-1 all prime.",
				"(iv) Every integer n \u003e 4 can be written as p + q + r (r \u003e 0) with p, q, 2*p*q-1, 2*p*r-1 and 2*q*r-1 all prime.",
				"(v) Any integer n \u003e 10 can be written as x^2 + y^2 + z (x, y, z \u003e 0) with 2*x*y-1, 2*x*z+1 and 2*y*z+1 all prime."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A230037/b230037.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;60ad884f.1310\"\u003eTwo conjectures involving six primes\u003c/a\u003e, a message to Number Theory List, Oct. 5, 2013.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1211.1588\"\u003eConjectures involving primes and quadratic forms\u003c/a\u003e, preprint, arXiv:1211.1588."
			],
			"example": [
				"a(10) = 1 since 10 = 1 + 2 + 7 , and {6*1-1, 6*1+1}, {6*2-1, 6*2+1}, {6*7-1, 6*7+1}  and {6*1*2-1, 6*1*2+1} are twin prime pairs."
			],
			"mathematica": [
				"a[n_]:=Sum[If[PrimeQ[6i-1]\u0026\u0026PrimeQ[6i+1]\u0026\u0026PrimeQ[6j-1]\u0026\u0026PrimeQ[6j+1]\u0026\u0026PrimeQ[6i*j-1]",
				"\u0026\u0026PrimeQ[6*i*j+1]\u0026\u0026PrimeQ[6(n-i-j)-1]\u0026\u0026PrimeQ[6(n-i-j)+1],1,0],{i,1,n/3},{j,i,(n-i)/2}]",
				"Table[a[n],{n,1,100}]"
			],
			"xref": [
				"Cf. A001359, A006512, A219842, A219864, A229969, A229974, A230040."
			],
			"keyword": "nonn",
			"offset": "1,5",
			"author": "_Zhi-Wei Sun_, Oct 06 2013",
			"references": 4,
			"revision": 18,
			"time": "2013-10-10T03:01:24-04:00",
			"created": "2013-10-06T03:04:54-04:00"
		}
	]
}