{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A122129",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 122129,
			"data": "1,1,1,2,3,4,5,7,9,12,15,19,24,30,37,46,57,69,84,102,123,148,177,211,252,299,353,417,491,576,675,789,920,1071,1244,1442,1670,1929,2224,2562,2946,3381,3876,4437,5072,5791,6602,7517,8551,9714,11021,12493,14145",
			"name": "Expansion of 1 + Sum_{k\u003e0} x^k^2/((1-x)(1-x^2)...(1-x^(2k))).",
			"comment": [
				"Generating function arises naturally in Rodney Baxter's solution of the Hard Hexagon Model according to George Andrews.",
				"a(n) = number of SE partitions of n, for n \u003e= 1; see A237981.  _Clark Kimberling_, Mar 19 2014",
				"In Watson 1937 page 275 he writes \"Psi_0(1,q) = prod_1^oo (1+q^{2n}) G(q^8)\" so this is the expansion in powers of q^2. - _Michael Somos_, Jun 28 2015",
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Rogers-Ramanujan functions: G(x) (see A003114), H(x) (A003106)."
			],
			"reference": [
				"G. E. Andrews, q-series, CBMS Regional Conference Series in Mathematics, 66, Amer. Math. Soc. 1986, see p. 8, Eq. (1.7). MR0858826 (88b:11063)",
				"G. E. Andrews, R. Askey and R. Roy, Special Functions, Cambridge University Press, 1999; Exercise 6(a), p. 591.",
				"Watson, G. N. (1937), \"The Mock Theta Functions (2)\", Proceedings of the London Mathematical Society, s2-42: 274-304, doi:10.1112/plms/s2-42.1.274"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A122129/b122129.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"M. D. Hirschhorn, \u003ca href=\"http://dx.doi.org/10.1016/0097-3165(79)90005-0\"\u003eSome partition theorems of the Rogers-Ramanujan type\u003c/a\u003e, J. Combin. Theory Ser. A 27 (1979), no. 1, 33--37. MR0541341 (80j:05010). See Theorem 1. [From _N. J. A. Sloane_, Mar 19 2012]",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Euler transform of period 20 sequence [ 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, ...].",
				"Expansion of f(-x^2) * f(-x^20) / (f(-x) * f(-x^4,-x^16)) in powers of x where f(,) is the Ramanujan general theta function.",
				"Expansion of f(x^3, x^7) / f(-x, -x^4) in powers of x where f(,) is the Ramanujan general theta function. - _Michael Somos_, Jun 28 2015",
				"Expansion of f(-x^8, -x^12) / psi(-x) in powers of x where psi() is a Ramanujan theta function. - _Michael Somos_, Jun 28 2015",
				"Expansion of G(x^4) / chi(-x) in powers of x where chi() is a Ramanujan theta function and G() is a Rogers-Ramanujan function. - _Michael Somos_, Jun 28 2015",
				"G.f.: Sum_{k\u003e=0} x^k^2 / ((1 - x) * (1 - x^2) ... (1 - x^(2*k))).",
				"G.f.: 1 / (Product_{k\u003e0} (1 - x^(2*k-1)) * (1 - x^(20*k-4)) * (1 - x^(20*k-16))).",
				"Let f(n) = 1/Product_{k \u003e= 0} (1-q^(20k+n)). Then g.f. is f(1)*f(3)*f(4)*f(5)*f(7)*f(9)*f(11)*f(13)*f(15)*f(16)*f(17)*f(19). - _N. J. A. Sloane_, Mar 19 2012.",
				"a(n) = number of partitions of n into parts that are either odd or == +/-4 (mod 20). - _Michael Somos_, Jun 28 2015",
				"a(n) ~ (3+sqrt(5))^(1/4) * exp(Pi*sqrt(2*n/5)) / (4*sqrt(5)*n^(3/4)). - _Vaclav Kotesovec_, Aug 30 2015"
			],
			"example": [
				"Clark Kimberling's SE partition comment, n=6: the 5 SE partitions are [1,1,1,1,1,1] from the partitions 6 and 1^6; [1,1,1,2,1] from 5,1 and 2,1^4; [1,1,3,1] from 4,2 and 2^2,1^2; [2,3,1] from 3,2,1 and 3^2 and 2^3; and [1,2,2,1] from 4,1^2 and 3,1^3. - _Wolfdieter Lang_, Mar 20 2014",
				"G.f. = 1 + x + x^2 + 2*x^3 + 3*x^4 + 4*x^5 + 5*x^6 + 7*x^7 + 9*x^8 + ...",
				"G.f. = 1/q + q^39 + q^79 + 2*q^119 + 3*q^159 + 4*q^199 + 5*q^239 + ..."
			],
			"maple": [
				"f:=n-\u003e1/mul(1-q^(20*k+n),k=0..20);",
				"f(1)*f(3)*f(4)*f(5)*f(7)*f(9)*f(11)*f(13)*f(15)*f(16)*f(17)*f(19);",
				"series(%,q,200); seriestolist(%); # _N. J. A. Sloane_, Mar 19 2012.",
				"# second Maple program:",
				"with(numtheory):",
				"a:= proc(n) option remember; `if`(n=0, 1, add(add(d*[0, 1, 0,",
				"       1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1]",
				"      [1+irem(d, 20)], d=divisors(j)) *a(n-j), j=1..n)/n)",
				"    end:",
				"seq(a(n), n=0..60);  # _Alois P. Heinz_, Jul 12 2013"
			],
			"mathematica": [
				"a[0] = 1; a[n_] := a[n] = Sum[Sum[d*{0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1}[[1+Mod[d, 20]]], {d, Divisors[j]}]*a[n-j], {j, 1, n}]/n; Table[a[n], {n, 0, 60}] (* _Jean-François Alcover_, Jan 10 2014, after _Alois P. Heinz_ *)",
				"a[ n_] := If[ n \u003c 0, 0, SeriesCoefficient[ Sum[ x^k^2 / QPochhammer[ x, x, 2 k], {k, 0, Sqrt @ n}], {x, 0, n}]]; (* _Michael Somos_, Jun 28 2015 *)",
				"a[ n_] := SeriesCoefficient[ 1 / (QPochhammer[ x, x^2] QPochhammer[ x^4, x^20] QPochhammer[ x^16, x^20]), {x, 0, n}]; (* _Michael Somos_, Jun 28 2015 *)"
			],
			"program": [
				"(PARI)",
				"{a(n) = if( n\u003c0, 0, polcoeff( sum(k=0, sqrtint(n), x^k^2 / prod(i=1, 2*k, 1 - x^i, 1 + x * O(x^(n-k^2)))), n))};"
			],
			"xref": [
				"Cf. A122135."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Michael Somos_, Aug 21 2006",
			"references": 9,
			"revision": 36,
			"time": "2021-11-23T05:50:32-05:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}