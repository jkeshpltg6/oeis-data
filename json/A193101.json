{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A193101",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 193101,
			"data": "1,2,1,2,3,2,1,2,3,2,3,4,3,1,2,3,2,3,4,3,2,3,4,3,1,2,3,2,3,4,3,2,3,4,3,4,5,4,2,3,1,2,3,2,3,3,3,2,3,2,3,4,3,4,2,3,3,3,4,4,4,3,1,2,3,2,3,4,3,2,3,4,3,4,3,4,2,3,4,3,4,2,3,3,3,4,4,2,3,4,3,1,2,3,2,3,4,3,2,3,4,3,4,2,3,2,3,4,3,4,3,4,3,4,5,4,2,3,4,3",
			"name": "Minimal number of numbers of the form (m^3+5m)/6 (see A004006) needed to sum to n.",
			"comment": [
				"Watson showed that a(n) \u003c= 8 for all n.",
				"It is conjectured that a(n) \u003c= 5 for all n."
			],
			"link": [
				"Lars Blomberg, \u003ca href=\"/A193101/b193101.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"H. E. Salzer and N. Levine, \u003ca href=\"https://doi.org/10.1090/S0025-5718-1968-0224578-6\"\u003eProof that every integer \u003c= 452,479,659 is a sum of five numbers of the form Q_x = (x^3+5x)/6, x\u003e= 0\u003c/a\u003e, Math. Comp., (1968), 191-192.",
				"N. J. A. Sloane, \u003ca href=\"/transforms.txt\"\u003eTransforms\u003c/a\u003e",
				"G. L. Watson, \u003ca href=\"https://doi.org/10.1112/jlms/s1-27.2.217\"\u003eSums of eight values of a cubic polynomial\u003c/a\u003e, J. London Math. Soc., 27 (1952), 217-224."
			],
			"maple": [
				"# LAGRANGE transform of a sequence {a(n)}",
				"# Suggested by Lagrange's theorem that at most 4 squares are needed to sum to n.",
				"# Returns b(n) = minimal number of terms of {a} needed to sum to n for 1 \u003c= n \u003c= M.",
				"# C = maximal number of terms of {a} to try to build n",
				"# M = upper limit on n",
				"# Internally, the initial terms of both a and b are taken to be 0, but since this is a number-theoretic function, the output starts at n=1",
				"LAGRANGE:=proc(a,C,M)",
				"local t1,ip,i,j,a1,a2,b,c,N1,N2,Nc;",
				"if whattype(a) \u003c\u003e list then RETURN([]); fi:",
				"# sort a, remove duplicates, include 0",
				"t1:=sort(a);",
				"a1:=sort(convert(convert(a,set),list));",
				"if not member(0,a1) then a1:=[0,op(a1)]; fi;",
				"N1:=nops(a1);",
				"b:=Array(1..M+1,-1);",
				"for i from 1 to N1 while a1[i]\u003c=M do b[a1[i]+1]:=1; od;",
				"a2:=a1; N2:=N1;",
				"for ip from 2 to C do",
				"c:={}:",
				"   for i from 1 to N1 while a1[i] \u003c= M do",
				"      for j from 1 to N2 while a1[i]+a2[j] \u003c= M do",
				"c:={op(c),a1[i]+a2[j]};",
				"                                                od;",
				"                                       od;",
				"c:=sort(convert(c,list));",
				"Nc:=nops(c);",
				"   for i from 1 to Nc do",
				"      if b[c[i]+1] = -1 then b[c[i]+1]:= ip; fi;",
				"                      od;",
				"a2:=c; N2:=Nc;",
				"                   od;",
				"[seq(b[i],i=2..M+1)];",
				"end;",
				"Q:=[seq((m^3+5*m)/6,m=0..20)];",
				"LAGRANGE(Q,8,120);"
			],
			"xref": [
				"Cf. A004006. A002828, A104246, A193105."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Jul 15 2011",
			"references": 4,
			"revision": 24,
			"time": "2017-10-12T10:20:13-04:00",
			"created": "2011-07-15T23:15:55-04:00"
		}
	]
}