{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A128552",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 128552,
			"data": "1,3,8,18,39,75,141,251,433,724,1185,1892,2972,4588,6981,10480,15553,22821,33164,47746,68163,96542,135747,189550,262997,362691,497339,678300,920417,1242898,1670688,2235880,2979809,3955422,5230471,6891234",
			"name": "Column 2 of triangle A128545; a(n) is the coefficient of q^(2n+4) in the central q-binomial coefficient [2n+4,n+2].",
			"comment": [
				"Column 1 of triangle A128552 equals the partitions of n (A000041).",
				"a(n) is the number of partitions of the integer 2n+4 into at most n+2 summands each of which is at most n+2. - _Geoffrey Critzer_, Sep 27 2013"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A128552/b128552.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"P. Flajolet and R. Sedgewick, \u003ca href=\"http://algo.inria.fr/flajolet/Publications/AnaCombi/anacombi.html\"\u003eAnalytic Combinatorics\u003c/a\u003e, 2009, page 45.",
				"Shishuo Fu and James Sellers, \u003ca href=\"https://www.emis.de/journals/INTEGERS/papers/m24/m24.Abstract.html\"\u003eEnumeration of the degree sequences of line-Hamiltonian multigraphs\u003c/a\u003e, INTEGERS 12 (2012), #A24. - From _N. J. A. Sloane_, Feb 04 2013"
			],
			"formula": [
				"a(n) = A000041(2*n+4) - 2*Sum_{k=0..n+1} A000041(k), where A000041(n) = number of partitions of n, due to a formula given in the Fu and Sellers paper. - _Paul D. Hanna_, Feb 06 2013"
			],
			"example": [
				"a(2) = 8 because we have: 4+4 = 4+3+1 = 4+2+2 = 4+2+1+1 = 3+3+2 = 3+3+1+1 = 3+2+2+1 = 2+2+2+2. - _Geoffrey Critzer_, Sep 27 2013"
			],
			"maple": [
				"with(combinat): p:= numbpart:",
				"s:= proc(n) s(n):= p(n) +`if`(n\u003e0, s(n-1), 0) end:",
				"a:= n-\u003e p(2*n+4) -2*s(n+1):",
				"seq(a(n), n=0..40);  # _Alois P. Heinz_, Sep 27 2013"
			],
			"mathematica": [
				"Table[nn=2n;Coefficient[Series[Product[(1-x^(n+i))/(1-x^i),{i,1,n}],{x,0,nn}],x^(2n)],{n,1,37}] (* _Geoffrey Critzer_, Sep 27 2013 *)"
			],
			"program": [
				"(PARI) {a(n)=polcoeff(prod(j=n+3,2*n+4,1-q^j)/prod(j=1,n+2,1-q^j),2*n+4,q)}",
				"(PARI) {a(n)=numbpart(2*n+4)-2*sum(k=0,n+1,numbpart(k))} \\\\ _Paul D. Hanna_, Feb 06 2013"
			],
			"xref": [
				"Cf. A128545; A128553, A128554."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Paul D. Hanna_, Mar 10 2007",
			"references": 5,
			"revision": 18,
			"time": "2019-04-25T04:25:32-04:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}