{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A145390",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 145390,
			"data": "1,1,2,3,2,2,2,5,3,2,2,6,2,2,4,7,2,3,2,6,4,2,2,10,3,2,4,6,2,4,2,9,4,2,4,9,2,2,4,10,2,4,2,6,6,2,2,14,3,3,4,6,2,4,4,10,4,2,2,12,2,2,6,11,4,4,2,6,4,4,2,15,2,2,6,6,4,4,2,14,5,2,2,12,4,2,4,10,2,6,4,6,4,2,4,18,2,3,6,9,2",
			"name": "Number of sublattices of index n of a centered rectangular lattice fixed by a reflection.",
			"comment": [
				"a(n) is the Dirichlet convolution of A000012 and A098178. - Domenico (domenicoo(AT)gmail.com), Oct 21 2009"
			],
			"link": [
				"Andrey Zabolotskiy, \u003ca href=\"/A145390/b145390.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Amihay Hanany, Domenico Orlando, and Susanne Reffert, \u003ca href=\"https://doi.org/10.1007/JHEP06(2010)051\"\u003eSublattice counting and orbifolds\u003c/a\u003e, High Energ. Phys., 2010 (2010), 51, \u003ca href=\"https://arxiv.org/abs/1002.2981\"\u003earXiv.org:1002.2981 [hep-th]\u003c/a\u003e (see Table 3)",
				"John S. Rutherford, \u003ca href=\"http://dx.doi.org/10.1107/S010876730804333X\"\u003eSublattice enumeration. IV. Equivalence classes of plane sublattices by parent Patterson symmetry and colour lattice group type\u003c/a\u003e, Acta Cryst. (2009). A65, 156-163. [See Table 1]. - From _N. J. A. Sloane_, Feb 23 2009"
			],
			"formula": [
				"Dirichlet g.f.: (1-2^(-s) + 2*4^(-s))*zeta^2(s).",
				"G.f.: Sum_n (1 + cos(n*Pi/2)) x^n / (1 - x^n). - Domenico (domenicoo(AT)gmail.com), Oct 21 2009",
				"If 4|n then a(n) = d(n) - d(n/2) + 2*d(n/4); else if 2|n then a(n) = d(n) - d(n/2); else a(n) = d(n); where d(n) is the number of divisors of n. [Rutherford] - _Andrey Zabolotskiy_, Mar 10 2018",
				"a(n) = Sum_{ m: m^2|n } A060594(n/m^2). - _Andrey Zabolotskiy_, May 07 2018",
				"Sum_{k=1..n} a(k) ~ n*(log(n) - 1 + 2*gamma - log(2)/2), where gamma is the Euler-Mascheroni constant A001620. - _Vaclav Kotesovec_, Feb 02 2019"
			],
			"maple": [
				"nmax := 100 :",
				"L := [1,-1,0,2,seq(0,i=1..nmax)] :",
				"MOBIUSi(%) :",
				"MOBIUSi(%) ; # _R. J. Mathar_, Sep 25 2017"
			],
			"mathematica": [
				"m = 101; Drop[ CoefficientList[ Series[ Sum[(1 + Cos[n*Pi/2])*x^n/(1 - x^n), {n, 1, m}], {x, 0, m}], x], 1] (* _Jean-François Alcover_, Sep 20 2011, after formula *)"
			],
			"program": [
				"(PARI) t1=direuler(p=2,200,1/(1-X)^2)",
				"t2=direuler(p=2,2,1-X+2*X^2,200)",
				"t3=dirmul(t1,t2)"
			],
			"xref": [
				"Cf. A098178, A060594 (primitive sublattices only), A145391."
			],
			"keyword": "nonn,mult",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_, Feb 23 2009, Mar 13 2009",
			"ext": [
				"New name from _Andrey Zabolotskiy_, Mar 10 2018"
			],
			"references": 6,
			"revision": 28,
			"time": "2019-02-02T03:12:15-05:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}