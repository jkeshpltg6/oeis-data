{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A343601",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 343601,
			"data": "0,1,2,3,4,7,6,5,8,9,12,21,10,13,22,19,14,23,18,15,24,11,16,25,20,17,26,27,36,63,30,37,64,57,38,65,28,39,66,31,40,67,58,41,68,55,42,69,32,43,70,59,44,71,54,45,72,33,46,73,60,47,74,29,48,75,34,49",
			"name": "For any positive number n, the ternary representation of a(n) is obtained by right-rotating the ternary representation of n until a nonzero digit appears again as the leftmost digit; a(0) = 0.",
			"comment": [
				"This sequence is a permutation of the nonnegative integers with inverse A343600."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A343601/b343601.txt\"\u003eTable of n, a(n) for n = 0..6561\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"A053735(a(n)) = A053735(n).",
				"A081604(a(n)) = A081604(n).",
				"a^k(n) = n for k = A160384(n) (where a^k denotes the k-th iterate of a)."
			],
			"example": [
				"The first terms, in base 10 and in base 3, are:",
				"  n   a(n)  ter(n)  ter(a(n))",
				"  --  ----  ------  ---------",
				"   0     0       0          0",
				"   1     1       1          1",
				"   2     2       2          2",
				"   3     3      10         10",
				"   4     4      11         11",
				"   5     7      12         21",
				"   6     6      20         20",
				"   7     5      21         12",
				"   8     8      22         22",
				"   9     9     100        100",
				"  10    12     101        110",
				"  11    21     102        210",
				"  12    10     110        101",
				"  13    13     111        111",
				"  14    22     112        211"
			],
			"program": [
				"(PARI) a(n, base=3) = { my (d=digits(n, base)); forstep (k=#d, 2, -1, if (d[k], return (fromdigits(concat(d[k..#d], d[1..k-1]), base)))); n }"
			],
			"xref": [
				"Cf. A053735, A081604, A139706 (binary variant), A160384, A343600 (inverse)."
			],
			"keyword": "nonn,base",
			"offset": "0,3",
			"author": "_Rémy Sigrist_, Apr 21 2021",
			"references": 4,
			"revision": 12,
			"time": "2021-04-23T01:20:31-04:00",
			"created": "2021-04-22T21:57:24-04:00"
		}
	]
}