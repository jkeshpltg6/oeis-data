{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A053669",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 53669,
			"data": "2,3,2,3,2,5,2,3,2,3,2,5,2,3,2,3,2,5,2,3,2,3,2,5,2,3,2,3,2,7,2,3,2,3,2,5,2,3,2,3,2,5,2,3,2,3,2,5,2,3,2,3,2,5,2,3,2,3,2,7,2,3,2,3,2,5,2,3,2,3,2,5,2,3,2,3,2,5,2,3,2,3,2,5,2,3,2,3,2,7,2,3,2,3,2,5,2,3,2,3,2,5,2,3,2",
			"name": "Smallest prime not dividing n.",
			"comment": [
				"Smallest prime coprime to n.",
				"Smallest k \u003e= 2 coprime to n.",
				"a(#(p-1)) = a(A034386(p-1)) = p is the first appearance of prime p in sequence.",
				"a(A005408(n)) = 2; for n \u003e 2: a(n) = A112484(n,1). - _Reinhard Zumkeller_, Sep 23 2011",
				"Average value is 2.920050977316134... = A249270. - _Charles R Greathouse IV_, Nov 02 2013",
				"Differs from A236454, \"smallest number not dividing n^2\", for the first time at n=210, where a(210)=11 while A236454(210)=8. A235921 lists all n for which a(n) differs from A236454. - _Antti Karttunen_, Jan 26 2014"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A053669/b053669.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Dylan Fridman, Juli Garbulsky, Bruno Glecer, James Grime and Massi Tron Florentin, \u003ca href=\"https://doi.org/10.1080/00029890.2019.1530554\"\u003eA Prime-Representing Constant\u003c/a\u003e, The American Mathematical Monthly, Vol. 126, No. 1 (2019), pp. 70-73; \u003ca href=\"https://www.researchgate.net/publication/330746181_A_Prime-Representing_Constant\"\u003eResearchGate link\u003c/a\u003e.",
				"James Grime and Brady Haran, \u003ca href=\"https://www.youtube.com/watch?v=_gCKX6VMvmU\u0026amp;t=347s\"\u003e2.920050977316\u003c/a\u003e, Numberphile video (2020)",
				"Igor Rivin, \u003ca href=\"https://doi.org/10.1016/j.aim.2012.07.018\"\u003eGeodesics with one self-intersection, and other stories\u003c/a\u003e, Advances in Mathematics, Vol. 231, No. 5 (2012), pp. 2391-2412; \u003ca href=\"http://arxiv.org/abs/0901.2543\"\u003earXiv preprint\u003c/a\u003e, arXiv:0901.2543 [math.GT], 2009-2011."
			],
			"formula": [
				"a(n) = A071222(n-1)+1. [Because the right hand side computes the smallest k \u003e= 2 such that gcd(n,k) = gcd(n-1,k-1) which is equal to the smallest k \u003e= 2 coprime to n] - _Antti Karttunen_, Jan 26 2014",
				"a(n) = 1 + Sum_{k=1..n}(floor((n^k)/k!)-floor(((n^k)-1)/k!)) = 2 + Sum_{k=1..n} A001223(k)*( floor(n/A002110(k))-floor((n-1)/A002110(k)) ). - _Anthony Browne_, May 11 2016",
				"a(n!) = A151800(n). - _Anthony Browne_, May 11 2016",
				"a(2k+1) = 2. - _Bernard Schott_, Jun 03 2019",
				"Asymptotic mean: lim_{n-\u003eoo} (1/n) * Sum_{k=1..n} a(k) =  A249270. - _Amiram Eldar_, Oct 29 2020"
			],
			"example": [
				"a(60) = 7, since all primes smaller than 7 divide 60 but 7 does not."
			],
			"maple": [
				"f:= proc(n) local p;",
				"p:= 2;",
				"while n mod p = 0 do p:= nextprime(p) od:",
				"p",
				"end proc:",
				"map(f, [$1..100]); # _Robert Israel_, May 18 2016"
			],
			"mathematica": [
				"Table[k := 1; While[Not[GCD[n, Prime[k]] == 1], k++ ]; Prime[k], {n, 1, 60}] (* _Stefan Steinerberger_, Apr 01 2006 *)",
				"With[{prs=Prime[Range[10]]},Flatten[Table[Select[prs,!Divisible[ n,#]\u0026,1],{n,110}]]] (* _Harvey P. Dale_, May 03 2012 *)"
			],
			"program": [
				"(Haskell)",
				"a053669 n = head $ dropWhile ((== 0) . (mod n)) a000040_list",
				"-- _Reinhard Zumkeller_, Nov 11 2012",
				"(PARI) a(n)=forprime(p=2,,if(n%p,return(p))) \\\\ _Charles R Greathouse IV_, Nov 20 2012",
				"(Scheme) (define (A053669 n) (let loop ((i 1)) (cond ((zero? (modulo n (A000040 i))) (loop (+ i 1))) (else (A000040 i))))) ;; _Antti Karttunen_, Jan 26 2014",
				"(Python)",
				"from sympy import nextprime",
				"def a(n):",
				"    p = 2",
				"    while True:",
				"        if n%p: return p",
				"        else: p=nextprime(p) # _Indranil Ghosh_, May 12 2017",
				"(Python)",
				"# using standard library functions only",
				"import math",
				"def a(n):",
				"    k = 2",
				"    while math.gcd(n,k) \u003e 1: k += 1",
				"    return k # _Ely Golden_, Nov 26 2020"
			],
			"xref": [
				"One more than A071222(n-1).",
				"Cf. also A053670-A053674, A055874, A071222, A235921, A236454, A249270.",
				"Cf. A079578, A087560, A096014."
			],
			"keyword": "nonn,nice,easy",
			"offset": "1,1",
			"author": "_Henry Bottomley_, Feb 15 2000",
			"ext": [
				"More terms from Andrew Gacek (andrew(AT)dgi.net), Feb 21 2000 and _James A. Sellers_, Feb 22 2000",
				"Entry revised by _David W. Wilson_, Nov 25 2006"
			],
			"references": 82,
			"revision": 90,
			"time": "2021-08-02T04:56:21-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}