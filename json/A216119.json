{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A216119",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 216119,
			"data": "0,0,0,2,30,360,4200,50400,635040,8467200,119750400,1796256000,28540512000,479480601600,8499883392000,158664489984000,3112264995840000,64023737057280000,1378644471300096000,31019500604252160000,728045925946859520000,17796678189812121600000",
			"name": "Number of stretching pairs in all permutations in S_n.",
			"comment": [
				"A stretching pair of a permutation p in S_n is a pair (i,j) (1 \u003c= i \u003c j \u003c= n) satisfying p(i) \u003c i \u003c j \u003c p(j). For example, for the permutation 31254 in S_5 the pair (2,4) is stretching because p(2) = 1 \u003c 2 \u003c 4 \u003c p(4) = 5."
			],
			"reference": [
				"E. Lundberg and B. Nagle, A permutation statistic arising in dynamics of internal maps. (submitted)"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A216119/b216119.txt\"\u003eTable of n, a(n) for n = 1..450\u003c/a\u003e",
				"E. Clark and R. Ehrenborg, \u003ca href=\"http://dx.doi.org/10.1016/j.ejc.2008.11.014\"\u003eExplicit expressions for the extremal excedance statistic\u003c/a\u003e, European J. Combinatorics, 31, 2010, 270-279.",
				"J. Cooper, E. Lundberg, and B. Nagle, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v20i1p28\"\u003eGeneralized pattern frequency in large permutations\u003c/a\u003e, Electron. J. Combin. 20, 2013, #P28."
			],
			"formula": [
				"a(n) = n! (n-2)(n-3)/24.",
				"a(n) = 2*A005461(n-3).",
				"a(n) = Sum_{k\u003e=1} A216118(k).",
				"a(n) = Sum_{k\u003e=1} k*A216120(n,k)."
			],
			"example": [
				"a(4) = 2 because 2143 has 1 stretching (namely (2,3)), 3142 has 1 stretching pair (namely (2,3)), and the other 22 permutations in S_4 have no stretching pairs."
			],
			"maple": [
				"0, seq((1/24)*factorial(n)*(n-2)*(n-3), n = 2 .. 22);"
			],
			"mathematica": [
				"Join[{0}, Table[n! (n - 2) (n - 3) / 24, {n, 2, 30}]] (* _Vincenzo Librandi_, Nov 29 2018 *)"
			],
			"program": [
				"(MAGMA) [Factorial(n)*(n-2)*(n-3) div 24: n in [1..30]]; // _Vincenzo Librandi_, Nov 29 2018",
				"(GAP) Concatenation([0],List([2..22],n-\u003eFactorial(n)*(n-2)*(n-3)/24)); # _Muniru A Asiru_, Nov 29 2018"
			],
			"xref": [
				"Cf. A005461, A216118, A216120."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Emeric Deutsch_, Feb 26 2013",
			"references": 3,
			"revision": 19,
			"time": "2018-11-29T16:01:30-05:00",
			"created": "2013-02-26T18:47:16-05:00"
		}
	]
}