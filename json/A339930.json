{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A339930",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 339930,
			"data": "0,0,0,1,1,1,1,2,1,2,2,2,2,2,3,2,3,2,3,3,2,4,2,4,2,4,3,3,4,3,4,3,3,5,2,5,2,5,3,4,5,3,5,3,4,4,4,5,3,6,3,5,3,4,6,4,6,4,5,4",
			"name": "a(n+1) = a(n-2-a(n)^2) + 1, starting with a(1) = a(2) = a(3) = 0.",
			"comment": [
				"To obtain the next term, square the current term and add 2, then count back this number and add 1.",
				"The sequence cannot repeat. Proof: Assume a finite period. Label an arbitrary term in the period x. Because of the back-referencing definition it follows that x-1 has to be in the period, and by the same argument so does x-2 and x-3, x-4,... until 0. But it is not possible to obtain new 0s since each new term is larger than one already existing term.",
				"Every positive integer appears in the sequence.",
				"First occurrence of n: 1, 4, 8, 15, 22, 34, 50, 69, 108, 171, 210, 277, 376, 464, 567, 670, 775, 993,...",
				"The sequence appears to grow with the cube root of n, which is expected since f(x) = (3*x)^(1/3) satisfies the definition for large x, i.e. lim_{x-\u003eoo} f(x+1)-(f(x-2-f(x)^2)+1) = 0.",
				"The width of the distribution of terms within a range (n^2,n^2+n) appears to be constant for large n and can be defined as: lim_{n-\u003eoo} ( 1/n*Sum_{k=n..2n} ( Max_{i=k^2..k^2+k} a(i) - Min_{i=k^2..k^2+k} a(i) ) ) and evaluates to 7.41... (for n^2 = 5*10^8)."
			],
			"formula": [
				"a(n) ~ (3*n)^(1/3) (conjectured)."
			],
			"example": [
				"a(4) = a(3-2-a(3)^2)+1 = a(1)+1 = 1.",
				"a(5) = a(4-2-a(4)^2)+1 = a(1)+1 = 1.",
				"a(6) = a(5-2-a(5)^2)+1 = a(2)+1 = 1.",
				"a(7) = a(6-2-a(6)^2)+1 = a(3)+1 = 1.",
				"a(8) = a(7-2-a(7)^2)+1 = a(4)+1 = 2."
			],
			"program": [
				"(Python)",
				"a = [0, 0, 0]",
				"for n in range(2, 1000):",
				"    a.append(a[n-2-a[n]**2]+1)",
				"(C)",
				"#include\u003cstdio.h\u003e",
				"#include\u003cstdlib.h\u003e",
				"int main(void){",
				"    int N = 1000;",
				"    int *a = (int*)malloc(N*sizeof(int));",
				"    a[0] = 0;",
				"    a[1] = 0;",
				"    a[2] = 0;",
				"    for(int n = 2; n \u003c N-1; ++n){",
				"        a[n+1] = a[n-2-a[n]*a[n]]+1;",
				"    }",
				"    free(a);",
				"    return 0;",
				"}"
			],
			"xref": [
				"Analogous sequences: A339929, A339931, A339932.",
				"Cf. A330772, A005206, A002516, A062039."
			],
			"keyword": "nonn",
			"offset": "1,8",
			"author": "_Rok Cestnik_, Dec 23 2020",
			"references": 3,
			"revision": 11,
			"time": "2020-12-27T19:45:20-05:00",
			"created": "2020-12-27T19:45:20-05:00"
		}
	]
}