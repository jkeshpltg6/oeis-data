{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A237040",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 237040,
			"data": "9,65,217,4097,5833,10649,21953,74089,195113,216001,343001,373249,474553,1000001,1061209,1191017,1404929,3241793,3796417,4251529,6859001,9261001,12487169,21952001,29791001,35937001,43614209,45882713,55742969,62099137,89915393,94818817,117649001",
			"name": "Semiprimes of the form k^3 + 1.",
			"comment": [
				"k^3 + 1 is a term iff k + 1 and k^2 - k + 1 are both prime.",
				"Is the sequence infinite? This is an analog of Landau's 4th problem, namely, are there infinitely many primes of the form k^2 + 1?",
				"In other words: are there infinitely many primes p such that p^2 - 3*p + 3 is also prime? - _Charles R Greathouse IV_, Jul 02 2017"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A237040/b237040.txt\"\u003eTable of n, a(n) for n = 1..1400\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Semiprime.html\"\u003eSemiprime\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Semiprime\"\u003eSemiprime\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Landau\u0026#39;s_problems\"\u003eLandau's problems\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A096173(n)^3 + 1 = 8*A237037(n)^3 + 1."
			],
			"example": [
				"9 = 3*3 = 2^3 + 1 is the first semiprime of the form n^3 + 1, so a(1) = 9."
			],
			"mathematica": [
				"L = Select[Range[500], PrimeQ[# + 1] \u0026\u0026 PrimeQ[#^2 - # + 1] \u0026]; L^3 + 1",
				"Select[Range[50]^3 + 1, PrimeOmega[#] == 2 \u0026] (* _Zak Seidov_, Jun 26 2017 *)"
			],
			"program": [
				"(PARI) lista(nn) = for (n=1, nn, if (bigomega(sp=n^3+1) == 2, print1(sp, \", \"));); \\\\ _Michel Marcus_, Jun 27 2017",
				"(PARI) list(lim)=my(v=List(),n,t); forprime(p=3,sqrtnint(lim\\1-1,3)+1, if(isprime(t=p^2-3*p+3), listput(v,t*p))); Vec(v) \\\\ _Charles R Greathouse IV_, Jul 02 2017",
				"(MAGMA) IsSemiprime:= func\u003cn | \u0026+[d[2]: d in Factorization(n)] eq 2\u003e; [s: n in [1..500] | IsSemiprime(s) where s is n^3 + 1]; // _Vincenzo Librandi_, Jul 02 2017"
			],
			"xref": [
				"Cf. A001358, A002383, A002496, A046315, A081256, A096173, A096174, A237037, A237038, A237039.",
				"Cf. A242262 (semiprimes of the form k^3 - 1)."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Jonathan Sondow_, Feb 02 2014",
			"references": 14,
			"revision": 22,
			"time": "2018-08-01T05:57:00-04:00",
			"created": "2014-02-03T03:30:16-05:00"
		}
	]
}