{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A281701",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 281701,
			"data": "1,3,7,28",
			"name": "a(n) is the largest number of coins obtainable by making repeated moves in this puzzle: Start with 1 coin in each of n boxes B(i), i=1..n. One can iterate moves of two types: (1) remove a coin from a nonempty B(i) (i \u003c= n-1) and place two coins in B(i+1); (2) remove a coin from a nonempty B(i) (i \u003c= n-2) and switch the contents of B(i+1) and B(i+2).",
			"comment": [
				"An Ackermann-like function. The underlying puzzle was invented by Hans Zantema. The derivation and proof of the general formula involving a palindromic sequence of up-arrows is by Richard Stong.",
				"The next term is too large to include (2^16385, it has 4933 digits)."
			],
			"link": [
				"Zuming Feng, Po-Shen Loh, and Yi Sun, \u003ca href=\"http://yisun.io/papers/imo2010.pdf\"\u003e51st International Mathematical Olympiad\u003c/a\u003e, Math. Mag. 83 (2010), pp. 320-323.",
				"Terence Tao, \u003ca href=\"https://polymathprojects.org/2010/07/08/minipolymath2-project-imo-2010-q5/\"\u003eMinipolymath2 project: IMO 2010 Q5\u003c/a\u003e (2010)",
				"A. van den Brandhof, J. Guichelaar, and A. Jaspers, \u003ca href=\"http://www.maa.org/press/ebooks/half-a-century-of-pythagoras-magazine\"\u003eHalf a Century of Pythagoras Magazine\u003c/a\u003e, MAA, 2015, 225",
				"Stan Wagon, \u003ca href=\"http://mathforum.org/wagon/2017/p1233.html\"\u003eThe Generous Automated Teller Machine\u003c/a\u003e",
				"Stan Wagon, \u003ca href=\"/A281701/a281701.pdf\"\u003eRichard Stong's proof of the uparrow formula\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Knuth\u0026#39;s_up-arrow_notation\"\u003eKnuth's up-arrow notation\u003c/a\u003e"
			],
			"formula": [
				"Let f_n(x) = 2↑↑...↑x, with n Knuth up-arrows, so f_0(x) = 2x, f_1(x) = 2^x, f_2(x) = 2↑↑x = 2^2^...^2 with x copies of 2, etc.",
				"Let F_n be the composition of f_0, f_1,...,f_(n-4).",
				"Let G_n be the same composition but in the opposite order.",
				"Then a(n) = G_n(F_n(7)), a formula due to Richard Stong."
			],
			"example": [
				"a(5) = f_0(f_1(f_1(f_0(7)))) = 2*2^(2^(2*7)) = 2*2^(2^14) = 2^16385."
			],
			"xref": [
				"Cf. A307611."
			],
			"keyword": "nonn,nice",
			"offset": "1,2",
			"author": "_Stan Wagon_, Jan 27 2017",
			"references": 2,
			"revision": 32,
			"time": "2019-04-19T10:33:42-04:00",
			"created": "2017-01-27T22:08:35-05:00"
		}
	]
}