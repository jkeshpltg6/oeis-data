{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A348576",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 348576,
			"data": "0,1,0,1,3,0,1,10,12,0,1,25,80,60,0,1,56,360,660,360,0,1,119,1372,4620,5880,2520,0,1,246,4788,26376,58800,57120,20160,0,1,501,15864,134316,466704,771120,604800,181440,0,1,1012,50880,637020,3238200,8094240,10584000,6955200,1814400,0",
			"name": "Triangle read by rows: T(n,k) is the number of ordered partitions of [n] into k nonempty subsets, in which the first subset has size at least 2, n \u003e= 1 and 1 \u003c= k \u003c= n.",
			"comment": [
				"Ordered partitions are also referred to as weak orders."
			],
			"link": [
				"D. Galvin, G. Wesley and B. Zacovic, \u003ca href=\"https://arxiv.org/abs/2110.08953\"\u003eEnumerating threshold graphs and some related graph classes\u003c/a\u003e, arXiv:2110.08953 [math.CO], 2021."
			],
			"formula": [
				"T(n,k) = Sum_{j=1..n-1} (n-j)*A173018(n-1, j-1)*binomial(j-1, n-k-1)."
			],
			"example": [
				"For n=3, the ordered partitions of {1,2,3} in which the first block has size at least 2 are 123, 12/3, 13/2 and 23/1, so T(3,1)=1, T(3,2)=3 and T(3,3)=0.",
				"Triangle begins:",
				"  0;",
				"  1,     0;",
				"  1,     3,     0;",
				"  1,    10,    12,       0;",
				"  1,    25,    80,      60,       0;",
				"  1,    56,   360,     660,     360,       0;",
				"  1,   119,  1372.    4620,    5880,    2520,        0;",
				"  1,   246,  4788,   26376,   58800,   57120,    20160,        0;",
				"  1,   501, 15864,  134316,  466704,  771120,   604800,   181440,       0;",
				"  1,  1012, 50880,  637020, 3238200, 8094240, 10584000,  6955200, 1814400, 0;",
				"  ..."
			],
			"maple": [
				"b:= proc(n, t) option remember; expand(`if`(n=0, 1,",
				"      add(x*b(n-j, 1)*binomial(n, j), j=t..n)))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=1..n))(b(n, 2)):",
				"seq(T(n), n=1..10);  # _Alois P. Heinz_, Oct 24 2021"
			],
			"mathematica": [
				"eulerian[n_,m_] := eulerian[n,m] =",
				"  Sum[((-1)^k)*Binomial[n+1,k]*((m+1-k)^n), {k,0,m+1}] (* eulerian[n, m] is an Eulerian number, counting the permutations of [n] with m descents *);",
				"op2[n_,k_] := op2[n,k] =",
				"   Sum[(n-j)*eulerian[n-1,j-1]*Binomial[j-1,n-k-1], {j,1,n-1}] (* op2[n,k] counts ordered partitions on [n] with k parts, with first part having size at least 2 *); Table[op2[n, k],{n,1,12},{k,1,n}]"
			],
			"program": [
				"(PARI) TE(n, k) = sum(j=0, k, (-1)^j * (k-j)^n * binomial( n+1, j)); \\\\ A008292",
				"T(n,k) = sum(j=1, n-1, (n-j)*TE(n-1,j)*binomial(j-1,n-k-1)); \\\\ _Michel Marcus_, Oct 24 2021"
			],
			"xref": [
				"Row sums are A053525.",
				"Cf. A173018, A000247, A001710, A131689."
			],
			"keyword": "nonn,tabl,changed",
			"offset": "1,5",
			"author": "_David Galvin_, Oct 23 2021",
			"references": 1,
			"revision": 32,
			"time": "2022-01-03T16:29:31-05:00",
			"created": "2021-10-25T03:22:00-04:00"
		}
	]
}