{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A076479",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 76479,
			"data": "1,-1,-1,-1,-1,1,-1,-1,-1,1,-1,1,-1,1,1,-1,-1,1,-1,1,1,1,-1,1,-1,1,-1,1,-1,-1,-1,-1,1,1,1,1,-1,1,1,1,-1,-1,-1,1,1,1,-1,1,-1,1,1,1,-1,1,1,1,1,1,-1,-1,-1,1,1,-1,1,-1,-1,1,1,-1,-1,1,-1,1,1,1,1,-1,-1,1,-1,1,-1,-1,1,1,1",
			"name": "a(n) = mu(rad(n)), where mu is the Moebius-function (A008683) and rad is the radical or squarefree kernel (A007947).",
			"comment": [
				"Multiplicative: a(1) = 1, a(n) for n \u003e=2 is sign of parity of number of distinct primes dividing n. a(p) = -1, a(pq) = 1, a(pq...z) = (-1)^k, a(p^k) = -1, where p,q,.. z are distinct primes and k natural numbers. - _Jaroslav Krizek_, Mar 17 2009",
				"a(n) is the unitary Moebius function, i.e., the inverse of the constant 1 function under the unitary convolution defined by (f X g)(n)= sum of f(d)g(n/d), where the sum is over the unitary divisors d of n (divisors d of n such that gcd(d,n/d)=1). - _Laszlo Toth_, Oct 08 2009"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A076479/b076479.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Eckford Cohen, \u003ca href=\"https://doi.org/10.1007/BF01180473\"\u003eArithmetical functions associated with the unitary divisors of an integer\u003c/a\u003e, Math. Zeitschr., Vol. 74 (1960), pp. 66-80.",
				"Jan van de Lune and Robert E. Dressler, \u003ca href=\"https://doi.org/10.1515/crll.1975.277.117\"\u003eSome theorems concerning the number theoretic function omega(n)\u003c/a\u003e, Journal für die reine und angewandte Mathematik, Vol. 277 (1975), pp. 117-119; \u003ca href=\"https://eudml.org/doc/151630\"\u003ealternative link\u003c/a\u003e."
			],
			"formula": [
				"a(n) = A008683(A007947(n)).",
				"a(n) = (-1)^A001221(n). Multiplicative with a(p^e) = -1. - _Vladeta Jovovic_, Dec 03 2002",
				"a(n) = sign(A180403(n)). - _Mats Granvik_, Oct 08 2010",
				"Sum_{n\u003e=1} a(n)*phi(n)/n^3 = A065463 with phi()=A000010() [Cohen, Lemma 3.5]. - _R. J. Mathar_, Apr 11 2011",
				"Dirichlet convolution of A000012 with A226177 (signed variant of A074823 with one factor mu(n) removed). - _R. J. Mathar_, Apr 19 2011",
				"Sum_{n\u003e=1} a(n)/n^2 = A065469. - _R. J. Mathar_, Apr 19 2011",
				"a(n) = Sum_{d|n} mu(d)*tau_2(d) = Sum_{d|n} A008683(d)*A000005(d) . - _Enrique Pérez Herrero_, Jan 17 2013",
				"a(A030230(n)) = -1; a(A030231(n)) = +1. - _Reinhard Zumkeller_, Jun 01 2013",
				"Dirichlet g.f.: zeta(s) * Product_{p prime} (1 - 2p^(-s)). - _Álvar Ibeas_, Dec 30 2018",
				"Sum_{n\u003e=1} a(n)/n = 0 (van de Lune and Dressler, 1975). - _Amiram Eldar_, Mar 05 2021",
				"From _Richard L. Ollerton_, May 07 2021: (Start)",
				"For n\u003e1, Sum_{k=1..n} a(gcd(n,k))*phi(gcd(n,k))*rad(gcd(n,k))/gcd(n,k) = 0.",
				"For n\u003e1, Sum_{k=1..n} a(n/gcd(n,k)))*phi(gcd(n,k))*rad(n/gcd(n,k))*gcd(n,k) = 0. (End)"
			],
			"maple": [
				"A076479 := proc(n)",
				"    (-1)^A001221(n) ;",
				"end proc:",
				"seq(A076479(n),n=1..80) ; # _R. J. Mathar_, Nov 02 2016"
			],
			"mathematica": [
				"Table[(-1)^PrimeNu[n], {n,50}] (* _Enrique Pérez Herrero_, Jan 17 2013 *)"
			],
			"program": [
				"(PARI)",
				"N=66;",
				"mu=vector(N); mu[1]=1;",
				"{ for (n=2,N,",
				"    s = 0;",
				"    fordiv (n,d,",
				"        if (gcd(d,n/d)!=1, next() ); /* unitary divisors only */",
				"        s += mu[d];",
				"    );",
				"    mu[n] = -s;",
				"); };",
				"mu /* _Joerg Arndt_, May 13 2011 */",
				"/* omitting the line if ( gcd(...)) gives the usual Moebius function */",
				"(PARI) a(n)=(-1)^omega(n) \\\\ _Charles R Greathouse IV_, Aug 02 2013",
				"(Haskell)",
				"a076479 = a008683 . a007947  -- _Reinhard Zumkeller_, Jun 01 2013",
				"(MAGMA) [(-1)^(#PrimeDivisors(n)): n in [1..100]]; // _Vincenzo Librandi_, Dec 31 2018",
				"(Python)",
				"from math import prod",
				"from sympy.ntheory import mobius, primefactors",
				"def A076479(n): return mobius(prod(primefactors(n))) # _Chai Wah Wu_, Sep 24 2021"
			],
			"xref": [
				"Cf. A000005, A000010, A001221, A007947, A008683, A008836, A030230, A065469, A076480, A180403, A226177.",
				"Cf. A174863 (partial sums)."
			],
			"keyword": "sign,mult",
			"offset": "1,1",
			"author": "_Reinhard Zumkeller_, Oct 14 2002",
			"references": 36,
			"revision": 59,
			"time": "2021-09-25T06:47:05-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}