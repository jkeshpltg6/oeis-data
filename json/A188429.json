{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A188429",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 188429,
			"data": "1,0,2,0,0,3,4,0,0,4,5,5,6,7,5,6,6,6,7,7,6,7,7,7,7,8,8,7,8,8,8,8,8,9,9,8,9,9,9,9,9,9,10,10,9,10,10,10,10,10,10,10,11,11,10,11,11,11,11,11,11,11,11,12,12,11,12,12,12,12,12,12,12,12,12,13,13,12,13,13",
			"name": "L(n) is the minimum of the largest elements of all n-full sets, or 0 if no such set exists.",
			"comment": [
				"Let A be a set of positive integers. We say that A is n-full if (sum A)=[n] for a positive integer n, where (sum A) is the set of all positive integers which are a sum of distinct elements of A and [n]={1,2,...,n}. The number L(n) denotes the minimum of the set {max A: (sum A)=[n] }.",
				"Terms m \u003e 7 occur exactly m times. - _Reinhard Zumkeller_, Aug 06 2015"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A188429/b188429.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Mohammad Saleh Dinparvar, \u003ca href=\"http://github.com/SalehDinparvar/sequence_computer/blob/master/A188429.py\"\u003ePython program\u003c/a\u003e",
				"L. Naranjani and M. Mirzavaziri, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL14/Mirzavaziri/mirza4.html\"\u003eFull Subsets of N\u003c/a\u003e, Journal of Integer Sequences, 14 (2011), Article 11.5.3."
			],
			"formula": [
				"for n\u003e= 15. Let n=k(k+1)/2+r, where r=0,1,..., k then",
				"       |k, if r=0",
				"L(n) = |k+1, if 1 \u003c= r \u003c= k-2",
				"       |k+2, if k-1 \u003c= r \u003c= k."
			],
			"example": [
				"From _Reinhard Zumkeller_, Aug 06 2015: (Start)",
				"Compressed table: no commas and for a and k: 10 replaced by A, 11 by B.",
				". -----------------------------------------------------------------------------",
				".   n   1   5   10   15   20   25   30   35   40   45   50   55   60   65   70",
				". ----  .---.----.----.----.----.----.----.----.----.----.----.----.----.----.-",
				". t(n)  10100100010000100000100000010000000100000000100000000010000000000100000",
				". k(n)  1 2  3   4    5     6      7       8        9         A          B",
				". r(n)  0101201230123401234501234560123456701234567801234567890123456789A012345",
				". ----  -----------------------------------------------------------------------",
				". a(n)  102003400455675666776777788788888998999999AA9AAAAAAABBABBBBBBBBCCBCCCCC",
				". -----------------------------------------------------------------------------",
				"where t(n)=A010054(n), k(n)=A127648(n) zeros blanked, and r(n)=A002262(n). (End)"
			],
			"mathematica": [
				"kr[n_] := {k, r} /. ToRules[Reduce[0 \u003c= r \u003c= k \u0026\u0026 n == k*((k+1)/2)+r, {k, r}, Integers]]; L[n_] := Which[{k0, r0} = kr[n]; r0 == 0, k0, 1 \u003c= r0 \u003c= k0-2, k0+1, k0-1 \u003c= r0 \u003c= k0, k0+2]; Join[{1, 0, 2, 0, 0, 3, 4, 0, 0, 4, 5, 5, 6, 7}, Table[L[n], {n, 15, 80}]] (* _Jean-François Alcover_, Oct 10 2015 *)"
			],
			"program": [
				"(Haskell)",
				"a188429 n = a188429_list !! (n-1)",
				"a188429_list = [1, 0, 2, 0, 0, 3, 4, 0, 0, 4, 5, 5, 6, 7] ++",
				"               f [15 ..] (drop 15 a010054_list) 0 4",
				"   where f (x:xs) (t:ts) r k | t == 1    = (k + 1) : f xs ts 1 (k + 1)",
				"                             | r \u003c k - 1 = (k + 1) : f xs ts (r + 1) k",
				"                             | otherwise = (k + 2) : f xs ts (r + 1) k",
				"-- _Reinhard Zumkeller_, Aug 06 2015"
			],
			"xref": [
				"Cf. A188430, A188431.",
				"Cf. A010054, A127648, A002262."
			],
			"keyword": "nonn,nice",
			"offset": "1,3",
			"author": "_Madjid Mirzavaziri_, Mar 31 2011",
			"references": 3,
			"revision": 37,
			"time": "2020-05-11T02:38:51-04:00",
			"created": "2011-04-04T08:41:57-04:00"
		}
	]
}