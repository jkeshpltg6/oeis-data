{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A024359",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 24359,
			"data": "0,0,1,0,1,0,1,1,1,0,1,1,1,0,1,1,1,0,1,2,1,0,1,1,1,0,1,2,1,0,1,1,2,0,1,2,1,0,2,1,1,0,1,2,1,0,1,2,1,0,2,2,1,0,1,1,2,0,1,3,1,0,1,1,2,0,1,2,2,0,1,1,1,0,2,2,1,0,1,1,1,0,1,3,2,0,2",
			"name": "Number of primitive Pythagorean triangles with short leg n.",
			"comment": [
				"Consider primitive Pythagorean triangles (A^2 + B^2 = C^2, (A, B) = 1, A \u003c= B); sequence gives number of times A takes value n.",
				"Number of times n occurs in A020884.",
				"a(A139544(n)) = 0; a(A024352(n)) \u003e 0. - _Reinhard Zumkeller_, Nov 09 2012"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A024359/b024359.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Ron Knott, \u003ca href=\"http://www.maths.surrey.ac.uk/hosted-sites/R.Knott/Pythag/pythag.html\"\u003ePythagorean Triples and Online Calculators\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A024361(n) - A024360(n). - _Ray Chandler_, Feb 03 2020"
			],
			"mathematica": [
				"solns[a_] := Module[{b, c, soln}, soln = Reduce[a^2 + b^2 == c^2 \u0026\u0026 a \u003c b \u0026\u0026 c \u003e 0 \u0026\u0026 GCD[a, b, c] == 1, {b, c}, Integers]; If[soln === False, 0, If[soln[[1, 1]] === b, 1, Length[soln]]]]; Table[solns[n], {n, 100}]",
				"(* Second program: *)",
				"a[n_] := Module[{s = 0, b, c, d, g}, Do[g = Quotient[n^2, d]; If[d \u003c= g \u0026\u0026 Mod[d+g, 2] == 0, c = Quotient[d+g, 2]; b = g-c; If[n \u003c b \u0026\u0026 GCD[b, c] == 1, s++]], {d, Divisors[n^2]}]; s]; Array[a, 100] (* _Jean-François Alcover_, Apr 27 2019, from PARI *)"
			],
			"program": [
				"(Haskell)",
				"a024359_list = f 0 1 a020884_list where",
				"   f c u vs'@(v:vs) | u == v = f (c + 1) u vs",
				"                    | u /= v = c : f 0 (u + 1) vs'",
				"-- _Reinhard Zumkeller_, Nov 09 2012",
				"(PARI)",
				"nppt(a) = {",
				"  my(s=0, b, c, d, g);",
				"  fordiv(a^2, d,",
				"    g=a^2\\d;",
				"    if(d\u003c=g \u0026\u0026 (d+g)%2==0,",
				"      c=(d+g)\\2; b=g-c;",
				"      if(a\u003cb \u0026\u0026 gcd(b, c)==1, s++)",
				"    )",
				"  );",
				"  s",
				"}",
				"vector(100, n, nppt(n)) \\\\ _Colin Barker_, Oct 25 2015"
			],
			"xref": [
				"Cf. A020884, A024352, A024360, A024361, A132404 (where records occur), A139544."
			],
			"keyword": "nonn",
			"offset": "1,20",
			"author": "_David W. Wilson_",
			"references": 8,
			"revision": 29,
			"time": "2020-02-03T11:45:40-05:00",
			"created": "1998-06-14T03:00:00-04:00"
		}
	]
}