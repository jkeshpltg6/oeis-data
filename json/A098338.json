{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A098338",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 98338,
			"data": "1,3,7,9,-21,-207,-911,-2769,-5213,2457,74997,400491,1409109,3323583,2219343,-27453951,-186624333,-750905127,-2088947819,-2955863589,8506703569,86421384387,401183114163,1280139325101,2522745571021",
			"name": "Expansion of 1/sqrt(1-6x+13x^2).",
			"comment": [
				"Binomial transform of A098335. Second binomial transform of A098331.",
				"Central coefficients of (1+3x-x^2)^n."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A098338/b098338.txt\"\u003eTable of n, a(n) for n = 0..1796\u003c/a\u003e",
				"Hacène Belbachir, Abdelghani Mehdaoui, László Szalay, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL22/Szalay/szalay42.html\"\u003eDiagonal Sums in the Pascal Pyramid, II: Applications\u003c/a\u003e, J. Int. Seq., Vol. 22 (2019), Article 19.3.5.",
				"Robert Israel, \u003ca href=\"/A098338/a098338.png\"\u003ePlot of a(n) sqrt(n)/13^(n/2) for 1\u003c=n\u003c=10000\u003c/a\u003e.",
				"Tony D. Noe, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL9/Noe/noe35.html\"\u003eOn the Divisibility of Generalized Central Trinomial Coefficients\u003c/a\u003e, Journal of Integer Sequences, Vol. 9 (2006), Article 06.2.7."
			],
			"formula": [
				"E.g.f.: exp(3*x)*BesselI(0, 2*I*x), I=sqrt(-1).",
				"a(n) = Sum{k=0..floor(n/2)} binomial(n, k)*binomial(n-k, k)*3^n*(-9)^(-k).",
				"a(n) = Sum{k=0..floor(n/2)} binomial(n, 2k)*binomial(2k, k)*3^n*(-9)^(-k).",
				"D-finite with recurrence: n*a(n) +3*(1-2*n)*a(n-1) +13*(n-1)*a(n-2)=0. - _R. J. Mathar_, Sep 26 2012",
				"Recurrence follows from the differential equation (13x-3) g(x) + (13x^2-6x+1) g'(x) = 0 satisfied by the generating function. - _Robert Israel_, Mar 02 2017",
				"Lim sup n-\u003einfinity |a(n)|^(1/n) = sqrt(13). - _Vaclav Kotesovec_, Sep 29 2013"
			],
			"maple": [
				"f:= gfun:-rectoproc({(13*n+13)*a(n)+(-9-6*n)*a(n+1)+(n+2)*a(n+2), a(0)=1, a(1)=3},a(n),remember):",
				"map(f, [$0..50]); # _Robert Israel_, Mar 02 2017"
			],
			"mathematica": [
				"CoefficientList[Series[1/Sqrt[1-6*x+13*x^2], {x, 0, 20}], x] (* _Vaclav Kotesovec_, Sep 29 2013 *)"
			],
			"keyword": "easy,sign",
			"offset": "0,2",
			"author": "_Paul Barry_, Sep 03 2004",
			"references": 1,
			"revision": 29,
			"time": "2020-01-30T21:29:15-05:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}