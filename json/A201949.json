{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A201949",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 201949,
			"data": "1,1,0,1,1,1,2,1,1,1,3,5,6,5,3,1,1,6,15,24,28,24,15,6,1,1,10,40,90,139,160,139,90,40,10,1,1,15,91,300,629,945,1078,945,629,300,91,15,1,1,21,182,861,2520,5019,7377,8358,7377,5019,2520,861,182,21,1,1,28,330,2156,8729,23520,45030,65016,73260,65016",
			"name": "Triangle, read by rows, where the g.f. of row n equals Product_{k=0..n-1} (1 + k*y + y^2) for n\u003e0 with a single '1' in row 0.",
			"comment": [
				"The formula for the main diagonal, BesselI(0, 2*log(1 - x)), was found by _Ilya Gutkovskiy_ (see A201950). - _Paul D. Hanna_, Feb 24 2019"
			],
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A201949/b201949.txt\"\u003eTable of n, a(n) for n = 0..2115\u003c/a\u003e"
			],
			"formula": [
				"Row sums yield the factorials.",
				"Central terms in rows form A201950.",
				"Antidiagonal sums yield A201951.",
				"GENERATING FUNCTIONS.",
				"E.g.f.: A(x,y) = 1/(1 - x*y)^(1/y + y). - _Paul D. Hanna_, Mar 02 2019",
				"E.g.f.: A(x,y) = Sum_{k\u003e=0} (1/y^k + y^k)/2^(0^k) * Sum_{n\u003e=0} (-log(1 - x*y))^(2*n+k) / (n!*(n+k)!). - _Paul D. Hanna_, Feb 24 2019",
				"E.g.f.: A(x,y) = x / Series_Reversion( F(x,y) ) such that F(x/A(x,y),y) = x, where F(x,y) = Sum_{n\u003e=1} x^n/n! * Product_{k=0..n-2} (n + k*y + n*y^2) is the e.g.f. of A324305. - _Paul D. Hanna_, Feb 28 2019",
				"E.g.f. of diagonal k: (1/y^k) * Sum_{n\u003e=0} (-log(1 - x*y))^(2*n+k) / (n!*(n+k)!) for k \u003e= 0. - _Paul D. Hanna_, Feb 24 2019",
				"PARTICULAR ARGUMENTS.",
				"E.g.f. at y = 0: A(x,y=0) = exp(x).",
				"E.g.f. at y = 1: A(x,y=1) = 1/(1-x)^2.",
				"E.g.f. at y = 2: A(x,y=2) = 1/(1-2*x)^(5/2)."
			],
			"example": [
				"E.g.f.: A(x,y) = 1 + (1 + y^2)*x + (1 + y + 2*y^2 + y^3 + y^4)*x^2/2! + (1 + 3*y + 5*y^2 + 6*y^3 + 5*y^4 + 3*y^5 + y^6)*x^3/3! + (1 + 6*y + 15*y^2 + 24*y^3 + 28*y^4 + 24*y^5 + 15*y^6 + 6*y^7 + y^8)*x^4/4! + (1 + 10*y + 40*y^2 + 90*y^3 + 139*y^4 + 160*y^5 + 139*y^6 + 90*y^7 + 40*y^8 + 10*y^9 + y^10)*x^5/5! + ...",
				"which equals the power series expansion in x of the series given by",
				"A(x,y)  =  Sum_{n\u003e=0} log(1 - x*y)^(2*n) / (n!^2)  -  (1/y + y) * Sum_{n\u003e=0} log(1 - x*y)^(2*n+1) / (n!*(n+1)!)  +  (1/y^2 + y^2) * Sum_{n\u003e=0} log(1 - x*y)^(2*n+2) / (n!*(n+2)!)  -  (1/y3 + y^3) * Sum_{n\u003e=0} (-log(1 - x*y))^(2*n+3) / (n!*(n+3)!)  +  (1/y^4 + y^4) * Sum_{n\u003e=0} log(1 - x*y)^(2*n+4) / (n!*(n+4)!) + ...",
				"Triangle begins:",
				"[1],",
				"[1, 0, 1],",
				"[1, 1, 2, 1, 1],",
				"[1, 3, 5, 6, 5, 3, 1],",
				"[1, 6, 15, 24, 28, 24, 15, 6, 1],",
				"[1, 10, 40, 90, 139, 160, 139, 90, 40, 10, 1],",
				"[1, 15, 91, 300, 629, 945, 1078, 945, 629, 300, 91, 15, 1],",
				"[1, 21, 182, 861, 2520, 5019, 7377, 8358, 7377, 5019, 2520, 861, 182, 21, 1],",
				"[1, 28, 330, 2156, 8729, 23520, 45030, 65016, 73260, 65016, 45030, 23520, 8729, 2156, 330, 28, 1], ...",
				"such that the g.f. of row n equals Product_{k=0..n-1} (1 + k*x + x^2) for n\u003e0.",
				"RELATED SERIES.",
				"The e.g.f. may be defined by A(x,y) = x / Series_Reversion( F(x,y) )",
				"where F(x,y) is the e.g.f. of triangle A324305 and equals",
				"F(x,y) = Sum_{n\u003e=1} x^n/n! * Product_{k=0..n-2} (n + k*y + n*y^2)",
				"so that",
				"F(x,y) = x + (2*y^2 + 2)*x^2/2! + (9*y^4 + 3*y^3 + 18*y^2 + 3*y + 9)*x^3/3! + (64*y^6 + 48*y^5 + 200*y^4 + 96*y^3 + 200*y^2 + 48*y + 64)*x^4/4! + (625*y^8 + 750*y^7 + 2775*y^6 + 2280*y^5 + 4300*y^4 + 2280*y^3 + 2775*y^2 + 750*y + 625)*x^5/5! + ...",
				"where F(x,y) = Series_Reversion( x/A(x,y) ).",
				"RELATED TRIANGLE.",
				"Triangle A324305 of coefficients in F(x,y) such that F(x/A(x,y),y) = x begins",
				"1;",
				"2, 0, 2;",
				"9, 3, 18, 3, 9;",
				"64, 48, 200, 96, 200, 48, 64;",
				"625, 750, 2775, 2280, 4300, 2280, 2775, 750, 625;",
				"7776, 12960, 46440, 53640, 100584, 81360, 100584, 53640, 46440, 12960, 7776; ...",
				"where the g.f. of row n is Product_{k=0..n-2} (n + k*y + n*y^2) for n \u003e= 1."
			],
			"program": [
				"(PARI) {T(n,k)=polcoeff(prod(j=0,n-1,1+j*x+x^2),k)}",
				"{for(n=0,10,for(k=0,2*n,print1(T(n,k),\",\"));print(\"\"))}"
			],
			"xref": [
				"Cf. A201950, A201951; diagonals: A201952, A201953.",
				"Cf. A324305."
			],
			"keyword": "nonn,tabf",
			"offset": "0,7",
			"author": "_Paul D. Hanna_, Dec 06 2011",
			"references": 8,
			"revision": 31,
			"time": "2019-03-02T12:42:23-05:00",
			"created": "2011-12-07T18:45:29-05:00"
		}
	]
}