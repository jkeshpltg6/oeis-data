{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006165",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6165,
			"id": "M0277",
			"data": "1,1,2,2,3,4,4,4,5,6,7,8,8,8,8,8,9,10,11,12,13,14,15,16,16,16,16,16,16,16,16,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,33,34,35,36,37,38,39,40,41,42,43",
			"name": "a(1) = a(2) = 1; thereafter a(2n+1) = a(n+1) + a(n), a(2n) = 2a(n).",
			"comment": [
				"a(n+1) is the second-order survivor of the n-person Josephus problem where every second person is marked until only one remains, who is then eliminated; the process is repeated from the beginning until all but one is eliminated. a(n) is first a power of 2 when n is three times a power of 2. For example, the first appearances of 2, 4, 8 and 16 are at positions 3, 6, 12 and 24, or (3*1),(3*2),(3*4) and (3*8). Eugene McDonnell (eemcd(AT)aol.com), Jan 19 2002, reporting on work of Boyko Bantchev (Bulgaria).",
				"a(n+1)=min(msb(n),1+n-msb(n)/2) for all n (msb = most significant bit, A053644). - Boyko Bantchev (bantchev(AT)math.bas.bg), May 17 2002",
				"Appears to coincide with following sequence: Let n \u003e= 1. Start with a bag B containing n 1's. At each step, replace the two least elements x and y in B with the single element x+y. Repeat until B contains 2 or fewer elements. Let a(n) be the largest element remaining in B at this point. - _David W. Wilson_, Jul 01 2003",
				"Hsien-Kuei Hwang, S Janson, TH Tsai (2016) show that A078881 is the same sequence, apart from the offset. - _N. J. A. Sloane_, Nov 26 2017"
			],
			"reference": [
				"J. Arkin, D. C. Arney, L. S. Dewald and W. E. Ebel, Jr., Families of recursive sequences, J. Rec. Math., 22 (No. 22, 1990), 85-94.",
				"Hsien-Kuei Hwang, S Janson, TH Tsai, Exact and asymptotic solutions of the recurrence f(n) = f(floor(n/2)) + f(ceiling(n/2)) + g(n): theory and applications, Preprint, 2016; http://140.109.74.92/hk/wp-content/files/2016/12/aat-hhrr-1.pdf. Also Exact and Asymptotic Solutions of a Divide-and-Conquer Recurrence Dividing at Half: Theory and Applications, ACM Transactions on Algorithms, 13:4 (2017), #47; DOI: 10.1145/3127585",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A006165/b006165.txt\"\u003eTable of n, a(n) for n = 1..1024\u003c/a\u003e",
				"J.-P. Allouche and J. Shallit, \u003ca href=\"http://www.math.jussieu.fr/~allouche/kreg2.ps\"\u003eThe Ring of k-regular Sequences, II\u003c/a\u003e",
				"J.-P. Allouche and J. Shallit, \u003ca href=\"http://dx.doi.org/10.1016/S0304-3975(03)00090-2\"\u003eThe ring of k-regular sequences, II\u003c/a\u003e, Theoret. Computer Sci., 307 (2003), 3-29.",
				"Dale Gerdemann, \u003ca href=\"https://www.youtube.com/watch?v=-BP_ENt8MdY\"\u003eSecond-Order Josephus Problem\u003c/a\u003e (video)",
				"R. Stephan, \u003ca href=\"/somedcgf.html\"\u003eSome divide-and-conquer sequences ...\u003c/a\u003e",
				"R. Stephan, \u003ca href=\"/A079944/a079944.ps\"\u003eTable of generating functions\u003c/a\u003e",
				"\u003ca href=\"/index/J#Josephus\"\u003eIndex entries for sequences related to the Josephus Problem\u003c/a\u003e"
			],
			"formula": [
				"For n\u003e=2, if a(n)\u003e=A006257(n), i.e., if msb(n)\u003en-a(n)/2, then a(n+1)=a(n)+1, otherwise a(n+1)=a(n). - _Henry Bottomley_, Jan 21 2002",
				"a(1)=1, a(n)=n-a(n-a(a(n-1))). - _Benoit Cloitre_, Nov 08 2002",
				"For k\u003e0, 0\u003c=i\u003c=2^k-1, a(2^k+i)=2^(k-1)+i; for 2^k-2^(k-2)\u003c=x\u003c=2^k a(x)=2^(k-1); (also a(m*2^k)=a(m)*2^k for m\u003e=2). - _Benoit Cloitre_, Dec 16 2002",
				"G.f. x * (1/(1+x) + 1/(1-x)^2 * sum(k\u003e=0, t^2(1-t), t=x^2^k)). - _Ralf Stephan_, Sep 12 2003",
				"a(n) = A005942(n+1)/2 - n = n - A060973(n) = 2n - A007378(n). - _Ralf Stephan_, Sep 13 2003",
				"a(n) = A080776(n-1) + A060937(n). - _Ralf Stephan_"
			],
			"mathematica": [
				"t = {1, 1}; Do[If[OddQ[n], AppendTo[t, t[[Floor[n/2]]] + t[[Ceiling[n/2]]]], AppendTo[t, 2*t[[n/2]]]], {n, 3, 128}] (* _T. D. Noe_, May 25 2011 *)"
			],
			"xref": [
				"Cf. A066997, A078881."
			],
			"keyword": "nonn,easy",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from Larry Reeves (larryr(AT)acm.org), Jun 12 2002"
			],
			"references": 7,
			"revision": 46,
			"time": "2020-06-23T18:45:06-04:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}