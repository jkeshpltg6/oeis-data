{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A335951",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 335951,
			"data": "1,0,1,0,0,1,0,0,-1,4,0,0,1,-4,6,0,0,-3,12,-20,16,0,0,5,-20,34,-32,16,0,0,-691,2764,-4720,4592,-2800,960,0,0,105,-420,718,-704,448,-192,48,0,0,-10851,43404,-74220,72912,-46880,21120,-6720,1280",
			"name": "Triangle read by rows. The numerators of the coefficients of the Faulhaber polynomials. T(n,k) for n \u003e= 0 and 0 \u003c= k \u003c= n.",
			"comment": [
				"There are many versions of Faulhaber's triangle: search the OEIS for his name.",
				"Faulhaber's claim (in 1631) is: S_{2*m-1} = 1^(2*m-1) + 2^(2*m-1) + ... + n^(2*m-1) = F_m((n^2+2)/2). The first proof was given by Jacobi in 1834."
			],
			"reference": [
				"Johann Faulhaber, Academia Algebra. Darinnen die miraculosische Inventiones zu den höchsten Cossen weiters continuirt und profitiert werden. Johann Ulrich Schönigs, Augsburg, 1631.",
				"C. G. J. Jacobi, De usu legitimo formulae summatoriae Maclaurinianae. J. Reine Angew. Math., 12 (1834), 263-272."
			],
			"link": [
				"Donald E. Knuth, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1993-1197512-7\"\u003eJohann Faulhaber and sums of powers\u003c/a\u003e, Math. Comput. 203 (1993), 277-294.",
				"Peter Luschny, \u003ca href=\"/A335951/a335951.pdf\"\u003eIllustrating the Faulhaber polynomials for n = 1..7\u003c/a\u003e."
			],
			"formula": [
				"Let F_n(x) be the polynomial after substituting (sqrt(8*x+1)-1)/2 for x in b_n(x), where b_n(x) = (Bernoulli_{2*n)(x+1) - Bernoulli_{2*n)(1))/(2*n).",
				"F_n(1) = 1 for all n \u003e= 0.",
				"T(n, k) = numerator([x^k] F_n(x))."
			],
			"example": [
				"The first few polynomials are:",
				"[0] 1;",
				"[1] x;",
				"[2] x^2;",
				"[3] (4*x - 1)*x^2*(1/3);",
				"[4] (6*x^2 - 4*x + 1)*x^2*(1/3);",
				"[5] (16*x^3 - 20*x^2 + 12*x - 3)*x^2*(1/5);",
				"[6] (16*x^4 - 32*x^3 + 34*x^2 - 20*x + 5)*x^2*(1/3);",
				"[7] (960*x^5 - 2800*x^4 + 4592*x^3 - 4720*x^2 + 2764*x - 691)*x^2*(1/105);",
				"[8] (48*x^6 - 192*x^5 + 448*x^4 - 704*x^3 + 718*x^2 - 420*x + 105)*x^2*(1/3);",
				"[9] (1280*x^7-6720*x^6+21120*x^5-46880*x^4+72912*x^3-74220*x^2+43404*x-10851)*x^2*(1/45);",
				".",
				"Triangle starts:",
				"[0] 1;",
				"[1] 0, 1;",
				"[2] 0, 0,  1;",
				"[3] 0, 0, -1,     4;",
				"[4] 0, 0,  1,    -4,      6;",
				"[5] 0, 0, -3,     12,    -20,    16;",
				"[6] 0, 0,  5,    -20,     34,   -32,     16;",
				"[7] 0, 0, -691,   2764,  -4720,  4592,  -2800,  960;",
				"[8] 0, 0,  105,  -420,    718,  -704,    448,  -192,    48;",
				"[9] 0, 0, -10851, 43404, -74220, 72912, -46880, 21120, -6720, 1280;"
			],
			"maple": [
				"FaulhaberPolynomial := proc(n) if n = 0 then return 1 fi;",
				"expand((bernoulli(2*n, x+1) - bernoulli(2*n,1))/(2*n));",
				"sort(simplify(expand(subs(x = (sqrt(8*x+1)-1)/2, %))), [x], ascending) end:",
				"Trow := n -\u003e seq(coeff(numer(FaulhaberPolynomial(n)), x, k), k=0..n):",
				"seq(print(Trow(n)), n=0..9);"
			],
			"xref": [
				"Cf. A335952 (polynomial denominators), A000012 (row sums of the polynomial coefficients).",
				"Other representations of the Faulhaber polynomials include A093556/A093557, A162298/A162299, A220962/A220963."
			],
			"keyword": "sign,tabl,frac",
			"offset": "0,10",
			"author": "_Peter Luschny_, Jul 16 2020",
			"references": 2,
			"revision": 22,
			"time": "2020-07-24T13:30:19-04:00",
			"created": "2020-07-16T19:36:33-04:00"
		}
	]
}