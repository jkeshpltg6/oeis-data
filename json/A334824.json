{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A334824",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 334824,
			"data": "1,3,0,15,0,-1,105,0,-10,0,945,0,-105,0,1,10395,0,-1260,0,21,0,135135,0,-17325,0,378,0,-1,2027025,0,-270270,0,6930,0,-36,0,34459425,0,-4729725,0,135135,0,-990,0,1,654729075,0,-91891800,0,2837835,0,-25740,0,55,0,13749310575,0,-1964187225,0,64324260,0,-675675,0,2145,0,-1",
			"name": "Triangle, read by rows, of Lambert's numerator polynomials related to convergents of tan(x).",
			"comment": [
				"Lambert's denominator polynomials related to convergents of tan(x), f(n, x), are given in A334823."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A334824/b334824.txt\"\u003eRows n = 0..100 of the triangle, flattened\u003c/a\u003e",
				"J.-H. Lambert, \u003ca href=\"http://www.kuttaka.org/~JHL/L1768b.pdf\"\u003eMémoire sur quelques propriétés remarquables des quantités transcendantes et logarithmiques\u003c/a\u003e (Memoir on some properties that can be traced from circular transcendent and logarithmic quantities), Histoire de l’Académie royale des sciences et belles-lettres (1761), Berlin. See \u003ca href=\"http://www.bibnum.education.fr/mathematiques/theorie-des-nombres/lambert-et-l-irrationalite-de-p-1761\"\u003ealso\u003c/a\u003e."
			],
			"formula": [
				"Equals the coefficients of the polynomials, g(n, x), defined by: (Start)",
				"g(n, x) = Sum_{k=0..floor(n/2)} ((-1)^k*(2*n-2*k+1)!/((2*k+1)!*(n-2*k)!))*(x/2)^(n-2*k).",
				"g(n, x) = ((2*n+1)!/n!)*(x/2)^n*Hypergeometric2F3(-n/2, (1-n)/2; 3/2, -n, -n-1/2; -1/x^2).",
				"g(n, x) = ((-i)^n/2)*(y(n+1, i*x) + (-1)^n*y(n+1, -i*x)), where y(n, x) are the Bessel Polynomials.",
				"g(n, x) = (2*n-1)*x*g(n-1, x) - g(n-2, x).",
				"E.g.f. of g(n, x): sin((1 - sqrt(1-2*x*t))/2)/sqrt(1-2*x*t).",
				"g(n, 1) = (-1)^n*g(n, -1) = A053984(n) = (-1)^n*A053983(-n-1) = (-1)^n*f(-n-1, 1).",
				"g(n, 2) = (-1)^n*g(n, -2) = A053987(n+1). (End)",
				"As a number triangle:",
				"T(n, k) = i^k*(2*n-k+1)!*(1+(-1)^k)/(2^(n-k+1)*(k+1)!*(n-k)!), where i = sqrt(-1).",
				"T(n, 0) = A001147(n+1)."
			],
			"example": [
				"Polynomials:",
				"g(0, x) = 1;",
				"g(1, x) = 3*x;",
				"g(2, x) = 15*x^2 - 1;",
				"g(3, x) = 105*x^3 - 10*x;",
				"g(4, x) = 945*x^4 - 105*x^2 + 1;",
				"g(5, x) = 10395*x^5 - 1260*x^3 + 21*x;",
				"g(6, x) = 135135*x^6 - 17325*x^4 + 378*x^2 - 1;",
				"g(7, x) = 2027025*x^7 - 270270*x^5 + 6930*x^3 - 36*x.",
				"Triangle of coefficients begins as:",
				"        1;",
				"        3, 0;",
				"       15, 0,      -1;",
				"      105, 0,     -10, 0;",
				"      945, 0,    -105, 0,    1;",
				"    10395, 0,   -1260, 0,   21, 0;",
				"   135135, 0,  -17325, 0,  378, 0,  -1;",
				"  2027025, 0, -270270, 0, 6930, 0, -36, 0."
			],
			"maple": [
				"T:= (n, k) -\u003e I^k*(2*n-k+1)!*(1+(-1)^k)/(2^(n-k+1)*(k+1)!*(n-k)!);",
				"seq(seq(T(n, k), k = 0..n), n = 0..10);"
			],
			"mathematica": [
				"(* First program *)",
				"y[n_, x_]:= Sqrt[2/(Pi*x)]*E^(1/x)*BesselK[-n -1/2, 1/x];",
				"g[n_, k_]:= Coefficient[((-I)^n/2)*(y[n+1, I*x] + (-1)^n*y[n+1, -I*x]), x, k];",
				"Table[g[n, k], {n,0,10}, {k,n,0,-1}]//Flatten",
				"(* Second program *)",
				"Table[I^k*(2*n-k+1)!*(1+(-1)^k)/(2^(n-k+1)*(k+1)!*(n-k)!), {n,0,10}, {k,0,n}]//Flatten"
			],
			"program": [
				"(MAGMA)",
				"C\u003ci\u003e := ComplexField();",
				"T:= func\u003c n, k| Round( i^k*Factorial(2*n-k+1)*(1+(-1)^k)/(2^(n-k+1)*Factorial(k+1)*Factorial(n-k)) ) \u003e;",
				"[T(n,k): k in [0..n], n in [0..10]];",
				"(Sage) [[ i^k*factorial(2*n-k+1)*(1+(-1)^k)/(2^(n-k+1)*factorial(k+1)*factorial(n-k)) for k in (0..n)] for n in (0..10)]"
			],
			"xref": [
				"Cf. A001497, A001498, A053983, A053984, A053987, A053988, A094675, A334823.",
				"Columns k: A001147 (k=0), A000457 (k=2), A001881 (k=4), A130563 (k=6)."
			],
			"keyword": "tabl,sign",
			"offset": "0,2",
			"author": "_G. C. Greubel_, May 13 2020, following a suggestion from _Michel Marcus_",
			"references": 2,
			"revision": 19,
			"time": "2020-05-17T02:15:12-04:00",
			"created": "2020-05-15T09:11:59-04:00"
		}
	]
}