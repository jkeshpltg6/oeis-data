{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A104706",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 104706,
			"data": "1,2,3,1,4,5,1,2,6,1,7,3,1,2,8,1,9,4,1,2,10,1,3,11,1,2,5,1,12,13,1,2,3,1,4,6,1,2,14,1,15,3,1,2,7,1,5,4,1,2,16,1,3,17,1,2,8,1,18,6,1,2,3,1,4,19,1,2,5,1,9,3,1,2,20,1,21,4,1,2,7,1,3,10,1,2,22,1,5,6,1,2,3,1,4,23,1",
			"name": "First terms in the rearrangements of integer numbers (see comments).",
			"comment": [
				"Take the sequence of natural numbers:",
				"s0=1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,",
				"Move the first term s(1)=1 to 2*1 places to the right:",
				"s1=2,3,1,4,5,6,7,8,9,10,11,12,13,14,15,16,",
				"Move the first term s(1)=2 to 2*2 places to the right:",
				"s2=3,1,4,5,2,6,7,8,9,10,11,12,13,14,15,16,",
				"Repeating the procedure we get successively:",
				"s3=1,4,5,2,6,7,3,8,9,10,11,12,13,14,15,16,",
				"s4=4,5,1,2,6,7,3,8,9,10,11,12,13,14,15,16,",
				"s5=5,1,2,6,7,3,8,9,4,10,11,12,13,14,15,16,",
				"s6=1,2,6,7,3,8,9,4,10,11,5,12,13,14,15,16,",
				".......................................................................",
				"s100=8,3,1,2,24,25,4,5,12,7,6,26,9,27,13,28,29,10,14,30,31,15,11,",
				"32,33,16,34,35,17,36,37,18,38,39,19,40,41,20,42,43,21,44,45,22,",
				"46,47,23,48,49,50,51,52,53,54,55,56,57,58,59,60,",
				"The sequence A104706 gives the first terms in the rearrangements s0,s1,s2,...,s100. Cf. A104705"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A104706/b104706.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A028920(2n-1)-1. - _Benoit Cloitre_, Mar 09 2007"
			],
			"maple": [
				"A104706:= proc(N) # to produce a(1) .. a(N)",
				"    local A, R, n,M;",
				"    M:= N;",
				"    R:= $1..M;",
				"    A[1]:= 1;",
				"    for n from 2 to N do",
				"       if 2*R[1]+1 \u003e M then",
				"         R:= R, [$M+1..M+N]",
				"       fi;",
				"       R:= R[2..2*R[1]+1],R[1],R[2*R[1]+2..N];",
				"       A[n]:= R[1];",
				"    od:",
				"    seq(A[n], n=1..N);",
				"end proc:",
				"A104706(100); # _Robert Israel_, Dec 04 2015"
			],
			"mathematica": [
				"s=Range[100];bb={1};Do[s=Drop[Insert[s, s[[1]], 2+2s[[1]]], 1];bb=Append[bb, s[[1]]], {i, 100}];bb",
				"NestList[Rest[Insert[#, #[[1]], 2 + 2 #[[1]]]] \u0026, Range[30], 30][[All, 1]] (* _Birkas Gyorgy_, Mar 03 2011 *)"
			],
			"program": [
				"(Sage)",
				"def A104706(n):",
				"    m, N = 2, 2*n-1",
				"    while true:",
				"        if m.divides(N): return m-2",
				"        N = N*(m-1)//m",
				"        m += 1",
				"print([A104706(n) for n in (1..97)]) # _Peter Luschny_, Dec 04 2015"
			],
			"xref": [
				"Cf. A104705, A028920."
			],
			"keyword": "easy,nonn",
			"offset": "1,2",
			"author": "_Zak Seidov_, Mar 19 2005",
			"references": 5,
			"revision": 26,
			"time": "2020-03-07T06:19:28-05:00",
			"created": "2005-04-09T03:00:00-04:00"
		}
	]
}