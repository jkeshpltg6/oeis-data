{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A262151",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 262151,
			"data": "1,-2,6,-15,33,-68,134,-253,460,-811,1394,-2344,3863,-6253,9964,-15653,24269,-37178,56331,-84489,125529,-184867,270027,-391391,563205,-804925,1142998,-1613195,2263675,-3159023,4385502,-6057865,8328200,-11397371,15529768",
			"name": "Expansion of f(-x^3)^3 / (f(x)^2 * f(-x^2)) in powers of x where f() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A262151/b262151.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-5/24) * eta(q)^2 * eta(q^3)^3 * eta(q^4)^2 / eta(q^2)^7 in power of q.",
				"Euler transform of period 12 sequence [ -2, 5, -5, 3, -2, 2, -2, 3, -5, 5, -2, 0, ...].",
				"a(n) ~ (-1)^n * exp(Pi*sqrt(3*n/2)) / (2^(11/4) * 3^(5/4) * n^(3/4)). - _Vaclav Kotesovec_, Sep 23 2015"
			],
			"example": [
				"G.f. = 1 - 2*x + 6*x^2 - 15*x^3 + 33*x^4 - 68*x^5 + 134*x^6 - 253*x^7 + ...",
				"G.f. = q^5 - 2*q^29 + 6*q^53 - 15*q^77 + 33*q^101 - 68*q^125 + 134*q^149 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x^3]^3 / (QPochhammer[ -x]^2 QPochhammer[ x^2]), {x, 0, n}];",
				"nmax = 40; CoefficientList[Series[Product[(1-x^(3*k))^3 * (1+x^(2*k))^2 / ((1-x^k)^3 * (1+x^k)^5), {k, 1, nmax}], {x, 0, nmax}], x] (* _Vaclav Kotesovec_, Sep 13 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A)^2 * eta(x^3 + A)^3 * eta(x^4 + A)^2 / eta(x^2 + A)^7, n))};"
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Sep 13 2015",
			"references": 4,
			"revision": 13,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2015-09-13T00:25:48-04:00"
		}
	]
}