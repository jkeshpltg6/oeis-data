{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A106520",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 106520,
			"data": "1,0,0,0,2,4,18,48,156,472,1526,4852,16000,52940,178276,605520,2079862,7201084,25138878,88358520,312576996,1112087012,3977502766,14294093652,51596165872,186997738504,680272334202,2483340387644,9094756956908",
			"name": "a(n) = A068875(n-1) - A003239(n).",
			"comment": [
				"This is the multiplicity of the trivial module in a sequence of modules of dimension (2*n-2)!/n! over the symmetric groups S_n, induced from modules of dimension (2*n-2)!/(n!*(n-1)!) (Catalan) over the cyclic groups C_n."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A106520/b106520.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"F. Chapoton, \u003ca href=\"http://dx.doi.org/10.2140/agt.2005.5.53\"\u003eOn some anticyclic operads\u003c/a\u003e, Algebraic and Geometric Topology 5 (2005), paper no. 4, pages 53-69."
			],
			"formula": [
				"a(n) = (2/n) * binomial(2*n-2, n-1) - 1/(2*n) * Sum_{d divides n} phi(d) * binomial(2*n/d, n/d) ).",
				"a(n) = 2*A000108(n-1) - (1/(2*n))*Sum_{d divides n} (n/d + 1)*A000108(n/d) * A000010(d). - _G. C. Greubel_, Aug 06 2021"
			],
			"maple": [
				"with(numtheory);",
				"a:= proc(n) (2/n)*binomial(2*n-2, n-1) - (1/(2*n))*add(phi(d)*binomial(2*n/d, n/d), d = divisors(n)) end:",
				"seq(a(n), n = 1..40);"
			],
			"mathematica": [
				"a[n_]:= 2/n*Binomial[2*n-2, n-1] - 1/(2*n)*DivisorSum[n, EulerPhi[#]* Binomial[2*n/#, n/#]\u0026]; Table[a[n], {n, 40}] (* _Jean-François Alcover_, Feb 20 2017 *)"
			],
			"program": [
				"(MAGMA)",
				"A106520:= func\u003c n | 2*Catalan(n-1) - (1/(2*n))*(\u0026+[Round(Gamma(2*n/d +1)/Gamma(n/d +1)^2)*EulerPhi(d): d in Divisors(n)]) \u003e;",
				"[A106520(n): n in [1..40]]; // _G. C. Greubel_, Aug 06 2021",
				"(Sage)",
				"def a(n): return 2*catalan_number(n-1) - (1/(2*n))*sum(euler_phi(n/d)*binomial(2*d, d) for d in divisors(n))",
				"[a(n) for n in (1..40)] # _G. C. Greubel_, Aug 06 2021",
				"(PARI) a(n) = (2/n) * binomial(2*n-2, n-1) - 1/(2*n) * sumdiv(n, d, eulerphi(d) * binomial(2*n/d, n/d)); \\\\ _Michel Marcus_, Aug 08 2021"
			],
			"xref": [
				"Cf. A000010, A000108, A001761."
			],
			"keyword": "nonn",
			"offset": "1,5",
			"author": "_F. Chapoton_, May 30 2005",
			"ext": [
				"Terms a(1) to a(4) prepended by _G. C. Greubel_, Aug 06 2021"
			],
			"references": 1,
			"revision": 24,
			"time": "2021-08-09T14:03:39-04:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}