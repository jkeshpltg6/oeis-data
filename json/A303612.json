{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A303612",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 303612,
			"data": "1,7,4,3,5,2,5,3,4,7,10,9,8,15,7,13,19,6,11,16,5,14,9,13,17,4,19,11,18,7,10,13,19,3,29,17,11,19,8,18,5,17,12,7,9,11,13,15,21,35,2,35,21,15,13,11,9,7,12,17,5,18,8,19,11,17,29,3,19,13,10",
			"name": "a(n) = min{denominator(r) with r in R} and R = {0 \u003c= r rational \u003c= 1 and [r*k] = n}. Here k = 10^(floor(log_10(n)+1) and [x] = floor(x+1/2) + ceiling((2*x-1)/4) - floor((2*x-1)/4) - 1.",
			"comment": [
				"a(n) is the smallest denominator of a fraction that, when rounded to d digits after the decimal point, is equal to 0.n, where d is the number of digits of n, and the rounding convention applied is that a number whose fractional part is 1/2 is rounded to the nearest even integer.",
				"a(k-n) = a(n), where k is the first power of 10 exceeding n.",
				"The sequence [A297367(n)/a(n), n = 10^(k-1)..10^(k)-1] is a subsequence of the Farey sequence A006842/A006843 of order ceiling((2/3)*10^k). For example, the terms a(1)..a(9) are the denominators of {1/7, 1/4, 1/3, 2/5, 1/2, 3/5, 2/3, 3/4, 6/7}; this sequence of fractions is a subsequence of the Farey sequence of order ceiling((2/3)*10^1) = 7, i.e., F7 = {0/1, 1/7, 1/6, 1/5, 1/4, 2/7, 1/3, 2/5, 3/7, 1/2, 4/7, 3/5, 2/3, 5/7, 3/4, 4/5, 5/6, 6/7, 1/1}.",
				"With the exception of n in {1, 2, 4, 13, 16}, r(n) = A297367(n)/a(n) is in the Farey series of order n (row n of A006842/A006843). - _Peter Luschny_, May 19 2018"
			],
			"reference": [
				"C. F. Gauss, Theorematis arithmetici demonstratio nova, Societati regiae scientiarum Gottingensis, Vol. XVI., January 15, 1808, pp. 5-7, section 4-5.",
				"L. Graham and Donald E. Knuth and Oren Patashnik, Concrete mathematics: a foundation for computer science (Second Edition), Addison-Wesley Publishing Company, 1994, pp. 86-101.",
				"Kenneth E. Iverson, A Programming Language, John Wiley And Sons, Inc., 1962 (4th printing 1967), pp. 11-12.",
				"Takeo Kamizawa, Note on the Distance to the Nearest Integer, Faculty of Physics, Astronomy and Informatics, Nicolaus Copernicus University, Toruń, Poland, 2016.",
				"A. M. Legendre, Théorie des nombres (deuxième édition), 1808.",
				"D. Zuras, M. Cowlishaw, R. M. Grow, et al., IEEE Standard for Floating-Point Arithmetic, Std 754(tm)-2008, ISBN 978-0-7381-5753-5, August 28, 2008, p. 16, sections 4.3.1-4.3.3."
			],
			"link": [
				"Luca Petrone, \u003ca href=\"/A303612/b303612.txt\"\u003eTable of n, a(n) for n = 0..9999\u003c/a\u003e",
				"IEEE, \u003ca href=\"https://ieeexplore.ieee.org/servlet/opac?punumber=4610933\"\u003e754-2008 - IEEE Standard for Floating-Point Arithmetic\u003c/a\u003e, IEEE Computer Society, August 28, 2008.",
				"Luca Petrone, \u003ca href=\"/A303612/a303612.pdf\"\u003elog-log plot for first 100000 terms\u003c/a\u003e",
				"Štefan Porubský, \u003ca href=\"http://www.cs.cas.cz/portal/AlgoMath/NumberTheory/ArithmeticFunctions/IntegerRoundingFunctions.htm\"\u003eInteger rounding functions\u003c/a\u003e, Interactive Information Portal for Algorithmic Mathematics, Institute of Computer Science of the Czech Academy of Sciences, April 1, 2007.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/NearestIntegerFunction.html\"\u003eNearest Integer Function\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Nearest_integer_function\"\u003eNearest integer function\u003c/a\u003e"
			],
			"example": [
				"The table below shows the different rational numbers which satisfy the requirements of the definition. The denominators of the first rational number in each row constitute the sequence. Note that the round function is not implemented uniformly in popular software. For example, Mathematica matches our definition, while Maple's round function would return incorrect values.",
				".",
				"  |                     |  decimal  |     round(10*r)",
				"n | rational numbers r  |   value   | Mathematica | Maple",
				"--+---------------------+-----------+-------------+------",
				"0 | 0/1                 | 0.0000000 |      0      |   0",
				"1 | 1/7, 1/8, 1/9, 1/10 | 0.1428571 |      1      |   1",
				"2 | 1/4, 1/5, 1/6, 2/9  | 0.2500000 |      2      | * 3 *",
				"3 | 1/3, 2/7, 3/10      | 0.3333333 |      3      |   3",
				"4 | 2/5, 3/7, 3/8, 4/9  | 0.4000000 |      4      |   4",
				"5 | 1/2                 | 0.5000000 |      5      |   5",
				"6 | 3/5, 4/7, 5/8, 5/9  | 0.6000000 |      6      |   6",
				"7 | 2/3, 5/7, 7/10      | 0.6666667 |      7      |   7",
				"8 | 3/4, 4/5, 5/6, 7/9  | 0.7500000 |      8      |   8",
				"9 | 6/7, 7/8, 8/9, 9/10 | 0.8571429 |      9      |   9"
			],
			"maple": [
				"r := proc(n) local nint, k, p, q; k := 10^(ilog10(n)+1);",
				"nint := m -\u003e floor(m + 1/2) + ceil((2*m-1)/4) - floor((2*m-1)/4) - 1;",
				"for p from 1 to k do for q from p+1 to k do if nint(p*k/q) = n then return p/q fi od od; 0/1 end:",
				"a := n -\u003e denom(r(n)): seq(a(n), n=0..99); # _Peter Luschny_, May 19 2018"
			],
			"mathematica": [
				"a = {1};",
				"For[i = 1, i \u003c= 100, i++,",
				"nmax = 10^(Floor[Log[10, i]] + 1);",
				"r = i/nmax;",
				"For[n = 1, n \u003c= nmax, n++,",
				"If[Round[Round[n r]/n, 1/nmax] == r,",
				"a = Flatten[Append[a, n]];",
				"Break[];",
				"]]]"
			],
			"xref": [
				"Cf. A239525, A297367, A006842, A006843, A304879."
			],
			"keyword": "nonn,base,frac,easy",
			"offset": "0,2",
			"author": "_Luca Petrone_, Apr 27 2018",
			"references": 3,
			"revision": 73,
			"time": "2018-05-21T02:46:15-04:00",
			"created": "2018-05-20T11:30:00-04:00"
		}
	]
}