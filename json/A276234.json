{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A276234",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 276234,
			"data": "1,1,3,1,5,3,7,1,9,5,11,3,13,7,15,1,17,9,19,5,21,11,23,3,25,13,27,7,29,15,31,1,33,17,35,9,37,19,39,5,41,21,43,11,45,23,47,3,49,25,51,13,53,27,55,7,57,29,59,15,61,31,63,1,65,33,67,17,69,35,71",
			"name": "a(n) = n/gcd(n, 256).",
			"comment": [
				"a(n) first differs from A000265(n) at n = 512. - _Andrew Howroyd_, Jul 23 2018",
				"A multiplicative sequence. Also, a(n) is a strong divisibility sequence, that is, gcd(a(n),a(m)) = a(gcd(n,m)) for n \u003e= 1, m \u003e= 1. In particular, a(n) is a divisibility sequence: if n divides m then a(n) divides a(m). - _Peter Bala_, Feb 27 2019"
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A276234/b276234.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"P. Bala, \u003ca href=\"/A306367/a306367.pdf\"\u003eA note on the sequence of numerators of a rational function \u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_512\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, order 512."
			],
			"formula": [
				"a(2k-1) = 2k-1.",
				"G.f.: (x+x^3)/(1-x^2)^2 +(x^2+x^6)/(1-x^4)^2 +(x^4+x^12)/(1-x^8)^2 +(x^8+x^24)/(1-x^16)^2 +(x^16+x^48)/(1-x^32)^2 +(x^32+x^96)/(1-x^64)^2 +(x^64+x^192)/(1-x^128)^2 +(x^128+x^256+x^384)/(1-x^256)^2. - _Robert Israel_, Aug 26 2016",
				"a(n) = 2*a(n-256) - a(n-512). - _Charles R Greathouse IV_, Aug 26 2016",
				"From _Peter Bala_, Feb 27 2019: (Start)",
				"a(n) = numerator(n/(n + 256)).",
				"O.g.f.: F(x) - Sum_{k = 1..8} F(x^(2^k)), where F(x) = x/(1 - x)^2. Cf. A106617. (End)"
			],
			"maple": [
				"seq(n/igcd(n,256), n=1..100); # _Robert Israel_, Aug 26 2016"
			],
			"mathematica": [
				"Table[n/GCD[n, 2^8], {n,1,80}] (* _G. C. Greubel_, Feb 27 2019 *)"
			],
			"program": [
				"(PARI) a(n)=n/gcd(n,256) \\\\ _Charles R Greathouse IV_, Aug 26 2016",
				"(MAGMA) [n/GCD(n, 2^8): n in [1..80]]; // _G. C. Greubel_, Feb 27 2019",
				"(Sage) [n/gcd(n, 2^8) for n in (1..80)] # _G. C. Greubel_, Feb 27 2019",
				"(GAP) List([1..80], n-\u003e n/Gcd(n, 2^8)); # _G. C. Greubel_, Feb 27 2019"
			],
			"xref": [
				"Cf. A276233 (numerators), A227140, A000265, A106617."
			],
			"keyword": "nonn,easy,mult",
			"offset": "1,3",
			"author": "_Artur Jasinski_, Aug 24 2016",
			"ext": [
				"Keyword:mult added and terms a(51) and beyond from _Andrew Howroyd_, Jul 23 2018"
			],
			"references": 3,
			"revision": 40,
			"time": "2019-02-28T03:17:54-05:00",
			"created": "2016-09-07T10:56:31-04:00"
		}
	]
}