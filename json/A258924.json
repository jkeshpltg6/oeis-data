{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A258924",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 258924,
			"data": "1,-24,-169344,-25255286784,-23089632627769344,-79051067969864491597824,-766667475511149432871084621824,-17578325209217134578862801556544159744,-839197248407269659950832532302025663168118784",
			"name": " E.g.f.: S(x) = Series_Reversion( Integral 1/(1-x^5)^(1/5) dx ), where the constant of integration is zero.",
			"formula": [
				" Let e.g.f. C(x) = Sum_{n\u003e=0} a(n)*x^(5*n)/(5*n)! and e.g.f. S(x) = Sum_{n\u003e=0} a(n)*x^(5*n+1)/(5*n+1)!, then C(x) and S(x) satisfy:",
				"(1) C(x)^5 + S(x)^5 = 1,",
				"(2) S'(x) = C(x),",
				"(3) C'(x) = -S(x)^4/C(x)^3,",
				"(4) C(x)^4 * C'(x) + S(x)^4 * S'(x) = 0,",
				"(5) S(x)/C(x) = Integral 1/C(x)^5 dx,",
				"(6) S(x)/C(x) = Series_Reversion( Integral 1/(1+x^5) dx ) = Series_Reversion( Sum_{n\u003e=0} (-1)^n * x^(5*n+1)/(5*n+1) )."
			],
			"example": [
				" E.g.f. with offset 0 is C(x) and e.g.f. with offset 1 is S(x) where:",
				"C(x) = 1 - 24*x^5/5! - 169344*x^10/10! - 25255286784*x^15/15! - 23089632627769344*x^20/20! +...",
				"S(x) = x - 24*x^6/6! - 169344*x^11/11! - 25255286784*x^16/16! - 23089632627769344*x^21/21! +...",
				"such that C(x)^5 + S(x)^5 = 1:",
				"C(x)^5 = 1 - 120*x^5/5! + 604800*x^10/10! + 13208832000*x^15/15! +...",
				"S(x)^5 = 120*x^5/5! - 604800*x^10/10! - 13208832000*x^15/15! -...",
				"Related Expansions.",
				"(1) The series reversion of S(x) is Integral 1/(1-x^5)^(1/5) dx:",
				"Series_Reversion(S(x)) = x + 24*x^6/6! + 435456*x^11/11! + 115075344384*x^16/16! +...",
				"1/(1-x^5)^(1/5) = 1 + 24*x^5/5! + 435456*x^10/10! + 115075344384*x^15/15! +...",
				"(2) d/dx S(x)/C(x) = 1/C(x)^5:",
				"1/C(x)^5 = 1 + 120*x^5/5! + 3024000*x^10/10! + 858574080000*x^15/15! +...",
				"S(x)/C(x) = x + 120*x^6/6! + 3024000*x^11/11! + 858574080000*x^16/16! + 1226178516326400000*x^21/21! +...+ A258925(n)*x^(5*n+1)/(5*n+1)! +...",
				"where",
				"Series_Reversion(S(x)/C(x)) = x - 1/6*x^6 + 1/11*x^11 - 1/16*x^16 + 1/21*x^21 +..."
			],
			"program": [
				" (PARI) /* E.g.f. Series_Reversion(Integral 1/(1-x^5)^(1/5) dx): */",
				"{a(n)=local(S=x); S = serreverse( intformal(  1/(1-x^5 +x*O(x^(5*n)))^(1/5) )); (5*n+1)!*polcoeff(S, 5*n+1)}",
				"for(n=0, 15, print1(a(n), \", \"))",
				"(PARI) /* E.g.f. C(x) with offset 0: */",
				"{a(n)=local(S=x, C=1+x); for(i=1, n, S=intformal(C +x*O(x^(5*n))); C=1-intformal(S^4/C^3 +x*O(x^(5*n))); ); (5*n)!*polcoeff(C, 5*n)}",
				"for(n=0, 15, print1(a(n), \", \"))",
				"(PARI) /* E.g.f. S(x) with offset 1: */",
				"{a(n)=local(S=x, C=1+x); for(i=1, n+1, S=intformal(C +x*O(x^(5*n+1))); C=1-intformal(S^4/C^3 +x*O(x^(5*n+1))); ); (5*n+1)!*polcoeff(S, 5*n+1)}",
				"for(n=0, 15, print1(a(n), \", \"))"
			],
			"xref": [
				" Cf. A258925 (S/C), A258878, A258900, A258927."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Paul D. Hanna_, Jun 15 2015",
			"references": 2,
			"revision": 3,
			"time": "2015-06-15T16:46:57-04:00",
			"created": "2015-06-15T16:46:57-04:00"
		}
	]
}