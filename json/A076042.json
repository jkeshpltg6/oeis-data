{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A076042",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 76042,
			"data": "0,1,5,14,30,5,41,90,26,107,7,128,272,103,299,74,330,41,365,4,404,845,361,890,314,939,263,992,208,1049,149,1110,86,1175,19,1244,2540,1171,2615,1094,2694,1013,2777,928,2864,839,2955,746,3050,649,3149",
			"name": "a(0) = 0; thereafter a(n) = a(n-1) + n^2 if a(n-1) \u003c n^2, otherwise a(n) = a(n-1) - n^2.",
			"comment": [
				"Does not return to zero within first 2^25000 =~ 10^7525 terms.  Define an epoch as an addition followed by a sequence of (addition, subtraction) pairs.  The first epoch has length 1 (+), the second 3 (++-), the third 5 (++-+-), and so forth (cf. A324792).  The epoch lengths increase geometrically by about the square root of 3, and the value at the end of each epoch is the low value in the epoch.  These observations lead to the Python program given. - _Tomas Rokicki_, Aug 31 2019",
				"Using the Maple program in A324791, I confirmed that a(n) != 0 for 0 \u003c n \u003c 10^2394. See the a- and b-files in A325056 and A324791. - _N. J. A. Sloane_, Oct 03 2019",
				"'Easy Recamán transform' of the squares. - _Daniel Forgues_, Oct 25 2019"
			],
			"link": [
				"Tomas Rokicki, \u003ca href=\"/A076042/b076042.txt\"\u003eTable of n, a(n) for n = 0..20000\u003c/a\u003e"
			],
			"maple": [
				"a:= proc(n) option remember; `if`(n\u003c0, 0,",
				"      ((s, t)-\u003e s+`if`(s\u003ct, t, -t))(a(n-1), n^2))",
				"    end:",
				"seq(a(n), n=0..70);  # _Alois P. Heinz_, Jan 11 2020"
			],
			"mathematica": [
				"a[0] = 0;",
				"a[n_] := a[n] = a[n-1] + If[a[n-1] \u003c n^2, n^2, -n^2];",
				"a /@ Range[0, 50] (* _Jean-François Alcover_, Apr 11 2020 *)"
			],
			"program": [
				"(PARI) v=vector(50); v[1]=1; for(n=2,50,if(v[n-1]\u003cn^2,v[n]=v[n-1]+n^2,v[n]=v[n-1]-n^2)); print(v)"
			],
			"xref": [
				"Cf. A076039, A003462, A076041, A046901.",
				"See also A325056, A324791, A324792.",
				"Cf. A053461 ('Recamán transform' of the squares).",
				"Cf. A000290, A008344, A008345."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Amarnath Murthy_, Oct 29 2002",
			"ext": [
				"More terms from _Ralf Stephan_, Mar 20 2003",
				"a(0)=0 prepended, at the suggestion of _Allan C. Wechsler_, by _N. J. A. Sloane_, Aug 31 2019",
				"Offset set to 0, to cohere with previous action of _N. J. A. Sloane_, by _Allan C. Wechsler_, Sep 08 2019"
			],
			"references": 15,
			"revision": 54,
			"time": "2021-03-13T10:50:00-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}