{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A185974",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 185974,
			"data": "1,2,3,4,5,6,8,7,10,9,12,16,11,14,15,20,18,24,32,13,22,21,25,28,30,27,40,36,48,64,17,26,33,35,44,42,50,45,56,60,54,80,72,96,128,19,34,39,55,49,52,66,70,63,75,88,84,100,90,81,112,120,108,160,144,192,256,23,38,51,65,77,68,78,110,98,99,105,125,104,132,140,126,150,135,176,168,200,180,162,224,240,216,320,288,384,512,29,46,57,85,91,121,76,102,130,154,117,165,147,175,136,156,220,196,198,210,250,189,225,208,264,280,252,300,270,243,352,336,400,360,324,448,480,432,640,576,768,1024",
			"name": "Partitions in Abramowitz-Stegun order A036036 mapped one-to-one to positive integers.",
			"comment": [
				"First differs from A334438 (shifted left once) at a(75) = 98, A334438(76) = 99. - _Gus Wiseman_, May 20 2020",
				"This mapping of the set of all partitions of N\u003e=1 to {2,3,...} (set of natural numbers without 1) is one to one (bijective). The empty partition for N=0 maps to 1.",
				"A129129 seems to be analogous, except that the partition ordering A080577 is used. This ordering, however, does not care about the number of parts: e.g., 1^2,4 = 4,1^2 comes before 3^2, so a(23)=28 and a(22)=25 are interchanged.",
				"Also Heinz numbers of all reversed integer partitions (finite weakly increasing sequences of positive integers), sorted first by sum, then by length, and finally lexicographically, where the Heinz number of an integer partition (y_1,...,y_k) is prime(y_1)*...*prime(y_k). The version for non-reversed partitions is A334433. - _Gus Wiseman_, May 20 2020"
			],
			"link": [
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"OEIS Wiki, \u003ca href=\"http://oeis.org/wiki/Orderings of partitions\"\u003eOrderings of partitions\u003c/a\u003e",
				"Wikiversity, \u003ca href=\"https://en.wikiversity.org/wiki/Lexicographic_and_colexicographic_order\"\u003e Lexicographic and colexicographic order\u003c/a\u003e."
			],
			"formula": [
				"a(n) = Product_{j=1..N(n)}p(j)^e(j), with p(j):=A000040(j) (j-th prime), and the exponent e(j)\u003e=0 of the part j in the n-th partition written in Abramowitz-Stegun (A-St) order, indicated in A036036. Note that j^0 is not 1 but has to be omitted in the partition. N(n) is the index (argument) of the smallest A026905-number greater or equal to n (the index of the A026905-ceiling of n).",
				"From _Gus Wiseman_, May 21 2020: (Start)",
				"A001221(a(n)) = A103921(n).",
				"A001222(a(n)) = A036043(n).",
				"A056239(a(n)) = A036042(n).",
				"A061395(a(n)) = A049085(n).",
				"(End)"
			],
			"example": [
				"a(22) = 25 because the 22nd partition in A-St order is the 2-part partition 3^2 = 3,3 with N=6 because A026905(5) = 18 and  A026905(6) = 29, so ceiling(A026905,22) = 29.",
				"a(23) = 28 relates to the partition 1^2 4 = 4 1^2 with three parts, also belonging to N=6.",
				"From _Gus Wiseman_, May 20 2020: (Start)",
				"Triangle begins:",
				"   1",
				"   2",
				"   3   4",
				"   5   6   8",
				"   7  10   9  12  16",
				"  11  14  15  20  18  24  32",
				"  13  22  21  25  28  30  27  40  36  48  64",
				"  17  26  33  35  44  42  50  45  56  60  54  80  72  96 128",
				"As a triangle of reversed partitions we have:",
				"                             0",
				"                            (1)",
				"                          (2)(11)",
				"                        (3)(12)(111)",
				"                   (4)(13)(22)(112)(1111)",
				"             (5)(14)(23)(113)(122)(1112)(11111)",
				"  (6)(15)(24)(33)(114)(123)(222)(1113)(1122)(11112)(111111)",
				"(End)"
			],
			"mathematica": [
				"Join@@Table[Times@@Prime/@#\u0026/@Sort[Reverse/@IntegerPartitions[n]],{n,0,8}] (* _Gus Wiseman_, May 21 2020 *)"
			],
			"xref": [
				"Row lengths are A000041.",
				"The constructive version is A036036.",
				"Also Heinz numbers of the partitions in A036037.",
				"The generalization to compositions is A124734.",
				"The version for non-reversed partitions is A334433.",
				"The non-reversed length-insensitive version is A334434.",
				"The opposite version (sum/length/revlex) is A334435.",
				"Ignoring length gives A334437.",
				"Sorting reversed partitions by Heinz number gives A112798.",
				"Partitions in lexicographic order are A193073.",
				"Partitions in colexicographic order are A211992.",
				"Graded Heinz numbers are A215366.",
				"Cf. A026791, A036043, A056239, A080577, A228531, A296150, A334301, A334302, A334436, A334438, A334439."
			],
			"keyword": "nonn,easy,tabf",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Feb 10 2011",
			"references": 37,
			"revision": 41,
			"time": "2020-05-28T04:59:37-04:00",
			"created": "2011-02-08T05:49:32-05:00"
		}
	]
}