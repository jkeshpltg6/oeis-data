{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A034178",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 34178,
			"data": "1,0,1,1,1,0,1,1,2,0,1,1,1,0,2,2,1,0,1,1,2,0,1,2,2,0,2,1,1,0,1,2,2,0,2,2,1,0,2,2,1,0,1,1,3,0,1,3,2,0,2,1,1,0,2,2,2,0,1,2,1,0,3,3,2,0,1,1,2,0,1,3,1,0,3,1,2,0,1,3,3,0,1,2,2,0,2,2,1,0,2,1,2,0,2,4,1,0,3",
			"name": "Number of solutions to n = a^2 - b^2, a \u003e b \u003e= 0.",
			"comment": [
				"Also, number of ways n can be expressed as the sum of one or more consecutive odd numbers. (E.g., 45 = 45 = 13+15+17 = 5+7+9+11+13, so a(45)=3.) - _Naohiro Nomoto_, Feb 26 2002",
				"a(A042965(n))\u003e0, a(A016825(n))=0; also number of occurrences of n in A094728. - _Reinhard Zumkeller_, May 24 2004",
				"It appears a(n) can be found by adding together the divisor pairs of n and finding the number of even results. For example: n=9 has the divisor pairs (1,9) and (3,3); adding the pairs: 1+9=10 is even and 3+3=6 is even, so a(9)=2. Another example: n=90 has the divisor pairs (1,96) (2,48) (3,32) (4,24) (6,16) (8,12); when each pair is added there are 4 even results, so a(96)=4. - _Gregory Bryant_, Dec 06 2016",
				"It appears a(n) is the number of nonnegative integers k for which sqrt(k) + sqrt(k + n) is an integer. For example: a(2015) = 4 since there are only four nonnegative integers k for which sqrt(k) + sqrt(k + 2015) is an integer, namely k = 289, 5041, 39601, 1014049. - _Joseph Barrera_, Nov 29 2020"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A034178/b034178.txt\"\u003eTable of n, a(n) for n = 1..2000\u003c/a\u003e",
				"M. A. Nyblom, \u003ca href=\"https://www.fq.math.ca/Scanned/40-3/nyblom.pdf\"\u003eOn the Representation of the Integers as a Difference of Squares\u003c/a\u003e, Fibonacci Quart., vol. 40 (2002), no. 3, 243-246.",
				"Edward T. H. Wang, \u003ca href=\"https://cms.math.ca/crux/backfile/Crux_v19n01_Jan.pdf\"\u003eProblem 1717\u003c/a\u003e, Crux Mathematicorum, page 30, Vol. 19, Jan. 93."
			],
			"formula": [
				"From _Naohiro Nomoto_, Feb 26 2002: (Start)",
				"a(2k) = A038548(2k) - A001227(k).",
				"a(2k+1) = A038548(2k+1). (End)",
				"From _Bernard Schott_, Apr 11 2019: (Start) (see Crux link)",
				"a(n) = 0 if n == 2 (mod 4)",
				"a(n) = floor((A000005(n) + 1)/2) if n == 1 or n == 3 (mod 4)",
				"a(n) = floor((A000005(n/4) + 1)/2) if n == 0 (mod 4). (End)",
				"G.f.: Sum_{i\u003e=1} Sum_{j\u003e=i} Product_{k=i..j} x^(2*k-1). - _Ilya Gutkovskiy_, Apr 18 2019"
			],
			"example": [
				"G.f. = x + x^3 + x^4 + x^5 + x^7 + x^8 + 2*x^9 + x^11 + x^12 + x^13 + 2*x^15 + ...",
				"From _Bernard Schott_, Apr 19 2019: (Start)",
				"a(8) = floor((A000005(2) + 1)/2) = floor(3/2) = 1 and 8 = 3^2 - 1^2.",
				"a(9) = floor((A000005(9) + 1)/2) = floor(4/2) = 2 and 9 = 3^2 - 0^2 = 5^2 - 4^2.",
				"a(10) = 0 and a^2 - b^2 = 10 has no solution.",
				"a(11) = floor(A000005(11) + 1)/2 = floor(3/2) = 1 and 11 = 6^2 - 5^2.  (End)"
			],
			"mathematica": [
				"nn = 100; t = Table[0, {nn}]; Do[n = a^2 - b^2; If[n \u003c= nn, t[[n]]++], {a, nn}, {b, 0, a - 1}];t (* _T. D. Noe_, May 04 2011 *)",
				"Table[Length[FindInstance[a^2-b^2==n\u0026\u0026a\u003eb\u003e=0,{a,b},Integers,10]],{n,100}] (* _Harvey P. Dale_, Jul 28 2021 *)"
			],
			"program": [
				"(PARI) a(n)=sum(k=1, sqrtint(n), (n-k^2)%(2*k)==0) \\\\ _Charles R Greathouse IV_, Sep 27 2012",
				"(PARI) a(n)=sumdiv(n, d, n\u003e=d^2 \u0026\u0026 (n-d^2)%(2*d)==0) \\\\ _Charles R Greathouse IV_, Sep 27 2012"
			],
			"xref": [
				"Cf. A000005, A058957, A016825."
			],
			"keyword": "easy,nonn,nice",
			"offset": "1,9",
			"author": "_Erich Friedman_",
			"references": 31,
			"revision": 65,
			"time": "2021-07-28T18:48:43-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}