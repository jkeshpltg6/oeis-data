{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A336571",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 336571,
			"data": "1,1,1,2,1,3,1,4,2,3,1,5,1,3,3,8,1,5,1,5,3,3,1,14,2,3,4,5,1,4,1,16,3,3,3,17,1,3,3,14,1,4,1,5,5,3,1,36,2,5,3,5,1,14,3,14,3,3,1,16,1,3,5,32,3,4,1,5,3,4,1,35,1,3,5,5,3,4,1,36,8,3,1",
			"name": "Number of sets of divisors d|n, 1 \u003c d \u003c n, all belonging to A130091 (numbers with distinct prime multiplicities) and forming a divisibility chain.",
			"comment": [
				"A number's prime signature (row n of A124010) is the sequence of positive exponents in its prime factorization, so a number has distinct prime multiplicities iff all the exponents in its prime signature are distinct."
			],
			"example": [
				"The a(n) sets for n = 4, 6, 12, 16, 24, 84, 36:",
				"  {}   {}   {}     {}       {}        {}        {}",
				"  {2}  {2}  {2}    {2}      {2}       {2}       {2}",
				"       {3}  {3}    {4}      {3}       {3}       {3}",
				"            {4}    {8}      {4}       {4}       {4}",
				"            {2,4}  {2,4}    {8}       {7}       {9}",
				"                   {2,8}    {12}      {12}      {12}",
				"                   {4,8}    {2,4}     {28}      {18}",
				"                   {2,4,8}  {2,8}     {2,4}     {2,4}",
				"                            {4,8}     {2,12}    {3,9}",
				"                            {2,12}    {2,28}    {2,12}",
				"                            {3,12}    {3,12}    {2,18}",
				"                            {4,12}    {4,12}    {3,12}",
				"                            {2,4,8}   {4,28}    {3,18}",
				"                            {2,4,12}  {7,28}    {4,12}",
				"                                      {2,4,12}  {9,18}",
				"                                      {2,4,28}  {2,4,12}",
				"                                                {3,9,18}"
			],
			"mathematica": [
				"strchns[n_]:=If[n==1,1,Sum[strchns[d],{d,Select[Most[Divisors[n]],UnsameQ@@Last/@FactorInteger[#]\u0026]}]];",
				"Table[strchns[n],{n,100}]"
			],
			"xref": [
				"A336423 is the version for chains containing n.",
				"A336570 is the maximal version.",
				"A000005 counts divisors.",
				"A001055 counts factorizations.",
				"A007425 counts divisors of divisors.",
				"A032741 counts proper divisors.",
				"A045778 counts strict factorizations.",
				"A071625 counts distinct prime multiplicities.",
				"A074206 counts strict chains of divisors from n to 1.",
				"A130091 lists numbers with distinct prime multiplicities.",
				"A181796 counts divisors with distinct prime multiplicities.",
				"A253249 counts chains of divisors.",
				"A336422 counts divisible pairs of divisors, both in A130091.",
				"A336424 counts factorizations using A130091.",
				"A336500 counts divisors of n in A130091 with quotient also in A130091.",
				"Cf. A001222, A002033, A005117, A098859, A118914, A124010, A294068, A305149, A327498, A327523, A336568, A336569."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Gus Wiseman_, Jul 29 2020",
			"references": 21,
			"revision": 18,
			"time": "2020-09-02T23:05:31-04:00",
			"created": "2020-07-30T22:48:59-04:00"
		}
	]
}