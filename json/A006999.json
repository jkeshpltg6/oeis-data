{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006999",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6999,
			"id": "M1047",
			"data": "0,1,2,4,7,11,17,26,40,61,92,139,209,314,472,709,1064,1597,2396,3595,5393,8090,12136,18205,27308,40963,61445,92168,138253,207380,311071,466607,699911,1049867,1574801,2362202,3543304,5314957,7972436",
			"name": "Partitioning integers to avoid arithmetic progressions of length 3.",
			"comment": [
				"a(n) = A006997(3^n-1).",
				"It appears that, aside from the first term, this is the (L)-sieve transform of A016789 ={2,5,8,11,...,3n+2....}. This has been verified up to a(30)=311071. See A152009 for the definition of the (L)-sieve transform. - _John W. Layman_, Nov 20 2008"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A006999/b006999.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"B. Chen, R. Chen, J. Guo, S. Lee et al, \u003ca href=\"http://arxiv.org/abs/1808.04304\"\u003eOn Base 3/2 and its sequences\u003c/a\u003e, arXiv:1808.04304 [math.NT], 2018.",
				"Joseph Gerver, James Propp and Jamie Simpson, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9939-1988-0929018-1\"\u003eGreedily partitioning the natural numbers into sets free of arithmetic progressions\u003c/a\u003e Proc. Amer. Math. Soc. 102 (1988), no. 3, 765-772.",
				"D. R. Hofstadter, \u003ca href=\"/A006336/a006336_1.pdf\"\u003eEta-Lore\u003c/a\u003e [Cached copy, with permission]",
				"D. R. Hofstadter, \u003ca href=\"/A006336/a006336_2.pdf\"\u003ePi-Mu Sequences\u003c/a\u003e [Cached copy, with permission]",
				"D. R. Hofstadter and N. J. A. Sloane, \u003ca href=\"/A006336/a006336.pdf\"\u003eCorrespondence, 1977 and 1991\u003c/a\u003e",
				"James Propp and N. J. A. Sloane, \u003ca href=\"/A006997/a006997.pdf\"\u003eEmail, March 1994\u003c/a\u003e"
			],
			"formula": [
				"a(n) = floor((3a(n-1)+2)/2).",
				"a(n) = -1 + floor(c*(3/2)^n) where c=1.081513668589844877304633988599549408710737041542024954790295591585622666484989650922411026555488940... - _Benoit Cloitre_, Jan 10 2002",
				"a(n+1) = (3*a(n))/2+1 if a(n) is even. a(n+1) = (3*a(n)+1)/2 if a(n) is odd. - _Miquel Cerda_, Jun 15 2019"
			],
			"mathematica": [
				"a[0] = 0; a[n_] := a[n] = Floor[(3 a[n-1] + 2)/2];",
				"Table[a[n], {n, 0, 40}] (* _Jean-François Alcover_, Aug 01 2018 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c1,0,floor((3*a(n-1)+2)/2))",
				"(Haskell)",
				"a006999 n = a006999_list !! n",
				"a006999_list = 0 : map ((`div` 2) . (+ 2) . (* 3)) a006999_list",
				"-- _Reinhard Zumkeller_, Oct 26 2011"
			],
			"xref": [
				"a(n) = A061419(n) - 1 = A061418(n) - 2.",
				"The constant c is 2/3*K(3) (see A083286). - _Ralf Stephan_, May 29 2003",
				"Cf. A003312.",
				"First differences are in A073941.",
				"Cf. A016789, A152009. - _John W. Layman_, Nov 20 2008",
				"Cf. A005428 (first differences)."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_, D. R. Hofstadter, and _James Propp_, Jul 15 1977",
			"ext": [
				"More terms from _James A. Sellers_, Feb 06 2000"
			],
			"references": 14,
			"revision": 55,
			"time": "2019-06-15T19:31:09-04:00",
			"created": "1994-04-28T03:00:00-04:00"
		}
	]
}