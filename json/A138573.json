{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A138573",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 138573,
			"data": "0,1,2,5,16,45,130,377,1088,3145,9090,26269,75920,219413,634114,1832625,5296384,15306833,44237570,127848949,369490320,1067846845,3086134658,8919094697,25776662080,74495936025,215297250946,622220603405",
			"name": "a(n) = 2*a(n-1) + 2*a(n-2) + 2*a(n-3) - a(n-4); a(0)=0, a(1)=1, a(2)=2, a(3)=5.",
			"comment": [
				"This is a divisibility sequence; that is, if n divides m, then a(n) divides a(m). - _T. D. Noe_, Dec 23 2008",
				"Case P1 = 2, P2 = -4, Q = 1 of the 3-parameter family of 4th-order linear divisibility sequences found by Williams and Guy. - _Peter Bala_, Mar 04 2014"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A138573/b138573.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Peter Bala, \u003ca href=\"/A100047/a100047.pdf\"\u003eLinear divisibility sequences and Chebyshev polynomials\u003c/a\u003e",
				"H. C. Williams and R. K. Guy, \u003ca href=\"http://www.emis.de/journals/INTEGERS/papers/a17self/a17self.Abstract.html\"\u003eSome Monoapparitic Fourth Order Linear Divisibility Sequences\u003c/a\u003e Integers, Volume 12A (2012) The John Selfridge Memorial Volume",
				"\u003ca href=\"/index/Di#divseq\"\u003eIndex to divisibility sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,2,2,-1)."
			],
			"formula": [
				"a(n) = round(w^n/2/sqrt(5)) where w = (1+r)/(1-r) = 2.89005363826396... and r = sqrt(sqrt(5)-2) = 0.485868271756...; for n \u003e= 3, a(n) = A071101(n+3).",
				"G.f.: -x*(x-1)*(1+x)/(1 - 2*x - 2*x^2 - 2*x^3 + x^4). - _R. J. Mathar_, Jun 03 2009",
				"From _Peter Bala_, Mar 04 2014: (Start)",
				"Define a Lucas sequence {U(n)} in the ring of Gaussian integers by the recurrence U(n) = (1 + i)*U(n-1) + U(n-2) with U(0) = 0 and U(1) = 1. Then a(n) = |U(n)|^2.",
				"Let a, b denote the zeros of x^2 - (1 + i)*x - 1 and c, d denote the zeros of x^2 - (1 - i)*x - 1.",
				"Then a(n) = (a^n - b^n)*(c^n - d^n)/((a - b)*(c - d)).",
				"a(n) = (alpha(1)^n + beta(1)^n - alpha(2)^n - beta(2)^n)/(2*sqrt(5)), where alpha(1), beta(1) are the roots of x^2 - ( 1 + sqrt(5))*x + 1 = 0, and alpha(2), beta(2) are the roots of x^2 - (1 - sqrt(5))*x + 1 = 0.",
				"The o.g.f. is the Hadamard product of the rational functions x/(1 - (1 + i)x - x^2) and x/(1 - (1 - i)x - x^2). (End)",
				"From _Peter Bala_, Mar 24 2014: (Start)",
				"a(n) = (1/sqrt(5))*(T(n,phi) - T(n,-1/phi)), where phi = 1/2*(1 + sqrt(5)) is the golden ratio and T(n,x) denotes the Chebyshev polynomial of the first kind. Compare with the Fibonacci numbers, A000045, whose terms are given by the Binet formula 1/sqrt(5)*( phi^n - (-1/phi)^n ).",
				"a(n) = top right (or bottom left) entry of the 2 X 2 matrix T(n, M), where M is the 2 X 2 matrix [0, 1; 1, 1]; the off-diagonal elements of M^n give the sequence of Fibonacci numbers. Bottom right entry of the matrix T(n, M) gives A138574. See the remarks in A100047 for the general connection between Chebyshev polynomials and linear divisibility sequences of the fourth order. (End)",
				"a(n) = (((phi + sqrt(phi))^n + (phi - sqrt(phi))^n)/2 - (-1)^n * cos(n*arctan(sqrt(phi))))/sqrt(5), where phi=(1+sqrt(5))/2. - _Vladimir Reshetnikov_, May 11 2016",
				"a(n) = A143056(n+1)^2 + A272665(n+1)^2. - _Vladimir Reshetnikov_, Oct 05 2016",
				"Lim_{n -\u003e inf} a(n)/a(n-1) = A318605. - _A.H.M. Smeets_, Sep 12 2018"
			],
			"maple": [
				"seq(coeff(series((x*(1-x)*(x+1))/(1-2*x-2*x^2-2*x^3+x^4),x,n+1), x, n), n = 0 .. 30); # _Muniru A Asiru_, Sep 12 2018"
			],
			"mathematica": [
				"Round@Table[(((GoldenRatio + Sqrt[GoldenRatio])^n + (GoldenRatio - Sqrt[GoldenRatio])^n)/2 - (-1)^n Cos[n ArcTan[Sqrt[GoldenRatio]]])/Sqrt[5], {n, 0, 20}] (* or *) LinearRecurrence[{2, 2, 2, -1}, {0, 1, 2, 5}, 20] (* _Vladimir Reshetnikov_, May 11 2016 *)",
				"Table[Abs[Fibonacci[n, 1 + I]]^2, {n, 0, 20}] (* _Vladimir Reshetnikov_, Oct 05 2016 *)",
				"CoefficientList[Series[-x*(x-1)*(1+x)/(1-2*x-2*x^2-2*x^3+x^4), {x, 0, 20}], x] (* _Stefano Spezia_, Sep 12 2018 *)"
			],
			"program": [
				"(PARI) x='x+O('x^50); concat([0], Vec(x*(1-x)*(1+x)/(1 -2*x -2*x^2 -2*x^3 +x^4))) \\\\ _G. C. Greubel_, Aug 08 2017",
				"(GAP) a:=[0,1,2,5];; for n in [5..30] do a[n]:=2*a[n-1]+2*a[n-2]+2*a[n-3]-a[n-4]; od; a; # _Muniru A Asiru_, Sep 12 2018"
			],
			"xref": [
				"Cf. A071101, A000045, A100047, A138574, A143056, A272665."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Benoit Cloitre_, May 12 2008",
			"references": 5,
			"revision": 59,
			"time": "2018-09-20T04:51:54-04:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}