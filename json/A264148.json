{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A264148",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 264148,
			"data": "1,2,1,-4,1,8,-139,16,-571,-8992,163879,-334144,5246819,698752,-534703531,23349012224,-4483131259,-1357305243136,432261921612371,-6319924923392,6232523202521089,8773495082018816,-25834629665134204969,49004477022654464,-1579029138854919086429",
			"name": "Numerators of rational coefficients related to Stirling's asymptotic series for the Gamma function.",
			"comment": [
				"The rational numbers SGGS = A264148/A264149 (SGGS stands for 'Stirling Generalized Gamma Series') are a supersequence of the coefficients in Stirling's asymptotic series for the Gamma function A001163/A001164 and of an asymptotic expansion of Ramanujan A090804/A065973, further they appear in scaled form in an expansion of -W_{-1}(-e^{-1-x^2/2}) where W_{-1} is Lambert W function A005447/A005446.",
				"Ramanujan's asymptotic expansion theta(n) = 1/3+4/(135n)-8/(2835n^2)- ... is considered in the literature also in the form 1-theta(n) (see for example formula (5) in the Choi link). It is this form to which we refer here."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A264148/b264148.txt\"\u003eTable of n, a(n) for n = 0..360\u003c/a\u003e",
				"K. P. Choi, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9939-1994-1195477-8\"\u003eOn the medians of gamma distributions and an equation of Ramanujan\u003c/a\u003e, Proceedings of the American Mathematical Society 121:1 (May, 1994), pp. 245-251. [From Vladimir Reshetnikov]",
				"G. Nemes, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL13/Nemes/nemes2.html\"\u003eOn the coefficients of the asymptotic expansion of n!\u003c/a\u003e, J. Integer Seqs. 13 (2010), 5. [From Vladimir Reshetnikov]"
			],
			"formula": [
				"Let SGGS(n) = h(n)*doublefactorial(n-1) where h(n) = 1 for n\u003c=0 and for n\u003e0 defined by the recurrence (h(k-1)/k - Sum_{j=1..k-1}((h(k-j)*h(j))/(j+1))/ (1+1/(k+1))) then a(n) = numerator(SGGS(n))."
			],
			"maple": [
				"h := proc(k) option remember; local j; `if`(k\u003c=0, 1,",
				"(h(k-1)/k-add((h(k-j)*h(j))/(j+1), j=1..k-1))/(1+1/(k+1))) end:",
				"SGGS := n -\u003e h(n)*doublefactorial(n-1):",
				"A264148 := n -\u003e numer(SGGS(n)): seq(A264148(n), n=0..24);"
			],
			"mathematica": [
				"h[k_] := h[k] = If[k \u003c= 0, 1, (h[k - 1]/k - Sum[h[k - j]*h[j]/(j + 1), {j, 1, k - 1}]) / (1 + 1/(k + 1))]; a[n_] := h[n]* Factorial2[n - 1] // Numerator; Table[a[n], {n, 0, 24}]"
			],
			"program": [
				"(Sage)",
				"def A264148(n):",
				"    @cached_function",
				"    def h(k):",
				"        if k\u003c=0: return 1",
				"        S = sum((h(k-j)*h(j))/(j+1) for j in (1..k-1))",
				"        return (h(k-1)/k-S)/(1+1/(k+1))",
				"    return numerator(h(n)*(n-1).multifactorial(2))",
				"print([A264148(n) for n in (0..17)])"
			],
			"xref": [
				"A264148(n) = numerator(SGGS(n)).",
				"A264149(n) = denominator(SGGS(n)).",
				"A001163(n) = numerator(SGGS(2*n)) = numerator(SGGS(2*n)/2^(n+1)).",
				"A001164(n) = denominator(SGGS(2*n)).",
				"A090804(n) = numerator(SGGS(2*n+1)).",
				"A065973(n) = denominator(SGGS(2*n+1)) = denominator(SGGS(2*n+1)/2^(n+1)).",
				"A005447(n+1) = numerator(SGGS(n)/2^(n+1)).",
				"A264150(n) = numerator(SGGS(2*n+1)/2^(n+1))."
			],
			"keyword": "sign,frac",
			"offset": "0,2",
			"author": "_Peter Luschny_, Nov 05 2015",
			"references": 5,
			"revision": 42,
			"time": "2020-03-07T06:44:28-05:00",
			"created": "2015-11-08T12:28:31-05:00"
		}
	]
}