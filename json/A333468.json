{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A333468",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 333468,
			"data": "1,2,2,3,5,6,3,4,9,4,7,10,9,14,4,5,7,18,8,10,7,7,14,11,6,26,12,9,29,30,5,6,33,11,21,6,11,15,22,27,41,6,17,8,8,7,22,24,15,50,28,8,53,18,22,14,25,9,15,55,14,50,6,7,65,11,19,34,69,23,35,14,22,74,10",
			"name": "Length of the largest disjoint cycle of the permutation that results from the composition of first n circular shifts.",
			"comment": [
				"Size of the largest part of the partition of n that is associated with the cycle structure of the permutation given by the permutation product (1)*(1,2)*(1,2,3)*...*(1,2,3,...n) after the product is rewritten as the product of disjoint cycles, where * means functional composition, and the permutations are written in cycle form.",
				"Also see Circular shift on Wikipedia.",
				"For n\u003e1, a(n) is always greater than 1, since the given product can never be the identity permutation on the set {1,2,...,n}, which is the only permutation associated with the partition \u003c1,1,...,1\u003e (1 repeated n times).",
				"Connections: The image of 1 in each resulting permutation appears to be the same as the numbers in A003602. The number of parts in the partition associated with each resulting permutation appear to match the numbers in A006694.",
				"The LCM of all cycle lengths gives A051732(n+1). - _Alois P. Heinz_, Apr 08 2020"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A333468/b333468.txt\"\u003eTable of n, a(n) for n = 1..3000\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Circular_shift\"\u003eCircular shift\u003c/a\u003e"
			],
			"formula": [
				"a(n) = n \u003c=\u003e n in { A163782 } union { 1 }. - _Alois P. Heinz_, Apr 08 2020"
			],
			"example": [
				"For n=3, the permutation (1)*(1,2)*(1,2,3)=(1)*(2,3), which is associated with the partition \u003c2,1\u003e of 3. The size of the largest part is 2, so a(3)=2.",
				"For n=11, the permutation (1)*(1,2)*..*(1,2,..11)=(1,2,7,5)*(3,4,8,10,11,6,9) when rewritten as the product of disjoint cycles, which is associated with the partition \u003c7,4\u003e of 11. The size of the largest part is 7, so a(11)=7."
			],
			"program": [
				"(PARI)",
				"Follow(s, f)={my(t=f(s), k=1); while(t\u003es, k++; t=f(t)); if(s==t, k, 0)}",
				"mkp(n)={my(v=vector(n,i,i)); for(k=1, n, my(t=v[1]); for(i=1, k-1, v[i]=v[i+1]); v[k]=t); v}",
				"a(n)={my(v=mkp(n), m=0); for(i=1, n, m=max(m, Follow(i, j-\u003ev[j]))); m} \\\\ _Andrew Howroyd_, Mar 27 2020"
			],
			"xref": [
				"Cf. A003602, A006694, A051732, A071642, A163782."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Richard Locke Peterson_, Mar 22 2020",
			"ext": [
				"Terms a(20) and beyond from _Andrew Howroyd_, Mar 27 2020"
			],
			"references": 1,
			"revision": 41,
			"time": "2020-04-09T17:32:34-04:00",
			"created": "2020-04-09T03:13:16-04:00"
		}
	]
}