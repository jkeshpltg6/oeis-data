{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A136388",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 136388,
			"data": "1,-2,2,1,-5,4,4,-12,8,-1,13,-28,16,-6,38,-64,32,1,-25,104,-144,64,8,-88,272,-320,128,-1,41,-280,688,-704,256,-10,170,-832,1696,-1536,512,1,-61,620,-2352,4096,-3328,1024,12,-292,2072,-6400,9728,-7168,2048",
			"name": "Triangle read by rows of coefficients of Chebyshev-like polynomials P_{n,2}(x) with 0 omitted (exponents in increasing order).",
			"comment": [
				"If U_n(x), T_n(x) are Chebyshev's polynomials then U_n(x)=P_{n,0}(x), T_n(x)=P_{n,1}(x).",
				"Let n\u003e=2 and k be of the same parity. Consider a set X consisting of (n+k)/2-2 main blocks of the size 2 and an additional block of the size 2, then (-1)^((n-k)/2)a(n,k) is the number of n-2-subsets of X intersecting each main block."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A136388/b136388.txt\"\u003eTable of n, a(n) for n = 2..10199\u003c/a\u003e (rows 2 \u003c= n \u003c= 200, flattened).",
				"Milan Janjic, \u003ca href=\"http://www.pmfbl.org/janjic\"\u003eTwo enumerative functions\u003c/a\u003e.",
				"M. Janjic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL11/Janjic/janjic19.html\"\u003eOn a class of polynomials with integer coefficients\u003c/a\u003e, JIS 11 (2008) 08.5.2.",
				"Milan Janjić, \u003ca href=\"https://arxiv.org/abs/1905.04465\"\u003eOn Restricted Ternary Words and Insets\u003c/a\u003e, arXiv:1905.04465 [math.CO], 2019."
			],
			"formula": [
				"If n\u003e=2 and k are of the same parity then a(n,k)= (-1)^((n-k)/2)*sum((-1)^i*binomial((n+k)/2-2, i)*binomial(n+k-2-2*i, n-2), i=0..(n+k)/2-2) and a(n,k)=0 if n and k are of different parity."
			],
			"example": [
				"Rows are (1),(-2,2),(1,-5,4),(4,-12,8),(-1,13,-28,16),...",
				"since P_{2,2}=x^2, P_{3,2}=-2x+2x^3, P_{4,2}=1-5x^2+4x^4,..."
			],
			"maple": [
				"if modp(n-k, 2)=0 then a[n,k]:=(-1)^((n-k)/2)*sum((-1)^i*binomial((n+k)/2-2, i)*binomial(n+k-2-2*i, n-2), i=0..(n+k)/2-2); end if;"
			],
			"mathematica": [
				"Rest@ Flatten@ Table[If[SameQ @@ Mod[{n, k}, 2], (-1)^((n - k)/2)*Sum[(-1)^i*Binomial[(n + k)/2 - 2, i]*Binomial[n + k - 2 - 2 i, n - 2], {i, 0, (n + k)/2 - 2}], 0], {n, 2, 13}, {k, Boole@ OddQ@ n, n, 2}] (* _Michael De Vlieger_, Jul 02 2019 *)"
			],
			"xref": [
				"Cf. A008310, A053117."
			],
			"keyword": "sign,tabf",
			"offset": "2,2",
			"author": "_Milan Janjic_, Mar 30 2008, entry revised Apr 05 2008",
			"references": 1,
			"revision": 12,
			"time": "2019-07-02T14:13:07-04:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}