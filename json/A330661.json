{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A330661",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 330661,
			"data": "1,1,2,1,2,3,1,3,4,5,1,3,5,6,7,1,5,8,9,10,11,1,5,9,12,13,14,15,1,8,13,18,19,20,21,22,1,8,19,22,26,27,28,29,30,1,13,22,30,37,38,39,40,41,42,1,13,30,41,46,51,52,53,54,55,56,1,20,44,59,62,71,72,73,74,75,76,77",
			"name": "T(n,k) is the index within the partitions of n in canonical ordering of the k-th partition whose parts differ pairwise by at most one.",
			"comment": [
				"For each length k in [1..n] there is exactly one such partition [p_1,...,p_k], with p_i = a+1 for i=1..j and p_i = a for i=j+1..k, where a = floor(n/k) and j = n - k * a.",
				"If k | n, then all parts p_i are equal. A027750 lists the indices of these partitions in this triangle.",
				"Canonical ordering is also known as graded reverse lexicographic ordering, see A080577 or link below."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A330661/b330661.txt\"\u003eRows n = 1..200, flattened\u003c/a\u003e",
				"OEIS Wiki, \u003ca href=\"http://oeis.org/wiki/Orderings of partitions#A_comparison\"\u003eOrderings of partitions (a comparison)\u003c/a\u003e."
			],
			"formula": [
				"T(n,1) = 1.",
				"T(n,n) = A000041(n).",
				"T(n,k) = A000041(n) - (n - k) for k = ceiling(n/2)..n.",
				"T(2n,2) = T(2n+1,2) = A216053(n). - _Alois P. Heinz_, Jan 28 2020"
			],
			"example": [
				"Partitions of 8 in canonical ordering begin: 8, 71, 62, 611, 53, 521, 5111, 44, 431, 422, 4211, 41111, 332, ... . The partitions whose parts differ pairwise by at most one in this list are 8, 44, 332, ... at indices 1, 8, 13, ... and this gives row 8 of this triangle.",
				"Triangle T(n,k) begins:",
				"  1;",
				"  1,  2;",
				"  1,  2,  3;",
				"  1,  3,  4,  5;",
				"  1,  3,  5,  6,  7;",
				"  1,  5,  8,  9, 10, 11;",
				"  1,  5,  9, 12, 13, 14, 15;",
				"  1,  8, 13, 18, 19, 20, 21, 22;",
				"  1,  8, 19, 22, 26, 27, 28, 29, 30;",
				"  1, 13, 22, 30, 37, 38, 39, 40, 41, 42;",
				"  ..."
			],
			"maple": [
				"b:= proc(l) option remember; (n-\u003e `if`(n=0, 1,",
				"      b(subsop(1=[][], l))+g(n, l[1]-1)))(add(j, j=l))",
				"    end:",
				"g:= proc(n, i) option remember; `if`(n=0 or i=1, 1,",
				"     `if`(i\u003c1, 0, g(n-i, min(n-i, i))+g(n, i-1)))",
				"    end:",
				"T:= proc(n, k) option remember; 1 + g(n$2)-",
				"      b((q-\u003e [q+1$r, q$k-r])(iquo(n, k, 'r')))",
				"    end:",
				"seq(seq(T(n, k), k=1..n), n=1..14);  # _Alois P. Heinz_, Feb 19 2020"
			],
			"mathematica": [
				"b[l_List] := b[l] = Function[n, If[n == 0, 1, b[ReplacePart[l, 1 -\u003e Nothing]] + g[n, l[[1]] - 1]]][Total[l]];",
				"g[n_, i_] := g[n, i] = If[n == 0 || i == 1, 1, If[i \u003c 1, 0, g[n - i, Min[n - i, i]] + g[n, i - 1]]];",
				"T[n_, k_] := T[n, k] = Module[{q, r}, {q, r} = QuotientRemainder[n, k]; 1 + g[n, n] - b[Join[Table[q + 1, {r}], Table[q, {k - r}]]]];",
				"Table[T[n, k], {n, 1, 14}, {k, 1, n}] // Flatten (* _Jean-François Alcover_, Apr 29 2020, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(PARI)",
				"balP(p) = p[1]-p[#p]\u003c=1",
				"Row(n)={v=vecsort([Vecrev(p) | p\u003c-partitions(n)], , 4);select(i-\u003ebalP(v[i]),[1..#v])}",
				"{ for(n=1, 10, print(Row(n))) }"
			],
			"xref": [
				"Cf. A000041, A063008, A027750, A080577, A216053, A238639, A238640.",
				"T(2n,n) gives A332706."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Peter Dolland_, Dec 23 2019",
			"references": 8,
			"revision": 36,
			"time": "2020-04-29T07:36:53-04:00",
			"created": "2020-02-20T19:36:08-05:00"
		}
	]
}