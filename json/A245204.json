{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A245204",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 245204,
			"data": "1,2,2,4,1,1,5,1,-2,-6,10,14,5,7,7,-28,-12,13,14,26,-21,-31,-13,-10,-11,-7,-6,5,2,-21,2,33,-15,-24,34,71,-15,24,9,37,73,-18,-84,-65,9,-90,-65,-47,97,-64,-100,-8,41,81,-81,-71,-65,-70,113,10,-80,119,57,78,20,124,167,-71,-48",
			"name": "The unique integer r with |r| \u003c prime(n)/2 such that E_{prime(n)-3}(1/4) == r (mod prime(n)), where E_m(x) denotes the Euler polynomial of degree m.",
			"comment": [
				"Conjecture: a(n) = 0 infinitely often. In other words, there are infinitely many odd primes p such that E_{p-3}(1/4) == 0 (mod p) (equivalently, p divides A001586(p-3)).",
				"This seems reasonable in view of the standard heuristic arguments. The first n with a(n) = 0 is 171 with prime(171) = 1019. The next such a number n is greater than 2600 and hence prime(n) \u003e 23321.",
				"Zhi-Wei Sun made many conjectures on congruences involving E_{p-3}(1/4), see the reference."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A245204/b245204.txt\"\u003eTable of n, a(n) for n = 2..1300\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1001.4453\"\u003eSuper congruences and Euler numbers\u003c/a\u003e, arXiv:1001.4453 [math.NT].",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.1007/s11425-011-4302-x\"\u003eSuper congruences and Euler numbers\u003c/a\u003e, Sci. China Math. 54(2011), 2509-2535."
			],
			"example": [
				"a(3) = 2 since E_{prime(3)-3}(1/4) = E_2(1/4) = -3/16 == 2 (mod prime(3)=5)."
			],
			"mathematica": [
				"rMod[m_,n_]:=Mod[Numerator[m]*PowerMod[Denominator[m],-1,n],n,-n/2]",
				"a[n_]:=rMod[EulerE[Prime[n]-3,1/4],Prime[n]]",
				"Table[a[n],{n,2,70}]"
			],
			"xref": [
				"Cf. A000040, A001586, A122045, A198245, A245089, A245206."
			],
			"keyword": "sign",
			"offset": "2,2",
			"author": "_Zhi-Wei Sun_, Jul 13 2014",
			"references": 2,
			"revision": 13,
			"time": "2014-07-13T17:09:14-04:00",
			"created": "2014-07-13T17:09:14-04:00"
		}
	]
}