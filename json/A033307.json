{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A033307",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 33307,
			"data": "1,2,3,4,5,6,7,8,9,1,0,1,1,1,2,1,3,1,4,1,5,1,6,1,7,1,8,1,9,2,0,2,1,2,2,2,3,2,4,2,5,2,6,2,7,2,8,2,9,3,0,3,1,3,2,3,3,3,4,3,5,3,6,3,7,3,8,3,9,4,0,4,1,4,2,4,3,4,4,4,5,4,6,4,7,4,8,4,9,5,0,5,1,5,2,5,3,5,4,5,5,5,6,5",
			"name": "Decimal expansion of Champernowne constant (or Mahler's number), formed by concatenating the positive integers.",
			"comment": [
				"This number is known to be normal in base 10.",
				"Named after David Gawen Champernowne (July 9, 1912 - August 19, 2000). - _Robert G. Wilson v_, Jun 29 2014"
			],
			"reference": [
				"G. Harman, One hundred years of normal numbers, in M. A. Bennett et al., eds., Number Theory for the Millennium, II (Urbana, IL, 2000), 149-166, A K Peters, Natick, MA, 2002.",
				"C. A. Pickover, The Math Book, Sterling, NY, 2009; see p. 364.",
				"H. M. Stark, An Introduction to Number Theory. Markham, Chicago, 1970, p. 172."
			],
			"link": [
				"Harry J. Smith, \u003ca href=\"/A033307/b033307.txt\"\u003eTable of n, a(n) for n = 0..20000\u003c/a\u003e",
				"D. H. Bailey and R. E. Crandall, \u003ca href=\"http://www.emis.de/journals/EM/expmath/volumes/11/11.4/pp527_546.pdf\"\u003eRandom Generators and Normal Numbers\u003c/a\u003e, Exper. Math. 11, 527-546, 2002.",
				"Maya Bar-Hillel and Willem A. Wagenaar, \u003ca href=\"https://doi.org/10.1016/0196-8858(91)90029-I\"\u003eThe perception of randomness\u003c/a\u003e, Advances in applied mathematics 12.4 (1991): 428-454. See page 428.",
				"Edward B. Burger, \u003ca href=\"http://www.maa.org/sites/default/files/pdf/upload_library/22/Chauvenet/Burger.pdf\"\u003eDiophantine Olympics and World Champions: Polynomials and Primes Down Under\u003c/a\u003e, Amer. Math. Monthly, 107 (Nov. 2000), 822-829.",
				"Chess Programming Wiki, \u003ca href=\"https://chessprogramming.wikispaces.com/David+Champernowne\"\u003eDavid Champernowne\u003c/a\u003e.",
				"D. G. Champernowne, \u003ca href=\"http://jlms.oxfordjournals.org/content/s1-8/4/254.extract\"\u003eThe Construction of Decimals Normal in the Scale of Ten\u003c/a\u003e, J. London Math. Soc., 8 (1933), 254-260.",
				"Arthur H. Copeland and Paul Erdős, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9904-1946-08657-7\"\u003eNote on Normal Numbers\u003c/a\u003e, Bull. Amer. Math. Soc. 52, 857-860, 1946.",
				"Peyman Nasehpour, \u003ca href=\"https://arxiv.org/abs/1806.07560\"\u003eA Simple Criterion for Irrationality of Some Real Numbers\u003c/a\u003e, arXiv:1806.07560 [math.AC], 2018.",
				"Simon Plouffe, \u003ca href=\"http://www.worldwideschool.org/library/books/sci/math/MiscellaneousMathematicalConstants/chap12.html\"\u003eChampernowne constant, the natural integers concatenated\u003c/a\u003e",
				"Simon Plouffe, \u003ca href=\"http://www.plouffe.fr/simon/constants/champernowne.txt\"\u003eChampernowne constant, the natural integers concatenated\u003c/a\u003e",
				"Simon Plouffe, \u003ca href=\"http://www.plouffe.fr/simon/gendev/123456.html\"\u003eGeneralized expansion of real constants\u003c/a\u003e",
				"Paul Pollack and Joseph Vandehey, \u003ca href=\"http://arxiv.org/abs/1405.6266\"\u003eBesicovitch, Bisection, and the normality of 0.(1)(4)(9)(16)(25)...\u003c/a\u003e, arXiv:1405.6266 [math.NT], 2014.",
				"Paul Pollack and Joseph Vandehey, \u003ca href=\"http://www.jstor.org/stable/10.4169/amer.math.monthly.122.8.757\"\u003eBesicovitch, Bisection, and the Normality of 0.(1)(4)(9)(16)(25)...\u003c/a\u003e, The American Mathematical Monthly 122.8 (2015): 757-765.",
				"John K. Sikora, \u003ca href=\"http://arxiv.org/abs/1210.1263\"\u003eOn the High Water Mark Convergents of Champernowne's Constant in Base Ten\u003c/a\u003e, arXiv:1210.1263 [math.NT], 2012.",
				"John K. Sikora, \u003ca href=\"http://arxiv.org/abs/1408.0261\"\u003eAnalysis of the High Water Mark Convergents of Champernowne's Constant in Various Bases\u003c/a\u003e, arXiv:1408.0261 [math.NT], 2014.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ChampernowneConstant.html\"\u003eChampernowne constant\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/D._G._Champernowne\"\u003eD. G. Champernowne\u003c/a\u003e.",
				"Hector Zenil, N. Kiani and J. Tegner, \u003ca href=\"https://arxiv.org/abs/1608.05972\"\u003eLow Algorithmic Complexity Entropy-deceiving Graphs\u003c/a\u003e, arXiv preprint arXiv:1608.05972 [cs.IT], 2016."
			],
			"formula": [
				"Let \"index\" i = ceiling( W(log(10)/10^(1/9) (n - 1/9))/log(10) + 1/9 ) where W denotes the principal branch of the Lambert W function. Then a(n) = (10^((n + (10^i - 10)/9) mod i - i + 1) * ceiling((9n + 10^i - 1)/(9i) - 1)) mod 10. See also Mathematica code. - David W. Cantrell, Feb 18 2007"
			],
			"example": [
				"0.12345678910111213141516171819202122232425262728293031323334353637383940414243..."
			],
			"mathematica": [
				"Flatten[IntegerDigits/@Range[0, 57]] (* Or *)",
				"almostNatural[n_, b_] := Block[{m = 0, d = n, i = 1, l, p}, While[m \u003c= d, l = m; m = (b - 1) i*b^(i - 1) + l; i++]; i--; p = Mod[d - l, i]; q = Floor[(d - l)/i] + b^(i - 1); If[p != 0, IntegerDigits[q, b][[p]], Mod[q - 1, b]]]; Array[ almostNatural[#, 10] \u0026, 105] (* _Robert G. Wilson v_, Jul 23 2012 and modified Jul 04 2014 *)",
				"intermediate[n_] := Ceiling[FullSimplify[ProductLog[Log[10]/10^(1/9) (n - 1/9)] / Log[10] + 1/9]]; champerDigit[n_] := Mod[Floor[10^(Mod[n + (10^intermediate[n] - 10)/9, intermediate[n]] - intermediate[n] + 1) Ceiling[(9n + 10^intermediate[n] - 1)/(9intermediate[n]) - 1]], 10]; (* David W. Cantrell, Feb 18 2007 *)"
			],
			"program": [
				"(PARI) { default(realprecision, 20080); x=0; y=1; d=10.0; e=1.0; n=0; while (y!=x, y=x; n++; if (n==d, d=d*10); e=e*d; x=x+n/e; ); d=0; for (n=0, 20000, x=(x-d)*10; d=floor(x); write(\"b033307.txt\", n, \" \", d)); } \\\\ _Harry J. Smith_, Apr 20 2009",
				"(Haskell)",
				"a033307 n = a033307_list !! n",
				"a033307_list = concatMap (map (read . return) . show) [1..] :: [Int]",
				"-- _Reinhard Zumkeller_, Aug 27 2013, Mar 28 2011",
				"(MAGMA) \u0026cat[Reverse(IntegerToSequence(n)):n in[1..50]]; // _Jason Kimberley_, Dec 07 2012",
				"(Scala) val numerStr = (1 to 100).map(Integer.toString(_)).toList.reduce(_ + _)",
				"numerStr.split(\"\").map(Integer.parseInt(_)).toList // _Alonso del Arte_, Nov 04 2019",
				"(Python)",
				"from itertools import count",
				"def agen():",
				"    for k in count(1): yield from list(map(int, str(k)))",
				"a = agen()",
				"print([next(a) for i in range(104)]) # _Michael S. Branicky_, Sep 13 2021"
			],
			"xref": [
				"See A030167 for the continued fraction expansion of this number.",
				"A007376 is the same sequence but with a different interpretation.",
				"Cf. A007908, A000027, A001191 (concatenate squares).",
				"Tables in which the n-th row lists the base b digits of n: A030190 and A030302 (b = 2), A003137 and A054635 (b = 3), A030373 (b = 4), A031219 (b = 5), A030548 (b = 6), A030998 (b = 7), A031035 and A054634 (b = 8), A031076 (b = 9), A007376 and this sequence (b = 10). - _Jason Kimberley_, Dec 06 2012",
				"Cf. A065648."
			],
			"keyword": "nonn,base,cons,easy",
			"offset": "0,2",
			"author": "_Eric W. Weisstein_",
			"references": 139,
			"revision": 133,
			"time": "2021-09-13T09:05:45-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}