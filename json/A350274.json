{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A350274",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 350274,
			"data": "1,1,2,6,23,1,109,1,10,619,16,45,40,4108,92,210,420,210,31240,771,1645,2800,2520,1344,268028,6883,17325,15960,26460,18144,10080,2562156,68914,173250,148400,226800,211680,151200,86400,27011016,757934,1854930,1798720,1801800,2494800,1940400,1425600,831600",
			"name": "Triangle read by rows: T(n,k) is the number of n-permutations whose fourth-shortest cycle has length exactly k; n \u003e= 0, 0 \u003c= k \u003c= max(0,n-3).",
			"comment": [
				"If the permutation has no fourth cycle, then its fourth-longest cycle is defined to have length 0."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A350274/b350274.txt\"\u003eRows n = 0..100, flattened\u003c/a\u003e"
			],
			"formula": [
				"Sum_{k=0..n-3} k * T(n,k) = A332908(n) for n \u003e= 4."
			],
			"example": [
				"Triangle begins:",
				"[0]      1;",
				"[1]      1;",
				"[2]      2;",
				"[3]      6;",
				"[4]     23,    1;",
				"[5]    109,    1,    10;",
				"[6]    619,   16,    45,    40;",
				"[7]   4108,   92,   210,   420,   210;",
				"[8]  31240,  771,  1645,  2800,  2520,  1344;",
				"[9] 268028, 6883, 17325, 15960, 26460, 18144, 10080;",
				"    ..."
			],
			"maple": [
				"m:= infinity:",
				"b:= proc(n, l) option remember; `if`(n=0, x^`if`(l[4]=m,",
				"      0, l[4]), add(b(n-j, sort([l[], j])[1..4])",
				"               *binomial(n-1, j-1)*(j-1)!, j=1..n))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=0..degree(p)))(b(n, [m$4])):",
				"seq(T(n), n=0..11);  # _Alois P. Heinz_, Dec 22 2021"
			],
			"mathematica": [
				"m = Infinity;",
				"b[n_, l_] := b[n, l] = If[n == 0, x^If[l[[4]] == m, 0, l[[4]]], Sum[b[n-j, Sort[Append[l, j]][[1 ;; 4]]]*Binomial[n-1, j-1]*(j-1)!, {j, 1, n}]];",
				"T[n_] := With[{p = b[n, {m, m, m, m}]}, Table[Coefficient[p, x, i], {i, 0, Exponent[p, x]}]];",
				"Table[T[n], {n, 0, 11}] // Flatten (* _Jean-François Alcover_, Dec 29 2021, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Row sums give A000142.",
				"Cf. A332908."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_Steven Finch_, Dec 22 2021",
			"ext": [
				"More terms from _Alois P. Heinz_, Dec 22 2021"
			],
			"references": 1,
			"revision": 18,
			"time": "2021-12-29T05:54:13-05:00",
			"created": "2021-12-22T14:24:26-05:00"
		}
	]
}