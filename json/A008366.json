{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008366",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8366,
			"data": "1,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97,101,103,107,109,113,127,131,137,139,149,151,157,163,167,173,179,181,191,193,197,199,211,223,227,229,233,239,241,251,257,263,269,271,277,281,283",
			"name": "Smallest prime factor is \u003e= 17.",
			"comment": [
				"Also the 17-rough numbers: positive integers that have no prime factors less than 17. - _Michael B. Porter_, Oct 10 2009",
				"a(n) - (1001/192) n is periodic with period 5760. - _Robert Israel_, Mar 18 2016",
				"From _Peter Bala_, May 12 2018: (Start)",
				"The product of two 17-rough numbers is a 17-rough number and the prime factors of a 17-rough number are 17-rough numbers.",
				"Let k equal either 13, 14, 15 or 16. Then the product of k numbers n*(n + a)*(n + 2*a)*...*(n + (k-1)*a) in arithmetical progression is divisible by k! for all integer n if and only if a is a 17-rough number.",
				"The sequence terms satisfy the congruence x^60 = 1 (mod 30030), where 30030 = 2*3*5*7*11*13. (End)",
				"The asymptotic density of this sequence is 192/1001. - _Amiram Eldar_, Sep 30 2020"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A008366/b008366.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Peter Bala, \u003ca href=\"/A008366/a008366_1.pdf\"\u003eA property of p-rough numbers\u003c/a\u003e.",
				"Benedict Irwin, \u003ca href=\"/A008366/a008366.txt\"\u003eGenerating Function\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RoughNumber.html\"\u003eRough Number\u003c/a\u003e.",
				"\u003ca href=\"/index/Sk#smooth\"\u003eIndex entries for sequences related to smooth numbers\u003c/a\u003e [_Michael B. Porter_, Oct 10 2009]"
			],
			"formula": [
				"Numbers n \u003e 1 such that ((Sum_{k=1..n} k^10) mod n = 0) and ((Sum_{k=1..n} k^12) mod n = 0) (conjecture). - _Gary Detlefs_, Dec 27 2011",
				"a(n) = a(n-1) + a(n-5760) - a(n-5761). - _Vaclav Kotesovec_, Mar 18 2016",
				"G.f: x*P(x)/(1 - x - x^5760 + x^5761) where P(x) is a polynomial of degree 5760. - _Benedict W. J. Irwin_, Mar 23 2016"
			],
			"maple": [
				"for i from 1 to 500 do if gcd(i,30030) = 1 then print(i); fi; od;"
			],
			"mathematica": [
				"Select[ Range[ 300 ], GCD[ #1, 30030 ]==1\u0026 ]",
				"Join[{1},Select[Range[300],FactorInteger[#][[1,1]]\u003e=17\u0026]] (* _Harvey P. Dale_, Mar 28 2020 *)"
			],
			"program": [
				"(PARI) isA008366(n) = gcd(n,30030)==1 \\\\ _Michael B. Porter_, Oct 10 2009"
			],
			"xref": [
				"For k-rough numbers with other values of k, see A000027 A005408 A007310 A007775 A008364 A008365 A008366 A166061 A166063. - _Michael B. Porter_, Oct 10 2009",
				"Cf. A005867."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"references": 13,
			"revision": 50,
			"time": "2020-09-30T03:28:44-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}