{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A051664",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 51664,
			"data": "2,2,3,2,5,3,7,2,3,5,11,3,13,7,7,2,17,3,19,5,9,11,23,3,5,13,3,7,29,7,31,2,15,17,17,3,37,19,17,5,41,9,43,11,7,23,47,3,7,5,23,13,53,3,17,7,25,29,59,7,61,31,9,2,31,15,67,17,31,17,71,3,73,37,7,19,31,17,79,5,3",
			"name": "a(n) is the number of nonzero coefficients in the n-th cyclotomic polynomial.",
			"comment": [
				"a(n)=p(n) if n=p(n); a(n) is not always A006530(n). - _Labos Elemer_, May 03 2002",
				"This sequence is the Mobius transform of A087073. Let m be the squarefree part of n, then a(n) = a(m). When n = pq, the product of two distinct odd primes, then there is a formula for a(pq). Let x = 1/p (mod q) and y = 1/q (mod p). Then a(pq) = 2xy-1. There are also formulas for the number of positive and negative terms. See papers by Carlitz or Lam and Leung. - _T. D. Noe_, Aug 08 2003"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A051664/b051664.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"L. Carlitz, \u003ca href=\"http://www.jstor.org/stable/2314500\"\u003eNumber of terms in the cyclotomic polynomial F(pq,x)\u003c/a\u003e, Amer. Math. Monthly, Vol. 73, No. 9, 1966, pp. 979-981.",
				"T. Y. Lam and K. H. Leung, \u003ca href=\"http://www.jstor.org/stable/2974668\"\u003eOn the Cyclotomic Polynomial Phi(pq,x)\u003c/a\u003e, Amer. Math. Monthly, Vol. 103, No. 7, 1996, pp. 562-564.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CyclotomicPolynomial.html\"\u003eCyclotomic Polynomial\u003c/a\u003e"
			],
			"formula": [
				"a(n) = phi(n) + 1 - A086798(n). - _T. D. Noe_, Aug 08 2003"
			],
			"example": [
				"9th cyclotomic polynomial is x^6+x^3+1 which has 3 terms, so a(9)=3."
			],
			"maple": [
				"A051664 := proc(n)",
				"        numtheory[cyclotomic](n,x) ;",
				"        nops([coeffs(%)]) ;",
				"end proc: # _R. J. Mathar_, Sep 15 2012"
			],
			"mathematica": [
				"Table[Count[CoefficientList[Cyclotomic[n, x], x], _?(#!=0\u0026)], {n, 0, 100}]",
				"Table[Length[Cyclotomic[n, x]], {n, 1, 100}] (* _Artur Jasinski_, Jan 15 2007 *)"
			],
			"program": [
				"(PARI) a(n)=sum(k=0,eulerphi(n),if(polcoeff(polcyclo(n),k),1,0))",
				"(PARI) a(n) = #select(x-\u003ex!=0, Vec(polcyclo(n))); \\\\ _Michel Marcus_, Mar 05 2017"
			],
			"xref": [
				"Cf. A086765 (number of positive terms in n-th cyclotomic polynomial), A086780 (number of negative terms in n-th cyclotomic polynomial), A086798 (number of zero terms in n-th cyclotomic polynomial), A087073."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Jud McCranie_",
			"ext": [
				"More terms from _Labos Elemer_, May 03 2002"
			],
			"references": 13,
			"revision": 31,
			"time": "2019-05-19T01:43:33-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}