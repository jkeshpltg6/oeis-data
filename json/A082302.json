{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A082302",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 82302,
			"data": "1,6,42,330,2814,25422,239442,2326434,23151030,234784662,2417832186,25216231866,265796560302,2827138163550,30306009654690,327081253546770,3551148743559270,38758882760119590,425024567305557450",
			"name": "Expansion of g.f.: (1 - 5*x - sqrt(25*x^2 - 14*x + 1))/(2*x).",
			"comment": [
				"More generally coefficients of (1 - m*x - sqrt(m^2*x^2 - (2*m + 4)*x + 1))/(2*x) are given by a(0)=1 and, for n \u003e 0, a(n) = (1/n)*Sum_{k=0..n} (m+1)^k*C(n,k)*C(n,k-1).",
				"Hankel transform is 6^C(n+1,2). - _Philippe Deléham_, Feb 11 2009"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A082302/b082302.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"Paul Barry, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL9/Barry/barry91.html\"\u003eOn Integer-Sequence-Based Constructions of Generalized Pascal Triangles\u003c/a\u003e, Journal of Integer Sequences, Vol. 9 (2006), Article 06.2.4.",
				"Paul Barry, \u003ca href=\"https://www.emis.de/journals/JIS/VOL22/Barry3/barry422.html\"\u003eGeneralized Catalan Numbers Associated with a Family of Pascal-like Triangles\u003c/a\u003e, J. Int. Seq., Vol. 22 (2019), Article 19.5.8."
			],
			"formula": [
				"Equals 6*A078018(n) for n \u003e 0.",
				"a(0)=1; for n \u003e 0, a(n) = (1/n)*Sum_{k=0..n} 6^k*C(n, k)*C(n, k-1).",
				"D-finite with recurrence: (n+1)*a(n) + 7*(1-2n)*a(n-1) + 25*(n-2)*a(n-2) = 0. - _R. J. Mathar_, Dec 08 2011",
				"a(n) ~ sqrt(12 + 7*sqrt(6))*(7 + 2*sqrt(6))^n/(2*sqrt(Pi)*n^(3/2)). - _Vaclav Kotesovec_, Oct 14 2012",
				"a(n) = 6*hypergeom([1 - n, -n], [2], 6) for n \u003e 0. - _Peter Luschny_, May 22 2017",
				"G.f.: 1/(1 - 5*x - x/(1 - 5*x - x/(1 - 5*x - x/(1 - 5*x - x/(1 - ...))))), a continued fraction. - _Ilya Gutkovskiy_, Apr 04 2018"
			],
			"maple": [
				"A082302_list := proc(n) local j, a, w; a := array(0..n); a[0] := 1;",
				"for w from 1 to n do a[w] := 6*a[w-1]+add(a[j]*a[w-j-1], j=1..w-1) od; convert(a,list)end: A082302_list(18); # _Peter Luschny_, May 19 2011",
				"a := n -\u003e `if`(n=0, 1, 6*hypergeom([1 - n, -n], [2], 6)):",
				"seq(simplify(a(n)), n=0..18); # _Peter Luschny_, May 22 2017"
			],
			"mathematica": [
				"Table[SeriesCoefficient[(1-5*x-Sqrt[25*x^2-14*x+1])/(2*x),{x,0,n}],{n,0,20}] (* _Vaclav Kotesovec_, Oct 14 2012 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c1,1,sum(k=0,n,6^k*binomial(n,k)*binomial(n,k-1))/n)",
				"(PARI) x='x+O('x^99); Vec((1-5*x-(25*x^2-14*x+1)^(1/2))/(2*x)) \\\\ _Altug Alkan_, Apr 04 2018",
				"(GAP) Concatenation([1],List([1..20],n-\u003e(1/n)*Sum([0..n],k-\u003e6^k*Binomial(n,k)*Binomial(n,k-1)))); # _Muniru A Asiru_, Apr 05 2018",
				"(MAGMA) m:=50; R\u003cx\u003e:=PowerSeriesRing(Rationals(), m); Coefficients(R!((1-5*x-Sqrt(25*x^2-14*x+1))/(2*x))); // _G. C. Greubel_, Aug 16 2018"
			],
			"xref": [
				"Cf. A006318, A047891."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Benoit Cloitre_, May 10 2003",
			"references": 6,
			"revision": 47,
			"time": "2020-01-30T21:29:15-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}