{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A191431",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 191431,
			"data": "1,2,3,4,5,6,7,8,9,10,11,12,14,15,13,16,18,21,22,19,17,24,26,31,32,28,25,20,35,38,45,46,41,36,29,23,50,55,65,66,59,52,42,33,27,72,79,93,94,84,74,60,48,39,30,103,113,132,134,120,106,86,69,56,43,34,147,161,188,190,171,151,123,98,80,62,49,37",
			"name": "Dispersion of ([nx+x]), where x=sqrt(2) and [ ]=floor, by antidiagonals.",
			"comment": [
				"Background discussion:  Suppose that s is an increasing sequence of positive integers, that the complement t of s is infinite, and that t(1)=1.  The dispersion of s is the array D whose n-th row is (t(n), s(t(n)), s(s(t(n)), s(s(s(t(n)))), ...).  Every positive integer occurs exactly once in D, so that, as a sequence, D is a permutation of the positive integers.  The sequence u given by u(n)=(number of the row of D that contains n) is a fractal sequence.  Examples:",
				"(1) s=A000040 (the primes), D=A114537, u=A114538.",
				"(2) s=A022343 (without initial 0), D=A035513 (Wythoff array), u=A003603.",
				"(3) s=A007067, D=A035506 (Stolarsky array), u=A133299.",
				"More recent examples of dispersions: A191426-A191455."
			],
			"example": [
				"Northwest corner:",
				"1.....2....4....7...11...16",
				"3.....5....8...12...18...26",
				"6.....9...14...21...31...45",
				"10...15...22...32...46...66",
				"13...19...28...41...59...84"
			],
			"mathematica": [
				"(* Program generates the dispersion array T of increasing sequence f[n] *)",
				"r = 40; r1 = 12; (* r=# rows of T, r1=# rows to show *)",
				"c = 40; c1 = 12; (* c=# cols of T, c1=# cols to show *)",
				"x = Sqrt[2];",
				"f[n_] := Floor[n*x + x] (* f(n) is complement of column 1 *)",
				"mex[list_] :=",
				"NestWhile[#1 + 1 \u0026, 1, Union[list][[#1]] \u003c= #1 \u0026, 1,",
				"  Length[Union[list]]]",
				"rows = {NestList[f, 1, c]};",
				"Do[rows = Append[rows, NestList[f, mex[Flatten[rows]], r]], {r}];",
				"t[i_, j_] := rows[[i, j]];",
				"TableForm[",
				"Table[t[i, j], {i, 1, 10}, {j, 1, 10}]]",
				"(* A191431 array *)",
				"Flatten[Table[t[k, n - k + 1], {n, 1, c1}, {k, 1, n}]]",
				"(* A191431 sequence *)",
				"(* Program by _Peter J. C. Moses_, Jun 01 2011 *)"
			],
			"xref": [
				"Cf. A114537, A035513, A035506."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Jun 03 2011",
			"references": 1,
			"revision": 11,
			"time": "2014-02-14T00:28:31-05:00",
			"created": "2011-06-05T06:29:23-04:00"
		}
	]
}