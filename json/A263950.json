{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A263950",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 263950,
			"data": "1,1,1,1,3,1,1,4,7,1,1,6,13,15,1,1,6,28,40,31,1,1,12,31,120,121,63,1,1,8,91,156,496,364,127,1,1,12,57,600,781,2016,1093,255,1,1,12,112,400,3751,3906,8128,3280,511,1,1,18,117,960,2801,22932,19531,32640",
			"name": "Array read by antidiagonals: T(n,k) is the number of lattices L in Z^k such that the quotient group Z^k / L is C_n.",
			"comment": [
				"All the enumerated lattices have full rank k, since the quotient group is finite.",
				"For m\u003e=1, T(n,k) is the number of lattices L in Z^k such that the quotient group Z^k / L is C_nm x (C_m)^(k-1); and also, (C_nm)^(k-1) x C_m.",
				"Also, number of subgroups of (C_n)^k isomorphic to C_n (and also, to (C_n)^{k-1}), cf. [Butler, Lemma 1.4.1].",
				"T(n,k) is the sum of the divisors d of n^(k-1) such that n^(k-1)/d is k-free. Namely, the coefficient in n^(-(k-1)*s) of the Dirichlet series zeta(s) * zeta(s-1) / zeta(ks).",
				"Also, number of isomorphism classes of connected (C_n)-fold coverings of a connected graph with circuit rank k.",
				"Columns are multiplicative functions."
			],
			"reference": [
				"L. M. Butler, Subgroup lattices and symmetric functions. Mem. Amer. Math. Soc., vol. 112, no. 539, 1994.",
				"J. H. Kwak and J. Lee, Enumeration of graph coverings, surface branched coverings and related group theory, in Combinatorial and Computational Mathematics (Pohang, 2000), ed. S. Hong et al., World Scientific, Singapore 2001, pp. 97-161. [Examples 2 and 3]"
			],
			"link": [
				"Álvar Ibeas, \u003ca href=\"/A263950/b263950.txt\"\u003eFirst 100 antidiagonals, flattened\u003c/a\u003e",
				"J. H. Kwak, J.-H. Chun, and J. Lee, \u003ca href=\"http://dx.doi.org/10.1137/S0895480196304428\"\u003eEnumeration of regular graph coverings having finite abelian covering transformation groups\u003c/a\u003e, SIAM J. Discrete Math. 11(2), 1998, pp. 273-285. [Page 277]"
			],
			"formula": [
				"T(n,k) = J_k(n) / J_1(n) = (Sum_{d|n} mu(n/d) * d^k) / phi(n).",
				"T(n,k) = n^(k-1) * Product_{p|n, p prime} (p^k - 1) / ((p - 1) * p^(k-1)).",
				"Dirichlet g.f. of k-th column: zeta(s-k+1) * Product_{p prime} (1 + p^(-s) + p^(1-s) + ... + p^(k-2-s)).",
				"If n is squarefree, T(n,k) = A160870(n,k) = A000203(n^(k-1))."
			],
			"example": [
				"There are 7 = A160870(4,2) lattices of volume 4 in Z^2. Among them, only one (\u003c(2,0), (0,2)\u003e) gives the quotient group C_2 x C_2, whereas the rest give C_4. Hence, T(4,2) = 6 and T(1,2) = 1.",
				"Array begins:",
				"      k=1    k=2    k=3    k=4    k=5    k=6",
				"n=1     1      1      1      1      1      1",
				"n=2     1      3      7     15     31     63",
				"n=3     1      4     13     40    121    364",
				"n=4     1      6     28    120    496   2016",
				"n=5     1      6     31    156    781   3906",
				"n=6     1     12     91    600   3751  22932"
			],
			"xref": [
				"Rows (1-11): A000012, A000225, A003462, A006516, A003463, A160869, A023000, A016152, A016142(n-1), A046915(n-1), A016123(n-1).",
				"Columns (1-17): A000012, A001615, A160889, A160891, A160893, A160895, A160897, A160908, A160953, A160957, A160960, A160972, A161010, A161025, A161139, A161167, A161213.",
				"Main diagonal gives A344210.",
				"Cf. A160870."
			],
			"keyword": "nonn,tabl",
			"offset": "1,5",
			"author": "_Álvar Ibeas_, Oct 30 2015",
			"references": 2,
			"revision": 19,
			"time": "2021-05-14T12:18:36-04:00",
			"created": "2015-11-25T21:14:46-05:00"
		}
	]
}