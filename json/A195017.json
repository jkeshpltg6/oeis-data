{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A195017",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 195017,
			"data": "0,1,-1,2,1,0,-1,3,-2,2,1,1,-1,0,0,4,1,-1,-1,3,-2,2,1,2,2,0,-3,1,-1,1,1,5,0,2,0,0,-1,0,-2,4,1,-1,-1,3,-1,2,1,3,-2,3,0,1,-1,-2,2,2,-2,0,1,2,-1,2,-3,6,0,1,1,3,0,1,-1,1,1,0,1,1,0,-1,-1,5,-4,2,1,0,2,0,-2,4,-1,0,-2,3,0,2,0,4,1,-1,-1,4,-1,1,1,2,-1",
			"name": "If n = Product_{k \u003e= 1} (p_k)^(c_k) where p_k is k-th prime and c_k \u003e= 0 then a(n) = Sum_{k \u003e= 1} c_k*((-1)^(k-1)).",
			"comment": [
				"Let p(n,x) be the completely additive polynomial-valued function such that p(1,x) = 0 and p(prime(n),x) = x^(n-1), like is defined in A206284 (although here we are not limited to just irreducible polynomials). Then a(n) is the value of the polynomial encoded in such a manner by n, when it is evaluated at x=-1. - The original definition rewritten and clarified by _Antti Karttunen_, Oct 03 2018",
				"Positions of 0 give the values of n for which the polynomial p(n,x) is divisible by x+1. For related sequences, see the Mathematica section."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A195017/b195017.txt\"\u003eTable of n, a(n) for n = 1..65537\u003c/a\u003e"
			],
			"formula": [
				"Totally additive with a(p^e) = e * (-1)^(1+PrimePi(p)), where PrimePi(n) = A000720(n). - _Antti Karttunen_, Oct 03 2018"
			],
			"example": [
				"The sequence can be read from a list of the polynomials:",
				"  p(n,x)      with x = -1, gives a(n)",
				"------------------------------------------",
				"  p(1,x) = 0           0",
				"  p(2,x) = 1x^0        1",
				"  p(3,x) = x          -1",
				"  p(4,x) = 2x^0        2",
				"  p(5,x) = x^2         1",
				"  p(6,x) = 1+x         0",
				"  p(7,x) = x^3        -1",
				"  p(8,x) = 3x^0        3",
				"  p(9,x) = 2x         -2",
				"  p(10,x) = x^2 + 1    2.",
				"(The list runs through all the polynomials whose coefficients are nonnegative integers.)"
			],
			"mathematica": [
				"b[n_] := Table[x^k, {k, 0, n}];",
				"f[n_] := f[n] = FactorInteger[n]; z = 200;",
				"t[n_, m_, k_] := If[PrimeQ[f[n][[m, 1]]] \u0026\u0026 f[n][[m, 1]]",
				"== Prime[k], f[n][[m, 2]], 0];",
				"u = Table[Apply[Plus,",
				"    Table[Table[t[n, m, k], {k, 1, PrimePi[n]}], {m, 1,",
				"      Length[f[n]]}]], {n, 1, z}];",
				"p[n_, x_] := u[[n]].b[-1 + Length[u[[n]]]]",
				"Table[p[n, x] /. x -\u003e 0, {n, 1, z/2}]   (* A007814 *)",
				"Table[p[2 n, x] /. x -\u003e 0, {n, 1, z/2}] (* A001511 *)",
				"Table[p[n, x] /. x -\u003e 1, {n, 1, z}]     (* A001222 *)",
				"Table[p[n, x] /. x -\u003e 2, {n, 1, z}]     (* A048675 *)",
				"Table[p[n, x] /. x -\u003e 3, {n, 1, z}]     (* A090880 *)",
				"Table[p[n, x] /. x -\u003e -1, {n, 1, z}]    (* A195017 *)"
			],
			"program": [
				"(PARI) A195017(n) = { my(f); if(1==n, 0, f=factor(n); sum(i=1, #f~, f[i,2] * (-1)^(1+primepi(f[i,1])))); } \\\\ _Antti Karttunen_, Oct 03 2018"
			],
			"xref": [
				"Cf. A206284, A277322, A284010.",
				"For other evaluation functions of such encoded polynomials, see A001222, A048675, A056239, A090880, A248663."
			],
			"keyword": "sign",
			"offset": "1,4",
			"author": "_Clark Kimberling_, Feb 06 2012",
			"ext": [
				"More terms, name changed and example-section edited by _Antti Karttunen_, Oct 03 2018"
			],
			"references": 22,
			"revision": 40,
			"time": "2018-10-03T21:35:20-04:00",
			"created": "2012-02-10T12:44:54-05:00"
		}
	]
}