{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000369",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 369,
			"data": "1,3,1,21,9,1,231,111,18,1,3465,1785,345,30,1,65835,35595,7650,825,45,1,1514205,848925,196245,24150,1680,63,1,40883535,23586255,5755050,775845,62790,3066,84,1,1267389585,748471185,190482705,27478710",
			"name": "Triangle of numbers related to triangle A049213; generalization of Stirling numbers of second kind A008277, Bessel triangle A001497.",
			"comment": [
				"a(n,m) := S2p(-3; n,m), a member of a sequence of triangles including S2p(-1; n,m) := A001497(n-1,m-1) (Bessel triangle) and ((-1)^(n-m))*S2p(1; n,m) := A008277(n,m) (Stirling 2nd kind). a(n,1)= A008545(n-1).",
				"a(n,m), n\u003e=m\u003e=1, enumerates unordered n-vertex m-forests composed of m increasing plane (aka ordered) trees, with one vertex of out-degree r=0 (leafs or a root) and each vertex with out-degree r\u003e=1 comes in r+2 types (like for an (r+2)-ary vertex). Proof from the e.g.f. of the first column Y(z):=1-(1-4*x)^(1/4) and the F. Bergeron et al. reference given in A001498, eq. (8), Y'(z)= phi(Y(z)), Y(0)=0, with out-degree o.g.f. phi(w)=1/(1-w)^3. - _Wolfdieter Lang_, Oct 12 2007",
				"Also the Bell transform of the quadruple factorial numbers Product_{k=0..n-1} (4*k+3) (A008545) adding 1,0,0,0,... as column 0. For the definition of the Bell transform see A264428 and for cross-references A265606. - _Peter Luschny_, Dec 31 2015"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A000369/b000369.txt\"\u003eRows n = 1..50, flattened\u003c/a\u003e",
				"P. Blasiak, K. A. Penson and A. I. Solomon, \u003ca href=\"http://www.arXiv.org/abs/quant-ph/0402027\"\u003eThe general boson normal ordering problem\u003c/a\u003e, arXiv:quant-ph/0212072, 2002.",
				"Tom Copeland, \u003ca href=\"http://tcjpn.wordpress.com/2015/08/23/a-class-of-differential-operators-and-the-stirling-numbers/\"\u003eA Class of Differential Operators and the Stirling Numbers\u003c/a\u003e",
				"M. Janjic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL12/Janjic/janjic22.html\"\u003eSome classes of numbers and derivatives\u003c/a\u003e, JIS 12 (2009) 09.8.3",
				"Wolfdieter Lang, \u003ca href=\"/A000369/a000369.txt\"\u003eFirst ten rows.\u003c/a\u003e",
				"Wolfdieter Lang, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL3/LANG/lang.html\"\u003eOn generalizations of Stirling number triangles\u003c/a\u003e, J. Integer Seqs., Vol. 3 (2000), #00.2.4.",
				"Toufik Mansour, Matthias Schork and Mark Shattuck, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL15/Schork/schork2.html\"\u003eThe Generalized Stirling and Bell Numbers Revisited\u003c/a\u003e, Journal of Integer Sequences, Vol. 15 (2012), #12.8.3.",
				"Mathias Pétréolle, Alan D. Sokal, \u003ca href=\"https://arxiv.org/abs/1907.02645\"\u003eLattice paths and branched continued fractions. II. Multivariate Lah polynomials and Lah symmetric functions\u003c/a\u003e, arXiv:1907.02645 [math.CO], 2019.",
				"\u003ca href=\"/index/Be#Bessel\"\u003eIndex entries for sequences related to Bessel functions or polynomials\u003c/a\u003e"
			],
			"formula": [
				"a(n, m) = n!*A049213(n, m)/(m!*4^(n-m)); a(n+1, m) = (4*n-m)*a(n, m) + a(n, m-1), n \u003e= m \u003e= 1; a(n, m) := 0, n\u003cm; a(n, 0) := 0, a(1, 1)=1.",
				"E.g.f. of m-th column: ((1-(1-4*x)^(1/4))^m)/m!.",
				"From _Peter Bala_, Jun 08 2016: (Start)",
				"With offset 0, the e.g.f. is 1/(1 - 4*x)^(3/4)*exp(t*(1 - (1 - 4*x)^(1/4))) = 1 + (3 + t)*x + (21 + 9*t + t^2)*x^2/2! + ....",
				"Thus with row and column numbering starting at 0, this triangle is the exponential Riordan array [d/dx(F(x)), F(x)], belonging to the Derivative subgroup of the exponential Riordan group, where F(x) = 1 - (1 - 4*x)^(1/4).",
				"Row polynomial recurrence: R(n+1,t) = t*Sum_{k = 0..n} binomial(n,k)*A008545(k)*R(n-k,t) with R(0,t) = 1. (End)"
			],
			"example": [
				"{1}; {3,1}; {21,9,1}; {231,111,18,1}; {3465,1785,345,30,1}; ...",
				"Tree combinatorics for a(3,2)=9: there are three m=2 forests each with one tree a root (with out-degree r=0) and the other tree a root and a leaf coming in three versions (like for a 3-ary vertex). Each such forest can be labeled increasingly in three ways (like (1,(23)), (2,(13)) and (3,(12)) yielding 9 such forests. - _Wolfdieter Lang_, Oct 12 2007"
			],
			"mathematica": [
				"a[n_, m_] /; n \u003e= m \u003e= 1 := a[n, m] = (4(n-1) - m)*a[n-1, m] + a[n-1, m-1]; a[n_, m_] /; n \u003c m = 0; a[_, 0] = 0; a[1, 1] = 1; Flatten[Table[a[n, m], {n, 1, 9}, {m, 1, n}]] (* _Jean-François Alcover_, Jul 22 2011 *)"
			],
			"program": [
				"(Sage) # uses[bell_transform from A264428]",
				"# Adds a column 1,0,0,0,... at the left side of the triangle.",
				"def A000369_row(n):",
				"    multifact_4_3 = lambda n: prod(4*k + 3 for k in (0..n-1))",
				"    mfact = [multifact_4_3(k) for k in (0..n)]",
				"    return bell_transform(n, mfact)",
				"[A000369_row(n) for n in (0..9)] # _Peter Luschny_, Dec 31 2015"
			],
			"xref": [
				"Row sums give A016036. Cf. A004747.",
				"Columns include A008545.",
				"Alternating row sums A132163."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "1,2",
			"author": "_Wolfdieter Lang_",
			"references": 16,
			"revision": 67,
			"time": "2020-03-23T12:11:31-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}