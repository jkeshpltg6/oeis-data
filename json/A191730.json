{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A191730",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 191730,
			"data": "1,2,3,4,7,5,9,17,12,6,22,42,29,14,8,54,104,72,34,19,10,134,259,179,84,47,24,11,334,647,447,209,117,59,27,13,834,1617,1117,522,292,147,67,32,15,2084,4042,2792,1304,729,367,167,79,37,16,5209,10104,6979",
			"name": "Dispersion of A047211, (numbers \u003e1 and congruent to 2 or 4 mod 5), by antidiagonals.",
			"comment": [
				"For a background discussion of dispersions and their fractal sequences, see A191426.  For dispersions of congruence sequences mod 3, mod 4, or mod 5, see A191655, A191663, A191667, A191702.",
				"...",
				"Suppose that {2,3,4,5,6} is partitioned as {x1, x2} and {x3,x4,x5}.  Let S be the increasing sequence of numbers \u003e1 and congruent to x1 or x2 mod 5, and let T be the increasing sequence of numbers \u003e1 and congruent to x3 or x4 or x5 mod 5.  There are 10 sequences in S, each matched by a (nearly) complementary sequence in T.  Each of the 20 sequences generates a dispersion, as listed here:",
				"...",
				"A191722=dispersion of A008851 (0, 1 mod 5 and \u003e1)",
				"A191723=dispersion of A047215 (0, 2 mod 5 and \u003e1)",
				"A191724=dispersion of A047218 (0, 3 mod 5 and \u003e1)",
				"A191725=dispersion of A047208 (0, 4 mod 5 and \u003e1)",
				"A191726=dispersion of A047216 (1, 2 mod 5 and \u003e1)",
				"A191727=dispersion of A047219 (1, 3 mod 5 and \u003e1)",
				"A191728=dispersion of A047209 (1, 4 mod 5 and \u003e1)",
				"A191729=dispersion of A047221 (2, 3 mod 5 and \u003e1)",
				"A191730=dispersion of A047211 (2, 4 mod 5 and \u003e1)",
				"A191731=dispersion of A047204 (3, 4 mod 5 and \u003e1)",
				"...",
				"A191732=dispersion of A047202 (2,3,4 mod 5 and \u003e1)",
				"A191733=dispersion of A047206 (1,3,4 mod 5 and \u003e1)",
				"A191734=dispersion of A032793 (1,2,4 mod 5 and \u003e1)",
				"A191735=dispersion of A047223 (1,2,3 mod 5 and \u003e1)",
				"A191736=dispersion of A047205 (0,3,4 mod 5 and \u003e1)",
				"A191737=dispersion of A047212 (0,2,4 mod 5 and \u003e1)",
				"A191738=dispersion of A047222 (0,2,3 mod 5 and \u003e1)",
				"A191739=dispersion of A008854 (0,1,4 mod 5 and \u003e1)",
				"A191740=dispersion of A047220 (0,1,3 mod 5 and \u003e1)",
				"A191741=dispersion of A047217 (0,1,2 mod 5 and \u003e1)",
				"...",
				"For further information about these 20 dispersions, see A191722.",
				"...",
				"Regarding the dispersions A191722-A191741, there are general formulas for sequences of the type \"(a or b mod m)\" and \"(a or b or c mod m)\" used in the relevant Mathematica programs."
			],
			"link": [
				"Ivan Neretin, \u003ca href=\"/A191730/b191730.txt\"\u003eTable of n, a(n) for n = 1..5050\u003c/a\u003e (first 100 antidiagonals, flattened)"
			],
			"example": [
				"Northwest corner:",
				"1....2....4.....9.....22",
				"3....7....17...42....104",
				"5....12...29...72....179",
				"6....14...34...84....209",
				"8....19...47...117...292",
				"10...24...59...147...367"
			],
			"mathematica": [
				"(* Program generates the dispersion array t of the increasing sequence f[n] *)",
				"r = 40; r1 = 12;  c = 40; c1 = 12;",
				"a=2; b=4; m[n_]:=If[Mod[n,2]==0,1,0];",
				"f[n_]:=a*m[n+1]+b*m[n]+5*Floor[(n-1)/2]",
				"Table[f[n], {n, 1, 30}]  (* A047211 *)",
				"mex[list_] := NestWhile[#1 + 1 \u0026, 1, Union[list][[#1]] \u003c= #1 \u0026, 1, Length[Union[list]]]",
				"rows = {NestList[f, 1, c]};",
				"Do[rows = Append[rows, NestList[f, mex[Flatten[rows]], r]], {r}];",
				"t[i_, j_] := rows[[i, j]];",
				"TableForm[Table[t[i, j], {i, 1, 10}, {j, 1, 10}]] (* A191730 *)",
				"Flatten[Table[t[k, n - k + 1], {n, 1, c1}, {k, 1, n}]] (* A191730  *)"
			],
			"xref": [
				"Cf. A047220, A047211, A191740, A191722, A191426."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Jun 13 2011",
			"references": 20,
			"revision": 12,
			"time": "2017-10-12T13:06:26-04:00",
			"created": "2011-06-15T17:56:32-04:00"
		}
	]
}