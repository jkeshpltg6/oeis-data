{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001767",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1767,
			"id": "M2459 N0976",
			"data": "0,0,0,0,1,3,5,10,13,26,25,50,49,73,81,133,109,196,169,241,241,375,289,476,421,568,529,806,577,1001,833,1081,1009,1393,1081,1768,1441,1849,1633,2451,1729,2850,2281,2809,2641,3773,2689,4215,3301,4321,3865,5500",
			"name": "Genus of modular group Gamma(n) = genus of modular curve Chi(n).",
			"comment": [
				"In Klein and Fricke, the level n is called Stufenzahlen, the congruence group is denoted by Gamma_{n} and the genus is called Geschlecht and denoted by p. - _Michael Somos_, Nov 08 2014"
			],
			"reference": [
				"R. C. Gunning, Lectures on Modular Forms. Princeton Univ. Press, Princeton, NJ, 1962, p. 15.",
				"B. Iversen, Hyperbolic Geometry, Cambridge Univ. Press, 1992, see p. 238.",
				"F. Klein and R. Fricke, Vorlesungen ueber die theorie der elliptischen modulfunctionen, Teubner, Leipzig, 1890, Vol. 1, see p. 398.",
				"Russian Encyclopedia of Mathematics, Vol. 3, page 931.",
				"B. Schoeneberg, Elliptic Modular Functions, Springer-Verlag, NY, 1974, p. 94.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A001767/b001767.txt\"\u003eTable of n, a(n) for n = 2..1000\u003c/a\u003e",
				"Ioannis Ivrissimtzis, David Singerman, James Strudwick, \u003ca href=\"https://arxiv.org/abs/1909.08568\"\u003eFrom Farey fractions to the Klein quartic and beyond\u003c/a\u003e, arXiv:1909.08568 [math.GR], 2019. See g(n) p. 3.",
				"\u003ca href=\"/index/Gre#groups_modular\"\u003eIndex entries for sequences related to modular groups\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 1 + (n-6)*A000010(n)*A001615(n)/24, for n \u003e 2. - _Gheorghe Coserea_, Oct 23 2016"
			],
			"example": [
				"G.f. = x^6 + 3*x^7 + 5*x^8 + 10*x^9 + 13*x^10 + 26*x^11 + 25*x^12 + ..."
			],
			"mathematica": [
				"Join[{0}, Table[1 + n^2 (n - 6)/24 Product[If[Mod[n, Prime[p]] == 0, 1 - 1/Prime[p]^2, 1], {p, PrimePi[n]}], {n, 3, 100}]] (* _T. D. Noe_, Aug 10 2012 *)",
				"a[ n_] := If[ n \u003c 3, 0, 1 + n^2 (n - 6)/24 Product[ If[ PrimeQ[p] \u0026\u0026 Divisible[n, p], 1 - 1/p^2, 1], {p, 2, n}]]; (* _Michael Somos_, Nov 08 2014 *)"
			],
			"program": [
				"(PARI) {a(n) = if(n\u003c3, 0, 1 + n^2 * (n-6) / 24 * prod(p=2, n, if( isprime(p) \u0026\u0026 (n%p==0), 1 - 1/p^2, 1)))}; /* _Michael Somos_, May 19 2004 */",
				"(PARI)",
				"a(n) = {",
				"  if (n \u003c 6, return(0));",
				"  my(f = factor(n), fsz = matsize(f)[1],",
				"     g = prod(k=1, fsz, f[k,1]),",
				"     h = prod(k=1, fsz, sqr(f[k,1]) - 1));",
				"  return(1 + (n-6)*sqr(n\\g)*h\\24);",
				"};",
				"vector(52, n, a(n+1))  \\\\ _Gheorghe Coserea_, Oct 23 2016"
			],
			"keyword": "nonn,easy",
			"offset": "2,6",
			"author": "_N. J. A. Sloane_",
			"references": 1,
			"revision": 38,
			"time": "2019-10-23T11:09:57-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}