{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A221367",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 221367,
			"data": "1,7,1,77,1,700,1,6237,1,55447,1,492800,1,4379767,1,38925117,1,345946300,1,3074591597,1,27325378087,1,242853811200,1,2158358922727,1,19182376493357,1,170483029517500,1,1515164889164157,1,13466000972959927,1",
			"name": "The simple continued fraction expansion of F(x) := product {n = 0..inf} (1 - x^(4*n+3))/(1 - x^(4*n+1)) when x = 1/2*(9 - sqrt(77)).",
			"comment": [
				"The function F(x) := product {n = 0..inf} (1 - x^(4*n+3))/(1 - x^(4*n+1)) is analytic for |x| \u003c 1. When x is a quadratic irrational of the form x = 1/2*(N - sqrt(N^2 - 4)), N an integer greater than 2, the real number F(x) has a predictable simple continued fraction expansion. The first examples of these expansions, for N = 2, 4, 6 and 8, are due to Hanna. See A174500 through A175503. The present sequence is the case N = 9. See also A221364 (N = 3), A221365 (N = 5) and A221366 (N = 7).",
				"If we denote the present sequence by [1, c(1), 1, c(2), 1, c(3), ...] then for k = 1, 2, ..., the simple continued fraction expansion of F({1/2*(9 - sqrt(77)}^k) is given by the sequence [1; c(k), 1, c(2*k), 1, c(3*k), 1, ...]. Examples are given below."
			],
			"link": [
				"P. Bala, \u003ca href=\"/A174500/a174500_2.pdf\"\u003eSome simple continued fraction expansions for an infinite product, Part 1\u003c/a\u003e",
				"\u003ca href=\"/index/Rec\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,10,0,-10,0,1)."
			],
			"formula": [
				"a(2*n-1) = (1/2*(9 + sqrt(77)))^n + (1/2*(9 - sqrt(77)))^n - 2; a(2*n) = 1.",
				"a(4*n-1) = 77*(A018913(n))^2; a(4*n+1) = 7*(A057081(n))^2.",
				"a(n) = 10*a(n-2)-10*a(n-4)+a(n-6). G.f.: -(x^4+7*x^3-9*x^2+7*x+1) / ((x-1)*(x+1)*(x^4-9*x^2+1)). [_Colin Barker_, Jan 20 2013]"
			],
			"example": [
				"F(1/2*(9 - sqrt(77)) = 1.12519 81018 34502 81936 ... = 1 + 1/(7 + 1/(1 + 1/(77 + 1/(1 + 1/(700 + 1/(1 + 1/(6237 + ...))))))).",
				"F({1/2*(9 - sqrt(77)}^2) = 1.01282 05391 65421 74656 ... = 1 + 1/(77 + 1/(1 + 1/(6237 + 1/(1 + 1/(492800 + 1/(1 + 1/(38925117 + ...))))))).",
				"F({1/2*(9 - sqrt(77)}^3) = 1.00142 65335 27667 24640 ... = 1 + 1/(700 + 1/(1 + 1/(492800 + 1/(1 + 1/(345946300 + 1/(1 + 1/(242853811200 + ...)))))))."
			],
			"xref": [
				"Cf. A018193, A057081, A174500 (N = 4), A221364 (N = 3), A221365 (N = 5), A221366 (N = 7)."
			],
			"keyword": "nonn,easy,cofr",
			"offset": "0,2",
			"author": "_Peter Bala_, Jan 15 2013",
			"references": 2,
			"revision": 12,
			"time": "2015-06-13T00:54:37-04:00",
			"created": "2013-01-16T09:38:55-05:00"
		}
	]
}