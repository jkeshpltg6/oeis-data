{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A117895",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 117895,
			"data": "1,1,2,1,3,3,1,4,4,8,1,5,5,11,19,1,6,6,14,26,46,1,7,7,17,33,63,111,1,8,8,20,40,80,152,268,1,9,9,23,47,97,193,367,647,1,10,10,26,54,114,234,466,886,1562,1,11,11,29,61,131,275,565,1125,2139,3771,1,12,12,32,68,148,316,664,1364,2716,5164,9104",
			"name": "Triangle T(n, k) = (k-n)*A000129(k+1) + (3*n-3*k+1)*A000129(k) with T(n,0) = 1, for 0 \u003c= k \u003c= n-1, read by rows.",
			"comment": [
				"Successive deletions of the right borders of triangle A117894 produces triangles whose row sums = generalized Pell sequences starting (1, 2...), (1, 3...), (1, 4...); etc. Row sums of A117894 = A000129: (1, 2, 5...). Row sums of A117895 = A001333: (1, 3, 7...). Deletion of the border of A117895 would produce a triangle with row sums of the Pell sequence A048654 (1, 4, 9...); and so on."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A117895/b117895.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"Delete right border of triangle A117894. Alternatively, let row 1 = 1 and using the heading 0, 1, 1, 3, 7, 17, 41, 99, 239...(i.e. A001333 starting with 0, 1, 1, 3...); add the first n terms of the heading to n-th row of triangle A117894.",
				"From _G. C. Greubel_, Sep 27 2021: (Start)",
				"T(n, k) = (k-n)*A000129(k+1) + (3*n-3*k+1)*A000129(k) with T(n,0) = 1.",
				"T(n, 1) = n+1 for n \u003e= 1.",
				"T(n, 2) = n+1 for n \u003e= 2.",
				"T(n, n) = 2*[n=0] + A078343(n). (End)"
			],
			"example": [
				"First few rows of the triangle are:",
				"  1;",
				"  1, 2;",
				"  1, 3, 3;",
				"  1, 4, 4,  8;",
				"  1, 5, 5, 11, 19;",
				"  1, 6, 6, 14, 26, 46;",
				"  1, 7, 7, 17, 33, 63, 111;",
				"  1, 8, 8, 20, 40, 80, 152, 268;",
				"...",
				"Row 4, (1, 4, 4, 8) is produced by adding (0, 1, 1, 3) to row 4 of A117894: (1, 3, 3, 5)."
			],
			"mathematica": [
				"T[n_, k_]:= T[n, k]= If[k==0, 1, (k-n)*Fibonacci[k+1, 2] + (3*n-3*k +1)*Fibonacci[k, 2]]; Table[T[n, k], {n,0,12}, {k,0,n-1}]//Flatten (* _G. C. Greubel_, Sep 27 2021 *)"
			],
			"program": [
				"(Magma) Pell:= func\u003c n | Round(((1+Sqrt(2))^n - (1-Sqrt(2))^n)/(2*Sqrt(2))) \u003e;",
				"[k eq 0 select 1 else (k-n)*Pell(k+1) + (3*n-3*k+1)*Pell(k): k in [0..n-1], n in [0..12]]; // _G. C. Greubel_, Sep 27 2021",
				"(Sage)",
				"def P(n): return lucas_number1(n, 2, -1)",
				"def A117895(n,k): return 1 if (k==0) else (k-n)*P(k+1) + (3*n-3*k+1)*P(k)",
				"flatten([[A117895(n,k) for k in (0..n-1)] for n in (0..12)]) # _G. C. Greubel_, Sep 27 2021"
			],
			"xref": [
				"Cf. A000129, A001333, A048654, A048655, A048693, A117894."
			],
			"keyword": "nonn,tabl",
			"offset": "0,3",
			"author": "_Gary W. Adamson_, Mar 30 2006",
			"ext": [
				"New name and more terms added by _G. C. Greubel_, Sep 27 2021"
			],
			"references": 2,
			"revision": 12,
			"time": "2021-09-30T18:15:40-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}