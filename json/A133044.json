{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A133044",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 133044,
			"data": "1,2,3,7,11,20,36,61,110,191,335,591,1032,1816,3185,5586,9811,17207,30203,53004,93004,163229,286430,502655,882111,1547967,2716528,4767152,8365761,14680930,25763171,45211271,79340235,139232356,244335860,428779421,752455502,1320467391",
			"name": "Area of the spiral of equilateral triangles with side lengths which follow the Padovan sequence, divided by the area of the initial triangle.",
			"comment": [
				"First differs from A014529 at a(8)."
			],
			"reference": [
				"Mohammad K. Azarian, A Trigonometric Characterization of Equilateral Triangle, Problem 336, Mathematics and Computer Education, Vol. 31, No. 1, Winter 1997, p. 96. Solution published in Vol. 32, No. 1, Winter 1998, pp. 84-85.",
				"Mohammad K. Azarian, Equating Distances and Altitude in an Equilateral Triangle, Problem 316, Mathematics and Computer Education, Vol. 28, No. 3, Fall 1994, p. 337. Solution published in Vol. 29, No. 3, Fall 1995, pp. 324-325."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A133044/b133044.txt\"\u003eTable of n, a(n) for n = 1..2000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,1,1,-1,1,-1)."
			],
			"formula": [
				"From _Colin Barker_, Sep 18 2013: (Start)",
				"Conjecture: a(n) = a(n-1) + a(n-2) + a(n-3) - a(n-4) + a(n-5) - a(n-6).",
				"G.f.: x*(x^3+x+1) / ((x^3-x^2+2*x-1)*(x^3-x-1)).",
				"(End)",
				"From _Félix Breton_, Dec 17 2015: (Start)",
				"a(n) = 2*p(n+4)*p(n+5) - p(n+2)^2 where p is the Padovan sequence (A000931). This establishes Colin Barker's conjecture, because",
				"a(n) = a(n-1) + p(n+4)^2",
				"= a(n-1) + (p(n+1) + p(n+2))^2",
				"= a(n-1) + p(n+1)^2 + p(n+2)^2 + 2*p(n+1)*p(n+2) - p(n-1)^2 + p(n-1)^2",
				"= a(n-1) + (a(n-3)-a(n-4)) + (a(n-2)-a(n-3)) + a(n-3) + (a(n-5)-a(n-6))",
				"= a(n-1) + a(n-2) + a(n-3) - a(n-4) + a(n-5) - a(n-6). (End)"
			],
			"mathematica": [
				"RecurrenceTable[{a[n + 6] == a[n + 5] + a[n + 4] + a[n + 3] - a[n + 2] + a[n + 1] - a[n], a[1] == 1, a[2] == 2, a[3] == 3, a[4] == 7, a[5] == 11, a[6] == 20}, a, {n, 1, 2000}] (* _G. C. Greubel_, Dec 17 2015 *)",
				"Rest@ CoefficientList[Series[x (x^3 + x + 1)/((x^3 - x^2 + 2 x - 1) (x^3 - x - 1)), {x, 0, 38}], x] (* _Michael De Vlieger_, Feb 21 2018 *)"
			],
			"program": [
				"(PARI) Vec((x^3+x+1)/((x^3-x^2+2*x-1)*(x^3-x-1)) + O(x^40)) \\\\ _Andrew Howroyd_, Feb 21 2018"
			],
			"xref": [
				"Cf. A000931, A014529, A133043."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Omar E. Pol_, Nov 04 2007",
			"ext": [
				"a(27) and beyond taken from _G. C. Greubel_'s table. - _Omar E. Pol_, Dec 18 2015",
				"a(589) in b-file corrected by _Andrew Howroyd_, Feb 21 2018"
			],
			"references": 2,
			"revision": 48,
			"time": "2018-02-22T05:53:42-05:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}