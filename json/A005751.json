{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005751",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5751,
			"id": "M1478",
			"data": "1,1,2,5,15,49,180,701,2891,12371,54564,246319,1133602,5300255,25119554,120441076,583373822,2851023191,14044428996,69677569603,347904448580,1747195558582,8820848574074,44747514381341,228004950808983,1166498678253839,5990376960443432",
			"name": "Number of matched trees with 2n nodes.",
			"comment": [
				"This sequence also describes the number of trees on 2n vertices that are in P-position (a player 2 win) in unrooted UVG (Undirected Vertex Geography).  This connection is discussed by Fraenkel, Scheinerman, and Ullman in their paper \"Undirected Edge Geography.\" - _Kaitlin Bruegge_, Jul 14 2017"
			],
			"reference": [
				"S. R. Finch, Mathematical Constants, Cambridge, 2003, p. 307 and 564.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A005751/b005751.txt\"\u003eTable of n, a(n) for n = 1..400\u003c/a\u003e",
				"Aviezri S. Fraenkel, Edward R. Scheinerman, and Daniel Ullman, \u003ca href=\"https://doi.org/10.1016/0304-3975(93)90026-P\"\u003eUndirected Edge Geography\u003c/a\u003e, Theoretical Computer Science, 112, (1993), 371-381.",
				"Indranil Ghosh, \u003ca href=\"/A005751/a005751.txt\"\u003ePython program for computing this sequence\u003c/a\u003e (translated from Maple code)",
				"Rodica Simion, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(91)90061-6\"\u003eTrees with 1-factors and oriented trees\u003c/a\u003e, Discrete Math., 88 (1991), 93-104.",
				"Rodica Simion, \u003ca href=\"/A005750/a005750.pdf\"\u003eTrees with 1-factors and oriented trees\u003c/a\u003e, Discrete Math., 88 (1981), 97. (Annotated scanned copy)",
				"\u003ca href=\"/index/Tra#trees\"\u003eIndex entries for sequences related to trees\u003c/a\u003e"
			],
			"formula": [
				"a(n) ~ c * d^n / n^(5/2), where d = A245870 = 5.646542616232949712892713..., c = 0.1128580768964135711615258... . - _Vaclav Kotesovec_, Aug 25 2014"
			],
			"example": [
				"a(3)=2; indeed we have the path P_6 and the tree obtained by identifying one endpoint of each of P_2, P_3, and P_3. - _Emeric Deutsch_, Apr 13 2014"
			],
			"maple": [
				"with(numtheory): r2:= proc(n) option remember; local m; `if`(n=1, 1, 2/(n-1) *add(r2(m) *add(d*r2(d), d=divisors(n-m)), m=1..n-1)) end: p2:= proc(n) option remember; local m; `if`(n=1, 1, 1/(n-1) *add(p2(m) *add(d*r2(d), d=divisors(n-m)), m=1..n-1)) end: m2:= n-\u003e (r2(n) -add(r2(m) *r2(n-m), m=1..n-1) +`if`(irem(n, 2)=0, r2(n/2), p2((n+1)/2)))/2: seq(m2(n), n=1..30); # _Alois P. Heinz_, Aug 04 2009"
			],
			"mathematica": [
				"r2[n_] := r2[n] = If[n == 1, 1, 2/(n-1)*Sum[r2[m]*Sum[d*r2[d], {d, Divisors[n-m]}], {m, 1, n-1}]]; p2[n_] := p2[n] = If[n == 1, 1, 1/(n-1)*Sum[p2[m]*Sum[d*r2[d], {d, Divisors[n-m]}], {m, 1, n-1}]]; m2[n_] := (r2[n] - Sum[r2[m]*r2[n-m], {m, 1, n-1}] + If[Mod[n, 2] == 0, r2[n/2], p2[(n+1)/2]])/2; Table[m2[n], {n, 1, 30}] (* _Jean-François Alcover_, Mar 17 2014, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A000151 for the rooted version.",
				"Cf. A245870."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Alois P. Heinz_, Aug 04 2009"
			],
			"references": 3,
			"revision": 69,
			"time": "2020-01-25T01:44:04-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}