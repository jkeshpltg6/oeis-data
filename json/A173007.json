{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A173007",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 173007,
			"data": "1,3,1,27,12,1,729,351,39,1,59049,29160,3510,120,1,14348907,7144929,882090,32670,363,1,10460353203,5223002148,650188539,24698520,297297,1092,1,22876792454961,11433166050879,1427185336941,54665851779,674887059,2685501,3279,1",
			"name": "Triangle T(n,k) read by rows: coefficient [x^k] of the polynomial Product_{i=1..n} (x + q^i) in row n and q = 3.",
			"comment": [
				"Triangle T(n,k), read by rows, given by [3,6,27,72,243,702,2187,6480,...] DELTA [1,0,3,0,9,0,27,0,81,0,243,0,...] where DELTA is the operator defined in A084938. - _Philippe Deléham_, Oct 01 2011"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A173007/b173007.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"p(x,n,q) = 1 if n=0, Product_{i=1..n} (x + q^i) otherwise, with q=3.",
				"T(n,k) = 3^n*T(n-1,k) + T(n-1,k-1), T(0,0)=1. - _Philippe Deléham_, Oct 01 2011",
				"Sum_{k=0..n} T(n, k, 4) = A290000(n+1). - _G. C. Greubel_, Feb 20 2021"
			],
			"example": [
				"Triangle begins as:",
				"            1;",
				"            3,          1;",
				"           27,         12,         1;",
				"          729,        351,        39,        1;",
				"        59049,      29160,      3510,      120,      1;",
				"     14348907,    7144929,    882090,    32670,    363,    1;",
				"  10460353203, 5223002148, 650188539, 24698520, 297297, 1092, 1;"
			],
			"mathematica": [
				"(* First program *)",
				"p[x_, n_, q_] = If[n==0, 1, Product[x + q^i, {i, 1, n}]];",
				"Table[CoefficientList[p[x, n, 3], x], {n, 0, 10}] (* modified by _G. C. Greubel_, Feb 20 2021 *)",
				"(* Second program *)",
				"T[n_, k_, q_]:= If[k\u003c0 || k\u003en, 0, If[k==n, 1, q^n*T[n-1,k,q] +T[n-1,k-1,q] ]];",
				"Table[T[n,k,3], {n,0,10}, {k,0,n}]//Flatten (* _G. C. Greubel_, Feb 20 2021 *)"
			],
			"program": [
				"(Sage)",
				"def T(n, k, q):",
				"    if (k\u003c0 or k\u003en): return 0",
				"    elif (k==n): return 1",
				"    else: return q^n*T(n-1,k,q) + T(n-1,k-1,q)",
				"flatten([[T(n,k,3) for k in (0..n)] for n in (0..10)]) # _G. C. Greubel_, Feb 20 2021",
				"(Magma)",
				"function T(n,k,q)",
				"  if k lt 0 or k gt n then return 0;",
				"  elif k eq n then return 1;",
				"  else return q^n*T(n-1,k,q) + T(n-1,k-1,q);",
				"  end if; return T; end function;",
				"[T(n,k,3): k in [0..n], n in [0..10]]; // _G. C. Greubel_, Feb 20 2021"
			],
			"xref": [
				"Cf. A023531 (q=0), A007318 (q=1), A108084 (q=2), this sequence (q=3), A173008 (q=4).",
				"Cf. A203148, A290000."
			],
			"keyword": "nonn,tabl",
			"offset": "0,2",
			"author": "_Roger L. Bagula_, Feb 07 2010",
			"ext": [
				"Edited by _G. C. Greubel_, Feb 20 2021"
			],
			"references": 5,
			"revision": 18,
			"time": "2021-02-20T23:12:16-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}