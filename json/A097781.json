{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097781",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97781,
			"data": "1,27,728,19629,529255,14270256,384767657,10374456483,279725557384,7542215592885,203360095450511,5483180361570912,147842509666964113,3986264580646460139,107481301167787459640,2898008866949614950141",
			"name": "Chebyshev polynomials S(n,27) with Diophantine property.",
			"comment": [
				"All positive integer solutions of Pell equation b(n)^2 - 725*a(n)^2 = +4 together with b(n)=A090248(n+1), n\u003e=0. Note that D=725=29*5^2 is not squarefree.",
				"For positive n, a(n) equals the permanent of the n X n tridiagonal matrix with 27's along the main diagonal, and i's along the superdiagonal and the subdiagonal (i is the imaginary unit). - _John M. Campbell_, Jul 08 2011",
				"For n\u003e=1, a(n) equals the number of 01-avoiding words of length n-1 on alphabet {0,1,...,26}. - _Milan Janjic_, Jan 26 2015"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A097781/b097781.txt\"\u003eTable of n, a(n) for n = 0..700\u003c/a\u003e",
				"R. Flórez, R. A. Higuita, A. Mukherjee, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Mukherjee/mukh2.html\"\u003eAlternating Sums in the Hosoya Polynomial Triangle\u003c/a\u003e, Article 14.9.5 Journal of Integer Sequences, Vol. 17 (2014).",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (27,-1)."
			],
			"formula": [
				"a(n) = S(n, 27) = U(n, 27/2) = S(2*n+1, sqrt(29))/sqrt(29) with S(n, x)=U(n, x/2) Chebyshev's polynomials of the 2nd kind, A049310. S(-1, x)= 0 = U(-1, x).",
				"a(n) = 27*a(n-1)-a(n-2), n \u003e= 1; a(0)=1, a(1)=27; a(-1)=0.",
				"a(n) = (ap^(n+1) - am^(n+1))/(ap-am) with ap = (27+5*sqrt(29))/2 and am = (27-5*sqrt(29))/2.",
				"G.f.: 1/(1-27*x+x^2).",
				"a(n) = Sum_{k, 0\u003c=k\u003c=n} A101950(n,k)*26^k. - _Philippe Deléham_, Feb 10 2012",
				"Product {n \u003e= 0} (1 + 1/a(n)) = 1/5*(5 + sqrt(29)). - _Peter Bala_, Dec 23 2012",
				"Product {n \u003e= 1} (1 - 1/a(n)) = 5/54*(5 + sqrt(29)). - _Peter Bala_, Dec 23 2012"
			],
			"example": [
				"(x,y) = (27;1), (727;27), (19602;728), ... give the positive integer solutions to x^2 - 29*(5*y)^2 =+4."
			],
			"maple": [
				"with (combinat):seq(fibonacci(2*n, 5)/5, n=1..16); # _Zerinvary Lajos_, Apr 20 2008"
			],
			"mathematica": [
				"Join[{a=1,b=27},Table[c=27*b-a;a=b;b=c,{n,60}]] (* _Vladimir Joseph Stephan Orlovsky_, Jan 21 2011 *)",
				"CoefficientList[Series[1/(1 - 27 x + x^2), {x, 0, 40}], x] (* _Vincenzo Librandi_, Dec 24 2012 *)"
			],
			"program": [
				"(Sage) [lucas_number1(n,27,1) for n in range(1,20)] # _Zerinvary Lajos_, Jun 25 2008",
				"(MAGMA) I:=[1, 27, 728]; [n le 3 select I[n] else 27*Self(n-1)-Self(n-2): n in [1..20]]; // _Vincenzo Librandi_, Dec 24 2012"
			],
			"xref": [
				"Cf. A078362, A078366."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Aug 31 2004",
			"references": 7,
			"revision": 47,
			"time": "2019-12-07T12:18:24-05:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}