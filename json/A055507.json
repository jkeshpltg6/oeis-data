{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A055507",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 55507,
			"data": "1,4,8,14,20,28,37,44,58,64,80,86,108,108,136,134,169,160,198,192,236,216,276,246,310,288,348,310,400,344,433,396,474,408,544,450,564,512,614,522,688,560,716,638,756,636,860,676,859,772,926,758,1016,804,1032",
			"name": "a(n) = Sum_{k=1..n} d(k)*d(n+1-k), where d(k) is number of positive divisors of k.",
			"comment": [
				"a(n) is the number of ordered ways to express n+1 as a*b+c*d with 1 \u003c= a,b,c,d \u003c= n. - _David W. Wilson_, Jun 16 2003",
				"tau(n) (A000005) convolved with itself, treating this result as a sequence whose offset is 2. - _Graeme McRae_, Jun 06 2006",
				"Convolution of A341062 and nonzero terms of A006218. - _Omar E. Pol_, Feb 16 2021"
			],
			"link": [
				"Charles R Greathouse IV, \u003ca href=\"/A055507/b055507.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"George E. Andrews, \u003ca href=\"http://dx.doi.org/10.1007/BF01608779\"\u003eStacked lattice boxes\u003c/a\u003e, Ann. Comb. 3 (1999), 115-130. See D_{0,0}.",
				"Yoichi Motohashi,\u003ca href=\"http://www.numdam.org/item?id=ASENS_1994_4_27_5_529_0\"\u003e The binary additive divisor problem\u003c/a\u003e, Annales scientifiques de l'École Normale Supérieure, Sér. 4, 27 no. 5 (1994), p. 529-572.",
				"E. C. Titchmarsh, \u003ca href=\"https://doi.org/10.1093/qmath/os-13.1.129\"\u003eSome problems in the analytic theory of numbers\u003c/a\u003e, The Quarterly Journal of Mathematics 1 (1942): 129-152."
			],
			"formula": [
				"G.f.: Sum_{i \u003e= 1, j \u003e= 1} x^(i+j-1)/(1-x^i)/(1-x^j). - _Vladeta Jovovic_, Nov 11 2001",
				"Working with an offset of 2, it appears that the o.g.f is equal to the Lambert series sum {n \u003e= 2} A072031(n-1)*x^n/(1 - x^n). - _Peter Bala_, Dec 09 2014",
				"a(n) = A212151(n+2) - A212151(n+1). - _Ridouane Oudra_, Sep 12 2020"
			],
			"example": [
				"a(4) = d(1)*d(4) + d(2)*d(3) + d(3)*d(2) + d(4)*d(1) = 1*3 +2*2 +2*2 +3*1 = 14.",
				"3 = 1*1+2*1 in 4 ways, so a(2)=4; 4 = 1*1+1*3 (4 ways) = 2*1+2*1 (4 ways), so a(3)=8; 5 = 4*1+1*1 (4 ways) = 2*2+1*1 (2 ways) + 3*1+2*1 (8 ways), so a(4) = 14. - _N. J. A. Sloane_, Jul 07 2012"
			],
			"maple": [
				"with(numtheory); D00:=n-\u003eadd(tau(j)*tau(n+1-j),j=1..n);"
			],
			"program": [
				"(PARI) a(n)=sum(k=1,n,numdiv(k)*numdiv(n+1-k)) \\\\ _Charles R Greathouse IV_, Oct 17 2012"
			],
			"xref": [
				"Cf. A000005, A000385, A072031, A212151.",
				"Cf. A006218, A341062."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Leroy Quet_, Jun 29 2000",
			"ext": [
				"More terms from _James A. Sellers_, Jul 04 2000",
				"Definition clarified by _N. J. A. Sloane_, Jul 07 2012"
			],
			"references": 21,
			"revision": 59,
			"time": "2021-02-16T17:55:35-05:00",
			"created": "2000-07-22T03:00:00-04:00"
		}
	]
}