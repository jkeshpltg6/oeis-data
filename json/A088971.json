{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A088971",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 88971,
			"data": "3,5,8,12,9,16,12,15,33,16,32,19,12,23,27,31,7,54,24,14,32,30,33,54,38,20,17,14,18,104,25,30,26,57,17,52,41,25,50,40,20,69,21,30,16,85,135,18,18,22,28,28,65,26,63,64,17,45,29,15,93,115,41,13,21,129,56,80,17,25,31,59,70,70,37,33,41,42,58,92",
			"name": "Number of twin prime pairs between consecutive prime-indexed primes of order 3. The bounds are included in the calculation.",
			"comment": [
				"This sequence contains no 0's between x=1 to 33000. The interval [PIPS3(4133), PIPS3(4134)] contains 1 twin prime pair; the interval [PIPS3(8268), PIPS3(8269)] contains 2 twin prime pairs."
			],
			"formula": [
				"PIPS3(x) = A049090(x) = the x-th prime-indexed prime of order 3 = prime(prime(prime(prime(x)))) where prime(x) is the x-th prime. a(n) = count of twins in [PIPS3(n), PIPS3(n+1)]."
			],
			"example": [
				"a(1) = 3, since there are three pairs of twin primes at least PIPS3(1) = 11 and at most PIPS3(2) = 31: (11,13), (17,19), and (29,31)."
			],
			"program": [
				"(PARI) piptwins3(m,n) = { for(x=m,n, f=1; c=0; p1 = prime(prime(prime(prime(x)))); p2 = prime(prime(prime(prime(x+1)))); forprime(j=p1,p2-2, if(isprime(j+2),f=0; c++) ); print1(c\",\"); ) }",
				"(Sage)",
				"def PIP(n, i): # Returns the n-th prime-indexed prime of order i.",
				"    if i==0:",
				"        return primes_first_n(n)[n-1]",
				"    else:",
				"        return PIP(PIP(n, i-1), 0)",
				"def A088971(n): # Returns a(n)",
				"    return len([i for i in range(PIP(n, 3), PIP(n+1, 3), 2) if (is_prime(i) and is_prime(i+2))])",
				"A088971(1) # _Danny Rorabaugh_, Mar 30 2015"
			],
			"xref": [
				"Cf. A006450, A049090, A077800, A088973."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Cino Hilliard_, Oct 30 2003",
			"ext": [
				"Edited to count twin pairs entirely within [PIPS3(n), PIPS3(n+1)], rather than pairs with the first prime in that interval. - _Danny Rorabaugh_, Apr 01 2015"
			],
			"references": 2,
			"revision": 24,
			"time": "2020-03-14T13:38:48-04:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}