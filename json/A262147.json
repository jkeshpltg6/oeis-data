{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A262147",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 262147,
			"data": "3,13,115,125,19,141,1011,1021,7171,7181,1027,7197,50403,50413,352915,352925,50419,352941,2470611,2470621,17294371,17294381,2470627,17294397,121060803,121060813,847425715,847425725,121060819,847425741",
			"name": "Numerators of domain values whose images via the flowsnake Q-function follow a spiral trajectory (see comments).",
			"comment": [
				"The spiral trajectory joins the centroids of hexagons that fit into a substitution hierarchy. Assign to each level of the hierarchy an index N in {0, 1, 2, ... }. Level 0 is a single hexagon. Subsequent levels of the hierarchy are enumerated by subdividing each hexagon at level N into seven hexagons at level N+1 (Cf. Gardner ). Call a hexagon central if its centroid coincides with the centroid of the hexagon at level 0. We determine a set of points in the plane by taking, for each N, the six centroids at level N+1 that fall within the central hexagon at level N but do not fall within the central hexagon at level N+1. Centroids are rational single-points, so the Q-function determines a bijection between our set of plane points and a set of rational numbers ( Klee, Complex Systems, Wolfram Demonstrations ). The fractional sequence a(n) is that set of rational numbers ordered by a(n+1) \u003e a(n). If we plot the image of a(n) we only see the spiral over domain [0,3/8] as it converges to the center of Gosper Island. The complete, double-spiral appears as Figure 12 in \"A Pit of Flowsnakes\"."
			],
			"reference": [
				"M. Gardner, Penrose Tiles and Trapdoor Ciphers... and the Return of Dr. Matrix, Cambridge University Press, 1997, pages 36-42."
			],
			"link": [
				"B. Klee, \u003ca href=\"http://www.complex-systems.com/abstracts/v24_i04_a01.html\"\u003eA Pit of Flowsnakes\u003c/a\u003e, Complex Systems, 24, 4 (2015).",
				"B. Klee, \u003ca href=\"http://demonstrations.wolfram.com/FlowsnakeQFunction/\"\u003eFlowsnake Q-Function\u003c/a\u003e, Wolfram Demonstrations(2015)."
			],
			"formula": [
				"Conjectures from _Colin Barker_, Jan 24 2016: (Start)",
				"a(n) = 50*a(n-6)-49*a(n-12) for n\u003e12.",
				"G.f.: (3 +13*x +115*x^2 +125*x^3 +19*x^4 +141*x^5 +861*x^6 +371*x^7 +1421*x^8 +931*x^9 +77*x^10 +147*x^11) / ((1 -x)*(1 +x)*(1 -x +x^2)*(1 +x +x^2)*(1 -7*x^3)*(1 +7*x^3)).",
				"(End)"
			],
			"example": [
				"FLSN[{3/56, 13/56, 115/392, 125/392, 19/56, 141/392, 1011/2744, 1021/2744, 7171/19208, 7181/19208, 1027/2744, 7197/19208}] = {3/14 + x/42, 4/7 - x/21, 30/49 + (17 x)/147, 31/49 + (29 x)/147, 51/98 + (73 x)/294, 19/49 + (32 x)/147, 153/343 + (170 x)/1029, 163/343 + (143 x)/1029, 1212/2401 + (1118 x)/7203, 2495/4802 + (2353 x)/14406, 1236/2401 + (1259 x)/7203, 1189/2401 + (1283 x)/7203}, where x = ( sqrt(3) * i )."
			],
			"mathematica": [
				"b[n_] := Select[Union@Join[Flatten[MapIndexed[#1/(8 7^#2[[1]]) \u0026, MapIndexed[With[{l =If[OddQ[#2[[1]] ], {10, 14, 8, 8, 10}, {10, 8, 8, 14,10}]},FoldList[Plus, #1, l]] \u0026,FoldList[7 (#1 + #2) + 24 \u0026, 3, Table[If[OddQ[i], 10, 26], {i, 1, 2 Ceiling[n/6]}]] ] ] ] ], # \u003c 3/8 \u0026][[1 ;; n]];Numerator[b[100]]"
			],
			"xref": [
				"Cf. A262148 (denominators)."
			],
			"keyword": "nonn,frac",
			"offset": "1,1",
			"author": "_Bradley Klee_, Sep 12 2015",
			"references": 1,
			"revision": 22,
			"time": "2016-03-13T10:24:37-04:00",
			"created": "2016-03-13T10:24:37-04:00"
		}
	]
}