{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A348227",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 348227,
			"data": "1,4,12,20,34,38,54,54,74,70,94,86,114,102,134,118,154,134,174,150,194,166,214,182,234,198,254,214,274,230,294,246,314,262,334,278,354,294,374,310,394,326,414,342,434,358,454,374,474,390,494,406,514,422,534,438,554",
			"name": "Coordination sequence for Wilkinson's 123-circle packing with respect to a circle of radius 1.",
			"comment": [
				"Wilkinson's 123-circle packing (that is my name for it) is a packing of non-overlapping circles in the plane, and can be seen in Figs. 1 and 1a. There are three sizes of circles: (a) radius 1, (b) radius 2, and (c) radius 3. Because 3^2 + 4^2 = 5^2, there is a right-angled triangle (shown in red in Fig. 3, \"Tiling of plane ...\") which when repeatedly translated and reflected builds the whole structure.",
				"A convenient set of coordinates for the centers are: (a) radius 1: the points (8*i, 6*j), (b) radius 2: the points (8*i, 6*j+3), and (c) radius 3: the points (8*i+4, 6*j), where i and j take all integer values.",
				"Let G denote the graph (Fig 1, Fig. 2) whose nodes correspond to the centers of the circles, with an edge for every pair of circles that touch. There are three types of nodes, corresponding to the circles of types (a), (b), and (c).",
				"The cells in the planar graph G form a tiling of the plane by two kinds of triangles (3-4-5 and 5-5-6).",
				"The coordination sequences for the three types of nodes in G are the present sequence, A348229, and A348231.",
				"If we draw tangent lines through the points where any two circles touch, we obtain a tiling of the plane (Fig. 3) with three kinds of tiles: squares, irregular hexagons, and irregular octagons.",
				"The boundary lines of the tiles form the cpq net (thanks to _Davide M. Proserpio_ for this observation). See Fig. 4 and the RCSR link. This net is the dual graph to G. It has two kinds of nodes, whose coordination sequences are given in A348236 and A348237."
			],
			"reference": [
				"Don Wilkinson, Packing Arrangement for Articles of Different Sizes, US Patent 4643307, Feb 17 1987."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A348227/b348227.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Chaim Goodman-Strauss and N. J. A. Sloane, \u003ca href=\"https://doi.org/10.1107/S2053273318014481\"\u003eA Coloring Book Approach to Finding Coordination Sequences\u003c/a\u003e, Acta Cryst. A75 (2019), 121-134, also \u003ca href=\"http://NeilSloane.com/doc/Cairo_final.pdf\"\u003eon NJAS's home page\u003c/a\u003e. Also \u003ca href=\"http://arxiv.org/abs/1803.08530\"\u003eon arXiv\u003c/a\u003e, arXiv:1803.08530 [math.CO], 2018-2019.",
				"Reticular Chemistry Structure Resource (RCSR), \u003ca href=\"http://rcsr.net/layers/cpq\"\u003eThe cpq tiling (or net)",
				"N. J. A. Sloane, \u003ca href=\"/A348227/a348227_5.pdf\"\u003eFig. 1: Sketch showing portion of Wilkinson's 123-circle packing and the underlying graph\u003c/a\u003e.",
				"N. J. A. Sloane, \u003ca href=\"/A348227/a348227_9.pdf\"\u003eFig. 1a: Drawing of portion of Wilkinson's 123-circle packing\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/A348227/a348227_3.pdf\"\u003eFig. 2: Graph G formed by centers of Wilkinson's 123-circle packing\u003c/a\u003e (black: center of circle of radius 1, green: radius 2, red: radius 3). This figure should be rotated counterclockwise by 90 degrees in order to match the other figures.",
				"N. J. A. Sloane, \u003ca href=\"/A348227/a348227_7.pdf\"\u003eFig. 3: Tiling of plane corresponding to Wilkinson's 123-circle packing\u003c/a\u003e. There are three tiles: squares (a), irregular hexagons (b), and irregular octagons (c). The red Pythagorean triangle, with sides 3, 4, 5, is a fundamental cell which underlies the whole structure.",
				"N. J. A. Sloane, \u003ca href=\"/A348227/a348227_8.pdf\"\u003eFig. 4: A portion of the cpq net formed by the boundaries of the tiles\u003c/a\u003e (the numbers correspond to the coordination sequence discussed in A348236).",
				"Don Wilkinson, \u003ca href=\"/A348227/a348227_2.pdf\"\u003eLetter to N. J. A. Sloane, Oct 12 1990\u003c/a\u003e. Among other things, this letter enclosed the illustration shown in the next link, and a copy of US Patent 4643307. [The handwritten notes at the top and bottom of the letter were added by me after the letter arrived.]",
				"Don Wilkinson, \u003ca href=\"/A348227/a348227_1.pdf\"\u003eIllustration of part of the circle packing\u003c/a\u003e [Enclosed with his letter of Oct 12 1990. The red numbers were added by me.]",
				"\u003ca href=\"/index/Con#coordination_sequences\"\u003eIndex entries for coordination sequences\u003c/a\u003e"
			],
			"formula": [
				"G.f. = (1+4*q+10*q^2+12*q^3+11*q^4+2*q^5-2*q^6-2*q^7)/(1-q^2)^2. Discovered and proved using the \"coloring book\" method (see Goodman-Strauss \u0026 Sloane)."
			],
			"example": [
				"We start at a black point (with a(0) = 1). It is joined to 4 neighbors (so a(1) = 4), which in turn are joined to 12 further points (so a(2) = 12), which are joined to 20 additional points (so a(2) = 20), and so on."
			],
			"xref": [
				"Cf. A348228-A348239."
			],
			"keyword": "nonn,nice",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Oct 08 2021",
			"references": 9,
			"revision": 101,
			"time": "2021-12-16T20:12:49-05:00",
			"created": "2021-10-08T12:04:21-04:00"
		}
	]
}