{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A226240",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 226240,
			"data": "1,2,0,4,2,0,0,0,2,6,0,4,4,0,0,0,2,4,0,4,0,0,0,0,4,2,0,8,0,0,0,0,2,8,0,0,6,0,0,0,0,4,0,4,4,0,0,0,4,2,0,8,0,0,0,0,0,8,0,4,0,0,0,0,2,0,0,4,4,0,0,0,6,4,0,4,4,0,0,0,0,10,0,4,0,0",
			"name": "Expansion of phi(q^4) * phi(q^8) + 2 * q *phi(q^2) * psi(q^8) in powers of q where phi(), psi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of phi(q) * phi(q^8) + 4 * q^3 * psi(q^8) * psi(q^16) in powers of q where phi(), psi() are Ramanujan theta functions.",
				"a(n) = 2 * b(n) where b(n) is multiplicative and b(2) = 0, b(2^e) = 1 if e\u003e1, b(p^e) = e+1 if p == 1, 3 (mod 8), b(p^e) = (1 + (-1)^e)/2 if p == 5, 7 (mod 8).",
				"G.f.: 1 + 2 * Sum_{k\u003e0 \u0026 k !=2 (mod 4)} q^k * (1 + q^(2*k)) / (1 + q^(4*k)).",
				"a(4*n + 2) = 0. a(2*n + 1) = 2 * A113411(n). a(4*n) = A033715(n)."
			],
			"example": [
				"G.f. = 1 + 2*q + 4*q^3 + 2*q^4 + 2*q^8 + 6*q^9 + 4*q^11 + 4*q^12 + 2*q^16 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ 1 + 2 Sum[ Boole[ 2 != Mod[ k, 4]] q^k (1 + q^(2 k)) / (1 + q^(4 k)), {k, n}], {q, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n==0, 2 * (n%4 != 2) * sumdiv( n, d, kronecker( -2, d)))};",
				"(PARI) {a(n) = local(A, p, e); if( n\u003c1, n==0, A = factor(n); 2 * prod( k= 1, matsize(A)[1], if( p=A[k, 1], e=A[k, 2]; if( p==2, e\u003e1, if( p%8\u003c5, e+1, (1 + (-1)^e) / 2)))))};",
				"(PARI) {a(n) = local(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( (eta(x^8 + A) * eta(x^16 + A))^3 / (eta(x^4 + A) * eta(x^32 + A))^2 + 2 * x * eta(x^4 + A)^5 * eta(x^16 + A)^2 / (eta(x^2 + A)^2 * eta(x^8 + A)^3), n))};",
				"(MAGMA) A := Basis( ModularForms( Gamma1(32), 1), 87); A[1] + 2*A[2] + 4*A[4] + 2*A[5] + 2*A[9] + 6*A[10] + 4*A[12] + 4*A[13] + 4*A[16]; /* _Michael Somos_, Jun 18 2014 */"
			],
			"xref": [
				"Cf. A033715, A113411."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Michael Somos_, Jun 01 2013",
			"references": 2,
			"revision": 14,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2013-06-01T10:28:08-04:00"
		}
	]
}