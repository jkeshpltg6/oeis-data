{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A176740",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 176740,
			"data": "-1,-1,3,-1,10,-15,-1,15,10,-105,105,-1,21,35,-210,-280,1260,-945,-1,28,56,35,-378,-1260,-280,3150,6300,-17325,10395,-1,36,84,126,-630,-2520,-1575,-2100,6930,34650,15400,-51975,-138600,270270,-135135,-1,45,120,210,126,-990,-4620,-6930,-4620,-5775",
			"name": "Inversion of e.g.f. formal power series. Partition array in Abramowitz-Stegun (A-St) order.",
			"comment": [
				"Compare with A134685 which uses a different order with fewer entries.",
				"For the inversion (aka reversion) of o.g.f. formal power series see A111785, and also A133437.",
				"The sequence of row lengths of this array is p(n)=A000041(n) (number of partitions of n).",
				"The unsigned triangle, with entries for like parts number m summed, is A134991 (2-associated Stirling numers of the second kind).",
				"The row sums are A133942(n) = ((-1)^n) * n!, and the row sums of the unsigned array give A000311(n+1) (Schroeder's fourth problem). These sums coincide with those of the triangle A134991.",
				"The signed a(n,k) numbers, k=1,...,p(n)=A000041(n), derive from the multinomial M_3 numbers A036040 (see also the W. Lang link there), namely, if the k-th partitions of n in A-St order has exponents (enk[1],..,enk[n]) then a(n,k) = ((-1)^m)*M3(n+m, (ehatnk[1],...,ehatnk[n+m])) with m the number of parts, i.e., m:=sum(enk[j],j=1..n), and *M3(n+m, (ehatnk[1],...,ehatnk[n+m])):=(n+m)!/(product(j!^ehatnk[j]*ehatnk[j]!,j=1..n+m)), where the n+m exponents ehatnk are ehatnk[1]:=0, (ehatnk[2],...,ehatnk[n+1]) := (enk[1],..,enk[n]), and (ehatnk[n+1],...,ehatnk[n+m]):=(0,...,0) (i.e., m-1 zeros) .",
				"The compositional inverse of the formal power series of the e.g.f. type g(x) = sum(g[j]*(x^j)/j! ,j=1..infty) is f=g^[-1] with f(y) = sum(f[n]*(y^n)/n! ,n=1..infty), and f[n] = fhat[n]/g[1]^(2*n-1) with fhat[1]=1 (f[1] = 1/g[1]) and f[n+1] = sum( a(n,k)*g(n,k) ,k=1..p(n)), n\u003e=1, where p(n)=A000041(n) (number of partitions of n), and g(n,k) is the monomial in coefficients of g(x) corresponding to the k-th partition of 2*n with n parts in A-St order. For details and a remark on the Faa di Bruno Hopf algebra see the W. Lang link."
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), p. 831-2.",
				"R. Aldrovandi, Special Matrices of Mathematical Physics, World Scientific, 2001, p. 175, eq. (13.84).",
				"Ch. A. Charalambides, Enumerative Combinatorics, Chapman \u0026Hall/CRC, 2002, p. 437, eq. (11.43) with p. 428. eq. (11.29)."
			],
			"link": [
				"W. P. Johnson, \u003ca href=\"http://www.jstor.org/stable/2695356\"\u003eCombinatorics of Higher Derivatives of Inverses\u003c/a\u003e, Amer. Math. Monthly 109 (3), (2002), 273-277",
				"Wolfdieter Lang, \u003ca href=\"/A176740/a176740_1.pdf\"\u003eE.g.f. Lagrange inversion partition array.\u003c/a\u003e"
			],
			"formula": [
				"See the fhat[n] formula explained above, and the W. Lang link for more details."
			],
			"example": [
				"[-1],",
				"[-1,3],",
				"[-1,10,-15],",
				"[-1,15,10,-105,105],",
				"[-1,21,35,-210,-280,1260,-945],",
				"...",
				"a(4,4): 4th partition of 4 has exponents (2,1,0,0) with m=3, and the derived exponents ehatm are (0,2,1,0,0,0,0) with one leading and 2 extra trailing zeros. (4+3)!/(2!^2*2!*3!^1*1!) = 105, hence a(4,4) = ((-1)^3)*105 = -105.",
				"fhat[4] = -1*g[1]^2*g[4] +10*g[1]*g[2]*g[3] - 15*g[2]^3 (n=3: 3 parts partitions of 6 for the g-monomials in A-St order)."
			],
			"keyword": "sign,easy,tabf",
			"offset": "0,3",
			"author": "_Wolfdieter Lang_, Jul 14 2010",
			"references": 3,
			"revision": 22,
			"time": "2015-03-24T16:58:02-04:00",
			"created": "2010-07-31T03:00:00-04:00"
		}
	]
}