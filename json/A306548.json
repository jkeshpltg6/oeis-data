{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A306548",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 306548,
			"data": "0,0,0,1,0,0,2,1,0,0,3,4,1,0,0,4,10,8,1,0,0,5,20,34,16,1,0,0,6,35,104,118,32,1,0,0,7,56,259,560,418,64,1,0,0,8,84,560,2003,3104,1510,128,1,0,0,9,120,1092,5888,16003,17600,5554,256,1,0,0,10,165,1968,14988,64064,130835,101504,20758,512,1,0,0",
			"name": "Triangle T(n,k) read by rows, where k-th column shows shifted self-convolution of power function n^k, n \u003e= 0, 0 \u003c= k \u003c= n.",
			"comment": [
				"For n \u003e 0 an odd-power identity n^(2m+1)+1, m \u003e= 0 can be found using the current sequence. The sum of the n-th diagonal of T(n,k) over 0 \u003c= k \u003c= m multiplied by A(m,k) gives n^(2m+1)-1, where A(m,k) = A302971(m,k)/A304042(m,k). For example, consider the case n=4, m=2, the n-th diagonal of T(n, 0 \u003c= k \u003c= m) is {5, 10, 34}, the m-th row of triangle A(m, 0 \u003c= k \u003c= m) is {1, 0, 30}, thus (3+1)^5 + 1 = 5*1 + 10*0 + 34*30 = 1025."
			],
			"link": [
				"D. V. Widder et al., \u003ca href=\"https://doi.org/10.1090/S0002-9904-1954-09828-2\"\u003eThe Convolution Transform\u003c/a\u003e, Bull. Amer. Math. Soc. 60 (1954), 444-456.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Convolution\"\u003eConvolution\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Convolution_power\"\u003eConvolution power\u003c/a\u003e."
			],
			"formula": [
				"f(m, s) = s^m, if s \u003e= 0;",
				"f(m, s) = 0, otherwise.",
				"F(n,m) = Sum_{k} f(m, n-k) * f(m, k), -infty \u003c k \u003c +infty;",
				"T(n,k) = F(n-k, k)."
			],
			"example": [
				"==================================================================",
				"k=    0     1     2     3      4      5     6    7    8    9    10",
				"==================================================================",
				"n=0:  2;",
				"n=1:  2,    0;",
				"n=2:  3,    0,    0;",
				"n=3:  4,    1,    0,    0;",
				"n=4:  5,    4,    1,    0,     0;",
				"n=5:  6,   10,    8,    1,     0,     0;",
				"n=6:  7,   20,   34,   16,     1,     0,    0;",
				"n=7:  8,   35,  104,  118,    32,     1,    0,   0;",
				"n=8:  9,   56,  259,  560,   418,    64,    1,   0,   0;",
				"n=9:  10,  84,  560, 2003,  3104,  1510,  128,   1,   0,   0;",
				"n=10: 11, 120, 1092, 5888, 16003, 17600, 5554, 256,   1,   0;   0;",
				"..."
			],
			"mathematica": [
				"f[m_, s_] := Piecewise[{{s^m, s \u003e= 0}, {0, True}}];",
				"F[n_, m_] := Sum[f[m, n - k]*f[m, k], {k, -Infinity, +Infinity}];",
				"T[n_, k_] := F[n - k, k];",
				"Column[Table[T[n, k], {n, 0, 12}, {k, 0, n}], Left]"
			],
			"xref": [
				"Nonzero terms of columns k=0..5 give: A000027, A000292, A033455, A145216, A145217, A145218.",
				"Partial sums of columns k=1..2 give: A000332, A259181.",
				"Cf. A302971, A304042, A198633, A000079."
			],
			"keyword": "nonn,tabl",
			"offset": "0,7",
			"author": "_Kolosov Petro_, Feb 23 2019",
			"ext": [
				"Edited by _Kolosov Petro_, Mar 13 2019"
			],
			"references": 1,
			"revision": 79,
			"time": "2021-02-12T12:16:42-05:00",
			"created": "2019-02-24T01:16:39-05:00"
		}
	]
}