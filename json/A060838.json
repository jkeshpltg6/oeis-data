{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A060838",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 60838,
			"data": "0,0,0,0,0,1,1,0,1,0,0,1,1,0,1,0,1,0,2,1,0,1,0,0,0,1,0,1,0,2,1,0,1,1,1,0,2,0,0,0,0,1,1,0,0,0,0,1,1,1,1,0,1,0,0,1,0,1,0,0,1,1,1,0,2,0,1,1,1,1,1,1,0,0,1,0,0,1,1,0,0,0,0,1,1",
			"name": "Rank of elliptic curve x^3 + y^3 = n.",
			"comment": [
				"The elliptic curve X^3 + Y^3 = D*Z^3 where D is a rational integer has a birationally equivalent form y^2*z = x^3 - 2^4*3^3*D^2*z^3 where x = 2^2*3*D*Z, y = 2^2*3^3*D*(Y - X), z = X + Y (see p. 123 of Stephens). Taking z = 1 and 2^2*3^3 = 432 yields y^2 = x^3 - 432*D^2, which is the Weierstrass form of the elliptic curve used by John Voight in the MAGMA program below. - _Ralf Steiner_, Nov 11 2017",
				"Zagier and Kramarz studied the analytic rank of the curve E: x^3 + y^3 = m, where m is cubefree. They computed L(E,1) for 0 \u003c m \u003c= 70000 and also L'(E,1) if the sign of the functional equation for L(E,1) was negative. In the second case the range was only 0 \u003c m \u003c= 20000. - Attila Pethő, Posting to the Number Theory List, Nov 11 2017"
			],
			"link": [
				"John Voight and Joseph L. Wetherell, \u003ca href=\"/A060838/b060838.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Nakao Hisayasu, \u003ca href=\"http://www.kaynet.or.jp/~kay/misc/nna2.html\"\u003eTables of the rank and rational points for the elliptic curve x^3 + y^3 = n for n cubefree, 1\u003c=n\u003c=200\u003c/a\u003e (text in Japanese)",
				"Nakao Hisayasu, \u003ca href=\"http://www.kaynet.or.jp/~kay/misc/nna3.html\"\u003eTables of the rank and rational points for the elliptic curve x^3 + y^3 = n for n cubefree, 201\u003c=n\u003c=500\u003c/a\u003e (text in Japanese)",
				"Nakao Hisayasu, \u003ca href=\"http://www.kaynet.or.jp/~kay/misc/nna4.html\"\u003eTables of the rank and rational points for the elliptic curve x^3 + y^3 = n for n cubefree, 501\u003c=n\u003c=1000\u003c/a\u003e (text in Japanese)",
				"Nakao Hisayasu, \u003ca href=\"http://www.kaynet.or.jp/~kay/misc/nna5.html\"\u003eTables of the rank and rational points for the elliptic curve x^3 + y^3 = n for n cubefree, 1001\u003c=n\u003c=1500\u003c/a\u003e (text in Japanese)",
				"Nakao Hisayasu, \u003ca href=\"http://www.kaynet.or.jp/~kay/misc/nna6.html\"\u003eTables of the rank and rational points for the elliptic curve x^3 + y^3 = n for n cubefree, 1501\u003c=n\u003c=2000\u003c/a\u003e (text in Japanese)",
				"Nakao Hisayasu, \u003ca href=\"http://www.kaynet.or.jp/~kay/misc/nna7.html\"\u003eTables of the rank and rational points for the elliptic curve x^3 + y^3 = n for n cubefree, 2001\u003c=n\u003c=2500\u003c/a\u003e (text in Japanese)",
				"...",
				"Nakao Hisayasu, \u003ca href=\"http://www.kaynet.or.jp/~kay/misc/nna20.html\"\u003eTables of the rank and rational points for the elliptic curve x^3 + y^3 = n for n cubefree, 8501\u003c=n\u003c=9000\u003c/a\u003e (text in Japanese)",
				"Nakao Hisayasu, \u003ca href=\"http://www.kaynet.or.jp/~kay/misc/nna21.html\"\u003eTables of the rank and rational points for the elliptic curve x^3 + y^3 = n for n cubefree, 9001\u003c=n\u003c=9500\u003c/a\u003e (text in Japanese)",
				"Nakao Hisayasu, \u003ca href=\"http://www.kaynet.or.jp/~kay/misc/nna22.html\"\u003eTables of the rank and rational points for the elliptic curve x^3 + y^3 = n for n cubefree, 9501\u003c=n\u003c=10000\u003c/a\u003e (text in Japanese)",
				"N. M. Stephens, \u003ca href=\"https://doi.org/10.1515/crll.1968.231.121\"\u003eThe Diophantine equation X^3 + Y^3 = D Z^3 and the conjectures of Birch and Swinnerton-Dyer\u003c/a\u003e, J. Reine Angew. Math. 231 (1968), 121-162.",
				"D. Zagier and G. Kramarz, \u003ca href=\"http://www.informaticsjournals.com/index.php/jims/article/view/21978\"\u003eNumerical investigations related to the L-series of certain elliptic curves\u003c/a\u003e, J. Indian Math. Soc. 52 (1987), 51-60 (the Ramanujan Centenary volume)."
			],
			"program": [
				"(MAGMA)",
				"seq := [];",
				"M := 10000;",
				"for m := 1 to M do",
				"E := EllipticCurve([0,-432*m^2]);",
				"Append(~seq, Rank(E));",
				"end for;",
				"seq;",
				"// John Voight, Nov 02 2017",
				"(PARI) {a(n) = ellanalyticrank(ellinit([0, 0, 0, 0, -432*n^2]))[1]} \\\\ _Seiichi Manyama_, Aug 25 2019"
			],
			"xref": [
				"Cf. A060748 (positions of records in this sequence), A060950."
			],
			"keyword": "nonn,nice",
			"offset": "1,19",
			"author": "Noam Katz (noamkj(AT)hotmail.com), May 02 2001",
			"ext": [
				"Many thanks to _Andrew V. Sutherland_, John Voight, and _Joseph L. Wetherell_, who all responded to my request for additional terms for this sequence. - _N. J. A. Sloane_, Nov 01 2017"
			],
			"references": 17,
			"revision": 69,
			"time": "2019-12-01T01:21:56-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}