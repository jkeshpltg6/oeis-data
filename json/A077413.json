{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A077413",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 77413,
			"data": "2,13,76,443,2582,15049,87712,511223,2979626,17366533,101219572,589950899,3438485822,20040964033,116807298376,680802826223,3968009658962,23127255127549,134795521106332,785645871510443,4579079707956326,26688832376227513,155553914549408752",
			"name": "Bisection (odd part) of Chebyshev sequence with Diophantine property.",
			"comment": [
				"-8*a(n)^2 + b(n)^2 = 17, with the companion sequence b(n) = A077239(n).",
				"The even part is A054488(n) with Diophantine companion A077240(n)."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A077413/b077413.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (6,-1)."
			],
			"formula": [
				"a(n) = 6*a(n-1) - a(n-2), a(-1)=-1, a(0)=2.",
				"a(n) = 2*S(n, 6)+S(n-1, 6), with S(n, x) = U(n, x/2), Chebyshev polynomials of 2nd kind, A049310. S(n, 6) = A001109(n+1).",
				"G.f.: (2+x)/(1-6*x+x^2).",
				"a(n) = (((3-2*sqrt(2))^n*(-7+4*sqrt(2))+(3+2*sqrt(2))^n*(7+4*sqrt(2))))/(4*sqrt(2)). - _Colin Barker_, Oct 12 2015"
			],
			"example": [
				"8*a(1)^2 + 17 = 8*13^2+17 = 1369 = 37^2 = A077239(1)^2."
			],
			"mathematica": [
				"LinearRecurrence[{6,-1}, {2,13}, 30] (* or *) CoefficientList[Series[ (2+x)/(1-6*x+x^2), {x, 0, 50}], x] (* _G. C. Greubel_, Jan 18 2018 *)"
			],
			"program": [
				"(PARI) Vec((2+x)/(1-6*x+x^2) + O(x^30)) \\\\ _Colin Barker_, Jun 16 2015",
				"(MAGMA) I:=[2,13]; [n le 2 select I[n] else 6*Self(n-1) - Self(n-2): n in [1..30]]; // _G. C. Greubel_, Jan 18 2018"
			],
			"xref": [
				"Cf. A077241 (even and odd parts)."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_Wolfdieter Lang_, Nov 08 2002",
			"references": 6,
			"revision": 23,
			"time": "2018-01-18T19:36:02-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}