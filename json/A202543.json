{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A202543",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 202543,
			"data": "9,6,2,4,2,3,6,5,0,1,1,9,2,0,6,8,9,4,9,9,5,5,1,7,8,2,6,8,4,8,7,3,6,8,4,6,2,7,0,3,6,8,6,6,8,7,7,1,3,2,1,0,3,9,3,2,2,0,3,6,3,3,7,6,8,0,3,2,7,7,3,5,2,1,6,4,4,3,5,4,8,8,2,4,0,1,8,8,5,8,2,4,5,4,4,6,9,4,9",
			"name": "Decimal expansion of the number x satisfying e^(x/2) - e^(-x/2) = 1.",
			"comment": [
				"See A202537 for a guide to related sequences. The Mathematica program includes a graph.",
				"W. Gawronski et al. in their paper - see ref. below - obtained the asymptotics for the Chebyshev-Stirling numbers. In the algebraic description of the respective \"asymptotic coefficients\" the number x = 2*log phi, where phi is the golden section, play the central role. - _Roman Witula_, Feb 02 2015",
				"Also two times the Lévy measure for the continued fraction of the golden section, i.e., A202543/log(2) is the mean number of bits gained from the next convergent of the continued fraction representation. (See also Dan Lascu in links.) - _A.H.M. Smeets_, Jun 06 2018"
			],
			"link": [
				"W. Gawronski, L. L. Littlejohn, and T. Neuschel, \u003ca href=\"http://arxiv.org/abs/1308.6803\"\u003eAsymptotics of Stirling and Chebyshev-Stirling numbers of the second kind\u003c/a\u003e, arXiv:1308.6803 [math.CO], 2013.",
				"W. Gawronski, L. L. Littlejohn, and T. Neuschel, \u003ca href=\"http://dx.doi.org/10.1111/sapm.12037\"\u003eAsymptotics of Stirling and Chebyshev-Stirling numbers of the second kind\u003c/a\u003e, Studies in Applied Mathematics by MIT 133 (2014), 1-17.",
				"Dan Lascu, \u003ca href=\"https://doi.org/10.1016/j.jnt.2012.12.007\"\u003eA Gauss-Kuzmintype problem for a family of continued fraction expansions\u003c/a\u003e, Journal of Number Theory 133 (2013), 2153-2181.",
				"\u003ca href=\"/index/Tra#transcendental\"\u003eIndex entries for transcendental numbers\u003c/a\u003e"
			],
			"formula": [
				"Equals 2*A002390. - _A.H.M. Smeets_, Jun 06 2018",
				"From _Amiram Eldar_, Aug 21 2020: (Start)",
				"Equals log(A104457) = log(1 + A001622).",
				"Equals 2*arcsinh(1/2). [corrected by _Georg Fischer_, Jul 12 2021]",
				"Equals Sum_{k\u003e=0} (-1)^k*binomial(2*k,k)/((2*k+1)*16^k). (End)"
			],
			"example": [
				"0.9624236501192068949955178268487368462703686..."
			],
			"mathematica": [
				"u = 1/2; v = 1/2;",
				"f[x_] := E^(u*x) - E^(-v*x); g[x_] := 1",
				"Plot[{f[x], g[x]}, {x, 0, 2}, {AxesOrigin -\u003e {0, 0}}]",
				"r = x /. FindRoot[f[x] == g[x], {x, .9, 1}, WorkingPrecision -\u003e 110]",
				"RealDigits[r]    (* A202543 *)",
				"RealDigits[ Log[ (3+Sqrt[5])/2], 10, 99] // First (* _Jean-François Alcover_, Feb 27 2013 *)",
				"RealDigits[ FindRoot[ Exp[x/2] == 1 +  Exp[-x/2] , {x, 0}, WorkingPrecision -\u003e 128][[1, 2]]][[1]] (* _Robert G. Wilson v_, Jun 13 2018 *)"
			],
			"program": [
				"(PARI) 2*asinh(1/2) \\\\ _Michel Marcus_, Jun 24 2018, after A002390"
			],
			"xref": [
				"Cf. A001622, A002390, A104457, A202537, A202543."
			],
			"keyword": "nonn,cons",
			"offset": "0,1",
			"author": "_Clark Kimberling_, Dec 21 2011",
			"ext": [
				"Typo in name fixed by _Jean-François Alcover_, Feb 27 2013"
			],
			"references": 6,
			"revision": 54,
			"time": "2021-07-12T15:59:13-04:00",
			"created": "2011-12-21T18:15:28-05:00"
		}
	]
}