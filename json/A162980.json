{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A162980",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 162980,
			"data": "1,0,1,0,0,1,1,1,2,2,1,6,6,3,1,22,22,12,4,1,102,102,51,15,2,506,506,264,88,19,2,2952,2952,1476,458,89,9,18502,18502,9504,3168,726,110,9,131112,131112,65556,20868,4479,621,44,991226,991226,504864,168288,39696,6672,749,44",
			"name": "Triangle read by rows: T(n,k) is the number of reverse alternating (i.e., up-down) permutations of {1,2,...,n} having k fixed points (n \u003e= 0, 0 \u003c= k \u003c= 1 + floor(n/2)).",
			"comment": [
				"Row n has 2 + floor(n/2) entries (n\u003e=5).",
				"Sum of entries in row n is the Euler (up-down) number A000111(n).",
				"T(n,0) = T(n,1) = A129815(n) (n\u003e=1).",
				"T(2n-1,n) = T(2n,n+1) = d(n-1), where d(n) = A000166 is a derangement number (see the Chapman \u0026 Williams reference).",
				"Sum_{k\u003e=0} k*T(n,k) = A162977(n)."
			],
			"link": [
				"R. Chapman and L. K. Williams, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v14i1n16\"\u003eA conjecture of Stanley on alternating permutations\u003c/a\u003e, The Electronic J. of Combinatorics, 14, 2007, #N16.",
				"R. P. Stanley, \u003ca href=\"https://dx.doi.org/10.1016/j.jcta.2006.06.008\"\u003eAlternating permutations and symmetric functions\u003c/a\u003e, J. Comb. Theory A 114 (3) (2007) 436-460."
			],
			"formula": [
				"The row generating polynomials can be obtained from Proposition 6.1 of the Stanley reference (see the Maple program)."
			],
			"example": [
				"T(5,2)=3 because we have 15243, 14352, and 25341.",
				"Triangle starts:",
				"    1;",
				"    0,   1;",
				"    0,   0,   1;",
				"    1,   1;",
				"    2,   2,   1;",
				"    6,   6,   3,   1;",
				"   22,  22,  12,   4,   1;",
				"  102, 102,  51,  15,   2;"
			],
			"maple": [
				"fo := exp(E*(arctan(q*t)-arctan(t)))/(1-E*t): fe := sqrt((1+q^2*t^2)/(1+t^2))*exp(E*(arctan(q*t)-arctan(t)))/(1-E*t): foser := simplify(series(fo, t = 0, 18)): feser := simplify(series(fe, t = 0, 18)): Q := proc (n) if `mod`(n, 2) = 1 then coeff(foser, t, n) else coeff(feser, t, n) end if end proc: for n from 0 to 16 do Q(n) end do: g := sec(x)+tan(x): gser := series(g, x = 0, 20): for n from 0 to 18 do a[n] := factorial(n)*coeff(gser, x, n) end do: for n from 0 to 15 do P[n] := sort(subs({E = a[1], E^2 = a[2], E^3 = a[3], E^4 = a[4], E^5 = a[5], E^6 = a[6], E^7 = a[7], E^8 = a[8], E^9 = a[9], E^10 = a[10], E^11 = a[11], E^12 = a[12], E^13 = a[13], E^14 = a[14], E^15 = a[15], E^16 = a[16]}, Q(n))) end do: 1; 0, 1; 0, 0, 1; 1, 1; 2, 2, 1; for n from 5 to 13 do seq(coeff(P[n], q, j), j = 0 .. 1+floor((1/2)*n)) end do;"
			],
			"mathematica": [
				"nmax = 10;",
				"fo = Exp[e*(ArcTan[q*t] - ArcTan[t])]/(1 - e*t);",
				"fe = Sqrt[(1+q^2 t^2)/(1+t^2)]*Exp[e*(ArcTan[q*t] - ArcTan[t])]/(1-e*t);",
				"Q[n_] := If[OddQ[n], SeriesCoefficient[fo, {t, 0, n}], SeriesCoefficient[fe, {t, 0, n}]] // Expand;",
				"b[n_] := n!*SeriesCoefficient[Sec[x] + Tan[x], {x, 0, n}];",
				"P[n_] := (Q[n] /. e^k_Integer :\u003e b[k]) /. e :\u003e b[1] // Expand;",
				"T[n_, k_] := Coefficient[P[n], q, k];",
				"Table[CoefficientList[P[n], q], {n, 0, nmax}] // Flatten (* _Jean-François Alcover_, Jul 24 2018, from Maple *)"
			],
			"xref": [
				"Cf. A000111, A000166, A129815, A162977, A162979."
			],
			"keyword": "nonn,tabf",
			"offset": "0,9",
			"author": "_Emeric Deutsch_, Aug 06 2009",
			"references": 3,
			"revision": 13,
			"time": "2018-07-24T09:40:22-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}