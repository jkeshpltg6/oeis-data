{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A121868",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 121868,
			"data": "0,-1,-1,0,5,23,74,161,-57,-3466,-27361,-155397,-687688,-1888525,4974059,134695952,1400820897,11055147275,70658948426,327448854237,223871274083,-19116044475298,-314203665206509,-3562429698724513,-33024521386113840,-250403183401213513",
			"name": "Let A(0) = 1, B(0) = 0; A(n+1) = Sum_{k=0..n} binomial(n,k)*B(k), B(n+1) = Sum_{k=0..n} -binomial(n,k)*A(k); entry gives B sequence (cf. A121867).",
			"comment": [
				"Stirling transform of (I^(n+1)+(-I)^(n+1))/2 = (0,-1,0,1,..) repeated."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A121868/b121868.txt\"\u003eTable of n, a(n) for n = 0..250\u003c/a\u003e",
				"A. Fekete and G. Martin, \u003ca href=\"http://www.jstor.org/stable/2695545\"\u003eProblem 10791: Squared Series Yielding Integers\u003c/a\u003e, Amer. Math. Monthly, 108 (No. 2, 2001), 177-178.",
				"V. V. Kruchinin, \u003ca href=\"http://arxiv.org/abs/1009.2565\"\u003eComposition of ordinary generating functions\u003c/a\u003e, arXiv:1009.2565 [math.CO], 2010."
			],
			"formula": [
				"This sequence and its companion A121867 are related to the pair of constants cos(1) + sin(1) and cos(1) - sin(1) and may be viewed as generalizations of the Uppuluri-Carpenter numbers (complementary Bell numbers) A000587. Define E_2(k) = Sum_{n \u003e= 0} (-1)^floor(n/2) *n^k/n! for k = 0,1,2,... . Then E_2(0) = cos(1) + sin(1) and E_2(1) = cos(1) - sin(1). Furthermore, E_2(k) is an integral linear combination of E_2(0) and E_2(1) (a Dobinski-type relation). For example, E_2(2) = - E_2(0) + E_2(1), E_2(3) = -3*E_2(0) and E_2(4) = - 6*E_2(0) - 5*E_2(1). More examples are given below. The precise result is E_2(k) = A121867(k) * E_2(0) - A121868(k) * E_2(1). For similar results see A143628. The decimal expansions of E_2(0) and E_2(1) are given in A143623 and A143624 respectively. - _Peter Bala_, Aug 28 2008",
				"E.g.f.: A(x) = -sin(exp(x)-1).",
				"a(n) = Sum_{k=0..floor(n/2)} stirling2(n,2*k+1)*(-1)^(k+1). - _Vladimir Kruchinin_, Jan 26 2011"
			],
			"example": [
				"From _Peter Bala_, Aug 28 2008: (Start)",
				"E_2(k) as a linear combination of E_2(i), i = 0..1.",
				"============================",
				"..E_2(k)..|...E_2(0)..E_2(1)",
				"============================",
				"..E_2(2)..|....-1.......1...",
				"..E_2(3)..|....-3.......0...",
				"..E_2(4)..|....-6......-5...",
				"..E_2(5)..|....-5.....-23...",
				"..E_2(6)..|....33.....-74...",
				"..E_2(7)..|...266....-161...",
				"..E_2(8)..|..1309......57...",
				"..E_2(9)..|..4905....3466...",
				"(End)"
			],
			"maple": [
				"# Maple code for A024430, A024429, A121867, A121868.",
				"M:=30; a:=array(0..100); b:=array(0..100); c:=array(0..100); d:=array(0..100); a[0]:=1; b[0]:=0; c[0]:=1; d[0]:=0;",
				"for n from 1 to M do a[n]:=add(binomial(n-1,k)*b[k], k=0..n-1); b[n]:=add(binomial(n-1,k)*a[k], k=0..n-1); c[n]:=add(binomial(n-1,k)*d[k], k=0..n-1); d[n]:=-add(binomial(n-1,k)*c[k], k=0..n-1); od: ta:=[seq(a[n],n=0..M)]; tb:=[seq(b[n],n=0..M)]; tc:=[seq(c[n],n=0..M)]; td:=[seq(d[n],n=0..M)];",
				"# Code based on Stirling transform:",
				"stirtr:= proc(p) proc(n) option remember;",
				"            add(p(k) *Stirling2(n, k), k=0..n) end",
				"         end:",
				"a:= stirtr(n-\u003e (I^(n+1) + (-I)^(n+1))/2):",
				"seq(a(n), n=0..30);  # _Alois P. Heinz_, Jan 29 2011"
			],
			"mathematica": [
				"stirtr[p_] := Module[{f}, f[n_] := f[n] = Sum[p[k]*StirlingS2[n, k], {k, 0, n}]; f]; a = stirtr[(I^(#+1)+(-I)^(#+1))/2\u0026]; Table[a[n], {n, 0, 30}] (* _Jean-François Alcover_, Mar 11 2014, after _Alois P. Heinz_ *)",
				"Table[Im[BellB[n, -I]], {n, 0, 25}] (* _Vladimir Reshetnikov_, Oct 22 2015 *)"
			],
			"program": [
				"(PARI) a(n) = sum(k=0,n\\2, (-1)^(k+1)*stirling(n,2*k+1,2));",
				"vector(30, n, a(n-1)) \\\\ _G. C. Greubel_, Oct 09 2019",
				"(MAGMA) [(\u0026+[(-1)^(k+1)*StirlingSecond(n,2*k+1): k in [0..Floor(n/2)]]): n in [0..30]]; // _G. C. Greubel_, Oct 09 2019",
				"(Sage) [sum((-1)^(k+1)*stirling_number2(n,2*k+1) for k in (0..floor(n/2))) for n in (0..30)] # _G. C. Greubel_, Oct 09 2019",
				"(GAP) List([0..30], n-\u003e Sum([0..Int(n/2)], k-\u003e (-1)^(k+1)* Stirling2(n,2*k+1)) ); # _G. C. Greubel_, Oct 09 2019"
			],
			"xref": [
				"Cf. A121867, A024430, A024429.",
				"Cf. A000587, A143623, A143624, A143628, A143631. - _Peter Bala_, Aug 28 2008"
			],
			"keyword": "sign",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_, Sep 05 2006",
			"references": 13,
			"revision": 40,
			"time": "2019-10-10T11:13:56-04:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}