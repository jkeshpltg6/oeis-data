{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A257537",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 257537,
			"data": "0,1,3,3,6,6,7,7,10,10,10,12,12,12,15,15,12,19,15,18,18,15,19,24,21,19,29,22,18,27,15,31,21,18,25,37,24,24,27,34,19,33,22,25,40,29,27,48,30,37,25,33,31,56,28,42,34,27,18,51,37,21,49,63,36,36,24,30,40,45,34,73,33,37,54,42,33,48,25",
			"name": "Number of subtrees with at least one edge of the rooted tree with Matula-Goebel number n.",
			"comment": [
				"The Matula number of a rooted tree can be defined in the following recursive manner: to the one-vertex tree there corresponds the number 1; to a tree T with root degree 1 there corresponds the t-th prime number, where t is the Matula  number of the tree obtained from T by deleting the edge emanating from the root; to a tree T with root degree m\u003e=2 there corresponds the product of the Matula numbers of the m branches of T.",
				"a(n) = A184161(n) - A061775(n).",
				"The subtree polynomial of a tree T is the bivariate generating polynomial of the subtrees of T with at least one edge with respect to the number of edges (marked by x) and number of pendant vertices (marked by y). See the Martin et. al, reference. For example, the subtree polynomial of the tree \\/ is 2xy^2 + x^2 y^2.",
				"If G(n;x,y) is the subtree polynomial of the rooted tree with Matula number n, then, obviously, a(n) = G(n;1,1). The Maple program uses this circuitous way for finding a(n). With the given Maple program, the command G(n) yields the subtree polynomial of the rooted tree having Matula number n (this is my \"secret\" reason to include this sequence)."
			],
			"link": [
				"E. Deutsch, \u003ca href=\"http://arxiv.org/abs/1111.4288\"\u003eRooted tree statistics from Matula numbers\u003c/a\u003e, arXiv:1111.4288 [math.CO], 2011.",
				"E. Deutsch, \u003ca href=\"http://dx.doi.org/10.1016/j.dam.2012.05.012\"\u003eRooted tree statistics from Matula numbers\u003c/a\u003e, Discrete Appl. Math., 160, 2012, 2314-2322.",
				"F. Goebel, \u003ca href=\"http://dx.doi.org/10.1016/0095-8956(80)90049-0\"\u003eOn a 1-1-correspondence between rooted trees and natural numbers\u003c/a\u003e, J. Combin. Theory, B 29 (1980), 141-143.",
				"I. Gutman and A. Ivic, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(95)00182-V\"\u003eOn Matula numbers\u003c/a\u003e, Discrete Math., 150, 1996, 131-142.",
				"I. Gutman and Yeong-Nan Yeh, \u003ca href=\"http://www.emis.de/journals/PIMB/067/3.html\"\u003eDeducing properties of trees from their Matula numbers\u003c/a\u003e, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"D. W. Matula, \u003ca href=\"http://www.jstor.org/stable/2027327\"\u003eA natural rooted tree enumeration by prime factorization\u003c/a\u003e, SIAM Rev. 10 (1968) 273.",
				"\u003ca href=\"/index/Mat#matula\"\u003eIndex entries for sequences related to Matula-Goebel numbers\u003c/a\u003e"
			],
			"example": [
				"a((4)=3 because the rooted tree with Matula number 4 is  \\/ with subtrees \\ ,  / , and  \\/ ."
			],
			"maple": [
				"with(numtheory): g := proc (n) local r, s: r := proc (n) options operator, arrow: op(1, factorset(n)) end proc: s := proc (n) options operator, arrow: n/r(n) end proc; if n = 1 then 0 elif bigomega(n) = 1 then expand(x*y^2+x*g(pi(n))+x*y*h(pi(n))) else expand(g(r(n))+g(s(n))) end if end proc: h := proc (n) local r, s: r := proc (n) options operator, arrow: op(1, factorset(n)) end proc; s := proc (n) options operator, arrow; n/r(n) end proc: if n = 1 then 0 elif bigomega(n) = 1 then 0 else expand(h(r(n))+h(s(n))+g(r(n))*g(s(n))/y^2+g(r(n))*h(s(n))/y+h(r(n))*g(s(n))/y+h(r(n))*h(s(n))) end if end proc: G := proc (n) local r, s: r := proc (n) options operator, arrow; op(1, factorset(n)) end proc: s := proc (n) options operator, arrow; n/r(n) end proc: if n = 1 then 0 elif bigomega(n) = 1 then g(n)+G(pi(n)) else expand(G(r(n))+G(s(n))+h(n)-h(r(n))-h(s(n))) end if end proc: seq(subs({x = 1, y = 1}, G(i)), i = 1 .. 150);"
			],
			"xref": [
				"Cf. A184161, A061775"
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Emeric Deutsch_, Apr 28 2015",
			"references": 0,
			"revision": 16,
			"time": "2017-03-07T06:20:25-05:00",
			"created": "2015-04-29T06:37:36-04:00"
		}
	]
}