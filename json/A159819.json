{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A159819",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 159819,
			"data": "1,1,-2,0,1,-4,-2,-2,2,4,0,8,-1,1,6,-8,-4,0,6,-2,-6,-4,-2,0,-7,2,-2,8,4,-4,-2,0,4,4,8,-8,10,-1,0,8,1,4,-4,6,-6,0,-8,-8,2,-4,-18,-16,0,12,-2,6,18,-16,-2,0,5,-6,12,8,-4,4,0,-2,-6,12,0,8,-12",
			"name": "Coefficients of L-series for elliptic curve \"48a4\": y^2 = x^3 + x^2 + x.",
			"comment": [
				"Number 54 of the 74 eta-quotients listed in Table I of Martin (1996).",
				"Table I of Martin (1996) for this q-series has exponent of 24 wrong. Number 54 should read 2^(-1)*4^4*6^(-1)*8^(-1)*12^4*24^(-1) (in column g).",
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"The present expansion corresponds in Martin's notation to",
				"  1^(-1)*2^4*3^(-1)*4^(-1)*6^4*12^(-1). For the expansion of the (corrected) Nr. 54 of Martin's reference see A271231. One finds for the p-defects prime(m) - N(prime(m)) = A271230(m) of the elliptic curve y^2 = x^3 + x^2 + x (mod prime(m)), where N(prime(n)) = A271229(n) is the number of solutions, the modularity pattern A271231(prime(m)) = A271230(m), m \u003e= 1. - _Wolfdieter Lang_, Apr 18 2016"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A159819/b159819.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Y. Martin, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-96-01743-6\"\u003eMultiplicative eta-quotients\u003c/a\u003e, Trans. Amer. Math. Soc. 348 (1996), no. 12, 4825-4856, see page 4852 Table I.",
				"Michael Somos, \u003ca href=\"/A030203/a030203.txt\"\u003eIndex to Yves Martin's list of 74 multiplicative eta-quotients and their A-numbers\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-1/2) * eta(q^2)^4 * eta(q^6)^4 / (eta(q) * eta(q^3) * eta(q^4) * eta(q^12)) in powers of q.",
				"Expansion of f(x) * f(-x^2) * f(x^3) * f(-x^6) in powers of x where f() is a Ramanujan theta function.",
				"Euler transform of period 12 sequence [ 1, -3, 2, -2, 1, -6, 1, -2, 2, -3, 1, -4, ...].",
				"a(n) = b(2*n + 1) where b(n) is multiplicative with b(2^e) = 0^e, b(3^e) = 1, b(p^e) = b(p) * b(p^(e-1)) - p * b(p^(e-2)) otherwise.",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (48 t)) = 48 (t/i)^2 f(t) where q = exp(2 Pi i t).",
				"G.f.: Product_{k\u003e0} (1 - (-x)^k) * (1 - x^(2*k)) * (1 - (-x)^(3*k)) * (1 - x^(6*k)).",
				"a(n) = (-1)^n * A030188(n)."
			],
			"example": [
				"G.f. = 1 + x - 2*x^2 + x^4 - 4*x^5 - 2*x^6 - 2*x^7 + 2*x^8 + 4*x^9 + 8*x^11 - ...",
				"G.f. = q + q^3 - 2*q^5 + q^9 - 4*q^11 - 2*q^13 - 2*q^15 + 2*q^17 + 4*q^19 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ -x] QPochhammer[ x^2] QPochhammer[ -x^3] QPochhammer[ x^6], {x, 0, n}]; (* _Michael Somos_, Mar 31 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if(n\u003c0, 0, ellak( ellinit([0, 1, 0, 1, 0], 1), 2*n + 1))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^4 * eta(x^6 + A)^4 / (eta(x + A) * eta(x^3 + A) * eta(x^4 + A) * eta(x^12 + A)), n))};",
				"(PARI) {a(n) = my(A, p, e, x, y, a0, a1); if( n\u003c0, 0, n = 2*n+1; A = factor(n); prod(k=1, matsize(A)[1], [p, e] = A[k,]; if( p==2, 0, p==3, 1, a0=1; a1 = y = -sum(x=0, p-1, kronecker(x^3 + x^2 + x, p)); for(i=2, e, x = y*a1 - p*a0; a0=a1; a1=x); a1)))};",
				"(PARI) q='q+O('q^220); Vec( (eta(q^2)*eta(q^6))^4 / (eta(q^1)*eta(q^3)*eta(q^4)*eta(q^12) ) ) \\\\ _Joerg Arndt_, Sep 12 2016",
				"(MAGMA) A := Basis( CuspForms( Gamma0(48), 2), 147); A[1] + A[3]; /* _Michael Somos_, Mar 31 2015 */"
			],
			"xref": [
				"Cf. A030188, A271229, A271230, A271231."
			],
			"keyword": "sign",
			"offset": "0,3",
			"author": "_Michael Somos_, Apr 22 2009",
			"references": 5,
			"revision": 27,
			"time": "2021-03-12T22:24:45-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}