{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A244544",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 244544,
			"data": "1,2,3,2,3,2,2,0,3,4,4,2,2,2,0,0,3,4,5,2,4,0,2,0,2,4,4,4,0,2,0,0,3,4,6,0,5,2,2,0,4,4,0,2,2,2,0,0,2,2,7,4,4,2,4,0,0,4,4,2,0,2,0,0,3,4,4,2,6,0,0,0,5,4,4,2,2,0,0,0,4,6,6,2,0,4,2",
			"name": "Expansion of (phi(q) + phi(q^2))^2 / 4 in powers of q where phi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A244544/b244544.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of f(-q^3, -q^5)^4 / psi(-q)^2 in powers of q where phi(), f() are Ramanujan theta functions.",
				"Euler transform of period 8 sequence [ 2, 0, -2, 2, -2, 0, 2, -2, ...].",
				"Moebius transform is period 8 sequence [ 2, 1, 0, 0, 0, -1, -2, 0, ...].",
				"Convolution square of A093709.",
				"a(2*n) = A244540(n). a(8*n + 3) = 2*A033761(n). a(8*n + 5) = 2*A053692(n). a(8*n + 7) = 0."
			],
			"example": [
				"G.f. = 1 + 2*q + 3*q^2 + 2*q^3 + 3*q^4 + 2*q^5 + 2*q^6 + 3*q^8 + 4*q^9 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 1, Boole[n == 0], Sum[ {2, 1, 0, 0, 0, -1, -2, 0}[[ Mod[ d, 8, 1] ]], {d, Divisors @ n}]];",
				"a[ n_] := SeriesCoefficient[ (EllipticTheta[ 3, 0, q] + EllipticTheta[ 3, 0, q^2])^2 / 4, {q, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n==0, sumdiv(n, d, [0, 2, 1, 0, 0, 0, -1, -2][d%8 + 1]))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = sum(k=1, sqrtint(n), 2 * x^k^2, 1 + x * O(x^n)); polcoeff( (A + subst(A, x, x^2))^2 / 4, n))};",
				"(Sage) A = ModularForms( Gamma1(8), 1, prec=33) . basis(); A[0] + 2*A[1] + 3*A[2];",
				"(MAGMA) A := Basis( ModularForms( Gamma1(8), 1), 33); A[1] + 2*A[2] + 3*A[3];"
			],
			"xref": [
				"Cf. A033761, A053692, A093709, A244540."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Michael Somos_, Jun 29 2014",
			"references": 1,
			"revision": 16,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2014-06-29T16:50:10-04:00"
		}
	]
}