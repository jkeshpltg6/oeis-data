{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A258409",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 258409,
			"data": "1,2,1,4,1,6,1,2,1,10,1,12,1,2,1,16,1,18,1,2,1,22,1,4,1,2,1,28,1,30,1,2,1,2,1,36,1,2,1,40,1,42,1,2,1,46,1,6,1,2,1,52,1,2,1,2,1,58,1,60,1,2,1,4,1,66,1,2,1,70,1,72,1,2,1,2,1,78,1",
			"name": "Greatest common divisor of all (d-1)'s, where the d's are the positive divisors of n.",
			"comment": [
				"a(n) = 1 for even n; a(p) = p-1 for prime p.",
				"a(n) is even for odd n (since all divisors of n are odd).",
				"It appears that a(n) = A052409(A005179(n)), i.e., it is the largest integer power of the smallest number with exactly n divisors. - _Michel Marcus_, Nov 10 2015",
				"Conjecture: GCD of all (p-1) for prime p|n. - _Thomas Ordowski_, Sep 14 2016",
				"Conjecture is true, because the set of numbers == 1 (mod g) is closed under multiplication. - _Robert Israel_, Sep 14 2016",
				"Conjecture: a(n) = A289508(A328023(n)) = GCD of the differences between consecutive divisors of n. See A328163 and A328164. - _Gus Wiseman_, Oct 16 2019"
			],
			"link": [
				"Ivan Neretin, \u003ca href=\"/A258409/b258409.txt\"\u003eTable of n, a(n) for n = 2..10000\u003c/a\u003e"
			],
			"example": [
				"65 has divisors 1, 5, 13, and 65, hence a(65) = gcd(1-1,5-1,13-1,65-1) = gcd(0,4,12,64) = 4."
			],
			"maple": [
				"f:= n -\u003e igcd(op(map(`-`,numtheory:-factorset(n),-1))):",
				"map(f, [$2..100]); # _Robert Israel_, Sep 14 2016"
			],
			"mathematica": [
				"Table[GCD @@ (Divisors[n] - 1), {n, 2, 100}]"
			],
			"program": [
				"(PARI) a(n) = my(g=0); fordiv(n, d, g = gcd(g, d-1)); g; \\\\ _Michel Marcus_, May 29 2015",
				"(PARI) a(n) = gcd(apply(x-\u003ex-1, divisors(n))); \\\\ _Michel Marcus_, Nov 10 2015",
				"(PARI) a(n)=if(n%2==0, return(1)); if(n%3==0, return(2)); if(n%5==0 \u0026\u0026 n%4 != 1, return(2)); gcd(apply(p-\u003ep-1, factor(n)[,1])) \\\\ _Charles R Greathouse IV_, Sep 19 2016",
				"(Haskell)",
				"a258409 n = foldl1 gcd $ map (subtract 1) $ tail $ a027750_row' n",
				"-- _Reinhard Zumkeller_, Jun 25 2015"
			],
			"xref": [
				"Cf. A049559, A057237, A060680, A063994, A187730, A027750.",
				"Cf. A084190 (similar but with LCM).",
				"Looking at prime indices instead of divisors gives A328167.",
				"Partitions whose parts minus 1 are relatively prime are A328170.",
				"Cf. A000005, A060681, A060683, A193829, A289508."
			],
			"keyword": "nonn",
			"offset": "2,2",
			"author": "_Ivan Neretin_, May 29 2015",
			"references": 17,
			"revision": 40,
			"time": "2019-10-16T20:22:36-04:00",
			"created": "2015-06-25T04:16:46-04:00"
		}
	]
}