{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003989",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3989,
			"data": "1,1,1,1,2,1,1,1,1,1,1,2,3,2,1,1,1,1,1,1,1,1,2,1,4,1,2,1,1,1,3,1,1,3,1,1,1,2,1,2,5,2,1,2,1,1,1,1,1,1,1,1,1,1,1,1,2,3,4,1,6,1,4,3,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,1,2,1,2,7,2,1,2,1,2,1,1,1,3,1,5,3,1,1,3,5,1,3,1,1,1,2,1",
			"name": "Triangle T from the array A(x, y) = gcd(x,y), for x \u003e= 1, y \u003e= 1, read by antidiagonals.",
			"comment": [
				"For m \u003c n, the maximal number of nonattacking queens that can be placed on the n by m rectangular toroidal chessboard is gcd(m,n), except in the case m=3, n=6.",
				"The determinant of the matrix of the first n rows and columns is A001088(n). [Smith, Mansion] - _Michael Somos_, Jun 25 2012",
				"Imagine a torus having regular polygonal cross-section of m sides. Now, break the torus and twist the free ends, preserving rotational symmetry, then reattach the ends. Let n be the number of faces passed in twisting the torus before reattaching it. For example, if n = m, then the torus has exactly one full twist. Do this for arbitrary m and n (m \u003e 1, n \u003e 0). Now, count the independent, closed paths on the surface of the resulting torus, where a path is \"closed\" if and only if it returns to its starting point after a finite number of times around the surface of the torus. Conjecture: this number is always gcd(m,n). NOTE: This figure constitutes a group with m and n the binary arguments and gcd(m,n) the resulting value. Twisting in the reverse direction is the inverse operation, and breaking \u0026 reattaching in place is the identity operation. - _Jason Richardson-White_, May 06 2013",
				"Regarded as a triangle, table of gcd(n - k +1, k) for 1 \u003c= k \u003c= n. - _Franklin T. Adams-Watters_, Oct 09 2014",
				"The n-th row of the triangle is 1,...,1, if and only if, n + 1 is prime. - _Alexandra Hercilia Pereira Silva_, Oct 03 2020"
			],
			"reference": [
				"R. L. Graham, D. E. Knuth and O. Patashnik, Concrete Mathematics, Addison-Wesley, 2nd ed., 1994, ch. 4.",
				"D. E. Knuth, The Art of Computer Programming, Addison-Wesley, section 4.5.2."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A003989/b003989.txt\"\u003eFirst 100 antidiagonals of array, flattened\u003c/a\u003e",
				"Grant Cairns, \u003ca href=\"https://doi.org/10.37236/1591\"\u003eQueens on Non-square Tori\u003c/a\u003e, El. J. Combinatorics, N6, 2001.",
				"P. Mansion, \u003ca href=\"https://archive.org/stream/messengermathem01glaigoog#page/n94/mode/2up\"\u003eOn an Arithmetical Theorem of Professor Smith's\u003c/a\u003e, Messenger of Mathematics, (1878), pp. 81-82.",
				"Kival Ngaokrajang, \u003ca href=\"/A003989/a003989_1.jpg\"\u003ePattern of GCD(x,y) \u003e 1 for x and y = 1..60\u003c/a\u003e. Non-isolated values larger than 1 (polyomino shapes) are colored.",
				"Marcelo Polezzi, \u003ca href=\"http://www.jstor.org/stable/2974739\"\u003eA Geometrical Method for Finding an Explicit Formula for the Greatest Common Divisor\u003c/a\u003e, The American Mathematical Monthly, Vol. 104, No. 5 (May, 1997), pp. 445-446.",
				"H. J. S. Smith, \u003ca href=\"https://doi.org/10.1112/plms/s1-7.1.208\"\u003eOn the value of a certain arithmetical determinant\u003c/a\u003e, Proc. London Math. Soc. 7 (1875-1876), pp. 208-212.",
				"\u003ca href=\"/index/Lc#lcm\"\u003eIndex entries for sequences related to lcm's\u003c/a\u003e"
			],
			"formula": [
				"Multiplicative in both parameters with a(p^e, m) = gcd(p^e, m). - _David W. Wilson_, Jun 12 2005",
				"T(n, k) = A(n - k + 1, k) = gcd(n - k + 1, k), n \u003e= 1, k = 1..n. See a comment above and the Mathematica program. - _Wolfdieter Lang_, May 12 2018",
				"Dirichlet generating function: Sum_{n\u003e=1} Sum_{k\u003e=1} gcd(n, k)/n^s/k^c = zeta(s)*zeta(c)*zeta(s + c - 1)/zeta(s + c). - _Mats Granvik_, Feb 13 2021"
			],
			"example": [
				"The array A begins:",
				"[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]",
				"[1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2]",
				"[1, 1, 3, 1, 1, 3, 1, 1, 3, 1, 1, 3, 1, 1, 3, 1]",
				"[1, 2, 1, 4, 1, 2, 1, 4, 1, 2, 1, 4, 1, 2, 1, 4]",
				"[1, 1, 1, 1, 5, 1, 1, 1, 1, 5, 1, 1, 1, 1, 5, 1]",
				"[1, 2, 3, 2, 1, 6, 1, 2, 3, 2, 1, 6, 1, 2, 3, 2]",
				"[1, 1, 1, 1, 1, 1, 7, 1, 1, 1, 1, 1, 1, 7, 1, 1]",
				"[1, 2, 1, 4, 1, 2, 1, 8, 1, 2, 1, 4, 1, 2, 1, 8]",
				"[1, 1, 3, 1, 1, 3, 1, 1, 9, 1, 1, 3, 1, 1, 3, 1]",
				"[1, 2, 1, 2, 5, 2, 1, 2, 1, 10, 1, 2, 1, 2, 5, 2]",
				"[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 11, 1, 1, 1, 1, 1]",
				"[1, 2, 3, 4, 1, 6, 1, 4, 3, 2, 1, 12, 1, 2, 3, 4]",
				"...",
				"The triangle T begins:",
				"n\\k 1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 ...",
				"1:  1",
				"2:  1  1",
				"3:  1  2  1",
				"4:  1  1  1  1",
				"5:  1  2  3  2  1",
				"6:  1  1  1  1  1  1",
				"7:  1  2  1  4  1  2  1",
				"8:  1  1  3  1  1  3  1  1",
				"9:  1  2  1  2  5  2  1  2  1",
				"10: 1  1  1  1  1  1  1  1  1  1",
				"11: 1  2  3  4  1  6  1  4  3  2  1",
				"12: 1  1  1  1  1  1  1  1  1  1  1  1",
				"13: 1  2  1  2  1  2  7  2  1  2  1  2  1",
				"14: 1  1  3  1  5  3  1  1  3  5  1  3  1  1",
				"15: 1  2  1  4  1  2  1  8  1  2  1  4  1  2  1",
				"...  - _Wolfdieter Lang_, May 12 2018"
			],
			"maple": [
				"a:=(n,k)-\u003egcd(n-k+1,k): seq(seq(a(n,k),k=1..n),n=1..15); # _Muniru A Asiru_, Aug 26 2018"
			],
			"mathematica": [
				"Table[ GCD[x - y + 1, y], {x, 1, 15}, {y, 1, x}] // Flatten (* _Jean-François Alcover_, Dec 12 2012 *)"
			],
			"program": [
				"(PARI) {A(n, m) = gcd(n, m)}; /* _Michael Somos_, Jun 25 2012 */",
				"(GAP) Flat(List([1..15],n-\u003eList([1..n],k-\u003eGcd(n-k+1,k)))); # _Muniru A Asiru_, Aug 26 2018"
			],
			"xref": [
				"Rows, columns and diagonals: A089128, A109007, A109008, A109009, A109010, A109011, A109012, A109013, A109014, A109015.",
				"A109004 is (0, 0) based.",
				"Cf. A003990, A003991, A050873, A054431, A001088.",
				"Cf. also A091255 for GF(2)[X] polynomial analog.",
				"A(x, y) = A075174(A004198(A075173(x), A075173(y))) = A075176(A004198(A075175(x), A075175(y))).",
				"Antidiagonal sums are in A006579."
			],
			"keyword": "tabl,nonn,easy,nice,mult",
			"offset": "1,5",
			"author": "_Marc LeBrun_",
			"references": 54,
			"revision": 127,
			"time": "2021-06-19T14:40:23-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}