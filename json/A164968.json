{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A164968",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 164968,
			"data": "10007,10009,40009,70001,70003,70009,90001,90007,100003,200003,200009,300007,400009,500009,700001,900001,900007,1000003,1000033,1000037,1000039,1000081,1000099,1000303,1000403,1000409,1000507,1000609,1000907,1001003,1003001",
			"name": "Naughty primes: primes in which the number of zeros is greater than the number of all other digits.",
			"comment": [
				"a(31) = 1003001 is the smallest palindromic naughty prime. - _M. F. Hasler_, Nov 22 2009",
				"This sequence can be considered as irregular table in which row n lists the terms with n digits. The row lengths (number of terms with n digits) are then 0, 0, 0, 0, 8, 9, 296, 275, 7934, 9527, 235729, ... - _M. F. Hasler_, Jul 13 2018"
			],
			"link": [
				"M. F. Hasler and Arkadiusz Wesolowski, \u003ca href=\"/A164968/b164968.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (first 5000 terms from M. F. Hasler).",
				"Chris Caldwell, The Prime Glossary, \u003ca href=\"http://primes.utm.edu/glossary/xpage/NaughtyPrime.html\"\u003eNaughty prime\u003c/a\u003e."
			],
			"example": [
				"a(24) = 1000303 is a naughty prime because the number of zeros is greater than the number of all other digits."
			],
			"maple": [
				"Q[1]:= [seq([i],i=1..9)]:",
				"for d from 2 to 6 do Q[d]:= map(t -\u003e seq([i,op(t)],i=1..9),Q[d-1]) od:",
				"F:= proc(d) local R,dn,s,sp,q,x;",
				"   R:= NULL;",
				"   for dn from 2 to floor((d-1)/2) do",
				"      for s in combinat:-choose([$1..d-2],dn-2) do",
				"        sp:= [0,op(s),d-1];",
				"        for q in Q[dn] do",
				"          x:= add(q[i]*10^sp[i],i=1..dn);",
				"          if isprime(x) then R:= R, x fi;",
				"    od od od;",
				"    sort([R])",
				"end proc:",
				"seq(op(F(d)),d=5..7); # _Robert Israel_, Jul 10 2018"
			],
			"mathematica": [
				"lst = {}; Do[If[PrimeQ[n] \u0026\u0026 Count[IntegerDigits[n], 0] \u003e IntegerLength[n]/2, AppendTo[lst, n]], {n, 10^4 + 1, 3^13, 2}]; lst (* _Arkadiusz Wesolowski_, Sep 18 2011 *)",
				"Select[Prime[Range[100000]],DigitCount[#,10,0]\u003eIntegerLength[#]/2\u0026] (* _Harvey P. Dale_, Jun 09 2015 *)"
			],
			"program": [
				"(PARI) next_A164968(p)={ for( n=#Str(p)\\2+1,oo, my(L=10^(2*n+1)); p=max(10^(2*n-3),p); while( L\u003ep=nextprime(p+1), vecsort(Vecsmall(Str(p)))[n]\u003e48 || return(p));p=0) } \\\\ _M. F. Hasler_, Nov 22 2009, syntax update Jul 10 2018",
				"(PARI) A164968_row(n, a=List(), t=vectorv(n, i, 10^(n-i)))={for(z=2, (n-1)\\2, my(v=vector(z, i, if(i\u003c2, [1, 1], i\u003cz, [2, n-1], [n, n]))); forvec(d=vector(z, i, [1, 9]), bittest(650, d[z])\u0026\u0026 vecsum(d)%3\u0026\u0026 forvec(p=v, isprime(d*vecextract(t, p))\u0026\u0026 listput(a, d*vecextract(t, p)), 2))); Set(a)} \\\\ _M. F. Hasler_, Jul 13 2018"
			],
			"keyword": "base,nonn",
			"offset": "1,1",
			"author": "_G. L. Honaker, Jr._, Sep 02 2009",
			"references": 9,
			"revision": 48,
			"time": "2018-07-15T13:30:20-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}