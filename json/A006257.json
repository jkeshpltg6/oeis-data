{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006257",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6257,
			"id": "M2216",
			"data": "0,1,1,3,1,3,5,7,1,3,5,7,9,11,13,15,1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35,37,39,41,43,45,47,49,51,53,55,57,59,61,63,1,3,5,7,9,11,13,15,17,19,21,23,25,27,29",
			"name": "Josephus problem: a(2*n) = 2*a(n)-1, a(2*n+1) = 2*a(n)+1.",
			"comment": [
				"Write the numbers 1 through n in a circle, start at 1 and cross off every other number until only one number is left.",
				"A version of the children's game \"One potato, two potato, ...\".",
				"a(n)/A062383(n) = (0, 0.1, 0.01, 0.11, 0.001, ...) enumerates all binary fractions in the unit interval [0, 1). - _Fredrik Johansson_, Aug 14 2006",
				"In base 2 n-a(n) is equal to n with all digits reverted (leading zeros not considered). For instance a(43)=23 -\u003e 43 is 101011, 43-23 = 20 is 10100. - _Paolo P. Lava_, Mar 09 2010",
				"Iterating a(n), a(a(n)), ... eventually leads to 2^A000120(n) - 1. - _Franklin T. Adams-Watters_, Apr 09 2010",
				"By inspection, the solution to the Josephus Problem is a sequence of odd numbers (from 1) starting at each power of 2.  This yields a direct closed form expression (see formula below). - _Gregory Pat Scandalis_, Oct 15 2013",
				"Also zero together with a triangle read by rows in which row n lists the first 2^(n-1) odd numbers (see A005408), n \u003e= 1. Row lengths give A011782. Right border gives A000225. Row sums give A000302, n \u003e= 1. See example. - _Omar E. Pol_, Oct 16 2013",
				"For n \u003e 0: a(n) = n + 1 - A080079(n). - _Reinhard Zumkeller_, Apr 14 2014",
				"In binary, a(n) = ROL(n), where ROL = rotate left = remove the leftmost digit and append it to the right. For example, n = 41 = 101001[2] =\u003e a(n) = (0)10011[2] = 19. This also explains FTAW's comment above. - _M. F. Hasler_, Nov 02 2016",
				"In the under-down Australian card deck separation: top card on bottom of a deck of n cards, next card separated on the table, etc., until one card is left. The position a(n), for n \u003e= 1, from top will be the left over card. See, e.g., the Behrends reference, pp. 156-164. For the down-under case see 2*A053645(n), for n \u003e= 3, n not a power of 2. If n \u003e= 2 is a power of 2 the botton card survives. - _Wolfdieter Lang_, Jul 28 2020"
			],
			"reference": [
				"Erhard Behrends, Der mathematische Zauberstab, Rowolth Taschenbuch Verlag, rororo 62902, 4. Auflage, 2019, pp.156-164.[English version: The Math Behind the Magic, AMS, 2019].",
				"R. L. Graham, D. E. Knuth and O. Patashnik, Concrete Mathematics. Addison-Wesley, Reading, MA, 1990, p. 10.",
				"M. S. Petković, \"Josephus problem\", Famous Puzzles of Great Mathematicians, page 179, Amer. Math. Soc.(AMS), 2009.",
				"Michel Rigo, Formal Languages, Automata and Numeration Systems, 2 vols., Wiley, 2014. Mentions this sequence - see \"List of Sequences\" in Vol. 2.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"P. Weisenhorn, Josephus und seine Folgen, MNU, 59(2006), pp. 18-19."
			],
			"link": [
				"Iain Fox, \u003ca href=\"/A006257/b006257.txt\"\u003eTable of n, a(n) for n = 0..100000\u003c/a\u003e (terms 0..1000 from T. D. Noe, terms 1001..10000 from Indranil Ghosh).",
				"J.-P. Allouche and J. Shallit, \u003ca href=\"http://dx.doi.org/10.1016/0304-3975(92)90001-V\"\u003eThe ring of k-regular sequences\u003c/a\u003e, Theoretical Computer Sci., 98 (1992), 163-197, ex. 34.",
				"J.-P. Allouche and J. Shallit, \u003ca href=\"http://dx.doi.org/10.1016/S0304-3975(03)00090-2\"\u003eThe ring of k-regular sequences, II\u003c/a\u003e, Theoret. Computer Sci., 307 (2003), 3-29.",
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/2107.00442\"\u003eConjectures and results on some generalized Rueppel sequences\u003c/a\u003e, arXiv:2107.00442 [math.CO], 2021.",
				"Daniel Erman and Brady Haran, \u003ca href=\"https://www.youtube.com/watch?v=uCsD3ZGzMgE\"\u003eThe Josephus Problem\u003c/a\u003e, Numberphile video (2016)",
				"Chris Groër, \u003ca href=\"http://www.jstor.org/stable/3647800\"\u003eThe Mathematics of Survival: From Antiquity to the Playground\u003c/a\u003e, Amer. Math. Monthly, 110 (No. 9, 2003), 812-825.",
				"Alasdair MacFhraing, \u003ca href=\"https://www.jstor.org/stable/20488493\"\u003eAireamh Muinntir Fhinn Is Dhubhain, Agus Sgeul Josephuis Is An Da Fhichead Iudhaich\u003c/a\u003e, [Gaelic with English summary], Proc. Royal Irish Acad., Vol. LII, Sect. A., No. 7, 1948, 87-93.",
				"Yuri Nikolayevsky and Ioannis Tsartsaflis, \u003ca href=\"http://arxiv.org/abs/1512.07676\"\u003eCohomology of N-graded Lie algebras of maximal class over Z_2\u003c/a\u003e, arXiv:1512.87676 [math.RA], (2016), pages 2, 6.",
				"Ralf Stephan, \u003ca href=\"/somedcgf.html\"\u003eSome divide-and-conquer sequences ...\u003c/a\u003e",
				"Ralf Stephan, \u003ca href=\"/A079944/a079944.ps\"\u003eTable of generating functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/JosephusProblem.html\"\u003eJosephus Problem\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Josephus_problem\"\u003eJosephus problem\u003c/a\u003e",
				"\u003ca href=\"/index/J#Josephus\"\u003eIndex entries for sequences related to the Josephus Problem\u003c/a\u003e"
			],
			"formula": [
				"To get a(n), write n in binary, rotate left 1 place.",
				"a(n) = 2*A053645(n) + 1 = 2(n-msb(n))+1. - _Marc LeBrun_, Jul 11 2001. Here \"msb\" = \"most significant bit\", A053644.",
				"G.f.: 1 + 2/(1-x) * ((3*x-1)/(2-2*x) - Sum_{k\u003e=1} 2^(k-1)*x^2^k). - _Ralf Stephan_, Apr 18 2003",
				"a(n) = number of positive integers k \u003c n such that n XOR k \u003c n. a(n) = n - A035327(n). - _Paul D. Hanna_, Jan 21 2006",
				"a(n) = n for n = 2^k - 1. - _Zak Seidov_, Dec 14 2006",
				"a(n) = n - A035327(n). - _K. Spage_, Oct 22 2009",
				"a(2^m+k) = 1+2*k; with 0 \u003c= m and 0 \u003c= k \u003c 2^m; n = 2^m+k; m = floor(log_2(n)); k = n-2^m; a(n) = ((a(n-1)+1) mod n) + 1; a(1) = 1. E.g. n=27; m=4; k=11; a(27) = 1 + 2*11 = 23. - _Paul Weisenhorn_, Oct 10 2010",
				"a(n) = 2*(n - 2^floor(log_2(n))) + 1 (see comment above). - _Gregory Pat Scandalis_, Oct 15 2013",
				"a(n) = 0 if n = 0 and a(n) = 2*a(floor(n/2)) - (-1)^(n mod 2) if n \u003e 0. - _Marek A. Suchenek_, Mar 31 2016",
				"G.f. A(x) satisfies: A(x) = 2*A(x^2)*(1 + x) + x/(1 + x). - _Ilya Gutkovskiy_, Aug 31 2019",
				"For n \u003e 0: a(n) = 2 * A062050(n) - 1. - _Frank Hollstein_, Oct 25 2021"
			],
			"example": [
				"From _Omar E. Pol_, Jun 09 2009: (Start)",
				"Written as an irregular triangle the sequence begins:",
				"  0;",
				"  1;",
				"  1,3;",
				"  1,3,5,7;",
				"  1,3,5,7,9,11,13,15;",
				"  1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31;",
				"  1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35,37,39,41,",
				"   43,45,47,49,51,53,55,57,59,61,63;",
				"...",
				"(End)",
				"From _Omar E. Pol_, Nov 03 2018: (Start)",
				"An illustration of initial terms, where a(n) is the area (or number of cells) in the n-th region of the structure:",
				"   n   a(n)       Diagram",
				"   0    0     _",
				"   1    1    |_|_ _",
				"   2    1      |_| |",
				"   3    3      |_ _|_ _ _ _",
				"   4    1          |_| | | |",
				"   5    3          |_ _| | |",
				"   6    5          |_ _ _| |",
				"   7    7          |_ _ _ _|",
				"(End)"
			],
			"maple": [
				"a(0):=0: for n from 1 to 100 do a(n):=(a(n-1)+1) mod n +1: end do:",
				"seq(a(i),i=0..100); # _Paul Weisenhorn_, Oct 10 2010; corrected by _Robert Israel_, Jan 13 2016",
				"A006257 := proc(n)",
				"    convert(n,base,2) ;",
				"    ListTools[Rotate](%,-1) ;",
				"    add( op(i,%)*2^(i-1),i=1..nops(%)) ;",
				"end proc: # _R. J. Mathar_, May 20 2016",
				"A006257 := n -\u003e 2*n  - Bits:-Iff(n, n):",
				"seq(A006257(n), n=0..78); # _Peter Luschny_, Sep 24 2019"
			],
			"mathematica": [
				"Table[ FromDigits[ RotateLeft[ IntegerDigits[n, 2]], 2], {n, 0, 80}] (* _Robert G. Wilson v_ *)",
				"Flatten@Table[Range[1, 2^n - 1, 2], {n, 0, 5}] (* _Birkas Gyorgy_ *)",
				"m = 5; Range[2^m - 1] + 1 - Flatten@Table[Reverse@Range[2^n], {n, 0, m - 1}] (* _Birkas Gyorgy_ *)"
			],
			"program": [
				"(PARI) a(n)=sum(k=1,n,if(bitxor(n,k)\u003cn,1,0)) \\\\ _Paul D. Hanna_",
				"(PARI) a(n)=if(n, 2*n-2^logint(2*n,2)+1, 0) \\\\ _Charles R Greathouse IV_, Oct 29 2016",
				"(Haskell)",
				"a006257 n = a006257_list !! n",
				"a006257_list =",
				"   0 : 1 : (map (+ 1) $ zipWith mod (map (+ 1) $ tail a006257_list) [2..])",
				"-- _Reinhard Zumkeller_, Oct 06 2011",
				"(MAGMA) [0] cat [2*(n-2^Floor(Log(2,n)))+1: n in [1..100]]; // _Vincenzo Librandi_, Jan 14 2016",
				"(Python)",
				"import math",
				"def A006257(n):",
				"     return 0 if n==0 else 2*(n-2**int(math.log(n,2)))+1 # _Indranil Ghosh_, Jan 11 2017",
				"(C#)",
				"static long cs_A006257(this long n) =\u003e n == 0 ? 0 : 1 + (1 + (n - 1).cs_A006257()) % n; // _Frank Hollstein_, Feb 24 2021"
			],
			"xref": [
				"Cf. A005428, A035327, A038572, A049940, A049964, A053644, A053645, A088147, A088442.",
				"Second column, and main diagonal, of triangle A032434.",
				"Cf. A181281 (with s=5), A054995 (with s=3)."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Robert G. Wilson v_, Sep 21 2003"
			],
			"references": 94,
			"revision": 206,
			"time": "2021-10-28T02:52:15-04:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}