{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A242531",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 242531,
			"data": "0,1,1,1,1,4,3,9,26,82,46,397,283,1675,9938,19503,10247,97978,70478,529383,3171795,7642285,3824927,48091810,116017829,448707198,1709474581,6445720883,3009267707,51831264296",
			"name": "Number of cyclic arrangements of S={1,2,...,n} such that the difference of any two neighbors is a divisor of their sum.",
			"comment": [
				"a(n)=NPC(n;S;P) is the count of all neighbor-property cycles for a specific set S of n elements and a specific pair-property P. For more details, see the link and A242519."
			],
			"link": [
				"S. Sykora, \u003ca href=\"http://dx.doi.org/10.3247/SL5Math14.002\"\u003eOn Neighbor-Property Cycles\u003c/a\u003e, \u003ca href=\"http://ebyte.it/library/Library.html#math\"\u003eStan's Library\u003c/a\u003e, Volume V, 2014."
			],
			"example": [
				"The only such cycle of length n=5 is {1,2,4,5,3}.",
				"For n=7 there are three solutions: C_1={1,2,4,5,7,6,3}, C_2={1,2,4,6,7,5,3}, C_3={1,2,6,7,5,4,3}."
			],
			"mathematica": [
				"A242531[n_] := Count[Map[lpf, Map[j1f, Permutations[Range[2, n]]]], 0]/2;",
				"j1f[x_] := Join[{1}, x, {1}];",
				"dvf[x_] := Module[{i},",
				"   Table[Divisible[x[[i]] + x[[i + 1]], x[[i]] - x[[i + 1]]], {i,",
				"     Length[x] - 1}]];",
				"lpf[x_] := Length[Select[dvf[x], ! # \u0026]];",
				"Join[{0, 1}, Table[A242531[n], {n, 3, 10}]]",
				"(* OR, a less simple, but more efficient implementation. *)",
				"A242531[n_, perm_, remain_] := Module[{opt, lr, i, new},",
				"   If[remain == {},",
				"     If[Divisible[First[perm] + Last[perm],",
				"       First[perm] - Last[perm]], ct++];",
				"     Return[ct],",
				"     opt = remain; lr = Length[remain];",
				"     For[i = 1, i \u003c= lr, i++,",
				"      new = First[opt]; opt = Rest[opt];",
				"      If[! Divisible[Last[perm] + new, Last[perm] - new], Continue[]];",
				"      A242531[n, Join[perm, {new}],",
				"       Complement[Range[2, n], perm, {new}]];",
				"      ];",
				"     Return[ct];",
				"     ];",
				"   ];",
				"Join[{0, 1}, Table[ct = 0; A242531[n, {1}, Range[2, n]]/2, {n, 3, 13}]] (* _Robert Price_, Oct 25 2018 *)"
			],
			"program": [
				"(C++) See the link."
			],
			"xref": [
				"Cf. A242519, A242520, A242521, A242522, A242523, A242524, A242525, A242526, A242527, A242528, A242529, A242530, A242532, A242533, A242534."
			],
			"keyword": "nonn,hard,more",
			"offset": "1,6",
			"author": "_Stanislav Sykora_, May 30 2014",
			"ext": [
				"a(24)-a(28) from _Fausto A. C. Cariboni_, May 25 2017",
				"a(29) from _Fausto A. C. Cariboni_, Jul 09 2020",
				"a(30) from _Fausto A. C. Cariboni_, Jul 14 2020"
			],
			"references": 16,
			"revision": 21,
			"time": "2020-07-14T20:30:17-04:00",
			"created": "2014-05-30T12:47:39-04:00"
		}
	]
}