{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A095133",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 95133,
			"data": "1,1,1,1,1,1,2,2,1,1,3,3,2,1,1,6,6,4,2,1,1,11,11,7,4,2,1,1,23,23,14,8,4,2,1,1,47,46,29,15,8,4,2,1,1,106,99,60,32,16,8,4,2,1,1,235,216,128,66,33,16,8,4,2,1,1,551,488,284,143,69,34,16,8,4,2,1,1,1301,1121,636,315,149,70,34,16,8,4,2,1,1",
			"name": "Triangle of numbers of forests on n nodes containing k trees.",
			"comment": [
				"Row sums are A005195.",
				"For k \u003e n/2, T(n,k) = T(n-1,k-1). - _Geoffrey Critzer_, Oct 13 2012"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A095133/b095133.txt\"\u003eRows n = 1..141, flattened\u003c/a\u003e",
				"Peter Steinbach, \u003ca href=\"/A000055/a000055_12.pdf\"\u003eField Guide to Simple Graphs, Volume 3\u003c/a\u003e, Part 12 (For Volumes 1, 2, 3, 4 of this book see A000088, A008406, A000055, A000664, respectively.)",
				"Peter Steinbach, \u003ca href=\"/A000664/a000664_6.pdf\"\u003eField Guide to Simple Graphs, Volume 4\u003c/a\u003e, Part 6 (For Volumes 1, 2, 3, 4 of this book see A000088, A008406, A000055, A000664, respectively.)",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Forest.html\"\u003eForest\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = sum over the partitions of n, 1M1 + 2M2 + ... + nMn, with exactly k parts, of Product_{i=1..n} binomial(A000055(i) + Mi - 1, Mi). - _Washington Bomfim_, May 12 2005"
			],
			"example": [
				"Triangle begins:",
				"    1;",
				"    1,  1;",
				"    1,  1,  1;",
				"    2,  2,  1,  1;",
				"    3,  3,  2,  1,  1;",
				"    6,  6,  4,  2,  1, 1;",
				"   11, 11,  7,  4,  2, 1, 1;",
				"   23, 23, 14,  8,  4, 2, 1, 1;",
				"   47, 46, 29, 15,  8, 4, 2, 1, 1;",
				"  106, 99, 60, 32, 16, 8, 4, 2, 1, 1;",
				"  ..."
			],
			"maple": [
				"with(numtheory):",
				"b:= proc(n) option remember; local d, j; `if` (n\u003c=1, n,",
				"      (add(add(d*b(d), d=divisors(j)) *b(n-j), j=1..n-1))/(n-1))",
				"    end:",
				"t:= proc(n) option remember; local k; `if` (n=0, 1,",
				"      b(n)-(add(b(k)*b(n-k), k=0..n)-`if`(irem(n, 2)=0, b(n/2), 0))/2)",
				"    end:",
				"g:= proc(n, i, p) option remember; `if`(p\u003en, 0, `if`(n=0, 1,",
				"      `if`(min(i, p)\u003c1, 0, add(g(n-i*j, i-1, p-j) *",
				"       binomial(t(i)+j-1, j), j=0..min(n/i, p)))))",
				"    end:",
				"a:= (n, k)-\u003e g(n, n, k):",
				"seq(seq(a(n, k), k=1..n), n=1..14);  # _Alois P. Heinz_, Aug 20 2012"
			],
			"mathematica": [
				"nn=30;s[n_,k_]:=s[n,k]=a[n+1-k]+If[n\u003c2k,0,s[n-k,k]];a[1]=1;a[n_]:=a[n]=Sum[a[i]s[n-1,i]i,{i,1,n-1}]/(n-1);ft=Table[a[i]-Sum[a[j]a[i-j],{j,1,i/2}]+If[OddQ[i],0,a[i/2](a[i/2]+1)/2],{i,1,nn}];CoefficientList[Series[Product[1/(1-y x^i)^ft[[i]],{i,1,nn}],{x,0,20}],{x,y}]//Grid (* _Geoffrey Critzer_, Oct 13 2012, after code given by _Robert A. Russell_ in A000055 *)"
			],
			"xref": [
				"Cf. A005195 (row sums), A005196, A106240, A000055 (first column), A274937 (2nd column), A105821.",
				"Limiting sequence of reversed rows gives A215930.",
				"Reflected table is A136605. - _Alois P. Heinz_, Apr 11 2014"
			],
			"keyword": "nonn,tabl",
			"offset": "1,7",
			"author": "_Eric W. Weisstein_, May 29 2004",
			"ext": [
				"More terms from _Vladeta Jovovic_, Jun 03 2004"
			],
			"references": 11,
			"revision": 48,
			"time": "2017-10-02T02:11:37-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}