{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003688",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3688,
			"data": "1,4,13,43,142,469,1549,5116,16897,55807,184318,608761,2010601,6640564,21932293,72437443,239244622,790171309,2609758549,8619446956,28468099417,94023745207,310539335038,1025641750321,3387464586001,11188035508324,36951571110973",
			"name": "a(n) = 3*a(n-1) + a(n-2), with a(1)=1 and a(2)=4.",
			"comment": [
				"Number of 2-factors in K_3 X P_n.",
				"Form the graph with matrix [1,1,1,1;1,1,1,0;1,1,0,1;1,0,1,1]. The sequence 1,1,4,13... with g.f. (1-2*x)/(1-3*x-x^2) counts closed walks of length n at the vertex of degree 5. - _Paul Barry_, Oct 02 2004",
				"a(n) is term (1,1) in M^n, where M is the 3x3 matrix [1,1,2; 1,1,1; 1,1,1]. - _Gary W. Adamson_, Mar 12 2009",
				"Starting with 1, INVERT transform of A003945: (1, 3, 6, 12, 24,...). - _Gary W. Adamson_, Aug 05 2010",
				"Row sums of triangle",
				"m/k.|..0.....1.....2.....3.....4.....5.....6.....7",
				"==================================================",
				".0..|..1",
				".1..|..1.....3",
				".2..|..1.....3.....9",
				".3..|..1.....6.....9.....27",
				".4..|..1.....6....27.....27...81",
				".5..|..1.....9....27....108...81...243",
				".6..|..1.....9....54....108..405...243...729",
				".7..|..1....12....54....270..405..1458...729..2187",
				"which is triangle for numbers 3^k*C(m,k) with duplicated diagonals. - _Vladimir Shevelev_, Apr 12 2012",
				"Pisano period lengths:  1, 3, 1, 6, 12, 3, 16, 12, 6, 12, 8, 6, 52, 48, 12, 24, 16, 6, 40, 12,... - _R. J. Mathar_, Aug 10 2012",
				"a(n-1) is the number of length-n strings of 4 letters {0,1,2,3} with no two adjacent nonzero letters identical. The general case (strings of L letters) is the sequence with g.f. (1+x)/(1-(L-1)*x-x^2). - _Joerg Arndt_, Oct 11 2012"
			],
			"reference": [
				"F. Faase, On the number of specific spanning subgraphs of the graphs G X P_n, Ars Combin. 49 (1998), 129-154."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A003688/b003688.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Joerg Arndt, \u003ca href=\"http://www.jjj.de/fxt/#fxtbook\"\u003eMatters Computational (The Fxtbook)\u003c/a\u003e",
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/1803.06408\"\u003eThree Études on a sequence transformation pipeline\u003c/a\u003e, arXiv:1803.06408 [math.CO], 2018.",
				"C. Bautista-Ramos and C. Guillen-Galvan, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL15/Bautista/bautista4.html\"\u003eFibonacci numbers of generalized Zykov sums\u003c/a\u003e, J. Integer Seq., 15 (2012), Article 12.7.8.",
				"F. Faase, \u003ca href=\"http://www.iwriteiam.nl/Cpaper.zip\"\u003eOn the number of specific spanning subgraphs of the graphs G X P_n\u003c/a\u003e, Preliminary version of paper that appeared in Ars Combin. 49 (1998), 129-154.",
				"F. Faase, \u003ca href=\"http://www.iwriteiam.nl/counting.html\"\u003eCounting Hamiltonian cycles in product graphs\u003c/a\u003e",
				"F. Faase, \u003ca href=\"http://www.iwriteiam.nl/Cresults.html\"\u003eResults from the counting program\u003c/a\u003e",
				"Sergio Falcón and Ángel Plaza, \u003ca href=\"http://dx.doi.org/10.1016/j.chaos.2006.09.022\"\u003eOn the Fibonacci k-numbers\u003c/a\u003e, Chaos, Solitons \u0026 Fractals 2007; 32(5): 1615-24.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=419\"\u003eEncyclopedia of Combinatorial Structures 419\u003c/a\u003e",
				"M. Janjic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL18/Janjic/janjic63.html\"\u003eOn Linear Recurrence Equations Arising from Compositions of Positive Integers\u003c/a\u003e, Journal of Integer Sequences, Vol. 18 (2015), Article 15.4.7.",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,1)."
			],
			"formula": [
				"a(n) = (1/2-sqrt(13)/26)*(3/2+sqrt(13)/2)^n+(1/2+sqrt(13)/26)*(3/2-sqrt(13)/2)^n. - _Paul Barry_, Oct 02 2004",
				"a(n) = Sum_{k=0..n} 2^k*A055830(n,k). - _Philippe Deléham_, Oct 18 2006",
				"Starting (1, 1, 4, 13, 43, 142, 469,...), row sums (unsigned) of triangle A136159. - _Gary W. Adamson_, Dec 16 2007",
				"G.f.: x*(1+x)/(1-3*x-x^2). - _Philippe Deléham_, Nov 03 2008",
				"a(n) = A006190(n) + A006190(n-1). - _Sergio Falcon_, Nov 26 2009",
				"For n\u003e=2, a(n) = F_n(3)+F_(n+1)(3), where F_n(x) is Fibonacci polynomial (cf. A049310): F_n(x)=sum{i=0,...,floor((n-1)/2)} C(n-i-1,i) x^(n-2*i-1). - _Vladimir Shevelev_, Apr 13 2012",
				"G.f.: G(0)*(1+x)/(2-3*x), where G(k)= 1 + 1/(1 - (x*(13*k-9))/( x*(13*k+4) - 6/G(k+1))); (continued fraction). - _Sergei N. Gladkovskii_, Jun 15 2013",
				"a(n)^2 is the denominator of continued fraction [3,3,...,3, 5, 3,3,...3], which has n-1 3's before, and n-1 3's after, the middle 5. - _Greg Dresden_, Sep 18 2019"
			],
			"example": [
				"G.f. = x + 4*x^2 + 13*x^3 + 43*x^4 + 142*x^5 + 469*x^6 + 1549*x^7 + 5116*x^8 + ..."
			],
			"maple": [
				"with(combinat): a:=n-\u003efibonacci(n,3)-2*fibonacci(n-1,3): seq(a(n), n=2..25); # _Zerinvary Lajos_, Apr 04 2008"
			],
			"mathematica": [
				"a[n_] := (MatrixPower[{{1, 3}, {1, 2}}, n].{{1}, {1}})[[1, 1]]; Table[ a[n], {n, 0, 23}] (* _Robert G. Wilson v_, Jan 13 2005 *)",
				"LinearRecurrence[{3,1},{1,4},30] (* _Harvey P. Dale_, Mar 15 2015 *)"
			],
			"program": [
				"(MAGMA) [ n eq 1 select 1 else n eq 2 select 4 else 3*Self(n-1)+Self(n-2): n in [1..30] ]; // _Vincenzo Librandi_, Aug 19 2011",
				"(PARI) a(n)=([0,1; 1,3]^(n-1)*[1;4])[1,1] \\\\ _Charles R Greathouse IV_, Aug 14 2017"
			],
			"xref": [
				"Partial sums of A052906. Pairwise sums of A006190.",
				"Cf. A136159, A290948, A003945."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Frans J. Faase_",
			"ext": [
				"Formula added by _Olivier Gérard_, Aug 15 1997",
				"Name clarified by _Michel Marcus_, Oct 16 2016"
			],
			"references": 22,
			"revision": 106,
			"time": "2019-09-19T09:05:16-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}