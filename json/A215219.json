{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A215219",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 215219,
			"data": "1,1,2,1,5,16470,1",
			"name": "Number of (indecomposable or decomposable) Type II binary self-dual codes of length 8n with the highest minimal distance.",
			"comment": [
				"It is important to distinguish between \"extremal\" (meaning having the highest possible minimal distance permitted by Gleason's theorem) and \"optimal\" (meaning having the highest minimal distance that can actually be achieved). This sequence enumerates optimal codes. Extremal codes do not exist when n is sufficiently large. For lengths up to at least 64 \"extremal\" and \"optimal\" coincide.",
				"\"There are 94343 inequivalent doubly even self-dual codes of length 40, 16470 of which are extremal.\" [Betsumiya et al.] - _Jonathan Vos Post_, Aug 06 2012"
			],
			"link": [
				"Koichi Betsumiya, Masaaki Harada and Akihiro Munemasa, \u003ca href=\"http://arxiv.org/abs/1104.3727\"\u003eA Complete Classification of Doubly Even Self-Dual Codes of Length 40\u003c/a\u003e, arXiv:1104.3727v3 [math.CO], v3, Aug 02, 2012. - From _Jonathan Vos Post_, Aug 06 2012",
				"J. H. Conway and V. S. Pless, On the enumeration of self-dual codes, J. Comb. Theory, A28 (1980), 26-53. \u003ca href=\"http://dx.doi.org/10.1016/0097-3165(80)90057-6\"\u003e[DOI]\u003c/a\u003e \u003ca href=\"http://www.ams.org/mathscinet-getitem?mr=558873\"\u003eMR0558873\u003c/a\u003e",
				"J. H. Conway, V. Pless and N. J. A. Sloane, The Binary Self-Dual Codes of Length Up to 32: A Revised Enumeration, J. Comb. Theory, A60 (1992), 183-195 (\u003ca href=\"http://neilsloane.com/doc/pless.txt\"\u003eAbstract\u003c/a\u003e, \u003ca href=\"http://neilsloane.com/doc/pless.pdf\"\u003epdf\u003c/a\u003e, \u003ca href=\"http://neilsloane.com/doc/pless.ps\"\u003eps\u003c/a\u003e, \u003ca href=\"http://neilsloane.com/doc/plesstaba.ps\"\u003eTable A\u003c/a\u003e, \u003ca href=\"http://neilsloane.com/doc/plesstabd.ps\"\u003eTable D\u003c/a\u003e).",
				"S. K. Houghten, C. W. H. Lam, L. H. Thiel and J. A. Parker, \u003ca href=\"http://dx.doi.org/10.1109/TIT.2002.806146\"\u003eThe extended quadratic residue code is the only (48,24,12) self-dual doubly-even code\u003c/a\u003e, IEEE Trans. Inform. Theory, 49 (2003), 53--59.",
				"W. C. Huffman, On the classification and enumeration of self-dual codes, Finite Fields Applic. 11 (2005), 451-490. \u003ca href=\"http://dx.doi.org/10.1016/j.ffa.2005.05.012\"\u003e[DOI]\u003c/a\u003e",
				"G. Nebe, E. M. Rains and N. J. A. Sloane, \u003ca href=\"http://neilsloane.com/doc/cliff2.html\"\u003eSelf-Dual Codes and Invariant Theory\u003c/a\u003e, Springer, Berlin, 2006.",
				"V. S. Pless, The children of the (32,16) doubly even codes, IEEE Trans. Inform. Theory, 24 (1978), 738-746. \u003ca href=\"http://dx.doi.org/10.1109/TIT.1978.1055966\"\u003e[DOI]\u003c/a\u003e \u003ca href=\"http://www.ams.org/mathscinet-getitem?mr=514353\"\u003eMR0514353\u003c/a\u003e",
				"E. M. Rains and N. J. A. Sloane, Self-dual codes, pp. 177-294 of Handbook of Coding Theory, Elsevier, 1998 (\u003ca href=\"http://neilsloane.com/doc/self.txt\"\u003eAbstract\u003c/a\u003e, \u003ca href=\"http://neilsloane.com/doc/self.pdf\"\u003epdf\u003c/a\u003e, \u003ca href=\"http://neilsloane.com/doc/self.ps\"\u003eps\u003c/a\u003e)."
			],
			"xref": [
				"Cf. A003178, A003179, A106162-A106167."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_, Aug 08 2012",
			"ext": [
				"a(6) = 1 (due to Houghten et al.) from Akihiro Munemasa, Aug 08 2012"
			],
			"references": 1,
			"revision": 36,
			"time": "2020-02-11T09:49:40-05:00",
			"created": "2012-08-08T14:16:12-04:00"
		}
	]
}