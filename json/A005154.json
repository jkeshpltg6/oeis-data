{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005154",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5154,
			"id": "M1992",
			"data": "1,2,10,268,195472,104310534400,29722161121961969778688,2413441860555924454205324333893477339897004032,15913289476042091181119569948276231488639535067163704670852319029791565485433738366445158400",
			"name": "a(0) = 1, a(1) = 2; thereafter a(n) = 3*a(n-1)^2 - 2*a(n-2)^4.",
			"comment": [
				"A lower bound for maximal number of stable matchings (or marriages) possible with 2^n men and 2^n women for suitable preference ordering."
			],
			"reference": [
				"D. Gusfield and R. W. Irving, The Stable Marriage Problem: Structure and Algorithms. MIT Press, 1989, p. 25.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"R. W. Irving and P. Leather, \u003ca href=\"https://doi.org/10.1137/0215048\"\u003eThe complexity of counting stable marriages\u003c/a\u003e, SIAM J. Computing 15 (1986), 655-667. [The sequence is v_n =g(2^n), where g(n) appears on page p. 657.]",
				"Anna R. Karlin, Shayan Oveis Gharan, Robbie Weber, \u003ca href=\"https://arxiv.org/abs/1711.01032\"\u003eA Simply Exponential Upper Bound on the Maximum Number of Stable Matchings\u003c/a\u003e, arXiv:1711.01032 [cs.DM], 2017.",
				"Anna R. Karlin, Shayan Oveis Gharan, Robbie Weber, \u003ca href=\"https://doi.org/10.1145/3188745.3188848\"\u003eA Simply Exponential Upper Bound on the Maximum Number of Stable Matchings\u003c/a\u003e, STOC 2018: Proceedings of the 50th Annual ACM SIGACT Symposium on Theory of ComputingJune 2018 Pages 920-925.",
				"D. E. Knuth, \u003ca href=\"https://www-cs-faculty.stanford.edu/~knuth/ms.html\"\u003eMariages Stables\u003c/a\u003e, Presses Univ. de Montréal, 1976 (gives 10 matchings illustrating a(2)).",
				"J. C. Lagarias, J. H. Spencer and J. P. Vinson, \u003ca href=\"https://doi.org/10.1016/S0012-365X(02)00508-3\"\u003eCounting dyadic equipartitions of the unit square\u003c/a\u003e, Discrete Math. 257 (2002), 481-499.",
				"Clayton Thomas, \u003ca href=\"/A005154/a005154.pdf\"\u003eA recurrence giving a lower bound for stable matchings\u003c/a\u003e (analysis of the asymptotic behavior of a_n, with proof due to Peter Shor)",
				"E. G. Thurber, \u003ca href=\"https://doi.org/10.1016/S0012-365X(01)00194-7\"\u003eConcerning the maximum number of stable matchings in the stable marriage problem\u003c/a\u003e, Discrete Math., 248 (2002), 195-219 (see Eq. (1))."
			],
			"formula": [
				"a(n) ~ r*s^(2^n), where r = (sqrt(3)-1)/2 = 0.366025... and s = 2.28014... . - _Clayton Thomas_, Aug 09 2019",
				"The Karlin, Gharan, Weber upper bound is C^(2^n) for a large C. - _Domotor Palvolgyi_, Feb 09 2020"
			],
			"maple": [
				"A005154 := proc(n) option remember; if n \u003c= 1 then n+1 else 3*A005154(n-1)^2-2*A005154(n-2)^4; fi; end;"
			],
			"mathematica": [
				"RecurrenceTable[{a[0]==1,a[1]==2,a[n]==3a[n-1]^2-2a[n-2]^4},a,{n,8}] (* _Harvey P. Dale_, Mar 19 2012 *)"
			],
			"program": [
				"(MAGMA) I:=[1,2]; [m le 2 select I[m] else 3*Self(m-1)^2-2*Self(m-2)^4: m in [1..9]]; // _Marius A. Burtea_, Aug 09 2019"
			],
			"xref": [
				"Recurrence is similar to A076725."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_.",
			"ext": [
				"Swapped formula and comment. - _N. J. A. Sloane_, Mar 01 2020"
			],
			"references": 2,
			"revision": 43,
			"time": "2020-08-27T10:08:32-04:00",
			"created": "1991-05-16T03:00:00-04:00"
		}
	]
}