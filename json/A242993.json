{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A242993",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 242993,
			"data": "0,2,4,4,11,13,16,16,57,78,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0",
			"name": "Least k such that R = (2^k*Q-Q-1)/(Q+1-2^k) is prime, where Q = A000668(n) is the n-th Mersenne prime, or 0 if no such k exists.",
			"comment": [
				"Kravitz has shown that 2^(k-1)*Q*R is a primitive weird number (cf. A002975) when Q \u003e 2^k =: M+1 and R = (M*Q-1)/(Q-M) = M + (M^2-1)/(Q-M) both are prime. R cannot be an integer unless Q \u003c M(M+1) which yields k \u003e p/2 for Mersenne primes Q = 2^p-1. [Edited by _M. F. Hasler_, Nov 11 2018]",
				"Sequence A242025 lists all primes R obtained in that way. Sequence A242998 gives the number of (k,R) for each Q in A000668. Sequence A242998 lists the primes p which give rise to a solution, with multiplicity, and A243003 lists the corresponding values of k. See the \"main entry\" A242025 for more information. - _M. F. Hasler_, Nov 11 2018"
			],
			"link": [
				"S. Kravitz, A search for large weird numbers. J. Recreational Math. 9(1976), 82-85 (1977). \u003ca href=\"http://zbmath.org/?format=complete\u0026amp;q=an:0365.10003\"\u003eZbl 0365.10003\u003c/a\u003e"
			],
			"example": [
				"For n = 2, Q = A000668(2) = 7, k = 2 yields the prime R = (2^k*Q-Q-1)/(Q+1-2^k) = 20/4 = 5 and the (smallest possible) weird number 2^(k-1)*Q*R = 2*7*5 = 70.",
				"For n = 9, Q = A000668(9) = 2^61-1, k = 57 yields the prime R = 2^57-1 + (2^57-2)/(2^4-1) and the 53-digit primitive weird number 2^56*Q*R = 25541592347764814106588251084767772206406532903993344.",
				"For n = 10, Q = A000668(10) = 2^89-1, k = 78 yields the prime R = 2^78-1 + (2^78-2)/(2^11-1) and the 74-digit primitive weird number 2^77*Q*R = 28283363272427014026275183563912621451964887156507346985599492888375328768."
			],
			"mathematica": [
				"A000043 = {2, 3, 5, 7, 13, 17, 19, 31, 61, 89, 107, 127, 521, 607,",
				"   1279, 2203, 2281, 3217, 4253, 4423, 9689, 9941, 11213, 19937,",
				"   21701, 23209, 44497, 86243, 110503, 132049, 216091, 756839, 859433,",
				"   1257787, 1398269, 2976221, 3021377, 6972593, 13466917, 20996011,",
				"   24036583, 25964951, 30402457, 32582657, 37156667, 42643801,",
				"   43112609};",
				"lst = {};",
				"For[i = 1, i \u003c= 25, i++,",
				"  p = A000043[[i]];",
				"  kc = 0;",
				"  For[k = 1, k \u003c p, k++,",
				"   r = 2^k - 1 + (2^k - 2)/(2^(p - k) - 1);",
				"   If[! IntegerQ[r], Continue[]];",
				"   If[PrimeQ[r], kc = k; Break[]]];",
				"  AppendTo[lst, kc]];",
				"lst (* _Robert Price_, Sep 05 2019 *)"
			],
			"program": [
				"(PARI) a(n)={p=A000043[n]; for(k=p\\2+1,p-1, Mod(2,2^(p-k)-1)^k==2 \u0026\u0026 ispseudoprime(2^k-1+(2^k-2)/(2^(p-k)-1)) \u0026\u0026 return(k))}"
			],
			"xref": [
				"Cf. A242025 (primes R obtained in that way), A242998 (number of such k values for given n), A242999 (p=A000043(n) listed A242998(n) times), A243003 (all values of k).",
				"Cf. A258882 (weird numbers of the form 2^k*p*q)."
			],
			"keyword": "nonn,more,hard",
			"offset": "1,2",
			"author": "_M. F. Hasler_, Aug 17 2014",
			"ext": [
				"Definition corrected by _Jens Kruse Andersen_, Aug 18 2014",
				"a(28)-a(37) from _Robert Price_, Sep 05 2019"
			],
			"references": 5,
			"revision": 35,
			"time": "2019-09-05T19:23:49-04:00",
			"created": "2014-08-18T11:28:51-04:00"
		}
	]
}