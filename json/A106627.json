{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A106627",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 106627,
			"data": "8,1,9,28,105,286,918,2871,8977,27892,87084,271635,847182,2641991,8240325,25700488,80156033,249994997,779700654,2431777739,7584375260",
			"name": "Product L(n)*L_4(n), where L(n) are Lucas numbers and L_4(n) are Lucas 4-step numbers.",
			"comment": [
				"a(n) is semiprime iff n is an element of A001606 (an index of a prime Lucas number) and an element of A104577 (an index of a prime Lucas 4-step number). The only known such are n = 2, 8, 16, 19, 71, (through 145858)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A106627/b106627.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_08\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,4,5,9,3,-2,1,-1)."
			],
			"formula": [
				"a(n) = A000032(n) * A073817(n).",
				"a(n) = +a(n-1) +4*a(n-2) +5*a(n-3) +9*a(n-4) +3*a(n-5) -2*a(n-6) +a(n-7) -a(n-8). - _R. J. Mathar_, Dec 22 2010",
				"G.f.: (8 -7*x -24*x^2 -25*x^3 -36*x^4 -9*x^5 +4*x^6 -x^7) / (1 -x -4*x^2 -5*x^3 -9*x^4 -3*x^5 +2*x^6 -x^7 +x^8). - _Colin Barker_, Jun 17 2012"
			],
			"example": [
				"a(0) = 8 because L(0) * L_4(0) = 2 * 4.",
				"a(1) = 1 because L(1) * L_4(1) = 1 * 1.",
				"a(2) = 9 because L(2) * L_4(2) = 3 * 3.",
				"a(3) = 28 because L(3) * L_4(3) = 4 * 7.",
				"a(4) = 105 because L(4) * L_4(4) = 7 * 15.",
				"a(5) = 286 because L(5) * L_4(5) = 11 * 26.",
				"a(6) = 918 because L(6) * L_4(6) = 18 * 51."
			],
			"mathematica": [
				"LinearRecurrence[{1,4,5,9,3,-2,1,-1}, {8,1,9,28,105,286,918,2871}, 40] (* _G. C. Greubel_, Feb 19 2019 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^40)); Vec((8-7*x-24*x^2-25*x^3-36*x^4-9*x^5+4*x^6 -x^7)/(1-x-4*x^2-5*x^3-9*x^4-3*x^5+2*x^6-x^7+x^8)) \\\\ _G. C. Greubel_, Feb 19 2019",
				"(MAGMA) I:=[8,1,9,28,105,286,918,2871]; [n le 8 select I[n] else Self(n-1)+4*Self(n-2)+5*Self(n-3)+9*Self(n-4)+3*Self(n-5)-2*Self(n-6) + Self(n-7)-Self(n-8): n in [1..30]]; // _G. C. Greubel_, Feb 19 2019",
				"(Sage) ((8-7*x-24*x^2-25*x^3-36*x^4-9*x^5+4*x^6-x^7)/(1-x-4*x^2-5*x^3 -9*x^4-3*x^5+2*x^6-x^7+x^8)).series(x, 40).coefficients(x, sparse=False) # _G. C. Greubel_, Feb 19 2019",
				"(GAP) a:=[8,1,9,28,105,286,918,2871];; for n in [9..30] do a[n]:=a[n-1] +4*a[n-2]+5*a[n-3]+9*a[n-4]+3*a[n-5]-2*a[n-6]+a[n-7]-a[n-8]; od; a; # _G. C. Greubel_, Feb 19 2019"
			],
			"xref": [
				"Cf. A000032, A000040, A001358, A001606, A073446, A073817, A104576."
			],
			"keyword": "easy,nonn",
			"offset": "0,1",
			"author": "_Jonathan Vos Post_, May 11 2005",
			"references": 1,
			"revision": 18,
			"time": "2019-02-19T18:21:51-05:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}