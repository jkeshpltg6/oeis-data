{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A129522",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 129522,
			"data": "1,0,-5,4,-1,0,0,0,16,0,-11,-20,0,0,5,16,0,0,0,-4,0,0,35,0,-24,0,-35,0,0,0,-37,0,55,0,0,64,-25,0,0,0,0,0,0,-44,-16,0,50,-80,49,0,0,0,-70,0,11,0,0,0,107,20,0,0,0,64,0,0,35,0,-175,0,-133,0,0",
			"name": "Expansion of unique weight 3 level 11 multiplicative cusp form in powers of q.",
			"comment": [
				"This is a member of an infinite family of odd weight level 11 multiplicative modular forms. g_1 = A035179, g_3 = A129522, g_5 = A065099, g_7 = A138661."
			],
			"formula": [
				"Expansion of (F(q)^2 + 4*F(q^2)^2 + 8*F(q^4)^2) * F(q)^2 / F(q^2) in powers of q where F(q) := eta(q) * eta(q^11) is the g.f. of A030200.",
				"a(n) is multiplicative with a(11^e) = (-11)^e, a(p^e) = (1+(-1)^e)/2*p^e if p == 2, 6, 7, 8, 10 (mod 11), a(p^e) = a(p)*a(p^(e-1)) - p^2*a(p^(e-2)) if p == 1, 3, 4, 5, 9 (mod 11) where a(p) = y^2 - 2*p and 4*p = y^2 + 11*x^2.",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (11 t)) = 11^(3/2) (t/i)^3 f(t) where q = exp(2 Pi i t).",
				"G.f.: (1/2) * Sum_{u, v in Z} (u*u - 3*v*v) * x^(u*u + u*v + 3*v*v). - _Michael Somos_, Jun 14 2007",
				"Convolution of A006571 and A028609. - _Michael Somos_, Aug 14 2012",
				"a(4*n + 2) = 0. - _Michael Somos_, Nov 11 2015"
			],
			"example": [
				"G.f. = q - 5*q^3 + 4*q^4 - q^5 + 16*q^9 - 11*q^11 - 20*q^12 + 5*q^15 + 16*q^16 + ..."
			],
			"mathematica": [
				"a[ n_] := Module[ {A, B}, B = QPochhammer[ q] QPochhammer[ q^11]; A = B / (q QPochhammer[ q^3] QPochhammer[ q^33]); SeriesCoefficient[ q B^3 (1 + 3 / A) Sqrt[ q (A + 1 + 3 / A)], {q, 0, n}]]; (* _Michael Somos_, Mar 26 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A, B); if( n\u003c1, 0, n--; A = x * O(x^n); B = eta(x + A) * eta(x^11 + A); A = B /( x * eta(x^3 + A) * eta(x^33 + A)); A = B^3 * (1 + 3/A) * sqrt(x * (A + 1 + 3/A)); polcoeff(A, n))};",
				"(PARI) {a(n) = my(A, p, e, x, y, a0, a1); if( n\u003c1, 0, A = factor(n); prod( k=1, matsize(A)[1], [p, e] = A[k, ]; if( p==11, (-11)^e, kronecker( -11, p)==-1, if( e%2, 0, p^e), for( x=1, sqrtint(4*p\\11), if( issquare(4*p - 11*x^2, \u0026y), break)); y = y^2 - p*2; a0=1; a1=y; for( i=2, e, x = y*a1 - p^2*a0; a0=a1; a1=x); a1)))}; /* _Michael Somos_, Jun 06 2007 */",
				"(PARI) {a(n) = my(A); if( n\u003c1, 0, n--; A = x * O(x^n); A = eta(x + A) * eta(x^11 + A); polcoeff( A^2 / subst(A + x * O(x^(n\\2)), x, x^2) * (A^2 + 4*x * subst(A + x * O(x^(n\\2)), x, x^2)^2 + 8 * x^3 * subst(A + x * O(x^(n\\4)), x, x^4)^2), n))}; /* _Michael Somos_, Jun 06 2007 */",
				"(MAGMA) A := Basis( CuspForms( Gamma1(11), 3), 73); A[1] - 5*A[3] + 4*A[4] - A[5]; /* _Michael Somos_, Mar 26 2015 */"
			],
			"xref": [
				"Cf. A006571, A028609, A030200, A035179, A065099, A138661."
			],
			"keyword": "sign,mult",
			"offset": "1,3",
			"author": "_Michael Somos_, Apr 19 2007, Jun 06 2007",
			"references": 4,
			"revision": 24,
			"time": "2015-11-11T20:53:59-05:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}