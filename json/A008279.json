{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008279",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8279,
			"data": "1,1,1,1,2,2,1,3,6,6,1,4,12,24,24,1,5,20,60,120,120,1,6,30,120,360,720,720,1,7,42,210,840,2520,5040,5040,1,8,56,336,1680,6720,20160,40320,40320,1,9,72,504,3024,15120,60480,181440,362880,362880",
			"name": "Triangle T(n,k) = n!/(n-k)! (0 \u003c= k \u003c= n) read by rows, giving number of permutations of n things k at a time.",
			"comment": [
				"Also called permutation coefficients.",
				"Also falling factorials triangle A068424 with column a(n,0)=1 and row a(0,1)=1 otherwise a(0,k)=0, added. - _Wolfdieter Lang_, Nov 07 2003",
				"The higher-order exponential integrals E(x,m,n) are defined in A163931; for information about the asymptotic expansion of E(x,m=1,n) see A130534. The asymptotic expansions for n = 1, 2, 3, 4, ..., lead to the right hand columns of the triangle given above. - _Johannes W. Meijer_, Oct 16 2009",
				"The number of injective functions from a set of size k to a set of size n. - _Dennis P. Walsh_, Feb 10 2011",
				"The number of functions f from {1,2,...,k} to {1,2,...,n} that satisfy f(x) \u003e= x for all x in {1,2,...,k}. - _Dennis P. Walsh_, Apr 20 2011",
				"T(n,k) = A181511(n,k) for k=1..n-1. - _Reinhard Zumkeller_, Nov 18 2012",
				"The e.g.f.s enumerating the faces of the permutohedra / permutahedra, Perm(s,t;x) = [e^(sx)-1]/[s-t(e^(sx)-1)], (cf. A090582 and A019538) and the stellahedra / stellohedra, St(s,t;x) = [s e^((s+t)x)]/[s-t(e^(sx)-1)], (cf. A248727) given in Toric Topology satisfy exp[u*d/dt] St(s,t;x) = St(s,u+t;x) = [e^(ux)/(1-u*Perm(s,t;x))]*St(s,t;x), where e^(ux)/(1-uy) is a bivariate e.g.f. for the row polynomials of this entry and A094587. Equivalently, d/dt St = (x+Perm)*St and d/dt Perm = Perm^2, or d/dt log(St) = x + Perm and d/dt log(Perm) = Perm. - _Tom Copeland_, Nov 14 2016",
				"T(n, k)/n! are the coefficients of the n-th exponential Taylor polynomial, or truncated exponentials, which was proved to be irreducible by Schur. See Coleman link. - _Michel Marcus_, Feb 24 2020"
			],
			"reference": [
				"CRC Standard Mathematical Tables and Formulae, 30th ed., 1996, p. 176; 31st ed., p. 215, Section 3.3.11.1.",
				"Maple V Reference Manual, p. 490, numbperm(n,k)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A008279/b008279.txt\"\u003eRows n = 0..100 of triangle, flattened\u003c/a\u003e",
				"J. Fernando Barbero G., Jesús Salas, Eduardo J. S. Villaseñor, \u003ca href=\"http://arxiv.org/abs/1307.2010\"\u003eBivariate Generating Functions for a Class of Linear Recurrences. I. General Structure\u003c/a\u003e, arXiv:1307.2010 [math.CO], 2013-2014.",
				"V. Buchstaber and T. Panov \u003ca href=\"https://arxiv.org/abs/1210.2368\"\u003eToric Topology\u003c/a\u003e, arXiv:1210.2368v3 [math.AT], 2014.",
				"R. F. Coleman, \u003ca href=\"http://dx.doi.org/10.5169/seals-87891\"\u003eOn the Galois groups of the exponential Taylor polynomials\u003c/a\u003e, L’Enseignement Mathématique 33, 183-189, (1987).",
				"J. Goldman, J. Haglund, \u003ca href=\"http://dx.doi.org/10.1006/jcta.2000.3113\"\u003eGeneralized rook polynomials\u003c/a\u003e, J. Combin. Theory A91 (2000), 509-530, 2-rook coefficients for k rooks on the 1xn board, all heights 1.",
				"R. L. Graham, D. E. Knuth, and O. Patashnik, \u003ca href=\"https://notendur.hi.is/pgg/%28ebook-pdf%29%20-%20Mathematics%20-%20Concrete%20Mathematics.pdf\"\u003eConcrete Mathematics: A Foundation for Computer Science, 2nd ed.\u003c/a\u003e Reading, MA: Addison-Wesley, 1994.",
				"Germain Kreweras, \u003ca href=\"http://www.numdam.org/item?id=MSH_1963__3__31_0\"\u003eUne dualité élémentaire souvent utile dans les problèmes combinatoires\u003c/a\u003e, Mathématiques et Sciences Humaines 3 (1963) 31-41.",
				"W. Lang, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL3/LANG/lang.html\"\u003eOn generalizations of Stirling number triangles\u003c/a\u003e, J. Integer Seqs., Vol. 3 (2000), #00.2.4.",
				"T. S. Motzkin, \u003ca href=\"/A000262/a000262.pdf\"\u003eSorting numbers for cylinders and other classification numbers\u003c/a\u003e, in Combinatorics, Proc. Symp. Pure Math. 19, AMS, 1971, pp. 167-176. [Annotated, scanned copy]",
				"OEIS Wiki, \u003ca href=\"http://oeis.org/wiki/Sorting_numbers\"\u003eSorting numbers\u003c/a\u003e",
				"Yuriy Shablya, Dmitry Kruchinin, \u003ca href=\"http://elibrary.matf.bg.ac.rs/bitstream/handle/123456789/4699/Proceedings_Book_of_MICOPAM2018_11_11_2018.pdf?sequence=3#page=233\"\u003eEuler-Catalan's Number Triangle and its Application\u003c/a\u003e, Proceedings Book of Micropam (The Mediterranean International Conference of Pure \u0026 Applied Mathematics and Related Areas 2018), 212-215.",
				"Dennis Walsh, \u003ca href=\"http://frank.mtsu.edu/~dwalsh/NOLESSFN.pdf\"\u003eNotes on no-less functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/FallingFactorial.html\"\u003eFalling Factorial\u003c/a\u003e",
				"\u003ca href=\"/index/Cor#core\"\u003eIndex entries for \"core\" sequences\u003c/a\u003e"
			],
			"formula": [
				"E.g.f.: Sum T(n,k) x^n/n! y^k = exp(x)/(1-x*y). - _Vladeta Jovovic_, Aug 19 2002",
				"Equals A007318 * A136572. - _Gary W. Adamson_, Jan 07 2008",
				"T(n, k) = n*T(n-1, k-1) = k*T(n-1, k-1)+T(n-1, k) = n*T(n-1, k)/(n-k) = (n-k+1)*T(n, k-1). - _Henry Bottomley_, Mar 29 2001",
				"T(n, k) = n!/(n-k)! if n \u003e= k \u003e= 0, otherwise 0.",
				"G.f. for k-th column k!*x^k/(1-x)^(k+1), k \u003e= 0.",
				"E.g.f. for n-th row (1+x)^n, n \u003e= 0.",
				"Sum T(n, k)x^k = permanent of n X n matrix a_ij = (x+1 if i=j, x otherwise). - _Michael Somos_, Mar 05 2004",
				"Ramanujan psi_1(k, x) polynomials evaluated at n+1. - _Ralf Stephan_, Apr 16 2004",
				"E.g.f.: Sum T(n,k) x^n/n! y^k/k! = e^{x+xy}. - _Franklin T. Adams-Watters_, Feb 07 2006",
				"The triangle is the binomial transform of an infinite matrix with (1, 1, 2, 6, 24, ...) in the main diagonal and the rest zeros. - _Gary W. Adamson_, Nov 20 2006",
				"G.f.: 1/(1-x-xy/(1-xy/(1-x-2xy/(1-2xy/(1-x-3xy/(1-3xy/(1-x-4xy/(1-4xy/(1-... (continued fraction). - _Paul Barry_, Feb 11 2009",
				"T(n,k) = Sum_{j=0..k} binomial(k,j)*T(x,j)*T(y,k-j) for x+y = n. - _Dennis P. Walsh_, Feb 10 2011",
				"E.g.f (with k fixed): x^k*exp(x). - _Dennis P. Walsh_, Apr 20 2011",
				"G.f. (with k fixed): k!*x^k/(1-x)^(k+1). - _Dennis P. Walsh_, Apr 20 2011",
				"For n \u003e= 2 and m \u003e= 2, Sum_{k=0..m-2} S2(n, k+2)*T(m-2, k) = Sum_{p=0..n-2} m^p. S2(n,k) are the Stirling numbers of the second kind A008277. - _Tony Foster III_, Jul 23 2019"
			],
			"example": [
				"Triangle begins:",
				"1",
				"1,  1",
				"1,  2,  2",
				"1,  3,  6,   6",
				"1,  4, 12,  24,   24",
				"1,  5, 20,  60,  120,   120",
				"1,  6, 30, 120,  360,   720,    720",
				"1,  7, 42, 210,  840,  2520,   5040,   5040",
				"1,  8, 56, 336, 1680,  6720,  20160,  40320,   40320",
				"1,  9, 72, 504, 3024, 15120,  60480, 181440,  362880,  362880",
				"1, 10, 90, 720, 5040, 30240, 151200, 604800, 1814400, 3628800, 3628800",
				"For example, T(4,2)=12 since there are 12 injective functions f:{1,2}-\u003e{1,2,3,4}. There are 4 choices for f(1) and then, since f is injective, 3 remaining choices for f(2), giving us 12 ways to construct an injective function. - _Dennis P. Walsh_, Feb 10 2011",
				"For example, T(5,3)=60 since there are 60 functions f:{1,2,3}-\u003e{1,2,3,4,5} with f(x) \u003e= x. There are 5 choices for f(1), 4 choices for f(2), and 3 choices for f(3), giving us 60 ways to construct such a function. - _Dennis P. Walsh_, Apr 30 2011"
			],
			"maple": [
				"with(combstruct): for n from 0 to 10 do seq(count(Permutation(n),size=m), m = 0 .. n) od; # _Zerinvary Lajos_, Dec 16 2007",
				"seq(seq(n!/(n-k)!,k=0..n),n=0..10); # _Dennis P. Walsh_, Apr 20 2011",
				"seq(print(seq(pochhammer(n-k+1,k),k=0..n)),n=0..6); # _Peter Luschny_, Mar 26 2015"
			],
			"mathematica": [
				"Table[CoefficientList[Series[(1 + x)^m, {x, 0, 20}], x]* Table[n!, {n, 0, m}], {m, 0, 10}] // Grid - _Geoffrey Critzer_, Mar 16 2010",
				"Table[ Pochhammer[n - k + 1, k], {n, 0, 9}, {k, 0, n}] // Flatten (* or *)",
				"Table[ FactorialPower[n, k], {n, 0, 9}, {k, 0, n}] // Flatten  (* _Jean-François Alcover_, Jul 18 2013, updated Jan 28 2016 *)"
			],
			"program": [
				"(PARI) {T(n, k) = if( k\u003c0 || k\u003en, 0, n!/(n-k)!)}; /* _Michael Somos_, Nov 14 2002 */",
				"(PARI) {T(n, k) = my(A, p); if( k\u003c0 || k\u003en, 0, if( n==0, 1, A = matrix(n, n, i, j, x + (i==j)); polcoeff( sum(i=1, n!, if( p = numtoperm(n, i), prod(j=1, n, A[j, p[j]]))), k)))}; /* _Michael Somos_, Mar 05 2004 */",
				"(Haskell)",
				"a008279 n k = a008279_tabl !! n !! k",
				"a008279_row n = a008279_tabl !! n",
				"a008279_tabl = iterate f [1] where",
				"   f xs = zipWith (+) ([0] ++ zipWith (*) xs [1..]) (xs ++ [0])",
				"-- _Reinhard Zumkeller_, Dec 15 2013, Nov 18 2012",
				"(Sage)",
				"for n in range(8): [falling_factorial(n,k) for k in (0..n)] # _Peter Luschny_, Mar 26 2015",
				"(MAGMA) / As triangle / [[Factorial(n)/Factorial(n-k): k in [0..n]]: n in [0.. 15]]; // _Vincenzo Librandi_, Oct 11 2015"
			],
			"xref": [
				"Row sums give A000522.",
				"Cf. A001497, A001498, A136572.",
				"T(n,0)=A000012, T(n,1)=A000027, T(n+1,2)=A002378, T(n,3)=A007531, T(n,4)=A052762, and T(n,n)=A000142.",
				"Cf. A019538, A090582, A094587, A248727."
			],
			"keyword": "nonn,tabl,nice,easy,core",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_",
			"references": 113,
			"revision": 117,
			"time": "2020-02-24T10:56:09-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}