{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A304790",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 304790,
			"data": "1,1,2,3,5,8,13,21,36,55,95,149,281,430,781,1211,2245,3456,6728,10092,18061,31529,51378,85659,167089,252748,431819,817991,1292697",
			"name": "The maximal number of different domino tilings allowed by the Ferrers-Young diagram of a single partition of 2n.",
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/FerrersDiagram.html\"\u003eFerrers Diagram\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Domino_(mathematics)\"\u003eDomino\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Domino_tiling\"\u003eDomino tiling\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Ferrers_diagram\"\u003eFerrers diagram\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Mutilated_chessboard_problem\"\u003eMutilated chessboard problem\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Partition_(number_theory)\"\u003ePartition (number theory)\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Young_tableau#Diagrams\"\u003eYoung tableau, Diagrams\u003c/a\u003e",
				"\u003ca href=\"/index/Do#domino\"\u003eIndex entries for sequences related to dominoes\u003c/a\u003e"
			],
			"formula": [
				"a(n) = max { k : A304789(n,k) \u003e 0 }.",
				"a(A001105(n)) = A004003(n).",
				"a(n) = A000045(n+1) for n \u003c 8."
			],
			"example": [
				"a(11) = 149 different domino tilings are possible for 444442 and 6655.",
				"a(18) = 6728 different domino tilings are possible for 666666."
			],
			"maple": [
				"h:= proc(l, f) option remember; local k; if min(l[])\u003e0 then",
				"     `if`(nops(f)=0, 1, h(map(u-\u003e u-1, l[1..f[1]]), subsop(1=[][], f)))",
				"    else for k from nops(l) while l[k]\u003e0 by -1 do od;",
				"        `if`(nops(f)\u003e0 and f[1]\u003e=k, h(subsop(k=2, l), f), 0)+",
				"        `if`(k\u003e1 and l[k-1]=0, h(subsop(k=1, k-1=1, l), f), 0)",
				"      fi",
				"    end:",
				"g:= l-\u003e `if`(add(`if`(l[i]::odd, (-1)^i, 0), i=1..nops(l))=0,",
				"        `if`(l=[], 1, h([0$l[1]], subsop(1=[][], l))), 0):",
				"b:= (n, i, l)-\u003e `if`(n=0 or i=1, g([l[], 1$n]), max(b(n, i-1, l),",
				"                   b(n-i, min(n-i, i), [l[], i]))):",
				"a:= n-\u003e b(2*n$2, []):",
				"seq(a(n), n=0..15);"
			],
			"mathematica": [
				"h[l_, f_] := h[l, f] = Module[{k}, If[Min[l] \u003e 0, If[Length[f] == 0, 1, h[Map[# - 1\u0026, l[[1 ;; f[[1]]]]], ReplacePart[f, 1 -\u003e Nothing]]], For[k = Length[l], l[[k]] \u003e 0 , k--]; If[Length[f] \u003e 0 \u0026\u0026 f[[1]] \u003e= k, h[ReplacePart[l, k -\u003e 2], f], 0] + If[k \u003e 1 \u0026\u0026 l[[k - 1]] == 0, h[ReplacePart[l, {k -\u003e 1, k - 1 -\u003e 1}], f], 0]]];",
				"g[l_] := If[Sum[If[OddQ@l[[i]], (-1)^i, 0], {i, 1, Length[l]}] == 0, If[l == {}, 1, h[Table[0, {l[[1]]}], ReplacePart[l, 1 -\u003e Nothing]]], 0];",
				"b[n_, i_, l_] := If[n == 0 || i == 1, g[Join[l, Table[1, {n}]]], Max[b[n, i - 1, l], b[n - i, Min[n - i, i], Append[l, i]]]];",
				"a[n_] := b[2n, 2n, {}];",
				"Table[a[n], {n, 0, 15}] (* _Jean-François Alcover_, Aug 24 2021, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A000045, A001105, A004003, A304789."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Alois P. Heinz_, May 18 2018",
			"references": 2,
			"revision": 20,
			"time": "2021-08-24T07:01:12-04:00",
			"created": "2018-05-19T15:21:30-04:00"
		}
	]
}