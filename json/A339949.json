{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A339949",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 339949,
			"data": "2,3,5,6,7,3,2,12,4,4,4,4,18,2,3,6,20,5,3,2,30,4,3,4,4,9,2,3,9,4,4,3,4,47,2,3,5,10,6,3,2,15,4,4,4,4,13,2,3,7,8,5,3,2,77,4,3,5,6,8,3,2,10,4,4,3,4,24,2,3,6,78,6,3,2,22,4,3,4,4,11,2",
			"name": "a(n) is the greatest runlength in all n-sections of the infinite Fibonacci word A014675.",
			"comment": [
				"Equivalently a(n) is the greatest runlength in all n-sections of the infinite Fibonacci word A003849.",
				"From _Jeffrey Shallit_, Mar 23 2021: (Start)",
				"We know that the Fibonacci word has exactly n+1 distinct factors of length n.",
				"So to verify a(n) we simply verify there is a monochromatic arithmetic progression of length a(n) and difference n by examining all factors of length (n*a(n) - n + 1) (and we know when we've seen all of them). Next we verify there is no monochromatic AP of length a(n)+1 and difference n by examining all factors of length n*a(n) + 1.",
				"Again, we know when we've seen all of them. (End)"
			],
			"link": [
				"Jeffrey Shallit, \u003ca href=\"/A339949/b339949.txt\"\u003eTable of n, a(n) for n = 1..231\u003c/a\u003e",
				"D. Badziahin and J. Shallit, \u003ca href=\"https://arxiv.org/abs/2006.15842\"\u003eBadly approximable numbers, Kronecker's theorem, and diversity of Sturmian characteristic sequences\u003c/a\u003e, arXiv:2006.15842 [math.NT], 2020."
			],
			"example": [
				"For n  \u003e= 1,  r =  0..n, k \u003e= 0, let A014675(n*k+r) denote the k-th term of the r-th n-section of A014675; i.e.,",
				"(A014675(k)) = 212212122122121221212212212122122121221212212212122121...",
				"  has runlengths 1,1,2,1,1,1,2,1,2,1,...; a(1) = 2.",
				"(A014675(2k)) = 22112211222122212221122112221222122211221122112221222...",
				"  has runlengths 2,2,2,2,3,1,3,1,3,2,...",
				"(A014675(2k+1)) = 122212221122112211222122211221122112221222122211221...",
				"   has runlengths 1,3,1,3,2,2,2,2,2,3,...; a(2) = 3.",
				"(A014675(3k)) = 22111222211122221122222112222211222211122221112222111...",
				"  has runlengths 2,3,4,3,4,2,5,2,5,2,4,3,4,3,...",
				"(A014675(3k+1)) = 112222111222211122221112222111222211222221122221112...",
				"  has runlengths 2,4,3,4,3,4,3,4,3,4,,5,2,4,3,...",
				"(A014675(3k+2)) = 222211222221122221112222111222211122221112222112222...",
				"  has runlengths 4,2,5,2,4,3,4,3,4,3,4,3,4,2,...; a(3) = 5."
			],
			"mathematica": [
				"r = (1 + Sqrt[5])/2; z = 4000;",
				"f[n_] := Floor[(n + 2) r] - Floor[(n+1) r];  (* A014675 *)",
				"t = Table[Max[Map[Length,Union[Split[Table [f[n m], {n, 0, Floor[z/m]}]]]]], {m, 1, 20}, {n, 1, m}];",
				"Map[Max, t] (* A339949 *)"
			],
			"xref": [
				"Cf. A001622, A003849, A014675, A339950."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Clark Kimberling_, Dec 26 2020",
			"ext": [
				"a(61) corrected by _Jeffrey Shallit_, Mar 23 2021"
			],
			"references": 2,
			"revision": 19,
			"time": "2021-04-10T22:39:23-04:00",
			"created": "2020-12-28T17:54:31-05:00"
		}
	]
}