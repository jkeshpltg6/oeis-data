{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A134465",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 134465,
			"data": "1,6,16,32,55,86,126,176,237,310,396,496,611,742,890,1056,1241,1446,1672,1920,2191,2486,2806,3152,3525,3926,4356,4816,5307,5830,6386,6976,7601,8262,8960,9696,10471,11286,12142,13040,13981,14966",
			"name": "Row sums of triangle A134464.",
			"comment": [
				"a(n) is the number of compositions of n+9 into n parts avoiding parts 2 and 3. - _Milan Janjic_, Jan 07 2016"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A134465/b134465.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"David Anderson, E. S. Egge, M. Riehl, L. Ryan, R. Steinke, Y. Vaughan, \u003ca href=\"http://arxiv.org/abs/1605.06825\"\u003ePattern Avoiding Linear Extensions of Rectangular Posets\u003c/a\u003e, arXiv:1605.06825 [math.CO], 2016.",
				"Colin Defant, \u003ca href=\"https://arxiv.org/abs/1905.02309\"\u003eProofs of Conjectures about Pattern-Avoiding Linear Extensions\u003c/a\u003e, arXiv:1905.02309 [math.CO], 2019.",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-6,4,-1)."
			],
			"formula": [
				"Binomial transform of [1, 5, 5, 1, 0, 0, 0,...].",
				"G.f.: x*(1+2*x-2*x^2) / (1-x)^4 . - _R. J. Mathar_, Apr 04 2012",
				"a(n) = 4*a(n-1) -6*a(n-2) +4*a(n-3) -a(n-4). - _Vincenzo Librandi_, Jun 29 2012"
			],
			"example": [
				"a(4) = 32 = sum of row 4, triangle A134464: (4 + 6 + 9 + 13).",
				"a(4) = 32 = (1, 3, 3, 1) dot (1, 5, 5, 1) = (1 + 15 + 15 + 1)."
			],
			"mathematica": [
				"CoefficientList[Series[(1+2*x-2*x^2)/(1-x)^4,{x,0,50}],x] (* _Vincenzo Librandi_, Jun 29 2012 *)"
			],
			"program": [
				"(MAGMA) I:=[1, 6, 16, 32]; [n le 4 select I[n] else 4*Self(n-1)-6*Self(n-2)+4*Self(n-3)-Self(n-4): n in [1..50]]; // _Vincenzo Librandi_, Jun 29 2012",
				"(PARI) Vec(x*(1+2*x-2*x^2)/(1-x)^4 + O(x^50)) \\\\ _Altug Alkan_, Jan 07 2016",
				"(Sage) ((1+2*x-2*x^2)/(1-x)^4).series(x, 50).coefficients(x, sparse=False) # _G. C. Greubel_, May 08 2019",
				"(GAP) a:=[1,6,16,32];; for n in [5..50] do a[n]:=4*a[n-1]-6*a[n-2]+ 4*a[n-3]-a[n-4]; od; a; # _G. C. Greubel_, May 08 2019"
			],
			"xref": [
				"Cf. A134464."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Gary W. Adamson_, Oct 26 2007",
			"references": 3,
			"revision": 30,
			"time": "2019-05-08T04:37:48-04:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}