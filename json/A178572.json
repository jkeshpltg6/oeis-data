{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A178572",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 178572,
			"data": "11,47,108,194,305,441,602,788,999,1235,1496,1782,2093,2429,2790,3176,3587,4023,4484,4970,5481,6017,6578,7164,7775,8411,9072,9758,10469,11205,11966,12752,13563,14399,15260,16146,17057,17993,18954,19940,20951",
			"name": "Numbers with ordered partitions that have periods of length 5.",
			"comment": [
				"From each ordered partition of the numbers (10+j) with 0\u003cj\u003c5 one removes the first part z(1) and adds 1 to the next z(1) parts to get a new partition until a period is reached.",
				"The a(n) sequence begins with 11 and each member has 1 period; the b(n) = A022282(n) sequence begins with 12 and each member has 2 periods; the c(n) = A022283(n) sequence begins with 13 and each member has 2 periods; the d(n) = n*(25*n + 3)/2 sequence begins with 14 and each member has 1 period of length 5."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A178572/b178572.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Amelia Carolina Sparavigna, \u003ca href=\"https://doi.org/10.5281/zenodo.3470205\"\u003eThe groupoid of the Triangular Numbers and the generation of related integer sequences\u003c/a\u003e, Politecnico di Torino, Italy (2019).",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1)."
			],
			"formula": [
				"G.f. for a(n): (11 + 14*x)/(1-x)^3.",
				"     for b(n): (12 + 13*x)/(1-x)^3.",
				"     for c(n): (13 + 12*x)/(1-x)^3.",
				"     for d(n): (14 + 11*x)/(1-x)^3.",
				"All sequences have the same recurrence",
				"  s(n+3) = 3*s(n+2) - 3*s(n+1) + s(n)",
				"  with s(0)=0, s(1) = 10 + j, s(2) = 45 + 2*j and 0\u003cj\u003c5.",
				"s(n) = n*(25*n - 5 + 2*j)/2  and 0\u003cj\u003c5.",
				"The general formula for numbers with periods of length k:  a(k,j,n) = n*(k^2*n - k + 2*j)/2 with 0\u003cj\u003ck.",
				"  For j=1 and j=(k-1) the numbers have 1 period.",
				"  For 1\u003cj\u003c(k-1) the numbers have A092964(k-4,j-1) periods.",
				"  G.f.: (binomial(k,2)*(1+x) + j + (k-j)*x)/(1-x)^3."
			],
			"example": [
				"For n=11 the period is [(4,3,2,1,1), (4,3,2,2), (4,3,3,1), (4,4,2,1), (5,3,2,1)].",
				"For n=47 the period is [(9,8,7,6,6,4,3,2,1,1), (9,8,7,7,5,4,3,2,2), (9,8,8,6,5,4,3,3,1), (9,9,7,6,5,4,4,2,1), (10,8,7,6,5,5,3,2,1)].",
				"For n=12 the 2 periods are [(4,3,2,2,1), (4,3,3,2), (4,4,3,1), (5,4,2,1), (5,3,2,1,1)] and [(4,3,3,1,1), (4,4,2,2), (5,3,3,1), (4,4,2,1,1), (5,3,2,2)].",
				"For n=49 the 2 periods are [(9,8,7,7,6,4,3,2,2,1), (9,8,8,7,5,4,3,3,2), (9,9,8,6,5,4,4,3,1), (10,9,7,6,5,5,4,2,1), (10,8,7,6,6,5,3,2,1,1)] and [(9,8,8,6,6,4,3,3,1,1), (9,9,7,7,5,4,4,2,2),(10,8,8,6,5,5,3,3,1), (9,9,7,6,6,4,4,2,1,1), (10,8,7,7,5,5,3,2,2)]."
			],
			"mathematica": [
				"LinearRecurrence[{3,-3,1},{11,47,108},50] (* _Harvey P. Dale_, Jan 14 2019 *)",
				"Table[n*(25*n-3)/2, {n,1,50}] (* _G. C. Greubel_, Jan 30 2019 *)"
			],
			"program": [
				"(PARI) a(n)=n*(25*n-3)/2 \\\\ _Charles R Greathouse IV_, Jun 18 2017",
				"(MAGMA) [n*(25*n-3)/2: n in [1..50]]; // _G. C. Greubel_, Jan 30 2019",
				"(Sage) [n*(25*n-3)/2 for n in (1..50)] # _G. C. Greubel_, Jan 30 2019",
				"(GAP) List([1..50], n -\u003e n*(25*n-3)/2); # _G. C. Greubel_, Jan 30 2019"
			],
			"xref": [
				"Cf. A092964, A037306."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_Paul Weisenhorn_, Dec 24 2010",
			"references": 2,
			"revision": 32,
			"time": "2020-01-11T15:29:03-05:00",
			"created": "2010-11-12T14:27:16-05:00"
		}
	]
}