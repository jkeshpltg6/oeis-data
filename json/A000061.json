{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000061",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 61,
			"id": "M0938 N0352",
			"data": "1,1,2,4,4,6,8,8,12,14,14,16,20,20,24,32,24,30,38,32,40,46,40,48,60,50,54,64,60,68,80,64,72,92,76,96,100,82,104,112,96,108,126,112,120,148,112,128,168,130,156,160,140,162,184,160,168,198,170,192,220,168,192",
			"name": "Generalized tangent numbers d(n,1).",
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Sean A. Irvine, \u003ca href=\"/A000061/b000061.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Donald E. Knuth and Thomas J. Buckholtz, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1967-0221735-9\"\u003eComputation of tangent, Euler and Bernoulli numbers\u003c/a\u003e, Math. Comp. 21 1967 663-688.",
				"D. Shanks, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1967-0223295-5\"\u003eGeneralized Euler and class numbers\u003c/a\u003e, Math. Comp. 21 1967 689-694.",
				"D. Shanks, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1968-0227093-9\"\u003eCorrigenda to: \"Generalized Euler and class numbers\"\u003c/a\u003e, Math. Comp. 22 (1968), 699",
				"D. Shanks, \u003ca href=\"/A000003/a000003.pdf\"\u003eGeneralized Euler and class numbers\u003c/a\u003e, Math. Comp. 21 (1967), 689-694; 22 (1968), 699. [Annotated scanned copy]",
				"Peter J. Taylor, \u003ca href=\"/A000061/a000061.py.txt\"\u003ePython program to compute terms for this and related sequences\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/TangentNumber.html\"\u003eTangent Number\u003c/a\u003e"
			],
			"formula": [
				"From _Sean A. Irvine_, Mar 26 2012, corrected by _Peter J. Taylor_, Sep 26 2017: (Start)",
				"Consider the Dirichlet series L_a(s) = sum_{k\u003e=0} (-a|2k+1) / (2k+1)^s, where (-a|2k+1) is the Jacobi symbol. Then the numbers d(a,n) are defined by L_-a(2n)= (pi/(2a))^(2n)*sqrt(a)* d(a,n)/ (2n-1)! for a\u003e1 and n=1,2,3...; or by L_-a(2n)= (1/2)*(pi/(2a))^(2n)*sqrt(a)* d(a,n)/ (2n-1)! for a=1 and n=1,2,3,...",
				"From the Shanks paper, these can be computed as:",
				"d(1,n)=A000182(n)",
				"d(m^2,n)=(1/2) * m^(2n-1) * (m*prod_(p_i|m)(p_i^(-1)))^(2*n) * prod_(p_i|m)(p_i^(2*n)-1) * d(1,n)",
				"Otherwise write a=bm^2, b squarefree, then d(a,n)=m^(2n-1) * (m*prod_(p_i|m)(p_i^(-1)))^(2*n) * prod_(p_i|m)(p_i^(2*n)-jacobi(b,p_i)) * d(b,n) with d(b,n), b squarefree determined by equating the recurrence",
				"D(b,n)=sum(d(b,n-i)*(-b^2)^i*C(2n-1,2i),i=0..n-1)with the case-wise expression",
				"D(b,n)=(-1)^(n-1) * sum(jacobi(k,b)*(b-4k)^(2n-1), k=1..(b-1)/2) if b == 1(mod 4)",
				"D(b,n)=(-1)^(n-1) * sum(jacobi(b,2k+1)*(b-(2k+1))^(2n-1),2k+1\u003cb) if b != 1(mod 4)",
				"Sequence gives a(n)=d(n,1). (End)"
			],
			"program": [
				"(Python) # See Taylor link."
			],
			"xref": [
				"Cf. A000176."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from Kok Seng Chua (chuaks(AT)ihpc.nus.edu.sg), Jun 03 2000",
				"It would be nice to have a more precise definition! - _N. J. A. Sloane_, May 26 2007"
			],
			"references": 6,
			"revision": 60,
			"time": "2021-05-13T11:23:23-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}