{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A173557",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 173557,
			"data": "1,1,2,1,4,2,6,1,2,4,10,2,12,6,8,1,16,2,18,4,12,10,22,2,4,12,2,6,28,8,30,1,20,16,24,2,36,18,24,4,40,12,42,10,8,22,46,2,6,4,32,12,52,2,40,6,36,28,58,8,60,30,12,1,48,20,66,16,44,24,70,2,72,36",
			"name": "a(n) = Product_{primes p dividing n} (p-1).",
			"comment": [
				"This is A023900 without the signs. - _T. D. Noe_, Jul 31 2013",
				"Numerator of c_n = Product_{odd p| n} (p-1)/(p-2). Denominator is A305444. The initial values c_1, c_2, ... are 1, 1, 2, 1, 4/3, 2, 6/5, 1, 2, 4/3, 10/9, 2, 12/11, 6/5, 8/3, 1, 16/15, ... [Yamasaki and Yamasaki]. - _N. J. A. Sloane_, Jan 19 2020",
				"Kim et al. (2019) named this function the absolute Möbius divisor function. - _Amiram Eldar_, Apr 08 2020"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A173557/b173557.txt\"\u003eTable of n, a(n) for n = 1..65536\u003c/a\u003e (first 1000 terms from T. D. Noe)",
				"Daeyeoul Kim, Umit Sarp, and Sebahattin Ikikardes, \u003ca href=\"http://dx.doi.org/10.18514/MMN.2019.2470\"\u003eCertain combinatoric convolution sums arising from Bernoulli and Euler Polynomials\u003c/a\u003e, Miskolc Mathematical Notes, No. 20, Vol. 1 (2019): pp. 311-330.",
				"Daeyeoul Kim, Umit Sarp, and Sebahattin Ikikardes, \u003ca href=\"https://doi.org/10.3390/math7111083\"\u003eIterating the Sum of Möbius Divisor Function and Euler Totient Function\u003c/a\u003e, Mathematics, Vol. 7, No. 11 (2019), pp. 1083-1094.",
				"Yamasaki, Yasuo, and Aiichi Yamasaki, \u003ca href=\"https://repository.kulib.kyoto-u.ac.jp/dspace/bitstream/2433/84326/1/0887-10.pdf\"\u003eOn the Gap Distribution of Prime Numbers\u003c/a\u003e, Kyoto University Research Information Repository, October 1994. MR1370273 (97a:11141)."
			],
			"formula": [
				"a(n) = A003958(n) iff n is squarefree. a(n) = |A023900(n)|.",
				"Multiplicative with a(p^e) = p-1, e \u003e= 1. - _R. J. Mathar_, Mar 30 2011",
				"a(n) = phi(rad(n)) = A000010(A007947(n)). - _Enrique Pérez Herrero_, May 30 2012",
				"a(n) = A000010(n) / A003557(n). - _Jason Kimberley_, Dec 09 2012",
				"Dirichlet g.f.: zeta(s) * Product_{p prime} (1 - 2p^(-s) + p^(1-s)). The Dirichlet inverse is multiplicative with b(p^e) = (1 - p) * (2 - p)^(e - 1) = Sum_k A118800(e, k) * p^k. - _Álvar Ibeas_, Nov 24 2017",
				"a(1) = 1; for n \u003e 1, a(n) = (A020639(n)-1) * a(A028234(n)). - _Antti Karttunen_, Nov 28 2017",
				"From _Vaclav Kotesovec_, Jun 18 2020: (Start)",
				"Dirichlet g.f.: zeta(s) * zeta(s-1) / zeta(2*s-2)  * Product_{p prime} (1 - 2/(p + p^s)).",
				"Sum_{k=1..n} a(k) ~ c * n^2 / 2, where c = A307868 = Product_{p prime} (1 - 2/(p*(p+1))) = 0.471680613612997868... (End)",
				"a(n) = (-1)^A001221(n)*A023900(n). - _M. F. Hasler_, Aug 14 2021"
			],
			"example": [
				"300 = 3*5^2*2^2 =\u003e a(300) = (3-1)*(2-1)*(5-1) = 8."
			],
			"maple": [
				"A173557 := proc(n) local dvs; dvs := numtheory[factorset](n) ; mul(d-1,d=dvs) ; end proc: # _R. J. Mathar_, Feb 02 2011",
				"# second Maple program:",
				"a:= n-\u003e mul(i[1]-1, i=ifactors(n)[2]):",
				"seq(a(n), n=1..80);  # _Alois P. Heinz_, Aug 27 2018"
			],
			"mathematica": [
				"a[n_] := Module[{fac = FactorInteger[n]}, If[n==1, 1, Product[fac[[i, 1]]-1, {i, Length[fac]}]]]; Table[a[n], {n, 100}]"
			],
			"program": [
				"(Haskell)",
				"a173557 1 = 1",
				"a173557 n = product $ map (subtract 1) $ a027748_row n",
				"-- _Reinhard Zumkeller_, Jun 01 2015",
				"(PARI) a(n) = my(f=factor(n)[,1]); prod(k=1, #f, f[k]-1); \\\\ _Michel Marcus_, Oct 31 2017",
				"(PARI) for(n=1, 100, print1(direuler(p=2, n, (1 - 2*X + p*X)/(1 - X))[n], \", \")) \\\\ _Vaclav Kotesovec_, Jun 18 2020",
				"(PARI) apply( {A173557(n)=vecprod([p-1|p\u003c-factor(n)[,1]])}, [1..77]) \\\\ _M. F. Hasler_, Aug 14 2021",
				"(Scheme, with memoization-macro definec) (definec (A173557 n) (if (= 1 n) 1 (* (- (A020639 n) 1) (A173557 (A028234 n))))) ;; _Antti Karttunen_, Nov 28 2017",
				"(MAGMA) [EulerPhi(n)/(\u0026+[(Floor(k^n/n)-Floor((k^n-1)/n)): k in [1..n]]): n in [1..100]]; // _Vincenzo Librandi_, Jan 20 2020"
			],
			"xref": [
				"Cf. A023900, A141564, A027748, A305444, A307868."
			],
			"keyword": "nonn,easy,mult",
			"offset": "1,3",
			"author": "_José María Grau Ribas_, Feb 21 2010",
			"ext": [
				"Definition corrected by _M. F. Hasler_, Aug 14 2021",
				"Incorrect formula removed by _Pontus von Brömssen_, Aug 15 2021"
			],
			"references": 83,
			"revision": 66,
			"time": "2021-08-17T10:45:34-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}