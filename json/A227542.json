{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A227542",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 227542,
			"data": "0,0,1,0,2,3,1,2,4,5,3,4,6,7,5,6,8,9,7,8,10,11,9,10,12,13,11,12,14,15,13,14,16,17,15,16,18,19,17,18,20,21,19,20,22,23,21,22,24,25,23,24,26,27,25,26,28,29,27,28,30,31,29,30,32,33,31,32,34,35,33,34,36,37,35,36,38,39,37,38,40,41,39,40,42,43,41,42,44,45,43,44,46",
			"name": "a(n) is the number of all terms preceding a(n-1) that have the same even-odd parity as a(n-1).",
			"comment": [
				"If a(n-1) is even, a(n) is the count of all even members preceding a(n-1). If a(n-1) is odd, then a(n) is the count of all odd members preceding a(n-1)."
			],
			"link": [
				"Andres M. Torres, \u003ca href=\"/A227542/b227542.txt\"\u003eTable of n, a(n) for n = 0..9999\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,0,0,1,-1)"
			],
			"formula": [
				"G.f. x^2 + x^4*(2+x-2*x^2+x^3)  / ( (1+x)*(1+x^2)*(x-1)^2 ). - _R. J. Mathar_, Jul 22 2013",
				"a(n) = (-3 - (-1)^n + (2+2*i)*(-i)^n + (2-i*2)*i^n + 2*n) / 4 for n\u003e2, where i=sqrt(-1). - _Colin Barker_, Oct 16 2015"
			],
			"example": [
				"{0,0}        : a(1)=0, because no values exist before a(0)=0.",
				"{0,0,1}      : a(2)=1, because 1 even value exists before a(1)=0.",
				"{0,0,1,0}    : a(3)=0, because no odd values exist before a(2)=1.",
				"{0,0,1,0,2}  : a(4)=2, because 2 even values exist before a(3)=0.",
				"{0,0,1,0,2,3}: a(5)=3, because 3 even values exist before a(4)=2."
			],
			"maple": [
				"A227542 := proc(n)",
				"    option remember;",
				"    local pari,a,i ;",
				"    if n = 0 then",
				"        0;",
				"    else",
				"        pari := type(procname(n-1),'even') ;",
				"        a := 0 ;",
				"        for i from 0 to n-2 do",
				"            if type(procname(i),'even') = pari then",
				"                a := a+1 ;",
				"            end if;",
				"        end do:",
				"        a ;",
				"    end if;",
				"end proc: # _R. J. Mathar_, Jul 22 2013"
			],
			"mathematica": [
				"Join[{0,0,1},LinearRecurrence[{1,0,0,1,-1},{0,2,3,1,2},100]] (* _Harvey P. Dale_, Oct 01 2013 *)"
			],
			"program": [
				";; [Blitz3D] Basic code",
				";; --a two index array to store counts of evens and odds",
				"Global EvenOdd[2]",
				";; store the sequence in an array",
				"Global a[10001]",
				"eo =0 ;; eo is a temporary variable",
				"a[1] = 0  ;; seq starts with \"0\"",
				"For z=1 To 10000  ;; create about 10000 values",
				"     eo = isOdd(a[z])",
				"     a[z+1] = EvenOdd[eo]",
				"     EvenOdd[eo] = EvenOdd[eo] +1",
				"Next",
				";; returns 1 if v is ODD, else returns zero",
				"Function isOdd(v)",
				"     Return v Mod 2",
				"End Function",
				"Function isEven(v)",
				"     Return (v Mod 2)=0",
				"End Function",
				"(PARI) a(n) = if(n==1, 0, if(n==2, 1, (-3 - (-1)^n + (2+2*I)*(-I)^n + (2-I*2)*I^n + 2*n) / 4)) \\\\ _Colin Barker_, Oct 16 2015",
				"(PARI) concat(vector(2), Vec((2*x^7-3*x^6+x^5+2*x^4-x^3+x^2)/(x^5-x^4-x+1) + O(x^100))) \\\\ _Colin Barker_, Oct 16 2015"
			],
			"keyword": "nonn,easy",
			"offset": "0,5",
			"author": "_Andres M. Torres_, Jul 15 2013",
			"references": 1,
			"revision": 31,
			"time": "2017-10-03T11:15:44-04:00",
			"created": "2013-07-22T08:44:20-04:00"
		}
	]
}