{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A308326",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 308326,
			"data": "1,1,-1,1,-4,3,1,-13,33,-21,1,-40,270,-546,315,1,-121,2010,-10080,17955,-9765,1,-364,14433,-165270,707805,-1171800,615195,1,-1093,102123,-2580081,24421005,-95765355,151953165,-78129765,1,-3280,718140,-39416076,795752370,-6790268520,25331269320,-39221142030,19923090075",
			"name": "The q-analog T(q; n,k) of the triangle A163626 for 0 \u003c= k \u003c= n, for q = 2.",
			"comment": [
				"The formulas are given for the general case depending on some fixed integer q. The terms are valid if q = 2.",
				"Special cases: T(0; n,k) = (-1)^k * binomial(n,k) for 0 \u003c= k \u003c= n and T(1; n,k) = A163626(n,k) for 0 \u003c= k \u003c= n."
			],
			"formula": [
				"T(q; n,k) = [k+1]_q * T(q; n-1,k) - [k]_q * T(q; n-1,k-1) for 1 \u003c= k \u003c= n with initial values T(q; n,0) = 1 for n \u003e= 0 and T(q; i,j) = 0 if i \u003c j or j \u003c 0 where [i]_q = (q^i - 1)/(q - 1) for i \u003e= 0.",
				"T(q; n,k) = (1/q^binomial(k+1,2)) * (Sum_{j=0..k} (-1)^j * [k,j]_q * q^binomial(k-j,2) * ([j+1]_q)^n) for 0 \u003c= k \u003c= n and q not equal zero where [m,i]_q are the q-binomials (here A022166 for q = 2) and [i]_q = (q^i - 1)/(q - 1) for i \u003e= 0.",
				"Sum_{k=0..n} T(q; n,k) = A000007(n) for n \u003e= 0.",
				"T(q; n,k)/T(q; k,k) give the q-analogs of the Stirling numbers of the second kind (for q = 2 see A139382, but offset 1).",
				"T(q; n,n) = (-1)^n * Product_{j=1..n} [j]_q for n\u003e=0 with empty product 1 (case n = 0) where [i]_q = (q^i - 1)/(q - 1) for i \u003e= 0.",
				"T(q; n,1) = -[n,1]_(q+1) for n \u003e= 1 where [m,i]_q are the q-binomials (here A022166 for q = 2 and A022167 for q = 3).",
				"G.f. of column k: col(q; t,k) = Sum_{n\u003e=k} T(q; n,k)*t^n = ((-t)^k/(1-t)) * Product_{j=1..k} ([i]_q/(1-[i+1]_q*t)) for k\u003e=0 with empty product 1 (case k=0) and [i]_q = i if q = 1 otherwise (q^i-1)/(q-1) for i\u003e=0."
			],
			"example": [
				"If q = 2 the triangle T(2; n,k) starts:",
				"n\\k:  0     1      2        3        4         5         6         7",
				"====================================================================",
				"  0:  1",
				"  1:  1    -1",
				"  2:  1    -4      3",
				"  3:  1   -13     33      -21",
				"  4:  1   -40    270     -546      315",
				"  5:  1  -121   2010   -10080    17955     -9765",
				"  6:  1  -364  14433  -165270   707805  -1171800    615195",
				"  7:  1 -1093 102123 -2580081 24421005 -95765355 151953165 -78129765",
				"etc."
			],
			"program": [
				"(PARI) q = 2; {T(n,k) = if(k\u003c0 || k\u003en, 0, if(k==0, 1, if(q==1, (k+1) * T(n-1,k) - k * T(n-1,k-1), ((q^(k+1) - 1)/(q - 1)) * T(n-1,k) - ((q^k - 1)/(q - 1)) * T(n-1,k-1))))};",
				"for(n=0, 9, for(k=0, n, print1(T(n,k), \", \"))) \\\\ _Werner Schulte_, May 26 2019"
			],
			"xref": [
				"Cf. A000007, A007318, A022166, A022167, A139382, A163626."
			],
			"keyword": "sign,tabl",
			"offset": "0,5",
			"author": "_Werner Schulte_, May 23 2019",
			"references": 1,
			"revision": 47,
			"time": "2019-08-11T00:09:52-04:00",
			"created": "2019-08-11T00:09:52-04:00"
		}
	]
}