{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A140996",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 140996,
			"data": "1,1,1,1,2,1,1,4,2,1,1,8,4,2,1,1,16,8,4,2,1,1,31,17,8,4,2,1,1,60,35,17,8,4,2,1,1,116,72,35,17,8,4,2,1,1,224,148,72,35,17,8,4,2,1,1,432,303,149,72,35,17,8,4,2,1,1,833,618,308,149,72,35,17,8,4,2,1,1,1606,1257,636,308,149,72,35,17,8,4,2,1",
			"name": "Triangle G(n, k) read by rows for 0 \u003c= k \u003c= n, where G(n, 0) = G(n+1, n+1) = 1, G(n+2, n+1) = 2, G(n+3, n+1) = 4, G(n+4, n+1) = 8, and G(n+5, m) = G(n+1, m-1) + G(n+1, m) + G(n+2, m) + G(n+3, m) + G(n+4, m) for n \u003e= 0 for m = 1..(n+1).",
			"comment": [
				"From _Petros Hadjicostas_, Jun 12 2019: (Start)",
				"This is a mirror of image of triangular array A140995. The current array has index of asymmetry s = 3 and index of obliqueness (obliquity) e = 0. Array A140995 has the same index of asymmetry, but has index of obliqueness e = 1. (In other related sequences, the author uses the letter y for the index of asymmetry and the letter z for the index of obliqueness, but the stone slab that appears over a tomb in a picture that he posted in those sequences, the letters s and e are used instead. See, for example, the documentation for sequences A140998, A141065, A141066, and A141067.)",
				"In general, if the index of asymmetry (from the Pascal triangle A007318) is s, then the order of the recurrence is s + 2 (because the recurrence of the Pascal triangle has order 2). There are also s + 2 infinite sets of initial conditions (as opposed to the Pascal triangle that has only 2 infinite sets of initial conditions, namely, G(n, 0) = G(n+1, n+1) = 1 for n \u003e= 0).",
				"Pascal's triangle A007318 has s = 0 and is symmetric, arrays A140998 and A140993 have s = 1 (with e = 0 and e = 1, respectively), arrays A140997 and A140994 have s = 2 (with e = 0 and e = 1, respectively), and arrays A141020 and A141021 have s = 4 (with e = 0 and e = 1, respectively).",
				"(End)"
			],
			"link": [
				"Robert Price, \u003ca href=\"/A140996/b140996.txt\"\u003eTable of n, a(n) for n = 0..5150\u003c/a\u003e",
				"Juri-Stepan Gerasimov, \u003ca href=\"/A140998/a140998.jpg\"\u003eStepan's triangles and Pascal's triangle are connected by the recurrence relation ...\u003c/a\u003e"
			],
			"formula": [
				"From _Petros Hadjicostas_, Jun 12 2019: (Start)",
				"G(n, k) = A140995(n, n - k) for 0 \u003c= k \u003c= n.",
				"Bivariate g.f.: Sum_{n,k \u003e= 0} G(n, k)*x^n*y^k = (1 - x - x^2 - x^3 - x^4 + x^2*y + x^3*y + x^5*y)/((1 - x) * (1 - x*y) * (1 - x - x^2 - x^3 - x^4 - x^4*y)).",
				"If we take the first derivative of the bivariate g.f. w.r.t. y and set y = 0, we get the g.f. of column k = 1: x/((1 - x) * (1 - x - x^2 - x^3 - x^4)). This is the g.f. of a shifted version of sequence A107066.",
				"Substituting y = 1 in the above bivariate function and simplifying, we get the g.f. of row sums: 1/(1 - 2*x). Hence, the row sums are powers of 2; i.e., A000079.",
				"(End)"
			],
			"example": [
				"Triangle (with rows n \u003e= 0 and columns k \u003e= 0) begins as follows:",
				"  1",
				"  1   1",
				"  1   2   1",
				"  1   4   2   1",
				"  1   8   4   2   1",
				"  1  16   8   4   2  1",
				"  1  31  17   8   4  2  1",
				"  1  60  35  17   8  4  2  1",
				"  1 116  72  35  17  8  4  2 1",
				"  1 224 148  72  35 17  8  4 2 1",
				"  1 432 303 149  72 35 17  8 4 2 1",
				"  1 833 618 308 149 72 35 17 8 4 2 1",
				"  ..."
			],
			"mathematica": [
				"nlim = 100;",
				"For[n = 0, n \u003c= nlim, n++, G[n, 0] = 1];",
				"For[n = 1, n \u003c= nlim, n++, G[n, n] = 1];",
				"For[n = 2, n \u003c= nlim, n++, G[n, n-1] = 2];",
				"For[n = 3, n \u003c= nlim, n++, G[n, n-2] = 4];",
				"For[n = 4, n \u003c= nlim, n++, G[n, n-3] = 8];",
				"For[n = 5, n \u003c= nlim, n++, For[k = 1, k \u003c n - 3, k++,",
				"   G[n, k] = G[n-4, k-1] + G[n-4, k] + G[n-3, k] + G[n-2, k] + G[n-1, k]]];",
				"A140996 = {}; For[n = 0, n \u003c= nlim, n++,",
				"For[k = 0, k \u003c= n, k++, AppendTo[A140996, G[n, k]]]];",
				"A140996 (* _Robert Price_, Jul 03 2019 *)"
			],
			"xref": [
				"Cf. A007318, A107066, A140993, A140994, A140995, A140997, A140998, A141020, A141021, A141031, A141065, A141066, A141067, A141068, A141069, A141070, A141072, A141073, A309462."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Juri-Stepan Gerasimov_, Jul 08 2008",
			"ext": [
				"Name edited by _Petros Hadjicostas_, Jun 12 2019"
			],
			"references": 24,
			"revision": 50,
			"time": "2019-08-05T02:07:00-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}