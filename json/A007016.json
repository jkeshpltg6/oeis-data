{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007016",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7016,
			"id": "M4491",
			"data": "0,1,0,0,8,20,96,656,5568,48912,494080,5383552,65097600,840566080,11833898496,176621049600,2838024476672,48060623405312,868000333234176,16441638519762944,329723762151352320,6907027877807330304",
			"name": "Number of permutations of length n with 1 fixed and 1 reflected point.",
			"comment": [
				"Number of distinct solutions to the order n checkerboard problem, including symmetrical solutions: place n pieces on an n X n board so there is exactly one piece in each row, column and main diagonal. Compare A064280.",
				"Number of magic permutation matrices of order n. - _Chai Wah Wu_, Jan 15 2019",
				"Upper bound for the number of diagonal transversals in a Latin square: A287647(n) \u003c= A287648(n) \u003c= a(n). - _Eduard I. Vatutin_, Jan 02 2020"
			],
			"reference": [
				"Simpson, Todd; Permutations with unique fixed and reflected points. Ars Combin. 39 (1995), 97-108.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Jean-François Alcover, \u003ca href=\"/A007016/b007016.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e",
				"F. Rakotondrajao, \u003ca href=\"http://emis.impa.br/EMIS/journals/SLC/wpapers/s54Arakoto.html\"\u003eMagic squares, rook polynomials and permutations\u003c/a\u003e, Séminaire Lotharingien de Combinatoire, vol. 54A, article B54Ac, 2006.",
				"T. Simpson, \u003ca href=\"/A002777/a002777.pdf\"\u003eLetter to N. J. A. Sloane, Mar. 1992\u003c/a\u003e",
				"T. Simpson, \u003ca href=\"/A007016/a007016.pdf\"\u003ePermutations with unique fixed and reflected points\u003c/a\u003e, Preprint. (Annotated scanned copy)",
				"E. Vatutin, \u003ca href=\"https://vk.com/wall162891802_1024\"\u003eUpper bound for the number of diagonal transversals in a Latin square\u003c/a\u003e (in Russian)."
			],
			"formula": [
				"a(2*m) = m*(x(2*m) - (2*m-3)*x(2*m-1)), a(2*m+1) = (2*m+1)*x(2*m) + 3*m*x(2*m-1) - 2*m*(m-1)*x(2*m-2), where x(n) = A003471(n)."
			],
			"mathematica": [
				"x[n_] := x[n] = Integrate[If[EvenQ[n], (x^2 - 4*x + 2)^(n/2), (x - 1)*(x^2 - 4*x + 2)^((n - 1)/2)]/E^x, {x, 0, Infinity}];",
				"a[n_ /; EvenQ[n]] := With[{m = n/2}, m*(x[2*m] - (2*m - 3)*x[2*m - 1])];",
				"a[n_ /; OddQ[n]] := With[{m = (n - 1)/2}, (2*m + 1)*x[2*m] + 3*m*x[2*m - 1] - 2*m*(m - 1)*x[2*m - 2]];",
				"Table[a[n], {n, 0, 21}] // Quiet (* _Jean-François Alcover_, Jun 29 2018 *)"
			],
			"program": [
				"(PARI)",
				"a(n) = {my(v = vector(n)); \\\\ v is A003471",
				"for(n=4, length(v), v[n] = (n-1)*v[n-1] + 2*if(n%2==1, (n-1)*v[n-2], (n-2) * if(n==4,1,v[n-4])));",
				"if(n\u003c4, [1,0,0][n], if(n%2==0, n*(v[n] - (n-3)*v[n-1]), 2*n*v[n-1] + 3*(n-1)*v[n-2] - (n-1)*(n-3)*v[n-3])/2)} \\\\ _Andrew Howroyd_, Sep 12 2017"
			],
			"xref": [
				"Cf. A003471, A064280."
			],
			"keyword": "nonn,easy",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_",
			"references": 36,
			"revision": 48,
			"time": "2020-01-03T23:30:03-05:00",
			"created": "1994-04-28T03:00:00-04:00"
		}
	]
}