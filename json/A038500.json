{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A038500",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 38500,
			"data": "1,1,3,1,1,3,1,1,9,1,1,3,1,1,3,1,1,9,1,1,3,1,1,3,1,1,27,1,1,3,1,1,3,1,1,9,1,1,3,1,1,3,1,1,9,1,1,3,1,1,3,1,1,27,1,1,3,1,1,3,1,1,9,1,1,3,1,1,3,1,1,9,1,1,3,1,1,3,1,1,81",
			"name": "Highest power of 3 dividing n.",
			"comment": [
				"To construct the sequence: start with 1 and concatenate twice: 1,1,1 then tripling the last term gives: 1,1,3. Concatenating those 3 terms twice gives: 1,1,3,1,1,3,1,1,3, triple the last term -\u003e 1,1,3,1,1,3,1,1,9. Concatenating those 9 terms twice gives: 1,1,3,1,1,3,1,1,9,1,1,3,1,1,3,1,1,9,1,1,3,1,1,3,1,1,9, triple the last term -\u003e 1,1,3,1,1,3,1,1,9,1,1,3,1,1,3,1,1,9,1,1,3,1,1,3,1,1,27 etc. - _Benoit Cloitre_, Dec 17 2002",
				"Also 3-adic value of 1/n, n \u003e= 1. See the Mahler reference, definition on p. 7. This is a non-archimedean valuation. See Mahler, p. 10. Sometimes also called 3-adic absolute value. - _Wolfdieter Lang_, Jun 28 2014"
			],
			"reference": [
				"K. Mahler, p-adic numbers and their functions, second ed., Cambridge University Press, 1981."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A038500/b038500.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Tyler Ball, Tom Edgar, and Daniel Juda, \u003ca href=\"http://dx.doi.org/10.4169/math.mag.87.2.135\"\u003eDominance Orders, Generalized Binomial Coefficients, and Kummer's Theorem\u003c/a\u003e, Mathematics Magazine, Vol. 87, No. 2, April 2014, pp. 135-143.",
				"Z. Sunic, \u003ca href=\"http://arXiv.org/abs/math.CO/0612080\"\u003eTree morphisms, transducers and integer sequences\u003c/a\u003e, arXiv:math/0612080 [math.CO], 2006."
			],
			"formula": [
				"Multiplicative with a(p^e) = p^e if p = 3, 1 otherwise. - _Mitch Harris_, Apr 19 2005",
				"a(n) = n / A038502(n). Dirichlet g.f. zeta(s)*(3^s-1)/(3^s-3). - _R. J. Mathar_, Jul 12 2012",
				"From _Peter Bala_, Feb 21 2019: (Start)",
				"a(n) = gcd(n,3^n).",
				"O.g.f.: x/(1 - x) + 2*Sum_{n \u003e= 1} 3^(n-1)*x^(3^n)/ (1 - x^(3^n)).",
				"End."
			],
			"maple": [
				"A038500 := n -\u003e 3^padic[ordp](n,3): # _Peter Luschny_, Nov 26 2010"
			],
			"mathematica": [
				"Flatten[{1,1,#}\u0026/@(3^IntegerExponent[#,3]\u0026/@(3*Range[40]))] (* or *) hp3[n_]:=If[Divisible[n,3],3^IntegerExponent[n,3],1]; Array[hp3,90] (* _Harvey P. Dale_, Mar 24 2012 *)",
				"Table[3^IntegerExponent[n, 3], {n, 100}] (* _Vincenzo Librandi_, Dec 29 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, 0, 3^valuation(n, 3))};",
				"(Haskell)",
				"a038500 = f 1 where",
				"   f y x = if m == 0 then f (y * 3) x' else y  where (x', m) = divMod x 3",
				"-- _Reinhard Zumkeller_, Jul 06 2014",
				"(MAGMA) [3^Valuation(n,3): n in [1..100]]; // _Vincenzo Librandi_, Dec 29 2015"
			],
			"xref": [
				"Cf. A007949, A038502, A060904, A268354, A268357."
			],
			"keyword": "nonn,mult",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_",
			"references": 60,
			"revision": 52,
			"time": "2019-02-22T05:12:59-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}