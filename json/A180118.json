{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A180118",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 180118,
			"data": "0,6,18,38,68,110,166,238,328,438,570,726,908,1118,1358,1630,1936,2278,2658,3078,3540,4046,4598,5198,5848,6550,7306,8118,8988,9918,10910,11966,13088,14278,15538,16870,18276,19758,21318,22958,24680,26486,28378,30358",
			"name": "a(n) = Sum_{k=1..n} (k+2)!/k! = Sum_{k=1..n} (k+2)*(k+1).",
			"comment": [
				"In general, sequences of the form a(n) = sum((k+x+2)!/(k+x)!,k=1..n) have a closed form a(n) = n*(11+12*x+3*x^2+3*x*n+6*n+n^2)/3.",
				"This sequence is related to A033487 by A033487(n) = n*a(n)-sum(a(i), i=0..n-1). - _Bruno Berselli_, Jan 24 2011",
				"The minimal number of multiplications (using schoolbook method) needed to compute the matrix chain product of a sequence of n+1 matrices having dimensions 1 X 2, 2 X 3, ..., (n+1) X (n+2), respectively. - _Alois P. Heinz_, Jan 27 2017"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A180118/b180118.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"B. Berselli, A description of the recursive method in Comments lines: website \u003ca href=\"http://www.lanostra-matematica.org/2008/12/sequenze-numeriche-e-procedimenti.html\"\u003eMatem@ticamente\u003c/a\u003e (in Italian).",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Matrix_chain_multiplication\"\u003eMatrix chain multiplication\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Matrix_multiplication_algorithm#Iterative_algorithm\"\u003eSchoolbook matrix multiplication\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-6,4,-1)."
			],
			"formula": [
				"a(n) = +4*a(n-1)-6*a(n-2)+4*a(n-3)-1*a(n-4) for n\u003e=4.",
				"a(n) = n*(n^2+6*n+11)/3.",
				"From _Bruno Berselli_, Jan 24 2011:  (Start)",
				"G.f.: 2*x*(3-3*x+x^2)/(1-x)^4. [corrected by _Georg Fischer_, May 10 2019]",
				"Sum(a(k), k=0..n) = 2*A005718(n) for n\u003e0. (End)"
			],
			"mathematica": [
				"f[n_]:=n*(n^2 + 6 n + 11)/3; f[Range[0,60]] (* _Vladimir Joseph Stephan Orlovsky_, Feb 10 2011*)",
				"CoefficientList[Series[2*x*(3 - 3*x + x^2)/(1 - x)^4, {x, 0, 50}], x] (* _Vaclav Kotesovec_, May 10 2019 *)",
				"Table[Sum[(k+1)(k+2),{k,n}],{n,0,50}] (* or *) LinearRecurrence[{4,-6,4,-1},{0,6,18,38},50] (* _Harvey P. Dale_, Apr 21 2020 *)"
			],
			"program": [
				"(MAGMA) [n*(n^2+6*n+11)/3: n in [0..45]]; // _Vincenzo Librandi_, Jun 15 2011"
			],
			"xref": [
				"Cf. A005718, A033487, A050534."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Gary Detlefs_, Aug 10 2010",
			"references": 1,
			"revision": 52,
			"time": "2020-04-21T15:19:11-04:00",
			"created": "2010-08-27T03:00:00-04:00"
		}
	]
}