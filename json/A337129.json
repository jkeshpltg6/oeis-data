{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A337129",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 337129,
			"data": "1,2,3,4,6,16,8,12,32,84,16,24,64,168,440,32,48,128,336,880,2304,64,96,256,672,1760,4608,12064,128,192,512,1344,3520,9216,24128,63168,256,384,1024,2688,7040,18432,48256,126336,330752,512,768,2048,5376,14080,36864,96512,252672,661504,1731840",
			"name": "Triangular array read by rows: T(n,0) = 2^n, T(n,k) = Sum_{i=n-k..n, j=0..i-n+k, i\u003c\u003en or j\u003c\u003ek} T(i,j) for k \u003e 0.",
			"formula": [
				"T(n,0) = 2^n.",
				"T(n,k) = ((3+sqrt(5))^(k+1)-(3-sqrt(5))^(k+1))*(2^(n-k))/(4*sqrt(5)) for 1\u003c=k\u003c=n.",
				"T(n+1,n) = 2*T(n,n).",
				"T(n+m,n) = 2^m*T(n,n), for m\u003e=1.",
				"T(n,n) = A069429(n) = 2^(n-1)*A001906(n+1) for n\u003e=1.",
				"T(2*n,n) = (1/2)*A099157(n+1) = A004171(n-1)*A001906(n+1) for n\u003e=1.",
				"G.f.: (1 - x*y)*(1 - 2*x*y)/((1 - 6*x*y + 4*x^2*y^2)*(1 - 2*x)). - _Andrew Howroyd_, Sep 23 2020"
			],
			"example": [
				"The triangle  T(n,k) begins:",
				"   n\\k  0    1    2    3    4    5",
				"   0:   1",
				"   1:   2    3",
				"   2:   4    6    16",
				"   3:   8    12   32  84",
				"   4:   16   24   64  168  440",
				"   5:   32   48   128 336  880  2304",
				"   ...",
				"T(3,2) = ((3+sqrt(5))^3-(3-sqrt(5))^3)*(2)/(4*sqrt(5)) = (64*sqrt(5))/(2*sqrt(5)) = 32."
			],
			"maple": [
				"T := proc (n, k) if k = 0 and 0 \u003c= n then 2^n elif 1 \u003c= k and k \u003c= n then round((((3+sqrt(5))^(k+1)-(3-sqrt(5))^(k+1))*(2^(n-k))/(4*sqrt(5)))) else 0 end if end proc:seq(print(seq(T(n, k), k=0..n)), n=0..9);"
			],
			"mathematica": [
				"T[n_, 0] := 2^n;",
				"T[n_, n_] := 2^(n-1) Fibonacci[2n+2];",
				"T[n_, k_] /; 0\u003ck\u003cn := T[n, k] = 2 T[n-1, k];",
				"Table[T[n, k], {n, 0, 9}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Nov 13 2020 *)"
			],
			"program": [
				"(PARI) T(n,k) = if (k == 0, 2^n, my(w=quadgen(5, 'w)); ((2*w+2)^(k+1)-(4-2*w)^(k+1))*(2^(n-k))/(4*(2*w-1))); \\\\ _Michel Marcus_, Sep 14 2020",
				"(PARI) Row(n)={Vecrev(polcoef((1-x*y)*(1-2*x*y)/((1-6*x*y+4*x^2*y^2)*(1-2*x)) + O(x*x^n), n))} \\\\ _Andrew Howroyd_, Sep 23 2020"
			],
			"xref": [
				"Cf. A000079 (1st column), A069429 (diagonal), A018903 (row sums), A001906, A004171."
			],
			"keyword": "nonn,tabl",
			"offset": "0,2",
			"author": "_Oboifeng Dira_, Sep 14 2020",
			"references": 0,
			"revision": 125,
			"time": "2020-11-14T07:04:48-05:00",
			"created": "2020-11-05T22:47:54-05:00"
		}
	]
}