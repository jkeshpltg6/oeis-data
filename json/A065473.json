{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A065473",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 65473,
			"data": "2,8,6,7,4,7,4,2,8,4,3,4,4,7,8,7,3,4,1,0,7,8,9,2,7,1,2,7,8,9,8,3,8,4,4,6,4,3,4,3,3,1,8,4,4,0,9,7,0,5,6,9,9,5,6,4,1,4,7,7,8,5,9,3,3,6,6,5,2,2,4,3,1,3,1,9,4,3,2,5,8,2,4,8,9,1,2,6,8,2,5,5,3,7,4,2,3,7,4,6,8,5,3,6,4,7",
			"name": "Decimal expansion of the strongly carefree constant: Product_{p prime} (1 - (3*p-2)/(p^3)).",
			"comment": [
				"Also decimal expansion of the probability that an integer triple (x, y, z) is pairwise coprime. - _Charles R Greathouse IV_, Nov 14 2011",
				"The probability that 2 numbers chosen at random are coprime, and both squarefree (Delange, 1969). - _Amiram Eldar_, Aug 04 2020"
			],
			"reference": [
				"Gerald Tenenbaum, Introduction to Analytic and Probabilistic Number Theory, 3rd edition, American Mathematical Society, 2015, page 59, exercise 55 and 56."
			],
			"link": [
				"Juan Arias de Reyna, R. Heyman, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL18/Heyman/heyman6.html\"\u003eCounting Tuples Restricted by Pairwise Coprimality Conditions\u003c/a\u003e, J. Int. Seq. 18 (2015) 15.10.4",
				"Tim Browning, \u003ca href=\"https://jtnb.centre-mersenne.org/article/JTNB_2011__23_3_579_0.pdf\"\u003eThe divisor problem for binary cubic forms\u003c/a\u003e, Journal de théorie des nombres de Bordeaux, Vol. 23, No. 3 (2011), pp. 579-602; \u003ca href=\"http://arxiv.org/abs/1006.3476\"\u003earXiv preprint\u003c/a\u003e, arXiv:1006.3476 [math.NT], 2010.",
				"Hubert Delange, \u003ca href=\"https://doi.org/10.1016/0022-314X(69)90045-6\"\u003eOn some sets of pairs of positive integers\u003c/a\u003e, Journal of Number Theory, Vol. 1, No. 3 (1969), pp. 261-279. See p. 277.",
				"Steven R. Finch, \u003ca href=\"https://doi.org/10.1017/9781316997741\"\u003eMathematical Constants II\u003c/a\u003e, Encyclopedia of Mathematics and Its Applications, Cambridge University Press, Cambridge, 2018, p. 181.",
				"G. Niklasch, \u003ca href=\"http://guests.mpim-bonn.mpg.de/moree/Moree.en.html\"\u003eSome number theoretical constants: 1000-digit values\u003c/a\u003e.",
				"G. Niklasch, \u003ca href=\"/A001692/a001692.html\"\u003eSome number theoretical constants: 1000-digit values\u003c/a\u003e. [cached copy]",
				"László Tóth, \u003ca href=\"http://www.fq.math.ca/Scanned/40-1/toth.pdf\"\u003eThe probability that k positive integers are pairwise relatively prime\u003c/a\u003e, Fibonacci Quart., Vol. 40 (2002), pp. 13-18.",
				"László Tóth, \u003ca href=\"https://arxiv.org/abs/2006.12438\"\u003eAnother generalization of Euler's arithmetic function and of Menon's identity\u003c/a\u003e, arXiv:2006.12438 [math.NT], 2020. See p. 3.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CarefreeCouple.html\"\u003eCarefree Couple\u003c/a\u003e."
			],
			"formula": [
				"Equals Prod_{p prime} (1 - 1/p)^2*(1 + 2/p). - _Michel Marcus_, Apr 16 2016",
				"The constant c in Sum_{k\u003c=x} mu(k)^2 * 2^omega(k) = c * x * log(x) + O(x), where mu is A008683 and omega is A001221, and in Sum_{k\u003c=x} 3^omega(k) = (1/2) * c * x * log(x)^2 + O(x*log(x)) (see Tenenbaum, 2015). - _Amiram Eldar_, May 24 2020",
				"Equals A065472 * A227929 =  A065472 / A098198. - _Amiram Eldar_, Aug 04 2020"
			],
			"example": [
				"0.2867474284344787341078927127898384..."
			],
			"mathematica": [
				"digits = 100; NSum[-(2+(-2)^n)*PrimeZetaP[n]/n, {n, 2, Infinity}, NSumTerms -\u003e 2 digits, WorkingPrecision -\u003e 2 digits, Method -\u003e \"AlternatingSigns\"] // Exp // RealDigits[#, 10, digits]\u0026 // First (* _Jean-François Alcover_, Apr 11 2016 *)"
			],
			"program": [
				"(PARI) prodeulerrat(1 - (3*p-2)/(p^3)) \\\\ _Amiram Eldar_, Mar 17 2021"
			],
			"xref": [
				"Cf. A065472, A069201, A069212, A074816, A074823, A078073, A098198, A227929, A299822."
			],
			"keyword": "cons,nonn",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_, Nov 19 2001",
			"ext": [
				"Name corrected by Antonio G. Astudillo (afg_astudillo(AT)lycos.com), Apr 03 2003",
				"More digits from _Vaclav Kotesovec_, Dec 19 2019"
			],
			"references": 21,
			"revision": 63,
			"time": "2021-06-13T13:31:08-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}