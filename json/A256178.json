{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A256178",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 256178,
			"data": "1,21,385,6930,124410,2232594,40062659,718896255,12900072515,231482415780,4153783429236,74536619356836,1337505365115205,24000559953034665,430672573790340805,7728105768275278134,138675231255170368494",
			"name": "Expansion of exp( Sum_{n \u003e= 1} L(2*n)*L(4*n)*x^n/n ), where L(n) = A000032(n) is a Lucas number.",
			"comment": [
				"Let L(n) = A000032(n) denote the n-th Lucas number.",
				"For a fixed positive integer k, the power series expansion of exp( Sum_{n \u003e= 1} L(k*n)x^n/n ) has integer coefficients given by the formula F(k*n)/F(k), where F(n) = A000045(n) [Johnson, 2.22].",
				"The power series expansion of exp( Sum_{n \u003e= 1} L(k*n)*L(2*k*n) *x^n/n ) has integer coefficients given by ( F(k*(n + 1))*F(k*(n + 2))*F(k*(n + 3)) )/( F(k)*F(2*k)*F(3*k) )",
				"The present sequence is the particular case k = 2. See A001655 for the case k = 1."
			],
			"link": [
				"B. Johnson, \u003ca href=\"http://maths.dur.ac.uk/~dma0rcj/PED/fib.pdf\"\u003eFibonacci Identities by Matrix Methods and Generalisation to Related Sequences\u003c/a\u003e",
				"Eric W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/FibonacciNumber.html\"\u003eFibonacci Number\u003c/a\u003e"
			],
			"formula": [
				"a(n) = ( F(2*n + 2)*F(2*n + 4)*F(2*n + 6) )/( F(2)*F(4)*F(6) ).",
				"a(n) = (1/8) * Sum_{k = 0..n} F(2*k + 2)*F(6*n - 6*k + 6).",
				"O.g.f.: 1/( (1 - 3*x + x^2)*(1 - 18*x + x^2) ) = 1/8 * Sum_{n \u003e= 0} F(2*n + 2)*x^n * Sum_{n \u003e= 0} F(6*n + 6)*x^n.",
				"O.g.f. also equals exp( Sum_{n \u003e= 1} ( trace( M^(2*n) + M^(6*n) )*x^n/n ), where M is the 2X2 matrix [ 1, 1; 1, 0 ].",
				"Recurrences: a(n) = 21*a(n-1) - 56*a(n-2) + 21*a(n-3) - a(n-4).",
				"Also a(0) = 1 and for n \u003e= 1, a(n) = (1/n)*Sum_{k = 1..n} L(2*k)*L(4*k)*a(n-k)."
			],
			"maple": [
				"seq((1/24)*fibonacci(2*n+2)*fibonacci(2*n+4)*fibonacci(2*n+6), n = 0 .. 16);"
			],
			"mathematica": [
				"Table[1/8 * Sum[Fibonacci[2*k + 2]*Fibonacci[6*n - 6*k + 6], {k, 0, n}], {n, 0, 17}] (* or *) RecurrenceTable[{a[n] == 21*a[n - 1] - 56*a[n - 2] + 21*a[n - 3] - a[n - 4], a[1] == 1, a[2] == 21, a[3] == 385, a[4] == 6930}, a, {n, 17}] (* _Michael De Vlieger_, Mar 18 2015 *)"
			],
			"xref": [
				"Cf. A000032, A000045, A001655, A010048."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Peter Bala_, Mar 18 2015",
			"references": 1,
			"revision": 12,
			"time": "2015-03-19T07:39:27-04:00",
			"created": "2015-03-19T07:39:27-04:00"
		}
	]
}