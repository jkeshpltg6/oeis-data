{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A208341",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 208341,
			"data": "1,1,2,1,3,4,1,4,8,8,1,5,13,20,16,1,6,19,38,48,32,1,7,26,63,104,112,64,1,8,34,96,192,272,256,128,1,9,43,138,321,552,688,576,256,1,10,53,190,501,1002,1520,1696,1280,512,1,11,64,253,743,1683,2972,4048",
			"name": "Triangle read by rows, T(n,k) = hypergeometric_2F1([n-k+1, -k], [1], -1) for n\u003e=0 and k\u003e=0.",
			"comment": [
				"Previous name was: Triangle of coefficients of polynomials v(n,x) jointly generated with A160232; see the Formula section.",
				"Row sums: (1,3,8,...), even-indexed Fibonacci numbers.",
				"Alt. row sums: (1,-1,2,-3,...), signed Fibonacci numbers.",
				"v(n,2) = A107839(n), v(n,n) = 2^(n-1), v(n+1,n) = A001792(n),",
				"v(n+2,n) = A049611, v(n+3,n) = A049612.",
				"Subtriangle of the triangle T(n,k) given by (1, 0, -1/2, 1/2, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Mar 12 2012",
				"Essentially triangle in A049600. - _Philippe Deléham_, Mar 23 2012"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A208341/b208341.txt\"\u003eRows n = 0..124 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"u(n,x) = u(n-1,x) + x*v(n-1,x), v(n,x) = u(n-1,x) + 2x*v(n-1,x), where u(1,x) = 1, v(1,x) = 1.",
				"As DELTA-triangle with 0 \u003c= k \u003c= n: T(n,k) = T(n-1,k) + 2*T(n-1,k-1) - T(n-2,k-1), T(0,0) = T(1,0) = T(2,0) = 1, T(1,1) = T(2,2) = 0, T(2,1) = 2 and T(n,k) = 0 if k\u003c0 or if k\u003en. - _Philippe Deléham_, Mar 12 2012",
				"G.f.: (1-2*y*x+y*x^2)/(1-x-2*y*x+y*x^2). - _Philippe Deléham_, Mar 12 2012",
				"T(n,k) = A106195(n-1,n-k), k = 1..n. - _Reinhard Zumkeller_, Dec 16 2013",
				"From _Peter Bala_, Aug 11 2015: (Start)",
				"The following remarks assume the row and column indexing start at 0.",
				"T(n,k) = Sum_{i = 0..k} 2^(k-i)*binomial(n-k,i)*binomial(k,i) = Sum_{i = 0..k} binomial(n-k+i,i)*binomial(k,i).",
				"Riordan array (1/(1 - x), x*(2 - x)/(1 - x)).",
				"O.g.f. 1/(1 - (2*t + 1)*x + t*x^2) = 1 + (1 + 2*t)*x + (1 + 3*t + 4*t^2)*x^2 + ....",
				"Read as a square array, this equals P * transpose(P^2), where P denotes Pascal's triangle A007318. (End)",
				"For k\u003cn, T(n,k) = T(n-1,k) + Sum_{i=1..k} T(n-i,k-i). - _Glen Whitney_, Aug 17 2021"
			],
			"example": [
				"First five rows:",
				"  1;",
				"  1, 2;",
				"  1, 3,  4;",
				"  1, 4,  8,  8;",
				"  1, 5, 13, 20, 16;",
				"First five polynomials v(n,x):",
				"  1",
				"  1 + 2x",
				"  1 + 3x +  4x^2",
				"  1 + 4x +  8x^2 +  8x^3",
				"  1 + 5x + 13x^2 + 20x^3 + 16x^4",
				"(1, 0, -1/2, 1/2, 0, 0, ...) DELTA (0, 2, 0, 0, 0, ...) begins:",
				"  1;",
				"  1, 0;",
				"  1, 2,  0;",
				"  1, 3,  4,  0;",
				"  1, 4,  8,  8,  0;",
				"  1, 5, 13, 20, 16,  0;",
				"  1, 6, 19, 38, 48, 32, 0;",
				"Triangle in A049600 begins:",
				"  0;",
				"  0, 1;",
				"  0, 1, 2;",
				"  0, 1, 3,  4;",
				"  0, 1, 4,  8,  8;",
				"  0, 1, 5, 13, 20, 16;",
				"  0, 1, 6, 19, 38, 48, 32;",
				"  ... - _Philippe Deléham_, Mar 23 2012"
			],
			"maple": [
				"T := (n,k) -\u003e hypergeom([n-k+1, -k],[1],-1):",
				"seq(lprint(seq(simplify(T(n,k)),k=0..n)),n=0..7); # _Peter Luschny_, May 20 2015"
			],
			"mathematica": [
				"u[1, x_] := 1; v[1, x_] := 1; z = 13;",
				"u[n_, x_] := u[n - 1, x] + x*v[n - 1, x];",
				"v[n_, x_] := u[n - 1, x] + 2*x*v[n - 1, x];",
				"Table[Expand[u[n, x]], {n, 1, z/2}]",
				"Table[Expand[v[n, x]], {n, 1, z/2}]",
				"cu = Table[CoefficientList[u[n, x], x], {n, 1, z}];",
				"TableForm[cu]",
				"Flatten[%]   (* A160232 *)",
				"Table[Expand[v[n, x]], {n, 1, z}]",
				"cv = Table[CoefficientList[v[n, x], x], {n, 1, z}];",
				"TableForm[cv]",
				"Flatten[%]   (* A208341 *)"
			],
			"program": [
				"(Haskell)",
				"a208341 n k = a208341_tabl !! (n-1) !! (k-1)",
				"a208341_row n = a208341_tabl !! (n-1)",
				"a208341_tabl = map reverse a106195_tabl",
				"-- _Reinhard Zumkeller_, Dec 16 2013",
				"(PARI) T(n,k) = sum(i = 0, k, 2^(k-i)*binomial(n-k,i)*binomial(k,i));",
				"tabl(nn) = for (n=0, nn, for (k=0, n, print1(T(n, k), \", \")); print();); \\\\ _Michel Marcus_, Aug 14 2015"
			],
			"xref": [
				"Cf. A160232, A000045, A049600, A106195."
			],
			"keyword": "nonn,tabl",
			"offset": "0,3",
			"author": "_Clark Kimberling_, Feb 25 2012",
			"ext": [
				"New name from _Peter Luschny_, May 20 2015",
				"Offset corrected by _Joerg Arndt_, Aug 12 2015"
			],
			"references": 9,
			"revision": 44,
			"time": "2021-08-21T02:45:12-04:00",
			"created": "2012-02-25T18:35:08-05:00"
		}
	]
}