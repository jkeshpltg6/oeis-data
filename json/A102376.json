{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A102376",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 102376,
			"data": "1,4,4,16,4,16,16,64,4,16,16,64,16,64,64,256,4,16,16,64,16,64,64,256,16,64,64,256,64,256,256,1024,4,16,16,64,16,64,64,256,16,64,64,256,64,256,256,1024,16,64,64,256,64,256,256,1024,64,256,256,1024,256,1024,1024",
			"name": "a(n) = 4^A000120(n).",
			"comment": [
				"Consider a simple cellular automaton, a grid of binary cells c(i,j), where the next state of the grid is calculated by applying the following rule to each cell: c(i,j) = ( c(i+1,j-1) + c(i+1,j+1) + c(i-1,j-1) + c(i-1,j+1) ) mod 2 If we start with a single cell having the value 1 and all the others 0, then the aggregate values of the subsequent states of the grid will be the terms in this sequence. - Andras Erszegi (erszegi.andras(AT)chello.hu), Mar 31 2006. See link for initial states. - _N. J. A. Sloane_, Feb 12 2015",
				"This is the odd-rule cellular automaton defined by OddRule 033 (see Ekhad-Sloane-Zeilberger \"Odd-Rule Cellular Automata on the Square Grid\" link). - _N. J. A. Sloane_, Feb 25 2015",
				"First differences of A116520. - _Omar E. Pol_, May 05 2010"
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A102376/b102376.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"David Applegate, Omar E. Pol and N. J. A. Sloane, \u003ca href=\"/A000695/a000695_1.pdf\"\u003eThe Toothpick Sequence and Other Sequences from Cellular Automata\u003c/a\u003e, Congressus Numerantium, Vol. 206 (2010), 157-191. [There is a typo in Theorem 6: (13) should read u(n) = 4.3^(wt(n-1)-1) for n \u003e= 2.]",
				"Shalosh B. Ekhad, N. J. A. Sloane, and Doron Zeilberger, \u003ca href=\"http://arxiv.org/abs/1503.01796\"\u003eA Meta-Algorithm for Creating Fast Algorithms for Counting ON Cells in Odd-Rule Cellular Automata\u003c/a\u003e, arXiv:1503.01796 [math.CO], 2015; see also the \u003ca href=\"http://www.math.rutgers.edu/~zeilberg/mamarim/mamarimhtml/CAcount.html\"\u003eAccompanying Maple Package\u003c/a\u003e.",
				"Shalosh B. Ekhad, N. J. A. Sloane, and Doron Zeilberger, \u003ca href=\"http://arxiv.org/abs/1503.04249\"\u003eOdd-Rule Cellular Automata on the Square Grid\u003c/a\u003e, arXiv:1503.04249 [math.CO], 2015.",
				"Nathan Epstein, \u003ca href=\"https://giant.gfycat.com/QueasyNeatHadrosaurus.webm\"\u003eAnimation of CA generating A102376\u003c/a\u003e",
				"N. J. A. Sloane, On the No. of ON Cells in Cellular Automata, Video of talk in Doron Zeilberger's Experimental Math Seminar at Rutgers University, Feb. 05 2015: \u003ca href=\"https://vimeo.com/119073818\"\u003ePart 1\u003c/a\u003e, \u003ca href=\"https://vimeo.com/119073819\"\u003ePart 2\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"http://arxiv.org/abs/1503.01168\"\u003eOn the Number of ON Cells in Cellular Automata\u003c/a\u003e, arXiv:1503.01168 [math.CO], 2015.",
				"N. J. A. Sloane, \u003ca href=\"/A102376/a102376.png\"\u003eIllustration of generations 0-15 of the cellular automaton\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/wiki/Catalog_of_Toothpick_and_CA_Sequences_in_OEIS\"\u003eCatalog of Toothpick and Cellular Automata Sequences in the OEIS\u003c/a\u003e",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e"
			],
			"formula": [
				"G.f.: product{k\u003e=0, 1 + 4x^(2^k)}; a(n)=product{k=0..log_2(n), 4^b(n, k)}, b(n, k)=coefficient of 2^k in binary expansion of n; a(n)=sum{k=0..n, (C(n, k) mod 2)*3^A000120(n-k)}. (Formulas due to _Paul D. Hanna_.)",
				"a(n) = sum{k=0..n, mod(C(n, k), 2)*sum{j=0..k, mod(C(k, j), 2)*sum{i=0..j, mod(C(j, i), 2)}}}. - _Paul Barry_, Apr 01 2005",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2), A(x^4)) where f(u, v, w) = w * (u^2 - 2*u*v + 5*v^2) - 4*v^3. - _Michael Somos_, May 29 2008",
				"Run length transform of A000302. - _N. J. A. Sloane_, Feb 23 2015"
			],
			"example": [
				"1 + 4*x + 4*x^2 + 16*x^3 + 4*x^4 + 16*x^5 + 16*x^6 + 64*x^7 + 4*x^8 + ...",
				"From _Omar E. Pol_, Jun 07 2009: (Start)",
				"Triangle begins:",
				"1;",
				"4;",
				"4,16;",
				"4,16,16,64;",
				"4,16,16,64,16,64,64,256;",
				"4,16,16,64,16,64,64,256,16,64,64,256,64,256,256,1024;",
				"4,16,16,64,16,64,64,256,16,64,64,256,64,256,256,1024,16,64,64,256,64,256,...",
				"(End)"
			],
			"maple": [
				"seq(4^convert(convert(n,base,2),`+`),n=0..100); # _Robert Israel_, Apr 30 2017"
			],
			"mathematica": [
				"Table[4^DigitCount[n, 2, 1], {n, 0, 100}] (* _Indranil Ghosh_, Apr 30 2017 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, 4^subst( Pol( binary(n)), x, 1))} /* _Michael Somos_, May 29 2008 */",
				"a(n) = 4^hammingweight(n); \\\\ _Michel Marcus_, Apr 30 2017",
				"(Haskell)",
				"a102376 = (4 ^) . a000120  -- _Reinhard Zumkeller_, Feb 13 2015",
				"(Python)",
				"def a(n): return 4**bin(n)[2:].count(\"1\") # _Indranil Ghosh_, Apr 30 2017"
			],
			"xref": [
				"For generating functions Prod_{k\u003e=0} (1+a*x^(b^k)) for the following values of (a,b) see: (1,2) A000012 and A000027, (1,3) A039966 and A005836, (1,4) A151666 and A000695, (1,5) A151667 and A033042, (2,2) A001316, (2,3) A151668, (2,4) A151669, (2,5) A151670, (3,2) A048883, (3,3) A117940, (3,4) A151665, (3,5) A151671, (4,2) A102376, (4,3) A151672, (4,4) A151673, (4,5) A151674.",
				"A151783 is a very similar sequence.",
				"Cf. A001316, A048883, A000079, A116520, A000302.",
				"See A160239 for the analogous CA defined by Rule 204 on an 8-celled neighborhood."
			],
			"keyword": "easy,nonn,tabf",
			"offset": "0,2",
			"author": "_Paul Barry_, Jan 05 2005",
			"references": 45,
			"revision": 58,
			"time": "2021-02-24T02:48:18-05:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}