{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A280850",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 280850,
			"data": "1,3,2,2,7,0,3,3,11,0,1,4,4,0,15,0,0,5,5,3,9,0,0,9,6,6,0,0,23,0,5,0,7,7,0,0,12,0,0,12,8,8,7,0,1,31,0,0,0,0,9,9,0,0,0,35,0,2,2,0,10,10,0,0,0,39,0,0,0,3,11,11,5,0,0,5,18,0,0,18,0,0,12,12,0,0,0,0,47,0,13,0,0,0",
			"name": "Irregular triangle read by rows in which row n is constructed with an algorithm using the n-th row of triangle A196020 (see Comments for precise definition).",
			"comment": [
				"For the construction of the n-th row of this triangle start with a copy of the n-th row of the triangle A196020.",
				"Then replace each element of the m-th pair of positive integers (x, y) with the value (x - y)/2, where \"y\" is the m-th even-indexed term of the row, and \"x\" is its previous nearest odd-indexed term not used in another pair in the same row, if such a pair exist. Otherwise T(n,k) = A196020(n,k). (See example).",
				"Observation 1: at least for the first 28 rows of the triangle the nonzero terms in the n-th row are also the subparts of the symmetric representation of sigma(n), assuming the ordering of the subparts in the same row does not matter.",
				"Question 1: are always the nonzero terms of the n-th row the same as all the subparts of the symmetric representation of sigma(n)? If not, what is the index of the row in which appears the first counterexample?",
				"Note that the \"subparts\" are the regions that arise after the dissection of the symmetric representation of sigma(n) into successive layers of width 1.",
				"For more information about \"subparts\" see A279387 and A237593.",
				"About the question 1, it appears that the n-th row of the triangle A280851 and the n-th row of this triangle contain the same nonzero numbers, though in different order; checked through n = 250000. - _Hartmut F. W. Hoft_, Jan 31 2018",
				"From _Omar E. Pol_, Feb 02 2018: (Start)",
				"Observation 2: at least for the first 28 rows of the triangle we have that in the n-th row the odd-indexed terms, from left to right, together with the even-indexed terms, from right to left, form a finite sequence in which the nonzero terms are the same as the n-th row of triangle A280851, which lists the subparts of the symmetric representation of sigma(n).",
				"Question 2: Are always the same for all rows? If not, what is the index of the row in which appears the first counterexample? (End)",
				"Conjecture: the odd-indexed terms of the n-th row together with the even-indexed terms of the same row but listed in reverse order give the n-th row of triangle A296508 (this is the same conjecture from A296508). - _Omar E. Pol_, Apr 20 2018"
			],
			"link": [
				"\u003ca href=\"/index/Si#SIGMAN\"\u003eIndex entries for sequences related to sigma(n)\u003c/a\u003e"
			],
			"example": [
				"Triangle begins (rows 1..28):",
				"   1;",
				"   3;",
				"   2,  2;",
				"   7,  0;",
				"   3,  3;",
				"  11,  0,  1;",
				"   4,  4,  0;",
				"  15,  0,  0;",
				"   5,  5,  3;",
				"   9,  0,  0,  9;",
				"   6,  6,  0,  0;",
				"  23,  0,  5,  0;",
				"   7,  7,  0,  0;",
				"  12,  0,  0, 12;",
				"   8,  8,  7,  0,  1;",
				"  31,  0,  0,  0,  0;",
				"   9,  9,  0,  0,  0;",
				"  35,  0,  2,  2,  0;",
				"  10, 10,  0,  0,  0;",
				"  39,  0,  0,  0,  3;",
				"  11, 11,  5,  0,  0,  5;",
				"  18,  0,  0, 18,  0,  0;",
				"  12, 12,  0,  0,  0,  0;",
				"  47,  0, 13,  0,  0,  0;",
				"  13, 13,  0,  0,  5,  0;",
				"  21,  0,  0, 21,  0,  0;",
				"  14, 14,  6,  0,  0,  6;",
				"  55,  0,  0,  0,  0,  0,  1;",
				"  ...",
				"An example of the algorithm.",
				"For n = 75, the construction of the 75th row of this triangle is as shown below:",
				".",
				"75th row of A196020:             [149,  73, 47, 0, 25, 19, 0, 0, 0,  5, 0]",
				".",
				"Odd-indexed terms:                149       47     25      0     0      0",
				"Even-indexed terms:                     73      0      19     0      5",
				".",
				"First even-indexed nonzero term:        73",
				"First pair:                       149   73",
				".                                   *----*",
				"Difference: 149 - 73 =                76",
				"76/2 = 38                           *----*",
				"New first pair:                    38   38",
				".",
				"Second even-indexed nonzero term:                      19",
				"Second pair:                                       25  19",
				".                                                   *---*",
				"Difference: 25 - 19 =                                 6",
				"6/2 = 3                                             *---*",
				"New second pair:                                    3   3",
				".",
				"Third even-indexed nonzero term:                                     5",
				"Third pair:                                  47                      5",
				".                                             *----------------------*",
				"Difference: 47 - 5 =                                     42",
				"42/2 = 21                                     *----------------------*",
				"New third pair:                              21                     21",
				".",
				"So the 75th row",
				"of this triangle is                [38,  38, 21, 0, 3,  3, 0, 0, 0, 21, 0]",
				".",
				"On the other hand, the 75th row of A237593 is [38, 13, 7, 4, 3, 3, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 3, 3, 4, 7, 13, 38], and the 74th row of the same triangle is [38, 13, 6, 5, 3, 2, 2, 1, 2, 1, 1, 1, 1, 2, 1, 2, 2, 3, 5, 6, 13, 38], therefore between both symmetric Dyck paths (described in A237593 and A279387) there are six subparts: [38, 38, 21, 21, 3, 3]. (The diagram of the symmetric representation of sigma(75) is too large to include.) At least in this case the nonzero terms of the 75th row of the triangle coincide with the subparts of the symmetric representation of sigma(75). The ordering of the elements does not matter.",
				"Continuing with the original example, in the 75th row of this triangle we have that the odd-indexed terms, from left to right, together with the even-indexed terms, from right to left, form the finite sequence [38, 21, 3, 0, 0, 0, 21, 0, 3, 0, 38] which is the 75th row of a triangle. At least in this case the nonzero terms coincide with the 75th row of triangle A280851: [38, 21, 3, 21, 3, 38], which lists the six subparts of the symmetric representation of sigma(75) in order of appearance from left to right. - _Omar E. Pol_, Feb 02 2018",
				"In accordance with the conjecture from the Comments section, the finite sequence [38, 21, 3, 0, 0, 0, 21, 0, 3, 0, 38] mentioned above should be the 75th row of triangle A296508. - _Omar E. Pol_, Apr 20 2018"
			],
			"mathematica": [
				"(* functions row[], line[] and their support are defined in A196020 *)",
				"(* maintain a stack of odd indices with nonzero entries for matching *)",
				"a280850[n_] := Module[{a=line[n], r=row[n], stack={1}, i, j, b}, For[i=2, i\u003c=r, i++, If[a[[i]]!=0, If[OddQ[i], AppendTo[stack, i], j=Last[stack]; b=(a[[j]]-a[[i]])/2; a[[i]]=b; a[[j]]=b; stack=Drop[stack, -1]]]]; a]",
				"Flatten[Map[a280850,Range[24]]] (* data *)",
				"TableForm[Map[a280850, Range[28]], TableDepth-\u003e2] (* triangle in Example *)",
				"(* _Hartmut F. W. Hoft_, Jan 31 2018 *)"
			],
			"xref": [
				"Row sums give A000203.",
				"The number of positive terms in row n is A001227(n).",
				"Row n has length A003056(n).",
				"Column k starts in row A000217(k).",
				"Cf. A196020, A235791, A236104, A237048, A237270, A237591, A237593, A239657, A239660, A244050, A245092, A250068, A250070, A261699, A262626, A279387, A279388, A279391, A280851, A296508."
			],
			"keyword": "nonn,tabf",
			"offset": "1,2",
			"author": "_Omar E. Pol_, Jan 09 2017",
			"ext": [
				"Name edited by _Omar E. Pol_, Nov 11 2018"
			],
			"references": 29,
			"revision": 168,
			"time": "2019-06-19T17:56:06-04:00",
			"created": "2017-05-14T00:20:06-04:00"
		}
	]
}