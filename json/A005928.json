{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005928",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5928,
			"id": "M2202",
			"data": "1,-3,0,6,-3,0,0,-6,0,6,0,0,6,-6,0,0,-3,0,0,-6,0,12,0,0,0,-3,0,6,-6,0,0,-6,0,0,0,0,6,-6,0,12,0,0,0,-6,0,0,0,0,6,-9,0,0,-6,0,0,0,0,12,0,0,0,-6,0,12,-3,0,0,-6,0,0,0,0,0,-6,0,6,-6,0,0,-6,0,6,0,0,12,0,0,0,0,0,0,-12,0,12,0,0,0,-6,0,0",
			"name": "G.f.: s(1)^3/s(3), where s(k) = eta(q^k) and eta(q) is Dedekind's function, cf. A010815.",
			"comment": [
				"Unsigned sequence is expansion of theta series of hexagonal net with respect to a node.",
				"Cubic AGM theta functions: a(q) (see A004016), b(q) (this: A005928), c(q) (A005882).",
				"Denoted by a_3(n) in Kassel and Reutenauer 2015. - _Michael Somos_, Jun 04 2015"
			],
			"reference": [
				"N. J. Fine, Basic Hypergeometric Series and Applications, Amer. Math. Soc., 1988; p. 79, Eq. (32.34).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A005928/b005928.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e (terms 0..1000 from T. D. Noe)",
				"J. M. Borwein and P. B. Borwein, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-1991-1010408-0\"\u003eA cubic counterpart of Jacobi's identity and the AGM\u003c/a\u003e, Trans. Amer. Math. Soc., 323 (1991), no. 2, 691-701. MR1010408 (91e:33012) see page 695.",
				"J. M. Borwein, P. B. Borwein and F. Garvan, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-1994-1243610-6\"\u003eSome Cubic Modular Identities of Ramanujan\u003c/a\u003e, Trans. Amer. Math. Soc. 343 (1994), 35-47.",
				"Christian Kassel and Christophe Reutenauer, \u003ca href=\"https://arxiv.org/abs/1505.07229v3\"\u003eThe zeta function of the Hilbert scheme of n points on a two-dimensional torus\u003c/a\u003e, arXiv:1505.07229v3 [math.AG], 2015. [A later version of this paper has a different title and different contents, and the number-theoretical part of the paper was moved to the publication below.]",
				"Christian Kassel and Christophe Reutenauer, \u003ca href=\"https://arxiv.org/abs/1610.07793\"\u003eComplete determination of the zeta function of the Hilbert scheme of n points on a two-dimensional torus\u003c/a\u003e, arXiv:1610.07793 [math.NT], 2016.",
				"N. J. A. Sloane, \u003ca href=\"http://dx.doi.org/10.1063/1.527472\"\u003eTheta series and magic numbers for diamond and certain ionic crystal structures\u003c/a\u003e, J. Math. Phys. 28 (1987), 1653-1657."
			],
			"formula": [
				"a(n) is the coefficient of q^n in b(q)=eta(q)^3/eta(q^3) = (3/2)*a(q^3)-a(q)/2 where a(q)=theta(Hexagonal). - Kok Seng Chua (chuaks(AT)ihpc.nus.edu.sg), May 07 2002",
				"From _Michael Somos_, May 20 2005: (Start)",
				"Euler transform of period 3 sequence [ -3, -3, -2, ...].",
				"a(n) = -3 * b(n) except for a(0) = 1, where b()=A123477() is multiplicative with b(p^e) = -2 if p = 3 and e\u003e0, b(p^e) = e+1 if p == 1 (mod 6), b(p^e) = (1 + (-1)^e)/2 if p == 2, 5 (mod 6).",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2), A(x^4)) where f(u, v, w) = v^3 - 2*u*w^2 + u^2*w.",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2), A(x^3), A(x^6)) where f(u1, u2, u3, u6) = u1^2*u6 - 2*u1*u2*u6 + 4*u2^2*u6 - 3*u2*u3^2.",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2), A(x^3), A(x^6)) where f(u1, u2, u3, u6) = u1*u2*u3 + u1^2*u3 - 3*u1*u6^2 + u2^2*u3. (End)",
				"a(3*n + 2) = 0. a(3*n + 1) = -A005882(n), a(3*n) = A004016(n). - _Michael Somos_, Jul 15 2005",
				"a(n) = -3 * A123477(n) unless n=0. |a(n)| = A113062(n).",
				"Moebius transform is period 9 sequence [-3, 3, 9, -3, 3, -9, -3, 3, 0, ...]. - _Michael Somos_, Dec 25 2007",
				"Expansion of b(q) = a(q^3) - c(q^3) in powers of q where a(), b(), c() are cubic AGM theta functions. - _Michael Somos_, Dec 25 2007",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (3 t)) = 3^(3/2) (t/i) g(t) where q = exp(2 Pi i t) and g() is the g.f. for A033687.",
				"G.f.: exp( sum(n\u003e=1, (sigma(n)-sigma(3*n))*x^n/n ) ). - Joerg Arndt, Jul 30 2011",
				"a(n) = (-1)^(mod(n, 3) = 1) * A113062(n). - _Michael Somos_, Sep 05 2014",
				"a(2*n + 1) = -3 * A123530(n). a(4*n) = a(n). a(4*n + 1) = -3 * A253243(n). a(4*n + 2) = 0. a(4*n + 3) = 6 * A246838(n). a(6*n + 1) = -3 * A097195(n). a(6*n + 3) = 6 * A033762(n). - _Michael Somos_, Jun 04 2015",
				"G.f.: 1 + Sum_{k\u003e0} -3 * x^k / (1 + x^k + x^(2*k)) + 9 * x^(3*k) / (1 + x^(3*k) + x^(6*k)). - _Michael Somos_, Jun 04 2015",
				"a(0) = 1, a(n) = -(3/n)*Sum_{k=1..n} A078708(k)*a(n-k) for n \u003e 0. - _Seiichi Manyama_, Apr 29 2017"
			],
			"example": [
				"G.f. = 1 - 3*q + 6*q^3 - 3*q^4 - 6*q^7 + 6*q^9 + 6*q^12 - 6*q^13 - 3*q^16 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ q]^3 / QPochhammer[ q^3], {q, 0, n}]; (* _Michael Somos_, May 24 2013 *)",
				"a[ n_] := If[ n \u003c 1, Boole[ n==0], -3 Sum[{1, -1, -3, 1, -1, 3, 1, -1, 0}[[ Mod[ d, 9, 1]]], {d, Divisors @ n}]]; (* _Michael Somos_, Sep 23 2013 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A, p, e); if( n\u003c1, n==0, A = factor(n); -3 * prod( k=1, matsize(A)[1], [p, e] = A[k, ]; if( p==3, -2, if( p%6==1, e+1, !(e%2)))))}; \\\\ _Michael Somos_, May 20 2005",
				"(PARI) {a(n) = my(A = x * O(x^n)); polcoeff( eta(x + A)^3 / eta(x^3 + A), n)}; \\\\ _Michael Somos_, May 20 2005",
				"(PARI) {a(n) = if( n\u003c1, n==0, sumdiv(n, d, [0, -3, 3, 9, -3, 3, -9, -3, 3] [d%9 + 1]))}; \\\\ _Michael Somos_, Dec 25 2007",
				"(PARI) N=66; x='x+O('x^N); gf=exp(sum(n=1,N,(sigma(n)-sigma(3*n))*x^n/n));",
				"Vec(gf) \\\\ _Joerg Arndt_, Jul 30 2011",
				"(PARI) lista(nn) = {q='q+O('q^nn); Vec(eta(q)^3/eta(q^3))} \\\\ _Altug Alkan_, Mar 20 2018",
				"(MAGMA) A := Basis( ModularForms( Gamma1(9), 1), 100); A[1] - 3*A[2] + 6*A[4]; \\\\ _Michael Somos_, Jan 31 2015"
			],
			"xref": [
				"Cf. A005882, A033762, A097195, A113062, A123477, A123530, A246838, A253243."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Edited by _M. F. Hasler_, May 07 2018"
			],
			"references": 215,
			"revision": 72,
			"time": "2021-02-22T09:09:51-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}