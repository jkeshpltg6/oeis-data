{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A163974",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 163974,
			"data": "0,1,1,0,1,0,1,0,2,0,3,0,7,0,3,3,11,1,11,2,11,3,37,0,44,18,52,24,103,50,147,52,214,170,475,229,711,375,1116,587,2101,542,3009,1940,4870,1680,8961,5923,16712,4190,24098,11552,42715,11347,69608,32495,103914,50493,189499,103581,304367,152520,453946,203153,783817,246991,1345661",
			"name": "Number of ways to write n as the root-mean-square (RMS) of a set of distinct primes.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A163974/b163974.txt\"\u003eTable of n, a(n) for n = 1..100\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Root-Mean-Square.html\"\u003eRoot-Mean-Square\u003c/a\u003e"
			],
			"example": [
				"a(13) = 7 because 13 is the RMS of 7 sets of distinct primes: 13 = RMS(13) = RMS(7,17) = RMS(5,11,19) = RMS(7,13,17) = RMS(5,11,13,19) = RMS(5,7,11,17,19) = RMS(5,7,11,13,17,19)."
			],
			"maple": [
				"sps:= proc(i) option remember; `if`(i=1, 4, sps(i-1) +ithprime(i)^2) end: b:= proc(n, i, t) if n\u003c0 then 0 elif n=0 then `if`(t=0, 1, 0) elif i=2 then `if`(n=4 and t=1, 1, 0) else b(n, i, t):= b(n, prevprime(i), t) +b(n-i^2, prevprime(i), t-1) fi end: a:= proc(n) option remember; local s, k; s:= `if`(isprime(n), 1, 0); for k from 2 while sps(k)\u003c=k*n^2 do s:= s +b(k*n^2, nextprime(floor(sqrt(k*n^2 -sps(k-1)))-1), k) od; s end: seq(a(n), n=1..30);"
			],
			"mathematica": [
				"sps[i_] := sps[i] = If[i == 1, 4, sps[i - 1] + Prime[i]^2]; b[n_, i_, t_] := b[n, i, t] = If[ n \u003c 0 , 0 , If[ n == 0 , If[t == 0, 1, 0], If[ i == 2 , If[n == 4 \u0026\u0026 t == 1, 1, 0], b[n, NextPrime[i, -1], t] + b[n - i^2, NextPrime[i, -1], t - 1]]]]; a[n_] := a[n] = (s = Boole[PrimeQ[n]]; For[k = 2, sps[k] \u003c= k*n^2, k++, s = s + b[k*n^2, NextPrime[ Floor[ Sqrt[k*n^2 - sps[k - 1]]] - 1], k]]; s); Table[ Print[a[n]]; a[n], {n, 1, 58}] (* _Jean-François Alcover_, Jul 11 2012, translated from Maple *)"
			],
			"program": [
				"(Haskell)",
				"a163974 n = f a000040_list 1 nn 0 where",
				"   f (p:ps) l nl xx",
				"     | yy \u003e nl   = 0",
				"     | yy \u003c nl   = f ps (l + 1) (nl + nn) yy + f ps l nl xx",
				"     | otherwise = if w == n then 1 else 0",
				"     where w = if r == 0 then a000196 m else 0",
				"           (m, r) = divMod yy l",
				"           yy = xx + p * p",
				"   nn = n ^ 2",
				"-- _Reinhard Zumkeller_, Feb 13 2013"
			],
			"xref": [
				"Cf. A072701.",
				"Cf. A000196, A000040, A164283, A211868."
			],
			"keyword": "nice,nonn",
			"offset": "1,9",
			"author": "_Alois P. Heinz_, Aug 07 2009",
			"ext": [
				"Terms a(59)-a(67) by _Reinhard Zumkeller_, Feb 13 2013"
			],
			"references": 5,
			"revision": 14,
			"time": "2018-03-09T12:10:05-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}