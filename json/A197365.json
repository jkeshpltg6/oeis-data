{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A197365",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 197365,
			"data": "1,1,2,5,1,16,8,63,54,3,296,368,56,1623,2649,753,15,10176,20544,9024,576,71793,172596,104814,13572,105,562848,1569408,1228608,259968,7968,4853949,15398829,14824314,4532034,306729,945,45664896,162412416,185991936,75929856",
			"name": "T(n,k) gives the number of permutations of the set [n] that contain k occurrences of the subword (132); irregular array read by rows (n \u003e= 0 and 0 \u003c= k \u003c= max(0, floor((n-1)/2))).",
			"comment": [
				"A permutation p(1)p(2)...p(n) in the symmetric group S_n contains the subword (132) if there are 3 consecutive elements p(i)p(j)p(k) that have the same order relations as (132), that is, p(i) \u003c p(j) \u003e p(k) and p(i) \u003c p(k). For the enumeration of permutations containing the subword (123) see A162975.",
				"From _Petros Hadjicostas_, Nov 05 2019: (Start)",
				"The attached Maple program gives a recurrence for the o.g.f. of each row in terms of u for T(n,k), the number of permutations of [n] containing exactly k occurrences of the consecutive pattern 123...(r+1)(r+3)(r+2) for r \u003e= 0. In the program, t = r + 2. Here, n \u003e= 0 and 0 \u003c= k \u003c= max(0, (n-1)/t).",
				"Using that recurrence we may get any row or column from the irregular triangular array T(n, k) for any r \u003e= 0. (Here r = 0, while in array A264781 we have r = 2.)",
				"The recurrence follows from manipulation of the bivariate o.g.f/e.g.f. 1/W(u,z) = Sum_{n, k \u003e= 0} T(n, k)*u^k*z^n/n!, whose reciprocal W(u,z) is the solution of the o.d.e. in Theorem 3.2 in Elizalde and Noy (2003) (with m = a = r + 1). The number t = r + 2 is the order of the o.d.e. in terms of the variable z.",
				"(End)"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A197365/b197365.txt\"\u003eRows n = 0..100, flattened\u003c/a\u003e",
				"A. Baxter, B. Nakamura, and D. Zeilberger, \u003ca href=\"http://www.math.rutgers.edu/~zeilberg/mamarim/mamarimhtml/auto.html\"\u003eAutomatic generation of theorems and proofs on enumerating consecutive Wilf-classes\u003c/a\u003e, 2011.",
				"S. Elizalde and M. Noy, \u003ca href=\"http://dx.doi.org/10.1016/S0196-8858(02)00527-4\"\u003eConsecutive patterns in permutations\u003c/a\u003e, Adv. Appl. Math. 30 (2003), 110-125.",
				"Petros Hadjicostas, \u003ca href=\"/A197365/a197365.txt\"\u003eGeneral Maple program for a recurrence for the number of permutations of [n] containing exactly k times the consecutive pattern 123...(r+1)(r+3)(r+2) for r \u003e= 0\u003c/a\u003e. [Here r = 0 and in the program t = r + 2 = 2.]"
			],
			"formula": [
				"E.g.f.: 1/(1 - int_{t = 0..z} exp((u-1)*t^2/2!)) = sqrt(1 - u)/(sqrt(1 - u) -sqrt(Pi/2) * erf(z/2*sqrt(1 - u))) = 1 + z + 2*z^2/2! + (5 + u)*z^3/3! + (16 + 8*u)*z^4/4! + ....",
				"n-th row sum = n!. First column is A111004."
			],
			"example": [
				"Table begins",
				".n\\k.|......0......1.....2......3",
				"= = = = = = = = = = = = = = = = =",
				"..0..|......1",
				"..1..|......1",
				"..2..|......2",
				"..3..|......5......1",
				"..4..|.....16......8",
				"..5..|.....63.....54.....3",
				"..6..|....296....368....56",
				"..7..|...1623...2649...753....15",
				"..8..|..10176..20544..9024...576",
				"...",
				"T(4,0) = 16: The 16 permutations of S_4 not containing the subword (132) are (1234), (2134), (2314), (3124), (3214), (1342), (2341), (3241), (2413), (3412), (3421), (4123), (4213), (4231), (4312), (4321).",
				"T(4,1) = 8: The 8 permutations of S_4 with 1 occurrence of the subword (132) are 1243, 1324, 1423, 1432, 2143, 2431, 3142, 4132."
			],
			"maple": [
				"b:= proc(u, o, t) option remember; `if`(u+o=0, 1, expand(",
				"      add(b(u-j, o+j-1, 0)*`if`(j\u003ct, x, 1), j=1..u)+",
				"      add(b(u+j-1, o-j, j), j=1..o)))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=0..degree(p)))(b(n, 0$2)):",
				"seq(T(n), n=0..14);  # _Alois P. Heinz_, Oct 30 2013"
			],
			"mathematica": [
				"b[u_, o_, t_] := b[u, o, t] = If[u+o == 0, 1, Expand[Sum[b[u-j, o+j-1, 0]*If[j\u003ct, x, 1], {j, 1, u}] + Sum[b[u+j-1, o-j, j], {j, 1, o}]]]; T[n_] := Function[{p}, Table[Coefficient[p, x, i], {i, 0, Exponent[p, x]}]][b[n, 0, 0]]; Table[T[n], {n, 0, 14}] // Flatten (* _Jean-François Alcover_, Mar 05 2015, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Columns k=0-10 give: A111004, A263885, A263886, A263887, A263888, A263889, A263890, A263891, A263892, A263893, A263894.",
				"T(2n+1,n) gives A001147.",
				"T(2n+2,n) gives 2*A076729.",
				"Cf. A162975, A264781 (pattern 12354)."
			],
			"keyword": "nonn,easy,tabf",
			"offset": "0,3",
			"author": "_Peter Bala_, Oct 14 2011",
			"references": 13,
			"revision": 45,
			"time": "2019-11-06T03:35:27-05:00",
			"created": "2011-10-14T12:59:08-04:00"
		}
	]
}