{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A349661",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 349661,
			"data": "1,3,3,2,4,5,2,2,4,5,6,4,3,6,4,1,6,7,4,6,8,5,1,4,6,10,10,3,6,10,2,3,8,6,10,10,5,6,4,5,12,14,6,5,9,6,2,3,6,12,14,7,5,8,2,7,14,6,9,9,5,9,4,2,10,15,7,7,8,7,3,5,5,7,14,5,9,9,1,4,11,8,11,13,7,13,7,2,11,17,12,8,5,6,7,5,7,11,12,8",
			"name": "Number of ways to write n as x^4 + y^2 + (z^2 + 4^w)/2 with x,y,z,w nonnegative integers.",
			"comment": [
				"Conjecture: a(n) \u003e 0 for all n \u003e 0.",
				"This has been verified for n up to 10^6.",
				"As (x^2 + y^2)/2 = ((x+y)/2)^2 + ((x-y)/2)^2, the conjecture gives a new refinement of Lagrange's four-square theorem.",
				"See also A350012 for a similar conjecture."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A349661/b349661.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2016.11.008\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, J. Number Theory 175(2017), 167--190.",
				"Zhi-Wei Sun, \u003ca href=\"https://doi.org/10.1142/S1793042119501045\"\u003eRestricted sums of four squares\u003c/a\u003e, Int. J. Number Theory 15(2019), 1863-1893.  See also \u003ca href=\"http://arxiv.org/abs/1701.05868\"\u003earXiv:1701.05868 [math.NT]\u003c/a\u003e.",
				"Zhi-Wei Sun, \u003ca href=\"http://hitpress.hit.edu.cn/2021/1015/c12593a261001/page.htm\"\u003eNew Conjectures in Number Theory and Combinatorics\u003c/a\u003e (in Chinese), Harbin Institute of Technology Press, 2021."
			],
			"example": [
				"a(1) = 1 with 1 = 0^4 + 0^2 + (1^2 + 4^0)/2.",
				"a(23) = 1 with 23 = 1^4 + 3^2 + (5^2 + 4^0)/2.",
				"a(79) = 1 with 79 = 1^4 + 2^2 + (12^2 + 4^1)/2.",
				"a(1199) = 1 with 1199 = 5^4 + 18^2 + (22^2 + 4^2)/2.",
				"a(3679) = 1 with 3679 = 5^4 + 2^2 + (78^2 + 4^2)/2.",
				"a(6079) = 1 with 6079 = 3^4 + 42^2 + (92^2 + 4^1)/2.",
				"a(33439) = 1 with 33439 = 1^4 + 175^2 + (75^2 + 4^0)/2.",
				"a(50399) = 1 with 50399 = 13^4 + 135^2 + (85^2 + 4^0)/2.",
				"a(207439) = 1 with 207439 = 1^4 + 142^2 + (612^2 + 4^1)/2."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]];",
				"tab={};Do[r=0;Do[If[SQ[2(n-x^4-y^2)-4^z],r=r+1],{x,0,(n-1)^(1/4)},{y,0,Sqrt[n-1-x^4]},{z,0,Log[4,2(n-x^4-y^2)]}];tab=Append[tab,r],{n,1,100}];Print[tab]"
			],
			"xref": [
				"Cf. A000118, A000290, A000302, A000583, A349957, A349992, A350012."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Zhi-Wei Sun_, Dec 08 2021",
			"references": 4,
			"revision": 66,
			"time": "2021-12-09T08:15:40-05:00",
			"created": "2021-12-09T01:07:09-05:00"
		}
	]
}