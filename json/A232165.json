{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A232165",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 232165,
			"data": "0,1,2,3,8,18,37,82,181,392,856,1873,4086,8919,19480,42530,92853,202742,442665,966496,2110240,4607473,10059866,21964555,47957080,104708706,228619317,499163818,1089866333,2379596808,5195573912,11343933537,24768164206,54078416287",
			"name": "Cardinality of the Weyl alternation set corresponding to the zero-weight in the adjoint representation of the Lie algebra sp(2n).",
			"comment": [
				"Number of Weyl group elements contributing nonzero terms to Kostant's weight multiplicity formula when computing the multiplicity of the zero-weight in the adjoint representation for the Lie algebra of type C and rank n."
			],
			"reference": [
				"P. E. Harris, Combinatorial problems related to Kostant's weight multiplicity formula, PhD Dissertation, University of Wisconsin-Milwaukee, 2012."
			],
			"link": [
				"P. E. Harris, E. Insko, L. K. Williams, \u003ca href=\"http://arxiv.org/abs/1401.0055\"\u003eThe adjoint representation of a Lie algebra and the support of Kostant's weight multiplicity formula\u003c/a\u003e, arXiv preprint arXiv:1401.0055, 2013",
				"B. Kostant, \u003ca href=\"http://www.ncbi.nlm.nih.gov/pmc/articles/PMC528626/\"\u003eA Formula for the Multiplicity of a Weight\u003c/a\u003e, Proc Natl Acad Sci U S A. 1958 June; 44(6): 588-589.",
				"\u003ca href=\"/index/Rec\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,1,3,1)."
			],
			"formula": [
				"a(n) = A232164(n) + A232164(n-1).",
				"a(n) = a(n-1)+a(n-2)+3*a(n-3)+a(n-4). G.f.: -x*(x+1) / (x^4+3*x^3+x^2+x-1). - _Colin Barker_, Jan 01 2014"
			],
			"example": [
				"For n=3, a(3) = A232164(3) + A232164(2) = 2+1 = 3."
			],
			"maple": [
				"r:=proc(n::nonnegint)",
				"if n=0 then return 0:",
				"elif n=1 then return 1:",
				"elif n=2 then return 1:",
				"elif n=3 then return 2:",
				"else return",
				"r(n-1)+r(n-2)+3*r(n-3)+r(n-4):",
				"end if;",
				"end proc:",
				"a:=proc(n::nonnegint)",
				"if n=0 then return 0:",
				"elif n=1 then return 1:",
				"else return",
				"r(n)+r(n-1):",
				"end if;",
				"end proc:"
			],
			"mathematica": [
				"LinearRecurrence[{1,1,3,1},{0,1,2,3},40] (* _Harvey P. Dale_, Nov 22 2014 *)"
			],
			"program": [
				"(PARI) Vec(-x*(x+1)/(x^4+3*x^3+x^2+x-1) + O(x^100)) \\\\ _Colin Barker_, Jan 01 2014"
			],
			"xref": [
				"Cf. A232164."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Pamela E Harris_, Nov 19 2013",
			"references": 2,
			"revision": 26,
			"time": "2015-06-13T00:54:54-04:00",
			"created": "2013-11-21T12:15:27-05:00"
		}
	]
}