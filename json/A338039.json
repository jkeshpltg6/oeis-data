{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A338039",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 338039,
			"data": "18,81,198,576,675,819,891,918,1131,1304,1311,1818,1998,2262,2622,3393,3933,4031,4154,4514,4636,6364,8181,8749,8991,9478,12441,14269,14344,14421,15167,15602,16237,18018,18449,18977,19998,20651,23843,24882,26677,26892,27225",
			"name": "Numbers m such that A338038(m) = A338038(A004086(m)) where A004086(i) is i read backwards and A338038(i) is the sum of the primes and exponents in the prime factorization of i ignoring 1-exponents; palindromes and multiples of 10 are excluded.",
			"comment": [
				"Palindromes (A002113) are excluded from the sequence because they obviously satisfy the condition.",
				"Sequence is infinite since it includes 18, 1818, 181818, .... See link.",
				"There are many cases of terms that are the repeated concatenation of integers like: 1818, 8181, 181818, ... , but also 131313131313131313131313131313 and more. See A338166.",
				"If n is in the sequence and has d digits, and gcd(n, x) = gcd(A004086(n), x) where x = (10^((k+1)*d)-1)/(10^d-1), then the concatenation of k copies of n is also in the sequence. - _Robert Israel_, Oct 13 2020"
			],
			"link": [
				"Michel Marcus, \u003ca href=\"/A338039/b338039.txt\"\u003eTable of n, a(n) for n = 1..2998\u003c/a\u003e",
				"Daniel Tsai, \u003ca href=\"https://arxiv.org/abs/2010.03151\"\u003eA recurring pattern in natural numbers of a certain property\u003c/a\u003e, arXiv:2010.03151 [math.NT], 2020.",
				"Daniel Tsai, \u003ca href=\"http://math.colgate.edu/~integers/v32/v32.mail.html\"\u003eA recurring pattern in natural numbers of a certain property\u003c/a\u003e, Integers (2021) Vol. 21, Article #A32.",
				"Daniel Tsai, \u003ca href=\"https://arxiv.org/abs/2111.10211\"\u003ev-palindromes: an analogy to the palindromes\u003c/a\u003e, arXiv:2111.10211 [math.HO], 2021."
			],
			"example": [
				"For m = 18 = 2*3^2, A338038(18) = 2 + (3+2) = 7 and for m = 81 = 3^4, A338038(81) = 7, so 18 and 81 are terms."
			],
			"maple": [
				"rev:= proc(n) local L,i;",
				"  L:= convert(n,base,10);",
				"  add(L[-i]*10^(i-1),i=1..nops(L))",
				"end proc:",
				"g:= proc(n) local t;",
				"  add(t[1]+t[2],t=subs(1=0,ifactors(n)[2]))",
				"end proc:",
				"filter:= proc(n) local r;",
				"  if n mod 10 = 0 then return false fi;",
				"  r:= rev(n);",
				"  r \u003c\u003e n and g(r)=g(n)",
				"end proc:",
				"select(filter, [$1..30000]); # _Robert Israel_, Oct 13 2020"
			],
			"mathematica": [
				"s[1] = 0; s[n_] := Plus @@ First /@ (f = FactorInteger[n]) + Plus @@ Select[Last /@ f, # \u003e 1 \u0026]; Select[Range[30000], !Divisible[#, 10] \u0026\u0026 (r = IntegerReverse[#]) != # \u0026\u0026  s[#] == s[r] \u0026] (* _Amiram Eldar_, Oct 08 2020 *)"
			],
			"program": [
				"(PARI) f(n) = my(f=factor(n)); vecsum(f[,1]) + sum(k=1, #f~, if (f[k,2]!=1, f[k,2])); \\\\ A338038",
				"isok(m) = my(r=fromdigits(Vecrev(digits(m)))); (m % 10) \u0026\u0026 (m != r) \u0026\u0026 (f(r) == f(m));"
			],
			"xref": [
				"Cf. A004086 (read n backwards), A002113, A029742 (non-palindromes), A338038, A338166."
			],
			"keyword": "nonn,base",
			"offset": "1,1",
			"author": "_Michel Marcus_, Oct 08 2020",
			"references": 5,
			"revision": 40,
			"time": "2021-11-23T09:43:18-05:00",
			"created": "2020-10-09T03:47:47-04:00"
		}
	]
}