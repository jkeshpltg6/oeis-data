{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A343936",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 343936,
			"data": "1,2,3,10,5,56,7,120,45,220,11,4368,13,560,680,3876,17,26334,19,42504,1771,2024,23,2035800,325,3276,3654,201376,29,8347680,31,376992,6545,7140,7770,145008513,37,9880,10660,53524680,41,73629072,43,1712304,1906884",
			"name": "Number of ways to choose a multiset of n divisors of n - 1.",
			"formula": [
				"a(n) = ((sigma(n - 1), n)) = binomial(sigma(n - 1) + n - 1, n) where sigma = A000005 and binomial = A007318."
			],
			"example": [
				"The a(1) = 1 through a(5) = 5 multisets:",
				"  {}  {1}  {1,1}  {1,1,1}  {1,1,1,1}",
				"      {2}  {1,3}  {1,1,2}  {1,1,1,5}",
				"           {3,3}  {1,1,4}  {1,1,5,5}",
				"                  {1,2,2}  {1,5,5,5}",
				"                  {1,2,4}  {5,5,5,5}",
				"                  {1,4,4}",
				"                  {2,2,2}",
				"                  {2,2,4}",
				"                  {2,4,4}",
				"                  {4,4,4}",
				"The a(6) = 56 multisets:",
				"  11111  11136  11333  12236  13366  22266  23666",
				"  11112  11166  11336  12266  13666  22333  26666",
				"  11113  11222  11366  12333  16666  22336  33333",
				"  11116  11223  11666  12336  22222  22366  33336",
				"  11122  11226  12222  12366  22223  22666  33366",
				"  11123  11233  12223  12666  22226  23333  33666",
				"  11126  11236  12226  13333  22233  23336  36666",
				"  11133  11266  12233  13336  22236  23366  66666"
			],
			"mathematica": [
				"multchoo[n_,k_]:=Binomial[n+k-1,k];",
				"Table[multchoo[DivisorSigma[0,n],n-1],{n,50}]"
			],
			"xref": [
				"The version for chains of divisors is A163767.",
				"Diagonal n = k + 1 of A343658.",
				"Choosing n divisors of n gives A343935.",
				"A000005 counts divisors.",
				"A000312 = n^n.",
				"A007318 counts k-sets of elements of {1..n}.",
				"A009998 = n^k (as an array, offset 1).",
				"A059481 counts k-multisets of elements of {1..n}.",
				"A146291 counts divisors of n with k prime factors (with multiplicity).",
				"A253249 counts nonempty chains of divisors of n.",
				"Strict chains of divisors:",
				"- A067824 counts strict chains of divisors starting with n.",
				"- A074206 counts strict chains of divisors from n to 1.",
				"- A251683 counts strict length k + 1 chains of divisors from n to 1.",
				"- A334996 counts strict length-k chains of divisors from n to 1.",
				"- A337255 counts strict length-k chains of divisors starting with n.",
				"- A337256 counts strict chains of divisors of n.",
				"- A343662 counts strict length-k chains of divisors.",
				"Cf. A062319, A143773, A163767, A176029, A327527, A334997, A343656, A343661."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Gus Wiseman_, May 05 2021",
			"references": 1,
			"revision": 8,
			"time": "2021-05-06T21:24:33-04:00",
			"created": "2021-05-06T21:24:33-04:00"
		}
	]
}