{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A172369",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 172369,
			"data": "1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,4,4,4,4,1,1,7,28,28,28,7,1,1,10,70,280,280,70,10,1,1,13,130,910,3640,910,130,13,1,1,25,325,3250,22750,22750,3250,325,25,1,1,46,1150,14950,149500,261625,149500,14950,1150,46,1",
			"name": "Triangle read by rows: T(n,k,q) = round(c(n)/(c(k)*c(n-k))) where c are partial products of a sequence defined in comments.",
			"comment": [
				"Start from A143454 and its partial products c(n) = 1, 1, 1, 1, 1, 4, 28, 280, 3640, 91000, 4186000, ... . Then T(n,k) = round(c(n)/(c(k)*c(n-k)))."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A172369/b172369.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k, q) = round( c(n,q)/(c(k,q)*c(n-k,q)) ), where c(n, q) = Product_{j=1..n} f(j, q), f(n, q) = f(n-1, q) + q*f(n-4, q), f(0, q) = 0, f(1, q) = f(2, q) = f(3, q) = 1, and q = 3. - _G. C. Greubel_, May 08 2021"
			],
			"example": [
				"Triangle begins as:",
				"  1;",
				"  1,  1;",
				"  1,  1,    1;",
				"  1,  1,    1,     1;",
				"  1,  1,    1,     1,      1;",
				"  1,  4,    4,     4,      4,      1;",
				"  1,  7,   28,    28,     28,      7,      1;",
				"  1, 10,   70,   280,    280,     70,     10,     1;",
				"  1, 13,  130,   910,   3640,    910,    130,    13,    1;",
				"  1, 25,  325,  3250,  22750,  22750,   3250,   325,   25,  1;",
				"  1, 46, 1150, 14950, 149500, 261625, 149500, 14950, 1150, 46, 1;"
			],
			"mathematica": [
				"f[n_, q_]:= f[n, q]= If[n==0, 0, If[n\u003c4, 1, f[n-1, q] +q*f[n-4, q]]];",
				"c[n_, q_]:= Product[f[j, q], {j,n}];",
				"T[n_, k_, q_]:= Round[c[n, q]/(c[k, q]*c[n - k, q])];",
				"Table[T[n, k, 3], {n, 0, 12}, {k, 0, n}]//Flatten (* modified by _G. C. Greubel_, May 08 2021 *)"
			],
			"program": [
				"(Sage)",
				"@CachedFunction",
				"def f(n,q): return 0 if (n==0) else 1 if (n\u003c4) else f(n-1, q) + q*f(n-4, q)",
				"def c(n,q): return product( f(j,q) for j in (1..n) )",
				"def T(n,k,q): round(return c(n, q)/(c(k, q)*c(n-k, q)))",
				"flatten([[T(n,k,3) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, May 08 2021"
			],
			"xref": [
				"Cf. A172363 (q=1), A172368 (q=2), this sequence (q=3), A318774."
			],
			"keyword": "nonn,tabl,less",
			"offset": "0,17",
			"author": "_Roger L. Bagula_, Feb 01 2010",
			"ext": [
				"Definition corrected to give integral terms, _G. C. Greubel_, May 08 2021"
			],
			"references": 2,
			"revision": 20,
			"time": "2021-05-09T09:52:41-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}