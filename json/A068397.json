{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A068397",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 68397,
			"data": "1,5,4,9,11,20,29,49,76,125,199,324,521,845,1364,2209,3571,5780,9349,15129,24476,39605,64079,103684,167761,271445,439204,710649,1149851,1860500,3010349,4870849,7881196,12752045,20633239,33385284,54018521,87403805",
			"name": "a(n) = Lucas(n) + (-1)^n + 1.",
			"comment": [
				"Number of domino tilings of a 2 X n strip on a cylinder.",
				"Number of domino tilings of a 2 X n rectangle = Fibonacci(n) - see A000045.",
				"Number of perfect matchings in the C_n X P_2 graph (C_n is the cycle graph on n vertices and P_2 is the path graph on 2 vertices). - _Emeric Deutsch_, Dec 29 2004",
				"For n \u003e= 3, also the number of maximum independent edge sets (matchings) in the n-prism graph. - _Eric W. Weisstein_, Mar 30 2017",
				"For n \u003e= 4, also the number of minimum clique coverings in the n-prism graph. - _Eric W. Weisstein_, May 03 2017"
			],
			"reference": [
				"S.-M. Belcastro, Tilings of 2 x n Grids on Surfaces, preprint. [Unpublished as of June 2016]"
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A068397/b068397.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e (corrected by Michel Marcus, Jan 19 2019)",
				"Cate S. Anstöter, Nino Bašić, Patrick W. Fowler, and Tomaž Pisanski, \u003ca href=\"https://arxiv.org/abs/2104.13290\"\u003eCatacondensed Chemical Hexagonal Complexes: A Natural Generalisation of Benzenoids\u003c/a\u003e, arXiv:2104.13290 [physics.chem-ph], 2021.",
				"M. Baake, J. Hermisson, and P. Pleasants, \u003ca href=\"http://dx.doi.org/10.1088/0305-4470/30/9/016\"\u003eThe torus parametrization of quasiperiodic LI-classes\u003c/a\u003e J. Phys. A 30 (1997), no. 9, 3029-3056. See Table 3.",
				"H. Hosoya and F. Harary, \u003ca href=\"http://dx.doi.org/10.1007/BF01164636\"\u003eOn the matching properties of three fence graphs\u003c/a\u003e, J. Math. Chem., 12(1993), 211-218.",
				"H. Hosoya and A. Motoyama, \u003ca href=\"http://dx.doi.org/10.1063/1.526778\"\u003eAn effective algorithm for obtaining polynomials for dimer statistics. Application of operator technique on the topological index to two- and three-dimensional rectangular and torus lattices\u003c/a\u003e, J. Math. Physics 26 (1985) 157-167 (eq. (21) and Table IV).",
				"B. Myers, \u003ca href=\"http://doi.org/10.1109/TCT.1971.1083273\"\u003eNumber of spanning trees in a wheel\u003c/a\u003e, IEE Trans. Circuit Theo. 18 (2) (1971) 280-282, Table 1.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CliqueCovering.html\"\u003eClique Covering\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Matching.html\"\u003eMatching\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/MaximumIndependentEdgeSet.html\"\u003eMaximum Independent Edge Set\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/MinimumEdgeCover.html\"\u003eMinimum Edge Cover\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PrismGraph.html\"\u003ePrism Graph\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,2,-1,-1)."
			],
			"formula": [
				"a(n) = F(n+1) + F(n-1) + 2 if n is even, a(n) = F(n+1) + F(n-1) if n is odd, where F(n) is the n-th Fibonacci number - sequence A000045.",
				"a(n) = 1 + (-1)^n + ((1 + sqrt(5))/2)^n + ((1 - sqrt(5))/2)^n = 1 + (-1)^n + A000032(n).  - _Vladeta Jovovic_, Apr 08 2002",
				"Recurrence: a(n) = a(n-1) + 2*a(n-2) - a(n-3) - a(n-4).  - _Vladeta Jovovic_, Apr 08 2002",
				"a(n+2) = a(n+1) + a(n) if n even, a(n+2) = a(n+1) + a(n) + 2 if n odd. - _Michael Somos_, Jan 28 2017",
				"a(1) = 1, a(2) = 5; a(n) = a(n - 1) + a(n - 2) - 2 Mod[n, 2]. (From Belcastro)",
				"G.f.: x*(1 + 4*x - 3*x^2 - 4*x^3)/(1 - x - 2*x^2 + x^3 + x^4). - _Vladeta Jovovic_, Apr 08 2002",
				"a(n) = ((1 + Sqrt(5))/2)^n + ((1 - Sqrt(5))/2)^n + 1 + (-1)^n. (from Hosoya/Harary)",
				"E.g.f.: exp(-x/phi) + exp(phi*x) + 2*cosh(x) - 4, where phi is the golden ratio. - _Ilya Gutkovskiy_, Jun 16 2016"
			],
			"example": [
				"G.f. = 5*x^2 + 4*x^3 + 9*x^4 + 11*x^5 + 20*x^6 + 29*x^7 + 49*x^8 + 76*x^9 + ...",
				"Example: a(3)=4 because in the graph with vertex set {A,B,C,A',B',C'} and edge set {AB,AC,BC, A'B',A'C',B'C',AA',BB',CC'} we have the following perfect matchings: {AA',BC,B'C'}, {BB',AC,A'C'}, {CC',AB,A'B'} and {AA',BB',CC'}."
			],
			"maple": [
				"a[2]:=5: a[3]:=4: a[4]:=9: a[5]:=11: for n from 6 to 45 do a[n]:=a[n-1]+2*a[n-2]-a[n-3]-a[n-4] od:seq(a[n],n=2..40); # _Emeric Deutsch_, Dec 29 2004",
				"f:= n -\u003e combinat:-fibonacci(n-1)+combinat:-fibonacci(n+1)+(-1)^n+1:",
				"map(f, [$1..50]); # _Robert Israel_, May 03 2017"
			],
			"mathematica": [
				"Table[LucasL[n] + (-1)^n + 1, {n, 1, 38}] (* _Jean-François Alcover_, Sep 01 2011 *)",
				"LucasL[#] + (-1)^# + 1 \u0026[Range[38]] (* _Eric W. Weisstein_, May 03 2017 *)",
				"LinearRecurrence[{1, 2, -1, -1}, {1, 5, 4, 9}, 20] (* _Eric W. Weisstein_, Dec 31 2017 *)",
				"CoefficientList[Series[(1 + 4 x - 3 x^2 - 4 x^3)/(1 - x - 2 x^2 + x^3 + x^4), {x, 0, 20}], x]"
			],
			"program": [
				"(PARI) a(n)=([0,1,0,0; 0,0,1,0; 0,0,0,1; -1,-1,2,1]^(n-1)*[1;5;4;9])[1,1] \\\\ _Charles R Greathouse IV_, Jun 19 2016",
				"(PARI) Vec(x*(1+4*x-3*x^2-4*x^3)/(1-x-2*x^2+x^3+x^4) + O(x^40)) \\\\ _Colin Barker_, Jan 28 2017; _Michel Marcus_, Jan 19 2019"
			],
			"xref": [
				"Cf. A000032, A000045.",
				"Cf. also A102079, A102091, A252054.",
				"a(n) = A102079(n, n)."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "Sharon Sela (sharonsela(AT)hotmail.com), Mar 30 2002",
			"ext": [
				"More terms from _Vladeta Jovovic_, Apr 08 2002",
				"Two initial terms added, third comment amended to be consonant with new initial terms, offset changed to be consonant with initial terms, two references added, two formulas added. - Sarah-Marie Belcastro, Jul 04 2009",
				"Edited by _N. J. A. Sloane_, Jan 10 2018 to incorporate information from a duplicate (but now dead) entry."
			],
			"references": 12,
			"revision": 81,
			"time": "2021-07-28T13:42:32-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}