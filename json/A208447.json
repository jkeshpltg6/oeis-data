{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A208447",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 208447,
			"data": "1,1,1,1,1,2,1,1,2,3,1,1,2,4,5,1,1,2,6,10,7,1,1,2,10,24,26,11,1,1,2,18,64,120,76,15,1,1,2,34,180,596,720,232,22,1,1,2,66,520,3060,8056,5040,764,30,1,1,2,130,1524,16076,101160,130432,40320,2620,42",
			"name": "Sum of the k-th powers of the numbers of standard Young tableaux over all partitions of n; square array A(n,k), n\u003e=0, k\u003e=0, read by antidiagonals.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A208447/b208447.txt\"\u003eAntidiagonals n = 0..50\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Young_tableau\"\u003eYoung tableau\u003c/a\u003e"
			],
			"example": [
				"A(3,2) = 1^2 + 2^2 + 1^2 = 6 = 3! because 3 has partitions 111, 21, 3 with 1, 2, 1 standard Young tableaux, respectively:",
				"  .111.    . 21 . . . . . . .    . 3 . . . .",
				"  +---+    +------+  +------+    +---------+",
				"  | 1 |    | 1  2 |  | 1  3 |    | 1  2  3 |",
				"  | 2 |    | 3 .--+  | 2 .--+    +---------+",
				"  | 3 |    +---+     +---+",
				"  +---+",
				"Square array A(n,k) begins:",
				"   1,  1,   1,    1,      1,       1,        1, ...",
				"   1,  1,   1,    1,      1,       1,        1, ...",
				"   2,  2,   2,    2,      2,       2,        2, ...",
				"   3,  4,   6,   10,     18,      34,       66, ...",
				"   5, 10,  24,   64,    180,     520,     1524, ...",
				"   7, 26, 120,  596,   3060,   16076,    86100, ...",
				"  11, 76, 720, 8056, 101160, 1379176, 19902600, ..."
			],
			"maple": [
				"h:= proc(l) local n; n:=nops(l); add(i, i=l)! /mul(mul(1+l[i]-j",
				"       +add(`if`(l[k]\u003e=j, 1, 0), k=i+1..n), j=1..l[i]), i=1..n)",
				"    end:",
				"g:= proc(n, i, k, l) `if`(n=0, h(l)^k, `if`(i\u003c1, 0, g(n, i-1, k, l)",
				"       + `if`(i\u003en, 0, g(n-i, i, k, [l[], i]))))",
				"    end:",
				"A:= (n, k)-\u003e `if`(n=0, 1, g(n, n, k, [])):",
				"seq(seq(A(n, d-n), n=0..d), d=0..10);"
			],
			"mathematica": [
				"h[l_] := With[{n = Length[l]}, Sum[i, {i, l}]! / Product[Product[1 + l[[i]] - j + Sum [If[l[[k]] \u003e= j, 1, 0], { k, i+1, n}], {j, 1, l[[i]]}], {i, 1, n}]]; g[n_, i_, k_, l_] := If[n == 0, h[l]^k, If[i \u003c 1, 0, g[n, i-1, k, l] + If[i \u003e n, 0, g[n-i, i, k, Append[l, i]]]]]; a [n_, k_] := If[n == 0, 1, g[n, n, k, {}]]; Table[Table[a[n, d-n], {n, 0, d}], {d, 0, 10}] // Flatten (* _Jean-François Alcover_, Dec 11 2013, translated from Maple *)"
			],
			"xref": [
				"Columns 0-10 give: A000041, A000085, A000142, A130721, A129627, A218432, A218433, A218434, A218435, A218436, A218437.",
				"Rows 0+1, 2, 3 give: A000012, A007395, A052548.",
				"Main diagonal gives A319607."
			],
			"keyword": "nonn,tabl",
			"offset": "0,6",
			"author": "_Alois P. Heinz_, Feb 26 2012",
			"references": 18,
			"revision": 32,
			"time": "2018-09-24T14:09:07-04:00",
			"created": "2012-02-28T09:00:04-05:00"
		}
	]
}