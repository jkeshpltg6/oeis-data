{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A213275",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 213275,
			"data": "1,1,1,1,1,1,1,2,1,1,1,6,3,1,1,1,24,15,7,1,1,1,120,105,106,19,1,1,1,720,945,2575,1075,56,1,1,1,5040,10395,87595,115955,13326,174,1,1,1,40320,135135,3864040,19558470,7364321,188196,561,1,1",
			"name": "Number A(n,k) of words w where each letter of the k-ary alphabet occurs n times and for every prefix z of w we have #(z,a_i) = 0 or #(z,a_i) \u003e= #(z,a_j) for all j\u003ei and #(z,a_i) counts the occurrences of the i-th letter in z; square array A(n,k), n\u003e=0, k\u003e=0, read by antidiagonals.",
			"comment": [
				"The words counted by A(n,k) have length n*k."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A213275/b213275.txt\"\u003eAntidiagonals n = 0..20, flattened\u003c/a\u003e"
			],
			"example": [
				"A(0,k) = A(n,0) = 1: the empty word.",
				"A(n,1) = 1: (a)^n for alphabet {a}.",
				"A(1,2) = 2: ab, ba for alphabet {a,b}.",
				"A(1,3) = 6: abc, acb, bac, bca, cab, cba for alphabet {a,b,c}.",
				"A(2,2) = 3: aabb, abab, baab.",
				"A(2,3) = 15: aabbcc, aabcbc, aacbbc, ababcc, abacbc, abcabc, acabbc, acbabc, baabcc, baacbc, bacabc, bcaabc, caabbc, cababc, cbaabc.",
				"A(3,2) = 7: aaabbb, aababb, aabbab, abaabb, ababab, baaabb, baabab.",
				"Square array A(n,k) begins:",
				"  1, 1,   1,      1,         1,             1,                 1, ...",
				"  1, 1,   2,      6,        24,           120,               720, ...",
				"  1, 1,   3,     15,       105,           945,             10395, ...",
				"  1, 1,   7,    106,      2575,         87595,           3864040, ...",
				"  1, 1,  19,   1075,    115955,      19558470,        4622269345, ...",
				"  1, 1,  56,  13326,   7364321,    7236515981,    10915151070941, ...",
				"  1, 1, 174, 188196, 586368681, 3745777177366, 40684710729862072, ..."
			],
			"maple": [
				"A:= (n, k)-\u003e b([n$k]):",
				"b:= proc(l) option remember;",
				"      `if`({l[]} minus {0}={}, 1, add(`if`(g(l, i),",
				"       b(subsop(i=l[i]-1, l)), 0), i=1..nops(l)))",
				"    end:",
				"g:= proc(l, i) local j;",
				"      if l[i]\u003c1     then return false",
				"    elif l[i]\u003e1     then for j from i+1 to nops(l) do",
				"      if l[i]\u003c=l[j] then return false",
				"    elif l[j]\u003e0     then break",
				"      fi od fi; true",
				"    end:",
				"seq(seq(A(n, d-n), n=0..d), d=0..12);"
			],
			"mathematica": [
				"a[n_, k_] := b[Array[n\u0026, k]];",
				"b[l_] := b[l] = If[l ~Complement~ {0} == {}, 1, Sum[If[g[l, i], b[ReplacePart[l, i -\u003e l[[i]] - 1]], 0], {i, 1, Length[l]}]];",
				"g[l_, i_] := Module[{j},",
				"     If[l[[i]] \u003c 1, Return[False],",
				"     If[l[[i]] \u003e 1, For[j = i+1, j \u003c= Length[l], j++,",
				"     If[l[[i]] \u003c= l[[j]], Return[False],",
				"     If[l[[j]] \u003e 0, Break[]]]]]]; True];",
				"Table[Table[a[n, d-n], {n, 0, d}], {d, 0, 12}] // Flatten (* _Jean-François Alcover_, Dec 16 2013, translated from Maple *)"
			],
			"xref": [
				"Rows n=0-10 give: A000012, A000142, A001147, A213863, A213864, A213865, A213866, A213867, A213868, A213869, A213870.",
				"Columns k=0+1, 2-10 give: A000012, A005807(n-1) for n\u003e0, A213873, A213874, A213875, A213876, A213877, A213878, A213871, A213872.",
				"Main diagonal gives A213862.",
				"Cf. A213276."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Alois P. Heinz_, Jun 08 2012",
			"references": 19,
			"revision": 29,
			"time": "2021-09-14T13:19:45-04:00",
			"created": "2012-06-15T03:50:43-04:00"
		}
	]
}