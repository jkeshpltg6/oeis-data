{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A266712",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 266712,
			"data": "-5,-7,-7,115,607,4615,30427,211687,1442695,9909907,67867135,465315847,3188935867,21858303175,149816390407,1026863749555,7038210692767,48240661271047,330646286854555,2266283690589607,15533338646986375,106467089195295187",
			"name": "Coefficient of x^2 in the minimal polynomial of the continued fraction [1^n,sqrt(2),1,1,...], where 1^n means n ones.",
			"comment": [
				"See A265762 for a guide to related sequences."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A266712/b266712.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (5,15,-15,-5,1)."
			],
			"formula": [
				"a(n) = 5*a(n-1) + 15*a(n-2) - 15*a(n-3) - 5*a(n-4) + a(n-5).",
				"G.f.:  (5 -18*x -103*x^2 -180*x^3 -7*x^4 +280*x^5 +56*x^6 -14*x^7)/(-1 + 5*x +15*x^2 -15*x^3 -5*x^4 +x^5)."
			],
			"example": [
				"Let p(n,x) be the minimal polynomial of the number given by the n-th continued fraction:",
				"[sqrt(2),1,1,1,...] has p(0,x) = -1 - 6 x - 5 x^2 + 2 x^3 + x^4, so a(0) = -5;",
				"[1,sqrt(2),1,1,1,...] has p(1,x) = 1 + 2 x - 7 x^2 + 2 x^3 + x^4, so a(1) = -7;",
				"[1,1,sqrt(2),1,1,1...] has p(2,x) = -9 + 18 x - 7 x^2 - 2 x^3 + x^4, so a(2) = -7."
			],
			"mathematica": [
				"u[n_] := Table[1, {k, 1, n}]; t[n_] := Join[u[n], {Sqrt[2]}, {{1}}];",
				"f[n_] := FromContinuedFraction[t[n]];",
				"t = Table[MinimalPolynomial[f[n], x], {n, 0, 40}];",
				"Coefficient[t, x, 0] ; (* A266710 *)",
				"Coefficient[t, x, 1];  (* A266711 *)",
				"Coefficient[t, x, 2];  (* A266712 *)",
				"Coefficient[t, x, 3];  (* A266713 *)",
				"Coefficient[t, x, 4];  (* A266710 *)",
				"LinearRecurrence[{5,15,-15,-5,1}, {-5, -7, -7, 115, 607, 4615, 30427, 211687}, 30] (* _G. C. Greubel_, Jan 26 2018 *)"
			],
			"program": [
				"(PARI) x='x+O('x^30); Vec((5 -18*x -103*x^2 -180*x^3 -7*x^4 +280*x^5 +56*x^6 -14*x^7)/(-1 + 5*x +15*x^2 -15*x^3 -5*x^4 +x^5)) \\\\ _G. C. Greubel_, Jan 26 2018",
				"(MAGMA) I:=[115, 607, 4615, 30427, 211687]; [-5, -7, -7] cat [n le 5 select I[n] else 5*Self(n-1) + 15*Self(n-2) - 15*Self(n-3) - 5*Self(n-4) + Self(n-5): n in [1..30]]; // _G. C. Greubel_, Jan 26 2018"
			],
			"xref": [
				"Cf. A265762, A266710, A266711, A266713."
			],
			"keyword": "sign,easy",
			"offset": "0,1",
			"author": "_Clark Kimberling_, Jan 09 2016",
			"references": 5,
			"revision": 7,
			"time": "2018-01-27T02:29:56-05:00",
			"created": "2016-01-09T16:06:04-05:00"
		}
	]
}