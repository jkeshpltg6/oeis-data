{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A080575",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 80575,
			"data": "1,1,1,1,3,1,1,4,3,6,1,1,5,10,10,15,10,1,1,6,15,15,10,60,20,15,45,15,1,1,7,21,21,35,105,35,70,105,210,35,105,105,21,1,1,8,28,28,56,168,56,35,280,210,420,70,280,280,840,560,56,105,420,210,28,1,1,9,36,36,84,252,84,126,504,378,756,126,315,1260,1260,1890,1260,126,280,2520,840,1260,3780,1260,84,945,1260,378,36,1,1,10,45,45,120,360,120,210,840,630,1260,210",
			"name": "Triangle of multinomial coefficients, read by rows (version 2).",
			"comment": [
				"This is different from A036040 and A178867.",
				"T[n,m] = count of set partitions of n with block lengths given by the m-th partition of n in the canonical ordering.",
				"From _Tilman Neumann_, Oct 05 2008: (Start)",
				"These are also the coefficients occurring in complete Bell polynomials, Faa di Bruno's formula (in its simplest form) and computation of moments from cumulants.",
				"Though the Bell polynomials seem quite unwieldy, they can be computed easily as the determinant of an n-dimensional square matrix. (see e.g. [Coffey] and program below)",
				"The complete Bell polynomial of the first n primes gives A007446. (End)",
				"The difference with A036040 and A178867 lies in the ordering of the monomials. This sequence uses lexicographic ordering, while in A036040 the total order (power) of the monomials prevails (Abramowitz-Stegun style): e.g., in row 6 we have ...+ 15*x[3]*x[5] + 15*x[3]*x[6]^2 + 10*x[4]^2 +...; in A036040 the coefficient of x[3]*x[6]^2 would come after that of x[4]^2 because the total order is higher, here it comes before in view of the lexicographic order. - _M. F. Hasler_, Jul 12 2015"
			],
			"reference": [
				"See A036040 for the column labeled \"M_3\" in Abramowitz and Stegun, Handbook, p. 831."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A080575/b080575.txt\"\u003eRows n = 1..26, flattened\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"Berg, Kimmo \u003ca href=\"https://doi.org/10.1007/s10479-013-1334-3\"\u003eComplexity of solution structures in nonlinear pricing\u003c/a\u003e, Ann. Oper. Res. 206, 23-37 (2013).",
				"R. J. Cano, \u003ca href=\"/A080575/a080575_8.txt\"\u003eSequencer program.\u003c/a\u003e",
				"Mark W. Coffey, \u003ca href=\"http://arxiv.org/abs/math-ph/0608049\"\u003eA Set of Identities for a Class of Alternating Binomial Sums Arising in Computing Applications\u003c/a\u003e, arXiv:math-ph/0608049, 2006.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Cumulant\"\u003eCumulant\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Bell_polynomials\"\u003eBell polynomials\u003c/a\u003e"
			],
			"example": [
				"For n=4 the 5 integer partitions in canonical ordering with corresponding set partitions and counts are:",
				"   [4]       -\u003e #{1234} = 1",
				"   [3,1]     -\u003e #{123/4, 124/3, 134/2, 1/234} = 4",
				"   [2,2]     -\u003e #{12/34, 13/24, 14/23} = 3",
				"   [2,1,1]   -\u003e #{12/3/4, 13/2/4, 1/23/4, 14/2/3, 1/24/3, 1/2/34} = 6",
				"   [1,1,1,1] -\u003e #{1/2/3/4} = 1",
				"Thus row 4 is [1, 4, 3, 6, 1].",
				"Triangle begins:",
				"1;",
				"1, 1;",
				"1, 3,  1;",
				"1, 4,  3,  6,  1;",
				"1, 5, 10, 10, 15,  10,  1;",
				"1, 6, 15, 15, 10,  60, 20, 15,  45,  15,  1;",
				"1, 7, 21, 21, 35, 105, 35, 70, 105, 210, 35, 105, 105, 21, 1;",
				"...",
				"Row 4 represents 1*k(4)+4*k(3)*k(1)+3*k(2)^2+6*k(2)*k(1)^2+1*k(1)^4 and T(4,4)=6 since there are six ways of partitioning four labeled items into one part with two items and two parts each with one item."
			],
			"mathematica": [
				"runs[li:{__Integer}] := ((Length/@ Split[ # ]))\u0026[Sort@ li]; Table[Apply[Multinomial, IntegerPartitions[w], {1}]/Apply[Times, (runs/@ IntegerPartitions[w])!, {1}], {w, 6}]",
				"(* Second program: *)",
				"completeBellMatrix[x_, n_] := Module[{M, i, j}, M[_, _] = 0; For[i=1, i \u003c= n-1 , i++, M[i, i+1] = -1]; For[i=1, i \u003c= n , i++, For[j=1, j \u003c= i, j++, M[i, j] = Binomial[i-1, j-1]*x[i-j+1]]]; Array[M, {n, n}]]; completeBellPoly[x_, n_] := Det[completeBellMatrix[x, n]]; row[n_] := List @@ completeBellPoly[x, n] /. x[_] -\u003e 1 // Reverse; Table[row[n], {n, 1, 10}] // Flatten (* _Jean-François Alcover_, Aug 31 2016, after _Tilman Neumann_ *)",
				"B[0] = 1;",
				"B[n_] := B[n] = Sum[Binomial[n-1, k] B[n-k-1] x[k+1], {k, 0, n-1}]//Expand;",
				"row[n_] := Reverse[List @@ B[n] /. x[_] -\u003e 1];",
				"Table[row[n], {n, 1, 10}] // Flatten (* _Jean-François Alcover_, Aug 10 2018, after _Wolfdieter Lang_ *)"
			],
			"program": [
				"(MuPAD)",
				"From _Tilman Neumann_, Oct 05 2008: (Start)",
				"completeBellMatrix := proc(x,n) // x - vector x[1]...x[m], m\u003e=n",
				"local i,j,M; begin M:=matrix(n,n): // zero-initialized",
				"for i from 1 to n-1 do M[i,i+1]:=-1: end_for:",
				"for i from 1 to n do for j from 1 to i do",
				"    M[i,j] := binomial(i-1,j-1)*x[i-j+1]:",
				"end_for: end_for:",
				"return (M): end_proc:",
				"completeBellPoly := proc(x, n) begin",
				"return (linalg::det(completeBellMatrix(x,n))): end_proc:",
				"for i from 1 to 10 do print(i,completeBellPoly(x,i)): end_for: (End)",
				"(PARI) See links.",
				"(PARI) From _M. F. Hasler_, Jul 12 2015: (Start)",
				"A080575_poly(n,V=vector(n,i,eval(Str('x,i))))={matdet(matrix(n,n,i,j,if(j\u003c=i,binomial(i-1,j-1)*V[n-i+j],-(j==i+1))))}",
				"A080575_row(n)={(f(s)=if(type(s)!=\"t_INT\",concat(apply(f,select(t-\u003et,Vec(s)))),s))(A080575_poly(n))} \\\\ (End)"
			],
			"xref": [
				"See A036040 for another version. Cf. A036036-A036039.",
				"Row sums are A000110.",
				"Row lengths are A000041.",
				"Cf. A007446. - _Tilman Neumann_, Oct 05 2008",
				"Cf. A178866 and A178867 (version 3). - _Johannes W. Meijer_, Jun 21, 2010",
				"Maximum value in row n gives A102356(n)."
			],
			"keyword": "nonn,easy,nice,tabf,look",
			"offset": "1,5",
			"author": "_Wouter Meeussen_, Mar 23 2003",
			"references": 18,
			"revision": 98,
			"time": "2020-07-22T11:39:23-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}