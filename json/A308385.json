{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A308385",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 308385,
			"data": "1,3,15,29,61,87,139,177,249,299,391,453,565,639,771,857,1009,1107,1279,1389,1581,1703,1915,2049,2281,2427,2679,2837,3109,3279,3571,3753,4065,4259,4591,4797,5149,5367,5739,5969,6361,6603,7015,7269,7701,7967,8419",
			"name": "a(n) is the last square visited by fers moves on a spirally numbered (2n-1) X (2n-1) board, moving to the lowest available unvisited square at each step.",
			"comment": [
				"A 5 X 5 board, for example, is numbered with the square spiral:",
				".",
				"  21--22--23--24--25",
				"   |",
				"  20   7---8---9--10",
				"   |   |           |",
				"  19   6   1---2  11",
				"   |   |       |   |",
				"  18   5---4---3  12",
				"   |               |",
				"  17--16--15--14--13",
				".",
				"A fers is a (1,1)-leaper and can move one square diagonally."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A308385/b308385.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Stephen Emmerson and Geoff Foster, \u003ca href=\"http://www.theproblemist.org/dloads/Glossary.pdf\"\u003eA glossary of fairy chess definitions\u003c/a\u003e, British Chess Problem Society, 2018.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Ferz\"\u003eFerz\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,2,-2,-1,1)."
			],
			"formula": [
				"a(n) = (4n^2-9n+6)*[n is odd] + (4n^2-11n+9)*[n is even] where [] is the Iverson bracket.",
				"a(n) = A054556(n)*[n is odd] + (A054552(n)+1)*[n is even] where [] is the Iverson bracket.",
				"a(n) = A316884(n^2)*[n is odd] + A316884(n^2-n)*[n is even] where [] is the Iverson bracket.",
				"From _Colin Barker_, May 23 2019: (Start)",
				"G.f.: x*(1 + 2*x + 10*x^2 + 10*x^3 + 9*x^4) / ((1 - x)^3*(1 + x)^2).",
				"a(n) = (3/2)*(5+(-1)^n) - (10+(-1)^n)*n + 4*n^2.",
				"a(n) = a(n-1) + 2*a(n-2) - 2*a(n-3) - a(n-4) + a(n-5) for n\u003e4.",
				"(End)",
				"E.g.f.: (1/2)*exp(-x)*(3 + 2*x + exp(2*x)*(15 - 12*x + 8*x^2)) - 9. - _Stefano Spezia_, Aug 17 2019"
			],
			"mathematica": [
				"Table[(3/2) (5 + (-1)^n) - (10 + (-1)^n) n + 4 n^2, {n, 60}] (* _Vincenzo Librandi_, Aug 01 2019 *)"
			],
			"program": [
				"(PARI) Vec(x*(1 + 2*x + 10*x^2 + 10*x^3 + 9*x^4) / ((1 - x)^3*(1 + x)^2) + O(x^40)) \\\\ _Colin Barker_, May 23 2019",
				"(MAGMA) [(3/2)*(5+(-1)^n) - (10+(-1)^n)*n + 4*n^2: n in [1..50]]; // _Vincenzo Librandi_, Aug 01 2019"
			],
			"xref": [
				"Cf. A054552, A054556, A316667, A316884."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Sangeet Paul_, May 23 2019",
			"references": 1,
			"revision": 33,
			"time": "2019-08-22T20:39:08-04:00",
			"created": "2019-08-18T04:47:02-04:00"
		}
	]
}