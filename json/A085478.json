{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A085478",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 85478,
			"data": "1,1,1,1,3,1,1,6,5,1,1,10,15,7,1,1,15,35,28,9,1,1,21,70,84,45,11,1,1,28,126,210,165,66,13,1,1,36,210,462,495,286,91,15,1,1,45,330,924,1287,1001,455,120,17,1,1,55,495,1716,3003,3003,1820,680,153,19,1",
			"name": "Triangle read by rows: T(n, k) = binomial(n + k, 2*k).",
			"comment": [
				"Coefficient array for Morgan-Voyce polynomial b(n,x). A053122 (unsigned) is the coefficient array for B(n,x). Reversal of A054142. - _Paul Barry_, Jan 19 2004",
				"This triangle is formed from even-numbered rows of triangle A011973 read in reverse order. - _Philippe Deléham_, Feb 16 2004",
				"T(n,k) is the number of nondecreasing Dyck paths of semilength n+1, having k+1 peaks. T(n,k) is the number of nondecreasing Dyck paths of semilength n+1, having k peaks at height \u003e= 2. T(n,k) is the number of directed column-convex polyominoes of area n+1, having k+1 columns. - _Emeric Deutsch_, May 31 2004",
				"Riordan array (1/(1-x), x/(1-x)^2). - _Paul Barry_, May 09 2005",
				"The triangular matrix a(n,k) = (-1)^(n+k)*T(n,k) is the matrix inverse of A039599. - _Philippe Deléham_, May 26 2005",
				"The n-th row gives absolute values of coefficients of reciprocal of g.f. of bottom-line of n-wave sequence. - Floor van Lamoen (fvlamoen(AT)planet.nl), Sep 24 2006",
				"Unsigned version of A129818. - _Philippe Deléham_, Oct 25 2007",
				"T(n, k) is also the number of idempotent order-preserving full transformations (of an n-chain) of height k \u003e=1 (height(alpha) = |Im(alpha)|) and of waist n (waist(alpha) = max(Im(alpha))). - _Abdullahi Umar_, Oct 02 2008",
				"A085478 is jointly generated with A078812 as a triangular array of coefficients of polynomials u(n,x): initially, u(1,x) = v(1,x) = 1; for n\u003e1, u(n,x) = u(n-1,x)+x*v(n-1)x and v(n,x) = u(n-1,x)+(x+1)*v(n-1,x). See the Mathematica section. - _Clark Kimberling_, Feb 25 2012",
				"Per Kimberling's recursion relations, see A102426. - _Tom Copeland_, Jan 19 2016",
				"Subtriangle of the triangle given by (0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (1, 0, 1, -1, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Mar 26 2012",
				"T(n,k) is also the number of compositions (ordered partitions) of 2*n+1 into 2*k+1 parts which are all odd. Proof: The o.g.f. of column k, x^k/(1-x)^(2*k+1) for k \u003e= 0, is the o.g.f. of the odd-indexed members of the sequence with o.g.f. (x/(1-x^2))^(2*k+1) (bisection, odd part). Thus T(n,k) is obtained from the sum of the multinomial numbers A048996 for the partitions of 2*n+1 into 2*k+1 parts, all of which are odd. E.g., T(3,1) = 3 + 3 from the numbers for the partitions [1,1,5] and [1,3,3], namely 3!/(2!*1!) and 3!/(1!*2!), respectively. The number triangle with the number of these partitions as entries is A152157. - _Wolfdieter Lang_, Jul 09 2012",
				"The matrix elements of the inverse are T^(-1)(n,k) = (-1)^(n+k)*A039599(n,k). - _R. J. Mathar_, Mar 12 2013",
				"T(n,k) = A258993(n+1,k) for k = 0..n-1. - _Reinhard Zumkeller_, Jun 22 2015",
				"The n-th row polynomial in descending powers of x is the n-th Taylor polynomial of the algebraic function F(x)*G(x)^n about 0, where F(x) = (1 + sqrt(1 + 4*x))/(2*sqrt(1 + 4*x)) and G(x) = ((1 + sqrt(1 + 4*x))/2)^2. For example, for n = 4, (1 + sqrt(1 + 4*x))/(2*sqrt(1 + 4*x)) * ((1 + sqrt(1 + 4*x))/2)^8 = (x^4  + 10*x^3 + 15*x^2 + 7*x + 1) + O(x^5). - _Peter Bala_, Feb 23 2018",
				"Row n also gives the coefficients of the characteristc polynomial of the tridiagonal n X n matrix M_n given in A332602: Phi(n, x) := Det(M_n - x*1_n) = Sum_{k=0..n} T(n, k)*(-x)^k, for n \u003e= 0, with Phi(0, x) := 1. - _Wolfdieter Lang_, Mar 25 2020"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A085478/b085478.txt\"\u003eRows n = 0..125 of triangle, flattened\u003c/a\u003e",
				"J.P. Allouche and M. Mendes France, \u003ca href=\"http://arxiv.org/abs/1202.0211\"\u003eStern-Brocot polynomials and power series\u003c/a\u003e, arXiv preprint arXiv:1202.0211 [math.NT], 2012. - _N. J. A. Sloane_, May 10 2012",
				"P. Bala, \u003ca href=\"/A264772/a264772_1.pdf\"\u003eA 4-parameter family of embedded Riordan arrays\u003c/a\u003e",
				"E. Barcucci, A. Del Lungo, S. Fezzi and R. Pinzani, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(97)82778-1\"\u003eNondecreasing Dyck paths and q-Fibonacci numbers\u003c/a\u003e, Discrete Math., 170, 1997, 211-217.",
				"Paul Barry, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL8/Barry/barry84.html\"\u003eA Catalan Transform and Related Transformations on Integer Sequences\u003c/a\u003e, J. Integer Sequ., Vol. 8 (2005), Article 05.4.5.",
				"Paul Barry, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL12/Barry4/barry64.html\"\u003eSymmetric Third-Order Recurring Sequences, Chebyshev Polynomials, and Riordan Arrays\u003c/a\u003e, JIS 12 (2009) 09.8.6.",
				"Paul Barry, A. Hennessey, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL12/Barry1/barry83.html\"\u003eNotes on a Family of Riordan Arrays and Associated Integer Hankel Transforms \u003c/a\u003e, JIS 12 (2009) 09.5.3.",
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/2011.10827\"\u003eNotes on the Hankel transform of linear combinations of consecutive pairs of Catalan numbers\u003c/a\u003e, arXiv:2011.10827 [math.CO], 2020.",
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/2011.13985\"\u003eThe second production matrix of a Riordan array\u003c/a\u003e, arXiv:2011.13985 [math.CO], 2020.",
				"E. Czabarka et al, \u003ca href=\"https://doi.org/10.1016/j.disc.2018.06.032\"\u003eEnumerations of peaks and valleys on non-decreasing Dyck paths\u003c/a\u003e, Disc. Math. 341 (2018) 2789-2807, Theorem 3.",
				"E. Deutsch and H. Prodinger, \u003ca href=\"http://dx.doi.org/10.1016/S0304-3975(03)00222-6\"\u003eA bijection between directed column-convex polyominoes and ordered trees of height at most three\u003c/a\u003e, Theoretical Comp. Science, 307, 2003, 319-325.",
				"James East, Nicholas Ham, \u003ca href=\"https://arxiv.org/abs/1811.05735\"\u003eLattice paths and submonoids of Z^2\u003c/a\u003e, arXiv:1811.05735 [math.CO], 2018.",
				"A. Laradji, and A. Umar, \u003ca href=\"http://dx.doi.org/10.1007/s00233-005-0553-6\"\u003eCombinatorial results for semigroups of order-preserving full transformations\u003c/a\u003e, Semigroup Forum 72 (2006), 51-62. - _Abdullahi Umar_, Oct 02 2008",
				"Donatella Merlini and Renzo Sprugnoli, \u003ca href=\"https://doi.org/10.1016/j.disc.2016.08.017\"\u003eArithmetic into geometric progressions through Riordan arrays\u003c/a\u003e, Discrete Mathematics 340.2 (2017): 160-174.",
				"Yidong Sun, \u003ca href=\"https://www.fq.math.ca/Papers1/43-4/paper43-4-10b.pdf\"\u003eNumerical Triangles and Several Classical Sequences\u003c/a\u003e, Fib. Quart. 43, no. 4, (2005) 359-370.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Morgan-VoycePolynomials.html\"\u003eMorgan-Voyce Polynomials\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = (n+k)!/((n-k)!*(2*k)!).",
				"G.f.: (1-z)/((1-z)^2-tz). - _Emeric Deutsch_, May 31 2004",
				"Row sums are A001519 (Fib(2n+1)). Diagonal sums are A011782. Binomial transform of A026729 (product of lower triangular matrices). - _Paul Barry_, Jun 21 2004",
				"T(n, 0) = 1, T(n, k) = 0 if n\u003ck; T(n, k) = Sum_{j\u003e=0} T(n-1-j, k-1)*(j+1). T(0, 0) = 1, T(0, k) = 0 if k\u003e0; T(n, k) = T(n-1, k-1) + T(n-1, k) + Sum_{j\u003e=0} (-1)^j*T(n-1, k+j)*A000108(j). For the column k, g.f.: Sum_{n\u003e=0} T(n, k)*x^n = (x^k) / (1-x)^(2*k+1). - _Philippe Deléham_, Feb 15 2004",
				"Sum_{k, 0\u003c=k\u003c=n} T(n,k)*x^(2*k) = A000012(n), A001519(n+1), A001653(n), A078922(n+1), A007805(n), A097835(n), A097315(n), A097838(n), A078988(n), A097841(n), A097727(n), A097843(n), A097730(n), A098244(n), A097733(n), A098247(n), A097736(n), A098250(n), A097739(n), A098253(n), A097742(n), A098256(n), A097767(n), A098259(n), A097770(n), A098262(n), A097773(n), A098292(n), A097776(n) for x=0,1,2,...27,28 respectively. - _Philippe Deléham_, Dec 31 2007",
				"T(2*n,n) = A005809(n). - _Philippe Deléham_, Sep 17 2009",
				"A183160(n) = Sum_{k=0..n} T(n,k)*T(n,n-k). - _Paul D. Hanna_, Dec 27 2010",
				"T(n,k) = 2*T(n-1,k) + T(n-1,k-1) - T(n-2,k). - _Philippe Deléham_, Feb 06 2012",
				"O.g.f. for column k: x^k/(1-x)^(2*k+1), k \u003e= 0. [See the o.g.f. of the triangle above, and a comment on compositions. - _Wolfdieter Lang_, Jul 09 2012]",
				"E.g.f.: (2/sqrt(x + 4))*sinh(1/2*t*sqrt(x + 4))*cosh(1/2*t*sqrt(x)) = t + (1 + x)*t^3/3! + (1 + 3*x + x^2)*t^5/5! + (1 + 6*x + 5*x^2 + x^3)*t^7/7! + .... Cf. A091042. - _Peter Bala_, Jul 29 2013",
				"T(n, k) = A065941(n+3*k, 4*k) = A108299(n+3*k, 4*k) = A194005(n+3*k, 4*k). - _Johannes W. Meijer_, Sep 05 2013",
				"Sum_{k=0..n} (-1)^k*T(n,k)*A000108(k) = A000007(n) for n \u003e= 0. - _Werner Schulte_, Jul 12 2017",
				"Sum_{k=0..floor(n/2)} T(n-k,k)*A000108(k) = A001006(n) for n \u003e= 0. - _Werner Schulte_, Jul 12 2017"
			],
			"example": [
				"Triangle begins as:",
				"  1;",
				"  1    1;",
				"  1    3    1;",
				"  1    6    5    1;",
				"  1   10   15    7    1;",
				"  1   15   35   28    9    1;",
				"  1   21   70   84   45   11    1;",
				"  1   28  126  210  165   66   13    1;",
				"  1   36  210  462  495  286   91   15    1;",
				"  1   45  330  924 1287 1001  455  120   17    1;",
				"  1   55  495 1716 3003 3003 1820  680  153   19    1;",
				"...",
				"From _Philippe Deléham_, Mar 26 2012: (Start)",
				"(0, 1, 0, 1, 0, 0, 0, ...) DELTA (1, 0, 1, -1, 0, 0, 0, ...) begins:",
				"  1",
				"  0, 1",
				"  0, 1,  1",
				"  0, 1,  3,   1",
				"  0, 1,  6,   5,   1",
				"  0, 1, 10,  15,   7,   1",
				"  0, 1, 15,  35,  28,   9,  1",
				"  0, 1, 21,  70,  84,  45, 11,  1",
				"  0, 1, 28, 126, 210, 165, 66, 13, 1. (End)"
			],
			"maple": [
				"T := (n,k) -\u003e binomial(n+k,2*k): seq(seq(T(n,k), k=0..n), n=0..11);"
			],
			"mathematica": [
				"(* First program *)",
				"u[1, x_]:= 1; v[1, x_]:= 1; z = 13;",
				"u[n_, x_]:= u[n-1, x] + x*v[n-1, x];",
				"v[n_, x_]:= u[n-1, x] + (x+1)*v[n-1, x];",
				"Table[Expand[u[n, x]], {n, 1, z/2}]",
				"Table[Expand[v[n, x]], {n, 1, z/2}]",
				"cu = Table[CoefficientList[u[n, x], x], {n, 1, z}];",
				"TableForm[cu]",
				"Flatten[%]   (* A085478 *)",
				"Table[Expand[v[n, x]], {n, 1, z}]",
				"cv = Table[CoefficientList[v[n, x], x], {n, 1, z}];",
				"TableForm[cv]",
				"Flatten[%]   (* A078812 *) (*_Clark Kimberling_, Feb 25 2012 *)",
				"(* Second program *)",
				"Table[Binomial[n+k,2*k], {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, Aug 01 2019 *)"
			],
			"program": [
				"(PARI) T(n,k) = binomial(n+k,n-k)",
				"(Haskell)",
				"a085478 n k = a085478_tabl !! n !! k",
				"a085478_row n = a085478_tabl !! n",
				"a085478_tabl = zipWith (zipWith a007318) a051162_tabl a025581_tabl",
				"-- _Reinhard Zumkeller_, Jun 22 2015",
				"(MAGMA) [Binomial(n+k, 2*k): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Aug 01 2019",
				"(Sage) [[binomial(n+k,2*k) for k in (0..n)] for n in (0..12)] # _G. C. Greubel_, Aug 01 2019",
				"(GAP) Flat(List([0..12], n-\u003e List([0..n], k-\u003e Binomial(n+k, 2*k) ))); # _G. C. Greubel_, Aug 01 2019"
			],
			"xref": [
				"Row sums: A001519.",
				"Cf. A000108, A001006, A007318, A098158, A183160, A078812, A091042.",
				"Cf. A258993, A025581, A051162, A054142, A332602."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,5",
			"author": "_Philippe Deléham_, Aug 14 2003",
			"references": 60,
			"revision": 143,
			"time": "2021-03-17T15:30:45-04:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}