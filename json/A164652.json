{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A164652",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 164652,
			"data": "1,0,1,1,0,1,0,5,0,1,8,0,15,0,1,0,84,0,35,0,1,180,0,469,0,70,0,1,0,3044,0,1869,0,126,0,1,8064,0,26060,0,5985,0,210,0,1,0,193248,0,152900,0,16401,0,330,0,1,604800,0,2286636,0,696905,0,39963,0,495,0,1,0,19056960,0,18128396,0,2641925,0,88803,0,715,0,1",
			"name": "Triangle read by rows: Hultman numbers: a(n,k) is the number of permutations of n elements whose cycle graph (as defined by Bafna and Pevzner) contains k cycles for n \u003e= 0 and 1 \u003c= k \u003c= n+1.",
			"comment": [
				"a(n,k) is also the number of ways to express a given (n+1)-cycle as the product of an (n+1)-cycle and a permutation with k cycles (see Doignon and Labarre). a(n,n+1-2k) is the number of permutations of n elements whose block-interchange distance is k (see Christie, Doignon and Labarre).",
				"Named after the Swedish mathematician Axel Hultman. - _Amiram Eldar_, Jun 11 2021"
			],
			"reference": [
				"Axel Hultman, Toric permutations, Master's thesis, Department of Mathematics, KTH, Stockholm, Sweden, 1999."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A164652/b164652.txt\"\u003eRows n = 0..125 of triangle, flattened\u003c/a\u003e",
				"Nikita Alexeev, Anna Pologova and Max A. Alekseyev, \u003ca href=\"https://doi.org/10.1089/cmb.2016.0190\"\u003eGeneralized Hultman Numbers and Cycle Structures of Breakpoint Graphs\u003c/a\u003e, Journal of Computational Biology, Vol. 24, No. 2 (2017), pp. 93-105; \u003ca href=\"http://arxiv.org/abs/1503.05285\"\u003earXiv preprint\u003c/a\u003e, arXiv:1503.05285 [q-bio.GN], 2015-2017.",
				"Nikita Alexeev and Peter Zograf, \u003ca href=\"http://arxiv.org/abs/1111.3061\"\u003eHultman numbers, polygon gluings and matrix integrals\u003c/a\u003e, arXiv preprint arXiv:1111.3061 [math.PR], 2011.",
				"Nikita Alexeev and Peter Zograf, \u003ca href=\"http://dx.doi.org/10.1089/cmb.2013.0066\"\u003eRandom matrix approach to the distribution of genomic distance\u003c/a\u003e, Journal of Computational Biology, Vol. 21, No. 8 (2014), pp. 622-631.",
				"Miklos Bona and Ryan Flynn, \u003ca href=\"http://arxiv.org/abs/0811.0740\"\u003eThe Average Number of Block Interchanges Needed to Sort A Permutation and a recent result of Stanley\u003c/a\u003e, arXiv:0811.0740 [math.CO], 2008.",
				"Miklos Bona and Ryan Flynn, \u003ca href=\"http://dx.doi.org/10.1016/j.ipl.2009.04.019\"\u003eThe Average Number of Block Interchanges Needed to Sort A Permutation and a recent result of Stanley\u003c/a\u003e, Inf. Process. Lett., Vol. 109 (2009), pp. 927-931",
				"David A. Christie, \u003ca href=\"http://dx.doi.org/10.1016/S0020-0190(96)00155-X\"\u003eSorting Permutations by Block-Interchanges\u003c/a\u003e, Inf. Process. Lett., Vol. 60, No. 4 (1996), pp. 165-169",
				"Jean-Paul Doignon and Anthony Labarre, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL10/Doignon/doignon77.html\"\u003eOn Hultman Numbers\u003c/a\u003e, J. Integer Seq., Vol. 10 (2007), Article 07.6.2, 13 pages.",
				"Simona Grusea and Anthony Labarre, \u003ca href=\"http://arxiv.org/abs/1104.3353\"\u003eThe distribution of cycles in breakpoint graphs of signed permutations\u003c/a\u003e, arXiv:1104.3353 [cs.DM], 2011-2012."
			],
			"formula": [
				"T(n,k) = S1(n+2,k)/C(n+2,2) if n-k is odd, and 0 otherwise. Here S1(n,k) are the unsigned Stirling numbers of the first kind A132393 and C(n,k) is the binomial coefficient (see Bona and Flynn).",
				"For n \u003e 0: T(n,k) = A128174(n+1,k) * A130534(n+1,k-1) / A000217(n+1). - _Reinhard Zumkeller_, Aug 01 2014"
			],
			"example": [
				"Triangle begins:",
				"n=0:  1;",
				"n=1:  0, 1;",
				"n=2:  1, 0, 1;",
				"n=3:  0, 5, 0, 1;",
				"n=4:  8, 0, 15, 0, 1;",
				"n=5:  0, 84, 0, 35, 0, 1;",
				"n=6:  180, 0, 469, 0, 70, 0, 1;",
				"n=7:  0, 3044, 0, 1869, 0, 126, 0, 1;",
				"n=8:  8064, 0, 26060, 0, 5985, 0, 210, 0, 1;",
				"n=9:  0, 193248, 0, 152900, 0, 16401, 0, 330, 0, 1;",
				"n=10: 604800, 0, 2286636, 0, 696905, 0, 39963, 0, 495, 0, 1;",
				"  ..."
			],
			"maple": [
				"A164652:= (n, k)-\u003e `if`(n-k mod 2 = 1, -Stirling1(n+2, k)/binomial(n+2, 2), 0):",
				"for n from 0 to 7 do seq(A164652(n,k),k=1..n+1) od; # _Peter Luschny_, Mar 22 2015"
			],
			"mathematica": [
				"T[n_, k_] := If[OddQ[n-k], Abs[StirlingS1[n+2, k]]/Binomial[n+2, 2], 0];",
				"Table[T[n, k], {n, 0, 11}, {k, 1, n+1}] // Flatten (* _Jean-François Alcover_, Aug 10 2018 *)"
			],
			"program": [
				"(Haskell)",
				"a164652 n k = a164652_tabl !! n !! k",
				"a164652_row n = a164652_tabl !! n",
				"a164652_tabl = [0] : tail (zipWith (zipWith (*)) a128174_tabl $",
				"   zipWith (map . flip div) (tail a000217_list) (map init $ tail a130534_tabl))",
				"-- _Reinhard Zumkeller_, Aug 01 2014",
				"(Sage)",
				"def A164652(n, k):",
				"    return stirling_number1(n+2,k)/binomial(n+2,2) if is_odd(n-k) else 0",
				"for n in (0..7): print([A164652(n,k) for k in (1..n+1)]) # _Peter Luschny_, Mar 22 2015",
				"(PARI)",
				"T(n,k)= my(s=(n-k)%2); (-1)^s*s*stirling(n+2,k,1)/binomial(n+2,2);",
				"concat(vector(12, n, vector(n, k, T(n-1,k)))) \\\\ _Gheorghe Coserea_, Jan 23 2018"
			],
			"xref": [
				"Cf. A000142 (row sums), A000217, A060593, A128174, A130534, A185259, A189507, A260695."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Anthony Labarre_, Aug 19 2009",
			"ext": [
				"T(0,1) set to 1 by _Peter Luschny_, Mar 24 2015",
				"Edited to match values of k to the range 1 to n+1. - _Max Alekseyev_, Nov 20 2020"
			],
			"references": 8,
			"revision": 87,
			"time": "2021-06-11T05:14:40-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}