{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A072270",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 72270,
			"data": "1,1,13,9,101,5,701,49,361,29,31021,33,204101,181,1021,1889,8799541,233,57746701,1361,41581,7589,2486401661,1633,161532401,49661,22810681,58241,702418373381,2245,4608956945501,3437249,74991181,2135149,2802699901,75921,1302034904649701,14007941,3219888061,3019201",
			"name": "A partial product representation of A006131 and A072265.",
			"comment": [
				"Define f(n) = A006131(n-1) and L(n) = 4*f(n-1)+f(n+1), which implies L(n) = A072265(n), n\u003e1.",
				"For even n, f(n) = product_{d|n} a(d) and for odd n, f(n) = product_{d|n} a(2d).",
				"Taking logarithms defines the sequence a(.) via a Mobius transformation (see A072183).",
				"Writing f(n) and L(n) in terms of Binet formulas leads to a representation as cyclotomic polynomials."
			],
			"formula": [
				"Let h=(1+sqrt(17))/2, Phi(n, x) = n-th cyclotomic polynomial, so x^n-1= product_{d|n} Phi(d, x), and let g(d) be the order of Phi(d, x). Then a(n)=(h-1)^g(n)*Phi(n, h^2/4), n\u003e2.",
				"a(p) = L(p) for odd prime p.",
				"a(2p) = f(p) for odd prime p.",
				"a(2^k+1) = L(2^k).",
				"a(3*2^k = L(2^k)-4^k.",
				"L(n) = product_{d|n} a(d) for odd n.",
				"L(n*2^k) = product_{d|n} a(d*2^(k+1)) for k\u003e0 and odd n."
			],
			"example": [
				"f(12)=a(1)*a(2)*a(3)*a(4)*a(6)*a(12) = 1*1*13*9*5*33 = 19305 for even n=12.",
				"f(9)=a(2)*a(6)*a(18)= 1*5*233 = 1165 for odd n=9.",
				"L(6)=a(4)*a(12) = 9*33 = 297 = 4*f(5)+f(7) = 4*29+181 for even n=6.",
				"L(15)=a(1)*a(3)*a(5)*a(15) = 1*13*101*1021 = 1340573 for odd n=15."
			],
			"maple": [
				"A072270 := proc(n) if n \u003c=2 then 1; else h := (1+sqrt(17))/2 ; cy := numtheory[cyclotomic](n,x) ; g := degree(cy) ; (h-1)^g*subs(x=h^2/4,cy) ; expand(%) ; end if; end proc: # _R. J. Mathar_, Nov 17 2010"
			],
			"xref": [
				"Cf. A006131, A072183."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Miklos Kristof_, Jul 09 2002",
			"ext": [
				"Divided argument of Phi by 4; moved comments to formula section - _R. J. Mathar_, Nov 17 2010"
			],
			"references": 3,
			"revision": 11,
			"time": "2015-08-06T01:31:32-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}