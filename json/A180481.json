{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A180481",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 180481,
			"data": "11,23,11,67,3119,19,941,739,29,41,79,127,5507,1399,191,56873,1193,16657,49411,30059,10453,373,719,18773,12277,1031,1489,131,823,1283,14251,317,10631,313,191,16987,70381,229,8447,3539,1019,3499,2777,301579,587,241,6229,229,11657,571,2969,701,1627,20327,467,2069,863",
			"name": "The smallest prime q \u003e p = prime(n) such that p*(q-p)+q, p*(q-p)-q, q*(q-p)+p and q*(q-p)-p are simultaneously prime, or 0 if no such q exists.",
			"comment": [
				"It is conjectured that a(n) \u003e 0 for all n, and for infinitely many terms, a(n) = prime(n+1).",
				"a(n) = prime(n+1) for n = 9, 100, 508, 627, 752, 835, 889, ... (that is, for p = 23, 541, 3631, 4643, 5711, 6421, 6911, ...) - _Derek Orr_, Aug 25 2014",
				"We have a(n) - prime(n) == 0 (mod 6) for all n \u003e 2. Indeed, suppose p = 6k + 1, then q - p = 6n + 2 would imply that q is divisible by 3, and q - p = 6n + 4 would imply that p*(q-p)+q is divisible by 3. A similar reasoning applies for p = 6k - 1: here q - p = 6n + 4 entails 3|q, and q - p = 6n + 2 yields 3 | p*(q-p)-q."
			],
			"link": [
				"Chai Wah Wu, \u003ca href=\"/A180481/b180481.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"W. Sindelar, \u003ca href=\"http://groups.yahoo.com/group/primenumbers/message/22455\"\u003eCertain Pairs of Consecutive Prime Numbers\u003c/a\u003e, in yahoo group \"primenumbers\", Jan 20 2011.",
				"W. Sindelar, David Broadhurst, \u003ca href=\"/A180476/a180476.txt\"\u003eCertain Pairs of Consecutive Prime Numbers\u003c/a\u003e, digest of 2 messages in primenumbers Yahoo group, Jan 20 - Jan 21, 2011."
			],
			"formula": [
				"a(n) = A000040(n) + 6*A180476(n) for all n \u003e 2."
			],
			"mathematica": [
				"sp[p_]:=Module[{q=NextPrime[p]},While[AnyTrue[{p(q-p)+q,p(q-p)-q,q(q-p)+p,q(q-p)-p},CompositeQ],q=NextPrime[q]];q]; Join[{11,23},Table[sp[p],{p,Prime[Range[3,60]]}]] (* Requires Mathematica version 10 or later *) (* _Harvey P. Dale_, Mar 23 2021 *)"
			],
			"program": [
				"(PARI) A180481(p)={ forprime( q=1+p=prime(p), default(primelimit), isprime(p*(q-p)+q)|next; isprime(p*(q-p)-q)|next; isprime(q*(q-p)+p)|next; isprime(q*(q-p)-p)|next; return(q)) }",
				"(Python)",
				"from sympy import prime, isprime",
				"def A180481(n):",
				"....p = prime(n)",
				"....n += 1",
				"....q = prime(n)",
				"....while q \u003c 10**14: # note: search limit",
				"........if isprime(p*(q-p)+q) and isprime(p*(q-p)-q) and isprime(q*(q-p)+p) and isprime(q*(q-p)-p):",
				"............return(q)",
				"........n += 1",
				"........q = prime(n)",
				"....return(0) # limit in search for q was reached. A180481(n) may be \u003e 0",
				"# _Chai Wah Wu_, Aug 24 2014"
			],
			"xref": [
				"Cf. A180476."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_M. F. Hasler_, Jan 20 2011",
			"references": 2,
			"revision": 31,
			"time": "2021-03-23T14:09:46-04:00",
			"created": "2010-11-12T14:29:50-05:00"
		}
	]
}