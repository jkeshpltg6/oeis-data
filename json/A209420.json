{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A209420",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 209420,
			"data": "1,2,2,3,6,3,5,14,13,4,8,30,41,24,5,13,60,109,96,40,6,21,116,262,308,196,62,7,34,218,590,868,743,364,91,8,55,402,1267,2240,2413,1604,630,128,9,89,730,2627,5424,7046,5926,3186,1032,174,10,144,1310,5299,12516,19040,19382,13255,5928,1617,230",
			"name": "Triangle of coefficients of polynomials v(n,x) jointly generated with A209419; see the Formula section.",
			"comment": [
				"Column 1:  Fibonacci numbers (A000045).",
				"For a discussion and guide to related arrays, see A208510.",
				"Triangle given by (2, -1/2, -1/2, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (2, -1/2, 1/2, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Mar 26 2012"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A209420/b209420.txt\"\u003eTable of n, a(n) for the first 50 rows, flattened\u003c/a\u003e"
			],
			"formula": [
				"u(n,x) = x*u(n-1,x) + v(n-1,x),",
				"v(n,x) = (x+1)*u(n-1,x) + (x+1)*v(n-1,x),",
				"where u(1,x)=1, v(1,x)=1.",
				"T(n,k) = T(n-1,k) + 2*T(n-1,k-1) + T(n-2,k) - T(n-2,k-2), T(1,0) = 1, T(2,0) = T(2,1) = 2, T(n,k) = 0 if k\u003c0 or if k\u003e=n. - _Philippe Deléham_, Mar 26 2012",
				"G.f.: x*(1 + x)/(1 - x - x^2 - 2*y*x + y^2*x^2). - _G. C. Greubel_, Jan 03 2018"
			],
			"example": [
				"First five rows:",
				"  1;",
				"  2,  2;",
				"  3,  6,  3;",
				"  5, 14, 13,  4;",
				"  8, 30, 41, 24,  5;",
				"First three polynomials v(n,x): 1, 2 + 2x, 3 + 6x + 3x^2."
			],
			"mathematica": [
				"u[1, x_] := 1; v[1, x_] := 1; z = 16;",
				"u[n_, x_] := x*u[n - 1, x] + v[n - 1, x];",
				"v[n_, x_] := (x + 1)*u[n - 1, x] + (x + 1)*v[n - 1, x];",
				"Table[Expand[u[n, x]], {n, 1, z/2}]",
				"Table[Expand[v[n, x]], {n, 1, z/2}]",
				"cu = Table[CoefficientList[u[n, x], x], {n, 1, z}];",
				"TableForm[cu]",
				"Flatten[%]    (* A209419 *)",
				"Table[Expand[v[n, x]], {n, 1, z}]",
				"cv = Table[CoefficientList[v[n, x], x], {n, 1, z}];",
				"TableForm[cv]",
				"Flatten[%]    (* A209420 *)",
				"CoefficientList[CoefficientList[Series[x*(1 + x)/(1 - x - x^2 - 2*y*x + y^2*x^2), {x,0,10}, {y,0,10}], x], y] // Flatten (* _G. C. Greubel_, Jan 03 2018 *)"
			],
			"xref": [
				"Cf. A209419, A208510."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Mar 09 2012",
			"references": 3,
			"revision": 18,
			"time": "2018-01-04T01:35:12-05:00",
			"created": "2012-03-09T22:52:38-05:00"
		}
	]
}