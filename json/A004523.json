{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A004523",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 4523,
			"data": "0,0,1,2,2,3,4,4,5,6,6,7,8,8,9,10,10,11,12,12,13,14,14,15,16,16,17,18,18,19,20,20,21,22,22,23,24,24,25,26,26,27,28,28,29,30,30,31,32,32,33,34,34,35,36,36,37,38,38,39,40,40,41,42,42,43,44,44,45,46",
			"name": "Two even followed by one odd; or floor(2n/3).",
			"comment": [
				"Guenther Rosenbaum showed that the sequence represents the optimal number of guesses in the static Mastermind game with two pegs. Namely, the optimal number of static guesses equals 2k, if the number of colors is either (3k - 1) or 3k and is (2k + 1), if the number of colors is (3k + 1), k \u003e= 1. - Alex Bogomolny, Mar 06 2002",
				"First differences are in A011655. - _R. J. Mathar_, Mar 19 2008",
				"a(n+1) is the maximum number of wins by a team in a sequence of n basketball games if the team's longest winning streak is 2 games. See example below. In general, floor(k(n+1)/(k+1)) gives the maximum number of wins in n games when the longest winning streak is of length k. - _Dennis P. Walsh_, Apr 18 2012",
				"Sum_{n\u003e=2} 1/a(n)^k = Sum_{j\u003e=1} Sum_{i=1..2} 1/(i*j)^k = Zeta(k)^2 - Zeta(k)*Zeta(k,3), where Zeta(,) is the generalized Riemann Zeta function, for the case k=2 this sum is 5*Pi^2/24. - _Enrique Pérez Herrero_, Jun 25 2012",
				"a(n) is the pattern of (0+2k, 0+2k, 1+2k), k\u003e=0. a(n) is also the number of odd integers divisible by 3 in ]2(n-1)^2, 2n^2[. - _Ralf Steiner_, Jun 25 2017 [seems to be INCORRECT, someone please check!]",
				"a(n) is also the total domination number of the n-trianguar (Johnson) graph for n \u003e 2. - _Eric W. Weisstein_, Apr 09 2018",
				"a(n) is the maximum total domination number of connected graphs with order n\u003e2.  The extremal graphs are \"brushes\", as defined in the links below. - _Allan Bickle_, Dec 24 2021"
			],
			"reference": [
				"R. C. Brigham, J. R. Carrington, and R. P. Vitray, Connected graphs with maximum total domination number. J. Combin. Comput. Combin. Math. 34 (2000), 81-96.",
				"Hsien-Kuei Hwang, S Janson, TH Tsai, Exact and asymptotic solutions of the recurrence f(n) = f(floor(n/2)) + f(ceiling(n/2)) + g(n): theory and applications, Preprint, 2016; http://140.109.74.92/hk/wp-content/files/2016/12/aat-hhrr-1.pdf. Also Exact and Asymptotic Solutions of a Divide-and-Conquer Recurrence Dividing at Half: Theory and Applications, ACM Transactions on Algorithms, 13:4 (2017), #47; DOI: 10.1145/3127585"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A004523/b004523.txt\"\u003eTable of n, a(n) for n = 0..2000\u003c/a\u003e",
				"Allan Bickle, \u003ca href=\"https://doi.org/10.7151/dmgt.1655\"\u003eTwo Short Proofs on Total Domination\u003c/a\u003e, Discuss Math Graph Theory, 33 2 (2013), 457-459.",
				"Alex Bogomolny and Don Greenwell, \u003ca href=\"http://www.cut-the-knot.org/ctk/Mastermind.shtml\"\u003eStatic Mastermind Game\u003c/a\u003e, Cut The Knot!, December 1999.",
				"E. J. Cockayne, R. M. Dawes, and S. T. Hedetniemi, \u003ca href=\"https://doi.org/10.1002/net.3230100304\"\u003eTotal domination in graphs\u003c/a\u003e, Networks 10 (1980), 211-219.",
				"Francis Laclé, \u003ca href=\"https://hal.archives-ouvertes.fr/hal-03201180v2\"\u003e2-adic parity explorations of the 3n+ 1 problem\u003c/a\u003e, hal-03201180v2 [cs.DM], 2021.",
				"G. Rosenbaum, \u003ca href=\"http://www.rosenbaum-games.de/3m/p1/p0071.htm\"\u003e(Static-)Mastermind\u003c/a\u003e",
				"Paul B. Slater, \u003ca href=\"http://arxiv.org/abs/1609.08561\"\u003eFormulas for Generalized Two-Qubit Separability Probabilities\u003c/a\u003e, arXiv:1609.08561 [quant-ph], 2016.",
				"Paul B. Slater, \u003ca href=\"http://arxiv.org/abs/1504.04555\"\u003eHypergeometric/Difference-Equation-Based Separability Probability Formulas and Their Asymptotics for Generalized Two-Qubit States Endowed with Random Induced Measure\u003c/a\u003e, arXiv:1504.04555 [quant-ph], 2015.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/JohnsonGraph.html\"\u003eJohnson Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/TotalDominationNumber.html\"\u003eTotal Domination Number\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/TriangularGraph.html\"\u003eTriangular Graph\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,0,1,-1)."
			],
			"formula": [
				"G.f.: (x^2 + 2*x^3 + 2*x^4 + x^5)/(1 - x^3)^2, not reduced. - _Len Smiley_",
				"a(n) = floor(2*n/3).",
				"a(0) = a(1) = 0; for n\u003e1, a(n) = n - 1 - floor(a(n-1)/2). - _Benoit Cloitre_, Nov 26 2002",
				"a(n) = a(n-1) + (1/2)((-1)^floor((2*n+2)/3)+1), with a(0)=0. - Mario Catalani (mario.catalani(AT)unito.it), Oct 20 2003",
				"a(n) = Sum_{k=0..n-1} (Fibonacci(k) mod 2). - _Paul Barry_, May 31 2005",
				"a(n) = A004773(n) - A004396(n). - _Reinhard Zumkeller_, Aug 29 2005",
				"O.g.f.: x^2*(1 + x)/((1 - x)^2*(1 + x + x^2)). - _R. J. Mathar_, Mar 19 2008",
				"a(n) = 2*(-1 + Sum_{k=0..n} (1/9*(-2*(k mod 3) + ((k+1) mod 3) + 4*((k+2) mod 3))) + (((n+2) mod 3) mod 2). - _Paolo P. Lava_, Oct 02 2008",
				"a(n) = ceiling(2*(n-1)/3) = n-1-floor((n-1)/3). - _Bruno Berselli_, Jan 18 2017",
				"a(n) = (6*n-3+2*sqrt(3)*sin(2*(n-2)*Pi/3))/9. - _Wesley Ivan Hurt_, Sep 30 2017"
			],
			"example": [
				"For n=11, we have a(11)=7 since there are at most 7 wins by a team in a sequence of 10 games in which its longest winning streak is 2 games. One such win-loss sequence with 7 wins is wwlwwlwwlw. - _Dennis P. Walsh_, Apr 18 2012"
			],
			"maple": [
				"seq(floor(2n/3), n=0..75);"
			],
			"mathematica": [
				"Table[Floor[2 n/3], {n, 0, 75}]",
				"Table[(6 n + 3 Cos[2 n Pi/3] - Sqrt[3] Sin[2 n Pi/3] - 3)/9, {n, 0, 20}] (* _Eric W. Weisstein_, Apr 08 2018 *)",
				"Floor[2 Range[0, 20]/3] (* _Eric W. Weisstein_, Apr 08 2018 *)",
				"LinearRecurrence[{1, 0, 1, -1}, {0, 1, 2, 2}, {0, 20}] (* _Eric W. Weisstein_, Apr 08 2018 *)",
				"CoefficientList[Series[x^2 (1 + x)/((-1 + x)^2 (1 + x + x^2)), {x, 0, 20}], x] (* _Eric W. Weisstein_, Apr 08 2018 *)",
				"Table[If[EvenQ[n],{n,n},n],{n,0,50}]//Flatten (* _Harvey P. Dale_, May 27 2021 *)"
			],
			"program": [
				"(Haskell)",
				"a004523 n = a004523_list !! n",
				"a004523_list = 0 : 0 : 1 : map (+ 2) a004523_list",
				"-- _Reinhard Zumkeller_, Nov 06 2012",
				"(PARI) a(n)=2*n\\3 \\\\ _Charles R Greathouse IV_, Sep 02 2015",
				"(MAGMA) [Floor(2*n/3): n in [0..50]]; // _G. C. Greubel_, Nov 02 2017"
			],
			"xref": [
				"Cf. A004396, A291778, A291779.",
				"Zero followed by partial sums of A011655."
			],
			"keyword": "nonn,easy",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Dead link fixed by _Nathaniel Johnston_, Sep 20 2012"
			],
			"references": 61,
			"revision": 125,
			"time": "2021-12-25T02:46:51-05:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}