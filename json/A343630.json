{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A343630",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 343630,
			"data": "0,0,0,0,0,1,1,0,0,0,1,0,-1,0,0,0,-1,0,0,0,-1,1,0,-1,0,1,-1,-1,0,-1,0,-1,-1,1,1,0,-1,1,0,-1,-1,0,1,-1,0,1,0,1,0,1,1,-1,0,1,0,-1,1,1,1,1,-1,1,1,-1,-1,1,1,-1,1,1,1,-1,-1,1,-1,-1,-1,-1,1,-1,-1,0,0,-2,2,0,0,0,2,0,-2,0,0,0,-2,0,0,0",
			"name": "Coordinate triples (x(n), y(n), z(n); n \u003e= 0) of the 3D spiral filling space with shells of increasing radius, using circles at fixed z-values which alternatingly move up and down as do the x-values.",
			"comment": [
				"This is a 3D generalization of a plane filling spiral using the Euclidean norm.",
				"See A343640 for an analog using the sup- or oo-norm, where circles are squares and spheres are cubes.",
				"The integer lattice points, Z^3, are listed in order of increasing Euclidean distance R^2 = x^2 + y^2 + z^2 from the origin. Each shell of given radius is filled using circles located at given latitude (i.e., z-value) on the sphere, and each circle is filled by points with increasing longitude, where the positive x axis corresponds to longitude 0. The latitudes / z-values are alternately increasing and decreasing (so over a period of two shells they follow the same cosine-type shape as the x-values do over the period of each circle).",
				"The sequence can be seen as a table with row length of 3, where each row corresponds to the (x,y,z)-coordinates of one point (the three columns are then A343631, A343632 and A343633), or as a table with row lengths 3*A005875, where A005875(r) is the number of points at distance sqrt(r) from the origin.",
				"Sequence A343640 gives a square spiral variant."
			],
			"example": [
				"Shell r = 0 is the origin, {(0,0,0)}.",
				"Shell r = 1 contains the 6 points {(0,0,1), (1,0,0), (0,1,0), (-1,0,0), (0,-1,0), (0,0,-1)}, located on the North pole, equator and South pole of the unit sphere. The equator (as all circles in the sequel) is \"scanned\" by increasing longitude = polar coordinate phi in the (x,y) plane with given z, where (x,y,z) = (R,0,0) has longitude 0.",
				"Shell r = R^2 = 2 contains the 12 points (now in order of increasing z-coordinate) {(1,0,-1), (0,1,-1), (-1,0,-1), (0,-1,-1); (1,1,0), (-1,1,0), (-1,-1,0), (1,-1,0); (1,0,1), (0,1,1), (-1,0,1), (0,-1,1)}.",
				"Then again, the points of shell r = R^2 = 3 are ordered by decreasing z-coordinate.",
				"There are no points in shell r = R^2 = 7 = A004215(1), so from there on up to the next empty shell, the shells with even r are filled by decreasing z-coordinate."
			],
			"program": [
				"(PARI) A343630_row(n, dir=(-1)^n, Q=Qfb(1, 0, 1), L=List())={for(z=if(n, sqrtint((n-1)\\3)+1), sqrtint(n), my(S=if(n\u003ez^2, Set(apply(vecsort, abs(qfbsolve(Q, n-z^2, 3)))), [[0, 0]])); foreach(S, s, forperm(concat(s, z), p, listput(L, p)))); for(i=1, 3, for(j=1, #L, my(X=L[j]); (X[i]*=-1) \u0026\u0026 listput(L, X))); vecsort(L, (p, q)-\u003eif( p[3]!=q[3], (p[3]-q[3])*dir, p[1]==q[1], q[2]-p[2], p[2]*q[2]\u003c0, q[2]-p[2], (q[1]-p[1])*(p[2]+q[2])))} \\\\ returns row n of the table, i.e., the list of points (x,y,z) in Z^3 with Euclidean norm equal to sqrt(n), sorted by by increasing latitude for dir = +1, else decreasing, and increasing longitude.",
				"A343630_vec=concat([[Vec(P) | P\u003c-A343630_row(n)] | n\u003c-[0..6]]) \\\\ beyond the empty row 7 one must correct the second argument, e.g. by using {... P\u003c-S=A343630_row(n,d)]+(#S\u0026\u0026!d*=-1) ...} to flip the sign of d, initialized to 1, at each nonempty shell."
			],
			"xref": [
				"Cf. A343631, A343632, A343633 (list of x, y resp. z-coordinates only).",
				"Cf. A343640, A343641, A343642, A343643 (variant using the sup norm =\u003e square spiral).",
				"Cf. A342561, A342562, A342563 for a variant which scans each sphere by increasing z.",
				"Cf. A005875 (number of points on a shell with given radius).",
				"Cf. A004215 (numbers that can't be written as sum of 3 squares =\u003e empty shells)."
			],
			"keyword": "sign",
			"offset": "0,84",
			"author": "_M. F. Hasler_, Apr 28 2021",
			"references": 6,
			"revision": 13,
			"time": "2021-05-01T21:52:35-04:00",
			"created": "2021-05-01T21:52:35-04:00"
		}
	]
}