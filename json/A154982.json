{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A154982",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 154982,
			"data": "1,1,1,1,4,1,1,9,9,1,1,18,50,18,1,1,35,212,212,35,1,1,68,823,2024,823,68,1,1,133,3131,16415,16415,3131,133,1,1,262,11968,124890,291902,124890,11968,262,1,1,519,46278,938394,4619032,4619032,938394,46278,519,1",
			"name": "Triangle T(n, k, m) = coefficients of p(x, n, m) where p(x,n,m) = (x+1)*p(x, n-1, m) + 2^(m+n-1) *x*p(x, n-2, m) and m=0, read by rows.",
			"comment": [
				"Row sums are: {1, 2, 6, 20, 88, 496, 3808, 39360, 566144, 11208448, ...}."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A154982/b154982.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k, m) = coefficients of p(x, n, m) where p(x,n,m) = (x+1)*p(x, n-1, m) + 2^(m+n-1) *x*p(x, n-2, m) and m=0.",
				"T(n, k, m) = T(n-1, k, m) + T(n-1, k-1, m) + 2^(n+m-1)*T(n-2, k-1, m) with T(n, 0, m) = T(n, n, m) = 1 and m=0. - _G. C. Greubel_, Mar 01 2021"
			],
			"example": [
				"Triangle begins as:",
				"  1;",
				"  1,   1;",
				"  1,   4,     1;",
				"  1,   9,     9,      1;",
				"  1,  18,    50,     18,       1;",
				"  1,  35,   212,    212,      35,       1;",
				"  1,  68,   823,   2024,     823,      68,      1;",
				"  1, 133,  3131,  16415,   16415,    3131,    133,     1;",
				"  1, 262, 11968, 124890,  291902,  124890,  11968,   262,   1;",
				"  1, 519, 46278, 938394, 4619032, 4619032, 938394, 46278, 519, 1;"
			],
			"mathematica": [
				"(* First program *)",
				"p[x_, n_, m_]:= p[x,n,m] = If[n\u003c2, n*x+1, (x+1)*p[x,n-1,m] + 2^(m+n-1)*x*p[x, n-2, m]];",
				"Table[CoefficientList[ExpandAll[p[x,n,0]], x], {n,0,12}]//Flatten (* modified by _G. C. Greubel_, Mar 01 2021 *)",
				"(* Second program *)",
				"T[n_, k_, m_]:= T[n,k,m] = If[k==0 || k==n, 1, T[n-1, k, m] + T[n-1, k-1, m] + 2^(n+m-1)*T[n-2, k-1, m]];",
				"Table[T[n,k,0], {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, Mar 01 2021 *)"
			],
			"program": [
				"(Sage)",
				"def T(n,k,m):",
				"    if (k==0 or k==n): return 1",
				"    else: return T(n-1, k, m) + T(n-1, k-1, m) + 2^(n+m-1)*T(n-2, k-1, m)",
				"flatten([[T(n,k,0) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Mar 01 2021",
				"(Magma)",
				"function T(n,k,m)",
				"  if k eq 0 or k eq n then return 1;",
				"  else return T(n-1, k, m) + T(n-1, k-1, m) + 2^(n+m-1)*T(n-2, k-1, m);",
				"  end if; return T;",
				"end function;",
				"[T(n,k,0): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Mar 01 2021"
			],
			"xref": [
				"Cf. this sequence (m=0), A154980 (m=1), A154979 (m=3)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Roger L. Bagula_, Jan 18 2009",
			"ext": [
				"Edited by _G. C. Greubel_, Mar 01 2021"
			],
			"references": 7,
			"revision": 5,
			"time": "2021-03-01T17:53:41-05:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}