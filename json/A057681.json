{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A057681",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 57681,
			"data": "1,1,1,0,-3,-9,-18,-27,-27,0,81,243,486,729,729,0,-2187,-6561,-13122,-19683,-19683,0,59049,177147,354294,531441,531441,0,-1594323,-4782969,-9565938,-14348907,-14348907,0,43046721,129140163,258280326,387420489,387420489",
			"name": "a(n) = Sum_{j=0..floor(n/3)} (-1)^j*binomial(n,3*j).",
			"comment": [
				"Let M be any endomorphism on any vector space, such that M^3 = 1 (identity). Then (1-M)^n = a(n)-A057682(n)*M+z(n)*M^2, where z(0)=z(1)=0 and, apparently, z(n+2)=A057083(n). - _Stanislav Sykora_, Jun 10 2012",
				"Pisano period lengths: 1, 3, 1, 6, 24, 3, 6, 12, 1, 24, 60, 6, 12, 6, 24, 24, 96, 3, 18, 24, ... . - _R. J. Mathar_, Aug 10 2012",
				"{A057681, A057682, A*}, where A* is A057083 prefixed by two 0's, is the difference analog of the trigonometric functions of order 3, {k_1(x), k_2(x), k_3(x)}. For a definition see [Erdelyi] and the Shevelev link. - _Vladimir Shevelev_, Jun 25 2017"
			],
			"reference": [
				"A. Erdelyi, Higher Transcendental Functions, McGraw-Hill, 1955, Vol. 3, Chapter XVIII."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A057681/b057681.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"T. Alden Gassert, \u003ca href=\"http://arxiv.org/abs/1409.7829\"\u003eDiscriminants of simplest 3^n-tic extensions\u003c/a\u003e, arXiv:1409.7829 [math.NT], 2014.",
				"Mark W. Coffey, \u003ca href=\"http://arxiv.org/abs/1506.09160\"\u003eReductions of particular hypergeometric functions 3F2 (a, a+1/3, a+2/3; p/3, q/3; +-1)\u003c/a\u003e, arXiv:1506.09160 [math.CA], 2015.",
				"John B. Dobson, \u003ca href=\"http://arxiv.org/abs/1610.09361\"\u003eA matrix variation on Ramus's identity for lacunary sums of binomial coefficients\u003c/a\u003e, arXiv:1610.09361, 2016",
				"Ira Gessel, \u003ca href=\"http://www.cs.brandeis.edu/~ira/\"\u003eThe Smith College diploma problem\u003c/a\u003e.",
				"Vladimir Shevelev, \u003ca href=\"https://arxiv.org/abs/1706.01454\"\u003eCombinatorial identities generated by difference analogs of hyperbolic and trigonometric functions of order n\u003c/a\u003e, arXiv:1706.01454 [math.CO], 2017.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3)."
			],
			"formula": [
				"From _Paul Barry_, Feb 26 2004: (Start)",
				"G.f.: (1-x)^2/((1-x)^3+x^3).",
				"a(n) = 0^n/3 + 2*3^((n-2)/2)*cos(Pi*n/6). (End)",
				"From _Paul Barry_, Feb 27 2004: (Start)",
				"Binomial transform of (1, 0, 0, -1, 0, 0, 1, 0, 0, -1, 0, ...).",
				"E.g.f.: 2*exp(3x/2)*cos(sqrt(3)*x/2)/3+1/3.",
				"a(n) = (((3+sqrt(-3))/2)^n+((3-sqrt(-3))/2)^n)/3+0^n/3. (End)",
				"a(n) = 6*a(n-1)-15*a(n-2)+20*a(n-3)-15*a(n-4)+6*a(n-5). - _Paul Curtz_, Jan 02 2008",
				"Start with x(0)=1,y(0)=0,z(0)=0 and set x(n+1)=x(n)-z(n), y(n+1)=y(n)-x(n),z(n+1)=z(n)-y(n). Then a(n)=x(n). But this recurrence falls into a repetitive cycle of length 6 and multiplicative factor -27, so that a(n) = -27*a(n-6) for any n\u003e6. - _Stanislav Sykora_, Jun 10 2012",
				"E.g.f.: (1+2*exp(3*z/2)*cos(z*sqrt(3/4)))/3. - _Peter Luschny_, Jul 10 2012",
				"a(0)=a(1)=a(2)=1, a(n)=3*a(n-1)-3*a(n-2), n\u003e=3. - _Wesley Ivan Hurt_, Nov 11 2014",
				"For n\u003e=1, a(n) = 2*3^((n-2)/2)*cos(Pi*n/6). - _Vladimir Shevelev_, Jun 25 2017",
				"a(n+m) = a(n)*a(m)-A057682(n)*A*057083(m)-A*057083(n)*A057682(m), where A*057083 is A057083 prefixed by two 0's. - _Vladimir Shevelev_, Jun 25 2017"
			],
			"example": [
				"If M^3=1 then (1-M)^6 = a(6)-A057682(6)*M+A057083(4)*M^2 = -18+9*M+9*M^2."
			],
			"maple": [
				"A057681 := n-\u003eadd((-1)^j*binomial(n,3*j),j=0..floor(n/3)); seq(A057681(n), n=0..50);",
				"A057681_list := proc(n) local i; series((1+2*exp(3*z/2)*cos(z*sqrt(3/4)))/3, z,n+2): seq(i!*coeff(%,z,i),i=0..n) end: A057681_list(38); # _Peter Luschny_, Jul 10 2012"
			],
			"mathematica": [
				"Join[{1},LinearRecurrence[{3,-3},{1,1},40]] (* _Harvey P. Dale_, Aug 19 2014 *)"
			],
			"program": [
				"(PARI) x='x+O('x^40); Vec((1-x)^2/((1-x)^3+x^3)) \\\\ _G. C. Greubel_, Oct 23 2018",
				"(MAGMA) I:=[1,1]; [1] cat [n le 2 select I[n] else 3*Self(n-1) - 3*Self(n-2): n in [1..40]]; // _G. C. Greubel_, Oct 23 2018",
				"(GAP) a:=[1,1];; for n in [3..40] do a[n]:=3*a[n-1]-3*a[n-2]; od; Concatenation([1],a); # _Muniru A Asiru_, Oct 24 2018"
			],
			"xref": [
				"Cf. A009116, A009545, A057682, A057083, A103312."
			],
			"keyword": "sign,easy",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_, Oct 20 2000",
			"references": 12,
			"revision": 66,
			"time": "2018-10-24T09:50:42-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}