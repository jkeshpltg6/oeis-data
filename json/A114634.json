{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A114634",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 114634,
			"data": "6,14,16,18,34,36,40,42,44,46,50,52,56,60,62,74,88,98,100,122,124,130,132,135,138,142,148,152,156,158,170,178,186,189,194,196,209,226,232,242,243,244,258,260,266,274,282,292,296,297,302,308,314,315,316,322",
			"name": "Numbers n such that n-th octagonal number is 6-almost prime.",
			"comment": [
				"It is necessary but not sufficient that n must be prime (A000040), semiprime (A001358), 3-almost prime (A014612), 4-almost prime (A014613), or 5-almost prime (A014614)."
			],
			"link": [
				"Harvey P. Dale, \u003ca href=\"/A114634/b114634.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/OctagonalNumber.html\"\u003eOctagonal Number.\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/AlmostPrime.html\"\u003eAlmost Prime.\u003c/a\u003e"
			],
			"formula": [
				"n such that n*(3*n-2) has exactly six prime factors (with multiplicity). n such that A000567(n) is an element of A046306. n such that A001222(A000567(n)) = 6. n such that A001222(n) + A001222(3*n-2) = 6. n such that [(3*n-2)*(3*n-1)*(3*n)]/[(3*n-2)+(3*n-1)+(3*n)] is an element of A046306."
			],
			"example": [
				"a(1) = 6 because OctagonalNumber(6) = Oct(6) = 6*(3*6-2) = 96 = 2^5 * 3 has exactly 6 prime factors (five are all equally 2; factors need not be distinct).",
				"a(2) = 14 because Oct(14) = 14*(3*14-2) = 560 = 2^4 * 5 * 7 is 6-almost prime.",
				"a(3) = 16 because Oct(16) = 16*(3*16-2) = 736 = 2^5 * 23.",
				"a(7) = 40 because Oct(40) = 40*(3*40-2) = 4720 = 2^4 * 5 * 59 [also, 4720 = Oct(40) = Oct(Oct(4)), an iterated octagonal number].",
				"a(19) = 100 because Oct(100) = 100*(3*100-2) = 29800 = 2^3 * 5^2 * 149."
			],
			"mathematica": [
				"Flatten[Position[Table[n(3n-2),{n,400}],_?(PrimeOmega[#]==6\u0026)]] (* _Harvey P. Dale_, Jun 17 2013 *)"
			],
			"program": [
				"(PARI) is(n)=my(t=bigomega(3*n-2)); t\u003c6 \u0026\u0026 (t\u003c5 || !isprime(n)) \u0026\u0026 t+bigomega(n)==6 \\\\ _Charles R Greathouse IV_, Feb 01 2017"
			],
			"xref": [
				"Cf. A000040, A000567, A001222, A001358, A014612, A014613, A014614, A046306."
			],
			"keyword": "easy,nonn",
			"offset": "1,1",
			"author": "_Jonathan Vos Post_, Feb 17 2006",
			"references": 2,
			"revision": 12,
			"time": "2017-02-01T22:36:47-05:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}