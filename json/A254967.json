{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A254967",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 254967,
			"data": "1,2,3,2,4,7,0,2,2,9,0,0,2,4,13,0,0,0,2,2,15,2,2,2,2,4,6,21,2,0,2,0,2,2,4,25,2,0,0,2,2,0,2,6,31,0,2,2,2,0,2,2,4,2,33,0,0,2,0,2,2,0,2,2,4,37,0,0,0,2,2,0,2,2,0,2,6,43,2,2,2,2,0,2",
			"name": "Triangle of iterated absolute differences of lucky numbers read by antidiagonals upwards.",
			"comment": [
				"This sequence is related to the lucky numbers (cf. A000959) in the same way as A036262 is related to the prime numbers;",
				"T(n,0) = A054978(n);",
				"T(2*n,n) = A254969(n);",
				"T(n,n-1) = A031883(n), n \u003e 0;",
				"T(n,n) = A000959(n+1);",
				"for n \u003e 0: T(n,k) = abs(T(n,k+1) - T(n-1,k), k = 0 .. n-1."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A254967/b254967.txt\"\u003eRows n = 0..125 of triangle, flattened\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/LuckyNumber.html\"\u003eLucky number.\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Lucky_number\"\u003eLucky number\u003c/a\u003e"
			],
			"example": [
				".   0:                      1",
				".   1:                     2 3",
				".   2:                    2 4 7",
				".   3:                   0 2 2 9",
				".   4:                  0 0 2 4 13",
				".   5:                 0 0 0 2 2 15",
				".   6:                2 2 2 2 4 6 21",
				".   7:               2 0 2 0 2 2 4 25",
				".   8:              2 0 0 2 2 0 2 6 31",
				".   9:             0 2 2 2 0 2 2 4 2 33",
				".  10:            0 0 2 0 2 2 0 2 2 4 37",
				".  11:           0 0 0 2 2 0 2 2 0 2 6 43",
				".  12:          2 2 2 2 0 2 2 0 2 2 0 6 49",
				".  13:         0 2 0 2 0 0 2 0 0 2 4 4 2 51 ."
			],
			"mathematica": [
				"nmax = 13; (* max index for triangle rows *)",
				"imax = 25; (* max index for initial lucky array L *)",
				"L = Table[2i + 1, {i, 0, imax}];",
				"For[n = 2, n \u003c Length[L], r = L[[n++]]; L = ReplacePart[L, Table[r*i -\u003e Nothing, {i, 1, Length[L]/r}]]];",
				"T[n_, n_] := If[n+1 \u003c= Length[L], L[[n+1]], Print[\"imax should be increased\"]; 0];",
				"T[n_, k_] := T[n, k] = Abs[T[n, k+1] - T[n-1, k]];",
				"Table[T[n, k], {n, 0, nmax}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Sep 22 2021 *)"
			],
			"program": [
				"(Haskell)",
				"a254967 n k = a254967_tabl !! n !! k",
				"a254967_row n = a254967_tabl !! n",
				"a254967_tabl = diags [] $",
				"   iterate (\\lds -\u003e map abs $ zipWith (-) (tail lds) lds) a000959_list",
				"   where diags uss (vs:vss) = (map head wss) : diags (map tail wss) vss",
				"                              where wss = vs : uss"
			],
			"xref": [
				"Cf. A054978 (left edge), A254969 (central terms), A000959 (right edge), A031883, A036262."
			],
			"keyword": "nonn,tabl",
			"offset": "0,2",
			"author": "_Reinhard Zumkeller_, Feb 11 2015",
			"references": 6,
			"revision": 13,
			"time": "2021-09-23T00:32:45-04:00",
			"created": "2015-02-11T12:52:04-05:00"
		}
	]
}