{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A286653",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 286653,
			"data": "1,1,0,1,1,0,1,1,1,0,1,1,2,2,0,1,1,2,2,2,0,1,1,2,3,4,3,0,1,1,2,3,4,5,4,0,1,1,2,3,5,6,7,5,0,1,1,2,3,5,6,9,9,6,0,1,1,2,3,5,7,10,12,13,8,0,1,1,2,3,5,7,10,13,16,16,10,0,1,1,2,3,5,7,11,14,19,22,22,12,0",
			"name": "Square array A(n,k), n\u003e=0, k\u003e=1, read by antidiagonals, where column k is the expansion of Product_{j\u003e=1} (1 - x^(k*j))/(1 - x^j).",
			"comment": [
				"A(n,k) is the number of partitions of n in which no parts are multiples of k.",
				"A(n,k) is also the number of partitions of n into at most k-1 copies of each part."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A286653/b286653.txt\"\u003eAntidiagonals n = 0..139, flattened\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PartitionFunctionb.html\"\u003ePartition Function b_k\u003c/a\u003e",
				"\u003ca href=\"/index/Par#part\"\u003eIndex entries for sequences related to partitions\u003c/a\u003e"
			],
			"formula": [
				"G.f. of column k: Product_{j\u003e=1} (1 - x^(k*j))/(1 - x^j)."
			],
			"example": [
				"Square array begins:",
				"  1,  1,  1,  1,  1,  1,  ...",
				"  0,  1,  1,  1,  1,  1,  ...",
				"  0,  1,  2,  2,  2,  2,  ...",
				"  0,  2,  2,  3,  3,  3,  ...",
				"  0,  2,  4,  4,  5,  5,  ...",
				"  0,  3,  5,  6,  6,  7,  ..."
			],
			"maple": [
				"b:= proc(n, i, k) option remember; `if`(n=0, [1, 0], `if`(k*i*(i+1)/2\u003cn, 0,",
				"      add((l-\u003e[0, l[1]*j]+l)(b(n-i*j, i-1, k)), j=0..min(n/i, k))))",
				"    end:",
				"A:= (n, k)-\u003e b(n$2, k-1)[1]:",
				"seq(seq(A(n, 1+d-n), n=0..d), d=0..16);  # _Alois P. Heinz_, Oct 17 2018"
			],
			"mathematica": [
				"Table[Function[k, SeriesCoefficient[Product[(1 - x^(i k))/(1 - x^i), {i, Infinity}], {x, 0, n}]][j - n + 1], {j, 0, 12}, {n, 0, j}] // Flatten",
				"Table[Function[k, SeriesCoefficient[QPochhammer[x^k, x^k]/QPochhammer[x, x], {x, 0, n}]][j - n + 1], {j, 0, 12}, {n, 0, j}] // Flatten"
			],
			"xref": [
				"Columns k=1-13 give: A000007, A000009, A000726, A001935, A035959, A219601, A035985, A261775, A104502, A261776, A328545, A328546, A341714.",
				"Main diagonal gives A000041.",
				"Mirror of A061198.",
				"Cf. A061199, A210485."
			],
			"keyword": "nonn,tabl",
			"offset": "0,13",
			"author": "_Ilya Gutkovskiy_, May 11 2017",
			"references": 8,
			"revision": 25,
			"time": "2021-06-19T12:37:46-04:00",
			"created": "2017-05-14T00:01:40-04:00"
		}
	]
}