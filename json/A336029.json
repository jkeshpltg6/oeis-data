{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A336029",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 336029,
			"data": "1,4,0,7,5,5,5,2,3,5,7,8,6,0,0,6,8,6,7,0,6,2,6,9,5,2,8,7,0,3,7,6,7,6,8,9,1,8,2,9,8,7,6,6,5,8,2,1,0,0,4,2,9,6,8,2,7,3,5,7,2,1,1,9,1,3,2,3,3,4,3,7,3,2,5,5,1,9,1,2,1,7,5,7,0,2,1,3,9,8,2,5,0,8,8,9,8,8,5,4,9,2,6,0,8,9,5,3,7,7,5,1",
			"name": "Given the two curves y = (1 - exp(x/2))/(exp(x) + exp(x/2)) and y = (exp(-x) - 1)/2, draw a line tangent to both. This sequence is the decimal expansion of the y-coordinate (negated) of the point at which the line touches y = (exp(-x) - 1)/2.",
			"comment": [
				"This constant is involved in the calculation of Gauchman's constant -A243261 (which equals A086278 - 1).",
				"Gauchman's constant is the point where the common tangent to the two curves y = (1 - exp(x/2))/(exp(x) + exp(x/2)) and y = (exp(-x) - 1)/2 intersects the y-axis."
			],
			"reference": [
				"Steven R. Finch, Mathematical Constants, Cambridge University Press, 2003, Section 3.1, Shapiro-Drinfeld constant, p. 210.",
				"Hillel Gauchman, Solution to Problem 10528(b), unpublished note, 1998."
			],
			"link": [
				"Vasile Cârtoaje, Jeremy Dawson and Hans Volkmer, \u003ca href=\"https://www.jstor.org/stable/3109827\"\u003eSolution to Problem 10528(a,b)\u003c/a\u003e, American Mathematical Monthly, 105 (1998), 473-474. [A comment was made about Hillel Gauchman's solution to part (b) of the problem that involves this constant, but no solution was published.]",
				"Petros Hadjicostas, \u003ca href=\"/A243261/a243261.pdf\"\u003ePlot of the curves y = (1 - exp(x/2))/(exp(x) + exp(x/2)) and y = (exp(-x) - 1)/2 and their common tangent\u003c/a\u003e, 2020."
			],
			"formula": [
				"We solve the following system of equations:",
				"exp(-c) = (exp(b/2) + 2*exp(b) - exp(3*b/2))/(exp(b) + exp(b/2))^2 and",
				"2*(1 - exp(b/2)) = (exp(b) + exp(b/2))*(exp(-c)*(1 + c - b) - 1).",
				"Then the constant equals C = (exp(-c) - 1)/2.",
				"It turns out that b = -A335810 = -0.387552... and c = A335809 = 0.330604... even though A335810 and A335809 are also involved in the calculation of the Shapiro cyclic sum constant mu (A086278).",
				"As a result, the constant C equals A335822 - 1.",
				"Gauchman's constant = -A243261 = C - (C + 1/2)*log(2*C + 1).",
				"Shapiro cyclic sum constant mu = A086278 = 1 + C - (C + 1/2)*log(2*C + 1)."
			],
			"example": [
				"-0.14075552357860068670626952870376... = -1 + 0.859244476421399313293730471..."
			],
			"program": [
				"(PARI) default(\"realprecision\", 200)",
				"c(b) = -log((exp(b/2) + 2*exp(b) - exp(3*b/2))/(exp(b) + exp(b/2))^2);",
				"a = solve(b=-2, 0, (exp(b) + exp(b/2))*(-1 + exp(-c(b))*(1 + c(b) - b)) - 2*(1 - exp(b/2)));",
				"(exp(-c(a))-1)/2"
			],
			"xref": [
				"Cf. A086278, A243261, A335809 (c), A335810 (-b), A335822 (1 plus constant), A336011 (y-coordinate for b)."
			],
			"keyword": "nonn,cons",
			"offset": "0,2",
			"author": "_Petros Hadjicostas_, Jul 05 2020",
			"references": 1,
			"revision": 27,
			"time": "2020-07-13T10:07:58-04:00",
			"created": "2020-07-13T02:19:42-04:00"
		}
	]
}