{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A152545",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 152545,
			"data": "1,1,1,1,2,1,2,2,1,3,2,2,1,4,3,3,2,1,5,4,4,3,3,1,1,7,5,5,5,4,3,3,1,1,9,7,7,7,5,5,5,4,3,1,1,1,12,9,9,9,8,7,7,7,5,4,4,4,1,1,1,1,16,12,12,12,12,9,9,9,8,8,8,7,5,4,4,4,1,1,1,1,1,21,16,16,16,16,13,12,12,12,12,12,11,9,8",
			"name": "Padovan-Fibonacci triangle, read by rows, where the first column equals the Padovan spiral numbers (A134816), while the row sums equal the Fibonacci numbers (A000045).",
			"comment": [
				"The number of terms in each row equal the Padovan spiral numbers (A134816, with offset)."
			],
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A152545/b152545.txt\"\u003eTable of n, a(n) for n = 0..261\u003c/a\u003e"
			],
			"formula": [
				"G.f. for row n: Sum_{k=0..A000931(n+5)-1} (x^{T(n-1,k)+T(n-2,k)} - 1)/(x-1) = Sum_{k=0..A000931(n+6)-1} T(n,k)*x^k for n\u003e1 with T(0,0)=T(1,0)=1, where A000931 is the Padovan sequence."
			],
			"example": [
				"Triangle begins:",
				"[1],",
				"[1],",
				"[1,1],",
				"[2,1],",
				"[2,2,1],",
				"[3,2,2,1],",
				"[4,3,3,2,1],",
				"[5,4,4,3,3,1,1],",
				"[7,5,5,5,4,3,3,1,1],",
				"[9,7,7,7,5,5,5,4,3,1,1,1],",
				"[12,9,9,9,8,7,7,7,5,4,4,4,1,1,1,1],",
				"[16,12,12,12,12,9,9,9,8,8,8,7,5,4,4,4,1,1,1,1,1],",
				"[21,16,16,16,16,13,12,12,12,12,12,11,9,8,8,8,5,5,5,5,4,1,1,1,1,1,1,1],",
				"[28,21,21,21,21,20,16,16,16,16,16,16,13,13,12,12,12,12,11,11,8,6,5,5,5,5,5,5,1,1,1,1,1,1,1,1,1],",
				"[37,28,28,28,28,28,22,21,21,21,21,21,20,20,20,20,18,16,16,16,14,13,12,12,12,12,12,11,6,6,6,6,6,5,5,5,5,1,1,1,1,1,1,1,1,1,1,1,1],",
				"[49,37,37,37,37,37,33,28,28,28,28,28,28,28,28,28,27,22,22,21,21,21,20,20,20,20,20,18,17,17,16,16,14,12,12,12,12,7,6,6,6,6,6,6,6,6,6,6,5,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],",
				"...",
				"ILLUSTRATION OF RECURRENCE.",
				"Start out with row 0 and row 1 consisting of a single '1'.",
				"To obtain any given row of this irregular triangle, first",
				"sum the prior two rows term-by-term; for rows 7 and 8 we get:",
				"[5,4,4,3,3,1,1] + [7,5,5,5,4,3,3,1,1] = [12,9,9,8,7,4,4,1,1].",
				"Place markers in an array so that the number of contiguous markers",
				"in each row correspond to the term-by-term sums like so:",
				"--------------------------",
				"12:o o o o o o o o o o o o",
				"9: o o o o o o o o o - - -",
				"9: o o o o o o o o o - - -",
				"8: o o o o o o o o - - - -",
				"7: o o o o o o o - - - - -",
				"4: o o o o - - - - - - - -",
				"4: o o o o - - - - - - - -",
				"1: o - - - - - - - - - - -",
				"1: o - - - - - - - - - - -",
				"--------------------------",
				"Then count the markers by columns to obtain the desired row;",
				"here, the number of markers in each column yields row 9:",
				"[9,7,7,7,5,5,5,4,3,1,1,1].",
				"Continuing in this way generates all the rows of this triangle."
			],
			"program": [
				"(PARI) {T(n,k)=local(G000931=(1-x^2)/(1-x^2-x^3+x*O(x^(n+6))));if(n\u003c0,0,if(n\u003c2\u0026k==0,1, polcoeff(sum(j=0,polcoeff(G000931,n+5)-1,(x^(T(n-1,j)+T(n-2,j)) - 1)/(x-1)),k) ))};",
				"/* To print, use Padovan g.f. to get the number of terms in row n: */",
				"for(n=0,10,for(k=0,polcoeff((1-x^2)/(1-x^2-x^3+x*O(x^(n+6))),n+6)-1,print1(T(n,k),\",\"));print(\"\"))"
			],
			"xref": [
				"Cf. A134816, A000045, A000931; A152546 (row squared sums)."
			],
			"keyword": "nonn,tabf",
			"offset": "0,5",
			"author": "_Paul D. Hanna_, Dec 13 2008",
			"references": 3,
			"revision": 5,
			"time": "2012-06-10T12:32:14-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}