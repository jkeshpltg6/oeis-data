{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A225468",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 225468,
			"data": "1,2,1,4,7,1,8,39,15,1,16,203,159,26,1,32,1031,1475,445,40,1,64,5187,12831,6370,1005,57,1,128,25999,107835,82901,20440,1974,77,1,256,130123,888679,1019746,369061,53998,3514,100,1",
			"name": "Triangle read by rows, S_3(n, k) where S_m(n, k) are the Stirling-Frobenius subset numbers of order m; n \u003e= 0, k \u003e= 0.",
			"comment": [
				"The definition of the Stirling-Frobenius subset numbers: S_m(n, k) = (sum_{j=0..n} binomial(j, n-k)*A_m(n, j)) / (m^k*k!) where A_m(n, j) are the generalized Eulerian numbers. For m = 1 this gives the classical Stirling set numbers A048993. (See the links for details.)",
				"From _Peter Bala_, Jan 27 2015: (Start)",
				"Exponential Riordan array [ exp(2*z), 1/3*(exp(3*z) - 1)].",
				"Triangle equals P * A111577 = P^(-1) * A075498, where P is Pascal's triangle A007318.",
				"Triangle of connection constants between the polynomial basis sequences {x^n}n\u003e=0 and { n!*3^n*binomial((x - 2)/3,n) }n\u003e=0. An example is given below.",
				"This triangle is the particular case a = 3, b = 0, c = 2 of the triangle of generalized Stirling numbers of the second kind S(a,b,c) defined in the Bala link. (End)"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A225468/b225468.txt\"\u003eRows n = 0..50, flattened\u003c/a\u003e",
				"P. Bala, \u003ca href=\"/A143395/a143395.pdf\"\u003eA 3 parameter family of generalized Stirling numbers\u003c/a\u003e",
				"Wolfdieter Lang, \u003ca href=\"https://arxiv.org/abs/1707.04451\"\u003eOn Sums of Powers of Arithmetic Progressions, and Generalized Stirling, Eulerian and Bernoulli numbers\u003c/a\u003e, arXiv:1707.04451 [math.NT], 2017.",
				"Peter Luschny, \u003ca href=\"http://www.luschny.de/math/euler/GeneralizedEulerianPolynomials.html\"\u003eGeneralized Eulerian polynomials.\u003c/a\u003e",
				"Peter Luschny, \u003ca href=\"http://www.luschny.de/math/euler/StirlingFrobeniusNumbers.html\"\u003eThe Stirling-Frobenius numbers.\u003c/a\u003e",
				"Shi-Mei Ma, Toufik Mansour, Matthias Schork, \u003ca href=\"http://arxiv.org/abs/1308.0169\"\u003eNormal ordering problem and the extensions of the Stirling grammar\u003c/a\u003e, Russian Journal of Mathematical Physics, 2014, 21(2), arXiv 1308.0169 p. 12."
			],
			"formula": [
				"T(n, k) = (sum_{j=0..n} binomial(j, n-k)*A_3(n, j)) / (3^k*k!) with A_3(n,j) = A225117.",
				"For a recurrence see the Maple program.",
				"T(n, 0) ~ A000079; T(n, 1) ~ A016127; T(n, 2) ~ A016297; T(n, 3) ~ A025999;",
				"T(n, n) ~ A000012; T(n, n-1) ~ A005449; T(n, n-2) ~ A024212.",
				"From _Peter Bala_, Jan 27 2015: (Start)",
				"T(n,k) = sum {i = 0..n} (-1)^(n+i)*3^(i-k)*binomial(n,i)*Stirling2(i+1,k+1).",
				"E.g.f.: exp(2*z)*exp(x/3*(exp(3*z) - 1)) = 1 + (2 + x)*z + (4 + 7*x + x^2)*z^2/2! + ....",
				"T(n,k) = 1/(3^k*k!)*sum {j = 0..k} (-1)^(k-j)*binomial(k,j)*(3*j + 2)^n.",
				"O.g.f. for n-th diagonal: exp(-2*x/3)*sum {k \u003e= 0} (3*k + 2)^(k + n - 1)*((x/3*exp(-x))^k)/k!.",
				"O.g.f. column k: 1/( (1 - 2*x)*(1 - 5*x)...(1 - (3*k + 2)*x ). (End)",
				"E.g.f. column k: exp(2*x)*(exp(3*x - 1)/3^k, k \u003e= 0. See the Bala link for the S(3,0,2) exponential Riordan aka Sheffer triangle. - _Wolfdieter Lang_, Apr 10 2017"
			],
			"example": [
				"[n\\k][ 0,    1,     2,    3,    4,  5,  6]",
				"[0]    1,",
				"[1]    2,    1,",
				"[2]    4,    7,     1,",
				"[3]    8,   39,    15,    1,",
				"[4]   16,  203,   159,   26,    1,",
				"[5]   32, 1031,  1475,  445,   40,  1,",
				"[6]   64, 5187, 12831, 6370, 1005, 57,  1.",
				"Connection constants: Row 3: [8, 39, 15, 1] so",
				"x^3 = 8 + 39*(x - 2) + 15*(x - 2)*(x - 5) + (x - 2)*(x - 5)*(x - 8). - _Peter Bala_, Jan 27 2015"
			],
			"maple": [
				"SF_S := proc(n, k, m) option remember;",
				"if n = 0 and k = 0 then return(1) fi;",
				"if k \u003e n or k \u003c 0 then return(0) fi;",
				"SF_S(n-1, k-1, m) + (m*(k+1)-1)*SF_S(n-1, k, m) end:",
				"seq(print(seq(SF_S(n, k, 3), k=0..n)), n = 0..5);"
			],
			"mathematica": [
				"EulerianNumber[n_, k_, m_] := EulerianNumber[n, k, m] = (If[ n == 0, Return[If[k == 0, 1, 0]]]; Return[(m*(n-k)+m-1)*EulerianNumber[n-1, k-1, m] + (m*k+1)*EulerianNumber[n-1, k, m]]); SFS[n_, k_, m_] := Sum[ EulerianNumber[n, j, m]*Binomial[j, n-k], {j, 0, n}]/(k!*m^k); Table[ SFS[n, k, 3], {n, 0, 8}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, May 29 2013, translated from Sage *)"
			],
			"program": [
				"(Sage)",
				"@CachedFunction",
				"def EulerianNumber(n, k, m) :",
				"    if n == 0: return 1 if k == 0 else 0",
				"    return (m*(n-k)+m-1)*EulerianNumber(n-1,k-1,m) + (m*k+1)*EulerianNumber(n-1,k,m)",
				"def SF_S(n, k, m):",
				"    return add(EulerianNumber(n, j, m)*binomial(j, n - k) for j in (0..n))/ (factorial(k)*m^k)",
				"for n in (0..6): [SF_S(n, k, 3) for k in (0..n)]"
			],
			"xref": [
				"Cf. A048993 (m=1), A039755 (m=2), A225469 (m=4).",
				"Cf. A075498, A111577. Columns: A000079, A016127, A016297, A025999. A225466, A225472."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,2",
			"author": "_Peter Luschny_, May 16 2013",
			"references": 11,
			"revision": 36,
			"time": "2017-12-07T22:16:59-05:00",
			"created": "2013-05-21T14:08:55-04:00"
		}
	]
}