{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A335487",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 335487,
			"data": "0,0,0,1,0,0,0,1,1,0,0,3,0,0,0,1,0,3,0,3,0,0,0,4,1,0,1,3,0,0,0,1,0,0,0,6,0,0,0,4,0,0,0,3,3,0,0,5,1,3,0,3,0,4,0,4,0,0,0,12,0,0,3,1,0,0,0,3,0,0,0,10,0,0,3,3,0,0,0,5,1,0,0,12,0,0",
			"name": "Number of (1,1)-matching permutations of the prime indices of n.",
			"comment": [
				"Depends only on sorted prime signature (A118914).",
				"A prime index of n is a number m such that prime(m) divides n. The multiset of prime indices of n is row n of A112798.",
				"We define a pattern to be a finite sequence covering an initial interval of positive integers. Patterns are counted by A000670. A sequence S is said to match a pattern P if there is a not necessarily contiguous subsequence of S whose parts have the same relative order as P. For example, (3,1,1,3) matches (1,1,2), (2,1,1), and (2,1,2), but avoids (1,2,1), (1,2,2), and (2,2,1)."
			],
			"link": [
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Permutation_pattern\"\u003ePermutation pattern\u003c/a\u003e",
				"Gus Wiseman, \u003ca href=\"/A102726/a102726.txt\"\u003eSequences counting and ranking compositions by the patterns they match or avoid.\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 0 if n is squarefree, otherwise a(n) = A008480(n).",
				"a(n) = A008480(n) - A281188(n) for n != 4."
			],
			"example": [
				"The a(n) permutations for n = 4, 12, 24, 48, 36, 72, 60:",
				"  (11)  (112)  (1112)  (11112)  (1122)  (11122)  (1123)",
				"        (121)  (1121)  (11121)  (1212)  (11212)  (1132)",
				"        (211)  (1211)  (11211)  (1221)  (11221)  (1213)",
				"               (2111)  (12111)  (2112)  (12112)  (1231)",
				"                       (21111)  (2121)  (12121)  (1312)",
				"                                (2211)  (12211)  (1321)",
				"                                        (21112)  (2113)",
				"                                        (21121)  (2131)",
				"                                        (21211)  (2311)",
				"                                        (22111)  (3112)",
				"                                                 (3121)",
				"                                                 (3211)"
			],
			"mathematica": [
				"primeMS[n_]:=If[n==1,{},Flatten[Cases[FactorInteger[n],{p_,k_}:\u003eTable[PrimePi[p],{k}]]]];",
				"Table[Length[Select[Permutations[primeMS[n]],!UnsameQ@@#\u0026]],{n,100}]"
			],
			"xref": [
				"Positions of zeros are A005117 (squarefree numbers).",
				"The case where the match must be contiguous is A333175.",
				"The avoiding version is A335489.",
				"The (1,1,1)-matching case is A335510.",
				"Patterns are counted by A000670.",
				"Permutations of prime indices are counted by A008480.",
				"(1,1)-matching patterns are counted by A019472.",
				"(1,1)-matching compositions are counted by A261982.",
				"STC-numbers of permutations of prime indices are A333221.",
				"Patterns matched by standard compositions are counted by A335454.",
				"Dimensions of downsets of standard compositions are A335465.",
				"(1,1)-matching compositions are ranked by A335488.",
				"Cf. A000961, A056239, A056986, A112798, A181796, A335451, A335452, A335460, A335462."
			],
			"keyword": "nonn",
			"offset": "1,12",
			"author": "_Gus Wiseman_, Jun 14 2020",
			"references": 2,
			"revision": 11,
			"time": "2020-06-30T09:55:35-04:00",
			"created": "2020-06-14T22:46:45-04:00"
		}
	]
}