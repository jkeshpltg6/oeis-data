{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A059750",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 59750,
			"data": "1,4,6,0,3,5,4,5,0,8,8,0,9,5,8,6,8,1,2,8,8,9,4,9,9,1,5,2,5,1,5,2,9,8,0,1,2,4,6,7,2,2,9,3,3,1,0,1,2,5,8,1,4,9,0,5,4,2,8,8,6,0,8,7,8,2,5,5,3,0,5,2,9,4,7,4,5,0,0,6,2,5,2,7,6,4,1,9,3,7,5,4,6,3,3,5,6,8,1",
			"name": "Decimal expansion of zeta(1/2) (negated).",
			"comment": [
				"Zeta(1/2) can be calculated as a limit similar to the limit for the Euler-Mascheroni constant or Euler gamma. - _Mats Granvik_ Nov 14 2012",
				"The WolframAlpha link gives 3 series and 3 integrals for zeta(1/2). - _Jonathan Sondow_, Jun 20 2013"
			],
			"link": [
				"Harry J. Smith, \u003ca href=\"/A059750/b059750.txt\"\u003eTable of n, a(n) for n = 1..5000\u003c/a\u003e",
				"B. K. Choudhury, \u003ca href=\"https://doi.org/10.1098/rspa.1995.0096\"\u003eThe Riemann zeta-function and its derivatives\u003c/a\u003e, Proc. R. Soc. Lond A 445 (1995) 477, Table 3.",
				"Steven R. Finch, \u003ca href=\"https://doi.org/10.1017/9781316997741\"\u003eMathematical Constants II\u003c/a\u003e, Encyclopedia of Mathematics and Its Applications, Cambridge University Press, Cambridge, 2018, p. 99.",
				"Fredrik Johansson, \u003ca href=\"https://fredrikj.net/math/zeta_1_2_1m.txt\"\u003ezeta(1/2) to 1 million digits\u003c/a\u003e.",
				"Fredrik Johansson, \u003ca href=\"https://arxiv.org/abs/2110.10583\"\u003eRapid computation of special values of Dirichlet L-functions\u003c/a\u003e, arxiv:2110.10583 [math.NA], 2021.",
				"Hisashi Kobayashi, \u003ca href=\"https://arxiv.org/abs/1603.02954\"\u003eSome results on the xi(s) and Xi(t) functions associated with Riemann's zeta(s) function\u003c/a\u003e, arXiv preprint arXiv:1603.02954 [math.NT], 2016.",
				"Lutz Mattner, Irina Shevtsova, \u003ca href=\"https://arxiv.org/abs/1710.08503\"\u003eAn optimal Berry-Esseen type theorem for integrals of smooth functions\u003c/a\u003e, arXiv:1710.08503 [math.PR], 2017.",
				"J. Sondow and E. W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/RiemannZetaFunction.html\"\u003eMathWorld: Riemann Zeta Function\u003c/a\u003e",
				"WolframAlpha, \u003ca href=\"http://www.wolframalpha.com/input/?i=zeta%281%2F2%29\"\u003ezeta(1/2)\u003c/a\u003e"
			],
			"formula": [
				"Zeta(1/2) = lim_{k-\u003einf} ( Sum_{n=1..k} 1/n^(1/2) - 2*k^(1/2) ) (according to Mathematica 8). - _Mats Granvik_ Nov 14 2012",
				"From _Magri Zino_, Jan 05 2014 - personal communication: (Start)",
				"The previous result is the case q=2 of the following generalization:",
				"Zeta(1/q) = lim_{k-\u003einf} (Sum_{n=1..k} 1/n^(1/q) - (q/(q-1))*k^((q-1)/q)), with q\u003e1. Example: for q=3/2, Zeta(2/3) = lim_{k-\u003einf} (Sum_{n=1..k} 1/n^(2/3) - 3*k^(1/3)) = -2.447580736233658231... (End)",
				"Equals -A014176*A113024. - _Peter Luschny_, Oct 25 2021"
			],
			"example": [
				"-1.4603545088095868128894991525152980124672293310125814905428860878..."
			],
			"maple": [
				"Digits := 120; evalf(Zeta(1/2));"
			],
			"mathematica": [
				"RealDigits[ Zeta[1/2], 10, 111][[1]] (* _Robert G. Wilson v_, Oct 11 2005 *)",
				"RealDigits[N[Limit[Sum[1/Sqrt[n], {n, 1, k}] - 2*Sqrt[k], k -\u003e Infinity], 90]][[1]] (* _Mats Granvik_ Nov 14 2012 *)"
			],
			"program": [
				"(PARI) default(realprecision, 5080); x=-zeta(1/2); for (n=1, 5000, d=floor(x); x=(x-d)*10; write(\"b059750.txt\", n, \" \", d)); \\\\ _Harry J. Smith_, Jun 29 2009"
			],
			"xref": [
				"Cf. A161688 (continued fraction), A078434, A014176, A113024."
			],
			"keyword": "nonn,cons",
			"offset": "1,2",
			"author": "Peter Walker (peterw(AT)aus.ac.ae), Feb 11 2001",
			"ext": [
				"Sign of the constant reversed by _R. J. Mathar_, Feb 05 2009"
			],
			"references": 23,
			"revision": 68,
			"time": "2021-10-25T16:10:29-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}