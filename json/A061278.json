{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A061278",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 61278,
			"data": "0,1,5,20,76,285,1065,3976,14840,55385,206701,771420,2878980,10744501,40099025,149651600,558507376,2084377905,7779004245,29031639076,108347552060,404358569165,1509086724601,5631988329240,21018866592360,78443478040201,292755045568445",
			"name": "a(n) = 5*a(n-1) - 5*a(n-2) + a(n-3) with a(1) = 1 and a(k) = 0 if k \u003c= 0.",
			"comment": [
				"Indices m of triangular numbers T(m) which are one-third of another triangular number: 3*T(m) = T(k); the k's are given by A001571. - Bruce Corrigan (scentman(AT)myfamily.com), Oct 31 2002",
				"On the previous comment: for m=0 this is actually one third of the same triangular number. - _Zak Seidov_, Apr 07 2011",
				"Also numbers n such that the n-th centered 24-gonal number 12*n*(n+1)+1 is a perfect square A001834(n)^2, where A001834(n) is defined by the recursion: a(0) = 1, a(1) = 5, a(n) = 4*a(n-1) - a(n-2) + 1. - _Alexander Adamchuk_, Apr 21 2007",
				"Also numbers n such that RootMeanSquare(5,...,6*n-1) is an integer. - _Ctibor O. Zizka_, Dec 17 2008 (Corrected by _Robert K. Moniot_, Jul 22 2020)",
				"Also numbers n such that n*(n+1) = Sum_{i=1..x} n+i for some x. (This does not apply to the first term.). - _Gil Broussard_, Dec 23 2008",
				"From _John P. McSorley_, May 26 2020: (Start)",
				"Consecutive terms (a(n-1), a(n)) = (u,v) give all points on the hyperbola u^2 - u + v^2 - v - 4*u*v = 0 in quadrant I with both coordinates an integer.",
				"Also related to the block sizes of small multi-set designs. (End)"
			],
			"reference": [
				"R. C. Alperin, A nonlinear recurrence and its relations to Chebyshev polynomials, Fib. Q., 58:2 (2020), 140-142."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A061278/b061278.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Niccolò Castronuovo, \u003ca href=\"https://arxiv.org/abs/2102.02739\"\u003eOn the number of fixed points of the map gamma\u003c/a\u003e, arXiv:2102.02739 [math.NT], 2021. Mentions this sequence.",
				"Brian Lawrence and Will Sawin, \u003ca href=\"https://arxiv.org/abs/2004.09046\"\u003eThe Shafarevich conjecture for hypersurfaces in abelian varieties\u003c/a\u003e, arXiv:2004.09046 [math.NT], 2020.",
				"Ioana-Claudia Lazăr, \u003ca href=\"https://arxiv.org/abs/1904.06555\"\u003eLucas sequences in t-uniform simplicial complexes\u003c/a\u003e, arXiv:1904.06555 [math.GR], 2019.",
				"L. A. Medina and A. Straub, \u003ca href=\"http://emmy.uprrp.edu/lmedina/papers/logconcave/logconcavity.pdf\"\u003eOn multiple and infinite log-concavity\u003c/a\u003e, 2013.",
				"S. Morier-Genoud, V. Ovsienko and S. Tabachnikov, \u003ca href=\"http://aif.cedram.org/item?id=AIF_2012__62_3_937_0\"\u003e2-frieze patterns and the cluster structure of the space of polygons\u003c/a\u003e, Annales de l'institut Fourier, 62 no. 3 (2012), 937-987. - From _N. J. A. Sloane_, Dec 26 2012",
				"Robert Phillips, \u003ca href=\"https://web.archive.org/web/20100713033404/http://www.usca.edu/math/~mathdept/bobp/pdf/result.pdf\"\u003eA triangular number result\u003c/a\u003e, 2009.",
				"Vladimir Pletser, \u003ca href=\"https://arxiv.org/abs/2101.00998\"\u003eRecurrent Relations for Multiple of Triangular Numbers being Triangular Numbers\u003c/a\u003e, arXiv:2101.00998 [math.NT], 2021.",
				"Vladimir Pletser, \u003ca href=\"https://arxiv.org/abs/2102.12392\"\u003eClosed Form Equations for Triangular Numbers Multiple of Other Triangular Numbers\u003c/a\u003e, arXiv:2102.12392 [math.GM], 2021.",
				"Vladimir Pletser, \u003ca href=\"https://arxiv.org/abs/2102.13494\"\u003eTriangular Numbers Multiple of Triangular Numbers and Solutions of Pell Equations\u003c/a\u003e, arXiv:2102.13494 [math.NT], 2021.",
				"Vladimir Pletser, \u003ca href=\"https://arxiv.org/abs/2103.03019\"\u003eCongruence Properties of Indices of Triangular Numbers Multiple of Other Triangular Numbers\u003c/a\u003e, arXiv:2103.03019 [math.GM], 2021.",
				"Vladimir Pletser, \u003ca href=\"https://doi.org/10.13140/RG.2.2.35428.91527\"\u003eSearching for multiple of triangular numbers being triangular numbers\u003c/a\u003e, 2021.",
				"Eric Weisstein, \u003ca href=\"http://mathworld.wolfram.com/CenteredPolygonalNumber.html\"\u003eCentered Polygonal Number\u003c/a\u003e.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (5,-5,1)."
			],
			"formula": [
				"a(n) = 4*a(n-1) - a(n-2) + 1.",
				"a(n) = A001075(n) - a(n-1) - 1.",
				"a(n) = (A001835(n+1) - 1)/2 = (A001353(n+1) - A001353(n) - 1)/2.",
				"a(n) = a(n-1) + A001353(n), i.e., partial sum of A001353.",
				"From Bruce Corrigan (scentman(AT)myfamily.com), Oct 31 2002: (Start)",
				"a(n+2) = 4*a(n+1) - a(n) + 1 for a(0)=0, a(1)=1.",
				"G.f.: x/((1 - x)*(1 - 4*x + x^2)).",
				"a(n) = (1/12)*((3 - sqrt(3))*(2 - sqrt(3))^n + (3 + sqrt(3))*(2 + sqrt(3))^n-6). (End)",
				"a(n) = (1/12)*(A003500(n) + A003500(n+1)-6). - Mario Catalani (mario.catalani(AT)unito.it), Apr 11 2003",
				"a(n+1) = Sum_{k=0..n} U(k, 2)} = Sum_{k=0..n} S(k, 4), where U(n,x) and S(n,x) are Chebyshev polynomials. - _Paul Barry_, Nov 14 2003",
				"G.f.: x/(1 - 5*x + 5*x^2 - x^3).",
				"a(n) = A079935(n+1) + A001571(n) for n\u003e0, a(0)=0. - _Gerry Martens_, Jun 05 2015",
				"a(n)*a(n-2) = a(n-1)*(a(n-1) - 1) for n\u003e1. - _Bruno Berselli_, Nov 29 2016",
				"From _John P. McSorley_, May 25 2020: (Start)",
				"a(n)^2 - a(n) + a(n-1)^2 - a(n-1) - 4*a(n)*a(n-1) = 0.",
				"a(n) = A001834(n-1) + a(n-2). (End)",
				"(T(a(n)-1) + T(a(n+1)-1))/T(a(n) + a(n+1) - 1) = 2/3 where T(i) is the i-th triangular number. - _Robert K. Moniot_, Oct 11 2020",
				"E.g.f.: exp(x)*(exp(x)*(3*cosh(sqrt(3)*x) + sqrt(3)*sinh(sqrt(3)*x)) - 3)/6. - _Stefano Spezia_, Feb 05 2021",
				"a(n) = A101265(n) - 1. - _Jon E. Schoenfield_, Jan 01 2022"
			],
			"example": [
				"a(2)=5 and T(5)=15 which is 1/3 of 45=T(9)."
			],
			"maple": [
				"f:= gfun:-rectoproc({a(n) = 5*a(n-1) - 5*a(n-2) + a(n-3),a(1)=1,a(0)=0,a(-1)=0},a(n),remember):",
				"map(f, [$0..50]); # _Robert Israel_, Jun 05 2015"
			],
			"mathematica": [
				"CoefficientList[Series[x/(1 - 5*x + 5*x^2 - x^3), {x, 0, nn}], x] (* _T. D. Noe_, Jun 04 2012 *)",
				"LinearRecurrence[{5,-5,1},{0,1,5},30] (* _Harvey P. Dale_, Dec 23 2012 *)"
			],
			"program": [
				"(PARI) M = [1, 1, 0; 1, 3, 1; 0, 1, 1]; for(i=1, 30, print1(([1, 0, 0]*M^i)[3], \",\")) \\\\ Lambert Klasen (Lambert.Klasen(AT)gmx.net), Jan 25 2005",
				"(MAGMA) I:=[0, 1]; [n le 2 select I[n] else 4*Self(n-1) - Self(n-2) + 1: n in [1..30]]; // _Vincenzo Librandi_, Dec 23 2012"
			],
			"xref": [
				"Cf. A001075, A001353, A001571, A001834, A001835, A079935, A101265. Also cf. A212336 for more sequences with g.f. of the type 1/(1-k*x+k*x^2-x^3)."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Henry Bottomley_, Jun 04 2001",
			"ext": [
				"More terms from Lambert Klasen (Lambert.Klasen(AT)gmx.net), Jan 25 2005"
			],
			"references": 35,
			"revision": 137,
			"time": "2022-01-02T00:10:42-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}