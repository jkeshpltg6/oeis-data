{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A176203",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 176203,
			"data": "1,1,1,1,17,1,1,33,33,1,1,49,81,49,1,1,65,145,145,65,1,1,81,225,305,225,81,1,1,97,321,545,545,321,97,1,1,113,433,881,1105,881,433,113,1,1,129,561,1329,2001,2001,1329,561,129,1,1,145,705,1905,3345,4017,3345,1905,705,145,1",
			"name": "Triangle read by rows: T(n, k) = 16*binomial(n, k) - 15.",
			"comment": [
				"This sequence belongs to the class defined by T(n, m, q) = 2*T(n, m, q-1) - 1. The first few q values gives the sequences: A007318 (q=0), A109128 (q=1), A131061 (q=2), A168625 (q=3), this sequence (q=4).",
				"Row sums are: {1, 2, 19, 68, 181, 422, 919, 1928, 3961, 8042, 16219, ...}.",
				"Former title: A recursive symmetrical triangular sequence:q=4: t(n, m, q) = 2*t(n, m, q-1) - 1. - _G. C. Greubel_, Mar 12 2020"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A176203/b176203.txt\"\u003eRows n = 0..100 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, m, q) = 2*T(n, m, q-1) - 1, with T(n, m, 0) = binomial(n, m) and q = 4.",
				"From _G. C. Greubel_, Mar 12 2020: (Start)",
				"T(n, k, q) = 2^q * binomial(n, k) - (2^q - 1), with q = 4.",
				"Sum_{k=0..n} T(n, k, q) = 2^(n + q) - (n + 1)*(2^q - 1) (row sums). (End)"
			],
			"example": [
				"Triangle begins as:",
				"  1;",
				"  1,   1;",
				"  1,  17,   1;",
				"  1,  33,  33,    1;",
				"  1,  49,  81,   49,    1;",
				"  1,  65, 145,  145,   65,    1;",
				"  1,  81, 225,  305,  225,   81,    1;",
				"  1,  97, 321,  545,  545,  321,   97,    1;",
				"  1, 113, 433,  881, 1105,  881,  433,  113,   1;",
				"  1, 129, 561, 1329, 2001, 2001, 1329,  561, 129,   1;",
				"  1, 145, 705, 1905, 3345, 4017, 3345, 1905, 705, 145, 1;"
			],
			"maple": [
				"A176203:= (n,k) -\u003e 16*binomial(n, k) -15; seq(seq(A176203(n, k), k = 0..n), n = 0.. 12); # _G. C. Greubel_, Mar 12 2020"
			],
			"mathematica": [
				"T[n_, m_, q]:= 2^q*(Binomial[n, m] -1) + 1; Table[T[n,m,4], {n,0,12}, {m,0,n} ]//Flatten (* modified by _G. C. Greubel_, Mar 12 2020 *)",
				"Table[16*Binomial[n, k] -15, {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, Mar 12 2020 *)"
			],
			"program": [
				"(MAGMA) [16*Binomial(n, k) -15: k in [0..n], n in [0..12]]; // _G. C. Greubel_, Mar 12 2020",
				"(Sage) [[16*binomial(n, k) -15 for k in (0..n)] for n in (0..12)] # _G. C. Greubel_, Mar 12 2020"
			],
			"xref": [
				"Sequence m*binomial(n,k) - (m-1): A007318 (m=1), this sequence (m=2), A131060 (m=3), A131061 (m=4), A131063 (m=5), A131065 (m=6), A131067 (m=7), A168625 (m=8), this sequence (m=16)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Roger L. Bagula_, Apr 11 2010",
			"ext": [
				"Edited by _G. C. Greubel_, Mar 12 2020"
			],
			"references": 1,
			"revision": 7,
			"time": "2020-03-13T08:38:41-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}