{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A129065",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 129065,
			"data": "1,0,1,0,2,1,0,12,10,1,0,144,156,28,1,0,2880,3696,908,60,1,0,86400,125280,37896,3508,110,1,0,3628800,5780160,2036592,236472,10528,182,1,0,203212800,349090560,138517632,19022736,1074176,26600,280,1,0",
			"name": "Coefficients of the v=1 member of a family of certain orthogonal polynomials.",
			"comment": [
				"For v\u003e=1 the orthogonal polynomials p(n,v,x) have v integer zeros k*(k-1), k=1..v, for every n\u003e=v.",
				"Coefficients of p(n,v=1,x) (in the quoted Bruschi et al. paper p^{(\\nu)}_n(x) of eqs. (4) and (8a),(8b)) in increasing powers of x.",
				"The v-family p(n,v,x) consists of characteristic polynomials of the tridiagonal M x M matrix V=V(M,v) with entries V_{m,n} given by v*(v-1)-(m-1)^2 - (v-m)^2 if n=m, m=1,...,M;(m-1)^2 if n=m-1, m=2,...,M; (v-m)^2 if n=m+1, m=1..M-1 and 0 else. p(n,v,x):=det(x*I_n - V(n,v) with the n dimensional unit matrix I_n.",
				"p(n,v=1,x) has, for every n\u003e=1 a zero for x=0, i.e. det(V(n,1))=0 for every n\u003e=1. This is obvious.",
				"The column sequences give A000007, A010790, A129460, A129461 for m=0,1,2,3."
			],
			"link": [
				"M. Bruschi, F. Calogero and R. Droghei, \u003ca href=\"http://dx.doi.org/10.1088/1751-8113/40/14/005\"\u003eProof of certain Diophantine conjectures and identification of remarkable classes of orthogonal polynomials\u003c/a\u003e, J. Physics A, 40(2007), pp. 3815-3829.",
				"W. Lang, \u003ca href=\"/A129065/a129065.txt\"\u003eFirst ten rows.\u003c/a\u003e"
			],
			"formula": [
				"a(n,m)=[x^m]p(n,1,x), n\u003e=0, with the three term recurrence for orthogonal polynomial systems of the form p(n,v,x) = (x+2*(n-1)^2-2*(v-1)*(n-1)-v+1)*p(n-1,v,x) -(n-1)^2*(n-1-v)^2*p(n-2,v,x), n\u003e=1; p(-1,v,x)=0 and p(0,v,x)=1. Put v=1 here.",
				"Recurrence: a(n,m) = a(n-1,m-1)+(2*(n-1)^2-2*(v-1)*(n-1)-v+1)*a(n-1,m) -((n-1)^2*(n-1-v)^2)*a(n-2, m); a(n,m)=0 if n\u003cm, a(-1,m):=0, a(0,0)=1, a(n,-1)=0. Put v=1 here."
			],
			"example": [
				"Triangle begins:",
				"[1];",
				"[0,1];",
				"[0,2,1];",
				"[0,12,10,1];",
				"[0,144,156,28,1];",
				"[0,2880,3696,908,60,1];",
				"...",
				"n=5,[0,2880,3696,908,60,1] stands for the polynomial x*(2880+3696*x+908*x^2+60*x^3+1*x^4) with one zero 0 and some other four zeros.",
				"Tridiagonal matrix V(5,1)=[[0,0,0,0,0],[1,-2,1,0,0],[0,4,-8,4,0],[0,0,9,-18,9],[0,0,0,16,-32]]."
			],
			"mathematica": [
				"nmax = 9; a[n_, m_] := a[n, m] = (-(n-2)^2)*(n-1)^2*a[n-2, m] + a[n-1, m-1] + 2*(n-1)^2*a[n-1, m]; a[n_, m_] /; n \u003c m = 0; a[-1, _] = 0; a[0, 0] = 1; a[_, -1] = 0; Flatten[ Table[ a[n, m], {n, 0, nmax}, {m, 0, n}]] (* _Jean-François Alcover_, Sep 26 2011, after recurrence *)"
			],
			"xref": [
				"Row sums give A129458. Cf. A129462 (v=2 triangle)."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,5",
			"author": "_Wolfdieter Lang_, May 04 2007",
			"references": 10,
			"revision": 20,
			"time": "2019-08-28T16:47:55-04:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}