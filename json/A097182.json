{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097182",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97182,
			"data": "1,7,21,21,-63,-231,-15,1521,3073,-4319,-29631,-29631,143361,489345,-255,-3342591,-6684671,9454081,64553985,64553985,-311689215,-1064175615,-4095,7266627585,14533263361,-20553129983,-140345589759,-140345589759,677648531457,2313636773889",
			"name": "G.f. A(x) has the property that the first (n+1) terms of A(x)^(n+1) form the n-th row polynomial R_n(y) of triangle A097181 and satisfy R_n(1/2) = 8^n for all n\u003e=0.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A097182/b097182.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e"
			],
			"formula": [
				"G.f.: A(x) = 16*x/(1-(1-2*x)^8)."
			],
			"example": [
				"A(x) = 1 + 7*x + 21*x^2 + 21*x^3 - 63*x^4 - 231*x^5 - 15*x^6 +-...",
				"For n\u003e=0, the first (n+1) coefficients of A(x)^(n+1) forms the",
				"n-th row polynomial R_n(y) of triangle A097181:",
				"A^1 = {1, _7,  21,    21,    -63,    -231,      -15,     1521, ...}",
				"A^2 = {1, 14, _91,   336,    609,    -462,    -5469,    -9516, ...}",
				"A^3 = {1, 21, 210, _1288,   5103,   11655,     2160,   -85590, ...}",
				"A^4 = {1, 28, 378,  3220, _18907,   77280,   199860,   153000, ...}",
				"A^5 = {1, 35, 595,  6475,  49910, _283192,  1175190,  3282870, ...}",
				"A^6 = {1, 42, 861, 11396, 108402,  778596, _4296034, 17959968, ...}",
				"These row polynomials satisfy: R_n(1/2) = 8^n:",
				"8^1 = 1 + 14/2;",
				"8^2 = 1 + 21/2 + 210/2^2;",
				"8^3 = 1 + 28/2 + 378/2^2 + 3220/2^3;",
				"8^4 = 1 + 35/2 + 595/2^2 + 6475/2^3 + 49910/2^4."
			],
			"maple": [
				"seq(coeff(series(16*x/(1-(1-2*x)^8), x, n+2), x, n), n = 0..30); # _G. C. Greubel_, Sep 17 2019"
			],
			"mathematica": [
				"CoefficientList[Series[16*x/(1-(1-2*x)^8), {x,0,30}], x] (* _G. C. Greubel_, Sep 17 2019 *)"
			],
			"program": [
				"(PARI) a(n)=polcoeff(16*x/(1-(1-2*x)^8)+x*O(x^n),n,x)",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Rationals(), 30); Coefficients(R!( 16*x/(1-(1-2*x)^8) )); // _G. C. Greubel_, Sep 17 2019",
				"(Sage)",
				"def A097194_list(prec):",
				"    P.\u003cx\u003e = PowerSeriesRing(QQ, prec)",
				"    return P(16*x/(1-(1-2*x)^8)).list()",
				"A097194_list(30) # _G. C. Greubel_, Sep 17 2019"
			],
			"xref": [
				"Cf. A097181, A097183, A097184, A097185."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Paul D. Hanna_, Aug 03 2004",
			"references": 5,
			"revision": 12,
			"time": "2020-04-28T15:15:08-04:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}