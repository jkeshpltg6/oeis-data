{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A324155",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 324155,
			"data": "4,498,1139556,33182655688",
			"name": "Greatest number N such that the number of primes (\u003c= N) \u003c= the number of base-n-zerofree numbers (\u003c= N).",
			"comment": [
				"Further terms:",
				"14670238462896430         \u003c a(6)  \u003c 14670469667698570;",
				"1.88655928870547380*10^22 \u003c a(7)  \u003c 1.8865698003644555*10^22;",
				"1.5845871199286*10^29     \u003c a(8)  \u003c 1.5845909238805*10^29;",
				"3.7023896360635*10^36     \u003c a(9)  \u003c 3.7023941021398*10^36;",
				"1.0075615552422*10^45     \u003c a(10) \u003c 1.0075622026833*10^45;",
				"1.3480809599483*10^53     \u003c a(11) \u003c 1.3480814844466*10^53;",
				"3.9618565460983*10^62     \u003c a(12) \u003c 3.9618574860993*10^62;",
				"7.8648507615953*10^71     \u003c a(13) \u003c 7.864851991241*10^71;",
				"4.7945106758325*10^81     \u003c a(14) \u003c 4.7945111864185*10^81;",
				"1.0953005932169*10^92     \u003c a(15) \u003c 1.0953006746693*10^92;",
				"8.3149001821943*10^148    \u003c a(20) \u003c 8.3149003278317*10^148.",
				"All terms are even, since a(n) + 1 is always an odd prime number.",
				"The numbers a(n) + 1 and a(n) + 2 are zero containing numbers (in base n).",
				"The numbers between a(n) and p := min( k \u003e a(n) + 1 | k is prime) are zero containing numbers, i.e., a(n) + j is a zero containing number for 0 \u003c j \u003c p - a(n).",
				"For numbers m \u003e a(10) = 1.00756...*10^45, we have pi(m) \u003e A324161(m) [= number of zerofree numbers \u003c= m]; in general, the ratio A324161(m) to pi(m) is O(log(n)*n^d), where d := 1 - 1/(1 - log_10(9)) = -0.0457..., and thus tends to 0 for m --\u003e infinity. Consequently, the share of primes \u003c= m which have no '0'-digit become significantly smaller as m rises beyond that bound a(10). For m = 10^100, the share is not greater than 0.000688, for m = 10^1000, the share cannot exceed 4.52757*10^(-43). Conversely, the share of primes which contain a '0'-digit tends to 1 as m grows to infinity (cf. A011540).",
				"Conjecture: a(n) can be represented in the form a(n) = k*n^m + j, where j \u003c (n^(m+1)-1)/(n-1) - n^m, and m \u003e 1, 0 \u003c k \u003c n."
			],
			"formula": [
				"a(n) = max(k | pi(k) \u003c= numOfZerofreeNum_n(k)), where numOfZerofreeNum_n(k) is the number of base-n zerofree numbers \u003c= k ((see A324161 for general formulas regarding numOfZerofreeNum _n(k))).",
				"a(n) \u003e= A324154(n) + 1.",
				"Estimate of the n-th term (n \u003e 2):",
				"a(n) \u003c e*(p*log(p*log((e/(e-1))*p*log(p))))^(1/(1-d)),",
				"a(n) \u003e e^1.1*(q*log(q*log(q*log(q))))^(1/(1-d)),",
				"where p := (n-1)/((n-2)*(1-d(n)))*e^(-(1-d)), q := (n-1)^d/((n-2)*(1-d(n)))*e^(-1.1*(1-d)), d := d(n) := log(n-1)/log(n).",
				"Also, but more imprecise:",
				"a(n) \u003c e*((e/(e-1))*p*log(p))^(1/(1-d)),",
				"a(n) \u003c e*((e/(e-1))*p*log(p))^((n-1/2)*log(n)),",
				"a(n) \u003c n*((e/(e-1))*n*log(n)*log(n*log(n)))^((n-1/2)*log(n)), n \u003e 3.",
				"Asymptotic behavior:",
				"a(n) = O(n*((e/(e-1))*n*log(n)*log(n*log(n)))^(n*log(n))).",
				"a(n) = O(n*((e+1)/(e-1)*n*log(n)^2)^(n*log(n)))."
			],
			"example": [
				"a(2) = 4, since pi(1) = 0 \u003c= 1 = numOfZerofreeNum_2(1), pi(2) = 1 \u003c= 1 = numOfZerofreeNum_2(2), pi(3) = 2 \u003c= 2 = numOfZerofreeNum_2(3), pi(4) = 2 \u003c= 2 = numOfZerofreeNum_2(3), and pi(m) \u003e numOfZerofreeNum_2(m) for m \u003e 4, where numOfZerofreeNum_2(m) is the number of base-2-zerofree numbers \u003c= m and pi(m) = number of primes \u003c= m. The first base-2-zerofree numbers are 1 = 1_2, 3 = 11_2, 7 = 111_2, ..."
			],
			"xref": [
				"Cf. A011540, A052382, A324154, A324160, A324161, A325164, A325165."
			],
			"keyword": "nonn,base,more,hard",
			"offset": "2,1",
			"author": "_Hieronymus Fischer_, Feb 22 2019",
			"references": 9,
			"revision": 32,
			"time": "2019-08-25T13:31:45-04:00",
			"created": "2019-03-28T12:01:39-04:00"
		}
	]
}