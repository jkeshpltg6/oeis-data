{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A345754",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 345754,
			"data": "1,16,45,192,225,720,637,2048,1701,3600,2541,8640,4225,10192,10125,20480,9537,27216,13357,43200,28665,40656,23805,92160,40625,67600,59049,122304,47937,162000,58621,196608,114345,152592,143325,326592,99937,213712,190125",
			"name": "Number of 2 X 2 matrices over Z_n such that their permanent equals their determinant.",
			"formula": [
				"a(n) =  A344372(n) * n^2 (conjectured).",
				"From _Sebastian Karlsson_, Aug 31 2021: (Start)",
				"The formula is correct. Proof:",
				"Let A = ([a, b], [c, d]) be an arbitrary 2 X 2 matrix over Z_n. So det(A) = a*d - b*c and perm(A) = a*d + b*c. Then, det(A) = perm(A) iff -b*c = b*c, i.e., 2*b*c = 0.",
				"Suppose first that n is odd. Then 2*b*c = 0 iff b*c = 0. The number of solutions to this equation over Z_n is A018804(n). Furthermore, the value of a and b in A can be anything, so there are n possible choices for a and n possible choices for b. Thus, there are n*n*A018804(n) = n^2 * A344372(n) matrices A over Z_n such that det(A) = perm(A).",
				"Suppose now that n is even. Then 2*b*c = 0 in Z_n iff b'*c' = 0 in Z_{n/2}, where b' and c' are b and c reduced modulo n/2. The latter equation has A018804(n/2) distinct solutions in Z_{n/2}. As the preimage of both b' and c' contains precisely 2 elements each, the number of solutions to 2*b*c = 0 in Z_n is 2*2*A018804(n/2). Hence, a(n) = n*n*4*A018804(n/2) = n^2 * A344372(n). Q.E.D.",
				"The formula implies that the sequence is multiplicative with a(2^e) = (e+1)*8^e, a(p^e) = p^(3*e-1)*((p-1)*e+p) for odd primes p. (End)"
			],
			"mathematica": [
				"a[n_] := a[n] =  Sum[If[Mod[Permanent[{{a, b}, {c, d}}] - Det[{{a, b}, {c, d}}],n] == 0, 1, 0], {a, 0, n - 1}, {b, 0, n - 1}, {c, 0, n - 1}, {d,0, n - 1}] ; Array[a,22]"
			],
			"xref": [
				"Cf. A020478, A344372.",
				"Cf. A018804."
			],
			"keyword": "nonn,mult",
			"offset": "1,2",
			"author": "_José María Grau Ribas_, Jul 19 2021",
			"references": 0,
			"revision": 64,
			"time": "2021-09-03T05:55:08-04:00",
			"created": "2021-08-23T00:52:19-04:00"
		}
	]
}