{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A055268",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 55268,
			"data": "1,15,65,185,420,826,1470,2430,3795,5665,8151,11375,15470,20580,26860,34476,43605,54435,67165,82005,99176,118910,141450,167050,195975,228501,264915,305515,350610,400520,455576,516120,582505,655095,734265",
			"name": "a(n) = (11*n + 4)*C(n+3, 3)/4.",
			"comment": [
				"a(n) is the number of compositions of n when there are 9 types of each natural number. - _Milan Janjic_, Aug 13 2010",
				"Convolution of A000027 with A051865 (excluding 0). - _Bruno Berselli_, Dec 07 2012"
			],
			"reference": [
				"A. H. Beiler, Recreations in the Theory of Numbers, Dover, N.Y., 1964, pp. 122-125, 194-196."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A055268/b055268.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"I. Adler, \u003ca href=\"http://www.fq.math.ca/Scanned/7-2/adler.pdf\"\u003eThree Diophantine equations - Part II\u003c/a\u003e, Fib. Quart., 7 (1969), pp. 181-193.",
				"E. I. Emerson, \u003ca href=\"http://www.fq.math.ca/Scanned/7-3/emerson.pdf\"\u003eRecurrent Sequences in the Equation DQ^2=R^2+N\u003c/a\u003e, Fib. Quart., 7 (1969), pp. 231-242.",
				"\u003ca href=\"/index/Ps#pyramidal_numbers\"\u003eIndex to sequences related to pyramidal numbers\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (5,-10,10,-5,1)."
			],
			"formula": [
				"G.f.: (1 + 10*x)/(1-x)^5. - _R. J. Mathar_, Oct 26 2011",
				"From _G. C. Greubel_, Jan 17 2020:(Start)",
				"a(n) = 11*binomial(n+4,4) - 10*binomial(n+3,3).",
				"E.g.f.: (24 + 336*x + 432*x^2 + 136*x^3 + 11*x^4)*exp(x)/24. (End)"
			],
			"maple": [
				"seq( (11*n+4)*binomial(n+3,3)/4, n=0..30); # _G. C. Greubel_, Jan 17 2020"
			],
			"mathematica": [
				"Table[11*Binomial[n+4,4] -10*Binomial[n+3,3], {n,0,30}] (* _G. C. Greubel_, Jan 17 2020 *)"
			],
			"program": [
				"(MAGMA) /* A000027 convolved with A051865 (excluding 0): */ A051865:=func\u003cn | n*(11*n-9)/2\u003e; [\u0026+[(n-i+1)*A051865(i): i in [1..n]]: n in [1..35]]; // _Bruno Berselli_, Dec 07 2012",
				"(Python)",
				"A055268_list, m = [], [11, 1, 1, 1, 1]",
				"for _ in range(10**2):",
				"    A055268_list.append(m[-1])",
				"    for i in range(4):",
				"        m[i+1] += m[i] # _Chai Wah Wu_, Jan 24 2016",
				"(PARI) a(n) = (11*n+4)*binomial(n+3, 3)/4; \\\\ _Michel Marcus_, Sep 07 2017",
				"(Sage) [(11*n+4)*binomial(n+3,3)/4 for n in (0..30)] # _G. C. Greubel_, Jan 17 2020",
				"(GAP) List([0..30], n-\u003e (11*n+4)*Binomial(n+3,3)/4 ); # _G. C. Greubel_, Jan 17 2020"
			],
			"xref": [
				"Partial sums of A050441.",
				"Cf. A000292, A051865.",
				"Cf. A220212 for a list of sequences produced by the convolution of the natural numbers with the k-gonal numbers."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Barry E. Williams_, May 10 2000",
			"references": 4,
			"revision": 31,
			"time": "2020-01-17T17:42:55-05:00",
			"created": "2000-06-15T03:00:00-04:00"
		}
	]
}