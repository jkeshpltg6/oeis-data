{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000139",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 139,
			"id": "M1660 N0651",
			"data": "2,1,2,6,22,91,408,1938,9614,49335,260130,1402440,7702632,42975796,243035536,1390594458,8038677054,46892282815,275750636070,1633292229030,9737153323590,58392041019795,352044769046880,2132866978427640",
			"name": "a(n) = 2*(3*n)!/((2*n+1)!*((n+1)!)).",
			"comment": [
				"This sequence arises in many different contexts, and over the years it has had several different definitions. I have now changed the definition back to one of the earlier ones, a self-contained formula. - _N. J. A. Sloane_, Apr 24 2012",
				"The number of 2-stack sortable permutations on n letters (n \u003e= 1).",
				"The number of rooted non-separable planar maps with n edges. - _Valery A. Liskovets_, Mar 17 2005",
				"The shifted sequence starting with a(1): Number of quadrangular dissections of a square, counted by the number of vertices. Rooted, non-separable planar maps with no multiple edges, in which each non-root face has degree 4.",
				"Number of left ternary trees having n nodes (n\u003e=1). - _Emeric Deutsch_, Jul 23 2006",
				"A combinatorial interpretation for this sequence in terms of a family of plane trees is given in [Schaeffer, Corollary 2 with k = 3]. - _Peter Bala_, Oct 12 2011",
				"Number of canopy intervals in the Tamari lattices, see [Préville-Ratelle and Viennot, section 6] - _F. Chapoton_, Apr 19 2015",
				"The number of fighting fish (branching polyominoes). - _David Bevan_, Jan 10 2018",
				"The number of 1324-avoiding dominoes (gridded permutations). - _David Bevan_, Jan 10 2018",
				"For n \u003e 0, a(n) is the number of simple strong triangulations of a fixed quadrilateral with n interior nodes. See A210664. - _Andrew Howroyd_, Feb 24 2021"
			],
			"reference": [
				"Miklos Bona, editor, Handbook of Enumerative Combinatorics, CRC Press, 2015, page 365.",
				"Eric S. Egge, Defying God: The Stanley-Wilf Conjecture, Stanley-Wilf Limits, and a Two-Generation Explosion of Combinatorics, pp. 65-82 of \"A Century of Advancing Mathematics\", ed. S. F. Kennedy et al., MAA Press 2015.",
				"J. L. Gross and J. Yellen, eds., Handbook of Graph Theory, CRC Press, 2004; p. 714.",
				"S. Kitaev, Patterns in Permutations and Words, Springer-Verlag, 2011. See p. 399 Table A.7",
				"W. F. Lunnon, Counting polyominoes, pp. 347-372 of A. O. L. Atkin and B. J. Birch, editors, Computers in Number Theory. Academic Press, NY, 1971.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"R. P. Stanley, Enumerative Combinatorics, Cambridge, Vol. 2, 1999; see Problem 6.41."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A000139/b000139.txt\"\u003eTable of n, a(n) for n=0..200\u003c/a\u003e",
				"A. M. Baxter, \u003ca href=\"https://pdfs.semanticscholar.org/2c5d/79e361d3aecb25c380402144177ad7cd9dc8.pdf\"\u003eAlgorithms for Permutation Statistics\u003c/a\u003e, Ph. D. Dissertation, Rutgers University, May 2011. See p. 15.",
				"E. Ben-Naim and P. L. Krapivsky, \u003ca href=\"http://arxiv.org/abs/1112.0049\"\u003ePopularity-Driven Networking\u003c/a\u003e, arXiv preprint arXiv:1112.0049 [cond-mat.stat-mech], 2011.",
				"David Bevan, Robert Brignall, Andrew Elvey Price and Jay Pantone, \u003ca href=\"http://arxiv.org/abs/1711.10325\"\u003eA structural characterisation of Av(1324) and new bounds on its growth rate\u003c/a\u003e, arXiv preprint arXiv:1711.10325 [math.CO], 2017.",
				"Daniel Birmajer, Juan B. Gil, and Michael D. Weiner, \u003ca href=\"https://arxiv.org/abs/1803.07727\"\u003eA family of Bell transformations\u003c/a\u003e, arXiv:1803.07727 [math.CO], 2018.",
				"M. Bousquet-Mélou and A. Jehanne, \u003ca href=\"https://arxiv.org/abs/math/0504018\"\u003ePolynomial equations with one catalytic variable, algebraic series and map enumeration\u003c/a\u003e, arXiv:math/0504018 [math.CO], 2005; J Comb. Thy. B 96 (2006), 623-672.",
				"W. G. Brown, \u003ca href=\"http://dx.doi.org/10.4153/CJM-1963-056-7\"\u003eEnumeration of non-separable planar maps\u003c/a\u003e, Canad. J. Math., 15 (1963), 526-545.",
				"Colin Defant, \u003ca href=\"https://arxiv.org/abs/1903.09138\"\u003eCounting 3-Stack-Sortable Permutations\u003c/a\u003e, arXiv:1903.09138 [math.CO], 2019.",
				"A. Del Lungo, F. Del Ristoro and J.-G. Penaud, \u003ca href=\"http://dx.doi.org/10.1016/S0304-3975(98)00025-5\"\u003eLeft ternary trees and non-separable rooted planar maps\u003c/a\u003e, Theor. Comp. Sci., 233, 2000, 201-215.",
				"E. Duchi, V. Guerrini, S. Rinaldi and G. Schaeffer, \u003ca href=\"http://www.mat.univie.ac.at/~slc/s/43%20Duchi%20Guerrini%20Rinaldi%20Schaeffer.html\"\u003eFighting fish: enumerative properties\u003c/a\u003e, Sém. Lothar. Combin. 78B (2017), Art. 43, 12 pp.",
				"S. Dulucq, S. Gire, and O. Guibert, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(98)80005-8\"\u003eA combinatorial proof of J. West's conjecture\u003c/a\u003e Discrete Math. 187 (1998), no. 1-3, 71--96. MR1630680(99f:05053).",
				"S. Dulucq, S. Gire, and J. West, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(95)00130-O\"\u003ePermutations with forbidden subsequences and nonseparable planar maps\u003c/a\u003e, Proceedings of the 5th Conference on Formal Power Series and Algebraic Combinatorics (Florence, 1993). Discrete Math. 153 (1996), no. 1-3, 85-103. MR1394948 (98a:05081)",
				"Eric S. Egge, \u003ca href=\"/A000139/a000139.pdf\"\u003eDefying God: The Stanley-Wilf Conjecture, Stanley-Wilf Limits, and a Two-Generation Explosion of Combinatorics\u003c/a\u003e, MAA Focus, August/September 2015, pp. 33-34. [Annotated scanned copy]",
				"W. Fang, \u003ca href=\"http://arxiv.org/abs/1711.0571\"\u003eFighting fish and two-stack sortable permutations\u003c/a\u003e, arXiv preprint arXiv:1711.0571 [math.CO], 2017.",
				"P. Flajolet and R. Sedgewick, \u003ca href=\"http://algo.inria.fr/flajolet/Publications/books.html\"\u003eAnalytic Combinatorics\u003c/a\u003e, 2009; see page 713",
				"I. Gessel and G. Xin, \u003ca href=\"http://www.combinatorics.org/Volume_13/Abstracts/v13i1r53.html\"\u003eThe generating function of ternary trees and continued fractions\u003c/a\u003e, Electron. J Combin. 13 (2006), paper 53.",
				"O. Guibert, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(99)00121-1\"\u003eStack words, standard Young tableaux, permutations with forbidden subsequences and planar maps\u003c/a\u003e, Discr. Math., 210 (2000), 71-85.",
				"Elizabeth Hartung, Hung Phuc Hoang, Torsten Mütze, and Aaron Williams, \u003ca href=\"https://arxiv.org/abs/1906.06069\"\u003eCombinatorial generation via permutation languages. I. Fundamentals\u003c/a\u003e, arXiv:1906.06069 [cs.DM], 2019.",
				"Hsien-Kuei Hwang, Mihyun Kang, and Guan-Huei Duh, \u003ca href=\"https://doi.org/10.4230/LIPIcs.AofA.2018.29\"\u003eAsymptotic Expansions for Sub-Critical Lagrangean Forms\u003c/a\u003e, LIPIcs Proceedings of Analysis of Algorithms 2018, Vol. 110. Schloss Dagstuhl-Leibniz-Zentrum für Informatik, 2018.",
				"S. Kitaev, P. Salimov, C. Severs and H. Ulfarsson, \u003ca href=\"http://staff.ru.is/henningu/papers/maps/maps.pdf\"\u003eRestricted non-separable planar maps and some pattern avoiding permutations\u003c/a\u003e, 2012.",
				"S. Kitaev, P. Salimov, C. Severs and H. Ulfarsson, \u003ca href=\"https://doi.org/10.1016/j.dam.2013.01.004\"\u003eRestricted non-separable planar maps and some pattern avoiding permutations\u003c/a\u003e, Discrete Applied Mathematics, Volume 161, Issues 16-17, November 2013, Pages 2514-2526.",
				"Sergey Kitaev, Anna de Mier, and Marc Noy, \u003ca href=\"http://dx.doi.org/10.1016/j.ejc.2013.06.013\"\u003eOn the number of self-dual rooted maps\u003c/a\u003e, European J. Combin. 35 (2014), 377-387. MR3090510. See Theorem 1.",
				"Sergey Kitaev, Pavel Salimov, Christopher Severs, and Henning Ulfarsson, \u003ca href=\"http://arxiv.org/abs/1202.1790\"\u003eRestricted rooted non-separable planar maps\u003c/a\u003e, arXiv preprint arXiv:1202.1790 [math.CO], 2012.",
				"Arturo Merino and Torsten Mütze, \u003ca href=\"https://arxiv.org/abs/2103.09333\"\u003eCombinatorial generation via permutation languages. III. Rectangulations\u003c/a\u003e, arXiv:2103.09333 [math.CO], 2021.",
				"Alois Panholzer, \u003ca href=\"https://arxiv.org/abs/2007.14676\"\u003eParking function varieties for combinatorial tree models\u003c/a\u003e, arXiv:2007.14676 [math.CO], 2020.",
				"L.-F. Préville-Ratelle and X. Viennot, \u003ca href=\"http://arxiv.org/abs/1406.3787\"\u003eAn extension of Tamari lattices\u003c/a\u003e, arXiv preprint arXiv:1406.3787 [math.CO], 2014.",
				"G. Schaeffer, \u003ca href=\"http://www.lix.polytechnique.fr/~schaeffe/Biblio/Sc03.ps\"\u003eA combinatorial interpretation of super-Catalan numbers of order two, (2001).",
				"W. T. Tutte, \u003ca href=\"http://dx.doi.org/10.4153/CJM-1963-029-x\"\u003eA Census of Planar Maps\u003c/a\u003e, Canad. J. Math. 15 (1963), 249-271.",
				"J. West, \u003ca href=\"http://dx.doi.org/10.1016/0304-3975(93)90321-J\"\u003eSorting twice through a stack\u003c/a\u003e, Conference on Formal Power Series and Algebraic Combinatorics (Bordeaux, 1991), Theoret. Comput. Sci. 117 (1993), no. 1-2, 303-313.",
				"D. Zeilberger, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(92)90351-F\"\u003eA proof of Julian West's conjecture that the number of two-stacksortable permutations of length n is 2(3n)!/((n + 1)!(2n + 1)!)\u003c/a\u003e, Discrete Math., 102 (1992), 85-93.",
				"P. Zinn-Justin and J.-B. Zuber, \u003ca href=\"https://doi.org/10.1016/S0012-365X(01)00267-9\"\u003eMatrix integrals and the counting of tangles and links\u003c/a\u003e, Discrete Math 246 (2002), 343-360.",
				"P. Zinn-Justin and J.-B. Zuber, \u003ca href=\"http://arxiv.org/abs/math-ph/0303049\"\u003eMatrix integrals and the generation and counting of virtual tangles and links\u003c/a\u003e, arXiv:math-ph/0303049, 2003; J.Knot Theor. Ramifications 13 (2004) 325-356."
			],
			"formula": [
				"a(n) = 2*C(3*n, 2*n+1)/(n*(n+1)), or 2*(3*n)!/((2*n+1)!*((n+1)!)).",
				"Using Stirling's formula in A000142 it easy to get the asymptotic expression a(n) ~ (27/4)^n / (sqrt(Pi*n / 3) * (2*n + 1) * (n + 1)). - Dan Fux (dan.fux(AT)OpenGaia.com or danfux(AT)OpenGaia.com), Apr 13 2001",
				"G.f.: A(z) = 2 + z*B(z), where B(z) = 1 - 8*z + 2*z*(5-6*z)*B - 2*z^2*(1+3*z)*B^2 - z^4*B^3.",
				"G.f.: (2/(3*x)) * (hypergeom([-2/3, -1/3],[1/2],(27/4)*x)-1). - _Mark van Hoeij_, Nov 02 2009",
				"G.f.: (2-3*R)/(R-1)^2 where R := RootOf(x-t*(t-1)^2,t) is an algebraic function in Maple notation. - _Mark van Hoeij_, Nov 08 2011",
				"G.f.: 2*Q(0), where Q(k) = 1 + 3*x*(3*k+1)*(6*k+1)/(2*(k+1)*(4*k+3) - 6*x*(k+1)*(3*k+2)*(4*k+3)*(6*k+5)/(3*x*(3*k+2)*(6*k+5) + (2*k+3)*(4*k+5)/Q(k+1))); (continued fraction). - _Sergei N. Gladkovskii_, Apr 30 2013",
				"E.g.f.: 2*Q(0), where Q(k) = 1 + 3*x*(3*k+1)*(6*k+1)/(2*(k+1)*(2*k+1)*(4*k+3) - 6*x*(k+1)*(2*k+1)*(3*k+2)*(4*k+3)*(6*k+5)/(3*x*(3*k+2)*(6*k+5) + (2*k+2)*(2*k+3)*(4*k+5)/Q(k+1))); (continued fraction). - _Sergei N. Gladkovskii_, Apr 30 2013",
				"a(n) = A007318(3*n, 2*n+1)/A000217(n) for n \u003e 0. - _Reinhard Zumkeller_, Feb 17 2013",
				"a(n) is the n-th Hausdorff moment of the positive function w(x) defined on (0,27) which is equal (in Maple notation) to w(x) = 3*sqrt(3)*2^(2/3)*(3-sqrt(81-12*x)/9)*(1+sqrt(81-12*x)/9)^(1/3)/(8*Pi*x^(2/3))-sqrt(3)*2^(1/3)*(3+sqrt(81-12*x)/9)*(1+sqrt(81-12*x)/9)^(-1/3)/(4*Pi*x^(1/3)), that is, a(n) is the integral int(x^n * w(x), x=0..27/4), n=0,1,2.... The function w(x) is unique. - _Karol A. Penson_, Jun 17 2013",
				"D-finite with recurrence 2*(n+1)*(2*n+1)*a(n) -3*(3*n-1)*(3*n-2)*a(n-1)=0. - _R. J. Mathar_, Aug 21 2014",
				"G.f. A(z) is related to the g.f. M(z) of A000168 by M(z) = 1 + A(z*M(z)^2)) (see Tutte 1963, equation 6.3). - _Noam Zeilberger_, Nov 02 2016",
				"From _Ilya Gutkovskiy_, Jan 17 2017: (Start)",
				"E.g.f.: 2*2F2(1/3,2/3; 3/2,2; 27*x/4).",
				"Sum_{n\u003e=0} 1/a(n) = (1/2)*3F2(1,3/2,2; 1/3,2/3; 4/27) = 2.226206199291261... (End)",
				"G.f. A(z) is the solution to the initial value problem 4*A + 2*z*A' = 8 + 3*z*A + 9*z^2*A' + 2*z^2*A*A', A(0) = 2. - _Bjarki Ágúst Guðmundsson_, Jul 03 2017",
				"a(n+1) = a(n)*3*(3*n+1)*(3*n+2)/(2*(n+2)*(2*n+3)). - _Chai Wah Wu_, Apr 02 2021",
				"a(n) = 4*(3n!)/(n!*(2n+2)!). - _Chai Wah Wu_, Dec 15 2021"
			],
			"example": [
				"G.f. = 2 + x + 2*x^2 + 6*x^3 + 22*x^4 + 91*x^5 + 408*x^6 + 1938*x^7 + ..."
			],
			"maple": [
				"A000139 := n-\u003e2*(3*n)!/((2*n+1)!*((n+1)!)): seq(A000139(n), n=0..23);"
			],
			"mathematica": [
				"Table[(2(3n)!)/((2n+1)!(n+1)!),{n,0,30}] (* _Harvey P. Dale_, Mar 31 2013 *)"
			],
			"program": [
				"(Haskell)",
				"a000139 0 = 2",
				"a000139 n = ((3 * n) `a007318` (2 * n + 1)) `div` a000217 n",
				"-- _Reinhard Zumkeller_, Feb 17 2013",
				"(Python)",
				"from sympy import binomial",
				"def A000139(n): return (binomial(3*n, n)*2)//((n+1)*(2*n+1))",
				"(Sage)",
				"def A000139(n): return (binomial(3*n, n)*2)//((n+1)*(2*n+1))",
				"[A000139(n) for n in (0..23)]  # _Peter Luschny_, Jun 17 2013",
				"(PARI) a(n)=binomial(3*n,n)*2/((n+1)*(2*n+1)); \\\\ _Joerg Arndt_, Jul 21 2014",
				"(MAGMA) [2*Factorial(3*n)/(Factorial(2*n+1)*Factorial(n+1)): n in [0..25]]; // _Vincenzo Librandi_, Apr 20 2015",
				"(Python)",
				"A000139_list = [2]",
				"for n in range(1,30):",
				"    A000139_list.append(3*(3*n-2)*(3*n-1)*A000139_list[-1]//(2*n+2)//(2*n+1)) # _Chai Wah Wu_, Apr 02 2021"
			],
			"xref": [
				"Cf. A000142, A000309, A005802, A006335, A004677.",
				"Row m=1 of A210664(n\u003e0)."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_, entry revised Apr 24 2012",
			"references": 22,
			"revision": 211,
			"time": "2021-12-15T22:05:50-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}