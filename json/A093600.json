{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A093600",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 93600,
			"data": "1,1,3,4,25,6,49,176,621,100,7381,552,86021,11662,18075,91072,2436559,133542,14274301,5431600,9484587,2764366,19093197,61931424,399698125,281538452,8770427199,1513702904,315404588903,323507400,9304682830147",
			"name": "Numerator of Sum_{1\u003c=k\u003c=n, gcd(k,n)=1} 1/k.",
			"comment": [
				"The divisibility properties of this sequence are given by Leudesdorf's theorem.",
				"Problem: are there numbers n \u003e 1 such that n^4 | a(n)? Let b(n) be the numerator of Sum_{1\u003c=k\u003c=n, gcd(k,n)=1} 1/k^2. Conjecture: if, for some e \u003e 0, n^e | a(n), then n^(e-1) | b(n). It appears that, for any odd number n, n^e | a(n) if and only if n^(e-1) | b(n). - _Thomas Ordowski_, Aug 12 2019"
			],
			"reference": [
				"G. H. Hardy and E. M. Wright, An Introduction to the Theory of Numbers, 4th ed., Oxford Univ. Press, 1971, page 100. [3rd. ed., Theorem 128, page 101]"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A093600/b093600.txt\"\u003eTable of n, a(n) for n = 1..2310\u003c/a\u003e",
				"Emre Alkan, \u003ca href=\"http://www.jstor.org/stable/2975168\"\u003eVariations on Wolstenholme's Theorem\u003c/a\u003e, Amer. Math. Monthly, Vol. 101, No. 10 (Dec. 1994), 1001-1004.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/LeudesdorfTheorem.html\"\u003eLeudesdorf Theorem\u003c/a\u003e"
			],
			"formula": [
				"G.f. A(x) (for fractions) satisfies: A(x) = -log(1 - x)/(1 - x) - Sum_{k\u003e=2} A(x^k)/k. - _Ilya Gutkovskiy_, Mar 31 2020"
			],
			"mathematica": [
				"Table[s=0; Do[If[GCD[i, n]==1, s=s+1/i], {i, n}]; Numerator[s], {n, 1, 35}]"
			],
			"program": [
				"(PARI) for (n=1, 40, print1(numerator(sum(k=1, n, if (gcd(k, n)==1, 1/k))), \", \")) \\\\ _Seiichi Manyama_, Aug 11 2017",
				"(MAGMA) [Numerator(\u0026+[1/k:k in [1..n]|Gcd(k,n) eq 1]):n in [1..31]]; // _Marius A. Burtea_, Aug 14 2019"
			],
			"xref": [
				"Cf. A069220 (denominator of this sum), A001008 (numerator of the n-th harmonic number)."
			],
			"keyword": "nonn,frac",
			"offset": "1,3",
			"author": "_T. D. Noe_, Apr 03 2004",
			"references": 4,
			"revision": 28,
			"time": "2021-08-29T20:20:14-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}