{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A308608",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 308608,
			"data": "1,1,1,1,1,2,1,1,1,1,1,3,1,1,1,1,1,3,1,2,1,1,1,6,1,1,1,2,1,6,1,1,1,1,1,8,1,1,1,4,1,5,1,1,2,1,1,11,1,1,1,1,1,5,1,3,1,1,1,36,1,1,2,1,1,4,1,1,1,3,1,32,1,1,1,1,1,4,1,7,1,1,1,28,1,1",
			"name": "For any n \u003e 0 and k \u003e= 0, let w_n(k) be the number of ways to write k as a sum of distinct divisors of n; a(n) = max_{k \u003e= 0} w_n(k).",
			"comment": [
				"If m and n are coprime then a(m*n) \u003e= a(m)*a(n). - _Charlie Neder_, Jun 11 2019"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A308608/b308608.txt\"\u003eTable of n, a(n) for n = 1..65537\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A308608/a308608.png\"\u003eColored logarithmic scatterplot of the first 10000 terms\u003c/a\u003e (where the color is function of A000005(n))"
			],
			"example": [
				"For n = 6: w_6 takes the following values:",
				"    k     w_6(k)    Corresponding sets of divisors",
				"    --    ------    ------------------------------",
				"     0    1         {}",
				"     1    1         {1}",
				"     2    1         {2}",
				"     3    2         {1, 2}, {3}",
				"     4    1         {1, 3}",
				"     5    1         {2, 3}",
				"     6    2         {1, 2, 3}, {6}",
				"     7    1         {1, 6}",
				"     8    1         {2, 6}",
				"     9    2         {1, 2, 6}, {3, 6}",
				"    10    1         {1, 3, 6}",
				"    11    1         {2, 3, 6}",
				"    12    1         {1, 2, 3, 6}",
				"    \u003e12   0         None",
				"Hence a(6) = 2."
			],
			"program": [
				"(PARI) a(n) = my (p=1); fordiv (n,d,p*=(1+'X^d)); vecmax(Vec(p))"
			],
			"xref": [
				"Cf. A000005, A027750."
			],
			"keyword": "nonn",
			"offset": "1,6",
			"author": "_Rémy Sigrist_, Jun 10 2019",
			"references": 1,
			"revision": 18,
			"time": "2020-05-20T12:44:07-04:00",
			"created": "2019-06-11T17:48:21-04:00"
		}
	]
}