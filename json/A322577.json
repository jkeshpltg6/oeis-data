{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A322577",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 322577,
			"data": "1,4,6,11,10,24,14,28,26,40,22,66,26,56,60,68,34,104,38,110,84,88,46,168,74,104,102,154,58,240,62,160,132,136,140,286,74,152,156,280,82,336,86,242,260,184,94,408,146,296,204,286,106,408,220,392,228,232,118,660",
			"name": "a(n) = Sum_{d|n} psi(n/d) * phi(d).",
			"comment": [
				"Dirichlet convolution of Dedekind psi function (A001615) with Euler totient function (A000010).",
				"Dirichlet convolution of A008966 with A018804.",
				"Dirichlet convolution of A038040 with A271102."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A322577/b322577.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DedekindFunction.html\"\u003eDedekind Function\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/TotientFunction.html\"\u003eTotient Function\u003c/a\u003e."
			],
			"formula": [
				"Dirichlet g.f.: zeta(s-1)^2 / zeta(2*s).",
				"a(p) = 2*p, where p is prime.",
				"Sum_{k=1..n} a(k) ~ 45*n^2*(2*Pi^4*log(n) - Pi^4 + 4*gamma*Pi^4 - 360*zeta'(4)) / (2*Pi^8), where gamma is the Euler-Mascheroni constant A001620 and for zeta'(4) see A261506. - _Vaclav Kotesovec_, Aug 31 2019",
				"a(p^k) = (k+1)*p^k - (k-1)*p^(k-2) where p is prime. - _Robert Israel_, Sep 01 2019",
				"a(n) = Sum_{k=1..n} psi(gcd(n,k)). - _Ridouane Oudra_, Nov 29 2019",
				"a(n) = Sum_{k=1..n} psi(n/gcd(n,k))*phi(gcd(n,k))/phi(n/gcd(n,k)). - _Richard L. Ollerton_, May 07 2021"
			],
			"maple": [
				"f:= proc(n) local t;",
				"  mul((t[2]+1)*t[1]^t[2] - (t[2]-1)*t[1]^(t[2]-2), t = ifactors(n)[2])",
				"end proc:",
				"map(f, [$1..100]); # _Robert Israel_, Sep 01 2019"
			],
			"mathematica": [
				"Table[Sum[DirichletConvolve[j, MoebiusMu[j]^2, j, n/d] EulerPhi[d], {d, Divisors[n]}], {n, 1, 60}]",
				"f[p_, e_] := (e + 1)*p^e - (e - 1)*p^(e - 2); a[1] = 1; a[n_] := Times @@ (f @@@ FactorInteger[n]); Array[a, 100] (* _Amiram Eldar_, Oct 26 2020 *)"
			],
			"program": [
				"(PARI) seq(n) = {dirmul(vector(n, n, eulerphi(n)), vector(n, n, n * sumdivmult(n, d, issquarefree(d)/d)))} \\\\ _Andrew Howroyd_, Aug 29 2019"
			],
			"xref": [
				"Cf. A000010, A001615, A008966, A018804, A038040, A271102."
			],
			"keyword": "nonn,mult,easy",
			"offset": "1,2",
			"author": "_Ilya Gutkovskiy_, Aug 29 2019",
			"references": 8,
			"revision": 43,
			"time": "2021-05-07T06:55:38-04:00",
			"created": "2019-08-31T10:21:06-04:00"
		}
	]
}