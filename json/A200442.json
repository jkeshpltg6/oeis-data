{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A200442",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 200442,
			"data": "1,31,960,29729,920639,28510080,882891841,27341136991,846692354880,26220121864289,811977085438079,25145069526716160,778685178242762881,24114095455998933151,746758273957724164800,23125392397233450175649,716140406040279231280319",
			"name": "Expansion of 1/(1-31*x+x^2).",
			"comment": [
				"A Diophantine property of these numbers: (a(n+1)-a(n-1))^2 - 957*a(n)^2 = 4. (See also comment in A200441.)",
				"For n\u003e=1, a(n) equals the number of 01-avoiding words of length n-1 on alphabet {0,1,...,30}. - _Milan Janjic_, Jan 26 2015"
			],
			"link": [
				"Bruno Berselli, \u003ca href=\"/A200442/b200442.txt\"\u003eTable of n, a(n) for n = 0..500\u003c/a\u003e",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (31,-1)."
			],
			"formula": [
				"G.f.: 1/(1-31*x+x^2).",
				"a(n) = 31*a(n-1)-a(n-2) with a(0)=1, a(1)=31.",
				"a(n) = -a(-n-2) = (t^(n+1)-1/t^(n+1))/(t-1/t) where t=(31+sqrt(957))/2.",
				"a(n) = sum((-1)^k*binomial(n-k, k)*31^(n-2k), k=0..floor(n/2)).",
				"a(n) = Sum_{k, 0\u003c=k\u003c=n} A101950(n,k)*30^k. - _Philippe Deléham_, Feb 10 2012",
				"Product {n \u003e= 0} (1 + 1/a(n)) = 1/29*(29 + sqrt(957)). - _Peter Bala_, Dec 23 2012",
				"Product {n \u003e= 1} (1 - 1/a(n)) = 1/62*(29 + sqrt(957)). - _Peter Bala_, Dec 23 2012"
			],
			"mathematica": [
				"LinearRecurrence[{31, -1}, {1, 31}, 17]"
			],
			"program": [
				"(PARI)  Vec(1/(1-31*x+x^2)+O(x^17))",
				"(MAGMA) /* By the closed form: */ Z\u003cx\u003e:=PolynomialRing(Integers()); N\u003cr\u003e:=NumberField(x^2-957); S:=[(((31+r)/2)^n-1/((31+r)/2)^n)/r: n in [1..17]]; [Integers()!S[j]: j in [1..#S]];",
				"(Maxima)  makelist(sum((-1)^k*binomial(n-k,k)*31^(n-2*k),k,0,floor(n/2)),n,0,16);"
			],
			"xref": [
				"Cf. A029548, A097313, A200441."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Bruno Berselli_, Nov 18 2011",
			"references": 6,
			"revision": 30,
			"time": "2015-12-29T04:35:02-05:00",
			"created": "2011-11-18T12:56:56-05:00"
		}
	]
}