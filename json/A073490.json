{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A073490",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 73490,
			"data": "0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,1,1,1,0,0,0,1,0,1,0,0,0,0,1,1,0,0,0,1,1,1,0,1,0,1,0,1,0,0,0,1,1,1,0,0,1,1,1,1,0,0,0,1,1,0,1,1,0,1,1,1,0,0,0,1,0,1,0,1,0,1,0,1,0,1,1,1,1,1,0,0,1,1,1,1,1,0,0,1,1,1,0,1,0,1,0,1,0,0,0,2,1,1,0,1,1,1,1,1,1,0",
			"name": "Number of prime gaps in factorization of n.",
			"comment": [
				"A137723(n) is the smallest number of the first occurring set of exactly n consecutive numbers with at least one prime gap in their factorization: a(A137723(n)+k)\u003e0 for 0\u003c=k\u003cn and a(A137723(n)-1)=a(A137723(n)+n)=0. - _Reinhard Zumkeller_, Feb 09 2008"
			],
			"link": [
				"R. Zumkeller, \u003ca href=\"/A073490/b073490.txt\"\u003eTable of n, a(n) for n = 1..50000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A073484(A007947(n)).",
				"a(A000040(n))=0; a(A000961(n))=0; a(A006094(n))=0; a(A002110(n))=0; a(A073485(n))=0.",
				"a(A073486(n))\u003e0; a(A073487(n)) = 1; a(A073488(n))=2; a(A073489(n))=3.",
				"a(n)=0 iff A073483(n) = 1.",
				"a(A097889(n)) = 0. - _Reinhard Zumkeller_, Nov 20 2004",
				"0 \u003c= a(m*n) \u003c= a(m) + a(n) + 1. A137794(n) = 0^a(n). - _Reinhard Zumkeller_, Feb 11 2008"
			],
			"example": [
				"84 = 2*2*3*7 with one gap between 3 and 7, therefore a(84) = 1;",
				"110 = 2*5*11 with two gaps: between 2 and 5 and between 5 and 11, therefore a(110) = 2."
			],
			"maple": [
				"A073490 := proc(n)",
				"    local a,plist ;",
				"    plist := sort(convert(numtheory[factorset](n),list)) ;",
				"    a := 0 ;",
				"    for i from 2 to nops(plist) do",
				"        if op(i,plist) \u003c\u003e nextprime(op(i-1,plist)) then",
				"            a := a+1 ;",
				"        end if;",
				"    end do:",
				"    a;",
				"end proc:",
				"seq(A073490(n),n=1..110) ; # _R. J. Mathar_, Oct 27 2019"
			],
			"mathematica": [
				"gaps[n_Integer/;n\u003e0]:=If[n===1, 0, Complement[Prime[PrimePi[Rest[ # ]]-1], # ]\u0026[First/@FactorInteger[n]]]; Table[Length[gaps[n]], {n, 1, 105}] (_Wouter Meeussen_, Oct 30 2004)",
				"pa[n_, k_] := If[k == NextPrime[n], 0, 1]; Table[Total[pa @@@ Partition[First /@ FactorInteger[n], 2, 1]], {n, 120}] (* _Jayanta Basu_, Jul 01 2013 *)"
			],
			"program": [
				"(Haskell)",
				"a073490 1 = 0",
				"a073490 n = length $ filter (\u003e 1) $ zipWith (-) (tail ips) ips",
				"   where ips = map a049084 $ a027748_row n",
				"-- _Reinhard Zumkeller_, Jul 04 2012",
				"(Python)",
				"from sympy import primefactors, nextprime",
				"def a(n):",
				"    pf = primefactors(n)",
				"    return sum(p2 != nextprime(p1) for p1, p2 in zip(pf[:-1], pf[1:]))",
				"print([a(n) for n in range(1, 121)]) # _Michael S. Branicky_, Oct 14 2021"
			],
			"xref": [
				"Cf. A073491, A073492, A073493, A073494, A073495.",
				"Cf. A137721, A137722, A027748, A049084."
			],
			"keyword": "nonn,nice",
			"offset": "1,110",
			"author": "_Reinhard Zumkeller_, Aug 03 2002",
			"ext": [
				"More terms from _Franklin T. Adams-Watters_, May 19 2006"
			],
			"references": 17,
			"revision": 29,
			"time": "2021-10-15T11:26:55-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}