{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A192798",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 192798,
			"data": "1,0,1,2,3,10,17,42,87,188,411,876,1907,4100,8863,19134,41289,89174,192459,415542,897049,1936576,4180809,9025544,19484825,42064320,90809993,196043706,423225563,913674090,1972469945,4258235410,9192822255",
			"name": "Constant term in the reduction of the n-th Fibonacci polynomial by x^3-\u003ex^2+2. See Comments.",
			"comment": [
				"For discussions of polynomial reduction, see A192232 and A192744."
			],
			"link": [
				"\u003ca href=\"/index/Rec\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,3,0,-3,1,1)."
			],
			"formula": [
				"a(n) = a(n-1)+3*a(n-2)-3*a(n-4)+a(n-5)+a(n-6).",
				"G.f.: -x*(x-1)*(x+1)*(x^2+x-1)/(x^6+x^5-3*x^4+3*x^2+x-1). [_Colin Barker_, Jul 27 2012]"
			],
			"example": [
				"The first five polynomials p(n,x) and their reductions:",
				"F1(x)=1 -\u003e 1",
				"F2(x)=x -\u003e x",
				"F3(x)=x^2+1 -\u003e x^2+1",
				"F4(x)=x^3+2x -\u003e x^2+2x+2",
				"F5(x)=x^4+3x^2+1 -\u003e 4x^2+2*x+3, so that",
				"A192798=(1,0,1,2,3,...), A192799=(0,1,0,2,2,...), A192800=(0,0,1,1,4,...)"
			],
			"mathematica": [
				"q = x^3; s = x^2 + 2; z = 40;",
				"p[n_, x_] := Fibonacci[n, x];",
				"Table[Expand[p[n, x]], {n, 1, 7}]",
				"reduce[{p1_, q_, s_, x_}] :=",
				"FixedPoint[(s PolynomialQuotient @@ #1 +",
				"       PolynomialRemainder @@ #1 \u0026)[{#1, q, x}] \u0026, p1]",
				"t = Table[reduce[{p[n, x], q, s, x}], {n, 1, z}];",
				"u1 = Table[Coefficient[Part[t, n], x, 0], {n, 1, z}]  (* A192798 *)",
				"u2 = Table[Coefficient[Part[t, n], x, 1], {n, 1, z}]",
				"  (* A192799 *)",
				"u3 = Table[Coefficient[Part[t, n], x, 2], {n, 1, z}]",
				"  (* A192800 *)"
			],
			"xref": [
				"Cf. A192744, A192232, A192616, A192799, A192800."
			],
			"keyword": "nonn,easy",
			"offset": "1,4",
			"author": "_Clark Kimberling_, Jul 10 2011",
			"ext": [
				"Comment in Mathematica code corrected by _Colin Barker_, Jul 27 2012"
			],
			"references": 6,
			"revision": 14,
			"time": "2015-06-13T00:53:54-04:00",
			"created": "2011-07-11T11:07:42-04:00"
		}
	]
}