{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A036968",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 36968,
			"data": "1,-1,0,1,0,-3,0,17,0,-155,0,2073,0,-38227,0,929569,0,-28820619,0,1109652905,0,-51943281731,0,2905151042481,0,-191329672483963,0,14655626154768697,0,-1291885088448017715,0,129848163681107301953",
			"name": "Genocchi numbers (of first kind): expansion of 2*x/(exp(x)+1).",
			"comment": [
				"The sign of a(1) depends on which convention one chooses: B(n) = B_n(1) or B(n) = B_n(0) where B(n) are the Bernoulli numbers and B_n(x) the Bernoulli polynomials (see the Wikipedia article on Bernoulli numbers). The definition given is in line with B(n) = B_n(0). The convention B(n) = B_n(1) corresponds to the e.g.f. -2*x/(1+exp(-x)). - _Peter Luschny_, Jun 28 2013",
				"According to Hetyei [2017], \"alternation acyclic tournaments in which at least one ascent begins at each vertex, except for the largest one, are counted by the Genocchi numbers of the first kind.\" - _Danny Rorabaugh_, Apr 25 2017",
				"Named after the Italian mathematician Angelo Genocchi (1817-1889). - _Amiram Eldar_, Jun 06 2021",
				"Conjecture: For any positive integer n, -a(n+1) is the permanent of the n X n matrix M with M(j, k) = floor((2*j - k)/n), (j,k=1..n). - _Zhi-Wei Sun_, Sep 07 2021",
				"A corresponding conjecture can also be made for L. Seidel's 'Genocchi numbers of second kind' A005439. - _Peter Luschny_, Sep 08 2021"
			],
			"reference": [
				"Louis Comtet, Advanced Combinatorics, Reidel, 1974, p. 49.",
				"A. Fletcher, J. C. P. Miller, L. Rosenhead and L. J. Comrie, An Index of Mathematical Tables. Vols. 1 and 2, 2nd ed., Blackwell, Oxford and Addison-Wesley, Reading, MA, 1962, Vol. 1, p. 73.",
				"Ronald L. Graham, Donald E. Knuth and Oren Patashnik, Concrete Mathematics. Addison-Wesley, Reading, MA, 1990, p. 528.",
				"Richard P. Stanley, Enumerative Combinatorics, Cambridge, Vol. 2, 1999; see Problem 5.8."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A036968/b036968.txt\"\u003eTable of n, a(n) for n = 1..551\u003c/a\u003e",
				"R. C. Archibald, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-45-99088-0\"\u003eReview of Terrill-Terrill paper\u003c/a\u003e, Math. Comp., Vol. 1, No. 10 (1945), pp. 385-386.",
				"Beáta Bényi and Matthieu Josuat-Vergès, \u003ca href=\"https://arxiv.org/abs/2010.10060\"\u003eCombinatorial proof of an identity on Genocchi numbers\u003c/a\u003e, arXiv:2010.10060 [math.CO], 2020.",
				"Kwang-Wu Chen, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL6/Chen/chen50.html\"\u003eAn Interesting Lemma for Regular C-fractions\u003c/a\u003e, J. Integer Seqs., Vol. 6 (2003), Article 03.4.8.",
				"Dominique Dumont, \u003ca href=\"http://projecteuclid.org/euclid.dmj/1077310398\"\u003eInterpretations combinatoires des nombres de Genocchi\u003c/a\u003e, Duke Math. J., Vol. 41 (1974), pp. 305-318.",
				"Dominique Dumont, \u003ca href=\"/A001469/a001469_3.pdf\"\u003eInterprétations combinatoires des nombres de Genocchi\u003c/a\u003e, Duke Math. J., Vol. 41 (1974), pp. 305-318. (Annotated scanned copy)",
				"Shishuo Fu, Zhicong Lin and Zhi-Wei Sun, \u003ca href=\"https://arxiv.org/abs/2109.11506\"\u003eProofs of five conjectures relating permanents to combinatorial sequences\u003c/a\u003e, arXiv:2109.11506 [math.CO], 2021.",
				"Hans-Christian Herbig, Daniel Herden and Christopher Seaton, \u003ca href=\"https://doi.org/10.1090/proc/12806\"\u003eOn compositions with x^2/(1-x)\u003c/a\u003e, Proceedings of the American Mathematical Society, Vol. 143, No. 11 (2015), pp. 4583-4596; \u003ca href=\"http://arxiv.org/abs/1404.1022\"\u003earXiv preprint\u003c/a\u003e, arXiv:1404.1022 [math.SG], 2014.",
				"Gábor Hetyei, \u003ca href=\"https://doi.org/10.1016/j.ejc.2019.04.007\"\u003eAlternation acyclic tournaments\u003c/a\u003e, European Journal of Combinatorics, Vol. 81 (2019), pp. 1-21; \u003ca href=\"https://arxiv.org/abs/1704.07245\"\u003earXiv preprint\u003c/a\u003e, arXiv:math/1704.07245 [math.CO], 2017.",
				"G. Kreweras, \u003ca href=\"http://dx.doi.org/10.1006/eujc.1995.0081\"\u003eSur les permutations comptées par les nombres de Genocchi de 1-ière et 2-ième espèce\u003c/a\u003e, Europ. J. Comb., Vol. 18 (1997), pp. 49-58.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/2108.07723\"\u003eArithmetic properties of some permanents\u003c/a\u003e, arXiv:2108.07723 [math.GM], 2021.",
				"H. M. Terrill and E. M. Terrill, \u003ca href=\"https://ur.booksc.eu/ireader/2106189\"\u003eTables of numbers related to the tangent coefficients\u003c/a\u003e, J. Franklin Inst., 239 (1945), 66-67.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Bernoulli_number\"\u003eBernoulli number\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Genocchi_number\"\u003eGenocchi number\u003c/a\u003e."
			],
			"formula": [
				"E.g.f.: 2*x/(exp(x)+1).",
				"a(n) = 2*(1-2^n)*B_n (B = Bernoulli numbers). - _Benoit Cloitre_, Oct 26 2003",
				"2*x/(exp(x)+1) = x + Sum_{n\u003e=1} x^(2*n)*G_{2*n}/(2*n)!.",
				"a(n) = Sum_{k=0..n-1} binomial(n,k) 2^k*B(k). - _Peter Luschny_, Apr 30 2009",
				"From _Sergei N. Gladkovskii_, Dec 12 2012 to Nov 23 2013: (Start) Continued fractions:",
				"E.g.f.: 2*x/(exp(x)+1) = x - x^2/2*G(0) where G(k) = 1 - x^2/(x^2 + 4*(2*k+1)*(2*k+3)/G(k+1)).",
				"E.g.f.: 2/(E(0)+1) where E(k) = 1 + x/(2*k+1 - x*(2*k+1)/(x + (2*k+2)/E(k+1))).",
				"G.f.: 2 - 1/G(0) where G(k) = 1 - x*(k+1)/(1 + x*(k+1)/(1 - x*(k+1)/(1 + x*(k+1)/G(k+1)))).",
				"E.g.f.: 2*x/(1 + exp(x)) = 2*x-2 - 2*T(0), where T(k) = 4*k-1 + x/(2 - x/( 4*k+1 + x/(2 - x/T(k+1)))).",
				"G.f.: 2 - Q(0)/(1-x+x^2) where Q(k) = 1 - x^4*(k+1)^4/(x^4*(k+1)^4 - (1 - x + x^2 + 2*x^2*k*(k+1))*(1 - x + x^2 + 2*x^2*(k+1)*(k+2))/Q(k+1)). (End)",
				"a(n) = n*zeta(1-n)*(2^(n+1)-2) for n \u003e 1. - _Peter Luschny_, Jun 28 2013",
				"O.g.f.: x*Sum_{n\u003e=0} n! * (-x)^n / (1 - n*x) / Product_{k=1..n} (1 - k*x). - _Paul D. Hanna_, Aug 03 2014",
				"Sum_{n\u003e=1} 1/a(2*n) = A321595. - _Amiram Eldar_, May 07 2021",
				"a(n) = (-1)^n*2*n*PolyLog(1 - n, -1). - _Peter Luschny_, Aug 17 2021"
			],
			"maple": [
				"a := n -\u003e n*euler(n-1,0); # _Peter Luschny_, Jul 13 2009"
			],
			"mathematica": [
				"a[n_] := n*EulerE[n - 1, 0]; Table[a[n], {n, 1, 32}] (* _Jean-François Alcover_, Dec 08 2011, after _Peter Luschny_ *)",
				"Range[0, 31]! CoefficientList[ Series[ 2x/(1 + Exp[x]), {x, 0, 32}], x] (* _Robert G. Wilson v_, Oct 26 2012 *)",
				"Table[(-1)^n 2 n PolyLog[1 - n, -1], {n, 1, 32}] (* _Peter Luschny_, Aug 17 2021 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, n! * polcoeff( 2*x / (1 + exp(x + x * O(x^n))), n))}; /* _Michael Somos_, Jul 23 2005 */",
				"(PARI) /* From o.g.f. (_Paul D. Hanna_, Aug 03 2014): */",
				"{a(n)=local(A=1); A=x*sum(m=0, n, m!*(-x)^m/(1-m*x)/prod(k=1,m,1 - k*x +x*O(x^n))); polcoeff(A, n)}",
				"for(n=1, 32, print1(a(n), \", \"))",
				"(Sage) # with a(1) = -1",
				"[z*zeta(1-z)*(2^(z+1)-2) for z in (1..32)]  # _Peter Luschny_, Jun 28 2013",
				"(Sage)",
				"def A036968_list(len):",
				"    e, f, R, C = 4, 1, [], [1]+[0]*(len-1)",
				"    for n in (2..len-1):",
				"        for k in range(n, 0, -1):",
				"            C[k] = C[k-1] / (k+1)",
				"        C[0] = -sum(C[k] for k in (1..n))",
				"        R.append((2-e)*f*C[0])",
				"        f *= n; e *= 2",
				"    return R",
				"print(A036968_list(34)) # _Peter Luschny_, Feb 22 2016"
			],
			"xref": [
				"A001469 is the main entry for this sequence. A226158 is another version.",
				"Cf. A005439 (Genocchi numbers of second kind).",
				"Cf. A083007, A083008, A083009, A083010, A083011, A083012, A083013, A083014, A321595."
			],
			"keyword": "sign,easy,nice",
			"offset": "1,6",
			"author": "_N. J. A. Sloane_",
			"references": 31,
			"revision": 130,
			"time": "2021-09-24T02:18:27-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}