{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A332246",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 332246,
			"data": "0,1,1,2,2,2,3,3,4,4,3,3,4,5,5,4,4,5,5,6,6,6,7,7,8,8,9,9,8,7,7,8,8,8,9,9,8,7,7,8,8,9,9,10,10,10,11,11,12,12,11,11,12,13,13,12,12,13,13,14,14,14,15,15,16,16,15,15,16,17,17,16,16,15,15,14",
			"name": "a(n) is the X-coordinate of the n-th point of the Minkowski sausage (or Minkowski curve). Sequence A332247 gives Y-coordinates.",
			"comment": [
				"This sequence is the real part of {f(n)} defined as:",
				"- f(0) = 0,",
				"- f(n+1) = f(n) + i^t(n)",
				"  where t(n) is the number of 1's and 6's minus the number of 3's and 4's",
				"             in the base 8 representation of n",
				"    and i denotes the imaginary unit.",
				"We can also build the curve by successively applying the following substitution to an initial vector (1, 0):",
				"                        .---\u003e.",
				"                        ^    |",
				"                        |    v",
				"                   .---\u003e.    .    .---\u003e.",
				"                             |    ^",
				"                             v    |",
				"                             .---\u003e."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A332246/b332246.txt\"\u003eTable of n, a(n) for n = 0..4096\u003c/a\u003e",
				"Robert Ferréol (MathCurve), \u003ca href=\"https://www.mathcurve.com/fractals/minkowski/minkowski.shtml\"\u003eSaucisse de Minkowski\u003c/a\u003e [in French]",
				"Rémy Sigrist, \u003ca href=\"/A332246/a332246.png\"\u003eRepresentation of the first 1+8^4 points of the Minkowski sausage\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Minkowski_Sausage\"\u003eMinkowski sausage\u003c/a\u003e",
				"\u003ca href=\"/index/Con#coordinates_2D_curves\"\u003eIndex entries for sequences related to coordinates of 2D curves\u003c/a\u003e"
			],
			"formula": [
				"a(8^k-m) + a(m) = 4^k for any k \u003e= 0 and m = 0..8^k."
			],
			"program": [
				"(PARI) { dd = [0,1,0,-1,-1,0,1,0]; z=0; for (n=0, 75, print1 (real(z)\", \"); z += I^vecsum(apply(d -\u003e dd[1+d], digits(n, #dd)))) }"
			],
			"xref": [
				"See A163528, A323258 and A332204 for similar sequences.",
				"Cf. A332247 (Y-coordinates)."
			],
			"keyword": "nonn,base",
			"offset": "0,4",
			"author": "_Rémy Sigrist_, Feb 08 2020",
			"references": 4,
			"revision": 24,
			"time": "2020-02-12T16:17:43-05:00",
			"created": "2020-02-12T14:17:39-05:00"
		}
	]
}