{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002982",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2982,
			"id": "M2321",
			"data": "3,4,6,7,12,14,30,32,33,38,94,166,324,379,469,546,974,1963,3507,3610,6917,21480,34790,94550,103040,147855,208003",
			"name": "Numbers n such that n! - 1 is prime.",
			"comment": [
				"The corresponding primes n!-1 are often called factorial primes."
			],
			"reference": [
				"J.-M. De Koninck, Ces nombres qui nous fascinent, Entry 166, p. 53, Ellipses, Paris 2008.",
				"R. K. Guy, Unsolved Problems in Number Theory, Section A2.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"A. Borning, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1972-0308018-5\"\u003eSome results for k!+-1 and 2.3.5...p+-1\u003c/a\u003e, Math. Comp., 26:118 (1972), pp. 567-570.",
				"J. P. Buhler et al., \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1982-0645679-1\"\u003ePrimes of the form n!+-1 and 2.3.5....p+-1\u003c/a\u003e, Math. Comp., 38:158 (1982), pp. 639-643.",
				"Chris K. Caldwell, \u003ca href=\"http://primes.utm.edu/top20/page.php?id=30\"\u003eFactorial Primes\u003c/a\u003e",
				"C. K. Caldwell and Y. Gallot, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-01-01315-1\"\u003eOn the primality of n!+-1 and 2*3*5*...*p+-1\u003c/a\u003e, Math. Comp., 71:237 (2002), pp. 441-448.",
				"P. Carmody, \u003ca href=\"http://83.143.57.194:16384/Factorial/\"\u003eFactorial Prime Search Progress Pages\u003c/a\u003e",
				"R. K. Guy \u0026 N. J. A. Sloane, \u003ca href=\"/A005648/a005648.pdf\"\u003eCorrespondence, 1985\u003c/a\u003e",
				"H. Dubner, \u003ca href=\"/A006794/a006794.pdf\"\u003eFactorial and primorial primes\u003c/a\u003e, J. Rec. Math., 19 (No. 3, 1987), 197-203. (Annotated scanned copy)",
				"Des MacHale and Joseph Manning, \u003ca href=\"http://dx.doi.org/10.1017/mag.2015.28\"\u003eMaximal runs of strictly composite integers\u003c/a\u003e, The Mathematical Gazette, 99 (2015), pp 213-219. doi:10.1017/mag.2015.28.",
				"R. Mestrovic, \u003ca href=\"http://arxiv.org/abs/1202.3670\"\u003eEuclid's theorem on the infinitude of primes: a historical survey of its proofs (300 BC--2012) and another new proof\u003c/a\u003e, arXiv preprint arXiv:1202.3670 [math.HO], 2012.",
				"R. Ondrejka, \u003ca href=\"http://www.utm.edu/research/primes/lists/top_ten/\"\u003eThe Top Ten: a Catalogue of Primal Configurations\u003c/a\u003e",
				"PrimeGrid, \u003ca href=\"http://www.primegrid.com/forum_thread.php?id=1336\u0026amp;nowrap=true#26809\"\u003eWorld Record Factorial Prime!!!\u003c/a\u003e",
				"PrimeGrid, \u003ca href=\"http://www.primegrid.com/download/fps-94550.pdf\"\u003eAnnouncement of 94550\u003c/a\u003e, (2010) - _Felix Fröhlich_, Jul 11 2014",
				"PrimeGrid, \u003ca href=\"http://www.primegrid.com/download/fps-103040.pdf\"\u003eAnnouncement of 103040\u003c/a\u003e, (2010) - _Felix Fröhlich_, Jul 11 2014",
				"PrimeGrid, \u003ca href=\"http://www.primegrid.com/download/FPS-147855.pdf\"\u003eAnnouncement of 147855\u003c/a\u003e, (2013) - _Felix Fröhlich_, Jul 11 2014",
				"Maxie D. Schmidt, \u003ca href=\"https://arxiv.org/abs/1701.04741\"\u003eNew Congruences and Finite Difference Equations for Generalized Factorial Functions\u003c/a\u003e, arXiv:1701.04741 [math.CO], 2017.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Factorial.html\"\u003eFactorial\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/FactorialPrime.html\"\u003eFactorial Prime\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/IntegerSequencePrimes.html\"\u003eInteger Sequence Primes\u003c/a\u003e",
				"R. G. Wilson, V, \u003ca href=\"/A002982/a002982.pdf\"\u003eLetter to N. J. A. Sloane, Jan. 1989\u003c/a\u003e",
				"\u003ca href=\"/index/Fa#factorial\"\u003eIndex entries for sequences related to factorial numbers\u003c/a\u003e"
			],
			"example": [
				"From _Gus Wiseman_, Jul 04 2019: (Start)",
				"The sequence of numbers n! - 1 together with their prime indices begins:",
				"                    1: {}",
				"                    5: {3}",
				"                   23: {9}",
				"                  119: {4,7}",
				"                  719: {128}",
				"                 5039: {675}",
				"                40319: {9,273}",
				"               362879: {5,5,430}",
				"              3628799: {10,11746}",
				"             39916799: {6,7,9,992}",
				"            479001599: {25306287}",
				"           6227020799: {270,256263}",
				"          87178291199: {3610490805}",
				"        1307674367999: {7,11,11,16,114905}",
				"       20922789887999: {436,318519035}",
				"      355687428095999: {8,21,10165484947}",
				"     6402373705727999: {17,20157,25293727}",
				"   121645100408831999: {119,175195,4567455}",
				"  2432902008176639999: {11715,659539127675}",
				"(End)"
			],
			"mathematica": [
				"Select[Range[10^3], PrimeQ[ #!-1] \u0026] (* _Vladimir Joseph Stephan Orlovsky_, May 01 2008 *)"
			],
			"program": [
				"(PARI) is(n)=ispseudoprime(n!-1) \\\\ _Charles R Greathouse IV_, Mar 21 2013",
				"(MAGMA) [n: n in [0..500] | IsPrime(Factorial(n)-1)]; // _Vincenzo Librandi_, Sep 07 2017",
				"(Python)",
				"from sympy import factorial, isprime",
				"A002982_list = [n for n in range(1,10**2) if isprime(factorial(n)-1)] # _Chai Wah Wu_, Apr 04 2021"
			],
			"xref": [
				"Cf. A002981 (numbers n such that n!+1 is prime).",
				"Cf. A055490 (primes of form n!-1).",
				"Cf. A088332 (primes of form n!+1).",
				"Cf. A000142, A001221, A001222, A046051, A054991, A112798, A325272."
			],
			"keyword": "hard,more,nonn,nice",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"21480 sent in by Ken Davis (ken.davis(AT)softwareag.com), Oct 29 2001",
				"Updated Feb 26 2007 by _Max Alekseyev_, based on progress reported in the Carmody web site.",
				"Inserted missing 21480 and 34790 (see Caldwell). Added 94550, discovered Oct 05 2010. _Eric W. Weisstein_, Oct 06 2010",
				"103040 was discovered by James Winskill, Dec 14 2010. It has 471794 digits. Corrected by _Jens Kruse Andersen_, Mar 22 2011",
				"a(26) = 147855 from _Felix Fröhlich_, Sep 02 2013",
				"a(27) = 208003 from _Sou Fukui_, Jul 27 2016"
			],
			"references": 87,
			"revision": 132,
			"time": "2021-04-04T21:49:52-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}