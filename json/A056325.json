{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A056325",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 56325,
			"data": "1,1,2,4,11,32,117,467,2135,10480,55091,301633,1704115,9819216,57365191,338134521,2005134639,11937364184,71254895955,426063226937,2550552314219,15280103807200,91588104196415,549159428968825",
			"name": "Number of reversible string structures with n beads using a maximum of six different colors.",
			"comment": [
				"A string and its reverse are considered to be equivalent. Permuting the colors will not change the structure. Thus aabc, cbaa and bbac are all considered to be identical.",
				"Number of set partitions of an unoriented row of n elements with six or fewer nonempty subsets. - _Robert A. Russell_, Oct 28 2018",
				"There are nonrecursive formulas, generating functions, and computer programs for A056273 and A305752, which can be used in conjunction with the first formula. - _Robert A. Russell_, Oct 28 2018"
			],
			"reference": [
				"M. R. Nester (1999). Mathematical investigations of some plant interaction designs. PhD Thesis. University of Queensland, Brisbane, Australia. [See A056391 for pdf file of Chap. 2]"
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A056325/b056325.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_11\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (16,-84,84,685,-2140,180,7200,-8244,-4176,11664,-5184)."
			],
			"formula": [
				"Use de Bruijn's generalization of Polya's enumeration theorem as discussed in reference.",
				"From _Robert A. Russell_, Oct 28 2018: (Start)",
				"a(n) = (A056273(n) + A305752(n)) / 2.",
				"a(n) = A056273(n) - A320936(n) = A320936(n) + A305752(n).",
				"a(n) = Sum_{j=0..k} (S2(n,j) + Ach(n,j)) / 2, where k=6 is the maximum number of colors, S2 is the Stirling subset number A008277, and Ach(n,k) = [n\u003e=0 \u0026 n\u003c2 \u0026 n==k] + [n\u003e1]*(k*Ach(n-2,k) + Ach(n-2,k-1) + Ach(n-2,k-2)).",
				"a(n) = A000007(n) + A057427(n) + A056326(n) + A056327(n) + A056328(n) + A056329(n) + A056330(n).",
				"(End)",
				"From _Colin Barker_, Mar 24 2020: (Start)",
				"G.f.: (1 - 15*x + 70*x^2 - 28*x^3 - 654*x^4 + 1479*x^5 + 783*x^6 - 5481*x^7 + 3512*x^8 + 4640*x^9 - 5922*x^10 + 1530*x^11) / ((1 - x)*(1 - 2*x)*(1 - 3*x)*(1 - 4*x)*(1 - 6*x)*(1 - 2*x^2)*(1 - 3*x^2)*(1 - 6*x^2)).",
				"a(n) = 16*a(n-1) - 84*a(n-2) + 84*a(n-3) + 685*a(n-4) - 2140*a(n-5) + 180*a(n-6) + 7200*a(n-7) - 8244*a(n-8) - 4176*a(n-9) + 11664*a(n-10) - 5184*a(n-11) for n\u003e11.",
				"(End)"
			],
			"example": [
				"For a(4)=11, the 7 achiral patterns are AAAA, AABB, ABAB, ABBA, ABCA, ABBC, and ABCD.  The 4 chiral pairs are AAAB-ABBB, AABA-ABAA, AABC-ABCC, and ABAC-ABCB."
			],
			"mathematica": [
				"Ach[n_, k_] := Ach[n, k] = If[n\u003c2, Boole[n==k \u0026\u0026 n\u003e=0], k Ach[n-2,k] + Ach[n-2,k-1] + Ach[n-2,k-2]] (* A304972 *)",
				"k=6; Table[Sum[StirlingS2[n,j]+Ach[n,j],{j,0,k}]/2,{n,0,40}] (* _Robert A. Russell_, Oct 28 2018 *)",
				"LinearRecurrence[{16, -84, 84, 685, -2140, 180, 7200, -8244, -4176, 11664, -5184}, {1, 1, 2, 4, 11, 32, 117, 467, 2135, 10480, 55091, 301633}, 40] (* _Robert A. Russell_, Oct 28 2018 *)"
			],
			"program": [
				"(PARI) Vec((1 - 15*x + 70*x^2 - 28*x^3 - 654*x^4 + 1479*x^5 + 783*x^6 - 5481*x^7 + 3512*x^8 + 4640*x^9 - 5922*x^10 + 1530*x^11) / ((1 - x)*(1 - 2*x)*(1 - 3*x)*(1 - 4*x)*(1 - 6*x)*(1 - 2*x^2)*(1 - 3*x^2)*(1 - 6*x^2)) + O(x^30)) \\\\ _Colin Barker_, Apr 15 2020"
			],
			"xref": [
				"Cf. A056308.",
				"Column 6 of A320750.",
				"Cf. A056273 (oriented), A320936 (chiral), A305752 (achiral)."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Marks R. Nester_",
			"ext": [
				"Another term from _Robert A. Russell_, Oct 29 2018",
				"a(0)=1 prepended by _Robert A. Russell_, Nov 09 2018"
			],
			"references": 10,
			"revision": 31,
			"time": "2020-04-16T01:24:36-04:00",
			"created": "2000-07-22T03:00:00-04:00"
		}
	]
}