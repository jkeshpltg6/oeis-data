{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A256068",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 256068,
			"data": "1,0,1,0,1,3,0,2,14,16,0,3,60,174,125,0,6,254,1434,2464,1296,0,12,1087,10746,33362,40455,16807,0,25,4742,77556,388312,816535,763104,262144,0,52,21020,551460,4191916,13617210,21501684,16328620,4782969",
			"name": "Number T(n,k) of rooted identity trees with n nodes and colored non-root nodes using exactly k colors; triangle T(n,k), n\u003e=1, 0\u003c=k\u003c=n-1, read by rows.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A256068/b256068.txt\"\u003eRows n = 1..141, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = Sum_{i=0..k} (-1)^i * C(k,i) * A255517(n)."
			],
			"example": [
				"T(4,2) = 14:",
				":   0   0   0   0   0   0     0       0",
				":   |   |   |   |   |   |     |       |",
				":   1   1   2   2   2   1     1       2",
				":   |   |   |   |   |   |    / \\     / \\",
				":   1   2   1   2   1   2   1   2   1   2",
				":   |   |   |   |   |   |",
				":   2   1   1   1   2   1",
				":",
				":     0      0      0      0      0      0",
				":    / \\    / \\    / \\    / \\    / \\    / \\",
				":   1   1  2   1  1   2  2   2  1   2  2   1",
				":   |      |      |      |      |      |",
				":   2      1      1      1      2      2",
				"Triangle T(n,k) begins:",
				"  1;",
				"  0,  1;",
				"  0,  1,    3;",
				"  0,  2,   14,    16;",
				"  0,  3,   60,   174,    125;",
				"  0,  6,  254,  1434,   2464,   1296;",
				"  0, 12, 1087, 10746,  33362,  40455,  16807;",
				"  0, 25, 4742, 77556, 388312, 816535, 763104, 262144;",
				"  ..."
			],
			"maple": [
				"with(numtheory):",
				"A:= proc(n, k) option remember; `if`(n\u003c2, n, add(A(n-j, k)*add(",
				"      k*A(d, k)*d*(-1)^(j/d+1), d=divisors(j)), j=1..n-1)/(n-1))",
				"    end:",
				"T:= (n, k)-\u003e add(A(n, k-i)*(-1)^i*binomial(k, i), i=0..k):",
				"seq(seq(T(n, k), k=0..n-1), n=1..10);"
			],
			"mathematica": [
				"A[n_, k_] := A[n, k] = If[n \u003c 2, n, Sum[A[n - j, k] Sum[k A[d, k] d * (-1)^(j/d + 1), {d, Divisors[j]}], {j, 1, n - 1}]/(n - 1)];",
				"T[n_, k_] := Sum[A[n, k - i] (-1)^i Binomial[k, i], {i, 0, k}];",
				"Table[T[n, k], {n, 10}, {k, 0, n - 1}] // Flatten (* _Jean-François Alcover_, May 29 2020, after Maple *)"
			],
			"xref": [
				"Columns k=0-1 give: A063524 (for n\u003e0), A004111 (for n\u003e1):",
				"Main diagonal gives: A000272 (for n\u003e0).",
				"Row sums give A319220(n-1).",
				"T(2n+1,n) gives A309996.",
				"Cf. A255517, A256064."
			],
			"keyword": "nonn,tabl",
			"offset": "1,6",
			"author": "_Alois P. Heinz_, Mar 13 2015",
			"references": 7,
			"revision": 18,
			"time": "2021-01-04T16:54:06-05:00",
			"created": "2015-03-13T21:02:17-04:00"
		}
	]
}