{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A230210",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 230210,
			"data": "-1,7,-21,35,-35,21,-7,1,-1,6,-14,14,0,-14,14,-6,1,-1,5,-8,0,14,-14,0,8,-5,1,-1,4,-3,-8,14,0,-14,8,3,-4,1,-1,3,1,-11,6,14,-14,-6,11,-1,-3,1,-1,2,4,-10,-5,20,0,-20,5,10,-4,-2,1,-1,1,6,-6,-15",
			"name": "Trapezoid of dot products of row 7 (signs alternating) with sequential 8-tuples read by rows in Pascal's triangle A007318: T(n,k) is the linear combination of the 8-tuples (C(7,0), -C(7,1), ..., C(7,6), -C(7,7)) and (C(n-1,k-7), C(n-1,k-6), ..., C(n-1,k)), n \u003e= 1, 0 \u003c= k \u003c= n+6.",
			"comment": [
				"The array is trapezoidal rather than triangular because C(n,k) is not uniquely defined for all negative n and negative k.",
				"Row sums are 0.",
				"Coefficients of (x-1)^7 (x+1)^(n-1), n \u003e 0."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A230210/b230210.txt\"\u003eRows n=1..50 of trapezoid, flattened\u003c/a\u003e",
				"Isabel Cação, Helmuth R. Malonek, Maria Irene Falcão, Graça Tomaz, \u003ca href=\"https://www.emis.de/journals/JIS/VOL21/Falcao/falcao2.html\"\u003eCombinatorial Identities Associated with a Multidimensional Polynomial Sequence\u003c/a\u003e, J. Int. Seq., Vol. 21 (2018), Article 18.7.4."
			],
			"formula": [
				"T(n,k) = Sum_{i=0..n+m-1} (-1)^(i+m)*C(m,i)*C(n-1,k-i), n \u003e= 1, with T(n,0) = (-1)^m and m=7."
			],
			"example": [
				"Trapezoid begins:",
				"  -1, 7, -21,  35, -35,  21,  -7,   1;",
				"  -1, 6, -14,  14,   0, -14,  14,  -6,   1;",
				"  -1, 5,  -8,   0,  14, -14,   0,   8,  -5,  1;",
				"  -1, 4,  -3,  -8,  14,   0, -14,   8,   3, -4,  1;",
				"  -1, 3,   1, -11,   6,  14, -14,  -6,  11, -1, -3,  1;",
				"  -1, 2,   4, -10,  -5,  20,   0, -20,   5, 10, -4, -2,  1;",
				"  -1, 1,   6,  -6, -15,  15,  20, -20, -15, 15,  6, -6, -1, 1;",
				"etc."
			],
			"mathematica": [
				"Flatten[Table[CoefficientList[(x - 1)^7 (x + 1)^n, x], {n, 0, 7}]] (* _T. D. Noe_, Oct 25 2013 *)",
				"m=7; Table[If[k == 0, (-1)^m, Sum[(-1)^(j+m)*Binomial[m, j]*Binomial[n-1, k-j], {j, 0, n+m-1}]], {n, 1, 10}, {k, 0, n+m-1}]//Flatten (* _G. C. Greubel_, Nov 28 2018 *)"
			],
			"program": [
				"(PARI) m=7; for(n=1, 10, for(k=0, n+m-1, print1(if(k==0, (-1)^m, sum(j=0, n+m-1, (-1)^(j+m)*binomial(m,j)*binomial(n-1,k-j))), \", \"))) \\\\ _G. C. Greubel_, Nov 28 2018",
				"(MAGMA) m:=7; [[k le 0 select (-1 )^m else (\u0026+[(-1)^(j+m)* Binomial(m,j) *Binomial(n-1,k-j): j in [0..(n+m-1)]]): k in [0..(n+m-1)]]: n in [1..10]]; // _G. C. Greubel_, Nov 28 2018",
				"(Sage) m=7; [[sum((-1)^(j+m)*binomial(m,j)*binomial(n-1,k-j) for j in range(n+m)) for k in range(n+m)] for n in (1..10)] # _G. C. Greubel_, Nov 28 2018"
			],
			"xref": [
				"Using row j of the alternating Pascal triangle as generator: A007318 (j=0), A008482 and A112467 (j=1 after the first term in each), A182533 (j=2 after the first two rows), A230206-A230209 (j=3 to j=6), A230211-A230212 (j=8 and j=9)."
			],
			"keyword": "easy,sign,tabf",
			"offset": "1,2",
			"author": "_Dixon J. Jones_, Oct 12 2013",
			"references": 3,
			"revision": 28,
			"time": "2018-11-29T01:24:27-05:00",
			"created": "2013-10-20T23:13:32-04:00"
		}
	]
}