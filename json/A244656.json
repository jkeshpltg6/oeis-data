{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A244656",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 244656,
			"data": "2,2,6,12,60,60,420,840,2520,2520,55440,55440,360360,360360,360360,2162160,85765680,85765680,33522128640,33522128640,33522128640,33522128640,19275223968000,19275223968000,19275223968000",
			"name": "Least product of consecutive positive integers which is divisible by each of 1, 2, ..., n.",
			"comment": [
				"For n \u003e 1, clearly a(n) is bounded below by lcm(1,2,...,n) and bounded above by n!. Further, a(n) is a positive multiple of lcm(1,2,...,n). Any product of two or more consecutive positive integers may be expressed as m!/k!, where 0 \u003c= k \u003c= m-2. For this sequence, the m corresponding to a(n) may or may not be a multiple of n. Whenever a(n) can be expressed as the product of exactly two consecutive integers, it is a term of A002378. See the a-file link for further comments."
			],
			"link": [
				"Rick L. Shepherd, \u003ca href=\"/A244656/b244656.txt\"\u003eTable of n, a(n) for n = 1..36\u003c/a\u003e",
				"Rick L. Shepherd, \u003ca href=\"/A244656/a244656.txt\"\u003eSample program output and calculation notes\u003c/a\u003e"
			],
			"example": [
				"a(7) = 20*21 = 21!/19! = 420 because 420 is divisible by 1, 2, 3, 4, 5, 6, and 7, and no positive integer less than 420 is divisible by each of these. Here, 420 = lcm(1,2,3,4,5,6,7). 420 is an oblong (or promic) number (A002378).",
				"a(11) = 7*8*9*10*11 = 11!/6! = 55440. Here, 27720 = lcm(1,2,3,4,5,6,7,8,9,10,11), but 27720 cannot be represented as a product of consecutive positive integers.",
				"a(31) = 6081487775*6081487776 = 36984493563555938400, also a promic number."
			],
			"program": [
				"(PARI)",
				"{a(n) =",
				"local(L, M, i, k = 0, s = 0, ret = 0, d, divs2,",
				"   st, pr, prt = 1); /* Use prt = 0 to suppress printing. */",
				"if(n \u003c 1, return, if(n \u003c 3, ret = 2,",
				"L = lcm(vector(n, i, i));",
				"M = n!/L;",
				"while(k \u003c M,",
				"   k++;",
				"   s += L; d = divisors(s); divs2 = #d \\ 2;",
				"   st = 2; pr = d[st];",
				"   i = 0;",
				"   while(st + i \u003c= divs2,",
				"      if(d[st + i + 1] == d[st + i] + 1,",
				"         pr *= d[st + i + 1]; i++;",
				"         if(pr == s,",
				"            if(prt,",
				"               print1(\"k*L = \", k, \"*\", L, \" = \",",
				"                 s, \" = \", d[st], \"*\");",
				"               if(d[st + i] \u003e d[st] + 2, print1(\"...*\"),",
				"                 if(d[st + i] == d[st] + 2,",
				"                   print1(d[st] + 1, \"*\")));",
				"               print(d[st + i], \" = \", d[st + i], \"!/\",",
				"                 d[st] - 1, \"!\"));",
				"            ret = s; break(2),",
				"            if(pr \u003e s, st++; pr = d[st]; i = 0)),",
				"         if(pr \u003c s, st += i + 1, st++); pr = d[st]; i = 0)))));",
				"return(ret)}"
			],
			"xref": [
				"Cf. A003418, A000142, A025527, A002378."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Rick L. Shepherd_, Jul 03 2014, Sep 14 2014",
			"references": 2,
			"revision": 14,
			"time": "2018-03-13T04:08:23-04:00",
			"created": "2014-10-15T09:38:09-04:00"
		}
	]
}