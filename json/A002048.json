{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002048",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2048,
			"id": "M0972 N0363",
			"data": "1,2,4,5,8,10,14,15,16,21,22,25,26,28,33,34,35,36,38,40,42,46,48,49,50,53,57,60,62,64,65,70,77,80,81,83,85,86,90,91,92,100,104,107,108,116,119,124,127,132,133,137,141,144,145,148,150,151,154,158,159,163,165",
			"name": "Segmented numbers, or prime numbers of measurement.",
			"comment": [
				"The segmented numbers are the positive integers excluding those equal to the sum of two or more consecutive smaller terms. The prime numbers of measurement are their partial sums, cf. A002049. - _M. F. Hasler_, Jun 26 2019",
				"Without the requirement that the smaller terms be consecutive, the sequence becomes the sequence of powers of 2 (A000079). - _Alonso del Arte_, Jan 25 2020"
			],
			"reference": [
				"R. K. Guy, Unsolved Problems in Number Theory, E30.",
				"Š. Porubský, On MacMahon's segmented numbers and related sequences. Nieuw Arch. Wisk. (3) 25 (1977), no. 3, 403--408. MR0485763 (58 #5575)",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"R. J. Mathar, \u003ca href=\"/A002048/b002048.txt\"\u003eTable of n, a(n) for n = 1..7836\u003c/a\u003e",
				"G. E. Andrews, \u003ca href=\"http://www.jstor.org/stable/2318498\"\u003eMacMahon's prime numbers of measurement\u003c/a\u003e, Amer. Math. Monthly, 82 (1975), 922-923.",
				"R. L. Graham and C. B. A. Peck, \u003ca href=\"http://www.jstor.org/stable/2315138\"\u003eProblem E1910\u003c/a\u003e, Amer. Math. Monthly, 75 (1968), 80-81.",
				"R. K. Guy, \u003ca href=\"/A002048/a002048.pdf\"\u003eLetter to G. E. Andrews, Apr 14 1975\u003c/a\u003e",
				"P. A. MacMahon, \u003ca href=\"https://www.biodiversitylibrary.org/item/88477#page/679/mode/1up\"\u003eThe prime numbers of measurement on a scale\u003c/a\u003e, Proc. Camb. Phil. Soc. 21 (1923), 651-654; reprinted in Coll. Papers I, pp. 797-800.",
				"Samuel B. Reid, \u003ca href=\"/A002048/a002048_1.c.txt\"\u003eC program for A002048\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PrimeNumberofMeasurement.html\"\u003ePrime Number of Measurement.\u003c/a\u003e"
			],
			"formula": [
				"Andrews conjectures that lim_{n -\u003e oo} n log n / (a(n) loglog n) = 1. - _N. J. A. Sloane_, Dec 01 2013"
			],
			"example": [
				"Although 5 is the sum of the terms 1 and 4, those prior terms are not consecutive, and therefore 5 is in the sequence.",
				"6 is not in the sequence because it is the sum of consecutive prior terms 2 and 4.",
				"7 is not in the sequence either because it is also the sum of consecutive prior terms, in this case 1, 2, 4.",
				"8 is in the sequence because no sum whatsoever of distinct prior terms adds up to 8."
			],
			"maple": [
				"A002048 := proc(anmax::integer,printlist::boolean)",
				"local a, asum,su,i,piv,j;",
				"a := [];",
				"for i from 1 to anmax do",
				"a := [op(a),i];",
				"od:",
				"if printlist then",
				"printf(\"%d %d\\n\",1,a[1]);",
				"printf(\"%d %d\\n\",2,a[2]);",
				"fi;",
				"asum := [a[1]+a[2],a[2]];",
				"for i from 3 to anmax do",
				"asum := [op(asum),0];",
				"od:",
				"piv := 3;",
				"while piv \u003c= nops(a) do",
				"for i from 1 to piv-2 do",
				"a := remove(has,a, asum[i]);",
				"od:",
				"if printlist then",
				"printf(\"%a %a\\n\",piv,a[piv]);",
				"fi;",
				"for i from 1 to piv do",
				"asum := subsop(i=asum[i]+a[piv], asum);",
				"od:",
				"piv := piv+1;",
				"od;",
				"RETURN(a);",
				"end:",
				"A002048(40000,true);",
				"# _R. J. Mathar_, Jun 04 2006"
			],
			"mathematica": [
				"A002048[anmax_] := (a = {}; Do[AppendTo[a, i], {i, anmax}]; asum = {a[[1]] + a[[2]], a[[2]]}; Do[AppendTo[asum, 0], {i, 3, anmax}]; piv = 3; While[piv \u003c= Length[a], Do[a = DeleteCases[a, asum[[i]]], {i, 1, piv - 2}]; Do[asum[[i]] += a[[piv]], {i, piv}]; piv = piv + 1;]; a); A002048[63] (* _Jean-François Alcover_, Jul 28 2011, converted from _R. J. Mathar_'s Maple prog. *)",
				"searchMax = 200; segmNums = {1}; curr = 2; While[curr \u003c searchMax, If[Not[MemberQ[Apply[Plus, Subsequences[segmNums], 1], curr]], AppendTo[segmNums, curr], ];  curr = curr + 1]; segmNums (* _Alonso del Arte_, Jan 25 2020 *)"
			],
			"program": [
				"(C++)",
				"#include \u003ciostream\u003e",
				"#include \u003cvector\u003e",
				"#include \u003calgorithm\u003e",
				"#define NMAX 400",
				"using namespace std;",
				"int main(int argc, char *argv[])",
				"{ vector\u003cint\u003e a; for(int i = 0; i \u003c NMAX; i++) a.push_back(i+1); for(int piv = 2; piv \u003c a.size(); piv++) for(int i = 0; i \u003c piv - 1 \u0026\u0026 i \u003c a.size() - 1; i++) { int su = a[i] + a[i + 1]; remove(a.begin(), a.end(), su); for(int j = i + 2; j \u003c piv \u0026\u0026 j \u003c a.size(); j++) { su += a[j]; remove(a.begin(), a.end(), su); if(su \u003e NMAX) break; } } for(int i = 0; i \u003c a.size() \u0026\u0026 a[i] \u003c NMAX; i++) cout \u003c\u003c a[i] \u003c\u003c \",\"; return 0;",
				"} /* _R. J. Mathar_, May 31 2006 */",
				"(Haskell)",
				"import Data.List ((\\\\))",
				"a002048 n = a002048_list !! (n-1)",
				"a002048_list = f [1..] [] where",
				"   f (x:xs) ys = x : f (xs \\\\ scanl (+) x ys) (x : ys)",
				"-- _Reinhard Zumkeller_, May 23 2013",
				"(C) // See Links section for C program by _Samuel B. Reid_, Jan 26 2020"
			],
			"xref": [
				"Cf. A002049 (partial sums), A004978, A005242, A033627."
			],
			"keyword": "nonn,nice",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _R. J. Mathar_, May 31 2006"
			],
			"references": 11,
			"revision": 74,
			"time": "2020-01-28T01:35:19-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}