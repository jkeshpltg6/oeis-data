{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A258170",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 258170,
			"data": "0,0,1,0,2,1,0,3,3,1,0,4,8,6,1,0,5,15,25,10,1,0,6,36,91,65,15,1,0,7,63,301,350,140,21,1,0,8,136,972,1702,1050,266,28,1,0,9,261,3027,7770,6951,2646,462,36,1,0,10,530,9355,34115,42526,22827,5880,750,45,1",
			"name": "T(n,k) = (1/k!) * Sum_{i=0..k} (-1)^(k-i) * C(k,i) * A185651(n,i); triangle T(n,k), n \u003e= 0, 0 \u003c= k \u003c= n, read by rows.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A258170/b258170.txt\"\u003eRows n = 0..140, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = 1/k! * Sum_{i=0..k} (-1)^(k-i) * C(k,i) * A185651(n,i).",
				"From _Petros Hadjicostas_, Sep 07 2018: (Start)",
				"Conjecture 1: T(n,k) = Stirling2(n,k) for k \u003e= 1 and k \u003c= n \u003c= 2*k - 1.",
				"Conjecture 2: T(n,k) = Stirling2(n,k) for k \u003e= 2 and n prime \u003e= 2.",
				"Here, Stirling2(n,k) = A008277(n,k).",
				"(End)"
			],
			"example": [
				"Triangle T(n,k) begins:",
				"  0;",
				"  0,  1;",
				"  0,  2,   1;",
				"  0,  3,   3,    1;",
				"  0,  4,   8,    6,     1;",
				"  0,  5,  15,   25,    10,     1;",
				"  0,  6,  36,   91,    65,    15,     1;",
				"  0,  7,  63,  301,   350,   140,    21,    1;",
				"  0,  8, 136,  972,  1702,  1050,   266,   28,   1;",
				"  0,  9, 261, 3027,  7770,  6951,  2646,  462,  36,  1;",
				"  0, 10, 530, 9355, 34115, 42526, 22827, 5880, 750, 45, 1;"
			],
			"maple": [
				"with(numtheory):",
				"A:= proc(n, k) option remember;",
				"      add(phi(d)*k^(n/d), d=divisors(n))",
				"    end:",
				"T:= (n, k)-\u003e add((-1)^(k-i)*binomial(k, i)*A(n, i), i=0..k)/k!:",
				"seq(seq(T(n, k), k=0..n), n=0..12);"
			],
			"mathematica": [
				"A[n_, k_] := A[n, k] = DivisorSum[n, EulerPhi[#]*k^(n/#)\u0026];",
				"T[n_, k_] := Sum[(-1)^(k-i)*Binomial[k, i]*A[n, i], {i, 0, k}]/k!; T[0, 0] = 0;",
				"Table[T[n, k], {n, 0, 12}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Mar 25 2017, translated from Maple *)"
			],
			"program": [
				"(Sage) # uses[DivisorTriangle from A327029]",
				"DivisorTriangle(euler_phi, stirling_number2, 10) # _Peter Luschny_, Aug 24 2019"
			],
			"xref": [
				"Columns k=0-1 give: A000004, A000027.",
				"Row sums give A258171.",
				"Main diagonal gives A057427.",
				"T(2*n+1,n+1) gives A129506(n+1).",
				"Cf. A008277, A185651, A327029."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Alois P. Heinz_, May 22 2015",
			"references": 4,
			"revision": 37,
			"time": "2020-03-24T12:37:37-04:00",
			"created": "2015-05-22T15:02:55-04:00"
		}
	]
}