{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A193230",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 193230,
			"data": "1,11,60,30,15,74,37,168,84,42,21,104,52,26,13,72,36,18,9,50,25,122,61,272,136,68,34,17,88,44,22,11,60,30,15,74,37,168,84,42,21,104,52,26,13,72,36,18,9,50,25,122,61,272,136,68,34,17,88,44,22,11,60,30,15,74,37,168,84,42,21,104,52,26,13,72,36,18,9,50,25,122,61,272,136,68,34,17,88,44,22,11,60,30,15,74,37,168,84,42,21,104",
			"name": "Start with 1; if even, divide by 2; if odd, add the next three primes.",
			"comment": [
				"Trajectory of 1 under the map x -\u003e A174221(x).",
				"Periodic with period of length 30, starting at a(2) = 11.",
				"Angelini conjectures that the orbit under A174221 becomes periodic for any initial value. He calls this the PrimeLatz conjecture, as tribute to L. Collatz, known for the 3n+1 conjecture.",
				"It has been checked that the loop (11, ..., 22) (or (9, ..., 18), to start with the smallest element) is the only loop (except for the fixed point 0) at least up to values not exceeding 10^8, and the orbit of every positive integer \u003c= 10^4 does end in this loop. - _M. F. Hasler_, Oct 25 2017",
				"It might have been more natural to start this sequence with offset 0. Since a(n) = a(n+30) from n = 2 on, this sequence consists essentially (except for the initial term) of the apparently unique \"loop\" of the \"PrimeLatz\" map A174221. It is used as such in related sequences A293978, ... - _M. F. Hasler_, Oct 31 2017"
			],
			"link": [
				"Eric Angelini, \u003ca href=\"http://www.cetteadressecomportecinquantesignes.com/PrimeLatz.htm\"\u003eThe PrimeLatz conjecture\u003c/a\u003e",
				"E. Angelini, \u003ca href=\"/A174221/a174221.pdf\"\u003eThe PrimeLatz Conjecture\u003c/a\u003e [Cached copy, with permission]",
				"\u003ca href=\"/index/Rec#order_30\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1)."
			],
			"example": [
				"1 is odd;  we add to 1 the next 3 primes (2,3,5) and get 11",
				"11 is odd;  we get 11+(13+17+19)=60",
				"60 is even; we get 30",
				"30 is even; we get 15",
				"15 is odd;  we get 15+(17+19+23)=74",
				"74 is even; we get 37",
				"37 is odd;  we get 37+(41+43+47)=168",
				"168 is even; we get 84",
				"84 is even; we get 42",
				"42 is even; we get 21",
				"21 is odd;  we get 21+(23+29+31)=104",
				"104 is even; we get 52",
				"52 is even; we get 26",
				"26 is even; we get 13",
				"13 is odd;  we get 13+(17+19+23)=72",
				"72 is even; we get 36",
				"36 is even; we get 18",
				"18 is even; we get 9",
				"9 is odd;  we get 9+(11+13+17)=50",
				"50 is even; we get 25",
				"25 is odd;  we get 25+(29+31+37)=122",
				"122 is even; we get 61",
				"61 is odd;  we get 61+(67+71+73)=272",
				"272 is even; we get 136",
				"136 is even; we get 68",
				"68 is even; we get 34",
				"34 is even; we get 17",
				"17 is odd;  we get 17+(19+23+29)=88",
				"88 is even; we get 44",
				"44 is even; we get 22",
				"22 is even; we get 11... thus entering in a loop.",
				"...",
				"(from Angelini's web page)"
			],
			"mathematica": [
				"NestList[If[EvenQ@ #, #/2, Total@ Prepend[NextPrime[#, {1, 2, 3}], #]] \u0026, 1, 101] (* _Michael De Vlieger_, Oct 25 2017 *)"
			],
			"program": [
				"(PARI) vector(100,i,t=if(i\u003e1,A174221(t),1)) \\\\ _M. F. Hasler_, Oct 25 2017"
			],
			"xref": [
				"Cf. A174221, A293980, A293979 (orbit of 83), A293978 (orbit of 443), A293981 (orbit of 209)."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Jul 18 2011",
			"references": 6,
			"revision": 21,
			"time": "2017-10-31T20:34:46-04:00",
			"created": "2011-07-18T18:02:19-04:00"
		}
	]
}