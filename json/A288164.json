{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A288164",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 288164,
			"data": "1,2,2310,1155,3,4,770,1365,6,8,385,1785,12,10,455,231,18,20,595,273,22,30,105,77,26,60,165,91,14,66,195,35,28,78,255,55,38,42,210,65,11,84,390,85,7,114,330,70,13,33,420,130,17,21,462,110,5,39,546,140",
			"name": "Lexicographically earliest sequence of distinct positive terms such that, for any n \u003e 0, a(n)*a(n+2) has at least 5 distinct prime factors.",
			"comment": [
				"This sequence is a permutation of the natural numbers, with inverse A288799.",
				"Conjecturally, a(n) ~ n.",
				"For k \u003e= 0, let f_k be the lexicographically earliest sequence of distinct positive terms such that, for any n \u003e 0, a(n)*a(n+k) has at least 5 distinct prime factors.",
				"In particular, we have:",
				"- f_0 = the numbers with at least 5 distinct prime factors,",
				"- f_1 = A285487,",
				"- f_2 = a (this sequence),",
				"- f_3 = A288171.",
				"If k \u003e 0, then:",
				"- f_k is a permutation of the natural numbers,",
				"- f_k(i) = i for any i \u003c= k,",
				"- f_k(k+1) = A002110(5),",
				"- conjecturally, f_k(n) ~ n."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A288164/b288164.txt\"\u003eTable of n, a(n) for n = 1..25000\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A288164/a288164.txt\"\u003eC++ program for A288164\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"example": [
				"The first terms, alongside the primes p dividing a(n)*a(n+2), are:",
				"n       a(n)    p",
				"--      ----    --------------",
				"1       1       2, 3, 5, 7, 11",
				"2       2       2, 3, 5, 7, 11",
				"3       2310    2, 3, 5, 7, 11",
				"4       1155    2, 3, 5, 7, 11",
				"5       3       2, 3, 5, 7, 11",
				"6       4       2, 3, 5, 7,     13",
				"7       770     2, 3, 5, 7, 11",
				"8       1365    2, 3, 5, 7,     13",
				"9       6       2, 3, 5, 7, 11",
				"10      8       2, 3, 5, 7,         17",
				"11      385     2, 3, 5, 7, 11",
				"12      1785    2, 3, 5, 7,         17",
				"13      12      2, 3, 5, 7,     13",
				"14      10      2, 3, 5, 7, 11",
				"15      455     2, 3, 5, 7,     13",
				"16      231     2, 3, 5, 7, 11",
				"17      18      2, 3, 5, 7,         17",
				"18      20      2, 3, 5, 7,     13",
				"19      595     2,    5, 7, 11,     17",
				"20      273     2, 3, 5, 7,     13",
				"21      22      2, 3, 5, 7, 11",
				"22      30      2, 3, 5, 7, 11",
				"23      105     2, 3, 5, 7,     13"
			],
			"xref": [
				"Cf. A002110, A285487, A288171, A288799 (inverse)."
			],
			"keyword": "nonn,look",
			"offset": "1,2",
			"author": "_Rémy Sigrist_, Jun 16 2017",
			"references": 3,
			"revision": 17,
			"time": "2017-06-16T22:21:53-04:00",
			"created": "2017-06-16T22:21:53-04:00"
		}
	]
}