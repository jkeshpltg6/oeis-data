{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A217905",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 217905,
			"data": "1,-1,-2,-14,-184,-3532,-89256,-2800016,-104967808,-4578528464,-227816059360,-12735645181536,-790296855912576,-53905019035510528,-4008716449677965312,-322807879692969879552,-27983800239966141382656,-2598368754552749176202496,-257284990746988090769530368",
			"name": "O.g.f.: Sum_{n\u003e=0} -n^n*(n-1)^(n-1) * exp(-n*(n-1)*x) * x^n / n!.",
			"comment": [
				"Compare the g.f. to the LambertW identity:",
				"1 = Sum_{n\u003e=0} -(n-1)^(n-1) * exp(-(n-1)*x) * x^n/n!."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A217905/b217905.txt\"\u003eTable of n, a(n) for n = 0..345\u003c/a\u003e"
			],
			"formula": [
				"a(n) = -A191236(n-1) for n\u003e=1. [corrected by _Vaclav Kotesovec_, Aug 22 2018]",
				"a(n) = 1/n! * Sum_{k=0..n} -(-1)^(n-k)*binomial(n,k) * k^n * (k-1)^(n-1) for n\u003e=0.",
				"a(n) = 1/n! * [x^n] Sum_{k\u003e=0} -k^k*(k-1)^(k-1)*x^k / (1 + k*(k-1)*x)^(k+1).",
				"a(n) = [x^n] 1 - x*(1-x)^(n-1) / Product_{k=1..n} (1-k*x).",
				"a(n) = [x^n] 1 - x*(1+x)^(n-1) / Product_{k=1..n} (1-(k-1)*x).",
				"a(n) ~ -2^(n-1) * exp(n*(r-1)-r) * n^(n - 3/2) / (sqrt(Pi*(r-1)*(2-r)) * r^(n-1)), where r = 2 + LambertW(-2*exp(-2)) = A256500 = 1.5936242600400400923230418... - _Vaclav Kotesovec_, Aug 22 2018"
			],
			"example": [
				"O.g.f.: A(x) = 1 - x - 2*x^2 - 14*x^3 - 184*x^4 - 3532*x^5 - 89256*x^6 +...",
				"where",
				"A(x) = 1 - 1^1*0^0*x*exp(-1*0*x) - 2^2*1^1*exp(-2*1*x)*x^2/2! - 3^3*2^2*exp(-3*2*x)*x^3/3! - 4^4*3^3*exp(-4*3*x)*x^4/4! - 5^5*4^4*exp(-5*4*x)*x^5/5! +...",
				"simplifies to a power series in x with integer coefficients."
			],
			"mathematica": [
				"Join[{1, -1}, Table[(1/n!)*Sum[(-1)^(n - k + 1)*Binomial[n, k]*k^n*(k - 1)^(n - 1), {k, 0, n}], {n, 2, 50}]] (* _G. C. Greubel_, Nov 16 2017 *)"
			],
			"program": [
				"(PARI) {a(n)=polcoeff(sum(m=0,n,-m^m*(m-1)^(m-1)*x^m*exp(-m*(m-1)*x+x*O(x^n))/m!),n)}",
				"(PARI) {a(n)=(1/n!)*polcoeff(sum(k=0, n, -k^k*(k-1)^(k-1)*x^k/(1+k*(k-1)*x +x*O(x^n))^(k+1)), n)}",
				"(PARI) {a(n)=1/n!*sum(k=0,n, -(-1)^(n-k)*binomial(n,k)*k^n*(k-1)^(n-1))}",
				"(PARI) {a(n)=polcoeff(1-x*(1-x)^(n-1)/prod(k=0, n, 1-k*x +x*O(x^n)), n)}",
				"(PARI) {a(n)=polcoeff(1-x*(1+x)^n/prod(k=0, n, 1-(k-1)*x +x*O(x^n)), n)}",
				"for(n=0,30,print1(a(n),\", \"))"
			],
			"xref": [
				"Cf. A191236, A217906, A217899, A217900, A217901, A217902, A217903, A217904."
			],
			"keyword": "sign",
			"offset": "0,3",
			"author": "_Paul D. Hanna_, Oct 14 2012",
			"references": 11,
			"revision": 24,
			"time": "2018-08-22T03:08:49-04:00",
			"created": "2012-10-14T21:38:24-04:00"
		}
	]
}