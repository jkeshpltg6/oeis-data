{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A018796",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 18796,
			"data": "0,1,25,36,4,529,64,729,81,9,100,1156,121,1369,144,1521,16,1764,1849,196,2025,2116,225,2304,2401,25,2601,2704,289,2916,3025,3136,324,3364,3481,35344,36,3721,3844,3969,400,41209,4225,4356,441,45369,4624,4761,484,49",
			"name": "Smallest square that begins with n.",
			"comment": [
				"If 4*(n+1) \u003c 10^d then a(n) \u003c (n+1)*10^d. - _Robert Israel_, Aug 01 2014"
			],
			"link": [
				"Zak Seidov, \u003ca href=\"/A018796/b018796.txt\"\u003eTable of n, a(n) for n = 0..100000\u003c/a\u003e, May 22 2016 (first 10000 terms from Chai Wah Wu)"
			],
			"example": [
				"Among first 100001 terms, the largest is a(99999) = 99999515529 = 316227^2. - _Zak Seidov_, May 22 2016"
			],
			"maple": [
				"a:= proc(n) local k,d,x;",
				"  if issqr(n) then return n",
				"  else for d from 1 do",
				"    for k from 0 to 10^d-1 do",
				"    x:= 10^d*n+k;",
				"    if issqr(x) then return x fi",
				"    od",
				"    od",
				"  fi",
				"end proc:",
				"seq(a(n),n=1..100); # _Robert Israel_, Jul 31 2014"
			],
			"mathematica": [
				"Table[With[{d = IntegerDigits@ n}, k = 1; While[Or[IntegerLength[k^2] \u003c Length@ d, Take[IntegerDigits[k^2], Length@ d] != d], k++]; k^2], {n, 49}] (* _Michael De Vlieger_, May 23 2016 *)"
			],
			"program": [
				"(Python)",
				"n = 1",
				"while n \u003c 100:",
				"    for k in range(10**3):",
				"        if str(k**2).startswith(str(n)):",
				"            print(k**2,end=', ')",
				"            break",
				"    n += 1 # _Derek Orr_, Jul 31 2014",
				"(Python)",
				"from gmpy2 import isqrt",
				"def A018796(n):",
				"    if n == 0:",
				"        return 0",
				"    else:",
				"        d, nd = 1, n",
				"        while True:",
				"            x = (isqrt(nd-1)+1)**2",
				"            if x \u003c nd+d:",
				"                return int(x)",
				"            d *= 10",
				"            nd *= 10 # _Chai Wah Wu_, May 23 2016",
				"(PARI) \\\\Set precision high enough (for the cases where n+1 is a square)!",
				"a(n) = {my(v=vector(2));if(issquare(n),return(n), v=[sqrt(n),sqrt(n+1-(10^-((#digits(n)+7))))]; while(ceil(v[1])\u003efloor(v[2]),v*=sqrt(10)));ceil(v[1])^2",
				"} \\\\ _David A. Corneth_, May 22 2016"
			],
			"xref": [
				"Cf. A018851, A077502."
			],
			"keyword": "nonn,base,easy",
			"offset": "0,3",
			"author": "_David W. Wilson_",
			"ext": [
				"a(0)=0  prepended by _Zak Seidov_, May 22 2016"
			],
			"references": 18,
			"revision": 45,
			"time": "2021-09-04T17:29:28-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}