{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A336834",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 336834,
			"data": "1,1,1,1,1,0,1,1,1,1,1,0,1,0,1,1,1,1,1,0,1,0,1,0,1,1,1,0,1,0,1,1,1,1,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,1,1,0,1,0,1,0,1,1,1,0,1,0,1,1,1,1,1,1,1,0,1,0,1,1,1,0,1,0,1,0,1,1,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,1,1,0,1,0,1,0,1,0",
			"name": "a(n) = 1 if A005940(1+n) is deficient, 0 otherwise.",
			"comment": [
				"Any node of Doudna-tree (A005940) which is deficient, is marked here with 1, and has also its all leftward descendants (those that are obtained only by prime shifting, A003961) deficient.",
				"Any odd node marked with 0 here (that is, nondeficient nodes in A005940) has all its ancestors up at least to the first even ancestor also nondeficient. None is the seen in the illustration below, because the first case is 945, which is the first odd abundant number (A005231), whose even parent is A064989(945) = 120, also an abundant number. (But please see also _Michael De Vlieger_'s illustration of the tree down to level 8 where it is included in the bottom row).",
				"Specifically, any odd perfect number, if they exist, should have an abundant mother (which should be also a member of A336930), and also abundant grand-ancestors (on the prime-shift line), up to and including the first even ancestor, at least.",
				"On the other hand, when going towards the right, any right child has larger abundancy index than its mother has, and on all rightward leaning edges, except on the rightmost (powers of 2), a nondeficient number is eventually reached. See A336915."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A336834/b336834.txt\"\u003eTable of n, a(n) for n = 0..65535\u003c/a\u003e",
				"Michael De Vlieger, \u003ca href=\"/A336834/a336834.png\"\u003eTree illustrated down to level 7\u003c/a\u003e (red circles stand for 1's, white circles for 0's, and the numbers inside circles are those in A005940)",
				"Michael De Vlieger, \u003ca href=\"/A336834/a336834_2.png\"\u003eTree illustrated down to level 8\u003c/a\u003e (otherwise like in above. Note how the first three odd abundant numbers 945, 1575, 2205 can be seen in the bottom row, inscribed in white circles, being left children of other white circles)",
				"Michael De Vlieger, \u003ca href=\"/A336834/a336834_1.png\"\u003eChart of levels 0 \u003c= j \u003c= 16\u003c/a\u003e, vertically exaggerated 120X.",
				"Michael De Vlieger, \u003ca href=\"/A336834/a336834_3.png\"\u003e4096-pixel square raster showing 2^24 terms\u003c/a\u003e, where black = 1 and white = 0.",
				"\u003ca href=\"/index/Ch#char_fns\"\u003eIndex entries for characteristic functions\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A294934(A005940(1+n))."
			],
			"example": [
				"a(n) tells whether the n-th node in A005940 (here counted with offset 0) is deficient (here marked with 1), or not (marked with 0).",
				"The first six levels of the binary tree (compare also to the illustration given at A005940):",
				"                               1",
				"                               |",
				"                               1",
				"                ............../ \\..............",
				"               1                               1",
				"        ....../ \\......                 ....../ \\......",
				"       1               0               1               1",
				"      / \\             / \\             / \\             / \\",
				"     /   \\           /   \\           /   \\           /   \\",
				"    1     1         1     0         1     0         1     1",
				"   / \\   / \\       / \\   / \\       / \\   / \\       / \\   / \\",
				"  1   1 1   0     1   0 1   0     1   1 1   0     1   0 1   1"
			],
			"mathematica": [
				"Boole[DivisorSigma[1, #] \u003c 2 #] \u0026 /@ Nest[Append[#1, Prime[1 + BitLength[#2] - DigitCount[#2, 2, 1]]*#1[[#2 - 2^Floor@ Log2@ #2 + 1]]] \u0026 @@ {#, Length@ #} \u0026, {1}, 105]",
				"(* Second program: decode chart of levels 0 \u003c= j \u003c= 16 to render 2^16 + 1 terms of a(n) in binary tree form *)",
				"Block[{img = Import[\"https://oeis.org/A336834/a336834_1.png\"], s, r, w, d}, s = ImageData[img]; r = (-1 + FirstPosition[s, 1.][[1]])/3; Set[{w, d}, {#1, -1 + #2/r}] \u0026 @@ ImageDimensions[img]; Array[Function[{i, j, k}, Array[s[[k, # i + 1]] /. {0. -\u003e 1, 1.0 -\u003e 0} \u0026, j, 0]] @@ {w/#1, #1, #2} \u0026 @@ {If[# == 0, 1, 2^(# - 1)], r # + 1} \u0026, d + 1, 0] ]",
				"(* Third program: decode raster of 2^24 terms *)",
				"ImageData[Import[\"https://oeis.org/A336834/a336834_3.png\"]] /. {0. -\u003e 1, 1. -\u003e 0} // Flatten",
				"(* _Michael De Vlieger_, Aug 18 2020 *)"
			],
			"program": [
				"(PARI)",
				"A005940(n) = { my(p=2, t=1); n--; until(!n\\=2, if((n%2), (t*=p), p=nextprime(p+1))); (t) };",
				"A294934(n) = ((n+n) \u003e sigma(n));",
				"A336834(n) = A294934(A005940(1+n));"
			],
			"xref": [
				"Cf. A003961, A005231, A005940, A064989, A294934, A336389, A336835, A336915, A336917, A336930, A337377, A337386.",
				"Cf. A252743, A342000 for similarly constructed sequences, and also arrays A341605/A341606 for a more promising development of the ideas presented here."
			],
			"keyword": "nonn,tabf",
			"offset": "0",
			"author": "_Antti Karttunen_ and _Michael De Vlieger_, Aug 07 2020",
			"references": 9,
			"revision": 46,
			"time": "2021-03-14T18:43:56-04:00",
			"created": "2020-09-05T22:43:12-04:00"
		}
	]
}