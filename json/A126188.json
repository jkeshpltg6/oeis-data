{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A126188",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 126188,
			"data": "1,3,10,36,135,2,519,24,2034,180,5,8100,1110,75,32688,6210,675,14,133380,32886,4851,252,549342,168210,30996,2646,42,2280690,840132,184842,21672,882,9534591,4124682,1053486,154980,10584,132,40103019",
			"name": "Triangle read by rows: T(n,k) is the number of hex trees with n edges and k pairs of adjacent vertices of outdegree 2.",
			"comment": [
				"A hex tree is a rooted tree where each vertex has 0, 1, or 2 children and, when only one child is present, it is either a left child, or a middle child, or a right child (name due to an obvious bijection with certain tree-like polyhexes; see the Harary-Read reference).",
				"Row n has floor(n/2) terms (n\u003e=2).",
				"Sum of terms in row n = A002212(n+1).",
				"T(n,0) = A126189(n).",
				"Sum_{k=0..floor(n/2)-1} k*T(n,k) = A126190(n)."
			],
			"link": [
				"F. Harary and R. C. Read, \u003ca href=\"https://doi.org/10.1017/S0013091500009135\"\u003eThe enumeration of tree-like polyhexes\u003c/a\u003e, Proc. Edinburgh Math. Soc. (2) 17 (1970), 1-13."
			],
			"formula": [
				"G.f.: G = G(t,z) = 1+3*z*G+z^2*(1+3*z*G+t*(G-1-3*z*G))^2 (explicit expression in the Maple program)."
			],
			"example": [
				"Triangle starts:",
				"     1;",
				"     3;",
				"    10;",
				"    36;",
				"   135,    2;",
				"   519,   24;",
				"  2034,  180,    5;",
				"  8100, 1110,   75;"
			],
			"maple": [
				"G:=1/2*(12*z^3*t+2*z^2*t^2-2*z^2*t-6*z^3*t^2-3*z-6*z^3+1-sqrt(1+9*z^2-4*z^2*t-6*z+12*z^3*t-12*z^3))/z^2/(3*z*t-t-3*z)^2: Gser:=simplify(series(G,z=0,18)): for n from 0 to 14 do P[n]:=sort(coeff(Gser,z,n)) od: 1;3;for n from 2 to 14 do seq(coeff(P[n],t,j),j=0..floor(n/2)-1) od;"
			],
			"mathematica": [
				"g[t_,z_] = G /. Solve[G == 1 + 3z*G + z^2*(1 + 3z*G + t*(G - 1 - 3z*G))^2, G][[1]]; Flatten[ CoefficientList[ CoefficientList[ Series[g[t,z], {z,0,13}], z], t]][[1 ;; 39]] (* _Jean-François Alcover_, May 27 2011, after g.f. *)"
			],
			"xref": [
				"Cf. A002212, A126189, A126190."
			],
			"keyword": "nonn,tabf",
			"offset": "0,2",
			"author": "_Emeric Deutsch_, Dec 25 2006",
			"references": 2,
			"revision": 17,
			"time": "2021-02-06T15:22:07-05:00",
			"created": "2007-01-12T03:00:00-05:00"
		}
	]
}