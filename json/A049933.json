{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A049933",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 49933,
			"data": "1,1,1,4,8,19,35,70,140,349,663,1310,2609,5214,10425,20850,41700,104249,198073,390935,779265,1557231,3113815,6227316,12454423,24908776,49817517,99635018,199270025,398540046,797080089,1594160178,3188320356,7970800889,15144521689",
			"name": "a(n) = a(1) + a(2) + ... + a(n-1) + a(m) for n \u003e= 4, where m = 2^(p+1) + 2 - n and p is the unique integer such that 2^p \u003c n - 1 \u003c= 2^(p+1), starting with a(1) = a(2) = a(3) = 1.",
			"formula": [
				"From _Petros Hadjicostas_, Nov 06 2019: (Start)",
				"a(n) = a(2^ceiling(log_2(n-1)) + 2 - n) + Sum_{i = 1..n-1} a(i) for n \u003e= 4.",
				"a(n) = a(n - 1 - A006257(n-2)) + Sum_{i = 1..n-1} a(i) for n \u003e= 4. (End)"
			],
			"example": [
				"From _Petros Hadjicostas_, Nov 06 2019: (Start)",
				"a(4) = a(2^ceiling(log_2(4-1)) + 2 - 4) + a(1) + a(2) + a(3) = a(2) + a(1) + a(2) + a(3) = 4.",
				"a(5) = a(2^ceiling(log_2(5-1)) + 2 - 5) + a(1) + a(2) + a(3) + a(4) = a(1) + a(1) + a(2) + a(3) + a(4) = 8.",
				"a(6) = a(2^ceiling(log_2(6-1)) + 2 - 6) + a(1) + a(2) + a(3) + a(4) + a(5) = a(4) + a(1) + a(2) + a(3) + a(4) + a(5) = 19.",
				"a(7) =  a(7 - 1 - A006257(7-2)) + Sum_{i = 1..6} a(i) = a(3) +  Sum_{i = 1..6} a(i) = 35.",
				"a(8) =  a(8 - 1 - A006257(8-2)) + Sum_{i = 1..7} a(i) = a(2) +  Sum_{i = 1..7} a(i) = 70. (End)"
			],
			"maple": [
				"s := proc(n) option remember; `if`(n \u003c 1, 0, a(n) + s(n - 1)); end proc;",
				"a := proc(n) option remember;",
				"  `if`(n \u003c 4, 1, s(n - 1) + a(Bits:-Iff(n - 2, n - 2) + 3 - n));",
				"end proc;",
				"seq(a(n), n = 1 .. 30);"
			],
			"mathematica": [
				"b[n_] := Module[{p}, For[p = 0, True, p++, If[2^p \u003c n - 1 \u003c= 2^(p + 1), Return[p]]]];",
				"a[n_] := a[n] = If[n \u003c 4, 1, With[{m = 2^(b[n] + 1) + 2 - n}, Total[ Array[a, n - 1]] + a[m]]];",
				"Array[a, 35] (* _Jean-François Alcover_, Apr 24 2020 *)"
			],
			"xref": [
				"Cf. A006257, A049885 (similar with minus a(m)), A049937, A049945."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Clark Kimberling_",
			"ext": [
				"Name edited by and more terms from _Petros Hadjicostas_, Nov 06 2019"
			],
			"references": 9,
			"revision": 23,
			"time": "2020-04-28T02:04:49-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}