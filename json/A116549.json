{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A116549",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 116549,
			"data": "1,2,3,4,4,5,6,7,5,6,7,8,8,9,10,11,5,6,7,8,8,9,10,11,9,10,11,12,12,13,14,15,6,7,8,9,9,10,11,12,10,11,12,13,13,14,15,16,10,11,12,13,13,14,15,16,14,15,16,17,17,18,19,20",
			"name": "a(0) = 1. a(m + 2^n) = a(n) + a(m), for 0 \u003c= m \u003c= 2^n - 1.",
			"comment": [
				"Consider the following bijection between the natural numbers and hereditarily finite sets. For each n, write out n in binary. Assign to each set already given a natural number m the (m+1)-th digit of the binary number (reading from right to left). Let the set assigned to n contain all and only those sets which have a 1 for their digit. Then a(n) gives the number of pairs of braces appearing in the n-th set written out in full, e.g., for 3, we have {{{}}{}}, with 4 pairs of braces. - _Thomas Anton_, Mar 16 2019"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A116549/b116549.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"formula": [
				"For n \u003e 0: a(n) = a(A000523(n)) + a(A053645(n)). - _Reinhard Zumkeller_, Aug 27 2014"
			],
			"example": [
				"From _Gus Wiseman_, Jul 22 2019: (Start)",
				"A finitary (or hereditarily finite) set is equivalent to a rooted identity tree. The following list shows the first few rooted identity trees together with their corresponding index in the sequence (o = leaf).",
				"   0: o",
				"   1: (o)",
				"   2: ((o))",
				"   3: (o(o))",
				"   4: (((o)))",
				"   5: (o((o)))",
				"   6: ((o)((o)))",
				"   7: (o(o)((o)))",
				"   8: ((o(o)))",
				"   9: (o(o(o)))",
				"  10: ((o)(o(o)))",
				"  11: (o(o)(o(o)))",
				"  12: (((o))(o(o)))",
				"  13: (o((o))(o(o)))",
				"  14: ((o)((o))(o(o)))",
				"  15: (o(o)((o))(o(o)))",
				"  16: ((((o))))",
				"  17: (o(((o))))",
				"  18: ((o)(((o))))",
				"  10: (o(o)(((o))))",
				"(End)"
			],
			"mathematica": [
				"Nest[Append[#1, #1[[#3 + 1]] + #1[[#2 - 2^#3 + 1]] \u0026 @@ {#1, #2, Floor@ Log2@ #2}] \u0026 @@ {#, Length@ #} \u0026, {1}, 63] (* _Michael De Vlieger_, Apr 21 2019 *)",
				"bpe[n_]:=Join@@Position[Reverse[IntegerDigits[n,2]],1];",
				"dab[n_]:=1+Total[dab/@(bpe[n]-1)];",
				"Array[dab,30,0] (* _Gus Wiseman_, Jul 22 2019 *)"
			],
			"program": [
				"(Haskell)",
				"import Data.Function (on); import Data.List (genericIndex)",
				"a116549 = genericIndex a116549_list",
				"a116549_list = 1 : zipWith ((+) `on` a116549) a000523_list a053645_list",
				"-- _Reinhard Zumkeller_, Aug 27 2014"
			],
			"xref": [
				"Cf. A000523, A053645.",
				"Cf. A000081, A000120, A004111, A029931, A048793, A061775, A070939, A072639, A276625, A279861, A326031."
			],
			"keyword": "easy,nonn",
			"offset": "0,2",
			"author": "_Leroy Quet_, Mar 16 2006",
			"references": 4,
			"revision": 34,
			"time": "2019-07-23T04:25:00-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}