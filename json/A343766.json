{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A343766",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 343766,
			"data": "0,-1,1,-2,-4,-3,3,2,4,-5,-7,-6,-12,-13,-11,-8,-10,-9,9,8,10,7,5,6,12,11,13,-14,-16,-15,-21,-22,-20,-17,-19,-18,-36,-37,-35,-38,-40,-39,-33,-34,-32,-23,-25,-24,-30,-31,-29,-26,-28,-27,27,26,28,25,23,24",
			"name": "Lexicographically earliest sequence of distinct integers such that a(0) = 0 and the balanced ternary expansions of two consecutive terms differ by a single digit, as far to the right as possible.",
			"comment": [
				"This sequence has similarities with A003188 and with A341055.",
				"A007949 gives the positions of the digit that is altered from one term to the other.",
				"To compute a(n):",
				"- consider the ternary representation of A128173(n),",
				"- replace 1's by -1's and 2's by 1's,",
				"- convert back to decimal."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A343766/b343766.txt\"\u003eTable of n, a(n) for n = 0..6560\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A343766/a343766.png\"\u003eScatterplot of the first 3^10 terms\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A343766/a343766.gp.txt\"\u003ePARi program for A343766\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the integers\u003c/a\u003e"
			],
			"formula": [
				"a(n) = -A117966(A128173(n)).",
				"Sum_{k=0..n-1} sign(a(k)) = -A081134(n).",
				"Sum_{k=0..n} a(k) = 0 iff n belongs to A024023."
			],
			"example": [
				"The first terms, alongside their balanced ternary expansion (with T's denoting -1's), are:",
				"  n   a(n)  bter(a(n))",
				"  --  ----  ----------",
				"   0     0           0",
				"   1    -1           T",
				"   2     1           1",
				"   3    -2          T1",
				"   4    -4          TT",
				"   5    -3          T0",
				"   6     3          10",
				"   7     2          1T",
				"   8     4          11",
				"   9    -5         T11",
				"  10    -7         T1T",
				"  11    -6         T10",
				"  12   -12         TT0",
				"  13   -13         TTT",
				"  14   -11         TT1"
			],
			"program": [
				"(PARI) See Links section."
			],
			"xref": [
				"Cf. A003188, A007949, A024023, A081134, A128173, A341055."
			],
			"keyword": "sign,base",
			"offset": "0,4",
			"author": "_Rémy Sigrist_, Apr 28 2021",
			"references": 1,
			"revision": 19,
			"time": "2021-05-02T04:17:38-04:00",
			"created": "2021-05-01T21:56:10-04:00"
		}
	]
}