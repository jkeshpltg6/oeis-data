{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006892",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6892,
			"id": "M0860",
			"data": "1,2,3,7,23,167,7223,13053767,42600227803223,453694852221687377444001767,51459754733114686962148583993443846186613037940783223",
			"name": "Representation as a sum of squares requires n squares with greedy algorithm.",
			"comment": [
				"Of course Lagrange's theorem tells us that any positive integer can be written as a sum of at most four squares (cf. A004215).",
				"Records in A053610. - _Hugo van der Sanden_, Jun 24 2015"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Rick L. Shepherd, \u003ca href=\"/A006892/b006892.txt\"\u003eTable of n, a(n) for n = 1..15\u003c/a\u003e",
				"Art of Problem Solving, \u003ca href=\"https://www.artofproblemsolving.com/Wiki/index.php/2010_AMC_10A_Problems/Problem_25\"\u003e2010 AMC 10A Problems/Problem 25\u003c/a\u003e [From _Rick L. Shepherd_, Jan 28 2014}",
				"E. Lemoine, \u003ca href=\"http://gallica.bnf.fr/ark:/12148/bpt6k30518/f719.image\"\u003eDécomposition d'un nombre entier N en ses puissances nièmes maxima\u003c/a\u003e, C. R. Acad. Sci. Paris, Vol. 95, pp. 719-722, 1882 (then next pages)."
			],
			"formula": [
				"For n \u003e= 4, a(n) = a(n-1) + ((a(n-1)+1)/2)^2. - Joe K. Crump (joecr(AT)carolina.rr.com), Apr 16 2000",
				"a(n) = n for n \u003c= 3; for n \u003e 3, a(n) = ((a(n-1)+3)/2)^2 - 2. - _Arkadiusz Wesolowski_, Mar 30 2013",
				"a(n+2) = 2 * A053630(n) - 3. - _Thomas Ordowski_, Jul 14 2014",
				"a(n+3) = A053630(n)^2 - 2. - _Thomas Ordowski_, Jul 19 2014"
			],
			"example": [
				"Here is why a(5) = 23: start with 23, subtract largest square \u003c= 23, which is 16, getting 7.",
				"Now subtract largest square \u003c= 7, which is 4, getting 3.",
				"Now subtract largest square \u003c= 3, which is 1, getting 2.",
				"Now subtract largest square \u003c= 2, which is 1, getting 1.",
				"Now subtract largest square \u003c= 1, which is 1, getting 0.",
				"Thus 23 = 16+4+1+1+1.",
				"It took 5 steps to get to 0, and 23 is the smallest number which takes 5 steps. - _N. J. A. Sloane_, Jan 29 2014"
			],
			"program": [
				"(PARI) a(n) = if (n \u003c= 3, n , ((a(n-1)+3)/2)^2 - 2) \\\\ _Michel Marcus_, May 25 2013"
			],
			"xref": [
				"Cf. A004215, A053610."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Jeffrey Shallit_",
			"ext": [
				"Four more terms from _Rick L. Shepherd_, Jan 27 2014"
			],
			"references": 7,
			"revision": 62,
			"time": "2015-06-25T10:32:06-04:00",
			"created": "1994-04-28T03:00:00-04:00"
		}
	]
}