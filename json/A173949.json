{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A173949",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 173949,
			"data": "0,1,26,2131,362164,105007621,5156362654,129102916279,108696708796264,13163623138673569,18033329053484721586,30330904507928806086691,30344915637965488890716,1487479897654682071525709",
			"name": "a(n) = numerator of (Zeta(2, 1/4) - Zeta(2, n+1/4))/16, where Zeta is the Hurwitz Zeta function.",
			"comment": [
				"For the Catalan constant see A006752.",
				"The denominators are given in A173948.",
				"a(n+1)/A173948(n+1), for n\u003e= 0, gives the partial sum Sum_{k=0..n} 1/(4*k + 1)^2. For {(4*k + 1)^2}_{k\u003e=0} see A016814. The limit n -\u003e infinity is given in A222183 as 1.074833072... . - _Wolfdieter Lang_, Nov 14 2017"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A173949/b173949.txt\"\u003eTable of n, a(n) for n = 0..250\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HurwitzZetaFunction.html\"\u003eHurwitz Zeta Function\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PolygammaFunction.html\"\u003ePolygamma Function\u003c/a\u003e"
			],
			"formula": [
				"a(n) = numerator of expression (8*Catalan + Pi^2 - Zeta(2, (4*n + 1)/4))/16.",
				"a(n) = numerator(r(n)) with r(n) = (Zeta(2,1/4) - Zeta(2, n + 1/4))/16, with the Hurwitz Zeta function Z(2, k). With Zeta(2,1/4) = 8 Catalan + Pi^2 this is the preceding formula, and Zeta(2, n + 1/4) = Psi(1, n + 1/4) with the polygamma (trigamma) function Psi(1, k). - _Wolfdieter Lang_, Nov 14 2017"
			],
			"example": [
				"The rationals r(n) begin: 0/1, 1/1, 26/25, 2131/2025, 362164/342225, 105007621/98903025, 5156362654/4846248225, 129102916279/121156205625, 108696708796264/101892368930625, 13163623138673569/12328976640605625, ... - _Wolfdieter Lang_, Nov 14 2017"
			],
			"maple": [
				"r := n -\u003e (Psi(1, 1/4) - Zeta(0, 2, n+1/4))/16:",
				"seq(numer(simplify(r(n))), n=0..13); # _Peter Luschny_, Nov 14 2017"
			],
			"mathematica": [
				"Table[Numerator[FunctionExpand[(8*Catalan + Pi^2 - Zeta[2, (4*n + 1)/4])/16]], {n, 0, 20}] (* _Vaclav Kotesovec_, Nov 14 2017 *)",
				"Numerator[Table[Sum[1/(4*k + 1)^2, {k, 0, n-1}], {n, 0, 20}]] (* _Vaclav Kotesovec_, Nov 14 2017 *)"
			],
			"program": [
				"(PARI) for(n=0,20, print1(numerator(sum(k=0,n-1, 1/(4*k+1)^2)), \", \")) \\\\ _G. C. Greubel_, Aug 23 2018",
				"(MAGMA) [0] cat [Numerator((\u0026+[1/(4*k+1)^2: k in [0..n-1]])): n in [1..20]]; // _G. C. Greubel_, Aug 23 2018"
			],
			"xref": [
				"Cf. A016814, A120268, A173945, A173947, A173948, A222183."
			],
			"keyword": "nonn,frac,easy",
			"offset": "0,3",
			"author": "_Artur Jasinski_, Mar 03 2010",
			"ext": [
				"Edited by _Wolfdieter Lang_, Nov 14 2017",
				"Name changed according to a formula of Lang by _Peter Luschny_, Nov 14 2017"
			],
			"references": 12,
			"revision": 40,
			"time": "2018-08-23T03:07:23-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}