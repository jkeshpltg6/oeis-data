{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A174981",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 174981,
			"data": "0,1,1,2,3,1,2,3,5,2,5,3,4,1,3,4,7,3,8,5,7,2,7,5,8,3,7,4,5,1,4,5,9,4,11,7,10,3,11,8,13,5,12,7,9,2,9,7,12,5,13,8,11,3,10,7,11,4,9,5,6,1,5,6,11,5,14,9,13,4,15,11,18,7,17,10,13,3,14,11,19,8,21,13,18,5,17,12,19,7,16",
			"name": "Numerators of the L-tree, left-to-right enumeration.",
			"comment": [
				"a(n) is a subsequence of A174980. a(n)/A002487(n+2) enumerates all the reduced nonnegative rational numbers exactly once (L-tree)."
			],
			"link": [
				"Edsger Dijkstra, Selected Writings on Computing, Springer, 1982, p. 232. \u003ca href=\"http://www.cs.utexas.edu/users/EWD/ewd05xx/EWD578.PDF\"\u003e EWD 578: More about the function fusc\u003c/a\u003e.",
				"Peter Luschny, \u003ca href=\"http://www.oeis.org/wiki/User:Peter_Luschny/SternsDiatomic\"\u003e Rational Trees and Binary Partitions\u003c/a\u003e.",
				"Moritz A. Stern, Über eine zahlentheoretische Funktion, \u003ca href=\"http://www.digizeitschriften.de/resolveppn/GDZPPN002150301\"\u003e J. Reine Angew. Math., 55 (1858), 193-220\u003c/a\u003e."
			],
			"example": [
				"The sequence splits into rows of length 2^k:",
				"0,",
				"1, 1,",
				"2, 3, 1, 2,",
				"3, 5, 2, 5, 3, 4, 1, 3,",
				"4, 7, 3, 8, 5, 7, 2, 7, 5, 8, 3, 7, 4, 5, 1, 4,",
				"...",
				"The fractions are",
				"0/1,",
				"1/2, 1/1,",
				"2/3, 3/2, 1/3, 2/1,",
				"3/4, 5/3, 2/5, 5/2, 3/5, 4/3, 1/4, 3/1,",
				"4/5, 7/4, 3/7, 8/3, 5/8, 7/5, 2/7, 7/2, 5/7, 8/5, 3/8, 7/3, 4/7, 5/4, 1/5, 4/1,",
				"..."
			],
			"maple": [
				"SternDijkstra := proc(L, p, n) local k, i, len, M; len := nops(L); M := L; k := n; while k \u003e 0 do M[1+(k mod len)] := add(M[i], i = 1..len); k := iquo(k, len); od; op(p, M) end:",
				"Ltree := proc(n) 5*2^ilog2(n+1); SternDijkstra([0,1], 1, n + 2 + %) / SternDijkstra([1,0], 2, n + 2) end:",
				"a := proc(n) 5*2^ilog2(n+1); SternDijkstra([0,1], 1, n + 2 + %) end:",
				"seq(a(n), n=0..90);"
			],
			"mathematica": [
				"SternDijkstra[L_, p_, n_] := Module[{k, i, len, M}, len := Length[L]; M = L; k = n; While[k \u003e 0, M[[1 + Mod[k, len]]] = Sum[M[[i]], {i, 1, len}]; k = Quotient[k, len]]; M[[p]]]; Ltree[n_] := With[{k = 5*2^Simplify[ Floor[ Log[2, n + 1]]]}, SternDijkstra[{0, 1}, 1, n + 2 + k]/ SternDijkstra[{1, 0}, 2, n + 2]]; a[0] = 0; a[n_] := With[{k = 5*2^Simplify[ Floor[ Log[2, n + 1]]]}, SternDijkstra[{1, 0}, 1, n + 2 + k]]; row[0] = {a[0]}; row[n_] := Table[a[k], {k, 2^n - 3, 2^(n+1) - 4}] // Reverse; Table[row[n], {n, 0, 6}] // Flatten (* _Jean-François Alcover_, Jul 26 2013, after Maple *)"
			],
			"xref": [
				"Cf. A002487, A070879, A047679, A007306, A174980."
			],
			"keyword": "easy,nonn,frac,tabf",
			"offset": "0,4",
			"author": "_Peter Luschny_, Apr 03 2010",
			"references": 3,
			"revision": 14,
			"time": "2017-11-29T06:39:36-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}