{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A152555",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 152555,
			"data": "1,2,7,5,30,42,42,14,143,297,462,495,363,198,42,728,2002,4004,6006,7436,7436,6292,4290,2288,858,132,3876,13260,31824,58604,91364,122876,145535,153361,143936,120185,87971,56329,29939,12584,3575,429,21318,87210",
			"name": "Coefficients in a q-analog of the function LambertW(-2x)/(-2x), as a triangle read by rows.",
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A152555/b152555.txt\"\u003eRows 0 to 30 of the triangle, flattened.\u003c/a\u003e",
				"Eric Weisstein, \u003ca href=\"http://mathworld.wolfram.com/q-ExponentialFunction.html\"\u003eq-Exponential Function\u003c/a\u003e from MathWorld.",
				"Eric Weisstein, \u003ca href=\"http://mathworld.wolfram.com/q-Factorial.html\"\u003eq-Factorial\u003c/a\u003e from MathWorld."
			],
			"formula": [
				"G.f.: A(x,q) = Sum_{n\u003e=0} Sum_{k=0..n(n-1)/2} T(n,k)*q^k*x^n/faq(n,q), where faq(n,q) is the q-factorial of n.",
				"G.f.: A(x,q) = (1/x)*Series_Reversion( x/e_q(x,q)^2 ) where e_q(x,q) = Sum_{n\u003e=0} x^n/faq(n,q) is the q-exponential function.",
				"G.f. satisfies: A(x,q) = e_q( x*A(x,q), q)^2 and A( x/e_q(x,q)^2, q) = e_q(x,q)^2.",
				"G.f. at q=1: A(x,1) = LambertW(-2x)/(-2x).",
				"Row sums at q=+1: Sum_{k=0..n(n-1)/2} T(n,k) = 2*(2n+2)^(n-1).",
				"Row sums at q=-1: Sum_{k=0..n(n-1)/2} T(n,k)*(-1)^k = 2*(2n+2)^[(n-1)/2].",
				"Sum_{k=0..n(n-1)/2} T(n,k)*exp(2Pi*I*k/n)) = 2 for n\u003e=1; i.e., the n-th row sum at q = exp(2Pi*I/n), the n-th root of unity, equals 2 for n\u003e=1. [From Vladeta Jovovic]",
				"Sum_{k=0..binomial(n,2)} T(n,k)*q^k = Sum_{pi} 2*(2*n+1)!/(2*n-k+2)!*faq(n,q)/Product_{i=1..n} e(i)!*faq(i,q)^e(i), where pi runs over all nonnegative integer solutions to e(1)+2*e(2)+...+n*e(n) = n and k = e(1)+e(2)+...+e(n). [From _Vladeta Jovovic_, Dec 07 2008]"
			],
			"example": [
				"Triangle begins:",
				"1;",
				"2;",
				"7,5;",
				"30,42,42,14;",
				"143,297,462,495,363,198,42;",
				"728,2002,4004,6006,7436,7436,6292,4290,2288,858,132;",
				"3876,13260,31824,58604,91364,122876,145535,153361,143936,120185,87971,56329,29939,12584,3575,429;",
				"21318,87210,242250,519384,945744,1508070,2165664,2826420,3392520,3756626,3853322,3662106,3221330,2613240,1944324,1313760,794614,420784,185640,64090,14586,1430;...",
				"where row sums = 2*(2*n+2)^(n-1) (A097629).",
				"Row sums at q=-1 = 2*(2*n+2)^[(n-1)/2] (A152556).",
				"The generating function starts:",
				"A(x,q) = 1 + 2*x + (7 + 5*q)*x^2/faq(2,q) + (30 + 42*q + 42*q^2 + 14*q^3)*x^3/faq(3,q) + (143 + 297*q + 462*q^2 + 495*q^3 + 363*q^4 + 198*q^5 + 42*q^6)*x^4/faq(4,q) + ...",
				"The q-factorial of n is faq(n,q) = Product_{k=1..n} (q^k-1)/(q-1): faq(0,q)=1, faq(1,q)=1, faq(2,q)=(1+q), faq(3,q)=(1+q)*(1+q+q^2), faq(4,q)=(1+q)*(1+q+q^2)*(1+q+q^2+q^3), ...",
				"Special cases.",
				"q=0: A(x,0) = 1 + 2*x + 7*x^2 + 30*x^3 + 143*x^4 + 728*x^5 +... (A006013)",
				"q=1: A(x,1) = 1 + 2*x + 12/2*x^2 + 128/6*x^3 + 2000/24*x^4 + 41472/120*x^5 +...",
				"q=2: A(x,2) = 1 + 2*x + 17/3*x^2 + 394/21*x^3 + 21377/315*x^4 + 2537724/9765*x^5 +...",
				"q=3: A(x,3) = 1 + 2*x + 22/4*x^2 + 912/52*x^3 + 126692/2080*x^4 + 56277344/251680*x^5 +..."
			],
			"program": [
				"(PARI) {T(n,k)=local(e_q=1+sum(j=1,n,x^j/prod(i=1,j,(q^i-1)/(q-1))), LW2_q=serreverse(x/(e_q+x*O(x^n))^2)/x); polcoeff(polcoeff(LW2_q+x*O(x^n),n,x)*prod(i=1,n,(q^i-1)/(q-1))+q*O(q^k),k,q)}"
			],
			"xref": [
				"Cf. A097629 (row sums), A006013 (column 0), A000108 (right border), A152559.",
				"Cf. A152556 (q=-1), A152557 (q=2), A152558 (q=3).",
				"Cf. variants: A152290, A152550."
			],
			"keyword": "eigen,nonn,tabf",
			"offset": "0,2",
			"author": "_Paul D. Hanna_, Dec 07 2008",
			"references": 6,
			"revision": 7,
			"time": "2015-07-10T19:37:00-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}