{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A342721",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 342721,
			"data": "0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,2,0,0,2,0,0,0,3,1,1,0,0,1,3,0,0,0,2,1,0,6,0,4,4,2,1,0,0,1,0,0,6,0,2,8,6,2,0,1,2,0,2,0,9,0,0,2,0,13,1,0,4,0,3,0,3,5,10,11",
			"name": "a(n) is the number of concave integer quadrilaterals (up to congruence) with integer side lengths a,b,c,d with n=Max(a,b,c,d), integer diagonals e,f and integer area.",
			"comment": [
				"Without loss of generality we assume that a is the largest side length and that the diagonal e divides the concave quadrilateral into two triangles with sides a,b,e and c,d,e. Then e \u003c a is a necessary condition for concavity. The triangle inequality further implies e \u003e a-b and abs(e-c) \u003c d \u003c e+c."
			],
			"example": [
				"a(66)=1 because the only concave integer quadrilateral with longest edge length 66 and integer area has sides a=66, b=55, c=12, d=65, diagonals e=55, f=65 and area 1650."
			],
			"mathematica": [
				"an={};",
				"area[a_,b_,c_,d_,e_,f_]:=(1/4)Sqrt[(4e^2 f^2-(a^2+c^2-b^2-d^2)^2)]",
				"he[a_,b_,e_]:=(1/(2 e))Sqrt[(-((a-b-e) (a+b-e) (a-b+e) (a+b+e)))];",
				"paX[e_]:={e,0} (*vertex A coordinate*)",
				"pbX[a_,b_,e_]:={(-a^2+b^2+e^2)/(2 e),he[a,b,e]}(*vertex B coordinate*)",
				"pc={0,0};(*vertex C coordinate*)",
				"pdX[c_,d_,e_]:={(c^2-d^2+e^2)/(2 e),-he[c,d,e]}(*vertex D coordinate*)",
				"concaveQ[{bx_,by_},{dx_,dy_},e_]:=If[by dx-bx dy\u003c0||by dx-bx dy\u003e(by-dy) e,True,False]",
				"gQ[x_,y_]:=Module[{z=x-y,res=False},Do[If[z[[i]]\u003e0,res=True;Break[],If[z[[i]]\u003c0,Break[]]],{i,1,4}];res]",
				"canonicalQ[{a_,b_,c_,d_}]:=Module[{m={a,b,c,d}},If[(gQ[{b,a,d,c},m]||gQ[{d,c,b,a},m]||gQ[{c,d,a,b},m]),False,True]]",
				"Do[cnt=0;",
				"Do[pa=paX[e];pb=pbX[a,b,e];pd=pdX[c,d,e];",
				"If[(f=Sqrt[(pb-pd).(pb-pd)];IntegerQ[f])\u0026\u0026(ar=area[a,b,c,d,e,f]; IntegerQ[ar])\u0026\u0026concaveQ[pb,pd,e]\u0026\u0026canonicalQ[{a,b,c,d}],cnt++",
				"(*;Print[{{a,b,c,d,e,f,ar},Graphics[Line[{pa,pb,pc,pd,pa}]]}]*)],",
				"{b,1,a},{e,a-b+1,a-1},{c,1,a},{d,Abs[e-c]+1,Min[a,e+c-1]}];",
				"AppendTo[an,cnt],{a,1,75}",
				"]",
				"an"
			],
			"xref": [
				"Cf. A340858 for trapezoids, A342720 for concave integer quadrilaterals with arbitrary area."
			],
			"keyword": "nonn",
			"offset": "1,17",
			"author": "_Herbert Kociemba_, Mar 19 2021",
			"references": 5,
			"revision": 15,
			"time": "2021-04-16T00:13:31-04:00",
			"created": "2021-04-16T00:13:31-04:00"
		}
	]
}