{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A340901",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 340901,
			"data": "0,-2,-3,4,-5,-5,-7,-8,9,-7,-11,1,-13,-9,-8,16,-17,7,-19,-1,-10,-13,-23,-11,25,-15,-27,-3,-29,-10,-31,-32,-14,-19,-12,13,-37,-21,-16,-13,-41,-12,-43,-7,4,-25,-47,13,49,23,-20,-9,-53,-29,-16,-15,-22,-31",
			"name": "Additive with a(p^e) = (-p)^e.",
			"comment": [
				"The sequence contains every integer infinitely many times.",
				"Proof (outline):",
				"1. Every integer m \u003e 9 is the sum of distinct odd primes [R. E. Dressler].",
				"2. Any integer k (positive as negative) can be written as k = 4^e - m, for sufficiently large and infinitely many e \u003e 0 and m \u003e 9.",
				"3. Pick an arbitrary integer k and write it like k = 4^e - m. Let p_1, p_2, ..., p_i be distinct odd primes such that p_1 + p_2 + ... + p_i = m. Then a(p_1*p_2*...*p_i*4^e) = 4^e - m = k. Since there are infinitely many representations of any k of the form 4^e - m, this means that there are infinitely many n such that a(n) = k.",
				"Q.E.D."
			],
			"link": [
				"Sebastian Karlsson, \u003ca href=\"/A340901/b340901.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"R. E. Dressler, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9939-1972-0292746-6\"\u003eA stronger Bertrand's postulate with an application to partitions\u003c/a\u003e, Proc. Amer. Math. Soc., 33 (1972), 226-228."
			],
			"formula": [
				"a(A002035(n)) = - A008475(A002035(n)).",
				"a(n^2) = A008475(n^2)."
			],
			"example": [
				"a(20) = a(2^2*5) = (-2)^2 + (-5) = -1."
			],
			"program": [
				"(Python)",
				"from sympy import primefactors as pf, multiplicity as mult",
				"def a(n):",
				"    return sum([(-p)**mult(p, n) for p in pf(n)])",
				"for n in range(1, 59):",
				"    print(a(n), end=', ')",
				"(PARI) a(n) = my(f=factor(n)); sum(k=1, #f~, (-f[k,1])^f[k,2]); \\\\ _Michel Marcus_, Jan 26 2021"
			],
			"xref": [
				"Cf. A001414, A002035, A008472, A008475, A008476, A316523."
			],
			"keyword": "sign",
			"offset": "1,2",
			"author": "_Sebastian Karlsson_, Jan 26 2021",
			"references": 1,
			"revision": 14,
			"time": "2021-02-15T02:59:08-05:00",
			"created": "2021-02-15T02:04:27-05:00"
		}
	]
}