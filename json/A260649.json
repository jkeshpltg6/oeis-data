{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A260649",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 260649,
			"data": "1,0,1,1,1,0,0,2,1,0,0,1,0,0,1,3,2,0,2,1,0,0,2,2,1,0,1,0,0,0,2,4,0,0,0,1,0,0,0,2,0,0,0,0,1,0,2,3,1,0,2,0,2,0,0,0,2,0,0,1,2,0,0,5,0,0,0,2,2,0,0,2,0,0,1,2,0,0,2,3,1,0,2,0,2,0,0",
			"name": "Expansion of (phi(q^3) * phi(q^5) + phi(q) * phi(q^15)) / 2 - 1 in powers of q where phi(q) is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A260649/b260649.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of (eta(q^2)^2 * eta(q^6) * eta(q^10) * eta(q^30)^2) / (eta(q) * eta(q^4) * eta(q^15) * eta(q^60)) - 1 in powers of q.",
				"a(n) is multiplicative with a(2^e) = |e-1|, a(3^e) = a(5^e) = 1, a(p^e) = e+1 if p == 1, 2, 4, 8 (mod 15), a(p^e) = (1 + (-1)^e)/2 if p == 7, 11, 13, 14 (mod 15).",
				"Moebius transform of a period 60 sequence.",
				"G.f.: Sum_{k\u003e0} Kronecker(-15, k) x^k / (1 - (-x)^k).",
				"a(n) = A122855(n) unless n=0.",
				"a(3*n) = a(5*n) = a(n). a(4*n) = A035175(n). a(4*n + 2) = 0.",
				"a(15*n + 7) = a(15*n + 11) = a(15*n + 13) = a(15*n + 14) = 0."
			],
			"example": [
				"G.f. = x + x^3 + x^4 + x^5 + 2*x^8 + x^9 + x^12 + x^15 + 3*x^16 + 2*x^17 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 1, 0, DivisorSum[ n, KroneckerSymbol[ -15, #] If[ Mod[#, 4] == 2, -1, 1] \u0026]];",
				"a[ n_] := If[ n \u003c 1, 0, Times@@ (Which[# == 1, 1, # == 2, #2 - 1, # \u003c 6, 1, KroneckerSymbol[#, -15] == 1, #2 + 1, True, 1 - Mod[#2, 2]]\u0026 @@@ FactorInteger[n])];",
				"a[ n_] := SeriesCoefficient[QPochhammer[ q^2]^2 QPochhammer[ q^6] QPochhammer[ q^10] QPochhammer[ q^30]^2 / (QPochhammer[ q] QPochhammer[ q^4] QPochhammer[ q^15] QPochhammer[ q^60]) - 1, {q, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, 0, sumdiv(n, d, kronecker(-15, d) * (-1)^(d%4==2) ))};",
				"(PARI) {a(n) = if( n\u003c1, 0, qfrep( [1, 0; 0, 15], n)[n] + qfrep( [3, 0; 0, 5], n)[n] )};",
				"(PARI) {a(n) = my(A, p, e); if( n\u003c1, 0, A = factor(n); prod(k=1, matsize(A)[1], [p, e] = A[k, ]; if( p==2, e-1, p==3 || p==5, 1, kronecker(p, -15) == 1, e+1, 1-e%2 )))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^2 * eta(x^6 + A) * eta(x^10 + A) * eta(x^30 + A)^2 / (eta(x + A) * eta(x^4 + A) * eta(x^15 + A) * eta(x^60 + A)) - 1, n))};"
			],
			"xref": [
				"Cf. A035175, A122855."
			],
			"keyword": "nonn,mult",
			"offset": "1,8",
			"author": "_Michael Somos_, Nov 12 2015",
			"references": 2,
			"revision": 21,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2015-11-12T22:28:18-05:00"
		}
	]
}