{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A332981",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 332981,
			"data": "4,21,57,93,183,291,327,395,501,545,695,791,815,831,1145,1205,1415,1631,1461,1745,1941,1865,2661,2315,2615,2855,2495,2285,3665,2705,2721,3521,3561,3351,3755,4341,3545,4701,4265,4881,3981,4821,5601,5255,6671,6041,4595",
			"name": "Smallest semiprime m = p*q such that the sum s = p + q can be expressed as an unordered sum of two primes in exactly n ways.",
			"comment": [
				"The unique square and even term of the sequence is a(1) = 4.",
				"For n = 1, the sequence of semiprimes having a unique decomposition as the sum of two primes begins with 4, 6, 9, 10, 14, 15, 22, 26, 34, 35, 38, 46, 58, 62, ... containing the even semiprimes (A100484).",
				"We observe a majority of terms where a(n) == 5 (mod 10)."
			],
			"link": [
				"Michel Marcus, \u003ca href=\"/A332981/b332981.txt\"\u003eTable of n, a(n) for n = 1..501\u003c/a\u003e"
			],
			"example": [
				"a(11) = 695 because 695 = 5*139 and the sum 5 + 139 = 144 = 5+139 = 7+137 = 13+131 = 17+127 = 31+113 = 37+107 = 41+103 = 43+101 = 47+97 = 61+83 = 71+73. There are exactly 11 decompositions of 144 into an unordered sum of two primes."
			],
			"maple": [
				"with(numtheory):",
				"for n from 1 to 50 do:",
				"ii:=0:",
				"for k from 2 to 10^8 while(ii=0) do:",
				"x:=factorset(k):it:=0:",
				"if bigomega(k) = 2",
				"  then",
				"   s:=x[1]+k/x[1]:",
				"    for m from 1 to s/2 do:",
				"     if isprime(m) and isprime(s-m)",
				"      then",
				"       it:=it+1:",
				"       else fi:",
				"     od:",
				"     if it = n",
				"     then",
				"      ii:=1: printf(`%d, `,k):",
				"     else fi:",
				"     fi:",
				"    od:",
				"    od:"
			],
			"program": [
				"(PARI) nbp(k) = {my(nb = 0); forprime(p=2, k\\2, if (isprime(k-p), nb++););nb;}",
				"a(n) = {forcomposite(k=1, oo, if (bigomega(k)==2, my(x=factor(k)[1,1]); if (nbp(x+k/x)==n, return(k));););} \\\\ _Michel Marcus_, Apr 26 2020"
			],
			"xref": [
				"Cf. A001358, A002375, A006881, A023036, A100484, A136244."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Michel Lagneau_, Mar 05 2020",
			"references": 1,
			"revision": 34,
			"time": "2021-06-30T01:53:09-04:00",
			"created": "2020-04-27T08:09:35-04:00"
		}
	]
}