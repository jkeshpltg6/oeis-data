{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A178548",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 178548,
			"data": "13,2,151,5,73,17,67,7,5,17,31,47,7,37,67,83,23,19,17,11,97,19,2,61,5,37,71,229,53,139,7,59,31,83,167,197,283,97,127,89,61,149,37,17,149,29,7,41,19,67,461,181,157,97,19,89,83,79,167,139,107,409,353,7,443,491",
			"name": "a(n) is the smallest prime p such that q = 2 * p + prime(n+2) is prime AND A007953(p) = A007953(q) where A007953 is sum of digits.",
			"comment": [
				"Prime q is sum of even semiprimes 2*p (A100484) and prime(i+2) (i = 1, 2, ...) (A000040).",
				"Equivalent conditions: q = 2*p + prime(i+2) \u003c=\u003e p = (q - prime(i+2))/2 \u003c=\u003e (q - prime(i+2))/p = 2.",
				"Algorithm: Q = prime(i+2) + 2*N, search first N with digsum(N) = digsum(Q);",
				"(a) if Q AND N prime: p = N;",
				"(b) if N composite: for n(k) = N + 9*k, q(k) = prime(i+2) + 2*(N + 9*k)",
				"(i) digsum(n(k)) = digsum(q(k)),",
				"(ii) n(k) has to be prime, let k = 1, 2, ... (Dirichlet's theorem on arithmetic progressions),",
				"(iii) q(k) has to be prime (k = 1, 2, ... , again Dirichlet's theorem on arithmetic progressions);",
				"Smallest k with (i), (ii) and (iii) gives term of sequence p = p(i).",
				"Each prime \u003e 3 is so \"associated\" uniquely with a pair (p,q) of primes.",
				"List of triples (prime(i+2), p, q) is given in link for 0 \u003c= i \u003c= 100.",
				"Pairs of primes (p, q) classified according to their digsum:",
				"2: (2,11) (11,101) (2,101)",
				"4: (13,31) (31,103) (31,211)",
				"5: (5,23) (5,41) (23,113) (5,113) (41,311)",
				"7: (151,313) (7,43) (7,61) (61,223) (7,151) (61,313) (7,241) (7,331)",
				"8: (17,53) (17,71) (71,251) (53,233) (17,233) (107,521)",
				"10: (73,163) (37,127) (19,109) (17,107) (19,127) (37,181) (127,433) (37,271) (19,271) (181,613) (19,307) (37,433)",
				"11: (47,137) (83,227) (83,317) (29,281) (461,1163) (83,443) (353,1019) (443,1217)",
				"13: (67,157) (67,193) (229,571) (139,409) (283,733) (67,373) (157,571) (139,571) (409,1129) (157,661)",
				"14: (59,257) (167,491) (149,419) (149,509) (167,617) (491,1319)",
				"16: (97,277) (97,367) (97,457) (79,439) (97,547)",
				"17: (197,557) (89,359) (89,449)",
				"Note: prime(i+2) is used in the sequence for 2 reasons: prime(i) could not be used for i=1 because q would be 2*p + prime(1) = 2*p + 2 that is not prime; prime(i+1) could not be used for i=1, because q would be 2*p + prime(2) = 2*p + 3, but numbers q with digsum(q) = digsum(p) are necessarily of form q = 9*k + 6 that is not prime."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A178548/b178548.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"G. C. Greubel, \u003ca href=\"/A178548/a178548_1.txt\"\u003eList of triples (prime(n+2), p=A178548(n), q=A178561(n)) for n=0..100\u003c/a\u003e"
			],
			"example": [
				"i=1: 2*13 + prime(3) = 26+5 = 31 = prime(11), digsum(13) = digsum(31) = 4, 13 is first term.",
				"i=2: 2*2 + prime(4) = 4+7 = 11 = prime(5), digsum(2) = digsum(11) = 2, 2 is 2nd term.",
				"i=3: 2*151 + prime(5) = 302+11 = 313 = prime(65), digsum(151) = digsum(313) = 7, 151 is 3rd term."
			],
			"mathematica": [
				"f[n_]:= Block[{p = 2}, While[q = 2*p +Prime[n+2]; !PrimeQ[q] ||",
				"(Total[IntegerDigits[p]] != Total[IntegerDigits[q]]), p = NextPrime[p]];",
				"Array[f, 100] (* _G. C. Greubel_, Feb 08 2019 *)"
			],
			"program": [
				"(PARI) a(n) = {my(p=2, q=prime(n+2)+2*p); while (!(isprime(q) \u0026\u0026 (sumdigits(p) == sumdigits(q))), p = nextprime(p+1); q = prime(n+2) + 2*p); p;} \\\\ _Michel Marcus_, Feb 09 2019"
			],
			"xref": [
				"Cf. A000040, A007953 (digsum), A100484, A178561 (primes q)."
			],
			"keyword": "base,nonn,less",
			"offset": "1,1",
			"author": "Ulrich Krug (leuchtfeuer37(AT)gmx.de), May 29 2010",
			"ext": [
				"Edited by _Michel Marcus_, Feb 28 2013",
				"a(37) and a(62) corrected by _Michel Marcus_, Feb 09 2019"
			],
			"references": 2,
			"revision": 42,
			"time": "2019-02-15T03:06:18-05:00",
			"created": "2010-07-11T03:00:00-04:00"
		}
	]
}