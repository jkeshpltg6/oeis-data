{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A329101",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 329101,
			"data": "0,2,1,3,6,10,8,9,4,11,5,7,12,14,13,15,18,26,22,24,34,42,38,40,25,41,27,30,32,43,33,35,16,36,17,19,37,46,39,44,20,45,21,23,28,47,29,31,48,50,49,51,54,58,56,57,52,59,53,55,60,62,61,63,66,74,70,72",
			"name": "Lexicographically earliest sequence of distinct nonnegative integers such that for any n \u003e= 0, the number of 1's in the base 4 expansion of n equals the number of 2's in the base 4 expansion of a(n).",
			"comment": [
				"This sequence is a permutation of the nonnegative integers with inverse A329180.",
				"Apparently, fixed points correspond to A001196.",
				"The sequence has fractal features; for any k \u003e= 0, the set of points { (n, a(n)), n = 0..4^k-1 } is symmetrical relative to the line of equation y + x = 4^k - 1 (see scatterplots in Links section)."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A329101/b329101.txt\"\u003eTable of n, a(n) for n = 0..4095\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A329101/a329101_1.gp.txt\"\u003ePARI program for A329101\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A329101/a329101.png\"\u003eScatterplot of the sequence for n = 0..4^3-1\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A329101/a329101_1.png\"\u003eScatterplot of the sequence for n = 0..4^10-1\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A329101/a329101_2.png\"\u003eColored scatterplot of the sequence for n = 0..4^10-1\u003c/a\u003e (where the color is function of A160381(n))",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"A160381(n) = A160382(a(n))."
			],
			"example": [
				"The first terms, alongside the base 4 representations of n and of a(n), are:",
				"  n   a(n)  qua(n)  qua(a(n))",
				"  --  ----  ------  ---------",
				"   0     0       0          0",
				"   1     2       1          2",
				"   2     1       2          1",
				"   3     3       3          3",
				"   4     6      10         12",
				"   5    10      11         22",
				"   6     8      12         20",
				"   7     9      13         21",
				"   8     4      20         10",
				"   9    11      21         23",
				"  10     5      22         11",
				"  11     7      23         13",
				"  12    12      30         30",
				"  13    14      31         32",
				"  14    13      32         31",
				"  15    15      33         33"
			],
			"program": [
				"(PARI) See Links section."
			],
			"xref": [
				"Cf. A001196, A160381, A160382, A329180."
			],
			"keyword": "nonn,base",
			"offset": "0,2",
			"author": "_Rémy Sigrist_, Nov 07 2019",
			"references": 2,
			"revision": 24,
			"time": "2019-11-08T09:46:49-05:00",
			"created": "2019-11-07T21:10:57-05:00"
		}
	]
}