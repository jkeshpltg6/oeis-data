{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A112555",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 112555,
			"data": "1,1,1,-1,0,1,1,1,1,1,-1,-2,-2,0,1,1,3,4,2,1,1,-1,-4,-7,-6,-3,0,1,1,5,11,13,9,3,1,1,-1,-6,-16,-24,-22,-12,-4,0,1,1,7,22,40,46,34,16,4,1,1,-1,-8,-29,-62,-86,-80,-50,-20,-5,0,1,1,9,37,91,148,166,130,70,25,5,1,1,-1,-10,-46,-128,-239,-314,-296,-200,-95,-30,-6,0",
			"name": "Triangle T, read by rows, such that the m-th matrix power satisfies T^m = I + m*(T - I) and consequently the matrix logarithm satisfies log(T) = T - I, where I is the identity matrix.",
			"comment": [
				"Signed version of A108561. Row sums equal A084247. The n-th unsigned row sum = A001045(n) + 1 (Jacobsthal numbers). Central terms of even-indexed rows are a signed version of A072547. Sums of squared terms in rows yields A112556, which equals the first differences of the unsigned central terms.",
				"Equals row reversal of triangle A112468 up to sign, where A112468 is the Riordan array (1/(1-x),x/(1+x)). - _Paul D. Hanna_, Jan 20 2006",
				"The elements here match A108561 in absolute value, but the signs are crucial to the properties that the matrix A112555 exhibits; the main property being T^m = I + m*(T - I). This property is not satisfied by A108561. - _Paul D. Hanna_, Nov 10 2009",
				"Eigensequence of the triangle = A140165. - _Gary W. Adamson_, Jan 30 2009",
				"Triangle T(n,k), read by rows, given by [1,-2,0,0,0,0,0,0,0,...] DELTA [1,0,-1,0,0,0,0,0,0,...] where DELTA is the operator defined in A084938. - _Philippe Deléham_, Sep 17 2009"
			],
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A112555/b112555.txt\"\u003eTable of n, a(n) for n = 0..1080\u003c/a\u003e"
			],
			"formula": [
				"G.f.: 1/(1-x*y) + x/((1-x*y)*(1+x+x*y)).",
				"The m-th matrix power T^m has the g.f.: 1/(1-x*y) + m*x/((1-x*y)*(1+x+x*y)).",
				"Recurrence: T(n, k) = [T^-1](n-1, k) + [T^-1](n-1, k-1), where T^-1 is the matrix inverse of T.",
				"Sum_{k=0..n} T(n,k)*x^(n-k) = A165760(n), A165759(n), A165758(n), A165755(n), A165752(n), A165746(n), A165751(n), A165747(n), A000007(n), A000012(n), A084247(n), A165553(n), A165622(n), A165625(n), A165638(n), A165639(n), A165748(n), A165749(n), A165750(n) for x= -9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9 respectively. - _Philippe Deléham_, Oct 07 2009",
				"Sum_{k=0..n} T(n,k)*x^k = A166157(n), A166153(n), A166152(n), A166149(n), A166036(n), A166035(n), A091004(n+1), A077925(n), A000007(n), A165326(n), A084247(n), A165405(n), A165458(n), A165470(n), A165491(n), A165505(n), A165506(n), A165510(n), A165511(n) for x = -9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9 respectively. - _Philippe Deléham_, Oct 08 2009"
			],
			"example": [
				"Triangle T begins:",
				"   1;",
				"   1,   1;",
				"  -1,   0,   1;",
				"   1,   1,   1,   1;",
				"  -1,  -2,  -2,   0,   1;",
				"   1,   3,   4,   2,   1,   1;",
				"  -1,  -4,  -7,  -6,  -3,   0,   1;",
				"   1,   5,  11,  13,   9,   3,   1,   1;",
				"  -1,  -6, -16, -24, -22, -12,  -4,   0,   1;",
				"   1,   7,  22,  40,  46,  34,  16,   4,   1,   1;",
				"  -1,  -8, -29, -62, -86, -80, -50, -20,  -5,   0,   1;",
				"  ...",
				"Matrix log, log(T) = T - I, begins:",
				"   0;",
				"   1,  0;",
				"  -1,  0,  0;",
				"   1,  1,  1,  0;",
				"  -1, -2, -2,  0,  0;",
				"   1,  3,  4,  2,  1,  0;",
				"  -1, -4, -7, -6, -3,  0,  0;",
				"  ...",
				"Matrix inverse, T^-1 = 2*I - T, begins:",
				"   1;",
				"  -1,  1;",
				"   1,  0,  1;",
				"  -1, -1, -1,  1;",
				"   1,  2,  2,  0,  1;",
				"  -1, -3, -4, -2, -1,  1;",
				"  ...",
				"where adjacent sums in row n of T^-1 gives row n+1 of T."
			],
			"mathematica": [
				"Clear[t]; t[0, 0] = 1; t[n_, 0] = (-1)^(Mod[n, 2]+1); t[n_, n_] = 1; t[n_, k_] /; k == n-1 := t[n, k] = Mod[n, 2]; t[n_, k_] /; 0 \u003c k \u003c n-1 := t[n, k] = -t[n-1, k] - t[n-1, k-1]; Table[t[n, k], {n, 0, 13}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Mar 06 2013 *)"
			],
			"program": [
				"(PARI) {T(n,k)=local(x=X+X*O(X^n),y=Y+Y*O(Y^k)); polcoeff( polcoeff( (1+2*x+x*y)/((1-x*y)*(1+x+x*y)),n,X),k,Y)}",
				"for(n=0,12, for(k=0,n, print1(T(n,k),\", \"));print(\"\"))",
				"(PARI) {T(n,k)=local(m=1,x=X+X*O(X^n),y=Y+Y*O(Y^k)); polcoeff(polcoeff(1/(1-x*y) + m*x/((1-x*y)*(1+x+x*y)),n,X),k,Y)}",
				"for(n=0,12, for(k=0,n, print1(T(n,k),\", \"));print(\"\"))",
				"(Sage)",
				"def A112555_row(n):",
				"    @cached_function",
				"    def prec(n, k):",
				"        if k==n: return 1",
				"        if k==0: return 0",
				"        return -prec(n-1,k-1)-sum(prec(n,k+i-1) for i in (2..n-k+1))",
				"    return [(-1)^(n-k+1)*prec(n+1, k) for k in (1..n+1)]",
				"for n in (0..12): print(A112555_row(n)) # _Peter Luschny_, Mar 16 2016"
			],
			"xref": [
				"Cf. A108561, A084247, A001045, A072547, A112556.",
				"Cf. A112468 (reversed rows).",
				"Cf. A140165. - _Gary W. Adamson_, Jan 30 2009"
			],
			"keyword": "sign,tabl",
			"offset": "0,12",
			"author": "_Paul D. Hanna_, Sep 21 2005",
			"references": 46,
			"revision": 33,
			"time": "2021-06-05T06:09:34-04:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}