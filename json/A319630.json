{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A319630",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 319630,
			"data": "1,2,3,4,5,7,8,9,10,11,13,14,16,17,19,20,21,22,23,25,26,27,28,29,31,32,33,34,37,38,39,40,41,43,44,46,47,49,50,51,52,53,55,56,57,58,59,61,62,63,64,65,67,68,69,71,73,74,76,79,80,81,82,83,85,86,87",
			"name": "Positive numbers that are not divisible by two consecutive prime numbers.",
			"comment": [
				"This sequence is the complement of A104210.",
				"Equivalently, this sequence corresponds to the positive numbers k such that:",
				"- A300820(k) \u003c= 1,",
				"- A087207(k) is a Fibbinary number (A003714).",
				"For any n \u003e 0 and k \u003e= 0, a(n)^k belongs to the sequence.",
				"The numbers of terms not exceeding 10^k, for k=1,2,..., are 9, 78, 758, 7544, 75368, 753586, 7535728, 75356719, 753566574, ... Apparently, the asymptotic density of this sequence is 0.75356... - _Amiram Eldar_, Apr 10 2021"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A319630/b319630.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"A300820(a(n)) \u003c= 1."
			],
			"example": [
				"The number 10 is only divisible by 2 and 5, hence 10 appears in the sequence.",
				"The number 42 is divisible by 2 and 3, hence 42 does not appear in the sequence."
			],
			"maple": [
				"N:= 1000: # for terms \u003c= N",
				"R:= {}:",
				"p:= 2:",
				"do",
				"  q:= p; p:= nextprime(p);",
				"  if p*q \u003e N then break fi;",
				"  R:= R union {seq(i,i=p*q..N,p*q)}",
				"od:",
				"sort(convert({$1..N} minus R,list)); # _Robert Israel_, Apr 13 2020"
			],
			"mathematica": [
				"q[n_] := SequenceCount[FactorInteger[n][[;; , 1]], {p1_, p2_} /; p2 == NextPrime[p1]] ==  0; Select[Range[100], q] (* _Amiram Eldar_, Apr 10 2021 *)"
			],
			"program": [
				"(PARI) is(n) = my (f=factor(n)); for (i=1, #f~-1, if (nextprime(f[i,1]+1)==f[i+1,1], return (0))); return (1)"
			],
			"xref": [
				"Cf. A003714, A006094, A087207, A104210, A300820."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Rémy Sigrist_, Sep 25 2018",
			"references": 13,
			"revision": 15,
			"time": "2021-04-10T05:53:55-04:00",
			"created": "2018-09-27T17:57:32-04:00"
		}
	]
}