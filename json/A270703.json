{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A270703",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 270703,
			"data": "1,4,41,670,15717,492112,19610565,961547874,56562256041,3914022281500,313638627550657,28730918805512678,2976543225606178893,345587228510915829224,44615408909143456529309,6361213086726610526079402,995709801367376369056571089",
			"name": "Total sum of the sizes of all blocks with maximal element n in all set partitions of {1,2,...,2n-1}.",
			"comment": [
				"Also total sum of the sizes of all blocks with minimal element n in all set partitions of {1,2,...,2n-1}."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A270703/b270703.txt\"\u003eTable of n, a(n) for n = 1..200\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Partition_of_a_set\"\u003ePartition of a set\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A270701(2n-1,n) = A270702(2n-1,n)."
			],
			"example": [
				"a(2) = 4 = 0+2+1+0+1 = sum of the sizes of all blocks with maximal element 2 in all set partitions of {1,2,3}: 123, 12|3, 13|2, 1|23, 1|2|3."
			],
			"maple": [
				"b:= proc(n, m, t) option remember; `if`(n=0, [1, 0], add(",
				"     `if`(t=1 and j\u003c\u003em+1, 0, (p-\u003ep+`if`(j=-t or t=1 and j=m+1,",
				"      [0, p[1]], 0))(b(n-1, max(m, j), `if`(t=1 and j=m+1, -j,",
				"     `if`(t\u003c0, t, `if`(t\u003e0, t-1, 0)))))), j=1..m+1))",
				"    end:",
				"a:= n-\u003e b(2*n-1, 0, n)[2]:",
				"seq(a(n), n=1..20);"
			],
			"mathematica": [
				"b[n_, m_, t_] := b[n, m, t] = If[n==0, {1, 0}, Sum[If[t==1 \u0026\u0026 j != m+1, 0, Function[p, p+If[j == -t || t == 1 \u0026\u0026 j == m+1, {0, p[[1]]}, 0]][b[n-1, Max[m, j], If[t == 1 \u0026\u0026 j == m+1, -j, If[t\u003c0, t, If[t\u003e0, t-1, 0]]]]]], {j, 1, m+1}]]; a[n_] := b[2*n-1, 0, n][[2]]; Table[a[n], {n, 1, 20}] (* _Jean-François Alcover_, Feb 15 2017, translated from Maple *)"
			],
			"xref": [
				"Cf. A000110, A270701, A270702."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Alois P. Heinz_, Mar 21 2016",
			"references": 3,
			"revision": 16,
			"time": "2017-02-15T11:27:10-05:00",
			"created": "2016-03-22T13:31:11-04:00"
		}
	]
}