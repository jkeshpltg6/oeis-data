{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A129118",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 129118,
			"data": "0,1,2,10,48,295,2068,16654,150382,1508500,16631696,199966907,2603709640,36501212971,548150650582,8779185528284,149376644391508,2690852138104504,51161190374132154,1023850096381041159,21512688329462044264",
			"name": "For each permutation p of {1,2,...,n} define minabsjump(p) = min(|p(i) - i|, 1\u003c=i\u003c=n); a(n) is the sum of minabsjumps of all p.",
			"formula": [
				"a(n) = Sum_{k=1..floor(n/2)} k * A299789(n,k). - _Alois P. Heinz_, Jan 21 2019"
			],
			"maple": [
				"n:=8; with(combinat); P:=permute(n); ct:= 0; for j to factorial(n) do ct:= ct+min(seq(abs(P[j][i]-i),i=1..n)) end do: ct; # yields a(n) for the specified n - _Emeric Deutsch_, Aug 24 2007",
				"# second Maple program:",
				"b:= proc(s) option remember; (n-\u003e `if`(n=1, x^(s[]-1), add((p-\u003e",
				"      add(coeff(p, x, i)*x^min(i, abs(n-j)), i=0..degree(p)))(",
				"        b(s minus {j})), j=s)))(nops(s))",
				"    end:",
				"a:= n-\u003e (p-\u003e add(coeff(p, x, i)*i, i=1..n-1))(b({$1..n})):",
				"seq(a(n), n=1..15);  # _Alois P. Heinz_, Jan 21 2019"
			],
			"mathematica": [
				"b[s_] := b[s] = Function[n, If[n == 1, x^(s - 1), Sum[Function[p, Sum[ SeriesCoefficient[p, {x, 0, i}]*x^Min[i, Abs[n - j]], {i, 0, Exponent[p, x]}]][b[s ~Complement~ {j}]], {j, s}]]][Length[s]] // Expand;",
				"a[n_] := a[n] = If[n == 1, 0, Function[p, Sum[SeriesCoefficient[p, {x, 0, i}]*i, {i, 1, n - 1}]][b[Range[n]][[1]]]];",
				"Table[Print[n, \" \", a[n]]; a[n], {n, 1, 12}] (* _Jean-François Alcover_, May 21 2020, after 2nd Maple program *)"
			],
			"program": [
				"(C++) #include \u003ciostream\u003e #include \u003cvector\u003e #include \u003calgorithm\u003e using namespace std; inline int mabsj(const vector\u003cint\u003e \u0026 s) { const int n = s.size() ; int mi =n ; for(int i=0; i\u003cn; i++) { const int thisdiff = abs(s[i]-i-1) ; if ( thisdiff \u003c mi ) mi=thisdiff ; } return mi ; } int main(int argc, char *argv[]) { for(int n=1 ;;n++) { vector\u003cint\u003e s; for(int i=1;i\u003c=n;i++) s.push_back(i) ; unsigned long long resul=0 ; do { resul += mabsj(s) ; } while( next_permutation(s.begin(),s.end()) ) ; cout \u003c\u003c n \u003c\u003c \" \" \u003c\u003c resul \u003c\u003c endl ; } return 0 ; } /* _R. J. Mathar_, Nov 01 2007 */"
			],
			"xref": [
				"Cf. A130153, A018927, A299789."
			],
			"keyword": "more,nonn",
			"offset": "1,3",
			"author": "_Emeric Deutsch_, _Vladeta Jovovic_, Jun 07 2007",
			"ext": [
				"One more term from _Emeric Deutsch_, Aug 24 2007",
				"a(11)-a(13) from _R. J. Mathar_, Nov 01 2007",
				"a(14) from _Donovan Johnson_, Sep 24 2010",
				"a(15)-a(21) from _Alois P. Heinz_, Jan 21 2019"
			],
			"references": 1,
			"revision": 24,
			"time": "2020-05-21T09:57:51-04:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}