{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A161820",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 161820,
			"data": "1,1,3,1,5,3,7,1,5,5,11,3,11,7,15,1,5,5,11,5,21,11,23,3,11,11,27,7,23,15,31,1,5,5,11,5,21,11,23,5,21,21,43,11,43,23,47,3,11,11,27,11,43,27,55,7,23,23,55,15,47,31,63,1,5,5,11,5,21,11,23,5,21,21,43,11,43,23,47,5",
			"name": "a(n) is the smallest positive integer such that both n and a(n), when represented in binary, contain the same types of runs of 1's, the runs being in any order.",
			"comment": [
				"Clarification of definition: Think of binary n and a(n) each as a string of 0's and 1's. Consider the \"runs\" of 1's in binary n and a(n), where each run is made up completely of 1's, and is bounded on both sides either by 0's or by the edge of the string. Now consider the lengths of each bounded run of 1's (the number of 1's in each run). Then a(n) is the smallest positive integer whose set of run-lengths is a permutation of the set of run-lengths for n. (See example.)"
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A161820/b161820.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"77 in binary is 1001101. There are three runs of 1's, two runs of one 1 each and one run of two 1's. So we are looking for the smallest positive integer with two runs of one 1 each and one run of two 1's (and no other runs of 1's). For example, 15 in binary is 1111, which contains the runs, except that it is required that each run be bounded by 0's or the edge of the binary string. The next number that fits the requirements completely is 43 = 101011 in binary. So a(77) = 43."
			],
			"maple": [
				"rtype := proc(n) local rt, bdgs,pr,i,rl ; rt := [seq(0,i=1..40)] ; bdgs := convert(n,base,2) ; pr := 0 ; for i from 1 to nops(bdgs) do if op(i,bdgs) = 1 then if pr = 0 then rl := 0 ; fi; rl := rl+1 ; else if pr = 1 then rt := subsop(rl=op(rl,rt)+1,rt) ; fi; fi; pr := op(i,bdgs) ; if i = nops(bdgs) and pr = 1 then rt := subsop(rl=op(rl,rt)+1,rt) ; fi; od: rt ; end: A161820 := proc(n) local rtn,a; rtn := rtype(n) ; for a from 1 do if rtype(a) = rtn then RETURN(a) ; fi; od: end: seq(A161820(n),n=1..100) ; # _R. J. Mathar_, Jul 20 2009"
			],
			"mathematica": [
				"f[n_] := Sort@ Map[Length, Select[Split@ IntegerDigits[n, 2], First@ # == 1 \u0026]]; Table[Block[{k = 1}, While[f@ k != f@ n, k++]; k], {n, 69}] (* _Michael De Vlieger_, Aug 30 2017 *)"
			],
			"xref": [
				"Cf. A161819, A161821, A161822."
			],
			"keyword": "base,nonn",
			"offset": "1,3",
			"author": "_Leroy Quet_, Jun 20 2009",
			"ext": [
				"More terms from _R. J. Mathar_, Jul 20 2009"
			],
			"references": 5,
			"revision": 13,
			"time": "2019-03-02T23:12:42-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}