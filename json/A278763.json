{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A278763",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 278763,
			"data": "1,1,1,1,2,2,1,2,4,2,1,2,5,6,3,1,2,5,8,9,3,1,2,5,9,14,12,4,1,2,5,9,16,20,16,4,1,2,5,9,17,25,30,20,5,1,2,5,9,17,27,39,40,25,5,1,2,5,9,17,28,44,56,55,30,6,1,2,5,9,17,28,46,65,80,70,36",
			"name": "Triangular array: row n shows the number of edges in successive levels of a graph of the partitions of n; see Comments.",
			"comment": [
				"The k-th number in row n (with rows numbered 2,3,4,...) is the number of edges from partitions of n into k parts to partitions of n into k-1 parts, for k = n..2, where partitions p and q share an edge if p has one more part than q, and exactly one part of p is a sum of two parts of q. The limiting row is A000097, which also gives the row sums."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A278763/b278763.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e"
			],
			"example": [
				"First 9 rows (for n = 2 to 10):",
				"  1;",
				"  1,  1;",
				"  1,  2,  2;",
				"  1,  2,  4,  2;",
				"  1,  2,  5,  6,  3;",
				"  1,  2,  5,  8,  9,  3;",
				"  1,  2,  5,  9, 14, 12,  4;",
				"  1,  2,  5,  9, 16, 20, 16,  4;",
				"  1,  2,  5,  9, 17, 25, 30, 20,  5;",
				"  1,  2,  5,  9, 17, 27, 39, 40, 25,  5;",
				"(See also the Example at A278762, for n = 5.)"
			],
			"mathematica": [
				"p[n_] := p[n] = IntegerPartitions[n];",
				"s[n_, k_] := s[n, k] = Select[p[n], Length[#] == k \u0026];",
				"x[n_, k_] := x[n, k] = Map[Length, Map[Union, s[n, k]]];",
				"b[h_] := b[h] = h (h - 1)/2;",
				"e[n_, k_] := e[n, k] = Total[Map[b, x[n, k]]];",
				"Flatten[Table[e[n, k], {n, 2, 20}, {k, 2, n - 1}]]  (* A278762 sequence *)",
				"TableForm[Table[e[n, k], {n, 2, 20}, {k, 2, n - 1}]]  (* A278762 triangle *)",
				"Flatten[Table[e[n, k], {n, 2, 20}, {k, n - 1, 2, -1}]]  (* A278763 sequence *)",
				"TableForm[Table[e[n, k], {n, 2, 20}, {k, n - 1, 2, -1}]]  (* A278763 triangle *)"
			],
			"xref": [
				"Cf. A000041, A000097 (row sums), A278762."
			],
			"keyword": "nonn,easy",
			"offset": "1,5",
			"author": "_Clark Kimberling_, Nov 30 2016",
			"references": 2,
			"revision": 6,
			"time": "2016-12-03T11:52:35-05:00",
			"created": "2016-12-03T11:52:35-05:00"
		}
	]
}