{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A302717",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 302717,
			"data": "0,1,3,2,5,6,7,12,4,9,20,11,30,13,42,15,56,8,17,72,19,90,10,21,110,23,132,25,156,27,182,14,29,210,31,240,16,33,272,35,306,18,37,342,39,380,41,420,43,462,22,45,506,47,552,24,49,600,51,650,26,53,702,55,756,28,57,812,59,870,61,930,63,992,32,65,1056,67,1122,34,69,1190",
			"name": "Start with a(0) = 0, then append the terms in [x, 2*x+1, x*(x+1)] which do not occur earlier, for x = 1, 2, ...",
			"comment": [
				"A permutation of the nonnegative integers.",
				"If a(n) is in A024701 (i.e., of the form (prime^2-1)/4), then a(n-1) is prime. Indeed, A024701(m) = k*(k+1) with k = (prime(m+1)-1)/2, and any term k*(k+1) \u003e 0 is preceded by 2*k+1 = prime(m+1). [Edited and proof added by _M. F. Hasler_, Apr 13 2018]",
				"The term x*(x+1) will always be appended since it is larger than all preceding terms (except for x = 1), and also 2*x+1 cannot occur earlier because it is odd while x*(x+1) is always even. So only the term x will be inserted (or not) in a somewhat irregular pattern, namely whenever x is an even but not oblong number (A002378). We see that this is the case for x = 4, 8, 10, 14, 16, 18, 22, ...; recognizable by the fact that a(n) = (a(n+1)-1)/2 and equivalently, there are two and not only one smaller number between two larger \"records\" x*(x+1).",
				"If we count the terms added from each 4-tuple during each iteration we find that either two or three terms are added: 2, 2, 2, 3, 2, 2, 2, 3, 2, 3, 2, 2, 2, 3, ... where the set of three twos (2, 2, 2) appears with decreasing frequency.",
				"A302906 is the sequence of starting indices of these sets."
			],
			"link": [
				"J. Stauduhar, \u003ca href=\"http://list.seqfan.eu/pipermail/seqfan/2018-April/018577.html\"\u003epermutation of nonnegative integers\u003c/a\u003e, SeqFan list, Apr 11 2018",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"example": [
				"Repeatedly take consecutive numbers a and b and append to the sequence any of {a, a+b, a*b, b} not already in the sequence. Beginning with a=0 and b=1:",
				"(0,1) -\u003e {0, 0+1, 0*1, 1} -\u003e [0,1]",
				"(1,2) -\u003e {1, 1+2, 1*2, 2} -\u003e [0,1,3,2]",
				"(2,3) -\u003e {2, 2+3, 2*3, 3} -\u003e [0,1,3,2,5,6]",
				"(3,4) -\u003e {3, 3+4, 3*4, 4} -\u003e [0,1,3,2,5,6,7,12,4]",
				"etc.",
				"In the above construction, we always have b = a+1. Thus [a, a+b, a*b, b] = [a, 2*a+1, a*(a+1), a+1], and a simpler description is to consider only { a, 2*a+1, a*(a+1) }, the 4th term being equal to the 1st term of the next 4-tuple. To ensure we have a permutation of the integers \u003e= 0 starting at index 0 and not a list stating at index 1, we can fix a(0) = 0 explicitly and then go on with a = x = 1, 2, 3, ... to get the same sequence."
			],
			"program": [
				"(PARI) u=[];(do(x)=setsearch(u,x)||print1(x\",\")||u=setunion(u,[x]));for(a=0,199,do(a);do(2*a+1);do(a^2+a)) \\\\ _M. F. Hasler_, Apr 12 2018"
			],
			"xref": [
				"Cf. A000096, A002378, A024701."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_J. Stauduhar_, Apr 12 2018",
			"references": 1,
			"revision": 42,
			"time": "2019-11-11T00:51:59-05:00",
			"created": "2018-04-17T03:28:52-04:00"
		}
	]
}