{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A298016",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 298016,
			"data": "1,6,12,12,24,36,24,42,60,36,60,84,48,78,108,60,96,132,72,114,156,84,132,180,96,150,204,108,168,228,120,186,252,132,204,276,144,222,300,156,240,324,168,258,348,180,276,372,192,294,396,204,312,420,216,330,444,228,348,468,240",
			"name": "Coordination sequence of snub-632 tiling with respect to a hexavalent node.",
			"comment": [
				"The snub-632 tiling in also called the fsz-d net. It is the dual of the 3.3.3.3.6 Archimedean tiling.",
				"This is also called the \"6-fold pentille\" tiling in Conway, Burgiel, Goodman-Strauss, 2008, p. 288. - _Felix Fröhlich_, Jan 13 2018"
			],
			"reference": [
				"J. H. Conway, H. Burgiel and Chaim Goodman-Strauss, The Symmetries of Things, A K Peters, Ltd., 2008, ISBN 978-1-56881-220-5."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A298016/b298016.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Chaim Goodman-Strauss and N. J. A. Sloane, \u003ca href=\"https://doi.org/10.1107/S2053273318014481\"\u003eA Coloring Book Approach to Finding Coordination Sequences\u003c/a\u003e, Acta Cryst. A75 (2019), 121-134, also \u003ca href=\"http://NeilSloane.com/doc/Cairo_final.pdf\"\u003eon NJAS's home page\u003c/a\u003e. Also \u003ca href=\"http://arxiv.org/abs/1803.08530\"\u003earXiv:1803.08530\u003c/a\u003e.",
				"Chaim Goodman-Strauss and N. J. A. Sloane, \u003ca href=\"/A298016/a298016.png\"\u003eTrunks and branches structure for calculating this sequence\u003c/a\u003e",
				"Tom Karzes, \u003ca href=\"/A250122/a250122.html\"\u003eTiling Coordination Sequences\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/A296368/a296368_2.png\"\u003eOverview of coordination sequences of Laves tilings\u003c/a\u003e [Fig. 2.7.1 of Grünbaum-Shephard 1987 with A-numbers added and in some cases the name in the RCSR database]",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,0,2,0,0,-1)."
			],
			"formula": [
				"For n \u003e= 1, let k=floor(n/3). Then a(3*k) = 12*k, a(3*k+1)=18*k+6, a(3*k+2)=24*k+12.",
				"a(n) = 2*a(n-3) - a(n-6) for n \u003e= 7.",
				"G.f.: -(-x^6-12*x^5-12*x^4-10*x^3-12*x^2-6*x-1)/(x^6-2*x^3+1)."
			],
			"maple": [
				"f:=proc(n) local k,r;",
				"if n=0 then return(1); fi;",
				"r:=(n mod 3); k:=(n-r)/3;",
				"if r=0 then 12*k elif r=1 then 18*k+6 else 24*k+12; fi;",
				"end;",
				"[seq(f(n),n=0..80)];"
			],
			"mathematica": [
				"Join[{1}, LinearRecurrence[{0, 0, 2, 0, 0, -1}, {6, 12, 12, 24, 36, 24}, 60]] (* _Jean-François Alcover_, Apr 23 2018 *)"
			],
			"program": [
				"(PARI) Vec((1 + 6*x + 12*x^2 + 10*x^3 + 12*x^4 + 12*x^5 + x^6) / ((1 - x)^2*(1 + x + x^2)^2) + O(x^60)) \\\\ _Colin Barker_, Jan 13 2018"
			],
			"xref": [
				"Cf. A298014, A298015.",
				"List of coordination sequences for Laves tilings (or duals of uniform planar nets): [3,3,3,3,3.3] = A008486; [3.3.3.3.6] = A298014, A298015, A298016; [3.3.3.4.4] = A298022, A298024; [3.3.4.3.4] = A008574, A296368; [3.6.3.6] = A298026, A298028; [3.4.6.4] = A298029, A298031, A298033; [3.12.12] = A019557, A298035; [4.4.4.4] = A008574; [4.6.12] = A298036, A298038, A298040; [4.8.8] = A022144, A234275; [6.6.6] = A008458."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "Chaim Goodman-Strauss and _N. J. A. Sloane_, Jan 11 2018",
			"references": 22,
			"revision": 36,
			"time": "2020-04-04T01:39:14-04:00",
			"created": "2018-01-13T08:55:13-05:00"
		}
	]
}