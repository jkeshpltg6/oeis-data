{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A279030",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 279030,
			"data": "1,0,5,0,21,0,85,0,341,0,1365,0,5461,0,21845,0,87381,0,349525,0,1398101,0,5592405,0,22369621,0,89478485,0,357913941,0,1431655765,0,5726623061,0,22906492245,0,91625968981,0,366503875925,0,1466015503701,0,5864062014805",
			"name": "Decimal representation of the x-axis, from the left edge to the origin, of the n-th stage of growth of the two-dimensional cellular automaton defined by \"Rule 129\", based on the 5-celled von Neumann neighborhood.",
			"comment": [
				"Initialized with a single black (ON) cell at stage zero.",
				"Also, the decimal representation of the x-axis, from the origin to the right edge, of the n-th stage of growth of the two-dimensional cellular automaton defined by \"Rule 129\", based on the 5-celled von Neumann neighborhood.  Initialized with a single black (ON) cell at stage zero.",
				"A279030 is related to A000975 in such a way that a(2*n) = b(2*n + 1) and a(2*n + 1) = b(2*n + 2) - 2*b(2*n + 1) = b(2*n + 2) - 2*a(2*n), where b(2*n + 1) and b(2*n + 2) are members of A000975. - _Mario C. Enriquez_, Dec 07 2016"
			],
			"reference": [
				"S. Wolfram, A New Kind of Science, Wolfram Media, 2002; p. 170."
			],
			"link": [
				"Robert Price, \u003ca href=\"/A279030/b279030.txt\"\u003eTable of n, a(n) for n = 0..126\u003c/a\u003e",
				"Robert Price, \u003ca href=\"/A279030/a279030.tmp.txt\"\u003eDiagrams of first 20 stages\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"http://arxiv.org/abs/1503.01168\"\u003eOn the Number of ON Cells in Cellular Automata\u003c/a\u003e, arXiv:1503.01168 [math.CO], 2015.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ElementaryCellularAutomaton.html\"\u003eElementary Cellular Automaton\u003c/a\u003e",
				"S. Wolfram, \u003ca href=\"http://wolframscience.com/\"\u003eA New Kind of Science\u003c/a\u003e",
				"Wolfram Research, \u003ca href=\"http://atlas.wolfram.com/\"\u003eWolfram Atlas of Simple Programs\u003c/a\u003e",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_2D_5-Neighbor_Cellular_Automata\"\u003eIndex to 2D 5-Neighbor Cellular Automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_Elementary_Cellular_Automata\"\u003eIndex to Elementary Cellular Automata\u003c/a\u003e"
			],
			"formula": [
				"Conjectures from _Colin Barker_, Dec 04 2016: (Start)",
				"a(n) = (1 + (-1)^n)*(2^(2 + n)-1)/6.",
				"a(n) = 5*a(n-2) - 4*a(n-4) for n\u003e3.",
				"G.f.: 1 / ((1 - x)*(1 + x)*(1 - 2*x)*(1 + 2*x)).",
				"(End)",
				"Bisection appears to be A002450 ((4^n-1)/3). - _N. J. A. Sloane_, Dec 06 2016"
			],
			"mathematica": [
				"CAStep[rule_, a_] := Map[rule[[10 - #]] \u0026, ListConvolve[{{0, 2, 0},{2, 1, 2}, {0, 2, 0}}, a, 2],{2}];",
				"code = 129; stages = 128;",
				"rule = IntegerDigits[code, 2, 10];",
				"g = 2 * stages + 1; (* Maximum size of grid *)",
				"a = PadLeft[{{1}}, {g, g}, 0,Floor[{g, g}/2]]; (* Initial ON cell on grid *)",
				"ca = a;",
				"ca = Table[ca = CAStep[rule, ca], {n, 1, stages + 1}];",
				"PrependTo[ca, a];",
				"(* Trim full grid to reflect growth by one cell at each stage *)",
				"k = (Length[ca[[1]]] + 1)/2;",
				"ca = Table[Table[Part[ca[[n]] [[j]],Range[k + 1 - n, k - 1 + n]], {j, k + 1 - n, k - 1 + n}], {n, 1, k}];",
				"Table[FromDigits[Part[ca[[i]] [[i]], Range[1, i]], 2], {i, 1, stages - 1}]"
			],
			"xref": [
				"Cf. A000975, A002450, A279028."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Robert Price_, Dec 03 2016",
			"references": 2,
			"revision": 27,
			"time": "2016-12-08T10:34:52-05:00",
			"created": "2016-12-06T05:15:05-05:00"
		}
	]
}