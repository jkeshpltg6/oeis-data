{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A331962",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 331962,
			"data": "2,3,2,5,3,2,5,3,2,7,5,3,2,7,11,5,3,2,7,11,13,17,5,3,2,7,11,13,17,5,3,2,7,11,19,13,17,5,3,23,2,7,11,19,13,17,5,3,23,29,2,7,11,19,13,17,5,3,23,29,2,7,31,11,19,13,17,5,3,23,29,2,7,31,11,19,37,13",
			"name": "Triangle read by rows: row j lists j primes by ascending n bit bientropy (n \u003e= 8).",
			"comment": [
				"Primes are sorted by the relative order and disorder of their n binary digits as measured by the bientropy function. When two or more primes have the same bientropy, their natural order is preserved."
			],
			"reference": [
				"G. J. Croll, Bientropy - the measurement and algebras of order and disorder in finite binary strings. Scientific essays in honor of H. Pierre Noyes on the occasion of his 90th Birthday, World Scientific, 2014, pages 48-64."
			],
			"link": [
				"Grenville J. Croll, \u003ca href=\"https://arxiv.org/abs/1912.08051\"\u003eBiEntropy, TriEntropy and Primality\u003c/a\u003e, arXiv:1912.08051 [cs.OH], 2019.",
				"Grenville J. Croll, \u003ca href=\"https://www.mdpi.com/1099-4300/22/3/311\"\u003eBiEntropy, TriEntropy and Primality\u003c/a\u003e, Entropy 2020, 22, 311.",
				"Grenville J. Croll, \u003ca href=\"https://doi.org/10.6084/m9.figshare.11743749\"\u003eBiEntropy_TriEntropy_and_Primality.zip\u003c/a\u003e, Figshare Dataset, 2020."
			],
			"formula": [
				"Given a binary string s = s1...sn (where in this application n \u003e= 8), there are n-1 binary derivatives of s, Dk(s), 1 \u003c= k \u003c= n-1.",
				"The first binary derivative of s, D1(s), is the binary string of length n-1 formed by XORing adjacent pairs of digits of s.",
				"We refer to the k-th derivative of s, Dk(s), as the binary derivative of Dk-1(s).",
				"p(k) is the proportion of 1's in Dk.",
				"D0 = s;",
				"0*log_2(0) is defined to be 0;",
				"C = 1/(2^(n-1)-1);",
				"D = Sum_{k=0..n-2}(-p(k)*log_2(p(k))-(1-p(k))*log_2(1-p(k)))*2^k.",
				"bientropy(s) = C*D;"
			],
			"example": [
				"For example, the 8-bit bientropy of 17 (a Fermat prime) is 0.0534 as, in binary, it is a periodic binary number - 00010001. The bientropy of 13 is 0.9532 as its binary digits - 00001101 are not periodic. Each row j reveals where the j-th prime sits relative to the rest given its bientropy.",
				"Triangle begins:",
				"2,",
				"3, 2,",
				"5, 3, 2,",
				"5, 3, 2, 7,",
				"5, 3, 2, 7, 11",
				"5, 3, 2, 7, 11, 13",
				"17, 5, 3, 2, 7, 11, 13",
				"17, 5, 3, 2, 7, 11, 19, 13",
				"17, 5, 3, 23, 2, 7, 11, 19, 13",
				"17, 5, 3, 23, 29, 2, 7, 11, 19, 13",
				"17, 5, 3, 23, 29, 2, 7, 31, 11, 19, 13",
				"17, 5, 3, 23, 29, 2, 7, 31, 11, 19, 37, 13",
				"..."
			],
			"program": [
				"(Excel) See Grenville J. Croll Figshare link."
			],
			"xref": [
				"Cf. A000040, A019434."
			],
			"keyword": "tabl,easy,nonn,base",
			"offset": "1,1",
			"author": "_Grenville J. Croll_, Feb 02 2020",
			"references": 1,
			"revision": 46,
			"time": "2020-05-09T17:07:51-04:00",
			"created": "2020-05-09T17:07:51-04:00"
		}
	]
}