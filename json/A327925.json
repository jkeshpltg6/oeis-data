{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A327925",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 327925,
			"data": "1,1,1,2,1,2,1,2,1,3,1,2,1,2,2,2,1,4,1,4,1,2,2,2,1,4,1,2,1,3,1,2,1,2,2,2,1,2,1,4,1,4,1,2,2,3,1,4,1,3,2,2,1,6,1,2,2,2,1,4,1,4,1,6,1,4,1,6,1,2,1,3,1,2,1,4,1,2,1,3,1,2,1,5,1,2,2,2,1,4,1,2,2,2,1,4,1,2,3,2,1,4,1,2,2,2,1,6",
			"name": "Irregular table read by rows: T(m,n) is the number of non-isomorphic groups G such that G is the semidirect product of C_m and C_n, where C_m is a normal subgroup of G and C_n is a subgroup of G, 1 \u003c= n \u003c= A002322(m).",
			"comment": [
				"The semidirect product of C_m and C_n has group representation G = \u003cx, y|x^m = y^n = 1, yxy^(-1) = x^r\u003e, where r is any number such that r^n == 1 (mod m). Two groups G = \u003cx, y|x^m = y^n = 1, yxy^(-1) = x^r\u003e and G' = \u003cx, y|x^m = y^n = 1, yxy^(-1) = x^s\u003e are isomorphic if and only if there exists some k, gcd(k,n) = 1 such that r^k == s (mod m), in which case f(x^i*y^j) = x^i*y^(k*j) is an isomorphic mapping from G to G'.",
				"T(m,n) only depends on the value of gcd(n,psi(m)), psi = A002322 (Carmichael lambda). So each row of A327924 is periodic with period psi(m), so we have this for an alternative version.",
				"Every number k occurs in the table. By Dirichlet's theorem on arithmetic progressions, there exists a prime p such that p == 1 (mod 2^(k-1)), then T(p,2^(k-1)) = d(gcd(2^(k-1),p-1)) = k (see the formula below). For example, T(5,4) = 3, T(17,8) = 4, T(17,16) = 5, T(97,32) = 6, T(193,64) = 7, ..."
			],
			"link": [
				"Jianing Song, \u003ca href=\"/A327925/b327925.txt\"\u003eTable of n, a(n) for n = 1..8346\u003c/a\u003e (the first 200 rows)"
			],
			"formula": [
				"T(m,n) = Sum_{d|n} (number of elements x such that ord(x,m) = d)/phi(d), where ord(x,m) is the multiplicative order of x modulo m, phi = A000010.",
				"For odd primes p, T(p^e,n) = d(gcd(n,(p-1)*p^(e-1))) = A051194((p-1)*p^(e-1),n), d = A000005; for e \u003e= 3, T(2^e,n) = 2*(v2(n)+1) for even n and 1 for odd n, where v2 is the 2-adic valuation."
			],
			"example": [
				"Table starts",
				"m = 1: 1;",
				"m = 2: 1;",
				"m = 3: 1, 2;",
				"m = 4: 1, 2;",
				"m = 5: 1, 2, 1, 3;",
				"m = 6: 1, 2;",
				"m = 7: 1, 2, 2, 2, 1, 4;",
				"m = 8: 1, 4;",
				"m = 9: 1, 2, 2, 2, 1, 4;",
				"m = 10: 1, 2, 1, 3;",
				"m = 11: 1, 2, 1, 2, 2, 2, 1, 2, 1, 4;",
				"m = 12: 1, 4;",
				"m = 13: 1, 2, 2, 3, 1, 4, 1, 3, 2, 2, 1, 6;",
				"m = 14: 1, 2, 2, 2, 1, 4;",
				"m = 15: 1, 4, 1, 6;",
				"m = 16: 1, 4, 1, 6;",
				"m = 17: 1, 2, 1, 3, 1, 2, 1, 4, 1, 2, 1, 3, 1, 2, 1, 5;",
				"m = 18: 1, 2, 2, 2, 1, 4;",
				"m = 19: 1, 2, 2, 2, 1, 4, 1, 2, 3, 2, 1, 4, 1, 2, 2, 2, 1, 6;",
				"m = 20: 1, 4, 1, 6;",
				"Example shows that T(21,6) = 6: The semidirect product of C_21 and C_6 has group representation G = \u003cx, y|x^21 = y^6 = 1, yxy^(-1) = x^r\u003e, where r = 1, 2, 4, 5, 8, 10, 11, 13, 16, 17, 19, 20. Since 2^5 == 11 (mod 21), 4^5 == 16 (mod 21), 5^5 == 17 (mod 21), 10^5 == 19 (mod 21), there are actually four pairs of isomorphic groups, giving a total of 8 non-isomorphic groups."
			],
			"program": [
				"(PARI) numord(n,q) = my(v=divisors(q),r=znstar(n)[2]); sum(i=1,#v,prod(j=1,#r,gcd(v[i],r[j]))*moebius(q/v[i]))",
				"T(m,n) = my(u=divisors(n)); sum(i=1,#u,numord(m,u[i])/eulerphi(u[i]))",
				"Row(m) = my(l=if(m\u003e2,znstar(m)[2][1],1), R=vector(l,n,T(m,n))); R"
			],
			"xref": [
				"Cf. A327925, A002322, A000010, A000005.",
				"Cf. also A060594, A060839, A073103, A319099, A319100, A319101, A051194."
			],
			"keyword": "nonn,tabf",
			"offset": "1,4",
			"author": "_Jianing Song_, Sep 30 2019",
			"references": 2,
			"revision": 18,
			"time": "2019-11-10T07:12:06-05:00",
			"created": "2019-10-13T10:47:01-04:00"
		}
	]
}