{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A194452",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 194452,
			"data": "0,0,2,3,8,12,24,35,60,87,136,192,287,396,567,773,1074,1439,1958,2587,3454,4514,5931,7666,9951,12736,16341,20743,26354,33184,41807,52262,65329,81144,100721,124344,153390,188303,230940,282063,344100,418242,507762",
			"name": "Total number of repeated parts in all partitions of n.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A194452/b194452.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A006128(n) - A024786(n+1).",
				"a(n) = Sum_{k=2..n} k*A264405(n,k). - _Alois P. Heinz_, Dec 07 2015",
				"G.f.: g = Sum_{j\u003e0} (x^{2*j}*(2 - x^j)/(1-x^j))/Product_{k\u003e0}(1 - x^k) (obtained by logarithmic differentiation of the bivariate g.f. given in A264405). - _Emeric Deutsch_, Feb 02 2016"
			],
			"example": [
				"For n = 6 we have:",
				"--------------------------------------",
				".                        Number of",
				"Partitions             repeated parts",
				"--------------------------------------",
				"6 .......................... 0",
				"3 + 3 ...................... 2",
				"4 + 2 ...................... 0",
				"2 + 2 + 2 .................. 3",
				"5 + 1 ...................... 0",
				"3 + 2 + 1 .................. 0",
				"4 + 1 + 1 .................. 2",
				"2 + 2 + 1 + 1 .............. 4",
				"3 + 1 + 1 + 1 .............. 3",
				"2 + 1 + 1 + 1 + 1 .......... 4",
				"1 + 1 + 1 + 1 + 1 + 1 ...... 6",
				"------------------------------------",
				"Total ..................... 24",
				"So a(6) = 24."
			],
			"maple": [
				"b:= proc(n, i) option remember; local h, j, t;",
				"      if n\u003c0 then [0, 0]",
				"    elif n=0 then [1, 0]",
				"    elif i\u003c1 then [0, 0]",
				"    else h:= [0, 0];",
				"         for j from 0 to iquo(n, i) do",
				"           t:= b(n-i*j, i-1);",
				"           h:= [h[1]+t[1], h[2]+t[2]+`if`(j\u003c2, 0, t[1]*j)]",
				"         od; h",
				"      fi",
				"    end:",
				"a:= n-\u003e b(n, n)[2]:",
				"seq(a(n), n=0..50);  # _Alois P. Heinz_, Nov 20 2011",
				"g := add(x^(2*j)*(2-x^j)/(1-x^j), j = 1 .. 80)/mul(1-x^j, j = 1 .. 80): gser := series(g, x = 0, 50): seq(coeff(gser, x, n), n = 0 .. 45); # _Emeric Deutsch_, Feb 02 2016"
			],
			"mathematica": [
				"myCount[p_List] := Module[{t}, If[p == {}, 0, t = Transpose[Tally[p]][[2]]; Sum[If[t[[i]] == 1, 0, t[[i]]], {i, Length[t]}]]]; Table[Total[Table[myCount[p], {p, IntegerPartitions[i]}]], {i, 0, 20}] (* _T. D. Noe_, Nov 19 2011 *)",
				"b[n_, i_] := b[n, i] = Module[{h, j, t}, Which[n\u003c0, {0, 0}, n==0, {1, 0}, i \u003c 1, {0, 0}, True, h={0, 0}; For[j=0, j \u003c= Quotient[n, i], j++, t = b[n - i*j, i-1]; h = {h[[1]]+t[[1]], h[[2]]+t[[2]] + If[j\u003c2, 0, t[[1]]*j]}]; h] ]; a[n_] := b[n, n][[2]]; Table[a[n], {n, 0, 50}] (* _Jean-François Alcover_, Oct 25 2015, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A006128, A024786, A047967, A135010, A138121, A194544, A264405."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Omar E. Pol_, Nov 19 2011",
			"references": 5,
			"revision": 44,
			"time": "2017-06-16T02:47:17-04:00",
			"created": "2011-11-19T21:03:26-05:00"
		}
	]
}