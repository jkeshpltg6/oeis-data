{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A108730",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 108730,
			"data": "0,1,0,0,2,1,0,0,1,0,0,0,3,2,0,1,1,1,0,0,0,2,0,1,0,0,0,1,0,0,0,0,4,3,0,2,1,2,0,0,1,2,1,1,0,1,0,1,1,0,0,0,0,3,0,2,0,0,1,1,0,1,0,0,0,0,2,0,0,1,0,0,0,0,1,0,0,0,0,0,5,4,0,3,1,3,0,0,2,2,2,1,0,2,0,1,2,0,0,0,1,3,1,2,0",
			"name": "Triangle read by rows: row n gives list of number of zeros following each 1 in binary representation of n.",
			"comment": [
				"This is probably the simplest method for putting the nonnegative integers into one-to-one correspondence with the finite sequences of nonnegative integers and is the standard ordering for such sequences in this database.",
				"This sequence contains every finite sequence of nonnegative integers.",
				"This can be regarded as a table in two ways: with each weak composition as a row, or with the weak compositions of each integer as a row. The first way has A000120 as row lengths and A080791 as row sums; the second has A001792 as row lengths and A001787 as row sums. - _Franklin T. Adams-Watters_, Nov 06 2006",
				"Concatenate the base-two positive integers (A030190 less the initial zero). Left to right and disallowing leading zeros, reorganize the digits into the smallest possible numbers. These will be the base-two powers-of-two of A108730. - _Hans Havermann_, Nov 14 2009",
				"T(2^(n-1),0) = n-1 and T(m,k) \u003c n-1 for all m \u003c 2^n, k \u003c= A000120(m). When the triangle is seen as a flattened list, each n occurs first at position n*2^(n-1)+1, cf. A005183. - _Reinhard Zumkeller_, Feb 26 2012",
				"Equals A066099-1, elementwise. - _Andrey Zabolotskiy_, May 18 2018"
			],
			"link": [
				"Franklin T. Adams-Watters, \u003ca href=\"/A108730/b108730.txt\"\u003eTable of n, a(n) for n = 1..5120 (through 10 bit numbers)\u003c/a\u003e"
			],
			"example": [
				"Triangle begins:",
				"0",
				"1",
				"0,0",
				"2",
				"1,0",
				"0,1",
				"0,0,0",
				"3",
				"For example, 25 base 2 = 11001; following the 1's are 0, 2 and 0 zeros, so row 25 is 0, 2, 0."
			],
			"mathematica": [
				"row[n_] := (id = IntegerDigits[n, 2]; sp = Split[id]; f[run_List] := If[First[run] == 0, run, Sequence @@ Table[{}, {Length[run] - 1}]]; len = Length /@ f /@ sp; If[Last[id] == 0, len, Append[len, 0]]); Flatten[ Table[row[n], {n, 1, 41}]] (* _Jean-François Alcover_, Jul 13 2012 *)"
			],
			"program": [
				"(Haskell)",
				"import Data.List (unfoldr, group)",
				"a108730 n k = a108730_tabf !! (n-1) !! (k-1)",
				"a108730_row = f . group . reverse . unfoldr",
				"   (\\x -\u003e if x == 0 then Nothing else Just $ swap $ divMod x 2) where",
				"   f [] = []",
				"   f [os] = replicate (length os) 0",
				"   f (os:zs:dss) = replicate (length os - 1) 0 ++ [length zs] ++ f dss",
				"a108730_tabf = map a108730_row [1..]",
				"a108730_list = concat a108730_tabf",
				"-- _Reinhard Zumkeller_, Feb 26 2012",
				"(PARI) row(n)=my(v=vector(hammingweight(n)),t=n); for(i=0,#v-1,v[#v-i] = valuation(t,2); t\u003e\u003e=v[#v-i]+1); v \\\\ _Charles R Greathouse IV_, Sep 14 2015"
			],
			"xref": [
				"Cf. A066099 (main entry for compositions), A007088, A000120, A080791, A001792, A001787, A124735."
			],
			"keyword": "easy,base,nice,nonn,tabf",
			"offset": "1,5",
			"author": "_Franklin T. Adams-Watters_, Jun 22 2005",
			"ext": [
				"Edited by _Franklin T. Adams-Watters_, Nov 06 2006"
			],
			"references": 7,
			"revision": 31,
			"time": "2020-05-20T15:17:20-04:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}