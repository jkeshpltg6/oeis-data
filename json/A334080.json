{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A334080",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 334080,
			"data": "1,2,2,3,2,4,2,4,3,4,2,6,3,4,4,5,2,6,2,6,4,4,2,8,3,6,4,6,2,8,2,6,4,5,4,9,2,4,6,8,2,8,2,6,6,4,2,10,3,6,4,9,2,8,4,8,4,4,2,12,2,4,6,7,6,8,2,8,4,9,2,12,2,4,6,6,4,12,2,10,5,4,2,12,4",
			"name": "Numbers of Pythagorean triples contained in the divisors of the multiples of 60.",
			"comment": [
				"The odd numbers of the sequence are rare (see the table below).",
				"The subsequence of odd terms begins with 1, 3, 3, 3, 5, 3, 5, 9, 3, 9, 7, 9, 5, 9, 9, 3, 11, 15, 5, 9, 5, 15, 9, 9, 9, 5, 19, 3, 15, 15, 9,...(see the table in the link).",
				"It is interesting to note that each set of divisors of A169823(n) contains m primitive Pythagorean triples for some n, m = 1, 2,...",
				"Examples:",
				"- The set of divisors of A169823(1)= 60 contains only one primitive Pythagorean triple: (3, 4, 5).",
				"- The set of divisors of A169823(136) = 8160 contains two primitive Pythagorean triples: (3, 4, 5) and (8, 15, 17).",
				"- The set of divisors of A169823(910) = 54600 contains three primitive Pythagorean triples: (3, 4, 5), (5, 12, 13) and (7, 24, 25).",
				"There is an interesting property: we observe that a(n) = A000005(n) except for n in the set {13, 26, 34, 39, 52, 65, 68, 70, 78, 91, 102, ...}. This set contains subset of numbers of the form 13*k, 34*k, 70*k, 203*k, 246*k, 259*k,... for k = 1, 2, ...",
				"We recognize the sequence A081752: {13, 34, 70, 203, 246, 259, 671,...} (ordered product of the sides of primitive Pythagorean triangles divided by 60).",
				"The following table shows the numbers of odd terms \u003c 10^k for k = 2, 3, 4, 5, 6 and 7. For instance, among the 16 multiples of 60 less than 10^3, the divisors of the five numbers 60, 240, 540, 780 and 960 contain 1, 3, 3, 3 and 5 Pythagorean triples respectively, and that represents 31.25% of odd numbers.",
				"+---------------+-----------------+---------------------+-----------+",
				"|   Intervals   |    Number of    | Number of odd terms |           |",
				"| D(k) \u003c 10^k   | multiples of 60 |      in D(k)        |     %     |",
				"| k = 2,3,...,7 |    in D(k)      |                     |           |",
				"+---------------+-----------------+---------------------+-----------+",
				"|   \u003c 10^2      |          1      |          1          | 100 %     |",
				"|   \u003c 10^3      |         16      |          5          |  31.250 % |",
				"|   \u003c 10^4      |        166      |         18          |  10.843 % |",
				"|   \u003c 10^5      |       1666      |         72          |   4.321 % |",
				"|   \u003c 10^6      |      16666      |        256          |   1.536 % |",
				"|   \u003c 10^7      |     166666      |        879          |   0.527 % |",
				"|---------------+-----------------+---------------------+-----------+"
			],
			"link": [
				"Michel Lagneau, \u003ca href=\"/A334080/a334080.pdf\"\u003eOdd terms\u003c/a\u003e"
			],
			"example": [
				"a(4) = 3 because the divisors of A169823(4)= 240 are {1, 2, 3, 4, 5, 6, 8, 10, 12, 15, 16, 20, 24, 30, 40, 48, 60, 80, 120, 240} with 3 Pythagorean triples: (3, 4, 5), (6, 8, 10) and (12, 16, 20). The first triple is primitive."
			],
			"maple": [
				"with(numtheory):",
				"for n from 60 by 60 to 5400 do :",
				"   d:=divisors(n):n0:=nops(d):it:=0:",
				"    for i from 1 to n0-1 do:",
				"     for j from i+1 to n0-2 do :",
				"      for m from i+2 to n0 do:",
				"       if d[i]^2 + d[j]^2 = d[m]^2",
				"        then",
				"        it:=it+1:",
				"        else",
				"       fi:",
				"      od:",
				"     od:",
				"    od:",
				"    printf(`%d, `,it):",
				"   od:"
			],
			"program": [
				"(PARI) ishypo(n) = setsearch(Set(factor(n)[, 1]%4), 1); \\\\ A009003",
				"a(n) = {n *= 60; my(d=divisors(n), nb=0); for (i=3, #d, if (ishypo(d[i]), for (j=2, i-1, for (k=3, j-1, if (d[j]^2 + d[k]^2 == d[i]^2, nb++););););); nb;} \\\\ _Michel Marcus_, Apr 26 2020"
			],
			"xref": [
				"Cf. A000005, A009003, A014442, A081752, A089120, A144255, A169823."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Michel Lagneau_, Apr 14 2020",
			"references": 2,
			"revision": 16,
			"time": "2020-04-27T06:53:33-04:00",
			"created": "2020-04-27T06:53:33-04:00"
		}
	]
}