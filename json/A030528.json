{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A030528",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 30528,
			"data": "1,1,1,0,2,1,0,1,3,1,0,0,3,4,1,0,0,1,6,5,1,0,0,0,4,10,6,1,0,0,0,1,10,15,7,1,0,0,0,0,5,20,21,8,1,0,0,0,0,1,15,35,28,9,1,0,0,0,0,0,6,35,56,36,10,1,0,0,0,0,0,1,21,70,84,45,11,1,0,0,0,0,0,0,7,56,126,120,55,12,1",
			"name": "Triangle read by rows: a(n,k) = binomial(k,n-k).",
			"comment": [
				"A convolution triangle of numbers obtained from A019590.",
				"a(n,m) := s1(-1; n,m), a member of a sequence of triangles including s1(0; n,m)= A023531(n,m) (unit matrix) and s1(2; n,m)= A007318(n-1,m-1) (Pascal's triangle).",
				"The signed triangular matrix a(n,m)*(-1)^(n-m) is the inverse matrix of the triangular Catalan convolution matrix A033184(n+1,m+1), n \u003e= m \u003e= 0, with A033184(n,m) := 0 if n\u003cm.",
				"Riordan array (1+x, x(1+x)). The signed triangle is the Riordan array (1-x,x(1-x)), inverse to (c(x),xc(x)) with c(x) g.f. for A000108. - _Paul Barry_, Feb 02 2005",
				"Also, a(n,k)=number of compositions of n into k parts of 1's and 2's. Example: a(6,4)=6 because we have 2211, 2121, 2112, 1221, 1212 and 1122. - _Emeric Deutsch_, Apr 05 2005",
				"Subtriangle of A026729. - _Philippe Deléham_, Aug 31 2006",
				"a(n,k) is the number of length n-1 binary sequences having no two consecutive 0's with exactly k-1 1's. Example: a(6,4)=6 because we have 01011, 01101, 01110, 10101, 10110, 11010. - _Geoffrey Critzer_, Jul 22 2013",
				"Mirrored, shifted Fibonacci polynomials of A011973. The polynomials (illustrated below) of this entry have the property that p(n,t) = t * [p(n-1,t) + p(n-2,t)]. The additive properties of Pascal's triangle (A007318) are reflected in those of these polynomials, as can be seen in the Example Section below and also when the o.g.f. G(x,t) below is expanded as the series x*(1+x) + t * [x*(1+x)]^2 + t^2 * [x*(1+x)]^3 + ... . See also A053122 for a relation to Cartan matrices. - _Tom Copeland_, Nov 04 2014",
				"Rows of this entry appear as columns of an array for an infinitesimal generator presented in the Copeland link. - _Tom Copeland_, Dec 23 2015",
				"For n \u003e= 2, the n-th row is also the coefficients of the vertex cover polynomial of the (n-1)-path graph P_{n-1}. - _Eric W. Weisstein_, Apr 10 2017",
				"With an additional initial matrix element a_(0,0) = 1 and column of zeros a_(n,0) = 0 for n \u003e 0, these are antidiagonals read from bottom to top of the numerical coefficients of the Maurer-Cartan form matrix of the Leibniz group L^(n)(1,1) presented on p. 9 of the Olver paper), which is generated as exp[c. * M] with (c.)^n = c_n and M the Lie infinitesimal generator A218272. Cf. A011973. And A169803. - _Tom Copeland_, Jul 02 2018"
			],
			"link": [
				"Indranil Ghosh, \u003ca href=\"/A030528/b030528.txt\"\u003eRows 1..125 of triangle, flattened\u003c/a\u003e",
				"Tom Copeland, \u003ca href=\"http://tcjpn.wordpress.com/2015/12/21/generators-inversion-and-matrix-binomial-and-integral-transforms/\"\u003eGenerators, Inversion, and Matrix, Binomial, and Integral Transforms\u003c/a\u003e, \u003ca href=\"http://tcjpn.wordpress.com/2015/10/12/the-elliptic-lie-triad-kdv-and-ricattt-equations-infinigens-and-elliptic-genera/\"\u003eAddendum to Elliptic Lie Triad\u003c/a\u003e",
				"Wolfdieter Lang, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL3/LANG/lang.html\"\u003eOn generalizations of Stirling number triangles\u003c/a\u003e, J. Integer Seqs., Vol. 3 (2000), #00.2.4.",
				"D. Merlini, R. Sprugnoli and M. C. Verri, \u003ca href=\"http://local.disia.unifi.it/merlini/papers/Lucidi.ps\"\u003eAn algebra for proper generating trees\u003c/a\u003e, Colloquium on Mathematics and Computer Science, Versailles, September 2000.",
				"D. Merlini, R. Sprugnoli and M. C. Verri, \u003ca href=\"http://dx.doi.org/10.1007/978-3-0348-8405-1_11\"\u003eAn algebra for proper generating trees\u003c/a\u003e, Mathematics and Computer Science, Part of the series Trends in Mathematics pp 127-139.",
				"P. Olver, \u003ca href=\"http://www.math.umn.edu/~olver/di_/contact.pdf\"\u003eThe canonical contact form\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PathGraph.html\"\u003ePath Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/VertexCoverPolynomial.html\"\u003eVertex Cover Polynomial\u003c/a\u003e"
			],
			"formula": [
				"a(n, m) = 2*(2*m-n+1)*a(n-1, m)/n + m*a(n-1, m-1)/n, n \u003e= m \u003e= 1; a(n, m) := 0, n\u003cm; a(n, 0) := 0; a(1, 1)=1.",
				"G.f. for m-th column: (x*(1+x))^m.",
				"As a number triangle with offset 0, this is T(n, k) = Sum_{k=0..n} (-1)^(n+i)binomial(n, i)binomial(i+k+1, 2k+1). The diagonal sums give the Padovan sequence A000931(n+5). Inverse binomial transform of A078812 (product of lower triangular matrices). - _Paul Barry_, Jun 21 2004",
				"G.f.: (1 + x)/(1 - y*x - y*x^2). - _Geoffrey Critzer_, Jul 22 2013 [offset 0]",
				"From _Tom Copeland_, Nov 04 2014: (Start)",
				"O.g.f: G(x,t) = x*(1+x) / [1 - t*x*(1+x)] = -P[Cinv(-x),t], where P(x,t)= x / (1 + t*x) and Cinv(x)= x*(1-x) are the compositional inverses in x of Pinv(x,t) = -P(-x,t) = x / (1 - t*x) and C(x) = [1-sqrt(1-4*x)]/2, an o.g.f. for the shifted Catalan numbers A000108.",
				"Therefore, Ginv(x,t) = -C[Pinv(-x,t)] = {-1 + sqrt[1 + 4*x/(1+t*x)]}/2, which is  -A124644(-x,t).",
				"This places this array in a family of arrays related by composition of P and C and their inverses and interpolation by t, such as A091867 and A104597, and associated to the Catalan, Motzkin, Fine, and Fibonacci numbers. Cf. A104597 (polynomials shifted in t) A125145, A146559, A057078, A000045, A155020, A125145, A039717, A001792, A057862, A011973, A115139. (End)"
			],
			"example": [
				"Triangle starts:",
				"[ 1]  1",
				"[ 2]  1   1",
				"[ 3]  0   2   1",
				"[ 4]  0   1   3   1",
				"[ 5]  0   0   3   4   1",
				"[ 6]  0   0   1   6   5   1",
				"[ 7]  0   0   0   4  10   6   1",
				"[ 8]  0   0   0   1  10  15   7   1",
				"[ 9]  0   0   0   0   5  20  21   8   1",
				"[10]  0   0   0   0   1  15  35  28   9   1",
				"[11]  0   0   0   0   0   6  35  56  36  10   1",
				"[12]  0   0   0   0   0   1  21  70  84  45  11   1",
				"[13]  0   0   0   0   0   0   7  56 126 120  55  12   1",
				"...",
				"From _Tom Copeland_, Nov 04 2014: (Start)",
				"For quick comparison to other polynomials:",
				"p(1,t) = 1",
				"p(2,t) = 1 + 1 t",
				"p(3,t) = 0 + 2 t + 1 t^2",
				"p(4,t) = 0 + 1 t + 3 t^2 + 1 t^3",
				"p(5,t) = 0 + 0   + 3 t^2 + 4 t^3 +  1 t^4",
				"p(6,t) = 0 + 0   + 1 t^2 + 6 t^3 +  5 t^4 +  1 t^5",
				"p(7,t) = 0 + 0   + 0     + 4 t^3 + 10 t^4 +  6 t^5 + 1 t^6",
				"p(8,t) = 0 + 0   + 0     + 1 t^3 + 10 t^4 + 15 t^5 + 7 t^6 + 1 t^7",
				"...",
				"Reading along columns gives rows for Pascal's triangle. (End)"
			],
			"maple": [
				"for n from 1 to 12 do seq(binomial(k,n-k),k=1..n) od; # yields sequence in triangular form - _Emeric Deutsch_, Apr 05 2005"
			],
			"mathematica": [
				"nn=10;CoefficientList[Series[(1+x)/(1-y x - y x^2),{x,0,nn}],{x,y}]//Grid (* _Geoffrey Critzer_, Jul 22 2013 *)",
				"Table[Binomial[k, n - k], {n, 13}, {k, n}] // Flatten (* _Michael De Vlieger_, Dec 23 2015 *)",
				"CoefficientList[Table[x^(n/2 - 1) Fibonacci[n + 1, Sqrt[x]], {n, 10}],",
				"   x] // Flatten (* _Eric W. Weisstein_, Apr 10, 2017 *)"
			],
			"program": [
				"(MAGMA) /* As triangle */ [[Binomial(k, n-k): k in [1..n]]: n in [1.. 15]]; // _Vincenzo Librandi_, Nov 05 2014"
			],
			"xref": [
				"Row sums A000045(n+1) (Fibonacci). a(n, 1)= A019590(n) (Fermat's last theorem). Cf. A049403.",
				"Cf. A104597, A146559, A155020, A125145, A057078, A039717, A001792, A057862, A011973, A115139."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "1,5",
			"author": "_Wolfdieter Lang_",
			"ext": [
				"More terms from _Emeric Deutsch_, Apr 05 2005"
			],
			"references": 47,
			"revision": 95,
			"time": "2021-07-10T06:51:25-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}