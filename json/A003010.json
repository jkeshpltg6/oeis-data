{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003010",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3010,
			"id": "M3494",
			"data": "4,14,194,37634,1416317954,2005956546822746114,4023861667741036022825635656102100994,16191462721115671781777559070120513664958590125499158514329308740975788034",
			"name": "A Lucas-Lehmer sequence: a(0) = 4; for n\u003e0, a(n) = a(n-1)^2 - 2.",
			"comment": [
				"Albert Beiler states (page 228 of Recreations in the Theory of Numbers): D. H. Lehmer modified Lucas's test to the relatively simple form: If and only if 2^n-1 divides a(n-2) then 2^n-1 is a prime, otherwise it is composite. Since 2^3 - 1 is a factor of a(1) = 14, 2^3 - 1 = 7 is a prime. - _Gary W. Adamson_, Jun 07 2003",
				"a(n) - a(n-1) divides a(n+1) - a(n). - _Thomas Ordowski_, Dec 24 2016"
			],
			"reference": [
				"A. H. Beiler, Recreations in the Theory of Numbers, Dover, NY, 1964, p. 228.",
				"L. E. Dickson, History of the Theory of Numbers. Carnegie Institute Public. 256, Washington, DC, Vol. 1, 1919; Vol. 2, 1920; Vol. 3, 1923, see vol. 1, p. 399.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A003010/b003010.txt\"\u003eTable of n, a(n) for n = 0..10\u003c/a\u003e",
				"A. V. Aho and N. J. A. Sloane, \u003ca href=\"https://www.fq.math.ca/Scanned/11-4/aho-a.pdf\"\u003eSome doubly exponential sequences\u003c/a\u003e, Fibonacci Quarterly, Vol. 11, No. 4 (1973), pp. 429-437, \u003ca href=\"http://neilsloane.com/doc/doubly.html\"\u003ealternative link\u003c/a\u003e.",
				"Larry Ericksen, \u003ca href=\"https://web.archive.org/web/20210307224444/http://siauliaims.su.lt/index.php?option=com_content\u0026amp;view=article\u0026amp;id=44\u0026amp;Itemid=9\"\u003ePrimality Testing and Prime Constellations\u003c/a\u003e, Šiauliai Mathematical Seminar, Vol. 3 (11), 2008.",
				"J. S. Hall, \u003ca href=\"https://doi.org/10.1112/jlms/s1-28.3.285\"\u003eA remark on the primeness of Mersenne numbers\u003c/a\u003e, J. London Math. Soc. 28, (1953). 285-287.",
				"D. H. Lehmer, \u003ca href=\"https://doi.org/10.1112/jlms/s1-10.2.162\"\u003eOn Lucas's test for the primality of Mersenne's numbers\u003c/a\u003e, Journal of the London Mathematical Society 1.3 (1935): 162-165. See page 162.",
				"D. H. Lehmer. \u003ca href=\"/A003009/a003009.pdf\"\u003eMathematical Reviews.  Review of: Hall, James S. A remark on the primeness of Mersenne numbers. (1953)\u003c/a\u003e [Annotated scanned copy]",
				"P. Liardet and P. Stambul, \u003ca href=\"http://www.numdam.org/item?id=JTNB_2000__12_1_37_0\"\u003eSéries d'Engel et fractions continuées\u003c/a\u003e, Journal de Théorie des Nombres de Bordeaux 12 (2000), 37-68.",
				"J. Shallit, \u003ca href=\"/A005248/a005248_1.pdf\"\u003eAn interesting continued fraction\u003c/a\u003e, Math. Mag., 48 (1975), 207-211. [Annotated scanned copy]",
				"P. Vellucci and A. M. Bersani, \u003ca href=\"https://arxiv.org/abs/1603.01989\"\u003eThe class of Lucas-Lehmer polynomials\u003c/a\u003e, arXiv preprint arXiv:1603.01989 [math.CA], 2016.",
				"Pierluigi Vellucci and A. M. Bersani, \u003ca href=\"https://arxiv.org/abs/1606.09597\"\u003eNew formulas for pi involving infinite nested square roots and Gray code\u003c/a\u003e, arXiv preprint arXiv:1606.09597 [math.NT], 2016 (The OEIS is cited in version 1, but has been dropped from version 4.)",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Lucas-LehmerTest.html\"\u003eLucas-Lehmer Test.\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Engel_expansion\"\u003eEngel Expansion\u003c/a\u003e",
				"\u003ca href=\"/index/Aa#AHSL\"\u003eIndex entries for sequences of form a(n+1)=a(n)^2 + ...\u003c/a\u003e"
			],
			"formula": [
				"a(n) = ceiling((2 + sqrt(3))^(2^n)). - _Benoit Cloitre_, Nov 30 2002",
				"More generally, if u(0) = z, integer \u003e 2 and u(n) = a(n-1)^2 - 2 then u(n) = ceiling(c^(2^n)) where c = (1/2)*(z+sqrt(z^2-4)) is the largest root of x^2 - zx + 1 = 0. - _Benoit Cloitre_, Dec 03 2002",
				"a(n) = (2+sqrt(3))^(2^n) + (2-sqrt(3))^(2^n). - John Sillcox (johnsillcox(AT)hotmail.com), Sep 20 2003",
				"a(n) = ceiling(tan(5*Pi/12)^(2^n)). Note: 5*Pi/12 radians is 75 degrees. - Jason M. Follas (jasonfollas(AT)hotmail.com), Jan 16 2004",
				"Sum_{n \u003e= 0} 1/( Product_{k = 0..n} a(k) ) = 2 - sqrt(3). - _Paul D. Hanna_, Aug 11 2004",
				"From _Ulrich Sondermann_, Sep 04 2006: (Start)",
				"To generate the n-th number in the sequence: let x = 2^(n-1), a = 2, b = sqrt(3). Take every other term of the binomial expansion (a+b)^x times 2.",
				"E.g., for the 4th term: x = 2^(4-1) = 8, the binomial expansion is: a^8 + 7a^7 b + 28a^6 b^2 + 56a^5 b^3 + 70a^4 b^4 + 56a^3 b^5 + 28a^2 b^6 + 7a b^7 + b^8, every other term times 2: 2(a^8 + 28a^6 b^2 + 70a^4 b^4 + 28a^2 b^6 + b^8) = 2(256 + (28)(64)(3) + (70)(16)(9) + (28)(4)(27) + 81) = 2(18817) = 37634. (End)",
				"a(n) = 2*cosh( 2^(n-1)*log(sqrt(3)+2) ) For n \u003e 0, a(n) = 2 + 3 * 4^n * (Product_{k=0..n-2} (a(k)/2))^2, where a(k)/2 = A002812(k) is a coprime sequence. - _M. F. Hasler_, Mar 09 2007",
				"a(n) = A003500(2^n). - _John Blythe Dobson_, Oct 28 2007",
				"a(n) = 2*T(2^n,2) where T(n,x) is the Chebyshev polynomial of the first kind. - _Leonid Bedratyuk_, Mar 17 2011",
				"Engel expansion of 2 - sqrt(3). Thus 2 - sqrt(3) = 1/4 + 1/(4*14) + 1/(4*14*194) + ... as noted by Hanna above. See Liardet and Stambul. Cf. A001566, A003423 and A003487. - _Peter Bala_, Oct 31 2012",
				"From _Peter Bala_, Nov 11 2012: (Start)",
				"2*sqrt(3)/5 = Product_{n = 0..inf} (1 - 1/a(n)).",
				"sqrt(3) = Product_{n = 0..inf} (1 + 2/a(n)).",
				"a(n) - 1 = A145503(n+1).",
				"a(n) = 2*A002812(n). (End)",
				"a(n+1) - a(n) = a(n)^2 - a(n-1)^2. - _Thomas Ordowski_, Dec 24 2016",
				"a(n) = 2*cos(2^n * arccos(2)). - _Ryan Brooks_, Oct 27 2020"
			],
			"maple": [
				"a := n-\u003e if n\u003e0 then a(n-1)^2-2 else 4 fi: 'a(i)' $ i=0..9; # _M. F. Hasler_, Mar 09 2007",
				"a := n-\u003e simplify(2*ChebyshevT(2^n, 2), 'ChebyshevT'): seq(a(n), n=0..7);"
			],
			"mathematica": [
				"seqLucasLehmer[0] = 4; seqLucasLehmer[n_] := seqLucasLehmer[n - 1]^2 - 2; Array[seqLucasLehmer, 8, 0] (* _Robert G. Wilson v_, Jun 28 2012 *)"
			],
			"program": [
				"(PARI)",
				"a(n)=if(n,a(n-1)^2-2,4)",
				"vector(10,i,a(i-1)) \\\\ _M. F. Hasler_, Mar 09 2007",
				"(MAGMA) [n le 1 select 4 else Self(n-1)^2-2: n in [1..10]]; // _Vincenzo Librandi_, Aug 24 2015",
				"(Python)",
				"from itertools import accumulate",
				"def f(anm1, _): return anm1**2 - 2",
				"print(list(accumulate([4]*8, f))) # _Michael S. Branicky_, Apr 14 2021"
			],
			"xref": [
				"Cf. A001566 (starting with 3), A003423 (starting with 6), A003487 (starting with 5).",
				"Cf. A002812, A145503."
			],
			"keyword": "nonn",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"One more term from Thomas A. Rockwell (LlewkcoRAT(AT)aol.com), Jan 18 2005"
			],
			"references": 36,
			"revision": 117,
			"time": "2021-07-27T15:49:04-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}