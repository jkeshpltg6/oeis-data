{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A331952",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 331952,
			"data": "-1,0,2,6,11,18,26,36,47,60,74,90,107,126,146,168,191,216,242,270,299,330,362,396,431,468,506,546,587,630,674,720,767,816,866,918,971,1026,1082,1140,1199,1260,1322,1386,1451,1518,1586,1656,1727,1800,1874,1950,2027",
			"name": "a(n) = (-7 + (-1)^(1+n) + 6*n^2) / 8.",
			"comment": [
				"a(n+1) is once in the hexagonal spiral in A330707. a(n+2) is twice in the same spiral.",
				"a(n) has one odd followed by three evens.",
				"Difference table:",
				"  -1, 0, 2, 6, 11, 18, 26, 36, ... = a(n)",
				"   1, 2, 4, 5,  7,  8, 10, 11, ... = A001651(n+1)",
				"   1, 2, 1, 2,  1,  2,  1,  2, ... = A000034."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A331952/b331952.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,0,-2,1).",
				"\u003ca href=\"/index/Tu#2wis\"\u003eIndex entries for two-way infinite sequences\u003c/a\u003e"
			],
			"formula": [
				"a(-n) = a(n).",
				"a(20+n) - a(n) = 30*(10+n).",
				"a(2+n) = a(n) + 3*(1+n), a(0)=-1 and a(1)=0.",
				"a(4*n) = 12*n^2 - 1, a(1+4*n) = 6*n*(1+2*n), a(2+4*n) = 2 + 12*n*(1+n), a(3+4*n) = 6*(1+n)*(1+2*n) for n\u003e= 0.",
				"From _Colin Barker_, Feb 02 2020: (Start)",
				"G.f.: -(1 - 2*x - 2*x^2) / ((1 - x)^3*(1 + x)).",
				"a(n) = 2*a(n-1) - 2*a(n-3) + a(n-4) for n\u003e3.",
				"a(n) = (-7 + (-1)^(1+n) + 6*n^2) / 8.",
				"(End)",
				"E.g.f.: (1/8)*(exp(x)*(6*x^2 + 6*x - 7) - exp(-x)). - _Stefano Spezia_, Feb 02 2020 after _Colin Barker_"
			],
			"mathematica": [
				"LinearRecurrence[{2, 0, -2, 1}, {-1, 0, 2, 6}, 100] (* _Amiram Eldar_, Feb 02 2020 *)"
			],
			"program": [
				"(MAGMA) a:=[-1,0,2,6]; [n le 4 select a[n] else 2*Self(n-1)-2*Self(n-3)+Self(n-4): n in [1..45]]; // _Marius A. Burtea_, Feb 02 2020",
				"(PARI) Vec(-(1 - 2*x - 2*x^2) / ((1 - x)^3*(1 + x)) + O(x^40)) \\\\ _Colin Barker_, Feb 03 2020"
			],
			"xref": [
				"Cf. A000034, A001651, A158463, 6*A014105, 2*A003154, A152746(n+1), A008585(1+n).",
				"Equals 2 less than A084684, 1 less than A077043, and 1 more than A276382(n-1). - _Greg Dresden_, Feb 22 2020"
			],
			"keyword": "sign,easy",
			"offset": "0,3",
			"author": "_Paul Curtz_, Feb 02 2020",
			"ext": [
				"a(42)-a(52) from _Stefano Spezia_, Feb 02 2020"
			],
			"references": 3,
			"revision": 33,
			"time": "2020-02-23T07:08:42-05:00",
			"created": "2020-02-19T04:09:25-05:00"
		}
	]
}