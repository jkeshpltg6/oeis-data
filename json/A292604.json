{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A292604",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 292604,
			"data": "1,1,0,5,1,0,61,28,1,0,1385,1011,123,1,0,50521,50666,11706,506,1,0,2702765,3448901,1212146,118546,2041,1,0,199360981,308869464,147485535,24226000,1130235,8184,1,0",
			"name": "Triangle read by rows, coefficients of generalized Eulerian polynomials F_{2}(x).",
			"comment": [
				"The generalized Eulerian polynomials F_{m}(x) are defined F_{m; 0}(x) = 1 for all m \u003e= 0 and for n \u003e 0:",
				"F_{0; n}(x) = Sum_{k=0..n} A097805(n, k)*(x-1)^(n-k) with coeffs. in A129186.",
				"F_{1; n}(x) = Sum_{k=0..n} A131689(n, k)*(x-1)^(n-k) with coeffs. in A173018.",
				"F_{2; n}(x) = Sum_{k=0..n} A241171(n, k)*(x-1)^(n-k) with coeffs. in A292604.",
				"F_{3; n}(x) = Sum_{k=0..n} A278073(n, k)*(x-1)^(n-k) with coeffs. in A292605.",
				"F_{4; n}(x) = Sum_{k=0..n} A278074(n, k)*(x-1)^(n-k) with coeffs. in A292606.",
				"The case m = 1 are the Eulerian polynomials whose coefficients are the Eulerian numbers which are displayed in Euler's triangle A173018.",
				"Evaluated at x in {-1, 1, 0} these families of polynomials give for the first few m:",
				"F_{m} :   F_{0}    F_{1}    F_{2}    F_{3}    F_{4}",
				"x = -1:  A165326  A155585  A002105  A292609  A292607",
				"x =  1:  A000012  A000142  A000680  A014606  A014608  ... (m*n)!/m!^n",
				"x =  0:    --     A000012  A000364  A002115  A211212  ... m-alternating permutations of length m*n.",
				"Note that the constant terms of the polynomials are the generalized Euler numbers as defined in A181985. In this sense generalized Euler numbers are also generalized Eulerian numbers."
			],
			"reference": [
				"G. Frobenius. Über die Bernoullischen Zahlen und die Eulerschen Polynome. Sitzungsber. Preuss. Akad. Wiss. Berlin, pages 200-208, 1910."
			],
			"formula": [
				"F_{2; n}(x) = Sum_{k=0..n} A241171(n, k)*(x-1)^(n-k) for n\u003e0 and F_{2; 0}(x) = 1."
			],
			"example": [
				"Triangle starts:",
				"[n\\k][    0        1        2       3     4  5  6]",
				"--------------------------------------------------",
				"[0][      1]",
				"[1][      1,       0]",
				"[2][      5,       1,       0]",
				"[3][     61,      28,       1,      0]",
				"[4][   1385,    1011,     123,      1,    0]",
				"[5][  50521,   50666,   11706,    506,    1, 0]",
				"[6][2702765, 3448901, 1212146, 118546, 2041, 1, 0]"
			],
			"maple": [
				"Coeffs := f -\u003e PolynomialTools:-CoefficientList(expand(f), x):",
				"A292604_row := proc(n) if n = 0 then return [1] fi;",
				"add(A241171(n, k)*(x-1)^(n-k), k=0..n); [op(Coeffs(%)), 0] end:",
				"for n from 0 to 6 do A292604_row(n) od;"
			],
			"mathematica": [
				"T[n_, k_] /; 1 \u003c= k \u003c= n := T[n, k] = k (2 k - 1) T[n - 1, k - 1] + k^2 T[n - 1, k]; T[_, 1] = 1; T[_, _] = 0;",
				"F[2, 0][_] = 1; F[2, n_][x_] := Sum[T[n, k] (x - 1)^(n - k), {k, 0, n}];",
				"row[n_] := If[n == 0, {1}, Append[CoefficientList[ F[2, n][x], x], 0]];",
				"Table[row[n], {n, 0, 7}] (* _Jean-François Alcover_, Jul 06 2019 *)"
			],
			"program": [
				"(Sage)",
				"def A292604_row(n):",
				"    if n == 0: return [1]",
				"    S = sum(A241171(n, k)*(x-1)^(n-k) for k in (0..n))",
				"    return expand(S).list() + [0]",
				"for n in (0..6): print(A292604_row(n))"
			],
			"xref": [
				"F_{0} = A129186, F_{1} = A173018, F_{2} is this triangle, F_{3} = A292605, F_{4} = A292606.",
				"First column: A000364. Row sums: A000680. Alternating row sums: A002105.",
				"Cf. A181985, A241171."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Peter Luschny_, Sep 20 2017",
			"references": 4,
			"revision": 29,
			"time": "2020-03-06T04:05:17-05:00",
			"created": "2017-09-26T09:46:37-04:00"
		}
	]
}