{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A220952",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 220952,
			"data": "0,1,2,3,4,9,14,19,18,17,16,11,12,13,8,7,6,5,10,15,20,21,22,23,24,49,74,99,98,97,96,71,72,73,48,47,46,45,70,95,90,85,80,55,60,65,40,35,30,31,32,33,38,37,36,41,42,43,68,67,66,61,62,63,58,57,56,81,82,83,88",
			"name": "A twisted enumeration of the nonnegative integers.",
			"comment": [
				"Initially _Don Knuth_ gave as the definition of this sequence \"A sequence that I'm submitting as a problem for publication (see note in comments!)\" and the comment that \"As soon as a solution is published, I'll provide lots more info; the sequence is so fascinating, it has caused me to take three days off from writing The Art of Computer Programming, but I plan to use it in Chapter 8 some day.\"",
				"In order for the definition to make sense, it looks like any integer has to be preceded by infinitely many zeroes in its base-5 representation. This ensures that the condition is not vacuous for single-digit numbers, so that (except for 0) they also have two adjacent numbers. - _Jean-Paul Allouche_, Aug 25 2017",
				"[Obviously it is understood that a_i = 0 for all i \u003e log_5(a)+1. But it is sufficient to take all i \u003c log_5(max(a,b))+2, i.e., to consider just one \"leading zero\" for the larger number, and as many digits for the smaller number. - _M. F. Hasler_, Mar 13 2018]",
				"From _Andrey Zabolotskiy_, Feb 21 2018: (Start)",
				"The sequence is defined by Knuth as follows.",
				"Say that nonnegative integers a and b are adjacent when their base-5 expansions ...a_2 a_1 a_0 and ...b_2 b_1 b_0 satisfy the condition that if i \u003e j then the pairs of base-5 digits (a_i,a_j) and (b_i,b_j) are either equal or consecutive in the path through {0, 1, 2, 3, 4}^2 shown at the diagram:",
				"  .",
				"  (0,4)--(1,4)--(2,4)--(3,4)  (4,4)",
				"    |                    |      |",
				"    |                    |      |",
				"  (0,3)  (1,3)--(2,3)  (3,3)  (4,3)",
				"    |      |      |      |      |",
				"    |      |      |      |      |",
				"  (0,2)  (1,2)  (2,2)  (3,2)  (4,2)",
				"    |      |      |      |      |",
				"    |      |      |      |      |",
				"  (0,1)  (1,1)  (2,1)--(3,1)  (4,1)",
				"    |      |                    |",
				"    |      |                    |",
				"  (0,0)  (1,0)--(2,0)--(3,0)--(4,0)",
				"  .",
				"Actually, every positive integer is adjacent to exactly two nonnegative integers, and we can write down a permutation of nonnegative integers starting with 0 such that the two consecutive numbers in it are adjacent. That permutation is this sequence.",
				"(End)",
				"From _Daniel Forgues_, Feb 22 2018: (Start)",
				"The first differences appear to be +- 5^k, for some k \u003e= 0:",
				"{1, 1, 1, 1, 5, 5, 5, -1, -1, -1, -5, 1, 1, -5, -1, -1, -1, 5, 5,  5, 1, 1, 1, 1, 25, 25, 25, -1, -1, -1, -25, 1, 1, -25, -1, -1, -1, 25, 25, -5, -5, -5, -25, 5, 5, ...}",
				"Fractal behavior: when n = 5^k - 1, k \u003e= 2, a similar image is completed.",
				"(End)"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A220952/b220952.txt\"\u003eTable of n, a(n) for n = 0..15624\u003c/a\u003e",
				"Donald Knuth (Proposer), \u003ca href=\"http://dx.doi.org/10.4169/amer.math.monthly.120.09.854\"\u003eA twisted enumeration of the positive integers; Problem 11733\u003c/a\u003e, Amer. Math. Monthly, 120 (9) (2013), 76.",
				"R. J. Mathar, \u003ca href=\"/A220952/a220952.txt\"\u003eMaple program for A220952\u003c/a\u003e.",
				"Richard Stong (Solver), \u003ca href=\"http://dx.doi.org/10.4169/amer.math.monthly.123.1.97\"\u003eA twisted enumeration of the positive integers; Solution to Problem 11733\u003c/a\u003e, Amer. Math. Monthly, 123 (1) (2016), 98-100. See \u003ca href=\"https://lupucezar.files.wordpress.com/2011/02/amer-math-monthly-123-1-97.pdf\"\u003ehere\u003c/a\u003e for another link.",
				"Richard Stong (Solver), \u003ca href=\"/A220952/a220952.pdf\"\u003eA twisted enumeration of the positive integers; Solution to Problem 11733\u003c/a\u003e, Amer. Math. Monthly, 123 (1) (2016), 98-100. [Annotated scanned copy]",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex to sequences related to permutations of the nonnegative integers\u003c/a\u003e"
			],
			"example": [
				"48 (equals 143 in base 5) is adjacent to 47 = 142_5 and 73 = 243_5, hence 48 follows 73 and precedes 47."
			],
			"maple": [
				"# See the link, _R. J. Mathar_, Aug 25 2017"
			],
			"program": [
				"(PARI) isAdj(a,b)={a=Vec(digits(min(a,b),5),-#b=concat(0,digits(max(a,b),5))); normlp(a-b,1)\u003c2 \u0026\u0026 !for(j=2,#b, for(i=1,j-1, if(a[i]==b[i], !a[i] || a[i]==4 || (a[i]==3 \u0026\u0026 min(a[j],b[j])) || (a[i]==1 \u0026\u0026 max(a[j],b[j])\u003c4) || (a[i]==2 \u0026\u0026 !#setminus(Set([a[j],b[j]]),[1,2,3])) || a[j]==b[j], (!a[j] \u0026\u0026 min(a[i],b[i])) || (a[j]==4 \u0026\u0026 max(a[i],b[i])\u003c4) || (a[j]==1 \u0026\u0026 Set([a[i],b[i]])==[2,3]) || (a[j]==3 \u0026\u0026 Set([a[i],b[i]])==[1,2]) || a[i]==b[i]) || return))}",
				"u=[];for(n=a=0,100,print1(a\",\");u=setunion(u,[a]); while(#u\u003e1\u0026\u0026u[2]==u[1]+1,u=u[^1]); for(k=u[1]+1,oo,!setsearch(u,k)\u0026\u0026isAdj(a,k)\u0026\u0026(a=k)\u0026\u0026next(2))) \\\\ _M. F. Hasler_, Mar 13 2018"
			],
			"xref": [
				"See A300855 for the inverse permutation, A300857 for the base-7 variant."
			],
			"keyword": "nonn,look,base,nice",
			"offset": "0,3",
			"author": "_Don Knuth_, Feb 20 2013",
			"ext": [
				"Extended beyond a(25) by _R. J. Mathar_, Aug 25 2017"
			],
			"references": 8,
			"revision": 88,
			"time": "2018-12-08T02:22:05-05:00",
			"created": "2013-02-20T20:43:18-05:00"
		}
	]
}