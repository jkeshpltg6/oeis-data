{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A035462",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 35462,
			"data": "1,0,0,1,0,0,1,1,0,1,1,1,1,1,2,2,1,2,3,2,2,4,4,3,4,5,5,5,6,7,8,7,8,11,10,10,13,14,14,15,17,19,20,20,24,27,26,28,33,35,35,39,44,46,48,52,58,62,63,69,78,80,83,93,100,104,111,120,130,137,143,156,169,175,185,203",
			"name": "Number of partitions of n into parts 4k-1.",
			"comment": [
				"Also, number of partitions into parts 8k+3 or 8k+7.",
				"Also number of partitions of n such that 2k-1 and 2k occur with the same multiplicity. Example: a(18)=3 because we have [8,7,2,1],[6,5,4,3] and [2,2,2,2,2,2,1,1,1,1,1,1]. It is easy to find a bijection between these partitions and those described in the definition. - _Emeric Deutsch_, Apr 05 2006"
			],
			"link": [
				"Vaclav Kotesovec, \u003ca href=\"/A035462/b035462.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"James Mc Laughlin, Andrew V. Sills, Peter Zimmer, \u003ca href=\"https://doi.org/10.37236/36\"\u003eRogers-Ramanujan-Slater Type Identities \u003c/a\u003e, arXiv:1901.00946 [math.NT]"
			],
			"formula": [
				"G.f.: 1/prod(i\u003e=1, 1-x^(4*i-1)). - _Emeric Deutsch_, Apr 05 2006",
				"G.f.: sum(n\u003e=0, x^(3*n) / prod(k=1..n, 1-x^(4*k) ) ) = 1 + sum(n\u003e=0, x^(4*n+3) / prod(k\u003e=n, 1-x^(4*k+3) ) ) = 1 + sum(n\u003e=0, x^(4*n+3) / prod(k=0..n, 1-x^(4*k+3) ) ). - _Joerg Arndt_, Apr 08 2011",
				"a(n) ~ Pi^(3/4) * exp(Pi*sqrt(n/6)) / (Gamma(1/4) * 2^(13/8) * 3^(3/8) * n^(7/8)) * (1 + (Pi/(96*sqrt(6)) - 21*sqrt(3/2)/(16*Pi)) / sqrt(n)). - _Vaclav Kotesovec_, Feb 26 2015, extended Jan 24 2017",
				"a(n) = (1/n)*Sum_{k=1..n} A050452(k)*a(n-k), a(0) = 1. - _Seiichi Manyama_, Mar 20 2017",
				"From _Peter Bala_, Feb 02 2021: (Start)",
				"G.f.: A(x) = Sum_{n \u003e= 0} x^(n*(4*n-1))/Product_{k = 1..n} ( (1 - x^(4*k))*(1 - x^(4*k-1)) ). (Set z = x^3 and q = x^4 in McLaughlin et al., Section 1.3, Entry 7.)",
				"Similarly, A(x) =  Sum_{n \u003e= 0} x^(n*(4*n+3))/( (1 - x^3)*Product_{k = 1..n} ((1 - x^(4*k))*(1 - x^(4*k+3))) ). (End)"
			],
			"example": [
				"a(18)=3 because we have [15,3],[11,7] and [3,3,3,3,3,3]."
			],
			"maple": [
				"g:=1/product(1-x^(4*i-1),i=1..50): gser:=series(g,x=0,80): seq(coeff(gser,x,n),n=1..75); # _Emeric Deutsch_, Apr 05 2006"
			],
			"mathematica": [
				"nmax = 100; CoefficientList[Series[Product[1/(1-x^(4*k+3)),{k, 0, nmax}], {x, 0, nmax}], x] (* _Vaclav Kotesovec_, Feb 26 2015 *)",
				"nmax = 50; kmax = nmax/4; s = Range[0, kmax]*4 - 1;",
				"Table[Count[IntegerPartitions@n, x_ /; SubsetQ[s, x]], {n, 0, nmax}] (* _Robert Price_, Aug 04 2020 *)"
			],
			"xref": [
				"Cf. A035441-A035468, A050452.",
				"Cf. similar sequences of number of partitions of n into parts congruent to m-1 mod m: A000009 (m=2), A035386 (m=3), this sequence (m=4), A109700 (m=5), A109702 (m=6), A109708 (m=7)."
			],
			"keyword": "nonn,easy",
			"offset": "0,15",
			"author": "_Olivier Gérard_",
			"ext": [
				"Offset changed by _N. J. A. Sloane_, Apr 11 2010"
			],
			"references": 9,
			"revision": 42,
			"time": "2021-02-02T11:32:23-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}