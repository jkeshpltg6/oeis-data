{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A300841",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 300841,
			"data": "1,3,4,5,7,12,9,15,11,21,13,20,16,27,28,17,19,33,23,35,36,39,25,60,29,48,44,45,31,84,37,51,52,57,63,55,41,69,64,105,43,108,47,65,77,75,49,68,53,87,76,80,59,132,91,135,92,93,61,140,67,111,99,85,112,156,71,95,100,189,73,165,79,123,116,115,117,192,81",
			"name": "Fermi-Dirac factorization prime shift towards larger terms: a(n) = A052330(2*A052331(n)).",
			"comment": [
				"With n having a unique factorization as A050376(i) * A050376(j) * ... * A050376(k), with i, j, ..., k all distinct, a(n) = A050376(1+i) * A050376(1+j) * ... * A050376(1+k).",
				"Multiplicative because for coprime m and n the Fermi-Dirac factorizations of m and n are disjoint and their union is the Fermi-Dirac factorization of m * n. - _Andrew Howroyd_, Aug 02 2018"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A300841/b300841.txt\"\u003eTable of n, a(n) for n = 1..32768\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A052330(2*A052331(n)).",
				"For all n \u003e= 1, a(A050376(n)) = A050376(1+n).",
				"For all n \u003e= 1, A300840(a(n)) = n.",
				"a(A059897(n,k)) = A059897(a(n), a(k)). - _Peter Munn_, Nov 23 2019"
			],
			"example": [
				"For n = 6 = A050376(1)*A050376(2), a(6) = A050376(2)*A050376(3) = 3*4 = 12.",
				"For n = 12 = A050376(2)*A050376(3), a(12) = A050376(3)*A050376(4) = 4*5 = 20."
			],
			"program": [
				"(PARI)",
				"up_to_e = 8192;",
				"v050376 = vector(up_to_e);",
				"A050376(n) = v050376[n];",
				"ispow2(n) = (n \u0026\u0026 !bitand(n,n-1));",
				"i = 0; for(n=1,oo,if(ispow2(isprimepower(n)), i++; v050376[i] = n); if(i == up_to_e,break));",
				"A052330(n) = { my(p=1,i=1); while(n\u003e0, if(n%2, p *= A050376(i)); i++; n \u003e\u003e= 1); (p); };",
				"A052331(n) = { my(s=0,e); while(n \u003e 1, fordiv(n, d, if(((n/d)\u003e1)\u0026\u0026ispow2(isprimepower(n/d)), e = vecsearch(v050376, n/d); if(!e, print(\"v050376 too short!\"); return(1/0)); s += 2^(e-1); n = d; break))); (s); };",
				"A300841(n) = A052330(2*A052331(n));"
			],
			"xref": [
				"Cf. A050376, A052330, A052331, A059897, A300840 (a left inverse).",
				"Cf. also A003961.",
				"Range of values is A003159."
			],
			"keyword": "nonn,mult",
			"offset": "1,2",
			"author": "_Antti Karttunen_, Apr 12 2018",
			"references": 12,
			"revision": 20,
			"time": "2020-01-20T21:43:36-05:00",
			"created": "2018-04-13T07:46:52-04:00"
		}
	]
}