{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A323834",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 323834,
			"data": "0,1,1,1,2,3,-2,-1,1,4,-5,-7,-8,-7,-3,16,11,4,-4,-11,-14,61,77,88,92,88,77,63,-272,-211,-134,-46,46,134,211,274,-1385,-1657,-1868,-2002,-2048,-2002,-1868,-1657,-1383,7936,6551,4894,3026,1024,-1024,-3026,-4894,-6551,-7934,50521,58457,65008,69902,72928,73952,72928,69902,65008,58457,50523",
			"name": "A Seidel matrix A(n,k) read by antidiagonals downwards.",
			"comment": [
				"The first row is a signed version of the Euler numbers A000111.",
				"Other rows are defined by A(n+1,k) = A(n,k) + A(n,k+1)."
			],
			"link": [
				"A. Randrianarivony and J. Zeng, \u003ca href=\"http://dx.doi.org/10.1006/aama.1996.0001\"\u003eUne famille de polynomes qui interpole plusieurs suites classiques de nombres\u003c/a\u003e, Adv. Appl. Math. 17 (1996), 1-26. See Section 6 (matrix b_{n,k} on p. 19)."
			],
			"formula": [
				"From _Petros Hadjicostas_, Mar 02 2021: (Start)",
				"Formulas for the square array A(n,k) (n, k \u003e= 0):",
				"A(0,k) = (-1)^floor((k-1)/2)*A000111(k) for k \u003e 0 with A(0,0) = 0.",
				"A(n,k) = Sum_{i=0..n} binomial(n, i)*A(0,k+i) for n, k \u003e= 0.",
				"A(n,n)/2 = A(n+1,n) = +/- A000657(n) for n \u003e 0.",
				"Bivariate e.g.f.: Sum_{n,k \u003e= 0} A(n,k)*(x^n/n!)*(y^k/k!) = (-sech(x + y) + tanh(x + y) + 1)*exp(x).",
				"Formulas for the triangular array T(n,k) = A(k,n-k) (n \u003e= 0, 0 \u003c= k \u003c= n):",
				"T(n,k) = T(n-1,k-1) + T(n,k-1) for 1 \u003c= k \u003c= n with T(n,0) = (-1)^floor((n-1)/2) * A000111(n) for n \u003e 0 and T(0,0) = 0.",
				"T(n,k) = Sum_{i=0..k} binomial(k,i)*T(n-k+i,0) for 0 \u003c= k \u003c= n. (End)"
			],
			"example": [
				"Read as triangle T(n,k) = A(k, n-k) (n \u003e= 0, k = 0..n), the first few antidiagonals of the square array A are:",
				"     0,",
				"     1,    1,",
				"     1,    2,    3,",
				"    -2,   -1,    1,   4,",
				"    -5,   -7,   -8,  -7,  -3,",
				"    16,   11,    4,  -4, -11, -14,",
				"    61,   77,   88,  92,  88,  77,  63,",
				"  -272, -211, -134, -46,  46, 134, 211, 274,",
				"  ...",
				"From _Petros Hadjicostas_, Mar 02 2021: (Start)",
				"Square array A(n,k) (with rows n \u003e= 0 and columns k \u003e= 0) begins:",
				"     0,   1,   1,    -2,    -5,    16,     61,     -272,    -1385, ...",
				"     1,   2,  -1,    -7,    11,    77,   -211,    -1657,     6551, ...",
				"     3,   1,  -8,     4,    88,  -134,  -1868,     4894,    65008, ...",
				"     4,  -7,  -4,    92,   -46, -2002,   3026,    69902,  -179806, ...",
				"    -3, -11,  88,    46, -2048,  1024,  72928,  -109904, -3784448, ...",
				"   -14,  77, 134, -2002, -1024, 73952, -36976, -3894352,  5860016, ...",
				"   ... (End)"
			],
			"program": [
				"(PARI) {b(n) = local(v=[1], t); if( n\u003c0, 0, for(k=2, n+2, t=0; v = vector(k, i, if( i\u003e1, t+= v[k+1-i]))); v[2])}; \\\\ _Michael Somos_'s PARI program for A000111.",
				"c(n) = if(n==0, 0, (-1)^floor((n-1)/2)*b(n))",
				"A(n, k) = sum(i=0, n, binomial(n, i)*c(k+i)) \\\\ _Petros Hadjicostas_, Mar 02 2021"
			],
			"xref": [
				"Cf. A000111, A000657 (next-to-main diagonal), A323833."
			],
			"keyword": "sign,tabl",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_, Feb 03 2019",
			"ext": [
				"Typo corrected by and more terms from _Petros Hadjicostas_, Mar 02 2021"
			],
			"references": 3,
			"revision": 40,
			"time": "2021-03-04T02:37:43-05:00",
			"created": "2019-02-03T20:18:49-05:00"
		}
	]
}