{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A132319",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 132319,
			"data": "1,-3,3,-4,9,-12,15,-24,39,-52,66,-96,130,-168,219,-292,390,-492,625,-804,1023,-1280,1599,-2016,2508,-3096,3807,-4688,5760,-7020,8532,-10368,12585,-15156,18213,-21912,26287,-31404,37410,-44584,53004,-62784,74245,-87768",
			"name": "Expansion of q^-1 * (chi(-q) * chi(-q^7))^3 in powers of q where chi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A132319/b132319.txt\"\u003eTable of n, a(n) for n = -1..10000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of (eta(q) * eta(q^7) / (eta(q^2) * eta(q^14)))^3 in powers of q.",
				"Euler transform of period 14 sequence [ -3, 0, -3, 0, -3, 0, -6, 0, -3, 0, -3, 0, -3, 0, ...].",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2)) where f(u, v) = u^2 * v - v^2 + 8 * u + 6 * u * v.",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (14 t)) = 8 g(t) where q = exp(2 Pi i t) and g() is the g.f. for A120006.",
				"G.f.: x^-1 * (Product_{k\u003e0} (1 + x^k) * (1 + x^(7*k)))^-3.",
				"a(n) = A058503(n) unless n = 0. Convolution inverse is A120006.",
				"a(n) = -(-1)^n * exp(2*Pi*sqrt(n/7)) / (2*7^(1/4)*n^(3/4)). - _Vaclav Kotesovec_, Sep 07 2017"
			],
			"example": [
				"G.f. = 1/q - 3 + 3*q - 4*q^2 + 9*q^3 - 12*q^4 + 15*q^5 - 24*q^6 + 39*q^7 - ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (QPochhammer[ q, q^2] QPochhammer[ q^7, q^14])^3 / q, {q, 0, n}]; (* _Michael Somos_, Aug 26 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c-1, 0, n++; A = x * O(x^n); polcoeff( (eta(x + A) * eta(x^7 + A) / (eta(x^2 + A) * eta(x^14 + A)))^3, n))};"
			],
			"xref": [
				"Cf. A058503, A120006."
			],
			"keyword": "sign",
			"offset": "-1,2",
			"author": "_Michael Somos_, Aug 18 2007",
			"references": 3,
			"revision": 17,
			"time": "2021-03-12T22:24:44-05:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}