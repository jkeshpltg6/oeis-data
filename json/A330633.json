{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A330633",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 330633,
			"data": "0,0,0,0,0,0,0,0,0,0,0,1,2,3,4,5,6,7,8,9,0,2,4,6,8,10,12,14,16,18,0,3,6,9,12,15,18,21,24,27,0,4,8,12,16,20,24,28,32,36,0,5,10,15,20,25,30,35,40,45,0,6,12,18,24,30,36,42,48,54,0,7,14,21,28,35,42,49,56,63,0",
			"name": "The concatenation of the products of every pair of consecutive digits of n (with a(n) = 0 for 0 \u003c= n \u003c= 9).",
			"comment": [
				"If the decimal expansion of n is d_1 d_2 ... d_k then a(n) is the number formed by concatenating the decimal numbers d_1*d_2, d_2*d_3, ..., d_{k-1}*d_k.",
				"Due to the fact that for two digit numbers the sequence is simply the multiplication of those two numbers, this sequence matches numerous others for the first 100 terms. See the sequences in the cross references. The terms begin to differ beyond n = 100."
			],
			"link": [
				"Scott R. Shannon, \u003ca href=\"/A330633/b330633.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(10) = 0 as 1 * 0 = 0.",
				"a(29) = 18 as 2 * 9 = 18.",
				"a(100) = 0 as 1 * 0 = 0 and 0 = 0 = 0, and '00' is reduced to 0.",
				"a(110) = 10 as 1 * 1 = 1 and 1 * 0 = 0. This is the first term that differs from A007954 and A171765, the multiplication of all digits of n."
			],
			"maple": [
				"read(\"transforms\") :",
				"A330633 := proc(n)",
				"    local dgs,L,i ;",
				"    if n \u003c=9 then",
				"        0;",
				"    else",
				"        dgs := ListTools[Reverse](convert(n,base,10)) ;",
				"        L := [] ;",
				"        for i from 2 to nops(dgs) do",
				"            L := [op(L), op(i-1,dgs)*op(i,dgs)] ;",
				"        end do:",
				"        digcatL(L) ;",
				"    end if;",
				"end proc: # _R. J. Mathar_, Jan 11 2020"
			],
			"mathematica": [
				"Array[If[Or[# == 0, IntegerLength@ # == 1], 0, FromDigits[Join @@ IntegerDigits[Times @@ # \u0026 /@ Partition[IntegerDigits@ #, 2, 1]]]] \u0026, 81, 0] (* _Michael De Vlieger_, Dec 23 2019 *)"
			],
			"program": [
				"(PARI) a(n) = my(d=digits(n), s=\"0\"); for (k=1, #d-1, s=concat(s, d[k]*d[k+1])); eval(s); \\\\ _Michel Marcus_, Apr 28 2020"
			],
			"xref": [
				"Cf. A007954, A088117, A040115, A053392, A035930, A257850, A080464, A171765, A169669, A257297."
			],
			"keyword": "nonn,base,easy",
			"offset": "0,13",
			"author": "_Scott R. Shannon_, Dec 21 2019",
			"references": 2,
			"revision": 27,
			"time": "2020-04-28T14:58:05-04:00",
			"created": "2020-01-05T11:34:39-05:00"
		}
	]
}