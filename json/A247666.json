{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A247666",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 247666,
			"data": "1,7,7,25,7,49,25,103,7,49,49,175,25,175,103,409,7,49,49,175,49,343,175,721,25,175,175,625,103,721,409,1639,7,49,49,175,49,343,175,721,49,343,343,1225,175,1225,721,2863,25,175,175,625",
			"name": "Number of ON cells after n generations of \"Odd-Rule\" cellular automaton on hexagonal lattice based on 7-celled neighborhood.",
			"comment": [
				"The neighborhood of a cell consists of the cell itself together with its six surrounding cells. A cell is ON at generation n iff an odd number of its neighbors were ON at the previous generation. We start with one ON cell.",
				"This is the Run Length Transform of the sequence 1,7,25,103,409,1639,26215,... (almost certainly A102900).",
				"This appears to be the same as the number of ON cells in a certain 2-D CA on the square grid in which the neighborhood of a cell is defined by f = 1/(x*y)+1/x+1/x*y+1/y+x/y+x+x*y, and in which a cell is ON iff there was an odd number of ON cells in the neighborhood at the previous generation. Here is the neighborhood:",
				"[X, 0, X]",
				"[X, 0, X]",
				"[X, X, X]",
				"which contains a(1) = 7 ON cells.",
				"This is the odd-rule cellular automaton defined by OddRule 557 (see Ekhad-Sloane-Zeilberger \"Odd-Rule Cellular Automata on the Square Grid\" link).",
				"Furthermore, this is also the number of ON cells in the 2-D CA on the square grid in which the neighborhood of a cell is defined by f = 1/(x*y)+1/x+1/y+1+y+x+x*y, with the same rule. Here is the neighborhood:",
				"[0, X, X]",
				"[X, X, X]",
				"[X, X, 0]",
				"- _N. J. A. Sloane_, Feb 19 2015",
				"This is the odd-rule cellular automaton defined by OddRule 376 (see Ekhad-Sloane-Zeilberger \"Odd-Rule Cellular Automata on the Square Grid\" link).",
				"The partial sums are in A253767 in which the structure looks like an irregular stepped pyramid, apparently with a like-hexagonal base. - _Omar E. Pol_, Jan 29 2015"
			],
			"link": [
				"Shalosh B. Ekhad, N. J. A. Sloane, and  Doron Zeilberger, \u003ca href=\"http://arxiv.org/abs/1503.01796\"\u003eA Meta-Algorithm for Creating Fast Algorithms for Counting ON Cells in Odd-Rule Cellular Automata\u003c/a\u003e, arXiv:1503.01796 [math.CO], 2015; see also the \u003ca href=\"http://www.math.rutgers.edu/~zeilberg/mamarim/mamarimhtml/CAcount.html\"\u003eAccompanying Maple Package\u003c/a\u003e.",
				"Shalosh B. Ekhad, N. J. A. Sloane, and  Doron Zeilberger, \u003ca href=\"http://arxiv.org/abs/1503.04249\"\u003eOdd-Rule Cellular Automata on the Square Grid\u003c/a\u003e, arXiv:1503.04249 [math.CO], 2015.",
				"N. J. A. Sloane, \u003ca href=\"/A247666/a247666.pdf\"\u003eIllustrations of generations 0 to 4\u003c/a\u003e",
				"N. J. A. Sloane, On the No. of ON Cells in Cellular Automata, Video of talk in Doron Zeilberger's Experimental Math Seminar at Rutgers University, Feb. 05 2015: \u003ca href=\"https://vimeo.com/119073818\"\u003ePart 1\u003c/a\u003e, \u003ca href=\"https://vimeo.com/119073819\"\u003ePart 2\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"http://arxiv.org/abs/1503.01168\"\u003eOn the Number of ON Cells in Cellular Automata\u003c/a\u003e, arXiv:1503.01168 [math.CO], 2015.",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e"
			],
			"formula": [
				"a(n) = number of terms in expansion of f^n mod 2, where f = 1+1/x+x+1/y+y+1/(x*y)+x*y (mod 2);"
			],
			"example": [
				"From _Omar E. Pol_, Jan 29 2015: (Start)",
				"May be arranged into blocks of sizes A011782:",
				"1;",
				"7;",
				"7, 25;",
				"7, 49, 25, 103;",
				"7, 49, 49, 175, 25, 175, 103, 409;",
				"7, 49, 49, 175, 49, 343, 175, 721, 25, 175, 175, 625, 103, 721, 409, 1639;",
				"7, 49, 49, 175, 49, 343, 175, 721, 49, 343, 343, 1225, 175, 1225, 721, 2863, 25, 175, 175, 625, ...",
				"It appears that right border gives A102900 without repetitions, see Comments section. [This is just a restatement of the fact that this sequence is the run length transform of what is presumably A102900. - _N. J. A. Sloane_, Feb 06 2015]",
				"(End)",
				"From _Omar E. Pol_, Mar 19 2015: (Start)",
				"Also, the sequence can be written as an irregular tetrahedron T(s,r,k) as shown below:",
				"1;",
				"..",
				"7;",
				"..",
				"7;",
				"25;",
				".........",
				"7,    49;",
				"25;",
				"103;",
				"...................",
				"7,    49,  49, 175;",
				"25,  175;",
				"103;",
				"409;",
				"......................................",
				"7,    49,  49, 175, 49, 343, 175, 721;",
				"25,  175, 175, 625;",
				"103, 721;",
				"409;",
				"1639;",
				"...",
				"Apart from the initial 1, we have that T(s,r,k) = T(s+1,r,k).",
				"(End)"
			],
			"maple": [
				"C := f-\u003e`if`(type(f,`+`),nops(f),1);",
				"f := 1+1/x+x+1/y+y+1/(x*y)+x*y;",
				"g := n-\u003eexpand(f^n) mod 2;",
				"[seq(C(g(n)),n=0..100)];"
			],
			"mathematica": [
				"A247666[n_] := Total[CellularAutomaton[{170, {2, {{1, 1, 0}, {1, 1, 1}, {0, 1, 1}}}, {1, 1}}, {{{1}}, 0}, {{{n}}}], 2]; Array[A247666, 52, 0] (* _JungHwan Min_, Sep 01 2016 *)",
				"A247666L[n_] := Total[#, 2] \u0026 /@ CellularAutomaton[{170, {2, {{1, 1, 0}, {1, 1, 1}, {0, 1, 1}}}, {1, 1}}, {{{1}}, 0}, n]; A247666L[51] (* _JungHwan Min_, Sep 01 2016 *)"
			],
			"xref": [
				"Cf. A102900, A071053, A160239, A247640."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Sep 22 2014",
			"references": 7,
			"revision": 53,
			"time": "2020-12-31T11:11:15-05:00",
			"created": "2014-09-22T13:02:55-04:00"
		}
	]
}