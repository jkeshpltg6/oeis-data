{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A125079",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 125079,
			"data": "1,1,2,0,1,0,2,2,2,0,0,0,3,1,2,0,0,0,2,2,2,0,2,0,1,2,2,0,0,0,2,0,4,0,0,0,2,3,0,0,1,0,4,2,2,0,0,0,2,0,2,0,0,0,2,2,2,0,2,0,1,2,4,0,0,0,0,2,2,0,0,0,4,1,2,0,2,0,2,2,0,0,0,0,3,0,2,0,0,0,2,2,4,0,0,0,2,4,2,0,0,0,4,0,0",
			"name": "Excess of number of divisors of 2n+1 of form 12k+1, 12k+5 over those of form 12k+7, 12k+11.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"reference": [
				"N. J. Fine, Basic Hypergeometric Series and Applications, Amer. Math. Soc., 1988; p. 82, Eq. (32.56)."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A125079/b125079.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-1/2) * eta(q^3)^3 *eta(q^4) * eta(q^12) / (eta(q) * eta(q^6)^2) in powers of q.",
				"Expansion of phi(-q^3) * psi(-q^3) / (chi(-q) * chi(-q^2)) in powers of q where phi(), psi(), chi() are Ramanujan theta functions.",
				"Euler transform of period 12 sequence [ 1, 1, -2, 0, 1, 0, 1, 0, -2, 1, 1, -2, ...].",
				"a(n) = b(2*n + 1) where b(n) is multiplicative and b(2^e) = 0^e, b(3^e) = 1, b(p^e) = e+1 if p == 1 (mod 4), b(p^e) = (1 + (-1)^e) / 2 if p == 3 (mod 4).",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (24 t)) = 2 (t/i) g(t) where q = exp(2 Pi i t) and g() is g.f. for A138745.",
				"a(6*n + 3) = a(6*n + 5) = 0. a(6*n) = A002175(n). a(6*n + 1) = a(2*n) = A008441(n). a(6*n + 2) = 2 * A121444(n). a(n) = A035154(2*n + 1)."
			],
			"example": [
				"1 + x + 2*x^2 + x^4 + 2*x^6 + 2*x^7 + 2*x^8 + 3*x^12 + x^13 + 2*x^14 + ...",
				"q + q^3 + 2*q^5 + q^9 + 2*q^13 + 2*q^15 + 2*q^17 + 3*q^25 + q^27 + 2*q^29 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 0, 0, With[{m = 2 n + 1}, Sum[ KroneckerSymbol[ -36, d], { d, Divisors[ m]}]]]"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, n = 2*n+1; sumdiv( n, d, kronecker( -36, d)))}",
				"(PARI) {a(n) = if( n\u003c0, 0, n = 2*n+1; sumdiv( n, d, kronecker( 6, d) * (-1)^(d\\12)))}",
				"(PARI) {a(n) = if( n\u003c0, 0, if( n%6==1, n\\=3, 1); sumdiv( 2*n + 1, d, kronecker( -4, d)) )}",
				"(PARI) {a(n) = local(A, p, e); if( n\u003c0, 0, n = 2*n + 1; A = factor(n); prod( k=1, matsize(A)[1], if( p=A[k, 1], e=A[k, 2]; if( p==2, 0, if( p==3, 1, if( p%4==1, e+1, !(e%2)))))))}",
				"(PARI) {a(n) = local(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^3 + A)^3 * eta(x^4 + A) * eta(x^12 + A) / (eta(x + A) * eta(x^6 + A)^2), n))}"
			],
			"xref": [
				"Cf. A002175, A008441, A035154, A121444."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Michael Somos_, Nov 18 2006",
			"references": 11,
			"revision": 13,
			"time": "2021-03-12T22:24:44-05:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}