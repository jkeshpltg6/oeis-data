{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A053220",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 53220,
			"data": "1,5,16,44,112,272,640,1472,3328,7424,16384,35840,77824,167936,360448,770048,1638400,3473408,7340032,15466496,32505856,68157440,142606336,297795584,620756992,1291845632,2684354560,5570035712,11542724608,23890755584,49392123904",
			"name": "a(n) = (3*n-1) * 2^(n-2).",
			"comment": [
				"Coefficients in the hypergeometric series identity 1 - 5*x/(x + 4) + 16*x*(x - 1)/((x + 4)*(x + 6)) - 44*x*(x - 1)*(x - 2)/((x + 4)*(x + 6)*(x + 8)) + ... = 0, valid in the half-plane Re(x) \u003e 0. Cf. A276289. - _Peter Bala_, May 30 2019"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A053220/b053220.txt\"\u003eTable of n, a(n) for n = 1..500\u003c/a\u003e",
				"F. K. Hwang and C. L. Mallows, \u003ca href=\"http://dx.doi.org/10.1016/0097-3165(95)90097-7\"\u003eEnumerating nested and consecutive partitions\u003c/a\u003e, J. Combin. Theory Ser. A 70 (1995), no. 2, 323-333.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-4)."
			],
			"formula": [
				"G.f.: x*(1+x)/(1-2*x)^2.",
				"a(n) = (3*n-1) * 2^(n-2).",
				"E.g.f.: exp(2*x)*(1+3*x). The sequence 0, 1, 5, 16, ... has a(n) = ((3n-1)*2^n + 0^n)/4 (offset 0). It is the binomial transform of A032766. The sequence 1, 5, 16, ... has a(n) = (2+3n)*2^(n-1) (offset 0). It is the binomial transform of A016777. - _Paul Barry_, Jul 23 2003",
				"Row sums of A132776(n-1). - _Gary W. Adamson_, Aug 29 2007",
				"a(n+1) = det(f(i-j+1))_{1 \u003c= i, j \u003c= n}, where f(0) = 1, f(1) = 5 and for k \u003e 0, we have f(k+1) = 9 and f(-k) = 0. - _Mircea Merca_, Jun 23 2012"
			],
			"mathematica": [
				"ListCorrelate[{1, 1}, Table[n 2^(n - 1), {n, 0, 28}]] (* or *) ListConvolve[{1, 1}, Table[n 2^(n - 1), {n, 0, 28}]] (* _Ross La Haye_, Feb 24 2007 *)",
				"LinearRecurrence[{4, -4}, {1, 5}, 35] (* _Vladimir Joseph Stephan Orlovsky_, Jan 29 2012 *)",
				"Array[(3# - 1) 2^(# - 2) \u0026, 35] (* _Alonso del Arte_, Sep 04 2018 *)",
				"CoefficientList[Series[(1 + x)/(1 - 2 * x)^2, {x, 0, 50}], x] (* _Stefano Spezia_, Sep 04 2018 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c1,0,(3*n-1)*2^(n-2))",
				"(PARI) a(n)=(3*n-1)\u003c\u003c(n-2) \\\\ _Charles R Greathouse IV_, Apr 17 2012",
				"(MAGMA) [(3*n-1)*2^(n-2): n in [1..50]]; // _Vincenzo Librandi_, May 09 2011",
				"(Haskell)",
				"a053220 n = a056242 (n + 1) n  -- _Reinhard Zumkeller_, May 08 2014"
			],
			"xref": [
				"Cf. A053219, A053221, A132776, A276289.",
				"Center elements from triangle A053218. Also a diagonal of triangle A056242."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "Asher Auel (asher.auel(AT)reed.edu), Jan 01 2000",
			"references": 22,
			"revision": 53,
			"time": "2019-06-03T01:17:44-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}