{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002446",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2446,
			"id": "M4193 N1748",
			"data": "0,6,30,126,510,2046,8190,32766,131070,524286,2097150,8388606,33554430,134217726,536870910,2147483646,8589934590,34359738366,137438953470,549755813886,2199023255550,8796093022206,35184372088830",
			"name": "a(n) = 2^(2*n+1) - 2.",
			"reference": [
				"H. T. Davis, Tables of the Mathematical Functions. Vols. 1 and 2, 2nd ed., 1963, Vol. 3 (with V. J. Fisher), 1962; Principia Press of Trinity Univ., San Antonio, TX, Vol. 2, p. 283.",
				"A. Fletcher, J. C. P. Miller, L. Rosenhead and L. J. Comrie, An Index of Mathematical Tables. Vols. 1 and 2, 2nd ed., Blackwell, Oxford and Addison-Wesley, Reading, MA, 1962, Vol. 1, p. 112.",
				"S. A. Joffe, Calculation of the first thirty-two Eulerian numbers from central differences of zero, Quart. J. Pure Appl. Math. 47 (1914), 103-126.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A002446/b002446.txt\"\u003eTable of n, a(n) for n = 0..300\u003c/a\u003e",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (5,-4)."
			],
			"formula": [
				"G.f.: 6*x/((1-x)*(1-4*x)). - _Simon Plouffe_, see MAPLE line",
				"E.g.f.: (cos(i*x)-1)^2. - _Vladimir Kruchinin_, Oct 28 2012"
			],
			"maple": [
				"A002446:=6*z/((4*z-1)*(z-1)); # [Generating function. _Simon Plouffe_ in his 1992 dissertation.]"
			],
			"mathematica": [
				"f[n_] := Det[{{1, 1}, {1, 4}}^(n - 1) {{1, 2}, {1, 2}}]; Array[f, 30] (* _Robert G. Wilson v_, Jul 13 2011 *)",
				"2^(2*Range[0,30]+1)-2 (* or *) LinearRecurrence[{5,-4},{0,6},30] (* _Harvey P. Dale_, Sep 01 2016 *)"
			],
			"program": [
				"(MAGMA) [2^(2*n+1) - 2: n in [0..30]]; // _Vincenzo Librandi_, Jun 01 2011",
				"(PARI) a(n) = 2*(4^n - 1); \\\\ _G. C. Greubel_, Jul 04 2019",
				"(Sage) [2*(4^n -1) for n in (0..30)] # _G. C. Greubel_, Jul 04 2019",
				"(GAP) List([0..30], n-\u003e 2*(4^n - 1)) # _G. C. Greubel_, Jul 04 2019"
			],
			"xref": [
				"Equals 6 * A002450(n).",
				"A diagonal of the triangle in A241171."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Vincenzo Librandi_, Jun 01 2011"
			],
			"references": 7,
			"revision": 51,
			"time": "2021-03-12T22:32:35-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}