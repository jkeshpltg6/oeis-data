{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A038220",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 38220,
			"data": "1,3,2,9,12,4,27,54,36,8,81,216,216,96,16,243,810,1080,720,240,32,729,2916,4860,4320,2160,576,64,2187,10206,20412,22680,15120,6048,1344,128,6561,34992,81648,108864,90720,48384,16128,3072,256",
			"name": "Triangle whose (i,j)-th entry is binomial(i,j)*3^(i-j)*2^j.",
			"comment": [
				"Row sums give A000351; central terms give A119309. - _Reinhard Zumkeller_, May 14 2006",
				"Triangle of coefficients in expansion of (3 + 2x)^n, where n is a nonnegative integer. - _Zagros Lalo_, Jul 23 2018"
			],
			"reference": [
				"Shara Lalo and Zagros Lalo, Polynomial Expansion Theorems and Number Triangles, Zana Publishing, 2018, ISBN: 978-1-9995914-0-3, pp. 44, 48"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A038220/b038220.txt\"\u003eRows n = 0..125 of triangle, flattened\u003c/a\u003e",
				"B. N. Cyvin et al., \u003ca href=\"http://match.pmf.kg.ac.rs/electronic_versions/Match34/match34_109-121.pdf\"\u003eIsomer enumeration of unbranched catacondensed polygonal systems with pentagons and heptagons\u003c/a\u003e, Match, No. 34 (Oct 1996), pp. 109-121.",
				"\u003ca href=\"/index/Pas#Pascal\"\u003eIndex entries for triangles and arrays related to Pascal's triangle\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = A007318(n,k) * A036561(n,k). - _Reinhard Zumkeller_, May 14 2006",
				"G.f.: 1/(1 - 3*x - 2*x*y). - _Ilya Gutkovskiy_, Apr 21 2017",
				"T(0,0) = 1; T(n,k) = 3 T(n-1,k) + 2 T(n-1,k-1) for k = 0...n; T(n,k)=0 for n or k \u003c 0. - _Zagros Lalo_, Jul 23 2018"
			],
			"example": [
				"Triangle begins:",
				"   1;",
				"   3,   2;",
				"   9,  12,   4;",
				"  27,  54,  36,   8;",
				"  81, 216, 216,  96,  16;",
				"  ..."
			],
			"mathematica": [
				"t[0, 0] = 1; t[n_, k_] := t[n, k] = If[n \u003c 0 || k \u003c 0, 0, 3 t[n - 1, k] + 2 t[n - 1, k - 1]]; Table[t[n, k], {n, 0, 9}, {k, 0, n}] // Flatten (* _Zagros Lalo_, Jul 23 2018 *)",
				"Table[CoefficientList[ Expand[(3 + 2x)^n], x], {n, 0, 9}] // Flatten  (* _Zagros Lalo_, Jul 23 2018 *)",
				"Table[CoefficientList[Binomial[i, j] *3^(i - j)*2^j, x], {i, 0, 9}, {j, 0, i}] // Flatten (* _Zagros Lalo_, Jul 23 2018 *)"
			],
			"program": [
				"(Haskell)",
				"a038220 n k = a038220_tabl !! n !! k",
				"a038220_row n = a038220_tabl !! n",
				"a038220_tabl = iterate (\\row -\u003e",
				"   zipWith (+) (map (* 3) (row ++ [0])) (map (* 2) ([0] ++ row))) [1]",
				"-- _Reinhard Zumkeller_, May 26 2013, Apr 02 2011",
				"(PARI) T(i,j)=binomial(i,j)*3^(i-j)*2^j \\\\ _Charles R Greathouse IV_, Jul 19 2016"
			],
			"xref": [
				"Cf. A013620, A000079, A000244, A013613, A038221."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 7,
			"revision": 33,
			"time": "2018-07-28T11:53:25-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}