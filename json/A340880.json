{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A340880",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 340880,
			"data": "1,1,2,1,5,8,3,1,9,33,60,53,23,4,1,14,87,310,685,965,893,546,214,49,5,1,20,185,1040,3930,10469,20229,29037,31480,25975,16278,7643,2611,614,89,6,1,27,345,2760,15415,63535,199724,490019,955902,1504600,1931685,2037007",
			"name": "Irregular table: n-th row polynomial given by the formal power series expansion of Sum_{k \u003e= 0} (1 + q)^(n*k + k*(k+1)/2)* Product_{j = 1..k} (1 - (1 + q)^j), n \u003e= 1.",
			"comment": [
				"The polynomial identity Product_{k = 1..N} (1 - q^k) = 1 - Sum_{k = 0..N-1} q^(k+1)*Product_{i = 1..k} (1 - q^i), N = 1,2,..., is easily established by induction on N. This identity is the starting point in Andrew's exposition of Euler's proof of the pentagonal number theorem.",
				"Replacing q with 1 + q and letting N tend to infinity leads to the formal power series identity S(1,q) := Sum_{k \u003e= 0} (1 + q)^k*Product_{j = 1..k} (1 - (1 + q)^j) = 1/(1 + q).",
				"More generally, for n = 1,2,..., it can be shown that the series S(n,q) := Sum_{k \u003e= 0} (1 + q)^(n*k)*Product_{j = 1..k} (1 - (1 + q)^j) is a rational function of q of the form R(n,q)/(1 + q)^(n*(n+1)/2), where R(n,q) is a polynomial of degree n*(n-1)/2 in q. The proof makes use of the following formal series identity, valid for n \u003e= 1:",
				"t^n*Sum_{k \u003e= 0} x^(n*k+n*(n+1)/2)*(Product_{i = 1..k} 1 - t*x^i) + (-1)^(n+1)*(1-x)*(1-x^2)*...*(1-x^(n-1))*( Product_{i \u003e= 0} 1 - t*x^i ) = Sum_{k = 0..n-1} t^k*x^(k*(k+1)/2)*Product_{i = k+1..n-1} (x^i - 1).",
				"The first few polynomials are R(1,q) = 1, R(2,q) = 1 + 2*q and R(3,q) = 1 + 5*q + 8*q^2 + 3*q^3. The present table lists the coefficients of the polynomials R(n,q) for n \u003e= 1.",
				"Conjecture: For fixed integers q and N, the sequence ( R(n,q) mod N )n\u003e=1 is eventually periodic."
			],
			"link": [
				"George E. Andrews, \u003ca href=\"https://www.jstor.org/stable/2690367\"\u003eEuler's Pentagonal Number Theorem\u003c/a\u003e, Mathematics Magazine, Vol. 56, No. 5 (Nov., 1983), pp. 279-284."
			],
			"formula": [
				"n-th row polynomial: R(n,q) = Sum_{k = 0..n-1} x^(k*(k+1)/2)* ( Product_{j = k+1..n-1} x^j - 1 ), where x = 1 + q.",
				"In the q-adic topology, lim_{n -\u003e oo} R(2*n+1, q - 1) = -lim_{n -\u003e oo} R(2*n, q - 1) = Product_{n \u003e= 1} (1 - q^n)^2."
			],
			"example": [
				"The table begins",
				"n\\k    0   1    2     3     4      5      6      7      8      9    10",
				"----------------------------------------------------------------------",
				"  1    1,",
				"  2    1,  2,",
				"  3    1,  5,   8,    3,",
				"  4    1,  9,  33,   60,   53,    23,     4,",
				"  5    1, 14,  87,  310,  685,   965,   893,   546,   214,    49,   5,",
				"  6    1, 20, 185, 1040, 3930, 10469, 20229, 29037, 31480, 25975,  ..."
			],
			"maple": [
				"row := n -\u003e add( (1+q)^((1/2)*k*(k+1))*mul((1+q)^j-1, j = k+1..n-1), k = 0..n-1 ):",
				"seq(seq(coeff(row(n), q, k), k = 0..(1/2)*n*(n-1)), n = 1..10);"
			],
			"xref": [
				"Row sums A340881. Cf. A340882."
			],
			"keyword": "nonn,tabf,easy",
			"offset": "1,3",
			"author": "_Peter Bala_, Feb 16 2021",
			"references": 3,
			"revision": 16,
			"time": "2021-03-08T23:37:31-05:00",
			"created": "2021-03-08T23:37:31-05:00"
		}
	]
}