{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A152818",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 152818,
			"data": "1,1,1,1,4,2,1,12,18,6,1,32,108,96,24,1,80,540,960,600,120,1,192,2430,7680,9000,4320,720,1,448,10206,53760,105000,90720,35280,5040,1,1024,40824,344064,1050000,1451520,987840,322560,40320",
			"name": "Array read by antidiagonals: T(n,k) = (k+1)^n*(n+k)!/n!.",
			"comment": [
				"A009998/A119502 gives triangle of unreduced coefficients of polynomials defined by A152650/A152656. a(n) gives numerators with denominators n! for each row.",
				"Row 0 is A000142. Row 1 is formed from positive members of A001563. Row 2 is A055533. Column 0 is A000012. Column 1 is formed from positive members of A001787. Column 2 is A006043. Column 3 is A006044. - _Omar E. Pol_, Jan 06 2009"
			],
			"link": [
				"F. A. Haight, \u003ca href=\"http://www.jstor.org/stable/2333538\"\u003eOverflow at a traffic light\u003c/a\u003e, Biometrika, 46 (1959), 420-424. See page 422.",
				"F. A. Haight, \u003ca href=\"/A001787/a001787_3.pdf\"\u003eOverflow at a traffic light\u003c/a\u003e, Biometrika, 46 (1959), 420-424. (Annotated scanned copy)",
				"F. A. Haight, \u003ca href=\"/A001787/a001787_2.pdf\"\u003eLetter to N. J. A. Sloane, n.d.\u003c/a\u003e"
			],
			"formula": [
				"E.g.f. for array as a triangle: exp(x)/(1-t*x*exp(x)) = 1+(1+t)*x+(1+4*t+2*t^2)*x^2/2!+(1+12*t+18*t^2+6*t^3)*x^3/3!+.... E.g.f. is int {z = 0..inf} exp(-z)*F(x,t*z), (x and t chosen sufficiently small for the integral to converge), where F(x,t) = exp(x*(1+t*exp(x))) is the e.g.f. for A154372. - _Peter Bala_, Oct 09 2011",
				"From the e.g.f., the row polynomials R(n,t) satisfy the recursion R(n,t) = 1 + t*sum {k = 0..n-1} n!/(k!*(n-k-1)!)*R(n-k-1,t). The polynomials 1/n!*R(n,x) are the polynomials P(n,x) of A152650. Row sums of triangle are A072597. - _Peter Bala_, Oct 09 2011"
			],
			"example": [
				"a(8)=18. Then a(6)+a(7)+a(8)+a(9)=A072597(3)=37.",
				"From _Omar E. Pol_, Jan 06 2009: (Start)",
				"T(2,3)=960 because (3+1)^2*(2+3)!/2! = 16*120/2 = 960.",
				"Array begins:",
				"1, 1, 2, 6, 24, 120,",
				"1, 4, 18, 96, 600,",
				"1, 12, 108, 960,",
				"1, 32, 540,",
				"1, 80,",
				"1,",
				"(End)"
			],
			"mathematica": [
				"len = 45; m = 1 + Ceiling[Sqrt[len]]; Sort[Flatten[#, 1] \u0026[MapIndexed[ {(2 + #2[[1]]^2 + (#2[[2]] - 1)*#2[[2]] + #2[[1]]*(2*#2[[2]] - 3))/ 2, #1}\u0026 ,Table[(k + 1)^n*(n + k)!/n!, {n, 0, m}, {k, 0, m}], {2}]]][[All, 2]][[1 ;; len]] (* From _Jean-François Alcover_, May 27 2011 *)"
			],
			"program": [
				"(Sage)",
				"def A152818_row(n):",
				"    R.\u003cx\u003e = ZZ[]",
				"    P = add((n-k+1)^k*x^(n-k+1)*factorial(n)/factorial(k) for k in (0..n))",
				"    return P.coefficients()",
				"for n in (0..5): print(A152818_row(n))  # _Peter Luschny_, May 03 2013",
				"(PARI) T(n,k) = (k+1)^n*(n+k)!/n! \\\\ _Charles R Greathouse IV_, Sep 10 2016"
			],
			"xref": [
				"Cf. A000012, A000142, A001563, A001787, A006043, A006044, A055533, A072597 (row sums), A152650, A154372."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Paul Curtz_, Dec 13 2008",
			"ext": [
				"Better definition, extended and edited by _Omar E. Pol_ and _N. J. A. Sloane_, Jan 05 2009"
			],
			"references": 13,
			"revision": 30,
			"time": "2020-03-05T07:04:56-05:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}