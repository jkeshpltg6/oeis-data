{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A127672",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 127672,
			"data": "2,0,1,-2,0,1,0,-3,0,1,2,0,-4,0,1,0,5,0,-5,0,1,-2,0,9,0,-6,0,1,0,-7,0,14,0,-7,0,1,2,0,-16,0,20,0,-8,0,1,0,9,0,-30,0,27,0,-9,0,1,-2,0,25,0,-50,0,35,0,-10,0,1,0,-11,0,55,0,-77,0,44,0,-11,0,1,2,0,-36,0,105,0,-112,0,54,0,-12,0,1,0,13,0,-91",
			"name": "Monic integer version of Chebyshev T-polynomials (increasing powers).",
			"comment": [
				"The row polynomials R(n,x):=sum(a(n,m)*x^m),m=0..n) have been called Chebyshev C_n(x) polynomials in the Abramowitz-Stegun handbook, p. 778, 22.5.11",
				"(see A049310 for the reference, and note that on p. 774 the S and C polynomials have been mixed up in older printings). - _Wolfdieter Lang_, Jun 03 2011",
				"This is a signed version of triangle A114525.",
				"The unsigned column sequences (without zeros) are, for m=1..11: A005408, A000290, A000330, A002415, A005585, A040977, A050486, A053347, A054333, A054334, A057788.",
				"The row polynomials R(n,x):= sum(a(n,m)*x*m,m=0..n), give for n=2,3,...,floor(N/2) the positive zeros of the Chebyshev S(N-1,x)-polynomial (see A049310) in terms of its largest zero rho(N):= 2*cos(Pi/N) by putting x=rho(N). The order of the positive zeros is falling: n=1 corresponds to the largest zero rho(N) and n=floor(N/2) to the smallest positive zero. Example N=5: rho(5)=phi (golden section), R(2,phi)= phi^2-2 = phi-1, the second largest (and smallest) positive zero of S(4,x). - _Wolfdieter Lang_, Dec 01 2010",
				"The row polynomial R(n,x), for n\u003e=1, factorizes into minimal polynomials of 2*cos(Pi/k), called C(k,x), with coefficients given in A187360, as follows.",
				"  R(n,x) = product(C(2*n/d,x),d|oddpart(n))",
				"         = product(C(2^{k+1}*d,x),d|oddpart(n)),",
				"  with oddpart(n)=A000265(n), and 2^k is the largest power of 2 dividing n, where k=0,1,2,...",
				"(Proof: R and C are monic, the degree on both sides coincides, and the zeros of R(n,x) appear all on the r.h.s.). - _Wolfdieter Lang_, Jul 31 2011 [Theorem 1B, eq. (43) in the W. Lang link. - _Wolfdieter Lang_, Apr 13 2018]",
				"The zeros of the row polynomials R(n,x) are 2*cos(Pi*(2*k+1)/(2*n)), k=0,1, ..., n-1; n\u003e=1 (from those of the Chebyshev T-polynomials). - _Wolfdieter Lang_, Sep 17 2011",
				"The discriminants of the row polynomials R(n,x) are found under A193678. - _Wolfdieter Lang_, Aug 27 2011",
				"The determinant of the N x N matrix M(N) with entries M(N;n,m) = R(m-1,x[n]), 1 \u003c= n,m \u003c= N, N\u003e=1, and any x[n], is identical with twice the Vandermondian Det(V(N)) with matrix entries V(N;n,m) = x[n]^(m-1). This is an instance of the general theorem given in the Vein-Dale reference on p. 59. Note that R(0,x) = 2 (not 1). See also the comments from Aug 26 2013 under A049310 and from Aug 27 2013 under A000178. - _Wolfdieter Lang_, Aug 27 2013",
				"This triangle a(n,m) is also used to express in the regular (2*(n+1))-gon, inscribed in a circle of radius R, the length ratio side/R, called s(2*(n+1)), as a polynomial in rho(2*(n+1)), the length ratio (smallest diagonal)/side. See the bisections ((-1)^(k-s))*A111125(k,s) and A127677 for comments and examples. - _Wolfdieter Lang_, Oct 05 2013",
				"From _Tom Copeland_, Nov 08 2015: (Start)",
				"These are the characteristic polynomials a_n(x) = 2 T_n(x/2) for the adjacency matrix of the Coxeter simple Lie algebra  B_n, related to the Cheybshev polynomials of the first kind, T_n(x) = cos(n*q) with x = cos(q) (see p. 20 of Damianou).  Given the polynomial (x - t)(x - 1/t) = 1 -(t + 1/t) x + x^2 = e2 - e1 x + x^2, the symmetric power sums p_n(t,1/t) = t^n + t^(-n) of the zeros of this polynomial may be expressed in terms of the elementary symmetric polynomials e1 = t + 1/t = y and e2 = t*1/t = 1 as p_n(t,1/t) = a_n(y) = F(n,-y,1,0,0,..), where F(n,b1,b2,..,bn) are the Faber polynomials of A263916.",
				"The partial sum of the first n+1 rows given t and y = t + 1/t is PS(n,t) =",
				"sum(k=0 to n, a_n(y)) = [t^(n/2) + t^(-n/2)][t^((n+1)/2) - t^(-(n+1)/2)] / [t^(1/2) - t^(-1/2)]. (For n prime, this is related simply to the cyclotomic polynomials.)",
				"Then a_n(y) = PS(n,t) - PS(n-1,t), and for t = e^(iq), y = 2 cos(q), and, therefore,  a_n(2 cos(q)) =  PS(n,e^(iq)) - PS(n-1,e^(iq)) = 2 cos(nq) = 2 T_n(cos(q)) with PS(n,e^(iq)) = 2 cos(nq/2) sin((n+1)q/2) / sin(q/2).",
				"(End)",
				"R(45, x) is the famous polynomial used by Adriaan van Roomen (Adrianus Romanus) in his Ideae mathematicae from 1593 to pose four problems, solved by Viète. See e.g., the Havil reference, pp. 69-74. - _Wolfdieter Lang_, Apr 28 2018",
				"From _Wolfdieter Lang_, May 05 2018: (Start)",
				"Some identities for the row polynomials R(n, x) following from the known ones for Chebyshev T-polynomials (A053120) are:",
				"(1) R(-n, x) = R(n, x).",
				"(2) R(n*m, x) = R(n, R(m, x)) = R(m, R(n, x)).",
				"(3) R(2*k+1, x) = (-1)^k*x*S(2*k, sqrt(4-x^2)), k \u003e= 0, with the S row polynomials of A049310.",
				"(4) R(2*k, x) = R(k, x^2-2), k \u003e= 0.",
				"(End)",
				"For y = z^n + z^(-n) and x = z+z^(-1), Hirzebruch notes that y(z) = R(n,x) for the row polynomial of this entry. - _Tom Copeland_, Nov 09 2019"
			],
			"reference": [
				"Julian Havil, The Irrationals, A Story of the Numbers You Can't Count On, Princeton University Press, Princeton and Oxford, 2012, pp. 69-74.",
				"F. Hirzebruch et al., Manifolds and Modular Forms, Vieweg 1994 pp. 77, 105.",
				"R. Vein and P. Dale, Determinants and Their Applications in Mathematical Physics, Springer, 1999."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A127672/b127672.txt\"\u003eTable of n, a(n) for n = 0..10010\u003c/a\u003e (rows 0 to 140, flattened)",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards Applied Math.Series 55, Tenth Printing, 1972.",
				"T. Copeland, \u003ca href=\"http://tcjpn.wordpress.com/2015/10/12/the-elliptic-lie-triad-kdv-and-ricattt-equations-infinigens-and-elliptic-genera/\"\u003eAddendum to Elliptic Lie Triad\u003c/a\u003e",
				"P. Damianou , \u003ca href=\"https://arxiv.org/abs/1110.6620\"\u003eOn the characteristic polynomials of Cartan matrices and Chebyshev polynomials\u003c/a\u003e, arXiv preprint arXiv:1110.6620 [math.RT], 2011-2014.",
				"Wolfdieter Lang, \u003ca href=\"/A127672/a127672.pdf\"\u003eRow polynomials.\u003c/a\u003e",
				"Wolfdieter Lang, \u003ca href=\"http://arxiv.org/abs/1210.1018\"\u003eThe field Q(2cos(pi/n)), its Galois group and length ratios in the regular n-gon\u003c/a\u003e, arXiv:1210.1018 [math.GR], 2012-2017.",
				"Wolfdieter Lang, \u003ca href=\"https://arxiv.org/abs/2008.04300\"\u003eOn the Equivalence of Three Complete Cyclic Systems of Integers\u003c/a\u003e, arXiv:2008.04300 [math.NT], 2020.",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e"
			],
			"formula": [
				"a(n,0)=0 if n is odd, a(n,0)=2*(-1)^(n/2) if n is even, else a(n,m)=t(n,m)/2^(m-1) with t(n,m):=A053120(n,m) (coefficients of Chebyshev T-polynomials).",
				"G.f. for m-th column (signed triangle): 2/(1+x^2) if m=0 else (x^m)*(1-x^2)/(1+x^2)^(m+1).",
				"Riordan type matrix ((1-x^2)/(1+x^2),x/(1+x^2)) if one puts a(0,0)=1 (instead of 2).",
				"O.g.f. for row polynomials: R(x,z):=sum(n\u003e=0, R(n,x)*z^n )=(2-x*z)*S(x,z), with the o.g.f. S(x,z)= 1/(1-x*z+z^2) for the S-polynomials (see A049310).",
				"  Note that R(n,x) = R(2*n,sqrt(2+x)). n\u003e=0 (from the o.g.f.s of both sides). - _Wolfdieter Lang_, Jun 03 2011",
				"a(n,m) := 0 if n\u003cm or n+m odd; a(n,0)= 2*(-1)^(n/2) (n even); else a(n,m)=((-1)^((n+m)/ 2+m))*n*binomial((n+m)/2-1,m-1)/m.",
				"Recursion for n \u003e= 2 and m\u003e=2: a(n,m) = a(n-1,m-1)-a(n-2,m), a(n,m)=0 if n\u003cm, a(2*k,1)=0, a(2*k+1,1)=(2*k+1)*(-1)^k. In addition, for column m=0: a(2*k,0)= 2*(-1)^k, a(2*k+1,0)=0, k\u003e=0.",
				"Chebyshev T(n,x) = sum(m=0..n, a(n,m)*(2^(m-1))*x^m ). - _Wolfdieter Lang_, Jun 03 2011",
				"R(n,x) = 2*T(n,x/2) = S(n,x) - S(n-2,x), n\u003e=0, with Chebyshev's T- and S-polynomials, showing that they are integer and monic polynomials. - _Wolfdieter Lang_, Nov 08 2011",
				"From _Tom Copeland_, Nov 08 2015: (Start)",
				"a(n,x)  = sqrt[2 + a(2n,x)], or 2 + a(2n,x) = (a(n,x))^2, is a reflection of the relation of the Chebyshev polynomials of the first kind to the cosine and the half-angle formula, (cos(q/2))^2 = (1 + cos(q))/2.",
				"Examples: For n = 2, -2 + x^2 = sqrt(2 + 2 - 4 x^2 + x^4).",
				"For n = 3, -3 x + x^3 =  sqrt(2 - 2 + 9 x^2 - 6 x^4 + x^6).",
				"(End)",
				"L(x,h1,h2) = -log(1 - h1*x + h2*x^2) = Sum_{n\u003e0} F(n,-h1,h2,0,..,0) x^n/n =  h1 x + (-2h2 + h1^2) x^2/2 + (-3h1 h2 + h1^3) x^3/3 + .... is a log series generator of the bivariate row polynomials where T(0,0) = 0 and F(n,b1,b2,..,bn) are the Faber polynomials of A263916. Exp(L(x,h1,h2)) = 1 / (1 - h1*x + h2*x^2 ) is the o.g.f. of A049310. - _Tom Copeland_, Feb 15 2016"
			],
			"example": [
				"Row n=4: [2,0,-4,0,1] stands for the polynomial 2*y^0 - 4*y^2 + 1*y^4. With y^m replaced by 2^(m-1)*x^m this becomes T(4,x)= 1-8*x^2+8*x^4.",
				"Triangle begins:",
				"n\\m   0   1   2   3    4    5   6   7   8   9 10 ...",
				"0:    2",
				"1:    0   1",
				"2:   -2   0   1",
				"3:    0  -3   0   1",
				"4:    2   0  -4   0    1",
				"5:    0   5   0  -5    0    1",
				"6:   -2   0   9   0   -6    0   1",
				"7:    0  -7   0  14    0   -7   0   1",
				"8:    2   0 -16   0   20    0  -8   0    1",
				"9:    0   9   0 -30    0   27   0  -9    0  1",
				"10:  -2   0  25   0  -50    0  35   0  -10  0  1 ...",
				"Factorization into minimal C-polynomials:",
				"R(12,x) = R((2^2)*3,x) = C(24,x)*C(8,x) = C((2^3)*1,x)*C((2^3)*3,x). - _Wolfdieter Lang_, Jul 31 2011"
			],
			"maple": [
				"seq(seq(coeff(2*orthopoly[T](n,x/2),x,j),j=0..n),n=0..20); # _Robert Israel_, Aug 04 2015"
			],
			"mathematica": [
				"a[n_, k_] := SeriesCoefficient[(2 - t*x)/(1 - t*x + x^2), {x, 0, n}, {t, 0, k}]; Flatten[Table[a[n, k], {n, 0, 12}, {k, 0, n}]] (* _L. Edson Jeffery_, Nov 02 2017 *)"
			],
			"xref": [
				"Row sums (signed): A057079(n-1). Row sums (unsigned): A000032(n) (Lucas numbers). Alternating row sums: A099837(n+3).",
				"Bisection: A127677 (even n triangle, without zero entries), ((-1)^(n-m))*A111125(n, m) (odd n triangle, without zero entries).",
				"Cf. A049310, A053120, A108045, A263916."
			],
			"keyword": "sign,tabl,easy",
			"offset": "0,1",
			"author": "_Wolfdieter Lang_, Mar 07 2007",
			"ext": [
				"Name changed and table rewritten by _Wolfdieter Lang_, Nov 08 2011"
			],
			"references": 81,
			"revision": 121,
			"time": "2020-10-15T16:35:27-04:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}