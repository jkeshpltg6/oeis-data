{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A343627",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 343627,
			"data": "0,0,0,0,0,1,2,3,1,3,7,2,2,5,5,4,8,1,9,1,9,6,7,4,4,4,8,9,4,7,1,2,4,4,4,4,0,0,3,9,3,6,6,6,9,0,5,7,8,6,6,2,6,3,7,0,7,2,8,1,9,6,3,7,0,6,2,0,2,1,0,5,7,4,1,2,0,6,7,2,6,0,0,6,9,5,5,9,2,2,1,2,7,4,9,2,4,8,2,5",
			"name": "Decimal expansion of the Prime Zeta modulo function P_{3,1}(7) = Sum 1/p^7 over primes p == 1 (mod 3).",
			"comment": [
				"The Prime Zeta modulo function at 7 for primes of the form 3k+1 is Sum_{primes in A002476} 1/p^7 = 1/7^7 + 1/13^7 + 1/19^7 + 1/31^7 + ...",
				"The complementary Sum_{primes in A003627} 1/p^7 is given by P_{3,2}(7) = A085967 - 1/3^7 - (this value here) = 0.0078253541130504928742517... = A343607."
			],
			"link": [
				"R. J. Mathar, \u003ca href=\"http://arxiv.org/abs/1008.2547\"\u003eTable of Dirichlet L-series and Prime Zeta Modulo Functions for Small Moduli\u003c/a\u003e, arXiv:1008.2547 [math.NT], 2010-2015, p.21.",
				"\u003ca href=\"/index/Z#zeta_function\"\u003eOEIS index to entries related to the (prime) zeta function\u003c/a\u003e."
			],
			"example": [
				"P_{3,1}(7) = 1.231372255481919674448947124444003936669057866...*10^-6"
			],
			"mathematica": [
				"With[{s=7}, Do[Print[N[1/2 * Sum[(MoebiusMu[2*n + 1]/(2*n + 1)) * Log[(Zeta[s + 2*n*s]*(Zeta[s + 2*n*s, 1/6] - Zeta[s + 2*n*s, 5/6])) / ((1 + 2^(s + 2*n*s))*(1 + 3^(s + 2*n*s)) * Zeta[2*(1 + 2*n)*s])], {n, 0, m}], 120]], {m, 100, 500, 100}]] (* adopted from _Vaclav Kotesovec_'s code in A175645 *)"
			],
			"program": [
				"(PARI) s=0; forprimestep(p=1, 1e8, 3, s+=1./p^7); s \\\\ For illustration: primes up to 10^N give 6N+2 (= 50 for N=8) correct digits.",
				"(PARI) A343627_upto(N=100)={localprec(N+5);digits((PrimeZeta31(7)+1)\\.1^N)[^1]} \\\\ cf. A175644 for PrimeZeta31"
			],
			"xref": [
				"Cf. A175645, A343624 - A343629 (P_{3,1}(3..9): same for 1/p^n, n = 3..9), A343607 (P_{3,2}(7): same for p==2 (mod 3)), A086037 (P_{4,1}(7): same for p==1 (mod 4)).",
				"Cf. A085967 (PrimeZeta(7)), A002476 (primes of the form 3k+1)."
			],
			"keyword": "cons,nonn",
			"offset": "0,7",
			"author": "_M. F. Hasler_, Apr 23 2021",
			"references": 2,
			"revision": 8,
			"time": "2021-04-24T03:12:52-04:00",
			"created": "2021-04-23T20:58:24-04:00"
		}
	]
}