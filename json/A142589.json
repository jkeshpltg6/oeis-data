{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A142589",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 142589,
			"data": "1,1,1,1,2,1,1,6,3,1,1,24,15,4,1,1,120,105,28,5,1,1,720,945,280,45,6,1,1,5040,10395,3640,585,66,7,1,1,40320,135135,58240,9945,1056,91,8,1,1,362880,2027025,1106560,208845,22176,1729,120,9,1,1,3628800,34459425,24344320,5221125,576576,43225,2640,153,10,1",
			"name": "Square array T(n,m) = Product_{i=0..m} (1+n*i) read by antidiagonals.",
			"comment": [
				"Antidiagonal sums are {1, 2, 4, 11, 45, 260, 1998, 19735, 244797, 3729346, 68276276, ...}."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A142589/b142589.txt\"\u003eAntidiagonal rows n = 0..100, flattened\u003c/a\u003e"
			],
			"example": [
				"The transpose of the array is:",
				"    1,    1,     1,     1,      1,      1,      1,      1,     1,",
				"    1,    2,     3,     4,      5,      6,      7,      8,     9,",
				"    1,    6,    15,    28,     45,     66,     91,     120,   153, ... A000384",
				"    1,   24,   105,   280,    585,   1056,   1729,    2640,  3825, ... A011199",
				"    1,  120,   945,  3640,   9945,  22176,  43225,   76560, 126225,... A011245",
				"    1,  720, 10395, 58240, 208845, 576576, 1339975, 2756160,...",
				"        /      |       \\       \\",
				"   A000142  A001147  A007559  A007696"
			],
			"maple": [
				"T:= (n, k)-\u003e `if`(n=0, 1, mul(j*k+1, j=0..n)):",
				"seq(seq(T(n-k, k), k=0..n), n=0..12); # _G. C. Greubel_, Mar 05 2020"
			],
			"mathematica": [
				"T[n_, k_]= If[n==0, 1, Product[1 + k*i, {i,0,n}]]; Table[T[n-k, k], {n,0,10}, {k,0,n}]//Flatten"
			],
			"program": [
				"(PARI) T(n, k) = if(n==0, 1, prod(j=0, n, j*k+1) );",
				"for(n=0, 12, for(k=0, n, print1(T(n-k, k), \", \"))) \\\\ _G. C. Greubel_, Mar 05 2020",
				"(MAGMA)",
				"function T(n,k)",
				"  if k eq 0 or n eq 0 then return 1;",
				"  else return (\u0026*[j*k+1: j in [0..n]]);",
				"  end if; return T; end function;",
				"[T(n-k,k): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Mar 05 2020",
				"(Sage)",
				"def T(n, k):",
				"    if (k==0 and n==0): return 1",
				"    else: return product(j*k+1 for j in (0..n))",
				"[[T(n-k, k) for k in (0..n)] for n in (0..12)] # _G. C. Greubel_, Mar 05 2020"
			],
			"xref": [
				"Cf. A000142, A006882(2n-1) = A001147, A007661(3n-2) = A007559, A007662(4n-3) = A007696, A153274."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Roger L. Bagula_ and _Gary W. Adamson_, Sep 22 2008",
			"ext": [
				"Edited by _M. F. Hasler_, Oct 28 2014",
				"More terms added by _G. C. Greubel_, Mar 05 2020"
			],
			"references": 4,
			"revision": 19,
			"time": "2020-03-07T12:37:18-05:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}