{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A349250",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 349250,
			"data": "8,7,5,10,12,14,16,18,20,22,6,5,3,8,10,12,14,16,18,20,4,2,6,8,10,12,14,16,18,20,6,8,8,12,10,12,14,16,18,20,10,10,10,10,12,12,14,16,18,20,12,12,12,12,12,16,14,16,18,20,14,14,14,14,14,14,14,16,18,20",
			"name": "Size of the orbit of n under iterations of the digit-census function A348783.",
			"comment": [
				"Equivalently: the number of times the function A348783 must be iterated, starting with n, before a value occurs for the second time.",
				"It can be shown that the trajectory of all numbers end in one of the two limit cycles (21, 110) or (22, 200, 102, 111, 30, 1001). Therefore a(n) \u003e= 2 with equality iff n = 21 or n = 110."
			],
			"example": [
				"Under iterations of A348783, 0 -\u003e 1 -\u003e 10 -\u003e 11 -\u003e 20 -\u003e 101 -\u003e 21 -\u003e 110 -\u003e 21 -\u003e ...: here, the cycle (21, 110) is reached, so the orbit of 0 is O(0) = {0, 1, 10, 11, 20, 101, 21, 110}, of size a(0) = 8. We may also deduce that a(1) = 7, a(10) = 6, a(11) = 5, a(20) = 4, a(101) = 3 and a(21) = a(110) = 2 which is the length of the cycle these elements belong to.",
				"Similarly, 2 -\u003e 100 -\u003e 12 -\u003e 110 -\u003e 21 -\u003e ..., so the orbit of 2 is O(2) = {2, 100, 12, 110, 21}, of size a(2) = 5. We also deduce a(100) = 4 and a(12) = 3.",
				"Then, 3 -\u003e 1000 -\u003e 13 -\u003e 1010 -\u003e 22 -\u003e 200 -\u003e 102 -\u003e 111 -\u003e 30 -\u003e 1001 -\u003e 22 -\u003e 200 -\u003e ..., here the cycle (22, 200, 102, 111, 30, 1001) is reached, and we have the orbit O(3) = {3, 1000, 13, 1010, 22, 200, 102, 111, 30, 1001}, of size a(3) = 10, and also a(1000) = 9, a(13) = 8, a(1010) = 7 and a(n) = 6 for the elements of that cycle."
			],
			"mathematica": [
				"Array[-1 + Length@ NestWhileList[FromDigits@ RotateLeft@ Reverse@ DigitCount[#] \u0026, #, UnsameQ[##] \u0026, All] \u0026, 60, 0] (* _Michael De Vlieger_, Nov 23 2021 *)"
			],
			"program": [
				"(PARI) apply( {A349250(n,S=[n])=while(!setsearch(S,n=A348783(n)),S=setunion(S,[n]));#S}, [0..99])",
				"(Python)",
				"def f(n):",
				"    s = str(n)",
				"    return int(\"\".join(str(s.count(d)) for d in \"9876543210\").lstrip(\"0\"))",
				"def a(n):",
				"    orbit, fn = {n}, f(n)",
				"    while fn not in orbit:",
				"        orbit.add(fn)",
				"        n, fn = fn, f(fn)",
				"    return len(orbit)",
				"print([a(n) for n in range(70)]) # _Michael S. Branicky_, Nov 18 2021"
			],
			"xref": [
				"Cf. A348783 (\"digit census function\"), A349249 (number of iterations to reach a limiting cycle)."
			],
			"keyword": "nonn,base",
			"offset": "0,1",
			"author": "_M. F. Hasler_, Nov 17 2021",
			"references": 3,
			"revision": 18,
			"time": "2021-12-04T12:42:17-05:00",
			"created": "2021-12-04T12:42:17-05:00"
		}
	]
}