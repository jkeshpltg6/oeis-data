{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A331284",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 331284,
			"data": "1,1,1,1,1,1,1,2,1,1,1,2,1,1,1,1,1,2,1,2,1,1,1,1,1,1,3,2,1,2,1,2,1,1,1,1,1,1,1,1,1,2,1,2,3,1,1,3,1,2,1,2,1,2,1,1,1,1,1,1,1,1,3,1,1,2,1,2,1,2,1,2,1,1,3,2,1,2,1,3,1,1,1,1,1,1,1,1,1,2,1,2,1,1,1,1,1,2,3,1,1,2,1,1,3",
			"name": "Number of values of k, 1 \u003c= k \u003c= n, with A329605(k) = A329605(n), where A329605 is the number of divisors of primorial inflation of n (A108951).",
			"comment": [
				"Ordinal transform of A329605, or equally, of A329606."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A331284/b331284.txt\"\u003eTable of n, a(n) for n = 1..65537\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#prime_indices\"\u003eIndex entries for sequences computed from indices in prime factorization\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#primorial_numbers\"\u003eIndex entries for sequences related to primorial numbers\u003c/a\u003e"
			],
			"formula": [
				"a(A331285(n)) = n for all n."
			],
			"mathematica": [
				"c[n_] := c[n] = If[n == 1, 1, Module[{f = FactorInteger[n], p, e}, If[Length[f] \u003e 1, Times @@ c /@ Power @@@ f, {{p, e}} = f; Times @@ (Prime[Range[PrimePi[p]]]^e)]]];",
				"A329605[n_] := DivisorSigma[0, c[n]];",
				"Module[{b}, b[_] = 0;",
				"a[n_] := With[{t = A329605[n]}, b[t] = b[t] + 1]];",
				"Array[a, 105] (* _Jean-François Alcover_, Jan 12 2022 *)"
			],
			"program": [
				"(PARI)",
				"up_to = 65537;",
				"ordinal_transform(invec) = { my(om = Map(), outvec = vector(length(invec)), pt); for(i=1, length(invec), if(mapisdefined(om,invec[i]), pt = mapget(om, invec[i]), pt = 0); outvec[i] = (1+pt); mapput(om,invec[i],(1+pt))); outvec; };",
				"A329605(n) = if(1==n,1,my(f=factor(n),e=1,m=1); forstep(i=#f~,1,-1, e += f[i,2]; m *= e^(primepi(f[i,1])-if(1==i,0,primepi(f[i-1,1])))); (m));",
				"v331284 = ordinal_transform(vector(up_to, n, A329605(n)));",
				"A331284(n) = v331284[n];"
			],
			"xref": [
				"Cf. A000005, A108951, A329605, A329606, A331285 (positions of the first occurrences of each n, also positions of records).",
				"Cf. also A067004."
			],
			"keyword": "nonn,changed",
			"offset": "1,8",
			"author": "_Antti Karttunen_, Jan 14 2020",
			"references": 3,
			"revision": 12,
			"time": "2022-01-12T03:21:16-05:00",
			"created": "2020-01-14T22:19:02-05:00"
		}
	]
}