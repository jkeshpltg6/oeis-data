{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A245421",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 245421,
			"data": "1,2,2,2,1,-1,-2,-3,-2,1,4,6,5,1,-5,-11,-12,-7,3,15,22,19,5,-15,-32,-36,-22,8,40,58,50,12,-41,-84,-93,-54,22,103,148,124,32,-96,-200,-219,-128,46,231,330,275,67,-216,-439,-477,-275,107,501,708,590,146",
			"name": "Expansion of q^(-1) * f(-q^2, -q^7) * f(-q^4, -q^5) / f(-q, -q^8)^2 in powers of q where f() is Ramanujan's two-variable theta function.",
			"comment": [
				"Number 11 of the 15 generalized eta-quotients listed in Table I of Yang 2004.",
				"A generator (Hauptmodul) of the function field associated with congruence subgroup Gamma_1(9). [Yang 2004]",
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. A. Edgar, \u003ca href=\"/A245421/b245421.txt\"\u003eTable of n, a(n) for n = -1..1003\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"Y. Yang, \u003ca href=\"http://dx.doi.org/10.1112/S0024609304003510\"\u003eTransformation formulas for generalized Dedekind eta functions\u003c/a\u003e, Bull. London Math. Soc. 36 (2004), no. 5, 671-682. See p. 679, Table 1."
			],
			"formula": [
				"Expansion of q^(-1) * f(-q) * f(-q^9)^3 / (f(-q^3) * f(-q, -q^8)^3) in powers of q where f() is a Ramanujan theta function.",
				"Euler transform of period 9 sequence [ 2, -1, 0, -1, -1, 0, -1, 2, 0, ...].",
				"Given g.f. A(q), then 0 = f(A(q), A(q^2)) where f(u, v) = (u^2 + v) * (u*v^2 + u - 1) - 2*u*v * (u + v - 1)^2.",
				"Given g.f. A(q), then 0 = f(A(q), A(q^3)) where f(u, v) = (v^2 - v + 1) * (u^3 - v)  - 3*u*v * (u - 1) * (2*v - 1);",
				"a(n) = A245424(n) unless n=0.",
				"G.f. T(q) = 1/q + 2 + 2*q + ... for this function is cubically related to T9B(q) of A058091: T9B = T - 2 - 1/T - 1/(T-1). - _G. A. Edgar_, Apr 13 2017"
			],
			"example": [
				"G.f. = 1/q + 2 + 2*q + 2*q^2 + q^3 - q^4 - 2*q^5 - 3*q^6 - 2*q^7 + q^8 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ 1/q  QPochhammer[ q] / (QPochhammer[ q^3] (QPochhammer[ q^1, q^9] QPochhammer[ q^8, q^9])^3), {q, 0, n}];",
				"a[ n_] := If[ n \u003c -1, 0, With[{m = n + 1}, SeriesCoefficient[ 1/q Product[ (1 - q^k)^{-2, 1, 0, 1, 1, 0, 1, -2, 0}[[Mod[k, 9, 1]]], {k, m}], {q, 0, n}]]];",
				"a[ n_] := SeriesCoefficient[ 1/q QPochhammer[ q^2, q^9] QPochhammer[ q^4, q^9] QPochhammer[ q^5, q^9] QPochhammer[ q^7, q^9] / (QPochhammer[ q^1, q^9] QPochhammer[ q^8, q^9])^2, {q, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c-1, 0, n++; polcoeff( prod(k=1, n, (1 - x^k + x * O(x^n))^[0, -2, 1,0, 1, 1, 0, 1, -2][k%9 + 1]), n))};"
			],
			"xref": [
				"Cf. A245424."
			],
			"keyword": "sign",
			"offset": "-1,2",
			"author": "_Michael Somos_, Jul 21 2014",
			"references": 2,
			"revision": 12,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2014-07-21T13:35:04-04:00"
		}
	]
}