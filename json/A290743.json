{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A290743",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 290743,
			"data": "2,3,4,6,8,11,14,18,22,27,32,38,44,51,58,66,74,83,92,102,112,123,134,146,158,171,184,198,212,227,242,258,274,291,308,326,344,363,382,402,422,443,464,486,508,531,554,578,602,627,652,678,704,731,758",
			"name": "Maximum number of distinct Lyndon factors that can appear in words of length n over an alphabet of size 2.",
			"comment": [
				"See theorem 1 of reference for formula."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A290743/b290743.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Amy Glen, Jamie Simpson, and W. F. Smyth, \u003ca href=\"https://doi.org/10.37236/6915\"\u003eCounting Lyndon Factors\u003c/a\u003e, Electronic Journal of Combinatorics 24(3) (2017), #P3.28.",
				"Ryo Hirakawa, Yuto Nakashima, Shunsuke Inenaga, and Masayuki Takeda, \u003ca href=\"https://arxiv.org/abs/2106.01190\"\u003eCounting Lyndon Subsequences\u003c/a\u003e, arXiv:2106.01190 [math.CO], 2021. See MDF(n, s).",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,0,-2,1)."
			],
			"formula": [
				"a(n) = binomial(n+1,2) - (s-p)*binomial(m+1,2) - p*binomial(m+2,2) + s where s=2, m=floor(n/s), p=n-m*s. - _Andrew Howroyd_, Aug 14 2017",
				"From _Colin Barker_, Oct 03 2017: (Start)",
				"G.f.: x*(2 - x - 2*x^2 + 2*x^3) / ((1 - x)^3*(1 + x)).",
				"a(n) = (2*n^2 + 16) / 8 for n even.",
				"a(n) = (2*n^2 + 14) / 8 for n odd.",
				"a(n) = 2*a(n-1) - 2*a(n-3) + a(n-4) for n \u003e 4. (End)",
				"E.g.f.: ((8 + x + x^2)*cosh(x) + (7 + x + x^2)*sinh(x) - 8)/4. - _Stefano Spezia_, Jul 06 2021"
			],
			"mathematica": [
				"Table[(Binomial[n+1,2] - (2-(n - 2 Floor[n/2])) Binomial[Floor[n/2]+1, 2] - (n-2 Floor[n/2]) Binomial[Floor[n/2]+2, 2] + 2), {n, 60}] (* _Vincenzo Librandi_, Oct 04 2017 *)"
			],
			"program": [
				"(PARI) a(n)=(s-\u003emy(m=n\\s,p=n%s); binomial(n+1,2)-(s-p)*binomial(m+1,2)-p*binomial(m+2,2)+s)(2); \\\\ _Andrew Howroyd_, Aug 14 2017",
				"(MAGMA) [Binomial(n+1,2)-(2-(n-2*Floor(n/2)))*Binomial(Floor(n/2)+1,2)-(n-2*Floor(n/2))*Binomial(Floor(n/2)+2,2)+2: n in [1..60]]; // _Vincenzo Librandi_, Oct 04 2017"
			],
			"xref": [
				"Cf. A290744, A290745, A290746, A014206 (bisection), A059100 (bisection)."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_, Aug 11 2017",
			"ext": [
				"a(11)-a(55) from _Andrew Howroyd_, Aug 14 2017"
			],
			"references": 7,
			"revision": 35,
			"time": "2021-07-13T19:44:14-04:00",
			"created": "2017-08-11T11:02:58-04:00"
		}
	]
}