{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A302711",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 302711,
			"data": "1,9,9,0,3,6,9,4,5,3,3,4,4,3,9,3,7,7,2,4,8,9,6,7,3,9,0,6,2,1,8,9,5,9,8,4,3,1,5,0,9,4,9,7,3,7,4,5,9,7,1,4,1,2,3,6,6,7,2,2,5,9,3,1,5,6,9,7,8,0,3,3,3,7,8,9,1,7,3,0,7,5,9,4,5,0,5,8,1,6,8,5,3,9,2,9,6,7,8,0",
			"name": "Decimal expansion of 2*sin(15*Pi/32).",
			"comment": [
				"This constant appears in a historic problem posed by Adriaan van Roomen (Adrianus Romanus) in his Ideae mathematicae from 1593, solved by Viète (see the Vieta link) using trigonometry. See the Havil reference, problem 1 (for a correction see below), pp. 69-74, and the Maor reference for Viète's approach, pp. 58-60.",
				"The problem involves the monic Chebyshev polynomial of the first kind R(45, x) (R coefficients are given in A127672). The present problem was stated as R(45, x) = sqrt(2 + sqrt(2 + sqrt(2 + sqrt(2)))) for x = (1/2)*sqrt(2 - sqrt(2 + sqrt(2 + sqrt(3)))). This is equivalent to R(45, 2*sin(Pi/96)) = 2*sin(15*Pi/32). It is a special case of the well known identity R(2*k+1, x) = x*(-1)^k*S(2*k, sqrt(4-x^2)), with the Chebyshev S polynomials (see A049310 for the coefficients). Take k = 22, x = 2*sin(Pi/96), and see the Havil reference, p. 71, for the proof of 2*sin(15*Pi/32) = sqrt(2 + sqrt(2 + sqrt(2 + sqrt(2)))). [In the Havil reference on p. 69, the second to last exponent is 43 (not 41), and in the first problem, for the argument x a furthe  +sqrt(2... is missing. In the general identity given on p. 71 a sign factor is missing. It should read, with n = 2*k+1: P_{2*k+1}(2*sin(theta)) = 2*(-1)^k*sin((2*k+1)*theta).]",
				"For the argument x = sqrt(2 - sqrt(2 + sqrt(2 + sqrt(2 + sqrt(3))))) = 2*sin(Pi/96) = 0.65438165643552284... see A302712.",
				"R(45, x) factorizes into minimal polynomials of 2*cos(Pi/k), named  C(k, x), for short, C[k], with coefficients given in A187360 as follows. R(45, x) = C[90]*C[30]*C[18]*C[10]*C[6]*C[2]. See a comment in A127672.",
				"All 45 zeros of R(45, x), which are real, are 2*cos((2*k+1)*Pi/90), for k = 0..44. See a comment in A127672.",
				"Viète used the iteration, written in terms of R polynomials as R(45, x) = -R(3, -R(3, R(5, x))) (from the semi-group property of Chebyshev T polynomials). See the Maor reference, pp. 58-60. - _Wolfdieter Lang_, May 05 2018"
			],
			"reference": [
				"Julian Havil, The Irrationals, A Story of the Numbers You Can't Count On, Princeton University Press, Princeton and Oxford, 2012, pp. 69-74.",
				"Eli Maor, Trigonometric Delights, Princeton University Press, NJ, 1998, pp. 56-62."
			],
			"link": [
				"Adriano Romano Lovaniensi, \u003ca href=\"https://babel.hathitrust.org/cgi/pt?id=ucm.5320258006;view=1up;seq=14 \"\u003eIdeae Mathematicae\u003c/a\u003e, 1593.",
				"Adriano Romano Lovaniensi,\u003ca href=\"https://archive.org/stream/bub_gb_qinevzxnHFoC#page/n15/mode/2up\"\u003eIdeae Mathematicae\u003c/a\u003e, 1593 [alternative link].",
				"Franciscus Vieta, \u003ca href=\"http://reader.digitale-sammlungen.de/de/fs1/object/display/bsb10314282_00001.html\"\u003e Ad Problema. Quod omnibus Mathematicis totius orbis construendum proposuit Adrianus Romanus, Paris, 1595\u003c/a\u003e",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e"
			],
			"formula": [
				"This constant is 2*sin(15*Pi/32) = sqrt(2 + sqrt(2 + sqrt(2 + sqrt(2)))). (for a proof see Havil. p.71)."
			],
			"example": [
				"2*sin(15*Pi/32) = 1.990369453344393772489673906218959843150949737459714123..."
			],
			"mathematica": [
				"RealDigits[2*Sin[15 Pi/32],10,120][[1]] (* _Harvey P. Dale_, Oct 22 2019 *)"
			],
			"xref": [
				"Cf. A049310, A127672, A179260, A187360, A272534, A302712, A302713, A302714, A302715, A302716."
			],
			"keyword": "nonn,cons,easy",
			"offset": "1,2",
			"author": "_Wolfdieter Lang_, Apr 28 2018",
			"references": 9,
			"revision": 29,
			"time": "2019-10-22T16:29:54-04:00",
			"created": "2018-04-29T09:18:44-04:00"
		}
	]
}