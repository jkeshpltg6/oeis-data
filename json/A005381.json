{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005381",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5381,
			"id": "M4598",
			"data": "9,10,15,16,21,22,25,26,27,28,33,34,35,36,39,40,45,46,49,50,51,52,55,56,57,58,63,64,65,66,69,70,75,76,77,78,81,82,85,86,87,88,91,92,93,94,95,96,99,100,105,106,111,112,115,116,117,118,119,120,121,122",
			"name": "Numbers k such that k and k-1 are composite.",
			"comment": [
				"Position where the composites first outnumber the primes by n, among the first natural numbers. - _Lekraj Beedassy_, Jul 11 2006"
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), p. 844.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"J. Stauduhar, \u003ca href=\"/A005381/b005381.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"R. P. Boas \u0026 N. J. A. Sloane, \u003ca href=\"/A005381/a005381.pdf\"\u003eCorrespondence, 1974\u003c/a\u003e",
				"B. W. J. Irwin, \u003ca href=\"https://www.authorea.com/users/5445/articles/111406/_show_article\"\u003eRecursive Modular Conjecture for pi(n)\u003c/a\u003e."
			],
			"formula": [
				"Conjecture: pi(n)=Sum_{k=1..n} k mod a(m) mod a(m-1) ... mod a(1) mod 2, for all values 1\u003cn\u003c=a(m), where the mod are evaluated from left to right. Verified for first 10000 a(n). - _Benedict W. J. Irwin_, May 04 2016",
				"As a check, take n=9, m=2, a(m)=10. Then we must take the numbers 1 through 9 and reduce them mod 10 then mod 9 then mod 2. The results are 1,0,1,0,1,0,1,0,0, whose sum is 4 = pi(9), as predicted. - _N. J. A. Sloane_, May 05 2016",
				"For an attempt at a proof for the conjecture above, see the link. If it is true, then for n\u003e2, isprime(n)=(n mod x) mod 2, where x is the largest a(n)\u003c=n. - _Benedict W. J. Irwin_, May 06 2016"
			],
			"maple": [
				"isA005381 := proc(n)",
				"    not isprime(n) and not isprime(n-1) ;",
				"end proc:",
				"A005381 := proc(n)",
				"    local a;",
				"    option remember;",
				"    if n = 1 then",
				"        9;",
				"    else",
				"        for a from procname(n-1)+1 do",
				"            if isA005381(a) then",
				"                return a;",
				"            end if;",
				"        end do:",
				"    end if;",
				"end proc: # _R. J. Mathar_, Jul 14 2015",
				"# second Maple program:",
				"q:= n-\u003e ormap(isprime, [n, n-1]):",
				"remove(q, [$2..130])[];  # _Alois P. Heinz_, Dec 26 2021"
			],
			"mathematica": [
				"Select[Range[2, 200], ! PrimeQ[# - 1] \u0026\u0026 ! PrimeQ[#] \u0026]"
			],
			"program": [
				"(PARI) is(n)=!isprime(n)\u0026\u0026!isprime(n-1) \\\\ _M. F. Hasler_, Jan 07 2019",
				"(Python)",
				"def ok(n): return n \u003e 3 and not isprime(n) and not isprime(n-1)",
				"print([k for k in range(122) if ok(k)]) # _Michael S. Branicky_, Dec 26 2021"
			],
			"xref": [
				"Equals A068780 + 1. Cf. A007921.",
				"Cf. A093515 (complement, apart from 1 which is in neither sequence), A323162 (characteristic function)."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_",
			"references": 28,
			"revision": 44,
			"time": "2021-12-26T21:48:53-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}