{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002176",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2176,
			"id": "M1569 N0612",
			"data": "2,6,8,90,288,840,17280,28350,89600,598752,87091200,63063000,402361344000,5003856000,2066448384,976924698750,3766102179840000,15209113920000,5377993912811520000,1646485441080480,89903156428800000",
			"name": "a(n) = LCM of denominators of Cotesian numbers {C(n,k), 0 \u003c= k \u003c= n}.",
			"comment": [
				"See A100640 for definition of C(n,k)."
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, Tenth Printing, 1972, p. 886.",
				"Louis Brand, Differential and Difference Equations, 1966, p. 612.",
				"W. W. Johnson, On Cotesian numbers: their history, computation and values to n=20, Quart. J. Pure Appl. Math., 46 (1914), 52-65.",
				"Charles Jordan, Calculus of Finite Differences, Chelsea 1965, p. 513.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A002176/b002176.txt\"\u003eTable of n, a(n) for n = 1..100\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards Applied Math. Series 55, Tenth Printing, 1972, p. 886.",
				"W. M. Johnson, \u003ca href=\"/A002176/a002176.pdf\"\u003eOn Cotesian numbers: their history, computation and values to n=20\u003c/a\u003e, Quart. J. Pure Appl. Math., 46 (1914), 52-65. [Annotated scanned copy]"
			],
			"maple": [
				"Define C(n,k) as in A100640, then: A002176:=proc(n) local t1,k; t1:=1; for k from 0 to n do t1:=lcm(t1,denom(C(n,k))); od: t1; end;"
			],
			"mathematica": [
				"cn[n_, 0] := Sum[ n^j*StirlingS1[n, j]/(j+1), {j, 1, n+1}]/n!; cn[n_, n_] := cn[n, 0]; cn[n_, k_] := 1/n!*Binomial[n, k]*Sum[n^(j+m)*StirlingS1[k, j]* StirlingS1[n-k, m]/((m+1)*Binomial[j+m+1, m+1]), {m, 1, n}, {j, 1, k+1}]; a[n_] := LCM @@ Table[ Denominator[cn[n, k]], {k, 0, n}]; Table[a[n], {n, 1, 21}] (* _Jean-François Alcover_, Oct 25 2011 *)"
			],
			"program": [
				"(PARI) cn(n)= mattranspose(matinverseimage( matrix(n+1,n+1,k,m,(m-1)^(k-1)),matrix(n+1,1,k,m,n^(k-1)/k)))[ 1, ] \\\\ vector of quadrature formula coefficients via matrix solution",
				"(PARI) A002176(n)= denominator(cn(n))"
			],
			"xref": [
				"Cf. A002177-A002179, A100620, A100621, A100640, A100641, A100642."
			],
			"keyword": "nonn,nice,easy",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_.",
			"ext": [
				"More terms and references from Michael Somos"
			],
			"references": 18,
			"revision": 27,
			"time": "2018-05-08T15:11:53-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}