{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A105219",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 105219,
			"data": "0,1,8,63,544,5225,55656,653023,8379008,116780049,1757211400,28394129951,490371506208,9013522796473,175679564492264,3618800515187775,78547755741723136,1791704327280481313",
			"name": "a(n) = Sum_{k=0..n} C(n,k)^2*(n-k)!*k^2.",
			"comment": [
				"If the e.g.f. of n^2 is E(x) and a(n) = Sum_{k=0..n} C(n,k)^2*(n-k)!*k^2, then the e.g.f. of a(n) is E(x/(1-x))/(1-x). (Thanks to _Vladeta Jovovic_ for help.)",
				"a(n) is the total number of edges in all matchings of the labeled complete bipartite graph K_n,n. Cf. A144084 for other interpretations. - _Geoffrey Critzer_, Nov 17 2021"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A105219/b105219.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"John Riordan, \u003ca href=\"/A002720/a002720_3.pdf\"\u003eLetter to N. J. A. Sloane, Sep 26 1980 with notes on the 1973 Handbook of Integer Sequences\u003c/a\u003e. Note that the sequences are identified by their N-numbers, not their A-numbers."
			],
			"formula": [
				"E.g.f.: (x/(1-x)^2+x^2/(1-x)^3)*exp(x/(1-x)).",
				"a(n) = n^2*A002720 [Riordan]. - _N. J. A. Sloane_, Jan 10 2018",
				"a(n) = (n+1)!*(2*L(n,-1)-L(n+1,-1)) where L(n,x) is the n-th Laguerre polynomial. - _Peter Luschny_, Jan 19 2012",
				"Recurrence: a(n) = 2*(n+2)*a(n-1) - (n^2+4*n-4)*a(n-2) + 2*(n-2)*(n-1)*a(n-3). - _Vaclav Kotesovec_, Oct 17 2012",
				"a(n) ~ exp(2*sqrt(n)-n-1/2)*n^(n+5/4)/sqrt(2)*(1-17/(48*sqrt(n))). - _Vaclav Kotesovec_, Oct 17 2012",
				"a(n) = n!*L(n-1,2,-1) for n\u003e=1 where L(n,b,x) is the n-th generalized Laguerre polynomial. - _Peter Luschny_, Apr 11 2015",
				"a(n) = Sum_{k=0...n} A144084(n,k)*k. - _Geoffrey Critzer_, Nov 17 2021"
			],
			"example": [
				"b(n) = 0,1,4,9,16,25,36,49,64,...",
				"a(3) = C(3,0)^2*3!*b(0) + C(3,1)^2*2!*b(1) + C(3,2)^2*1!*b(2) + C(3,3)^2*0!*b(3) = 1*6*0 + 9*2*1 + 9*1*4 + 1*1*9 = 0 + 18 + 36 + 9 = 63."
			],
			"maple": [
				"for n from 0 to 30 do b[n]:=n^2 od: seq(add(binomial(n,k)^2*(n-k)!*b[k], k=0..n), n=0..30);",
				"seq(`if`(n=0,0,simplify(n!*LaguerreL(n-1,2,-1))),n=0..17); # _Peter Luschny_, Apr 11 2015"
			],
			"mathematica": [
				"CoefficientList[Series[(x/(1-x)^2+x^2/(1-x)^3)*E^(x/(1-x)), {x, 0, 20}], x]* Table[n!, {n, 0, 20}] (* _Vaclav Kotesovec_, Oct 17 2012 *)"
			],
			"xref": [
				"Cf. A000290, A002720, A144084, A202410."
			],
			"keyword": "easy,nonn",
			"offset": "0,3",
			"author": "_Miklos Kristof_, Apr 13 2005",
			"references": 4,
			"revision": 39,
			"time": "2021-12-14T20:32:46-05:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}