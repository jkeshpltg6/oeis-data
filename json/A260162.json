{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A260162",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 260162,
			"data": "1,-2,0,1,0,0,1,0,0,0,-2,0,1,-2,0,2,0,0,1,0,0,1,-2,0,2,-4,0,3,-2,0,2,0,0,1,-4,0,4,-6,0,5,-2,0,3,0,0,3,-6,0,6,-10,0,8,-4,0,5,-2,0,4,-10,0,9,-14,0,12,-6,0,8,-2,0,7,-14,0,14,-22,0,18,-10",
			"name": "Expansion of phi(-x) / psi(-x^3) in powers of x where psi(), phi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A260162/b260162.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(3/8) * eta(q)^2 * eta(q^6) / (eta(q^2) * eta(q^3) * eta(q^12)) in powers of q.",
				"Euler transform of period 12 sequence [ -2, -1, -1, -1, -2, -1, -2, -1, -1, -1, -2, 0, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (768 t)) = 12^(1/2) g(t) where q = exp(2 Pi i t) and g() is the g.f. for A132217.",
				"G.f.: 1 / (Product_{k\u003e0} (1 + x^k) * (1 + x^k + x^(2*k)) * (1 + x^(6*k))).",
				"2 * a(n) = A253243(2*n + 3). a(3*n + 2) = 0.",
				"Convolution inverse of A132218."
			],
			"example": [
				"G.f. = 1 - 2*x + x^3 + x^6 - 2*x^10 + x^12 - 2*x^13 + 2*x^15 + x^18 + x^21 + ...",
				"G.f. = 1/q^3 - 2*q^5 + q^21 + q^45 - 2*q^77 + q^93 - 2*q^101 + 2*q^117 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ 2^(1/2) x^(3/8) EllipticTheta[ 4, 0, x] / EllipticTheta[ 2, Pi/4, x^(3/2)], {x, 0, n}];",
				"a[n_]:= SeriesCoefficient[EllipticTheta[3,0,-q]*QPochhammer[-q^3, q^6]/ QPochhammer[q^6,q^6], {q, 0, n}]; Table[a[n], {n,0,50}] (* _G. C. Greubel_, Mar 19 2018 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A)^2 * eta(x^6 + A) / (eta(x^2 + A) * eta(x^3 + A) * eta(x^12 + A)), n))};",
				"(PARI) q='q+O('q^99); Vec(eta(q)^2*eta(q^6)/(eta(q^2)*eta(q^3)*eta(q^12))) \\\\ _Altug Alkan_, Mar 20 2018"
			],
			"xref": [
				"Cf. A132217, A132218, A253243."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Nov 09 2015",
			"references": 1,
			"revision": 15,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2015-11-09T17:36:35-05:00"
		}
	]
}