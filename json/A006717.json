{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006717",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6717,
			"id": "M3005",
			"data": "1,3,15,133,2025,37851,1030367,36362925,1606008513,87656896891,5778121715415,452794797220965,41609568918940625",
			"name": "Number of ways of arranging 2n+1 nonattacking semi-queens on a (2n+1) X (2n+1) toroidal board.",
			"comment": [
				"Also the number of \"good\" permutations on 2n+1 elements [Novakovich]. - _N. J. A. Sloane_, Feb 22 2011",
				"Also the number of transversals of a cyclic Latin square of order 2n+1 and the number of orthomorphisms of the cyclic group of order 2n+1. - _Ian Wanless_, Oct 07 2001",
				"Also the number of complete mappings of a cyclic group of order 2n+1; also (2n+1) times the number of \"standard\" complete mappings of cyclic group of order 2n+1. - Jieh Hsiang, D. Frank Hsu and Yuh Pyng Shieh (arping(AT)turing.csie.ntu.edu.tw), May 08 2002",
				"See A003111 for further information.",
				"A very simple model using only addition mod n: Let i=index vector (0,1,..n-1) on any set of n distinct values, and j=index vector for the values after reordering. Then j=(i + d) mod n, where d is the vector of distances moved, and a(n) = number of reorderings that give an equidistributed set d (i.e., 1 instance of each distance moved). Since a(n)=0 for all even n, taking only odd n gives the sequence above - _Ross Drewe_, Sep 03 2017"
			],
			"reference": [
				"Yuh Pyng Shieh, Jieh Hsiang and D. Frank Hsu, On the enumeration of Abelian k-complete mappings, vol. 144 of Congressus Numerantium, 2000, pp. 67-88.",
				"Yuh Pyng Shieh, Partition Strategies for #P-complete problem with applications to enumerative combinatorics, PhD thesis, National Taiwan University, 2001.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"I. Vardi, Computational Recreations in Mathematica. Addison-Wesley, Redwood City, CA, 1991, p. 118."
			],
			"link": [
				"Christian Carley, \u003ca href=\"https://scholarworks.boisestate.edu/math_undergraduate_theses/12\"\u003eThe Name Tag Problem\u003c/a\u003e, Mathematics Undergraduate Theses (Boise State University, 2019).",
				"N. J. Cavenagh and I. M. Wanless, \u003ca href=\"http://dx.doi.org/10.1016/j.dam.2009.09.006\"\u003eOn the number of transversals in Cayley tables of cyclic groups\u003c/a\u003e, Disc. Appl. Math. 158 (2010), 136-146.",
				"S. Eberhard, F. Manners, and R. Mrazovic, \u003ca href=\"https://arxiv.org/abs/1510.05987\"\u003eAdditive Triples of Bijections, or the Toroidal Semiqueens Problem \u003c/a\u003e, arxiv:1510.05987, [math.CO], 2016.",
				"V. Kotesovec, \u003ca href=\"https://oeis.org/wiki/User:Vaclav_Kotesovec\"\u003eNon-attacking chess pieces\u003c/a\u003e, 6ed, 2013.",
				"N. Yu. Kuznetsov, \u003ca href=\"https://doi.org/10.1007/s10559-016-9799-0\"\u003eUsing the Monte Carlo Method for Fast Simulation of the Number of \"Good\" Permutations on the SCIT-4 Multiprocessor Computer Complex\u003c/a\u003e, Cybernetics and Systems Analysis, January 2016, Volume 52, Issue 1, pp 52-57.",
				"B. D. McKay, J. C. McLeod and I. M. Wanless, \u003ca href=\"http://dx.doi.org/10.1007/s10623-006-0012-8\"\u003eThe number of transversals in a Latin square\u003c/a\u003e, Des. Codes Cryptogr., 40, (2006) 269-284.",
				"D. Novakovic, \u003ca href=\"https://doi.org/10.1007/BF02678671\"\u003eComputation of the number of complete mappings for permutations\u003c/a\u003e, Cybernetics \u0026 System Analysis, No. 2, v. 36 (2000), pp. 244-247.",
				"Kevin Pratt, \u003ca href=\"https://arxiv.org/abs/1609.09585\"\u003eClosed-Form Expressions for the n-Queens Problem and Related Problems\u003c/a\u003e, arXiv:1609.09585 [cs.DM], 2016.",
				"D. S. Stones and I. M. Wanless, \u003ca href=\"http://dx.doi.org/10.1016/j.ffa.2010.04.001\"\u003eCompound orthomorphisms of the cyclic group\u003c/a\u003e, Finite Fields Appl. 16 (2010), 277-289.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/QueensProblem.html\"\u003eQueens Problem.\u003c/a\u003e"
			],
			"formula": [
				"Suppose n is odd and let b(n)=a((n-1)/2). Then b(n) is odd; if n\u003e3 and n is not 1 mod 3 then b(n) is divisible by 3n; b(n)=-2n mod n^2 in n is prime; b(n) is divisible by n^2 if n is composite; b(n) is asymptotically in between 3.2^n and 0.62^n n!. [Cavenagh, Wanless], [McKay, McLeod, Wanless], [Stones, Wanless] - _Ian Wanless_, Jul 30 2010",
				"b(n) is asymptotic to n!/e [Eberhard, Manners, Mrazovic]. - _Sam Spiro_, Apr 16 2019",
				"a(n) = (2*n+1) * A003111(n). - _Andrew Howroyd_, Sep 28 2020"
			],
			"program": [
				"(MATLAB) k = 6; A = zeros(1,k); for i = 1:k; n = 2*i-1; x = [0: n-1]; allP = perms(x); T = size(allP,1); X = repmat(x, T, 1); Y = mod(X + allP, n); Y = sort(Y, 2); L = ~(sum(Y ~= X, 2)); A(i) = sum(L); end; A",
				"% 1st 6 terms by testing all n! possible distance vectors",
				"% _Ross Drewe_, Sep 03 2017"
			],
			"xref": [
				"Cf. A003111, A007705."
			],
			"keyword": "nonn,more,nice",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from Jieh Hsiang, D. Frank Hsu and Yuh Pyng Shieh (arping(AT)turing.csie.ntu.edu.tw), May 08 2002",
				"a(12) added from A003111 by _N. J. A. Sloane_, Mar 29 2007",
				"Definition clarified by _Vaclav Kotesovec_, Sep 16 2014"
			],
			"references": 10,
			"revision": 80,
			"time": "2020-09-28T21:43:21-04:00",
			"created": "1991-07-25T03:00:00-04:00"
		}
	]
}