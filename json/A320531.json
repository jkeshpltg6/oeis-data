{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A320531",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 320531,
			"data": "0,1,0,0,1,0,0,2,1,0,0,3,4,1,0,0,4,12,6,1,0,0,5,32,27,8,1,0,0,6,80,108,48,10,1,0,0,7,192,405,256,75,12,1,0,0,8,448,1458,1280,500,108,14,1,0,0,9,1024,5103,6144,3125,864,147,16,1,0,0,10,2304",
			"name": "T(n,k) = n*k^(n - 1), k \u003e 0, with T(n,0) = A063524(n), square array read by antidiagonals upwards.",
			"comment": [
				"T(n,k) is the number of length n*k binary words of n consecutive blocks of length k, respectively, one of the blocks having exactly k letters 1, and the other having exactly one letter 0. First column follows from the next definition.",
				"In Kauffman's language, T(n,k) is the total number of Jordan trails that are obtained by placing state markers at the crossings of the Pretzel universe P(k, k, ..., k) having n tangles, of k half-twists respectively. In other words, T(n,k) is the number of ways of splitting the crossings of the Pretzel knot shadow P(k, k, ..., k) such that the final diagram is a single Jordan curve. The aforementionned binary words encode these operations by assigning each tangle a length k binary words with the adequate choice for splitting the crossings.",
				"Columns are linear recurrence sequences with signature (2*k, -k^2)."
			],
			"reference": [
				"Louis H. Kauffman, Formal Knot Theory, Princeton University Press, 1983."
			],
			"link": [
				"Louis H. Kauffman, \u003ca href=\"https://doi.org/10.1016/0040-9383(87)90009-7\"\u003eState models and the Jones polynomial\u003c/a\u003e, Topology, Vol. 26 (1987), 395-407.",
				"Franck Ramaharo, \u003ca href=\"https://arxiv.org/abs/1805.10680\"\u003eA generating polynomial for the pretzel knot\u003c/a\u003e, arXiv:1805.10680 [math.CO], 2018.",
				"Alexander Stoimenow, \u003ca href=\"https://doi.org/10.3390/sym7020365\"\u003eEverywhere Equivalent 2-Component Links\u003c/a\u003e, Symmetry Vol. 7 (2015), 365-375.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Pretzel_link\"\u003ePretzel link\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = (2*k)*T(n-1,k) - (k^2)*T(n-2,k).",
				"G.f. for columns: x/(1 - k*x)^2.",
				"E.g.f. for columns: x*exp(k*x).",
				"T(n,1) = A001477(n).",
				"T(n,2) = A001787(n).",
				"T(n,3) = A027471(n+1).",
				"T(n,4) = A002697(n).",
				"T(n,5) = A053464(n).",
				"T(n,6) = A053469(n), n \u003e 0.",
				"T(n,7) = A027473(n), n \u003e 0.",
				"T(n,8) = A053539(n).",
				"T(n,9) = A053540(n), n \u003e 0.",
				"T(n,10) = A053541(n), n \u003e 0.",
				"T(n,11) = A081127(n).",
				"T(n,12) = A081128(n)."
			],
			"example": [
				"Square array begins:",
				"    0, 0,   0,    0,     0,      0,      0,      0, ...",
				"    1, 1,   1,    1,     1,      1,      1,      1, ...",
				"    0, 2,   4,    6,     8,     10,     12,     14, ... A005843",
				"    0, 3,  12,   27,    48,     75,    108,    147, ... A033428",
				"    0, 4,  32,  108,   256,    500,    864,   1372, ... A033430",
				"    0, 5,  80,  405,  1280,   3125,   6480,  12005, ... A269792",
				"    0, 6, 192, 1458,  6144,  18750,  46656, 100842, ...",
				"    0, 7, 448, 5103, 28672, 109375, 326592, 823543, ...",
				"    ...",
				"T(3,2) = 3*2^(3 - 1) = 12. The corresponding binary words are 110101, 110110, 111001, 111010, 011101, 011110, 101101, 101110, 010111, 011011, 100111, 101011."
			],
			"mathematica": [
				"T[n_, k_] = If [k \u003e 0, n*k^(n - 1), If[k == 0 \u0026\u0026 n == 1, 1, 0]];",
				"Table[Table[T[n - k, k], {k, 0, n}], {n, 0, 12}]//Flatten"
			],
			"program": [
				"(Maxima)",
				"T(n, k) := if k \u003e 0 then n*k^(n - 1) else if k = 0 and n = 1 then 1 else 0$",
				"tabl(nn) := for n:0 thru nn do print(makelist(T(n, k), k, 0, nn))$"
			],
			"xref": [
				"Antidiagonal sums: A101495.",
				"Column 1 is column 2 of A300453.",
				"Column 2 is column 1 of A300184.",
				"Cf. A104002, A320530."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,8",
			"author": "_Franck Maminirina Ramaharo_, Oct 14 2018",
			"references": 0,
			"revision": 4,
			"time": "2018-10-18T03:05:21-04:00",
			"created": "2018-10-18T03:05:21-04:00"
		}
	]
}