{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A349718",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 349718,
			"data": "1,1,28,12600,69699849,4070693024640,2484046163254367574,15778915364062895746351104,1040828457711477326843036225608036,711789875509887224494712166194197254144000,5040627715175514814159607456023227379139001458908168",
			"name": "Number of spanning trees in the n X n grid graph where rotations and reflections are not counted as distinct.",
			"comment": [
				"The number of perfect mazes on an n X n grid of cells where rotations and reflections are not counted as distinct.",
				"The sequence A007341 enumerates the same spanning trees or mazes but with duplicates due to symmetries of the square counted.",
				"A lower bound for a(n) is the elements of A007341 divided by 8.",
				"Terms can be computed using Burnside's lemma and Kirchhoff's matrix tree theorem applied to various graphs. See the PARI program link for technical details. - _Andrew Howroyd_, Nov 27 2021"
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A349718/b349718.txt\"\u003eTable of n, a(n) for n = 1..40\u003c/a\u003e",
				"Andrew Howroyd, \u003ca href=\"/A349718/a349718.txt\"\u003ePARI program using Kirchoff's Matrix Tree Theorem\u003c/a\u003e, 2021.",
				"Paul Kim, \u003ca href=\"http://rave.ohiolink.edu/etdc/view?acc_num=osu1563286393237089\"\u003eIntelligent Maze Generation\u003c/a\u003e, Doctoral dissertation, Ohio State University, 2019.",
				"Mike Koss, \u003ca href=\"https://github.com/mckoss/maze-canvas\"\u003eMaze Canvas\u003c/a\u003e, Open source unique maze generator, 2021.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GridGraph.html\"\u003eGrid Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/SpanningTree.html\"\u003eSpanning Tree\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Burnside%27s_lemma\"\u003eBurnside's_lemma\u003c/a\u003e, \u003ca href=\"https://en.wikipedia.org/wiki/Kirchhoff%27s_theorem\"\u003eKirchhoff's_theorem\u003c/a\u003e"
			],
			"formula": [
				"a(n) ~ A007341(n) / 8; a(n) \u003e= A007341(n) / 8.",
				"a(2*n) = (A116469(2*n,2*n) + 4*n*A116469(2*n,n))/8. - _Andrew Howroyd_, Nov 27 2021"
			],
			"example": [
				"While there are 192 mazes on a 3 X 3 grid, only a(3) = 28 are distinct mod rotations and reflections.",
				"21 are asymmetric:",
				"    _____     _____     _____     _____     _____     _____     _____     _____",
				"   |     |   |     |   |     |   |    _|   |    _|   |    _|   |    _|   |    _|",
				"   | | |_|   | |_| |   | |_|_|   | |   |   | |  _|   | |_  |   | |_  |   | |_ _|",
				"   |_|_ _|   |_ _|_|   |_ _ _|   |_|_|_|   |_|_ _|   |_ _|_|   |_|_ _|   |_ _ _|",
				"    _____     _____     _____     _____     _____     _____     _____     _____",
				"   |    _|   |    _|   |    _|   |    _|   |    _|   |  _  |   |  _  |   |  _  |",
				"   |_|   |   |_|  _|   |_|_  |   | | | |   | |_| |   |_  | |   |_  |_|   |_ _| |",
				"   |_ _|_|   |_ _ _|   |_ _ _|   |_|_ _|   |_ _ _|   |_ _|_|   |_ _ _|   |_ _ _|",
				"    _____     _____     _____     _____     _____",
				"   |  _ _|   |  _ _|   |_   _|   |_   _|   |_   _|",
				"   |_    |   |_   _|   |    _|   |  _  |   |   | |",
				"   |_ _|_|   |_ _ _|   |_|_ _|   |_ _|_|   |_|_ _|",
				".",
				"5 have 2-way symmetry:",
				"    _____     _____     _____     _____     _____",
				"   |     |   |     |   |    _|   |  _ _|   |_   _|",
				"   | | | |   |_| |_|   |_| | |   |_ _  |   |     |",
				"   |_|_|_|   |_ _ _|   |_ _ _|   |_ _ _|   |_|_|_|",
				".",
				"2 have 4-way symmetry:",
				"    _____     _____",
				"   |_   _|   |_  | |",
				"   |_   _|   |    _|",
				"   |_ _ _|   |_|_ _|"
			],
			"program": [
				"(PARI) \\\\ See link. - _Andrew Howroyd_, Nov 27 2021"
			],
			"xref": [
				"Cf. A007341, A116469."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Mike Koss_, Nov 26 2021",
			"ext": [
				"Terms a(7) and beyond from _Andrew Howroyd_, Nov 27 2021"
			],
			"references": 1,
			"revision": 30,
			"time": "2021-11-28T02:38:51-05:00",
			"created": "2021-11-27T11:35:42-05:00"
		}
	]
}