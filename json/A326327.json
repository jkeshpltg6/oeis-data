{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A326327",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 326327,
			"data": "1,0,1,0,-1,1,0,5,-2,1,0,-61,16,-3,1,0,1385,-272,33,-4,1,0,-50521,7936,-723,56,-5,1,0,2702765,-353792,25953,-1504,85,-6,1,0,-199360981,22368256,-1376643,64256,-2705,120,-7,1,0,19391512145,-1903757312,101031873,-3963904,134185,-4416,161,-8,1",
			"name": "A(n, k) = (m*k)! [x^k] MittagLefflerE(m, x)^(-n), for m = 2, n \u003e= 0, k \u003e= 0; square array read by descending antidiagonals.",
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Mittag-LefflerFunction.html\"\u003eMittag-Leffler Function\u003c/a\u003e"
			],
			"example": [
				"Array starts:",
				"[0] 1,  0,   0,     0,      0,         0,          0,             0, ... A000007",
				"[1] 1, -1,   5,   -61,   1385,    -50521,    2702765,    -199360981, ... A028296",
				"[2] 1, -2,  16,  -272,   7936,   -353792,   22368256,   -1903757312, ... A000182",
				"[3] 1, -3,  33,  -723,  25953,  -1376643,  101031873,   -9795436563, ... A326328",
				"[4] 1, -4,  56, -1504,  64256,  -3963904,  332205056,  -36246728704, ...",
				"[5] 1, -5,  85, -2705, 134185,  -9451805,  892060285, -108357876905, ...",
				"[6] 1, -6, 120, -4416, 249600, -19781376, 2078100480, -278400270336, ...",
				"       A045944,",
				"Seen as a triangle:",
				"[0] [1]",
				"[1] [0, 1]",
				"[2] [0, -1,      1]",
				"[3] [0, 5,       -2,      1]",
				"[4] [0, -61,     16,      -3,    1]",
				"[5] [0, 1385,    -272,    33,    -4,    1]",
				"[6] [0, -50521,  7936,    -723,  56,    -5, 1]",
				"[7] [0, 2702765, -353792, 25953, -1504, 85, -6, 1]"
			],
			"mathematica": [
				"cl[m_, p_, len_] := CoefficientList[",
				"   Series[FunctionExpand[MittagLefflerE[m, z]^p], {z, 0, len}], z];",
				"MLPower[m_, 0,  len_] := Table[KroneckerDelta[0, n], {n, 0, len - 1}];",
				"MLPower[m_, n_, len_] := cl[m, n, len - 1] (m Range[0, len - 1])!;",
				"For[n = 0, n \u003c 8, n++, Print[MLPower[2, -n, 8]]]"
			],
			"program": [
				"(Sage)",
				"def MLPower(m, p, len):",
				"    if p == 0: return [p^k for k in (0..len-1)]",
				"    f = [i/m for i in (1..m-1)]",
				"    h = lambda x: hypergeometric([], f, (x/m)^m)",
				"    g = [v for v in taylor(h(x)^p, x, 0, (len-1)*m).list() if v != 0]",
				"    return [factorial(m*k)*v for (k, v) in enumerate(g)]",
				"for p in (0..6): print(MLPower(2, -p, 9))"
			],
			"xref": [
				"Rows: A000007 (row 0), A028296 (row 1), A000182 (row 2), A326328(row 3).",
				"Columns: A045944 (col. 2).",
				"Cf. A326476 (m=2, p\u003e=0), this sequence (m=2, p\u003c=0), A326474 (m=3, p\u003e=0), A326475 (m=3, p\u003c=0)."
			],
			"keyword": "sign,tabl",
			"offset": "0,8",
			"author": "_Peter Luschny_, Jul 07 2019",
			"references": 4,
			"revision": 16,
			"time": "2020-03-06T04:04:51-05:00",
			"created": "2019-07-08T05:12:48-04:00"
		}
	]
}