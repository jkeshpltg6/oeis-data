{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A178815",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 178815,
			"data": "3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2",
			"name": "First base of a nonzero Fermat quotient mod the n-th prime.",
			"comment": [
				"First number m coprime to p = p_n such that p does not divide q_p(m), where q_p(m) = (m^(p-1) - 1)/p is the Fermat quotient of p to the base m.",
				"It is known that a(n) = O((log p_n)^2) as n -\u003e oo. It is conjectured that a(n) = 3 if p_n is a Wieferich prime. See Section 1.1 in Ostafe-Shparlinski (2010).",
				"Additional comments, references, links, and cross-refs are in A001220.",
				"a(n) \u003e 3 iff prime(n) is a term of both A001220 and A014127, i.e., iff A240987(n) = 2. - _Felix Fröhlich_, Jul 09 2016"
			],
			"link": [
				"A. Ostafe and I. Shparlinski, \u003ca href=\"http://arxiv.org/abs/1001.1504\"\u003e Pseudorandomness and Dynamics of Fermat Quotients\u003c/a\u003e, arXiv:1001.1504 [math.NT], 2010.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Fermat_quotient#Generalized_Wieferich_primes\"\u003eGeneralized Wieferich primes\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 2 if n \u003e 1 and p_n is not a Wieferich prime A001220.",
				"a(n) \u003e 2 if p_n is a Wieferich prime.",
				"A178844(n) = (a(n)^(p-1) - 1)/p mod p, where p = p_n."
			],
			"example": [
				"p_1 = 2 and 2^2 divides 1^(2-1) - 1 = 0 but not 3^(2-1) - 1 = 2, so a(1) = 3.",
				"p_4 = 7 and 7^2 does not divide 2^(7-1) - 1 = 63, so a(4) = 2.",
				"p_183 = 1093 and 1093^2 divides 2^1092 - 1 but not 3^1092 - 1, so a(183) = 3.",
				"Similarly, p_490 = 3511 and a(490) = 3. See A001220."
			],
			"mathematica": [
				"Table[b = 2; While[PowerMod[b, Prime[n] - 1, #^2] == 1 || GCD[b, #] \u003e 1, b++] \u0026@ Prime@ n; b, {n, 120}] (* _Michael De Vlieger_, Jul 09 2016 *)"
			],
			"program": [
				"(PARI) a(n) = my(b=2, p=prime(n)); while(Mod(b, p^2)^(p-1)==1 || gcd(b, p) \u003e 1, b++); b \\\\ _Felix Fröhlich_, Jul 09 2016"
			],
			"xref": [
				"Cf. A001220, A178844."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Jonathan Sondow_, Jun 17 2010, Jun 24 2010, Jun 25 2010",
			"references": 3,
			"revision": 10,
			"time": "2016-07-10T15:18:49-04:00",
			"created": "2010-07-11T03:00:00-04:00"
		}
	]
}