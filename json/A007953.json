{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007953",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7953,
			"data": "0,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,10,2,3,4,5,6,7,8,9,10,11,3,4,5,6,7,8,9,10,11,12,4,5,6,7,8,9,10,11,12,13,5,6,7,8,9,10,11,12,13,14,6,7,8,9,10,11,12,13,14,15,7,8,9,10,11,12,13,14,15,16,8,9,10,11,12,13,14,15",
			"name": "Digital sum (i.e., sum of digits) of n; also called digsum(n).",
			"comment": [
				"Do not confuse with the digital root of n, A010888 (first term that differs is a(19)).",
				"Also the fixed point of the morphism 0 -\u003e {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, 1 -\u003e {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, 2 -\u003e {2, 3, 4, 5, 6, 7, 8, 9, 10, 11}, etc. - _Robert G. Wilson v_, Jul 27 2006",
				"For n \u003c 100 equal to (floor(n/10) + n mod 10) = A076314(n). - _Hieronymus Fischer_, Jun 17 2007"
			],
			"reference": [
				"Krassimir Atanassov, On the 16th Smarandache Problem, Notes on Number Theory and Discrete Mathematics, Sophia, Bulgaria, Vol. 5 (1999), No. 1, 36-38."
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A007953/b007953.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Krassimir Atanassov, \u003ca href=\"http://www.gallup.unm.edu/~smarandache/Atanassov-SomeProblems.pdf\"\u003eOn Some of Smarandache's Problems\u003c/a\u003e.",
				"Jean-Luc Baril, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v18i1p178\"\u003eClassical sequences revisited with permutations avoiding dotted pattern\u003c/a\u003e, Electronic Journal of Combinatorics, Vol. 18 (2011), #P178.",
				"Ernesto Estrada and Puri Pereira-Ramos, \u003ca href=\"https://doi.org/10.1155/2018/9893867\"\u003eSpatial 'Artistic' Networks: From Deconstructing Integer-Functions to Visual Arts\u003c/a\u003e, Complexity, Vol. 2018 (2018), Article ID 9893867.",
				"A. O. Gel'fond, \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/aa/aa13/aa13115.pdf\"\u003eSur les nombres qui ont des propriétés additives et multiplicatives données\u003c/a\u003e (French) Acta Arith., Vol. 13 (1967/1968), pp. 259-265. MR0220693 (36 #3745)",
				"Christian Mauduit and András Sárközy, \u003ca href=\"http://dx.doi.org/10.1006/jnth.1998.2229\"\u003eOn the arithmetic structure of sets characterized by sum of digits properties\u003c/a\u003e J. Number Theory, Vol. 61 , No. 1 (1996), pp. 25-38. MR1418316 (97g:11107)",
				"Christian Mauduit and András Sárközy, \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/aa/aa81/aa8122.pdf\"\u003e On the arithmetic structure of the integers whose sum of digits is fixed\u003c/a\u003e, Acta Arith., Vol. 81, No. 2 (1997), pp. 145-173. MR1456239 (99a:11096)",
				"Kerry Mitchell, \u003ca href=\"http://kerrymitchellart.com/articles/Spirolateral-Type_Images_from_Integer_Sequences.pdf\"\u003eSpirolateral-Type Images from Integer Sequences\u003c/a\u003e, 2013.",
				"Kerry Mitchell, \u003ca href=\"/A007953/a007953.jpg\"\u003eSpirolateral image for this sequence\u003c/a\u003e . [taken, with permission, from the Spirolateral-Type Images from Integer Sequences article]",
				"Jan-Christoph Puchta and Jürgen Spilker, \u003ca href=\"http://dx.doi.org/10.1007/s00591-002-0048-4\"\u003eAltes und Neues zur Quersumme\u003c/a\u003e, Mathematische Semesterberichte, Vol. 49 (2002), pp. 209-226.",
				"Jan-Christoph Puchta and Jürgen Spilker, \u003ca href=\"http://www.math.uni-rostock.de/~schlage-puchta/papers/Quersumme.pdf\"\u003eAltes und Neues zur Quersumme\u003c/a\u003e.",
				"Maxwell Schneider and Robert Schneider, \u003ca href=\"https://arxiv.org/abs/1807.06710\"\u003eDigit sums and generating functions\u003c/a\u003e, arXiv:1807.06710 [math.NT], 2018.",
				"Jeffrey O. Shallit, \u003ca href=\"http://www.jstor.org/stable/2322179\"\u003eProblem 6450\u003c/a\u003e, Advanced Problems, The American Mathematical Monthly, Vol. 91, No. 1 (1984), pp. 59-60; \u003ca href=\"http://www.jstor.org/stable/2322523\"\u003eTwo series, solution to Problem 6450\u003c/a\u003e, ibid., Vol. 92, No. 7 (1985), pp. 513-514.",
				"Vladimir Shevelev, \u003ca href=\"http://journals.impan.pl/aa/Inf/126-3-1.html\"\u003eCompact integers and factorials\u003c/a\u003e, Acta Arith., Vol. 126, No. 3 (2007), pp. 195-236 (cf. pp. 205-206).",
				"Robert Walker, \u003ca href=\"http://robertinventor.com/ftswiki/Self_Similar_Sloth_Canon_Number_Sequences\"\u003eSelf Similar Sloth Canon Number Sequences\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DigitSum.html\"\u003eDigit Sum\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Digit_sum\"\u003eDigit sum\u003c/a\u003e.",
				"\u003ca href=\"/index/Coi#Colombian\"\u003eIndex entries for Colombian or self numbers and related sequences\u003c/a\u003e"
			],
			"formula": [
				"a(A051885(n)) = n.",
				"a(n) \u003c= 9(log_10(n)+1). - _Stefan Steinerberger_, Mar 24 2006",
				"From _Benoit Cloitre_, Dec 19 2002: (Start)",
				"a(0) = 0, a(10n+i) = a(n) + i for 0 \u003c= i \u003c= 9.",
				"a(n) = n - 9*(Sum_{k \u003e 0} floor(n/10^k)) = n - 9*A054899(n). (End)",
				"From _Hieronymus Fischer_, Jun 17 2007: (Start)",
				"G.f. g(x) = Sum_{k \u003e 0, (x^k - x^(k+10^k) - 9x^(10^k))/(1-x^(10^k))}/(1-x).",
				"a(n) = n - 9*Sum_{10 \u003c= k \u003c= n} Sum_{j|k, j \u003e= 10} floor(log_10(j)) - floor(log_10(j-1)). (End)",
				"From _Hieronymus Fischer_, Jun 25 2007: (Start)",
				"The g.f. can be expressed in terms of a Lambert series, in that g(x) = (x/(1-x) - 9*L[b(k)](x))/(1-x) where L[b(k)](x) = sum{k \u003e= 0, b(k)*x^k/(1-x^k)} is a Lambert series with b(k) = 1, if k \u003e 1 is a power of 10, else b(k) = 0.",
				"G.f.: g(x) = (Sum_{k \u003e 0} (1 - 9*c(k))*x^k)/(1-x), where c(k) = Sum_{j \u003e 1, j|k} floor(log_10(j)) - floor(log_10(j-1)).",
				"a(n) = n - 9*Sum_{0 \u003c k \u003c= floor(log_10(n))} a(floor(n/10^k))*10^(k-1). (End)",
				"From _Hieronymus Fischer_, Oct 06 2007: (Start)",
				"a(n) \u003c= 9*(1 + floor(log_10(n)), equality holds for n = 10^m - 1, m \u003e 0.",
				"lim sup (a(n) - 9*log_10(n)) = 0 for n --\u003e infinity.",
				"lim inf (a(n+1) - a(n) + 9*log_10(n)) = 1 for n --\u003e infinity. (End)",
				"a(n) = A138530(n, 10) for n \u003e 9. - _Reinhard Zumkeller_, Mar 26 2008",
				"a(A058369(n)) = A004159(A058369(n)); a(A000290(n)) = A004159(n). - _Reinhard Zumkeller_, Apr 25 2009",
				"a(n) mod 2 = A179081(n). - _Reinhard Zumkeller_, Jun 28 2010",
				"a(n) \u003c= 9*log_10(n+1). - _Vladimir Shevelev_, Jun 01 2011",
				"a(n) = a(n-1) + a(n-10) - a(n-11), for n \u003c 100. - _Alexander R. Povolotsky_, Oct 09 2011",
				"a(n) = Sum_{k \u003e= 0} A031298(n, k). - _Philippe Deléham_, Oct 21 2011",
				"a(n) = a(n mod b^k) + a(floor(n/b^k)), for all k \u003e= 0. - _Hieronymus Fischer_, Mar 24 2014",
				"Sum_{n\u003e=1} a(n)/(n*(n+1)) = 10*log(10)/9 (Shallit, 1984). - _Amiram Eldar_, Jun 03 2021"
			],
			"example": [
				"a(123) = 1 + 2 + 3 = 6, a(9875) = 9 + 8 + 7 + 5 = 29."
			],
			"maple": [
				"A007953 := proc(n) add(d,d=convert(n,base,10)) ; end proc: # _R. J. Mathar_, Mar 17 2011"
			],
			"mathematica": [
				"Table[Sum[DigitCount[n][[i]] * i, {i, 9}], {n, 50}] (* _Stefan Steinerberger_, Mar 24 2006 *)",
				"Table[Plus @@ IntegerDigits @ n, {n, 0, 87}] (* or *)",
				"Nest[Flatten[# /. a_Integer -\u003e Array[a + # \u0026, 10, 0]] \u0026, {0}, 2] (* _Robert G. Wilson v_, Jul 27 2006 *)",
				"Table[Sum[Floor[n/10^k] - 10 * Floor[n/10^(k + 1)], {k, 0, Floor[Log[10, n]]}], {n, 300}] (* _José de Jesús Camacho Medina_, Mar 31 2014 *)",
				"Total/@IntegerDigits[Range[0,90]] (* _Harvey P. Dale_, May 10 2016 *)"
			],
			"program": [
				"/* The next few PARI programs are kept for historical and pedagogical reasons.",
				"   For practical use, the suggested and most efficient code is: A007953=sumdigits */",
				"(PARI) a(n)=if(n\u003c1, 0, if(n%10, a(n-1)+1, a(n/10))) \\\\ Recursive, very inefficient. A more efficient recursive variant: a(n)=if(n\u003e9, n=divrem(n, 10); n[2]+a(n[1]), n)",
				"(PARI) a(n, b=10)={my(s=(n=divrem(n, b))[2]); while(n[1]\u003e=b, s+=(n=divrem(n[1], b))[2]); s+n[1]} \\\\ _M. F. Hasler_, Mar 22 2011",
				"(PARI) a(n)=sum(i=1, #n=digits(n), n[i]) \\\\ Twice as fast. Not so nice but faster:",
				"(PARI) a(n)=sum(i=1,#n=Vecsmall(Str(n)),n[i])-48*#n \\\\ - _M. F. Hasler_, May 10 2015",
				"/* Since PARI 2.7, one can also use: a(n)=vecsum(digits(n)), or better: A007953=sumdigits. [Edited and commented by _M. F. Hasler_, Nov 09 2018] */",
				"(PARI) a(n) = sumdigits(n); \\\\ _Altug Alkan_, Apr 19 2018",
				"(Haskell)",
				"a007953 n | n \u003c 10 = n",
				"          | otherwise = a007953 n' + r where (n',r) = divMod n 10",
				"-- _Reinhard Zumkeller_, Nov 04 2011, Mar 19 2011",
				"(MAGMA) [ \u0026+Intseq(n): n in [0..87] ];  // _Bruno Berselli_, May 26 2011",
				"(Smalltalk)",
				"\"Recursive version for general bases. Set base = 10 for this sequence.\"",
				"digitalSum: base",
				"| s |",
				"base = 1 ifTrue: [^self].",
				"(s := self // base) \u003e 0",
				"  ifTrue: [^(s digitalSum: base) + self - (s * base)]",
				"  ifFalse: [^self]",
				"\"by _Hieronymus Fischer_, Mar 24 2014\"",
				"(Python)",
				"def A007953(n):",
				"    return sum(int(d) for d in str(n)) # _Chai Wah Wu_, Sep 03 2014",
				"(Python)",
				"def a(n): return sum(map(int, str(n))) # _Michael S. Branicky_, May 22 2021",
				"(Scala) (0 to 99).map(_.toString.map(_.toInt - 48).sum) // _Alonso del Arte_, Sep 15 2019",
				"(Swift 5)",
				"A007953(n): String(n).compactMap{$0.wholeNumberValue}.reduce(0, +) // _Egor Khmara_, Jun 15 2021"
			],
			"xref": [
				"Cf. A003132, A055012, A055013, A055014, A055015, A010888, A007954, A031347, A055017, A076313, A076314, A054899, A138470, A138471, A138472, A000120, A004426, A004427, A054683, A054684, A069877, A179082-A179085, A108971, A179987, A179988, A180018, A180019, A217928, A216407, A037123, A074784, A231688, A231689, A225693, A254524 (ordinal transform).",
				"For n + digsum(n) see A062028."
			],
			"keyword": "nonn,base,nice,easy,look",
			"offset": "0,3",
			"author": "R. Muller",
			"ext": [
				"More terms from _Hieronymus Fischer_, Jun 17 2007",
				"Edited by _Michel Marcus_, Nov 11 2013"
			],
			"references": 984,
			"revision": 251,
			"time": "2021-06-03T03:28:59-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}