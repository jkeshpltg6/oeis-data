{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A339885",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 339885,
			"data": "1,1,1,0,1,1,0,1,1,1,-1,0,1,1,1,0,-1,1,1,1,1,-1,-1,-1,1,1,1,1,0,-1,-1,0,1,1,1,1,0,-1,-2,-1,0,1,1,1,1,0,1,-1,-2,0,0,1,1,1,1,0,0,0,-2,-2,0,0,1,1,1,1",
			"name": "Triangle read by rows: T(n, m) gives the sum of the weights of weighted partitions of n with m parts from generalized pentagonal numbers {A001318(k)}_{k\u003e=1}.",
			"comment": [
				"The row sums are given in A341417.",
				"One could add a row n=0 and the column (1,repeat(0)) including the empty partition with no parts, and number of parts m = 0. The weight w(0) = -1.",
				"The weight from {-1, 0, +1} of a positive number n is w(n) = 0 if n is not an element of the generalized pentagonal numbers {Pent(k) = A001318(k)}_{k\u003e=1}, and if n = Pent(k) then w(n) = (-1)^(ceiling(Pent(k)/2)+1). The sequence",
				"{(n, w(n))}_{n\u003e=1} begins: {(1,+1), (2,+1), (3,0), (4,0), (5,-1), (6,0), (7,-1), ...}. One can also use w(0) = -1. w(n) = -A010815(n), for n \u003e= 0. For n \u003e= 1 w(n) = A257028(n) also.",
				"The weight of a partition is the product of the weights of its parts.",
				"For the triangle giving the sum of the weights of weighted compositions of n with m parts from the generalized pentagonal numbers see A341418."
			],
			"formula": [
				"T(n, m) = Sum_{j=1..p(n,m)} w(Part(n, m, j)), where p(n, m) = A008284(n, m), and the ternary weight of the j-th partition of n with m parts Part(n,m,j), in Abramowitz-Stegun order, is defined as the product of the weights of the parts, by w(n) = -A010815(n), for n \u003e= 1 and m = 1, 2, ..., n."
			],
			"example": [
				"The triangle T(n, m) begins:",
				"n\\m   1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 ... A341417",
				"----------------------------------------------------------------------------",
				"1:    1                                                                 1",
				"2:    1  1                                                              2",
				"3:    0  1  1                                                           2",
				"4:    0  1  1  1                                                        3",
				"5;   -1  0  1  1  1                                                     2",
				"6:    0 -1  1  1  1  1                                                  3",
				"7:   -1 -1 -1  1  1  1  1                                               1",
				"8:    0 -1 -1  0  1  1  1  1                                            2",
				"9:    0 -1 -2 -1  0  1  1  1  1                                         0",
				"10:   0  1 -1 -2  0  0  1  1  1  1                                      2",
				"11:   0  0  0 -2 -2  0  0  1  1  1  1                                   0",
				"12:   1  1  1  0 -2 -1  0  0  1  1  1  1                                4",
				"13:   0  1  1  0 -1 -2 -1  0  0  1  1  1  1                             2",
				"14:   0  2  2  2  0 -1 -1 -1  0  0  1  1  1  1                          7",
				"15:   1  0  1  2  1 -1 -1 -1 -1  0  0  1  1  1  1                       5",
				"16:   0  1  2  2  3  1 -1  0 -1 -1  0  0  1  1  1  1                   10",
				"17:   0  0  0  1  2  2  0 -1  0 -1 -1  0  0  1  1  1  1                 6",
				"18:   0  0  0  2  2  3  2  0  0  0 -1 -1  0  0  1  1  1  1             11",
				"19:   0 -1 -1 -1  1  2  2  1  0  0  0 -1 -1  0  0  1  1  1  1           5",
				"20:   0 -1 -1  0  1  2  3  2  1  1  0  0 -1 -1  0  0  1  1  1  1       10",
				"...",
				"n = 5: (Partition; weight w) with | separating same m numbers (in Abramowitz -Stegun order):",
				"(5;-1) | (1,4;0), (2,3;0) | (1^2,3;0), (1,2^2;1) | (1^3,2;1) | (1^5;1), hence row n=5 is [-1, 0, 1, 1, 1] from the sum of same m weighs."
			],
			"xref": [
				"Cf. A000045, A001318, A008284, -A010815, A257028, A341417, A341418."
			],
			"keyword": "sign,tabl",
			"offset": "1,39",
			"author": "_Wolfdieter Lang_, Feb 15 2021",
			"references": 2,
			"revision": 7,
			"time": "2021-02-17T02:33:29-05:00",
			"created": "2021-02-15T22:40:15-05:00"
		}
	]
}