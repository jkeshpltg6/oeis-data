{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A017898",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 17898,
			"data": "1,0,0,0,1,1,1,1,2,3,4,5,7,10,14,19,26,36,50,69,95,131,181,250,345,476,657,907,1252,1728,2385,3292,4544,6272,8657,11949,16493,22765,31422,43371,59864,82629,114051,157422,217286,299915,413966,571388,788674",
			"name": "Expansion of (1-x)/(1-x-x^4).",
			"comment": [
				"A Lamé sequence of higher order.",
				"Essentially the same as A003269, which has much more information.",
				"Number of compositions of n into parts \u003e= 4. - _Joerg Arndt_, Aug 13 2012"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A017898/b017898.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Christian Ballot, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL20/Ballot/ballot22.html\"\u003eOn Functions Expressible as Words on a Pair of Beatty Sequences\u003c/a\u003e, Journal of Integer Sequences, Vol. 20 (2017), Article 17.4.2.",
				"I. M. Gessel, Ji Li, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL16/Gessel/gessel6.html\"\u003eCompositions and Fibonacci identities\u003c/a\u003e, J. Int. Seq. 16 (2013) 13.4.5",
				"J. Hermes, \u003ca href=\"http://dx.doi.org/10.1007/BF01446684\"\u003eAnzahl der Zerlegungen einer ganzen rationalen Zahl in Summanden\u003c/a\u003e, Math. Ann., 45 (1894), 371-380.",
				"Milan Janjic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL19/Janjic/janjic73.html\"\u003eBinomial Coefficients and Enumeration of Restricted Words\u003c/a\u003e, Journal of Integer Sequences, 2016, Vol 19, #16.7.3",
				"T. Mansour, M. Shattuck, \u003ca href=\"http://arxiv.org/abs/1410.6943\"\u003eA monotonicity property for generalized Fibonacci sequences\u003c/a\u003e, arXiv:1410.6943 [math.CO], 2014.",
				"Augustine O. Munagi, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL21/Munagi/munagi10.html\"\u003eInteger Compositions and Higher-Order Conjugation\u003c/a\u003e, J. Int. Seq., Vol. 21 (2018), Article 18.8.5.",
				"J. D. Opdyke, \u003ca href=\"http://dx.doi.org/10.1007/s10852-009-9116-2\"\u003eA unified approach to algorithms generating unrestricted..\u003c/a\u003e, J. Math. Model. Algor. 9 (2010) 53-97",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,0,0,1)."
			],
			"formula": [
				"a(n) = a(n-1) + a(n-4). - _R. J. Mathar_, Mar 06 2008",
				"G.f.: 1/(1-sum(k\u003e=4, x^k)). - _Joerg Arndt_, Aug 13 2012",
				"Apparently a(n) = hypergeometric([1-1/4*n, 5/4-1/4*n, 3/2-1/4*n, 7/4-1/4*n],[4/3-1/3*n, 5/3-1/3*n, 2-1/3*n], -4^4/3^3) for n\u003e=13. - _Peter Luschny_, Sep 18 2014",
				"a(n) = A003269(n+1)-A003269(n). - _R. J. Mathar_, Jun 10 2018"
			],
			"maple": [
				"f := proc(r) local t1,i; t1 := []; for i from 1 to r do t1 := [op(t1),0]; od: for i from 1 to r+1 do t1 := [op(t1),1]; od: for i from 2*r+2 to 50 do t1 := [op(t1),t1[i-1]+t1[i-1-r]]; od: t1; end; # set r = order",
				"a:= n-\u003e (Matrix(4, (i,j)-\u003e if (i=j-1) then 1 elif j=1 then [1, 0$2, 1][i] else 0 fi)^n)[4,4]: seq(a(n), n=0..42); # _Alois P. Heinz_, Aug 04 2008"
			],
			"mathematica": [
				"LinearRecurrence[{1, 0, 0, 1}, {1, 0, 0, 0}, 80] (* _Vladimir Joseph Stephan Orlovsky_, Feb 11 2012 *)",
				"CoefficientList[Series[(1-x)/(1-x-x^4),{x,0,50}],x] (* _Harvey P. Dale_, Sep 12 2019 *)"
			],
			"program": [
				"(PARI) a(n)=([0,1,0,0; 0,0,1,0; 0,0,0,1; 1,0,0,1]^n*[1;0;0;0])[1,1] \\\\ _Charles R Greathouse IV_, Oct 03 2016"
			],
			"xref": [
				"For Lamé sequences of orders 1 through 9 see A000045, A000930, this one, and A017899-A017904."
			],
			"keyword": "nonn,easy",
			"offset": "0,9",
			"author": "_N. J. A. Sloane_",
			"references": 18,
			"revision": 63,
			"time": "2020-07-03T14:55:05-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}