{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A327358",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 327358,
			"data": "1,1,0,2,1,0,5,3,2,0,20,14,10,6,0,180,157,128,91,54,0",
			"name": "Triangle read by rows where T(n,k) is the number of unlabeled antichains of nonempty sets covering n vertices with vertex-connectivity \u003e= k.",
			"comment": [
				"An antichain is a set of sets, none of which is a subset of any other. It is covering if there are no isolated vertices.",
				"The vertex-connectivity of a set-system is the minimum number of vertices that must be removed (along with any empty or duplicate edges) to obtain a non-connected set-system or singleton. Note that this means a single node has vertex-connectivity 0.",
				"If empty edges are allowed, we have T(0,0) = 2."
			],
			"example": [
				"Triangle begins:",
				"    1",
				"    1   0",
				"    2   1   0",
				"    5   3   2   0",
				"   20  14  10   6   0",
				"  180 157 128  91  54   0",
				"Non-isomorphic representatives of the antichains counted in row n = 4:",
				"  {1234}          {1234}           {1234}           {1234}",
				"  {1}{234}        {12}{134}        {123}{124}       {12}{134}{234}",
				"  {12}{34}        {123}{124}       {12}{13}{234}    {123}{124}{134}",
				"  {12}{134}       {12}{13}{14}     {12}{134}{234}   {12}{13}{14}{234}",
				"  {123}{124}      {12}{13}{24}     {123}{124}{134}  {123}{124}{134}{234}",
				"  {1}{2}{34}      {12}{13}{234}    {12}{13}{24}{34} {12}{13}{14}{23}{24}{34}",
				"  {2}{13}{14}     {12}{134}{234}   {12}{13}{14}{234}",
				"  {12}{13}{14}    {123}{124}{134}  {12}{13}{14}{23}{24}",
				"  {12}{13}{24}    {12}{13}{14}{23} {123}{124}{134}{234}",
				"  {1}{2}{3}{4}    {12}{13}{24}{34} {12}{13}{14}{23}{24}{34}",
				"  {12}{13}{234}   {12}{13}{14}{234}",
				"  {12}{134}{234}  {12}{13}{14}{23}{24}",
				"  {123}{124}{134} {123}{124}{134}{234}",
				"  {4}{12}{13}{23} {12}{13}{14}{23}{24}{34}",
				"  {12}{13}{14}{23}",
				"  {12}{13}{24}{34}",
				"  {12}{13}{14}{234}",
				"  {12}{13}{14}{23}{24}",
				"  {123}{124}{134}{234}",
				"  {12}{13}{14}{23}{24}{34}"
			],
			"xref": [
				"Column k = 0 is A261005, or A006602 if empty edges are allowed.",
				"Column k = 1 is A261006 (clutters), if we assume A261006(0) = A261006(1) = 0.",
				"Column k = 2 is A305028 (blobs), if we assume A305028(0) = A305028(2) = 0.",
				"Column k = n - 1 is A327425 (cointersecting).",
				"The labeled version is A327350.",
				"Negated first differences of rows are A327359.",
				"Cf. A006126, A055621, A120338, A293606, A293993, A327334, A327351, A327356."
			],
			"keyword": "nonn,tabl,more",
			"offset": "0,4",
			"author": "_Gus Wiseman_, Sep 09 2019",
			"references": 6,
			"revision": 7,
			"time": "2019-09-10T19:58:02-04:00",
			"created": "2019-09-10T19:58:02-04:00"
		}
	]
}