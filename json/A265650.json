{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A265650",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 265650,
			"data": "1,1,2,1,3,2,4,1,5,3,6,2,7,8,4,9,1,10,11,5,12,3,13,14,6,15,2,16,17,7,18,19,8,20,21,4,22,23,24,9,25,1,26,27,28,10,29,30,31,11,32,33,5,34,35,36,12,37,3,38,39,40,13,41,42,43,14,44,45,6,46,47,48,15,49,2,50,51,52,53,16,54,55,56,57,17,58,59,7,60,61,62,63,18,64,65,66",
			"name": "Removing the first occurrence of 1, 2, 3, ... reproduces the sequence itself. Each run of consecutive removed terms is separated from the next one by a term a(k) \u003c= a(k-1) such that floor(sqrt(a(k))) equals the length of the run.",
			"comment": [
				"A fractal sequence : If one deletes the first occurrence of 1, 2, 3, ... the original sequence is reproduced.",
				"Subsequent runs of consecutive terms which are these first occurrences are separated by a term whose square root yields the length of the preceding run (when rounded down).",
				"Motivated by Project Euler problem 535, see LINKS."
			],
			"link": [
				"Martin Møller Skarbiniks Pedersen, \u003ca href=\"/A265650/b265650.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Project Euler, \u003ca href=\"https://projecteuler.net/problem=535\"\u003eProblem 535: Fractal Sequence\u003c/a\u003e",
				"Clark Kimberling, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL10/Kimberling3/kimberling602.html\"\u003eInterspersions and Fractal Sequences Associated with Fractions c^j/d^k\u003c/a\u003e, Journal of Integer Sequences, Issue 5, Volume 10 (2007), Article 07.5.1"
			],
			"formula": [
				"The sequence contains marked numbers and non-marked numbers.",
				"The marked numbers are consecutive starting with a(1)=1.",
				"Immediately preceding each non-marked number in a(n), there are exactly floor(sqrt(a(n)) [= A000196(a(n))] adjacent marked numbers."
			],
			"example": [
				"The runs of first occurrences of the positive integers are {1}, {2}, {3}, {4}, {5}, {6}, {7, 8}, {9}, {10, 11}, ... each separated from the next one by, respectively, 1, 1, 2, 1, 3, 2, 4, 1, 5, ... where 4 and 5 follow the groups {7, 8} and {10, 11} of length 2 = sqrt(4) = floor(sqrt(5)). - _M. F. Hasler_, Dec 13 2015"
			],
			"program": [
				"(C)",
				"#include \u003cstdio.h\u003e",
				"#include \u003cmath.h\u003e",
				"#define SIZE 1000",
				"unsigned int numbers[SIZE];",
				"int main() {",
				"  unsigned int pointer=0, next=1, circle_count=1, next_circle_number=2, sqrt_non_circle=1;",
				"  numbers[0]=1; printf(\"1\");",
				"  while (next\u003cSIZE) {",
				"    if (circle_count==sqrt_non_circle) {",
				"      numbers[next]=numbers[pointer]; circle_count=0; pointer++;",
				"      sqrt_non_circle=sqrt(numbers[pointer]);",
				"    } else {",
				"      circle_count++; numbers[next]=next_circle_number;",
				"      next_circle_number++;",
				"    }",
				"    printf(\",%u\",numbers[next]); next++;",
				"  }",
				"}",
				"(PARI) A265650(n, list=0, a=[1], cc=0, nc=1, p=0)={for(i=2, n, a=concat(a, if(0\u003c=cc-=1, nc+=1, cc=sqrtint(a[!!p+p+=1]); a[p]))); list\u0026\u0026return(a); a[n]} \\\\ Set 2nd optional arg.to 1 to return the whole list. - _M. F. Hasler_, Dec 13 2015"
			],
			"xref": [
				"Cf. A000196, A003603, A035513."
			],
			"keyword": "easy,nonn",
			"offset": "1,3",
			"author": "_Martin Møller Skarbiniks Pedersen_, Dec 11 2015",
			"ext": [
				"New name from _M. F. Hasler_, Dec 13 2015"
			],
			"references": 4,
			"revision": 40,
			"time": "2020-02-21T19:59:18-05:00",
			"created": "2015-12-13T08:12:32-05:00"
		}
	]
}