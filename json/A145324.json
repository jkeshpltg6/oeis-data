{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A145324",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 145324,
			"data": "1,1,2,1,5,6,1,9,26,24,1,14,71,154,120,1,20,155,580,1044,720,1,27,295,1665,5104,8028,5040,1,35,511,4025,18424,48860,69264,40320,1,44,826,8624,54649,214676,509004,663696,362880,1,54,1266,16884,140889,761166",
			"name": "Triangle read by rows: coefficients of 1; 1(X+2); 1(X+2)(X+3); 1(X+2)(X+3)(X+4); ....",
			"comment": [
				"The last number of row n is n!.",
				"Essentially the triangle given by [1, 0, 1, 0, 1, 0, 1, 0, 1, ...] DELTA [2, 1, 3, 2, 4, 3, 5, 4, 6, 5, ...] where DELTA is the operator defined in A084938. - _Philippe Deléham_, Nov 09 2008",
				"T(n+1,k+1) = a_k(2,3,...,n+1), n \u003e= 0, k = 0..n, with the elementary symmetric function a_k(x[1],x[2],...,x[n]), with a_0(0):=1. E.g., a_2(2,3,4) = 2*3 + 2*4 + 3*4 = 26 = T(4,3). - _Wolfdieter Lang_, Oct 24 2011"
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A145324/b145324.txt\"\u003eTable of n, a(n) for n = 1..11325\u003c/a\u003e (rows 1 \u003c= n \u003c= 150, flattened)",
				"Olivier Bodini, Antoine Genitrini, Mehdi Naima, \u003ca href=\"https://arxiv.org/abs/1808.08376\"\u003eRanked Schröder Trees\u003c/a\u003e, arXiv:1808.08376 [cs.DS], 2018.",
				"Olivier Bodini, Antoine Genitrini, Cécile Mailler, Mehdi Naima, \u003ca href=\"https://hal.archives-ouvertes.fr/hal-02865198\"\u003eStrict monotonic trees arising from evolutionary processes: combinatorial and probabilistic study\u003c/a\u003e, hal-02865198 [math.CO] / [math.PR] / [cs.DS] / [cs.DM], 2020.",
				"Robert E. Moritz, \u003ca href=\"/A001701/a001701.pdf\"\u003eOn the sum of products of n consecutive integers\u003c/a\u003e, Univ. Washington Publications in Math., 1 (No. 3, 1926), 44-49 [Annotated scanned copy]"
			],
			"formula": [
				"T(n,k) = A143491(n+1,n+2-k). - _R. J. Mathar_, Oct 10 2008",
				"T(n,k) = Sum_{m=0..k-1} (-1)^m*|s(n+1, n+2-k+m)|, n \u003e= 1, k = 1..n, with the Stirling numbers of the first kind s(n,k) = A048994(n,k). - _Wolfdieter Lang_, Oct 24 2011",
				"T(n,k) = T(n-1,k)+n*T(n-1,k-1). - _Mikhail Kurkov_, Jun 26 2018"
			],
			"example": [
				"From _Wolfdieter Lang_, Oct 24 2011: (Start)",
				"n\\k 1  2   3    4     5    6     7 ...",
				"1:  1",
				"2:  1  2",
				"3:  1  5   6",
				"4:  1  9  26   24",
				"5:  1 14  71  154   120",
				"6:  1 20 155  580  1044  720",
				"7:  1 27 295 1665  5104 8028  5040",
				"...",
				"T(4,3)= 26 = |s(5,3)| - |s(5,4)| + |s(5,5)| = 35 - 10 + 1.",
				"(End)"
			],
			"maple": [
				"A145324 := proc(n,k) coeftayl( 1*mul(x+i,i=2..n),x=0,n-k) ; end: for n from 1 to 11 do for k from 1 to n do printf(\"%d,\",A145324(n,k)) ; od: od: # _R. J. Mathar_, Oct 10 2008"
			],
			"mathematica": [
				"Table[Reverse[CoefficientList[Product[x+j, {j, 2, k}], x]], {k, 1, 15}] // Flatten (* _Robert A. Russell_, Sep 29 2018 *)"
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Jose Ramon Real_, Oct 07 2008",
			"ext": [
				"More terms from _R. J. Mathar_, Oct 10 2008"
			],
			"references": 11,
			"revision": 33,
			"time": "2020-09-09T17:47:56-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}