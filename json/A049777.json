{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A049777",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 49777,
			"data": "1,3,2,6,5,3,10,9,7,4,15,14,12,9,5,21,20,18,15,11,6,28,27,25,22,18,13,7,36,35,33,30,26,21,15,8,45,44,42,39,35,30,24,17,9,55,54,52,49,45,40,34,27,19,10,66,65,63,60,56,51,45,38,30,21,11,78,77,75,72,68,63,57,50",
			"name": "Triangular array read by rows: T(m,n) = n + n+1 + ... + m = (m+n)(m-n+1)/2.",
			"comment": [
				"Triangle read by rows, T(n,k) = A000217(n) - A000217(k), 0 \u003c= k \u003c n. - _Philippe Deléham_, Mar 07 2013",
				"Subtriangle of triangle in A049780. - _Philippe Deléham_, Mar 07 2013",
				"No primes and all composite numbers (except 2^x) are generated after the first two columns of the square array for this sequence. In other words, no primes and all composites except 2^x are generated when m-n \u003e= 2. - _Bob Selcoe_, Jun 18 2013",
				"Diagonal sums in the square array equal partial sums of squares (A000330). - _Bob Selcoe_, Feb 14 2014",
				"From _Bob Selcoe_, Oct 27 2014: (Start)",
				"The following apply to the triangle as a square array read by rows unless otherwise specified (see Table link);",
				"Conjecture: There is at least one prime in interval [T(n,k), T(n,k+1)]. Since T(n,k+1)/T(n,k) decreases to (k+1)/k as n increases, this is true for k=1 (\"Bertrand's Postulate\", first proved by P. Chebyshev), k=2 (proved by El Bachraoui) and k=3 (proved by Loo).",
				"Starting with T(1,1), The falling diagonal of the first 2 numbers in each column (read by column) are the generalized pentagonal numbers (A001318). That is, the coefficients of T(1,1), T(2,1), T(2,2), T(3,2), T(3,3), T(4,3), T(4,4) etc. are the generalized pentagonal numbers. These are A000326 and A005449 (Pentagonal and Second pentagonal numbers: n*(3*n+1)/2, respectively), interweaved.",
				"Let D(n,k) denote falling diagonals starting with T(n,k):",
				"Treating n as constant: pentagonal numbers of the form n*k + 3*k*(k-1)/2 are D(n,1); sequences A000326, 005449, A045943, A115067, A140090, A140091, A059845, A140672, A140673, A140674, A140675, A151542 are formed by n = 1 through 12, respectively.",
				"Treating k as constant: D(1,k) are (3*n^2 + (4k-5)*n + (k-1)*(k-2))/2. When k = 2(mod3), D(1,k), is same as D(k+1,1) omitting the first (k-2)/3 numbers in the sequences. So D(1,2) is same as D(3,1); D(1,5) is same as D(6,1) omitting the 6; D(1,8) is same as D(9,1) omitting the 9 and 21; etc.",
				"D(1,3) and D(1,4) are sequences A095794 and A140229, respectively.",
				"(End)"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A049777/b049777.txt\"\u003eTable of n, a(n) for n = 1..5000\u003c/a\u003e",
				"M. El Bachraoui, \u003ca href=\"http://www.m-hikari.com/ijcms-password/ijcms-password13-16-2006/elbachraouiIJCMS13-16-2006.pdf\"\u003ePrimes in the interval [2n,3n]\u003c/a\u003e, Int. J. Contemp. Math. Sciences 1:13 (2006), pp. 617-621.",
				"A. Loo, \u003ca href=\"http://www.m-hikari.com/ijcms-2011/37-40-2011/looIJCMS37-40-2011.pdf\"\u003eOn the primes in the interval [3n,4n]\u003c/a\u003e, Int. J. Contemp. Math. Sciences 6 (2011), no. 38, 1871-1882.",
				"S. Ramanujan, \u003ca href=\"http://ramanujan.sirinudi.org/Volumes/published/ram24.html\"\u003eA proof of Bertrand's postulate\u003c/a\u003e, J. Indian Math. Soc., 11 (1919), 181-182.",
				"Vladimir Shevelev, Charles R. Greathouse IV, Peter J. C. Moses, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL16/Moses/moses1.html\"\u003eOn intervals (kn, (k+1)n) containing a prime for all n\u003e1\u003c/a\u003e, Journal of Integer Sequences, Vol. 16 (2013), Article 13.7.3. \u003ca href=\"http://arxiv.org/abs/1212.2785\"\u003earXiv\u003c/a\u003e, arXiv:1212.2785 [math.NT], 2012."
			],
			"formula": [
				"Partial sums of A002260 row terms, starting from the right; e.g., row 3 of A002260 = (1, 2, 3), giving (6, 5, 3). - _Gary W. Adamson_, Oct 23 2007",
				"Sum_{k=0..n-1} (-1)^k*(2*k+1)*A000203(T(n,k)) = (-1)^(n-1)*A000330(n). - _Philippe Deléham_, Mar 07 2013",
				"Read as a square array: T(n,k) = k*(k+2n-1)/2. - _Bob Selcoe_, Oct 27 2014"
			],
			"example": [
				"Rows: {1}; {3,2}; {6,5,3}; ...",
				"Triangle begins:",
				"   1;",
				"   3,  2;",
				"   6,  5,  3;",
				"  10,  9,  7,  4;",
				"  15, 14, 12,  9,  5;",
				"  21, 20, 18, 15, 11,  6;",
				"  28, 27, 25, 22, 18, 13,  7;",
				"  36, 35, 33, 30, 26, 21, 15,  8;",
				"  45, 44, 42, 39, 35, 30, 24, 17,  9;",
				"  55, 54, 52, 49, 45, 40, 34, 27, 19, 10; ..."
			],
			"mathematica": [
				"Flatten[Table[(n+k) (n-k+1)/2,{n,15},{k,n}]] (* _Harvey P. Dale_, Feb 27 2012 *)"
			],
			"program": [
				"(PARI) {T(n,k) = if( k\u003c1 || n\u003ck, 0, (n + k) * (n - k + 1) / 2 )} /* _Michael Somos_, Oct 06 2007 */",
				"(MAGMA) /* As triangle */ [[(m+n)*(m-n+1) div 2: n in [1..m]]: m in [1.. 15]]; // _Vincenzo Librandi_, Oct 27 2014"
			],
			"xref": [
				"Row sums = A000330.",
				"Cf. A001318 (generalized pentagonal numbers).",
				"Cf. A000217, A002260, A049780, A094728, A095794, A140229.",
				"Cf. A000326, 005449, A045943, A115067, A140090, A140091, A059845, A140672, A140673, A140674, A140675, A151542 (pentagonal numbers of form n*k + 3*k*(k-1)/2)."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Clark Kimberling_",
			"references": 15,
			"revision": 62,
			"time": "2020-04-20T03:18:40-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}