{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A196839",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 196839,
			"data": "1,2,1,6,1,1,1,2,2,1,30,1,1,1,1,1,6,1,3,2,1,42,1,2,1,2,1,1,1,6,1,6,1,2,2,1,30,1,3,1,3,1,3,1,1,1,10,1,1,1,5,1,1,2,1,66,1,2,1,1,1,1,1,2,1,1,1,6,1,2,1,1,1,1,1,6,2,1,2730,1,1,1,2,1,1,1,2,1,1,1,1",
			"name": "Triangle of denominators of the coefficient of x^m in the n-th Bernoulli polynomial, 0 \u003c= m \u003c= n.",
			"comment": [
				"The numerator triangle is found under A196838.",
				"This is the row reversed triangle A053383."
			],
			"link": [
				"D. H. Lehmer, \u003ca href=\"http://www.jstor.org/stable/2322383\"\u003eA new approach to Bernoulli polynomials\u003c/a\u003e, The American mathematical monthly 95.10 (1988): 905-911."
			],
			"formula": [
				"T(n,m) = denominator([x^m]Bernoulli(n,x)), n\u003e=0, m=0..n.",
				"E.g.f. of Bernoulli(n,x): z*exp(x*z)/(exp(z)-1).",
				"See the Graham et al. reference given in A196838, eq. (7.80), p. 354.",
				"T(n,m) = denominator(binomial(n,m)*Bernoulli(n-m)). - _Fabián Pereyra_, Mar 04 2020"
			],
			"example": [
				"The triangle starts with",
				"n\\m 0  1  2  3  4  5  6  7  8 ...",
				"0:  1",
				"1:  2  1",
				"2:  6  1  1",
				"3:  1  2  2  1",
				"4: 30  1  1  1  1",
				"5:  1  6  1  3  2  1",
				"6: 42  1  2  1  2  1  1",
				"7:  1  6  1  6  1  2  2  1",
				"8: 30  1  3  1  3  1  3  1  1",
				"...",
				"For the start of the rational triangle A196838(n,m)/a(n,m) see the example section in A196838."
			],
			"maple": [
				"with(ListTools):with(PolynomialTools):",
				"CoeffList := p -\u003e CoefficientList(p, x):",
				"Trow := n -\u003e denom(CoeffList(bernoulli(n, x))):",
				"Flatten([seq(Trow(n), n = 0..12)]); # _Peter Luschny_, Apr 10 2021"
			],
			"xref": [
				"Three versions of coefficients of Bernoulli polynomials: A053382/A053383; for reflected version see A196838/A196839; see also A048998 and A048999."
			],
			"keyword": "nonn,easy,tabl,frac",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Oct 23 2011",
			"ext": [
				"Name edited by _M. F. Hasler_, Mar 09 2020"
			],
			"references": 21,
			"revision": 34,
			"time": "2021-04-10T11:31:15-04:00",
			"created": "2011-10-24T14:34:31-04:00"
		}
	]
}