{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A327921",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 327921,
			"data": "1,0,1,5,1,3,1,3,7,9,1,1,3,5,9,11,13,1,3,5,7,1,5,7,11,13,17,1,2,1,3,5,7,9,13,15,17,19,21,1,5,7,11,1,3,5,7,9,11,15,17,19,21,23,25,1,3,5,1,7,11,13,17,19,23,29,1,3,5,7,9,11,13,15",
			"name": "Irregular triangle T read by rows: row n gives the values determining the zeros of the minimal polynomial ps(n, x) of 2*sin(Pi/n) (coefficients in A228786), for n \u003e= 1.",
			"comment": [
				"The minimal polynomials of the algebraic number s(n) = 2*sin(Pi/n) of degree gamma(n) = A055035(n) = A093819(2*n) has the zeros 2*cos(2*Pi*T(n,m)/c(2*n)), with c(2*n) = A178182(2*n), for m = 1, 2, ..., gamma(n) and n \u003e= 1.",
				"The number s(n) is the length ratio side(n)/R of the regular n-gon inscribed in a circle of radius R.",
				"The motivation to look at these zeros came from the book of Carl Schick, and the paper by Brändli and Beyne. There, only length ratios diagonals/R in 2*(2*m + 1)-gons, for m \u003e= 1, are considered.",
				"If one is interested in length ratios diagonals/side then the minimal polynomials of rho(n) := 2*cos(Pi/n) (smallest diagonal/side) are important. These are given in A187360, called there C(n, x)."
			],
			"reference": [
				"Carl Schick, Trigonometrie und unterhaltsame Zahlentheorie, ISBN 3-9522917-0-6, Bobos Druck, Zürich, 2003."
			],
			"link": [
				"Gerold Brändli and Tim Beyne, \u003ca href=\"https://arxiv.org/abs/1504.02757\"\u003eModified Congruence Modulo n with Half the Amount of Residues\u003c/a\u003e, arXiv:1504.02757v2 [math.NT], 2015 and 2016."
			],
			"formula": [
				"Row n gives the first gamma(n) = A055035(n) members of RRS(c(2*n)), for n \u003e= 1, where RRS(k) is the smallest nonnegative restricted residue system modulo k.",
				"The numbers with odd c(2*n) are n = 2 + 8*k, k \u003e= 0.",
				"The zeros x0^{(n)}_m := 2*cos(2*Pi*T(n,m)/c(2*n)) can be written as polynomials of rho(n) := 2*cos(Pi/n) for even n, and as polynomials of rho(2*n) for odd n as follows. x0^{(n)}_m = R(t*T(n,m), rho(b*n)), with b = 1 or 2 for n even or odd, respectively, and t = 1 for n == 1 (mod 2) and 0 (mod 4), t = 2 and 4 for n == 6 and 2 (mod 8), respectively. Here the monic Chebyshev T polynomials R(n, x) enter, with coefficients given in A127672. This results from 2*n/c(2*n) = 4, 2, 1, 1/2 for n == 2, 6 (mod 8), 0 (mod 4), 1 (mod 2), respectively. Note that rho(n)^2 = 4 - s(n)^2.",
				"In terms of s(n) = 2*sin(Pi/n) the zeros x0^{(n)}_m are written with Chebyshev S (A049310) and R polynomials (A127672) as follows.",
				"  x0^{(n)}_m = sqrt(4 - s(b*n)^2) * {S((T(n,m)-1)/2, -R(2, s(bn))) - S((T(n,m)-3)/2, -R(2, s(b*n)))}, for n == 1 (mod 2) with b(n) = 2, and for n == 0 (mod 4) with b = 1,",
				"  x0^{(n)}_m = (2 - s(n)^2) * {S((T(n,m)-1)/2, R(4, s(n))) - S((T(n,m)-3)/2, R(4, s(n)))}, for n == 6 (mod 8), and",
				"  x0^{(n)}_m = R(T(n,m), R(4, sqrt(4 - s(n)^2))), for n == 2 (mod 8)."
			],
			"example": [
				"The irregular triangle T(n,m) begins:",
				"   n\\m   1 2  3  4  5  6  7  8  9 10 11 12 ...      A178182(2*n)  A055035(n)",
				"   -------------------------------------------------------------------------",
				"   1:    1                                                4            1",
				"   2:    0                                                1            1",
				"   3:    1 5                                             12            2",
				"   4:    1 3                                              8            2",
				"   5:    1 3  7  9                                       20            4",
				"   6:    1                                                6            1",
				"   7:    1 3  5  9 11 13                                 28            6",
				"   8:    1 3  5  7                                       16            4",
				"   9:    1 5  7 11 13 17                                 36            6",
				"  10:    1 2                                              5            2",
				"  11:    1 3  5  7  9 13 15 17 19 21                     44           10",
				"  12:    1 5  7 11                                       24            4",
				"  13:    1 3  5  7  9 11 15 17 19 21 23 25               52           12",
				"  14:    1 3  5                                          14            3",
				"  15:    1 7 11 13 17 19 23 29                           60            8",
				"  16:    1 3  5  7  9 11 13 15                           32            8",
				"  ...",
				"--------------------------------------------------------------------------",
				"Some zeros are:",
				"n = 1:  2*cos(2*Pi*1/4) = 0 = s(1),",
				"n = 2:  2*cos(2*Pi*1/4) = 2 = s(2) (diameter/R),",
				"n = 3:  2*cos(2*Pi*1/12) = -2*cos(2*Pi*5/12) = sqrt(3) = s(3),",
				"n = 5:  2*cos(2*Pi*1/20) = -2*cos(2*Pi*9/20) = sqrt(2 + tau),",
				"        2*cos(2*Pi*3/20) = -2*cos(2*Pi*7/20) = sqrt(tau - 3) = s(5),",
				"with the golden ratio tau = A001622,",
				"n = 10: 2*cos(2*Pi*1/5) = tau - 1 = s(10),  -2*cos(2*Pi*2/5) = -tau.",
				"--------------------------------------------------------------------------"
			],
			"xref": [
				"Cf. A001622, A049310, A055035, A093819, A127672, A178182, A187360, A228786."
			],
			"keyword": "nonn,easy,tabf",
			"offset": "1,4",
			"author": "_Wolfdieter Lang_, Nov 02 2019",
			"references": 1,
			"revision": 30,
			"time": "2021-12-11T02:34:12-05:00",
			"created": "2019-11-04T07:16:29-05:00"
		}
	]
}