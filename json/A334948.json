{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A334948",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 334948,
			"data": "1,1,1,1,1,1,1,2,1,2,1,2,1,2,1,2,1,2,1,2,2,2,1,3,1,2,2,2,1,3,1,2,2,2,1,3,1,2,2,3,1,3,1,3,2,2,1,4,1,2,2,3,1,3,1,3,2,2,1,4,1,2,2,3,2,3,1,3,2,3,1,4,1,2,3,3,1,3,1,4,2,2,1,4,2,2,2,3,1,4,1,3,2,2,2,5,1,2,2,4,1,4,1,3,3",
			"name": "a(n) is the number of partitions of n into consecutive parts that differ by 6.",
			"comment": [
				"Note that all sequences of this family as A000005, A001227, A038548, A117277, A334461, A334541, etc. could be prepended with a(0) = 1 when they are interpreted as sequences of number of partitions, since A000041(0) = 1. However here a(0) is omitted in accordance with the mentioned members of the same family.",
				"For the relation to octagonal numbers see also A334946."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A334948/b334948.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"G.f.: Sum_{k\u003e=1} x^(k*(3*k - 2)) / (1 - x^k). - _Ilya Gutkovskiy_, Nov 23 2020"
			],
			"example": [
				"For n = 24 there are three partitions of 24 into consecutive parts that differ by 6, including 24 as a valid partition. They are [24], [15, 9] and [14, 8, 2], so a(24) = 3."
			],
			"mathematica": [
				"nmax = 105;",
				"col[k_] := col[k] = CoefficientList[Sum[x^(n(k n - k + 2)/2 - 1)/(1 - x^n), {n, 1, nmax}] + O[x]^nmax, x];",
				"a[n_] := col[6][[n]];",
				"Array[a, nmax] (* _Jean-François Alcover_, Nov 30 2020 *)",
				"Table[Count[IntegerPartitions[n],_?(Union[Abs[Differences[#]]]=={6}\u0026)]+1,{n,110}] (* _Harvey P. Dale_, Dec 07 2020 *)"
			],
			"program": [
				"(PARI) my(N=66, x='x+O('x^N)); Vec(sum(k=1, N, x^(k*(3*k-2))/(1-x^k))) \\\\ _Seiichi Manyama_, Dec 04 2020"
			],
			"xref": [
				"Row sums of A334946.",
				"Column k=6 of A323345.",
				"Sequences of this family whose consecutive parts differ by k are A000005 (k=0), A001227 (k=1), A038548 (k=2), A117277 (k=3), A334461 (k=4), A334541 (k=5), this sequence (k=6).",
				"Cf. A000041, A000567, A303300, A334947,  A334949, A334953."
			],
			"keyword": "nonn",
			"offset": "1,8",
			"author": "_Omar E. Pol_, May 27 2020",
			"references": 8,
			"revision": 32,
			"time": "2020-12-07T13:31:10-05:00",
			"created": "2020-11-22T12:17:59-05:00"
		}
	]
}