{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A116595",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 116595,
			"data": "1,0,1,1,1,1,1,1,2,2,1,1,4,2,4,4,2,1,2,8,4,1,6,8,6,2,5,12,10,3,9,16,12,4,1,7,23,19,6,1,16,24,25,10,2,11,40,33,14,3,22,45,41,22,5,20,59,63,27,6,1,33,72,73,42,10,1,28,99,101,53,14,2,51,108,127,75,21,3,42,153,167",
			"name": "Triangle read by rows: T(n,k) is the number of partitions of n having exactly k parts that appear exactly once (n\u003e=0, k\u003e=0).",
			"comment": [
				"Row n has 1+floor([sqrt(1+8n)-1]/2) terms. Row sums yield the partition numbers (A000041). T(n,0)=A007690(n). Sum(k*T(n,k),k\u003e=0)=A024786(n+1)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A116595/b116595.txt\"\u003eRows n = 0..500, flattened\u003c/a\u003e"
			],
			"formula": [
				"G.f.: product(1+tx^j+x^(2j)/(1-x^j), j=1..infinity).",
				"More generally, g.f. for the number of partitions of n having exactly k parts that appear exactly m times is product((t-1)*x^(m*j)+1/(1-x^j), j=1..infinity). - _Vladeta Jovovic_, Feb 21 2006"
			],
			"example": [
				"T(7,2) = 4 because we have [6,1], [5,2], [4,3], [3,2,1,1].",
				"Triangle starts:",
				"1;",
				"0, 1;",
				"1, 1;",
				"1, 1,  1;",
				"2, 2,  1;",
				"1, 4,  2;",
				"4, 4,  2,  1;",
				"2, 8,  4,  1;",
				"6, 8,  6,  2;",
				"5, 12, 10, 3;",
				"9, 16, 12, 4, 1;"
			],
			"maple": [
				"g:=product(1+t*x^j+x^(2*j)/(1-x^j),j=1..40): gser:=simplify(series(g,x=0,23)): P[0]:=1: for n from 1 to 21 do P[n]:=sort(coeff(gser,x^n)) od: for n from 0 to 21 do seq(coeff(P[n],t,j),j=0..floor((sqrt(1+8*n)-1)/2)) od; # yields sequence in triangular form",
				"# second Maple program:",
				"b:= proc(n, i) option remember; local j; if n=0 then 1",
				"      elif i\u003c1 then 0 else []; for j from 0 to n/i do zip((x, y)",
				"      -\u003ex+y, %, [`if`(j=1, 0, [][]), b(n-i*j, i-1)], 0) od; %[] fi",
				"    end:",
				"T:= n-\u003e b(n, n):",
				"seq(T(n), n=0..30);  # _Alois P. Heinz_, Nov 07 2012"
			],
			"mathematica": [
				"b[n_, i_] := b[n, i] = Module[{j, pc}, If[n == 0, pc = {1}, If[i\u003c1, pc = {0}, pc = {}; For[j = 0, j \u003c= n/i, j++, pc = Plus @@ PadRight[{pc, If[j == 1, {0}, {}] ~Join~ b[n-i*j, i-1]}]]; pc]]]; T[n_] := b[n, n]; Table[T[n], {n, 0, 30}] // Flatten (* _Jean-François Alcover_, Jan 31 2014, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A000041, A007690, A024786."
			],
			"keyword": "nonn,tabf",
			"offset": "0,9",
			"author": "_Emeric Deutsch_, Feb 18 2006",
			"references": 5,
			"revision": 15,
			"time": "2016-09-12T15:45:56-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}