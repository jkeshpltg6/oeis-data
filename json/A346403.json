{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A346403",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 346403,
			"data": "1,1,1,3,1,1,1,4,3,1,1,1,1,1,1,7,1,1,1,1,1,1,1,1,3,1,4,1,1,1,1,6,1,1,1,3,1,1,1,1,1,1,1,1,1,1,1,1,3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,12,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,7,1,1,1,1,1,1",
			"name": "a(1)=1; for n\u003e1, a(n) gives the sum of the exponents in the different ways to write n as n = x^y, 2 \u003c= x, 1 \u003c= y.",
			"comment": [
				"Denoted by tau(n) in Mycielski (1951), Fehér et al. (2006), and Awel and Küçükaslan (2020).",
				"This function depends only on the prime signature of n (see the Formula section)."
			],
			"link": [
				"Abdu Awel and M. Küçükaslan, \u003ca href=\"http://dx.doi.org/10.22342/jims.26.2.808.224-233\"\u003eA Note on Statistical Limit and Cluster Points of the Arithmetical Functions a_p(n), gamma(n), and tau(n) in the Sense of Density\u003c/a\u003e, Journal of the Indonesian Mathematical Society, Vol. 26, No. 2 (2020), pp. 224-233.",
				"Zoltán Fehér, Béla László, Martin Mačaj and Tibor Šalát, \u003ca href=\"https://eudml.org/doc/128780\"\u003eRemarks on arithmetical functions a_p(n), gamma(n), tau(n)\u003c/a\u003e, Annales Mathematicae et Informaticae, Vol. 33 (2006), pp. 35-43.",
				"Jan Mycielski, \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/cm/cm2/cm2140.pdf\"\u003eSur les représentations des nombres naturels par des puissances à base et exposant naturels\u003c/a\u003e, Colloquium Mathematicum, Vol. 2 (1951), pp. 254-260."
			],
			"formula": [
				"If n = Product_{i} p_i^e_i, then a(n) = sigma(gcd(\u003ce_i\u003e)).",
				"Sum_{n\u003e=1} (a(n)-1)/n = Pi^2/6 + 1 (= A013661 + 1) (Mycielski, 1951)."
			],
			"example": [
				"4 = 2^2, gcd(2) = 2, sigma(2) = 3, so a(4) = 3. The representations are 4^1 and 2^2, and 1 + 2 = 3.",
				"144 = 2^4 * 3^2, gcd(4,2) = 2, sigma(2) = 3, so a(144) = 3. The representations are 144^1 and 12^2, and 1 + 2 = 3."
			],
			"mathematica": [
				"a[n_] := DivisorSigma[1, GCD @@ FactorInteger[n][[;; , 2]]]; Array[a, 100]"
			],
			"program": [
				"(PARI) a(n) = if (n==1, 1, sigma(gcd(factor(n)[,2]))); \\\\ _Michel Marcus_, Jul 16 2021"
			],
			"xref": [
				"Cf. A000203, A013661, A089723."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Amiram Eldar_, Jul 15 2021",
			"references": 0,
			"revision": 10,
			"time": "2021-07-16T10:06:09-04:00",
			"created": "2021-07-16T04:03:38-04:00"
		}
	]
}