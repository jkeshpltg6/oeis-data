{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A208845",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 208845,
			"data": "1,2,-1,-2,1,-2,-2,0,-2,2,1,0,0,-2,3,2,2,0,0,2,-2,0,0,2,-1,0,2,-2,-2,-2,1,-2,0,-2,-2,2,2,0,-2,0,-4,0,0,0,1,2,0,0,2,0,2,-2,1,2,0,-2,2,0,0,2,0,2,0,2,2,0,-4,0,0,2,-1,-2,0,-2,0,0,0,2,2",
			"name": "Expansion of f(x)^2 in powers of x where f() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Number 73 of the 74 eta-quotients listed in Table I of Martin (1996)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A208845/b208845.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Y. Martin, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-96-01743-6\"\u003eMultiplicative eta-quotients\u003c/a\u003e, Trans. Amer. Math. Soc. 348 (1996), no. 12, 4825-4856, see page 4852 Table I.",
				"Michael Somos, \u003ca href=\"/A030203/a030203.txt\"\u003eIndex to Yves Martin's list of 74 multiplicative eta-quotients and their A-numbers\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-1/12) * (eta(q^2)^3 / (eta(q) * eta(q^4)))^2 in powers of q.",
				"Euler transform of period 4 sequence [ 2, -4, 2, -2, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (576 t)) = (24)^(1/2) (t/i)^(1/2) f(t) where q = exp(2 Pi i t).",
				"a(n) = b(12*n + 1) where b(n) is multiplicative with  b(2^e) = b(3^e) = 0^e, b(p^e) = (1 + (-1)^e) / 2 if p == 7, 11 (mod 12), b(p^e) = (-1)^(e/2) * (1 + (-1)^e) / 2 if p == 5 (mod 12), b(p^e) = (e + 1) * (-1)^(e * ((p%24\u003e1) + x)) if p == 1 (mod 12) and p = x^2 + 9 * y^2.",
				"a(n) = (-1)^n * A002107(n). a(25*n + 2) = -a(n).",
				"Convolution cube is A209941. - _Michael Somos_, Jun 09 2015"
			],
			"example": [
				"G.f. = 1 + 2*x - x^2 - 2*x^3 + x^4 - 2*x^5 - 2*x^6 - 2*x^8 + 2*x^9 + x^10 + ...",
				"G.f. = q + 2*q^13 - q^25 - 2*q^37 + q^49 - 2*q^61 - 2*q^73 - 2*q^97 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ -x]^2 , {x, 0, n}]; (* _Michael Somos_, Jun 09 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A, p, e, m); if( n\u003c0, 0, n = 12*n + 1; A=factor(n); prod( k=1, matsize(A)[1], [p, e] = A[k,]; if( p\u003c5, 0, p%12\u003e1, if( e%2, 0, (-1)^( (p%12==5) * e/2)), for( i=1, sqrtint(p\\9), if( issquare( p - 9*i^2), m=i; break)); (e+1) * (-1)^(e * ( (p%24\u003e1) + m )))))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( (eta(x^2 + A)^3 / (eta(x + A) * eta(x^4 + A)))^2, n))};"
			],
			"xref": [
				"Cf. A002107, A209941."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Mar 03 2012",
			"references": 2,
			"revision": 23,
			"time": "2021-03-12T22:24:46-05:00",
			"created": "2012-03-05T12:25:29-05:00"
		}
	]
}