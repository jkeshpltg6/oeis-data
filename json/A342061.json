{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A342061",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 342061,
			"data": "1,1,1,1,1,1,1,2,2,1,1,3,8,3,1,1,4,16,16,4,1,1,5,38,63,38,5,1,1,7,72,218,218,72,7,1,1,8,134,622,1075,622,134,8,1,1,10,224,1600,4214,4214,1600,224,10,1,1,12,375,3703,14381,22222,14381,3703,375,12,1",
			"name": "Triangle read by rows: T(n,k) is the number of sensed 2-connected (nonseparable) planar maps with n edges and k vertices, n \u003e= 2, 2 \u003c= k \u003c= n.",
			"comment": [
				"The number of faces is n + 2 - k."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A342061/b342061.txt\"\u003eTable of n, a(n) for n = 2..1276\u003c/a\u003e (first 50 rows)",
				"Timothy R. Walsh, \u003ca href=\"https://doi.org/10.1016/j.disc.2004.08.036\"\u003eEfficient enumeration of sensed planar maps\u003c/a\u003e, Discrete Math. 293 (2005), no. 1-3, 263--289. MR2136069 (2006b:05062)."
			],
			"formula": [
				"T(n,k) = T(n, n+2-k)."
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  1, 1;",
				"  1, 1,   1;",
				"  1, 2,   2,   1;",
				"  1, 3,   8,   3,    1;",
				"  1, 4,  16,  16,    4,   1;",
				"  1, 5,  38,  63,   38,   5,   1;",
				"  1, 7,  72, 218,  218,  72,   7, 1;",
				"  1, 8, 134, 622, 1075, 622, 134, 8, 1;",
				"  ..."
			],
			"program": [
				"(PARI) \\\\ See section 4 of Walsh reference.",
				"T(n)={",
				"  my(B=matrix(n, n, i, j, if(i+j \u003c= n+1, (2*i+j-2)!*(2*j+i-2)!/(i!*j!*(2*i-1)!*(2*j-1)!))));",
				"  my(C(i,j)=((i+j-1)*(i+1)*(j+1)/(2*(2*i+j-1)*(2*j+i-1)))*B[(i+1)/2,(j+1)/2]);",
				"  my(D(i,j)=((j+1)/2)*B[i/2, (j+1)/2]);",
				"  my(E(i,j)=((i-1)*(j-1) + 2*(i+j)*(i+j-1))*B[i,j]);",
				"  my(F(i,j)=if(!i, j==1, ((i+j)*(6*j+2*i-5)*j*(2*i+j-1)/(2*(2*i+1)*(2*j+i-2)))*B[i,j]) + if(j-1, binomial(i+2,2)*B[i+1,j-1]));",
				"  vector(n, n, vector(n, i, my(j=n+1-i); B[i,j]",
				"    + (i+j)*if(i%2, if(j%2, C(i,j), D(j,i)), if(j%2, D(i,j)))",
				"    + sumdiv(i+j, d, if(d\u003e1, eulerphi(d)*( if(i%d==0, E(i/d, j/d) ) + if(i%d==1, F((i-1)/d, (j+1)/d)) + if(j%d==1, F((j-1)/d, (i+1)/d)) )))",
				"   )/(2*n+2));",
				"}",
				"{ my(A=T(10)); for(n=1, #A, print(A[n])) }"
			],
			"xref": [
				"Column k=3 is A001399(n-3).",
				"Row sums are A006402.",
				"Cf. A082680 (rooted), A239893, A342059."
			],
			"keyword": "nonn,tabl,changed",
			"offset": "2,8",
			"author": "_Andrew Howroyd_, Mar 30 2021",
			"references": 2,
			"revision": 14,
			"time": "2022-01-02T16:09:59-05:00",
			"created": "2021-03-31T04:56:23-04:00"
		}
	]
}