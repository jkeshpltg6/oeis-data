{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A320525",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 320525,
			"data": "0,0,0,0,1,0,0,2,2,0,0,6,10,4,0,0,12,40,28,6,0,0,28,141,167,64,9,0,0,56,464,824,508,124,12,0,0,120,1480,3840,3428,1300,220,16,0,0,240,4600,16920,21132,11316,2900,360,20,0,0,496,14145,72655,123050,89513,31846,5890,560,25,0,0,992,43052,305140,688850,660978,313190,79256,11060,830,30,0",
			"name": "Triangle read by rows: T(n,k) = number of chiral pairs of color patterns (set partitions) in a row of length n using exactly k colors (subsets).",
			"comment": [
				"Two color patterns are equivalent if we permute the colors. Chiral color patterns must not be equivalent if we reverse the order of the pattern.",
				"If the top entry of the triangle is changed from 0 to 1, this is the number of non-equivalent distinguishing partitions of the path on n vertices (n \u003e= 1) with exactly k parts (1 \u003c= k \u003c= n). - _Bahman Ahmadi_, Aug 21 2019"
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A320525/b320525.txt\"\u003eTable of n, a(n) for n = 1..1275\u003c/a\u003e",
				"B. Ahmadi, F. Alinaghipour and M. H. Shekarriz, \u003ca href=\"https://arxiv.org/abs/1910.12102\"\u003eNumber of Distinguishing Colorings and Partitions\u003c/a\u003e, arXiv:1910.12102 [math.CO], 2019."
			],
			"formula": [
				"T(n,k) = (S2(n,k) - A(n,k))/2, where S2 is the Stirling subset number A008277 and A(n,k) = [n\u003e1] * (k*A(n-2,k) + A(n-2,k-1) + A(n-2,k-2)) + [n\u003c2 \u0026 n==k \u0026 n\u003e=0].",
				"T(n,k) = (A008277(n,k) - A304972(n,k)) / 2 = A008277(n,k) - A284949(n,k) = A284949(n,k) - A304972(n,k)."
			],
			"example": [
				"Triangle begins with T(1,1):",
				"  0;",
				"  0,   0;",
				"  0,   1,     0;",
				"  0,   2,     2,      0;",
				"  0,   6,    10,      4,      0;",
				"  0,  12,    40,     28,      6,      0;",
				"  0,  28,   141,    167,     64,      9,      0;",
				"  0,  56,   464,    824,    508,    124,     12,     0;",
				"  0, 120,  1480,   3840,   3428,   1300,    220,    16,     0;",
				"  0, 240,  4600,  16920,  21132,  11316,   2900,   360,    20,   0;",
				"  0, 496, 14145,  72655, 123050,  89513,  31846,  5890,   560,  25, 0;",
				"  0, 992, 43052, 305140, 688850, 660978, 313190, 79256, 11060, 830, 30, 0;",
				"  ...",
				"For T(3,2)=1, the chiral pair is AAB-ABB.  For T(4,2)=2, the chiral pairs are AAAB-ABBB and AABA-ABAA.  For T(5,2)=6, the chiral pairs are AAAAB-ABBBB, AAABA-ABAAA, AAABB-AABBB, AABAB-ABABB, AABBA-ABBAA, and ABAAB-ABBAB."
			],
			"mathematica": [
				"Ach[n_, k_] := Ach[n, k] = If[n\u003c2, Boole[n==k \u0026\u0026 n\u003e=0], k Ach[n-2,k] + Ach[n-2,k-1] + Ach[n-2,k-2]] (* A304972 *)",
				"Table[(StirlingS2[n, k] - Ach[n, k])/2, {n, 1, 12}, {k, 1, n}] // Flatten"
			],
			"program": [
				"(PARI) \\\\ here Ach is A304972 as square matrix.",
				"Ach(n)={my(M=matrix(n,n,i,k,i\u003e=k)); for(i=3, n, for(k=2, n, M[i,k]=k*M[i-2,k] + M[i-2,k-1] + if(k\u003e2, M[i-2,k-2]))); M}",
				"T(n)={(matrix(n,n,i,k,stirling(i,k,2)) - Ach(n))/2}",
				"{ my(A=T(10)); for(n=1, #A, print(A[n,1..n])) } \\\\ _Andrew Howroyd_, Sep 18 2019"
			],
			"xref": [
				"Columns 1-6 are A000004, A122746(n-2), A320526, A320527, A320528, A320529.",
				"Row sums are A320937.",
				"Cf. A008277 (oriented), A284949 (unoriented), A304972 (achiral)."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "1,8",
			"author": "_Robert A. Russell_, Oct 14 2018",
			"references": 8,
			"revision": 26,
			"time": "2019-11-05T05:59:47-05:00",
			"created": "2018-10-22T13:48:55-04:00"
		}
	]
}