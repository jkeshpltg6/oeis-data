{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A094310",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 94310,
			"data": "1,2,1,6,3,2,24,12,8,6,120,60,40,30,24,720,360,240,180,144,120,5040,2520,1680,1260,1008,840,720,40320,20160,13440,10080,8064,6720,5760,5040,362880,181440,120960,90720,72576,60480,51840,45360,40320,3628800,1814400,1209600,907200,725760,604800,518400,453600,403200,362880",
			"name": "Triangle read by rows: T(n,k), the k-th term of the n-th row, is the product of all numbers from 1 to n except k: T(n,k) = n!/k.",
			"comment": [
				"The sum of the rows gives A000254 (Stirling numbers of first kind). The first column and the leading diagonal are factorials given by A000142 with offsets of 0 and 1.",
				"T(n,k) is the number of length k cycles in all permutations of {1..n}.",
				"Second diagonal gives A001048(n). - _Anton Zakharov_, Oct 24 2016",
				"T(n,k) is the number of permutations of [n] with all elements of [k] in a single cycle. To prove this result, let m denote the length of the cycle containing {1,..,k}. Letting m run from k to n, we obtain T(n,k) = Sum_{m=k..n} (C(n-k,m-k)*(m-1)!*(n-m)!) = n!/k. See an example below. - _Dennis P. Walsh_, May 24 2020"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A094310/b094310.txt\"\u003eRows n = 1..141, flattened\u003c/a\u003e"
			],
			"formula": [
				"E.g.f. for column k: x^k/(k*(1-x)).",
				"T(n,k)*k = n*n! = A001563(n)."
			],
			"example": [
				"Triangle begins as:",
				"      1;",
				"      2,     1;",
				"      6,     3,     2;",
				"     24,    12,     8,     6;",
				"    120,    60,    40,    30,   24;",
				"    720,   360,   240,   180,  144,  120;",
				"   5040,  2520,  1680,  1260, 1008,  840,  720;",
				"  40320, 20160, 13440, 10080, 8064, 6720, 5760, 5040;",
				"  ...",
				"T(4,2) counts the 12 permutations of [4] with elements 1 and 2 in the same cycle, namely, (1 2)(3 4), (1 2)(3)(4), (1 2 3)(4), (1 3 2)(4), (1 2 4)(3), (1 4 2)(3), (1 2 3 4), (1 2 4 3), (1 3 2 4), (1 3 4 2), (1 4 2 3), and (1 4 3 2). - _Dennis P. Walsh_, May 24 2020"
			],
			"maple": [
				"seq(seq(n!/k, k=1..n), n=1..10);"
			],
			"mathematica": [
				"Table[n!/k, {n,10}, {k,n}]//Flatten",
				"Table[n!/Range[n], {n,10}]//Flatten (* _Harvey P. Dale_, Mar 12 2016 *)"
			],
			"xref": [
				"Cf. A000142, A000254, A001563, A001710, A002301.",
				"Cf. A061579, A094307, A110468, A133799.",
				"Cf. A129825. - _Johannes W. Meijer_, Jun 18 2009"
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Amarnath Murthy_, Apr 29 2004",
			"ext": [
				"More terms from _Philippe Deléham_, Jun 11 2005"
			],
			"references": 9,
			"revision": 41,
			"time": "2020-06-08T04:26:46-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}