{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A108494",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 108494,
			"data": "1,-2,2,-4,6,-8,12,-16,22,-30,40,-52,68,-88,112,-144,182,-228,286,-356,440,-544,668,-816,996,-1210,1464,-1768,2128,-2552,3056,-3648,4342,-5160,6116,-7232,8538,-10056,11820,-13872,16248,-18996,22176,-25844,30068,-34936,40528",
			"name": "Expansion of f(-q) / f(q) in powers of q where f() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"reference": [
				"T. Miwa, Integrable Lattice Models and Branching Coefficients, Proceedings of the International Congress of Mathematicians, Vol. 1, (Berkeley, Calif., 1986), 862-870, Amer. Math. Soc., Providence, RI, 1987. MR0934288 (89h:82051)"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A108494/b108494.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of (1 - k^2)^(1/8) = k'^(1/4) in powers of q = exp(-Pi K'/K).",
				"Expansion of (theta_4(q) / theta_3(q))^(1/2) = (phi(-q) / phi(q))^(1/2) = chi(-q) / chi(q) = psi(-q) / psi(q) = f(-q) / f(q) where phi(), chi(), psi(), f() are Ramanujan theta functions.",
				"Expansion of eta(q)^2 eta(q^4) / eta(q^2)^3 in powers of q.",
				"Euler transform of period 4 sequence [ -2, 1, -2, 0, ...].",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2)) where f(u, v) = -2*u^2 + v^4 + u^4*v^4.",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^3)) where f(u, v) = u^4 + 2*u*v -2*u^3*v^3 - v^4.",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2), A(x^4)) where f(u, v, w) = -2*u + v*w^2 + u^2*v*w^2.",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2), A(x^3), A(x^6)) where f(u1, u2, u3, u6) = u1^2*u2*u6 - 2*u1*u3 + u2*u3^2*u6.",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^5)) where f(u, v) = 4 * u*v *(1 - u^4) * (1 + v^4) - (v^2 - u^2) * (u + v)^4. - _Michael Somos_, Sep 11 2006",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^7)) where f(u, v) = (1 - u^8) * (1 - v^8) - (1 - u*v)^8. - _Michael Somos_, Jan 01 2006",
				"G.f.: Product_{k\u003e0} (1 - x^(2*k - 1)) / (1 + x^(2*k - 1)).",
				"a(n) = (-1)^n * A080054(n). Convolution inverse of A080054.",
				"Empirical: sum(exp(-Pi)^(n-1)*(-1)^(n+1)*a(n),n=1..infinity) = 2^(1/8). - _Simon Plouffe_, Feb 20 2011",
				"Empirical: sum(exp(-Pi)^(n-1)*a(n),n=1..infinity) = 2^(7/8)/2. - _Simon Plouffe_, Feb 20 2011",
				"a(n) ~ (-1)^n * exp(Pi*sqrt(n/2)) / (2^(9/4) * n^(3/4)). - _Vaclav Kotesovec_, Oct 23 2017",
				"G.f.: exp(-2*Sum_{k\u003e=1} sigma(2*k - 1)*x^(2*k-1)/(2*k - 1)). - _Ilya Gutkovskiy_, Apr 19 2019"
			],
			"example": [
				"1 - 2*q + 2*q^2 - 4*q^3 + 6*q^4 - 8*q^5 + 12*q^6 - 16*q^7 + 22*q^8 - ..."
			],
			"mathematica": [
				"CoefficientList[QPochhammer[q]/QPochhammer[-q] + O[q]^50, q] (* _Jean-François Alcover_, Nov 05 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if(n\u003c0, 0, polcoeff( prod(k=1, (n+1)\\2, (1 - x^(2*k -1)) / (1 + x^(2*k - 1)), 1 + x * O(x^n)), n))}",
				"(PARI) {a(n) = local(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A)^2 * eta(x^4 + A) / eta(x^2 + A)^3, n))}"
			],
			"xref": [
				"Cf. A080054."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Jun 06 2005",
			"references": 5,
			"revision": 37,
			"time": "2021-03-12T22:24:43-05:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}