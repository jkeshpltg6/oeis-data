{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005515",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5515,
			"id": "M4105",
			"data": "1,1,6,14,47,111,280,600,1282,2494,4752,8524,14938,25102,41272,65772,102817,156871,235378,346346,502303,716859,1010256,1404624,1931540,2625658,3534776,4711448,6226148,8156396,10603704",
			"name": "Number of n-bead bracelets (turnover necklaces) of two colors with 10 red beads and n-10 black beads.",
			"comment": [
				"From _Vladimir Shevelev_, Apr 23 2011: (Start)",
				"Also number of non-equivalent (turnover) necklaces of 10 beads each of them painted by one of n colors.",
				"The sequence solves the so-called Reis problem about convex k-gons in case k=10 (see our comment to A032279). (End)"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"N. Zagaglia Salvi, Ordered partitions and colourings of cycles and necklaces, Bull. Inst. Combin. Appl., 27 (1999), 37-40."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A005515/b005515.txt\"\u003eTable of n, a(n) for n = 10..1000\u003c/a\u003e",
				"Hansraj Gupta, \u003ca href=\"https://web.archive.org/web/20200806162943/https://www.insa.nic.in/writereaddata/UpLoadedFiles/IJPAM/20005a66_964.pdf\"\u003eEnumeration of incongruent cyclic k-gons\u003c/a\u003e, Indian J. Pure and Appl. Math., 10 (1979), no.8, 964-999.",
				"W. D. Hoskins and Anne Penfold Street, \u003ca href=\"https://dx.doi.org/10.1017/S1446788700017547\"\u003eTwills on a given number of harnesses\u003c/a\u003e, J. Austral. Math. Soc. Ser. A, 33 (1982), no. 1, 1-15.",
				"W. D. Hoskins and A. P. Street, \u003ca href=\"/A005513/a005513_1.pdf\"\u003eTwills on a given number of harnesses\u003c/a\u003e, J. Austral. Math. Soc. (Series A), 33 (1982), 1-15. (Annotated scanned copy)",
				"F. Ruskey, \u003ca href=\"http://combos.org/necklace\"\u003eNecklaces, Lyndon words, De Bruijn sequences, etc.\u003c/a\u003e",
				"F. Ruskey, \u003ca href=\"/A000011/a000011.pdf\"\u003eNecklaces, Lyndon words, De Bruijn sequences, etc.\u003c/a\u003e [Cached copy, with permission, pdf format only]",
				"Vladimir Shevelev, \u003ca href=\"https://web.archive.org/web/20200722171019/http://www.insa.nic.in/writereaddata/UpLoadedFiles/IJPAM/2000c4e8_629.pdf\"\u003eNecklaces and convex k-gons\u003c/a\u003e, Indian J. Pure and Appl. Math., 35 (2004), no. 5, 629-638.",
				"Vladimir Shevelev, \u003ca href=\"https://www.math.bgu.ac.il/~shevelev/Shevelev_Neclaces.pdf\"\u003eNecklaces and convex k-gons\u003c/a\u003e, Indian J. Pure and Appl. Math., 35 (2004), no. 5, 629-638.",
				"Vladimir Shevelev, \u003ca href=\"http://arxiv.org/abs/1104.4051\"\u003eSpectrum of permanent's values and its extremal magnitudes in Lambda_n^3 and Lambda_n(alpha,beta,gamma)\u003c/a\u003e, arXiv:1104.4051 [math.CO], 2011. (Cf. Section 5).",
				"A. P. Street, \u003ca href=\"/A005513/a005513.pdf\"\u003eLetter to N. J. A. Sloane, N.D.\u003c/a\u003e",
				"\u003ca href=\"/index/Br#bracelets\"\u003eIndex entries for sequences related to bracelets\u003c/a\u003e"
			],
			"formula": [
				"From _Vladimir Shevelev_, Apr 23 2011: (Start)",
				"Put s(n,k,d) = 1, if n == k (mod d), and s(n,k,d) = 0, otherwise. Then a(n) = n*s(n,0,5)/25 + ((384*C(n-1,9) + (n+1)*(n-2)*(n-4)*(n-6)*(n-8))/7680, if n is even; a(n) = (n-5)*s(n,0,5)/25 + ((384*C(n-1,9) + (n-1)*(n-3)*(n-5)*(n-7)*(n-9))/7680, if n is odd. (End)",
				"From _Herbert Kociemba_, Nov 04 2016: (Start)",
				"G.f.: (1/20)*x^10*(1/(-1+x)^10 + 10/((-1+x)^6*(1+x)^5) + 1/(1-x^2)^5 + 4/(-1+x^5)^2 - 4/(-1+x^10)).",
				"G.f.: k=10, x^k*((1/k)*Sum_{d|k} phi(d)*(1-x^d)^(-k/d) + (1+x)/(1-x^2)^floor((k+2)/2))/2. [edited by _Petros Hadjicostas_, Jan 10 2019] (End)"
			],
			"mathematica": [
				"k = 10; Table[(Apply[Plus, Map[EulerPhi[ # ]Binomial[n/#, k/# ] \u0026, Divisors[GCD[n, k]]]]/n + Binomial[If[OddQ[n], n - 1, n - If[OddQ[k], 2, 0]]/2, If[OddQ[k], k - 1, k]/2])/2, {n, k, 50}] (* _Robert A. Russell_, Sep 27 2004 *)",
				"k=10;CoefficientList[Series[x^k*(1/k Plus@@(EulerPhi[#] (1-x^#)^(-(k/#))\u0026/@Divisors[k])+(1+x)/(1-x^2)^Floor[(k+2)/2])/2,{x,0,50}],x] (* _Herbert Kociemba_, Nov 04 2016 *)"
			],
			"xref": [
				"Column k=10 of A052307."
			],
			"keyword": "nonn",
			"offset": "10,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Sequence extended and description corrected by _Christian G. Bower_",
				"Name edited by _Petros Hadjicostas_, Jan 10 2019"
			],
			"references": 4,
			"revision": 72,
			"time": "2021-02-11T09:22:22-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}