{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A331804",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 331804,
			"data": "0,1,1,3,1,5,3,7,1,9,5,5,3,5,7,15,1,17,9,9,5,21,6,7,3,9,5,27,7,7,15,31,1,33,17,17,9,9,9,9,5,9,21,21,6,45,14,15,3,17,9,51,5,21,27,27,7,9,7,27,15,15,31,63,1,65,33,33,17,17,17,17,9,73,10,9",
			"name": "a(n) is the largest positive integer occurring, when written in binary, as a substring in both binary n and its reversal (A030101(n)).",
			"comment": [
				"We set a(0) = 0 by convention.",
				"a(7479) = 29 (\"11101\" in binary) is the first term that does not belong to A057890."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A331804/b331804.txt\"\u003eTable of n, a(n) for n = 0..8192\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A175466(n, A030101(n)) for any n \u003e 0.",
				"a(n) \u003c= n with equality iff n is a binary palindrome (A006995)."
			],
			"example": [
				"The first terms, alongside the binary representations of n and of a(n), are:",
				"  n   a(n)  bin(n)  bin(a(n))",
				"  --  ----  ------  ---------",
				"   0     0       0          0",
				"   1     1       1          1",
				"   2     1      10          1",
				"   3     3      11         11",
				"   4     1     100          1",
				"   5     5     101        101",
				"   6     3     110         11",
				"   7     7     111        111",
				"   8     1    1000          1",
				"   9     9    1001       1001",
				"  10     5    1010        101",
				"  11     5    1011        101",
				"  12     3    1100         11"
			],
			"program": [
				"(PARI) sub(n) = { my (b=binary(n), s=[0]); for (i=1, #b, if (b[i], for (j=i, #b, s=setunion(s, Set(fromdigits(b[i..j], 2)))))); return (s) }",
				"a(n) = my (i=setintersect(sub(n), sub(fromdigits(Vecrev(binary(n)),2)))); i[#i]"
			],
			"xref": [
				"Cf. A006995, A030101, A047813, A057890, A175466."
			],
			"keyword": "nonn,base",
			"offset": "0,4",
			"author": "_Rémy Sigrist_, Jan 26 2020",
			"references": 1,
			"revision": 12,
			"time": "2020-01-28T01:36:53-05:00",
			"created": "2020-01-28T00:31:57-05:00"
		}
	]
}