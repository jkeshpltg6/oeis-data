{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A084158",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 84158,
			"data": "0,1,5,30,174,1015,5915,34476,200940,1171165,6826049,39785130,231884730,1351523251,7877254775,45912005400,267594777624,1559656660345,9090345184445,52982414446326,308804141493510,1799842434514735",
			"name": "a(n) = A000129(n)*A000129(n+1)/2.",
			"comment": [
				"May be called Pell triangles."
			],
			"reference": [
				"S. Falcon, On the Sequences of Products of Two k-Fibonacci Numbers, American Review of Mathematics and Statistics, March 2014, Vol. 2, No. 1, pp. 111-120."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A084158/b084158.txt\"\u003eTable of n, a(n) for n = 0..500\u003c/a\u003e",
				"P. Barry, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL12/Barry4/barry64.html\"\u003eSymmetric Third-Order Recurring Sequences, Chebyshev Polynomials, and Riordan Arrays\u003c/a\u003e, JIS 12 (2009) 09.8.6",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (5,5,-1)."
			],
			"formula": [
				"a(n) = ((sqrt(2)+1)^(2*n+1)-(sqrt(2)-1)^(2*n+1)-2(-1)^n)/16.",
				"a(n) = 5*a(n-1) + 5*a(n-2) - a(n-3). - _Mohamed Bouhamida_, Sep 02 2006; corrected by _Antonio Alberto Olivares_, Mar 29 2008",
				"a(n) = (-1/8)*(-1)^n + (( sqrt(2)+1)/16)*(3+2*sqrt(2))^n + ((-sqrt(2)+1)/16)*(3-2*sqrt(2))^n. - _Antonio Alberto Olivares_, Mar 30 2008",
				"(a(n)-a(n-1))^(1/2) = A000129(n). - _Antonio Alberto Olivares_, Mar 30 2008",
				"O.g.f.: x/((1+x)(x^2-6*x+1)). - _R. J. Mathar_, May 18 2008",
				"a(n) = A041011(n)*A041011(n+1). - _R. K. Guy_, May 18 2008",
				"a(n) = 6*a(n-1)-a(n-2)-(-1)^n. a(n)=7*(a(n-1)-a(n-2))+a(n-3)-2*(-1)^n. - _Mohamed Bouhamida_, Aug 30 2008",
				"In general, for n\u003ek+1, a(n+k) = A003499(k+1)*a(n-1) - a(n-k-2) - (-1)^n A000129(k+1)^2. - _Charlie Marion_, Jan 04 2012",
				"For n\u003e0, a(2n-1)*a(2n+1) = oblong(a(2n)); a(2n)*a(2n+2) = oblong(a(2n+1)-1). - _Charlie Marion_, Jan 09 2012",
				"a(n) = A046729(n)/4. - _Wolfdieter Lang_, Mar 07 2012",
				"a(n) = sum of squares of first n Pell numbers A000129 (A079291). - _N. J. A. Sloane_, Jun 18 2012"
			],
			"maple": [
				"with(combinat): a:=n-\u003efibonacci(n,2)*fibonacci(n-1,2)/2: seq(a(n), n=1..22); # _Zerinvary Lajos_, Apr 04 2008"
			],
			"mathematica": [
				"LinearRecurrence[{5,5,-1},{0,1,5},30] (* _Harvey P. Dale_, Sep 07 2011 *)"
			],
			"program": [
				"(MAGMA) [Floor(((Sqrt(2)+1)^(2*n+1)-(Sqrt(2)-1)^(2*n+1)-2*(-1)^n)/16): n in [0..35]]; // _Vincenzo Librandi_, Jul 05 2011",
				"(PARI) Lucas(n)=([2, 1; 1, 0]^n)[2, 1];",
				"a(n)=Lucas(n)*Lucas(n+1)/2 \\\\ _Charles R Greathouse IV_, Mar 21 2016",
				"(PARI) a(n)=([0,1,0; 0,0,1; -1,5,5]^n*[0;1;5])[1,1] \\\\ _Charles R Greathouse IV_, Mar 21 2016"
			],
			"xref": [
				"Cf. A084159, A084175, A001654, A001652."
			],
			"keyword": "easy,nonn",
			"offset": "0,3",
			"author": "_Paul Barry_, May 18 2003",
			"references": 16,
			"revision": 42,
			"time": "2020-02-15T10:52:26-05:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}