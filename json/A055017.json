{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A055017",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 55017,
			"data": "0,1,2,3,4,5,6,7,8,9,-1,0,1,2,3,4,5,6,7,8,-2,-1,0,1,2,3,4,5,6,7,-3,-2,-1,0,1,2,3,4,5,6,-4,-3,-2,-1,0,1,2,3,4,5,-5,-4,-3,-2,-1,0,1,2,3,4,-6,-5,-4,-3,-2,-1,0,1,2,3,-7,-6,-5,-4,-3,-2,-1,0,1,2,-8,-7,-6,-5,-4,-3",
			"name": "Difference between sums of alternate digits of n starting with the last, i.e., (Sum of ultimate digit of n, antepenultimate digit of n,...)-(sum of penultimate digit of n, preantepenultimate digit of n,...).",
			"comment": [
				"n is divisible by 11 iff a(n) is a multiple of 11",
				"Digital sum with alternating signs starting with a positive sign for the rightmost digit. - _Hieronymus Fischer_, Jun 18 2007",
				"For n\u003c100 equal to (n mod 10 - floor(n/10)) = -A076313(n). - _Hieronymus Fischer_, Jun 18 2007"
			],
			"link": [
				"Hieronymus Fischer, \u003ca href=\"/A055017/b055017.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"formula": [
				"From _Hieronymus Fischer_, Jun 18 2007, Jun 25 2007, Mar 23 2014: (Start)",
				"a(n) = n + 11*Sum_{k\u003e0} (-1)^k*floor(n/10^k).",
				"a(10n+k) = k-a(n), 0\u003c=k\u003c10.",
				"G.f.: Sum_{k\u003e0} (x^k-x^(k+10^k)+(-1)^k*11*x^(10^k))/(1-x^(10^k)) / (1-x).",
				"a(n) = n+11*sum{10\u003c=k\u003c=n, sum{j|k,j\u003e=10, (-1)^floor(log_10(j))*(floor(log_10(j))-floor(log_10(j-1)))}}.",
				"G.f. expressed in terms of Lambert series: g(x)=(x/(1-x)+11*L[b(k)](x))/(1-x) where L[b(k)](x)=sum{k\u003e=0, b(k)*x^k/(1-x^k)} is a Lambert series with b(k)=(-1)^floor(log_10(k)), if k\u003e1 is a power of 10, else b(k)=0.",
				"G.f.: sum{k\u003e0, (1+11*c(k))*x^k}/(1-x), where c(k)=sum{j\u003e1,j|k, (-1)^floor(log_10(j))*(floor(log_10(j))-floor(log_10(j-1)))}.",
				"Formulas for general bases b \u003e 1 (b = 10 for this sequence).",
				"a(n) = sum_{k\u003e=0} (-1)^k*(floor(n/b^k) mod b).",
				"a(n) = n + (b+1)*sum_{k\u003e0} (-1)^k*floor(n/b^k). Both sums are finite with floor(log_b(n)) as the highest index.",
				"a(n) = a(n mod b^k) + (-1)^k*a(floor(n/b^k)), for all k\u003e=0.",
				"a(n) = a(n mod b) - a(floor(n/b)).",
				"a(n) = a(n mod b^2) + a(floor(n/b^2)).",
				"a(n) = (-1)^m*A225693(n), where m = floor(log_b(n)).",
				"a(n) = (-1)^k*A225693(A004086(n)), where k = is the number of trailing 0's of n, formally, k = max(j | n == 0 mod 10^j).",
				"a(A004086(A004086(n))) = (-1)^k*a(n), where k = is the number of trailing 0's in the decimal representation of n. (End)"
			],
			"example": [
				"a(123)=3-2+1=2, a(9875)=5-7+8-9=-3."
			],
			"maple": [
				"sumodigs := proc(n) local dg; dg := convert(n,base,10) ; add(op(1+2*i,dg), i=0..floor(nops(dg)-1)/2) ; end proc:",
				"sumedigs := proc(n) local dg; dg := convert(n,base,10) ; add(op(2+2*i,dg), i=0..floor(nops(dg)-2)/2) ; end proc:",
				"A055017 := proc(n) sumodigs(n)-sumedigs(n) ; end proc: # _R. J. Mathar_, Aug 26 2011"
			],
			"program": [
				"(Smalltalk)",
				"\"Recursive version for general bases\"",
				"\"Set base = 10 for this sequence\"",
				"altDigitalSumRight: base",
				"| s |",
				"base = 1 ifTrue: [^self \\\\ 2].",
				"(s := self // base) \u003e 0",
				"  ifTrue: [^(self - (s * base) - (s altDigitalSumRight: base))]",
				"  ifFalse: [^self]",
				"[by _Hieronymus Fischer_, Mar 23 2014]"
			],
			"xref": [
				"Cf. A225693 (alternating sum of digits).",
				"Unsigned version differs from A040114 and A040115 when n=100 and from A040997 when n=101.",
				"Cf. A076313, A076314, A007953, A003132.",
				"Cf. A004086."
			],
			"keyword": "base,easy,sign",
			"offset": "0,3",
			"author": "_Henry Bottomley_, May 31 2000",
			"references": 25,
			"revision": 26,
			"time": "2020-12-18T18:52:58-05:00",
			"created": "2000-06-15T03:00:00-04:00"
		}
	]
}