{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A138688",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 138688,
			"data": "1,2,4,6,11,18,28,42,62,90,128,180,250,342,464,624,831,1098,1440,1878,2432,3132,4012,5112,6485,8190,10300,12900,16097,20016,24804,30636,37724,46314,56700,69228,84302,102402,124088,150024,180973,217836,261664,313680",
			"name": "McKay-Thompson series of class 24I for the Monster group with a(0) = 2.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Rogers-Ramanujan functions: G(q) (see A003114), H(q) (A003106)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A138688/b138688.txt\"\u003eTable of n, a(n) for n = -1..1000\u003c/a\u003e",
				"K. Bringmann and H. Swisher, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9939-07-08735-7\"\u003eOn a conjecture of Koike on identities between Thompson series and Roger-Ramanujan functions\u003c/a\u003e, Proc. Amer. Math. Soc. 135 (2007), 2317-2326. See page 2325 (A.7).",
				"J. H. Conway and S. P. Norton, \u003ca href=\"https://doi.org/10.1112/blms/11.3.308\"\u003eMonstrous Moonshine\u003c/a\u003e, Bull. Lond. Math. Soc. 11 (1979) 308-339.",
				"Vaclav Kotesovec, \u003ca href=\"http://arxiv.org/abs/1509.08708\"\u003eA method of finding the asymptotics of q-series based on the convolution of generating functions\u003c/a\u003e, arXiv:1509.08708 [math.CO], Sep 30 2015",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"\u003ca href=\"/index/Mat#McKay_Thompson\"\u003eIndex entries for McKay-Thompson series for Monster simple group\u003c/a\u003e"
			],
			"formula": [
				"Expansion of psi(q^4) * phi(-q^3) / (phi(-q) * psi(q^12)) in powers of q where phi(), psi() are Ramanujan theta functions.",
				"Expansion of eta(q^2) * eta(q^3)^2 * eta(q^8)^2 * eta(q^12) / (eta(q)^2 * eta(q^4) * eta(q^6) * eta(q^24)^2) in powers of q.",
				"Euler transform of period 24 sequence [ 2, 1, 0, 2, 2, 0, 2, 0, 0, 1, 2, 0, 2, 1, 0, 0, 2, 0, 2, 2, 0, 1, 2, 0, ...].",
				"G.f.: (G(x) * G(x^24) + x^5 * H(x) * H(x^24))^2 * (G(x^4) * G(x^6) + x^2 * H(x^4) * H(x^6)) where G() and H() are Rogers-Ramanujan functions.",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (24 t)) = f(t) where q = exp(2 Pi i t).",
				"a(n) = A058579(n) unless n=0.",
				"a(n) ~ exp(sqrt(2*n/3)*Pi) / (2^(5/4) * 3^(1/4) * n^(3/4)). - _Vaclav Kotesovec_, Oct 14 2015"
			],
			"example": [
				"G.f. = 1/q + 2 + 4*q + 6*q^2 + 11*q^3 + 18*q^4 + 28*q^5 + 42*q^6 + 62*q^7 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 2, 0, q^2] EllipticTheta[ 4, 0, q^3] / (EllipticTheta[ 4, 0, q] EllipticTheta[ 2, 0, q^6]), {q, 0, n}]; (* _Michael Somos_, Sep 08 2015 *)",
				"nmax=60; CoefficientList[Series[Product[(1+x^k) * (1-x^(3*k))^2 * (1-x^(4*k)) * (1+x^(4*k))^2 * (1+x^(6*k)) / ((1-x^k) * (1-x^(24*k))^2),{k,1,nmax}],{x,0,nmax}],x] (* _Vaclav Kotesovec_, Oct 14 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c-1, 0, n++; A = x * O(x^n); polcoeff( eta(x^2 + A) * eta(x^3 + A)^2 * eta(x^8 + A)^2 * eta(x^12 + A) / (eta(x + A)^2 * eta(x^4 + A) * eta(x^6 + A) * eta(x^24 + A)^2), n))};"
			],
			"xref": [
				"Cf. A058579."
			],
			"keyword": "nonn",
			"offset": "-1,2",
			"author": "_Michael Somos_, Mar 26 2008",
			"references": 2,
			"revision": 19,
			"time": "2021-03-12T22:24:45-05:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}