{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000782",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 782,
			"data": "1,3,8,23,70,222,726,2431,8294,28730,100776,357238,1277788,4605980,16715250,61020495,223931910,825632610,3056887680,11360977650,42368413620,158498860260,594636663660,2236748680998,8433988655580,31872759742852,120699748759856",
			"name": "a(n) = 2*Catalan(n) - Catalan(n-1).",
			"comment": [
				"Number of Dyck (n+1)-paths that have a leading or trailing hill. - _David Scambler_, Aug 22 2012"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A000782/b000782.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Guo-Niu Han, \u003ca href=\"/A196265/a196265.pdf\"\u003eEnumeration of Standard Puzzles\u003c/a\u003e, 2011. [Cached copy]",
				"Guo-Niu Han, \u003ca href=\"https://arxiv.org/abs/2006.14070\"\u003eEnumeration of Standard Puzzles\u003c/a\u003e, arXiv:2006.14070 [math.CO], 2020.",
				"J. R. Stembridge, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-97-01805-9\"\u003eSome combinatorial aspects of reduced words in finite Coxeter groups\u003c/a\u003e, Trans. Amer. Math. Soc. 349(4) (1997), 1285-1332."
			],
			"formula": [
				"Expansion of x*(1 + x*C)*C^2, where C = (1 - (1 - 4*x)^(1/2))/(2*x) is the g.f. for the Catalan numbers, A000108.",
				"Also, expansion of (1 + x^2*C^2)*C - 1, where C = (1 - (1 - 4*x)^(1/2))/(2*x) is the g.f. for Catalan numbers, A000108.",
				"a(n) = (7*n - 5)/(n + 1) * C(n-1), where C(n) = A000108(n). - _Ralf Stephan_, Jan 13 2004",
				"a(n) = leftmost column term of M^(n-1)*V, where M is a tridiagonal matrix with 1's in the super- and subdiagonals, (1, 2, 2, 2, ...) in the main diagonal, and the rest zeros; and V is the vector [1, 2, 0, 0, 0, ...]. - _Gary W. Adamson_, Jun 16 2011",
				"a(n) = A000108(n+1) - A026012(n-1). - _David Scambler_, Aug 22 2012"
			],
			"mathematica": [
				"CoefficientList[Series[(1+x*(1-(1-4*x)^(1/2))/(2*x)^1)*((1-(1-4*x)^(1/2))/(2*x))^2,{x,0,40}],x] (* _Vincenzo Librandi_, Jun 10 2012 *)"
			],
			"program": [
				"(MAGMA) [2*Catalan(n)-Catalan(n-1): n in [1..30]]; // _Vincenzo Librandi_, Jun 10 2012"
			],
			"xref": [
				"Partial sums of A071735.",
				"Essentially the same as A061557.",
				"Cf. A000108, A026012."
			],
			"keyword": "nonn,changed",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"references": 5,
			"revision": 41,
			"time": "2022-01-13T01:30:53-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}