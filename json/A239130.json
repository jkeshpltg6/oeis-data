{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A239130",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 239130,
			"data": "1,1,1,1,1,17,49,49,177,177,177,177,2225,2225,2225,18609,18609,84145,84145,84145,608433,1657009,1657009,1657009,1657009,1657009,1657009,1657009,135874737,404310193,941181105,2014922929,2014922929",
			"name": "Smallest positive integer solution x = a(n) of (3^4)*x - 2^n*y = 1 for n \u003e= 0.",
			"comment": [
				"This is instance m=4 of the m-family of smallest positive solutions [x0(m,n), y0(m,n)] of 3^m*x - 2^n*y = 1, n \u003e= 0, m \u003e= 0, described in a comment on A239125.",
				"The companion sequence is y(n) = y0(4, n) = A239131(n), which is periodic with period length phi(3^4) = 54, where phi(n) = A000010(n) (Euler's totient).",
				"The G.f. can be found from that of the periodic sequence y(n)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A239130/b239130.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"W. Lang, \u003ca href=\"http://arxiv.org/abs/1404.2710\"\u003eOn Collatz' Words, Sequences and Trees\u003c/a\u003e, arXiv preprint arXiv:1404.2710, 2014 and \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Lang/lang6.html\"\u003eJ. Int. Seq. 17 (2014) # 14.11.7\u003c/a\u003e"
			],
			"formula": [
				"a(n) = (1 + 2^n*y0(4, n))/3^4, with y0(4, n) == ((3^4+1)/2)^(n + 3^3) (mod 3^4) = A239131(n), n \u003e= 0.",
				"a(n + 54) = 2^(54)*a(n) - (2^(54)-1)/3^4, n \u003e= 0, from the y0(4, n) periodicity."
			],
			"example": [
				"n=0: 81*1 - 1*80 = 1;",
				"n=1: 81*1 - 2*40 = 1;",
				"n=2: 81*1 - 4*20 = 1;",
				"n=3: 81*1 - 8*10 = 1;",
				"n=4: 81*1 - 16*5 = 1;",
				"n=5: 81*17 - 32*5 =1; ..."
			],
			"mathematica": [
				"Floor[Table[(2^n Mod[(41^(n + 27)), 81])/81 + 1, {n, 0, 40}]] (* _Vincenzo Librandi_, Mar 23 2014 *)"
			],
			"program": [
				"(MAGMA) [Floor(2^n*((41^(n+27) mod 81)/81))+1: n in [0..40]]; // _Vincenzo Librandi_, Mar 23 2014"
			],
			"xref": [
				"Cf. A007583 (m=1), A234038 (m=2), A239125 (m=3), A239131."
			],
			"keyword": "nonn,easy",
			"offset": "0,6",
			"author": "_Wolfdieter Lang_, Mar 22 2014",
			"references": 2,
			"revision": 19,
			"time": "2018-03-06T14:39:32-05:00",
			"created": "2014-03-22T14:12:47-04:00"
		}
	]
}