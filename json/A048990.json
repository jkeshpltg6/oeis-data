{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A048990",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 48990,
			"data": "1,2,14,132,1430,16796,208012,2674440,35357670,477638700,6564120420,91482563640,1289904147324,18367353072152,263747951750360,3814986502092304,55534064877048198,812944042149730764,11959798385860453492,176733862787006701400",
			"name": "Catalan numbers with even index (A000108(2*n), n \u003e= 0): a(n) = binomial(4*n, 2*n)/(2*n+1).",
			"comment": [
				"With interpolated zeros, this is C(n)*(1+(-1)^n)/2 with g.f. given by 2/(sqrt(1+4x) + sqrt(1-4x)). - _Paul Barry_, Sep 09 2004",
				"Self-convolution of a(n)/4^n gives Catalan numbers (A000108). - _Vladimir Reshetnikov_, Oct 10 2016",
				"a(n) is the number of grand Dyck paths from (0,0) to (4n,0) that avoid vertices (2k,0) for all odd k \u003e 0. - _Alexander Burstein_, May 11 2021"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A048990/b048990.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/1910.00875\"\u003eGeneralized Catalan recurrences, Riordan arrays, elliptic curves, and orthogonal polynomials\u003c/a\u003e, arXiv:1910.00875 [math.CO], 2019.",
				"Gi-Sang Cheon, S.-T. Jin, and L. W. Shapiro, \u003ca href=\"http://dx.doi.org/10.1016/j.laa.2015.03.015\"\u003eA combinatorial equivalence relation for formal power series\u003c/a\u003e, Linear Algebra and its Applications, Vol. 491 (2016), pp. 123-137.",
				"Greg Markowsky, \u003ca href=\"https://doi.org/10.1080/10652469.2012.689301\"\u003eA method for deriving hypergeometric and related identities from the H 2 Hardy norm of conformal maps\u003c/a\u003e, Integral Transforms and Special Functions, Vol. 24, No. 4 (2013), pp. 302-313; \u003ca href=\"http://arxiv.org/abs/1205.2458\"\u003earXiv preprint\u003c/a\u003e, arXiv:1205.2458 [math.CV], 2012.",
				"Khodabakhsh Hessami Pilehrood and Tatiana Hessami Pilehrood, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL18/Pilehrood/pile5.html\"\u003eJacobi Polynomials and Congruences Involving Some Higher-Order Catalan Numbers and Binomial Coefficients\u003c/a\u003e, J. Int. Seq., Vol. 18 (2015), Article 15.11.7."
			],
			"formula": [
				"a(n) = 2 * A065097(n) - A000007(n).",
				"G.f.: A(x) = sqrt((1/8)*x^(-1)*(1-sqrt(1-16*x))).",
				"G.f.: 2F1( (1/4, 3/4); (3/2))(16*x). - _Olivier Gérard_ Feb 17 2011",
				"D-finite with recurrence n*(2*n+1)*a(n) - 2*(4*n-1)*(4*n-3)*a(n-1) = 0. - _R. J. Mathar_, Nov 30 2012",
				"E.g.f: 2F2(1/4, 3/4; 1, 3/2; 16*x). - _Vladimir Reshetnikov_, Apr 24 2013",
				"G.f. A(x) satisfies: A(x) = exp( x*A(x)^4 + Integral(A(x)^4 dx) ). - _Paul D. Hanna_, Nov 09 2013",
				"G.f. A(x) satisfies: A(x) = sqrt(1 + 4*x*A(x)^4). - _Paul D. Hanna_, Nov 09 2013",
				"a(n) = hypergeom([1-2*n,-2*n],[2],1). - _Peter Luschny_, Sep 22 2014",
				"a(n) ~ 2^(4*n-3/2)/(sqrt(Pi)*n^(3/2)). - _Ilya Gutkovskiy_, Oct 10 2016",
				"From _Peter Bala_, Feb 27 2020: (Start)",
				"a(n) = (4^n)*binomial(2*n + 1/2, n)/(4*n + 1).",
				"O.g.f.: A(x) = sqrt(c(4*x)), where c(x) = (1 - sqrt(1 - 4*x))/(2*x) is the o.g.f. of the Catalan numbers. Cf. A228411. (End)",
				"Sum_{n\u003e=0} 1/a(n) = A276483. - _Amiram Eldar_, Nov 18 2020"
			],
			"example": [
				"sqrt(2*x^-1*(1-sqrt(1-x))) = 1 + (1/8)*x + (7/128)*x^2 + (33/1024)*x^3 + ..."
			],
			"mathematica": [
				"f[n_] := CatalanNumber[ 2n]; Array[f, 18, 0] (* Or *)",
				"CoefficientList[ Series[ Sqrt[2]/Sqrt[1 + Sqrt[1 - 16 x]], {x, 0, 17}], x] (* _Robert G. Wilson v_ *)",
				"CatalanNumber[Range[0,40,2]] (* _Harvey P. Dale_, Mar 19 2015 *)"
			],
			"program": [
				"(MuPAD) combinat::dyckWords::count(2*n) $ n = 0..28 // _Zerinvary Lajos_, Apr 14 2007",
				"(PARI) /* G.f.: A(x) = exp( x*A(x)^4 + Integral(A(x)^4 dx) ): */",
				"{a(n)=local(A=1+x); for(i=1, n, A=exp(x*A^4 + intformal(A^4 +x*O(x^n)))); polcoeff(A, n)} \\\\ _Paul D. Hanna_, Nov 09 2013",
				"for(n=0, 30, print1(a(n), \", \"))",
				"(Sage)",
				"A048990 = lambda n: hypergeometric([1-2*n,-2*n],[2],1)",
				"[Integer(A048990(n).n()) for n in range(20)] # _Peter Luschny_, Sep 22 2014"
			],
			"xref": [
				"Cf. A000007, A000108, A024492, A065097, A099250, A228411, A276483."
			],
			"keyword": "easy,nonn",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_",
			"references": 18,
			"revision": 109,
			"time": "2021-11-29T09:01:48-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}