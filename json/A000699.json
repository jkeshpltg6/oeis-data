{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000699",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 699,
			"id": "M3618 N1468",
			"data": "1,1,1,4,27,248,2830,38232,593859,10401712,202601898,4342263000,101551822350,2573779506192,70282204726396,2057490936366320,64291032462761955,2136017303903513184,75197869250518812754,2796475872605709079512,109549714522464120960474,4509302910783496963256400,194584224274515194731540740",
			"name": "Number of irreducible chord diagrams with 2n nodes.",
			"comment": [
				"Perturbation expansion in quantum field theory: spinor case in 4 spacetime dimensions.",
				"a(n)*2^(-n) is the coefficient of the x^(2*n-1) term in the series reversal of the asymptotic expansion of 2*DawsonF(x) = sqrt(Pi)*exp(-x^2)*erfi(x) for x -\u003e inf. - _Vladimir Reshetnikov_, Apr 23 2016",
				"The September 2018 talk by _Noam Zeilberger_ (see link to video) connects three topics (planar maps, Tamari lattices, lambda calculus) and eight sequences: A000168, A000260, A000309, A000698, A000699, A002005, A062980, A267827. - _N. J. A. Sloane_, Sep 17 2018",
				"A set partition is topologically connected if the graph whose vertices are the blocks and whose edges are crossing pairs of blocks is connected, where two blocks cross each other if they are of the form {{...x...y...},{...z...t...}} for some x \u003c z \u003c y \u003c t or z \u003c x \u003c t \u003c y. Then a(n) is the number of topologically connected 2-uniform set partitions of {1...2n}. See my links for examples. - _Gus Wiseman_, Feb 23 2019"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A000699/b000699.txt\"\u003eTable of n, a(n) for n = 0..404\u003c/a\u003e (terms up to n=100 from T. D. Noe)",
				"S. Belinschi, M. Bozejko, F. Lehner, and R. Speicher, \u003ca href=\"https://arxiv.org/abs/0910.4263\"\u003eThe normal distribution is \"boxplus\"-infinitely divisible\u003c/a\u003e, arXiv:0910.4263 [math.OA], 2009-2010.",
				"Daniel J. Bernstein, S. Engels, T. Lange, R. Niederhagen, et al., \u003ca href=\"https://cr.yp.to/dlog/sect113r2-20160806.pdf\"\u003eFaster elliptic-curve discrete logarithms on FPGAs\u003c/a\u003e, Preprint, 2016.",
				"Michael Borinsky, \u003ca href=\"https://arxiv.org/abs/1603.01236\"\u003eGenerating asymptotics for factorially divergent sequences\u003c/a\u003e, arXiv preprint arXiv:1603.01236 [math.CO], 2016.",
				"Michael Borinsky, \u003ca href=\"https://doi.org/10.37236/5999\"\u003eGenerating asymptotics for factorially divergent sequences\u003c/a\u003e, El. J. Combin. 25 (2018) P4.1, Sect 7.1.",
				"D. J. Broadhurst and D. Kreimer, \u003ca href=\"http://arXiv.org/abs/hep-th/9912093\"\u003eCombinatoric explosion of renormalization ...\u003c/a\u003e, arXiv:hep-th/9912093, 1999, 2000.",
				"D. J. Broadhurst and D. Kreimer, \u003ca href=\"http://dx.doi.org/10.1016/S0370-2693(00)00051-4\"\u003eCombinatoric explosion of renormalization tamed by Hopf algebra: 30-loop Pad-Borel resummation\u003c/a\u003e, Phys. Lett. B 475 (2000), 63-70.",
				"C. Brouder, \u003ca href=\"https://arxiv.org/abs/hep-th/9906111\"\u003eOn the trees of quantum fields\u003c/a\u003e, arXiv:hep-th/9906111, 1999, p. 6.",
				"Jonathan Burns, \u003ca href=\"http://shell.cas.usf.edu/~saito/DNAweb/SimpleAssemblyTable.txt\"\u003eAssembly Graph Words - Single Transverse Component (Counts)\u003c/a\u003e.",
				"Jonathan Burns, Egor Dolzhenko, Natasa Jonoska, Tilahun Muche and Masahico Saito, \u003ca href=\"http://jtburns.myweb.usf.edu/assembly/papers/Graphs_and_DNA_Recomb_2011.pdf\"\u003eFour-Regular Graphs with Rigid Vertices Associated to DNA Recombination\u003c/a\u003e, May 23, 2011.",
				"Jonathan Burns and Tilahun Muche, \u003ca href=\"http://arxiv.org/abs/1105.2926\"\u003eCounting Irreducible Double Occurrence Words\u003c/a\u003e, arXiv preprint arXiv:1105.2926 [math.CO], 2011.",
				"Julien Courtiel, Karen Yeats, and Noam Zeilberger, \u003ca href=\"https://arxiv.org/abs/1611.04611\"\u003eConnected chord diagrams and bridgeless maps\u003c/a\u003e, arXiv:1611.04611 [math.CO], 2016.",
				"S. Dulucq, \u003ca href=\"/A005819/a005819.pdf\"\u003eEtude combinatoire de problèmes d'énumération, d'algorithmique sur les arbres et de codage par des mots\u003c/a\u003e, a thesis presented to l'Université de Bordeaux I, 1987. (Annotated scanned copy)",
				"P. Flajolet and M. Noy, \u003ca href=\"http://algo.inria.fr/flajolet/Publications/FlNo00.pdf\"\u003eAnalytic Combinatorics of Chord Diagrams\u003c/a\u003e, in: Formal power series and algebraic combinatorics (FPSAC '00) Moscow, 2000, \u003ca href=\"https://doi.org/10.1007/978-3-662-04166-6\"\u003ep 191-201\u003c/a\u003e",
				"M. Klazar, \u003ca href=\"http://dx.doi.org/10.1016/S0196-8858(02)00528-6\"\u003eNon-P-recursiveness of numbers of matchings or linear chord diagrams with many crossings\u003c/a\u003e, Advances in Appl. Math., Vol. 30 (2003), pp. 126-136.",
				"M. Klazar, \u003ca href=\"http://kam.mff.cuni.cz/~klazar/evenodd.pdf\"\u003eCounting even and odd partitions\u003c/a\u003e, Amer. Math. Monthly, 110 (No. 6, 2003), 527-532.",
				"Ali Assem Mahmoud, \u003ca href=\"https://uwaterloo.ca/scholar/a39mahmo/publications/aymptotics-connected-chord-diagrams\"\u003eOn the Asymptotics of Connected Chord Diagrams\u003c/a\u003e, University of Waterloo (Ontario, Canada 2019).",
				"Ali Assem Mahmoud, \u003ca href=\"https://arxiv.org/abs/2009.12688\"\u003eAn Asymptotic Expansion for the Number of 2-Connected Chord Diagrams\u003c/a\u003e, arXiv:2009.12688 [math.CO], 2020.",
				"Ali Assem Mahmoud, \u003ca href=\"https://arxiv.org/abs/2011.04291\"\u003eChord Diagrams and the Asymptotic Analysis of QED-type Theories\u003c/a\u003e, arXiv:2011.04291 [hep-th], 2020.",
				"Ali Assem Mahmoud and Karen Yeats, \u003ca href=\"https://arxiv.org/abs/2010.06550\"\u003eConnected Chord Diagrams and the Combinatorics of Asymptotic Expansions\u003c/a\u003e, arXiv:2010.06550 [math.CO], 2020.",
				"N. Marie, K. Yeats, \u003ca href=\"https://arxiv.org/abs/1210.5457\"\u003eA chord diagram expansion coming from some Dyson-Schwinger equations\u003c/a\u003e, arXiv:1210.5457, Section 4.1.",
				"Albert Nijenhuis and Herbert S. Wilf, \u003ca href=\"http://dx.doi.org/10.1016/0097-3165(79)90023-2\"\u003eThe enumeration of connected graphs and linked diagrams\u003c/a\u003e, J. Combin. Theory Ser. A 27 (1979), no. 3, 356--359. MR0555804 (82b:05074).",
				"V. Pilaud and J. Rué, \u003ca href=\"http://arxiv.org/abs/1307.6440\"\u003eAnalytic combinatorics of chord and hyperchord diagrams with k crossings\u003c/a\u003e, arXiv preprint arXiv:1307.6440 [math.CO], 2013.",
				"J. Riordan, \u003ca href=\"/A001850/a001850_2.pdf\"\u003eLetter, Jul 06 1978\u003c/a\u003e",
				"R. R. Stein, \u003ca href=\"http://dx.doi.org/10.1016/0097-3165(78)90065-1\"\u003eOn a class of linked diagrams, I. Enumeration\u003c/a\u003e, J. Combin. Theory, A 24 (1978), 357-366.",
				"R. R. Stein and C. J. Everett, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(78)90162-0\"\u003eOn a class of linked diagrams, II. Asymptotics\u003c/a\u003e, Discrete Math., 21 (1978), 309-318.",
				"J. Touchard, \u003ca href=\"http://dx.doi.org/10.4153/CJM-1952-001-8\"\u003eSur un problème de configurations et sur les fractions continues\u003c/a\u003e, Canad. J. Math., 4 (1952), 2-25.",
				"J. Touchard, \u003ca href=\"/A000698/a000698.pdf\"\u003eSur un problème de configurations et sur les fractions continues\u003c/a\u003e, Canad. J. Math., 4 (1952), 2-25. [Annotated, corrected, scanned copy]",
				"T. R. S. Walsh and A. B. Lehman, \u003ca href=\"http://dx.doi.org/10.1016/0095-8956(75)90050-7\"\u003eCounting rooted maps by genus. III: Nonseparable maps\u003c/a\u003e, J. Combinatorial Theory Ser. B 18 (1975), 222-259 (nonseparable integer systems on n pairs). (Give an incorrect a(6)=2720.)",
				"Gus Wiseman, \u003ca href=\"/A000699/a000699.png\"\u003eThe a(4) = 27 connected chord diagrams\u003c/a\u003e.",
				"Gus Wiseman, \u003ca href=\"/A000699/a000699_1.png\"\u003eThe a(5) = 248 connected chord diagrams\u003c/a\u003e.",
				"Gus Wiseman, \u003ca href=\"/A000699/a000699.txt\"\u003eConstructive Mathematica program for A000699\u003c/a\u003e.",
				"Noam Zeilberger, \u003ca href=\"https://arxiv.org/abs/1804.10540\"\u003eA theory of linear typings as flows on 3-valent graphs\u003c/a\u003e, arXiv:1804.10540 [cs.LO], 2018.",
				"Noam Zeilberger, \u003ca href=\"https://arxiv.org/abs/1803.10080\"\u003eA Sequent Calculus for a Semi-Associative Law\u003c/a\u003e, arXiv preprint 1803.10030, March 2018 (A revised version of a 2017 conference paper)",
				"Noam Zeilberger, \u003ca href=\"https://vimeo.com/289907363\"\u003eA proof-theoretic analysis of the rotation lattice of binary trees, Part 1 (video)\u003c/a\u003e, Rutgers Experimental Math Seminar, Sep 13 2018. Part 2 is vimeo.com/289910554."
			],
			"formula": [
				"a(n) = (n-1)*Sum_{i=1..n-1} a(i)*a(n-i) for n \u003e 1, with a(1) = a(0) = 1. [Modified to include a(0) = 1. - _Paul D. Hanna_, Nov 06 2020]",
				"A212273(n) = n * a(n). - _Michael Somos_, May 12 2012",
				"G.f. satisfies: A(x) = 1 + x + x^2*[d/dx (A(x) - 1)^2/x]. - _Paul D. Hanna_, Dec 31 2010 [Modified to include a(0) = 1. - _Paul D. Hanna_, Nov 06 2020]",
				"a(n) ~ n^n * 2^(n+1/2) / exp(n+1) * (1 - 31/(24*n) - 2207/(1152*n^2) - 3085547/(414720*n^3) - 1842851707/(39813120*n^4) - ...). - _Vaclav Kotesovec_, Feb 22 2014, extended Oct 23 2017",
				"G.f. A(x) satisfies: 1 = A(x) - x/(A(x) - 2*x/(A(x) - 3*x/(A(x) - 4*x/(A(x) - 5*x/(A(x) - ...))))), a continued fraction relation. - _Paul D. Hanna_, Nov 04 2020"
			],
			"example": [
				"a(31)=627625976637472254550352492162870816129760 was computed using Kreimer's Hopf algebra of rooted trees. It subsumes 2.6*10^21 terms in quantum field theory.",
				"G.f.: A(x) = 1 + x + x^2 + 4*x^3 + 27*x^4 + 248*x^5 + 2830*x^6 +...",
				"where d/dx (A(x) - 1)^2/x = 1 + 4*x + 27*x^2 + 248*x^3 + 2830*x^4 +..."
			],
			"maple": [
				"A000699 := proc(n)",
				"    option remember;",
				"    if n \u003c= 1 then",
				"        1;",
				"    else",
				"        add((2*i-1)*procname(i)*procname(n-i),i=1..n-1) ;",
				"    end if;",
				"end proc:",
				"seq(A000699(n),n=0..30) ; # _R. J. Mathar_, Jun 12 2018"
			],
			"mathematica": [
				"terms = 22; A[_] = 0; Do[A[x_] = x + x^2 * D[A[x]^2/x, x] + O[x]^(terms+1) // Normal, terms]; CoefficientList[A[x], x] // Rest (* _Jean-François Alcover_, Apr 06 2012, after _Paul D. Hanna_, updated Jan 11 2018 *)",
				"a = ConstantArray[0,20]; a[[1]]=1; Do[a[[n]] = (n-1)*Sum[a[[i]]*a[[n-i]],{i,1,n-1}],{n,2,20}]; a (* _Vaclav Kotesovec_, Feb 22 2014 *)",
				"Module[{max = 20, s}, s = InverseSeries[ComplexExpand[Re[Series[2 DawsonF[x], {x, Infinity, 2 max + 1}]]]]; Table[SeriesCoefficient[s, 2 n - 1] 2^n, {n, 1, max}]] (* _Vladimir Reshetnikov_, Apr 23 2016 *)"
			],
			"program": [
				"(PARI) {a(n)=local(A=1+x*O(x^n)); for(i=1, n, A=1+x+x^2*deriv((A-1)^2/x)+x*O(x^n)); polcoeff(A, n)} \\\\ _Paul D. Hanna_, Dec 31 2010 [Modified to include a(0) = 1. - _Paul D. Hanna_, Nov 06 2020]",
				"(PARI) {a(n) = my(A); A = 1+O(x) ; for( i=0, n, A = 1+x + (A-1)*(2*x*A' - A + 1)); polcoeff(A, n)}; /* _Michael Somos_, May 12 2012 [Modified to include a(0) = 1. - _Paul D. Hanna_, Nov 06 2020] */",
				"(PARI)",
				"seq(N) = {",
				"  my(a = vector(N)); a[1] = 1;",
				"  for (n=2, N, a[n] = sum(k=1, n-1, (2*k-1)*a[k]*a[n-k])); a;",
				"};",
				"seq(22)  \\\\ _Gheorghe Coserea_, Jan 22 2017",
				"(Python)",
				"def A000699_list(n):",
				"    list = [1, 1] + [0] * (n - 1)",
				"    for i in range(2, n + 1):",
				"        list[i] = (i - 1) * sum(list[j] * list[i - j] for j in range(1, i))",
				"    return list",
				"print(A000699_list(22)) # _M. Eren Kesim_, Jun 23 2021"
			],
			"xref": [
				"Sequences mentioned in the _Noam Zeilberger_ 2018 video: A000168, A000260, A000309, A000698, A000699, A002005, A062980, A267827.",
				"Cf. A004300, A051862, A212273. Column sums of A232223. First column of A322402.",
				"Cf. A007297, A016098, A099947, A136653, A268815, A306438, A324166, A324172, A324173, A324327."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _David Broadhurst_, Dec 14 1999",
				"Inserted \"chord\" in definition. - _N. J. A. Sloane_, Jan 19 2017",
				"Added a(0)=1. - _N. J. A. Sloane_, Nov 05 2020",
				"Modified formulas slightly to include a(0) = 1. - _Paul D. Hanna_, Nov 06 2020"
			],
			"references": 67,
			"revision": 171,
			"time": "2021-06-24T01:47:29-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}