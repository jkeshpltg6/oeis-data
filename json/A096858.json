{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A096858",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 96858,
			"data": "1,1,2,2,3,4,3,5,6,7,6,9,11,12,13,11,17,20,22,23,24,20,31,37,40,42,43,44,40,60,71,77,80,82,83,84,77,117,137,148,154,157,159,160,161,148,225,265,285,296,302,305,307,308,309,285,433,510,550,570,581,587,590,592,593,594",
			"name": "Triangle read by rows in which row n gives the n-set obtained as the differences {b(n)-b(n-i), 0 \u003c= i \u003c= n-1}, where b() = A005318().",
			"comment": [
				"It is conjectured that the triangle has the property that all 2^n subsets of row n have distinct sums. This conjecture was proved by T. Bohman in 1996 - _N. J. A. Sloane_, Feb 09 2012",
				"It is also conjectured that in some sense this triangle is optimal. See A005318 for further information and additional references."
			],
			"reference": [
				"J. H. Conway and R. K. Guy, Solution of a problem of Erdős, Colloq. Math. 20 (1969), p. 307.",
				"R. K. Guy, Sets of integers whose subsets have distinct sums, pp. 141-154 of Theory and practice of combinatorics. Ed. A. Rosa, G. Sabidussi and J. Turgeon. Annals of Discrete Mathematics, 12. North-Holland 1982.",
				"R. K. Guy, Unsolved Problems in Number Theory, C8."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A096858/b096858.txt\"\u003eRows n = 1..141, flattened\u003c/a\u003e",
				"Tom Bohman, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9939-96-03653-2\"\u003eA sum packing problem of Erdős and the Conway-Guy sequence\u003c/a\u003e, Proc. AMS 124, (No. 12, 1996), pp. 3627-3636."
			],
			"example": [
				"The triangle begins:",
				"{1}",
				"{1,2}",
				"{2,3,4}",
				"{3,5,6,7}",
				"{6,9,11,12,13}",
				"{11,17,20,22,23,24}",
				"{20,31,37,40,42,43,44}",
				"{40,60,71,77,80,82,83,84}",
				"{77,117,137,148,154,157,159,160,161}",
				"{148,225,265,285,296,302,305,307,308,309}",
				"{285,433,510,550,570,581,587,590,592,593,594}",
				"{570,855,1003,1080,1120,1140,1151,1157,1160,1162,1163,1164}",
				"{1120,1690,1975,2123,2200,2240,2260,2271,2277,2280,2282,2283,2284}",
				"{2200,3320,3890,4175,4323,4400,4440,4460,4471,4477,4480,4482,4483,4484}",
				"{4323,6523,7643,8213,8498,8646,8723,8763,8783,8794,8800,8803,8805,8806,8807}"
			],
			"maple": [
				"b:= proc(n) option remember;",
				"      `if`(n\u003c2, n, 2*b(n-1) -b(n-1-floor(1/2 +sqrt(2*n-2))))",
				"    end:",
				"T:= n-\u003e seq(b(n)-b(n-i), i=1..n):",
				"seq(T(n), n=1..15);  # _Alois P. Heinz_, Nov 29 2011"
			],
			"mathematica": [
				"b[n_] := b[n] = If[n \u003c 2, n, 2*b[n-1] - b[n-1-Floor[1/2 + Sqrt[2*n-2]]]]; t[n_] := Table[b[n] - b[n-i], {i, 1, n}]; Table[t[n], {n, 1, 15}] // Flatten (* _Jean-François Alcover_, Jan 14 2014, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A005318."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_, Aug 18 2004",
			"ext": [
				"Typo in definition (limits on i were wrong) corrected and reference added to Bohman's paper. _N. J. A. Sloane_, Feb 09 2012"
			],
			"references": 7,
			"revision": 22,
			"time": "2016-11-06T10:48:02-05:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}