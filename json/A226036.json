{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A226036",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 226036,
			"data": "1,0,58,66,57,104,46,70,144,98,59,59,105,70,66,107,102,46,150,124,105,105,145,71,146,47,145,65,69,115,70,70,71,152,142,104,106,106,103,44,66,66,146,142,189,151,50,62,141,101,107,107,47,104,151,102,186,76",
			"name": "Let abc... be the decimal expansion of n. a(n) is the number of iterations of the map n -\u003e f(n) needed to reach the last number of the cycle, where f(n) = a^a + b^b + c^c + ...",
			"comment": [
				"Additive persistence with powers of decimal digits: number of steps for \"add digit(i) ^ digit(i)\" operation to stabilize when started at n.",
				"Or number of distinct values obtained by iterating n -\u003e A045503(n).",
				"We take 0^0 = 1.",
				"It is conjectured that the trajectory for every number converges to a single number. The growth of a(n) is very slow; for example, a(457) = 211, a(10337) = 213, a(16669) = 214, ..."
			],
			"link": [
				"Michel Lagneau, \u003ca href=\"/A226036/b226036.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"example": [
				"a(0) = 1 because 0 -\u003e 0^0 = 1 with 1 iteration;",
				"a(1) = 0 because 1 -\u003e 1^1 =\u003e 0 iteration;",
				"a(354) = 4 because:",
				"354 -\u003e 3^3 + 5^5 + 4^4 = 3408;",
				"3408 -\u003e 3^3 + 4^4 + 0^0 + 8^8 = 16777500;",
				"16777500 -\u003e 1^1 + 6^6 + 7^7 + 7^7 + 7^7 + 5^5 + 0^0 + 0^0 = 2520413;",
				"2520413 -\u003e 2^2 + 5^5 + 2^2 + 0^0 + 4^4 + 1^1 + 3^3 = 3418 and",
				"3418 is the last number of the cycle because 3418 -\u003e 16777500 is already in the trajectory. We obtain 4 iterations: 354 -\u003e 3408 -\u003e 16777500 -\u003e 2520413 -\u003e 3418."
			],
			"maple": [
				"A000312:=proc(n)",
				"    if n = 0 then 1;",
				"    else add(d^d, d=convert(n, base, 10)) ;",
				"    end if;",
				"end proc:",
				"A226036:= proc(n)",
				"    local traj , c;",
				"    traj := n ;",
				"    c := [n] ;",
				"    while true do",
				"       traj := A000312(traj) ;",
				"       if member(traj, c) then",
				"       return nops(c)-1 ;",
				"       end if;",
				"       c := [op(c), traj] ;",
				"    end do:",
				"end proc:",
				"seq(A226036(n), n=0..100) ;"
			],
			"mathematica": [
				"Unprotect[Power]; 0^0 = 1; Protect[Power]; f[n_] := (cnt++; id = IntegerDigits[n]; Total[id^id]); a[n_] := (cnt = 0; NestWhile[f, n, UnsameQ, All]; cnt-1); Table[a[n], {n, 0, 60}] (* _Jean-François Alcover_, May 24 2013 *)"
			],
			"xref": [
				"Cf. A000312, A031348, A031349, A045503, A133500, A225974."
			],
			"keyword": "nonn,base",
			"offset": "0,3",
			"author": "_Michel Lagneau_, May 24 2013",
			"references": 1,
			"revision": 20,
			"time": "2017-09-16T00:27:08-04:00",
			"created": "2013-05-25T04:41:28-04:00"
		}
	]
}