{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001871",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1871,
			"id": "M4166 N1733",
			"data": "1,6,25,90,300,954,2939,8850,26195,76500,221016,632916,1799125,5082270,14279725,39935214,111228804,308681550,853904015,2355364650,6480104231,17786356776,48715278000,133167004200,363372003625,989900286774",
			"name": "Expansion of 1/(1 - 3*x + x^2)^2.",
			"comment": [
				"Convolution of A001906(n), n \u003e= 1 (even-indexed Fibonacci numbers) with itself.",
				"A001787 and this sequence arise in counting ordered trees of height at most k where only the rightmost branch at the root actually achieves this height and the count is by the number of edges, with k = 3 for A001787 and k = 4 for this sequence.",
				"Gives the number of 3412-avoiding permutations containing exactly one subsequence of type 321. - Dan Daly (ddaly(AT)du.edu), Apr 24 2008"
			],
			"reference": [
				"Rigoberto Flórez, Leandro Junes, José L. Ramírez, Enumerating several aspects of non-decreasing Dyck paths, Discrete Mathematics (2019) Vol. 342, Issue 11, 3079-3097. See page 3092.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A001871/b001871.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Rigoberto Flórez, Robinson Higuita, Alexander Ramírez, \u003ca href=\"https://arxiv.org/abs/1808.01264\"\u003eThe resultant, the discriminant, and the derivative of generalized Fibonacci polynomials\u003c/a\u003e, arXiv:1808.01264 [math.NT], 2018.",
				"S. Kitaev, J. Remmel and M. Tiefenbruck, \u003ca href=\"http://arxiv.org/abs/1201.6243\"\u003eMarked mesh patterns in 132-avoiding permutations I\u003c/a\u003e, arXiv preprint arXiv:1201.6243, 2012. - From _N. J. A. Sloane_, May 09 2012",
				"Sergey Kitaev, Jeffrey Remmel, Mark Tiefenbruck, \u003ca href=\"http://www.emis.de/journals/INTEGERS/papers/p16/p16.Abstract.html\"\u003eQuadrant Marked Mesh Patterns in 132-Avoiding Permutations II\u003c/a\u003e, Electronic Journal of Combinatorial Number Theory, Volume 15 #A16. (\u003ca href=\"http://arxiv.org/abs/1302.2274\"\u003earXiv:1302.2274\u003c/a\u003e)",
				"Eunjeong Lee, Mikiya Masuda, Seonjeong Park, \u003ca href=\"https://arxiv.org/abs/2009.02125\"\u003eOn Schubert varieties of complexity one\u003c/a\u003e, arXiv:2009.02125 [math.AT], 2020.",
				"Valentin Ovsienko, Serge Tabachnikov, \u003ca href=\"https://arxiv.org/abs/1705.01623\"\u003eDual numbers, weighted quivers, and extended Somos and Gale-Robinson sequences\u003c/a\u003e, arXiv:1705.01623 [math.CO], 2017. See p. 9.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"J. Riordan, \u003ca href=\"/A001820/a001820.pdf\"\u003eNotes to N. J. A. Sloane, Jul. 1968\u003c/a\u003e",
				"John Riordan, \u003ca href=\"/A002720/a002720_3.pdf\"\u003eLetter to N. J. A. Sloane, Sep 26 1980 with notes on the 1973 Handbook of Integer Sequences\u003c/a\u003e. Note that the sequences are identified by their N-numbers, not their A-numbers.",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Tu#2wis\"\u003eIndex entries for two-way infinite sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (6,-11,6,-1)."
			],
			"formula": [
				"a(n) = (2*(2*n+1)*F(2*(n+1))+3*(n+1)*F(2*n+1))/5 with F(n) = A000045 (Fibonacci numbers).",
				"a(n) = -a(-4-n) = ((4n+2)F(2n) + (7n+5)F(2n+1))/5 with F(n) = A000045 (Fibonacci numbers).",
				"a(n) = (2*a(n-1) + (n+1)*F(2n+4))/3, where F(n) = A000045 (Fibonacci numbers). - _Emeric Deutsch_, Oct 08 2002",
				"G.f.: 1/(1 - 3*x + x^2)^2.",
				"a(n) = (Sum_{k=0..n} S(k, 3)S(n-k, 3)) S(n, x) = U(n, x/2) Chebyshev polynomials of 2nd kind, A049310. - _Paul Barry_, Nov 14 2003",
				"a(n) = Sum_{k=1..n+1} F(2k)*F(2(n-k+2)) where F(k) is the k-th Fibonacci number. - Dan Daly (ddaly(AT)du.edu), Apr 24 2008",
				"a(n) = 6*a(n-1) - 11*a(n-2) + 6*a(n-3) - a(n-4). - _Vincenzo Librandi_, Mar 14 2011",
				"a(n) = 2*A001870(n) - A238846(n). - _Philippe Deléham_, Mar 06 2014"
			],
			"maple": [
				"A001871:=1/(z**2-3*z+1)**2; # _Simon Plouffe_ in his 1992 dissertation",
				"f:= gfun:-rectoproc({a(n)=6*a(n-1)-11*a(n-2)+6*a(n-3)-a(n-4),",
				"a(0)=1,a(1)=6,a(2)=25,a(3)=90},a(n),remember):",
				"map(f, [$0..50]); # _Robert Israel_, May 05 2017"
			],
			"mathematica": [
				"CoefficientList[Series[1/(1-3x+x^2)^2,{x,0,40}],x] (* _Vincenzo Librandi_, Jun 10 2012 *)"
			],
			"program": [
				"(PARI) a(n)=((4*n+2)*fibonacci(2*n)+(7*n+5)*fibonacci(2*n+1))/5",
				"(MAGMA) I:=[1, 6, 25, 90]; [n le 4 select I[n] else 6*Self(n-1)-11*Self(n-2)+6*Self(n-3)-Self(n-4): n in [1..30]]; // _Vincenzo Librandi_, Jun 10 2012",
				"(PARI) Vec(1/(1-3*x+x^2)^2 + O(x^100)) \\\\ _Altug Alkan_, Oct 31 2015"
			],
			"xref": [
				"Partial sums of A001870 (one half of odd-indexed A001629(n), n \u003e= 2, Fibonacci convolution).",
				"Cf. A001629."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Additional comments from _Wolfdieter Lang_, Apr 07 2000"
			],
			"references": 20,
			"revision": 105,
			"time": "2021-03-12T22:32:35-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}