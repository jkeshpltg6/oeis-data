{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A270747",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 270747,
			"data": "2,2,1,2,2,1,3,7,71,3131,5821925,14364035515947,451397201144015321568515204,88020328073777548345010277436911545872870466008026310,4344173888544359227731947461270153179826227998155726069662805370800638822815760136590246135744249701337368",
			"name": "(r,1)-greedy sequence, where r(k) = 4/Pi^k.",
			"comment": [
				"Let x \u003e 0, and let r = (r(k)) be a sequence of positive irrational numbers.  Let a(1) be the least positive integer m such that r(1)/m \u003c x, and inductively let a(n) be the least positive integer m such that r(1)/a(1) + ... + r(n-1)/a(n-1) + r(n)/m \u003c x.  The sequence (a(n)) is the (r,x)-greedy sequence.  We are interested in choices of r and x for which the series r(1)/a(1) + ... + r(n)/a(n) + ... converges to x.  See A270744 for a guide to related sequences."
			],
			"formula": [
				"a(n) = ceiling(r(n)/s(n)), where s(n) = 1 - r(1)/a(1) - r(2)/a(2) - ... - r(n-1)/a(n-1).",
				"r(1)/a(1) + ... + r(n)/a(n) + ... = 1"
			],
			"example": [
				"a(1) = ceiling(r(1)) = ceiling(1/tau) = ceiling(0.618...) = 2;",
				"a(2) = ceiling(r(2)/(1 - r(1)/1) = 2;",
				"a(3) = ceiling(r(3)/(1 - r(1)/1 - r(2)/2) = 1.",
				"The first 6 terms of the series r(1)/a(1) + ... + r(n)/a(n) + ...  are",
				"0.636..., 0.839..., 0.968..., 0.988..., 0.995..., 0.9994..."
			],
			"mathematica": [
				"$MaxExtraPrecision = Infinity; z = 16;",
				"r[k_] := N[4/Pi^k, 1000]; f[x_, 0] = x;",
				"n[x_, k_] := n[x, k] = Ceiling[r[k]/f[x, k - 1]]",
				"f[x_, k_] := f[x, k] = f[x, k - 1] - r[k]/n[x, k]",
				"x = 1; Table[n[x, k], {k, 1, z}]",
				"N[Sum[r[k]/n[x, k], {k, 1, 18}], 200]"
			],
			"xref": [
				"Cf.  A001620, A270744, A049541."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_Clark Kimberling_, Apr 09 2016",
			"references": 1,
			"revision": 8,
			"time": "2018-10-03T16:12:06-04:00",
			"created": "2016-04-10T10:03:44-04:00"
		}
	]
}