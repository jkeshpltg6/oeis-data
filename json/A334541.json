{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A334541",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 334541,
			"data": "1,1,1,1,1,1,2,1,2,1,2,1,2,1,2,1,2,2,2,1,3,1,2,2,2,1,3,1,2,2,2,1,3,2,2,2,2,2,3,1,2,3,2,1,3,2,2,2,2,2,3,1,2,3,3,1,3,2,2,3,2,2,3,1,3,3,2,1,3,3,2,2,2,2,4,1,2,3,2,2,4,2,2,2,3,2,4,1,2,4,2,1,4,2,3,2,2,2,4,2,2,3,2,1,5",
			"name": "a(n) is the number of partitions of n into consecutive parts that differ by 5.",
			"comment": [
				"Note that all sequences of this family as A000005, A001227, A038548, A117277, A334461, etc. could be prepended with a(0) = 1 when they are interpreted as sequences of number of partitions, since A000041(0) = 1. However here a(0) is omitted in accordance with the mentioned members of the same family.",
				"For the relation to heptagonal numbers see also A334465."
			],
			"link": [
				"David A. Corneth, \u003ca href=\"/A334541/b334541.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"The g.f. for \"consecutive parts that differ by d\" is Sum_{k\u003e=1} x^(k*(d*k-d+2)/2) / (1-x^k); cf. A117277. - _Joerg Arndt_, Nov 30 2020"
			],
			"example": [
				"For n = 27 there are three partitions of 27 into consecutive parts that differ by 5, including 27 as a valid partition. They are [27], [16, 11] and [14, 9, 4], so a(27) = 3."
			],
			"mathematica": [
				"first[n_] := Module[{res = Array[1\u0026, n]}, For[i = 2, True, i++, start = i + 5 Binomial[i, 2]; If[start \u003e n, Return[res]]; For[j = start, j \u003c= n, j += i, res[[j]]++]]];",
				"first[105] (* _Jean-François Alcover_, Nov 30 2020, after _David A. Corneth_ *)"
			],
			"program": [
				"(PARI) seq(N, d)=my(x='x+O('x^N)); Vec(sum(k=1, N, x^(k*(d*k-d+2)/2)/(1-x^k)));",
				"seq(100, 5) \\\\ _Joerg Arndt_, May 06 2020",
				"(PARI) first(n) = { my(res = vector(n, i, 1)); for(i = 2, oo, start = i + 5 * binomial(i, 2); if(start \u003e n, return(res)); forstep(j = start, n, i, res[j]++ ) ); } \\\\ _David A. Corneth_, May 17 2020"
			],
			"xref": [
				"Row sums of A334465.",
				"Column k=5 of A323345.",
				"Sequences of this family whose consecutive parts differ by k are A000005 (k=0), A001227 (k=1), A038548 (k=2), A117277 (k=3), A334461 (k=4), this sequence (k=5).",
				"Cf. A000041, A000566, A303300."
			],
			"keyword": "nonn,easy",
			"offset": "1,7",
			"author": "_Omar E. Pol_, May 05 2020",
			"references": 8,
			"revision": 52,
			"time": "2020-12-02T18:08:39-05:00",
			"created": "2020-05-09T07:20:23-04:00"
		}
	]
}