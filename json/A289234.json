{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A289234",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 289234,
			"data": "0,1,2,3,4,5,6,7,8,9,10,11,18,19,20,21,22,23,12,13,14,15,16,17,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,48,49,50,51,52,53,42,43,44,45,46,47,54,55,56,57,58,59,120,121,122,123,124,125",
			"name": "In primorial base: a(n) is obtained by replacing each nonzero digit of n with its inverse (see Comments for precise definition).",
			"comment": [
				"For a number n \u003e= 0, let d_k, ..., d_0 be the digits of n in primorial base (n = Sum_{i=0..k} d_i * A002110(i), and for i=0..k, 0 \u003c= d_i \u003c prime(i+1)); the digits of a(n) in primorial base, say e_k, ..., e_0, satisfy: for i=0..k:",
				"- if d_i = 0, then e_i = 0,",
				"- if d_i \u003e 0, then e_i is the inverse of d_i mod prime(i+1) (in other words, 1 \u003c= e_i \u003c prime(i+1) and e_i * d_i = 1 mod prime(i+1)).",
				"This sequence is a self-inverse permutation of the nonnegative numbers.",
				"a(n) \u003c A002110(k) iff n \u003c A002110(k) for any n \u003e= 0 and k \u003e= 0.",
				"a(n) = n iff the digits of n in primorial base, say d_k, ..., d_0, satisfy: for i=0..k: d_i = 0, 1 or prime(i+1)-1.",
				"For k \u003e 0: the plotting of the first A002110(k) terms can be obtained by arranging prime(k) copies of the plotting of the first A002110(k-1) terms in a prime(k) X prime(k) grid:",
				"- one copy in the cell at position (0,0),",
				"- one copy in any cell at position (i,j) with i*j = 1 mod prime(k) (with 0 \u003c i \u003c prime(k) and 0 \u003c j \u003c prime(k))."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A289234/b289234.txt\"\u003eTable of n, a(n) for n = 0..30029\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A289234/a289234.png\"\u003eScatterplot of the first 510510 (=17#) terms\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#primorialbase\"\u003eIndex entries for sequences related to primorial base\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A276085(A328617(A276086(n))). - _Antti Karttunen_, Oct 26 2019"
			],
			"example": [
				"The digits of 7772 in primorial base are 3,4,0,0,1,0.",
				"Also:",
				"- 9 * 3 = 27 = 1 mod prime(6) = 13,",
				"- 3 * 4 = 12 = 1 mod prime(5) = 11,",
				"- 1 * 1 = 1 mod prime(2) = 3.",
				"Hence, the digits of a(7772) in primorial base are 9,3,0,0,1,0,",
				"and a(7772) = 9 * 11# + 3 * 7# + 1 * 2# = 21422."
			],
			"program": [
				"(PARI) a(n) = my (pr=1, p=2, v=0); while (n\u003e0, my (d=n%p); if (d\u003e0, v += pr * lift(1/Mod(d,p))); pr *= p; n \\= p; p = next prime(p+1)); return (v)"
			],
			"xref": [
				"Cf. A002110, A049345, A276085, A276086, A328617.",
				"Cf. also A231716, A328622, A328623, A328625, A328626."
			],
			"keyword": "nonn,base",
			"offset": "0,3",
			"author": "_Rémy Sigrist_, Jun 28 2017",
			"references": 11,
			"revision": 13,
			"time": "2019-10-26T09:58:26-04:00",
			"created": "2017-06-29T13:47:21-04:00"
		}
	]
}