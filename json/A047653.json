{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A047653",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 47653,
			"data": "1,2,4,10,26,76,236,760,2522,8556,29504,103130,364548,1300820,4679472,16952162,61790442,226451036,833918840,3084255128,11451630044,42669225172,159497648600,597950875256,2247724108772,8470205600640,31991616634296,121086752349064",
			"name": "Constant term in expansion of (1/2) * Product_{k=-n..n} (1 + x^k).",
			"comment": [
				"Or, constant term in expansion of Product_{k=1..n} (x^k + 1/x^k)^2. - _N. J. A. Sloane_, Jul 09 2008",
				"Or, maximal coefficient of the polynomial (1+x)^2 * (1+x^2)^2 *...* (1+x^n)^2.",
				"a(n) = A000302(n) - A181765(n)."
			],
			"link": [
				"T. D. Noe, Alois P. Heinz and Ray Chandler, \u003ca href=\"/A047653/b047653.txt\"\u003eTable of n, a(n) for n = 0..1669\u003c/a\u003e (terms \u003c 10^1000, first 201 terms from T. D. Noe, next 200 terms from Alois P. Heinz)",
				"Ovidiu Bagdasar and Dorin Andrica, \u003ca href=\"https://dx.doi.org/10.1109/ICMSAO.2017.7934928\"\u003eNew results and conjectures on 2-partitions of multisets\u003c/a\u003e, 2017 7th International Conference on Modeling, Simulation, and Applied Optimization (ICMSAO).",
				"Dorin Andrica and Ovidiu Bagdasar, \u003ca href=\"https://doi.org/10.1007/s11139-021-00418-7\"\u003eOn k-partitions of multisets with equal sums\u003c/a\u003e, The Ramanujan J. (2021) Vol. 55, 421-435.",
				"R. C. Entringer, \u003ca href=\"http://dx.doi.org/10.4153/CMB-1968-036-3\"\u003eRepresentation of m as Sum_{k=-n..n} epsilon_k k\u003c/a\u003e, Canad. Math. Bull., 11 (1968), 289-293.",
				"Steven R. Finch, \u003ca href=\"/A000980/a000980.pdf\"\u003eSignum equations and extremal coefficients\u003c/a\u003e, February 7, 2009. [Cached copy, with permission of the author]",
				"R. P. Stanley, \u003ca href=\"http://dx.doi.org/10.1137/0601021\"\u003eWeyl groups, the hard Lefschetz theorem and the Sperner property\u003c/a\u003e, SIAM J. Algebraic and Discrete Methods 1 (1980), 168-184."
			],
			"formula": [
				"Sum of squares of coefficients in Product_{k=1..n} (1+x^k):",
				"a(n) = Sum_{k=0..n(n+1)/2} A053632(n,k)^2. - _Paul D. Hanna_, Nov 30 2010",
				"a(n) = A000980(n)/2.",
				"a(n) ~ sqrt(3) * 4^n / (sqrt(Pi) * n^(3/2)). - _Vaclav Kotesovec_, Sep 11 2014"
			],
			"maple": [
				"f:=n-\u003ecoeff( expand( mul((x^k+1/x^k)^2,k=1..n) ),x,0);",
				"# second Maple program:",
				"b:= proc(n, i) option remember; `if`(n\u003ei*(i+1)/2, 0,",
				"      `if`(i=0, 1, 2*b(n, i-1)+b(n+i, i-1)+b(abs(n-i), i-1)))",
				"    end:",
				"a:=n-\u003e b(0, n):",
				"seq(a(n), n=0..40);  # _Alois P. Heinz_, Mar 10 2014"
			],
			"mathematica": [
				"b[n_, i_] := b[n, i] = If[n\u003ei*(i+1)/2, 0, If[i == 0, 1, 2*b[n, i-1]+b[n+i, i-1]+b[Abs[n-i], i-1]]]; a[n_] := b[0, n]; Table[a[n], {n, 0, 40}] (* _Jean-François Alcover_, Mar 10 2014, after _Alois P. Heinz_ *)",
				"nmax = 26; d = {1}; a1 = {};",
				"Do[",
				"  i = Ceiling[Length[d]/2];",
				"  AppendTo[a1, If[i \u003e Length[d], 0, d[[i]]]];",
				"  d = PadLeft[d, Length[d] + 2 n] + PadRight[d, Length[d] + 2 n] +",
				"    2 PadLeft[PadRight[d, Length[d] + n], Length[d] + 2 n];",
				", {n, nmax}];",
				"a1 (* _Ray Chandler_, Mar 15 2014 *)"
			],
			"program": [
				"(PARI) a(n)=polcoeff(prod(k=-n,n,1+x^k),0)/2",
				"(PARI) {a(n)=sum(k=0,n*(n+1)/2,polcoeff(prod(m=1,n,1+x^m+x*O(x^k)),k)^2)} \\\\ _Paul D. Hanna_, Nov 30 2010"
			],
			"xref": [
				"Cf. A025591.",
				"Cf. A053632; variant: A127728."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Michael Somos_, Jun 10 2000"
			],
			"references": 12,
			"revision": 56,
			"time": "2021-08-16T13:44:50-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}