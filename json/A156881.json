{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A156881",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 156881,
			"data": "1,1,1,1,1,2,1,1,3,6,1,1,7,21,24,1,1,13,301,315,120,1,1,21,2041,77959,9765,720,1,1,31,8841,3847285,121226245,615195,5040,1,1,43,28861,74450061,87029433985,1131162092095,78129765,40320,1,1,57,77701,806116591,12538953723681,23624400943530205,63330372050122765,19923090075,362880",
			"name": "Square array T(n, k) = Product_{j=1..n} ( Sum_{i=0..j-1} ((k+1)^2 - (k+1))^i ) with T(n, 0) = n!, read by antidiagonals.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A156881/b156881.txt\"\u003eAntidiagonal rows n = 0..25, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = Product_{j=1..n} ( Sum_{i=0..j-1} ((k+1)^2 - (k+1))^i ) with T(n, 0) = n! (square array).",
				"T(n, k) = ( Product_{j=1..n} (k^j*(k+1)^j -1) )/(k^2 + k -1)^n with T(n, 0) = n! (square array). - _G. C. Greubel_, Jun 12 2021"
			],
			"example": [
				"Square array begins as:",
				"   1,   1,     1,       1,        1, ...;",
				"   1,   1,     1,       1,        1, ...;",
				"   2,   3,     7,      13,       21, ...;",
				"   6,  21,   301,    2041,     8841, ...;",
				"  24, 315, 77959, 3847285, 74450061, ...;",
				"Triangle begins as:",
				"  1;",
				"  1, 1;",
				"  1, 1,  2;",
				"  1, 1,  3,     6;",
				"  1, 1,  7,    21,       24;",
				"  1, 1, 13,   301,      315,         120;",
				"  1, 1, 21,  2041,    77959,        9765,           720;",
				"  1, 1, 31,  8841,  3847285,   121226245,        615195,     5040;",
				"  1, 1, 43, 28861, 74450061, 87029433985, 1131162092095, 78129765, 40320;"
			],
			"mathematica": [
				"(* First program *)",
				"T[n_, m_] = If[m==0, n!, Product[Sum[(-(m+1) + (m+1)^2)^i, {i,0,k-1}], {k,n}]];",
				"Table[T[k,n-k], {n,0,12}, {k,0,n}]//Flatten (* modified by _G. C. Greubel_, Jun 12 2021 *)",
				"(* Second program *)",
				"T[n_, k_]= If[k==0, n!, Product[(k^j*(k+1)^j -1), {j,n}]/(k^2+k-1)^n];",
				"Table[T[k, n-k], {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, Jun 12 2021 *)"
			],
			"program": [
				"(Sage)",
				"def A156881(n, k): return factorial(n) if (k==0) else product((k^j*(k+1)^j -1) for j in (1..n))/(k^2 +k-1)^n",
				"flatten([[A156881(k,n-k) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Jun 12 2021"
			],
			"xref": [
				"Cf. A156882, A156883."
			],
			"keyword": "nonn,tabl",
			"offset": "0,6",
			"author": "_Roger L. Bagula_, Feb 17 2009",
			"ext": [
				"Edited by _G. C. Greubel_, Jun 12 2021"
			],
			"references": 6,
			"revision": 16,
			"time": "2021-06-14T03:07:13-04:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}