{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A015443",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 15443,
			"data": "1,1,9,17,89,225,937,2737,10233,32129,113993,371025,1282969,4251169,14514921,48524273,164643641,552837825,1869986953,6292689553,21252585177,71594101601,241614783017,814367595825,2747285859961",
			"name": "Generalized Fibonacci numbers: a(n) = a(n-1) + 8*a(n-2).",
			"comment": [
				"Construct a graph as follows: form the graph whose adjacency matrix is the tensor product of that of P_3 and [1,1;1,1], then add a loop at each of the extremity nodes. a(n-1) counts walks of length n between adjacent nodes. - _Paul Barry_, Nov 12 2004",
				"The compositions of n in which each natural number is colored by one of p different colors are called p-colored compositions of n. For n \u003e= 2, 9*a(n-2) equals the number of 9-colored compositions of n with all parts \u003e= 2, such that no adjacent parts have the same color. - _Milan Janjic_, Nov 26 2011",
				"Pisano period lengths: 1, 1, 6, 1, 24, 6, 16, 1, 6, 24, 110, 6, 56, 16, 24, 2, 16, 6, 60, 24, ... - _R. J. Mathar_, Aug 10 2012"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A015443/b015443.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Joerg Arndt, \u003ca href=\"http://www.jjj.de/fxt/#fxtbook\"\u003eMatters Computational (The Fxtbook)\u003c/a\u003e, p. 318",
				"M. Janjic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL18/Janjic/janjic63.html\"\u003eOn Linear Recurrence Equations Arising from Compositions of Positive Integers\u003c/a\u003e, Journal of Integer Sequences, Vol. 18 (2015), Article 15.4.7.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,8)"
			],
			"formula": [
				"a(n) = (((1+sqrt(33))/2)^(n+1) - ((1-sqrt(33))/2)^(n+1))/sqrt(33).",
				"a(n) = Sum_{k=0..n} A109466(n,k)*(-8)^(n-k). - _Philippe Deléham_, Oct 26 2008",
				"G.f.: 1/(1-x-8*x^2). - _R. J. Mathar_, Apr 07 2011",
				"a(n) = (Sum_{1\u003c=k\u003c=n+1, k odd} C(n+1,k)*33^((k-1)/2))/2^n. - _Vladimir Shevelev_, Feb 05 2014"
			],
			"mathematica": [
				"CoefficientList[Series[1/(1-x-8*x^2), {x,0,50}], x] (* _G. C. Greubel_, Apr 30 2017 *)"
			],
			"program": [
				"(Sage) [lucas_number1(n,1,-8) for n in range(1, 27)] # _Zerinvary Lajos_, Apr 22 2009",
				"(MAGMA) [ n eq 1 select 1 else n eq 2 select 1 else Self(n-1)+8*Self(n-2): n in [1..30] ]; // _Vincenzo Librandi_, Aug 23 2011",
				"(PARI) a(n)=Vec(1/(1-x-8*x^2)+O(x^99)) \\\\ _Charles R Greathouse IV_, Feb 03 2014"
			],
			"xref": [
				"Cf. A015442, A015441, A100302, A100303."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Olivier Gérard_",
			"references": 27,
			"revision": 59,
			"time": "2019-12-07T12:18:19-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}