{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A287615",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 287615,
			"data": "5,13,19,29,37,47,79,101,97,103,113,131,127,137,181,199,181,227,233,229,239,257,277,283,311,307,317,409,383,367,389,409,439,521,463,509,491,509,613,571,541,563,577,587,619,653,677,677,709,743,787,853,743,877",
			"name": "Let r = prime(n). Then a(n) is the smallest prime p such that there is a prime q with p \u003e q \u003e r and p mod q = r.",
			"comment": [
				"Prime p such that p = k * q + r, r \u003c q \u003c p primes; k even multiples such that p is minimal."
			],
			"link": [
				"Charles R Greathouse IV, \u003ca href=\"/A287615/b287615.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Ophir Spector, \u003ca href=\"https://sites.google.com/site/ophirscobwebs/maths-fun\"\u003eC++ code and Excel file\u003c/a\u003e"
			],
			"example": [
				"a(1) = 5, as r = 2, q = 3, p = 5, is the smallest prime such that 5 = 2 mod 3.",
				"a(9) = 97, as r = 23, q = 37, p = 97.  97 = 2 * 37 + 23 is smaller than 139 = 4 * 29 + 23 (A129919)."
			],
			"maple": [
				"f:= proc(n) local p,q,r;",
				"  r:= ithprime(n);",
				"  p:= r+1;",
				"  do",
				"   p:= nextprime(p);",
				"   q:= max(numtheory:-factorset(p-r));",
				"   if q \u003e r then return p fi",
				"  od:",
				"end proc:",
				"map(f, [$1..100]); # _Robert Israel_, Jun 05 2017"
			],
			"mathematica": [
				"a[n_] := Module[{p, q, r}, r = Prime[n]; p = r+1; While[True, p = NextPrime[p]; q = Max[FactorInteger[p-r][[All, 1]]]; If[q\u003er, Return[p]]] ];",
				"Array[a, 100] (* _Jean-François Alcover_, Oct 06 2020, after _Robert Israel_ *)"
			],
			"program": [
				"(PARI) findfirstTerms(n)=my(t:small=0,a:vec=[]);forprime(r=2,,forprime(p=r+2,,forprime(q=r+2,p-2,if(p%q==r,a=concat(a,[p]);if(t++==n,a[1]-=2;return(a),break(2)))))) \\\\ _R. J. Cano_, Jun 06 2017",
				"(PARI) first(n)=my(v=vector(n),best,k=1); v[1]=5; forprime(r=3,prime(n), best=oo; forprime(q=r+2,, if(q\u003e=best, v[k++]=best; next(2)); forstep(p=r+2*q,best,2*q, if(isprime(p), best=p; break)))); v \\\\ _Charles R Greathouse IV_, Jun 07 2017"
			],
			"xref": [
				"Cf. A129919."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_Ophir Spector_, May 27 2017",
			"references": 1,
			"revision": 31,
			"time": "2020-10-06T06:54:52-04:00",
			"created": "2017-06-07T02:32:13-04:00"
		}
	]
}