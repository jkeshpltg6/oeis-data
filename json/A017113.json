{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A017113",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 17113,
			"data": "4,12,20,28,36,44,52,60,68,76,84,92,100,108,116,124,132,140,148,156,164,172,180,188,196,204,212,220,228,236,244,252,260,268,276,284,292,300,308,316,324,332,340,348,356,364,372,380,388,396,404,412,420,428,436,444,452,460,468",
			"name": "a(n) = 8*n + 4.",
			"comment": [
				"Apart from initial term(s), dimension of the space of weight 2n cuspidal newforms for Gamma_0( 65 ).",
				"n such that 16 is the largest power of 2 dividing A003629(k)^n-1 for any k. - _Benoit Cloitre_, Mar 23 2002",
				"Continued fraction expansion of tanh(1/4). - _Benoit Cloitre_, Dec 17 2002",
				"Consider all primitive Pythagorean triples (a,b,c) with c-a=8, sequence gives values for b. (Corresponding values for a are A078371(n), while c follows A078370(n).) - Lambert Klasen (Lambert.Klasen(AT)gmx.net), Nov 19 2004",
				"Also numbers of the form a^2 + b^2 + c^2 + d^2, where a,b,c,d are odd integers. - _Alexander Adamchuk_, Dec 01 2006",
				"If X is an n-set and Y_i (i=1,2,3) mutually disjoint 2-subsets of X then a(n-5) is equal to the number of 4-subsets of X intersecting each Y_i (i=1,2,3). - _Milan Janjic_, Aug 26 2007",
				"From the Cody Clifton's reference : \"There are no 5/8 commutative groups of order 4 mod 8. The maximum commutativity of a non-Abelian group is 5/8, and this degree of commutativity only occurs when the order of the center of the group is equal to one fourth the order of the group [proof given]\". - _Jonathan Vos Post_, May 23 2012",
				"A007814(a(n)) = 2; A037227(a(n)) = 5. - _Reinhard Zumkeller_, Jun 30 2012",
				"With the substitution n -\u003e n-2 this is 8n-12, as in the paper by Gu and Hao. - _Jonathan Vos Post_, Sep 23 2013",
				"Numbers k such that 3^k + 1 is divisible by 41. - _Bruno Berselli_, Aug 22 2018",
				"Numbers whose even divisors are twice the odd ones. - _Paolo P. Lava_, Oct 17 2018"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A017113/b017113.txt\"\u003eTable of n, a(n) for n = 0..1100\u003c/a\u003e",
				"E. Catalan, \u003ca href=\"https://doi.org/10.24033/bsmf.401\"\u003eExtrait d'une lettre\u003c/a\u003e, Bulletin de la S. M. F., tome 17 (1889), pp. 205-206. [If N is a prime number of the form 4*m+1, then 8*N+4 is the sum of four odd squares.]",
				"Cody Clifton, \u003ca href=\"http://www.whitman.edu/mathematics/SeniorProjectArchive/2010/SeniorProject_CodyClifton.pdf\"\u003eCommutativity in non-Abelian Groups\u003c/a\u003e, May 06 2010.",
				"Colin Defant and Noah Kravitz, \u003ca href=\"https://arxiv.org/abs/2201.03461\"\u003eLoops and Regions in Hitomezashi Patterns\u003c/a\u003e, arXiv:2201.03461 [math.CO], 2022. Theorem 1.2.",
				"Meimei Gu and Rongxia Hao, \u003ca href=\"http://arxiv.org/abs/1309.5083\"\u003e3-extra connectivity of 3-ary n-cube networks\u003c/a\u003e, arXiv:1309.5083 [cs.DM], Sep 19, 2013.",
				"Milan Janjic, \u003ca href=\"https://pmf.unibl.org/wp-content/uploads/2017/10/enumfor.pdf\"\u003eTwo Enumerative Functions\u003c/a\u003e.",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e.",
				"William A. Stein, \u003ca href=\"http://wstein.org/Tables/dimskg0new.gp\"\u003eDimensions of the spaces S_k^{new}(Gamma_0(N))\u003c/a\u003e.",
				"William A. Stein, \u003ca href=\"http://wstein.org/Tables/\"\u003eThe modular forms database\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,-1)."
			],
			"formula": [
				"a(n) = A118413(n+1,3) for n\u003e2. - _Reinhard Zumkeller_, Apr 27 2006",
				"a(n) = Sum_{k=0..4*n} ((i^k+1)*(i^(4*n-k)+1), where i=sqrt(-1). - _Bruno Berselli_, Mar 19 2012",
				"a(n) = 4*A005408(n). - _Omar E. Pol_, Apr 17 2016",
				"E.g.f.: (8*x + 4)*exp(x). - _G. C. Greubel_, Apr 26 2018",
				"G.f.: 4*(1+x)/(1-x)^2. - _Wolfdieter Lang_, Oct 27 2020",
				"Sum_{n\u003e=0} (-1)^n/a(n) = Pi/16 (A019683). - _Amiram Eldar_, Dec 11 2021"
			],
			"mathematica": [
				"LinearRecurrence[{2,-1}, {4,12}, 50] (* _G. C. Greubel_, Apr 26 2018 *)"
			],
			"program": [
				"(MAGMA) [8*n+4: n in [0..50]]; // _Vincenzo Librandi_, Apr 26 2011",
				"(Haskell)",
				"a017113 = (+ 4) . (* 8)",
				"a017113_list = [4, 12 ..]  -- _Reinhard Zumkeller_, Jul 13 2013",
				"(PARI) a(n)=8*n+4 \\\\ _Charles R Greathouse IV_, Sep 23 2013"
			],
			"xref": [
				"First differences of A016742 (even squares). Cf. A078370, A078371.",
				"Cf. A081770 (subsequence).",
				"Cf. A019683, A051062."
			],
			"keyword": "nonn,easy,changed",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_",
			"references": 46,
			"revision": 111,
			"time": "2022-01-11T05:50:56-05:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}