{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A099776",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 99776,
			"data": "5,13,25,41,61,85,113,145,181,221,265,313,365,421,481,545,613,685,761,841,925,1013,1105,1201,1301,1405,1513,1625,1741,1861,1985,2113,2245,2381,2521,2665,2813,2965,3121,3281,3445,3613,3785,3961,4141,4325,4513",
			"name": "Length of the hypotenuse of an integer right triangle with the hypotenuse being one more than the longer side. The shorter sides are just consecutive odd numbers 3, 5, 7, ...",
			"comment": [
				"Largest hypotenuse of primitive Pythagorean triangles with inradius n. (For smallest hypotenuse of PPT with inradius n, see A087484.)  Essentially the same as A001844. - _Lekraj Beedassy_, May 08 2006",
				"The complete triple {X(n), Y(n), Z(n)=Y(n)+1}, with X\u003cY\u003cZ, {X(n) = A005408(n); Y(n) = A046092(n), Z(n) = A001844(n)} may be recursively generated through the mapping W(n) -\u003e M*W(n), where W(n) = transpose of vector [X(n) Y(n) Z(n)] and M a 3 X 3 matrix given by [1 -2 2 / 2 -1 2 / 2 -2 3 ]. Such triples correspond to successive number pair Pythagorean generators(p,q=p+1) yielding {X=p+q,Y=2p*q,Z=p^2 + q^2}. - _Lekraj Beedassy_, Jun 04 2006",
				"Sum of two consecutive squares: 1^4=5, 4+9=13, 9+16=25, 16+25=41, ... - _Vladimir Joseph Stephan Orlovsky_, Sep 25 2009",
				"The sequence provides all integers m \u003e 1 such that 2*m - 1 is a square. - _Vincenzo Librandi_, Mar 03 2013"
			],
			"link": [
				"Ray Chandler, \u003ca href=\"/A099776/b099776.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (first 1000 terms from Vincenzo Librandi)",
				"M. Janjic and B. Petkovic, \u003ca href=\"http://arxiv.org/abs/1301.4550\"\u003eA Counting Function\u003c/a\u003e, arXiv 1301.4550 [math.CO], 2013.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1)."
			],
			"formula": [
				"a(n) = ((2*n+1)^2 -1)/2 + 1.",
				"a(n) = a(n-1) + 4*n for n\u003e1, a(1)=5. - _Vincenzo Librandi_, Nov 17 2010",
				"From _Colin Barker_, Nov 03 2012: (Start)",
				"a(n) = 1 + 2*n + 2*n^2.",
				"a(n) = 3*a(n-1) - 3*a(n-2) + a(n-3).",
				"G.f.: x*(5 -2*x +x^2)/(1-x)^3. (End)",
				"All other formulas given in A001844 also apply, with the restriction n\u003e0. - _M. F. Hasler_, Nov 03 2012",
				"E.g.f.: -1 +(1 +4*x +2*x^2)*exp(x). - _G. C. Greubel_, Sep 04 2019"
			],
			"maple": [
				"seq(n^2 +(n+1)^2, n=1..50); # _G. C. Greubel_, Sep 04 2019"
			],
			"mathematica": [
				"Table[n^2 +(n+1)^2, {n,50}]  (* _Vladimir Joseph Stephan Orlovsky_, Sep 25 2009, modified by _G. C. Greubel_, Sep 04 2019 *)",
				"RecurrenceTable[{a[1]==5, a[n]==a[n-1] +4n}, a, {n, 50}] (* _Vincenzo Librandi_, Mar 03 2013 *)",
				"LinearRecurrence[{3,-3,1},{5,13,25},50] (* _Harvey P. Dale_, Jul 16 2018 *)"
			],
			"program": [
				"(C) #include \"stdio.h\"",
				"int main(int argc, char* argv[]){",
				"  unsigned long i; int L = (argc\u003e1) ? atol(argv[1]) : 50;",
				"  for (i=(L\u003e0) ? 1 : (L*=-1); i\u003c=L; i++)",
				"    printf (\"%u, \", (i+1)*i*2+1);",
				"  return 0;",
				"} // optional arg implemented by _M. F. Hasler_, Nov 03 2012",
				"(MAGMA) [n eq 1 select 5 else Self(n-1)+4*n: n in [1..50]]; // _Vincenzo Librandi_, Mar 03 2013",
				"(PARI) a(n)=1+2*n+2*n^2 \\\\ _Charles R Greathouse IV_, Oct 07 2015",
				"(Sage) [n^2 +(n+1)^2 for n in (1..50)] # _G. C. Greubel_, Sep 04 2019",
				"(GAP) List([1..50], n-\u003e n^2 +(n+1)^2); # _G. C. Greubel_, Sep 04 2019"
			],
			"keyword": "easy,nonn",
			"offset": "1,1",
			"author": "Nick Robins (nrobins(AT)hackettfreedman.com), Nov 12 2004",
			"references": 7,
			"revision": 50,
			"time": "2020-01-26T12:43:44-05:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}