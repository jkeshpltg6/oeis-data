{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A234586",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 234586,
			"data": "1,2,1,4,3,6,1,8,3,10,5,12,7,14,5,16,7,18,5,20,7,22,5,24,7,26,9,28,11,30,9,32,11,34,13,36,15,38,13,40,15,42,17,44,19,46,17,48,19,50,17,52,19,54,17,56,19,58,21,60,23,62,21,64,23,66,21,68,23,70,21,72,23,74,25,76,27,78,25,80,27,82,25",
			"name": "Odd-indexed terms are absolute values of differences.",
			"comment": [
				"The precise definition is: Set a(2n)=2n for all n, set a(1)=1, and for n \u003e= 1 choose a(2n+1) so that the subsequence {a(2i+1), i\u003e=0} is the same as the sequence of differences {|a(j+1)-a(j)|, j\u003e=0}."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A234586/b234586.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Eric Angelini, \u003ca href=\"http://www.cetteadressecomportecinquantesignes.com/FirstDiffImpair.htm\"\u003eThe first differences of S ...\u003c/a\u003e",
				"E. Angelini, \u003ca href=\"/A234586/a234586.pdf\"\u003eThe first differences of S ...\u003c/a\u003e [Cached copy, with permission]"
			],
			"formula": [
				"There is a surprising connection with the Thue-Morse sequence A010060. If the k-th run of equal terms in A010060 (k\u003e=0) has length L (L=1 or 2, see A026465), replace it by 2L copies of the pair 4k+1, 4k+3. This produces the odd-indexed terms of the sequence (ignoring the initial 1): 0 1 1 0 1 0 0 1 ... becomes 1 3 1 3 5 7 5 7 5 7 5 7 9 11 9 11 13 ... - _N. J. A. Sloane_, Dec 31 2013"
			],
			"example": [
				"We start by alternating even numbers and \"holes\" like this:",
				"S = . 2 . 4 . 6 . 8 . 10 . 12 . 14 . 16 . 18 . 20 . 22 .....",
				"We fill the first hole with '1' and the second and third holes with x, y:",
				"S = 1 2 x 4 y 6 . 8 . 10 . 12 . 14 . 16 . 18 . 20 . 22 .....",
				"The absolute values of differences are 1, |x-2|, |4-x|, ... which must equal 1, x, y, ..., which forces x=1, y=3. And so on."
			],
			"maple": [
				"with(LinearAlgebra): M:=1000; S:=Array(1..2*M); S[1]:=1; S[3]:=1;",
				"for i from 1 to M do S[2*i]:=2*i; od:",
				"for i from 2 to M-1 do S[2*i+1]:=abs(S[i+2]-S[i+1]); od:",
				"[seq(S[i],i=1..2*M)];"
			],
			"mathematica": [
				"a[1] = a[3] = 1; a[n_?EvenQ] := n; a[n_] := a[n] = Abs[a[(n-1)/2+2]-a[(n-1)/2+1]]; Table[a[n], {n, 1, 100}] (* _Jean-François Alcover_, Jan 13 2015 *)"
			],
			"program": [
				"(Haskell)",
				"import Data.List (transpose)",
				"a234586 n = a234586_list !! (n-1)",
				"a234586_list = concat (transpose [a234587_list, [2, 4 ..]])",
				"a234587_list = 1 : 1 : (drop 2 $",
				"               map abs $ zipWith (-) a234586_list $ tail a234586_list)",
				"-- _Reinhard Zumkeller_, Jul 15 2014"
			],
			"xref": [
				"Cf. A010060, A026465, A234587 (the odd-indexed terms)."
			],
			"keyword": "nonn,nice",
			"offset": "1,2",
			"author": "_Eric Angelini_, Dec 31 2013",
			"ext": [
				"Entered by N. J. A. Sloane on Eric Angelini's behalf and submitted for the 2014 JMM competition with his permission."
			],
			"references": 2,
			"revision": 28,
			"time": "2016-06-10T00:21:00-04:00",
			"created": "2014-01-20T10:23:38-05:00"
		}
	]
}