{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A077236",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 77236,
			"data": "4,11,40,149,556,2075,7744,28901,107860,402539,1502296,5606645,20924284,78090491,291437680,1087660229,4059203236,15149152715,56537407624,211000477781,787464503500,2938857536219,10967965641376",
			"name": "a(n) = 4*a(n-1) - a(n-2) with a(0) = 4 and a(1) = 11.",
			"comment": [
				"a(n)^2 - 3*b(n)^2 = 13, with the companion sequence b(n)= A054491(n).",
				"Bisection (even part) of Chebyshev sequence with Diophantine property.",
				"The odd part is A077235(n) with Diophantine companion A077234(n)."
			],
			"link": [
				"Luigi Cerlienco, Maurice Mignotte, and F. Piras, \u003ca href=\"http://dx.doi.org/10.5169/seals-87887\"\u003eSuites récurrentes linéaires: Propriétés algébriques et arithmétiques\u003c/a\u003e, L'Enseignement Math., 33 (1987), 67-108. See Example 2, page 93.",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-1)."
			],
			"formula": [
				"a(n) = T(n+1,2) + 2*T(n,2), with T(n,x) Chebyshev's polynomials of the first kind, A053120. T(n,2) = A001075(n).",
				"G.f.: (4-5*x)/(1-4*x+x^2).",
				"a(n) = -(1/2)*sqrt(3)*(2-sqrt(3))^n + (1/2)*sqrt(3)*(2+sqrt(3))^n + 2*(2-sqrt(3))^n + 2*(2+sqrt(3))^n, with n \u003e= 0. - _Paolo P. Lava_, Nov 20 2008",
				"From Al Hakanson (hawkuu(AT)gmail.com), Jul 06 2009: (Start)",
				"a(n) = ((4+sqrt(3))*(2+sqrt(3))^n + (4-sqrt(3))*(2-sqrt(3))^n)/2. Offset 0.",
				"a(n) = second binomial transform of 4,3,12,9,36. (End)",
				"a(n) = (A054491(n+1) - A054491(n-1))/2 = sqrt(3*A054491(n-1)*A054491(n+1) + 52), n \u003e= 1. - _Klaus Purath_, Oct 12 2021"
			],
			"example": [
				"11 = a(1) = sqrt(3*A054491(1)^2 + 13) = sqrt(3*6^2 + 13)= sqrt(121) = 11."
			],
			"mathematica": [
				"CoefficientList[Series[(4-5*x)/(1-4*x+x^2), {x,0,20}], x] (* or *) LinearRecurrence[{4,-1}, {4,11}, 30] (* _G. C. Greubel_, Apr 28 2019 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^30)); Vec((4-5*x)/(1-4*x+x^2)) \\\\ _G. C. Greubel_, Apr 28 2019",
				"(PARI) a(n) = polchebyshev(n+1, 1, 2) + 2*polchebyshev(n, 1, 2); \\\\ _Michel Marcus_, Oct 13 2021",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Integers(), 30); Coefficients(R!( (4-5*x)/(1-4*x+x^2) )); // _G. C. Greubel_, Apr 28 2019",
				"(Sage) ((4-5*x)/(1-4*x+x^2)).series(x, 30).coefficients(x, sparse=False) # _G. C. Greubel_, Apr 28 2019",
				"(GAP) a:=[4,11];; for n in [3..30] do a[n]:=4*a[n-1]-a[n-2]; od; a; # _G. C. Greubel_, Apr 28 2019"
			],
			"xref": [
				"Cf. A077238 (even and odd parts), A077235, A053120."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_Wolfdieter Lang_, Nov 08 2002",
			"ext": [
				"Edited by _N. J. A. Sloane_, Sep 07 2018, replacing old definition with simple formula from _Philippe Deléham_, Nov 16 2008"
			],
			"references": 8,
			"revision": 38,
			"time": "2021-12-11T04:40:21-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}