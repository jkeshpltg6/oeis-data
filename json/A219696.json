{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A219696",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 219696,
			"data": "1,2,4,8,10,14,16,20,22,26,40,44,52,106,184,206,244,274,322,526,650,668,790,866,976,1154,1300,1438,1732,1780,1822,2308,2734,3238,7288",
			"name": "Numbers k such that the trajectory of 3k + 1 under the '3x + 1' map reaches k.",
			"comment": [
				"This sequence seems complete; there are no other terms \u003c= 10^9. - _T. D. Noe_, Dec 03 2012",
				"If the 3x+1 step is replaced with (3x+1)/2, the sequence becomes {1, 2, 4, 8, 10, 14, 20, 22, 26, 40, 44, 206, 244, 650, 668, 866, 1154, 1822, 2308, ...}. - _Robert G. Wilson v_, Jan 13 2015"
			],
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CollatzProblem.html\"\u003eCollatz Problem\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Collatz_conjecture\"\u003eCollatz conjecture\u003c/a\u003e",
				"\u003ca href=\"/index/3#3x1\"\u003eIndex entries for sequences related to 3x+1 (or Collatz) problem\u003c/a\u003e"
			],
			"example": [
				"For k = 4, the Collatz trajectory of 3k + 1 is (13, 40, 20, 10, 5, 16, 8, 4, 2, 1), which includes 4; thus, 4 is in the sequence.",
				"For k = 5, the Collatz trajectory of 3k + 1 is (16, 8, 4, 2, 1), which does not include 5; thus, 5 is not in the sequence."
			],
			"mathematica": [
				"Collatz[n_] := NestWhileList[If[EvenQ[#], #/2, 3 # + 1] \u0026, n, # \u003e 1 \u0026]; Select[Range[10000], MemberQ[Collatz[3 # + 1], #] \u0026] (* _T. D. Noe_, Dec 03 2012 *)"
			],
			"program": [
				"(Haskell)",
				"a219696 n = a219696_list !! (n-1)",
				"a219696_list = filter (\\x -\u003e collatz'' x == x) [1..] where",
				"   collatz'' x = until (`elem` [1, x]) a006370 (3 * x + 1)",
				"-- _Reinhard Zumkeller_, Aug 11 2014",
				"(Python)",
				"def ok(n):",
				"    if n==1: return [1]",
				"    N=3*n + 1",
				"    l=[N, ]",
				"    while True:",
				"        if N%2==1: N = 3*N + 1",
				"        else: N/=2",
				"        l+=[N, ]",
				"        if N\u003c2: break",
				"    if n in l: return 1",
				"    return 0 # _Indranil Ghosh_, Apr 22 2017",
				"(PARI) a006370(n) = if(n%2==0, n/2, 3*n+1)",
				"is(n) = my(x=3*n+1); while(1, x=a006370(x); if(x==n, return(1), if(x==1, return(0)))) \\\\ _Felix Fröhlich_, Jun 10 2021"
			],
			"xref": [
				"Cf. A014682, A070991, A006370, A070165."
			],
			"keyword": "nonn,nice,more",
			"offset": "1,2",
			"author": "_Robert C. Lyons_, Nov 25 2012",
			"ext": [
				"Initial 1 from Clark R. Lyons, Dec 02 2012"
			],
			"references": 2,
			"revision": 30,
			"time": "2021-06-10T22:31:36-04:00",
			"created": "2012-12-03T15:31:14-05:00"
		}
	]
}