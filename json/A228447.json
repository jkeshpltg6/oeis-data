{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A228447",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 228447,
			"data": "1,-3,7,-15,30,-57,104,-183,313,-522,852,-1365,2150,-3336,5106,-7719,11538,-17067,25004,-36306,52280,-74700,105960,-149277,208951,-290706,402127,-553224,757158,-1031166,1397744,-1886151,2534316,-3391254,4520112,-6002007",
			"name": "Expansion of q * (psi(q^3) * psi(q^6)) / (psi(q) * phi(q)) in powers of q where phi(), psi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A228447/b228447.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q * (psi(q^3)^3 / psi(q)) / (phi(q) * phi(q^3)) in powers of q where phi(), psi() are Ramanujan theta functions.",
				"Expansion of eta(q)^3 * eta(x^4)^2 * eta(x^6) * eta(x^12)^2 / (eta(x^2)^7 * eta(x^3)) in powers of q.",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2)) where f(u, v) = u^2 * (1 - 3*v) - v * (1 - 4*v) * (1 - 3*u)^2.",
				"a(n) = -(-1)^n * A187100(n). a(2*n) = -3 * A128638(n).",
				"Convolution inverse is A187145. Convolution with A033716 is A093829."
			],
			"example": [
				"G.f. = q - 3*q^2 + 7*q^3 - 15*q^4 + 30*q^5 - 57*q^6 + 104*q^7 - 183*q^8 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (1/4) EllipticTheta[ 2, 0, q^(3/2)]^3 / (EllipticTheta[ 2, 0, q^(1/2)] EllipticTheta[ 3, 0, q] EllipticTheta[ 3, 0, q^3]), {q, 0, n}]"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c1, 0, n--; A = x * O(x^n); polcoeff( eta(x + A)^3 * eta(x^4 + A)^2 * eta(x^6 + A) * eta(x^12 + A)^2 / (eta(x^2 + A)^7 * eta(x^3 + A)), n))}"
			],
			"xref": [
				"Cf. A033716, A093829, A128638, A187100, A187145."
			],
			"keyword": "sign",
			"offset": "1,2",
			"author": "_Michael Somos_, Oct 26 2013",
			"references": 5,
			"revision": 13,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2013-10-27T04:21:06-04:00"
		}
	]
}