{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A242378",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 242378,
			"data": "0,1,0,2,1,0,3,3,1,0,4,5,5,1,0,5,9,7,7,1,0,6,7,25,11,11,1,0,7,15,11,49,13,13,1,0,8,11,35,13,121,17,17,1,0,9,27,13,77,17,169,19,19,1,0,10,25,125,17,143,19,289,23,23,1,0,11,21,49,343,19,221,23,361,29,29,1,0",
			"name": "Square array read by antidiagonals: to obtain A(i,j), replace each prime factor prime(k) in prime factorization of j with prime(k+i).",
			"comment": [
				"Each row k is a multiplicative function, being in essence \"the k-th power\" of A003961, i.e., A(row,col) = A003961^row (col). Zeroth power gives an identity function, A001477, which occurs as the row zero.",
				"The terms in the same column have the same prime signature.",
				"The array is read by antidiagonals: A(0,0), A(0,1), A(1,0), A(0,2), A(1,1), A(2,0), ... ."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A242378/b242378.txt\"\u003eTable of n, a(n) for n = 0..10439; Antidiagonals n = 0..143, flattened\u003c/a\u003e"
			],
			"formula": [
				"A(0,n) = n, A(row,0) = 0, A(row\u003e0,n\u003e0) = A003961(A(row-1,n))."
			],
			"example": [
				"The top-left corner of the array:",
				"  0,   1,   2,   3,   4,   5,   6,   7,   8, ...",
				"  0,   1,   3,   5,   9,   7,  15,  11,  27, ...",
				"  0,   1,   5,   7,  25,  11,  35,  13, 125, ...",
				"  0,   1,   7,  11,  49,  13,  77,  17, 343, ...",
				"  0,   1,  11,  13, 121,  17, 143,  19,1331, ...",
				"  0,   1,  13,  17, 169,  19, 221,  23,2197, ...",
				"...",
				"A(2,6) = A003961(A003961(6)) = p_{1+2} * p_{2+2} = p_3 * p_4 = 5 * 7 = 35, because 6 = 2*3 = p_1 * p_2."
			],
			"program": [
				"(Scheme, with function factor from with Aubrey Jaffer's SLIB Scheme library)",
				"(require 'factor)",
				"(define (ifactor n) (cond ((\u003c n 2) (list)) (else (sort (factor n) \u003c))))",
				"(define (A242378 n) (A242378bi (A002262 n) (A025581 n)))",
				"(define (A242378bi row col) (if (zero? col) col (apply * (map A000040 (map (lambda (k) (+ k row)) (map A049084 (ifactor col)))))))"
			],
			"xref": [
				"Taking every second column from column 2 onward gives array A246278 which is a permutation of natural numbers larger than 1.",
				"Transpose: A242379.",
				"Row 0: A001477, Row 1: A003961 (from 1 onward), Row 2: A045966 (from 5 onward), Row 3: A045968 (from 7 onward), Row 4: A045970 (from 11 onward).",
				"Column 2: A000040 (primes), Column 3: A065091 (odd primes), Column 4: A001248 (squares of primes), Column 6: A006094 (products of two successive primes), Column 8: A030078 (cubes of primes).",
				"Permutations whose formulas refer to this array: A122111, A241909, A242415, A242419, A246676, A246678, A246684."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Antti Karttunen_, May 12 2014",
			"references": 17,
			"revision": 30,
			"time": "2017-03-30T22:16:04-04:00",
			"created": "2014-05-15T10:27:58-04:00"
		}
	]
}