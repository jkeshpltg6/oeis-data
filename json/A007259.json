{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007259",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7259,
			"id": "M4504",
			"data": "1,-8,28,-64,134,-288,568,-1024,1809,-3152,5316,-8704,13990,-22208,34696,-53248,80724,-121240,180068,-264448,384940,-556064,796760,-1132544,1598789,-2243056,3127360,-4333568,5971922,-8188096,11170160,-15163392,20491033,-27572936",
			"name": "Expansion of Product_{m\u003e=1} (1 + q^m)^(-8).",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"McKay-Thompson series of class 6F for the Monster group."
			],
			"reference": [
				"T. J. I'a. Bromwich, Introduction to the Theory of Infinite Series, Macmillan, 2nd. ed. 1949, p. 118, Problem 24.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A007259/b007259.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"J. H. Conway and S. P. Norton, \u003ca href=\"http://blms.oxfordjournals.org/content/11/3/308.extract\"\u003eMonstrous Moonshine\u003c/a\u003e, Bull. Lond. Math. Soc. 11 (1979) 308-339.",
				"D. Ford, J. McKay and S. P. Norton, \u003ca href=\"http://dx.doi.org/10.1080/00927879408825127\"\u003eMore on replicable functions\u003c/a\u003e, Commun. Algebra 22, No. 13, 5175-5193 (1994).",
				"J. McKay and H. Strauss, \u003ca href=\"http://dx.doi.org/10.1080/00927879008823911\"\u003eThe q-series of monstrous moonshine and the decomposition of the head characters\u003c/a\u003e, Comm. Algebra 18 (1990), no. 1, 253-278.",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"\u003ca href=\"/index/Mat#McKay_Thompson\"\u003eIndex entries for McKay-Thompson series for Monster simple group\u003c/a\u003e"
			],
			"formula": [
				"Expansion of chi(-q)^8 in powers of q where chi() is a Ramanujan theta function. - _Michael Somos_, Aug 18 2007",
				"Expansion of q^(-1/3) * (eta(q) / eta(q^2))^8 in powers of q. - _Michael Somos_, Aug 18 2007",
				"Euler transform of period 2 sequence [ -8, 0, ...]. - _Michael Somos_, Aug 18 2007",
				"Given g.f. A(x), then B(x) = A(x^3) / x satisfies 0 = f(B(x), B(x^2)) where f(u, v) = v^2 - u^2 * v - 16 * u. - _Michael Somos_, Aug 18 2007",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (2 t)) = 16 / f(t) where q = exp(2 Pi i t). - _Michael Somos_, Aug 18 2007",
				"G.f.: Product_{k\u003e0} (1 + x^k)^(-8).",
				"a(2*n) = A014705(n). a(2*n + 1) = -8 * A022573(n). a(n) = A007263(3*n - 1).",
				"a(n) ~ (-1)^n * exp(2*Pi*sqrt(n/3)) / (2 * 3^(1/4) * n^(3/4)). - _Vaclav Kotesovec_, Aug 27 2015",
				"a(0) = 1, a(n) = -(8/n)*Sum_{k=1..n} A000593(k)*a(n-k) for n \u003e 0. - _Seiichi Manyama_, Apr 05 2017",
				"G.f.: exp(-8*Sum_{k\u003e=1} (-1)^(k+1)*x^k/(k*(1 - x^k))). - _Ilya Gutkovskiy_, Feb 06 2018"
			],
			"example": [
				"1 - 8*x + 28*x^2 - 64*x^3 + 134*x^4 - 288*x^5 + 568*x^6 - 1024*x^7 + ...",
				"T6F = 1/q - 8q^2 + 28q^5 - 64q^8 + 134q^11 - 288q^14 + 568q^17 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ q, q^2]^8, {q, 0, n}] (* _Michael Somos_, Jul 11 2011 *)",
				"a[ n_] := SeriesCoefficient[ Product[ 1 - q^k, {k, 1, n, 2}]^8, {q, 0, n}] (* _Michael Somos_, Jul 11 2011 *)",
				"a[ n_] := SeriesCoefficient[ Product[ 1 + q^k, {k, n}]^-8, {q, 0, n}] (* _Michael Somos_, Jul 11 2011 *)"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( (eta(x + A) / eta(x^2 + A))^8, n))}"
			],
			"xref": [
				"Cf. A007263, A014705, A022573.",
				"Column k=8 of A286352."
			],
			"keyword": "sign,easy,nice",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 7,
			"revision": 50,
			"time": "2021-03-12T22:24:41-05:00",
			"created": "1994-04-28T03:00:00-04:00"
		}
	]
}