{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A207537",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 207537,
			"data": "1,2,1,4,3,8,8,1,16,20,5,32,48,18,1,64,112,56,7,128,256,160,32,1,256,576,432,120,9,512,1280,1120,400,50,1,1024,2816,2816,1232,220,11,2048,6144,6912,3584,840,72,1,4096,13312,16640,9984,2912,364,13",
			"name": "Triangle of coefficients of polynomials u(n,x) jointly generated with A207538; see Formula section.",
			"comment": [
				"Another version in A201701. - _Philippe Deléham_, Mar 03 2012",
				"Subtriangle of the triangle given by (1, 1, 0, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (0, 1, -1, 0, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Mar 03 2012",
				"Columns: A011782, A001792, A001793, A001794, A006974, A006975, A006976. - _Philippe Deléham_, Mar 03 2012",
				"Diagonal sums: A052980. - _Philippe Deléham_, Mar 03 2012"
			],
			"formula": [
				"u(n,x) = u(n-1,x) + (x+1)*v(n-1,x), v(n,x) = u(n-1,x) + v(n-1,x), where u(1,x)=1, v(1,x)=1.  Also, A207537 = |A028297|.",
				"T(n,k) = 2*T(n-1,k) + T(n-2,k-1). - _Philippe Deléham_, Mar 03 2012",
				"G.f.: -(1+x*y)*x*y/(-1+2*x+x^2*y). - _R. J. Mathar_, Aug 11 2015",
				"T(n, k) = [x^k] hypergeom([-n/2, -n/2 + 1/2], [1/2], x + 1) provided offset is set to 0 and 1 prepended. - _Peter Luschny_, Feb 03 2021"
			],
			"example": [
				"First seven rows:",
				"   1;",
				"   2,   1;",
				"   4,   3;",
				"   8,   8,  1;",
				"  16,  20,  5,",
				"  32,  48, 18, 1;",
				"  64, 112, 56, 7;",
				"From _Philippe Deléham_, Mar 03 2012: (Start)",
				"Triangle A201701 begins:",
				"   1;",
				"   1,   0;",
				"   2,   1,  0;",
				"   4,   3,  0, 0;",
				"   8,   8,  1, 0, 0;",
				"  16,  20,  5, 0, 0, 0;",
				"  32,  48, 18, 1, 0, 0, 0;",
				"  64, 112, 56, 7, 0, 0, 0, 0;",
				"  ... (End)"
			],
			"mathematica": [
				"u[1, x_] := 1; v[1, x_] := 1; z = 16;",
				"u[n_, x_] := u[n - 1, x] + (x + 1)*v[n - 1, x]",
				"v[n_, x_] := u[n - 1, x] + v[n - 1, x]",
				"Table[Factor[u[n, x]], {n, 1, z}]",
				"Table[Factor[v[n, x]], {n, 1, z}]",
				"cu = Table[CoefficientList[u[n, x], x], {n, 1, z}];",
				"TableForm[cu]",
				"Flatten[%]  (* A207537, |A028297| *)",
				"Table[Expand[v[n, x]], {n, 1, z}]",
				"cv = Table[CoefficientList[v[n, x], x], {n, 1, z}];",
				"TableForm[cv]",
				"Flatten[%]  (* A207538, |A133156| *)",
				"(* Prepending 1 and with offset 0: *)",
				"Tpoly[n_] := HypergeometricPFQ[{-n/2, -n/2 + 1/2}, {1/2}, x + 1];",
				"Table[CoefficientList[Tpoly[n], x], {n, 0, 12}] // Flatten (* _Peter Luschny_, Feb 03 2021 *)"
			],
			"xref": [
				"Cf. A028297, A207538, A133156."
			],
			"keyword": "nonn,tabf",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Feb 18 2012",
			"references": 4,
			"revision": 24,
			"time": "2021-02-03T17:45:43-05:00",
			"created": "2012-02-18T20:47:56-05:00"
		}
	]
}