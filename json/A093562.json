{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A093562",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 93562,
			"data": "1,5,1,5,6,1,5,11,7,1,5,16,18,8,1,5,21,34,26,9,1,5,26,55,60,35,10,1,5,31,81,115,95,45,11,1,5,36,112,196,210,140,56,12,1,5,41,148,308,406,350,196,68,13,1,5,46,189,456,714,756,546,264,81,14,1,5,51,235,645,1170",
			"name": "(5,1) Pascal triangle.",
			"comment": [
				"This is the fifth member, d=5, in the family of triangles of figurate numbers, called (d,1) Pascal triangles: A007318 (Pascal), A029653, A093560-1, for d=1..4.",
				"This is an example of a Riordan triangle (see A093560 for a comment and A053121 for a comment and the 1991 Shapiro et al. reference on the Riordan group). Therefore the o.g.f. for the row polynomials p(n,x):=Sum_{m=0..n} a(n,m)*x^m is G(z,x)=(1+4*z)/(1-(1+x)*z).",
				"The SW-NE diagonals give A022095(n-1) = Sum_{k=0..ceiling((n-1)/2)} a(n-1-k,k), n \u003e= 1, with n=0 value 4. Observation by _Paul Barry_, Apr 29 2004. Proof via recursion relations and comparison of inputs.",
				"The array F(5;n,m) gives in the columns m \u003e= 1 the figurate numbers based on A016861, including the heptagonal numbers A000566 (see the W. Lang link).",
				"For a closed-form formula for generalized Pascal's triangle see A228576. - _Boris Putievskiy_, Sep 09 2013",
				"The n-th row polynomial is (4 + x)*(1 + x)^(n-1) for n \u003e= 1. More generally, the n-th row polynomial of the Riordan array ( (1-a*x)/(1-b*x), x/(1-b*x) ) is (b - a + x)*(b + x)^(n-1) for n \u003e= 1. - _Peter Bala_, Mar 02 2018"
			],
			"reference": [
				"Kurt Hawlitschek, Johann Faulhaber 1580-1635, Veroeffentlichung der Stadtbibliothek Ulm, Band 18, Ulm, Germany, 1995, Ch. 2.1.4. Figurierte Zahlen.",
				"Ivo Schneider, Johannes Faulhaber 1580-1635, Birkhäuser, Basel, Boston, Berlin, 1993, ch.5, pp. 109-122."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A093562/b093562.txt\"\u003eRows n = 0..125 of triangle, flattened\u003c/a\u003e",
				"P. Bala, \u003ca href=\"/A081577/a081577.pdf\"\u003eA note on the diagonals of a proper Riordan Array\u003c/a\u003e",
				"W. Lang, \u003ca href=\"/A093562/a093562.txt\"\u003eFirst 10 rows and array of figurate numbers \u003c/a\u003e."
			],
			"formula": [
				"a(n, m) = F(5;n-m, m) for 0\u003c= m \u003c= n, otherwise 0, with F(5;0, 0)=1, F(5;n, 0)=5 if n\u003e=1 and F(5;n, m):=(5*n+m)*binomial(n+m-1, m-1)/m if m\u003e=1.",
				"G.f. column m (without leading zeros): (1+4*x)/(1-x)^(m+1), m\u003e=0.",
				"Recursion: a(n, m)=0 if m\u003en, a(0, 0)= 1; a(n, 0)=5 if n\u003e=1; a(n, m)= a(n-1, m) + a(n-1, m-1).",
				"T(n, k) = C(n, k) + 4*C(n-1, k). - _Philippe Deléham_, Aug 28 2005",
				"exp(x) * e.g.f. for row n = e.g.f. for diagonal n. For example, for n = 3 we have exp(x)*(5 + 11*x + 7*x^2/2! + x^3/3!) = 5 + 16*x + 34*x^2/2! + 60*x^3/3! + 95*x^4/4! + .... The same property holds more generally for Riordan arrays of the form ( f(x), x/(1 - x) ). - _Peter Bala_, Dec 22 2014"
			],
			"example": [
				"Triangle begins",
				"  [1];",
				"  [5,  1];",
				"  [5,  6,  1];",
				"  [5, 11,  7,  1];",
				"  ..."
			],
			"program": [
				"(Haskell)",
				"a093562 n k = a093562_tabl !! n !! k",
				"a093562_row n = a093562_tabl !! n",
				"a093562_tabl = [1] : iterate",
				"               (\\row -\u003e zipWith (+) ([0] ++ row) (row ++ [0])) [5, 1]",
				"-- _Reinhard Zumkeller_, Aug 31 2014"
			],
			"xref": [
				"Cf. Row sums: A007283(n-1), n\u003e=1, 1 for n=0. A082505(n+1), alternating row sums are 1 for n=0, 4 for n=2 and 0 else.",
				"Column sequences give for m=1..9: A016861, A000566 (heptagonal), A002413, A002418, A027800, A051946, A050484, A052255, A055844.",
				"A007318, A093563 (d=6), A228196, A228576."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Apr 22 2004",
			"references": 15,
			"revision": 39,
			"time": "2019-08-28T16:09:30-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}