{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A339929",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 339929,
			"data": "0,0,1,1,1,2,1,2,2,2,2,3,1,3,2,3,3,2,2,4,2,4,2,3,4,3,4,3,3,3,5,2,5,2,4,3,4,5,4,5,4,4,5,4,5,3,4,4,6,4,6,4,5,5,4,6,3,5,3,7,3,7,4,4,5,5,6,4,7,3,8,3,8,3,5,7,4,8,2,4,5,5,7,6,5,4,8,5,8,4,9,3,6,7,5",
			"name": "a(n+1) = a(n-1-a(n)^2) + 1, starting with a(1) = a(2) = 0.",
			"comment": [
				"To obtain the next term, square the current term and add 1, then count back this number and add 1.",
				"The sequence cannot repeat. Proof: Assume a finite period. Label an arbitrary term in the period x. Because of the back-referencing definition it follows that x-1 has to be in the period, and by the same argument so does x-2 and x-3, x-4, ... until 0. But it is not possible to obtain new 0s since each new term is larger than one already existing term.",
				"Every positive integer appears in the sequence.",
				"First occurrence of n: 1, 3, 6, 12, 20, 31, 49, 60, 71, 91, 129, 163, 214, 265, 303, 354, 516, 594, 792, 915, ...",
				"The sequence appears to grow with the cube root of n, which is expected since f(x) = (3*x)^(1/3) satisfies the definition for large x, i.e., lim_{x-\u003eoo} f(x+1)-(f(x-1-f(x)^2)+1) = 0.",
				"The width of the distribution of terms within a range (n^2,n^2+n) appears to be constant for large n and can be defined as: lim_{n-\u003eoo} ( 1/n*Sum_{k=n..2n} ( Max_{i=k^2..k^2+k} a(i) - Min_{i=k^2..k^2+k} a(i) ) ) and evaluates to 12.98... (for n^2 = 5*10^8).",
				"Each term is determined by referencing an earlier term. Following the chain of these references we can uniquely trace back every term to one of the two initial zeros, e.g., a(60) -\u003e a(49) -\u003e a(31) -\u003e a(20) -\u003e a(14) -\u003e a(11) -\u003e a(5) -\u003e a(2). These progressions form two trees, with the zeros being at their roots (for visualization see the Links section). The average branching factor B (number of links divided by the number of non-leaf nodes) is numerically evaluated to B = 1.51803... (for n = 10^8). Considering the asymptotic behavior of the sequence a(n) ~ (3*n)^(1/3), we can conclude that the number of links within a range (n,m) is asymptotically equal to m-n and therefore B is the inverse of the proportion of non-leaf terms (B = 1.518... then implies that roughly 34% of all terms never get referenced).",
				"An extended definition can be considered that only requires one initial value: a(0) = -1, and the next terms are obtained via a(n+1) = a(n-1-a(n)*|a(n)|)+1."
			],
			"link": [
				"Rok Cestnik, \u003ca href=\"/A339929/b339929.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Rok Cestnik, \u003ca href=\"/A339929/a339929.pdf\"\u003eTerm-referencing tree for 1000 terms\u003c/a\u003e",
				"Rok Cestnik, \u003ca href=\"/A339929/a339929.py.txt\"\u003eProgram for plotting the term-referencing tree\u003c/a\u003e"
			],
			"formula": [
				"a(n) ~ (3*n)^(1/3) (conjectured)."
			],
			"example": [
				"a(3) = a(2-1-a(2)^2)+1 = a(1)+1 = 1.",
				"a(4) = a(3-1-a(3)^2)+1 = a(1)+1 = 1.",
				"a(5) = a(4-1-a(4)^2)+1 = a(2)+1 = 1.",
				"a(6) = a(5-1-a(5)^2)+1 = a(3)+1 = 2."
			],
			"program": [
				"(Python)",
				"a = [0,0]",
				"for n in range(1,1000):",
				"    a.append(a[n-1-a[n]**2]+1)",
				"(C)",
				"#include\u003cstdio.h\u003e",
				"#include\u003cstdlib.h\u003e",
				"int main(void){",
				"    int N = 1000;",
				"    int *a = (int*)malloc(N*sizeof(int));",
				"    a[0] = 0;",
				"    a[1] = 0;",
				"    for(int n = 1; n \u003c N-1; ++n){",
				"        a[n+1] = a[n-1-a[n]*a[n]]+1;",
				"    }",
				"    free(a);",
				"    return 0;",
				"}"
			],
			"xref": [
				"Analogous sequences: A339930, A339931, A339932.",
				"Cf. A330772, A005206, A002516, A062039."
			],
			"keyword": "nonn",
			"offset": "1,6",
			"author": "_Rok Cestnik_, Dec 23 2020",
			"references": 6,
			"revision": 23,
			"time": "2020-12-31T07:32:38-05:00",
			"created": "2020-12-27T19:45:09-05:00"
		}
	]
}