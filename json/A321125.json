{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A321125",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 321125,
			"data": "1,1,1,1,2,1,1,1,1,1,1,1,3,1,1,1,1,2,2,1,1,1,1,2,1,2,1,1,1,1,2,1,1,2,1,1,1,1,2,1,1,1,2,1,1,1,1,2,1,1,1,1,2,1,1,1,1,2,1,1,1,1,1,2,1,1,1,1,2,1,1,1,1,1,1,2,1,1,1,1,2,1,1,1,1,1,1,1,2,1,1",
			"name": "T(n,k) = b(n+k) - (2*b(n)*b(k) + 1)*b(n*k) + b(n) + b(k) + 1, where b(n) = A154272(n+1), square array read by antidiagonals (n \u003e= 0, k \u003e= 0).",
			"comment": [
				"Let \u003cK\u003e(A,B,d) denote the three-variable bracket polynomial for the two-bridge knot with Conway's notation C(n,k). Then T(n,k) is the leading coefficient of the reduced polynomial x*\u003cK\u003e(1,1,x). In Kauffman's language, T(n,k) is the number of  states of the two-bridge knot C(n,k) corresponding to the maximum number of Jordan curves."
			],
			"reference": [
				"Louis H. Kauffman, Formal Knot Theory, Princeton University Press, 1983."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A321125/b321125.txt\"\u003eTable of n, a(n) for n = 0..11475\u003c/a\u003e (rows 0 \u003c= n \u003c= 150, flattened).",
				"Louis H. Kauffman, \u003ca href=\"https://doi.org/10.1016/0040-9383(87)90009-7\"\u003eState models and the Jones polynomial\u003c/a\u003e, Topology Vol. 26 (1987), 395-407.",
				"Kelsey Lafferty, \u003ca href=\"https://scholar.rose-hulman.edu/rhumj/vol14/iss2/7/\"\u003eThe three-variable bracket polynomial for reduced, alternating links\u003c/a\u003e, Rose-Hulman Undergraduate Mathematics Journal Vol. 14 (2013), 98-113.",
				"Matthew Overduin, \u003ca href=\"https://www.math.csusb.edu/reu/OverduinPaper.pdf\"\u003eThe three-variable bracket polynomial for two-bridge knots\u003c/a\u003e, California State University REU, 2013.",
				"Franck Maminirina Ramaharo, \u003ca href=\"/A321125/a321125.pdf\"\u003eIllustration of T(2,2)\u003c/a\u003e",
				"Franck Maminirina Ramaharo, \u003ca href=\"/A321125/a321125.txt\"\u003eNote on this sequence and related ones\u003c/a\u003e",
				"Franck Ramaharo, \u003ca href=\"https://arxiv.org/abs/1902.08989\"\u003eA generating polynomial for the two-bridge knot with Conway's notation C(n,r)\u003c/a\u003e, arXiv:1902.08989 [math.CO], 2019.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/BracketPolynomial.html\"\u003eBracket Polynomial\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/2-bridge_knot\"\u003e2-bridge knot\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Bracket_polynomial\"\u003eBracket polynomial\u003c/a\u003e"
			],
			"formula": [
				"T(n,0) = T(0,n) = 1, and T(n,k) = b(n+k) - b(n)*b(k) - b(n*k) + c(n)*c(k) for n \u003e= 1, k \u003e= 1, where b(n) = A154272(n+1) and c(n) = A294619(n).",
				"T(n,1) = A300453(n+1,A321126(n,1)).",
				"T(n,2) = A300454(n,A321126(n,2)).",
				"T(n,n) = A321127(n,A004280(n+1)).",
				"G.f.: (1 + (x - x^2)*y - (x - 3*x^2 + x^3)*y^2 - x^2*y^3)/((1 - x)*(1 - y)).",
				"E.g.f.: ((x^2 + 2*exp(x))*exp(y) - x^2 + (2*x - x^2)*y - (1 + x - exp(x))*y^2)/2."
			],
			"example": [
				"Square array begins:",
				"  1, 1, 1, 1, 1, 1, ...",
				"  1, 2, 1, 1, 1, 1, ...",
				"  1, 1, 3, 2, 2, 2, ...",
				"  1, 1, 2, 1, 1, 1, ...",
				"  1, 1, 2, 1, 1, 1, ...",
				"  1, 1, 2, 1, 1, 1, ...",
				"  ..."
			],
			"mathematica": [
				"b[n_] = If[n == 0 || n == 2, 1, 0];",
				"T[n_, k_] = b[n + k] - (2*b[n]*b[k] + 1)*b[n*k] + b[n] + b[k] + 1;",
				"Table[T[k, n - k], {n, 0, 12}, {k, 0, n}] // Flatten"
			],
			"program": [
				"(Maxima) b(n) := if n = 0 or n = 2 then 1 else 0$ /* A154272(n+1) */",
				"T(n, k) := b(n + k) - (2*b(n)*b(k) + 1)*b(n*k) + b(n) + b(k) + 1$",
				"create_list(T(k, n - k), n, 0, 12, k, 0, n);"
			],
			"xref": [
				"Cf. A300453, A300454, A316989, A321126, A321127."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,5",
			"author": "_Franck Maminirina Ramaharo_, Nov 24 2018",
			"references": 2,
			"revision": 21,
			"time": "2019-04-10T21:55:02-04:00",
			"created": "2018-12-20T23:47:23-05:00"
		}
	]
}