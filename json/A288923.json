{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A288923",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 288923,
			"data": "1,64,2,32,3,48,4,16,6,24,8,12,18,20,27,28,30,36,9,40,10,54,14,56,15,60,21,72,5,80,7,96,11,108,13,112,17,120,19,128,22,81,25,84,26,88,33,90,34,100,35,104,38,126,39,132,42,44,45,50,52,63,66,68,70",
			"name": "Lexicographically earliest sequence of distinct positive terms such that the product of two consecutive terms has at least 6 prime factors (counted with multiplicity).",
			"comment": [
				"The number of prime factors counted with multiplicity is given by A001222.",
				"This sequence is a permutation of the natural numbers, with inverse A288924.",
				"Conjecturally, a(n) ~ n.",
				"For a function g over the natural numbers and a constant K, let f(g,K) be the lexicographically earliest sequence of distinct positive terms such that, for any n \u003e 0, g( f(g,K)(n) * f(g,K)(n+1) ) \u003e= K. In particular we have:",
				"- f(bigomega,  6) = a (this sequence), where bigomega = A001222,",
				"- f(tau,      34) = A288921,           where tau = A000005,",
				"- f(omega,     5) = A285487,           where omega = A001221,",
				"- f(omega,     6) = A285655,           where omega = A001221.",
				"Some of these sequences have similar graphical features."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A288923/b288923.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A288923/a288923.gp.txt\"\u003ePARI program for A288923\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A288923/a288923.png\"\u003eColored scatterplot of a(n) for n = 1..20000\u003c/a\u003e (where the color is function of A001222(a(n)))",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"example": [
				"The first terms, alongside a(n) * a(n+1) and its number of prime divisors counted with multiplicity, are:",
				"   n   a(n)   a(n)*a(n+1)   Bigomega",
				"  --   ----   -----------   --------",
				"   1     1         64           6",
				"   2    64        128           7",
				"   3     2         64           6",
				"   4    32         96           6",
				"   5     3        144           6",
				"   6    48        192           7",
				"   7     4         64           6",
				"   8    16         96           6",
				"   9     6        144           6",
				"  10    24        192           7",
				"  11     8         96           6",
				"  12    12        216           6",
				"  13    18        360           6",
				"  14    20        540           6",
				"  15    27        756           6",
				"  16    28        840           6",
				"  17    30       1080           7",
				"  18    36        324           6",
				"  19     9        360           6",
				"  20    40        400           6"
			],
			"xref": [
				"Cf. A001221, A001222, A285487, A285655, A288921, A288924 (inverse)."
			],
			"keyword": "nonn,look",
			"offset": "1,2",
			"author": "_Rémy Sigrist_, Jun 19 2017",
			"references": 3,
			"revision": 14,
			"time": "2018-01-21T09:38:33-05:00",
			"created": "2017-06-20T23:53:25-04:00"
		}
	]
}