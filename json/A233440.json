{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A233440",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 233440,
			"data": "0,1,0,2,0,0,3,0,0,3,4,0,0,16,4,5,0,0,50,25,40,6,0,0,120,90,288,216,7,0,0,245,245,1176,1764,1603,8,0,0,448,560,3584,8064,14656,13000,9,0,0,756,1134,9072,27216,74196,131625,118872,10,0,0,1200,2100,20160,75600,274800,731250,1320800,1202880",
			"name": "Triangle read by rows: T(n, k) = n*binomial(n, k)*A000757(k), 0 \u003c= k \u003c= n.",
			"comment": [
				"For n \u003e= 0, 0 \u003c= k \u003c= n, T(n, k) is the number of permutations of n symbols that k-commute with an n-cycle (we say that two permutations f and g k-commute if H(fg, gf) = k, where H(, ) denotes the Hamming distance between permutations).",
				"Row sums give A000142."
			],
			"link": [
				"Luis Manuel Rivera Martínez, \u003ca href=\"/A233440/b233440.txt\"\u003eRows n = 0..30 of triangle, flattened\u003c/a\u003e",
				"R. Moreno and L. M. Rivera, \u003ca href=\"http://arxiv.org/abs/1306.5708\"\u003eBlocks in cycles and k-commuting permutations\u003c/a\u003e, arXiv:1306.5708 [math.CO], 2013-2014.",
				"Luis Manuel Rivera, \u003ca href=\"http://arxiv.org/abs/1406.3081\"\u003eInteger sequences and k-commuting permutations\u003c/a\u003e, arXiv preprint arXiv:1406.3081 [math.CO], 2014-2015."
			],
			"formula": [
				"T(n,k) = n*C(n,k)*A000757(k), 0 \u003c= k \u003c= n.",
				"Bivariate e.g.f.: G(z, u) = z*exp(z*(1-u))*(u/(1-z*u)+(1-log(1-z*u))*(1-u)).",
				"T(n, 0)  = A001477(n), n\u003e=0;",
				"T(n, 1)  = A000004(n), n\u003e=1;",
				"T(n, 2)  = A000004(n), n\u003e=2;",
				"T(n, 3)  = A004320(n-2), n\u003e=3;",
				"T(n, 4)  = A027764(n-1), n\u003e=4;",
				"T(n, 5)  = A027765(n-1)*A000757(5), n\u003e=5;",
				"T(n, 6)  = A027766(n-1)*A000757(6), n\u003e=6;",
				"T(n, 7)  = A027767(n-1)*A000757(7), n\u003e=7;",
				"T(n, 8)  = A027768(n-1)*A000757(8), n\u003e=8;",
				"T(n, 9)  = A027769(n-1)*A000757(9), n\u003e=9;",
				"T(n, 10) = A027770(n-1)*A000757(10), n\u003e=10;",
				"T(n, 11) = A027771(n-1)*A000757(11), n\u003e=11;",
				"T(n, 12) = A027772(n-1)*A000757(12), n\u003e=12;",
				"T(n, 13) = A027773(n-1)*A000757(13), n\u003e=13;",
				"T(n, 14) = A027774(n-1)*A000757(14), n\u003e=14;",
				"T(n, 15) = A027775(n-1)*A000757(15), n\u003e=15;",
				"T(n, 16) = A027776(n-1)*A000757(16), n\u003e=16. - _Luis Manuel Rivera Martínez_, Feb 08 2014",
				"T(n, 0)+T(n, 3) = n*A050407(n+1), for n\u003e=0. - _Luis Manuel Rivera Martínez_, Mar 06 2014"
			],
			"example": [
				"For n = 4 and k = 4, T(4, 4) = 4 because all the permutations of 4 symbols that 4-commute with permutation (1, 2, 3, 4) are (1, 3), (2, 4), (1, 2)(3, 4) and (1, 4)(2, 3)."
			],
			"mathematica": [
				"T[n_, k_] := n Binomial[n, k] ((-1)^k+Sum[(-1)^j k!/(k-j)/j!, {j, 0, k-1}]);",
				"Table[T[n, k], {n, 0, 10}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Nov 03 2018 *)"
			],
			"xref": [
				"Cf. A007318, A000757."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Luis Manuel Rivera Martínez_, Dec 09 2013",
			"references": 13,
			"revision": 63,
			"time": "2019-04-20T08:08:13-04:00",
			"created": "2013-12-11T15:20:22-05:00"
		}
	]
}