{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A341211",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 341211,
			"data": "3,3,3,13,3,3,3,113,331,3631,827,3109,4253,7487,71",
			"name": "Smallest prime p such that (p^(2^n) + 1)/2 is prime.",
			"comment": [
				"Expressions of the form m^j + 1 can be factored (e.g., m^3 + 1 = (m + 1)*(m^2 - m + 1)) for any positive integer j except when j is a power of 2, so (p^j + 1)/2 for prime p cannot be prime unless j is a power of 2.",
				"a(12) \u003c= 4253, a(13) \u003c= 7487, a(14) \u003c= 71. - _Daniel Suteu_, Feb 07 2021",
				"a(13) \u003e 2500 and a(14) = 71. - _Jinyuan Wang_, Feb 07 2021"
			],
			"link": [
				"Dario Alpern, \u003ca href=\"https://alpertron.com.ar/ECM.HTM\"\u003eInteger factorization calculator\u003c/a\u003e"
			],
			"example": [
				"No term is smaller than 3 (since 2 is the only smaller prime, and (2^(2^n) + 1)/2 is not an integer).",
				"(3^(2^0) + 1)/2 = (3^1 + 1)/2 = (3 + 1)/2 = 4/2 = 2 is prime, so a(0)=3.",
				"(3^(2^1) + 1)/2 = (3^2 + 1)/2 = 5 is prime, so a(1)=3.",
				"(3^(2^2) + 1)/2 = (3^4 + 1)/2 = 41 is prime, so a(2)=3.",
				"(3^(2^3) + 1)/2 = (3^8 + 1)/2 = 3281 = 17*193 is not prime, nor is (p^8 + 1)/2 for any other prime \u003c 13, but (13^8 + 1)/2 = 407865361 is prime, so a(3)=13."
			],
			"program": [
				"(PARI) a(n) = my(p=3); while (!isprime((p^(2^n) + 1)/2), p=nextprime(p+1)); p; \\\\ _Michel Marcus_, Feb 07 2021",
				"(Alpertron) x=3;x=N(x);NOT IsPrime((x^8192+1)/2);N(x)",
				"# _Martin Ehrenstein_, Feb 08 2021",
				"(Python)",
				"from sympy import isprime, nextprime",
				"def a(n):",
				"  p, pow2 = 3, 2**n",
				"  while True:",
				"    if isprime((p**pow2 + 1)//2): return p",
				"    p = nextprime(p)",
				"print([a(n) for n in range(9)]) # _Michael S. Branicky_, Mar 03 2021"
			],
			"xref": [
				"Cf. A005383, A048161, A176116, A340480.",
				"Cf. A093625 and A171381 (both for when p=3)."
			],
			"keyword": "nonn,hard,more",
			"offset": "0,1",
			"author": "_Jon E. Schoenfield_, Feb 06 2021",
			"ext": [
				"a(11) from _Daniel Suteu_, Feb 07 2021",
				"a(12) from _Jinyuan Wang_, Feb 07 2021",
				"a(13)-a(14), using Dario Alpern's integer factorization calculator and prior bounds, from _Martin Ehrenstein_, Feb 08 2021"
			],
			"references": 4,
			"revision": 34,
			"time": "2021-03-04T02:29:56-05:00",
			"created": "2021-02-06T22:12:28-05:00"
		}
	]
}