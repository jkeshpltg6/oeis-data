{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A052446",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 52446,
			"data": "0,1,1,3,10,52,351,3714,63638,1912203,103882478,10338614868,1892863194064,639799762452639,400857034314325045,467526363203064793081,1019286659457016864347582,4170114225096278323394128049,32130213534058019378134295287305",
			"name": "Number of unlabeled simple connected bridged graphs on n nodes.",
			"comment": [
				"These are unlabeled connected graphs with spanning edge-connectivity 1, where the spanning edge-connectivity of a graph is the minimum number of edges that must be removed (without removing incident vertices) to obtain a disconnected or empty graph. - _Gus Wiseman_, Sep 02 2019"
			],
			"link": [
				"Jean-François Alcover, \u003ca href=\"/A052446/b052446.txt\"\u003eTable of n, a(n) for n = 1..22\u003c/a\u003e",
				"Travis Hoppe and Anna Petrone, \u003ca href=\"https://github.com/thoppe/Encyclopedia-of-Finite-Graphs\"\u003eEncyclopedia of Finite Graphs\u003c/a\u003e",
				"T. Hoppe and A. Petrone, \u003ca href=\"http://arxiv.org/abs/1408.3644\"\u003eInteger sequence discovery from small graphs\u003c/a\u003e, arXiv preprint arXiv:1408.3644 [math.CO], 2014.",
				"T. Hoppe and A. Petrone, \u003ca href=\"http://doi.org/10.1016/j.dam.2015.07.017\"\u003eInteger sequence discovery from small graphs\u003c/a\u003e, Discr. Appl. Math. 201 (2016) 172-181",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/k-Edge-ConnectedGraph.html\"\u003ek-Edge-Connected Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/BridgedGraph.html\"\u003eBridged Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ConnectedGraph.html\"\u003eConnected Graph\u003c/a\u003e",
				"Gus Wiseman, \u003ca href=\"/A052446/a052446.png\"\u003eThe a(2) = 1 through a(5) = 10 connected bridged graphs\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A001349(n) - A007146(n)."
			],
			"mathematica": [
				"A001349 = Cases[Import[\"https://oeis.org/A001349/b001349.txt\", \"Table\"], {_, _}][[All, 2]];",
				"A007146 = Cases[Import[\"https://oeis.org/A007146/b007146.txt\", \"Table\"], {_, _}][[All, 2]] ;",
				"a[n_] := A001349[[n + 1]] - A007146[[n]];",
				"Array[a, 22] (* _Jean-François Alcover_, Nov 09 2019 *)"
			],
			"xref": [
				"Cf. other k-edge-connected unlabeled graph sequences A052446, A052447, A052448, A241703, A241704, A241705.",
				"Cf. A001349 (number of simple connected graphs).",
				"Cf. A007146 (number of simple connected bridgeless graphs).",
				"Cf. A263914 (number of simple bridgeless graphs).",
				"Cf. A263915 (number of simple bridged graphs).",
				"Column k = 1 of A263296.",
				"Row sums of A327077 if the first column is removed.",
				"BII-numbers of set-systems with spanning edge-connectivity 1 are A327111.",
				"The labeled version is A327071.",
				"Cf. A002494, A327069, A327074, A327109, A327144."
			],
			"keyword": "nonn,hard",
			"offset": "1,4",
			"author": "_Eric W. Weisstein_, May 08 2000",
			"ext": [
				"a(8) and a(9) and better description by _Eric W. Weisstein_, Nov 07 2010",
				"a(10) from the Encyclopedia of Finite Graphs by _Travis Hoppe_ and _Anna Petrone_, Apr 22 2014",
				"Additional terms from A001349 and A007146 by _Eric W. Weisstein_, Oct 29 2015",
				"a(18)-a(22) from A001349 and A007146 by _Jean-François Alcover_, Nov 09 2019"
			],
			"references": 25,
			"revision": 54,
			"time": "2019-11-10T01:40:08-05:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}