{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A326564",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 326564,
			"data": "1,1,-2,7,-26,102,-420,1787,-7794,34666,-156636,716982,-3317700,15494156,-72935624,345701843,-1648489762,7902956738,-38067806892,184152092450,-894259126540,4357738501844,-21302682030328,104439435098718,-513390992000340,2529846489669412,-12494572784556440,61838364112438732,-306647601790749384,1523380558254732312,-7580755340625743760,37783723921640161923",
			"name": "O.g.f. A(x) satisfies: 0 = Sum_{n\u003e=1} (b(n) - A(x))^n * (2*x)^n / n, where b(n) = 1 if n is odd or b(n) = 2 if n is even.",
			"comment": [
				"a(n) is odd iff n = 2^k - 1 for k \u003e= 0.",
				"Signed version of A307413."
			],
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A326564/b326564.txt\"\u003eTable of n, a(n) for n = 0..520\u003c/a\u003e"
			],
			"formula": [
				"O.g.f. A = A(x) satisfies:",
				"(1) 0 = Sum_{n\u003e=1} (3 + (-1)^n - 2*A(x))^n * x^n / n.",
				"(2) 0 = arctanh(2*x - 2*x*A) - log(1 - 4*x^2*(2 - A)^2)/2.",
				"(3) 1 - 4*x^2*(2 - A)^2 = (1 + 2*x - 2*x*A) / (1 - 2*x + 2*x*A).",
				"(4) A(x) = 1 + (A - 2)^2*x + 2*(A - 1)*(A - 2)^2*x^2.",
				"(5) 0 = 2*(A - 1)*(A - 2)^2*x^2 + (A - 2)^2*x - (A - 1).",
				"(6) x = ( sqrt( (A-2)^4 + 8*(A-1)^2*(A-2)^2 ) - (A-2)^2 ) / (4*(A-1)*(A-2)^2).",
				"(7) A(x) = 2 - (1/x) * Series_Reversion( x + x^2/(1 - 2*x^2) )."
			],
			"example": [
				"O.g.f.: A(x) = 1 + x - 2*x^2 + 7*x^3 - 26*x^4 + 102*x^5 - 420*x^6 + 1787*x^7 - 7794*x^8 + 34666*x^9 - 156636*x^10 + 716982*x^11 - 3317700*x^12 + 15494156*x^13 - 72935624*x^14 + 345701843*x^15 - 1648489762*x^16 + ...",
				"such that",
				"0 = (1 - A(x))*(2*x) + (2 - A(x))^2*(2*x)^2/2 + (1 - A(x))^3*(2*x)^3/3 + (2 - A(x))^4*(2*x)^4/4 + (1 - A(x))^5*(2*x)^5/5 + (2 - A(x))^6*(2*x)^6/6 + (1 - A(x))^7*(2*x)^7/7 + (2 - A(x))^8*(2*x)^8/8 + (1 - A(x))^9*(2*x)^9/9 + ...",
				"SPECIAL ARGUMENTS.",
				"A( (3 - sqrt(17))/6 ) = 1/2.",
				"A( (15 - sqrt(513))/40 ) = 1/3.",
				"ODD TERMS.",
				"The odd numbers occur at positions 2^n-1 and begin",
				"[1, 1, 7, 1787, 345701843, 37783723921640161923, 1297226675901009799785880946943488094880739, 4359630365907394639251834255689265800511483817161978056491648421720696612963282942355107, ...]."
			],
			"program": [
				"(PARI) /* By definition */",
				"{a(n) = my(A=[1]); for(i=0, n, A = concat(A, 0); A[#A] = polcoeff(sum(m=1, #A, ( ((m+1)%2) + 1 - Ser(A) )^m * (2*x)^m/m), #A)/2); A[n+1]}",
				"for(n=0, 32, print1(a(n), \", \"))",
				"(PARI) /* From: A(x) = 2 - (1/x) * Series_Reversion( x + x^2/(1 - 2*x^2) ) */",
				"{a(n) = my(A = 2 - (1/x)*serreverse(x + x^2/(1 - 2*x^2 +x*O(x^n)))); polcoeff(A,n)}",
				"for(n=0, 32, print1(a(n), \", \"))"
			],
			"xref": [
				"Cf. A316363, A307413."
			],
			"keyword": "sign",
			"offset": "0,3",
			"author": "_Paul D. Hanna_, Aug 28 2019",
			"references": 2,
			"revision": 14,
			"time": "2019-08-29T12:40:23-04:00",
			"created": "2019-08-28T23:04:22-04:00"
		}
	]
}