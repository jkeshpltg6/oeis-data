{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A309951",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 309951,
			"data": "1,1,1,1,1,3,2,1,10,27,18,1,47,718,4416,10656,6912,1,246,20545,751800,12911500,100380000,304200000,216000000,1,1602,929171,260888070,39883405500,3492052425000,177328940580000,5153150631600000,82577533320000000,669410956800000000,2224399449600000000,1632586752000000000,1,11481",
			"name": "Irregular triangular array, read by rows: T(n,k) is the sum of the products of multinomial coefficients (n_1 + n_2 + n_3 + ...)!/(n_1! * n_2! * n_3! * ...) taken k at a time, where (n_1, n_2, n_3, ...) runs over all integer partitions of n (n \u003e= 0, 0 \u003c= k \u003c= A000041(n)).",
			"comment": [
				"This array was inspired by _R. H. Hardin_'s recurrences for the columns of array A212855. Rows k=1 to k=5 are due to him, while the remaining rows were computed by _Alois P. Heinz_.",
				"Row n has length A000041(n) + 1, i.e., one more than the number of partitions of n.",
				"Let R(m,n) := R(m,n,t=0) = A212855(m,n) for m,n \u003e= 1, where R(m,n,t) = LHS of Eq. (6) of Abramson and Promislow (1978, p. 248).",
				"Let P_n be the set of all lists a = (a_1, a_2,..., a_n) of integers a_i \u003e= 0, i = 1,..., n such that 1*a_1 + 2*a_2 + ... + n*a_n = n; i.e., P_n is the set all integer partitions of n. (We use a different notation for partitions than the one in the name of T(n,k).) Then |P_n| = A000041(n) for n \u003e= 0.",
				"We have R(m,n) = A212855(m,n) = Sum_{a in P_n} (-1)^(n - Sum_{j=1..n} a_j) * (a_1 + a_2 + ... + a_n)!/(a_1! * a_2! * ... * a_n!) * (n! / ((1!)^a_1 * (2!)^a_2 * ... * (n!)^a_n))^m.",
				"The recurrence of _R. H. Hardin_ for column n of array A212855 is Sum_{s = 0..|P_n|} (-1)^s * T(n,s) * R(m-s,n) = 0 for n \u003e= 1 and m \u003e= |P_n| + 1.",
				"The above recurrence is correct for all n \u003e= 1, but it is not always a minimal one. For example, it seems to be the minimal one for n = 1,...,6, but not for n = 7 (see A212854). It seems to be minimal whenever every two different partitions of n give different multinomial coefficients.",
				"For n = 7, the partitions (a_1, a_2, a_3, a_4, a_5, a_6, a_7) = (0, 2, 1, 0, 0, 0, 0) (i.e., 2 + 2 + 3) and  (a_1, a_2, a_3, a_4, a_5, a_6, a_7) = (3, 0, 0, 1, 0, 0, 0) (i.e., 1 + 1 + 1 + 4) give the same multinomial coefficient: 210 = 7!/(2!2!3!) = 7!/(1!1!1!4!). Hence, to find the minimal recurrence for n = 7, we count 210 only once in the set of multinomial coefficients: 1, 7, 21, 35, 42, 105, 140, 210, 420, 630, 840, 1260, 2520, 5040. Then the absolute value of the coefficient of a(n-1) in the minimal recurrence is the sum of these multinomial coefficients (i.e., 11271); the absolute value of the coefficient of a(n-2) in the minimal recurrence is the sum of products of every two of them (i.e., 46169368), and so on.",
				"Looking at the multinomial coefficients of the integer partitions of n = 8, 9, 10 on pp. 831-832 of Abramowitz and Stegun (1964), we see that, even in these cases, the above recurrence is not the minimal one. The number of distinct multinomial coefficients among the integer partitions of n is given by A070289."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A309951/b309951.txt\"\u003eRows n = 0..14, flattened\u003c/a\u003e",
				"Milton Abramowitz and Irene A. Stegun, \u003ca href=\"http://www.cs.bham.ac.uk/~aps/research/projects/as/book.php\"\u003eHandbook of Mathematical Functions with Formulas, Graphs, and Mathematical Tables\u003c/a\u003e, National Bureau of Standards (Applied Mathematics Series, 55), 1964; see pp. 831-832 for the multinomial coefficients of integer partitions of n = 1..10.",
				"Morton Abramson and David Promislow, \u003ca href=\"https://doi.org/10.1016/0097-3165(78)90012-2\"\u003eEnumeration of arrays by column rises\u003c/a\u003e, J. Combinatorial Theory Ser. A 24(2) (1978), 247-250; see Eq. (6), p. 248, and the comments above.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Partition_(number_theory)\"\u003ePartition (number theory)\u003c/a\u003e."
			],
			"formula": [
				"Sum_{k=0..A000041(n)} (-1)^k * T(n,k) = 0."
			],
			"example": [
				"Triangle begins as follows:",
				"  [n=0]: 1,   1;",
				"  [n=1]: 1,   1;",
				"  [n=2]: 1,   3,     2;",
				"  [n=3]: 1,  10,    27,     18;",
				"  [n=4]: 1,  47,   718,   4416,    10656,      6912;",
				"  [n=5]: 1, 246, 20545, 751800, 12911500, 100380000, 304200000, 216000000;",
				"  ...",
				"For example, when n = 3, the integer partitions of 3 are 3, 1+2, 1+1+1, and the corresponding multinomial coefficients are 3!/3! = 1, 3!/(1!2!) = 3, and 3!/(1!1!1!) = 6. Then T(n=3, k=0) = 1, T(n=3, k=1) = 1 + 3 + 6 = 10, T(n=3, k=2) = 1*3 + 1*6 + 3*6 = 27, and T(n=3, k=3) = 1*3*6 = 18.",
				"Since |P_3| = A000041(3) = 3, the recurrence of _R. H. Hardin_ for column n = 3 of array A212855 is T(3,0)*R(m,3) - T(3,1)*R(m-1,3) + T(3,2)*R(m-2,3) - T(3,3)*R(m-3,3) = 0; i.e., R(m,3) - 10*R(m-1,3) + 27*R(m-2,3) - 18*R(m-3,3) = 0 for m \u003e= 4. We have the initial conditions R(m=1,3) = 1, R(m=2,3) = 19, and R(m=3,3) = 163. Thus, R(m,3) = 6^m - 2*3^m + 1 = A212850(m) for m \u003e= 1. See the documentation of array A212855."
			],
			"maple": [
				"g:= proc(n, i) option remember; `if`(n=0 or i=1, [n!], [map(x-\u003e",
				"      binomial(n, i)*x, g(n-i, min(n-i, i)))[], g(n, i-1)[]])",
				"    end:",
				"b:= proc(n, m) option remember; `if`(n=0, 1,",
				"      expand(b(n-1, m)*(g(m$2)[n]*x+1)))",
				"    end:",
				"T:= n-\u003e(p-\u003eseq(coeff(p, x, i), i=0..degree(p)))(b(nops(g(n$2)), n)):",
				"seq(T(n), n=0..7);  # _Alois P. Heinz_, Aug 25 2019"
			],
			"mathematica": [
				"g[n_, i_] := g[n, i] = If[n==0 || i==1, {n!}, Join[Binomial[n, i]*#\u0026 /@ g[n - i, Min[n - i, i]], g[n, i - 1]]];",
				"b[n_, m_] := b[n, m] = If[n==0, 1, Expand[b[n-1, m]*(g[m, m][[n]]*x+1)]];",
				"T[n_] := CoefficientList[b[Length[g[n, n]], n], x];",
				"T /@ Range[0, 7] // Flatten (* _Jean-François Alcover_, Feb 18 2021, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A000012 (column k=0), A000041, A005651 (column k=1), A070289, A212850, A212851, A212852, A212853, A212854, A212855, A212856, A212857, A212858, A212859, A212860.",
				"Rightmost terms in rows give A309972."
			],
			"keyword": "nonn,tabf",
			"offset": "0,6",
			"author": "_Petros Hadjicostas_, Aug 25 2019",
			"references": 12,
			"revision": 88,
			"time": "2021-02-18T14:17:21-05:00",
			"created": "2019-08-26T04:22:48-04:00"
		}
	]
}