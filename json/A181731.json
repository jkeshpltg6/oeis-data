{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A181731",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 181731,
			"data": "1,1,1,1,2,2,1,6,14,4,1,24,222,106,8,1,120,6384,9918,838,16,1,720,291720,2306904,486924,6802,32,1,5040,19445040,1085674320,964948464,25267236,56190,64,1,40320,1781750880,906140159280,4927561419120,439331916888,1359631776,470010,128,1,362880,214899027840,1224777388630320,54259623434853360",
			"name": "Table A(d,n) of the number of paths of a chess rook in a d-dimensional hypercube from (0...0) to (n...n) where the rook may move in steps that are multiples of (1,0..0), (0,1,0..0), ..., (0..0,1).",
			"comment": [
				"The table is enumerated along antidiagonals: A(1,0), A(2,0), A(1,1), A(3,0), A(2,1), A(1,2), A(4,0), A(3,1), A(2,2), A(1,3), ... ."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A181731/b181731.txt\"\u003eAntidiagonals n = 1..20\u003c/a\u003e",
				"M. Kauers and D. Zeilberger, \u003ca href=\"http://arxiv.org/abs/1011.4671\"\u003eThe Computational Challenge of Enumerating High-Dimensional Rook Walks\u003c/a\u003e, arXiv:1011.4671 [math.CO], 2010."
			],
			"example": [
				"A(3,1) = 6 because there are 6 rook paths on 3D chessboards from (0,0,0) to (1,1,1).",
				"Square table A(d,n) begins:",
				"  1,   1,      2,          4,             8, ...",
				"  1,   2,     14,        106,           838, ...",
				"  1,   6,    222,       9918,        486924, ...",
				"  1,  24,   6384,    2306904,     964948464, ...",
				"  1, 120, 291720, 1085674320, 4927561419120, ..."
			],
			"maple": [
				"b:= proc(l) option remember; `if`({l[]} minus {0}={}, 1, add(add",
				"       (b(sort(subsop(i=l[i]-j, l))), j=1..l[i]), i=1..nops(l)))",
				"    end:",
				"A:= (d, n)-\u003e b([n$d]):",
				"seq(seq(A(h-n, n), n=0..h-1), h=1..10); # _Alois P. Heinz_, Jul 21 2012"
			],
			"mathematica": [
				"b[l_List] := b[l] = If[Union[l] ~Complement~ {0} == {}, 1, Sum[ Sum[ b[ Sort[ ReplacePart[l, i -\u003e l[[i]] - j]]], {j, 1, l[[i]]}], {i, 1, Length[l]}]]; A[d_, n_] := b[Array[n\u0026, d]]; Table[Table[A[h-n, n], {n, 0, h-1}], {h, 1, 10}] // Flatten (* _Jean-François Alcover_, Feb 25 2015, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Rows d=1-12 give: A011782, A051708 (from [1,1]), A144045 (from [1,1,1]), A181749, A181750, A181751, A181752, A181724, A181725, A181726, A181727, A181728.",
				"Columns n=0-2 give: A000012, A000142, A105749.",
				"Main diagonal gives A246623."
			],
			"keyword": "nonn,tabl",
			"offset": "1,5",
			"author": "_Manuel Kauers_, Nov 16 2010",
			"ext": [
				"Edited by _Alois P. Heinz_, Jul 21 2012",
				"Minor edits by _Vaclav Kotesovec_, Sep 03 2014"
			],
			"references": 14,
			"revision": 32,
			"time": "2019-07-24T15:24:22-04:00",
			"created": "2010-11-12T14:31:08-05:00"
		}
	]
}