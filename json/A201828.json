{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A201828",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 201828,
			"data": "37,37,2,2,2,2,907,2,2833,907,2,8269,2749,2953,5413,7699,2137,27103,28513,74377,45673,56629,79147,33529,15259,96847,101599,57649,44983,300973,706309,715357,351847,38557,308809,720607,901447,2229889,867253,2370937,1276867",
			"name": "The smallest A(m) such that the interval (A(m)*n, A(m+1)*n) contains exactly one element of A, where A is the sequence of primes p for which p-2 is not prime.",
			"comment": [
				"This sequence is the \"A-analog\" of A195871.",
				"This is a possible model sequence to understand the role of twin primes in sequences like A195871.  In particular, if after a large number N_tw, there are no twin primes, what primes will take their place in A195871?  Our observations and expectations are expressed in the following conjecture.",
				"Conjecture: For n\u003e=13, every a(n) is the lesser of a pair of cousin primes p and p+4, cf. A023200.  Note that it is only conjectured that there are infinitely many pairs of cousin primes.",
				"The limit of a(n) as n goes to infinity is infinity."
			],
			"example": [
				"Let n=2. We have the following intervals of the form (2*p,2*q), where p,q are consecutive primes in A025584:(4,6),(6,22),(22,34),(34,46),(46,58),(58,74),(74,82),..., containing 0,2,2,2,2,3,1,... primes from A025584. The interval (74,82) is the first to contain exactly one prime from A025584, so a(2)=74/2=37."
			],
			"mathematica": [
				"myPrime=Select[#,!PrimeQ[#-2]\u0026]\u0026[Prime[Range[500000]]];  binarySearch[lst_,find_]:=Module[{lo=1,up=Length[lst],v},(While[lo\u003c=up,v=Floor[(lo+up)/2];If[lst[[v]]-find==0,Return[v]];If[lst[[v]]\u003cfind,lo=v+1,up=v-1]];0)]; myPrimeQ[n_]:=binarySearch[myPrime,n]; nextMyPrime[n_,offset_Integer:1]:=myPrime[[myPrimeQ[NextPrime[n,NestWhile[#1+1\u0026,1,!myPrimeQ[NextPrime[n,#1]]\u003e0\u0026]]]+offset-1]];   z=1;(*example for \"contains exactly ONE myPrime in the interval\"*)Table[myPrime[[NestWhile[#1+1\u0026,1,!((nextMyPrime[n myPrime[[#1]],z]\u003cn myPrime[[#1+1]]\u0026\u0026nextMyPrime[n myPrime[[#1]],z+1]\u003en myPrime[[#1+1]]))\u0026]]],{n,2,30}]"
			],
			"program": [
				"(PARI) npr(n) = {local(p); p=n+1; while(!isprime(p) || isprime(p-2), p=p+1); p}",
				"cnt(a,b) = {local(r); r=0; for(p=a, b, if(isprime(p) \u0026\u0026 !isprime(p-2), r=r+1)); r}",
				"a201828(n) = {local(a,b); a=2; b=3; while(cnt(a*n, b*n) != 1, a=b; b=npr(b)); a} \\\\ _Michael B. Porter_, Feb 18 2013"
			],
			"xref": [
				"Cf. A025584, A195871, A023200."
			],
			"keyword": "nonn",
			"offset": "2,1",
			"author": "_Vladimir Shevelev_ and _Peter J. C. Moses_, Jan 09 2013",
			"references": 1,
			"revision": 41,
			"time": "2014-12-03T00:53:07-05:00",
			"created": "2013-02-19T04:29:48-05:00"
		}
	]
}