{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097664",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97664,
			"data": "1,4,2,9,8,8,4,3,0,8,4,0,1,2,3,4,2,0,5,6,6,1,7,9,0,4,2,4,7,7,5,1,3,8,0,9,6,5,6,4,9,8,2,3,6,7,6,7,5,6,4,4,6,4,8,8,7,6,3,4,6,2,1,4,8,8,3,6,9,9,4,5,0,9,1,2,2,0,3,9,6,1,6,1,8,2,1,9,5,9,1,4,6,9,0,1,8,4,6,3,6,2,3,7,8",
			"name": "Decimal expansion of the constant 3*exp(psi(2/3) + EulerGamma), where EulerGamma is the Euler-Mascheroni constant (A001620) and psi(x) is the digamma function.",
			"comment": [
				"This constant appears in _Benoit Cloitre_'s generalized Euler-Gauss formula for the Gamma function (see Cloitre link) and is involved in the exact determination of asymptotic limits of certain order-3 linear recursions with varying coefficients (see A097678 for example)."
			],
			"reference": [
				"A. M. Odlyzko, Linear recurrences with varying coefficients, in Handbook of Combinatorics, Vol. 2, R. L. Graham, M. Grotschel and L. Lovasz, eds., Elsevier, Amsterdam, 1995, pp. 1135-1138."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A097664/b097664.txt\"\u003eTable of n, a(n) for n = 1..2500\u003c/a\u003e",
				"Benoit Cloitre, \u003ca href=\"/A097679/a097679.pdf\"\u003eOn a generalization of Euler-Gauss formula for the Gamma function\u003c/a\u003e, preprint 2004.",
				"Andrew Odlyzko, \u003ca href=\"http://www.dtc.umn.edu/~odlyzko/doc/asymptotic.enum.pdf\"\u003eAsymptotic enumeration methods\u003c/a\u003e, in Handbook of Combinatorics, vol. 2, 1995, pp. 1063-1229.",
				"Xavier Gourdon and Pascal Sebah, \u003ca href=\"http://numbers.computation.free.fr/Constants/Miscellaneous/gammaFunction.html\"\u003eIntroduction to the Gamma Function\u003c/a\u003e."
			],
			"formula": [
				"c = 1/sqrt(3)*exp(Pi/sqrt(12))."
			],
			"example": [
				"c = 1.42988430840123420566179042477513809656498236767564464887634..."
			],
			"mathematica": [
				"RealDigits[1/Sqrt[3]*E^(Pi/Sqrt[12]), 10, 105][[1]] (* _Robert G. Wilson v_, Aug 28 2004 *)"
			],
			"program": [
				"(PARI) 3*exp(psi(2/3)+Euler)",
				"(MAGMA) SetDefaultRealField(RealField(100)); R:= RealField(); (1/Sqrt(3))*Exp(Pi(R)/Sqrt(12)); // _G. C. Greubel_, Aug 27 2018"
			],
			"xref": [
				"Cf. A097663, A097665-A097676."
			],
			"keyword": "cons,nonn",
			"offset": "1,2",
			"author": "_Paul D. Hanna_, Aug 25 2004",
			"ext": [
				"More terms from _Robert G. Wilson v_, Aug 28 2004"
			],
			"references": 4,
			"revision": 22,
			"time": "2021-02-27T13:20:38-05:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}