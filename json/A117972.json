{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A117972",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 117972,
			"data": "1,-1,3,-45,315,-14175,467775,-42567525,638512875,-97692469875,9280784638125,-2143861251406875,147926426347074375,-48076088562799171875,9086380738369043484375,-3952575621190533915703125",
			"name": "Numerator of zeta'(-2n), n \u003e= 0.",
			"comment": [
				"In A160464 the coefficients of the ES1 matrix are defined. This matrix led to the discovery that the successive differences of the ES1[1-2*m,n] coefficients for m = 1, 2, 3, ..., are equal to the values of zeta'(-2n), see also A094665 and A160468. - _Johannes W. Meijer_, May 24 2009",
				"A048896(n), n \u003e= 1: Numerators of Maclaurin series for 1 - ((sin x)/x)^2,",
				"  a(n), n \u003e= 2: Denominators of Maclaurin series for 1 - ((sin x)/x)^2, the correlation function in Montgomery's pair correlation conjecture. - _Daniel Forgues_, Oct 16 2011",
				"From _Andrey Zabolotskiy_, Sep 23 2021: (Start)",
				"zeta'(-2n), which is mentioned in the Name, is irrational. For n \u003e 0, a(n) is the numerator of the rational fraction g(n) = Pi^(2n)*zeta'(-2n)/zeta(2n+1). The denominator is 4*A048896(n-1). g(n) = f(n) for n \u003e 0, where f(n) is given in the Formula section. Also, f(n) = Bernoulli(2n)/z(n)/4 (see Formula section) for all n.",
				"For n = 0, zeta'(0) = -log(2Pi)/2, g(0) can be set to 0 because of the infinite denominator. However, a(0) is set to 1 because it is the numerator of f(0).",
				"It seems that -4*f(n)*alpha_n = A000182(n), where alpha_n = A191657(n, p(n)) / A191658(n, p(n)) [where p(n) = A000041(n)] is the n-th \"elementary coefficient\" from the paper by Izaurieta et al. (End)"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A117972/b117972.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"Fernando Izaurieta, Ricardo Ramírez and Eduardo Rodríguez, \u003ca href=\"https://arxiv.org/abs/1106.1648\"\u003eDirac Matrices for Chern-Simons Gravity\u003c/a\u003e, arXiv:1106.1648 [math-ph], 2011-2012.",
				"J. Sondow and E. W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/RiemannZetaFunction.html\"\u003eMathWorld: Riemann Zeta Function\u003c/a\u003e"
			],
			"formula": [
				"a(n) = numerator(f(n)) where f(n) = (2*n)!/2^(2*n + 1)(-1)^n, from the first Mathematica code.",
				"From _Terry D. Grant_, May 28 2017: (Start)",
				"|a(n)| = A049606(2n).",
				"a(n) = -numerator(Bernoulli(2n)/z(n)) where Bernoulli(2n) = A000367(n) / A002445(n) and z(n) = A046988(n) / A002432(n) for n \u003e 0. (End) [Corrected by _Andrey Zabolotskiy_, Sep 23 2021]"
			],
			"example": [
				"-1/4, 3/4, -45/8, 315/4, -14175/8, 467775/8, -42567525/16, ...",
				"-zeta(3)/(4*Pi^2), (3*zeta(5))/(4*Pi^4), (-45*zeta(7))/(8*Pi^6), (315*zeta(9))/(4*Pi^8), (-14175*zeta(11))/(8*Pi^10), ..."
			],
			"maple": [
				"# Without rational arithmetic",
				"a := n -\u003e (-1)^n*(2*n)!*2^(add(i,i=convert(n,base,2))-2*n);",
				"# _Peter Luschny_, May 02 2009"
			],
			"mathematica": [
				"Table[Numerator[(2 n)!/2^(2 n + 1) (-1)^n], {n, 0, 30}]",
				"-(Numerator[(Table[ BernoulliB[2*n]], {n, 1, 22}] / (Table[[Zeta[2*n]/Pi^(2 n)], {n,1,22}]]) for terms \u003e a(1) (* _Terry D. Grant_, May 28 2017 *)"
			],
			"program": [
				"(Maxima) L:taylor(1/x*sin(sqrt(x))^2,x,0,15); makelist(denom(coeff(L,x,n)),n,0,15); // _Vladimir Kruchinin_, May 30 2011"
			],
			"xref": [
				"Cf. A000367, A002445, A049606, A046988, A002432, A117973, A048896.",
				"From _Johannes W. Meijer_, May 24 2009: (Start)",
				"Cf. A160464, A094665 and A160468.",
				"Absolute values equal row sums of A160468. (End)",
				"Cf. A191657, A191658, A000182, A000041."
			],
			"keyword": "sign,frac",
			"offset": "0,3",
			"author": "_Eric W. Weisstein_, Apr 06 2006",
			"ext": [
				"First term added, offset changed and edited by _Johannes W. Meijer_, May 15 2009"
			],
			"references": 9,
			"revision": 48,
			"time": "2021-09-23T11:45:53-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}