{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A136487",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 136487,
			"data": "1,1,1,1,1,-1,-1,-1,0,2,0,-1,2,0,-4,0,2,-3,2,7,-4,-5,2,1,5,-5,-11,11,7,-7,-1,1,-8,12,16,-28,-8,20,0,-4,13,-25,-20,60,-2,-46,12,12,-3,-1,-21,50,19,-120,38,92,-50,-24,15,2,-1",
			"name": "Coefficients of polynomial recursion p(n,x) = (-1+x)*p(n-1,x)+(1-x^2)*p(n-2,x), p(0,x)=1, p(1,x)=1+x.",
			"comment": [
				"Only coefficients of x^k for k \u003c= degree of p(n,x) are included.  Thus since p(2,x) = 0, row 2 is empty.",
				"Row sums for n\u003e= 2 are 0.",
				"A converse recursion is with different signs but same absolute coefficients is:",
				"P[x, 0] = 1; P[x, 1] = x - 1;",
				"P[x_, n_] := P[x, n] = (x + 1)*P[x, n - 1] - (x^2 - 1)*P[x, n - 2]"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A136487/b136487.txt\"\u003eTable of n, a(n) for n = 0..10103\u003c/a\u003e(rows 0 to 141, flattened)"
			],
			"formula": [
				"p(x,0)=1;p(x,1)=x+1; p(x,n)=(1-x)*p(x,n-1)+(1-x^2)*p(x,n-2)",
				"From _Robert Israel_, Dec 03 2018: (Start)",
				"T(n,k) = T(n-1,k-1) - T(n-1,k) - T(n-2,k-2) + T(n-2,k).",
				"G.f. as array: (1-2*x)/(1 + x*(y-1)+x^2*(1-y^2)).",
				"T(n,0) = (-1)^(n+1)*A000045(n-2) for n \u003e= 3.",
				"(End)"
			],
			"example": [
				"First few rows are",
				"{1},",
				"{1, 1},",
				"{},",
				"{1, 1, -1, -1},",
				"{-1, 0, 2, 0, -1},",
				"{2, 0, -4, 0, 2},",
				"{-3, 2, 7, -4, -5, 2, 1},",
				"{5, -5, -11, 11, 7, -7, -1, 1},",
				"{-8, 12, 16, -28, -8, 20, 0, -4},",
				"{13, -25, -20, 60, -2, -46, 12, 12, -3, -1},",
				"{-21, 50, 19, -120, 38, 92, -50, -24, 15, 2, -1}"
			],
			"maple": [
				"F:= proc(n) option remember; expand((1-x)*procname(n-1)+(1-x^2)*procname(n-2)) end proc:",
				"F(0):= 1: F(1):= 1+x:",
				"R:=proc(n) local V,j;",
				" V:= F(n);",
				" seq(coeff(V,x,j),j=0..degree(V))",
				"end proc:",
				"for i from 0 to 20 do R(i) od; # _Robert Israel_, Dec 03 2018"
			],
			"mathematica": [
				"Clear[P,n,m,x] P[x,-1]=0;P[x,0]=1;P[x,1]=x-1; P[x_,n_]:=P[x,n]=(x+1)*P[x,n-1]-(x^2-1)*P[x,n-2]; a=Table[CoefficientList[P[x,n],x],{n,0,10}]; Flatten[a]"
			],
			"xref": [
				"Cf. A000045."
			],
			"keyword": "tabf,sign",
			"offset": "0,10",
			"author": "_Roger L. Bagula_, Mar 21 2008",
			"ext": [
				"Edited by _Robert Israel_, Dec 03 2018"
			],
			"references": 1,
			"revision": 10,
			"time": "2018-12-03T18:28:10-05:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}