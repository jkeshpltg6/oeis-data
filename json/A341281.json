{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A341281",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 341281,
			"data": "2,1,1,0,1,1,0,1,1,0,1,2,0,1,2,1,1,2,1,1,1,1,1,1,1,1,1,1,1,1,2,0,1,1,0,1,1,0,1,2,0,1,2,1,1,2,1,1,2,1,1,1,1,1,1,1,1,1,1,0,2,1,0,1,1,0,1,1,0,1,2,0,1,2,1,1,2,1,1,1,1,1,1,1,1,1,1,1,1,1",
			"name": "Number of n-digit nonnegative integers m whose first n digits of their binary expansion are equal to m.",
			"comment": [
				"This sequence comes from a problem proposed on the French maths site Diophante.",
				"The corresponding numbers m are in A181929.",
				"Question: does some k exist such that a(k) \u003e= 3?",
				"Yes, the first occurrence is a(770) = 3. See attached file. - _Scott R. Shannon_, Feb 25 2021"
			],
			"link": [
				"Michel Marcus, \u003ca href=\"/A341281/b341281.txt\"\u003eTable of n, a(n) for n = 1..499\u003c/a\u003e (using A181929 b-file).",
				"Diophante, \u003ca href=\"http://www.diophante.fr/problemes-par-themes/arithmetique-et-algebre/a1-pot-pourri/37-a107-binaire-decimal\"\u003eA107, Binaire = décimal\u003c/a\u003e (in French).",
				"Scott R. Shannon, \u003ca href=\"/A341281/a341281.txt\"\u003eThe three numbers for a(770) = 3\u003c/a\u003e."
			],
			"formula": [
				"a(n) \u003e 0 iff length(A305213(n)) = n."
			],
			"example": [
				"a(1) = 2 because 0 and 1 have same expansion in base 2 and in base 10.",
				"a(3) = 1 because 110_10 = 1101110_2 and 110 is the only 3-digit integer with this property.",
				"a(4) = 0 because there is no 4-digit number whose binary expansion's first 4 digits are the base-10 digits of m. Indeed, the 5-digit integer 10010_10 = 10011100011010_2 is the smallest integer whose binary and decimal expansions coincide through the first 4 digits (1001), but 10010 is not a 4-digit integer.",
				"a(12) = 2 because 101111000101_10 = 1011110001010101011110110110000100101_2 and 110011001110_10 = 1100110011101001010101010100100010110_2; there are no other 12-digit integers with this property."
			],
			"program": [
				"(PARI) lista(nn) = {my(list = List()); for (n=0, nn, if (n==0, listput(list, n), my(b = binary(n), db = fromdigits(b), bb = binary(db)); if (vector(#b, k, bb[k]) == b, listput(list, db)););); my(lens = apply(x-\u003e#Str(x), list)); vector(vecmax(Vec(lens)), k, #select(x-\u003e(x==k), lens));} \\\\ _Michel Marcus_, Feb 10 2021"
			],
			"xref": [
				"Cf. A181929, A305213."
			],
			"keyword": "nonn,base",
			"offset": "1,1",
			"author": "_Bernard Schott_, Feb 08 2021",
			"references": 3,
			"revision": 35,
			"time": "2021-02-27T20:59:05-05:00",
			"created": "2021-02-13T06:31:26-05:00"
		}
	]
}