{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A323381",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 323381,
			"data": "1,2,2,4,2,4,4,12,2,14,0,8,4,16,0,8,2,8,6,22,4,4,12,12,4,30,0,20,12,8,8,8,2,16,4,16,14,28,12,8,0,12,0,40,8,12,16,12,4,22,2,28,16,24,8,32,0,48,0,4,8,20,8,36,2,16,8,36,8,12,16,8,6,32,0,28",
			"name": "Number of solutions to n = x^2 + 2*y^2 + 5*z^2 + x*z in integers.",
			"comment": [
				"If n\u003c0, then a(n)=0. If n\u003e0, then a(n) is even since (-x, -y, -z) is a solution if (x, y, z) is.",
				"Rouse [2014] conjectures that the ternary quadratic form x^2 + 2y^2 + 5z^2 + xz represents all positive odd integers.",
				"If n == 10, 26, 40, 42 or 58 (mod 64), or n == 160 (mod 256), then a(n) = 0. - _Robert Israel_, Jun 18 2019"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A323381/b323381.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"J. Rouse, \u003ca href=\"http://users.wfu.edu/rouseja/cv/451thm.pdf\"\u003eQuadratic forms representing all odd positive integers\u003c/a\u003e Amer. J. Math, 136 (2014), no. 6, 1693-1745.",
				"K. S. Williams, \u003ca href=\"http://dx.doi.org/10.1080/00029890.2018.1503003\"\u003eEveything You Wanted to Know about ax^2+by^2+cz^2+dt^2 But Were Afraid To Ask\u003c/a\u003e, Amer. Math. Monthly, Vol. 125, No. 9, (2018), 797-810. See page 803."
			],
			"example": [
				"G.f. = 1 + 2*x + 2*x^2 + 4*x^3 + 2*x^4 + 4*x^5 + 4*x^6 + 12*x^7 + ..."
			],
			"maple": [
				"N:= 100: # for a(0)..a(N)",
				"V:= Array(0..N):",
				"for z from 0 to floor(sqrt(4*N/19)) do",
				"  if z = 0 then mz:= 1 else mz:= 2 fi;",
				"  for y from 0 to floor(sqrt((N - 19/4*z^2)/2)) do",
				"    if y = 0 then my:= 1 else my:= 2 fi;",
				"    r:= sqrt(N - 2*y^2 - 19/4*z^2);",
				"    for x from ceil(-r -z/2) to floor(r-z/2) do",
				"      v:= x^2 + 2*y^2 + 5*z^2 + x*z;",
				"      V[v]:= V[v] + my*mz;",
				"    od",
				"  od",
				"od:",
				"convert(V,list); # _Robert Israel_, Jun 18 2019"
			],
			"mathematica": [
				"a[ n_] := Length @ FindInstance[ x^2 + 2 y^2 + 5 z^2 + x z == n, {x, y, z}, Integers, 10^8];"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n==0, 2*qfrep([4, 0, 0; 0, 2, 1; 0, 1, 10], 2*n)[2*n])};"
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Michael Somos_, Jan 12 2019",
			"references": 1,
			"revision": 8,
			"time": "2019-06-19T02:45:30-04:00",
			"created": "2019-01-12T20:41:48-05:00"
		}
	]
}