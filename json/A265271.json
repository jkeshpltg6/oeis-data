{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A265271",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 265271,
			"data": "3,2,1,4,2,8,5,6,9,2,9,9,8,3,4,1,0,7,2,3,4,4,5,6,0,4,4,5,6,3,8,5,9,8,6,7,1,6,9,9,3,1,0,3,6,1,2,1,8,8,6,3,5,8,1,1,9,1,2,4,0,1,8,0,9,9,6,2,1,0,0,5,7,2,7,4,2,8,9,6,4,2,5,5,1,1,3,0,2,1,4,8,9,6,5,3,8,1,6,4,0,8,1,1,9,4,1,1,7,9,6,7,7,6,2,4,9,2,4,7,7,0,0,9,0,4,4,8,7,4,4,9,3,1,9,9,8,6,4,3,7,7,0,8,0,8,8,8,9,6,0,8,1,1,8,2,7,1,8,5,7,9,4,0,6,7,3,2,9,8,9,1,2,7,6,8,4,3,4,4,0,8,1,8,9,4,8,4,5,0,7,5,5,1,3,5,9,0,4,0",
			"name": "Least positive real z such that 1/2 = Sum_{n\u003e=1} {n*z} / 2^n, where {x} denotes the fractional part of x.",
			"comment": [
				"This constant is transcendental.",
				"The rational approximation z ~ 345131297/1073741820 is accurate to over 5 million digits.",
				"This constant is one of 6 solutions to the equation 1/2 = Sum_{n\u003e=1} {n*z}/2^n, where z is in the interval (0,1) - see cross-references for other solutions.",
				"The complement to this constant is given by A265276."
			],
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DevilsStaircase.html\"\u003eDevil's Staircase\u003c/a\u003e.",
				"\u003ca href=\"/index/Tra#transcendental\"\u003eIndex entries for transcendental numbers\u003c/a\u003e"
			],
			"formula": [
				"The constant z satisfies:",
				"(1) 2*z - 1/2 = Sum_{n\u003e=1} [n*z] / 2^n,",
				"(2) 2*z - 1/2 = Sum_{n\u003e=1} 1 / 2^[n/z],",
				"(3) 3/2 - 2*z = Sum_{n\u003e=1} 1 / 2^[n/(1-z)],",
				"(4) 3/2 - 2*z = Sum_{n\u003e=1} [n*(1-z)] / 2^n,",
				"(5) 1/2 = Sum_{n\u003e=1} {n*(1-z)} / 2^n,",
				"where [x] denotes the integer floor function of x."
			],
			"example": [
				"z = 0.321428569299834107234456044563859867169931036121886358119124...",
				"where z satisfies",
				"(0) 1/2 = {z}/2 + {2*z}/2^2 + {3*z}/2^3 + {4*z}/2^4 + {5*z}/2^5 +...",
				"(1) 2*z - 1/2 = [z]/2 + [2*z]/2^2 + [3*z]/2^3 + [4*z]/2^4 + [5*z]/2^5 +...",
				"(2) 2*z - 1/2 = 1/2^[1/z] + 1/2^[2/z] + 1/2^[3/z] + 1/2^[4/z] + 1/2^[5/z] +...",
				"The continued fraction of the constant z begins:",
				"[0; 3, 8, 1, 599185, 2, 1, 1, 3, 1, 2, ...]",
				"(the next partial quotient has over 5 million digits).",
				"The convergents of the continued fraction of z begin:",
				"[0/1, 1/3, 8/25, 9/28, 5392673/16777205, 10785355/33554438, 16178028/50331643, 26963383/83886081, 97068177/301989886, 124031560/385875967, 345131297/1073741820, ...].",
				"The partial quotients of the continued fraction of 2*z - 1/2 are as follows:",
				"[0; 7, 4793490, 8, ..., Q_n, ...]",
				"where",
				"Q_1 = 2^0*(2^(3*1) - 1)/(2^1 - 1) = 7 ;",
				"Q_2 = 2^1*(2^(8*3) - 1)/(2^3 - 1) = 4793490 ;",
				"Q_3 = 2^3*(2^(1*25) - 1)/(2^25 - 1) = 8 ;",
				"Q_4 = 2^25*(2^(599185*28) - 1)/(2^28 - 1) ;",
				"Q_5 = 2^28*(2^(2*16777205) - 1)/(2^16777205 - 1) = 2^28*(2^16777205 + 1) ;",
				"Q_6 = 2^16777205*(2^(1*33554438) - 1)/(2^33554438 - 1) = 2^16777205 ;",
				"Q_7 = 2^33554438*(2^(1*50331643) - 1)/(2^50331643 - 1) = 2^33554438 ;",
				"Q_8 = 2^50331643*(2^(3*83886081) - 1)/(2^83886081 - 1) ;",
				"Q_9 = 2^83886081*(2^(1*301989886) - 1)/(2^301989886 - 1) ;",
				"Q_10 = 2^301989886*(2^(2*385875967) - 1)/(2^385875967 - 1) ; ...",
				"These partial quotients can be calculated from the simple continued fraction of z and the denominators in the convergents of the continued fraction of z; see the Mathworld link entitled \"Devil's Staircase\" for more details."
			],
			"xref": [
				"Cf. A265272, A265273, A265274, A265275, A265276."
			],
			"keyword": "nonn,cons",
			"offset": "0,1",
			"author": "_Paul D. Hanna_, Dec 08 2015",
			"references": 5,
			"revision": 25,
			"time": "2019-05-14T21:21:24-04:00",
			"created": "2015-12-08T22:34:41-05:00"
		}
	]
}