{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A256279",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 256279,
			"data": "1,1,0,0,-1,0,0,0,0,-4,-2,0,0,2,0,0,-1,0,4,0,0,0,0,0,0,3,0,0,0,0,0,0,0,0,-2,0,4,2,0,0,-2,0,0,0,0,-8,0,0,0,1,0,0,-2,0,0,0,0,0,-2,0,0,2,0,0,-1,0,0,0,0,0,0,0,4,2,0,0,0,0,0,0,0,-4,-2",
			"name": "Expansion of psi(q) * chi(-q^3) * phi(-q^9) in powers of q where phi(), psi(), chi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A256279/b256279.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of eta(q^2)^2 * eta(q^3) * eta(q^9)^2 / (eta(q) * eta(q^6) * eta(q^18)) in powers of q.",
				"Euler transform of period 18 sequence [ 1, -1, 0, -1, 1, -1, 1, -1, -2, -1, 1, -1, 1, -1, 0, -1, 1, -2, ...].",
				"a(n) = (-1)^n * A256269(n). a(4*n) = A256269(n).",
				"a(3*n + 2) = a(4*n + 3) = 0. a(3*n + 1) = A258277(n). a(6*n + 4) = - A122856(n). a(12*n + 1) = A002175(n). a(12*n + 4) = - A122865(n)."
			],
			"example": [
				"G.f. = 1 + q - q^4 - 4*q^9 - 2*q^10 + 2*q^13 - q^16 + 4*q^18 + 3*q^25 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 2, 0, q^(1/2)] / (2 q^(1/8)) QPochhammer[ q^3, q^6] EllipticTheta[ 4, 0, q^9], {q, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n==0, (-1)^(n\\3) * (n%3\u003c2) * sumdiv(n, d, [0, 1, 2, -1][d%4 + 1] * if(d%9, 1, 4) * (-1)^((d%8==6) + n+d)))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^2 * eta(x^3 + A) * eta(x^9 + A)^2 / (eta(x + A) * eta(x^6 + A) * eta(x^18 + A)), n))};"
			],
			"xref": [
				"Cf. A002175, A122856, A122865, A256269, A258277."
			],
			"keyword": "sign",
			"offset": "0,10",
			"author": "_Michael Somos_, Jun 02 2015",
			"references": 1,
			"revision": 11,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2015-06-02T20:07:03-04:00"
		}
	]
}