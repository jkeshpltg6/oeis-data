{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001705",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1705,
			"id": "M3944 N1625",
			"data": "0,1,5,26,154,1044,8028,69264,663696,6999840,80627040,1007441280,13575738240,196287356160,3031488633600,49811492505600,867718162483200,15974614352793600,309920046408806400,6320046028584960000,135153868608460800000,3024476051557847040000",
			"name": "Generalized Stirling numbers: a(n) = n! * Sum_{k=0..n-1} (k+1)/(n-k).",
			"comment": [
				"Partial sum of first n harmonic numbers multiplied by n!: a(n) = n!*Sum[Sum[1/k, {k,1,m}], {m,1,n}] = n!*Sum[H(m), {m,1,n}], where H(m) = Sum[1/k, {k,1,m}] = A001008(m)/A002805(m) is m-th Harmonic number.",
				"Contribution from _Emeric Deutsch_, Sep 22 2008: (Start)",
				"a(n) is also the sum of the positions of the right-to-left minima in all permutations of [n]. Example: a(3)=26 because the positions of the right-to-left minima in the permutations 123,132,213,231,312 and 321 are 123, 13, 23, 3, 23 and 3, respectively and 1 + 2 + 3 + 1 + 3 + 2 + 3 + 3 + 2 + 3 + 3 = 26.",
				"a(n) = Sum_{k=n..n*(n+1)/2} k*A143947(n,k). (End)",
				"The asymptotic expansion of the higher order exponential integral E(x,m=2,n=2) ~ exp(-x)/x^2*(1 - 5/x + 26/x^2 - 154/x^3 + 1044/x^4 - 8028/x^5 + 69264/x^6 - ...) leads to the sequence given above. See A163931 and A028421 for more information. - _Johannes W. Meijer_, Oct 20 2009",
				"a(n) is the total number of cycles (excluding fixed points) in all permutations of [n+1]. [_Olivier Gérard_, Oct 23 2012; Dec 31 2012]",
				"A length n sequence is formed by randomly selecting (one-by-one) n real numbers in (0,1).  a(n)/(n+1)! is the expected value of the sum of the new maximums in such a sequence.  For example for n=3: If we select (in this order): 0.591996, 0.646474, 0.163659  we would add 0.591996 + 0.646474 which would be a bit above the average of a(3)/4! = 26/24. - _Geoffrey Critzer_, Oct 17 2013"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A001705/b001705.txt\"\u003eTable of n, a(n) for n = 0..445\u003c/a\u003e (terms 0 through 100 were provided by T. D. Noe)",
				"J.-L. Baril and S. Kirgizov, \u003ca href=\"http://jl.baril.u-bourgogne.fr/Stirling.pdf\"\u003eThe pure descent statistic on permutations\u003c/a\u003e, preprint, 2016.",
				"J.-L. Baril and S. Kirgizov, \u003ca href=\"https://doi.org/10.1016/j.disc.2017.06.005\"\u003eThe pure descent statistic on permutations\u003c/a\u003e, Discrete Mathematics, 340(10) (2017), 2550-2558.",
				"Jean-Luc Baril and Sergey Kirgizov, \u003ca href=\"https://arxiv.org/abs/2101.01928\"\u003eTransformation à la Foata for special kinds of descents and excedances\u003c/a\u003e, arXiv:2101.01928 [math.CO], 2021.",
				"Gang Chen, Henrik Johansson, Fei Teng, and Tianheng Wang, \u003ca href=\"https://arxiv.org/abs/2104.12726\"\u003eNext-to-MHV Yang-Mills kinematic algebra\u003c/a\u003e, arXiv:2104.12726 [hep-th], 2021. See p. 46.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=406\"\u003eEncyclopedia of Combinatorial Structures 406\u003c/a\u003e.",
				"D. S. Mitrinovic and M. S. Mitrinovic, \u003ca href=\"http://pefmath2.etf.rs/files/47/77.pdf\"\u003eTableaux d'une classe de nombres reliés aux nombres de Stirling\u003c/a\u003e, Univ. Beograd. Publ. Elektrotehn. Fak. Ser. Mat. Fiz. No. 77 (1962), 1-77.",
				"Robert E. Moritz, \u003ca href=\"/A001701/a001701.pdf\"\u003eOn the sum of products of n consecutive integers\u003c/a\u003e, Univ. Washington Publications in Math., 1 (No. 3, 1926), 44-49. [Annotated scanned copy]",
				"J. Riordan, \u003ca href=\"/A000254/a000254.pdf\"\u003eLetter of 04/11/74\u003c/a\u003e.",
				"J. Riordan, \u003ca href=\"/A001850/a001850_2.pdf\"\u003eLetter, Jul 06 1978\u003c/a\u003e."
			],
			"formula": [
				"E.g.f.: - log (1 - x) / (1 - x)^2. a(n) = (n+1)! * H[ n ] - n*n!, H[ n ] = Sum_{k=1..n} (1/k).",
				"a(n) = A112486(n, 1).",
				"a(n) = a(n-1)*(n+1) + n! = A000254(n+1) - A000142(n+1) = A067176(n+1, 1). [_Henry Bottomley_, Jan 09 2002]",
				"a(n) = Sum_{k=0..n-1} ((-1)^(n-1+k) * (k+1) * 2^k * Stirling1(n, k+1)). - Borislav Crstici (bcrstici(AT)etv.utt.ro), Jan 26 2004",
				"With alternating signs: Ramanujan polynomials psi_2(n, x) evaluated at 0. - _Ralf Stephan_, Apr 16 2004",
				"a(n) = Sum_{k=1..n} (k*StirlingCycle[n+1,k+1]). - _David Callan_, Sep 25 2006",
				"For n\u003e=1, a(n) = Sum_{j=0..n-1} ((-1)^(n-j-1) * 2^j * (j+1) * Stirling1(n,j+1)). - _Milan Janjic_, Dec 14 2008",
				"a(n) = (2*n+1)*a(n-1) - n^2*a(n-2). - _Gary Detlefs_, Nov 27 2009",
				"a(n) = (n+1)!*(h(n+1) - 1) where h(n) is the n-th harmonic number. - _Gary Detlefs_, Dec 18 2009",
				"a(n) = n!*Sum_{k=1..n} (-1)^(k+1)*binomial(n+1,k+1)/k. - _Vladimir Kruchinin_, Oct 10 2016"
			],
			"example": [
				"(1-x)^-2 * (-log(1-x)) = x + 5/2*x^2 + 13/3*x^3 + 77/12*x^4 + ...",
				"Examples: a(6) = 6!*(1/6 + 2/5 + 3/4 + 4/3 + 5/2 + 6/1) = 8028; a(20) = 20!*(1/20 + 2/19 + 3/18 + 4/17 + 5/16 + ... + 16/5 + 17/4 + 18/3 + 19/2 + 20/1) = 135153868608460800000. - _Alexander Adamchuk_, Oct 09 2004",
				"From _Olivier Gérard_, Dec 31 2012: (Start)",
				"The cycle decomposition of all permutations of 4 elements gives the following list: {{{1},{2},{3},{4}}, {{1},{2},{3,4}}, {{1},{2,3},{4}}, {{1},{2,4,3}}, {{1},{2,3,4}}, {{1},{2,4},{3}}, {{1,2},{3},{4}}, {{1,2},{3,4}}, {{1,3,2},{4}},{{1,4,3,2}}, {{1,3,4,2}}, {{1,4,2},{3}}, {{1,2,3},{4}}, {{1,2,4,3}},{{1,3},{2},{4}}, {{1,4,3},{2}}, {{1,3},{2,4}}, {{1,4,2,3}}, {{1,2,3,4}}, {{1,2,4},{3}}, {{1,3,4},{2}}, {{1,4},{2},{3}}, {{1,3,2,4}}, {{1,4},{2,3}}}.",
				"Deleting the fixed points gives the following 26 items: {{3,4}, {2,3}, {2,4,3}, {2,3,4}, {2,4}, {1,2}, {1,2}, {3,4}, {1,3,2}, {1,4,3,2}, {1,3,4,2}, {1,4,2}, {1,2,3}, {1,2,4,3}, {1,3}, {1,4,3}, {1,3}, {2,4}, {1,4,2,3}, {1,2,3,4}, {1,2,4}, {1,3,4}, {1,4}, {1,3,2,4}, {1,4}, {2,3}}. (End)"
			],
			"maple": [
				"a := n-\u003e add((n+1)!/k, k=2..n+1): seq(a(n), n=0..21); # _Zerinvary Lajos_, Jan 22 2008; edited _Johannes W. Meijer_, Nov 28 2012",
				"a := n -\u003e ((n+1)!*(h(n+1)-1)): h := n-\u003e harmonic(n): seq(a(n), n=0..21); # _Gary Detlefs_, Dec 18 2009; corrected by _Johannes W. Meijer_, Nov 28 2012"
			],
			"mathematica": [
				"Table[n!*Sum[Sum[1/k,{k,1,m}], {m,1,n}], {n,0,20}] (* _Alexander Adamchuk_, Apr 14 2006 *)"
			],
			"program": [
				"(Maxima)",
				"a(n):=n!*sum(((-1)^(k+1)*binomial(n+1,k+1))/k,k,1,n); /* _Vladimir Kruchinin_, Oct 10 2016 */",
				"(PARI) for(n=0,25, print1(n!*sum(k=0,n-1,(k+1)/(n-k)), \", \")) \\\\ _G. C. Greubel_, Jan 20 2017"
			],
			"xref": [
				"Cf. A000254 (total number of cycles in permutations, including fixed points).",
				"Cf. A002104 (number of different cycles in permutations, without fixed points).",
				"Cf. A006231 (number of different cycles in permutations, including fixed points).",
				"Cf. A001008, A002805, A006675, A143947.",
				"Related to n!*the k-th successive summation of the harmonic numbers:",
				"  (k=0) A000254, (k=1) A001705, (k=2) A001711, (k=3) A001716,",
				"  (k=4) A001721, (k=5) A051524, (k=6) A051545, (k=7) A051560,",
				"  (k=8) A051562, (k=9) A051564."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Sascha Kurz_, Mar 22 2002",
				"2 cross-refs added by _Olivier Gérard_, Dec 31 2012"
			],
			"references": 47,
			"revision": 132,
			"time": "2021-07-28T13:42:45-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}