{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A293415",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 293415,
			"data": "2,1,8,7,0,7,7,2,3,9,7,1,5,5,9,3,9,7,4,1,9,1,1,8,0,2,0,0,6,7,2,7,2,3,4,7,6,0,3,3,7,2,7,6,9,6,6,8,1,4,2,0,8,6,6,5,0,8,0,6,6,4,3,6,3,5,2,1,1,6,7,2,3,1,7,1,1,3,7,7,5,4,3,8,7,3,2,1,3,6,2,5,7,5,7,3,8,5,8,5,9,5,9,4,3,5,7,8",
			"name": "Decimal expansion of the minimum ripple factor for a seventh-order, reflectionless, Chebyshev filter.",
			"comment": [
				"This is the smallest ripple factor (a constant) for which the prototype elements of the seventh-order generalized reflectionless filter topology (see Morgan, 2017) needs no negative elements. It is also the ripple factor for which the first two and last two Chebyshev prototype parameters (of the canonical ladder, or Cauer, topology) are equal.",
				"Other related sequences in the OEIS are the decimal and continued fraction expansions of the limiting ripple factors for third, fifth, seventh, and ninth order, as well as for the limiting case where the order diverges to infinity. As these ripple factors do approach a common limit very quickly, the sequences for the fifth- and higher-order constants share the same initial terms, to greater length as the order increases.",
				"There are simple radical expressions for the third- and fifth-order constants (see formulas). Further, the third-order constant is a quadratic irrational, thus having a repeating continued fraction expansion. I do not know if such simple expressions or patterns exist for the higher-order constants or the limiting (infinite-order) constant."
			],
			"reference": [
				"M. Morgan, Reflectionless Filters, Norwood, MA: Artech House, pp. 129-132, January 2017."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A293415/b293415.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"formula": [
				"Equals sqrt(exp(4*arctanh(exp(-2*7*arcsinh(sqrt(1/2*sin(Pi/7)tan(Pi/7))))))-1)."
			],
			"example": [
				"0.2187077239..."
			],
			"mathematica": [
				"RealDigits[Sqrt[Exp[4 ArcTanh[Exp[-2*7*ArcSinh[Sqrt[1/2*Sin[Pi/7] Tan[Pi/7]]]]]] - 1], 10, 100][[1]]"
			],
			"program": [
				"(PARI) sqrt(exp(4*atanh(exp(-2*7*asinh(sqrt(1/2*sin(Pi/7)*tan(Pi/7))))))-1) \\\\ _Michel Marcus_, Oct 16 2017",
				"(MAGMA) R:= RealField(); Sqrt(Exp(4*Argtanh(Exp(-2*7*Argsinh(Sqrt(1/2* Sin(Pi(R)/7)*Tan(Pi(R)/7))))))-1); // _G. C. Greubel_, Feb 15 2018"
			],
			"xref": [
				"Decimal expansions (A020784, A293409, A293415, A293416, A293417) and continued fractions (A040021, A293768, A293769, A293770, A293882) for third-, fifth-, seventh-, ninth-order and the limiting \"infinite-order\" constant, respectively."
			],
			"keyword": "cons,easy,nonn",
			"offset": "0,1",
			"author": "_Matthew A. Morgan_, Oct 15 2017",
			"references": 8,
			"revision": 36,
			"time": "2019-02-22T01:51:47-05:00",
			"created": "2017-10-18T13:43:20-04:00"
		}
	]
}