{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A027448",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 27448,
			"data": "1,15,1,575,65,8,5845,865,175,27,874853,153713,39743,9963,1728,1009743,200403,60333,19153,5368,1000,389919909,84873489,28400079,10419739,3681784,1105000,216000,3449575767,807843807,292420227",
			"name": "Triangle read by rows: 4th power of the lower triangular mean matrix (M[i,j] = 1/i for i \u003c= j).",
			"link": [
				"Robert Israel, \u003ca href=\"/A027448/b027448.txt\"\u003eTable of n, a(n) for n = 1..10011\u003c/a\u003e (rows 1 to 141, flattened)"
			],
			"formula": [
				"Let M be the lower triangular matrix with entries M[i,j] = 1/i for 1\u003c=j\u003c=i, and B = M^4. Then a(i,j) = B(i,j)*lcm(denom(B(i,1)),...,denom(B(i,i))). - _Robert Israel_, Oct 05 2019",
				"That is, the fractions in M^4 are written using the least common denominator before taking the numerators. - _M. F. Hasler_, Nov 05 2019"
			],
			"example": [
				"Table starts:",
				"          1",
				"         15           1",
				"        575          65           8",
				"       5845         865         175          27",
				"     874853      153713       39743        9963        1728",
				"    1009743      200403       60333       19153        5368        1000"
			],
			"maple": [
				"Rows:= 10:",
				"M:= Matrix(Rows,Rows,(i,j) -\u003e `if`(i\u003e=j,1/i,0)):",
				"B:= M^4:",
				"L:= [seq(ilcm(seq(denom(B[i,j]),j=1..i)),i=1..Rows)]:",
				"seq(seq(B[i,j]*L[i],j=1..i),i=1..Rows); # _Robert Israel_, Oct 05 2019"
			],
			"mathematica": [
				"rows = 8; m = Table[ If[j \u003c= i, 1/i, 0], {i, 1, rows}, {j, 1, rows}]; m4 = m.m.m.m; Table[ fracs = m4[[i]]; nums = fracs // Numerator; dens = fracs // Denominator; lcm = LCM @@ dens; Table[ nums[[j]]*lcm/dens[[j]], {j, 1, i}], {i, 1, rows}] // Flatten (* _Jean-François Alcover_, Mar 05 2013 *)"
			],
			"program": [
				"(PARI) A027448_upto(n)={my(M=matrix(n, n, i, j, (j\u003c=i)/i)^4); vector(n,r,M[r,1..r]*denominator(M[r,1..r]))} \\\\ _M. F. Hasler_, Nov 05 2019"
			],
			"xref": [
				"Cf. A027446 (square of M), A027447 (cube of M)."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Olivier Gérard_",
			"ext": [
				"Edited by _Robert Israel_, Oct 05 2019"
			],
			"references": 11,
			"revision": 22,
			"time": "2019-11-06T18:20:44-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}