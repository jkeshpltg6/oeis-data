{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A073252",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 73252,
			"data": "1,2,1,2,4,4,5,6,9,12,13,16,21,26,29,36,46,54,62,74,90,106,122,142,171,200,227,264,311,358,408,470,545,626,709,810,933,1062,1198,1362,1555,1760,1980,2238,2536,2858,3205,3602,4063,4560,5092,5704,6400,7150,7966",
			"name": "Coefficients of replicable function number \"48g\".",
			"comment": [
				"Old name was: McKay-Thompson series of class 48g for the Monster group.",
				"Number of partitions of n into distinct odd parts of two kinds. [_Joerg Arndt_, Jul 30 2011]",
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Combinatorial interpretation of sequence: [ X1,X2 ] = 2 strictly increasing sequences (possibly null) of odd positive integers; a(n)=#pairs with sum of entries = n."
			],
			"reference": [
				"T. J. I'a. Bromwich, Introduction to the Theory of Infinite Series, Macmillan, 2nd. ed. 1949, p. 116, q_2^2."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A073252/b073252.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"D. Foata and G.-N. Han, \u003ca href=\"http://www-irma.u-strasbg.fr/~foata/paper/pub81.html\"\u003eJacobi and Watson Identities Combinatorially Revisited\u003c/a\u003e",
				"D. Ford, J. McKay and S. P. Norton, \u003ca href=\"http://dx.doi.org/10.1080/00927879408825127\"\u003eMore on replicable functions\u003c/a\u003e, Commun. Algebra 22, No. 13, 5175-5193 (1994).",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"\u003ca href=\"/index/Mat#McKay_Thompson\"\u003eIndex entries for McKay-Thompson series for Monster simple group\u003c/a\u003e"
			],
			"formula": [
				"G.f.: 1 / (Prod_{k\u003e0} 1 + (-x)^k)^2 = (Prod_{k\u003e0} 1 + x^(2*k - 1))^2.",
				"Expansion of q^(1/12) * (eta(q^2)^2 / (eta(q) * eta(q^4)))^2 in powers of q.",
				"Expansion of chi(q)^2 = phi(q) / f(-q^2) = f(q) / psi(-q) = (phi(q) / f(q))^2 = (psi(q) / f(-q^4))^2 = (f(-q^2) / psi(-q))^2 = (phi(-q^2) / f(-q))^2 = (f(q) / f(-q^2))^2 in powers of q where phi(), psi(), chi(), f() are Ramanujan theta functions.",
				"Euler transform of period 4 sequence [2, -2, 2, 0, ...].",
				"Convolution square of A000700. A022597(n) = (-1)^n * a(n).",
				"a(n) ~ exp(Pi*sqrt(n/3)) / (2^(3/2) * 3^(1/4) * n^(3/4)). - _Vaclav Kotesovec_, Aug 27 2015",
				"G.f.: exp(2*Sum_{k\u003e=1} x^k/(k*(1 - (-x)^k))). - _Ilya Gutkovskiy_, Jun 07 2018",
				"a(2*n) = A226622(n). a(2*n + 1) = 2 * A226635(n). - _Michael Somos_, Nov 03 2019"
			],
			"example": [
				"a(4)=4:[ (1),(3) ],[ (3),(1) ],[ (),(1,3) ],[ (1,3),() ]",
				"G.f. = 1 + 2*x + x^2 + 2*x^3 + 4*x^4 + 4*x^5 + 5*x^6 + 6*x^7 + 9*x^8 + 12*x^9 + ...",
				"G.f. = 1/q + 2*q^11 + q^23 + 2*q^35 + 4*q^47 + 4*q^59 + 5*q^71 + 6*q^83 +..."
			],
			"mathematica": [
				"nmax = 50; CoefficientList[Series[Product[(1 + x^(2*k+1))^2, {k, 0, nmax}], {x, 0, nmax}], x] (* _Vaclav Kotesovec_, Aug 27 2015 *)",
				"QP = QPochhammer; s = (QP[q^2]^2 / (QP[q] * QP[q^4]))^2 + O[q]^60; CoefficientList[s, q] (* _Jean-François Alcover_, Nov 14 2015, adapted from PARI *)",
				"a[ n_] := SeriesCoefficient[ QPochhammer[ -x, x^2]^2, {x, 0, n}]; (* _Michael Somos_, Nov 03 2019 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, polcoeff( prod( i=1, (1+n)\\2, 1 + x^(2*i - 1), 1 + x * O(x^n))^2, n))};",
				"(PARI) {a(n) = if( n\u003c0, 0, polcoeff( 1 / prod( i=1, n, 1 + (-x)^i, 1 + x * O(x^n))^2, n))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x*O(x^n); polcoeff( (eta(x^2 + A)^2 / eta(x + A) / eta(x^4 + A))^2, n))};"
			],
			"xref": [
				"Cf. A000700, A022597."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Michael Somos_, Jul 22 2002",
			"ext": [
				"Comments from _Len Smiley_.",
				"New name from _Michael Somos_, Nov 03 2019"
			],
			"references": 7,
			"revision": 50,
			"time": "2021-03-12T22:24:42-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}