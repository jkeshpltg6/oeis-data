{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000498",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 498,
			"id": "M5188 N2255",
			"data": "1,26,302,2416,15619,88234,455192,2203488,10187685,45533450,198410786,848090912,3572085255,14875399450,61403313100,251732291184,1026509354985,4168403181210,16871482830550,68111623139600",
			"name": "Eulerian numbers (Euler's triangle: column k=4 of A008292, column k=3 of A173018)",
			"comment": [
				"There are 2 versions of Euler's triangle:",
				"* A008292 Classic version of Euler's triangle used by Comtet (1974).",
				"* A173018 Version of Euler's triangle used by Graham, Knuth and Patashnik in Concrete Math. (1990).",
				"Euler's triangle rows and columns indexing conventions:",
				"* A008292 The rows and columns of the Eulerian triangle are both indexed starting from 1. (Classic version: used in the classic books by Riordan and Comtet.)",
				"* A173018 The rows and columns of the Eulerian triangle are both indexed starting from 0.(Graham et al.)",
				"Number of permutations of n letters with exactly 3 descents."
			],
			"reference": [
				"L. Comtet, \"Permutations by Number of Rises; Eulerian Numbers.\" §6.5 in Advanced Combinatorics: The Art of Finite and Infinite Expansions, rev. enl. ed. Dordrecht, Netherlands: Reidel, pp. 51 and 240-246, 1974.",
				"L. Comtet, Advanced Combinatorics, Reidel, 1974, p. 243.",
				"F. N. David and D. E. Barton, Combinatorial Chance. Hafner, NY, 1962, p. 151.",
				"F. N. David, M. G. Kendall and D. E. Barton, Symmetric Function and Allied Tables, Cambridge, 1966, p. 260.",
				"J. Riordan, An Introduction to Combinatorial Analysis, Wiley, 1958, p. 215.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A000498/b000498.txt\"\u003eTable of n, a(n) for n = 4..200\u003c/a\u003e",
				"E. Banaian, S. Butler, C. Cox, J. Davis, J. Landgraf and S. Ponce \u003ca href=\"http://arxiv.org/abs/1508.03673\"\u003eA generalization of Eulerian numbers via rook placements\u003c/a\u003e, arXiv:1508.03673 [math.CO], 2015.",
				"L. Carlitz et al., \u003ca href=\"http://dx.doi.org/10.1016/S0021-9800(66)80057-1\"\u003ePermutations and sequences with repetitions by number of increases\u003c/a\u003e, J. Combin. Theory, 1 (1966), 350-374.",
				"F. N. Castro, O. E. González, L. A. Medina, \u003ca href=\"http://emmy.uprrp.edu/lmedina/papers/eulerian/EulerianFinal.pdf\"\u003eThe p-adic valuation of Eulerian numbers: trees and Bernoulli numbers\u003c/a\u003e, 2014.",
				"E. T. Frankel, \u003ca href=\"/A000217/a000217_1.pdf\"\u003e A calculus of figurate numbers and finite differences\u003c/a\u003e, American Mathematical Monthly, 57 (1950), 14-25. [Annotated scanned copy]",
				"J. C. P. Miller, \u003ca href=\"/A002439/a002439_1.pdf\"\u003eLetter to N. J. A. Sloane, Mar 26 1971\u003c/a\u003e",
				"Nagatomo Nakamura, \u003ca href=\"http://libir.josai.ac.jp/il/user_contents/02/G0000284repository/pdf/JOS-13447777-0808.pdf\"\u003ePseudo-Normal Random Number Generation via the Eulerian Numbers\u003c/a\u003e, Josai Mathematical Monographs, vol. 8, p 85-95, 2015.",
				"P. A. Piza, \u003ca href=\"http://www.jstor.org/stable/3029339\"\u003eKummer numbers\u003c/a\u003e, Mathematics Magazine, 21 (1947/1948), 257-260.",
				"P. A. Piza, \u003ca href=\"/A001117/a001117.pdf\"\u003eKummer numbers\u003c/a\u003e, Mathematics Magazine, 21 (1947/1948), 257-260. [Annotated scanned copy]",
				"J. Riordan, \u003ca href=\"/A000217/a000217_2.pdf\"\u003eReview of Frankel (1950)\u003c/a\u003e [Annotated scanned copy]",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/EulerianNumber.html\"\u003eEulerian Number\u003c/a\u003e",
				"R. G. Wilson, V, \u003ca href=\"/A007347/a007347.pdf\"\u003eLetter to N. J. A. Sloane, Apr. 1994\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_10\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (20, -175, 882, -2835, 6072, -8777, 8458, -5204, 1848, -288)."
			],
			"formula": [
				"From _Mike Zabrocki_, Nov 12 2004: (Start)",
				"G.f.: x^4*(1 +6*x -43*x^2 +44*x^3 +52*x^4 -72*x^5)/((1-x)^4*(1-2*x)^3*(1-3*x)^2*(1-4*x)).",
				"a(n) = (6*4^n - 6*(n + 1)*3^n + 3*(n)*(n + 1)*2^n - (n - 1)*(n)*(n + 1))/6. (End)",
				"If n\u003e3 is prime, then a(n) == 1 (mod n). A generalization: if a_t(n) denote the number of permutations of n letters with exactly t descents (column t+1 of Euler's triangle A008292), then, for prime n\u003et, we have a(n) == 1 (mod n). - _Vladimir Shevelev_, Sep 26 2010",
				"E.g.f.: exp(x)*(exp(3*x) - (1 + 3*x)*exp(2*x) + 2*(x + 2*x^2/2!)*exp(x) - x^2/2! - x^3/3!). - _Wolfdieter Lang_, Apr 17 2017"
			],
			"example": [
				"There is one permutation of 4 with exactly 3 descents (4321).",
				"There are 26 permutations of 5 with 3 descents: 15432, 21543, 25431, 31542, 32154, 32541, 35421, 41532, 42153, 42531, 43152, 43215, 43251, 43521, 45321, 51432, 52143, 52431, 53142, 53214, 53241, 53421, 54132, 54213, 54231, 54312. - Neven Juric, Jan 21 2010."
			],
			"maple": [
				"A000498:=proc(n); 4^n-(n+1)*3^n+1/2*(n)*(n+1)*2^n-1/6*(n-1)*(n)*(n+1); end:"
			],
			"mathematica": [
				"LinearRecurrence[{20, -175, 882, -2835, 6072, -8777, 8458, -5204, 1848, -288}, {1, 26, 302, 2416, 15619, 88234, 455192, 2203488, 10187685, 45533450}, 30] (* _Jean-François Alcover_, Feb 09 2016 *)",
				"Table[(6*4^n - 6*(n + 1)*3^n + 3*(n)*(n + 1)*2^n - (n - 1)*(n)*(n + 1))/6, {n, 4, 50}] (* _G. C. Greubel_, Oct 23 2017 *)"
			],
			"program": [
				"(PARI) for(n=4,50, print1((6*4^n - 6*(n + 1)*3^n + 3*(n)*(n + 1)*2^n - (n - 1)*(n)*(n + 1))/6, \", \")) \\\\ _G. C. Greubel_, Oct 23 2017",
				"(MAGMA) [(6*4^n - 6*(n + 1)*3^n + 3*(n)*(n + 1)*2^n - (n - 1)*(n)*(n + 1))/6: n in [4..50]]; // _G. C. Greubel_, Oct 23 2017"
			],
			"xref": [
				"Cf. A008292 (classic version of Euler's triangle used by Comtet (1974).)",
				"Cf. A173018 (version of Euler's triangle used by Graham, Knuth and Patashnik in Concrete Math. (1990).)",
				"Cf. A066912."
			],
			"keyword": "nonn,nice,easy",
			"offset": "4,2",
			"author": "_N. J. A. Sloane_, _Mira Bernstein_, _Robert G. Wilson v_",
			"ext": [
				"More terms from _Christian G. Bower_, May 12 2000"
			],
			"references": 11,
			"revision": 83,
			"time": "2017-11-17T00:51:06-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}