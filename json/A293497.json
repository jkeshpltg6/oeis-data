{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A293497",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 293497,
			"data": "0,1,0,1,2,3,0,1,2,3,4,5,0,1,2,3,4,5,6,7,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,10,11,0,1,2,3,4,5,6,7,8,9,10,11,12,13,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,0,1,2,3,4,5,6,7,8",
			"name": "Triangular array read by rows: row n \u003e= 1 is the list of integers from 0 to 2n-1.",
			"comment": [
				"a(n) = the least nonnegative n - 2 * T, where T is a triangular number.",
				"a(n) = the least nonnegative n - k * (k + 1), where k is a nonnegative integer.",
				"This sequence shares several properties with A053186 (square excess of n):",
				"- same recursion formula a(n) = f(n,1) with f(n,m) = if n \u003c m then n, otherwise f(n-m,m+2);",
				"- same formula pattern a(n) = n - g(floor(f(n))), with f and g each other's inverse function: f(x)=sqrt(x) and g(x)=x^2 in the case of A053186, f(x)=(sqrt(1+4x)-1)/2 and g(x)=x(x+1) in the case of this sequence;",
				"- similar graphic representation (arithmetically increasing sawtooth shape);",
				"- both sequences appear to intertwine into A288969.",
				"Odd-indexed rows of A002262. - _Omar E. Pol_, Oct 10 2017"
			],
			"formula": [
				"a(n) = n - g(floor(f(n))), with f(x) = (sqrt(1+4x)-1)/2 and g(x) = x(x+1).",
				"a(n) = f(n,1) with f(n,m) = if n \u003c m then n, otherwise f(n-m,m+2).",
				"a(n) = t - t^2 + n, where t = floor(sqrt(n+1) + 1/2). - _Ridouane Oudra_, May 03 2019"
			],
			"example": [
				"Triangle begins:",
				"0, 1;",
				"0, 1, 2, 3;",
				"0, 1, 2, 3, 4, 5;",
				"0, 1, 2, 3, 4, 5, 6, 7;",
				"0, 1, 2, 3, 4, 5, 6, 7, 8, 9;",
				"..."
			],
			"mathematica": [
				"FOdd[x_] := x*(x + 1)",
				"InvFOdd[x_] := (Sqrt[1 + 4 x] - 1)/2",
				"GOdd[n_] := n - FOdd[Floor[InvFOdd[n]]]",
				"Table[GOdd[n], {n, 0, 80}]"
			],
			"xref": [
				"Cf. A002262, A053186, A288969."
			],
			"keyword": "nonn,tabf",
			"offset": "0,5",
			"author": "_Luc Rousseau_, Oct 10 2017",
			"references": 1,
			"revision": 26,
			"time": "2019-05-03T21:22:25-04:00",
			"created": "2017-10-22T01:44:32-04:00"
		}
	]
}