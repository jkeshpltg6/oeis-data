{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A163940",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 163940,
			"data": "1,1,0,1,2,0,1,5,3,0,1,9,17,4,0,1,14,52,49,5,0,1,20,121,246,129,6,0,1,27,240,834,1039,321,7,0,1,35,428,2250,5037,4083,769,8,0,1,44,707,5214,18201,27918,15274,1793,9,0,1,54,1102,10829,54111,133530,145777,55152,4097,10,0",
			"name": "Triangle related to the divergent series 1^m*1! - 2^m*2! + 3^m*3! - 4^m*4! + ... for m \u003e= -1.",
			"comment": [
				"The divergent series g(x,m) = sum((-1)^(k+1)*k^m*k!*x^k, k= 1..infinity), m \u003e= -1, are related to the higher order exponential integrals E(x,m,n=1), see A163931.",
				"Hardy, see the link below, describes how Euler came to the rather surprising conclusion that g(x,-1) = exp(1/x)*Ei(1,1/x) with Ei(1,x) = E(x,m=1,n=1). From this result it follows inmediately that g(x,0) = 1 - g(x,-1). Following in Euler's footsteps we discovered that g(x,m) = (-1)^(m) * (M(x,m)*x - ST(x,m)* Ei(1,1/x) * exp(1/x))/x^(m+1), m =\u003e -1.",
				"So g(x=1,m) = (-1)^m*(A040027(m) - A000110 (m+1)*A073003), with A040027(m = -1) = 0. We observe that A073003 = - exp(1)*Ei(-1) is Gompertz's constant, A000110 are the Bell numbers and A040027 was published a few years ago by Gould.",
				"The polynomial coefficients of the M(x,m) = sum(a(m,k) * x^k, k = 0..m), for m \u003e= 0 lead to the triangle given above. We point out that M(x,m=-1) = 0.",
				"The polynomial coefficients of the ST(x,m) = sum(S2(m+1, k) * x^k, k = 0..m+1), m \u003e= -1, lead to the Stirling numbers of the second kind, see A106800.",
				"The formulas that generate the coefficients of the left hand columns lead to the Minkowski numbers A053657. We have a closer look at them in A163972.",
				"The right hand columns have simple generating functions, see the formulas. We used them in the first Maple program to generate the triangle coefficients (n \u003e= 0 and 0 \u003c= k \u003c= n). The second Maple program calculates the values of g(x,m) for m \u003e= -1, at x=1."
			],
			"link": [
				"G. H. Hardy, \u003ca href=\"http://www.archive.org/texts/flipbook/flippy.php?id=divergentseries033523mbp\"\u003eDivergent Series\u003c/a\u003e, Oxford University Press, 1949. pp. 26-29 and pp. 7-8."
			],
			"formula": [
				"The generating functions of the right hand columns are Gf(p) = 1/((1-(p-1)*x)^2 * product((1-k*x), k=1..p-2)); Gf(1) = 1. For the first right hand column p=1, for the second p=2, etc..",
				"From _Peter Bala_, Jul 23 2013: (Start)",
				"Conjectural explicit formula: T(n,k) = Stirling2(n,n-k) + (n-k)*sum {j = 0..k-1} (-1)^j*Stirling2(n,n+1+j-k)*j! for 0 \u003c= k \u003c= n.",
				"The n-th row polynomial R(n,x) appears to satisfy the recurrence equation R(n,x) = n*x^(n-1) + sum {k = 1..n-1} binomial(n,k+1)*x^(n-k-1)*R(k,x). The row polynomials appear to have only real zeros. (End)"
			],
			"example": [
				"The first few triangle rows are:",
				"[1]",
				"[1, 0]",
				"[1, 2, 0]",
				"[1, 5, 3, 0]",
				"[1, 9, 17, 4, 0]",
				"[1, 14, 52, 49, 5, 0]",
				"The first few M(x,m) are:",
				"M(x,m=0) = 1",
				"M(x,m=1) = 1 + 0*x",
				"M(x,m=2) = 1 + 2*x + 0*x^2",
				"M(x,m=3) = 1 + 5*x + 3*x^2 + 0*x^3",
				"The first few ST(x,m) are:",
				"ST(x,m=-1) = 1",
				"ST(x,m=0) = 1 + 0*x",
				"ST(x,m=1) = 1 + 1*x + 0*x^2",
				"ST(x,m=2) = 1 + 3*x + x^2 + 0*x^3",
				"ST(x,m=3) = 1 + 6*x + 7*x^2 + x^3 + 0*x^4",
				"The first few g(x,m) are:",
				"g(x,-1) = (-1)*(- (1)*Ei(1,1/x)*exp(1/x))/x^0",
				"g(x,0) = (1)*((1)*x - (1)*Ei(1,1/x)*exp(1/x))/x^1",
				"g(x,1) = (-1)*((1)*x - (1+ x)*Ei(1,1/x)*exp(1/x))/x^2",
				"g(x,2) = (1)*((1+2*x)*x - (1+3*x+x^2)*Ei(1,1/x)*exp(1/x))/x^3",
				"g(x,3) = (-1)*((1+5*x+3*x^2)*x - (1+6*x+7*x^2+x^3)*Ei(1,1/x)*exp(1/x))/x^4"
			],
			"maple": [
				"nmax := 10; for p from 1 to nmax do Gf(p) := convert(series(1/((1-(p-1)*x)^2*product((1-k1*x), k1=1..p-2)), x, nmax+1-p), polynom); for q from 0 to nmax-p do a(p+q-1, q) := coeff(Gf(p), x, q) od: od: seq(seq(a(n, k), k=0..n), n=0..nmax-1);",
				"# End program 1",
				"nmax1:=nmax; A040027 := proc(n): if n = -1 then 0 elif n= 0 then 1 else add(binomial(n, k1-1)*A040027(n-k1), k1 = 1..n) fi: end: A000110 := proc(n) option remember; if n \u003c= 1 then 1 else add( binomial(n-1, i) * A000110(n-1-i), i=0..n-1); fi; end: A073003 := - exp(1) * Ei(-1): for n from -1 to nmax1 do g(1, n) := (-1)^n * (A040027(n) - A000110(n+1) * A073003) od;",
				"# End program 2"
			],
			"mathematica": [
				"nmax = 11;",
				"For[p = 1, p \u003c= nmax, p++, gf = 1/((1-(p-1)*x)^2*Product[(1-k1*x), {k1, 1, p-2}]) + O[x]^(nmax-p+1) // Normal; For[q = 0, q \u003c= nmax-p, q++, a[p+q-1, q] = Coefficient[gf, x, q]]];",
				"Table[a[n, k], {n, 0, nmax-1}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Nov 02 2019, from 1st Maple program *)"
			],
			"xref": [
				"The row sums equal A040027 (Gould).",
				"A000007, A000027, A000337, A163941 and A163942 are the first five right hand columns.",
				"A000012, A000096, A163943 and A163944 are the first four left hand columns.",
				"Cf. A163931, A163972, A106800 (Stirling2), A000110 (Bell), A073003 (Gompertz), A053657 (Minkowski)."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,5",
			"author": "_Johannes W. Meijer_, Aug 13 2009",
			"ext": [
				"Edited by _Johannes W. Meijer_, Sep 23 2012"
			],
			"references": 12,
			"revision": 14,
			"time": "2019-11-02T08:46:08-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}