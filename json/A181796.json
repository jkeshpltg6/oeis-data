{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A181796",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 181796,
			"data": "1,2,2,3,2,3,2,4,3,3,2,5,2,3,3,5,2,5,2,5,3,3,2,7,3,3,4,5,2,4,2,6,3,3,3,7,2,3,3,7,2,4,2,5,5,3,2,9,3,5,3,5,2,7,3,7,3,3,2,7,2,3,5,7,3,4,2,5,3,4,2,10,2,3,5,5,3,4,2,9,5,3,2,7,3,3,3,7,2,7,3,5,3,3,3,11,2,5,5,7,2,4,2,7,4",
			"name": "a(n) = number of divisors of n whose canonical prime factorizations contain no repeated positive exponents (cf. A130091).",
			"comment": [
				"The canonical factorization of n into prime powers can be written as Product p(i)^e(i), for example. A host of equivalent notations can also be used (for another example, see Weisstein link). a(n) depends only on prime signature of n (cf. A025487).",
				"a(n) \u003e= A085082(n). (A085082(n) equals the number of members of A025487 that divide A046523(n), and each member of A025487 is divisible by at least one member of A130091 that divides no smaller member of A025487.) a(n) \u003e A085082(n) iff n has in its canonical prime factorization at least two exponents greater than 1.",
				"a(n) = number of such divisors of n that in their prime factorization all exponents are unique. - _Antti Karttunen_, May 27 2017",
				"First differs from A335549 at a(90) = 7, A335549(90) = 8. First differs from A335516 at a(180) = 9, A335516(180) = 10. - _Gus Wiseman_, Jun 28 2020"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A181796/b181796.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PrimeFactorization.html\"\u003e Prime Factorization\u003c/a\u003e",
				"Gus Wiseman, \u003ca href=\"https://docs.google.com/document/d/e/2PACX-1vSX9dPMGJhxB8rOknCGvOs6PiyhupdWNpqLsnphdgU6MEVqFBnWugAXidDhwHeKqZe_YnUqYeGOXsOk/pub\"\u003eSequences counting and encoding certain classes of multisets\u003c/a\u003e"
			],
			"formula": [
				"a(A000079(n)) = a(A002110(n)) = n+1.",
				"a(A006939(n)) = A000110(n+1).",
				"a(A181555(n)) = A002720(n)."
			],
			"example": [
				"12 has a total of six divisors (1, 2, 3, 4, 6 and 12). Of those divisors, the number 1 has no prime factors, hence, no positive exponents at all (and no repeated positive exponents) in its canonical prime factorization. The lists of positive exponents for 2, 3, 4, 6 and 12 are (1), (1), (2), (1,1) and (2,1) respectively (cf. A124010). Of all six divisors, only the number 6 (2^1*3^1) has at least one positive exponent repeated (namely, 1). The other five do not; hence, a(12) = 5.",
				"For n = 90 = 2 * 3^2 * 5, the divisors that satisfy the condition are: 1, 2, 3, 3^2, 5, 2 * 3^2, 3^2 * 5, altogether 7, (but for example 90 itself is not included), thus a(90) = 7."
			],
			"mathematica": [
				"Table[DivisorSum[n, 1 \u0026, Length@ Union@ # == Length@ # \u0026@ FactorInteger[#][[All, -1]] \u0026], {n, 105}] (* _Michael De Vlieger_, May 28 2017 *)"
			],
			"program": [
				"(PARI)",
				"no_repeated_exponents(n) = { my(es = factor(n)[, 2]); if(length(Set(es)) == length(es),1,0); }",
				"A181796(n) = sumdiv(n,d,no_repeated_exponents(d)); \\\\ _Antti Karttunen_, May 27 2017",
				"(Python)",
				"from sympy import factorint, divisors",
				"def ok(n):",
				"    f=factorint(n)",
				"    ex=[f[i] for i in f]",
				"    for i in ex:",
				"        if ex.count(i)\u003e1: return 0",
				"    return 1",
				"def a(n): return sum([1 for i in divisors(n) if ok(i)]) # _Indranil Ghosh_, May 27 2017"
			],
			"xref": [
				"Diverges from A088873 at n=24 and from A085082 at n=36. a(36) = 7, while A085082(36) = 6.",
				"Partitions with distinct multiplicities are A098859.",
				"Sorted prime signature is A118914.",
				"Unsorted prime signature is A124010.",
				"a(n) is the number of divisors of n in A130091.",
				"Factorizations with distinct multiplicities are A255231.",
				"The largest of the counted divisors is A327498.",
				"Factorizations using the counted divisors are A327523.",
				"Cf. A000005, A007916, A045778, A056239, A212168, A327498, A335519, A335549."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Matthew Vandermast_, Nov 22 2010",
			"references": 73,
			"revision": 30,
			"time": "2020-07-30T06:52:26-04:00",
			"created": "2010-11-13T14:07:27-05:00"
		}
	]
}