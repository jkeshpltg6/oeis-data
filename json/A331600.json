{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A331600",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 331600,
			"data": "1,1,1,2,1,2,1,3,2,2,1,3,1,2,4,3,1,4,1,3,4,2,1,3,2,2,3,3,1,4,1,5,4,2,4,3,1,2,4,3,1,4,1,3,7,2,1,5,2,12,4,3,1,3,8,3,4,2,1,3,1,2,7,5,8,4,1,3,4,12,1,5,1,2,4,3,4,4,1,5,3,2,1,3,8,2,4,3,1,3,8,3,4,2,8,5,1,16,7,3,1,4,1,3,18",
			"name": "a(n) = A002487(A331595(n)).",
			"link": [
				"Antti Karttunen, \u003ca href=\"/A331600/b331600.txt\"\u003eTable of n, a(n) for n = 1..16384\u003c/a\u003e",
				"Antti Karttunen, \u003ca href=\"/A331600/a331600.txt\"\u003eData supplement: n, a(n) computed for n = 1..65537\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#prime_indices\"\u003eIndex entries for sequences computed from indices in prime factorization\u003c/a\u003e",
				"\u003ca href=\"/index/St#Stern\"\u003eIndex entries for sequences related to Stern's sequences\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A002487(A331595(n)) = A002487(gcd(A122111(n), A241909(n))).",
				"a(n) = A002487(A331731(n))."
			],
			"mathematica": [
				"Array[If[# == 1, 1, NestWhile[If[OddQ[#3], {#1, #1 + #2, #4}, {#1 + #2, #2, #4}] \u0026 @@ Append[#, Floor[#[[-1]]/2]] \u0026, {1, 0, #}, #[[-1]] \u003e 0 \u0026][[2]] \u0026@ Apply[GCD, {Block[{k = #, m = 0}, Times @@ Power @@@ Table[k -= m; k = DeleteCases[k, 0]; {Prime@ Length@ k, m = Min@ k}, Length@ Union@ k]] \u0026@ Catenate[ConstantArray[PrimePi[#1], #2] \u0026 @@@ #], Function[t, Times @@ Prime@ Accumulate[If[Length@ t \u003c 2, {0}, Join[{1}, ConstantArray[0, Length@ t - 2], {-1}]] + ReplacePart[t, Map[#1 -\u003e #2 \u0026 @@ # \u0026, #]]]]@ ConstantArray[0, Transpose[#][[1, -1]]] \u0026[# /. {p_, e_} /; p \u003e 0 :\u003e {PrimePi@ p, e}]}] \u0026@ FactorInteger[#]] \u0026, 105] (* _Michael De Vlieger_, Jan 25 2020, after _JungHwan Min_ at A122111 *)"
			],
			"program": [
				"(PARI)",
				"A002487(n) = { my(a=1, b=0); while(n\u003e0, if(bitand(n, 1), b+=a, a+=b); n\u003e\u003e=1); (b); }; \\\\ From A002487",
				"A064989(n) = {my(f); f = factor(n); if((n\u003e1 \u0026\u0026 f[1,1]==2), f[1,2] = 0); for (i=1, #f~, f[i,1] = precprime(f[i,1]-1)); factorback(f)};",
				"A122111(n) = if(1==n,n,prime(bigomega(n))*A122111(A064989(n)));",
				"A241909(n) = if(1==n||isprime(n),2^primepi(n),my(f=factor(n),h=1,i,m=1,p=1,k=1); while(k\u003c=#f~, p = nextprime(1+p); i = primepi(f[k,1]); m *= p^(i-h); h = i; if(f[k,2]\u003e1, f[k,2]--, k++)); (p*m));",
				"A331595(n) = gcd(A122111(n), A241909(n));",
				"A331600(n) = A002487(A331595(n));"
			],
			"xref": [
				"Cf. A002487, A122111, A241909, A331595, A331731.",
				"Cf. also A323901, A323902, A323903, A331601."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Antti Karttunen_, Jan 22 2020",
			"references": 5,
			"revision": 15,
			"time": "2020-01-26T20:56:12-05:00",
			"created": "2020-01-25T20:53:20-05:00"
		}
	]
}