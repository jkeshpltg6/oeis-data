{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A079404",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 79404,
			"data": "0,1,1,3,4,3,3,5,7,9,10,9,10,12,14,13,13,15,17,19,19",
			"name": "Let G(n) be the set of numbers between 2^(n-1) and 2^n-1, inclusive. There is a unique number m(n) in G(n) so that the denominator of the m(n)-th partial sum of the double harmonic series is divisible by smaller 2-power than that of others in G(n). This power is defined to be a(n).",
			"comment": [
				"The sequence is conjectured to go to positive infinity."
			],
			"reference": [
				"Partial sums of multiple zeta value series II: finiteness of p-divisible sets."
			],
			"link": [
				"J. Zhao, \u003ca href=\"https://arxiv.org/abs/math/0303043\"\u003ePartial sums of multiple zeta value series II: finiteness of p-divisible sets\u003c/a\u003e, arXiv:math/0303043 [math.NT], 2003-2010. See (24) p. 11."
			],
			"example": [
				"a(3)=1 because G(3)={4,5,6,7} and among Sum_{1 \u003c= k \u003c l \u003c= 4} 1/(kl) = 35/24, Sum_{1 \u003c= k \u003c l \u003c= 5} 1/(kl) = 15/8, Sum_{1 \u003c= k \u003c l \u003c= 6} 1/(kl) = 203/90, Sum_{1 \u003c= k \u003c l \u003c= 7} 1/(kl) = 469/180, 90 has the smallest 2-power factor among the denominators."
			],
			"maple": [
				"sequ := proc(T) local b,counter,A,n,t,psum,innersum; psum := 0; innersum := 0; A := array(1..T-1); for t to T-1 do for n from 2^(t) to 2^(t+1)-1 do innersum := innersum+1/(n-1); psum := psum+innersum/n; if 2^(2*t)*psum mod 2^(2*t+1)=0 then print(`The conjecture that 2 never divides the numerators of partial sums of double harmonic series is wrong.`); else b := 0; counter := 2*t; while b=0 do b := 2^counter*psum mod 2; counter := counter-1; od; if counter\u003ct-1 then A[t] := counter+1: end if; end if; od; od; RETURN(eval(A)): end:"
			],
			"mathematica": [
				"nmax = 15; dhs = Array[HarmonicNumber[# - 1 ]/# \u0026, 2^nmax] // Accumulate; Print[\"dhs finished\"];",
				"f[s_] := IntegerExponent[s // Denominator, 2];",
				"a[n_] := Table[{f[dhs[[k]] ], k}, {k, 2^(n - 1), 2^n - 1}] // Sort // First // First;",
				"Table[an = a[n]; Print[\"a(\", n, \") = \", an]; an, {n, 2, nmax}] (* _Jean-François Alcover_, Jan 22 2018 *)"
			],
			"xref": [
				"Cf. A079403."
			],
			"keyword": "nonn",
			"offset": "2,4",
			"author": "Jianqiang Zhao (jqz(AT)math.upenn.edu), Jan 06 2003",
			"ext": [
				"Typo in data corrected by _Jean-François Alcover_, Jan 22 2018"
			],
			"references": 1,
			"revision": 20,
			"time": "2018-01-22T13:26:32-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}