{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A223549",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 223549,
			"data": "1,3,1,21,15,3,77,43,35,5,1155,885,1095,315,35,4389,8589,7161,777,693,63,33649,80353,42245,12285,16485,3003,231,129789,91635,233001,170145,152625,20889,6435,429,4023459,3283533,9804465,8625375,9695565,1772199,819819,109395,6435,15646785,58019335,49782755,25638305,69324255,31726695,9794785,245245,230945,12155",
			"name": "Triangle T(n,k), read by rows, giving the numerator of the coefficient of x^k in the Boros-Moll polynomial P_n(x) for n \u003e= 0 and 0 \u003c= k \u003c=n.",
			"comment": [
				"From _Petros Hadjicostas_, May 21 2020: (Start)",
				"Let P_n(x) = Sum_{k=0..n} (T(n,k)/A223550(n,k))*x^k be the Boros-Moll polynomial. It follows from the theory in Comtet (1967, pp. 81-83 and 85-86) that the polynomial Q_n(x) = 2^n*n!*P_n(x) has integer coefficients and satisfies the recurrence (x-1)*Q_n(x) = 2*(2*n - 1)*(x^2 - 2)*Q_{n-1}(x) + (16*(n-1)^2 - 1)*(x + 1)*Q_{n-2}(x).",
				"We have integral_{y = 0..infinity} dy/(y^4 + 2*x*y + 1)^(n + 1) = Pi * P_n(x)/(2^(n + (3/2)) * (x + 1)^(n + (1/2))) = Pi * Q_n(x)/(2^(2*n + (3/2)) * n! * (x + 1)^(n + (1/2))) for x \u003e -1 and n integer \u003e= 0.",
				"It also follows from the theory in Comtet (1967, pp. 81-83) that g(t) = (sqrt(x + sqrt(x^2 - 1 + t)) - sqrt(x - sqrt(x^2 - 1 + t))) / sqrt((1 - t) * (x^2 - 1 + t)) = Sum_{n \u003e= 0} t^n * P_n(x)/(2^(n - (1/2)) * (x + 1)^(n + (1/2))) for x \u003e= 1 and 0 \u003c= t \u003c 1.",
				"From Comtet's result, we get g(t)^2 = 2*(x - sqrt(1-t))/((1-t) * (x^2 - 1 + t)) = 2/((1-t) * (x + sqrt(1-t))) = Sum_{n \u003e= 0} (Sum_{k=0..n} P_k(x) * P_{n-k}(x)) / (2^(n-1) * (x+1)^(n+1)) * t^n for 0 \u003c= t \u003c 1 and x \u003e 1. (End)"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A223549/b223549.txt\"\u003eRows n = 0..50, flattened\u003c/a\u003e",
				"Tewodros Amdeberhan and Victor H. Moll, \u003ca href=\"http://arxiv.org/abs/0707.2118\"\u003e A formula for a quartic integral: a survey of old proofs and some new ones\u003c/a\u003e, arXiv:0707.2118 [math.CA], 2007.",
				"George Boros and Victor H. Moll, \u003ca href=\"http://dx.doi.org/10.1016/S0377-0427(99)00081-3\"\u003eAn integral hidden in Gradshteyn and Ryzhik\u003c/a\u003e, Journal of Computational and Applied Mathematics, 106(2) (1999), 361-368.",
				"William Y. C. Chen and Ernest X. W. Xia, \u003ca href=\"http://arxiv.org/abs/0806.4333\"\u003e The Ratio Monotonicity of the Boros-Moll Polynomials\u003c/a\u003e, arXiv:0806.4333 [math.CO], 2009.",
				"William Y. C. Chen and Ernest X. W. Xia, \u003ca href=\"https://doi.org/10.1090/S0025-5718-09-02223-6\"\u003e The Ratio Monotonicity of the Boros-Moll Polynomials\u003c/a\u003e, Mathematics of Computation, 78(268) (2009), 2269-2282.",
				"Louis Comtet, \u003ca href=\"https://www.jstor.org/stable/43667287\"\u003eFonctions génératrices et calcul de certaines intégrales\u003c/a\u003e, Publikacije Elektrotechnickog faculteta - Serija Matematika i Fizika, No. 181/196 (1967), 77-87."
			],
			"formula": [
				"T(n,k)/A223550(n,k) =  2^(-2*n)*Sum_{j=k..n} 2^j*binomial(2*n - 2*j, n - j)*binomial(n + j, j)*binomial(j, k) = 2^(-2*n)*A067001(n,n-k) for n \u003e= 0 and k = 0..n.",
				"P_n(x) = Sum_{k=0..n} (T(n, k)/A223550(n,k))*x^k = ((2*n)!/4^n/(n!)^2)*2F1([-n, n + 1], [1/2 - n], (x + 1)/2).",
				"From _Petros Hadjicostas_, May 22 2020: (Start)",
				"Recurrence for the polynomial: 4*n*(n - 1)*(x - 1)*P_n(x) = 4*(2*n - 1)*(n - 1)*(x^2 - 2)*P_{n-1}(x) + (16*(n - 1)^2 - 1)*(x + 1)*P_{n-2}(x).",
				"O.g.f. for P_n(x): sqrt((x + 1)/(1 - 2*(x + 1)*w)/(x + sqrt(1 - 2*(x + 1)*w))). [It follows from Comtet's theory and my comments.]",
				"P_n(1) = Sum_{k=0..n} T(n,k)/A223550(n,k) = A334907(n)/(2^n*n!). (End)"
			],
			"example": [
				"P_3(x) = 77/16 + 43*x/4 + 35*x^2/4 + 5*x^3/2.",
				"As a result, integral_{y = 0..infinity} dy/(y^4 + 2*x*y + 1)^4 = Pi * P_3(x)/(2^(3 + (3/2)) * (x + 1)^(3 + (1/2))) = Pi * (40*x^3 + 140*x^2 + 172*x + 77)/(32 * sqrt(2*(x + 1)^7)) for x \u003e -1. - _Petros Hadjicostas_, May 22 2020",
				"From _Bruno Berselli_, Mar 22 2013: (Start)",
				"Triangle T(n,k) (with rows n \u003e= 0 and columns k = 0..n) begins as follows:",
				"       1;",
				"       3,     1;",
				"      21,    15,      3;",
				"      77,    43,     35,      5;",
				"    1155,   885,   1095,    315,     35;",
				"    4389,  8589,   7161,    777,    693,    63;",
				"   33649, 80353,  42245,  12285,  16485,  3003,  231;",
				"  129789, 91635, 233001, 170145, 152625, 20889, 6435, 429;",
				"  ... (End)"
			],
			"mathematica": [
				"t[n_, k_] := 2^(-2*n)*Sum[ 2^j*Binomial[2*n - 2*j, n-j]*Binomial[n+j, j]*Binomial[j, k], {j, k, n}]; Table[t[n, k] // Numerator, {n, 0, 9}, {k, 0, n}] // Flatten"
			],
			"program": [
				"(MAGMA) /* As triangle: */ [[Numerator(2^(-2*n)*\u0026+[2^j*Binomial(2*n-2*j, n-j)*Binomial(n+j, j)*Binomial(j, k): j in [k..n]]): k in [0..n]]: n in [0..10]]; // _Bruno Berselli_, Mar 22 2013"
			],
			"xref": [
				"Cf. A067001, A223550 (denominators), A334907."
			],
			"keyword": "nonn,easy,frac,tabl",
			"offset": "0,2",
			"author": "_Jean-François Alcover_, Mar 22 2013",
			"ext": [
				"Various sections and name edited by _Petros Hadjicostas_, May 22 2020"
			],
			"references": 6,
			"revision": 58,
			"time": "2020-05-27T02:10:45-04:00",
			"created": "2013-03-22T15:28:32-04:00"
		}
	]
}