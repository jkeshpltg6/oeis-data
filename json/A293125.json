{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A293125",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 293125,
			"data": "1,-1,3,-13,73,-501,4051,-37633,394353,-4596553,58941091,-824073141,12470162233,-202976401213,3535017524403,-65573803186921,1290434218669921,-26846616451246353,588633468315403843,-13564373693588558173,327697927886085654441",
			"name": "Expansion of e.g.f.: exp(-x/(1+x)).",
			"comment": [
				"For n \u003e= 1, gives row sums of A008297, triangle of Lah numbers. - _Daniel Forgues_, Oct 12 2019"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A293125/b293125.txt\"\u003eTable of n, a(n) for n = 0..444\u003c/a\u003e",
				"Richard P. Brent, M. L. Glasser, Anthony J. Guttmann, \u003ca href=\"https://arxiv.org/abs/1812.00316\"\u003eA Conjectured Integer Sequence Arising From the Exponential Integral\u003c/a\u003e, arXiv:1812.00316 [math.NT], 2018."
			],
			"formula": [
				"a(n) = (-1)^n * A000262(n).",
				"From _Vaclav Kotesovec_, Sep 30 2017: (Start)",
				"a(n) = -(2*n-1)*a(n-1) - (n-2)*(n-1)*a(n-2).",
				"a(n) ~ (-1)^n * n^(n-1/4) * exp(-1/2 + 2*sqrt(n) - n) / sqrt(2) * (1 - 5/(48*sqrt(n)) - 95/(4608*n)).",
				"(End)",
				"a(n) = (-1)^n *n! * Sum_{j=0..n-1} binomial(n-1, j)/(j+1)!, for n \u003e 0. - _G. C. Greubel_, Dec 04 2018",
				"a(n) = (-1)^n*n!*hypergeom([1 - n], [2], -1) for n \u003e 0. - _Peter Luschny_, Oct 13 2019"
			],
			"maple": [
				"a:=series(exp(-x/(1+x)),x=0,21): seq(n!*coeff(a,x,n),n=0..20); # _Paolo P. Lava_, Mar 27 2019"
			],
			"mathematica": [
				"CoefficientList[Series[E^(-x/(1+x)), {x, 0, 20}], x] * Range[0, 20]! (* _Vaclav Kotesovec_, Sep 30 2017 *)",
				"a[n_] := If[n == 0, 1, (-1)^n n!  Hypergeometric1F1[1 - n, 2, -1]];",
				"Table[a[n], {n, 0, 20}] (* _Peter Luschny_, Oct 13 2019 )"
			],
			"program": [
				"(PARI) x='x+O('x^66); Vec(serlaplace(exp(-x/(1+x))))",
				"(MAGMA) [1] cat [(-1)^n*Factorial(n)*(\u0026+[Binomial(n-1, j)/Factorial(j+1): j in [0..n-1]]): n in [1..30]]; // _G. C. Greubel_, Dec 04 2018",
				"(Sage) [1] + [(-1)^n*factorial(n)*sum(binomial(n-1,j)/factorial(j+1) for j in (0..n-1)) for n in (1..30)] # _G. C. Greubel_, Dec 04 2018",
				"(GAP) a:=[-1,3];; for n in [3..25] do a[n]:=-(2*n-1)*a[n-1]-(n-2)*(n-1)*a[n-2]; od; Concatenation([1], a); # _G. C. Greubel_, Dec 04 2018"
			],
			"xref": [
				"Column k=0 of A293134.",
				"Cf. A000262, A008297."
			],
			"keyword": "sign",
			"offset": "0,3",
			"author": "_Seiichi Manyama_, Sep 30 2017",
			"references": 4,
			"revision": 31,
			"time": "2019-10-13T14:41:26-04:00",
			"created": "2017-09-30T23:54:27-04:00"
		}
	]
}