{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A214574",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 214574,
			"data": "1,1,1,2,1,2,2,2,2,2,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,3,2,2,2,2,2,2,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,3",
			"name": "The Strahler number of the rooted tree with Matula-Goebel number n.",
			"comment": [
				"The Strahler number of a vertex of a rooted tree is defined recursively in the following way: (i) the Strahler number of a leaf is 1; (ii) if the vertex has one child with Strahler number i and all other children have Strahler number less than i, then the Strahler number of the vertex is again i; (iii) if the vertex has two or more children with Strahler number i and no child with Strahler number greater than i, then the Strahler number of the vertex is i+1. See the Wikipedia reference. The Strahler number of a rooted tree T is defined as the Strahler number of the root of T.",
				"The Matula-Goebel number of a rooted tree can be defined in the following recursive manner: to the one-vertex tree there corresponds the number 1; to a tree T with root degree 1 there corresponds the t-th prime number, where t is the Matula-Goebel number of the tree obtained from T by deleting the edge emanating from the root; to a tree T with root degree m\u003e=2 there corresponds the product of the Matula-Goebel numbers of the m branches of T."
			],
			"link": [
				"E. Deutsch, \u003ca href=\"http://arxiv.org/abs/1111.4288\"\u003eRooted tree statistics from Matula numbers\u003c/a\u003e, arXiv:1111.4288 [math.CO], 2011.",
				"E. Deutsch, \u003ca href=\"http://dx.doi.org/10.1016/j.dam.2012.05.012\"\u003eRooted tree statistics from Matula numbers\u003c/a\u003e, Discrete Appl. Math., 160, 2012, 2314-2322.",
				"F. Goebel, \u003ca href=\"http://dx.doi.org/10.1016/0095-8956(80)90049-0\"\u003eOn a 1-1-correspondence between rooted trees and natural numbers\u003c/a\u003e, J. Combin. Theory, B 29 (1980), 141-143.",
				"I. Gutman and A. Ivic, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(95)00182-V\"\u003eOn Matula numbers\u003c/a\u003e, Discrete Math., 150, 1996, 131-142.",
				"I. Gutman and Yeong-Nan Yeh, \u003ca href=\"http://www.emis.de/journals/PIMB/067/3.html\"\u003eDeducing properties of trees from their Matula numbers\u003c/a\u003e, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"D. W. Matula, \u003ca href=\"http://www.jstor.org/stable/2027327\"\u003eA natural rooted tree enumeration by prime factorization\u003c/a\u003e, SIAM Rev. 10 (1968) 273.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Strahler_number\"\u003eStrahler number\u003c/a\u003e",
				"\u003ca href=\"/index/Mat#matula\"\u003eIndex entries for sequences related to Matula-Goebel numbers\u003c/a\u003e"
			],
			"formula": [
				"Define the Strahler polynomial of a rooted tree T as the generating polynomial of the vertices of T with respect to their Strahler numbers. For example, it follows at once that the Strahler polynomial of the rooted tree V is 2x + x^2. Denote by G(n)=G(n;x) the Strahler polynomial of the rooted tree with Matula-Goebel number n. Clearly, A214573(n,k) is the coefficient of x^k in G(n). We have (i) G(1)= x; (ii) if n=p(t) (the t-th prime), then G(n) = x^{degree(G(t)} + G(t); (iii) if n=rs (r,s\u003e=2), then G(n) = G(r) - degree (G(r)) + G(s) - degree(G(s)  + x^m, where m = 1+degree(G(r)) if degree(G(r))=degree(G(s)) and m = max(degree(G(r), G(s)) otherwise. The Strahler number a(n) = degree(G(n))."
			],
			"example": [
				"a((4)=2 because the rooted tree with Matula-Goebel number 4 is V; the two leaves have Strahler numbers 1,1, and the root has Strahler number 2; this is - by definition - the Strahler number of the tree."
			],
			"maple": [
				"with(numtheory): G := proc (n) local r, s: r := proc (n) options operator, arrow: op(1, factorset(n)) end proc; s := proc (n) options operator, arrow: n/r(n) end proc: if n = 1 then x elif bigomega(n) = 1 then sort(expand(x^degree(G(pi(n)))+G(pi(n)))) elif 1 \u003c bigomega(n) and degree(G(r(n))) \u003c\u003e degree(G(s(n))) then sort(G(r(n))-x^degree(G(r(n)))+G(s(n))-x^degree(G(s(n)))+x^max(degree(G(r(n))), degree(G(s(n))))) else sort(G(r(n))-x^degree(G(r(n)))+G(s(n))-x^degree(G(s(n)))+x^(1+degree(G(r(n))))) end if end proc: seq(degree(G(n)), n = 1 .. 200);"
			],
			"xref": [
				"Cf. A214573."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Emeric Deutsch_, Aug 14 2012",
			"references": 1,
			"revision": 25,
			"time": "2017-03-07T11:22:49-05:00",
			"created": "2012-08-14T16:05:49-04:00"
		}
	]
}