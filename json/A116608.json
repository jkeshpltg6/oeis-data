{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A116608",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 116608,
			"data": "1,2,2,1,3,2,2,5,4,6,1,2,11,2,4,13,5,3,17,10,4,22,15,1,2,27,25,2,6,29,37,5,2,37,52,10,4,44,67,20,4,44,97,30,1,5,55,117,52,2,2,59,154,77,5,6,68,184,117,10,2,71,235,162,20,6,81,277,227,36,4,82,338,309,58,1,4,102",
			"name": "Triangle read by rows: T(n,k) is number of partitions of n having k distinct parts (n\u003e=1, k\u003e=1).",
			"comment": [
				"Row n has floor([sqrt(1+8n)-1]/2) terms (number of terms increases by one at each triangular number).",
				"Row sums yield the partition numbers (A000041).",
				"Row n has length A003056(n), hence the first element of column k is in row A000217(k). - _Omar E. Pol_, Jan 19 2014"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A116608/b116608.txt\"\u003eRows n = 1..500, flattened\u003c/a\u003e",
				"Emmanuel Briand, \u003ca href=\"https://arxiv.org/abs/2004.13180\"\u003eOn partitions with k corners not containing the staircase with one more corner\u003c/a\u003e, arXiv:2004.13180 [math.CO], 2020.",
				"Sang June Lee, Jun Seok Oh, \u003ca href=\"https://arxiv.org/abs/2003.02511\"\u003eOn zero-sum free sequences contained in random subsets of finite cyclic groups\u003c/a\u003e, arXiv:2003.02511 [math.CO], 2020."
			],
			"formula": [
				"G.f.: -1 + Product_{j=1..infinity} 1 + tx^j/(1-x^j).",
				"T(n,1) = A000005(n) (number of divisors of n).",
				"T(n,2) = A002133(n).",
				"T(n,3) = A002134(n).",
				"Sum_{k\u003e=1} k * T(n,k) = A000070(n-1).",
				"Sum_{k\u003e=0} k! * T(n,k) = A274174(n). - _Alois P. Heinz_, Jun 13 2016",
				"T(n + A000217(k), k) = A000712(n), for 0 \u003c= n \u003c= k [Briand]. - _Álvar Ibeas_, Nov 04 2020"
			],
			"example": [
				"T(6,2) = 6 because we have [5,1], [4,2], [4,1,1], [3,1,1,1], [2,2,1,1] and [2,1,1,1,1,1] ([6], [3,3], [3,2,1], [2,2,2] and [1,1,1,1,1,1] do not qualify).",
				"Triangle starts:",
				"  1;",
				"  2;",
				"  2,  1;",
				"  3,  2;",
				"  2,  5;",
				"  4,  6, 1;",
				"  2, 11, 2;",
				"  4, 13, 5;",
				"  3, 17, 10;",
				"  4, 22, 15, 1;",
				"  ..."
			],
			"maple": [
				"g:=product(1+t*x^j/(1-x^j),j=1..30)-1: gser:=simplify(series(g,x=0,27)): for n from 1 to 23 do P[n]:=sort(coeff(gser,x^n)) od: for n from 1 to 23 do seq(coeff(P[n],t^j),j=1..floor(sqrt(1+8*n)/2-1/2)) od; # yields sequence in triangular form",
				"# second Maple program:",
				"b:= proc(n, i) option remember; local j; if n=0 then 1",
				"      elif i\u003c1 then 0 else []; for j from 0 to n/i do zip((x, y)",
				"      -\u003ex+y, %, [`if`(j\u003e0, 0, [][]), b(n-i*j, i-1)], 0) od; %[] fi",
				"    end:",
				"T:= n-\u003e subsop(1=NULL, [b(n, n)])[]:",
				"seq(T(n), n=1..30); # _Alois P. Heinz_, Nov 07 2012"
			],
			"mathematica": [
				"p=Product[1+(y x^i)/(1-x^i),{i,1,20}];f[list_]:=Select[list,#\u003e0\u0026];Flatten[Map[f,Drop[CoefficientList[Series[p,{x,0,20}],{x,y}],1]]] (* _Geoffrey Critzer_, Nov 28 2011 *)",
				"Table[Length /@ Split[Sort[Length /@ Union /@ IntegerPartitions@n]], {n, 22}] // Flatten (* _Robert Price_, Jun 13 2020 *)"
			],
			"xref": [
				"Cf. A000041, A000005, A000070, A002133, A002134.",
				"Cf. A060177 (reflected rows). - _Alois P. Heinz_, Jan 29 2014",
				"Cf. A274174."
			],
			"keyword": "nonn,tabf,look",
			"offset": "1,2",
			"author": "_Emeric Deutsch_, Feb 19 2006",
			"references": 50,
			"revision": 58,
			"time": "2020-11-04T11:04:29-05:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}