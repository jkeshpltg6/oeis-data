{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A265901",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 265901,
			"data": "1,2,3,4,7,5,8,15,12,6,16,31,27,14,9,32,63,58,30,21,10,64,127,121,62,48,24,11,128,255,248,126,106,54,26,13,256,511,503,254,227,116,57,29,17,512,1023,1014,510,475,242,120,61,38,18,1024,2047,2037,1022,978,496,247,125,86,42,19,2048,4095,4084,2046,1992,1006,502,253,192,96,45,20",
			"name": "Square array read by descending antidiagonals: A(n,1) = A188163(n), and for k \u003e 1, A(n,k) = A087686(1+A(n,k-1)).",
			"comment": [
				"Square array read by descending antidiagonals: A(1,1), A(1,2), A(2,1), A(1,3), A(2,2), A(3,1), etc.",
				"The topmost row (row 1) of the array is A000079 (powers of 2), and in general each row 2^k contains the sequence (2^n - k), starting from the term (2^(k+1) - k). This follows from the properties (3) and (4) of A004001 given on page 227 of Kubo \u0026 Vakil paper (page 3 in PDF).",
				"Moreover, each row 2^k - 1 (for k \u003e= 2) contains the sequence 2^n - n - (k-2), starting from the term (2^(k+1) - (2k-1)). To see why this holds, consider the definitions of sequences A162598 and A265332, the latter which also illustrates how the frequency counts Q_n for A004001 are recursively constructed (in the Kubo \u0026 Vakil paper)."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A265901/b265901.txt\"\u003eTable of n, a(n) for n = 1..210; the first 20 antidiagonals of array\u003c/a\u003e",
				"T. Kubo and R. Vakil, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(94)00303-Z\"\u003eOn Conway's recursive sequence\u003c/a\u003e, Discr. Math. 152 (1996), 225-252.",
				"\u003ca href=\"/index/Ho#Hofstadter\"\u003eIndex entries for Hofstadter-type sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"For the first column k=1, A(n,1) = A188163(n), for columns k \u003e 1, A(n,k) = A087686(1+A(n,k-1))."
			],
			"example": [
				"The top left corner of the array:",
				"   1,  2,   4,   8,  16,   32,   64,  128,  256,   512,  1024, ...",
				"   3,  7,  15,  31,  63,  127,  255,  511, 1023,  2047,  4095, ...",
				"   5, 12,  27,  58, 121,  248,  503, 1014, 2037,  4084,  8179, ...",
				"   6, 14,  30,  62, 126,  254,  510, 1022, 2046,  4094,  8190, ...",
				"   9, 21,  48, 106, 227,  475,  978, 1992, 4029,  8113, 16292, ...",
				"  10, 24,  54, 116, 242,  496, 1006, 2028, 4074,  8168, 16358, ...",
				"  11, 26,  57, 120, 247,  502, 1013, 2036, 4083,  8178, 16369, ...",
				"  13, 29,  61, 125, 253,  509, 1021, 2045, 4093,  8189, 16381, ...",
				"  17, 38,  86, 192, 419,  894, 1872, 3864, 7893, 16006, 32298, ...",
				"  18, 42,  96, 212, 454,  950, 1956, 3984, 8058, 16226, 32584, ...",
				"  19, 45, 102, 222, 469,  971, 1984, 4020, 8103, 16281, 32650, ...",
				"  20, 47, 105, 226, 474,  977, 1991, 4028, 8112, 16291, 32661, ...",
				"  22, 51, 112, 237, 490,  999, 2020, 4065, 8158, 16347, 32728, ...",
				"  23, 53, 115, 241, 495, 1005, 2027, 4073, 8167, 16357, 32739, ...",
				"  25, 56, 119, 246, 501, 1012, 2035, 4082, 8177, 16368, 32751, ...",
				"  28, 60, 124, 252, 508, 1020, 2044, 4092, 8188, 16380, 32764, ...",
				"  ..."
			],
			"program": [
				"(Scheme)",
				"(define (A265901 n) (A265901bi (A002260 n) (A004736 n)))",
				"(define (A265901bi row col) (if (= 1 col) (A188163 row) (A087686 (+ 1 (A265901bi row (- col 1))))))"
			],
			"xref": [
				"Inverse permutation: A267102.",
				"Transpose: A265903.",
				"Cf. A265900 (main diagonal).",
				"Cf. A162598 (row index of n in array), A265332 (column index of n in array).",
				"Cf. A004001, A051135, A088359, A087686.",
				"Column 1: A188163.",
				"Column 2: A266109.",
				"Row 1: A000079 (2^n).",
				"Row 2: A000225 (2^n - 1, from 3 onward).",
				"Row 3: A000325 (2^n - n, from 5 onward).",
				"Row 4: A000918 (2^n - 2, from 6 onward).",
				"Row 5: A084634 (?, from 9 onward).",
				"Row 6: A132732 (2^n - 2n + 2, from 10 onward).",
				"Row 7: A000295 (2^n - n - 1, from 11 onward).",
				"Row 8: A036563 (2^n - 3).",
				"Row 9: A084635 (?, from 17 onward).",
				"Row 12: A048492 (?, from 20 onward).",
				"Row 13: A249453 (?, from 22 onward).",
				"Row 14: A183155 (2^n - 2n + 1, from 23 onward. Cf. also A244331).",
				"Row 15: A000247 (2^n - n - 2, from 25 onward).",
				"Row 16: A028399 (2^n - 4).",
				"Cf. also permutations A267111, A267112."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Antti Karttunen_, Dec 18 2015",
			"references": 15,
			"revision": 50,
			"time": "2016-09-20T13:25:24-04:00",
			"created": "2016-01-17T20:24:49-05:00"
		}
	]
}