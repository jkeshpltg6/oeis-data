{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A345227",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 345227,
			"data": "0,1,2,3,4,5,6,7,8,9,10,23,45,67,89,12,30,46,57,98,13,20,47,56,890,14,25,36,78,90,15,24,37,68,91,26,34,50,79,80,16,27,35,48,92,17,38,40,59,60,18,29,43,65,70,19,28,53,64,71,32,49,58,607,21,39,54,76,800,31,42,69,75,81,52,63",
			"name": "Lexicographically earliest infinite sequence of distinct positive numbers such that every block of ten consecutive digits starting from a(1) contains the digits 0 to 9.",
			"comment": [
				"This sequence is similar to A120125, with which it shares the first twenty-four terms, but has a more restrictive condition that every block of ten consecutive digits, beginning with a(1), must contains the digits 0 to 9. The sequence is also forced to be infinite, which means a number cannot be chosen for a(n) that would make it impossible for a(n+1) to exist. See the example below for a(64).",
				"Numerous numbers cannot appear in the sequence as they would violate the requirement of ten consecutive unique digits. Any number with three or more equal consecutive digits cannot occur, nor can numbers with consecutive pairs of the same digit, e.g., 1122. However numbers with a single pair of equal consecutive digits can occur as the first digit may end a block of ten digits while the second digit may start the next. This is not a common occurrence so such numbers may not appear for many terms, e.g., a(4231) = 11.",
				"When n becomes greater than 10^10 all numbers would be forced to consist of runs of the ten digits 0 to 9 in all possible orderings, leading to even greater restrictions on the possible terms. The behavior of the sequence in this regime is not known."
			],
			"link": [
				"Scott R. Shannon, \u003ca href=\"/A345227/a345227.png\"\u003eImage of the first 511000 terms\u003c/a\u003e. The thin green line is a(n) = n."
			],
			"example": [
				"a(11) = 10. The first ten terms contain the digits 0 to 9 so a(11) begins the second block of ten digits so is free to choose any distinct digits that can form the lowest number not yet seen. The lowest such number is 10.",
				"a(12) = 23 as a(11) used the digits 0 and 1, so the next lowest distinct number that can be created from the available digits 2 to 9 is 23.",
				"a(16) = 12 as a(15) ended the second block of ten digits so a(16) is free to choose any distinct digits that can form the lowest number not yet seen. The lowest such number is 12.",
				"a(25) = 890. This term ends the forth block of ten unique digits and the only available digits to use are 8 and 9. But both 89 and 98 have previously appeared so the next lowest number possible is 890. This term uses the 0 digit in the fifth block of ten digits. This is the first term that differs from A120125.",
				"a(64) = 607. This is the first time where a term is determined by the requirement that the next term must exist. As a(64) is written the three available digits are 0, 6 and 7. The number 67 has been used but 76 is available. But setting a(64) to 76 would force the next term to start with 0. As we do not allow 0 as a leading digit this would mean a(65) would not exist. Thus 76 cannot be chosen, and the next lowest number that can be created from 0, 6 and 7 that avoids leaving 0 as the only available digit is 607."
			],
			"xref": [
				"Cf. A120125, A333832, A171901, A291266."
			],
			"keyword": "nonn,base",
			"offset": "1,3",
			"author": "_Scott R. Shannon_, Jun 11 2021",
			"references": 1,
			"revision": 20,
			"time": "2021-07-30T21:31:38-04:00",
			"created": "2021-06-16T21:02:38-04:00"
		}
	]
}