{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A145460",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 145460,
			"data": "1,1,1,1,1,2,1,0,3,5,1,0,1,10,15,1,0,0,3,41,52,1,0,0,1,9,196,203,1,0,0,0,4,40,1057,877,1,0,0,0,1,10,210,6322,4140,1,0,0,0,0,5,30,1176,41393,21147,1,0,0,0,0,1,15,175,7273,293608,115975,1,0,0,0,0,0,6,35,1176,49932,2237921,678570",
			"name": "Square array A(n,k), n\u003e=0, k\u003e=0, read by antidiagonals, where sequence a_k of column k is the exponential transform of C(n,k).",
			"comment": [
				"A(n,k) is also the number of ways of placing n labeled balls into indistinguishable boxes, where in each filled box k balls are seen at the top. E.g. A(3,1)=10:",
				"  |1.| |2.| |3.| |1|2| |1|2| |1|3| |1|3| |2|3| |2|3| |1|2|3|",
				"  |23| |13| |12| |3|.| |.|3| |2|.| |.|2| |1|.| |.|1| |.|.|.|",
				"  +--+ +--+ +--+ +-+-+ +-+-+ +-+-+ +-+-+ +-+-+ +-+-+ +-+-+-+"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A145460/b145460.txt\"\u003eAntidiagonals n = 0..140, flattened\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/transforms.txt\"\u003eTransforms\u003c/a\u003e"
			],
			"formula": [
				"A(0,k) = 1 and A(n,k) = Sum_{i=0..n-1} binomial(n-1,i) * binomial(i+1,k) * A(n-1-i,k) for n \u003e 0. - _Seiichi Manyama_, Sep 28 2017"
			],
			"example": [
				"Square array A(n,k) begins:",
				"   1,   1,  1,  1,  1,  1,  ...",
				"   1,   1,  0,  0,  0,  0,  ...",
				"   2,   3,  1,  0,  0,  0,  ...",
				"   5,  10,  3,  1,  0,  0,  ...",
				"  15,  41,  9,  4,  1,  0,  ...",
				"  52, 196, 40, 10,  5,  1,  ..."
			],
			"maple": [
				"exptr:= proc(p) local g; g:=",
				"          proc(n) option remember; `if`(n=0, 1,",
				"             add(binomial(n-1, j-1) *p(j) *g(n-j), j=1..n))",
				"        end: end:",
				"A:= (n,k)-\u003e exptr(i-\u003e binomial(i, k))(n):",
				"seq(seq(A(n, d-n), n=0..d), d=0..12);"
			],
			"mathematica": [
				"Exptr[p_] := Module[{g}, g[n_] := g[n] = If[n == 0, 1, Sum[Binomial[n-1, j-1] *p[j]*g[n-j], {j, 1, n}]]; g]; A[n_, k_] := Exptr[Function[i, Binomial[i, k]]][n]; Table[Table[A[n, d-n], {n, 0, d}], {d, 0, 12}] // Flatten (* _Jean-François Alcover_, Jan 15 2014, translated from Maple *)"
			],
			"program": [
				"(Ruby)",
				"def ncr(n, r)",
				"  return 1 if r == 0",
				"  (n - r + 1..n).inject(:*) / (1..r).inject(:*)",
				"end",
				"def A(k, n)",
				"  ary = [1]",
				"  (1..n).each{|i| ary \u003c\u003c (0..i - 1).inject(0){|s, j| s + ncr(i - 1, j) * ncr(j + 1, k) * ary[i - 1 - j]}}",
				"  ary",
				"end",
				"def A145460(n)",
				"  a = []",
				"  (0..n).each{|i| a \u003c\u003c A(i, n - i)}",
				"  ary = []",
				"  (0..n).each{|i|",
				"    (0..i).each{|j|",
				"      ary \u003c\u003c a[i - j][j]",
				"    }",
				"  }",
				"  ary",
				"end",
				"p A145460(20) # _Seiichi Manyama_, Sep 28 2017"
			],
			"xref": [
				"Columns k=0-9 give: A000110, A000248, A133189, A145453, A145454, A145455, A145456, A145457, A145458, A145459.",
				"A(2n,n) gives A029651.",
				"Cf.: A007318, A143398, A292948."
			],
			"keyword": "nonn,tabl",
			"offset": "0,6",
			"author": "_Alois P. Heinz_, Oct 10 2008",
			"references": 15,
			"revision": 34,
			"time": "2018-10-24T08:20:53-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}