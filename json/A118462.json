{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A118462",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 118462,
			"data": "0,1,2,3,4,5,8,6,9,16,7,10,17,32,11,12,18,33,64,13,19,20,34,65,128,14,21,24,35,36,66,129,256,15,22,25,37,40,67,68,130,257,512,23,26,38,41,48,69,72,131,132,258,513,1024,27,28,39,42,49,70,73,80,133,136,259,260,514",
			"name": "Decimal equivalent of binary encoding of partitions into distinct parts.",
			"comment": [
				"A part of size k in the partition makes the 2^(k-1) bit of the number be 1. The partitions of n are in reverse Mathematica ordering, so that each row is in ascending order. This is a permutation of the nonnegative integers.",
				"The sequence is the concatenation of the sets: e_n={j\u003e=0: A029931(j)=n}, n=0,1,...: e_0={0}, e_1={1}, e_2={2}, e_3={3,4}, e_4={5,8}, e_5={6,9,16}, e_6={7,10,17,32}, e_7={11,12,18.33.64}, ... . - _Vladimir Shevelev_, Mar 16 2009",
				"This permutation of the nonnegative integers A001477 has fixed points 0, 1, 2, 3, 4, 5, 325, 562, 800, 4449, ... and inverse permutation A118463. - _Alois P. Heinz_, Sep 06 2014"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A118462/b118462.txt\"\u003eRows n = 0..42, flattened\u003c/a\u003e",
				"V. Shevelev, \u003ca href=\"http://arXiv.org/abs/0903.1743\"\u003eA recursion for divisor function over divisors belonging to a prescribed finite sequence of positive integers and a solution of the Lahiri problem for divisor function sigma_x(n)\u003c/a\u003e, arXiv:0903.1743 [math.NT], 2009. [From _Vladimir Shevelev_, Mar 17 2009]",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"example": [
				"Partition 11 is [4,2], which gives binary 1010 (2^(4-1)+2^(2-1)), or 10, so a(11)=10.",
				"Triangle begins:",
				"0;",
				"1;",
				"2;",
				"3,   4;",
				"5,   8;",
				"6,   9, 16;",
				"7,  10, 17, 32;",
				"11, 12, 18, 33, 64;",
				"13, 19, 20, 34, 65, 128;",
				"14, 21, 24, 35, 36,  66, 129, 256;",
				"15, 22, 25, 37, 40,  67,  68, 130, 257, 512;"
			],
			"maple": [
				"b:= proc(n, i) option remember; `if`(n=0, [0], `if`(i\u003c1, [], [seq(",
				"      map(p-\u003ep+2^(i-1)*j, b(n-i*j, i-1))[], j=0..min(1, n/i))]))",
				"    end:",
				"T:= n-\u003e sort(b(n$2))[]:",
				"seq(T(n), n=0..14);  # _Alois P. Heinz_, Sep 06 2014"
			],
			"mathematica": [
				"b[n_, i_] := b[n, i] = If[n==0, {0}, If[i\u003c1, {}, Flatten[Table[b[n-i*j, i-1 ] + 2^(i-1)*j, {j, 0, Min[1, n/i]}]]]]; T[n_] := Sort[b[n, n]]; Table[ T[n], {n, 0, 14}] // Flatten (* _Jean-François Alcover_, Dec 27 2015, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A118463, A118457, A000009 (row lengths).",
				"Cf. A089633 (first column), A000079 (last in each column). - _Franklin T. Adams-Watters_, Mar 16 2009",
				"Cf. A246867."
			],
			"keyword": "base,nonn,tabf,look",
			"offset": "0,3",
			"author": "_Franklin T. Adams-Watters_, Apr 28 2006",
			"references": 3,
			"revision": 29,
			"time": "2015-12-27T09:21:50-05:00",
			"created": "2006-05-19T03:00:00-04:00"
		}
	]
}