{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A279614",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 279614,
			"data": "1,2,3,4,5,4,6,5,5,6,7,6,6,7,7,6,7,6,8,8,8,8,7,7,7,7,7,9,8,8,8,7,9,8,10,8,7,9,8,9,8,9,7,10,9,8,9,8,9,8,9,9,9,8,11,10,10,9,9,10,8,9,10,9,10,10,8,10,9,11,8,9,8,8,9,11,12,9,8,10,10,9",
			"name": "a(1)=1, a(d(x_1)*..*d(x_k)) = 1+a(x_1)+..+a(x_k) where d(n) = n-th Fermi-Dirac prime.",
			"comment": [
				"A Fermi-Dirac prime (A050376) is a positive integer of the form p^(2^k) where p is prime and k\u003e=0.",
				"In analogy with the Matula-Goebel correspondence between rooted trees and positive integers (see A061775), the iterated normalized Fermi-Dirac representation gives a correspondence between rooted identity trees and positive integers. Then a(n) is the number of nodes in the rooted identity tree corresponding to n."
			],
			"link": [
				"OEIS Wiki, \u003ca href=\"/wiki/%22Fermi-Dirac_representation%22_of_n\"\u003e\"Fermi-Dirac representation\" of n\u003c/a\u003e"
			],
			"formula": [
				"Number of appearances of n is |a^{-1}(n)| = A004111(n)."
			],
			"example": [
				"Sequence of rooted identity trees represented as finitary sets begins:",
				"{}, {{}}, {{{}}}, {{{{}}}}, {{{{{}}}}}, {{}{{}}}, {{{{{{}}}}}},",
				"{{}{{{}}}}, {{{}{{}}}}, {{}{{{{}}}}}, {{{{{{{}}}}}}}, {{{}}{{{}}}},",
				"{{{}{{{}}}}}, {{}{{{{{}}}}}}, {{{}}{{{{}}}}}, {{{{}{{}}}}},",
				"{{{}{{{{}}}}}}, {{}{{}{{}}}}, {{{{{{{{}}}}}}}}, {{{{}}}{{{{}}}}},",
				"{{{}}{{{{{}}}}}}, {{}{{{{{{}}}}}}}, {{{{}}{{{}}}}}, {{}{{}}{{{}}}}."
			],
			"mathematica": [
				"nn=200;",
				"FDfactor[n_]:=If[n===1,{},Sort[Join@@Cases[FactorInteger[n],{p_,k_}:\u003ePower[p,Cases[Position[IntegerDigits[k,2]//Reverse,1],{m_}-\u003e2^(m-1)]]]]];",
				"FDprimeList=Array[FDfactor,nn,1,Union];",
				"FDweight[n_?(#\u003c=nn\u0026)]:=If[n===1,1,1+Total[FDweight[Position[FDprimeList,#][[1,1]]]\u0026/@FDfactor[n]]];",
				"Array[FDweight,nn]"
			],
			"xref": [
				"Cf. A004111, A050376, A061773, A061775, A084400, A276625, A279065."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Gus Wiseman_, Dec 15 2016",
			"references": 8,
			"revision": 9,
			"time": "2016-12-15T23:36:06-05:00",
			"created": "2016-12-15T23:36:06-05:00"
		}
	]
}