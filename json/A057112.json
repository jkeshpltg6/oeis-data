{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A057112",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 57112,
			"data": "1,2,1,2,1,3,1,2,3,2,1,2,3,2,1,2,3,1,3,2,3,2,3,4,1,2,1,2,1,3,1,2,3,2,1,2,3,2,1,2,3,1,3,2,3,2,3,4,1,2,1,2,1,3,1,2,3,2,1,2,3,2,1,2,3,1,3,2,3,2,3,4,1,2,1,2,1,3,1,2,3,2,1,2,3,2,1,2,3,1,3,2,3,2,3,4,1,2,1,2,1,3,1,2,3,2,1,2,3,2,1,2,3,1,3,2,3,2,3",
			"name": "Sequence of 719 adjacent transpositions (a[n] a[n]+1), which, when starting from the identity permutation and applied successively, produce a Hamiltonian circuit/path through all 720 permutations of S_6, in such a way that S_{n-1} is always traversed before the rest of S_n.",
			"comment": [
				"If the 120 permutations of S_5 are connected by adjacent transpositions, the graph produced is isomorphic to the prismatodecachoron (a 4-dimensional polytope) graph (see the Olshevsky link) and this sequence gives directions for a Hamiltonian circuit through its vertices. The first 24 terms give a Hamiltonian path through truncated octahedron's graph (the last path shown in the Karttunen link).",
				"Comment from _N. J. A. Sloane_: This is the subject of \"bell-ringing\" or \"change-ringing\", which has been studied for hundreds of years. See for example Amer. Math. Monthly, Vol. 94, Number 8, 1987, pp. 721-."
			],
			"link": [
				"Georg Fischer, \u003ca href=\"/A057112/b057112.txt\"\u003eTable of n, a(n) for n = 1..719\u003c/a\u003e",
				"A. Karttunen, \u003ca href=\"http://www.iki.fi/~kartturi/matikka/permgraf/troctahe.htm\"\u003eTruncated octahedron\u003c/a\u003e",
				"G. Olshevsky, \u003ca href=\"http://members.aol.com/Polycell/section1.html\"\u003eGreat prismatodecachoron\u003c/a\u003e",
				"Arthur T. White, \u003ca href=\"https://www.jstor.org/stable/2323414\"\u003eRinging the Cosets\u003c/a\u003e, Amer. Math. Monthly, Vol. 94, Number 8, 1987, pp. 721-746.",
				"\u003ca href=\"/index/Be#bell_ringing\"\u003eIndex entries for sequences related to bell ringing\u003c/a\u003e"
			],
			"formula": [
				"tp_seq := [seq(adj_tp_seq(n), n=1..719)];"
			],
			"example": [
				"Starting from the identity permutation and applying these transpositions (from right), we get:",
				"[1,2,3,4,5,6,...] o (1 2) -\u003e",
				"[2,1,3,4,5,6,...] o (2 3) -\u003e",
				"[2,3,1,4,5,6,...] o (1 2) -\u003e",
				"[3,2,1,4,5,6,...] o (2 1) -\u003e",
				"[3,1,2,4,5,6,...] o (1 2) -\u003e",
				"[1,3,2,4,5,6,...] o (3 4) -\u003e",
				"[1,3,4,2,5,6,...] o (1 2) -\u003e",
				"[3,1,4,2,5,6,...] o (2 3) -\u003e",
				"[3,4,1,2,5,6,...] o (3 4) etc."
			],
			"maple": [
				"adj_tp_seq := proc(n) local fl,fd,v; fl := fac_base(n); fd := fl[1]; if((1 = fd) and (0 = convert(cdr(fl),`+`))) then RETURN(nops(fl)); fi; if(n \u003c 6) then RETURN(2 - (`mod`(n,2))); fi; if((0 = convert(cdr(fl),`+`)) and (n \u003c 24)) then RETURN((nops(fl)+1)-fd); fi; if(n \u003c 18) then if(0 = (`mod`(n,2))) then RETURN(2); else RETURN(4-(`mod`(n,4))); fi; else if(n \u003c 24) then RETURN(2+(`mod`(n,2))); else if(n \u003c 120) then if(0 = convert(cdr(fl),`+`)) then RETURN(nops(fl)); else RETURN(adj_tp_seq(`mod`(n,24))); fi; else if(n \u003c 720) then if(125 = n) then RETURN(5); fi; v := (`mod`(n,5)); if(0 = v) then v := (n-125)/5; RETURN(adj_tp_seq(v)+(`mod`(v+1,2))); else if(5 \u003e (`mod`(n,10))) then RETURN(5-v); else RETURN(v); fi; fi; else if(0 = convert(cdr(fl),`+`)) then RETURN(nops(fl)); fi; RETURN(adj_tp_seq(`mod`(n,720))); fi; fi; fi; fi; end;"
			],
			"xref": [
				"Cf. A057113, A055089 (for the Maple definitions of fac_base and cdr), A060135 (palindromic variant of the same idea)."
			],
			"keyword": "nonn,fini,full",
			"offset": "1,2",
			"author": "_Antti Karttunen_, Aug 09 2000",
			"references": 5,
			"revision": 17,
			"time": "2021-06-12T23:35:16-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}