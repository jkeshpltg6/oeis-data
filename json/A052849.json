{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A052849",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 52849,
			"data": "0,2,4,12,48,240,1440,10080,80640,725760,7257600,79833600,958003200,12454041600,174356582400,2615348736000,41845579776000,711374856192000,12804747411456000,243290200817664000,4865804016353280000",
			"name": "a(0) = 0; a(n+1) = 2*n! (n \u003e= 0).",
			"comment": [
				"For n \u003e= 1 a(n) is the size of the centralizer of a transposition in the symmetric group S_(n+1). - Ahmed Fares (ahmedfares(AT)my-deja.com), May 12 2001",
				"For n \u003e 0, a(n) = n! - A062119(n-1) = number of permutations of length n that have two specified elements adjacent. For example, a(4) = 12 as of the 24 permutations, 12 have say 1 and 2 adjacent: 1234, 2134, 1243, 2143, 3124, 3214, 4123, 4213, 3412, 3421, 4312, 4321. - _Jon Perry_, Jun 08 2003",
				"With different offset, denominators of certain sums computed by Ramanujan.",
				"From _Michael Somos_, Mar 04 2004: (Start)",
				"Stirling transform of a(n) = [2, 4, 12, 48, 240, ...] is A000629(n) = [2, 6, 26, 150, 1082, ...].",
				"Stirling transform of a(n-1) = [1, 2, 4, 12, 48, ...] is A007047(n-1) = [1, 3, 11, 51, 299, ...].",
				"Stirling transform of a(n) = [1, 4, 12, 48, 240, ...] is A002050(n) = [1, 5, 25, 149, 1081, ...].",
				"Stirling transform of 2*A006252(n) = [2, 2, 4, 8, 28, ...] is a(n) = [2, 4, 12, 48, 240, ...].",
				"Stirling transform of a(n+1) = [4, 12, 48, 240, ...] is 2*A005649(n) = [4, 16, 88, 616, ...].",
				"Stirling transform of a(n+1) = [4, 12, 48, 240, ...] is 4*A083410(n) = [4, 16, 88, 616, ...]. (End)",
				"Number of {12, 12*, 21, 21*}-avoiding signed permutations in the hyperoctahedral group.",
				"Permanent of the (0, 1)-matrices with (i, j)-th entry equal to 0 if and only if it is in the border but not the corners. The border of a matrix is defined the be the first and the last row, together with the first and the last column. The corners of a matrix are the entries (i = 1, j = 1), (i = 1, j = n), (i = n, j = 1) and (i = n, j = n). - _Simone Severini_, Oct 17 2004",
				"a(n) = A245334(n, n-1), n \u003e 0. - _Reinhard Zumkeller_, Aug 31 2014"
			],
			"reference": [
				"B. C. Berndt, Ramanujan's Notebooks Part V, Springer-Verlag, see p. 520."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A052849/b052849.txt\"\u003eTable of n, a(n) for n = 0..400\u003c/a\u003e",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=817\"\u003eEncyclopedia of Combinatorial Structures 817\u003c/a\u003e",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=490\"\u003eEncyclopedia of Combinatorial Structures 490\u003c/a\u003e",
				"T. Mansour and J. West, \u003ca href=\"http://arXiv.org/abs/math.CO/0207204\"\u003eAvoiding 2-letter signed patterns\u003c/a\u003e, arXiv:math/0207204 [math.CO], 2002.",
				"Luis Manuel Rivera, \u003ca href=\"http://arxiv.org/abs/1406.3081\"\u003eInteger sequences and k-commuting permutations\u003c/a\u003e, arXiv preprint arXiv:1406.3081 [math.CO], 2014.",
				"\u003ca href=\"/index/Fa#facbase\"\u003eIndex entries for sequences related to factorial base representation\u003c/a\u003e"
			],
			"formula": [
				"D-finite with recurrence: {a(0) = 0, a(1) = 2, (-1 - n)*a(n+1) + a(n+2)=0}.",
				"E.g.f.: 2*x/(1-x).",
				"a(n) = A090802(n, n - 1) for n \u003e 0. - _Ross La Haye_, Sep 26 2005",
				"For n \u003e= 1, a(n) = (n+3)!*Sum_((-1)^k*binomial(2, k)/(n + 3 - k), k = 0..n + 2). - _Milan Janjic_, Dec 14 2008",
				"G.f.: 2/Q(0) - 2, where Q(k) = 1 - x*(k + 1)/(1 - x*(k + 1)/Q(k+1) ); (continued fraction ). - _Sergei N. Gladkovskii_, Apr 01 2013",
				"G.f.: -2 + 2/Q(0), where Q(k) = 1 + k*x - x*(k+1)/Q(k+1); (continued fraction). - _Sergei N. Gladkovskii_, May 01 2013",
				"G.f.: W(0) - 2 , where W(k) = 1 + 1/( 1 - x*(k+1)/( x*(k+1) + 1/W(k+1) )); (continued fraction). - _Sergei N. Gladkovskii_, Aug 21 2013"
			],
			"maple": [
				"spec := [S,{B=Cycle(Z),C=Cycle(Z),S=Union(B,C)},labeled]: seq(combstruct[count](spec,size=n), n=0..20);"
			],
			"mathematica": [
				"Join[{0}, 2Range[20]!] (* _Harvey P. Dale_, Jul 13 2013 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c1,0,n!*2)",
				"(Haskell)",
				"a052849 n = if n == 0 then 0 else 2 * a000142 n",
				"a052849_list = 0 : fs where fs = 2 : zipWith (*) [2..] fs",
				"-- _Reinhard Zumkeller_, Aug 31 2014",
				"(MAGMA) [0] cat [2*Factorial(n-1): n in [2..25]]; // _Vincenzo Librandi_, Nov 03 2014"
			],
			"xref": [
				"a(n) = T(n, 2) for n\u003e1, where T is defined as in A080046.",
				"Essentially the same sequence as A098558. Cf. A062119.",
				"Cf. A245334, A000142.",
				"Row 3 of A276955 (from term a(2)=4 onward)."
			],
			"keyword": "easy,nonn",
			"offset": "0,2",
			"author": "encyclopedia(AT)pommard.inria.fr, Jan 25 2000",
			"ext": [
				"More terms from _Ross La Haye_, Sep 26 2005"
			],
			"references": 33,
			"revision": 61,
			"time": "2020-02-24T14:41:16-05:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}