{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A286298",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 286298,
			"data": "0,1,3,2,4,5,4,3,5,6,7,6,5,6,5,4,6,7,8,7,8,9,8,7,6,7,8,7,6,7,6,5,7,8,9,8,9,10,9,8,9,10,11,10,9,10,9,8,7,8,9,8,9,10,9,8,7,8,9,8,7,8,7,6,8,9,10,9,10,11,10,9,10,11,12,11,10,11,10,9,10,11,12,11,12,13,12,11,10,11,12",
			"name": "a(0) = 0, a(1) = 1; thereafter, a(2n) = a(n) + 1 + (n mod 2), a(2n+1) = a(n) + 2 - (n mod 2).",
			"comment": [
				"Let S be a set containing 0 and let S grow in generations G(i), defined by these rules: If x is in S then 2x is in S and 1 - x is in S. So G(0) = 0, G(1) = {1}, G(2) = {2}, G(3) = {-1,4}, ... Then a(n) is the generation containing the integer k where n = 2k - 1 for k\u003e0 and -2k for k \u003c= 0. The question posed by Clark Kimberling was \"Does each generation contain a Fibonacci number or its negative?\" It has been proved that every integer occurs in some G(i). - _Karyn McLellan_, Aug 16 2018"
			],
			"reference": [
				"D. Cox and K. McLellan, A problem on generation sets containing Fibonacci numbers, Fib. Quart., 55 (No. 2, 2017), 105-113.",
				"C. Kimberling, Problem proposals, Proceedings of the Sixteenth International Conference on Fibonacci Numbers and Their Applications, P. Anderson, C. Ballot and W. Webb, eds. The Fibonacci Quarterly, 52.5(2014), 5-14."
			],
			"link": [
				"Chai Wah Wu, \u003ca href=\"/A286298/b286298.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A005811(n) + A000523(n) for n \u003e= 1. - _Karyn McLellan_, Aug 16 2018"
			],
			"example": [
				"For k = -5, n = 10 and f(10) = 7, so -5 first appears in G(7). - _Karyn McLellan_, Aug 16 20 18"
			],
			"maple": [
				"f:=proc(n) option remember;",
				"   if n \u003c= 1 then n",
				"   elif (n mod 2) = 0 then f(n/2)+1+((n/2) mod 2);",
				"   else f((n-1)/2) + 2 - ((n-1)/2 mod 2); end if;",
				"end proc;",
				"[seq(f(n),n=0..120)];"
			],
			"program": [
				"(Python)",
				"def A286298(n):",
				"    if n \u003c= 1:",
				"        return n",
				"    a, b = divmod(n, 2)",
				"    return A286298(a) + 1 + b + (-1)**b*(a % 2) # _Chai Wah Wu_, Jun 02 2017",
				"(PARI) a(n) = if (n==0, 0, logint(n, 2) + hammingweight(bitxor(n, n\u003e\u003e1))); \\\\ _Michel Marcus_, Aug 21 2018"
			],
			"xref": [
				"Cf. A000523, A005811, A286299 (first differences)."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_, Jun 02 2017",
			"references": 2,
			"revision": 22,
			"time": "2018-10-14T03:34:43-04:00",
			"created": "2017-06-02T01:03:32-04:00"
		}
	]
}