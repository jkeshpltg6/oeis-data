{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000069",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 69,
			"id": "M1031 N0388",
			"data": "1,2,4,7,8,11,13,14,16,19,21,22,25,26,28,31,32,35,37,38,41,42,44,47,49,50,52,55,56,59,61,62,64,67,69,70,73,74,76,79,81,82,84,87,88,91,93,94,97,98,100,103,104,107,109,110,112,115,117,118,121,122,124,127,128",
			"name": "Odious numbers: numbers with an odd number of 1's in their binary expansion.",
			"comment": [
				"This sequence and A001969 give the unique solution to the problem of splitting the nonnegative integers into two classes in such a way that sums of pairs of distinct elements from either class occur with the same multiplicities [Lambek and Moser]. Cf. A000028, A000379.",
				"In French: les nombres impies.",
				"Has asymptotic density 1/2, since exactly 2 of the 4 numbers 4k, 4k+1, 4k+2, 4k+3 have an even sum of bits, while the other 2 have an odd sum. - _Jeffrey Shallit_, Jun 04 2002",
				"Nim-values for game of mock turtles played with n coins.",
				"A115384(n) = number of odious numbers \u003c= n; A000120(a(n)) = A132680(n). - _Reinhard Zumkeller_, Aug 26 2007",
				"Indices of 1's in the Thue-Morse sequence A010060. - _Tanya Khovanova_, Dec 29 2008",
				"For any positive integer m, the partition of the set of the first 2^m positive integers into evil ones E and odious ones O is a fair division for any polynomial sequence p(k) of degree less than m, that is, Sum_{k in E} p(k) = Sum_{k in O} p(k) holds for any polynomial p with deg(p)\u003cm. - _Pietro Majer_, Mar 15 2009",
				"For n\u003e1 let b(n) = a(n-1). Then b(b(n)) = 2b(n). - _Benoit Cloitre_, Oct 07 2010",
				"A000069(n)(mod 2) == A010060(n). - _Robert G. Wilson v_, Jan 18 2012",
				"A005590(a(n)) \u003e 0. - _Reinhard Zumkeller_, Apr 11 2012",
				"A106400(a(n)) = -1. - _Reinhard Zumkeller_, Apr 29 2012",
				"Lexicographically earliest sequence of distinct nonnegative integers with no term being the binary exclusive OR of any terms. The equivalent sequence for addition or for subtraction is A005408 (the odd numbers) and for multiplication is A026424. - _Peter Munn_, Jan 14 2018"
			],
			"reference": [
				"E. R. Berlekamp, J. H. Conway and R. K. Guy, Winning Ways, Academic Press, NY, 2 vols., 1982, see p. 433.",
				"J. Roberts, Lure of the Integers, Math. Assoc. America, 1992, p. 22.",
				"Vladimir S. Shevelev, On some identities connected with the partition of the positive integers with respect to the Morse sequence, Izv. Vuzov of the North-Caucasus region, Nature sciences 4 (1997), 21-23 (in Russian).",
				"N. J. A. Sloane, A handbook of Integer Sequences, Academic Press, 1973 (including this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A000069/b000069.txt\"\u003eTable of n, a(n) for n = 1..10001\u003c/a\u003e",
				"J.-P. Allouche, \u003ca href=\"https://arxiv.org/abs/1906.10532\"\u003eThe zeta-regularized product of odious numbers\u003c/a\u003e, arXiv:1906.10532 [math.NT], 2019.",
				"J.-P. Allouche and J. Shallit, \u003ca href=\"https://doi.org/10.1016/S0304-3975(03)00090-2\"\u003eThe ring of k-regular sequences, II\u003c/a\u003e, Theoret. Computer Sci., 307 (2003), 3-29.",
				"J.-P. Allouche, J. Shallit and G. Skordev, \u003ca href=\"https://doi.org/10.1016/j.disc.2004.12.004\"\u003eSelf-generating sets, integers with missing blocks and substitutions\u003c/a\u003e, Discrete Math. 292 (2005) 1-15.",
				"J.-P. Allouche, B. Cloitre, and V. Shevelev, \u003ca href=\"https://arxiv.org/abs/1405.6214\"\u003eBeyond odious and evil\u003c/a\u003e, arXiv preprint arXiv:1405.6214 [math.NT], 2014.",
				"J.-P. Allouche, B. Cloitre, and V. Shevelev, \u003ca href=\"http://www.math.bgu.ac.il/~shevelev/58_Beyond_J.pdf\"\u003eBeyond odious and evil\u003c/a\u003e, Aequationes mathematicae, March 2015, pp 1-13.",
				"E. Fouvry and C. Mauduit, \u003ca href=\"http://www.digizeitschriften.de/dms/resolveppn/?PID=PPN235181684_0305%7Clog64\"\u003eSommes des chiffres et nombres presque premiers\u003c/a\u003e, (French) [Sums of digits and almost primes] Math. Ann. Vol. 305, No. 1 (1996), 571-599, DOI:\u003ca href=\"https://doi.org/10.1007/BF01444238\"\u003e10.1007/BF01444238\u003c/a\u003e, MR1397437 (97k:11029).",
				"Aviezri S. Fraenkel, \u003ca href=\"https://doi.org/10.1016/j.disc.2011.03.032\"\u003eThe vile, dopey, evil and odious game players\u003c/a\u003e, Discrete Mathematics, Volume 312, Issue 1, 6 January 2012, Pages 42-46.",
				"Maciej Gawron, and Maciej Ulas, \u003ca href=\"https://doi.org/10.1016/j.disc.2015.12.016\"\u003eOn formal inverse of the Prouhet-Thue-Morse sequence\u003c/a\u003e, Discrete Mathematics 339.5 (2016): 1459-1470. Also \u003ca href=\"https://arxiv.org/abs/1601.04840\"\u003earXiv preprint\u003c/a\u003earXiv:1601.04840 [math.CO], 2016.",
				"R. K. Guy, \u003ca href=\"https://doi.org/10.1007/978-1-4613-3554-2_9\"\u003eThe unity of combinatorics\u003c/a\u003e, Proc. 25th Iranian Math. Conf, Tehran, (1994), Math. Appl 329 129-159, Kluwer Dordrecht 1995, Math. Rev. 96k:05001.",
				"R. K. Guy, \u003ca href=\"http://library.msri.org/books/Book29/files/imp.pdf\"\u003eImpartial games\u003c/a\u003e, pp. 35-55 of Combinatorial Games, ed. R. K. Guy, Proc. Sympos. Appl. Math., 43, Amer. Math. Soc., 1991.",
				"Sajed Haque, Chapter 3.2 of \u003ca href=\"https://uwspace.uwaterloo.ca/handle/10012/12234\"\u003eDiscriminators of Integer Sequences\u003c/a\u003e, Thesis, 2017.",
				"Sajed Haque and Jeffrey Shallit, \u003ca href=\"https://arxiv.org/abs/1605.00092\"\u003eDiscriminators and k-Regular Sequences\u003c/a\u003e, arXiv:1605.00092 [cs.DM], 2016.",
				"K. Jensen, \u003ca href=\"http://vbn.aau.dk/files/197163405/IJART0702_0307_JENSEN.pdf\"\u003eAesthetics and quality of numbers using the primety measure\u003c/a\u003e, Int. J. Arts and Technology, Vol. 7, Nos. 2/3, 2014.",
				"Clark Kimberling, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(03)00085-2\"\u003eAffinely recursive sets and orderings of languages\u003c/a\u003e, Discrete Math., 274 (2004), 147-160. [From _N. J. A. Sloane_, Jan 31 2012]",
				"Tanya Khovanova, \u003ca href=\"http://arxiv.org/abs/1410.2193\"\u003eThere are no coincidences\u003c/a\u003e, arXiv:1410.2193 [math.CO], 2014.",
				"J. Lambek and L. Moser, \u003ca href=\"http://dx.doi.org/10.4153/CMB-1959-013-x\"\u003eOn some two way classifications of integers\u003c/a\u003e, Canad. Math. Bull. 2 (1959), 85-89.",
				"M. D. McIlroy, \u003ca href=\"http://dx.doi.org/10.1137/0203020\"\u003eThe number of 1's in binary integers: bounds and extremal properties\u003c/a\u003e, SIAM J. Comput., 3 (1974), 255-261.",
				"H. L. Montgomery, \u003ca href=\"http://www-personal.umich.edu/~hlm/ten.html\"\u003eTen Lectures on the Interface Between Analytic Number Theory and Harmonic Analysis\u003c/a\u003e, Amer. Math. Soc., 1996, p. 208.",
				"D. J. Newman, \u003ca href=\"http://dx.doi.org/10.1007/978-1-4613-8214-0\"\u003eA Problem Seminar\u003c/a\u003e, Problem 15 pp. 5; 15 Springer-Verlag NY 1982.",
				"Aayush Rajasekaran, Jeffrey Shallit and Tim Smith, \u003ca href=\"https://doi.org/10.1007/s00224-019-09929-9\"\u003eAdditive Number Theory via Automata Theory\u003c/a\u003e, Theory of Computing Systems (2019) 1-26.",
				"Jeffrey Shallit, \u003ca href=\"https://arxiv.org/abs/2112.13627\"\u003eAdditive Number Theory via Automata and Logic\u003c/a\u003e, arXiv:2112.13627 [math.NT], 2021.",
				"Vladimir Shevelev and Peter J. C. Moses, \u003ca href=\"http://arxiv.org/abs/1207.0404\"\u003eTangent power sums and their applications\u003c/a\u003e, arXiv:1207.0404 [math.NT], 2012-2014. - From _N. J. A. Sloane_, Dec 17 2012",
				"Vladimir Shevelev and Peter J. C. Moses, \u003ca href=\"http://www.emis.de/journals/INTEGERS/papers/o64/o64.Abstract.html\"\u003eTangent power sums and their applications\u003c/a\u003e, INTEGERS, 14(2014) #64.",
				"Vladimir Shevelev and Peter J. C. Moses, \u003ca href=\"http://arxiv.org/abs/1209.5705\"\u003eA family of digit functions with large periods\u003c/a\u003e, arXiv:1209.5705 [math.NT], 2012.",
				"Andrzej Tomski and Maciej Zakarczemny, \u003ca href=\"https://doi.org/10.4467/2353737XCT.18.106.8801\"\u003eA note on Browkin's and Cao's cancellation algorithm\u003c/a\u003e, Technical Transections 7/2018.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/OdiousNumber.html\"\u003eOdious Number\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e",
				"\u003ca href=\"/index/Cor#core\"\u003eIndex entries for \"core\" sequences\u003c/a\u003e"
			],
			"formula": [
				"G.f.: 1+Sum_[k\u003e=0, t(2+2t+5t^2-t^4)/(1-t^2)^2 * Product_(l=0, k-1, 1-x^(2^l)), t=x^2^k]. - _Ralf Stephan_, Mar 25 2004",
				"a(n+1) = 1/2 * (4*n + 1 + (-1)^A000120(n)). - _Ralf Stephan_, Sep 14 2003",
				"Numbers n such that A010060(n) = 1. - _Benoit Cloitre_, Nov 15 2003",
				"a(2*n+1) + a(2*n) = A017101(n) = 8*n+3. a(2*n+1) - a(2*n) gives the Thue-Morse sequence (1, 3 version): 1, 3, 3, 1, 3, 1, 1, 3, 3, 1, 1, 3, 1, ... A001969(n) + A000069(n) = A016813(n) = 4*n+1. - _Philippe Deléham_, Feb 04 2004",
				"(-1)^a(n) = 2*A010060(n)-1. - _Benoit Cloitre_, Mar 08 2004",
				"a(1) = 1; for n\u003e1: a(2*n) = 6*n-3 -a(n), a(2*n+1) = a(n+1) + 2*n. - Corrected by _Vladimir Shevelev_, Sep 25 2011",
				"For k\u003e=1 and for every real (or complex) x, we have Sum_{i=1..2^k} (a(i)+x)^s = Sum_{i=1..2^k} (A001969(i)+x)^s, s=0..k.",
				"For x=0, s\u003c=k-1, it is known as Prouhet theorem (see: J.-P. Allouche and Jeffrey Shallit, The Ubiquitous Prouhet-Thue-Morse Sequence). - _Vladimir Shevelev_, Jan 16 2012"
			],
			"example": [
				"For k=2, x=0 and x=0.2 we respectively have 1^2 + 2^2 + 4^2 + 7^2 = 0^2 + 3^2 + 5^2 + 6^2 = 70;",
				"(1.2)^2 + (2.2)^2 + (4.2)^2 + (7.2)^2 = (0.2)^2 + (3.2)^2 + (5.2)^2 + (6.2)^2 = 75.76;",
				"for k=3, x=1.8, we have (2.8)^3 + (3.8)^3 + (5.8)^3 + (8.8)^3 + (9.8)^3 + (12.8)^3 + (14.8)^3 + (15.8)^3 = (1.8)^3 + (4.8)^3 + (6.8)^3 + (7.8)^3 + (10.8)^3 + (11.8)^3 + (13.8)^3 + (16.8)^3 = 11177.856. - _Vladimir Shevelev_, Jan 16 2012"
			],
			"maple": [
				"s := proc(n) local i,j,k,b,sum,ans; ans := [ ]; j := 0; for i while j\u003cn do sum := 0; b := convert(i,base,2); for k to nops(b) do sum := sum+b[ k ]; od; if sum mod 2 = 1 then ans := [ op(ans),i ]; j := j+1; fi; od; RETURN(ans); end; t1 := s(100); A000069 := n-\u003et1[n]; # s(k) gives first k terms.",
				"is_A000069 := n -\u003e type(add(i,i=convert(n,base,2)),odd):",
				"seq(`if`(is_A000069(i),i,NULL),i=0..40); # _Peter Luschny_, Feb 03 2011"
			],
			"mathematica": [
				"Select[Range[300], OddQ[DigitCount[ #, 2][[1]]] \u0026] (* _Stefan Steinerberger_, Mar 31 2006 *)",
				"a[ n_] := If[ n \u003c 1, 0, 2 n - 1 - Mod[ Total @ IntegerDigits[ n - 1, 2], 2]]; (* _Michael Somos_, Jun 01 2013 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, 0, 2*n - 1 - subst( Pol(binary( n-1)), x, 1) % 2)}; /* _Michael Somos_, Jun 01 2013 */",
				"(PARI) {a(n) = if( n\u003c2, n==1, if( n%2, a((n+1)/2) + n-1, -a(n/2) + 3*(n-1)))}; /* _Michael Somos_, Jun 01 2013 */",
				"(PARI) a(n)=2*n-1-hammingweight(n-1)%2 \\\\ _Charles R Greathouse IV_, Mar 22 2013",
				"(MAGMA) [ n: n in [1..130] | IsOdd(\u0026+Intseq(n, 2)) ]; // _Klaus Brockhaus_, Oct 07 2010",
				"(Haskell)",
				"a000069 n = a000069_list !! (n-1)",
				"a000069_list = [x | x \u003c- [0..], odd $ a000120 x]",
				"-- _Reinhard Zumkeller_, Feb 01 2012",
				"(Python)",
				"[n for n in range(1, 201) if bin(n)[2:].count(\"1\") % 2] # _Indranil Ghosh_, May 03 2017"
			],
			"xref": [
				"The basic sequences concerning the binary expansion of n are A000120, A000788, A000069, A001969, A023416, A059015.",
				"Complement of A001969 (the evil numbers). Cf. A133009.",
				"a(n) = 2*n+1-A010060(n)=A001969(n)+(-1)^A010060(n).",
				"First differences give A007413.",
				"Cf. A000773, A181155, A019568, A059009.",
				"Note that A000079, A083420, A002042, A002089, A132679 are subsequences.",
				"See A027697 for primes, also A230095.",
				"Cf. A005408 (odd numbers), A026424."
			],
			"keyword": "easy,core,nonn,nice,base",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"references": 275,
			"revision": 215,
			"time": "2021-12-27T22:35:53-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}