{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A228956",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 228956,
			"data": "1,1,1,1,5,9,17,84,30,127,791,2404,11454,27680,25942,137272,515947,2834056,26583034,82099932,306004652,4518630225,11242369312,8942966426,95473633156,533328765065",
			"name": "Number of undirected circular permutations i_0, i_1, ..., i_n of 0, 1, ..., n such that all the 2*n+2 numbers |i_0 +/- i_1|, |i_1 +/- i_2|, ..., |i_{n-1} +/- i_n|, |i_n +/- i_0| have the form (p-1)/2 with p an odd prime.",
			"comment": [
				"Conjecture: a(n) \u003e 0 for all n \u003e 0.",
				"Note that if i-j = (p-1)/2 and i+j = (q-1)/2 for some odd primes p and q then 4*i+2 is the sum of the two primes p and q. So the conjecture is related to Goldbach's conjecture.",
				"Zhi-Wei Sun also made the following similar conjecture:  For any integer n \u003e 5, there exists a circular permutation i_0, i_1, ..., i_n of 0, 1, ..., n such that all the 2*n+2 numbers 2*|i_k-i_{k+1}|+1 and  2*(i_k+i_{k+1})-1 (k = 0,...,n) (with i_{n+1} = i_0) are primes."
			],
			"link": [
				"Z.-W. Sun, \u003ca href=\"http://arxiv.org/abs/1309.1679\"\u003eSome new problems in additive combinatorics\u003c/a\u003e, arXiv preprint arXiv:1309.1679 [math.NT], 2013-2014."
			],
			"example": [
				"a(n) = 1 for n = 1,2,3 due to the natural circular permutation (0,...,n).",
				"a(4) = 1 due to the circular permutation (0,1,4,2,3).",
				"a(5) = 5 due to the circular permutations (0,1,2,4,5,3), (0,1,4,2,3,5), (0,1,4,5,3,2), (0,2,1,4,5,3), (0,3,2,1,4,5).",
				"a(6) = 9 due to the circular permutations",
				"  (0,1,2,4,5,3,6), (0,1,2,4,5,6,3), (0,1,4,2,3,5,6),",
				"  (0,1,4,2,3,6,5), (0,1,4,5,6,3,2), (0,2,1,4,5,3,6),",
				"  (0,2,1,4,5,6,3), (0,3,2,1,4,5,6), (0,5,4,1,2,3,6).",
				"a(7) = 17 due to the circular permutations",
				"  (0,1,2,7,4,5,3,6), (0,1,2,7,4,5,6,3), (0,1,4,7,2,3,5,6),",
				"  (0,1,4,7,2,3,6,5), (0,1,7,2,4,5,3,6), (0,1,7,2,4,5,6,3),",
				"  (0,1,7,4,2,3,5,6), (0,1,7,4,2,3,6,5), (0,1,7,4,5,6,3,2),",
				"  (0,2,1,7,4,5,3,6), (0,2,1,7,4,5,6,3), (0,2,7,1,4,5,3,6),",
				"  (0,2,7,1,4,5,6,3), (0,3,2,1,7,4,5,6), (0,3,2,7,1,4,5,6),",
				"  (0,5,4,1,7,2,3,6), (0,5,4,7,1,2,3,6)."
			],
			"mathematica": [
				"(* A program to compute required circular permutations for n = 7. To get \"undirected\" circular permutations, we should identify a circular permutation with the one of the opposite direction; for example, (0,6,3,5,4,7,2,1) is identical to (0,1,2,7,4,5,3,6) if we ignore direction. Thus a(7) is half of the number of circular permutations yielded by this program. *)",
				"p[i_,j_]:=PrimeQ[2*Abs[i-j]+1]\u0026\u0026PrimeQ[2(i+j)+1]",
				"V[i_]:=Part[Permutations[{1,2,3,4,5,6,7}],i]",
				"m=0",
				"Do[Do[If[p[If[j==0,0,Part[V[i],j]],If[j\u003c7,Part[V[i],j+1],0]]==False,Goto[aa]],{j,0,7}]; m=m+1;Print[m,\":\",\" \",0,\" \",Part[V[i],1],\" \",Part[V[i],2],\" \",Part[V[i],3],\" \",Part[V[i],4],\" \",Part[V[i],5],\" \",Part[V[i],6],\" \",Part[V[i],7]];Label[aa];Continue,{i,1,7!}]"
			],
			"xref": [
				"Cf. A000040, A051252, A002375, A228917, A228886."
			],
			"keyword": "nonn,more,hard",
			"offset": "1,5",
			"author": "_Zhi-Wei Sun_, Sep 09 2013",
			"ext": [
				"a(10)-a(26) from _Max Alekseyev_, Sep 17 2013"
			],
			"references": 6,
			"revision": 32,
			"time": "2018-04-23T11:46:36-04:00",
			"created": "2013-09-09T09:44:42-04:00"
		}
	]
}