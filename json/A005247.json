{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005247",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5247,
			"id": "M0149",
			"data": "2,1,3,2,7,5,18,13,47,34,123,89,322,233,843,610,2207,1597,5778,4181,15127,10946,39603,28657,103682,75025,271443,196418,710647,514229,1860498,1346269,4870847,3524578,12752043,9227465,33385282,24157817",
			"name": "a(n) = 3*a(n-2) - a(n-4), a(0)=2, a(1)=1, a(2)=3, a(3)=2. Alternates Lucas (A000032) and Fibonacci (A000045) sequences for even and odd n.",
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A005247/b005247.txt\"\u003eTable of n, a(n) for n=0..500\u003c/a\u003e",
				"T. Crilly, \u003ca href=\"http://www.jstor.org/stable/3617570\"\u003eDouble sequences of positive integers\u003c/a\u003e, Math. Gaz., 69 (1985), 263-271.",
				"R. K. Guy, \u003ca href=\"/A005246/a005246.pdf\"\u003eLetter to N. J. A. Sloane, Feb 1986\u003c/a\u003e",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,3,0,-1)."
			],
			"formula": [
				"a(0)=2, a(1)=1, a(2)=3, a(n) = (1+a(n-1)a(n-2))/a(n-3), n \u003e= 3. a(-n) = a(n).",
				"G.f.: (2+x-3*x^2-x^3)/((1-x-x^2)*(1+x-x^2))",
				"a(n) = F(n) if n odd, a(n) = L(n) if n even. a(n) = F(n+1)+(-1)^nF(n-1). - Mario Catalani (mario.catalani(AT)unito.it), Sep 20 2002",
				"a(n) = ((5+sqrt(5))/10)*(((1+sqrt(5))/2)^n+((-1+sqrt(5))/2)^n)+((5-sqrt(5))/10)*(((1-sqrt(5))/2)^n+((-1-sqrt(5))/2)^n). With additional leading 1: a(n)=((sqrt(5))/5)*(((1+sqrt(5))/2)^n-((1-sqrt(5))/2)^n)+((5+3*sqrt(5))/10)*((-1+sqrt(5))/2)^n+((5-3*sqrt(5))/10)*((-1-sqrt(5))/2)^n. - _Tim Monahan_, Jul 25 2011",
				"From _Peter Bala_, Jan 11 2013: (Start)",
				"Let phi = 1/2*(sqrt(5) - 1). This sequence is the simple continued fraction expansion of the real number 1 + product {n \u003e= 0} (1 + sqrt(5)*phi^(4*n+1))/(1 + sqrt(5)*phi^(4*n+3)) = 2.77616 23282 02325 23857 ... = 2 + 1/(1 + 1/(3 + 1/(2 + 1/(7 + ...)))). Cf. A005248.",
				"Furthermore, for k = 0,1,2,... the simple continued fraction expansion of 1 + product {n \u003e= 0} (1 + 1/5^k*sqrt(5)*phi^(4*n+1))/(1 + 1/5^k*sqrt(5)*phi^(4*n+3)) equals [2; 1*5^k, 3, 2*5^k, 7, 5*5^k, 18, 13*5^k, 47, ...]. (End)",
				"a(n) = hypergeom([(1-n)/2, n mod 2 - n/2], [1 - n], -4) for n \u003e 2. - _Peter Luschny_, Sep 03 2019"
			],
			"maple": [
				"with(combinat): A005247 := n-\u003e if n mod 2 = 1 then fibonacci(n) else fibonacci(n+1)+fibonacci(n-1); fi;",
				"A005247:=-(z+1)*(3*z**2-z-1)/(z**2-z-1)/(z**2+z-1); # _Simon Plouffe_ in his 1992 dissertation. Gives sequence with an additional leading 1."
			],
			"mathematica": [
				"CoefficientList[Series[(2 + x - 3x^2 - x^3)/(1 - 3x^2 + x^4), {x, 0, 40}], x]",
				"LinearRecurrence[{0,3,0,-1},{2,1,3,2},50] (* _Harvey P. Dale_, Oct 10 2012 *)"
			],
			"program": [
				"(PARI) a(n)=if(n%2,fibonacci(n),fibonacci(n+1)+fibonacci(n-1))",
				"(Haskell)",
				"a005247 n = a005247_list !! n",
				"a005247_list = f a000032_list a000045_list where",
				"   f (x:_:xs) (_:y:ys) = x : y : f xs ys",
				"-- _Reinhard Zumkeller_, Dec 27 2012",
				"(MAGMA) I:=[2,1,3,2]; [n le 4 select I[n] else 3*Self(n-2) - Self(n-2): n in [1..30]]; // _G. C. Greubel_, Dec 21 2017"
			],
			"xref": [
				"Cf. A000032, A000045, A005013, A005013. A005248."
			],
			"keyword": "nonn,nice,easy",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Additional comments from _Michael Somos_, May 01 2000"
			],
			"references": 6,
			"revision": 54,
			"time": "2021-03-12T22:32:36-05:00",
			"created": "1991-05-20T03:00:00-04:00"
		}
	]
}