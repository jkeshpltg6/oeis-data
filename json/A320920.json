{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A320920",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 320920,
			"data": "1,4,9,33,28,165,54,1029,40832,31752,28680,2588680,2162700,12996613,12341252,4516741125,500367376,133207162881,93770874890,7043274506259,40985291653137,70766492123145,321901427163142,58731756479578128,676814631896875010,6820060161969750025",
			"name": "a(n) is the smallest number m such that binomial(m,n) is nonzero and is divisible by n!.",
			"comment": [
				"a(n) is such that a nontrivial n-symmetric permutation of [1..a(n)] might exist."
			],
			"link": [
				"Bert Dobbelaere, \u003ca href=\"/A320920/b320920.txt\"\u003eTable of n, a(n) for n = 1..34\u003c/a\u003e",
				"Bert Dobbelaere, \u003ca href=\"/A320920/a320920.py.txt\"\u003ePython program\u003c/a\u003e",
				"Tanya Khovanova, \u003ca href=\"https://blog.tanyakhovanova.com/2018/10/3-symmetric-permutations/#comment-12716\"\u003e3-Symmetric Permutations\u003c/a\u003e"
			],
			"example": [
				"The sequence of binomial coefficients C(n,3) starts as: 0, 0, 1, 4, 10, 20, 35, 56, 84, 120, 165, and so on. The smallest nonzero number divisible by 3! is 84, which is C(9,3). Therefore a(3) = 9."
			],
			"mathematica": [
				"a[n_] := Module[{w, m, bc}, {w, m} = {n!, n}; bc[i_] := Binomial[n-1, i] ~Mod~ w; While[True, bc[n] = (bc[n-1] + bc[n]) ~Mod~ w; If[bc[n] == 0, Return[m]]; For[i = n-1, i \u003e= 0, i--, bc[i] = (bc[i-1] + bc[i]) ~Mod~ w]; m++]];",
				"Array[a, 12] (* _Jean-François Alcover_, May 31 2019, after _Chai Wah Wu_ *)"
			],
			"program": [
				"(Python)",
				"from sympy import factorial, binomial",
				"def A320920(n):",
				"    w, m = int(factorial(n)), n",
				"    bc = [int(binomial(n-1,i)) % w for i in range(n+1)]",
				"    while True:",
				"        bc[n] = (bc[n-1]+bc[n]) % w",
				"        if bc[n] == 0:",
				"            return m",
				"        for i in range(n-1,0,-1):",
				"            bc[i] = (bc[i-1]+bc[i]) % w",
				"        m += 1 # _Chai Wah Wu_, Oct 25 2018"
			],
			"xref": [
				"Cf. A042948, A316775, A320919."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Tanya Khovanova_, Oct 24 2018",
			"ext": [
				"a(14)-a(15) from _Alois P. Heinz_, Oct 24 2018",
				"a(16)-a(17) from _Chai Wah Wu_, Oct 25 2018",
				"a(18)-a(19) from _Giovanni Resta_, Oct 26 2018",
				"a(20) from _Giovanni Resta_, Oct 27 2018",
				"a(21) and beyond from _Bert Dobbelaere_, Feb 11 2020"
			],
			"references": 1,
			"revision": 38,
			"time": "2020-02-11T18:55:43-05:00",
			"created": "2018-10-25T14:55:19-04:00"
		}
	]
}