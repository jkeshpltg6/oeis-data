{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A209350",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 209350,
			"data": "1,0,1,5,9,11,16,19,25,29,36,41,49,55,64,71,81,89,100,109,121,131,144,155,169,181,196,209,225,239,256,271,289,305,324,341,361,379,400,419,441,461,484,505,529,551,576,599,625,649,676,701,729,755,784,811,841",
			"name": "Number of initially rising meander words, where each letter of the cyclic n-ary alphabet occurs twice.",
			"comment": [
				"In a meander word letters of neighboring positions have to be neighbors in the alphabet, where in a cyclic alphabet the first and the last letters are considered neighbors too.  The words are not considered cyclic here.",
				"A word is initially rising if it is empty or if it begins with the first letter of the alphabet that can only be followed by the second letter in this word position.",
				"a(n) is also the number of (2*n-1)-step walks on n-dimensional cubic lattice from (1,0,...,0) to (2,2,...,2) with positive unit steps in all dimensions such that the indices of dimensions used in consecutive steps differ by 1 or are in the set {1,n}."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A209350/b209350.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,0,-2,1)."
			],
			"formula": [
				"G.f.: -(3*x^6-5*x^5-2*x^4+5*x^3+x^2-2*x+1) / ((x+1)*(x-1)^3).",
				"a(n) = (n-1)^2 if n\u003c3, a(n) = (n/2+1)^2 - (n mod 2)*5/4 else."
			],
			"example": [
				"a(0) = 1: the empty word.",
				"a(1) = 0 = |{ }|.",
				"a(2) = 1 = |{abab}|.",
				"a(3) = 5 = |{abacbc, abcabc, abcacb, abcbac, abcbca}|.",
				"a(4) = 9 = |{ababcdcd, abadcbcd, abadcdcb, abcbadcd, abcbcdad, abcdabcd, abcdadcb, abcdcbad, abcdcdab}|."
			],
			"maple": [
				"a:= n-\u003e `if`(n\u003c3, (n-1)^2, (n/2+1)^2 -(n mod 2)*5/4):",
				"seq(a(n), n=0..60);"
			],
			"mathematica": [
				"LinearRecurrence[{2,0,-2,1},{1,0,1,5,9,11,16},60] (* _Harvey P. Dale_, Jan 02 2020 *)"
			],
			"xref": [
				"Row n=2 of A209349.",
				"First differences for n\u003e2 give: A084964(n+1), A097065(n+3).",
				"Cf. A245578."
			],
			"keyword": "nonn,walk,easy",
			"offset": "0,4",
			"author": "_Alois P. Heinz_, Mar 06 2012",
			"references": 4,
			"revision": 18,
			"time": "2020-01-02T14:33:06-05:00",
			"created": "2012-03-12T12:07:01-04:00"
		}
	]
}