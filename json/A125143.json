{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A125143",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 125143,
			"data": "1,-3,9,-3,-279,2997,-19431,65853,292329,-7202523,69363009,-407637387,702049401,17222388453,-261933431751,2181064727997,-10299472204311,-15361051476987,900537860383569,-10586290198314843,74892552149042721,-235054958584593843",
			"name": "Almkvist-Zudilin numbers: Sum_{k=0..n} (-1)^(n-k) * ((3^(n-3*k) * (3*k)!) / (k!)^3) * binomial(n,3*k) * binomial(n+k,k).",
			"comment": [
				"Apart from signs, this is one of the Apery-like sequences - see Cross-references. - _Hugo Pfoertner_, Aug 06 2017",
				"Diagonal of rational function 1/(1 - (x + y + z + w - 27*x*y*z*w)). - _Gheorghe Coserea_, Oct 14 2018",
				"Named after the Swedish mathematician Gert Einar Torsten Almkvist (1934-2018) and the Russian mathematician Wadim Walentinowitsch Zudilin (b. 1970). - _Amiram Eldar_, Jun 23 2021"
			],
			"reference": [
				"G. Almkvist and W. Zudilin, Differential equations, mirror maps and zeta values. In Mirror Symmetry V, N. Yui, S.-T. Yau, and J. D. Lewis (eds.), AMS/IP Studies in Advanced Mathematics 38 (2007), International Press and Amer. Math. Soc., pp. 481-515. Cited in Chan \u0026 Verrill.",
				"Helena Verrill, in a talk at the annual meeting of the Amer. Math. Soc., New Orleans, LA, Jan 2007 on \"Series for 1/pi\"."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A125143/b125143.txt\"\u003eTable of n, a(n) for n = 0..1052\u003c/a\u003e (terms 0..200 from Arkadiusz Wesolowski)",
				"Gert Almkvist, Christian Krattenthaler, and Joakim Petersson, \u003ca href=\"http://www.emis.de/journals/EM/expmath/volumes/12/12.4/Almkvist.pdf\"\u003eSome new formulas for pi\u003c/a\u003e, Experiment. Math., Vol. 12 (2003), pp. 441-456. (Math Rev MR2043994 by W. Zudilin)",
				"G. Almkvist and W. Zudilin, \u003ca href=\"https://arxiv.org/abs/math/0402386\"\u003eDifferential equations, mirror maps and zeta values\u003c/a\u003e, arXiv:math/0402386 [math.NT], 2004.",
				"Tewodros Amdeberhan, and Roberto Tauraso, \u003ca href=\"http://arxiv.org/abs/1506.08437\"\u003eSupercongruences for the Almkvist-Zudilin numbers\u003c/a\u003e, arXiv:1506.08437 [math.NT], 2015.",
				"Yuliy Baryshnikov, Stephen Melczer, Robin Pemantle and Armin Straub, \u003ca href=\"https://arxiv.org/abs/1804.10929\"\u003eDiagonal asymptotics for symmetric rational functions via ACSV\u003c/a\u003e, LIPIcs Proceedings of Analysis of Algorithms 2018, arXiv:1804.10929 [math.CO], 2018.",
				"Heng Huat Chan and Helena Verrill, \u003ca href=\"http://intlpress.com/site/pub/files/_fulltext/journals/mrl/2009/0016/0003/MRL-2009-0016-0003-a003.pdf\"\u003eThe Apery numbers, the Almkvist-Zudilin numbers and new series for 1/Pi\u003c/a\u003e, Math. Res. Lett., Vol. 16, No. 3 (2009), pp. 405-420.",
				"Ofir Gorodetsky, \u003ca href=\"https://arxiv.org/abs/2102.11839\"\u003eNew representations for all sporadic Apéry-like sequences, with applications to congruences\u003c/a\u003e, arXiv:2102.11839 [math.NT], 2021. See delta p. 3.",
				"Ji-Cai Liu, \u003ca href=\"https://arxiv.org/abs/2008.06675\"\u003eA p-adic analogue of Chan and Verrill's formula for 1/Pi\u003c/a\u003e, arXiv:2008.06675 [math.NT], 2020.",
				"Amita Malik and Armin Straub, \u003ca href=\"https://doi.org/10.1007/s40993-016-0036-8\"\u003eDivisibility properties of sporadic Apéry-like numbers\u003c/a\u003e, Research in Number Theory, Vol. 2, (2016), Article 5.",
				"Zhi-Hong Sun, \u003ca href=\"https://arxiv.org/abs/1803.10051\"\u003eCongruences for Apéry-like numbers\u003c/a\u003e, arXiv:1803.10051 [math.NT], 2018.",
				"Zhi-Hong Sun, \u003ca href=\"https://arxiv.org/abs/2002.12072\"\u003eSuper congruences concerning binomial coefficients and Apéry-like numbers\u003c/a\u003e, arXiv:2002.12072 [math.NT], 2020.",
				"Zhi-Hong Sun, \u003ca href=\"https://arxiv.org/abs/2004.07172\"\u003eNew congruences involving Apéry-like numbers\u003c/a\u003e, arXiv:2004.07172 [math.NT], 2020."
			],
			"formula": [
				"a(n) = Sum_{k=0..n} (-1)^(n-k) * ((3^(n-3*k) * (3*k)!) / (k!)^3) * binomial(n,3*k) * binomial(n+k,k) . - _Arkadiusz Wesolowski_, Jul 13 2011",
				"Recurrence: n^3*a(n) = -(2*n-1)*(7*n^2 - 7*n + 3)*a(n-1) - 81*(n-1)^3*a(n-2). - _Vaclav Kotesovec_, Sep 11 2013",
				"Lim sup n-\u003einfinity |a(n)|^(1/n) = 9. - _Vaclav Kotesovec_, Sep 11 2013",
				"G.f. y=A(x) satisfies: 0 = x^2*(81*x^2 + 14*x + 1)*y''' + 3*x*(162*x^2 + 21*x + 1)*y'' + (21*x + 1)*(27*x + 1)*y' + 3*(27*x + 1)*y. - _Gheorghe Coserea_, Oct 15 2018",
				"G.f.: hypergeom([1/8, 5/8], [1], -256*x^3/((81*x^2 + 14*x + 1)*(-x + 1)^2))^2/((81*x^2 + 14*x + 1)^(1/4)*sqrt(-x + 1)). - _Sergey Yurkevich_, Aug 31 2020"
			],
			"mathematica": [
				"Table[Sum[(-1)^(n-k)*((3^(n-3*k)*(3*k)!)/(k!)^3) *Binomial[n,3*k] *Binomial[n+k,k],{k,0,n}],{n,0,20}] (* _Vaclav Kotesovec_, Sep 11 2013 *)"
			],
			"program": [
				"(PARI) a(n) = sum(k=0,n, (-1)^(n-k)*((3^(n-3*k)*(3*k)!)/(k!)^3)*binomial(n,3*k)*binomial(n+k,k) );"
			],
			"xref": [
				"The Apéry-like numbers [or Apéry-like sequences, Apery-like numbers, Apery-like sequences] include A000172, A000984, A002893, A002895, A005258, A005259, A005260, A006077, A036917, A063007, A081085, A093388, A125143 (apart from signs), A143003, A143007, A143413, A143414, A143415, A143583, A183204, A214262, A219692,A226535, A227216, A227454, A229111 (apart from signs), A260667, A260832, A262177, A264541, A264542, A279619, A290575, A290576. (The term \"Apery-like\" is not well-defined.)",
				"For primes that do not divide the terms of the sequences A000172, A005258, A002893, A081085, A006077, A093388, A125143, A229111, A002895, A290575, A290576, A005259 see A260793, A291275-A291284 and A133370 respectively."
			],
			"keyword": "easy,sign",
			"offset": "0,2",
			"author": "_R. K. Guy_, Jan 11 2007",
			"ext": [
				"Edited and more terms added by _Arkadiusz Wesolowski_, Jul 13 2011"
			],
			"references": 47,
			"revision": 100,
			"time": "2021-06-23T09:05:19-04:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}