{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A118801",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 118801,
			"data": "1,1,-1,1,-3,1,1,-7,5,-1,1,-15,17,-7,1,1,-31,49,-31,9,-1,1,-63,129,-111,49,-11,1,1,-127,321,-351,209,-71,13,-1,1,-255,769,-1023,769,-351,97,-15,1,1,-511,1793,-2815,2561,-1471,545,-127,17,-1,1,-1023,4097,-7423,7937,-5503,2561,-799,161,-19,1",
			"name": "Triangle T that satisfies the matrix products: C*[T^-1]*C = T and T*[C^-1]*T = C, where C is Pascal's triangle.",
			"comment": [
				"Matrix inverse is triangle A118800. Row sums are: (1-n). Unsigned row sums equal A007051(n) = (3^n + 1)/2. Row squared sums equal A118802. Antidiagonal sums equal A080956(n) = (n+1)(2-n)/2. Unsigned antidiagonal sums form A024537 (with offset).",
				"T = C^2*D^-1 where matrix product D = C^-1*T*C = T^-1*C^2 has only 2 nonzero diagonals: D(n,n)=-D(n+1,n)=(-1)^n, with zeros elsewhere. Also, [B^-1]*T*[B^-1] = B*[T^-1]*B forms a self-inverse matrix, where B^2 = C and B(n,k) = C(n,k)/2^(n-k). - _Paul D. Hanna_, May 04 2006",
				"Riordan array ( 1/(1 - x), -x/(1 - 2*x) ) The matrix square is the Riordan array ( (1 - 2*x)/(1 - x)^2, x ), which belongs to the Appell subgroup of the Riordan group. See the Example section below. - _Peter Bala_, Jul 17 2013"
			],
			"link": [
				"M. Shattuck and T. Waldhauser, \u003ca href=\"http://www.fq.math.ca/Papers1/48-4/Shattuck_Waldhauser.pdf\"\u003eProofs of some binomial identities using the method of last squares\u003c/a\u003e, Fib. Q., 48 (2010), 290-297."
			],
			"formula": [
				"T(n,k) = 1 + (-1)^k*2^(n-k+1)*Sum_{j=0..[k/2]} C(n-2j-2,k-2j-1) for n\u003e=k\u003e=0 with T(0,0) = 1.",
				"For k\u003e0, T(n,k) = -T(n-1,k-1) + 2*T(n-1,k). - _Gerald McGarvey_, Aug 05 2006",
				"O.g.f.: (1 - 2*t)/(1 - t) * 1/(1 + t*(x - 2)) = 1 + (1 - x)*t + (1 - 3*x + x^2)*t^2 + (1 - 7*x + 5*x^2 - x^3)*t^3 + .... - _Peter Bala_, Jul 17 2013",
				"From _Tom Copeland_, Nov 17 2016: (Start)",
				"Let M = A200139^(-1) = (unsigned A118800)^(-1) and NpdP be the signed padded Pascal matrix defined in A097805. Then T(n,k) = (-1)^n* M(n,k) and T = P*NpdP = (A239473)^(-1)*P^(-1) = P*A167374*P^(-1) = A156644*P^(-1), where P is the Pascal matrix A007318 with inverse A130595. Cf. A112857.",
				"Signed P^2 = signed A032807 = T*A167374. (End)"
			],
			"example": [
				"Formulas for initial columns are, for n\u003e=0:",
				"T(n+1,1) = 1 - 2^(n+1);",
				"T(n+2,2) = 1 + 2^(n+1)*n;",
				"T(n+3,3) = 1 - 2^(n+1)*(n*(n+1)/2 + 1);",
				"T(n+4,4) = 1 + 2^(n+1)*(n*(n+1)*(n+2)/6 + n);",
				"T(n+5,5) = 1 - 2^(n+1)*(n*(n+1)*(n+2)*(n+3)/24 + n*(n+1)/2 + 1).",
				"Triangle begins:",
				"1;",
				"1,-1;",
				"1,-3,1;",
				"1,-7,5,-1;",
				"1,-15,17,-7,1;",
				"1,-31,49,-31,9,-1;",
				"1,-63,129,-111,49,-11,1;",
				"1,-127,321,-351,209,-71,13,-1;",
				"1,-255,769,-1023,769,-351,97,-15,1;",
				"1,-511,1793,-2815,2561,-1471,545,-127,17,-1;",
				"1,-1023,4097,-7423,7937,-5503,2561,-799,161,-19,1; ...",
				"The matrix square, T^2, starts:",
				"1;",
				"0,1;",
				"-1,0,1;",
				"-2,-1,0,1;",
				"-3,-2,-1,0,1;",
				"-4,-3,-2,-1,0,1; ...",
				"where all columns are the same.",
				"The matrix product C^-1*T*C = T^-1*C^2 is:",
				"1;",
				"-1,-1;",
				"0, 1, 1;",
				"0, 0,-1,-1;",
				"0, 0, 0, 1, 1; ...",
				"where C(n,k) = n!/(n-k)!/k!."
			],
			"mathematica": [
				"Table[(1 + (-1)^k*2^(n - k + 1)*Sum[ Binomial[n - 2 j - 2, k - 2 j - 1], {j, 0, Floor[k/2]}]) - 4 Boole[And[n == 1, k == 0]], {n, 0, 10}, {k, 0, n}] // Flatten (* _Michael De Vlieger_, Nov 24 2016 *)"
			],
			"program": [
				"(PARI) {T(n,k)=if(n==0\u0026k==0,1,1+(-1)^k*2^(n-k+1)*sum(j=0,k\\2,binomial(n-2*j-2,k-2*j-1)))}"
			],
			"xref": [
				"Cf. A118800 (inverse), A007051 (unsigned row sums), A118802 (Row squared sums), A080956 (antidiagonal sums), A024537 (unsigned antidiagonal sums).",
				"A145661, A119258 and A118801 are all essentially the same (see the Shattuck and Waldhauser paper). - Tamas Waldhauser, Jul 25 2011",
				"Cf. A007318, A032807, A097805, A103595, A112857, A156644, A167374, A200139, A239473."
			],
			"keyword": "sign,tabl",
			"offset": "0,5",
			"author": "_Paul D. Hanna_, May 02 2006",
			"references": 10,
			"revision": 33,
			"time": "2016-11-25T18:47:27-05:00",
			"created": "2006-05-19T03:00:00-04:00"
		}
	]
}