{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A061551",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 61551,
			"data": "1,1,2,3,6,10,20,35,69,124,241,440,846,1560,2977,5525,10490,19551,36994,69142,130532,244419,460737,863788,1626629,3052100,5743674,10782928,20283121,38092457,71632290,134560491,252989326,475313762",
			"name": "Number of paths along a corridor width 8, starting from one side.",
			"comment": [
				"Counts all paths of length n starting at initial node on the path graph P_8. - _Paul Barry_, May 11 2004",
				"The a(n) represent the number of possible chess games, ignoring the fifty-move and the triple repetition rules, after n moves by White in the following position: White Ka1, pawns a2, b6, d2, d6 and g2; Black Ka8, Bc8, pawns a3, b7, d3, d7 and g3. - _Johannes W. Meijer_, May 29 2010",
				"Define the 4 X 4 tridiagonal unit-primitive matrix (see [Jeffery]) M = A_{9,1} = [0,1,0,0; 1,0,1,0; 0,1,0,1; 0,0,1,1]; then a(n)=[M^n]_(4,4). - _L. Edson Jeffery_, Mar 18 2011",
				"a(n) is the length of n-th word derived by certain iterated substitutions on four letters {1,2,3,4} as follows. Define the substitution rules 1 -\u003e {2}, 2 -\u003e {1,3}, 3 -\u003e {2,4}, 4 -\u003e {3,4}, in which \",\" denotes concatenation, so 1 -\u003e 2, 2 -\u003e 13, 3 -\u003e 24, 4 -\u003e 34. Let w(k) be the k-th word formed by applying the substitution rules to each letter (digit) in word w(k-1), k\u003e0, putting w(0) = 1. Then, for n=0,1,..., {w(n)} = {1, 2, 13, 224, 131334, 2242242434, 13133413133413342434, ...} in which {length(w(n)} = {1,1,2,3,6,10,...} = A061551. The maps 1 -\u003e 2, etc., are given by the above matrix A_{9,1} by taking i -\u003e {j : [A_{9,1}]_(i,j) \u003c\u003e 0}, i, j in {1,2,3,4}. Moreover, the entry in row 1 and column j of [A_{9,1}]^n gives the relative frequency of the letter j in the n-th word w(n). Finally, the sum of the first-row entries of [A_{9,1}]^n again gives a(n), so obviously a(n) = sum of relative frequencies of each j in word w(n). - _L. Edson Jeffery_, Feb 06 2012",
				"Range of row n of the circular Pascal array of order 9. - _Shaun V. Ault_, Jun 05 2014",
				"In general, a(n,m) = (2^n/(m+1))*Sum_{r=1..m} (1-(-1)^r)*cos(Pi*r/(m+1))^n*(1+cos(Pi*r/(m+1))) gives the number of paths of length n starting at the initial node on the path graph P_m. Here we have m=8. - _Herbert Kociemba_, Sep 17 2020"
			],
			"link": [
				"Harvey P. Dale, \u003ca href=\"/A061551/b061551.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Shaun V. Ault and Charles Kicey, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2014.05.020\"\u003eCounting paths in corridors using circular Pascal arrays\u003c/a\u003e, Discrete Mathematics (2014).",
				"Shaun V. Ault and Charles Kicey, \u003ca href=\"http://arxiv.org/abs/1407.2197\"\u003eCounting paths in corridors using circular Pascal arrays\u003c/a\u003e, arXiv:1407.2197 [math.CO], (8-July-2014)",
				"Johann Cigler, \u003ca href=\"http://arxiv.org/abs/1501.04750\"\u003eSome remarks and conjectures related to lattice paths in strips along the x-axis\u003c/a\u003e, arXiv:1501.04750 [math.CO], 2015-2016.",
				"Nachum Dershowitz, \u003ca href=\"https://arxiv.org/abs/2006.06516\"\u003eBetween Broadway and the Hudson\u003c/a\u003e, arXiv:2006.06516 [math.CO], 2020.",
				"L. E. Jeffery, \u003ca href=\"/wiki/User:L._Edson_Jeffery/Unit-Primitive_Matrices\"\u003eUnit-primitive matrices\u003c/a\u003e",
				"Marc A. A. Van Leeuwen, \u003ca href=\"http://arxiv.org/abs/1010.4847\"\u003eSome simple bijections involving lattice walks and ballot sequences\u003c/a\u003e, arXiv:1010.4847 [math.CO], (23-October-2010)",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,3,-2,-1)."
			],
			"formula": [
				"a(n) = sum(b(n,i)) where b(n,0) = b(n,9) = 0, b(0,1)=1, b(0, n)=0 if n!=1 and b(n,i) = b(n-1,i) + b(n+1,i) if 0 \u003c n \u003c 9.",
				"From _Emeric Deutsch_, Aug 14 2006: (Start)",
				"G.f.: (1-2*x^2)/((1-x)*(1-3*x^2-x^3)).",
				"a(n) = 7*a(n-2) - 15*a(n-4) + 10*a(n-6) - a(n-8). (End)",
				"a(2*n) = A094854(n) and a(2*n+1) = A094855(n). - _Johannes W. Meijer_, May 29 2010",
				"a(n) = a(n-1) + 3*a(n-2) - 2*a(n-3) - a(n-4), for n \u003e 3, with {a(k)}={1,1,2,3}, k=0,1,2,3. - _L. Edson Jeffery_, Mar 18 2011",
				"a(n) = A187498(3*n + 2). - _L. Edson Jeffery_, Mar 18 2011",
				"a(n) = A205573(3,n). - _L. Edson Jeffery_, Feb 06 2012",
				"G.f.: 1 / (1 - x / (1 - x / (1 + x / (1 + x / (1 - x / (1 - x / (1 + x))))))). - _Michael Somos_, Feb 08 2015",
				"a(n) = 2^n/9*Sum_{r=1..8} (1-(-1)^r)cos(Pi*r/9)^n*(1+cos(Pi*r/9)). - _Herbert Kociemba_, Sep 17 2020"
			],
			"example": [
				"G.f. = 1 + x + 2*x^2 + 3*x^3 + 6*x^4 + 10*x^5 + 20*x^6 + 35*x^7 + 69*x^8 + ...."
			],
			"maple": [
				"a[0]:=1: a[1]:=1: a[2]:=2: a[3]:=3: a[4]:=6: a[5]:=10: a[6]:=20: a[7]:=35: for n from 8 to 33 do a[n]:=7*a[n-2]-15*a[n-4]+10*a[n-6]-a[n-8] od: seq(a[n],n=0..33); # _Emeric Deutsch_, Aug 14 2006",
				"with(GraphTheory): P:=8: G:=PathGraph(P): A:= AdjacencyMatrix(G): nmax:=33; for n from 0 to nmax do B(n):=A^n; a(n):=add(B(n)[1,k],k=1..P); od: seq(a(n),n=0..nmax); # _Johannes W. Meijer_, May 29 2010",
				"X := j -\u003e (-1)^(j/9) - (-1)^(1-j/9):",
				"a := k -\u003e add((2 + X(j))*X(j)^k, j in [1, 3, 5, 7])/9:",
				"seq(simplify(a(n)), n=0..30); # _Peter Luschny_, Sep 17 2020"
			],
			"mathematica": [
				"LinearRecurrence[{1,3,-2,-1},{1,1,2,3},40] (* _Harvey P. Dale_, Dec 19 2011 *)",
				"a[n_,m_]:=2^(n+1)/(m+1) Module[{x=(Pi r)/(m+1)},Sum[Cos[x]^n (1+Cos[x]),{r,1,m,2}]]",
				"Table[a[n,8],{n,0,40}]//Round (* _Herbert Kociemba_, Sep 17 2020 *)"
			],
			"xref": [
				"Narrower corridors effectively produce A000007, A000012, A016116, A000045, A038754, A028495, A030436, A061551, A178381, A336675, A336678.",
				"An infinitely wide corridor (i.e., just one wall) would produce A001405.",
				"Equivalently, the above mentioned corridor numbers are exactly the ranges of the circular Pascal array of order d = 2, 3, 4, 5, 6, 7, 8, respectively, and this is true for any natural number d greater than or equal to 2.",
				"a(n) = A094718(8, n).",
				"Cf. A030436 and A178381."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Henry Bottomley_, May 16 2001",
			"references": 13,
			"revision": 80,
			"time": "2020-09-18T06:40:22-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}