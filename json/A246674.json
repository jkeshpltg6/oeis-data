{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A246674",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 246674,
			"data": "1,1,1,3,1,1,3,7,1,1,1,3,3,3,7,15,1,1,1,3,1,1,3,7,3,3,3,9,7,7,15,31,1,1,1,3,1,1,3,7,1,1,1,3,3,3,7,15,3,3,3,9,3,3,9,21,7,7,7,21,15,15,31,63,1,1,1,3,1,1,3,7,1,1,1,3,3,3,7,15,1,1,1,3,1,1,3,7,3,3,3,9,7,7,15,31,3,3,3,9,3,3,9,21,3,3,3,9,9,9,21,45,7,7,7,21,7,7,21,49,15,15,15,45,31,31,63,127,1",
			"name": "Run Length Transform of A000225.",
			"comment": [
				"a(n) can be also computed by replacing all consecutive runs of zeros in the binary expansion of n with * (multiplication sign), and then performing that multiplication, still in binary, after which the result is converted into decimal. See the example below."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A246674/b246674.txt\"\u003eTable of n, a(n) for n = 0..8192\u003c/a\u003e"
			],
			"formula": [
				"For all n \u003e= 0, a(A051179(n)) = A247282(A051179(n)) = A051179(n)."
			],
			"example": [
				"115 is '1110011' in binary. The run lengths of 1-runs are 2 and 3, thus a(115) = A000225(2) * A000225(3) = ((2^2)-1) * ((2^3)-1) = 3*7 = 21.",
				"The same result can be also obtained more directly, by realizing that '111' and '11' are the binary representations of 7 and 3, and 7*3 = 21.",
				"From _Omar E. Pol_, Feb 15 2015: (Start)",
				"Written as an irregular triangle in which row lengths are the terms of A011782:",
				"1;",
				"1;",
				"1,3;",
				"1,1,3,7;",
				"1,1,1,3,3,3,7,15;",
				"1,1,1,3,1,1,3,7,3,3,3,9,7,7,15,31;",
				"1,1,1,3,1,1,3,7,1,1,1,3,3,3,7,15,3,3,3,9,3,3,9,21,7,7,7,21,15,15,31,63;",
				"...",
				"Right border gives 1 together with the positive terms of A000225.",
				"(End)"
			],
			"mathematica": [
				"f[n_] := 2^n - 1; Table[Times @@ (f[Length[#]]\u0026) /@ Select[ Split[ IntegerDigits[n, 2]], #[[1]] == 1\u0026], {n, 0, 100}] (* _Jean-François Alcover_, Jul 11 2017 *)"
			],
			"program": [
				"(MIT/GNU Scheme)",
				"(define (A246674 n) (fold-left (lambda (a r) (* a (A000225 r))) 1 (bisect (reverse (binexp-\u003eruncount1list n)) (- 1 (modulo n 2)))))",
				"(define (A000225 n) (- (A000079 n) 1))",
				"(define (A000079 n) (expt 2 n))",
				";; Other functions as in A227349."
			],
			"xref": [
				"Cf. A003714 (gives the positions of ones).",
				"Cf. A000225, A051179.",
				"A001316 is obtained when the same transformation is applied to A000079, the powers of two.",
				"Run Length Transforms of other sequences: A071053, A227349, A246588, A246595, A246596, A246660, A246661, A246685, A247282."
			],
			"keyword": "nonn,base",
			"offset": "0,4",
			"author": "_Antti Karttunen_, Sep 08 2014",
			"references": 10,
			"revision": 25,
			"time": "2017-07-11T11:53:35-04:00",
			"created": "2014-09-23T10:57:42-04:00"
		}
	]
}