{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A232343",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 232343,
			"data": "1,-1,2,0,3,-2,4,0,5,-5,8,0,7,-4,8,0,9,-8,10,0,14,-6,12,0,16,-14,14,0,15,-8,20,0,17,-14,18,0,19,-10,24,0,26,-21,22,0,23,-16,28,0,25,-20,32,0,32,-14,28,0,29,-28,30,0,38,-16,32,0,33,-31,40,0,40",
			"name": "Expansion of q^(-5/3) * c(q^2)^3 / (9 * c(q)) in powers of q where c() is a cubic AGM theta function.",
			"comment": [
				"Cubic AGM theta functions: a(q) (see A004016), b(q) (A005928), c(q) (A005882)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A232343/b232343.txt\"\u003eTable of n, a(n) for n = 0..2500\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-5/3) * eta(q) * eta(q^6)^9 / (eta(q^2) * eta(q^3))^3 in powers of q.",
				"Euler transform of period 6 sequence [-1, 2, 2, 2, -1, -4, ...].",
				"a(n) = 1/6 * b(3*n + 5) where b() is multiplicative with b(2^e) = 2 - 2^e, b(3^e) = 0^e, b(p^e) = (p^(e+1) - 1) / (p - 1) if p\u003e3.",
				"a(2*n) = A098098(n). a(4*n + 1) = - A033686(n). a(4*n + 3) = 0."
			],
			"example": [
				"G.f. = 1 - x + 2*x^2 + 3*x^4 - 2*x^5 + 4*x^6 + 5*x^8 - 5*x^9 + 8*x^10 + ...",
				"G.f. = q^5 - q^8 + 2*q^11 + 3*q^17 - 2*q^20 + 4*q^23 + 5*q^29 - 5*q^32 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x] QPochhammer[ x^6]^9 / (QPochhammer[ x^2] QPochhammer[ x^3])^3, {x, 0, n}];",
				"a[ n_] := SeriesCoefficient[ (QPochhammer[ -x^3] QPochhammer[ x^12])^3 / (QPochhammer[ -x] QPochhammer[ x^4]), {x, 0, n}];",
				"a[ n_] := If[ n \u003c 0, 0, Times @@ (Which[# == 2, 2 - 2^#2,# == 3, 1, True, (#^(#2 + 1) - 1) / (# - 1)] \u0026 @@@ FactorInteger[3 n + 5]) / 6]; (* _Michael Somos_, Jul 09 2018 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A) * eta(x^6 + A)^9 / (eta(x^2 + A) * eta(x^3 + A))^3, n))};",
				"(PARI) {a(n) = my(A, p, e); if( n\u003c0, 0, n = 3*n + 5; A = factor(n); 1/6 * prod( k=1, matsize(A)[1], [p, e] = A[k, ]; if( p==2, 2 - 2^e, p==3, 0, (p^(e+1) - 1) / (p - 1))))};",
				"(MAGMA) Basis( ModularForms( Gamma0(18), 2), 210) [6]; /* _Michael Somos_, Jul 09 2018 */"
			],
			"xref": [
				"Cf. A033686, A098098."
			],
			"keyword": "sign",
			"offset": "0,3",
			"author": "_Michael Somos_, Nov 22 2013",
			"references": 3,
			"revision": 9,
			"time": "2018-08-08T23:08:07-04:00",
			"created": "2013-11-22T21:41:44-05:00"
		}
	]
}