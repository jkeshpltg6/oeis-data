{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A052409",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 52409,
			"data": "0,1,1,2,1,1,1,3,2,1,1,1,1,1,1,4,1,1,1,1,1,1,1,1,2,1,3,1,1,1,1,5,1,1,1,2,1,1,1,1,1,1,1,1,1,1,1,1,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,6,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,4,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,1,1,1,1,1",
			"name": "a(n) = largest integer power m for which a representation of the form n = k^m exists (for some k).",
			"comment": [
				"Greatest common divisor of all prime-exponents in canonical factorization of n for n\u003e1: a(n)\u003e1 iff n is a perfect power; a(A001597(k))=A025479(k). - _Reinhard Zumkeller_, Oct 13 2002",
				"a(1) set to 0 since there is no largest finite integer power m for which a representation of the form 1 = 1^m exists (infinite largest m). - _Daniel Forgues_, Mar 06 2009",
				"A052410(n)^a(n) = n. - _Reinhard Zumkeller_, Apr 06 2014",
				"Positions of 1's are A007916. Smallest base is given by A052410. - _Gus Wiseman_, Jun 09 2020"
			],
			"link": [
				"Daniel Forgues, \u003ca href=\"/A052409/b052409.txt\"\u003eTable of n, a(n) for n = 1..100000\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/A278028/a278028.txt\"\u003eMaple programs for A007916, A278028, A278029, A052409, A089723, A277564\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Power.html\"\u003ePower\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PerfectPower.html\"\u003ePerfect Power\u003c/a\u003e",
				"\u003ca href=\"/index/Eu#epf\"\u003eIndex entries for sequences computed from exponents in factorization of n\u003c/a\u003e"
			],
			"formula": [
				"a(1) = 0; for n \u003e 1, a(n) = gcd(A067029(n), a(A028234(n))). - _Antti Karttunen_, Aug 07 2017"
			],
			"example": [
				"n = 72 = 2*2*2*3*3: GCD[exponents] = GCD[3,2] = 1. This is the least n for which a(n) \u003c\u003e A051904(n), the minimum of exponents.",
				"For n = 10800 = 2^4 * 3^3 * 5^2, GCD[4,3,2] = 1, thus a(10800) = 1."
			],
			"maple": [
				"# See link.",
				"#",
				"a:= n-\u003e igcd(map(i-\u003e i[2], ifactors(n)[2])[]):",
				"seq(a(n), n=1..120);  # _Alois P. Heinz_, Oct 20 2019"
			],
			"mathematica": [
				"Table[GCD @@ Last /@ FactorInteger[n], {n, 100}] (* _Ray Chandler_, Jan 24 2006 *)"
			],
			"program": [
				"(Haskell)",
				"a052409 1 = 0",
				"a052409 n = foldr1 gcd $ a124010_row n",
				"-- _Reinhard Zumkeller_, May 26 2012",
				"(PARI) a(n)=my(k=ispower(n)); if(k, k, n\u003e1) \\\\ _Charles R Greathouse IV_, Oct 30 2012",
				"(Scheme) (define (A052409 n) (if (= 1 n) 0 (gcd (A067029 n) (A052409 (A028234 n))))) ;; _Antti Karttunen_, Aug 07 2017"
			],
			"xref": [
				"Cf. A052410, A005361, A051903, A072411-A072414, A124010, A075802, A072776, A270492.",
				"Apart from the initial term essentially the same as A253641.",
				"Differs from A051904 for the first time at n=72, where a(72) = 1, while A051904(72) = 2.",
				"Differs from A158378 for the first time at n=10800, where a(10800) = 1, while A158378(10800) = 2.",
				"Cf. A000005, A000961, A001597, A052410, A303386, A327501."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Eric W. Weisstein_",
			"ext": [
				"More terms from _Labos Elemer_, Jun 17 2002"
			],
			"references": 107,
			"revision": 54,
			"time": "2021-11-30T08:38:05-05:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}