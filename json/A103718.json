{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A103718",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 103718,
			"data": "1,2,-1,5,-4,1,17,-17,7,-1,74,-85,45,-11,1,394,-499,310,-100,16,-1,2484,-3388,2359,-910,196,-22,1,18108,-26200,19901,-8729,2282,-350,29,-1,149904,-227708,185408,-89733,26985,-5082,582,-37,1,1389456,-2199276,1896380,-993005,332598,-72723,10320,-915,46,-1",
			"name": "Triangle of coefficients of certain polynomials used with prime numbers as variables in the computation of the array A103728.",
			"comment": [
				"The g.f. for the sequence {b(N,p)}, with b(N,p) the number of cyclically inequivalent two-color, N bead necklaces with p beads of one color and N-p beads of the other color is, for prime numbers p, G(p(n),x):=P(p(n)-1,x)/((1-x)^(p(n)-1)*(1-x^p(n))), with the numerator polynomial P(p(n)-1,x):= sum(r(n,k)*x^k,k=0..p(n)-1) and the row polynomials of this triangle r(n,k):=sum(a(k,m)*p(n)^m,m=0..k). p(n)=A000040(n) (prime numbers).",
				"Row sums (signed) give A000142(k)=k!. Row sums (unsigned) coincide with A007680(k)=(2*k+1)*k!, k\u003e=0.",
				"The (unsigned) column sequences are, for m=0..10: A000774, A081052, A103719-A103727."
			],
			"link": [
				"W. Lang, \u003ca href=\"/A103718/a103718.txt\"\u003eInitial section of triangular array.\u003c/a\u003e"
			],
			"formula": [
				"a(k, m) = ((-1)^m)*(|S1(k+1, m+1)| + |S1(k+1, m+2)|) = ((-1)^m)*(|S1(k+2, m+2)|-k*|S1(k+1, m+2)|), with the (signed) Stirling number triangle S1(n, m) = A048994(n, m), n \u003e= m \u003e= 0.",
				"a(0, 0)=1, a(k, 0) = (k-1)! + k*a(k-1, 0); a(k, m) = -a(k-1, m-1) + k*a(k-1, m), m \u003e 0 and a(k, m)=0 if k \u003c m.",
				"Let B = (n+1)-st row of Stirling cycle numbers (unsigned, A008275); say a,b,c,d,.... Then n-th row of present triangle = ((a+b), (b+c), (c+d), ..., (d)). E.g., 4th row of the Stirling cycle numbers = (6, 11, 6, 1). Then third row of A103718 = ((6+11), (11+6), (6+1), (1)) = (17, 17, 7, 1). - _Gary W. Adamson_, May 07 2006"
			],
			"example": [
				"Triangle begins:",
				"    1;",
				"    2,   -1;",
				"    5,   -4,    1;",
				"   17,  -17,    7,   -1;",
				"   74,  -85,   45,  -11,    1;",
				"  394, -499,  310, -100,   16,   -1;",
				"  ..."
			],
			"mathematica": [
				"a[0, 0] = 1; a[k_, 0] := (k - 1)! + k*a[k - 1, 0]; a[k_, m_]:= If[k\u003cm, 0, -a[k - 1, m - 1] + k a[k - 1, m]]; Flatten[Table[a[k, m], {k, 0, 9}, {m, 0, k}]] (* _Indranil Ghosh_, Mar 11 2017 *)"
			],
			"program": [
				"(PARI) a(k, m) = if(m==0, if(k==0, 1, (k - 1)! + k*a(k - 1, 0)) , if(k\u003cm, 0, -a(k - 1, m - 1) + k*a(k - 1, m)));",
				"{for(k=0, 9, for(m=0, k, print1(a(k, m),\", \");); print(););} \\\\ _Indranil Ghosh_, Mar 11 2017"
			],
			"xref": [
				"Cf. A008275."
			],
			"keyword": "sign,easy,tabl",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Feb 24 2005",
			"ext": [
				"More terms from _Indranil Ghosh_, Mar 11 2017"
			],
			"references": 21,
			"revision": 27,
			"time": "2019-08-28T15:53:09-04:00",
			"created": "2005-04-09T03:00:00-04:00"
		}
	]
}