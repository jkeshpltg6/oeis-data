{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A227587",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 227587,
			"data": "1,4,12,24,44,72,120,192,300,456,680,1008,1464,2104,2976,4176,5804,7992,10920,14800,19944,26688,35504,46944,61752,80828,105288,136536,176288,226728,290448,370720,471468,597600,755032,950976,1194216,1495352,1867344,2325648",
			"name": "Expansion of (phi(-q^3)^2 / (phi(-q) * phi(-q^9)))^2 in powers of q where phi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A227587/b227587.txt\"\u003eTable of n, a(n) for n = 0..2500\u003c/a\u003e",
				"Vaclav Kotesovec, \u003ca href=\"http://arxiv.org/abs/1509.08708\"\u003eA method of finding the asymptotics of q-series based on the convolution of generating functions\u003c/a\u003e, arXiv:1509.08708 [math.CO], Sep 30 2015.",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of 1 + 4 * q * chi(-q^3)^2 / (chi(-q) * chi(-q^9))^3 in powers of q where chi() is a Ramanujan theta function.",
				"Expansion of 1 + 4 * b(q^2) * c(q^6) / (b(q) * c(q^3)) in powers of q where b(), c() are cubic AGM theta functions.",
				"Expansion of (eta(q^2) * eta(q^3)^4 * eta(q^18) / (eta(q) * eta(q^6) * eta(q^9))^2)^2 in powers of q.",
				"Euler transform of period 18 sequence [4, 2, -4, 2, 4, -2, 4, 2, 0, 2, 4, -2, 4, 2, -4, 2, 4, 0, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (18 t)) = g(t) where q = exp(2 Pi i t) and g() is the g.f. for A215412.",
				"a(n) ~ exp(2*Pi*sqrt(2*n)/3) / (2^(3/4) * sqrt(3) * n^(3/4)). - _Vaclav Kotesovec_, Jul 11 2016"
			],
			"example": [
				"G.f. = 1 + 4*q + 12*q^2 + 24*q^3 + 44*q^4 + 72*q^5 + 120*q^6 + 192*q^7 + 300*q^8 + ..."
			],
			"mathematica": [
				"nmax = 50; CoefficientList[Series[Product[((1-x^(2*k)) * (1-x^(3*k))^4 * (1-x^(18*k)) / ((1-x^k) * (1-x^(6*k)) * (1-x^(9*k)))^2)^2, {k, 1, nmax}], {x, 0, nmax}], x] (* _Vaclav Kotesovec_, Jul 11 2016 *)",
				"eta[q_] := q^(1/24)*QPochhammer[q]; CoefficientList[Series[(eta[q^2]* eta[q^3]^4*eta[q^18]/(eta[q]*eta[q^6]*eta[q^9])^2)^2, {q, 0, 50}], q] (* _G. C. Greubel_, Aug 09 2018 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( (eta(x^2 + A) * eta(x^3 + A)^4 * eta(x^18 + A) / (eta(x + A) * eta(x^6 + A) * eta(x^9 + A))^2)^2, n))};"
			],
			"xref": [
				"Cf. A215412."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Michael Somos_, Jul 16 2013",
			"references": 2,
			"revision": 19,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2013-07-17T12:24:10-04:00"
		}
	]
}