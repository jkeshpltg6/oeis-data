{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A275824",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 275824,
			"data": "1,1,2,2,1,2,1,2,1,1,2,2,3,3,2,3,2,3,1,1,2,1,3,2,6,4,3,4,2,3,2,2,3,1,5,4,4,5,4,4,1,3,3,1,3,5,7,6,4,5,3,5,3,3,4,4,5,4,6,7,3,5,4,4,3,2,6,3,6,5,5,7,7,7,3,6,7,5,4,4,9",
			"name": "Number of ways to write n as pen(x) + pen(y) + pen (z) + 2*pen(w), where x,y,z,w are nonnegative integers with x \u003c= y \u003c= z, and pen(k) denotes the pentagonal number k*(3*k-1)/2.",
			"comment": [
				"Conjecture: (i) a(n) \u003e 0 for all n = 0,1,2,...., and a(n) = 1 only for n = 0, 1, 4, 6, 8, 9, 18, 19, 21, 33, 40, 43.",
				"(ii) For every m = 5,6,7,... and each integer d \u003e 1 not divisible by 4, any sufficiently large integer can be written as x + y + z + d*w (or x + y + 2*z + d*w) with x,y,z,w m-gonal numbers.",
				"Part (i) of the conjecture is stronger than the classical result that any natural number is the sum of five pentagonal numbers."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A275824/b275824.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Xiang-Zi Meng and Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1608.02022\"\u003eSums of four polygonal numbers with coefficients\u003c/a\u003e, arXiv:1608.02022 [math.NT], 2016.",
				"Zhi-Wei Sun, \u003ca href=\"http://maths.nju.edu.cn/~zwsun/170o.pdf\"\u003eA result similar to Lagrange's theorem\u003c/a\u003e, J. Number Theory 162(2016), 190-211."
			],
			"example": [
				"a(4) = 1 since 4 = 0*(3*0-1)/2 + 1*(3*1-1)/2 + 1*(3*1-1)/2 + 1*(3*1-1).",
				"a(18) = 1 since 18 = 1*(3*1-1)/2 + 2*(3*2-1)/2 + 3*(3*3-1)/2 + 0*(3*0-1).",
				"a(19) = 1 since 19 = 0*(3*0-1)/2 + 2*(3*2-1)/2 + 3*(3*3-1)/2 + 1*(3*1-1).",
				"a(21) = 1 since 21 = 1*(3*1-1)/2 + 2*(3*2-1)/2 + 2*(3*2-1)/2 + 2*(3*2-1).",
				"a(33) = 1 since 33 = 0*(3*0-1)/2 + 1*(3*1-1)/2 + 4*(3*4-1)/2 + 2*(3*2-1).",
				"a(40) = 1 since 40 = 0*(3*0-1)/2 + 2*(3*2-1)/2 + 5*(3*5-1)/2 + 0*(3*0-1).",
				"a(43) = 1 since n = 1*(3*1-1)/2 + 2*(3*2-1)/2 + 5*(3*5-1)/2 + 1*(3*1-1)."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]]",
				"pQ[n_]:=pQ[n]=SQ[24n+1]\u0026\u0026(n==0||Mod[Sqrt[24n+1]+1,6]==0)",
				"Do[r=0;Do[If[pQ[n-x*(3x-1)-y(3y-1)/2-z(3z-1)/2],r=r+1],{x,0,(Sqrt[12n+1]+1)/6},{y,0,(Sqrt[8(n-x*(3x-1))+1]+1)/6},{z,y,(Sqrt[12(n-x*(3x-1)-y(3y-1)/2)+1]+1)/6}];Print[n,\" \",r];Continue,{n,0,80}]"
			],
			"xref": [
				"Cf. A000326, A256106."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Zhi-Wei Sun_, Aug 10 2016",
			"references": 1,
			"revision": 11,
			"time": "2018-12-06T12:03:50-05:00",
			"created": "2016-08-11T05:27:04-04:00"
		}
	]
}