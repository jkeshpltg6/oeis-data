{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A248416",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 248416,
			"data": "1,4,1,6,4,1,7,7,7,1,10,11,13,13,1,11,13,21,25,25,1,13,16,25,41,49,49,1,16,19,31,49,81,97,97,1,18,21,37,61,97,161,193,193,1,19,25,41,73,121,193,321,385,385,1,21,28,49,81,145,241,385,641,769,769,1,24,31,55,97,161,289,481,769,1281,1537,1537,1",
			"name": "Rectangular array by antidiagonals:  for n \u003e= 0, row n gives the positions in the Thue-Morse sequence A010059 at which the first 2^n terms occur.",
			"comment": [
				"Each row contains contains its successor as a proper subsequence.",
				"Note that this supposes that the Thue-Morse sequence A010059 has offset 1, whereas the true offset is 0. So really the entries should all be reduced by 1. - _N. J. A. Sloane_, Jul 01 2016",
				"Apparently T(n,3) = A0004119(n+1) for n\u003e0. Apparently T(n,4) = A083575(n) for n\u003e0. - _R. J. Mathar_, Nov 06 2018"
			],
			"example": [
				"Northwest corner, n\u003e=0, k\u003e=1:",
				"   1    4    6    7   10   11   13   16   18   19",
				"   1    4    7   11   13   16   19   21   25   28",
				"   1    7   13   21   25   31   37   41   49   55",
				"   1   13   25   41   49   61   73   81   97  109",
				"   1   25   49   81   97  121  145  161  193  217",
				"   1   49   97  161  193  241  289  321  385  433",
				"   1   97  193  321  385  481  577  641  769  865",
				"The Thue-Morse sequence A010059 begins with 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 0, from which we see that the first 4 terms (=1,0,0,1) occur at positions 1, 7, 13, ..., as indicated for row n=2."
			],
			"maple": [
				"A010060 := proc(n)",
				"    local i;",
				"    add(i, i=convert(n, base, 2)) mod 2 ;",
				"end proc:",
				"A010059 := proc(n)",
				"    1-A010060(n) ;",
				"end proc:",
				"A248416Off0 := proc(n,k)",
				"    option remember ;",
				"    local strtN,binpat,src,thue ;",
				"    if k = 1 then",
				"        strtN := 0 ;",
				"    else",
				"        strtN := 1+procname(n,k-1) ;",
				"    end if;",
				"    binpat := [seq(A010059(i),i=0..n-1)] ;",
				"    for src from strtN do",
				"        thue := [seq(A010059(i),i=src..src+nops(binpat)-1)] ;",
				"        if binpat=thue then",
				"            return src ;",
				"        end if;",
				"    end do:",
				"end proc:",
				"A248416 := proc(n,k)",
				"    1+A248416Off0(2^n,k) ;",
				"end proc:",
				"for d from 1 to 11 do",
				"    for k from d to 1 by -1 do",
				"        printf(\"%d,\",A248416(d-k,k)) ;",
				"end do: # _R. J. Mathar_, Nov 06 2018"
			],
			"mathematica": [
				"z = 3000; u = Nest[Flatten[# /. {0 -\u003e {0, 1}, 1 -\u003e {1, 0}}] \u0026, {0}, 20]; Length[u]",
				"t[p_, q_] := t[p, q] = Table[u[[k]], {k, p, q}];",
				"r[n_] := Select[Range[z], t[#, # + 2^(n - 1)] == t[1, 1 + 2^(n - 1)] \u0026]",
				"TableForm[Table[r[n], {n, 0, 10}]]"
			],
			"xref": [
				"Cf. A010059 (Thue-Morse), A026147 (row 0), A091855 (row 1?), A157971 (row 2?),",
				"Column 1 is essentially A004119 (or A181565)."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Oct 06 2014",
			"ext": [
				"Definitions and examples clarified. - _R. J. Mathar_, Nov 06 2018"
			],
			"references": 1,
			"revision": 20,
			"time": "2018-11-06T07:00:21-05:00",
			"created": "2014-10-15T20:54:01-04:00"
		}
	]
}