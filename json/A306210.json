{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A306210",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 306210,
			"data": "1,1,1,1,2,1,1,2,3,1,1,3,4,4,1,1,3,8,7,5,1,1,4,10,17,11,6,1,1,4,16,26,31,16,7,1,1,5,19,47,56,51,22,8,1,1,5,27,65,112,106,78,29,9,1,1,6,31,101,176,232,183,113,37,10,1,1,6,41,131,296,407,435,295,157,46,11,1",
			"name": "T(n,k) = binomial(n + k, n) - binomial(n + floor(k/2), n) + 1, square array read by descending antidiagonals (n \u003e= 0, k \u003e= 0).",
			"comment": [
				"There are at most T(n,k) possible values for the number of knots in an interpolatory cubature formula of degree k for an integral over an n-dimensional region."
			],
			"link": [
				"Ronald Cools, \u003ca href=\"http://nines.cs.kuleuven.be/ecf/\"\u003eEncyclopaedia of Cubature Formulas\u003c/a\u003e",
				"Ronald Cools, \u003ca href=\"https://doi.org/10.1007/978-94-011-2646-5_1\"\u003eA Survey of Methods for Constructing Cubature Formulae\u003c/a\u003e, In: Espelid T.O., Genz A. (eds), Numerical Integration, NATO ASI Series (Series C: Mathematical and Physical Sciences), Vol. 357, 1991, Springer, Dordrecht, pp. 1-24.",
				"T. N. L. Patterson, \u003ca href=\"https://doi.org/10.1007/978-94-009-3889-2_27\"\u003eOn the Construction of a Practical Ermakov-Zolotukhin Multiple Integrator\u003c/a\u003e, In: Keast P., Fairweather G. (eds), Numerical Integration, NATO ASI Series (Series C: Mathematical and Physical Sciences), Vol. 203, 1987, Springer, Dordrecht, pp. 269-290."
			],
			"formula": [
				"T(n,k) = A007318(n+k,n) - A046854(n+k,n) + 1.",
				"G.f.: (1 - x - x^2 + x^3 - 2*y + 2*x*y + y^2 - x*y^2 + x^2*y^2)/((1 - x)*(1 - y)*(1 - x - y)*(1 - x^2 - y))."
			],
			"example": [
				"Square array begins:",
				"  1, 1,  1,   1,   1,    1,    1,    1,     1,  ...",
				"  1, 2,  2,   3,   3,    4,    4,    5,     5,  ...",
				"  1, 3,  4,   8,  10,   16,   19,   27,    31,  ...",
				"  1, 4,  7,  17,  26,   47,   65,  101,   131,  ...",
				"  1, 5, 11,  31,  56,  112,  176,  296,   426,  ...",
				"  1, 6, 16,  51, 106,  232,  407,  737,  1162,  ...",
				"  1, 7, 22,  78, 183,  435,  841, 1633,  2794,  ...",
				"  1, 8, 29, 113, 295,  757, 1597, 3313,  6106,  ...",
				"  1, 9, 37, 157, 451, 1243, 2839, 6271, 12376,  ...",
				"  ...",
				"As triangular array, this begins:",
				"  1;",
				"  1, 1;",
				"  1, 2,  1;",
				"  1, 2,  3,  1;",
				"  1, 3,  4,  4,  1;",
				"  1, 3,  8,  7,  5,  1;",
				"  1, 4, 10, 17, 11,  6,  1;",
				"  1, 4, 16, 26, 31, 16,  7, 1;",
				"  1, 5, 19, 47, 56, 51, 22, 8, 1;",
				"  ..."
			],
			"mathematica": [
				"T[n_, k_] = Binomial[n + k, n] - Binomial[n + Floor[k/2], n] + 1;",
				"Table[T[k, n - k], {k, 0, n}, {n, 0, 20}] // Flatten"
			],
			"program": [
				"(Maxima)",
				"T(n, k) := binomial(n + k, n) - binomial(n + floor(k/2), n) + 1$",
				"create_list(T(k, n - k), n, 0, 20, k, 0, n);"
			],
			"xref": [
				"Cf. A007318, A046854, A322596."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,5",
			"author": "_Franck Maminirina Ramaharo_, Jan 29 2019",
			"references": 0,
			"revision": 7,
			"time": "2019-02-04T11:22:43-05:00",
			"created": "2019-02-04T11:22:43-05:00"
		}
	]
}