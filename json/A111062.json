{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A111062",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 111062,
			"data": "1,1,1,2,2,1,4,6,3,1,10,16,12,4,1,26,50,40,20,5,1,76,156,150,80,30,6,1,232,532,546,350,140,42,7,1,764,1856,2128,1456,700,224,56,8,1,2620,6876,8352,6384,3276,1260,336,72,9,1,9496,26200,34380,27840,15960,6552,2100,480,90,10,1",
			"name": "Triangle T(n, k) = binomial(n, k) * A000085(n-k), 0 \u003c= k \u003c= n.",
			"comment": [
				"Triangle related to A000085.",
				"Riordan array [exp(x(2+x)/2),x]. - _Paul Barry_, Nov 05 2008",
				"Array is exp(S+S^2/2) where S is A132440 the infinitesimal generator for Pascal's triangle. T(n,k) gives the number of ways to choose a subset of {1,2,...,n) of size k and then partitioning the remaining n-k elements into sets each of size 1 or 2. Cf. A122832. - _Peter Bala_, May 14 2012",
				"T(n,k) is equal to the number of R-classes (equivalently, L-classes) in the D-class consisting of all rank k elements of the partial Brauer monoid of degree n. - _James East_, Aug 17 2015"
			],
			"link": [
				"Muniru A Asiru, \u003ca href=\"/A111062/b111062.txt\"\u003eTable of n, a(n) for n = 0..1325\u003c/a\u003e",
				"Igor Dolinka, James East, Athanasios Evangelou, Des FitzGerald, Nicholas Ham, James Hyde, Nicholas Loughlin, \u003ca href=\"http://arxiv.org/abs/1408.2021\"\u003eEnumeration of idempotents in diagram semigroups and algebras\u003c/a\u003e, arXiv:1408.2021 [math.GR], 2014.",
				"Igor Dolinka, James East, Athanasios Evangelou, Des FitzGerald, Nicholas Ham, James Hyde, Nicholas Loughlin, \u003ca href=\"http://dx.doi.org/10.1016/j.jcta.2014.11.008\"\u003eEnumeration of idempotents in diagram semigroups and algebras\u003c/a\u003e, J. Combin. Theory Ser. A 131 (2015), 119-152.",
				"Tom Halverson, Theodore N. Jacobson, \u003ca href=\"https://arxiv.org/abs/1808.08118\"\u003eSet-partition tableaux and representations of diagram algebras\u003c/a\u003e, arXiv:1808.08118 [math.RT], 2018."
			],
			"formula": [
				"Sum_{k\u003e=0} T(m, k)*T(n, k)*k! = T(m+n, 0) = A000085(m+n).",
				"Sum_{k=0..n} T(n, k) = A005425(n).",
				"Apparently satisfies T(n,m) = T(n-1,m-1) + T(n-1,m) + m * T(n-1,m+1). - _Franklin T. Adams-Watters_, Dec 22 2005",
				"T(n,k) = (n!/k!)*Sum_{j=0..n-k} C(j,n-k-j)/(j!*2^(n-k-j)). - _Paul Barry_, Nov 05 2008",
				"G.f.: 1/(1-xy-x-x^2/(1-xy-x-2x^2/(1-xy-x-3x^2/(1-xy-x-4x^2/(1-... (continued fraction). - _Paul Barry_, Apr 23 2009",
				"T(n,k) = C(n,k)*Sum_{j=0..n-k} C(n-k,j)*(n-k-j-1)!! where m!!=0 if m is even. - _James East_, Aug 17 2015",
				"From _Tom Copeland_, Jun 26 2018: (Start)",
				"E.g.f.: exp[t*p.(x)] = exp[t + t^2/2] e^(x*t).",
				"These polynomials (p.(x))^n = p_n(x) are an Appell sequence with the lowering and raising operators L = D and R = x + 1 + D, with D = d/dx, such that L p_n(x) = n * p_(n-1)(x) and R p_n(x) = p_(n+1)(x), so the formalism of A133314 applies here, giving recursion relations.",
				"The transpose of the production matrix gives a matrix representation of the raising operator R.",
				"exp(D + D^2/2) x^n= e^(D^2/2) (1+x)^n = h_n(1+x) = p_n(x) = (a. + x)^n, with (a.)^n = a_n = A000085(n) and h_n(x) the modified Hermite polynomials of A099174.",
				"A159834 with the e.g.f. exp[-(t + t^2/2)] e^(x*t) gives the matrix inverse for this entry with the umbral inverse polynomials q_n(x), an Appell sequence with the raising operator  x - 1 - D, such that umbrally composed q_n(p.(x)) = x^n = p_n(q.(x)). (End)"
			],
			"example": [
				"Rows begin:",
				"     1;",
				"     1,    1;",
				"     2,    2,    1;",
				"     4,    6,    3,    1;",
				"    10,   16,   12,    4,    1;",
				"    26,   50,   40,   20,    5,    1;",
				"    76,  156,  150,   80,   30,    6,   1;",
				"   232,  532,  546,  350,  140,   42,   7,  1;",
				"   764, 1856, 2128, 1456,  700,  224,  56,  8, 1;",
				"  2620, 6876, 8352, 6384, 3276, 1260, 336, 72, 9, 1;",
				"From _Paul Barry_, Apr 23 2009: (Start)",
				"Production matrix is:",
				"  1, 1,",
				"  1, 1, 1,",
				"  0, 2, 1, 1,",
				"  0, 0, 3, 1, 1,",
				"  0, 0, 0, 4, 1, 1,",
				"  0, 0, 0, 0, 5, 1, 1,",
				"  0, 0, 0, 0, 0, 6, 1, 1,",
				"  0, 0, 0, 0, 0, 0, 7, 1, 1,",
				"  0, 0, 0, 0, 0, 0, 0, 8, 1, 1 (End)",
				"From _Peter Bala_, Feb 12 2017: (Start)",
				"The infinitesimal generator has integer entries and begins",
				"  0",
				"  1  0",
				"  1  2  0",
				"  0  3  3  0",
				"  0  0  6  4  0",
				"  0  0  0 10  5  0",
				"  0  0  0  0 15  6  0",
				"  ...",
				"and is the generalized exponential Riordan array [x + x^2/2!,x].(End)"
			],
			"mathematica": [
				"a[n_] := Sum[(2 k - 1)!! Binomial[n, 2 k], {k, 0, n/2}]; Table[Binomial[n, k] a[n - k], {n, 0, 10}, {k, 0, n}] // Flatten (* _Michael De Vlieger_, Aug 20 2015, after _Michael Somos_ at A000085 *)"
			],
			"program": [
				"(Sage)",
				"def A111062_triangle(dim):",
				"    M = matrix(ZZ,dim,dim)",
				"    for n in (0..dim-1): M[n,n] = 1",
				"    for n in (1..dim-1):",
				"        for k in (0..n-1):",
				"            M[n,k] = M[n-1,k-1]+M[n-1,k]+(k+1)*M[n-1,k+1]",
				"    return M",
				"A111062_triangle(9) # _Peter Luschny_, Sep 19 2012",
				"(GAP) Flat(List([0..10],n-\u003eList([0..n],k-\u003e(Factorial(n)/Factorial(k))*Sum([0..n-k],j-\u003eBinomial(j,n-k-j)/(Factorial(j)*2^(n-k-j)))))); # _Muniru A Asiru_, Jun 29 2018"
			],
			"xref": [
				"Cf. A000085, A005425 (row sums), A007318, A013989, A122832, A132440.",
				"Cf. A099174, A133314, A159834 (inverse matrix)."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,4",
			"author": "_Philippe Deléham_, Oct 07 2005",
			"ext": [
				"Corrected by _Franklin T. Adams-Watters_, Dec 22 2005",
				"10th row added by _James East_, Aug 17 2015"
			],
			"references": 4,
			"revision": 58,
			"time": "2020-03-17T13:49:51-04:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}