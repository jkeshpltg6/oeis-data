{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A243763",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 243763,
			"data": "1,6,16,32,60,92,128,192,253,316,432,512,604,792,896,1024,1272,1410,1584,1920,2104,2236,2688,2944,3101,3732,3904,4096,4884,5080,5376,6144,6424,6776,7776,8096,8188,9492,9856,10112,11664,11704,11952,13824,14100,14360",
			"name": "Expansion of q * phi(q)^3 * psi(q^2)^4 in powers of q where phi(), psi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"Vaclav Kotesovec, \u003ca href=\"/A243763/b243763.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of eta(q^2)^11 * eta(q^4)^2 / eta(q)^6 in powers of q.",
				"Euler transform of period 4 sequence [ 6, -5, 6, -7, ...]."
			],
			"example": [
				"G.f. = q + 6*q^2 + 16*q^3 + 32*q^4 + 60*q^5 + 92*q^6 + 128*q^7 + 192*q^8 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 3, 0, q]^3 EllipticTheta[ 2, 0, q]^4 / 16, {q, 0, n}];",
				"a[ n_] := SeriesCoefficient[ q QPochhammer[ q^2]^11 QPochhammer[ q^4]^2 / QPochhammer[ q]^6, {q, 0, n}];",
				"nmax = 50; CoefficientList[Series[Product[(1-x^k)^7 * (1+x^(2*k))^2 * (1+x^k)^13, {k, 1, nmax}], {x, 0, nmax}], x] (* _Vaclav Kotesovec_, Jul 11 2016 *)"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c1, 0, n--; A = x * O(x^n); polcoeff( eta(x^2 + A)^11 * eta(x^4 + A)^2 / eta(x + A)^6, n))};",
				"(MAGMA) Basis( ModularForms( Gamma0(4), 7/2), 50) [2] ;"
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Michael Somos_, Jun 10 2014",
			"references": 1,
			"revision": 9,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2014-06-10T02:34:46-04:00"
		}
	]
}