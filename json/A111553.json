{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A111553",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 111553,
			"data": "1,1,1,6,2,1,46,10,3,1,416,72,16,4,1,4256,632,116,24,5,1,48096,6352,1016,184,34,6,1,591536,70912,10176,1664,282,46,7,1,7840576,864192,113216,17024,2696,416,60,8,1,111226816,11371072,1375456,192384,28792,4256,592,76,9,1",
			"name": "Triangular matrix T, read by rows, that satisfies: SHIFT_LEFT(column 0 of T^p) = p*(column p+4 of T), or [T^p](m,0) = p*T(p+m,p+4) for all m\u003e=1 and p\u003e=-4.",
			"comment": [
				"Column 0 equals A111531 (related to log of factorial series). Column 4 (A111557) equals SHIFT_LEFT(column 0 of log(T)), where the matrix logarithm, log(T), equals the integer matrix A111560."
			],
			"link": [
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/1804.06801\"\u003eA note on number triangles that are almost their own production matrix\u003c/a\u003e, arXiv:1804.06801 [math.CO], 2018."
			],
			"formula": [
				"T(n, k) = k*T(n, k+1) + Sum_{j=0..n-k-1} T(j+3, 3)*T(n, j+k+1) for n\u003ek\u003e0, with T(n, n) = 1, T(n+1, n) = n+1, T(n+4, 3) = 4*T(n+1, 0), T(n+5, 5) = T(n+1, 0), for n\u003e=0."
			],
			"example": [
				"SHIFT_LEFT(column 0 of T^-4) = -4*(column 0 of T);",
				"SHIFT_LEFT(column 0 of T^-3) = -3*(column 1 of T);",
				"SHIFT_LEFT(column 0 of T^-2) = -2*(column 2 of T);",
				"SHIFT_LEFT(column 0 of T^-1) = -1*(column 3 of T);",
				"SHIFT_LEFT(column 0 of log(T)) = column 4 of T;",
				"SHIFT_LEFT(column 0 of T^1) = 1*(column 5 of T);",
				"where SHIFT_LEFT of column sequence shifts 1 place left.",
				"Triangle T begins:",
				"1;",
				"1,1;",
				"6,2,1;",
				"46,10,3,1;",
				"416,72,16,4,1;",
				"4256,632,116,24,5,1;",
				"48096,6352,1016,184,34,6,1;",
				"591536,70912,10176,1664,282,46,7,1;",
				"7840576,864192,113216,17024,2696,416,60,8,1; ...",
				"After initial term, column 3 is 4 times column 0.",
				"Matrix inverse T^-1 = A111559 starts:",
				"1;",
				"-1,1;",
				"-4,-2,1;",
				"-24,-4,-3,1;",
				"-184,-24,-4,-4,1;",
				"-1664,-184,-24,-4,-5,1;",
				"-17024,-1664,-184,-24,-4,-6,1; ...",
				"where columns are all equal after initial terms;",
				"compare columns of T^-1 to column 3 of T.",
				"Matrix logarithm log(T) = A111560 is:",
				"0;",
				"1,0;",
				"5,2,0;",
				"34,7,3,0;",
				"282,44,10,4,0;",
				"2696,354,60,14,5,0;",
				"28792,3328,470,84,19,6,0; ...",
				"compare column 0 of log(T) to column 4 of T."
			],
			"mathematica": [
				"T[n_, k_] := T[n, k] = If[n\u003ck || k\u003c0, 0, If[n == k, 1, If[n == k + 1, n, k T[n, k + 1] + Sum[T[j + 3, 3] T[n, j + k + 1], {j, 0, n - k - 1}]]]];",
				"Table[T[n, k], {n, 0, 9}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Aug 09 2018, from PARI *)"
			],
			"program": [
				"(PARI) T(n,k)=if(n\u003ck || k\u003c0,0,if(n==k,1,if(n==k+1,n, k*T(n,k+1)+sum(j=0,n-k-1,T(j+3,3)*T(n,j+k+1)))))"
			],
			"xref": [
				"Cf. A111531 (column 0), A111554 (column 1), A111555 (column 2), A111556 (column 3), A111557 (column 4), A111558 (row sums), A111559 (matrix inverse), A111560 (matrix log); related tables: A111528, A104980, A111536, A111544."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Paul D. Hanna_, Aug 07 2005",
			"references": 11,
			"revision": 15,
			"time": "2018-08-09T04:49:31-04:00",
			"created": "2005-09-21T03:00:00-04:00"
		}
	]
}