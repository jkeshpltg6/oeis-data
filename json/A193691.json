{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A193691",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 193691,
			"data": "1,1,1,2,1,2,2,4,5,1,2,2,4,5,2,4,4,8,10,5,10,13,14,1,2,2,4,5,2,4,4,8,10,5,10,13,14,2,4,4,8,10,4,8,8,16,20,10,20,26,28,5,10,10,20,25,13,26,34,37,14,28,37,41,42,1,2,2,4,5,2,4,4,8,10,5,10,13,14,2,4,4,8,10,4",
			"name": "Triangle T(n,k), n\u003e=0, 1\u003c=k\u003c=C(n), read by rows: T(n,k) = number of elements \u003c= k-th path in the poset of Dyck paths of semilength n ordered by inclusion.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A193691/b193691.txt\"\u003eRows n = 0..9, flattened\u003c/a\u003e"
			],
			"example": [
				"Dyck paths of semilength n=3 listed in lexicographic order:",
				".                                               /\\",
				".                  /\\      /\\        /\\/\\      /  \\",
				".     /\\/\\/\\    /\\/  \\    /  \\/\\    /    \\    /    \\",
				".     101010    101100    110010    110100    111000",
				". k = (1)       (2)       (3)       (4)       (5)",
				".",
				"We have (1) \u003c= (1); (1),(2) \u003c= (2); (1),(3) \u003c= (3); (1),(2),(3),(4) \u003c= (4); and (1),(2),(3),(4),(5) \u003c= (5), thus row 3 = [1, 2, 2, 4, 5].",
				"Triangle begins:",
				"  1;",
				"  1;",
				"  1, 2;",
				"  1, 2, 2, 4, 5;",
				"  1, 2, 2, 4, 5, 2, 4, 4, 8, 10, 5, 10, 13, 14;",
				"  1, 2, 2, 4, 5, 2, 4, 4, 8, 10, 5, 10, 13, 14, 2, 4, 4, 8, ..."
			],
			"maple": [
				"d:= proc(n, l) local m; m:= nops(l);",
				"      `if`(n=m, [l], [seq(d(n, [l[], j])[],",
				"                     j=`if`(m=0, 1, max(m+1, l[-1]))..n)])",
				"    end:",
				"le:= proc(x, y) local i;",
				"       for i to nops(x) do if x[i]\u003ey[i] then return false fi od; true",
				"     end:",
				"T:= proc(n) option remember; local l;",
				"      l:= d(n, []);",
				"      seq(add(`if`(le(l[i], l[j]), 1, 0), i=1..j), j=1..nops(l))",
				"    end:",
				"seq(T(n), n=0..6);"
			],
			"mathematica": [
				"d[n_, l_] := d[n, l] = Module[{m}, m = Length[l]; If[n == m, {l}, Flatten[#, 1]\u0026 @ Table[d[n, Append[l, j]], {j, If[m == 0, 1, Max[m+1, Last[l]]], n}]]]; le[x_, y_] := Module[{i}, For[i = 1, i \u003c= Length[x], i++, If[x[[i]] \u003e y[[i]], Return[False]]]; True]; T[n_] := T[n] = Module[{l}, l = d[n, {}]; Table[Sum[If[le[l[[i]], l[[j]]], 1, 0], {i, 1, j}], {j, 1, Length[l]}]]; Table[T[n], {n, 0, 6}] // Flatten (* _Jean-François Alcover_, Feb 01 2017, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Row sums give A005700.",
				"Lengths and last elements of rows give A000108.",
				"Cf. A193692, A193693, A193694."
			],
			"keyword": "nonn,look,tabf",
			"offset": "0,4",
			"author": "_Alois P. Heinz_, Aug 02 2011",
			"references": 5,
			"revision": 23,
			"time": "2019-05-15T16:22:22-04:00",
			"created": "2011-08-03T03:56:33-04:00"
		}
	]
}