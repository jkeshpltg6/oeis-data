{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A261321",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 261321,
			"data": "1,4,4,-4,-12,-8,12,32,20,-28,-72,-48,60,152,96,-120,-300,-184,228,560,344,-416,-1008,-608,732,1756,1048,-1252,-2976,-1768,2088,4928,2900,-3408,-7992,-4672,5460,12728,7408,-8600,-19944,-11544,13344,30800,17744,-20424",
			"name": "Expansion of (phi(q) / phi(q^3))^2 in powers of q where phi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"The generating function is associated with a modular equation of degree 3 and is the multiplier denoted by \"m\". - _Michael Somos_, Nov 01 2017"
			],
			"reference": [
				"B. C. Berndt, Ramanujan's Notebooks Part III, Springer-Verlag, see p. 230 Entry 5(iii), g.f. denoted by multiplier m."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A261321/b261321.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of eta(q^2)^10 * eta(q^3)^4 * eta(q^12)^4 / (eta(q)^4 * eta(q^4)^4 * eta(q^6)^10) in powers of q.",
				"G.f.: (Sum_{k in Z} x^k^2) / (Sum_{k in Z} x^(3*k^2))^2.",
				"a(n) = -(1)^n * A217771(n). a(n) = 4 * A187153(n) = 4 * A213265(n) unless n=0.",
				"a(2*n) = 4 * A123633(n) = 4 * A128636(n) unless n=0. a(3*n) = -4 * A228447(n) unless n=0.",
				"Convolution inverse is A261320. Convolution square of A139137."
			],
			"example": [
				"G.f. = 1 + 4*x + 4*x^2 - 4*x^3 - 12*x^4 - 8*x^5 + 12*x^6 + 32*x^7 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (EllipticTheta[ 3, 0, q] / EllipticTheta[ 3, 0, q^3])^2, {q, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^10 * eta(x^3 + A)^4 * eta(x^12 + A)^4 / (eta(x + A)^4 * eta(x^4 + A)^4 * eta(x^6 + A)^10), n))};"
			],
			"xref": [
				"Cf. A123633, A128636, A139137, A187153, A213265, A217771, A228447, A261320."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Aug 14 2015",
			"references": 1,
			"revision": 13,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2015-08-14T19:10:26-04:00"
		}
	]
}