{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A291228",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 291228,
			"data": "2,6,18,56,170,522,1594,4880,14922,45654,139642,427176,1306690,3997146,12227058,37402144,114411538,349980390,1070575586,3274847512,10017625050,30643508586,93737246762,286738430256,877121205338,2683078129590,8207426973258",
			"name": "p-INVERT of (0,1,0,1,0,1,...), where p(S) = 1 - 2 S - 2 S^2.",
			"comment": [
				"Suppose s = (c(0), c(1), c(2),...) is a sequence and p(S) is a polynomial. Let S(x) = c(0)*x + c(1)*x^2 + c(2)*x^3 + ... and T(x) = (-p(0) + 1/p(S(x)))/x. The p-INVERT of s is the sequence t(s) of coefficients in the Maclaurin series for T(x).  Taking p(S) = 1 - S gives the \"INVERT\" transform of s, so that p-INVERT is a generalization of the \"INVERT\" transform (e.g., A033453).",
				"See A291219 for a guide to related sequences."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A291228/b291228.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2, 4, -2, -1)"
			],
			"formula": [
				"G.f.: -((2 (-1 - x + x^2))/(1 - 2 x - 4 x^2 + 2 x^3 + x^4)).",
				"a(n) = 2*a(n-1) + 4*a(n-2) - 2*a(n-3) - a(n-4) for n \u003e= 5.",
				"a(n) = 2*A291257(n) for n \u003e= 0."
			],
			"mathematica": [
				"z = 60; s = x/(1 - x^2); p = 1 - 2 s - 2 s^2;",
				"Drop[CoefficientList[Series[s, {x, 0, z}], x], 1]  (* A000035 *)",
				"u = Drop[CoefficientList[Series[1/p, {x, 0, z}], x], 1]  (* A291228 *)",
				"u/2         (* A291257 *)",
				"LinearRecurrence[{2,4,-2,-1},{2,6,18,56},30] (* _Harvey P. Dale_, Aug 08 2019 *)"
			],
			"xref": [
				"Cf. A000035, A291219, A291257."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_Clark Kimberling_, Aug 25 2017",
			"references": 3,
			"revision": 6,
			"time": "2019-08-08T10:19:00-04:00",
			"created": "2017-08-25T23:39:50-04:00"
		}
	]
}