{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A137822",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 137822,
			"data": "1,3,2,7,2,3,1,21,2,3,1,8,1,3,2,61,2,3,1,8,1,3,2,21,1,3,2,7,2,3,1,183,2,3,1,8,1,3,2,21,1,3,2,7,2,3,1,62,1,3,2,7,2,3,1,21,2,3,1,8,1,3,2,547,2,3,1,8,1,3,2,21,1,3,2,7,2,3,1,62,1,3,2,7,2,3,1,21,2,3,1,8,1,3,2,183,1,3,2",
			"name": "First differences of A137821 (numbers such that sum( Catalan(k), k=1..2n) = 0 (mod 3)).",
			"comment": [
				"For the initial term, we use A137821(0)=0 (cf. formula).",
				"Sequence A122983 lists record values of this one, which occur at index 2^j (cf. formula). The fact that these values roughly grow by a factor 3 is explained by the fact that these values are given as the sum of all preceding terms (up to +1 or +2 according to the parity of j, cf. formula).",
				"The only values occurring in this sequence are { 1, 2, 3, 7, 8, 21, 61, 62, 183, 547, 548, 1641,... } = A137823, consisting of the record values a(2^j) and, for every other one of these (i.e. for even j), its successor a(2^j)+1, occurring first as a(3*2^j).",
				"The remarkably simple sequence A137824 (= 1,3,2, 4,12,8,...: pattern 1,3,2 multiplied by powers of 4) gives the index at which the value A137823(m) first occurs. - M. F. Hasler, Mar 15 2008",
				"The PARI code given here (function A137822(n)) allows one to calculate hundreds of terms of A107755 in a few microseconds. - _M. F. Hasler_, Mar 15 2008"
			],
			"link": [
				"M. F. Hasler, \u003ca href=\"/A137822/b137822.txt\"\u003eTable of n, a(n) for n = 1..499\u003c/a\u003e."
			],
			"formula": [
				"a(m) = A137821(m)-A137821(m-1), A137821(m)=sum( a(j), j=1..m).",
				"a(2^j) = A122983(j-1) = A137821(2^j-1) + 1 (resp. +2) for j even (resp. odd).",
				"a(3*2^j) = a(2^j) (resp. = a(2^j)+1) for j odd (resp. j even)."
			],
			"example": [
				"Record values are a(1)=1, a(2)=3, a(4)=7, a(8)=21, a(16)=61, ...",
				"Apart from these values, the only other values occurring in the sequence are:",
				"2=a(1)+1=a(3*1), 8=a(4)+1=a(3*4), 62=a(16)+1=a(3*16), ..."
			],
			"program": [
				"(PARI) A137822 = D( A137821 ) /* where D(v)=vector(#v-1,i,v[i+1]-v[i]) or D(v)=vecextract(v, \"^1\")-vecextract(v,\"^-1\") */",
				"(PARI) n=0; A137822=vector(499,i,{ o=n; if( bitand(i,i-1), while(n++ \u0026\u0026 s+=binomial(4*n-2, 2*n-1)/(2*n)*(10*n-1)/(2*n+1),),s=Mod(0,3); n=2*n+1+log(i+.5)\\log(2)%2 ); n-o})",
				"(PARI) A137822(n)= local( L=log(n+.5)\\log(2) ); while( n\u003e0 || ((n+=2^L) \u0026\u0026 L=log(n+.5)\\log(2)), (n-=2^L) || return( 3^(L+1)\\4+1 ); (n-=2^(L-1)) || return( 3^L\\4+1+L%2 );n\u003c0 \u0026\u0026 n+=2\u003c\u003cL--);1 \\\\ _M. F. Hasler_, Mar 15 2008"
			],
			"xref": [
				"Cf. A000108, A107755, A137821-A137824.",
				"Cf. A122983 (record values of this)."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_M. F. Hasler_, Feb 25 2008, revised Mar 15 2008",
			"references": 8,
			"revision": 11,
			"time": "2021-01-26T10:48:05-05:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}