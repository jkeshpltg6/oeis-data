{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A067743",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 67743,
			"data": "0,1,2,2,2,2,2,3,2,4,2,4,2,4,2,4,2,5,2,4,4,4,2,6,2,4,4,4,2,6,2,5,4,4,2,8,2,4,4,6,2,6,2,6,4,4,2,8,2,5,4,6,2,6,4,6,4,4,2,10,2,4,4,6,4,6,2,6,4,6,2,9,2,4,6,6,2,8,2,8,4,4,2,10,4,4,4,6,2,10,2,6,4,4,4,10,2,5,4,8,2,8",
			"name": "Number of divisors of n not in the half-open interval [sqrt(n/2), sqrt(n*2)).",
			"comment": [
				"From _Max Alekseyev_, May 13 2008: (Start)",
				"Direct proof of _Joerg Arndt_'s g.f. (see formula section).",
				"We need to count divisors d|n such that d^2\u003c=n/2 or d^2\u003e2n. In the latter case, let's switch to co-divisor, replacing d with n/d.",
				"Then we need to find the total count of: 1) divisors d|n such that 2d^2\u003c=n; 2) divisors d|n such that 2d^2\u003cn.",
				"Let d|n and 2d^2\u003c=n. Then n-2d^2 must be a multiple of d, i.e., n-2d^2=td for some integer t\u003e=0.",
				"Moreover it is easy to see that 1) is equivalent to n = 2d^2 + td for some integer t\u003e=0. Therefore the answer for 1) is the coefficient of z^n in Sum_{d\u003e=1} Sum_{t\u003e=0} x^(2d^2 + td) = Sum_{d\u003e=1} x^(2d^2)/(1 - x^d).",
				"Similarly, the answer for 2) is Sum_{d\u003e=1} x^(2d^2)/(1 - x^d) * x^d.",
				"Therefore the g.f. for A067743 is Sum_{d\u003e=1} x^(2d^2)/(1 - x^d) + Sum_{d\u003e=1} x^(2d^2)/(1 - x^d) * x^d = Sum_{d\u003e=1} x^(2d^2)/(1 - x^d) * (1 + x^d), as proposed. (End)",
				"a(n) is odd if and only if n is in A001105. - _Robert Israel_, Oct 05 2020"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A067743/b067743.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Robin Chapman, Kimmo Ericksson, Richard P. Stanley and Reiner Martin, \u003ca href=\"http://www.jstor.org/stable/2695782\"\u003eOn the Number of Divisors of n in a Special Interval: Problem 10847\u003c/a\u003e, The American Mathematical Monthly, Vol. 109, No. 1 (Jan., 2002), p. 80."
			],
			"formula": [
				"a(n) = A000005(n) - A067742(n).",
				"G.f.: Sum_{k\u003e=1} z^(2*k^2)*(1+z^k)/(1-z^k). - _Joerg Arndt_, May 12 2008"
			],
			"example": [
				"a(6)=2 because 2 divisors of 6 (i.e., 1 and 6) fall outside sqrt(3) to sqrt(12)."
			],
			"maple": [
				"f:=proc(n) nops(select(t -\u003e t^2 \u003c n/2 or t^2 \u003e= 2*n, numtheory:-divisors(n))) end proc:",
				"map(f, [$1..200]); # _Robert Israel_, Oct 05 2020"
			],
			"mathematica": [
				"hoi[n_]:=Length[DeleteCases[Divisors[n],_?(Sqrt[n/2]\u003c=#\u003cSqrt[2*n]\u0026)]]; Array[ hoi,110] (* _Harvey P. Dale_, Aug 22 2020 *)"
			],
			"program": [
				"(PARI) A067743(n)=sumdiv( n,d, d*d\u003cn/2 || d*d \u003e= 2*n ) \\\\ _M. F. Hasler_, May 12 2008"
			],
			"xref": [
				"Cf. A067742, A000005, A001105."
			],
			"keyword": "easy,nonn",
			"offset": "1,3",
			"author": "_Marc LeBrun_, Jan 29 2002",
			"references": 3,
			"revision": 25,
			"time": "2020-10-06T02:24:42-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}