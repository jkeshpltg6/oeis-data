{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A327787",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 327787,
			"data": "1729,252601,1152271,1615681,4335241,172947529,214852609,79624621,178837201,775368901,686059921,985052881,5781222721,10277275681,84350561,5255104513,492559141,74340674101,9293756581,1200778753,129971289169,2230305949,851703301,8714965001,6693621481",
			"name": "a(n) is the smallest Carmichael number k such that gpf(p-1) = prime(n) for all prime factors p of k.",
			"comment": [
				"The first term is the Hardy-Ramanujan number. - _Omar E. Pol_, Nov 25 2019"
			],
			"link": [
				"Daniel Suteu, \u003ca href=\"/A327787/b327787.txt\"\u003eTable of n, a(n) for n = 2..831\u003c/a\u003e",
				"Daniel Suteu, \u003ca href=\"/A327787/a327787.txt\"\u003eTerms and upper bounds for n = 2..10000\u003c/a\u003e (values greater than 2^64 are upper bounds)",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CarmichaelNumber.html\"\u003eCarmichael Number\u003c/a\u003e"
			],
			"example": [
				"a(2) = 1729 = (2*3 + 1)(2*2*3 + 1)(2*3*3 + 1).",
				"a(3) = 252601 = (2*2*2*5 + 1)(2*2*3*5 + 1)(2*2*5*5 + 1).",
				"a(4) = 1152271 = (2*3*7 + 1)(2*3*3*7 + 1)(2*3*5*7 + 1).",
				"a(5) = 1615681 = (2*11 + 1)(2*3*3*11 + 1)(2*2*2*2*2*11 + 1)."
			],
			"mathematica": [
				"carmQ[n_] := CompositeQ[n] \u0026\u0026 Divisible[n - 1, CarmichaelLambda[n]]; gpf[n_] := FactorInteger[n][[-1, 1]]; g[n_] := If[Length[(u = Union[gpf /@ (FactorInteger[n][[;; , 1]] - 1)])] == 1, u[[1]], 1]; m = 5; c = 0; k = 0; v = Table[0, {m}]; While[c \u003c m, k++ If[! carmQ[k], Continue[]]; If[(p = g[k]) \u003e 1, i = PrimePi[p] - 1; If[i \u003c= m \u0026\u0026 v[[i]] == 0, c++; v[[i]] = k]]]; v (* _Amiram Eldar_, Oct 08 2019 *)"
			],
			"program": [
				"(Perl) use ntheory \":all\"; sub a { my $p = nth_prime(shift); for(my $k = 1; ; ++$k) { return $k if (is_carmichael($k) and vecall { (factor($_-1))[-1] == $p } factor($k)) } }",
				"for my $n (2..10) { print \"a($n) = \", a($n), \"\\n\" }"
			],
			"xref": [
				"Cf. A002997 (Carmichael numbers), A006530 (gpf), A001235."
			],
			"keyword": "nonn",
			"offset": "2,1",
			"author": "_Daniel Suteu_, Sep 25 2019",
			"references": 1,
			"revision": 18,
			"time": "2019-12-03T07:16:48-05:00",
			"created": "2019-10-09T03:24:55-04:00"
		}
	]
}