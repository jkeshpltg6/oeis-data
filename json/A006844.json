{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006844",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6844,
			"id": "M3245",
			"data": "4,5,9,13,14,17,19,21,24,25,27,35,37,43,45,47,57,67,69,73,77,83,93,101,105,109,113,115,123,125,133,149,153,163,173,197,201,205,209,211,213,217,219,227,229,235,237,239",
			"name": "a(1)=4, a(2)=5; thereafter a(n) is smallest number that is greater than a(n-1) and having a unique representation as a(j) + a(k) for j\u003ck.",
			"comment": [
				"This is the 1-additive sequence with base {4,5}. Apart from three extra terms (4, 14, 24) in the initial segment, this breaks up naturally into segments of 32 terms each. [Finch, 1992]. - _N. J. A. Sloane_, Aug 12 2015",
				"An Ulam-type sequence - see A002858 for many further references, comments, etc. - _T. D. Noe_, Jan 21 2008"
			],
			"reference": [
				"Steven R. Finch, Mathematical Constants, Cambridge, 2003, pp. 145-151.",
				"R. K. Guy, \"s-Additive sequences,\" preprint, 1994.",
				"R. K. Guy, Unsolved Problems in Number Theory, Section C4.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A006844/b006844.txt\"\u003eTable of n, a(n) for n=1..1000\u003c/a\u003e",
				"Experimental Mathematics, \u003ca href=\"http://www.emis.de/journals/EM/\"\u003eHome Page\u003c/a\u003e",
				"Steven R. Finch, \u003ca href=\"http://www.people.fas.harvard.edu/~sfinch/constant/stlrsky/stlrsky.html\"\u003eStolarsky-Harborth Constant\u003c/a\u003e [Broken link]",
				"Steven R. Finch, \u003ca href=\"http://web.archive.org/web/20010207195349/http://www.mathsoft.com/asolve/constant/stlrsky/stlrsky.html\"\u003eStolarsky-Harborth Constant\u003c/a\u003e [From the Wayback machine]",
				"Steven R. Finch, \u003ca href=\"http://www.emis.de/journals/EM/expmath/volumes/1/1.html\"\u003ePatterns in 1-additive sequences\u003c/a\u003e, Experimental Mathematics 1 (1992), 57-63.",
				"R. K. Guy, \u003ca href=\"/A007300/a007300.pdf\"\u003es-Additive sequences\u003c/a\u003e, Preprint, 1994. (Annotated scanned copy)",
				"R. Queneau, \u003ca href=\"http://dx.doi.org/10.1016/0097-3165(72)90083-0\"\u003eSur les suites s-additives\u003c/a\u003e, J. Combin. Theory, A12 (1972), 31-71.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/UlamSequence.html\"\u003eUlam Sequence\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Ulam_number\"\u003eUlam number\u003c/a\u003e",
				"\u003ca href=\"/index/U#Ulam_num\"\u003eIndex entries for Ulam numbers\u003c/a\u003e"
			],
			"formula": [
				"For n\u003e9, a(n+32) = a(n) + 192. - _T. D. Noe_, Jan 21 2008"
			],
			"mathematica": [
				"s = {4, 5}; n0 = 9; dn = 32; m = 192; Do[ AppendTo[s, n = Last[s]; While[n++; Length[ DeleteCases[ Intersection[s, n - s], n/2, 1, 1]] != 2]; n], {n0 + dn}]; Clear[a]; a[n_] := a[n] = If[n \u003c= n0 + dn, s[[n]], a[n - dn] + m]; Table[a[n], {n, 1, 200}] (* _Jean-François Alcover_, Apr 03 2013 *)"
			],
			"program": [
				"(Haskell)",
				"a006844 n = a006844_list !! (n-1)",
				"a006844_list = 4 : 5 : ulam 2 5 a006844_list",
				"-- Function ulam as defined in A002858.",
				"-- _Reinhard Zumkeller_, Nov 03 2011"
			],
			"keyword": "easy,nonn,nice",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_",
			"references": 3,
			"revision": 38,
			"time": "2019-04-19T10:16:23-04:00",
			"created": "1991-07-25T03:00:00-04:00"
		}
	]
}