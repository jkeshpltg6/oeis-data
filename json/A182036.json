{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A182036",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 182036,
			"data": "1,1,0,1,0,0,-1,-2,0,1,4,0,0,-1,0,-3,-8,0,4,14,0,1,-4,0,-6,-23,0,5,40,0,1,-10,0,-10,-60,0,11,98,0,4,-24,0,-19,-140,0,17,224,0,4,-54,0,-31,-304,0,31,478,0,9,-112,0,-50,-627,0,46,968,0,11,-224",
			"name": "Expansion of q^(1/2) * eta(q^2)^2 * eta(q^6)^2 / (eta(q) * eta(q^9)^3) in powers of q.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A182036/b182036.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Euler transform of period 18 sequence [1, -1, 1, -1, 1, -3, 1, -1, 4, -1, 1, -3, 1, -1, 1, -1, 1, 0, ...].",
				"a(3*n) = A132179(n). a(3*n + 2) = 0.",
				"Expansion of psi(x) * f(-x^6)^2 / f(-x^9)^3 in powers of x where psi(), f() are Ramanujan theta functions. - _Michael Somos_, Aug 10 2017"
			],
			"example": [
				"G.f. = 1 + x + x^3 - x^6 - 2*x^7 + x^9 + 4*x^10 - x^13 - 3*x^15 - 8*x^16 + ...",
				"G.f. = 1/q + q + q^5 - q^11 - 2*q^13 + q^17 + 4*q^19 - q^25 - 3*q^29 - 8*q^31 + ..."
			],
			"mathematica": [
				"eta[x_] := x^(1/24)*QPochhammer[x]; A182036[n_] := SeriesCoefficient[q^(1/2)*(eta[q^2]* eta[q^6])^2/(eta[q]*eta[q^9]^3), {q,0,n}]; Table[A182036[n], {n,0,50}] (* _G. C. Greubel_, Aug 09 2017 *)",
				"a[ n_] := SeriesCoefficient[ 1/2 x^(-1/8) EllipticTheta[ 2, 0, x^(1/2)] QPochhammer[ x^6]^2 / QPochhammer[ x^9]^3, {x, 0, n}]; (* _Michael Somos_, Aug 10 2017 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^2 * eta(x^6 + A)^2 / (eta(x + A) * eta(x^9 + A)^3), n))};"
			],
			"xref": [
				"Cf. A132179."
			],
			"keyword": "sign",
			"offset": "0,8",
			"author": "_Michael Somos_, Apr 07 2012",
			"references": 4,
			"revision": 16,
			"time": "2021-03-12T22:24:46-05:00",
			"created": "2012-04-07T13:50:16-04:00"
		}
	]
}