{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A275314",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 275314,
			"data": "1,2,3,3,5,4,7,4,5,6,11,5,13,8,7,5,17,6,19,7,9,12,23,6,9,14,7,9,29,8,31,6,13,18,11,7,37,20,15,8,41,10,43,13,9,24,47,7,13,10,19,15,53,8,15,10,21,30,59,9,61,32,11,7,17,14,67,19,25,12,71,8,73,38,11,21,17,16,79,9,9,42,83,11,21,44,31,14,89,10,19,25,33,48,23,8,97,14,15,11",
			"name": "Euler's gradus (\"suavitatis gradus\", or degrees of softness) function.",
			"comment": [
				"This series is described by Euler in the 1739 book \"Tentamen\", which provides numbers with gradus between 2 and 16 (page 41); the function is later used to calculate a measure of consonance of music intervals (e.g., see ratios on page 61). A description of Euler's function appears as a footnote in Helmhotz \"Sensations of Tone\", which states when p is prime the degree is p, all other numbers are products of prime numbers and the number of the degree for a product of two factors a and b, for which separately the numbers of degree are a and b respectively, is a + b - 1."
			],
			"link": [
				"Michael S. Branicky, \u003ca href=\"/A275314/b275314.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"David De Roure, P. Willcox, and D. M. Weigl, \u003ca href=\"https://wp.nyu.edu/ismir2016/wp-content/uploads/sites/2294/2016/08/deroure-numbers.pdf\"\u003eNumbers Into Notes: Cast Your Mind Back 200 Years\u003c/a\u003e, Extended Abstracts for the Late-Breaking Demo Session of the 17th International Society for Music Information Retrieval Conference, 2016.",
				"L. Euler, \u003ca href=\"https://archive.org/stream/tentamennovaethe00eule#page/40/mode/2up\"\u003eTentamen novae theoriae mvsicae ex certissimis harmoniae principiis dilvcide expositae\u003c/a\u003e, Petropoli, ex typographia Academiae scientiarvm, 1739, page 41.",
				"Daniel Muzzulini, \u003ca href=\"http://www.muzzulini.ch/publications/dm_1994_EulerOnConsonance.pdf\"\u003eLeonhard Eulers Konsonanztheorie\u003c/a\u003e, Musiktheorie 1994, 2, 135-146 (in German).",
				"H. L. F. von Helmholtz, \u003ca href=\"https://archive.org/stream/onsensationsofto00helmrich#page/230/mode/2up\"\u003eOn the sensations of tone as a physiological basis for the theory of music\u003c/a\u003e, 1895, chapter XII. See Footnote p. 231.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Sensations_of_Tone\"\u003eSensations of Tone\u003c/a\u003e."
			],
			"formula": [
				"If n = Product (p_j^k_j) then a(n) = 1 + Sum (k_j * (p_j - 1)).",
				"a(n) = A001414(n) - A001222(n) + 1. - _Michel Marcus_, Jul 23 2016"
			],
			"example": [
				"For n = 5 the prime factors are 5 so a(5) = 1 + 4 = 5.",
				"For n = 6 the prime factors are 2 and 3 so a(6) = 1 + (1 + 2) = 4."
			],
			"mathematica": [
				"Gradus[n_] := Plus @@ (Flatten[Table[#1, {#2}] \u0026 @@@ FactorInteger[n]] - 1) + 1"
			],
			"program": [
				"(PARI) a(n) = my(f = factor(n)); sum(k=1, #f~, (f[k,1]-1)*f[k,2])+ 1; \\\\ _Michel Marcus_, Jul 23 2016",
				"(Python)",
				"from sympy import factorint",
				"def a(n): return 1 + sum(kj*(pj-1) for pj, kj in factorint(n).items())",
				"print([a(n) for n in range(1, 101)]) # _Michael S. Branicky_, Dec 12 2021"
			],
			"xref": [
				"Cf. A001414, A001222."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_David De Roure_, Jul 22 2016",
			"references": 3,
			"revision": 40,
			"time": "2021-12-12T22:51:28-05:00",
			"created": "2016-07-28T12:17:14-04:00"
		}
	]
}