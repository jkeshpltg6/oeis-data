{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A306211",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 306211,
			"data": "1,1,2,2,1,2,2,1,2,2,1,2,1,2,2,1,2,1,2,1,1,1,2,2,1,2,1,2,1,1,1,2,1,1,1,1,3,2,2,1,2,1,2,1,1,1,2,1,1,1,1,3,2,1,1,1,1,3,1,4,1,2,2,1,2,1,2,1,1,1,2,1,1,1,1,3,2,1,1,1,1,3,1,4,1,2,1,1,1,1,3",
			"name": "Concatenation of the current sequence with the lengths of the runs in the sequence, with a(1) = 1.",
			"comment": [
				"Conjecture: All terms are less than or equal to 5. - _Peter Kagey_, Jan 29 2019",
				"Conjecture: Every number appears! (Based on the analogy with the somewhat similar sequence A090822, where the first 5 appeared at around 10^(10^23) steps). - _N. J. A. Sloane_, Jan 29 2019",
				"An alternative definition: Start with 1, extend the sequence by appending its RUNS transform, recompute the RUNS transform, append it, repeat. - _N. J. A. Sloane_, Jan 29 2019",
				"The first time we see 1, 2, 3, 4, 5 is at n=1, 3, 37, 60, 255. After 65 generations (10228800161220 terms) the largest term is 5. The relative frequencies of 1..5 are roughly 0.71, 6.7e-9, 0.23, 1.6e-8, 0.061. 2s and 4s appear to get rarer as n increases. - _Benjamin Chaffin_, Feb 07 2019",
				"If we record the successive RUNS transforms and concatenate them, we get 1; 2; 2, 1; 2, 2, 1; 2, 2, 1, 2, 1; ..., which is this sequence without the initial 1. - _A. D. Skovgaard_, Jan 30 2019 (Rephrased by _N. J. A. Sloane_, Jan 30 2019)"
			],
			"link": [
				"Peter Kagey, \u003ca href=\"/A306211/b306211.txt\"\u003eTable of n, a(n) for n = 1..10029\u003c/a\u003e (first 20 generations)",
				"N. J. A. Sloane, \u003ca href=\"/A306211/a306211_1.txt\"\u003eTable of n, a(n) for n = 1..236878\u003c/a\u003e (first 27 generations)",
				"N. J. A. Sloane, \u003ca href=\"/A306211/a306211.txt\"\u003eNotes on A306211\u003c/a\u003e, Feb 01 2019"
			],
			"example": [
				"a(2) = 1, since there is a run of length 1 at a(1).",
				"a(3) = 2, since there is a run of length 2 at a(1..2).",
				"a(4..5) = 2, 1, since the runs are as follows:",
				"1, 1, 2  a(1..3)",
				"\\__/  |",
				"2,    1  a(4..5)",
				"a(37) = 3, since a(20..22) = 1, 1, 1.",
				"Steps in construction:",
				"[1]  initial sequence",
				"[1]  its run length",
				".",
				"[1, 1]  concatenation of above is new sequence",
				"[2]  its run length",
				".",
				"[1, 1, 2] concatenation of above is new sequence",
				"[2, 1]  its run lengths",
				".",
				"[1, 1, 2, 2, 1]",
				"[2, 2, 1]",
				".",
				"[1, 1, 2, 2, 1, 2, 2, 1]",
				"[2, 2, 1, 2, 1]",
				".",
				"[1, 1, 2, 2, 1, 2, 2, 1, 2, 2, 1, 2, 1]",
				"[2, 2, 1, 2, 1, 2, 1, 1, 1]",
				".",
				"[1, 1, 2, 2, 1, 2, 2, 1, 2, 2, 1, 2, 1, 2, 2, 1, 2, 1, 2, 1, 1, 1]",
				"[2, 2, 1, 2, 1, 2, 1, 1, 1, 2, 1, 1, 1, 1, 3]",
				".",
				"[1, 1, 2, 2, 1, 2, 2, 1, 2, 2, 1, 2, 1, 2, 2, 1, 2, 1, 2, 1, 1, 1, 2, 2, 1, 2, 1, 2, 1, 1, 1, 2, 1, 1, 1, 1, 3]",
				"From _N. J. A. Sloane_, Jan 31 2019: (Start)",
				"The first 9 generations, in compressed notation (see A323477) are:",
				"1",
				"11",
				"112",
				"11221",
				"11221221",
				"1122122122121",
				"1122122122121221212111",
				"1122122122121221212111221212111211113",
				"1122122122121221212111221212111211113221212111211113211113141",
				"... (End)"
			],
			"maple": [
				"P:=proc(q) local a,b,c,k,n; a:=[1,1];",
				"for n from 1 to q do b:=1; c:=[];",
				"for k from 1 to nops(a)-1 do if a[k+1]=a[k] then b:=b+1;",
				"else c:=[op(c),b]; b:=1; fi; od; a:=[op(a),op(c),b]; od;",
				"a; end: P(10); # _Paolo P. Lava_, Jan 30 2019. P(g) produces generations 1 through g+2."
			],
			"program": [
				"(Haskell)",
				"group [] = []",
				"group (x:xs)= (x:ys):group zs where (ys,zs) = span (==x) xs",
				"a306211_next_gen xs = xs ++ (map length $ group xs)",
				"a306211_gen 1 = [1]",
				"a306211_gen n = a306211_next_gen $ a306211_gen (n-1)",
				"a306211 n = a306211_gen n !! (n-1)",
				"-- _Jean-François Antoniotti_, Jan 31 2021"
			],
			"xref": [
				"Cf. A000002, A107946, A306215, A090822.",
				"Positions of 3's, 4's, 5's: A323476, A306222, A306223.",
				"Successive generations: A323477, A323478, A306215, A323475, A306333.",
				"See also A323479, A323480, A323481, A323826 (RUNS transform), A323827, A323829 (where n first appears)."
			],
			"keyword": "nonn,nice",
			"offset": "1,3",
			"author": "_A. D. Skovgaard_, Jan 29 2019",
			"references": 16,
			"revision": 90,
			"time": "2021-02-27T15:34:52-05:00",
			"created": "2019-01-29T14:06:02-05:00"
		}
	]
}