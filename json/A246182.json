{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A246182",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 246182,
			"data": "1,1,1,1,3,0,1,5,2,0,1,9,5,2,0,1,19,9,6,2,0,1,39,21,12,7,2,0,1,79,53,27,15,8,2,0,1,167,118,74,34,18,9,2,0,1,357,269,180,96,42,21,10,2,0,1,763,639,419,254,119,51,24,11,2,0,1,1651,1486,1045,605,340,143,61,27,12,2,0,1",
			"name": "Triangle read by rows: T(n,k) is the number of weighted lattice paths B(n) having k hh's. B(n) is the set of lattice paths of weight n that start in (0,0), end on the horizontal axis and never go below this axis, whose steps are of the following four kinds: a (1,0)-step h of weight 1; a (1,0)-step H of weight 2; a (1,1)-step u of weight 2; a (1,-1)-step d of weight 1. The weight of a path is the sum of the weights of its steps.",
			"comment": [
				"Number of entries in row n is n (n\u003e=1).",
				"Sum of entries in row n is A004148(n+1) (the 2ndary structure numbers).",
				"Sum(k*T(n,k), k\u003e=0) = A110320(n-1) (n\u003e=1)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A246182/b246182.txt\"\u003eRows n = 0..141, flattened\u003c/a\u003e",
				"M. Bona and A. Knopfmacher, \u003ca href=\"http://dx.doi.org/10.1007/s00026-010-0060-7\"\u003eOn the probability that certain compositions have the same number of parts\u003c/a\u003e, Ann. Comb., 14 (2010), 291-306."
			],
			"formula": [
				"G.f. G=G(t,z) satisfies z^3*(1+z-t*z)*G^2 - (1-t*z-z^2+t*z^3-z^3)*G+1+z-t*z=0."
			],
			"example": [
				"Row 3 is 3,0,1. Indeed, the four paths of weight 3 are: ud, hH, Hh, and hhh, having 0, 0, 0, and 2 hh's, respectively.",
				"Triangle starts:",
				"1;",
				"1;",
				"1,1;",
				"3,0,1;",
				"5,2,0,1;",
				"9,5,2,0,1;"
			],
			"maple": [
				"eq := z^3*(1+z-t*z)*G^2-(-z^3+1-z^2-t*z+t*z^3)*G+1+z-t*z = 0: g := RootOf(eq, G): gser := simplify(series(g, z = 0, 18)): for j from 0 to 15 do P[j] := coeff(gser, z, j) end do: 1; for j to 13 do seq(coeff(P[j], t, q), q = 0 .. j-1) end do; # yields sequence in triangular form",
				"# second Maple program:",
				"b:= proc(n, y, t) option remember; `if`(y\u003c0 or y\u003en, 0, `if`(n=0, 1,",
				"      expand(b(n-1, y, 1)*`if`(t=1, x, 1)+ `if`(n\u003e1, b(n-2, y, 0)+",
				"      b(n-2, y+1, 0), 0) +b(n-1, y-1, 0))))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=0..degree(p)))(b(n, 0$2)):",
				"seq(T(n), n=0..12); # _Alois P. Heinz_, Aug 24 2014"
			],
			"mathematica": [
				"b[n_, y_, t_] := b[n, y, t] = If[y\u003c0 || y\u003en, 0, If[n==0, 1, Expand[b[n-1, y, 1] * If[t==1, x, 1] + If[n\u003e1, b[n-2, y, 0] + b[n-2, y+1, 0], 0] + b[n-1, y-1, 0]]]]; T[n_] := Function[{p}, Table[Coefficient[p, x, i], {i, 0, Exponent[p, x]}]][b[n, 0, 0]]; Table[T[n], {n, 0, 12}] // Flatten (* _Jean-François Alcover_, Jun 29 2015, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A004148, A110320, A246177."
			],
			"keyword": "nonn,tabf",
			"offset": "0,5",
			"author": "_Emeric Deutsch_, Aug 23 2014",
			"references": 1,
			"revision": 17,
			"time": "2015-06-29T08:31:08-04:00",
			"created": "2014-08-24T11:34:26-04:00"
		}
	]
}