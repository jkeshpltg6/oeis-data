{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A168561",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 168561,
			"data": "1,0,1,1,0,1,0,2,0,1,1,0,3,0,1,0,3,0,4,0,1,1,0,6,0,5,0,1,0,4,0,10,0,6,0,1,1,0,10,0,15,0,7,0,1,0,5,0,20,0,21,0,8,0,1,1,0,15,0,35,0,28,0,9,0,1,0,6,0,35,0,56,0,36,0,10,0,1,1,0,21,0,70,0,84,0,45,0,11,0,1",
			"name": "Riordan array (1/(1-x^2), x/(1-x^2)). Unsigned version of A049310.",
			"comment": [
				"Row sums: A000045(n+1), Fibonacci numbers.",
				"A168561*A007318 = A037027, as lower triangular matrices. Diagonal sums : A077957. - _Philippe Deléham_, Dec 02 2009",
				"T(n,k) is the number of compositions of n+1 into k+1 odd parts. Example: T(4,2)=3 because we have 5 = 1+1+3 = 1+3+1 = 3+1+1.",
				"Coefficients of monic Fibonacci polynomials (rising powers of x). Ftilde(n, x) = x*Ftilde(n-1, x) + Ftilde(n-2, x), n \u003e=0, Ftilde(-1,x) = 0, Ftilde(0, x) = 1. G.f.: 1/(1 - x*z - z^2). Compare with Chebyshev S-polynomials (A049310). - _Wolfdieter Lang_, Jul 29 2014"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A168561/b168561.txt\"\u003eTable of n, a(n) for the first 50 rows, flattened\u003c/a\u003e",
				"J.P. Allouche and M. Mendès-France, \u003ca href=\"http://arxiv.org/abs/1202.0211\"\u003eStern-Brocot polynomials and power series\u003c/a\u003e, arXiv preprint arXiv:1202.0211 [math.NT], 2012. - From _N. J. A. Sloane_, May 10 2012",
				"Tom Copeland, \u003ca href=\"http://tcjpn.wordpress.com/2015/10/12/the-elliptic-lie-triad-kdv-and-ricattt-equations-infinigens-and-elliptic-genera/\"\u003eAddendum to Elliptic Lie Triad\u003c/a\u003e",
				"Milan Janjić, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL21/Janjic/janjic93.html\"\u003eWords and Linear Recurrences\u003c/a\u003e, J. Int. Seq. 21 (2018), #18.1.4."
			],
			"formula": [
				"Sum_{k=0..n} T(n,k)*x^k = A059841(n), A000045(n+1), A000129(n+1), A006190(n+1), A001076(n+1), A052918(n), A005668(n+1), A054413(n), A041025(n), A099371(n+1), A041041(n), A049666(n+1), A041061(n), A140455(n+1), A041085(n), A154597(n+1), A041113(n) for x = 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16 respectively. - _Philippe Deléham_, Dec 02 2009",
				"T(2n,2k) = A085478(n,k). T(2n+1,2k+1) = A078812(n,k). Sum_{k=0..n} T(n,k)*x^(n-k) = A000012(n), A000045(n+1), A006131(n), A015445(n), A168579(n), A122999(n) for x = 0,1,2,3,4,5 respectively. - _Philippe Deléham_, Dec 02 2009",
				"T(n,k) = binomial((n+k)/2,k) if (n+k) is even; otherwise T(n,k)=0.",
				"G.f.: (1-z^2)/(1-tz-z^2) if offset is 1.",
				"T(n,k) = T(n-1,k-1) + T(n-2,k), T(0,0) = 1, T(0,1) = 0. - _Philippe Deléham_, Feb 09 2012",
				"Sum_{k=0..n} T(n,k)^2 = A051286(n). - _Philippe Deléham_, Feb 09 2012"
			],
			"example": [
				"The triangle T(n,k) begins:",
				"n\\k 0  1   2   3   4    5    6    7    8    9  10  11  12  13 14 15 ...",
				"0:  1",
				"1:  0  1",
				"2:  1  0   1",
				"3:  0  2   0   1",
				"4:  1  0   3   0   1",
				"5:  0  3   0   4   0    1",
				"6:  1  0   6   0   5    0    1",
				"7:  0  4   0  10   0    6    0    1",
				"8:  1  0  10   0  15    0    7    0    1",
				"9:  0  5   0  20   0   21    0    8    0    1",
				"10: 1  0  15   0  35    0   28    0    9    0   1",
				"11: 0  6   0  35   0   56    0   36    0   10   0   1",
				"12: 1  0  21   0  70    0   84    0   45    0  11   0   1",
				"13: 0  7   0  56   0  126    0  120    0   55   0  12   0   1",
				"14: 1  0  28   0 126    0  210    0  165    0  66   0  13   0  1",
				"15: 0  8   0  84   0  252    0  330    0  220   0  78   0  14  0  1",
				"... reformatted by _Wolfdieter Lang_, Jul 29 2014.",
				"------------------------------------------------------------------------"
			],
			"maple": [
				"T:=proc(n,k) if n-k mod 2 = 0 then binomial((n+k)/2,k) else 0 fi end: for n from 0 to 12 do seq(T(n,k),k=0..n) od; # yields sequence in triangular form"
			],
			"mathematica": [
				"Table[If[EvenQ[n + k], Binomial[(n + k)/2, k], 0], {n, 0, 10}, {k, 0, n}] // Flatten (* _G. C. Greubel_, Apr 16 2017 *)"
			],
			"program": [
				"(PARI) T(n,k) = if ((n+k) % 2, 0, binomial((n+k)/2,k));",
				"tabl(nn) = for (n=0, nn, for (k=0, n, print1(T(n,k), \", \")); print();); \\\\ _Michel Marcus_, Oct 09 2016"
			],
			"xref": [
				"Cf. A162515 (rows reversed), A112552."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,8",
			"author": "_Philippe Deléham_, Nov 29 2009",
			"ext": [
				"Typo in name corrected (1(1-x^2) changed to 1/(1-x^2)) by _Wolfdieter Lang_, Nov 20 2010"
			],
			"references": 17,
			"revision": 57,
			"time": "2021-07-03T14:29:26-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}