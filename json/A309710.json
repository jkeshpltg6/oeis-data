{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A309710",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 309710,
			"data": "1,0,6,4,7,3,4,1,7,1,0,4,3,5,0,3,3,7,0,3,9,2,8,2,7,4,5,1,4,6,1,6,6,8,8,8,9,4,8,3,0,9,9,1,5,1,7,7,4,4,8,5,1,2,4,4,1,9,8,7,4,5,0,8,0,6,3,9,9,0,1,7,1,7,5,8,6,4,3,7,6,3,6,6,6,5,3,4,2,5,0",
			"name": "Decimal expansion of Sum_{k\u003e=1} Kronecker(-8,k)/k^2.",
			"comment": [
				"Let Chi() be a primitive character modulo d, the so-called Dirichlet L-series L(s,Chi) is the analytic continuation (see the functional equations involving L(s,Chi) in the MathWorld link entitled Dirichlet L-Series) of the sum Sum_{k\u003e=1} Chi(k)/k^s, Re(s)\u003e0 (if d = 1, the sum converges requires Re(s)\u003e1).",
				"If s != 1, we can represent L(s,Chi) in terms of the Hurwitz zeta function by L(s,Chi) = (Sum_{k=1..d} Chi(k)*zeta(s,k/d))/d^s.",
				"L(s,Chi) can also be represented in terms of the polylog function by L(s,Chi) = (Sum_{k=1..d} Chi'(k)*polylog(s,u^k))/(Sum_{k=1..d} Chi'(k)*u^k), where Chi' is the complex conjugate of Chi, u is any primitive d-th root of unity.",
				"If m is a positive integer, we have L(m,Chi) = (Sum_{k=1..d} Chi(k)*polygamma(m-1,k/d))/((-d)^m*(m-1)!).",
				"In this sequence we have Chi = A188510 and s = 2."
			],
			"link": [
				"Steven R. Finch, \u003ca href=\"https://doi.org/10.1017/9781316997741\"\u003eMathematical Constants II\u003c/a\u003e, Encyclopedia of Mathematics and Its Applications, Cambridge University Press, Cambridge, 2018, p. 98.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DirichletL-Series.html\"\u003eDirichlet L-Series\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PolygammaFunction.html\"\u003ePolygamma Function\u003c/a\u003e"
			],
			"formula": [
				"Equals (zeta(2,1/8) + zeta(2,3/8) - zeta(2,5/8) - zeta(2,7/8))/64, where zeta(s,a) is the Hurwitz zeta function.",
				"Equals (polylog(2,u) + polylog(2,u^3) - polylog(2,-u) - polylog(2,-u^3))/sqrt(-8), where u = sqrt(2)/2 + i*sqrt(2)/2 is an 8th primitive root of unity, i = sqrt(-1).",
				"Equals (polygamma(1,1/8) + polygamma(1,3/8) - polygamma(1,5/8) - polygamma(1,7/8))/64."
			],
			"example": [
				"1 + 1/3^2 - 1/5^2 - 1/7^2 + 1/9^2 + 1/11^2 - 1/13^2 - 1/15^2 + ...= 1.0647341710..."
			],
			"mathematica": [
				"(PolyGamma[1, 1/8] + PolyGamma[1, 3/8] - PolyGamma[1, 5/8] - PolyGamma[1, 7/8])/64 // RealDigits[#, 10, 102] \u0026 // First"
			],
			"xref": [
				"Cf. A188510.",
				"Decimal expansion of Sum_{k\u003e=1} Kronecker(d,k)/k^2, where d is a fundamental discriminant: this sequence (d=-8), A103133 (d=-7), A006752 (d=-4), A086724 (d=-3), A013661 (d=1), A328717 (d=5), A328895 (d=8), A258414 (d=12).",
				"Decimal expansion of Sum_{k\u003e=1} Kronecker(-8,k)/k^s: A093954 (s=1), this sequence (s=2), A251809 (s=3)."
			],
			"keyword": "nonn,cons",
			"offset": "1,3",
			"author": "_Jianing Song_, Nov 19 2019",
			"references": 2,
			"revision": 33,
			"time": "2021-06-13T05:20:27-04:00",
			"created": "2019-11-21T07:30:04-05:00"
		}
	]
}