{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A036987",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 36987,
			"data": "1,1,0,1,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0",
			"name": "Fredholm-Rueppel sequence.",
			"comment": [
				"Binary representation of the Kempner-Mahler number Sum_{k\u003e=0} 1/2^(2^k) = A007404.",
				"Also a(n) = A(n) mod 2 where A is any of A001700, A005573, A007854, A026641, A049027, A064063, A064088, A064090, A064092, A064325, A064327, A064329, A064331, A064613, A076026, A105523, A123273, A126694, A126930, A126931, A126982, A126983, A126987, A127016, A127053, A127358, A127360, A127361, A127363. - _Philippe Deléham_, May 26 2007",
				"a(n) = (product of digits of n; n in binary notation) mod 2. This sequence is a transformation of the Thue-Morse sequence (A010060), since there exists a function f such that f(sum of digits of n) = (product of digits of n). - _Ctibor O. Zizka_, Feb 12 2008",
				"a(n-1), n \u003e= 1, the characteristic sequence for powers of 2, A000079, is the unique solution of the following formal product and formal power series identity: Product_{j\u003e=1} (1 + a(j-1)*x^j) = 1 + Sum_{k\u003e=1} x^k = 1/(1-x). The product is therefore Product_{l\u003e=1} (1 + x^(2^l)). Proof. Compare coefficients of x^n and use the binary representation of n. Uniqueness follows from the recurrence relation given for the general case under A147542. - _Wolfdieter Lang_, Mar 05 2009",
				"a(n) is also the number of orbits of length n for the map x -\u003e 1-cx^2 on [-1,1] at the Feigenbaum critical value c=1.401155... . - _Thomas Ward_, Apr 08 2009",
				"A054525 (Mobius transform) * A001511 = A036987 = A047999^(-1) * A001511 = the inverse of Sierpiński's gasket * the ruler sequence. - _Gary W. Adamson_, Oct 26 2009 [Of course this is only vaguely correct depending on how the fuzzy indexing in these formulas is made concrete. - _R. J. Mathar_, Jun 20 2014]",
				"Characteristic function of A000225. - _Reinhard Zumkeller_, Mar 06 2012",
				"Also parity of the Catalan numbers A000108. - _Omar E. Pol_, Jan 17 2012",
				"For n \u003e= 2, also the largest exponent k \u003e= 0 such that n^k in binary notation does not contain both 0 and 1. Unlike for the decimal version of this sequence, A062518, where the terms are only conjectural, for this sequence the values of a(n) can be proved to be the characteristic function of A000225, as follows: n^k will contain both 0 and 1 unless n^k = 2^r-1 for some r. But this is a special case of Catalan's equation x^p = y^q-1, which was proved by Preda Mihăilescu to have no nontrivial solution except 2^3 = 3^2 - 1. - _Christopher J. Smyth_, Aug 22 2014",
				"Image, under the coding a,b -\u003e 1; c -\u003e 0, of the fixed point, starting with a, of the morphism a -\u003e ab, b -\u003e cb, c -\u003e cc. - _Jeffrey Shallit_, May 14 2016",
				"Number of nonisomorphic Boolean algebras of order n+1. - _Jianing Song_, Jan 23 2020"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A036987/b036987.txt\"\u003eTable of n, a(n) for n = 0..65537\u003c/a\u003e",
				"D. Bailey et al., \u003ca href=\"https://doi.org/10.5802/jtnb.457\"\u003eOn the binary expansions of algebraic numbers\u003c/a\u003e, Journal de Théorie des Nombres de Bordeaux 16 (2004), 487-518.",
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/2005.04066\"\u003eSome observations on the Rueppel sequence and associated Hankel determinants\u003c/a\u003e, arXiv:2005.04066 [math.CO], 2020.",
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/2107.00442\"\u003eConjectures and results on some generalized Rueppel sequences\u003c/a\u003e, arXiv:2107.00442 [math.CO], 2021.",
				"Daniele A. Gewurz and Francesca Merola, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL6/Gewurz/gewurz5.html\"\u003eSequences realized as Parker vectors of oligomorphic permutation groups\u003c/a\u003e, J. Integer Seqs., Vol. 6, 2003.",
				"Mats Granvik, \u003ca href=\"http://pastebin.com/9KDtcwfz\"\u003eMathematica program for computing Fredholm Rueppel sequences\u003c/a\u003e",
				"D. Kohel, S. Ling and C. Xing, \u003ca href=\"http://www.maths.usyd.edu.au/u/kohel/doc/perfect.ps\"\u003eExplicit Sequence Expansions\u003c/a\u003e, in Sequences and their Applications, C. Ding, T. Helleseth, and H. Niederreiter, eds., Proceedings of SETA'98 (Singapore, 1998), 308-317, 1999.",
				"Preda Mihăilescu, \u003ca href=\"http://dx.doi.org/10.1515/crll.2004.048\"\u003ePrimary Cyclotomic Units and a Proof of Catalan's Conjecture\u003c/a\u003e, J. Reine angew. Math. 572 (2004): 167-195. doi:10.1515/crll.2004.048. MR 2076124.",
				"H. Niederreiter and M. Vielhaber, \u003ca href=\"http://dx.doi.org/10.1006/jcom.1996.0014\"\u003eTree Complexity and a Doubly Exponential Gap between Structured and Random Sequences\u003c/a\u003e, J. Complexity, 12 (1996), 187-198.",
				"Apisit Pakapongpun and Thomas Ward, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL12/Ward/ward17.html\"\u003eFunctorial orbit counting\u003c/a\u003e, Journal of Integer Sequences, 12 (2009) Article 09.2.4. [From _Thomas Ward_, Apr 08 2009]",
				"Eric Rowland and Reem Yassawi, \u003ca href=\"https://arxiv.org/abs/1403.7659\"\u003eProfinite automata\u003c/a\u003e, arXiv:1403.7659 [math.DS], 2014. See p. 8.",
				"E. Sheppard, \u003ca href=\"http://groups.google.com/groups?hl=en\u0026amp;selm=61%40epsilon.UUCP\"\u003enet.math post (1985)\u003c/a\u003e",
				"Stephen Wolfram, \u003ca href=\"http://www.wolframscience.com/nksonline/page-1092\"\u003e[Page 1092] A New Kind of Science | Online\u003c/a\u003e.",
				"\u003ca href=\"/index/Ch#char_fns\"\u003eIndex entries for characteristic functions\u003c/a\u003e"
			],
			"formula": [
				"1 followed by a string of 2^k - 1 0's. Also a(n)=1 iff n = 2^m - 1.",
				"a(n) = a(floor(n/2)) * (n mod 2) for n\u003e0 with a(0)=1. - _Reinhard Zumkeller_, Aug 02 2002 [Corrected by _Mikhail Kurkov_, Jul 16 2019]",
				"Sum_{n\u003e=0} 1/10^(2^n) = 0.110100010000000100000000000000010...",
				"1 if n=0, floor(log_2(n+1)) - floor(log_2(n)) otherwise. G.f.: (1/x) * Sum_{k\u003e=0} x^(2^k) = Sum_{k\u003e=0} x^(2^k-1). - _Ralf Stephan_, Apr 28 2003",
				"a(n) = 1 - A043545(n). - _Michael Somos_, Aug 25 2003",
				"a(n) = -Sum_{d|n+1} mu(2*d). - _Benoit Cloitre_, Oct 24 2003",
				"Dirichlet g.f. for right-shifted sequence: 2^(-s)/(1-2^(-s)).",
				"a(n) = A000108(n) mod 2 = A001405(n) mod 2. - _Paul Barry_, Nov 22 2004",
				"a(n) = Sum_{k=0..n} (-1)^(n-k)*binomial(n,k)*Sum_{j=0..k} binomial(k, 2^j-1). - _Paul Barry_, Jun 01 2006",
				"A000523(n+1) = Sum_{k=1..n} a(k). - _Mitch Harris_, Jul 22 2011",
				"a(n) = A209229(n+1). - _Reinhard Zumkeller_, Mar 07 2012",
				"a(n) = Sum_{k=1..n} A191898(n,k)*cos(Pi*(n-1)*(k-1))/n; (conjecture). - _Mats Granvik_, Mar 04 2013",
				"a(n) = A000035(A000108(n)). - _Omar E. Pol_, Aug 06 2013",
				"a(n) = 1 iff n=2^k-1 for some k, 0 otherwise. - _M. F. Hasler_, Jun 20 2014",
				"a(n) = ceiling(log_2(n+2)) - ceiling(log_2(n+1)). - _Gionata Neri_, Sep 06 2015",
				"From _John M. Campbell_, Jul 21 2016: (Start)",
				"a(n) = (A000168(n-1) mod 2).",
				"a(n) = (A000531(n+1) mod 2).",
				"a(n) = (A000699(n+1) mod 2).",
				"a(n) = (A000891(n) mod 2).",
				"a(n) = (A000913(n-1) mod 2), for n\u003e1.",
				"a(n) = (A000917(n-1) mod 2), for n\u003e0.",
				"a(n) = (A001142(n) mod 2).",
				"a(n) = (A001246(n) mod 2).",
				"a(n) = (A001246(n) mod 4).",
				"a(n) = (A002057(n-2) mod 2), for n\u003e1.",
				"a(n) = (A002430(n+1) mod 2). (End)",
				"a(n) = 2 - A043529(n). - _Antti Karttunen_, Nov 19 2017",
				"a(n) = floor(1+log(n+1)/log(2)) - floor(log(2n+1)/log(2)). - _Adriano Caroli_, Sep 22 2019",
				"This is also the decimal expansion of -Sum_{k\u003e=1} mu(2*k)/(10^k - 1), where mu is the Möbius function (A008683). - _Amiram Eldar_, Jul 12 2020"
			],
			"example": [
				"G.f. = 1 + x + x^3 + x^7 + x^15 + x^31 + x^63 + x^127 + x^255 + x^511 + ...",
				"a(7) = 1 since 7 = 2^3 - 1, while a(10) = 0 since 10 is not of the form 2^k - 1 for any integer k."
			],
			"maple": [
				"A036987:= n-\u003e `if`(2^ilog2(n+1) = n+1, 1, 0):",
				"seq (A036987(n), n=0..128);"
			],
			"mathematica": [
				"RealDigits[ N[ Sum[1/10^(2^n), {n, 0, Infinity}], 110]][[1]]",
				"(* Recurrence: *)",
				"t[n_, 1] = 1; t[1, k_] = 1;",
				"t[n_, k_] := t[n, k] =",
				"  If[n \u003c k, If[n \u003e 1 \u0026\u0026 k \u003e 1, -Sum[t[k - i, n], {i, 1, n - 1}], 0],",
				"   If[n \u003e 1 \u0026\u0026 k \u003e 1, Sum[t[n - i, k], {i, 1, k - 1}], 0]];",
				"Table[t[n, k], {k, n, n}, {n, 104}]",
				"(* _Mats Granvik_, Jun 03 2011 *)",
				"mb2d[n_]:=1 - Module[{n2 = IntegerDigits[n, 2]}, Max[n2] - Min[n2]]; Array[mb2d, 120, 0] (* _Vincenzo Librandi_, Jul 19 2019 *)"
			],
			"program": [
				"(PARI) {a(n) =( n++) == 2^valuation(n, 2)}; /* _Michael Somos_, Aug 25 2003 */",
				"(Haskell)",
				"a036987 n = ibp (n+1) where",
				"   ibp 1 = 1",
				"   ibp n = if r \u003e 0 then 0 else ibp n' where (n',r) = divMod n 2",
				"a036987_list = 1 : f [0,1] where f (x:y:xs) = y : f (x:xs ++ [x,x+y])",
				"-- Same list generator function as for a091090_list, cf. A091090.",
				"-- _Reinhard Zumkeller_, May 19 2015, Apr 13 2013, Mar 13 2013",
				"(Python)",
				"from sympy import catalan",
				"def a(n): return catalan(n)%2 # _Indranil Ghosh_, May 25 2017"
			],
			"xref": [
				"Cf. A007404, A043545, A062518, A078885, A078585, A078886, A078887, A078888, A078889, A078890.",
				"The first row of A073346. Occurs for first time in A073202 as row 6 (and again as row 8).",
				"Congruent to any of the sequences A000108, A007460, A007461, A007463, A007464, A061922, A068068 reduced modulo 2. Characteristic function of A000225.",
				"If interpreted with offset=1 instead of 0 (i.e., a(1)=1, a(2)=1, a(3)=0, a(4)=1, ...) then this is the characteristic function of 2^n (A000079) and as such occurs as the first row of A073265. Also, in that case the INVERT transform will produce A023359.",
				"This is Guy Steele's sequence GS(1, 3), also GS(3, 1) (see A135416).",
				"Cf. A054525, A047999. - _Gary W. Adamson_, Oct 26 2009",
				"Cf. A043529, A127802."
			],
			"keyword": "nonn",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Edited by _M. F. Hasler_, Jun 20 2014"
			],
			"references": 114,
			"revision": 216,
			"time": "2021-07-02T11:41:55-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}