{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A269940",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 269940,
			"data": "1,0,1,0,2,3,0,6,20,15,0,24,130,210,105,0,120,924,2380,2520,945,0,720,7308,26432,44100,34650,10395,0,5040,64224,303660,705320,866250,540540,135135,0,40320,623376,3678840,11098780,18858840,18288270,9459450,2027025",
			"name": "Triangle read by rows, T(n,k) = Sum_{m=0..k} (-1)^(m+k)*C(n+k,n+m)*Stirling1(n+m,m), for n\u003e=0, 0\u003c=k\u003c=n.",
			"link": [
				"Peter Luschny, \u003ca href=\"http://oeis.org/wiki/User:Peter_Luschny/P-Transform\"\u003eThe P-transform\u003c/a\u003e.",
				"A. Mansuy, \u003ca href=\"https://doi.org/10.1016/j.jalgebra.2014.04.017\"\u003ePreordered forests, packed words and contraction algebras\u003c/a\u003e, J. Algebra 411 (2014) 259-311, section 4.4"
			],
			"formula": [
				"T(n,k) = (-1)^k*FF(n+k,n)*P[n,k](n/(n+1)) where P is the P-transform and FF the falling factorial function. For the definition of the P-transform see the link.",
				"T(n,k) = A268438(n,k)*FF(n+k,n)/(2*n)!."
			],
			"example": [
				"Triangle starts:",
				"[1]",
				"[0, 1]",
				"[0, 2, 3]",
				"[0, 6, 20, 15]",
				"[0, 24, 130, 210, 105]",
				"[0, 120, 924, 2380, 2520, 945]",
				"[0, 720, 7308, 26432, 44100, 34650, 10395]",
				"[0, 5040, 64224, 303660, 705320, 866250, 540540, 135135]"
			],
			"maple": [
				"T := (n, k) -\u003e add((-1)^(m+k)*binomial(n+k,n+m)*abs(Stirling1(n+m, m)), m=0..k):",
				"seq(print(seq(T(n, k), k=0..n)), n=0..6);",
				"# Alternatively:",
				"T := proc(n, k) option remember;",
				"    `if`(k=0, k^n,",
				"    `if`(k\u003c=0 or k\u003en, 0,",
				"    (n+k-1)*(T(n-1, k)+T(n-1, k-1)))) end:",
				"for n from 0 to 6 do seq(T(n, k), k=0..n) od;"
			],
			"program": [
				"(Sage)",
				"T = lambda n, k: sum((-1)^(m+k)*binomial(n+k, n+m)*stirling_number1(n+m, m) for m in (0..k))",
				"for n in (0..7): print([T(n, k) for k in (0..n)])",
				"(Sage) # uses[PtransMatrix from A269941]",
				"PtransMatrix(8, lambda n: n/(n+1), lambda n, k: (-1)^k*falling_factorial(n+k,n))"
			],
			"xref": [
				"Variants: A111999, A259456.",
				"Cf. A269939 (Stirling2 counterpart), A268438, A032188 (row sums)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Peter Luschny_, Mar 27 2016",
			"references": 4,
			"revision": 20,
			"time": "2020-11-13T06:47:01-05:00",
			"created": "2016-03-29T03:45:46-04:00"
		}
	]
}