{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A117618",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 117618,
			"data": "1,6,7,10,22,683",
			"name": "Least number with complexity height of n, under integer complexity A005245.",
			"comment": [
				"A005245 Complexity of n: number of 1's required to build n using + and * (and parentheses). A005520 Smallest number of complexity n: smallest number requiring n 1's to build using + and *. Now consider the recursion: A005245(n), A005245(A005245(n)), A005245(A005245(A005245(n))), ... which we know is finite before reaching a fixed point, as A005245(n) \u003c= n. The number of steps needed to reach such a fixed point is the complexity height of n (with respect to the A005245 measure of complexity, there being others in the OEIS)."
			],
			"reference": [
				"W. A. Beyer, M. L. Stein and S. M. Ulam, The Notion of Complexity. Report LA-4822, Los Alamos Scientific Laboratory of the University of California, Los Alamos, NM, December 1971.",
				"R. K. Guy, Unsolved Problems in Number Theory, Sect. F26."
			],
			"link": [
				"W. A. Beyer, M. L. Stein and S. M. Ulam, \u003ca href=\"/A003037/a003037.pdf\"\u003eThe Notion of Complexity\u003c/a\u003e. Report LA-4822, Los Alamos Scientific Laboratory of the University of California, Los Alamos, NM, December 1971. [Annotated scanned copy]",
				"R. K. Guy, \u003ca href=\"http://www.jstor.org/stable/2323338\"\u003eSome suspiciously simple sequences\u003c/a\u003e, Amer. Math. Monthly 93 (1986), 186-190; 94 (1987), 965; 96 (1989), 905.",
				"E. Pegg, Jr., \u003ca href=\"http://library.wolfram.com/infocenter/MathSource/5175/\"\u003eInteger Complexity.\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/IntegerComplexity.html\"\u003eInteger Complexity.\u003c/a\u003e",
				"\u003ca href=\"/index/Com#complexity\"\u003eIndex to sequences related to the complexity of n\u003c/a\u003e"
			],
			"formula": [
				"a(n) = least k such that A005245^(n)(k) = A005245^(n-1)(k) but (if n\u003e1) A005245^(n-1)(k) != A005245^(n-2)(k), where ^ denotes repeated application."
			],
			"example": [
				"a(1) = 1 because the A005245 complexity of 1 is 1, already giving a fixed point.",
				"a(2) = 6 because it is the smallest x such that A005245(x) =/= x and A005245(x) = A005245(A005245(x)).",
				"a(3) = 7 because 7 is the least number x with complexity 6, thus taking a further step of recursion to reach a fixed point.",
				"a(4) = 10 because 10 is the least number with complexity 7.",
				"a(5) = 22 because 22 is the least number with complexity 10.",
				"a(6) = 683 because 683 is the least number with complexity 22.",
				"a(7) = the least number with complexity 683."
			],
			"xref": [
				"Cf. A003037, A003313, A005245, A005421, A005520, A025280, A061373, A064097, A076091, A076142."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Jonathan Vos Post_, Apr 07 2006",
			"ext": [
				"a(2)=6 inserted by _Giovanni Resta_, Jun 15 2016"
			],
			"references": 3,
			"revision": 32,
			"time": "2017-06-09T20:43:11-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}