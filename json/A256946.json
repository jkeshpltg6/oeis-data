{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A256946",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 256946,
			"data": "1,2,3,1,4,5,2,6,7,3,8,1,4,9,10,5,11,2,6,12,13,7,3,14,8,15,1,4,9,16,17,10,5,18,11,19,2,6,12,20,21,13,7,22,3,14,23,8,15,24,1,4,9,16,25,26,17,10,27,5,18,28,11,19,29,2,6,12,20,30,31,21,13,7,32,22,3,14,33,23,8,34,15,24,35",
			"name": "Irregular triangle where n-th row is integers from 1 to n*(n+2), sorted with first squares in order, then remaining numbers by fractional part of the square root.",
			"comment": [
				"This is a fractal sequence.",
				"T(n,k) = T(n+1,A256507(n,k),k), that is, A256507 gives the positions of n-th's row terms in row n+1. - _Reinhard Zumkeller_, Apr 22 2015"
			],
			"link": [
				"Franklin T. Adams-Watters, \u003ca href=\"/A256946/b256946.txt\"\u003eTable of n, a(n) for n = 1..10385\u003c/a\u003e (Rows 1 to 30, flattened.)"
			],
			"example": [
				"The table starts:",
				"1 2 3",
				"1 4 5 2 6 7 3 8",
				"1 4 9 10 5 11 2 6 12 13 7 3 14 8 15",
				"1 4 9 16 17 10 5 18 11 19 2 6 12 20 21 13 7 22 3 14 23 8 15 24"
			],
			"mathematica": [
				"row[n_] := SortBy[Range[n(n+2)], If[IntegerQ[Sqrt[#]], 0, N[FractionalPart[ Sqrt[#]]]]\u0026];",
				"Array[row, 5] // Flatten (* _Jean-François Alcover_, Sep 17 2019 *)"
			],
			"program": [
				"(PARI) arow(n)=vecsort(vector(n*(n+2),k,if(issquare(k),0.,sqrt(k)-floor(sqrt(k)))),,1) \\\\ This relies on vecsort being stable.",
				"(Haskell)",
				"import Data.List (sortBy); import Data.Function (on)",
				"a256946 n k = a256946_tabf !! (n-1) !! (k-1)",
				"a256946_row n = a256946_tabf !! (n-1)",
				"a256946_tabf = f 0 [] [] where",
				"   f k us vs = (xs ++ ys) : f (k+1) xs ys where",
				"     xs = us ++ qs",
				"     ys = sortBy (compare `on`",
				"                  snd . properFraction . sqrt . fromIntegral) (vs ++ rs)",
				"     (qs, rs) = span ((== 1) . a010052') [k*(k+2)+1 .. (k+1)*(k+3)]",
				"-- _Reinhard Zumkeller_, Apr 22 2015"
			],
			"xref": [
				"Cf. A005563 (row lengths and last of each row), A083374 (row sums).",
				"Cf. A256507."
			],
			"keyword": "nonn,tabf,nice",
			"offset": "1,2",
			"author": "_Franklin T. Adams-Watters_, Apr 19 2015",
			"references": 2,
			"revision": 36,
			"time": "2019-09-17T08:57:58-04:00",
			"created": "2015-04-21T15:39:07-04:00"
		}
	]
}