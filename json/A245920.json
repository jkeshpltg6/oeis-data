{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A245920",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 245920,
			"data": "2,1,2,1,2,2,1,2,1,2,2,1,2,2,1,2,1,2,2,1,2,2,1,2,1,2,2,1,2,1,2,2,1,2,2,1,2,1,2,2,1,2,1,2,2,1,2,2,1,2,1,2,2,1,2,2,1,2,1,2,2,1,2,1,2,2,1,2,2,1,2,1,2,2,1,2,2,1,2,1,2,2,1,2,1,2",
			"name": "Limit-reverse of the (2,1)-version of the infinite Fibonacci word A014675 with first term as initial block.",
			"comment": [
				"Suppose S = (s(0), s(1), s(2),...) is an infinite sequence such that every finite block of consecutive terms occurs infinitely many times in S.  (It is assumed that A014675 is such a sequence.)  Let B = B(m,k) = (s(m-k), s(m-k+1),...,s(m)) be such a block, where m \u003e= 0 and k \u003e= 0.  Let m(1) be the least i \u003e m such that (s(i-k), s(i-k+1),...,s(i)) = B(m,k), and put B(m(1),k+1) = (s(m(1)-k-1), s(m(1)-k),...,s(m(1))).  Let m(2) be the least i \u003e m(1) such that (s(i-k-1), s(i-k),...,s(i)) = B(m(1),k+1), and put B(m(2),k+2) = (s(m(2)-k-2), s(m(2)-k-1),...,s(m(2))).  Continuing in this manner gives a sequence of blocks B(m(n),k+n).  Let B'(n) = reverse(B(m(n),k+n)), so that for n \u003e= 1, B'(n) comes from B'(n-1) by suffixing a single term; thus the limit of B'(n) is defined; we call it the \"limit-reverse of S with initial block B(m,k)\", denoted by S*(m,k), or simply S*.",
				"The sequence (m(i)), where m(0) = 0, is the \"index sequence for limit-reversing S with initial block B(m,k)\" or simply the index sequence for S*, as in A245921.",
				"For numbers represented by taking S and S* as continued fractions, see A245975 and A245976.  If S is taken to be the classical (0,1)-version of the infinite Fibonacci word, then S* is obtained from the present sequence by substituting 0 for 2 throughout, as in A241422.",
				"The limit-reverse, S*, is analogous to a limiting block extension, S^, defined at A246127.  The essential difference is that S^ is formed by extending each new block one term to the right, whereas S* is formed by extending each new block one term to the left (and then reversing)."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A245920/b245920.txt\"\u003eTable of n, a(n) for n = 0..300\u003c/a\u003e"
			],
			"example": [
				"S = infinite Fibonacci word A014675, B = (s(0)); that is, (m,k) = (0,0);",
				"S = (2,1,2,2,1,2,1,2,2,1,2,2,1,2,1,2,2,1,2,...)",
				"B'(0) = (2)",
				"B'(1) = (2,1)",
				"B'(2) = (2,1,2)",
				"B'(3) = (2,1,2,1)",
				"B'(4) = (2,1,2,1,2)",
				"B'(5) = (2,1,2,1,2,2)",
				"S* = (2,1,2,1,2,2,1,2,1,2,2,1,2,2,1,2,1,2,2,1,2,...),",
				"with index sequence (0,2,5,7,15,...)"
			],
			"mathematica": [
				"z = 100; seqPosition2[list_, seqtofind_] := Last[Last[Position[Partition[list, Length[#], 1], Flatten[{___, #, ___}], 1, 2]]] \u0026[seqtofind]; x = GoldenRatio; s =  Differences[Table[Floor[n*x], {n, 1, z^2}]] ; ans = Join[{s[[p[0] = pos = seqPosition2[s, #] - 1]]}, #] \u0026[{s[[1]]}]; cfs = Table[s = Drop[s, pos - 1]; ans = Join[{s[[p[n] = pos = seqPosition2[s, #] - 1]]}, #] \u0026[ans], {n, z}]; rcf = Last[Map[Reverse, cfs]]"
			],
			"xref": [
				"Cf. A245921, A245922, A003849, A014675, A245975, A245976."
			],
			"keyword": "nonn",
			"offset": "0,1",
			"author": "_Clark Kimberling_ and _Peter J. C. Moses_, Aug 07 2014",
			"references": 23,
			"revision": 16,
			"time": "2014-08-21T17:55:25-04:00",
			"created": "2014-08-21T17:55:25-04:00"
		}
	]
}