{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A180009",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 180009,
			"data": "1,-1,-1,-1,0,-1,0,0,0,-1,-1,-1,1,0,-1,1,2,-2,1,0,-1,-1,-3,2,-1,1,0,-1,0,3,-1,1,-1,1,0,-1,0,-3,-1,-2,2,-1,1,0,-1,1,4,2,2,-3,2,-1,1,0,-1,-1,-5,0,-1,1,-2,2,-1,1,0,-1,0,5,-3,2,1,0,-2,2,-1,1,0,-1,-1,-6,3,-4,0,0,1,-2,2,-1,1,0,-1",
			"name": "Triangle T(n,k), read by rows, T(n,1) = mu(n), T(n,2) = T(n,k-1) - T(n-1,k), k \u003e 2, T(n,k) = Sum_{i=1..k-1} ( T(n-i,k-1) - T(n-i,k) ).",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A180009/b180009.txt\"\u003eRows n = 1..100 of triangle, flattened\u003c/a\u003e"
			],
			"example": [
				"Table begins:",
				"   1;",
				"  -1, -1;",
				"  -1,  0, -1;",
				"   0,  0,  0, -1;",
				"  -1, -1,  1,  0, -1;",
				"   1,  2, -2,  1,  0, -1;",
				"  -1, -3,  2, -1,  1,  0, -1;",
				"   0,  3, -1,  1, -1,  1,  0, -1;",
				"   0, -3, -1, -2,  2, -1,  1,  0, -1;",
				"   1,  4,  2,  2, -3,  2, -1,  1,  0, -1;",
				"  -1, -5,  0, -1,  1, -2,  2, -1,  1,  0, -1;",
				"   0,  5, -3,  2,  1,  0, -2,  2, -1,  1,  0, -1;",
				"  -1, -6,  3, -4,  0,  0,  1, -2,  2, -1,  1,  0, -1;",
				"   1,  7, -1,  3,  0,  2, -1,  1, -2,  2, -1,  1,  0, -1;",
				"   1, -6, -1, -2, -2, -3,  1,  0,  1, -2,  2, -1,  1,  0, -1;"
			],
			"maple": [
				"T:= proc(n,k) option remember;",
				"   if k\u003c1 or k\u003en then 0",
				"   elif k=1 then NumberTheory[Moebius](n)",
				"   elif k=2 then T(n,k-1) - T(n-1,k)",
				"   else add( T(n-j,k-1) - T(n-j,k), j=1..k-1 )",
				"   fi; end;",
				"seq(seq( T(n,k), k=1..n), n=1..15); # _G. C. Greubel_, Dec 17 2019"
			],
			"mathematica": [
				"T[n_, k_]:= T[n, k]= If[k\u003c1 || k\u003en, 0, If[k==1, MoebiusMu[n], If[k==2, T[n, k-1] - T[n-1, k], Sum[T[n-j, k-1] - T[n-j, k], {j, k-1}]]]]; Table[T[n, k], {n,15}, {k,n}]//Flatten (* _G. C. Greubel_, Dec 17 2019 *)"
			],
			"program": [
				"(PARI) T(n,k) = if(k\u003c1 || k\u003en, 0, if(k==1, moebius(n), if(k==2, T(n,k-1) - T(n-1,k), sum(j=1,k-1, T(n-j,k-1) - T(n-j,k)) ))); \\\\ _G. C. Greubel_, Dec 17 2019",
				"(MAGMA)",
				"function T(n,k)",
				"  if k lt 1 or k gt n then return 0;",
				"  elif k eq 1 then return MoebiusMu(n);",
				"  elif k eq 2 then return T(n,k-1) - T(n-1,k);",
				"  else return (\u0026+[T(n-j,k-1) - T(n-j,k): j in [1..k-1]]);",
				"  end if; return T; end function;",
				"[T(n,k): k in [1..n], n in [1..15]]; // _G. C. Greubel_, Dec 17 2019",
				"(Sage)",
				"@CachedFunction",
				"def T(n, k):",
				"    if (k\u003c1 or k\u003en): return 0",
				"    elif (k==1): return moebius(n)",
				"    elif (k==2): return T(n,k-1) - T(n-1,k)",
				"    else: return sum(T(n-j,k-1) - T(n-j, k) for j in (1..k-1))",
				"[[T(n, k) for k in (1..n)] for n in (1..15)] # _G. C. Greubel_, Dec 17 2019",
				"(GAP)",
				"T:= function(n,k)",
				"    if k\u003c1 or k\u003en then return 0;",
				"    elif k=1 then return MoebiusMu(n);",
				"    elif k=2 then return T(n,k-1) - T(n-1,k);",
				"    else return Sum([1..k-1], j-\u003e T(n-j,k-1) - T(n-j,k) );",
				"    fi; end;",
				"Flat(List([1..15], n-\u003e List([1..n], k-\u003e T(n,k) ))); # _G. C. Greubel_, Dec 17 2019"
			],
			"xref": [
				"Cf. A008683, A180010."
			],
			"keyword": "sign,tabl",
			"offset": "1,17",
			"author": "_Mats Granvik_, Aug 06 2010",
			"references": 2,
			"revision": 15,
			"time": "2019-12-30T12:10:17-05:00",
			"created": "2010-08-27T03:00:00-04:00"
		}
	]
}