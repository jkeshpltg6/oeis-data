{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A182431",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 182431,
			"data": "0,14,4,0,14,7,12,1,14,8,98,4,2,14,10,602,35,0,3,14,11,3540,218,0,4,4,14,12,20664,1285,2,21,4,5,14,13,120470,7504,14,122,14,8,6,14,14,702182,43751,84,711,74,35,12,7,14,15",
			"name": "Table, read by antidiagonals, in which the n-th row comprises A214206(n) 0 followed by a second-order recursive series G in which each product G(i)*G(i+1) lies in the same row of A001477 (interpreted as a square array).",
			"comment": [
				"This is a table related to the square array of nonnegative integers, A001477. Each row k contains the positive argument of the largest triangular number equal to or less than 14*k in column 0 and a corresponding 2nd-order recursive sequence G(k) beginning at position a(k,1). Each term G(i) is the same as a(k,i+1). If the product 14*k appears in row \"r\" of the square array A001477, then the product of adjacent terms G(i)*G(i+1), if greater than (r^2 + 3*r - 2)/2, is always in row \"r\" of table A001477. If the product is less than (r^2 +3*r -2)/2, then the product less r would be a triangular number, i.e., still lie in the same row assumed to contain all numbers n that equal a triangular number + r. For example, 3 is a triangular number and appears in row 0 of A001477, but if the rows could take negative indices, A001477(2,-1) would be a 3 so 3 can be said to also lie in row 2. See A182102 for a table of the arguments of the triangular numbers G(i)*G(i+1) - r.",
				"A property of this table is that a(k+1,i)-a(k,i) directly depends on the value of a(k+1,0)-a(k,0) in the same manner regardless of the value of k. For instance, if a(k+1,0) - a(k,0) = 1 then a(k+1,i+1) - a(k,i+1) equals A182435(i) for all i. Also, for i\u003e0, A143608(i) divides a(k+1,i+1)-a(k,i+1) for all k."
			],
			"formula": [
				"a(k,0) equals the largest m such that m*(m+1)/2 is less than or equal to 14*k.",
				"a(k,1) = 14; a(k,2) = k.",
				"For i \u003e 2, a(k,i) = 6*a(k,i-1) - a(k,i-2) + G_k where G_k is a constant equal to 28 + 2*k - 2 - 4*a(k,0)."
			],
			"example": [
				"The Table begins:",
				"0 14  0 12  98  602 3540 ...",
				"4 14  1  4  35  218 1285 ...",
				"7 14  2  0   0    2   14 ...",
				"8 14  3  4  21  122  711 ...",
				"10 14  4  4  14   74  424 ...",
				"11 14  5  8  35  194 1121 ...",
				"12 14  6 12  56  314 1818 ...",
				"13 14  7 16  77  434 2515 ...",
				"14 14  8 20  98  554 3212 ...",
				"15 14  9 24 119  674 3909 ...",
				"16 14 10 28 140  794 4606 ...",
				"17 14 11 32 161  914 5303 ...",
				"17 14 12 40 210 1202 6984 ...",
				"...",
				"Note that 14*0,0*12,12*98, 98*602 etc are each 0 more than a triangular number and are in row 0 of square array A001477; while 14*1, 1*4, 4*35, 35*218 etc are each 4 more than a triangular number and thus can be said to lie in row 4 of square array A001477."
			],
			"mathematica": [
				"highTri = Compile[{{S1,_Integer}}, Module[{xS0=0, xS1=S1}, While[xS1-xS0*(xS0+1)/2 \u003e xS0, xS0++]; xS0]];",
				"overTri = Compile[{{S2,_Integer}}, Module[{xS0=0, xS2=S2}, While[xS2-xS0*(xS0+1)/2 \u003e xS0, xS0++]; xS2 - (xS0*(1+xS0)/2)]];",
				"K1 = 0; m = 14; tab=Reap[While[K1\u003c16,J1=highTri[m*K1]; X = 2*(m+K1-(J1*2+1)); K2 = (6 K1 - m + X); K3 = 6 K2 - K1 + X; K4 = 6 K3 - K2 + X; K5 = 6 K4 -K3 + X; K6 = 6*K5 - K4 + X; K7 = 6*K6-K5+X; K8 = 6*K7-K6+X; Sow[J1,c]; Sow[m,d]; Sow[K1,e]; Sow[K2,f]; Sow[K3,g]; Sow[K4,h];",
				"  Sow[K5,i]; Sow[K6,j]; Sow[K7,k]; Sow[K8,l]; K1++]][[2]]; a=1; list5 = Reap[While[a\u003c11, b=a; While[b\u003e0, Sow[tab[[b,a+1-b]]]; b--]; a++]][[2,1]]; list5"
			],
			"xref": [
				"Cf. A182189, A182190, A182435."
			],
			"keyword": "nonn,tabl",
			"offset": "0,2",
			"author": "_Kenneth J Ramsey_, Apr 28 2012",
			"references": 7,
			"revision": 61,
			"time": "2016-12-10T22:36:38-05:00",
			"created": "2012-06-04T21:44:28-04:00"
		}
	]
}