{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A128638",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 128638,
			"data": "1,5,19,61,174,455,1112,2573,5689,12102,24900,49759,96902,184408,343722,628717,1130418,2000669,3489788,6005910,10207688,17147892,28494120,46865519,76342903,123236446,197233723,313106264,493231830,771301986,1197743552,1847606573",
			"name": "Expansion of q * (psi(q^3)^3 / psi(q)) / (phi(-q)^3 / phi(-q^3)) in powers of q where phi(), psi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Cubic AGM theta functions: a(q) (see A004016), b(q) (A005928), c(q) (A005882)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A128638/b128638.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Kevin Acres, David Broadhurst, \u003ca href=\"https://arxiv.org/abs/1810.07478\"\u003eEta quotients and Rademacher sums\u003c/a\u003e, arXiv:1810.07478 [math.NT], 2018. See Table 1 p. 10.",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of (1/3) * (c(q^2)^2 / c(q)) / (b(q)^2 / b(q^2)) in powers of q where b(), c() are cubic AGM theta functions.",
				"Expansion of (eta(q^6) / eta(q))^5 * eta(q^2) / eta(q^3) in powers of q.",
				"Euler transform of period 6 sequence [ 5, 4, 6, 4, 5, 0, ...].",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2)) where f(u, v) = v * (1 + 8*u) * (1 + 9*v) - (u-v)^2.",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (6 t)) = (1/72) g(t) where q = exp(2 Pi i t) and g() is the g.f. for A128632.",
				"G.f.: x * Product_{k\u003e0} ((1 - x^(6*k)) / (1-x^k))^5 * ((1 - x^(2*k)) / (1 - x^(3*k))).",
				"8 * a(n) = A128639(n) unless n = 0. Convolution inverse of A128632.",
				"a(n) ~ exp(2*Pi*sqrt(2*n/3)) / (72 * 2^(3/4) * 3^(1/4) * n^(3/4)). - _Vaclav Kotesovec_, Sep 08 2015"
			],
			"example": [
				"G.f. = q + 5*q^2 + 19*q^3 + 61*q^4 + 174*q^5 + 455*q^6 + 1112*q^7 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (QPochhammer[ q^6] / QPochhammer[ q])^5 (QPochhammer[ q^2] / QPochhammer[ q^3]), {q, 0, n}]; (* _Michael Somos_, Jun 08 2015 *)",
				"nmax = 40; Rest[CoefficientList[Series[x * Product[((1 - x^(6*k)) / (1-x^k))^5 * ((1 - x^(2*k)) / (1 - x^(3*k))), {k, 1, nmax}], {x, 0, nmax}], x]] (* _Vaclav Kotesovec_, Sep 08 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c1, 0, n--; A = x * O(x^n); polcoeff( (eta(x^6 + A) / eta(x + A))^5 * eta(x^2 + A) / eta(x^3 + A), n))};"
			],
			"xref": [
				"Cf. A128632, A128639."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Michael Somos_, Mar 16 2007",
			"ext": [
				"Edited by _N. J. A. Sloane_, Apr 01 2008"
			],
			"references": 8,
			"revision": 32,
			"time": "2021-03-12T22:24:44-05:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}