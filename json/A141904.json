{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A141904",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 141904,
			"data": "1,-1,1,1,-2,1,-1,23,-1,1,1,-44,14,-4,1,-1,563,-818,22,-5,1,1,-3254,141,-1436,19,-2,1,-1,88069,-13063,21757,-457,43,-7,1,1,-11384,16774564,-11368,7474,-680,56,-8,1,-1,1593269,-1057052,35874836,-261502,3982,-688,212,-3,1,1,-15518938,4651811",
			"name": "Triangle of the numerators of coefficients c(n,k) = [x^k] P(n,x) of some polynomials P(n,x).",
			"comment": [
				"Let the polynomials P be defined by P(0,x)=u(0), P(n,x)= u(n) + x*sum_{i=0..n-1} u(i)*P(n-i-1,x) and coefficients u(i)=(-1)^i/(2i+1). These u are reminiscent of the Leibniz' Taylor expansion to calculate arctan(1) =pi/4 = A003881. Then P(n,x) = sum_{k=0..n} c(n,k)*x^k."
			],
			"reference": [
				"P. Curtz, Gazette des Mathematiciens, 1992, no. 52, p.44.",
				"P. Flajolet, X. Gourdon, B. Salvy, Gazette des Mathematiciens, 1993, no. 55, pp.67-78."
			],
			"link": [
				"Jean-François Alcover, \u003ca href=\"/A141904/a141904.pdf\"\u003eRoots of P(120,x) in the shape of a cardioid.\u003c/a\u003e"
			],
			"example": [
				"The polynomials P(n,x) are for n=0 to 5:",
				"1 = P(0,x).",
				"-1/3+x = P(1,x).",
				"1/5-2/3*x+x^2 = P(2,x).",
				"-1/7+23/45*x-x^2+x^3 = P(3,x).",
				"1/9-44/105*x+14/15*x^2-4/3*x^3+x^4 = P(4,x).",
				"-1/11+563/1575*x-818/945*x^2+22/15*x^3-5/3*x^4+x^5 = P(5,x)."
			],
			"maple": [
				"u := proc(i) (-1)^i/(2*i+1) ; end:",
				"P := proc(n,x) option remember ; if n =0 then u(0); else u(n)+x*add( u(i)*procname(n-1-i,x),i=0..n-1) ; expand(%) ; fi; end:",
				"A141904 := proc(n,k) p := P(n,x) ; numer(coeftayl(p,x=0,k)) ; end: seq(seq(A141904(n,k),k=0..n),n=0..13) ; # _R. J. Mathar_, Aug 24 2009"
			],
			"mathematica": [
				"ClearAll[u, p]; u[n_] := (-1)^n/(2*n + 1); p[0][x_] := u[0]; p[n_][x_] := p[n][x] = u[n] + x*Sum[u[i]*p[n - i - 1][x] , {i, 0, n-1}] // Expand; row[n_] := CoefficientList[ p[n][x], x]; Table[row[n], {n, 0, 10}] // Flatten // Numerator (* _Jean-François Alcover_, Oct 02 2012 *)"
			],
			"xref": [
				"Cf. A142048 (denominators), A140749, A141412 (where u=(-1)^i/(i+1))."
			],
			"keyword": "sign,frac,tabl",
			"offset": "0,5",
			"author": "_Paul Curtz_, Sep 14 2008",
			"ext": [
				"Edited and extended by _R. J. Mathar_, Aug 24 2009"
			],
			"references": 4,
			"revision": 15,
			"time": "2015-10-04T01:44:50-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}