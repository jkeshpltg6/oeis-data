{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A309280",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 309280,
			"data": "1,6,1,1,24,6,4,1,1,1,80,20,9,4,4,2,2,1,1,1,240,60,30,14,12,7,5,3,3,3,2,2,1,1,1,672,168,84,42,29,20,15,10,9,7,5,5,4,4,4,3,2,2,1,1,1,1792,448,202,112,71,49,40,27,23,17,15,12,10,10,8,8,7,7,6,5,5,4,3,2,2,1,1,1",
			"name": "T(n,k) is (1/k) times the sum of the elements of all subsets of [n] whose sum is divisible by k; triangle T(n,k), n \u003e= 1, 1 \u003c= k \u003c= n*(n+1)/2, read by rows.",
			"comment": [
				"T(n,k) is defined for all n \u003e= 0, k \u003e= 1.  The triangle contains only the positive terms.  T(n,k) = 0 if k \u003e n*(n+1)/2.",
				"The sequence of column k satisfies a linear recurrence with constant coefficients of order 3*A000593(k)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A309280/b309280.txt\"\u003eRows n = 1..50, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n+1,n*(n+1)/2+1) = A000009(n) for n \u003e= 0."
			],
			"example": [
				"The subsets of [4] whose sum is divisible by 3 are: {}, {3}, {1,2}, {2,4}, {1,2,3}, {2,3,4}.  The sum of their elements is 0 + 3 + 3 + 6 + 6 + 9 = 27.  So T(4,3) = 27/3 = 9.",
				"Triangle T(n,k) begins:",
				"    1;",
				"    6,  1,  1;",
				"   24,  6,  4,  1,  1, 1;",
				"   80, 20,  9,  4,  4, 2, 2, 1, 1, 1;",
				"  240, 60, 30, 14, 12, 7, 5, 3, 3, 3, 2, 2, 1, 1, 1;",
				"  ..."
			],
			"maple": [
				"b:= proc(n, m, s) option remember; `if`(n=0, [`if`(s=0, 1, 0), 0],",
				"      b(n-1, m, s) +(g-\u003e g+[0, g[1]*n])(b(n-1, m, irem(s+n, m))))",
				"    end:",
				"T:= (n, k)-\u003e b(n, k, 0)[2]/k:",
				"seq(seq(T(n, k), k=1..n*(n+1)/2), n=1..10);",
				"# second Maple program:",
				"b:= proc(n, s) option remember; `if`(n=0, add(s/d *x^d,",
				"      d=numtheory[divisors](s)), b(n-1, s)+b(n-1, s+n))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=1..degree(p)))(b(n, 0)):",
				"seq(T(n), n=1..10);"
			],
			"mathematica": [
				"b[n_, m_, s_] := b[n, m, s] = If[n == 0, {If[s == 0, 1, 0], 0}, b[n-1, m, s] + Function[g, g + {0, g[[1]] n}][b[n-1, m, Mod[s+n, m]]]];",
				"T[n_, k_] := b[n, k, 0][[2]]/k;",
				"Table[T[n, k], {n, 1, 10}, {k, 1, n(n+1)/2}] // Flatten (* _Jean-François Alcover_, Oct 04 2019, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Columns k=1..10 give: A001788, A309294, A309295, A309296, A309297, A309298, A309299, A309300, A309301, A309302.",
				"Row sums give A309281.",
				"Row lengths give A000217.",
				"T(n,n) gives A309128.",
				"Rows reversed converge to A000009.",
				"Cf. A000593, A309402."
			],
			"keyword": "nonn,look,tabf",
			"offset": "1,2",
			"author": "_Alois P. Heinz_, Jul 20 2019",
			"references": 13,
			"revision": 32,
			"time": "2019-10-04T13:10:15-04:00",
			"created": "2019-07-21T12:28:18-04:00"
		}
	]
}