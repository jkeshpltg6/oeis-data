{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A073089",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 73089,
			"data": "0,0,1,1,1,0,0,1,1,0,1,1,0,0,0,1,1,0,1,1,1,0,0,1,0,0,1,1,0,0,0,1,1,0,1,1,1,0,0,1,1,0,1,1,0,0,0,1,0,0,1,1,1,0,0,1,0,0,1,1,0,0,0,1,1,0,1,1,1,0,0,1,1,0,1,1,0,0,0,1,1,0,1,1,1,0,0,1,0,0,1,1,0,0,0,1,0,0,1,1,1,0,0,1,1",
			"name": "a(n) = (1/2)*(4n - 3 - Sum_{k=1..n} A007400(k)).",
			"comment": [
				"From _Joerg Arndt_, Oct 28 2013: (Start)",
				"Sequence is (essentially) obtained by complementing every other term of A014577.",
				"Turns (by 90 degrees) of a curve similar to the Heighway dragon which can be rendered as follows: [Init] Set n=0 and direction=0. [Draw] Draw a unit line (in the current direction). Turn left/right if a(n) is zero/nonzero respectively. [Next] Set n=n+1 and goto (draw).",
				"See the linked pdf files for two renderings of the curve. (End)"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A073089/b073089.txt\"\u003eTable of n, a(n) for n = 1..65537\u003c/a\u003e",
				"Joerg Arndt, pdf rendering of the \u003ca href=\"/A073089/a073089.pdf\"\u003ecurve described in comment\u003c/a\u003e, \u003ca href=\"/A073089/a073089_1.pdf\"\u003ealternate rendering of the curve\u003c/a\u003e.",
				"\u003ca href=\"/index/Ch#char_fns\"\u003eIndex entries for characteristic functions\u003c/a\u003e"
			],
			"formula": [
				"Recurrence: a(1) = a(4n+2) = a(8n+7) = a(16n+13) = 0, a(4n) = a(8n+3) = a(16n+5) = 1, a(8n+1) = a(4n+1).",
				"G.f.: The following series has a simple continued fraction expansion:",
				"x + Sum_{n\u003e=1} 1/x^(2^n-1) = [x; x, -x, -x, -x, x, ..., (-1)^a(n)*x, ...]. - _Paul D. Hanna_, Oct 19 2012",
				"a(n) = A014577(n-2) + A056594(n).  Conjecture: a(n) = (1 + (-1)^A057661(n - 1))/2 for all n \u003e 1. - _Velin Yanev_, Feb 01 2021"
			],
			"example": [
				"From _Paul D. Hanna_, Oct 19 2012: (Start)",
				"Let F(x) = x + 1/x + 1/x^3 + 1/x^7 + 1/x^15 + 1/x^31 +...+ 1/x^(2^n-1) +...",
				"then F(x) = x + 1/(x + 1/(-x + 1/(-x + 1/(-x + 1/(x + 1/(x + 1/(-x + 1/(-x + 1/(x + 1/(-x + 1/(-x + 1/(x + 1/(x + 1/(x + 1/(-x + 1/(-x + 1/(x + 1/(-x + 1/(-x + 1/(-x + 1/(x +...+ 1/((-1)^a(n)*x +...)))))))))))))))))))))),",
				"a continued fraction in which the partial quotients equal (-1)^a(n)*x.  (End)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c2,0,if(n%8==1,a((n+1)/2),[1,-1,0,1,1,1,0,0,1,-1,0,1,1,0,0,0][(n%16)+1])) \\\\ _Ralf Stephan_",
				"(PARI) /* Using the Continued Fraction, Print 2^N terms of this sequence: */",
				"{N=10;CF=contfrac(x+sum(n=1,N,1/x^(2^n-1)),2^N);for(n=1,2^N,print1((1-CF[n]/x)/2,\", \"))} \\\\ _Paul D. Hanna_, Oct 19 2012",
				"(PARI) a(n) = { if ( n\u003c=1, return(0)); n-=1; my(v=2^valuation(n,2) ); return( (0==bitand(n, v\u003c\u003c1)) != (v%2) ); } \\\\ _Joerg Arndt_, Oct 28 2013"
			],
			"xref": [
				"Cf. A007400, A073088 (the sum part here), A123725."
			],
			"keyword": "easy,nonn",
			"offset": "1,1",
			"author": "_Benoit Cloitre_, Aug 18 2002",
			"references": 5,
			"revision": 47,
			"time": "2021-03-13T00:11:47-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}