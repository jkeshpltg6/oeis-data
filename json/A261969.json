{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A261969",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 261969,
			"data": "1,2,3,2,5,6,7,2,3,10,11,2,13,14,15,2,17,3,19,2,21,22,23,2,5,26,3,2,29,30,31,2,33,34,35,6,37,38,39,2,41,42,43,2,3,46,47,2,7,5,51,2,53,3,55,2,57,58,59,2,61,62,3,2,65,66,67,2,69,70,71,2,73,74,5,2,77,78,79,2,3,82,83,2,85,86,87,2,89,3,91",
			"name": "Product of primes dividing n with maximum multiplicity.",
			"comment": [
				"a(1) = 1 by convention.",
				"If n is prime then a(n) = n; e.g., a(2) = 2, a(3) = 3, a(5) = 5, etc. Also if n is nonsquare semiprime then a(n) = n; e.g., a(6) = 6, a(10) = 10, a(14) = 14, a(15) = 15, etc. - _Zak Seidov_, Sep 07 2015",
				"a(n)= n precisely when n is squarefree. - _Franklin T. Adams-Watters_, Feb 16 2019"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A261969/b261969.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"18 = 2^1 * 3^2. 2 is the maximum exponent, 3 is the only prime with that exponent, so a(18) = 3.",
				"36 = 2^2 * 3^2, maximum exponent 2 for both 2 and 3, so a(36) = 2*3 = 6."
			],
			"maple": [
				"a:= n-\u003e (l-\u003e(m-\u003emul(`if`(m=j[2], j[1], 1), j=l))(",
				"         max(seq(i[2], i=l))))(ifactors(n)[2]):",
				"seq(a(n), n=1..100);  # _Alois P. Heinz_, Sep 07 2015"
			],
			"mathematica": [
				"f[n_] := Block[{pf = FactorInteger@ n}, Times @@ Take[First /@ pf, Flatten@ Position[Last /@ pf, Max[Last /@ pf]]]]; f /@ Range@ 91 (* _Michael De Vlieger_, Sep 07 2015 *)"
			],
			"program": [
				"(PARI) a(n) = my(fm=factor(n),m); if(n\u003c2,return(n)); m=vecmax(fm[,2]~); prod(k=1,#fm[,2]~,if(fm[k,2]==m,fm[k,1],1))",
				"(PARI) a(n) = {my(f = factor(n)); if (n\u003e1, m = vecmax(f[,2])); for (i=1, #f~, f[i,2] = (f[i,2]==m)); factorback(f);} \\\\ _Michel Marcus_, Sep 08 2015",
				"(Haskell)",
				"a261969 n = product $ map fst $ filter ((== emax) . snd) $ zip ps es",
				"    where emax = maximum es",
				"          ps = a027748_row n; es = a124010_row n",
				"-- _Reinhard Zumkeller_, Sep 08 2015"
			],
			"xref": [
				"Cf. A007947.",
				"Cf. A000040, A001358. - _Zak Seidov_, Sep 07 2015",
				"Cf. A027748, A124010, A051903."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Franklin T. Adams-Watters_, Sep 07 2015",
			"references": 3,
			"revision": 28,
			"time": "2019-02-16T09:22:39-05:00",
			"created": "2015-09-07T11:23:42-04:00"
		}
	]
}