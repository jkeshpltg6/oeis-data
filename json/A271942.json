{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A271942",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 271942,
			"data": "1,1,1,1,3,1,1,5,6,1,1,7,16,10,1,1,9,31,40,15,1,1,11,51,105,85,21,1,1,13,76,219,295,161,28,1,1,15,106,396,771,721,280,36,1,1,17,141,650,1681,2331,1582,456,45,1,1,19,181,995,3235,6083,6244,3186,705,55,1,1,21,226,1445,5685,13663,19348,15156,5985,1045,66,1,1,23,276,2014,9325,27483,50464,55308,33903,10615,1496,78,1",
			"name": "Triangle read by rows: T(n,k) is the number of bargraphs of semiperimeter n having width k (n\u003e=2, k\u003e=1).",
			"comment": [
				"Sum of entries in row n = A082582(n).",
				"Sum(k*T(n,k), k\u003e=1) = A271943(n).",
				"Connection with A145904 should be explored."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A271942/b271942.txt\"\u003eRows n = 2..150, flattened\u003c/a\u003e",
				"A. Blecher, C. Brennan, and A. Knopfmacher, \u003ca href=\"http://dx.doi.org/10.1080/0035919X.2015.1059905\"\u003ePeaks in bargraphs\u003c/a\u003e, Trans. Royal Soc. South Africa, 71, No. 1, 2016, 97-103.",
				"M. Bousquet-Mélou and A. Rechnitzer, \u003ca href=\"http://dx.doi.org/10.1016/S0196-8858(02)00553-5\"\u003eThe site-perimeter of bargraphs\u003c/a\u003e, Adv. in Appl. Math. 31 (2003), 86-112."
			],
			"formula": [
				"G.f.: G(x,z) satisfies xzG^2-(1-xz-z-xz^2)G+xz^2=0 (z marks semiperimeter, x marks width).",
				"T(n, k) = hypergeom([-k, k + 3, k - n], [1, 2], 1), provided one bases the offset in (0, 0). - _Peter Luschny_, Oct 18 2020"
			],
			"example": [
				"Row 4 is 1,3,1 because the 5 (=A082582(4)) bargraphs of semiperimeter 4 correspond to the compositions [1,1,1], [1,2], [2,1], [2,2], [3] which, clearly, have widths 3,2,2,2,1.",
				"Triangle starts:",
				"                               1",
				"                              1, 1",
				"                            1, 3, 1",
				"                           1, 5, 6, 1",
				"                        1, 7, 16, 10, 1",
				"                      1, 9, 31, 40, 15, 1",
				"                   1, 11, 51, 105, 85, 21, 1",
				"                1, 13, 76, 219, 295, 161, 28, 1",
				"             1, 15, 106, 396, 771, 721, 280, 36, 1",
				"         1, 17, 141, 650, 1681, 2331, 1582, 456, 45, 1"
			],
			"maple": [
				"eq := x*z*G^2-(1-x*z-z-x*z^2)*G+x*z^2 = 0: G := RootOf(eq, G): Gser := simplify(series(G, z = 0, 23)): for n from 2 to 20 do P[n] := sort(expand(coeff(Gser, z, n))) end do: for n from 2 to 20 do seq(coeff(P[n], x, j), j = 1 .. degree(P[n])) end do; # yields sequence in triangular form",
				"# second Maple program:",
				"b:= proc(n, y, t) option remember; expand(`if`(n=0, (1-t),",
				"      `if`(t\u003c0, 0, b(n-1, y+1, 1))+`if`(t\u003e0 or y\u003c2, 0,",
				"       b(n, y-1, -1))+`if`(y\u003c1, 0, b(n-1, y, 0)*z)))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, z, i), i=1..n-1))(b(n, 0$2)):",
				"seq(T(n), n=2..20);  # _Alois P. Heinz_, Jun 06 2016",
				"# Alternative, (assuming offset (0,0)):",
				"T := (n, k) -\u003e simplify(hypergeom([-k, k + 3, k - n], [1, 2], 1)):",
				"seq(seq(T(n, k), k=0..n), n=0..9); # _Peter Luschny_, Oct 18 2020"
			],
			"mathematica": [
				"b[n_, y_, t_] := b[n, y, t] = Expand[If[n == 0, {1 - t}, If[t \u003c 0, 0, b[n - 1, y + 1, 1]] + If[t \u003e 0 || y \u003c 2, 0, b[n, y - 1, -1]] + If[y \u003c 1, 0, b[n - 1, y, 0]*z]]];",
				"T[n_] := Function[p, Table[Coefficient[p, z, i], {i, 1, n-1}]][b[n, 0, 0] ];",
				"Table[T[n], {n, 2, 20}] // Flatten (* _Jean-François Alcover_, Jul 21 2016, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A082582, A145904, A271943."
			],
			"keyword": "nonn,tabl",
			"offset": "2,5",
			"author": "_Emeric Deutsch_, May 21 2016",
			"references": 3,
			"revision": 21,
			"time": "2020-10-18T11:31:20-04:00",
			"created": "2016-05-21T12:13:55-04:00"
		}
	]
}