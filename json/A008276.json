{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008276",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8276,
			"data": "1,1,-1,1,-3,2,1,-6,11,-6,1,-10,35,-50,24,1,-15,85,-225,274,-120,1,-21,175,-735,1624,-1764,720,1,-28,322,-1960,6769,-13132,13068,-5040,1,-36,546,-4536,22449,-67284,118124,-109584,40320,1,-45",
			"name": "Triangle of Stirling numbers of first kind, s(n, n-k+1), n \u003e= 1, 1 \u003c= k \u003c= n. Also triangle T(n,k) giving coefficients in expansion of n!*binomial(x,n)/x in powers of x.",
			"comment": [
				"n-th row of the triangle = charpoly of an (n-1) X (n-1) matrix with (1,2,3,...) in the diagonal and the rest zeros. - _Gary W. Adamson_, Mar 19 2009",
				"From _Daniel Forgues_, Jan 16 2016: (Start)",
				"For n \u003e= 1, the row sums [of either signed or absolute values] are",
				"  Sum_{k=1..n} T(n,k) = 0^(n-1),",
				"  Sum_{k=1..n} |T(n,k)| = T(n+1,1) = n!. (End)",
				"The moment generating function of the probability density function p(x, m=q, n=1, mu=q) = q^q*x^(q-1)*E(x, q, 1)/(q-1)!, with q \u003e= 1, is M(a, m=q, n=1, mu=q) = Sum_{k=0..q}(A000312(q) / A000142(q-1)) * A008276(q, k) * polylog(k, a) / a^q , see A163931 and A274181. - _Johannes W. Meijer_, Jun 17 2016",
				"Triangle of coefficients of the polynomial x(x-1)(x-2)...(x-n+1), also denoted as falling factorial (x)_n, expanded into decreasing powers of x. - _Ralf Stephan_, Dec 11 2016"
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), p. 833.",
				"F. N. David, M. G. Kendall and D. E. Barton, Symmetric Function and Allied Tables, Cambridge, 1966, p. 226.",
				"R. L. Graham, D. E. Knuth and O. Patashnik, Concrete Mathematics, 2nd ed. (Addison-Wesley, 1994), p. 257."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A008276/b008276.txt\"\u003eRows n=0..100 of triangle, flattened\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"T. Copeland, \u003ca href=\"http://tcjpn.wordpress.com/2015/12/21/generators-inversion-and-matrix-binomial-and-integral-transforms/\"\u003eGenerators, Inversion, and Matrix, Binomial, and Integral Transforms\u003c/a\u003e",
				"Bill Gosper, \u003ca href=\"/A008275/a008275.png\"\u003eColored illustrations of triangle of Stirling numbers of first kind read mod 2, 3, 4, 5, 6, 7\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/StirlingNumberoftheFirstKind.html\"\u003eStirling Number of the First Kind\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Stirling_numbers_and_exponential_generating_functions\"\u003eStirling numbers and exponential generating functions \u003c/a\u003e"
			],
			"formula": [
				"n!*binomial(x, n) = Sum_{k=1..n-1} T(n, k)*x^(n-k).",
				"|A008276(n, k)| = T(n-1, k-1) where T(n, k) is the triangle, read by rows, given by [1, 0, 1, 0, 1, 0, 1, 0, 1, ...] DELTA [1, 1, 2, 2, 3, 3, 4, 4, 5, 5, ...]; A008276(n, k) = T(n-1, k-1) where T(n, k) is the triangle, read by rows, given by [1, 0, 1, 0, 1, 0, 1, 0, 1, ...] DELTA [ -1, -1, -2, -2, -3, -3, -4, -4, -5, -5, ...]. Here DELTA is the operator defined in A084938. - _Philippe Deléham_, Dec 30 2003",
				"|T(n, k)| = Sum_{m=0..n} A008517(k, m+1)*binomial(n+m, 2*(k-1)), n \u003e= k \u003e= 1. A008517 is the second-order Eulerian triangle. See the Graham et al. reference p. 257, eq. (6.44).",
				"A094638 formula for unsigned T(n, k).",
				"|T(n, k)| = Sum_{m=0..min(k-1, n-k)} A112486(k-1, m)*binomial(n-1, k-1+m) if n \u003e= k \u003e= 1, else 0. - _Wolfdieter Lang_, Sep 12 2005, see A112486.",
				"|T(n, k)| = (f(n-1, k-1)/(2*(k-1))!)* Sum_{m=0..min(k-1, n-k)} A112486(k-1, m)*f(2*(k-1), k-1-m)*f(n-k, m) if n \u003e= k \u003e= 1, else 0, where f(n, k) stands for the falling factorial n*(n-1)*...*(n-(k-1)) and f(n, 0):=1. - _Wolfdieter Lang_, Sep 12 2005, see A112486.",
				"With P(n,t) = Sum_{k=0..n-1} T(n,k+1) * t^k = (1-t)*(1-2*t)*...*(1-(n-1)t) and P(0,t) = 1, exp(P(.,t)*x) = (1+t*x)^(1/t) . Compare A094638. T(n,k+1) = (1/k!) (D_t)^k (D_x)^n ( (1+t*x)^(1/t) - 1 ) evaluated at t=x=0 . - _Tom Copeland_, Dec 09 2007",
				"Product_{i=1..n} (x-i) = Sum_{k=0..n} T(n,k)*x^k. - _Reinhard Zumkeller_, Dec 29 2007",
				"E.g.f.: Sum_{n\u003e=0} (Sum_{k=0..n} T(n,n-k)*t^k)/n!) = Sum_{n\u003e=0} (x)_n * t^k/n! = exp(x * log(1+t)), with (x)_n the n-th falling factorial polynomial. - _Ralf Stephan_, Dec 11 2016",
				"Sum_{j=0..m} T(m, m-j)*s2(j+k+1, m) =  m^k, where s2(j, m) are Stirling numbers of the second kind. - _Tony Foster III_, Jul 25 2019",
				"For n\u003e=2, Sum_{k=1..n} k*T(n,k) = (-1)^(n-1)*(n-2)!. - _Zizheng Fang_, Dec 27 2020"
			],
			"example": [
				"3!*binomial(x,3) = x*(x-1)*(x-2) = x^3 - 3*x^2 + 2*x.",
				"Triangle begins",
				"  1;",
				"  1,  -1;",
				"  1,  -3,   2;",
				"  1,  -6,  11,   -6;",
				"  1, -10,  35,  -50,  24;",
				"  1, -15,  85, -225, 274, -120;",
				"..."
			],
			"maple": [
				"seq(seq(coeff(expand(n!*binomial(x,n)),x,j),j=n..1,-1),n=1..15); # _Robert Israel_, Jan 24 2016",
				"A008276 := proc(n, k): combinat[stirling1](n, n-k+1) end: seq(seq(A008276(n, k), k=1..n), n=1..9); # _Johannes W. Meijer_, Jun 17 2016"
			],
			"mathematica": [
				"len = 47; m = Ceiling[Sqrt[2*len]]; t[n_, k_] = StirlingS1[n, n-k+1]; Flatten[Table[t[n, k], {n, 1, m}, {k, 1, n}]][[1 ;; len]] (* _Jean-François Alcover_, May 31 2011 *)"
			],
			"program": [
				"(PARI) T(n,k)=if(n\u003c1,0,n!*polcoeff(binomial(x,n),n-k+1))",
				"(PARI) T(n,k)=if(n\u003c1,0,n!*polcoeff(polcoeff(y*(1+y*x+x*O(x^n))^(1/y),n),k))",
				"(Haskell)",
				"a008276 n k = a008276_tabl !! (n-1) !! (k-1)",
				"a008276_row n = a008276_tabl !! (n-1)",
				"a008276_tabl = map init $ tail a054654_tabl",
				"-- _Reinhard Zumkeller_, Mar 18 2014",
				"(Sage) def T(n,k): return falling_factorial(x,n).expand().coefficient(x,n-k+1) # _Ralf Stephan_, Dec 11 2016"
			],
			"xref": [
				"See A008275 and A048994, which are the main entries for this triangle of numbers.",
				"See A008277 triangle of Stirling numbers of the second kind, S2(n,k).",
				"Cf. A054654, A054655, A084938, A145324, A094216, A003422, A000166, A000110, A000204, A000045, A000108."
			],
			"keyword": "sign,tabl,nice",
			"offset": "1,5",
			"author": "_N. J. A. Sloane_",
			"references": 35,
			"revision": 93,
			"time": "2020-12-29T07:06:51-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}