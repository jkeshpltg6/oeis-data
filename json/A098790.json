{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A098790",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 98790,
			"data": "1,2,6,15,37,90,218,527,1273,3074,7422,17919,43261,104442,252146,608735,1469617,3547970,8565558,20679087,49923733,120526554,290976842,702480239,1695937321,4094354882,9884647086,23863649055,57611945197",
			"name": "a(n) = 2*a(n-1) + a(n-2) + 1, a(0) = 1, a(1) = 2.",
			"comment": [
				"Previous name was: a(n) = A048739(n) - A000129(n).",
				"Partial sums of Pell numbers A000129 except omit next-to-last Pell number. E.g., 37 = 0+1+2+5+12+29 - 12."
			],
			"reference": [
				"M. Bicknell-Johnson and G. E. Bergum, The Generalized Fibonacci Numbers {C(n)}, C(n)=C(n-1)+C(n-2)+K, Applications of Fibonacci Numbers, 1986, pp. 193-205."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A098790/b098790.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"M. Bicknell, \u003ca href=\"http://www.fq.math.ca/Scanned/13-4/bicknell.pdf\"\u003eA Primer on the Pell Sequence and related sequences\u003c/a\u003e, Fibonacci Quarterly, Vol. 13, No. 4, 1975, pp. 345-349.",
				"A. F. Horadam, \u003ca href=\"http://www.fq.math.ca/Scanned/5-5/horadam.pdf\"\u003eSpecial properties of the sequence W_n(a,b; p,q)\u003c/a\u003e, Fib. Quart., 5.5 (1967), 424-434.",
				"Hermann Stamm-Wilbrandt, \u003ca href=\"/A098790/a098790.svg\"\u003e4 interlaced bisections\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3, -1, -1)."
			],
			"formula": [
				"a(n) = 2*a(n-1) + a(n-2) + 1, a(0) = 1, a(1) = 2.",
				"G.f.: (x^2-x+1)/((1-x)(1-2x-x^2)).",
				"a(n+1) = - A024537(n+1) + 2*A048739(n+1) - 2*A048739(n).",
				"a(n) = - A024537(n) + A052542(n+1).",
				"Partial sums of A074323. - _Paul Barry_, Mar 11 2007",
				"a(n) = (sqrt(2)+1)^n*(3/4+sqrt(2)/4)+(sqrt(2)-1)^n*(3/4-sqrt(2)/4)*(-1)^n-1/2; - _Paul Barry_, Mar 11 2007",
				"a(0)=1, a(1)=2, a(2)=6, a(n)=3*a(n-1)-a(n-2)-a(n-3). [_Harvey P. Dale_, Oct 15 2011]",
				"a(2*n) = A124124(2*n+1). - _Hermann Stamm-Wilbrandt_, Aug 03 2014",
				"a(2*n+1) = A006451(2*n+1). - _Hermann Stamm-Wilbrandt_, Aug 26 2014",
				"a(n) = 7*a(n-2) - 7*a(n-4) + a(n-6), for n\u003e5. - _Hermann Stamm-Wilbrandt_, Aug 26 2014"
			],
			"mathematica": [
				"a[0] = 1; a[1] = 2; a[n_] := a[n] = 2a[n - 1] + a[n - 2] + 1; Table[ a[n], {n, 0, 28}] (* _Robert G. Wilson v_, Nov 04 2004 *)",
				"LinearRecurrence[{3,-1,-1},{1,2,6},31] (* _Harvey P. Dale_, Oct 15 2011 *)",
				"CoefficientList[Series[(x^2 - x + 1)/((1 - x) (1 - 2 x - x^2)), {x, 0, 40}], x] (* _Vincenzo Librandi_, Aug 14 2014 *)"
			],
			"xref": [
				"Cf. A006451, A000129, A048739, A124124, A024537, A074323, A052542."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Creighton Dement_, Oct 30 2004",
			"ext": [
				"More terms from _Robert G. Wilson v_, Nov 04 2004",
				"Definition edited by _N. J. A. Sloane_, Aug 03 2014",
				"New name from existing formula by _Joerg Arndt_, Aug 13 2014"
			],
			"references": 9,
			"revision": 56,
			"time": "2018-06-09T13:39:58-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}