{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002874",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2874,
			"id": "M1863 N0738",
			"data": "1,2,8,42,268,1994,16852,158778,1644732,18532810,225256740,2933174842,40687193548,598352302474,9290859275060,151779798262202,2600663778494172,46609915810749130,871645673599372868,16971639450858467002,343382806080459389676",
			"name": "The number of partitions of {1..3n} that are invariant under a permutation consisting of n 3-cycles.",
			"comment": [
				"Original name: Sorting numbers."
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A002874/b002874.txt\"\u003eTable of n, a(n) for n = 0..484\u003c/a\u003e (first 101 terms from T. D. Noe)",
				"Victor Meally, \u003ca href=\"/A002868/a002868.pdf\"\u003eComparison of several sequences given in Motzkin's paper \"Sorting numbers for cylinders...\", letter to N. J. A. Sloane, N. D.\u003c/a\u003e",
				"T. S. Motzkin, \u003ca href=\"/A000262/a000262.pdf\"\u003eSorting numbers for cylinders and other classification numbers\u003c/a\u003e, in Combinatorics, Proc. Symp. Pure Math. 19, AMS, 1971, pp. 167-176. [Annotated, scanned copy]",
				"J. Pasukonis, S. Ramgoolam, \u003ca href=\"https://arxiv.org/abs/1010.1683\"\u003eFrom counting to construction for BPS states in N=4SYM\u003c/a\u003e, arXiv:1010.1683 [hep-th], 2010, (E.3).",
				"J. Pasukonis, S. Ramgoolam, \u003ca href=\"https://doi.org/10.1007/JHEP02(2011)078\"\u003eFrom counting to construction for BPS states in N=4SYM\u003c/a\u003e, J. High En. Phys. 2011 (2) (2011), (E.3).",
				"OEIS Wiki, \u003ca href=\"http://oeis.org/wiki/Sorting_numbers\"\u003eSorting numbers\u003c/a\u003e",
				"\u003ca href=\"/index/So#sorting\"\u003eIndex entries for sequences related to sorting\u003c/a\u003e"
			],
			"formula": [
				"E.g.f.: exp( (exp(3*x) - 4)/3 + exp(x) )."
			],
			"maple": [
				"S:= series(exp( (exp(3*x) - 4)/3 + exp(x)), x, 31):",
				"seq(coeff(S,x,j)*j!, j=0..30); # _Robert Israel_, Oct 30 2015",
				"# second Maple program:",
				"a:= proc(n) option remember; `if`(n=0, 1, add((1+",
				"      3^(j-1))*binomial(n-1, j-1)*a(n-j), j=1..n))",
				"    end:",
				"seq(a(n), n=0..30);  # _Alois P. Heinz_, Oct 17 2017"
			],
			"mathematica": [
				"u[0,j_]:=1;u[k_,j_]:=u[k,j]=Sum[Binomial[k-1,i-1]Plus@@(u[k-i,j]#^(i-1)\u0026/@Divisors[j]),{i,k}]; Table[u[n,3],{n,0,12}] (* _Wouter Meeussen_, Dec 06 2008 *)",
				"mx = 16; p = 3; Range[0, mx]! CoefficientList[ Series[ Exp[ (Exp[p*x] - p - 1)/p + Exp[x]], {x, 0, mx}], x] (* _Robert G. Wilson v_, Dec 12 2012 *)"
			],
			"xref": [
				"u[n,j] generates for j=1, A000110; j=2, A002872; j=3, this sequence; j=4, A141003; j=5, A036075; j=6, A141004; j=7, A036077. - _Wouter Meeussen_, Dec 06 2008",
				"Equals column 3 of A162663. - _Michel Marcus_, Mar 27 2013",
				"Row sums of A294201."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, _Simon Plouffe_",
			"ext": [
				"New name from _Danny Rorabaugh_, Oct 24 2015"
			],
			"references": 15,
			"revision": 66,
			"time": "2019-09-20T13:10:59-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}