{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A038612",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 38612,
			"data": "2,3,5,7,11,13,17,19,23,29,31,37,53,59,61,67,71,73,79,83,89,97,101,103,107,109,113,127,131,137,139,151,157,163,167,173,179,181,191,193,197,199,211,223,227,229,233,239,251,257,263,269,271,277,281,283,293",
			"name": "Primes not containing the digit '4'.",
			"comment": [
				"Subsequence of primes of A052406. - _Michel Marcus_, Feb 22 2015",
				"Maynard proves that this sequence is infinite and in particular contains the expected number of elements up to x, on the order of x^(log 9/log 10)/log x. - _Charles R Greathouse IV_, Apr 08 2016"
			],
			"link": [
				"M. F. Hasler, \u003ca href=\"/wiki/Numbers_avoiding_certain_digits\"\u003eNumbers avoiding certain digits\u003c/a\u003e OEIS wiki, Jan 12 2020.",
				"James Maynard, \u003ca href=\"http://arxiv.org/abs/1604.01041\"\u003ePrimes with restricted digits\u003c/a\u003e, arXiv:1604.01041 [math.NT], 2016.",
				"James Maynard and Brady Haran, \u003ca href=\"https://www.youtube.com/watch?v=eeoBCS7IEqs\"\u003ePrimes without a 7\u003c/a\u003e, Numberphile video (2019)."
			],
			"mathematica": [
				"Select[Prime[Range[70]], DigitCount[#, 10, 4] == 0 \u0026] (* _Vincenzo Librandi_, Aug 08 2011 *)"
			],
			"program": [
				"(MAGMA) [ p: p in PrimesUpTo(300) | not 4 in Intseq(p) ]; // _Bruno Berselli_, Aug 08 2011",
				"(PARI)",
				"lista(nn)=forprime(p=2, nn, if (!vecsearch(vecsort(digits(p),,8), 4), print1(p, \", \"));); \\\\ _Michel Marcus_, Feb 22 2015",
				"( {A038612_upto(N)=select( is_A052406, primes([1, N]))} )(444) \\\\ or better:",
				"next_A038612(n)={until(isprime(n), n=next_A052406(nextprime(n+1)-1)); n}",
				"( {A038612_vec(n,M=1)=M--;vector(n,i, n=next_A038612(if(i\u003e1, n)))} )(20, 1000)",
				"\\\\ (See the OEIS wiki page for more.) - _M. F. Hasler_, Jan 12 2020"
			],
			"xref": [
				"Intersection of A000040 (primes) and A052406 (numbers without digit 4).",
				"Primes having no digit d = 0..9 are A038618, A038603, A038604, A038611, this sequence, A038613, A038614, A038615, A038616, and A038617, respectively."
			],
			"keyword": "nonn,easy,base",
			"offset": "1,1",
			"author": "Vasiliy Danilov (danilovv(AT)usa.net), Jul 15 1998",
			"ext": [
				"Offset corrected by _Arkadiusz Wesolowski_, Aug 07 2011"
			],
			"references": 11,
			"revision": 31,
			"time": "2020-01-16T02:05:12-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}