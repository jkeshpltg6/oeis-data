{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A207543",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 207543,
			"data": "1,0,3,0,1,5,0,0,5,7,0,0,1,14,9,0,0,0,7,30,11,0,0,0,1,27,55,13,0,0,0,0,9,77,91,15,0,0,0,0,1,44,182,140,17,0,0,0,0,0,11,156,378,204,19,0,0,0,0,0,1,65,450,714,285,21,0",
			"name": "Triangle read by rows, expansion of (1+y*x)/(1-2*y*x+y*(y-1)*x^2).",
			"comment": [
				"Previous name was: \"A scaled version of triangle A082985.\"",
				"Triangle, read by rows, given by (0, 1/3, -1/3, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (3, -4/3, 1/3, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938."
			],
			"link": [
				"R. Zielinski, \u003ca href=\"https://arxiv.org/abs/1608.04006\"\u003eInduction and Analogy in a Problem of Finite Sums\u003c/a\u003e arXiv:1608.04006 [math.GM], 2016."
			],
			"formula": [
				"T(n,k) = 2*T(n-1,k-1) + T(n-2,k-1) - T(n-2,k-2), T(0,0) = 1, T(1,0) = 0, T(1,1) = 3.",
				"G.f.: (1+y*x)/(1-2*y*x+y*(y-1)*x^2).",
				"Sum_{i, i\u003e=0} T(n+i,n) = A000204(2*n+1).",
				"Sum_{k, 0\u003c=k\u003c=n} T(n,k)*x^k = A078069(n), A000007(n), A003945(n), A111566(n) for x = -1, 0, 1, 2 respectively.",
				"Sum_{k, 0\u003c=k\u003c=n} T(n,k)*x^(n-k) = A090131(n), A005408(n), A003945(n), A078057(n), A028859(n), A000244(n), A063782(n), A180168(n) for x = -1, 0, 1, 2, 3, 4, 5, 6 respectively.",
				"From _Peter Bala_, Aug 17 2016: (Start)",
				"Let S(k,n) = Sum_{i = 1..n} i^k. Calculations in Zielinski 2016 suggest the following identity holds involving the p-th row elements of this triangle:",
				"Sum_{k = 0..p} T(p,k)*S(2*k,n) = 1/2*(2*n + 1)*(n*(n + 1))^p.",
				"For example, for row 6 we find S(6,n) + 27*S(8,n) + 55*S(10,n) + 13*S(12,n) = 1/2*(2*n + 1)*(n*(n + 1))^6.",
				"There appears to be a similar result for the odd power sums S(2*k + 1,n) involving A119900. (End)"
			],
			"example": [
				"Triangle begins :",
				"1",
				"0, 3",
				"0, 1, 5",
				"0, 0, 5, 7",
				"0, 0, 1, 14, 9",
				"0, 0, 0, 7, 30, 11",
				"0, 0, 0, 1, 27, 55, 13",
				"0, 0, 0, 0, 9, 77, 91, 15",
				"0, 0, 0, 0, 1, 44, 182, 140, 17",
				"0, 0, 0, 0, 0, 11, 156, 378, 204, 19",
				"0, 0, 0, 0, 0, 1, 65, 450, 714, 285, 21",
				"0, 0, 0, 0, 0, 0, 13, 275, 1122, 1254, 385, 23"
			],
			"maple": [
				"s := (1+y*x)/(1-2*y*x+y*(y-1)*x^2): t := series(s,x,12):",
				"seq(print(seq(coeff(coeff(t,x,n),y,m),m=0..n)),n=0..11); # _Peter Luschny_, Aug 17 2016"
			],
			"xref": [
				"Cf. A082985 which is another version of this triangle.",
				"Cf. Diagonals : A005408, A000330, A005585, A050486, A054333, A057788. Cf. A119900."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,3",
			"author": "_Philippe Deléham_, Feb 24 2012",
			"ext": [
				"New name using a formula of the author from _Peter Luschny_, Aug 17 2016"
			],
			"references": 3,
			"revision": 26,
			"time": "2016-08-21T14:19:30-04:00",
			"created": "2012-02-25T18:37:15-05:00"
		}
	]
}