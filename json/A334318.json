{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A334318",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 334318,
			"data": "1,2,1,3,1,0,4,5,5,2,5,6,6,1,0,6,13,18,8,7,2,7,15,33,34,16,7,0,8,25,50,58,52,21,8,3,9,28,67,98,101,57,30,7,0,10,41,115,168,220,88,51,9,4,1,11,45,134,275,398,315,220,126,32,10,0,12,61,206,428,690,568,503,158,32,5,1,0",
			"name": "Number T(n,k) of integers in base n having exactly k distinct digits such that the number formed by the consecutive subsequence of the initial j digits is divisible by j for all j in {1,...,k}; triangle T(n,k), n\u003e=1, 1\u003c=k\u003c=n, read by rows.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A334318/b334318.txt\"\u003eRows n = 1..25, flattened\u003c/a\u003e"
			],
			"example": [
				"T(4,3) = 5: 102, 120, 201, 123, 321 (written in base 4):",
				"T(7,2) = 15: 13, 15, 20, 24, 26, 31, 35, 40, 42, 46, 51, 53, 60, 62, 64 (written in base 7)",
				"T(10,1) = 10: 0, 1, 2, 3, 4, 5, 6, 7, 8, 9.",
				"T(10,10) = 1: 3816547290.",
				"Triangle T(n,k) begins:",
				"   1;",
				"   2,  1;",
				"   3,  1,   0;",
				"   4,  5,   5,   2;",
				"   5,  6,   6,   1,   0;",
				"   6, 13,  18,   8,   7,   2;",
				"   7, 15,  33,  34,  16,   7,   0;",
				"   8, 25,  50,  58,  52,  21,   8,   3;",
				"   9, 28,  67,  98, 101,  57,  30,   7,  0;",
				"  10, 41, 115, 168, 220,  88,  51,   9,  4,  1;",
				"  11, 45, 134, 275, 398, 315, 220, 126, 32, 10, 0;",
				"  12, 61, 206, 428, 690, 568, 503, 158, 32,  5, 1, 0;",
				"  ..."
			],
			"maple": [
				"b:= proc(n, s, w) option remember; `if`(s={}, 0, (k-\u003e add((t-\u003e",
				"      `if`(t=0, x, `if`(irem(t, k)=0, b(n, s minus {j}, t)",
				"          +x^k, 0)))(w*n+j), j=s)))(1+n-nops(s))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=1..n))(b(n, {$0..n-1}, 0)):",
				"seq(T(n), n=1..14);"
			],
			"mathematica": [
				"b[n_, s_, w_] := b[n, s, w] = If[s == {}, 0, With[{k = 1+n-Length[s]}, Sum[With[{t = w*n + j}, If[t == 0, x, If[Mod[t, k] == 0, b[n, s ~Complement~ {j}, t] + x^k, 0]]], {j, s}]]];",
				"T[n_] := PadRight[CoefficientList[b[n, Range[0, n-1], 0]/x, x], n];",
				"Array[T, 14] // Flatten (* _Jean-François Alcover_, Feb 11 2021, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Columns k=1-4 give: A000027, A334320, A333405, A333469.",
				"Row sums give A334319.",
				"Bisection of main diagonal (even part) gives A181736.",
				"Cf. A111456."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Alois P. Heinz_, Apr 22 2020",
			"references": 5,
			"revision": 27,
			"time": "2021-02-11T14:33:15-05:00",
			"created": "2020-05-03T20:46:52-04:00"
		}
	]
}