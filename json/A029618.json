{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A029618",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 29618,
			"data": "1,3,2,3,5,2,3,8,7,2,3,11,15,9,2,3,14,26,24,11,2,3,17,40,50,35,13,2,3,20,57,90,85,48,15,2,3,23,77,147,175,133,63,17,2,3,26,100,224,322,308,196,80,19,2,3,29,126,324,546,630,504,276,99,21,2,3,32,155,450,870",
			"name": "Numbers in (3,2)-Pascal triangle (by row).",
			"comment": [
				"Reverse of A029600. - _Philippe Deléham_, Nov 21 2006",
				"Triangle T(n,k), read by rows, given by (3,-2,0,0,0,0,0,0,0,...) DELTA (2,-1,0,0,0,0,0,0,0,...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Oct 10 2011",
				"Row n: expansion of (3+2x)*(1+x)^(n-1), n\u003e0. - _Philippe Deléham_, Oct 10 2011",
				"For a closed-form formula for generalized Pascal's triangle see A228576. - _Boris Putievskiy_, Sep 04 2013"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A029618/b029618.txt\"\u003eRows n = 0..100 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = T(n-1,k-1) + T(n-1,k) with T(0,0)=1, T(n,0)=3, T(n,n)=2; n, k \u003e 0. - _Boris Putievskiy_, Sep 04 2013",
				"G.f.: (-1-x*y-2*x)/(-1+x*y+x). - _R. J. Mathar_, Aug 11 2015"
			],
			"example": [
				"Triangle begins as:",
				"  1;",
				"  3,  2;",
				"  3,  5,  2;",
				"  3,  8,  7,  2;",
				"  3, 11, 15,  9,  2;",
				"  ..."
			],
			"maple": [
				"A029618 := proc(n,k)",
				"    if k \u003c 0 or k \u003e n then",
				"        0;",
				"    elif  n = 0 then",
				"        1;",
				"    elif k=0 then",
				"        3;",
				"    elif k = n then",
				"        2;",
				"    else",
				"        procname(n-1,k-1)+procname(n-1,k) ;",
				"    end if;",
				"end proc: # _R. J. Mathar_, Jul 08 2015"
			],
			"mathematica": [
				"T[n_, k_]:= T[n, k]= If[n==0 \u0026\u0026 k==0, 1, If[k==0, 3, If[k==n, 2, T[n-1, k-1] + T[n-1, k] ]]]; Table[T[n, k], {n, 0, 12}, {k, 0, n}]//Flatten (* _G. C. Greubel_, Nov 13 2019 *)"
			],
			"program": [
				"(PARI) T(n,k) = if(n==0 \u0026\u0026 k==0, 1, if(k==0, 3, if(k==n, 2, T(n-1, k-1) + T(n-1, k) ))); \\\\ _G. C. Greubel_, Nov 12 2019",
				"(Sage)",
				"@CachedFunction",
				"def T(n, k):",
				"    if (n==0 and k==0): return 1",
				"    elif (k==0): return 3",
				"    elif (k==n): return 2",
				"    else: return T(n-1,k-1) + T(n-1, k)",
				"[[T(n, k) for k in (0..n)] for n in (0..12)] # _G. C. Greubel_, Nov 12 2019",
				"(GAP)",
				"T:= function(n,k)",
				"    if n=0 and k=0 then return 1;",
				"    elif k=0 then return 3;",
				"    elif k=n then return 2;",
				"    else return T(n-1,k-1) + T(n-1,k);",
				"    fi;",
				"  end;",
				"Flat(List([0..12], n-\u003e List([0..n], k-\u003e T(n,k) ))); # _G. C. Greubel_, Nov 12 2019"
			],
			"xref": [
				"Cf. A007318, A029600, A084938, A228196, A228576, A016789 (2nd column), A005449 (3rd column), A006002 (4th column)."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,2",
			"author": "_Mohammad K. Azarian_",
			"references": 24,
			"revision": 40,
			"time": "2019-11-13T01:49:27-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}