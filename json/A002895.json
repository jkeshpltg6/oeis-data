{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002895",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2895,
			"id": "M3626 N1473",
			"data": "1,4,28,256,2716,31504,387136,4951552,65218204,878536624,12046924528,167595457792,2359613230144,33557651538688,481365424895488,6956365106016256,101181938814289564,1480129751586116848,21761706991570726096,321401321741959062016",
			"name": "Domb numbers: number of 2n-step polygons on diamond lattice.",
			"comment": [
				"a(n) is the (2n)th moment of the distance from the origin of a 4-step random walk in the plane. - Peter M.W. Gill (peter.gill(AT)nott.ac.uk), Mar 03 2004",
				"Row sums of the cube of A008459. - _Peter Bala_, Mar 05 2013",
				"Conjecture: Let D(n) be the (n+1) X (n+1) Hankel-type determinant with (i,j)-entry equal to a(i+j) for all i,j = 0..n. Then the number D(n)/12^n is always a positive odd integer. - _Zhi-Wei Sun_, Aug 14 2013",
				"It appears that the expansions exp( Sum_{n \u003e= 1} a(n)*x^n/n ) = 1 + 4*x + 22*x^2 + 152*x^3 + 1241*x^4 + ... and exp( Sum_{n \u003e= 1} 1/4*a(n)*x^n/n ) = 1 + x + 4*x^2 + 25*x^3 + 199*x^4 + ... have integer coefficients. See A267219. - _Peter Bala_, Jan 12 2016",
				"This is one of the Apéry-like sequences - see Cross-references. - _Hugo Pfoertner_, Aug 06 2017",
				"Named after the British-Israeli theoretical physicist Cyril Domb (1920-2012). - _Amiram Eldar_, Mar 20 2021"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Indranil Ghosh, \u003ca href=\"/A002895/b002895.txt\"\u003eTable of n, a(n) for n = 0..832\u003c/a\u003e (terms 0..100 from T. D. Noe)",
				"B. Adamczewski, Jason P. Bell and E. Delaygue, \u003ca href=\"http://arxiv.org/abs/1603.04187\"\u003eAlgebraic independence of G-functions and congruences \"a la Lucas\"\u003c/a\u003e, arXiv preprint arXiv:1603.04187 [math.NT], 2016.",
				"David H. Bailey, Jonathan M. Borwein, David Broadhurst and M. L. Glasser, \u003ca href=\"https://doi.org/10.1088/1751-8113/41/20/205203\"\u003eElliptic integral evaluations of Bessel moments and applications\u003c/a\u003e, Journal of Physics A: Mathematical and Theoretical, Vol. 41, No. 20 (2008), 205203; \u003ca href=\"http://arxiv.org/abs/0801.0891\"\u003earXiv preprint\u003c/a\u003e, arXiv:0801.0891 [hep-th], 2008.",
				"Jonathan M. Borwein, \u003ca href=\"http://doi.org/10.5642/jhummath.201601.07\"\u003eA short walk can be beautiful\u003c/a\u003e, Journal of Humanistic Mathematics, Vol. 6, No. 1 (2016), pp. 86-109; \u003ca href=\"https://www.carma.newcastle.edu.au/jon/beauty.pdf\"\u003epreprint\u003c/a\u003e, 2015.",
				"Jonathan M. Borwein, \u003ca href=\"https://www.carma.newcastle.edu.au/jon/OEIStalk.pdf\"\u003eAdventures with the OEIS: Five sequences Tony may like\u003c/a\u003e, Guttman 70th [Birthday] Meeting, 2015, revised May 2016.",
				"Jonathan M. Borwein, \u003ca href=\"/A060997/a060997.pdf\"\u003eAdventures with the OEIS: Five sequences Tony may like\u003c/a\u003e, Guttman 70th [Birthday] Meeting, 2015, revised May 2016. [Cached copy, with permission]",
				"Jonathan M. Borwein and Armin Straub, \u003ca href=\"https://doi.org/10.1016/j.tcs.2012.10.025\"\u003eMahler measures, short walks and log-sine integrals\u003c/a\u003e, Theoretical Computer Science, Vol. 479 (2013), pp. 4-21.",
				"Jonathan M. Borwein, Armin Straub and Christophe Vignat, \u003ca href=\"http://www.carma.newcastle.edu.au/jon/dwalks.pdf\"\u003eDensities of short uniform random walks, Part II: Higher dimensions\u003c/a\u003e, Preprint, 2015.",
				"Jonathan M. Borwein, Dirk Nuyens, Armin Straub and James Wan, \u003ca href=\"https://carma.newcastle.edu.au/resources/jon/Preprints/Papers/Submitted%20Papers/Walks/fpsac.pdf\"\u003eRandom Walk Integrals\u003c/a\u003e, 2010.",
				"Alin Bostan, Andrew Elvey Price, Anthony John Guttmann and Jean-Marie Maillard, \u003ca href=\"https://arxiv.org/abs/2001.00393\"\u003eStieltjes moment sequences for pattern-avoiding permutations\u003c/a\u003e, arXiv:2001.00393 [math.CO], 2020.",
				"H. Huat Chan, Song Heng Chan and Zhiguo Liu, \u003ca href=\"http://dx.doi.org/10.1016/j.aim.2003.07.012\"\u003eDomb's numbers and Ramanujan-Sato type series for 1/pi\u003c/a\u003e, Adv. Math., Vol. 186, No. 2 (2004), pp. 396-410.",
				"Shaun Cooper, James G. Wan and Wadim Zudilin, \u003ca href=\"https://doi.org/10.1007/978-3-319-68376-8_12\"\u003eHolonomic Alchemy and Series for 1/pi\u003c/a\u003e, in: G. Andrews and F. Garvan (eds.) Analytic Number Theory, Modular Forms and q-Hypergeometric Series, ALLADI60 2016, Springer Proceedings in Mathematics \u0026 Statistics, Vol 221. Springer, Cham, 2016; \u003ca href=\"https://arxiv.org/abs/1512.04608\"\u003earXiv preprint\u003c/a\u003e, arXiv:1512.04608 [math.NT], 2015.",
				"Eric Delaygue, \u003ca href=\"https://doi.org/10.1112/S0010437X17007552\"\u003eArithmetic properties of Apéry-like numbers\u003c/a\u003e, Compositio Mathematica, Vol. 154, No. 2 (2018), pp. 249-274; \u003ca href=\"http://arxiv.org/abs/1310.4131\"\u003earXiv preprint\u003c/a\u003e, arXiv:1310.4131 [math.NT], 2013-2015.",
				"Cyril Domb, \u003ca href=\"http://dx.doi.org/10.1080/00018736000101199\"\u003eOn the theory of cooperative phenomena in crystals\u003c/a\u003e, Advances in Phys., Vol. 9 (1960), pp. 149-361.",
				"Ofir Gorodetsky, \u003ca href=\"https://arxiv.org/abs/2102.11839\"\u003eNew representations for all sporadic Apéry-like sequences, with applications to congruences\u003c/a\u003e, arXiv:2102.11839 [math.NT], 2021. See alpha p. 3.",
				"John A. Hendrickson, Jr., \u003ca href=\"http://dx.doi.org/10.1080/00949659508811639\"\u003eOn the enumeration of rectangular (0,1)-matrices\u003c/a\u003e, Journal of Statistical Computation and Simulation, Vol. 51 (1995), pp. 291-313.",
				"Ji-Cai Liu, \u003ca href=\"https://arxiv.org/abs/2008.02647\"\u003eSupercongruences for sums involving Domb numbers\u003c/a\u003e, arXiv:2008.02647 [math.NT], 2020.",
				"Rui-Li Liu and Feng-Zhen Zhao, \u003ca href=\"https://www.emis.de/journals/JIS/VOL21/Liu/liu19.html\"\u003eNew Sufficient Conditions for Log-Balancedness, With Applications to Combinatorial Sequences\u003c/a\u003e, J. Int. Seq., Vol. 21 (2018), Article 18.5.7.",
				"Yen Lee Loh, \u003ca href=\"https://doi.org/10.1088/1751-8121/aa85f6\"\u003eA general method for calculating lattice green functions on the branch cut\u003c/a\u003e, Journal of Physics A: Mathematical and Theoretical, Vol. 50, No. 40 (2017), 405203; \u003ca href=\"https://arxiv.org/abs/1706.03083\"\u003earXiv preprint\u003c/a\u003e, arXiv:1706.03083 [math-ph], 2017.",
				"Amita Malik and Armin Straub, \u003ca href=\"https://doi.org/10.1007/s40993-016-0036-8\"\u003eDivisibility properties of sporadic Apéry-like numbers\u003c/a\u003e, Research in Number Theory, 2016, 2:5.",
				"Guo-Shuai Mao and Yan Liu, \u003ca href=\"https://arxiv.org/abs/2112.00511\"\u003eProof of some conjectural congruences involving Domb numbers\u003c/a\u003e, arXiv:2112.00511 [math.NT], 2021.",
				"Guo-Shuai Mao and Michael J. Schlosser, \u003ca href=\"https://arxiv.org/abs/2112.12732\"\u003eSupercongruences involving Domb numbers and binary quadratic forms\u003c/a\u003e, arXiv:2112.12732 [math.NT], 2021.",
				"Robert Osburn and Brundaban Sahu, \u003ca href=\"https://projecteuclid.org/euclid.facm/1364222827\"\u003eA supercongruence for generalized Domb numbers\u003c/a\u003e, Functiones et Approximatio Commentarii Mathematici, Vol. 48, No. 1 (2013), pp. 29-36; \u003ca href=\"http://maths.ucd.ie/~osburn/superdomb.pdf\"\u003epreprint\u003c/a\u003e.",
				"L. B. Richmond and Jeffrey Shallit, \u003ca href=\"http://ftp.gwdg.de/pub/EMIS/journals/EJC/Volume_16/PDF/v16i1r72.pdf\"\u003eCounting Abelian Squares\u003c/a\u003e, The Electronic Journal of Combinatorics, Vol. 16, No. 1 (2009), Article R72; \u003ca href=\"http://arxiv.org/abs/0807.5028\"\u003earXiv preprint\u003c/a\u003e, arXiv:0807.5028 [math.CO], 2008.",
				"Armin Straub, \u003ca href=\"http://arminstraub.com/pub/dissertation\"\u003eArithmetic aspects of random walks and methods in definite integration\u003c/a\u003e, Ph. D. Dissertation, School Of Science And Engineering, Tulane University, 2012.",
				"Zhi-Hong Sun, \u003ca href=\"https://arxiv.org/abs/2002.12072\"\u003eSuper congruences concerning binomial coefficients and Apéry-like numbers\u003c/a\u003e, arXiv:2002.12072 [math.NT], 2020.",
				"Zhi-Hong Sun, \u003ca href=\"https://arxiv.org/abs/2004.07172\"\u003eNew congruences involving Apéry-like numbers\u003c/a\u003e, arXiv:2004.07172 [math.NT], 2020.",
				"Zhi-Wei Sun, \u003ca href=\"https://doi.org/10.1142/9789814452458_0014\"\u003eConjectures involving arithmetical sequences\u003c/a\u003e, in: S. Kanemitsu, H. Li and J. Liu (eds.), Number Theory: Arithmetic in Shangri-La, Proc. the 6th China-Japan Sem. Number Theory (Shanghai, August 15-17, 2011), World Sci., Singapore, 2013, pp. 244-258; \u003ca href=\"http://maths.nju.edu.cn/~zwsun/143p.pdf\"\u003ealternative link\u003c/a\u003e.",
				"H. A. Verrill, \u003ca href=\"https://arxiv.org/abs/math/0407327\"\u003eSums of squares of binomial coefficients, with applications to Picard-Fuchs equations\u003c/a\u003e, arXiv:math/0407327 [math.CO], 2004.",
				"Chen Wang, \u003ca href=\"https://arxiv.org/abs/2003.09888\"\u003eSupercongruences and hypergeometric transformations\u003c/a\u003e, arXiv:2003.09888 [math.NT], 2020.",
				"Yi Wang and BaoXuan Zhu, \u003ca href=\"https://doi.org/10.1007/s11425-014-4851-x\"\u003eProofs of some conjectures on monotonicity of number-theoretic and combinatorial sequences\u003c/a\u003e, Science China Mathematics, Vol. 57, No. 11 (2014), pp. 2429-2435; \u003ca href=\"http://arxiv.org/abs/1303.5595\"\u003earXiv preprint\u003c/a\u003e, arXiv:1303.5595 [math.CO], 2013.",
				"Bao-Xuan Zhu, \u003ca href=\"http://arxiv.org/abs/1309.6025\"\u003eHigher order log-monotonicity of combinatorial sequences\u003c/a\u003e, arXiv preprint, arXiv:1309.6025 [math.CO], 2013."
			],
			"formula": [
				"a(n) = Sum_{k=0..n} binomial(n, k)^2 * binomial(2n-2k, n-k) * binomial(2k, k).",
				"D-finite with recurrence: n^3*a(n) = 2*(2*n-1)*(5*n^2-5*n+2)*a(n-1) - 64*(n-1)^3*a(n-2). - _Vladeta Jovovic_, Jul 16 2004",
				"Sum_{n\u003e=0} a(n)*x^n/n!^2 = BesselI(0, 2*sqrt(x))^4. - _Vladeta Jovovic_, Aug 01 2006",
				"G.f.: hypergeom([1/6, 1/3],[1],108*x^2/(1-4*x)^3)^2/(1-4*x). - _Mark van Hoeij_, Oct 29 2011",
				"From _Zhi-Wei Sun_, Mar 20 2013: (Start)",
				"Via the Zeilberger algorithm, _Zhi-Wei Sun_ proved that:",
				"(1) 4^n*a(n) = Sum_{k = 0..n} (binomial(2k,k)*binomial(2(n-k),n-k))^3/ binomial(n,k)^2,",
				"(2) a(n) = Sum_{k = 0..n} (-1)^(n-k)*binomial(n,k)*binomial(2k,n)*binomial(2k,k)* binomial(2(n-k),n-k). (End)",
				"a(n) ~ 2^(4*n+1)/((Pi*n)^(3/2)). - _Vaclav Kotesovec_, Aug 20 2013",
				"G.f. y=A(x) satisfies: 0 = x^2*(4*x - 1)*(16*x - 1)*y''' + 3*x*(128*x^2 - 30*x + 1)*y'' + (448*x^2 - 68*x + 1)*y' + 4*(16*x - 1)*y. - _Gheorghe Coserea_, Jun 26 2018",
				"a(n) = Sum_{p+q+r+s=n} (n!/(p!*q!*r!*s!))^2 with p,q,r,s \u003e= 0. See Verrill, p. 5. - _Peter Bala_, Jan 06 2020"
			],
			"maple": [
				"A002895 := n -\u003e add(binomial(n,k)^2*binomial(2*n-2*k,n-k)*binomial(2*k,k), k=0..n): seq(A002895(n), n=0..25); # _Wesley Ivan Hurt_, Dec 20 2015",
				"A002895 := n -\u003e binomial(2*n,n)*hypergeom([1/2, -n, -n, -n],[1, 1, 1/2 - n], 1):",
				"seq(simplify(A002895(n)), n=0..19); # _Peter Luschny_, May 23 2017"
			],
			"mathematica": [
				"Table[Sum[Binomial[n,k]^2 Binomial[2n-2k,n-k]Binomial[2k,k],{k,0,n}], {n,0,30}] (* _Harvey P. Dale_, Aug 15 2011 *)",
				"a[n_] = Binomial[2*n, n]*HypergeometricPFQ[{1/2, -n, -n, -n}, {1, 1, 1/2-n}, 1]; (* or *) a[n_] := SeriesCoefficient[BesselI[0, 2*Sqrt[x]]^4, {x, 0, n}]*n!^2; Table[a[n], {n, 0, 19}] (* _Jean-François Alcover_, Dec 30 2013, after _Vladeta Jovovic_ *)",
				"max = 19; Total /@ MatrixPower[Table[Binomial[n, k]^2, {n, 0, max}, {k, 0, max}], 3] (* _Jean-François Alcover_, Mar 24 2015, after _Peter Bala_ *)"
			],
			"program": [
				"(PARI) C=binomial;",
				"a(n) = sum(k=0,n, C(n,k)^2 * C(2*n-2*k,n-k) * C(2*k,k) );",
				"/* _Joerg Arndt_, Apr 19 2013 */"
			],
			"xref": [
				"Cf. A002893, A008459, A169714, A169715, A228289, A267219.",
				"The Apéry-like numbers [or Apéry-like sequences, Apery-like numbers, Apery-like sequences] include A000172, A000984, A002893, A002895, A005258, A005259, A005260, A006077, A036917, A063007, A081085, A093388, A125143 (apart from signs), A143003, A143007, A143413, A143414, A143415, A143583, A183204, A214262, A219692,A226535, A227216, A227454, A229111 (apart from signs), A260667, A260832, A262177, A264541, A264542, A279619, A290575, A290576. (The term \"Apery-like\" is not well-defined.)",
				"For primes that do not divide the terms of the sequences A000172, A005258, A002893, A081085, A006077, A093388, A125143, A229111, A002895, A290575, A290576, A005259 see A260793, A291275-A291284 and A133370 respectively."
			],
			"keyword": "nonn,easy,nice,walk",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Vladeta Jovovic_, Mar 11 2003"
			],
			"references": 62,
			"revision": 209,
			"time": "2021-12-24T08:12:00-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}