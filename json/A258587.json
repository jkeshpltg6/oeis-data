{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A258587",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 258587,
			"data": "1,-2,1,-2,2,0,2,0,0,-2,1,-4,0,0,2,0,3,-2,2,-2,2,0,0,0,0,-4,2,-2,0,0,0,0,3,-2,0,-2,4,0,2,0,0,-4,1,-2,0,0,4,0,2,-2,0,-4,2,0,0,0,0,-2,2,-2,0,0,0,0,2,-2,3,-4,2,0,2,0,0,0,2,-2,0,0,2,0,3",
			"name": "Expansion of f(-x, -x) * f(x^2, x^10) in powers of x where f(, ) is Ramanujan's general theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A258587/b258587.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of phi(-x) * chi(x^2) * psi(-x^6) in powers of x where phi(), psi(), chi() are Ramanujan theta functions.",
				"Expansion of q^(-2/3) * eta(q)^2 * eta(q^4)^2 * eta(q^6) * eta(q^24) / (eta(q^2)^2 * eta(q^8) * eta(q^12)) in powers of q.",
				"Euler transform of period 24 sequence [ -2, 0, -2, -2, -2, -1, -2, -1, -2, 0, -2, -2, -2, 0, -2, -1, -2, -1, -2, -2, -2, 0, -2, -2, ...].",
				"a(n) = (-1)^n * A263548(n) = A128581(3*n + 2) = A190611(3*n + 2).",
				"a(2*n) = A263571(n). a(2*n + 1) = -2 * A128582(n)."
			],
			"example": [
				"G.f. = 1 - 2*x + x^2 - 2*x^3 + 2*x^4 + 2*x^6 - 2*x^9 + x^10 - 4*x^11 + 2*x^14 + ...",
				"G.f. = q^2 - 2*q^5 + q^8 - 2*q^11 + 2*q^14 + 2*q^20 - 2*q^29 + q^32 - 4*q^35 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 4, 0, x] QPochhammer[ -x^2, x^12] QPochhammer[ -x^10, x^12] QPochhammer[ x^12], {x, 0, n}];",
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 4, 0, x] QPochhammer[ -x^2, x^4] EllipticTheta[ 2, 0, x^3] / (2^(1/2) x^(3/4)), {x, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A)^2 * eta(x^4 + A)^2 * eta(x^6 + A) * eta(x^24 + A) / (eta(x^2 + A)^2 * eta(x^8 + A) * eta(x^12 + A)), n))};"
			],
			"xref": [
				"Cf. A128581, A128582, A190611, A263548, A263571."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Nov 06 2015",
			"references": 1,
			"revision": 14,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2015-11-06T09:31:49-05:00"
		}
	]
}