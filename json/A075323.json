{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A075323",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 75323,
			"data": "3,5,7,11,13,19,23,31,37,47,17,29,53,67,43,59,61,79,83,103,109,131,73,97,101,127,139,167,41,71,149,181,157,191,137,173,113,151,193,233,197,239,179,223,211,257,229,277,263,313,199",
			"name": "Pair the odd primes so that the n-th pair is (p, p+2n) where p is the smallest prime not included earlier such that p and p+2n are primes and p+2n also does not occur earlier: (3, 5), (7, 11), (13, 19), (23, 31), (37, 47), (17, 29), ... This lists the successive pairs in order.",
			"comment": [
				"Question: Is every odd prime a member of some pair?",
				"2683 = A065091(388) seems to be missing, as presumably A247233(388)=0; but if A247233(n) \u003e 0: a(A247233(n)) = A065091(n) = A000040(n+1). - _Reinhard Zumkeller_, Nov 29 2014"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A075323/b075323.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"maple": [
				"# A075321p implemented in A075321",
				"A075323 := proc(n)",
				"    if type(n,'odd') then",
				"        op(1,A075321p((n+1)/2)) ;",
				"    else",
				"        op(2,A075321p(n/2)) ;",
				"    end if;",
				"end proc:",
				"seq(A075323(n),n=1..60) ; # _R. J. Mathar_, Nov 26 2014"
			],
			"mathematica": [
				"A075321p[n_] := A075321p[n] = Module[{prevlist, i, p, q}, If[n == 1, Return[{3, 5}], prevlist = Array[A075321p, n - 1] // Flatten]; For[i = 2, True, i++, p = Prime[i]; If[FreeQ[prevlist, p], q = p + 2*n; If[PrimeQ[q] \u0026\u0026 FreeQ[prevlist, q], Return[{p, q}]]]]];",
				"A075323[n_] := If[OddQ[n], A075321p[(n+1)/2][[1]], A075321p[n/2][[2]]];",
				"Array[A075323, 50] (* _Jean-François Alcover_, Feb 12 2018, after _R. J. Mathar_ *)"
			],
			"program": [
				"(Haskell)",
				"import Data.List ((\\\\))",
				"a075323 n = a075323_list !! (n-1)",
				"a075323_list = f 1 []  $ tail a000040_list where",
				"   f k ys qs = g qs where",
				"     g (p:ps) | a010051' pk == 0 || pk `elem` ys = g ps",
				"              | otherwise = p : pk : f (k + 1) (p:pk:ys) (qs \\\\ [p, pk])",
				"              where pk = p + 2 * k",
				"-- _Reinhard Zumkeller_, Nov 29 2014"
			],
			"xref": [
				"Cf. A075321, A075322.",
				"Cf. A010051, A000040, A065091, A247233."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Amarnath Murthy_, Sep 14 2002",
			"ext": [
				"Corrected by _R. J. Mathar_, Nov 26 2014"
			],
			"references": 5,
			"revision": 21,
			"time": "2018-02-12T11:03:12-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}