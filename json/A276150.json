{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A276150",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 276150,
			"data": "0,1,1,2,2,3,1,2,2,3,3,4,2,3,3,4,4,5,3,4,4,5,5,6,4,5,5,6,6,7,1,2,2,3,3,4,2,3,3,4,4,5,3,4,4,5,5,6,4,5,5,6,6,7,5,6,6,7,7,8,2,3,3,4,4,5,3,4,4,5,5,6,4,5,5,6,6,7,5,6,6,7,7,8,6,7,7,8,8,9,3,4,4,5,5,6,4,5,5,6,6,7,5,6,6,7,7,8,6,7,7,8,8,9,7,8,8,9,9,10,4",
			"name": "Sum of digits when n is written in primorial base (A049345); minimal number of primorials (A002110) that add to n.",
			"comment": [
				"The sum of digits of n in primorial base is odd if n is 1 or 2 (mod 4) and even if n is 0 or 3 (mod 4). Proof: primorials are 1 or 2 (mod 4) and a(n) can be constructed via the greedy algorithm. So if n = 4k + r where 0 \u003c= r \u003c 4, 4k needs an even number of primorials and r needs hammingweight(n) = A000120(n) primorials. Q.E.D. - _David A. Corneth_, Feb 27 2019"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A276150/b276150.txt\"\u003eTable of n, a(n) for n = 0..30030\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#primorialbase\"\u003eIndex entries for sequences related to primorial base\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 1 + a(A276151(n)) = 1 + a(n-A002110(A276084(n))), a(0) = 0.",
				"or for n \u003e= 1: a(n) = 1 + a(n-A260188(n)).",
				"Other identities and observations. For all n \u003e= 0:",
				"a(n) = A001222(A276086(n)) = A001222(A278226(n)).",
				"a(n) \u003e= A267263(n).",
				"From _Antti Karttunen_, Feb 27 2019: (Start)",
				"a(n) = A000120(A277022(n)).",
				"a(A283477(n)) = A324342(n).",
				"(End)"
			],
			"example": [
				"For n=24, which is \"400\" in primorial base (as 24 = 4*(3*2*1) + 0*(2*1) + 0*1, see A049345), the sum of digits is 4, thus a(24) = 4."
			],
			"mathematica": [
				"nn = 120; b = MixedRadix[Reverse@ Prime@ NestWhileList[# + 1 \u0026, 1, Times @@ Prime@ Range[# + 1] \u003c= nn \u0026]]; Table[Total@ IntegerDigits[n, b], {n, 0, nn}] (* Version 10.2, or *)",
				"nn = 120; f[n_] := Block[{a = {{0, n}}}, Do[AppendTo[a, {First@ #, Last@ #} \u0026@ QuotientRemainder[a[[-1, -1]], Times @@ Prime@ Range[# - i]]], {i, 0, #}] \u0026@ NestWhile[# + 1 \u0026, 0, Times @@ Prime@ Range[# + 1] \u003c= n \u0026]; Rest[a][[All, 1]]]; Table[Total@ f@ n, {n, 0, 120}] (* _Michael De Vlieger_, Aug 26 2016 *)"
			],
			"program": [
				"(Scheme, two versions)",
				"(definec (A276150 n) (if (zero? n) 0 (+ 1 (A276150 (- n (A002110 (A276084 n)))))))",
				"(define (A276150 n) (A001222 (A276086 n)))",
				"(Python)",
				"from sympy import prime, primefactors",
				"def Omega(n): return 0 if n==1 else Omega(n//primefactors(n)[0]) + 1",
				"def a276086(n):",
				"    i=0",
				"    m=pr=1",
				"    while n\u003e0:",
				"        i+=1",
				"        N=prime(i)*pr",
				"        if n%N!=0:",
				"            m*=(prime(i)**((n%N)/pr))",
				"            n-=n%N",
				"        pr=N",
				"    return m",
				"def a(n): return Omega(a276086(n))",
				"print([a(n) for n in range(201)]) # _Indranil Ghosh_, Jun 23 2017",
				"(PARI) A276150(n) = { my(s=0, p=2, d); while(n, d = (n%p); s += d; n = (n-d)/p; p = nextprime(1+p)); (s); }; \\\\ _Antti Karttunen_, Feb 27 2019"
			],
			"xref": [
				"Cf. A000120, A001222, A002110, A049345, A053589, A235168, A260188, A267263, A276084, A276086, A276151, A277022, A278226, A283477, A319713, A319715, A321683, A324342, A324382, A324383, A324386, A324387.",
				"Cf. A014601, A042963 (positions of even and odd terms).",
				"Differs from analogous A034968 for the first time at n=24."
			],
			"keyword": "nonn,look,base",
			"offset": "0,4",
			"author": "_Antti Karttunen_, Aug 22 2016",
			"references": 47,
			"revision": 56,
			"time": "2020-04-30T13:35:22-04:00",
			"created": "2016-08-23T12:41:03-04:00"
		}
	]
}