{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A229816",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 229816,
			"data": "1,0,2,2,4,5,9,11,18,23,34,44,63,80,111,142,190,242,319,402,522,655,837,1045,1322,1638,2053,2532,3144,3857,4757,5803,7111,8636,10516,12716,15404,18543,22355,26807,32168,38430,45929,54670,65088,77220,91599,108330,128077,151006,177974",
			"name": "Number of partitions of n such that if the length is k then k is not a part.",
			"comment": [
				"For example with n=5 neither 32 or 311 are allowed.",
				"Conjecture: Also, for n\u003e=1, a(n-1) is the total number of distinct parts of each partition of 2n with partition rank n. - _George Beck_, Jun 23 2019"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A229816/b229816.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A000041(n) - A002865(n-1), n\u003e=1. [_Joerg Arndt_, Sep 30 2013]",
				"G.f.: 1/E(x) - x*(1-x)/E(x) where E(x) = Product_{k\u003e=1} 1-x^k. [_Joerg Arndt_, Sep 30 2013]"
			],
			"example": [
				"a(2) = 2 : 2, 11.",
				"a(6) = 9 : 6, 51, 411, 33, 3111, 222, 2211, 21111, 111111."
			],
			"maple": [
				"b:= proc(n, i, t) option remember; `if`(n=0, 1, `if`(i\u003ct, 0,",
				"       b(n, i-1, t)+`if`(i\u003en, 0, b(n-i, i, t))))",
				"    end:",
				"a:= n-\u003e b(n$2, 1)-b((n-1)$2, 2):",
				"seq(a(n), n=0..60);  # _Alois P. Heinz_, Sep 30 2013"
			],
			"mathematica": [
				"nn=50;CoefficientList[Series[ Product[1/(1-x^i),{i,1,nn}]-x Product[1/(1-x^i),{i,2,nn}],{x,0,nn}],x] (* _Geoffrey Critzer_, Sep 30 2013 *)",
				"Table[PartitionsP[n] - (PartitionsP[n - 1] - PartitionsP[n - 2]), {n, 0, 60}] (* _Vincenzo Librandi_, Juan 30 2019 *)"
			],
			"program": [
				"(PARI)",
				"N=66;  x='x+O('x^N);",
				"gf = 1/eta(x) - x*(1-x)/eta(x);",
				"Vec( gf )",
				"\\\\ _Joerg Arndt_, Sep 30 2013"
			],
			"xref": [
				"Cf. A116645.",
				"Cf. A002865 (partitions where the number of parts is itself a part).",
				"Cf. A000041, A002865."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Jon Perry_, Sep 30 2013",
			"ext": [
				"Corrected a(8) and extended beyond a(9), _Joerg Arndt_, Sep 30 2013"
			],
			"references": 2,
			"revision": 37,
			"time": "2019-06-30T22:42:44-04:00",
			"created": "2013-09-30T11:42:40-04:00"
		}
	]
}