{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A071234",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 71234,
			"data": "11,3,5,7,919,11,13,0,17,19,21121,23,0,27127,29,31,33533,0,37,39139,41,43,0,47,49549,51151,53,0,57457,59,61,63463,0,67,690269,71,73,0,77377,79,81181,83,0,87187,89,91291,93493,0,97",
			"name": "Smallest prime beginning and ending in 2n+1 or 0 if no such prime exists.",
			"comment": [
				"If the two copies of the 2n+1 are not allowed to share digits, a(1) is not 3, a(2) is not 5 and a(3) is not 7 and a(5) is not 11, which results in A070278.",
				"Conjecture: a(n) = 0 if and only if n \u003e 2 and n == 2 mod 5 (which makes 2*n+1 == 0 mod 5). - _Robert Israel_, May 26 2015"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A071234/b071234.txt\"\u003eTable of n, a(n) for n = 0..10000 \u003c/a\u003e"
			],
			"example": [
				"a(6060) = 1212121, because it is the smallest prime beginning and ending in 2*6060+1 = 12121.",
				"a(7+5*k) = 0, because 2*(7+5*k)+1 = 15+10*k == 0 (mod 5) is no prime."
			],
			"maple": [
				"A071234 := proc(n)",
				"        local dgsn ,p,wrks,doff;",
				"        dgsn := convert(2*n+1,base,10) ;",
				"        for i from 1 to 1000000 do",
				"                p := ithprime(i) ;",
				"                wrks := true;",
				"                dgsp := convert(p,base,10) ;",
				"                doff := nops(dgsp)-nops(dgsn) ;",
				"                if doff \u003e= 0 then",
				"                        for d from 1 to nops(dgsn) do",
				"                                if op(d,dgsp) \u003c\u003e op(d,dgsn) then",
				"                                        wrks := false;",
				"                                        break;",
				"                                end if;",
				"                                if op(d+doff,dgsp) \u003c\u003e op(d,dgsn) then",
				"                                        wrks := false;",
				"                                        break;",
				"                                end if;",
				"                        end do:",
				"                        if wrks then",
				"                                return p",
				"                        end if;",
				"                end if;",
				"        end do:",
				"        return 0 ;",
				"end proc: # _R. J. Mathar_, Feb 03 2011",
				"# Alternative:",
				"f:= proc(n)",
				"    local u,d,r,x,y;",
				"    u:= 2*n+1;",
				"    if isprime(u) then return(u) fi;",
				"    if u mod 5 = 0 then return(0) fi;",
				"    d:= ilog10(u);",
				"    for r from 0 do",
				"      for x from 0 to 10^(r+1)-1 do",
				"       y:= u + 10^(d+1)*x + 10^(r+d+2)*u;",
				"      if isprime(y) then return(y) fi",
				"    od od",
				"end proc:",
				"11, seq(f(n), n=1..100); # _Robert Israel_, May 26 2015"
			],
			"program": [
				"(Sage)",
				"def A071234(n):",
				"    s = str(2*n+1)",
				"    if s.endswith('5') and not s == '5': return 0",
				"    for p in Primes():",
				"        ps = str(p)",
				"        if ps.startswith(s) and ps.endswith(s): return p",
				"[A071234(n) for n in range(20)]",
				"# _D. S. McNeil_, Feb 03 2011"
			],
			"xref": [
				"Cf. A070278."
			],
			"keyword": "nonn,base",
			"offset": "0,1",
			"author": "_Amarnath Murthy_, May 18 2002",
			"references": 2,
			"revision": 31,
			"time": "2020-03-14T13:18:31-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}