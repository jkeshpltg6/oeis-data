{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A014571",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 14571,
			"data": "4,1,2,4,5,4,0,3,3,6,4,0,1,0,7,5,9,7,7,8,3,3,6,1,3,6,8,2,5,8,4,5,5,2,8,3,0,8,9,4,7,8,3,7,4,4,5,5,7,6,9,5,5,7,5,7,3,3,7,9,4,1,5,3,4,8,7,9,3,5,9,2,3,6,5,7,8,2,5,8,8,9,6,3,8,0,4,5,4,0,4,8,6,2,1,2,1,3,3,3,9,6,2,5,6",
			"name": "Consider the Morse-Thue sequence (A010060) as defining a binary constant and convert it to decimal.",
			"comment": [
				"This constant is transcendental (Mahler, 1929). - _Amiram Eldar_, Nov 14 2020"
			],
			"reference": [
				"Steven R. Finch, Mathematical Constants, Cambridge University Press, 2003, Section 6.8 Prouhet-Thue-Morse Constant, p. 437."
			],
			"link": [
				"Harry J. Smith, \u003ca href=\"/A014571/b014571.txt\"\u003eTable of n, a(n) for n = 0..20000\u003c/a\u003e",
				"Boris Adamczewski and Yann Bugeaud, \u003ca href=\"https://www.jstor.org/stable/27642252\"\u003eA short proof of the transcendence of Thue-Morse continued fractions\u003c/a\u003e, The American Mathematical Monthly, Vol. 114, No. 6 (2007), pp. 536-540; \u003ca href=\"http://irma.math.unistra.fr/~bugeaud/travaux/Monthly1.pdf\"\u003ealternative link\u003c/a\u003e.",
				"Jean-Paul Allouche and Jeffrey Shallit, \u003ca href=\"https://doi.org/10.1007/978-1-4471-0551-0_1\"\u003eThe ubiquitous Prouhet-Thue-Morse sequence\u003c/a\u003e, in: C. Ding, T. Helleseth, and H. Niederreiter (eds.), Sequences and their applications, Springer, London, 1999, pp. 1-16; \u003ca href=\"http://ciencias.uis.edu.co/lenguajes/doc/ubiq.pdf\"\u003ealternative link\u003c/a\u003e.",
				"Joerg Arndt, \u003ca href=\"http://www.jjj.de/fxt/#fxtbook\"\u003eMatters Computational (The Fxtbook)\u003c/a\u003e, p.726 ff.",
				"Michel Dekking, \u003ca href=\"https://gallica.bnf.fr/ark:/12148/bpt6k5619975b/f21.image.r\"\u003eTranscendance du nombre de Thue-Morse, Comptes Rendus de l'Academie des Sciences de Paris, Série A, Vol. 285 (1977) A157-A160.",
				"Arturas Dubickas, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2005.07.004\"\u003eOn the distance from a rational power to the nearest integer\u003c/a\u003e, Journal of Number Theory, Volume 117, Issue 1, March 2006, Pages 222-239.",
				"Kurt Mahler, \u003ca href=\"https://doi.org/10.1007/BF01454845\"\u003eArithmetische Eigenschaften der Lösungen einer Klasse von Funktionalgleichungen\u003c/a\u003e, Mathematische Annalen, Vol. 101 (1929), pp. 342-366, \u003ca href=\"http://resolver.sub.uni-goettingen.de/purl?GDZPPN002273217\"\u003ealternative link\u003c/a\u003e.",
				"R. Schroeppel and R. W. Gosper, \u003ca href=\"http://www.inwap.com/pdp10/hbaker/hakmem/series.html#item122\"\u003eHACKMEM #122\u003c/a\u003e (1972).",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Thue-MorseConstant.html\"\u003eThue-Morse Constant\u003c/a\u003e.",
				"\u003ca href=\"/index/Tra#transcendental\"\u003eIndex entries for transcendental numbers\u003c/a\u003e"
			],
			"formula": [
				"Equals Sum_{k\u003e=0} A010060(n)*2^(-(k+1)). [Corrected by _Jianing Song_, Oct 27 2018]",
				"Equals Sum_{k\u003e=1} 2^(-(A000069(k)+1)). - _Jianing Song_, Oct 27 2018",
				"From _Amiram Eldar_, Nov 14 2020: (Start)",
				"Equals 1/2 - (1/4) * A215016.",
				"Equals 1/(3 - 1/A247950). (End)"
			],
			"example": [
				"0.412454033640107597783361368258455283089...",
				"In hexadecimal, .6996966996696996... ."
			],
			"maple": [
				"A014571 := proc()",
				"    local nlim,aold,a ;",
				"    nlim := ilog2(10^Digits) ;",
				"    aold := add( A010060(n)/2^n,n=0..nlim) ;",
				"    a := 0.0 ;",
				"    while abs(a-aold) \u003e abs(a)/10^(Digits-3) do",
				"        aold := a;",
				"        nlim := nlim+200 ;",
				"        a := add( A010060(n)/2^n,n=0..nlim) ;",
				"    od:",
				"    evalf(%/2) ;",
				"end:",
				"A014571() ; # _R. J. Mathar_, Mar 03 2008"
			],
			"mathematica": [
				"digits = 105; t[0] = 0; t[n_?EvenQ] := t[n] = t[n/2]; t[n_?OddQ] := t[n] = 1-t[(n-1)/2]; FromDigits[{t /@ Range[digits*Log[10]/Log[2] // Ceiling], -1}, 2] // RealDigits[#, 10, digits]\u0026 // First (* _Jean-François Alcover_, Feb 20 2014 *)",
				"1/2-1/4*Product[1-2^(-2^k), {k, 0, Infinity}] // N[#, 105]\u0026 // RealDigits // First (* _Jean-François Alcover_, May 15 2014, after _Steven Finch_ *)",
				"(* ThueMorse function needs $Version \u003e= 10.2 *)",
				"P = FromDigits[{ThueMorse /@ Range[0, 400], 0}, 2];",
				"RealDigits[P, 10, 105][[1]] (* _Jean-François Alcover_, Jan 30 2020 *)"
			],
			"program": [
				"(PARI) default(realprecision, 20080); x=0.0; m=67000; for (n=1, m-1, x=x+x; x=x+sum(k=0, length(binary(n))-1, bittest(n, k))%2); x=10*x/2^m; for (n=0, 20000, d=floor(x); x=(x-d)*10; write(\"b014571.txt\", n, \" \", d)); \\\\ _Harry J. Smith_, Apr 25 2009",
				"(PARI) 1/2-prodinf(n=0,1-1.\u003e\u003e2^n)/4 \\\\ _Charles R Greathouse IV_, Jul 31 2012"
			],
			"xref": [
				"Cf. A000069, A001969, A010060, A058631, A215016, A247950."
			],
			"keyword": "nonn,cons",
			"offset": "0,1",
			"author": "_Eric W. Weisstein_",
			"ext": [
				"Corrected and extended by _R. J. Mathar_, Mar 03 2008"
			],
			"references": 16,
			"revision": 61,
			"time": "2020-11-14T09:45:58-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}