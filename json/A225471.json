{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A225471",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 225471,
			"data": "1,3,1,21,10,1,231,131,21,1,3465,2196,446,36,1,65835,45189,10670,1130,55,1,1514205,1105182,290599,36660,2395,78,1,40883535,31354119,8951355,1280419,101325,4501,105,1,1267389585,1012861224,308846124,48644344,4421494,240856,7756,136,1",
			"name": "Triangle read by rows, s_4(n, k) where s_m(n, k) are the Stirling-Frobenius cycle numbers of order m; n \u003e= 0, k \u003e= 0.",
			"comment": [
				"The Stirling-Frobenius cycle numbers are defined in A225470.",
				"Triangle T(n,k), read by rows, given by (3, 4, 7, 8, 11, 12, 15, 16, 19, 20, ... (A014601)) DELTA (1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, May 14 2015"
			],
			"link": [
				"P. Bala, \u003ca href=\"/A143395/a143395.pdf\"\u003eA 3 parameter family of generalized Stirling numbers\u003c/a\u003e.",
				"Peter Luschny, \u003ca href=\"http://www.luschny.de/math/euler/GeneralizedEulerianPolynomials.html\"\u003eGeneralized Eulerian polynomials.\u003c/a\u003e",
				"Peter Luschny, \u003ca href=\"http://www.luschny.de/math/euler/StirlingFrobeniusNumbers.html\"\u003eThe Stirling-Frobenius numbers.\u003c/a\u003e"
			],
			"formula": [
				"For a recurrence see the Sage program.",
				"T(n, 0) ~ A008545; T(n, n) ~ A000012; T(n, n-1) = A014105.",
				"Row sums ~ A047053; alternating row sums ~ A001813.",
				"From _Wolfdieter Lang_, May 29 2017: (Start)",
				"This is the Sheffer triangle (1/(1 - 4*x)^{-3/4}, -(1/4)*log(1-4*x)). See the P. Bala link where this is called exponential Riordan array, and the signed version is denoted by s_{(4,0,3)}.",
				"E.g.f. of row polynomials in the variable x (i.e., of the triangle): (1 - 4*z)^{-(3+x)/4}.",
				"E.g.f. of column k: (1-4*x)^(-3/4)*(-(1/4)*log(1-4*x))^k/k!, k \u003e= 0.",
				"Recurrence for row polynomials R(n, x) = Sum_{k=0..n} T(n, k)*x^k: R(n, x) = (x+3)*R(n-1,x+4), with R(0, x) = 1.",
				"R(n, x) = risefac(4,3;x,n) := Product_{j=0..(n-1)} (x + (3 + 4*j)). (See the P. Bala link, eq. (16) for the signed s_{4,0,3} row polynomials.)",
				"T(n, k) = Sum_{j=0..(n-m)} binomial(n-j, k)* S1p(n, n-j)*3^(n-k-j)*4^j, with S1p(n, m) = A132393(n, m).",
				"T(n, k) = sigma[4,3]^{(n)}_{n-k}, with the elementary symmetric functions sigma[4,3]^{(n)}_m of degree m in the n numbers 3, 7, 11, ..., 3+4*(n-1), with sigma[4,3]^{(n)}_0 := 1. (End)",
				"Boas-Buck type recurrence for column sequence k: T(n, k) = (n!/(n - k)) * Sum_{p=k..n-1} 4^(n-1-p)*(3 + 8*beta(n-1-p))*T(p, k)/p!, for n \u003e k \u003e= 0, with input T(k, k) = 1, and beta(k) = A002208(k+1)/A002209(k+1), beginning with {1/2, 5/12, 3/8, 251/720, ...}. See a comment and references in A286718. - _Wolfdieter Lang_, Aug 11 2017"
			],
			"example": [
				"[n\\k][    0,       1,      2,     3,    4,  5,  6 ]",
				"[0]       1,",
				"[1]       3,       1,",
				"[2]      21,      10,      1,",
				"[3]     231,     131,     21,     1,",
				"[4]    3465,    2196,    446,    36,    1,",
				"[5]   65835,   45189,  10670,  1130,   55,  1,",
				"[6] 1514205, 1105182, 290599, 36660, 2395, 78,  1.",
				"...",
				"From _Wolfdieter Lang_, Aug 11 2017: (Start)",
				"Recurrence: T(4, 2) = T(3, 1) + (4*4 - 1)*T(3, 2) = 131 +15*21 = 446.",
				"Boas-Buck recurrence for column k=2 and n=4: T(4, 2) = (4!/2)*(4*(3+8*(5/12)) *T(2, 2)/2! + 1*(3 + 8*(1/2))*T(3,2)/3!) = (4!/2)*(4*(19/3)/2  + 7*21/3!) =  446.",
				"(End)"
			],
			"mathematica": [
				"T[0, 0] = 1; T[n_, k_] := Sum[Binomial[n - j, k]*Abs[StirlingS1[n, n - j]]* 3^(n - k - j)*4^j, {j, 0, n - k}];",
				"Table[T[n, k], {n, 0, 8}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Jul 30 2018, after _Wolfdieter Lang_ *)"
			],
			"program": [
				"(Sage)",
				"@CachedFunction",
				"def SF_C(n, k, m):",
				"    if k \u003e n or k \u003c 0 : return 0",
				"    if n == 0 and k == 0: return 1",
				"    return SF_C(n-1, k-1, m) + (m*n-1)*SF_C(n-1, k, m)",
				"for n in (0..8): [SF_C(n, k, 4) for k in (0..n)]"
			],
			"xref": [
				"Cf. A132393 (m=1), A028338 (m=2), A225470 (m=3)."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,2",
			"author": "_Peter Luschny_, May 17 2013",
			"references": 7,
			"revision": 32,
			"time": "2018-07-30T12:30:04-04:00",
			"created": "2013-05-21T14:08:38-04:00"
		}
	]
}