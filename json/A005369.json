{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005369",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5369,
			"data": "1,0,1,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0",
			"name": "a(n) = 1 if n is of the form m(m+1), else 0.",
			"comment": [
				"This is essentially the q-expansion of the Jacobi theta function theta_2(q). (In theta_2 one has to ignore the initial factor of 2*q^(1/4). See also A010054.) - _N. J. A. Sloane_, Aug 03 2014",
				"For n \u003e 0, a(n) is the number of partitions of n into two parts such that the larger part is equal to the square of the smaller part. - _Wesley Ivan Hurt_, Dec 23 2020"
			],
			"reference": [
				"S. Wolfram, A New Kind of Science, Wolfram Media, 2002; p. 55."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A005369/b005369.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Robert Price, \u003ca href=\"/A005369/a005369.txt\"\u003eComments on A005369 concerning Elementary Cellular Automata\u003c/a\u003e, Jan 29 2016",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/JacobiThetaFunctions.html\"\u003eJacobi Theta Functions\u003c/a\u003e [From _Franklin T. Adams-Watters_, Jun 29 2009]",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ElementaryCellularAutomaton.html\"\u003eElementary Cellular Automaton\u003c/a\u003e",
				"S. Wolfram, \u003ca href=\"http://wolframscience.com/\"\u003eA New Kind of Science\u003c/a\u003e",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_Elementary_Cellular_Automata\"\u003eIndex to Elementary Cellular Automata\u003c/a\u003e",
				"\u003ca href=\"/index/Ch#char_fns\"\u003eIndex entries for characteristic functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-1/4) * eta(q^4)^2 / eta(q^2) in powers of q.",
				"Euler transform of period 4 sequence [ 0, 1, 0, -1, ...].",
				"G.f.: Product_{k\u003e0} (1 - x^(4*k)) / (1 - x^(4*k-2)) = f(x^2, x^6) where f(, ) is Ramanujan's general theta function.",
				"Given g.f. A(x), then B(q) = (q*A(q^4))^2 satisfies 0 = f(B(q), B(q^2), B(q^4)) where f(u, v, w) = v^3 + 4*v*w^2 - u^2*w. - _Michael Somos_, Apr 13 2005",
				"Given g.f. A(x), then B(q) = q*A(q^4) satisfies 0 = f(B(q), B(q^2), B(q^3), B(q^6)) where f(u1, u2, u3, u6) = u1*u2^2*u6 - u1*u6^3 - u3^3*u2. - _Michael Somos_, Apr 13 2005",
				"a(n) = b(4*n + 1) where b() = A098108() is multiplicative and b(2^e) = 0^e, b(p^e) = (1 + (-1)^e)/2 if p\u003e2. - _Michael Somos_, Jun 06 2005",
				"G.f.: 1/2 x^{-1/4}theta_2(0,x), where theta_2 is a Jacobi theta function. - _Franklin T. Adams-Watters_, Jun 29 2009",
				"a(A002378(n)) = 1; a(A078358(n)) = 0. - _Reinhard Zumkeller_, Jul 05 2014",
				"a(n) = floor(sqrt(n+1)+1/2)-floor(sqrt(n)+1/2). - _Mikael Aaltonen_, Jan 02 2015",
				"a(2*n) = A010054(n).",
				"a(n) = A000729(n)(mod 2). - _John M. Campbell_, Jul 16 2016",
				"For n \u003e 0, a(n) = Sum_{k=1..floor(n/2)} [k^2 = n-k], where [ ] is the Iverson bracket. - _Wesley Ivan Hurt_, Dec 23 2020"
			],
			"example": [
				"G.f. = 1 + x^2 + x^6 + x^12 + x^20 + x^30 + x^42 + x^56 + x^72 + x^90 + ...",
				"G.f. = q + q^9 + q^25 + q^49 + q^81 + q^121 + q^169 + q^225 + q^289 + ..."
			],
			"maple": [
				"A005369 := proc(n)",
				"    if issqr(1+4*n) then",
				"        if type( sqrt(1+4*n)-1,'even') then",
				"            1;",
				"        else",
				"            0;",
				"        end if;",
				"    else",
				"        0;",
				"    end if;",
				"end proc:",
				"seq(A005369(n),n=0..80) ; # _R. J. Mathar_, Feb 22 2021"
			],
			"mathematica": [
				"a005369[n_] := If[IntegerQ[Sqrt[4 # + 1]], 1, 0] \u0026 /@ Range[0, n]; a005369[100] (* _Michael De Vlieger_, Jan 02 2015 *)",
				"a[ n_] := SquaresR[ 1, 4 n + 1] / 2; (* _Michael Somos_, Feb 22 2015 *)",
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 2, 0, x] / (2 x^(1/4)), {x, 0, n}]; (* _Michael Somos_, Feb 22 2015 *)",
				"QP = QPochhammer; s = QP[q^4]^2/QP[q^2] + O[q]^100; CoefficientList[s, q] (* _Jean-François Alcover_, Dec 01 2015, adapted from PARI *)",
				"nmax = 200; CoefficientList[Series[Sum[x^(k*(k + 1)), {k, 0, Sqrt[nmax]}], {x, 0, nmax}], x] (* _Vaclav Kotesovec_, Nov 12 2020 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, issquare(4*n + 1))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^4 + A)^2 / eta(x^2 + A), n))};",
				"(Haskell)",
				"a005369 = a010052 . (+ 1) . (* 4) -- _Reinhard Zumkeller_, Jul 05 2014"
			],
			"xref": [
				"Cf. A002378. Partial sums give A000194.",
				"Cf. A010052, A010054, A016813, A240025."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Additional comments from _Michael Somos_, Apr 29 2003",
				"Erroneous formula removed by _Reinhard Zumkeller_, Jul 05 2014"
			],
			"references": 16,
			"revision": 59,
			"time": "2021-02-22T08:10:45-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}