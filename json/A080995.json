{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A080995",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 80995,
			"data": "1,1,1,0,0,1,0,1,0,0,0,0,1,0,0,1,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0",
			"name": "Characteristic function of generalized pentagonal numbers A001318.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Repeatedly [1,[0,]^2k,1,[0,]^k], k\u003e=0; characteristic function of generalized pentagonal numbers: a(A001318(n))=1, a(A090864(n))=0. - _Reinhard Zumkeller_, Apr 22 2006",
				"Starting with offset 1 with 1's signed (++--++,...), i.e. (1, 1, 0, 0, -1, 0, -1, 0,...); is the INVERTi transform of A000041 starting (1, 2, 3, 5, 7, 11,...). - _Gary W. Adamson_, May 17 2013",
				"Number 9 of the 14 primitive eta-products which are holomorphic modular forms of weight 1/2 listed by D. Zagier on page 30 of \"The 1-2-3 of Modular Forms\". - _Michael Somos_, May 04 2016"
			],
			"reference": [
				"P. A. MacMahon, Combinatory Analysis, Cambridge Univ. Press, London and New York, Vol. 1, 1915 and Vol. 2, 1916; see vol. 2, p. 81, Article 331."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A080995/b080995.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e (terms 0..1001 from T. D. Noe)",
				"S. Cooper and M. D. Hirschhorn, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(03)00079-7\"\u003eResults of Hurwitz type for three squares.\u003c/a\u003e Discrete Math. 274 (2004), no. 1-3, 9-24. See P(q).",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/JacobiThetaFunctions.html\"\u003eJacobi Theta Functions\u003c/a\u003e",
				"\u003ca href=\"/index/Ch#char_fns\"\u003eIndex entries for characteristic functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of phi(-x^3) / chi(-x) in powers of x where phi(), chi() are Ramanujan theta functions. - _Michael Somos_, Sep 14 2007",
				"Expansion of psi(x) - x * psi(x^9) in powers of x^3 where psi() is a Ramanujan theta function. - _Michael Somos_, Sep 14 2007",
				"Expansion of f(x, x^2) in powers of x where f() is Ramanujan's two-variable theta function.",
				"Expansion of q^(-1/24) * eta(q^2) * eta(q^3)^2 / (eta(q) * eta(q^6)) in powers of q.",
				"a(n) = b(24*n + 1) where b() is multiplicative with b(2^e) = b(3^e) = 0^e, b(p^e) = (1 + (-1)^e) / 2 if p\u003e3. - _Michael Somos_, Jun 06 2005",
				"Euler transform of period 6 sequence [ 1, 0, -1, 0, 1, -1, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (144 t)) = 2^(1/2) (t/i)^(1/2) g(t) where q = exp(2 Pi i t) and g() is the g.f. for A089810.",
				"G.f.: Product_{k\u003e0} (1 - x^(3*k)) / (1 - x^k + x^(2*k)). - _Michael Somos_, Jan 26 2008",
				"G.f.: Sum x^(n*(3n+1)/2), n=-inf..inf [the exponents are the pentagonal numbers, A000326].",
				"a(n) = |A010815(n)| = A089806(2*n) = A033683(24*n + 1).",
				"For n \u003e 0, a(n) = b(n) - b(n-1) + c(n) - c(n-1), where b(n) = floor(sqrt(2n/3+1/36)+1/6) (= A180447(n)) and c(n) = floor(sqrt(2n/3+1/36)-1/6) (= A085141(n)). - _Mikael Aaltonen_, Mar 08 2015",
				"a(n) = (-1)^n * A133985(n). - _Michael Somos_, Jul 12 2015",
				"a(n) = A000009(n) (mod 2). - _John M. Campbell_, Jun 29 2016"
			],
			"example": [
				"G.f. = 1 + x + x^2 + x^5 + x^7 + x^12 + x^15 + x^22 + x^26 + x^35 + x^40 + x^51 + ...",
				"G.f. = q + q^25 + q^49 + q^121 + q^169 + q^289 + q^361 + q^529 + q^625 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 0, 0, SeriesCoefficient[ (Series[ EllipticTheta[ 3, Log[y] / (2 I), x^(3/2)], {x, 0, n + Floor@Sqrt[n]}] // Normal // TrigToExp) /. {y -\u003e x^(1/2)}, {x, 0, n}]]; (* _Michael Somos_, Nov 18 2011 *)",
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 4, 0, x^3] / QPochhammer[ x, x^2], {x, 0, n}]; (* _Michael Somos_, Jun 08 2013 *)",
				"a[ n_] := If[ n \u003c 0, 0, Boole[ IntegerQ[ Sqrt[ 24 n + 1]]]]; (* _Michael Somos_, Jun 08 2013 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, abs( polcoeff( eta(x + x * O(x^n)), n)))};",
				"(PARI) {a(n) = issquare( 24*n + 1)}; /* _Michael Somos_, Apr 13 2005 */",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A) * eta(x^3 + A)^2 / (eta(x + A) * eta(x^6 + A)), n))};",
				"(Haskell)",
				"a080995 = a033683 . (+ 1) . (* 24)  -- _Reinhard Zumkeller_, Nov 14 2015"
			],
			"xref": [
				"Cf. A001318 (support), A010815 (absolute values), A033683, A089806.",
				"Cf. A000326, A180447, A085141, A133985."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_Michael Somos_, Feb 27 2003",
			"ext": [
				"Minor edits by _N. J. A. Sloane_, Feb 03 2012"
			],
			"references": 39,
			"revision": 89,
			"time": "2021-03-12T22:24:42-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}