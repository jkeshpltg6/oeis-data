{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A156222",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 156222,
			"data": "-2,-6,9,-18,21,-15,-54,57,-51,375,-162,165,-159,1131,4666413,-486,489,-483,3399,98015025,148865383434975,-1458,1461,-1455,10203,2058376701,46892624598373299,83234757492356072395126701",
			"name": "Triangle T(n, k, q) = q^k*Q(k, n, q), with T(0, 0, q) = -2, where Q(k, n, q) = (1/q)*( -Q(k-1, n, q) + (1+q)*p(q, k-1)^n), Q(k, 0, q) = -q*(1+q)^n, p(q, n) = Product_{j=1..n} ( (1-q^k)/(1-q) ), and q = 2, read by rows.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A156222/b156222.txt\"\u003eRows n = 0..19 of the triangle, flattened\u003c/a\u003e",
				"L. Carlitz, \u003ca href=\"https://projecteuclid.org/journals/duke-mathematical-journal/volume-15/issue-4/q-Bernoulli-numbers-and-polynomials/10.1215/S0012-7094-48-01588-9.short\"\u003eq-Bernoulli numbers and polynomials\u003c/a\u003e Duke Math. J. Volume 15, Number 4 (1948), pp. 987 - 1000."
			],
			"formula": [
				"T(n, k, q) = q^k*Q(k, n, q), with T(0, 0, q) = -2, where Q(k, n, q) = (1/q)*( -Q(k-1, n, q) + (1+q)*p(q, k-1)^n), Q(k, 0, q) = -q*(1+q)^n, p(q, n) = Product_{j=1..n} ( (1-q^k)/(1-q) ), and q = 2."
			],
			"example": [
				"Triangle begins as:",
				"    -2;",
				"    -6,   9;",
				"   -18,  21,  -15;",
				"   -54,  57,  -51,  375;",
				"  -162, 165, -159, 1131,  4666413;",
				"  -486, 489, -483, 3399, 98015025, 148865383434975;"
			],
			"mathematica": [
				"Q[k_, n_, q_]:= Q[k, n, q]= If[n==0, 1, If[k==0, -q*(1+q)^n, (1/q)*( -Q[k-1, n, q] + (1+q)*(-1)^(n*(k-1))*QPochhammer[q,q,k-1]^n ) ]];",
				"T[n_, k_, q_]:= If[n==0, -2, 2^k*Q[k, n, q]];",
				"Table[T[n, k, 2], {n,0,10}, {k,0,n}]//Flatten (* modified by _G. C. Greubel_, Jan 01 2022 *)"
			],
			"program": [
				"(Sage)",
				"from sage.combinat.q_analogues import q_pochhammer",
				"@CachedFunction",
				"def Q(k,n,q):",
				"    if (n==0): return 1",
				"    elif (k==0): return -q*(q+1)^n",
				"    else: return (1/q)*(-Q(k-1,n,q) + (1+q)*((-1)^(k-1)*q_pochhammer(k-1,q,q))^n)",
				"def T(n,k,q): return -2 if (n==0) else q^k*Q(k,n,q)",
				"flatten([[T(n,k,2) for k in (0..n)] for n in (0..10)]) # _G. C. Greubel_, Jan 01 2022"
			],
			"xref": [
				"Cf. A156220."
			],
			"keyword": "sign,tabl,changed",
			"offset": "0,1",
			"author": "_Roger L. Bagula_, Feb 06 2009",
			"ext": [
				"Edited by _G. C. Greubel_, Jan 01 2022"
			],
			"references": 3,
			"revision": 12,
			"time": "2022-01-03T11:13:13-05:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}