{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A220024",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 220024,
			"data": "1,1,500,500,250,4,125,100,500,250,1,500,500,500,250,2,125,500,100,500,1,500,500,500,50,2,25,500,500,500,1,250,100,500,250,4,125,500,500,250,1,250,500,100,250,4,125,500,500,50,1,100,500,500,250,2,125,20",
			"name": "The period with which the powers of n repeat mod 10000.",
			"comment": [
				"a(n) will always be a divisor of Phi(10000) = 4000.",
				"This sequence is periodic with a period of 10000 because n^i mod 10000 = (n + 10000)^i mod 10000.",
				"For the odd numbers n ending in {1, 3, 7, 9} which are coprime to 10, we can expect the powers of n mod 10000 to loop back to 1, with the value of n^a(n) mod 10000 = 1, but for the other numbers n that are not coprime to 10, they do not loop back to 1.",
				"For the even numbers n ending in {2, 4, 6, 8}, n^a(n) mod 10000 = 9376.",
				"For the numbers n ending in 5, n^(4*i) mod 10000 = 625, for all i \u003e= 1.",
				"For the numbers n ending in 0, n^i mod 10000 = 0, for all i \u003e= 4."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A220024/b220024.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e"
			],
			"example": [
				"a(2) = 500 since 2^i mod 10000 = 2^(i + 500) mod 10000, for all i \u003e= 4.",
				"a(3) = 500 since 3^i mod 10000 = 3^(i + 500) mod 10000, for all i \u003e= 0.",
				"But a(7) = 100 since 7^i mod 10000 = 7^(i + 100) mod 10000, for all i \u003e= 0."
			],
			"mathematica": [
				"Flatten[Table[s = Table[PowerMod[n, e, 10000], {e, 2, 10000}]; Union[Differences[Position[s, s[[3]]]]], {n, 0, 40}]] (* _Vincenzo Librandi_, Jan 26 2013 *)",
				"Table[Length[FindTransientRepeat[PowerMod[n,Range[3000],10000],3] [[2]]],{n,0,60}] (* Requires Mathematica version 10 or later *) (* _Harvey P. Dale_, Nov 08 2016 *)"
			],
			"program": [
				"(PARI) k=10000; for(n=0, 100, x=(n^4)%k; y=(n^5)%k; z=1; while(x!=y, x=(x*n)%k; y=(y*n*n)%k; z++); print1(z\", \"))"
			],
			"xref": [
				"Cf. A173635 (period with which the powers of n repeat mod 10).",
				"Cf. A220022 (period with which the powers of n repeat mod 100)."
			],
			"keyword": "nonn,base",
			"offset": "0,3",
			"author": "_V. Raman_, Dec 15 2012",
			"references": 1,
			"revision": 22,
			"time": "2017-07-25T02:29:56-04:00",
			"created": "2013-01-02T13:41:19-05:00"
		}
	]
}