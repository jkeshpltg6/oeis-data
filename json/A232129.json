{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A232129",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 232129,
			"data": "1979339339,29399999,37337999,4391339,59393339,6733997,73939133,839,9719,103997939939,113,12791333,13999133,149399,15797,1637,17333,1811993,1979339339,0,21139,2273993,23399339,24179399,2579939,2699393,27191939,2837,29399999,3079,31379,0,331999799,3491333,35393999",
			"name": "Largest prime that can be obtained from n by successively appending digits to the right with the constraint that each of the numbers obtained that way must be prime; a(n)=0 if there is no such prime at all.",
			"comment": [
				"See A232128 for the number of steps required to reach a(n), equal to the length of a(n) minus the length of n. See A232126 for a variant \"working backwards\", where truncation is considered."
			],
			"link": [
				"Michael S. Branicky, \u003ca href=\"/A232129/b232129.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Archimedes' Lab, \u003ca href=\"http://www.archimedes-lab.org/numbers/Num24_69.html\"\u003eWhat's Special About This Number?\u003c/a\u003e, section about 43."
			],
			"example": [
				"Starting with 8, one can get the primes 83 and 89 which is larger, but 83 allows one further extension to 839 while 89 does not (no prime in the range 890..899. No further extension is possible, since there are no primes in the range 8390,...,8399. Therefore a(8)=839 and A232128(8)=2.",
				"a(20)=a(42)=0 since no prime can be obtained by appending one digit to 20 or 42."
			],
			"program": [
				"(PARI) {A232129(n)=local(t(p)=my(m,r=[0,p]);forstep(d=1,9,2,isprime(p*10+d)\u0026\u0026(m=t(10*p+d)+[1,0])[1]\u003e=r[1]\u0026\u0026r=m);r);n\u003c(n=t(n))[2]\u0026\u0026return(n[2])}",
				"(Python)",
				"from sympy import isprime, nextprime",
				"def a(n):",
				"    while True:",
				"        extends, reach, maxp = -1, {n}, 0",
				"        while len(reach) \u003e 0:",
				"            candidates = (int(str(e)+d) for d in \"1379\" for e in reach)",
				"            reach1 = set(filter(isprime, candidates))",
				"            extends, reach, maxp = extends+1, reach1, max({maxp}|reach1)",
				"        return maxp",
				"print([a(n) for n in range(1, 36)]) # _Michael S. Branicky_, Sep 07 2021"
			],
			"xref": [
				"Cf. A232128, A232127, A232126, A232125."
			],
			"keyword": "nonn,base",
			"offset": "1,1",
			"author": "_M. F. Hasler_, Nov 19 2013",
			"references": 3,
			"revision": 14,
			"time": "2021-09-07T09:28:45-04:00",
			"created": "2013-11-23T04:18:57-05:00"
		}
	]
}