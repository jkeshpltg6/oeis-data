{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005864",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5864,
			"id": "M1111",
			"data": "1,1,1,2,2,4,8,16,20,40,72,144,256,512,1024,2048",
			"name": "The coding-theoretic function A(n,4).",
			"comment": [
				"Since A(n,3) = A(n+1,4), A(n,3) gives essentially the same sequence.",
				"The next term a(17) is in the range 2816-3276.",
				"Let T_n be the set of SDS-maps of sequential dynamical systems defined over the complete graph K_n in which all vertices have the same vertex function (defined using a set of two possible vertex states). Then a(n) is the maximum number of period-2 orbits that a function in T_n can have. - _Colin Defant_, Sep 15 2015",
				"Since the n-halved cube graph is isomorphic to (or, if you prefer, defined as) the graph with binary sequences of length n-1 as nodes and edges between pairs of sequences that differ in at most two positions, the independence number of the n-halved cube graph is A(n-1,3) = a(n). - _Pontus von Brömssen_, Dec 12 2018"
			],
			"reference": [
				"J. H. Conway and N. J. A. Sloane, \"Sphere Packings, Lattices and Groups\", Springer-Verlag, p. 248.",
				"F. J. MacWilliams and N. J. A. Sloane, The Theory of Error-Correcting Codes, Elsevier-North Holland, 1978, p. 674.",
				"A. M. Romanov, New binary codes of minimal distance 3, Problemy Peredachi Informatsii, 19 (1983) 101-102.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"A. E. Brouwer, \u003ca href=\"http://www.win.tue.nl/~aeb/codes/binary-1.html\"\u003eTables of general binary codes\u003c/a\u003e",
				"A. E. Brouwer, J. B. Shearer, N. J. A. Sloane and W. D. Smith, \u003ca href=\"http://dx.doi.org/10.1109/18.59932\"\u003eNew table of constant weight codes\u003c/a\u003e, IEEE Trans. Info. Theory 36 (1990), 1334-1380.",
				"Colin Defant, \u003ca href=\"http://arxiv.org/abs/1509.03907\"\u003eBinary Codes and Period-2 Orbits of Sequential Dynamical Systems\u003c/a\u003e, arXiv:1509.03907 [math.CO], 2015.",
				"Moshe Milshtein, \u003ca href=\"http://dx.doi.org/10.1016/j.ipl.2015.07.001\"\u003eA new binary code of length 16 and minimum distance 3\u003c/a\u003e, Information Processing Letters 115.12 (2015): 975-976.",
				"Patric R. J. Östergård (patric.ostergard(AT)hut.fi), T. Baicheva and E. Kolev, \u003ca href=\"http://saturn.hut.fi/~pat/\"\u003eOptimal binary one-error-correcting codes of length 10 have 72 codewords\u003c/a\u003e, IEEE Trans. Inform. Theory, 45 (1999), 1229-1231.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Error-CorrectingCode.html\"\u003eError-Correcting Code\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HalvedCubeGraph.html\"\u003eHalved Cube Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/IndependenceNumber.html\"\u003eIndependence Number\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Halved_cube_graph\"\u003eHalved cube graph\u003c/a\u003e",
				"\u003ca href=\"/index/Aa#And\"\u003eIndex entries for sequences related to A(n,d)\u003c/a\u003e"
			],
			"xref": [
				"Cf. A005865: A(n,6) ~ A(n,5), A005866: A(n,8) ~ A(n,7).",
				"Cf. A001839: A(n,4,3), A001843: A(n,4,4), A169763: A(n,4,5)."
			],
			"keyword": "nonn,hard,nice,more",
			"offset": "1,4",
			"author": "_N. J. A. Sloane_",
			"references": 2,
			"revision": 80,
			"time": "2020-02-02T22:23:49-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}