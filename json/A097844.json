{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097844",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97844,
			"data": "1,171,29240,4999869,854948359,146191169520,24997835039561,4274483600595411,730911697866775720,124981625851618052709,21371127108928820237519,3654337754000976642563040,624870384807058077058042321,106849181464252930200282673851",
			"name": "Chebyshev polynomials S(n,171).",
			"comment": [
				"Used for all positive integer solutions of Pell equation x^2 - 173*y^2 = -4. See A097845 with A098244."
			],
			"link": [
				"Indranil Ghosh, \u003ca href=\"/A097844/b097844.txt\"\u003eTable of n, a(n) for n = 0..446\u003c/a\u003e",
				"Hacène Belbachir, Soumeya Merwa Tebtoub, and László Németh, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL23/Nemeth/nemeth7.html\"\u003eEllipse Chains and Associated Sequences\u003c/a\u003e, J. Int. Seq., Vol. 23 (2020), Article 20.8.5.",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (171,-1)."
			],
			"formula": [
				"a(n) = S(n, 171) = U(n, 171/2) = S(2*n+1, sqrt(173))/sqrt(173) with S(n, x) = U(n, x/2) Chebyshev's polynomials of the second kind, A049310. S(-1, x) = 0 = U(-1, x).",
				"a(n) = 171*a(n-1) - a(n-2), n \u003e= 1, a(-1)=0, a(0)=1, a(1)=171.",
				"a(n) = (ap^(n+1) - am^(n+1))/(ap-am) with ap = (171+13*sqrt(173))/2 and am = (171-13*sqrt(173))/2 = 1/ap.",
				"G.f.: 1/(1-171*x+x^2)."
			],
			"mathematica": [
				"CoefficientList[Series[1/(1-171x+x^2),{x,0,30}],x] (* or *) LinearRecurrence[{171,-1},{1,171},30] (* _Harvey P. Dale_, Mar 21 2013 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^30)); Vec(1/(1-171*x+x^2)) \\\\ _G. C. Greubel_, Jan 14 2019",
				"(MAGMA) m:=30; R\u003cx\u003e:=PowerSeriesRing(Integers(), m); Coefficients(R!( 1/(1-171*x+x^2) )); // _G. C. Greubel_, Jan 14 2019",
				"(Sage) (1/(1-171*x+x^2)).series(x, 30).coefficients(x, sparse=False) # _G. C. Greubel_, Jan 14 2019",
				"(GAP) a:=[1,171];; for n in [3..30] do a[n]:=171*a[n-1]-a[n-2]; od; a; # _G. C. Greubel_, Jan 14 2019"
			],
			"xref": [
				"Cf. A097845, A098244."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Sep 10 2004",
			"references": 4,
			"revision": 28,
			"time": "2021-01-01T03:32:47-05:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}