{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A342989",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 342989,
			"data": "1,4,4,10,39,10,20,190,190,20,35,651,1568,651,35,56,1792,8344,8344,1792,56,84,4242,33580,64667,33580,4242,84,120,8988,111100,361884,361884,111100,8988,120,165,17490,317680,1607125,2713561,1607125,317680,17490,165",
			"name": "Triangle read by rows: T(n,k) is the number of nonseparable rooted toroidal maps with n edges and k faces, n \u003e= 2, k = 1..n-1.",
			"comment": [
				"The number of vertices is n - k.",
				"Column k is a polynomial of degree 3*k. This is because adding a face can increase the number of vertices whose degree is greater than two by at most two."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A342989/b342989.txt\"\u003eTable of n, a(n) for n = 2..1276\u003c/a\u003e (first 50 rows)",
				"T. R. S. Walsh and A. B. Lehman, \u003ca href=\"http://dx.doi.org/10.1016/0095-8956(75)90050-7\"\u003eCounting rooted maps by genus. III: Nonseparable maps\u003c/a\u003e, J. Combinatorial Theory Ser. B 18 (1975), 222-259, Table II."
			],
			"formula": [
				"T(n,n-k) = T(n,k)."
			],
			"example": [
				"Triangle begins:",
				"    1;",
				"    4,    4;",
				"   10,   39,     10;",
				"   20,  190,    190,     20;",
				"   35,  651,   1568,    651,     35;",
				"   56, 1792,   8344,   8344,   1792,     56;",
				"   84, 4242,  33580,  64667,  33580,   4242,   84;",
				"  120, 8988, 111100, 361884, 361884, 111100, 8988, 120;",
				"  ..."
			],
			"program": [
				"(PARI)",
				"MQ(n,g,x=1)={ \\\\ after Quadric in A269921.",
				"  my(Q=matrix(n+1,g+1)); Q[1,1]=x;",
				"  for(n=1, n, for(g=0, min(n\\2,g),",
				"     my(t = (1+x)*(2*n-1)/3 * Q[n, 1+g]",
				"       + if(g \u0026\u0026 n\u003e1, (2*n-3)*(2*n-2)*(2*n-1)/12 * Q[n-1, g])",
				"       + sum(k = 1, n-1, sum(i = 0, g, (2*k-1) * (2*(n-k)-1) * Q[k, 1+i] * Q[n-k, 1+g-i]))/2);",
				"     Q[1+n, 1+g] = t * 6/(n+1); ));",
				"  Q",
				"}",
				"F(n,m,y,z)={my(Q=MQ(n,m,z)); sum(n=0, n, x^n*Ser(Q[1+n,]/z, y)) + O(x*x^n)}",
				"H(n,g=1)={my(p=F(n,g,'y,'z), v=Vec(polcoef(subst(p, x, serreverse(x*p^2)), g, 'y))); vector(#v, n, Vecrev(v[n], n))}",
				"{ my(T=H(10)); for(n=1, #T, print(T[n])) }"
			],
			"xref": [
				"Columns 1..4 are A000292, A006408, A006409, A006410.",
				"Row sums are A343089.",
				"Cf. A082680 (planar case), A269921 (rooted toroidal maps), A343090, A343092."
			],
			"keyword": "nonn,tabl,changed",
			"offset": "2,2",
			"author": "_Andrew Howroyd_, Apr 04 2021",
			"references": 7,
			"revision": 16,
			"time": "2022-01-02T16:09:19-05:00",
			"created": "2021-04-05T00:07:30-04:00"
		}
	]
}