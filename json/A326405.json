{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A326405",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 326405,
			"data": "3,-1,-1,3,-1,2,-1,4,4,0,-1,4,-1,2,0,3,-1,3,-1,1,0,2,-1,3,3,1,1,0,-1,3,-1,2,2,1,2,0,-1,3,3,2,-1,2,-1,2,0,2,-1,2,3,2,3,2,-1,1,0,1,2,2,-1,3,-1,2,2,1,1,0,-1,1,1,3,-1,3,-1,1,2,1,2,0",
			"name": "Minesweeper sequence of positive integers arranged on a 2D grid along ascending antidiagonals.",
			"comment": [
				"Map the positive integers on a 2D grid starting with 1 in top left corner and continue along increasing antidiagonals.",
				"Replace each prime with -1 and each nonprime with the number of primes in adjacent grid cells around it.",
				"If n is the original number, a(n) is the number that replaces it.",
				"This sequence treats prime numbers as \"mines\" and fills gaps according to the rules of the classic Minesweeper game.",
				"a(n) \u003c 5 (conjectured).",
				"Set of n such that a(n) = 4 is unbounded (conjectured)."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A326405/b326405.txt\"\u003eTable of n, a(n) for n = 1..11325\u003c/a\u003e (150 antidiagonals).",
				"Michael De Vlieger, \u003ca href=\"/A326405/a326405.png\"\u003eMinesweeper-style graph\u003c/a\u003e read along original mapping, replacing -1 with a \"mine\", and 0 with blank space.",
				"Michael De Vlieger, \u003ca href=\"/A326405/a326405_1.png\"\u003eSquare plot of a million terms\u003c/a\u003e read along original mapping, with black indicating a prime and levels of gray commensurate to a(n).",
				"Witold Tatkiewicz, \u003ca href=\"https://pastebin.com/pTPNEuzd\"\u003elink for Java program\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Minesweeper_(video_game)\"\u003eMinesweeper game\u003c/a\u003e"
			],
			"example": [
				"Consider positive integers distributed on the plane along antidiagonals:",
				"   1  2  4  7 11 16 ...",
				"   3  5  8 12 17 ...",
				"   6  9 13 18 ...",
				"  10 14 19 ...",
				"  15 20 ...",
				"  21 ...",
				"  ...",
				"1 is not prime and in its adjacent grid cells there are 3 primes: 2, 3 and 5. Therefore a(1) = 3.",
				"2 is prime, therefore a(2) = -1.",
				"8 is not prime and in adjacent grid cells there are 4 primes: 2, 5, 7 and 13. Therefore a(8) = 4.",
				"From _Michael De Vlieger_, Oct 01 2019: (Start)",
				"Replacing n with a(n) in the plane described above, and using \".\" for a(n) = 0 and \"*\" for negative a(n), we produce a graph resembling Minesweeper, where the mines are situated at prime n:",
				"  3  *  3  *  *  3  2  *  *  2  1  * ...",
				"  *  *  4  4  *  *  3  3  *  2  1  2",
				"  2  4  *  3  3  *  3  2  2  1  1  1",
				"  .  2  *  3  2  2  3  *  3  1  1  *",
				"  .  1  1  2  *  2  3  *  *  2  1  1",
				"  .  1  1  2  3  *  3  3  *  3  1  .",
				"  .  2  *  2  2  *  3  2  3  *  2  1",
				"  .  2  *  2  1  1  2  *  2  1  3  *",
				"  .  1  1  2  1  1  1  2  3  2  3  *",
				"  .  1  1  2  *  2  1  1  *  *  2  2",
				"  .  2  *  3  2  *  1  1  2  2  1  1",
				"  .  2  *  3  2  2  1  1  1  1  .  1",
				"   ...  (End)"
			],
			"mathematica": [
				"Block[{n = 12, s}, s = ArrayPad[Array[1 + PolygonalNumber[#1 + #2 - 1] - #2 \u0026, {# + 1, # + 1}], 1] \u0026@ n; Table[If[PrimeQ@ m, -1, Count[#, _?PrimeQ] \u0026@ Union@ Map[s[[#1, #2]] \u0026 @@ # \u0026, Join @@ Array[FirstPosition[s, m] + {##} - 2 \u0026, {3, 3}]]], {m, PolygonalNumber@ n}]] (* _Michael De Vlieger_, Sep 30 2019 *)"
			],
			"program": [
				"(Java) See Links section."
			],
			"xref": [
				"Different arrangements of integers:",
				"Cf. A326406 - triangle maze,",
				"Cf. A326407 - square mapping,",
				"Cf. A326408 - square maze,",
				"Cf. A326409 - Hamiltonian path,",
				"Cf. A326410 - Ulam's spiral."
			],
			"keyword": "sign,tabl",
			"offset": "1,1",
			"author": "_Witold Tatkiewicz_, Sep 26 2019",
			"references": 7,
			"revision": 41,
			"time": "2020-02-28T02:07:34-05:00",
			"created": "2019-10-04T08:14:27-04:00"
		}
	]
}