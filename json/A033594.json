{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A033594",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 33594,
			"data": "-1,0,15,80,231,504,935,1560,2415,3536,4959,6720,8855,11400,14391,17864,21855,26400,31535,37296,43719,50840,58695,67320,76751,87024,98175,110240,123255,137256,152279,168360",
			"name": "a(n) = (n-1)*(2*n-1)*(3*n-1).",
			"comment": [
				"The sequence of n such that n is prime and (2*n+1) is prime is the sequence of Sophie Germain primes A005384 and the subsequence of those for which in addition (3*n+2) is prime is A067256. - _Jonathan Vos Post_, Dec 15 2004"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A033594/b033594.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-6,4,-1)."
			],
			"formula": [
				"a(n)*A016921(n) + 1 = A051866(n)^2. - _Bruno Berselli_, May 23 2011",
				"a(n) = 4*a(n-1) - 6*a(n-2) + 4*a(n-3) - a(n-4), with a(0)=-1, a(1)=0, a(2)=15, a(3)=80. - _Harvey P. Dale_, Aug 23 2012",
				"G.f.: (-1 +4*x +9*x^2 +24*x^3)/(1-x)^4. - _R. J. Mathar_, Feb 06 2017",
				"E.g.f.: (-1 + x + 7*x^2 + 6*x^3)*exp(x). - _G. C. Greubel_, Mar 05 2020",
				"From _Amiram Eldar_, Jan 03 2021: (Start)",
				"Sum_{n\u003e=2} 1/a(n) = (7 - sqrt(3)*Pi - 16*log(2) + 9*log(3))/4.",
				"Sum_{n\u003e=2} (-1)^n/a(n) = Pi - 7/4 - sqrt(3)*Pi/2 + 2*log(2). (End)"
			],
			"maple": [
				"A033594:=n-\u003e(n-1)*(2*n-1)*(3*n-1); seq(A033594(n), n=0..40); # _Wesley Ivan Hurt_, Feb 24 2014"
			],
			"mathematica": [
				"Table[(n-1)*(2*n-1)*(3*n-1),{n,0,40}] (* _Vladimir Joseph Stephan Orlovsky_, Apr 28 2010 *)",
				"LinearRecurrence[{4,-6,4,-1},{-1,0,15,80},40] (* _Harvey P. Dale_, Aug 23 2012 *)"
			],
			"program": [
				"(MAGMA) [(n-1)*(2*n-1)*(3*n-1): n in [0..40]]; // _Vincenzo Librandi_, May 24 2011",
				"(PARI) vector(41, n, my(m=n-1); (m-1)*(2*m-1)*(3*m-1) ) \\\\ _G. C. Greubel_, Mar 05 2020",
				"(Sage) [-1]+[n^3*rising_factorial((n-1)/n, 3) for n in (1..40)] # _G. C. Greubel_, Mar 05 2020",
				"(GAP) List([0..40], n-\u003e (n-1)*(2*n-1)*(3*n-1) ); # _G. C. Greubel_, Mar 05 2020"
			],
			"xref": [
				"Cf. A005384, A016921, A067256."
			],
			"keyword": "sign,easy",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"references": 4,
			"revision": 38,
			"time": "2021-01-03T02:49:09-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}