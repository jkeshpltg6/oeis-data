{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A208344",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 208344,
			"data": "1,1,1,1,1,3,1,1,4,7,1,1,5,10,17,1,1,6,13,27,41,1,1,7,16,38,71,99,1,1,8,19,50,106,186,239,1,1,9,22,63,146,294,484,577,1,1,10,25,77,191,424,806,1253,1393,1,1,11,28,92,241,577,1212,2191,3229,3363,1,1,12",
			"name": "Triangle of coefficients of polynomials u(n,x) jointly generated with A208345; see the Formula section.",
			"comment": [
				"Row sums, u(n,1):  (1,2,5,13,...), odd-indexed Fibonacci numbers.",
				"Row sums, v(n,1):  (1,3,8,21,...), even-indexed Fibonacci numbers.",
				"Subtriangle of the triangle given by (1, 0, -1, 1, 0, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (0, 1, 2, -1, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Apr 09 2012"
			],
			"formula": [
				"u(n,x) = u(n-1,x) + x*v(n-1,x),",
				"v(n,x) = x*u(n-1,x) + 2x*v(n-1,x),",
				"where u(1,x)=1, v(1,x)=1.",
				"From _Philippe Deléham_, Apr 09 2012: (Start)",
				"As DELTA-triangle T(n,k) with 0 \u003c= k \u003c= n:",
				"G.f.: (1-2*y*x+y*x^2-y^2*x^2)/(1-x-2*y*x+2*y*x^2-y^2*x^2).",
				"T(n,k) = T(n-1,k) + 2*T(n-1,k-1) -2*T(n-2,k-1) + T(n-2,k-2), T(0,0) = T(1,0) = T(2,0) = T(2,1) = 1, T(1,1) = T(2,2) = 0 and T(n,k) = 0 if k \u003c 0 or if k \u003e n. (End)"
			],
			"example": [
				"First five rows:",
				"  1;",
				"  1,  1;",
				"  1,  1,  3;",
				"  1,  1,  4,  7;",
				"  1,  1,  5, 10, 17;",
				"First five polynomials u(n,x):",
				"  1",
				"  1 + x",
				"  1 + x + 3x^2",
				"  1 + x + 4x^2 + 7x^3",
				"  1 + x + 5x^2 + 10x^3 + 17x^4.",
				"From _Philippe Deléham_, Apr 09 2012: (Start)",
				"(1, 0, -1, 1, 0, 0, ...) DELTA (0, 1, 2, -1, 0, 0, ...) begins:",
				"  1;",
				"  1,  0;",
				"  1,  1,  0;",
				"  1,  1,  3,  0;",
				"  1,  1,  4,  7,  0;",
				"  1,  1,  5, 10, 17,  0;",
				"  1,  1,  6, 13, 27, 41,  0; (End)"
			],
			"mathematica": [
				"u[1, x_] := 1; v[1, x_] := 1; z = 13;",
				"u[n_, x_] := u[n - 1, x] + x*v[n - 1, x];",
				"v[n_, x_] := x*u[n - 1, x] + 2 x*v[n - 1, x];",
				"Table[Expand[u[n, x]], {n, 1, z/2}]",
				"Table[Expand[v[n, x]], {n, 1, z/2}]",
				"cu = Table[CoefficientList[u[n, x], x], {n, 1, z}];",
				"TableForm[cu]",
				"Flatten[%]    (* A208344 *)",
				"Table[Expand[v[n, x]], {n, 1, z}]",
				"cv = Table[CoefficientList[v[n, x], x], {n, 1, z}];",
				"TableForm[cv]",
				"Flatten[%]    (* A208345 *)",
				"Table[u[n, x] /. x -\u003e 1, {n, 1, z}]",
				"Table[v[n, x] /. x -\u003e 1, {n, 1, z}]"
			],
			"xref": [
				"Cf. A208345."
			],
			"keyword": "nonn,tabl",
			"offset": "1,6",
			"author": "_Clark Kimberling_, Feb 25 2012",
			"ext": [
				"a(69) corrected by _Georg Fischer_, Sep 03 2021"
			],
			"references": 3,
			"revision": 17,
			"time": "2021-09-03T13:57:52-04:00",
			"created": "2012-02-25T18:35:46-05:00"
		}
	]
}