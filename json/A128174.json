{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A128174",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 128174,
			"data": "1,0,1,1,0,1,0,1,0,1,1,0,1,0,1,0,1,0,1,0,1,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1",
			"name": "Transform, (1,0,1,...) in every column.",
			"comment": [
				"Inverse of the triangle = a tridiagonal matrix with (1,1,1,...) in the superdiagonal, (0,0,0,...) in the main diagonal and (-1,-1,-1,...) in the subdiagonal.",
				"Riordan array (1/(1-x^2), x) with inverse (1-x^2,x). - _Paul Barry_, Sep 10 2008",
				"The position of 1's in this sequence is equivalent to A246705, and the position of 0's is equivalent to A246706. - _Bernard Schott_, Jun 05 2019"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A128174/b128174.txt\"\u003eTable of n, a(n) for the first 100 rows, flattened\u003c/a\u003e"
			],
			"formula": [
				"A lower triangular matrix transform, (1, 0, 1, ...) in every column; n terms of (1, 0, 1, ...) in odd rows; n terms of (0, 1, 0, ...) in even rows.",
				"T(n,k) = [k\u003c=n]*(1+(-1)^(n-k))/2. - _Paul Barry_, Sep 10 2008",
				"With offset n=1, k=0: Sum_{k=0..n} {T(n,k)*x^k} = A000035(n), A004526(n+1), A000975(n), A033113(n), A033114(n), A033115(n), A033116(n), A033117(n), A033118(n), A033119(n), A056830(n+1) for x=0,1,2,3,4,5,6,7,8,9,10 respectively. - _Philippe Deléham_, Oct 17 2011",
				"T(n+1,1) = 1 - T(n,1); T(n+1,k) = T(n,k-1), 1 \u003c k \u003c= n+1. - _Reinhard Zumkeller_, Aug 01 2014"
			],
			"example": [
				"First few rows of the triangle are:",
				"  1;",
				"  0, 1;",
				"  1, 0, 1;",
				"  0, 1, 0, 1;",
				"  1, 0, 1, 0, 1; ..."
			],
			"maple": [
				"A128174 := proc(n,k)",
				"    if k \u003e n or k \u003c 1 then",
				"        0;",
				"    else",
				"        modp(k+n+1,2) ;",
				"    end if;",
				"end proc: # _R. J. Mathar_, Aug 06 2016"
			],
			"mathematica": [
				"a128174[r_] := Table[If[EvenQ[n+k], 1, 0], {n, 1, r}, {k, 1, n}]",
				"TableForm[a128174[5]] (* triangle *)",
				"Flatten[a128174[10]] (* data *) (* _Hartmut F. W. Hoft_, Mar 15 2017 *)",
				"Table[(1+(-1)^(n-k))/2, {n,1,12}, {k,1,n}]//Flatten (* _G. C. Greubel_, Sep 26 2017 *)"
			],
			"program": [
				"(Haskell)",
				"a128174 n k = a128174_tabl !! (n-1) !! (k-1)",
				"a128174_row n = a128174_tabl !! (n-1)",
				"a128174_tabl = iterate (\\xs@(x:_) -\u003e (1 - x) : xs) [1]",
				"-- _Reinhard Zumkeller_, Aug 01 2014",
				"(PARI) for(n=1,12, for(k=1,n, print1((1+(-1)^(n-k))/2, \", \"))) \\\\ _G. C. Greubel_, Sep 26 2017",
				"(MAGMA) [[(1+(-1)^(n-k))/2: k in [1..n]]: n in [1..12]]; // _G. C. Greubel_, Jun 05 2019",
				"(Sage) [[(1+(-1)^(n-k))/2 for k in (1..n)] for n in (1..12)] # _G. C. Greubel_, Jun 05 2019"
			],
			"xref": [
				"Cf. A004526 (row sums)."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "1,1",
			"author": "_Gary W. Adamson_, Feb 17 2007",
			"references": 57,
			"revision": 35,
			"time": "2019-06-10T23:26:10-04:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}