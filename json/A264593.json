{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A264593",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 264593,
			"data": "1,0,0,0,0,0,1,1,1,1,1,1,1,1,2,2,3,3,4,4,5,5,6,6,8,8,10,11,13,14,17,18,21,23,26,28,33,35,40,44,50,54,62,67,76,83,93,101,114,123,138,150,167,181,202,219,243,264,292,317,351,380,419,455,500,542,596,645,707,766,838,907,992,1072",
			"name": "Let G[1](q) denote the g.f. for A003114 and G[2](q) the g.f. for A003106 (the two Rogers-Ramanujan identities). For i\u003e=3, let G[i](q) = (G[i-1](q)-G[i-2](q))/q^(i-2). Sequence gives coefficients of G[6](q).",
			"comment": [
				"It is conjectured that G[i](q) = 1 + O(q^i) for all i.",
				"For n \u003e=1 a(n) gives the number of partitions of n without parts 1, 2, 3, 4, and 5, and the parts differ by at least 2. For the proof see a comment given in A264592. - _Wolfdieter Lang_, Nov 10 2016"
			],
			"link": [
				"Vaclav Kotesovec, \u003ca href=\"/A264593/b264593.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"George E. Andrews; R. J. Baxter, \u003ca href=\"http://www.computing-wisdom.com/jstor/rogers-ramanujan.pdf\"\u003eA motivated proof of the Rogers-Ramanujan identities\u003c/a\u003e, Amer. Math. Monthly 96 (1989), no. 5, 401-409.",
				"Shashank Kanade, \u003ca href=\"http://www.math.rutgers.edu/~skanade/SK-Defense-Handout.pdf\"\u003eSome results on the representation theory of vertex operator algebras and integer partition identities\u003c/a\u003e, PhD Handout, Math. Dept., Rutgers University, April 2015.",
				"Shashank Kanade, \u003ca href=\"http://dx.doi.org/doi:10.7282/T3TX3H7B\"\u003eSome results on the representation theory of vertex operator algebras and integer partition identities\u003c/a\u003e, PhD Dissertation, Math. Dept., Rutgers University, April 2015."
			],
			"formula": [
				"From _Wolfdieter Lang_, Nov 10 2016: (Start)",
				"G.f.: G[6](q) = GII_2(q) = Sum_{m\u003e=0} q^(m*(m+5)) / Product_{j =1..m} (1 - q^j).",
				"See Andrews and Baxter [A-B], eq. (5.1) for i=6.",
				"G.f.: Sum_{m=0} ((-1)^m*(1 - q^(m+1))*(1 - q^(m+2))*(1 - q^(m+3))*(1 - q^(m+4))*(1 - q^(2*m+5))*q^((5*m+19)*m/2)) / Product_{j\u003e=1} (1 - q^j). See [A-B] eq. (3.8) for i=6. (End)",
				"a(n) ~ exp(2*Pi*sqrt(n/15)) / (2 * 3^(1/4) * sqrt(5) * phi^(9/2) * n^(3/4)), where phi = A001622 = (1+sqrt(5))/2 is the golden ratio. - _Vaclav Kotesovec_, Dec 24 2016"
			],
			"example": [
				"a(18) = 4 because the four partitions of 18 without parts 1, 2, 3, 4 and 5, and the parts differ by at least 2 are [18], [12, 6], [11, 7], [10, 8]. - _Wolfdieter Lang_, Nov 10 2016"
			],
			"mathematica": [
				"nmax = 100; CoefficientList[Series[Sum[x^(k*(k+5))/Product[1-x^j, {j, 1, k}], {k, 0, nmax}], {x, 0, nmax}], x] (* _Vaclav Kotesovec_, Dec 24 2016 *)"
			],
			"xref": [
				"For the generalized Rogers-Ramanujan series G[0], G[1], G[2], G[3], G[4], G[5], G[6], G[7], G[8] see A003113, A003114, A003106, A006141, A264591, A264592, A264593, A264594, A264595."
			],
			"keyword": "nonn",
			"offset": "0,15",
			"author": "_N. J. A. Sloane_, Nov 19 2015",
			"references": 9,
			"revision": 25,
			"time": "2016-12-24T10:30:48-05:00",
			"created": "2015-11-19T02:00:26-05:00"
		}
	]
}