{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A303543",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 303543,
			"data": "0,1,2,3,2,3,4,4,2,3,5,5,2,3,5,5,4,3,6,8,4,3,6,6,3,3,5,7,6,3,4,8,5,2,6,7,3,4,5,5,6,4,5,10,6,4,7,8,4,2,7,9,9,5,7,11,8,2,5,11,5,4,4,8,8,4,6,11,10,3,6,8,5,5,6,7,6,6,5,9",
			"name": "Number of ways to write n as a^2 + b^2 + C(k) + C(m) with 0 \u003c= a \u003c= b and 0 \u003c k \u003c= m, where C(k) denotes the Catalan number binomial(2k,k)/(k+1).",
			"comment": [
				"Conjecture: a(n) \u003e 0 for all n \u003e 1. In other words, any integer n \u003e 1 can be written as the sum of two squares and two Catalan numbers.",
				"This is similar to the author's conjecture in A303540. It has been verified that a(n) \u003e 0 for all n = 2..10^9."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A303543/b303543.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2016.11.008\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, J. Number Theory 175(2017), 167-190.",
				"Zhi-Wei Sun, \u003ca href=\"http://maths.nju.edu.cn/~zwsun/179b.pdf\"\u003eNew conjectures on representations of integers (I)\u003c/a\u003e, Nanjing Univ. J. Math. Biquarterly 34(2017), no. 2, 97-120.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1701.05868\"\u003eRestricted sums of four squares\u003c/a\u003e, arXiv:1701.05868 [math.NT], 2017-2018."
			],
			"example": [
				"a(2) = 1 with 2 = 0^2 + 0^2 + C(1) + C(1).",
				"a(3) = 2 with 3 = 0^2 + 1^2 + C(1) + C(1) = 0^2 + 0^2 + C(1) + C(2)."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]];",
				"c[n_]:=c[n]=Binomial[2n,n]/(n+1);",
				"f[n_]:=f[n]=FactorInteger[n];",
				"g[n_]:=g[n]=Sum[Boole[Mod[Part[Part[f[n],i],1],4]==3\u0026\u0026Mod[Part[Part[f[n],i],2],2]==1],{i,1,Length[f[n]]}]==0;",
				"QQ[n_]:=QQ[n]=(n==0)||(n\u003e0\u0026\u0026g[n]);",
				"tab={};Do[r=0;k=1;Label[bb];If[c[k]\u003en,Goto[aa]];Do[If[QQ[n-c[k]-c[j]],Do[If[SQ[n-c[k]-c[j]-x^2],r=r+1],{x,0,Sqrt[(n-c[k]-c[j])/2]}]],{j,1,k}];k=k+1;Goto[bb];Label[aa];tab=Append[tab,r],{n,1,80}];Print[tab]"
			],
			"xref": [
				"Cf. A000108, A000290, A001481, A303233, A303234, A303338, A303363, A303389, A303393, A303399, A303428, A303401, A303432, A303434, A303539, A303540, A303541, A303601."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Zhi-Wei Sun_, Apr 25 2018",
			"references": 22,
			"revision": 13,
			"time": "2018-05-30T12:19:08-04:00",
			"created": "2018-04-26T03:29:12-04:00"
		}
	]
}