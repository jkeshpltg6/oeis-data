{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A013988",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 13988,
			"data": "1,5,1,55,15,1,935,295,30,1,21505,7425,925,50,1,623645,229405,32400,2225,75,1,21827575,8423415,1298605,103600,4550,105,1,894930575,358764175,59069010,5235405,271950,8330,140,1,42061737025,17398082625",
			"name": "Triangle read by rows, the inverse Bell transform of n!*binomial(5,n) (without column 0).",
			"comment": [
				"Previous name was: Triangle of numbers related to triangle A049224; generalization of Stirling numbers of second kind A008277, Bessel triangle A001497.",
				"a(n,m) := S2p(-5; n,m), a member of a sequence of triangles including S2p(-1; n,m) := A001497(n-1,m-1) (Bessel triangle) and ((-1)^(n-m))*S2p(1; n,m) := A008277(n,m) (Stirling 2nd kind). a(n,1)= A008543(n-1).",
				"For the definition of the Bell transform see A264428 and the link. - _Peter Luschny_, Jan 16 2016"
			],
			"link": [
				"P. Blasiak, K. A. Penson and A. I. Solomon, \u003ca href=\"http://www.arXiv.org/abs/quant-ph/0402027\"\u003eThe general boson normal ordering problem\u003c/a\u003e, arXiv:quant-ph/0402027, 2004.",
				"M. Janjic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL12/Janjic/janjic22.html\"\u003eSome classes of numbers and derivatives\u003c/a\u003e, JIS 12 (2009) 09.8.3",
				"W. Lang, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL3/LANG/lang.html\"\u003eOn generalizations of Stirling number triangles\u003c/a\u003e, J. Integer Seqs., Vol. 3 (2000), #00.2.4.",
				"Peter Luschny, \u003ca href=\"https://oeis.org/wiki/User:Peter_Luschny/BellTransform\"\u003eThe Bell transform\u003c/a\u003e",
				"\u003ca href=\"/index/Be#Bessel\"\u003eIndex entries for sequences related to Bessel functions or polynomials\u003c/a\u003e"
			],
			"formula": [
				"a(n, m) = n!*A049224(n, m)/(m!*6^(n-m));",
				"a(n+1, m) = (6*n-m)*a(n, m) + a(n, m-1), n \u003e= m \u003e= 1;",
				"a(n, m) = 0, n\u003cm; a(n, 0) = 0, a(1, 1) = 1;",
				"E.g.f. of m-th column: ((1-(1-6*x)^(1/6))^m)/m!."
			],
			"example": [
				"{    1}",
				"{    5,    1}",
				"{   55,   15,   1}",
				"{  935,  295,  30,  1}",
				"{21505, 7425, 925, 50, 1}"
			],
			"mathematica": [
				"rows = 10;",
				"b[n_, m_] := BellY[n, m, Table[k! Binomial[5, k], {k, 0, rows}]];",
				"A = Table[b[n, m], {n, 1, rows}, {m, 1, rows}] // Inverse // Abs;",
				"A013988 = Table[A[[n, m]], {n, 1, rows}, {m, 1, n}] // Flatten (* _Jean-François Alcover_, Jun 22 2018 *)"
			],
			"program": [
				"(Sage) # uses[inverse_bell_matrix from A264428]",
				"# Adds 1,0,0,0, ... as column 0 at the left side of the triangle.",
				"inverse_bell_matrix(lambda n: factorial(n)*binomial(5, n), 8) # _Peter Luschny_, Jan 16 2016"
			],
			"xref": [
				"Cf. A001497, A008277, A049224.",
				"Cf. A000369, A004747, A011801, A028844."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "1,2",
			"author": "_Wolfdieter Lang_",
			"ext": [
				"New name from _Peter Luschny_, Jan 16 2016"
			],
			"references": 9,
			"revision": 35,
			"time": "2020-03-27T06:57:54-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}