{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A343253",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 343253,
			"data": "1,2,3,4,5,3,7,8,9,5,1,4,13,7,5,16,17,9,19,5,7,2,23,8,25,13,27,7,29,5,31,32,3,17,7,9,37,19,13,8,41,7,43,4,9,23,47,16,49,25,17,13,53,27,5,8,19,29,59,5,61,31,9,64,13,3,67,17,23,7,71,9,73,37,25,19,7,13,79,16",
			"name": "a(n) is the least k0 \u003c= n such that v_11(n), the 11-adic order of n, can be obtained by the formula: v_11(n) = log_11(n / L_11(k0, n)), where L_11(k0, n) is the lowest common denominator of the elements of the set S_11(k0, n) = {(1/n)*binomial(n, k), with 0 \u003c k \u003c= k0 such that k is not divisible by 11} or 0 if no such k0 exists.",
			"comment": [
				"Conjecture: a(n) is the greatest power of a prime different from 11 that divides n."
			],
			"example": [
				"For n = 12, a(12) = 4. To understand this result, consider the largest set S_11, which is the S_11(k0=12, 12). According to the definition, S_11(n, n) is the set of elements of the form (1/n)*binomial(n, k), where k goes from 1 to n, skipping the multiples of 11. The elements of S_11(12, 12) are {1, 11/2, 55/3, 165/4, 66, 77, 66, 165/4, 55/3, 11/2, 0, 1/12}, where the zero was inserted pedagogically to identify the skipped term, i.e., when k is divisible by 11. At this point we verify which of the nested subsets {1}, {1, 11/2}, {1, 11/2, 55/3}, {1, 11/2, 55/3, 165/4}, ... will match for the first time the p-adic order's formula. If k varies from 1 to 4 (instead of 12) we see that the lowest common denominator of the set S_11(4, 12) will be 12. So, L_11(4, 12) = 12 and the equation v_11(12) = log_11(12/12) yields a True result. Then we may say that a(12) = 4 specifically because 4 was the least k0."
			],
			"mathematica": [
				"j = 5;",
				"Nmax = 250;",
				"Array[val, Nmax];",
				"Do[val[i] = 0, {i, 1, Nmax}];",
				"Do[flag = 0;",
				"  Do[If[(flag == 0 \u0026\u0026",
				"      Prime[j]^IntegerExponent[n, Prime[j]] ==",
				"       n/LCM[Table[",
				"           If[Divisible[k, Prime[j]], 1,",
				"            Denominator[(1/n) Binomial[n, k]]], {k, 1, k}] /.",
				"          List -\u003e Sequence]), val[n] = k; flag = 1; , Continue], {k, 1,",
				"     n, 1}], {n, 1, Nmax}];",
				"tabseq = Table[val[i], {i, 1, Nmax}];",
				"(* alternate code *)",
				"a[n_] := Module[{k = 1, v = IntegerExponent[n, 11]}, While[Log[11, n/LCM @@ Denominator[Binomial[n, Select[Range[k], ! Divisible[#, 11] \u0026]]/n]] != v, k++]; k]; Array[a, 100] (* _Amiram Eldar_, Apr 23 2021 *)"
			],
			"program": [
				"(PARI) Lp(k, n, p) = {my(list = List()); for (i=1, k, if (i%p, listput(list, binomial(n, i)/n)); ); lcm(apply(denominator, Vec(list))); }",
				"isok(k, n, v, p) = p^v == n/Lp(k, n, p);",
				"a(n, p=11) = {my(k=1, v=valuation(n, p)); for (k=1, n, if (isok(k, n, v, p), return(k)); ); n; } \\\\ _Michel Marcus_, Apr 22 2021"
			],
			"xref": [
				"Cf. A343249, A343250, A343251, A343252."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Dario T. de Castro_, May 31 2021",
			"references": 5,
			"revision": 26,
			"time": "2021-10-27T14:30:32-04:00",
			"created": "2021-08-06T07:27:49-04:00"
		}
	]
}