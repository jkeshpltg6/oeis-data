{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A108735",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 108735,
			"data": "1,6,-18,108,-810,6804,-61236,577368,-5629338,56293380,-574192476,5950722024,-62482581252,663276631752,-7106535340200,76750581674160,-834662575706490,9132190534200420,-100454095876204620,1110282112315945800",
			"name": "Expansion of (1 + 12*x)^(1/2).",
			"comment": [
				"This is also the expansion of sqrt(3)*(2*B2inv(x) - 1), where B2inv is the compositional inverse of the Bernoulli polynomial B(2, x) = 1/6 - x + x^2 = (x - 1/2)^2 - 1/12, for x \u003e= 1/2. (see A196838 and A196839 for the Bernoulli polynomials). - _Wolfdieter Lang_, Aug 26 2015"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A108735/b108735.txt\"\u003eTable of n, a(n) for n = 0..838\u003c/a\u003e"
			],
			"formula": [
				"From _Wolfdieter Lang_, Aug 26 2015: (Start)",
				"G.f.: sqrt(1 + 12*x) = 1 + 6*x*c(-3*x), with the g.f. c of the Catalan numbers A000108.",
				"a(n) = -2*(-3)^n*C(n-1), n \u003e= 1, and a(0) = 1, with C(n) = A000108(n). (End)",
				"From _Robert Israel_, Aug 27 2015: (Start)",
				"D-finite with recurrence: a(n) = (18/n - 12)*a(n-1).",
				"a(n) ~ (-1)^(n+1)*12^n/(2*sqrt(Pi)*n^(3/2)). (End)",
				"0 = a(n)*(144*a(n+1) +30*a(n+2)) +a(n+1)*(-6*a(n+1) +a(n+2)) for all n in Z. - _Michael Somos_, Aug 27 2015",
				"a(n) = 2*(-1)^(n+1)*A025226(n). - _R. J. Mathar_, Jan 23 2020"
			],
			"example": [
				"G.f. = 1 + 6*x - 18*x^2 + 108*x^3 - 810*x^4 + 6804*x^5 - 61236*x^6 + ..."
			],
			"maple": [
				"f:= proc(n) option remember; (18/n - 12)*procname(n-1) end proc: f(0):= 1:",
				"map(f, [$0..100]); # _Robert Israel_, Aug 27 2015"
			],
			"mathematica": [
				"CoefficientList[Series[(1 + 12 x)^(1/2), {x, 0, 19}], x] (* _Michael De Vlieger_, Aug 26 2015 *)",
				"Join[{1}, RecurrenceTable[{a[1] == 6, a[n] == a[n-1] (18/n - 12)}, a, {n, 20}]] (* _Vincenzo Librandi_, Aug 27 2015 *)"
			],
			"program": [
				"(PARI) x = xx+O(xx^30); Vec(sqrt(1 + 12*x)) \\\\ _Michel Marcus_, Aug 26 2015"
			],
			"xref": [
				"Cf. A000108."
			],
			"keyword": "sign,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Jun 22 2005",
			"references": 5,
			"revision": 31,
			"time": "2020-01-30T21:29:15-05:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}