{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A320348",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 320348,
			"data": "1,1,1,2,3,2,4,4,4,6,9,7,13,12,13,16,22,17,28,28,31,36,50,45,63,62,74,78,102,92,123,123,146,148,191,181,228,233,280,283,348,350,420,437,518,523,616,641,727,774,884,911,1038,1102,1240,1292,1463,1530,1715,1861,2002",
			"name": "Number of partition into distinct parts (a_1, a_2, ... , a_m) (a_1 \u003e a_2 \u003e ... \u003e a_m and Sum_{k=1..m} a_k = n) such that a1 - a2, a2 - a_3, ... , a_{m-1} - a_m, a_m are different.",
			"comment": [
				"Also the number of integer partitions of n whose parts cover an initial interval of positive integers with distinct multiplicities. Also the number of integer partitions of n whose multiplicities cover an initial interval of positive integers and are distinct (see A048767 for a bijection). - _Gus Wiseman_, May 04 2019"
			],
			"link": [
				"Fausto A. C. Cariboni, \u003ca href=\"/A320348/b320348.txt\"\u003eTable of n, a(n) for n = 1..500\u003c/a\u003e (terms 1..100 from Seiichi Manyama)",
				"Gus Wiseman, \u003ca href=\"/A325325/a325325.txt\"\u003eSequences counting and ranking integer partitions by the differences of their successive parts.\u003c/a\u003e"
			],
			"example": [
				"n = 9",
				"[9]        *********  a_1 = 9.",
				"           ooooooooo",
				"------------------------------------",
				"[8, 1]             *        a_2 = 1.",
				"            *******o  a_1 - a_2 = 7.",
				"            oooooooo",
				"------------------------------------",
				"[7, 2]            **        a_2 = 2.",
				"             *****oo  a_1 - a_2 = 5.",
				"             ooooooo",
				"------------------------------------",
				"[5, 4]          ****        a_2 = 4.",
				"               *oooo  a_1 - a_2 = 1.",
				"               ooooo",
				"------------------------------------",
				"a(9) = 4.",
				"From _Gus Wiseman_, May 04 2019: (Start)",
				"The a(1) = 1 through a(11) = 9 strict partitions with distinct differences (where the last part is taken to be 0) are the following (A = 10, B = 11). The Heinz numbers of these partitions are given by A325388.",
				"  (1)  (2)  (3)  (4)   (5)   (6)   (7)   (8)   (9)   (A)    (B)",
				"                 (31)  (32)  (51)  (43)  (53)  (54)  (64)   (65)",
				"                       (41)        (52)  (62)  (72)  (73)   (74)",
				"                                   (61)  (71)  (81)  (82)   (83)",
				"                                                     (91)   (92)",
				"                                                     (631)  (A1)",
				"                                                            (632)",
				"                                                            (641)",
				"                                                            (731)",
				"The a(1) = 1 through a(10) = 6 partitions covering an initial interval of positive integers with distinct multiplicities are the following. The Heinz numbers of these partitions are given by A325326.",
				"  1  11  111  211   221    21111   2221     22211     22221      222211",
				"              1111  2111   111111  22111    221111    2211111    322111",
				"                    11111          211111   2111111   21111111   2221111",
				"                                   1111111  11111111  111111111  22111111",
				"                                                                 211111111",
				"                                                                 1111111111",
				"The a(1) = 1 through a(10) = 6 partitions whose multiplicities cover an initial interval of positive integers and are distinct are the following (A = 10). The Heinz numbers of these partitions are given by A325337.",
				"  (1)  (2)  (3)  (4)    (5)    (6)    (7)    (8)    (9)    (A)",
				"                 (211)  (221)  (411)  (322)  (332)  (441)  (433)",
				"                        (311)         (331)  (422)  (522)  (442)",
				"                                      (511)  (611)  (711)  (622)",
				"                                                           (811)",
				"                                                           (322111)",
				"(End)"
			],
			"mathematica": [
				"Table[Length[Select[IntegerPartitions[n],UnsameQ@@#\u0026\u0026UnsameQ@@Differences[Append[#,0]]\u0026]],{n,30}] (* _Gus Wiseman_, May 04 2019 *)"
			],
			"xref": [
				"Cf. A000009, A320347.",
				"Cf. A007294, A007862, A048767, A098859, A179269, A320509, A320510, A325324, A325325, A325349, A325367, A325404, A325468."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Seiichi Manyama_, Oct 11 2018",
			"references": 21,
			"revision": 25,
			"time": "2021-02-23T10:08:15-05:00",
			"created": "2018-10-11T14:10:29-04:00"
		}
	]
}