{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A263207",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 263207,
			"data": "0,1,2,20,374,21313,5115140",
			"name": "Number of integer solutions for Product_{k=1..n}(c(k) + 1) = 2 * Product_{k=1..n}(c(k) - 1) with 1 \u003c c(k) \u003c= c(k+1).",
			"comment": [
				"Geometrically interpreted, the sequence a(n) provides the number of distinct ways to cut an n-dimensional cube orthogonally into equally many outer parts, i.e., those that can be seen from the outside, and inner parts. All cuts must go through the whole body. c(k) is the number of cuts for the k-th dimension.",
				"See section Example for all solutions for n=1 and n=2 and section Links for all solutions for n=3, n=4, n=5.",
				"For any n\u003e=1 the solution given by c(k)=A204321(k) for k=1..n-1 and c(n)=A204321(n)-1 always exists. Conjecture: there is no solution with a greater c(n). - _Martin Janecke_, Dec 01 2015",
				"From _Wolfdieter Lang_, Dec 01 2015: (Start)",
				"In terms of the j-th elementary symmetric functions sigma(n, j) in the indeterminates [c(1), ..., c[n]] the equation with the products can be rewritten as Sum_{j=0..n} ((1 - 2*(-1)^(n-j))*sigma(n, j) = 0, with sigma(n, 0) = 1.",
				"If one uses the reciprocals x(k) = 1/c(k) then",
				"  0 \u003c x(k+1) \u003c= x(k) \u003c  1 (the c's are all \u003e1, and finite), and the original equation becomes (by taking logarithms)",
				"  Sum_{k=1..n} Artanh(x(k)) = log(2)/2 = Artanh(1/3), (which  is about .35).",
				"  Here only positive terms appear. It is clear from the monotony of Artanh that all the x(k) \u003c 1/3 for n \u003e= 2, hence c(1) \u003e= 4 for n \u003e= 2.",
				"(End)"
			],
			"link": [
				"M. Janecke, \u003ca href=\"https://prlbr.de/2015/mehrdimensional-kuchen-schneiden/\"\u003eKuchengerechtigkeit in n Dimensionen\u003c/a\u003e, includes a Python program that finds solutions for a given n, July 2015.",
				"M. Janecke, \u003ca href=\"https://prlbr.de/2015/hypercube-cutting-supremum-proof-attempt/\"\u003eOn a conjecture about cutting hypercubes\u003c/a\u003e, Nov 2015.",
				"Martin Janecke, \u003ca href=\"/A263207/a263207.txt\"\u003eAll solutions for n=3\u003c/a\u003e",
				"Martin Janecke, \u003ca href=\"/A263207/a263207_1.txt\"\u003eAll solutions for n=4\u003c/a\u003e",
				"Martin Janecke, \u003ca href=\"/A263207/a263207_2.txt\"\u003eAll solutions for n=5\u003c/a\u003e"
			],
			"example": [
				"a(1) = 1, because there is exactly one solution: c(1) = 3.",
				"a(2) = 2, because there are two solutions: c(1) = 4, c(2) = 11 and c(1) = 5, c(2) = 7. The first solution can be illustrated geometrically as a square cut into thirty outer and equally many inner parts:",
				"    OOOOOOOOOOOO",
				"    OIIIIIIIIIIO",
				"    OIIIIIIIIIIO",
				"    OIIIIIIIIIIO",
				"    OOOOOOOOOOOO,",
				"  The second solution yields twenty-four parts of each kind:",
				"    OOOOOOOO",
				"    OIIIIIIO",
				"    OIIIIIIO",
				"    OIIIIIIO",
				"    OIIIIIIO",
				"    OOOOOOOO."
			],
			"xref": [
				"Cf. A204321."
			],
			"keyword": "nonn,more",
			"offset": "0,3",
			"author": "_Martin Janecke_, Oct 12 2015",
			"references": 1,
			"revision": 14,
			"time": "2015-12-01T17:20:28-05:00",
			"created": "2015-12-01T17:16:54-05:00"
		}
	]
}