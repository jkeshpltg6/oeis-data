{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A093644",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 93644,
			"data": "1,9,1,9,10,1,9,19,11,1,9,28,30,12,1,9,37,58,42,13,1,9,46,95,100,55,14,1,9,55,141,195,155,69,15,1,9,64,196,336,350,224,84,16,1,9,73,260,532,686,574,308,100,17,1,9,82,333,792,1218,1260,882,408,117,18,1,9,91,415",
			"name": "(9,1) Pascal triangle.",
			"comment": [
				"The array F(9;n,m) gives in the columns m\u003e=1 the figurate numbers based on A017173, including the 11-gonal numbers A051682 (see the W. Lang link).",
				"This is the ninth member, d=9, in the family of triangles of figurate numbers, called (d,1) Pascal triangles: A007318 (Pascal), A029653, A093560-5, for d=1..8.",
				"This is an example of a Riordan triangle (see A093560 for a comment and A053121 for a comment and the 1991 Shapiro et al. reference on the Riordan group). Therefore the o.g.f. for the row polynomials p(n,x) := Sum_{m=0..n} a(n,m)*x^m is G(z,x) = (1+8*z)/(1-(1+x)*z).",
				"The SW-NE diagonals give A022099(n-1) = Sum_{k=0..ceiling((n-1)/2)} a(n-1-k,k), n \u003e= 1, with n=0 value 8. Observation by _Paul Barry_, Apr 29 2004. Proof via recursion relations and comparison of inputs.",
				"Triangle T(n,k), read by rows, given by (9,-8,0,0,0,0,0,0,0,...) DELTA (1,0,0,0,0,0,0,0,...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Oct 10 2011"
			],
			"reference": [
				"Kurt Hawlitschek, Johann Faulhaber 1580-1635, Veroeffentlichung der Stadtbibliothek Ulm, Band 18, Ulm, Germany, 1995, Ch. 2.1.4. Figurierte Zahlen.",
				"Ivo Schneider: Johannes Faulhaber 1580-1635, Birkhäuser, Basel, Boston, Berlin, 1993, ch.5, pp. 109-122."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A093644/b093644.txt\"\u003eRows n = 0..125 of triangle, flattened\u003c/a\u003e",
				"Wolfdieter Lang, \u003ca href=\"/A093644/a093644.txt\"\u003eFirst 10 rows and array of figurate numbers \u003c/a\u003e."
			],
			"formula": [
				"a(n, m) = F(9;n-m, m) for 0 \u003c= m \u003c= n, otherwise 0, with F(9;0, 0)=1, F(9;n, 0)=9 if n \u003e= 1 and F(9;n, m):=(9*n+m)*binomial(n+m-1, m-1)/m if m \u003e= 1.",
				"Recursion: a(n, m)=0 if m \u003e n, a(0, 0)= 1; a(n, 0)=9 if n \u003e= 1; a(n, m) = a(n-1, m) + a(n-1, m-1).",
				"G.f. column m (without leading zeros): (1+8*x)/(1-x)^(m+1), m \u003e= 0.",
				"T(n, k) = C(n, k) + 8*C(n-1, k). - _Philippe Deléham_, Aug 28 2005",
				"Row n: Expansion of (9+x)*((1+x)^(n-1), n \u003e 0. - _Philippe Deléham_, Oct 10 2011",
				"exp(x) * e.g.f. for row n = e.g.f. for diagonal n. For example, for n = 3 we have exp(x)*(9 + 19*x + 11*x^2/2! + x^3/3!) = 9 + 28*x + 58*x^2/2! + 100*x^3/3! + 155*x^4/4! + .... The same property holds more generally for Riordan arrays of the form ( f(x), x/(1 - x) ). - _Peter Bala_, Dec 22 2014",
				"G.f.: (-1-8*x)/(-1+x+x*y). - _R. J. Mathar_, Aug 11 2015"
			],
			"example": [
				"Triangle begins",
				"  [1];",
				"  [9,  1];",
				"  [9, 10,  1];",
				"  [9, 19, 11,  1];",
				"  ..."
			],
			"program": [
				"(Haskell)",
				"a093644 n k = a093644_tabl !! n !! k",
				"a093644_row n = a093644_tabl !! n",
				"a093644_tabl = [1] : iterate",
				"               (\\row -\u003e zipWith (+) ([0] ++ row) (row ++ [0])) [9, 1]",
				"-- _Reinhard Zumkeller_, Aug 31 2014"
			],
			"xref": [
				"Row sums: A020714(n-1), n \u003e= 1, 1 for n=0, alternating row sums are 1 for n=0, 8 for n=2 and 0 otherwise.",
				"The column sequences give for m=1..9: A017173, A051682 (11-gonal), A007586, A051798, A051879, A050405, A052206, A056117, A056003.",
				"Cf. A093645 (d=10)."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Apr 22 2004",
			"references": 19,
			"revision": 39,
			"time": "2020-01-21T00:44:26-05:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}