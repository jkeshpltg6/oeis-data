{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A336431",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 336431,
			"data": "1,2,1,1,2,2,1,3,2,1,1,2,2,1,2,2,2,1,4,3,2,2,1,3,3,1,1,3,3,2,4,2,3,2,3,4,3,2,2,3,2,3,4,1,2,3,3,3,3,3,1,3,1,4,4,2,3,4,2,3,4,2,3,4,1,4,5,1,2,3,3,3,4,4,2,4,1,4,4,1,2,6,3,3,5,2,4,5,2,4,5,1,3,4,2,2,5,3,2,6",
			"name": "Number of ordered ways to write n as the sum of a practical number (A005153) and a generalized heptagonal number (A085787).",
			"comment": [
				"a(n) \u003e 0 for all n = 1..5*10^7.",
				"Conjecture 1: a(n) \u003e 0 for all n \u003e 0. Also, a(n) = 1 only for n = 1, 3, 4, 7, 10, 11, 14, 18, 23, 26, 27, 44, 51, 53, 65, 68, 77, 80, 92, 125, 143, 170, 179, 182, 185, 191, 206, 296, 362, 383, 425, 437, 674, 1340, 1622, 2273, 2558, 3167, 5591.",
				"Conjecture 2: Any positive integer can be written as the sum of a practical number and a generalized pentagonal number.",
				"Both conjectures are motivated by A208244."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A336431/b336431.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"https://doi.org/10.1007/978-3-319-68032-3_20\"\u003eConjectures on representations involving primes\u003c/a\u003e, in: M. Nathanson (ed.), Combinatorial and Additive Number Theory II, Springer Proc. in Math. \u0026 Stat., Vol. 220, Springer, Cham, 2017, pp. 279-310. (See also \u003ca href=\"http://arxiv.org/abs/1211.1588\"\u003earXiv\u003c/a\u003e, arXiv:1211.1588 [math.NT], 2012-2017.)"
			],
			"example": [
				"Set p_7(x) = x*(5*x-3)/2.",
				"a(14) = 1, and 14 = 1 + p_7(-2) with 1 practical.",
				"a(80) = 1, and 80 = 80 + p_7(0) with 80 practical.",
				"a(425) = 1, and 425 = 160 + p_7(-10) with 160 practical.",
				"a(1340) = 1, and 1340 = 800 + p_7(15) with 800 practical.",
				"a(2273) = 1, and 2273 = 544 + p_7(-26) with 544 practical.",
				"a(5591) = 1, and 5591 = 2752 + p_7(34) with 2752 practical."
			],
			"mathematica": [
				"f[n_]:=f[n]=FactorInteger[n];",
				"Pow[n_, i_]:=Pow[n,i]=Part[Part[f[n], i], 1]^(Part[Part[f[n], i], 2]);",
				"Con[n_]:=Con[n]=Sum[If[Part[Part[f[n], s+1], 1]\u003c=DivisorSigma[1, Product[Pow[n, i], {i, 1, s}]]+1, 0, 1], {s, 1, Length[f[n]]-1}];",
				"pr[n_]:=pr[n]=n\u003e0\u0026\u0026(n\u003c3||Mod[n, 2]+Con[n]==0);",
				"tab={};Do[r=0;Do[If[pr[n-x*(5*x-3)/2],r=r+1],{x,-Floor[(Sqrt[40n+9]-3)/10],(Sqrt[40n+9]+3)/10}];tab=Append[tab,r],{n,1,100}];Print[tab]"
			],
			"xref": [
				"Cf. A001318, A005153, A085787, A208244."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Zhi-Wei Sun_, Oct 04 2020",
			"references": 1,
			"revision": 25,
			"time": "2020-10-06T12:22:39-04:00",
			"created": "2020-10-04T23:43:51-04:00"
		}
	]
}