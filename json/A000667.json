{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000667",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 667,
			"data": "1,2,4,9,24,77,294,1309,6664,38177,243034,1701909,13001604,107601977,959021574,9157981309,93282431344,1009552482977,11568619292914,139931423833509,1781662223749884,23819069385695177,333601191667149054,4884673638115922509",
			"name": "Boustrophedon transform of all-1's sequence.",
			"comment": [
				"Fill in a triangle, like Pascal's triangle, beginning each row with a 1 and filling in rows alternately right to left and left to right.",
				"Row sums of triangle A109449. - _Reinhard Zumkeller_, Nov 04 2013"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A000667/b000667.txt\"\u003eTable of n, a(n) for n = 0..485\u003c/a\u003e (first 101 terms from T. D. Noe)",
				"C. K. Cook, M. R. Bacon, and R. A. Hillman, \u003ca href=\"https://www.fq.math.ca/Abstracts/55-3/cook.pdf\"\u003eHigher-order Boustrophedon transforms for certain well-known sequences\u003c/a\u003e, Fib. Q., 55(3) (2017), 201-208.",
				"Peter Luschny, \u003ca href=\"http://oeis.org/wiki/User:Peter_Luschny/SeidelTransform\"\u003eAn old operation on sequences: the Seidel transform\u003c/a\u003e.",
				"J. Millar, N. J. A. Sloane, and N. E. Young, A new operation on sequences: the Boustrophedon transform, J. Combin. Theory Ser. A, 76(1) (1996), 44-54 (\u003ca href=\"http://neilsloane.com/doc/bous.txt\"\u003eAbstract\u003c/a\u003e, \u003ca href=\"http://neilsloane.com/doc/bous.pdf\"\u003epdf\u003c/a\u003e, \u003ca href=\"http://neilsloane.com/doc/bous.ps\"\u003eps\u003c/a\u003e).",
				"J. Millar, N. J. A. Sloane, and N. E. Young, \u003ca href=\"https://doi.org/10.1006/jcta.1996.0087\"\u003eA new operation on sequences: the Boustrophedon transform\u003c/a\u003e, J. Combin. Theory Ser. A, 76(1) (1996), 44-54.",
				"Ludwig Seidel, \u003ca href=\"https://babel.hathitrust.org/cgi/pt?id=hvd.32044092897461\u0026amp;view=1up\u0026amp;seq=176\"\u003eÜber eine einfache Entstehungsweise der Bernoulli'schen Zahlen und einiger verwandten Reihen\u003c/a\u003e, Sitzungsberichte der mathematisch-physikalischen Classe der königlich bayerischen Akademie der Wissenschaften zu München, volume 7 (1877), 157-187. [USA access only through the \u003ca href=\"https://www.hathitrust.org/accessibility\"\u003eHATHI TRUST Digital Library\u003c/a\u003e]",
				"Ludwig Seidel, \u003ca href=\"https://www.zobodat.at/pdf/Sitz-Ber-Akad-Muenchen-math-Kl_1877_0157-0187.pdf\"\u003eÜber eine einfache Entstehungsweise der Bernoulli'schen Zahlen und einiger verwandten Reihen\u003c/a\u003e, Sitzungsberichte der mathematisch-physikalischen Classe der königlich bayerischen Akademie der Wissenschaften zu München, volume 7 (1877), 157-187. [Access through \u003ca href=\"https://de.wikipedia.org/wiki/ZOBODAT\"\u003eZOBODAT\u003c/a\u003e]",
				"N. J. A. Sloane, \u003ca href=\"http://neilsloane.com/doc/sg.txt\"\u003eMy favorite integer sequences\u003c/a\u003e, in Sequences and their Applications (Proceedings of SETA '98).",
				"N. J. A. Sloane, \u003ca href=\"/transforms.txt\"\u003eTransforms\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Boustrophedon_transform\"\u003eBoustrophedon transform\u003c/a\u003e.",
				"\u003ca href=\"/index/Bo#boustrophedon\"\u003eIndex entries for sequences related to boustrophedon transform\u003c/a\u003e."
			],
			"formula": [
				"E.g.f.: exp(x) * (tan(x) + sec(x)).",
				"Limit_{n-\u003einfinity} 2*n*a(n-1)/a(n) = Pi; lim_{n-\u003einfinity} a(n)*a(n-2)/a(n-1)^2 = 1 + 1/(n-1). - _Gerald McGarvey_, Aug 13 2004",
				"a(n) = Sum_{k=0..n} binomial(n, k)*A000111(n-k). a(2*n) = A000795(n) + A009747(n), a(2*n+1) = A002084(n) + A003719(n). - _Philippe Deléham_, Aug 28 2005",
				"a(n) = A227862(n, n * (n mod 2)). - _Reinhard Zumkeller_, Nov 01 2013",
				"G.f.: E(0)*x/(1-x)/(1-2*x) + 1/(1-x), where E(k) = 1 - x^2*(k + 1)*(k + 2)/(x^2*(k + 1)*(k + 2) - 2*(x*(k + 2) - 1)*(x*(k + 3) - 1)/E(k+1) ); (continued fraction). - _Sergei N. Gladkovskii_, Jan 16 2014",
				"a(n) ~ n! * exp(Pi/2) * 2^(n+2) / Pi^(n+1). - _Vaclav Kotesovec_, Jun 12 2015"
			],
			"example": [
				"...............1..............",
				"............1..-\u003e..2..........",
				".........4..\u003c-.3...\u003c-..1......",
				"......1..-\u003e.5..-\u003e..8...-\u003e..9.."
			],
			"mathematica": [
				"With[{nn=30},CoefficientList[Series[Exp[x](Tan[x]+Sec[x]),{x,0,nn}], x]Range[0,nn]!] (* _Harvey P. Dale_, Nov 28 2011 *)",
				"t[_, 0] = 1; t[n_, k_] := t[n, k] = t[n, k-1] + t[n-1, n-k];",
				"a[n_] := t[n, n];",
				"Array[a, 30, 0] (* _Jean-François Alcover_, Feb 12 2016 *)"
			],
			"program": [
				"(Sage) # Algorithm of L. Seidel (1877)",
				"def A000667_list(n) :",
				"    R = []; A = {-1:0, 0:0}",
				"    k = 0; e = 1",
				"    for i in range(n) :",
				"        Am = 1",
				"        A[k + e] = 0",
				"        e = -e",
				"        for j in (0..i) :",
				"            Am += A[k]",
				"            A[k] = Am",
				"            k += e",
				"        # print [A[z] for z in (-i//2..i//2)]",
				"        R.append(A[e*i//2])",
				"    return R",
				"A000667_list(10)  # _Peter Luschny_, Jun 02 2012",
				"(Haskell)",
				"a000667 n = if x == 1 then last xs else x",
				"            where xs@(x:_) = a227862_row n",
				"-- _Reinhard Zumkeller_, Nov 01 2013",
				"(PARI) x='x+O('x^33); Vec(serlaplace( exp(x)*(tan(x) + 1/cos(x)) ) ) \\\\ _Joerg Arndt_, Jul 30 2016"
			],
			"xref": [
				"Absolute value of pairwise sums of A009337.",
				"Column k=1 of A292975."
			],
			"keyword": "nonn,easy,nice,changed",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_ and _Simon Plouffe_",
			"references": 36,
			"revision": 95,
			"time": "2022-01-05T00:31:52-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}