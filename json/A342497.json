{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A342497",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 342497,
			"data": "1,1,2,3,5,6,9,11,15,18,23,25,32,36,43,49,60,65,75,83,96,106,121,131,150,163,178,194,217,230,254,275,300,320,350,374,411,439,470,503,548,578,625,666,710,758,815,855,913,970,1029,1085,1157,1212,1288,1360",
			"name": "Number of integer partitions of n with weakly increasing first quotients.",
			"comment": [
				"Also called log-concave-up partitions.",
				"Also the number of reversed integer partitions of n with weakly increasing first quotients.",
				"The first quotients of a sequence are defined as if the sequence were an increasing divisor chain, so for example the first quotients of (6,3,1) are (1/2,1/3)."
			],
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/LogarithmicallyConcaveSequence.html\"\u003e Logarithmically Concave Sequence\u003c/a\u003e.",
				"Gus Wiseman, \u003ca href=\"/A325325/a325325.txt\"\u003eSequences counting and ranking integer partitions by the differences of their successive parts\u003c/a\u003e.",
				"Gus Wiseman, \u003ca href=\"/A069916/a069916.txt\"\u003eSequences counting and ranking partitions and compositions by their differences and quotients\u003c/a\u003e."
			],
			"example": [
				"The partition y = (6,3,2,1,1) has first quotients (1/2,2/3,1/2,1) so is not counted under a(13). However, the first differences (-3,-1,-1,0) are weakly increasing, so y is counted under A240026(13).",
				"The a(1) = 1 through a(8) = 15 partitions:",
				"  (1)  (2)   (3)    (4)     (5)      (6)       (7)        (8)",
				"       (11)  (21)   (22)    (32)     (33)      (43)       (44)",
				"             (111)  (31)    (41)     (42)      (52)       (53)",
				"                    (211)   (311)    (51)      (61)       (62)",
				"                    (1111)  (2111)   (222)     (322)      (71)",
				"                            (11111)  (411)     (421)      (422)",
				"                                     (3111)    (511)      (521)",
				"                                     (21111)   (4111)     (611)",
				"                                     (111111)  (31111)    (2222)",
				"                                               (211111)   (4211)",
				"                                               (1111111)  (5111)",
				"                                                          (41111)",
				"                                                          (311111)",
				"                                                          (2111111)",
				"                                                          (11111111)"
			],
			"mathematica": [
				"Table[Length[Select[IntegerPartitions[n],LessEqual@@Divide@@@Reverse/@Partition[#,2,1]\u0026]],{n,0,30}]"
			],
			"xref": [
				"The version for differences instead of quotients is A240026.",
				"The ordered version is A342492.",
				"The strictly increasing version is A342498.",
				"The weakly decreasing version is A342513.",
				"The strict case is A342516.",
				"The Heinz numbers of these partitions are A342523.",
				"A000005 counts constant partitions.",
				"A000009 counts strict partitions.",
				"A000041 counts partitions.",
				"A000929 counts partitions with all adjacent parts x \u003e= 2y.",
				"A001055 counts factorizations.",
				"A003238 counts chains of divisors summing to n - 1 (strict: A122651).",
				"A074206 counts ordered factorizations.",
				"A167865 counts strict chains of divisors \u003e 1 summing to n.",
				"A342094 counts partitions with all adjacent parts x \u003c= 2y.",
				"Cf. A000837, A002843, A003242, A175342, A318991, A318992, A325557, A342527, A342528, A342529."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Gus Wiseman_, Mar 17 2021",
			"references": 4,
			"revision": 9,
			"time": "2021-03-18T23:47:38-04:00",
			"created": "2021-03-18T23:47:38-04:00"
		}
	]
}