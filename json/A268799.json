{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A268799",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 268799,
			"data": "4,8,12,20,24,36,40,56,60,64,68,112,120,132,144,156,168,176,184,200,240,256,272,280,296,356,396,444,452,480,532,616,620,672,692,708,840,864,896,916,1004",
			"name": "Record (maximal) gaps between primes of the form 4k + 3.",
			"comment": [
				"Dirichlet's theorem on arithmetic progressions and GRH suggest that average gaps between primes of the form 4k + 3 below x are about phi(4)*log(x). This sequence shows that the record gap ending at p grows almost as fast as phi(4)*log^2(p). Here phi(n) is A000010, Euler's totient function; phi(4)=2.",
				"Conjecture: a(n) \u003c phi(4)*log^2(A268801(n)) almost always.",
				"Conjecture: a(n) \u003c phi(4)*n^2 for all n\u003e2. - _Alexei Kourbatov_, Aug 12 2017"
			],
			"link": [
				"Alexei Kourbatov, \u003ca href=\"http://arxiv.org/abs/1610.03340\"\u003eOn the distribution of maximal gaps between primes in residue classes\u003c/a\u003e, arXiv:1610.03340 [math.NT], 2016.",
				"Alexei Kourbatov, \u003ca href=\"https://arxiv.org/abs/1709.05508\"\u003eOn the nth record gap between primes in an arithmetic progression\u003c/a\u003e, arXiv:1709.05508 [math.NT], 2017; \u003ca href=\"https://doi.org/10.12988/imf.2018.712103\"\u003eInt. Math. Forum, 13 (2018), 65-78\u003c/a\u003e.",
				"Alexei Kourbatov and Marek Wolf, \u003ca href=\"http://arxiv.org/abs/1901.03785\"\u003ePredicting maximal gaps in sets of primes\u003c/a\u003e, arXiv preprint arXiv:1901.03785 [math.NT], 2019."
			],
			"example": [
				"The first two primes of the form 4k+3 are 3 and 7, so a(1)=7-3=4. The next prime of this form is 11; the gap 11-7 is not a record so no term is added to the sequence. The next prime of this form is 19; the gap 19-11=8 is a new record, so a(2)=8."
			],
			"mathematica": [
				"re = 0; s = 3; Reap[For[p = 7, p \u003c 10^8, p = NextPrime[p], If[Mod[p, 4] != 3, Continue[]]; g = p - s; If[g \u003e re, re = g; Print[g]; Sow[g]]; s = p]][[2, 1]] (* _Jean-François Alcover_, Dec 12 2018, from PARI *)"
			],
			"program": [
				"(PARI) re=0; s=3; forprime(p=7, 1e8, if(p%4!=3, next); g=p-s; if(g\u003ere, re=g; print1(g\", \")); s=p)"
			],
			"xref": [
				"Cf. A002145, A084161.",
				"Corresponding primes: A268800 (lower ends), A268801 (upper ends)."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Alexei Kourbatov_, Feb 13 2016",
			"references": 3,
			"revision": 29,
			"time": "2019-01-17T02:55:18-05:00",
			"created": "2016-02-13T23:01:55-05:00"
		}
	]
}