{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A290319",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 290319,
			"data": "1,1,1,5,6,1,45,59,15,1,585,812,254,28,1,9945,14389,5130,730,45,1,208845,312114,122119,20460,1675,66,1,5221125,8011695,3365089,633619,62335,3325,91,1,151412625,237560280,105599276,21740040,2441334,158760,5964,120,1,4996616625,7990901865,3722336388,823020596,102304062,7680414,355572,9924,153,1,184874815125,300659985630,145717348221,34174098440,4608270890,386479380,20836578,722760,15585,190,1",
			"name": "Triangle read by rows: T(n, k)is the Sheffer triangle ((1 - 4*x)^(-1/4), (-1/4)*log(1 - 4*x)). A generalized Stirling1 triangle.",
			"comment": [
				"This generalization of the unsigned Stirling1 triangle A132393 is called here |S1hat[4,1]|.",
				"The signed matrix S1hat[4,1] with elements (-1)^(n-k)*|S1hat[4,1]|(n, k) is the inverse of the generalized Stirling2 Sheffer matrix S2hat[4,1] with elements S2[4,1](n, k)/d^k, where S2[4,1] is Sheffer (exp(x), exp(4*x) - 1), given in A285061. See also the P. Bala link below for the  scaled and signed version s_{(4,0,1)}.",
				"For the general |S1hat[d,a]| case see a comment in A286718."
			],
			"link": [
				"P. Bala, \u003ca href=\"/A143395/a143395.pdf\"\u003eA 3 parameter family of generalized Stirling numbers\u003c/a\u003e.",
				"Wolfdieter Lang, \u003ca href=\"http://arXiv.org/abs/1707.04451\"\u003eOn Sums of Powers of Arithmetic Progressions, and Generalized Stirling, Eulerian and Bernoulli Numbers\u003c/a\u003e, arXiv:math/1707.04451 [math.NT], July 2017."
			],
			"formula": [
				"Recurrence: T(n, k) = T(n-1, k-1) + (4*n - 3)*T(n-1, k), for n \u003e= 1, k = 0..n, and T(n, -1) = 0, T(0, 0) = 1 and T(n, k) = 0 for n \u003c k.",
				"E.g.f. of row polynomials R(n, x) = Sum_{k=0..n} T(n, k)*x^k (i.e., e.g.f. of the triangle): (1 - 4*z)^{-(x + 1)/4}.",
				"E.g.f. of column k is (1 - 4*x)^(-1/4)*((-1/4)*log(1 - 4*x))^k/k!.",
				"Recurrence for row polynomials is R(n, x) = (x+1)*R(n-1, x+4), with R(0, x) = 1. Row polynomial R(n, x) = risefac(4,1;x,n) with the rising factorial risefac(d,a;x,n) :=Product_{j=0..n-1} (x + (a + j*d)). (For the signed case see the Bala link, eq. (16)).",
				"T(n, k) = sigma^{(n)}_{n-k}(a_0, a_1, ..., a_{n-1}) with the elementary symmetric functions with indeterminates a_j = 1 + 4*j.",
				"T(n, k) = Sum_{j=0..n-k} binomial(n-j, k)*|S1|(n, n-j)*4^j, with the unsigned Stirling1 triangle |S1| = A132393.",
				"Boas-Buck type recurrence for column sequence k: T(n, k) = (n!/(n - k)) * Sum_{p=k..n-1} 4^(n-1-p)*(1 + 4*k*beta(n-1-p))*T(p, k)/p!, for n \u003e k \u003e= 0, with input T(k, k) = 1, and beta(k) = A002208(k+1)/A002209(k+1), beginning with {1/2, 5/12, 3/8, 251/720, ...}. See a comment and references in A286718. - _Wolfdieter Lang_, Aug 11 2017"
			],
			"example": [
				"The triangle T(n, k) begins:",
				"n\\k         0         1         2        3       4      5    6   7  8 ...",
				"O:          1",
				"1:          1         1",
				"2:          5         6         1",
				"3:         45        59        15        1",
				"4:        585       812       254       28       1",
				"5:       9945     14389      5130      730      45      1",
				"6:     208845    312114    122119    20460    1675     66    1",
				"7:    5221125   8011695   3365089   633619   62335   3325   91   1",
				"8:  151412625 237560280 105599276 21740040 2441334 158760 5964 120  1",
				"...",
				"From _Wolfdieter Lang_, Aug 11 2017: (Start)",
				"Recurrence: T(4, 2) = T(3, 1) + (16 - 3)*T(3, 2) = 59 + 13*15 = 254.",
				"Boas-Buck recurrence for column k=2 and n=4:",
				"T(4, 2) = (4!/2)*(4*(1 + 8*(5/12))*T(2, 2)/2! + 1*(1 + 8*(1/2))*T(3,2)/3!) = (4!/2)*(2*13/3 + 5*15/3!) = 254. - _Wolfdieter Lang_, Aug 11 2017"
			],
			"xref": [
				"S2[d,a] for [d,a] = [1,0], [2,1], [3,1], [3,2], [4,1] and [4,3] is A048993, A154537, A282629, A225466, A285061 and A225467, respectively.",
				"|S1hat[d,a]| for [d,a] = [1,0], [2,1], [3,1],  [3,2]  and [4,3] is A132393, A028338, A286718,  A225470 and A225471, respectively.",
				"Column sequences for k = 0, 1: A007696, A024382.",
				"Row sums: A001813. Alternating row sums: A000007."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,4",
			"author": "_Wolfdieter Lang_, Aug 08 2017",
			"references": 1,
			"revision": 7,
			"time": "2017-08-11T15:08:34-04:00",
			"created": "2017-08-08T21:41:36-04:00"
		}
	]
}