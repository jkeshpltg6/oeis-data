{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A329147",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 329147,
			"data": "0,2,3,5,7,11,13,17,19,23,20,22,23,25,27,211,213,217,219,223,30,32,33,35,37,311,313,317,319,323,50,52,53,55,57,511,513,517,519,523,70,72,73,75,77,711,713,717,719,723,110,112,113,115,117,1111,1113,1117,1119,1123",
			"name": "Replace in n each nonzero digit d with prime(d).",
			"comment": [
				"Some properties:",
				"No term has a digit 4, 6 or 8.",
				"No term begins with 9, 10, 12, 15, 29, 39, 59, 79.",
				"If a is regarded as a function a: n --\u003e a(n) from N to N, then",
				"1) a is neither increasing: a(9) \u003e a(10) nor decreasing: a(3) \u003c a(4),",
				"2) a is not injective: a(92) = a(122) = 233,",
				"3) a is not surjective: 4 and 15 are not terms. The integers that are not in this sequence are in A329149 and the integers that are obtained are in A329150, with increasing order.",
				"Some primes remain primes: 2, 3, 5, 7, 19, 59, ...",
				"Some primes become composites: 11, 13, 17, 23, 29, 31, ...",
				"Some composites remain composites: 10, 14, 16, 18, 20, 21, 22,...",
				"Some composites become primes: 4, 6, 8, 9, 12, 15, 24, 25, 26,...",
				"When n \u003e 4 ends respectively with 0, 1 or 3 then a(n) that ends with 0, 2, 5 is composite.",
				"The sequence 9, 99, 999, ..., respectively 12, 1212, 121212, ... generates the same numbers 23, 2323, 232323, ... Analogously, 9, 92, 922, 922, ... and 12, 122, 1222, ... generate the same sequence 23, 233, 2333, 23333, .... For the numbers 91,9191,919191, ... the terms of the sequence are 232, 232232, 232232232, ... so palindromes. - _Marius A. Burtea_, Nov 07 2019",
				"The numbers 113, 14113, 1441113, 144411113, ... determine the terms 225 = 15^2, 27225 = 165^2, 2772225 = 1665^2, ... (in A191486). The numbers 14, 14000, 14000000, ... determine the terms 27 = 3^3, 27000 = 30^3, 27000000 = 300^3, .... - _Marius A. Burtea_, Nov 12 2019"
			],
			"link": [
				"Metin Sariyar, \u003ca href=\"/A329147/b329147.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"example": [
				"As a(2) = prime(2) = 3, a(5) = prime(5) = 11 and a(8) = prime(8) = 19, a(258)= 31119.",
				"As a(3) = prime(3) = 5, a(0) = 0 and a(7) = prime(7) = 17, hence a(307) = 5017."
			],
			"maple": [
				"a:= n-\u003e (l-\u003e parse(cat(seq(`if`(l[-i]=0, 0, ithprime(l[-i])),",
				"             i=1..nops(l)))))(convert(n, base, 10)):",
				"seq(a(n), n=0..80);  # _Alois P. Heinz_, Nov 07 2019"
			],
			"mathematica": [
				"p[n_] := If[n \u003e 0, Prime[n], 0]; a[n_] := FromDigits[Flatten @ IntegerDigits @ (p /@ IntegerDigits[n])]; Array[a, 60, 0] (* _Amiram Eldar_, Nov 06 2019 *)"
			],
			"program": [
				"(MAGMA) v:=[0,2,3,5,7,11,13,17,19,23]; [0] cat [StringToInteger(\u0026cat[IntegerToString(k): k in Reverse([v[m+1]: m in Intseq(n)])]): n in [1..60]]; // _Marius A. Burtea_, Nov 07 2019",
				"(PARI) a(n) = if (n, fromdigits(concat(apply(d -\u003e if (d, digits(prime(d)), [0]), digits(n)))), 0) \\\\ _Rémy Sigrist_, Nov 07 2019"
			],
			"xref": [
				"Cf. A000040, A329149, A329150.",
				"Similar to A048380, A048385 and A322131."
			],
			"keyword": "nonn,base",
			"offset": "0,2",
			"author": "_Bernard Schott_, Nov 06 2019",
			"references": 3,
			"revision": 66,
			"time": "2019-12-04T09:23:21-05:00",
			"created": "2019-11-12T07:00:30-05:00"
		}
	]
}