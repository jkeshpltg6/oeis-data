{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A123968",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 123968,
			"data": "-2,1,6,13,22,33,46,61,78,97,118,141,166,193,222,253,286,321,358,397,438,481,526,573,622,673,726,781,838,897,958,1021,1086,1153,1222,1293,1366,1441,1518,1597,1678,1761,1846,1933,2022,2113,2206,2301,2398,2497",
			"name": "a(n) = n^2 - 3.",
			"comment": [
				"Essentially the same as A028872 (n^2-3 with offset 2).",
				"a(n) is the constant term of the quadratic factor of the characteristic polynomial of the 5 X 5 tridiagonal matrix M_n with M_n(i,j) = n for i = j, M_n(i,j) = -1 for i = j+1 and i = j-1, M_n(i,j) = 0 otherwise.",
				"The characteristic polynomial of M_n is (x-(n-1))*(x-n)*(x-(n+1))*(x^2-2*n*x+c) with c = n^2-3.",
				"The characteristic polynomials are related to chromatic polynomials, cf. links. They have roots n+sqrt(3)."
			],
			"link": [
				"Eric W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/ChromaticPolynomial.html\"\u003eChromatic Polynomial\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Chromatic_polynomial\"\u003eChromatic polynomial\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1)."
			],
			"formula": [
				"a(n) = 2*n + a(n-1) - 1. - _Vincenzo Librandi_, Nov 12 2010",
				"G.f.: x*(-2+x)*(1-3*x)/(1-x)^3. - _Colin Barker_, Jan 29 2012"
			],
			"example": [
				"The quadratic factors of the characteristic polynomials of M_n for n = 1..6 are",
				"  x^2 -  2*x -  2,",
				"  x^2 -  4*x +  1,",
				"  x^2 -  6*x +  6,",
				"  x^2 -  8*x + 13,",
				"  x^2 - 10*x + 22,",
				"  x^2 - 12*x + 33."
			],
			"maple": [
				"with(combinat):seq(fibonacci(3, i)-4,i=1..55); # _Zerinvary Lajos_, Mar 20 2008"
			],
			"mathematica": [
				"M[n_] := {{n, -1, 0, 0, 0}, {-1, n, -1, 0, 0}, {0, -1, n, -1, 0}, {0, 0, -1, n, -1}, {0, 0, 0, -1, n}}; p[n_, x_] = Factor[CharacteristicPolynomial[M[n], x]] Table[ -3 + n^2, {n, 1, 25}]"
			],
			"program": [
				"(MAGMA) mat:=func\u003c n | Matrix(IntegerRing(), 5, 5, [\u003c i, j, i eq j select n else (i eq j+1 or i eq j-1) select -1 else 0 \u003e : i, j in [1..5] ]) \u003e; [ Coefficients(Factorization(CharacteristicPolynomial(mat(n)))[4][1])[1]:n in [1..50] ]; // _Klaus Brockhaus_, Nov 13 2010",
				"(PARI) A123968(n) = n^2-3   /* or: */",
				"(PARI) a(n)=polcoeff(factor(charpoly(matrix(5,5,i,j,if(abs(i-j)\u003e1,0,if(i==j,n,-1)))))[4,1], 0)"
			],
			"xref": [
				"Essentially the same: A028872, A267874."
			],
			"keyword": "sign,easy",
			"offset": "1,1",
			"author": "_Gary W. Adamson_ and _Roger L. Bagula_, Oct 29 2006",
			"ext": [
				"Edited and extended by _Klaus Brockhaus_, Nov 13 2010",
				"Definition simplified by _M. F. Hasler_, Nov 12 2010"
			],
			"references": 3,
			"revision": 49,
			"time": "2020-11-30T01:30:47-05:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}