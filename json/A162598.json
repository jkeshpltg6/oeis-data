{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A162598",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 162598,
			"data": "1,1,2,1,3,4,2,1,5,6,7,3,8,4,2,1,9,10,11,12,5,13,14,6,15,7,3,16,8,4,2,1,17,18,19,20,21,9,22,23,24,10,25,26,11,27,12,5,28,29,13,30,14,6,31,15,7,3,32,16,8,4,2,1,33,34,35,36,37,38,17,39,40,41,42,18,43,44,45,19,46,47",
			"name": "Ordinal transform of A265332.",
			"comment": [
				"This is a fractal sequence.",
				"It appears that each group of 2^k terms starts with 1 and ends with the remaining powers of two from 2^k down to 2^1.",
				"From _Antti Karttunen_, Jan 09-12 2016: (Start)",
				"This is ordinal transform of A265332, which is modified A051135 (with a(1) = 1, instead of 2). -  after _Franklin T. Adams-Watters_' original definition for this sequence.",
				"A000079 (powers of 2) indeed gives the positions of ones in this sequence. This follows from the properties (3) and (4) of A004001 given on page 227 of Kubo \u0026 Vakil paper (page 3 of PDF), which together also imply the pattern observed above, more clearly represented as:",
				"a(2) = 1.",
				"a(3..4) = 2, 1.",
				"a(6..8) = 4, 2, 1.",
				"a(13..16) = 8, 4, 2, 1.",
				"a(28..31) = 16, 8, 4, 2, 1.",
				"etc.",
				"(End)"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A162598/b162598.txt\"\u003eTable of n, a(n) for n = 1..8192\u003c/a\u003e",
				"T. Kubo and R. Vakil, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(94)00303-Z\"\u003eOn Conway's recursive sequence\u003c/a\u003e, Discr. Math. 152 (1996), 225-252.",
				"\u003ca href=\"/index/Ho#Hofstadter\"\u003eIndex entries for Hofstadter-type sequences\u003c/a\u003e"
			],
			"formula": [
				"Let b(1) = 1, b(n) = A051135(n) for n \u003e 1. Then a(n) is the number of b(k) that equal b(n) for 1 \u003c= k \u003c= n: sum( 1, 1\u003c=k\u003c=n and a(k)=a(n) ).",
				"If A265332(n) = 1, then a(n) = A004001(n), otherwise a(n) = a(A080677(n)-1) = a(n - A004001(n)). - _Antti Karttunen_, Jan 09 2016"
			],
			"mathematica": [
				"terms = 100;",
				"h[1] = 1; h[2] = 1;",
				"h[n_] := h[n] = h[h[n - 1]] + h[n - h[n - 1]];",
				"t = Array[h, 2*terms];",
				"A051135 = Take[Transpose[Tally[t]][[2]], terms];",
				"b[_] = 1;",
				"a[n_] := a[n] = With[{t = If[n == 1, 1, A051135[[n]]]}, b[t]++];",
				"Array[a, terms] (* _Jean-François Alcover_, Dec 19 2021, after _Robert G. Wilson v_ in A051135 *)"
			],
			"program": [
				"(Scheme, with memoization-macro definec)",
				"(definec (A162598 n) (if (= 1 (A265332 n)) (A004001 n) (A162598 (- (A080677 n) 1))))",
				";; _Antti Karttunen_, Jan 09 2016"
			],
			"xref": [
				"Cf. A004001, A051135, A080677, A087686.",
				"Row index of A265901, column index of A265903.",
				"Cf. A265332 (corresponding other index).",
				"Cf. A000079 (positions of ones).",
				"Cf. A000225 (from the term 3 onward the positions of 2's).",
				"Cf. A000325 (from its third term 5 onward the positions of 3's, which occur always as the last term before the next descending subsequence of powers of two)."
			],
			"keyword": "nonn,look",
			"offset": "1,3",
			"author": "_Franklin T. Adams-Watters_, Jul 07 2009",
			"ext": [
				"Name amended by _Antti Karttunen_, Jan 09 2016"
			],
			"references": 7,
			"revision": 22,
			"time": "2021-12-19T08:31:47-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}