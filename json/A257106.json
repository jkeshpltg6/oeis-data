{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A257106",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 257106,
			"data": "1,3,6,2,10,6,42,6,30,2,22,6,2730,6,6,2,170,6,798,6,330,2,46,6,2730,6,6,2,290,6,14322,6,510,2,2,6,1919190,6,6,2,4510,6,1806,6,690,2,94,6,46410,6,66,2,530,6,798,6,870,2,118,6,56786730,6,6,2,170,6",
			"name": "Denominators of the inverse binomial transform of the Bernoulli numbers with B(1)=2/3.",
			"comment": [
				"Difference table of Bernoulli numbers with B(1)=2/3:",
				"1,       2/3,    1/6,      0, -1/30,     0,  1/42, 0, ...",
				"-1/3,   -1/2,   -1/6,  -1/30,  1/30,  1/42, -1/42, ...",
				"-1/6,    1/3,   2/15,   1/15, -1/105, -1/21, ...",
				"1/2,    -1/5,  -1/15, -8/105, -4/105, ...",
				"-7/10,  2/15, -1/105,  4/105, ...",
				"5/6,    -1/7,   1/21, ...",
				"-41/42, 2/15, ...",
				"7/6, ...",
				"...",
				"First column: 1, -1/3, -1/6, 1/2, -7/10, 5/6, -41/42, 7/6, -41/30, 3/2, -35/22, 11/6, ... . a(n) is the n-th term of the denominators.",
				"Antidiagonal sums: 1, 1/3, -1/2, 2/3, -5/6, 1, -7/6, 4/3, -3/2, 5/3, -11/6, 2, ... . See A060789(n).",
				"a(2n+2)/a(2n+1) = 2, 5, 7, 5, 11, 455, ... .",
				"By definition, for B(1) = b, the inverse binomial transform is",
				"Bi(b) =       1,  -1 + b, 7/6 - 2*b, -3/2 + 3*b, 59/30 + 4*b, ...",
				"      = A176328(n)/A176591(n) - (-1)^n *n*b.",
				"With Bic(b) = 0, -1/2 + b, 1 - 2*b,  -3/2 + 3*b,   2 + 4*b, ...",
				"            = (-1)^n *(A001477(n)/2 - n*b),",
				"Bi(b) = (-1)^n *(A164555(n)/A027642(n) + A001477(n)/2 - n*b) =",
				"      = A027641(n)/A027642(n) + Bic(b) ."
			],
			"formula": [
				"Conjecture: a(2n+1) = 3 followed by period 3: repeat 2, 6, 6.",
				"Conjecture: a(2n) = A002445(n)/(period 3: repeat 1, 1, 3).",
				"a(n) = A027641(n)/A027642(n) - (-1)^n *n/6."
			],
			"example": [
				"a(0) = 1-0, a(1) = -1/2 +1/6 = -1/3, a(2) = 1/6 -1/3 = -1/6, a(3) = 0 +1/2."
			],
			"mathematica": [
				"max = 66; B[1] = 2/3; B[n_] := BernoulliB[n]; BB = Array[B, max, 0]; a[n_] := Differences[BB, n] // First // Denominator; Table[a[n], {n, 0, max-1}] (* _Jean-François Alcover_, May 11 2015 *)"
			],
			"program": [
				"(Sage)",
				"def A257106_list(len, B1) :",
				"    T = matrix(QQ, 2*len+1)",
				"    for m in (0..2*len) :",
				"        T[0, m] = bernoulli_polynomial(1, m) if m \u003c\u003e 1 else B1",
				"        for k in range(m-1, -1, -1) :",
				"            T[m-k, k] = T[m-k-1, k+1] - T[m-k-1, k]",
				"    return [denominator(T[k, 0]) for k in (0..len-1)]",
				"A257106_list(66, 2/3) # _Peter Luschny_, May 09 2015"
			],
			"xref": [
				"Cf. A256595, A027641/A027642(n), A164555(n)/A027642(n), A060789, A176328/A176591, A001477, A109007."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Paul Curtz_, Apr 23 2015",
			"references": 1,
			"revision": 40,
			"time": "2021-11-22T10:25:46-05:00",
			"created": "2015-05-12T06:25:21-04:00"
		}
	]
}