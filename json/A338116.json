{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A338116",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 338116,
			"data": "1,1,3,3,0,1,26,306,1400,2800,2520,840,0,0,0,1,766,199902,10426768,200588850,1903776420,10360383600,35133957600,77643846000,113816253600,109880971200,67199932800,23610787200,3632428800,0,0,0,0,0,0",
			"name": "Triangle read by rows: T(n,k) is the number of achiral colorings of the faces (and peaks) of a regular n-dimensional simplex using exactly k colors. Row n has C(n+1,3) columns.",
			"comment": [
				"An n-dimensional simplex has n+1 vertices, C(n+1,3) faces, and C(n+1,3) peaks, which are (n-3)-dimensional simplexes. For n=2, the figure is a triangle with one face. For n=3, the figure is a tetrahedron with four triangular faces and four peaks (vertices). For n=4, the figure is a 4-simplex with ten triangular faces and ten peaks (edges). The Schläfli symbol {3,...,3}, of the regular n-dimensional simplex consists of n-1 3's. An achiral coloring is identical to its reflection.",
				"The algorithm used in the Mathematica program below assigns each permutation of the vertices to a cycle-structure partition of n+1. It then determines the number of permutations for each partition and the cycle index for each partition. If the value of m is increased, one can enumerate colorings of higher-dimensional elements beginning with T(m,1)."
			],
			"link": [
				"G. Royle, \u003ca href=\"http://teaching.csse.uwa.edu.au/units/CITS7209/partition.pdf\"\u003ePartitions and Permutations\u003c/a\u003e"
			],
			"formula": [
				"A337886(n,k) = Sum_{j=1..C(n+1,3)} T(n,j) * binomial(k,j).",
				"T(n,k) = 2*A338114(n,k) - A338113(n,k) = A338113(n,k) - 2*A338115(n,k) = A338114(n,k) - A338115(n,k).",
				"T(3,k) = A325003(3,k); T(4,k) = A327090(4,k)."
			],
			"example": [
				"Triangle begins with T(2,1):",
				"  1",
				"  1   3      3        0",
				"  1  26    306     1400      2800       2520         840           0   0   0",
				"  1 766 199902 10426768 200588850 1903776420 10360383600 35133957600 ...",
				"  ...",
				"For T(3,3)=3, one of the three colors appears on two faces (vertices) of the tetrahedron."
			],
			"mathematica": [
				"m=2; (* dimension of color element, here a triangular face *)",
				"lw[n_, k_]:=lw[n, k]=DivisorSum[GCD[n, k], MoebiusMu[#]Binomial[n/#, k/#]\u0026]/n (*A051168*)",
				"cxx[{a_, b_}, {c_, d_}]:={LCM[a, c], GCD[a, c] b d}",
				"compress[x:{{_, _} ...}] := (s=Sort[x]; For[i=Length[s], i\u003e1, i-=1, If[s[[i, 1]]==s[[i-1, 1]], s[[i-1, 2]]+=s[[i, 2]]; s=Delete[s, i], Null]]; s)",
				"combine[a : {{_, _} ...}, b : {{_, _} ...}] := Outer[cxx, a, b, 1]",
				"CX[p_List, 0] := {{1, 1}} (* cycle index for partition p, m vertices *)",
				"CX[{n_Integer}, m_] := If[2m\u003en, CX[{n}, n-m], CX[{n}, m] = Table[{n/k, lw[n/k, m/k]}, {k, Reverse[Divisors[GCD[n, m]]]}]]",
				"CX[p_List, m_Integer] := CX[p, m] = Module[{v = Total[p], q, r}, If[2 m \u003e v, CX[p, v - m], q = Drop[p, -1]; r = Last[p]; compress[Flatten[Join[{{CX[q, m]}}, Table[combine[CX[q, m - j], CX[{r}, j]], {j, Min[m, r]}]], 2]]]]",
				"pc[p_] := Module[{ci, mb}, mb = DeleteDuplicates[p]; ci = Count[p, #] \u0026/@ mb; Total[p]!/(Times @@ (ci!) Times @@ (mb^ci))] (* partition count *)",
				"row[n_Integer] := row[n] = Factor[Total[If[OddQ[Total[1-Mod[#, 2]]], pc[#] j^Total[CX[#, m+1]][[2]], 0] \u0026 /@ IntegerPartitions[n+1]]/((n+1)!/2)]",
				"array[n_, k_] := row[n] /. j -\u003e k",
				"Table[LinearSolve[Table[Binomial[i,j],{i,Binomial[n+1,m+1]},{j,Binomial[n+1,m+1]}], Table[array[n,k],{k,Binomial[n+1,m+1]}]], {n,m,m+4}] // Flatten"
			],
			"xref": [
				"Cf. A338113 (oriented), A338114 (unoriented), A338115 (chiral), A337886 (k or fewer colors), A325003 (vertices and facets), A327090 (edges and ridges)."
			],
			"keyword": "nonn,tabf",
			"offset": "2,3",
			"author": "_Robert A. Russell_, Oct 10 2020",
			"references": 3,
			"revision": 12,
			"time": "2020-10-14T10:38:31-04:00",
			"created": "2020-10-14T10:38:31-04:00"
		}
	]
}