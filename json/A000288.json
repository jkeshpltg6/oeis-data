{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000288",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 288,
			"id": "M3307 N1332",
			"data": "1,1,1,1,4,7,13,25,49,94,181,349,673,1297,2500,4819,9289,17905,34513,66526,128233,247177,476449,918385,1770244,3412255,6577333,12678217,24438049,47105854,90799453,175021573,337364929,650291809,1253477764",
			"name": "Tetranacci numbers: a(n) = a(n-1) + a(n-2) + a(n-3) + a(n-4) with a(0) = a(1) = a(2) = a(3) = 1.",
			"comment": [
				"The \"standard\" Tetranacci numbers with initial terms (0,0,0,1) are listed in A000078. - _M. F. Hasler_, Apr 20 2018",
				"For n\u003e=0: a(n+2) is the number of length-n strings with letters {0,1,2,3} where the letter x is followed by at least x zeros, see Fxtbook link. [_Joerg Arndt_, Apr 08 2011]",
				"Satisfies Benford's law [see A186191]. - _N. J. A. Sloane_, Feb 09 2017"
			],
			"reference": [
				"Michel Rigo, Formal Languages, Automata and Numeration Systems, 2 vols., Wiley, 2014. Mentions this sequence - see \"List of Sequences\" in Vol. 2.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Indranil Ghosh, \u003ca href=\"/A000288/b000288.txt\"\u003eTable of n, a(n) for n = 0..3503\u003c/a\u003e (terms 0..200 from T. D. Noe)",
				"Joerg Arndt, \u003ca href=\"http://www.jjj.de/fxt/#fxtbook\"\u003eMatters Computational (The Fxtbook)\u003c/a\u003e, pp.311-312.",
				"B. G. Baumgart, Letter to the editor, \u003ca href=\"http://www.fq.math.ca/Scanned/2-4/baumgart-a.pdf\"\u003ePart 1\u003c/a\u003e, \u003ca href=\"http://www.fq.math.ca/Scanned/2-4/baumgart-b.pdf\"\u003ePart 2\u003c/a\u003e, \u003ca href=\"http://www.fq.math.ca/Scanned/2-4/baumgart-c.pdf\"\u003ePart 3\u003c/a\u003e, Fib. Quart. 2 (1964), 260, 302.",
				"Martin Burtscher, Igor Szczyrba, Rafał Szczyrba, \u003ca href=\"http://www.emis.de/journals/JIS/VOL18/Szczyrba/sz3.html\"\u003eAnalytic Representations of the n-anacci Constants and Generalizations Thereof\u003c/a\u003e, Journal of Integer Sequences, Vol. 18 (2015), Article 15.4.5.",
				"W. C. Lynch, \u003ca href=\"http://www.fq.math.ca/Scanned/8-1/lynch.pdf\"\u003eThe t-Fibonacci numbers and polyphase sorting\u003c/a\u003e, Fib. Quart., 8 (1970), pp. 6ff.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,1,1,1).",
				"\u003ca href=\"/index/Be#Benford\"\u003eIndex entries for sequences related to Benford's law\u003c/a\u003e"
			],
			"formula": [
				"[a(n), a(n+1), a(n+2), a(n+3)]' = (M^n)*[1 1 1 1]', where M = the 4 X 4 matrix [0 1 0 0 / 0 0 1 0 / 0 0 0 1 / 1 1 1 1]. E.g. [7 13 25 49]' = (M^5)*[1 1 1 1]' = [a(5), a(6), a(7), a(8)]'. Here the prime denotes transpose. - _Gary W. Adamson_, Feb 22 2004.",
				"a(0) = a(1) = a(2) = a(3) = 1, a(4) = 4, a(n) = 2*a(n-1) - a(n-5). - _Vincenzo Librandi_, Dec 21 2010",
				"a(n) = -2*A000078(n)-A000078(n+1)+A000078(n+3). - _R. J. Mathar_, Apr 07 2011",
				"G.f.: (1 - x^2 - 2*x^3) / (1 - x - x^2 - x^3 - x^4) = 1 / (1 - x / (1 - 3*x^3 / (1 - x^2 / (1 + x / (1 - x))))). - _Michael Somos_, May 12 2012",
				"G.f. A(x) = 1 + x / (1 - x / (1 - 3 * x^2 / (1 + 2 * x^2))). - _Michael Somos_, Jan 04 2013"
			],
			"example": [
				"G.f. = 1 + x + x^2 + x^3 + 4*x^4 + 7*x^5 + 13*x^6 + 25*x^7 + 49*x^8 + ..."
			],
			"maple": [
				"A000288:=(-1+z**2+2*z**3)/(-1+z**2+z**3+z+z**4); # _Simon Plouffe_ in his 1992 dissertation"
			],
			"mathematica": [
				"a[0] = a[1] = a[2] = a[3] = 1; a[n_] := a[n] = a[n - 1] + a[n - 2] + a[n - 3] + a[n - 4]; Table[ a[n], {n, 0, 34}] (* _Robert G. Wilson v_, Oct 27 2005 *)",
				"LinearRecurrence[{1,1,1,1},{1,1,1,1},30] (* _Harvey P. Dale_, May 23 2011 *)",
				"a[ n_] := If[ n \u003c 0, SeriesCoefficient[ x (-2 - x + x^3) / (1 + x + x^2 + x^3 - x^4), {x, 0, -n}], SeriesCoefficient[ (1 - x^2 - 2 x^3) / (1 - x - x^2 - x^3 - x^4), {x, 0, n}]]; (* _Michael Somos_, Aug 15 2015 *)"
			],
			"program": [
				"(Maxima) A000288[0]:1$ A000288[1]:1$ A000288[2]:1$ A000288[3]:1$ A000288[n]:=A000288[n-1] + A000288[n-2]+ A000288[n-3] + A000288[n-4]$ makelist(A000288[n],n,0,30); /* _Martin Ettl_, Oct 25 2012 */",
				"(PARI) {a(n) = if( n\u003c0, n = -n; polcoeff( x*(-2 - x + x^3) / (1 + x + x^2 + x^3 - x^4) + x*O(x^n), n), polcoeff( (1 - x^2 - 2*x^3) / (1 - x - x^2 - x^3 - x^4) + x*O(x^n), n))}; /* _Michael Somos_, Jan 04 2013 */"
			],
			"xref": [
				"Cf. A060455.",
				"Cf. A000078: Tetranacci numbers with a(0) = a(1) = a(2) = 0, a(3) = 1."
			],
			"keyword": "nonn,easy",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Robert G. Wilson v_, Oct 27 2005"
			],
			"references": 71,
			"revision": 101,
			"time": "2021-03-12T22:32:33-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}