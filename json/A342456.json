{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A342456",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 342456,
			"data": "2,3,5,9,7,25,35,15,11,49,117649,625,717409,1225,55,225,13,121,1771561,2401,36226650889,184877,1127357,875,902613283,514675673281,3780549773,1500625,83852850675321384784127,3025,62004635,21,17,169,4826809,14641,8254129,143,2924207,77,8223741426987700773289,59797108943,546826709",
			"name": "A276086 applied to the primorial inflation of Doudna-tree, where A276086(n) is the prime product form of primorial base expansion of n.",
			"comment": [
				"This sequence (which could be viewed as a binary tree, like the underlying A005940 and A329886) is similar to A324289, but unlike its underlying tree A283477 that generates only numbers that are products of distinct primorial numbers (i.e., terms of A129912), here the underlying tree A329886 generates all possible products of primorial numbers, i.e., terms of A025487, but in different order."
			],
			"link": [
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#primorialbase\"\u003eIndex entries for sequences related to primorial base\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#primorial_numbers\"\u003eIndex entries for sequences related to primorial numbers\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A276086(A329886(n)) = A324886(A005940(1+n)).",
				"For all n \u003e= 0, gcd(a(n), A329886(n)) = 1.",
				"For all n \u003e= 1, A055396(a(n))-1 = A061395(A329886(n)) = A290251(n) = 1+A080791(n).",
				"For all n \u003e= 0, a(2^n) = A000040(2+n)."
			],
			"mathematica": [
				"Block[{a, f, r = MixedRadix[Reverse@ Prime@ Range@ 24]}, f[n_] :=",
				"Times @@ MapIndexed[Prime[First[#2]]^#1 \u0026, Reverse@ IntegerDigits[n, r]]; a[0] = 1; a[1] = 2; a[n_] := a[n] = If[EvenQ@ n, (Times @@ Map[Prime[PrimePi@ #1 + 1]^#2 \u0026 @@ # \u0026, FactorInteger[#]] - Boole[# == 1])*2^IntegerExponent[#, 2] \u0026[a[n/2]], 2 a[(n - 1)/2]]; Array[f@ a[#] \u0026, 43, 0]] (* _Michael De Vlieger_, Mar 17 2021 *)"
			],
			"program": [
				"(PARI)",
				"A276086(n) = { my(m=1, p=2); while(n, m *= (p^(n%p)); n = n\\p; p = nextprime(1+p)); (m); };",
				"A283980(n) = {my(f=factor(n)); prod(i=1, #f~, my(p=f[i, 1], e=f[i, 2]); if(p==2, 6, nextprime(p+1))^e)};",
				"A329886(n) = if(n\u003c2,1+n,if(!(n%2),A283980(A329886(n/2)),2*A329886(n\\2)));",
				"A342456(n) = A276086(A329886(n));"
			],
			"xref": [
				"Cf. A005940, A025487, A108951, A129912, A276086, A283980, A324886, A342457 [= 2*A246277(a(n))], A342461 [= A001221(a(n))], A342462 [= A001222(a(n))], A342463 [= A342001(a(n))], A342464 [= A051903(a(n))].",
				"Cf. A324289 (a subset of these terms, in different order)."
			],
			"keyword": "nonn",
			"offset": "0,1",
			"author": "_Antti Karttunen_ and _Michael De Vlieger_, Mar 14 2021",
			"references": 6,
			"revision": 18,
			"time": "2021-03-20T10:55:49-04:00",
			"created": "2021-03-20T10:55:49-04:00"
		}
	]
}