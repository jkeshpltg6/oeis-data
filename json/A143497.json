{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A143497",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 143497,
			"data": "1,4,1,20,10,1,120,90,18,1,840,840,252,28,1,6720,8400,3360,560,40,1,60480,90720,45360,10080,1080,54,1,604800,1058400,635040,176400,25200,1890,70,1,6652800,13305600,9313920,3104640,554400,55440,3080,88,1",
			"name": "Triangle of unsigned 2-Lah numbers.",
			"comment": [
				"For a signed version of this triangle see A062137. The unsigned 2-Lah number L(2;n,k) gives the number of partitions of the set {1,2,...,n} into k ordered lists with the restriction that the elements 1 and 2 must belong to different lists. More generally, the unsigned r-Lah number L(r;n,k) gives the number of partitions of the set {1,2,...,n} into k ordered lists with the restriction that the elements 1, 2, ..., r belong to different lists. If r = 1 there is no restriction and we obtain the unsigned Lah numbers A105278. For other cases see A143498 (r = 3) and A143499 (r = 4). We make some remarks on the general case.",
				"The unsigned r-Lah numbers occur as connection constants in the generalized Lah identity (x+2r-1)*(x+2r)*...*(x+2r+n-r-2) = Sum_{k = r..n} L(r;n,k)*(x-1)*(x-2)*...*(x-k+r) for n \u003e=r and where any empty products are taken equal to 1 (for a bijective proof of the identity, follow the proof of [Petkovsek and Pisanski] but restrict the first r of the Argonauts to different paths).",
				"The unsigned r-Lah numbers satisfy the same recurrence as the unsigned Lah numbers, namely, L(r;n,k) = (n+k-1)*L(r;n-1,k) + L(r;n-1,k-1), but with the boundary conditions: L(r;n,k) = 0 if n \u003c r or if k \u003c r; L(r;r,r) = 1. The recurrence has the explicit solution L(r;n,k) = (n-r)!/(k-r)!*binomial(n+r-1,k+r-1) for n,k \u003e= r. It follows that the unsigned r-Lah numbers have 'vertical' generating functions for k \u003e= r of the form Sum_{n \u003e= k} L(r;n,k)*t^n/(n-r)! = 1/(k-r)!*t^k/(1-t)^(k+r). This yields the e.g.f. for the array of unsigned r-restricted Lah numbers in the form: Sum_{n,k \u003e= r} L(r;n,k)*x^k*t^n/(n-r)! = (x*t)^r * 1/(1-t)^(2*r) * exp(x*t/(1-t)) = (x*t)^r (1 + (2*r+x)*t + (2r*(2*r+1) + 2*(2*r+1)*x + x^2)*t^2/2! + ... ). The array of unsigned r-Lah numbers begins",
				"1...................0..................0.............0...",
				"2r..................1..................0.............0...",
				"2r*(2r+1)...........2*(2r+1)...........1.............0...",
				"2r*(2r+1)*(2r+2)....3*(2r+1)*(2r+2)....3*(2r+2)......1...",
				"...",
				"and equals exp(D(r)), where D(r) is the array with the sequence (2*r, 2*(2*r+1), 3*(2*r+2), 4*(2*r+3), ... ) on the main subdiagonal and zeros everywhere else.",
				"The unsigned r-Lah numbers are related to the r-Stirling numbers: the lower triangular array of unsigned r-Lah numbers may be expressed as the matrix product St1(r) * St2(r), where St1(r) and St2(r) denote the arrays of r-Stirling numbers of the first and second kind respectively. The theory of r-Stirling numbers is developed in [Broder]. See A143491 - A143496 for tables of r-Stirling numbers. An alternative factorization for the array is as St1 * P^(2r-2) * St2, where P denotes Pascal's triangle, A007318, St1 is the triangle of unsigned Stirling numbers of the first kind, abs(A008275) and St2 denotes the triangle of Stirling numbers of the second kind, A008277 (apply Theorem 10 of [Neuwirth]).",
				"The array of unsigned r-Lah numbers is an example of the fundamental matrices sketched in A133314. So redefining the offset as n=0, given matrices A and B with A(n,k) = T(n,k)*a(n-k) and B(n,k) = T(n,k)*b(n-k), then A*B = C where C(n,k) = T(n,k)*[a(.)+b(.)]^(n-k), umbrally. An e.g.f. for the row polynomials of A is exp(x*t) exp{-x*t*[a*t/(a*t-1)]}/(1-a*t)^4 = exp(x*t) exp[(.)!*Laguerre(.,3,-x*t)*a(.)*t)], umbrally. [From _Tom Copeland_, Sep 19 2008]"
			],
			"link": [
				"Muniru A Asiru, \u003ca href=\"/A143497/b143497.txt\"\u003eTable of n, a(n) for n = 2..4951\u003c/a\u003e Rows n = 2..100",
				"A. Z. Broder, \u003ca href=\"http://infolab.stanford.edu/TR/CS-TR-82-949.html\"\u003eThe r-Stirling numbers\u003c/a\u003e, Report CS-TR-82-949, Stanford University, Department of Computer Science, 1982.",
				"A. Z. Broder, \u003ca href=\"https://doi.org/10.1016/0012-365X(84)90161-4\"\u003eThe r-Stirling numbers\u003c/a\u003e, Discrete Math. 49, 241-259 (1984).",
				"Gi-Sang Cheon and Ji-Hwan Jung, \u003ca href=\"https://doi.org/10.1016/j.disc.2012.04.001\"\u003er-Whitney numbers of Dowling lattices\u003c/a\u003e, Discrete Math., 312 (2012), 2337-2348.",
				"Erich Neuwirth, \u003ca href=\"http://homepage.univie.ac.at/erich.neuwirth/papers/TechRep99-05.pdf\"\u003eRecursively defined combinatorial functions: Extending Galton's board\u003c/a\u003e, Tech Report TR 99-05, 1999.",
				"Erich Neuwirth, \u003ca href=\"https://doi.org/10.1016/S0012-365X(00)00373-3\"\u003eRecursively defined combinatorial functions: Extending Galton's board\u003c/a\u003e, Discrete Math. 239 No. 1-3, 33-51 (2001).",
				"G. Nyul, G. Rácz, \u003ca href=\"https://doi.org/10.1016/j.disc.2014.03.029\"\u003eThe r-Lah numbers\u003c/a\u003e, Discrete Mathematics, 338 (2015), 1660-1666.",
				"Marko Petkovsek, Tomaz Pisanski, \u003ca href=\"http://preprinti.imfm.si/PDF/00837.pdf\"\u003eCombinatorial interpretation of unsigned Stirling and Lah numbers\u003c/a\u003e, University of Ljubljana, Preprint series, Vol. 40 (2002), 837.",
				"Jose L. Ramirez, M. Shattuck, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL19/Ramirez2/ramirez12.html\"\u003eA (p, q)-Analogue of the r-Whitney-Lah Numbers\u003c/a\u003e, Journal of Integer Sequences, 19, 2016, #16.5.6.",
				"Michael J. Schlosser and Meesue Yoo, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v24i1p31\"\u003eElliptic Rook and File Numbers\u003c/a\u003e, Electronic Journal of Combinatorics, 24(1) (2017), #P1.31.",
				"M. Shattuck, \u003ca href=\"https://arxiv.org/abs/1412.8721\"\u003eGeneralized r-Lah numbers\u003c/a\u003e, arXiv:1412.8721 [math.CO], 2014."
			],
			"formula": [
				"T(n,k) = (n-2)!/(k-2)!*C(n+1,k+1), n,k \u003e= 2.",
				"Recurrence: T(n,k) = (n+k-1)*T(n-1,k) + T(n-1,k-1) for n,k \u003e= 2, with the boundary conditions: T(n,k) = 0 if n \u003c 2 or k \u003c 2; T(2,2) = 1.",
				"E.g.f. for column k: Sum_{n \u003e= k} T(n,k)*t^n/(n-2)! = 1/(k-2)!*t^k/(1-t)^(k+2) for k \u003e= 2.",
				"E.g.f: Sum_{n = 2..inf} Sum_{k = 2..n} T(n,k)*x^k*t^n/(n-2)! = (x*t)^2/(1-t)^4*exp(x*t/(1-t)) = (x*t)^2*(1 + (4+x)*t +(20+10*x+x^2)*t^2/2! + ... ).",
				"Generalized Lah identity: (x+3)*(x+4)*...*(x+n) = Sum_{k = 2..n} T(n,k)*(x-1)*(x-2)*...*(x-k+2).",
				"The polynomials 1/n!*Sum_{k = 2..n+2} T(n+2,k)*(-x)^(k-2) for n \u003e= 0 are the generalized Laguerre polynomials Laguerre(n,3,x). See A062137.",
				"Array = A143491 * A143494 = abs(A008275) * ( A007318 )^2 * A008277 (apply Theorem 10 of [Neuwirth]). Array equals exp(D), where D is the array with the quadratic sequence (4,10,18,28, ... ) on the main subdiagonal and zeros elsewhere.",
				"After adding 1 to the head of the main diagonal and a zero to each of the subdiagonals, the n-th diagonal may be generated as coefficients of (1/n!) [D^(-1) tDt t^(-3)D t^3]^n exp(x t), where D is the derivative w.r.t. t and D^(-1) t^j/j! = t^(j+1)/(j+1)!. E.g., n=2 generates 20 x t^3/3! + 90 x^2 t^4/4! + 252 x^3 t^5/5! + ... . For the general unsigned r-Lah number array, replace the threes by (2r-1) in the operator. The e.g.f. of the row polynomials is then exp[D^(-1) tDt t^(-(2r-1))D t^(2r-1)] exp(x t), with offset n=0. [From _Tom Copeland_, Sep 21 2008]"
			],
			"example": [
				"Triangle begins",
				"n\\k|.....2.....3.....4.....5.....6.....7",
				"========================================",
				"2..|.....1",
				"3..|.....4.....1",
				"4..|....20....10.....1",
				"5..|...120....90....18.....1",
				"6..|...840...840...252....28.....1",
				"7..|..6720..8400..3360...560....40.....1",
				"...",
				"T(4,3) = 10. The ten partitions of {1,2,3,4} into 3 ordered lists such that the elements 1 and 2 lie in different lists are: {1}{2}{3,4} and {1}{2}{4,3}, {1}{3}{2,4} and {1}{3}{4,2}, {1}{4}{2,3} and {1}{4}{3,2}, {2}{3}{1,4} and {2}{3}{4,1}, {2}{4}{1,3} and {2}{4}{3,1}. The remaining two partitions {3}{4}{1,2} and {3}{4}{2,1} are not allowed because the elements 1 and 2 belong to the same block."
			],
			"maple": [
				"with combinat: T := (n, k) -\u003e (n-2)!/(k-2)!*binomial(n+1,k+1): for n from 2 to 11 do seq(T(n, k), k = 2..n) end do;"
			],
			"mathematica": [
				"T[n_, k_] := (n-2)!/(k-2)!*Binomial[n+1, k+1]; Table[T[n, k], {n,2,10}, {k,2,n}] // Flatten (* _Amiram Eldar_, Nov 27 2018 *)"
			],
			"program": [
				"(Maxima) create_list((n - 2)!/(k - 2)!*binomial(n + 1, k + 1), n, 2, 12, k, 2, n); /* _Franck Maminirina Ramaharo_, Nov 27 2018 */",
				"(GAP) T:=Flat(List([2..10],n-\u003eList([2..n],k-\u003e(Factorial(n-2)/Factorial(k-2))*Binomial(n+1,k+1)))); # _Muniru A Asiru_, Nov 27 2018"
			],
			"xref": [
				"Cf. A001715 (column 2), A007318, A008275, A008277, A061206 (column 3), A062137, A062141 - A062144 ( column 4 to column 7), A062146 (alt. row sums), A062147 (row sums), A105278 (unsigned Lah numbers), A143491, A143494, A143498, A143499."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "2,2",
			"author": "_Peter Bala_, Aug 25 2008",
			"references": 7,
			"revision": 37,
			"time": "2018-11-27T09:12:14-05:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}