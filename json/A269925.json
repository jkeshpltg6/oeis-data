{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A269925",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 269925,
			"data": "59520825,4304016990,4304016990,158959754226,354949166565,158959754226,4034735959800,14805457339920,14805457339920,4034735959800,79553497760100,420797306522502,691650582088536,420797306522502,79553497760100,1302772718028600,9220982517965400,21853758736216200,21853758736216200,9220982517965400,1302772718028600",
			"name": "Triangle read by rows: T(n,f) is the number of rooted maps with n edges and f faces on an orientable surface of genus 5.",
			"comment": [
				"Row n contains n-9 terms."
			],
			"link": [
				"Gheorghe Coserea, \u003ca href=\"/A269925/b269925.txt\"\u003eRows n = 10..210, flattened\u003c/a\u003e",
				"Sean R. Carrell, Guillaume Chapuy, \u003ca href=\"http://arxiv.org/abs/1402.6300\"\u003eSimple recurrence formulas to count maps on orientable surfaces\u003c/a\u003e, arXiv:1402.6300 [math.CO], 2014."
			],
			"example": [
				"Triangle starts:",
				"n\\f  [1]             [2]             [3]             [4]",
				"[10] 59520825;",
				"[11] 4304016990,     4304016990;",
				"[12] 15895975422,    354949166565,   158959754226;",
				"[13] 4034735959800,  14805457339920, 14805457339920, 4034735959800;",
				"[14] ..."
			],
			"mathematica": [
				"Q[0, 1, 0] = 1; Q[n_, f_, g_] /; n\u003c0 || f\u003c0 || g\u003c0 = 0;",
				"Q[n_, f_, g_] := Q[n, f, g] = 6/(n+1)((2n-1)/3 Q[n-1, f, g] + (2n-1)/3 Q[n - 1, f-1, g] + (2n-3)(2n-2)(2n-1)/12 Q[n-2, f, g-1] + 1/2 Sum[l = n-k; Sum[v = f-u; Sum[j = g-i; Boole[l \u003e= 1 \u0026\u0026 v \u003e= 1 \u0026\u0026 j \u003e= 0] (2k-1)(2l-1) Q[k-1, u, i] Q[l-1, v, j], {i, 0, g}], {u, 1, f}], {k, 1, n}]);",
				"Table[Q[n, f, 5], {n, 10, 15}, {f, 1, n-9}] // Flatten (* _Jean-François Alcover_, Aug 10 2018 *)"
			],
			"program": [
				"(PARI)",
				"N = 15; G = 5; gmax(n) = min(n\\2, G);",
				"Q = matrix(N + 1, N + 1);",
				"Qget(n, g) = { if (g \u003c 0 || g \u003e n/2, 0, Q[n+1, g+1]) };",
				"Qset(n, g, v) = { Q[n+1, g+1] = v };",
				"Quadric({x=1}) = {",
				"  Qset(0, 0, x);",
				"  for (n = 1, length(Q)-1, for (g = 0, gmax(n),",
				"    my(t1 = (1+x)*(2*n-1)/3 * Qget(n-1, g),",
				"       t2 = (2*n-3)*(2*n-2)*(2*n-1)/12 * Qget(n-2, g-1),",
				"       t3 = 1/2 * sum(k = 1, n-1, sum(i = 0, g,",
				"       (2*k-1) * (2*(n-k)-1) * Qget(k-1, i) * Qget(n-k-1, g-i))));",
				"    Qset(n, g, (t1 + t2 + t3) * 6/(n+1))));",
				"};",
				"Quadric('x);",
				"concat(apply(p-\u003eVecrev(p/'x), vector(N+1 - 2*G, n, Qget(n-1 + 2*G, G))))"
			],
			"xref": [
				"Rooted maps of genus 5 with n edges and f faces for 1\u003c=f\u003c=10: A288281 f=1, A288282 f=2, A288283 f=3, A288284 f=4, A288285 f=5, A288286 f=6, A288287 f=7, A288288 f=8, A288289 f=9, A288290 f=10.",
				"Row sums give A238355 (column 5 of A269919).",
				"Cf. A035309, A269921, A269922, A269923, A269924, A270406, A270407, A270408, A270409, A270410, A270411, A270412."
			],
			"keyword": "nonn,tabl",
			"offset": "10,1",
			"author": "_Gheorghe Coserea_, Mar 15 2016",
			"references": 13,
			"revision": 23,
			"time": "2018-08-10T17:36:44-04:00",
			"created": "2016-03-16T12:06:15-04:00"
		}
	]
}