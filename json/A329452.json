{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A329452",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 329452,
			"data": "0,1,2,8,4,5,6,3,7,11,10,9,12,13,28,15,17,16,20,14,21,22,19,23,25,24,29,30,26,18,35,31,32,27,34,36,33,38,37,40,63,39,41,44,42,45,47,50,51,43,52,49,46,48,53,54,57,55,56,58,69,62,59,65,66,61,60,67,64,68,70,81,72,76,73,75,71",
			"name": "There are exactly two primes in {a(n+i) + a(n+j), 0 \u003c= i \u003c j \u003c= 3} for any n: lexicographically earliest such sequence of distinct nonnegative integers.",
			"comment": [
				"That is, there are exactly two primes among the 6 pairwise sums of any four consecutive terms.",
				"Conjectured to be a permutation of the nonnegative numbers.",
				"a(100) = 97, a(1000) = 1001, a(10^4) = 9997, a(10^5) = 10^5, a(10^6) = 999984 and all numbers below 999963 have appeared at that point.",
				"See the wiki page for considerations about existence and surjectivity of the sequence and variants thereof."
			],
			"link": [
				"Eric Angelini, \u003ca href=\"http://cinquantesignes.blogspot.com/2019/11/prime-sums-from-neighbouring-terms.html\"\u003ePrime sums from neighbouring terms\u003c/a\u003e, personal blog \"Cinquante signes\" (and post to the SeqFan list), Nov. 11, 2019.",
				"M. F. Hasler, \u003ca href=\"/wiki/User:M._F._Hasler/Prime_sums_from_neighboring_terms\"\u003ePrime sums from neighboring terms\u003c/a\u003e, OEIS wiki, Nov. 23, 2019"
			],
			"example": [
				"We start with a(0) = 0, a(1) = 1, a(2) = 2, the smallest possibilities which do not lead to a contradiction.",
				"Now there are already 2 primes, 0 + 2 and 1 + 2, among the pairwise sums, so the next term must not generate any further prime. Given 0 and 1, primes and (primes - 1) are excluded, and a(3) = 8 is the smallest possible choice.",
				"Now there is only one prime, 1 + 2 = 3, among the pairwise sums using {1, 2, 8}; the next term must produce exactly one additional prime as sum with these. We see that 3 is not possible (2 + 3 = 5 and 8 + 3 = 11), but a(4) = 4 is possible.",
				"Now using {2, 8, 4} we have no prime as a pairwise sum, so the next term must produce two primes among the sums with these terms. Again, 3 would give three primes, but 5 yields exactly two primes, 2 + 5 = 7 and 8 + 5 = 13."
			],
			"program": [
				"(PARI) A329452(n,show=0,o=0,p=[],U,u=o)={for(n=o,n-1, show\u0026\u0026print1(o\",\"); U+=1\u003c\u003c(o-u); U\u003e\u003e=-u+u+=valuation(U+1,2); p=concat(if(2\u003c#p,p[^1],p),o); my(c=2-sum(i=2,#p,sum(j=1,i-1,isprime(p[i]+p[j])))); if(#p\u003c3, o=u;next); for(k=u,oo, bittest(U,k-u) || sum(i=1,#p,isprime(p[i]+k))!=c || [o=k, break]));print([u]);o} \\\\ Optional args: show=1: print a(o..n-1); o=1: use indices \u0026 terms \u003e= 1, i.e., compute A329412. See the wiki page for more general code returning a vector: S(n,2,4) = a(0..n-1)."
			],
			"xref": [
				"Cf. A329412 (analog for positive integers), A329453 (2 primes in a(n+i)+a(n+j), i \u003c j \u003c 5).",
				"Cf. A329333 (one odd prime among a(n+i)+a(n+j), 0 \u003c= i \u003c j \u003c 3), A329450 (no prime in a(n+i)+a(n+j), i \u003c j \u003c 3)."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_M. F. Hasler_, Nov 15 2019",
			"ext": [
				"Edited (deleted comments now found on the wiki) by _M. F. Hasler_, Nov 24 2019"
			],
			"references": 29,
			"revision": 23,
			"time": "2019-12-01T05:43:40-05:00",
			"created": "2019-11-16T02:30:40-05:00"
		}
	]
}