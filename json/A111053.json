{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A111053",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 111053,
			"data": "1,2,6,22,89,379,1661,7405,33367,151398,690147,3156112,14465746,66409493,305232025,1404129530,6463476538,29767212095,137142651679,632021380433,2913316615372,13431328632593,61931182541194,285592218851606,1317104663887309,6074682489939359,28018852961838675,129239701278757210",
			"name": "Number of permutations which avoid the patterns 1324 and (2143 with Bruhat restriction {2\u003c-\u003e3}). Also the number of permutations whose graphs are acyclic.",
			"reference": [
				"S. Kitaev, Patterns in Permutations and Words, Springer-Verlag, 2011. See p. 399, Table A.7."
			],
			"link": [
				"H. Abe and S. Billey, \u003ca href=\"http://www.math.washington.edu/~billey/papers/abe.billey.pdf\"\u003eConsequences of the Lakshmibai-Sandhya theorem: the ubiquity of permutation patterns in Schubert calculus and related geometry\u003c/a\u003e, 2014.",
				"M. Bousquet-Mélou and S. Butler, \u003ca href=\"https://arxiv.org/abs/math/0603617\"\u003eForest-like permutations\u003c/a\u003e, arXiv:math/0603617 [math.CO], 2006.",
				"S. Butler, \u003ca href=\"https://www.semanticscholar.org/paper/On-permutations-which-are-1324-and-2143-avoiding-Butler/47ff2e8d8461087e56de6840058a5dffd1290196\"\u003eOn permutations which are 1324 and {overline 2143} avoiding\u003c/a\u003e, 2005.",
				"S. B. Ekhad, M. Yang, \u003ca href=\"http://sites.math.rutgers.edu/~zeilberg/tokhniot/oMathar1maple12.txt\"\u003e Proofs of Linear Recurrences of Coefficients of Certain Algebraic Formal Power Series Conjectured in the On-Line Encyclopedia Of Integer Sequences\u003c/a\u003e, (2017).",
				"Haruhisa Enomoto, \u003ca href=\"https://arxiv.org/abs/2002.09205\"\u003eBruhat inversions in Weyl groups and torsion-free classes over preprojective algebras\u003c/a\u003e, arXiv:2002.09205 [math.RT], 2020."
			],
			"formula": [
				"G.f.: ((1-X)*(1-4*X-2*X*X)-(1-5*X)*sqrt(1-4*X))/2/(1-5*X+2*X^2-X^3. - _Ralf Stephan_, May 09 2007",
				"G.f.: 2 * x * (1 - 4*x - x^2) / ((1 - x) * (1 - 4*x - 2*x^2) + (1 - 5*x) * sqrt(1 - 4*x)). - _Michael Somos_, Jan 12 2012",
				"G.f. is the power series composition of g.f. A204200 and g.f. A000108 (Catalan) with offset 1. - _Michael Somos_, Jan 12 2012",
				"Conjecture: n*(n+5)*a(n) +3*(20-13*n-3*n^2)*a(n-1) +2*(11*n^2+40*n-150)*a(n-2) +3*(40-11*n-3*n^2)*a(n-3) +2*(n+6)*(2*n-5)*a(n-4)=0. - _R. J. Mathar_, Aug 14 2012"
			],
			"example": [
				"x + 2*x^2 + 6*x^3 + 22*x^4 + 89*x^5 + 379*x^6 + 1661*x^7 + 7405*x^8 + ..."
			],
			"mathematica": [
				"a = DifferenceRoot[Function[{a, n}, {(4n^2 + 46n + 60)a[n] + (-9n^2 - 105n - 156)a[n+1] + (22n^2 + 256n + 372)a[n+2] + (-9n^2 - 111n - 240)a[n+3] + (n+4)(n+9)a[n+4] == 0, a[1] == 1, a[2] == 2, a[3] == 6, a[4] == 22}]];",
				"Array[a, 28] (* _Jean-François Alcover_, Dec 17 2018 *)"
			],
			"program": [
				"(PARI) x='x+O('x^66);",
				"gf=((1-x)*(1-4*x-2*x^2)-(1-5*x)*sqrt(1-4*x))/(2*(1-5*x+2*x^2-x^3));",
				"Vec(gf) /* _Joerg Arndt_, Jun 25 2011 */",
				"(PARI) {a(n) = if( n\u003c0, 0, polcoeff( 2 * x * (1 - 4*x - x^2) / ((1 - x) * (1 - 4*x - 2*x^2) + (1 - 5*x) * sqrt(1 - 4*x + x * O(x^n))), n))} /* _Michael Somos_, Jan 12 2012 */"
			],
			"xref": [
				"Cf. A204200."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Steve Butler_, Oct 06 2005",
			"ext": [
				"More terms from _Joerg Arndt_, Jun 25 2011"
			],
			"references": 0,
			"revision": 40,
			"time": "2020-05-19T19:10:30-04:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}