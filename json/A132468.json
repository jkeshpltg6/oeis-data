{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A132468",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 132468,
			"data": "0,1,1,1,1,3,1,1,1,3,1,3,1,3,2,1,1,3,1,3,2,3,1,3,1,3,1,3,1,5,1,1,2,3,2,3,1,3,2,3,1,5,1,3,2,3,1,3,1,3,2,3,1,3,2,3,2,3,1,5,1,3,2,1,2,5,1,3,2,5,1,3,1,3,2,3,2,5,1,3,1,3,1,5,2,3,2,3,1,5,2,3,2,3,2,3,1,3,2,3,1,5,1,3,4",
			"name": "Longest gap between numbers relatively prime to n.",
			"comment": [
				"Here \"gap\" does not include the endpoints.",
				"a(n) is given by the maximum length of a run of numbers satisfying one congruence modulo each of n's distinct prime factors. It follows that if m is the number of distinct prime factors of n and each of n's prime factors is greater than m then a(n) = m. - _Thomas Anton_, Dec 30 2018"
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A132468/b132468.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e",
				"Mario Ziller, John F. Morack, \u003ca href=\"https://arxiv.org/abs/1611.03310\"\u003eAlgorithmic concepts for the computation of Jacobsthal's function\u003c/a\u003e, arXiv:1611.03310 [math.NT], 2016."
			],
			"formula": [
				"a(n) = 1 at every prime power."
			],
			"example": [
				"E.g. n=3: the longest gap in 1, 2, 4, 5, 7, ... is 1, between 2 and 4, so a(3) = 1."
			],
			"maple": [
				"a:=[];",
				"for n from 1 to 120 do",
				"s:=[seq(j,j=1..4*n)];",
				"rec:=0;",
				"   for st from 1 to n do",
				"   len:=0;",
				"    for i from 1 to n while gcd(s[st+i-1],n)\u003e1 do len:=len+1; od:",
				"    if len\u003erec then rec:=len; fi;",
				"   od:",
				"a:=[op(a),rec];",
				"od:",
				"a; # _N. J. A. Sloane_, Apr 18 2017"
			],
			"mathematica": [
				"a[ n_ ] := (Max[ Drop[ #,1 ]-Drop[ #,-1 ] ]-1\u0026)[ Select[ Range[ n+1 ],GCD[ #,n ]==1\u0026 ] ]",
				"Do[Print[n, \" \", a[n]],{n,20000}]"
			],
			"xref": [
				"Equals A048669(n) - 1.",
				"See also A048670, A049298, A070791, A070194."
			],
			"keyword": "nonn",
			"offset": "1,6",
			"author": "_Michael Kleber_, Nov 16 2007",
			"ext": [
				"Incorrect formula removed by _Thomas Anton_, Dec 30 2018"
			],
			"references": 4,
			"revision": 40,
			"time": "2019-02-14T12:36:19-05:00",
			"created": "2007-12-09T03:00:00-05:00"
		}
	]
}