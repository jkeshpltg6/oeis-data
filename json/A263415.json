{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A263415",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 263415,
			"data": "1,0,0,0,0,0,0,0,1,0,0,2,0,0,3,0,1,4,0,2,5,0,6,6,1,10,7,2,19,8,6,28,10,14,44,12,28,60,17,52,86,26,93,112,46,152,152,78,243,196,142,372,264,244,552,350,422,798,486,692,1136,680,1125,1582,997,1758",
			"name": "Expansion of Product_{k\u003e=1} 1/(1-x^(3*k+5))^k.",
			"comment": [
				"In general, if v\u003e0, GCD(v,3)=1 and g.f. = Product_{k\u003e=1} 1/(1-x^(3*k+v))^k, then",
				"a(n) ~ d3(v) * 3^(v^2/27 - 8/9) * exp(-Pi^4 * v^2 / (3888*Zeta(3)) - v * Pi^2 * n^(1/3) / (2^(4/3) * 3^(7/3) * Zeta(3)^(1/3)) + 3^(1/3) * Zeta(3)^(1/3) * n^(2/3) / 2^(2/3)) * n^(v^2/54 - 25/36) / (sqrt(Pi) * 2^(v^2/54 + 11/36) * Zeta(3)^(v^2/54 - 7/36)), where",
				"d3(v) = exp(Integral_{x=0..infinity} (exp((3-v)*x) / (exp(3*x)-1)^2 + (1/12 - v^2/18)/exp(x) - 1/(9*x^2) + v/(9*x))/x dx).",
				"if mod(v,3)=1, then d3(v) = exp(A263031) * 2^((v+2)/6) * 3^((v+2)/18) * Pi^((v+2)/6) / (Gamma(1/3)^((v+2)/3) * A263416((v-1)/3)).",
				"if mod(v,3)=2, then d3(v) = exp(A263030) * 2^((v+1)/6) * Pi^((v+1)/6) / (3^((v+1)/18) * Gamma(2/3)^((v+1)/3) * A263417((v-2)/3))."
			],
			"link": [
				"Vaclav Kotesovec, \u003ca href=\"/A263415/b263415.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Vaclav Kotesovec, \u003ca href=\"/A263415/a263415.jpg\"\u003eGraph - The asymptotic ratio (80000 terms)\u003c/a\u003e"
			],
			"formula": [
				"G.f.: exp(Sum_{k\u003e=1} x^(8*k)/(k*(1-x^(3*k))^2).",
				"a(n) ~ c * 3^(1/27) * exp(-25*Pi^4 / (3888*Zeta(3)) - 5*Pi^2 * n^(1/3) / (2^(4/3) * 3^(7/3) * Zeta(3)^(1/3)) + 3^(1/3) * Zeta(3)^(1/3) * n^(2/3) / 2^(2/3)) / (sqrt(Pi) * 2^(83/108) * Zeta(3)^(29/108) * n^(25/108)), where c = exp(A263030) * Pi / (3^(1/3) * Gamma(2/3)^2) = 0.98365214791227284535715328899346961376609..."
			],
			"maple": [
				"with(numtheory):",
				"a:= proc(n) option remember; local r; `if`(n=0, 1,",
				"       add(add(`if`(irem(d-3, 3, 'r')=2, d*r, 0)",
				"        , d=divisors(j))*a(n-j), j=1..n)/n)",
				"    end:",
				"seq(a(n), n=0..70);  # _Alois P. Heinz_, Oct 17 2015"
			],
			"mathematica": [
				"nmax = 80; CoefficientList[Series[Product[1/(1-x^(3*k+5))^k,{k,1,nmax}],{x,0,nmax}],x]",
				"nmax = 80; CoefficientList[Series[E^Sum[x^(8*k)/(k*(1-x^(3*k))^2), {k, 1, nmax}], {x, 0, nmax}], x]"
			],
			"xref": [
				"Cf. A262877, A262876, A263405 (v=1), A263406 (v=2), A263414 (v=4), A263030, A263417."
			],
			"keyword": "nonn",
			"offset": "0,12",
			"author": "_Vaclav Kotesovec_, Oct 17 2015",
			"references": 6,
			"revision": 14,
			"time": "2015-10-18T03:10:36-04:00",
			"created": "2015-10-17T11:20:24-04:00"
		}
	]
}