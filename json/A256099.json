{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A256099",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 256099,
			"data": "1,5,4,3,6,8,9,0,1,2,6,9,2,0,7,6,3,6,1,5,7,0,8,5,5,9,7,1,8,0,1,7,4,7,9,8,6,5,2,5,2,0,3,2,9,7,6,5,0,9,8,3,9,3,5,2,4,0,8,0,4,0,3,7,8,3,1,1,6,8,6,7,3,9,2,7,9,7,3,8,6,6,4,8,5,1,5,7,9,1,4,5,7,6,0,5,9,1",
			"name": "Decimal expansion of the real root of a cubic used by Omar Khayyám in a geometrical problem.",
			"comment": [
				"This geometrical problem is considered in the Alten et al. reference on pp. 190-192.",
				"The geometrical problem is to find in the first quadrant the point P on a circle (radius R) such that the ratio of the normal to the y-axis  through P and the radius equals the ratio of the segments of the radius on the y-axis. See the link with a figure and more details. For Omar Khayyám see the references as well as the Wikipedia and MacTutor Archive links.",
				"The ratio of the length of the normal x and the segment h on the y-axis starting at the origin is called xtilde, and satisfies the cubic equation",
				"  xtilde^3 -2*xtilde^2 + 2*xtilde - 2 = 0. This xtilde is the tangent of the angle alpha between the positive y-axis and the radius vector from the origin to the point P. This cubic equation has only one real solution xtilde = tan(alpha) given in the formula section. The present decimal expansion belongs to xtilde.",
				"Apart from the first digit the same as A192918. - _R. J. Mathar_, Apr 14 2015"
			],
			"reference": [
				"H.-W. Alten et al., 4000 Jahre Algebra, 2. Auflage, Springer, 2014, pp. 190-192.",
				"O. Khayyam, A paper of Omar Khayyam, Scripta Math. 26 (1963), 323-337."
			],
			"link": [
				"Wolfdieter Lang, \u003ca href=\"/A256099/a256099_1.pdf\"\u003eA Geometrical Problem of Omar Khayyám and its Cubic\u003c/a\u003e.",
				"MacTutor History of Mathematics archive, \u003ca href=\"http://www-history.mcs.st-and.ac.uk/Biographies/Khayyam.html\"\u003e Omar Khayyám\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Omar_Khayy%C3%A1m\"\u003e Omar Khayyám\u003c/a\u003e"
			],
			"formula": [
				"xtilde = tan(alpha) = ((3*sqrt(33) + 17)^(1/3) - (3*sqrt(33) - 17)^(1/3) + 2)/3 = 1.54368901269...",
				"The corresponding angle alpha is approximately 57.065 degrees.",
				"The real root of x^3-2*x^2+2*x-2. Equals tau^2-tau where tau is the tribonacci constant A058265. - _N. J. A. Sloane_, Jun 19 2019"
			],
			"example": [
				"1.5436890126920763615708559..."
			],
			"mathematica": [
				"RealDigits[Root[x^3 - 2 x^2 + 2 x - 2, 1], 10, 105][[1]] (* _Jean-François Alcover_, Oct 24 2019 *)"
			],
			"program": [
				"(PARI) solve(x=1, 2, x^3-2*x^2+2*x-2) \\\\ _Michel Marcus_, Oct 24 2019"
			],
			"xref": [
				"Cf. A058265."
			],
			"keyword": "nonn,cons,easy",
			"offset": "1,2",
			"author": "_Wolfdieter Lang_, Apr 08 2015",
			"references": 3,
			"revision": 32,
			"time": "2019-10-24T10:25:17-04:00",
			"created": "2015-04-08T11:00:11-04:00"
		}
	]
}