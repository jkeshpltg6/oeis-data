{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A094759",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 94759,
			"data": "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,6,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74",
			"name": "Least k \u003c= n such that n*sigma(k) = k*sigma(n), where sigma(n) is the sum of divisors of n (A000203).",
			"comment": [
				"Conjecture: There are infinitely many terms such that a(n)\u003cn. A050973 has those n, A050972 has the a(n).",
				"See A095301 for a version of A050973 that do not duplicate every n that has several smaller k of the same abundancy. - _Jeppe Stig Nielsen_, Jul 09 2015",
				"That conjecture is an easy fact: Since, e.g., (6,28) is a friendly pair, then so is (6k,28k) for any multiplier k with gcd(42,k)=1. So any n=28k, where gcd(42,k)=1, satisfies a(n)\u003cn. This even shows that A095301 does not have asymptotic density zero. - _Jeppe Stig Nielsen_, Jul 09 2015",
				"This sequence is related to Theorem 1 on p. 173 of the Erdős link in the following way. For a given x, let us consider the set of integers such that a(n) \u003c= x, which is equivalent to removing duplicates from the current sequence. This set would begin with: 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 29, ... So this set has the same number of elements as the number of distinct terms numbers of the form sigma(n)/n with 1 \u003c= n \u003c=x. Then by Erdős, it is c1*x + o(x), with 6/Pi^2 \u003c c1 \u003c 1. With x = 10^7, we find c1 ~= 0.98208... - _Michel Marcus_, Jul 21 2015",
				"a(n) is the least k which has the same abundancy index as n, that is, minimal k for which sigma(k)/k = sigma(n)/n. - _Antti Karttunen_, Jul 10 2019"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A094759/b094759.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"P. Erdős, \u003ca href=\"https://www.renyi.hu/~p_erdos/1959-21.pdf\"\u003eRemarks on number theory II: Some problems on the sigma function\u003c/a\u003e, Acta Arith., 5 (1959), 171-177."
			],
			"maple": [
				"N:= 100: # to get a(1) to a(N)",
				"for n from 1 to N do",
				"   v:= numtheory:-sigma(n)/n;",
				"   if not assigned(R[v]) then R[v]:= n fi;",
				"   A[n]:= R[v];",
				"od:",
				"seq(A[n],n=1..N); # _Robert Israel_, Jul 21 2015"
			],
			"program": [
				"(PARI) for(n=1,74,s=sigma(n);k=1;while(n*sigma(k)!=k*s,k++);print1(k,\",\"));"
			],
			"xref": [
				"Cf. A000203, A050972, A050973, A094757, A094758, A326200.",
				"Cf. A095301 for n such that a(n) \u003c n.",
				"Cf. A000396 (positions of 6's), A005820 (positions of 120's)."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Amarnath Murthy_, May 30 2004",
			"ext": [
				"Edited and extended by _Don Reble_ and _Klaus Brockhaus_, May 31 2004"
			],
			"references": 6,
			"revision": 35,
			"time": "2019-07-10T21:25:36-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}