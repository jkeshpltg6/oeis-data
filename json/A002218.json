{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002218",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2218,
			"id": "M2873 N1155",
			"data": "1,1,1,3,10,56,468,7123,194066,9743542,900969091,153620333545,48432939150704,28361824488394169,30995890806033380784,63501635429109597504951,244852079292073376010411280,1783160594069429925952824734641,24603887051350945867492816663958981",
			"name": "Number of unlabeled nonseparable graphs (or blocks) with n nodes.",
			"comment": [
				"By definition, a(n) gives the number of graphs with zero cutpoints. - _Travis Hoppe_, Apr 28 2014",
				"Isolated points (and hence the singleton graph K_1) are considered blocks; cf. West (2000, p. 155). - _Eric W. Weisstein_, Dec 07 2021",
				"For n \u003e 2, a(n) is also the number of simple biconnected graphs on n nodes. - _Eric W. Weisstein_, Dec 07 2021"
			],
			"reference": [
				"P. Butler and R. W. Robinson, On the computer calculation of the number of nonseparable graphs, pp. 191 - 208 of Proc. Second Caribbean Conference Combinatorics and Computing (Bridgetown, 1977). Ed. R. C. Read and C. C. Cadogan. University of the West Indies, Cave Hill Campus, Barbados, 1977. vii+223 pp.",
				"F. Harary and E. M. Palmer, Graphical Enumeration, Academic Press, NY, 1973, p. 188.",
				"R. C. Read and R. J. Wilson, An Atlas of Graphs, Oxford, 1998.",
				"R. W. Robinson, Numerical implementation of graph counting algorithms, AGRC Grant, Math. Dept., Univ. Newcastle, Australia, 1978.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A002218/b002218.txt\"\u003eTable of n, a(n) for n = 1..40\u003c/a\u003e (terms 1..26 from R. W. Robinson)",
				"P. Butler and R. W. Robinson, \u003ca href=\"/A002218/a002218.pdf\"\u003eOn the computer calculation of the number of nonseparable graphs\u003c/a\u003e, pp. 191 - 208 of Proc. Second Caribbean Conference Combinatorics and Computing (Bridgetown, 1977). Ed. R. C. Read and C. C. Cadogan. University of the West Indies, Cave Hill Campus, Barbados, 1977. vii+223 pp. [Annotated scanned copy]",
				"Steven R. Finch, \u003ca href=\"https://doi.org/10.1017/9781316997741\"\u003eMathematical Constants II\u003c/a\u003e, Encyclopedia of Mathematics and Its Applications, Cambridge University Press, Cambridge, 2018.",
				"R. W. Robinson, \u003ca href=\"/A002218/a002218_1.pdf\"\u003eComputer print-out of first 26 terms\u003c/a\u003e [Annotated scanned copy]",
				"R. W. Robinson, \u003ca href=\"http://cobweb.cs.uga.edu/~rwr/publications/tables.pdf\"\u003eTables\u003c/a\u003e",
				"R. W. Robinson, \u003ca href=\"/A002218/a002218_2.pdf\"\u003eTables\u003c/a\u003e [Local copy, with permission]",
				"R. W. Robinson, \u003ca href=\"http://dx.doi.org/10.1016/S0021-9800(70)80089-8\"\u003eEnumeration of non-separable graphs\u003c/a\u003e, J. Combin. Theory 9 (1970), 327-356.",
				"R. W. Robinson and T. R. S. Walsh, \u003ca href=\"http://cobweb.cs.uga.edu/~rwr/publications/cycleindex.pdf\"\u003eInversion of cycle index sum relations for 2- and 3-connected graphs\u003c/a\u003e, J. Combin. Theory Ser. B. 57 (1993), 289-308.",
				"R. W. Robinson and T. R. S. Walsh, \u003ca href=\"https://doi.org/10.1006/jctb.1993.1022\"\u003eInversion of cycle index sum relations for 2- and 3-connected graphs\u003c/a\u003e, J. Combin. Theory Ser. B. 57 (1993), 289-308.",
				"Andrés Santos, \u003ca href=\"https://doi.org/10.1007/978-3-319-29668-5_3\"\u003eDensity Expansion of the Equation of State\u003c/a\u003e, in A Concise Course on the Theory of Classical Liquids, Volume 923 of the series Lecture Notes in Physics, pp 33-96, 2016. DOI:10.1007/978-3-319-29668-5_3. See Reference 40.",
				"Andrew J. Schultz and David A. Kofke, \u003ca href=\"https://doi.org/10.1103/PhysRevE.90.023301\"\u003eFifth to eleventh virial coefficients of hard spheres\u003c/a\u003e, Phys. Rev. E 90, 023301, 4 August 2014",
				"D. Stolee, \u003ca href=\"http://arxiv.org/abs/1104.5261\"\u003eIsomorph-free generation of 2-connected graphs with applications\u003c/a\u003e, arXiv preprint arXiv:1104.5261 [math.CO], 2011.",
				"Rodrigo Stange Tessinari, Marcia Helena Moreira Paiva, Maxwell E. Monteiro, Marcelo E. V. Segatto, Anilton Garcia, George T. Kanellos, Reza Nejabati, and Dimitra Simeonidou, \u003ca href=\"https://doi.org/10.1109/BICOP.2018.8658361\"\u003eOn the Impact of the Physical Topology on the Optical Network Performance\u003c/a\u003e, IEEE British and Irish Conference on Optics and Photonics (BICOP 2018), London.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/BiconnectedGraph.html\"\u003eBiconnected Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/k-ConnectedGraph.html\"\u003ek-Connected Graph\u003c/a\u003e"
			],
			"program": [
				"(PARI) \\\\ See A004115 for graphsSeries and A339645 for combinatorial species functions.",
				"cycleIndexSeries(n)={my(g=graphsSeries(n), gc=sLog(g), gcr=sPoint(gc)); intformal(x*sSolve( sLog( gcr/(x*sv(1)) ), gcr ), sv(1)) + sSolve(subst(gc, sv(1), 0), gcr)}",
				"{ my(N=12); Vec(OgfSeries(cycleIndexSeries(N)), -N) } \\\\ _Andrew Howroyd_, Dec 28 2020"
			],
			"xref": [
				"Column k=0 of A325111 (for n\u003e1).",
				"Cf. A000088, A001349, A006289, A006290, A004115, A013922, A241767.",
				"Cf. A010355, A339070, A339071."
			],
			"keyword": "nonn,nice",
			"offset": "1,4",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from R. C. Read (rcread(AT)math.uwaterloo.ca). Robinson and Walsh list the first 26 terms.",
				"a(1) changed from 0 to 1 by _Eric W. Weisstein_, Dec 07 2021"
			],
			"references": 54,
			"revision": 115,
			"time": "2021-12-07T20:04:38-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}