{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006045",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6045,
			"id": "M3946",
			"data": "1,26,272,722,5270,5260,37358,18414,56216,95668,487714,99796,1304262,627046,593398,481982,7044222,931396,11570384,1602940,4037650,8694134,40220524,2069292,15855230,21686124,13215872,10948486,129952894,10451648",
			"name": "Sum of orders of all 2 X 2 matrices with entries mod n.",
			"comment": [
				"The order of a matrix M over Z/(nZ) is the smallest k such that M^k is idempotent."
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Michael S. Branicky, \u003ca href=\"/A006045/b006045.txt\"\u003eTable of n, a(n) for n = 1..150\u003c/a\u003e (first 61 terms from _Sean A. Irvine_)",
				"Michael S. Branicky, \u003ca href=\"/A006045/a006045.py.txt\"\u003ePython program\u003c/a\u003e",
				"A. Wilansky, \u003ca href=\"https://www.maa.org/sites/default/files/pdf/upload_library/22/Ford/AlbertWilansky.pdf\"\u003eSpectral decomposition of matrices for high school students\u003c/a\u003e, Math. Mag., vol. 41, 1968, pp. 51-59.",
				"A. Wilansky, \u003ca href=\"/A006045/a006045_1.pdf\"\u003eSpectral decomposition of matrices for high school students\u003c/a\u003e, Math. Mag., vol. 41, 1968, pp. 51-59. (Annotated scanned copy)",
				"A. Wilansky, \u003ca href=\"/A006045/a006045.pdf\"\u003eLetters to N. J. A. Sloane, Jun. 1991\u003c/a\u003e."
			],
			"program": [
				"(PARI) order(m) = {kk = 1; ok = 0; while (! ok, mk = m^kk; if (mk^2 == mk, ok = 1, kk++);); return(kk);}",
				"a(n) = {ret = 0; m = matrix(2, 2); for (i=0, n-1, m[1, 1] = Mod(i, n); for (j=0, n-1, m[1, 2] = Mod(j, n); for (k=0, n-1, m[2, 1] = Mod(k, n); for (l=0, n-1, m[2, 2] = Mod(l, n); ret += order(m););););); return (ret);}",
				"(Python) # see link for faster version",
				"from itertools import product",
				"def mmm2(A, B, modder): # matrix multiply modulo for 2x2",
				"  return ((A[0]*B[0]+A[1]*B[2])%modder, (A[0]*B[1]+A[1]*B[3])%modder,",
				"          (A[2]*B[0]+A[3]*B[2])%modder, (A[2]*B[1]+A[3]*B[3])%modder)",
				"def order(A, modder):",
				"  Ak, k = A, 1",
				"  while mmm2(Ak, Ak, modder) != Ak: Ak, k = mmm2(Ak, A, modder), k+1",
				"  return k",
				"def a(n): return sum(order(A, n) for A in product(range(n), repeat=4))",
				"print([a(n) for n in range(1, 12)]) # _Michael S. Branicky_, Jan 26 2021"
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Albert Wilansky",
			"ext": [
				"The article gives an incorrect value for a(5).",
				"More terms from _Michel Marcus_, Jun 07 2013",
				"More terms from _Sean A. Irvine_, Dec 18 2016"
			],
			"references": 2,
			"revision": 38,
			"time": "2021-01-28T21:35:49-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}