{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A341592",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 341592,
			"data": "1,1,1,1,1,2,1,0,1,2,1,1,1,2,2,0,1,1,1,2,2,2,1,1,1,2,0,2,1,4,1,0,2,2,2,1,1,2,2,1,1,4,1,2,1,2,1,0,1,1,2,2,1,0,2,1,2,2,1,3,1,2,1,0,2,4,1,2,2,4,1,0,1,2,1,2,2,4,1,1,0,2,1,3,2,2,2",
			"name": "Number of squarefree superior divisors of n.",
			"comment": [
				"We define a divisor d|n to be superior if d \u003e= n/d. Superior divisors are counted by A038548 and listed by A161908."
			],
			"example": [
				"The strictly superior squarefree divisors (columns) of selected n:",
				"  1  6  8  30  60  210  420  630  1050  2310  4620  6930",
				"  ------------------------------------------------------",
				"  1  3  .   6  10   15   21   30   35     55    70   105",
				"     6     10  15   21   30   35   42     66    77   110",
				"           15  30   30   35   42   70     70   105   154",
				"           30       35   42   70  105     77   110   165",
				"                    42   70  105  210    105   154   210",
				"                    70  105  210         110   165   231",
				"                   105  210              154   210   330",
				"                   210                   165   231   385",
				"                                         210   330   462",
				"                                         231   385   770",
				"                                         330   462  1155",
				"                                         385   770  2310",
				"                                         462  1155",
				"                                         770  2310",
				"                                        1155",
				"                                        2310"
			],
			"maple": [
				"with(numtheory):",
				"a := n -\u003e nops(select(d -\u003e d*d \u003e= n and issqrfree(d), divisors(n))):",
				"seq(a(n), n = 1..88); # _Peter Luschny_, Feb 20 2021"
			],
			"mathematica": [
				"Table[Length[Select[Divisors[n],SquareFreeQ[#]\u0026\u0026#\u003e=n/#\u0026]],{n,100}]"
			],
			"xref": [
				"Positions of zeros are A059172.",
				"The inferior version is A333749.",
				"The version for prime instead of squarefree divisors is A341591.",
				"The version for prime powers instead of squarefree divisors is A341593.",
				"The strictly superior case is A341595.",
				"The version for odd instead of squarefree divisors is A341675.",
				"A001221 counts prime divisors, with sum A001414.",
				"A033677 selects the smallest superior divisor.",
				"A038548 counts superior (or inferior) divisors.",
				"A056924 counts strictly superior (or strictly inferior) divisors.",
				"A161908 lists superior divisors.",
				"A207375 lists central divisors.",
				"- Inferior: A033676, A063962, A066839, A069288, A161906, A217581, A333750.",
				"- Superior: A051283, A063538, A063539, A070038, A116882, A116883, A341676.",
				"- Strictly Inferior: A060775, A333805, A333806, A341596, A341674.",
				"- Strictly Superior: A048098, A064052 A140271, A238535, A341594, A341642, A341643, A341644, A341645, A341646, A341673.",
				"Cf. A000005, A000203, A001222, A001248, A006530, A020639, A112798."
			],
			"keyword": "nonn",
			"offset": "1,6",
			"author": "_Gus Wiseman_, Feb 19 2021",
			"references": 25,
			"revision": 13,
			"time": "2021-02-20T23:13:19-05:00",
			"created": "2021-02-20T23:13:19-05:00"
		}
	]
}