{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A306260",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 306260,
			"data": "1,1,1,2,1,2,2,1,2,1,2,1,2,2,1,4,2,3,3,2,4,4,3,1,2,1,2,3,1,2,5,5,4,5,5,4,3,1,2,4,4,4,4,5,5,7,2,2,5,3,4,5,5,3,7,4,2,5,2,4,7,6,6,6,5,6,5,3,5,6,5,8,9,8,4,7,2,4,9,2,6,5,8,6,7,7,2,6,4,4,12,6,5,5,7,9,8,5,6,9,8",
			"name": "Number of ways to write n as w*(4w+1) + x*(4x-1) + y*(4y-2) + z*(4z-3) with w,x,y,z nonnegative integers.",
			"comment": [
				"Conjecture 1: a(n) \u003e 0 for all n \u003e= 0, and a(n) = 1 only for n = 0, 1, 2, 4, 7, 9, 11, 14, 23, 25, 28, 37.",
				"Conjecture 2: Each n = 0,1,2,... can be written as w*(4w+2) + x*(4x-1) + y*(4y-2) + z*(4z-3) with w,x,y,z nonnegative integers.",
				"Conjecture 3: Each n = 0,1,2,... can be written as 4*w^2 + x*(4x+1) + y*(4y-2) + z*(4z-3) with w,x,y,z nonnegative integers.",
				"We have verified that a(n) \u003e 0 for all n = 0..2*10^6. By Theorem 1.3 in the linked 2017 paper of the author, any nonnegative integer can be written as x*(4x-1) + y*(4y-2) + z*(4z-3) with x,y,z integers."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A306260/b306260.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2015.10.014\"\u003eA result similar to Lagrange's theorem\u003c/a\u003e, J. Number Theory 162(2016), 190-211.",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2016.07.024\"\u003eOn x(ax+1)+y(by+1)+z(cz+1) and x(ax+b)+y(ay+c)+z(az+d)\u003c/a\u003e, J. Number Theory 171(2017), 275-283."
			],
			"example": [
				"a(11) = 1 with 11 = 1*(4*1+1) + 1*(4*1-1) + 1*(4*1-2) + 1*(4*1-3).",
				"a(23) = 1 with 23 = 2*(4*2+1) + 1*(4*1-1) + 1*(4*1-2) + 0*(4*0-3).",
				"a(25) = 1 with 25 = 0*(4*0+1) + 1*(4*1-1) + 2*(4*2-2) + 2*(4*2-3).",
				"a(28) = 1 with 28 = 2*(4*2+1) + 0*(4*0-1) + 0*(4*0-2) + 2*(4*2-3).",
				"a(37) = 1 with 37 = 1*(4*1+1) + 1*(4*1-1) + 1*(4*1-2) + 3*(4*3-3)."
			],
			"mathematica": [
				"QQ[n_]:=QQ[n]=IntegerQ[Sqrt[16n+1]]\u0026\u0026Mod[Sqrt[16n+1],8]==1;",
				"tab={};Do[r=0;Do[If[QQ[n-x(4x-1)-y(4y-2)-z(4z-3)],r=r+1],{x,0,(Sqrt[16n+1]+1)/8},{y,0,(Sqrt[4(n-x(4x-1))+1]+1)/4},{z,0,(Sqrt[16(n-x(4x-1)-y(4y-2))+9]+3)/8}];tab=Append[tab,r],{n,0,100}];Print[tab]"
			],
			"xref": [
				"Cf. A001107, A002939, A007742, A033991, A255350, A306225, A306227, A306239, A306240, A306249, A306250."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Zhi-Wei Sun_, Feb 01 2019",
			"references": 1,
			"revision": 13,
			"time": "2019-02-01T08:52:08-05:00",
			"created": "2019-02-01T08:52:08-05:00"
		}
	]
}