{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A128613",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 128613,
			"data": "0,1,0,1,2,0,0,6,6,0,0,12,36,12,0,1,29,147,155,28,0,1,64,586,1208,605,56,0,0,120,2160,7800,7800,2160,120,0,0,240,7320,44160,78000,44160,7320,240,0,1,517,23893,227569,655315,655039,227623,23947,496,0,1,1044,76332,1101420,4869558,7862124,4868556,1102068,76305,992,0",
			"name": "Triangle T(n,k) read by rows: number of permutations in [n] with exactly k ascents that have an odd number of inversions.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A128613/b128613.txt\"\u003eRows n = 1..141, flattened\u003c/a\u003e",
				"Jason Fulman, Gene B. Kim, Sangchul Lee, T. Kyle Petersen, \u003ca href=\"https://arxiv.org/abs/1910.04258\"\u003eOn the joint distribution of descents and signs of permutations\u003c/a\u003e, arXiv:1910.04258 [math.CO], 2019.",
				"S. Tanimoto, \u003ca href=\"http://arXiv.org/abs/math.CO/0602263\"\u003eA new approach to signed Eulerian numbers\u003c/a\u003e, arXiv:math/0602263 [math.CO], 2006."
			],
			"formula": [
				"a(n) = 1/2 * [A008292(n,k) - A049061(n,k) ].",
				"T(n,k) = 1/2 * [A008292(n,n-k)-A049061(n,n-k)], n\u003e=1, 0\u003c=k\u003cn. - _R. J. Mathar_, Nov 01 2007"
			],
			"example": [
				"Triangle starts:",
				"0;",
				"1,0;",
				"1,2,0;",
				"0,6,6,0;",
				"0,12,36,12,0;",
				"1,29,147,155,28,0;",
				"1,64,586,120,605,56,0;",
				"0,120,2160,7800,7800,2160,120,0;"
			],
			"maple": [
				"A008292 := proc(n,k) local j; add( (-1)^j*(k-j)^n*binomial(n+1,j),j=0..k) ; end: A049061 := proc(n,k) if k \u003c= 0 or n \u003c=0 or k \u003e n then 0; elif n = 1 then 1 ; elif n mod 2 = 0 then A049061(n-1,k)-A049061(n-1,k-1) ; else k*A049061(n-1,k)+(n-k+1)*A049061(n-1,k-1) ; fi ; end: A128613 := proc(n,k) (A008292(n,n-k)-A049061(n,n-k))/2 ; end: for n from 1 to 11 do for k from 0 to n-1 do printf(\"%d,\",A128613(n,k)) ; od: od: # _R. J. Mathar_, Nov 01 2007",
				"# second Maple program:",
				"b:= proc(u, o, i) option remember; expand(`if`(u+o=0, i,",
				"       add(b(u+j-1, o-j, irem(i+u+j-1, 2)), j=1..o)*x+",
				"       add(b(u-j, o+j-1, irem(i+u-j, 2)), j=1..u)))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=0..n-1))(b(n, 0$2)):",
				"seq(T(n), n=1..14);  # _Alois P. Heinz_, May 02 2017"
			],
			"mathematica": [
				"b[u_, o_, i_] := b[u, o, i] = Expand[If[u + o == 0, i, Sum[b[u + j - 1, o - j, Mod[i + u + j - 1, 2]], {j, 1, o}]*x + Sum[b[u - j, o + j - 1, Mod[i + u - j, 2]], {j, 1, u}]]];",
				"T[n_] := Function[p, Table[Coefficient[p, x, i], {i, 0, n-1}]][b[n, 0,0]];",
				"Table[T[n], {n, 1, 14}] // Flatten (* _Jean-François Alcover_, Jul 25 2017, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A008292, A049061, A128612."
			],
			"keyword": "nonn,tabl",
			"offset": "1,5",
			"author": "_Ralf Stephan_, May 08 2007",
			"ext": [
				"Corrected and extended by _R. J. Mathar_, Nov 01 2007"
			],
			"references": 2,
			"revision": 19,
			"time": "2019-10-11T03:02:45-04:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}