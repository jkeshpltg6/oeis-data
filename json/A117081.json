{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A117081",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 117081,
			"data": "2753,1979,1277,647,89,-397,-811,-1153,-1423,-1621,-1747,-1801,-1783,-1693,-1531,-1297,-991,-613,-163,359,953,1619,2357,3167,4049,5003,6029,7127,8297,9539,10853,12239,13697,15227,16829,18503,20249,22067,23957,25919,27953,30059,32237,34487,36809,39203,41669",
			"name": "a(n) = 36*n^2 - 810*n + 2753, producing the conjectured record number of 45 primes in a contiguous range of n for quadratic polynomials, i.e., abs(a(n)) is prime for 0 \u003c= n \u003c 44.",
			"comment": [
				"The absolute values of a(n) for 0 \u003c= n \u003c= 44 are primes, a(45) = 39203 = 197*199. The positive prime terms are in A050268.",
				"The polynomial is a transformed version of the polynomial P(x) = 36*x^2 + 18*x - 1801 whose absolute value gives 45 distinct primes for -33 \u003c= x \u003c= 11, found by Ruby in 1989. It is one of the 3 known quadratic polynomials whose absolute value produces more than 40 primes in a contiguous range from 0 to n. For the other two polynomials, which produce 43 primes, see A050267 and A267252. - _Hugo Pfoertner_, Dec 13 2019"
			],
			"reference": [
				"Paulo Ribenboim, The Little Book of Bigger Primes, Second Edition, Springer-Verlag New York, 2004."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A117081/b117081.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"François Dress and Michel Olivier, \u003ca href=\"https://projecteuclid.org/euclid.em/1047262355\"\u003ePolynômes prenant des valeurs premières\u003c/a\u003e, Experimental Mathematics, Volume 8, Issue 4 (1999), 319-338.",
				"Carlos Rivera, \u003ca href=\"http://www.primepuzzles.net/problems/prob_012.htm\"\u003eProblem 12: Prime producing polynomials\u003c/a\u003e, The Prime Puzzles and Problems Connection.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Prime-GeneratingPolynomial.html\"\u003ePrime-Generating Polynomial\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1)."
			],
			"formula": [
				"G.f.: (2753-6280*x+3599*x^2)/(1-x)^3. [_Colin Barker_, May 10 2012]",
				"a(0)=2753, a(1)=1979, a(2)=1277, a(n)=3*a(n-1)-3*a(n-2)+a(n-3). - _Harvey P. Dale_, Jun 20 2013"
			],
			"mathematica": [
				"f[n_] := If[Mod[n, 2] == 1, 36*n^2 - 810*n + 2753, 36*n^2 - 810*n + 2753] a = Table[f[n], {n, 0, 100}]",
				"CoefficientList[Series[(2753-6280*x+3599*x^2)/(1-x)^3,{x,0,50}],x] (* _Vincenzo Librandi_, May 12 2012 *)",
				"Table[36n^2-810n+2753,{n,0,50}] (* or *) LinearRecurrence[{3,-3,1},{2753,1979,1277},50] (* _Harvey P. Dale_, Jun 20 2013 *)"
			],
			"program": [
				"(PARI) {for(n=0, 46, print1(36*n^2-810*n+2753, \",\"))}",
				"(MAGMA) I:=[2753, 1979, 1277]; [n le 3 select I[n] else 3*Self(n-1)-3 *Self(n-2)+Self(n-3): n in [1..50]]; // _Vincenzo Librandi_, May 12 2012"
			],
			"xref": [
				"Cf. A005846, A050267, A050268, A117081, A267252."
			],
			"keyword": "sign,easy,less",
			"offset": "0,1",
			"author": "_Roger L. Bagula_, Apr 17 2006",
			"ext": [
				"Edited by _N. J. A. Sloane_, Apr 27 2007",
				"Title extended by _Hugo Pfoertner_, Dec 13 2019"
			],
			"references": 5,
			"revision": 32,
			"time": "2021-06-15T10:39:51-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}