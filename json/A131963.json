{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A131963",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 131963,
			"data": "1,1,1,0,1,2,1,1,0,1,0,1,2,1,1,1,1,1,0,2,0,0,1,0,2,1,3,1,0,1,1,1,0,0,1,1,1,0,1,2,2,1,1,0,1,1,1,2,0,0,1,1,2,0,0,2,0,1,0,1,1,2,2,1,1,1,1,1,0,1,1,0,1,0,1,3,0,1,0,0,1,2,2,0,1,1,2",
			"name": "Expansion of f(x, x^2) * f(x^4, x^12) in powers of x where f(, ) is Ramanujan's general theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A131963/b131963.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of psi(x^4) * phi(-x^3) / chi(-x) in powers of x where phi(), psi(), chi() are Ramanujan theta functions.",
				"Expansion of q^(-13/24) * eta(q^2) * eta(q^3)^2 * eta(q^8)^2 / (eta(q) * eta(q^4) * eta(q^6)) in powers of q.",
				"Euler transform of period 24 sequence [ 1, 0, -1, 1, 1, -1, 1, -1, -1, 0, 1, 0, 1, 0, -1, -1, 1, -1, 1, 1, -1, 0, 1, -2, ...].",
				"a(25*n + 13) = a(n). a(25*n + 3) = a(25*n + 8) = a(25*n + 18) = a(25*n + 23) = 0.",
				"2 * a(n) = A123484(24*n + 13)."
			],
			"example": [
				"G.f. = 1 + x + x^2 + x^4 + 2*x^5 + x^6 + x^7 + x^9 + x^11 + 2*x^12 + x^13 + ...",
				"G.f. = q^13 + q^37 + q^61 + q^109 + 2*q^133 + q^157 + q^181 + q^229 + q^277 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 0, 0, With[ {m = 24 n + 13}, DivisorSum[ m, KroneckerSymbol[ -12, #] Mod[m/#, 2] \u0026] / 2]]; (* _Michael Somos_, Nov 04 2015 *)",
				"a[ n_] := SeriesCoefficient[(1/2) x^(-1/2) EllipticTheta[ 4, 0, x^3] QPochhammer[ -x, x] EllipticTheta[ 2, 0, x^2], {x, 0, n}]; (* _Michael Somos_, Nov 04 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, n = 24*n + 13; sumdiv(n, d, kronecker( -12, d) * (n/d %2)) / 2)};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A) * eta(x^3 + A)^2 * eta(x^8 + A)^2 / (eta(x + A) * eta(x^4 + A) * eta(x^6 + A)), n))};"
			],
			"xref": [
				"Cf. A123484."
			],
			"keyword": "nonn",
			"offset": "0,6",
			"author": "_Michael Somos_, Aug 02 2007",
			"references": 11,
			"revision": 13,
			"time": "2021-03-12T22:24:44-05:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}