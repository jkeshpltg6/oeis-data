{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A346070",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 346070,
			"data": "0,1,0,2,0,1,0,3,0,1,0,2,0,1,0,0,0,1,0,2,0,1,0,3,0,1,0,2,0,1,0,1,0,1,0,2,0,1,0,3,0,1,0,2,0,1,0,0,0,1,0,2,0,1,0,3,0,1,0,2,0,1,0,2,0,1,0,2,0,1,0,3,0,1,0,2,0,1,0,0,0,1,0,2,0,1,0",
			"name": "Symbolic code for the corner turns in the Lévy dragon curve.",
			"comment": [
				"We consider the Lévy dragon curve as constructed using line segments starting with an initial horizontal segment. The left endpoint of this segment is considered the beginning of the curve. Let L(k) denote the k-th iteration. It consists of 2^k segments and 2^k-1 corners. To get the next iteration, L(k+1), each of the segments in L(k) is replaced by two segments at right angles, always placing those segments towards the left of the segment in L(k).",
				"Each iteration can be represented symbolically by a sequence consisting of the integers 0, 1, 2, and 3. One can imagine moving along the individual segments making up the curve starting at the left endpoint. A corner is labeled 0 if the curve makes a right turn there of 90 degrees, is labeled 2 if the curve makes a left turn of 90 degrees at that corner, is labeled 1 if the curve continues straight at that corner (0 degree turn), and is labeled 3 if the curve makes a 180-degree turn at that corner. The Lévy dragon is equal to the limit of L(k) as k goes to infinity.",
				"It makes sense, as Knuth and Davis remarked in their article about the Heighway dragon, to express the Lévy dragon as the infinite sequence of 0's, 1's, 2's, and 3's obtained as each finite sequence for L(k) is extended to the limit. Then a(n) is the code for the behavior of the n-th corner in this infinite sequence."
			],
			"reference": [
				"Paul Lévy, \"Plane or space curves and surfaces consisting of parts similar to the whole,\" in Classics on Fractals, Gerald A. Edgar, Editor, Addison-Wesley, 181-239."
			],
			"link": [
				"Larry Riddle, \u003ca href=\"/A346070/b346070.txt\"\u003eTable of n, a(n) for n = 1..4094\u003c/a\u003e",
				"Paul Lévy, \u003ca href=\"https://gallica.bnf.fr/ark:/12148/bpt6k57344323/f53.image\"\u003eLes courbes planes ou gauches et les surfaces composées de parties semblables au tout\u003c/a\u003e, Journal de l'École Polytechnique, July 1938 pages 227-247, and \u003ca href=\"https://gallica.bnf.fr/ark:/12148/bpt6k57344820/f3.image\"\u003econtinued\u003c/a\u003e October 1938 pages 249-292, curve C.",
				"Larry Riddle, \u003ca href=\"https://larryriddle.agnesscott.org/ifs/levy/LevyCode.htm\"\u003eRepresenting the Levy Dragon Curve Symbolically\u003c/a\u003e",
				"Larry Riddle, \u003ca href=\"https://larryriddle.agnesscott.org/ifs/levy/Levy1234codes5.png\"\u003eFirst 31 terms\u003c/a\u003e, where R=0, S=1, L=2, and B=3.",
				"\u003ca href=\"/index/Fi#FIXEDPOINTS\"\u003eIndex entries for sequences that are fixed points of mappings\u003c/a\u003e"
			],
			"formula": [
				"a(n) = (A092412(n) - 1) mod 4.",
				"a(2n-1) = 0, a(2n) = (a(n)+1) mod 4 = A092412(n).",
				"Write n = k*4^m where k is not a multiple of 4. If k is odd, then a(n) = (2m) mod 4, and if k is even, then a(n) = (2m+1) mod 4.",
				"a(n) = A007814(n) mod 4."
			],
			"example": [
				"The first 15 terms correspond to iteration L(4) of the Levy dragon curve. These terms are 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0. The initial direction of the curve is 4*45=180 degrees (due west). The 15 terms indicate that the curve follows the pattern shown below.",
				"      -- --  -- --",
				"     |     ||     |",
				"   --              --",
				"  |                  |",
				"  |                  |",
				"   -- *            --",
				"     start"
			],
			"xref": [
				"Cf. A092412 (essentially the same), A007814."
			],
			"keyword": "easy,nonn",
			"offset": "1,4",
			"author": "_Larry Riddle_, Jul 04 2021",
			"references": 3,
			"revision": 60,
			"time": "2021-07-21T22:14:11-04:00",
			"created": "2021-07-21T10:06:12-04:00"
		}
	]
}