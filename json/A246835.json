{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A246835",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 246835,
			"data": "1,-2,3,-6,4,-4,7,-2,8,-10,4,-10,9,-6,8,-10,4,-8,16,-8,9,-12,8,-12,20,-6,8,-10,8,-18,11,-12,8,-20,12,-8,20,-6,20,-26,8,-8,15,-10,16,-18,12,-16,20,-10,16,-16,8,-24,24,-8,21,-26,8,-20,20,-14,8,-28",
			"name": "Expansion of psi(-x)^2 * phi(x^2) in powers of x where phi(), psi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A246835/b246835.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-1/4) * eta(q)^2 * eta(q^4)^7 / (eta(q^2)^4 * eta(q^8)^2) in powers of q.",
				"Euler transform of period 8 sequence [ -2, 2, -2, -5, -2, 2, -2, -3, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (64 t)) = 16 (t/i)^(3/2) g(t) where q = exp(2 Pi i t) and g() is the g.f. for A246836.",
				"a(n) = (-1)^n * A213625(n). a(2*n) = A213622(n). a(2*n + 1) = -2 * A132969(n)."
			],
			"example": [
				"G.f. = 1 - 2*x + 3*x^2 - 6*x^3 + 4*x^4 - 4*x^5 + 7*x^6 - 2*x^7 + 8*x^8 + ...",
				"G.f. = q - 2*q^5 + 3*q^9 - 6*q^13 + 4*q^17 - 4*q^21 + 7*q^25 - 2*q^29 + ..."
			],
			"mathematica": [
				"a[n_]:= SeriesCoefficient[EllipticTheta[3, 0, q^2]* EllipticTheta[2, 0, I*q^(1/2)]^2/(4*(-q)^(1/4)), {q, 0, n}]; Table[a[n], {n, 0, 50}] (* _G. C. Greubel_, Nov 29 2017 *)"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A)^2 * eta(x^4 + A)^7 / (eta(x^2 + A)^4 * eta(x^8 + A)^2), n))};"
			],
			"xref": [
				"Cf. A132969, A213622, A213625, A246836."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Sep 04 2014",
			"references": 3,
			"revision": 10,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2014-09-04T14:38:07-04:00"
		}
	]
}