{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A054334",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 54334,
			"data": "1,12,77,352,1287,4004,11011,27456,63206,136136,277134,537472,999362,1790712,3105322,5230016,8580495,13748020,21559395,33153120,50075025,74397180,108864405,157073280,223689180,314707536,437766252,602516992,821063892,1108479152",
			"name": "1/512 of 11th unsigned column of triangle A053120 (T-Chebyshev, rising powers, zeros omitted).",
			"comment": [
				"Partial sums of A054333.",
				"If a 2-set Y and an (n-3)-set Z are disjoint subsets of an n-set X then a(n-11) is the number of 11-subsets of X intersecting both Y and Z. - _Milan Janjic_, Sep 08 2007",
				"10-dimensional square numbers, ninth partial sums of binomial transform of [1,2,0,0,0,...]. a(n)=sum{i=0,n,C(n+9,i+9)*b(i)}, where b(i)=[1,2,0,0,0,...]. [From Borislav St. Borisov (b.st.borisov(AT)abv.bg), Mar 05 2009]",
				"2*a(n) is number of ways to place 9 queens on an (n+9) X (n+9) chessboard so that they diagonally attack each other exactly 36 times. The maximal possible attack number, p=binomial(k,2)=36 for k=9 queens, is achievable only when all queens are on the same diagonal. In graph-theory representation they thus form the corresponding complete graph. - _Antal Pinter_, Dec 27 2015"
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), p. 795.",
				"Theodore J. Rivlin, Chebyshev polynomials: from approximation theory to algebra and number theory, 2. ed., Wiley, New York, 1990."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A054334/b054334.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"Milan Janjic, \u003ca href=\"http://www.pmfbl.org/janjic/\"\u003eTwo Enumerative Functions\u003c/a\u003e",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e"
			],
			"formula": [
				"a(n) = (2*n+10)*binomial(n+9, 9)/10 = ((-1)^n)*A053120(2*n+10, 10)/2^9.",
				"G.f.: (1+x)/(1-x)^11.",
				"a(n) = 2*binomial(n+10, 10) - binomial(n+9, 9). - _Paul Barry_, Mar 04 2003",
				"a(n) = binomial(n+9,9) + 2*binomial(n+9,10). - Borislav St. Borisov (b.st.borisov(AT)abv.bg), Mar 05 2009",
				"a(n) = binomial(n+9,9)*(n+5)/5. - _Antal Pinter_, Dec 27 2015"
			],
			"mathematica": [
				"Table[(2*n + 10)*Binomial[n + 9, 9]/10, {n, 0, 100}] (* _Vladimir Joseph Stephan Orlovsky_, Jan 15 2009 *)"
			],
			"program": [
				"(PARI) vector(40, n, n--; (2*n+10)*binomial(n+9, 9)/10) \\\\ _G. C. Greubel_, Dec 02 2018",
				"(MAGMA) [(2*n+10)*Binomial(n+9, 9)/10: n in [0..40]]; // _G. C. Greubel_, Dec 02 2018",
				"(Sage) [(2*n+10)*binomial(n+9, 9)/10 for n in range(40)] # _G. C. Greubel_, Dec 02 2018",
				"(GAP) List([0..30], n -\u003e (2*n+10)*Binomial(n+9, 9)/10); # _G. C. Greubel_, Dec 02 2018"
			],
			"xref": [
				"Cf. A053120, A054333.",
				"Cf. A005585, A040977, A050486, A053347, A054333."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_",
			"references": 15,
			"revision": 30,
			"time": "2018-12-04T04:10:36-05:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}