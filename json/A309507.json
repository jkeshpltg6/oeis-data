{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A309507",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 309507,
			"data": "0,1,1,1,3,3,1,2,5,3,3,3,3,7,3,1,5,5,3,7,7,3,3,5,5,7,7,3,7,7,1,3,7,7,11,5,3,7,7,3,7,7,3,11,11,3,3,5,8,11,7,3,7,15,7,7,7,3,7,7,3,11,5,3,15,7,3,7,15,7,5,5,3,11,11,7,15,7,3,9,9,3,7",
			"name": "Number of ways the n-th triangular number T(n) = A000217(n) can be written as the difference of two positive triangular numbers.",
			"comment": [
				"Equivalently, a(n) is the number of triples [n,k,m] with k\u003e0 satisfying the Diophantine equation n*(n+1) + k*(k+1) - m*(m+1) = 0. Any such triple satisfies a triangle inequality, n+k \u003e m. The n for which there is a triple [n,n,m] are listed in A053141. - _Bradley Klee_, Mar 01 2020; edited by _N. J. A. Sloane_, Mar 31 2020"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A309507/b309507.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e",
				"J. S. Myers, R. Schroeppel, S. R. Shannon, N. J. A. Sloane, and P. Zimmermann, \u003ca href=\"http://arxiv.org/abs/2004.14000\"\u003eThree Cousins of Recaman's Sequence\u003c/a\u003e, arXiv:2004:14000 [math.NT], April 2020.",
				"M. A. Nyblom, \u003ca href=\"http://www.fq.math.ca/Scanned/39-3/nyblom.pdf\"\u003eOn the representation of the integers as a difference of nonconsecutive triangular numbers\u003c/a\u003e, Fibonacci Quarterly 39:3 (2001), pp. 256-263."
			],
			"formula": [
				"a(n) = 1 \u003c=\u003e n in { A068194 } \\ { 1 }.",
				"a(n) is even \u003c=\u003e n in { A001108 } \\ { 0 }.",
				"a(n) = number of odd divisors of n*(n+1) (or, equally, of T(n)) that are greater than 1. - _N. J. A. Sloane_, Apr 03 2020"
			],
			"example": [
				"a(5) = 3: T(5) = T(6)-T(3) = T(8)-T(6) = T(15)-T(14).",
				"a(7) = 1: T(7) = T(28)-T(27).",
				"a(8) = 2: T(8) = T(13)-T(10) = T(36)-T(35).",
				"a(9) = 5: T(9) = T(10)-T(4) = T(11)-T(6) = T(16)-T(13) = T(23)-T(21) = T(45)-T(44).",
				"a(49) = 8: T(49) = T(52)-T(17) = T(61)-T(36) = T(94)-T(80) = T(127)-T(117) = T(178)-T(171) = T(247)-T(242) = T(613)-T(611) = T(1225)-T(1224).",
				"The triples with n \u003c= 16 are:",
				"2, 2, 3",
				"3, 5, 6",
				"4, 9, 10",
				"5, 3, 6",
				"5, 6, 8",
				"5, 14, 15",
				"6, 5, 8",
				"6, 9, 11",
				"6, 20, 21",
				"7, 27, 28",
				"8, 10, 13",
				"8, 35, 36",
				"9, 4, 10",
				"9, 6, 11",
				"9, 13, 16",
				"9, 21, 23",
				"9, 44, 45",
				"10, 8, 13",
				"10, 26, 28",
				"10, 54, 55",
				"11, 14, 18",
				"11, 20, 23",
				"11, 65, 66",
				"12, 17, 21",
				"12, 24, 27",
				"12, 77, 78",
				"13, 9, 16",
				"13, 44, 46",
				"13, 90, 91",
				"14, 5, 15",
				"14, 11, 18",
				"14, 14, 20",
				"14, 18, 23",
				"14, 33, 36",
				"14, 51, 53",
				"14, 104, 105",
				"15, 21, 26",
				"15, 38, 41",
				"15, 119, 120",
				"16, 135, 136. - _N. J. A. Sloane_, Mar 31 2020"
			],
			"mathematica": [
				"(* _Bradley Klee_, Mar 01 2020 *)",
				"TriTriples[TNn_] := Sort[Select[{TNn, (TNn + TNn^2 - # - #^2)/(2 #),",
				"      (TNn + TNn^2 - # + #^2)/(2 #)} \u0026 /@",
				"    Complement[Divisors[TNn (TNn + 1)], {TNn}],",
				"   And[And @@ (IntegerQ /@ #), And @@ (# \u003e 0 \u0026 /@ #)] \u0026]]",
				"Length[TriTriples[#]] \u0026 /@ Range[100]"
			],
			"xref": [
				"Cf. A000217, A001108, A046079 (the same for squares), A068194, A100821 (the same for primes for n\u003e1), A309332.",
				"See also A053141. The monotonic triples [n,k,m] with n \u003c= k \u003c= m are counted in A333529."
			],
			"keyword": "nonn",
			"offset": "1,5",
			"author": "_Alois P. Heinz_, Aug 05 2019",
			"references": 7,
			"revision": 49,
			"time": "2021-04-26T06:33:49-04:00",
			"created": "2019-08-05T18:34:08-04:00"
		}
	]
}