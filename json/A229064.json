{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A229064",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 229064,
			"data": "5,7,9,11,17,23,29,41,47,59,71,79,81,101,107,137,149,167,179,191,197,227,239,269,281,311,347,359,419,431,461,521,569,599,617,641,659,809,821,827,839,857,881,1019,1031,1049,1061,1091,1151,1229,1277,1289,1301,1319,1367",
			"name": "Lesser of Fermi-Dirac twin primes: both a(n)(\u003e=5) and a(n)+2 are in A050376.",
			"comment": [
				"Terms of A050376 play the role of primes in Fermi-Dirac arithmetic. Therefore, if q and q+2 are consecutive terms of A050376, then we call them twin primes in Fermi-Dirac arithmetic. The sequence lists lessers of them.",
				"There exist conjecturally only 5 Fermat primes F, such that both F-1 and F are in A050376. If we add pair (3,4), then we obtain exactly 6 such pairs as an analog of the unique pair (2,3) in usual arithmetic, which is not considered as a pair of twin primes.",
				"For n\u003e4, numbers n such that n and n+2 are of the form p^(2^k), where p is prime and k \u003e= 0. - _Ralf Stephan_, Sep 23 2013",
				"If a(n) is not the lesser of twin primes (A001359), then either a(n) or a(n)+2 is a perfect square. For example, a(4)=9 and a(7)=23. Note that the first case is possible only if a(n) = 3^(2^m), m\u003e=1. - _Vladimir Shevelev_, Jun 27 2014"
			],
			"reference": [
				"V. S. Shevelev, Multiplicative functions in the Fermi-Dirac arithmetic, Izvestia Vuzov of the North-Caucasus region, Nature sciences 4 (1996), 28-43 [Russian]."
			],
			"link": [
				"Peter J. C. Moses, \u003ca href=\"/A229064/b229064.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"S. Litsyn and V. S. Shevelev, \u003ca href=\"http://www.emis.de/journals/INTEGERS/papers/h33/h33.Abstract.html\"\u003eOn factorization of integers with restrictions on the exponent\u003c/a\u003e, INTEGERS: Electronic Journal of Combinatorial Number Theory, 7 (2007), #A33, 1-36."
			],
			"example": [
				"2, 3 are not in the sequence, although pairs (2,4) and (3,5) are in A050376. Indeed, 2 and 4 as well as 3 and 5 are not consecutive terms of A050376."
			],
			"mathematica": [
				"inA050376Q[1]:=False; inA050376Q[n_] := Length[#] == 1 \u0026\u0026 (Union[Rest[IntegerDigits[#[[1]][[2]], 2]]] == {0} || #[[1]][[2]] == 1)\u0026[FactorInteger[n]]; nextA050376[n_] := NestWhile[#+1\u0026, n+1, !inA050376Q[#] == True\u0026]; Select[Range[1500], inA050376Q[#] \u0026\u0026 (nextA050376[#]-#) == 2\u0026] (* _Peter J. C. Moses_, Sep 19 2013 *)"
			],
			"program": [
				"(PARI) is(n)=if(n\u003c5,return(false);m=factor(n);mm=factor(n+2);e=m[1,2];ee=mm[1,2];matsize(m)[1]==1\u0026\u0026matsize(mm)[1]==1\u0026\u0026e==2^valuation(e,2)\u0026\u0026ee=2^valuation(ee,2) /* _Ralf Stephan_, Sep 22 2013 */"
			],
			"xref": [
				"Cf. A001359."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Vladimir Shevelev_, Sep 17 2013",
			"references": 3,
			"revision": 45,
			"time": "2014-07-07T00:30:18-04:00",
			"created": "2013-09-24T12:06:45-04:00"
		}
	]
}