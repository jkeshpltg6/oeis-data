{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A184157",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 184157,
			"data": "0,0,2,2,4,4,6,6,10,10,10,8,8,8,16,12,8,14,12,18,18,16,14,14,28,14,24,12,18,24,16,20,28,18,24,20,14,14,24,28,14,22,12,24,34,24,24,22,30,40,24,22,20,30,40,18,28,24,18,34,20,28,36,30,36,36,14,30,34,32,28,28,22,20,50,18,42,32,24,40",
			"name": "The sum of the even distances in the rooted tree with Matula-Goebel number n.",
			"comment": [
				"The Matula-Goebel number of a rooted tree is defined in the following recursive manner: to the one-vertex tree there corresponds the number 1; to a tree T with root degree 1 there corresponds the t-th prime number, where t is the Matula-Goebel number of the tree obtained from T by deleting the edge emanating from the root; to a tree T with root degree m\u003e=2 there corresponds the product of the Matula-Goebel numbers of the m branches of T.",
				"a(n) + A184158(n) = A196051(n) (= the Wiener index of the rooted tree with Matula-Goebel number n)."
			],
			"reference": [
				"F. Goebel, On a 1-1-correspondence between rooted trees and natural numbers, J. Combin. Theory, B 29 (1980), 141-143.",
				"I. Gutman and A. Ivic, On Matula numbers, Discrete Math., 150, 1996, 131-142.",
				"I. Gutman and Yeong-Nan Yeh, Deducing properties of trees from their Matula numbers, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"O. Ivanciuc, T. Ivanciuc, D. J. Klein, W. A. Seitz, and A. T. Balaban, Wiener index extension by counting even/odd graph distances, J. Chem. Inf. Comput. Sci., 41, 2001, 536-549.",
				"D. W. Matula, A natural rooted tree enumeration by prime factorization, SIAM Review, 10, 1968, 273."
			],
			"link": [
				"E. Deutsch, \u003ca href=\"http://arxiv.org/abs/1111.4288\"\u003eTree statistics from Matula numbers\u003c/a\u003e, arXiv preprint arXiv:1111.4288, 2011",
				"\u003ca href=\"/index/Mat#matula\"\u003eIndex entries for sequences related to Matula-Goebel numbers\u003c/a\u003e"
			],
			"formula": [
				"a(n) is the value at x=1 of the derivative of the even part of the Wiener polynomial W(n)=W(n,x) of the rooted tree with Matula number n. W(n) is obtained recursively in A196059. The Maple program is based on the above."
			],
			"example": [
				"a(7)=6 because the rooted tree with Matula-Goebel number 7 is the rooted tree Y with 3 distances equal to 2."
			],
			"maple": [
				"with(numtheory): WP := proc (n) local r, s, R: r := proc (n) options operator, arrow: op(1, factorset(n)) end proc: s := proc (n) options operator, arrow: n/r(n) end proc: R := proc (n) if n = 1 then 0 elif bigomega(n) = 1 then sort(expand(x*R(pi(n))+x)) else sort(expand(R(r(n))+R(s(n)))) end if end proc: if n = 1 then 0 elif bigomega(n) = 1 then sort(expand(WP(pi(n))+x*R(pi(n))+x)) else sort(expand(WP(r(n))+WP(s(n))+R(r(n))*R(s(n)))) end if end proc: a := proc (n) options operator, arrow: (1/2)*subs(x = 1, diff(WP(n), x))-(1/2)*subs(x = -1, diff(WP(n), x)) end proc: seq(a(n), n = 1 .. 80);"
			],
			"xref": [
				"Cf. A184158, A196051"
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Emeric Deutsch_, Oct 15 2011",
			"references": 1,
			"revision": 13,
			"time": "2017-03-07T11:32:35-05:00",
			"created": "2011-10-15T23:36:19-04:00"
		}
	]
}