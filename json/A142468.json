{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A142468",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 142468,
			"data": "1,1,1,1,9,1,1,45,45,1,1,165,825,165,1,1,495,9075,9075,495,1,1,1287,70785,259545,70785,1287,1,1,3003,429429,4723719,4723719,429429,3003,1,1,6435,2147145,61408347,184225041,61408347,2147145,6435,1",
			"name": "An eight-products triangle sequence of coefficients: T(n,k) = binomial(n,k) * Product_{j=1..7} j!*(n+j)!/((k+j)!*(n-k+j)!).",
			"comment": [
				"Triangle of generalized binomial coefficients (n,k)_8; cf. A342889. - _N. J. A. Sloane_, Apr 03 2021",
				"Row sums are {1, 2, 11, 92, 1157, 19142, 403691, 10312304, 311348897, 10826298914, 426196716090, ...}.",
				"The general function is T(n,m)_L = binomial(n,m)*Product_{k=1..L} k!*(n + k)!/((m + k)!*(n - m + k)!) to give the quadratic row {1, L+2, 1}."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A142468/b142468.txt\"\u003eRows n = 0..100 of triangle, flattened\u003c/a\u003e",
				"Johann Cigler, \u003ca href=\"https://arxiv.org/abs/2103.01652\"\u003ePascal triangle, Hoggatt matrices, and analogous constructions\u003c/a\u003e, arXiv:2103.01652 [math.CO], 2021.",
				"Johann Cigler, \u003ca href=\"https://www.researchgate.net/publication/349376205_Some_observations_about_Hoggatt_triangles\"\u003eSome observations about Hoggatt triangles\u003c/a\u003e, Universität Wien (Austria, 2021)."
			],
			"formula": [
				"T(n,k) = binomial(n,k)*Product_{j=1..7} j!*(n+j)!/((k+j)!*(n-k+j)!)."
			],
			"example": [
				"Triangle begins as:",
				"  1;",
				"  1,    1;",
				"  1,    9,       1;",
				"  1,   45,      45,        1;",
				"  1,  165,     825,      165,         1;",
				"  1,  495,    9075,     9075,       495,        1;",
				"  1, 1287,   70785,   259545,     70785,     1287,       1;",
				"  1, 3003,  429429,  4723719,   4723719,   429429,    3003,    1;",
				"  1, 6435, 2147145, 61408347, 184225041, 61408347, 2147145, 6435, 1;"
			],
			"maple": [
				"b:= binomial;",
				"T:= (n, k) -\u003e b(n, k)*mul(b(n+2*j, k+j)/b(n+2*j, j), j = 1..7);",
				"seq(seq(T(n, k), k = 0..n), n = 0..10); # _G. C. Greubel_, Nov 14 2019, Mar 03 2021"
			],
			"mathematica": [
				"T[n_, k_]:= T[n,k]= With[{B=Binomial}, B[n,k]* Product[B[n+2*j,k+j]/B[n+2*j,j], {j, 7}] ];",
				"Table[T[n, k], {n,0,10}, {k,0,n}]//Flatten (* modified by _G. C. Greubel_, Nov 14 2019, Mar 03 2021 *)"
			],
			"program": [
				"(PARI) T(n,k) = b=binomial; b(n,k)*prod(j=1,7, b(n+ 2*j,k+j)/b(n+2*j,j)); \\\\ _G. C. Greubel_, Nov 14 2019, Mar 03 2021",
				"(MAGMA) B:=Binomial; [B(n,k)*(\u0026*[B(n+2*j, k+j)/B(n+2*j, j): j in [1..7]]): k in [0..n], n in [0..10]]; // _G. C. Greubel_, Nov 14 2019, Mar 03 2021",
				"(Sage)",
				"b=binomial;",
				"def T(n, k): return b(n,k)*product(b(n+2*j,k+j)/b(n+2*j,j) for j in (1..7))",
				"[[T(n, k) for k in (0..n)] for n in (0..10)] # _G. C. Greubel_, Nov 14 2019, Mar 03 2021"
			],
			"xref": [
				"Cf. A001263, A056939, A056940, A056941.",
				"Triangles of generalized binomial coefficients (n,k)_m (or generalized Pascal triangles) for m = 1,...,12: A007318 (Pascal), A001263, A056939, A056940, A056941, A142465, A142467, A142468, A174109, A342889, A342890, A342891."
			],
			"keyword": "nonn",
			"offset": "0,5",
			"author": "_Roger L. Bagula_, Sep 20 2008",
			"ext": [
				"Edited by _G. C. Greubel_, Nov 14 2019"
			],
			"references": 14,
			"revision": 27,
			"time": "2021-05-14T22:40:33-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}