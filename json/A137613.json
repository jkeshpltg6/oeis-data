{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A137613",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 137613,
			"data": "5,3,11,3,23,3,47,3,5,3,101,3,7,11,3,13,233,3,467,3,5,3,941,3,7,1889,3,3779,3,7559,3,13,15131,3,53,3,7,30323,3,60647,3,5,3,101,3,121403,3,242807,3,5,3,19,7,5,3,47,3,37,5,3,17,3,199,53,3,29,3,486041,3,7,421,23",
			"name": "Omit the 1's from Rowland's sequence f(n) - f(n-1) = gcd(n,f(n-1)), where f(1) = 7.",
			"comment": [
				"Rowland proves that each term is prime. He says it is likely that all odd primes occur.",
				"In the first 5000 terms, there are 965 unique primes and 397 is the least odd prime that does not appear. - _T. D. Noe_, Mar 01 2008",
				"In the first 10000 terms, the least odd prime that does not appear is 587, according to Rowland. [_Jonathan Sondow_, Aug 14 2008]",
				"Removing duplicates from this sequence yields A221869. The duplicates are A225487. - _Jonathan Sondow_, May 03 2013"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A137613/b137613.txt\"\u003eTable of n, a(n) for n = 1..5000\u003c/a\u003e",
				"Jean-Paul Delahaye, \u003ca href=\"http://www2.lifl.fr/~delahaye/IAGL/Conjectures.pdf\"\u003eDéconcertantes conjectures\u003c/a\u003e, Pour la science, 5 (2008), 92-97. [broken link]",
				"Brian Hayes, \u003ca href=\"http://bit-player.org/2015/pumping-the-primes\"\u003ePumping the Primes\u003c/a\u003e, bit-player, 19 August 2015.",
				"John Moyer, \u003ca href=\"http://www.rsok.com/~jrm/source/find_primes_rowland/\"\u003eSource code in C and C++ to print this sequence or sorted and unique values from this sequence.\u003c/a\u003e [From John Moyer (jrm(AT)rsok.com), Nov 06 2009]",
				"Ivars Peterson, \u003ca href=\"http://www.maa.org/mathtourist/mathtourist_8_8_08.html\"\u003eA New Formula for Generating Primes\u003c/a\u003e, The Mathematical Tourist.",
				"Eric S. Rowland, A simple prime-generating recurrence, Abstracts Amer. Math. Soc. 29 (No. 1, 2008), p. 50 (Abstract 1035-11-986). \u003ca href=\"http://arXiv.org/abs/0710.3217\"\u003earXiv:0710.3217\u003c/a\u003e",
				"Eric S. Rowland, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL11/Rowland/rowland21.html\"\u003eA natural prime-generating recurrence\u003c/a\u003e, J. of Integer Sequences 11 (2008), Article 08.2.8.",
				"Eric Rowland, \u003ca href=\"http://thenksblog.wordpress.com/2008/07/21/a-simple-recurrence-that-produces-complex-behavior-and-primes/\"\u003eA simple recurrence that produces complex behavior ...\u003c/a\u003e, A New Kind of Science blog.",
				"Eric Rowland, \u003ca href=\"http://demonstrations.wolfram.com/PrimeGeneratingRecurrence/\"\u003ePrime-Generating Recurrence\u003c/a\u003e, Wolfram Demonstrations Project, 2008.",
				"Jeffrey Shallit, \u003ca href=\"http://recursed.blogspot.com/2008/07/rutgers-graduate-student-finds-new.html\"\u003eRutgers Graduate Student Finds New Prime-Generating Formula\u003c/a\u003e, Recursivity blog.",
				"Vladimir Shevelev, \u003ca href=\"http://arxiv.org/abs/0911.3491\"\u003eGeneralizations of the Rowland theorem\u003c/a\u003e, arXiv:0911.3491 [math.NT], 2009-2010.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Formula_for_primes#Recurrence_relation\"\u003eFormula for primes\u003c/a\u003e"
			],
			"formula": [
				"Denote by Lpf(n) the least prime factor of n. Then a(n) = Lpf(6-n+sum{i=1,...,n-1}a(i)). [_Vladimir Shevelev_, Mar 03 2010]",
				"a(n) = A168008(2*n+4) (conjectured). - _Jon Maiga_, May 20 2021"
			],
			"example": [
				"f(n) = 7, 8, 9, 10, 15, 18, 19, 20, ..., so f(n) - f(n-1) = 1, 1, 1, 5, 3, 1, 1, ... and a(n) = 5, 3, ... .",
				"We have a(1) = Lpf(6-1) = 5; a(2) = Lpf(6-2+5) = 3; a(3) = Lpf(6-3+5+3) = 11; a(4) = Lpf(6-4+5+3+11) = 3; a(5) = Lpf(6-5+5+3+11+3) = 23. [_Vladimir Shevelev_, Mar 03 2010]"
			],
			"maple": [
				"A137613_list := proc(n)",
				"local a, c, k, L;",
				"L := NULL; a := 7;",
				"for k from 2 to n do",
				"    c := igcd(k,a);",
				"    a := a + c;",
				"    if c \u003e 1 then L:=L,c fi;",
				"od;",
				"L end:",
				"A137613_list(500000); # _Peter Luschny_, Nov 17 2011"
			],
			"mathematica": [
				"f[1] = 7; f[n_] := f[n] = f[n - 1] + GCD[n, f[n - 1]]; DeleteCases[Differences[Table[f[n], {n, 10^6}]], 1] (* _Alonso del Arte_, Nov 17 2011 *)"
			],
			"program": [
				"(Haskell)",
				"a137613 n = a137613_list !! (n-1)",
				"a137613_list =  filter (\u003e 1) a132199_list",
				"-- _Reinhard Zumkeller_, Nov 15 2013",
				"(PARI)",
				"ub=1000; n=3; a=9; while(n\u003cub, m=a\\n; d=factor((m-1)*n-1)[1,1]; print1(d,\", \"); n=n+((d-1)\\(m-1)); a=m*n; ); \\\\ _Daniel Constantin Mayer_, Aug 31 2014"
			],
			"xref": [
				"f(n) = f(n-1) + gcd(n, f(n-1)) = A106108(n) and f(n) - f(n-1) = A132199(n-1).",
				"Cf. also A084662, A084663, A134734, A134736, A134743, A134744, A221869.",
				"Cf. A168008, A231900."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Jonathan Sondow_, Jan 29 2008, Jan 30 2008",
			"references": 10,
			"revision": 71,
			"time": "2021-05-22T04:38:24-04:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}