{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A213064",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 213064,
			"data": "0,2,4,4,8,10,8,8,16,18,20,20,16,18,16,16,32,34,36,36,40,42,40,40,32,34,36,36,32,34,32,32,64,66,68,68,72,74,72,72,80,82,84,84,80,82,80,80,64,66,68,68,72,74,72,72,64,66,68,68,64,66,64,64,128,130,132",
			"name": "Bitwise AND of 2n with the one's-complement of n.",
			"comment": [
				"In two's-complement binary arithmetic, -n is ~(n - 1). As such, this could be written instead as a(n) = 2n AND -(n + 1).  Further, because the least significant bits are never matched both of the operands to the AND, the negative form of n can be used rather than the one's-complement, i.e. a(n) = 2n AND -n.",
				"a(n) has a 1-bit immediately above each run of 1's in n, and everywhere else 0's.  Or equivalently, each 01 bit pair in n becomes 10 in a(n) and everywhere else 0's.  The most significant 1-bit of n has a 0 above it for this purpose, so is an 01 bit pair. - _Kevin Ryde_, Jun 04 2020"
			],
			"link": [
				"Juli Mallett, \u003ca href=\"/A213064/b213064.txt\"\u003eTable of n, a(n) for n = 0..9999\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 2n AND ~n",
				"a(n) = 2*A292272(n). - _Kevin Ryde_, Jun 04 2020"
			],
			"example": [
				"For n = 31, 2n is 62, which in binary is 111110, as multiplication by two is the same as shifting the bits of 31 (11111) to the left by one. As the number is one less than a power of two, all of its least significant bits are set. Before the shift, the most significant bit has a value of 16. After the shift, the most significant bit has a value of 32.",
				"The ~n has all bits set but the five least significant, the highest bit set being the power of two above n: .....111111111100000. When these two values are ANDed together, only the 6th bit, that with the value of 32, is common to them, and the result is 32.",
				"From _Kevin Ryde_, Jun 04 2020: (Start)",
				"     n = 1831 = binary  11100100111",
				"  a(n) = 2120 = binary 100001001000   1-bit above each run",
				"(End)"
			],
			"mathematica": [
				"Table[BitAND[2n, -n], {n, 0, 59}] (* _Alonso del Arte_, Jun 04 2012 *)"
			],
			"program": [
				"(C) int a(int n) { return ((n + n) \u0026 ~n); }",
				"(R, with bitops) bitAnd(2 * n, bitFlip(n))",
				"(PARI) a(n) = bitnegimply(n\u003c\u003c1,n); \\\\ _Kevin Ryde_, Jun 04 2020"
			],
			"xref": [
				"Cf. A048724 (with XOR)."
			],
			"keyword": "nonn,base,easy",
			"offset": "0,2",
			"author": "_Juli Mallett_, Jun 04 2012",
			"references": 4,
			"revision": 31,
			"time": "2020-06-04T07:57:56-04:00",
			"created": "2012-06-06T02:58:12-04:00"
		}
	]
}