{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A342306",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 342306,
			"data": "1,0,240,20160,0,319334400,77127879628800,0",
			"name": "Number of pandiagonal Latin squares of order 2n+1.",
			"comment": [
				"A pandiagonal Latin square is a Latin square in which the diagonal, antidiagonal and all broken diagonals and antidiagonals are transversals.",
				"For orders 5, 7 and 11 all pandiagonal Latin squares are cyclic, so a(n) = A338562(n) for n \u003c 6. For n=6 (order 13) this is not true (from Dabbaghian and Wu).",
				"Pandiagonal Latin squares exist only for odd orders not divisible by 3. - _Andrew Howroyd_, May 26 2021"
			],
			"link": [
				"A.O.L. Atkin, L. Hay, and R. G. Larson, \u003ca href=\"https://doi.org/10.1016/0898-1221(83)90130-X\"\u003eEnumeration and construction of pandiagonal Latin squares of prime order\u003c/a\u003e, Computers \u0026 Mathematics with Applications, Volume. 9, Iss. 2, 1983, pp. 267-292.",
				"Vahid Dabbaghian and Tiankuang Wu, \u003ca href=\"http://dx.doi.org/10.1016/j.jda.2014.12.001\"\u003eConstructing non-cyclic pandiagonal Latin squares of prime orders\u003c/a\u003e, Journal of Discrete Algorithms 30, 2015."
			],
			"formula": [
				"a(n) = A338620(n) * (2*n+1)!."
			],
			"example": [
				"Example of a cyclic pandiagonal Latin square of order 5:",
				"  0 1 2 3 4",
				"  2 3 4 0 1",
				"  4 0 1 2 3",
				"  1 2 3 4 0",
				"  3 4 0 1 2",
				"Example of a cyclic pandiagonal Latin square of order 7:",
				"  0 1 2 3 4 5 6",
				"  2 3 4 5 6 0 1",
				"  4 5 6 0 1 2 3",
				"  6 0 1 2 3 4 5",
				"  1 2 3 4 5 6 0",
				"  3 4 5 6 0 1 2",
				"  5 6 0 1 2 3 4",
				"Example of a cyclic pandiagonal Latin square of order 11:",
				"   0  1  2  3  4  5  6  7  8  9 10",
				"   2  3  4  5  6  7  8  9 10  0  1",
				"   4  5  6  7  8  9 10  0  1  2  3",
				"   6  7  8  9 10  0  1  2  3  4  5",
				"   8  9 10  0  1  2  3  4  5  6  7",
				"  10  0  1  2  3  4  5  6  7  8  9",
				"   1  2  3  4  5  6  7  8  9 10  0",
				"   3  4  5  6  7  8  9 10  0  1  2",
				"   5  6  7  8  9 10  0  1  2  3  4",
				"   7  8  9 10  0  1  2  3  4  5  6",
				"   9 10  0  1  2  3  4  5  6  7  8",
				"For order 13 there is a square",
				"   7  1  0  3  6  5 12  2  8  9 10 11  4",
				"   2  3  4 10  0  7  6  9 12 11  5  8  1",
				"   4 11  1  7  8  9 10  3  6  0 12  2  5",
				"   6  5  8 11 10  4  7  0  1  2  3  9 12",
				"   8  9  2  5 12 11  1  4  3 10  0  6  7",
				"   3  6 12  0  1  2  8 11  5  4  7 10  9",
				"  10  0  3  2  9 12  5  6  7  8  1  4 11",
				"   1  7 10  4  3  6  9  8  2  5 11 12  0",
				"  11  4  5  6  7  0  3 10  9 12  2  1  8",
				"   5  8  7  1  4 10 11 12  0  6  9  3  2",
				"  12  2  9  8 11  1  0  7 10  3  4  5  6",
				"   9 10 11 12  5  8  2  1  4  7  6  0  3",
				"   0 12  6  9  2  3  4  5 11  1  8  7 10",
				"that is pandiagonal but not cyclic (Dabbaghian and Wu)."
			],
			"xref": [
				"Cf. A338562, A338620."
			],
			"keyword": "nonn,more,hard",
			"offset": "0,3",
			"author": "_Eduard I. Vatutin_, Mar 08 2021",
			"ext": [
				"Zero terms for even orders removed by _Andrew Howroyd_, May 26 2021"
			],
			"references": 2,
			"revision": 13,
			"time": "2021-05-26T21:41:49-04:00",
			"created": "2021-03-27T01:23:29-04:00"
		}
	]
}