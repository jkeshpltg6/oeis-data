{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A328464",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 328464,
			"data": "1,3,1,7,4,1,9,16,6,1,31,19,36,8,1,33,106,41,78,12,1,37,109,386,85,144,14,1,39,121,391,1002,155,222,18,1,211,124,421,1009,2432,235,324,20,1,213,1156,426,1079,2443,4200,341,438,24,1,217,1159,5006,1086,2575,4213,7430,457,668,30,1,219,1171,5011,17018,2586,4421,7447,12674,691,900,32,1",
			"name": "Square array A(n,k) = A276156((2^(n-1)) * (2k-1)) / A002110(n-1), read by descending antidiagonals.",
			"comment": [
				"Array is read by falling antidiagonals with n (row) and k (column) ranging as: (n,k) = (1,1), (1,2), (2,1), (1,3), (2,2), (3,1), ...",
				"Row n contains all such sums of distinct primorials whose least significant summand is A002110(n-1), with each sum divided by that least significant primorial, which is also the largest primorial which divides that sum."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A328464/b328464.txt\"\u003eTable of n, a(n) for n = 1..10440; the first 144 antidiagonals of array\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#primorialbase\"\u003eIndex entries for sequences related to primorial base\u003c/a\u003e"
			],
			"formula": [
				"A(n,k) = A276156((2^(n-1)) * (2k-1)) / A002110(n-1).",
				"a(n) = A328461(A135764(n)). [When all sequences are considered as one-dimensional]"
			],
			"example": [
				"Top left 9 X 11 corner of the array:",
				"1: | 1,  3,   7,   9,    31,    33,    37,    39,    211,    213,    217",
				"2: | 1,  4,  16,  19,   106,   109,   121,   124,   1156,   1159,   1171",
				"3: | 1,  6,  36,  41,   386,   391,   421,   426,   5006,   5011,   5041",
				"4: | 1,  8,  78,  85,  1002,  1009,  1079,  1086,  17018,  17025,  17095",
				"5: | 1, 12, 144, 155,  2432,  2443,  2575,  2586,  46190,  46201,  46333",
				"6: | 1, 14, 222, 235,  4200,  4213,  4421,  4434,  96578,  96591,  96799",
				"7: | 1, 18, 324, 341,  7430,  7447,  7753,  7770, 215442, 215459, 215765",
				"8: | 1, 20, 438, 457, 12674, 12693, 13111, 13130, 392864, 392883, 393301",
				"9: | 1, 24, 668, 691, 20678, 20701, 21345, 21368, 765050, 765073, 765717"
			],
			"program": [
				"(PARI)",
				"up_to = 105;",
				"A002110(n) = prod(i=1,n,prime(i));",
				"A276156(n) = { my(p=2,pr=1,s=0); while(n,if(n%2,s += pr); n \u003e\u003e= 1; pr *= p; p = nextprime(1+p)); (s); };",
				"A328464sq(n,k) = (A276156((2^(n-1)) * (k+k-1)) / A002110(n-1));",
				"A328464list(up_to) = { my(v = vector(up_to), i=0); for(a=1,oo, for(col=1,a, i++; if(i \u003e up_to, return(v)); v[i] = A328464sq(col,(a-(col-1))))); (v); };",
				"v328464 = A328464list(up_to);",
				"A328464(n) = v328464[n];"
			],
			"xref": [
				"Cf. A328463 (transpose).",
				"Cf. A000265, A002110, A007814, A135764, A276154, A276156,",
				"Rows 1 - 5: A328462, A328465, A328466, A328467, A328468.",
				"Column 2: A008864.",
				"Column 3: A023523 (after its initial term).",
				"Column 4: A286624.",
				"Cf. also arrays A276945, A286625."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Antti Karttunen_, Oct 16 2019",
			"references": 12,
			"revision": 19,
			"time": "2019-10-18T21:29:09-04:00",
			"created": "2019-10-18T21:29:09-04:00"
		}
	]
}