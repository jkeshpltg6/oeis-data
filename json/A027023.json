{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A027023",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 27023,
			"data": "1,1,1,1,1,1,1,3,1,1,1,1,3,5,5,1,1,1,1,3,5,9,13,11,1,1,1,1,3,5,9,17,27,33,25,1,1,1,1,3,5,9,17,31,53,77,85,59,1,1,1,1,3,5,9,17,31,57,101,161,215,221,145,1,1,1,1,3,5,9,17,31,57,105,189,319,477,597,581,367,1",
			"name": "Tribonacci array: triangular array T read by rows: T(n,0)=1 for n \u003e= 0, T(n,1) = T(n,2n) = 1 for n \u003e= 1, T(n,2)=1 for n \u003e= 2 and for n \u003e= 3, T(n,k) = T(n-1,k-3) + T(n-1,k-2) + T(n-1,k-1) for 3 \u003c= k \u003c= 2n-1.",
			"comment": [
				"The n-th row has 2n+1 terms."
			],
			"link": [
				"R. J. Mathar, \u003ca href=\"/A027023/b027023.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e, replaces Zumkeller's file for new offset."
			],
			"example": [
				"The array begins:",
				"  1;",
				"  1, 1, 1;",
				"  1, 1, 1, 3, 1;",
				"  1, 1, 1, 3, 5, 5,  1;",
				"  1, 1, 1, 3, 5, 9, 13, 11, 1;"
			],
			"maple": [
				"T:= proc(n, k) option remember;",
				"      if k\u003c3 or k=2*n  then 1",
				"    else T(n-1, k-3) + T(n-1, k-2) + T(n-1, k-1)",
				"      fi",
				" end proc:",
				"seq(seq(T(n, k), k=0..2*n), n=0..10); # _G. C. Greubel_, Nov 04 2019"
			],
			"mathematica": [
				"T[n_, 0] := 1; T[n_, 1] := 1; T[n_, k_]/; (k==2n) := 1 /; n \u003e=1; T[n_, 2] := 1; T[n_, k_]/; (k \u003c= 2n-1) := T[n, k]=T[n-1, k-3]+T[n-1, k-2]+T[n-1, k-1]."
			],
			"program": [
				"(PARI) {T(n, k) = if( k\u003c0 || k\u003e2*n, 0, if( k\u003c3 || k==2*n, 1, T(n-1, k-3) + T(n-1, k-2) + T(n-1,k-1)))}; /* _Michael Somos_, Feb 14 2004 */",
				"(Haskell)",
				"a027023 n k = a027023_tabf !! (n-1) !! (k-1)",
				"a027023_row n = a027023_tabf !! (n-1)",
				"a027023_tabf = [1] : iterate f [1, 1, 1] where",
				"   f row = 1 : 1 : 1 :",
				"           zipWith3 (((+) .) . (+)) (drop 2 row) (tail row) row ++ [1]",
				"-- _Reinhard Zumkeller_, Jul 06 2014",
				"(Sage)",
				"def T(n, k):",
				"    if (k\u003c3 or k==2*n): return 1",
				"    else: return T(n-1, k-3) + T(n-1, k-2) + T(n-1, k-1)",
				"[[T(n, k) for k in (0..2*n)] for n in (0..10)] # _G. C. Greubel_, Nov 04 2019",
				"(GAP)",
				"T:= function(n,k)",
				"    if k\u003c3 or k=2*n then return 1;",
				"    else return T(n-1, k-3) + T(n-1, k-2) + T(n-1, k-1);",
				"    fi;",
				"  end;",
				"Flat(List([0..10], n-\u003e List([0..2*n], k-\u003e T(n,k) ))); # _G. C. Greubel_, Nov 04 2019"
			],
			"xref": [
				"Columns are essentially constant with values from A000213 (tribonacci numbers).",
				"Diagonals T(n, n+c) are A027024 (c=2), A027025 (c=3), A027026 (c=4).",
				"Diagonals T(n, 2n-c) are A027050 (c=1), A027051 (c=2), A027027 (c=3), A027028 (c=4), A027029 (c=5), A027030 (c=6), A027031 (c=7), A027032 (c=8), A027033 (c=9), A027034 (c=10).",
				"Many other sequences are derived from this one: see A027035 A027036 A027037 A027038 A027039 A027040 A027041 A027042 A027043 A027044 A027045 and A027046 A027047 A027048 A027049.",
				"Other arrays of this type: A027052, A027082, A027113.",
				"Cf. A027907."
			],
			"keyword": "nonn,tabf,nice",
			"offset": "0,8",
			"author": "_Clark Kimberling_",
			"ext": [
				"Edited by _N. J. A. Sloane_ and _Ralf Stephan_, Feb 13 2004",
				"Offset corrected to 0. - _R. J. Mathar_, Jun 24 2020"
			],
			"references": 30,
			"revision": 31,
			"time": "2020-06-24T14:14:12-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}