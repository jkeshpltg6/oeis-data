{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A103306",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 103306,
			"data": "1,1,0,1,0,1,1,0,0,1,1,0,2,0,1,1,0,0,0,0,1,1,0,3,2,3,0,1,1,0,0,0,0,0,0,1,1,0,4,0,6,0,4,0,1,1,0,0,3,0,0,3,0,0,1,1,0,5,0,10,2,10,0,5,0,1,1,0,0,0,0,0,0,0,0,0,0,1,1,0,6,4,15,12,24,12,15,4,6,0,1,1,0,0,0,0",
			"name": "Triangle read by rows: T(n,k) = number of k-subsets of the n-th roots of 1 that add to zero (0 \u003c= k \u003c= n).",
			"comment": [
				"Observe that T(n,k) = binomial(n,k) (mod n). Because the sum of the n n-th roots of unity is 0 for n\u003e1, each row is symmetric for n\u003e1. Hence only k=0..floor(n/2) need to be computed. - _T. D. Noe_, Jan 16 2008"
			],
			"link": [
				"Wouter Meeussen and T. D. Noe, \u003ca href=\"/A103306/b103306.txt\"\u003eRows n=0..43 of triangle, flattened\u003c/a\u003e",
				"Wouter Meeussen, \u003ca href=\"/A103306/a103306.txt\"\u003eMore terms\u003c/a\u003e",
				"Gary Sivek, \u003ca href=\"http://www.emis.de/journals/INTEGERS/papers/k31/k31.Abstract.html\"\u003eOn vanishing sums of distinct roots of unity\u003c/a\u003e, #A31, Integers 10 (2010), 365-368."
			],
			"example": [
				"Triangle begins:",
				"{1},",
				"{1, 0},",
				"{1, 0, 1},",
				"{1, 0, 0, 1},",
				"{1, 0, 2, 0, 1},",
				"{1, 0, 0, 0, 0, 1},",
				"{1, 0, 3, 2, 3, 0, 1},",
				"{1, 0, 0, 0, 0, 0, 0, 1},",
				"{1, 0, 4, 0, 6, 0, 4, 0, 1},",
				"{1, 0, 0, 3, 0, 0, 3, 0, 0, 1},",
				"T(10,4)=10, counting {1,2,6,7}, {1,3,6,8}, {1,4,6,9}, {1,5,6,10}, {2,3,7,8}, {2,4,7,9}, {2,5,7,10}, {3,4,8,9}, {3,5,8,10}, {4,5,9,10}."
			],
			"mathematica": [
				"\u003c\u003cDiscreteMath`Combinatorica`; Table[Count[(KSubsets[Range[n], k]), q_List/;Chop[Plus@@(E^(2.*Pi*I*q/n))]===0], {n, 0, 24}, {k, 0, n}]",
				"T[n_, k_] := T[n, k] = Piecewise[{{T[n, n-k], k \u003e n/2 \u003e= 1}}, Count[Subsets[Range[n], {k}], subset_/;PossibleZeroQ[ExpToTrig[Sum[Exp[2*Pi*I*m/n], {m, subset}]]]]]; Table[T[n, k], {n, 0, 12}, {k, 0, n}] // TableForm (* _David M. Zimmerman_, Sep 23 2020 *)"
			],
			"xref": [
				"Row sums give A103314.",
				"Cf. A070894, A322366."
			],
			"keyword": "nonn,tabl",
			"offset": "0,13",
			"author": "_Wouter Meeussen_, Mar 11 2005",
			"references": 5,
			"revision": 23,
			"time": "2020-10-06T02:41:31-04:00",
			"created": "2005-04-09T03:00:00-04:00"
		}
	]
}