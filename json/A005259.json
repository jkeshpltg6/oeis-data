{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005259",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5259,
			"id": "M4020",
			"data": "1,5,73,1445,33001,819005,21460825,584307365,16367912425,468690849005,13657436403073,403676083788125,12073365010564729,364713572395983725,11111571997143198073,341034504521827105445,10534522198396293262825,327259338516161442321485",
			"name": "Apery (Apéry) numbers: Sum_{k=0..n} (binomial(n,k)*binomial(n+k,k))^2.",
			"comment": [
				"Conjecture: For each n = 1,2,3,... the Apéry polynomial A_n(x) = Sum_{k = 0..n} binomial(n,k)^2*binomial(n+k,k)^2*x^k is irreducible over the field of rational numbers. - _Zhi-Wei Sun_, Mar 21 2013",
				"The expansions of exp( Sum_{n \u003e= 1} a(n)*x^n/n ) = 1 + 5*x + 49*x^2 + 685*x^3 + 11807*x^4 + 232771*x^5 + ... and exp( Sum_{n \u003e= 1} a(n-1)*x^n/n ) = 1 + 3*x + 27*x^2 + 390*x^3 + 7038*x^4 + 144550*x^5 + ... both appear to have integer coefficients. See A267220. - _Peter Bala_, Jan 12 2016",
				"Diagonal of the rational function R(x, y, z, w) = 1 / (1 - (w*x*y*z + w*x*y + w*z + x*y + x*z + y + z)); also diagonal of rational function H(x, y, z, w) = 1/(1 - w*(1+x)*(1+y)*(1+z)*(x*y*z + y*z + y + z + 1)). - _Gheorghe Coserea_, Jun 26 2018",
				"Named after the Greek-French mathematician Roger Apéry (1916-1994). - _Amiram Eldar_, Jun 10 2021"
			],
			"reference": [
				"Julian Havil, The Irrationals, Princeton University Press, Princeton and Oxford, 2012, pp. 137-153.",
				"Wolfram Koepf, Hypergeometric Identities. Ch. 2 in Hypergeometric Summation: An Algorithmic Approach to Summation and Special Function Identities. Braunschweig, Germany: Vieweg, pp. 55, 119 and 146, 1998.",
				"Maxim Kontsevich and Don Zagier, Periods, pp. 771-808 of B. Engquist and W. Schmid, editors, Mathematics Unlimited - 2001 and Beyond, 2 vols., Springer-Verlag, 2001.",
				"Leonard Lipshitz and Alfred van der Poorten, \"Rational functions, diagonals, automata and arithmetic.\" In Number Theory, Richard A. Mollin, ed., Walter de Gruyter, Berlin (1990), pp. 339-358.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A005259/b005259.txt\"\u003eTable of n, a(n) for n = 0..656\u003c/a\u003e (first 101 terms from T. D. Noe)",
				"Boris Adamczewski, Jason P. Bell and Eric Delaygue, \u003ca href=\"http://arxiv.org/abs/1603.04187\"\u003eAlgebraic independence of G-functions and congruences \"a la Lucas\"\u003c/a\u003e, arXiv preprint arXiv:1603.04187 [math.NT], 2016.",
				"Jean-Paul Allouche, \u003ca href=\"http://www.math.jussieu.fr/~allouche/bibliorecente.html\"\u003eA remark on Apéry's numbers\u003c/a\u003e, J. Comput. Appl. Math., Vol. 83 (1997), pp. 123-125.",
				"Roger Apéry, \u003ca href=\"http://www.numdam.org/book-part/AST_1979__61__11_0/\"\u003eIrrationalité de zeta(2) et zeta(3)\u003c/a\u003e, in Journées Arith. de Luminy. Colloque International du Centre National de la Recherche Scientifique (CNRS) held at the Centre Universitaire de Luminy, Luminy, Jun 20-24, 1978. Astérisque, Vol. 61 (1979), pp. 11-13.",
				"Roger Apéry, \u003ca href=\"http://www.numdam.org/item?id=GAU_1981-1982__9_1_A9_0\"\u003eSur certaines séries entières arithmétiques\u003c/a\u003e, Groupe de travail d'analyse ultramétrique, Vol. 9, No. 1 (1981-1982), Exp. No. 16, 2 p.",
				"Roger Apéry, \u003ca href=\"https://someclassicalmaths.files.wordpress.com/2011/10/apery-1981-paper.pdf\"\u003eInterpolation de fractions continues et irrationalité de certaines constantes\u003c/a\u003e, Bulletin de la section des sciences du C.T.H.S III (1981), pp. 37-53.",
				"Thomas Baruchel and Carsten Elsner, \u003ca href=\"http://arxiv.org/abs/1602.06445\"\u003eOn error sums formed by rational approximations with split denominators\u003c/a\u003e, arXiv preprint arXiv:1602.06445 [math.NT], 2016.",
				"Frits Beukers, \u003ca href=\"http://dx.doi.org/10.1016/0022-314X(87)90025-4\"\u003eAnother congruence for the Apéry numbers\u003c/a\u003e, J. Number Theory, Vol. 25, No. 2 (1987), pp. 201-210.",
				"Frits Beukers, \u003ca href=\"https://www.researchgate.net/profile/F-Beukers/publication/27708775_Consequences_of_Apery%27s_work_on_z3\"\u003eConsequences of Apéry's work on zeta(3)\u003c/a\u003e, in \"Zeta(3) irrationnel: les retombées\", Rencontres Arithmétiques de Caen, June 2-3, 1995 [Mentions divisibility of a(n) by powers of 5 and powers of 11]",
				"Francis Brown, \u003ca href=\"http://arxiv.org/abs/1412.6508\"\u003eIrrationality proofs for zeta values, moduli spaces and dinner parties\u003c/a\u003e, arXiv:1412.6508 [math.NT], 2014.",
				"William Y. C. Chen, Qing-Hu Hou and Yan-Ping Mu, \u003ca href=\"http://dx.doi.org/10.1016/j.cam.2005.10.010\"\u003eA telescoping method for double summations\u003c/a\u003e, J. Comp. Appl. Math., Vol. 196, No. 2 (2006), pp. 553-566, Example 4.",
				"M. Coster, \u003ca href=\"/A001850/a001850_1.pdf\"\u003eEmail, Nov 1990\u003c/a\u003e",
				"Eric Delaygue, \u003ca href=\"http://arxiv.org/abs/1310.4131\"\u003eArithmetic properties of Apéry-like numbers\u003c/a\u003e, arXiv preprint arXiv:1310.4131 [math.NT], 2013.",
				"Emeric Deutsch and Bruce E. Sagan, \u003ca href=\"https://arxiv.org/abs/math/0407326\"\u003eCongruences for Catalan and Motzkin numbers and related sequences\u003c/a\u003e, arXiv:math/0407326 [math.CO], 2004.",
				"Emeric Deutsch and Bruce E. Sagan, \u003ca href=\"https://doi.org/10.1016/j.jnt.2005.06.005\"\u003eCongruences for Catalan and Motzkin numbers and related sequences\u003c/a\u003e, J. Num. Theory 117 (2006), 191-215.",
				"Gerald A. Edgar, \u003ca href=\"https://web.archive.org/web/20150911173217/http://mathforum.org/kb/message.jspa?messageID=3704516\u0026amp;tstart=0\"\u003eA formula with Legendre polynomials\u003c/a\u003e, Sci. Math. Research posting Mar 21 2005.",
				"G. A. Edgar, \u003ca href=\"https://arxiv.org/abs/2005.10733\"\u003eThe Apéry Numbers as a Stieltjes Moment Sequence\u003c/a\u003e, arXiv:2005.10733 [math.CA], 2020.",
				"Carsten Elsner, \u003ca href=\"http://www.fq.math.ca/Papers1/43-1/paper43-1-5.pdf\"\u003eOn recurrence formulas for sums involving binomial coefficients\u003c/a\u003e, Fib. Q., Vol. 43, No. 1 (2005), pp. 31-45.",
				"Carsten Elsner, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL11/Elsner/elsner7.html\"\u003eOn prime-detecting sequences from Apéry's recurrence formulas for zeta(3) and zeta(2)\u003c/a\u003e, JIS, Vol. 11 (2008), Article 08.5.1.",
				"Stéphane Fischler, \u003ca href=\"https://arxiv.org/abs/math/0303066\"\u003eIrrationalité de valeurs de zeta\u003c/a\u003e, arXiv:math/0303066 [math.NT], 2003.",
				"Scott Garrabrant and Igor Pak, \u003ca href=\"http://arxiv.org/abs/1407.8222\"\u003eCounting with irrational tiles\u003c/a\u003e, arXiv:1407.8222 [math.CO], 2014.",
				"Ira Gessel, \u003ca href=\"https://doi.org/10.1016/0022-314X(82)90071-3\"\u003eSome congruences for Apéry numbers\u003c/a\u003e, Journal of Number Theory, Vol. 14, No. 3 (1982) 362-368.",
				"Ofir Gorodetsky, \u003ca href=\"https://arxiv.org/abs/2102.11839\"\u003eNew representations for all sporadic Apéry-like sequences, with applications to congruences\u003c/a\u003e, arXiv:2102.11839 [math.NT], 2021. See gamma p. 3.",
				"Richard K. Guy, \u003ca href=\"/A005258/a005258.pdf\"\u003eLetter to N. J. A. Sloane, Oct 1985\u003c/a\u003e",
				"Vaclav Kotesovec, \u003ca href=\"https://oeis.org/wiki/User:Vaclav_Kotesovec\"\u003eAsymptotic of generalized Apéry sequences with powers of binomial coefficients\u003c/a\u003e, Nov 04 2012",
				"Leonard Lipshitz and Alfred J. van der Poorten, \u003ca href=\"https://doi.org/10.1515/9783110848632-029\"\u003eRational functions, diagonals, automata and arithmetic\u003c/a\u003e, in: Richard A. Mollin (ed.), Number theory, Proceedings of the First Conference of the Canadian Number Theory Association Held at the Banff Center, Banff, Alberta, April 17-27, 1988, de Gruyter, 2016, pp. 339-358; \u003ca href=\"https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.87.3448\u0026amp;rep=rep1\u0026amp;type=pdf\"\u003ealternative link\u003c/a\u003e; \u003ca href=\"https://web.archive.org/web/20040405052230/http://www-centre.mpce.mq.edu.au/alfpapers/a084.pdf\"\u003eWayback Machine copy\u003c/a\u003e.",
				"Ji-Cai Liu, \u003ca href=\"https://arxiv.org/abs/1803.11442\"\u003eSupercongruences for the (p-1)th Apéry number\u003c/a\u003e, arXiv:1803.11442 [math.NT], 2018.",
				"Amita Malik and Armin Straub, \u003ca href=\"https://doi.org/10.1007/s40993-016-0036-8\"\u003eDivisibility properties of sporadic Apéry-like numbers\u003c/a\u003e, Research in Number Theory, Vol. 2 (2016), Article 5.",
				"Stephen Melczer and Bruno Salvy, \u003ca href=\"http://arxiv.org/abs/1605.00402\"\u003eSymbolic-Numeric Tools for Analytic Combinatorics in Several Variables\u003c/a\u003e, arXiv:1605.00402 [cs.SC], 2016.",
				"Romeo Meštrović, \u003ca href=\"http://arxiv.org/abs/1409.3820\"\u003eLucas' theorem: its generalizations, extensions and applications (1878--2014)\u003c/a\u003e, arXiv preprint arXiv:1409.3820 [math.NT], 2014.",
				"Robert Osburn and Brundaban Sahu, \u003ca href=\"http://doi.org/10.7169/facm/2013.48.1.3\"\u003eA supercongruence for generalized Domb numbers\u003c/a\u003e, Funct. Approx. Comment. Math., Vol. 48, No. 1 (2013), pp. 29-36; \u003ca href=\"https://maths.ucd.ie/~osburn/superdomb.pdf\"\u003ealternative link\u003c/a\u003e.",
				"Math Overflow, \u003ca href=\"http://mathoverflow.net/questions/178790/a-conjectured-formula-for-apery-numbers\"\u003eA conjectured formula for Apéry numbers\u003c/a\u003e",
				"Eric Rowland and Reem Yassawi, \u003ca href=\"http://www.numdam.org/item/JTNB_2015__27_1_245_0/\"\u003eAutomatic congruences for diagonals of rational functions\u003c/a\u003e, Journal de théorie des nombres de Bordeaux, Vol. 27, No. 1 (2015), pp. 245-288; \u003ca href=\"http://arxiv.org/abs/1310.8635\"\u003earXiv preprint\u003c/a\u003e, arXiv:1310.8635 [math.NT], 2013-2014.",
				"Eric Rowland, Reem Yassawi and Christian Krattenthaler, \u003ca href=\"https://arxiv.org/abs/2005.04801\"\u003eLucas congruences for the Apéry numbers modulo p^2\u003c/a\u003e, arXiv:2005.04801 [math.NT], 2020.",
				"Andrew Strangeway, \u003ca href=\"http://arxiv.org/abs/1302.5089\"\u003eA Reconstruction Theorem for Quantum Cohomology of Fano Bundles on Projective Space\u003c/a\u003e, arXiv preprint arXiv:1302.5089 [math.AG], 2013.",
				"Andrew Strangeway, \u003ca href=\"http://dx.doi.org/10.1215/00277630-2817545\"\u003eQuantum reconstruction for Fano bundles on projective space\u003c/a\u003e, Nagoya Math. J., Vol. 218 (2015), pp. 1-28.",
				"Armin Straub, \u003ca href=\"http://dx.doi.org/10.2140/ant.2014.8.1985\"\u003eMultivariate Apéry numbers and supercongruences of rational functions\u003c/a\u003e, Algebra \u0026 Number Theory, Vol. 8, No. 8 (2014), pp. 1985-2008; \u003ca href=\"https://arxiv.org/abs/1401.0854\"\u003earXiv preprint\u003c/a\u003e, arXiv:1401.0854 [math.NT], 2014.",
				"Volker Strehl, \u003ca href=\"http://www.mat.univie.ac.at/~slc/opapers/s29strehl.html\"\u003eRecurrences and Legendre transform\u003c/a\u003e, Séminaire Lotharingien de Combinatoire, B29b (1992), 22 pp.",
				"Volker Strehl, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(94)00118-3\"\u003eBinomial identities -- combinatorial and algorithmic aspects\u003c/a\u003e, Discrete Mathematics, Vol. 136 (1994), 309-346.",
				"Zhi-Hong Sun, \u003ca href=\"https://arxiv.org/abs/1803.10051\"\u003eCongruences for Apéry-like numbers\u003c/a\u003e, arXiv:1803.10051 [math.NT], 2018.",
				"Zhi-Hong Sun, \u003ca href=\"https://arxiv.org/abs/2004.07172\"\u003eNew congruences involving Apéry-like numbers\u003c/a\u003e, arXiv:2004.07172 [math.NT], 2020.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1112.1034\"\u003eCongruences for Franel numbers\u003c/a\u003e, arXiv preprint arXiv:1112.1034 [math.NT], 2011.",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2012.05.014\"\u003eOn sums of Apéry polynomials and related congruences\u003c/a\u003e, J. Number Theory 132(2012), 2673-2699. [_Zhi-Wei Sun_, Mar 21 2013]",
				"Zhi-Wei Sun. Sun, \u003ca href=\"http://arxiv.org/abs/1101.1946\"\u003eOn sums of Apéry polynomials and related congruences\u003c/a\u003e, arXiv:1101.1946 [math.NT], 2011-2014. [_Zhi-Wei Sun_, Mar 21 2013]",
				"Alfred van der Poorten, \u003ca href=\"http://pracownicy.uksw.edu.pl/mwolf/Poorten_MI_195_0.pdf\"\u003eA proof that Euler missed ...\u003c/a\u003e, Math. Intelligencer, Vol. 1, No. 4 (December 1979), pp. 196-203, (b_n) after eq. (1.2), and Exercise 3.",
				"Chen Wang, \u003ca href=\"https://arxiv.org/abs/1909.08983\"\u003eTwo congruences concerning Apéry numbers\u003c/a\u003e, arXiv:1909.08983 [math.NT], 2019.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/AperyNumber.html\"\u003eApéry Number\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/StrehlIdentities.html\"\u003eStrehl Identities\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/SchmidtsProblem.html\"\u003eSchmidt's Problem\u003c/a\u003e.",
				"Ernest X. W. Xia and Olivia X. M. Yao, \u003ca href=\"https://doi.org/10.37236/3412\"\u003eA Criterion for the Log-Convexity of Combinatorial Sequences\u003c/a\u003e, The Electronic Journal of Combinatorics, Vol. 20 (2013), #P3."
			],
			"formula": [
				"D-finite with recurrence (n+1)^3*a(n+1) = (34*n^3 + 51*n^2 + 27*n + 5)*a(n) - n^3*a(n-1), n \u003e= 1.",
				"Representation as a special value of the hypergeometric function 4F3, in Maple notation: a(n)=hypergeom([n+1, n+1, -n, -n], [1, 1, 1], 1), n=0, 1, ... - _Karol A. Penson_ Jul 24 2002",
				"a(n) = Sum_{k \u003e= 0} A063007(n, k)*A000172(k)). A000172 = Franel numbers. - _Philippe Deléham_, Aug 14 2003",
				"G.f.: (-1/2)*(3*x - 3 + (x^2-34*x+1)^(1/2))*(x+1)^(-2)*hypergeom([1/3,2/3],[1],(-1/2)*(x^2 - 7*x + 1)*(x+1)^(-3)*(x^2 - 34*x + 1)^(1/2)+(1/2)*(x^3 + 30*x^2 - 24*x + 1)*(x+1)^(-3))^2. - _Mark van Hoeij_, Oct 29 2011",
				"Let g(x, y) = 4*cos(2*x) + 8*sin(y)*cos(x) + 5 and let P(n,z) denote the Legendre polynomial of degree n. Then G. A. Edgar posted a conjecture of Alexandru Lupas that a(n) equals the double integral 1/(4*Pi^2)*int {y = -Pi..Pi} int {x = -Pi..Pi} P(n,g(x,y)) dx dy. (Added Jan 07 2015: Answered affirmatively in Math Overflow question 178790) - _Peter Bala_, Mar 04 2012; edited by _G. A. Edgar_, Dec 10 2016",
				"a(n) ~ (1+sqrt(2))^(4*n+2)/(2^(9/4)*Pi^(3/2)*n^(3/2)). - _Vaclav Kotesovec_, Nov 01 2012",
				"a(n) = Sum_{k=0..n} C(n,k)^2 * C(n+k,k)^2. - _Joerg Arndt_, May 11 2013",
				"0 = (-x^2+34*x^3-x^4)*y''' + (-3*x+153*x^2-6*x^3)*y'' + (-1+112*x-7*x^2)*y' + (5-x)*y, where y is g.f. - _Gheorghe Coserea_, Jul 14 2016",
				"From _Peter Bala_, Jan 18 2020: (Start)",
				"a(n) = Sum_{0 \u003c= j, k \u003c= n} (-1)^(n+j) * C(n,k)^2 * C(n+k,k)^2 * C(n,j) * C(n+k+j,k+j).",
				"a(n) = Sum_{0 \u003c= j, k \u003c= n} C(n,k) * C(n+k,k) * C(k,j)^3 (see Koepf, p. 55).",
				"a(n) = Sum_{0 \u003c= j, k \u003c= n} C(n,k)^2 * C(n,j)^2 * C(3*n-j-k,2*n) (see Koepf, p. 119).",
				"Diagonal coefficients of the rational function 1/((1 - x - y)*(1 - z - t) - x*y*z*t) (Straub, 2014). (End)",
				"a(n) = [x^n] 1/(1 - x)*( Legendre_P(n,(1 + x)/(1 - x)) )^m at m = 2. At m = 1 we get the Apéry numbers A005258. - _Peter Bala_, Dec 22 2020"
			],
			"example": [
				"G.f. = 1 + 5*x + 73*x^2 + 1445*x^3 + 33001*x^4 + 819005*x^5 + 21460825*x^6 + ...",
				"a(2) = (binomial(2,0) * binomial(2+0,0))^2 + (binomial(2,1) * binomial(2+1,1))^2 + (binomial(2,2) * binomial(2+2,2))^2 = (1*1)^2 + (2*3)^2 + (1*6)^2 = 1 + 36 + 36 = 73. - _Michael B. Porter_, Jul 14 2016"
			],
			"maple": [
				"a := proc(n) option remember; if n=0 then 1 elif n=1 then 5 else (n^(-3))* ( (34*(n-1)^3 + 51*(n-1)^2 + 27*(n-1) +5)*a((n-1)) - (n-1)^3*a((n-1)-1)); fi; end;",
				"# Alternative:",
				"a := n -\u003e hypergeom([-n, -n, 1+n, 1+n], [1, 1, 1], 1):",
				"seq(simplify(a(n)), n=0..17); # _Peter Luschny_, Jan 19 2020"
			],
			"mathematica": [
				"Table[HypergeometricPFQ[{-n, -n, n+1, n+1}, {1,1,1}, 1],{n,0,13}] (* _Jean-François Alcover_, Apr 01 2011 *)",
				"Table[Sum[(Binomial[n,k]Binomial[n+k,k])^2,{k,0,n}],{n,0,30}] (* _Harvey P. Dale_, Oct 15 2011 *)",
				"a[ n_] := SeriesCoefficient[ SeriesCoefficient[ SeriesCoefficient[ SeriesCoefficient[ 1 / (1 - t (1 + x ) (1 + y ) (1 + z ) (x y z + (y + 1) (z + 1))), {t, 0, n}], {x, 0, n}], {y, 0, n}], {z, 0, n}]; (* _Michael Somos_, May 14 2016 *)"
			],
			"program": [
				"(PARI) a(n)=sum(k=0,n,(binomial(n,k)*binomial(n+k,k))^2) \\\\ _Charles R Greathouse IV_, Nov 20 2012",
				"(Haskell)",
				"a005259 n = a005259_list !! n",
				"a005259_list = 1 : 5 : zipWith div (zipWith (-)",
				"   (tail $ zipWith (*) a006221_list a005259_list)",
				"   (zipWith (*) (tail a000578_list) a005259_list)) (drop 2 a000578_list)",
				"-- _Reinhard Zumkeller_, Mar 13 2014",
				"(GAP) List([0..20],n-\u003eSum([0..n],k-\u003eBinomial(n,k)^2*Binomial(n+k,k)^2)); # _Muniru A Asiru_, Sep 28 2018",
				"(MAGMA) [\u0026+[Binomial(n, k) ^2 *Binomial(n+k, k)^2: k in [0..n]]:n in  [0..17]]; // _Marius A. Burtea_, Jan 20 2020"
			],
			"xref": [
				"Cf. A002736, A005258, A005429, A005430, A059415, A059416, A063007, A000172.",
				"Cf. A006221, A000578, A006353.",
				"Related to diagonal of rational functions: A268545-A268555.",
				"The Apéry-like numbers [or Apéry-like sequences, Apery-like numbers, Apery-like sequences] include A000172, A000984, A002893, A002895, A005258, A005259, A005260, A006077, A036917, A063007, A081085, A093388, A125143 (apart from signs), A143003, A143007, A143413, A143414, A143415, A143583, A183204, A214262, A219692,A226535, A227216, A227454, A229111 (apart from signs), A260667, A260832, A262177, A264541, A264542, A279619, A290575, A290576. (The term \"Apery-like\" is not well-defined.)",
				"For primes that do not divide the terms of the sequences A000172, A005258, A002893, A081085, A006077, A093388, A125143, A229111, A002895, A290575, A290576, A005259 see A260793, A291275-A291284 and A133370 respectively.",
				"Cf. A092826 (prime terms)."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,2",
			"author": "_Simon Plouffe_, _N. J. A. Sloane_, May 20 1991",
			"references": 92,
			"revision": 285,
			"time": "2021-11-02T19:32:27-04:00",
			"created": "1991-05-20T03:00:00-04:00"
		}
	]
}