{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A136621",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 136621,
			"data": "1,1,1,1,3,1,1,7,5,1,1,11,20,9,1,1,18,51,48,13,1,1,26,112,169,100,20,1,1,38,221,486,461,194,28,1,1,52,411,1210,1667,1128,352,40,1,1,73,720,2761,5095,4959,2517,615,54,1,1,97,1221,5850,13894,18084,13241,5288,1034,75,1",
			"name": "Transpose T(n,k) of Parker's partition triangle A047812 (n \u003e= 1 and 0 \u003c= k \u003c= n-1).",
			"comment": [
				"Parker's triangle is closely associated with q-binomial coefficients and Gaussian polynomials; cf. A063746. For example, row 4 of A063746 is 1, 1, 2, 3, 5, 5, 7, 7, 8, 7, 7, 5, 5, 3, 2, 1, 1, the coefficients of [8, 4], while the entries in row 4 of A047812 are the coefficients of q^(k*(4+1)) = q^(5*k) in [8, 4] where k runs from 0 to n-1 = 3. Likewise, by symmetry, \"1 7 5 1\" is embedded also because they are the coefficients of q^(5*(3-k)), where k runs from 0 to n-1 = 3. [Edited by _Petros Hadjicostas_, May 30 2020]"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A136621/b136621.txt\"\u003eRows n = 1..141, flattened\u003c/a\u003e",
				"R. K. Guy, \u003ca href=\"http://www.jstor.org/stable/2324467\"\u003eParker's permutation problem involves the Catalan numbers\u003c/a\u003e, Amer. Math. Monthly 100 (1993), 287-289.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/E._T._Parker\"\u003eE. T. Parker\u003c/a\u003e."
			],
			"example": [
				"Row four of A047812 is 1 5 7 1, so row four of the present entry is 1 7 5 1.",
				"From _Petros Hadjicostas_, May 30 2020: (Start)",
				"Triangle T(n,k) (with rows n \u003e= 1 and columns k = 0..n-1) begins:",
				"  1;",
				"  1,  1;",
				"  1,  3,   1;",
				"  1,  7,   5,    1;",
				"  1, 11,  20,    9,    1;",
				"  1, 18,  51,   48,   13,    1;",
				"  1, 26, 112,  169,  100,   20,   1;",
				"  1, 38, 221,  486,  461,  194,  28,  1;",
				"  1, 52, 411, 1210, 1667, 1128, 352, 40, 1;",
				"  ... (End)"
			],
			"maple": [
				"b:= proc(n, i, t) option remember; `if`(n=0, 1, `if`(t*i",
				"      \u003cn, 0, b(n, i-1, t)+b(n-i, min(i, n-i), t-1)))",
				"    end:",
				"T:= (n, k)-\u003e b((n-k-1)*(n+1), n$2):",
				"seq(seq(T(n, k), k=0..n-1), n=1..12);  # _Alois P. Heinz_, May 30 2020"
			],
			"mathematica": [
				"T[n_, k_]:= SeriesCoefficient[QBinomial[2*n, n, q], {q, 0, k*(n+1)}];",
				"Table[T[n, n-k], {n,12}, {k,n}]//Flatten (* _G. C. Greubel_, May 31 2020 *)",
				"b[n_, i_, t_] := b[n, i, t] = If[n == 0, 1, If[n \u003c 0 || t i \u003c n, 0, b[n, i - 1, t] + b[n - i, Min[i, n - i], t - 1]]];",
				"T[n_, k_] := b[(n-k-1)(n+1), n, n];",
				"Table[T[n, k], {n, 1, 12}, {k, 0, n-1}] // Flatten (* _Jean-François Alcover_, Nov 27 2020, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(PARI) T(n, k) = #partitions(k*(n+1), n, n);",
				"for (n=1, 10, for (k=0, n-1, print1(T(n, n-1-k), \", \"); ); print(); ); \\\\ _Petros Hadjicostas_, May 30 2020",
				"/* Second program, courtesy of _G. C. Greubel_ */",
				"T(n,k) = polcoeff(prod(j=0, n-1, (1-q^(2*n-j))/(1-q^(j+1)) ), k*(n+1) );",
				"vector(12, n, vector(n, k, T(n,n-k))) \\\\ _Petros Hadjicostas_, May 31 2020",
				"(Sage)",
				"def T(n,k):",
				"    P.\u003cx\u003e = PowerSeriesRing(ZZ, k*(n+1)+1)",
				"    return P( q_binomial(2*n, n, x) ).list()[k*(n+1)]",
				"[[ T(n,n-k) for k in (1..n)] for n in (1..12)] # _G. C. Greubel_, May 31 2020"
			],
			"xref": [
				"Cf. A000108 (Catalan row sums), A047812, A063746."
			],
			"keyword": "nonn,tabl",
			"offset": "1,5",
			"author": "_Alford Arnold_, Jan 26 2008",
			"ext": [
				"Name edited by _Petros Hadjicostas_, May 30 2020"
			],
			"references": 4,
			"revision": 37,
			"time": "2020-11-27T23:43:13-05:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}