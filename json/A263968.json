{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A263968",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 263968,
			"data": "-3,4,-18,112,-930,9664,-120498,1752832,-29140290,545004544,-11325668178,258892951552,-6456024679650,174410345857024,-5074158021135858,158168121299894272,-5258993667674555010,185786981314092335104,-6949466928081909755538",
			"name": "a(n) = Li_{-n}(phi) + Li_{-n}(1-phi), where Li_n(x) is the polylogarithm, phi=(1+sqrt(5))/2 is the golden ratio.",
			"comment": [
				"2*Li_{-n}(phi) = a(n) - (-1)^n*A000557(n)*sqrt(5), so a(n) represents integer terms in 2*Li_{-n}(phi), and A000557(n) (with alternating signs) represents terms proportional to sqrt(5)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A263968/b263968.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e [a(62) corrected by _Georg Fischer_, Jun 29 2021]",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Polylogarithm.html\"\u003ePolylogarithm\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GoldenRatio.html\"\u003eGolden Ratio\u003c/a\u003e."
			],
			"formula": [
				"a(n) = (-1)^(n+1)*Sum_{k=0..n} k!*Lucas(k+2)*Stirling2(n,k), where Lucas(n) = A000032(n) and A048993(n,k) = Stirling2(n,k).",
				"a(n) = (-1)^(n+1)*(2*A000556(n) + A000557(n)).",
				"E.g.f.: -(1+2*exp(x))/(1+2*sinh(x)).",
				"a(n) ~ (-1)^(n+1) * n! / log((1+sqrt(5))/2)^(n+1). - _Vaclav Kotesovec_, Oct 31 2015"
			],
			"example": [
				"For n = 4, Li_{-4}(phi) = -930 - 416*sqrt(5), so a(4) = -930 and A000557(4) = 416."
			],
			"maple": [
				"a := n -\u003e polylog(-n,(1+sqrt(5))/2)+polylog(-n,(1-sqrt(5))/2):",
				"seq(round(evalf(a(n),32)), n=0..18); # _Peter Luschny_, Nov 01 2015"
			],
			"mathematica": [
				"Round@Table[PolyLog[-n, GoldenRatio] + PolyLog[-n, 1-GoldenRatio], {n, 0, 20}]",
				"Table[(-1)^(n+1) Sum[k! LucasL[k+2] StirlingS2[n, k], {k, 0, n}], {n, 0, 20}]"
			],
			"program": [
				"(PARI) vector(100, n, n--; (-1)^(n+1)*sum(k=0, n, k!*stirling(n, k, 2)*(2*fibonacci(k+1) + fibonacci(k+2)))) \\\\ _Altug Alkan_, Oct 31 2015"
			],
			"xref": [
				"Cf. A000032, A000556, A000557, A048993."
			],
			"keyword": "sign",
			"offset": "0,1",
			"author": "_Vladimir Reshetnikov_, Oct 30 2015",
			"references": 2,
			"revision": 31,
			"time": "2021-06-29T10:18:25-04:00",
			"created": "2015-11-01T14:10:47-05:00"
		}
	]
}