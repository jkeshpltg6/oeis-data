{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A191434",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 191434,
			"data": "1,4,2,11,6,3,30,17,9,5,80,46,25,14,7,210,121,66,38,19,8,551,318,174,100,51,22,10,1444,834,457,263,135,59,27,12,3781,2184,1197,690,354,155,72,32,13,9900,5719,3135,1807,928,407,189,85,35,15,25920,14974",
			"name": "Dispersion of ([nx+n+3/2]), where x=(golden ratio) and [ ]=floor, by antidiagonals.",
			"comment": [
				"Background discussion:  Suppose that s is an increasing sequence of positive integers, that the complement t of s is infinite, and that t(1)=1.  The dispersion of s is the array D whose n-th row is (t(n), s(t(n)), s(s(t(n)), s(s(s(t(n)))), ...).  Every positive integer occurs exactly once in D, so that, as a sequence, D is a permutation of the positive integers.  The sequence u given by u(n)=(number of the row of D that contains n) is a fractal sequence.  Examples:",
				"(1) s=A000040 (the primes), D=A114537, u=A114538.",
				"(2) s=A022343 (without initial 0), D=A035513 (Wythoff array), u=A003603.",
				"(3) s=A007067, D=A035506 (Stolarsky array), u=A133299.",
				"More recent examples of dispersions: A191426-A191455."
			],
			"example": [
				"Northwest corner:",
				"1.....4....11....30...80",
				"2.....6....17....46...121",
				"3.....9....25....66...174",
				"5.....14...38...100...263",
				"7.....19...51...135...354"
			],
			"mathematica": [
				"(* Program generates the dispersion array T of increasing sequence f[n] *)",
				"r = 40; r1 = 12; c = 40; c1 = 12;",
				"x = 1 + GoldenRatio; f[n_] := Floor[n*x + 3/2]",
				"(* f(n) is complement of column 1 *)",
				"mex[list_] :=  NestWhile[#1 + 1 \u0026, 1, Union[list][[#1]] \u003c= #1 \u0026, 1,",
				"  Length[Union[list]]]",
				"rows = {NestList[f, 1, c]};",
				"Do[rows = Append[rows, NestList[f, mex[Flatten[rows]], r]], {r}];",
				"t[i_, j_] := rows[[i, j]];",
				"TableForm[Table[t[i, j], {i, 1, 10}, {j, 1, 10}]]",
				"(* A191434 array *)",
				"Flatten[Table[",
				"  t[k, n - k + 1], {n, 1, c1}, {k, 1, n}]]",
				"(* A191434 sequence *)",
				"(* Program by _Peter J. C. Moses_, Jun 01 2011 *)"
			],
			"xref": [
				"Cf. A114537, A035513, A035506, A191426."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Jun 04 2011",
			"references": 1,
			"revision": 9,
			"time": "2014-02-14T00:29:05-05:00",
			"created": "2011-06-06T12:45:02-04:00"
		}
	]
}