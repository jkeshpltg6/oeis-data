{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A131946",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 131946,
			"data": "1,-4,4,-4,20,-24,4,-32,52,-4,24,-48,20,-56,32,-24,116,-72,4,-80,120,-32,48,-96,52,-124,56,-4,160,-120,24,-128,244,-48,72,-192,20,-152,80,-56,312,-168,32,-176,240,-24,96,-192,116,-228,124,-72,280,-216,4",
			"name": "Expansion of (phi(-q) * phi(-q^3))^2 in powers of q where phi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Cubic AGM theta functions: a(q) (see A004016), b(q) (A005928), c(q) (A005882)."
			],
			"reference": [
				"N. J. Fine, Basic Hypergeometric Series and Applications, Amer. Math. Soc., 1988; p. 85, Eq. (32.66)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A131946/b131946.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of (4*a(q^2)^2 - a(q)^2) / 3 in powers of q where a() is a cubic AGM theta function.",
				"Expansion of (b(q)^2 / b(q^2)) * (c(q)^2 / c(q^2)) / 3 in powers of q where b(), c() are cubic AGM theta functions.",
				"Expansion of (eta(q) * eta(q^3))^4 / ( eta(q^2) * eta(q^6))^2 in powers of q.",
				"Euler transform of period 6 sequence [-4, -2, -8, -2, -4, -4, ...].",
				"a(n) = -4 * b(n) where b() is multiplicative with b(2^e) = 3 - 2^(e+1), b(3^e) = 1, b(p^e) = (p^(e+1) - 1) / (p - 1) if p\u003e3. - _Michael Somos_, Sep 19 2013",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (6 t)) = 48 (t/i)^2 g(t) where q = exp(2 Pi i t) and g() is the g.f. for A111932. - _Michael Somos_, Sep 19 2013",
				"G.f.: 1 - 4 * (Sum_{k\u003e0} k * (-x)^k / (1 - x^k) * Kronecker(9, k)) = (theta_3(-x) * theta_3(-x^3))^2.",
				"a(n) = (-1)^n * A034896(n). a(n) = -4 * A131947(n) unless n = 0.",
				"a(3*n) = a(n). a(2*n) = A125514(n). - _Michael Somos_, Sep 19 2013"
			],
			"example": [
				"G.f. = 1 - 4*q + 4*q^2 - 4*q^3 + 20*q^4 - 24*q^5 + 4*q^6 - 32*q^7 + 52*q^8 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (EllipticTheta[ 4, 0, q] EllipticTheta[ 4, 0, q^3])^2, {q, 0, n}]; (* _Michael Somos_, Sep 19 2013 *)",
				"a[ n_] := SeriesCoefficient[ (QPochhammer[ q] QPochhammer[ q^3])^4 / (QPochhammer[ q^2] QPochhammer[ q^6])^2, {q, 0, n}]; (* _Michael Somos_, Sep 19 2013 *)",
				"a[ n_] := If[ n \u003c 1, Boole[n == 0], -4 Sum[ d {0, 1, -1, 0, -1, 1}[[ Mod[ d, 6] + 1]], {d, Divisors @ n}]]; (* _Michael Somos_, Sep 19 2013 *)",
				"a[ n_] := If[ n \u003c 1, Boole[n == 0], -4 Sum[ n/d {6, 1, -3, -2, -3, 1}[[ Mod[ d, 6] + 1]], {d, Divisors @ n}]]; (* _Michael Somos_, Sep 19 2013 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( (eta(x + A) * eta(x^3 + A))^4 / (eta(x^2 + A) * eta(x^6 + A))^2, n))};",
				"(PARI) {a(n) = if( n\u003c1, n==0, -4 * sumdiv( n, d, d * [0, 1, -1, 0, -1, 1][d%6 + 1]))}; /* _Michael Somos_, Sep 19 2013 */",
				"(Sage) A = ModularForms( Gamma0(6), 2, prec=55) . basis();  A[0] - 4*A[1] + 4*A[2]; # _Michael Somos_, Sep 19 2013",
				"(MAGMA) A := Basis( ModularForms( Gamma0(6), 2), 55); A[1] - 4*A[2] + 4*A[3]; /* _Michael Somos_, Nov 11 2015 */"
			],
			"xref": [
				"Cf. A034896, A111932, A125514, A131947."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Jul 30 2007",
			"references": 4,
			"revision": 31,
			"time": "2020-04-12T09:35:22-04:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}