{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A085968",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 85968,
			"data": "0,0,4,0,6,1,4,0,5,3,6,6,5,1,7,8,3,0,5,6,0,5,2,3,4,3,9,1,4,2,6,8,3,0,8,0,5,2,2,9,7,7,1,4,4,5,1,2,0,7,1,7,4,1,0,0,1,0,3,2,6,8,8,6,8,1,7,2,8,6,3,0,4,0,7,0,7,8,8,0,4,4,0,6,0,9,2,2,8,2,8,0,5,3,0,4,3,1,3,4,4,2,6,5,6",
			"name": "Decimal expansion of the prime zeta function at 8.",
			"comment": [
				"Mathar's Table 1 (cited below) lists expansions of the prime zeta function at integers s in 10..39. - _Jason Kimberley_, Jan 07 2017"
			],
			"reference": [
				"Henri Cohen, Number Theory, Volume II: Analytic and Modern Tools, GTM Vol. 240, Springer, 2007; see pp. 208-209.",
				"J. W. L. Glaisher, On the Sums of Inverse Powers of the Prime Numbers, Quart. J. Math. 25, 347-362, 1891."
			],
			"link": [
				"Jason Kimberley, \u003ca href=\"/A085968/b085968.txt\"\u003eTable of n, a(n) for n = 0..1971\u003c/a\u003e",
				"Henri Cohen, \u003ca href=\"http://www.math.u-bordeaux.fr/~cohen/hardylw.dvi\"\u003eHigh Precision Computation of Hardy-Littlewood Constants\u003c/a\u003e, Preprint, 1998.",
				"Henri Cohen, \u003ca href=\"/A221712/a221712.pdf\"\u003eHigh-precision computation of Hardy-Littlewood constants\u003c/a\u003e. [pdf copy, with permission]",
				"X. Gourdon and P. Sebah, \u003ca href=\"http://numbers.computation.free.fr/Constants/Miscellaneous/constantsNumTheory.html\"\u003eSome Constants from Number theory\u003c/a\u003e",
				"R. J. Mathar, \u003ca href=\"http://arxiv.org/abs/0803.0900\"\u003eSeries of reciprocal powers of k-almost primes\u003c/a\u003e, arXiv:0803.0900 [math.NT], 2008-2009. Table 1.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PrimeZetaFunction.html\"\u003ePrime Zeta Function\u003c/a\u003e"
			],
			"formula": [
				"P(8) = Sum_{p prime} 1/p^8 = Sum_{n\u003e=1} mobius(n)*log(zeta(8*n))/n.",
				"Equals Sum_{k\u003e=1} 1/A179645(k). - _Amiram Eldar_, Jul 27 2020"
			],
			"example": [
				"0.0040614053665178305605..."
			],
			"maple": [
				"A085968:= proc(i) print(evalf(add(1/ithprime(k)^8,k=1..i),100)); end:",
				"A085968(100000); # _Paolo P. Lava_, May 29 2012"
			],
			"mathematica": [
				"s[n_] := s[n] = Sum[ MoebiusMu[k]*Log[Zeta[8*k]]/k, {k, 1, n}] // RealDigits[#, 10, 104]\u0026 // First // Prepend[#, 0]\u0026; s[100]; s[n = 200]; While[s[n] != s[n - 100], n = n + 100]; s[n] (* _Jean-François Alcover_, Feb 14 2013 *)",
				"RealDigits[ PrimeZetaP[ 8], 10, 111][[1]] (* _Robert G. Wilson v_, Sep 03 2014 *)"
			],
			"program": [
				"(MAGMA) R := RealField(106);",
				"PrimeZeta := func\u003ck,N | \u0026+[R|MoebiusMu(n)/n*Log(ZetaFunction(R,k*n)): n in[1..N]]\u003e;",
				"[0,0] cat Reverse(IntegerToSequence(Floor(PrimeZeta(8,43)*10^105)));",
				"// _Jason Kimberley_, Dec 30 2016",
				"(PARI) sumeulerrat(1/p, 8) \\\\ _Hugo Pfoertner_, Feb 03 2020"
			],
			"xref": [
				"Decimal expansion of the prime zeta function: A085548 (at 2), A085541 (at 3), A085964 (at 4) to A085967 (at 7), this sequence (at 8), A085969 (at 9).",
				"Cf. A013666, A179645."
			],
			"keyword": "cons,easy,nonn",
			"offset": "0,3",
			"author": "Antonio G. Astudillo (afg_astudillo(AT)lycos.com), Jul 06 2003",
			"references": 14,
			"revision": 40,
			"time": "2021-04-10T15:15:53-04:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}