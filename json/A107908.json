{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A107908",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 107908,
			"data": "1,16,110,490,1666,4704,11592,25740,52635,100672,182182,314678,522340,837760,1303968,1976760,2927349,4245360,6042190,8454754,11649638,15827680,21229000,28138500,36891855,47882016,61566246,78473710",
			"name": "a(n) = (n+1)(n+2)^2*(n+3)^2*(n+4)(3n+5)/720.",
			"comment": [
				"Kekulé numbers for certain benzenoids."
			],
			"link": [
				"Chai Wah Wu, \u003ca href=\"/A107908/b107908.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"P. Aluffi, \u003ca href=\"https://arxiv.org/abs/1408.1702\"\u003eDegrees of projections of rank loci\u003c/a\u003e, arXiv:1408.1702 [math.AG], 2014. [\"After compiling the results of many explicit computations, we noticed that many of the numbers d_{n,r,S} appear in the existing literature in contexts far removed from the enumerative geometry of rank conditions; we owe this surprising (to us) observation to perusal of [Slo14].\"]",
				"S. J. Cyvin and I. Gutman, \u003ca href=\"https://doi.org/10.1007/978-3-662-00892-8\"\u003eKekulé structures in benzenoid hydrocarbons\u003c/a\u003e, Lecture Notes in Chemistry, No. 46, Springer, New York, 1988 (see p. 167).",
				"\u003ca href=\"/index/Rec#order_08\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (8,-28,56,-70,56,-28,8,-1)."
			],
			"formula": [
				"a(0)=1, a(1)=16, a(2)=110, a(3)=490, a(4)=1666, a(5)=4704, a(6)=11592, a(7)=25740, a(n) = 8*a(n-1) - 28*a(n-2) + 56*a(n-3) - 70*a(n-4) + 56*a(n-5) - 28*a(n-6) + 8*a(n-7) - a(n-8). - _Harvey P. Dale_, Aug 07 2013",
				"G.f.: (1 + 8*x + 10*x^2 + 2*x^3) / (1 - x)^8. - _Colin Barker_, Apr 22 2020"
			],
			"maple": [
				"a:=n-\u003e(1/720)*(n+1)*(n+2)^2*(n+3)^2*(n+4)*(3*n+5): seq(a(n),n=0..33);"
			],
			"mathematica": [
				"Table[((n+1)(n+2)^2 (n+3)^2 (n+4)(3n+5))/720,{n,0,30}] (* or *) LinearRecurrence[ {8,-28,56,-70,56,-28,8,-1},{1,16,110,490,1666,4704,11592,25740},30] (* _Harvey P. Dale_, Aug 07 2013 *)"
			],
			"program": [
				"(Python)",
				"from itertools import islice",
				"def A107908_generator():",
				"    m = [21, -13, 3]+[1]*5",
				"    yield m[-1]",
				"    while True:",
				"        for i in range(7):",
				"            m[i+1]+= m[i]",
				"        yield m[-1]",
				"list(islice(A107908_generator(),0,50,1)) # _Chai Wah Wu_, Nov 14 2014",
				"(PARI) Vec((1 + 8*x + 10*x^2 + 2*x^3) / (1 - x)^8 + O(x^30)) \\\\ _Colin Barker_, Apr 22 2020"
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Emeric Deutsch_, Jun 12 2005",
			"references": 1,
			"revision": 24,
			"time": "2021-11-13T04:43:05-05:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}