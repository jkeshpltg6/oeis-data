{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A333625",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 333625,
			"data": "1,8,8,27,27,216,512,216,512,648,648,686,12096,46656,262144,46656,262144,12096,686,192000,139968,192000,139968,1866240,179712,74088,91125,74088,91125,179712,1866240,343000,1000000,5832000,4251528,5832000,80621568,13824000,1073741824",
			"name": "Read terms e = T(n,k) in A333624 as Product(prime(k)^e) for n in A334556.",
			"comment": [
				"Row a(n) of A067255 = row A334556(n) of A333624.",
				"An XOR-triangle t(n) is an inverted 0-1 triangle formed by choosing a top row the binary rendition of n and having each entry in subsequent rows be the XOR of the two values above it, i.e., A038554(n) applied recursively until we reach a single bit.",
				"Let T(n,k) address the terms in the k-th position of row n in A333624.",
				"This sequence encodes T(n,k) via A067255 to succinctly express the number of zero-triangles in A334556(n). To decode a(n) =\u003e A333624(A334556(n)), we use A067255(a(n))."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A333625/b333625.txt\"\u003eTable of n, a(n) for n = 1..9999\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"example": [
				"Relationship of this sequence to A334556 and A333624:",
				"       n A334556(n) a(n)  Row n of A333624",
				"       -----------------------------------",
				"       1     1        1   0",
				"       2    11        8   3",
				"       3    13        8   3",
				"       4    39       27   0, 3",
				"       5    57       27   0, 3",
				"       6    83      216   3, 3",
				"       7    91      512   9",
				"       8   101      216   3, 3",
				"       9   109      512   9",
				"      10   151      648   3, 4",
				"      11   233      648   3, 4",
				"      12   543      686   1, 0, 0, 3",
				"      13   599    12096   6, 3, 0, 1",
				"      14   659    46656   6, 6",
				"      15   731   262144   18",
				"      16   805    46656   6, 6",
				"      ...",
				"Let b(n) = n written in binary and let L(n) = ceiling(log_2(n)) = A070939(n). Let =\u003e be a single iteration of XOR across pairs of bits in b(n). Let t(n) be the XOR triangle initiated by b(n).",
				"a(1) = 0, since b(1) = 1 and row 1 of A333624 is {0}. Since the XOR triangle t(1) that results from a single 1-bit merely consists of that bit and since there are no zeros in the triangle t(1), we write the single term zero in row n of A333624. thus a(n) = prime(1)^0 = 2^0 = 1.",
				"a(2) = 8 because row A334556(2) of A333624 (i.e., the 11th row) has {3}. b(11) = 1011 =\u003e 110 =\u003e 01 =\u003e 1 (a rotationally symmetrical t(11)). We have 3 isolated zeros thus row 11 of A333624 = {3}, therefore a(2) = prime(1)^3 = 2^3 = 8.",
				"a(4) = 27 because row A334556(4) of A333624 (i.e., the 39th row) has {0, 3}. b(39) = 100111 =\u003e 10100 =\u003e 1110 =\u003e 001 =\u003e 01 =\u003e 1 (a rotationally symmetrical t(39)). We have 3 isolated triangles of zeros with edge length 2, thus row 39 of A333624 = {0, 3}, therefore a(4) = prime(1)^0 * prime(2)^3 = 2^0 * 3^3 = 27."
			],
			"mathematica": [
				"With[{s = Rest[Import[\"https://oeis.org/A334556/b334556.txt\", \"Data\"][[All, -1]] ]}, Map[With[{w = NestWhileList[Map[BitXor @@ # \u0026, Partition[#, 2, 1]] \u0026, IntegerDigits[#, 2], Length@ # \u003e 1 \u0026]}, If[Length@ # == 0, 1, Times @@ Flatten@ MapIndexed[Prime[#2]^#1 \u0026, #] \u0026@ ReplacePart[ConstantArray[0, Max@ #[[All, 1]]], Map[#1 -\u003e #2 \u0026 @@ # \u0026, #]]] \u0026@ Tally@ Flatten@ Array[If[# == 1, Map[If[First@ # == 1, Nothing, Length@ #] \u0026, Split@ w[[#]] ], Map[If[First@ # == -1, Length@ #, Nothing] \u0026, Split[w[[#]] - Most@ w[[# - 1]] ] ]] \u0026, Length@ w]] /. -Infinity -\u003e 0 \u0026, s[[1 ;; 36]]]]"
			],
			"xref": [
				"Cf. A038554, A067255, A070939, A333624, A334591, A334556."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Michael De Vlieger_, May 13 2020",
			"references": 2,
			"revision": 11,
			"time": "2020-07-19T11:53:11-04:00",
			"created": "2020-05-14T21:22:00-04:00"
		}
	]
}