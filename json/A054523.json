{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A054523",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 54523,
			"data": "1,1,1,2,0,1,2,1,0,1,4,0,0,0,1,2,2,1,0,0,1,6,0,0,0,0,0,1,4,2,0,1,0,0,0,1,6,0,2,0,0,0,0,0,1,4,4,0,0,1,0,0,0,0,1,10,0,0,0,0,0,0,0,0,0,1,4,2,2,2,0,1,0,0,0,0,0,1,12,0,0,0,0,0,0,0,0,0,0,0,1,6,6",
			"name": "Triangle read by rows: T(n,k) = phi(n/k) if k divides n, T(n,k)=0 otherwise (n \u003e= 1, 1 \u003c= k \u003c= n).",
			"comment": [
				"From _Gary W. Adamson_, Jan 08 2007: (Start)",
				"Let H be this lower triangular matrix. Then:",
				"H * A051731 = A126988,",
				"H * [1, 2, 3, ...] = 1, 3, 5, 8, 9, 15, ... = A018804,",
				"H * sigma(n) = A038040 = d(n) * n = 1, 4, 6, 12, 10, ... where sigma(n) = A000203,",
				"H * d(n) (A000005) = sigma(n) = A000203,",
				"Row sums are A000027 (corrected by _Werner Schulte_, Sep 06 2020, see comment of Gary W. Adamson, Aug 03 2008),",
				"H^2 * d(n) = d(n)*n, H^2 = A127192,",
				"H * mu(n) (A008683) = A007431(n) (corrected by _Werner Schulte_, Sep 06 2020),",
				"H^2 row sums = A018804. (End)",
				"The Möbius inversion principle of Richard Dedekind and Joseph Liouville (1857), cf. \"Concrete Mathematics\", p. 136, is equivalent to the statement that row sums are the row index n. - _Gary W. Adamson_, Aug 03 2008",
				"The multivariable row polynomials give n times the cycle index for the cyclic group C_n, called Z(C_n) (see the MathWorld link with the Harary reference): n*Z(C_n) = Sum_{k=1..n} T(n,k)*(y_{n/k})^k, n \u003e= 1. E.g., 6*Z(C_6) = 2*(y_6)^1 + 2*(y_3)^2 + 1*(y_2)^3 + 1*(y_1)^6. - _Wolfdieter Lang_, May 22 2012",
				"See A102190 (no 0's, rows reversed). - _Wolfdieter Lang_, May 29 2012",
				"This is the number of permutations in the n-th cyclic group which are the product of k disjoint cycles. - _Robert A. Beeler_, Aug 09 2013",
				"From _Werner Schulte_, Sep 06 2020: (Start)",
				"Sum_{k=1..n} T(n,k) * A000010(k) = A029935(n) for n \u003e 0.",
				"Sum_{k=1..n} T(n,k) * k^2 = A069097(n) for n \u003e 0. (End)"
			],
			"reference": [
				"Ronald L. Graham, D. E. Knuth, Oren Patashnik, Concrete Mathematics, Addison-Wesley, 2nd ed., 1994, p. 136. [_Gary W. Adamson_, Aug 03 2008]"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A054523/b054523.txt\"\u003eRows n = 1..125 of triangle, flattened\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CycleIndex.html\"\u003eCycle Index\u003c/a\u003e."
			],
			"formula": [
				"Equals A054525 * A126988 as infinite lower triangular matrices. - _Gary W. Adamson_, Aug 03 2008"
			],
			"example": [
				"Triangle begins",
				"   1;",
				"   1, 1;",
				"   2, 0, 1;",
				"   2, 1, 0, 1;",
				"   4, 0, 0, 0, 1;",
				"   2, 2, 1, 0, 0, 1;",
				"   6, 0, 0, 0, 0, 0, 1;",
				"   4, 2, 0, 1, 0, 0, 0, 1;",
				"   6, 0, 2, 0, 0, 0, 0, 0, 1;",
				"   4, 4, 0, 0, 1, 0, 0, 0, 0, 1;",
				"  10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1;",
				"   4, 2, 2, 2, 0, 1, 0, 0, 0, 0, 0, 1;"
			],
			"maple": [
				"A054523 := proc(n,k) if n mod k = 0 then numtheory[phi](n/k) ; else 0; end if; end proc: # _R. J. Mathar_, Apr 11 2011"
			],
			"mathematica": [
				"T[n_, k_] := If[Divisible[n, k], EulerPhi[n/k], 0]; T[1, 1] = 1; Table[T[n, k], {n, 1, 10}, {k, 1, n}]//Flatten (* _G. C. Greubel_, Dec 15 2017 *)"
			],
			"program": [
				"(Haskell)",
				"a054523 n k = a054523_tabl !! (n-1) !! (k-1)",
				"a054523_row n = a054523_tabl !! (n-1)",
				"a054523_tabl = map (map (\\x -\u003e if x == 0 then 0 else a000010 x)) a126988_tabl",
				"-- _Reinhard Zumkeller_, Jan 20 2014",
				"(PARI) for(n=1, 10, for(k=1, n, print1(if(!(n % k), eulerphi(n/k), 0), \", \"))) \\\\ _G. C. Greubel_, Dec 15 2017"
			],
			"xref": [
				"Cf. A054521, A054525, A102190, A000027, A007431, A029935, A069097."
			],
			"keyword": "nonn,tabl",
			"offset": "1,4",
			"author": "_N. J. A. Sloane_, Apr 09 2000",
			"references": 37,
			"revision": 59,
			"time": "2020-10-05T12:24:42-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}