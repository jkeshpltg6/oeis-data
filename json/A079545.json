{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A079545",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 79545,
			"data": "2,3,5,11,17,19,37,41,53,59,73,83,101,107,131,137,149,163,179,181,197,227,233,251,257,293,307,347,389,401,443,467,491,521,523,563,577,587,593,613,641,677,739,773,809,811,821,883",
			"name": "Primes of the form x^2 + y^2 + 1 with x,y \u003e= 0.",
			"comment": [
				"Bredihin proves that this sequence is infinite. Motohashi improves the upper and lower bounds. - _Charles R Greathouse IV_, Sep 16 2011",
				"Sun \u0026 Pan prove that there are arbitrarily long arithmetic progressions in this sequence. - _Charles R Greathouse IV_, Mar 03 2018",
				"For this sequence in short intervals, see Wu and Matomäki; for its Goldbach problem, see Teräväinen. - _Charles R Greathouse IV_, Oct 10 2018"
			],
			"link": [
				"Charles R Greathouse IV, \u003ca href=\"/A079545/b079545.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"B. M. Bredihin, \u003ca href=\"http://mi.mathnet.ru/eng/izv3124\"\u003eBinary additive problems of indeterminate type II. Analogue of the problem of Hardy and Littlewood\u003c/a\u003e (in Russian). Izvestiya Akademii Nauk SSSR Seriya Matematicheskaya 27 (1963), pp. 577-612.",
				"M. N. Huxley and H. Iwaniec, \u003ca href=\"https://doi.org/10.1112/S0025579300006069\"\u003eBombieri's theorem in short intervals\u003c/a\u003e, Mathematika 22 (1975), pp. 188-194.",
				"Henryk Iwaniec, \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/aa/aa21/aa21118.pdf\"\u003ePrimes of the type φ(x, y) + A where φ is a quadratic form\u003c/a\u003e, Acta Arithmetica 21 (1972), pp. 203-234.",
				"Kaisa Matomäki, \u003ca href=\"http://users.utu.fi/ksmato/papers/Primesm2n2p1.pdf\"\u003ePrime numbers of the form p = m^2 + n^2 + 1 in short intervals\u003c/a\u003e, Acta Arithmetica 128 (2007), pp. 193-200.",
				"Y. Motohashi, \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/aa/aa16/aa1642.pdf\"\u003eOn the distribution of prime numbers which are of the form x^2 + y^2 + 1\u003c/a\u003e. Acta Arithmetica 16 (1969), pp. 351-364.",
				"Y. Motohashi, \u003ca href=\"https://doi.org/10.1007/BF01896011\"\u003eOn the distribution of prime numbers which are of the form x^2 + y^2 + 1. II\"\u003c/a\u003e, Acta Mathematica Academiae Scientiarum Hungaricae 22 (1971), pp. 207-210.",
				"Yu-Chen Sun and Hao Pan, \u003ca href=\"https://arxiv.org/abs/1708.08629\"\u003eThe Green-Tao theorem for primes of the form x^2 + y^2 + 1\u003c/a\u003e, arXiv:1708.08629 [math.NT], 2017.",
				"Joni Teräväinen, \u003ca href=\"https://arxiv.org/abs/1611.08585\"\u003eThe Goldbach problem for primes that are sums of two squares plus one\u003c/a\u003e, Mathematika 64 (2018), pp. 20-70. arXiv:1611.08585 [math.NT], 2016-2017.",
				"J. Wu, \u003ca href=\"https://doi.org/10.1090/S0002-9939-98-04414-1\"\u003ePrimes of the form p = 1 + m^2 + n^2 in short intervals\u003c/a\u003e, Proceedings of the American Mathematical Society 126 (1998), pp. 1-8."
			],
			"formula": [
				"Iwaniec proves that n (log n)^(3/2) \u003c\u003c a(n) \u003c\u003c n (log n)^(3/2). - _Charles R Greathouse IV_, Mar 06 2018"
			],
			"example": [
				"17 = 0^2 + 4^2 + 1 is prime so in this sequence."
			],
			"mathematica": [
				"Select[Select[Range[1000], SquaresR[2, #] != 0\u0026]+1, PrimeQ] (* _Jean-François Alcover_, Aug 31 2018 *)"
			],
			"program": [
				"(PARI) list(lim)={",
				"    my(A,t,v=List([2]));",
				"    forstep(a=2,sqrt(lim-1),2,",
				"        A=a^2+1;",
				"        forstep(b=0,min(a,sqrt(lim-A)),2,",
				"            if(isprime(t=A+b^2),listput(v,t))",
				"        )",
				"    );",
				"    forstep(a=1,sqrt(lim-2),2,",
				"        A=a^2+1;",
				"        forstep(b=1,min(a,sqrt(lim-A)),2,",
				"            if(isprime(t=A+b^2),listput(v,t))",
				"        )",
				"    );",
				"    vecsort(Vec(v),,8)",
				"}; \\\\ _Charles R Greathouse IV_, Sep 16 2011",
				"(PARI) is(n)=for(x=sqrtint(n\\2),sqrtint(n-1), if(issquare(n-x^2-1), return(isprime(n)))); 0 \\\\ _Charles R Greathouse IV_, Jun 12 2015",
				"(PARI) B=bnfinit('x^2+1);",
				"is(n)=!!#bnfisintnorm(B,n-1) \u0026\u0026 isprime(n) \\\\ _Charles R Greathouse IV_, Jun 13 2015"
			],
			"xref": [
				"Primes in A166687.",
				"Cf. A079544, A079739, A079740."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_, Jan 23 2003",
			"references": 10,
			"revision": 39,
			"time": "2020-06-05T15:06:37-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}