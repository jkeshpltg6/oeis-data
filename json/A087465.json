{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A087465",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 87465,
			"data": "1,2,3,4,5,7,6,8,10,12,9,11,14,16,19,13,15,18,21,24,27,17,20,23,26,30,33,37,22,25,29,32,36,40,44,48,28,31,35,39,43,47,52,56,61,34,38,42,46,51,55,60,65,70,75,41,45,50,54,59,64,69,74,80,85,91,49,53,58,63,68,73",
			"name": "Rank array R of 3/2 read by antidiagonals; this array is the dispersion of the complement of the sequence given by r(n) = r(n-1) + 1 + floor(3n/2) for n\u003e=1, with r(0) = 1; that is, A077043(n+1).",
			"comment": [
				"The sequence is a permutation of the positive integers and the array is a transposable dispersion.",
				"Let T(n,k) be the rectangular version of the array at A036561, with northwest corner as shown here:",
				"   1     2     4     8    16    32",
				"   3     6    12    24    48    96",
				"   9    18    36    72   144   288",
				"  27    54   108   216   432   864",
				"Then R(n,k) is the rank of T(n,k) when all the numbers in {T(n,k}} are jointly ranked. - _Clark Kimberling_, Jan 25 2018"
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A087465/b087465.txt\"\u003eAntidiagonals n = 1..60, flattened\u003c/a\u003e",
				"Clark Kimberling and John E. Brown, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL7/Kimberling/kimber67.html\"\u003ePartial Complements and Transposable Dispersions\u003c/a\u003e, J. Integer Seqs., Vol. 7, 2004."
			],
			"formula": [
				"R(i,j) = R(i,0) + R(0,j) + i*j - 1, for i\u003e=1, j\u003e=1."
			],
			"example": [
				"Northwest corner of R:",
				"   1    2    4    6    9   13   17   22",
				"   3    5    8   11   15   20   25   31",
				"   7   10   14   18   23   29   35   42",
				"  12   16   21   26   32   39   46   54",
				"  19   24   30   36   43   51   59   68",
				"  27   33   40   47   55   64   73   83",
				"  37   44   52   60   69   79   89  100",
				"Let t=3/2; then R(i,j) = rank of (j,i) when all nonnegative integer pairs (a,b) are ranked by the relation \u003c\u003c defined as follows: (a,b) \u003c\u003c (c,d) if a + b*t \u003c c + d*t, and also (a,b) \u003c\u003c (c,d) if a + b*t = c + d*t and b \u003c d.  Thus R(2,1) = 10 is the rank of (1,2) in the list (0,0) \u003c\u003c (1,0) \u003c\u003c (0,1) \u003c\u003c (2,0) \u003c\u003c (1,1) \u003c\u003c (3,0) \u003c\u003c (0,2) \u003c\u003c (2,1) \u003c\u003c (4,0) \u003c\u003c (1,2)."
			],
			"mathematica": [
				"r = 20; r1 = 12;(*r=# rows of T,r1=# rows to show*);",
				"c = 20; c1 = 12;(*c=# cols of T,c1=# cols to show*);",
				"s[0] = 1; s[n_] := s[n] = s[n - 1] + 1 + Floor[3 n/2]; u = Table[s[n], {n, 0, 100}]",
				"v = Complement[Range[Max[u]], u]; f[n_] := v[[n]]; Table[f[n], {n, 1, 30}]",
				"mex[list_] := NestWhile[#1 + 1 \u0026, 1, Union[list][[#1]] \u003c= #1 \u0026, 1,   Length[Union[list]]]; rows = {NestList[f, 1, c]}; Do[rows = Append[rows, NestList[f, mex[Flatten[rows]], r]], {r}]; w[i_, j_] := rows[[i, j]];",
				"TableForm[Table[w[i, j], {i, 1, 10}, {j, 1, 10}]]   (* A087465 array *)",
				"Flatten[Table[w[k, n - k + 1], {n, 1, c1}, {k, 1, n}]] (* A087465 sequence *)",
				"TableForm[Table[w[i, 1] + w[1, j] + (i - 1)*(j - 1) - 1, {i, 1, 10}, {j, 1, 10}]] (* A087465 array, by formula *)"
			],
			"xref": [
				"Cf. A087466, A087468, A087483, A007780 (row 1), A077043 (column 1)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,2",
			"author": "_Clark Kimberling_, Sep 09 2003",
			"ext": [
				"Updated by _Clark Kimberling_, Sep 23 2014"
			],
			"references": 7,
			"revision": 26,
			"time": "2018-01-26T05:20:26-05:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}