{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A119881",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 119881,
			"data": "1,3,8,18,32,48,128,528,512,-6912,2048,357888,8192,-22351872,32768,1903822848,131072,-209865080832,524288,29088886161408,2097152,-4951498048929792,8388608,1015423886523629568,33554432,-246921480190140874752,134217728,70251601603944228323328",
			"name": "Expansion of e.g.f. exp(3*x)*sech(x).",
			"comment": [
				"Transform of 3^n under the matrix A119879.",
				"Also the Swiss-Knife polynomials A153641 evaluated at x=3. - _Peter Luschny_, Nov 23 2012"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A119881/b119881.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e"
			],
			"formula": [
				"a(n) = sum{k=0..n, A119879(n,k)3^k}.",
				"a(n) = Sum(binomial(n,k)*B(k,1)*2^(n+k)/(n-k+1), k=0..n). Here B(k,1) are the Bernoulli number A027641(k)/A027642(k) with the exception B(1,1)=1/2. - _Peter Luschny_, Dec 14 2008",
				"a(n) = 2^n |E(n,-1)| where E(n,x) are the Euler polynomials. - _Peter Luschny_, Jan 25 2009",
				"The odd part of a(n) = numerator(Euler(n,2)/2) = 1, 3, 1, 9, 1, 3, 1, 33, 1, -27, 1, 699,... (compare A143074). - _Peter Luschny_, Nov 23 2012",
				"G.f.: 1/Q(0), where Q(k)= 1 - 2*x - x*(k+1)/(1+x*(k+1)/Q(k+1)); (continued fraction). - _Sergei N. Gladkovskii_, Apr 19 2013",
				"G.f.: 1/Q(0), where Q(k)= 1 - 4*x + x*(k+1)/(1-x*(k+1)/Q(k+1)); (continued fraction). - _Sergei N. Gladkovskii_, Apr 20 2013",
				"a(n) = 2^(n+1)*(zeta[-n]*(2^(n+1)-1)+1). - _Peter Luschny_, Jul 16 2013",
				"E.g.f.: 2/Q(0), where Q(k) = 1 + 2^k/( 1 - 2*x/( 2*x  -  2^k*(k+1)/Q(k+1) )); (continued fraction). - _Sergei N. Gladkovskii_, Dec 16 2013",
				"a(n) = 2^(n+1)*(1+(-1)^n*(2^(n+1)-1)*Bernoulli(n+1)/(n+1)). - _Vladimir Reshetnikov_, Oct 21 2015"
			],
			"maple": [
				"a := proc(n) add(binomial(n,k)*bernoulli(k,1)*2^(n+k)/(n-k+1),k=0..n) end: # _Peter Luschny_, Dec 14 2008",
				"a := n -\u003e 2^n*abs(euler(n,-1)):  # _Peter Luschny_, Jan 25 2009",
				"P := proc(n,x) option remember; if n = 0 then 1 else",
				"   (n*x+2*(1-x))*P(n-1,x)+x*(1-x)*diff(P(n-1,x),x);",
				"   expand(%) fi end:",
				"A119881 := n -\u003e subs(x=-1,P(n,x)):",
				"seq(A119881(n), n=0..27);  # _Peter Luschny_, Mar 07 2014"
			],
			"mathematica": [
				"Table[2^(n+1) (Zeta[-n] (2^(n+1)-1)+1), {n,0,27}] (* _Peter Luschny_, Jul 16 2013 *)",
				"Range[0, 30]! CoefficientList[Series[Exp[3 x] Sech[x], {x, 0, 30}], x] (* _Vincenzo Librandi_, Mar 08 2014 *)"
			],
			"program": [
				"(Sage)",
				"def skp(n, x):",
				"    A = lambda k: 0 if (k+1)%4 == 0 else (-1)^((k+1)//4)*2^(-(k//2))",
				"    return add(A(k)*add((-1)^v*binomial(k,v)*(v+x+1)^n for v in (0..k)) for k in (0..n))",
				"A119881 = lambda n: skp(n,3)",
				"[A119881(n) for n in (0..27)]  # _Peter Luschny_, Nov 23 2012",
				"(PARI) x='x+O('x^66); Vec(serlaplace(exp(3*x)/cosh(x))) \\\\ _Joerg Arndt_, Apr 20 2013"
			],
			"xref": [
				"Cf. A119880."
			],
			"keyword": "easy,sign",
			"offset": "0,2",
			"author": "_Paul Barry_, May 26 2006",
			"references": 6,
			"revision": 39,
			"time": "2015-10-23T09:21:30-04:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}