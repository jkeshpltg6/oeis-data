{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A157149",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 157149,
			"data": "1,1,1,1,11,1,1,57,57,1,1,247,930,247,1,1,1013,10006,10006,1013,1,1,4083,89139,225230,89139,4083,1,1,16369,719691,3771323,3771323,719691,16369,1,1,65519,5495836,53239541,108865438,53239541,5495836,65519,1",
			"name": "Triangle T(n, k, m) = (m*(n-k) + 1)*T(n-1, k-1, m) + (m*k + 1)*T(n-1, k, m) + m*k*(n-k)*T(n-2, k-1, m) with T(n, 0, m) = T(n, n, m) = 1 and m = 3, read by rows.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A157149/b157149.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k, m) = (m*(n-k) + 1)*T(n-1, k-1, m) + (m*k + 1)*T(n-1, k, m) + m*k*(n-k)*T(n-2, k-1, m) with T(n, 0, m) = T(n, n, m) = 1 and m = 3.",
				"T(n, n-k, 3) = T(n, k, 3).",
				"T(n, 1, 3) = A289255(n). - _G. C. Greubel_, Jan 09 2022"
			],
			"example": [
				"Triangle begins as:",
				"  1;",
				"  1,     1;",
				"  1,    11,       1;",
				"  1,    57,      57,        1;",
				"  1,   247,     930,      247,         1;",
				"  1,  1013,   10006,    10006,      1013,        1;",
				"  1,  4083,   89139,   225230,     89139,     4083,       1;",
				"  1, 16369,  719691,  3771323,   3771323,   719691,   16369,     1;",
				"  1, 65519, 5495836, 53239541, 108865438, 53239541, 5495836, 65519, 1;"
			],
			"maple": [
				"A157149 := proc(n,k)",
				"    option remember;",
				"    if k \u003c 0 or k\u003e n then 0;",
				"    elif k = 0 or k = n then 1;",
				"    else (3*(n-k)+1)*procname(n-1,k-1) + (3*k+1)*procname(n-1,k) + 3*k*(n-k)*procname(n-2,k-1);",
				"    end if;",
				"end proc:",
				"seq(seq(A157149(n,k),k=0..n),n=0..10) ; # _R. J. Mathar_, Feb 06 2015"
			],
			"mathematica": [
				"T[n_, k_, m_]:= T[n, k, m]= If[k==0 || k==n, 1, (m*(n-k)+1)*T[n-1,k-1,m] + (m*k+1)*T[n-1,k,m] + m*k*(n-k)*T[n-2,k-1,m]];",
				"Table[T[n,k,3], {n,0,10}, {k,0,n}]//Flatten (* modified by _G. C. Greubel_, Jan 09 2022 *)"
			],
			"program": [
				"(Sage)",
				"@CachedFunction",
				"def T(n,k,m):  # A157149",
				"    if (k==0 or k==n): return 1",
				"    else: return (m*(n-k) +1)*T(n-1,k-1,m) + (m*k+1)*T(n-1,k,m) + m*k*(n-k)*T(n-2,k-1,m)",
				"flatten([[T(n,k,3) for k in (0..n)] for n in (0..20)]) # _G. C. Greubel_, Jan 09 2022"
			],
			"xref": [
				"Cf. A007318 (m=0), A157147 (m=1), A157148 (m=2), this sequence (m=3), A157150 (m=4), A157151 (m=5).",
				"Cf. A157152, A157153, A157154, A157155, A157156, A157207, A157208, A157209, A157210, A157211, A157212, A157268, A157272, A157273, A157274, A157275.",
				"Cf. A289255."
			],
			"keyword": "nonn,tabl,easy,changed",
			"offset": "0,5",
			"author": "_Roger L. Bagula_, Feb 24 2009",
			"ext": [
				"Edited by _G. C. Greubel_, Jan 09 2022"
			],
			"references": 1,
			"revision": 12,
			"time": "2022-01-10T03:07:00-05:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}