{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A265885",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 265885,
			"data": "2,3,5,7,11,13,25,23,23,29,31,55,59,59,63,63,63,61,111,111,107,111,123,127,103,101,103,107,111,113,127,223,223,223,221,223,223,251,255,255,247,245,255,211,215,215,211,223,239,237,237,239,251,251,457,455",
			"name": "a(n) = n IMPL prime(n), where IMPL is the bitwise logical implication.",
			"comment": [
				"a(n) = A265705(A000040(n),n)."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A265885/b265885.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Implies.html\"\u003eImplies\u003c/a\u003e"
			],
			"example": [
				".   prime(25)=97 | 1100001",
				".             25 |   11001",
				".   -------------+--------",
				".     25 IMPL 97 | 1100111 -\u003e a(25) = 103 ."
			],
			"maple": [
				"a:= n-\u003e Bits[Implies](n, ithprime(n)):",
				"seq(a(n), n=1..56);  # _Alois P. Heinz_, Sep 24 2021"
			],
			"mathematica": [
				"IMPL[n_, k_] := If[n == 0, 0, BitOr[2^Length[IntegerDigits[k, 2]]-1-n, k]];",
				"a[n_] := n ~IMPL~ Prime[n];",
				"Table[a[n], {n, 1, 100}] (* _Jean-François Alcover_, Sep 25 2021, after _David A. Corneth_'s code in A265705 *)"
			],
			"program": [
				"(Haskell)",
				"a265885 n = n `bimpl` a000040 n where",
				"   bimpl 0 0 = 0",
				"   bimpl p q = 2 * bimpl p' q' + if u \u003c= v then 1 else 0",
				"               where (p', u) = divMod p 2; (q', v) = divMod q 2",
				"(Julia)",
				"using IntegerSequences",
				"[Bits(\"IMP\", n, p) for (n, p) in enumerate(Primes(1, 263))] |\u003e println  # _Peter Luschny_, Sep 25 2021"
			],
			"xref": [
				"Cf. A000040, A004676, A007088, A070883 (XOR), A265705."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Reinhard Zumkeller_, Dec 17 2015",
			"references": 3,
			"revision": 14,
			"time": "2021-09-25T07:23:19-04:00",
			"created": "2015-12-17T15:12:27-05:00"
		}
	]
}