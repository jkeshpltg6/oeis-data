{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A256553",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 256553,
			"data": "1,1,1,2,1,2,3,1,2,3,4,1,2,3,4,5,6,1,2,3,4,5,6,1,2,3,4,5,6,7,10,12,1,2,3,4,5,6,7,8,10,12,15,1,2,3,4,5,6,7,8,9,10,12,14,15,20,1,2,3,4,5,6,7,8,9,10,12,14,15,20,21,30",
			"name": "Triangle T(n,k) in which the n-th row contains the increasing list of distinct orders of degree-n permutations; n\u003e=0, 1\u003c=k\u003c=A009490(n).",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A256553/b256553.txt\"\u003eRows n = 0..60, flattened\u003c/a\u003e"
			],
			"formula": [
				"Sum_{k\u003e=0} T(n,k)*A256554(n,k) = A181844(n).",
				"T(n,k) = k for n\u003e0 and 1\u003c=k\u003c=n."
			],
			"example": [
				"Triangle T(n,k) begins:",
				"  1;",
				"  1;",
				"  1, 2;",
				"  1, 2, 3;",
				"  1, 2, 3, 4;",
				"  1, 2, 3, 4, 5, 6;",
				"  1, 2, 3, 4, 5, 6;",
				"  1, 2, 3, 4, 5, 6, 7, 10, 12;",
				"  1, 2, 3, 4, 5, 6, 7,  8, 10, 12, 15;",
				"  1, 2, 3, 4, 5, 6, 7,  8,  9, 10, 12, 14, 15, 20;",
				"  1, 2, 3, 4, 5, 6, 7,  8,  9, 10, 12, 14, 15, 20, 21, 30;"
			],
			"maple": [
				"b:= proc(n, i) option remember; `if`(n=0 or i=1, x,",
				"      b(n, i-1)+(p-\u003e add(coeff(p, x, t)*x^ilcm(t, i),",
				"      t=1..degree(p)))(add(b(n-i*j, i-1), j=1..n/i)))",
				"    end:",
				"T:= n-\u003e(p-\u003eseq((h-\u003e`if`(h=0, [][], i))(coeff(p, x, i))",
				"     , i=1..degree(p)))(b(n$2)):",
				"seq(T(n), n=0..12);"
			],
			"mathematica": [
				"b[n_, i_] := b[n, i] = If[n == 0 || i == 1, x,",
				"     b[n, i - 1] + Function[p, Sum[Coefficient[p, x, t]*x^LCM[t, i],",
				"     {t, 1, Exponent[p, x]}]][Sum[b[n - i*j, i - 1], {j, 1, n/i}]]];",
				"T[n_] := Function[p, Table[Function[h, If[h == 0, Nothing, i]][",
				"     Coefficient[p, x, i]], {i, 1, Exponent[p, x]}]][b[n, n]];",
				"Table[T[n], {n, 0, 12}] // Flatten (* _Jean-François Alcover_, Jul 15 2021, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Row sums give A060179.",
				"Row lengths give A009490.",
				"Last elements of rows give A000793.",
				"Main diagonal gives A000027.",
				"Cf. A181844, A256067, A256554."
			],
			"keyword": "nonn,look,tabf",
			"offset": "0,4",
			"author": "_Alois P. Heinz_, Apr 01 2015",
			"references": 4,
			"revision": 26,
			"time": "2021-07-15T19:35:15-04:00",
			"created": "2015-04-02T11:14:28-04:00"
		}
	]
}