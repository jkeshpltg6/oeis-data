{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A238606",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 238606,
			"data": "1,2,2,1,4,1,4,3,8,2,1,10,3,2,14,7,1,20,5,5,30,5,6,1,36,15,2,3,52,16,6,3,70,13,15,3,94,22,12,7,122,32,8,13,1,160,45,12,10,4,206,51,26,8,6,276,49,44,9,7,350,75,30,28,7,448,108,22,38,11,566",
			"name": "Array: t(n,k) = number of partitions p of n such that the principal antidiagonal of the Ferrers matrix of p has k 1's.",
			"comment": [
				"\"Ferrers matrix\" is defined (A237981) as follows:  an m X m matrix (x(i,j)) of 0's and 1's satisfying three properties: (1) x(1,m) = 1 or x(m,1) = 1; (2) x(i,j+1) \u003e= x(i,j) for j=1..m-1 and i = 1..m; and (3) x(i+1,j) \u003e= x(i,j) for i=1..m-1 and j=1..m.  Ferrers matrices arise from Ferrers graphs of partitions, as follows:  suppose that p is a partition of n, and let m = max{greatest part of p, number of parts of p}.  Write the Ferrers graph of p with 1s as nodes, and pad the graph with 0's to form an m X m square matrix, which is the Ferrers matrix of p.",
				"If \"antidiagonal\" is changed to \"diagonal\" in the definition of t(n,k), the resulting array is given by A115995.  For both arrays, the sum of terms in row n is A000041(n)."
			],
			"link": [
				"Clark Kimberling and Peter J. C. Moses, \u003ca href=\"http://faculty.evansville.edu/ck6/GalleryThree/Introduction3.html\"\u003eFerrers Matrices and Related Partitions of Integers\u003c/a\u003e"
			],
			"example": [
				"First 17 rows:",
				"1",
				"2",
				"2 .... 1",
				"4 .... 1",
				"4 .... 3",
				"8 .... 2 .... 1",
				"10 ... 3 .... 2",
				"14 ... 7 .... 1",
				"20 ... 5 .... 5",
				"30 ... 5 .... 6 ... 1",
				"36 ... 15 ... 2 ... 3",
				"52 ... 16 ... 6 ... 3",
				"70 ... 13 ... 15 .. 3",
				"94 ... 22 ... 12 .. 7",
				"122 ... 32 .. 8 ... 13 .. 1",
				"160 ... 45 .. 12 .. 10 .. 4",
				"Row 5 counts 4 antidiagonals that have exactly one 1 and 3 antidiagonals that have exactly two 1's.  The Ferrers matrix for each of the latter three cases are as shown below.",
				"For the partition 32:",
				"1 1 1",
				"1 1 0",
				"0 0 0  (antidiagonal, from row 1:  1,1,0)",
				"For the partition 311:",
				"1 1 1",
				"1 0 0",
				"1 0 0  (antidiagonal, from row 1:  1,0,1,)",
				"For the partition 221:",
				"1 1 0",
				"1 1 0",
				"1 0 0  (antidiagonal, from row 1:  0,1,1)"
			],
			"mathematica": [
				"z = 30; ferrersMatrix[list_] := PadRight[Map[Table[1, {#}] \u0026, #], {#, #} \u0026[Max[#, Length[#]]]] \u0026[list]; diagAntidiagDots[list_] := {Total[Diagonal[#]], Total[Diagonal[Reverse[#]]]} \u0026[ferrersMatrix[list]]; u[n_, k_] := Length[Select[   Map[diagAntidiagDots, IntegerPartitions[n]], #[[2]] == k \u0026]]; t[n_] := t[n] = Floor[(-1 + Sqrt[1 + 8 n])/2]; w = Table[u[n, k], {n, 1, z}, {k, 1, t[n]}]; y = Flatten[w] (* A238606 *) (* _Peter J. C. Moses_, Mar 01 2014 *)"
			],
			"xref": [
				"Cf. A115994, A000041."
			],
			"keyword": "nonn,tabf,easy",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Mar 01 2014",
			"references": 1,
			"revision": 13,
			"time": "2020-04-07T22:23:56-04:00",
			"created": "2014-03-03T11:36:58-05:00"
		}
	]
}