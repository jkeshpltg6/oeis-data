{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A292779",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 292779,
			"data": "1,-2,-11,-11,-92,151,-578,-578,-578,19105,-39944,-39944,-571385,1022938,5805907,5805907,-37240814,-37240814,-424661303,-424661303,3062123098,13522476301,-17858583308,-17858583308,-17858583308,829430026135,829430026135,829430026135",
			"name": "Interpret the values of the Moebius function mu(k) for k = n to 1 as a balanced ternary number.",
			"comment": [
				"Balanced ternary is much like regular ternary, but with the crucial difference of using the digit -1 instead of the digit 2. Then some powers of 3 are added, others are subtracted.",
				"Since the least significant digit is always 1, a(n) is never a multiple of 3.",
				"If mu(n) = 0, then a(n) is the same as a(n - 1).",
				"Run lengths are given by A076259. - _Andrey Zabolotskiy_, Oct 13 2017"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A292779/b292779.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Balanced_ternary\"\u003eBalanced ternary\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{k = 1 .. n} mu(k) 3^(k - 1)."
			],
			"example": [
				"mu(1) = 1, so a(1) = 1 * 3^0 = 1.",
				"mu(2) = -1, so a(2) = -1 * 3^1 + 1 * 3^0 = -3 + 1 = -2.",
				"mu(3) = -1, so a(3) = -1 * 3^2 + -1 * 3^1 + 1 * 3^0 = -9 - 3 + 1 = -11.",
				"mu(4) = 0, so a(4) = 0 * 3^3 + -1 * 3^2 + -1 * 3^1 + 1 * 3^0 = -9 - 3 + 1 = -11."
			],
			"maple": [
				"a:= proc(n) option remember; `if`(n=0, 0,",
				"      a(n-1)+3^(n-1)*numtheory[mobius](n))",
				"    end:",
				"seq(a(n), n=1..33);  # _Alois P. Heinz_, Oct 13 2017"
			],
			"mathematica": [
				"Table[3^Range[0, n - 1].MoebiusMu[Range[n]], {n, 50}]"
			],
			"program": [
				"(PARI) a(n) = sum(k=1, n, moebius(k)*3^(k-1)); \\\\ _Michel Marcus_, Oct 01 2017"
			],
			"xref": [
				"Cf. A008683, A127513, A292524."
			],
			"keyword": "easy,sign,base",
			"offset": "1,2",
			"author": "_Alonso del Arte_, Sep 22 2017",
			"references": 3,
			"revision": 21,
			"time": "2021-05-19T10:22:32-04:00",
			"created": "2017-10-13T15:51:14-04:00"
		}
	]
}