{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A284003",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 284003,
			"data": "1,2,6,3,30,15,5,10,210,105,35,70,7,14,42,21,2310,1155,385,770,77,154,462,231,11,22,66,33,330,165,55,110,30030,15015,5005,10010,1001,2002,6006,3003,143,286,858,429,4290,2145,715,1430,13,26,78,39,390,195,65,130,2730,1365,455,910,91,182,546,273,510510,255255,85085,170170,17017",
			"name": "a(n) = A007913(A283477(n)) = A019565(A006068(n)).",
			"comment": [
				"A squarefree analog of A302783. Each term is either a divisor or a multiple of the next one. In contrast to A302033 at each step the previous term can be multiplied (or divided), not just by a single prime, but possibly by a product of several distinct ones, A019565(A000975(k)). E.g., a(3) = 3, a(4) = 2*5*a(3) = 30. - _Antti Karttunen_, Apr 17 2018"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A284003/b284003.txt\"\u003eTable of n, a(n) for n = 0..8191\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A007913(A283477(n)).",
				"Other identities. For all n \u003e= 0:",
				"A048675(a(n)) = A006068(n).",
				"A046523(a(n)) = A284004(n).",
				"It seems that A001222(a(n)) = A209281(n).",
				"a(n) = A019565(A006068(n)) = A302033(A064707(n)). - _Antti Karttunen_, Apr 16 2018"
			],
			"mathematica": [
				"Table[Apply[Times, FactorInteger[#] /. {p_, e_} /; e \u003e 0 :\u003e Times @@ (p^Mod[e, 2])] \u0026[Times @@ Map[#1^#2 \u0026 @@ # \u0026, FactorInteger[#] /. {p_, e_} /; e == 1 :\u003e {Times @@ Prime@ Range@ PrimePi@ p, e}] \u0026[Times @@ Prime@ Flatten@ Position[#, 1] \u0026@ Reverse@ IntegerDigits[n, 2]]], {n, 0, 52}] (* _Michael De Vlieger_, Mar 18 2017 *)"
			],
			"program": [
				"(PARI)",
				"A007913(n) = core(n);",
				"A034386(n) = prod(i=1, primepi(n), prime(i));",
				"A108951(n) = { my(f=factor(n)); prod(i=1, #f~, A034386(f[i, 1])^f[i, 2]) };  \\\\ From _Charles R Greathouse IV_, Jun 28 2015",
				"A019565(n) = {my(j,v); factorback(Mat(vector(if(n, #n=vecextract(binary(n), \"-1..1\")), j, [prime(j), n[j]])~))}; \\\\ This function from _M. F. Hasler_",
				"A283477(n) = A108951(A019565(n));",
				"A284003(n) = A007913(A283477(n));",
				"(PARI)",
				"A006068(n)= { my(s=1, ns); while(1, ns = n \u003e\u003e s; if(0==ns, break()); n = bitxor(n, ns); s \u003c\u003c= 1; ); return (n); } \\\\ From A006068",
				"A284003(n) = A019565(A006068(n)); \\\\ (and use A019565 from above) - _Antti Karttunen_, Apr 16 2018",
				"(Scheme) (define (A284003 n) (A007913 (A283477 n)))"
			],
			"xref": [
				"A permutation of A005117.",
				"Cf. A000975, A001222, A006068, A007913, A019565, A046523, A048675, A064707, A209281, A283477, A284004.",
				"Cf. also A277811, A283475, A302033, A302783."
			],
			"keyword": "nonn,look",
			"offset": "0,2",
			"author": "_Antti Karttunen_, Mar 18 2017",
			"ext": [
				"Name amended with a second formula by _Antti Karttunen_, Apr 16 2018"
			],
			"references": 8,
			"revision": 39,
			"time": "2018-04-27T17:10:30-04:00",
			"created": "2017-03-19T01:10:35-04:00"
		}
	]
}