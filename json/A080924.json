{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A080924",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 80924,
			"data": "0,1,3,1,15,1,63,1,255,1,1023,1,4095,1,16383,1,65535,1,262143,1,1048575,1,4194303,1,16777215,1,67108863,1,268435455,1,1073741823,1,4294967295,1,17179869183,1,68719476735,1,274877906943,1,1099511627775,1",
			"name": "Jacobsthal gap sequence.",
			"comment": [
				"Inverse binomial transform of A080925",
				"From _Peter Bala_, Dec 26 2012: (Start)",
				"Let F(x) = product {n \u003e= 0} (1 - x^(3*n+1))/(1 - x^(3*n+2)). This sequence is the simple continued fraction expansion of the real number F(1/4) = 0.79761 68651 30459 16010 ... = 1/(1 + 1/(3 + 1/(1 + 1/(15 + 1/(1 + 1/(63 + 1/(1 + 1/(255 + ...)))))))). See A111317. (End)",
				"Also, the decimal representation of the diagonal from the corner to the origin of the n-th stage of growth of the two-dimensional cellular automaton defined by \"Rule 3\", based on the 5-celled von Neumann neighborhood, initialized with a single black (ON) cell at stage zero. - _Robert Price_, Apr 19 2017"
			],
			"reference": [
				"S. Wolfram, A New Kind of Science, Wolfram Media, 2002; p. 170."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A080924/b080924.txt\"\u003eTable of n, a(n) for n = 0..300\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"http://arxiv.org/abs/1503.01168\"\u003eOn the Number of ON Cells in Cellular Automata\u003c/a\u003e, arXiv:1503.01168 [math.CO], 2015",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ElementaryCellularAutomaton.html\"\u003eElementary Cellular Automaton\u003c/a\u003e",
				"S. Wolfram, \u003ca href=\"http://wolframscience.com/\"\u003eA New Kind of Science\u003c/a\u003e",
				"Wolfram Research, \u003ca href=\"http://atlas.wolfram.com/\"\u003eWolfram Atlas of Simple Programs\u003c/a\u003e",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_2D_5-Neighbor_Cellular_Automata\"\u003eIndex to 2D 5-Neighbor Cellular Automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_Elementary_Cellular_Automata\"\u003eIndex to Elementary Cellular Automata\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (-1,4,4)."
			],
			"formula": [
				"a(2n) = 3*A001045(2n) = 3*A002450(n) = 4^n-1, a(2n+1)=1.",
				"a(n) = (2^n-2*(-1)^n+(-2)^n)/2.",
				"G.f.: x*(1+4*x)/((1+x)*(1+2*x)*(1-2*x)).",
				"E.g.f.: (exp(2*x)-2*exp(-x)+exp(-2*x))/2."
			],
			"mathematica": [
				"CoefficientList[Series[x (1 + 4 x) / ((1 + x) (1 + 2 x) (1 - 2 x)), {x, 0, 50}], x] (* _Vincenzo Librandi_, Aug 05 2013 *)",
				"LinearRecurrence[{-1, 4, 4}, {0, 1, 3}, 42] (* _Jean-François Alcover_, Sep 21 2017 *)"
			],
			"xref": [
				"Cf. A001045, A002450, A080926, A080927, A111317."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Paul Barry_, Feb 26 2003",
			"references": 6,
			"revision": 26,
			"time": "2017-09-21T11:00:13-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}