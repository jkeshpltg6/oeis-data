{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A336633",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 336633,
			"data": "1,1,2,2,6,16,6,24,116,116,24,120,888,1624,888,120,720,7416,20984,20984,7416,720,5040,67968,270432,419680,270432,67968,5040,40320,682272,3587904,7861664,7861664,3587904,682272,40320,362880,7467840,49701024,144570624,204403264,144570624",
			"name": "Triangle read by rows: T(n,k) is the number of generalized permutations related to the degenerate Eulerian numbers with exactly k ascents (0 \u003c= k \u003c= max(0,n-1)).",
			"link": [
				"Orli Herscovici, \u003ca href=\"https://arxiv.org/abs/2007.13205\"\u003eGeneralized permutations related to the degenerate Eulerian numbers\u003c/a\u003e, arXiv preprint arXiv:2007.13205 [math.CO], 2020."
			],
			"formula": [
				"T(n,k) = (n+k)*T(n-1,k) + (2*n-k-1)*T(n-1,k-1) for positive integers n and 0 \u003c= k \u003c n; T(0,0)=1 (or T(1,0)=1); otherwise T(n,k)=0.",
				"From _Peter Bala_, Jan 08 2021: (Start)",
				"The following remarks are all conjectures:",
				"The e.g.f. (without the initial 1) A(x,t) = x + (2 + 2*t)*x^2/2! + (6 + 16*t + 6*t^2)*x^3/3! + ... satisfies the autonomous differential equation dA/dx = (1 + A)^2*(1 + t*A)^2.",
				"The series reversion of A(x,t) with respect to x equals Integral_{u = 0..x} 1/((1 + u)^2*(1 + t*u)^2) du.",
				"Let f(x,t) = (1 + x)^2*(1 + t*x)^2 and let D be the operator f(x,t)*d/dx. Then the (n+1)-th row polynomial = D^n(f(x,t)) evaluated at x = 0. (End)"
			],
			"example": [
				"Triangle T(n,k) (with rows n \u003e= 0 and columns k = 0..max(0,n-1)) begins:",
				"     1;",
				"     1;",
				"     2,     2;",
				"     6,    16,      6;",
				"    24,   116,    116,     24;",
				"   120,   888,   1624,    888,    120;",
				"   720,  7416,  20984,  20984,   7416,   720;",
				"  5040, 67968, 270432, 419680, 270432, 67968, 5040;",
				"  ..."
			],
			"maple": [
				"Tnk[0, 0] := 1; for n to N do",
				"    for k from 0 to n do if 0 \u003c k and k \u003c n then Tnk[n, k] := (n + k)*Tnk[n - 1, k] + (2*n - k - 1)*Tnk[n - 1, k - 1]; else if k = 0 then Tnk[n, k] := (n + k)*Tnk[n - 1, k]; else Tnk[n, k] := 0; end if; end if; end do;",
				"end do"
			],
			"xref": [
				"Columns k = 0..1 give: A000142, A288964. Row sums give A007559.",
				"Cf. A008292, A008517."
			],
			"keyword": "easy,nonn,tabf",
			"offset": "0,3",
			"author": "_Orli Herscovici_, Jul 28 2020",
			"references": 1,
			"revision": 39,
			"time": "2021-01-11T23:20:43-05:00",
			"created": "2020-09-20T01:41:18-04:00"
		}
	]
}