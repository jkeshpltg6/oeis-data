{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A296780",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 296780,
			"data": "0,184,8788,126016,953312,4980744,20000844,66781696,192914176,498751000,1176318500,2576159424,5295012768,10321114216,19204598812,34338770944,59257739264,99137135736,161273290356,255920008000,397011388000,603492894664,900354295148",
			"name": "Detour index of the n X n X n grid graph.",
			"comment": [
				"Also, the maximum detour index of any bipartite graph on n^3 nodes. - _Andrew Howroyd_, Dec 21 2017"
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A296780/b296780.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DetourIndex.html\"\u003eDetour Index\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GridGraph.html\"\u003eGrid Graph\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_17\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,4,-20,0,56,-28,-84,70,70,-84,-28,56,0,-20,4,3,-1)."
			],
			"formula": [
				"a(2n-1) = 4*(n - 1)^3*(4*n^2 - 2*n + 1)^3, a(2n) = 8*n^3*(32*n^6 - 10*n^3 + 1). - _Andrew Howroyd_, Dec 21 2017",
				"From _Colin Barker_, Dec 21 2017: (Start)",
				"G.f.: 4*x^2*(46 + 2059*x + 24729*x^2 + 135948*x^3 + 448126*x^4 + 938845*x^5 + 1358863*x^6 + 1346304*x^7 + 941714*x^8 + 442773*x^9 + 138599*x^10 + 25508*x^11 + 2482*x^12 + 83*x^13 + x^14) / ((1 - x)^10*(1 + x)^7).",
				"a(n) = 3*a(n-1) + 4*a(n-2) - 20*a(n-3) + 56*a(n-5) - 28*a(n-6) - 84*a(n-7) + 70*a(n-8) + 70*a(n-9) - 84*a(n-10) - 28*a(n-11) + 56*a(n-12) - 20*a(n-14) + 4*a(n-15) + 3*a(n-16) - a(n-17) for n\u003e17.",
				"(End)",
				"a(n) = A296819(n^3). - _Andrew Howroyd_, Dec 23 2017"
			],
			"mathematica": [
				"Table[Piecewise[{{n^3 (n^6 - 5 n^3/2 + 2)/2, Mod[n, 2] == 0}, {(n - 1)^3 (n^2 + n + 1)^3/2, Mod[n, 2] == 1}}], {n, 20}]",
				"LinearRecurrence[{3, 4, -20, 0, 56, -28, -84, 70, 70, -84, -28, 56, 0, -20, 4, 3, -1}, {0, 184, 8788, 126016, 953312, 4980744, 20000844, 66781696, 192914176, 498751000, 1176318500, 2576159424, 5295012768, 10321114216, 19204598812, 34338770944, 59257739264}, 20]",
				"CoefficientList[Series[4 x (46 + 2059 x + 24729 x^2 + 135948 x^3 + 448126 x^4 + 938845 x^5 + 1358863 x^6 + 1346304 x^7 + 941714 x^8 + 442773 x^9 + 138599 x^10 + 25508 x^11 + 2482 x^12 + 83 x^13 + x^14)/((1 - x)^10 (1 + x)^7), {x, 0, 20}], x]"
			],
			"program": [
				"(PARI) a(n) = if(n%2, (n-1)^3*(n^2 + n + 1)^3, n^3*(n^6 - 5*n^3/2 + 2))/2; \\\\ _Andrew Howroyd_, Dec 21 2017",
				"(PARI) concat(0, Vec(4*x^2*(46 + 2059*x + 24729*x^2 + 135948*x^3 + 448126*x^4 + 938845*x^5 + 1358863*x^6 + 1346304*x^7 + 941714*x^8 + 442773*x^9 + 138599*x^10 + 25508*x^11 + 2482*x^12 + 83*x^13 + x^14) / ((1 - x)^10*(1 + x)^7) + O(x^40))) \\\\ _Colin Barker_, Dec 21 2017"
			],
			"xref": [
				"Cf. A296779, A296819."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Eric W. Weisstein_, Dec 20 2017",
			"ext": [
				"Terms a(4) and beyond from _Andrew Howroyd_, Dec 21 2017"
			],
			"references": 3,
			"revision": 16,
			"time": "2017-12-24T10:35:30-05:00",
			"created": "2017-12-20T14:39:51-05:00"
		}
	]
}