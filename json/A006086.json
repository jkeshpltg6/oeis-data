{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006086",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6086,
			"id": "M4248",
			"data": "1,6,45,60,90,420,630,1512,3780,5460,7560,8190,9100,15925,16632,27300,31500,40950,46494,51408,55125,64260,66528,81900,87360,95550,143640,163800,172900,185976,232470,257040,330750,332640,464940,565488,598500,646425,661500",
			"name": "Unitary harmonic numbers (those for which the unitary harmonic mean is an integer).",
			"comment": [
				"Let ud(n) and usigma(n) be number of and sum of unitary divisors of n; then the unitary harmonic mean of the unitary divisors is H(n) = n*ud(n)/usigma(n). - _Emeric Deutsch_, Dec 22 2004",
				"A103340(a(n)) = 1; A103339(a(n)) = A006087(n). - _Reinhard Zumkeller_, Mar 17 2012"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Donovan Johnson, \u003ca href=\"/A006086/b006086.txt\"\u003eTable of n, a(n) for n = 1..290\u003c/a\u003e (terms \u003c 10^12)",
				"Takeshi Goto, \u003ca href=\"http://doi.org/10.1216/rmjm/1194275935\"\u003eUpper Bounds for Unitary Perfect Numbers and Unitary Harmonic Numbers\u003c/a\u003e, Rocky Mountain Journal of Mathematics, Vol. 37, No. 5 (2007), pp. 1557-1576.",
				"P. Hagis, Jr. and G. Lord, \u003ca href=\"https://doi.org/10.1090/S0002-9939-1975-0369231-9\"\u003eUnitary harmonic numbers\u003c/a\u003e, Proc. Amer. Math. Soc., 51 (1975), 1-7.",
				"P. Hagis, Jr. and G. Lord, \u003ca href=\"/A006086/a006086.pdf\"\u003eUnitary harmonic numbers\u003c/a\u003e, Proc. Amer. Math. Soc., 51 (1975), 1-7. (Annotated scanned copy)",
				"Charles R. Wall, \u003ca href=\"http://www.fq.math.ca/Scanned/21-1/wall.pdf\"\u003eUnitary harmonic numbers\u003c/a\u003e, Fibonacci Quarterly, Vol. 21, No. 1 (1983), pp. 18-25."
			],
			"formula": [
				"If m is a term and omega(m) = A001221(m) = k, then m \u003c 2^(k*2^k) (Goto, 2007). - _Amiram Eldar_, Jun 06 2020"
			],
			"mathematica": [
				"ud[n_] := 2^PrimeNu[n]; usigma[n_] := Sum[ If[ GCD[d, n/d] == 1, d, 0], {d, Divisors[n]}]; uhm[n_] := n*ud[n]/usigma[n]; Reap[ Do[ If[ IntegerQ[uhm[n]], Print[n]; Sow[n]], {n, 1, 10^6}]][[2, 1]] (* _Jean-François Alcover_, May 16 2013 *)"
			],
			"program": [
				"(Haskell)",
				"a006086 n = a006086_list !! (n-1)",
				"a006086_list = filter ((== 1) . a103340) [1..]",
				"-- _Reinhard Zumkeller_, Mar 17 2012",
				"(PARI) udivs(n) = {my(d = divisors(n)); select(x-\u003e(gcd(x, n/x)==1), d); }",
				"isok(n) = my(v=udivs(n)); denominator(n*#v/vecsum(v))==1; \\\\ _Michel Marcus_, May 07 2017",
				"(PARI) is(n,f=factor(n))=(n\u003c\u003c(#f~))%sumdivmult([n,f], d, if(gcd(d, n/d)==1, d))==0 \\\\ _Charles R Greathouse IV_, Nov 05 2021",
				"(PARI) list(lim)=my(v=List()); forfactored(n=1,lim\\1, if((n[1]\u003c\u003comega(n))%sumdivmult(n, d, if(gcd(d, n[1]/d)==1, d))==0, listput(v, n[1]))); Vec(v) \\\\ _Charles R Greathouse IV_, Nov 05 2021"
			],
			"xref": [
				"See A006087 for more info.",
				"Cf. A103339, A103340."
			],
			"keyword": "nonn,nice",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Emeric Deutsch_, Dec 22 2004"
			],
			"references": 19,
			"revision": 51,
			"time": "2021-11-05T11:59:22-04:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}