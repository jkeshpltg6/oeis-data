{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097248",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97248,
			"data": "1,2,3,3,5,6,7,6,5,10,11,5,13,14,15,5,17,10,19,15,21,22,23,10,7,26,15,21,29,30,31,10,33,34,35,15,37,38,39,30,41,42,43,33,7,46,47,15,11,14,51,39,53,30,55,42,57,58,59,7,61,62,35,15,65,66,67,51,69,70,71,30,73,74,21",
			"name": "a(n) is the eventual stable point reached when iterating k -\u003e A097246(k), starting from k = n.",
			"comment": [
				"a(n) = r(n,m) with m such that r(n,m)=r(n,m+1), where r(n,k) = A097246(r(n,k-1)), r(n,0)=n. (The original definition.)",
				"A097248(n) = r(n,a(n)).",
				"From _Antti Karttunen_, Nov 15 2016: (Start)",
				"The above remark could be interpreted to mean that A097249(n) \u003c= a(n).",
				"All terms are squarefree, and the squarefree numbers are the fixed points.",
				"These are also fixed points eventually reached when iterating A277886.",
				"(End)"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A097248/b097248.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(A005117(n)) = A005117(n).",
				"From _Antti Karttunen_, Nov 15 2016: (Start)",
				"If A008683(n) \u003c\u003e 0 [when n is squarefree], a(n) = n, otherwise a(n) = a(A097246(n)).",
				"If A277885(n) = 0, a(n) = n, otherwise a(n) = a(A277886(n)).",
				"A007913(a(n)) = a(n).",
				"a(A007913(n)) = A007913(n).",
				"A048675(a(n)) = A048675(n).",
				"a(A260443(n)) = A019565(n).",
				"(End)",
				"From _Peter Munn_, Feb 06 2020: (Start)",
				"a(1) = 1; a(p) = p, for prime p; a(m*k) = A331590(a(m), a(k)).",
				"a(A331590(m,k)) = A331590(a(m), a(k)).",
				"a(n^2) = a(A003961(n)) = A003961(a(n)).",
				"a(A225546(n)) = a(n).",
				"a(n) = A225546(2^A048675(n)) = A019565(A048675(n)).",
				"a(A329050(n,k)) = prime(n+k-1) = A000040(n+k-1).",
				"a(A329332(n,k)) = A019565(n * k).",
				"Equivalently, a(A019565(n)^k) = A019565(n * k).",
				"(End)",
				"From _Antti Karttunen_, Feb 22-25 \u0026 Mar 01 2020: (Start)",
				"a(A019565(x)*A019565(y)) = A019565(x+y).",
				"a(A332461(n)) = A332462(n).",
				"a(A332824(n)) = A019565(n).",
				"a(A277905(n,k)) = A277905(n,1) = A019565(n), for all n \u003e= 1, and 1 \u003c= k \u003c= A018819(n).",
				"(End)"
			],
			"mathematica": [
				"Table[FixedPoint[Times @@ Map[#1^#2 \u0026 @@ # \u0026, Partition[#, 2, 2] \u0026@ Flatten[FactorInteger[#] /. {p_, e_} /; e \u003e= 2 :\u003e {If[OddQ@ e, {p, 1}, {1, 1}], {NextPrime@ p, Floor[e/2]}}]] \u0026, n], {n, 75}] (* _Michael De Vlieger_, Mar 18 2017 *)"
			],
			"program": [
				"(PARI)",
				"A097246(n) = { my(f=factor(n)); prod(i=1, #f~, (nextprime(f[i,1]+1)^(f[i,2]\\2))*((f[i,1])^(f[i,2]%2))); };",
				"A097248(n) = { my(k=A097246(n)); while(k\u003c\u003en, n = k; k = A097246(k)); k; };",
				"\\\\ _Antti Karttunen_, Mar 18 2017",
				"(Scheme) ;; with memoization-macro definec",
				";; Two implementations:",
				"(definec (A097248 n) (if (not (zero? (A008683 n))) n (A097248 (A097246 n))))",
				"(definec (A097248 n) (if (zero? (A277885 n)) n (A097248 (A277886 n))))",
				";; _Antti Karttunen_, Nov 15 2016",
				"(Python)",
				"from sympy import factorint, nextprime",
				"from operator import mul",
				"def a097246(n):",
				"    f=factorint(n)",
				"    return 1 if n==1 else reduce(mul, [(nextprime(i)**int(f[i]/2))*(i**(f[i]%2)) for i in f])",
				"def a(n):",
				"    k=a097246(n)",
				"    while k!=n:",
				"        n=k",
				"        k=a097246(k)",
				"    return k # _Indranil Ghosh_, May 15 2017"
			],
			"xref": [
				"Range of values is A005117.",
				"Cf. A008683, A019565, A048675, A097246, A097247, A097249, A277905, A332824.",
				"A003961, A225546, A277885, A277886, A331590 are used to express relationship between terms of this sequence.",
				"The formula section also details how the sequence maps the terms of A007913, A260443, A329050, A329332.",
				"See comments/formulas in A283475, A283478, A331751 giving their relationship to this sequence."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Reinhard Zumkeller_, Aug 03 2004",
			"ext": [
				"Name changed and the original definition moved to the Comments section by _Antti Karttunen_, Nov 15 2016"
			],
			"references": 19,
			"revision": 57,
			"time": "2020-04-16T09:37:39-04:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}