{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A293442",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 293442,
			"data": "1,2,2,3,2,4,2,6,3,4,2,6,2,4,4,5,2,6,2,6,4,4,2,12,3,4,6,6,2,8,2,10,4,4,4,9,2,4,4,12,2,8,2,6,6,4,2,10,3,6,4,6,2,12,4,12,4,4,2,12,2,4,6,15,4,8,2,6,4,8,2,18,2,4,6,6,4,8,2,10,5,4,2,12,4,4,4,12,2,12,4,6,4,4,4,20,2,6,6,9,2,8,2,12,8",
			"name": "Multiplicative with a(p^e) = A019565(e).",
			"comment": [
				"From _Peter Munn_, Apr 06 2021: (Start)",
				"a(n) is determined by the prime signature of n.",
				"Compare with the multiplicative, self-inverse A225546, which also maps 2^e to the squarefree number A019565(e). However, this sequence maps p^e to the same squarefree number for every prime p, whereas A225546 maps the e-th power of progressively larger primes to progressively greater powers of A019565(e).",
				"Both sequences map powers of squarefree numbers to powers of squarefree numbers.",
				"(End)"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A293442/b293442.txt\"\u003eTable of n, a(n) for n = 1..65537\u003c/a\u003e",
				"\u003ca href=\"/index/Eu#epf\"\u003eIndex entries for sequences computed from exponents in factorization of n\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#prime_signature\"\u003eIndex entries for sequences related to prime signature\u003c/a\u003e"
			],
			"formula": [
				"a(1) = 1; for n \u003e 1, a(n) = A019565(A067029(n)) * a(A028234(n)).",
				"Other identities. For all n \u003e= 1:",
				"a(a(n)) = A293444(n).",
				"A048675(a(n)) = A001222(n).",
				"A001222(a(n)) = A064547(n) = A048675(A293444(n)).",
				"A007814(a(n)) = A162642(n).",
				"A087207(a(n)) = A267116(n).",
				"A248663(a(n)) = A268387(n).",
				"From _Peter Munn_, Mar 14 2021: (Start)",
				"Alternative definition: a(1) = 1; a(2) = 2; a(n^2) = A003961(a(n)); a(A003961(n)) = a(n); if A059895(n, k) = 1, a(n*k) = a(n) * a(k).",
				"For n \u003e= 3, a(n) \u003c n.",
				"a(2n) = A334747(a(A006519(n))) * a(n/A006519(n)), where A006519(n) is the largest power of 2 dividing n.",
				"a(2n+1) = a(A064989(2n+1)).",
				"a(n) = a(A007913(n)) * a(A008833(n)) = 2^A162642(n) * A003961(a(A000188(n))).",
				"(End)"
			],
			"mathematica": [
				"f[n_] := If[n == 1, 1, Apply[Times, Prime@ Flatten@ Position[Reverse@ IntegerDigits[Last@ #, 2], 1]] * f[n/Apply[Power, #]] \u0026@ FactorInteger[n][[1]]]; Array[f, 105] (* _Michael De Vlieger_, Oct 31 2017 *)"
			],
			"program": [
				"(Scheme, with memoization-macro definec)",
				"(definec (A293442 n) (if (= 1 n) n (* (A019565 (A067029 n)) (A293442 (A028234 n)))))"
			],
			"xref": [
				"Sequences used in a definition of this sequence: A000188, A003961, A019565, A028234, A059895, A067029, A162642.",
				"Sequences with related definitions: A225546, A293443, A293444.",
				"Cf. also A293214.",
				"Sequences used to express relationship between terms of this sequence: A006519, A007913, A008833, A064989, A334747.",
				"Sequences related via this sequence: (A001222, A048675, A064547), (A007814, A162642), (A087207, A267116), (A248663, A268387)."
			],
			"keyword": "nonn,mult",
			"offset": "1,2",
			"author": "_Antti Karttunen_, Oct 31 2017",
			"references": 23,
			"revision": 28,
			"time": "2021-04-29T04:10:15-04:00",
			"created": "2017-11-02T15:36:37-04:00"
		}
	]
}