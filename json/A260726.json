{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A260726",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 260726,
			"data": "4,8,363,484,5445,46464,252,2138312,12321,0,44,23232,31213,686,53187678135,44944,272,24642,171,0,525,88,575,46464,5221225,62426,36963,252,464,0,1783799973871,291080192,2112,4114,53235,69696,333,20102,93639,0,656,858858",
			"name": "a(n) = smallest palindrome k \u003e n such that k/n is a square; a(n) = 0 if no solution exists.",
			"comment": [
				"If n is a multiple of 10 then a(n) = 0, since no palindrome ends in 0.",
				"Up to 200 only 3 terms are currently unknown, a(125) \u003e 5.2*10^28, a(177) \u003e 3.5*10^27 and a(185) \u003e 4.5*10^27. See Links for a table of known values. - _Giovanni Resta_, Aug 05 2015",
				"If a(125) \u003e 0, then the first 3 digits of a(125) are 521 and the last 3 digits of a(125) are 125. Proof: Let m^2 = a(125)/125. Then m is odd as otherwise 125*m^2 is a multiple of 10 which is not a palindrome. Since m is odd, m^2 == 1 mod 8 and thus 125*m^2 == 125 mod 1000. - _Chai Wah Wu_, Mar 31 2016"
			],
			"link": [
				"Giovanni Resta, \u003ca href=\"/A260726/a260726.txt\"\u003eKnown values up to a(200)\u003c/a\u003e"
			],
			"example": [
				"a(3) = 363, because 363/3 = 11^2. 363 * 3 = 1089, which is also a square.",
				"a(15) = 53187678135, because 53187678135/15 = 59547^2 and 53187678135 * 15 = 893205^2."
			],
			"maple": [
				"ispali:= proc(n) local L; L:= convert(n,base,10); ListTools:-Reverse(L)=L end proc:",
				"f:= proc(n) local m;",
				"   if n mod 10 = 0 then return 0 fi;",
				"   for m from 2 to 10^6 do if ispali(m^2*n) then return m^2*n fi od:",
				"   -1  # signals time-out",
				"end proc:",
				"seq(f(n), n=1..50); # _Robert Israel_, Aug 21 2015"
			],
			"mathematica": [
				"palQ[n_] := Block[{d = IntegerDigits@ n}, d == Reverse@ d]; a[n_] := If[ Mod[n, 10] == 0, 0, Block[{q = 2}, While[! palQ[q^2 * n], q++]; q^2 * n]]; Array[a, 42] (* _Giovanni Resta_, Aug 18 2015 *)"
			],
			"program": [
				"(Python)",
				"for k in range(1,150):",
				"....c=2",
				"....while c \u003c 10**8:",
				"........kk=k*c**2",
				"........if kk==int(str(kk)[::-1])",
				"............print (k,kk)",
				"............c=10**9",
				"........c=c+1"
			],
			"xref": [
				"Cf. A002113, A000290, A023108, A061563."
			],
			"keyword": "nonn,base",
			"offset": "1,1",
			"author": "_Pieter Post_, Jul 30 2015",
			"ext": [
				"Missing a(13) from _Giovanni Resta_, Aug 05 2015"
			],
			"references": 1,
			"revision": 19,
			"time": "2016-04-01T10:39:45-04:00",
			"created": "2015-08-29T17:21:14-04:00"
		}
	]
}