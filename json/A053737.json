{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A053737",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 53737,
			"data": "0,1,2,3,1,2,3,4,2,3,4,5,3,4,5,6,1,2,3,4,2,3,4,5,3,4,5,6,4,5,6,7,2,3,4,5,3,4,5,6,4,5,6,7,5,6,7,8,3,4,5,6,4,5,6,7,5,6,7,8,6,7,8,9,1,2,3,4,2,3,4,5,3,4,5,6,4,5,6,7,2,3,4,5,3,4,5,6,4,5,6,7,5,6,7,8,3,4,5,6,4,5,6,7,5",
			"name": "Sum of digits of (n written in base 4).",
			"comment": [
				"Also the fixed point of the morphism 0-\u003e{0,1,2,3}, 1-\u003e{1,2,3,4}, 2-\u003e{2,3,4,5}, etc. - _Robert G. Wilson v_, Jul 27 2006"
			],
			"link": [
				"Donovan Johnson, \u003ca href=\"/A053737/b053737.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"F. T. Adams-Watters, F. Ruskey, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL12/Ruskey2/ruskey14.html\"\u003eGenerating Functions for the Digital Sum and Other Digit Counting Sequences\u003c/a\u003e, JIS Vol. 12 (2009), Article 09.5.6.",
				"Steve Butler and Ron L. Graham, \u003ca href=\"http://arxiv.org/abs/1003.4422\"\u003eShuffling with ordered cards\u003c/a\u003e, arXiv 1003:4422 [math.CO], 2010.",
				"Jeffrey O. Shallit, \u003ca href=\"http://www.jstor.org/stable/2322179\"\u003eProblem 6450\u003c/a\u003e, Advanced Problems, The American Mathematical Monthly, Vol. 91, No. 1 (1984), pp. 59-60; \u003ca href=\"http://www.jstor.org/stable/2322523\"\u003eTwo series, solution to Problem 6450\u003c/a\u003e, ibid., Vol. 92, No. 7 (1985), pp. 513-514.",
				"Robert Walker, \u003ca href=\"http://robertinventor.com/ftswiki/Self_Similar_Sloth_Canon_Number_Sequences\"\u003eSelf Similar Sloth Canon Number Sequences\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DigitSum.html\"\u003eDigit Sum\u003c/a\u003e."
			],
			"formula": [
				"From _Benoit Cloitre_, Dec 19 2002: (Start)",
				"a(0) = 0, a(4n+i) = a(n)+i for 0 \u003c= i \u003c= 3.",
				"a(n) = n - 3*Sum_{k\u003e0} floor(n/4^k) = n - 3*A054893(n). (End)",
				"G.f.: (Sum_{k\u003e=0} (x^(4^k) + 2*x^(2*4^k) + 3*x^(3*4^k))/(1 + x^(4^k) + x^(2*4^k) + x^(3*4^k))/(1-x). - _Franklin T. Adams-Watters_, Nov 03 2005",
				"a(n) = A138530(n,4) for n \u003e 3. - _Reinhard Zumkeller_, Mar 26 2008",
				"a(n) = Sum_{k\u003e=0} A030386(n,k). - _Philippe Deléham_, Oct 21 2011",
				"a(n) = A007953(A007090(n)). - _Reinhard Zumkeller_, Mar 19 2015",
				"a(0) = 0; a(n) = a(n - 4^floor(log_4(n))) + 1. - _Ilya Gutkovskiy_, Aug 23 2019",
				"Sum_{n\u003e=1} a(n)/(n*(n+1)) = 4*log(4)/3 (Shallit, 1984). - _Amiram Eldar_, Jun 03 2021"
			],
			"example": [
				"a(20) = 1+1+0 = 2 because 20 is written as 110 base 4.",
				"From _Omar E. Pol_, Feb 21 2010: (Start)",
				"This can be written as a triangle (cf. A000120):",
				"0,",
				"1,2,3,",
				"1,2,3,4,2,3,4,5,3,4,5,6,",
				"1,2,3,4,2,3,4,5,3,4,5,6,4,5,6,7,2,3,4,5,3,4,5,6,4,5,6,7,5,6,7,8,3,4,5,6,4,5,6,7,5,6,7,8,6,7,8,9,",
				"1,2,3,4,2,3,4,5,3,4,5,6,4,5,6,7,2,3,4,5,3,4,5,6,4,5,6,7,5,6,7,8,3,4,5,6,4,...",
				"where the rows converge to A173524.",
				"(End)"
			],
			"maple": [
				"A053737 := proc(n)",
				"    add(d,d=convert(n,base,4)) ;",
				"end proc: # _R. J. Mathar_, Oct 31 2012"
			],
			"mathematica": [
				"Table[Plus @@ IntegerDigits[n, 4], {n, 0, 100}] (* or *)",
				"Nest[ Flatten[ #1 /. a_Integer -\u003e {a, a+1, a+2, a+3}] \u0026, {0}, 4] (* _Robert G. Wilson v_, Jul 27 2006 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c1,0,if(n%4,a(n-1)+1,a(n/4)))",
				"(PARI) a(n) = sumdigits(n, 4); \\\\ _Michel Marcus_, Aug 24 2019",
				"(Haskell)",
				"a053737 n = if n == 0 then 0 else a053737 m + r where (m, r) = divMod n 4",
				"-- _Reinhard Zumkeller_, Mar 19 2015",
				"(MAGMA) [\u0026+Intseq(n,4):n in [0..104]]; // _Marius A. Burtea_, Jan 17 2019",
				"(MATLAB) for u=0:104; sol(u+1)=sum(dec2base(u,4)-'0');end",
				"sol % _Marius A. Burtea_, Jan 17 2019"
			],
			"xref": [
				"Cf. A000120, A007953, A053735, A231664-A231667.",
				"Cf. A173524. - _Omar E. Pol_, Feb 21 2010",
				"Related base-4 sequences:  A053737, A230631, A230632, A010064, A230633, A230634, A230635, A230636, A230637, A230638, A010065 (trajectory of 1).",
				"Cf. A007090, A239690."
			],
			"keyword": "base,nonn,easy",
			"offset": "0,3",
			"author": "_Henry Bottomley_, Mar 28 2000",
			"references": 48,
			"revision": 74,
			"time": "2021-06-03T03:29:32-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}