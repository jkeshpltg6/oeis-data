{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A024810",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 24810,
			"data": "1,2,5,10,20,40,81,162,325,651,1303,2607,5215,10430,20860,41721,83443,166886,333772,667544,1335088,2670176,5340353,10680707,21361414,42722829,85445659,170891318,341782637,683565275,1367130551,2734261102,5468522204,10937044409",
			"name": "a(n) = floor( tan(m*Pi/2) ), where m = 1 - 2^(-n).",
			"comment": [
				"Geometrically, each term of the sequence represents the integer part of the distance between opposite vertices and also edges of even sided polygons, each of which has double the number of sides of the previous, starting with a square of unit length. - _Torlach Rush_, Feb 21 2014",
				"a(n) = greatest integer k such that k/2^n \u003c 2/Pi. - _Clark Kimberling_, Oct 10 2017",
				"Number of roots of sin(1/x) = 0 in interval 1/2^(n+1) \u003c x \u003c 1. - _Hugo Pfoertner_, Oct 24 2019",
				"Or simply: number of zeros of sin(x) in the range [1, 2^(n+1)]. - _M. F. Hasler_, Oct 25 2019"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A024810/b024810.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Sanjar M. Abrarov, Rajinder K. Jagpal, Rehan Siddiqui, and Brendan M. Quine, \u003ca href=\"https://arxiv.org/abs/2107.01027\"\u003eAlgorithmic determination of a large integer in the two-term Machin-like formula for pi\u003c/a\u003e, arXiv:2107.01027 [math.GM], 2021.",
				"Hugo Pfoertner, \u003ca href=\"/A024810/a024810.pdf\"\u003eIllustration of initial terms\u003c/a\u003e, sin(1/x) plotted on logarithmic x axis."
			],
			"formula": [
				"a(n) = floor( 1 / tan( Pi / 2^(n+1) )). - _Michael Somos_, Feb 24 2014",
				"a(n) = floor(2^(n+1)/Pi). - _Clark Kimberling_, Oct 10 2017 [Corrected by _Michel Marcus_, Oct 25 2019]"
			],
			"mathematica": [
				"Table[Floor[Tan[(1 - 2^(-n)) Pi/2]], {n, 1, 40}] (* _Vincenzo Librandi_, Feb 26 2014 *)"
			],
			"program": [
				"(PARI) a(n) = floor(tan((1 - 2^(-n))*Pi/2)) \\\\ _Michel Marcus_, Mar 23 2013",
				"(PARI) A024810(n)=2^(n+1)\\Pi \\\\ _M. F. Hasler_, Oct 25 2019"
			],
			"xref": [
				"Cf. A172265 (partial sums)."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Clark Kimberling_",
			"ext": [
				"a(30)-a(33) corrected by _Michel Marcus_, Mar 23 2013"
			],
			"references": 2,
			"revision": 38,
			"time": "2021-07-06T02:37:41-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}