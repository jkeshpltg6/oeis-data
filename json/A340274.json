{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A340274",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 340274,
			"data": "0,0,1,1,1,1,1,1,2,1,1,3,3,2,4,2,5,2,1,2,2,2,2,4,2,4,3,3,3,4,5,2,3,5,5,4,4,2,4,4,5,3,4,3,6,3,2,5,2,2,7,7,1,3,6,4,4,3,3,6,2,5,5,3,6,5,4,6,6,6,3,6,6,4,5,6,2,6,3,5,4,5,3,5,12,4,4,5,1,6,6,7,9,3,3,6,5,6,7,4",
			"name": "Number of ways to write n as x + y + z with x, y, z positive integers such that 3*x^2*y^2 + 5*y^2*z^2 + 8*z^2*x^2 is a square.",
			"comment": [
				"Conjecture 1: a(n) \u003e 0 for all n \u003e 2.",
				"We have verified a(n) \u003e 0 for all n = 3..10000. The conjecture holds if a(p) \u003e 0 for every odd prime p. For any n \u003e 0 we have a(3*n) \u003e 0, since 3*n = n + n + n and 3 + 5 + 8 = 4^2.",
				"It seems that a(n) = 1 only for n = 3..8, 10, 11, 19, 53, 89, 127, 178, 257, 461.",
				"See also A343862 for similar conjectures.",
				"Conjecture 1 holds for all n \u003c 2^15. Note a(1823) = 1. - _Martin Ehrenstein_, May 03 2021"
			],
			"link": [
				"Martin Ehrenstein, \u003ca href=\"/A340274/b340274.txt\"\u003eTable of n, a(n) for n = 1..32767\u003c/a\u003e (first 1500 terms from Zhi-Wei Sun)",
				"Zhi-Wei Sun, \u003ca href=\"https://doi.org/10.1016/j.jnt.2016.11.008\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, J. Number Theory 175 (2017), 167-190. See also \u003ca href=\"https://arxiv.org/abs/1604.06723\"\u003earXiv version\u003c/a\u003e, arXiv:1604.06723 [math.NT], 2016-2017."
			],
			"example": [
				"a(4) = 1 with 4 = 2 + 1 + 1 and 3*2^2*1^2 + 5*1^2*1^2 + 8*1^2*2^2 = 7^2.",
				"a(19) = 1 with 19 = 9 + 9 + 1 and 3*9^2*9^2 + 5*9^2*1^2 + 8*1^2*9^2 = 144^2.",
				"a(53) = 1 with 53 = 23 + 7 + 23 and 3*23^2*7^2 + 5*7^2*23^2 + 8*23^2*23^2 = 1564^2.",
				"a(89) = 1 with 89 = 2 + 58 + 29 and 3*2^2*58^2 + 5*58^2*29^2 + 8*29^2*2^2 = 3770^2.",
				"a(257) = 1 with 257 = 11 + 164 + 82 and 3*11^2*164^2 + 5*164^2*82^2 + 8*82^2*11^2 = 30340^2.",
				"a(461) = 1 with 461 = 186 + 165 + 110 and 3*186^2*165^2 + 5*165^2*110^2 + 8*110^2*186^2 = 88440^2."
			],
			"mathematica": [
				"SQ[n_]:=IntegerQ[Sqrt[n]];",
				"tab={};Do[r=0;Do[If[SQ[3x^2*y^2+(n-x-y)^2*(5*y^2+8*x^2)],r=r+1],{x,1,n-2},{y,1,n-1-x}];tab=Append[tab,r],{n,1,100}];Print[tab]"
			],
			"xref": [
				"Cf. A000041, A000290, A230121, A230747, A231168, A262357, A271719, A273278, A343862."
			],
			"keyword": "nonn",
			"offset": "1,9",
			"author": "_Zhi-Wei Sun_, Apr 24 2021",
			"references": 5,
			"revision": 44,
			"time": "2021-05-04T07:37:56-04:00",
			"created": "2021-05-02T12:04:48-04:00"
		}
	]
}