{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A195989",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 195989,
			"data": "1,2,3,4,1,6,1,8,9,10,1,12,1,2,3,16,1,18,1,20,21,2,1,24,1,2,27,4,1,30,1,32,3,2,1,36,1,2,3,40,1,42,1,4,9,2,1,48,1,50,3,4,1,54,11,8,3,2,1,60,1,2,63,64,1,6,1,4,3,10,1,72,1,2,3,4,1,78,1,80,81,2,1,84",
			"name": "Quotient of denominators of (BernoulliB(2n)/n) and BernoulliB(2n).",
			"comment": [
				"The fixed points (entries equal to their index) are 1, 2, 3, 4, 6, 8, 9, 10, 12, 16, 18, 20, 21, 24, 27, 30, 32, 36, 40, 42,... See A193267.",
				"Are the indices of the 1's, that is 1, 5, 7, 11, 13,... , the sequence A069040 (checked to be true for their first 700 entries)? This provides another link between the Bernoulli numbers.",
				"a(10*k) = 10, 20, 30, 40, 50, 60, 10, 70, 80, 90, 100,... for k= 1, 2, 3,...."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A195989/b195989.txt\"\u003eTable of n, a(n) for n = 1..2000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A193267(2*n)/2 = A036283(n) / A002445(n).",
				"a(n) = n/A300711(n). - _Peter Luschny_, Mar 12 2018",
				"2a(n) is the product over all prime powers p^e, where p^e is the highest power of p dividing 2n and p-1 divides 2n. - _Peter Luschny_, Mar 12 2018"
			],
			"example": [
				"a(1) = 6/6 =1, a(2) = 60/30 =2, a(3) =126/42 =3, a(4) = 120/30 =4, a(5) = 66/66 =1."
			],
			"maple": [
				"A195989 := proc(n)",
				"    q1 := denom(bernoulli(2*n)/n) ;",
				"    q2 := denom(bernoulli(2*n)) ;",
				"    q1/q2 ;",
				"end proc: # _R. J. Mathar_, Jan 06 2013",
				"# Alternatively, without Bernoulli numbers:",
				"A195989 := proc(n) local P, F, f, divides; divides := (a,b) -\u003e is(irem(b,a) = 0):",
				"P := 1; F := ifactors(2*n)[2]; for f in F do if not divides(f[1]-1, 2*n) then",
				"P := P*f[1]^f[2] fi od; n/P end: seq(A195989(n),n=1..84); # _Peter Luschny_, Mar 12 2018"
			],
			"mathematica": [
				"a[n_] := Denominator[ BernoulliB[2*n]/n] / Denominator[ BernoulliB[2*n]]; Table[a[n], {n, 1, 84}] (* _Jean-François Alcover_, Jan 04 2013 *)"
			],
			"program": [
				"(PARI) a(n) = my(b=bernfrac(2*n)); denominator(b/n)/denominator(b); \\\\ _Michel Marcus_, Mar 12 2018",
				"(MAGMA) [Denominator(Bernoulli(2*n)/n)/Denominator(Bernoulli(2*n)): n in [1..100]]; // _Vincenzo Librandi_, Mar 12 2018"
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Paul Curtz_, Dec 21 2012",
			"references": 3,
			"revision": 40,
			"time": "2018-03-12T07:33:58-04:00",
			"created": "2013-01-07T02:58:58-05:00"
		}
	]
}