{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A198895",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 198895,
			"data": "1,1,1,1,2,1,1,4,5,2,1,8,18,16,5,1,16,58,88,61,16,1,32,179,416,479,272,61,1,64,543,1824,3111,2880,1385,272,1,128,1636,7680,18270,24576,19028,7936,1385,1,256,4916,31616,101166,185856,206276",
			"name": "Triangle of coefficients arising in expansion of n-th derivative of tan(x) + sec(x).",
			"comment": [
				"From _Petros Hadjicostas_, Aug 10 2019: (Start)",
				"The recurrence about T(n, k) and the equation that connects T(n, k) to P(n, k) = A059427(n,k), which are given below, appear on p. 159 of the book by David and Barton (1962). The initial conditions, however, for their triangular array S^*_{N,t} are slightly different, but there is an agreement starting at t = k = 1. They do not provide tables for S^*_{N,t) (that matches the current array T(n, k) for N = n \u003e= 0 and t = k \u003e= 1).",
				"Despite the slightly different initial conditions between T(n, k) and S^*_{N,t} (from p. 159 in the book), the recurrence given below can be proved very easily from the recurrence for the row polynomials R_n(x) given in Shi-Mei Ma (2011, 2012).",
				"(End)"
			],
			"reference": [
				"Florence Nightingale David and D. E. Barton, Combinatorial Chance, Charles Griffin, 1962; see pp. 159-162."
			],
			"link": [
				"Shi-Mei Ma, \u003ca href=\"http://arxiv.org/abs/1106.5781\"\u003eDerivative polynomials and permutations by numbers of interior peaks and left peaks\u003c/a\u003e, arXiv:1106.5781 [math.CO], 2011.",
				"Shi-Mei Ma, \u003ca href=\"https://doi.org/10.1016/j.disc.2011.10.003\"\u003eDerivative polynomials and enumeration of permutations by number of interior and left peaks \u003c/a\u003e, Discrete Mathematics 312(2) (2012), 405-412.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Florence_Nightingale_David\"\u003eFlorence Nightingale David\u003c/a\u003e."
			],
			"formula": [
				"n-th row represents the coefficients of the polynomial R_n(x) defined by the recurrence: R_0(x) = 1, R_1(x) = 1 + x, and for n \u003e= 1, R_{n+1}(x) = (1 + n*x^2)*R_n(x) + x*(1 - x^2)*R'_n(x).",
				"From _Petros Hadjicostas_, Aug 10 2019: (Start)",
				"T(n, k) = (k + 1) * T(n-1, k) + (n - k + 1) * T(n-1, k-2) for n \u003e= 0 and 2 \u003c= k \u003c= n with initial conditions T(n, k=0) = 1 for n \u003e= 0, T(n, k=1) = 2^(n-1) for n \u003e= 1, and T(n, k) = 0 for n \u003c 0 or n \u003c k.",
				"Setting x = 1 in the equation R_{n+1}(x) = (1 + n*x^2)*R_n(x) + x*(1 - x^2)*R'_n(x) (valid for n \u003e= 1), we get R_{n+1}(1) = (n + 1)*R_n(1) for n \u003e= 1. Since R_1(1) = 2, we have that R_n(1) = 2*n! for n \u003e= 1. Since also R_0(1) = 1, we conclude that Sum_{k = 0..n} T(n,k) = R_n(1) = 2*n! - 0^n = A098558(n) for n \u003e= 0.",
				"Let P(n, k) = A059427(n,k) with P(n, k) = 0 for n \u003c= 1 or n \u003c= k. Then T(n, k) = (1/2)*P(n, k-1) + P(n, k) + (1/2) * P(n, k+1) for n \u003e= 2 and 0 \u003c= k \u003c= n (but this is not true for n = 0 and n = 1).",
				"(End)"
			],
			"example": [
				"Triangle T(n,k) (with rows n \u003e= 0 and columns k \u003e= 0) begins as follows:",
				"  1",
				"  1   1",
				"  1   2    1",
				"  1   4    5     2",
				"  1   8   18    16      5",
				"  1  16   58    88     61     16",
				"  1  32  179   416    479    272     61",
				"  1  64  543  1824   3111   2880   1385    272",
				"  1 128 1636  7680  18270  24576  19028   7936  1385",
				"  1 256 4916 31616 101166 185856 206276 137216 50521 7936",
				"  ..."
			],
			"xref": [
				"Cf. A059427, A098558 (row sums), A000111 (diagonal and 1st subdiagonal), A000340 (column 3) A000431 (column 4), A000363 (column 5)"
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_, Oct 31 2011",
			"ext": [
				"More terms from _Max Alekseyev_, Feb 17 2012"
			],
			"references": 0,
			"revision": 42,
			"time": "2019-08-11T02:35:37-04:00",
			"created": "2011-10-31T00:58:07-04:00"
		}
	]
}