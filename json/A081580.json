{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A081580",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 81580,
			"data": "1,1,1,1,7,1,1,13,13,1,1,19,61,19,1,1,25,145,145,25,1,1,31,265,595,265,31,1,1,37,421,1585,1585,421,37,1,1,43,613,3331,6145,3331,613,43,1,1,49,841,6049,17401,17401,6049,841,49,1,1,55,1105,9955,40105,65527,40105,9955,1105,55,1",
			"name": "Pascal-(1,5,1) array.",
			"comment": [
				"One of a family of Pascal-like arrays. A007318 is equivalent to the (1,0,1)-array. A008288 is equivalent to the (1,1,1)-array. Rows include A016921, A081589, A081590. Coefficients of the row polynomials in the Newton basis are given by A013613."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A081580/b081580.txt\"\u003eRows n = 0..100, flattened\u003c/a\u003e",
				"Paul Barry, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL9/Barry/barry91.html\"\u003eOn Integer-Sequence-Based Constructions of Generalized Pascal Triangles\u003c/a\u003e, Journal of Integer Sequences, Vol. 9 (2006), Article 06.2.4."
			],
			"formula": [
				"Square array T(n, k) defined by T(n, 0) = T(0, k) = 1, T(n, k) = T(n, k-1) + 5*T(n-1, k-1) + T(n-1, k).",
				"Rows are the expansions of (1+5*x)^k/(1-x)^(k+1).",
				"From _Paul Barry_, Aug 28 2008: (Start)",
				"Number triangle T(n,k) = Sum_{j=0..n-k} binomial(n-k,j)*binomial(k,j)*5^j.",
				"Riordan array (1/(1-x), x*(1+5*x)/(1-x)). (End)",
				"T(n, k) = Hypergeometric2F1([-k, k-n], [1], 6). - _Jean-François Alcover_, May 24 2013",
				"E.g.f. for the n-th subdiagonal, n = 0,1,2,..., equals exp(x)*P(n,x), where P(n,x) is the polynomial Sum_{k = 0..n} binomial(n,k)*(6*x)^k/k!. For example, the e.g.f. for the second subdiagonal is exp(x)*(1 + 12*x + 36*x^2/2) = 1 + 13*x + 61*x^2/2! + 145*x^3/3! + 265*x^4/4! + 421*x^5/5! + .... - _Peter Bala_, Mar 05 2017",
				"Sum_{k=0..n} T(n, k, 3) = A002532(n+1). - _G. C. Greubel_, May 26 2021"
			],
			"example": [
				"Square array begins as:",
				"  1,  1,   1,    1,    1, ... A000012;",
				"  1,  7,  13,   19,   25, ... A016921;",
				"  1, 13,  61,  145,  265, ... A081589;",
				"  1, 19, 145,  595, 1585, ... A081590;",
				"  1, 25, 265, 1585, 6145, ...",
				"The triangle begins as:",
				"  1;",
				"  1,  1;",
				"  1,  7,    1;",
				"  1, 13,   13,    1;",
				"  1, 19,   61,   19,     1;",
				"  1, 25,  145,  145,    25,     1;",
				"  1, 31,  265,  595,   265,    31,     1;",
				"  1, 37,  421, 1585,  1585,   421,    37,    1;",
				"  1, 43,  613, 3331,  6145,  3331,   613,   43,    1;",
				"  1, 49,  841, 6049, 17401, 17401,  6049,  841,   49,  1;",
				"  1, 55, 1105, 9955, 40105, 65527, 40105, 9955, 1105, 55, 1; - _Philippe Deléham_, Mar 15 2014"
			],
			"mathematica": [
				"Table[Hypergeometric2F1[-k, k-n, 1, 6], {n,0,10}, {k,0,n}]//Flatten (* _Jean-François Alcover_, May 24 2013 *)"
			],
			"program": [
				"(MAGMA)",
				"A081580:= func\u003c n,k,q | (\u0026+[Binomial(k, j)*Binomial(n-j, k)*q^j: j in [0..n-k]]) \u003e;",
				"[A081580(n,k,5): k in [0..n], n in [0..12]]; // _G. C. Greubel_, May 26 2021",
				"(Sage) flatten([[hypergeometric([-k, k-n], [1], 6).simplify() for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, May 26 2021"
			],
			"xref": [
				"Cf. Pascal (1,m,1) array: A123562 (m = -3), A098593 (m = -2), A000012 (m = -1), A007318 (m = 0), A008288 (m = 1), A081577 (m = 2), A081578 (m = 3), A081579 (m = 4), A081581 (m = 6), A081582 (m = 7), A143683 (m = 8).",
				"Cf. A002532, A016921, A081589, A081590."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,5",
			"author": "_Paul Barry_, Mar 23 2003",
			"references": 12,
			"revision": 27,
			"time": "2021-05-27T01:54:55-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}