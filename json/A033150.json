{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A033150",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 33150,
			"data": "1,7,0,5,2,1,1,1,4,0,1,0,5,3,6,7,7,6,4,2,8,8,5,5,1,4,5,3,4,3,4,5,0,8,1,6,0,7,6,2,0,2,7,6,5,1,6,5,3,4,6,9,0,9,9,9,9,4,2,8,4,9,0,6,5,4,7,3,1,3,1,9,2,1,6,8,1,2,2,4,9,1,9,3,4,2,4,4,1,3,2,1,0,0,8,7,1,0,0,1,7,9",
			"name": "Decimal expansion of Niven's constant.",
			"comment": [
				"This constant is the average value of A051903. - _Charles R Greathouse IV_, Oct 30 2012",
				"There are no 9's in the first 50 digits after the decimal point. Then, suddenly, it goes 909999. - _Bobby Jacobs_, Aug 13 2017",
				"Named after the Canadian-American mathematician Ivan Morton Niven (1915 - 1999). - _Amiram Eldar_, Aug 19 2020"
			],
			"reference": [
				"Steven R. Finch, Mathematical Constants, Cambridge University Press, 2003, pp. 112-115."
			],
			"link": [
				"C. W. Anderson, \u003ca href=\"http://www.jstor.org/stable/2319672\"\u003eProblem 6015\u003c/a\u003e, The American Mathematical Monthly, Vol. 82, No. 2 (1975), pp. 183-184, T. Salat, \u003ca href=\"http://www.jstor.org/stable/2318702\"\u003ePrime Decomposition of Integers, solution to Problem 6015\u003c/a\u003e, ibid., Vol. 83, No. 10 (1976), p. 820.",
				"Ivan Niven, \u003ca href=\"https://doi.org/10.1090/S0002-9939-1969-0241373-5\"\u003eAverages of Exponents in Factoring Integers\u003c/a\u003e, Proc. Amer. Math. Soc., Vol. 22, No. 2 (1969), pp. 356-360.",
				"Simon Plouffe, \u003ca href=\"http://www.plouffe.fr/simon/constants/niven.txt\"\u003eThe Niven constant to 256 digits\u003c/a\u003e.",
				"Kaneenika Sinha, \u003ca href=\"https://www.iiserkol.ac.in/~kaneenika/maxnrevfinal.pdf\"\u003eAverage orders of certain arithmetical functions\u003c/a\u003e, Journal of the Ramanujan Mathematical Society, Vol. 21, No. 3 (2006), pp. 267-277.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/NivensConstant.html\"\u003eNiven's Constant\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Niven%27s_constant\"\u003eNiven's constant\u003c/a\u003e."
			],
			"formula": [
				"Equals 1 + Sum_{j\u003e=2} 1-(1/zeta(j)).",
				"Equals 1 - Sum_{k\u003e=2} mu(k)/(k*(k-1)), where mu is the Möbius function (A008683) (Anderson, 1975; Sinha, 2006). - _Amiram Eldar_, Aug 19 2020"
			],
			"example": [
				"1.7052111401..."
			],
			"mathematica": [
				"rd[n_] := rd[n] = RealDigits[ N[1 + Sum[1 - 1/Zeta[j], {j, 2, 2^n}] , 105]][[1]]; rd[n = 4]; While[rd[n] =!= rd[n-1], n++]; rd[n] (* _Jean-François Alcover_, Oct 25 2012 *)"
			],
			"program": [
				"(PARI) 1+suminf(j=2,1-1/zeta(j)) \\\\ _Charles R Greathouse IV_, Aug 13 2017"
			],
			"xref": [
				"Cf. A008683, A033151, A033152, A033153, A033154."
			],
			"keyword": "nonn,cons",
			"offset": "1,2",
			"author": "_Eric W. Weisstein_",
			"ext": [
				"Offset corrected by Oleg Marichev (oleg(AT)wolfram.com), Jan 28 2008"
			],
			"references": 14,
			"revision": 35,
			"time": "2020-08-19T06:48:08-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}