{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A254027",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 254027,
			"data": "0,2,-1,8,1,-3,26,7,-1,-7,80,25,5,-5,-15,242,79,23,1,-13,-31,728,241,77,19,-7,-29,-63,2186,727,239,73,11,-23,-61,-127,6560,2185,725,235,65,-5,-55,-125,-255,19682,6559,2183,721,227,49,-37,-119,-253,-511,59048,19681,6557,2179,713,211,17,-101,-247,-509,-1023",
			"name": "Table T(n,k) = 3^n - 2^k read by antidiagonals.",
			"comment": [
				"Table shows differences of a given power of 3 to the powers of 2 (columns), and differences of the powers of 3 to a given power of 2 (rows), respectively.",
				"Note that positive terms (table's upper right area) and negative terms (lower left area) are separated by an imaginary line with slope -log(3)/log(2) = -1.5849625.. (see A020857). This \"border zone\" of the table is of interest in terms of how close powers of 3 and powers of 2 can get: i.e., those T(n,k) where k/n is a good rational approximation to log(3)/log(2), see A254351 for numerators k and respective A060528 for denominators n."
			],
			"link": [
				"K. G. Stier, \u003ca href=\"/A254027/b254027.txt\"\u003eTable of n, a(n) for n = 0..5150\u003c/a\u003e"
			],
			"example": [
				"Table begins",
				"   0    2   8  26  80..",
				"  -1    1   7  25  79..",
				"  -3   -1   5  23  73..",
				"  -7   -5   1  19  65..",
				"  -15 -13  -7  11  49..",
				"  ..   ..  ..  ..  .."
			],
			"mathematica": [
				"Table[3^(n-k) - 2^k, {n, 0, 10}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Dec 18 2017 *)"
			],
			"program": [
				"(PARI) for(i=0, 10, {",
				"     for(j=0, i, print1((3^(i-j)-2^j),\", \"))",
				"});"
			],
			"xref": [
				"Row 0 (=3^n-1) is A024023.",
				"Row 1 (=3^n-2) is A058481.",
				"Row 2 (=3^n-4) is A168611.",
				"Column 0 (=1-2^n) is (-1)A000225.",
				"Column 1 (=3-2^n) is (-1)A036563.",
				"Column 2 (=9-2^n) is (-1)A185346.",
				"Column 3 (=27-2^n) is (-1)A220087.",
				"0,0-Diagonal (=3^n-2^n) is A001047.",
				"1,0-Diagonal (=3^n-2^(n-1)) for n\u003e0 is A083313 or A064686.",
				"0,1-Diagonal (=3^n-2^(n+1)) is A003063.",
				"0,2-Diagonal (=3^n-2^(n+2)) is A214091."
			],
			"keyword": "sign,tabl",
			"offset": "0,2",
			"author": "_K. G. Stier_, Jan 22 2015",
			"references": 1,
			"revision": 26,
			"time": "2017-12-18T19:46:40-05:00",
			"created": "2015-02-23T18:53:26-05:00"
		}
	]
}