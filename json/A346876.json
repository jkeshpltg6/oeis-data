{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A346876",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 346876,
			"data": "4,1,1,15,5,3,2,1,1,1,249,83,42,25,17,13,9,7,6,5,5,3,4,2,3,2,2,2,2,2,1,2,1,2,1,1,1,1,1,1,1,4065,1355,678,407,271,194,146,113,91,75,62,52,45,40,34,30,27,25,22,19,19,16,15,14,13,12,12,10,10,9,9,8,8,7",
			"name": "Irregular triangle read by rows in which row n lists the row n-th even perfect number of A237591, n \u003e= 1.",
			"comment": [
				"The characteristic shape of the symmetric representation of sigma(A000396(n)) consists in that the diagram has only one region (or part) and that region has whidth 1 except in the main diagonal where the width is 2.",
				"So knowing this characteristic shape we can know if a number is a even perfect number (or not) just by looking at the diagram, even ignoring the concept of even perfect number (see the examples).",
				"Therefore we can see a geometric pattern of the distribution of the even perfect numbers in the stepped pyramid described in A245092.",
				"For the definition of \"width\" see A249351.",
				"T(n,k) is the length of the k-th line segment of the largest Dyck path of the symmetric representation of sigma(A000396(n)), from the border to the center, hence the sum of the n-th row of triangle is equal to A000396(n) assuming there are no odd perfect numbers.",
				"T(n,k) is also the difference between the total number of partitions of all positive integers \u003c= n-th even perfect number into exactly k consecutive parts, and the total number of partitions of all positive integers \u003c= n-th perfect number into exactly k + 1 consecutive parts."
			],
			"link": [
				"Michel Marcus, \u003ca href=\"/A346876/b346876.txt\"\u003eTable of n, a(n) for n = 1..8359\u003c/a\u003e (rows 1..5)."
			],
			"example": [
				"Triangle begins:",
				"    4, 1, 1;",
				"   15, 5, 3, 2, 1, 1,1;",
				"  249,83,42,25,17,13,9,7,6,5,5,3,4,2,3,2,2,2,2,2,1,2,1,2,1,1,1,1,1,1,1;",
				"...",
				"Illustration of initial terms:",
				"Column P gives the even perfect numbers (A000396 assuming there are no odd perfect numbers).",
				"Column S gives A139256, the sum of the divisors of the even perfect numbers equals the area (and the number of cells) of the associated diagram.",
				"-------------------------------------------------------------------------",
				"  n    P   S    Diagram:   1                                           2",
				"-------------------------------------------------------------------------",
				"                           _                                           _",
				"                          | |                                         | |",
				"                          | |                                         | |",
				"                       _ _| |                                         | |",
				"                      |    _|                                         | |",
				"                 _ _ _|  _|                                           | |",
				"  1    6   12   |_ _ _ _| 1                                           | |",
				"                    4    1                                            | |",
				"                                                                      | |",
				"                                                                      | |",
				"                                                                      | |",
				"                                                                      | |",
				"                                                                      | |",
				"                                                                      | |",
				"                                                             _ _ _ _ _| |",
				"                                                            |  _ _ _ _ _|",
				"                                                            | |",
				"                                                         _ _| |",
				"                                                     _ _|  _ _|",
				"                                                    |    _|",
				"                                                   _|  _|",
				"                                                  |  _|1 1",
				"                                             _ _ _| | 1",
				"                                            |  _ _ _|2",
				"                                            | |  3",
				"                                            | |",
				"                                            | |5",
				"                 _ _ _ _ _ _ _ _ _ _ _ _ _ _| |",
				"  2   28   56   |_ _ _ _ _ _ _ _ _ _ _ _ _ _ _|",
				"                              15",
				".",
				"For n = 3, P = 496, the diagram is too large to include here. To draw that diagram note that the lengths of the line segments of the smallest Dyck path are [248, 83, 42, 25, 17, 13, 9, 7, 6, 5, 5, 3, 4, 2, 3, 2, 2, 2, 2, 2, 1, 2, 1, 2, 1, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 2, 1, 2, 1, 2, 2, 2, 2, 2, 3, 2, 4, 3, 5, 5, 6, 7, 9, 13, 17, 25, 42, 83, 248] and the lengths of the line segments of the largest Dyck path are [249, 83, 42, 25, 17, 13, 9, 7, 6, 5, 5, 3, 4, 2, 3, 2, 2, 2, 2, 2, 1, 2, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 2, 1, 2, 2, 2, 2, 2, 3, 2, 4, 3, 5, 5, 6, 7, 9, 13, 17, 25, 42, 83, 249]."
			],
			"program": [
				"(PARI) row235791(n) = vector((sqrtint(8*n+1)-1)\\2, i, 1+(n-(i*(i+1)/2))\\i);",
				"row(n) = {my(orow = concat(row235791(n), 0)); vector(#orow -1, i, orow[i] - orow[i+1]); } \\\\ A237591",
				"tabf(nn) = {for (n=1, nn, my(p=prime(n)); if (isprime(2^n-1), print(row(2^(n-1)*(2^n-1)));););}",
				"tabf(7) \\\\ _Michel Marcus_, Aug 31 2021"
			],
			"xref": [
				"Row sums give A000396.",
				"Row lengths give A000668.",
				"For the characteristic shape of sigma(A000040(n)) see A346871.",
				"For the characteristic shape of sigma(A000079(n)) see A346872.",
				"For the characteristic shape of sigma(A000217(n)) see A346873.",
				"For the visualization of Mersenne numbers A000225 see A346874.",
				"For the characteristic shape of sigma(A000384(n)) see A346875.",
				"For the characteristic shape of sigma(A008588(n)) see A224613.",
				"Cf. A000203, A139256, A237591, A237593, A245092, A249351, A262626."
			],
			"keyword": "nonn,tabf",
			"offset": "1,1",
			"author": "_Omar E. Pol_, Aug 06 2021",
			"ext": [
				"More terms from _Michel Marcus_, Aug 31 2021"
			],
			"references": 8,
			"revision": 49,
			"time": "2021-12-10T11:19:43-05:00",
			"created": "2021-08-17T10:13:19-04:00"
		}
	]
}