{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A022170",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 22170,
			"data": "1,1,1,1,7,1,1,43,43,1,1,259,1591,259,1,1,1555,57535,57535,1555,1,1,9331,2072815,12485095,2072815,9331,1,1,55987,74630671,2698853335,2698853335,74630671,55987,1,1,335923",
			"name": "Triangle of Gaussian binomial coefficients [ n,k ] for q = 6.",
			"reference": [
				"F. J. MacWilliams and N. J. A. Sloane, The Theory of Error-Correcting Codes, Elsevier-North Holland, 1978, p. 698."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A022170/b022170.txt\"\u003eRows n=0..50 of triangle, flattened\u003c/a\u003e",
				"R. Mestrovic, \u003ca href=\"http://arxiv.org/abs/1409.3820\"\u003eLucas' theorem: its generalizations, extensions and applications (1878--2014)\u003c/a\u003e, arXiv preprint arXiv:1409.3820 [math.NT], 2014.",
				"Kent E. Morrison, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL9/Morrison/morrison37.html\"\u003eInteger Sequences and Matrices Over Finite Fields\u003c/a\u003e, Journal of Integer Sequences, Vol. 9 (2006), Article 06.2.1.",
				"\u003ca href=\"/index/Ga#Gaussian_binomial_coefficients\"\u003eIndex entries for sequences related to Gaussian binomial coefficients\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = T(n-1,k-1) + q^k * T(n-1,k). - _Peter A. Lawrence_, Jul 13 2017"
			],
			"example": [
				"1 ;",
				"1 1;",
				"1 7 1;",
				"1 43 43 1;",
				"1 259 1591 259 1;",
				"1 1555 57535 57535 1555 1;",
				"1 9331 2072815 12485095 2072815 9331 1;",
				"1 55987 74630671 2698853335 2698853335 74630671 55987 1 ;"
			],
			"maple": [
				"A027873 := proc(n)",
				"    mul(6^i-1,i=1..n) ;",
				"end procc:",
				"A022170 := proc(n,m)",
				"    A027873(n)/A027873(m)/A027873(n-m) ;",
				"end proc: # _R. J. Mathar_, Jul 19 2017"
			],
			"mathematica": [
				"p[n_]:= Product[6^i - 1, {i, 1, n}]; t[n_, k_]:= p[n]/(p[k]*p[n-k]); Table[t[n, k], {n, 0, 15}, {k, 0, n}]//Flatten (* _Vincenzo Librandi_, Aug 13 2016 *)",
				"Table[QBinomial[n,k,6], {n,0,10}, {k,0,n}]//Flatten (* or *) q:= 6; T[n_, 0]:= 1; T[n_,n_]:= 1; T[n_,k_]:= T[n,k] = If[k \u003c 0 || n \u003c k, 0, T[n-1, k -1] +q^k*T[n-1,k]]; Table[T[n,k], {n,0,10}, {k,0,n}] // Flatten  (* _G. C. Greubel_, May 27 2018 *)"
			],
			"program": [
				"(PARI) {q=6; T(n,k) = if(k==0,1, if (k==n, 1, if (k\u003c0 || n\u003ck, 0, T(n-1,k-1) + q^k*T(n-1,k))))};",
				"for(n=0,10, for(k=0,n, print1(T(n,k), \", \"))) \\\\ _G. C. Greubel_, May 27 2018"
			],
			"xref": [
				"Cf. A003463 (k=1), A022220 (k=2), A022221 (k=3)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_",
			"references": 16,
			"revision": 27,
			"time": "2018-05-28T03:46:19-04:00",
			"created": "1998-06-14T03:00:00-04:00"
		}
	]
}