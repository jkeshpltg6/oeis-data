{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A195467",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 195467,
			"data": "0,1,0,1,2,3,0,1,3,2,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,0,1,3,2,6,7,5,4,12,13,15,14,10,11,9,8,0,1,2,3,5,4,7,6,10,11,8,9,15,14,13,12,0,1,3,2,7,6,4,5,15,14,12,13,8,9,11,10",
			"name": "Consecutive powers of the Gray code permutation.",
			"comment": [
				"The powers of the Gray code permutation (GCP, A003188) form an infinite array, where row n is the n-th power of the GCP. Row 0 is the identity permutation (i.e., the sequence of nonnegative integers), and row 1 is the GCP itself.",
				"The different powers of the n-bit GCP form a matrix of size (A062383(n-1)) X (2^n).",
				"This sequence represents the infinite array in a somewhat redundant way: It shows the rows of all the (2^n) X (2^2^n) matrices of powers of (2^n)-bit GCP. So this sequence forms a triangle, and these 3 matrices are its first 7 rows:",
				"The 1-bit GCP is the identity permutation:",
				"0:   0  1",
				"The 2 different powers of the 2-bit GCP:",
				"0:   0  1  2  3",
				"1:   0  1  3  2",
				"The 4 different powers of the 4-bit GCP:",
				"0:   0  1  2  3   4  5  6  7   8  9 10 11  12 13 14 15",
				"1:   0  1  3  2   6  7  5  4  12 13 15 14  10 11  9  8",
				"2:   0  1  2  3   5  4  7  6  10 11  8  9  15 14 13 12",
				"3:   0  1  3  2   7  6  4  5  15 14 12 13   8  9 11 10",
				".",
				"This array A can be defined using the binary array B = A197819 by",
				"A  =  B + 2 * 2stretched(B) + 4 * 4stretched(B) + 8 * 8stretched(B) + ...",
				"where nstretched has the following meaning:",
				"2stretched(1,2,3,4,...) = 1,1,2,2,3,3,4,4,...",
				"4stretched(1,2,3,4,...) = 1,1,1,1,2,2,2,2,3,3,3,3,4,4,4,4,...",
				"etc."
			],
			"link": [
				"Tilman Piesk, \u003ca href=\"/A195467/b195467.txt\"\u003eFirst 15 rows of the triangle, flattened\u003c/a\u003e",
				"Tilman Piesk,  \u003ca href=\"/A195467/a195467_7.txt\"\u003eExplanations\u003c/a\u003e (including the 8x256 submatrix) and \u003ca href=\"/A195467/a195467_5.txt\"\u003eMATLAB code\u003c/a\u003e showing the connection with A197819",
				"Tilman Piesk, \u003ca href=\"/A195467/a195467_1.txt\"\u003eThe 8 different powers of the 6-bit Gray code permutation\u003c/a\u003e",
				"Tilman Piesk, \u003ca href=\"http://en.wikiversity.org/wiki/Gray_code_permutation_powers\"\u003eGray code permutation powers\u003c/a\u003e (Wikiversity)"
			],
			"xref": [
				"Cf. A003188 (Gray code permutation).",
				"Cf. A006068 (inverse of the Gray code permutation).",
				"Cf. A064706 (square of the Gray code permutation).",
				"Cf. A197819 (this array mod 2)."
			],
			"keyword": "nonn,tabf",
			"offset": "0,5",
			"author": "_Tilman Piesk_, Sep 23 2011",
			"ext": [
				"Huge edit by _Tilman Piesk_, Aug 25 2013"
			],
			"references": 3,
			"revision": 84,
			"time": "2017-12-16T23:29:41-05:00",
			"created": "2011-10-13T14:19:06-04:00"
		}
	]
}