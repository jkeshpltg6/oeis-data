{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A033999",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 33999,
			"data": "1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1",
			"name": "a(n) = (-1)^n.",
			"comment": [
				"(-1)^(n+1) = signed area of parallelogram with vertices (0,0), U=(F(n),F(n+1)), V=(F(n+1),F(n+2)), where F = A000045 (Fibonacci numbers). The area of every such parallelogram is 1. The signed area is -1 if and only if F(n+1)^2 \u003e F(n)*F(n+2), or, equivalently, n is even, or, equivalently, the vector U is \"above\" V, indicating that U and V \"cross\" as n -\u003e n+1. - _Clark Kimberling_, Sep 09 2013",
				"Periodic with period length 2. - _Ray Chandler_, Apr 03 2017"
			],
			"link": [
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/2001.08799\"\u003eCharacterizations of the Borel triangle and Borel polynomials\u003c/a\u003e, arXiv:2001.08799 [math.CO], 2020.",
				"F. Javier de Vega, \u003ca href=\"https://arxiv.org/abs/2003.13378\"\u003eAn extension of Furstenberg's theorem of the infinitude of primes\u003c/a\u003e, arXiv:2003.13378 [math.NT], 2020.",
				"S. K. Ghosal and J. K. Mandal, \u003ca href=\"http://dx.doi.org/10.1016/j.protcy.2013.12.341\"\u003eStirling Transform Based Color Image Authentication\u003c/a\u003e, Procedia Technology, 2013 Volume 10, 2013, Pages 95-104.",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"László Németh, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL21/Nemeth/nemeth6.html\"\u003eThe trinomial transform triangle\u003c/a\u003e, J. Int. Seqs., Vol. 21 (2018), Article 18.7.3. Also \u003ca href=\"https://arxiv.org/abs/1807.07109\"\u003earXiv:1807.07109\u003c/a\u003e [math.NT], 2018.",
				"Michael Somos, \u003ca href=\"http://cis.csuohio.edu/~somos/rfmc.txt\"\u003eRational Function Multiplicative Coefficients\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/InverseTangent.html\"\u003eInverse Tangent\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/StirlingTransform.html\"\u003eStirling Transform\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Grandi%27s_series\"\u003eGrandi's series\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/%C2%B11-sequence\"\u003e+/-1-sequence\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Dirichlet_eta_function\"\u003eDirichlet eta function\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_01\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (-1).",
				"\u003ca href=\"/index/Pol#poly_cyclo_inv\"\u003eIndex to sequences related to inverse of cyclotomic polynomials\u003c/a\u003e"
			],
			"formula": [
				"G.f.: 1/(1+x).",
				"E.g.f.: exp(-x).",
				"Linear recurrence: a(0)=1, a(n)=-a(n-1) for n\u003e0. - _Jaume Oliver Lafont_, Mar 20 2009",
				"Sum_{k=0..n} a(k) = A059841(n). - _Jaume Oliver Lafont_, Nov 21 2009",
				"Sum_{k\u003e=0} a(k)/(k+1) = log(2). - _Jaume Oliver Lafont_, Mar 30 2010",
				"Euler transform of length 2 sequence [ -1, 1]. - _Michael Somos_, Mar 21 2011",
				"Moebius transform is length 2 sequence [ -1, 2]. - _Michael Somos_, Mar 21 2011",
				"a(n) = -b(n) where b(n) = multiplicative with b(2^e) = -1 if e\u003e0, b(p^e) = 1 if p\u003e2. - _Michael Somos_, Mar 21 2011",
				"a(n) = a(-n) = a(n + 2) = cos(n * Pi). a(n) = c_2(n) if n\u003e1 where c_k(n) is Ramanujan's sum. - _Michael Somos_, Mar 21 2011",
				"a(n) = (1/2)*Product_{k=0..2*n-1} 2*cos((2*k+1)*Pi/(4*n)), n \u003e= 1. See the product given in the Oct 21 2013 formula comment in A056594, and replace there n -\u003e 2*n. - _Wolfdieter Lang_, Oct 23 2013",
				"D.g.f.: (2^(1-s)-1)*zeta(s) = -eta(s) (the Dirichlet eta function). - _Ralf Stephan_, Mar 27 2015",
				"From _Ilya Gutkovskiy_, Aug 17 2016: (Start)",
				"a(n) = T_n(-1), where T_n(x) are the Chebyshev polynomials of the first kind.",
				"Binomial transform of A122803. (End)",
				"a(n) = exp(i*Pi*n) = exp(-i*Pi*n). - _Carauleanu Marc_, Sep 15 2016",
				"a(n) = Sum_{k=0..n} (-1)^k*A063007(n, k), n \u003e= 0. - _Wolfdieter Lang_, Sep 13 2016"
			],
			"example": [
				"G.f. = 1 - x + x^2 - x^3 + x^4 - x^5 + x^6 - x^7 + x^8 - x^9 + x^10 - x^11 + x^12 + ..."
			],
			"maple": [
				"A033999 := n-\u003e(-1)^n: seq(A033999(n), n=0..100);"
			],
			"mathematica": [
				"Table[(-1)^n, {n, 0, 88}] (* _Alonso del Arte_, Nov 30 2009 *)",
				"PadRight[{}, 89, {1, -1}] (* _Arkadiusz Wesolowski_, Sep 16 2012 *)"
			],
			"program": [
				"(PARI) a(n)=1-2*(n%2) /* _Jaume Oliver Lafont_, Mar 20 2009 */",
				"(Haskell)",
				"a033999 = (1 -) . (* 2) . (`mod` 2)",
				"a033999_list = cycle [1,-1] -- _Reinhard Zumkeller_, May 06 2012, Jan 02 2012",
				"(MAGMA) [(-1)^n : n in [0..100]]; // _Wesley Ivan Hurt_, Nov 19 2014"
			],
			"xref": [
				"Cf. A056594, A059841, A063007, A122803."
			],
			"keyword": "sign,easy",
			"offset": "0,1",
			"author": "Vasiliy Danilov (danilovv(AT)usa.net), Jun 15 1998",
			"references": 135,
			"revision": 146,
			"time": "2021-06-11T09:11:36-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}