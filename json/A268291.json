{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A268291",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 268291,
			"data": "0,1,3,6,10,15,21,28,36,45,55,66,78,78,79,81,84,88,93,99,106,114,123,133,144,156,156,157,159,162,166,171,177,184,192,201,211,222,234,234,235,237,240,244,249,255,262,270,279,289,300,312,312,313,315,318,322,327,333,340,348",
			"name": "a(n) = Sum_{k = 0..n} (k mod 13).",
			"comment": [
				"More generally, the ordinary generating function for the Sum_{k = 0..n} (k mod m) is (Sum_{k = 1..(m - 1)} k*x^k)/((1 - x^m)*(1 - x)).",
				"Sum_{k = 0..n} (k mod m) = m*(m - 1)/2 + Sum_{k = 1..(m - 1)} k*floor((n - k)/m), m\u003e0."
			],
			"link": [
				"Shawn A. Broyles, \u003ca href=\"/A268291/b268291.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Ilya Gutkovskiy, \u003ca href=\"/A268291/a268291.pdf\"\u003eExtended example\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_14\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,0,0,0,0,0,0,0,0,0,0,0,1,-1)."
			],
			"formula": [
				"G.f.: (x + 2*x^2 + 3*x^3 + 4*x^4 + 5*x^5 + 6*x^6 + 7*x^7 + 8*x^8 + 9*x^9 + 10*x^10 + 11*x^11 + 12*x^12)/((1 - x^13)*(1 - x)).",
				"a(n) = 12*floor((n - 12)/13) + 11*floor((n - 11)/13) + 10*floor((n - 10)/13) + 9*floor((n - 9)/13) + 8*floor((n - 8)/13) + 7*floor((n - 7)/13) + 6*floor((n - 6)/13) + 5*floor((n - 5)/13) + 4*floor((n - 4)/13) + 3*floor((n - 3)/13) + 2*floor((n - 2)/13) + floor((n - 1)/13) + 78."
			],
			"example": [
				"(see Extended example in Links section)",
				"a(0)  = 0;",
				"a(1)  = 0+1 = 1;",
				"a(2)  = 0+1+2 = 3;",
				"a(3)  = 0+1+2+3 = 6;",
				"a(4)  = 0+1+2+3+4 = 10;",
				"a(5)  = 0+1+2+3+4+5 = 15;",
				"...",
				"a(11) = 0+1+2+3+4+5+6+7+8+9+10+11 = 66;",
				"a(12) = 0+1+2+3+4+5+6+7+8+9+10+11+12 = 78;",
				"a(13) = 0+1+2+3+4+5+6+7+8+9+10+11+12+0 = 78;",
				"a(14) = 0+1+2+3+4+5+6+7+8+9+10+11+12+0+1 = 79;",
				"a(15) = 0+1+2+3+4+5+6+7+8+9+10+11+12+0+1+2 = 81, etc."
			],
			"mathematica": [
				"Table[Sum[Mod[k, 13], {k, 0, n}], {n, 0, 60}]",
				"Table[Sum[k - 13 Floor[k/13], {k, 0, n}], {n, 0, 60}]",
				"LinearRecurrence[{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, -1}, {0, 1, 3, 6, 10, 15, 21, 28, 36, 45, 55, 66, 78, 78}, 61]",
				"CoefficientList[Series[(x + 2 x^2 + 3 x^3 + 4 x^4 + 5 x^5 + 6 x^6 + 7 x^7 + 8 x^8 + 9 x^9 + 10 x^10 + 11 x^11 + 12 x^12) / ((1 - x^13) (1 - x)), {x, 0, 70}], x] (* _Vincenzo Librandi_, Jan 31 2016 *)",
				"Accumulate[Mod[Range[0,60],13]] (* _Harvey P. Dale_, May 10 2021 *)"
			],
			"program": [
				"(PARI) a(n) = sum(k = 0, n, k % 13); \\\\ _Michel Marcus_, Jan 31 2016"
			],
			"xref": [
				"Cf. A004526, A130481-A130490."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Ilya Gutkovskiy_, Jan 31 2016",
			"references": 1,
			"revision": 22,
			"time": "2021-05-10T10:42:58-04:00",
			"created": "2016-02-02T03:47:27-05:00"
		}
	]
}