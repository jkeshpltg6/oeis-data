{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A347286",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 347286,
			"data": "0,1,1,3,3,4,5,7,6,8,9,10,11,12,11,15,15,15,17,18,17,20,21,22,22,24,23,26,27,26,29,31,29,32,31,33,35,36,35,38,39,38,41,42,39,44,45,46,46,47,47,50,51,50,51,54,53,56,57,56,59,60,57,63,61,62,65,66,65,66",
			"name": "a(n) is n minus the number of odd divisors of n.",
			"comment": [
				"a(n) is n minus the number of partitions of n into consecutive parts.",
				"This definition is in accordance with the diagram as shown below in the Example section which also appears in many sequences related to A237048, A237591, A237593 and possible others.",
				"a(n) is also the number of zeros in the n-th row of A285898."
			],
			"formula": [
				"a(n) = n - A001227(n)."
			],
			"example": [
				"For n = 18 the divisors of 18 are 1, 2, 3, 6, 9, 18. There are three odd divisors: 1, 3, 9, so a(18) = 18 - 3 = 15.",
				"On the other hand the partitions of 18 into consecutive parts are [18], [7, 6, 5], [6, 5, 4, 3]. There are three of such partitions, so a(18) = 18 - 3 = 15.",
				"Illustration of initial terms:",
				".",
				"   n   a(n)                   Diagram                 _",
				"   1     0                                          _|x|",
				"   2     1                                        _|x _|",
				"   3     1                                      _|x  |x|",
				"   4     3                                    _|x   _| |",
				"   5     3                                  _|x    |x _|",
				"   6     4                                _|x     _| |x|",
				"   7     5                              _|x      |x  | |",
				"   8     7                            _|x       _|  _| |",
				"   9     6                          _|x        |x  |x _|",
				"  10     8                        _|x         _|   | |x|",
				"  11     9                      _|x          |x   _| | |",
				"  12    10                    _|x           _|   |x  | |",
				"  13    11                  _|x            |x    |  _| |",
				"  14    12                _|x             _|    _| |x _|",
				"  15    11              _|x              |x    |x  | |x|",
				"  16    15            _|x               _|     |   | | |",
				"  17    15          _|x                |x     _|  _| | |",
				"  18    15        _|x                 _|     |x  |x  | |",
				"  19    17      _|x                  |x      |   |  _| |",
				"  20    18    _|x                   _|      _|   | |x _|",
				"  21    17   |x                    |x      |x    | | |x|",
				"...",
				"In the above diagram the number of x's in row n equals A001227(n), the number of partitions n into consecutive parts.",
				"a(n) is the number of square cells in row n that do not contain a \"x\".",
				"In other words: a(n) is the number of square cells in row n that do not have a horizontal line segment above."
			],
			"mathematica": [
				"a[n_] := n - DivisorSigma[0, n/2^IntegerExponent[n, 2]]; Array[a, 70] (* _Amiram Eldar_, Sep 12 2021 *)"
			],
			"program": [
				"(PARI) a(n) = n - sumdiv(n, d, d%2); \\\\ _Michel Marcus_, Sep 12 2021"
			],
			"xref": [
				"Cf. A001227, A196020, A204217, A235791, A237048, A237591, A237593, A238005, A245092, A285898, A286000, A286001."
			],
			"keyword": "nonn,easy",
			"offset": "1,4",
			"author": "_Omar E. Pol_, Sep 12 2021",
			"references": 0,
			"revision": 57,
			"time": "2021-10-14T18:11:13-04:00",
			"created": "2021-10-14T18:11:13-04:00"
		}
	]
}