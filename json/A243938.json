{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A243938",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 243938,
			"data": "1,2,5,10,20,26,45,60,85,100,156,172,240,270,365,376,517,520,705,740,932,942,1260,1200,1510,1580,1928,1880,2420,2300,2861,2864,3410,3310,4265,3876,4780,4740,5625,5300,6672,6082,7460,7270,8400,8026,10092,9100",
			"name": "Expansion of f(-x^5)^10 / f(-x)^2 in powers of x where f() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A243938/b243938.txt\"\u003eTable of n, a(n) for n = 0..2500\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-2) * eta(q^5)^10 / eta(q)^2 in powers of q.",
				"Euler transform of period 5 sequence [2, 2, 2, 2, -8, ...].",
				"Given g.f. A(x), then B(q) = q^2 * A(q) satisfies 0 = f(B(q), B(q^2), B(q^4)) where f(u, v, w) = (v^3 + u^2*w + 16*u*w^2)^2 - 4*u*w * (u + 2*v) * (v + 8*w) * (v^2 + 2*u*w).",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (5 t)) = 1/5 (t/i)^4 g(t) where q = exp(2 Pi i t) and g() is the g.f. for A243939.",
				"Convolution square of A053723."
			],
			"example": [
				"G.f. = 1 + 2*x + 5*x^2 + 10*x^3 + 20*x^4 + 26*x^5 + 45*x^6 + 60*x^7 + ...",
				"G.f. = q^2 + 2*q^3 + 5*q^4 + 10*q^5 + 20*q^6 + 26*q^7 + 45*q^8 + 60*q^9 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x^5]^10 / QPochhammer[ x]^2, {x, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^5 + A)^10 / eta(x + A)^2, n))};",
				"(MAGMA) Basis( ModularForms( Gamma0(5), 4), 49) [3];",
				"(Sage) A = ModularForms( Gamma0(5), 4, prec=49) . basis(); (-A[0] + A[2]) / 13;"
			],
			"xref": [
				"Cf. A053723, A243939."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Michael Somos_, Jun 15 2014",
			"references": 2,
			"revision": 13,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2014-06-15T13:29:54-04:00"
		}
	]
}