{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A285769",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 285769,
			"data": "1,2,3,4,5,6,7,8,9,10,11,36,13,14,15,16,17,36,19,100,21,22,23,216,25,26,27,196,29,30,31,32,33,34,35,1296,37,38,39,1000,41,42,43,484,225,46,47,1296,49,100,51,676,53,216,55,2744,57,58,59,900,61,62,441",
			"name": "(Product of distinct prime factors)^(Product of prime exponents).",
			"comment": [
				"a(n) and A066638 differ at {36, 72, 100, 108, 144, ...}, i.e., for all n in A036785, since a(n) takes the product of the multiplicities of prime factors of n, while A066638 takes the maximum value of the multiplicities of prime factors of n. For these n, a(n) \u003e A066638(n).",
				"a(1) = 1 since 1 is the empty product; 1^1 = 1.",
				"a(p) = p since omega(p) = A001221(p) = 1 thus p^1 = p.",
				"a(p^m) = p^m since omega(p) = 1 thus p^m is maintained.",
				"For squarefree n with omega(n) \u003e 1, a(n) = n.",
				"For n with omega(n) \u003e 1 and at least one multiplicity m \u003e 1, a(n) \u003e n. In other words, let a(n) = k^m, where k is the product of the distinct prime factors of n and m is the product of the multiplicities of the distinct prime factors of n. a(n) \u003e n for n in A126706 since there are 2 or more prime factors in k and m \u003e 1.",
				"Squarefree kernels of terms a(n) \u003e n: {6, 6, 10, 6, 14, 6, 10, 22, 15, 6, 10, 26, 6, 14, 30, 21, ...}."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A285769/b285769.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A007947(n)^A005361(n)."
			],
			"example": [
				"a(2) = 2 since (2)^(1) = 2^1 = 2.",
				"a(6) = 6 since (2*3)^(1*1) = 6^1 = 6.",
				"a(12) = 36 since (2*3)^(2*1) = 6^2 = 36.",
				"a(30) = 30 since (2*3*5)^(1*1*1) = 30^1 = 30.",
				"a(144) = 1679616 since (2*3)^(4*2) = 6^8 = 1679616."
			],
			"mathematica": [
				"Array[Power @@ Map[Times @@ # \u0026, Transpose@ FactorInteger@ #] \u0026, 63] (* _Michael De Vlieger_, Apr 25 2017 *)"
			],
			"program": [
				"(Python)",
				"from sympy import divisor_count, divisors",
				"from sympy.ntheory.factor_ import core",
				"def rad(n): return max(list(filter(lambda i: core(i) == i, divisors(n))))",
				"def a(n): return rad(n)**divisor_count(n/rad(n)) # _Indranil Ghosh_, Apr 26 2017"
			],
			"xref": [
				"Cf. A005361, A007947, A066638, A088865, A126706."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Michael De Vlieger_, Apr 25 2017",
			"references": 5,
			"revision": 20,
			"time": "2017-04-26T23:17:44-04:00",
			"created": "2017-04-25T19:00:38-04:00"
		}
	]
}