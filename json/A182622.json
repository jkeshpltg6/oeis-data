{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A182622",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 182622,
			"data": "1,6,7,52,13,222,15,840,121,858,27,28268,29,894,991,26896,49,113970,51,215892,2037,3446,55,14471576,441,3514,3899,217052,61,14538238,63,1721376,7905,13410,7139,926213284,101,13542,8039,221009192",
			"name": "a(n) is the number whose binary representation is the concatenation of the divisors of n written in base 2.",
			"comment": [
				"a(n) is A182621(n), interpreted as a binary number, written in base 10. The first repeated element is 991, from 15 and 479.",
				"Except for 1, no power of 2 can occur in this sequence, an obvious consequence of the fact that a(n) has to be the sum of at least two distinct powers of 2 for all n \u003e 1. - _Alonso del Arte_, Nov 13 2013"
			],
			"link": [
				"Indranil Ghosh, \u003ca href=\"/A182622/b182622.txt\"\u003eTable of n, a(n) for n = 1..50000\u003c/a\u003e"
			],
			"formula": [
				"a(p) = 2^(floor(log_2(p)) + 1) + p for p prime. Also, a(p + k) \u003e a(p) for all k \u003e 0. Furthermore, for all primes p \u003e 3, a(p) \u003c a(p - 1).",
				"a(2^(m - 1)) = sum(k = 0 .. m - 1, 2^((m^2 + m)/2 - (k^2 + k)/2 - 1)) = A164894(m). - _Alonso del Arte_, Nov 13 2013"
			],
			"example": [
				"The divisors of 10 are 1, 2, 5, 10. Then 1, 2, 5, 10 written in base 2 are 1, 10, 101, 1010. The concatenation of 1, 10, 101, 1010 is 1101011010. Then a(10) = 858 because the binary number 1101011010 written in base 10 is 858."
			],
			"mathematica": [
				"concatBits[n_] := FromDigits[Join @@ (IntegerDigits[#, 2]\u0026 /@ Divisors[n]), 2]; concatBits /@ Range[40](* _Giovanni Resta_, Nov 23 2010 *)"
			],
			"program": [
				"(Python)",
				"def A182622(n):",
				"....s=\"\"",
				"....for i in range(1,n+1):",
				"........if n%i==0:",
				"............s+=bin(i)[2:]",
				"....return int(s,2) # _Indranil Ghosh_, Jan 28 2017",
				"(PARI) a(n) = {my(cbd = []); fordiv(n, d, cbd = concat(cbd, binary(d));); fromdigits(cbd, 2);} \\\\ _Michel Marcus_, Jan 28 2017"
			],
			"xref": [
				"Cf. A027750, A007088, A182620, A182621, A182623, A182624, A182627, A182632."
			],
			"keyword": "nonn,base,easy",
			"offset": "1,2",
			"author": "_Omar E. Pol_, Nov 22 2010",
			"ext": [
				"More terms from _Giovanni Resta_, Nov 23 2010"
			],
			"references": 7,
			"revision": 24,
			"time": "2017-01-31T16:25:32-05:00",
			"created": "2010-11-22T19:57:28-05:00"
		}
	]
}