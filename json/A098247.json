{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A098247",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 98247,
			"data": "1,226,51301,11645101,2643386626,600037119001,136205782626601,30918112619119426,7018275358757483101,1593117588325329544501,361630674274491049118626,82088569942721142820383601",
			"name": "First differences of Chebyshev polynomials S(n,227)=A098245(n) with Diophantine property.",
			"comment": [
				"(15*b(n))^2 - 229*a(n)^2 = -4 with b(n)=A098246(n) give all positive solutions of this Pell equation."
			],
			"link": [
				"Indranil Ghosh, \u003ca href=\"/A098247/b098247.txt\"\u003eTable of n, a(n) for n = 0..423\u003c/a\u003e",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"Giovanni Lucca, \u003ca href=\"http://forumgeom.fau.edu/FG2019volume19/FG201902index.html\"\u003eInteger Sequences and Circle Chains Inside a Hyperbola\u003c/a\u003e, Forum Geometricorum (2019) Vol. 19, 11-16.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (227,-1).",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e"
			],
			"formula": [
				"a(n) = S(n, 227) - S(n-1, 227) = T(2*n+1, sqrt(229)/2)/(sqrt(229)/2), with S(n, x) = U(n, x/2) Chebyshev's polynomials of the second kind, A049310. S(-1, x)= 0 = U(-1, x) and T(n, x) Chebyshev's polynomials of the second kind, A053120.",
				"a(n) = ((-1)^n)*S(2*n, 15*i) with the imaginary unit i and the S(n, x) = U(n, x/2) Chebyshev polynomials.",
				"G.f.: (1-x)/(1-227*x+x^2).",
				"a(n) = 227*a(n-1) - a(n-2), n \u003e 1; a(0)=1, a(1)=226. - _Philippe Deléham_, Nov 18 2008"
			],
			"example": [
				"All positive solutions of Pell equation x^2 - 229*y^2 = -4 are (15=15*1,1), (3420=15*228,226), (776325=15*51755,51301), (176222355=15*11748157,11645101), ..."
			],
			"mathematica": [
				"LinearRecurrence[{227,-1}, {1,226}, 20] (* _G. C. Greubel_, Aug 01 2019 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^20)); Vec((1-x)/(1-227*x+x^2)) \\\\ _G. C. Greubel_, Aug 01 2019",
				"(MAGMA) I:=[1,226]; [n le 2 select I[n] else 227*Self(n-1) - Self(n-2): n in [1..20]]; // _G. C. Greubel_, Aug 01 2019",
				"(Sage) ((1-x)/(1-227*x+x^2)).series(x, 20).coefficients(x, sparse=False) # _G. C. Greubel_, Aug 01 2019",
				"(GAP) a:=[1,226];; for n in [3..20] do a[n]:=227*a[n-1]-a[n-2]; od; a; # _G. C. Greubel_, Aug 01 2019"
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Sep 10 2004",
			"references": 5,
			"revision": 31,
			"time": "2020-01-23T03:47:32-05:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}