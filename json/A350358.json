{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A350358",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 350358,
			"data": "4,2,0,3,6,9,5,8,8,7,8,3,2,0,2,2,9,8,1,3,2,4,3,9,3,8,1,8,1,8,0,8,8,1,8,9,9,8,1,5,4,9,5,5,3,2,8,7,1,2,2,3,9,5,1,4,5,3,5,4,0,7,3,5,4,3,6,4,0,1,2,0,8,1,2,1,8,7,0,6,2,2,7,3,1,5,1,4",
			"name": "Value of -F(0), where F(x) is the indefinite integral of x^(1/x).",
			"comment": [
				"The indefinite integral of x^(1/x) can be derived by expanding",
				"    x^(1/x) = exp(log(x)/x) = Sum_{n\u003e=0} (log(x)/x)^n/n!,",
				"then integrating term-by-term to get the double summation",
				"    F(x) = x + (1/2)*(log(x))^2",
				"             - (Sum_{n\u003e=2} (n-1)^(-n-1) * x^(-n+1)/n!",
				"             * (Sum_{k=0..n} A350297(n,k)*(log(x))^k)).",
				"We assume the constant of integration in F(x) is zero.",
				"To compute definite integrals of x^(1/x) for ranges of x starting at x=0 or x=1, we would need the values",
				"    F(0) = lim {x-\u003e0} F(x) = -0.4203695887832022981324...",
				"    F(1) = 1 - Sum_{n\u003e=2} (n-1)^(-n-1) = -0.06687278808178024266...",
				"Note that the definite Integral_{t=0..1} x^(1/x) = F(1)-F(0) = A175999.",
				"The calculation of F(0) requires some care. See the FORMULA below.",
				"Since x*(1/x) is the inverse of the infinite exponentiation function E(y) = y^(y^(y^(...))) = x, the definite integrals of these two functions are related by",
				"    (Integral_{t=0..y} E(t)) + (Integral_{t=0..x) t*1/t)) = x*y.",
				"Note that the repeated exponentiation in E(y) converges for 0 \u003c= y \u003c e^(1/e), but diverges for y \u003e e^(1/e)."
			],
			"link": [
				"Peter Luschny, \u003ca href=\"/A350358/b350358.txt\"\u003eTable of n, a(n) for n = 0..999\u003c/a\u003e",
				"Jonathan Sondow and Diego Marques, \u003ca href=\"http://ami.ektf.hu/uploads/papers/finalpdf/AMI_37_from151to164.pdf\"\u003eAlgebraic and transcendental solutions of some exponential equations\u003c/a\u003e, Annales Mathematicae et Informaticae 37 (2010) 151-164; see Figure 5.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/SteinersProblem.html\"\u003eSteiner's Problem\u003c/a\u003e."
			],
			"formula": [
				"The calculation of F(0) requires some care, because terms in the formula for F(x) diverge for x=0, but converge for all x \u003e 0, although convergence is progressively slower as x approaches zero. To calculate F(0), first choose a desired precision d (absolute error). Then choose any x such that 0 \u003c x^(1/x) \u003c d, and evaluate F(x) as defined above.",
				"Since F(x)-F(0) \u003c x^(1/x) \u003c d, F(0)=F(x) to the desired precision.",
				"For small x, the main summation terms initially increase in absolute value (but with alternating signs), reach a maximum of about 1/d at n = -log(x)/x, then decrease at an accelerating rate and reach a value of around d at n = -3.5911*log(x)/x, at which point the large terms have mostly cancelled out, and further terms are below precision level.",
				"For example, for a final precision of d = 10^-50, the calculation must allow for intermediate terms and sums as large as 1/d = 10^50, so these terms must be evaluated to at least 100 digits. For 50 digits, F(.03) is a suitable choice, because .03^(1/.03) = 1.7273...*10^-51 \u003c 10^-50. A few extra digits should also be allowed for round off error."
			],
			"example": [
				"0.4203695887832022981324..."
			],
			"maple": [
				"# The chosen parameters give about 100 exact decimal places.",
				"Digits := 400: F := proc(b, x) local s, lnx; lnx := log(x);",
				"   s := add(add((n*lnx)^k / k!, k = 0..n+1) / (n*x)^(n+2), n = 1..b);",
				"   x - x^2 * s + lnx^2 / 2 end:",
				"F(2000, 0.01); # _Peter Luschny_, Dec 27 2021"
			],
			"mathematica": [
				"RealDigits[N[Sum[1/n^(n+2), {n, 1, 100}] + Integrate[x^(1/x), {x, 0, 1}] - 1, 110]][[1]] (* _Amiram Eldar_, Dec 29 2021 *)"
			],
			"program": [
				"(Julia) # The chosen parameters give about 100 exact decimal places.",
				"using Nemo",
				"RR = RealField(1100)",
				"function F(b::Int, x::arb)",
				"    lnx = log(x)",
				"    s = sum(gamma_regularized(RR(n+2), RR(n)*lnx) * RR(n)^(-n-2) for n in 1:b)",
				"    x + (lnx * lnx) / RR(2) - s",
				"end",
				"println( F(1400, RR(0.015)) ) # _Peter Luschny_, Dec 26 2021"
			],
			"xref": [
				"Cf. A350297, A175999, A176000, A073229."
			],
			"keyword": "nonn,cons",
			"offset": "0,1",
			"author": "_Robert B Fowler_, Dec 26 2021",
			"ext": [
				"More terms from _Hugo Pfoertner_, Dec 26 2021"
			],
			"references": 2,
			"revision": 39,
			"time": "2021-12-30T01:52:49-05:00",
			"created": "2021-12-28T00:14:48-05:00"
		}
	]
}