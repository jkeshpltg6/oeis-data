{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A274805",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 274805,
			"data": "1,2,-3,-6,45,11,-1372,4298,59244,-573463,-2432023,75984243,-136498141,-10881169822,100704750342,1514280063802,-36086469752977,-102642110690866,11883894518252419,-77863424962770751,-3705485804176583500,71306510264347489177",
			"name": "The logarithmic transform of sigma(n).",
			"comment": [
				"The logarithmic transform [LOG] transforms an input sequence b(n) into the output sequence a(n). The LOG transform is the inverse of the exponential transform [EXP], see the Weisstein link and the Sloane and Plouffe reference. This relation goes by the name of Riddell’s formula. For information about the EXP transform see A274804. The logarithmic transform is related to the inverse multinomial transform, see A274844 and the first formula.",
				"The definition of the LOG transform, see the second formula, shows that n \u003e= 1. To preserve the identity EXP[LOG[b(n)]] = b(n) for n \u003e= 0 for a sequence b(n) with offset 0 the shifted sequence b(n-1) with offset 1 has to be used as input for the LOG transform, otherwise information about b(0) will be lost in transformation.",
				"In the a(n) formulas, see the examples, the cumulant expansion numbers A127671 appear.",
				"We observe that the logarithmic transform leaves the value of a(0) undefined.",
				"The Maple programs can be used to generate the logarithmic transform of a sequence. The first program uses a formula found by _Alois P. Heinz_, see A001187 and the first formula. The second program uses the definition of the logarithmic transform, see the Weisstein link and the second formula. The third program uses information about the inverse of the logarithmic transform, see A274804."
			],
			"reference": [
				"Frank Harary and Edgar M. Palmer, Graphical Enumeration, 1973.",
				"Robert James Riddell, Contributions to the theory of condensation, Dissertation, University of Michigan, Ann Arbor, 1951.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, 1995, pp. 18-23."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A274805/b274805.txt\"\u003eTable of n, a(n) for n = 1..451\u003c/a\u003e",
				"M. Bernstein and N. J. A. Sloane, \u003ca href=\"https://arxiv.org/abs/math/0205301\"\u003eSome Canonical Sequences of Integers\u003c/a\u003e, Linear Algebra and its Applications, Vol. 226-228 (1995), pp. 57-72. Erratum 320 (2000), 210. [Link to arXiv version]",
				"M. Bernstein and N. J. A. Sloane, \u003ca href=\"/A003633/a003633_1.pdf\"\u003eSome canonical sequences of integers\u003c/a\u003e, Linear Alg. Applications, 226-228 (1995), 57-72; erratum 320 (2000), 210. [Link to Lin. Alg. Applic. version together with omitted figures]",
				"N. J. A. Sloane, \u003ca href=\"/transforms.txt\"\u003eTransforms\u003c/a\u003e.",
				"Eric W. Weisstein MathWorld, \u003ca href=\"http://mathworld.wolfram.com/LogarithmicTransform.html\"\u003eLogarithmic Transform\u003c/a\u003e."
			],
			"formula": [
				"a(n) = b(n) - Sum_{k = 1..n-1}((k*binomial(n, k)*b(n-k)*a(k))/n), n \u003e= 1, with b(n) = A000203(n) = sigma(n).",
				"E.g.f. log(1+ Sum_{n \u003e= 1}(b(n)*x^n/n!)), n \u003e= 1, with b(n) = A000203(n) = sigma(n)."
			],
			"example": [
				"Some a(n) formulas, see A127671:",
				"a(0) = undefined",
				"a(1) = 1*x(1)",
				"a(2) = 1*x(2) - x(1)^2",
				"a(3) = 1*x(3) - 3*x(1)*x(2) + 2*x(1)^3",
				"a(4) = 1*x(4) - 4*x(1)*x(3) - 3*x(2)^2 + 12*x(1)^2*x(2) - 6*x(1)^4",
				"a(5) = 1*x(5) - 5*x(1)*x(4) - 10*x(2)*x(3) + 20*x(1)^2*x(3) + 30*x(1)*x(2)^2 - 60*x(1)^3*x(2) + 24*x(1)^5"
			],
			"maple": [
				"nmax:=22: with(numtheory): b := proc(n): sigma(n) end: a:= proc(n) option remember; b(n) - add(k*binomial(n, k)*b(n-k)*a(k), k=1..n-1)/n: end: seq(a(n), n=1..nmax); # End first LOG program.",
				"nmax:=22: with(numtheory): b := proc(n): sigma(n) end: t1 := log(1 + add(b(n)*x^n/n!, n=1..nmax+1)): t2 := series(t1, x, nmax+1): a := proc(n): n!*coeff(t2, x, n) end: seq(a(n), n=1..nmax); # End second LOG program.",
				"nmax:=22: with(numtheory): b := proc(n): sigma(n) end: f := series(exp(add(r(n)*x^n/n!, n=1..nmax+1)), x, nmax+1): d := proc(n): n!*coeff(f, x, n) end: a(1):=b(1): r(1):= b(1): for n from 2 to nmax+1 do r(n) := solve(d(n)-b(n), r(n)): a(n):=r(n): od: seq(a(n), n=1..nmax); # End third LOG program."
			],
			"mathematica": [
				"a[1] = 1; a[n_] := a[n] = DivisorSigma[1, n] - Sum[k*Binomial[n, k] * DivisorSigma[1, n-k]*a[k], {k, 1, n-1}]/n; Table[a[n], {n, 1, 22}] (* _Jean-François Alcover_, Feb 27 2017 *)"
			],
			"program": [
				"(PARI) N=33; x='x+O('x^N); Vec(serlaplace(log(1+sum(n=1,N,sigma(n)*x^n/n!)))) \\\\ _Joerg Arndt_, Feb 27 2017"
			],
			"xref": [
				"Some LOG transform pairs are, n \u003e= 1: A006125(n-1) and A033678(n); A006125(n) and A001187(n); A006125(n+1) and A062740(n); A000045(n) and A112005(n); A000045(n+1) and A007553(n); A000040(n) and A007447(n); A000051(n) and (-1)*A263968(n-1); A002416(n) and A062738(n); A000290(n) and A033464(n-1); A029725(n-1) and A116652(n-1); A052332(n) and A002031(n+1); A027641(n)/A027642(n) and (-1)*A060054(n+1)/(A075180(n-1).",
				"Cf. A274804, A274844, A127671, A000203.",
				"Cf. A112005, A007553, A062740, A007447, A062738, A033464, A116652, A002031, A003704, A003707, A155585, A000142, A226968."
			],
			"keyword": "sign",
			"offset": "1,2",
			"author": "_Johannes W. Meijer_, Jul 27 2016",
			"references": 4,
			"revision": 25,
			"time": "2020-02-09T15:43:42-05:00",
			"created": "2016-07-27T09:58:14-04:00"
		}
	]
}