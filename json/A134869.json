{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A134869",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 134869,
			"data": "1,4,7,11,16,22,29,37,46,56,67,79,92,106,121,137,154,172,191,211,232,254,277,301,326,352,379,407,436,466,497,529,562,596,631,667,704,742,781,821,862,904,947,991,1036,1082,1129,1177,1226,1276,1327,1379,1432",
			"name": "Row sums of triangle A134868.",
			"comment": [
				"Where records occur in A182703. - _Omar E. Pol_, Feb 14 2012",
				"Consider quadratic polynomials x^2+cx+d. Then a(n) is the number of these polynomials with 0 \u003c= c \u003c n, 0 \u003c= d \u003c n where no polynomial can be horizontally translated into another. For example, a(3) = 7, the coefficients are as follows: (c, d) = {(0, 0), (0, 1), (0, 2), (1, 0), (1, 1), (1, 2), (2, 0)}. Two polynomials are excluded, namely x^2+2x+1 = (x+1)^2+0(x+1)+0, and x^2+2x+2 = (x+1)^2+0(x+1)+1. - _Griffin N. Macris_, Jul 19 2016",
				"a(n) gives the number of regions into which the square [0,1]x[0,1] is divided by the Bernstein polynomials of degree n. - _Franck Maminirina Ramaharo_, Feb 28 2018"
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A134869/b134869.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1)."
			],
			"formula": [
				"a(n) = 1, then for n\u003e1, a(n) = T(n) + 1, where A000217 = (1, 3, 6, 10, 15, ...).",
				"Binomial transform of [1, 3, 0, 1, -1, 1, -1, 1, ...].",
				"From _R. J. Mathar_, Oct 27 2008: (Start)",
				"G.f.: x(1+x-2x^2+x^3)/(1-x)^3.",
				"a(n) = 1 + A000217(n) = A000124(n), n \u003e 1. (End)"
			],
			"example": [
				"a(4) = 11 = sum of row 4 terms of triangle A134868: (2, + 2 + 3 + 4).",
				"a(4) = 11 = 1 + 10, where 10 = T(4).",
				"a(4) = 11 = (1, 3, 3, 1) dot (1, 3, 0, 1) = (1 + 9 + 0 + 1)."
			],
			"maple": [
				"a:=n-\u003esum((stirling2(j+1,n)), j=1..n):seq(a(n), n=1..50); # _Zerinvary Lajos_, Apr 12 2008"
			],
			"mathematica": [
				"Table[(n^2 + n)/2 + Boole[n != 1], {n, 53}] (* or *)",
				"Table[PolygonalNumber@ n + Boole[n != 1], {n, 53}] (* Version 10.4, or *)",
				"Table[Sum[StirlingS2[k + 1, n], {k, n}], {n, 53}] (* or *)",
				"Rest@ CoefficientList[Series[x (1 + x - 2 x^2 + x^3)/(1 - x)^3, {x, 0, 53}], x] (* _Michael De Vlieger_, Jul 19 2016 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003e1, n*(n+1)/2+1, 1) \\\\ _Charles R Greathouse IV_, Aug 05 2016"
			],
			"xref": [
				"Cf. A000217, A134868."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Gary W. Adamson_, Nov 14 2007",
			"ext": [
				"More terms from _R. J. Mathar_, Oct 27 2008"
			],
			"references": 5,
			"revision": 55,
			"time": "2018-03-04T23:28:55-05:00",
			"created": "2007-12-09T03:00:00-05:00"
		}
	]
}