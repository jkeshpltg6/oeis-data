{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007910",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7910,
			"data": "1,2,3,6,13,26,51,102,205,410,819,1638,3277,6554,13107,26214,52429,104858,209715,419430,838861,1677722,3355443,6710886,13421773,26843546,53687091,107374182,214748365,429496730,858993459,1717986918,3435973837,6871947674",
			"name": "Expansion of 1/((1-2*x)*(1+x^2)).",
			"comment": [
				"Also describes the location a(n) of the minimal scaling factor when rescaling an FFT of order 2^{n+2} in order to (currently) minimize the arithmetic operation count (Johnson \u0026 Frigo, 2007). - Steven G. Johnson (stevenj(AT)math.mit.edu), Dec 27 2006"
			],
			"reference": [
				"M. E. Larsen, Summa Summarum, A. K. Peters, Wellesley, MA, 2007; see p. 38."
			],
			"link": [
				"M. F. Hasler, \u003ca href=\"/A007910/b007910.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e (in replacement of a(0..999) indexed 1..1000 by Vincenzo Librandi)",
				"M. H. Cilasun, \u003ca href=\"http://arxiv.org/abs/1412.3265\"\u003eAn Analytical Approach to Exponent-Restricted Multiple Counting Sequences\u003c/a\u003e, arXiv preprint arXiv:1412.3265 [math.NT], 2014.",
				"M. H. Cilasun, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL19/Cilasun/cila5.html\"\u003eGeneralized Multiple Counting Jacobsthal Sequences of Fermat Pseudoprimes\u003c/a\u003e, Journal of Integer Sequences, Vol. 19, 2016, #16.2.3.",
				"I. Gessel, \u003ca href=\"http://www.jstor.org/stable/2974863\"\u003eProblem 10424\u003c/a\u003e, Amer. Math. Monthly, 102 (1995), 70.",
				"S. G. Johnson and M. Frigo, \u003ca href=\"http://www.fftw.org/newsplit.pdf\"\u003eA modified split-radix FFT with fewer arithmetic operations\u003c/a\u003e, IEEE Trans. Signal Processing 55 (2007), 111-119.",
				"Kyu-Hwan Lee, Se-jin Oh, \u003ca href=\"http://arxiv.org/abs/1601.06685\"\u003eCatalan triangle numbers and binomial coefficients\u003c/a\u003e, arXiv:1601.06685 [math.CO], 2016.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,-1,2)."
			],
			"formula": [
				"a(0) = 1, a(2n+1) = 2*a(2n) and a(2n) = 2*a(2n-1) + (-1)^n. [Corrected by _M. F. Hasler_, Feb 22 2018]",
				"a(n) = (4*2^n+cos(Pi*n/2)+2*sin(Pi*n/2))/5. - _Paul Barry_, Dec 17 2003",
				"a(n) = 2a(n-1)-a(n-2)+2a(n-3). Sequence equals half its second differences with first term dropped. a(n) + a(n+2) = 2^(n+2). - _Paul Curtz_, Dec 17 2007",
				"a(n) = round(2^(n+2)/5). - _Mircea Merca_, Dec 27 2010"
			],
			"maple": [
				"A007910:=n-\u003e(1/5)*(2^(n-1)+2*cos(n*Pi/2)-sin(n*Pi/2)); [seq(V(n),n=0..12)];",
				"seq(round(2^(n+2)/5),n=1..25) # _Mircea Merca_, Dec 27 2010"
			],
			"mathematica": [
				"CoefficientList[Series[1/((1 - 2 x) (1 + x^2)), {x, 0, 50}], x] (* _Vladimir Joseph Stephan Orlovsky_, Jun 20 2011 *)",
				"LinearRecurrence[{2,-1,2},{1,2,3},40] (* _Harvey P. Dale_, Feb 22 2016 *)"
			],
			"program": [
				"(MAGMA) [Round(2^(n+2)/5): n in [0..40]]; // _Vincenzo Librandi_, Jun 21 2011",
				"(PARI) a(n)=2^(n+2)\\/5 \\\\ _Charles R Greathouse IV_, Jun 21 2011"
			],
			"xref": [
				"Cf. A007909, A007679."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "Mogens Esrom Larsen (mel(AT)math.ku.dk)",
			"ext": [
				"Entry revised by _N. J. A. Sloane_, Feb 24 2004",
				"Offset corrected and minor edits by _M. F. Hasler_, Feb 22 2018"
			],
			"references": 14,
			"revision": 59,
			"time": "2018-02-22T20:07:24-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}