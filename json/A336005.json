{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A336005",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 336005,
			"data": "1,1,1,1,2,1,2,1,1,2,2,2,2,3,2,1,2,1,2,2,2,2,3,2,3,2,1,2,2,2,2,1,2,2,2,2,3,2,3,2,2,3,3,3,3,4,3,2,3,2,3,3,3,1,2,2,2,2,3,2,3,2,2,1,2,2,2,2,3,2,3,2,2,3,3,3,3,4,3,2,1,2,2,2,2,3",
			"name": "a(n) is the number of terms in the mixed binary-ternary representation of n. See Comments.",
			"comment": [
				"Suppose that B1 and B2 are increasing sequences of positive integers, and let B be the increasing sequence of numbers in the union of B1 and B2.  Every positive integer n has a unique representation given by the greedy algorithm with B1 as base, and likewise for B2 and B. For many n, the number of terms in the B-representation of n is less than the number of terms in the B1-representation, as well as the B2-representation, but not for all n, as in the example 45 = 27 + 18 (ternary) and 45 = 32 + 9 + 4 (mixed)."
			],
			"link": [
				"Michael S. Branicky, \u003ca href=\"/A336005/b336005.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"7 = 6 + 1, so that a(7) = 2.",
				"45 = 32 + 9 + 4, so that a(45) = 3."
			],
			"mathematica": [
				"z = 20; zz = 100;",
				"b1 = Sort[Table[2^k, {k, 0, z}], Greater];",
				"b2 = Sort[Union[Table[3^k, {k, 0, z}], Table[2*3^k, {k, 0, z}]],",
				"   Greater]; b = Sort[Union[b1, b2], Greater];",
				"g1 = Map[{#, DeleteCases[b1  Reap[",
				"         FoldList[(Sow[Quotient[#1, #2]]; Mod[#1, #2]) \u0026, #, b1]][[2,",
				"         1]], 0]} \u0026, Range[zz]];",
				"m1 = Map[Length[#[[2]]] \u0026, g1];",
				"g2 = Map[{#, DeleteCases[b2 Reap[FoldList[(Sow[Quotient[#1, #2]]; Mod[#1, #2]) \u0026, #, b2]][[2, 1]], 0]} \u0026, Range[zz]];",
				"m2 = Map[Length[#[[2]]] \u0026, g2];",
				"g = Map[{#, DeleteCases[",
				"     b Reap[FoldList[(Sow[Quotient[#1, #2]]; Mod[#1, #2]) \u0026, #,",
				"         b]][[2, 1]], 0]} \u0026, Range[zz]]",
				"m = Map[Length[#[[2]]] \u0026, g];",
				"m1 (* # terms in binary representation *)",
				"m2 (* # terms in trinary representation *)",
				"m  (* # terms in mixed base representation *)  (* A336005 *)"
			],
			"program": [
				"(Python)",
				"from itertools import count, takewhile",
				"N = 10**6",
				"B1 = list(takewhile(lambda x: x[0] \u003c= N, ((2**i, 2) for i in count(0))))",
				"B21 = list(takewhile(lambda x: x[0] \u003c= N, ((3**i, 3) for i in count(0))))",
				"B22 = list(takewhile(lambda x: x[0] \u003c= N, ((2*3**i, 3) for i in count(0))))",
				"B = sorted(set(B1 + B21 + B22), reverse=True)",
				"def gbt(n, B): # greedy binary-ternary representation",
				"    r = []",
				"    for t, b in B:",
				"        if t \u003c= n:",
				"            r.append(t)",
				"            n -= t",
				"            if n == 0:",
				"                return r",
				"def a(n): return len(gbt(n, B))",
				"print([a(n) for n in range(1, 87)]) # _Michael S. Branicky_, Jan 06 2022"
			],
			"xref": [
				"Cf. A336004, A336006."
			],
			"keyword": "nonn,base,changed",
			"offset": "1,5",
			"author": "_Clark Kimberling_, Jul 06 2020",
			"references": 3,
			"revision": 20,
			"time": "2022-01-07T08:28:14-05:00",
			"created": "2020-08-17T06:41:28-04:00"
		}
	]
}