{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A100327",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 100327,
			"data": "1,2,8,42,252,1636,11188,79386,579020,4314300,32697920,251284292,1953579240,15336931928,121416356108,968187827834,7769449728780,62696580696172,508451657412496,4141712433518956,33872033298518728,278014853384816184,2289376313410678312",
			"name": "Row sums of triangle A100326, in which row n equals the inverse binomial of column n of square array A100324.",
			"comment": [
				"Self-convolution yields A100328, which equals column 1 of triangle A100326 (omitting leading zero)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A100327/b100327.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e"
			],
			"formula": [
				"G.f.: (1/x)*Series_Reversion( x*(1-x + sqrt(1 - 4*x)) / (2+x) ). - _Paul D. Hanna_, Nov 22 2012",
				"G.f. A(x) = (1+G(x))/(1-G(x)), also A(x)^2 = (1+G(x))*G(x)/x, where G(x) = x*(1+G(x))/(1-G(x))^2 is the g.f. of A003169.",
				"a(n) = 2*A003168(n) for n\u003e0 with a(0)=1.",
				"a(n) = Sum_{k=1..n} 2*binomial(n, k)*binomial(2n+k, k-1)/n for n\u003e0 with a(0)=1.",
				"Recurrence: 20*n*(2*n+1)*a(n) = (371*n^2 - 395*n + 96)*a(n-1) - 6*(27*n^2 - 103*n + 96)*a(n-2) + 4*(n-3)*(2*n-5)*a(n-3). - _Vaclav Kotesovec_, Oct 17 2012",
				"a(n) ~ sqrt(4046 + 1122*sqrt(17))*((71 + 17*sqrt(17))/16)^n/(136*sqrt(Pi)*n^(3/2)). - _Vaclav Kotesovec_, Oct 17 2012",
				"a(n) = 2^n*binomial(3*n,2*n)*hypergeom([-1-2*n,-n], [-3*n],1/2)/(n+1/2)). - _Peter Luschny_, Jun 10 2017"
			],
			"maple": [
				"A100327 := n -\u003e simplify(2^n*binomial(3*n,2*n)*hypergeom([-1-2*n,-n], [-3*n], 1/2)/ (n+1/2)): seq(A100327(n), n=0..22); # _Peter Luschny_, Jun 10 2017"
			],
			"mathematica": [
				"Flatten[{1,Table[Sum[2*Binomial[n,k]*Binomial[2n+k,k-1]/n,{k,1,n}],{n,1,20}]}] (* _Vaclav Kotesovec_, Oct 17 2012 *)"
			],
			"program": [
				"(PARI) a(n)=if(n==0,1,sum(k=0,n,2*binomial(n,k)*binomial(2*n+k,k-1)/n))",
				"(PARI) a(n)=polcoeff((1/x)*serreverse(x*(1-x+sqrt(1-4*x +x^2*O(x^n)))/(2+x)),n)",
				"for(n=0,25,print1(a(n),\", \")) \\\\ _Paul D. Hanna_, Nov 22 2012"
			],
			"xref": [
				"Cf. A003168, A003169, A100326, A219538."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Paul D. Hanna_, Nov 17 2004",
			"references": 5,
			"revision": 26,
			"time": "2017-06-10T10:31:39-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}