{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A266591",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 266591,
			"data": "1,1,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0",
			"name": "Middle column of the \"Rule 37\" elementary cellular automaton starting with a single ON (black) cell.",
			"comment": [
				"Also the number of excursions of length n with Motzkin-steps forbidding all consecutive steps of length 2 except UD and DU. The Motzkin step set is U=(1,1), H=(1,0) and D=(1,-1). An excursion is a path starting at (0,0), ending on the x-axis and never crossing the x-axis, i.e., staying at nonnegative altitude. This sequence is periodic with a pre-period of length 2 (namely 1, 1) and a period of length 2 (namely 1, 0). It can be shown (via induction) that the middle column of rule 37 is eventually periodic, too. - _Valerie Roitner_, Apr 02 2020"
			],
			"reference": [
				"S. Wolfram, A New Kind of Science, Wolfram Media, 2002; p. 55."
			],
			"link": [
				"Robert Price, \u003ca href=\"/A266591/b266591.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Andrei Asinowski, Cyril Banderier, and Valerie Roitner, \u003ca href=\"https://lipn.univ-paris13.fr/~banderier/Papers/several_patterns.pdf\"\u003eGenerating functions for lattice paths with several forbidden patterns\u003c/a\u003e, preprint, 2019.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ElementaryCellularAutomaton.html\"\u003eElementary Cellular Automaton\u003c/a\u003e",
				"S. Wolfram, \u003ca href=\"http://wolframscience.com/\"\u003eA New Kind of Science\u003c/a\u003e",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_Elementary_Cellular_Automata\"\u003eIndex to Elementary Cellular Automata\u003c/a\u003e"
			],
			"formula": [
				"G.f.: (1+t-t^3)/((1-t)*(1+t)). - _Valerie Roitner_, Jan 13 2020"
			],
			"example": [
				"For n=2k we only have one allowed excursion, namely (UD)^k. For n=1, however, the excursion H is allowed (since we have not attained length two or more, no forbidden consecutive step sequence has appeared yet). - _Valerie Roitner_, Jan 13 2020"
			],
			"mathematica": [
				"rule=37; rows=20; ca=CellularAutomaton[rule,{{1},0},rows-1,{All,All}]; (* Start with single black cell *) catri=Table[Take[ca[[k]],{rows-k+1,rows+k-1}],{k,1,rows}]; (* Truncated list of each row *) Table[catri[[k]][[k]],{k,1,rows}] (* Keep only middle cell from each row *)"
			],
			"xref": [
				"Cf. A059841, A266588."
			],
			"keyword": "nonn,easy,walk",
			"offset": "0",
			"author": "_Robert Price_, Jan 01 2016",
			"references": 3,
			"revision": 25,
			"time": "2020-04-09T07:30:43-04:00",
			"created": "2016-01-01T17:16:53-05:00"
		}
	]
}