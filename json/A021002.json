{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A021002",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 21002,
			"data": "2,2,9,4,8,5,6,5,9,1,6,7,3,3,1,3,7,9,4,1,8,3,5,1,5,8,3,1,3,4,4,3,1,1,2,8,8,7,1,3,1,6,3,7,9,9,4,4,1,6,6,8,6,7,3,2,7,5,8,1,4,0,3,0,0,0,1,3,9,7,0,1,2,0,1,1,3,2,3,1,5,7,5,0,1,7,9,6,8,0,4,5,2,3,2,7,2,4,9,0,8,1,3,8,4",
			"name": "Decimal expansion of zeta(2)*zeta(3)*zeta(4)*...",
			"comment": [
				"A very good approximation is 2e-Pi = ~2.29497100332829723225793155942... - _Marco Matosic_, Nov 16 2005",
				"This constant is equal to the asymptotic mean of number of Abelian groups of order n (A000688). - _Amiram Eldar_, Oct 16 2020"
			],
			"reference": [
				"R. Ayoub, An Introduction to the Analytic Theory of Numbers, Amer. Math. Soc., 1963, p. 198-9.",
				"Steven R. Finch, Mathematical Constants, Cambridge University Press, 2003, Section 5.1 Abelian group enumeration constants, p. 274."
			],
			"link": [
				"Steven R. Finch, \u003ca href=\"http://web.archive.org/web/20010603070928/http://www.mathsoft.com/asolve/constant/abel/abel.html\"\u003eAbelian Group Enumeration Constants\u003c/a\u003e. [From the Wayback machine]",
				"Steven R. Finch, \u003ca href=\"https://doi.org/10.1017/9781316997741\"\u003eMathematical Constants II\u003c/a\u003e, Encyclopedia of Mathematics and Its Applications, Cambridge University Press, Cambridge, 2018, p. 86.",
				"Felix Fontein and Pawel Wocjan, \u003ca href=\"http://arxiv.org/abs/1111.1348\"\u003eQuantum Algorithm for Computing the Period Lattice of an Infrastructure\u003c/a\u003e, arXiv preprint arXiv:1111.1348 [quant-ph], 2011.",
				"Felix Fontein and Pawel Wocjan, \u003ca href=\"https://doi.org/10.1016/j.jsc.2013.12.002\"\u003eOn the probability of generating a lattice\u003c/a\u003e, Journal of Symbolic Computation, Vol. 64 (2014), pp. 3-15, \u003ca href=\"http://arxiv.org/abs/1211.6246\"\u003earXiv preprint\u003c/a\u003e, arXiv:1211.6246 [math.CO], 2012-2013. - From _N. J. A. Sloane_, Jan 03 2013",
				"Bernd C. Kellner, \u003ca href=\"http://arxiv.org/abs/math.NT/0604505\"\u003eOn asymptotic constants related to products of Bernoulli numbers and factorials\u003c/a\u003e, arXiv:math/0604505 [math.NT], 2006-2009.",
				"B. R. Srinivasan, \u003ca href=\"http://doi.org/10.4064/aa-23-2-195-205\"\u003eOn the Number of Abelian Groups of a Given Order\u003c/a\u003e, Acta Arithmetica, Vol. 23, No. 2 (1973), pp. 195-205, \u003ca href=\"https://eudml.org/doc/205185\"\u003ealternative link\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/AbelianGroup.html\"\u003eAbelian Group\u003c/a\u003e."
			],
			"formula": [
				"Product of A080729 and A080730. - _R. J. Mathar_, Feb 16 2011"
			],
			"example": [
				"2.2948565916733137941835158313443112887131637994416686732758140300..."
			],
			"maple": [
				"evalf(product(Zeta(n), n=2..infinity), 200);"
			],
			"mathematica": [
				"p = Product[ N[ Zeta[n], 256], {n, 2, 1000}]; RealDigits[p, 10, 111][[1]] (* _Robert G. Wilson v_, Nov 22 2005 *)"
			],
			"program": [
				"(PARI) prodinf(n=2,zeta(n)) \\\\ _Charles R Greathouse IV_, May 27 2015"
			],
			"xref": [
				"Cf. A068982 (reciprocal), A082868 (continued fraction).",
				"Cf. A002117, A000688, A063966, A080729, A080730."
			],
			"keyword": "cons,nonn",
			"offset": "1,1",
			"author": "Andre Neumann Kauffman (ank(AT)nlink.com.br)",
			"ext": [
				"More terms from _Simon Plouffe_, Jan 07 2002",
				"Further terms from _Robert G. Wilson v_, Nov 22 2005",
				"Mathematica program fixed by _Vaclav Kotesovec_, Sep 20 2014"
			],
			"references": 18,
			"revision": 56,
			"time": "2021-09-15T11:44:41-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}