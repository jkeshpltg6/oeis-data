{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A292849",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 292849,
			"data": "1,1,3,1,7,3,7,1,15,7,3,3,5,7,15,1,31,15,7,7,13,3,7,3,31,5,31,7,31,15,31,1,63,31,11,15,7,7,7,7,57,13,3,3,23,7,11,3,21,31,43,5,39,31,7,7,9,31,35,15,21,31,63,1,127,63,23,31,15,11,15,15,29,7",
			"name": "a(n) is the least positive k such that the Hamming weight of k equals the Hamming weight of k*n.",
			"comment": [
				"The Hamming weight of a number n is given by A000120(n).",
				"All terms are odd.",
				"Numbers n such that a(n) is not squarefree are 33, 57, 63, 66, 83, 114, 115, 126, 132, 153, 155, ...",
				"Numbers n such that a(n) \u003e n are 5, 9, 17, 25, 27, 29, 33, 41, 65, 97, 101, 109, 113, ...",
				"a(n) = 1 iff n = 2^i for some i \u003e= 0.",
				"a(n) = 3 iff n = A007583(i) * 2^j for some i \u003e 0 and j \u003e= 0.",
				"Apparently:",
				"- if n \u003c 2^k then a(n) \u003c 2^k,",
				"- a(n) = n iff n = A000225(i) for some i \u003e 0.",
				"Proof that a(n) \u003c 2^k if n \u003c 2^k (see preceding comment): We can assume that n is not a power of two and take k such that 2^(k-1) \u003c n \u003c 2^k (so that k is the number of binary digits of n). Now, n - 1 and 2^k - n have complementary binary digits, so the binary digits of (2^k - 1)*n = 2^k*(n - 1) + (2^k - n) consist of the k digits of n - 1 followed by the complementary digits. This implies that the number of binary 1's is k, so that (2^k - 1)*n and 2^k - 1 have the same number of 1's and a(n) \u003c= 2^k - 1. - _Pontus von Brömssen_, Jan 01 2021",
				"See also A180012 for the base 10 equivalent sequence."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A292849/b292849.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Altug Alkan, \u003ca href=\"/A292849/a292849.png\"\u003eLine graph of n - a(n) for n \u003c= 2^20 + 1\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A292849/a292849_1.png\"\u003eScatterplot of n XOR a(n) for n \u003c= 2^16\u003c/a\u003e"
			],
			"formula": [
				"a((2^m)*n) = a(n) for all m \u003e= 0 and n \u003e= 1.",
				"a(2^m + 1) = 2^(m + 1) - 1 for all m \u003e= 0.",
				"a(2^m - 1) = 2^m - 1 for all m \u003e= 1.",
				"a(2^m) = 1 for all m \u003e= 0."
			],
			"mathematica": [
				"Table[SelectFirst[Range[1, 2^8 + 1, 2], Equal @@ Thread[DigitCount[{#, # n}, 2, 1]] \u0026], {n, 74}] (* _Michael De Vlieger_, Sep 25 2017 *)"
			],
			"program": [
				"(PARI) a(n) = forstep(k=1, oo, 2, if (hammingweight(k) == hammingweight(k*n), return (k)))",
				"(PARI) a(n)=my(k=1); while ((hammingweight(k)) != hammingweight(k*n), k++); k;"
			],
			"xref": [
				"Cf. A000120, A000225, A007583, A115873, A180012."
			],
			"keyword": "nonn,base,easy,look",
			"offset": "1,3",
			"author": "_Rémy Sigrist_ and _Altug Alkan_, Sep 25 2017",
			"references": 6,
			"revision": 45,
			"time": "2021-01-02T13:24:44-05:00",
			"created": "2017-09-25T15:17:12-04:00"
		}
	]
}