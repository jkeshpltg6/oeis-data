{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A319582",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 319582,
			"data": "1,1,1,1,2,1,1,1,1,1,1,2,2,1,1,1,1,1,1,1,1,1,2,1,2,1,2,1,1,1,2,1,1,2,1,1,1,2,1,1,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,1,4,1,1,1,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,1,1,1,2,2,1,1,2,1,1",
			"name": "Square array T(n, k) = 2 ^ (Sum_{p prime} [v_p(n) \u003e= v_p(k) \u003e 0]) read by antidiagonals up, where [] is the Iverson bracket and v_p is the p-adic valuation, n \u003e= 1, k \u003e= 1.",
			"comment": [
				"T(n, k) is the number of integers located on the sphere with center n and radius log(k), when the metric is given by log(A089913).",
				"T(., k) is multiplicative and k-periodic.",
				"T(n, .) is multiplicative and n^2-periodic."
			],
			"formula": [
				"T(n, k) = card({x; d(n, x) = log(k)}), if d denotes log(A089913(., .)), which is a metric.",
				"T(n, k) = 2 ^ (Sum_{p prime} [v_p(n) \u003e= v_p(k) \u003e 0]).",
				"a(n) = 2 ^ A319581(n).",
				"T(n, n) = 2 ^ A001221(n) = A034444(n)."
			],
			"example": [
				"T(60, 50) = T(2^2 * 3^1 * 5^1, 2^1 * 5^2)",
				"  = T(2^2, 2^1) * T(3^1, 3^0) * T(5^1, 5^2)",
				"  = 2^[2 \u003e= 1 \u003e 0] * 2^[1 \u003e= 0 \u003e 0] * 2^[1 \u003e= 2 \u003e 0])",
				"  = 2^1 * 2^0 * 2^0 = 2 * 1 * 1 = 2.",
				"Array begins:",
				"     k =                 1 1 1",
				"   n   1 2 3 4 5 6 7 8 9 0 1 2",
				"   =  ------------------------",
				"   1 | 1 1 1 1 1 1 1 1 1 1 1 1",
				"   2 | 1 2 1 1 1 2 1 1 1 2 1 1",
				"   3 | 1 1 2 1 1 2 1 1 1 1 1 2",
				"   4 | 1 2 1 2 1 2 1 1 1 2 1 2",
				"   5 | 1 1 1 1 2 1 1 1 1 2 1 1",
				"   6 | 1 2 2 1 1 4 1 1 1 2 1 2",
				"   7 | 1 1 1 1 1 1 2 1 1 1 1 1",
				"   8 | 1 2 1 2 1 2 1 2 1 2 1 2",
				"   9 | 1 1 2 1 1 2 1 1 2 1 1 2",
				"  10 | 1 2 1 1 2 2 1 1 1 4 1 1",
				"  11 | 1 1 1 1 1 1 1 1 1 1 2 1",
				"  12 | 1 2 2 2 1 4 1 1 1 2 1 4"
			],
			"mathematica": [
				"F[n_] := If[n == 1, {}, FactorInteger[n]]",
				"V[p_] := If[KeyExistsQ[#, p], #[p], 0] \u0026",
				"PreT[n_, k_] :=",
				"Module[{fn = F[n], fk = F[k], p, an = \u003c||\u003e, ak = \u003c||\u003e, w},",
				"  p = Union[First /@ fn, First /@ fk];",
				"  (an[#[[1]]] = #[[2]]) \u0026 /@ fn;",
				"  (ak[#[[1]]] = #[[2]]) \u0026 /@ fk;",
				"  w = ({V[#][an], V[#][ak]}) \u0026 /@ p;",
				"  Select[w, (#[[1]] \u003e= #[[2]] \u003e 0) \u0026]",
				"  ]",
				"T[n_, k_] := 2^Length[PreT[n, k]]",
				"A004736[n_] := Binomial[Floor[3/2 + Sqrt[2*n]], 2] - n + 1",
				"A002260[n_] := n - Binomial[Floor[1/2 + Sqrt[2*n]], 2]",
				"a[n_] := T[A004736[n], A002260[n]]",
				"Table[a[n], {n, 1, 90}]"
			],
			"program": [
				"(PARI) maxp(n) = if (n==1, 1, vecmax(factor(n)[,1]));",
				"T(n, k) = {pmax = max(maxp(n), maxp(k)); x = 0; forprime(p=2, pmax, if ((valuation(n, p) \u003e= valuation(k, p)) \u0026\u0026 (valuation(k, p) \u003e 0), x ++);); 2^x;} \\\\ _Michel Marcus_, Oct 28 2018"
			],
			"xref": [
				"Cf. A319581 (an additive variant).",
				"Cf. A001221, A034444, A089913."
			],
			"keyword": "nonn,tabl",
			"offset": "1,5",
			"author": "_Luc Rousseau_, Sep 24 2018",
			"references": 1,
			"revision": 17,
			"time": "2018-12-19T18:56:58-05:00",
			"created": "2018-12-19T18:56:58-05:00"
		}
	]
}