{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002534",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2534,
			"id": "M2058 N0814",
			"data": "0,1,2,13,44,205,806,3457,14168,59449,246410,1027861,4273412,17797573,74055854,308289865,1283082416,5340773617,22229288978,92525540509,385114681820,1602959228221,6671950592822,27770534239633,115588623814664",
			"name": "a(n) = 2*a(n-1) + 9*a(n-2).",
			"comment": [
				"For n\u003e=2, a(n) equals the permanent of the (n-1)X(n-1) tridiagonal matrix with 2's along the main diagonal, and 3's along the superdiagonal and the subdiagonal. - _John M. Campbell_, Jul 19 2011"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"A. Tarn, Approximations to certain square roots and the series of numbers connected therewith, Mathematical Questions and Solutions from the Educational Times, 1 (1916), 8-12."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A002534/b002534.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"Albert Tarn, \u003ca href=\"/A001333/a001333_1.pdf\"\u003eApproximations to certain square roots and the series of numbers connected therewith\u003c/a\u003e [Annotated scanned copy]",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2, 9)."
			],
			"formula": [
				"From _Paul Barry_, Sep 29 2004: (Start)",
				"E.g.f.: exp(x)*sinh(sqrt(10)*x)/sqrt(10).",
				"a(n) = Sum_{k=0..n} binomial(n, 2*k+1)*10^k. (End)",
				"a(n) = ((1+sqrt(10))^n - (1-sqrt(10))^n)/(2*sqrt(10)). - _Artur Jasinski_, Dec 10 2006",
				"G.f.: x/(1 - 2*x - 9*x^2) - _Iain Fox_, Jan 17 2018"
			],
			"maple": [
				"A002534:=-z/(-1+2*z+9*z**2); # [_Simon Plouffe_ in his 1992 dissertation.]"
			],
			"mathematica": [
				"Table[((1 + Sqrt[10])^n - (1 - Sqrt[10])^n)/(2 Sqrt[10]), {n, 0, 30}]] (* _Artur Jasinski_, Dec 10 2006 *)",
				"LinearRecurrence[{2, 9}, {0, 1}, 30] (* _T. D. Noe_, Aug 18 2011 *)"
			],
			"program": [
				"(Sage) [lucas_number1(n,2,-9) for n in range(0, 20)] # _Zerinvary Lajos_, Apr 22 2009",
				"(MAGMA) [Ceiling(((1+Sqrt(10))^n-(1-Sqrt(10))^n)/(2*Sqrt(10))): n in [0..30]]; // _Vincenzo Librandi_, Aug 15 2011",
				"(PARI) first(n) = Vec(x/(1 - 2*x - 9*x^2) + O(x^n), -n) \\\\ _Iain Fox_, Jan 17 2018"
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Johannes W. Meijer_, Aug 18 2011"
			],
			"references": 19,
			"revision": 50,
			"time": "2021-03-12T22:32:35-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}