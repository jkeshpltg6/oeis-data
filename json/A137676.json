{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A137676",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 137676,
			"data": "1,1,0,0,1,1,0,0,1,2,0,0,2,2,0,0,3,3,0,0,4,4,0,0,5,6,0,0,7,7,0,0,9,10,0,0,12,12,0,0,15,16,0,0,19,20,0,0,24,26,0,0,30,31,0,0,37,40,0,0,46,48,0,0,57,60,0,0,69,72,0,0,84,89,0,0,102,106,0",
			"name": "Expansion of f(-x^2, -x^3) / f(-x, -x^3) in powers of x where f(, ) is Ramanujan's general theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A137676/b137676.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"G. E. Andrews, \u003ca href=\"http://dx.doi.org/10.1090/cbms/066\"\u003eq-series\u003c/a\u003e, CBMS Regional Conference Series in Mathematics, 66, Amer. Math. Soc. 1986, see p. 36, Eq. (4.11). MR0858826 (88b:11063).",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of f(-x^2) * f(-x^5) / (f(-x^4) * f(-x, -x^4)) in powers of x where f(, ) is Ramanujan's general theta function.",
				"Expansion of (f(-x^13, -x^17) + x * f(-x^7, -x^23)) / f(-x^4) in powers of x where f(, ) is Ramanujan's general theta function.",
				"Euler transform of period 20 sequence [ 1, -1, 0, 1, 0, 0, 0, 0, 1, -1, 1, 0, 0, 0, 0, 1, 0, -1, 1, 0, ...].",
				"G.f.: Sum_{k\u003e=0} x^k^2 / (Product_{j=1..k} 1 - x^(4*j)).",
				"a(4*n) = A122129(n). a(4*n + 1) = A122135(n). a(4*n + 2) = a(4*n + 3) = 0.",
				"G.f.: (Sum_{k in Z} (-1)^k^2 * x^(k * (5*k + 1) / 2)) / (Sum_{k in Z} (-1)^k^2 * x^(k * (2*k + 1))). - _Michael Somos_, Oct 08 2015"
			],
			"example": [
				"G.f. = 1 + x + x^4 + x^5 + x^8 + 2*x^9 + 2*x^12 + 2*x^13 + 3*x^16 + 3*x^17 + ...",
				"G.f. = 1/q + q^9 + q^39 + q^49 + q^79 + 2*q^89 + 2*q^119 + 2*q^129 + 3*q^159 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x^2] / (QPochhammer[ x^4] QPochhammer[ x, x^5] QPochhammer[ x^4, x^5]), {x, 0, n}]; (* _Michael Somos_, Oct 08 2015 *)",
				"a[ n_] := If[ n \u003c 0, 0, SeriesCoefficient[ Sum[ x^(k^2) / QPochhammer[ x^4, x^4, k], {k, 0, Sqrt @ n}], {x, 0, n}]]; (* _Michael Somos_, Oct 08 2015 *)",
				"a[ n_] := SeriesCoefficient[ Sqrt[2] x^(1/8) QPochhammer[ x^2, x^5] QPochhammer[ x^3, x^5] QPochhammer[ x^5] / EllipticTheta[ 2, Pi/4, x^(1/2)], {x, 0, n}]; (* _Michael Somos_, Oct 08 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, polcoeff( sum(k=0, sqrtint(n), x^k^2 / prod(i=1, k, 1 - x^(4*i), 1 + x * O(x^(n - k^2)))), n))};"
			],
			"xref": [
				"Cf. A122129, A122135."
			],
			"keyword": "nonn",
			"offset": "0,10",
			"author": "_Michael Somos_, Feb 04 2008",
			"references": 1,
			"revision": 15,
			"time": "2021-03-12T22:24:45-05:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}