{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A103632",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 103632,
			"data": "1,0,1,1,2,2,3,4,6,8,11,15,21,29,40,55,76,105,145,200,276,381,526,726,1002,1383,1909,2635,3637,5020,6929,9564,13201,18221,25150,34714,47915,66136,91286,126000,173915,240051,331337,457337,631252,871303,1202640",
			"name": "Expansion of (1 - x + x^2)/(1 - x - x^4).",
			"comment": [
				"Diagonal sums of A103631.",
				"The Kn11 sums, see A180662, of triangle A065941 equal the terms of this sequence without a(0) and a(1). - _Johannes W. Meijer_, Aug 11 2011",
				"For n \u003e= 2, a(n) is the number of palindromic compositions of n-2 with parts in {2,1,3,5,7,9,...}. The generating function follows easily from Theorem 1,2 of the Hoggatt et al. reference. Example: a(9) = 8 because we have 7, 151, 11311, 232, 313, 12121, 21112, and 1111111. - _Emeric Deutsch_, Aug 17 2016",
				"Essentially the same as A003411. - _Georg Fischer_, Oct 07 2018"
			],
			"link": [
				"Muniru A Asiru, \u003ca href=\"/A103632/b103632.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"V. E. Hoggatt, Jr., and Marjorie Bicknell, \u003ca href=\"http://www.fq.math.ca/Scanned/13-4/hoggatt1.pdf\"\u003ePalindromic compositions\u003c/a\u003e, Fibonacci Quart., Vol. 13(4), 1975, pp. 350-356.",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,0,0,1)."
			],
			"formula": [
				"G.f.: (1 - x + x^2)/(1 - x - x^4).",
				"a(n) = a(n-1) + a(n-4) with a(0)=1, a(1)=0, a(2)=1 and a(3)=1.",
				"a(n) = Sum_{k=0..floor(n/2)} binomial(floor((2*n-3*k-1)/2), n-2*k).",
				"a(n) = A003269(n+1) - A003269(n-4), n \u003e 4."
			],
			"maple": [
				"A103632 := proc(n): add( binomial(floor((2*n-3*k-1)/2), n-2*k), k=0..floor(n/2)) end: seq(A103632(n), n=0..46); # _Johannes W. Meijer_, Aug 11 2011"
			],
			"mathematica": [
				"LinearRecurrence[{1,0,0,1}, {1,0,1,1}, 50] (* _G. C. Greubel_, Mar 10 2019 *)"
			],
			"program": [
				"(GAP) a:=[1,0,1,1];;  for n in [5..50] do a[n]:=a[n-1]+a[n-4]; od; a; # _Muniru A Asiru_, Oct 07 2018",
				"(PARI) my(x='x+O('x^50)); Vec((1-x+x^2)/(1-x-x^4)) \\\\ _G. C. Greubel_, Mar 10 2019",
				"(MAGMA) I:=[1,0,1,1]; [n le 4 select I[n] else Self(n-1) + Self(n-4): n in [1..50]]; // _G. C. Greubel_, Mar 10 2019",
				"(Sage) ((1-x+x^2)/(1-x-x^4)).series(x, 50).coefficients(x, sparse=False) # _G. C. Greubel_, Mar 10 2019"
			],
			"xref": [
				"Cf. A275446."
			],
			"keyword": "easy,nonn",
			"offset": "0,5",
			"author": "_Paul Barry_, Feb 11 2005",
			"ext": [
				"Formula corrected by _Johannes W. Meijer_, Aug 11 2011"
			],
			"references": 3,
			"revision": 35,
			"time": "2019-03-10T19:33:19-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}