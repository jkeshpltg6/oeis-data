{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A122495",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 122495,
			"data": "1,3,22,5,32,42,312,2112,7,52,43,322,313,2212,21112,62,512,44,413,4112,332,3212,3113,31112,2312,2222,22112,9,72,63,54,522,513,423,4212,4122,41112,342,333,3222,3213,31212,31122,311112,2412,2322,23112,22122,21312",
			"name": "Integers corresponding to rational knots in Conway's enumeration.",
			"comment": [
				"\"Conway's motivation for studying tangles was to extend the [knot and link] catalogues.... here we shall concentrate on finding the first few rational links.",
				"\"The problem is reduced to listing sequences of integers and noting which sequences lead to isotopic links.",
				"\"The technique is so powerful that Conway claims to have verified the Tait-Little tables 'in an afternoon'.",
				"\"He then went on to list the 100-crossings knots and 10-crossing links.... A rational link (or its mirror image) has a regular continued fraction expansion in which all the integers are positive....",
				"\"We can discard all sequences that end in a 1 and that makes the regular sequence unique.... we do not need to keep both a sequence and its reverse.",
				"\"Applying these simple rules to the partitions of the first four integers, we see that we keep only the sequences shown in bold: 1, 2, 11, 3, 21, 12, 111, 4, 31, 22, 13, 211, 121, 112, 1111.\" [typographically, the bold subsequence is 1, 2, 3, 4, 22] \"These sequences correspond to the trivial knot, the Hopf link, the trefoil, the (2,4) torus link and the figure 8 knot.",
				"\"Continuing in this fashion, we find that for knots and links with up to seven crossings, the sequences for rational knots are: 3, 22, 5, 32, 42, 312, 2112, 7, 52, 43, 322, 313, 2212, 21112 and the sequences for rational 2-component links are 2, 4, 212, 6, 33, 222, 412, 232, 3112.... we see that a sequence represents an amphicheiral knot or link only if the sequence is palindromic (equal to its reverse) and of even length (n even).",
				"\"This shows that the only amphicheiral knots in the list are the figure-8 knot (sequence 22) and the knot 6_3 (sequence 2112); all of the links are cheiral....\" [Cromwell]",
				"The ordering among the terms with the same sum of digits (i.e., number of crossings) is the inverse lexicographical. Each term is actually an ordered set of positive integers, concatenated; as long as all integers are 1-digit, it's not a problem, but a(97) requires \"digit\" 11, so at that point the sequence becomes not fully well-defined. An irregular array of these numbers would be well-defined. - _Andrey Zabolotskiy_, May 22 2017"
			],
			"reference": [
				"Peter R. Cromwell, Knots and Links, Cambridge University Press, 2004, pp. 209-211."
			],
			"link": [
				"Alain Caudron, \u003ca href=\"http://sites.mathdoc.fr/PMO/PDF/C_CAUDRON_82_04.pdf\"\u003eClassification des noeuds et des enlancements\u003c/a\u003e (see p. 168).",
				"J. H. Conway, \u003ca href=\"http://www.maths.ed.ac.uk/~aar/papers/conway.pdf\"\u003eAn enumeration of knots and links and some of their algebraic properties\u003c/a\u003e, 1970. Computational Problems in Abstract Algebra (Proc. Conf., Oxford, 1967) pp. 329-358 Pergamon, Oxford.",
				"\u003ca href=\"/index/K#knots\"\u003eIndex entries for sequences related to knots\u003c/a\u003e"
			],
			"example": [
				"a(1) = 1 because 1 corresponds to the trivial knot.",
				"a(2) = 3 because 3 corresponds to the trefoil.",
				"a(3) = 22 because 22 corresponds to the figure-8 knot."
			],
			"mathematica": [
				"whereTangle[{n_}] := If[EvenQ[n], 1, 2];",
				"whereTangle[{rest__, n_}] := Switch[whereTangle[{rest}], 1, 3, 2, Switch[whereTangle[{n}], 1, 2, 2, 1, 3, 3], 3, whereTangle[{n}]];",
				"FromDigits /@ Prepend[Select[Flatten[Table[Reverse@SortBy[Flatten[Permutations /@ IntegerPartitions[n], 1], PadRight[#, n] \u0026], {n, 10}], 1], OrderedQ[{Reverse[#], #}] \u0026\u0026 Last[#] != 1 \u0026\u0026 whereTangle[#] != 1 (*change to \"==1\" for rational 2-component links*) \u0026], {1}]",
				"(* _Andrey Zabolotskiy_, May 22 2017 *)"
			],
			"xref": [
				"Cf. A002863, A002864, A173637, A018240, A078666."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Jonathan Vos Post_, Sep 16 2006",
			"ext": [
				"Sequence edited and more terms added by _Andrey Zabolotskiy_, May 22 2017"
			],
			"references": 2,
			"revision": 31,
			"time": "2017-08-15T13:54:43-04:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}