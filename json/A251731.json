{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A251731",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 251731,
			"data": "2,1,2,16,32,145,62,1363,3458,19492,58928,89308,70028,1594318,1890551,189871,31401806,47918575,190704887,163454602,502048577,9481323661,11627845304,34656488290,115450061084,286130228125,2303721331049,1569269836240,22013516320412",
			"name": "Least k such that k^3 + q is divisible by 3^n where q is the n-th number congruent to 1 or -1 (mod 18).",
			"comment": [
				"It is known that k always exists if q is congruent to +-1 mod 18.",
				"The numbers congruent to 1 or -1 (mod 18) are 1, 17, 19, 35, 37, ... = {A161705} UNION {A239129}.",
				"For n \u003e= 2, k^3 == (9 - 18*n - 7*(-1)^n)/2 (mod 3^n) if and only if k - a(n) is divisible by 3^(n-1). - _Jinyuan Wang_, Feb 13 2020"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A251731/b251731.txt\"\u003eTable of n, a(n) for n = 1..2095\u003c/a\u003e"
			],
			"example": [
				"a(1) = 2 because the first number of the form +-1 (mod 18) is 1, and 2^3 + 1 = 9 = 3*3^1;",
				"a(2) = 1 because the second number of the form +-1 (mod 18) is 17, and 1^3 + 17 = 18 = 2*3^2;",
				"a(3) = 2 because the third number of the form +-1 (mod 18) is 19, and 2^3 + 19 = 27 = 3^3;",
				"a(4)= 16 because the fourth number of the form +-1 (mod 18) is 35, and 16^3 + 35 = 4131 = 51*3^4."
			],
			"maple": [
				"f:= proc(n) local q,R,k;",
				"  if n::odd then q:= 9*n-8 else q:= 9*n-1 fi;",
				"  min(map(subs,[msolve(k^3+q,3^n)],k))",
				"end proc:",
				"map(f, [$1..30]); # _Robert Israel_, Dec 23 2018"
			],
			"mathematica": [
				"lst1={1};Do[lst1=Union[lst1,Union[{18*n+1},{18*n-1}]],{n,1,10}];lst={};Do[k=1;While[Mod[k^3+lst1[[n]],3^n]!=0,k++];Print[n,\" \",k],{n,1,10}];lst"
			],
			"program": [
				"(PARI) a(n) = {if (n % 2, q = 9*(n-1)+1, q = 9*n-1); m = 3^n; k = 1; while ((k^3+q) % m, k++); k;} \\\\ _Michel Marcus_, Jan 07 2015"
			],
			"xref": [
				"Cf. A161705, A239129."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Michel Lagneau_, Dec 07 2014",
			"references": 1,
			"revision": 25,
			"time": "2020-02-14T03:22:09-05:00",
			"created": "2015-01-15T13:10:21-05:00"
		}
	]
}