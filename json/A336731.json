{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A336731",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 336731,
			"data": "4,0,0,14,8,0,20,48,4,60,80,28,68,224,68,148,368,124,224,616,268,336,1008,420,384,1672,648,712,2208,972,972,3120,1464,1300,4304,1996,1496,6040,2788,2044,7936,3580,2612,10224,4672,3540,12656,5980,4224,16104,7676,5484,19648,9500",
			"name": "Three-column table read by rows: row n gives [number of triangle-triangle, triangle-quadrilateral, quadrilateral-quadrilateral] contacts for a row of n adjacent congruent rectangles divided by drawing diagonals of all possible rectangles (cf. A331452).",
			"comment": [
				"For a row of n adjacent rectangles the only polygons formed when dividing all possible rectangles along their diagonals are 3-gons (triangles) and 4-gons (quadrilaterals). Hence the only possible edge-sharing contacts are 3-gons with 3-gons, 3-gons with 4-gons, and 4-gons with 4-gons. This sequence lists the number of these three possible combinations for a row of n adjacent rectangles. Note that the edges along the outside of the n adjacent rectangles are not counted as they are only in one n-gon.",
				"These are graphs T(1,n) described in A331452. - _N. J. A. Sloane_, Aug 03 2020"
			],
			"link": [
				"Scott R. Shannon, \u003ca href=\"/A336731/a336731_3.png\"\u003eImage of the rectangles for n = 1\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A336731/a336731.png\"\u003eImage of the rectangles for n = 2\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A336731/a336731_1.png\"\u003eImage of the rectangles for n = 3\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A336731/a336731_2.png\"\u003eImage of the rectangles for n = 4\u003c/a\u003e."
			],
			"formula": [
				"Sum of row t = A331757(t) - 2(t + 1)."
			],
			"example": [
				"a(1) = 4, a(2) = 0, a(3) = 0. A single rectangle divided along its diagonals consists of four 3-gons, four edges, and no 4-gons. Therefore there are only four 3-gon-to-3-gon contacts. See the link image for n = 1.",
				"a(4) = 14, a(5) = 8, a(6) = 0. Two adjacent rectangles divided along all diagonals consists of fourteen 3-gons and two 4-gons. The two 4-gons are separated and thus share all their edges, eight in total, with 3-gons. There are fourteen pairs of 3-gon-to-3-gon contacts. See the link image for n = 2.",
				"a(7) = 20, a(8) = 48, a(9) = 4. Three adjacent rectangles divided along all diagonals consists of thirty-two 3-gons and fourteen 4-gons. There are two groups of three adjacent 4-gons, so there are four 4-gons-to-4-gon contacts. These, along with the other 4-gons, share 48 edges with 3-gons. There are also twenty 3-gon-to-3-gon contacts. See the link image for n = 3.",
				".",
				"The table begins:",
				"4,0,0;",
				"14,8,0;",
				"20,48,4;",
				"60,80,28;",
				"68,224,68;",
				"148,368,124;",
				"224,616,268;",
				"336,1008,420;",
				"384,1672,648;",
				"712,2208,972;",
				"972,3120,1464;",
				"1300,4304,1996;",
				"1496,6040,2788;",
				"2044,7936,3580;",
				"2612,10224,4672;",
				"3540,12656,5980;",
				"4224,16104,7676;",
				"5484,19648,9500;",
				"6568,24216,11936;",
				"7836,29616,14468;",
				"See A306302 for a count of the regions and images for other values of n."
			],
			"xref": [
				"Cf. A306302, A331452, A331755, A331757, A333288."
			],
			"keyword": "nonn,tabf",
			"offset": "1,1",
			"author": "_Scott R. Shannon_, Aug 02 2020",
			"references": 2,
			"revision": 30,
			"time": "2020-08-03T10:05:11-04:00",
			"created": "2020-08-03T09:57:14-04:00"
		}
	]
}