{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A100597",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 100597,
			"data": "1,1,2,5,14,49,258,1385,1342,-13739,1727362,20549165,-892047378,-13084315271,979519187138,16158974238545,-1747908612654946,-32246548780758179,4903305033480792642,100032668564662494485,-20685044415403212103730,-462550882810484735564351",
			"name": "Based on the first matrix inverse of transformed Bernoulli numbers as defined in the Comments line.",
			"comment": [
				"A family of polynomials is defined by P(0,x) = u(0), P(n,x) = u(n) +x*Sum_{i=0..n-1} u(i)*P(n-i-1,x), where u(n) is the n-th Bernoulli number. The coefficients of P(n-1,x) are used to fill the n-th row of the infinite lower triangle matrix M. Then a(n) is given by M^(-1)[n,1] * n!."
			],
			"reference": [
				"P. Curtz, Gazette des Mathematiciens, 1992, 52, p.44.",
				"P. Flajolet, X. Gourdon and B. Salvy, Gazette des Mathematiciens, 1993, 55, pp.67-78."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A100597/b100597.txt\"\u003eTable of n, a(n) for n = 1..100\u003c/a\u003e"
			],
			"example": [
				"a(3) = 2, because M = [1; -1/2 1; 1/6 -1 1; ...], M^(-1) = [1; 1/2 1; 1/3 1 1; ...], and (1/3)*3! = 2."
			],
			"maple": [
				"P:= proc(n) option remember; local i, u, x; u:= bernoulli; `if`(n=0, u(0), unapply(expand(u(n) +x *add(u(i) *P(n-i-1)(x), i=0..n-1)), x)) end: a:= n-\u003e (1/Matrix(n, (i, j)-\u003e coeff(P(i-1)(x), x, j-1)))[n, 1] *n!: seq(a(n), n=1..30);  # _Alois P. Heinz_, Oct 12 2009"
			],
			"mathematica": [
				"p[0, x_] = BernoulliB[0]; p[n_, x_] := p[n, x] = BernoulliB[n] + x*Sum[BernoulliB[i]*p[n-i-1, x], {i, 0, n-1}]; t[m_] := Table[ PadRight[CoefficientList[p[n, x], x], m+1], {n, 0, m}]; mmax = 20; Inverse[t[mmax-1]][[All, 1]]*Range[mmax]!",
				"(* _Jean-François Alcover_, Jun 29 2011 *)"
			],
			"xref": [
				"Cf. A027641/A027642, A130620, A141411."
			],
			"keyword": "sign",
			"offset": "1,3",
			"author": "_Paul Curtz_, Jun 06 2007",
			"ext": [
				"Edited and more terms from _Alois P. Heinz_, Oct 12 2009"
			],
			"references": 2,
			"revision": 16,
			"time": "2015-01-14T16:05:00-05:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}