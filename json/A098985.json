{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A098985",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 98985,
			"data": "1,1,1,3,1,5,3,7,1,9,5,11,3,13,7,5,1,17,9,19,5,21,11,23,3,25,13,27,7,29,5,31,1,11,17,35,9,37,19,39,5,41,21,43,11,15,23,47,3,49,25,17,13,53,27,55,7,57,29,59,5,61,31,63,1,65,11,67,17,23,35,71,9,73,37,75,19,77,39,79",
			"name": "Denominators in series expansion of log( Sum_{m=-oo,oo} q^(m^2) ).",
			"comment": [
				"a(n) is the denominator of Sum_{odd d|n} 1/d. See Sumit Kumar Jha link. - _Michel Marcus_, Jul 21 2020"
			],
			"link": [
				"Sumit Kumar Jha, \u003ca href=\"https://arxiv.org/abs/2007.04243\"\u003eAn identity for the sum of inverses of odd divisors of n in terms of the number of representations of n as a sum of r squares\u003c/a\u003e, arXiv:2007.04243 [math.GM], 2020."
			],
			"example": [
				"2*q-2*q^2+8/3*q^3-2*q^4+12/5*q^5-8/3*q^6+16/7*q^7-2*q^8+26/9*q^9-..."
			],
			"maple": [
				"A098985_list := proc(n::integer)",
				"    local q,m,nsq ;",
				"    nsq := floor(sqrt(n)) ;",
				"    add(q^(m^2),m=-nsq-1..nsq+1) ;",
				"    log(%) ;",
				"    taylor(%,q=0,n+1) ;",
				"    [seq( denom(coeftayl(%,q=0,i)) ,i=1..n) ] ;",
				"end proc:",
				"A098985_list(200) ; # _R. J. Mathar_, Jul 16 2020",
				"A336114 := proc(n::integer)",
				"    local a ;",
				"    for d in numtheory[divisors](n) do",
				"        if type(d,'odd') then",
				"            a := a+1/d ;",
				"        end if;",
				"    end do;",
				"    denom(a) ;",
				"end proc:",
				"seq(A336114(n),n=1..70) ; # _R. J. Mathar_, Jul 16 2020"
			],
			"mathematica": [
				"Denominator[CoefficientList[Series[Log[Sum[q^m^2, {m, -Infinity, Infinity}]], {q, 0, 79}], q]] (* _L. Edson Jeffery_, Jul 14 2014 *)",
				"a[n_] := Denominator @ DivisorSum[n, 1/# \u0026, OddQ[#] \u0026]; Array[a, 100] (* _Amiram Eldar_, Jul 09 2020 *)"
			],
			"program": [
				"(PARI) lista(nn) = {my(k = sqrtint(nn), s = sum(m=-k-1, k+1, x^(m^2)) + O(x^nn)); apply(x-\u003edenominator(x), Vec(log(s)));} \\\\ _Michel Marcus_, Jul 17 2020",
				"(PARI) a(n) = denominator(sumdiv(n, d, if (d%2, 1/d))); \\\\ _Michel Marcus_, Jul 21 2020"
			],
			"xref": [
				"Cf. A000122, A098984, A098986.",
				"Cf. A336113 (numerators)."
			],
			"keyword": "nonn,frac",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_, Oct 24 2004",
			"references": 3,
			"revision": 20,
			"time": "2020-07-21T03:11:53-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}