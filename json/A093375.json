{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A093375",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 93375,
			"data": "1,2,1,3,4,1,4,9,6,1,5,16,18,8,1,6,25,40,30,10,1,7,36,75,80,45,12,1,8,49,126,175,140,63,14,1,9,64,196,336,350,224,84,16,1,10,81,288,588,756,630,336,108,18,1,11,100,405,960,1470,1512,1050,480,135,20,1,12",
			"name": "Array T(m,n) read by ascending antidiagonals: T(m,n) = m*binomial(n+m-2, n-1) for m, n \u003e= 1.",
			"comment": [
				"Number of n-long m-ary words avoiding the pattern 1-1'2'.",
				"T(n,n+1) = Sum_{i=1..n} T(n,i).",
				"Exponential Riordan array [(1+x)e^x, x] as a number triangle. - _Paul Barry_, Feb 17 2009",
				"From _Peter Bala_, Jul 22 2014: (Start)",
				"Call this array M and for k = 0,1,2,... define M(k) to be the lower unit triangular block array",
				"/I_k 0\\",
				"\\ 0  M/",
				"having the k X k identity matrix I_k as the upper left block; in particular, M(0) = M. The infinite matrix product M(0)*M(1)*M(2)*..., which is clearly well-defined, is equal to A059298. (End)"
			],
			"link": [
				"Muniru A Asiru, \u003ca href=\"/A093375/b093375.txt\"\u003eAntidiagonals, n=1..100 flattened\u003c/a\u003e",
				"S. Kitaev and T. Mansour, \u003ca href=\"https://arxiv.org/abs/math/0210023\"\u003ePartially ordered generalized patterns and k-ary words\u003c/a\u003e, arXiv:math/0210023 [math.CO], 2002.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Sheffer_sequence\"\u003eSheffer sequence\u003c/a\u003e"
			],
			"formula": [
				"Triangle = P*M, the binomial transform of the infinite bidiagonal matrix M with (1,1,1,...) in the main diagonal and (1,2,3,...) in the subdiagonal, and zeros elsewhere. P = Pascal's triangle as an infinite lower triangular matrix. - _Gary W. Adamson_, Nov 05 2006",
				"From _Peter Bala_, Sep 20 2012: (Start)",
				"E.g.f. for triangle: (1 + z)*exp((1 + x)*z) = 1 + (2 + x)*z + (3 + 4*x + x^2)*z^2/2! + ....",
				"O.g.f. for triangle: (1 - x*z)/(1 - z - x*z)^2 = 1 + (2 + x)*z + (3 + 4*x + x^2)*z^2 + ....",
				"The n-th row polynomial R(n,x) of the triangle equals (1+x)^n + n*(1+x)^(n-1) for n \u003e= 0 and satisfies d/dx(R(n,x)) = n*R(n-1,x), as well as R(n,x+y) = Sum_{k = 0..n} binomial(n,k)*R(k,x)*y^(n-k). The row polynomials are a Sheffer sequence of Appell type.",
				"Matrix inverse of the triangle is a signed version of A073107. (End)",
				"From _Tom Copeland_, Oct 20 2015: (Start)",
				"With offset 0 and D = d/dx, the raising operator for the signed row polynomials P(n,x) is RP = x - d{log[e^D/(1-D)]}/dD = x - 1 - 1/(1-D) =  x - 2 - D - D^2 + ..., i.e., RP P(n,x) = P(n+1,x).",
				"The e.g.f. for the signed array is (1-t) * e^(-t) * e^(x*t).",
				"From the Appell formalism, the row polynomials PI(n,x) of A073107 are the umbral inverse of this entry's row polynomials; that is, P(n,PI(.,x)) = x^n = PI(n,P(.,x)) under umbral composition. (End)",
				"From _Petros Hadjicostas_, Nov 01 2019: (Start)",
				"As a triangle, we let S(n,k) = T(n-k+1, k+1) = (n-k+1)*binomial(n, k) for n \u003e= 0 and 0 \u003c= k \u003c= n). See the example below.",
				"As stated above by _Peter Bala_, Sum_{n,k \u003e= 0} S(n,k)*z^n*x^k = (1 - x*z)/(1 - z -x*z)^2.",
				"Also, Sum_{n, k \u003e= 0} S(n,k)*z^n*x^k/n! = (1+z)*exp((1+x)*z).",
				"As he also states, the n-th row polynomial is R(n,x) = Sum_{k = 0..n} S(n, k)*x^k = (1 + x)^n + n*(1 + x)^(n-1).",
				"If we define the signed triangle S*(n,k) = (-1)^(n+k) * S(n,k) = (-1)^(n+k) * T(n-k+1, k+1), as _Tom Copeland_ states, Sum_{n,k \u003e= 0} S^*(n,k)*t^n*x^k/n! = (1-t)*exp((1-x)*(-t)) = (1-t) * e^(-t) * e^(x*t).",
				"Apparently, S*(n,k) = A103283(n,k).",
				"As he says above, the signed n-th row polynomial is P(n,x) =  (-1)^n*R(n,-x) = (x - 1)^n - n*(x - 1)^(n-1).",
				"According to _Gary W. Adamson_, P(n,x) is \"the monic characteristic polynomial of the n X n matrix with 2's on the diagonal and 1's elsewhere.\" (End)"
			],
			"example": [
				"Array T(m,n) (with rows m \u003e= 1 and columns n \u003e= 1) begins as follows:",
				"   1   1   1   1   1   1 ...",
				"   2   4   6   8  10  12 ...",
				"   3   9  18  30  45  63 ...",
				"   4  16  40  80 140 224 ...",
				"   5  25  75 175 350 630 ...",
				"   ...",
				"Triangle S(n,k) = T(n-k+1, k+1) begins",
				".n\\k.|....0....1....2....3....4....5....6",
				"= = = = = = = = = = = = = = = = = = = = =",
				"..0..|....1",
				"..1..|....2....1",
				"..2..|....3....4....1",
				"..3..|....4....9....6....1",
				"..4..|....5...16...18....8....1",
				"..5..|....6...25...40...30...10....1",
				"..6..|....7...36...75...80...45...12....1",
				"..."
			],
			"mathematica": [
				"nmax = 10;",
				"T = Transpose[CoefficientList[# + O[z]^(nmax+1), z]\u0026 /@ CoefficientList[(1 - x z)/(1 - z - x z)^2 + O[x]^(nmax+1), x]];",
				"row[n_] := T[[n+1, 1 ;; n+1]];",
				"Table[row[n], {n, 0, nmax}] // Flatten (* _Jean-François Alcover_, Aug 07 2018 *)"
			],
			"program": [
				"(GAP) nmax:=14;; T:=List([1..nmax],n-\u003eList([1..nmax],k-\u003ek*Binomial(n+k-2,n-1)));;",
				"b:=List([2..nmax],n-\u003eOrderedPartitions(n,2));;",
				"a:=Flat(List([1..Length(b)],i-\u003eList([1..Length(b[i])],j-\u003eT[b[i][j][1]][b[i][j][2]]))); # _Muniru A Asiru_, Aug 07 2018",
				"(Sage) # uses[riordan_array from A256893]",
				"riordan_array((1+x)*exp(x), x, 8, exp=true) # _Peter Luschny_, Nov 02 2019"
			],
			"xref": [
				"Rows include A045943. Columns include A002411, A027810.",
				"Main diagonal is A037965. Subdiagonals include A002457.",
				"Antidiagonal sums are A001792.",
				"See A103283 for a signed version.",
				"Cf. A103406, A059298, A073107 (unsigned inverse)."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Ralf Stephan_, Apr 28 2004",
			"references": 7,
			"revision": 77,
			"time": "2020-03-24T12:37:33-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}