{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A220338",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 220338,
			"data": "2,6,10,2,50,98,2,4802,9602,2,46099202,92198402,2,4250272665676802,8500545331353602,2,36129635465198759610694779187202,72259270930397519221389558374402,2,2610701117696295981568349760414651575095962187244375364404428802",
			"name": "A modified Engel expansion for 8*sqrt(6) - 19.",
			"comment": [
				"For a brief description of the modified Engel expansion of a real number see A220335.",
				"Let p \u003e= 2 be an integer and set Q(p) = (p - 1)*sqrt(p^2 - 1) - (p^2 - p - 1), so Q(5) = 8*sqrt(6) - 19. Iterating the identity Q(p) = 1/2 + 1/(2*(p+1)) + 1/(2*(p+1)*(2*p)) + 1/(2*(p+1)*(2*p))*Q(2*p^2-1) leads to a representation for Q(p) as an infinite series of unit fractions. The sequence of denominators of these unit fractions can be used to find the modified Engel expansion of Q(p). For further details see the Bala link. The present sequence is the case p = 5. For other cases see A220335 (p = 2), A220336 (p = 3) and A220337 (p = 4)."
			],
			"link": [
				"P. Bala, \u003ca href=\"/A220335/a220335.pdf\"\u003eA modified Engel expansion for certain quadratic irrationals\u003c/a\u003e",
				"S. Crowley, \u003ca href=\"http://arxiv.org/abs/1210.5652\"\u003eIntegral transforms of the harmonic sawtooth map, the Riemann zeta function, fractal strings, and a finite reflection formula\u003c/a\u003e, arXiv:1210.5652 [math.NT]",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Engel_expansion\"\u003eEngel Expansion\u003c/a\u003e"
			],
			"formula": [
				"Define the harmonic sawtooth map h(x) := floor(1/x)*(x*ceiling(1/x) - 1). Let x = 8*sqrt(6) - 19. Then a(1) = ceiling(1/x) and for n \u003e= 2, a(n) = floor(1/h^(n-2)(x))*ceiling(1/h^(n-1)(x)), where h^(n)(x) denotes the n-th iterate of the map h(x), with the convention h^(0)(x) = x.",
				"a(3*n+2) = 1/2*{2 + (5 + 2*sqrt(6))^(2^n) + (5 - 2*sqrt(6))^(2^n)} and",
				"a(3*n+3) = {(5 + 2*sqrt(6))^(2^n) + (5 - 2*sqrt(6))^(2^n)} both for n \u003e= 0.",
				"For n \u003e= 0, a(3*n+1) = 2. For n \u003e= 1, a(3*n+2) = 2*A084765(n)^2 and a(3*n+3) = 4*A085765(n)^2 - 2.",
				"Recurrence equations:",
				"For n \u003e= 1, a(3*n+2) = 2*{a(3*n-1)^2 - 2*a(3*n-1) + 1} and",
				"a(3*n+3) = 2*a(3*n+2) - 2.",
				"Put P(n) = product(k = 0..n} a(k). Then we have the infinite Egyptian fraction representation 8*sqrt(6) - 19 = sum {n \u003e=0} 1/P(n) = 1/2 + 1/(2*6) + 1/(2*6*10) + 1/(2*6*10*2) + 1/(2*6*10*2*50) + ...."
			],
			"xref": [
				"Cf. A084765, A220335 (p = 2), A220336 (p = 3), A220337 (p = 4)."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_Peter Bala_, Dec 12 2012",
			"references": 9,
			"revision": 9,
			"time": "2012-12-13T10:40:01-05:00",
			"created": "2012-12-12T12:20:52-05:00"
		}
	]
}