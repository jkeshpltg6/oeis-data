{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A209149",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 209149,
			"data": "1,3,1,6,5,1,12,16,7,1,24,44,30,9,1,48,112,104,48,11,1,96,272,320,200,70,13,1,192,640,912,720,340,96,15,1,384,1472,2464,2352,1400,532,126,17,1,768,3328,6400,7168,5152,2464,784,160,19,1,1536,7424",
			"name": "Triangle of coefficients of polynomials v(n,x) jointly generated with A209146; see the Formula section.",
			"comment": [
				"Alternating row sums: 1,2,2,2,2,2,2,2,2,2,2,2,2,...",
				"For a discussion and guide to related arrays, see A208510.",
				"As triangle T(n,k) with 0 \u003c= k \u003c= n, it is (3, -1, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (1, 0, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Mar 08 2012",
				"A skew triangle of A209144. - _Philippe Deléham_, Mar 08 2012",
				"Riordan array ( (1 + x)/(1 - 2*x), x/(1 - 2*x) ). Cf. A118800. Matrix inverse is a signed version of A112626. - _Peter Bala_, Jul 17 2013"
			],
			"formula": [
				"u(n,x) = u(n-1,x) + (x+1)*v(n-1,x),",
				"v(n,x) = u(n-1,x) + (x+1)*v(n-1,x) + 1,",
				"where u(1,x)=1, v(1,x)=1.",
				"As DELTA-triangle:",
				"T(n,k) = 2*T(n-1,k) + T(n-1,k-1), T(0,0) = 1, T(1,0) = 3, T(1,1) = 1, T(n,k) = 0 if k \u003c 0 or if k \u003e n. - _Philippe Deléham_, Mar 08 2012",
				"As DELTA-triangle: G.f. is (1+x)/(1-2*x-yx). - _Philippe Deléham_, Mar 08 2012"
			],
			"example": [
				"First five rows:",
				"   1;",
				"   3,  1;",
				"   6,  5,  1;",
				"  12, 16,  7, 1;",
				"  24, 44, 30, 9, 1;",
				"First three polynomials v(n,x): 1, 3 + x, 6 + 5x + x^2.",
				"v(1,x) = 1",
				"v(2,x) = 3 + x",
				"v(3,x) = (3 + x)*(2 + x)",
				"v(4,x) = (3 + x)*(2 + x)^2",
				"v(5,x) = (3 + x)*(2 + x)^3",
				"v(n,x) = (3 + x)*(2 + x)^(n-2)for n \u003e 1. - _Philippe Deléham_, Mar 08 2012"
			],
			"mathematica": [
				"u[1, x_] := 1; v[1, x_] := 1; z = 16;",
				"u[n_, x_] := u[n - 1, x] + (x + 1)*v[n - 1, x];",
				"v[n_, x_] := u[n - 1, x] + (x + 1)*v[n - 1, x] + 1;",
				"Table[Expand[u[n, x]], {n, 1, z/2}]",
				"Table[Expand[v[n, x]], {n, 1, z/2}]",
				"cu = Table[CoefficientList[u[n, x], x], {n, 1, z}];",
				"TableForm[cu]",
				"Flatten[%]    (* A209148 *)",
				"Table[Expand[v[n, x]], {n, 1, z}]",
				"cv = Table[CoefficientList[v[n, x], x], {n, 1, z}];",
				"TableForm[cv]",
				"Flatten[%]    (* A209149 *)"
			],
			"xref": [
				"Cf. A209144, A209146, A209148, A208510.",
				"Cf. A084938, A112626."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Mar 07 2012",
			"references": 5,
			"revision": 18,
			"time": "2020-01-22T03:35:49-05:00",
			"created": "2012-03-07T18:28:21-05:00"
		}
	]
}