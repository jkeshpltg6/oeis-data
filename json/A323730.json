{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A323730",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 323730,
			"data": "1,1,2,1,3,1,28,40,1,5,9,45,1,1,7,1,225,1,153,1,640,1,11,441,2541,4851,1,6348,1,13,25,325,1,19474560,1,1,976,1,17,1089,9537,18513,1,1225,1,19,1,1521,70840000,107747640000,1,81,1,1,23,1,343000,3763008,245790720",
			"name": "Table read by rows: row n lists every number j whose n-th power has exactly j divisors.",
			"comment": [
				"Row n lists every j such that tau(j^n) = j.",
				"Since tau(1^n) = tau(1) = 1 for all n, every row of the table includes 1 as a term.",
				"Each prime p appears as a term in row p-1 since, for n=p-1, tau(p^n) = tau(p^(p-1)) = p."
			],
			"link": [
				"Jon E. Schoenfield, \u003ca href=\"/A323730/b323730.txt\"\u003eTable of n, a(n) for n = 0..282\u003c/a\u003e (all terms of rows 0..100)",
				"Jon E. Schoenfield, \u003ca href=\"/A323730/a323730.txt\"\u003eRows 0..100 of the table\u003c/a\u003e",
				"Jon E. Schoenfield, \u003ca href=\"/A323730/a323730_2.txt\"\u003eMagma program for computing rows 0..23 of the table\u003c/a\u003e"
			],
			"formula": [
				"A073049(n) = T(n,2) if row n contains more than 1 term, 0 otherwise.",
				"A323731(n) is the number of terms in row n.",
				"A323732 lists the numbers n such that row n contains only the single term 1.",
				"A323733 lists the numbers n such that row n contains more than one term; i.e., A323733 is the complement of A323732.",
				"A323734(n) = T(n, A323731(n)) is the largest term in row n."
			],
			"example": [
				"Row n=3 includes 28 as a term because tau(28^3) = tau((2^2 * 7)^3) = tau(2^6 * 7^3) = (6+1)*(3+1) = 7*4 = 28.",
				"Row n=3 includes 40 as a term because tau(40^3) = tau((2^3 * 5)^3) = tau(2^9 * 5^3) = (9+1)*(3+1) = 10*4 = 40.",
				"Row n=5 includes no terms other than 1 because there exists no number j \u003e 1 such that tau(j^5) = j.",
				"Row n=23 includes 245790720 as a term because tau(245790720^23) = tau((2^11 * 3^3 * 5 * 7 * 127)^23) = tau(2^253 * 3^69 * 5^23 * 7^23 * 127^23) = (253+1)*(69+1)(23+1)*(23+1)*(23+1) = 254*70*24^3 = 245790720.",
				"Table begins as follows:",
				"   n | row n",
				"  ---+---------------------------------",
				"   0 | 1;",
				"   1 | 1, 2;",
				"   2 | 1, 3;",
				"   3 | 1, 28, 40;",
				"   4 | 1, 5, 9, 45;",
				"   5 | 1;",
				"   6 | 1, 7;",
				"   7 | 1, 225;",
				"   8 | 1, 153;",
				"   9 | 1, 640;",
				"  10 | 1, 11, 441, 2541, 4851;",
				"  11 | 1, 6348;",
				"  12 | 1, 13, 25, 325;",
				"  13 | 1, 19474560;",
				"  14 | 1;",
				"  15 | 1, 976;",
				"  16 | 1, 17, 1089, 9537, 18513;",
				"  17 | 1, 1225;",
				"  18 | 1, 19;",
				"  19 | 1, 1521, 70840000, 107747640000;",
				"  20 | 1, 81;",
				"  21 | 1;",
				"  22 | 1, 23;",
				"  23 | 1, 343000, 3763008, 245790720;"
			],
			"xref": [
				"Cf. A073049 (Least m \u003e 1 such that m^n has m divisors, or 0 if no such m exists).",
				"Cf. A000005, A323731, A323732, A323733, A323734."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_Jon E. Schoenfield_, Jan 25 2019",
			"references": 6,
			"revision": 16,
			"time": "2019-01-27T08:46:47-05:00",
			"created": "2019-01-27T08:46:47-05:00"
		}
	]
}