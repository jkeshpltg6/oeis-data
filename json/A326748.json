{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A326748",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 326748,
			"data": "1,1,3,4,1,1,15,64,48,8,4,4,35,256,8640,576,216,144,36,36,315,16384,430080,1024,138240,4608,6912,576,576,576,693,65536,387072000,3686400,4838400,30720,576000,115200,43200,11520,14400,14400",
			"name": "Triangular array, read by rows: T(n,k) = denominator of Jtilde_k(n), 1 \u003c= k \u003c= 2*n+2.",
			"comment": [
				"When a general definition was made in a recent paper, it was slightly different from the previous definition. Please check the annotation on page 15 of the paper in 2019."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A326748/b326748.txt\"\u003eRows n = 0..15, flattened\u003c/a\u003e",
				"Kazufumi Kimoto, Masato Wakayama, \u003ca href=\"http://doi.org/10.2206/kyushujm.60.383\"\u003eApéry-like numbers arising from special values of spectral zeta functions for non-commutative harmonic oscillators\u003c/a\u003e, Kyushu Journal of Mathematics, Vol. 60 (2006) No. 2 p. 383-404 (see Table 2).",
				"Kazufumi Kimoto, Masato Wakayama, \u003ca href=\"https://arxiv.org/abs/1905.01775\"\u003eApéry-like numbers for non-commutative harmonic oscillators and automorphic integrals\u003c/a\u003e, arXiv:1905.01775 [math.PR], 2019. See p.22."
			],
			"formula": [
				"4*n^2 * Jtilde_k(n) = (8*n^2 - 8*n + 3) * Jtilde_k(n-1) - 4*(n - 1)^2 * Jtilde_k(n-2) + 4 * Jtilde_{k - 2}(n-1).",
				"Jtilde_n(2*n+1) = Jtilde_n(2*n+2) = 1/A001044(n). So T(n,2*n+1) = T(n,2*n+2) = A001044(n)."
			],
			"example": [
				"Triangle begins:",
				"      1,       1;",
				"    2/3,     3/4,          1,       1;",
				"   8/15,   41/64,      65/48,    11/8,     1/4,    1/4;",
				"  16/35, 147/256, 13247/8640, 907/576, 109/216, 73/144, 1/36, 1/36;"
			],
			"program": [
				"(Ruby)",
				"def f(n)",
				"  return 1 if n \u003c 2",
				"  (1..n).inject(:*)",
				"end",
				"def Jtilde(k, n)",
				"  return 0 if k == 0",
				"  return (2r ** n * f(n)) ** 2 / f(2 * n + 1) if k == 1",
				"  if n == 0",
				"    return 1 if k == 2",
				"    return 0",
				"  end",
				"  if n == 1",
				"    return 3r / 4 if k == 2",
				"    return 1      if k == 3 || k == 4",
				"    return 0",
				"  end",
				"  ((8r * n * n - 8 * n + 3) * Jtilde(k, n - 1) - 4 * (n - 1) ** 2 * Jtilde(k, n - 2) + 4 * Jtilde(k - 2, n - 1)) / (4 * n * n)",
				"end",
				"def A326748(n)",
				"  (0..n).map{|i| (1..2 * i + 2).map{|j| Jtilde(j, i).denominator}}.flatten",
				"end",
				"p A326748(10)"
			],
			"xref": [
				"Cf. A056982 (k=2), A264542(n)/2 (k=3) (By the definition of A264542, Jtilde3(1)(1) = 1/2).",
				"Cf. A001044, A326303 (numerator)."
			],
			"keyword": "nonn,frac,tabf",
			"offset": "0,3",
			"author": "_Seiichi Manyama_, Oct 19 2019",
			"references": 2,
			"revision": 29,
			"time": "2019-10-24T11:34:53-04:00",
			"created": "2019-10-23T22:34:22-04:00"
		}
	]
}