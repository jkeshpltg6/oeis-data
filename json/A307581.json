{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A307581",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 307581,
			"data": "0,2,0,6,15,5,371,742,60,787",
			"name": "Position of the first permutation of { 0 .. n-1 } occurring in the digits of Pi written in base n.",
			"comment": [
				"\"The first permutation of {0 .. n-1}\" means the first string of n distinct digits.",
				"\"Position\" means the index of the digit where this string begins, where index = p means the digit corresponding to n^-p: e.g., the first digit after the decimal point would have index 1.",
				"By inspection, a(12) \u003e 1000. - _Alvin Hoover Belt_, Mar 17 2021"
			],
			"formula": [
				"a(n) \u003c= A307582(n) \u003c= A307583(n)."
			],
			"example": [
				"Pi written in base 2 is 11.0...[2] so \"10\" occurring at position a(2) = 0 (digits corresponding to 2^0 and 2^-1) is the first permutation of the digits 01 to occur in the digits of Pi written in base 2",
				"3: [2,",
				"Pi written in base 3 = 1.00102...[3], so \"102\" occurring at position a(3) = 3 (the string starts at the digit corresponding to 3^-3) is the first permutation of digits 012 to occur in the digits of Pi written in base 3.",
				"Pi written in base 4 is 3.021...[4], so \"3021\" occurring at position a(4) = 0 (the string starts at the digit corresponding to 4^0) is the first permutation of digits 0123 to occur in the digits of Pi written in base 4.",
				"Pi written in base 5 is 3.0323221430...[5], so \"21430\" occurring at position a(5) = 6 (the string starts at the digit corresponding to 5^-6) is the first permutation of digits 01234 to occur in the digits of Pi written in base 5.",
				"Pi written in base 6 is 3.0, 5, 0, 3, 3, 0, 0, 5, 1, 4, 1, 5, 1, 2, 4, 1, 0, 5, 2, 3...[6], so \"102\" occurring at position a(6) = 15 (the string starts at the digit corresponding to 3^-3) is the first permutation of digits 012 to occur in the digits of Pi written in base 3.",
				"Pi written in base 7 is 3.06636514320...[7], so \"6514320\" occurring at position a(7) = 5 (the string starts at the digit corresponding to 7^-5) is the first permutation of digits 0123456 to occur in the digits of Pi written in base 3.",
				"Pi written in base 8 is 3.11037...(360 digits omitted)...6253510756243...[8], so \"10756243\" occurring at position a(8) = 371 (the string starts at the digit corresponding to 8^-371) is the first permutation of digits 01234567 to occur in the digits of Pi written in base 3.",
				"Pi written in base 9 has the first string of 9 distinct digits, \"352710468\", starting at position a(9) = 742.",
				"Pi = 3.141592653589793238462643383279502884197169399375105820974944592307816... in base 10) has the first string of 10 distinct digits, \"4592307816\", starting at position a(10) = 60."
			],
			"program": [
				"(PARI) A307581(n,x=Pi,m=n^n)=for(k=0,oo,#Set(d=digits(x\\n^-k%m,n))\u003e=n \u0026\u0026 (#Set(d)==n||vecsort(d)==[1..n-1]) \u0026\u0026 return([k-n+1,digits(x\\n^-k,n)])) \\\\ Returns position and the digits up to there. Ensure sufficient realprecision (\\p): an error should occur if a suitable permutation of digits is not found early enough, but in case of results near the limit of precision, it is suggested to double check (by increasing the precision further) that the relevant digits are all correct."
			],
			"xref": [
				"Cf. A307582 (start of first occurrence of (0, ..., n-1) in digits of Pi in base n).",
				"Cf. A307583 (start of last permutation of {0 .. n-1} not to occur earlier, in base-n digits of Pi).",
				"Cf. A068987, A121280."
			],
			"keyword": "nonn,more,base",
			"offset": "2,2",
			"author": "_M. F. Hasler_, Apr 15 2019",
			"references": 3,
			"revision": 18,
			"time": "2021-04-08T07:34:44-04:00",
			"created": "2019-04-19T10:38:24-04:00"
		}
	]
}