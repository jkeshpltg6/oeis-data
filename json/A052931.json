{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A052931",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 52931,
			"data": "1,0,3,1,9,6,28,27,90,109,297,417,1000,1548,3417,5644,11799,20349,41041,72846,143472,259579,503262,922209,1769365,3269889,6230304,11579032,21960801,40967400,77461435,144863001,273351705,512050438,964918116,1809503019",
			"name": "Expansion of 1/(1 - 3*x^2 - x^3).",
			"comment": [
				"Let A be the tridiagonal unit-primitive matrix (see [Jeffery]) A = A_{9,1} = [0,1,0,0; 1,0,1,0; 0,1,0,1; 0,0,1,1]. Then a(n)=[A^n]_(2,3). - _L. Edson Jeffery_, Mar 19 2011",
				"From _Wolfdieter Lang_, Oct 02 2013: (Start)",
				"This sequence a(n) appears in the formula for the nonnegative powers of the algebraic number rho(9) := 2*cos(Pi/9) of degree 3, the ratio of the smallest diagonal/side in the regular 9-gon, in terms of the power basis of the algebraic number field Q(rho(9)) (see A187360, n=9).",
				"rho(9)^n = A(n)*1 + B(n)*rho(9) + C(n)*rho(9)^2, with A(0) = 1, A(1) = 0, A(n) = B(n-2), n \u003e= 2, B(0) = 0, B(n) = a(n-1), n \u003e= 1, C(0) = 0, C(n) = B(n-1), n \u003e= 1. (End)"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A052931/b052931.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"N. Gogin and A. Mylläri, \u003ca href=\"http://math.unm.edu/~aca/ACA/2013/Nonstandard/Gogin.pdf\"\u003ePadovan-like sequences and Bell polynomials\u003c/a\u003e, Proceedings of Applications of Computer Algebra ACA, 2013.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=917\"\u003eEncyclopedia of Combinatorial Structures 917\u003c/a\u003e",
				"L. E. Jeffery, \u003ca href=\"https://oeis.org/wiki/User:L._Edson_Jeffery/Unit-Primitive_Matrices\"\u003eUnit-primitive matrices\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,3,1)."
			],
			"formula": [
				"G.f.: 1/(1-3*x^2-x^3).",
				"a(n) = 3*a(n-2) + a(n-3), with a(0)=1, a(1)=0, a(2)=3.",
				"a(n) = Sum_{alpha=RootOf(-1+3*z^2+z^3)} (1/9)*(-1 +5*alpha +2*alpha^2) * alpha^(-1-n).",
				"a(n) = Sum_{k=0..floor(n/2)} binomial(k, n-2k)3^(3k-n). - _Paul Barry_, Oct 04 2004",
				"a(n) = A187497(3*(n+1)). - _L. Edson Jeffery_, Mar 19 2011.",
				"3*a(n) = abs(A214699(n+1)). - _Roman Witula_, Oct 06 2012"
			],
			"example": [
				"From _Wolfdieter Lang_, Oct 02 2013: (Start)",
				"In the 9-gon (enneagon), powers of rho(9) = 2*cos(pi/9):",
				"rho(9)^5 = A(5)*1 + B(5)*rho(9) + C(5)*rho(9)^2, with A(5) = B(3) = a(2) = 3, B(5) = a(4) = 9 and C(5) = B(4) = a(3) = 1:",
				"  rho(9)^5 = 3 + 9*rho(9) + rho(9)^2. (End)"
			],
			"maple": [
				"spec := [S,{S=Sequence(Prod(Z,Union(Z,Z,Z,Prod(Z,Z))))},unlabeled]: seq(combstruct[count](spec,size=n), n=0..20);",
				"seq(coeff(series(1/(1-3*x^2-x^3), x, n+1), x, n), n = 0..40); # _G. C. Greubel_, Oct 17 2019"
			],
			"mathematica": [
				"CoefficientList[Series[1/(1-3x^2-x^3),{x,0,40}],x] (* or *) LinearRecurrence[{0,3,1},{1,0,3},40] (* _Vladimir Joseph Stephan Orlovsky_, Jan 28 2012 *)"
			],
			"program": [
				"(PARI) x='x+O('x^40); Vec(1/(1-3*x^2-x^3)) \\\\ _Altug Alkan_, Feb 20 2018",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Integers(), 40); Coefficients(R!( 1/(1-3*x^2-x^3) )); // _G. C. Greubel_, Oct 17 2019",
				"(Sage)",
				"def A052931_list(prec):",
				"    P.\u003cx\u003e = PowerSeriesRing(ZZ, prec)",
				"    return P(1/(1-3*x^2-x^3)).list()",
				"A052931_list(40) # _G. C. Greubel_, Oct 17 2019",
				"(GAP) a:=[1,0,3];; for n in [4..40] do a[n]:=3*a[n-2]+a[n-3]; od; a; # _G. C. Greubel_, Oct 17 2019"
			],
			"xref": [
				"Cf. A214699."
			],
			"keyword": "easy,nonn",
			"offset": "0,3",
			"author": "encyclopedia(AT)pommard.inria.fr, Jan 25 2000",
			"ext": [
				"More terms from _James A. Sellers_, Jun 06 2000"
			],
			"references": 9,
			"revision": 69,
			"time": "2019-10-21T11:34:43-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}