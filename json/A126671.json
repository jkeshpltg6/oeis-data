{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A126671",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 126671,
			"data": "0,0,1,0,1,3,0,2,7,11,0,6,26,46,50,0,24,126,274,326,274,0,120,744,1956,2844,2556,1764,0,720,5160,16008,28092,30708,22212,13068,0,5040,41040,147120,304464,401136,351504,212976,109584,0,40320",
			"name": "Triangle read by rows: row n (n\u003e=0) has g.f. Sum_{i=1..n} n!*x^i*(1+x)^(n-i)/(n+1-i).",
			"comment": [
				"The first nonzero column gives the factorial numbers, which are Stirling_1(*,1), the rightmost diagonal gives Stirling_1(*,2), so this triangle may be regarded as interpolating between the first two columns of the Stirling numbers of the first kind.",
				"This is a slice (the right-hand wall) through the infinite square pyramid described in the link. The other three walls give A007318 and A008276 (twice).",
				"The coefficients of the A165674 triangle are generated by the asymptotic expansion of the higher order exponential integral E(x,m=2,n). The a(n) formulas for the coefficients in the right hand columns of this triangle lead to Wiggen's triangle A028421 and their o.g.f.s. lead to the sequence given above. Some right hand columns of the A165674 triangle are A080663, A165676, A165677, A165678 and A165679. - _Johannes W. Meijer_, Oct 07 2009"
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A126671/a126671.txt\"\u003eNotes on Carlo Wood's Polynomials\u003c/a\u003e"
			],
			"formula": [
				"Recurrence: T(n,0) = 0; for n\u003e=0, i\u003e=1, T(n+1,i) = (n+1)*T(n,i) + n!*binomial(n,i).",
				"E.g.f.: x*log(1-(1+x)*y)/(x*y-1)/(1+x). - _Vladeta Jovovic_, Feb 13 2007"
			],
			"example": [
				"Triangle begins:",
				"0,",
				"0, 1,",
				"0, 1, 3,",
				"0, 2, 7, 11,",
				"0, 6, 26, 46, 50,",
				"0, 24, 126, 274, 326, 274,",
				"0, 120, 744, 1956, 2844, 2556, 1764,",
				"0, 720, 5160, 16008, 28092, 30708, 22212, 13068,",
				"0, 5040, 41040, 147120, 304464, 401136, 351504, 212976, 109584,",
				"0, 40320, 367920, 1498320, 3582000, 5562576, 5868144, 4292496, 2239344, 1026576, ..."
			],
			"maple": [
				"for n from 1 to 15 do t1:=add( n!*x^i*(1+x)^(n-i)/(n+1-i), i=1..n); series(t1,x,100); lprint(seriestolist(%)); od:"
			],
			"mathematica": [
				"Join[{{0}}, Reap[For[n = 1, n \u003c= 15, n++, t1 = Sum[n!*x^i*(1+x)^(n-i)/(n+1-i), {i, 1, n}]; se = Series[t1, {x, 0, 100}]; Sow[CoefficientList[se, x]]]][[2, 1]]] // Flatten (* _Jean-François Alcover_, Jan 07 2014, after Maple *)"
			],
			"xref": [
				"Columns give A000142, A108217, A126672; diagonals give A000254, A067318, A126673. Row sums give A126674. Alternating row sums give A000142.",
				"See A126682 for the full pyramid of coefficients of the underlying polynomials.",
				"Cf. A165674, A028421, A080663, A165676, A165677, A165678 and A165679. - _Johannes W. Meijer_, Oct 07 2009"
			],
			"keyword": "nonn,tabl",
			"offset": "1,6",
			"author": "_N. J. A. Sloane_ and Carlo Wood (carlo(AT)alinoe.com), Feb 13 2007",
			"references": 12,
			"revision": 15,
			"time": "2016-06-16T23:27:31-04:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}