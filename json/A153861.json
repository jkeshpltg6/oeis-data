{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A153861",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 153861,
			"data": "1,1,1,2,3,1,3,6,4,1,4,10,10,5,1,5,15,20,15,6,1,6,21,35,35,21,7,1,7,28,56,70,56,28,8,1,8,36,84,126,126,84,36,9,1,9,45,120,210,252,210,120,45,10,1,10,55,165,330,462,462,330,165,55,11,1",
			"name": "Triangle read by rows, binomial transform of triangle A153860.",
			"comment": [
				"Row sums = A095121: (1, 2, 6, 14, 30, 62, 126,...).",
				"Triangle T(n,k), 0\u003c=k\u003c=n, read by rows, given by [1,1,-1,1,0,0,0,0,0,0,0,...] DELTA [1,0,-1,1,0,0,0,0,0,0,0,...] where DELTA is the operator defined in A084938. - _Philippe Deléham_, Jan 03 2009",
				"A123110*A007318 as infinite lower triangular matrices. - _Philippe Deléham_, Jan 06 2009",
				"A153861 is the fusion of polynomial sequences p(n,x)=x^n+x^(n-1)+...+x+1 and q(n,x)=(x+1)^n; see A193722 for the definition of fusion. - _Clark Kimberling_, Aug 06 2011"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A153861/b153861.txt\"\u003eTable of n, a(n) for the first 46 rows\u003c/a\u003e"
			],
			"formula": [
				"Triangle read by rows, A007318 * A153860. Remove left two columns of Pascal's triangle and append (1, 1, 2, 3, 4, 5,...).",
				"As a recursive operation by way of example, row 3 = (3, 6, 4, 1) =",
				"[1, 1, 1, 0] * (flipped Pascal's triangle matrix) = [1, 3, 3, 1]",
				"[1, 2, 1, 0]",
				"[1, 1, 0, 0]",
				"[1, 0, 0, 0].",
				"(Cf. analogous operation in A130405, but in A153861 the linear multiplier = [1,1,1,...,0].)",
				"T(n,k) = 2*T(n-1,k)+T(n-1,k-1)-T(n-2,k)-T(n-2,k-1), T(0,0) = T(1,0) = T(1,1) = T(2,2) = 1, T(2,0)=2, T(2,1)=3, T(n,k)=0 if k\u003c0 or if k\u003en. - _Philippe Deléham_, Dec 15 2013",
				"G.f.: (1-x+x^2+x^2*y)/((x-1)*(-1+x+x*y)). - _R. J. Mathar_, Aug 11 2015"
			],
			"example": [
				"First few rows of the triangle are:",
				"1;",
				"1, 1;",
				"2, 3, 1;",
				"3, 6, 4, 1;",
				"4, 10, 10, 5, 1;",
				"5, 15, 20, 15, 6, 1;",
				"6, 21, 35, 35, 21, 7, 1;",
				"7, 28, 56, 70, 56, 28, 8, 1;",
				"8, 36, 84, 126, 126, 84, 36, 9, 1;",
				"9, 45, 120, 210, 252, 210, 120, 45, 10, 1;",
				"..."
			],
			"mathematica": [
				"z = 10; c = 1; d = 1;",
				"p[0, x_] := 1",
				"p[n_, x_] := x*p[n - 1, x] + 1; p[n_, 0] := p[n, x] /. x -\u003e 0;",
				"q[n_, x_] := (c*x + d)^n",
				"t[n_, k_] := Coefficient[p[n, x], x^k]; t[n_, 0] := p[n, x] /. x -\u003e 0;",
				"w[n_, x_] := Sum[t[n, k]*q[n + 1 - k, x], {k, 0, n}]; w[-1, x_] := 1",
				"g[n_] := CoefficientList[w[n, x], {x}]",
				"TableForm[Table[Reverse[g[n]], {n, -1, z}]]",
				"Flatten[Table[Reverse[g[n]], {n, -1, z}]]  (* A193815 *)",
				"TableForm[Table[g[n], {n, -1, z}]]",
				"Flatten[Table[g[n], {n, -1, z}]]   (* A153861 *)",
				"(* _Clark Kimberling_, Aug 06 2011 *)"
			],
			"xref": [
				"Cf. A153860, A095121, A130405."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Gary W. Adamson_, Jan 03 2009",
			"references": 5,
			"revision": 26,
			"time": "2016-09-01T03:34:38-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}