{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A337608",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 337608,
			"data": "7,9,2,2,0,8,2,3,8,1,6,7,5,4,1,6,6,8,7,7,5,4,5,5,5,6,6,5,7,9,0,2,4,1,0,1,1,2,8,9,3,2,2,5,0,9,8,6,2,2,1,1,1,7,2,2,7,9,7,3,4,5,2,5,6,9,5,1,4,1,5,4,9,4,4,1,2,4,9,0,6,6,0,2,9,5,3,8,8,3,9,8,0,2,7,5,2,9,2,7,8,7,3,9,7,3",
			"name": "Decimal expansion of Lal's constant: the Hardy-Littlewood constant for A217795.",
			"comment": [
				"Shanks (1967) conjectured that the number of primes of the form (m + 1)^4 + 1 such that (m - 1)^4 + 1 is also a prime (A217795 plus 1), with m \u003c= x, is asymptotic to c * li_2(x), where li_2(x) = Integral_{t=2..n} (1/log(t)^2) dt, and c is this constant. He defined c as in the formula section, evaluated it by 0.79220 and named it after the mathematician Mohan Lal who conjectured the asymptotic formula without evaluating this constant.",
				"The first 100 digits of this constant were calculated by Ettahri et al. (2019)."
			],
			"reference": [
				"Steven R. Finch, Mathematical Constants, Cambridge University Press, 2003, pp. 90-91."
			],
			"link": [
				"Keith Conrad, \u003ca href=\"https://doi.org/10.1007/978-1-4615-0304-0_15\"\u003eHardy-Littlewood constants\u003c/a\u003e in: Mathematical properties of sequences and other combinatorial structures, Jong-Seon No et al. (eds.), Kluwer, Boston/Dordrecht/London, 2003, pp. 133-154, \u003ca href=\"https://kconrad.math.uconn.edu/articles/hlconst.pdf\"\u003ealternative link\u003c/a\u003e.",
				"Salma Ettahri, Olivier Ramaré, Léon Surel, \u003ca href=\"https://arxiv.org/abs/1908.06808\"\u003eFast multi-precision computation of some Euler products\u003c/a\u003e, arXiv:1908.06808 [math.NT], 2019 (Corollary 1.9).",
				"Mohan Lal, \u003ca href=\"https://doi.org/10.1090/S0025-5718-1967-0222007-9\"\u003ePrimes of the form n^4 + 1\u003c/a\u003e, Mathematics of Computation, Vol. 21, No. 98 (1967), pp. 245-247.",
				"Daniel Shanks, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1967-0223315-8\"\u003eLal's constant and generalizations\u003c/a\u003e, Mathematics of Computation, Vol. 21, No. 100 (1967), pp. 705-707.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"https://mathworld.wolfram.com/LalsConstant.html\"\u003eLal's Constant\u003c/a\u003e."
			],
			"formula": [
				"Equals (Pi^4/(2^7 * log(1+sqrt(2))^2)) * Product_{primes p == 1 (mod 8)} (1 - 4/p)^2 * ((p + 1)/(p - 1))^4 * p*(p-8)/(p-4)^2 = (Pi^2/32) * A088367^2 * A334826^2 * A210630 = 2 * A337607^2 * A210630."
			],
			"example": [
				"0.792208238167541668775455566579024101128932250986221..."
			],
			"mathematica": [
				"$MaxExtraPrecision = 1000; digits = 121;",
				"f[p_] := (p-8)*(p+1)^4/((p-1)^4*p);",
				"coefs = Rest[CoefficientList[Series[Log[f[1/x]], {x, 0, 1000}], x]];",
				"S[m_, n_, s_] := (t = 1; sums = 0; difs = 1; While[Abs[difs] \u003e 10^(-digits - 5) || difs == 0, difs = (MoebiusMu[t]/t) * Log[If[s*t == 1, DirichletL[m, n, s*t], Sum[Zeta[s*t, j/m]*DirichletCharacter[m, n, j]^t, {j, 1, m}]/m^(s*t)]]; sums = sums + difs; t++]; sums);",
				"P[m_, n_, s_] := 1/EulerPhi[m] * Sum[Conjugate[DirichletCharacter[m, r, n]] * S[m, r, s], {r, 1, EulerPhi[m]}] + Sum[If[GCD[p, m] \u003e 1 \u0026\u0026 Mod[p, m] == n, 1/p^s, 0], {p, 1, m}];",
				"m = 2; sump = 0; difp = 1; While[Abs[difp] \u003e 10^(-digits - 5) || difp == 0, difp = coefs[[m]]*(P[8, 1, m] - 1/17^m); sump = sump + difp; m++];",
				"RealDigits[Chop[N[f[17] * Pi^4/(2^7 * Log[1+Sqrt[2]]^2) * Exp[sump], digits]], 10, digits - 1][[1]] (* _Vaclav Kotesovec_, Jan 16 2021 *)"
			],
			"xref": [
				"Cf. A000068, A037896, A088367, A210630, A217795, A217796.",
				"Similar constants: A005597, A331941, A337606, A337607."
			],
			"keyword": "nonn,cons",
			"offset": "0,1",
			"author": "_Amiram Eldar_, Sep 04 2020",
			"ext": [
				"More terms from _Vaclav Kotesovec_, Jan 16 2021"
			],
			"references": 2,
			"revision": 9,
			"time": "2021-01-16T05:22:37-05:00",
			"created": "2020-09-04T21:24:13-04:00"
		}
	]
}