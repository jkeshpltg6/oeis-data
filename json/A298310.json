{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A298310",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 298310,
			"data": "2,3,2,2,9,2,2,15,2,2,15,2,32,81,2,2,55,21,2,39,2,2,4141,2,18,51,2,551,39,2,2,21267,21,2,1012,2,2,826,330,2,729,2",
			"name": "Least k \u003e 1 such that all divisors of (k^(2n+1)+1)/(k+1) are == 1 (mod 2n+1).",
			"comment": [
				"a(n) is the smallest k \u003e 1 such that Phi_m(-k) has all its divisors == 1 (mod n) for all m \u003e 1 dividing 2n+1, where Phi_m(x) are the cyclotomic polynomials.",
				"By Schinzel's hypothesis H, a(n) exists for every n (see A298076).",
				"If 2n+1 is a prime \u003e 3, then a(n) = 2.",
				"We have a(n)^(2n+1) == a(n) (mod 2n+1), so every composite number 2n+1 is a weak Fermat pseudoprime to base a(n).",
				"a(n) \u003e= A239452(2n+1).",
				"a(42) requires factorization of a 132 digit composite. - _M. F. Hasler_, Oct 16 2018"
			],
			"example": [
				"a(170) = 2 wherein 2*170 + 1 = 341 = 11*31 is the smallest psp(2).",
				"From _M. F. Hasler_, Oct 15 2018: (Start)",
				"a(0) = 2 is the least integer k \u003e 1 for which (k+1)/(k+1) == 1 (mod 1). (Here we even have equality, but any integer is congruent to any other integer, modulo 1.)",
				"a(1) = 3 is the least k \u003e 1 for which (k^3+1)/(k+1) = k^2 - k + 1 = P3(-k) == 1 (mod 3). Indeed, P3(-3) = 7 == 1 (mod 3), while P3(-2) = 3 == 0 (mod 3). (End)"
			],
			"mathematica": [
				"Table[SelectFirst[Range[2, 100], AllTrue[Divisors[(#^(2 n + 1) + 1)/(# + 1)], Mod[#, 2 n + 1] == 1 \u0026] \u0026], {n, 21}] (* _Michael De Vlieger_, Feb 01 2018 *)"
			],
			"program": [
				"(PARI) isok(k, n) = {fordiv((k^(2*n+1)+1)/(k+1), d, if (Mod(d, (2*n+1)) != 1, return (0));); return(1);}",
				"a(n) = {my(k = 2); while (!isok(k, n), k++); k;} \\\\ _Michel Marcus_, Jan 19 2018",
				"(PARI) A298310(n)={n=n*2+1;for(k=2,oo,fordiv(n,m,m\u003e1\u0026\u0026vecmax(factor(polcyclo(m,-k))[,1]%n)!=1\u0026\u0026 next(2));return(k))} \\\\ _M. F. Hasler_, Oct 14 2018"
			],
			"xref": [
				"Cf. A239452, A298076 (see Ordowski's conjecture for b \u003c -1 and odd n)."
			],
			"keyword": "nonn,hard,more",
			"offset": "0,1",
			"author": "_Thomas Ordowski_ and _Krzysztof Ziemak_, Jan 17 2018",
			"ext": [
				"a(22) corrected by _Robert Israel_, Jan 18 2018",
				"a(1) corrected by _Michel Marcus_, Jan 19 2018",
				"a(27)-a(30) from _Robert Price_, Feb 17 2018",
				"a(31)-a(41) from _M. F. Hasler_, Oct 15 2018"
			],
			"references": 2,
			"revision": 57,
			"time": "2018-10-18T03:21:55-04:00",
			"created": "2018-01-18T09:51:54-05:00"
		}
	]
}