{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A051924",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 51924,
			"data": "1,4,14,50,182,672,2508,9438,35750,136136,520676,1998724,7696444,29716000,115000920,445962870,1732525830,6741529080,26270128500,102501265020,400411345620,1565841089280,6129331763880,24014172955500",
			"name": "a(n) = binomial(2*n,n) - binomial(2*n-2,n-1); or (3n-2)*C(n-1), where C = Catalan numbers (A000108).",
			"comment": [
				"Number of partitions with Ferrers plots that fit inside an n X n box, but not in an n-1 X n-1 box. - _Wouter Meeussen_, Dec 10 2001",
				"From _Benoit Cloitre_, Jan 29 2002: (Start)",
				"Let m(1,j)=j, m(i,1)=i and m(i,j) = m(i-1,j) + m(i,j-1); then a(n) = m(n,n):",
				"  1  2  3  4 ...",
				"  2  4  7 11 ...",
				"  3  7 14 25 ...",
				"  4 11 25 50 ... (End)",
				"This sequence also gives the number of clusters and non-crossing partitions of type D_n. - _F. Chapoton_, Jan 31 2005",
				"If Y is a 2-subset of a 2n-set X then a(n) is the number of (n+1)-subsets of X intersecting Y. - _Milan Janjic_, Nov 18 2007",
				"Prefaced with a 1: (1, 1, 4, 14, 50, ...) and convolved with the Catalan sequence = A097613: (1, 2, 7, 25, 91, ...). - _Gary W. Adamson_, May 15 2009",
				"Total number of up steps before the second return in all Dyck n-paths. - _David Scambler_, Aug 21 2012",
				"Conjecture: a(n) mod n^2 = n+2 iff n is an odd prime. - _Gary Detlefs_, Feb 19 2013",
				"First differences of A000984 and A030662. - _J. M. Bergot_, Jun 22 2013",
				"From _R. J. Mathar_, Jun 30 2013: (Start)",
				"Equivalent to the Meeussen comment and the Bergot comment: The array view of A007318 is",
				"  1,   1,   1,   1,   1,   1,",
				"  1,   2,   3,   4,   5,   6,",
				"  1,   3,   6,  10,  15,  21,",
				"  1,   4,  10,  20,  35,  56,",
				"  1,   5,  15,  35,  70, 126,",
				"  1,   6,  21,  56, 126, 252,",
				"and a(n) are the hook sums Sum_{k=0..n} A(n,k) + Sum_{r=0..n-1} A(r,n). (End)",
				"From _Gus Wiseman_, Apr 12 2019: (Start)",
				"Equivalent to Wouter Meeussen's comment, a(n) is the number of integer partitions (of any positive integer) such that the maximum of the length and the largest part is k. For example, the a(1) = 1 through a(3) = 14 partitions are:",
				"  (1)  (2)   (3)",
				"       (11)  (31)",
				"       (21)  (32)",
				"       (22)  (33)",
				"             (111)",
				"             (211)",
				"             (221)",
				"             (222)",
				"             (311)",
				"             (321)",
				"             (322)",
				"             (331)",
				"             (332)",
				"             (333)",
				"(End)"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A051924/b051924.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"F. Chapoton, \u003ca href=\"http://irma.math.unistra.fr/~chapoton/clusters.html\"\u003eClusters\u003c/a\u003e.",
				"FindStat, \u003ca href=\"http://www.findstat.org/StatisticsDatabase/St000784\"\u003eSt000784: The maximum of the length and the largest part of the integer partition\u003c/a\u003e",
				"Sergey Fomin and Andrei Zelevinsky, \u003ca href=\"http://www.jstor.org/stable/3597238\"\u003eY-systems and generalized associahedra\u003c/a\u003e, Ann. of Math. (2) 158 (2003), no. 3, 977-1018.",
				"Joël Gay, Vincent Pilaud, \u003ca href=\"https://arxiv.org/abs/1804.06572\"\u003eThe weak order on Weyl posets\u003c/a\u003e, arXiv:1804.06572 [math.CO], 2018.",
				"Milan Janjic, \u003ca href=\"http://www.pmfbl.org/janjic/\"\u003eTwo Enumerative Functions\u003c/a\u003e",
				"Sanjay Moudgalya, Abhinav Prem, Rahul Nandkishore, Nicolas Regnault, B. Andrei Bernevig, \u003ca href=\"https://arxiv.org/abs/1910.14048\"\u003eThermalization and its absence within Krylov subspaces of a constrained Hamiltonian\u003c/a\u003e, arXiv:1910.14048 [cond-mat.str-el], 2019.",
				"Hugh Thomas, \u003ca href=\"http://arxiv.org/abs/math/0311334\"\u003eTamari Lattices and Non-Crossing Partitions in Types B and D\u003c/a\u003e, arXiv:math/0311334 [math.CO], 2003-2005."
			],
			"formula": [
				"G.f.: (1-x) / sqrt(1-4*x) - 1. - _Paul D. Hanna_, Nov 08 2014",
				"G.f.: Sum_{n\u003e=1} x^n/(1-x)^(2*n) * Sum_{k=0..n} C(n,k)^2 * x^k. - _Paul D. Hanna_, Nov 08 2014",
				"a(n+1) = binomial(2*n, n)+2*sum(i=0, n-1, binomial(n+i, i) (V's in Pascal's Triangle). - _Jon Perry_ Apr 13 2004",
				"a(n) = n*C(n-1) - (n-1)*C(n-2), where C(n) = A000108(n) = Catalan(n). For example, a(5) = 50 = 5*C(4) - 4*C(3) - 5*14 - 3*5 = 70 - 20. Triangle A128064 as an infinite lower triangular matrix * A000108 = A051924 prefaced with a 1: (1, 1, 4, 14, 50, 182, ...). - _Gary W. Adamson_, May 15 2009",
				"Sum of 3 central terms of Pascal's triangle: 2*C(2+2*n, n)+C(2+2*n, 1+n). - _Zerinvary Lajos_, Dec 20 2005",
				"a(n+1) = A051597(2n,n). - _Philippe Deléham_, Nov 26 2006",
				"The sequence 1,1,4,... has a(n)=C(2n,n)-C(2(n-1),n-1)=0^n+sum{k=0..n, C(n-1,k-1)*A002426(k)}, and g.f. given by (1-x)/(1-2x-2x^2/(1-2x-x^2/(1-2x-x^2/(1-2x-x^2/(1-.... (continued fraction). - _Paul Barry_, Oct 17 2009",
				"a(n) = (3*n-2)*(2*n-2)!/(n*(n-1)!^2) = A001700(n) + A001791(n-1). - _David Scambler_, Aug 21 2012",
				"D-finite with recurrence: a(n) = 2*(3*n-2)*(2*n-3)*a(n-1)/(n*(3*n-5)). - _Alois P. Heinz_, Apr 25 2014",
				"a(n) = 2^(-2+2*n)*Gamma(-1/2+n)*(3*n-2)/(sqrt(Pi)*Gamma(1+n)). - _Peter Luschny_, Dec 14 2015",
				"a(n) ~ (3/4)*4^n*(1-(7/24)/n-(7/128)/n^2-(85/3072)/n^3-(581/32768)/n^4-(2611/262144)/n^5)/sqrt(n*Pi). - _Peter Luschny_, Dec 16 2015",
				"E.g.f.: ((1 - x)*BesselI(0,2*x) + x*BesselI(1,2*x))*exp(2*x) - 1. - _Ilya Gutkovskiy_, Dec 20 2016",
				"a(n) = 2 * A097613(n) for n \u003e 1. - _Bruce J. Nicholson_, Jan 06 2019"
			],
			"example": [
				"Sums of {1}, {2, 1, 1}, {2, 2, 3, 3, 2, 1, 1}, {2, 2, 4, 5, 7, 6, 7, 5, 5, 3, 2, 1, 1}, ..."
			],
			"maple": [
				"C:=proc(n) options operator, arrow: binomial(2*n, n)/(n+1) end proc: seq(n*C(n-1)-(n-1)*C(n-2),n=2..25); # _Emeric Deutsch_, Jan 08 2008",
				"Z:=(1-z-sqrt(1-4*z))/sqrt(1-4*z): Zser:=series(Z, z=0, 32): seq(coeff(Zser, z, n), n=1..24); # _Zerinvary Lajos_, Jan 01 2007",
				"a := n -\u003e 2^(-2+2*n)*GAMMA(-1/2+n)*(3*n-2)/(sqrt(Pi)*GAMMA(1+n)):",
				"seq(simplify(a(n)), n=1..24); # _Peter Luschny_, Dec 14 2015"
			],
			"mathematica": [
				"Table[Binomial[2n,n]-Binomial[2n-2,n-1],{n,30}] (* _Harvey P. Dale_, Jan 15 2012 *)"
			],
			"program": [
				"(Haskell)",
				"a051924 n = a051924_list !! (n-1)",
				"a051924_list = zipWith (-) (tail a000984_list) a000984_list",
				"-- _Reinhard Zumkeller_, May 25 2013",
				"(PARI) a(n)=binomial(2*n,n)-binomial(2*n-2,n-1) \\\\ _Charles R Greathouse IV_, Jun 25 2013",
				"(PARI) {a(n)=polcoeff((1-x) / sqrt(1-4*x +x*O(x^n)) - 1,n)}",
				"for(n=1,30,print1(a(n),\", \")) \\\\ _Paul D. Hanna_, Nov 08 2014",
				"(PARI) {a(n)=polcoeff( sum(m=1, n, x^m * sum(k=0, m, binomial(m, k)^2 * x^k) / (1-x +x*O(x^n))^(2*m)), n)}",
				"for(n=1, 30, print1(a(n), \", \")) \\\\ _Paul D. Hanna_, Nov 08 2014",
				"(Sage)",
				"a = lambda n: 2^(-2+2*n)*gamma(n-1/2)*(3*n-2)/(sqrt(pi)*gamma(1+n))",
				"[a(n) for n in (1..120)] # _Peter Luschny_, Dec 14 2015",
				"(MAGMA) [Binomial(2*n, n)-Binomial(2*n-2, n-1): n in [1..28]]; // _Vincenzo Librandi_, Dec 21 2016"
			],
			"xref": [
				"Left-central elements of the (1, 2)-Pascal triangle A029635.",
				"Column sums of A096771.",
				"Cf. A000108, A024482 (diagonal from 2), A076540 (diagonal from 3), A000124 (row from 2), A004006 (row from 3), A006522 (row from 4).",
				"Cf. A128064; first differences of A000984.",
				"Cf. A097613.",
				"Cf. A115720, A252464, A257990, A263297, A325189, A325192, A325193."
			],
			"keyword": "easy,nice,nonn",
			"offset": "1,2",
			"author": "_Barry E. Williams_, Dec 19 1999",
			"ext": [
				"Edited by _N. J. A. Sloane_, May 03 2008, at the suggestion of _R. J. Mathar_"
			],
			"references": 37,
			"revision": 125,
			"time": "2020-01-30T21:29:14-05:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}