{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A268112",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 268112,
			"data": "848,9338,10583,3546471722268916272",
			"name": "Numbers k for which the numerator of the k-th harmonic number H_k is divisible by the third power of a prime less than k.",
			"comment": [
				"The sequence contains numbers k for which there is a prime p \u003c k with v_p(H_k) \u003e= 3, where v_p(x) is the p-adic valuation of x and H_k is the k-th Harmonic number. All numbers were found by D. W. Boyd. The corresponding p for a(1) through a(4) is 11 while for a(5) (in the b-file) is 83. [Edited by _Petros Hadjicostas_, May 25 2020]",
				"It is a widely believed conjecture that there is no pair of an integer k and a prime p for which v_p(H_k) \u003e= 4. If variations of this conjecture hold, then Krattenhaler and Rivoal (2007-2009) would be able to establish some formulas for their theory. See also A007757, A131657, and A131658. - _Petros Hadjicostas_, May 25 2020"
			],
			"link": [
				"Petros Hadjicostas, \u003ca href=\"/A268112/b268112.txt\"\u003eTable of n, a(n) for n = 1..5\u003c/a\u003e",
				"David W. Boyd, \u003ca href=\"https://doi.org/10.1080/10586458.1994.10504298\"\u003eA p-adic study of the partial sum of the harmonic series\u003c/a\u003e, Experimental Mathematics, 3(4) (1994), 287-302.",
				"Christian Krattenthaler and Tanguy Rivoal, \u003ca href=\"http://arxiv.org/abs/0709.1432\"\u003eOn the integrality of the Taylor coefficients of mirror maps\u003c/a\u003e, arXiv:0709.1432 [math.NT], 2007-2009.",
				"Christian Krattenthaler and Tanguy Rivoal, \u003ca href=\"http://dx.doi.org/10.4310/CNTP.2009.v3.n3.a5\"\u003eOn the integrality of the Taylor coefficients of mirror maps, II\u003c/a\u003e, Communications in Number Theory and Physics, Volume 3, Number 3 (2009), 555-591.",
				"Tamás Lengyel, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2014.09.015\"\u003eOn p-adic properties of the Stirling numbers of the first kind\u003c/a\u003e, Journal of Number Theory, 148 (2015), 73-94."
			],
			"program": [
				"(PARI) h(n) = sum(i=1, n, 1/i);",
				"is(n) = {forprime(p=1, n-1, if(valuation((numerator(h(n))), p) \u003e 2, return(1))); return(0)} \\\\ Edited by _Petros Hadjicostas_, May 25 2020"
			],
			"xref": [
				"Cf. A001008, A007757, A131657, A131658."
			],
			"keyword": "nonn,hard,more",
			"offset": "1,1",
			"author": "_Felix Fröhlich_, Jan 26 2016",
			"ext": [
				"Name edited by and a(5) copied from the references by _Petros Hadjicostas_, May 25 2020"
			],
			"references": 5,
			"revision": 48,
			"time": "2020-05-29T04:27:43-04:00",
			"created": "2016-03-01T18:55:35-05:00"
		}
	]
}