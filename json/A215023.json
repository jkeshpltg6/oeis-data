{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A215023",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 215023,
			"data": "0,10,1001,1000,1010,100101,100100,100001,100000,100010,101001,101000,101010,10010101,10010100,10010001,10010000,10010010,10000101,10000100,10000001,10000000,10000010,10001001,10001000,10001010,10100101,10100100,10100001,10100000",
			"name": "NegaFibonacci representation for -n.",
			"comment": [
				"Let F_{-n} be the negative Fibonacci numbers (as defined in the first comment in A039834): F_{-1}=1, F_{-2}=-1, F_{-3}=2, F_{-4}=-3, F_{-5}=5, ..., F_{-n}=(-1)^(n-1)F_n.",
				"Every integer has a unique representation as n = Sum_{k=1..r} c_k F_{-k} for some r, where the c_k are 0 or 1 and no two adjacent c's are 1.",
				"Then a(n) = c_r ... c_3 c_2 c_1."
			],
			"reference": [
				"Donald E. Knuth, The Art of Computer Programming, Volume 4A, Combinatorial algorithms, Part 1, Addison-Wesley, 2011, pp. 168-171."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A215023/b215023.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"M. W. Bunder, \u003ca href=\"https://www.fq.math.ca/Scanned/30-2/bunder.pdf\"\u003eZeckendorf representations using negative Fibonacci numbers\u003c/a\u003e, The Fibonacci Quarterly, Vol. 30, No. 2 (1992), pp. 111-115.",
				"Donald E. Knuth, \u003ca href=\"http://www.cs.utsa.edu/~wagner/knuth/fasc1a.pdf\"\u003eThe Art of Computer Programming, Volume 4, Fascicle 1: Bitwise Tricks \u0026 Techniques; Binary Decision Diagrams\u003c/a\u003e, a pre-publication draft of section 7.1.3, 2009, pp. 36-39.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/NegaFibonacci_coding\"\u003eNegaFibonacci coding\u003c/a\u003e."
			],
			"example": [
				"-4 = -3 - 1 = F_{-4} + F_{-2}, so a(4) = 1010."
			],
			"mathematica": [
				"ind[n_] := Floor[Log[Abs[n]*Sqrt[5] + 1/2]/Log[GoldenRatio]]; f[1] = 1; f[n_] := If[n \u003e 0, i = ind[n - 1]; If[EvenQ[i], i++]; i, i = ind[-n]; If[OddQ[i], i++]; i]; a[n_] := Module[{k = n, s = 0}, While[k != 0, i = f[k]; s += 10^(i - 1); k -= Fibonacci[-i]]; s]; Table[a[n], {n, 0, -100, -1}] (* _Amiram Eldar_, Oct 15 2019 *)"
			],
			"program": [
				"(PARI) a(n)=my(s=0,k=0,v);while(s\u003cn,s+=fibonacci(k+=2));v=binary(2^k\\2);n-=fibonacci(k);forstep(i=k-2,1,-1,if(abs(n+fibonacci(-i))\u003cabs(n), n+=fibonacci(-i);v[#v+1-i]=1;i--));subst(Pol(v),x,10) \\\\ _Charles R Greathouse IV_, Aug 03 2012 [Caution: returns wrong values for some values of n \u003e 17. _Amiram Eldar_, Oct 15 2019]"
			],
			"xref": [
				"Cf. A039834, A215022, A215025, A000045, A014417, A003714."
			],
			"keyword": "nonn,base",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Aug 03 2012",
			"ext": [
				"a(18) inserted by _Amiram Eldar_, Oct 11 2019"
			],
			"references": 9,
			"revision": 36,
			"time": "2020-01-22T23:30:58-05:00",
			"created": "2012-08-03T00:46:02-04:00"
		}
	]
}