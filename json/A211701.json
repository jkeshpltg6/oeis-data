{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A211701",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 211701,
			"data": "1,2,1,3,3,1,4,4,3,1,5,6,5,3,1,6,7,7,5,3,1,7,9,8,8,5,3,1,8,10,11,9,8,5,3,1,9,12,12,12,10,8,5,3,1,10,13,14,13,13,10,8,5,3,1,11,15,16,16,14,14,10,8,5,3,1,12,16,18,18,17,15,14,10,8,5,3,1,13,18,19,20,19",
			"name": "Rectangular array by antidiagonals, n \u003e= 1, k \u003e= 1: R(n,k) = n + [n/2] + ... + [n/k], where [ ]=floor.",
			"comment": [
				"R(n,k) is the number of ordered pairs (x,y) of integers x,y satisfying 1 \u003c= x \u003c= k, 1 \u003c= y \u003c= k, and x*y \u003c= n.",
				"Limiting row: A000618=(1,3,5,8,10,14,16,20,...).",
				"Row 1: A000027",
				"Row 2: A032766",
				"Row 3: A106252",
				"Row 4: A211703",
				"Row 5: A211704",
				"R(n,n) = A000618(n)",
				"...",
				"For n \u003e =1, row n is a homogeneous linear recurrence sequence of order A005728(n), and it exemplifies a certain class, C, of recurrences which are palindromic (in the sense given below). The class depends on sequences s having n-th term [(n^k)/j], where k and j are arbitrary fixed positive integers and [ ] = floor. The characteristic polynomial of s is (x^j-1)(x-1)^k, which is a palindromic polynomial (sometimes called a reciprocal polynomial). The class C consists of sequences u given by the form",
				"   ...",
				"   u(n) = c(1)*[r(1)*n^k(1)] + ... + c(m)*[r(m)*n^k(m)],",
				"   ...",
				"where c(i) are integers and r(i) are rational numbers. Assume that r(i) is in lowest terms, and let j(i) be its denominator. Then the characteristic polynomial of u is the least common multiple of all the irreducible (over the integers) factors of all the polynomials (x^j(i)-1)(x-1)^k(i). As all such factors are palindromic (indeed, they are all cyclotomic polynomials), the characteristic polynomial of u is also palindromic. In other words, if the generating function of u is written as p(x)/q(x), then q(x) is a palindromic polynomial.",
				"Thus, if q(x) = q(h)x^h + ... + q(1)x + q(0),",
				"then (q(h), q(h-1), ..., q(1), q(0)) is palindromic, and consequently, the recurrence coefficients for u, after excluding q(0); i.e., (- q(h-1), ... - q(1)), are palindromic. For example, row 3 of A211701 has the following recurrence: u(n) = u(n-2) + u(n-3) - u(n-5), for which q(x) = x^5 - x^3 - x^2 + 1, with recurrence coefficients (0,1,1,0,-1).",
				"Recurrence coefficients (palindromic after excluding the last term) are shown here:",
				"for row 1:  (2, -1)",
				"for row 2:  (1 ,1, -1)",
				"for row 3:  (0, 1, 1, 0, -1)",
				"for row 4:  (0, 0, 1, 1, 0, 0, -1)",
				"for row 5:  (-1, -1, 0, 1, 2, 2, 1, 0, -1, -1, -1)",
				"for row 6:  (0, -1, 0, 0, 1, 1, 1, 1, 0, 0, -1, 0, -1)",
				"for row 7:  (-1, -2, -2, -2, -1, 0, 2, 3, 4, 4, 3, 2,",
				"              0, -1, -2, -2, -2, -1, -1)",
				"for row 13:  (-2,-4,-7,-12,-18,-27,-37,-50,-64,-80,-95,",
				"              -111,-123,-133,-137,-136,-126,-110,-84,-52,",
				"              -12,32,80,127,173,213,246,269,281,281,269,",
				"               246,213,173,127,80,32,-12,-52,-84,-110,",
				"              -126,-136,-137,-133,-123,-111,-95,-80,-64,",
				"              -50,-37,-27,-18,-12,-7,-4,-2,-1)"
			],
			"example": [
				"Northwest corner:",
				"  1   2   3   4   5   6   7   8   9  10",
				"  1   3   4   6   7   9  10  12  13  15",
				"  1   3   5   7   8  11  12  14  16  18",
				"  1   3   5   8   9  12  13  16  18  19"
			],
			"mathematica": [
				"f[n_, m_] := Sum[Floor[n/k], {k, 1, m}]",
				"TableForm[Table[f[n, m], {m, 1, 20}, {n, 1, 16}]]",
				"Flatten[Table[f[n + 1 - m, m], {n, 1, 14}, {m, 1, n}]]"
			],
			"xref": [
				"Cf. A211702, A211703, A211704, A211705."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Apr 19 2012",
			"references": 13,
			"revision": 11,
			"time": "2017-11-11T17:27:25-05:00",
			"created": "2012-04-20T15:09:49-04:00"
		}
	]
}