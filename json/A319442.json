{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A319442",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 319442,
			"data": "1,2,3,3,2,6,4,4,5,4,2,9,4,8,6,5,2,10,4,6,12,4,2,12,3,8,7,12,2,12,4,6,6,4,8,15,4,8,12,8,2,24,4,6,10,4,2,15,9,6,6,12,2,14,4,16,12,4,2,18,4,8,20,7,8,12,4,6,6,16,2,20,4,8,9,12,8,24,4,10",
			"name": "Number of divisors of n over the Eisenstein integers.",
			"comment": [
				"Equivalent of d (A000005) in the ring of Eisenstein integers.",
				"Divisors which are associates are identified (two Eisenstein integers z1, z2 are associates if z1 = u * z2 where u is an Eisenstein unit, i.e., one of +-1 or (+-1 +- sqrt(3)*i)/2."
			],
			"link": [
				"Jianing Song, \u003ca href=\"/A319442/b319442.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Eisenstein_integer\"\u003eEisenstein integer\u003c/a\u003e"
			],
			"formula": [
				"Multiplicative with a(3^e) = 2*e + 1, a(p^e) = (e + 1)^2 if p == 1 (mod 3) and e + 1 if p == 2 (mod 3)."
			],
			"example": [
				"Let w = (1 + sqrt(3)*i)/2, w' = (1 - sqrt(3)*i)/2.",
				"Divisors of 7 over the Eisenstein integers are 1, 2 + w, 2 + w', 7 and their association, so a(7) = 4.",
				"Divisors of 9 over the Eisenstein integers are 1, 1 + w, 3, 3 + 3w, 9 and their association, so a(9) = 5."
			],
			"maple": [
				"A319442 := proc(n) local t, f, j, e, m; t := 1: f := ifactors(n)[2];",
				"   for j from 1 to nops(f) do",
				"      e := f[j, 2] + 1; m := f[j, 1] mod 3;",
				"      if   m = 0 then 2*e-1",
				"      elif m = 1 then e^2",
				"      else e fi;",
				"      t := t * % od;",
				"t end: seq(A319442(n), n=1..80); # _Peter Luschny_, Oct 03 2018"
			],
			"mathematica": [
				"f[p_, e_] := Switch[Mod[p, 3], 0, 2*e + 1, 1, (e + 1)^2, 2, e + 1]; eisNumDiv[1] = 1; eisNumDiv[n_] := Times @@ f @@@ FactorInteger[n]; Array[eisNumDiv, 100] (* _Amiram Eldar_, Feb 10 2020 *)"
			],
			"program": [
				"(PARI)",
				"A319442(n)=",
				"{",
				"    my(r=1, f=factor(n));",
				"    for(j=1, #f[, 1], my(p=f[j, 1], e=f[j, 2]);",
				"        if(p==3, r*=(2*e+1));",
				"        if(p%3==1, r*=(e+1)^2);",
				"        if(p%3==2, r*=(e+1));",
				"    );",
				"    return(r);",
				"}"
			],
			"xref": [
				"Equivalent of arithmetic functions in the ring of Eisenstein integers (the corresponding functions in the ring of integers are in the parentheses): this sequence (\"d\", A000005), A319449 (\"sigma\", A000203), A319445 (\"phi\", A000010), A319446 (\"psi\", A002322), A319443 (\"omega\", A001221), A319444 (\"Omega\", A001222), A319448 (\"mu\", A008683).",
				"Equivalent in the ring of Gaussian integers: A062327."
			],
			"keyword": "nonn,mult",
			"offset": "1,2",
			"author": "_Jianing Song_, Sep 19 2018",
			"references": 13,
			"revision": 24,
			"time": "2020-02-10T17:40:08-05:00",
			"created": "2018-09-24T08:50:22-04:00"
		}
	]
}