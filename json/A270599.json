{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A270599",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 270599,
			"data": "1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,4,0,0,0,0,0,0,0,3,0,0,0,0,0,0,0,4,0,0,0,0,0,0,0,10,0,0,0",
			"name": "Number of ways to express 1 as the sum of unit fractions with odd denominators such that the sum of those denominators is n.",
			"comment": [
				"Number of partitions of n into such odd parts that the sum of their reciprocals is one. - _Antti Karttunen_, Jul 23 2018",
				"It would be nice to know whether nonzero values may occur only on n of the form 8k+1."
			],
			"link": [
				"David A. Corneth, \u003ca href=\"/A270599/b270599.txt\"\u003eTable of n, a(n) for n = 1..370\u003c/a\u003e (terms 1..150 from Seiichi Manyama, terms 150..273 from Antti Karttunen)",
				"David A. Corneth, \u003ca href=\"/A270599/a270599_1.gp.txt\"\u003eTuples up to n = 370. \u003c/a\u003e",
				"\u003ca href=\"/index/Ed#Egypt\"\u003eIndex entries for sequences related to Egyptian fractions\u003c/a\u003e"
			],
			"formula": [
				"a(2*k) = 0. - _David A. Corneth_, Jul 24 2018"
			],
			"example": [
				"1 = 1/3 + 1/3 + 1/3, the sum of denominators is 9, this is the only expression of 1 as unit fractions with odd denominators that sum to 9, so a(9)=1.",
				"1 = 1/15 + 1/5 + 1/5 + 1/5 + 1/3 = 1/9 + 1/9 + 1/9 + 1/3 + 1/3 are the only solutions with odd denominators that sum to 33, thus a(33) = 2. - _Antti Karttunen_, Jul 24 2018"
			],
			"mathematica": [
				"Array[Count[IntegerPartitions[#, All, Range[1, #, 2]], _?(Total[1/#] == 1 \u0026)] \u0026, 70] (* _Michael De Vlieger_, Jul 26 2018 *)"
			],
			"program": [
				"(Ruby)",
				"def f(n)",
				"  n - 1 + n % 2",
				"end",
				"def partition(n, min, max)",
				"  return [[]] if n == 0",
				"  [f(max), f(n)].min.step(min, -2).flat_map{|i| partition(n - i, min, i).map{|rest| [i, *rest]}}",
				"end",
				"def A270599(n)",
				"  ary = [1]",
				"  (2..n).each{|m|",
				"    cnt = 0",
				"    partition(m, 2, m).each{|ary|",
				"      cnt += 1 if ary.inject(0){|s, i| s + 1 / i.to_r} == 1",
				"    }",
				"    ary \u003c\u003c cnt",
				"  }",
				"  ary",
				"end",
				"(PARI) A270599(n,maxfrom=n,fracsum=0) = if(!n,(1==fracsum),my(s=0, tfs, k=(maxfrom-!(maxfrom%2))); while(k \u003e= 1, tfs = fracsum + (1/k); if(tfs \u003e 1, return(s), s += A270599(n-k,min(k,n-k),tfs)); k -= 2); (s)); \\\\ _Antti Karttunen_, Jul 23 2018",
				"(PARI)",
				"\\\\ More verbose version for computing values of a(n) for large n:",
				"A270599(n) = if(!(n%2), 0, my(s=0); forstep(k = n, 1, -2, print(\"A270599(\", n, \") at toplevel, k=\", k, \" s=\", s); s += A270599aux(n-k, min(k, n-k), 1/k)); (s));",
				"A270599aux(n,maxfrom,fracsum) = if(!n,(1==fracsum),my(s=0, tfs, k=(maxfrom-!(maxfrom%2))); while(k \u003e= 1, tfs = fracsum + (1/k); if(tfs \u003e 1, return(s), s += A270599aux(n-k,min(k,n-k),tfs)); k -= 2); (s)); \\\\ _Antti Karttunen_, Jul 24 2018"
			],
			"xref": [
				"Cf. A000009, A051908.",
				"Cf. also A201644, A201646, A201647, A201648, A201649."
			],
			"keyword": "nonn",
			"offset": "1,33",
			"author": "_Seiichi Manyama_, Mar 26 2016",
			"ext": [
				"Name corrected by _Antti Karttunen_, Jul 23 2018 at the suggestion of _David A. Corneth_"
			],
			"references": 2,
			"revision": 69,
			"time": "2018-10-24T08:20:36-04:00",
			"created": "2016-03-31T13:04:27-04:00"
		}
	]
}