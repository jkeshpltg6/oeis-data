{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A259936",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 259936,
			"data": "1,1,1,1,1,2,1,1,1,2,1,2,1,2,2,1,1,2,1,2,2,2,1,2,1,2,1,2,1,5,1,1,2,2,2,2,1,2,2,2,1,5,1,2,2,2,1,2,1,2,2,2,1,2,2,2,2,2,1,5,1,2,2,1,2,5,1,2,2,5,1,2,1,2,2,2,2,5,1,2,1,2,1,5,2,2,2,2,1,5,2,2,2,2,2,2,1,2,2,2,1,5,1,2,5",
			"name": "Number of ways to express the integer n as a product of its unitary divisors (A034444).",
			"comment": [
				"Equivalently, a(n) is the number of ways to express the cyclic group Z_n as a direct sum of its Hall subgroups.  A Hall subgroup of a finite group G is a subgroup whose order is coprime to its index.",
				"a(n) is the number of ways to partition the set of distinct prime factors of n.",
				"Also the number of singleton or pairwise coprime factorizations of n. - _Gus Wiseman_, Sep 24 2019"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A259936/b259936.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Hall_subgroup\"\u003eHall subgroup\u003c/a\u003e",
				"\u003ca href=\"/index/Eu#epf\"\u003eIndex entries for sequences computed from exponents in factorization of n\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A000110(A001221(n)).",
				"a(n \u003e 1) = A327517(n) + 1. - _Gus Wiseman_, Sep 24 2019"
			],
			"example": [
				"a(60) = 5 because we have: 60 = 4*3*5 = 4*15 = 3*20 = 5*12.",
				"For n = 36, its unitary divisors are 1, 4, 9, 36. From these we obtain 36 either as 1*36 or 4*9, thus a(36) = 2. - _Antti Karttunen_, Oct 21 2017"
			],
			"maple": [
				"map(combinat:-bell @ nops @ numtheory:-factorset, [$1..100]); # _Robert Israel_, Jul 09 2015"
			],
			"mathematica": [
				"Table[BellB[PrimeNu[n]], {n, 1, 75}]",
				"(* second program *)",
				"facs[n_]:=If[n\u003c=1,{{}},Join@@Table[Map[Prepend[#,d]\u0026,Select[facs[n/d],Min@@#\u003e=d\u0026]],{d,Rest[Divisors[n]]}]];",
				"Table[Length[Select[facs[n],Length[#]==1||CoprimeQ@@#\u0026]],{n,100}] (* _Gus Wiseman_, Sep 24 2019 *)"
			],
			"program": [
				"(PARI) a(n) = my(t=omega(n), x='x, m=contfracpnqn(matrix(2, t\\2, y, z, if( y==1, -z*x^2, 1 - (z+1)*x)))); polcoeff(1/(1 - x + m[2, 1]/m[1, 1]) + O(x^(t+1)), t) \\\\ _Charles R Greathouse IV_, Jun 30 2017"
			],
			"xref": [
				"Cf. A000110, A001055, A001221, A034444, A089233, A258466, A281116, A285572.",
				"Differs from A050320 for the first time at n=36.",
				"Cf. A304716, A302569, A304711, A305079.",
				"Related classes of factorizations:",
				"- No conditions: A001055",
				"- Strict: A045778",
				"- Constant: A089723",
				"- Distinct multiplicities: A255231",
				"- Singleton or coprime: A259936",
				"- Relatively prime: A281116",
				"- Aperiodic: A303386",
				"- Stable (indivisible): A305149",
				"- Connected: A305193",
				"- Strict relatively prime: A318721",
				"- Uniform: A319269",
				"- Intersecting: A319786",
				"- Constant or distinct factors coprime: A327399",
				"- Constant or relatively prime: A327400",
				"- Coprime: A327517",
				"- Not relatively prime: A327658",
				"- Distinct factors coprime: A327695"
			],
			"keyword": "nonn",
			"offset": "1,6",
			"author": "_Geoffrey Critzer_, Jul 09 2015",
			"references": 23,
			"revision": 25,
			"time": "2019-09-24T22:01:23-04:00",
			"created": "2015-07-09T15:22:25-04:00"
		}
	]
}