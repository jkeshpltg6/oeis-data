{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A213806",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 213806,
			"data": "1,1,7,3,1,3,4,5,1,9,1,1,39,2,46,16,42,68,1,175,1,5,50,1,627,1256,1177,10,1860,7144,15,170,27156,178,64,2,6335,6334,15592,4522,3230,113926,99010,72256,114606,199042,1,198518,151036,236203,8557,26542,21388",
			"name": "Number of minimal coprime labelings for the complete bipartite graph K_{n,n}.",
			"comment": [
				"A minimal coprime labeling for K_{n,n} uses two disjoint n-subsets of {1,...,m} with minimal m = A213273(n) \u003e= 2*n as labels for the two disjoint vertex sets such that labels of adjacent vertices are relatively prime.  One of the label sets contains m."
			],
			"link": [
				"Kevin Cuadrado, \u003ca href=\"/A213806/b213806.txt\"\u003eTable of n, a(n) for n = 1..105\u003c/a\u003e",
				"Adam H. Berliner, N. Dean, J. Hook, A. Marr, A. Mbirika, ​C. McBee, \u003ca href=\"https://arxiv.org/abs/1604.07698\"\u003eCoprime and prime labelings of graphs\u003c/a\u003e, arXiv preprint arXiv:1604.07698 [math.CO], 2016.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CompleteBipartiteGraph.html\"\u003eComplete Bipartite Graph\u003c/a\u003e"
			],
			"formula": [
				"a(A284875(n)) = 1. - _Jonathan Sondow_, May 21 2017"
			],
			"example": [
				"a(1) = 1: the two label sets are {{1}, {2}} with m=2.",
				"a(2) = 1: {{1,3}, {2,4}} with m=4.",
				"a(3) = 7: {{2,4,5}, {1,3,7}}, {{1,3,5}, {2,4,7}}, {{2,3,4}, {1,5,7}}, {{2,3,6}, {1,5,7}}, {{2,4,6}, {1,5,7}}, {{3,4,6}, {1,5,7}}, {{1,2,4}, {3,5,7}}.",
				"a(4) = 3: {{2,4,7,8}, {1,3,5,9}}, {{2,4,5,8}, {1,3,7,9}}, {{1,2,4,8}, {3,5,7,9}}.",
				"a(5) = 1: {{2,4,5,8,10}, {1,3,7,9,11}}.",
				"a(21) = 1: {{2,4,5,8,10,11,16,20,22,23,25,29,31,32,40,44,46,50,55,58,62}, {1,3,7,9,13,17,19,21,27,37,39,41,43,47,49,51,53,57,59,61,63}}."
			],
			"maple": [
				"b:= proc(n, k, t, s) option remember;",
				"      `if`(nops(s)\u003e=t and k\u003e=t, binomial(nops(s), t),",
				"      `if`(n\u003c1, 0, b(n-1, k, t, s)+ b(n-1, k+1, t,",
				"      select(x-\u003e x\u003c\u003en and igcd(n, x)=1, s))))",
				"    end:",
				"g:= proc(n) option remember; local m, r;",
				"      for m from `if`(n=1, 2, g(n-1)[1]) do",
				"        r:= b(m-1, 1, n, select(x-\u003e igcd(m, x)=1, {$1..m-1}));",
				"        if r\u003e0 then break fi",
				"      od; [m, r]",
				"    end:",
				"a:= n-\u003e g(n)[2]:",
				"seq(a(n), n=1..11);"
			],
			"mathematica": [
				"b[n_, k_, t_, s_] := b[n, k, t, s] = If[Length[s] \u003e= t \u0026\u0026 k \u003e= t, Binomial[Length[s], t], If[n \u003c 1, 0, b[n - 1, k, t, s] + b[n - 1, k + 1, t, Select[s, # != n \u0026\u0026 GCD[n, #] == 1 \u0026]]]];",
				"g[n_] := g[n] = Module[{m, r}, For[ m = If[n == 1, 2, g[n - 1][[1]] ], True, m++, r = b[m - 1, 1, n, Select[Range[1, m - 1], GCD[m, #] == 1 \u0026]]; If [r \u003e 0,  Break[]]]; {m, r}];",
				"a[n_] := a[n] = g[n][[2]];",
				"Table[Print[\"a(\", n, \") = \", a[n]]; a[n], {n, 1, 18}] (* _Jean-François Alcover_, Nov 08 2017, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A213273, A284875, A291465."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Alois P. Heinz_, Jun 20 2012",
			"ext": [
				"Terms a(24) and beyond from _Kevin Cuadrado_, Dec 01 2020"
			],
			"references": 4,
			"revision": 36,
			"time": "2020-12-13T16:32:46-05:00",
			"created": "2012-06-21T06:19:37-04:00"
		}
	]
}