{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A168510",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 168510,
			"data": "1,4,54,2304,300000,116640000,133413966000,444110104166400,4267295479315169280,117595223746560000000000,9245836018244425723200000000,2065215715357207851951980544000000",
			"name": "Products across consecutive rows of the denominators of the Leibniz harmonic triangle (A003506).",
			"comment": [
				"As in A001142, lim_{n-\u003einf} (a(n)a(n+2))/a(n+1)^2 = e, demonstrating an underlying relation between A003506 and Pascal's triangle A007318. Unlike A001142, in this case the function is asymptotic from above."
			],
			"link": [
				"A. Bogomolny, \u003ca href=\"http://www.cut-the-knot.org/Curriculum/Combinatorics/LeibnitzTriangle.shtml\"\u003eCut The Knot: Leibniz and Pascal Triangles\u003c/a\u003e",
				"H. J. Brothers, \u003ca href=\"https://doi.org/10.1017/S0025557200004447\"\u003ePascal's prism\u003c/a\u003e, The Mathematical Gazette, 96 (July 2012), 213-220."
			],
			"formula": [
				"a(n) = n!*Product_{k=1..n} k^(2k-n-1).",
				"a(n) = Product_{j=1..n} Product_{k=2..j} ((1-1/k)^-k).",
				"a(1) = 1; a(n) = a(n-1)*Product_{k=2..n} ((1-1/k)^-k).",
				"a(n) ~ A^2 * exp(n^2/2 - 1/12) * n^(n/2 + 1/6) / (2*Pi)^(n/2), where A is the Glaisher-Kinkelin constant A074962. - _Vaclav Kotesovec_, Oct 22 2017",
				"a(n) = Product_{k=0..n-1} (n-k)^(n-2k). - _Peter Munn_, Mar 07 2018"
			],
			"example": [
				"For n=3, row 3 of A003506 = {3, 6, 3} and a(3)=54.",
				"a(5) = 5^5 * 4^3 * 3^1 * 2^-1 * 1^-3 = 5^5 * 3 * 2^5 = 300000. - _Peter Munn_, Mar 07 2018"
			],
			"mathematica": [
				"Table[n! Product[k^(2 k - n - 1), {k, 1, n}], {n, 1, 12}]",
				"Table[Product[Product[(1 - 1/k)^-k, {k, 2, j}], {j, 1, n}], {n, 1, 12}]",
				"(* or *)",
				"a[1] = 1; a[n_] := a[n - 1] Product[(1 - 1/k)^-k, {k, 2, n}]"
			],
			"xref": [
				"Cf. A003506, A001142, A007318. For n \u003e= 1, a(n) = n!*A001142(n)."
			],
			"keyword": "easy,nonn",
			"offset": "1,2",
			"author": "_Harlan J. Brothers_, Nov 27 2009",
			"references": 2,
			"revision": 24,
			"time": "2018-03-11T03:22:43-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}