{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A308737",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 308737,
			"data": "1,1,3,1,8,7,1,17,31,15,1,34,96,94,31,1,67,258,382,253,63,1,132,645,1280,1275,636,127,1,261,1545,3845,5115,3831,1531,255,1,518,3598,10766,17920,17906,10738,3578,511,1,1031,8212,28700,57358,71666,57316,28652,8185,1023",
			"name": "Triangle of scaled 1-tiered binomial coefficients, T(n,k) = 2^(n+1)*(n-k,k)_1 (n \u003e= 0, 0 \u003c= k \u003c= n), where (N,M)_1 is the 1-tiered binomial coefficient.",
			"link": [
				"Michael E. Hoffman, \u003ca href=\"https://www.usna.edu/Users/math/meh/mpi18.pdf\"\u003e(Poly)logarithmic Integrals and Multiple Zeta Values\u003c/a\u003e, Number Theory Talk, Max-Planck-Institut für Mathematik, Bonn, 20 June 2018. See Slide 29.",
				"Michael E. Hoffman and Markus Kuba, \u003ca href=\"https://arxiv.org/abs/1906.08347\"\u003eLogarithmic integrals, zeta values, and tiered binomial coefficients\u003c/a\u003e, arXiv:1906.08347 [math.CO], 2019-2020. See Example 4 on pp. 12-13."
			],
			"formula": [
				"(N,M)_1 = ((N-1,M)_1 + (N,M-1)_1 + (N,M-1)_0)/2 for N, M \u003e= 0 and N + M \u003e 0 with initial conditions: (N,-1)_i = 0 = (-1,M)_i for i = 0, 1; (0,0)_1 = 1/2; and (N,M)_0 = binomial(N+M, M) for N, M \u003e= 0. (This is the recurrence for the square array presentation of the regular triangle.) [Edited by _Petros Hadjicostas_, Jul 06 2020]",
				"(N,M)_1 = binomial(N+M-1, M-1) - (binomial(N+M, N)/2 - binomial(N+M-1, N-1))/2^(N+M) for N,M \u003e= 0 and N + M \u003e 0 with (0,0)_1 = 1/2. - _Petros Hadjicostas_, Jul 06 2020",
				"G.f. for (N,M)_1: (1-x)/((1-x-y)*(2-x-y)). - _Jean-François Alcover_, Jun 21 2019",
				"Scaled coefficients satisfy T(n,0) = 1 for n \u003e= 0 and T(n,k) = T(n-1,k) + T(n-1,k-1) + 2^n*C(n-1,k-1) for n \u003e= k+1 \u003e= 1. - _Charlie Neder_, Jun 21 2019 [Corrected by _Petros Hadjicostas_, Jul 06 2020]",
				"From _Petros Hadjicostas_, Jul 07 2020: (Start)",
				"(N,M)_1 + (M,N)_1 = (N,M)_0 = binomial(N+M, N) for N, M \u003e= 0.",
				"(n-k,k)_1 + (k, n-k)_1 = binomial(n,k) for n \u003e= k \u003e= 0.",
				"T(n,k) + T(n,n-k) = 2^(n+1)*binomial(n,k) = 2*A038208(n,k) for n \u003e= k \u003e= 0.",
				"T(n,k) = 2^(n + 1)*binomial(n-1, k-1) + 2*binomial(n-1,k) - binomial(n,k) for n \u003e= k \u003e= 0 and (n,k) \u003c\u003e (0,0) with T(0,0) = 1.",
				"G.f. for T(n,k): (1 - 2*x)/((1 - 2*x*(1 + y))*(1 - x*(1 + y))). (End)"
			],
			"example": [
				"From _Petros Hadjicostas_, Jul 07 2020: (Start)",
				"Square array for (N,M)_1 of 1-tiered binomial coefficients (N, M \u003e= 0):",
				"  1/2,   3/4,    7/8,     15/16,   31/32,     63/64,   127/128, ...",
				"  1/4,    1,    31/16,    47/16,  253/64,    159/32,  1531/256, ...",
				"  1/8,  17/16,    3,     191/32, 1275/128,  3831/256,  5369/256, ...",
				"  1/16, 17/16, 129/32,     10,   5115/256,  8953/256, 14329/256, ...",
				"  1/32, 67/64, 645/128, 3845/256,   35,    35833/512, 129003/1024, ...",
				"  ... (End)",
				"Triangle (n-k,k)_1 of 1-tiered binomial coefficients (n \u003e= 0 and k = 0..n):",
				"  1/2,",
				"  1/4,    3/4,",
				"  1/8,     1,    7/8,",
				"  1/16,  17/16, 31/16, 15/16,",
				"  1/32,  17/16,   3,   47/16, 31/32,",
				"  ...",
				"Scaled triangle T(n,k) after multiplying each row by 2^(n+1):",
				"  1,",
				"  1,  3,",
				"  1,  8,  7,",
				"  1, 17, 31, 15,",
				"  1, 34, 96, 94, 31,",
				"  ..."
			],
			"mathematica": [
				"rows = 10;",
				"cc = CoefficientList[# + O[y]^rows, y]\u0026 /@ CoefficientList[(1-x)/((1-x-y)* (2-x-y)) + O[x]^rows, x];",
				"T[n_, m_, 1] := cc[[n-m+1, m+1]];",
				"Table[2^(n+1) Table[T[n, m, 1], {m, 0, n}], {n, 0, rows-1}] (* _Jean-François Alcover_, Jun 21 2019 *)"
			],
			"program": [
				"(PARI) T(n,m) = if ((n==0) \u0026\u0026 (m==0), 1/2, binomial(n+m-1, m-1) - (binomial(n+m,n)/2 - binomial(n+m-1,n-1))/2^(n+m));",
				"TT(n, k) = T(n-k, k);",
				"tabls(nn) = for (n=0, nn, for (k=0, n, print1(2^(n+1)*TT(n, k), \", \")));"
			],
			"xref": [
				"Cf. A007318 (Pascal triangle: 0-tiered binomial coefficient), A038208, A145071 (column k = 1)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,3",
			"author": "_Michel Marcus_, Jun 21 2019",
			"ext": [
				"Name edited by _Petros Hadjicostas_, Jul 07 2020"
			],
			"references": 1,
			"revision": 72,
			"time": "2020-07-12T03:33:57-04:00",
			"created": "2019-06-25T06:10:15-04:00"
		}
	]
}