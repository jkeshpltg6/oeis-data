{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A055801",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 55801,
			"data": "1,1,1,1,1,1,1,1,1,1,1,1,1,2,1,1,1,1,2,2,1,1,1,1,2,3,3,1,1,1,1,2,3,4,3,1,1,1,1,2,3,5,6,4,1,1,1,1,2,3,5,7,7,4,1,1,1,1,2,3,5,8,11,10,5,1,1,1,1,2,3,5,8,12,14,11,5,1,1,1,1,2,3,5,8,13,19,21,15,6,1",
			"name": "Triangle T read by rows: T(i,0)=T(i,i)=1, T(i,j) = Sum_{k=1..floor(n/2)} T(i-2k, j-2k+1) for 1\u003c=j\u003c=i-1, where T(m,n) := 0 if m\u003c0 or n\u003c0.",
			"comment": [
				"T(i+j,j) is the number of strings (s(1),...,s(m)) of nonnegative integers s(k) such that m\u003c=i+1, s(m)=j and s(k)-s(k-1) is an odd positive integer for k=2,3,...,m.",
				"T(i+j,j) is the number of compositions of numbers \u003c=j using up to i parts, each an odd positive integer."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A055801/b055801.txt\"\u003eRows n = 0..100 of triangle, flattened\u003c/a\u003e",
				"Clark Kimberling, \u003ca href=\"https://www.fq.math.ca/Scanned/40-4/kimberling.pdf\"\u003ePath-counting and Fibonacci numbers\u003c/a\u003e, Fib. Quart. 40 (4) (2002) 328-338, Example 2B."
			],
			"example": [
				"Rows:",
				"  1",
				"  1  1",
				"  1  1  1",
				"  1  1  1  1",
				"  1  1  1  2  1",
				"  1  1  1  2  2  1",
				"  1  1  1  2  3  3  1",
				"  1  1  1  2  3  4  3  1",
				"  1  1  1  2  3  5  6  4  1",
				"  1  1  1  2  3  5  7  7  4  1",
				"  1  1  1  2  3  5  8 11 10  5  1",
				"  1  1  1  2  3  5  8 12 14 11  5  1",
				"  1  1  1  2  3  5  8 13 19 21 15  6  1",
				"  1  1  1  2  3  5  8 13 20 26 25 16  6  1",
				"  1  1  1  2  3  5  8 13 21 32 40 36 21  7  1",
				"  1  1  1  2  3  5  8 13 21 33 46 51 41 22  7  1",
				"T(9,6) counts the strings 3456, 1236, 1256, 1456, 036, 016, 056.",
				"T(9,6) counts the compositions 111, 113, 131, 311, 33, 15, 51."
			],
			"maple": [
				"A055801 := proc(i,j) option remember;",
				"    if j =0 or j = i then 1;",
				"    elif i \u003c 0 or j \u003c 0 then 0;",
				"    else add(procname(i-2*k,j-2*k+1),k=1..floor(i/2)) ;",
				"    end if;",
				"end proc:",
				"seq(seq(A055801(n,k), k=0..n),n=0..20); # _R. J. Mathar_, Feb 11 2018"
			],
			"mathematica": [
				"T[n_, k_]:= T[n, k]= If[n\u003c0 || k\u003c0, 0, If[k==0 || k==n, 1, Sum[T[n-2*j, k-2*j+1 ], {j, Floor[n/2]}]]]; Table[T[n, k], {n,0,15}, {k,0,n}]//Flatten (* _G. C. Greubel_, Jan 23 2020 *)"
			],
			"program": [
				"(PARI) T(n,k) = if(n\u003c0 || k\u003c0, 0, if(k==0 || k==n, 1, sum(j=1, n\\2, T(n-2*j, k-2*j+1))));",
				"for(n=0,15, for(k=0,n, print1(T(n,k), \", \"))) \\\\ _G. C. Greubel_, Jan 23 2020",
				"(MAGMA)",
				"function T(n,k)",
				"  if n lt 0 or k lt 0 then return 0;",
				"  elif k eq 0 or k eq n then return 1;",
				"  else return (\u0026+[T(n-2*j, k-2*j+1): j in [1..Floor(n/2)]]);",
				"  end if; return T; end function;",
				"[T(n,k): k in [0..n], n in [0..15]]; // _G. C. Greubel_, Jan 23 2020",
				"(Sage)",
				"@CachedFunction",
				"def T(n, k):",
				"    if (n\u003c0 or k\u003c0): return 0",
				"    elif (k==0 or k==n): return 1",
				"    else: return sum(T(n-2*j, k-2*j+1) for j in (1..floor(n/2)))",
				"[[T(n, k) for k in (0..n)] for n in (0..15)] # _G. C. Greubel_, Jan 23 2020",
				"(GAP)",
				"T:= function(n,k)",
				"    if n\u003c0 or k\u003c0 then return 0;",
				"    elif k=0 or k=n then return 1;",
				"    else return Sum([1..Int(n/2)], j-\u003e T(n-2*j, k-2*j+1));",
				"    fi; end;",
				"Flat(List([0..15], n-\u003e List([0..n], k-\u003e T(n,k) ))); # _G. C. Greubel_, Jan 23 2020"
			],
			"xref": [
				"Infinitely many of the columns are (1, 1, 1, 2, 3, 5, 8, ..., Fibonacci numbers)",
				"Essentially a reflected version of A011794.",
				"Cf. A055802, A055803, A055804, A055805, A055806."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,14",
			"author": "_Clark Kimberling_, May 28 2000",
			"references": 7,
			"revision": 21,
			"time": "2020-01-24T07:04:05-05:00",
			"created": "2000-06-15T03:00:00-04:00"
		}
	]
}