{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A053262",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 53262,
			"data": "1,1,1,2,1,3,2,3,3,5,3,6,5,7,7,9,7,12,11,13,13,17,15,21,20,24,24,29,28,36,35,40,42,50,48,58,58,67,70,80,79,93,95,106,111,125,127,145,149,166,172,191,196,222,229,250,262,289,298,330,343,373,391,427,442,486",
			"name": "Coefficients of the 5th-order mock theta function chi_0(q).",
			"comment": [
				"The rank of a partition is its largest part minus the number of parts."
			],
			"reference": [
				"Srinivasa Ramanujan, Collected Papers, Chelsea, New York, 1962, pp. 354-355.",
				"Srinivasa Ramanujan, The Lost Notebook and Other Unpublished Papers, Narosa Publishing House, New Delhi, 1988, pp. 20, 23, 25."
			],
			"link": [
				"Vaclav Kotesovec, \u003ca href=\"/A053262/b053262.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e (corrected and extended previous b-file from G. C. Greubel)",
				"George E. Andrews, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-1986-0814916-2\"\u003eThe fifth and seventh order mock theta functions\u003c/a\u003e, Trans. Amer. Math. Soc., 293 (1986) 113-134.",
				"George E. Andrews and Frank G. Garvan, \u003ca href=\"http://dx.doi.org/10.1016/0001-8708(89)90070-4\"\u003eRamanujan's \"lost\" notebook VI: The mock theta conjectures\u003c/a\u003e, Advances in Mathematics, 73 (1989) 242-255.",
				"George N. Watson, \u003ca href=\"https://doi.org/10.1112/plms/s2-42.1.274\"\u003eThe mock theta functions (2)\u003c/a\u003e, Proc. London Math. Soc., series 2, 42 (1937) 274-304."
			],
			"formula": [
				"G.f.: chi_0(q) = Sum_{n\u003e=0} q^n/((1-q^(n+1))(1-q^(n+2))...(1-q^(2n))).",
				"G.f.: chi_0(q) = 1 + Sum_{n\u003e=0} q^(2n+1)/((1-q^(n+1))(1-q^(n+2))...(1-q^(2n+1))).",
				"a(n) is the number of partitions of 5n with rank == 1 (mod 5) minus number with rank == 0 (mod 5).",
				"a(n) is the number of partitions of n with unique smallest part and all other parts \u003c= twice the smallest part.",
				"a(n) is the number of partitions where the largest part is odd and all other parts are greater than half of the largest part. - _N. Sato_, Jan 21 2010",
				"a(n) ~ exp(Pi*sqrt(2*n/15)) / sqrt((5 + sqrt(5))*n). - _Vaclav Kotesovec_, Jun 12 2019"
			],
			"mathematica": [
				"1+Series[Sum[q^(2n+1)/Product[1-q^k, {k, n+1, 2n+1}], {n, 0, 49}], {q, 0, 100}]",
				"nmax = 100; CoefficientList[Series[1 + Sum[x^(2*k+1)/Product[1-x^j, {j, k+1, 2*k+1}], {k, 0, Floor[nmax/2]}], {x, 0, nmax}], x] (* _Vaclav Kotesovec_, Jun 12 2019 *)"
			],
			"xref": [
				"Other '5th-order' mock theta functions are at A053256, A053257, A053258, A053259, A053260, A053261, A053263, A053264, A053265, A053266, A053267."
			],
			"keyword": "nonn,easy",
			"offset": "0,4",
			"author": "_Dean Hickerson_, Dec 19 1999",
			"references": 15,
			"revision": 34,
			"time": "2021-02-02T21:11:08-05:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}