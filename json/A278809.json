{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A278809",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 278809,
			"data": "1,0,8,2,8,7,3,6,0,9,5,2,0,7,3,8,6,9,4,0,8,2,8,5,0,3,1,3,4,5,3,1,0,0,8,0,2,5,7,8,6,3,4,5,4,7,8,5,3,8,5,0,6,4,3,2,8,8,4,7,8,2,1,6,8,0,6,9,2,2,7,8,8,9,5,2,9,9,5,5,7,4,7,0,6,8,1,4,4,8,7,8,6,2,3,9,2,4,4,3,1,1,5,4,5,9,9,1,8,9,2,4,3,8,8,4,0,6,3,6,2,6,1,3,5,9,3,4,0,0",
			"name": "Decimal expansion of b(1) in the sequence b(n+1) = c^(b(n)/n) A278449, where c=3 and b(1) is chosen such that the sequence neither explodes nor goes to 1.",
			"comment": [
				"For the given c there exists a unique b(1) for which the sequence b(n) does not converge to 1 and at the same time always satisfies b(n-1)b(n+1)/b(n)^2 \u003c 1.",
				"If b(1) were chosen smaller the sequence b(n) would approach 1, if it were chosen greater it would at some point violate b(n-1)b(n+1)/b(n)^2 \u003c 1 and from there on quickly escalate.",
				"The value of b(1) is found through trial and error. Illustrative example for the case of c=2 (for c=3 similar): \"Suppose one starts with b(1) = 2, the sequence b(n) would continue b(2) = 4, b(3) = 4, b(4) = 2.51..., b(5) = 1.54... and from there one can see that such a sequence is tending to 1. One continues by trying a larger value, say b(1) = 3, which gives rise to b(2) = 8, b(3) = 16, b(4) = 40.31... and from there one can see that such a sequence is escalating too fast. Therefore, one now knows that the true value of b(1) is between 2 and 3.\""
			],
			"link": [
				"Rok Cestnik, \u003ca href=\"/A278809/b278809.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Rok Cestnik, \u003ca href=\"/A278809/a278809.pdf\"\u003ePlot of the dependence of b(1) on c\u003c/a\u003e"
			],
			"formula": [
				"log_3(2*log_3(3*log_3(4*log_3(...)))). - _Andrey Zabolotskiy_, Dec 01 2016"
			],
			"example": [
				"1.08287360952073869408285031345310080257863454785385..."
			],
			"mathematica": [
				"c = 3;",
				"n = 100;",
				"acc = Round[n*1.2];",
				"th = 1000000;",
				"b1 = 0;",
				"For[p = 0, p \u003c acc, ++p,",
				"  For[d = 0, d \u003c 9, ++d,",
				"    b1 = b1 + 1/10^p;",
				"    bn = b1;",
				"    For[i = 1, i \u003c Round[n*1.2], ++i,",
				"     bn = N[c^(bn/i), acc];",
				"     If[bn \u003e th, Break[]];",
				"     ];",
				"    If[bn \u003e th, {",
				"      b1 = b1 - 1/10^p;",
				"      Break[];",
				"      }];",
				"    ];",
				"  ];",
				"N[b1,n]",
				"RealDigits[ Fold[ Log[3, #1*#2] \u0026, 1, Reverse@ Range[2, 160]], 10, 111][[1]] (* _Robert G. Wilson v_, Dec 02 2016 *)"
			],
			"xref": [
				"For sequence round(b(n)) see A278449.",
				"For different values of c see A278808, A278810, A278811, A278812.",
				"For b(1)=0 see A278813."
			],
			"keyword": "nonn,cons",
			"offset": "1,3",
			"author": "_Rok Cestnik_, Nov 28 2016",
			"references": 7,
			"revision": 18,
			"time": "2016-12-11T08:42:41-05:00",
			"created": "2016-11-30T09:16:43-05:00"
		}
	]
}