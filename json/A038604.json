{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A038604",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 38604,
			"data": "3,5,7,11,13,17,19,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97,101,103,107,109,113,131,137,139,149,151,157,163,167,173,179,181,191,193,197,199,307,311,313,317,331,337,347,349,353,359,367,373,379,383,389,397",
			"name": "Primes not containing the digit '2'.",
			"comment": [
				"Subsequence of primes of A052404. - _Michel Marcus_, Feb 21 2015",
				"Maynard proves that this sequence is infinite and in particular contains the expected number of elements up to x, on the order of x^(log 9/log 10)/log x. - _Charles R Greathouse IV_, Apr 08 2016"
			],
			"link": [
				"Indranil Ghosh, \u003ca href=\"/A038604/b038604.txt\"\u003eTable of n, a(n) for n = 1..50000\u003c/a\u003e",
				"M. F. Hasler, \u003ca href=\"/wiki/Numbers_avoiding_certain_digits\"\u003eNumbers avoiding certain digits\u003c/a\u003e OEIS wiki, Jan 12 2020.",
				"James Maynard, \u003ca href=\"http://arxiv.org/abs/1604.01041\"\u003ePrimes with restricted digits\u003c/a\u003e, arXiv:1604.01041 [math.NT], 2016.",
				"James Maynard and Brady Haran, \u003ca href=\"https://www.youtube.com/watch?v=eeoBCS7IEqs\"\u003ePrimes without a 7\u003c/a\u003e, Numberphile video (2019)."
			],
			"formula": [
				"Intersection of A000040 and A052404. - _M. F. Hasler_, Jan 11 2020"
			],
			"mathematica": [
				"Select[Prime[Range[70]], DigitCount[#, 10, 2] == 0 \u0026] (* _Vincenzo Librandi_, Aug 08 2011 *)"
			],
			"program": [
				"(MAGMA) [ p: p in PrimesUpTo(400) | not 2 in Intseq(p) ]; // _Bruno Berselli_, Aug 08 2011",
				"(PARI) lista(nn, d=2) = {forprime(p=2, nn, if (!vecsearch(vecsort(digits(p),,8), d), print1(p, \", \")););} \\\\ _Michel Marcus_, Feb 21 2015",
				"(PARI)",
				"select( {is_A038604(n)=is_A052404(n)\u0026\u0026isprime(n)}, [1..400]) \\\\ see Wiki for more",
				"{next_A038604(n)=until((n==nextprime(n+1))==n=next_A052404(n-1),);n} \\\\ _M. F. Hasler_, Jan 12 2020",
				"(Python)",
				"from sympy import isprime, nextprime",
				"def is_A038604(n): return str(n).find('2')\u003c0 and isprime(n)",
				"def next_A038604(n): # get smallest term \u003e n",
				"    while True:",
				"        n = nextprime(n); s = str(n); t = s.find('2')",
				"        if t \u003c 0: return n",
				"        t = 10**(len(s)-1-t); n += t - n%t",
				"def A038604_upto(stop=math.inf, start=3):",
				"    while start \u003c stop: yield start; start = next_A038604(start)",
				"list(A038604_upto(400))",
				"# _M. F. Hasler_, Jan 12 2020"
			],
			"xref": [
				"Cf. A000040, A052404.",
				"Subsequence of A065091 (odd primes).",
				"Primes having no digit d = 0..9 are A038618, A038603, this sequence, A038611, A038612, A038613, A038614, A038615, A038616, and A038617, respectively."
			],
			"keyword": "nonn,easy,base",
			"offset": "1,1",
			"author": "Vasiliy Danilov (danilovv(AT)usa.net), Jul 15 1998",
			"ext": [
				"Offset corrected by _Arkadiusz Wesolowski_, Aug 07 2011"
			],
			"references": 13,
			"revision": 70,
			"time": "2020-04-18T09:18:46-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}