{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A247498",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 247498,
			"data": "1,0,1,-1,1,1,0,0,2,1,5,-2,3,3,1,0,0,2,8,4,1,-61,16,-3,18,15,5,1,0,0,2,32,52,24,6,1,1385,-272,63,48,165,110,35,7,1,0,0,2,128,484,480,198,48,8,1,-50521,7936,-1383,528,1395,2000,1085,322,63,9,1",
			"name": "Generalized Euler numbers: Square array read by descending antidiagonals, T(n, k) = k!*[x^k] exp(n*x)*sech(x), n\u003e=0, k\u003e=0.",
			"comment": [
				"This two-dimensional array of numbers can be seen as a generalization of the Euler secant and Euler tangent numbers (which are in their compressed and signless form A000364 resp. A000182 or interleaved in A000111). The cases n=0 and n=1 reduce to their expanded and signed forms A122045 and A155585. Moreover the columns are the values of the Swiss-Knife polynomials A153641 evaluated at the nonnegative integers.",
				"Subsequences [3,3,1], [8,4,1], [15,5,1], [24,6,1], [35,7,1], [48,8,1], [[63,9,1] found in rows of this entry, as a triangular array, are present in the antidiagonals of Table 5 of the East and Gray reference (A244490), and some subsequences in the rows of Table 5 are found in the antidiagonals of this entry, including [3,2,1] and [1,1]. Equivalently, the first four columns of Table 5 are embedded in this entry viewed as a square array on the table page. An explicit formula with combinatorial interpretations for these numbers is provided in the reference, and others are known for the corresponding columns for the modified Hermite polynomials of A244490. - _Tom Copeland_, Oct 04 2016"
			],
			"link": [
				"J. East, R. D. Gray, \u003ca href=\"http://arxiv.org/abs/1404.2359\"\u003eIdempotent generators in finite partition monoids and related semigroups\u003c/a\u003e, arXiv preprint arXiv:1404.2359 [math.GR], 2014."
			],
			"formula": [
				"G.f. for column k: the k-th column consists of the values of the k-th Swiss-Knife polynomial skp_{k}(x) evaluated at x = 0,1,2,...",
				"O.g.f. for column k: Sum_{j=0..k} (-1)^(k+1)*A247501(k,j)/(x-1)^(j+1)."
			],
			"example": [
				"Square array starts:",
				"[n\\k][0][1] [2]  [3]   [4]   [5]    [6]     [7]     [8]",
				"[0]   1, 0, -1,   0,    5,    0,   -61,      0,   1385, ... A122045",
				"[1]   1, 1,  0,  -2,    0,   16,     0,   -272,      0, ... A155585",
				"[2]   1, 2,  3,   2,   -3,    2,    63,      2,  -1383, ... A119880",
				"[3]   1, 3,  8,  18,   32,   48,   128,    528,    512, ... A119881",
				"[4]   1, 4, 15,  52,  165,  484,  1395,   4372,  14505, ...",
				"[5]   1, 5, 24, 110,  480, 2000,  8064,  32240, 130560, ... A225116",
				"[6]   1, 6, 35, 198, 1085, 5766, 29855, 151878, 766745, ...",
				"A000012, A001477, A067998, A121670, ...",
				".",
				"Triangular array starts:",
				"                1,",
				"              0,  1,",
				"           -1,  1,  1,",
				"          0,  0,  2,  1,",
				"        5, -2,  3,  3,  1,",
				"      0,  0,  2,  8,  4,  1,",
				"  -61, 16, -3, 18, 15,  5,  1."
			],
			"maple": [
				"# EGF (row)",
				"egf := n -\u003e exp(n*x)*sech(x):",
				"seq(print(seq(k!*coeff(series(egf(n),x,k+2),x,k),k=0..8)), n=0..6);",
				"# Swiss-Knife polynomial (column)",
				"SKP := proc(n, x) local v, k, A; A := k -\u003e `if`(k mod 4 = 0,0,(-1)^iquo(k,4)); add(2^iquo(-k,2)*A(k+1)*add((-1)^v* binomial(k,v)*(v+x+1)^n,v=0..k), k=0..n); expand(%) end:",
				"seq(print(seq(SKP(k, n), n=0..9)), k=0..6);",
				"# OGF (column)",
				"col := proc(n, len) local T; T := A247501_row(n);",
				"(-1)^(n+1)*add(T[k+1]/(x-1)^(k+1),k=0..n);",
				"seq(coeff(series(%,x,len+1),x,j),j=0..len) end:",
				"seq(print(col(n,8)), n=0..6);"
			],
			"mathematica": [
				"nmax = 10; Clear[row]; row[n_] := row[n] = CoefficientList[Exp[n*x]*Sech[x] + O[x]^(nmax+2), x][[1 ;; nmax+1]]*Range[0, nmax]!;",
				"rows = Table[row[n], {n, 0, nmax}];",
				"T[n_, k_] := rows[[n+1, k+1]];",
				"Table[T[n-k, k], {n, 0, nmax}, {k, n, 0, -1}] // Flatten (* _Jean-François Alcover_, Dec 03 2017 *)"
			],
			"xref": [
				"Cf. A122045, A155585, A119880, A119881, A225116, A000364, A000182, A000111, A067998, A121670, A153641, A247501.",
				"Cf. A244490."
			],
			"keyword": "tabl,sign",
			"offset": "0,9",
			"author": "_Peter Luschny_, Dec 14 2014",
			"references": 1,
			"revision": 43,
			"time": "2018-01-15T21:10:26-05:00",
			"created": "2015-01-16T10:17:07-05:00"
		}
	]
}