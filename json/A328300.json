{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A328300",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 328300,
			"data": "1,1,1,1,3,1,1,7,7,1,1,15,26,15,1,1,31,82,82,31,1,1,63,237,343,237,63,1,1,127,651,1257,1257,651,127,1,1,255,1730,4256,5594,4256,1730,255,1,1,511,4494,13669,22411,22411,13669,4494,511,1,1,1023,11485,42279,83680,103730,83680,42279,11485,1023,1",
			"name": "Number T(n,k) of n-step walks on cubic lattice starting at (0,0,0), ending at (0,k,n-k), remaining in the first (nonnegative) octant and using steps (0,0,1), (0,1,0), (1,0,0), (-1,1,1), (1,-1,1), and (1,1,-1); triangle T(n,k), n\u003e=0, 0\u003c=k\u003c=n, read by rows.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A328300/b328300.txt\"\u003eRows n = 0..200, flattened\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Lattice_path\"\u003eLattice path\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Self-avoiding_walk\"\u003eSelf-avoiding walk\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = T(n,n-k)."
			],
			"example": [
				"Triangle T(n,k) begins:",
				"  1;",
				"  1,   1;",
				"  1,   3,    1;",
				"  1,   7,    7,    1;",
				"  1,  15,   26,   15,    1;",
				"  1,  31,   82,   82,   31,    1;",
				"  1,  63,  237,  343,  237,   63,    1;",
				"  1, 127,  651, 1257, 1257,  651,  127,   1;",
				"  1, 255, 1730, 4256, 5594, 4256, 1730, 255, 1;",
				"  ..."
			],
			"maple": [
				"b:= proc(l) option remember; `if`(l[-1]=0, 1, (r-\u003e add(",
				"      add(add(`if`(i+j+k=1, (h-\u003e `if`(h[1]\u003c0, 0, b(h)))(",
				"      sort(l-[i, j, k])), 0), k=r), j=r), i=r))([$-1..1]))",
				"    end:",
				"T:= (n, k)-\u003e b(sort([0, k, n-k])):",
				"seq(seq(T(n, k), k=0..n), n=0..12);"
			],
			"mathematica": [
				"b[l_List] := b[l] = If[l[[-1]] == 0, 1, Function[r, Sum[Sum[Sum[If[i + j + k == 1, Function[h, If[h[[1]] \u003c 0, 0, b[h]]][Sort[l - {i, j, k}]], 0], {k, r}], {j, r}], {i, r}]][{-1, 0, 1}]];",
				"T[n_, k_] := b[Sort[{0, k, n - k}]];",
				"Table[T[n, k], {n, 0, 12}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, May 10 2020, after Maple *)"
			],
			"xref": [
				"Columns k=0-1 give: A000012, A000225.",
				"Row sums give A328296.",
				"T(2n,n) gives A328269.",
				"T(n,floor(n/2)) gives A328280.",
				"Cf. A007318, A008288, A091533, A328297, A328347."
			],
			"keyword": "nonn,tabl,walk",
			"offset": "0,5",
			"author": "_Alois P. Heinz_, Oct 11 2019",
			"references": 6,
			"revision": 26,
			"time": "2020-05-10T19:35:11-04:00",
			"created": "2019-10-11T22:13:06-04:00"
		}
	]
}