{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A347571",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 347571,
			"data": "1,0,1,2,9,44,280,2064,17528,167488,1777536,20721920,263055232,3610443264,53256280064,839974309888,14103897738240,251146689069056,4726795773018112,93746994502828032,1954053073794596864,42702893781890498560,976276451410488066048,23303485413254033309696",
			"name": "Expansion of the e.g.f. (-1 - 2*x - 2*log(1 - x) + exp(-2*x) / (1 - x)^2) / 4 + 1.",
			"comment": [
				"For all p prime, a(p) == -1 (mod p).",
				"For n \u003e 1, a(n) == 0 (mod (n-1))."
			],
			"formula": [
				"a(n) = Sum_{k=0..floor(n/2)} ceiling(2^(k-2))*A106828(n, k).",
				"a(n) ~ n * n! / (4*exp(2)). - _Vaclav Kotesovec_, Sep 10 2021"
			],
			"example": [
				"E.g.f.: 1 + x^2/2! + 2*x^3/3! + 9*x^4/4! + 44*x^5/5! + 280*x^6/6! + 2064*x^7/7! + 17528*x^8/8! + 167488*x^9/9! + ...",
				"a(11) = Sum_{k=0..5} ceiling(2^(k-2))*A106828(11, k) = 20721920.",
				"For k = 0, A106828(11,0) = 0.",
				"For k = 1, ceiling(2^(1-2))*A106828(11, 1) == -1 (mod 11), because ceiling(2^(1-2)) = 1 and A106828(11, 1) = (11-1)!",
				"For k \u003e= 2, ceiling(2^(k-2))*A106828(11, k) == 0 (mod 11), because A106828(11, k) == 0 (mod 11), result a(11) == -1 (mod 11).",
				"a(10) = Sum_{k=0..5} ceiling(2^(k-2))*A106828(10, k) = 1777536.",
				"a(10) == 0 (mod (10-1)), because for k \u003e= 0, A106828(10, k) == 0 (mod 9)."
			],
			"maple": [
				"a := series((-1-2*x-2*log(1-x)+exp(-2*x)/(1-x)^2)/4+1, x=0, 24):",
				"seq(n!*coeff(a, x, n), n=0..23);",
				"# second program:",
				"a := n -\u003e add(ceil(2^(k-2))*A106828(n, k), k=0..iquo(n, 2)):",
				"seq(a(n), n=0..23);"
			],
			"mathematica": [
				"CoefficientList[Series[(-1 - 2*x - 2*Log[1 - x] + Exp[-2*x]/(1 - x)^2)/4 + 1, {x, 0, 23}], x]*Range[0, 23]!"
			],
			"program": [
				"(PARI) my(x='x+O('x^30)); Vec(serlaplace((-1-2*x-2*log(1-x)+exp(-2*x)/(1-x)^2)/4 + 1)) \\\\ _Michel Marcus_, Sep 07 2021"
			],
			"xref": [
				"Cf. A106828, A343482, A345697, A345969, A346119, A347210."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Mélika Tebni_, Sep 07 2021",
			"references": 3,
			"revision": 8,
			"time": "2021-09-10T14:22:13-04:00",
			"created": "2021-09-10T14:22:13-04:00"
		}
	]
}