{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A195034",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 195034,
			"data": "0,21,41,83,123,186,246,330,410,515,615,741,861,1008,1148,1316,1476,1665,1845,2055,2255,2486,2706,2958,3198,3471,3731,4025,4305,4620,4920,5256,5576,5933,6273,6651,7011,7410,7790,8210,8610,9051,9471",
			"name": "Vertex number of a square spiral in which the length of the first two edges are the legs of the primitive Pythagorean triple [21, 20, 29]. The edges of the spiral have length A195033.",
			"comment": [
				"Zero together with partial sums of A195033.",
				"The only primes in the sequence are 41 and 83 since a(n) = (1/2)*((2*n+(-1)^n+3)/4)*((82*n-43*(-1)^n+43)/4). - Bruno Berselli, Oct 12 2011",
				"The spiral contains infinitely many Pythagorean triples in which the hypotenuses on the main diagonal are the positives multiples of 29 (Cf. A195819). The vertices on the main diagonal are the numbers A195038 = (21+20)*A000217 = 41*A000217, where both 21 and 20 are the first two edges in the spiral. The distance \"a\" between nearest edges that are perpendicular to the initial edge of the spiral is 21, while the distance \"b\" between nearest edges that are parallel to the initial edge is 20, so the distance \"c\" between nearest vertices on the same axis is 29 because from the Pythagorean theorem we can write c = (a^2+b^2)^(1/2) = sqrt(21^2+20^2) = sqrt(441+400) = sqrt(841) = 29. - _Omar E. Pol_, Oct 12 2011"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A195034/b195034.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Ron Knott, \u003ca href=\"http://www.maths.surrey.ac.uk/hosted-sites/R.Knott/Pythag/pythag.html#uadgen\"\u003ePythagorean triangles and Triples\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PythagoreanTriple.html\"\u003ePythagorean Triple\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,2,-2,-1,1)."
			],
			"formula": [
				"From _Bruno Berselli_, Oct 12 2011:  (Start)",
				"G.f.: x*(21+20*x)/((1+x)^2*(1-x)^3).",
				"a(n) = (2*n*(41*n+83)-(2*n+43)*(-1)^n+43)/16.",
				"a(n) = a(n-1)+2*a(n-2)-2*a(n-3)-a(n-4)+a(n-5).",
				"a(n)-a(-n-2) = A142150(n+1).  (End)"
			],
			"mathematica": [
				"LinearRecurrence[{1,2,-2,-1,1},{0,21,41,83,123},50] (* _Harvey P. Dale_, May 02 2012 *)"
			],
			"program": [
				"(MAGMA) [(2*n*(41*n+83)-(2*n+43)*(-1)^n+43)/16: n in [0..50]]; // _Vincenzo Librandi_, Oct 14 2011",
				"(PARI) concat(0, Vec(x*(21+20*x)/((1+x)^2*(1-x)^3) + O(x^60))) \\\\ _Michel Marcus_, Mar 08 2016"
			],
			"xref": [
				"Cf. A195020, A195032, A195033, A195036, A195038."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Omar E. Pol_, Sep 12 2011",
			"references": 8,
			"revision": 34,
			"time": "2016-03-08T10:10:48-05:00",
			"created": "2011-10-12T12:54:58-04:00"
		}
	]
}