{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A343634",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 343634,
			"data": "23,24,3,25,26,4,27,28,1,2951,23,327,2952,2953,328,2954,2955,34,2956,2957,329,24,2958,330,2959,2960,35,2961,2962,331,2963,2964,3,2965,2966,36,2967,2968,332,2969,2970,333,2971,25,37,2972,2973,334,2974,2975,335,2976,2977,38,26",
			"name": "Number of the fraction which has (the digits of) n as repeating period in its decimal expansion, according to the canonical enumeration A038566/A038567.",
			"comment": [
				"The fraction f = n/(10^L-1), where L is the number of decimal digits of n, has the infinite decimal expansion 0.{n}{n}{n}... (with special cases n = 9, 99 etc., where f = 0.999... = 1). This sequence lists the index of this fraction, for given n, corresponding to the \"canonical enumeration of positive fractions \u003c= 1\", i.e., m such that f = A038566(m) / A038567(m-1).",
				"Inspired by Angelini's blog post, which however takes a different approach to encoding fractions (in a more \"decimal\" way) and to deal with n's made of digits 9 or with repetitions, such as 11, 111, or 1010, etc."
			],
			"link": [
				"Eric Angelini, \u003ca href=\"http://cinquantesignes.blogspot.com/2021/10/cantorisation.html\"\u003eCantorisation\u003c/a\u003e, personal blog CinquanteSignes.blogspot.com, Oct 17 2021"
			],
			"formula": [
				"a(n) = m such that A038566(m)/A038567(m-1) = n/A002283(A055642(n)), where A002283(1, 2, 3, ...) = (9, 99, 999, ...) and A055642(n) = number of digits of n."
			],
			"example": [
				"a(n = 1) = 23 because A038566(23)/A038567(22) = 1/9, the unique fraction (in lowest terms) whose decimal expansion is 0.111..., i.e., period = (1), repeated.",
				"a(n = 2) = 24 because A038566(24)/A038567(23) = 2/9, the unique fraction (in lowest terms) whose decimal expansion is 0.222..., i.e., period = (2), repeated.",
				"a(n = 3) = 3 because A038566(3)/A038567(2) = 1/3, the unique fraction (in lowest terms) whose decimal expansion is 0.333..., i.e., period = (3), repeated.",
				"a(n = 9) = 1 because A038566(1)/A038567(0) = 1/1, the unique fraction (in lowest terms) equal to 0.999..., i.e., period = (9), repeated.",
				"a(n = 10) = 2951 because A038566(2951)/A038567(2950) = 10/99, the unique fraction (in lowest terms) whose decimal expansion is 0.1010..., i.e., period = (10), repeated.",
				"a(11) = a(1) because the unique fraction that has decimal expansion 0.1111..., i.e., period (11) repeated, is 1/9, the same as for 0.111..., i.e., period (1), repeated."
			],
			"program": [
				"(PARI) apply( {A343634(n, d=10^(logint(n,10)+1)-1, g=gcd(n,d)) = A002088(d\\g-1) - sum(k=1, n\\=g, gcd(k, d) \u003e 1) + n}, [1..55])"
			],
			"xref": [
				"Cf. A038566/A038567, A002283, A055642, A002088."
			],
			"keyword": "nonn,base",
			"offset": "1,1",
			"author": "_M. F. Hasler_, Oct 18 2021",
			"references": 1,
			"revision": 6,
			"time": "2021-10-22T23:44:04-04:00",
			"created": "2021-10-22T23:44:04-04:00"
		}
	]
}