{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A327246",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 327246,
			"data": "0,11,21,22,23,24,25,26,27,28,29,210,31,211,32,212,33,213,34,214,35,215,36,216,37,217,38,218,39,219,310,311,312,313,314,315,316,317,318,319,320,321,322,323,324,325,326,327,328,329,330,331,332,333,334,335",
			"name": "a(1) = 0; build a(n+1) by concatenating 2 strings: first, the number of digits of a(n), second, the number of terms with same number of digits as a(n) up to a(n).",
			"comment": [
				"Empirical observations:",
				"The sequence is not sensitive to the initial value; sooner or later the sequence arrives at 21 and has the same trajectory thereafter.",
				"Certain numbers cannot appear more than once. An example is 210, since it signifies a(n) was the 10th instance of a 2-digit number. We cannot ever say that a 21-digit number has appeared 0 times. However, 211 may signify a(n) was the 11th instance of a 2-digit number, or the first instance of a 21-digit number. It is difficult to ascertain the structure of repetitions, when they occur, etc."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A327246/b327246.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Michael De Vlieger, \u003ca href=\"/A327246/a327246.png\"\u003eLog log scatterplot of a(n)\u003c/a\u003e for n = 1..2^20."
			],
			"example": [
				"a(1) = 0.",
				"a(2) = 11 since 0 is the first instance of a 1-digit number in the sequence,",
				"a(3) = 21 since 11 is the first instance of a 2-digit number,",
				"a(4) = 22 since 21 is the second instance of a 2-digit number, ...",
				"a(12) = 210 since 29 is the tenth instance of a 2-digit number,",
				"a(13) = 31 since 210 is the first instance of a 3-digit number, etc.",
				"a(1) can be any positive integer number between 0-9, the sequence will not have further change. The overall sequence is defined by the number of digits of a(1).",
				"For example if a(1) were 1000000000 or 9999999999, then we would have a(2)=101, a(3)=31, a(4)=21, a(5)=22, ... where a(1) has 10 digits so a(2) would start with 10 and ends with 1, etc."
			],
			"mathematica": [
				"iL[n_] := IntegerLength[n] + Boole[n==0]; a[1]=0; a[n_] := a[n] = Block[{t = iL@ a[n-1]}, FromDigits[ Join @@ IntegerDigits@ {t, Count[iL /@ Array[a, n - 1], t]}]]; Array[a, 56] (* _Giovanni Resta_, Sep 16 2019 *)",
				"(* Fast program memoizing the number of instances of a given integer length: *)",
				"c[_] = j = 0; c[0] = 1; {0}~Join~Reap[Do[(c[#]++; Set[k, #*10^IntegerLength[c[#]] + c[#]]) \u0026[Boole[j == 0] + IntegerLength[j]]; Sow[k]; j = k, {i, 10^4}]][[-1, -1]] (* _Michael De Vlieger_, Dec 23 2021 *)"
			],
			"program": [
				"(Matlab)",
				"length_of_sequence=2000;",
				"sequence=zeros(1,length_of_sequence); %allocation for the sequence",
				"sequence(1)=11; %start from a(2)=11",
				"for i=1:1:(length_of_sequence-1)",
				"first=numel(num2str(sequence(1,i))); %defining the first string",
				"second=0;",
				"for index = 1:size(sequence, 2)",
				"    if numel(num2str(sequence(index)))==first",
				"    second=second+1; %defining the second string",
				"    end",
				"end",
				"s = strcat(num2str(first),num2str(second));%building a(n+1)",
				"sequence(i+1)=str2double(s); %placing the value into a(n+1)",
				"end",
				"result_sequence=transpose(sequence);",
				"(PARI) lista(nn) = {my(va = vector(nn)); va[1] = 0; for (n=2, nn, my(nba = #Str(va[n-1])); my(vn = vector(n-1, k, va[k])); my(nbb = #select(x-\u003e(#Str(x)==nba), vn)); va[n] = eval(concat(Str(nba), Str(nbb)));); va;} \\\\ _Michel Marcus_, Sep 16 2019"
			],
			"keyword": "nonn,base",
			"offset": "1,2",
			"author": "_Bence Bernáth_, Sep 14 2019",
			"references": 2,
			"revision": 50,
			"time": "2021-12-26T01:36:36-05:00",
			"created": "2019-09-21T19:59:09-04:00"
		}
	]
}