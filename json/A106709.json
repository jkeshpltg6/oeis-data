{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A106709",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 106709,
			"data": "0,-2,-10,-46,-210,-958,-4370,-19934,-90930,-414782,-1892050,-8630686,-39369330,-179585278,-819187730,-3736768094,-17045465010,-77753788862,-354678014290,-1617882493726,-7380056440050,-33664517212798,-153562473183890,-700483331493854",
			"name": "Expansion of g.f. -2*x/(1 - 5*x + 2*x^2).",
			"comment": [
				"Let T(n,k) denote the k-th element of row n of Stern's triangle (see A337277). Then b(n) = Sum_k T(n,k)*T(n,k+1) gives the present sequence (without the signs). - _N. J. A. Sloane_, Nov 19 2020"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A106709/b106709.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Richard P. Stanley, \u003ca href=\"https://arxiv.org/abs/1901.04647\"\u003eSome Linear Recurrences Motivated by Stern's Diatomic Array\u003c/a\u003e, arXiv:1901.04647 [math.CO], 2019. Also American Mathematical Monthly 127.2 (2020): 99-111.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (5,-2)."
			],
			"formula": [
				"a(n) = -2*A107839(n-1), n\u003e0.",
				"a(n) = first entry of v(n), where v(n) = M*v(n-1), M is the 2 X 2 matrix ({0, -2}, {1, 5}) and v(0) is the column vector (0, 1).",
				"G.f.: -2*x/(1-5*x+2*x^2). - _Alois P. Heinz_, Nov 26 2020",
				"a(n) = -sqrt(2)^(n+1)*ChebyshevU(n-1, 5/(2*sqrt(2))). - _G. C. Greubel_, Sep 10 2021"
			],
			"maple": [
				"a:= n-\u003e (\u003c\u003c0|-2\u003e, \u003c1|5\u003e\u003e^n)[1,2]:",
				"seq(a(n), n=0..25);  # _Alois P. Heinz_, Nov 19 2020"
			],
			"mathematica": [
				"LinearRecurrence[{5,-2}, {0,-2}, 41] (* _G. C. Greubel_, Sep 10 2021 *)"
			],
			"program": [
				"(MAGMA) I:=[0,-2]; [n le 2 select I[n] else 5*Self(n-1) - 2*Self(n-2): n in [1..41]]; // _G. C. Greubel_, Sep 10 2021",
				"(Sage) [-round(sqrt(2)^(n+1)*chebyshev_U(n-1, 5/(2*sqrt(2)))) for n in (0..40)] # _G. C. Greubel_, Sep 10 2021"
			],
			"xref": [
				"Cf. A107839, A337277."
			],
			"keyword": "sign,easy,less",
			"offset": "0,2",
			"author": "_Roger L. Bagula_, May 30 2005",
			"ext": [
				"Edited by _N. J. A. Sloane_, Apr 30 2006",
				"New name by _G. C. Greubel_, Sep 10 2021"
			],
			"references": 3,
			"revision": 25,
			"time": "2021-09-10T02:06:54-04:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}