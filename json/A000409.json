{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000409",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 409,
			"id": "M4306 N1801",
			"data": "0,6,350,43260,14591171,14657461469,46173502811223,474928141312623525,16489412944755088235117,1985178211854071817861662307,846428472480689964807653763864449,1299141117072945982773752362381072143359,7268140170419155675761326840423792818571154945,149650282980396792665043455999899697765782372693740287",
			"name": "Singular n X n (0,1)-matrices: the number of n X n (0,1)-matrices having distinct, nonzero ordered rows, but having at least two equal columns or at least one zero column.",
			"comment": [
				"This is a lower bound for the set of all n X n (0,1)-matrices having distinct, nonzero ordered rows and determinant 0 (compare A000410).",
				"Here ordered means that we take only one representative from the n! matrices obtained by all permutations of the distinct rows of an n X n matrix.",
				"a(n) is also the number of sets of n distinct nonzero (0,1)-vectors in R^n that do not span R^n."
			],
			"reference": [
				"G. Kilibarda and V. Jovovic, \"Enumeration of some classes of T_0-hypergraphs\", in preparation, 2004.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"J. Kahn, J. Komlos and E. Szemeredi, \u003ca href=\"https://doi.org/10.1090/S0894-0347-1995-1260107-2\"\u003eOn the probability that a random ±1-matrix is singular\u003c/a\u003e, J. AMS 8 (1995), 223-240.",
				"J. Komlos, \u003ca href=\"http://real-j.mtak.hu/5453/\"\u003eOn the determinant of (0,1)-matrices\u003c/a\u003e, Studia Math. Hungarica 2 (1967), 7-21.",
				"N. Metropolis and P. R. Stein, \u003ca href=\"https://doi.org/10.1016/S0021-9800(67)80006-1\"\u003eOn a class of (0,1) matrices with vanishing determinants\u003c/a\u003e, J. Combin. Theory, 3 (1967), 191-198.",
				"\u003ca href=\"/index/Mat#binmat\"\u003eIndex entries for sequences related to binary matrices\u003c/a\u003e"
			],
			"formula": [
				"a(n) = (-1)*Sum_{k=0..n-1} Stirling1(n+1, k+1)*binomial(2^k-1, n).",
				"a(n) = binomial(2^n-1, n) - A094000(n). - _Vladeta Jovovic_, Nov 27 2005"
			],
			"maple": [
				"with(combinat): T := proc(n) -sum(stirling1(n+1,k+1)*binomial(2^k-1,n),k=0..n-1); end proc:"
			],
			"mathematica": [
				"a[n_] := -Sum[ StirlingS1[n+1, k+1]*Binomial[2^k-1, n], {k, 0, n-1}]; Table[a[n], {n, 2, 15}] (* _Jean-François Alcover_, Nov 21 2012, from formula *)"
			],
			"program": [
				"(PARI) a(n) = -sum(k=0, n-1, stirling(n+1, k+1, 1)*binomial(2^k-1, n)); \\\\ _Michel Marcus_, Jun 05 2020",
				"(MAGMA) [ -(\u0026+[StirlingFirst(n+1,k+1)*Binomial(2^k-1,n): k in [0..n-1]]): n in [2..15]]; // _G. C. Greubel_, Jun 05 2020",
				"(Sage) [sum((-1)^(n+k+1)*stirling_number1(n+1,k+1)*binomial(2^k-1,n) for k in (0..n-1)) for n in (2..15)] # _G. C. Greubel_, Jun 05 2020"
			],
			"xref": [
				"Cf. A000410, A002884, A046747."
			],
			"keyword": "nonn,nice",
			"offset": "2,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Edited by _W. Edwin Clark_, Nov 02 2003"
			],
			"references": 7,
			"revision": 30,
			"time": "2020-06-05T15:05:26-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}