{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A262125",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 262125,
			"data": "1,1,0,0,1,0,0,2,1,0,0,5,3,1,0,0,16,24,4,1,0,0,61,101,57,5,1,0,0,272,862,311,123,6,1,0,0,1385,4743,3857,778,254,7,1,0,0,7936,47216,27589,14126,1835,514,8,1,0,0,50521,322039,355751,111811,47673,4189,1031,9,1,0",
			"name": "Number T(n,k) of permutations p of [n] such that the up-down signature of p has nonnegative partial sums with a maximal value of k; triangle T(n,k), n\u003e=0, 0\u003c=k\u003c=n, read by rows.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A262125/b262125.txt\"\u003eRows n = 0..100, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = A262124(n,k) - A262124(n,k-1) for k\u003e0, T(n,0) = A262124(n,0)."
			],
			"example": [
				"T(4,1) = 5: 1324, 1423, 2314, 2413, 3412.",
				"T(4,2) = 3: 1243, 1342, 2341.",
				"T(4,3) = 1: 1234.",
				"Triangle T(n,k) begins:",
				"  1;",
				"  1,    0;",
				"  0,    1,    0;",
				"  0,    2,    1,    0;",
				"  0,    5,    3,    1,   0;",
				"  0,   16,   24,    4,   1,   0;",
				"  0,   61,  101,   57,   5,   1, 0;",
				"  0,  272,  862,  311, 123,   6, 1, 0;",
				"  0, 1385, 4743, 3857, 778, 254, 7, 1, 0;"
			],
			"maple": [
				"b:= proc(u, o, c) option remember; `if`(c\u003c0, 0, `if`(u+o=0, x^c,",
				"      (p-\u003e add(coeff(p, x, i)*x^max(i, c), i=0..degree(p)))(add(",
				"       b(u-j, o-1+j, c-1), j=1..u)+add(b(u+j-1, o-j, c+1), j=1..o))))",
				"    end:",
				"T:= n-\u003e `if`(n=0, 1, (p-\u003e seq(coeff(p, x, i), i=0..n)",
				"             )(add(b(j-1, n-j, 0), j=1..n))):",
				"seq(T(n), n=0..10);"
			],
			"mathematica": [
				"b[u_, o_, c_] := b[u, o, c] = If[c\u003c0, 0, If[u+o==0, x^c, Sum[Coefficient[ #, x, i]*x^Max[i, c], {i, 0, Exponent[#, x]}]]\u0026 @ Sum[b[u-j, o-1+j, c-1], {j, 1, u}] + Sum[b[u+j-1, o-j, c+1], {j, 1, o}]];",
				"T[n_] := If[n==0, {1}, Table[Coefficient[#, x, i], {i, 0, n}]]\u0026 @ Sum[b[j-1, n-j, 0], {j, 1, n}];",
				"T /@ Range[0, 10] // Flatten (* _Jean-François Alcover_, Jan 19 2020, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Columns k=1-10 give: A000111 (for n\u003e1), A320976, A320977, A320978, A320979, A320980, A320981, A320982, A320983, A320984.",
				"Row sums give A000246.",
				"T(2n,n) gives A262127.",
				"Cf. A258829, A262124."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Alois P. Heinz_, Sep 11 2015",
			"references": 15,
			"revision": 18,
			"time": "2020-01-19T07:29:24-05:00",
			"created": "2015-09-11T18:46:51-04:00"
		}
	]
}