{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A309494",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 309494,
			"data": "1,1,1,2,1,2,3,5,8,8,7,5,2,5,13,12,18,3,5,6,4,23,21,9,5,2,5,26,14,31,3,5,6,4,36,34,9,5,2,5,39,14,44,3,5,6,4,49,47,9,5,2,5,52,14,57,3,5,6,4,62,60,9,5,2,5,65,14,70,3,5,6,4,75,73,9,5,2,5,78,14,83,3,5,6,4,88,86,9,5,2,5,91",
			"name": "a(1) = a(2) = a(3) = a(5) = 1, a(4) = 2; a(n) = a(n-a(n-3)) + a(n-a(n-4)) for n \u003e 5.",
			"comment": [
				"A well-defined solution sequence for recurrence a(n) = a(n-a(n-3)) + a(n-a(n-4))."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A309494/b309494.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_26\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,-1)."
			],
			"formula": [
				"For k \u003e 1,",
				"  a(13*k-9) = 13*k-8,",
				"  a(13*k-8) = 3,",
				"  a(13*k-7) = 5,",
				"  a(13*k-6) = 6,",
				"  a(13*k-5) = 4,",
				"  a(13*k-4) = 13*k-3,",
				"  a(13*k-3) = 13*k-5,",
				"  a(13*k-2) = 9,",
				"  a(13*k-1) = 5,",
				"  a(13*k)   = 2,",
				"  a(13*k+1) = 5,",
				"  a(13*k+2) = 13*k,",
				"  a(13*k+3) = 14.",
				"From _Colin Barker_, Aug 05 2019: (Start)",
				"G.f.: x*(1 + x + x^2 + 2*x^3 + x^4 + 2*x^5 + 3*x^6 + 5*x^7 + 8*x^8 + 8*x^9 + 7*x^10 + 5*x^11 + 2*x^12 + 3*x^13 + 11*x^14 + 10*x^15 + 14*x^16 + x^17 + x^18 - 6*x^20 + 7*x^21 + 5*x^22 - 5*x^23 - 5*x^24 - 2*x^25 - 4*x^26 + x^27 - 9*x^28 - 3*x^29 - 2*x^30 - 3*x^31 - 3*x^32 + x^33 - 2*x^34 - 2*x^36 - 2*x^41) / ((1 - x)^2*(1 + x + x^2 + x^3 + x^4 + x^5 + x^6 + x^7 + x^8 + x^9 + x^10 + x^11 + x^12)^2).",
				"a(n) = 2*a(n-13) - a(n-26) for n \u003e 42.",
				"(End)"
			],
			"maple": [
				"for n from 1 to 5 do a[n]:= `if`(n=4,2,1) od:",
				"for n from 6 to 100 do a[n]:= a[n-a[n-3]] + a[n-a[n-4]] od:",
				"seq(a[n],n=1..100); # _Robert Israel_, Aug 07 2019"
			],
			"mathematica": [
				"a[1]=a[2]=a[3]=a[5]=1; a[4]=2; a[n_] := a[n] = a[n - a[n-3]] + a[n - a[n-4]]; Array[a, 93] (* _Giovanni Resta_, Aug 07 2019 *)"
			],
			"program": [
				"(PARI) q=vector(100); q[1]=q[2]=q[3]=q[5]=1; q[4]=2; for(n=6, #q, q[n]=q[n-q[n-3]]+q[n-q[n-4]]); q",
				"(PARI) Vec(x*(1 + x + x^2 + 2*x^3 + x^4 + 2*x^5 + 3*x^6 + 5*x^7 + 8*x^8 + 8*x^9 + 7*x^10 + 5*x^11 + 2*x^12 + 3*x^13 + 11*x^14 + 10*x^15 + 14*x^16 + x^17 + x^18 - 6*x^20 + 7*x^21 + 5*x^22 - 5*x^23 - 5*x^24 - 2*x^25 - 4*x^26 + x^27 - 9*x^28 - 3*x^29 - 2*x^30 - 3*x^31 - 3*x^32 + x^33 - 2*x^34 - 2*x^36 - 2*x^41) / ((1 - x)^2*(1 + x + x^2 + x^3 + x^4 + x^5 + x^6 + x^7 + x^8 + x^9 + x^10 + x^11 + x^12)^2) + O(x^80)) \\\\ _Colin Barker_, Aug 08 2019"
			],
			"xref": [
				"Cf. A064657, A244477, A309492."
			],
			"keyword": "nonn,easy",
			"offset": "1,4",
			"author": "_Altug Alkan_, Aug 04 2019",
			"references": 5,
			"revision": 33,
			"time": "2019-08-08T07:41:59-04:00",
			"created": "2019-08-07T05:44:50-04:00"
		}
	]
}