{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A074764",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 74764,
			"data": "1,4,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74",
			"name": "Numbers of smaller squares into which a square may be dissected.",
			"comment": [
				"All even n\u003e2 are present by generalizing this corner+border construction, all odd n\u003e5 are present because n+3 can be obtained from n by splitting any single square into four, 1 is trivially present and n=2, 3 \u0026 5 are then fairly easily eliminated.",
				"Also number of smaller similar triangles into which a triangle may be dissected. - _Lekraj Beedassy_, Nov 25 2003",
				"Also positive integers k such that there exist k integers x_1, x_2, ..., x_k, distinct or not, satisfying 1 = 1/(x_1)^2 + 1/(x_2)^2 + ... + 1/(x_k)^2. For example, the unique solution for k = 4 is 1 = 1/2^2 + 1/2^2 + 1/2^2 + 1/2^2 (see Hassan Tarfaoui link, Concours Général 1990). - _Bernard Schott_, Oct 05 2021"
			],
			"reference": [
				"A. Soifer, How Does One Cut A Triangle?, Chapter 2, CEME, Colorado Springs CO 1990.",
				"Allan C. Wechsler and Michael Kleber, messages to math-fun mailing list, Sep 06, 2002."
			],
			"link": [
				"Mr. Glaeser, \u003ca href=\"http://www.lepetitarchimede.fr/pa/PA00p6-7+.jpg\"\u003eCarrés\u003c/a\u003e, Le Petit Archimède, no. 0, January 1973.",
				"Murray Klamkin, \u003ca href=\"/A074764/a074764.pdf\"\u003eReview of \"How Does One Cut a Triangle?\" by Alexander Soifer\u003c/a\u003e, Amer. Math. Monthly, October 1991, pp. 775-. [Annotated scanned copy of pages 775-777 only] See \"Grand Problem 2\".",
				"Miklós Laczkovich, \u003ca href=\"https://doi.org/10.1007/BF02122782\"\u003eTilings of polygons with similar triangles\u003c/a\u003e, Combinatorica 10.3 (1990): 281-306.",
				"Miklós Laczkovich. \u003ca href=\"https://doi.org/10.1016/0012-365X(93)E0176-5\"\u003eTilings of triangles\u003c/a\u003e Discrete mathematics 140.1 (1995): 79-94.",
				"Miklós Laczkovich, \u003ca href=\"https://doi.org/10.1007/PL00009359\"\u003eTilings of polygons with similar triangles, II\u003c/a\u003e, Discrete \u0026 Computational Geometry 19.3 (1998): 411-425.",
				"Alexander Soifer, \u003ca href=\"https://doi.org/10.1007/978-0-387-74652-4\"\u003eHow Does One Cut a Triangle?\u003c/a\u003e, Chapter 2, Springer-Verlag New York, 2009.",
				"Hassan Tarfaoui, \u003ca href=\"http://d.tarfaoui.free.fr/cg/1990/EX3/exobis.pdf\"\u003eConcours Général 1990 - Exercice 3\u003c/a\u003e (in French).",
				"Andrzej Zak, \u003ca href=\"http://home.agh.edu.pl/~zakandrz/Publikacje/zak.pdf\"\u003eDissection of a triangle into similar triangles\u003c/a\u003e, Discrete \u0026 Computational Geometry 34.2 (2005): 295-312.",
				"\u003ca href=\"/index/O#Olympiads\"\u003eIndex to sequences related to Olympiads and other Mathematical competitions\u003c/a\u003e.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,-1)."
			],
			"formula": [
				"n # 2, 3 or 5.",
				"G.f. of characteristic function: x*(1 - x + x^3 - x^4 + x^5)/(1-x).",
				"G.f.: (1 + 2*x -x^2 - x^3)/(1 - x)^2. - _Georg Fischer_, Aug 17 2021"
			],
			"example": [
				"6 is a term of the sequence because:",
				"+---+---+---+",
				"|...|...|...|",
				"+---+---+---+",
				"|.......|...|",
				"|.......+---+",
				"|.......|...|",
				"+-------+---+"
			],
			"maple": [
				"gf:= x*(1 - x + x^3 - x^4 + x^5)/(1-x):",
				"select(t-\u003e coeftayl(gf, x=0, t)=1, [$1..100])[];  # _Alois P. Heinz_, Aug 17 2021"
			],
			"mathematica": [
				"CoefficientList[Series[(1 + 2*x -x^2 - x^3)/(1 - x)^2, {x, 0, 20}], x] (* _Georg Fischer_, Aug 17 2021 *)",
				"LinearRecurrence[{2,-1},{1,4,6,7},80] (* _Harvey P. Dale_, Oct 17 2021 *)"
			],
			"xref": [
				"Cf. A005792."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Marc LeBrun_, Sep 06 2002",
			"references": 2,
			"revision": 60,
			"time": "2021-10-17T13:54:15-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}