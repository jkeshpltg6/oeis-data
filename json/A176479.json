{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A176479",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 176479,
			"data": "1,2,9,44,225,1182,6321,34232,187137,1030490,5707449,31760676,177435297,994551222,5590402785,31500824304,177880832001,1006362234162,5703029112297,32367243171740,183945502869345,1046646207221582,5961966567317649,33995080211156904",
			"name": "a(n) = (n+1)*A001003(n).",
			"comment": [
				"Central coefficients T(2n,n) of the Riordan array ((1-x)/(1-2x), x(1-x)/(1-2x)), A105306.",
				"a(n) counts the bi-degree sequences of directed trees (i.e., digraphs whose underlying graph is a tree) with n edges. - _Nikos Apostolakis_, Dec 31 2016",
				"a(n) is also the number of Dyck paths having exactly n peaks in level 1 and n peaks in level 2 and no other peaks. a(2) = 9: /\\/\\//\\/\\\\, /\\//\\/\\\\/\\, //\\/\\\\/\\/\\, /\\/\\//\\\\//\\\\, /\\//\\\\/\\//\\\\, /\\//\\\\//\\\\/\\, //\\\\/\\/\\//\\\\, //\\\\/\\//\\\\/\\, //\\\\//\\\\/\\/\\. - _Alois P. Heinz_, Jun 20 2017"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A176479/b176479.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Sergi Elizalde, Rigoberto Flórez, and José Luis Ramírez, \u003ca href=\"https://doi.org/10.26493/1855-3974.2478.d1b\"\u003eEnumerating symmetric peaks in non-decreasing Dyck paths\u003c/a\u003e, Ars Mathematica Contemporanea (2021).",
				"Milan Janjić, \u003ca href=\"https://arxiv.org/abs/1905.04465\"\u003eOn Restricted Ternary Words and Insets\u003c/a\u003e, arXiv:1905.04465 [math.CO], 2019.",
				"V. V. Kruchinin and D. V. Kruchinin, \u003ca href=\"http://arxiv.org/abs/1206.0877\"\u003eA Method for Obtaining Generating Function for Central Coefficients of Triangles\u003c/a\u003e, arXiv preprint arXiv:1206.0877 [math.CO], 2012, and \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL15/Kruchinin/kruchinin5.html\"\u003eJ. Int. Seq. 15 (2012) #12.9.3\u003c/a\u003e"
			],
			"formula": [
				"E.g.f.: 1+exp(3*x)*Bessel_I(1,2*sqrt(2)*x)/sqrt(2) +int(exp(3*x) *Bessel_I(1,2*sqrt(2)*x) /(sqrt(2)*x),x).",
				"G.f.: 1/4 - (x-3)/(4*sqrt(x^2-6*x+1)). - _Dmitry Kruchinin_, Aug 31 2012",
				"Conjecture: n*(n-1)*a(n) -3*(2*n-1)*(n-1)*a(n-1) +n*(n-2)*a(n-2) = 0. - _R. J. Mathar_, Dec 03 2014",
				"a(n) = Sum_{k=0..n} binomial(n-1,n-k) * binomial(n+k,n). - _Nikos Apostolakis_, Dec 31 2016",
				"a(n) = (n+1)*hypergeom([1-n, -n], [2], 2). - _Peter Luschny_, Jan 02 2017"
			],
			"maple": [
				"a:= proc(n) option remember; `if`(n\u003c2, n+1,",
				"     (6*n-3)/n*a(n-1) -(n-2)/(n-1)*a(n-2))",
				"    end:",
				"seq(a(n), n=0..25);  # _Alois P. Heinz_, Jun 22 2017"
			],
			"mathematica": [
				"a[n_] := Sum[Binomial[n - 1, k - 1]*Binomial[n + k, n], {k, 0, n}]; Array[a, 25, 0] (* or *)",
				"CoefficientList[ Series[1/4 - (x - 3)/(4 Sqrt[x^2 - 6x +1]), {x, 0, 25}], x] (* _Robert G. Wilson v_, Dec 31 2016 *)",
				"Table[(n+1)Hypergeometric2F1[1-n, -n, 2, 2], {n,0,21}] (* _Peter Luschny_, Jan 02 2017 *)"
			],
			"xref": [
				"Cf. A001003, A105306.",
				"Row n=2 of A288972."
			],
			"keyword": "easy,nonn",
			"offset": "0,2",
			"author": "_Paul Barry_, Apr 18 2010",
			"references": 4,
			"revision": 62,
			"time": "2021-07-14T11:37:21-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}