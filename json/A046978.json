{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A046978",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 46978,
			"data": "0,1,1,1,0,-1,-1,-1,0,1,1,1,0,-1,-1,-1,0,1,1,1,0,-1,-1,-1,0,1,1,1,0,-1,-1,-1,0,1,1,1,0,-1,-1,-1,0,1,1,1,0,-1,-1,-1,0,1,1,1,0,-1,-1,-1,0,1,1,1,0,-1,-1,-1,0,1,1,1,0,-1,-1,-1,0,1,1,1,0,-1,-1,-1,0,1,1,1,0,-1,-1,-1,0,1,1,1,0,-1,-1,-1,0,1,1,1,0,-1,-1,-1,0",
			"name": "Numerators of Taylor series for exp(x)*sin(x).",
			"comment": [
				"Period 8: repeat [0, 1, 1, 1, 0, -1, -1, -1].",
				"Lehmer sequence U_n for R=2, Q=1. - _Artur Jasinski_, Oct 06 2008",
				"4*a(n+6) = period 8: repeat -4,-4,0,4,4,4,0,-4 = A189442(n+1) + A189442(n+5). - _Paul Curtz_, Jun 03 2011",
				"This is a strong elliptic divisibility sequence t_n as given in [Kimberling, p. 16] where x = 1, y = 1, z = 0. - _Michael Somos_, Nov 27 2019"
			],
			"reference": [
				"G. W. Caunt, Infinitesimal Calculus, Oxford Univ. Press, 1914, p. 477."
			],
			"link": [
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,0,0,-1).",
				"C. Kimberling, \u003ca href=\"http://www.fq.math.ca/Scanned/17-1/kimberling1.pdf\"\u003eStrong divisibility sequences and some conjectures\u003c/a\u003e, Fib. Quart., 17 (1979), 13-17."
			],
			"formula": [
				"Euler transform of length 8 sequence [1, 0, -1, -1, 0, 0, 0, 1]. - _Michael Somos_, Jul 16 2006",
				"G.f.: x * (1 + x + x^2) / (1 + x^4) = x * (1 - x^3) * (1 - x^4) / ((1 - x) * (1 - x^8)). a(-n) = a(n + 4) = -a(n). - _Michael Somos_, Jul 16 2006",
				"a(n) = round((b^n - c^n)/(b - c)) where b = sqrt(2)-((1+i)/sqrt(2)), c = (1+i)/sqrt(2). - _Artur Jasinski_, Oct 06 2008",
				"a(n) = sign(cos(Pi*(n-2)/4)). - _Wesley Ivan Hurt_, Oct 02 2013"
			],
			"example": [
				"G.f. = x + x^2 + x^3 - x^5 - x^6 - x^7 + x^9 + x^10 + x^11 - x^13 - x^14 - ...",
				"1*x + 1*x^2 + (1/3)*x^3 - (1/30)*x^5 - (1/90)*x^6 - (1/630)*x^7 + (1/22680)*x^9 + (1/113400)*x^10 + ..."
			],
			"maple": [
				"A046978 := n -\u003e `if`(n mod 4 = 0,0,(-1)^iquo(n,4)): # _Peter Luschny_, Aug 21 2011"
			],
			"mathematica": [
				"a = -((1 + I)/Sqrt[2]) + Sqrt[2]; b = (1 + I)/Sqrt[2]; Table[ Round[(a^n - b^n)/(a - b)], {n, 0, 200}] (* _Artur Jasinski_, Oct 06 2008 *)",
				"Table[Sign[Cos[Pi*(n-2)/4]],{n,0,100}] (* _Wesley Ivan Hurt_, Oct 10 2013 *)",
				"LinearRecurrence[{0,0,0,-1},{0,1,1,1},120] (* or *) PadRight[{},120,{0,1,1,1,0,-1,-1,-1}] (* _Harvey P. Dale_, Mar 17 2017 *)"
			],
			"program": [
				"(PARI) {a(n) = (n%4 \u003e 0) * (-1)^(n\\4)}; /* _Michael Somos_, Jul 16 2006 */"
			],
			"xref": [
				"Cf. A046979."
			],
			"keyword": "sign,frac,easy",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_",
			"references": 9,
			"revision": 44,
			"time": "2020-03-07T07:53:42-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}