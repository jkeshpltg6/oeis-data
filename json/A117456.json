{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A117456",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 117456,
			"data": "1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,1,1,1,1,1,1,2,1,1,1,1,1,1,2,2,1,1,1,1,1,2,2,2,2,1,1,1,1,1,1,2,3,2,2,1,1,1,1,1,1,2,3,3,2,2,1,1,1,1,1,2,2,3,4,3,2,2,1,1,1,1,1,1,2,3,4,4,3,2,2,1,1,1,1,1,1,2,3,4,5,4,3,2,2,1,1,1",
			"name": "Triangle read by rows: T(n,k) is the number of partitions of n in which every integer from the smallest part to the largest part occurs and the number of parts is k (1\u003c=k\u003c=n).",
			"comment": [
				"Row sums yield A034296.",
				"Sum_{1..n} k * T(n,k) = A117457(n).",
				"T(2n,n) gives A000009(n). - _Alois P. Heinz_, Oct 09 2020"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A117456/b117456.txt\"\u003eRows n = 1..200, flattened\u003c/a\u003e"
			],
			"formula": [
				"G.f.: G(t,x) = Sum_{j\u003e=1} t^j*x^j/(1-x^j) * Product_{i=1..j-1} (1+x^i)."
			],
			"example": [
				"T(10,5) = 3 because we have [3,3,2,1,1], [3,2,2,2,1] and [2,2,2,2,2].",
				"Triangle starts:",
				"  1;",
				"  1,1;",
				"  1,1,1;",
				"  1,1,1,1;",
				"  1,1,1,1,1;",
				"  1,1,2,1,1,1;",
				"  1,1,1,2,1,1,1;",
				"  1,1,1,2,2,1,1,1;",
				"  ..."
			],
			"maple": [
				"g:=sum(t^j*x^j*product(1+x^i,i=1..j-1)/(1-x^j),j=1..60): gser:=simplify(series(g,x=0,55)): for n from 1 to 15 do P[n]:=coeff(gser,x^n) od: for n from 1 to 15 do seq(coeff(P[n],t,j),j=1..n) od; # yields sequence in triangular form",
				"# second Maple program:",
				"b:= proc(n, i) option remember; expand(`if`(n=0, 1,",
				"     `if`(i\u003c1, 0, add(x^j*b(n-i*j, i-1), j=1..n/i))))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=1..n))(add(b(n, k), k=0..n)):",
				"seq(T(n), n=1..14);  # _Alois P. Heinz_, Oct 09 2020"
			],
			"mathematica": [
				"b[n_, i_] := b[n, i] = Expand[If[n == 0, 1, If[i \u003c 1, 0, Sum[x^j b[n - i j, i - 1], {j, 1, n/i}]]]];",
				"T[n_] := Function[p, Table[Coefficient[p, x, i], {i, 1, n}]][Sum[b[n, k], {k, 0, n}]];",
				"Array[T, 14] // Flatten (* _Jean-François Alcover_, Nov 23 2020, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A000009, A034296, A117457."
			],
			"keyword": "nonn,tabl",
			"offset": "1,18",
			"author": "_Emeric Deutsch_, Mar 18 2006",
			"references": 2,
			"revision": 12,
			"time": "2020-11-23T08:03:53-05:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}