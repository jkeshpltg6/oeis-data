{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097769",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97769,
			"data": "1,579,334661,193433479,111804216201,64622643530699,37351776156527821,21589261995829549839,12478556081813323279121,7212583826026105025782099,4168860972887006891578774101,2409594429744863957227505648279",
			"name": "Pell equation solutions (12*a(n))^2 - 145*b(n)^2 = -1 with b(n):=A097770(n), n \u003e= 0.",
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A097769/b097769.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"Giovanni Lucca, \u003ca href=\"http://forumgeom.fau.edu/FG2019volume19/FG201902index.html\"\u003eInteger Sequences and Circle Chains Inside a Hyperbola\u003c/a\u003e, Forum Geometricorum (2019) Vol. 19, 11-16.",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (578,-1)."
			],
			"formula": [
				"G.f.: (1 + x)/(1 - 2*289*x + x^2).",
				"a(n) = S(n, 2*289) + S(n-1, 2*289) = S(2*n, 2*sqrt(145)), with Chebyshev polynomials of the 2nd kind. See A049310 for the triangle of S(n, x)= U(n, x/2) coefficients. S(-1, x) := 0 =: U(-1, x).",
				"a(n) = ((-1)^n)*T(2*n+1, 12*i)/(12*i) with the imaginary unit i and Chebyshev polynomials of the first kind. See the T-triangle A053120.",
				"a(n) = 578*a(n-1) - a(n-2), n \u003e 1; a(0)=1, a(1)=579. - _Philippe Deléham_, Nov 18 2008",
				"a(n) = (1/12)*sinh((2*n + 1)*arcsinh(12)). - _Bruno Berselli_, Apr 05 2018"
			],
			"example": [
				"(x,y) = (12*1=12;1), (6948=12*579;577), (4015932=12*334661;333505), ... give the positive integer solutions to x^2 - 145*y^2 = -1."
			],
			"mathematica": [
				"LinearRecurrence[{578, -1}, {1, 579}, 20] (* or *) CoefficientList[Series[(1 + x)/(1 - 578 x + x^2), {x, 0, 20}], x] (* _Harvey P. Dale_, May 15 2011 *)"
			],
			"program": [
				"(MAGMA) I:=[1, 579]; [n le 2 select I[n] else 578*Self(n-1)-Self(n-2): n in [1..15]]; // _Vincenzo Librandi_, May 20 2012",
				"(PARI) x='x+O('x^99); Vec((1+x)/(1-2*289*x+x^2)) \\\\ _Altug Alkan_, Apr 05 2018"
			],
			"xref": [
				"Cf. A097768 for S(n, 2*289).",
				"Cf. similar sequences of the type (1/k)*sinh((2*n+1)*arcsinh(k)) listed in A097775."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Aug 31 2004",
			"references": 4,
			"revision": 37,
			"time": "2020-01-23T00:15:12-05:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}