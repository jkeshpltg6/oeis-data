{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A285282",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 285282,
			"data": "1,2,3,5,7,8,18,57,239",
			"name": "Numbers n such that n^2 + 1 is 13-smooth.",
			"comment": [
				"Equivalently: Numbers n such that all prime factors of n^2 + 1 are \u003c= 13.",
				"Since an odd prime factor of n^2 + 1 must be of the form 4m + 1, n^2 + 1 must be of the form 2*5^a*13^b.",
				"This sequence is complete by a theorem of Størmer.",
				"The largest instance 239^2 + 1 = 2*13^4 also gives the only nontrivial solution for x^2 + 1 = 2y^4 (Ljunggren)."
			],
			"reference": [
				"W. Ljunggren, Zur Theorie der Gleichung x^2 + 1 = 2y^4, Avh. Norsk Vid. Akad. Oslo. 1(5) (1942), 1--27."
			],
			"link": [
				"A. Schinzel, \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/aa/aa13/aa13113.pdf\"\u003eOn two theorems of Gelfond and some of their applications\u003c/a\u003e, Acta Arithmetica 13 (1967-1968), 177--236.",
				"Ray Steiner, \u003ca href=\"https://doi.org/10.1016/S0022-314X(05)80029-0\"\u003eSimplifying the Solution of Ljunggren's Equation X^2 + 1 = 2Y^4\u003c/a\u003e, J. Number Theory 37 (1991), 123--132, more accesible proof of Ljunggren's result.",
				"Carl Størmer, \u003ca href=\"http://www.archive.org/stream/skrifterudgivnea1897chri#page/n79/mode/2up\"\u003eQuelques théorèmes sur l'équation de Pell x^2 - Dy^2 = +-1 et leurs applications\u003c/a\u003e (in French), Skrifter Videnskabs-selskabet (Christiania), Mat.-Naturv. Kl. I Nr. 2 (1897), 48 pp."
			],
			"example": [
				"For n = 8, a(8)^2 + 1 = 57^2 + 1 = 3250 = 2*5^3*13."
			],
			"mathematica": [
				"Select[Range[1000], FactorInteger[#^2 + 1][[-1, 1]] \u003c= 13\u0026] (* _Jean-François Alcover_, May 17 2017 *)"
			],
			"program": [
				"(PARI) for(n=1, 9e6, if(vecmax(factor(n^2+1)[, 1])\u003c=13, print1(n\", \")))",
				"(Python)",
				"from sympy import primefactors",
				"def ok(n): return max(primefactors(n**2 + 1))\u003c=13 # _Indranil Ghosh_, Apr 16 2017"
			],
			"xref": [
				"Cf. A014442, A252493 (n(n+1) instead of n^2 + 1)."
			],
			"keyword": "nonn,fini,full",
			"offset": "1,2",
			"author": "_Tomohiro Yamada_, Apr 16 2017",
			"references": 2,
			"revision": 19,
			"time": "2017-05-17T05:36:15-04:00",
			"created": "2017-04-20T14:53:42-04:00"
		}
	]
}