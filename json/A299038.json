{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A299038",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 299038,
			"data": "1,1,1,1,1,0,1,1,1,0,1,1,1,1,0,1,1,1,2,1,0,1,1,1,2,3,1,0,1,1,1,2,4,6,1,0,1,1,1,2,4,8,11,1,0,1,1,1,2,4,9,17,23,1,0,1,1,1,2,4,9,19,39,46,1,0,1,1,1,2,4,9,20,45,89,98,1,0,1,1,1,2,4,9,20,47,106,211,207,1,0",
			"name": "Number A(n,k) of rooted trees with n nodes where each node has at most k children; square array A(n,k), n\u003e=0, k\u003e=0, read by antidiagonals.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A299038/b299038.txt\"\u003eAntidiagonals n = 0..140, flattened\u003c/a\u003e"
			],
			"formula": [
				"A(n,k) = Sum_{i=0..k} A244372(n,i) for n\u003e0, A(0,k) = 1."
			],
			"example": [
				"Square array A(n,k) begins:",
				"  1, 1,   1,   1,   1,   1,   1,   1,   1,   1,   1, ...",
				"  1, 1,   1,   1,   1,   1,   1,   1,   1,   1,   1, ...",
				"  0, 1,   1,   1,   1,   1,   1,   1,   1,   1,   1, ...",
				"  0, 1,   2,   2,   2,   2,   2,   2,   2,   2,   2, ...",
				"  0, 1,   3,   4,   4,   4,   4,   4,   4,   4,   4, ...",
				"  0, 1,   6,   8,   9,   9,   9,   9,   9,   9,   9, ...",
				"  0, 1,  11,  17,  19,  20,  20,  20,  20,  20,  20, ...",
				"  0, 1,  23,  39,  45,  47,  48,  48,  48,  48,  48, ...",
				"  0, 1,  46,  89, 106, 112, 114, 115, 115, 115, 115, ...",
				"  0, 1,  98, 211, 260, 277, 283, 285, 286, 286, 286, ...",
				"  0, 1, 207, 507, 643, 693, 710, 716, 718, 719, 719, ..."
			],
			"maple": [
				"b:= proc(n, i, t, k) option remember; `if`(n=0, 1,",
				"      `if`(i\u003c1, 0, add(binomial(b((i-1)$2, k$2)+j-1, j)*",
				"       b(n-i*j, i-1, t-j, k), j=0..min(t, n/i))))",
				"    end:",
				"A:= (n, k)-\u003e `if`(n=0, 1, b(n-1$2, k$2)):",
				"seq(seq(A(n, d-n), n=0..d), d=0..14);"
			],
			"mathematica": [
				"b[n_, i_, t_, k_] := b[n, i, t, k] = If[n == 0, 1, If[i\u003c1, 0, Sum[Binomial[ b[i-1, i-1, k, k]+j-1, j]*b[n-i*j, i-1, t-j, k], {j, 0, Min[t, n/i]}]]];",
				"A[n_, k_] := If[n == 0, 1, b[n - 1, n - 1, k, k]];",
				"Table[A[n, d-n], {d, 0, 14}, {n, 0, d}] // Flatten (* _Jean-François Alcover_, Jun 04 2018, from Maple *)"
			],
			"program": [
				"(Python)",
				"from sympy import binomial",
				"from sympy.core.cache import cacheit",
				"@cacheit",
				"def b(n, i, t, k): return 1 if n==0 else 0 if i\u003c1 else sum([binomial(b(i-1, i-1, k, k)+j-1, j)*b(n-i*j, i-1, t-j, k) for j in range(min(t, n//i)+1)])",
				"def A(n, k): return 1 if n==0 else b(n-1, n-1, k, k)",
				"for d in range(15): print([A(n, d-n) for n in range(d+1)]) # _Indranil Ghosh_, Mar 02 2018, after Maple code"
			],
			"xref": [
				"Columns k=1-11 give: A000012, A001190(n+1), A000598, A036718, A036721, A036722, A182378, A292553, A292554, A292555, A292556.",
				"Main diagonal gives A000081 for n\u003e0.",
				"A(2n,n) gives A299039.",
				"Cf. A244372."
			],
			"keyword": "nonn,tabl",
			"offset": "0,19",
			"author": "_Alois P. Heinz_, Feb 01 2018",
			"references": 14,
			"revision": 20,
			"time": "2018-06-04T04:28:55-04:00",
			"created": "2018-02-02T10:11:41-05:00"
		}
	]
}