{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A069734",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 69734,
			"data": "1,3,3,6,4,9,5,11,8,12,7,19,8,15,14,20,10,24,11,26,18,21,13,37,17,24,22,33,16,42,17,37,26,30,26,53,20,33,30,52,22,54,23,47,42,39,25,71,30,51,38,54,28,66,38,67,42,48,31,94,32,51,55,70,44,78,35,68,50,78,37,108",
			"name": "Number of pairs (p,q), 0\u003c=p\u003c=q, such that p+q divides n.",
			"comment": [
				"Also number of orientable coverings of the Klein bottle with 2n lists (orientable m-list coverings exist only for even m).",
				"Equals row sums of triangle A178650. - _Gary W. Adamson_, May 31 2010",
				"Also number of sublattices of index n of a 2D rectangular lattice fixed by the p2mm (pmm) Patterson symmetry operations of the parent lattice [Rutherford]. For other 2D Patterson groups, the analogous sequences are A000203 (p2), A145391 (c2mm), A145392 (p4), A145393 (p4mm), A145394 (p6), A003051 (p6mm). - _Andrey Zabolotskiy_, Mar 12 2018"
			],
			"link": [
				"Andrey Zabolotskiy, \u003ca href=\"/A069734/b069734.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"V. A. Liskovets and A. Mednykh, \u003ca href=\"https://www.researchgate.net/publication/251203042\"\u003eNumber of non-orientable coverings of the Klein bottle\u003c/a\u003e",
				"John S. Rutherford, \u003ca href=\"http://dx.doi.org/10.1107/S010876730804333X\"\u003eSublattice enumeration. IV. Equivalence classes of plane sublattices by parent Patterson symmetry and colour lattice group type\u003c/a\u003e, Acta Cryst. (2009). A65, 156-163. [See Table 2]. [From _N. J. A. Sloane_, Feb 23 2009]",
				"\u003ca href=\"/index/Su#sublatts\"\u003eIndex entries for sequences related to sublattices\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A046524(2n) - A069733(2n).",
				"Inverse Moebius transform of: 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, ... G.f.: Sum_{n\u003e0} x^n*(1+x^n-x^(2*n))/(1-x^(2*n))/(1-x^n). - _Vladeta Jovovic_, Feb 03 2003",
				"a(n) = (A000203(n) + A069735(n))/2. [Rutherford] - _N. J. A. Sloane_, Mar 13 2009",
				"a(n) = Sum_{ m: m^2|n } A304182(n/m^2) + A304183(n/m^2) = A069735(n) + Sum_{ m: m^2|n } A304183(n/m^2). - _Andrey Zabolotskiy_, May 07 2018",
				"a(n) = Sum_{ d|n } A008619(d) = Sum_{ d|n } (1 + floor(d/2)). - _Andrey Zabolotskiy_, Jul 20 2019",
				"a(n) = (A007503(n) + A183063(n))/2. - _Peter Luschny_, Jul 20 2019"
			],
			"example": [
				"There are 9 pairs (p,q), 0\u003c=p\u003c=q, such that p+q divides 6: (0,1), (0,2), (0,3), (0,6), (1,1), (1, 2), (1, 5), (2, 4), (3, 3); thus a(6) = 9.",
				"x + 3*x^2 + 3*x^3 + 6*x^4 + 4*x^5 + 9*x^6 + 5*x^7 + 11*x^8 + 8*x^9 + ..."
			],
			"maple": [
				"with(numtheory): a := n -\u003e (sigma(n) + tau(n) + `if`(irem(n,2) = 1, 0, tau(n/2)))/2: seq(a(n), n=1..72); # _Peter Luschny_, Jul 20 2019"
			],
			"mathematica": [
				"a[n_] := (DivisorSigma[1, n] + DivisorSigma[0, n] + If[OddQ[n], 0, DivisorSigma[0, n/2]])/2;",
				"Array[a, 72] (* _Jean-François Alcover_, Aug 27 2019, from Maple *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, 0, sum( k=1, n, sum( j=0, k, n%(j+k) == 0)))} /* _Michael Somos_, Mar 24 2012 */"
			],
			"xref": [
				"Cf. A069733, A069735, A046524, A178650.",
				"Cf. A000203, A145391-A145394, A003051.",
				"Cf. A304182, A304183, A008619, A007503, A183063."
			],
			"keyword": "easy,nonn",
			"offset": "1,2",
			"author": "_Valery A. Liskovets_, Apr 07 2002",
			"ext": [
				"New description from _Vladeta Jovovic_, Feb 03 2003"
			],
			"references": 13,
			"revision": 40,
			"time": "2019-08-27T10:58:12-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}