{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007502",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7502,
			"id": "M1170",
			"data": "1,2,4,9,17,33,61,112,202,361,639,1123,1961,3406,5888,10137,17389,29733,50693,86204,146246,247577,418299,705479,1187857,1997018,3352636,5621097,9412937,15744681,26307469,43912648",
			"name": "Les Marvin sequence: a(n) = F(n)+(n-1)*F(n-1), F() = Fibonacci numbers.",
			"comment": [
				"Denominators of convergents of the continued fraction with the n partial quotients: [1;1,1,...(n-1 1's)...,1,n], starting with [1], [1;2], [1;1,3], [1;1,1,4], ... Numerators are A088209(n-1). - _Paul D. Hanna_, Sep 23 2003"
			],
			"reference": [
				"Les Marvin, Problem, J. Rec. Math., Vol. 10 (No. 3, 1976-1977), p. 213.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A007502/b007502.txt\"\u003eTable of n, a(n) for n = 1..500\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,1,-2,-1)."
			],
			"formula": [
				"G.f.: (1-x^2+x^3)/(1-x-x^2)^2. - _Paul D. Hanna_, Sep 23 2003",
				"a(n+1) = A109754(n, n+1) = A101220(n, 0, n+1).  - _N. J. A. Sloane_, May 19 2006",
				"a(n) = 2*a(n-1) + a(n-2) - 2*a(n-3) - a(n-4) for n\u003e3, a(0)=1, a(1)=2, a(2)=4, a(3)=9. - Harvey P. Dale, Jul 13 2011"
			],
			"example": [
				"a(7) = F(7)+6*F(6) = 13+6*8 = 61."
			],
			"mathematica": [
				"Table[Fibonacci[n]+(n-1)*Fibonacci[n-1], {n,40}] (* or *) LinearRecurrence[ {2,1,-2,-1}, {1,2,4,9}, 40](* _Harvey P. Dale_, Jul 13 2011 *)",
				"f[n_] := Denominator@  FromContinuedFraction@ Join[ Table[1, {n}], {n + 1}]; Array[f, 30, 0] (* _Robert G. Wilson v_, Mar 04 2012 *)"
			],
			"program": [
				"(Haskell)",
				"a007502 n = a007502_list !! (n-1)",
				"a007502_list = zipWith (+) a045925_list $ tail a000045_list",
				"-- _Reinhard Zumkeller_, Oct 01 2012, Mar 04 2012",
				"(PARI) Vec((1-x^2+x^3)/(1-x-x^2)^2+O(x^99)) \\\\ _Charles R Greathouse IV_, Mar 04, 2012"
			],
			"xref": [
				"Cf. A088209 (numerators) , A000045, A109754, A101220.",
				"Cf. A045925."
			],
			"keyword": "nonn,nice,easy",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, _Robert G. Wilson v_",
			"references": 7,
			"revision": 41,
			"time": "2015-11-05T11:20:15-05:00",
			"created": "1994-09-19T03:00:00-04:00"
		}
	]
}