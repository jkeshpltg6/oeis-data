{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A301701",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 301701,
			"data": "3,1,4,10,12,17,16,19,20,22,22,23,24,25,25,25,24,26,26,28,27,27,29,28,28,29,29,30,28,29,30,30,30,30,30,31,31,31,31,31,31,31,32,33,33,33,32,33,32,33,32,33,33,36,35,33,33,36,34,34,37,35,34,37,35,34,34,35,35,35,35",
			"name": "a(n) is the smallest positive integer m, with the property that n appears as a coefficient in the polynomial P_m(x) = (x-1)(x^2-1)...(x^m-1).",
			"comment": [
				"We conjecture that all integers appear as a coefficient of a polynomial P_m(x).",
				"This property is known to hold for the cyclotomic polynomials.",
				"The conjecture holds for the first 10^5 positive integers, with a maximum on those integers of a(99852) = 1921. - _David A. Corneth_, Apr 08 2018"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A301701/b301701.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Dorin Andrica, Ovidiu Bagdasar, \u003ca href=\"http://hdl.handle.net/10545/623501\"\u003eOn some results concerning the polygonal polynomials\u003c/a\u003e, Carpathian Journal of Mathematics (2019) Vol. 35, No. 1, 1-11.",
				"Jiro Suzuki, \u003ca href=\"http://projecteuclid.org/euclid.pja/1195513653\"\u003eOn coefficients of cyclotomic polynomials\u003c/a\u003e, Proc. Japan Acad. Ser. A Math. Sci. 63:7 (1987), pp. 279-280."
			],
			"example": [
				"We have:",
				"P_1(x) = x-1, hence a(1)=1.",
				"P_2(x) = (x-1)*(x^2-1) = x^3-x^2-x+1;",
				"P_3(x) = (x-1)*(x^2-1)*(x^3-1) = x^6-x^5-x^4+x^2+x-1;",
				"P_4(x) = (x-1)*(x^2-1)*(x^3-1)*(x^4-1) = x^10 - x^9 - x^8+2x^5-x^2-x+1, hence a(2)=4.",
				"n=3 first appears as a coefficient of P_{10}(x)."
			],
			"maple": [
				"T:= proc(n) option remember; [(p-\u003e seq(coeff(p, x, i),",
				"    i=0..degree(p)))(expand(mul(1-x^i, i=1..n)))] end:",
				"a:= proc(n) local k; for k while not n in T(k) do od: k end:",
				"seq(a(n), n=0..70);  # _Alois P. Heinz_, Mar 29 2019"
			],
			"mathematica": [
				"With[{s = Array[CoefficientList[Times @@ Array[x^# - 1 \u0026, #], x] \u0026, 40]}, TakeWhile[Array[FirstPosition[s, #][[1]] \u0026, Max@ Map[Max, s]], IntegerQ]] (* _Michael De Vlieger_, Apr 05 2018 *)"
			],
			"program": [
				"(PARI) a(n) = {my(k=1); while (!vecsearch(vecsort(Vec(prod(j=1, k, x^j-1))), n), k++); k;} \\\\ _Michel Marcus_, Apr 08 2018",
				"(PARI) first(n) = {my(pol = [1], res = vector(n), todo = n+1, t = 0); while(1, t++; for(i = 1, #pol, if(0 \u003c pol[i] \u0026\u0026 pol[i] \u003c=n, if(res[pol[i]] == 0, res[pol[i]] = t-1; todo--; if(todo == 0, return(concat([3], res))))));  pol = concat(pol, vector(t)) - concat(vector(t), pol))} \\\\ _David A. Corneth_, Apr 08 2018"
			],
			"xref": [
				"Cf. A231599: a(n) is the index of the first row m containing number n."
			],
			"keyword": "nonn",
			"offset": "0,1",
			"author": "_Ovidiu Bagdasar_, Mar 25 2018",
			"ext": [
				"Offset changed to 0 by _David A. Corneth_, Apr 08 2018"
			],
			"references": 1,
			"revision": 38,
			"time": "2019-03-29T18:07:42-04:00",
			"created": "2018-04-16T11:13:38-04:00"
		}
	]
}