{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A060734",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 60734,
			"data": "1,4,2,9,3,5,16,8,6,10,25,15,7,11,17,36,24,14,12,18,26,49,35,23,13,19,27,37,64,48,34,22,20,28,38,50,81,63,47,33,21,29,39,51,65,100,80,62,46,32,30,40,52,66,82,121,99,79,61,45,31,41,53,67,83,101",
			"name": "Natural numbers written as a square array ending in last row from left to right and rightmost column from bottom to top are read by antidiagonals downwards.",
			"comment": [
				"A simple permutation of natural numbers.",
				"Parity of the sequence is given by A057211 (n-th run has length n). - _Jeremy Gardiner_, Dec 26 2008",
				"The square with corners T(1,1)=1 and T(n,n)=n^2-n+1 is occupied by the numbers 1,2,...,n^2. - _Clark Kimberling_, Feb 01 2011",
				"a(n) is pairing function - function that reversibly maps Z^{+} x Z^{+} onto Z^{+}, where Z^{+} - the set of integer positive numbers. - _Boris Putievskiy_, Dec 17 2012"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A060734/b060734.txt\"\u003eRows n = 1..141 of triangle, flattened\u003c/a\u003e",
				"Boris Putievskiy, \u003ca href=\"http://arxiv.org/abs/1212.2732\"\u003eTransformations Integer Sequences And Pairing Functions\u003c/a\u003e arXiv:1212.2732 [math.CO], 2012.",
				"Eric W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/PairingFunction.html\"\u003eMathWorld: Pairing functions\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = (n-1)^2+k, T(k, n)=n^2+1-k, 1 \u003c= k \u003c= n.",
				"From _Clark Kimberling_, Feb 01 2011: (Start)",
				"T(1,k) = k^2 (A000290).",
				"T(n,n) = n^2-n+1 (A002061).",
				"T(n,1) = (n-1)^2+1 (A002522). (End)"
			],
			"example": [
				"Northwest corner:",
				".1  4  9 16 ..  =\u003e a(1) =  1",
				".2  3  8 15 ..  =\u003e a(2) =  4, a(3) = 2",
				".5  6  7 14 ..  =\u003e a(4) =  9, a(5) = 3, a(6) = 5",
				"10 11 12 13 ..  =\u003e a(7) = 16, a(8) = 8, a(9) = 6, a(10)=10"
			],
			"maple": [
				"T:= (n,k)-\u003e `if`(n\u003c=k, k^2-n+1, (n-1)^2+k):",
				"seq(seq(T(n, d-n), n=1..d-1), d=2..15);"
			],
			"mathematica": [
				"f[n_, k_]:=k^2-n+1/; k\u003e=n;",
				"f[n_, k_]:=(n-1)^2+k/; k\u003cn;",
				"TableForm[Table[f[n, k], {n, 1, 10}, {k, 1, 15}]]",
				"Table[f[n-k+1, k], {n, 14}, {k, n, 1, -1}]//Flatten (* _Clark Kimberling_, Feb 01 2011 *)"
			],
			"xref": [
				"Cf. A060736. Inverse: A064790.",
				"Cf. A185725, A185726, A185728."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Frank Ellermann_, Apr 23 2001",
			"ext": [
				"Corrected by _Jeremy Gardiner_, Dec 26 2008"
			],
			"references": 15,
			"revision": 32,
			"time": "2020-02-28T05:33:14-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}