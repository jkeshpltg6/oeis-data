{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A104404",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 104404,
			"data": "1,1,1,2,1,1,1,4,2,1,1,2,1,1,1,6,1,2,1,2,1,1,1,4,2,1,3,2,1,1,1,8,1,1,1,4,1,1,1,4,1,1,1,2,2,1,1,6,2,2,1,2,1,3,1,4,1,1,1,2,1,1,2,12,1,1,1,2,1,1,1,8,1,1,2,2,1,1,1,6,5,1,1,2,1,1,1,4,1,2,1,2,1,1,1,8,1,2,2,4,1,1",
			"name": "Number of groups of order n all of whose subgroups are normal.",
			"comment": [
				"A finite non-Abelian group has all of its subgroups normal precisely when it is the direct product of the quaternion group of order 8, a (possibly trivial) elementary Abelian 2-group, and an Abelian group of odd order. [Carmichael, p. 114] - _Eric M. Schmidt_, Jan 12 2014"
			],
			"reference": [
				"R. D. Carmichael, Introduction to the Theory of Groups of Finite Order, New York, Dover, 1956.",
				"J. C. Lennox, S. E. Stonehewer, Subnormal Subgroups of Groups, Oxford University Press, 1987."
			],
			"link": [
				"Hans Havermann, \u003ca href=\"/A104404/b104404.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"B. Horvat, G. Jaklic and T. Pisanski, \u003ca href=\"https://arxiv.org/abs/math/0503183\"\u003eOn the number of Hamiltonian groups\u003c/a\u003e, arXiv:math/0503183 [math.CO], 2005.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/AbelianGroup.html\"\u003eAbelian Group\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HamiltonianGroup.html\"\u003eHamiltonian Group\u003c/a\u003e"
			],
			"formula": [
				"The number a(n) of all groups of order n all of whose subgroups are normal is given as a(n) = b(n) + h(n), where b(n) denotes the number of Abelian groups of order n and h(n) denotes the number of Hamiltonian groups of order n.",
				"a(n) = A000688(n) + A104488(n). - _Andrew Howroyd_, Aug 08 2018"
			],
			"mathematica": [
				"orders[n_]:=Map[Last, FactorInteger[n]]; b[n_]:=Apply[Times, Map[PartitionsP, orders[n]]]; e[n_]:=n/ 2^IntegerExponent[n, 2]; h[n_]/;Mod[n, 8]==0:=b[e[n]]; h[n_]:=0; a[n_]:= b[n]+h[n];"
			],
			"program": [
				"(PARI) a(n)={my(e=valuation(n, 2)); my(f=factor(n/2^e)[, 2]); prod(i=1, #f, numbpart(f[i]))*(numbpart(e) + (e\u003e=3))} \\\\ _Andrew Howroyd_, Aug 08 2018"
			],
			"xref": [
				"Cf. A000688, A000001, A104488."
			],
			"keyword": "nonn,easy,mult",
			"offset": "1,4",
			"author": "Boris Horvat (Boris.Horvat(AT)fmf.uni-lj.si), Gasper Jaklic (Gasper.Jaklic(AT)fmf.uni-lj.si), _Tomaz Pisanski_, Apr 19 2005",
			"ext": [
				"Keyword:mult added by _Andrew Howroyd_, Aug 08 2018"
			],
			"references": 6,
			"revision": 22,
			"time": "2018-08-08T19:27:45-04:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}