{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A058127",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 58127,
			"data": "1,1,1,1,2,3,1,3,8,16,1,4,15,50,125,1,5,24,108,432,1296,1,6,35,196,1029,4802,16807,1,7,48,320,2048,12288,65536,262144,1,8,63,486,3645,26244,177147,1062882,4782969,1,9,80,700,6000,50000,400000,3000000,20000000,100000000",
			"name": "Triangle read by rows: T(j,k) = number of acyclic functions from {1,...,j} to {1,...,k}. For n \u003e= 1, a(n) = (k-j)*k^(j-1), where k is such that C(k,2) \u003c n \u003c= C(k+1,2) and j = (n-1) mod C(k,2). Alternatively, table T(k,j) read by antidiagonals with k \u003e= 1, 0 \u003c= j \u003c= k: T(k,j) = number of acyclic-function digraphs on k vertices with j vertices of outdegree 1 and (k-j) vertices of outdegree 0; T(k,j) = (k-j)*k^(j-1).",
			"comment": [
				"An acyclic function f from domain D={1,...,j} to codomain C={1,...,k} is a function such that, for every subset A of D, f(A) does not equal A. Equivalently, an acyclic function f \"eventually sends\" under successive composition all elements of D to {j+1,...,k}. An acyclic-function digraph G is a labeled directed graph that satisfies (i) all vertices have outdegree 0 or 1; (ii) if vertex x has outdegree 0 and vertex y has outdegree 1, then x \u003e y; (iii) G has no cycles and no loops. There is a one-to-one correspondence between acyclic functions from D to C and acyclic-function digraphs with j vertices of outdegree 1 and j-k vertices of outdegree 0.",
				"n-th row of the triangle is the n-th iterate of \"perform binomial transform operation\" (bto) on current row to get next row, extracting the leftmost n terms for n-th row (i.e., all terms left of the zero). First row is (bto): [1, -1, 0, 0, 0, ...]. 5th row is 1, 4, 15, 50, 125, since (bto) performed 5 times iteratively on [1, -1, 0, 0, 0, ...] = 1, 4, 15, 50, 125, 0, -31, ... - _Gary W. Adamson_, Apr 30 2005",
				"T(k,j) can be shown to be equal to the number of spanning trees of the complete graph on k vertices that contain a specific subtree with k-j-1 edges. - _John L. Chiarelli_, Oct 04 2016",
				"T(k-1, j-1) is also the number of parking functions with j cars and k spots (see Theorem 2.2 in Kenyon and Yin). - _Stefano Spezia_, Apr 09 2021"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A058127/b058127.txt\"\u003eRows n = 1..50 of triangle, flattened\u003c/a\u003e",
				"Richard Kenyon and Mei Yin, \u003ca href=\"https://arxiv.org/abs/2103.17180\"\u003eParking functions: From combinatorics to probability\u003c/a\u003e, arXiv:2103.17180 [math.CO] (2021).",
				"Henri Mühle, \u003ca href=\"http://fpsac2019.fmf.uni-lj.si/resources/Proceedings/29.pdf\"\u003eBallot-Noncrossing Partitions\u003c/a\u003e, Proceedings of the 31st Conference on Formal Power Series and Algebraic Combinatorics (Ljubljana), Séminaire Lotharingien de Combinatoire (2019) Vol. 82B, Article #7.",
				"Jim Pitman and Richard P. Stanley, \u003ca href=\"https://doi.org/10.1007/s00454-002-2776-6\"\u003eA polytope related to empirical distributions, plane trees, parking functions, and the associahedron\u003c/a\u003e, Discrete Comput. Geom. 27: 603-634 (2002).",
				"D. P. Walsh, \u003ca href=\"http://www.mtsu.edu/~dwalsh/acyclic/acycnote.html\"\u003eNotes on acyclic functions and their directed graphs\u003c/a\u003e"
			],
			"formula": [
				"For fixed m = k-j, a(n) = T(k, j) = T(m+j, j) = m*(m+j)^(j-1). Exponential generating function g for T(m+j, j) = m*(m+j)^(j-1) is given by g(t) = exp(-m*W(-t)), where W denotes the principal branch of Lambert's W function. Lambert's W function satisfies W(t)*exp(W(t)) = t for t \u003e= -exp(-1).",
				"T(n, k) = Sum_{i=0..k} T(n-1, i) * binomial(k, i) if k \u003c n. - _Michael Somos_, Sep 20 2017"
			],
			"example": [
				"a(6) = T(3,2) = 3 because there are 3 acyclic functions from {1,2} to {1,2,3}: {(1,2),(2,3)}, {(1,3),(2,3)} and {(1,3),(2,1)}.",
				"Triangle begins:",
				"  1;",
				"  1, 1;",
				"  1, 2,  3;",
				"  1, 3,  8,  16;",
				"  1, 4, 15,  50,  125;",
				"  1, 5, 24, 108,  432,  1296;",
				"  1, 6, 35, 196, 1029,  4802, 16807;",
				"  1, 7, 48, 320, 2048, 12288, 65536, 262144;",
				"  ..."
			],
			"maple": [
				"T := proc(n,k) (n-k)*n^(k-1) end; seq(print(seq(T(n,k),k=0..n-1)),n=1..9); # _Peter Luschny_, Jan 14 2009"
			],
			"mathematica": [
				"t[n_, k_] := (n-k)*n^(k-1); Table[t[n, k], {n, 1, 10}, {k, 0, n-1}] // Flatten (* _Jean-François Alcover_, Dec 03 2013 *)"
			],
			"program": [
				"(MAGMA) /* As triangle */ [[(n-k)*n^(k-1): k in [0..n-1]]: n in [1.. 10]]; // _Vincenzo Librandi_, Aug 11 2017",
				"(PARI) {T(n, k) = if( k\u003c0 || k\u003en, 0, n==0, 1, (n-k) * n^(k-1))}; /* _Michael Somos_, Sep 20 2017 */"
			],
			"xref": [
				"The sum of antidiagonals is A058128. The sequence b(n) = T(n, n-1) for n \u003e= 1 is A000272, labeled trees on n nodes.",
				"The sequence c(n) = T(n, n-2) for n \u003e= 2 is A007334(n). The sequence d(n) = T(n, n-3) for n \u003e= 3 is A089463(n-3,0). - _Peter Luschny_, Apr 22 2009"
			],
			"keyword": "nice,nonn,tabl",
			"offset": "1,5",
			"author": "_Dennis P. Walsh_, Nov 14 2000",
			"ext": [
				"a(32) corrected by _T. D. Noe_, Jan 25 2008"
			],
			"references": 7,
			"revision": 44,
			"time": "2021-06-04T22:50:01-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}