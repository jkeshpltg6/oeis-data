{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A347458",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 347458,
			"data": "1,2,2,4,2,6,2,8,4,6,2,17,2,6,6,15,2,17,2,16,6,6,2,41,4,6,8,16,2,31,2,27,6,6,6,56,2,6,6,39,2,31,2,17,17,6,2,90,4,17,6,17,2,41,6,39,6,6,2,105,2,6,17,48,6,31,2,17,6,31,2,148,2,6,17,17,6",
			"name": "Number of factorizations of n^2 with integer alternating product.",
			"comment": [
				"We define the alternating product of a sequence (y_1,...,y_k) to be Product_i y_i^((-1)^(i-1)).",
				"A factorization of n is a weakly increasing sequence of positive integers \u003e 1 with product n.",
				"The even-length case, the case of alternating product 1, and the case of alternating sum 0 are all counted by A001055."
			],
			"formula": [
				"a(2^n) = A344611(n).",
				"a(n) = A347437(n^2)."
			],
			"example": [
				"The a(2) = 2 through a(8) = 8 factorizations:",
				"  4     9     16        25    36        49    64",
				"  2*2   3*3   4*4       5*5   6*6       7*7   8*8",
				"              2*2*4           2*2*9           2*4*8",
				"              2*2*2*2         2*3*6           4*4*4",
				"                              3*3*4           2*2*16",
				"                              2*2*3*3         2*2*4*4",
				"                                              2*2*2*2*4",
				"                                              2*2*2*2*2*2"
			],
			"mathematica": [
				"facs[n_]:=If[n\u003c=1,{{}},Join@@Table[Map[Prepend[#,d]\u0026,Select[facs[n/d],Min@@#\u003e=d\u0026]],{d,Rest[Divisors[n]]}]];",
				"altprod[q_]:=Product[q[[i]]^(-1)^(i-1),{i,Length[q]}];",
				"Table[Length[Select[facs[n^2],IntegerQ[altprod[#]]\u0026]],{n,100}]"
			],
			"xref": [
				"Positions of 2's are A000040, squares A001248.",
				"The restriction to powers of 2 is A344611.",
				"This is the restriction to perfect squares of A347437.",
				"The non-squared even-length version is A347438.",
				"The reciprocal version is A347459, non-squared A347439.",
				"The additive version (partitions) is the even bisection of A347446.",
				"The non-squared ordered version is A347463.",
				"The case of alternating product 1 in the ordered version is A347464.",
				"Allowing any alternating product gives A347466.",
				"A000290 lists squares, complement A000037.",
				"A001055 counts factorizations.",
				"A046099 counts factorizations with no alternating permutations.",
				"A071321 gives the alternating sum of prime factors of n (reverse: A071322).",
				"A273013 counts ordered factorizations of n^2 with alternating product 1.",
				"A347460 counts possible alternating products of factorizations.",
				"A339846 counts even-length factorizations.",
				"A339890 counts odd-length factorizations.",
				"A347457 ranks partitions with integer alternating product.",
				"Cf. A062312, A119620, A330972, A346635, A347440, A347441, A347442, A347445, A347451, A347456, A347704, A347705."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Gus Wiseman_, Sep 21 2021",
			"references": 11,
			"revision": 11,
			"time": "2021-10-04T10:02:28-04:00",
			"created": "2021-09-27T07:56:52-04:00"
		}
	]
}