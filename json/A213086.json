{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A213086",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 213086,
			"data": "1,1,1,1,2,1,1,3,3,1,1,5,6,4,1,1,7,14,10,5,1,1,11,25,30,15,6,1,1,15,53,65,55,21,7,1,1,22,89,173,140,91,28,8,1,1,30,167,343,448,266,140,36,9,1,1,42,278,778,1022,994,462,204,45,10,1,1,56,480,1518,2710,2562,1974,750,285,55,11,1",
			"name": "Square array read by antidiagonals: T(n,m) (n\u003e=1, m\u003e=0) is the number of partitions of mn that are the sum of m not necessarily distinct partitions of n.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A213086/b213086.txt\"\u003eAntidiagonals n = 1..18, flattened\u003c/a\u003e",
				"N. Metropolis and P. R. Stein, \u003ca href=\"http://dx.doi.org/10.1016/S0021-9800(70)80091-6\"\u003eAn elementary solution to a problem in restricted partitions\u003c/a\u003e, J. Combin. Theory, 9 (1970), 365-376."
			],
			"formula": [
				"Row n is a polynomial in m: see A213074 for the coefficients."
			],
			"example": [
				"The array begins:",
				"1,  1,   1,   1,    1,    1,     1,     1,     1,      1, ...",
				"1,  2,   3,   4,    5,    6,     7,     8,     9,     10, ...",
				"1,  3,   6,  10,   15,   21,    28,    36,    45,     55, ...",
				"1,  5,  14,  30,   55,   91,   140,   204,   285,    385, ...",
				"1,  7,  25,  65,  140,  266,   462,   750,  1155,   1705, ...",
				"1, 11,  53, 173,  448,  994,  1974,  3606,  6171,  10021, ...",
				"1, 15,  89, 343, 1022, 2562,  5670, 11418, 21351,  37609, ...",
				"1, 22, 167, 778, 2710, 7764, 19314, 43164, 88671, 170170, ...",
				"..."
			],
			"maple": [
				"with(combinat):",
				"g:= proc(n, m) option remember;",
				"      `if`(m\u003e1, map(x-\u003e map(y-\u003e sort([x[], y[]]), g(n, 1))[],",
				"       g(n, m-1)), `if`(m=1, map(x-\u003emap(y-\u003e `if`(y\u003e1, y-1, NULL), x),",
				"       {partition(n)[]}), {[]}))",
				"    end:",
				"T:= (n, m)-\u003e nops(g(n, m)):",
				"seq(seq(T(d-m, m), m=0..d-1), d=1..12);  # _Alois P. Heinz_, Jul 11 2012"
			],
			"mathematica": [
				"T[n_, m_] := Module[{ip, lg, i}, ip = IntegerPartitions[n]; lg = Length[ ip]; i[0]=1; Table[Join[Sequence @@ Table[ip[[i[k]]], {k, 1, m}]] // Sort, Evaluate[Sequence @@ Table[{i[k], i[k-1], lg}, {k, 1, m}]]] // Flatten[#, m-1]\u0026 // Union // Length]; T[_, 0] = 1;",
				"Table[T[n-m, m], {n, 1, 12}, {m, 0, n - 1}] // Flatten (* _Jean-François Alcover_, May 25 2016 *)"
			],
			"xref": [
				"Columns are A000041, A002219, A002220, A002221, A002222. Cf. A213074.",
				"Rows are A000027, A000217, A000330, A001296, A207361.",
				"Main diagonal gives A284645."
			],
			"keyword": "nonn,tabl",
			"offset": "1,5",
			"author": "_N. J. A. Sloane_, Jun 05 2012",
			"ext": [
				"More terms and cross-references from _Alois P. Heinz_, Jul 11 2012"
			],
			"references": 8,
			"revision": 32,
			"time": "2017-04-05T19:16:44-04:00",
			"created": "2012-06-05T09:13:02-04:00"
		}
	]
}