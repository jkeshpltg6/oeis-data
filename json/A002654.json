{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002654",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2654,
			"id": "M0012 N0001",
			"data": "1,1,0,1,2,0,0,1,1,2,0,0,2,0,0,1,2,1,0,2,0,0,0,0,3,2,0,0,2,0,0,1,0,2,0,1,2,0,0,2,2,0,0,0,2,0,0,0,1,3,0,2,2,0,0,0,0,2,0,0,2,0,0,1,4,0,0,2,0,0,0,1,2,2,0,0,0,0,0,2,1,2,0,0,4,0,0,0,2,2,0,0,0,0,0,0,2,1,0,3,2,0,0,2,0",
			"name": "Number of ways of writing n as a sum of at most two nonzero squares, where order matters; also (number of divisors of n of form 4m+1) - (number of divisors of form 4m+3).",
			"comment": [
				"Glaisher calls this E(n) or E_0(n). - _N. J. A. Sloane_, Nov 24 2018",
				"Number of sublattices of Z X Z of index n that are similar to Z X Z; number of (principal) ideals of Z[i] of norm n.",
				"a(n) is also one fourth of the number of integer solutions of n = x^2 + y^2 (order and signs matter, and 0 (without signs) is allowed). a(n) = N(n)/4, with N(n) from p. 147 of the Niven-Zuckermann reference. See also Theorem 5.12, p. 150, which defines a (strongly) multiplicative function h(n) which coincides with A056594(n-1), n \u003e= 1, and N(n)/4 = sum(h(d), d divides n). - _Wolfdieter Lang_, Apr 19 2013",
				"a(2+8*N) = A008441(N) gives the number of ways of writing N as the sum of 2 (nonnegative) triangular numbers for N \u003e= 0. - _Wolfdieter Lang_, Jan 12 2017"
			],
			"reference": [
				"J. M. Borwein, D. H. Bailey and R. Girgensohn, Experimentation in Mathematics, A K Peters, Ltd., Natick, MA, 2004. x+357 pp. See p. 194.",
				"George Chrystal, Algebra: An elementary text-book for the higher classes of secondary schools and for colleges, 6th ed., Chelsea Publishing Co., New York 1959 Part II, p. 346 Exercise XXI(17). MR0121327 (22 #12066)",
				"Emil Grosswald, Representations of Integers as Sums of Squares. Springer-Verlag, NY, 1985, p. 15.",
				"Ivan Niven and Herbert S. Zuckerman, An Introduction to the Theory of Numbers, New York: John Wiley (1980), pp. 147 and 150.",
				"Günter Scheja and Uwe Storch, Lehrbuch der Algebra, Tuebner, 1988, p. 251.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"J. V. Uspensky and M. A. Heaslet, Elementary Number Theory, McGraw-Hill, NY, 1939, p. 340."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A002654/b002654.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Michael Baake, Solution of the coincidence problem in dimensions d \u003c= 4, in R. V. Moody, ed., The Mathematics of Long-Range Aperiodic Order, Kluwer 1997, pp. 9-44; arXiv:\u003ca href=\"https://arxiv.org/abs/math/0605222\"\u003emath/0605222\u003c/a\u003e [math.MG], 2006.",
				"Michael Baake and Uwe Grimm, \u003ca href=\"http://www.ma.utexas.edu/mp_arc-bin/mpa?yn=02-392\"\u003eQuasicrystalline combinatorics\u003c/a\u003e.",
				"J. W. L. Glaisher, \u003ca href=\"http://gdz.sub.uni-goettingen.de/en/dms/loader/img/?PID=PPN600494829_0020%7CLOG_0017\"\u003eOn the function chi(n)\u003c/a\u003e, Quarterly Journal of Pure and Applied Mathematics, 20 (1884), 97-167.",
				"J. W. L. Glaisher, \u003ca href=\"/A002171/a002171.pdf\"\u003eOn the function chi(n)\u003c/a\u003e, Quarterly Journal of Pure and Applied Mathematics, 20 (1884), 97-167. [Annotated scanned copy]",
				"J. W. L. Glaisher, \u003ca href=\"https://doi.org/10.1112/plms/s1-15.1.104\"\u003eOn the function which denotes the difference between the number of (4m+1)-divisors and the number of (4m+3)-divisors of a number\u003c/a\u003e, Proc. London Math. Soc., 15 (1884), 104-122.",
				"J. W. L. Glaisher, \u003ca href=\"/A002654/a002654.pdf\"\u003eOn the function which denotes the difference between the number of (4m+1)-divisors and the number of (4m+3)-divisors of a number\u003c/a\u003e, Proc. London Math. Soc., 15 (1884), 104-122. [Annotated scanned copy of pages 104-107 only]",
				"J. W. L. Glaisher, \u003ca href=\"https://books.google.com/books?id=bLs9AQAAMAAJ\u0026amp;pg=RA1-PA1\"\u003eOn the representations of a number as the sum of two, four, six, eight, ten, and twelve squares\u003c/a\u003e, Quart. J. Math. 38 (1907), 1-62 (see p. 4 and p. 8).",
				"Stephen C. Milne, \u003ca href=\"http://dx.doi.org/10.1023/A:1014865816981\"\u003eInfinite families of exact sums of squares formulas, Jacobi elliptic functions, continued fractions and Schur functions\u003c/a\u003e, Ramanujan J., 6 (2002), 7-149.",
				"John S. Rutherford, \u003ca href=\"http://dx.doi.org/10.1107/S010876730804333X\"\u003eSublattice enumeration. IV. Equivalence classes of plane sublattices by parent Patterson symmetry and colour lattice group type\u003c/a\u003e, Acta Cryst. (2009). A65, 156-163. [See Table 1]. - From _N. J. A. Sloane_, Feb 23 2009",
				"\u003ca href=\"/index/Su#ssq\"\u003eIndex entries for sequences related to sums of squares\u003c/a\u003e",
				"\u003ca href=\"/index/Su#sublatts\"\u003eIndex entries for sequences related to sublattices\u003c/a\u003e",
				"\u003ca href=\"/index/Cor#core\"\u003eIndex entries for \"core\" sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Ge#Glaisher\"\u003eIndex entries for sequences mentioned by Glaisher\u003c/a\u003e"
			],
			"formula": [
				"Dirichlet series: (1-2^(-s))^(-1)*Product (1-p^(-s))^(-2) (p=1 mod 4) * Product (1-p^(-2s))^(-1) (p=3 mod 4) = Dedekind zeta-function of Z[ i ].",
				"Coefficients in expansion of Dirichlet series Product_p (1-(Kronecker(m, p)+1)*p^(-s)+Kronecker(m, p)*p^(-2s))^(-1) for m = -16.",
				"If n=2^k*u*v, where u is product of primes 4m+1, v is product of primes 4m+3, then a(n)=0 unless v is a square, in which case a(n) = number of divisors of u (Jacobi).",
				"Multiplicative with a(p^e) = 1 if p = 2; e+1 if p == 1 (mod 4); (e+1) mod 2 if p == 3 (mod 4). - _David W. Wilson_, Sep 01 2001",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2), A(x^4)) where f(u, v, w) = (u - v)^2 - (v - w) * (4*w + 1). - _Michael Somos_, Jul 19 2004",
				"G.f.: Sum((-1)^floor(n/2)*x^((n^2+n)/2)/(1+(-x)^n), n=1..infinity). - _Vladeta Jovovic_, Sep 15 2004",
				"Expansion of (eta(q^2)^10 / (eta(q) * eta(q^4))^4 - 1)/4 in powers of q.",
				"G.f.: Sum_{k\u003e0} x^k / (1 + x^(2*k)) = Sum_{k\u003e0} -(-1)^k * x^(2*k - 1) / (1 - x^(2*k - 1)). - _Michael Somos_, Aug 17 2005",
				"a(4*n + 3) = a(9*n + 3) = a(9*n + 6) = 0. a(9*n) = a(2*n) = a(n). - _Michael Somos_, Nov 01 2006",
				"a(4*n + 1) = A008441(n). a(3*n + 1) = A122865(n). a(3*n + 2) = A122856(n). a(12*n + 1) = A002175(n). a(12*n + 5) = 2 * A121444(n). 4 * a(n) = A004018(n) unless n=0.",
				"a(n) = Sum_{k=1..n} A010052(k)*A010052(n-k). a(A022544(n)) = 0; a(A001481(n)) \u003e 0.",
				"- _Reinhard Zumkeller_, Sep 27 2008",
				"a(n) = A001826(n) - A001842(n). - _R. J. Mathar_, Mar 23 2011",
				"a(n) = sum(A056594(d-1), d divides n), n \u003e= 1. See the above comment on A056594(d-1) = h(d) of the Niven-Zuckerman reference. - _Wolfdieter Lang_, Apr 19 2013",
				"Dirichlet g.f.: zeta(s)*beta(s) = zeta(s)*L(chi_2(4),s). - _Ralf Stephan_, Mar 27 2015",
				"G.f.: (theta_3(x)^2 - 1)/4, where theta_3() is the Jacobi theta function. - _Ilya Gutkovskiy_, Apr 17 2018",
				"a(n) = Sum_{ m: m^2|n } A000089(n/m^2). - _Andrey Zabolotskiy_, May 07 2018",
				"a(n) = A053866(n) + 2 * A025441(n). - _Andrey Zabolotskiy_, Apr 23 2019",
				"a(n) = Im(Sum_{d|n} i^d). - _Ridouane Oudra_, Feb 02 2020",
				"a(n) = Sum_{d|n} sin((1/2)*d*Pi). - _Ridouane Oudra_, Jan 22 2021"
			],
			"example": [
				"4 = 2^2, so a(4) = 1; 5 = 1^2 + 2^2 = 2^2 + 1^2, so a(5) = 2.",
				"x + x^2 + x^4 + 2*x^5 + x^8 + x^9 + 2*x^10 + 2*x^13 + x^16 + 2*x^17 + x^18 + ...",
				"2 = (+1)^2 + (+1)^2 = (+1)^2 + (-1)^2  = (-1)^2 + (+1)^2 = (-1)^2 + (-1)^2. Hence there are 4 integer solutions, called N(2) in the Niven-Zuckerman reference, and a(2) = N(2)/4 = 1.  4 = 0^1 + (+2)^2 = (+2)^2 + 0^2 = 0^2 + (-2)^2 = (-2)^2 + 0^2. Hence N(4) = 4 and a(4) = N(4)/4 = 1. N(5) = 8, a(5) = 2. - _Wolfdieter Lang_, Apr 19 2013"
			],
			"maple": [
				"with(numtheory):",
				"A002654 := proc(n)",
				"    local count1, count3, d;",
				"    count1 := 0:",
				"    count3 := 0:",
				"    for d in numtheory[divisors](n) do",
				"        if d mod 4 = 1 then",
				"            count1 := count1+1",
				"        elif d mod 4 = 3 then",
				"            count3 := count3+1",
				"        fi:",
				"    end do:",
				"    count1-count3;",
				"end proc:",
				"# second Maple program:",
				"a:= n-\u003e add(`if`(d::odd, (-1)^((d-1)/2), 0), d=numtheory[divisors](n)):",
				"seq(a(n), n=1..100);  # _Alois P. Heinz_, Feb 04 2020"
			],
			"mathematica": [
				"a[n_] := Count[Divisors[n], d_ /; Mod[d, 4] == 1] - Count[Divisors[n], d_ /; Mod[d, 4] == 3]; a/@Range[105] (* _Jean-François Alcover_, Apr 06 2011, after _R. J. Mathar_ *)",
				"QP = QPochhammer; CoefficientList[(1/q)*(QP[q^2]^10/(QP[q]*QP[q^4])^4-1)/4 + O[q]^100, q] (* _Jean-François Alcover_, Nov 24 2015 *)",
				"f[2, e_] := 1; f[p_, e_] := If[Mod[p, 4] == 1, e + 1, Mod[e + 1, 2]]; a[1] = 1; a[n_] := Times @@ f @@@ FactorInteger[n]; Array[a, 100] (* _Amiram Eldar_, Sep 19 2020 *)"
			],
			"program": [
				"(PARI) direuler(p=2,101,1/(1-X)/(1-kronecker(-4,p)*X))",
				"(PARI) {a(n) = polcoeff( sum(k=1, n, x^k / (1 + x^(2*k)), x * O(x^n)), n)}",
				"(PARI) {a(n) = sumdiv( n, d, (d%4==1) - (d%4==3))}",
				"(PARI) {a(n) = local(A); A = x * O(x^n); polcoeff( eta(x^2 + A)^10 / (eta(x + A) * eta(x^4 + A))^4 / 4, n)} /* _Michael Somos_, Jun 03 2005 */",
				"(PARI) a(n)=my(f=factor(n\u003e\u003evaluation(n,2))); prod(i=1,#f~, if(f[i,1]%4==1, f[i,2]+1, (f[i,2]+1)%2)) \\\\ _Charles R Greathouse IV_, Sep 09 2014",
				"(Haskell)",
				"a002654 n = product $ zipWith f (a027748_row m) (a124010_row m) where",
				"   f p e | p `mod` 4 == 1 = e + 1",
				"         | otherwise      = (e + 1) `mod` 2",
				"   m = a000265 n",
				"-- _Reinhard Zumkeller_, Mar 18 2013"
			],
			"xref": [
				"Cf. A000161, A001481. Equals 1/4 of A004018. Partial sums give A014200.",
				"Cf. A002175, A008441, A121444, A122856, A122865, A022544, A143574, A000265, A027748, A124010, A025426 (two squares, order does not matter), A120630 (Dirichlet inverse), A101455 (Mobius transform), A000089.",
				"If one simply reads the table in Glaisher, PLMS 1884, which omits the zero entries, one gets A213408."
			],
			"keyword": "core,easy,nonn,nice,mult",
			"offset": "1,5",
			"author": "_N. J. A. Sloane_",
			"references": 65,
			"revision": 140,
			"time": "2021-02-20T14:31:06-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}