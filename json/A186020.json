{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A186020",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 186020,
			"data": "1,1,1,2,1,1,5,3,1,1,15,9,4,1,1,52,31,14,5,1,1,203,121,54,20,6,1,1,877,523,233,85,27,7,1,1,4140,2469,1101,400,125,35,8,1,1,21147,12611,5625,2046,635,175,44,9,1,1,115975,69161,30846,11226,3488,952,236,54,10,1,1",
			"name": "Eigentriangle of the binomial matrix.",
			"comment": [
				"Reversal of Gould triangle A121207. First column is A000110. Second column is A040027.",
				"Row sums are A186021. Diagonal sums are A186022.",
				"Construction is described by Paul D. Hanna in A121207. The method of construction is general for this class of eigentriangle."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A186020/b186020.txt\"\u003eRows n = 0..100 of triangle, flattened\u003c/a\u003e",
				"Paul Barry, \u003ca href=\"http://arxiv.org/abs/1107.5490\"\u003eInvariant number triangles, eigentriangles and Somos-4 sequences\u003c/a\u003e, arXiv:1107.5490 [math.CO], 2011.",
				"Emeric Deutsch, Luca Ferrari, and Simone Rinaldi, \u003ca href=\"http://dx.doi.org/10.1016/j.aam.2004.05.002\"\u003eProduction Matrices\u003c/a\u003e, Advances in Applied Mathematics, 34 (2005) pp. 101-122."
			],
			"formula": [
				"Lower triangular (infinite) matrix T = (U - D*P)^{-1} with the unit matrix U, the Pascal matrix P from A007318 and the matrix D with elements delta_{i,j+1}, for i, j \u003e= 0 (row 0 has only 0s). From the _Paul Barry_ paper rewritten in matrix notation. T satisfies P*T = D'*(T - U), with D' the transposed matrix D, that is the diagonal of T has been erased and the row index shifted on the r.h.s. (showing that the name Eigentriangle or -matrix is a misnomer). For finite N X N matrices P*T = D'*(T - U), only up to the last row. - _Wolfdieter Lang_, Apr 07 2021"
			],
			"example": [
				"Triangle T begins",
				"       1;",
				"       1,     1;",
				"       2,     1,     1;",
				"       5,     3,     1,     1;",
				"      15,     9,     4,     1,    1;",
				"      52,    31,    14,     5,    1,   1;",
				"     203,   121,    54,    20,    6,   1,   1;",
				"     877,   523,   233,    85,   27,   7,   1,  1;",
				"    4140,  2469,  1101,   400,  125,  35,   8,  1,  1;",
				"   21147, 12611,  5625,  2046,  635, 175,  44,  9,  1, 1;",
				"  115975, 69161, 30846, 11226, 3488, 952, 236, 54, 10, 1, 1;",
				"Inverse is the identity matrix I minus binomial matrix B shifted down once, or",
				"T^{-1}(n,k)=if(k=n,1,if(k\u003cn,-binomial(n-1,k),0)). This begins",
				"   1;",
				"  -1,  1;",
				"  -1, -1,   1;",
				"  -1, -2,  -1,   1;",
				"  -1, -3,  -3,  -1,   1;",
				"  -1, -4,  -6,  -4,  -1,   1;",
				"  -1, -5, -10, -10,  -5,  -1,   1;",
				"  -1, -6, -15, -20, -15,  -6,  -1,  1;",
				"  -1, -7, -21, -35, -35, -21,  -7, -1,  1;",
				"  -1, -8, -28, -56, -70, -56, -28, -8, -1, 1;",
				"Production matrix is",
				"      1,     1;",
				"      1,     0,    1;",
				"      2,     1,    0,    1;",
				"      5,     3,    1,    0,   1;",
				"     15,     9,    4,    1,   0,   1;",
				"     52,    31,   14,    5,   1,   0,  1;",
				"    203,   121,   54,   20,   6,   1,  0, 1;",
				"    877,   523,  233,   85,  27,   7,  1, 0, 1;",
				"   4140,  2469, 1101,  400, 125,  35,  8, 1, 0, 1;",
				"  21147, 12611, 5625, 2046, 635, 175, 44, 9, 1, 0, 1;"
			],
			"mathematica": [
				"t[n_, k_] := t[n, k] = If[k == 0, 1, Sum[t[n-j, k-j] Binomial[n-1, j-1], {j, 1, k}]];",
				"T[n_, k_] := t[n, n-k];",
				"Table[T[n, k], {n, 0, 10}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Jul 27 2018 *)"
			],
			"xref": [
				"Cf. A000110, A007318, A121207, A124496, A160185, A186021, A186022."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,4",
			"author": "_Paul Barry_, Feb 10 2011",
			"references": 10,
			"revision": 37,
			"time": "2021-04-09T09:43:39-04:00",
			"created": "2011-02-10T07:28:56-05:00"
		}
	]
}