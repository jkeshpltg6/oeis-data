{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A138514",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 138514,
			"data": "1,1,-2,-1,0,-2,1,0,0,2,1,2,-2,0,2,1,0,-2,0,-2,0,-1,0,0,-2,0,0,0,-1,2,-2,0,2,0,0,2,3,0,0,-2,0,0,2,0,2,1,-2,0,0,0,-2,-2,0,2,-2,1,-2,-2,0,0,0,0,0,0,0,-2,1,0,0,0,0,-2,2,0,2,2,0,2,1,0,-2,0,2,0,-2,0,0,4,0,0,0,1,0,0,0,-2,-2,0,0,0,2,-2,0,0,-2",
			"name": "Expansion of q^(-1/8) * eta(q^2)^4 / (eta(q) * eta(q^4)) in powers of q.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Number 70 of the 74 eta-quotients listed in Table I of Martin (1996).",
				"A030204, A083650 and A138514 are the same except for signs. - _N. J. A. Sloane_, May 07 2010"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A138514/b138514.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Y. Martin, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-96-01743-6\"\u003eMultiplicative eta-quotients\u003c/a\u003e, Trans. Amer. Math. Soc. 348 (1996), no. 12, 4825-4856, see page 4852 Table I.",
				"Michael Somos, \u003ca href=\"/A030203/a030203.txt\"\u003eIndex to Yves Martin's list of 74 multiplicative eta-quotients and their A-numbers\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of f(x) * f(-x^2) = psi(-x) * phi(x) = chi(x) * f(-x^2)^2 = psi(x) * phi(-x^2) = f(x)^2 / chi(x) = f(x)^3 / phi(x) = f(-x^2)^3 / psi(-x) = phi(x)^2 / chi(x)^3 = chi(x)^3 * psi(-x)^2 = (f(x)^3 * psi(-x))^(1/2) = (f(-x^2)^3 * phi(x))^(1/2) in powers of x where phi(), psi(), chi(), f() are Ramanujan theta functions.",
				"Expansion of psi(i * x) * psi(-i * x) in powers x^2 where i^2 = -1 and psi() is a Ramanujan theta function. - _Michael Somos_, Feb 16 2014",
				"Euler transform of period 4 sequence [ 1, -3, 1, -2, ...].",
				"a(n) = b(8*n + 1) where b(n) is multiplicative and b(2^e) = 0^e, b(p^e) = 0 if p == 3, 5, 7 (mod 8) and e odd, b(p^e) = 1 if p == 3 (mod 4) and e even, b(p^e) = (-1)^(e/2) if p == 5 (mod 8) and e even, b(p^e) = e+1 if p == 1 (mod 8) and p = x^2 + 64*y^2, b(p^e) = (-1)^e * (e+1) if p == 1 (mod 8) and p is not of the form x^2 + 64*y^2.",
				"a(9*n + 1) = a(n), a(9*n + 4) = a(9*n + 7) = 0. a(n) = (-1)^n * A030204(n) = (-1)^floor((n+1)/2) * A083650(n).",
				"G.f.: Product_{k\u003e0} (1 - x^(2*k))^2 * (1 + x^(2*k - 1)).",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (256 t)) = 16 (t/i) f(t) where q = exp(2 Pi i t). - _Michael Somos_, Jun 10 2015"
			],
			"example": [
				"G.f. = 1 + x - 2*x^2 - x^3 - 2*x^5 + x^6 + 2*x^9 + x^10 + 2*x^11 - 2*x^12 + ...",
				"G.f. = q + q^9 - 2*q^17 - q^25 - 2*q^41 + q^49 + 2*q^73 + q^81 + 2*q^89 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x^2] QPochhammer[ -x], {x, 0, n}]; (* _Michael Somos_, Jun 10 2015 *)",
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x^2]^4 / (QPochhammer[ x] QPochhammer[ x^4]), {x, 0, n}]; (* _Michael Somos_, Jun 10 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^4 / (eta(x + A) * eta(x^4 + A)), n))};",
				"(PARI) {a(n) = my(A, p, e); if( n\u003c0, 0, n = 8*n + 1; A = factor(n); prod(k=1, matsize(A)[1], [p, e] = A[k,]; if( p==2, 0, p%8==1, (e+1) * if( qfbclassno( -8 * p) / 4 % 2, (-1)^e, 1), if( e%2==0, (-1)^(e/2 * (p%8==5)))))) };",
				"(PARI) {a(n) = if( n\u003c0, 0, n = 8*n + 1; (qfrep([ 1, 0; 0, 64], n) - qfrep([ 4, 2; 2, 17], n))[n])};"
			],
			"xref": [
				"Cf. A030204, A083650."
			],
			"keyword": "sign",
			"offset": "0,3",
			"author": "_Michael Somos_, Mar 22 2008",
			"references": 4,
			"revision": 26,
			"time": "2021-03-12T22:24:45-05:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}