{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A176205",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 176205,
			"data": "1,3,8,9,23,24,49,27,68,69,139,72,169,147,272,81,203,204,409,207,484,417,767,216,529,507,992,441,1007,816,1441,243,608,609,1219,612,1429,1227,2252,621,1519,1452,2837,1251,2852,2301,4051,648,1609,1587,3152,1521,3527,2976,5401,1323,3212,3021,5851,2448",
			"name": "Coefficients of S(x) which satisfies S(x) = (1 + 3*x + 5*x^2)*S(x^2).",
			"comment": [
				"Has an apparent fractal structure with the following properties:",
				"Terms n = (0, 1, 3, 7, 15,...) = (1, 3, 9, 27,...).",
				"Odd n-th terms are divisible by 3 (starting with n=1) creating the same sequence.",
				"Then the result is relabeled with n=0,1,2,...; with the odds again divisible by 3, getting (1, 3, 8, 9, 23,...); and so on."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A176205/b176205.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e"
			],
			"formula": [
				"Given M = an infinite triangular matrix with (1, 3, 5,...) in each column; shifted down twice for columns \u003e0. Then A176205 = Lim_{n-\u003einf.} M^n, the left shifted vector considered as a sequence.",
				"a(2*n) = a(n) + 5*a(n-1) and a(2*n+1) = 3*a(n), with a(0) = 1. - _G. C. Greubel_, Mar 13 2020"
			],
			"maple": [
				"a:= proc(n) option remember;",
				"      if n=0 then 1",
				"    elif `mod`(n,2)=0 then a(n/2) + 5*a(n/2 -1)",
				"    else 3*a((n-1)/2)",
				"      fi; end:",
				"seq( a(n), n=0..60); # _G. C. Greubel_, Mar 13 2020"
			],
			"mathematica": [
				"a[n_]:= If[n==0, 1, If[EvenQ[n], a[n/2] +5*a[n/2 -1], 3*a[(n-1)/2]]]; Table[a[n], {n, 0, 60}] (* _G. C. Greubel_, Mar 13 2020 *)"
			],
			"program": [
				"(Sage)",
				"@CachedFunction",
				"def a(n):",
				"    if (n==0): return 1",
				"    elif (n%2==0): return a(n/2) + 5*a(n/2 -1)",
				"    else: return 3*a((n-1)/2)",
				"[a(n) for n in (0..60)] # _G. C. Greubel_, Mar 13 2020"
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Gary W. Adamson_, Apr 11 2010",
			"ext": [
				"Terms a(25) onward added by _G. C. Greubel_, Mar 13 2020"
			],
			"references": 1,
			"revision": 11,
			"time": "2020-03-13T08:38:57-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}