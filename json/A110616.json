{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A110616",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 110616,
			"data": "1,1,1,3,2,1,12,7,3,1,55,30,12,4,1,273,143,55,18,5,1,1428,728,273,88,25,6,1,7752,3876,1428,455,130,33,7,1,43263,21318,7752,2448,700,182,42,8,1,246675,120175,43263,13566,3876,1020,245,52,9,1",
			"name": "A convolution triangle of numbers based on A001764.",
			"comment": [
				"Reflected version of A069269. - _Vladeta Jovovic_, Sep 27 2006",
				"With offset 1 for n and k, T(n,k) = number of Dyck paths of semilength n for which all descents are of even length (counted by A001764) with no valley vertices at height 1 and with k returns to ground level. For example, T(3,2)=2 counts U^4 D^4 U^2 D^2, U^2 D^2 U^4 D^4 where U=upstep, D=downstep and exponents denote repetition. - _David Callan_, Aug 27 2009",
				"Riordan array (f(x), x*f(x)) with f(x) = (2/sqrt(3*x))*sin((1/3)*arcsin(sqrt(27*x/4))). - _Philippe Deléham_, Jan 27 2014",
				"Antidiagonals of convolution matrix of Table 1.4, p. 397, of Hoggatt and Bicknell. - _Tom Copeland_, Dec 25 2019"
			],
			"link": [
				"Naiomi Cameron, J. E. McLeod, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL19/McLeod/mcleod3.html\"\u003eReturns and Hills on Generalized Dyck Paths\u003c/a\u003e, Journal of Integer Sequences, Vol. 19, 2016, #16.6.1.",
				"V. E. Hoggatt, Jr. and M. Bicknell, \u003ca href=\"http://www.fq.math.ca/Scanned/14-5/hoggatt1.pdf\"\u003eCatalan and related sequences arising from inverses of Pascal's triangle matrices\u003c/a\u003e, Fib. Quart., 14 (1976), 395-405.",
				"Sheng-Liang Yang, LJ Wang, \u003ca href=\"https://ajc.maths.uq.edu.au/pdf/64/ajc_v64_p420.pdf\"\u003eTaylor expansions for the m-Catalan numbers\u003c/a\u003e, Australasian Journal of Combinatorics, Volume 64(3) (2016), Pages 420-431."
			],
			"formula": [
				"T(n, k) = Sum_{j\u003e=0} T(n-1, k-1+j)*A000108(j); T(0, 0) = 1; T(n, k) = 0 if k \u003c 0 or if k \u003e n.",
				"G.f.: 1/(1 - x*y*TernaryGF) = 1 + (y)x + (y+y^2)x^2 + (3y+2y^2+y^3)x^3 +... where TernaryGF = 1 + x + 3x^2 + 12x^3 + ... is the GF for A001764. - _David Callan_, Aug 27 2009",
				"T(n, k) = ((k+1)*binomial(3*n-2*k,2*n-k))/(2*n-k+1). - _Vladimir Kruchinin_, Nov 01 2011"
			],
			"example": [
				"Triangle begins:",
				"       1;",
				"       1,      1;",
				"       3,      2,     1;",
				"      12,      7,     3,     1;",
				"      55,     30,    12,     4,    1;",
				"     273,    143,    55,    18,    5,    1;",
				"    1428,    728,   273,    88,   25,    6,   1;",
				"    7752,   3876,  1428,   455,  130,   33,   7,  1;",
				"   43263,  21318,  7752,  2448,  700,  182,  42,  8, 1;",
				"  246675, 120175, 43263, 13566, 3876, 1020, 245, 52, 9, 1;",
				"  ..."
			],
			"mathematica": [
				"Table[(k + 1) Binomial[3 n - 2 k, 2 n - k]/(2 n - k + 1), {n, 0, 9}, {k, 0, n}] // Flatten (* _Michael De Vlieger_, Jun 28 2017 *)"
			],
			"program": [
				"(Maxima) T(n,k):=((k+1)*binomial(3*n-2*k,2*n-k))/(2*n-k+1); // _Vladimir Kruchinin_, Nov 01 2011"
			],
			"xref": [
				"Successive columns: A001764, A006013, A001764, A006629, A102893, A006630, A102594, A006631; row sums: A098746; see also A092276."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Philippe Deléham_, Sep 14 2005, Jun 15 2007",
			"references": 5,
			"revision": 36,
			"time": "2019-12-28T10:18:25-05:00",
			"created": "2005-09-21T03:00:00-04:00"
		}
	]
}