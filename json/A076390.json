{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A076390",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 76390,
			"data": "5,9,9,0,7,0,1,1,7,3,6,7,7,9,6,1,0,3,7,1,9,9,6,1,2,4,6,1,4,0,1,6,1,9,3,9,1,1,3,6,0,6,3,3,1,6,0,7,8,2,5,7,7,9,1,3,1,8,3,7,4,7,6,4,7,3,2,0,2,6,0,7,0,7,1,9,5,7,8,3,5,4,1,7,9,4,2,7,7,8,2,4,4,8,9,6,6,9,4,6,8,7,9,5,3,6",
			"name": "Decimal expansion of lemniscate constant B.",
			"comment": [
				"Also decimal expansion of AGM(1,i)/(1+i).",
				"See A085565 for the lemniscate constant A. - _Peter Bala_, Oct 25 2019",
				"Also the ratio of height to diameter of a \"Mylar balloon\" (see Paulsen). - _Jeremy Tan_, May 05 2021"
			],
			"reference": [
				"J. M. Borwein and P. B. Borwein, Pi and the AGM: A Study in Analytic Number Theory and Computational Complexity, Wiley, 1998."
			],
			"link": [
				"W. H. Paulsen, \u003ca href=\"https://doi.org/10.2307/2975161\"\u003eWhat Is the Shape of a Mylar Balloon?\u003c/a\u003e, Amer. Math. Monthly 101 (10), (Dec. 1994), pp. 953-958.",
				"J. Todd, \u003ca href=\"https://doi.org/10.1145/360569.360580\"\u003eThe lemniscate constants\u003c/a\u003e, Comm. ACM, Vol. 18, No. 1 (1975), pp. 14-19; corrigendum, Vol. 18, No. 8 (1975), p. 462.",
				"J. Todd, \u003ca href=\"https://doi.org/10.1007/978-1-4757-3240-5_45\"\u003eThe lemniscate constants\u003c/a\u003e, in Pi: A Source Book, pp. 412-417.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Arithmetic-GeometricMean.html\"\u003eArithmetic-Geometric Mean\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Mylar_balloon_(geometry)\"\u003eMylar balloon\u003c/a\u003e",
				"Wolfram Research, \u003ca href=\"http://functions.wolfram.com/EllipticFunctions/ArithmeticGeometricMean/\"\u003eArithmetic-Geometric Mean\u003c/a\u003e."
			],
			"formula": [
				"Equals (2*Pi)^(-1/2)*GAMMA(3/4)^2.",
				"Equals ee/sqrt(2)-1/2*sqrt(2*ee^2-Pi) where ee = EllipticE(1/2), or also prod_{m\u003e=1} ((2*m)/(2*m-1))^(-1)^m. - _Jean-François Alcover_, Sep 02 2014, after Steven Finch.",
				"Equals sqrt(2) * Pi^(3/2) / GAMMA(1/4)^2. - _Vaclav Kotesovec_, Oct 03 2019",
				"From _Peter Bala_, Oct 25 2019: (Start)",
				"Equals 1 - 1/3 - 1/(3*7) - (1*3)/(3*7*11) - (1*3*5)/(3*7*11*15) - ... = hypergeom([-1/2,1],[3/4],1/2) by Gauss’s second summation theorem.",
				"Equivalently, define a sequence of rational numbers r(n) recursively by r(n) = (2*n - 3)/(4*n - 1)*r(n-1) with r(0) = 1. Then the constant equals Sum_{n \u003e= 0} r(n) = 1 - 1/3 - 1/21 - 1/77 - 1/231 - 1/627 - 3/4807 - 1/3933 - 13/121923 - 13/284487 - 17/853461 - .... The partial sum of the series to 100 terms gives the constant correct to 32 decimal places.",
				"Equals (1/3) + (1*3)/(3*7) + (1*3*5)/(3*7*11) + ... = (1/3) * hypergeom ([3/2,1],[7/4],1/2). (End)",
				"Equals (1/2) * A053004. - _Amiram Eldar_, Aug 26 2020"
			],
			"example": [
				"0.599070117367796103719961246140161939113606331607825779131837476473202607...",
				"AGM(1,i) = 0.59907011736779610371... + 0.59907011736779610371...*i"
			],
			"mathematica": [
				"RealDigits[ Chop[ N[ ArithmeticGeometricMean[1, I]/(1 + I), 111]]] [[1]]",
				"RealDigits[N[Pi/(4 EllipticK[-1]), 106]][[1]] (* _Jean-François Alcover_, Jun 02 2019 *)"
			],
			"program": [
				"(PARI) real(agm(1,I)/(1+I)) \\\\ _Charles R Greathouse IV_, Mar 03 2016",
				"(PARI) (2*Pi)^(-1/2)*gamma(3/4)^2 \\\\ _Michel Marcus_, Nov 10 2017"
			],
			"xref": [
				"Cf. A053004, A076391, A076392, A085565."
			],
			"keyword": "nonn,cons",
			"offset": "0,1",
			"author": "_Robert G. Wilson v_, Oct 09 2002",
			"ext": [
				"Edited by _N. J. A. Sloane_, Nov 01 2008 at the suggestion of _R. J. Mathar_"
			],
			"references": 9,
			"revision": 60,
			"time": "2021-05-06T01:36:17-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}