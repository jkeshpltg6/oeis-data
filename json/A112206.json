{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A112206",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 112206,
			"data": "1,1,0,2,2,1,2,2,3,4,4,4,7,7,6,10,11,11,14,16,17,21,22,24,32,34,34,44,49,50,60,66,72,84,90,98,117,125,132,156,171,181,206,226,245,277,298,322,369,397,422,480,522,557,620,674,728,807,868,936,1043,1121,1198",
			"name": "Coefficients of replicable function number \"72b\".",
			"comment": [
				"From _Michael Somos_, Oct 28 2019: (Start)",
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Convolution squared is A112173.",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (12 t)) = f(t) where q = exp(2 Pi i t).",
				"Given G.f. A(x), then B(q) = q^(-1) * A(q^6) satisifes 0 = f(B(q), B(q^2), B(q^4)) where f(u, v, w) = 2 + (u^2 - v)*v*w^2 + (u^2 + v)*v^2.",
				"(End)"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A112206/b112206.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"D. Ford, J. McKay and S. P. Norton, \u003ca href=\"http://dx.doi.org/10.1080/00927879408825127\"\u003eMore on replicable functions\u003c/a\u003e, Commun. Algebra 22, No. 13, 5175-5193 (1994).",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"\u003ca href=\"/index/Mat#McKay_Thompson\"\u003eIndex entries for McKay-Thompson series for Monster simple group\u003c/a\u003e"
			],
			"formula": [
				"a(n) ~ exp(sqrt(2*n)*Pi/3) / (2^(5/4) * sqrt(3) * n^(3/4)). - _Vaclav Kotesovec_, Sep 08 2015",
				"Expansion of  q^(1/6)*((eta(q^2)*eta(q^6))^2/(eta(q)*eta(q^3)*eta(q^4) *eta(q^12))) in powers of q. - _G. C. Greubel_, Jun 01 2018",
				"From _Michael Somos_, Oct 28 2019: (Start)",
				"Expansion of chi(x) * chi(x^3) in powers of x where chi() is a Ramanujan theta function.",
				"Euler transform of period 12 sequence [1, -1, 2, 0, 1, -2, 1, 0, 2, -1, 1, 0, ...].",
				"G.f.: Product_{k\u003e=0} (1 + x^(2*k + 1)) * (1 + x^(6*k + 3)).",
				"a(n) = (-1)^n * A112175(n). a(2*n) = A328789(n). a(2*n + 1) = A328790(n).",
				"(End)"
			],
			"example": [
				"G.f. = 1 + x + 2*x^3 + 2*x^4 + x^5 + 2*x^6 + 2*x^7 + 3*x^8 + ...",
				"G.f. = q^-1 + q^5 + 2*q^17 + 2*q^23 + q^29 + 2*q^35 + 2*q^41 + ..."
			],
			"mathematica": [
				"nmax = 60; CoefficientList[Series[Product[(1 + x^k)*(1 + x^(3*k)) / ((1 + x^(2*k))*(1 + x^(6*k))), {k, 1, nmax}], {x, 0, nmax}], x] (* _Vaclav Kotesovec_, Sep 08 2015 *)",
				"eta[q_]:= q^(1/24)*QPochhammer[q]; h:= q^(1/6)*((eta[q^2]*eta[q^6])^2/(eta[q]*eta[q^3]*eta[q^4]*eta[q^12])); a:= CoefficientList[Series [h, {q,0,60}], q]; Table[a[[n]], {n,1,50}] (* _G. C. Greubel_, Jun 01 2018 *)",
				"a[ n_] := SeriesCoefficient[ QPochhammer[ -x, x^2] QPochhammer[ -x^3, x^6], {x, 0 ,n}]; (* _Michael Somos_, Oct 28 2019 *)"
			],
			"program": [
				"(PARI) q='q+O('q^50); h=((eta(q^2)*eta(q^6))^2/(eta(q)*eta(q^3)*eta(q^4) *eta(q^12))); Vec(h) \\\\ _G. C. Greubel_, Jun 01 2018",
				"(PARI) {a(n) = my(A); if( n \u003c 0, 0, A = x * O(x^n); polcoeff( (eta(x^2 + A) * eta(x^6 + A))^2 / (eta(x + A) * eta(x^3 + A) * eta(x^4 + A) * eta(x^12 + A)), n))}; /* _Michael Somos_, Oct 28 2019 */"
			],
			"xref": [
				"Cf. A112173, A112175, A328789, A328790."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Michael Somos_, Aug 28 2005",
			"references": 9,
			"revision": 29,
			"time": "2019-10-28T21:11:30-04:00",
			"created": "2005-09-21T03:00:00-04:00"
		}
	]
}