{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A128564",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 128564,
			"data": "1,1,1,1,2,1,1,5,5,1,1,9,22,15,1,1,29,90,90,29,1,1,49,359,573,359,98,1,1,174,1415,3450,3450,1415,174,1,1,285,5545,17957,29228,21450,5545,628,1,1,1068,21670,110010,230131,230131,110010,21670,1068,1",
			"name": "Triangle, read by rows, where T(n,k) equals the number of permutations of {1..n+1} with [(nk+k)/2] inversions for n\u003e=k\u003e=0.",
			"comment": [
				"Row sums equal 2*n! for n\u003e0."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A128564/b128564.txt\"\u003eRows n = 0..140, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = A008302(n+1, [(nk+k)/2]) = coefficient of q^[(nk+k)/2] in the q-factorial of n+1 for n\u003e=0."
			],
			"example": [
				"Row sums equal 2*n! for n\u003e0:",
				"[1, 2, 4, 12, 48, 240, 1440, 10080, 80640, ..., 2*n!,...].",
				"Triangle begins:",
				"1;",
				"1, 1;",
				"1, 2, 1;",
				"1, 5, 5, 1;",
				"1, 9, 22, 15, 1;",
				"1, 29, 90, 90, 29, 1;",
				"1, 49, 359, 573, 359, 98, 1;",
				"1, 174, 1415, 3450, 3450, 1415, 174, 1;",
				"1, 285, 5545, 17957, 29228, 21450, 5545, 628, 1;",
				"1, 1068, 21670, 110010, 230131, 230131, 110010, 21670, 1068, 1;",
				"1, 1717, 84591, 526724, 1729808, 2409581, 1729808, 686763, 84591, 4015, 1;"
			],
			"maple": [
				"b:= proc(u, o) option remember; expand(`if`(u+o=0, 1,",
				"       add(b(u+j-1, o-j)*x^(u+j-1), j=1..o)+",
				"       add(b(u-j, o+j-1)*x^(u-j), j=1..u)))",
				"    end:",
				"T:= (n, k)-\u003e coeff(b(n+1, 0), x, iquo((n+1)*k, 2)):",
				"seq(seq(T(n,k), k=0..n), n=0..10);  # _Alois P. Heinz_, May 02 2017"
			],
			"mathematica": [
				"b[u_, o_] := b[u, o] = Expand[If[u + o == 0, 1, Sum[b[u + j - 1, o - j]* x^(u+j-1), {j, 1, o}] + Sum[b[u-j, o+j-1]*x^(u-j), {j, 1, u}]]];",
				"T[n_, k_] := Coefficient[b[n+1, 0], x, Quotient[(n+1)*k, 2]];",
				"Table[Table[T[n, k], {k, 0, n}], {n, 0, 10}] // Flatten (* _Jean-François Alcover_, Dec 06 2019, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(PARI) {T(n,k)=local(faq=prod(j=1, n+1, (1-q^j)/(1-q))); polcoeff(faq, (n*k+k)\\2, q)}"
			],
			"xref": [
				"Cf. A008302 (Mahonian numbers); A128565 (column 1), A128566 (column 2).",
				"Row sums give A098558."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Paul D. Hanna_, Mar 12 2007",
			"references": 6,
			"revision": 9,
			"time": "2019-12-06T11:49:32-05:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}