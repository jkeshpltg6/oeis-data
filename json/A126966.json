{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A126966",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 126966,
			"data": "1,0,-2,-8,-26,-80,-244,-752,-2362,-7584,-24892,-83376,-284324,-984672,-3455144,-12259168,-43908026,-158531392,-576352364,-2107982128,-7750490636,-28629222112,-106190978264,-395347083808,-1476813394916,-5533435084480,-20790762971864,-78316232088032",
			"name": "Expansion of sqrt(1 - 4*x)/(1 - 2*x).",
			"comment": [
				"Hankel transform is 2^n*(-1)^binomial(n+1, 2) = A120617(n). - _Paul Barry_, Feb 08 2008"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A126966/b126966.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = -Sum_{j=0..n} ( 2^j*binomial(2n-2j, n-j)/(2n-2j-1) ). - _Emeric Deutsch_, Mar 25 2007",
				"D-finite with recurrence: n*a(n) + 6*(1-n)*a(n-1) + 4*(2*n-3)*a(n-2) = 0. - _R. J. Mathar_, Nov 14 2011, corrected Feb 17 2020",
				"a(n) ~ -4^n/(sqrt(Pi)*n^(3/2)). - _Vaclav Kotesovec_, Jun 29 2013",
				"a(n) = 2^n*i + CatalanNumber(n)*hypergeom([1, n + 1/2], [n + 2], 2). - _Peter Luschny_, Aug 04 2020"
			],
			"maple": [
				"a := n -\u003e -add(2^j*binomial(2*n-2*j,n-j)/(2*n-2*j-1), j=0..n):",
				"seq(a(n),n=0..30); # _Emeric Deutsch_, Mar 25 2007",
				"# second Maple program:",
				"CatalanNumber := n -\u003e binomial(2*n, n)/(n+1):",
				"a := n -\u003e 2^n*I + CatalanNumber(n)*simplify(hypergeom([1, n + 1/2], [n + 2], 2)):",
				"seq(a(n), n=0..26); # _Peter Luschny_, Aug 04 2020"
			],
			"mathematica": [
				"CoefficientList[Series[Sqrt[1-4*x]/(1-2*x), {x,0,30}], x] (* _G. C. Greubel_, Jan 31 2017 *)"
			],
			"program": [
				"(PARI) Vec(sqrt(1-4*x)/(1-2*x) + O(x^30)) \\\\ _G. C. Greubel_, Jan 31 2017",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Rationals(), 30); Coefficients(R!( Sqrt(1-4*x)/(1-2*x) )); // _G. C. Greubel_, Jan 29 2020",
				"(Sage)",
				"def A126966_list(prec):",
				"    P.\u003cx\u003e = PowerSeriesRing(ZZ, prec)",
				"    return P( sqrt(1-4*x)/(1-2*x) ).list()",
				"A126966_list(30) # _G. C. Greubel_, Jan 29 2020",
				"(GAP) List([0..30], n-\u003e (-1)*Sum([0..n], j-\u003e 2^j*Binomial(2*(n-j), n-j)/(2*(n-j) -1) )); # _G. C. Greubel_, Jan 29 2020"
			],
			"xref": [
				"Cf. A000108, A126967."
			],
			"keyword": "sign",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_, Mar 22 2007",
			"references": 5,
			"revision": 40,
			"time": "2020-08-04T12:41:40-04:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}