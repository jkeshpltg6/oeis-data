{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A177208",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 177208,
			"data": "1,1,3,17,19,81,8351,184553,52907,1768847,70442753,1096172081,22198464713,195894185831,42653714271997,30188596935106763,20689743895700791,670597992748852241,71867806446352961329,8445943795439038164379,379371134635840861537",
			"name": "Numerators of exponential transform of 1/n.",
			"comment": [
				"b(n) = a(n)/A177209(n) is the sum over all set partitions of [n] of the product of the reciprocals of the part sizes.",
				"Numerators of moments of Dickman-De Bruijn distribution as shown on page 257 of Cellarosi and Sinai. [_Jonathan Vos Post_, Jan 07 2012]"
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), pp. 228-230."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A177208/b177208.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"F. Cellarosi and Ya. G. Sinai, \u003ca href=\"http://dx.doi.org/10.1007/s13373-011-0011-6\"\u003eThe Möbius function and statistical mechanics\u003c/a\u003e, Bull. Math. Sci., 2011.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Exponential_integral\"\u003eExponential integral\u003c/a\u003e"
			],
			"formula": [
				"E.g.f. for fractions is exp(f(z)), where f(z) = sum(k\u003e0, z^k/(k*k!)) = integral(0..z,(exp(t)-1)/t dt) = Ei(z) - gamma - log(z) = -Ein(-z). Here gamma is Euler's constant, and Ei and Ein are variants of the exponential integral."
			],
			"example": [
				"For n=4, there is 1 set partition with a single part of size 4, 4 with sizes [3,1], 3 with sizes [2,2], 6 with sizes [2,1,1], and 1 with sizes [1,1,1,1]; so b(4) = 1/4 + 4/(3*1) + 3/(2*2) + 6/(2*1*1) + 1/(1^4) = 1/4 + 4/3 + 3/4 + 3 + 1 = 19/4."
			],
			"maple": [
				"b:= proc(n) option remember; `if`(n=0, 1,",
				"      add(binomial(n-1, j-1)*b(n-j)/j, j=1..n))",
				"    end:",
				"a:= n-\u003e numer(b(n)):",
				"seq(a(n), n=0..25); # _Alois P. Heinz_, Jan 08 2012"
			],
			"mathematica": [
				"b[n_] := b[n] = If[n==0, 1, Sum[Binomial[n-1, j-1]*b[n-j]/j, {j, 1, n}]]; a[n_] := Numerator[b[n]]; Table[a[n], {n, 0, 25}] (* _Jean-François Alcover_, Feb 21 2017, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(PARI) Vec(serlaplace(exp(sum(n=1,30,x^n/(n*n!),O(x^31)))))"
			],
			"xref": [
				"Denominators are in A177209.",
				"Cf. A000110, A000248, A001620, A322364, A322365, A322380, A322381, A323339, A323340."
			],
			"keyword": "frac,nonn",
			"offset": "0,3",
			"author": "_Franklin T. Adams-Watters_, May 04 2010",
			"references": 8,
			"revision": 31,
			"time": "2019-01-11T10:42:56-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}