{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A216917",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 216917,
			"data": "1,1,1,2,1,1,6,1,1,1,12,3,2,1,1,60,3,2,1,1,1,60,15,4,3,2,1,1,420,15,20,3,6,1,1,1,840,105,20,15,12,1,2,1,1,2520,105,140,15,12,1,6,1,1,1,2520,315,280,105,12,5,12,3,2,1,1,27720,315,280,105,84",
			"name": "Square array read by antidiagonals, T(N,n) = lcm{1\u003c=j\u003c=N, gcd(j,n)=1 | j} for N \u003e= 0, n \u003e= 1.",
			"comment": [
				"T(N,n) is the least common multiple of all integers up to N that are relatively prime to n.",
				"Replacing LCM in the definition with \"product\" gives the Gauss factorial A216919."
			],
			"formula": [
				"For n \u003e 0:",
				"A(n,1) = A003418(n);",
				"A(n,2^k) = A217858(n) for k \u003e 0;",
				"A(n,3^k) = A128501(n-1) for k \u003e 0;",
				"A(2,n) = A000034(n);",
				"A(3,n) = A129203(n-1);",
				"A(4,n) = A129197(n-1);",
				"A(n,n) = A038610(n);",
				"A(floor(n/2),n) = A124443(n);",
				"A(n,1)/A(n,n) = A064446(n);",
				"A(n,1)/A(n,2) = A053644(n)."
			],
			"example": [
				"   n | N=0 1 2 3  4  5  6   7   8    9   10",
				"-----+-------------------------------------",
				"   1 |   1 1 2 6 12 60 60 420 840 2520 2520",
				"   2 |   1 1 1 3  3 15 15 105 105  315  315",
				"   3 |   1 1 2 2  4 20 20 140 280  280  280",
				"   4 |   1 1 1 3  3 15 15 105 105  315  315",
				"   5 |   1 1 2 6 12 12 12  84 168  504  504",
				"   6 |   1 1 1 1  1  5  5  35  35   35   35",
				"   7 |   1 1 2 6 12 60 60  60 120  360  360",
				"   8 |   1 1 1 3  3 15 15 105 105  315  315",
				"   9 |   1 1 2 2  4 20 20 140 280  280  280",
				"  10 |   1 1 1 3  3  3  3  21  21   63   63",
				"  11 |   1 1 2 6 12 60 60 420 840 2520 2520",
				"  12 |   1 1 1 1  1  5  5  35  35   35   35",
				"  13 |   1 1 2 6 12 60 60 420 840 2520 2520"
			],
			"mathematica": [
				"t[_, 0] = 1; t[n_, k_] := LCM @@ Select[Range[k], CoprimeQ[#, n]\u0026]; Table[t[n - k + 1, k], {n, 0, 11}, {k, n, 0, -1}] // Flatten (* _Jean-François Alcover_, Jul 29 2013 *)"
			],
			"program": [
				"(Sage)",
				"def A216917(N, n):",
				"    return lcm([j for j in (1..N) if gcd(j, n) == 1])",
				"for n in (1..13): [A216917(N,n) for N in (0..10)]"
			],
			"keyword": "nonn,tabl",
			"offset": "1,4",
			"author": "_Peter Luschny_, Oct 02 2012",
			"references": 4,
			"revision": 20,
			"time": "2018-03-04T17:47:27-05:00",
			"created": "2012-10-15T12:35:51-04:00"
		}
	]
}