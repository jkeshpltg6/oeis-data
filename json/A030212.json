{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A030212",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 30212,
			"data": "1,-4,0,16,-14,0,0,-64,81,56,0,0,-238,0,0,256,322,-324,0,-224,0,0,0,0,-429,952,0,0,82,0,0,-1024,0,-1288,0,1296,2162,0,0,896,-3038,0,0,0,-1134,0,0,0,2401,1716,0,-3808,2482,0,0,0,0,-328,0,0,-6958,0,0,4096,3332,0,0,5152,0,0",
			"name": "Glaisher's chi_4(n).",
			"comment": [
				"Number 10 of the 74 eta-quotients listed in Table I of Martin (1996). Cusp form level 4 weight 5.",
				"Called chi_4(n) by Glaisher and Hardy because as Glaisher (1907) writes on page 21 \"It can be shown (see section 53) that chi_4(n) admits of an arithmetical definition, being in fact equal to one-fourth of the sum of the fourth powers of all complex numbers which have n as norm, viz. chi_4(n) = 1/4 sum_n (a + i b)^4, where a + i b is any number which has n for norm. It is in consequence of this definition that the notation chi_4(n) has been used.\" - _Michael Somos_, Jun 18 2012",
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"reference": [
				"G. H. Hardy, Ramanujan: twelve lectures on subjects suggested by his life and work, Chelsea Publishing Company, New York 1959, p. 135 section 9.3. MR0106147 (21 #4881)",
				"H. McKean and V. Moll, Elliptic Curves, Cambridge University Press, 1997, page 175, 4.7 Exercise 5. MR1471703 (98g:14032)"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A030212/b030212.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"J. W. L. Glaisher, \u003ca href=\"https://books.google.com/books?id=bLs9AQAAMAAJ\u0026amp;pg=RA1-PA1\"\u003eOn the representations of a number as the sum of two, four, six, eight, ten, and twelve squares\u003c/a\u003e, Quart. J. Math. 38 (1907), 1-62 (see p. 34).",
				"M. Koike, \u003ca href=\"http://projecteuclid.org/euclid.nmj/1118787564\"\u003eOn McKay's conjecture\u003c/a\u003e, Nagoya Math. J., 95 (1984), 85-89.",
				"Y. Martin, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-96-01743-6\"\u003eMultiplicative eta-quotients\u003c/a\u003e, Trans. Amer. Math. Soc. 348 (1996), no. 12, 4825-4856, see page 4852 Table I.",
				"K. Ono, S. Robins and P. T. Wahl, \u003ca href=\"http://www.mathcs.emory.edu/~ono/publications-cv/pdfs/006.pdf\"\u003eOn the representation of integers as sums of triangular numbers\u003c/a\u003e, Aequationes mathematicae, August 1995, Volume 50, Issue 1-2, pp 73-94. Case k=10, called F(tau).",
				"M. Rogers, J.G. Wan, and I. J. Zucker, \u003ca href=\"http://arXiv.org/abs/1303.2259\"\u003eMoments of Elliptic Integrals and Critical L-Values\u003c/a\u003e, arXiv:1303.2259 [math.NT], 2013.",
				"Shobhit, \u003ca href=\"http://math.stackexchange.com/questions/483708/\"\u003eHow to prove that eta(q^4)^14/eta(q^8)^4 = 4eta(q^2)^4eta(q^4)^2eta(q^8)^4 + eta(q)^4eta(q^2)^2eta(q^4)^4?\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A030203/a030203.txt\"\u003eIndex to Yves Martin's list of 74 multiplicative eta-quotients and their A-numbers\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"\u003ca href=\"/index/Ge#Glaisher\"\u003eIndex entries for sequences mentioned by Glaisher\u003c/a\u003e"
			],
			"formula": [
				"Expansion of phi(q)^2 * psi(-q)^8 = chi(q)^6 * psi(-q)^10 = f(q)^3 * psi(-q)^7 = f(-q^2)^6 * psi(-q)^4 = f(-q^2)^10 / chi(q)^4 in powers of q where phi(), psi(), chi(), f() are Ramanujan theta functions. - _Michael Somos_, Mar 12 2013",
				"Expansion of eta(q)^4 * eta(q^2)^2 * eta(q^4)^4 in powers of q.",
				"G.f.: x * (Product_{k\u003e0} (1 - x^k)^4 * (1 - x^(2*k))^2 * (1 - x^(4*k))^4).",
				"G.f.: (t*t'' - 3(t')^2) / 2 where t = theta_3(x) (A000122) and t' := x * (dt/dx), t'' := (t')'. - _Michael Somos_, Nov 08 2005",
				"Euler transform of period 4 sequence [ -4, -6, -4, -10, ...]. - _Michael Somos_, Jul 17 2004",
				"a(n) is multiplicative with a(2^e) = (-4)^e, a(p^e) = p^(2*e) * (1 + (-1)^e)/2 if p == 3 (mod 4), a(p^e) = a(p) * a(p^(e-1)) - p^4 * a(p^(e-2)) for p == 1 (mod 4) where a(p) = 2 * Re( (x + i*y)^4 ) and p = x^2 + y^2 with even x. - _Michael Somos_, Nov 18 2014",
				"Given A = A0 + A1 + A2 + A3 is the 4-section, then 0 = (A0^2 - A2^2)^2 + 4 * A0*A2*A1^2. - _Michael Somos_, Mar 08 2006",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (4 t)) = 32 (t/i)^5 f(t) where q = exp(2 Pi i t). - _Michael Somos_, May 28 2013",
				"a(4*n + 3) = 0. - _Michael Somos_, Mar 12 2013",
				"a(2*n) = -4 * a(n). a(4*n + 1) = A215472(n). - _Michael Somos_, Sep 05 2013",
				"a(n) = 1/4 * Sum_{a^2 + b^2 = n} (a + bi)^4 = Sum_{a \u003e 0, b \u003e= 0, a^2 + b^2 = n} (a + bi)^4. - _Seiichi Manyama_, Apr 25 2017"
			],
			"example": [
				"G.f. = q - 4*q^2 + 16*q^4 - 14*q^5 - 64*q^8 + 81*q^9 + 56*q^10 - 238*q^13 + ...",
				"From _Seiichi Manyama_, Apr 25 2017: (Start)",
				"a(1) = (1 + 0i)^4 = 1,",
				"a(2) = (1 + 1i)^4 = -4,",
				"a(4) = (2 + 0i)^4 = 16,",
				"a(5) = (1 + 2i)^4 + (2 + 1i)^4 = -7 - 24i - 7 + 24i = -14,",
				"a(8) = (2 + 2i)^4 = -64,",
				"a(9) = (3 + 0i)^4 = 81,",
				"a(10) = (1 + 3i)^4 + (3 + 1i)^4 = 28 - 96i + 28 + 96i = 56 (End)"
			],
			"mathematica": [
				"If[SquaresR[2,#]==0,0,1/4 Plus@@((x+I y)^4/.{ToRules[Reduce[x^2+y^2==#,{x,y},Integers]]})] \u0026/@Range[70] (* _Ant King_, Nov 10 2012 *)",
				"a[ n_] := SeriesCoefficient[ q (QPochhammer[ q]^2 QPochhammer[ q^2] QPochhammer[ q^4]^2)^2, {q, 0, n}]; (* _Michael Somos_, May 28 2013 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c1, 0, n--; A = x * O(x^n); polcoeff( (eta(x + A) * eta(x^4 + A))^4 * eta(x^2 + A)^2, n))}; /* _Michael Somos_, Jul 17 2004 */",
				"(PARI) {a(n) = local(r); if( n\u003c1, 0, r = sqrtint(n); sum( x=-r, r, sum( y=-r, r, if( x^2 + y^2 == n, (x + I*y)^4) )) / 4 )}; /* _Michael Somos_, Sep 12 2005 */",
				"(PARI) {a(n) = my(A, p, e, x, y, z, a0, a1); if( n\u003c0, 0, A = factor(n); prod( k=1, matsize(A)[1], [p, e] = A[k,]; if( p==2, (-4)^e, p%4 == 3, if( e%2, 0, p^(2*e)), forstep( i=0, sqrtint(p), 2, if( issquare( p - i^2, \u0026y), x = i; break)); a0 = 1; a1 = x = real( (x + I*y)^4 ) * 2; for( i=2, e, y = x*a1 - p^4*a0; a0=a1; a1=y); a1))) }; /* _Michael Somos_, Nov 18 2014 */",
				"(Sage) CuspForms( Gamma1(4), 5, prec=71).0; # _Michael Somos_, May 28 2013",
				"(MAGMA) Basis( CuspForms( Gamma1(4), 5), 71) [1]; /* _Michael Somos_, May 27 2014 */"
			],
			"xref": [
				"Cf. A002607, A215472, A247067."
			],
			"keyword": "sign,mult",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"references": 7,
			"revision": 70,
			"time": "2021-03-12T22:24:42-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}