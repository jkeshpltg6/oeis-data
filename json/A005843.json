{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005843",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5843,
			"id": "M0985",
			"data": "0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38,40,42,44,46,48,50,52,54,56,58,60,62,64,66,68,70,72,74,76,78,80,82,84,86,88,90,92,94,96,98,100,102,104,106,108,110,112,114,116,118,120",
			"name": "The nonnegative even numbers: a(n) = 2n.",
			"comment": [
				"-2, -4, -6, -8, -10, -12, -14, ... are the trivial zeros of the Riemann zeta function. - Vivek Suri (vsuri(AT)jhu.edu), Jan 24 2008",
				"If a 2-set Y and an (n-2)-set Z are disjoint subsets of an n-set X then a(n-2) is the number of 2-subsets of X intersecting both Y and Z. - _Milan Janjic_, Sep 19 2007",
				"A134452(a(n)) = 0; A134451(a(n)) = 2 for n \u003e 0. - _Reinhard Zumkeller_, Oct 27 2007",
				"Omitting the initial zero gives the number of prime divisors with multiplicity of product of terms of n-th row of A077553. - _Ray Chandler_, Aug 21 2003",
				"A059841(a(n))=1, A000035(a(n))=0. - _Reinhard Zumkeller_, Sep 29 2008",
				"(APSO) Alternating partial sums of (a-b+c-d+e-f+g...)=(a+b+c+d+e+f+g...)-2*(b+d+f...), it appears that APSO(A005843) = A052928 = A002378 - 2*(A116471), with A116471=2*A008794. - _Eric Desbiaux_, Oct 28 2008",
				"A056753(a(n)) = 1. - _Reinhard Zumkeller_, Aug 23 2009",
				"Twice the nonnegative numbers. - _Juri-Stepan Gerasimov_, Dec 12 2009",
				"The number of hydrogen atoms in straight-chain (C(n)H(2n+2)), branched (C(n)H(2n+2), n \u003e 3), and cyclic, n-carbon alkanes (C(n)H(2n), n \u003e 2). - _Paul Muljadi_, Feb 18 2010",
				"For n \u003e= 1; a(n) = the smallest numbers m with the number of steps n of iterations of {r - (smallest prime divisor of r)} needed to reach 0 starting at r = m. See A175126 and A175127. A175126(a(n)) = A175126(A175127(n)) = n. Example (a(4)=8): 8-2=6, 6-2=4, 4-2=2, 2-2=0; iterations has 4 steps and number 8 is the smallest number with such result. - _Jaroslav Krizek_, Feb 15 2010",
				"For n \u003e= 1, a(n) = numbers k such that arithmetic mean of the first k positive integers is not integer. A040001(a(n)) \u003e 1. See A145051 and A040001. - _Jaroslav Krizek_, May 28 2010",
				"Union of A179082 and A179083. - _Reinhard Zumkeller_, Jun 28 2010",
				"a(k) is the (Moore lower bound on and the) order of the (k,4)-cage: the smallest k-regular graph having girth four: the complete bipartite graph with k vertices in each part. - _Jason Kimberley_, Oct 30 2011",
				"For n \u003e 0: A048272(a(n)) \u003c= 0. - _Reinhard Zumkeller_, Jan 21 2012",
				"Let n be the number of pancakes that have to be divided equally between n+1 children. a(n) is the minimal number of radial cuts needed to accomplish the task. - _Ivan N. Ianakiev_, Sep 18 2013",
				"For n \u003e 0, a(n) is the largest number k such that (k!-n)/(k-n) is an integer. - _Derek Orr_, Jul 02 2014",
				"a(n) when n \u003e 2 is also the number of permutations simultaneously avoiding 213, 231 and 321 in the classical sense which can be realized as labels on an increasing strict binary tree with 2n-1 nodes. See A245904 for more information on increasing strict binary trees. - _Manda Riehl_ Aug 07 2014",
				"It appears that for n \u003e 2, a(n) = A020482(n) + A002373(n), where all sequences are infinite. This is consistent with Goldbach's conjecture, which states that every even number \u003e 2 can be expressed as the sum of two prime numbers. - _Bob Selcoe_, Mar 08 2015",
				"Number of partitions of 4n into exactly 2 parts. - _Colin Barker_, Mar 23 2015",
				"Number of neighbors in von Neumann neighborhood. - _Dmitry Zaitsev_, Nov 30 2015",
				"Unique solution b( ) of the complementary equation a(n) = a(n-1)^2 - a(n-2)*b(n-1), where a(0) = 1, a(1) = 3, and a( ) and b( ) are increasing complementary sequences. - _Clark Kimberling_, Nov 21 2017",
				"Also the maximum number of non-attacking bishops on an (n+1) X (n+1) board (n\u003e0). (Cf. A000027 for rooks and queens (n\u003e3), A008794 for kings or A030978 for knights.) - _Martin Renner_, Jan 26 2020",
				"Integer k is even positive iff phi(2k) \u003e phi(k), where phi is Euler's totient (A000010) [see reference De Koninck \u0026 Mercier]. - _Bernard Schott_, Dec 10 2020"
			],
			"reference": [
				"T. M. Apostol, Introduction to Analytic Number Theory, Springer-Verlag, 1976, page 2.",
				"J.-M. De Koninck and A. Mercier, 1001 Problèmes en Théorie Classique des Nombres, Problème 529a pp. 71 and 257, Ellipses, 2004, Paris.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A005843/b005843.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"David Callan, \u003ca href=\"https://arxiv.org/abs/1911.02209\"\u003eOn Ascent, Repetition and Descent Sequences\u003c/a\u003e, arXiv:1911.02209 [math.CO], 2019.",
				"Charles Cratty, Samuel Erickson, Frehiwet Negass, and Lara Pudwell, \u003ca href=\"http://www.valpo.edu/mathematics-statistics/files/2015/07/Pattern-Avoidance-in-Double-Lists.pdf\"\u003ePattern Avoidance in Double Lists\u003c/a\u003e, preprint, 2015.",
				"Kevin Fagan, \u003ca href=\"http://chesswanks.com/pot/IntelligenceTest.jpg\"\u003eDrabble cartoon, Jun 15 1987: Intelligence Test\u003c/a\u003e",
				"Adam M. Goyt and Lara K. Pudwell, \u003ca href=\"http://arxiv.org/abs/1203.3786\"\u003eAvoiding colored partitions of two elements in the pattern sense\u003c/a\u003e, arXiv preprint arXiv:1203.3786 [math.CO], 2012, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL15/Goyt/goyt4.html\"\u003eJ. Int. Seq. 15 (2012) # 12.6.2\u003c/a\u003e",
				"Milan Janjic, \u003ca href=\"http://www.pmfbl.org/janjic/\"\u003eTwo Enumerative Functions\u003c/a\u003e",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/EvenNumber.html\"\u003eEven Number\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HamiltonianCycle.html\"\u003eHamiltonian Cycle\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RiemannZetaFunctionZeros.html\"\u003eRiemann Zeta Function Zeros\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Alkane\"\u003eAlkane\u003c/a\u003e",
				"\u003ca href=\"/index/Cor#core\"\u003eIndex entries for \"core\" sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,-1)."
			],
			"formula": [
				"G.f.: 2*x/(1-x)^2.",
				"E.g.f.: 2*x*exp(x). - _Geoffrey Critzer_, Aug 25 2012",
				"G.f. with interpolated zeros: 2x^2/((1-x)^2 * (1+x)^2); e.g.f. with interpolated zeros: x*sinh(x). - _Geoffrey Critzer_, Aug 25 2012.",
				"Inverse binomial transform of A036289, n*2^n. - _Joshua Zucker_, Jan 13 2006",
				"a(0) = 0, a(1) = 2, a(n) = 2a(n-1) - a(n-2). - _Jaume Oliver Lafont_, May 07 2008",
				"a(n) = Sum_{k=1..n} floor(6n/4^k + 1/2). - _Vladimir Shevelev_, Jun 04 2009",
				"a(n) = A034856(n+1) - A000124(n) = A000217(n) + A005408(n) - A000124(n) = A005408(n) - 1. - _Jaroslav Krizek_, Sep 05 2009",
				"a(n) = Sum_{k\u003e=0} A030308(n,k)*A000079(k+1). - _Philippe Deléham_, Oct 17 2011",
				"Digit sequence 22 read in base n-1. - _Jason Kimberley_, Oct 30 2011",
				"a(n) = 3*a(n-1) - 3*a(n-2) + a(n-3). - _Vincenzo Librandi_, Dec 23 2011",
				"a(n) = 2*n = Product_{k=1..2*n-1} 2*sin(Pi*k/(2*n)), n \u003e= 0 (undefined product := 1). See an Oct 09 2013 formula contribution in A000027 with a reference. - _Wolfdieter Lang_, Oct 10 2013",
				"From _Ilya Gutkovskiy_, Aug 19 2016: (Start)",
				"Convolution of A007395 and A057427.",
				"Sum_{n\u003e=1} (-1)^(n+1)/a(n) = log(2)/2 = (1/2)*A002162 = (1/10)*A016655. (End)",
				"From _Bernard Schott_, Dec 10 2020: (Start)",
				"Sum_{n\u003e=1} 1/a(n)^2 = Pi^2/24 = A222171.",
				"Sum_{n\u003e=1} (-1)^(n+1)/a(n)^2 = Pi^2/48 = A245058. (End)"
			],
			"example": [
				"G.f. = 2*x + 4*x^2 + 6*x^3 + 8*x^4 + 10*x^5 + 12*x^6 + 14*x^7 + 16*x^8 + ..."
			],
			"maple": [
				"A005843 := n-\u003e2*n;",
				"A005843:=2/(z-1)**2; # _Simon Plouffe_ in his 1992 dissertation"
			],
			"mathematica": [
				"Range[0,120,2] (* _Harvey P. Dale_, Aug 16 2011 *)"
			],
			"program": [
				"(MAGMA) [ 2*n : n in [0..100]];",
				"(R) seq(0,200,2)",
				"(PARI) A005843(n) = 2*n",
				"(Haskell)",
				"a005843 = (* 2)",
				"a005843_list = [0, 2 ..]  -- _Reinhard Zumkeller_, Feb 11 2012"
			],
			"xref": [
				"a(n)=2*A001477(n). - _Juri-Stepan Gerasimov_, Dec 12 2009",
				"Cf. A000027, A002061, A005408, A001358, A077553, A077554, A077555, A002024, A087112, A157888, A157889, A140811, A157872, A157909, A157910, A165900.",
				"Moore lower bound on the order of a (k,g) cage: A198300 (square); rows: A000027 (k=2), A027383 (k=3), A062318 (k=4), A061547 (k=5), A198306 (k=6), A198307 (k=7), A198308 (k=8), A198309 (k=9), A198310 (k=10), A094626 (k=11); columns: A020725 (g=3), this sequence (g=4), A002522 (g=5), A051890 (g=6), A188377 (g=7). - _Jason Kimberley_, Oct 30 2011",
				"Cf. A231200 (boustrophedon transform)."
			],
			"keyword": "nonn,easy,core,nice",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 684,
			"revision": 234,
			"time": "2021-03-12T22:32:36-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}