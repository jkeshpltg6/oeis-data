{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A182338",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 182338,
			"data": "3,6,8,9,12,15,18,21,24,27,30,33,36,39,40,42,45,48,51,54,56,57,60,63,64,66,69,72,75,78,81,84,87,88,90,93,96,99,102,104,105,108,111,114,117,120,123,125,126,129,132,135,136,138,141,144,147,150,152,153",
			"name": "List of positive integers whose prime tower factorization, as defined in comments, contains the prime 3.",
			"comment": [
				"This set is the complement of A182337.",
				"The prime tower factorization of a number can be recursively defined as follows:",
				"(0) The prime tower factorization of 1 is itself",
				"(1) To find the prime tower factorization of an integer n\u003e1, let n = p1^e1 * p2^e2 * ... * pk^ek be the usual prime factorization of n. Then the prime tower factorization is given by p1^(f1) * p2^(f2) * ... * pk^(fk), where fi is the prime tower factorization of ei."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A182338/b182338.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Patrick Devlin and Edinah Gnang, \u003ca href=\"http://arxiv.org/abs/1204.5251\"\u003ePrimes Appearing in Prime Tower Factorization\u003c/a\u003e, arXiv:1204.5251v1 [math.NT], 2012-2014."
			],
			"maple": [
				"# The integer n is in this sequence if and only if",
				"# containsPrimeInTower(3, n) returns true",
				"containsPrimeInTower:=proc(q, n) local i, L, currentExponent; option remember;",
				"if n \u003c= 1 then return false: end if;",
				"if type(n/q, integer) then return true: end if;",
				"L := ifactors(n)[2];",
				"for i to nops(L) do currentExponent := L[i][2];",
				"if containsPrimeInTower(q, currentExponent) then return true: end if",
				"end do;",
				"return false:",
				"end proc:",
				"select(x-\u003e containsPrimeInTower(3,x), [$1..160])[];"
			],
			"mathematica": [
				"indic[1] = 1; indic[n_] := indic[n] = Switch[f = FactorInteger[n], {{3, _}}, 0, {{_, _}}, indic[f[[1, 2]]], _, Times @@ (indic /@ (Power @@@ f))];",
				"Select[Range[200], indic[#] != 1\u0026] (* _Jean-François Alcover_, Jul 11 2018 *)"
			],
			"xref": [
				"Complement of A182337.  Cf. A182318."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Patrick Devlin_, Apr 25 2012",
			"references": 1,
			"revision": 19,
			"time": "2020-04-11T06:10:50-04:00",
			"created": "2012-04-25T21:04:19-04:00"
		}
	]
}