{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A214293",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 214293,
			"data": "1,0,0,1,-1,0,0,0,1,0,0,0,0,0,0,1,0,0,0,-1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,-1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,-1,1,0,0,0,0",
			"name": "a(n) = 1 if n is a square, -1 if n is five times a square.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A214293/b214293.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"S. Cooper and M. Hirschhorn, \u003ca href=\"http://dx.doi.org/10.1216/rmjm/1008959672\"\u003eOn some infinite product identities\u003c/a\u003e, Rocky Mountain J. Math., 31 (2001) 131-139. see p. 134 Theorem 4.",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of (phi(q) - phi(q^5)) / 2 in powers of q where phi() is a Ramanujan theta function. - _Michael Somos_, Sep 24 2013",
				"Expansion of q * f(q^3, q^7) * f(-q^4, -q^16) / f(-q^8, -q^12) in powers of q where f() is Ramanujan's two-variable theta function.",
				"Expansion of q * f(x, x^9) * f(-q, -q^4) / f(-q^2, -q^3) in powers of q where f() is Ramanujan's two-variable theta function. - _Michael Somos_, Sep 24 2013",
				"Euler transform of period 20 sequence [ 0, 0, 1, -1, 0, -1, 1, 1, 0, -1, 0, 1, 1, -1, 0, -1, 1, 0, 0, -1, ...].",
				"Multiplicative with a(5^e) = (-1)^e, a(p^e) = 1 if e even, 0 otherwise.",
				"G.f.: (theta_3(q) - theta_3(q^5)) / 2 = Sum_{k\u003e0} x^(k^2) - x^(5*k^2).",
				"Dirichlet g.f.: zeta(2*s) * (1 - 5^-s).",
				"a(4*n + 2) = a(4*n + 3) = 0. a(4*n) = a(n). a(5*n) = -a(n).",
				"a(4*n) = A214293(n). a(4*n+1) = A214960(n). - _Michael Somos_, Sep 24 2013"
			],
			"example": [
				"G.f. = q + q^4 - q^5 + q^9 + q^16 - q^20 + q^25 + q^36 - q^45 + q^49 + q^64 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (EllipticTheta[ 3, 0, q] - EllipticTheta[ 3, 0, q^5]) / 2,  {q, 0, n}];",
				"a[ n_] := If[ n \u003c 0, 0, Boole[ OddQ [ Length @ Divisors @ n]] - Boole[ OddQ [ Length @ Divisors [5 n]]]];"
			],
			"program": [
				"(PARI) {a(n) = issquare(n) - issquare(5*n)};",
				"(PARI) {a(n) = if( n\u003c1, 0, direuler( p=2, n, if( p==5, 1 - X, 1) / (1 - X^2 ))[n])};",
				"(MAGMA) Basis( ModularForms( Gamma1(20), 1/2), 65) [2]; /* _Michael Somos_, Jul 01 2014 */"
			],
			"xref": [
				"Cf. A214284, A214293, A214960."
			],
			"keyword": "sign,mult,easy",
			"offset": "1,1",
			"author": "_Michael Somos_, Jul 10 2012",
			"references": 1,
			"revision": 30,
			"time": "2021-03-12T22:24:46-05:00",
			"created": "2012-07-11T15:02:30-04:00"
		}
	]
}