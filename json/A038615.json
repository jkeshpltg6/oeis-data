{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A038615",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 38615,
			"data": "2,3,5,11,13,19,23,29,31,41,43,53,59,61,83,89,101,103,109,113,131,139,149,151,163,181,191,193,199,211,223,229,233,239,241,251,263,269,281,283,293,311,313,331,349,353,359,383,389,401,409,419,421,431,433,439",
			"name": "Primes not containing the digit '7'.",
			"comment": [
				"Subsequence of primes of A052419. - _Michel Marcus_, Feb 22 2015",
				"Maynard proves that this sequence is infinite and in particular contains the expected number of elements up to x, on the order of x^(log 9/log 10)/log x. - _Charles R Greathouse IV_, Apr 08 2016"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A038615/b038615.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e",
				"Marianne Freiberger, \u003ca href=\"https://plus.maths.org/content/missing-7s\"\u003ePrimes without 7s\u003c/a\u003e, +plus magazine, August 2016.",
				"M. F. Hasler, \u003ca href=\"/wiki/Numbers_avoiding_certain_digits\"\u003eNumbers avoiding certain digits\u003c/a\u003e, OEIS wiki, Jan 12 2020.",
				"James Maynard, \u003ca href=\"http://arxiv.org/abs/1604.01041\"\u003ePrimes with restricted digits\u003c/a\u003e, arXiv:1604.01041 [math.NT], 2016.",
				"James Maynard and Brady Haran, \u003ca href=\"https://www.youtube.com/watch?v=eeoBCS7IEqs\"\u003ePrimes without a 7\u003c/a\u003e, Numberphile video (2019)."
			],
			"formula": [
				"Intersection of A000040 (primes) and A052419 (numbers with no digit 7). - _M. F. Hasler_, Jan 11 2020"
			],
			"mathematica": [
				"Select[Prime[Range[70]], DigitCount[#, 10, 7] == 0 \u0026] (* _Vincenzo Librandi_, Aug 08 2011 *)"
			],
			"program": [
				"(MAGMA) [ p: p in PrimesUpTo(500) | not 7 in Intseq(p) ]; // _Bruno Berselli_, Aug 08 2011",
				"(PARI) lista(nn)=forprime(p=2, nn, if (!vecsearch(vecsort(digits(p),,8), 7), print1(p, \", \"));); \\\\ _Michel Marcus_, Feb 22 2015",
				"(PARI) (A038615_upto(N)=select( is_A052419, primes([1,N])))(444) \\\\ i.e.: {is_A038615(n)=is_A052419(n)\u0026\u0026isprime(n)}; {is_A052419(n)=!setsearch(Set(digits(n)),7)}. - _M. F. Hasler_, Jan 11 2020"
			],
			"xref": [
				"Cf. A000040, A052419.",
				"Primes having no digit d = 0..9 are A038618, A038603, A038604, A038611, A038612, A038613, A038614, this sequence, A038616, and A038617, respectively."
			],
			"keyword": "nonn,easy,base",
			"offset": "1,1",
			"author": "Vasiliy Danilov (danilovv(AT)usa.net), Jul 15 1998",
			"ext": [
				"Offset corrected by _Arkadiusz Wesolowski_, Aug 07 2011"
			],
			"references": 23,
			"revision": 41,
			"time": "2020-01-16T02:04:34-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}