{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A123023",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 123023,
			"data": "1,0,1,0,3,0,15,0,105,0,945,0,10395,0,135135,0,2027025,0,34459425,0,654729075,0,13749310575,0,316234143225,0,7905853580625,0,213458046676875,0,6190283353629375,0,191898783962510625,0,6332659870762850625,0,221643095476699771875",
			"name": "a(n) = (n-1)*a(n-2), a(0)=1, a(1)=0.",
			"comment": [
				"a(n) is the number of ways of separating n terms into pairs. - _Stephen Crowley_, Apr 07 2007",
				"a(n) is the n-th moment of the standard normal distribution. - _Hal M. Switkay_, Nov 06 2019",
				"a(n) is the number of fixed-point free involutions in the symmetric group of degree n. - _Nick Krempel_, Feb 26 2020"
			],
			"reference": [
				"Richard Bronson, Schaum's Outline of Modern Introductory Differential Equations, MacGraw-Hill, New York, 1973, page 107, solved problem 19.18",
				"Norbert Wiener, Nonlinear Problems in Random Theory, 1958, Equation 1.31"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A123023/b123023.txt\"\u003eTable of n, a(n) for n = 0..799\u003c/a\u003e",
				"Michael Wallner, \u003ca href=\"https://arxiv.org/abs/1706.07163\"\u003eA bijection of plane increasing trees with relaxed binary trees of right height at most one\u003c/a\u003e, arXiv:1706.07163 [math.CO], 2017-2018, Table 2 on p. 15."
			],
			"formula": [
				"a(n) = (1/2)*Gamma((1/2)*n + 1/2)*2^((1/2)*n)*(1 + (-1)^n)/sqrt(Pi). - _Stephen Crowley_, Apr 07 2007",
				"E.g.f.: exp(x^2/2). - _Geoffrey Critzer_, Mar 15 2009",
				"a(2n) = A001147(n). - _R. J. Mathar_, Oct 11 2011",
				"From _Sergei N. Gladkovskii_, Nov 18 2012, Dec 05 2012, May 16 2013, May 24 2013, Jun 07 2013: (Start)",
				"Continued fractions:",
				"E.g.f.: E(0) where E(k) = 1 + x^2*(4*k+1)/((4*k+2)*(4*k+3) - x^2*(4*k+2)*(4*k+3)^2/(x^2*(4*k+3) + (4*k+4)*(4*k+5)/E(k+1))).",
				"G.f.: 1/G(0) where G(k) =  1 - x^2*(k+1)/G(k+1).",
				"G.f.: 1 + x^2/(1+x) + Q(0)*x^3/(1+x), where Q(k) = 1 + (2*k+3)*x/(1 - x/(x + 1/Q(k+1))).",
				"G.f.: G(0)/2, where G(k) = 1 + 1/(1-x/(x+1/x/(2*k+1)/G(k+1))).",
				"G.f.: (G(0) - 1)*x/(1+x) + 1, where G(k) = 1 + x*(2*k+1)/(1 - x/(x + 1/G(k+1))). (End)",
				"For n even, a(n) = A001147(n/2) = A124794(3^(n/2)). a(n) is also the coefficient of x1*...*xn in Product_{1 \u003c= i \u003c j \u003c= n} (1 + xi*xj). - _Gus Wiseman_, Dec 23 2018"
			],
			"example": [
				"From _Gus Wiseman_, Dec 23 2018: (Start)",
				"The a(6) = 15 ways of partitioning {1,2,3,4,5,6} into disjoint pairs:",
				"  {{12}{34}{56}}",
				"  {{12}{35}{46}}",
				"  {{12}{36}{45}}",
				"  {{13}{24}{56}}",
				"  {{13}{25}{46}}",
				"  {{13}{26}{45}}",
				"  {{14}{23}{56}}",
				"  {{14}{25}{36}}",
				"  {{14}{26}{35}}",
				"  {{15}{23}{46}}",
				"  {{15}{24}{36}}",
				"  {{15}{26}{34}}",
				"  {{16}{23}{45}}",
				"  {{16}{24}{35}}",
				"  {{16}{25}{34}}",
				"(End)"
			],
			"maple": [
				"with(combstruct): ZL2 := [S, {S=Set(Cycle(Z, card=2))}, labeled]:",
				"seq(count(ZL2, size=n), n=0..36); # _Zerinvary Lajos_, Sep 24 2007"
			],
			"mathematica": [
				"RecurrenceTable[{a[0] == 1, a[1] == 0, a[n] == (n - 1) a[n - 2]}, a[n], {n, 0, 31}] (* _Ray Chandler_, Jul 30 2015 *)"
			],
			"program": [
				"(MAGMA) a:=[1,0]; [n le 2 select a[n] else (n-2)*Self(n-2): n in [1..30]]; // _Marius A. Burtea_, Nov 07 2019"
			],
			"xref": [
				"Cf. A000085, A000110, A000124, A000142, A000587, A001147, A001464, A006129, A079267, A124794, A186021, A295193."
			],
			"keyword": "nonn,easy",
			"offset": "0,5",
			"author": "_Roger L. Bagula_, Sep 24 2006",
			"ext": [
				"Edited by _N. J. A. Sloane_, Jan 06 2008",
				"Better name by _Sergei N. Gladkovskii_, May 24 2013",
				"Leading term 1 dropped, offset changed, and entry edited correspondingly by _Andrey Zabolotskiy_, Nov 07 2019"
			],
			"references": 19,
			"revision": 86,
			"time": "2020-02-26T13:28:18-05:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}