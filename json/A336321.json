{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A336321",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 336321,
			"data": "1,2,3,4,7,5,19,6,9,11,53,10,131,23,13,8,311,15,719,22,29,59,1619,14,49,137,21,46,3671,17,8161,12,61,313,37,25,17863,727,139,26,38873,31,84017,118,39,1621,180503,20,361,77,317,274,386093,33,71,58,733,3673,821641,34,1742537,8167,87,18,151,67,3681131,626,1627,41,7754077,35,16290047",
			"name": "a(n) = A122111(A225546(n)).",
			"comment": [
				"A122111 and A225546 are both self-inverse permutations of the positive integers based on prime factorizations, and they share further common properties. For instance, they map the prime numbers to powers of 2: A122111 maps the k-th prime to 2^k, whereas A225546 maps it to 2^2^(k-1).",
				"In composing these permutations, this sequence maps the squarefree numbers, as listed in A019565, to the prime numbers in increasing order; and the list of powers of 2 to the \"normal\" numbers (A055932), as listed in A057335."
			],
			"link": [
				"Michel Marcus, \u003ca href=\"/A336321/b336321.txt\"\u003eTable of n, a(n) for n = 1..148\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A122111(A225546(n)).",
				"Alternative definition: (Start)",
				"Write n = m^2 * A019565(j), where m = A000188(n), j = A248663(n).",
				"a(1) = 1; otherwise for m = 1, a(n) = A000040(j), for m \u003e 1, a(n) = A253550^j(A253560(a(m))).",
				"(End)",
				"a(A000040(m)) = A033844(m-1).",
				"a(A001146(m)) = 2^(m+1).",
				"a(2^n) = A057335(n).",
				"a(n^2) = A253560(a(n)).",
				"For n in A003159, a(2n) = b(a(n)), where b(1) = 2, b(n) = A253550(n), n \u003e= 2.",
				"More generally, a(A334747(n)) = b(a(n)).",
				"a(A003961(n)) = A297002(a(n)).",
				"a(A334866(m)) = A253563(m)."
			],
			"example": [
				"From _Peter Munn_, Jan 04 2021: (Start)",
				"In this set of examples we consider [a(n)] as a function a(.) with an inverse, a^-1(.).",
				"First, a table showing mapping of the powers of 2:",
				"  n     a^-1(2^n) =    2^n =        a(2^n) =",
				"        A001146(n-1)   A000079(n)   A057335(n)",
				"  0             (1)         1            1",
				"  1               2         2            2",
				"  2               4         4            4",
				"  3              16         8            6",
				"  4             256        16            8",
				"  5           65536        32           12",
				"  6      4294967296        64           18",
				"  ...",
				"Next, a table showing mapping of the squarefree numbers, as listed in A019565 (a lexicographic ordering by prime factors):",
				"  n   a^-1(A019565(n))   A019565(n)      a(A019565(n))   a^2(A019565(n))",
				"      Cf. {A337533}      Cf. {A005117}   = prime(n)      = A033844(n-1)",
				"  0              1               1             (1)               (1)",
				"  1              2               2               2                 2",
				"  2              3               3               3                 3",
				"  3              8               6               5                 7",
				"  4              6               5               7                19",
				"  5             12              10              11                53",
				"  6             18              15              13               131",
				"  7            128              30              17               311",
				"  8              5               7              19               719",
				"  9             24              14              23              1619",
				"  ...",
				"As sets, the above columns are A337533, A005117, A008578, {1} U A033844.",
				"Similarly, we get bijections between sets A000290\\{0} -\u003e {1} U A070003; and {1} U A335740 -\u003e A005408 -\u003e A066207.",
				"(End)"
			],
			"xref": [
				"A122111 composed with A225546.",
				"Cf. A336322 (inverse permutation).",
				"Other sequences used in a definition of this sequence: A000040, A000188, A019565, A248663, A253550, A253560.",
				"Sequences used to express relationship between terms of this sequence: A003159, A003961, A297002, A334747.",
				"Cf. A057335.",
				"A mapping between the binary tree sequences A334866 and A253563.",
				"Lists of sets (S_1, S_2, ... S_j) related by the bijection defined by the sequence: (A000290\\{0}, {1} U A070003), ({1} U A001146, A000079, A055932), ({1} U A335740, A005408, A066207), (A337533, A005117, A008578, {1} U A033844)."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Antti Karttunen_ and _Peter Munn_, Jul 17 2020",
			"references": 4,
			"revision": 41,
			"time": "2021-02-12T11:39:06-05:00",
			"created": "2020-08-20T21:23:14-04:00"
		}
	]
}