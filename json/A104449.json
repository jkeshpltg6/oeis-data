{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A104449",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 104449,
			"data": "3,1,4,5,9,14,23,37,60,97,157,254,411,665,1076,1741,2817,4558,7375,11933,19308,31241,50549,81790,132339,214129,346468,560597,907065,1467662,2374727,3842389,6217116,10059505,16276621,26336126,42612747,68948873,111561620",
			"name": "Fibonacci sequence with initial values a(0) = 3 and a(1) = 1.",
			"comment": [
				"The old name was: The Pibonacci numbers (a Fibonacci-type sequence): each term is the sum of the two previous terms.",
				"The 6th row in the Wythoff array begins with the 6th term of the sequence (14, 23, 37, 60, 97, 157, ...). a(n) = f(n-3) + f(n+2) for the Fibonacci numbers f(n) = f(n-1) + f(n-2); f(0) = 0, f(1) = 1.",
				"(a(2*k), a(2*k+1)) give for k \u003e= 0 the proper positive solutions of one of two families (or classes) of solutions (x, y) of the indefinite binary quadratic form x^2 + x*y - y^2 of discriminant 5 representing 11. The other family of such solutions is given by (x2, y2) = (b(2*k), b(2*k+1)) with b = A013655. See the formula in terms of Chebyshev S polynomials S(n, 3) = A001906(n+1) below, which follows from the fundamental solution (3, 1) by applying positive powers of the automorphic matrix given in a comment in A013655. See also A089270 with the Alfred Brousseau link with D = 11. - _Wolfdieter Lang_, May 28 2019"
			],
			"reference": [
				"V. E. Hoggatt, Jr., Fibonacci and Lucas Numbers. Houghton, Boston, MA, 1969."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A104449/b104449.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"John Conway, Alex Ryba, \u003ca href=\"https://doi.org/10.1007/s00283-015-9582-5\"\u003eThe extra Fibonacci series and the Empire State Building\u003c/a\u003e, Math. Intelligencer 38 (2016), no. 1, 41-48. (Uses the name Pibonacci.)",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"Ron Knott, \u003ca href=\"http://www.maths.surrey.ac.uk/hosted-sites/R.Knott/Fibonacci/\"\u003eFibonacci Numbers and the Golden Section \u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/FibonacciNumber.html\"\u003eFibonacci Number\u003c/a\u003e",
				"Shaoxiong Yuan, \u003ca href=\"https://arxiv.org/abs/1907.12459\"\u003eGeneralized Identities of Certain Continued Fractions\u003c/a\u003e, arXiv:1907.12459 [math.NT], 2019.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,1).",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e"
			],
			"formula": [
				"a(n) = a(n-1) + a(n-2) with a(0) = 3, a(1) = 1.",
				"a(n) = 3*Fibonacci(n-1) + Fibonacci(n). - _Zerinvary Lajos_, Oct 05 2007",
				"G.f.: (3-2*x)/(1-x-x^2). - _Philippe Deléham_, Nov 19 2008",
				"a(n) = ( (3*sqrt(5)-1)*((1+sqrt(5))/2)^n + (3*sqrt(5)+1)*((1-sqrt(5) )/2)^n )/(2*sqrt(5)). - _Bogart B. Strauss_, Jul 19 2013",
				"Bisection: a(2*k) = 4*S(k-1, 3) - 3*S(k-2, 3), a(2*k+1) = 2*S(k-1, 3) + S(k, 3) for k \u003e= 0, with the Chebyshev S(n, 3) polynomials from A001906(n+1) for n \u003e= -1. - _Wolfdieter Lang_, May 28 2019",
				"a(n) = Fibonacci(n-1) + Lucas(n). - _G. C. Greubel_, May 29 2019",
				"a(3n + 4)/a(3n + 1) = continued fraction 4,4,4,...,4,9 (that's n 4's followed by a single 9). - _Greg Dresden_ and _Shaoxiong Yuan_, Jul 16 2019",
				"E.g.f.: (exp((1/2)*(1 - sqrt(5))*x)*(1 + 3*sqrt(5) + (- 1 + 3*sqrt(5))*exp(sqrt(5)*x)))/(2*sqrt(5)). - _Stefano Spezia_, Jul 18 2019"
			],
			"maple": [
				"a:=n-\u003e3*fibonacci(n-1)+fibonacci(n): seq(a(n), n=0..40); # _Zerinvary Lajos_, Oct 05 2007"
			],
			"mathematica": [
				"LinearRecurrence[{1,1},{3,1},40] (* _Harvey P. Dale_, May 23 2014 *)"
			],
			"program": [
				"(PARI) a(n)=3*fibonacci(n-1)+fibonacci(n) \\\\ _Charles R Greathouse IV_, Jun 05 2011",
				"(MAGMA) [Fibonacci(n-1) + Lucas(n): n in [0..40]]; // _G. C. Greubel_, May 29 2019",
				"(Sage) ((3-2*x)/(1-x-x^2)).series(x, 40).coefficients(x, sparse=False) # _G. C. Greubel_, May 29 2019",
				"(GAP) a:=[3,1];; for n in [3..40] do a[n]:=a[n-1]+a[n-2]; od; a; # _G. C. Greubel_, May 29 2019"
			],
			"xref": [
				"Cf. Other Fibonacci-type sequences: A000045, A000032, A013655. Other related sequences: A001906, A013655, A089270, A103343, A103344.",
				"Wythoff array: A035513.",
				"Essentially the same as A000285."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_Casey Mongoven_, Mar 08 2005",
			"ext": [
				"Name changed by _Wolfdieter Lang_, Jun 17 2019"
			],
			"references": 11,
			"revision": 85,
			"time": "2019-08-03T18:07:56-04:00",
			"created": "2005-04-09T03:00:00-04:00"
		}
	]
}