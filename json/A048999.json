{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A048999",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 48999,
			"data": "1,2,-1,6,-6,1,24,-36,12,0,120,-240,120,0,-4,720,-1800,1200,0,-120,0,5040,-15120,12600,0,-2520,0,120,40320,-141120,141120,0,-47040,0,6720,0,362880,-1451520,1693440,0,-846720,0,241920,0,-12096,3628800",
			"name": "Triangle giving coefficients of (n+1)!*B_n(x), where B_n(x) is a Bernoulli polynomial, ordered by falling powers of x.",
			"reference": [
				"I. S. Gradshteyn and I. M. Ryzhik, Tables of Integrals, Series and Products, 5th ed., Section 9.62."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A048999/b048999.txt\"\u003eRows n = 0..50, flattened\u003c/a\u003e",
				"D. H. Lehmer, \u003ca href=\"http://www.jstor.org/stable/2322383\"\u003eA new approach to Bernoulli polynomials\u003c/a\u003e, The American mathematical monthly 95.10 (1988): 905-911.",
				"\u003ca href=\"/index/Be#Bernoulli\"\u003eIndex entries for sequences related to Bernoulli numbers.\u003c/a\u003e"
			],
			"formula": [
				"t*exp(x*t)/(exp(t)-1) = Sum_{n \u003e= 0} B_n(x)*t^n/n!.",
				"a(n,m) = [x^(n-m)]((n+1)!*B_n(x)), n\u003e=0, m=0,...,n. - _Wolfdieter Lang_, Jun 21 2011"
			],
			"example": [
				"B_0=1  =\u003e  a(0) = 1;",
				"B_1(x)=x-1/2  =\u003e  a(1..2) = 2, -1;",
				"B_2(x)=x^2-x+1/6  =\u003e  a(3..5) = 6, -6, 1;",
				"B_3(x)=x^3-3*x^2/2+x/2  =\u003e  a(6..9) = 24, -36, 12, 0;",
				"B_4(x)=x^4-2*x^3+x^2-1/30  =\u003e a(10..14) = 120, -240, 120, 0, -4;",
				"..."
			],
			"mathematica": [
				"row[n_] := (n+1)!*Reverse[ CoefficientList[ BernoulliB[n, x], x]]; Flatten[ Table[ row[n], {n, 0, 9}]] (* _Jean-François Alcover_, Feb 17 2012 *)"
			],
			"program": [
				"(PARI) P=Pol(t*exp(x*t)/(exp(t)-1)); for(i=0,15, z=polcoeff(P,i,t)*i!; print(z\"  =\u003e  \",(i+1)!*Vec(z)))  /* print B_n's and list of normalized coefficients */ \\\\ _M. F. Hasler_, Jun 21 2011"
			],
			"xref": [
				"Three versions of coefficients of Bernoulli polynomials: A053382/A053383; for reflected version see A196838/A196839; see also A048998 and A048999."
			],
			"keyword": "sign,easy,nice,tabl",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Name clarified by adding 'Falling powers of x.' from _Wolfdieter Lang_, Jun 21 2011",
				"Values corrected by inserting a(9),a(20),a(35)=0 by _M. F. Hasler_, Jun 21 2011"
			],
			"references": 7,
			"revision": 41,
			"time": "2017-06-24T00:28:29-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}