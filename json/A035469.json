{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A035469",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 35469,
			"data": "1,4,1,28,12,1,280,160,24,1,3640,2520,520,40,1,58240,46480,11880,1280,60,1,1106560,987840,295960,40040,2660,84,1,24344320,23826880,8090880,1296960,109200,4928,112,1,608608000,643843200",
			"name": "Triangle read by rows, the Bell transform of the triple factorial numbers A007559(n+1) without column 0.",
			"comment": [
				"Previous name was: Triangle of numbers related to triangle A035529; generalization of Stirling numbers of second kind A008277, Lah-numbers A008297 and A035342.",
				"a(n,m) enumerates unordered n-vertex m-forests composed of m plane increasing quartic (4-ary) trees. Proof based on the a(n,m) recurrence. See a D. Callan comment on the m=1 case A007559. See also the F. Bergeron et al. reference, especially Table 1, first row and Example 1 for the e.g.f. for m=1. - _Wolfdieter Lang_, Sep 14 2007",
				"For the definition of the Bell transform see A264428. - _Peter Luschny_, Jan 19 2016"
			],
			"reference": [
				"F. Bergeron, Ph. Flajolet and B. Salvy, Varieties of Increasing Trees, in Lecture Notes in Computer Science vol. 581, ed. J.-C. Raoult, Springer 1922, pp. 24-48."
			],
			"link": [
				"Peter Bala, \u003ca href=\"/A035342/a035342.txt\"\u003eGeneralized Dobinski formulas\u003c/a\u003e",
				"P. Blasiak, K. A. Penson and A. I. Solomon, \u003ca href=\"http://www.arXiv.org/abs/quant-ph/0402027\"\u003eThe general boson normal ordering problem\u003c/a\u003e, arXiv:quant-ph/0402027, 2004.",
				"Richell O. Celeste, Roberto B. Corcino, Ken Joffaniel M. Gonzales. \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL20/Celeste/celeste3.html\"\u003e Two Approaches to Normal Order Coefficients\u003c/a\u003e. Journal of Integer Sequences, Vol. 20 (2017), Article 17.3.5.",
				"Tom Copeland, \u003ca href=\"http://tcjpn.wordpress.com/2015/08/23/a-class-of-differential-operators-and-the-stirling-numbers/\"\u003eA Class of Differential Operators and the Stirling Numbers\u003c/a\u003e",
				"Tom Copeland, \u003ca href=\"http://tcjpn.wordpress.com/\"\u003eMathemagical Forests\u003c/a\u003e",
				"Tom Copeland, \u003ca href=\"http://tcjpn.wordpress.com/\"\u003eAddendum to Mathemagical Forests\u003c/a\u003e",
				"Milan Janjic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL12/Janjic/janjic22.html\"\u003eSome classes of numbers and derivatives\u003c/a\u003e, JIS 12 (2009) 09.8.3",
				"Wolfdieter Lang, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL3/LANG/lang.html\"\u003eOn generalizations of Stirling number triangles\u003c/a\u003e, J. Integer Seqs., Vol. 3 (2000), #00.2.4.",
				"Wolfdieter Lang, \u003ca href=\"/A035469/a035469.txt\"\u003eFirst 10 rows\u003c/a\u003e.",
				"Shi-Mei Ma, \u003ca href=\"http://arxiv.org/abs/1208.3104\"\u003eSome combinatorial sequences associated with context-free grammars\u003c/a\u003e, arXiv:1208.3104v2 [math.CO]. - From _N. J. A. Sloane_, Aug 21 2012",
				"E. Neuwirth, \u003ca href=\"http://homepage.univie.ac.at/erich.neuwirth/papers/TechRep99-05.pdf\"\u003eRecursively defined combinatorial functions: Extending Galton's board\u003c/a\u003e, Discrete Math. 239 (2001) 33-51.",
				"Mathias Pétréolle and Alan D. Sokal, \u003ca href=\"https://arxiv.org/abs/1907.02645\"\u003eLattice paths and branched continued fractions. II. Multivariate Lah polynomials and Lah symmetric functions\u003c/a\u003e, arXiv:1907.02645 [math.CO], 2019."
			],
			"formula": [
				"a(n, m) = Sum_{j=m..n} |A051141(n, j)|*S2(j, m) (matrix product), with S2(j, m):=A008277(j, m) (Stirling2 triangle). Priv. comm. to _Wolfdieter Lang_ by E. Neuwirth, Feb 15 2001; see also the 2001 Neuwirth reference. See the general comment on products of Jabotinsky matrices given under A035342.",
				"a(n, m) = n!*A035529(n, m)/(m!*3^(n-m)); a(n+1, m) = (3*n+m)*a(n, m) + a(n, m-1), n \u003e= m \u003e= 1; a(n, m) := 0, n \u003c m; a(n, 0) := 0, a(1, 1)=1;",
				"E.g.f. of m-th column: ((-1+(1-3*x)^(-1/3))^m)/m!.",
				"From _Peter Bala_, Nov 25 2011: (Start)",
				"E.g.f.: G(x,t) = exp(t*A(x)) = 1 + t*x + (4*t+t^2)*x^2/2! + (28*t + 12*t^2 + t^3)*x^3/3! + ..., where A(x) = -1 + (1-3*x)^(-1/3) satisfies the autonomous differential equation A'(x) = (1+A(x))^4.",
				"The generating function G(x,t) satisfies the partial differential equation t*(dG/dt+G) = (1-3*x)*dG/dx, from which follows the recurrence given above.",
				"The row polynomials are given by D^n(exp(x*t)) evaluated at x = 0, where D is the operator (1+x)^4*d/dx. Cf. A008277 (D = (1+x)*d/dx), A105278 (D = (1+x)^2*d/dx), A035342 (D = (1+x)^3*d/dx) and A049029 (D = (1+x)^5*d/dx).",
				"(End)",
				"Dobinski-type formula for the row polynomials: R(n,x) = exp(-x)*Sum_{k\u003e=0} k*(k+3)*(k+6)*...*(k+3*(n-1))*x^k/k!. - _Peter Bala_, Jun 23 2014"
			],
			"example": [
				"Triangle starts:",
				"     {1}",
				"     {4,    1}",
				"    {28,   12,    1}",
				"   {280,  160,   24,    1}",
				"  {3640, 2520,  520,   40,    1}"
			],
			"mathematica": [
				"a[n_, m_] /; n \u003e= m \u003e= 1 := a[n, m] = (3(n-1) + m)*a[n-1, m] + a[n-1, m-1]; a[n_, m_] /; n \u003c m = 0; a[_, 0] = 0; a[1, 1] = 1; Flatten[Table[a[n, m], {n, 1, 9}, {m, 1, n}]] (* _Jean-François Alcover_, Jul 22 2011 *)",
				"rows = 9;",
				"a[n_, m_] := BellY[n, m, Table[Product[3k+1, {k, 0, j}], {j, 0, rows}]];",
				"Table[a[n, m], {n, 1, rows}, {m, 1, n}] // Flatten (* _Jean-François Alcover_, Jun 22 2018 *)"
			],
			"program": [
				"(Sage) # uses[bell_matrix from A264428]",
				"# Adds a column 1,0,0,0, ... at the left side of the triangle.",
				"bell_matrix(lambda n: A007559(n+1) , 9) # _Peter Luschny_, Jan 19 2016"
			],
			"xref": [
				"a(n, m)=: S2(4, n, m) is the fourth triangle of numbers in the sequence S2(1, n, m) := A008277(n, m) (Stirling 2nd kind), S2(2, n, m) := A008297(n, m) (Lah), S2(3, n, m) := A035342(n, m). a(n, 1)= A007559(n).",
				"Row sums: A049119(n), n \u003e= 1.",
				"Cf. A094638."
			],
			"keyword": "easy,nice,nonn,tabl",
			"offset": "1,2",
			"author": "_Wolfdieter Lang_",
			"ext": [
				"New name from _Peter Luschny_, Jan 19 2016"
			],
			"references": 37,
			"revision": 90,
			"time": "2021-11-27T11:05:55-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}