{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A089025",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 89025,
			"data": "8,15,21,35,40,48,55,65,77,80,91,96,99,112,117,119,133,143,153,160,168,171,176,187,207,209,221,224,225,247,253,255,264,275,280,285,299,312,319,323,325,341,345,352,360,377,391,403,408,416,425,435,437,440,448",
			"name": "Side of primitive equilateral triangle bearing at least one integral cevian that partitions an edge into two integral sections.",
			"comment": [
				"The cevians are numbers divisible only by primes of form 6n+1:A002476 (i.e., correspond to entries of A004611).",
				"Composite cevians c belong to more than one equilateral triangle, actually to 2^(omega(c)-1) of them, where omega(n)=A001221(n). For instance, cevian 1813=7^2*37, with omega(1813)=2, belongs to 2^(2-1)=2 equilateral triangles, their sides being 1927=255+1627 and 1960=343+1617, while cevian 1729=7*13*19, with omega(1729)=3, belongs to 2^(3-1)=4 equilateral triangles whose sides are 1775=96+1679, 1824=209+1615, 1840=249+1591, 1859=299+1560.",
				"Given a triangle with integer side lengths a, b, c relatively prime with a \u003c b, c \u003c b, and angle opposite c of 60 degrees then a*a - a*b + b*b = c*c from law of cosines and called a primitive Eisenstein triple by Gordon. This sequence is the possible side lengths of b. - _Michael Somos_, Apr 11 2012"
			],
			"reference": [
				"Mohammad K. Azarian, A Trigonometric Characterization of  Equilateral Triangle, Problem 336, Mathematics and Computer Education, Vol. 31, No. 1, Winter 1997, p. 96.  Solution published in Vol. 32, No. 1, Winter 1998, pp. 84-85.",
				"Mohammad K. Azarian, Equating Distances and Altitude in an Equilateral Triangle, Problem 316, Mathematics and Computer Education, Vol. 28, No. 3, Fall 1994, p. 337.  Solution published in Vol. 29, No. 3, Fall 1995, pp. 324-325."
			],
			"link": [
				"O. Delgado-Friedrichs and M. O'Keeffe, \u003ca href=\"http://dx.doi.org/10.1107/S0108767309026270\"\u003eEdge-transitive lattice nets\u003c/a\u003e, Acta Cryst. A, A65 (2009), 360-363.",
				"Russell A. Gordon, \u003ca href=\"http://www.jstor.org/stable/10.4169/math.mag.85.1.12\"\u003eProperties of Eisenstein Triples\u003c/a\u003e, Mathematics Magazine 85 (2012), 12-25."
			],
			"example": [
				"The equilateral triangle with side 280, for instance, has cevian 247 partitioning an edge into 93+187, as well as cevian 271 that sections the edge into 19+261."
			],
			"mathematica": [
				"findPrimIntEquiSide[maxC_] :=",
				"Reap[Do[Do[",
				"     With[{cevian = Abs[c E^((2 \\[Pi] I)/6) - a]},",
				"      If[FractionalPart[cevian] == 0 \u0026\u0026 GCD[a, c] == 1,",
				"       Sow[c]; Break[]]], {a, Floor[c/2],",
				"      1, -1}], {c, maxC}]][[2, 1]]",
				"(* _Andrew Turner_, Aug 04 2017 *)"
			],
			"xref": [
				"Cf. A088513, A088514, A088977."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Lekraj Beedassy_, Nov 12 2003",
			"references": 10,
			"revision": 36,
			"time": "2017-08-10T04:39:31-04:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}