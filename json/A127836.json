{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A127836",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 127836,
			"data": "1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,1,1,1,1,1,1,2,2,2,1,1,1,1,1,1,1,2,2,3,2,2,2,2,1,1,1,1,1,1,2,2,3,3,3,3,3,3,3,2,1,1,1,1,1,1,1,2,2,3,3,4,4,4,4,5,4,4,3,3,2,2,1,1,1,1,1,1,2,2,3,3,4,5,5,5,6,6,6,6,6,5,5",
			"name": "Triangle read by rows: row n gives coefficients (lowest degree first) of P_n(x), where P_0(x) = P_1(x) = 1; P_n(x) = P_{n-1}(x) + x^(n-1)*P_{n-2}(x).",
			"comment": [
				"P_n(x) has degree A002620(n).",
				"Row sums are the Fibonacci numbers (A000045). - _Emeric Deutsch_, May 12 2007",
				"T(n,k) is the number of Fibonacci words of length n-1 in which the sum of the positions of the 0's is equal to k. A Fibonacci binary word is a binary word having no 00 subword. Examples: T(5,4) = 2 because we have 1110 and 0101; T(7,6) = 3 because we have 111110, 101011 and 011101. - _Emeric Deutsch_, Jan 04 2009"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A127836/b127836.txt\"\u003eTable of n, a(n) for n = 0..9548 (rows n=0..48 of triangle, flattened).\u003c/a\u003e",
				"M. Barnabei, F. Bonetti, S. Elizalde, M. Silimbani, \u003ca href=\"http://arxiv.org/abs/1401.3011\"\u003eDescent sets on 321-avoiding involutions and hook decompositions of partitions\u003c/a\u003e, arXiv preprint arXiv:1401.3011 [math.CO], 2014.",
				"A. V. Sills, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v10i1r13\"\u003eFinite Rogers-Ramanujan type identities\u003c/a\u003e, Electron. J. Combin. 10 (2003), Research Paper 13, 122 pp. See Identity 3-18, pp. 26-27.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Rogers-RamanujanContinuedFraction.html\"\u003eRogers-Ramanujan Continued Fraction\u003c/a\u003e"
			],
			"example": [
				"Triangle begins:",
				"   1;",
				"   1;",
				"   1, 1;",
				"   1, 1, 1;",
				"   1, 1, 1, 1, 1;",
				"   1, 1, 1, 1, 2, 1, 1;",
				"   1, 1, 1, 1, 2, 2, 2, 1, 1, 1;",
				"   1, 1, 1, 1, 2, 2, 3, 2, 2, 2, 2, 1, 1;",
				"   1, 1, 1, 1, 2, 2, 3, 3, 3, 3, 3, 3, 3, 2, 1, 1, 1;",
				"   1, 1, 1, 1, 2, 2, 3, 3, 4, 4, 4, 4, 5, 4, 4, 3, 3, 2, 2, 1, 1;",
				"   ..."
			],
			"maple": [
				"P[0]:=1; P[1]:=1; d:=[0,0]; M:=14; for n from 2 to M do P[n]:=expand(P[n-1]+q^(n-1)*P[n-2]);",
				"lprint(seriestolist(series(P[n],q,M^2))); d:=[op(d),degree(P[n],q)]; od: d;"
			],
			"mathematica": [
				"P[0] = P[1] = 1; P[n_] := P[n] = P[n-1] + x^(n-1) P[n-2];",
				"Table[CoefficientList[P[n], x], {n, 0, 10}] // Flatten (* _Jean-François Alcover_, Jul 23 2018 *)"
			],
			"program": [
				"(Maxima) P(n, x) := if n = 0 or n = 1 then 1 else P(n - 1, x) + x^(n  - 1)*P(n - 2, x)$ create_list(ratcoef(expand(P(n, x)), x, k), n, 0, 10, k, 0, floor(n^2/4)); /* _Franck Maminirina Ramaharo_, Nov 30 2018 */"
			],
			"xref": [
				"Rows converge to A003114 (coefficients in expansion of the first Rogers-Ramanujan identities). Cf. A128915, A119469."
			],
			"keyword": "nonn,tabf",
			"offset": "0,17",
			"author": "_N. J. A. Sloane_, Apr 07 2007",
			"references": 5,
			"revision": 35,
			"time": "2018-12-26T17:09:50-05:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}