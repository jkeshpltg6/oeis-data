{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A128617",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 128617,
			"data": "0,1,1,0,1,0,0,1,0,0,0,1,0,0,0,0,2,1,0,1,0,0,2,0,0,0,1,0,0,1,0,1,0,0,0,0,0,2,0,0,0,0,0,0,1,0,2,1,0,1,0,0,2,0,0,0,2,0,0,0,0,2,0,0,0,0,0,2,0,0,0,1,0,0,1,0,0,0,0,1,0,0,2,0,0,0,0",
			"name": "Expansion of q^2 * psi(q) * psi(q^15) in powers of q where psi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Also the number of positive odd solutions to equation x^2 + 15y^2 = 8n. - _Seiichi Manyama_, May 21 2017"
			],
			"reference": [
				"B. C. Berndt, Ramanujan's Notebooks Part III, Springer-Verlag, see p. 377, Entry 9(i)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A128617/b128617.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of (eta(q^2) * eta(q^30))^2 / (eta(q) * eta(q^15)) in powers of q.",
				"Euler transform of period 30 sequence [ 1, -1, 1, -1, 1, -1, 1, -1, 1, -1, 1, -1, 1, -1, 2, -1, 1, -1, 1, -1, 1, -1, 1, -1, 1, -1, 1, -1, 1, -2, ...].",
				"For n\u003e0, n in A028955 equivalent to a(n) nonzero. If a(n) nonzero, a(n) = A082451(n) and a(n) = -A121362(n).",
				"a(n)= (A082451(n) - A121362(n) )/2.",
				"G.f.: x^2 * Product_{k\u003e0} (1 - x^k) * (1 - x^(15*k)) * (1 + x^(2*k))^2 * (1 + x^(30*k))^2."
			],
			"example": [
				"G.f. = x^2 + x^3 + x^5 + x^8 + x^12 + 2*x^17 + x^18 + x^20 + 2*x^23 + x^27 + x^30 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 1, 0, DivisorSum[ n, KroneckerSymbol[ -60, #] - KroneckerSymbol[ 20, #] KroneckerSymbol[ -3, n/#] \u0026] / 2]; (* _Michael Somos_, Nov 12 2015 *)",
				"a[ n_] := SeriesCoefficient[ q^2 (QPochhammer[ q^2] QPochhammer[ q^30])^2 / (QPochhammer[ q] QPochhammer[ q^15]), {q, 0, n}]; (* _Michael Somos_, Nov 12 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, 0, sumdiv(n, d, kronecker(-60, d) - kronecker(20, d) * kronecker(-3, n/d) )/2)};",
				"(PARI) {a(n) = my(A); if( n\u003c2, 0, n-=2; A = x * O(x^n); polcoeff( (eta(x^2 + A) * eta(x^30 + A))^2 / (eta(x + A) * eta(x^15 + A)), n))};"
			],
			"xref": [
				"Cf. A035162."
			],
			"keyword": "nonn",
			"offset": "1,17",
			"author": "_Michael Somos_, Mar 13 2007",
			"references": 7,
			"revision": 16,
			"time": "2021-03-12T22:24:44-05:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}