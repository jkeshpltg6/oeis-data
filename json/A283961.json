{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A283961",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 283961,
			"data": "1,2,4,3,6,10,5,8,13,18,7,11,16,22,29,9,14,20,26,34,43,12,17,24,31,39,49,59,15,21,28,36,45,55,66,78,19,25,33,41,51,62,73,86,99,23,30,38,47,57,69,81,94,108,123,27,35,44,53,64,76,89,103,117,133",
			"name": "Rank array, R, of (golden ratio)^2, by antidiagonals.",
			"comment": [
				"Antidiagonals n = 1..60, flattened.",
				"Every row intersperses all other rows, and every column intersperses all other columns.  The array is the dispersion of the complement of column 1, where column 1 is given by c(n) = c(n-1) + 1 + U(n), where U = upper Wythoff sequence (A001950)."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A283961/b283961.txt\"\u003eTable of n, a(n) for n = 1..1829\u003c/a\u003e",
				"Clark Kimberling and John E. Brown, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL7/Kimberling/kimber67.html\"\u003ePartial Complements and Transposable Dispersions\u003c/a\u003e, J. Integer Seqs., Vol. 7, 2004."
			],
			"formula": [
				"R(i,j) = R(i,0) + R(0,j) + i*j - 1, for i\u003e=1, j\u003e=1."
			],
			"example": [
				"Northwest corner of R:",
				"1  2  3  5  7  9  12 15",
				"4  6  8  11 14 17 21 25",
				"10 13 16 20 24 28 33 38",
				"18 22 26 31 36 41 47 53",
				"29 34 39 45 51 57 64 71",
				"43 49 55 62 69 76 84 92",
				"Let t = (golden ratio)^2 = (3 + sqrt(5))/2; then R(i,j) = rank of (j,i) when all nonnegative integer pairs (a,b) are ranked by the relation \u003c\u003c defined as follows: (a,b) \u003c\u003c (c,d) if a + b*t \u003c c + d*t, and also (a,b) \u003c\u003c (c,d) if a + b*t = c + d*t and b \u003c d. Thus R(2,0) = 10 is the rank of (0,2) in the list (0,0) \u003c\u003c (1,0) \u003c\u003c (2,0) \u003c\u003c (0,1) \u003c\u003c (3,0) \u003c\u003c (1,1) \u003c\u003c (4,0) \u003c\u003c (2,1) \u003c\u003c (5,0) \u003c\u003c (0,2).",
				"From _Indranil Ghosh_, Mar 19 2017: (Start)",
				"Triangle formed when the array is read by antidiagonals:",
				"   1;",
				"   2,  4;",
				"   3,  6, 10;",
				"   5,  8, 13, 18;",
				"   7, 11, 16, 22, 29;",
				"   9, 14, 20, 26, 34, 43;",
				"  12, 17, 24, 31, 39, 49, 59;",
				"  15, 21, 28, 36, 45, 55, 66, 78;",
				"  19, 25, 33, 41, 51, 62, 73, 86, 99;",
				"  23, 30, 38, 47, 57, 69, 81, 94, 108, 123;",
				"  ...",
				"(End)"
			],
			"mathematica": [
				"r = GoldenRatio^2; z = 100;",
				"s[0] = 1; s[n_] := s[n] = s[n - 1] + 1 + Floor[n*r];",
				"u = Table[n + 1 + Sum[Floor[(n - k)/r], {k, 0, n}], {n, 0, z}]; (* A283968, row 1 of A283961 *)",
				"v = Table[s[n], {n, 0, z}]; (* A283969, col 1 of A283961 *)",
				"w[i_, j_] := v[[i]] + u[[j]] + (i - 1)*(j - 1) - 1;",
				"Grid[Table[w[i, j], {i, 1, 10}, {j, 1, 10}]] (* A283961 *)",
				"v1 = Flatten[Table[w[k, n - k + 1], {n, 1, 60}, {k, 1, n}]] (* A283961,sequence *)"
			],
			"program": [
				"(PARI)",
				"\\\\ This code produces the triangle mentioned in the example section",
				"r = (3 +sqrt(5))/2;",
				"z = 100;",
				"s(n) = if(n\u003c1, 1, s(n - 1) + 1 + floor(n*r));",
				"p(n) = n + 1 + sum(k=0, n, floor((n - k)/r));",
				"u = v = vector(z + 1);",
				"for(n=1, 101, (v[n] = s(n - 1)));",
				"for(n=1, 101, (u[n] = p(n - 1)));",
				"w(i,j) = v[i] + u[j] + (i - 1) * (j - 1) - 1;",
				"tabl(nn) = {for(n=1, nn, for(k=1, n, print1(w(k, n - k + 1),\", \");); print(););};",
				"tabl(10) \\\\ _Indranil Ghosh_, Mar 19 2017"
			],
			"xref": [
				"Cf. A001622, A118276, A283961, A283968, A283969."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Mar 18 2017",
			"references": 4,
			"revision": 19,
			"time": "2017-03-19T12:32:44-04:00",
			"created": "2017-03-19T01:15:37-04:00"
		}
	]
}