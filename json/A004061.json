{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A004061",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 4061,
			"id": "M2620",
			"data": "3,7,11,13,47,127,149,181,619,929,3407,10949,13241,13873,16519,201359,396413,1888279,3300593",
			"name": "Numbers n such that (5^n - 1)/4 is prime.",
			"comment": [
				"With the addition of the 18th prime in the sequence, the new best linear fit to the sequence has G=0.46271, which is slightly closer to the conjectured limit of G=0.56145948 (see link for Generalized Repunit Conjecture). [_Paul Bourdelais_, Apr 30 2018]"
			],
			"reference": [
				"J. Brillhart et al., Factorizations of b^n +- 1. Contemporary Mathematics, Vol. 22, Amer. Math. Soc., Providence, RI, 2nd edition, 1985; and later supplements.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Paul Bourdelais, \u003ca href=\"https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;417ab0d6.0906\"\u003eA Generalized Repunit Conjecture\u003c/a\u003e [From _Paul Bourdelais_, Jun 01 2010]",
				"J. Brillhart et al., \u003ca href=\"http://dx.doi.org/10.1090/conm/022\"\u003eFactorizations of b^n +- 1\u003c/a\u003e, Contemporary Mathematics, Vol. 22, Amer. Math. Soc., Providence, RI, 3rd edition, 2002.",
				"H. Dubner, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1993-1185243-9\"\u003eGeneralized repunit primes\u003c/a\u003e, Math. Comp., 61 (1993), 927-930.",
				"H. Dubner, \u003ca href=\"/A028491/a028491.pdf\"\u003eGeneralized repunit primes\u003c/a\u003e, Math. Comp., 61 (1993), 927-930. [Annotated scanned copy]",
				"H. Lifchitz, \u003ca href=\"http://www.primenumbers.net/Henri/us/MersFermus.htm\"\u003eMersenne and Fermat primes field\u003c/a\u003e",
				"S. S. Wagstaff, Jr., \u003ca href=\"http://www.cerias.purdue.edu/homes/ssw/cun/index.html\"\u003eThe Cunningham Project\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Repunit.html\"\u003eRepunit\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#primepop\"\u003eIndex to primes in various ranges\u003c/a\u003e, form ((k+1)^n-1)/k"
			],
			"mathematica": [
				"lst={};Do[If[PrimeQ[(5^n-1)/4], AppendTo[lst, n]], {n, 10^4}];lst (* _Vladimir Joseph Stephan Orlovsky_, Aug 20 2008 *)"
			],
			"program": [
				"(PARI) forprime(p=2,1e4,if(ispseudoprime(5^p\\4),print1(p\", \"))) \\\\ _Charles R Greathouse IV_, Jul 15 2011"
			],
			"keyword": "hard,nonn",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_.",
			"ext": [
				"a(13)-a(15) from Kamil Duszenko (kdusz(AT)wp.pl), Mar 25 2003",
				"a(16) corresponds to a probable prime based on trial factoring to 4*10^13 and Fermat primality testing base 2. - _Paul Bourdelais_, Dec 11 2008",
				"a(17) corresponds to a probable prime discovered by _Paul Bourdelais_, Jun 01 2010",
				"a(18) corresponds to a probable prime discovered by _Paul Bourdelais_, Apr 30 2018",
				"a(19) corresponds to a probable prime discovered by _Ryan Propper_, Jan 02 2022"
			],
			"references": 38,
			"revision": 61,
			"time": "2022-01-03T15:06:55-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}