{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A034691",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 34691,
			"data": "1,1,3,7,18,42,104,244,585,1373,3233,7533,17547,40591,93711,215379,493735,1127979,2570519,5841443,13243599,29953851,67604035,152258271,342253980,767895424,1719854346,3845443858",
			"name": "Euler transform of powers of 2 [1,2,4,8,16,...].",
			"comment": [
				"This is the number of different hierarchical orderings that can be formed from n unlabeled elements: these are divided into groups and the elements in each group are then arranged in an \"unlabeled preferential arrangement\" or \"composition\" as in A000079. - _Thomas Wieder_ and _N. J. A. Sloane_, Jun 10 2003",
				"From _Gus Wiseman_, Mar 03 2016: (Start)",
				"The original Sloane-Wieder definition, \"To obtain a hierarchical ordering we partition the elements into unlabeled nonempty subsets and form a composition of each subset,\" [arXiv:math/0307064] has two possible meanings. The first possible meaning is that we should (1) choose a set partition pi of {1...n} and (2) for each block of pi choose a composition of the number of elements. In this case the correct number of such structures would evidently be counted by A004211 which differs from a(n) for n \u003e 2.",
				"The other possible meaning is that after we have done (1) and (2) above we (3) \"forget\" the choice of pi. We will have produced a collection M of multisets of compositions. The span of M (its set of distinct elements) is correctly counted by A034691 and it seems that non-isomorphic hierarchical orderings of unlabeled sets are nothing more than multisets of compositions. This discovery is due to Wieder. (End)",
				"The asymptotic formula in the article by N. J. A. Sloane and Thomas Wieder, \"The Number of Hierarchical Orderings\" (Theorem 3) is incorrect (a multiplicative factor of 1.397... is missing, see my formula below). - _Vaclav Kotesovec_, Sep 08 2014",
				"Number of partitions of n into 1 sort of 1, 2 sorts of 2's, 4 sorts of 3's, ..., 2^(k-1) sorts of k's, ... . - _Joerg Arndt_, Sep 09 2014",
				"Also number of normal multiset partitions of weight n, where a multiset is normal if it spans an initial interval of positive integers. - _Gus Wiseman_, Mar 03 2016"
			],
			"link": [
				"T. D. Noe and Vaclav Kotesovec, \u003ca href=\"/A034691/b034691.txt\"\u003eTable of n, a(n) for n = 0..3190\u003c/a\u003e (first 300 terms from T. D. Noe)",
				"Vaclav Kotesovec, \u003ca href=\"/A034691/a034691_1.pdf\"\u003eAsymptotics of sequence A034691\u003c/a\u003e",
				"Vaclav Kotesovec, \u003ca href=\"http://arxiv.org/abs/1508.01796\"\u003eAsymptotics of the Euler transform of Fibonacci numbers\u003c/a\u003e, arXiv:1508.01796 [math.CO], Aug 07 2015",
				"N. J. A. Sloane and Thomas Wieder, \u003ca href=\"http://arXiv.org/abs/math.CO/0307064\"\u003eThe Number of Hierarchical Orderings\u003c/a\u003e, Order 21 (2004), 83-89.",
				"Thomas Wieder, \u003ca href=\"/A034691/a034691_1.txt\"\u003eAn explicit formula for the n-th term\u003c/a\u003e",
				"Thomas Wieder, \u003ca href=\"http://www.m-hikari.com/ams/ams-password-2009/ams-password53-56-2009/wiederAMS53-56-2009.pdf\"\u003eThe number of certain rankings and hierarchies formed from labeled or unlabeled elements and sets\u003c/a\u003e, Applied Mathematical Sciences, vol. 3, 2009, no. 55, 2707 - 2724. [From _Thomas Wieder_, Nov 14 2009]"
			],
			"formula": [
				"G.f.: 1 / Product_{n\u003e=1} (1-x^n)^(2^(n-1)).",
				"Recurrence: a(n) = (1/n) * Sum_{m=1..n} a(n-m)*c(m) where c(m) = A083413(m).",
				"a(n) ~ c * 2^n * exp(sqrt(2*n)) / (sqrt(2*Pi) * exp(1/4) * 2^(3/4) * n^(3/4)), where c = exp( Sum_{k\u003e=2} 1/(k*(2^k-2)) ) = 1.3976490050836502... (see A247003). - _Vaclav Kotesovec_, Sep 08 2014"
			],
			"example": [
				"The normal multiset partitions for a(4) = 18: {{1111},{1222},{1122},{1112},{1233},{1223},{1123},{1234},{1,111},{1,122},{1,112},{1,123},{11,11},{11,12},{12,12},{1,1,11},{1,1,12},{1,1,1,1}}"
			],
			"maple": [
				"oo := 101: mul( 1/(1-x^j)^(2^(j-1)),j=1..oo): series(%,x,oo): t1 := seriestolist(%); A034691 := n-\u003e t1[n+1];",
				"with(combstruct); SetSeqSetU := [T, {T=Set(S), S=Sequence(U,card \u003e= 1), U=Set(Z,card \u003e=1)},unlabeled]; seq(count(SetSeqSetU,size=j),j=1..12);"
			],
			"mathematica": [
				"nn = 30; b = Table[2^n, {n, 0, nn}]; CoefficientList[Series[Product[1/(1 - x^m)^b[[m]], {m, nn}], {x, 0, nn}],  x] (* _T. D. Noe_, Nov 21 2011 *)",
				"Table[SeriesCoefficient[E^(Sum[x^k/(1 - 2*x^k)/k, {k, 1, n}]), {x, 0, n}], {n, 0, 30}] (* _Vaclav Kotesovec_, Sep 08 2014 *)",
				"allnorm[n_Integer]:=Function[s,Array[Count[s,y_/;y\u003c=#]+1\u0026,n]]/@Subsets[Range[n-1]+1];",
				"allnmsp[0]={};allnmsp[1]={{{1}}};allnmsp[n_Integer]:=allnmsp[n]=Join[allnmsp[n-1],List/@allnorm[n],Join@@Function[ptn,Append[ptn,#]\u0026/@Select[allnorm[n-Length[Join@@ptn]],OrderedQ[{Last[ptn],#}]\u0026]]/@allnmsp[n-1]];",
				"Apply[SequenceForm,Select[allnmsp[4],Length[Join@@#]===4\u0026],{2}] (* to construct the example *)",
				"Table[Length[Complement[allnmsp[n],allnmsp[n-1]]],{n,1,8}] (* _Gus Wiseman_, Mar 03 2016 *)"
			],
			"program": [
				"(PARI) A034691(n,l=1+O('x^(n+1)))={polcoeff(1/prod(k=1,n,(l-'x^k)^2^(k-1)),n)} \\\\ _Michael Somos_, Nov 21 2011, edited by _M. F. Hasler_, Jul 24 2017",
				"(SageMath) # uses[EulerTransform from A166861]",
				"a = BinaryRecurrenceSequence(2, 0)",
				"b = EulerTransform(a)",
				"print([b(n) for n in range(30)]) # _Peter Luschny_, Nov 11 2020"
			],
			"xref": [
				"Cf. A034899, A075729, A247003, A004211, A104500 (Euler transform), A290222 (Multiset transform)."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_.",
			"references": 123,
			"revision": 76,
			"time": "2020-11-11T05:57:19-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}