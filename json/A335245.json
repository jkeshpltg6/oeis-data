{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A335245",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 335245,
			"data": "1,1,6,1,0,7,7,7,5,1,0,3,2,8,3,1,8,1,8,6,0,7,5,9,4,9,1,9,6,2,9,0,8,2,2,9,9,9,2,6,8,7,9,6,2,1,8,2,4,3,8,5,5,2,3,6,9,5,8,6,5,6,1,8,3,4,7,1,2,1,1,7,6,1,8,1,2,7,2,0,3,6,8,1,9,0,3,9,9,0,5,2,4,5,8",
			"name": "Given the two curves y = exp(-x) and y = 2/(exp(x) + exp(x/2)), draw a line tangent to both. This sequence is the decimal expansion of the y-coordinate of the point at which the line touches y = 2/(exp(x) + exp(x/2)).",
			"reference": [
				"Steven R. Finch, Mathematical Constants, Cambridge University Press, 2003, Section 3.1, Shapiro-Drinfeld Constant, p. 209."
			],
			"link": [
				"V. G. Drinfel'd, \u003ca href=\"https://doi.org/10.1007/BF01316982\"\u003eA cyclic inequality\u003c/a\u003e, Mathematical Notes of the Academy of Sciences of the USSR, 9 (1971), 68-71.",
				"Petros Hadjicostas, \u003ca href=\"/A086277/a086277.pdf\"\u003ePlot of the curves y = exp(-x) and y = 2/(exp(x) + exp(x/2)) and their common tangent\u003c/a\u003e, 2020.",
				"R. A. Rankin, \u003ca href=\"https://doi.org/10.2307/3608356\"\u003e2743. An inequality\u003c/a\u003e, Mathematical Gazette, 42(339) (1958), 39-40.",
				"B. A. Troesch, \u003ca href=\"https://doi.org/10.1090/S0025-5718-1989-0983563-0\"\u003eThe validity of Shapiro's cyclic inequality\u003c/a\u003e, Mathematics of Computation, 53 (1989), 657-664.",
				"Eric Weisstein's MathWorld, \u003ca href=\"http://mathworld.wolfram.com/ShapirosCyclicSumConstant.html\"\u003eShapiro's Cyclic Sum Constant\u003c/a\u003e."
			],
			"formula": [
				"Equals 2/(exp(b) + exp(b/2)), where b = -A319568.",
				"Slope of common tangent = (A335339 - A335245)/(A319569 - (-A319568)) = (exp(-c) - 2/(exp(b) + exp(b/2))) / (c - b) = -A335339 = -exp(-c)."
			],
			"example": [
				"1.1610777510328318186075949..."
			],
			"program": [
				"(PARI) c(b) = b + exp(b/2)/(2*exp(b)+exp(b/2));",
				"y(b) = 2/(exp(b) + exp(b/2));",
				"a=solve(b=-2, 2, exp(-c(b))*(1-b+c(b))-2/(exp(b)+exp(b/2)));",
				"y(a)"
			],
			"xref": [
				"Cf. A086277, A245330, A319568 (negative of x-coordinate), A319569 (x-coordinate at other curve), A335339 (y-coordinate at other curve)."
			],
			"keyword": "nonn,cons",
			"offset": "1,3",
			"author": "_Petros Hadjicostas_, Jun 02 2020",
			"references": 2,
			"revision": 52,
			"time": "2020-06-23T09:00:12-04:00",
			"created": "2020-06-04T03:28:57-04:00"
		}
	]
}