{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A120572",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 120572,
			"data": "6,24,48,84,150,192,294,336,432,540,726,756,1014,1134,1170,1344,1734,1710,2166,2100,2310,2640,3174,3000,3750,4056,3888,4116,5046,4680,5766,5376,5808,6936,6510,6804,8214,8664,8112,8400,10086,9240,11094,10164",
			"name": "Smallest area of any triangle with integer sides a \u003c= b \u003c= c and inradius n.",
			"comment": [
				"a(n) == 0 (mod 6).",
				"Empirically, 3*sqrt(3) \u003c a(n)/n^2 \u003c= 6. The lower bound is provably tight, the upper bound seems to be achieved infinitely often, e.g., for prime n \u003e= 5.",
				"From _Michel Lagneau_, Mar 02 2012: (Start)",
				"Subset of A188158.",
				"The area A of a triangle whose sides have lengths a, b, and c is given by Heron's formula: A = sqrt(s(s-a)(s-b)(s-c)), where s = (a+b+c)/2. The radius of the incircle or inscribed circle (also known as the inradius, r) is given by r = A/s.",
				"From n = 17, it is possible to find couples of triangles with the property: r1 \u003e r2 and A1 \u003c A2 where A1, A2 are the consecutive areas corresponding to the inradius r1, r2. For example, a(17) = 1734 with (a,b,c) = (51, 68, 85) and a(18) = 1710 with (a,b,c) = (57, 65, 68).",
				"Another interesting property of this sequence is that a(n) is divisible by 6 and, except n = 3, a(n)/6 = n^2 if n prime, hence the proposition:",
				"Among the set of triangles whose area and sides are integers and whose inradius r is a prime number other than 3, the smallest area A is given by A = 6r^2.",
				"Example: if r = 5, the areas of the triangles are {150, 210, 270, 330, 390, 510, ...} and the smallest area of them is A = 6*5^2 = 150 because 5 is prime.",
				"Proof: Let r be a number such that the sides of a triangle are a = 3r, b = 4r, c = 5r. Then s = (a+b+c)/2 = 6r and A = sqrt(s(s-a)(s-b)(s-c) = sqrt(36r^4) = 6r^2 is a possible area. Is 6r^2 the smallest area? The response is no in the general case for the composite numbers.",
				"Writing a = (m+n)/2, b = (n+l)/2, c = (l+m)/2, and using rs = A and Heron's formula for A, we find lmn = 4r^2(l+m+n). Since m, n and l have to be of the same parity for a, b and c to be integral, they must therefore be even. Setting l = 2u, m = 2v, and n = 2w, we have a = v+w, b = w+u, c = u+v, and uvw = r^2(u+v+w). Then r^2 = uvw/(u+v+w).",
				"First case: If r = p is prime, we prove that A = 6p^2 is the smallest area of all the triangles whose inradius is p. Suppose A' \u003c A with inradius(A') = p. The area A is the corresponding value of the triangle (u,v,w) = (1*p, 2*p, 3*p) because 6p^3/6p = p^2. However, inradius(A') = p =\u003e u'v'w'/(u'+v'+w') = p^2 =\u003e (u',v',w') = (u,v,w) and A is the smallest area.",
				"Second case: If r = q is composite, the triangle (u,v,w) = (1*q, 2*q, 3*q) gives an area A with inradius(A) = q, but it is possible to find A' \u003c A with inradius(A') = q; for example, if q = 10, the triangle (u,v,w) = (30, 20, 10) whose area is A = 600 gives sqrt{(30*20*10)/(30+20+10)} = sqrt(6000/60) = 10 and the triangle(u',v',w') = (24,15,15) whose area is A' = 540 gives sqrt{(24*15*15)/(24+15+15)} = sqrt(5400/54) = 10.",
				"(End)"
			],
			"link": [
				"David W. Wilson, \u003ca href=\"/A120572/b120572.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Mohammad K. Azarian, \u003ca href=\"http://www.jstor.org/stable/25678790\"\u003eSolution of problem 125: Circumradius and Inradius\u003c/a\u003e, Math Horizons, Vol. 16, No. 2 (Nov. 2008), p. 32."
			],
			"example": [
				"a(4) = 84 because, for (a,b,c) = (13,14,15) =\u003e A = sqrt(21(21-13)(21-14)(21-15)) = 84 and r = 84/21 = 4."
			],
			"maple": [
				"T:=array(1..500):nn:=70: for n from 1 to 16 do:k:=0:ii:=0:for a from 1",
				"to nn do: for b from a to nn  do: for c from b to nn  do: p:=(a+b+c)/2 : x:=p*(p-a)*(p-b)*(p-c): if x\u003e0 then s:=sqrt(x) :if s=floor(s) and s/p = n then k:=k+1:T[k]:=s: else fi:fi:od:od:od: L := [seq(T[i], i=1..k)]:A:=sort(L, `\u003c`): w:=A[1]: printf ( \"%d %d \\n\", n, w):od: # _Michel Lagneau_, Mar 02 2012"
			],
			"xref": [
				"See A120062 for sequences related to integer-sided triangles with integer inradius n."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_David W. Wilson_, Jun 17 2006",
			"references": 7,
			"revision": 34,
			"time": "2017-09-09T20:14:58-04:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}