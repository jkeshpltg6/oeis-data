{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A211391",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 211391,
			"data": "0,0,0,0,0,0,2,2,2,6,4,15,15,24,29,33,63,55,126,117,110,103,225,212,288,282,319,428,504,774,859,943,924,1336,1307,1681,1869,2097,2067,2866,3342,3487,5612,5567,5513,5549,9287,9220,11594,11524,11481,11403,18690",
			"name": "The number of divisors d of n! such that d \u003c A000793(n) (Landau's function g(n)) and the symmetric group S_n contains no elements of order d.",
			"comment": [
				"This sequence gives the number of divisors d of |S_n| such that d \u003c Lambda(n) (where Lambda(n) = the largest order of an element in S_n) for which S_n contains no element of order d.  These divisors constitute a set of 'missing' element orders of S_n.",
				"For computational purposes, the smallest divisor d0(n) of n! = |S_n| for which S_n has no element of order d0(n) is the smallest divisor of n! which is not the least common multiple of an integer partition of n.  Thus d0(n) is given by the smallest prime power \u003e= n+1 that is not prime (with the exception of n = 3 and 4, for which d0(n) = 6)."
			],
			"example": [
				"For n = 7, we refer to the following table:",
				"Symmetric Group on 7 letters.",
				"  # of elements of order  1 -\u003e    1",
				"  # of elements of order  2 -\u003e  231",
				"  # of elements of order  3 -\u003e  350",
				"  # of elements of order  4 -\u003e  840",
				"  # of elements of order  5 -\u003e  504",
				"  # of elements of order  6 -\u003e 1470",
				"  # of elements of order  7 -\u003e  720",
				"  # of elements of order  8 -\u003e    0",
				"  # of elements of order  9 -\u003e    0",
				"  # of elements of order 10 -\u003e  504",
				"  # of elements of order 12 -\u003e  420",
				"  (All other divisors of 7! -\u003e 0.)",
				"So there are two missing element orders in S_7, whence a(7) = 2."
			],
			"program": [
				"(MAGMA)",
				"for n in [1..25] do",
				"D := Set(Divisors(Factorial(n)));",
				"O := { LCM(s) : s in Partitions(n) };",
				"L := Max(O);",
				"N := D diff O;",
				"#{ n : n in N | n lt L };",
				"end for;"
			],
			"xref": [
				"d0(n) is equal to A167184(n) for n \u003e= 5.",
				"Cf. A000793 (Landau's function g(n)), A057731, A211392."
			],
			"keyword": "nonn",
			"offset": "1,7",
			"author": "_Alexander Gruber_, Feb 07 2013",
			"ext": [
				"More terms from _Alois P. Heinz_, Feb 11 2013"
			],
			"references": 1,
			"revision": 27,
			"time": "2017-03-24T00:47:54-04:00",
			"created": "2013-02-15T03:51:40-05:00"
		}
	]
}