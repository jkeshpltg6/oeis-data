{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A060280",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 60280,
			"data": "1,0,1,1,2,2,4,5,8,11,18,25,40,58,90,135,210,316,492,750,1164,1791,2786,4305,6710,10420,16264,25350,39650,61967,97108,152145,238818,374955,589520,927200,1459960,2299854,3626200,5720274,9030450,14263078,22542396",
			"name": "Number of orbits of length n under the map whose periodic points are counted by A001350.",
			"comment": [
				"Euler transform is A000045. 1/((1-x)*(1-x^3)*(1-x^4)*(1-x^5)^2*(1-x^6)^2*...) = 1 + x + x^2 + 2*x^3 + 3*x^4 + 5*x^5 + 8*x^6 + ... - _Michael Somos_, Jan 28 2003"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A060280/b060280.txt\"\u003eTable of n, a(n) for n = 1..4000\u003c/a\u003e",
				"Michael Baake, Joachim Hermisson, and Peter Pleasants, \u003ca href=\"http://dx.doi.org/10.1088/0305-4470/30/9/016\"\u003eThe torus parametrization of quasiperiodic LI-classes\u003c/a\u003e J. Phys. A 30 (1997), no. 9, 3029-3056.",
				"Michael Baake, John A.G. Roberts, and Alfred Weiss, \u003ca href=\"http://arxiv.org/abs/0808.3489\"\u003ePeriodic orbits of linear endomorphisms on the 2-torus and its lattices\u003c/a\u003e, arXiv:0808.3489 [math.DS], Aug 26, 2008. [_Jonathan Vos Post_, Aug 27 2008]",
				"Larry Ericksen, \u003ca href=\"http://siauliaims.su.lt/index.php?option=com_content\u0026amp;view=article\u0026amp;id=44\u0026amp;Itemid=9\"\u003ePrimality Testing and Prime Constellations\u003c/a\u003e, Šiauliai Mathematical Seminar, Vol. 3 (11), 2008. Mentions this sequence.",
				"N. Neumarker, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL12/\"\u003eRealizability of Integer Sequences as Differences of Fixed Point Count Sequences\u003c/a\u003e, JIS 12 (2009) 09.4.5.",
				"Y. Puri and T. Ward, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL4/WARD/short.html\"\u003eArithmetic and growth of periodic orbits\u003c/a\u003e, J. Integer Seqs., Vol. 4 (2001), #01.2.1.",
				"T. Ward, \u003ca href=\"http://www.mth.uea.ac.uk/~h720/research/files/integersequences.html\"\u003eExactly realizable sequences\u003c/a\u003e"
			],
			"formula": [
				"a(n) = (1/n)* Sum_{d|n} mu(d)*A001350(n/d).",
				"a(n) = A006206(n) except for n=2. - _Michael Somos_, Jan 28 2003",
				"a(n) = A031367(n)/n. - _R. J. Mathar_, Jul 15 2016",
				"G.f.: Sum_{k\u003e=1} mu(k)*log(1 + x^k/(1 - x^k - x^(2*k)))/k. - _Ilya Gutkovskiy_, May 18 2019"
			],
			"example": [
				"a(7)=4 since the 7th term of A001350 is 29 and the 1st is 1, so there are (29-1)/7 = 4 orbits of length 7.",
				"x + x^3 + x^4 + 2*x^5 + 2*x^6 + 4*x^7 + 5*x^8 + 8*x^9 + 11*x^10 + 18*x^11 + ..."
			],
			"maple": [
				"A060280 := proc(n)",
				"    add( numtheory[mobius](d)*A001350(n/d), d=numtheory[divisors](n)) ;",
				"    %/n;",
				"end proc: # _R. J. Mathar_, Jul 15 2016"
			],
			"mathematica": [
				"a001350[n_] := Fibonacci[n - 1] + Fibonacci[n + 1] - (-1)^n - 1;",
				"a[n_] := (1/n)*DivisorSum[n, MoebiusMu[#]*a001350[n/#]\u0026 ];",
				"Array[a, 30] (* _Jean-François Alcover_, Nov 23 2017 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c3, n==1, sumdiv( n, d, moebius(n/d) * (fibonacci(d - 1) + fibonacci(d + 1))) / n)} /* _Michael Somos_, Jan 28 2003 */"
			],
			"xref": [
				"Cf. A000045, A001350, A006206, A324485, A324489. First column of A348422."
			],
			"keyword": "easy,nonn",
			"offset": "1,5",
			"author": "_Thomas Ward_, Mar 29 2001",
			"references": 10,
			"revision": 48,
			"time": "2021-10-18T06:46:25-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}