{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A188440",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 188440,
			"data": "1,1,1,2,1,2,1,4,4,1,4,4,1,6,12,8,1,6,12,8,1,8,24,32,16,1,8,24,32,16,1,10,40,80,80,32,1,10,40,80,80,32,1,12,60,160,240,192,64,1,12,60,160,240,192,64,1,14,84,280,560,672,448,128,1,14,84,280",
			"name": "Triangle T(n,k) read by rows: number of size-k antisymmetric subsets of {1,2,...,n}.",
			"comment": [
				"A subset S of {1,2,...,n} is antisymmetric if x is an element of S implies n+1-x is not an element of S. In other words, the sum of any two elements of S does not equal n+1. For example, {1,2,5} is an antisymmetric subset of {1,2,3,4,5,6,7}. If n is odd, (n+1)/2 cannot be an element of an antisymmetric subset of {1,2,...,n}. (Note that for n=0, we define {1,...,n} to be the empty set, and thus T(0,0)=1 since the empty set is vacuously antisymmetric.)",
				"We note, for example, that T(100,k) provides the number of possible size-k committees of the U.S. Senate in which no two members are from the same state.",
				"Triangle, read by rows, A013609 rows repeated. - _Philippe Deléham_, Apr 09 2012",
				"Triangle, with zeros omitted, given by (1, 0, -1, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (0, 2, -2, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Apr 09 2012"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A188440/b188440.txt\"\u003eRows n = 0..100, flattened\u003c/a\u003e",
				"Dennis Walsh, \u003ca href=\"http://frank.mtsu.edu/~dwalsh/ANTISYMM.pdf\"\u003eNotes on antisymmetric subsets of {1,2,...,n}\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = 2^k*C(floor(n/2),k) where C(*,*) denotes a binomial coefficient.",
				"Sum(T(n,k),k=0..floor(n/2)) = 3^floor(n/2) = A108411(n).",
				"G.f. for columns(k fixed):(2t^2)^k/((1-t)*(1-t^2)^k).",
				"T(n,k) = A152198(n,k)*2^k. - _Philippe Deléham_, Apr 09 2012",
				"G.f.: (1+x)/(1-x^2-2*y*x^2). - _Philippe Deléham_, Apr 09 2012",
				"T(n,k) = T(n-2,k) + 2*T(n-2,k-1), T(0,0) = T(1,0) = 1, T(1,1) = 0 and T(n,k) = 0 if k\u003c0 or if k\u003en.- _Philippe Deléham_, Apr 09 2012"
			],
			"example": [
				"Triangle T(n,k) initial values 0 \u003c= k \u003c= floor(n/2), n=0..13:",
				"  1",
				"  1",
				"  1   2",
				"  1   2",
				"  1   4   4",
				"  1   4   4",
				"  1   6  12   8",
				"  1   6  12   8",
				"  1   8  24  32  16",
				"  1   8  24  32  16",
				"  1  10  40  80  80  32",
				"  1  10  40  80  80  32",
				"  1  12  60 160 240 192  64",
				"  1  12  60 160 240 192  64",
				"  ...",
				"For n=7 and k=2, T(7,2)=12 since there are 12 antisymmetric size-2 subsets of {1,2,...,7}:",
				"  {1,2}, {1,3}, {1,5}, {1,6}, {2,3}, {2,5},",
				"  {2,7}, {3,6}, {3,7}, {5,6}, {5,7}, and {6,7}.",
				"(1, 0, -1, 0, 0, 0, 0, ...) DELTA (0, 2, -2, 0, 0, 0, 0, ...) begins:",
				"  1",
				"  1   0",
				"  1   2   0",
				"  1   2   0   0",
				"  1   4   4   0   0",
				"  1   4   4   0   0   0",
				"  1   6  12   8   0   0   0",
				"  1   6  12   8   0   0   0   0",
				"  1   8  24  32  16   0   0   0   0",
				"  1   8  24  32  16   0   0   0   0   0",
				"  1  10  40  80  80  32   0   0   0   0   0",
				"  1  10  40  80  80  32   0   0   0   0   0   0",
				"  1  12  60 160 240 192  64   0   0   0   0   0   0",
				"  1  12  60 160 240 192  64   0   0   0   0   0   0   0",
				"- _Philippe Deléham_, Apr 09 2012"
			],
			"maple": [
				"seq(seq(binomial(floor(n/2),k)*2^k,k=0..floor(n/2)),n=0..22);"
			],
			"mathematica": [
				"Table[ CoefficientList[(1 + 2*x)^n, x] , {n, 0, 7}, {2}] // Flatten (* _Jean-François Alcover_, Aug 19 2013, after _Philippe Deléham_ *)"
			],
			"xref": [
				"Cf. A108411, row sums of triangle T(n,k).",
				"Cf. A000079, A007318, A013109, A152198"
			],
			"keyword": "nice,easy,nonn,tabf",
			"offset": "0,4",
			"author": "_Dennis P. Walsh_, Mar 31 2011",
			"references": 2,
			"revision": 25,
			"time": "2015-12-13T07:17:20-05:00",
			"created": "2011-04-01T21:24:35-04:00"
		}
	]
}