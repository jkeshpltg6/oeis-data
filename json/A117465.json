{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A117465",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 117465,
			"data": "9,0,15,0,105,24,945,120,3465,360,9009,840,19305,1680,36465,3024,62985,5040,101745,7920,156009,11880,229425,17160,326025,24024,450225,32760,606825,43680,801009,57120,1038345,73440,1324785,93024,1666665,116280",
			"name": "Denominator of -16/((n+2)*n*(n-2)*(n-4)).",
			"comment": [
				"I came up with the equation to help analyze the path to stable orbits of the logistic function",
				"f(n+1) = k*n(1-n) for f(n) with n =\u003e 9, then f(n)*A072346(n-5) = A072346(n+3).",
				"a(n) is the denominator of f(n). The numerator of f(n) is -1 if n is even, else -16."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A117465/b117465.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_10\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,5,0,-10,0,10,0,-5,0,1)."
			],
			"formula": [
				"a(n) = denominator of the reduced -16/(n*(n-2)*(n+2)*(n-4)).",
				"a(2n) = A052762(n+1).",
				"a(n) = 5*a(n-2) -10*a(n-4) +10*a(n-6) -5*a(n-8) +a(n-10) for n\u003e15. - _R. J. Mathar_, Mar 27 2010",
				"a(n) = -(-17+15*(-1)^n)*(n*(16-4*n-4*n^2+n^3))/32 for n\u003e3. - _Colin Barker_, Nov 11 2014",
				"G.f.: 3*x*(10*x^12-50*x^10+105*x^8-160*x^6-8*x^5-40*x^4+10*x^2-3) / ((x-1)^5*(x+1)^5). - _Colin Barker_, Nov 11 2014"
			],
			"example": [
				"f(5) = -16/(7*5*3*1) = -16/105, denominator a(5) = 105.",
				"f(6) = -16/(8*6*4*2) = -1/24, denominator a(6) = 24."
			],
			"maple": [
				"f(n) := n -\u003e (1/((n/4)+(n^2/4)-(n^3/16)-1))/n;"
			],
			"mathematica": [
				"Join[{9,0,15,0},Denominator[Table[-(16/(n (n^3-4 n^2-4 n+16))), {n,5,40}]]]    (* _Harvey P. Dale_, Nov 06 2011 *)"
			],
			"program": [
				"(PARI) Vec(3*x*(10*x^12-50*x^10+105*x^8-160*x^6-8*x^5-40*x^4+10*x^2-3)/((x-1)^5*(x+1)^5) + O(x^100)) \\\\ _Colin Barker_, Nov 11 2014"
			],
			"xref": [
				"Cf. A052762, A072346."
			],
			"keyword": "frac,nonn,easy",
			"offset": "1,1",
			"author": "_Steven J. Forsberg_, Apr 25 2006",
			"ext": [
				"Clearer definition from _R. J. Mathar_, Mar 27 2010"
			],
			"references": 1,
			"revision": 14,
			"time": "2015-06-13T00:52:05-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}