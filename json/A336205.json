{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A336205",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 336205,
			"data": "0,1,2,3,6,7,8,9,10,15,16,17,18,19,20,24,25,26,27,28,29,34,35,36,37,38,43,45,46,48,53,54,55,56,57,60,61,62,63,64,65,66,69,71,72,73,80,81,83,88,90,91,92,97,98,99,100,101,106,109,116,117,118,119,120,123,124,125,126,127,128,129,132",
			"name": "Numbers k that can be expressed as x^3 + y^3 + z^3 with x^2 + y^2 + z^2 \u003c= k where x, y, z are integers.",
			"comment": [
				"See A336240 for border case x^2 + y^2 + z^2 = x^3 + y^3 + z^3.",
				"What is the natural density of this sequence?",
				"There are infinitely many infinite parametric families of solutions which have negative values in (x,y,z). For example, 8*(3*a-1)^2*m^6 + 12*(3*a-1)*(a-1)*m^4 - 6*(2*a-1)*m^2 + 2*a^3 + 1 are terms for all a \u003e= 0, m \u003e= 0. (x = 1 - (6*a-2)*m^2, y = a - m*(1-(6*a-2)*m^2), z = a + m*(1-(6*a-2)*m^2)). - _Altug Alkan_, Jul 17 2020",
				"By definition, corresponding (x,y,z) variables are produced by equation x^3 + y^3 + z^3 = x^2 + y^2 + z^2 + t with t \u003e= 0. That is, x^2*(x-1) + y^2*(y-1) + z^2*(z-1) \u003e= 0. Conjecture: Every even integer can be represented as x^2*(x-1) + y^2*(y-1) + z^2*(z-1) where x, y, z are integers. - _Altug Alkan_, Jul 19 2020"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A336205/b336205.txt\"\u003eTable of n, a(n) for n = 1..8000\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A336205/a336205.txt\"\u003eC program for A336205\u003c/a\u003e"
			],
			"example": [
				"11 is not a term because there is no (x,y,z) with x^2 + y^2 + z^2 \u003c= 11 when x^3 + y^3 + z^3 = 11.",
				"18 is a term because (-1)^3 + (-2)^3 + 3^3 = 18 and (-1)^2 + (-2)^2 + 3^2 \u003c= 18.",
				"61 is a term because (-4)^3 + 0^3 + 5^3 = 61 and (-4)^2 + 0^2 + 5^2 \u003c= 61.",
				"354 is a term because (-11)^3 + (-8)^3 + 13^3 = (-11)^2 + (-8)^2 + 13^2 = 354."
			],
			"maple": [
				"filter:= proc(n) local x,y,z,e1,e2;",
				"  for x from 0 while 3*x^2 \u003c= n do",
				"    for y from 0 while x^2 + 2*y^2 \u003c= n do",
				"      for e1 in [-1,1] do for e2 in [-1,1] do",
				"        z:= surd(n + e1*x^3 + e2*y^3,3);",
				"        if z::integer and x^2 + y^2 + z^2 \u003c= n then return true fi;",
				"  od od od od;",
				"  false",
				"end proc:",
				"select(filter, [$0..200]); # _Robert Israel_, Jul 12 2020"
			],
			"program": [
				"(C) See Links section."
			],
			"xref": [
				"Cf. A004825 (subsequence), A060464 (supersequence), A336240."
			],
			"keyword": "nonn,easy",
			"offset": "1,3",
			"author": "_Altug Alkan_, Jul 12 2020",
			"references": 3,
			"revision": 61,
			"time": "2020-07-20T01:55:31-04:00",
			"created": "2020-07-13T21:25:44-04:00"
		}
	]
}