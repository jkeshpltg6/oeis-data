{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006077",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6077,
			"id": "M2775",
			"data": "1,3,9,21,9,-297,-2421,-12933,-52407,-145293,-35091,2954097,25228971,142080669,602217261,1724917221,283305033,-38852066421,-337425235479,-1938308236731,-8364863310291,-24286959061533,-3011589296289,574023003011199,5028616107443691",
			"name": "(n+1)^2*a(n+1) = (9n^2+9n+3)*a(n) - 27*n^2*a(n-1), with a(0) = 1 and a(1) = 3.",
			"comment": [
				"This is the Taylor expansion of a special point on a curve described by Beauville. - _Matthijs Coster_, Apr 28 2004",
				"Conjecture: Let W(n) be the (n+1) X (n+1) Hankel-type determinant with (i,j)-entry equal to a(i+j) for all i,j = 0,...,n. If n == 1 (mod 3) then W(n) = 0. When n == 0 or 2 (mod 3), W(n)*(-1)^(floor((n+1)/3))/6^n is always a positive odd integer. - _Zhi-Wei Sun_, Aug 21 2013",
				"Conjecture: Let p == 1 (mod 3) be a prime, and write 4*p = x^2 + 27*y^2 with x, y integers and x == 1 (mod 3). Then W(p-1) == (-1)^{(p+1)/2}*(x-p/x) (mod p^2), where W(n) is defined as the above. - _Zhi-Wei Sun_, Aug 23 2013",
				"This is one of the Apery-like sequences - see Cross-references. - _Hugo Pfoertner_, Aug 06 2017",
				"Diagonal of rational functions 1/(1 - (x^2*y + y^2*z - z^2*x + 3*x*y*z)), 1/(1 - (x^3 + y^3 - z^3 + 3*x*y*z)), 1/(1 + x^3 + y^3 + z^3 - 3*x*y*z). - _Gheorghe Coserea_, Aug 04 2018"
			],
			"reference": [
				"Matthijs Coster, Over 6 families van krommen [On 6 families of curves], Master's Thesis (unpublished), Aug 26 1983.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"D. Zagier, Integral solutions of Apery-like recurrence equations, in: Groups and Symmetries: from Neolithic Scots to John McKay, CRM Proc. Lecture Notes 47, Amer. Math. Soc., Providence, RI, 2009, pp. 349-366."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A006077/b006077.txt\"\u003eTable of n, a(n) for n = 0..1400\u003c/a\u003e (terms 0..200 from T. D. Noe)",
				"Arnaud Beauville, \u003ca href=\"http://gallica.bnf.fr/ark:/12148/bpt6k5543443c/f31.item\"\u003eLes familles stables de courbes sur P_1 admettant quatre fibres singulières\u003c/a\u003e, Comptes Rendus, Académie Sciences Paris, no. 294, May 24 1982.",
				"A. Bostan, S. Boukraa, J.-M. Maillard, and J.-A. Weil, \u003ca href=\"http://arxiv.org/abs/1507.03227\"\u003eDiagonals of rational functions and selected differential Galois groups\u003c/a\u003e, arXiv preprint arXiv:1507.03227 [math-ph], 2015.",
				"M. Coster, \u003ca href=\"/A001850/a001850_1.pdf\"\u003eEmail, Nov 1990\u003c/a\u003e",
				"Ofir Gorodetsky, \u003ca href=\"https://arxiv.org/abs/2102.11839\"\u003eNew representations for all sporadic Apéry-like sequences, with applications to congruences\u003c/a\u003e, arXiv:2102.11839 [math.NT], 2021. See B p. 2.",
				"Amita Malik and Armin Straub, \u003ca href=\"https://doi.org/10.1007/s40993-016-0036-8\"\u003eDivisibility properties of sporadic Apéry-like numbers\u003c/a\u003e, Research in Number Theory, 2016, 2:5.",
				"Stéphane Ouvry and Alexios Polychronakos, \u003ca href=\"https://arxiv.org/abs/2006.06445\"\u003eLattice walk area combinatorics, some remarkable trigonometric sums and Apéry-like numbers\u003c/a\u003e, arXiv:2006.06445 [math-ph], 2020.",
				"Zhi-Wei Sun, \u003ca href=\"http://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;fb189464.1308\"\u003eThree mysterious conjectures on Hankel-type determinants\u003c/a\u003e, a message to Number Theory List, August 22, 2013.",
				"Zhi-Wei Sun, \u003ca href=\"http://maths.nju.edu.cn/~zwsun/150f.pdf\"\u003eConnections between p = x^2+3*y^2 and Franel numbers\u003c/a\u003e, J. Number Theory 133(2013), 2914-2928."
			],
			"formula": [
				"G.f.: hypergeom([1/3, 2/3], [1], x^3/(x-1/3)^3) / (1-3*x). - _Mark van Hoeij_, Oct 25 2011",
				"a(n) = Sum_{k=0..floor(n/3)}(-1)^k*3^(n-3k)*C(n,3k)*C(2k,k)*C(3k,k). - _Zhi-Wei Sun_, Aug 21 2013",
				"0 = x*(x^2+9*x+27)*y'' + (3*x^2 + 18*x + 27)*y' + (x + 3)*y, where y(x) = A(x/-27). - _Gheorghe Coserea_, Aug 26 2016",
				"a(n) = 3^n*hypergeom([-n/3, (1-n)/3, (2-n)/3], [1, 1], 1). - _Peter Luschny_, Nov 01 2017"
			],
			"example": [
				"G.f. = 1 + 3*x + 9*x^2 + 21*x^3 + 9*x^4 - 297*x^5 - 2421*x^6 - 12933*x^7 - ..."
			],
			"maple": [
				"a := n -\u003e 3^n*hypergeom([-n/3, (1-n)/3, (2-n)/3], [1, 1], 1):",
				"seq(simplify(a(n)), n=0..24); # _Peter Luschny_, Nov 01 2017"
			],
			"mathematica": [
				"Table[Sum[(-1)^k*3^(n - 3*k)*Binomial[n, 3*k]*Binomial[2*k, k]* Binomial[3*k, k], {k, 0, Floor[n/3]}], {n, 0, 50}] (* _G. C. Greubel_, Oct 24 2017 *)",
				"a[ n_] := SeriesCoefficient[ HypergeometricPFQ[ {1/3, 2/3}, {1}, x^3 / (x - 1/3)^3 ] / (1 - 3 x), {x, 0, n}]; (* _Michael Somos_, Nov 01 2017 *)"
			],
			"program": [
				"(PARI) subst(eta(q)^3/eta(q^3), q, serreverse(eta(q^9)^3/eta(q)^3*q)) \\\\ (generating function) Helena Verrill (verrill(AT)math.lsu.edu), Apr 20 2009 [for (-1)^n*a(n)]",
				"(PARI)",
				"diag(expr, N=22, var=variables(expr)) = {",
				"  my(a = vector(N));",
				"  for (k = 1, #var, expr = taylor(expr, var[#var - k + 1], N));",
				"  for (n = 1, N, a[n] = expr;",
				"    for (k = 1, #var, a[n] = polcoeff(a[n], n-1)));",
				"  return(a);",
				"};",
				"diag(1/(1 + x^3 + y^3 + z^3 - 3*x*y*z), 25)",
				"(PARI)",
				"seq(N) = {",
				"  my(a = vector(N)); a[1] = 3; a[2] = 9;",
				"  for (n = 2, N-1, a[n+1] = ((9*n^2+9*n+3)*a[n] - 27*n^2*a[n-1])/(n+1)^2);",
				"  concat(1,a);",
				"};",
				"seq(24)",
				"\\\\ test: y=subst(Ser(seq(202)), 'x, -'x/27); 0 == x*(x^2+9*x+27)*y'' + (3*x^2+18*x+27)*y' + (x+3)*y",
				"\\\\ _Gheorghe Coserea_, Nov 09 2017",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); (-1)^n * polcoeff( subst(eta(x + A)^3 / eta(x^3 + A), x, serreverse( x * eta(x^9 + A)^3 / eta(x + A)^3)), n))}; /* _Michael Somos_, Nov 01 2017 */"
			],
			"xref": [
				"Related to diagonal of rational functions: A268545-A268555.",
				"Cf. A091401.",
				"The Apéry-like numbers [or Apéry-like sequences, Apery-like numbers, Apery-like sequences] include A000172, A000984, A002893, A002895, A005258, A005259, A005260, A006077, A036917, A063007, A081085, A093388, A125143 (apart from signs), A143003, A143007, A143413, A143414, A143415, A143583, A183204, A214262, A219692,A226535, A227216, A227454, A229111 (apart from signs), A260667, A260832, A262177, A264541, A264542, A279619, A290575, A290576. (The term \"Apery-like\" is not well-defined.)",
				"For primes that do not divide the terms of the sequences A000172, A005258, A002893, A081085, A006077, A093388, A125143, A229111, A002895, A290575, A290576, A005259 see A260793, A291275-A291284 and A133370 respectively."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from Kok Seng Chua (chuaks(AT)ihpc.nus.edu.sg), Jun 20 2000"
			],
			"references": 51,
			"revision": 126,
			"time": "2021-02-25T15:55:27-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}