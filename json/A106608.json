{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A106608",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 106608,
			"data": "0,1,2,3,4,5,6,1,8,9,10,11,12,13,2,15,16,17,18,19,20,3,22,23,24,25,26,27,4,29,30,31,32,33,34,5,36,37,38,39,40,41,6,43,44,45,46,47,48,7,50,51,52,53,54,55,8,57,58,59,60,61,62,9,64,65,66,67,68,69,10,71,72,73,74,75,76",
			"name": "a(n) = numerator of n/(n+7).",
			"comment": [
				"In general, the numerators of n/(n+p) for prime p and n \u003e= 0 form a sequence with the g.f.: x/(1-x)^2 - (p-1)*x^p/(1-x^p)^2. - _Paul D. Hanna_, Jul 27 2005",
				"Multiplicative with a(p^e) = p^e if p\u003c\u003e7, a(7^e) = 7^(e-1) if e \u003e 0. - _R. J. Mathar_, Apr 18 2011",
				"Generalizing the above comment of Hanna, the numerators of n/(n + k) for a fixed positive integer k and n \u003e= 0 form a sequence with the g.f.: Sum_{d divides k} f(d)*x^d/(1 - x^d)^2, where f(n) is the Dirichlet inverse of the Euler totient function A000010. f(n) is a multiplicative function defined on prime powers p^k by f(p^k) = 1 - p. See A023900. - _Peter Bala_, Feb 17 2019",
				"a(n) \u003c\u003e n iff n = 7 * k, in this case, a(n) = k. - _Bernard Schott_, Feb 19 2019"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A106608/b106608.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"formula": [
				"G.f.: x/(1-x)^2 - 6*x^7/(1-x^7)^2. - _Paul D. Hanna_, Jul 27 2005",
				"From _R. J. Mathar_, Apr 18 2011: (Start)",
				"a(n) = A109048(n)/7.",
				"Dirichlet g.f.: zeta(s-1)*(1-6/7^s). (End)"
			],
			"maple": [
				"seq(numer(n/(n+7)),n=0..80); # _Muniru A Asiru_, Feb 19 2019"
			],
			"mathematica": [
				"f[n_]:=Numerator[n/(n+7)];Array[f,100,0] (* _Vladimir Joseph Stephan Orlovsky_, Feb 16 2011 *)"
			],
			"program": [
				"(Sage) [lcm(n,7)/7 for n in range(0, 100)] # _Zerinvary Lajos_, Jun 09 2009",
				"(MAGMA) [Numerator(n/(n+7)): n in [0..100]]; // _Vincenzo Librandi_, Apr 18 2011",
				"(PARI) vector(100, n, n--; numerator(n/(n+7))) \\\\ _G. C. Greubel_, Feb 19 2019",
				"(GAP) List([0..80],n-\u003eNumeratorRat(n/(n+7))); # _Muniru A Asiru_, Feb 19 2019"
			],
			"xref": [
				"Cf. A109048, A023900. Sequences given by the formula numerator(n/(n + k)): A026741 (k = 2), A051176 (k = 3), A060819 (k = 4), A060791 (k = 5), A060789 (k = 6), A106609 thru A106612 (k = 8 thru 11), A051724 (k = 12), A106614 thru A106621 (k = 13 thru 20)."
			],
			"keyword": "nonn,frac,mult",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_, May 15 2005",
			"references": 21,
			"revision": 35,
			"time": "2019-12-07T12:18:24-05:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}