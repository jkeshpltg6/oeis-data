{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A324524",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 324524,
			"data": "1,2,4,8,9,16,18,32,36,64,72,81,125,128,144,162,250,256,288,324,500,512,576,648,729,1000,1024,1125,1152,1296,1458,2000,2048,2250,2304,2401,2592,2916,4000,4096,4500,4608,4802,5184,5832,6561,8000,8192,9000,9216",
			"name": "Numbers where every prime index divides its multiplicity in the prime factorization. Numbers divisible by a power of prime(k)^k for each prime index k.",
			"comment": [
				"These are a kind of self-describing numbers (cf. A001462, A304679).",
				"A prime index of n is a number m such that prime(m) divides n. The multiset of prime indices of n is row n of A112798. The prime signature of a number is the multiset of multiplicities (or exponents) in its prime factorization.",
				"Also Heinz numbers of integer partitions in which every part divides its multiplicity (counted by A001156). The Heinz number of an integer partition (y_1, ..., y_k) is prime(y_1) * ... * prime(y_k).",
				"Also products of elements of A062457."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A324524/b324524.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"Closed under multiplication.",
				"Sum_{n\u003e=1} 1/a(n) = Product_{k\u003e=1} 1/(1-prime(k)^(-k)) = 2.26910478689594012492... - _Amiram Eldar_, Sep 30 2020"
			],
			"example": [
				"The sequence of terms together with their prime indices begins as follows. For example, we have 18: {1,2,2} because 18 = prime(1) * prime(2) * prime(2).",
				"    1: {}",
				"    2: {1}",
				"    4: {1,1}",
				"    8: {1,1,1}",
				"    9: {2,2}",
				"   16: {1,1,1,1}",
				"   18: {1,2,2}",
				"   32: {1,1,1,1,1}",
				"   36: {1,1,2,2}",
				"   64: {1,1,1,1,1,1}",
				"   72: {1,1,1,2,2}",
				"   81: {2,2,2,2}",
				"  125: {3,3,3}",
				"  128: {1,1,1,1,1,1,1}",
				"  144: {1,1,1,1,2,2}",
				"  162: {1,2,2,2,2}",
				"  250: {1,3,3,3}",
				"  256: {1,1,1,1,1,1,1,1}"
			],
			"maple": [
				"q:= n-\u003e andmap(i-\u003e irem(i[2], numtheory[pi](i[1]))=0, ifactors(n)[2]):",
				"select(q, [$1..10000])[];  # _Alois P. Heinz_, Mar 08 2019"
			],
			"mathematica": [
				"Select[Range[1000],And@@Cases[If[#==1,{},FactorInteger[#]],{p_,k_}:\u003eDivisible[k,PrimePi[p]]]\u0026]",
				"v = Join[{1}, Prime[(r = Range[10])]^r]; n = Length[v]; vmax = 10^4; s = {1}; Do[v1 = v[[k]]; rmax = Floor[Log[v1, vmax]]; s1 = v1^Range[0, rmax]; s2 = Select[Union[Flatten[Outer[Times, s, s1]]], # \u003c= vmax \u0026]; s = Union[s, s2], {k, 2, n}]; Length[s] (* _Amiram Eldar_, Sep 30 2020 *)"
			],
			"xref": [
				"Cf. A001156, A033461, A056239, A062457, A066328, A072873, A112798, A118914 (prime signature), A124010, A181819, A276078, A304679.",
				"Cf. A109298, A324525, A324570, A324571, A324572, A324587, A324588.",
				"Sequences related to self-description: A000002, A001462, A079000, A079254, A276625, A304360."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Gus Wiseman_, Mar 07 2019",
			"references": 13,
			"revision": 23,
			"time": "2020-09-30T03:30:04-04:00",
			"created": "2019-03-08T15:18:27-05:00"
		}
	]
}