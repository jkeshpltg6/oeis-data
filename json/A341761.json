{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A341761",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 341761,
			"data": "0,0,1,0,-1,3,0,0,-3,6,0,-1,1,-6,10,0,2,-6,4,-10,15,0,-2,10,-18,10,-15,21,0,2,-12,31,-41,20,-21,28,0,-1,11,-41,76,-80,35,-28,36,0,2,-6,37,-109,161,-141,56,-36,45,0,0,9,-29,110,-251,308,-231,84,-45,55",
			"name": "Triangle read by rows in which row n is the coefficients of the subword complexity polynomial S(n,x).",
			"comment": [
				"S(n,x) is the sum of subword complexities (number of nonempty distinct subwords) of all words of length n and an alphabet with size x.",
				"Note that although the coefficients can be negative, S(n,x) is always a nonnegative number for n,x \u003e= 0.",
				"The degree of S(n,x) is n.",
				"The constant coefficient of S(n,x) is always 0.",
				"Conjecture: the coefficient of x^n in S(n,x) is n*(n+1)/2."
			],
			"link": [
				"Shiyao Guo, \u003ca href=\"/A341761/b341761.txt\"\u003eTable of n, a(n) for n = 0..1890\u003c/a\u003e",
				"Shiyao Guo, \u003ca href=\"https://mivik.gitee.io/2021/research/expected-subword-complexity-en/\"\u003eOn the Expected Subword Complexity of Random Words\u003c/a\u003e.",
				"Shiyao Guo, \u003ca href=\"https://gist.github.com/Mivik/0dd1068c0d768aa7f53abd78f9d4cf0f\"\u003eC++ program that computes subword complexity polynomial for n up to 60.\u003c/a\u003e"
			],
			"example": [
				"The triangle begins as",
				"  0;",
				"  0,   1;",
				"  0,  -1,   3;",
				"  0,   0,  -3,   6;",
				"  0,  -1,   1,  -6,  10;",
				"  0,   2,  -6,   4, -10,  15;",
				"  0,  -2,  10, -18,  10, -15,  21;",
				"  0,   2, -12,  31, -41,  20, -21,  28;",
				"  ...",
				"Below lists some subword complexity polynomials:",
				"  S(0,x) = 0",
				"  S(1,x) =    1*x",
				"  S(2,x) =   -1*x + 3*x^2",
				"  S(3,x) =         -3*x^2 + 6*x^3",
				"  S(4,x) =   -1*x +   x^2 - 6*x^3 + 10*x^4",
				"  ...",
				"For n = 3 and x = 2 there are eight possible words: \"aaa\", \"aab\", \"aba\", \"abb\", \"baa\", \"bab\", \"bba\" and \"bbb\", and their subword complexities are 3, 5, 5, 5, 5, 5, 5 and 3 respectively, and their sum = S(3,2) = -3*(2^2)+6*(2^3) = 36."
			],
			"mathematica": [
				"S[n_, x_] := Total[Length /@ DeleteDuplicates /@ Subsequences /@ Tuples[Table[i, {i, 0, x}], n] - 1]; A341761[n_] := CoefficientList[FindSequenceFunction[ParallelTable[S[n, i], {i, 0, n + 1}], x], {x}]; Join[{0, 0, 1}, Table[A341761[n], {n, 2, 7}] // Flatten] (* _Robert P. P. McKone_, Feb 20 2021 *)"
			],
			"program": [
				"(C++) (* see link above *)"
			],
			"xref": [
				"Cf. A340885 (values of S(n,2))."
			],
			"keyword": "sign,tabl",
			"offset": "0,6",
			"author": "_Shiyao Guo_, Feb 19 2021",
			"references": 1,
			"revision": 30,
			"time": "2021-03-14T16:40:09-04:00",
			"created": "2021-02-20T16:17:57-05:00"
		}
	]
}