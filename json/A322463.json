{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A322463",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 322463,
			"data": "0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,20,19,18,21,22,23,24,25,26,27,28,29,30,31,32,33,40,35,36,41,44,39,34,37,42,43,38,45,46,47,48,49,52,51,50,53,54,55,56,57,58,59,60,61,62,63,64,65,80,67",
			"name": "Reverse runs of zeros in binary expansion of n and convert back to decimal.",
			"comment": [
				"This sequence is a self-inverse permutation of nonnegative integers."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A322463/b322463.txt\"\u003eTable of n, a(n) for n = 0..8192\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A322463/a322463.png\"\u003eColored scatterplot of (n, a(n)) for n = 0..2^18-1\u003c/a\u003e (where the color is function of A005811(n))",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"A000120(a(n)) = A000120(n).",
				"A005811(a(n)) = A005811(n).",
				"a(A164710(n)) = A164710(n).",
				"a(A322464(n)) = A322464(a(n)).",
				"a(2^n) = 2^n.",
				"a(2^n-1) = 2^n-1."
			],
			"example": [
				"For n = 150:",
				"- the binary representation of 150 is \"10010110\",",
				"- we have three runs of zeros: \"00\", \"0\" and \"0\",",
				"- we exchange the first and the third run, and the second remains in place,",
				"- we obtain: \"10101100\",",
				"- hence a(150) = 172."
			],
			"mathematica": [
				"a[n_] := Module[{s=Split[IntegerDigits[n,2]]}, m=Length[s]; m2=m-Mod[m,2]; If[m2\u003e0, ind=Riffle[Range[1,m,2],Range[m2,1,-2]]; FromDigits[Flatten[s[[ind]]],2],n]]; Array[a, 100, 0] (* _Amiram Eldar_, Dec 12 2018 *)"
			],
			"program": [
				"(PARI) a(n) = {",
				"    my (r=n, z=[], v=0, p=1, i=0);",
				"    while (r, my (l=valuation(r+(r%2),2)); if (r%2==0, z=concat(l,z)); r\\=2^l);",
				"    while (n, my (l=valuation(n+(n%2),2)); if (n%2, v+=(2^l-1)*p; p*=2^l, p*=2^z[i++]); n\\=2^l);",
				"    return (v);",
				"}"
			],
			"xref": [
				"See A322464 for the variant where we reverse the runs of ones.",
				"See A056539 for a similar sequence.",
				"Cf. A000120, A005811, A164710."
			],
			"keyword": "nonn,base,look",
			"offset": "0,3",
			"author": "_Rémy Sigrist_, Dec 09 2018",
			"references": 2,
			"revision": 18,
			"time": "2018-12-12T15:40:05-05:00",
			"created": "2018-12-12T14:24:44-05:00"
		}
	]
}