{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A030182",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 30182,
			"data": "1,-12,54,-76,-243,1188,-1384,-2916,11934,-11580,-21870,79704,-71022,-123444,421308,-352544,-581013,1885572,-1510236,-2388204,7469928,-5777672,-8852004,26869968,-20218587,-30177684,89408826",
			"name": "McKay-Thompson series of class 3B for the Monster group with a(0) = -12.",
			"comment": [
				"Let t(q) = (eta(q)/eta(q^3))^12 = 1/q-12+54q-76q^2-243q^3+.... If j(q) is the j-invariant, with q-series given by A000521, then j(q) = (t+27)(t+243)^3/t^3 j(q^3) = (t+27)(t+3)^3/t. Hence t(q) can be used to parametrize the classical modular curve X0(3). - _Gene Ward Smith_, Aug 04 2006",
				"Cubic AGM theta functions: a(q) (see A004016), b(q) (A005928), c(q) (A005882)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A030182/b030182.txt\"\u003eTable of n, a(n) for n = -1..10000\u003c/a\u003e",
				"J. H. Conway and S. P. Norton, \u003ca href=\"http://blms.oxfordjournals.org/content/11/3/308.extract\"\u003eMonstrous Moonshine\u003c/a\u003e, Bull. Lond. Math. Soc. 11 (1979) 308-339.",
				"N. D. Elkies, \u003ca href=\"http://www.math.harvard.edu/~elkies/modular.pdf\"\u003eElliptic and modular curves over finite fields and related computational issues\u003c/a\u003e, in AMS/IP Studies in Advanced Math., 7 (1998), 21-76, esp. p. 38.",
				"D. Ford, J. McKay and S. P. Norton, \u003ca href=\"http://dx.doi.org/10.1080/00927879408825127\"\u003eMore on replicable functions\u003c/a\u003e, Commun. Algebra 22, No. 13, 5175-5193 (1994).",
				"J. McKay and H. Strauss, \u003ca href=\"http://dx.doi.org/10.1080/00927879008823911\"\u003eThe q-series of monstrous moonshine and the decomposition of the head characters\u003c/a\u003e, Comm. Algebra 18 (1990), no. 1, 253-278.",
				"\u003ca href=\"/index/Mat#McKay_Thompson\"\u003eIndex entries for McKay-Thompson series for Monster simple group\u003c/a\u003e"
			],
			"formula": [
				"Expansion of (eta(q) / eta(q^3))^12 in powers of q.",
				"Expansion of (3 * b(q) / c(q))^3 in powers of q where b(), c() are cubic AGM theta functions. - _Michael Somos_, Jun 16 2012",
				"Euler transform of period 3 sequence [ -12, -12, 0, ...]. - _Michael Somos_, Nov 08 2011",
				"G.f. A(q) satisfies 0 = f(A(q), A(q^2)) where f(u, v) = (u + v)^3 - u * (27 + u) * v * (27 + v). - _Michael Somos_, Nov 08 2011",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (3 t)) = 729 g(t) where q = exp(2 Pi i t) and g() is the g.f. for A121590. - _Michael Somos_, Nov 08 2011",
				"G.f.: x^-1 * (Product_{k\u003e0} (1 - x^k) / (1 - x^(3*k)))^12.",
				"Convolution inverse of A121590. Convolution square of A007262. Convolution cube of A058095. Convolution fourth power of A199659. Convolution sixth power of A112157. Convolution twelfth power of A137569.",
				"a(-1) = 1, a(n) = -(12/(n+1))*Sum_{k=1..n+1} A046913(k)*a(n-k) for n \u003e -1. - _Seiichi Manyama_, Mar 29 2017"
			],
			"example": [
				"G.f. = 1/q - 12 + 54*q - 76*q^2 - 243*q^3 + 1188*q^4 - 1384*q^5 - 2916*q^6 + ..."
			],
			"mathematica": [
				"a[ n_] := With[{m = n + 1}, SeriesCoefficient[ (Product[ 1 - q^k, {k, m}] / Product[ 1 - q^k, {k, 3, m, 3}])^12, {q, 0, m}]]; (* _Michael Somos_, Nov 08 2011 *)",
				"a[ n_] := SeriesCoefficient[ 1/q (QPochhammer[ q] / QPochhammer[ q^3])^12, {q, 0, n}]; (* _Michael Somos_, May 03 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c-1, 0, n++; A = x * O(x^n); polcoeff( (eta(x + A) / eta(x^3 + A))^12, n))}; /* _Michael Somos_, Nov 08 2011 */"
			],
			"xref": [
				"Cf. A007244, A007262, A045481, A058095, A112157, A121590, A137569, A198955, A199659."
			],
			"keyword": "sign,nice,easy",
			"offset": "-1,2",
			"author": "_N. J. A. Sloane_",
			"references": 6,
			"revision": 36,
			"time": "2017-03-29T08:53:50-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}