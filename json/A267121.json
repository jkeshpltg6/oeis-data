{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A267121",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 267121,
			"data": "1,3,2,1,6,7,1,3,7,7,6,2,6,12,1,1,12,10,7,6,13,7,2,7,8,19,8,1,18,12,2,3,14,15,13,7,7,18,1,7,25,14,6,6,19,13,2,2,14,22,12,6,18,27,4,12,13,9,19,1,18,25,5,1,24,26,6,12,26,14,2,10,16,31,16,7,24,13,4,6",
			"name": "Number of ordered ways to write n as x^2 + y^2 + z^2 + w^2 with x*y*z*(x+9*y+11*z+10*w) a square, where x is a positive integer and y,z,w are nonnegative integers.",
			"comment": [
				"Conjecture: (i) a(n) \u003e 0 for all n \u003e 0, and a(n) = 1 only for n = 4^k*m (k = 0,1,2,... and m = 1, 7, 15, 39, 119, 127, 159, 239, 359, 391, 527, 543, 863, 5791).",
				"(ii) Any positive integer can be written as x^2 + y^2 + z^2 + w^2 with 2*x*y*(x+2y+z+2w) (or 2*x*y*(x+6y+z+2w), or x*y*(x+11y+z+2w)) a square, where x,y,z,w are nonnegative integers with z \u003e 0 (or w \u003e 0).",
				"(iii) Any natural number can be written as w^2 + x^2 + y^2 + z^2 with w,x,y,z nonnegative integers such that w*(a*w+b*x+c*y+d*z) is a square, provided that (a,b,c,d) is among the following quadruples (1,1,2,3), (1,1,4,5), (1,1,6,9), (1,2,6,34), (1,3,6,m) (m = 12, 21, 27, 36), (1,3,9,18), (1,3,9,36), (1,3,18,27), (1,3,24,117), (1,3,90,99), (1,6,6,18), (1,6,6,30), (1,8,16,24).",
				"For more refinements of Lagrange's four-square theorem, see arXiv:1604.06723."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A267121/b267121.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1604.06723\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, arXiv:1604.06723 [math.GM], 2016.",
				"Zhi-Wei Sun, \u003ca href=\"http://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;852b9c4a.1604\"\u003eRefine Lagrange's four-square theorem\u003c/a\u003e, a message to Number Theory List, April 26, 2016."
			],
			"example": [
				"a(4) = 1 since 4 = 2^2 + 0^2 + 0^2 + 0^2 with 2 \u003e 0 and 2*0*0*(2+9*0+11*0+10*0) = 0^2.",
				"a(7) = 1 since 7 = 2^2 + 1^2 + 1^2 + 1^2 with 2 \u003e 0 and 2*1*1*(2+9*1+11*1+10*1) = 8^2.",
				"a(15) = 1 since 15 = 2^2 + 1^2 + 3^2 + 1^2 with 2 \u003e 0 and 2*1*3*(2+9*1+11*3+10*1) = 18^2.",
				"a(39) = 1 since 39 = 1^2 + 1^2 + 1^2 + 6^2 with 1 \u003e 0 and 1*1*1*(1+9*1+11*1+10*6) = 9^2.",
				"a(119) = 1 since 119 = 1^2 + 1^2 + 9^2 + 6^2 with 1 \u003e 0 and 1*1*9*(1+9*1+11*9+10*6) = 39^2.",
				"a(127) = 1 since 127 = 5^2 + 1^2 + 1^2 + 10^2 with 5 \u003e 0 and 5*1*1*(5+9*1+11*1+10*10) = 25^2.",
				"a(159) = 1 since 159 = 3^2 + 1^2 + 7^2 + 10^2 with 3 \u003e 0 and 3*1*7*(3+9*1+11*7+10*10) = 63^2.",
				"a(239) = 1 since 239 = 3^2 + 3^2 + 10^2 + 11^2 with 3 \u003e 0 and 3*3*10*(3+9*3+11*10+10*11) = 150^2.",
				"a(359) = 1 since 359 = 9^2 + 11^2 + 11^2 + 6^2 with 9 \u003e 0 and 9*11*11*(9+9*11+11*11+10*6) = 561^2.",
				"a(391) = 1 since 391 = 19^2 + 5^2 + 1^2 + 2^2 with 19 \u003e 0 and 19*5*1*(19+9*5+11*1+10*2) = 95^2.",
				"a(527) = 1 since 527 = 21^2 + 6^2 + 7^2 + 1^2 with 21 \u003e 0 and 21*6*7*(21+9*6+11*7+10*1) = 378^2.",
				"a(543) = 1 since 543 = 15^2 + 13^2 + 10^2 + 7^2 with 15 \u003e 0 and 15*13*10*(15+9*13+11*10+10*7) = 780^2.",
				"a(863) = 1 since 863 = 9^2 + 9^2 + 5^2 + 26^2 with 9 \u003e 0 and 9*9*5*(9+9*9+11*5+10*26) = 405^2.",
				"a(5791) = 1 since 5791 = 57^2 + 38^2 + 33^2 + 3^2 with 57 \u003e 0 and 57*38*33*(57+9*38+11*33+10*3) = 7524^2."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]]",
				"Do[r=0;Do[If[SQ[n-x^2-y^2-z^2]\u0026\u0026SQ[x*y*z(x+9y+11z+10*Sqrt[n-x^2-y^2-z^2])],r=r+1],{x,1,Sqrt[n]},{y,0,Sqrt[n-x^2]},{z,0,Sqrt[n-x^2-y^2]}];Print[n,\" \",r];Continue,{n,1,80}]"
			],
			"xref": [
				"Cf. A000118, A000290, A260625, A261876, A262357, A268507, A269400, A271510, A271513, A271518, A271608, A271665, A271714, A271721, A271724, A271775, A271778, A271824, A272084, A272332, A272351."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Zhi-Wei Sun_, May 01 2016",
			"references": 23,
			"revision": 29,
			"time": "2016-05-01T15:48:53-04:00",
			"created": "2016-05-01T15:48:53-04:00"
		}
	]
}