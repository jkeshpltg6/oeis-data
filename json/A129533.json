{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A129533",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 129533,
			"data": "0,0,0,0,1,0,0,3,3,0,0,6,12,6,0,0,10,30,30,10,0,0,15,60,90,60,15,0,0,21,105,210,210,105,21,0,0,28,168,420,560,420,168,28,0,0,36,252,756,1260,1260,756,252,36,0,0,45,360,1260,2520,3150,2520,1260,360,45,0,0,55,495",
			"name": "Array read by antidiagonals: T(n,k) = binomial(n+1,2)*binomial(n+k,n+1) for 0 \u003c= k \u003c= n.",
			"comment": [
				"Previous name was: Triangle read by rows: T(n,k)=derivative of the q-binomial coefficient [n,k] evaluated at q=1 (0\u003c=k\u003c=n). - _N. J. A. Sloane_, Jan 06 2016",
				"For example, T(5,2)=30 because [5,2] = q^6 + q^5 + 2*q^4 + 2*q^3 + 2*q^2 + q + 1 with derivative 6q^5 + 5q^4 + 8q^3 + 6q^2 + 4q + 1, having value 30 at q=1. - _Emeric Deutsch_, Apr 22 2007",
				"Sum of entries in n-th antidiagonal = n(n-1)2^(n-3) = A001788(n-1).",
				"T(n,k) = A094305(n-2, k-1) for n \u003e= 2, k \u003e= 1.",
				"T(n,k) is total number of pips on a set of generalized linear dominoes with n cells (rather than two) and with the number of pips in each cell running from 0 to k (rather than 6). T(2,6) = 168 gives the total number of pips on a standard set of dominoes. We regard a generalized linear domino with n cells and up to k pips per cell as an ordered n-tuple [i_1, i_2, ..., i_n] with 0 \u003c= i_1 \u003c= i_2 \u003c=  ... \u003c= i_n \u003c= k. - _Alan Shore_ and _N. J. A. Sloane_, Jan 06 2016",
				"T(n,k) can also be written more symmetrically as the trinomial coefficient (n+k; n-1, k-1, 2). - _N. J. A. Sloane_, Jan 06 2016",
				"As a triangle read by rows, T(n,k) is the total number of inversions over all length n binary words having exactly k 1's. T(n,k) is also the total area above all North East lattice paths from the origin to the point (k,n-k). - _Geoffrey Critzer_, Mar 22 2018"
			],
			"reference": [
				"G. E. Andrews, The Theory of Partitions, Addison-Wesley, 1976."
			],
			"link": [
				"\u003ca href=\"/index/Do#domino\"\u003eIndex entries for sequences related to dominoes\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = (1/2)*k*(k+1)*binomial(n,k+1).",
				"G.f.: G(q,z) = qz^2/(1-z-qz)^3."
			],
			"example": [
				"Array begins:",
				"0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... (A000004)",
				"0, 1, 3, 6, 10, 15, 21, 28, 36, 45, 55, 66, 78, 91, ... (A000217)",
				"0, 3, 12, 30, 60, 105, 168, 252, 360, 495, 660, 858, ... (A027480)",
				"0, 6, 30, 90, 210, 420, 756, 1260, 1980, 2970, 4290, ... (A033487)",
				"0, 10, 60, 210, 560, 1260, 2520, 4620, 7920, 12870, ... (A266732)",
				"0, 15, 105, 420, 1260, 3150, 6930, 13860, 25740, ... (A240440)",
				"0, 21, 168, 756, 2520, 6930, 16632, 36036, ... (A266733)",
				"...",
				"If regarded as a triangle, this begins:",
				"  0;",
				"  0,  0;",
				"  0,  1,  0;",
				"  0,  3,  3,  0;",
				"  0,  6, 12,  6,  0;",
				"  0, 10, 30, 30, 10,  0;",
				"  0, 15, 60, 90, 60, 15, 0;",
				"  ..."
			],
			"maple": [
				"dd:=proc(n,m) if m=0 or n=0 then 0 else (m+n)!/(2*(m-1)!*(n-1)!); fi; end;",
				"f:=n-\u003e[seq(dd(n,m),m=0..30)];",
				"for n from 0 to 10 do lprint(f(n)); od: # produces sequence as square array",
				"T:=(n,k)-\u003ek*(k+1)*binomial(n,k+1)/2: for n from 0 to 12 do seq(T(n,k),k=0..n) od; # yields sequence in triangular form"
			],
			"mathematica": [
				"Table[Table[D[Expand[FunctionExpand[QBinomial[n, k, q]]], q] /. q -\u003e 1, {k, 0, n}], {n, 0, 15}] // Grid (* _Geoffrey Critzer_, Mar 22 2018 *)"
			],
			"xref": [
				"Cf. A001788.",
				"Rows give A000004, A000217, A027480, A033487, A266732, A240440, A266733.",
				"A004305 and A094305 are very similar sequences."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Emeric Deutsch_, Apr 22 2007",
			"ext": [
				"Entry revised by _N. J. A. Sloane_, Jan 06 2016"
			],
			"references": 6,
			"revision": 39,
			"time": "2020-07-07T05:09:54-04:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}