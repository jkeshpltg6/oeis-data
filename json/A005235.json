{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005235",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5235,
			"id": "M2418",
			"data": "3,5,7,13,23,17,19,23,37,61,67,61,71,47,107,59,61,109,89,103,79,151,197,101,103,233,223,127,223,191,163,229,643,239,157,167,439,239,199,191,199,383,233,751,313,773,607,313,383,293,443,331,283,277,271,401,307,331",
			"name": "Fortunate numbers: least m \u003e 1 such that m + prime(n)# is prime, where p# denotes the product of all primes \u003c= p.",
			"comment": [
				"Reo F. Fortune conjectured that a(n) is always prime.",
				"You might be searching for Fortunate Primes, which is an alternative name for this sequence. It is not the official name yet, because it is possible, although unlikely, that not all the terms are primes. - _N. J. A. Sloane_, Sep 30 2020",
				"The first 500 terms are primes. - _Robert G. Wilson v_. The first 2000 terms are primes. - _Joerg Arndt_, Apr 15 2013",
				"The strong form of Cramér's conjecture implies that a(n) is a prime for n \u003e 1618, as previously noted by Golomb. - _Charles R Greathouse IV_, Jul 05 2011",
				"a(n) is the smallest m such that m \u003e 1 and A002110(n) + m is prime. For every n, a(n) must be greater than prime(n+1) - 1. - _Farideh Firoozbakht_, Aug 20 2003",
				"If a(n) \u003c prime(n+1)^2 then a(n) is prime. According to Cramér's conjecture a(n) = O(prime(n)^2). - _Thomas Ordowski_, Apr 09 2013",
				"From _Pierre CAMI_, Sep 08 2017: (Start)",
				"If a(n) = prime(i), lim_{N-\u003einfinity} (Sum_{n=1..N} i) / (Sum_{n=1..N} n) = 3/2.",
				"i/n is always \u003c 6.",
				"Limit_{N-\u003einfinity} (Sum_{n=1..N} a(n)) / (Sum_{n=1..N} prime(n)) = Pi/2.",
				"a(n) / prime(n) is always \u003c 8.",
				"(End)",
				"The name \"Fortunate numbers\" was coined by Golomb (1981) after the New Zealand social anthropologist Reo Franklin Fortune (1903 - 1979). According to Golomb, Fortune's conjecture first appeared in print in Martin Gardner's Mathematical Games column in 1980. - _Amiram Eldar_, Aug 25 2020"
			],
			"reference": [
				"Martin Gardner, The Last Recreations (1997), pp. 194-195.",
				"Richard K. Guy, Unsolved Problems in Number Theory, 3rd ed., Springer, 1994, Section A2, p. 11.",
				"Stephen P. Richards, A Number For Your Thoughts, 1982, p. 200.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"David Wells, Prime Numbers: The Most Mysterious Figures In Math, Hoboken, New Jersey: John Wiley \u0026 Sons (2005), pp. 108-109."
			],
			"link": [
				"Pierre CAMI, \u003ca href=\"/A005235/b005235.txt\"\u003eTable of n, a(n) for n = 1..3000\u003c/a\u003e (first 2000 terms from T. D. Noe)",
				"Ray Abrahams and Huon Wardle, \u003ca href=\"https://www.jstor.org/stable/23819766\"\u003eFortune's 'Last Theorem'\u003c/a\u003e, Cambridge Anthropology, Vol. 23, No. 1 (2002), pp. 60-62.",
				"Cyril Banderier, \u003ca href=\"http://algo.inria.fr/banderier/Computations/prime_factorial.html\"\u003eConjecture checked for n \u003c 1000\u003c/a\u003e [It has been reported that the data given here contains several errors]",
				"C. K. Caldwell, \u003ca href=\"https://primes.utm.edu/glossary/page.php?sort=FortunateNumber\"\u003eFortunate number\u003c/a\u003e, The Prime Glossary.",
				"Martin Gardner, \u003ca href=\"https://www.jstor.org/stable/24966473\"\u003ePatterns in primes are a clue to the strong law of sma11 numbers\u003c/a\u003e, Mathematical Games, Scientific American, Vol. 243, No. 6 (December, 1980), pp. 18-28.",
				"Solomon W. Golomb, \u003ca href=\"http://www.jstor.org/stable/2689634\"\u003eThe evidence for Fortune's conjecture\u003c/a\u003e, Mathematics Magazine, Vol. 54, No. 4 (1981), pp. 209-210.",
				"Richard K. Guy, \u003ca href=\"/A005728/a005728.pdf\"\u003eLetter to N. J. A. Sloane, 1987\u003c/a\u003e",
				"Richard K. Guy, \u003ca href=\"http://www.jstor.org/stable/2322249\"\u003eThe strong law of small numbers\u003c/a\u003e. Amer. Math. Monthly 95 (1988), no. 8, 697-712.",
				"Richard K. Guy, \u003ca href=\"/A005165/a005165.pdf\"\u003eThe strong law of small numbers\u003c/a\u003e. Amer. Math. Monthly 95 (1988), no. 8, 697-712. [Annotated scanned copy]",
				"Bill McEachen, \u003ca href=\"http://garden.irmacs.sfu.ca/?q=op/maceachen_conjecture\"\u003eMcEachen Conjecture\u003c/a\u003e",
				"Romeo Meštrović, \u003ca href=\"http://arxiv.org/abs/1202.3670\"\u003eEuclid's theorem on the infinitude of primes: a historical survey of its proofs (300 BC--2012) and another new proof\u003c/a\u003e, arXiv preprint arXiv:1202.3670 [math.HO], 2012.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/FortunatePrime.html\"\u003eFortunate Prime\u003c/a\u003e",
				"Robert G. Wilson v, \u003ca href=\"/A005235/a005235.pdf\"\u003eLetter to N. J. A. Sloane with attachment, Jan 1992\u003c/a\u003e"
			],
			"formula": [
				"If x(n) = 1 + Product_{i=1..n} prime(i), q(n) = least prime \u003e x(n), then a(n) = q(n) - x(n) + 1.",
				"a(n) = 1 + the difference between the n-th primorial plus one and the next prime.",
				"a(n) = A035345(n) - A002110(n). - _Jonathan Sondow_, Dec 02 2015"
			],
			"example": [
				"a(4) = 13 because P_4# = 2*3*5*7 = 210, plus one is 211, the next prime is 223 and the difference between 210 and 223 is 13."
			],
			"maple": [
				"Primorial:= 2:",
				"p:= 2:",
				"A[1]:= 3:",
				"for n from 2 to 100 do",
				"  p:= nextprime(p);",
				"  Primorial:= Primorial * p;",
				"  A[n]:= nextprime(Primorial+p+1)-Primorial;",
				"od:",
				"seq(A[n],n=1..100); # _Robert Israel_, Dec 02 2015"
			],
			"mathematica": [
				"NPrime[n_Integer] := Module[{k}, k = n + 1; While[! PrimeQ[k], k++]; k]; Fortunate[n_Integer] := Module[{p, q}, p = Product[Prime[i], {i, 1, n}] + 1; q = NPrime[p]; q - p + 1]; Table[Fortunate[n], {n, 60}]",
				"r[n_] := (For[m = (Prime[n + 1] + 1)/2, ! PrimeQ[Product[Prime[k], {k, n}] + 2 m - 1], m++]; 2 m - 1); Table[r[n], {n, 60}]",
				"FN[n_] := Times @@ Prime[Range[n]]; Table[NextPrime[FN[k] + 1] - FN[k], {k, 60}] (* _Jayanta Basu_, Apr 24 2013 *)",
				"NextPrime[#]-#+1\u0026/@(Rest[FoldList[Times,1,Prime[Range[60]]]]+1) (* _Harvey P. Dale_, Dec 15 2013 *)"
			],
			"program": [
				"(PARI) a(n)=my(P=prod(k=1,n,prime(k)));nextprime(P+2)-P \\\\ _Charles R Greathouse IV_, Jul 15 2011; corrected by _Jean-Marc Rebert_, Jul 28 2015",
				"(Haskell)",
				"a005235 n = head [m | m \u003c- [3, 5 ..], a010051'' (a002110 n + m) == 1]",
				"-- _Reinhard Zumkeller_, Apr 02 2014",
				"(Sage)",
				"def P(n): return prod(nth_prime(k) for k in range(1, n + 1))",
				"it = (P(n) for n in range(1, 31))",
				"print([next_prime(Pn + 2) - Pn for Pn in it]) # _F. Chapoton_, Apr 28 2020",
				"(Python)",
				"from sympy import nextprime, primorial",
				"def a(n): psharp = primorial(n); return nextprime(psharp+1) - psharp",
				"print([a(n) for n in range(1, 59)]) # _Michael S. Branicky_, Jan 15 2022"
			],
			"xref": [
				"Cf. A046066, A002110, A006862, A035345, A035346, A055211, A129912, A010051, A005408, A038771, A038711."
			],
			"keyword": "nonn,nice,changed",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_",
			"references": 53,
			"revision": 184,
			"time": "2022-01-15T09:50:32-05:00",
			"created": "1991-05-20T03:00:00-04:00"
		}
	]
}