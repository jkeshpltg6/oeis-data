{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A103919",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 103919,
			"data": "1,0,1,1,0,1,0,2,0,1,2,0,2,0,1,0,4,0,2,0,1,3,0,5,0,2,0,1,0,7,0,5,0,2,0,1,5,0,9,0,5,0,2,0,1,0,12,0,10,0,5,0,2,0,1,7,0,17,0,10,0,5,0,2,0,1,0,19,0,19,0,10,0,5,0,2,0,1,11,0,28,0,20,0,10,0,5,0,2,0,1,0,30,0,33,0,20,0,10,0,5,0,2,0,1",
			"name": "Triangle of numbers of partitions of n with total number of odd parts equal to k from {0,...,n}.",
			"comment": [
				"The partition (0) of n=0 is included. For n\u003e0 no part 0 appears.",
				"The first (k=0) column gives the number of partitions without odd parts, i.e., those with even parts only. See A035363.",
				"Without the alternating zeros this becomes a triangle with columns given by the rows of the S_n(m) table shown in the Riordan reference.",
				"From _Gregory L. Simay_, Oct 31 2015: (Start)",
				"T(2n+k,k) = the number of partitions of n with parts 1..k of two kinds. If n\u003c=k, then T(2n+k) = A000712(n), the number of partitions of n with parts of two kinds.",
				"T(2n+k) = the convolution of A000041(n) and the number of partitions of n+k having exactly k parts.",
				"T(2n+k) = d(n,k) where d(n,0) = p(n) and d(n,k) = d(n,k-1) + d(n-k,k-1) + d(n-2k,k-1) + ... (End)",
				"From _Emeric Deutsch_, Oct 04 2016: (Start)",
				"T(n,k) = number of partitions (p1 \u003e= p2 \u003e= p3 \u003e= ...) of n having alternating sum p1 - p2 + p3 - ... = k. Example: T(5,3) = 2 because there are two partitions (3,1,1) and (4,1) of 5 with alternating sum 3.",
				"The equidistribution of the partition statistics \"alternating sum\" and \"total number of odd parts\" follows by conjugation. (End)"
			],
			"reference": [
				"J. Riordan, Combinatorial Identities, Wiley, 1968, p. 199."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A103919/b103919.txt\"\u003eRows n = 0..140, flattened\u003c/a\u003e",
				"D. Kim, A. J. Yee, \u003ca href=\"http://dx.doi.org/10.1023/A:1006905826378\"\u003eA note on partitions into distinct parts and odd parts\u003c/a\u003e, Ramanujan J. 3 (1999), 227-231. [_R. J. Mathar_, Nov 11 2008]",
				"Wolfdieter Lang, \u003ca href=\"/A103919/a103919.pdf\"\u003e First 11 rows.\u003c/a\u003e"
			],
			"formula": [
				"a(n, k) = number of partitions of n\u003e=0, which have exactly k odd parts (and possibly even parts) for k from {0, ..., n}.",
				"Sum_{k=0..n} k*T(n,k) = A066897(n). - _Emeric Deutsch_, Feb 17 2006",
				"G.f.: G(t,x) = 1/Product_{j\u003e=1} (1-t*x^(2*j-1))*(1-x^(2*j)). - _Emeric Deutsch_, Feb 17 2006",
				"G.f. T(2n+k,k) = g.f. d(n,k) = (1/Product_{j=1..k} (1-x^j)) * g.f. p(n). - _Gregory L. Simay_, Oct 31 2015",
				"T(n,k) = T(n-1,k-1) + T(n-2k,k). - _Gregory L. Simay_, Nov 01 2015"
			],
			"example": [
				"The triangle a(n,k) begins:",
				"n\\k 0  1  2  3  4  5  6  7  8  9 10",
				"0:  1",
				"1:  0  1",
				"2:  1  0  1",
				"3:  0  2  0  1",
				"4:  2  0  2  0  1",
				"5:  0  4  0  2  0  1",
				"6:  3  0  5  0  2  0  1",
				"7:  0  7  0  5  0  2  0  1",
				"8:  5  0  9  0  5  0  2  0  1",
				"9:  0 12  0 10  0  5  0  2  0  1",
				"10: 7  0 17  0 10  0  5  0  2  0  1",
				"... Reformatted - _Wolfdieter Lang_, Apr 28 2013",
				"a(0,0) = 1 because n=0 has no odd part, only one even part, 0, by definition. a(5,3) = 2 because there are two partitions (1,1,3) and (1,1,1,2) of 5 with exactly 3 odd parts.",
				"From _Gregory L. Simay_, Oct 31 2015: (Start)",
				"T(10,4) = T(2*3+4,4) = d(3,4) = A000712(3) = 10.",
				"T(10,2) = T(2*4+2,2) = d(4,2) = d(4,1)+d(2,1)+d(0,1) = d(4,0)+d(3,0)+d(2,0)+d(1,0)+d(0,0) + d(2,0)+d(1,0)+d(0,0) + d(0,0) = convolution sum p(4)+p(3)+2*p(2)+2*p(1)+3*p(0) = 5+3+2*2+2*1+3*1 = 17.",
				"T(9,1) = T(8,0) + T(7,1) = 5 + 7 = 12.",
				"(End)"
			],
			"maple": [
				"g:=1/product((1-t*x^(2*j-1))*(1-x^(2*j)),j=1..20): gser:=simplify(series(g,x=0,22)): P[0]:=1: for n from 1 to 18 do P[n]:=coeff(gser,x^n) od: for n from 0 to 18 do seq(coeff(P[n],t,j),j=0..n) od; # yields sequence in triangular form # _Emeric Deutsch_, Feb 17 2006"
			],
			"mathematica": [
				"T[n_, k_] := T[n, k] = Which[n\u003ck, 0, n == k, 1, Mod[n-k+1, 2] == 0, 0, k == 0, Sum[T[Quotient[n, 2], m], {m, 0, n}], True, T[n-1, k-1]+T[n-2*k, k]]; Table[T[n, k], {n, 0, 10}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Mar 05 2014, after _Paul D. Hanna_ *)",
				"Table[Length[Select[IntegerPartitions[n],Count[#,_?OddQ]==k\u0026]],{n,0,15},{k,0,n}] (* _Gus Wiseman_, Jun 20 2021 *)"
			],
			"program": [
				"(PARI)",
				"{T(n, k)=if(n\u003e=k, if(n==k, 1, if((n-k+1)%2==0, 0, if(k==0, sum(m=0, n, T(n\\2, m)), T(n-1, k-1)+T(n-2*k, k)))))}",
				"for(n=0, 20, for(k=0, n, print1(T(n, k), \", \")); print(\"\"))",
				"\\\\ _Paul D. Hanna_, Apr 27 2013"
			],
			"xref": [
				"Row sums gives A000041 (partition numbers). Columns: k=0: A035363 (with zero entries) A000041 (without zero entries), k=1: A000070, k=2: A000097, k=3: A000098, k=4: A000710, 3k\u003e=n: A000712.",
				"Cf. A066897.",
				"The strict version (without zeros) is A152146 interleaved with A152157.",
				"The rows (without zeros) are A239830 interleaved with A239829.",
				"The reverse version (without zeros) is the right half of A344612.",
				"Removing all zeros gives A344651.",
				"The strict reverse version (without zeros) is the right half of A344739.",
				"Cf. A006330, A027187, A116406, A344607, A344608, A344649, A344654."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,8",
			"author": "_Wolfdieter Lang_, Mar 24 2005",
			"references": 117,
			"revision": 66,
			"time": "2021-06-21T00:05:38-04:00",
			"created": "2005-04-09T03:00:00-04:00"
		}
	]
}