{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A056861",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 56861,
			"data": "1,3,2,10,7,6,37,27,23,21,151,114,97,88,83,674,523,446,403,378,363,3263,2589,2217,1999,1867,1785,1733,17007,13744,11829,10658,9923,9452,9145,8942,94828,77821,67340,60689,56380,53541,51644,50361,49484,562595",
			"name": "Triangle T(n,k) is the number of restricted growth strings (RGS) of set partitions of {1..n} that have an increase at index k (1\u003c=k\u003cn).",
			"comment": [
				"Number of rises s_{k+1} \u003e s_k in an RGS [s_1, ..., s_n] of a set partition of {1, ..., n}, where s_i is the subset containing i, and s_i \u003c= 1 + max(j\u003ci, s_j).",
				"Note that the number of equalities at any index is B(n-1), where B(n) are the Bell numbers. - _Franklin T. Adams-Watters_, Jun 08 2006"
			],
			"reference": [
				"W. C. Yang, Conjectures on some sequences involving set partitions and Bell numbers, preprint, 2000. [apparently unpublished, _Joerg Arndt_, Mar 05 2016]"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A056861/b056861.txt\"\u003eRows n = 2..100, flattened\u003c/a\u003e"
			],
			"example": [
				"For example, [1, 2, 1, 2, 2, 3] is the RGS of a set partition of {1, 2, 3, 4, 5, 6} and has 3 rises, at i = 1, i = 3 and i = 5.",
				"1;",
				"3,2;",
				"10,7,6;",
				"37,27,23,21;",
				"151,114,97,88,83;",
				"674,523,446,403,378,363;",
				"3263,2589,2217,1999,1867,1785,1733;",
				"17007,13744,11829,10658,9923,9452,9145,8942;",
				"94828,77821,67340,60689,56380,53541,51644,50361,49484;",
				"562595,467767,406953,367101,340551,322619,310365,301905,296011,291871;",
				"3535027,2972432,2599493,2348182,2176575,2058068,1975425,1917290,1876075, 1846648,1825501;"
			],
			"mathematica": [
				"b[n_, i_, m_, t_] := b[n, i, m, t] = If[n == 0, {1, 0}, Sum[Function[p, p + {0, If[j\u003ci, p[[1]]*x^t, 0]}][b[n-1, j, Max[m, j], t+1]], {j, 1, m+1}]];",
				"T[n_] := BellB[n] - BellB[n-1] - Function[p, Table[Coefficient[p, x, i], {i, 1, n-1}]][b[n, 1, 0, 0][[2]]];",
				"Table[T[n], {n, 2, 12}] // Flatten (* _Jean-François Alcover_, May 23 2016, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. Bell numbers A005493, A011965.",
				"Cf. A056857-A056863."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "2,2",
			"author": "Winston C. Yang (winston(AT)cs.wisc.edu), Aug 31 2000",
			"ext": [
				"Edited and extended by _Franklin T. Adams-Watters_, Jun 08 2006",
				"Clarified definition and edited comment and example, _Joerg Arndt_, Mar 08 2016",
				"Several terms corrected, _R. J. Mathar_, Mar 08 2016"
			],
			"references": 2,
			"revision": 26,
			"time": "2016-05-23T02:46:17-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}