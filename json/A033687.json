{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A033687",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 33687,
			"data": "1,1,2,0,2,1,2,0,1,2,2,0,2,0,2,0,3,2,0,0,2,1,2,0,2,2,2,0,0,0,4,0,2,1,2,0,2,2,0,0,1,2,2,0,4,0,2,0,0,2,2,0,2,0,2,0,3,2,2,0,2,0,0,0,2,3,2,0,0,2,2,0,4,0,2,0,2,0,0,0,2,2,4,0,0,1,4,0,0,2,2,0,2,0,2,0,1,2,0,0,4,2,2,0,2",
			"name": "Theta series of hexagonal lattice A_2 with respect to deep hole divided by 3.",
			"comment": [
				"The hexagonal lattice is the familiar 2-dimensional lattice in which each point has 6 neighbors. This is sometimes called the triangular lattice.",
				"a(n)=0 if and only if A000731(n)=0 (see the Han-Ono paper). - _Emeric Deutsch_, May 16 2008",
				"Number of 3-core partitions of n (denoted c_3(n) in Granville and Ono, p. 340). - _Brian Hopkins_, May 13 2008",
				"Denoted by g_1(q) in Cynk and Hulek in Remark 3.4 on page 12 (but not explicitly listed).",
				"This is a member of an infinite family of integer weight modular forms. g_1 = A033687, g_2 = A030206, g_3 = A130539, g_4 = A000731. - _Michael Somos_, Aug 24 2012",
				"Cubic AGM theta functions: a(q) (see A004016), b(q) (A005928), c(q) (A005882).",
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"reference": [
				"J. H. Conway and N. J. A. Sloane, \"Sphere Packings, Lattices and Groups\", Springer-Verlag, p. 111.",
				"N. J. Fine, Basic Hypergeometric Series and Applications, Amer. Math. Soc., 1988; p. 79, Eq. (32.35) and (32.351)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A033687/b033687.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e (terms 0..1000 from T. D. Noe)",
				"J. M. Borwein and P. B. Borwein, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-1991-1010408-0\"\u003eA cubic counterpart of Jacobi's identity and the AGM\u003c/a\u003e, Trans. Amer. Math. Soc., 323 (1991), no. 2, 691-701. MR1010408 (91e:33012) see page 697.",
				"S. Cynk and K. Hulek, \u003ca href=\"http://arXiv.org/abs/math/0509424\"\u003eConstruction and examples of higher-dimensional modular Calabi-Yau manifolds\u003c/a\u003e, arXiv:math/0509424 [math.AG], 2005-2006.",
				"Andrew Granville and Ken Ono, \u003ca href=\"https://doi.org/10.1090/S0002-9947-96-01481-X\"\u003eDefect Zero p-blocks for Finite Simple Groups\u003c/a\u003e, Transactions of the American Mathematical Society, Vol. 348 (1996), pp. 331-347.",
				"G.-N. Han and Ken Ono, \u003ca href=\"http://www-irma.u-strasbg.fr/~guoniu/hook/hh3core\"\u003eHook lengths and 3-cores\u003c/a\u003e",
				"J. Lovejoy and O. Mallet, \u003ca href=\"http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.206.472\"\u003en-color overpartitions, twisted divisor functions, and Rogers-Ramanujan identities\u003c/a\u003e, South East Asian J. Math. Math. Sci., 6 (2008), 23-36. [From _Jeremy Lovejoy_, Jun 12 2009]",
				"G. Nebe and N. J. A. Sloane, \u003ca href=\"http://www.math.rwth-aachen.de/~Gabriele.Nebe/LATTICES/A2.html\"\u003eHome page for hexagonal (or triangular) lattice A2\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Euler transform of period 3 sequence [1, 1, -2, ...].",
				"Expansion of q^(-1/3) * eta(q^3)^3 / eta(q) in powers of q.",
				"a(4*n + 1) = a(n). - _Michael Somos_, Dec 06 2004",
				"a(n) = b(3*n + 1) where b(n) is multiplicative and b(p^e) = 0^e if p = 3, b(p^e) = e+1 if p == 1 (mod 6), b(p^e) = (1+(-1)^e)/2 if p == 2, 5 (mod 6). - _Michael Somos_, May 20 2005",
				"Given g.f. A(x), B(q) = q * A(q^3) satisfies 0 = f(B(q), B(q^2), B(q^4)) where f(u, v, w) = u^2*w - 2*u*w^2 - v^3. - _Michael Somos_, Dec 06 2004",
				"Given g.f. A(x), B(q)= q * A(q^3) satisfies 0 = f(B(q), B(q^2), B(q^3), B(q^6)) where f(u1, u2, u3, u6) = u1*u3^2 + u1*u6^2 - u1*u3*u6 - u2^2*u3. - _Michael Somos_, May 20 2005",
				"Given g.f. A(x), B(q) = q * A(q^3) satisfies 0 = f(B(q), B(q^2), B(q^3), B(q^6)) where f(u1, u2, u3, u6) = u2*u3^2 + 2*u2*u3*u6 + 4*u2*u6^2 - u1^2*u6. - _Michael Somos_, May 20 2005",
				"G.f.: Product_{k\u003e0} (1 - x^(3*k))^3 / (1 - x^k).",
				"G.f.: Sum_{k in Z} x^k / (1 - x^(3*k + 1)) = Sum_{k in Z} x^k / (1 - x^(6*k + 2)). - _Michael Somos_, Nov 03 2005",
				"Expansion of q^(-1) * c(q^3) / 3 = q^(-1) * (a(q) - b(q)) / 9 in powers of q^3 where a(), b(), c() are cubic AGM theta functions. - _Michael Somos_, Dec 25 2007",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (27 t)) = 3^(1/2) (t/i) g(t) where q = exp(2 Pi i t) and g(t) is the g.f. for A005928.",
				"a(n) = Sum_{d|3n+1} LegendreSymbol{d,3} - _Brian Hopkins_, May 13 2008",
				"q-series for a(n): Sum_{n \u003e= 0} q^(n^2+n)(1-q)(1-q^2)...(1-q^n)/((1-q^(n+1))(1-q^(n+2))...(1-q^(2n+1))). [From _Jeremy Lovejoy_, Jun 12 2009]",
				"a(n) = A002324(3*n + 1). 3*a(n) = A005882(n) = A033685(3*n + 1). - _Michael Somos_, Apr 04 2003",
				"G.f.: (2 * psi(x^2) * f(x^2, x^4) + phi(x) * f(x^1, x^5)) / 3 where phi(), psi() are Ramanujan theta functions and f(, ) is Ramanujan's general theta function. - _Michael Somos_, Sep 07 2018"
			],
			"example": [
				"G.f. = 1 + x + 2*x^2 + 2*x^4 + x^5 + 2*x^6 + x^8 + 2*x^9 + 2*x^10 + 2*x^12 + 2*x^14 + ...",
				"G.f. = q + q^4 + 2*q^7 + 2*q^13 + q^16 + 2*q^19 + q^25 + 2*q^28 + 2*q^31 + 2*q^37 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 0, 0, DivisorSum[ 3 n + 1, KroneckerSymbol[ -3, #] \u0026]]; (* _Michael Somos_, Sep 23 2013 *)",
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x^3]^3 / QPochhammer[ x], {x, 0, n}]; (* _Michael Somos_, Sep 01 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, sumdiv( 3*n + 1, d, kronecker( -3, d)))}; /* _Michael Somos_, Nov 03 2005 */",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^3 + A)^3 / eta(x + A), n))};",
				"(PARI) {a(n) = my(A, p, e); if( n\u003c0, 0, A = factor( 3*n + 1); prod( k=1, matsize(A)[1], [p, e] = A[k, ]; if( p==3, 0, p%6==1, e+1, 1-e%2)))}; /* _Michael Somos_, May 06 2015 */",
				"(MAGMA) Basis( ModularForms( Gamma1(9), 1), 316) [2]; /* _Michael Somos_, May 06 2015 */"
			],
			"xref": [
				"Cf. A000731, A002324, A005882, A006938, A033685.",
				"Cf. A045831, A053723, A081622."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"references": 50,
			"revision": 59,
			"time": "2021-03-12T22:24:42-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}