{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A124419",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 124419,
			"data": "1,1,1,2,4,10,25,75,225,780,2704,10556,41209,178031,769129,3630780,17139600,87548580,447195609,2452523325,13450200625,78697155750,460457244900,2859220516290,17754399678409,116482516809889,764214897046969,5277304280371714",
			"name": "Number of partitions of the set {1,2,...n} having no blocks that contain both odd and even entries.",
			"comment": [
				"Column 0 of A124418."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A124419/b124419.txt\"\u003eTable of n, a(n) for n = 0..500\u003c/a\u003e",
				"A. Dzhumadil’daev and D. Yeliussizov, \u003ca href=\"http://arxiv.org/abs/1408.6764v1\"\u003ePath decompositions of digraphs and their applications to Weyl algebra\u003c/a\u003e, arXiv preprint arXiv:1408.6764v1, 2014. [Version 1 contained many references to the OEIS, which were removed in Version 2. - _N. J. A. Sloane_, Mar 28 2015]",
				"Askar Dzhumadil’daev and Damir Yeliussizov, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v22i4p10\"\u003eWalks, partitions, and normal ordering\u003c/a\u003e, Electronic Journal of Combinatorics, 22(4) (2015), #P4.10."
			],
			"formula": [
				"a(n) = Q[n](1,1,0), where the polynomials Q[n]=Q[n](t,s,x) are defined by Q[0]=1; Q[n]=t*dQ[n-1]/dt + x*dQ[n-1]/ds + x*dQ[n-1]/dx + t*Q[n-1] if n is odd and Q[n]=x*dQ[n-1]/dt + s*dQ[n-1]/ds + x*dQ[n-1]/dx + s*Q[n-1] if n is even.",
				"a(n) = A000110(floor(n/2)) * A000110(ceiling(n/2)). - _Alois P. Heinz_, Oct 23 2013"
			],
			"example": [
				"a(4) = 4 because we have 13|24, 1|24|3, 13|2|4 and 1|2|3|4."
			],
			"maple": [
				"Q[0]:=1: for n from 1 to 30 do if n mod 2 = 1 then Q[n]:=expand(t*diff(Q[n-1],t)+x*diff(Q[n-1],s)+x*diff(Q[n-1],x)+t*Q[n-1]) else Q[n]:=expand(x*diff(Q[n-1],t)+s*diff(Q[n-1],s)+x*diff(Q[n-1],x)+s*Q[n-1]) fi od: for n from 0 to 30 do Q[n]:=Q[n] od: seq(subs({t=1,s=1,x=0},Q[n]),n=0..30);",
				"# second Maple program:",
				"with(combinat):",
				"a:= n-\u003e bell(floor(n/2))*bell(ceil(n/2)):",
				"seq(a(n), n=0..30);  # _Alois P. Heinz_, Oct 23 2013"
			],
			"mathematica": [
				"a[n_] := BellB[Floor[n/2]]*BellB[Ceiling[n/2]]; Table[a[n], {n, 0, 30}] (* _Jean-François Alcover_, May 20 2015, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A000110, A124418, A124420, A124421, A124422, A124423, A274310.",
				"Column k=2 of A275069."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Emeric Deutsch_, Oct 31 2006",
			"references": 14,
			"revision": 31,
			"time": "2016-07-15T15:37:32-04:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}