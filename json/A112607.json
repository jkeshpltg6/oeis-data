{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A112607",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 112607,
			"data": "1,1,0,1,0,0,1,0,0,0,1,0,1,1,0,2,0,0,1,0,0,1,1,0,0,0,0,1,1,0,0,0,0,1,0,0,2,1,0,1,1,0,1,0,0,1,1,0,1,0,0,1,0,0,0,1,0,2,0,0,0,0,0,0,1,0,1,1,0,0,0,0,2,1,0,1,0,0,3,0,0,1,1,0,0,0,0,1,0,0,1,2,0,1,0,0,0,0,0,0,1,0,1,1,0",
			"name": "Number of representations of n as a sum of a triangular number and twelve times a triangular number.",
			"comment": [
				"Ramanujan theta functions: f(q) := Prod_{k\u003e=1} (1-(-q)^k) (see A121373), phi(q) := theta_3(q) := Sum_{k=-oo..oo} q^(k^2) (A000122), psi(q) := Sum_{k=0..oo} q^(k*(k+1)/2) (A010054), chi(q) := Prod_{k\u003e=0} (1+q^(2k+1)) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A112607/b112607.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"M. D. Hirschhorn, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2004.08.045\"\u003eThe number of representations of a number by various forms\u003c/a\u003e, Discrete Mathematics 298 (2005), 205-211",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 1/2*( d_{1, 3}(8n+13) - d_{2, 3}(8n+13) ) where d_{a, m}(n) equals the number of divisors of n which are congruent to a mod m.",
				"Expansion of q^(-13/8)*(eta(q^2)*eta(q^24))^2/(eta(q)*eta(q^12)) in powers of q. - _Michael Somos_, Sep 29 2006",
				"Expansion of psi(q)*psi(q^12) in powers of q where psi() is a Ramanujan theta function. - _Michael Somos_, Sep 29 2006",
				"Euler transform of period 24 sequence [ 1, -1, 1, -1, 1, -1, 1, -1, 1, -1, 1, 0, 1, -1, 1, -1, 1, -1, 1, -1, 1, -1, 1, -2, ...]. - _Michael Somos_, Sep 29 2006",
				"a(3n+2)=0. - _Michael Somos_, Sep 29 2006"
			],
			"example": [
				"a(15) = 2 since we can write 15 = 15 + 12*0 = 3 + 12*1."
			],
			"mathematica": [
				"a[n_] := DivisorSum[8n+13, KroneckerSymbol[-3, #]\u0026]/2; Table[a[n], {n, 0, 104}] (* _Jean-François Alcover_, Dec 04 2015, adapted from PARI *)"
			],
			"program": [
				"(PARI) {a(n)=if(n\u003c0, 0, n=8*n+13; sumdiv(n, d, kronecker(-3,d))/2)} /* _Michael Somos_, Sep 29 2006 */",
				"(PARI) {a(n)=local(A); if(n\u003c0, 0, A=x*O(x^n); polcoeff( eta(x^2+A)^2*eta(x^24+A)^2/eta(x+A)/eta(x^12+A), n))} /* _Michael Somos_, Sep 29 2006 */"
			],
			"xref": [
				"A123484(24n+15) = 2*a(n). A112609(3n+4) = a(n)."
			],
			"keyword": "nonn",
			"offset": "0,16",
			"author": "_James A. Sellers_, Dec 21 2005",
			"references": 15,
			"revision": 20,
			"time": "2021-03-12T22:24:43-05:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}