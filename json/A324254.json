{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A324254",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 324254,
			"data": "1,-1,1,2,-3,1,-6,8,3,-6,1,24,-30,-20,20,15,-10,1,-120,144,90,40,-90,-120,-15,40,45,-15,1,720,-840,-504,-420,504,630,280,210,-210,-420,-105,70,105,-21,1,-5040,5760,3360,2688,1260,-3360,-4032,-3360,-1260,-1120,1344,2520,1120,1680,105,-420,-1120,-420,112,210,-28,1",
			"name": "Signed version of the partition array A036039 (signed M_2 multinomial numbers).",
			"comment": [
				"The length of row n is p(n) = A000041(n), for n \u003e= 1 (partition numbers).",
				"The Abramowitz-Stegun order of partitions is used.",
				"The triangle obtained by summing the terms belonging to like number of parts is A008275 (Stirling1).",
				"This partition array T(n, k) and its partition row polynomials enter in the formula expressing the r-th power sums for the n-th elementary symmetric functions psigma(n, r) := Sum_{1 \u003c= i1 \u003c i2 \u003c ... \u003c in \u003c= N} (x_{i1}*x_{i2}* ... *x_{in})^r  in terms of the power sums of the first elementary function {ps(j*r)}_{j=1..n} with  ps(i) :=  psigma(1, i) = Sum_{j = 1...N} (x_j)^i, for n \u003e= 2 (for n = 1 it becomes an identity) and r \u003e= 0. Note that psigma(n, 0) = binomial(N,n). The formula is given below.",
				"The number N \u003e= 1 of indeterminates is taken as fixed, and it is suppressed in the notations.",
				"Note that only after the power sums have been replaced by the elementary symmetric functions (via the Girard-Waring formula) the result for the symmetric function psigma(n, r) becomes a function with integer coefficients (as guaranteed by the main theorem of symmetric functions).",
				"The coefficients of partitions of 2*r with no more than r parts for psigma(2, r) in terms of the elementary symmetric functions {e_j} are given in array A308684."
			],
			"link": [
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP?Res=150\u0026amp;Page=831\u0026amp;Submit=Go\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy]."
			],
			"formula": [
				"T(n, k) = (-1)^n*n!/Product_{j=1..n} (-1)^{a(n,k,j)}*j^a(n,k,j)*a(n,k,j)!, with the k-th partition of n \u003e= 1 with m parts in Abramowitz-Stegun order written as Product_{j=1..n} j^a(n,k,j) with nonnegative integers a(n,k,j) satisfying Sum_{j=1..n} j*a(n,k,j) = n, for k = 1.. A000041(n), and the number of parts is Sum_{j=1..n} a(n,k,j) =: m(n,k). Hence the sign is (-1)^{n + m(n,k)}.",
				"The formula for psigma(n, r), the r-th power sums of the n-th elementary symmetric functions in terms of the power sums {ps(j)}_{j=1..r*n}), is  psigma(n, r) = (1/n!)*Sum_{k=1..p(n)} T(n,k) * Product_{j=1..n} ps(j*r)^{a(n,k,j)}, for n \u003e= 1 and r \u003e= 0, with the k-th partition of n as given in the T(n, k) formula."
			],
			"example": [
				"The partition array T(n, k) begins:",
				"n\\k   1     2    3     4    5     6    7    8     9    10    11  12   13   14 15",
				"--------------------------------------------------------------------------------",
				"1:    1",
				"2:   -1     1",
				"3:    2    -3    1",
				"4    -6     8    3    -6    1",
				"5    24   -30  -20    20   15   -10    1",
				"6  -120   144   90    40  -90  -120  -15   40    45   -15    1",
				"7   720  -840 -504  -420  504   630  280  210  -210  -420  -105  70  105  -21  1",
				"...",
				"n = 8: [-5040] [5760, 3360, 2688, 1260] [-3360, -4032, -3360, -1260, -1120] [1344, 2520, 1120, 1680, 105] [-420, -1120, -420] [112, 210] [-28] [1];",
				"n = 9:  [40320] [-45360, -25920, -20160, -18144] [25920, 30240, 24192, 11340, 9072, 15120, 2240] [-10080, -18144, -15120, -11340, -10080, -2520] [3024, 7560, 3360, 7560, 945] [-756, -2520, -1260] [168, 378] [-36] [1];",
				"n = 10: [-362880] [403200, 226800, 172800, 151200, 72576] [-226800, -259200, -201600, -181440, -75600, -120960, -56700, -50400] [86400, 151200, 120960, 56700, 90720, 151200, 22400, 18900, 25200] [-25200, -60480, -50400, -56700, -50400, -25200, -945] [6048, 18900, 8400, 25200, 4725] [-1260, -5040, -3150] [240, 630] [-45] [1];",
				"The brackets collect numbers belonging to the same number of parts m = m(n,k).",
				"...",
				"--------------------------------------------------------------------------------",
				"The first psigma(n, r) are: (the N indeterminates are suppressed)",
				"psigma(1, r) =  ps(1*r);",
				"psigma(2, r) = (1/2)*(-ps(2*r) + ps(r)^2);",
				"psigma(3, r) = (1/3!)*(2*ps(3*r) - 3*ps(1*r)*ps(2*r) + ps(1*r)^3);",
				"psigma(4, r) = (1/4!)*(-6*ps(4*r) + 8*ps(1*r)*ps(3*r) + 3*ps(2*r)^2 - 6*ps(1*r)^2*ps(2*r) + ps(1*r)^4);",
				"psigma(5, r) = (1/5!)*(24*ps(5*r) - 30*ps(1*r)*p(4*r) - 20*ps(2*r)^2*ps(3*r)  + 20*ps(1*r)^2*ps(3*r) + 15*ps(1*r)*ps(2*r)^2 - - 10*ps(1*r)^3*ps(2*r) + 1*ps(1*r)^5);",
				"psigma(6, r) = (1/6!)*(-120*ps(6*r) + 144*ps(1*r)*ps(5*r) + 90*ps(2*r)*ps(4*r) + 40*(ps(3*r))^2 - 90*ps(1*r)^2*ps(4*r) - 120*ps(1*r)*ps(2*r)*ps(3*r) - 15*ps(2*r)^3 + 40*(ps(1*r))^3*ps(3*r) + 45*ps(1*r)^2*ps(2*r)^2 - 15*ps(1*r)^4*ps(2*r) + ps(1*r)^6:",
				"...",
				"--------------------------------------------------------------------------------"
			],
			"xref": [
				"Cf. A008275, A036039 (unsigned), A308684."
			],
			"keyword": "sign,tabf",
			"offset": "1,4",
			"author": "_Wolfdieter Lang_, Jul 05 2019",
			"references": 1,
			"revision": 12,
			"time": "2020-10-05T00:13:05-04:00",
			"created": "2019-07-24T02:42:27-04:00"
		}
	]
}