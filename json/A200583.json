{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A200583",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 200583,
			"data": "1,2,1,4,1,8,3,1,16,1,32,10,4,1,64,1,128,35,5,1,256,22,1,512,126,6,1,1024,1,2048,462,134,46,7,1,4096,1,8192,1716,8,1,16384,866,94,1,32768,6435,485,9,1,65536,1,131072,24310,5812,190,10,1,262144,1,524288",
			"name": "Table read by rows, n \u003e= 1, 1 \u003c= k \u003c= card(divisors(n)), T(n,k) meanders of length n and central angle of 360/d degrees, d the k-th divisor of n.",
			"comment": [
				"A meander is a closed curve drawn by arcs of equal length and central angle of equal magnitude, starting with a positively oriented arc."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A200583/b200583.txt\"\u003eTable of n, a(n) for n = 1..482\u003c/a\u003e",
				"Peter Luschny, \u003ca href=\"http://oeis.org/wiki/User:Peter_Luschny/Meander\"\u003eMeander\u003c/a\u003e."
			],
			"formula": [
				"T(n,k) = A198060(d-1,n/d-1) where d is the k-th divisor of n (the divisors in natural order)."
			],
			"example": [
				"[ 1]            1",
				"[ 2]          2, 1",
				"[ 3]          4, 1",
				"[ 4]        8, 3, 1",
				"[ 5]         16, 1",
				"[ 6]      32, 10, 4, 1",
				"[ 7]         64, 1",
				"[ 8]     128, 35, 5, 1",
				"[ 9]       256, 22, 1",
				"[10]     512, 126, 6, 1",
				"[11]        1024, 1",
				"[12] 2048, 462, 134, 46, 7, 1"
			],
			"maple": [
				"A200583_row := proc(n) local i;",
				"seq(A198060(i-1,n/i-1),i=numtheory[divisors](n)) end:",
				"seq(print(A200583_row(i)),i=1..12);"
			],
			"mathematica": [
				"A198060[m_, n_] := Sum[Sum[Sum[(-1)^(j+i)*Binomial[i, j]*Binomial[n, k]^(m+1)*(n+1)^j*(k+1)^(m-j)/(k+1)^m, {i, 0, m}], {j, 0, m}], {k, 0, n}]; row[n_] := Table[A198060[d-1, n/d-1], {d, Divisors[n]}]; Table[row[n], {n, 1, 20}] // Flatten (* _Jean-François Alcover_, Feb 25 2014, after Maple *)"
			],
			"xref": [
				"Cf. A198060, A199932, A200062."
			],
			"keyword": "nonn,tabf",
			"offset": "1,2",
			"author": "_Peter Luschny_, Nov 20 2011",
			"references": 1,
			"revision": 14,
			"time": "2014-03-03T04:25:09-05:00",
			"created": "2011-11-21T12:14:08-05:00"
		}
	]
}