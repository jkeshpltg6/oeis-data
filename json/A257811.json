{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A257811",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 257811,
			"data": "1,8,3,10,5,12,7,2,9,4,11,6,1,8,3,10,5,12,7,2,9,4,11,6,1,8,3,10,5,12,7,2,9,4,11,6,1,8,3,10,5,12,7,2,9,4,11,6,1,8,3,10,5,12,7,2,9,4,11,6,1,8,3,10,5,12,7,2,9,4,11,6,1,8,3,10,5,12,7,2,9,4,11,6,1,8,3,10,5,12,7,2,9,4,11,6,1,8,3,10,5,12,7,2,9,4,11,6",
			"name": "Circle of fifths cycle (clockwise).",
			"comment": [
				"The twelve notes dividing the octave are numbered 1 through 12 sequentially. This sequence begins at a certain note, travels up a perfect fifth twelve times (seven semitones), and arrives back at the same note. If justly tuned fifths are used, the final note will be sharp by the Pythagorean comma (roughly 23.46 cents or about a quarter of a semitone).",
				"Period 12: repeat [1, 8, 3, 10, 5, 12, 7, 2, 9, 4, 11, 6]. - _Omar E. Pol_, May 12 2015"
			],
			"link": [
				"Alonso del Arte, Alvin Hoover Belt, Daniel Forgues, and Charles R Greathouse IV, \u003ca href=\"https://oeis.org/wiki/The_multi-faceted_reach_of_the_OEIS#Music\"\u003eThe multi-faceted reach of the OEIS: Music\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Circle_of_fifths\"\u003eCircle of fifths\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Pythagorean_comma\"\u003ePythagorean comma\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_OEIS:_Section_Mu#music\"\u003eOEIS index entry for music\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_12\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,0,0,0,0,0,0,0,0,0,0,1)."
			],
			"formula": [
				"Periodic with period 12: a(n) = 1 + 7(n-1)  mod 12.",
				"From _Colin Barker_, Nov 15 2019: (Start)",
				"G.f.: x*(1 + 8*x + 3*x^2 + 10*x^3 + 5*x^4 + 12*x^5 + 7*x^6 + 2*x^7 + 9*x^8 + 4*x^9 + 11*x^10 + 6*x^11) / (1 - x^12).",
				"a(n) = a(n-12) for n\u003e12.",
				"(End)"
			],
			"example": [
				"For a(3), 1+7+7 = 3 (mod 12).",
				"For a(4), 1+7+7+7 = 10 (mod 12)."
			],
			"mathematica": [
				"PadRight[{}, 100, {1, 8, 3, 10, 5, 12, 7, 2, 9, 4, 11, 6}] (* _Vincenzo Librandi_, May 10 2015 *)",
				"LinearRecurrence[{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},{1, 8, 3, 10, 5, 12, 7, 2, 9, 4, 11, 6},108] (* _Ray Chandler_, Aug 27 2015 *)"
			],
			"program": [
				"(MAGMA) [1+7*(n-1) mod(12): n in [1..80]]; // _Vincenzo Librandi_, May 10 2015",
				"(PARI) a(n)=7*(n-1)%12+1 \\\\ _Charles R Greathouse IV_, Jun 02 2015",
				"(PARI) Vec(x*(1 + 8*x + 3*x^2 + 10*x^3 + 5*x^4 + 12*x^5 + 7*x^6 + 2*x^7 + 9*x^8 + 4*x^9 + 11*x^10 + 6*x^11) / (1 - x^12) + O(x^80)) \\\\ _Colin Barker_, Nov 15 2019"
			],
			"xref": [
				"Cf. A194835 (Contains this circle of fifths sequence), A007337 (sqrt3 sequence), A258054 (counterclockwise circle of fifths cycle)."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Peter Woodward_, May 09 2015",
			"ext": [
				"Extended by _Ray Chandler_, Aug 27 2015"
			],
			"references": 1,
			"revision": 39,
			"time": "2019-11-15T11:12:57-05:00",
			"created": "2015-05-12T20:20:53-04:00"
		}
	]
}