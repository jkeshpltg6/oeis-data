{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A263023",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 263023,
			"data": "1,2,4,4,14,9,25,15,13,50,19,35,77,42,32,37,122,43,72,153,54,88,63,52,113,235,121,252,130,40,156,108,339,71,375,128,134,210,144,151,466,96,504,256,523,90,96,304,618,313,214,657,134,233,240,247,755,255",
			"name": "Largest integer k such that prime(n+1) \u003c prime(n)^(1+1/k).",
			"comment": [
				"Firoozbakht's conjecture: prime(n+1) \u003c prime(n)^(1+1/n).",
				"Firoozbakht's conjecture restated for this sequence: a(n) \u003e= n.",
				"I further conjecture that n = 1,2,4 are the only values of n with a(n) = n.",
				"Record values of a(n) occur when prime(n) and prime(n+1) are twin primes.",
				"Upper bound for all n: a(n) \u003c (1/2)*(prime(n)+2)*log(prime(n))."
			],
			"reference": [
				"Paulo Ribenboim, The little book of bigger primes, 2nd edition, Springer, 2004, p. 185."
			],
			"link": [
				"A. Kourbatov, \u003ca href=\"http://arxiv.org/abs/1503.01744\"\u003eVerification of the Firoozbakht conjecture for primes up to four quintillion\u003c/a\u003e, arXiv:1503.01744 [math.NT], 2015.",
				"A. Kourbatov, \u003ca href=\"http://arxiv.org/abs/1506.03042\"\u003eUpper bounds for prime gaps related to Firoozbakht's conjecture\u003c/a\u003e, arXiv:1506.03042 [math.NT], 2015.",
				"Carlos Rivera, \u003ca href=\"http://www.primepuzzles.net/conjectures/conj_030.htm\"\u003eConjecture 30\u003c/a\u003e"
			],
			"formula": [
				"a(n) = floor(log(prime(n))/(log(prime(n+1)) - log(prime(n))))."
			],
			"example": [
				"prime(1)=2; a(1)=1 because k=1 is the largest k for which 3 \u003c 2^(1+1/k).",
				"prime(2)=3; a(2)=2 because k=2 is the largest k for which 5 \u003c 3^(1+1/k).",
				"prime(10)=29; a(10)=50 because k=50 is the largest k for which 31 \u003c 29^(1+1/k)."
			],
			"mathematica": [
				"Table[Floor[Log@ Prime@ n /(Log@ Prime[n + 1] - Log@ Prime@ n)], {n, 58}] (* _Michael De Vlieger_, Oct 08 2015 *)"
			],
			"program": [
				"(MAGMA) [Floor(Log(NthPrime(n))/(Log(NthPrime(n+1))-Log(NthPrime(n)))): n in [1..60]]; // _Vincenzo Librandi_, Oct 08 2015",
				"(PARI) a(n) = floor(log(prime(n))/(log(prime(n+1)) - log(prime(n)))) \\\\ _Michel Marcus_, Oct 10 2015"
			],
			"xref": [
				"Cf. A000040, A002386, A182514, A182519, A245396, A246776, A246778, A249669."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Alexei Kourbatov_, Oct 08 2015",
			"ext": [
				"More terms from _Vincenzo Librandi_, Oct 08 2015"
			],
			"references": 1,
			"revision": 24,
			"time": "2015-10-26T22:21:49-04:00",
			"created": "2015-10-26T22:21:49-04:00"
		}
	]
}