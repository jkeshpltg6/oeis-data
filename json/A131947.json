{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A131947",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 131947,
			"data": "1,-1,1,-5,6,-1,8,-13,1,-6,12,-5,14,-8,6,-29,18,-1,20,-30,8,-12,24,-13,31,-14,1,-40,30,-6,32,-61,12,-18,48,-5,38,-20,14,-78,42,-8,44,-60,6,-24,48,-29,57,-31,18,-70,54,-1,72,-104,20,-30,60,-30,62",
			"name": "Expansion of (1 - (phi(-q) * phi(-q^3))^2)/4 in powers of q where phi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"reference": [
				"N. J. Fine, Basic Hypergeometric Series and Applications, Amer. Math. Soc., 1988; p. 85, Eq. (32.66)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A131947/b131947.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"a(n) is multiplicative with a(2^e) = 3 - 2^(e+1), a(3^e) = 1, a(p^e) = (p^(e+1) - 1) / (p-1) if p\u003e3.",
				"G.f.: Sum_{k\u003e0} k * (-x)^k / (1 - x^k) * Kronecker(9, k) = ((theta_3(-x) * theta_3(-x^3))^2 - 1) / 4.",
				"a(n) = -(-1)^n * A113262(n). -4 * a(n) = A131946(n) unless n=0."
			],
			"example": [
				"G.f. = x - x^2 + x^3 - 5*x^4 + 6*x^5 - x^6 + 8*x^7 - 13*x^8 + x^9 - 6*x^10 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (1 - (EllipticTheta[ 4, 0, q] EllipticTheta[ 4, 0, q^3])^2) / 4, {q, 0, n}]; (* _Michael Somos_, Nov 11 2015 *)",
				"a[ n_] := SeriesCoefficient[ (1 - (QPochhammer[ q] QPochhammer[ q^3])^4 / (QPochhammer[ q^2] QPochhammer[ q^6])^2) / 4, {q, 0, n}]; (* _Michael Somos_, Nov 11 2015 *)",
				"a[ n_] := If[ n \u003c 1, 0, Sum[ d {0, 1, -1, 0, -1, 1}[[Mod[ d, 6] + 1]], {d, Divisors @ n}]]; (* _Michael Somos_, Nov 11 2015 *)",
				"a[ n_] := If[ n \u003c 1, 0, Sum[ n/d {6, 1, -3, -2, -3, 1}[[Mod[ d, 6] + 1]], {d, Divisors @ n}]]; (* _Michael Somos_, Nov 11 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, 0, sumdiv(n, d, d*((abs(d%6-3) == 2) - (abs(d%6-3) == 1))))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( (1 - (eta(x + A) * eta(x^3 + A))^4 / (eta(x^2 + A) * eta(x^6 + A))^2) / 4, n))};",
				"(PARI) {a(n) = my(A, p, e); if( n\u003c1, 0, A = factor(n); prod(k=1, matsize(A)[1], [p, e] = A[k, ]; if( p==2, 3 - p^(e+1), p==3, 1, (p^(e+1) - 1) / (p-1) )))};"
			],
			"xref": [
				"Cf. A113262, A131946."
			],
			"keyword": "sign,mult",
			"offset": "1,4",
			"author": "_Michael Somos_, Jul 30 2007",
			"references": 2,
			"revision": 20,
			"time": "2021-03-12T22:24:44-05:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}