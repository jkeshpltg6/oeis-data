{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A101083",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 101083,
			"data": "2,3,4,4,3,5,4,6,6,7,6,7,6,5,8,8,7,8,7,7,8,7,6,7,9,8,8,11,10,11,10,11,11,10,12,12,11,10,9,9,8,11,10,9,10,9,8,11,13,13,12,11,10,11,14,15,14,13,12,14,13,12,13,13,14,14,13,12,11,13,12,15,14,13,14,13,13,17,16,17",
			"name": "Largest k such that the product (n+1)(n+2)...(n+k) has at least k distinct prime factors.",
			"comment": [
				"This sequence is based on a slightly weaker, but still unproved, version of Grimm's conjecture: If there is no prime in the interval [n+1, n+k], then the product (n+1)(n+2)...(n+k) has at least k distinct prime divisors. We have a(n) \u003e= A059686(n), with the two sequences first differing at n=70. Computing a(n) is much faster than computing A059686.",
				"It seems that Grimm's conjecture could have another (but not a weak) form: let p(1)...p(i) be a subset of prime numbers such that while n is integer, 0 \u003c n \u003c i, for any n, p(n) \u003c p(n+1). Then there exists such sequence c(1)...c(i) where each term is a composite number, c(n+1) = c(n) + 1, and c(n) == 0 (mod p(n)). - _Sergey Pavlov_, Mar 21 2017"
			],
			"reference": [
				"See A059686"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A101083/b101083.txt\"\u003eTable of n, a(n) for n=1..1000\u003c/a\u003e",
				"M. Waldschmidt, \u003ca href=\"https://arxiv.org/abs/math/0312440\"\u003eOpen Diophantine problems\u003c/a\u003e, arXiv:math/0312440 [math.NT], 2003-2004, pages 6-7."
			],
			"example": [
				"a(6) = 5 because 7*8*9*10*11 has 5 prime factors and 7*8*9*10*11*12 does not have 6 prime factors."
			],
			"mathematica": [
				"Table[k=2; While[Length[FactorInteger[Times@@Range[n0+1, n0+k]]]\u003e=k, k++ ]; k-1, {n0, 100}]"
			],
			"xref": [
				"Cf. A059686 (Grimm numbers)."
			],
			"keyword": "easy,nonn",
			"offset": "1,1",
			"author": "_T. D. Noe_, Nov 30 2004",
			"references": 4,
			"revision": 11,
			"time": "2017-03-24T22:11:19-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}