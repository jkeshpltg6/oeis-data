{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A308900",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 308900,
			"data": "1,6,4,66,34,666,334,6666,3334,66666,33334,666666,333334,6666666,3333334,66666666,33333334,666666666,333333334,6666666666,3333333334,66666666666,33333333334,666666666666,333333333334,6666666666666,3333333333334,66666666666666,33333333333334",
			"name": "An explicit example of an infinite sequence with a(1)=1 and, for n \u003e= 2, a(n) and S(n) = Sum_{i=1..n} a(i) have no digit in common.",
			"comment": [
				"Used in a proof that the initial terms of A309151 are correct.",
				"The S(n) sequence is 1, 7, 11, 77, 111, 777, 1111, 7777, 11111, 77777, ...",
				"A093137 interleaved with positive terms of A002280. - _Felix Fröhlich_, Jul 15 2019"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A308900/b308900.txt\"\u003eTable of n, a(n) for n = 1..1999\u003c/a\u003e",
				"R. Israel, \u003ca href=\"http://list.seqfan.eu/pipermail/seqfan/2019-July/018885.html\"\u003eRe: Help with a(n) and a cumulative sum\u003c/a\u003e, Seqfan (Jul 15 2019).",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (-1,10,10)."
			],
			"formula": [
				"For even n \u003e= 2, a(n) = 6666...66 (with n/2 6's). For odd n \u003e= 5, a(n) = 3333...334 (with (n-3)/2 3's and a single 4).",
				"From _Robert Israel_, Jul 15 2019: (Start)",
				"G.f. (1+7*x)/((1+x)*(1-10*x^2)).",
				"a(n) = -a(n - 1) + 10*a(n - 2) + 10*a(n - 3). (End)",
				"a(-n) = a(n+1). - _Paul Curtz_, Jul 18 2019",
				"a(n) = (1/60)*(-40*(-1)^n + (1 + (-1)^n)*(2^(2+n/2)*5^(1+n/2)) + (1 + (-1)^(n+1))*10^((1+n)/2)). - _Stefano Spezia_, Jul 20 2019"
			],
			"maple": [
				"1, seq(op([6*(10^i-1)/9, 3*(10^i-1)/9+1]), i=1..30); # _Robert Israel_, Jul 15 2019"
			],
			"mathematica": [
				"CoefficientList[Series[(1 + 7 x)/((1 + x) (1 - 10 x^2)), {x, 0, 26}], x] (* _Michael De Vlieger_, Jul 18 2019 *)",
				"LinearRecurrence[{-1,10,10},{1,6,4},30] (* _Harvey P. Dale_, Jan 02 2022 *)"
			],
			"program": [
				"(PARI) Vec((1+7*x)/((1+x)*(1-10*x^2)) + O(x^20)) \\\\ _Felix Fröhlich_, Jul 15 2019",
				"(PARI) a(n) = if(n==1, 1, if(n%2==0, 6*(10^(n/2)-1)/9, 3*(10^((n-1)/2)-1)/9+1)) \\\\ _Felix Fröhlich_, Jul 15 2019",
				"(MAGMA) I:=[1,6,4]; [n le 3 select I[n] else - Self(n-1) + 10*Self(n-2) + 10*Self(n-3): n in [1..30]]; // _Vincenzo Librandi_, Ul 20 2019"
			],
			"xref": [
				"Cf. A002280, A093137, A309151.",
				"Cf. A289404."
			],
			"keyword": "nonn,base,easy,changed",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Jul 15 2019",
			"references": 2,
			"revision": 38,
			"time": "2022-01-02T12:14:59-05:00",
			"created": "2019-07-15T14:22:20-04:00"
		}
	]
}