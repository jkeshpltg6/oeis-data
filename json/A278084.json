{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A278084",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 278084,
			"data": "1,2,5,6,6,12,14,10,18,20,12,22,25,18,28,32,24,30,38,28,40,42,30,46,42,36,54,60,40,60,60,36,70,66,44,72,74,50,72,80,54,82,90,56,88,84,64,100,98,72,100,102,60,106,108,76,114,110,84,108,132,80,125,126",
			"name": "a(n) is 1/24 of the number of primitive integral quadruples with sum = 2*m and sum of squares = 6*m^2, where m = 2*n-1.",
			"comment": [
				"Set b(m) = a(n) for m = 2*n-1, and b(m) = 0 for m even.",
				"Conjecture: b(m) is multiplicative: for k \u003e= 1, b(2^k) = 0, and for p an odd prime, b(p^k) = p^(k-1)*b(p), with b(p) = p + 1 for p == (11, 13, 17, 19) (mod 20), b(p) = p - 1 for p == (1, 3, 7, 9) (mod 20), b(5) = 5.  It would be nice to have a proof of this."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A278084/b278084.txt\"\u003eTable of n, a(n) for n = 1..500\u003c/a\u003e",
				"Petros Hadjicostas, \u003ca href=\"/A278081/a278081.r.txt\"\u003eSlight modification of Mallows' R program\u003c/a\u003e. [To get the total counts for n = 1 to 120, with the zeros, i.e., the sequence (b(n): n \u003e= 1) shown in the comments above, type gc(1:120, 2, 6), where r = 2 and s = 6. To get the 1/24 of these counts with no zeros, type gc(seq(1,59,2), 2, 6)[,3]/24.]",
				"Colin Mallows, \u003ca href=\"/A278081/a278081_2.txt\"\u003eR programs for A278081-A278086\u003c/a\u003e."
			],
			"example": [
				"24*a(2) = 48 = 24*b(3) because of (-4,2,3,5) and (-2,0,1,7) (24 permutations each). For example, (-2) + 0 + 1 + 7 = 6 = 2*3 and (-2)^2 + 0^2 + 1^2 + 7^2 = 54 = 6*3^2 (with n = 2 and m = 3 = 2*2 - 1)."
			],
			"mathematica": [
				"sqrtint = Floor[Sqrt[#]]\u0026;",
				"q[r_, s_, g_] := Module[{d = 2s - r^2, h}, If[d \u003c= 0, d == 0 \u0026\u0026 Mod[r, 2] == 0 \u0026\u0026 GCD[g, r/2] == 1, h = Sqrt[d]; If[IntegerQ[h] \u0026\u0026 Mod[r+h, 2] == 0 \u0026\u0026 GCD[g, GCD[(r+h)/2, (r-h)/2]]==1, 2, 0]]] /. {True -\u003e 1, False -\u003e 0};",
				"a[n_] := Module[{m = 2n - 1, s}, s = 6m^2; Sum[q[2m - i - j, s - i^2 - j^2, GCD[i, j]] , {i, -sqrtint[s], sqrtint[s]}, {j, -sqrtint[s - i^2], sqrtint[s - i^2]}]/24];",
				"Table[an = a[n]; Print[n, \" \", an]; an, {n, 1, 100}] (* _Jean-François Alcover_, Sep 20 2020, after _Andrew Howroyd_ *)"
			],
			"program": [
				"(PARI)",
				"q(r, s, g)={my(d=2*s - r^2); if(d\u003c=0, d==0 \u0026\u0026 r%2==0 \u0026\u0026 gcd(g, r/2)==1, my(h); if(issquare(d, \u0026h) \u0026\u0026 (r+h)%2==0 \u0026\u0026 gcd(g, gcd((r+h)/2, (r-h)/2))==1, 2, 0))}",
				"a(n)={my(m=2*n-1, s=6*m^2); sum(i=-sqrtint(s), sqrtint(s), sum(j=-sqrtint(s-i^2), sqrtint(s-i^2), q(2*m-i-j, s-i^2-j^2, gcd(i,j)) ))/24} \\\\ _Andrew Howroyd_, Aug 02 2018"
			],
			"xref": [
				"Cf. A046897, A278081, A278082, A278083, A278085, A278086."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Colin Mallows_, Nov 14 2016",
			"ext": [
				"Terms a(51) and beyond from _Andrew Howroyd_, Aug 02 2018",
				"Name and example section edited by _Petros Hadjicostas_, Apr 21 2020"
			],
			"references": 6,
			"revision": 31,
			"time": "2020-09-20T08:20:19-04:00",
			"created": "2016-11-14T13:34:03-05:00"
		}
	]
}