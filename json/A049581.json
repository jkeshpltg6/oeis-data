{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A049581",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 49581,
			"data": "0,1,1,2,0,2,3,1,1,3,4,2,0,2,4,5,3,1,1,3,5,6,4,2,0,2,4,6,7,5,3,1,1,3,5,7,8,6,4,2,0,2,4,6,8,9,7,5,3,1,1,3,5,7,9,10,8,6,4,2,0,2,4,6,8,10,11,9,7,5,3,1,1,3,5,7,9,11,12,10,8,6,4,2,0,2,4,6,8,10,12",
			"name": "Table T(n,k) = |n-k| read by antidiagonals (n \u003e= 0, k \u003e= 0).",
			"comment": [
				"Commutative non-associative operator with identity 0. T(nx,kx) = x T(n,k). A multiplicative analog is A089913. - _Marc LeBrun_, Nov 14 2003",
				"For the characteristic polynomial of the n X n matrix M_n with entries M_n(i, j) = |i-j| see A203993. - _Wolfdieter Lang_, Feb 04 2018",
				"For the determinant of the n X n matrix M_n with entries M_n(i, j) = |i-j| see A085750. - _Bernard Schott_, May 13 2020",
				"a(n) = 0 iff n = 4 times triangular number (A046092). - _Bernard Schott_, May 13 2020"
			],
			"link": [
				"Peter Kagey, \u003ca href=\"/A049581/b049581.txt\"\u003eRows n = 0..125 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"G.f.: (x + y - 4xy + x^2y + xy^2)/((1-x)^2 (1-y)^2) (1-xy)) = (x/(1-x)^2 + y/(1-y)^2)/(1-xy). T(n,0) = T(0,n) = n; T(n+1,k+1) = T(n,k). - _Franklin T. Adams-Watters_, Feb 06 2006",
				"a(n) = |A002260(n+1)-A004736(n+1)| or a(n) = |((n+1)-t(t+1)/2) - (t*t+3*t+4)/2-(n+1))| where t=floor[(-1+sqrt(8*(n+1)-7))/2]. - _Boris Putievskiy_, Dec 24 2012; corrected by _Altug Alkan_, Sep 30 2015",
				"From _Robert Israel_, Sep 30 2015: (Start)",
				"If b(n) = a(n+1) - 2*a(n) + a(n-1), then for n \u003e= 3 we have",
				"  b(n) = -1 if n = (j^2+5j+4)/2 for some integer j \u003e= 1",
				"  b(n) = -3 if n = (j^2+5j+6)/2 for some integer j \u003e= 0",
				"  b(n) = 4 if n = 2j^2 + 6j + 4 for some integer j \u003e= 0",
				"  b(n) = 2 if n = 2j^2 + 8j + 7 or 2j^2 + 8j + 8 for some integer j \u003e= 0",
				"  b(n) = 0 otherwise. (End)",
				"Triangle t(n,k) = max(k, n-k) - min(k, n-k). - _Peter Luschny_, Jan 26 2018",
				"Triangle t(n, k) = |n - 2*k| for n \u003e= 0, k = 0..n. See the Maple and Mathematica programs. Hence t(n, k)= t(n, n-k). - _Wolfdieter Lang_, Feb 04 2018",
				"a(n) = |t^2 - 2*n - 1|, where t = floor(sqrt(2*n+1) + 1/2). - _Ridouane Oudra_, Jun 07 2019; Dec 11 2020",
				"As a rectangle, T(n,k) = |n-k| = max(n,k) - min(n,k). - _Clark Kimberling_, May 11 2020"
			],
			"example": [
				"Displayed as a triangle t(n, k):",
				"n\\k   0 1 2 3 4 5 6 7 8 9 10 ...",
				"0:    0",
				"1:    1 1",
				"2:    2 0 2",
				"3:    3 1 1 3",
				"4:    4 2 0 2 4",
				"5:    5 3 1 1 3 5",
				"6:    6 4 2 0 2 4 6",
				"7:    7 5 3 1 1 3 5 7",
				"8:    8 6 4 2 0 2 4 6 8",
				"9:    9 7 5 3 1 1 3 5 7 9",
				"10:  10 8 6 4 2 0 2 4 6 8 10",
				"... reformatted by _Wolfdieter Lang_, Feb 04 2018",
				"Displayed as a table:",
				"  0 1 2 3 4 5 6 ...",
				"  1 0 1 2 3 4 5 ...",
				"  2 1 0 1 2 3 4 ...",
				"  3 2 1 0 1 2 3 ...",
				"  4 3 2 1 0 1 2 ...",
				"  5 4 3 2 1 0 1 ...",
				"  6 5 4 3 2 1 0 ...",
				"  ..."
			],
			"maple": [
				"seq(seq(abs(n-2*k),k=0..n),n=0..12); # _Robert Israel_, Sep 30 2015"
			],
			"mathematica": [
				"Table[Abs[(n-k) -k], {n,0,12}, {k,0,n}]//Flatten (* _Michael De Vlieger_, Sep 29 2015 *)"
			],
			"program": [
				"(PARI) a(n) = abs(2*(n+1)-binomial((sqrtint(8*(n+1))+1)\\2, 2)-(binomial(1+floor(1/2 + sqrt(2*(n+1))), 2))-1);",
				"vector(100, n , a(n-1)) \\\\ _Altug Alkan_, Sep 29 2015",
				"(PARI) {t(n,k) = abs(n-2*k)}; \\\\ _G. C. Greubel_, Jun 07 2019",
				"(GAP) a := Flat(List([0..12],n-\u003eList([0..n],k-\u003eMaximum(k,n-k)-Minimum(k,n-k)))); # _Muniru A Asiru_, Jan 26 2018",
				"(MAGMA) [[Abs(n-2*k): k in [0..n]]: n in [0..12]]; // _G. C. Greubel_, Jun 07 2019",
				"(Sage) [[abs(n-2*k) for k in (0..n)] for n in (0..12)] # _G. C. Greubel_, Jun 07 2019"
			],
			"xref": [
				"Cf. A003989, A003990, A003991, A003056, A004247, A002260, A004736.",
				"Cf. A089913. Apart from signs, same as A114327. A203993."
			],
			"keyword": "nonn,tabl,easy,nice",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_",
			"references": 17,
			"revision": 106,
			"time": "2020-12-25T11:02:38-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}