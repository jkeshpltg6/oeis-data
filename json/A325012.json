{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A325012",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 325012,
			"data": "1,4,1,9,6,1,16,24,23,1,25,70,333,496,1,36,165,2916,230076,2275974,1,49,336,16725,22456756,965227578201,800648638402240,1,64,616,70911,795467350,9607713956430560,149031415906337877339236058,1054942853799126580390222487977120,1",
			"name": "Array read by descending antidiagonals: A(n,k) is the number of oriented colorings of the facets of a regular n-dimensional orthoplex using up to k colors.",
			"comment": [
				"Also called cross polytope and hyperoctahedron. For n=1, the figure is a line segment with two vertices. For n=2 the figure is a square with four edges. For n=3 the figure is an octahedron with eight triangular faces. For n=4, the figure is a 16-cell with sixteen tetrahedral facets. The Schläfli symbol, {3,...,3,4}, of the regular n-dimensional orthoplex (n\u003e1) consists of n-2 threes followed by a four. Each of its 2^n facets is an (n-1)-dimensional simplex. Two oriented colorings are the same if one is a rotation of the other; chiral pairs are counted as two.",
				"Also the number of oriented colorings of the vertices of a regular n-dimensional orthotope (cube) using up to k colors."
			],
			"link": [
				"Robert A. Russell, \u003ca href=\"/A325012/b325012.txt\"\u003eTable of n, a(n) for n = 1..78\u003c/a\u003e",
				"E. M. Palmer and R. W. Robinson, \u003ca href=\"https://projecteuclid.org/euclid.acta/1485889789\"\u003eEnumeration under two representations of the wreath product\u003c/a\u003e, Acta Math., 131 (1973), 123-143."
			],
			"formula": [
				"The algorithm used in the Mathematica program below assigns each permutation of the axes to a partition of n. It then determines the number of permutations for each partition and the cycle index for each partition.",
				"A(n,k) = A325013(n,k) + A325014(n,k) = 2*A325013(n,k) - A325015(n,k) = 2*A325014(n,k) + A325015(n,k).",
				"A(n,k) = Sum_{j=1..2^n} A325016(n,j) * binomial(k,j)."
			],
			"example": [
				"Array begins with A(1,1):",
				"1   4      9       16        25          36           49            64 ...",
				"1   6     24       70       165         336          616          1044 ...",
				"1  23    333     2916     16725       70911       241913        701968 ...",
				"1 496 230076 22456756 795467350 14697611496 173107727191 1466088119056 ...",
				"For A(1,2) = 4, the two achiral colorings use just one of the two colors for both vertices; the chiral pair uses one color for each vertex."
			],
			"mathematica": [
				"a48[n_] := a48[n] = DivisorSum[NestWhile[#/2\u0026,n,EvenQ], MoebiusMu[#]2^(n/#)\u0026]/(2n); (* A000048 *)",
				"a37[n_] := a37[n] = DivisorSum[n,MoebiusMu[n/#]2^#\u0026]/n; (* A001037 *)",
				"CI0[{n_Integer}] := CI0[{n}] = CI[Transpose[If[EvenQ[n], p2 = IntegerExponent[n, 2]; sub = Divisors[n/2^p2]; {2^(p2+1) sub, a48 /@ (2^p2 sub) }, sub = Divisors[n]; {sub, a37 /@ sub}]]] 2^(n-1);(* even perm. *)",
				"CI1[{n_Integer}] := CI1[{n}] = CI[sub = Divisors[n]; Transpose[If[EvenQ[n], {sub, a37 /@ sub}, {2 sub, a48 /@ sub}]]] 2^(n-1); (* odd perm. *)",
				"compress[x : {{_, _} ...}] := (s = Sort[x]; For[i = Length[s], i \u003e 1, i -= 1, If[s[[i,1]]==s[[i-1,1]], s[[i-1,2]] += s[[i,2]]; s = Delete[s, i], Null]]; s)",
				"cix[{a_, b_}, {c_, d_}] := {LCM[a, c], (a b c d)/LCM[a, c]};",
				"Unprotect[Times]; Times[CI[a_List], CI[b_List]] :=  (* combine *) CI[compress[Flatten[Outer[cix, a, b, 1], 1]]]; Protect[Times];",
				"CI0[p_List] := CI0[p] = Expand[CI0[Drop[p, -1]] CI0[{Last[p]}] + CI1[Drop[p, -1]] CI1[{Last[p]}]]",
				"CI1[p_List] := CI1[p] = Expand[CI0[Drop[p, -1]] CI1[{Last[p]}] + CI1[Drop[p, -1]] CI0[{Last[p]}]]",
				"pc[p_List] := Module[{ci,mb},mb = DeleteDuplicates[p]; ci = Count[p, #] \u0026 /@ mb; n!/(Times @@ (ci!) Times @@ (mb^ci))] (* partition count *)",
				"row[n_Integer] := row[n] = Factor[(Total[(CI0[#] pc[#]) \u0026 /@ IntegerPartitions[n]])/(n! 2^(n - 1))] /. CI[l_List] :\u003e j^(Total[l][[2]])",
				"array[n_, k_] := row[n] /. j -\u003e k",
				"Table[array[n, d-n+1], {d, 1, 10}, {n, 1, d}] // Flatten"
			],
			"xref": [
				"Cf. A325013 (unoriented), A325014 (chiral), A325015 (achiral), A325016 (exactly k colors).",
				"Other n-dimensional polytopes: A324999 (simplex), A325004 (orthotope).",
				"Rows 1-3 are A000290, A006528, A000543; column 2 is A237748.",
				"Cf. A000048, A001037."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "1,2",
			"author": "_Robert A. Russell_, May 27 2019",
			"references": 13,
			"revision": 29,
			"time": "2019-06-15T14:43:14-04:00",
			"created": "2019-06-11T06:07:47-04:00"
		}
	]
}