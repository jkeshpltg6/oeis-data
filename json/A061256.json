{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A061256",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 61256,
			"data": "1,1,4,8,21,39,92,170,360,667,1316,2393,4541,8100,14824,26071,46422,80314,139978,238641,408201,686799,1156062,1920992,3189144,5238848,8589850,13963467,22641585,36447544,58507590,93334008,148449417,234829969,370345918",
			"name": "Euler transform of sigma(n), cf. A000203.",
			"comment": [
				"This is also the number of ordered triples of permutations f, g, h in Symm(n) which all commute, divided by n!. This was conjectured by _Franklin T. Adams-Watters_, Jan 16 2006, and proved by J. R. Britnell in 2012.",
				"According to a message on a blog page by \"Allan\" (see Secret Blogging Seminar link) it appears that a(n) = number of conjugacy classes of commutative ordered pairs in Symm(n).",
				"John McKay (email to _N. J. A. Sloane_, Apr 23 2013) observes that A061256 and A006908 coincide for a surprising number of terms, and asks for an explanation. - _N. J. A. Sloane_, May 19 2013"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A061256/b061256.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e (terms 0..1000 from T. D. Noe)",
				"J. R. Britnell, \u003ca href=\"http://arXiv.org/abs/1203.5079\"\u003eA formal identity involving commuting triples of permutations\u003c/a\u003e, arXiv:1203.5079 [math.CO], 2012.",
				"J. R. Britnell, \u003ca href=\"http://wwwf.imperial.ac.uk/~jbritnel/CommTriples.pdf\"\u003eA formal identity involving commuting triples of permutations\u003c/a\u003e, Preprint 2012. - _N. J. A. Sloane_, Jun 13 2012",
				"J. R. Britnell, \u003ca href=\"https://doi.org/10.1016/j.jcta.2013.01.009\"\u003eA formal identity involving commuting triples of permutations\u003c/a\u003e, Journal of Combinatorial Theory, Series A, Volume 120, Issue 4, May 2013.",
				"E. Marberg, \u003ca href=\"http://arxiv.org/abs/1202.1311\"\u003eHow to compute the Frobenius-Schur indicator of a unipotent character of a finite Coxeter system\u003c/a\u003e, arXiv preprint arXiv:1202.1311 [math.RT], 2012. -  _N. J. A. Sloane_, Jun 10 2012",
				"Secret Blogging Seminar, \u003ca href=\"https://sbseminar.wordpress.com/2010/10/06/a-peculiar-numerical-coincidence/\"\u003eA peculiar numerical coincidence\u003c/a\u003e.",
				"N. J. A. Sloane, \u003ca href=\"/transforms.txt\"\u003eTransforms\u003c/a\u003e",
				"Tad White, \u003ca href=\"http://arxiv.org/abs/1304.2830\"\u003eCounting Free Abelian Actions\u003c/a\u003e, arXiv:1304.2830 [math.CO], 2013."
			],
			"formula": [
				"a(n) = A072169(n) / n!.",
				"G.f.: Product_{k=1..infinity} (1 - x^k)^(-sigma(k)). a(n)=1/n*Sum_{k=1..n} a(n-k)*b(k), n\u003e1, a(0)=1, b(k)=Sum_{d|k} d*sigma(d), cf. A001001.",
				"G.f.: exp( Sum_{n\u003e=1} sigma(n)*x^n/(1-x^n)^2 /n ). [_Paul D. Hanna_, Mar 28 2009]",
				"G.f.: exp( Sum_{n\u003e=1} sigma_2(n)*x^n/(1-x^n)/n ). [_Vladeta Jovovic_, Mar 28 2009]",
				"G.f.: prod(n\u003e=1, E(x^n)^n ) where E(x) = prod(k\u003e=1, 1-x^k). [_Joerg Arndt_, Apr 12 2013]",
				"a(n) ~ exp((3*Pi)^(2/3) * Zeta(3)^(1/3) * n^(2/3)/2 - Pi^(4/3) * n^(1/3) / (4 * 3^(2/3) * Zeta(3)^(1/3)) - 1/24 - Pi^2/(288*Zeta(3))) * A^(1/2) * Zeta(3)^(11/72) / (2^(11/24) * 3^(47/72) * Pi^(11/72) * n^(47/72)), where A is the Glaisher-Kinkelin constant A074962. - _Vaclav Kotesovec_, Mar 23 2018"
			],
			"example": [
				"1 + x + 4*x^2 + 8*x^3 + 21*x^4 + 39*x^5 + 92*x^6 + 170*x^7 + 360*x^8 + ..."
			],
			"maple": [
				"with(numtheory):",
				"a:= proc(n) option remember; `if`(n=0, 1, add(add(",
				"      d*sigma(d), d=divisors(j)) *a(n-j), j=1..n)/n)",
				"    end:",
				"seq(a(n), n=0..40);  # _Alois P. Heinz_, Jun 08 2017"
			],
			"mathematica": [
				"nn = 30; b = Table[DivisorSigma[1, n], {n, nn}]; CoefficientList[Series[Product[1/(1 - x^m)^b[[m]], {m, nn}], {x, 0, nn}], x] (* _T. D. Noe_, Jun 18 2012 *)",
				"nmax = 40; CoefficientList[Series[Product[1/QPochhammer[x^k]^k, {k, 1, nmax}], {x, 0, nmax}], x] (* _Vaclav Kotesovec_, Nov 29 2015 *)"
			],
			"program": [
				"(PARI) N=66; x='x+O('x^N); gf=1/prod(j=1,N, eta(x^j)^j); Vec(gf) /* _Joerg Arndt_, May 03 2008 */",
				"(PARI) {a(n)=if(n==0,1,polcoeff(exp(sum(m=1,n,sigma(m)*x^m/(1-x^m+x*O(x^n))^2/m)),n))} /* _Paul D. Hanna_, Mar 28 2009 */"
			],
			"xref": [
				"Product_{k\u003e=1} 1/(1 - x^k)^sigma_m(k): A006171 (m=0), this sequence (m=1), A275585 (m=2), A288391 (m=3), A301542 (m=4), A301543 (m=5), A301544 (m=6), A301545 (m=7), A301546 (m=8), A301547 (m=9).",
				"Cf. A000203, A001001, A001970, A053529, A061255, A061257, A006908, A192065."
			],
			"keyword": "easy,nonn",
			"offset": "0,3",
			"author": "_Vladeta Jovovic_, Apr 21 2001",
			"ext": [
				"Entry revised by _N. J. A. Sloane_, Jun 13 2012"
			],
			"references": 44,
			"revision": 57,
			"time": "2018-10-28T04:35:37-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}