{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A196058",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 196058,
			"data": "0,1,2,2,3,3,2,2,4,4,4,3,3,3,5,2,3,4,2,4,4,5,4,3,6,4,4,3,4,5,5,2,6,4,5,4,3,3,5,4,4,4,3,5,5,4,5,3,4,6,5,4,2,4,7,3,4,5,4,5,4,6,4,2,6,6,3,4,5,5,4,4,4,4,6,3,6,5,5,4,4,5,4,4,6,4,6,5,3,5,5,4,7,5,5,3,6,4,6,6,4,5,4,4,5,3,3,4,5,7,5,3,5,4,6,5,5,5,5,5",
			"name": "Diameter (i.e., largest distance between two vertices) of the rooted tree with Matula-Goebel number n.",
			"comment": [
				"The Matula-Goebel number of a rooted tree can be defined in the following recursive manner: to the one-vertex tree there corresponds the number 1; to a tree T with root degree 1 there corresponds the t-th prime number, where t is the Matula-Goebel number of the tree obtained from T by deleting the edge emanating from the root; to a tree T with root degree m\u003e=2 there corresponds the product of the Matula-Goebel numbers of the m branches of T."
			],
			"link": [
				"François Marques, \u003ca href=\"/A196058/b196058.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e.",
				"Emeric Deutsch, \u003ca href=\"http://arxiv.org/abs/1111.4288\"\u003eTree statistics from Matula numbers\u003c/a\u003e, arXiv preprint arXiv:1111.4288 [math.CO], 2011.",
				"F. Göbel, \u003ca href=\"http://dx.doi.org/10.1016/0095-8956(80)90049-0\"\u003eOn a 1-1-correspondence between rooted trees and natural numbers\u003c/a\u003e, J. Combin. Theory, B 29 (1980), 141-143.",
				"I. Gutman and A. Ivic, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(95)00182-V\"\u003eOn Matula numbers\u003c/a\u003e, Discrete Math., 150, 1996, 131-142.",
				"I. Gutman and Yeong-Nan Yeh, \u003ca href=\"http://www.emis.de/journals/PIMB/067/3.html\"\u003eDeducing properties of trees from their Matula numbers\u003c/a\u003e, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"D. W. Matula, \u003ca href=\"http://www.jstor.org/stable/2027327\"\u003eA natural rooted tree enumeration by prime factorization\u003c/a\u003e, SIAM Rev. 10 (1968) 273.",
				"\u003ca href=\"/index/Mat#matula\"\u003eIndex entries for sequences related to Matula-Goebel numbers\u003c/a\u003e"
			],
			"formula": [
				"a(1)=0; if n=p(t) (=the t-th prime), then a(n)=max(a(t), 1+H(t)); if n=rs (r,s,\u003e=2), then a(n)=max(a(r), a(s), H(r)+H(s)), where H(m) is the height of the tree with Matula-Goebel number m (see A109082). The Maple program is based on this recursive formula.",
				"The Gutman et al. references contain a different recursive formula.",
				"a(n^k) = 2*A109082(n) for k \u003e 1. - _François Marques_, Mar 13 2021"
			],
			"example": [
				"a(2^m) = 2 because the rooted tree with Matula-Goebel number 2^m is a star with m edges."
			],
			"maple": [
				"with(numtheory): a := proc (n) local r, s, H: r := proc (n) options operator, arrow: op(1, factorset(n)) end proc: s := proc (n) options operator, arrow: n/r(n) end proc: H := proc (n) if n = 1 then 0 elif bigomega(n) = 1 then 1+H(pi(n)) else max(H(r(n)), H(s(n))) end if end proc: if n = 1 then 0 elif bigomega(n) = 1 then max(a(pi(n)), 1+H(pi(n))) else max(a(r(n)), a(s(n)), H(r(n))+H(s(n))) end if end proc: seq(a(n), n = 1 .. 120);"
			],
			"mathematica": [
				"r[n_] := r[n] = FactorInteger[n][[1, 1]];",
				"s[n_] := s[n] = n/r[n];",
				"H[n_] := H[n] = Which[n == 1, 0, PrimeOmega[n] == 1, 1 + H[PrimePi[n]], True, Max[H[r[n]], H[s[n]]]];",
				"a[n_] := a[n] = Which[n == 1, 0, PrimeOmega[n] == 1, Max[a[PrimePi[n]], 1 + H[PrimePi[n]]], True, Max[a[r[n]], a[s[n]], H[r[n]] + H[s[n]]]];",
				"Table[a[n], {n, 1, 120}] (* _Jean-François Alcover_, Nov 13 2017, after _Emeric Deutsch_ *)"
			],
			"program": [
				"(PARI) HD(n) = { if(n==1, return([0,0]),",
				"           my(f=factor(n)~, h=0, d=0, hd);",
				"           foreach(f, p,",
				"             hd=HD(primepi(p[1]));",
				"             hd[1]++;",
				"             d=max(max(d,if(p[2]\u003e1, 2*hd[1], hd[2])),h+hd[1]);",
				"             h=max(h,hd[1])",
				"           );",
				"           return([h,d])",
				"           )",
				"};",
				"A196058(n)=HD(n)[2]; \\\\ _François Marques_, Mar 13 2021"
			],
			"xref": [
				"Cf. A109082."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Emeric Deutsch_, Sep 30 2011",
			"references": 2,
			"revision": 27,
			"time": "2021-03-14T00:53:18-05:00",
			"created": "2011-09-30T12:12:07-04:00"
		}
	]
}