{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A246904",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 246904,
			"data": "2,5,2,3,5,2,5,10,85,2,3,5,6,30,221,2,5,13,65,145,290,533,2813,2,3,5,10,42,85,87,105,210,455,1365,1517,7565,2,5,29,58,74,149,185,442,565,901,2026,2117,2210,3973,10001,11026,16133,18229,2,3,5,6,26,30",
			"name": "Irregular triangular array: every periodic simple continued fraction CF represents a quadratic irrational (c + f*sqrt(d))/b, where b,c,f,d are integers and d is squarefree. Row n of this array shows the distinct values of d as CF ranges through the periodic continued fractions having period an n-tuple of 1s and 2s.",
			"comment": [
				"Is every squarefree positive integer in this array?"
			],
			"example": [
				"First 5 rows:",
				"2 ... 5",
				"2 ... 3 ... 5",
				"2 ... 5 ... 10 .. 85",
				"2 ... 3 ... 5 ... 6 ... 30 ... 221",
				"2 ... 5 ... 13 .. 65 .. 145 .. 290 .. 533 .. 2813",
				"The following list shows for n = 3 the 2^n purely periodic continued fractions, each followed by the number r it represents, the minimal polynomial a*x^2 + b*x + c of r, the discriminant, D = b^2 - 4*a*c, and the squarefree factor, d, of D.",
				"[(1,1,1)] = (1+sqrt(5))/2, -1 - x + x^2, D = 5, d = 5",
				"[(1,1,2)] = sqrt(10)/4, -5 + 2 x^2, D = 40, d = 10",
				"[(1,2,1)] = (2 + sqrt(10)/3, -2 - 4 x + 3 x^2, D = 10, d = 10",
				"[(2,1,1)] = (1 + sqrt(85))/6, -7 - x + 3 x^2, D = 85, d = 85",
				"[(1,2,2)] = (1 + sqrt(10)/3, -3 - 2 x + 3 x^2, D = 10, d = 10",
				"[(2,1,2)] = (-1 + sqrt(85))/6, -7 + x + 3 x^2, D = 85, d = 85",
				"[(2,2,2)] = (5 + sqrt(85))/10, -3 - 5 x + 5 x^2, D = 85, d = 85",
				"[(2,2,2)] = sqrt(2), -2 + x^2, D = 8, d = 2",
				"The distinct values of d are 2, 5, 10, 85, as in row 3."
			],
			"mathematica": [
				"z = 6;",
				"t[n_] := t[n] = Map[FromContinuedFraction[{1, #}] \u0026, Tuples[{1, 2}, n]]",
				"u[n_] := u[n] = Table[MinimalPolynomial[t[k], x], {k, 1, n}]",
				"d = Discriminant[u[z], x]; v[n_] := Table[{p, m} =",
				"Transpose[FactorInteger[k]]; Times @@ (p^Mod[m, 2]), {k, d[[n]]}];",
				"w = Table[Union[Table[v[n], {n, 1, z}][[n]]], {n, 1, z}];",
				"TableForm[w] (* A246904 array *)",
				"Flatten[w]   (* A246904 sequence *)"
			],
			"xref": [
				"Cf. A246903, A246905."
			],
			"keyword": "nonn,tabf,easy",
			"offset": "1,1",
			"author": "_Clark Kimberling_, Sep 06 2014",
			"references": 4,
			"revision": 7,
			"time": "2014-09-13T14:54:09-04:00",
			"created": "2014-09-13T14:54:09-04:00"
		}
	]
}