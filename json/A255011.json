{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A255011",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 255011,
			"data": "0,4,56,340,1120,3264,6264,13968,22904,38748,58256,95656,120960,192636,246824,323560,425408,587964,682296,932996,1061232,1327524,1634488,2049704,2227672,2806036,3275800,3810088,4307520,5298768",
			"name": "Number of polygons formed by connecting all the 4n points on the perimeter of an n X n square by straight lines; a(0) = 0 by convention.",
			"comment": [
				"There are n+1 points on each side of the square, but that counts the four corners twice, so there are a total of 4n points on the perimeter. - _N. J. A. Sloane_, Jan 23 2020",
				"a(n) is always divisible by 4, by symmetry. If n is odd, a(n) is divisible by 8.",
				"From _Michael De Vlieger_, Feb 19-20 2015: (Start)",
				"For n \u003e 0, the vertices of the bounding square generate diametrical bisectors that cross at the center. Thus each diagram has fourfold symmetry.",
				"For n \u003e 0, an orthogonal n X n grid is produced by corresponding horizontal and vertical points on opposite sides.",
				"Terms {1, 3, 9} are not congruent to 0 (mod 8).",
				"Number of edges: {0, 8, 92, 596, 1936, 6020, 11088, 26260, 42144, 72296, 107832, ...}. See A331448. (End)"
			],
			"link": [
				"Lars Blomberg, \u003ca href=\"/A255011/b255011.txt\"\u003eTable of n, a(n) for n = 0..52\u003c/a\u003e",
				"Lars Blomberg, Scott R. Shannon, N. J. A. Sloane, \u003ca href=\"http://neilsloane.com/doc/rose_5.pdf\"\u003eGraphical Enumeration and Stained Glass Windows, 1: Rectangular Grids\u003c/a\u003e, (2021). Also arXiv:2009.07918.",
				"Michael De Vlieger, \u003ca href=\"/A255011/a255011.pdf\"\u003eDiagrams of A255011(n) for n \u003c= 10\u003c/a\u003e",
				"B. Poonen and M. Rubinstein (1998) \u003ca href=\"http://math.mit.edu/~poonen/papers/ngon.pdf\"\u003eThe Number of Intersection Points Made by the Diagonals of a Regular Polygon\u003c/a\u003e, SIAM J. Discrete Mathematics 11(1), pp. 135-156, doi:\u003ca href=\"http://dx.doi.org/10.1137/S0895480195281246\"\u003e10.1137/S0895480195281246\u003c/a\u003e, arXiv:\u003ca href=\"http://arXiv.org/abs/math.MG/9508209\"\u003emath.MG/9508209\u003c/a\u003e (has fewer typos than the SIAM version)",
				"Scott R. Shannon, \u003ca href=\"/A331452/a331452_6.png\"\u003eColored illustration for a(1)\u003c/a\u003e",
				"Scott R. Shannon, \u003ca href=\"/A331452/a331452_12.png\"\u003eColored illustration for a(2)\u003c/a\u003e",
				"Scott R. Shannon, \u003ca href=\"/A331452/a331452_1.png\"\u003eColored illustration for a(3)\u003c/a\u003e",
				"Scott R. Shannon, \u003ca href=\"/A331452/a331452_21.png\"\u003eColored illustration for a(4)\u003c/a\u003e",
				"Scott R. Shannon, \u003ca href=\"/A331452/a331452_24.png\"\u003eColored illustration for a(5)\u003c/a\u003e"
			],
			"formula": [
				"No formula is presently known. - _N. J. A. Sloane_, Feb 04 2020"
			],
			"example": [
				"For n = 3, the perimeter of the square contains 12 points:",
				"* * * *",
				"*     *",
				"*     *",
				"* * * *",
				"Connect each point to every other point with a straight line inside the square. Then count the polygons (or regions) that have formed. There are 340 polygons, so a(3) = 340.",
				"For n = 1, the full picture is:",
				"*-*",
				"|X|",
				"*-*",
				"The lines form four triangular regions, so a(1) = 4.",
				"For n = 0, the square can be regarded as consisting of a single point, producing no lines or polygons, and so a(0) = 0."
			],
			"xref": [
				"Cf. A092098 (triangular analog), A331448 (edges), A331449 (points).",
				"For the circular analog see A006533, A007678."
			],
			"keyword": "nonn,more",
			"offset": "0,2",
			"author": "_Johan Westin_, Feb 12 2015",
			"ext": [
				"a(11)-a(29) from _Hiroaki Yamanouchi_, Feb 23 2015",
				"Offset changed by _N. J. A. Sloane_, Jan 23 2020"
			],
			"references": 25,
			"revision": 77,
			"time": "2021-05-21T07:08:39-04:00",
			"created": "2015-02-23T09:32:32-05:00"
		}
	]
}