{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A290655",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 290655,
			"data": "1,0,0,240,1782,9072,59328,216432,810000,2059152,6080832,12349584,31045596,57036960,122715648,204193872,418822650,622067040,1193611392,1734272208,3043596384,4217152080,7354100160,9446435136,15901091892,20507712192,32268036096,40493364288,64454759856",
			"name": "Theta series of the 20-dimensional lattice of hyper-roots A_3(SU(3)).",
			"comment": [
				"This lattice is the k=3 member of the family of lattices of SU(3) hyper-roots associated with the fusion category A_k(SU(3)).",
				"Simple objects of the latter are irreducible integrable representations of the affine Lie algebra of SU(3) at level k.",
				"With k=3 there are r=(k+1)(k+2)/2=10 simple objects. The lattice is defined by 2 * r * (k+3)^2/3=240 hyper-roots of norm 6 which are also the vectors of shortest length. Minimal norm is 6.  Det = 6^12.",
				"The lattice is rescaled (q --\u003e q^2): its theta function starts as 1 + 240*q^6 + 1782*q^8 +... See example."
			],
			"link": [
				"Robert Coquereaux, \u003ca href=\"/A290655/b290655.txt\"\u003eTable of n, a(n) for n = 0..59\u003c/a\u003e",
				"R. Coquereaux, \u003ca href=\"https://arxiv.org/abs/1708.00560\"\u003eTheta functions for lattices of SU(3) hyper-roots\u003c/a\u003e, arXiv:1708.00560[math.QA], 2017.",
				"A. Ocneanu, \u003ca href=\"https://cel.archives-ouvertes.fr/cel-00374414\"\u003eThe Classification of subgroups of quantum SU(N)\u003c/a\u003e, in \"Quantum symmetries in theoretical physics and mathematics\", Bariloche 2000, Eds. Coquereaux R., Garcia A. and Trinchero R., AMS Contemporary Mathematics, 294, pp. 133-160, (2000). End of Sec 2.5."
			],
			"example": [
				"G.f. = 1 + 240*x^3 + 1782*x^4 + 9072*x^5 + ...",
				"G.f. = 1 + 240*q^6 + 1782*q^8 + 9072*q^10 + ..."
			],
			"program": [
				"(MAGMA)",
				"order:=60; // Example",
				"L:=LatticeWithGram(20,[6,0,0,0,2,0,2,2,2,0,0,2,0,2,1,-1,1,2,0,2,0,6,0,2,2,0,0,2,0,2,2,2,0,0,1,1,-1,0,2,2,0,0,\\",
				"6,0,2,2,0,0,2,2,0,2,2,0,-1,1,1,2,2,0,0,2,0,6,0,0,0,0,0,2,-2,1,0,0,0,0,2,0,2,0,2,2,2,0,6,0,0,2,2,2,1,0,1,1,2,2,2,\\",
				"2,2,2,0,0,2,0,0,6,0,0,2,0,0,1,-2,0,2,0,0,2,0,0,2,0,0,0,0,0,6,2,0,0,0,1,0,-2,0,2,0,0,0,2,2,2,0,0,2,0,2,6,0,0,2,0,\\",
				"0,-2,2,0,-2,1,1,-1,2,0,2,0,2,2,0,0,6,0,0,0,-2,2,0,-2,2,-1,1,1,0,2,2,2,2,0,0,0,0,6,-2,0,2,0,-2,2,0,1,-1,1,0,2,0,-\\",
				"2,1,0,0,2,0,-2,6,0,0,0,2,0,-2,0,2,0,2,2,2,1,0,1,1,0,0,0,0,6,0,0,0,0,0,2,2,2,0,0,2,0,1,-2,0,0,-2,2,0,0,6,0,-2,2,0\\",
				",2,0,0,2,0,0,0,1,0,-2,-2,2,0,0,0,0,6,0,-2,2,0,0,2,1,1,-1,0,2,2,0,2,0,-2,2,0,-2,0,6,0,0,2,2,0,-1,1,1,0,2,0,2,0,-2\\",
				",2,0,0,2,-2,0,6,0,2,0,2,1,-1,1,2,2,0,0,-2,2,0,-2,0,0,2,0,0,6,0,2,2,2,0,2,0,2,2,0,1,-1,1,0,2,2,0,2,2,0,6,0,0,0,2,\\",
				"2,2,2,0,0,1,1,-1,2,2,0,0,2,0,2,0,6,0,2,2,0,0,2,0,2,-1,1,1,0,2,0,2,0,2,2,0,0,6]);",
				"theta:=ThetaSeriesModularForm(L); PowerSeries(theta,order);"
			],
			"xref": [
				"Cf. A008434. {D_6}^{+} lattice is rescaled A_1(SU(3)).",
				"A290654 is A_2(SU(3)). Cf. A287329, A287944, A288488, A288489, A288776, A288779, A288909."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Robert Coquereaux_, Aug 08 2017",
			"references": 10,
			"revision": 16,
			"time": "2017-09-03T21:44:53-04:00",
			"created": "2017-08-08T21:40:51-04:00"
		}
	]
}