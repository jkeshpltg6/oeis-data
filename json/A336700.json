{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A336700",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 336700,
			"data": "1,3,7,15,31,63,127,255,511,1023,2047,2431,2943,3775,4095,8191,13311,14335,16383,17407,21951,22527,32767,34335,44031,57855,65535,85375,131071,204799,262143,376831,524287,923647,1048575,1562623,1632255,2056191,2097151,2744319,4194303,6815743,8388607,8781823,10059775,16777215",
			"name": "Numbers k such that the odd part of (1+k) divides (1 + odd part of sigma(k)).",
			"comment": [
				"Numbers k for which A337194(k) = 1+A161942(k) is a multiple of A000265(1+k).",
				"Conjecture: After 1, all terms are of the form 4u+3 (in A004767). If this could be proved, then it would refute at once the existence of both the odd perfect numbers and the quasiperfect numbers. Concentrating on the latter is probably easier, as it is known that all quasiperfect numbers must be odd squares, thus k is of the form 4u+1, in which case the condition given in A336701 that A000265(1+A000265(sigma(k))) must be equal to A000265(1+k) reduces to a simpler form, A000265(1+sigma(k)) = (1+k)/2, and as k = s^2, with s odd, so (s^2 + 1)/2 should divide 1+sigma(s^2). Does that condition allow any other solutions than s=1 ? See A337339."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A336700/b336700.txt\"\u003eTable of n, a(n) for n = 1..77; all terms \u003c 2^32\u003c/a\u003e",
				"Paolo Cattaneo, \u003ca href=\"http://www.bdim.eu/item?fmt=pdf\u0026amp;id=BUMI_1951_3_6_1_59_0\"\u003eSui numeri quasiperfetti\u003c/a\u003e, Bollettino dell’Unione Matematica Italiana, Serie 3, Vol.6(1951), n.1, p. 59-62.",
				"P. Hagis and G. L. Cohen, \u003ca href=\"http://dx.doi.org/10.1017/S1446788700018401\"\u003eSome Results Concerning Quasiperfect Numbers\u003c/a\u003e, J. Austral. Math. Soc. Ser. A 33, 275-286, 1982.",
				"V. Siva Rama Prasad and C. Sunitha, \u003ca href=\"http://nntdm.net/papers/nntdm-23/NNTDM-23-3-073-078.pdf\"\u003eOn quasiperfect numbers\u003c/a\u003e, Notes on Number Theory and Discrete Mathematics, Vol. 23, 2017, No. 3, 73-78.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/QuasiperfectNumber.html\"\u003eQuasiperfect Number\u003c/a\u003e",
				"\u003ca href=\"/index/O#opnseqs\"\u003eIndex entries for sequences where odd perfect numbers must occur, if they exist at all\u003c/a\u003e",
				"\u003ca href=\"/index/Si#SIGMAN\"\u003eIndex entries for sequences related to sigma(n)\u003c/a\u003e"
			],
			"mathematica": [
				"Block[{f}, f[n_] := n/2^IntegerExponent[n, 2]; Select[Range[2^20], Mod[f[1 + f[DivisorSigma[1, #]]], f[1 + #]] == 0 \u0026] ] (* _Michael De Vlieger_, Aug 22 2020 *)"
			],
			"program": [
				"(PARI)",
				"A000265(n) = (n\u003e\u003evaluation(n,2));",
				"isA336700(n) = !((1+A000265(sigma(n)))%A000265(1+n));"
			],
			"xref": [
				"Cf. A000203, A000265, A004767, A016754, A161942, A228058, A336697, A336698, A337339, A337342.",
				"Subsequences: A000225, A336701 (terms where the quotient is a power of 2)."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Antti Karttunen_, Aug 02 2020",
			"references": 7,
			"revision": 41,
			"time": "2020-08-29T03:09:52-04:00",
			"created": "2020-08-06T09:28:56-04:00"
		}
	]
}