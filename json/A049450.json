{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A049450",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 49450,
			"data": "0,2,10,24,44,70,102,140,184,234,290,352,420,494,574,660,752,850,954,1064,1180,1302,1430,1564,1704,1850,2002,2160,2324,2494,2670,2852,3040,3234,3434,3640,3852,4070,4294,4524,4760,5002,5250,5504,5764",
			"name": "Pentagonal numbers multiplied by 2: a(n) = n*(3*n-1).",
			"comment": [
				"From _Floor van Lamoen_, Jul 21 2001: (Start)",
				"Write 1,2,3,4,... in a hexagonal spiral around 0, then a(n) is the sequence found by reading the line from 0 in the direction 0,2,.... The spiral begins:",
				".",
				"                   56--55--54--53--52",
				"                   /                 \\",
				"                 57  33--32--31--30  51",
				"                 /   /             \\   \\",
				"               58  34  16--15--14  29  50",
				"               /   /   /         \\   \\   \\",
				"             59  35  17   5---4  13  28  49",
				"             /   /   /   /     \\   \\   \\   \\",
				"           60  36  18   6   0   3  12  27  48",
				"           /   /   /   /   / . /   /   /   /",
				"         61  37  19   7   1---2  11  26  47",
				"           \\   \\   \\   \\       . /   /   /",
				"           62  38  20   8---9--10  25  46",
				"             \\   \\   \\           . /   /",
				"             63  39  21--22--23--24  45",
				"               \\   \\               . /",
				"               64  40--41--42--43--44",
				"                 \\                   .",
				"                 65--66--67--68--69--70",
				"(End)",
				"Starting with offset 1 = binomial transform of [2, 8, 6, 0, 0, 0, ...]. - _Gary W. Adamson_, Jan 09 2009",
				"Number of possible pawn moves on an (n+1) X (n+1) chessboard (n=\u003e3). - _Johannes W. Meijer_, Feb 04 2010",
				"a(n) = A069905(6n-1): Number of partitions of 6*n-1 into 3 parts. - _Adi Dani_, Jun 04 2011",
				"Even octagonal numbers divided by 4. - _Omar E. Pol_, Aug 19 2011",
				"Partial sums give A011379. - _Omar E. Pol_, Jan 12 2013",
				"First differences are A016933; second differences equal 6. - _Bob Selcoe_, Apr 02 2015"
			],
			"link": [
				"Ivan Panchenko, \u003ca href=\"/A049450/b049450.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Richard P. Brent, \u003ca href=\"http://arxiv.org/abs/1407.3533\"\u003eGeneralising Tuenter's binomial sums\u003c/a\u003e, arXiv:1407.3533 [math.CO], 2014. (page 16)",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1)."
			],
			"formula": [
				"O.g.f.: A(x) = 2*x*(1+2*x)/(1-x)^3.",
				"a(n) = A049452(n) - A033428(n). - _Zerinvary Lajos_, Jun 12 2007",
				"a(n) = 2*A000326(n), twice pentagonal numbers. - _Omar E. Pol_, May 14 2008",
				"a(n) = A022264(n) - A000217(n). - _Reinhard Zumkeller_, Oct 09 2008",
				"a(n) = a(n-1) + 6*n - 4 (with a(0)=0). - _Vincenzo Librandi_, Aug 06 2010",
				"a(n) = A014642(n)/4 = A033579(n)/2. - Omar E. Pol, Aug 19 2011",
				"a(n) = A000290(n) + A000384(n) = A000217(n) + A000566(n). - _Omar E. Pol_, Jan 11 2013",
				"a(n+1) = A014107(n+2) + A000290(n). - _Philippe Deléham_, Mar 30 2013",
				"E.g.f.: x*(2 + 3*x)*exp(x). - _Vincenzo Librandi_, Apr 28 2016",
				"a(n) = (2/3)*A000217(3*n-1). - _Bruno Berselli_, Feb 13 2017",
				"a(n) = A002061(n) + A056220(n). - _Bruce J. Nicholson_, Sep 21 2017"
			],
			"example": [
				"On a 4 X 4 chessboard pawns at the second row have (3+4+4+3) moves and pawns at the third row have (2+3+3+2) moves so a(3) = 24. - _Johannes W. Meijer_, Feb 04 2010",
				"From _Adi Dani_, Jun 04 2011: (Start)",
				"a(1)=2: the partitions of 6*1-1=5 into 3 parts are [1,1,3] and[1,2,2].",
				"a(2)=10: the partitions of 6*2-1=11 into 3 parts are [1,1,9], [1,2,8], [1,3,7], [1,4,6], [1,5,5], [2,2,7], [2,3,6], [2,4,5], [3,3,5], and [3,4,4].",
				"(End)",
				".",
				".                                                         o",
				".                                                       o o o",
				".                                      o              o o o o o",
				".                                    o o o          o o o o o o o",
				".                       o          o o o o o      o o o o o o o o o",
				".                     o o o      o o o o o o o    o o o o o o o o o",
				".            o      o o o o o    o o o o o o o    o o o o o o o o o",
				".          o o o    o o o o o    o o o o o o o    o o o o o o o o o",
				".    o     o o o    o o o o o    o o o o o o o    o o o o o o o o o",
				".    o     o o o    o o o o o    o o o o o o o    o o o o o o o o o",
				".    2      10         24             44                 70",
				"- _Philippe Deléham_, Mar 30 2013"
			],
			"maple": [
				"seq(n*(3*n-1),n=0..44); # _Zerinvary Lajos_, Jun 12 2007"
			],
			"mathematica": [
				"Table[n(3n-1),{n,0,50}] (* or *) LinearRecurrence[{3,-3,1},{0,2,10},50] (* _Harvey P. Dale_, Jun 21 2014 *)",
				"2*PolygonalNumber[5,Range[0,50]] (* Requires Mathematica version 10 or later *) (* _Harvey P. Dale_, Jun 01 2018 *)"
			],
			"program": [
				"(PARI) a(n)=n*(3*n-1) \\\\ _Charles R Greathouse IV_, Nov 20 2012",
				"(MAGMA) [n*(3*n-1) : n in [0..50]]; // _Wesley Ivan Hurt_, Sep 24 2017",
				"(Sage) [n*(3*n-1) for n in (0..50)] # _G. C. Greubel_, Aug 31 2019",
				"(GAP) List([0..50], n-\u003e n*(3*n-1)); # _G. C. Greubel_, Aug 31 2019"
			],
			"xref": [
				"Cf. A000567.",
				"Bisection of A001859. Cf. A045944, A000326, A033579, A027599, A049451.",
				"Cf. A033586 (King), A035005 (Queen), A035006 (Rook), A035008 (Knight) and A002492 (Bishop).",
				"Cf. numbers of the form n*(n*k-k+4))/2 listed in A226488. [_Bruno Berselli_, Jun 10 2013]",
				"Cf. sequences listed in A254963."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,2",
			"author": "Joe Keane (jgk(AT)jgk.org).",
			"ext": [
				"Comment, example and crossrefs added and minor errors corrected by _Johannes W. Meijer_, Feb 04 2010"
			],
			"references": 48,
			"revision": 93,
			"time": "2019-08-31T11:05:00-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}