{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A054854",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 54854,
			"data": "1,1,5,11,35,93,269,747,2115,5933,16717,47003,132291,372157,1047181,2946251,8289731,23323853,65624397,184640891,519507267,1461688413,4112616845,11571284395,32557042499,91602704493,257733967693,725161963867",
			"name": "Number of ways to tile a 4 X n region with 1 X 1 and 2 X 2 tiles.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A054854/b054854.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"S. Heubach, \u003ca href=\"https://www.calstatela.edu/sites/default/files/users/u1231/Papers/cgtc30.pdf\"\u003eTiling an m-by-n area with squares of size up to k-by-k (m\u003c=5)\u003c/a\u003e, Congressus Numerantium 140 (1999), 43-64.",
				"Richard M. Low and Ardak Kapbasov, \u003ca href=\"https://www.emis.de/journals/JIS/VOL20/Low/low2.html\"\u003eNon-Attacking Bishop and King Positions on Regular and Cylindrical Chessboards\u003c/a\u003e, Journal of Integer Sequences, Vol. 20 (2017), Article 17.6.1, Table 7.",
				"R. J. Mathar, \u003ca href=\"http://arxiv.org/abs/1609.03964\"\u003eTiling n x m rectangles with 1 x 1 and s x s squares\u003c/a\u003e, arXiv:1609.03964 [math.CO], 2016, Section 4.1.",
				"A. Ugolnikova, \u003ca href=\"https://tel.archives-ouvertes.fr/tel-01889871/\"\u003ePavages Aleatories\u003c/a\u003e, Phd Thesis (2016) Section 2.2.3",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,3,-2)."
			],
			"formula": [
				"G.f.: (1-x)/(1-2*x-3*x^2+2*x^3). - _N. J. A. Sloane_, Nov 17 2002",
				"a(n) = a(n-1)+4*a(n-2)+2*( a(n-3)+a(n-4)+...+a(0) ).",
				"a(n) = 2*a(n-1)+3*a(n-2)-2*a(n-3). - Keith Schneider (kschneid(AT)bulldog.unca.edu), Apr 02 2006",
				"a(n) = Term (2,2) of matrix [5,1,1; 1,1,0; 1,0,1/2]*[2,1,0; 3,0,1; -2,0,0]^n. - _Alois P. Heinz_, May 18 2008"
			],
			"example": [
				"a(2) = 5 as there is one tiling of a 4x2 region with only 1 X 1 tiles, 3 tilings with exactly one 2 X 2 tile and one tiling consisting of two 2 X 2 tiles."
			],
			"maple": [
				"A:= Matrix([[5,1,1],[1,1,0],[1,0,1/2]]); M:= Matrix([[2,1,0],[3,0,1],[ -2,0,0]]): a:= n-\u003e(A.M^n)[2,2]: seq(a(n), n=0..50); # _Alois P. Heinz_, May 18 2008"
			],
			"mathematica": [
				"f[{A_, B_}] := Module[{til = A, basic = B}, {Flatten[Append[til, ListConvolve[A, B]]], AppendTo[basic, 2]}]; NumOfTilings[n_] := Nest[f, {{1, 1}, {1, 4}}, n - 2][[1]] NumOfTilings[30]",
				"(* Second program: *)",
				"LinearRecurrence[{2, 3, -2}, {1, 1, 5}, 30] (* _Jean-François Alcover_, Jul 28 2018 *)"
			],
			"xref": [
				"Cf. A054855.",
				"Column k=4 of A245013. First differences of A046672."
			],
			"keyword": "easy,nonn",
			"offset": "0,3",
			"author": "Silvia Heubach (silvi(AT)cine.net), Apr 21 2000",
			"references": 16,
			"revision": 43,
			"time": "2020-08-24T22:40:34-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}