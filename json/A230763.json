{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A230763",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 230763,
			"data": "6,1,8,5,2,7,4,10,1,2,2,3,7,10,2,1,2,6,19,8,10,2,2,2,2,2,20,2,10,2,4,7,11,8,2,2,2,2,5,10,2,4,7,1,2,5,2,2,2,10,2,5,10,11,2,8,2,1,10,1,6,3,10,2,11",
			"name": "Length of segments delimited by the letter \"e\" when the sequence is spelled out in English.",
			"comment": [
				"Translate the sequence S to English, to get English(S) = (SIX, ONE, EIGHT, FIVE, TWO, SEVEN, FOUR, TEN, ONE, TWO, TWO, THREE, SEVEN, TEN, TWO, ONE, TWO, SIX, NINETEEN, EIGHT, TEN, TWO, TWO, TWO, TWO, TWO, TWENTY, TWO, TEN, TWO, FOUR, SEVEN, ELEVEN, ...).",
				"Then insert a stroke immediately after each letter \"E\" of S: \"SIX, ONE|, E|IGHT, FIVE|, TWO, SE|VE|N, FOUR, TE|N, ONE|, TWO, TWO, THRE|E|, SE|VE|N, TE|N, TWO, ONE|, TWO, SIX, NINE|TE|E|N, E|IGHT, TE|N, TWO, TWO, TWO, TWO, TWO, TWE|NTY, TWO, TE|N, TWO, FOUR, SE|VE|N, E|LE|VE|N,\" ...",
				"Now the length of each chunk is given by the sequence S itself. In case of several possible choices compatible with this property, we require the smallest one to be chosen."
			],
			"link": [
				"E. Angelini, \u003ca href=\"http://www.cetteadressecomportecinquantesignes.com/EchunkEnglish.htm\"\u003eSelf-count of Letter-Chunks ending with E\u003c/a\u003e.",
				"E. Angelini, \u003ca href=\"/A230763/a230763.pdf\"\u003eSelf-count of Letter-Chunks ending with E\u003c/a\u003e [Cached copy, with permission]",
				"E. Angelini, \u003ca href=\"http://list.seqfan.eu/pipermail/seqfan/2013-October/011793.html\"\u003eBacktracking the letter E chunk\u003c/a\u003e, SeqFan List, Oct 22 2013"
			],
			"example": [
				"We see thus that the sequence cannot start with ONE, or TWO, or THREE, or FOUR or FIVE (because the presence or the absence of the letter \"E\" in those numbers leads to a contradiction). We could start with THIRTY, FORTY, FIFTY and SIXTY also -- but SIX is the smallest available integer. After SIX, we could also have chosen TWENTY, but again, we want the smallest integer fitting the pattern, which is ONE; therefore, the sequence starts: S = (SIX, ONE|, ...)",
				"See the link to Angelini's page for further developments."
			],
			"program": [
				"(PARI) /* see A052360 for the function English(). We implement a function which \"caches\" the English words (as Vecsmall() of ASCII codes) in an array for faster treatment. */",
				"VET=[];VE(n)={#VET\u003cn \u0026\u0026 VET=concat(VET,vector(n-#VET));VET[n] \u0026\u0026 return(VET[n]); VET[n]=Vecsmall(English(n))}",
				"/* This checks whether the sequence 'a' is compatible with the sequence of letters (ASCII codes) 's' */ {check(a,s)=my(L=0,j=1);!for(i=1,#s,( i\u003eL+a[j] || (( i==L+a[j] ) != (s[i] ==101))) \u0026\u0026 return; s[i] != 101 \u0026\u0026 next; L=i; j++)}",
				"/* This extends the sequence 'a' by 'n' terms. Note, the last terms added may turn out to be not \"valid\", when the sequence is extended further. Could be programmed much more efficiently. */",
				"extend(n,a=[],s=Vecsmall([]))={check(a,s)||return; n-- || return(a); my(t); for(k=1,99,(t=extend(n,concat(a,k),concat(s,VE(k))))\u0026\u0026 return(t))}",
				"/* To produce the sequence, use, e.g. */ extend(50)"
			],
			"xref": [
				"Cf. A052360."
			],
			"keyword": "nonn,word",
			"offset": "1,1",
			"author": "_Eric Angelini_ and _M. F. Hasler_, Oct 29 2013",
			"references": 3,
			"revision": 21,
			"time": "2018-09-09T02:47:10-04:00",
			"created": "2013-10-31T04:13:29-04:00"
		}
	]
}