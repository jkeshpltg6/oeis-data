{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A188599",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 188599,
			"data": "0,1,6,11,-84,-779,-2574,4031,88536,430441,369246,-8545549,-60504444,-149387939,616283466,7432399271,29187308976,-10686127919,-793799491914,-4495643753509,-7128875223204,69617842498501,595928935571106,1835127550964111,-3887458083492984",
			"name": "Expansion of x/(1-6*x+25*x^2).",
			"comment": [
				"Original name: G.f.: 1/(1-6*x+25*x^2).",
				"Suggested by _Philippe Flajolet_ as an example of a simple formula for which the general term is hard to guess because 1-6*x+25*x^2 has 2 complex roots of equal size and modulus 1.",
				"The Lucas sequence U_n(6,25). - _Peter Bala_, Feb 02 2017"
			],
			"reference": [
				"Discussion in 1993 at the FPSAC 1993 in Florence."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A188599/b188599.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e (first 110 terms from Vincenzo Librandi)",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Lucas_sequence\"\u003eLucas sequence\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (6,-25)."
			],
			"formula": [
				"a(n) = ((3+4*i)^n-(3-4*i)^n)/8/i, where i=sqrt(-1). - Denis Excoffier, Jan 19 2013",
				"From _Peter Bala_, Feb 02 2017: (Start)",
				"a(n) = (1/4)*( Re((2 - i)^n)*Im((2 + i)^n) - Re((2 + i)^n)*Im((2 - i)^n) ).",
				"a(n) = (1/2) * the directed or signed area of the triangle in the complex plane with vertices at the points 0, (2 - i)^n and (2 + i)^n. (End)",
				"a(n) = 5^n*sin(n*arctan(1/2))*cos(n*arctan(1/2))/2. - _Peter Luschny_, Feb 02 2017",
				"E.g.f.: (1/4)*exp(3*x)*sin(4*x). - _Stefano Spezia_, Feb 01 2020"
			],
			"maple": [
				"x/(1-6*x+25*x^2):series(%,x,44):seriestolist(%);"
			],
			"mathematica": [
				"Table[Im[(3 + 4*I)^n]/4, {n, 0, 22}] (* _Jean-François Alcover_, Jun 14 2011 *)",
				"CoefficientList[Series[x/(1-6*x+25*x^2),{x,0,30}],x] (* _Harvey P. Dale_, Dec 01 2018 *)",
				"LinearRecurrence[{6,-25},{0,1},30] (* _Harvey P. Dale_, Jul 03 2021 *)"
			],
			"program": [
				"(PARI) Vec(x/(1-6*x+25*x^2)+O(x^99)) \\\\ _Charles R Greathouse IV_, Jun 14 2011"
			],
			"keyword": "sign,easy",
			"offset": "0,3",
			"author": "_Simon Plouffe_, Apr 06 2011",
			"ext": [
				"Minor edits by _N. J. A. Sloane_, Apr 06 2011",
				"Minor modification to Name by _Peter Bala_, Feb 02 2017"
			],
			"references": 3,
			"revision": 59,
			"time": "2021-07-03T19:25:14-04:00",
			"created": "2011-04-06T12:53:09-04:00"
		}
	]
}