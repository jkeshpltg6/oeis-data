{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A138351",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 138351,
			"data": "1,0,2,1,11,16,95,232,1085,3460,14820,54275,227095,895688,3756688,15462293,65586405,277342336,1192038266,5136760581,22357937431,97730561480,430177280197,1901975209706,8454151507801,37734802709796",
			"name": "Central moment sequence of tr(A^2) in USp(4).",
			"comment": [
				"If A is a random matrix in the compact group USp(4) (4 X 4 complex matrices which are unitary and symplectic), then a(n)=E[(tr(A^2)+1)^n] is the n-th central moment of the trace of A^2, since E[tr(A^2)] = -1 (see A138350)."
			],
			"link": [
				"Kiran S. Kedlaya and Andrew V. Sutherland, \u003ca href=\"https://arxiv.org/abs/0803.4462\"\u003eHyperelliptic curves, L-polynomials and random matrices\u003c/a\u003e, arXiv:0803.4462 [math.NT], 2008-2010.",
				"Kiran S. Kedlaya and Andrew V. Sutherland, \u003ca href=\"https://dspace.mit.edu/handle/1721.1/64701\"\u003eHyperelliptic curves, L-polynomials and random matrices\u003c/a\u003e, in Arithmetic, Geometry, Cryptography, and Coding Theory: International Conference, November 5-9, 2007, CIRM, Marseilles, France. Gilles Lachaud, Christophe Ritzenthaler, Michael A. Tsfasman, editors. 2009. (Contemporary Mathematics ; v.487).."
			],
			"formula": [
				"a(n) = (1/2)Integral_{x=0..Pi,y=0..Pi}(2cos(2x)+2cos(2y)+1)^n(2cos(x)-2cos(y))^2(2/Pi*sin^2(x))(2/Pi*sin^2(y))dxdy.",
				"a(n) = Sum_{i=0..n} binomial(n,i)*A138350(i)."
			],
			"example": [
				"a(4) = 11 because E[((tr(A^2)+1)^4] = 11 for a random matrix A in USp(4).",
				"a(4) = 1*A138350(0)+4*A138350(1)+6*A138350(2)+4*A138350(3)+1*A138350(4)",
				"= 1*1 + 4*(-1) + 6*3 + 4*(-6) + 1*20 = 11."
			],
			"mathematica": [
				"a126120[n_] := If[EvenQ[n], CatalanNumber[n/2], 0];",
				"a138364[n_] := If[EvenQ[n], 0, Binomial[n, Floor[n/2]], 0];",
				"a138350[n_] := a126120[n] a138364[n+1] - a138364[n] a126120[n+1];",
				"a[n_] := Sum[Binomial[n, i] a138350[i], {i, 0, n}];",
				"Table[a[n], {n, 0, 25}] (* _Jean-François Alcover_, Aug 13 2018 *)"
			],
			"xref": [
				"Cf. A138350."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Andrew V. Sutherland_, Mar 16 2008, Mar 31 2008",
			"references": 1,
			"revision": 10,
			"time": "2018-08-13T09:04:40-04:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}