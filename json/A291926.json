{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A291926",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 291926,
			"data": "1,5,0,18,25,20,0,61,68,64,72,103,110,134,0,138,141,140,141,172,191,228,225,244,306,281,272,339,384,412,0,390,421,372,472,395,441,486,495,473,566,576,629,735,626,661,706,707,741,825,782,751,811,924,930,908,927,975,1049,934,1018,1070,0",
			"name": "a(n) is the smallest integer k\u003e=0 such that 2^k contains every digit in base n, or 0 if no such integer exists.",
			"comment": [
				"a(n) \u003e= ceiling(log_2(n)*(n-1)), whenever a(n)\u003e0. This is because in order for an integer to have n digits in base n it must have at least a magnitude of n-1 in base n.",
				"a(n) = 0 at all powers of 2 (except 2 itself). This is because powers of 2 in power-of-2 bases can only have 2 distinct digits. Is a(n) equal to 0 for any other values of n?",
				"It seems that the base n representation of 2^(2*n^2) contains all n digits whenever n is not a power of 2. A proof of this would yield a negative answer to the above question. In the absence of a negative answer to this question, at least an algorithm would be desirable whose application to any concrete value of n solves the problem whether a(n)=0 for this n (for instance, if a(n)\u003cn^2 for all n then a proof of this would yield such an algorithm). - _Dimiter Skordev_, Aug 18 2021"
			],
			"link": [
				"Chai Wah Wu, \u003ca href=\"/A291926/b291926.txt\"\u003eTable of n, a(n) for n = 2..512\u003c/a\u003e (n = 2..256 from Ely Golden)."
			],
			"example": [
				"a(3) = 5, since 2^5 is the smallest power of 2 which contains every digit in base 3: Namely, 2^5 is 1012 in base 3, whereas the previous powers are 1, 2, 11, 22, and 121, respectively, none of which contain all possible base-3 digits."
			],
			"mathematica": [
				"TakeWhile[#, # \u003e -1 \u0026] \u0026@ Table[If[And[IntegerQ@ #, # \u003e 1] \u0026@ Log2@ n, 0, SelectFirst[Range[2^11], Times @@ DigitCount[2^#, n] \u003e 0 \u0026]] /. k_ /; MissingQ@ k -\u003e -1, {n, 2, 64}] (* _Michael De Vlieger_, Sep 05 2017 *)"
			],
			"program": [
				"(Python)",
				"def floorLog(b,n):",
				"    x=-1",
				"    while(n\u003e0):",
				"        x+=1",
				"        n//=b",
				"    return x",
				"def distinctDigits(n,b):",
				"    li=[]",
				"    while(n\u003e0):",
				"        li.append(n%b)",
				"        n//=b",
				"    li=list(set(li))",
				"    li.sort()",
				"    return li",
				"def iroot(k,n):",
				"    u, s = n, n+1",
				"    while u \u003c s:",
				"        s = u",
				"        t = (k-1) * s + n // (s**(k-1))",
				"        u = t // k",
				"    return s",
				"def perfectPower(n):",
				"    if(n==1): return 0",
				"    x=1",
				"    for i in range(2,floorLog(2,n)+1):",
				"        if(iroot(i,n)**i==n): x=i",
				"    return x",
				"def leastPandigital(b,n):",
				"    if(n\u003c=1 or b\u003c=1): return 0",
				"    if(n==2): return 2 if (b==(1\u003c\u003cb.bit_length())-1) else 1",
				"    if(iroot(perfectPower(n),n)==iroot(perfectPower(b),b)): return 0",
				"    a=(floorLog(b,n)*(n-1))",
				"    while(distinctDigits(b**a,n)!=list(range(n))): a+=1",
				"    return a",
				"for i in range(2,257):",
				"    print(str(i)+\" \"+str(leastPandigital(2,i)))",
				"(Python)",
				"from sympy.ntheory.digits import digits",
				"def a(n):",
				"    b = bin(n)[2:]",
				"    if b.strip('0') == '1': return int(n == 2)",
				"    k = (len(b)-1)*(n-1)",
				"    while len(set(digits(2**k, n)[1:])) != n: k += 1",
				"    return k",
				"print([a(n) for n in range(2, 65)]) # _Michael S. Branicky_, Oct 07 2021",
				"(PARI) a(n) = {if (n==2, return (1)); if (ispower(n,,\u0026k) \u0026\u0026 (k==2), return (0)); k = 1; while (#Set(digits(2^k, n)) != n, k++); k;} \\\\ _Michel Marcus_, Sep 06 2017"
			],
			"xref": [
				"Cf. A090493."
			],
			"keyword": "nonn,base",
			"offset": "2,2",
			"author": "_Ely Golden_, Sep 05 2017",
			"references": 3,
			"revision": 50,
			"time": "2021-10-08T12:05:03-04:00",
			"created": "2017-09-21T11:03:09-04:00"
		}
	]
}