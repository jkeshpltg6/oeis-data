{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A124224",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 124224,
			"data": "0,1,1,2,1,3,1,3,2,4,1,5,1,4,5,2,3,6,1,3,5,7,1,5,7,2,4,8,1,7,3,9,1,6,4,3,9,2,8,7,5,10,1,5,7,11,1,7,9,10,8,11,2,5,3,4,6,12,1,5,3,11,9,13,1,8,4,13,2,11,7,14,1,11,13,7,9,3,5,15,1,9,6,13,7,3,5,15,2,12,14,10,4,11,8,16",
			"name": "Table T(n,k) = reciprocal of k-th number prime to n, modulo n, for 1 \u003c= k \u003c= phi(n).",
			"comment": [
				"T(n,k) = smallest m such that A038566(n,k) * m = 1 (mod n).",
				"For n\u003e1 every row begins with 1 and ends with n-1.  T(n,k) = A038566(n,k)^(phi(n) - 1) (mod n). - _Geoffrey Critzer_, Jan 03 2015"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A124224/b124224.txt\"\u003eTable of n, a(n) for n = 1..10060\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ModularInverse.html\"\u003eModular Inverse\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) * A038566(n,k) = 1 (mod n), for n \u003e=1 and k=1..A000010(n). - _Wolfdieter Lang_, Oct 06 2016"
			],
			"example": [
				"The table T(n,k) starts:",
				"n\\k 1  2  2  3 4  5 6  7 8  9 10 11",
				"1:  0",
				"2:  1",
				"3:  1  2",
				"4:  1  3",
				"5:  1  3  2  4",
				"6:  1  5",
				"7:  1  4  5  2 3  6",
				"8:  1  3  5  7",
				"9:  1  5  7  2 4  8",
				"10: 1  7  3  9",
				"11: 1  6  4  3 9  2 8  7 5 10",
				"12: 1  5  7 11",
				"13: 1  7  9 10 8 11 2  5 3  4  6 12",
				"14: 1  5  3 11 9 13",
				"15: 1  8  4 13 2 11 7 14",
				"16: 1 11 13  7 9  3 5 15",
				"...",
				"n = 17: 1  9  6 13 7  3  5 15 2 12 14 10 4 11 8 16,",
				"n = 18: 1 11 13  5 7 17,",
				"n = 19: 1 10 13  5 4 16 11 12 17 2 7 8 3 15 14 6 9 18,",
				"n = 20: 1 7 3 9 11 17 13 19.",
				"... reformatted (extended and corrected), - _Wolfdieter Lang_, Oct 06 2016"
			],
			"maple": [
				"0,seq(seq(i^(-1) mod m, i = select(t-\u003eigcd(t,m)=1, [$1..m-1])),m=1..100); # _Robert Israel_, May 18 2014"
			],
			"mathematica": [
				"Table[nn = n; a = Select[Range[nn], CoprimeQ[#, nn] \u0026];",
				"PowerMod[a, -1, nn], {n, 1, 20}] // Grid (* _Geoffrey Critzer_, Jan 03 2015 *)"
			],
			"xref": [
				"Cf. A124223, A102057, A038566, A000010 (row lengths), A023896 (row sums after first)"
			],
			"keyword": "nonn,tabf",
			"offset": "1,4",
			"author": "_Franklin T. Adams-Watters_, Oct 20 2006",
			"references": 2,
			"revision": 22,
			"time": "2016-10-19T07:55:57-04:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}