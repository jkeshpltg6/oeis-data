{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A238385",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 238385,
			"data": "1,1,1,-1,2,1,2,-3,3,1,-6,8,-6,4,1,24,-30,20,-10,5,1,-120,144,-90,40,-15,6,1,720,-840,504,-210,70,-21,7,1,-5040,5760,-3360,1344,-420,112,-28,8,1,40320,-45360,25920,-10080,3024,-756,168,-36,9,1,-362880,403200,-226800,86400,-25200,6048,-1260,240,-45,10,1",
			"name": "Shifted lower triangular matrix A238363 with a main diagonal of ones.",
			"comment": [
				"Shift A238363 and add a main diagonal of ones to obtain this array. The row polynomials form a special Sheffer sequence of polynomials, an Appell sequence."
			],
			"formula": [
				"a(n,k) = (-1)^(n+k-1)*n!/((n-k)*k!) for k\u003cn and a(n,n)=1.",
				"Along the n-th diagonal (n\u003e0) Diag(n,k) = a(n+k,k) = (-1)^(n-1)(n-1)! * A007318(n+k,k).",
				"E.g.f.: (log(1+t)+1)*exp(x*t).",
				"E.g.f. for inverse: exp(x*t)/(log(1+t)+1).",
				"The lowering/annihilation and raising/creation operators for the row polynomials are L=D=d/dx and R=x+1/[(1+D)(1+log(1+D))], i.e., L p(n,x)= n*p(n-1,x) and R p(n,x)= p(n+1,x).",
				"E.g.f. of row sums: (log(1+t)+1)*exp(t). Cf. |row sums-1|=|A002741|.",
				"E.g.f. of unsigned row sums: (-log(1-t)+1)*exp(t). Cf. A002104 + 1.",
				"Let dP = A132440, the infinitesimal generator for the Pascal matrix, I, the identity matrix, and T, this entry's lower triangular matrix, then exp(T-I)=I+dP, i.e., T=I+log(I+dP). Also, ((T-I)_n)^n=0, where (T-I)_n denotes the n X n submatrix, i.e., (T-I)_n is nilpotent of order n. - _Tom Copeland_, Mar 02 2014",
				"Dividing each subdiagonal by its first element (-1)^(n-1)*(n-1)! yields Pascal's triangle A007318. This is equivalent to multiplying the e.g.f. by exp(t)/(log(1+t)+1). - _Tom Copeland_, Apr 16 2014",
				"From _Tom Copeland_, Apr 25 2014: (Start)",
				"A) T = [St1]*[dP]*[St2] + I = [padded A008275]*A132440*A048993 + I",
				"B) = [St1]*[dP]*[St1]^(-1) + I",
				"C) = [St2]^(-1)*[dP]*[St2] + I",
				"D) = [St2]^(-1)*[dP]*[St1]^(-1) + I,",
				"  where [St1]=padded A008275 just as [St2]=A048993=padded A008277 and I=identity matrix. Cf. A074909. (End)",
				"From _Tom Copeland_, Jul 26 2017: (Start)",
				"p_n(x) = (1 + log(1+D)) x^n = (1 + D - D^2/2 + D^3/3- ...) x^n = x^n + n! * Sum_(k=1,..,n) (-1)^(k+1) (1/k) x^(n-k)/(n-k)!.",
				"Unsigned T with the first two diagonals nulled gives an exponential infinitesimal generator M (infinigen) for the rencontres numbers A008290, and negated M gives the infinigen for A055137; i.e., with M = |T| - I - dP = -log(I-dP)-dP, then e^M = e^(-dP) / (I-dP) = lower triangular A008290, and e^(-M) = e^dP (I-dP) = A007318 * (I-dP) = lower triangular A055137. The matrix formulation is consistent with the operator relations  e^(-D) / (1-D) x^n = n-th row polynomial of A008290 and e^D (1-D) x^n = n-th row polynomial of A055137. (End)"
			],
			"example": [
				"The triangle a(n,k) begins:",
				"n\\k       0       1        2      3       4     5      6    7   8   9 10 ...",
				"0:        1",
				"1:        1       1",
				"2:       -1       2        1",
				"3:        2      -3        3      1",
				"4:       -6       8       -6      4       1",
				"5:       24     -30       20    -10       5     1",
				"6:     -120     144      -90     40     -15     6      1",
				"7:      720    -840      504   -210      70   -21      7    1",
				"8:    -5040    5760    -3360   1344    -420   112    -28    8   1",
				"9:    40320  -45360    25920 -10080    3024  -756    168  -36   9   1",
				"10: -362880  403200  -226800  86400  -25200  6048  -1260  240 -45  10  1",
				"... formatted by _Wolfdieter Lang_, Mar 09 2014",
				"----------------------------------------------------------------------------"
			],
			"xref": [
				"Cf. A002741, A007318, A008275, A008277, A008290, A048993, A055137, A074909, A132440, A238363."
			],
			"keyword": "sign,tabl,easy",
			"offset": "0,5",
			"author": "_Tom Copeland_, Feb 25 2014",
			"references": 16,
			"revision": 42,
			"time": "2017-07-27T09:09:48-04:00",
			"created": "2014-02-26T14:27:12-05:00"
		}
	]
}