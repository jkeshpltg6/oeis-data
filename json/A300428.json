{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A300428",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 300428,
			"data": "1,1,5,1,3,5,9,1,5,3,11,5,11,9,17,1,9,7,5,11,3,13,11,9,5,11,13,9,11,17,33,1,17,11,9,7,19,5,13,11,29,3,19,13,27,11,19,17,9,19,5,11,19,13,29,9,19,11,13,17,25,33,65,1,33,23,17,13,11,29,9,23,7",
			"name": "a(n) is the least positive k such that the binary representation of n appears as a substring in the binary representation of 1/k (ignoring the radix point and adding trailing zeros if necessary in case of a terminating expansion).",
			"comment": [
				"In other words, a(n) is the least k \u003e 0 such that floor((2^i) / k) mod A062383(n) = n for some integer i \u003e= 0.",
				"This sequence is similar to A035335 for the base 2.",
				"All terms are odd.",
				"All terms appears infinitely many times (as a(n) equals at least a(2*n) or a(2*n + 1)).",
				"See also A300475 for a similar sequence."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A300428/b300428.txt\"\u003eTable of n, a(n) for n = 1..8191\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A300428/a300428.gp.txt\"\u003ePARI program for A300428\u003c/a\u003e"
			],
			"formula": [
				"a(2^k) = 1 for any k \u003e= 0.",
				"a(2^k - 1) = 2^k + 1 for any k \u003e 1.",
				"a(A000975(k)) = 3 for any k \u003e 2.",
				"a(A033138(k)) = 7 for any k \u003e 4.",
				"a(n) \u003c= A300475(n) for any n \u003e 0."
			],
			"example": [
				"The first terms, alongside the binary representation of 1/a(n) with the earliest occurrence of the binary representation of n in parentheses, are:",
				"  n  a(n)    bin(1/a(n))",
				"  -- ----    -----------",
				"   1    1    (1).000...",
				"   2    1    (1.0)000...",
				"   3    5    0.00(11)001...",
				"   4    1    (1.00)000...",
				"   5    3    0.0(101)010...",
				"   6    5    0.00(110)011...",
				"   7    9    0.000(111)000...",
				"   8    1    (1.000)000...",
				"   9    5    0.001(1001)100...",
				"  10    3    0.0(1010)101...",
				"  11   11    0.000(1011)101...",
				"  12    5    0.00(1100)110...",
				"  13   11    0.000101(1101)000...",
				"  14    9    0.000(1110)001...",
				"  15   17    0.0000(1111)000...",
				"  16    1    (1.0000)000...",
				"  17    9    0.00011(10001)110...",
				"  18    7    0.00(10010)010...",
				"  19    5    0.001(10011)001...",
				"  20   11    0.0001011(10100)010..."
			],
			"program": [
				"(PARI) See Links section."
			],
			"xref": [
				"Cf. A000975, A033138, A035335, A062383, A300475."
			],
			"keyword": "nonn,base",
			"offset": "1,3",
			"author": "_Rémy Sigrist_, Mar 05 2018",
			"references": 3,
			"revision": 20,
			"time": "2018-03-11T10:09:06-04:00",
			"created": "2018-03-10T07:14:43-05:00"
		}
	]
}