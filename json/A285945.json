{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A285945",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 285945,
			"data": "1,11,11,1111,110,111111,1100,11111111,11000,1111111111,110000,111111111111,1100000,11111111111111,11000000,1111111111111111,110000000,111111111111111111,1100000000,11111111111111111111,11000000000,1111111111111111111111,110000000000",
			"name": "Binary representation of the diagonal from the corner to the origin of the n-th stage of growth of the two-dimensional cellular automaton defined by \"Rule 123\", based on the 5-celled von Neumann neighborhood.",
			"comment": [
				"Initialized with a single black (ON) cell at stage zero.",
				"Differs from A285841 beginning at a(28)."
			],
			"reference": [
				"S. Wolfram, A New Kind of Science, Wolfram Media, 2002; p. 170."
			],
			"link": [
				"Robert Price, \u003ca href=\"/A285945/b285945.txt\"\u003eTable of n, a(n) for n = 0..126\u003c/a\u003e",
				"Robert Price, \u003ca href=\"/A285945/a285945.tmp.txt\"\u003eDiagrams of first 20 stages\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"http://arxiv.org/abs/1503.01168\"\u003eOn the Number of ON Cells in Cellular Automata\u003c/a\u003e, arXiv:1503.01168 [math.CO], 2015",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ElementaryCellularAutomaton.html\"\u003eElementary Cellular Automaton\u003c/a\u003e",
				"S. Wolfram, \u003ca href=\"http://wolframscience.com/\"\u003eA New Kind of Science\u003c/a\u003e",
				"Wolfram Research, \u003ca href=\"http://atlas.wolfram.com/\"\u003eWolfram Atlas of Simple Programs\u003c/a\u003e",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_2D_5-Neighbor_Cellular_Automata\"\u003eIndex to 2D 5-Neighbor Cellular Automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_Elementary_Cellular_Automata\"\u003eIndex to Elementary Cellular Automata\u003c/a\u003e"
			],
			"formula": [
				"Conjectures from _Colin Barker_, May 02 2017: (Start)",
				"G.f.: (1 + 11*x - 100*x^2 - 110*x^3 - x^4 + 100*x^6) / ((1 - x)*(1 + x)*(1 - 10*x)*(1 + 10*x)*(1 - 10*x^2)).",
				"a(n) = 11*10^(n/2-1) for n\u003e0 and even.",
				"a(n) = (10^(n+1) - 1)/9 for n odd.",
				"a(n) = 111*a(n-2) - 1110*a(n-4) + 1000*a(n-6) for n\u003e6.",
				"(End)"
			],
			"mathematica": [
				"CAStep[rule_, a_] := Map[rule[[10 - #]] \u0026, ListConvolve[{{0, 2, 0},{2, 1, 2}, {0, 2, 0}}, a, 2],{2}];",
				"code = 123; stages = 128;",
				"rule = IntegerDigits[code, 2, 10];",
				"g = 2 * stages + 1; (* Maximum size of grid *)",
				"a = PadLeft[{{1}}, {g, g}, 0,Floor[{g, g}/2]]; (* Initial ON cell on grid *)",
				"ca = a;",
				"ca = Table[ca = CAStep[rule, ca], {n, 1, stages + 1}];",
				"PrependTo[ca, a];",
				"(* Trim full grid to reflect growth by one cell at each stage *)",
				"k = (Length[ca[[1]]] + 1)/2;",
				"ca = Table[Table[Part[ca[[n]] [[j]],Range[k + 1 - n, k - 1 + n]], {j, k + 1 - n, k - 1 + n}], {n, 1, k}];",
				"Table[FromDigits[Part[ca[[i]] [[i]], Range[i, 2 * i - 1]], 10], {i, 1, stages - 1}]"
			],
			"xref": [
				"Cf. A285841, A285946, A285947, A285948."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Robert Price_, Apr 29 2017",
			"references": 5,
			"revision": 13,
			"time": "2017-05-02T10:53:58-04:00",
			"created": "2017-04-30T07:53:08-04:00"
		}
	]
}