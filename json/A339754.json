{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A339754",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 339754,
			"data": "1,0,2,0,2,3,0,2,6,6,0,4,12,16,10,0,8,24,40,40,20,0,20,60,104,120,90,35,0,50,150,270,350,330,210,70,0,140,420,768,1040,1080,840,448,126,0,392,1176,2184,3080,3468,3108,2128,1008,252",
			"name": "Triangle read by rows: T(n,k) is the number of Dyck paths of semilength n having k symmetric vertices (n \u003e= 1, 1 \u003c= k \u003c= n).",
			"comment": [
				"A symmetric vertex is a vertex in the first half of the path (not including the midpoint) that is a mirror image of a vertex in the second half, with respect to reflection along the vertical line through the midpoint of the path."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A339754/b339754.txt\"\u003eRows n = 1..200, flattened\u003c/a\u003e",
				"Sergi Elizalde, \u003ca href=\"https://arxiv.org/abs/2002.12874\"\u003eThe degree of symmetry of lattice paths\u003c/a\u003e, arXiv:2002.12874 [math.CO], 2020.",
				"Sergi Elizalde, \u003ca href=\"https://www.mat.univie.ac.at/~slc/wpapers/FPSAC2020/26.html\"\u003eMeasuring symmetry in lattice paths and partitions\u003c/a\u003e, Sem. Lothar. Combin. 84B.26, 12 pp (2020)."
			],
			"example": [
				"For n=5 there are 4 Dyck paths with 2 symmetric vertices: uuuuddddud, uduuuudddd, uuudddudud, ududuuuddd.",
				"Triangle begins:",
				"  1;",
				"  0,   2;",
				"  0,   2,    3;",
				"  0,   2,    6,    6;",
				"  0,   4,   12,   16,   10;",
				"  0,   8,   24,   40,   40,   20;",
				"  0,  20,   60,  104,  120,   90,   35;",
				"  0,  50,  150,  270,  350,  330,  210,   70;",
				"  0, 140,  420,  768, 1040, 1080,  840,  448,  126;",
				"  0, 392, 1176, 2184, 3080, 3468, 3108, 2128, 1008, 252;",
				"  ..."
			],
			"maple": [
				"b:= proc(x, y, v) option remember; expand(",
				"      `if`(min(y, v, x-max(y, v))\u003c0, 0, `if`(x=0, 1, (l-\u003e add(add(",
				"      `if`(y+i=v+j, z, 1)*b(x-1, y+i, v+j), i=l), j=l))([-1, 1]))))",
				"    end:",
				"g:= proc(n) option remember; add(b(n, j$2), j=0..n) end:",
				"T:= (n, k)-\u003e coeff(g(n), z, k):",
				"seq(seq(T(n, k), k=1..n), n=1..10);  # _Alois P. Heinz_, Feb 12 2021"
			],
			"mathematica": [
				"b[x_, y_, v_] := b[x, y, v] = Expand[If[Min[y, v, x - Max[y, v]] \u003c 0, 0, If[x == 0, 1, Function[l, Sum[Sum[If[y + i == v + j, z, 1]*b[x - 1, y + i, v + j], {i, l}], {j, l}]][{-1, 1}]]]];",
				"g[n_] := g[n] = Sum[b[n, j, j], {j, 0, n}];",
				"T[n_, k_] := Coefficient[g[n], z, k];",
				"Table[Table[T[n, k], {k, 1, n}], {n, 1, 10}] // Flatten (* _Jean-François Alcover_, Feb 13 2021, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Row sums give A000108.",
				"Main diagonal gives A001405.",
				"Column k=2 gives 2*A005817(n-3) (for n\u003e2)."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Sergi Elizalde_, Feb 12 2021",
			"references": 1,
			"revision": 40,
			"time": "2021-02-13T19:38:13-05:00",
			"created": "2021-02-12T18:44:19-05:00"
		}
	]
}