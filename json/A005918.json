{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005918",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5918,
			"id": "M3843",
			"data": "1,5,14,29,50,77,110,149,194,245,302,365,434,509,590,677,770,869,974,1085,1202,1325,1454,1589,1730,1877,2030,2189,2354,2525,2702,2885,3074,3269,3470,3677,3890,4109,4334,4565,4802,5045,5294,5549,5810,6077,6350,6629",
			"name": "Number of points on surface of square pyramid: 3*n^2 + 2 (n\u003e0).",
			"comment": [
				"Also coordination sequence of the 5-connected (or bnn) net = hexagonal net X integers.",
				"Also (except for initial term) numbers of the form 3n^2+2 that are not squares. All numbers 3n^2+2 are == 2 (mod 3), and hence not squares. - _Cino Hilliard_, Mar 01 2003, modified by _Franklin T. Adams-Watters_, Jun 27 2014",
				"If a 2-set Y and a 3-set Z are disjoint subsets of an n-set X then a(n-4) is the number of 4-subsets of X intersecting both Y and Z. - _Milan Janjic_, Sep 08 2007",
				"Sums of three consecutive squares: (n - 2)^2 + (n - 1)^2 + n^2 for n \u003e 1. - _Keith Tyler_, Aug 10 2010"
			],
			"reference": [
				"H. S. M. Coxeter, Polyhedral numbers, in R. S. Cohen et al., editors, For Dirk Struik. Reidel, Dordrecht, 1974, pp. 25-35.",
				"B. Grünbaum, Uniform tilings of 3-space, Geombinatorics, 4 (1994), 49-56. See tiling #26.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"A. F. Wells, Three-Dimensional Nets and Polyhedra, Fig. 15.1 (e)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A005918/b005918.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Milan Janjic, \u003ca href=\"http://www.pmfbl.org/janjic/\"\u003eTwo Enumerative Functions\u003c/a\u003e",
				"M. Janjic and B. Petkovic, \u003ca href=\"http://arxiv.org/abs/1301.4550\"\u003eA Counting Function\u003c/a\u003e, arXiv preprint arXiv:1301.4550, 2013. - From _N. J. A. Sloane_, Feb 13 2013",
				"M. Janjic and B. Petkovic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Janjic/janjic45.html\"\u003eA Counting Function Generalizing Binomial Coefficients and Some Other Classes of Integers\u003c/a\u003e, J. Int. Seq. 17 (2014) # 14.3.5.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992; arXiv:0911.4975 [math.NT], 2009.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992.",
				"Reticular Chemistry Structure Resource (RCSR), \u003ca href=\"http://rcsr.net/nets/bnn\"\u003eThe bnn tiling (or net)\u003c/a\u003e",
				"B. K. Teo and N. J. A. Sloane, \u003ca href=\"http://neilsloane.com/doc/magic1/magic1.html\"\u003eMagic numbers in polygonal and polyhedral clusters\u003c/a\u003e, Inorgan. Chem. 24 (1985),4545-4558.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1)."
			],
			"formula": [
				"G.f.: (1 - x^2)*(1 - x^3)/(1 - x)^5 = (1+x)*(1+x+x^2)/(1-x)^3.",
				"Euler transform of length 3 sequence [ 5, -1, -1]. - _Michael Somos_, Aug 07 2014",
				"a(-n) = a(n) for all n in Z. - _Michael Somos_, Aug 07 2014",
				"a(n) = 3*a(n-1)-3*a(n-2)+a(n-3) for n\u003e3. - _Colin Barker_, Aug 07 2014",
				"a(0) = 1; for n \u003e 0, a(n) = A120328(n-1). - _Doug Bell_, Aug 18 2015",
				"E.g.f.: (2+3*x+3*x^2)*exp(x)-1. - _Robert Israel_, Aug 18 2015",
				"a(n) = A005448(n) + A005448(n+1), sum of 2 consecutive centered triangular numbers. - _R. J. Mathar_, Apr 28 2020",
				"a(n) = (n - 1)^2 + n^2 + (n + 1)^2. - _Charlie Marion_, Aug 31 2021"
			],
			"example": [
				"G.f. = 1 + 5*x + 14*x^2 + 29*x^3 + 50*x^4 + 77*x^5 + 110*x^6 + 149*x^7 + ..."
			],
			"maple": [
				"A005918:=-(z+1)*(z**2+z+1)/(z-1)**3; # _Simon Plouffe_ in his 1992 dissertation."
			],
			"mathematica": [
				"Join[{1}, Table[Plus@@(Range[n, n + 2]^2), {n, 0, 49}]] (* _Alonso del Arte_, Oct 27 2012 *)",
				"CoefficientList[Series[(1 - x^2) (1 - x^3)/(1 - x)^5, {x, 0, 40}], x] (* _Vincenzo Librandi_, Aug 07 2014 *)",
				"LinearRecurrence[{3,-3,1},{1,5,14,29},50] (* _Harvey P. Dale_, Dec 12 2015 *)"
			],
			"program": [
				"(PARI) sq3nsqp2(n) = { for(x=1,n, y = 3*x*x+2; print1(y, \", \") ) }",
				"(PARI) {a(n) = 3*n^2 + 2 - (n==0)}; /* _Michael Somos_, Aug 07 2014 */"
			],
			"xref": [
				"Cf. A120328, A206399.",
				"Partial sums give A063488."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 17,
			"revision": 94,
			"time": "2021-08-31T11:32:56-04:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}