{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A129116",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 129116,
			"data": "1,1,2,1,2,6,1,2,3,24,1,2,3,8,120,1,2,3,4,15,720,1,2,3,4,10,48,5040,1,2,3,4,5,18,105,40320,1,2,3,4,5,12,28,384,362880,1,2,3,4,5,6,21,80,945,3628800,1,2,3,4,5,6,14,32,162,3840,39916800,1,2,3,4,5,6,7,24,45,280",
			"name": "Multifactorial array: A(k,n) = k-tuple factorial of n, for positive n, read by ascending antidiagonals.",
			"comment": [
				"The term \"Quintuple factorial numbers\" is also used for the sequences A008546, A008548, A052562, A047055, A047056 which have a different definition. The definition given here is the one commonly used. This problem exists for the other rows as well. \"n!2\" = n!!, \"n!3\" = n!!!, \"n!4\" = n!!!!, etcetera. Main diagonal is A[n,n] = n!n = n.",
				"Similar to A114423 (with rows and columns exchanged). - _Georg Fischer_, Nov 02 2021"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A129116/b129116.txt\"\u003eAntidiagonals n = 1..141, flattened\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Multifactorial.html\"\u003eMultifactorial.\u003c/a\u003e"
			],
			"formula": [
				"A(k,n) = n!k.",
				"A(k,n) = M(n,k) in A114423. - _Georg Fischer_, Nov 02 2021"
			],
			"example": [
				"Table begins:",
				"k / A(k,n)",
				"1.|.1.2.6.24.120.720.5040.40320.362880.3628800... = A000142.",
				"2.|.1.2.3..8..15..48..105...384....945....3840... = A006882.",
				"3.|.1.2.3..4..10..18...28....80....162.....280... = A007661.",
				"4.|.1.2.3..4...5..12...21....32.....45.....120... = A007662.",
				"5.|.1.2.3..4...5...6...14....24.....36......50... = A085157.",
				"6.|.1.2.3..4...5...6....7....16.....27......40... = A085158."
			],
			"maple": [
				"A:= proc(k,n) option remember; if n \u003e= 1 then n* A(k, n-k) elif n \u003e= 1-k then 1 else 0 fi end: seq(seq(A(1+d-n, n), n=1..d), d=1..16); # _Alois P. Heinz_, Feb 02 2009"
			],
			"mathematica": [
				"A[k_, n_] := A[k, n] = If[n \u003e= 1, n*A[k, n-k], If[n \u003e= 1-k, 1, 0]]; Table[ A[1+d-n, n], {d, 1, 16}, {n, 1, d}] // Flatten (* _Jean-François Alcover_, May 27 2016, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A000142 (n!), A006882 (n!!), A007661 (n!!!), A007662(n!4), A085157 (n!5), A085158 (n!6), A114799 (n!7), A114800 (n!8), A114806 (n!9), A288327 (n!10).",
				"Cf. A114423 (transposed)."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "1,3",
			"author": "_Jonathan Vos Post_, May 24 2007",
			"ext": [
				"Corrected and extended by _Alois P. Heinz_, Feb 02 2009"
			],
			"references": 3,
			"revision": 23,
			"time": "2021-11-02T22:25:48-04:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}