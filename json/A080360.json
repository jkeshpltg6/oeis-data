{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A080360",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 80360,
			"data": "10,16,28,40,46,58,66,70,96,100,106,126,148,150,166,178,180,226,228,232,238,240,262,268,280,306,310,346,348,366,372,400,408,418,430,432,438,460,486,490,502,568,570,586,592,598,600,606,640,642,646,652,658,676",
			"name": "a(n) is the largest positive integer x such that the number of unitary-prime-divisors of x! equals n. Same as the largest positive integer x such that the number of primes in (x/2,x] equals n.",
			"reference": [
				"S. Ramanujan, Collected Papers of Srinivasa Ramanujan (Ed. G. H. Hardy, S. Aiyar, P. Venkatesvara and B. M. Wilson), Amer. Math. Soc., Providence, 2000, pp. 208-209."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A080360/b080360.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"S. Ramanujan, \u003ca href=\"http://ramanujan.sirinudi.org/Volumes/published/ram24.html\"\u003eA proof of Bertrand's postulate\u003c/a\u003e, J. Indian Math. Soc., 11 (1919), 181-182.",
				"V. Shevelev, \u003ca href=\"http://arxiv.org/abs/0909.0715\"\u003eRamanujan and Labos primes, their generalizations and classifications of primes\u003c/a\u003e, arXiv:0909.0715 [math.NT], 2009-2011.",
				"J. Sondow, \u003ca href=\"http://mathworld.wolfram.com/RamanujanPrime.html\"\u003eRamanujan Prime in MathWorld\u003c/a\u003e",
				"J. Sondow and E. W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/BertrandsPostulate.html\"\u003eBertrand's Postulate in MathWorld\u003c/a\u003e",
				"J. Sondow, \u003ca href=\"http://arxiv.org/abs/0907.5232\"\u003eRamanujan primes and Bertrand's postulate\u003c/a\u003e, Amer. Math. Monthly, 116 (2009), 630-635; arXiv:0907.5232 [math.NT], 2009-2010.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Ramanujan_prime\"\u003eRamanujan prime\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Max{x; Pi[x]-Pi[x/2]=n} = Max{x; A056171(x)=n} = Max{x; A056169(n!)=n}; where Pi()=A000720().",
				"a(n) = A104272(n+1) - 1. [_Jonathan Sondow_, Aug 11 2008]"
			],
			"example": [
				"n=5: in 46! five unitary-prime-divisors[UPD] appear: {29,31,37,41,43}. In larger factorials number of UPD is not more equal 5. Thus a(5)=46."
			],
			"mathematica": [
				"nn = 60; R = Table[0, {nn}]; s = 0;",
				"Do[If[PrimeQ[k], s++]; If[PrimeQ[k/2], s--]; If[s \u003c nn, R[[s+1]] = k], {k, Prime[3*nn]}];",
				"Rest[R] (* _Jean-François Alcover_, Dec 02 2018, after _T. D. Noe_ in A104272 *)"
			],
			"xref": [
				"Cf. A056171, A056169, A000720, A000142, A080359.",
				"Cf. A104272 (Ramanujan primes)."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Labos Elemer_, Feb 21 2003",
			"ext": [
				"Definition corrected by _Jonathan Sondow_, Aug 10 2008"
			],
			"references": 6,
			"revision": 27,
			"time": "2019-10-01T03:59:23-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}