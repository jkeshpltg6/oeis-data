{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A116915",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 116915,
			"data": "1,-1,1,0,-1,0,0,0,0,0,0,-1,0,0,0,1,0,0,-1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,-1,0,0,0,0,1,0,0,0,0,0,0,0,-1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,-1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,-1,0,0,0,0,0,0,0,0,0",
			"name": "Expansion of f(-x, -x^4)^2 / f(-x, -x^2) in powers of x where f(, ) is Ramanujan's general theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"This is an example of the quintuple product identity in the form f(a*b^4, a^2/b) - (a/b) * f(a^4*b, b^2/a) = f(-a*b, -a^2*b^2) * f(-a/b, -b^2) / f(a, b) where a = -x^3, b = -x^2. - _Michael Somos_, Jul 12 2012"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A116915/b116915.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/QuintupleProductIdentity.html\"\u003eQuintuple Product Identity\u003c/a\u003e"
			],
			"formula": [
				"Expansion of f(-x^4, -x^11) - x * f(-x, -x^14) where f() is Ramanujan's two-variable theta function. - _Michael Somos_, Nov 08 2015",
				"Euler transform of period 5 sequence [ -1, 1, 1, -1, -1, ...].",
				"G.f.: Sum_{k in Z} (-1)^k * (x^((15*k^2 - 7*k)/2) - x^((15*k^2 + 13*k)/2 + 1)).",
				"G.f.: Product_{k\u003e0} (1 - x^(5*k)) * (1 - x^(5*k - 1)) * (1 - x^(5*k - 4)) / ((1 - x^(5*k - 2)) * (1 - x^(5*k - 3))).",
				"- a(n) = A010815(5*n + 2)."
			],
			"example": [
				"G.f. = 1 - x + x^2 - x^4 - x^11 + x^15 - x^18 + x^23 + x^37 - x^44 + x^49 - x^57 + ...",
				"G.f. = q^49 - q^169 + q^289 - q^529 - q^1369 + q^1849 - q^2209 + q^2809 + q^4489 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (QPochhammer[ x^5] QPochhammer[ x, x^5] QPochhammer[ x^4, x^5])^2 / QPochhammer[ x], {x, 0, n}]; (* _Michael Somos_, Jul 12 2012 *)",
				"a[ n_] := With[ {k = Sqrt[ 120 n + 49]}, If[ IntegerQ[ k], -KroneckerSymbol[ 12, k], 0]]; (* _Michael Somos_, Nov 08 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = n = 5*n + 2; if( issquare(24*n + 1, \u0026n), -kronecker( 12, n))};",
				"(PARI) {a(n) = if( n\u003c0, 0, polcoeff( prod( k=1, n, (1 - x^k)^((k%5==0) + kronecker( 5, k)), 1 + x * O(x^n)), n))};"
			],
			"xref": [
				"Cf. A010815."
			],
			"keyword": "sign",
			"offset": "0,1",
			"author": "_Michael Somos_, Feb 26 2006",
			"references": 2,
			"revision": 15,
			"time": "2021-03-12T22:24:44-05:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}