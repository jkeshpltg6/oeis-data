{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A242219",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 242219,
			"data": "3,4,9,8,15,12,21,12,15,20,33,24,39,28,45,24,51,24,57,40,63,44,69,36,35,52,45,56,87,60,93,40,99,68,105,48,111,76,117,60,123,84,129,88,75,92,141,72,63,60,153,104,159,72,165,84,171,116,177,120,183,124,105,80,195,132,201,136,207,140,213,84,219,148,105,152,231,156",
			"name": "Smallest a(n) in Pythagorean triple (a, b, c) such that c(n) - b(n) = n.",
			"comment": [
				"The local minima a(n) predominantly fluctuate, with an increasing amplitude, between the multiples of the leg lengths of the smallest primitive triple (3,4,5) and of its symmetric counterpart (4,3,5). When n grows, minima appear from higher primitive triples which further increase the amplitude.",
				"We have a^2 = c^2 - b^2 = (c-b)(c+b) = n*(c+b). To find the least such square, use n=core(n)*f^2 with core = A007913, f = A000188(n), and look for the least c+b = c-b+2b = n+2b = core(n)*x^2 or x^2 = (n+2b)/core(n) = f^2 + 2b/core(n). The least such integer x is f+1 if core(n) is even, or else f+2. - _M. F. Hasler_, May 08 2014"
			],
			"link": [
				"Charles R Greathouse IV, \u003ca href=\"/A242219/b242219.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = min(sqrt(2n*h(i)-n^2), where h(i)=A009003(i)\u003en.",
				"a(n) = sqrt(( n + (x^2 - f^2)*core(n))*n ) where f = A000188(n), x = f+1 if core(n) = A007913(n) = n / f^2 is even, x = f+2 if core(n) is odd. - _M. F. Hasler_, May 08 2014"
			],
			"example": [
				"For n=7, a(7) = sqrt(2*7*h(12)-7^2) = 21;",
				"for n=8, a(8) = sqrt(2*8*h(3)-8^2) = 12;",
				"for n=9, a(9) = sqrt(2*9*h(5)-9^2) = 15;",
				"for n=10, a(10) = sqrt(2*10*h(7)-10^2) = 20."
			],
			"mathematica": [
				"(* first do *) hypos = A009003; (* then *)",
				"Table[ First[ Union[ Flatten[ Table[ Select[{Sqrt[2 hypos[[i]]*n - n^2]}, IntegerQ \u0026\u0026 hypos[[i]] \u003e n], {i, 1, Length[hypos]}]]]], {n, 1, 200}]",
				"(* view table *) ListLinePlot[%]"
			],
			"program": [
				"(PARI)  a(n)={ my( f=core(n,1)); sqrtint(( if( bittest( f[1],0), 4*f[2]+4, 2*f[2]+1)*f[1]+n )*n )} \\\\ _M. F. Hasler_, May 08 2014"
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_V.J. Pohjola_, May 07 2014",
			"references": 2,
			"revision": 23,
			"time": "2014-05-17T11:35:03-04:00",
			"created": "2014-05-17T11:35:03-04:00"
		}
	]
}