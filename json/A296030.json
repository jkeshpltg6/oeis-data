{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A296030",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 296030,
			"data": "0,0,1,0,1,1,0,1,-1,1,-1,0,-1,-1,0,-1,1,-1,2,-1,2,0,2,1,2,2,1,2,0,2,-1,2,-2,2,-2,1,-2,0,-2,-1,-2,-2,-1,-2,0,-2,1,-2,2,-2,3,-2,3,-1,3,0,3,1,3,2,3,3,2,3,1,3,0,3,-1,3,-2,3,-3,3,-3,2",
			"name": "Pairs of coordinates for successive integers in the square spiral (counterclockwise).",
			"comment": [
				"The spiral is also called the Ulam spiral, cf. A174344, A274923 (x and y coordinates). - _M. F. Hasler_, Oct 20 2019",
				"The n-th positive integer occupies the point whose x- and y-coordinates are represented in the sequence by a(2n-1) and a(2n), respectively. - _Robert G. Wilson v_, Dec 03 2017",
				"From _Robert G. Wilson v_, Dec 05 2017: (Start)",
				"The cover of the March 1964 issue of Scientific American (see link) depicts the Ulam Spiral with a heavy black line separating the numbers from their non-sequential neighbors. The pairs of coordinates for the points on this line, assuming it starts at the origin, form this sequence, negated.",
				"The first number which has an abscissa value of k beginning at 0: 1, 2, 10, 26, 50, 82, 122, 170, 226, 290, 362, 442, 530, 626, 730, 842, 962, ...; g.f.: -(x^3 +7x^2 -x +1)/(x-1)^3;",
				"The first number which has an abscissa value of -k beginning at 0: 1, 5, 17, 37, 65, 101, 145, 197, 257, 325, 401, 485, 577, 677, 785, 901, ...; g.f.: -(5x^2 +2x +1)/(x-1)^3;",
				"The first number which has an ordinate value of k beginning at 0: 1, 3, 13, 31, 57, 91, 133, 183, 241, 307, 381, 463, 553, 651, 757, 871, 993, ...; g.f.: -(7x^2+1)/(x-1)^3;",
				"The first number which has an ordinate value of -k beginning at 0: 1, 7, 21, 43, 73, 111, 157, 211, 273, 343, 421, 507, 601, 703, 813, 931, ...; g.f.: -(3x^2+4x+1)/(x-1)^3;",
				"The union of the four sequences above is A033638.",
				"(End)",
				"Sequences A174344, A268038 and A274923 start with the integer 0 at the origin (0,0). One might then prefer offset 0 as to have (a(2n), a(2n+1)) as coordinates of the integer n. - _M. F. Hasler_, Oct 20 2019",
				"This sequence can be read as an infinite table with 2 columns, where row n gives the x- and y-coordinate of the n-th point on the spiral. If the point at the origin has number 0, then the points with coordinates (n,n), (-n,n), (n,-n) and (n,-n) have numbers given by A002939(n) = 2n(2n-1): (0, 2, 12, 30, ...), A016742(n) = 4n^2: (0, 4, 16, 36, ...), A002943(n) = 2n(2n+1): (0, 6, 20, 42, ...) and A033996(n) = 4n(n+1): (0, 8, 24, 48, ...), respectively. - _M. F. Hasler_, Nov 02 2019"
			],
			"reference": [
				"S. Wolfram, A New Kind of Science, Wolfram Media, 2002; p. 935."
			],
			"link": [
				"Benjamin Mintz, \u003ca href=\"/A296030/b296030.txt\"\u003eTable of n, a(n) for n = 1..100000\u003c/a\u003e",
				"BackIssues.com, \u003ca href=\"http://backissues.com/issue/Scientific-American-March-1964\"\u003eScientific American March 1964 back issue\u003c/a\u003e",
				"Scientific American, \u003ca href=\"/A168022/a168022.pdf\"\u003eMarch 1964 cover\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Ulam_spiral\"\u003eUlam Spiral\u003c/a\u003e."
			],
			"formula": [
				"a(2*n-1) = A174344(n).",
				"a(2*n) = A274923(n) = -A268038(n).",
				"abs(a(n+2) - a(n)) \u003c 2.",
				"a(2*n-1)+a(2*n) = A180714(n).",
				"f(n) = floor(-n/4)*ceiling(-3*n/4 - 1/4) mod 2 + ceiling(n/8) (gives the pairs of coordinates for integers in the diagonal rays). - _Mikk Heidemaa_, May 07 2020"
			],
			"example": [
				"The integer 1 occupies the initial position, so its coordinates are {0,0}; therefore a(1)=0 and a(2)=0.",
				"The integer 2 occupies the position immediately to the right of 1, so its coordinates are {1,0}.",
				"The integer 3 occupies the position immediately above 2, so its coordinates are {1,1}; etc."
			],
			"mathematica": [
				"f[n_] := Block[{k = Ceiling[(Sqrt[n] - 1)/2], m, t}, t = 2k +1; m = t^2; t--; If[n \u003e= m - t, {k -(m - n), -k}, m -= t; If[n \u003e= m - t, {-k, -k +(m - n)}, m -= t; If[n \u003e= m - t, {-k +(m - n), k}, {k, k -(m - n - t)}]]]]; Array[f, 40] // Flatten (* _Robert G. Wilson v_, Dec 04 2017 *)",
				"f[n_] := Block[{k = Mod[ Floor[ Sqrt[4 If[OddQ@ n, (n + 1)/2 - 2, (n/2 - 2)] + 1]], 4]}, f[n - 2] + If[OddQ@ n, Sin[k*Pi/2], -Cos[k*Pi/2]]]; f[1] = f[2] = 0; Array[f, 90] (* _Robert G. Wilson v_, Dec 14 2017 *)",
				"f[n_] := With[{t = Round@ Sqrt@ n}, 1/2*(-1)^t*({1, -1}(Abs[t^2 - n] - t) + t^2 - n - Mod[t, 2])]; Table[f@ n, {n, 0, 95}] // Flatten (* _Mikk Heidemaa_ May 23 2020, after Stephen Wolfram *)"
			],
			"program": [
				"(Python) def get_coordinate(n):",
				"....k=ceil((sqrt(n)-1)/2)",
				"....t=2*k+1",
				"....m=t**2",
				"....t=t-1",
				"....if n \u003e= m - t:",
				"........return k - (m-n), -k",
				"....else:",
				"........m -= t",
				"....if n \u003e= m - t:",
				"........return -k, -k+(m-n)",
				"....else:",
				"........m -= t",
				"....if n \u003e= m-t:",
				"........return -k+(m-n), k",
				"....else:",
				"........return k, k-(m-n-t)",
				"(PARI) apply( {coords(n)=my(m=sqrtint(n), k=m\\/2); if(m \u003c= n -= 4*k^2, [n-3*k,-k], n \u003e= 0, [-k,k-n], n \u003e= -m, [-k-n,k], [k,3*k+n])}, [0..99]) \\\\ Use concat(%) to remove brackets '[', ']'. This function gives the coordinates of n on the spiral starting with 0 at (0,0), as shown in Examples for A174344, A274923, ..., so (a(2n-1),a(2n)) = coords(n-1). To start with 1 at (0,0), change n to n-=1 in sqrtint(). The inverse function is pos(x,y) given e.g. in A316328. - _M. F. Hasler_, Oct 20 2019"
			],
			"xref": [
				"Cf. A033638, A063826, A174344, A180714, A268038, A274923.",
				"Cf. Diagonal rays (+-n,+-n): A002939 (2n(2n-1): 0, 2, 12, 30, ...: NE), A016742 (4n^2: 0, 4, 16, 36, ...: NW), A002943 (2n(2n+1): 0, 6, 20, 42, ...: SW) and A033996 (4n(n+1): 0, 8, 24, 48, ...: SE)."
			],
			"keyword": "sign,easy,look",
			"offset": "1,19",
			"author": "_Benjamin Mintz_, Dec 03 2017",
			"references": 23,
			"revision": 139,
			"time": "2020-08-02T18:12:23-04:00",
			"created": "2017-12-16T01:14:48-05:00"
		}
	]
}