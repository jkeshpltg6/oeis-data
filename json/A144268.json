{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A144268",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 144268,
			"data": "1,5,1,55,15,1,935,220,75,30,1,21505,4675,2750,550,375,50,1,623645,129030,70125,30250,14025,16500,1875,1100,1125,75,1,21827575,4365515,2258025,1799875,451605,490875,211750,144375,32725,57750,13125,1925,2625,105,1,894930575",
			"name": "Partition number array, called M32(-5), related to A013988(n,m)= |S2(-5;n,m)| ( generalized Stirling triangle).",
			"comment": [
				"Each partition of n, ordered as in Abramowitz-Stegun (A-St order; for the reference see A134278), is mapped to a nonnegative integer a(n,k)=:M32(-5;n,k) with the k-th partition of n in A-St order.",
				"The sequence of row lengths is A000041 (partition numbers) [1, 2, 3, 5, 7, 11, 15, 22, 30, 42, ...].",
				"a(n,k) enumerates special unordered forests related to the k-th partition of n in the A-St order. The k-th partition of n is given by the exponents enk =(e(n,k,1),...,e(n,k,n)) of 1,2,...n. The number of parts is m = sum(e(n,k,j),j=1..n). The special (enk)-forest is composed of m rooted increasing (r+4)-ary trees if the outdegree is r \u003e= 0.",
				"If M32(-5;n,k) is summed over those k with fixed number of parts m one obtains triangle A013988(n,m)= |S2(-5;n,m)|, a generalization of Stirling numbers of the second kind. For S2(K;n,m), K from the integers, see the reference under A035342."
			],
			"link": [
				"W. Lang, \u003ca href=\"/A144268/a144268.txt\"\u003eFirst 10 rows of the array and more.\u003c/a\u003e",
				"W. Lang, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL12/Lang/lang.html\"\u003eCombinatorial Interpretation of Generalized Stirling Numbers\u003c/a\u003e, J. Int. Seqs. Vol. 12 (2009) 09.3.3."
			],
			"formula": [
				"a(n,k)= (n!/product(e(n,k,j)!*j!^(e(n,k,j),j=1..n))*product(|S2(-5,j,1)|^e(n,k,j),j=1..n) = M3(n,k)*product(|S2(-5,j,1)|^e(n,k,j),j=1..n), with |S2(-5,n,1)|= A008543(n-1) = (6*n-7)(!^6) (6-factorials) for n\u003e=2 and 1 if n=1 and the exponent e(n,k,j) of j in the k-th partition of n in the A-St ordering of the partitions of n. Exponents 0 can be omitted due to 0!=1. M3(n,k):= A036040(n,k), k=1..p(n), p(n):= A000041(n)."
			],
			"example": [
				"a(4,3)=75. The relevant partition of 4 is (2^2). The 75 unordered (0,2,0,0)-forests are composed of the following 2 rooted increasing trees 1--2,3--4; 1--3,2--4 and 1--4,2--3. The trees are 5-ary because r=1 vertices are 5-ary and for the leaves (r=0) the arity does not matter. Each of the three differently labeled forests comes therefore in 5^2=25 versions due to the two 5-ary root vertices."
			],
			"xref": [
				"Cf. A144267 (M32(-4) array)."
			],
			"keyword": "nonn,easy,tabf",
			"offset": "1,2",
			"author": "_Wolfdieter Lang_, Oct 09 2008",
			"references": 4,
			"revision": 14,
			"time": "2019-08-29T16:41:38-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}