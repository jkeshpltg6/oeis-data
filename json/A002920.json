{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002920",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2920,
			"id": "M4196 N1750",
			"data": "1,6,30,138,606,2586,10818,44574,181542,732678,2935218,11687202,46296210,182588850,717395262,2809372302,10969820358,42724062966,166015496838,643768299018,2491738141314,9628130289018,37146098272266,143110933254702,550643544948090",
			"name": "High-temperature series in w = tanh(J/kT) for ferromagnetic susceptibility for the spin-1/2 Ising model on hexagonal lattice.",
			"comment": [
				"Previous name was: Susceptibility series for hexagonal lattice.",
				"The hexagonal lattice is the familiar 2-dimensional lattice in which each point has 6 neighbors. This is sometimes called the triangular lattice.",
				"The actual susceptibility per spin is this series times m^2/kT."
			],
			"reference": [
				"C. Domb, Ising model, in Phase Transitions and Critical Phenomena, vol. 3, ed. C. Domb and M. S. Green, Academic Press, 1974; p. 380.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Y. Chan, A. J. Guttmann, B. G. Nickel, J. H. H. Perk, \u003ca href=\"https://doi.org/10.1007/s10955-011-0212-0\"\u003eThe Ising Susceptibility Scaling Function\u003c/a\u003e, J Stat Phys 145 (2011), 549-590; arXiv:\u003ca href=\"https://arxiv.org/abs/1012.5272\"\u003e1012.5272\u003c/a\u003e [cond-mat.stat-mech], 2010-2020. Gives 320 terms in the file Triangle_v319.",
				"C. Domb, \u003ca href=\"/A007239/a007239.pdf\"\u003eIsing model\u003c/a\u003e, Phase Transitions and Critical Phenomena 3 (1974), 257, 380-381, 384-387, 390-391, 412-423. (Annotated scanned copy)",
				"Michael E. Fisher, \u003ca href=\"https://doi.org/10.1103/PhysRev.113.969\"\u003eTransformations of Ising Models\u003c/a\u003e, Phys. Rev. 113 (1959), 969-981.",
				"M. E. Fisher and R. J. Burford, \u003ca href=\"https://doi.org/10.1103/PhysRev.156.583\"\u003eTheory of critical point scattering and correlations I: the Ising model\u003c/a\u003e, Phys. Rev. 156 (1967), 583-621.",
				"G. Nebe and N. J. A. Sloane, \u003ca href=\"http://www.math.rwth-aachen.de/~Gabriele.Nebe/LATTICES/A2.html\"\u003eHome page for hexagonal (or triangular) lattice A2\u003c/a\u003e",
				"M. F. Sykes, \u003ca href=\"https://doi.org/10.1063/1.1724212\"\u003eSome counting theorems in the theory of the Ising problem and the excluded volume problem\u003c/a\u003e, J. Math. Phys., 2 (1961), 52-62.",
				"M. F. Sykes, D. G. Gaunt, P. D. Roberts and J. A. Wyles, \u003ca href=\"https://doi.org/10.1088/0305-4470/5/5/004\"\u003eHigh temperature series for the susceptibility of the Ising model, I. Two dimensional lattices\u003c/a\u003e, J. Phys. A 5 (1972) 624-639."
			],
			"formula": [
				"G.f.: (h(v(w)) + h(-v(w))) / 2, where h(v) is the g.f. of A002910 and v(w)^2 = w*(1+w)/(1+w^3) [Fisher, p. 979]. - _Andrey Zabolotskiy_, Mar 01 2021"
			],
			"xref": [
				"Cf. A002910, A128834, A047709, A002919."
			],
			"keyword": "nonn,nice",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Edited and extended from Chan et al by _Andrey Zabolotskiy_, Mar 03 2021"
			],
			"references": 4,
			"revision": 27,
			"time": "2021-03-03T06:45:27-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}