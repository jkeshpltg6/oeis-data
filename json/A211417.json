{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A211417",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 211417,
			"data": "1,77636318760,53837289804317953893960,43880754270176401422739454033276880,38113558705192522309151157825210540422513019720,34255316578084325260482016910137568877961925210286281393760",
			"name": "Integral factorial ratio sequence: a(n) = (30*n)!*n!/((15*n)!*(10*n)!*(6*n)!).",
			"comment": [
				"The integrality of this sequence can be used to prove Chebyshev's estimate C(1)*x/log(x) \u003c= #{primes \u003c= x} \u003c= C(2)*x/log(x), for x sufficiently large; the constant C(1) = 0.921292... and C(2) = 1.105550.... Chebyshev's approach used the related step function floor(x) -floor(x/2) -floor(x/3) -floor(x/5) +floor(x/30). See A182067.",
				"This sequence is one of the 52 sporadic integral factorial ratio sequences of height 1 found by V. I. Vasyunin.",
				"The o.g.f. sum {n \u003e= 0} a(n)*z^n is a generalized hypergeometric series of type 8F7 (see Bober, Table 2, Entry 31) and is an algebraic function of degree 483840 over the field of rational functions Q(z) (see Rodriguez-Villegas). Bober remarks that the monodromy group of the differential equation satisfied by the o.g.f. is W(E_8), the Weyl group of the E_8 root system.",
				"See the Bala link for the proof that a(n), n = 0,1,2..., is an integer.",
				"Congruences: a(p^k) == a(p^(k-1)) ( mod p^(3*k) ) for any prime p \u003e= 5 and any positive integer k (write a(n) as C(30*n,15*n)*C(15*n,5*n)/C(6*n,n) and use equation 39 in Mestrovic, p. 12). More generally, the congruences a(n*p^k) == a(n*p^(k-1)) ( mod p^(3*k) ) may hold for any prime p \u003e= 5 and any positive integers n and k. Cf. A295431. - _Peter Bala_, Jan 24 2020"
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A211417/b211417.txt\"\u003eTable of n, a(n) for n = 0..50\u003c/a\u003e",
				"Peter Bala, \u003ca href=\"/A211417/a211417_1.txt\"\u003eProof of the integrality of A211417 and A211418\u003c/a\u003e",
				"Frits Beukers, \u003ca href=\"http://www.ams.org/notices/201401/rnoti-p48.pdf\"\u003eHypergeometric functions, how special are they?\u003c/a\u003e, Notices Amer. Math. Soc. 61 (2014), no. 1, 48--56. MR3137256",
				"J. W. Bober, \u003ca href=\"http://arxiv.org/abs/0709.1977\"\u003eFactorial ratios, hypergeometric series, and a family of step functions\u003c/a\u003e, arXiv:0709.1977 [math.NT], 2007; J. London Math. Soc., 79, Issue 2, (2009), 422-444.",
				"R. Mestrovic, \u003ca href=\"http://arxiv.org/abs/1111.3057\"\u003eWolstenholme's theorem: Its Generalizations and Extensions in the last hundred and fifty years (1862-2011)\u003c/a\u003e, arXiv:1111.3057 [math.NT], 2011.",
				"F. Rodriguez-Villegas, \u003ca href=\"http://arxiv.org/abs/math/0701362\"\u003eIntegral ratios of factorials and algebraic hypergeometric functions\u003c/a\u003e, arXiv:math.NT/0701362, 2007.",
				"Fernando Rodriguez Villegas, \u003ca href=\"https://arxiv.org/abs/1907.02722\"\u003eMixed Hodge numbers and factorial ratios\u003c/a\u003e, arXiv:1907.02722 [math.NT], 2019.",
				"K. Soundararajan, \u003ca href=\"https://arxiv.org/abs/1901.05133\"\u003eIntegral Factorial Ratios\u003c/a\u003e, arXiv:1901.05133 [math.NT], 2019.",
				"Wadim Zudilin, \u003ca href=\"http://mathoverflow.net/questions/26336/\"\u003eInteger-valued factorial ratios\u003c/a\u003e, MathOverflow question 26336, 2010."
			],
			"formula": [
				"a(n) ~ 2^(14*n-1) * 3^(9*n-1/2) * 5^(5*n-1/2) / sqrt(Pi*n). - _Vaclav Kotesovec_, Aug 30 2016"
			],
			"mathematica": [
				"Table[(30 n)!*n!/((15 n)!*(10 n)!*(6 n)!), {n, 0, 5}] (* _Michael De Vlieger_, Oct 02 2015 *)"
			],
			"program": [
				"(PARI) a(n) = (30*n)!*n!/((15*n)!*(10*n)!*(6*n)!);",
				"vector(10, n, a(n-1)) \\\\ _Altug Alkan_, Oct 02 2015",
				"(MAGMA) [Factorial(30*n)*Factorial(n)/(Factorial(15*n)*Factorial(10*n)*Factorial(6*n)): n in [0..10]]; // _Vincenzo Librandi_, Oct 03 2015"
			],
			"xref": [
				"Cf. A182067, A211418, A061162, A061163, A061164, A091496, A091527, A112292, A182400, A211419, A211420, A211421, A276100, A262733, A295431."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Peter Bala_, Apr 11 2012",
			"references": 7,
			"revision": 55,
			"time": "2021-10-06T14:19:51-04:00",
			"created": "2012-04-11T13:01:37-04:00"
		}
	]
}