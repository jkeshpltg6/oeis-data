{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A309771",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 309771,
			"data": "2,5,4,1,18,4,5,1,2,5,6,1,3,2,3,1,2,5,12,1,5,6,4,1,2,3,10,1,5,2,3,1,2,4,4,1,5,10,7,1,2,5,8,1,3,2,3,1,2,6,8,1,4,3,13,1,2,3,5,1,5,2,3,1,2,6,4,1,6,10,6,1,2,4,9,1,3,2,3,1,2",
			"name": "a(n) is the number of odd numbers found in the Collatz trajectory of 6*n + 3 before reaching a number x == 5 (mod 8).",
			"comment": [
				"The number reached at the end of the computation, 8x + 5, will need more than 2 even steps to get to an odd number in the Collatz trajectory.",
				"a(3 + 4*n) = 1 always because 6*(3 + 4*n) + 3 = 21 + 24*n = 8*(3*n + 2) + 5.",
				"There's an interesting pattern in these seemingly chaotic numbers:",
				"   a(12 + 32*n) = 3,",
				"   a(13 + 32*n) = 2,",
				"   a(14 + 32*n) = 3,",
				"   a(15 + 32*n) = a(3 + 4(8*n + 3)) = 1,",
				"   a(16 + 32*n) = 2.",
				"A general formula for a(n), if it exists, could shed some light on the Collatz problem. The big spike at n=4 (large sequence for 27) seems a mystery for me. For big values of n we don't usually get big a(n), probably because of the presence of patterns like the above (just an observation)."
			],
			"link": [
				"Máximo Pérez López, \u003ca href=\"/A309771/b309771.txt\"\u003eTable of n, a(n) for n = 0..20000\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Collatz_conjecture\"\u003eCollatz Conjecture\u003c/a\u003e"
			],
			"formula": [
				"Not known for now, a(n) is calculated from computer program and definition of the Collatz function, e.g., f(x) = 3x + 1 if x is odd, or x/2 if x is even."
			],
			"example": [
				"For n = 0, 6*0 + 3 = 3. Collatz sequence of 3 omitting the even numbers: 3, 5. 5 = 8*0 + 5 so the computation stops: 2 odd numbers found.",
				"For n = 1, 6*1 + 3 = 9. Sequence: 9, 7, 11, 17, 13. 13 = 8*1 + 5, halt. a(1) = 5."
			],
			"mathematica": [
				"Array[Count[NestWhileList[If[EvenQ@ #, #/2, 3 # + 1] \u0026, 6 # + 3, Mod[#, 8] != 5 \u0026], _?OddQ] \u0026, 81, 0] (* _Michael De Vlieger_, Sep 27 2019 *)"
			],
			"program": [
				"(C)",
				"#include \u003cstdio.h\u003e",
				"#include \u003cstdlib.h\u003e",
				"int collatzOdd(int n) {",
				"        int i = 1;",
				"        int r;",
				"        while ( (r = n % 8) != 5) {",
				"                i++;",
				"                if (r == 1) {",
				"                        n = 6*(n / 8) + 1;",
				"                } else {",
				"                        n = 6*(n / 4) + 5;",
				"                }",
				"        }",
				"        return i;",
				"}",
				"int main(int argc, char *argv[]) {",
				"        int i = 0;",
				"        while (i \u003c= 20000) {",
				"                printf(\"%d %d\\n\", i, collatzOdd((6 * i) + 3));",
				"                i++;",
				"        }",
				"        return (EXIT_SUCCESS);",
				"}"
			],
			"keyword": "easy,nonn",
			"offset": "0,1",
			"author": "_Máximo Pérez López_, Aug 16 2019",
			"references": 1,
			"revision": 23,
			"time": "2019-10-23T15:44:47-04:00",
			"created": "2019-10-23T15:44:47-04:00"
		}
	]
}