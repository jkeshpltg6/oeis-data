{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A340620",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 340620,
			"data": "3,6,6,10,28,10,15,81,81,15,21,186,354,186,21,28,371,1137,1137,371,28,36,672,3018,4836,3018,672,36,45,1134,7023,16374,16374,7023,1134,45,55,1812,14829,47286,68644,47286,14829,1812,55,66,2772,29043,121314,240021,240021,121314,29043,2772,66",
			"name": "T(n,k) is the number of 4-ary strings of length n+1 with k+1 indispensable digits and a nonzero leading digit with 0 \u003c= k \u003c= n.",
			"comment": [
				"A digit in a string is called indispensable if it is greater than the following digit or equal to the following digits which are eventually greater than the following digit.  We also assume that there is an invisible digit 0 at the end of any string.  For example, in the string 33102232, the digits 3, 3, 1, 3, and 2 are indispensable (from the left).",
				"T(n,k) is also the number of integers m where the length of base-4 representation of m is n+k and the digit sum of the base-4 representation of 3m is 3(k+1)."
			],
			"link": [
				"J. Y. Choi, \u003ca href=\"http://nntdm.net/volume-25-2019/number-2/40-48/\"\u003eIndispensable digits for digit sums\u003c/a\u003e, Notes on Number Theory and Discrete Mathematics, 25(2), (2019), pp. 40-48.",
				"J. Y. Choi, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL22/Choi/choi15.html\"\u003eDigit sums generalizing binomial coefficients\u003c/a\u003e, J. Integer Seq. 22 (2019), Article 19.8.3."
			],
			"formula": [
				"T(n,k) = A330509(n+1,k+1) - A330509(n,k+1)."
			],
			"example": [
				"Triangle begins",
				"   3;",
				"   6,   6;",
				"  10,  28,   10;",
				"  15,  81,   81,   15;",
				"  21, 186,  354,  186,   21;",
				"  28, 371, 1137, 1137,  371,  28;",
				"  36, 672, 3018, 4836, 3018, 672, 36;",
				"  ...",
				"There are 6 4-ary strings (10, 12, 13, 20, 23, 30) of length 2 with 1 indispensable digits and a nonzero leading digit.",
				"There are 6 4-ary strings (11, 21, 22, 31, 32, 33) of length 2 with 2 indispensable digits and a nonzero leading digit.",
				"There are 10 4-ary strings (111, 211, 221, 222, 311, 321, 322, 331, 332, 333) of length 3 with 3 indispensable digits and a nonzero leading digit.",
				"Hence, T(1,0)=6, T(1,1)=6, T(2,2)=10."
			],
			"program": [
				"(PARI) A008287(n, k) = if(n\u003c0, 0, polcoeff((1 + x + x^2 + x^3)^n, k));",
				"A330509(n, k) = A008287(n, 3*k-2)+A008287(n, 3*k-1) + A008287(n, 3*k);",
				"T(n, k) = A330509(n+1,k+1) - A330509(n,k+1);",
				"tabl(nn) = for (n=0, nn, for (k=0, n, print1(T(n, k), \", \"))); \\\\ _Michel Marcus_, Jan 19 2021"
			],
			"xref": [
				"Cf. A330381, A330509, A330510, A005773, A008287."
			],
			"keyword": "nonn,base",
			"offset": "0,1",
			"author": "_Ji Young Choi_, Jan 13 2021",
			"ext": [
				"More terms from _Michel Marcus_, Jan 19 2021"
			],
			"references": 0,
			"revision": 20,
			"time": "2021-03-14T13:33:28-04:00",
			"created": "2021-03-14T08:09:46-04:00"
		}
	]
}