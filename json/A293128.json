{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A293128",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 293128,
			"data": "1,1,6,51,588,7990,126060,2242618,44546320,977152266,23500234512,615372604033,17442275104496,532242021137346,17399782340548920,606732491690590816,22477989291826848000,881635273413199806994,36493478646922003374096,1589642562747880936613248",
			"name": "Number of standard Young tableaux of 2n cells and height \u003c= n.",
			"comment": [
				"Also the number of standard Young tableaux of 2n cells and \u003c= n columns.",
				"Also the number of 2n-length words w over n-ary alphabet {a1,a2,...,an} such that for every prefix z of w we have #(z,a1) \u003e= #(z,a2) \u003e= ... \u003e= #(z,an), where #(z,x) counts the letters x in word z. The a(2) = 6 words of length 4 over alphabet {a,b} are: aaaa, aaab, aaba, abaa, aabb, abab."
			],
			"link": [
				"Vaclav Kotesovec, \u003ca href=\"/A293128/b293128.txt\"\u003eTable of n, a(n) for n = 0..41\u003c/a\u003e (terms 0..32 from Alois P. Heinz)",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Young_tableau\"\u003eYoung tableau\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A182172(2n,n)."
			],
			"maple": [
				"h:= l-\u003e (n-\u003e add(i, i=l)!/mul(mul(1+l[i]-j+add(`if`(l[k]",
				"     \u003cj, 0, 1), k=i+1..n), j=1..l[i]), i=1..n))(nops(l)):",
				"g:= (n, i, l)-\u003e `if`(n=0 or i=1, h([l[], 1$n]), add(",
				"               g(n-i*j, i-1, [l[], i$j]), j=0..n/i)):",
				"a:= n-\u003e g(2*n, n, []):",
				"seq(a(n), n=0..15);"
			],
			"mathematica": [
				"h[l_] := With[{n = Length[l]}, Total[l]!/Product[Product[1 + l[[i]] - j + Sum[If[l[[k]] \u003c j, 0, 1], {k, i + 1, n}], {j, 1, l[[i]]}], {i, 1, n}]];",
				"g[n_, i_, l_] := If[n == 0 || i == 1, h[Join[l, Table[1, {n}]]], Sum[g[n - i*j, i - 1, Join[l, Table[i, {j}]]], {j, 0, n/i}]];",
				"a[n_] := g[2n, n, {}];",
				"a /@ Range[0, 15] (* _Jean-François Alcover_, Jan 02 2021, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A182172, A267436."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Alois P. Heinz_, Sep 30 2017",
			"references": 4,
			"revision": 16,
			"time": "2021-01-02T08:01:03-05:00",
			"created": "2017-10-01T09:02:59-04:00"
		}
	]
}