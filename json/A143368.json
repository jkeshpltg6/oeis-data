{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A143368",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 143368,
			"data": "0,1,8,4,25,72,10,56,154,320,20,105,280,570,1000,35,176,459,920,1595,2520,56,273,700,1386,2380,3731,5488,84,400,1012,1984,3380,5264,7700,10752,120,561,1404,2730,4620,7155,10416,14484,19440",
			"name": "Triangle read by rows: T(n,k) is the Wiener index of a k X n grid (i.e., P_k X P_n, where P_m is the path graph on m vertices; 1 \u003c= k \u003c= n).",
			"comment": [
				"The Wiener index of a connected graph is the sum of distances between all unordered pairs of vertices in the graph.",
				"This is the lower triangular half of a symmetric square array."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A143368/b143368.txt\"\u003eTable of n, a(n) for n = 1..11325\u003c/a\u003e (rows 1 \u003c= n \u003c= 150).",
				"A. Graovac and T. Pisanski, \u003ca href=\"http://dx.doi.org/10.1007/BF01166923\"\u003eOn the Wiener index of a graph\u003c/a\u003e, J. Math. Chem., 8 (1991), 53-62.",
				"B. E. Sagan, Y-N. Yeh and P. Zhang, \u003ca href=\"http://dx.doi.org/10.1002/(SICI)1097-461X(1996)60:5\u0026lt;959::AID-QUA2\u0026gt;3.0.CO;2-W\"\u003eThe Wiener Polynomial of a Graph\u003c/a\u003e, Internat. J. of Quantum Chem., 60, 1996, 959-969.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GridGraph.html\"\u003eGrid Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/WienerIndex.html\"\u003eWiener Index\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = k*n*(n+k)*(k*n-1)/6 (k, n \u003e= 1)."
			],
			"example": [
				"Presentation as symmetric square array starts:",
				"======================================================",
				"n\\k|   1   2    3    4    5    6     7     8     9",
				"---|--------------------------------------------------",
				"1  |   0   1    4   10   20   35    56    84   120 ...",
				"2  |   1   8   25   56  105  176   273   400   561 ...",
				"3  |   4  25   72  154  280  459   700  1012  1404 ...",
				"4  |  10  56  154  320  570  920  1386  1984  2730 ...",
				"5  |  20 105  280  570 1000 1595  2380  3380  4620 ...",
				"6  |  35 176  459  920 1595 2520  3731  5264  7155 ...",
				"7  |  56 273  700 1386 2380 3731  5488  7700 10416 ...",
				"8  |  84 400 1012 1984 3380 5264  7700 10752 14484 ...",
				"9  | 120 561 1404 2730 4620 7155 10416 14484 19440 ...",
				"... - _Andrew Howroyd_, May 27 2017",
				"T(2,2)=8 because in a square we have four distances equal to 1 and two distances equal to 2.",
				"T(2,1)=1 because on the path graph on two vertices there is one distance equal to 1.",
				"T(3,2)=25 because on the P(2) X P(3) graph there are 7 distances equal to 1, 6 distances equal to 2 and 2 distances equal to 3, with 7*1 + 6*2 + 2*3 = 25.",
				"Triangle starts: 0; 1,8; 4,25,72; 10,56,154,320;"
			],
			"maple": [
				"T:=proc(n, k) options operator, arrow: (1/6)*k*n*(n+k)*(k*n-1) end proc: for n to 9 do seq(T(n,k),k=1..n) end do; # yields sequence in triangular form"
			],
			"mathematica": [
				"Table[k n (n + k) (k n - 1)/6, {n, 9}, {k, n}] // Flatten (* _Michael De Vlieger_, May 28 2017 *)"
			],
			"program": [
				"(PARI)",
				"T(n,k)=k*n*(n+k)*(k*n-1)/6;",
				"for (n=1,8,for(k=1,8,print1(T(n,k),\", \"));print) \\\\ _Andrew Howroyd_, May 27 2017"
			],
			"xref": [
				"Cf. A180569 (row 3), A131423 (row 2).",
				"Main diagonal is A143945.",
				"Cf. A245826."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Emeric Deutsch_, Sep 05 2008",
			"references": 3,
			"revision": 30,
			"time": "2017-07-21T17:34:01-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}