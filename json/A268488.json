{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A268488",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 268488,
			"data": "1,19,29,13,49,59,23,79,89,11,109,119,43,139,149,53,169,179,21,199,209,73,229,239,83,259,269,31,289,299,103,319,329,113,349,359,41,379,389,133,409,419,143,439,449,51,469,479,163,499,509,173,529,539,61",
			"name": "Least number k of the form k = n*(k % 10) + [k / 10], where k % 10 = last digit of k, [k / 10] = k without its last digit.",
			"comment": [
				"E. Angelini considered 3, 29, 289, 321, ... obtained by iteration of this map, while the lexicographic first nontrivial sequence obtained that way is 2, 19, 21, 209, 2089, 2321, 23209, 77363, 773629, ... See A268492, A268493 for these two sequences."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A268488/b268488.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Eric Angelini (and reply by M. Hasler), \u003ca href=\"http://list.seqfan.eu/pipermail/seqfan/2016-February/016113.html\"\u003e3, 29, 289, 321, ...\u003c/a\u003e, SeqFan list, Feb. 13, 2016",
				"\u003ca href=\"/index/Rec#order_18\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,-1)."
			],
			"formula": [
				"G.f.: x*(1 +19*x +29*x^2 +13*x^3 +49*x^4 +59*x^5 +23*x^6 +79*x^7 +89*x^8 +9*x^9 +71*x^10 +61*x^11 +17*x^12 +41*x^13 +31*x^14 +7*x^15 +11*x^16 +x^17) / ((1 -x)^2*(1 +x +x^2)^2*(1 +x^3 +x^6)^2). - _Colin Barker_, Feb 15 2016 and Feb 22 2016",
				"a(n) = 2*a(n-9)-a(n-18) for n\u003e18. - _Colin Barker_, Feb 15 2016",
				"a(n) = if n mod 9 == 1 then (n-1)/9*10+1 else if n mod 3 == 1 then (n-1)/3*10+3 else n*10-1, cf. SeqFan post for the proof. This implies the above recurrence relation and generating function. - _M. F. Hasler_, Feb 15 2016"
			],
			"mathematica": [
				"Table[SelectFirst[Range@ 1000, # == n Mod[#, 10] + Floor[#/10] \u0026], {n,",
				"  55}] (* Version 10, or *)",
				"Table[k = 1; While[k != n Mod[k, 10] + Floor[k/10], k++]; k, {n, 55}] (* _Michael De Vlieger_, Feb 15 2016 *)"
			],
			"program": [
				"(PARI) A268488(n)=if(n%9==1,n\\9*10+1,if(n%3==1,n\\3*10+3,n*10-1))",
				"(PARI) a(n) = k=1; while(k != n*(k%10)+k\\10, k++); k",
				"vector(100, n, a(n)) \\\\ _Colin Barker_, Feb 15 2016",
				"(PARI) Vec(x*(1 +19*x +29*x^2 +13*x^3 +49*x^4 +59*x^5 +23*x^6 +79*x^7 +89*x^8 +9*x^9 +71*x^10 +61*x^11 +17*x^12 +41*x^13 +31*x^14 +7*x^15 +11*x^16 +x^17) / ((1 -x)^2*(1 +x +x^2)^2*(1 +x^3 +x^6)^2) + O(x^40)) \\\\ _Colin Barker_, Feb 22 2016"
			],
			"xref": [
				"Cf. A268492 and A268493 for the orbits of 2 and 3 under this map."
			],
			"keyword": "nonn,base,easy",
			"offset": "1,2",
			"author": "_M. F. Hasler_, Feb 14 2016",
			"references": 3,
			"revision": 27,
			"time": "2016-02-22T06:56:31-05:00",
			"created": "2016-02-21T13:41:55-05:00"
		}
	]
}