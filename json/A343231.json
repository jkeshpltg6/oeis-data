{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A343231",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 343231,
			"data": "0,1,3,2,3,7,6,7,5,4,5,7,6,7,15,14,15,13,12,13,15,14,15,11,10,11,9,8,9,11,10,11,15,14,15,13,12,13,15,14,15,31,30,31,29,28,29,31,30,31,27,26,27,25,24,25,27,26,27,31,30,31,29,28,29,31,30,31,23",
			"name": "A binary encoding of the nonzero digits in balanced ternary representation of n.",
			"comment": [
				"The ones in the binary representation of a(n) correspond to the nonzero digits in the balanced ternary representation of n.",
				"We can extend this sequence to negative indices: a(-n) = a(n) for any n \u003e= 0."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A343231/b343231.txt\"\u003eTable of n, a(n) for n = 0..6561\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Balanced_ternary\"\u003eBalanced ternary\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A343228(n) + A343229(n)."
			],
			"example": [
				"The first terms, alongside the balanced ternary representation of n (with \"T\" instead of digits \"-1\") and the binary representation of a(n), are:",
				"  n   a(n)  ter(n)  bin(a(n))",
				"  --  ----  ------  ---------",
				"   0     0       0          0",
				"   1     1       1          1",
				"   2     3      1T         11",
				"   3     2      10         10",
				"   4     3      11         11",
				"   5     7     1TT        111",
				"   6     6     1T0        110",
				"   7     7     1T1        111",
				"   8     5     10T        101",
				"   9     4     100        100",
				"  10     5     101        101",
				"  11     7     11T        111",
				"  12     6     110        110",
				"  13     7     111        111",
				"  14    15    1TTT       1111",
				"  15    14    1TT0       1110"
			],
			"program": [
				"(PARI) a(n) = { my (v=0, b=1, t); while (n, t=centerlift(Mod(n, 3)); if (t, v+=b); n=(n-t)\\3; b*=2); v }"
			],
			"xref": [
				"Cf. A059095, A140267, A289831, A343228, A343229, A343230."
			],
			"keyword": "nonn,look,base",
			"offset": "0,3",
			"author": "_Rémy Sigrist_, Apr 08 2021",
			"references": 5,
			"revision": 12,
			"time": "2021-04-09T11:55:23-04:00",
			"created": "2021-04-09T09:31:03-04:00"
		}
	]
}