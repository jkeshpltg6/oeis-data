{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A054445",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 54445,
			"data": "1,2,1,5,3,1,14,9,4,1,42,28,14,5,1,132,90,48,20,6,1,429,297,165,75,27,7,1,1430,1001,572,275,110,35,8,1,4862,3432,2002,1001,429,154,44,9,1,16796,11934,7072,3640,1638,637,208,54,10,1,58786,41990,25194,13260",
			"name": "Triangle read by rows giving partial row sums of triangle A033184(n,m), n \u003e= m \u003e= 1 (Catalan triangle).",
			"comment": [
				"In the language of the Shapiro et al. reference (given in A053121) such a lower triangular (ordinary) convolution array, considered as a matrix, belongs to the Riordan-group. The g.f. for the row polynomials p(n,x) (increasing powers of x) is (c(z)^2)/(1-x*z*c(z)) with c(z) = g.f. A000108 (Catalan numbers).",
				"This coincides with the lower triangular Catalan convolution matrix A033184 with first row and first column deleted: a(n,m)= A033184(n+2,m+2), n \u003e= m \u003e= 0, a(n,m) := 0 if n\u003cm.",
				"The Catalan convolution matrix R(n,m) = A033184(n+1,m+1), n \u003e= m \u003e= 0, is the only Riordan-type matrix with R(0,0)=1 whose partial row sums (prs) matrix satisfies (prs(R))(n,m)= R(n+1,m+1), n \u003e= m \u003e= 0.",
				"Riordan array (c(x)^2,x*c(x)) where c(x)is the g.f. of A000108. - _Philippe Deléham_, Nov 11 2009"
			],
			"formula": [
				"T(n, m) = Sum_{k=m..n} A033184(n+1, k+1), (partial row sums in columns m).",
				"Column m recursion: a(n, m)= sum(a(j-1, m)*A033184(n-j+1, 1), j=m..n) + A033184(n+1, m+1) if n \u003e= m \u003e= 0, a(n, m) := 0 if n\u003cm.",
				"G.f. for column m: (c(x)^2)*(x*c(x))^m, m \u003e= 0, with c(x) = g.f. A000108.",
				"From _Gary W. Adamson_, Jan 19 2012: (Start)",
				"n-th row of the triangle = top row of M^n, where M is the following infinite square production matrix:",
				"  2, 1, 0, 0, 0, ...",
				"  1, 1, 1, 0, 0, ...",
				"  1, 1, 1, 1, 0, ...",
				"  1, 1, 1, 1, 1, ...",
				"  ...",
				"(End)",
				"G.f.: (((2-2*x)*y)/(2*y+x*sqrt(1-4*y)-x)-1)/(x*y). - _Vladimir Kruchinin_, Apr 13 2015",
				"T(n, m) = (m+1) * binomial(2*n - m, n) / (n+1) if n\u003e=m\u003e=1. - _Michael Somos_, Oct 01 2018"
			],
			"example": [
				"Triangle starts:",
				"    1;",
				"    2,  1;",
				"    5,  3,  1;",
				"   14,  9,  4,  1;",
				"   42, 28, 14,  5,  1;",
				"  132, 90, 48, 20,  6,  1;",
				"  ...",
				"Fourth row polynomial (n=3): p(3,x)= 14 + 9*x + 4*x^2 + x^3.",
				"Top row of M^3 = [14, 9, 4, 1, 0, 0, 0, ...]."
			],
			"mathematica": [
				"T[n_, k_] := SeriesCoefficient[((2-2*x)*y)/(2*y+x*Sqrt[1-4*y]-x), {x, 0, n}, {y, 0, k}]; Table[T[n-k+2, k], {n, 0, 10}, {k, n+1, 1, -1}] // Flatten (* _Jean-François Alcover_, Apr 13 2015, after _Vladimir Kruchinin_ *)",
				"T[ n_, k_] := (k + 1) Binomial[2 n - k, n] / (n + 1); (* _Michael Somos_, Oct 01 2018 *)"
			],
			"program": [
				"(PARI)",
				"tabl(nn) = {",
				"  default(seriesprecision, nn+1);",
				"  my( gf = ((2-2*x)*y)/(2*y+x*sqrt(1-4*y)-x) + O(x^nn) );",
				"  for (n=0, nn-1,  my( P = polcoeff(gf, n, x) );",
				"    for (k=0, nn-1, print1(polcoeff(P, k, y), \", \"); );",
				"    print(); );",
				"} \\\\ _Michel Marcus_, Apr 13 2015"
			],
			"xref": [
				"Cf. A033184, A000108. Row sums: a(n+1, 1)."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Apr 27 2000 and May 08 2000",
			"references": 3,
			"revision": 41,
			"time": "2021-06-05T06:29:28-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}