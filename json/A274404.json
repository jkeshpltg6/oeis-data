{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A274404",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 274404,
			"data": "1,1,2,5,1,14,6,42,28,3,132,120,28,1,429,495,180,20,1430,2002,990,195,10,4862,8008,5005,1430,165,4,16796,31824,24024,9009,1650,117,1,58786,125970,111384,51688,13013,1617,70,208012,497420,503880,278460,89180,16016,1386,35",
			"name": "Number T(n,k) of modified skew Dyck paths of semilength n with exactly k anti-down steps; triangle T(n,k), n\u003e=0, 0\u003c=k\u003c=n-floor((1+sqrt(max(0,8n-7)))/2), read by rows.",
			"comment": [
				"A modified skew Dyck path is a path in the first quadrant which begins at the origin, ends on the x-axis, consists of steps U=(1,1) (up), D=(1,-1) (down) and A=(-1,1) (anti-down) so that A and D steps do not overlap."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A274404/b274404.txt\"\u003eRows n = 0..160, flattened\u003c/a\u003e"
			],
			"formula": [
				"Sum_{k\u003e0} k * T(n,k) = A274405(n)."
			],
			"example": [
				"              /\\",
				"              \\ \\",
				"T(3,1) = 1:   /  \\",
				".",
				"Triangle T(n,k) begins:",
				":     1;",
				":     1;",
				":     2;",
				":     5,     1;",
				":    14,     6;",
				":    42,    28,     3;",
				":   132,   120,    28,    1;",
				":   429,   495,   180,   20;",
				":  1430,  2002,   990,  195,   10;",
				":  4862,  8008,  5005, 1430,  165,   4;",
				": 16796, 31824, 24024, 9009, 1650, 117, 1;"
			],
			"maple": [
				"b:= proc(x, y, t, n) option remember; expand(`if`(y\u003en, 0,",
				"      `if`(n=y, `if`(t=2, 0, 1), b(x+1, y+1, 0, n-1)+",
				"      `if`(t\u003c\u003e1 and x\u003e0, b(x-1, y+1, 2, n-1)*z, 0)+",
				"      `if`(t\u003c\u003e2 and y\u003e0, b(x+1, y-1, 1, n-1), 0))))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, z, i), i=0..degree(p)))(b(0$3, 2*n)):",
				"seq(T(n), n=0..14);"
			],
			"mathematica": [
				"b[x_, y_, t_, n_] := b[x, y, t, n] = Expand[If[y \u003e n, 0,",
				"     If[n == y, If[t == 2, 0, 1], b[x + 1, y + 1, 0, n - 1] +",
				"     If[t != 1 \u0026\u0026 x \u003e 0, b[x - 1, y + 1, 2, n - 1] z, 0] +",
				"     If[t != 2 \u0026\u0026 y \u003e 0, b[x + 1, y - 1, 1, n - 1], 0]]]];",
				"T[n_] := CoefficientList[b[0, 0, 0, 2n], z];",
				"T /@ Range[0, 14] // Flatten (* _Jean-François Alcover_, Mar 27 2021, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Columns k=0-3 give: A000108, A002694(n-1), A074922(n-2), A232224(n-3).",
				"Row sums give A230823.",
				"Last elements of rows give A092392(n-1) for n\u003e0.",
				"Cf. A083920, A274405."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_Alois P. Heinz_, Jun 20 2016",
			"references": 5,
			"revision": 15,
			"time": "2021-03-27T08:07:46-04:00",
			"created": "2016-06-20T11:41:34-04:00"
		}
	]
}