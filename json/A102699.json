{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A102699",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 102699,
			"data": "1,1,2,6,16,42,104,252,592,1370,3112,6996,15536,34244,74832,162616,351136,754938,1615208,3443940,7314928,15493676,32714992,68918856,144815456,303703972,635554064,1327816392,2769049312,5766417480,11989472672,24897569648",
			"name": "Number of strings of length n, using as symbols numbers from the set {1, 2, ..., n}, in which consecutive symbols differ by exactly 1.",
			"comment": [
				"Equally, number of different n-digit numbers, using only the digits 1 through n, where consecutive digits differ by 1. It is assumed that there are n different digits available even when n \u003e 9.",
				"Number of endomorphisms of a path P_n. - _N. J. A. Sloane_, Sep 20 2009"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A102699/b102699.txt\"\u003eTable of n, a(n) for n = 0..3311\u003c/a\u003e (terms n = 1..300 from T. D. Noe)",
				"Sr. Arworn, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2007.12.049\"\u003eAn algorithm for the number of endomorphisms on paths\u003c/a\u003e, Disc. Math., 309 (2009), 94-103 (see p. 95).",
				"Zhicong Lin and Jiang Zeng, \u003ca href=\"http://arxiv.org/abs/1112.4026\"\u003eOn the number of congruence classes of paths\u003c/a\u003e, arXiv preprint arXiv:1112.4026 [math.CO], 2011.",
				"M. A. Michels and U. Knauer, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2008.11.022\"\u003eThe congruence classes of paths and cycles\u003c/a\u003e, Discr. Math., 309 (2009), 5352-5359. See p. 5356. [From _N. J. A. Sloane_, Sep 20 2009]",
				"Joseph Myers, \u003ca href=\"http://www.polyomino.org.uk/publications/2008/bmo1-2009-q1.pdf\"\u003eBMO 2008-2009 Round 1 Problem 1-Generalisation\u003c/a\u003e"
			],
			"formula": [
				"It appears that the limit of a(n)/a(n-1) is decreasing towards 2. - _Ben Paul Thurston_, Oct 04 2006",
				"a(n) = (n+1)2^(n-1) - 4(n-1)binomial(n-2,(n-2)/2) for n even, a(n) = (n+1)2^(n-1) - (2n-1)binomial(n-1,(n-1)/2) for n odd. - _Joseph Myers_, Dec 23 2008",
				"a(n) = 2 * Sum_{k=1..n-1} k*A110971(n,k). - _N. J. A. Sloane_, Sep 20 2009",
				"G.f.: x * (2*(1 - x) - sqrt(1 - 4*x^2)) / (1 - 2*x)^2. - _Michael Somos_, Mar 17 2014",
				"0 = a(n) * 8*n^2 - a(n+1) * 4*(n^2 - 2*n - 1) - a(n+2) * 2*(n^2 + 3*n - 2) + a(n+3) * (n-1)*(n+2) for n\u003e0. - _Michael Somos_, Mar 17 2014",
				"0 = a(n) * (16*a(n+1) - 16*a(n+2) + 4*a(n+3)) + a(n+1) * (-16*a(n+1) + 20*a(n+2) - 4*a(n+3)) + a(n+2) * (-4*a(n+2) + a(n+3)) for n\u003e0. - _Michael Somos_, Mar 17 2014"
			],
			"example": [
				"For example, a(4)=16: the 16 strings are 1212, 1232, 1234, 2121, 2123, 2321, 2323, 2343, 3212, 3232, 3234, 3432, 3434, 4321, 4323, 4343.",
				"G.f. = x + 2*x^2 + 6*x^3 + 16*x^4 + 42*x^5 + 104*x^6 + 252*x^7 + 592*x^8 + ..."
			],
			"maple": [
				"p:= 0; paths := proc(m, n, s, t) global p; if(((t+1) \u003c= m) and s \u003c= (n)) then paths(m,n,s+1,t+1); end if; if(((t-1) \u003e 0) and s \u003c= (n)) then paths(m,n,s+1,t-1); end if; if(s = n) then p:=p+1; end if; end proc; sumpaths:=proc(j) global p; p:=0; sp:=0; for h from 1 to j do p:=0; paths(j,j,1,h); sp:=sp+ p ; end do; sp; end proc; for l from 1 to 50 do sumpaths(l); end do; # _Ben Paul Thurston_, Oct 04 2006",
				"# second Maple program:",
				"a:= proc(n) option remember;",
				"      `if`(n\u003c5, [1, 1, 2, 6, 16][n+1], ((2*n^2-6*n-4) *a(n-1)",
				"      +(56-32*n+4*n^2) *a(n-2) -8*(n-3)^2 *a(n-3))/ ((n-1)*(n-4)))",
				"    end:",
				"seq(a(n), n=0..30);  # _Alois P. Heinz_, Nov 23 2012"
			],
			"mathematica": [
				"a[n_] := a[n] = If[n \u003c= 4, n*((n-3)*n+4)/2, ((2*n^2 - 6*n - 4)*a[n-1] + (4*n^2 - 32*n + 56)*a[n-2] - 8*(n-3)^2*a[n-3])/((n-1)*(n-4))]; Table[ a[n], {n, 1, 30}] (* _Jean-François Alcover_, Nov 10 2015, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(PARI) x='x+O('x^55); Vec(x*(2*(1-x)-sqrt(1-4*x^2))/(1-2*x)^2) \\\\ _Altug Alkan_, Nov 10 2015"
			],
			"xref": [
				"Cf. A110971, A152086.",
				"Main diagonal of A220062. - _Alois P. Heinz_, Dec 03 2012"
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "Don Rogers (donrogers42(AT)aol.com), Feb 07 2005",
			"ext": [
				"More terms from _Ben Paul Thurston_, Oct 04 2006",
				"a(20) onwards from _David Wasserman_, Apr 26 2008",
				"Edited by _N. J. A. Sloane_, Jan 03 2009 and Sep 23 2010",
				"a(0)=1 prepended by _Alois P. Heinz_, Apr 17 2017"
			],
			"references": 10,
			"revision": 46,
			"time": "2017-04-17T18:40:12-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}