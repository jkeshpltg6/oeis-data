{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A327119",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 327119,
			"data": "0,1,3,2,7,4,8,6,14,5,15,10,20,12,17,9,34,16,27,18,31,13,29,22,47,19,39,11,48,28,44,30,76,21,51,26,62,36,53,25,69,40,55,42,75,24,65,46,97,35,63,33,94,52,71,43,95,37,87,58,90,60,89,32,167,50,84",
			"name": "Sequence obtained by swapping each (k*(2n))-th element of the nonnegative integers with the (k*(2n+1))-th element, for all k\u003e0 in ascending order, omitting the first term.",
			"comment": [
				"The first term must be omitted because it does not converge.",
				"Start with the sequence of nonnegative integers [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, ...].",
				"Swap all pairs specified by k=1, resulting in [1, 0, 3, 2, 5, 4, 7, 6, 9, 8, 11, ...], so the first term of the final sequence is 0 (No swaps for k\u003e1 will affect this term).",
				"Swap all pairs specified by k=2, resulting in [3, 0, 1, 2, 7, 4, 5, 6, 11, 8, 9, ...], so the second term of the final sequence is 1 (No swaps for k\u003e2 will affect this term).",
				"Swap all pairs specified by k=3, resulting in [2, 0, 1, 3, 7, 4, 8, 6, 11, 5, 9, ...], so the third term of the final sequence is 3 (No swaps for k\u003e3 will affect this term).",
				"Continue for all values of k.",
				"a(n) is equivalent to -A327093(-n), if A327093 is extended to all integers.",
				"It appears that n is an odd prime number iff a(n+1)=n-1. If true, is there a formal analogy with the Sieve of Eratosthenes (by swapping instead of marking terms), or is this another type of sieve? - _Jon Maiga_, May 31 2021"
			],
			"link": [
				"Jennifer Buckley, \u003ca href=\"/A327119/b327119.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A004442(A327420(n)) (conjectured). - _Jon Maiga_, May 31 2021"
			],
			"program": [
				"(golang) func a(n int) int {",
				"    for k := n; k \u003e 0; k-- {",
				"        if n%k == 0 {",
				"            if (n/k)%2 == 0 {",
				"                n = n + k",
				"            } else {",
				"                n = n - k",
				"            }",
				"        }",
				"    }",
				"    return n",
				"}"
			],
			"xref": [
				"Cf. A004442, A327093, A327420.",
				"Inverse: A327120."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Jennifer Buckley_, Sep 13 2019",
			"references": 3,
			"revision": 30,
			"time": "2021-05-31T21:37:04-04:00",
			"created": "2019-09-14T10:38:51-04:00"
		}
	]
}