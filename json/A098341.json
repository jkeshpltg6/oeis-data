{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A098341",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 98341,
			"data": "1,3,1,-45,-255,-477,2689,25203,82945,-90045,-2379519,-11581677,-12063999,197669475,1423716225,3645266355,-12180238335,-156702949245,-626511576575,51239061075,15179398450945,87687927568035,151934475887745",
			"name": "Expansion of 1/sqrt(1 - 6*x + 25*x^2).",
			"comment": [
				"Central coefficients of (1 + 3*x - 4*x^2)^n.",
				"(-1)^n*a(n) is the sum of squares of coefficients of (1+2*i*x)^n where i=sqrt(-1) (see PARI code). - _Joerg Arndt_, Jul 06 2011",
				"Binomial transform of A098337.",
				"Second binomial transform of A098334."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A098341/b098341.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Hacène Belbachir and Abdelghani Mehdaoui, \u003ca href=\"https://doi.org/10.2989/16073606.2020.1729269\"\u003eRecurrence relation associated with the sums of square binomial coefficients\u003c/a\u003e, Quaestiones Mathematicae (2021) Vol. 44, Issue 5, 615-624.",
				"Hacène Belbachir, Abdelghani Mehdaoui, and László Szalay, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL22/Szalay/szalay42.html\"\u003eDiagonal Sums in the Pascal Pyramid, II: Applications\u003c/a\u003e, J. Int. Seq., Vol. 22 (2019), Article 19.3.5.",
				"Tony D. Noe, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL9/Noe/noe35.html\"\u003eOn the Divisibility of Generalized Central Trinomial Coefficients\u003c/a\u003e, Journal of Integer Sequences, Vol. 9 (2006), Article 06.2.7."
			],
			"formula": [
				"E.g.f.: exp(3*x)*BesselI(0, 4*I*x), I=sqrt(-1).",
				"a(n) = (-1)^n*Sum_{k=0..n} binomial(n, k)^2*(-4)^k.",
				"a(n) = (-1)^n*hypergeometric([-n,-n], [1], -4). - _Peter Luschny_, Sep 23 2014",
				"D-finite with recurrence: n*a(n) +3*(-2*n+1)*a(n-1) +25*(n-1)*a(n-2)=0. - _R. J. Mathar_, Nov 27 2014",
				"From _Peter Bala_, Nov 28 2021: (Start)",
				"a(n) = (5^n)*P(n,3/5), where P(n,x) is the n-th Legendre polynomial.",
				"a(n) = [x^n] ((1 - x)*(1 + 4*x))^n.",
				"a(n) = 5^(2*n+1)*Sum_{k \u003e= n} (-1)^(n+k)*binomial(k,n)^2*(1/4)^(k+1).",
				"a(n) = (5/4)*(25/4)^n*hypergeom([n+1, n+1], [1], -1/4). (End)"
			],
			"mathematica": [
				"Table[(-5)^n*LegendreP[n,-3/5],{n,0,20}] (* _Vaclav Kotesovec_, Jul 23 2013 *)",
				"CoefficientList[Series[1/Sqrt[1-6x+25x^2],{x,0,30}],x] (* _Harvey P. Dale_, Aug 22 2014 *)"
			],
			"program": [
				"(PARI) a(n)={local(v=Vec((1+2*I*x)^n)); (-1)^n*sum(k=1,#v,v[k]^2);} /* Joerg Arndt, Jul 06 2011 */",
				"(PARI) a(n)={local(v=Vec((1+2*I*x)^n)); sum(k=1,#v, real(v[k])^2-imag(v[k])^2);} /* Joerg Arndt, Jul 06 2011 */",
				"(Sage)",
				"A098341 = lambda n: (-1)^n*hypergeometric([-n,-n], [1], -4)",
				"[Integer(A098341(n).n(100)) for n in (0..22)] # _Peter Luschny_, Sep 23 2014"
			],
			"xref": [
				"Cf. A098332, A012000, A116091, A126869."
			],
			"keyword": "easy,sign",
			"offset": "0,2",
			"author": "_Paul Barry_, Sep 03 2004",
			"references": 3,
			"revision": 41,
			"time": "2021-12-04T12:35:19-05:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}