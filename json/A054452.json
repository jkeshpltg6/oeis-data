{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A054452",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 54452,
			"data": "0,0,1,5,17,50,138,370,979,2575,6755,17700,46356,121380,317797,832025,2178293,5702870,14930334,39088150,102334135,267914275,701408711,1836311880,4807526952,12586269000,32951280073,86267571245,225851433689,591286729850",
			"name": "Partial sums of A027941(n-1) with a(-1) = 0.",
			"link": [
				"Colin Barker, \u003ca href=\"/A054452/b054452.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"László Németh, \u003ca href=\"https://arxiv.org/abs/1701.06022\"\u003ePascal pyramid in the space H^2 x R\u003c/a\u003e, arXiv:1701.06022 [math.CO], 2017 (5th line of Table 1 is a(n-2)).",
				"A. Shriki and O. Liba, \u003ca href=\"http://www.fq.math.ca/Problems/ElemProbSolnMay16No53.pdf\"\u003ePolygons with Fibonacci Number Coordinates: Problem B-1167\u003c/a\u003e, Fib. Quart. 54,2 May 2016, p. 180-181.",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (5,-8,5,-1)."
			],
			"formula": [
				"a(n) = +5*a(n-1) -8*a(n-2) +5*a(n-3) -1*a(n-4).",
				"G.f.: x^2/((1-x)^2*(1-3*x+x^2)).",
				"a(n) = Sum_{k=0..n} A027941(k-1) = F(2*n)-n = A054450(2*n-1, 2) = A054451(2*n-3).",
				"G.f.: x^2*Fibe(x)/(1-x)^2, with Fibe(x) := 1/(1-3*x+x^2) = g.f. A001906(n+1) (Fibonacci numbers F(2(n+1))).",
				"Fourth diagonal of array defined by T(i, 1) = T(1, j) = 1, T(i, j) = Max(T(i-1, j) + T(i-1, j-1); T(i-1, j-1) + T(i, j-1)). - _Benoit Cloitre_, Aug 05 2003",
				"a(n) = Sum_{k=0..n-2} binomial(2*n-k-1, k). - _Johannes W. Meijer_, Aug 12 2013",
				"a(n) = Sum_{i=1..n-1} Sum_{j=1..n-1} binomial(i+j, i-j). - _Wesley Ivan Hurt_, Mar 25 2015",
				"a(n) = Sum_{k=0..n} (binomial(n+1,k+2)*Fibonacci(k)). - _Vladimir Kruchinin_, Oct 21 2016",
				"a(n) = (-((3-sqrt(5))/2)^n + ((3+sqrt(5))/2)^n)/sqrt(5) - n. - _Colin Barker_, Jan 28 2017"
			],
			"maple": [
				"a[0]:=0: a[1]:=1: for n from 2 to 50 do a[n] := 3*a[n-1]-a[n-2] od: seq(a[n]-n, n=0..27); # _Zerinvary Lajos_, Mar 20 2008",
				"with(combinat): seq(fibonacci(2*n)-n, n=0..27); # _Zerinvary Lajos_, Jun 19 2008",
				"g:=z/(1-3*z+z^2): gser:=series(g, z=0, 43): seq(abs(coeff(gser, z, n)-n), n=0..27); # _Zerinvary Lajos_, Mar 22 2009"
			],
			"mathematica": [
				"CoefficientList[Series[x^2 / ((1 - x)^2 (1 - 3 x + x^2)), {x, 0, 33}], x] (* _Vincenzo Librandi_, Mar 26 2015 *)"
			],
			"program": [
				"(Sage) [(lucas_number1(n, 3, 1)-lucas_number1(n, 2, 1)) for n in range(1, 28)]# _Zerinvary Lajos_, Mar 13 2009",
				"(MAGMA) I:=[0,0,1,5]; [n le 4 select I[n] else 5*Self(n-1)-8*Self(n-2)+5*Self(n-3)-Self(n-4): n in [1..30]]; // _Vincenzo Librandi_, Mar 26 2015",
				"(Maxima)",
				"makelist(sum(fib(k)*binomial(n+1,k+2),k,0,n),n,0,20); /* _Vladimir Kruchinin_, Oct 21 2016 */",
				"(PARI) concat(vector(2), Vec(x^2/((1-x)^2*(1-3*x+x^2)) + O(x^40))) \\\\ _Colin Barker_, Jan 28 2017"
			],
			"xref": [
				"Cf. A027941, A054451, A001906, A052952."
			],
			"keyword": "easy,nonn",
			"offset": "0,4",
			"author": "_Wolfdieter Lang_, Apr 27 2000",
			"ext": [
				"More terms from _James A. Sellers_, Apr 28 2000",
				"a(0) added by _Arkadiusz Wesolowski_, Jun 07 2011"
			],
			"references": 7,
			"revision": 51,
			"time": "2019-12-07T12:18:22-05:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}