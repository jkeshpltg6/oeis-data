{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A334719",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 334719,
			"data": "0,4,30,250,2245,21221,208129,2098565,21619910,226593015,2408424760,25899375645,281273231985,3080585212120,33986840371400,377364606387005,4213620859310140,47284625533425750,532996618440511710,6032169040263819485,68517222947120776290",
			"name": "a(n) is the total number of down-steps after the final up-step in all 4-Dyck paths of length 5*n (n up-steps and 4*n down-steps).",
			"comment": [
				"A 4-Dyck path is a lattice path with steps U = (1, 4), d = (1, -1) that starts at (0,0), stays (weakly) above the x-axis, and ends at the x-axis."
			],
			"link": [
				"Andrei Asinowski, Benjamin Hackl, Sarah J. Selkirk, \u003ca href=\"https://arxiv.org/abs/2007.15562\"\u003eDown-step statistics in generalized Dyck paths\u003c/a\u003e, arXiv:2007.15562 [math.CO], 2020."
			],
			"formula": [
				"a(n) = binomial(5*(n+1)+1, n+1)/(5*(n+1)+1) - binomial(5*n+1, n)/(5*n+1).",
				"a(n) = A062985(n+1, 4*n-1)."
			],
			"example": [
				"For n = 2, the a(2) = 30 is the total number of down-steps after the last up-step in UddddUdddd, UdddUddddd, UddUdddddd, UdUddddddd, UUdddddddd (thus, 4 + 5 + 6 + 7 + 8)."
			],
			"maple": [
				"b:= proc(x, y) option remember; `if`(x=y, x,",
				"     `if`(y+4\u003cx, b(x-1, y+4), 0)+`if`(y\u003e0, b(x-1, y-1), 0))",
				"    end:",
				"a:= n-\u003e b(5*n, 0):",
				"seq(a(n), n=0..20);  # _Alois P. Heinz_, May 09 2020",
				"# second Maple program:",
				"a:= proc(n) option remember; `if`(n\u003c2, 4*n, (5*(5*n-4)*",
				"      (5*n-3)*(5*n-2)*(5*n-1)*n*(2869*n^3+5354*n^2+3239*n+634)*",
				"       a(n-1))/(8*(n-1)*(4*n+3)*(2*n+1)*(4*n+5)*(n+1)*",
				"       (2869*n^3-3253*n^2+1138*n-120)))",
				"    end:",
				"seq(a(n), n=0..20);  # _Alois P. Heinz_, May 09 2020"
			],
			"mathematica": [
				"a[n_] := Binomial[5*n + 6, n + 1]/(5*n + 6) - Binomial[5*n + 1, n]/(5*n + 1); Array[a, 21, 0] (* _Amiram Eldar_, May 13 2020 *)"
			],
			"program": [
				"(PARI) a(n) = {binomial(5*(n+1)+1, n+1)/(5*(n+1)+1) - binomial(5*n+1, n)/(5*n+1)} \\\\ _Andrew Howroyd_, May 08 2020",
				"(SageMath) [binomial(5*(n + 1) + 1, n + 1)/(5*(n + 1) + 1) - binomial(5*n + 1, n)/(5*n + 1) for n in srange(30)] # _Benjamin Hackl_, May 13 2020"
			],
			"xref": [
				"First order differences of A002294. Cf. A062985.",
				"Cf. A334682 (similar for 3-Dyck paths)."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Andrei Asinowski_, May 08 2020",
			"references": 4,
			"revision": 25,
			"time": "2020-08-08T01:39:32-04:00",
			"created": "2020-05-14T05:12:48-04:00"
		}
	]
}