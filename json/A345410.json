{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A345410",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 345410,
			"data": "44,1090,10450,5104,88888,10780,289982,299992,482174,478874,868868,499994,1073270,1087790,1071070,1069970,10904990,10794980,1091090,10892990,1100000,29955992,1101100,26688662,31022002,27599572,46400354,44688644,29821792,45289244,30122092,26988962",
			"name": "a(n) is the least number that is the sum of an emirp and its reversal in exactly n ways.",
			"comment": [
				"Interchanging an emirp and its reversal is not counted as a different way.",
				"a(n) is the least number k such there are exactly n unordered pairs of distinct primes (p,p') such that p' is the digit reversal of p and p+p' = k.",
				"Are terms not divisible by 3? _Amiram Eldar_ finds proof they are; A056964(n) = n + reverse(n) is divisible by 3 if and only if n is divisible by 3. But emirps are primes (other than 3) so they are not divisible by 3. - _David A. Corneth_, Jun 19 2021"
			],
			"link": [
				"David A. Corneth, \u003ca href=\"/A345410/b345410.txt\"\u003eTable of n, a(n) for n = 1..423\u003c/a\u003e",
				"David A. Corneth, \u003ca href=\"/A345410/a345410.gp.txt\"\u003eA few examples\u003c/a\u003e"
			],
			"example": [
				"a(3) = 10450 because 10450 = 1229+9221 = 1409+9041 = 3407+7043."
			],
			"maple": [
				"revdigs:= proc(n) local L,i; L:= convert(n,base,10); add(L[-i]*10^(i-1),i=1..nops(L)) end proc:",
				"isemirp1:= proc(n) local r;",
				"if not isprime(n) then return false fi;",
				"r:= revdigs(n);",
				"r \u003e n and isprime(r)",
				"end proc:",
				"E:= select(isemirp1, [seq(seq(seq(i*10^d+j,j=1..10^d-1,2),i=[1,3,7,9]),d=1..5)]):",
				"V:= sort(map(t -\u003e t+revdigs(t),E)):",
				"N:= nops(V):",
				"W:= Vector(16):",
				"i:= 1:",
				"while i \u003c N do",
				"for j from 1 to N-i while V[i+j]=V[i] do od:",
				"if j \u003c= 16 and W[j] = 0 then W[j]:= V[i] fi;",
				"  i:= i+j;",
				"od:",
				"convert(W,list);"
			],
			"program": [
				"(Python)",
				"from itertools import product",
				"from collections import Counter",
				"from sympy import isprime, nextprime",
				"def epgen(start=1, end=float('inf')): # generates unique emirp/prime pairs",
				"    digits = 2",
				"    while True:",
				"      for first in \"1379\":",
				"        for last in \"1379\":",
				"          if last \u003c first: continue",
				"          for mid in product(\"0123456789\", repeat=digits-2):",
				"            strp = first + \"\".join(mid) + last",
				"            revstrp = strp[::-1]",
				"            if strp \u003e= revstrp: continue",
				"            p = int(strp)",
				"            if p \u003e end: return",
				"            revp = int(strp[::-1])",
				"            if isprime(p) and isprime(revp): yield (p, revp)",
				"      digits += 1",
				"def aupto(lim):",
				"    alst = []",
				"    c = Counter(sum(ep) for ep in epgen(1, lim) if sum(ep) \u003c= lim)",
				"    r = set(c.values())",
				"    for i in range(1, max(r)+1):",
				"        if i in r: alst.append(min(s for s in c if c[s] == i))",
				"        else: break",
				"    return alst",
				"print(aupto(11*10**5)) # _Michael S. Branicky_, Jun 19 2021"
			],
			"xref": [
				"Cf. A006567, A345408, A345409."
			],
			"keyword": "nonn,base",
			"offset": "1,1",
			"author": "_J. M. Bergot_ and _Robert Israel_, Jun 18 2021",
			"ext": [
				"More terms from _David A. Corneth_, Jun 18 2021"
			],
			"references": 1,
			"revision": 21,
			"time": "2021-06-21T11:57:55-04:00",
			"created": "2021-06-18T14:49:46-04:00"
		}
	]
}