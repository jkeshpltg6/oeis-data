{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A010900",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 10900,
			"data": "4,13,42,136,440,1424,4609,14918,48285,156284,505844,1637264,5299328,17152321,55516872,179691313,581606398,1882483892,6093030640,19721296176,63831867233,206604436042,668716032329,2164431415224,7005609443657,22675037578854",
			"name": "Pisot sequence E(4,13): a(n) = floor( a(n-1)^2/a(n-2) + 1/2 ).",
			"comment": [
				"According to David Boyd his last use (as of April, 2006) of his Pisot number finding program was to prove that in fact this sequence does not satisfy a linear recurrence. He remarks \"This took a couple of years in background on various Sun workstations.\" - _Gene Ward Smith_, Apr 11 2006",
				"Satisfies a linear recurrence of order 6 just for n \u003c= 23 (see A274952). - _N. J. A. Sloane_, Aug 07 2016"
			],
			"reference": [
				"Cantor, D. G. \"Investigation of T-numbers and E-sequences.\" In Computers in Number Theory, ed. AOL Atkin and BJ Birch, Acad. Press, NY (1971); pp. 137-140.",
				"Cantor, D. G. (1976). On families of Pisot E-sequences. In Annales scientifiques de l'École Normale Supérieure (Vol. 9, No. 2, pp. 283-308)."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A010900/b010900.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"D. W. Boyd, \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/aa/aa34/aa3444.pdf\"\u003eSome integer sequences related to the Pisot sequences\u003c/a\u003e, Acta Arithmetica, 34 (1979), 295-305",
				"D. W. Boyd, \u003ca href=\"https://www.researchgate.net/profile/David_Boyd7/publication/262181133_Linear_recurrence_relations_for_some_generalized_Pisot_sequences_-_annotated_with_corrections_and_additions/links/00b7d536d49781037f000000.pdf\"\u003eLinear recurrence relations for some generalized Pisot sequences\u003c/a\u003e, Advances in Number Theory ( Kingston ON, 1991) 333-340, Oxford Sci. Publ., Oxford Univ. Press, New York, 1993.",
				"David Cantor, \u003ca href=\"/A010900/a010900.pdf\"\u003eInvestigation of T-numbers and E-sequences\u003c/a\u003e, In Computers in Number Theory, ed. A. O. L. Atkin and B. J. Birch, Acad. Press, NY (1971); pp. 137-140. [Annotated scanned copy]",
				"C. Pisot, \u003ca href=\"http://www.numdam.org/item?id=ASNSP_1938_2_7_3-4_205_0\"\u003eLa répartition modulo 1 et les nombres algébriques\u003c/a\u003e, Ann. Scuola Norm. Sup. Pisa, 7 (1938), 205-248."
			],
			"formula": [
				"It is known that this does not satisfy any linear recurrence [Boyd]."
			],
			"mathematica": [
				"nxt[{a_,b_}]:={b,Floor[b^2/a+1/2]}; NestList[nxt,{4,13},30][[All,1]] (* _Harvey P. Dale_, Jun 24 2018 *)"
			],
			"program": [
				"(PARI) pisotE(nmax, a1, a2) = {",
				"  a=vector(nmax); a[1]=a1; a[2]=a2;",
				"  for(n=3, nmax, a[n] = floor(a[n-1]^2/a[n-2]+1/2));",
				"  a",
				"}",
				"pisotE(50, 4, 13) \\\\ _Colin Barker_, Jul 28 2016"
			],
			"xref": [
				"Cf. A007698, A007699, A010916, A274952.",
				"See A008776 for definitions of Pisot sequences."
			],
			"keyword": "nonn",
			"offset": "0,1",
			"author": "_Simon Plouffe_",
			"references": 3,
			"revision": 46,
			"time": "2018-06-24T12:24:51-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}