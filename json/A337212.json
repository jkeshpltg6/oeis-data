{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A337212",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 337212,
			"data": "1,8,13,26,104,728,364,80,91,8744,3851,3280,59048,4782968,7174453,3438578,16139240,5373368,5235412,1678822106,86049704,387420488,47071589413,140633637386,2952400,728,757,9526168288,7312949144072,49566102697280,24477226494760",
			"name": "Modulo 3 Pisano period of 'n-bonacci' series.",
			"comment": [
				"The modulo 2 variant of this sequence gives 1, 3, 4, 5, 6, 7, 8, ... (the natural numbers not including 2), and likewise, when the modulus is a power of 2, it seems that the Pisano period lengths form an arithmetic progression. (Note that both of these observations are based on empirical observation only).",
				"a(39)=797161, a(80)=6560, a(81)=6643, a(90)=5380840, a(242)=59048, a(243)=59293, a(728)=531440, a(729)= 532171, a(2186)=4782968, a(2187)=4785157, a(6560)=43046720, a(6561)=43053283, a(19682)=387420488, a(19683)=387440173. - _Chai Wah Wu_, Sep 15 2020"
			],
			"formula": [
				"Conjecture: a(3^k-1)=a(3^k)-3^k-2=3^(2k)-1, a(3^k)=3^k(3^k+1)+1 for k\u003e0. - _Chai Wah Wu_, Sep 15 2020"
			],
			"example": [
				"For n = 3, the remainders modulo 3 of the tribonacci series are 0, 1, 1, 2, 1, 1, 1, 0, 2, 0, 2, 1, 0, (these repeat indefinitely), so the Pisano period of the 'tribonacci' sequence is 13."
			],
			"program": [
				"(Python)",
				"m = 3",
				"def FindRec(a):",
				"    F = []",
				"    F += [0]",
				"    for i in range(a-1):",
				"        F += [1]",
				"    Match = F[0:a]",
				"    Sum = F",
				"    while True:",
				"        n = len(F)",
				"        Next = 0",
				"        for i in Sum:",
				"            Next += i",
				"        F += [Next%m]",
				"        Sum = []",
				"        for i in range(a):",
				"            Sum += [F[n-a+i+1]]",
				"        if Match == Sum:",
				"            return n - a + 1",
				"aLim = 15 # number of terms in the series #",
				"Recs = []",
				"for i in range(1,aLim+1):",
				"    Recs += [FindRec(i)]",
				"for j in range(len(Recs)):",
				"    print(Recs[j])",
				"(PARI) a(n) = {my(v=w=concat(0, vector(n-1, i, 1))); for(k=1, oo, v=concat(v[2..n], vecsum(v)%3); if(v==w, return(k))); } \\\\ _Jinyuan Wang_, Aug 20 2020",
				"(Python)",
				"def A337212(n):",
				"    x, y, k, r, m = (3**n-3)//2, (3**n-3)//2, (n-1)%3, 3**(n-1), 0",
				"    while True:",
				"        m += 1",
				"        a, b = divmod(x,3)",
				"        x, k = a+k*r, (k+k-b)%3",
				"        if y == x:",
				"            return m # _Chai Wah Wu_, Sep 14 2020"
			],
			"xref": [
				"Cf. A001175 (period of Fibonacci numbers mod n)."
			],
			"keyword": "nonn,more",
			"offset": "1,2",
			"author": "_Adam Bascal_, Aug 19 2020",
			"ext": [
				"a(20)-a(22) from _Jinyuan Wang_, Aug 20 2020",
				"a(23) from _Chai Wah Wu_, Sep 14 2020",
				"a(24)-a(28) from _Chai Wah Wu_, Sep 15 2020",
				"a(29)-a(31) from _Chai Wah Wu_, Sep 21 2020"
			],
			"references": 1,
			"revision": 38,
			"time": "2020-09-22T00:28:00-04:00",
			"created": "2020-09-13T16:46:28-04:00"
		}
	]
}