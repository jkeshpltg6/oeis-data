{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A090390",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 90390,
			"data": "1,1,9,49,289,1681,9801,57121,332929,1940449,11309769,65918161,384199201,2239277041,13051463049,76069501249,443365544449,2584123765441,15061377048201,87784138523761,511643454094369,2982076586042449,17380816062160329,101302819786919521,590436102659356801",
			"name": "Repeatedly multiply (1,0,0) by ([1,2,2],[2,1,2],[2,2,3]); sequence gives leading entry.",
			"comment": [
				"The values of a and b in (a,b,c)*A give all (positive integer) solutions to Pell equation a^2 - 2*b^2 = -1; the values of c are A000129(2n)",
				"Binomial transform of A086348. - _Johannes W. Meijer_, Aug 01 2010",
				"All values of a(n) are squares.  sqrt(a(n+1)) = A001333(n). The ratio a(n+1)/a(n) converges to 3 + 2*sqrt(2). - _Richard R. Forberg_, Aug 14 2013"
			],
			"link": [
				"Harvey P. Dale, \u003ca href=\"/A090390/b090390.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Robert Munafo, \u003ca href=\"http://www.mrob.com/pub/math/seq-floretion.html\"\u003eSequences Related to Floretions\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (5, 5, -1)."
			],
			"formula": [
				"G.f.: (1-4*x-x^2)/((1+x)*(1-6*x+x^2)).",
				"a(n) = A001333(n)^2",
				"(a, b, c) = (1, 0, 0). Recursively multiply (a, b, c)*( [1, 2, 2], [2, 1, 2], [2, 2, 3] ).",
				"M^n * [ 1 1 1] = [a(n+1) q a(n)], where M = the 3 X 3 matrix [4 4 1 / 2 1 0 / 1 0 0]. E.g. M^5 * [1 1 1] = [9801 4059 1681] where 9801 = a(6), 1681 = a(5). Similarly, M^n * [1 0 0] generates A079291 (Pell number squares). - _Gary W. Adamson_, Oct 31 2004",
				"(((1+sqrt(2))^(2*n)+(1-sqrt(2))^(2*n))+2*(-1)^n)/4 - Lambert Klasen (lambert.klasen(AT)gmx.net), Oct 09 2005",
				"a(n) = (A001541(n)+(-1)^n)/2. - _R. J. Mathar_, Nov 20 2009",
				"a(0)=1, a(1)=1, a(2)=9, a(n)=5*a(n-1)+5*a(n-2)-a(n-3) - _Harvey P. Dale_, May 20 2012",
				"(a(n)) = tesseq(- .5'j + .5'k - .5j' + .5k' - 2'ii' + 'jj' - 'kk' + .5'ij' + .5'ik' + .5'ji' + 'jk' + .5'ki' + 'kj' + e), apart from initial term. - _Creighton Dement_, Nov 16 2004",
				"a(n) = A302946(n)/4. - _Eric W. Weisstein_, Apr 17 2018"
			],
			"maple": [
				"a:= n-\u003e (\u003c\u003c1|0|0\u003e\u003e. \u003c\u003c1|2|2\u003e, \u003c2|1|2\u003e, \u003c2|2|3\u003e\u003e^n)[1, 1]:",
				"seq(a(n), n=0..30);  # _Alois P. Heinz_, Aug 17 2013"
			],
			"mathematica": [
				"CoefficientList[Series[(1 - 4 x - x^2)/((1 + x)(1 - 6 x + x^2)),{x, 0, 20}], x] (* _Harvey P. Dale_, May 20 2012 *)",
				"LinearRecurrence[{5, 5, -1}, {1, 1, 9}, 20] (* _Harvey P. Dale_, May 20 2012 *)",
				"Table[(ChebyshevT[n, 3] + (-1)^n)/2, {n, 0, 20}] (* _Eric W. Weisstein_, Apr 17 2018 *)"
			],
			"program": [
				"(Perl) use Math::Matrix; use Math::BigInt; $a = new Math::Matrix ([ 1, 2, 2], [ 2, 1, 2], [ 2, 2, 3]); $p = new Math::Matrix ([1, 0, 0]); $p-\u003eprint(); for ($i=1; $i\u003c20;$i++) { $p = $p-\u003emultiply($a); $p-\u003eprint(); }",
				"(PARI) a(n)=polcoeff((1-4*x-x^2)/((1+x)*(1-6*x+x^2))+x*O(x^n),n)",
				"(PARI) a(n)=if(n\u003c0,0,([1,2,2;2,1,2;2,2,3]^n)[1,1])",
				"(PARI) Vec( (1-4*x-x^2)/((1+x)*(1-6*x+x^2)) + O(x^66) ) \\\\ _Joerg Arndt_, Aug 16 2013",
				"(Haskell)",
				"a090390 n = a090390_list !! n",
				"a090390_list = 1 : 1 : 9 : zipWith (-) (map (* 5) $",
				"   tail $ zipWith (+) (tail a090390_list) a090390_list) a090390_list",
				"-- _Reinhard Zumkeller_, Aug 17 2013"
			],
			"xref": [
				"Cf. A000129.",
				"Cf. A079291.",
				"Cf. A095344, A123270."
			],
			"keyword": "easy,nonn",
			"offset": "0,3",
			"author": "_Vim Wenders_, Jan 30 2004",
			"references": 17,
			"revision": 36,
			"time": "2018-04-17T09:37:31-04:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}