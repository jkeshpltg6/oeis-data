{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A212529",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 212529,
			"data": "11,10,1101,1100,1111,1110,1001,1000,1011,1010,110101,110100,110111,110110,110001,110000,110011,110010,111101,111100,111111,111110,111001,111000,111011,111010,100101,100100,100111,100110,100001,100000,100011,100010,101101,101100,101111,101110,101001,101000,101011,101010,11010101",
			"name": "Negative numbers in base -2.",
			"comment": [
				"The formula a(n) = A039724(-n) is slightly misleading because sequence A039724 isn't defined for n \u003c 0, and none of the terms a(n) is a term of A039724. It can be seen as the definition of the extension of A039724 to negative indices. Also, recursive definitions or implementations of A039724 require that function to be defined for negative arguments, and using a generic formula it will work as expected for -n, n \u003e 0. - _M. F. Hasler_, Oct 18 2018"
			],
			"link": [
				"Joerg Arndt, \u003ca href=\"/A212529/b212529.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Joerg Arndt, \u003ca href=\"http://www.jjj.de/fxt/#fxtbook\"\u003eMatters Computational (The Fxtbook)\u003c/a\u003e, p. 58-59"
			],
			"formula": [
				"a(n) = A039724(-n). - _Reinhard Zumkeller_, Feb 05 2014"
			],
			"maple": [
				"a:= proc(n) local d, i, l, m;",
				"      m:= n; l:= NULL;",
				"      for i from 0 while m\u003e0 do",
				"        d:= irem(m, 2, 'm');",
				"        if d=1 and irem(i, 2)=0 then m:= m+1 fi;",
				"        l:= d, l",
				"      od; parse(cat(l))",
				"    end:",
				"seq(a(n), n=1..100);  # _Alois P. Heinz_, May 20 2012"
			],
			"program": [
				"(Haskell)",
				"a212529 = a039724 . negate  -- _Reinhard Zumkeller_, Feb 05 2014",
				"(Python)",
				"def A212529(n):",
				"    s, q = '', -n",
				"    while q \u003e= 2 or q \u003c 0:",
				"        q, r = divmod(q, -2)",
				"        if r \u003c 0:",
				"            q += 1",
				"            r += 2",
				"        s += str(r)",
				"    return int(str(q)+s[::-1]) # _Chai Wah Wu_, Apr 09 2016",
				"(PARI) A212529(n)=A039724(-n) \\\\ _M. F. Hasler_, Oct 16 2018"
			],
			"xref": [
				"Cf. A039724 (nonnegative numbers in base -2).",
				"Cf. A007608 (nonnegative numbers in base -4), A212526 (negative numbers in base -4).",
				"Cf. A005352."
			],
			"keyword": "nonn,base",
			"offset": "1,1",
			"author": "_Joerg Arndt_, May 20 2012",
			"references": 10,
			"revision": 29,
			"time": "2018-10-19T08:31:05-04:00",
			"created": "2012-05-20T09:32:42-04:00"
		}
	]
}