{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006873",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6873,
			"id": "M4430",
			"data": "1,1,7,47,497,6241,95767,1704527,34741217,796079041,20273087527,567864586607,17352768515537,574448847467041,20479521468959287,782259922208550287,31872138933891307457,1379749466246228538241,63243057486503656319047,3059895336952604166395567",
			"name": "Number of alternating 4-signed permutations.",
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A006873/b006873.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"R. Ehrenborg and M. A. Readdy, \u003ca href=\"/A006873/a006873.pdf\"\u003eSheffer posets and r-signed permutations\u003c/a\u003e, Preprint, 1994. (Annotated scanned copy)",
				"Richard Ehrenborg and Margaret A. Readdy, \u003ca href=\"http://www.labmath.uqam.ca/~annales/volumes/19-2/PDF/173-196.pdf\"\u003eSheffer posets and r-signed permutations\u003c/a\u003e, Annales des Sciences Mathématiques du Québec, 19 (1995), 173-196."
			],
			"formula": [
				"E.g.f.: (sin(x) + cos(3*x)) / cos(4*x). - _M. F. Hasler_, Apr 28 2013",
				"a(n) = Re(2*((1-I)/(1+I))^n*(1 + Sum_{j=0..n}(binomial(n,j)*Li_{-j}(I)* 4^j))). - _Peter Luschny_, Apr 29 2013",
				"a(n) ~ sqrt(2-sqrt(2)) * 2^(3*n+3/2) * n^(n+1/2) / (Pi^(n+1/2) * exp(n)). - _Vaclav Kotesovec_, Feb 25 2014",
				"a(n) ~ GAMMA(n)*8^n/(Pi^n*(2*sqrt(4+2*sqrt(2)))). - _Simon Plouffe_, Nov 29 2018"
			],
			"maple": [
				"per4 := proc(n) local j; 2*((1-I)/(1+I))^n*(1+add(binomial(n,j)* polylog(-j,I)*4^j, j=0..n)) end: A006873 := n -\u003e Re(per4(n));",
				"seq(A006873(i), i=0..11); # _Peter Luschny_, Apr 29 2013"
			],
			"mathematica": [
				"mx = 17; Range[0, mx]! CoefficientList[ Series[ (Sin[x] + Cos[3x])/Cos[4x], {x, 0, mx}], x] (* _Robert G. Wilson v_, Apr 28 2013 *)"
			],
			"program": [
				"(PARI) x='x+O('x^66); Vec(serlaplace((sin(x)+cos(3*x))/cos(4*x))) \\\\ _Joerg Arndt_, Apr 28 2013",
				"(MAGMA) m:=50; R\u003cx\u003e:=PowerSeriesRing(Rationals(), m); b:=Coefficients(R!(Sin(x)+Cos(3*x))/Cos(4*x))); [Factorial(n-1)*b[n]: n in [1..m-1]]; // _G. C. Greubel_, Nov 29 2018",
				"(Sage)",
				"f=(sin(x) + cos(3*x))/cos(4*x)",
				"g=f.taylor(x,0,50)",
				"L=g.coefficients()",
				"coeffs={c[1]:c[0]*factorial(c[1]) for c in L}",
				"coeffs # _G. C. Greubel_, Nov 29 2018"
			],
			"xref": [
				"Cf. A007286, A007289, A225109."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_ and _Simon Plouffe_",
			"ext": [
				"Added more terms, _Joerg Arndt_, Apr 28 2013"
			],
			"references": 5,
			"revision": 52,
			"time": "2020-07-28T10:29:13-04:00",
			"created": "1994-04-28T03:00:00-04:00"
		}
	]
}