{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008836",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8836,
			"data": "1,-1,-1,1,-1,1,-1,-1,1,1,-1,-1,-1,1,1,1,-1,-1,-1,-1,1,1,-1,1,1,1,-1,-1,-1,-1,-1,-1,1,1,1,1,-1,1,1,1,-1,-1,-1,-1,-1,1,-1,-1,1,-1,1,-1,-1,1,1,1,1,1,-1,1,-1,1,-1,1,1,-1,-1,-1,1,-1,-1,-1,-1,1,-1,-1,1,-1,-1,-1,1,1,-1,1,1,1,1,1,-1,1,1,-1,1,1,1,1,-1,-1,-1,1,-1",
			"name": "Liouville's function lambda(n) = (-1)^k, where k is number of primes dividing n (counted with multiplicity).",
			"comment": [
				"Coons and Borwein: \"We give a new proof of Fatou's theorem: if an algebraic function has a power series expansion with bounded integer coefficients, then it must be a rational function.} This result is applied to show that for any non-trivial completely multiplicative function from N to {-1,1), the series sum_{n=1 to infinity) f(n)z^n is transcendental over {Z}[z]; in particular, sum_{n=1 to infinity) lambda(n)z^n is transcendental, where lambda is Liouville's function. The transcendence of sum_{n=1 to infinity) mu(n)z^n is also proved.\" - _Jonathan Vos Post_, Jun 11 2008",
				"Coons proves that a(n) is not k-automatic for any k \u003e 2. - _Jonathan Vos Post_, Oct 22 2008",
				"The Riemann hypothesis is equivalent to the statement that for every fixed epsilon \u003e 0, lim_{n -\u003e infinity} (a(1) + a(2) + ... + a(n))/n^(1/2 + epsilon) = 0 (Borwein et al., theorem 1.2). - _Arkadiusz Wesolowski_, Oct 08 2013"
			],
			"reference": [
				"T. M. Apostol, Introduction to Analytic Number Theory, Springer-Verlag, 1976, page 37.",
				"P. Borwein, S. Choi, B. Rooney and A. Weirathmueller, The Riemann Hypothesis: A Resource for the Aficionado and Virtuoso Alike, Springer, Berlin, 2008, pp. 1-11.",
				"H. Gupta, On a table of values of L(n), Proceedings of the Indian Academy of Sciences. Section A, 12 (1940), 407-409.",
				"H. Gupta, A table of values of Liouville's function L(n), Research Bulletin of East Panjab University, No. 3 (Feb. 1950), 45-55.",
				"P. Ribenboim, Algebraic Numbers, p. 44.",
				"J. Roberts, Lure of the Integers, Math. Assoc. America, 1992, p. 279.",
				"J. V. Uspensky and M. A. Heaslet, Elementary Number Theory, McGraw-Hill, NY, 1939, p. 112."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A008836/b008836.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"P. Borwein, R. Ferguson, and M. J. Mossinghoff, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-08-02036-X\"\u003eSign changes in sums of the Liouville function\u003c/a\u003e, Math. Comp. 77 (2008), 1681-1694.",
				"Benoit Cloitre, \u003ca href=\"http://arxiv.org/abs/1107.0812\"\u003eA tauberian approach to RH\u003c/a\u003e, arXiv:1107.0812 [math.NT], 2011.",
				"Michael Coons and Peter Borwein, \u003ca href=\"http://arxiv.org/abs/0806.1563\"\u003eTranscendence of Power Series for Some Number Theoretic Functions\u003c/a\u003e, arXiv:0806.1563 [math.NT], 2008.",
				"Michael Coons, \u003ca href=\"http://arxiv.org/abs/0810.3709\"\u003e(Non)Automaticity of number theoretic functions\u003c/a\u003e, arXiv:0810.3709 [math.NT], Oct 21, 2008.",
				"H. Gupta, \u003ca href=\"/A002053/a002053.pdf\"\u003eOn a table of values of L(n)\u003c/a\u003e, Proceedings of the Indian Academy of Sciences. Section A, 12 (1940), 407-409. [Annotated scanned copy]",
				"R. S. Lehman, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1960-0120198-5\"\u003eOn Liouville's function\u003c/a\u003e, Math. Comp., 14 (1960), 311-320.",
				"Andrei Vieru, \u003ca href=\"http://arxiv.org/abs/1306.0496\"\u003eEuler constant as a renormalized value of Riemann zeta function at its pole. Rationals related to Dirichlet L-functions\u003c/a\u003e, arXiv:1306.0496 [math.GM], 2015.",
				"H. Walum, \u003ca href=\"http://dx.doi.org/10.1016/0022-314X(80)90072-4\"\u003eA recurrent pattern in the list of quadratic residues mod a prime and in the values of the Liouville lambda function\u003c/a\u003e, J. Numb. Theory 12 (1) (1980) 53-56.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/LiouvilleFunction.html\"\u003eLiouville Function\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Liouville_function\"\u003eLiouville function\u003c/a\u003e",
				"\u003ca href=\"/index/Di#divseq\"\u003eIndex to divisibility sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Eu#epf\"\u003eIndex entries for sequences computed from exponents in factorization of n\u003c/a\u003e"
			],
			"formula": [
				"Dirichlet g.f.: zeta(2s)/zeta(s); Dirichlet inverse of A008966.",
				"Sum_{ d divides n } lambda(d) = 1 if n is a square, else 0.",
				"Completely multiplicative with a(p) = -1, p prime.",
				"a(n) = (-1)^A001222(n) = (-1)^bigomega(n). - _Jonathan Vos Post_, Apr 16 2006",
				"a(n) = A033999(A001222(n)). - _Jaroslav Krizek_, Sep 28 2009",
				"Sum{d|n} a(d) *(A000005(d))^2 = a(n) *Sum{d|n} A000005(d). - _Vladimir Shevelev_, May 22 2010",
				"a(n) = 1 - 2*A066829(n). - _Reinhard Zumkeller_, Nov 19 2011",
				"a(n) = i^(tau(n^2)-1) where tau(n) is A000005 and i is the imaginary unit. - _Anthony Browne_, May 11 2016",
				"a(n) = A106400(A156552(n)). - _Antti Karttunen_, May 30 2017",
				"Recurrence: a(1)=1, n \u003e 1: a(n) = sign(1/2 - Sum_{d\u003cn, d|n} a(d)). - _Mats Granvik_, Oct 11 2017",
				"a(n) = Sum_{ d | n } A008683(d)*A010052(n/d). - _Jinyuan Wang_, Apr 20 2019",
				"a(1) = 1; a(n) = -Sum_{d|n, d \u003c n} mu(n/d)^2 * a(d). - _Ilya Gutkovskiy_, Mar 10 2021"
			],
			"example": [
				"a(4) = 1 because since bigomega(4) = 2 (the prime divisor 2 is counted twice), then (-1)^2 = 1.",
				"a(5) = -1 because 5 is prime and therefore bigomega(5) = 1 and (-1)^1 = -1."
			],
			"maple": [
				"A008836 := n -\u003e (-1)^numtheory[bigomega](n); # _Peter Luschny_, Sep 15 2011",
				"with(numtheory): A008836 := proc(n) local i,it,s; it := ifactors(n): s := (-1)^add(it[2][i][2], i=1..nops(it[2])): RETURN(s) end:"
			],
			"mathematica": [
				"Table[LiouvilleLambda[n], {n, 100}] (* _Enrique Pérez Herrero_, Dec 28 2009 *)",
				"Table[If[OddQ[PrimeOmega[n]],-1,1],{n,110}] (* _Harvey P. Dale_, Sep 10 2014 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, 0, n=factor(n); (-1)^sum(i=1, matsize(n)[1], n[i,2]))}; /* _Michael Somos_, Jan 01 2006 */",
				"(PARI) a(n)=(-1)^bigomega(n) \\\\ _Charles R Greathouse IV_, Jan 09 2013",
				"(Haskell)",
				"a008836 = (1 -) . (* 2) . a066829  -- _Reinhard Zumkeller_, Nov 19 2011"
			],
			"xref": [
				"Cf. A000005, A001222, A002053, A007421, A002819 (partial sums), A008683, A010052, A026424, A028260, A028488, A056912, A056913, A065043, A066829, A106400, A156552."
			],
			"keyword": "sign,easy,nice,mult",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_",
			"references": 156,
			"revision": 112,
			"time": "2021-07-29T01:22:22-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}