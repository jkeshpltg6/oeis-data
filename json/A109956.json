{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A109956",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 109956,
			"data": "1,-1,1,3,-4,1,-12,18,-7,1,55,-88,42,-10,1,-273,455,-245,75,-13,1,1428,-2448,1428,-510,117,-16,1,-7752,13566,-8379,3325,-910,168,-19,1,43263,-76912,49588,-21252,6578,-1472,228,-22,1,-246675,444015,-296010,134550,-45630,11700,-2223,297,-25,1",
			"name": "Inverse of Riordan array (1/(1-x), x/(1-x)^3), A109955.",
			"comment": [
				"Riordan array (g,f) where f/(1-f)^3=x and g=1-f.",
				"First column is (-1)^n*binomial(3n,n)/(2n+1), a signed version of A001764.",
				"Second column is a signed version of A006629.",
				"Diagonal sums are A109957."
			],
			"link": [
				"Paul Drube, \u003ca href=\"https://arxiv.org/abs/2007.01892\"\u003eGeneralized Path Pairs and Fuss-Catalan Triangles\u003c/a\u003e, arXiv:2007.01892 [math.CO], 2020. See Figure 4 p. 8 (up to signs)."
			],
			"formula": [
				"Number triangle T(n, k) = (-1)^(n-k)*((3k+1)/(2n+k+1))*binomial(3n, n-k).",
				"From _Werner Schulte_, Oct 27 2015: (Start)",
				"If u(m,n) = (-1)^n*(Sum_{k=0..n} T(n,k)*((m+1)*k+1)) and v(m,n) = (-1)^n*(Sum_{k=0..n} (-1)^k*T(n,k)*m^k) and D(x) is the g.f. of A001764 then P(m,x) = Sum_{n\u003e=0} u(m,n)*x^n = 1-(m+1)*x*D(x)^2 and Q(m,x) = Sum_{n\u003e=0} v(m,n)*x^n = 1/P(m,x).",
				"If G(k,x) is the g.f. of column k (k\u003e=0) then G(k,x) = G(0,x)^(3*k+1). (End)"
			],
			"example": [
				"Triangle begins:",
				"      1;",
				"     -1,   1;",
				"      3,  -4,    1;",
				"    -12,  18,   -7,   1;",
				"     55, -88,   42, -10,   1;",
				"   -273, 455, -245,  75, -13, 1;",
				"   ..."
			],
			"maple": [
				"# Function RiordanSquare defined in A321620.",
				"tt := sin(arcsin(3*sqrt(x*3/4))/3)/sqrt(x*3/4): R := RiordanSquare(tt, 11):",
				"seq(seq(LinearAlgebra:-Row(R,n)[k]*(-1)^(n+k), k=1..n), n=1..11); # _Peter Luschny_, Nov 27 2018"
			],
			"mathematica": [
				"T[n_, k_] := (-1)^(n - k)((3k + 1)/(2n + k + 1)) Binomial[3n, n - k];",
				"Table[T[n, k], {n, 0, 9}, {k, 0, n}] (* _Jean-François Alcover_, Jun 13 2019 *)"
			],
			"program": [
				"(PARI) tabl(nn) = {my(m = matrix(nn, nn, n, k, if (n\u003ck, 0, binomial(n+2*k-3, 3*k-3)))); m = 1/m; for (n=1, nn, for (k=1, n, print1(m[n, k], \", \");); print(););} \\\\ _Michel Marcus_, Nov 20 2015"
			],
			"xref": [
				"Cf. A001764, A109955, A109957, A321620."
			],
			"keyword": "easy,sign,tabl",
			"offset": "0,4",
			"author": "_Paul Barry_, Jul 06 2005",
			"references": 3,
			"revision": 24,
			"time": "2020-07-07T06:58:31-04:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}