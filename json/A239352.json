{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A239352",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 239352,
			"data": "0,0,1,12,48,130,285,546,952,1548,2385,3520,5016,6942,9373,12390,16080,20536,25857,32148,39520,48090,57981,69322,82248,96900,113425,131976,152712,175798,201405,229710,260896,295152,332673,373660,418320,466866,519517",
			"name": "van Heijst's upper bound on the number of squares inscribed by a real algebraic curve in R^2 of degree n, if the number is finite.",
			"comment": [
				"In 1911 Toeplitz conjectured the Square Peg (or Inscribed Square) Problem: Every continuous simple closed curve in the plane contains 4 points that are the vertices of a square. The conjecture is still open. Many special cases have been proved; see Matschke's beautiful 2014 survey.",
				"Recently van Heijst proved that any real algebraic curve in R^2 of degree d inscribes either at most (d^4 - 5d^2 + 4d)/4 or infinitely many squares. He conjectured that a generic complex algebraic plane curve inscribes exactly (d^4 - 5d^2 + 4d)/4 squares."
			],
			"reference": [
				"Otto Toeplitz, Über einige Aufgaben der Analysis situs, Verhandlungen der Schweizerischen Naturforschenden Gesellschaft in Solothurn, 4 (1911), 197."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A239352/b239352.txt\"\u003eTable of n, a(n) for n = 0..2500\u003c/a\u003e",
				"Wouter van Heijst, \u003ca href=\"http://arxiv.org/abs/1403.5979\"\u003eThe algebraic square peg problem\u003c/a\u003e, arXiv:1403.5979 [math.AG], 2014.",
				"Wouter van Heijst, \u003ca href=\"http://urn.fi/URN:NBN:fi:aalto-201404101659\"\u003eThe algebraic square peg problem\u003c/a\u003e, Master’s thesis, Aalto University, 2014.",
				"Benjamin Matschke, \u003ca href=\"http://www.ams.org/notices/201404/rnoti-p346.pdf\"\u003eA Survey on the Square Peg Problem\u003c/a\u003e, AMS Notices, 61 (2014), 346-352.",
				"Benjamin Matschke, \u003ca href=\"http://people.mpim-bonn.mpg.de/matschke/SurveyOnSquarePeg_extension14.pdf\"\u003eExtended Survey on the Square Peg Problem\u003c/a\u003e, Max Planck Institute for Mathematics, 2014.",
				"\u003ca href=\"https://oeis.org/search?q=%22Inscribed+Square%22\"\u003eSequences related to inscribed squares\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (5,-10,10,-5,1)."
			],
			"formula": [
				"a(n) = (n^4 - 5*n^2 + 4*n)/4 = n*(n - 1)*(n^2 + n - 4)/4, which shows the formula is an integer.",
				"G.f.: x^2 * (1 + 7*x - 2*x^2) / (1 - x)^5. - _Michael Somos_, Mar 21 2014",
				"a(n) = A172225(n)/2. - _R. J. Mathar_, Jan 09 2018"
			],
			"example": [
				"A point or a line has no inscribed squares, so a(0) = a(1) = 0.",
				"A circle has infinitely many inscribed squares, and an ellipse that is not a circle has exactly one, agreeing with a(2) = 1.",
				"G.f. = x^2 + 12*x^3 + 48*x^4 + 130*x^5 + 285*x^6 + 546*x^7 + 952*x^8 + ..."
			],
			"mathematica": [
				"Table[(n^4 - 5 n^2 + 4 n)/4, {n, 0, 38}]"
			],
			"program": [
				"(PARI) for(n=0,50, print1((n^4 - 5*n^2 + 4*n)/4, \", \")) \\\\ _G. C. Greubel_, Aug 07 2018",
				"(MAGMA) [(n^4 - 5*n^2 + 4*n)/4: n in [0..50]]; // _G. C. Greubel_, Aug 07 2018"
			],
			"xref": [
				"Cf. A088544, A089058, A123673, A123697, A209432, A231739."
			],
			"keyword": "nonn,easy",
			"offset": "0,4",
			"author": "_Jonathan Sondow_, Mar 21 2014",
			"references": 2,
			"revision": 40,
			"time": "2018-08-07T06:29:03-04:00",
			"created": "2014-03-21T19:17:59-04:00"
		}
	]
}