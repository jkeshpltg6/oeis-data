{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A110625",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 110625,
			"data": "1,1,3,101,5807,77801,82949,170636,170636,170636,363113,363113,84848,710567,22435781,3901243741,27210449083,1003538672911,248595095590537,10165684261926701,438167567023512863,439119040574907047",
			"name": "Numerator of b(n) = -Sum_{k=1..n} A037861(k)/((2*k)*(2*k+1)), where A037861(k) = (number of 0's) - (number of 1's) in the binary representation of k.",
			"comment": [
				"Numerators of partial sums of a series for the \"alternating Euler constant\" log(4/Pi) (see A094640 and Sondow 2005, 2010). Denominators are A110626."
			],
			"link": [
				"Petros Hadjicostas, \u003ca href=\"/A110625/b110625.txt\"\u003eTable of n, a(n) for n = 1..120\u003c/a\u003e",
				"Jonathan Sondow, \u003ca href=\"https://arxiv.org/abs/math/0211148\"\u003e Double integrals for Euler's constant and ln(4/Pi) and an analog of Hadjicostas's formula\u003c/a\u003e, arXiv:math/0211148 [math.CA], 2002-2004.",
				"Jonathan Sondow, \u003ca href=\"https://www.jstor.org/stable/30037385\"\u003e Double integrals for Euler's constant and ln(4/Pi) and an analog of Hadjicostas's formula\u003c/a\u003e, Amer. Math. Monthly 112 (2005), 61-65.",
				"Jonathan Sondow, \u003ca href=\"https://arxiv.org/abs/math/0508042\"\u003eNew Vacca-Type Rational Series for Euler's Constant and Its \"Alternating\" Analog ln(4/Pi)\u003c/a\u003e, arXiv:math/0508042 [math.NT], 2005.",
				"Jonathan Sondow, \u003ca href=\"https://doi.org/10.1007/978-0-387-68361-4_23\"\u003eNew Vacca-Type Rational Series for Euler's Constant and Its \"Alternating\" Analog ln(4/Pi)\u003c/a\u003e, Additive Number Theory, Festschrift In Honor of the Sixtieth Birthday of Melvyn B. Nathanson (D. Chudnovsky and G. Chudnovsky, eds.), Springer, 2010, pp. 331-340."
			],
			"formula": [
				"Lim_{n -\u003e infinity} b(n) = log 4/Pi = 0.24156..."
			],
			"example": [
				"a(3) = 3 because b(3) = 1/6 + 0 + 1/21 = 3/14.",
				"The first few fractions b(n) are 1/6, 1/6, 3/14, 101/504, 5807/27720, 77801/360360, 82949/360360, ... = A110625/A110626. - _Petros Hadjicostas_, May 15 2020"
			],
			"program": [
				"(PARI) a(n) = numerator(-sum(k=1, n, (#binary(k) - 2*hammingweight(k))/(2*k*(2*k+1)))); \\\\ _Petros Hadjicostas_, May 15 2020"
			],
			"xref": [
				"Cf. A037861, A073099, A094640, A110626."
			],
			"keyword": "easy,frac,nonn",
			"offset": "1,3",
			"author": "_Jonathan Sondow_, Aug 01 2005",
			"references": 6,
			"revision": 27,
			"time": "2020-05-15T13:05:15-04:00",
			"created": "2005-09-21T03:00:00-04:00"
		}
	]
}