{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A206924",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 206924,
			"data": "1,1,3,4,6,6,10,9,9,9,15,13,11,11,21,18,14,16,14,14,14,16,28,24,16,16,18,18,18,18,36,31,21,19,19,19,25,21,23,23,19,21,21,21,21,25,45,39,23,25,23,23,23,21,29,29,23,21,25,25,25,27,55,48,30,26,26",
			"name": "Number of contiguous palindromic bit patterns in the n-th binary palindrome.",
			"comment": [
				"For a given number of places m a binary palindrome has at least 2*(m-1) + floor((m-3)/2) palindromic substrings. To a certain extent, this number indicates the minimal possible grade of symmetry (cf. A210926 and A217099)."
			],
			"link": [
				"Hieronymus Fischer, \u003ca href=\"/A206924/b206924.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) \u003c= m*(m+1)/2, where m = 1+floor(log_2(A006995(n)), equality holds if n+1 is a power of 2 or n+1 is 3-times a power of 2.",
				"a(n) \u003e= 2*floor(log_2(A006995(n))).",
				"a(n) = A206925(A006995(n)).",
				"a(n) \u003c= ((floor(log_2(n)) + floor(log_2(n/3)) + 3) * (floor(log_2(n)) + floor(log_2(n/3))) + 2)/2.",
				"a(n) \u003e= 2*(floor(log_2(n)) + floor(log_2(n/3))), n\u003e1. Equality holds for n=4 and n=6, only.",
				"With m = 1+floor(log_2(A006995(n)), n\u003e1:",
				"a(n) \u003e= 2(m-1) + floor((m-3)/2). Equality holds infinitely often for those n\u003e3 for which A006995(n) is a term of A217099.",
				"a(n) \u003e= (5m - 8)/2. Equality holds infinitely often for those n\u003e3 for which A006995(n) is a term of A217099 with an even number of digits.",
				"a(n) \u003e= 3*floor(log_2(n)) + 2*floor(log_2(n/3)) - 2. Equality holds infinitely often for those n\u003e3 for which A006995(n) is a term of A217099",
				"a(n) \u003e= |3*floor(log_2(n)) + 2*floor(log_2(n/3)) - 2|, n\u003e1.",
				"Asymptotic behavior:",
				"a(n) = O(log(n)^2).",
				"lim sup a(n)/log_2(n)^2 = 2, for n -\u003e infinity.",
				"lim inf a(n)/log_2(n) = 5, for n -\u003e infinity.",
				"lim inf (a(n) - 3*floor(log_2(n)) - 2*floor(log_2(n/3))) = -2, for n -\u003e infinity.",
				"lim inf a(n)/log_2(A006995(n)) = 5/2, for n -\u003e infinity.",
				"lim inf (2a(n) - 5*floor(log_2(A006995(n)))) = -3, for n -\u003e infinity."
			],
			"example": [
				"a(1) = a(2) = 1, since A006995(1) = 0 and A006995(2) = 1;",
				"a(3) = 3, since A006995(3)=3=11_2 and so there are the following 3 palindromic bit patterns the left 1, the right 1 and 11;",
				"a(10) = 9, since A006995(10) = 27 = 11011_2 and so there are the following 9 palindromic bit patterns: 1, 1, 0, 1, 1, 11, 11, 101, 11011."
			],
			"mathematica": [
				"palQ[w_] := w == Reverse@w; subs[w_] := Flatten[Table[Take[w, {j, i}], {i, Length@w}, {j,i}], 1]; seq={}; k=0; While[Length@seq \u003c 100, u = IntegerDigits[k++,2]; If[palQ@u, AppendTo[seq, Length@Select[subs@u, palQ]]]]; seq (* _Giovanni Resta_, Feb 13 2013 *)"
			],
			"program": [
				"(Smalltalk)",
				"A206924",
				"\"Calculates a(n)\"",
				"^self A006995 A206925"
			],
			"xref": [
				"Cf. A006995, A070939, A206923, A206925, A206926, A217099."
			],
			"keyword": "nonn,base",
			"offset": "1,3",
			"author": "_Hieronymus Fischer_, Mar 12 2012; additional formulas Jan 23 2013",
			"references": 9,
			"revision": 24,
			"time": "2013-03-05T15:34:11-05:00",
			"created": "2012-03-26T15:33:28-04:00"
		}
	]
}