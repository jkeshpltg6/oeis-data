{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A229468",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 229468,
			"data": "1,4,1,15,3,1,50,11,2,1,156,35,10,4,1,460,101,36,14,4,1,1296,298,105,44,16,6,1,3522,798,300,130,56,23,6,1,9255,2154,827,377,174,82,31,9,1,23672,5490,2164,1015,502,243,108,43,10,1,59050,13914,5525,2658,1350,705,343,154,55,13,1",
			"name": "Number T(n,k) of parts of each size k^2 in all partitions of n^2 into squares; triangle T(n,k), 1 \u003c= k \u003c= n, read by rows.",
			"link": [
				"Christopher Hunt Gribble and Alois P. Heinz, \u003ca href=\"/A229468/b229468.txt\"\u003eRows n = 1..141, flattened\u003c/a\u003e (Rows n = 1..21 from Christopher Hunt Gribble)",
				"Christopher Hunt Gribble, \u003ca href=\"/A229468/a229468.cpp.txt\"\u003eC++ program\u003c/a\u003e"
			],
			"formula": [
				"Sum_{k=1..n} T(n,k) * k^2 = A037444(n) * n^2."
			],
			"example": [
				"For n = 3, the 4 partitions are:",
				"Square side 1 2 3",
				"            9 0 0",
				"            5 1 0",
				"            1 2 0",
				"            0 0 1",
				"Total      15 3 1",
				"So T(3,1) = 15, T(3,2) = 3, T(3,3) = 1.",
				"The triangle begins:",
				".\\ k    1     2     3     4     5     6     7     8     9 ...",
				".n",
				".1      1",
				".2      4     1",
				".3     15     3     1",
				".4     50    11     2     1",
				".5    156    35    10     4     1",
				".6    460   101    36    14     4     1",
				".7   1296   298   105    44    16     6     1",
				".8   3522   798   300   130    56    23     6     1",
				".9   9255  2154   827   377   174    82    31     9     1",
				"10  23672  5490  2164  1015   502   243   108    43    10 ...",
				"11  59050 13914  5525  2658  1350   705   343   154    55 ..."
			],
			"maple": [
				"b:= proc(n, i) option remember;",
				"      `if`(n=0 or i=1, 1+n*x, b(n, i-1)+",
				"      `if`(i^2\u003en, 0, (g-\u003eg+coeff(g, x, 0)*x^i)(b(n-i^2, i))))",
				"    end:",
				"T:= n-\u003e (p-\u003eseq(coeff(p, x, i), i=1..n))(b(n^2, n)):",
				"seq(T(n), n=1..14);  # _Alois P. Heinz_, Sep 24 2013"
			],
			"mathematica": [
				"b[n_, i_] := b[n, i] = If[n==0 || i==1, 1+n*x, b[n, i-1] + If[i^2\u003en, 0, Function[ {g}, g+Coefficient[g, x, 0]*x^i][b[n-i^2, i]]]]; T[n_] := Function[{p}, Table[ Coefficient[p, x, i], {i, 1, n}]][ b[n^2, n]]; Table[T[n], {n, 1, 14}] // Flatten (* _Jean-François Alcover_, Mar 09 2015, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Row sums give: A229239.",
				"Cf. A037444."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Christopher Hunt Gribble_, Sep 24 2013",
			"references": 3,
			"revision": 25,
			"time": "2015-03-09T06:07:07-04:00",
			"created": "2013-09-24T20:27:14-04:00"
		}
	]
}