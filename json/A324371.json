{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A324371",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 324371,
			"data": "1,2,3,2,5,3,7,2,3,5,11,3,13,7,5,2,17,3,19,5,7,11,23,1,5,13,3,7,29,15,31,2,11,17,35,3,37,19,13,5,41,7,43,11,1,23,47,1,7,5,17,13,53,3,55,7,19,29,59,5,61,31,7,2,13,11,67,17,23,7,71,1,73,37,5,19,77,13,79,5,3,41,83,21",
			"name": "Product of all primes p dividing n such that the sum of the base p digits of n is less than p, or 1 if no such prime.",
			"comment": [
				"Does not contain any elements of A324315, and thus none of the Carmichael numbers A002997.",
				"See the section on Bernoulli polynomials in Kellner and Sondow 2019."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A324371/b324371.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Bernd C. Kellner and Jonathan Sondow, Power-Sum Denominators, Amer. Math. Monthly, 124 (2017), 695-709. doi:\u003ca href=\"https://doi.org/10.4169/amer.math.monthly.124.8.695\"\u003e10.4169/amer.math.monthly.124.8.695\u003c/a\u003e, arXiv:\u003ca href=\"https://arxiv.org/abs/1705.03857\"\u003e1705.03857\u003c/a\u003e",
				"Bernd C. Kellner and Jonathan Sondow, \u003ca href=\"https://arxiv.org/abs/1902.10672\"\u003eOn Carmichael and polygonal numbers, Bernoulli polynomials, and sums of base-p digits\u003c/a\u003e, arXiv:1902.10672 [math.NT] 2019."
			],
			"formula": [
				"a(n) * A324369(n) = A007947(n) = radical(n).",
				"a(n) * A195441(n) = a(n) * A324369(n) * A324370(n) = A144845(n-1) = denominator(Bernoulli_{n-1}(x))."
			],
			"example": [
				"For p = 2 and 3, the sum of the base p digits of 6 is 1+1+0 = 2 \u003e= 2 and 2+0 = 2 \u003c 3, respectively, so a(6) = 3."
			],
			"maple": [
				"f:= n -\u003e convert(select(p -\u003e convert(convert(n,base,p),`+`)\u003cp,",
				"numtheory:-factorset(n)),`*`):map(f, [$1..100]); # _Robert Israel_, Apr 26 2020"
			],
			"mathematica": [
				"SD[n_, p_] := If[n \u003c 1 || p \u003c 2, 0, Plus @@ IntegerDigits[n, p]];",
				"LP[n_] := Transpose[FactorInteger[n]][[1]];",
				"DD3[n_] := Times @@ Select[LP[n], SD[n, #] \u003c # \u0026];",
				"Table[DD3[n], {n, 1, 100}]"
			],
			"xref": [
				"Cf. A002997, A007947, A144845, A195441, A324315, A324316, A324317, A324318, A324319, A324320, A324369, A324370, A324404, A324405."
			],
			"keyword": "nonn,base,look",
			"offset": "1,2",
			"author": "_Bernd C. Kellner_ and _Jonathan Sondow_, Feb 25 2019",
			"references": 12,
			"revision": 17,
			"time": "2020-04-26T21:50:53-04:00",
			"created": "2019-03-01T04:40:06-05:00"
		}
	]
}