{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A048898",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 48898,
			"data": "0,2,7,57,182,2057,14557,45807,280182,280182,6139557,25670807,123327057,123327057,5006139557,11109655182,102662389557,407838170807,3459595983307,3459595983307,79753541295807,365855836217682,2273204469030182,2273204469030182,49956920289342682",
			"name": "One of the two successive approximations up to 5^n for the 5-adic integer sqrt(-1). Here the 2 (mod 5) numbers (except for n=0).",
			"comment": [
				"This is the root congruent to 2 mod 5.",
				"Or, residues modulo 5^n of a 5-adic solution of x^2+1=0.",
				"The radix-5 expansion of a(n) is obtained from the n rightmost digits in the expansion of the following pentadic integer:",
				"  ...422331102414131141421404340423140223032431212 = u",
				"The residues modulo 5^n of the other 5-adic solution of x^2+1=0 are given by A048899 which corresponds to the pentadic integer -u:",
				"  ...022113342030313303023040104021304221412013233 = -u",
				"The digits of u and -u are given in A210850 and A210851, respectively. - _Wolfdieter Lang_, May 02 2012.",
				"For approximations for p-adic square roots see also the W. Lang link under A268922. - _Wolfdieter Lang_, Apr 03 2016."
			],
			"reference": [
				"J. H. Conway, The Sensual Quadratic Form, p. 118, Mathematical Association of America, 1997, The Carus Mathematical Monographs, Number 26.",
				"K. Mahler, Introduction to p-Adic Numbers and Their Functions, Cambridge, 1973, p. 35."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A048898/b048898.txt\"\u003eTable of n, a(n) for n = 0..1431\u003c/a\u003e (terms 0..200 from Vincenzo Librandi)",
				"G. P. Michon, \u003ca href=\"http://www.numericana.com/answer/pseudo.htm#witness\"\u003eOn the witnesses of a composite integer\u003c/a\u003e, Numericana.",
				"G. P. Michon, \u003ca href=\"http://www.numericana.com/answer/p-adic.htm#integers\"\u003eIntroduction to p-adic integers\u003c/a\u003e, Numericana."
			],
			"formula": [
				"If n\u003e0, a(n) = 5^n - A048899(n).",
				"From _Wolfdieter Lang_, Apr 28 2012: (Start)",
				"Recurrence: a(n) = a(n-1)^5 (mod 5^n), a(1) = 2, n\u003e=2. See the J.-F. Alcover Mathematica program and the Pari program below.",
				"a(n) == 2^(5^(n-1)) (mod 5^n), n\u003e=1.",
				"a(n)*a(n-1) + 1 == 0 (mod 5^(n-1)), n\u003e=1.",
				"(a(n)^2 + 1)/5^n = A210848(n), n\u003e=0.",
				"(End)",
				"Another recurrence: a(n) = modp(a(n-1) + a(n-1)^2 + 1, 5^n), n \u003e= 2, a(1) = 2. Here modp(a, m) is the representative from {0, 1, ... ,|m|-1} of the residue class a modulo m. Note that a(n) is in the residue class of a(n-1) modulo 5^(n-1) (see Hensel lifting). - _Wolfdieter Lang_, Feb 28 2016"
			],
			"example": [
				"a(0)=0 because 0 satisfies any equation in integers modulo 1.",
				"a(1)=2 because 2 is one solution of x^2+1=0 modulo 5. (The other solution is 3, which gives rise to A048899.)",
				"a(2)=7 because the equation (5y+a(1))^2+1=0 modulo 25 means that y is 1 modulo 5."
			],
			"mathematica": [
				"a[0] = 0; a[1] = 2; a[n_] := a[n] = Mod[a[n-1]^5, 5^n]; Table[a[n], {n, 0, 21}] (* _Jean-François Alcover_, Nov 24 2011, after Pari *)",
				"Join[{0}, RecurrenceTable[{a[1] == 2, a[n] == Mod[a[n-1]^5, 5^n]}, a, {n, 25}]] (* _Vincenzo Librandi_, Feb 29 2016 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c2, 2, a(n-1)^5) % 5^n}",
				"(PARI) a(n) = lift(sqrt(-1 + O(5^n))); \\\\ _Kevin Ryde_, Dec 22 2020",
				"(MAGMA) [n le 2 select 2*(n-1) else Self(n-1)^5 mod 5^(n-1): n in [1..30]]; // _Vincenzo Librandi_, Feb 29 2016"
			],
			"xref": [
				"The two successive approximations up to p^n for p-adic integer sqrt(-1): this sequence and A048899 (p=5), A286840 and A286841 (p=13), A286877 and A286878 (p=17).",
				"Cf. A000351 (powers of 5), A034939(n) = Min(a(n), A048899(n)).",
				"Different from A034935."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,2",
			"author": "_Michael Somos_, Jul 26 1999",
			"ext": [
				"Additional comments from _Gerard P. Michon_, Jul 15 2009",
				"Edited by _N. J. A. Sloane_, Jul 25 2009",
				"Name clarified by _Wolfdieter Lang_, Feb 19 2016"
			],
			"references": 34,
			"revision": 73,
			"time": "2021-09-01T20:00:40-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}