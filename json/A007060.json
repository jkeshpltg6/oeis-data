{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007060",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7060,
			"data": "1,0,8,240,13824,1263360,168422400,30865121280,7445355724800,2287168006717440,871804170613555200,403779880746418176000,223346806774106790297600,145427383048755178635264000",
			"name": "Number of ways n couples can sit in a row without any spouses next to each other.",
			"comment": [
				"a(n) approaches (2n)!*exp(-1) as n goes to infinity.",
				"Also the number of (directed) Hamiltonian paths of the n-cocktail party graph - _Eric W. Weisstein_, Dec 16 2013"
			],
			"link": [
				"Andrew Woods, \u003ca href=\"/A007060/b007060.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e",
				"G. Almkvist, \u003ca href=\"/A007043/a007043.pdf\"\u003eLetter to N. J. A. Sloane, Apr. 1992\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CocktailPartyGraph.html\"\u003eCocktail Party Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HamiltonianPath.html\"\u003eHamiltonian Path\u003c/a\u003e"
			],
			"formula": [
				"a(n) = (Pi*BesselI(n+1/2,1)*(-1)^n+BesselK(n+1/2,1))*exp(-1)*(2/Pi)^(1/2)*2^n*n! - _Mark van Hoeij_, Nov 12 2009",
				"a(n) = (-1)^n*2^n*n!*A000806(n), n\u003e0. - _Vladeta Jovovic_, Nov 19 2009",
				"a(n) = n!*hypergeom([ -n, n+1],[],1/2)*(-2)^n. - _Mark van Hoeij_, Nov 13 2009",
				"a(n) = 2^n * A114938(n). - _Toby Gottfried_, Nov 22 2010",
				"a(n) = 2*n((2*n-1)*a(n-1) + (2*n-2)*a(n-2)), n\u003e1. - _Aaron Meyerowitz_, May 14 2014",
				"From _Peter Bala_, Mar 06 2015: (Start)",
				"a(n) = Sum_{k = 0..n} (-1)^(n-k)*binomial(n,k)*A000166(2*k).",
				"For n \u003e= 1, int_{x = 0..1} (x^2 - 1)^n*exp(x) dx = a(n)*e - A177840(n). Hence A177840(n)/a(n) -\u003e e as n -\u003e infinity (End)",
				"a(n) ~ sqrt(Pi) * 2^(2*n+1) * n^(2*n + 1/2) / exp(2*n+1). - _Vaclav Kotesovec_, Mar 09 2016"
			],
			"example": [
				"For n = 2, the a(2) = 8 solutions for the couples {1,2} and {3,4} are {1324, 1423, 2314, 2413, 3142, 3241, 4132, 4231}."
			],
			"maple": [
				"seq(add((-1)^i*binomial(n, i)*2^i*(2*n-i)!, i=0..n),n=0..20);"
			],
			"mathematica": [
				"Table[Sum[(-1)^i Binomial[n,i] (2 n - i)! 2^i, {i, 0, n}], {n, 0, 20}]",
				"Table[(2 n)! Hypergeometric1F1[-n, -2 n, -2], {n, 0, 20}]"
			],
			"program": [
				"(PARI) a(n)=sum(k=0, n, binomial(n, k)*(-1)^(n-k)*(n+k)!*2^(n-k)) \\\\ _Charles R Greathouse IV_, May 11 2016",
				"(Python)",
				"from sympy import binomial, subfactorial",
				"def a(n): return sum([(-1)**(n - k)*binomial(n, k)*subfactorial(2*k) for k in range(n + 1)]) # _Indranil Ghosh_, Apr 28 2017"
			],
			"xref": [
				"Cf. A000166, A000806, A114938, A177840, A053983, A053984."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,3",
			"author": "David Roberts Keeney (David.Roberts.Keeney(AT)directory.Reed.edu)",
			"ext": [
				"More terms from _Michel ten Voorde_, Apr 11 2001"
			],
			"references": 12,
			"revision": 57,
			"time": "2019-12-07T12:18:17-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}