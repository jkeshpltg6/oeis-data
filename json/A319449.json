{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A319449",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 319449,
			"data": "1,5,13,21,26,65,64,85,121,130,122,273,196,320,338,341,290,605,400,546,832,610,530,1105,651,980,1093,1344,842,1690,1024,1365,1586,1450,1664,2541,1444,2000,2548,2210,1682,4160,1936,2562,3146,2650,2210,4433,3249,3255",
			"name": "Sum of the norm of divisors of n over Eisenstein integers, with associated divisors counted only once.",
			"comment": [
				"Equivalent of sigma (A000203) in the ring of Eisenstein integers. Note that only norms are summed up."
			],
			"link": [
				"Jianing Song, \u003ca href=\"/A319449/b319449.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Eisenstein_integer\"\u003eEisenstein integer\u003c/a\u003e"
			],
			"formula": [
				"Multiplicative with a(3^e) = sigma(3^(2e)) = (3^(2e+1) - 1)/2, a(p^e) = sigma(p^e)^2 = ((p^(e+1) - 1)/(p - 1))^2 if p == 1 (mod 3) and sigma_2(p^e) = A001157(p^e) = (p^(2e+2) - 1)/(p^2 - 1) if p == 2 (mod 3)."
			],
			"example": [
				"Let w = (1 + sqrt(3)*i)/2, w' = (1 - sqrt(3)*i)/2, and ||d|| denote the norm of d.",
				"a(3) = ||1|| + ||1 + w|| + ||3|| = 1 + 3 + 9 = 13.",
				"a(7) = ||1|| + ||2 + w|| + ||2 + w'|| + ||7|| = 1 + 7 + 7 + 49 = 64."
			],
			"mathematica": [
				"f[p_, e_] := If[p == 3 , DivisorSigma[1, 3^(2*e)], Switch[Mod[p, 3], 1, DivisorSigma[1, p^e]^2, 2, DivisorSigma[2, p^e]]]; eisSigma[1] = 1; eisSigma[n_] := Times @@ f @@@ FactorInteger[n]; Array[eisSigma, 100] (* _Amiram Eldar_, Feb 10 2020 *)"
			],
			"program": [
				"(PARI)",
				"a(n)=",
				"{",
				"    my(r=1, f=factor(n));",
				"    for(j=1, #f[, 1], my(p=f[j, 1], e=f[j, 2]);",
				"        if(p==3, r*=((3^(2*e+1)-1)/2));",
				"        if(Mod(p, 3)==1, r*=((p^(e+1)-1)/(p-1))^2);",
				"        if(Mod(p, 3)==2, r*=(p^(2*e+2)-1)/(p^2-1));",
				"    );",
				"    return(r);",
				"}"
			],
			"xref": [
				"Cf. A001157.",
				"Equivalent of arithmetic functions in the ring of Eisenstein integers (the corresponding functions in the ring of integers are in the parentheses): A319442 (\"d\", A000005), this sequence (\"sigma\", A000203), A319445 (\"phi\", A000010), A319446 (\"psi\", A002322), A319443 (\"omega\", A001221), A319444 (\"Omega\", A001222), A319448 (\"mu\", A008683).",
				"Equivalent in the ring of Gaussian integers: A317797."
			],
			"keyword": "nonn,mult",
			"offset": "1,2",
			"author": "_Jianing Song_, Sep 19 2018",
			"references": 7,
			"revision": 21,
			"time": "2020-02-10T18:06:33-05:00",
			"created": "2018-09-24T08:51:09-04:00"
		}
	]
}