{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A106607",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 106607,
			"data": "1,1,3,5,9,13,20,28,39,51,67,85,107,131,160,192,229,269,315,365,421,481,548,620,699,783,875,973,1079,1191,1312,1440,1577,1721,1875,2037,2209,2389,2580,2780,2991,3211,3443,3685,3939,4203,4480,4768,5069,5381,5707,6045",
			"name": "Expansion of (1+t^3)^2/((1-t)*(1-t^2)^2*(1-t^4)).",
			"comment": [
				"Molien series for 5-dimensional group of order 8.",
				"For of each of the quadrisections the n-th term is a polynomial in n of degree 3. - _Ralf Stephan_, Nov 16 2010",
				"Number of non-isomorphic 3 X 3 nonnegative integer matrices with all row and column sums equal to n up to permutations of rows and columns. - _Andrew Howroyd_, Apr 08 2020"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A106607/b106607.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"S. Ling and P. Solé, \u003ca href=\"https://doi.org/10.1006/eujc.2001.0509\"\u003eType II Codes over F_4 + u F_4\u003c/a\u003e, European J. Combinatorics, 22 (2001), 983-997.",
				"\u003ca href=\"/index/Rec#order_07\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1,1,-3,3,-1)."
			],
			"formula": [
				"G.f.: (1-x+x^2)^2/( (1+x)*(1+x^2)*(1-x)^4 ). - _R. J. Mathar_, Dec 18 2014",
				"a(n) = (4*n^3 +18*n^2 +56*n +3*(9*(-1)^n +2*(1-i)*(-i)^n +2*(1+i)*i^n +19))/96 where i is the imaginary unit. - _Colin Barker_, Feb 08 2016",
				"E.g.f.: (1/48)*(6*(cos(x) - sin(x)) + p(x)*sinh(x) + (27 + p(x))*cosh(x)), where p(x) = 15 + 39*x + 15*x^2 + 2*x^3. - _G. C. Greubel_, Sep 08 2021"
			],
			"example": [
				"The a(4) = 9 symmetric matrices are:",
				"  [0 0 4]  [0 1 3]  [0 1 3]  [0 2 2]  [0 2 2]",
				"  [0 4 0]  [1 2 1]  [1 3 0]  [2 0 2]  [2 1 1]",
				"  [4 0 0]  [3 1 0]  [3 0 1]  [2 2 0]  [2 1 1]",
				".",
				"  [1 1 2]  [1 0 3]  [1 1 2]  [2 0 2]",
				"  [1 2 1]  [0 4 0]  [1 3 0]  [0 4 0]",
				"  [2 1 1]  [3 0 1]  [2 0 2]  [2 0 2]"
			],
			"maple": [
				"(1+t^3)^2/((1-t)*(1-t^2)^2*(1-t^4));",
				"seq(coeff(series(%,t,n+1), t,n), n=0..60);"
			],
			"mathematica": [
				"LinearRecurrence[{3,-3,1,1,-3,3,-1}, {1,1,3,5,9,13,20}, 61] (* _G. C. Greubel_, Sep 08 2021 *)"
			],
			"program": [
				"(PARI) a(n) = i=I; (4*n^3+18*n^2+56*n+3*(9*(-1)^n+(2-2*i)*(-i)^n+(2+2*i)*i^n+19))/96 \\\\ _Colin Barker_, Feb 08 2016",
				"(Sage)",
				"def A106607_list(prec):",
				"    P.\u003cx\u003e = PowerSeriesRing(ZZ, prec)",
				"    return P( (1+x^3)^2/((1-x)*(1-x^2)^2*(1-x^4)) ).list()",
				"A106607_list(60) # _G. C. Greubel_, Sep 08 2021"
			],
			"xref": [
				"Row n=3 of A333737.",
				"Cf. A100779."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_, May 12 2005",
			"references": 3,
			"revision": 42,
			"time": "2021-09-10T06:09:17-04:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}