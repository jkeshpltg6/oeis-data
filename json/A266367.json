{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A266367",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 266367,
			"data": "1,4,10,24,54,116,250,536,1142,2436,5194,11064,23574,50228,107002,227960,485654,1034628,2204170,4695768,10003830,21312116,45403258,96726872,206066486,439003140,935250250,1992452856,4244712534,9042916148,19264987258,41042041016,87435776726",
			"name": "Expansion of b(2)*b(4)/(1 - 2*x - 2*x^3 + 3*x^4), where b(k) = (1-x^k)/(1-x).",
			"comment": [
				"This is the Poincaré series [or Poincare series] for the quasi-Lannér diagram QL4_16 - see Table 7.8 in Maxim Chapovalov, Dimitry Leites and Rafael Stekolshchik (2009), or equivalently Table 6 in the shorter version, Maxim Chapovalov, Dimitry Leites and Rafael Stekolshchik (2010)."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A266367/b266367.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Maxim Chapovalov, Dimitry Leites, and Rafael Stekolshchik, \u003ca href=\"http://arxiv.org/abs/0906.1596\"\u003eThe Poincaré series [or Poincare series] of the hyperbolic Coxeter groups with finite volume of fundamental domains\u003c/a\u003e, arXiv:0906.1596 [math.RT], 2009, page 31.",
				"Maxim Chapovalov, Dimitry Leites, and Rafael Stekolshchik, \u003ca href=\"http://dx.doi.org/10.1142/S1402925110000842\"\u003eThe Poincaré series [or Poincare series] of the hyperbolic Coxeter groups with finite volume of fundamental domains\u003c/a\u003e, Journal of Nonlinear Mathematical Physics, Volume 17, Supplement 1 (2010), page 186.",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,0,2,-3)"
			],
			"formula": [
				"G.f.: (1 + x)^2*(1 + x^2)/((1 - x)*(1 - x - x^2 - 3*x^3)).",
				"a(n) = 2*a(n-1) + 2*a(n-3) - 3*a(n-4) for n\u003e4."
			],
			"mathematica": [
				"CoefficientList[Series[(1 + x)^2 (1 + x^2)/((1 - x) (1 - x - x^2 - 3 x^3)), {x, 0, 40}], x]",
				"LinearRecurrence[{2,0,2,-3},{1,4,10,24,54},40] (* _Harvey P. Dale_, Mar 22 2016 *)"
			],
			"program": [
				"(MAGMA) /* By definition: */ m:=40; R\u003cx\u003e:=PowerSeriesRing(Integers(), m); b:=func\u003ck|(1-x^k)/(1-x)\u003e; Coefficients(R!(b(2)*b(4)/(1-2*x-2*x^3+3*x^4)));"
			],
			"xref": [
				"Cf. similar sequences listed in A265055."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Bruno Berselli_, Dec 28 2015",
			"references": 2,
			"revision": 14,
			"time": "2018-01-30T18:57:40-05:00",
			"created": "2015-12-28T11:35:26-05:00"
		}
	]
}