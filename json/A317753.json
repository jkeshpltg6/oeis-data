{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A317753",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 317753,
			"data": "0,1,13,2,10,14,18,3,7,11,53,15,19,19,23,4,27,8,50,12,73,54,16,16,58,20,20,20,43,24,24,5,47,28,325,9,70,51,32,13,13,74,272,55,55,17,17,17,276,59,40,21,40,21,21,21,63,44,63",
			"name": "Number of steps to reach 1 in 7x+-1 problem, or -1 if 1 is never reached.",
			"comment": [
				"The 7x+-1 problem is as follows. Start with any natural number n. If 4 divides n-1, multiply it by 7 and add 1; if 4 divides n+1, multiply it by 7 and subtract 1; otherwise divide it by 2. The 7x+-1 problem concerns the question whether we always reach 1.",
				"The number of steps to reach 1 is also called the total stopping time.",
				"Also the least positive k for which the iterate A317640^k(n) = 1."
			],
			"link": [
				"David Barina, \u003ca href=\"/A317753/b317753.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"D. Barina, \u003ca href=\"https://arxiv.org/abs/1807.00908\"\u003e7x+-1: Close Relative of Collatz Problem\u003c/a\u003e, arXiv:1807.00908 [math.NT], 2018.",
				"K. Matthews, \u003ca href=\"http://www.numbertheory.org/php/barina.html\"\u003eDavid Barina's 7x+1 conjecture\u003c/a\u003e."
			],
			"example": [
				"a(5)=10 because the trajectory of 5 is (5, 36, 18, 9, 64, 32, 16, 8, 4, 2, 1)."
			],
			"mathematica": [
				"f[n_] := Switch[Mod[n, 4], 0, n/2, 1, 7 n + 1, 2, n/2, 3, 7 n - 1]; a[n_] := Length@NestWhileList[f, n, # \u003e 1 \u0026] - 1; Array[a, 70] (* _Robert G. Wilson v_, Aug 07 2018 *)"
			],
			"program": [
				"(C)",
				"int a(int n) {",
				"        int s = 0;",
				"        while( n != 1 ) {",
				"                switch(n%4) {",
				"                        case 1: n = 7*n+1; break;",
				"                        case 3: n = 7*n-1; break;",
				"                        default: n = n/2;",
				"                }",
				"                s++;",
				"        }",
				"        return s;",
				"}",
				"(PARI) a(n) = my(nb=0); while(n != 1, if (!((n-1)%4), n = 7*n+1, if (!((n+1)%4), n = 7*n-1, n = n/2)); nb++); nb; \\\\ _Michel Marcus_, Aug 06 2018"
			],
			"xref": [
				"Cf. A317640 (7x+-1 function), A006577 (3x+1 equivalent)."
			],
			"keyword": "nonn,easy,hear,look",
			"offset": "1,3",
			"author": "_David Barina_, Aug 06 2018",
			"references": 1,
			"revision": 19,
			"time": "2018-08-19T06:20:54-04:00",
			"created": "2018-08-14T00:48:50-04:00"
		}
	]
}