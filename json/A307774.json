{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A307774",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 307774,
			"data": "0,0,0,0,1,2,2,2,3,2,4,2,3,3,5,4,6,3,5,5,7,5,8,5,8,7,10,7,9,7,10,10,12,9,13,10,14,12,14,12,15,12,16,14,17,14,18,15,20,19,23,18,21,17,22,21,24,18,22,20,26,25,28,24,29,25,31,27,29,25,30,27",
			"name": "Number of partitions of n into 3 parts such that the middle and largest parts are both prime.",
			"comment": [
				"Number of Goldbach partitions of [n-1, n-2, ..., n-floor(n/3)] into two parts whose smallest part is \u003e= i, where i is the index in the list (i=1,2,..). For example, a(11)=4; The numbers 10, 9 and 8 contain a total of 4 Goldbach partitions into two parts whose smallest parts are greater than or equal to 1, 2, and 3 respectively. 10 = 7+3 = 5+5 (3,5 \u003e= 1), 9 = 7+2 (2 \u003e= 2), 8 = 5+3 (3 \u003e= 3)."
			],
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GoldbachPartition.html\"\u003eGoldbach Partition\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Goldbach%27s_conjecture\"\u003eGoldbach's conjecture\u003c/a\u003e",
				"\u003ca href=\"/index/Go#Goldbach\"\u003eIndex entries for sequences related to Goldbach conjecture\u003c/a\u003e",
				"\u003ca href=\"/index/Par#part\"\u003eIndex entries for sequences related to partitions\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{k=1..floor(n/3)} Sum_{i=k..floor((n-k)/2)} A010051(i) * A010051(n-k-i)."
			],
			"example": [
				"Figure 1: The partitions of n into 3 parts for n = 3, 4, ...",
				"                                                          1+1+8",
				"                                                   1+1+7  1+2+7",
				"                                                   1+2+6  1+3+6",
				"                                            1+1+6  1+3+5  1+4+5",
				"                                     1+1+5  1+2+5  1+4+4  2+2+6",
				"                              1+1+4  1+2+4  1+3+4  2+2+5  2+3+5",
				"                       1+1+3  1+2+3  1+3+3  2+2+4  2+3+4  2+4+4",
				"         1+1+1  1+1+2  1+2+2  2+2+2  2+2+3  2+3+3  3+3+3  3+3+4    ...",
				"-----------------------------------------------------------------------",
				"  n  |     3      4      5      6      7      8      9     10      ...",
				"-----------------------------------------------------------------------",
				"a(n) |     0      0      1      2      2      2      3      2      ...",
				"-----------------------------------------------------------------------",
				"- _Wesley Ivan Hurt_, Sep 07 2019"
			],
			"mathematica": [
				"Table[Sum[Sum[(PrimePi[i] - PrimePi[i - 1]) (PrimePi[n - k - i] - PrimePi[n - k - i - 1]), {i, k, Floor[(n - k)/2]}], {k, Floor[n/3]}], {n, 100}]"
			],
			"program": [
				"(PARI) a(n) = sum(k=1, n\\3, sum(i=k, (n-k)\\2, ispseudoprime(i)*ispseudoprime(n-k-i))) \\\\ _Felix Fröhlich_, Apr 29 2019"
			],
			"xref": [
				"Cf. A010051."
			],
			"keyword": "nonn,easy",
			"offset": "1,6",
			"author": "_Wesley Ivan Hurt_, Apr 27 2019",
			"references": 0,
			"revision": 21,
			"time": "2019-09-07T08:16:56-04:00",
			"created": "2019-05-07T23:25:12-04:00"
		}
	]
}