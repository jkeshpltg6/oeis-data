{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A334916",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 334916,
			"data": "0,0,0,6,12,160,324,405,12,8385,36,189,784,32,1656,20,721,25215,80,45,559,2585,5525,323844,30,160,60,90,150,1071,11650,1038448,6275,2669,77,42,2224,324224,1817,2016,252,7425,1593074855,96,5450,192,345906,23541,56",
			"name": "a(n) is the smallest number \u003e 1 whose base n digits yield the original number when added and multiplied left to right; or 0 if no such number exists",
			"comment": [
				"These numbers have been called \"baseless in base n\".",
				"a(n) is divisible by its last base n digit.",
				"The number 8385 = ((((8)8+3)3+8)8+5)5 is known to be the unique baseless number in base 10. Are there number bases n, other than 6 and 10, that have a unique example?",
				"If the term a(107) is not zero, then it is at least a(107) \u003e 107^6 \u003e 1.5*10^12. Is it true that a(n)\u003e0 for all n\u003e3?"
			],
			"link": [
				"Math StackExchange user \"Vepir\" (Matej Veselovac), \u003ca href=\"https://pastebin.com/raw/ydA64hig\"\u003eTerms a(n) \u003c 10^10 for n \u003c 500, and including the 11th record a(73) ~ 2*10^10.\u003c/a\u003e",
				"Math StackExchange, \u003ca href=\"https://math.stackexchange.com/q/3658214/318073\"\u003eDoes every number base have at least one \"baseless number\"?\u003c/a\u003e"
			],
			"formula": [
				"If n is a perfect square, then a(n) = n + sqrt(n). Otherwise, a(n) \u003e 2n."
			],
			"example": [
				"Every number can be written as A = (...((((a)N+b)N+c)N+d)...) where a,b,c,d,... are digits of number A in base N. If we take that expression and replace the \"multiplications by base N\" with \"multiplications by digits a,b,c,d,...\" and also multiply it with the last digit to use up all digits, we get some number A*. If it holds A = A*, then we say number A is a baseless number.",
				"For example, the decimal number base has only one baseless number:",
				".",
				"a(10) = 8385 = ((((8)*10+3)*10+8)*10+5) = ((((8)*8+3)*3+8)*8+5)*5.",
				".",
				"There are at most finitely many baseless numbers for every fixed number base. For example, the number base 4 has exactly three baseless numbers:",
				".",
				"6 = ((1)*4+2)        = ((1)*1+2)*2       = 12_4;",
				"27 = (((1)*4+2)*4+3) = (((1)*1+2)*2+3)*3 = 123_4;",
				"46 = (((2)*4+3)*4+2) = (((2)*2+3)*3+2)*2 = 232_4;",
				".",
				"The smallest of them is 6, hence a(4)=6."
			],
			"program": [
				"(C++)",
				"#include \u003ciostream\u003e",
				"using namespace std;",
				"typedef unsigned long long ull;",
				"int main() {",
				"    for (int b = 1; b\u003e0; b++) {",
				"    for (ull n = b; n\u003e0; n++) {",
				"            if (b\u003c4) {cout \u003c\u003c b \u003c\u003c \" \" \u003c\u003c 0 \u003c\u003c endl;break;}",
				"            if (b==43) {cout \u003c\u003c b \u003c\u003c \" \" \u003c\u003c 1593074855 \u003c\u003c endl;break;}",
				"            if (b==73) {cout \u003c\u003c b \u003c\u003c \" \" \u003c\u003c 25683204625 \u003c\u003c endl;break;}",
				"            ull a=n, m=n;",
				"            while (m != 0) {",
				"                int d = a%b;",
				"                if (d\u003e0 \u0026\u0026 m%d==0) {",
				"                    m /= d; if (m \u003c d) {break;} m -= d; a -= d; a /= b;",
				"                } else {break;}",
				"            }",
				"            if (m==0 \u0026\u0026 a==0){cout \u003c\u003c b \u003c\u003c \" \" \u003c\u003c n \u003c\u003c endl;break;}",
				"    }}",
				"    return 0;",
				"}",
				"(PARI) \\\\ for n\u003e=4",
				"isok(k,n) = {my(d=digits(k, n), s=0); for (i=1, #d, s = (s+d[i])*d[i];); s == k;}",
				"a(n) = {my(k=2); while (!isok(k, n), k++); k;} \\\\ _Michel Marcus_, Jun 18 2020"
			],
			"xref": [
				"Cf. A000290 (perfect squares), A334917 (indices of records)."
			],
			"keyword": "nonn,base,hard",
			"offset": "1,4",
			"author": "_Matej Veselovac_, May 16 2020",
			"references": 1,
			"revision": 15,
			"time": "2020-07-05T12:15:48-04:00",
			"created": "2020-07-05T12:15:48-04:00"
		}
	]
}