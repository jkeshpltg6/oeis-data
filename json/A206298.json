{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A206298",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 206298,
			"data": "1,-2,0,2,-1,-2,4,-2,-2,6,-4,-4,10,-6,-8,16,-9,-10,24,-14,-16,36,-20,-24,53,-30,-32,76,-43,-48,108,-60,-68,150,-84,-92,206,-114,-128,280,-155,-172,376,-208,-228,504,-276,-304,668,-366,-400,878,-480,-524,1148",
			"name": "McKay-Thompson series of class 24C for the Monster group with a(0) = -2.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Essentially the same as A206299, A184990 and A058573. - _R. J. Mathar_, Mar 11 2012"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A206298/b206298.txt\"\u003eTable of n, a(n) for n = -1..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of (1/q) * (phi(-q) * psi(q^4)) / (phi(-q^3) * psi(q^12)) in powers of q where phi(), psi() are Ramanujan theta functions.",
				"Expansion of eta(q)^2 * eta(q^6) * eta(q^8)^2 * eta(q^12) / (eta(q^2) * eta(q^3)^2 * eta(q^4) * eta(q^24)^2) in powers of q.",
				"Euler transform of period 24 sequence [ -2, -1, 0, 0, -2, 0, -2, -2, 0, -1, -2, 0, -2, -1, 0, -2, -2, 0, -2, 0, 0, -1, -2, 0, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (24 t)) = 3 / f(t) where q = exp(2 Pi i t).",
				"a(n) = A058573(n) unless n = 0."
			],
			"example": [
				"1/q - 2 + 2*q^2 - q^3 - 2*q^4 + 4*q^5 - 2*q^6 - 2*q^7 + 6*q^8 - 4*q^9 + ..."
			],
			"mathematica": [
				"QP = QPochhammer; s = QP[q]^2*QP[q^6]*QP[q^8]^2*(QP[q^12] / (QP[q^2]* QP[q^3]^2*QP[q^4]*QP[q^24]^2)) + O[q]^60; CoefficientList[s, q] (* _Jean-François Alcover_, Nov 16 2015, adapted from PARI *)"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c-1, 0, n++; A = x * O(x^n); polcoeff( eta(x + A)^2 * eta(x^6 + A) * eta(x^8 + A)^2 * eta(x^12 + A) / (eta(x^2 + A) * eta(x^3 + A)^2 * eta(x^4 + A) * eta(x^24 + A)^2), n))}"
			],
			"xref": [
				"Cf. A058573."
			],
			"keyword": "sign",
			"offset": "-1,2",
			"author": "_Michael Somos_, Feb 05 2012",
			"references": 2,
			"revision": 24,
			"time": "2021-03-12T22:24:46-05:00",
			"created": "2012-02-05T20:50:08-05:00"
		}
	]
}