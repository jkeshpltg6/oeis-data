{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A259545",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 259545,
			"data": "1,2,4,7,16,34,78,178",
			"name": "Minimum number k such that, for every m \u003e= k, there exists a set of n positive integers whose largest element is m and whose subsets all have distinct arithmetic means.",
			"comment": [
				"Let a set of integers be called \"of different average\" if it satisfies that the arithmetic mean of any two different subsets of it is different (the empty set ignored). E.g., the set {1,2,5} is of different average because 1 != 2 != 5 != (1+2)/2 != (1+5)/2 != (2+5)/2 != (1+2+5)/3.",
				"In order for a set to be of different average it is obvious that all its elements have to be different. Also, if a set is of different average and a constant k is added to all the terms, the resulting set will also be of different average. Because of this, in order to study such sets it is convenient to select an arbitrary first element, say 1. We have by definition a(n)\u003e=A259544(n).",
				"If we already have a different average sequence of (n-1) terms: 1\u003ca_2\u003c...\u003ca_(n-1) and add a term a_n satisfying: (i) The average of any set including a_n is greater than a_(n-1) and (ii) If two sets include a_n, the one having more elements will have a lower average, then any a_n greater than this particular a_n will also satisfy these properties and therefore it provides an upper bound for a(n). It can be easily proved that, by constructing the bound for a(n) recursively this way, the resulting sequence is Sum_{j=1..n-1} j!.",
				"But a much better upper bound can be found by considering the upper bound for A259544(n-1). This is 4^(n-2), and indeed the bound 4^(k-1), for 1\u003c=k\u003c=n-1, applies to all the terms of the sequence. Therefore a different average sequence of n-1 elements exists in which 1=a_1=4^0, a_2\u003c4^1, a3\u003c4_2, etc., until a_(n-1)\u003c4^(n-2). We see that, for this sequence, condition (ii) is more restrictive than (i), in the worst possible scenario for both, and it provides the bound: a(n) \u003c (n-1)4^(n-1)/3, n\u003e=3.",
				"Conjecture: a(n)=A259544(n) only for a finite number of n. Supposing this to be true, what is the order of growth of a(n)-A259544(n)?",
				"Conjecture: lim_{n-\u003einf} a(n)/A259544(n)=1, and indeed the bound \u003c4^(n-1), n\u003e1, is also valid for a(n).",
				"Conjecture: a(n) \u003c A259544(n+1). (This seems obvious, but lacks a proof.)"
			],
			"link": [
				"Javier Múgica, \u003ca href=\"/A259545/a259545.txt\"\u003edifferent average sets up to 219\u003c/a\u003e from where the values of this sequence up to the 8th were obtained (there are much more different average sets.)"
			],
			"xref": [
				"Cf. A259544."
			],
			"keyword": "nonn,more,hard",
			"offset": "1,2",
			"author": "_Javier Múgica_, Jun 30 2015",
			"references": 2,
			"revision": 42,
			"time": "2019-03-15T14:48:52-04:00",
			"created": "2015-08-28T17:42:15-04:00"
		}
	]
}