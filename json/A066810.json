{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A066810",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 66810,
			"data": "0,0,1,7,33,131,473,1611,5281,16867,52905,163835,502769,1532883,4651897,14070379,42456897,127894979,384799049,1156756443,3475250065,10436235955,31330727961,94038321227,282211432673,846835624611",
			"name": "Expansion of x^2/((1-3*x)*(1-2*x)^2).",
			"comment": [
				"Binomial transform of A000295.",
				"a(n) = A112626(n, 2). - _Ross La Haye_, Jan 11 2006",
				"Let Q be a binary relation on the power set P(A) of a set A having n = |A| elements such that for all x,y of P(A), xQy if x is a proper subset of y and |y| - |x| \u003e 1. Then a(n) = |Q|. - _Ross La Haye_, Jan 11 2008",
				"a(n) is the number of n-digit ternary sequences that have at least two 0's. - _Geoffrey Critzer_, Apr 14 2009"
			],
			"link": [
				"Harry J. Smith, \u003ca href=\"/A066810/b066810.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"Ross La Haye, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL12/LaHaye/lahaye5.html\"\u003eBinary Relations on the Power Set of an n-Element Set\u003c/a\u003e, Journal of Integer Sequences, Vol. 12 (2009), Article 09.2.6.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (7,-16,12)."
			],
			"formula": [
				"a(n) = 3^n - 2^n - n*2^(n-1).",
				"From _Ross La Haye_, Apr 26 2006: (Start)",
				"a(n) = A000244(n) - A001792(n).",
				"a(n) = Sum_{k=2..n} binomial(n,k)2^(n-k). (End)",
				"Inverse binomial transform of A086443. - _Ross La Haye_, Apr 29 2006",
				"Convolution of A000244 beginning [0,1,3,9,27,81,...] and A001787. - _Ross La Haye_, Feb 15 2007",
				"From _Geoffrey Critzer_, Apr 14 2009: (Start)",
				"E.g.f.: exp(2*x)*(exp(x) - x - 1).",
				"a(n) = 3*a(n-1) + (n-1)*2^(n-2). (End)"
			],
			"maple": [
				"seq(3^n - 2^n - n*2^(n-1), n=0..30); # _G. C. Greubel_, Nov 18 2019"
			],
			"mathematica": [
				"RecurrenceTable[{a[n]==3*a[n-1] + (n-1) 2^(n-2), a[0]==0}, a, {n, 0, 30}] (* _Geoffrey Critzer_, Apr 14 2009 *)",
				"CoefficientList[Series[x^2/((1-3x)(1-2x)^2), {x, 0, 30}], x] (* _Vincenzo Librandi_, Nov 29 2015 *)"
			],
			"program": [
				"(PARI) for(n=0, 50, write(\"b066810.txt\", n, \" \", 3^n -2^n -n*2^(n-1)) ) \\\\ _Harry J. Smith_, Mar 29 2010",
				"(MAGMA) [3^n-2^n-n*2^(n-1): n in [0..30]]; // _Vincenzo Librandi_, Nov 29 2015",
				"(Sage) [3^n - 2^n - n*2^(n-1) for n in (0..30)] # _G. C. Greubel_, Nov 18 2019",
				"(GAP) List([0..30], n-\u003e 3^n - 2^n - n*2^(n-1)); # _G. C. Greubel_, Nov 18 2019"
			],
			"xref": [
				"Column k=1 of A238858 (with different offset)."
			],
			"keyword": "nonn,easy",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_, Jan 25 2002",
			"ext": [
				"Additional comments from _Ross La Haye_, Sep 27 2005"
			],
			"references": 13,
			"revision": 39,
			"time": "2019-11-18T22:09:50-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}