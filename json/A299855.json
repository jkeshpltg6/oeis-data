{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A299855",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 299855,
			"data": "1,6,-12,60,-384,2772,-21504,175032,-1474560,12748164,-112459776,1008263880,-9160359936,84151254600,-780341870592,7294711613040,-68670084612096,650409360439140,-6193772337561600,59267126633699880,-569566264641454080,5494909312181603160,-53198968510406983680,516695227418183158800,-5033085020810678108160",
			"name": "G.f. C(x)^(1/2) satisfies C(x)^(1/2) - S(x)^(1/2) = 1 such that C'(x)*S(x)^(1/2) = S'(x)*C(x)^(1/2) = 72*x.",
			"comment": [
				"a(n) = -(-1)^n * A244038(n) / (3*n-2) for n\u003e=1."
			],
			"formula": [
				"The functions C = C(x) and S = S(x) satisfy:",
				"(1a) sqrt(C) - sqrt(S) = 1.",
				"(1b) C'*sqrt(S) = S'*sqrt(C) = 72*x.",
				"(1c) C' = 72*x/sqrt(S).",
				"(1d) S' = 72*x/sqrt(C).",
				"Integrals.",
				"(2a) C = 1 + Integral 72*x/sqrt(S) dx.",
				"(2b) S = Integral 72*x/sqrt(C) dx.",
				"(2c) C = 1 + Integral S'*sqrt(C/S) dx.",
				"(2d) S = Integral C'*sqrt(S/C) dx.",
				"Exponentials.",
				"(3a) sqrt(C) = exp( Integral 36*x/(C*sqrt(S)) dx ).",
				"(3b) sqrt(S) = 6*x*exp( Integral 36*x/(S*sqrt(C)) - 1/x dx ).",
				"(3c) C - S = exp( Integral 72*x/(C*sqrt(S) + S*sqrt(C)) dx ).",
				"(3d) C - S = exp( Integral C'*S'/(C*S' + S*C') dx).",
				"Functional equations.",
				"(4a) C = 1/3 - 36*x^2 + (2/3)*C^(3/2).",
				"(4b) S = 36*x^2 - (2/3)*S^(3/2).",
				"Explicit solutions.",
				"(5a) C(x) = 1 + Sum_{n\u003e=1} 2*(-4)^n*binomial(3*n/2,n)/((3*n-2)*(3*n-4)) * x^n.",
				"(5b) S(x) = 36*x^2 + Sum_{n\u003e=3} 18*(-4)^n*(3*n-3)*binomial(3*n/2-2,n)/((3*n-4)*(3*n-6)) * x^n.",
				"(5c) sqrt(C(x)) = 1 + Sum_{n\u003e=1} -(-4)^n * binomial(3*n/2,n)/(3*n-2) * x^n.",
				"Formulas for terms.",
				"a(n) = -(-4)^n * binomial(3*n/2,n) / (3*n-2) for n\u003e=1, with a(0) = 1."
			],
			"example": [
				"G.f.: C(x)^(1/2) = 1 + 6*x - 12*x^2 + 60*x^3 - 384*x^4 + 2772*x^5 - 21504*x^6 + 175032*x^7 - 1474560*x^8 + 12748164*x^9 - 112459776*x^10 + ...",
				"RELATED SERIES.",
				"C(x) = 1 + 12*x + 12*x^2 - 24*x^3 + 96*x^4 - 504*x^5 + 3072*x^6 - 20592*x^7 + 147456*x^8 - 1108536*x^9 + 8650752*x^10 + ...",
				"S(x) = 36*x^2 - 144*x^3 + 864*x^4 - 6048*x^5 + 46080*x^6 - 370656*x^7 + 3096576*x^8 - 26604864*x^9 + 233570304*x^10 + ...",
				"sqrt(S(x)) = 6*x - 12*x^2 + 60*x^3 - 384*x^4 + 2772*x^5 - 21504*x^6 + 175032*x^7 - 1474560*x^8 + 12748164*x^9 - 112459776*x^10 + ...",
				"where C(x)^(1/2) - S(x)^(1/2) = 1",
				"and C'*sqrt(S) = S'*sqrt(C) = 72*x."
			],
			"program": [
				"(PARI) {a(n) = my(C=1, S=x^2); for(i=0, n, C = 1 + intformal( 72*x/sqrt(S +x^3*O(x^n)) ); S = intformal( 72*x/sqrt(C) ) ); polcoeff(sqrt(C), n)}",
				"for(n=0,30,print1(a(n),\", \"))",
				"(PARI) {a(n) = if(n==0,1, -(-4)^n * binomial(3*n/2,n) / (3*n-2) )}",
				"for(n=0,30,print1(a(n),\", \"))"
			],
			"xref": [
				"Cf. A299853, A299854, A244038."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Paul D. Hanna_, Feb 20 2018",
			"references": 2,
			"revision": 11,
			"time": "2019-04-05T12:00:06-04:00",
			"created": "2018-02-20T19:28:49-05:00"
		}
	]
}