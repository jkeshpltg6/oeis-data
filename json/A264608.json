{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A264608",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 264608,
			"data": "1,0,1,1,7,31,175,981,5719,33922,204687,1251460,7737807,48297536,303922983,1926038492,12281450455,78741558512,507301771543,3282586312161,21323849229781,139012437340660,909161626641121,5963576112550771,39223341189188339,258619428254117476,1709124801693650075",
			"name": "Degeneracies of entanglement witness eigenstates for spin 3 particles.",
			"link": [
				"Gheorghe Coserea, \u003ca href=\"/A264608/b264608.txt\"\u003eTable of n, a(n) for n = 0..401\u003c/a\u003e",
				"Eliahu Cohen, Tobias Hansen, Nissan Itzhaki, \u003ca href=\"http://arxiv.org/abs/1511.06623\"\u003eFrom Entanglement Witness to Generalized Catalan Numbers\u003c/a\u003e, arXiv:1511.06623 [quant-ph], 2015.",
				"T. L. Curtright, T. S. Van Kortryk, and C. K. Zachos, \u003ca href=\"https://hal.archives-ouvertes.fr/hal-01345527\"\u003eSpin Multiplicities\u003c/a\u003e, hal-01345527, 2016.",
				"T. L. Curtright, T. S. Van Kortryk, and C. K. Zachos, \u003ca href=\"https://arxiv.org/abs/1607.05849\"\u003eSpin Multiplicities\u003c/a\u003e, arXiv:1607.05849 [hep-th], 2016."
			],
			"formula": [
				"a(n) ~ (1/8^(3/2))*7^n/(sqrt(Pi)*n^(3/2)) * (1-27/(32*n)+O(1/n^2)). - _Thomas Curtright_, Jun 17 2016, updated Jul 26 2016",
				"D-finite with recurrence 3*n*(2*n - 5)*(2*n - 3)*(3*n - 4)*(3*n - 1)*(3*n + 1)*a(n) = (n-1)*(2*n - 5)*(2*n - 1)*(3*n - 4)*(4*n - 3)*(37*n - 38)*a(n-1) + 7*(n-1)*(2*n - 3)*(3*n - 1)*(92*n^3 - 404*n^2 + 509*n - 150)*a(n-2) - 49*(n-2)*(n-1)*(2*n - 5)*(2*n - 1)*(3*n - 4)*(4*n - 3)*a(n-3) - 343*(n-3)*(n-2)*(n-1)*(2*n - 3)*(2*n - 1)*(3*n - 1)*a(n-4). - _Vaclav Kotesovec_, Jun 24 2016",
				"a(n) = (1/Pi)*int((sin(7x)/sin(x))^n*(sin(x))^2,x,0,2Pi). - _Thomas Curtright_, Jun 24 2016",
				"From _Gheorghe Coserea_, Aug 07 2018: (Start)",
				"G.f. y=A(x) satisfies:",
				"0 = x^3*(x + 1)^4*(49*x^2 - 14*x - 27)^2*y^8 + 2*x^3*(x + 1)^3*(35*x + 23)*(49*x^2 - 14*x - 27)*y^6 + x^2*(x + 1)^2*(1421*x^3 + 1652*x^2 + 393*x - 54)*y^4 + x*(x + 1)*(147*x^3 + 175*x^2 + 51*x - 1)*y^2 + x*(2*x + 1)^2.",
				"0 = x^2*(x + 1)*(7*x - 1)*(7*x + 1)*(49*x^2 - 70*x + 5)*(49*x^2 - 14*x - 27)*y''' + x*(1058841*x^7 - 1092455*x^6 - 1212505*x^5 + 627347*x^4 + 222999*x^3 - 6657*x^2 - 5015*x + 405)*y'' + 2*(1058841*x^7 - 1428595*x^6 - 725102*x^5 + 224322*x^4 + 24157*x^3 + 6909*x^2 - 720*x + 60)*y' + 14*x*(50421*x^5 - 84035*x^4 - 19894*x^3 - 2058*x^2 + 665*x - 75)*y.",
				"(End)"
			],
			"example": [
				"A(x) = 1 + x^2 + x^3 + 7*x^4 + 31*x^5 + 175*x^6 + 981*x^7 + ..."
			],
			"mathematica": [
				"a[n_]:= 2/Pi*Integrate[Sqrt[(1-t)/t]*(64t^3-80t^2+24t-1)^n, {t, 0, 1}] (* _Thomas Curtright_, Jun 23 2016 *)",
				"a[n_]:= c[0, n, 3]-c[1, n, 3]; c[j_, n_, s_]:= Sum[(-1)^k*Binomial[n, k]*Binomial[j - (2*s + 1)*k + n + n*s - 1, j - (2*s + 1)*k + n*s], {k, 0, Floor[(j + n*s)/(2*s + 1)]}]; Table[a[n], {n, 0, 20}] (* _Thomas Curtright_, Jul 26 2016 *)",
				"a[n_]:= mult[0, n, 3]; mult[j_,n_,s_]:=Sum[(-1)^(k+1)*Binomial[n,k]*Binomial[n*s+j-(2*s+1)*k+n- 1,n*s+j-(2*s+1)*k+1],{k,0,Floor[(n*s+j+1)/(2*s+1)]}] (* _Thomas Curtright_, Jun 14 2017 *)"
			],
			"program": [
				"(PARI)",
				"N = 26; S = 3;",
				"M = matrix(N+1, N*numerator(S)+1);",
				"Mget(n, j) =  { M[1 + n, 1 + j*denominator(S)] };",
				"Mset(n, j, v) = { M[1 + n, 1 + j*denominator(S)] = v };",
				"Minit() = {",
				"  my(step = 1/denominator(S));",
				"  Mset(0, 0, 1);",
				"  for (n = 1, N, forstep (j = 0, n*S, step,",
				"     my(acc = 0);",
				"     for (k = abs(j-S), min(j+S, (n-1)*S), acc += Mget(n-1, k));",
				"     Mset(n, j, acc)));",
				"};",
				"Minit();",
				"vector(1 + N\\denominator(S), n, Mget((n-1)*denominator(S),0)) \\\\ _Gheorghe Coserea_, Apr 28 2016",
				"(PARI)",
				"seq(N) = {",
				"  my(a = vector(N), s); a[2]=1; a[3]=1; a[4]=7; a[5]=31;",
				"  for (n=6, N, s = ((n-1)*(2*n - 5)*(2*n - 1)*(3*n - 4)*(4*n - 3)*(37*n - 38)*a[n-1] + 7*(n-1)*(2*n - 3)*(3*n - 1)*(92*n^3 - 404*n^2 + 509*n - 150)*a[n-2] - 49*(n-2)*(n-1)*(2*n - 5)*(2*n - 1)*(3*n - 4)*(4*n - 3)*a[n-3] - 343*(n-3)*(n-2)*(n-1)*(2*n - 3)*(2*n - 1)*(3*n - 1)*a[n-4]);",
				"    a[n] = s/(3*n*(2*n - 5)*(2*n - 3)*(3*n - 4)*(3*n - 1)*(3*n + 1)));",
				"  concat(1,a);",
				"};",
				"seq(26) \\\\ _Gheorghe Coserea_, Aug 07 2018"
			],
			"xref": [
				"For spin S = 1/2, 1, 3/2, 2, 5/2, 3, 7/2, 4, 9/2, 5 we get A000108, A005043, A264607, A007043, A272391, this sequence, A272392, A272393, A272394, A272395.",
				"Cf. A348210 (column k=3)."
			],
			"keyword": "nonn",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_, Nov 24 2015",
			"ext": [
				"More terms from _Gheorghe Coserea_, Apr 28 2016"
			],
			"references": 8,
			"revision": 68,
			"time": "2021-10-12T07:54:22-04:00",
			"created": "2015-11-24T13:29:23-05:00"
		}
	]
}