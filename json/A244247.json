{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A244247",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 244247,
			"data": "3,3,11,727,10501,13931,10601,10301,14341,16061,12821,12721,10501,12421,15551,13931,13331,30103,30703,30103,30803,31513,31013,74747,70607,73637,72227,70607,73037,79397,94049,93739,90709,95959,96469,94849",
			"name": "First prime in set of 3 palindromic primes in arithmetic progression ordered by the largest term in the progression.",
			"comment": [
				"This sequence is a subsequence of A002385, the palindromic primes.",
				"The list is ordered based on the highest member of the arithmetic progression.",
				"Some primes generate multiple progressions for different common differences."
			],
			"reference": [
				"Albert H. Beiler, Recreations in the Theory of Numbers, Second Edition, Dower Publications Inc, page 222."
			],
			"link": [
				"Abhiram R Devesh, \u003ca href=\"/A244247/b244247.txt\"\u003eTable of n, a(n) for n = 1..1015\u003c/a\u003e",
				"The Prime Glossary, \u003ca href=\"http://primes.utm.edu/glossary/xpage/PalindromicPrime.html\"\u003ePalindromic Primes\u003c/a\u003e"
			],
			"example": [
				"a(1) = p = 3. For d = 2; [p , p+d, p+2d ] = [3, 5, 7] are in arithmetic progression and are palindromic.",
				"a(2) = p = 3. For d = 4; [p , p+d, p+2d ] = [3, 7, 11] are in arithmetic progression and are palindromic.",
				"a(5) = p = 10501. For d = 1920; [p , p+d, p+2d ] = [10501, 12421, 14341] are in arithmetic progression and are palindromic.",
				"a(13) = p = 10501. For d = 3840; [p , p+d, p+2d ] = [10501, 14341, 18181] are in arithmetic progression and are palindromic.",
				"[3, 7, 11] is an instance with d\u003ep. With first term 110909011, there are 4 instances of common difference d greater than p, yielding 3 palindromic primes in arithmetic progression: d=9914652990, 9916572990, 9925563990, 9928383990. - _Michel Marcus_, Jul 21 2014"
			],
			"program": [
				"(PARI) ispal(n) = eval(concat(Vecrev(Str(n)))) == n;",
				"ispp(p) = isprime(p) \u0026\u0026 ispal(p);",
				"isokppap(p) = {if (ispp(p), for (d=1, p-1, if (ispp(p-d) \u0026\u0026 ispp(p-2*d), return (1));); return (0););} \\\\ _Michel Marcus_, Jul 07 2014"
			],
			"xref": [
				"Cf. A002385 (palindromic primes).",
				"Cf. A120627 (Least positive k such that both prime(n)+k and prime(n)+2k are prime, or 0 if no such k exists)."
			],
			"keyword": "nonn,base",
			"offset": "1,1",
			"author": "_Abhiram R Devesh_, Jun 23 2014",
			"references": 1,
			"revision": 40,
			"time": "2014-08-22T11:20:19-04:00",
			"created": "2014-08-22T11:20:19-04:00"
		}
	]
}