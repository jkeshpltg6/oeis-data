{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A335973",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 335973,
			"data": "13,24,35,46,57,68,791,202,14,25,36,47,58,691,203,15,26,37,48,591,204,16,27,38,491,205,17,28,391,206,18,291,207,181,208,191,302,131,2002,135,461,303,141,304,151,305,161,306,171,307,182,31,2003,142,308,192,41,2004,152,313,241,2005,162,51,402",
			"name": "The Locomotive Pushing or Pulling its Wagons sequence (see comments for definition).",
			"comment": [
				"a(1) is the locomotive; a(2), a(3), a(4),... a(n),... are the wagons. To hook a wagon both to its predecessor (on the left) and successor (on the right) you must be able to insert the leftmost digit of a(n) between the last two digits of a(n-1) AND to insert the rightmost digit of a(n) between the first two digits of a(n+1). In mathematical terms, the value of the leftmost digit of a(n) must be between (not equal to) the last two digits of a(n-1) AND the value of the rightmost digit of a(n) must be between (not equal to) the first and the second digit of a(n+1). This is the lexicographically earliest sequence of distinct positive terms with this property.",
				"a(n) cannot end in 0 or 9. - _Michael S. Branicky_, Dec 14 2020"
			],
			"link": [
				"Carole Dubois, \u003ca href=\"/A335973/b335973.txt\"\u003eTable of n, a(n) for n = 1..5006\u003c/a\u003e"
			],
			"example": [
				"The sequence starts with 13, 24, 35, 46, 57, 68, 791, 202, 14,...",
				"a(1) = 13 as there is no earliest possible locomotive;",
				"a(2) = 24 starts with 2 and 4: now 2 \u003c 3 \u003c 4 [3 being the rightmost digit of a(1)] AND 3 \u003c 4 \u003c 5 [4 being the rightmost digit of a(2), 3 and 5 being the first two digits of a(3);",
				"a(3) = 35 starts with 3 and 5: now 3 \u003c 4 \u003c 5 [4 being the rightmost digit of a(2)] AND 4 \u003c 5 \u003c 6 [5 being the rightmost digit of a(3), 4 and 6 being the first two digits of a(4);",
				"a(4) = 46 starts with 4 and 6: now 4 \u003c 5 \u003c 6 [5 being the rightmost digit of a(3)] AND 5 \u003c 6 \u003c 7 [6 being the rightmost digit of a(4), 5 and 7 being the first two digits of a(5);",
				"a(5) = 57 starts with 5 and 7: now 5 \u003c 6 \u003c 7 [6 being the rightmost digit of a(4)] AND 6 \u003c 7 \u003c 8 [7 being the rightmost digit of a(5), 6 and 8 being the first two digits of a(6);",
				"a(6) = 68 starts with 6 and 8: now 6 \u003c 7 \u003c 8 [7 being the rightmost digit of a(5)] AND 7 \u003c 8 \u003c 9 [8 being the rightmost digit of a(6), 7 and 9 being the first two digits of a(7);",
				"a(7) = 791 starts with 7 and 9: now 7 \u003c 8 \u003c 9 [8 being the rightmost digit of a(6)] AND 2 \u003e 1 \u003e 0 [1 being the rightmost digit of a(7); 2 and 0 being the first two digits of a(8); etc."
			],
			"program": [
				"(Python)",
				"def between(i, j, k):",
				"  return i \u003c j \u003c k or i \u003e j \u003e k",
				"def dead_end(k):",
				"  rest, last = divmod(k, 10)",
				"  if last in {0, 9}: return True",
				"  return abs(rest%10 - last) \u003c= 1",
				"def aupto(n, seed=13):",
				"  train, used = [seed], {seed}",
				"  for n in range(2, n+1):",
				"    caboose = train[-1]",
				"    cabbody, cabhook = divmod(caboose, 10)",
				"    h1, h2 = sorted([cabbody%10, cabhook])",
				"    hooks = set(range(h1+1, h2))",
				"    pow10 = 10",
				"    an = min(hooks)*pow10",
				"    while an in used or dead_end(an): an += 1",
				"    hook = an//pow10",
				"    while True:",
				"      if hook in hooks:",
				"        if between(hook, cabhook, an//(pow10//10)%10):",
				"          train.append(an)",
				"          used.add(an)",
				"          break",
				"      else: pow10 *= 10",
				"      an = max(an+1, min(hooks)*pow10)",
				"      while an in used or dead_end(an): an += 1",
				"      hook = an//pow10",
				"  return train    # use train[n-1] for a(n)",
				"print(aupto(65))  # _Michael S. Branicky_, Dec 14 2020"
			],
			"xref": [
				"Cf. A335971 (locomotive pulling to the left) and A335972 (locomotive pushing to the right)."
			],
			"keyword": "base,nonn",
			"offset": "1,1",
			"author": "_Eric Angelini_ and _Carole Dubois_, Jul 03 2020",
			"references": 4,
			"revision": 14,
			"time": "2020-12-14T01:36:18-05:00",
			"created": "2020-07-14T23:40:12-04:00"
		}
	]
}