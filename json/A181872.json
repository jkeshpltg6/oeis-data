{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A181872",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 181872,
			"data": "0,1,0,1,-3,0,1,-1,1,5,0,-5,0,1,-3,0,1,-7,0,7,0,-7,0,1,-1,0,1,-3,0,9,0,-3,0,1,5,0,-5,0,1,-11,0,55,0,-77,0,11,0,-11,0,1,-1,1,13,0,-91,0,91,0,-39,0,65,0,-13,0,1,-7,0,7,0,-7,0,1,1,0,-1,0,7,0,-7,0,1,1,0,-1,0,1,17,0,-51,0,357,0,-561,0,935,0,-221,0,119,0,-17,0,1,-3,0,9,0,-3,0,1,-19,0,285,0,-627,0,627,0,-2717,0,1729,0,-665,0,19,0,-19,0,1,-1,1,1,1,0,-1,0,15,0,-39,0,11,0,-11,0,1,-11,0,55,0,-77,0,11,0,-11,0,1",
			"name": "Numerators of coefficient array for minimal polynomials of sin(2*Pi/n). Rising powers of x.",
			"comment": [
				"The corresponding denominator array is given in A181873(n,m).",
				"The sequence of row lengths of this array is A093819(n)+1: [2, 2, 3, 2, 5, 3, 7, 3, 7, 5, 11, ...].",
				"The minimal polynomial of the algebraic number sin(2*Pi/n), n \u003e= 1, is here called Pi(n,x) := Sum_{m=0..d(n)} r(n,m)*x^m with the degree sequence d(n):=A093819(n), and the rationals r(n):=a(n,m)/b(n,m) with b(n,m):=A181873(n,m).",
				"  See the Niven reference, p. 28, for the definition of 'minimal polynomial of an algebraic number'.",
				"  Minimal polynomials are irreducible.",
				"  The minimal polynomials of sin(2*Pi/n) are treated, e.g., in the Lehmer, Niven and Watkins-Zeitlin references.",
				"The minimal polynomials Pi(n,x) of sin(2*Pi/n) are found from Psi(c(n),x), where Psi(m,x) is the minimal polynomial of cos(2*Pi/m), and",
				"  c(n):= denominator(|(4-n)/(4*n)|) = A178182(n).",
				"For the regular n-gon inscribed in the unit circle the area is n*sin(2*Pi/n). See the remark by _Jack W Grahl_ under A093819.",
				"S. Beslin and V. de Angelis (see the reference) give an explicit formula for the (integer) minimal polynomial of sin(2*Pi/p), called S_p(x), and cos(2*Pi/p), called C_p(x),for odd prime p, p=2k+1, with the results:",
				"  S_p(x) = Sum_{l=0..k} ((-1)^l)*binomial(p,2*l+1)*(1-x^2)^(k-l)*x^(2*l), and C_p(x) = S_p(sqrt((1-x)/2)), where S_p(x), with leading term ((-2)^k))*x^(p-1), checks with((-2)^k)*Pi(p,x). - _Wolfdieter Lang_, Feb 28 2011",
				"The zeros of Pi(n, x) result from those of the minimal polynomial Psi(n, x) of cos(2*Pi/n), and they are cos(2*Pi*k/n), for k = 0, ..., floor(c(n)/2), with c(n) = A178182(n), and restriction gcd(k, c(n)) = 1, for n \u003e= 1. There are d(n) = A093819(n) such zeros. - _Wolfdieter Lang_, Oct 30 2019"
			],
			"reference": [
				"I. Niven, Irrational Numbers, The Math. Assoc. of America, second printing, 1963, distributed by John Wiley and Sons."
			],
			"link": [
				"S. Beslin and V. de Angelis, \u003ca href=\"https://www.jstor.org/stable/3219105\"\u003eThe minimal Polynomials of sin(2Pi/p) and cos(2Pi/p)\u003c/a\u003e, Mathematics Mag. 77.2 (2004) 146-9.",
				"Wolfdieter Lang, \u003ca href=\"/A181872/a181872_1.pdf\"\u003eMinimal polynomials for sin(2Pi/n)\u003c/a\u003e.",
				"D. H. Lehmer, \u003ca href=\"http://www.jstor.org/stable/2301023\"\u003eA Note on Trigonometric Algebraic Numbers\u003c/a\u003e, Am. Math. Monthly 40 (3) (1933) 165-6.",
				"W. Watkins and J. Zeitlin, \u003ca href=\"http://www.jstor.org/stable/2324301\"\u003eThe Minimal Polynomial of cos(2Pi/n)\u003c/a\u003e, Am. Math. Monthly 100,5 (1993) 471-4."
			],
			"formula": [
				"a(n,m) = numerator([x^m]Pi(n,x)), n\u003e=1, m=0..A093819(n). For Pi(n,x) see the comments.",
				"The minimal polynomial Pi(n,x) = Product_{k=0..floor(c(n)/2), gcd(k, c(n)) = 1}, x - cos(2*Pi*k/c(n)), for n \u003e= 1. - _Wolfdieter Lang_, Oct 30 2019"
			],
			"example": [
				"Triangle begins:",
				"  [0, 1],",
				"  [0, 1],",
				"  [-3, 0, 1],",
				"  [-1, 1],",
				"  [5, 0, -5, 0, 1],",
				"  [-3, 0, 1],",
				"  [-7, 0, 7, 0, -7, 0, 1],",
				"  [-1, 0, 1],",
				"  [-3, 0, 9, 0, -3, 0, 1],",
				"  [5, 0, -5, 0, 1],",
				"  ...",
				"The rational coefficients r(n,m) start like:",
				"  [0, 1],",
				"  [0, 1],",
				"  [-3/4, 0, 1],",
				"  [-1, 1],",
				"  [5/16, 0, -5/4, 0, 1],",
				"  [-3/4, 0, 1],",
				"  [-7/64, 0, 7/8, 0, -7/4, 0, 1],",
				"  [-1/2, 0, 1],",
				"  [-3/64, 0, 9/16, 0, -3/2, 0, 1],",
				"  ...",
				"Pi(6,n) = Psi(c(6),x) = Psi(12,x) = x^2-3/4."
			],
			"mathematica": [
				"p[n_, x_] := MinimalPolynomial[ Sin[2 Pi/n], x]; Flatten[ Numerator[ Table[ coes = CoefficientList[ p[n, x], x]; coes / Last[coes], {n, 1, 22}]]] (* _Jean-François Alcover_, Nov 07 2011 *)"
			],
			"xref": [
				"Cf. A181875, A181876 (minimal polynomials of cos(2*Pi/n))."
			],
			"keyword": "sign,easy,frac,tabf",
			"offset": "1,5",
			"author": "_Wolfdieter Lang_, Jan 13 2011",
			"references": 8,
			"revision": 42,
			"time": "2021-04-29T12:03:46-04:00",
			"created": "2010-11-15T12:59:49-05:00"
		}
	]
}