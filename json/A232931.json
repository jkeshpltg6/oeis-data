{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A232931",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 232931,
			"data": "2,3,5,2,3,2,7,5,2,5,2,7,3,3,2,3,5,13,2,3,2,5,7,2,2,5,3,3,2,5,2,3,11,2,3,11,7,7,2,7,3,3,2,7,2,3,11,2,3,2,5,5,2,5,2,11,3,3,5,2,7,11,2,3,2,5,7,2,2,5,3,3,2,7,3,11,2,3,7,7,5,2,5,2,13,3,3,2,2,3,2,3,2,5,5,11,2,7,5,3,3,5,2,3,13,5,2,3,2,17,2,2,7,3,3,2,13,2,5,2,3,5,7,5,2,5,2,11,3,2,5,2,3,7,2,3,2,17,5,7,2,7,2,5,3,3,7,2,3,7,5,2,3",
			"name": "The least positive integer k such that Kronecker(D/k) = -1 where D runs through all positive fundamental discriminants (A003658).",
			"comment": [
				"From _Jianing Song_, Jan 30 2019: (Start)",
				"a(n) is necessarily prime. Otherwise, if a(n) is not prime, we have (D/p) = 0 or 1 for all prime divisors p of a(n), so (D/a(n)) must be 0 or 1 too, a contradiction.",
				"a(n) is the least inert prime in the real quadratic field with discriminant D, D = A003658(n). (End)"
			],
			"link": [
				"Charles R Greathouse IV, \u003ca href=\"/A232931/b232931.txt\"\u003eTable of n, a(n) for n = 2..10000\u003c/a\u003e",
				"S. R. Finch, \u003ca href=\"/A232927/a232927.pdf\"\u003eAverage least nonresidues\u003c/a\u003e, December 4, 2013. [Cached copy, with permission of the author]",
				"Andrew Granville, R. A. Mollin and H. C. Williams, \u003ca href=\"https://doi.org/10.4153/CJM-2000-017-5\"\u003eAn upper bound on the least inert prime in a real quadratic field\u003c/a\u003e, Canad. J. Math. 52:2 (2000), pp. 369-380.",
				"P. Pollack, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2011.12.015\"\u003eThe average least quadratic nonresidue modulo m and other variations on a theme of Erdős\u003c/a\u003e, J. Number Theory 132 (2012) 1185-1202.",
				"Enrique Treviño, \u003ca href=\"http://campus.lakeforest.edu/trevino/LeastInertPrimeMCOM.pdf\"\u003eThe least inert prime in a real quadratic field\u003c/a\u003e, Mathematics of Computation 81:279 (2012), pp. 1777-1797. See also his \u003ca href=\"http://campus.lakeforest.edu/trevino/LeastInertPrimeTalkUSC.pdf\"\u003ePANTS 2010 talk\u003c/a\u003e."
			],
			"formula": [
				"With D = A003658(n): Mollin conjectured, and Granville, Mollin, \u0026 Williams prove, that for n \u003e 1128, a(n) \u003c= D^0.5 / 2. Treviño proves that for n \u003e 484, a(n) \u003c= D^0.45. Asymptotically the exponent is less than 0.16. - _Charles R Greathouse IV_, Apr 23 2014",
				"a(n) = A092419(A003658(n) - floor(sqrt(A003658(n))), n \u003e= 2. - _Jianing Song_, Jan 30 2019"
			],
			"example": [
				"A003658(3) = 8, (8/3) = -1 and (8/2) = 0, so a(3) = 3."
			],
			"mathematica": [
				"nMax = 200; A003658 = Select[Range[4nMax], NumberFieldDiscriminant[Sqrt[#]] == #\u0026]; f[d_] := For[k = 1, True, k++, If[FreeQ[{0, 1}, KroneckerSymbol[d, k]], Return[k]]]; a[n_] := f[A003658[[n]]]; Table[a[n], {n, 2, nMax}] (* _Jean-François Alcover_, Nov 05 2016 *)"
			],
			"program": [
				"(PARI) lp(D)=forprime(p=2,,if(kronecker(D,p)\u003c0,return(p)))",
				"for(n=5,1e3,if(isfundamental(n),print1(lp(n)\", \"))) \\\\ _Charles R Greathouse IV_, Apr 23 2014"
			],
			"xref": [
				"Cf. A003658, A092419, A241482."
			],
			"keyword": "nonn",
			"offset": "2,1",
			"author": "_Steven Finch_, Dec 02 2013",
			"ext": [
				"Name simplified by _Jianing Song_, Jan 30 2019"
			],
			"references": 10,
			"revision": 40,
			"time": "2019-02-15T10:21:11-05:00",
			"created": "2013-12-02T16:32:56-05:00"
		}
	]
}