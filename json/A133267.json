{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A133267",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 133267,
			"data": "2,1,4,8,24,56,156,400,1092,2928,8052,22080,61320,170664,478288,1344800,3798240,10760568,30585828,87166656,249055976,713197848,2046590844,5883926400,16945772184,48881973840,141214767876,408513980160",
			"name": "Number of Lyndon words on {1, 2, 3} with an even number of 1's.",
			"comment": [
				"A Lyndon word is the aperiodic necklace representative which is lexicographically earliest among its cyclic shifts. Thus we can apply the fixed density formulas: L_k(n,d)=sum L(n-d, n_1,..., n_(k-1)); n_1+...+n_(k-1)=d where L(n_0, n_1,...,n_(k-1))=(1/n)sum mu(j)*[(n/j)!/((n_0/j)!(n_1/j)!...(n_(k-1)/j)!)]; j|gcd(n_0, n_1,...,n_(k-1)). For this sequence, sum over n_0=even. Alternatively, a(n)=(sum mu(d)*3^(n/d)/n; d|n) - (sum mu(d)*(3^(n/d)-1)/(2n); d|n, d odd)."
			],
			"reference": [
				"M. Lothaire, Combinatorics on Words, Addison-Wesley, Reading, MA, 1983."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A133267/b133267.txt\"\u003eTable of n, a(n) for n = 1..650\u003c/a\u003e",
				"E. N. Gilbert and J. Riordan, \u003ca href=\"http://projecteuclid.org/euclid.ijm/1255631587\"\u003eSymmetry types of periodic sequences\u003c/a\u003e, Illinois J. Math., 5 (1961), 657-665.",
				"F. Ruskey, \u003ca href=\"http://combos.org/necklace\"\u003eNecklaces, Lyndon words, De Bruijn sequences, etc.\u003c/a\u003e",
				"F. Ruskey and J. Sawada, \u003ca href=\"http://dx.doi.org/10.1137/S0097539798344112\"\u003eAn Efficient Algorithm for Generating Necklaces with Fixed Density\u003c/a\u003e, SIAM J. Computing, 29 (1999) 671-684.",
				"F. Ruskey, \u003ca href=\"/A000011/a000011.pdf\"\u003eNecklaces, Lyndon words, De Bruijn sequences, etc.\u003c/a\u003e [Cached copy, with permission, pdf format only]",
				"M. Zabrocki, \u003ca href=\"http://garsia.math.yorku.ca/~zabrocki/math5020y0708/\"\u003eCourse website\u003c/a\u003e"
			],
			"formula": [
				"a(1)=2; for n\u003e1, if n=2^k for some k, then a(n)=((3^(n/2)-1)^2)/(2n). Otherwise, if n=even then a(n)=sum mu(d)*(3^(n/d)-2*3^(n/(2d))/(2n); d|n, d odd. If n=odd then a(n)=sum mu(d)*(3^(n/d)-1)/(2n); d|n, d odd."
			],
			"example": [
				"For n=3, out of 8 possible Lyndon words: 112, 113, 122, 123, 132, 133, 223, 233, only the first two and the last two have an even number of 1's. Thus a(3) = 4."
			],
			"maple": [
				"with(numtheory): a:= n-\u003e add(mobius(d) *3^(n/d), d=divisors(n))/n -add(mobius(d) *(3^(n/d)-1), d=select(x-\u003e irem(x, 2)=1, divisors(n)))/ (2*n): seq(a(n), n=1..30);  # _Alois P. Heinz_, Jul 29 2011"
			],
			"mathematica": [
				"a[n_] := DivisorSum[n, MoebiusMu[#]*(3^(n/#) - (1/2)*Boole[OddQ[#]]*(3^(n/#)-1))\u0026]/n; Table[a[n], {n, 1, 30}] (* _Jean-François Alcover_, Mar 21 2017, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(PARI) a133267(n) = sumdiv(n, d, moebius(d)*3^(n/d)/n - if (d%2, moebius(d)*(3^(n/d)-1)/(2*n))); \\\\ _Michel Marcus_, May 17 2018"
			],
			"xref": [
				"Cf. A006575, A027376."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "Jennifer Woodcock (jennifer.woodcock(AT)ugdsb.on.ca), Jan 03 2008",
			"references": 5,
			"revision": 26,
			"time": "2019-04-30T08:24:32-04:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}