{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A246685",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 246685,
			"data": "1,1,1,3,1,1,3,5,1,1,1,3,3,3,5,17,1,1,1,3,1,1,3,5,3,3,3,9,5,5,17,257,1,1,1,3,1,1,3,5,1,1,1,3,3,3,5,17,3,3,3,9,3,3,9,15,5,5,5,15,17,17,257,65537,1,1,1,3,1,1,3,5,1,1,1,3,3,3,5,17,1,1,1,3,1,1,3,5,3,3,3,9,5,5,17,257",
			"name": "Run Length Transform of sequence 1, 3, 5, 17, 257, 65537, ... (1 followed by Fermat numbers).",
			"comment": [
				"The Run Length Transform of a sequence {S(n), n\u003e=0} is defined to be the sequence {T(n), n\u003e=0} given by T(n) = Product_i S(i), where i runs through the lengths of runs of 1's in the binary expansion of n. E.g. 19 is 10011 in binary, which has two runs of 1's, of lengths 1 and 2. So T(19) = S(1)*S(2). T(0)=1 (the empty product).",
				"This sequence is obtained by applying Run Length Transform to sequence b = 1, 3, 5, 17, 257, 65537, ... (1 followed by Fermat numbers, with b(1) = 1, b(2) = 3, b(3) = 5, ..., b(n) = 2^(2^(n-2)) + 1 for n \u003e= 2)."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A246685/b246685.txt\"\u003eTable of n, a(n) for n = 0..1024\u003c/a\u003e"
			],
			"example": [
				"115 is '1110011' in binary. The run lengths of 1-runs are 2 and 3, thus we multiply the second and the third elements of the sequence 1, 3, 5, 17, 257, 65537, ... to get a(115) = 3*5 = 15."
			],
			"mathematica": [
				"f[n_] := Switch[n, 0|1, 1, _, 2^(2^(n-2))+1]; Table[Times @@ (f[Length[#]] \u0026) /@ Select[s = Split[IntegerDigits[n, 2]], #[[1]] == 1\u0026], {n, 0, 95}] (* _Jean-François Alcover_, Jul 11 2017 *)"
			],
			"program": [
				"(MIT/GNU Scheme)",
				"(define (A246685 n) (fold-left (lambda (a r) (if (= 1 r) a (* a (A000215 (- r 2))))) 1 (bisect (reverse (binexp-\u003eruncount1list n)) (- 1 (modulo n 2)))))",
				"(define (A000215 n) (+ 1 (A000079 (A000079 n))))",
				"(define (A000079 n) (expt 2 n))",
				";; Other functions as in A227349."
			],
			"xref": [
				"Cf. A003714 (gives the positions of ones).",
				"Cf. A000215.",
				"A001316 is obtained when the same transformation is applied to A000079, the powers of two. Cf. also A001317.",
				"Run Length Transforms of other sequences: A071053, A227349, A246588, A246595, A246596, A246660, A246661, A246674, A247282."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Antti Karttunen_, Sep 22 2014",
			"references": 3,
			"revision": 17,
			"time": "2017-07-12T03:19:56-04:00",
			"created": "2014-09-23T11:02:45-04:00"
		}
	]
}