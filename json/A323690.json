{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A323690",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 323690,
			"data": "1,2,1,-8,0,18,11,-16,-44,0,36,-12,35,112,56,-144,-260,-88,48,-20,95,504,636,288,-578,-1016,-292,-216,-624,-210,73,-64,622,2892,4062,672,-2916,-1508,1010,-80,-4894,-8618,-6809,-2728,46,6740,17502,19104,7040,-4820,-3898,1488,1080,-192,96,-8792,-37129,-57508,-40242,-12120,15902,62044,91082,41584,-51684,-50196,59462,122896,85616",
			"name": "G.f.: Sum_{n\u003e=0} (x^(n+1) + i)^n / (1 + i*x^n)^(n+1), in which the constant term is taken to be 1.",
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A323690/b323690.txt\"\u003eTable of n, a(n) for n = 0..10100\u003c/a\u003e"
			],
			"formula": [
				"G.f.: i * Sum_{n\u003e=0} (1 + i*x^(n+1))^n / (x^n + i)^(n+1), in which the constant term is taken to be 1."
			],
			"example": [
				"G.f.: A(x) = 1 + 2*x + x^2 - 8*x^3 + 18*x^5 + 11*x^6 - 16*x^7 - 44*x^8 + 36*x^10 - 12*x^11 + 35*x^12 + 112*x^13 + 56*x^14 - 144*x^15 - 260*x^16 - 88*x^17 + 48*x^18 - 20*x^19 + 95*x^20 + 504*x^21 + 636*x^22 + 288*x^23 - 578*x^24 - 1016*x^25 - 292*x^26 - 216*x^27 - 624*x^28 - 210*x^29 + 73*x^30 + ...",
				"which equals the following sum when expanded as a power series in x:",
				"A(x) = 1/(1+i) + (x^2 + i)/(1 + i*x)^2 + (x^3 + i)^2/(1 + i*x^2)^3 + (x^4 + i)^3/(1 + i*x^3)^4 + (x^5 + i)^4/(1 + i*x^4)^5 + (x^6 + i)^5/(1 + i*x^5)^6 + (x^7 + i)^6/(1 + i*x^6)^7 + (x^8 + i)^7/(1 + i*x^7)^8 + (x^9 + i)^8/(1 + i*x^8)^9 + ...",
				"where the coefficient of x^0 is taken to be 1.",
				"The limit of the following sum expands into a power series in x with only real coefficients after the initial coefficient of x^0:",
				"S(N) = Sum_{n=0..N} (x^(n+1) + i)^n / (1 + i*x^n)^(n+1) = i^N/(1+i) + 2*x + x^2 - 8*x^3 + 18*x^5 + 11*x^6 - 16*x^7 - 44*x^8 + 36*x^10 - 12*x^11 + 35*x^12 + ...",
				"here, we ignore the coefficient of x^0 and set a(0) = 1.",
				"TRIANGLE FORM.",
				"This sequence may be written as a triangle that begins",
				"1, 2;",
				"1, -8, 0, 18;",
				"11, -16, -44, 0, 36, -12;",
				"35, 112, 56, -144, -260, -88, 48, -20;",
				"95, 504, 636, 288, -578, -1016, -292, -216, -624, -210;",
				"73, -64, 622, 2892, 4062, 672, -2916, -1508, 1010, -80, -4894, -8618;",
				"-6809, -2728, 46, 6740, 17502, 19104, 7040, -4820, -3898, 1488, 1080, -192, 96, -8792;",
				"-37129, -57508, -40242, -12120, 15902, 62044, 91082, 41584, -51684, -50196, 59462, 122896, 85616, 35300, 18822, 12072; ...",
				"in which the first column consists of all the odd terms in this sequence."
			],
			"program": [
				"(PARI) {a(n) = my(SUM = sum(m=0,n, (x^(m+1) + I +x*O(x^n))^m / (1 + I*x^m +x*O(x^n))^(m+1) ) ); polcoeff(1 + SUM - I^n/(1+I),n)}",
				"for(n=0,100,print1(a(n),\", \"))"
			],
			"xref": [
				"Cf. A323691, A323557, A323689."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Paul D. Hanna_, Feb 13 2019",
			"references": 3,
			"revision": 22,
			"time": "2019-02-14T21:05:22-05:00",
			"created": "2019-02-13T12:16:46-05:00"
		}
	]
}