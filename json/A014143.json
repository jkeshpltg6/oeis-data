{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A014143",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 14143,
			"data": "1,4,12,34,98,294,919,2974,9891,33604,116103,406614,1440025,5147876,18550572,67310938,245716094,901759950,3325066996,12312494462,45766188948,170702447074,638698318850,2396598337950",
			"name": "Partial sums of A014138.",
			"comment": [
				"Self-convolution of A014137. Column in triangle A200965. - _Philippe Deléham_, Jan 24 2014",
				"For n \u003e= 2, a(n-2) is the number of 021-avoiding ascent sequences of length n with exactly one occurrence of the consecutive pattern 01. For example, with n=3, a(1)=4 counts 001, 010, 011, 012. - _David Callan_, Nov 13 2019"
			],
			"reference": [
				"Silvia Heubach and Toufik Mansour, Combinatorics of Compositions and Words, CRC Press, 2010."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A014143/b014143.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"S. Kitaev, J. Remmel and M. Tiefenbruck, \u003ca href=\"http://arxiv.org/abs/1201.6243\"\u003eMarked mesh patterns in 132-avoiding permutations I\u003c/a\u003e, arXiv preprint arXiv:1201.6243 [math.CO], 2012. - From _N. J. A. Sloane_, May 09 2012 [An early version on the arXiv had A014043 instead of A014143]",
				"Sergey Kitaev, Jeffrey Remmel, Mark Tiefenbruck, \u003ca href=\"http://www.emis.de/journals/INTEGERS/papers/p16/p16.Abstract.html\"\u003eQuadrant Marked Mesh Patterns in 132-Avoiding Permutations II\u003c/a\u003e, Electronic Journal of Combinatorial Number Theory, Volume 15 #A16. (\u003ca href=\"http://arxiv.org/abs/1302.2274\"\u003earXiv\u003c/a\u003e, arXiv:1302.2274 [math.CO], 2013)"
			],
			"formula": [
				"G.f.: (1-2*z-sqrt(1-4*z))/(2*z^2*(1-z)^2). - _Emeric Deutsch_, Jan 27 2003",
				"Recurrence: (n+2)*a(n) = 6*(n+1)*a(n-1) - 3*(3*n+2)*a(n-2) + 2*(2*n+1)*a(n-3). - _Vaclav Kotesovec_, Oct 07 2012",
				"a(n) ~ 2^(2n+6)/(9*sqrt(Pi)*n^(3/2)). - _Vaclav Kotesovec_, Oct 07 2012",
				"a(n) = 2 * Sum_{k=0..n} Sum_{j=0..k} C(2*j+1,j)/(j+2). - _Vaclav Kotesovec_, Oct 27 2012"
			],
			"mathematica": [
				"Table[SeriesCoefficient[(1-2*x-Sqrt[1-4*x])/(2*x^2*(1-x)^2),{x,0,n}],{n,0,20}] (* _Vaclav Kotesovec_, Oct 07 2012 *)",
				"Table[2*Sum[Sum[Binomial[2*j+1,j]/(j+2),{j,0,k}],{k,0,n}],{n,0,20}] (* _Vaclav Kotesovec_, Oct 27 2012 *)"
			],
			"program": [
				"(PARI) x='x+O('x^66); Vec((1-2*x-sqrt(1-4*x))/(2*x^2*(1-x)^2)) \\\\ _Joerg Arndt_, May 04 2013"
			],
			"xref": [
				"Cf. A014137, A200965."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 11,
			"revision": 38,
			"time": "2019-11-14T05:35:12-05:00",
			"created": "1998-06-14T03:00:00-04:00"
		}
	]
}