{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A050603",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 50603,
			"data": "1,1,2,2,1,1,3,3,1,1,2,2,1,1,4,4,1,1,2,2,1,1,3,3,1,1,2,2,1,1,5,5,1,1,2,2,1,1,3,3,1,1,2,2,1,1,4,4,1,1,2,2,1,1,3,3,1,1,2,2,1,1,6,6,1,1,2,2,1,1,3,3,1,1,2,2,1,1,4,4,1,1,2,2,1,1,3,3,1,1,2,2,1,1,5,5,1,1,2,2,1,1,3,3,1,1,2,2,1,1,4",
			"name": "A001511 with every term repeated.",
			"comment": [
				"Column 2 of A050600: a(n) = add1c(n,2).",
				"Absolute values of A094267.",
				"Consider the Collatz (or 3x+1) problem and the iterative sequence c(k) where c(0)=n is a positive integer and c(k+1)=c(k)/2 if c(k) is even, c(k+1)=(3*c(k)+1)/2 if c(k) is odd. Then a(n) is the minimum number of iterations in order to have c(a(n)) odd if n is even or c(a(n)) even if n is odd. - _Benoit Cloitre_, Nov 16 2001"
			],
			"link": [
				"James Spahlinger, \u003ca href=\"/A050603/b050603.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Cristian Cobeli, Mihai Prunescu, Alexandru Zaharescu, \u003ca href=\"http://arxiv.org/abs/1511.04315\"\u003eA growth model based on the arithmetic Z-game\u003c/a\u003e, arXiv:1511.04315 [math.NT], 2015.",
				"Francis Laclé, \u003ca href=\"https://hal.archives-ouvertes.fr/hal-03201180v2\"\u003e2-adic parity explorations of the 3n+ 1 problem\u003c/a\u003e, hal-03201180v2 [cs.DM], 2021."
			],
			"formula": [
				"Equals A053398(2, n).",
				"G.f.: (1+x)/x^2 * Sum(k\u003e=1, x^(2^k)/(1-x^(2^k))). - _Ralf Stephan_, Apr 12 2002",
				"a(n) = A136480(n+1). - _Reinhard Zumkeller_, Dec 31 2007",
				"a(n) = A007814(n + 2 - n mod 2). - _James Spahlinger_, Oct 11 2013, corrected by _Charles R Greathouse IV_, Oct 14 2013",
				"a(2n) = a(2n+1). 1 \u003c= a(n) \u003c= log_2(n+2). - _Charles R Greathouse IV_, Oct 14 2013",
				"a(n)=A007814(n+1)+A007814(n+2).",
				"a(n) = (-1)^n * A094267(n). - _Michael Somos_, May 11 2014"
			],
			"mathematica": [
				"With[{c=Table[Position[Reverse[IntegerDigits[n,2]],1,1,1],{n,110}]// Flatten}, Riffle[c,c]] (* _Harvey P. Dale_, Dec 06 2018 *)"
			],
			"program": [
				"(PARI) a(n)=valuation(n+2-n%2,2) \\\\ _Charles R Greathouse IV_, Oct 14 2013",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = sum(k=1, length( binary(n+2)) - 1, x^(2^k) / (1 - x^(2^k)), x^3 * O(x^n)); polcoeff( A * (1 + x) / x^2, n))}; /* _Michael Somos_, May 11 2014 */"
			],
			"xref": [
				"Bisection gives column 1 of A050600: A001511.",
				"Cf. A007814, A094267."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Antti Karttunen_ Jun 22 1999",
			"ext": [
				"Definition simplified by _N. J. A. Sloane_, Aug 27 2016"
			],
			"references": 8,
			"revision": 38,
			"time": "2021-07-19T21:18:05-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}