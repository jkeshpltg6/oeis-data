{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A277341",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 277341,
			"data": "1,2,5,101,19,873,44,3455,716066,122,3682385,42002,239,74612,38038256,75356321,487,168475200,414001,701,473945370,786827,996734911,1854156102515,1757001,1408,2223586,1578,2777435,102598699146418244788937,5067957,14314401926,2577,172311367283303079,3045",
			"name": "a(n) is the nearest integer to prime(n)^prime(n+1)/prime(n+1)^prime(n).",
			"comment": [
				"Conjecture 1: For each positive integer m, there exists a minimum sufficiently large positive integer r that depends on m, such that taking any two distinct positive integers r1, r2 \u003e= r, we have abs(a(r1) - a(r2)) \u003e= m. For the special case of m=1 it is conjectured that r=1, which would imply (if the conjecture were true) that all terms of this sequence are distinct. - _Ahmad J. Masad_, Jun 28 2018",
				"A complementary conjecture to Conjecture 1: For each nonnegative integer q, there are infinitely many possible positive integers k, t, w, s such that k \u003c t \u003c= w \u003c s and (t-k) \u003e (s-w) and abs((the nearest integer to (k^t/t^k)) - (the nearest integer to (w^s/s^w))) = q. These two conjectures together describe partially the significance of the set of primes among the set of natural numbers. - _Ahmad J. Masad_, Mar 29 2018",
				"Conjecture 3: The Riemann hypothesis is true if and only if all terms of this sequence are distinct. This conjecture idea comes from the visual representation of the logarithmic scatterplot of the first 10000 terms of this sequence: we see the logarithmic scatterplot of these 10000 terms being like simple logarithmic functions above each other, and also the sequence definition is directly defined on the sequence of primes. On the other hand, the set of primes is related to Euler's number and to logarithmic functions by the prime number theorem, and also the Riemann hypothesis is equivalent to the statement: for each positive integer k larger than one, sigma(k) \u003c H(k) + e^(H(k))*log(H(k)), so this means that the Riemann hypothesis is related to Euler's number and logarithmic functions. All of this makes it a possibility that conjecture 3 is true. - _Ahmad J. Masad_, Jan 09 2019",
				"Conjecture 4: For each value of n, a(n+1) \u003e a(n) if and only if A058077(n+1) \u003e A058077(n), checked for n \u003c= 10000. Note that the logarithmic scatterplot of A058077 seems to be similar to the logarithmic scatterplot of this sequence. - _Ahmad J. Masad_, Jun 28 2019",
				"Notification: the conjecture that says that all terms of this sequence are distinct has been checked for the first 10000 terms; that is, the first 10000 terms of this sequence are distinct. - _Ahmad J. Masad_, Aug 25 2019",
				"Conjecture 5: For each value of n \u003e 1, if a(n) has the same number of digits as a(n+1) and a(n+1) \u003e a(n), then prime(n+2) - prime(n+1) = prime(n+1) - prime(n). This conjecture has been verified for all n \u003c 10000. - _Ahmad J. Masad_, Oct 08 2019"
			],
			"link": [
				"Daniel Suteu, \u003ca href=\"/A277341/b277341.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (first 1000 terms from Vincenzo Librandi)",
				"Rémy Sigrist, \u003ca href=\"/A277341/a277341.png\"\u003eColored logarithmic scatterplot of (n, a(n)) for n = 1..10000\u003c/a\u003e (where the color is function of prime(n+1)-prime(n))",
				"Rémy Sigrist, \u003ca href=\"/A277341/a277341_1.png\"\u003eColored logarithmic scatterplot of (n, a(n)) for n = 1..100000\u003c/a\u003e (where the color is function of prime(n+1)-prime(n))",
				"Daniel Suteu, \u003ca href=\"/A277341/a277341.pl.txt\"\u003ePerl program\u003c/a\u003e"
			],
			"example": [
				"For n = 4, we have ((prime(4)^prime(5))/(prime(5)^prime(4))) = (7^11)/(11^7) = 1977326743/19487171 = 101.4681271..., and 101 is the nearest integer to 101.4681271..., so a(4) = 101."
			],
			"mathematica": [
				"Table[Round[((Prime[n]^Prime[n + 1])/(Prime[n + 1]^Prime[n]))], {n, 35}] (* _Michael De Vlieger_, Oct 14 2016 *)"
			],
			"program": [
				"(MAGMA) [Round((NthPrime(n)^NthPrime(n+1))/(NthPrime(n+1)^NthPrime(n))): n in [1..40]]; // _Vincenzo Librandi_ Oct 18 2016",
				"(PARI) a(n) = round(prime(n)^prime(n+1)/prime(n+1)^prime(n)); \\\\ _Michel Marcus_, Jan 13 2018"
			],
			"xref": [
				"Cf. A053089, A058077, A078422."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Ahmad J. Masad_, Oct 09 2016",
			"references": 2,
			"revision": 190,
			"time": "2021-06-16T02:08:47-04:00",
			"created": "2016-10-18T06:01:16-04:00"
		}
	]
}