{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A278575",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 278575,
			"data": "0,0,0,1,1,2,1,3,1,4,3,8,5,10,7,16,11,23,17,34,25,46,36,68,52,91,73,128,103,173,142,236,194,313,265,424,357,555,476,737,634,961,837,1256,1098,1621,1433,2102,1860,2687,2401,3445,3089,4379,3952,5563,5034,7015,6391,8852,8082,11087,10177",
			"name": "Number of decompositions of n as a sum of nonnegative multiples of 5, 7, even numbers greater than 2, and the two partitions obtained by expressing the numbers 6 and 8 (each of them exactly once) as a sum using positive integers less than 4.",
			"comment": [
				"According to the text of the paper (see Corollary 13), this should be the number of decompositions of n as a sum of nonnegative multiples of 3 and 4. But then a(3) would be 1, and a(5) would be zero. So that is not the definition. But they give 100 terms, so it should be possible to find the correct definition.",
				"Old name was: Number of decompositions of n as a \"sum of nonnegative multiples of 3 and 4\". - _Arkadiusz Wesolowski_, Feb 03 2017"
			],
			"link": [
				"Best, Darcy, Hadi Kharaghani, and Hugh Ramp, \u003ca href=\"/A278575/b278575.txt\"\u003eTable of n, a(n) for n = 1..100\u003c/a\u003e",
				"Darcy Best, Hadi Kharaghani, and Hugh Ramp, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2012.12.022\"\u003eOn unit weighing matrices with small weight\u003c/a\u003e, Discrete Mathematics 313.7 (2013): 855-864. Also \u003ca href=\"https://arxiv.org/abs/1209.4581\"\u003earXiv:1209.4581 [math.CO]\u003c/a\u003e, 2012. See Table 2."
			],
			"example": [
				"From _Arkadiusz Wesolowski_, Feb 03 2017: (Start)",
				"Let 6 be partitioned as 3 + 3, and let 8 be partitioned as 1 + 2 + 2 + 3. Then:",
				"4 = 4, so a(4) = 1.",
				"5 = 5, so a(5) = 1.",
				"6 = 6 =",
				"  = 3 + 3, so a(6) = 2.",
				"7 = 7, so a(7) = 1.",
				"8 = 8 =",
				"  = 2*4 =",
				"  = 1 + 2 + 2 + 3, so a(8) = 3.",
				"9 = 5 + 4, so a(9) = 1.",
				"10 = 10 =",
				"   = 6 + 4 =",
				"   = 2*5 =",
				"   = 4 + (3 + 3), so a(10) = 4.",
				"11 = 7 + 4 =",
				"   = 6 + 5 =",
				"   = 5 + (3 + 3), so a(11) = 3.",
				"12 = 12 =",
				"   = 8 + 4 =",
				"   = 5 + 7 =",
				"   = 2*6 =",
				"   = 6 + (3 + 3) =",
				"   = 3*4 =",
				"   = 4 + (1 + 2 + 2 + 3) =",
				"   = 2*(3 + 3), so a(12) = 8. (End)"
			],
			"program": [
				"(MAGMA) /* computes the first 27 elements of the sequence */ lst:=[]; m:=27; if m lt 28 then b:=SequenceToSet([p: p in [4..m by 2]]); o:={5, 7} join b; for n in [1..m] do p:=RestrictedPartitions(n, o); a:=#p; b:=0; for y in [1..#p] do lst1:=p[y]; if #lst1 eq 1 then if lst1[1] eq 6 or lst1[1] eq 8 then b+:=#p[y]; end if; else t:=0; for r in [1..#lst1] do if lst1[r] eq 6 or lst1[r] eq 8 then t+:=1; end if; end for; if t eq 1 then b+:=1; end if; if t ge 2 then if 6 in lst1 and not 8 in lst1 then b+:=t; end if; if 8 in lst1 and not 6 in lst1 then b+:=t; end if; if 6 in lst1 and 8 in lst1 then b+:=2*t-1; end if; end if; end if; end for; Append(~lst, a+b); end for; end if; lst; // _Arkadiusz Wesolowski_, Feb 06 2017"
			],
			"keyword": "nonn",
			"offset": "1,6",
			"author": "_N. J. A. Sloane_, Nov 27 2016",
			"ext": [
				"Name changed by _Arkadiusz Wesolowski_, Feb 03 2017"
			],
			"references": 1,
			"revision": 37,
			"time": "2017-02-16T03:15:14-05:00",
			"created": "2016-11-27T23:53:29-05:00"
		}
	]
}