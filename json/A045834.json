{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A045834",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 45834,
			"data": "1,4,5,4,8,8,5,12,8,4,16,12,9,12,8,12,16,16,8,16,17,8,24,8,8,28,16,12,16,20,13,24,24,8,16,16,16,28,24,12,32,16,13,28,8,20,32,32,8,20,24,16,40,16,16,32,25,20,24,24,24,28,24,8,32,36,16,44,16,12,40,32,17,36,32",
			"name": "Half of theta series of cubic lattice with respect to edge.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A045834/b045834.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"J. H. Conway and N. J. A. Sloane, \u003ca href=\"http://dx.doi.org/10.1007/978-1-4757-2016-7\"\u003eSphere Packings, Lattices and Groups\u003c/a\u003e, Springer-Verlag, p. 107.",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Euler transform of period 4 sequence [ 4, -5, 4, -3,...]. - _Michael Somos_, Feb 28 2006",
				"Expansion of theta_2(q^2)^2 * (theta_3(q) + theta_4(q)) / (8*q) in powers of q^4. - _Michael Somos_, Feb 28 2006",
				"Expansion of q^(-1/4) * eta(q^2)^9 / (eta(q)^4 * eta(q^4)^2) in powers of q. - _Michael Somos_, Feb 28 2006",
				"G.f.: Product_{k\u003e0} (1 + x^k)^4 * (1 - x^(2*k))^3 / (1 + x^(2*k))^2. - _Michael Somos_, Feb 28 2006",
				"Expansion of phi(x)^2 * psi(x^2) in powers of x where phi(), psi() are Ramanujan theta functions. - _Michael Somos_, Oct 25 2006",
				"A005876(n) = 2*a(n)."
			],
			"example": [
				"G.f. = 1 + 4*x + 5*x^2 + 4*x^3 + 8*x^4 + 8*x^5 + 5*x^6 + 12*x^7 + 8*x^8 + ...",
				"G.f. = q + 4*q^5 + 5*q^9 + 4*q^13 + 8*q^17 + 8*q^21 + 5*q^25 + 12*q^29 + ..."
			],
			"maple": [
				"S:= series((1/8)*JacobiTheta2(0, sqrt(q))^2*(JacobiTheta3(0, q^(1/4))+JacobiTheta4(0, q^(1/4)))/q^(1/4), q, 1001):",
				"seq(coeff(S,q,j),j=0..1000); # _Robert Israel_, Nov 13 2016"
			],
			"mathematica": [
				"s = EllipticTheta[3, 0, q]^2*EllipticTheta[2, 0, q]/(2*q^(1/4)) + O[q]^75; CoefficientList[s, q] (* _Jean-François Alcover_, Nov 04 2015, from 5th formula *)",
				"QP = QPochhammer; s = QP[q^2]^9/(QP[q]^4*QP[q^4]^2) + O[q]^80; CoefficientList[s, q] (* _Jean-François Alcover_, Dec 01 2015, adapted from PARI *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 +A)^9 / (eta(x + A)^4 * eta(x^4 + A)^2), n))}; /* _Michael Somos_, Feb 28 2006 */"
			],
			"xref": [
				"Cf. A005876."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 16,
			"revision": 28,
			"time": "2021-03-12T22:24:42-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}