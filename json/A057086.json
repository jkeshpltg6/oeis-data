{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A057086",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 57086,
			"data": "1,10,90,800,7100,63000,559000,4960000,44010000,390500000,3464900000,30744000000,272791000000,2420470000000,21476790000000,190563200000000,1690864100000000,15003009000000000,133121449000000000,1181184400000000000,10480629510000000000",
			"name": "Scaled Chebyshev U-polynomials evaluated at sqrt(10)/2.",
			"comment": [
				"This is the m=10 member of the m-family of sequences S(n,sqrt(m))*(sqrt(m))^n; for S(n,x) see Formula. The m=4..9 instances are A001787, A030191, A030192, A030240, A057084-5 and the m=1..3 signed sequences are A010892, A009545, A057083.",
				"The characteristic roots are rp(m) := (m+sqrt(m*(m-4)))/2 and rm(m) := (m-sqrt(m*(m-4)))/2 and a(n,m)= (rp(m)^(n+1)-rm(m)^(n+1))/(rp(m)-rm(m)) is the Binet form of these m-sequences."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A057086/b057086.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"A. F. Horadam, \u003ca href=\"http://www.fq.math.ca/Scanned/5-5/horadam.pdf\"\u003eSpecial properties of the sequence W_n(a,b; p,q)\u003c/a\u003e, Fib. Quart., 5.5 (1967), 424-434. Case n-\u003en+1, a=0,b=1; p=10, q=-10.",
				"W. Lang, \u003ca href=\"http://www.fq.math.ca/Scanned/38-5/lang.pdf\"\u003eOn polynomials related to powers of the generating function of Catalan's numbers\u003c/a\u003e, Fib. Quart. 38 (2000) 408-419. Eqs.(38) and (45),lhs, m=10.",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (10,-10)."
			],
			"formula": [
				"a(n) = 10*(a(n-1)-a(n-2)), a(-1)=0, a(0)=1.",
				"a(n) = S(n, sqrt(10))*(sqrt(10))^n with S(n, x) := U(n, x/2), Chebyshev's polynomials of the 2nd kind, A049310.",
				"a(2*k) = A057080(k)*10^k, a(2*k+1) = A001090(k)*10^(k+1).",
				"G.f.: 1/(1-10*x+10*x^2).",
				"a(n) = Sum_[k, 0\u003c=k\u003c=n} A109466(n,k)*10^k. - _Philippe Deléham_, Oct 28 2008",
				"a(n) = -(1/30)*[5-sqrt(15)]^(n+1)*sqrt(15)+(1/30)*sqrt(15)*[5+sqrt(15)]^(n+1), with n\u003e=0. - _Paolo P. Lava_, Nov 20 2008"
			],
			"mathematica": [
				"Join[{a=1,b=10},Table[c=10*b-10*a;a=b;b=c,{n,60}]] (* _Vladimir Joseph Stephan Orlovsky_, Jan 20 2011 *)"
			],
			"program": [
				"(Sage) [lucas_number1(n,10,10) for n in range(1, 20)] # _Zerinvary Lajos_, Apr 26 2009",
				"(PARI) Vec(1/(1-10*x+10*x^2) + O(x^30)) \\\\ _Colin Barker_, Jun 14 2015"
			],
			"xref": [
				"Cf. A030240, A057084."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_ Aug 11 2000",
			"references": 11,
			"revision": 27,
			"time": "2019-12-07T12:18:22-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}