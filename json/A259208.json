{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A259208",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 259208,
			"data": "1,1,3,1,25,1,721,1,6721,181441,151201,1,203575681,1,121080961,108972864001,3491282995201,1,133541574566401,1,304119455447808001,212878925715456001,309744468633601,1,17665560315112008499201,646300418472124416000001,841941782922240001",
			"name": "E.g.f.: Sum_{n\u003e=0} x^n * (1 + x^n)^n / n!.",
			"comment": [
				"More generally, the following sums are equal:",
				"(1) Sum_{n\u003e=0} (q^n + p)^n * r^n/n!,",
				"(2) Sum_{n\u003e=0} q^(n^2) * exp(p*q^n*r) * r^n/n!;",
				"here, q = x, p = 1, r = x. - _Paul D. Hanna_, Jul 03 2019",
				"In general, let F(x) be a formal power series in x such that F(0)=1, then",
				"Sum_{n\u003e=0} m^n * F(q^n*r)^p * log( F(q^n*r) )^n / n! =",
				"Sum_{n\u003e=0} r^n * [y^n] F(y)^(m*q^n + p);",
				"here, F(x) = exp(x), q = , p = 1, r = x, m = 1. - _Paul D. Hanna_, Jul 06 2019"
			],
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A259208/b259208.txt\"\u003eTable of n, a(n) for n = 0..1025\u003c/a\u003e"
			],
			"formula": [
				"E.g.f.: Sum_{n\u003e=0} x^(n*(n+1)) * exp(x^(n+1)) / n!.",
				"a(n) = Sum_{d|n} binomial(d, n/d - 1) * n!/d!."
			],
			"example": [
				"E.g.f.: A(x) = 1 + x + 3*x^2/2! + x^3/3! + 25*x^4/4! + x^5/5! + 721*x^6/6! + x^7/7! + 6721*x^8/8! + 181441*x^9/9! +...",
				"where",
				"A(x) = 1 + x*(1+x) + x^2*(1+x^2)^2/2! + x^3*(1+x^3)^3/3! + x^4*(1+x^4)^4/4! + x^5*(1+x^5)^5/5! + x^6*(1+x^6)^6/6! +...",
				"also",
				"A(x) = exp(x) + x^2*exp(x^2) + x^6*exp(x^3)/2! + x^12*exp(x^4)/3! + x^20*exp(x^5)/4! + x^30*exp(x^6)/5! + x^42*exp(x^7)/6! +...",
				"RELATED SERIES.",
				"Below we illustrate the following identity at specific values of x:",
				"Sum_{n\u003e=0} x^n * (1 + x^n)^n / n!  =  Sum_{n\u003e=0} x^(n*(n+1)) * exp(x^(n+1)) / n!.",
				"(1) At x = 1/2, the following sums are equal",
				"S1 = Sum_{n\u003e=0} 2^(-n*(n+1)) * (2^n + 1)^n / n!,",
				"S1 = Sum_{n\u003e=0} 2^(-n*(n+1)) * exp( 1/2^(n+1) ) / n!,",
				"where S1 = 1.97862370255774939923047215233920359461142155926360261512472...",
				"(2) At x = 1/3, the following sums are equal",
				"S2 = Sum_{n\u003e=0} 3^(-n*(n+1)) * (3^n + 1)^n / n!,",
				"S2 = Sum_{n\u003e=0} 3^(-n*(n+1)) * exp( 1/3^(n+1) ) / n!,",
				"where S2 = 1.52049327799122758174016893855657751957768595647287750170026..."
			],
			"program": [
				"(PARI) {a(n) = local(A=1); A = sum(m=0, n, x^m/m!*(1 + x^m +x*O(x^n))^m); n!*polcoeff(A, n)}",
				"for(n=0, 30, print1(a(n), \", \"))",
				"(PARI) {a(n) = local(A=1); A = sum(m=0, n, x^(m*(m+1)) / m! * exp(x^(m+1) +x*O(x^n))); n!*polcoeff(A, n)}",
				"for(n=0, 30, print1(a(n), \", \"))",
				"(PARI) {a(n) = if(n==0,1, sumdiv(n,d, binomial(d, n/d-1) * n!/d! ) )}",
				"for(n=0, 30, print1(a(n), \", \"))"
			],
			"xref": [
				"Cf. A326096."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Paul D. Hanna_, Jun 21 2015",
			"references": 4,
			"revision": 15,
			"time": "2019-07-09T01:17:54-04:00",
			"created": "2015-06-21T02:28:39-04:00"
		}
	]
}