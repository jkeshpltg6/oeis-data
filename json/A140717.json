{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A140717",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 140717,
			"data": "1,1,1,1,1,2,2,1,3,5,4,1,1,4,9,12,10,4,2,1,5,14,25,31,26,16,9,4,1,1,6,20,44,70,82,74,54,38,22,12,4,2,1,7,27,70,134,196,227,215,179,139,99,64,38,20,9,4,1,1,8,35,104,231,400,558,644,641,576,488,384,288,200,134,80",
			"name": "Triangle read by rows: T(n,k) is the number of Dyck paths d of semilength n such that sum of peakheights of d - number of peaks of d = k (n \u003e= 0, 0 \u003c= k \u003c= floor(n^2/4)).",
			"comment": [
				"T(n,k) is the number of 321-avoiding permutations of {1,2,...,n} having inversion number equal to k. Example: T(4,2) = 5 because we have 1423, 1342, 3124, 2143 and 2341."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A140717/b140717.txt\"\u003eRows n = 0..50, flattened\u003c/a\u003e",
				"E. Barcucci, A. Del Lungo, E. Pergola and R. Pinzani, \u003ca href=\"https://doi.org/10.1080/10236199908808200\"\u003eECO: a methodology for the enumeration of combinatorial objects\u003c/a\u003e, Journal of Difference Equations and Applications, 5, 1999, 435-490.",
				"E. Barcucci, A. Del Lungo, E. Pergola and R. Pinzani, \u003ca href=\"https://doi.org/10.1016/S0012-365X(00)00359-9\"\u003eSome permutations with forbidden subsequences and their inversion number\u003c/a\u003e, Discrete Math., 234, 2001, 1-15.",
				"E. Deutsch, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(98)00371-9\"\u003eDyck path enumeration\u003c/a\u003e, Discrete Math., 204, 1999, 167-202 (see section 5).",
				"G. Feinberg, K.-H. Lee, \u003ca href=\"http://arxiv.org/abs/1401.0845\"\u003eHomogeneous representations of KLR-algebras and fully commutative elements\u003c/a\u003e, arXiv preprint arXiv:1401.0845 [math.RT], 2014-2015.",
				"Niket Gowravaram and Tanya Khovanova, \u003ca href=\"http://arxiv.org/abs/1509.00462\"\u003eOn the Structure of nil-Temperley-Lieb Algebras of type A\u003c/a\u003e, arXiv:1509.00462 [math.CO], 2015."
			],
			"formula": [
				"G.f.: G(t,z) = H(t,1/t,z), where H(t,x,z) = 1 + zH(t,x,z)[H(t,tx,z)-1+tx] (H(t,x,z) is the trivariate g.f. of Dyck paths with respect to semilength, sum of peak-heights and number of peaks, marked by z, t and x, respectively).",
				"Sum_{k\u003e=0} k*T(n,k) = A008549(n-1).",
				"Row n has 1 + floor(n^2/4) entries."
			],
			"example": [
				"T(4,2) = 5 because we have UDUUDUDD (5 - 3 = 2), UDUUUDD (4 - 2 = 2), UUDDUUDD (4 - 2 = 2), UUDUDDUD (5 - 3 = 2) and UUUDDDUD (4 - 2 = 2); here U = (1,1), D = (1,-1).",
				"Triangle starts:",
				"  1;",
				"  1;",
				"  1, 1;",
				"  1, 2,  2;",
				"  1, 3,  5,  4,  1;",
				"  1, 4,  9, 12, 10,  4,  2;",
				"  1, 5, 14, 25, 31, 26, 16, 9, 4, 1;"
			],
			"maple": [
				"H := 1/(1+z-t*x*z-z*h[1]):",
				"for n to 13 do h[n]:=1/(1+z-x*t^(n+1)*z-z*h[n+1]) end do:",
				"G := subs({h[11]=0,x=1/t},H): Gser := simplify(series(G,z=0,12)):",
				"for n from 0 to 9 do P[n] := sort(coeff(Gser,z,n)) end do:",
				"for n from 0 to 9 do seq(coeff(P[n],t,j), j=0..floor((1/4)*n^2)) end do;",
				"# yields sequence in triangular form"
			],
			"mathematica": [
				"m = rows = 10; mt = 2 m + 1; mx = mz = m - 1;",
				"H[_, _, _] = 0; Do[H[t_, x_, z_] = Series[1 + z (H[t, t x, z] - 1 + t x) H[t, x, z], {t, 0, mt}, {x, 0, mx}, {z, 0, mz}] // Normal, {m}];",
				"G[t_, z_] = Series[H[t, 1/t, z], {t, 0, mt}, {z, 0, mz}] // Normal // Collect[#, z]\u0026;",
				"CoefficientList[#, t]\u0026 /@ CoefficientList[G[t, z], z] // Take[#, m]\u0026 // Flatten (* _Jean-François Alcover_, Nov 25 2018 *)"
			],
			"xref": [
				"Row sums are the Catalan numbers A000108.",
				"Cf. A008549, A129183."
			],
			"keyword": "nonn,tabf",
			"offset": "0,6",
			"author": "_Emeric Deutsch_, Jun 08 2008",
			"references": 1,
			"revision": 21,
			"time": "2018-11-26T04:24:50-05:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}