{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A337241",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 337241,
			"data": "11,11,11,11,11,11,11,11,11,10,1,12,9,8,15,7,7,8,6,5,12,1,5,6,4,13,37,4,4,10,5,7,1,8,13,4,3,3,3,5,13,6,8,1,5,7,3,3,7,2,5,13,4,20,1,2,2,2,2,5,2,7,4,7,7,1,5,4,6,10,5,2,4,3,3,3,1,11,6,5,14,8,2,3,3",
			"name": "a(n) is the least k such that the decimal representation of k*n contains at least two digits which are the same.",
			"comment": [
				"First differs from A045538 at a(21) = 12 since 21 * 12 =  252 contains two equal but not consecutive digits. A045538(21) = 16.",
				"The largest value is a(27)=37. - _Robert Israel_, Sep 17 2020"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A337241/b337241.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_01\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1)."
			],
			"formula": [
				"a(n) \u003c= A045538(n).",
				"a(n) = 1 for any n \u003e= 10^10. - _Rémy Sigrist_, Sep 15 2020"
			],
			"example": [
				"a(21) = 12 because 21*12 = 252 is the smallest multiple of 21 with equal digits.",
				"a(23) = 5 because 23*5 = 115 is the smallest multiple of 23 with equal digits.",
				"a(34) = 8 because 34*8 = 272 is the smallest multiple of 34 with equal digits."
			],
			"maple": [
				"f:= proc(n) local k,L;",
				"  for k from 1 do",
				"    L:= convert(k*n,base,10);",
				"    if nops(convert(L,set))\u003cnops(L) then return k fi",
				"  od;",
				"end proc:",
				"map(f, [$1..100]); # _Robert Israel_, Sep 17 2020"
			],
			"mathematica": [
				"a[n_] := Module[{k = 1}, While[Max@ (Last /@ Tally @ IntegerDigits[k*n]) == 1, k++]; k]; Array[a, 100] (* _Amiram Eldar_, Aug 22 2020 *)"
			],
			"program": [
				"(PARI) a(n) = {my(k=1, d=digits(n)); while(#Set(d) == #d, k++; d=digits(k*n)); k;} \\\\ _Michel Marcus_, Aug 22 2020"
			],
			"xref": [
				"Cf. A045538 (where the 2 digits must be consecutive), A337240 (resulting k*n)."
			],
			"keyword": "nonn,base",
			"offset": "1,1",
			"author": "_Rodolfo Kurchan_, Aug 20 2020",
			"references": 2,
			"revision": 32,
			"time": "2020-09-18T02:09:36-04:00",
			"created": "2020-09-16T02:22:51-04:00"
		}
	]
}