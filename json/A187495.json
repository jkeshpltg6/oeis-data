{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A187495",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 187495,
			"data": "0,0,0,1,0,0,0,1,0,2,0,1,0,3,1,5,1,4,1,9,5,14,6,14,7,28,20,42,27,48,34,90,75,132,109,165,143,297,274,429,417,571,560,1000,988,1429,1548,1988,2108,3417,3536,4846,5644,6953,7752,11799,12597",
			"name": "Let i be in {1,2,3,4} and let r \u003e= 0 be an integer. Let p = {p_1, p_2, p_3, p_4} = {-3,0,1,2}, n=3*r+p_i, and define a(-3)=1. Then a(n)=a(3*r+p_i) gives the quantity of H_(9,1,0) tiles in a subdivided H_(9,i,r) tile after linear scaling by the factor Q^r, where Q=sqrt(2*cos(Pi/9)).",
			"comment": [
				"(Start) See A187498 for supporting theory. Define the matrix",
				"U_1=",
				"(0 1 0 0)",
				"(1 0 1 0)",
				"(0 1 0 1)",
				"(0 0 1 1).",
				"Let r\u003e=0, and let A_r be the r-th \"block\" defined by A_r={a(3*r-3),a(3*r),a(3*r+1),a(3*r+2)} with a(-3)=1. Note that A_r-A_(r-1)-3*A_(r-2)+2*A_(r-3)+A_(r-4)={0,0,0,0}, for r\u003e=4, with initial conditions {A_k}={{1,0,0,0},{0,1,0,0},{1,0,1,0},{0,2,0,1}}, k=0,1,2,3. Let p={p_1,p_2,p_3,p_4}={-3,0,1,2}, n=3*r+p_i and M=(m_(i,j))=(U_1)^r, i,j=1,2,3,4. Then A_r corresponds component-wise to the first column of M, and a(n)=a(3*r+p_i)=m_(i,1) gives the quantity of H_(9,1,0) tiles that should appear in a subdivided H_(9,i,r) tile. (End)",
				"Since a(3*r)=a(3*(r+1)-3) for all r, this sequence arises by concatenation of first-column entries m_(2,1), m_(3,1) and m_(4,1) from successive matrices M=(U_1)^r.",
				"This sequence is a nontrivial extension of A187496."
			],
			"reference": [
				"L. E. Jeffery, Unit-primitive matrices and rhombus substitution tilings, (in preparation)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A187495/b187495.txt\"\u003eTable of n, a(n) for n = 0..5000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_12\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,0,1,0,0,3,0,0,-2,0,0,-1)."
			],
			"formula": [
				"Recurrence: a(n) = a(n-3) +3*a(n-6) -2*a(n-9) -a(n-12), for n\u003e=12, with initial conditions {a(m)}={0,0,0,1,0,0,0,1,0,2,0,1}, m=0,1,...,11.",
				"G.f.: x^3*(1-x^3+x^4-x^6-x^7+x^8)/(1-x^3-3*x^6+2*x^9+x^12)."
			],
			"mathematica": [
				"LinearRecurrence[{0,0,1,0,0,3,0,0,-2,0,0,-1}, {0,0,0,1,0,0,0,1,0,2,0,1}, 50] (* _G. C. Greubel_, Apr 20 2018 *)"
			],
			"program": [
				"(PARI) x='x+O('x^50); concat([0,0,0], Vec(x^3*(1-x^3+x^4-x^6-x^7+x^8)/(1-x^3-3*x^6+2*x^9+x^12))) \\\\ _G. C. Greubel_, Apr 20 2018",
				"(MAGMA) I:=[0,0,0,1,0,0,0,1,0,2,0,1]; [n le 12 select I[n] else Self(n-3) + 3*Self(n-6) - 2*Self(n-9) - Self(n-12): n in [1..50]]; // _G. C. Greubel_, Apr 20 2018"
			],
			"xref": [
				"Cf. A187496, A187497, A187498."
			],
			"keyword": "nonn,easy",
			"offset": "0,10",
			"author": "_L. Edson Jeffery_, Mar 17 2011",
			"references": 4,
			"revision": 15,
			"time": "2019-02-03T16:48:21-05:00",
			"created": "2011-03-10T17:29:56-05:00"
		}
	]
}