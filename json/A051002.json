{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A051002",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 51002,
			"data": "1,1,244,1,3126,244,16808,1,59293,3126,161052,244,371294,16808,762744,1,1419858,59293,2476100,3126,4101152,161052,6436344,244,9768751,371294,14408200,16808,20511150,762744,28629152,1,39296688,1419858,52541808,59293,69343958",
			"name": "Sum of 5th powers of odd divisors of n.",
			"comment": [
				"The Apostol exercise F(x) is the g.f. of a(n)*(-1)^(n+1). - _Michael Somos_, Jul 05 2021"
			],
			"reference": [
				"T. M. Apostol, Modular Functions and Dirichlet Series in Number Theory, Springer-Verlag, 1990, page 25, Exercise 15."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A051002/b051002.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"J. W. L. Glaisher, \u003ca href=\"https://books.google.com/books?id=bLs9AQAAMAAJ\u0026amp;pg=RA1-PA1\"\u003eOn the representations of a number as the sum of two, four, six, eight, ten, and twelve squares\u003c/a\u003e, Quart. J. Math. 38 (1907), 1-62 (see p. 4 and p. 8).",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/OddDivisorFunction.html\"\u003eOdd Divisor Function\u003c/a\u003e.",
				"\u003ca href=\"/index/Ge#Glaisher\"\u003eIndex entries for sequences mentioned by Glaisher\u003c/a\u003e"
			],
			"formula": [
				"Dirichlet g.f.: (1-2^(5-s))*zeta(s)*zeta(s-5). - _R. J. Mathar_, Apr 06 2011",
				"G.f.: Sum_{k\u003e=1} (2*k - 1)^5*x^(2*k-1)/(1 - x^(2*k-1)). - _Ilya Gutkovskiy_, Jan 04 2017",
				"The preceding g.f. is also 34*sigma_5(x^2) - 64*sigma_5(x^4) - sigma_5(-x), with sigma_5 the g.f. of A001160. Compare this with the Apostol reference which gives the g.f. of a(n)*(-1)^(n+1). - _Wolfdieter Lang_, Jan 31 2017",
				"Multiplicative with a(2^e) = 1 and a(p^e) = (p^(5*e+5)-1)/(p^5-1) for p \u003e 2. - _Amiram Eldar_, Sep 14 2020",
				"Sum_{k=1..n} a(k) ~ Pi^6 * n^6 / 11340. - _Vaclav Kotesovec_, Sep 24 2020",
				"G.f.: Sum_{n \u003e= 1} x^n*R(5,x^(2*n))/(1 - x^(2*n))^6, where R(5,x) = 1 + 237*x + 1682*x^2 + 1682*x^3 + 237*x^4 + x^5 is the fifth row polynomial of A060187. - _Peter Bala_, Dec 20 2021"
			],
			"example": [
				"G.f. = x + x^2 + 244*x^3 + x^4 + 3126*x^5 + 244*x^6 + 16808*x^7 + x^8 + ... - _Michael Somos_, Jul 05 2021"
			],
			"mathematica": [
				"a[n_] := Select[ Divisors[n], OddQ]^5 // Total; Table[a[n], {n, 1, 34}] (* _Jean-François Alcover_, Oct 25 2012 *)",
				"f[2, e_] := 1; f[p_, e_] := (p^(5*e + 5) - 1)/(p^5 - 1); a[1] = 1; a[n_] := Times @@ f @@@ FactorInteger[n]; Array[a, 100] (* _Amiram Eldar_, Sep 14 2020 *)",
				"a[ n_] := If[n == 0, 0, DivisorSigma[5, n / 2^IntegerExponent[n, 2]]]; (* _Michael Somos_, Jul 05 2021 *)"
			],
			"program": [
				"(PARI) a(n) = sumdiv(n , d, (d%2)*d^5); \\\\ _Michel Marcus_, Jan 14 2014",
				"(PARI) a(n)=sumdiv(n\u003e\u003evaluation(n,2), d, d^5) \\\\ _Charles R Greathouse IV_, Jul 05 2021"
			],
			"xref": [
				"Cf. A000593, A001227, A050999, A051000, A051001, A001160."
			],
			"keyword": "nonn,mult",
			"offset": "1,3",
			"author": "_Eric W. Weisstein_",
			"references": 14,
			"revision": 46,
			"time": "2021-12-21T07:40:54-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}