{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A219536",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 219536,
			"data": "1,3,24,255,3102,40854,566934,8164263,120864390,1827982362,28122626760,438720097638,6923868098820,110346550539780,1773394661610258,28707809007278775,467677404522668742,7661583171651546786,126137791939032756960,2085923447593966281378",
			"name": "G.f. satisfies: A(x) = 1 + x*(A(x)^2 + 2*A(x)^3).",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A219536/b219536.txt\"\u003eTable of n, a(n) for n = 0..790\u003c/a\u003e",
				"Elżbieta Liszewska, Wojciech Młotkowski, \u003ca href=\"https://arxiv.org/abs/1907.10725\"\u003eSome relatives of the Catalan sequence\u003c/a\u003e, arXiv:1907.10725 [math.CO], 2019."
			],
			"formula": [
				"Let G(x) = (1-x - sqrt(1 - 10*x + x^2)) / (4*x), then g.f. A(x) satisfies:",
				"(1) A(x) = (1/x)*Series_Reversion(x/G(x)),",
				"(2) A(x) = G(x*A(x)) and G(x) = A(x/G(x)),",
				"where G(x) is the g.f. of A103210.",
				"Recurrence: 4*n*(2*n+1)*(19*n-26)*a(n) = (2717*n^3 - 6435*n^2 + 4342*n - 840)*a(n-1) + 2*(n-2)*(2*n-3)*(19*n-7)*a(n-2). - _Vaclav Kotesovec_, Dec 28 2013",
				"a(n) ~ (3/19)^(1/4) * (5+sqrt(57)) * ((143 + 19*sqrt(57))/16)^n / (16*sqrt(Pi)*n^(3/2)). - _Vaclav Kotesovec_, Dec 28 2013",
				"From _Seiichi Manyama_, Jul 26 2020: (Start)",
				"a(n) = Sum_{k=0..n} 2^k * binomial(n,k) * binomial(2*n+k+1,n)/(2*n+k+1).",
				"a(n) = (1/(2*n+1)) * Sum_{k=0..n} 2^(n-k) * binomial(2*n+1,k) * binomial(3*n-k,n-k). (End)"
			],
			"example": [
				"G.f.: A(x) = 1 + 3*x + 24*x^2 + 255*x^3 + 3102*x^4 + 40854*x^5 +...",
				"Related expansions:",
				"A(x)^2 = 1 + 6*x + 57*x^2 + 654*x^3 + 8310*x^4 + 112560*x^5 +...",
				"A(x)^3 = 1 + 9*x + 99*x^2 + 1224*x^3 + 16272*x^4 + 227187*x^5 +...",
				"The g.f. satisfies A(x) = G(x*A(x)) and G(x) = A(x/G(x)) where",
				"G(x) = 1 + 3*x + 15*x^2 + 93*x^3 + 645*x^4 + 4791*x^5 +...+ A103210(n)*x^n +..."
			],
			"mathematica": [
				"CoefficientList[1/x*InverseSeries[Series[4*x^2/(1-x-Sqrt[1-10*x+x^2]), {x, 0, 20}], x],x] (* _Vaclav Kotesovec_, Dec 28 2013 *)"
			],
			"program": [
				"(PARI) /* Formula A(x) = 1 + x*(A(x)^2 + 2*A(x)^3): */",
				"{a(n)=local(A=1);for(i=1,n,A=1+x*(A^2+2*A^3) +x*O(x^n));polcoeff(A,n)}",
				"for(n=0,25,print1(a(n),\", \"))",
				"(PARI) /* Formula using Series Reversion: */",
				"{a(n)=local(A=1,G=(1-x-sqrt(1-10*x+x^2+x^3*O(x^n)))/(4*x));A=(1/x)*serreverse(x/G);polcoeff(A,n)}",
				"for(n=0,25,print1(a(n),\", \"))",
				"(PARI) {a(n) = sum(k=0, n, 2^k*binomial(n, k)*binomial(2*n+k+1, n)/(2*n+k+1))} \\\\ _Seiichi Manyama_, Jul 26 2020",
				"(PARI) {a(n) = sum(k=0, n, 2^(n-k)*binomial(2*n+1, k)*binomial(3*n-k, n-k))/(2*n+1)} \\\\ _Seiichi Manyama_, Jul 26 2020"
			],
			"xref": [
				"Column k=2 of A336574.",
				"Cf. A003168, A103210, A219534, A219535."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Paul D. Hanna_, Nov 21 2012",
			"references": 5,
			"revision": 24,
			"time": "2020-07-30T02:13:03-04:00",
			"created": "2012-11-21T23:23:19-05:00"
		}
	]
}