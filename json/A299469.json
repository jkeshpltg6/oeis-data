{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A299469",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 299469,
			"data": "15,25,40,36,18,11,17,28,58,9,51,13,25,41,44,17,43,35,21,10,21,38,9,12,40,43,45,30,41,0,3,25,34,49,45,9,19,48,57,11,13,29,28,44,41,37,33,29,43,8,57,12,48,15,15,53,57,16,51,16,54,30,9,26,8,49,22,58,11,42,28,36,33,45,24,32,18,12,29,45,40,27,19,40,41,11,42,49,35,41,57,54,33,0,34,34,49,6,31",
			"name": "p-adic digits of the unique simple zero of the p-adic zeta-function zeta_{(p,l)} with (p,l) = (59,44).",
			"comment": [
				"The p-adic digits are used to compute A299466(n) = least even integer k such that numerator(B_k) == 0 (mod 59^n) (see 2nd formula below and the program in A299466).",
				"The algorithm used in the Mathematica program below is from Kellner 2007, Prop. 5.3, p. 428.",
				"The corresponding sequences for (p,l) = (37,32) and (p,l) = (67,58) are A299468 and A299470, respectively."
			],
			"link": [
				"Bernd C. Kellner and Jonathan Sondow, \u003ca href=\"/A299469/b299469.txt\"\u003eTable of n, a(n) for n = 0..98\u003c/a\u003e",
				"Bernd C. Kellner, \u003ca href=\"http://bernoulli.org/\"\u003eThe Bernoulli Number Page\u003c/a\u003e",
				"Bernd C. Kellner, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-06-01887-4\"\u003eOn irregular prime power divisors of the Bernoulli numbers\u003c/a\u003e, Math. Comp. 76 (2007) 405-441."
			],
			"formula": [
				"0 \u003c= a(n) \u003c= 58.",
				"l + (p - 1)*Sum_{i=0..n-2} a(i)*p^i = A299466(n) with (p,l) = (59,44)."
			],
			"example": [
				"The zero is given by a(0) + a(1)*p + a(2)*p^2 + ... with p = 59."
			],
			"mathematica": [
				"n = 99; p = 59; l = 44;",
				"ModR[x_, m_] := Mod[Mod[Numerator[x], m] PowerMod[Denominator[x], -1, m], m];",
				"B[n_] := -(1 - p^(n - 1)) BernoulliB[n]/n;",
				"T[r_, k_, x_] := Sum[(-1)^(j + k) Binomial[j, k] Binomial[x, j], {j, k, r}];",
				"zt = Table[ModR[B[l + (p - 1) k]/p, p^n], {k, 0, n}];",
				"Z[n_] := zt[[n + 1]]; d = Mod[Z[0] - Z[1], p]; t = 0; L = {};",
				"For[r = 1, r \u003c= n, r++, x = Mod[Sum[Z[k] T[r, k, t], {k, 0, r}], p^r];",
				"  s = ModR[x/(d*p^(r - 1)), p]; AppendTo[L, s]; t += s*p^(r - 1)];",
				"Print[L]"
			],
			"xref": [
				"Cf. A251782, A299466, A299467, A299468, A299470."
			],
			"keyword": "nonn",
			"offset": "0,1",
			"author": "_Bernd C. Kellner_ and _Jonathan Sondow_, Apr 08 2018",
			"references": 3,
			"revision": 8,
			"time": "2018-04-11T12:05:02-04:00",
			"created": "2018-04-11T12:05:02-04:00"
		}
	]
}