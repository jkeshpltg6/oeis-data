{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A261320",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 261320,
			"data": "1,-4,12,-28,60,-120,228,-416,732,-1252,2088,-3408,5460,-8600,13344,-20424,30876,-46152,68268,-100016,145224,-209120,298800,-423840,597108,-835804,1162824,-1608508,2212896,-3028632,4124664,-5590976,7544604,-10137264,13565016",
			"name": "Expansion of (phi(q^3) / phi(q))^2 in powers of q where phi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"Vaclav Kotesovec, \u003ca href=\"/A261320/b261320.txt\"\u003eTable of n, a(n) for n = 0..2000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of eta(q)^4 * eta(q^4)^4 * eta(q^6)^10 / ( eta(q^2)^10 * eta(q^3)^4 * eta(q^12)^4) in powers of q.",
				"Euler transform of period 12 sequence [ -4, 6, 0, 2, -4, 0, -4, 2, 0, 6, -4, 0, ...].",
				"G.f.: (Sum_{k in Z} x^(3*k^2)) / (Sum_{k in Z} x^k^2)^2.",
				"G.f.: (Product_{k\u003e0} (1 + (-x)^k + x^(2*k)) / (1 - (-x)^k + x^(2*k)))^2.",
				"a(n) = (-1)^n * A186924(n) = A233673(3*n) = A260215(3*n).",
				"Convolution square of A132002.",
				"a(n) ~ (-1)^n * exp(2*Pi*sqrt(n/3)) / (2*3^(5/4)*n^(3/4)). - _Vaclav Kotesovec_, Nov 16 2017"
			],
			"example": [
				"G.f. = 1 - 4*x + 12*x^2 - 28*x^3 + 60*x^4 - 120*x^5 + 228*x^6 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (EllipticTheta[ 3, 0, q^3] / EllipticTheta[ 3, 0, q])^2, {q, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A)^4 * eta(x^4 + A)^4 * eta(x^6 + A)^10 / (eta(x^2 + A)^10 * eta(x^3 + A)^4 * eta(x^12 + A)^4), n))};"
			],
			"xref": [
				"Cf. A132002, A186924, A260215, A233673."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Aug 14 2015",
			"references": 6,
			"revision": 9,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2015-08-14T18:17:32-04:00"
		}
	]
}