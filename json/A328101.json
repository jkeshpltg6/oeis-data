{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A328101",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 328101,
			"data": "1,0,1,0,1,0,0,1,0,1,1,1,1,0,0,0,0,1,1,0,1,1,0,1,0,0,1,1,0,0,0,1,1,0,1,0,0,1,1,0,0,0,0,1,1,1,1,0,1,1,1,0,0,1,1,0,0,1,0,0,1,1,0,0,0,0,0,0,1,1,1,1,0,0,0,0,1,1,1,0,0,0,1,0,1,0,0,1,1,1,1,0,0,1,0,0,0,0,1,1,1,0,1,0,0",
			"name": "Column immediately right of the middle column of rule-30 1-D cellular automaton, when started from a lone 1 cell.",
			"comment": [
				"Equally, column immediately left of the middle column of rule-86 1-D cellular automaton, when started from a lone 1 cell."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A328101/b328101.txt\"\u003eTable of n, a(n) for n = 1..100000\u003c/a\u003e",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e",
				"\u003ca href=\"/index/Ch#char_fns\"\u003eIndex entries for characteristic functions\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A000035(floor(A110240(n) / 2^(n-1))).",
				"a(n) = A000035(floor(A265281(n) / 2^(n+1)))."
			],
			"example": [
				"The evolution of one-dimensional cellular automaton rule 30 proceeds as follows, when started from a single alive (1) cell:",
				"---------------------------------------------- a(n)",
				"   0:              1",
				"   1:             11(1)                         1",
				"   2:            110(0)1                        0",
				"   3:           1101(1)11                       1",
				"   4:          11001(0)001                      0",
				"   5:         110111(1)0111                     1",
				"   6:        1100100(0)01001                    0",
				"   7:       11011110(0)111111                   0",
				"   8:      110010001(1)1000001                  1",
				"   9:     1101111011(0)01000111                 0",
				"  10:    11001000010(1)111011001                1",
				"  11:   110111100110(1)0000101111               1",
				"  12:  1100100011100(1)10011010001              1",
				"  13: 11011110110011(1)011100110111             1",
				"For this sequence, we pick the bit immediately right of the center (marked here with parentheses)."
			],
			"mathematica": [
				"Rest@ CellularAutomaton[30, {{1}, 0}, {106, {{1}}}] (* _Michael De Vlieger_, Oct 04 2019 *)"
			],
			"program": [
				"(PARI)",
				"A269160(n) = bitxor(n, bitor(2*n, 4*n)); \\\\ From A269160.",
				"A110240(n) = if(!n,1,A269160(A110240(n-1)));",
				"A328101(n) = ((A110240(n)\u003e\u003e(n-1))%2);",
				"\\\\ Use this one if writing directly to a file:",
				"A328101write(up_to) = { my(s=1, n=0); for(n=0,up_to, if(n\u003e=1,write(\"b328101.txt\", n, \" \", ((s\u003e\u003e(n-1))%2))); s = A269160(s)); };",
				"(PARI)",
				"A269161(n) = bitxor(4*n, bitor(2*n, n));",
				"A265281(n) = if(!n,1,A269161(A265281(n-1)));",
				"A328101(n) = ((A265281(n)\u003e\u003e(n+1))%2);"
			],
			"xref": [
				"Cf. A000035, A110240, A269160, A269161, A265281.",
				"Cf. also A051023, A328100, A328102."
			],
			"keyword": "nonn",
			"offset": "1",
			"author": "_Antti Karttunen_, Oct 04 2019",
			"references": 4,
			"revision": 11,
			"time": "2019-10-04T23:30:30-04:00",
			"created": "2019-10-04T23:30:30-04:00"
		}
	]
}