{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A219234",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 219234,
			"data": "1,0,0,1,1,-4,6,-4,1,0,0,16,-32,24,-8,1,1,-12,58,-144,195,-144,58,-12,1,0,0,81,-432,972,-1200,886,-400,108,-16,1,1,-24,236,-1228,3678,-6612,7490,-5532,2701,-864,174,-20,1,0,0,256,-2560,11136,-27776,44176,-47232,34912,-18048,6504,-1600,256,-24,1",
			"name": "Coefficient array for the fourth power of Chebyshev's S-polynomials as a function of x^2.",
			"comment": [
				"The row lengths sequence for this array is 2*n+1, given in A005408.",
				"The coefficient triangle for the monic Chebyshev S-polynomials S(n,x) = U(n,x/2) are given in A049310.",
				"The coefficients for S(n,x)^2 are given in A158454 and in A181878 (odd numbered rows shifted by one unit to the left)."
			],
			"formula": [
				"a(n, m) = [x^(2*m)] S(n, x)^4, n \u003e= 0, with the monic Chebyshev S-polynomials given in terms of the U-polynomials in a comment above.",
				"The o.g.f. GS4(x, z) := sum((S(n, x)^4)*z^n,n=0..infinity) = ((1+z)/(1-z))*(1 - (2-3*x^2)*z + z^2)/((1-z*(-2+x^2)+z^2)*(1-z*(2-4*x^2+x^4)+z^2)). For the o.g.f. of the row polynomials p(n,x) :=sum(a(n,m)*x^m,m=0..n) take GS4(sqrt(x), z).",
				"The row polynomial p(n, x^2) = Sum_{m=0..2*n} a(n, m)*x^(2*m) = (S(n, x))^4 = (R(4*(n+1), x) - 4*R(2*(n+1), x) + 6)/(x^2 - 4)^2, where R are the monic Chebyshev T polynomials with coefficients given in A127672. For factorizations of the S polynomials see comments on A049310. - _Wolfdieter Lang_, Apr 09 2018"
			],
			"example": [
				"The irregular triangle a(n, m) starts:",
				"n\\m  0   1   2     3    4     5    6     7    8    9  10  11 12",
				"0:   1",
				"1:   0   0   1",
				"2:   1  -4   6    -4    1",
				"3:   0   0  16   -32   24    -8    1",
				"4:   1 -12  58  -144  195  -144   58   -12    1",
				"5:   0   0  81  -432  972 -1200  886  -400  108  -16   1",
				"6:   1 -24 236 -1228 3678 -6612 7490 -5532 2701 -864 174 -20  1",
				"...",
				"Row n=7: [0, 0, 256, -2560, 11136, -27776, 44176, -47232, 34912, -18048, 6504, -1600, 256, -24, 1].",
				"Row n=8: [1, -40, 660, -5828, 30194, -96780, 203374, -293464, 300231, -222112, 119938, -47244, 13415, -2672, 354, -28, 1].",
				"Row n=1 polynomial p(1,x) = 1*x^2 = S(1,sqrt(x))^4 = (sqrt(x))^4.",
				"Row n=2 polynomial p(2,x) = 1 - 4*x + 6*x^2 - 4*x^3 + 1*x^4 =",
				"  S(2,sqrt(x))^4 = (-1+x)^4."
			],
			"xref": [
				"Cf. A049310, A127672, A158454, A181878, A219240."
			],
			"keyword": "sign,easy,tabf",
			"offset": "0,6",
			"author": "_Wolfdieter Lang_, Nov 28 2012",
			"references": 2,
			"revision": 12,
			"time": "2018-04-17T05:28:58-04:00",
			"created": "2012-11-28T13:27:04-05:00"
		}
	]
}