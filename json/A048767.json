{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A048767",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 48767,
			"data": "1,2,4,3,8,8,16,5,9,16,32,12,64,32,32,7,128,18,256,24,64,64,512,20,27,128,25,48,1024,64,2048,11,128,256,128,27,4096,512,256,40,8192,128,16384,96,72,1024,32768,28,81,54,512,192,65536,50,256,80,1024,2048",
			"name": "If n = Product (p_j^k_j) then a(n) = Product ( prime(k_j)^pi(p_j) ) where pi is A000720.",
			"comment": [
				"If the prime power factors p^e of n are replaced by prime(e)^pi(p), then the prime terms q in the sequence pertain to 2^m with m \u003e 1, since pi(2) = 1. - _Michael De Vlieger_, Apr 25 2017",
				"Also the Heinz number of the integer partition obtained by applying the map described in A217605 (which interchanges the parts with their multiplicities) to the integer partition with Heinz number n, where the Heinz number of an integer partition (y_1,...,y_k) is prime(y_1)*...*prime(y_k). The image of this map (which is the union of this sequence) is A130091. - _Gus Wiseman_, May 04 2019"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A048767/b048767.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Stephan Wagner, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v20i4p13\"\u003eThe Number of Fixed Points of Wilf's Partition Involution\u003c/a\u003e, The Electronic Journal of Combinatorics, 20(4) (2013), #P13."
			],
			"example": [
				"For n=6, 6=(2^1)*(3^1), a(6)=([ first prime ]^pi[ 2 ])*([ first prime ]^pi[ 3 ])=(2^1)*(2^2)=8.",
				"From _Gus Wiseman_, May 04 2019: (Start)",
				"For n = 1...20, the prime indices of n together with the prime indices of a(n) are the following:",
				"   1: {} {}",
				"   2: {1} {1}",
				"   3: {2} {1,1}",
				"   4: {1,1} {2}",
				"   5: {3} {1,1,1}",
				"   6: {1,2} {1,1,1}",
				"   7: {4} {1,1,1,1}",
				"   8: {1,1,1} {3}",
				"   9: {2,2} {2,2}",
				"  10: {1,3} {1,1,1,1}",
				"  11: {5} {1,1,1,1,1}",
				"  12: {1,1,2} {1,1,2}",
				"  13: {6} {1,1,1,1,1,1}",
				"  14: {1,4} {1,1,1,1,1}",
				"  15: {2,3} {1,1,1,1,1}",
				"  16: {1,1,1,1} {4}",
				"  17: {7} {1,1,1,1,1,1,1}",
				"  18: {1,2,2} {1,2,2}",
				"  19: {8} {1,1,1,1,1,1,1,1}",
				"  20: {1,1,3} {1,1,1,2}",
				"(End)"
			],
			"maple": [
				"A048767 := proc(n)",
				"    local a,p,e,f;",
				"    a := 1 ;",
				"    for f in ifactors(n)[2] do",
				"        p := op(1,f) ;",
				"        e := op(2,f) ;",
				"        a := a*ithprime(e)^numtheory[pi](p) ;",
				"    end do:",
				"    a ;",
				"end proc: # _R. J. Mathar_, Nov 08 2012"
			],
			"mathematica": [
				"Table[{p, k} = Transpose@ FactorInteger[n]; Times @@ (Prime[k]^PrimePi[p]), {n, 58}] (* _Ivan Neretin_, Jun 02 2016 *)",
				"Array[Apply[Times, FactorInteger[#] /. {p_, e_} /; e \u003e= 0 :\u003e Prime[e]^PrimePi[p]] \u0026, 65] (* _Michael De Vlieger_, Apr 25 2017 *)"
			],
			"xref": [
				"Cf. A008477, A048768, A048769, A064988.",
				"Cf. A056239, A098859, A109298, A112798, A118914, A130091, A217605, A320348, A325326, A325337."
			],
			"keyword": "easy,nonn,mult",
			"offset": "1,2",
			"author": "_Naohiro Nomoto_",
			"ext": [
				"a(1)=1 prepended by _Alois P. Heinz_, Jul 26 2015"
			],
			"references": 12,
			"revision": 33,
			"time": "2019-05-04T08:30:25-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}