{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006503",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6503,
			"id": "M2835",
			"data": "0,3,10,22,40,65,98,140,192,255,330,418,520,637,770,920,1088,1275,1482,1710,1960,2233,2530,2852,3200,3575,3978,4410,4872,5365,5890,6448,7040,7667,8330,9030,9768,10545,11362,12220,13120,14063,15050,16082,17160,18285",
			"name": "a(n) = n*(n+1)*(n+8)/6.",
			"comment": [
				"If Y is a 3-subset of an n-set X then, for n\u003e=4, a(n-4) is the number of 3-subsets of X having at most one element in common with Y. - _Milan Janjic_, Nov 23 2007",
				"The coefficient of x^3 in (1-x-x^2)^{-n} is the coefficient of x^3 in (1+x+2x^2+3x^3)^n. Using the multinomial theorem one then finds that a(n)=n(n+1)(n+8)/3!. - _Sergio Falcon_, May 22 2008"
			],
			"reference": [
				"A. H. Beiler, Recreations in the Theory of Numbers, Dover, N.Y., 1964, pp. 194-196.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A006503/b006503.txt\"\u003eTable of n, a(n) for n = 0..5000\u003c/a\u003e",
				"G. E. Bergum and V. E. Hoggatt, Jr., \u003ca href=\"/A006503/a006503.pdf\"\u003eNumerator polynomial coefficient array for the convolved Fibonacci sequence\u003c/a\u003e, Fib. Quart., 14 (1976), 43-44. (Annotated scanned copy)",
				"G. E. Bergum and V. E. Hoggatt, Jr., \u003ca href=\"http://www.fq.math.ca/Scanned/14-1/bergum.pdf\"\u003eNumerator polynomial coefficient array for the convolved Fibonacci sequence\u003c/a\u003e, Fib. Quart., 14 (1976), 43-48.",
				"M. Janjic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL13/Janjic/janjic33.html\"\u003eHessenberg Matrices and Integer Sequences \u003c/a\u003e, J. Int. Seq. 13 (2010) # 10.7.8, section 3.",
				"P. Moree, \u003ca href=\"https://arxiv.org/abs/math/0311205\"\u003eConvoluted convolved Fibonacci numbers\u003c/a\u003e, arXiv:math/0311205 [math.CO], 2003.",
				"P. Moree, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL7/Moree/moree12.htm\"\u003eConvoluted Convolved Fibonacci Numbers\u003c/a\u003e, Journal of Integer Sequences, Vol. 7 (2004), Article 04.2.2.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992; arXiv:0911.4975 [math.NT], 2009.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-6,4,-1)."
			],
			"formula": [
				"a(n) = n*(n+1)*(n+8)/6.",
				"G.f.: x*(3-2*x)/(1-x)^4.",
				"a(n) = A000292(n) + A002378(n). - _Reinhard Zumkeller_, Sep 24 2008",
				"a(n) = 4*a(n-1)-6*a(n-2)+ 4*a(n-3)- a(n-4) with a(0)=0, a(1)=3, a(2)=10, a(3)=22. - _Harvey P. Dale_, Jan 27 2016"
			],
			"maple": [
				"A006503:=-(-3+2*z)/(z-1)**4; # [_Simon Plouffe_ in his 1992 dissertation.]"
			],
			"mathematica": [
				"Clear[\"Global`*\"] a[n_] := n(n + 1)(n + 8)/3! Do[Print[n, \" \", a[n]], {n, 1, 25}] (* _Sergio Falcon_, May 22 2008 *)",
				"Table[n(n+1)(n+8)/6,{n,0,50}] (* or *) LinearRecurrence[{4,-6,4,-1},{0,3,10,22},50] (* _Harvey P. Dale_, Jan 27 2016 *)"
			],
			"program": [
				"(PARI) x='x+O('x^50); concat([0], Vec(x*(3-2*x)/(1-x)^4)) \\\\ _G. C. Greubel_, May 11 2017"
			],
			"xref": [
				"a(n) = A095660(n+2, 3): fourth column of (1, 3)-Pascal triangle.",
				"Cf. A000027, A000096, A006504.",
				"Cf. A000292, A002378.",
				"Row n=3 of A144064."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Better description from _Jeffrey Shallit_, Aug 1995"
			],
			"references": 14,
			"revision": 61,
			"time": "2021-12-13T17:51:13-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}