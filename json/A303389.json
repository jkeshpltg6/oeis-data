{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A303389",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 303389,
			"data": "0,1,1,1,1,2,1,3,2,2,2,4,3,2,2,3,3,3,2,2,2,4,3,2,1,5,4,3,2,5,5,5,5,3,3,5,5,4,4,4,5,5,2,5,3,5,4,7,2,4,6,6,5,4,4,5,8,4,4,4,7,6,4,3,4,8,4,7,3,3,6,8,2,5,6,5,4,6,4,3",
			"name": "Number of ways to write n as a*(a+1)/2 + b*(b+1)/2 + 5^c + 5^d, where a,b,c,d are nonnegative integers with a \u003c= b and c \u003c= d.",
			"comment": [
				"Conjecture: a(n) \u003e 0 for all n \u003e 1. In other words, any integers n \u003e 1 can be written as the sum of two triangular numbers and two powers of 5.",
				"This has been verified for all n = 2..10^10.",
				"See A303393 for the numbers of the form x*(x+1)/2 + 5^y with x and y nonnegative integers.",
				"See also A303401, A303432 and A303540 for similar conjectures."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A303389/b303389.txt\"\u003eTable of n, a(n) for n = 1..100000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2016.11.008\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, J. Number Theory 175(2017), 167-190.",
				"Zhi-Wei Sun, \u003ca href=\"http://maths.nju.edu.cn/~zwsun/179b.pdf\"\u003eNew conjectures on representations of integers (I)\u003c/a\u003e, Nanjing Univ. J. Math. Biquarterly 34(2017), no. 2, 97-120.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1701.05868\"\u003eRestricted sums of four squares\u003c/a\u003e, arXiv:1701.05868 [math.NT], 2017-2018."
			],
			"example": [
				"a(4) = 1 with 4 = 1*(1+1)/2 + 1*(1+1)/2 + 5^0 + 5^0.",
				"a(5) = 1 with 5 = 0*(0+1)/2 + 2*(2+1)/2 + 5^0 + 5^0.",
				"a(7) = 1 with 7 = 0*(0+1)/2 + 1*(1+1)/2 + 5^0 + 5^1.",
				"a(25) = 1 with 25 = 0*(0+1)/2 + 5*(5+1)/2 + 5^1 + 5^1."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]];",
				"f[n_]:=f[n]=FactorInteger[n];",
				"g[n_]:=g[n]=Sum[Boole[Mod[Part[Part[f[n],i],1],4]==3\u0026\u0026Mod[Part[Part[f[n],i],2],2]==1],{i,1,Length[f[n]]}]==0;",
				"QQ[n_]:=QQ[n]=(n==0)||(n\u003e0\u0026\u0026g[n]);",
				"tab={};Do[r=0;Do[If[QQ[4(n-5^j-5^k)+1],Do[If[SQ[8(n-5^j-5^k-x(x+1)/2)+1],r=r+1],{x,0,(Sqrt[4(n-5^j-5^k)+1]-1)/2}]],{j,0,Log[5,n/2]},{k,j,Log[5,n-5^j]}];tab=Append[tab,r],{n,1,80}];Print[tab]"
			],
			"xref": [
				"Cf. A000217, A000351, A271518, A273812, A281976, A299924, A299537, A299794, A300219, A300362, A300396, A300441, A301376, A301391, A301471, A301472, A302920, A302981, A302982, A302983, A302984, A302985, A303233, A303234, A303235, A303338, A303363, A303393, A303401, A303432, A303540."
			],
			"keyword": "nonn",
			"offset": "1,6",
			"author": "_Zhi-Wei Sun_, Apr 23 2018",
			"references": 30,
			"revision": 21,
			"time": "2018-06-06T11:36:19-04:00",
			"created": "2018-04-23T04:04:17-04:00"
		}
	]
}