{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A291223",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 291223,
			"data": "0,0,1,1,3,5,8,17,25,52,83,159,271,497,868,1572,2762,4984,8784,15799,27939,50089,88831,158880,282293,504179,896780,1600335,2848339,5080363,9045953,16129172,28726972,51209648,91223508,162594868,289675121,516264093",
			"name": "p-INVERT of (0,1,0,1,0,1,...), where p(S) = 1 - S^3 - S^4.",
			"comment": [
				"Suppose s = (c(0), c(1), c(2), ...) is a sequence and p(S) is a polynomial. Let S(x) = c(0)*x + c(1)*x^2 + c(2)*x^3 + ... and T(x) = (-p(0) + 1/p(S(x)))/x. The p-INVERT of s is the sequence t(s) of coefficients in the Maclaurin series for T(x). Taking p(S) = 1 - S gives the \"INVERT\" transform of s, so that p-INVERT is a generalization of the \"INVERT\" transform (e.g., A033453).",
				"See A291219 for a guide to related sequences."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A291223/b291223.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_08\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0, 4, 1, -5, -1, 4, 0, -1)"
			],
			"formula": [
				"a(n) = 4*a(n-2) + a(n-3) - 5*a(n-4) - a(n-5) + 4*a(n-6) - a(n-8) for n \u003e= 9.",
				"G.f.: x^2*(1 + x - x^2) / (1 - 4*x^2 - x^3 + 5*x^4 + x^5 - 4*x^6 + x^8). - _Colin Barker_, Aug 25 2017"
			],
			"mathematica": [
				"z = 60; s = x/(1 - x^2); p = 1 - s^3 - s^4;",
				"Drop[CoefficientList[Series[s, {x, 0, z}], x], 1]  (* A000035 *)",
				"Drop[CoefficientList[Series[1/p, {x, 0, z}], x], 1]  (* A291223 *)"
			],
			"program": [
				"(PARI) concat(vector(2), Vec(x^2*(1 + x - x^2) / (1 - 4*x^2 - x^3 + 5*x^4 + x^5 - 4*x^6 + x^8) + O(x^50))) \\\\ _Colin Barker_, Aug 25 2017"
			],
			"xref": [
				"Cf. A000035, A291219."
			],
			"keyword": "nonn,easy",
			"offset": "0,5",
			"author": "_Clark Kimberling_, Aug 24 2017",
			"references": 2,
			"revision": 8,
			"time": "2017-08-25T06:23:02-04:00",
			"created": "2017-08-25T06:23:02-04:00"
		}
	]
}