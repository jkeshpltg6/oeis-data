{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A332493",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 332493,
			"data": "1369391,87613571,1172531,216646267,251331775687,214159878489239,750247439134737983",
			"name": "The minimal Skewes number for prime n-tuplets, choosing the n-tuplet with latest occurrence of the first sign change relative to the Hardy-Littlewood prediction when more than one type of n-tuplets exists (A083409(n)\u003e1) for the given n.",
			"comment": [
				"a(n) \u003e= A210439(n). Equals A210439(n) at n=2,4,6, i.e., at those n for which there is only one type of prime n-tuplets (admissible prime n-tuples of minimal span). The corresponding minimal span (diameter) is given by A008407(n).",
				"See A210439 for more information, references and links.",
				"From _Hugo Pfoertner_, Oct 21 2021: (Start)",
				"There are two options for choosing a(8):",
				"Either one interprets \"latest occurrence\" as the largest number of 8-tuplets before the Hardy-Littlewood (H-L) prediction is exceeded, or one selects the larger value of the first 8-tuplet term causing the first crossing.",
				"In the first case, 40634356 8-tuplets of the type p + [0, 2, 6, 12, 14, 20, 24, 26] are required before the H-L prediction is exceeded with an 8-tuplet 523250002674163757 + [0, 2, 6, ...].",
				"In the second case, 20316822 8-tuplets of type p + [0, 6, 8, 14, 18, 20, 24, 26] are needed to reach the first crossing of the H-L prediction. The corresponding 8-tuplet has 750247439134737983 as first term.",
				"The interchanging is a consequence of the different H-L constants for the two tuplet types, 475.36521.. vs. 178.261954.., which have a ratio of 8/3 to one another.",
				"Since the H-L constant for the \"earliest occurrence\" A210439(8) is 178.26.., this speaks in favor of a choice from the two possibilities, which uses the same H-L constant, i.e., the occurrence with the larger tuplet start and not the occurrence with the larger number of required tuplets, for which a separate sequence A348053 is created. (End)"
			],
			"link": [
				"Tony Forbes and Norman Luhn, \u003ca href=\"http://www.pzktupel.de/ktuplets\"\u003ePrime k-tuplets\u003c/a\u003e",
				"Alexei Kourbatov, \u003ca href=\"/A332493/a332493.gp.txt\"\u003eOptimized PARI code for computing a(7)\u003c/a\u003e",
				"Norman Luhn, \u003ca href=\"http://www.pzktupel.de/smarchive.html\"\u003eDatabase of the smallest prime k-tuplets\u003c/a\u003e, compressed files.",
				"Hugo Pfoertner, \u003ca href=\"/A332493/a332493.pdf\"\u003eIllustration of growth of number of 7-tuples\u003c/a\u003e, (2020).",
				"Hugo Pfoertner, \u003ca href=\"/A332493/a332493_1.pdf\"\u003eComparison of number of octuplets needed to achieve the H-L prediction\u003c/a\u003e, (2021)."
			],
			"example": [
				"Denote by pi_n(x) the n-tuplet counting function, C_n the corresponding Hardy-Littlewood constant, and Li_n(x) the integral from 2 to x of (1/(log t)^n) dt.",
				"For 7-tuples with pattern (0 2 8 12 14 18 20) we have the Skewes number p=214159878489239; this is the initial prime p in the 7-tuple where for the first time we have pi_7(p) \u003e C_7 Li_7(p). For the other dense pattern (0 2 6 8 12 18 20), the first sign change of pi_7(x) - C_7 Li_7(x) occurs earlier, at 7572964186421. Therefore we have a(7)=214159878489239, while A210439(7)=7572964186421."
			],
			"program": [
				"(PARI) See A. Kourbatov link.",
				"(PARI) \\\\ The first result is A210439(5), the 2nd is a(5)",
				"Li(x, n)=intnum(t=2, n, 1/log(t)^x);",
				"G5=(15^4/2^11)*0.409874885088236474478781212337955277896358; \\\\ A269843",
				"n1=0;n2=0;n1found=0;n2found=0;p1=5;p2=7;p3=11;p4=13;",
				"forprime(p5=17,10^12,if(p5-p1==12,my(L=Li(5,p1));if(p2-p1==2,n1++;if(!n1found\u0026\u0026n1/L\u003eG5,print(p1,\" \",p2,\" \",n1,\" \",n1/L);n1found=1),n2++;if(!n2found\u0026\u0026n2/L\u003eG5,print(p1,\" \",p2,\" \",n2,\" \",n2/L);n2found=1)));if(n1found\u0026\u0026n2found,break);p1=p2;p2=p3;p3=p4;p4=p5) \\\\ _Hugo Pfoertner_, May 12 2020",
				"\\\\ Code for a(7), similar to A. Kourbatov's code but much shorter.",
				"\\\\ Run time approx. 2 days, prints every 1000th 7-tuple",
				"G7=(35^6/(3*2^22))*0.36943751038649868932319074987675; \\\\ A271742",
				"s=[0,2,8,12,14,18,20];",
				"r=[809, 2069, 2909, 5639, 6689, 7529, 7739, 8999, 10259, 12149, 12359, 14459, 14879, 15929, 17189, 19289, 20549, 21389, 23909, 24119, 26009, 27479, 28529, 28739];",
				"forstep(p0=0,10^15,30030,for(j=1,24,my(p1=p0+r[j],isp=1,L);for(k=1,7,my(p=p1+s[k]);if(!ispseudoprime(p),isp=0;break));if(isp,L=Li(7,p1);n++;if(n%1000==0||n/L\u003eG7,print(p1,\" \",p1+s[#s],\" \",n/L,\" \",n));if(n/L\u003eG7,break(2))))) \\\\ _Hugo Pfoertner_, May 16 2020"
			],
			"xref": [
				"Cf. A008407, A083409, A210439, A333586, A333587, A348053."
			],
			"keyword": "nonn,hard,more",
			"offset": "2,1",
			"author": "_Alexei Kourbatov_ and _Hugo Pfoertner_, May 11 2020",
			"ext": [
				"a(8) from _Norman Luhn_ and _Hugo Pfoertner_, Oct 21 2021"
			],
			"references": 8,
			"revision": 77,
			"time": "2021-10-24T08:42:10-04:00",
			"created": "2020-05-11T22:38:20-04:00"
		}
	]
}