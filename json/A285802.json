{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A285802",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 285802,
			"data": "7,35,9,18,14,12,1,9,3,7,7,6,6,5,5,11,1,4,3,35,7,8,9,3,3,3,1,17,3,9,7,18,9,5,2,2,1,2,2,18,7,9,4,4,6,6,1,12,3,14,7,11,7,5,5,12,1,3,3,12,7,6,6,9,11,11,1,4,3,1,1,1,1,1,1,1,1,1,1,9,7,7",
			"name": "Smallest number that when multiplied by n contains the digit 7.",
			"comment": [
				"Conjecture: 35 is the highest value to occur in this sequence.",
				"Conjecture is true. First of all we notice that a(10*k) = a(k), so we can consider only numbers not ending in 0. Then, it is easy to verify that for any number k between 1 and 99 not ending in 0, there is a multiple of k not larger than 35*k which has a 7 in one of its last two digits. Since the last two digits of the products only depend on the last two digits of k, this extends immediately to larger numbers. - _Giovanni Resta_, Apr 27 2017"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A285802/b285802.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"a(2) = 35 because no even number ends in 7, but we can have even numbers whose next-to-last digit is 7; the smallest number is 70, which is even, and 2 times 35 is 70."
			],
			"maple": [
				"f:= proc(n) local k;",
				"  for k from 1 do",
				"    if has(convert(n*k,base,10),7) then return k fi",
				"  od",
				"end proc:",
				"map(f, [$1..100]); # _Robert Israel_, Nov 26 2019"
			],
			"mathematica": [
				"Table[k = 1; While[DigitCount[k n, 10, 7] == 0, k++]; k, {n, 82}] (* _Michael De Vlieger_, Apr 26 2017 *)"
			],
			"program": [
				"(Python)",
				"def a(n):",
				"    k=1",
				"    while True:",
				"        if \"7\" in str(n*k): return k",
				"        k+=1",
				"print([a(n) for n in range(1, 101)]) # _Indranil Ghosh_, Apr 27 2017",
				"(PARI) a(n) = {my(k=1); while(!vecsearch(vecsort(digits(n*k)), 7), k++); k;} \\\\ _Michel Marcus_, Jun 09 2018"
			],
			"keyword": "nonn,base,easy,look",
			"offset": "1,1",
			"author": "_J. Lowell_, Apr 26 2017",
			"references": 1,
			"revision": 30,
			"time": "2021-08-31T02:43:15-04:00",
			"created": "2017-05-01T22:14:31-04:00"
		}
	]
}