{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A324543",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 324543,
			"data": "0,1,3,3,7,2,15,4,9,5,31,3,63,2,8,16,127,-1,255,4,21,16,511,8,21,20,12,27,1023,6,2047,8,20,48,20,20,4095,2,78,32,8191,-6,16383,17,9,288,32767,8,45,-3,122,45,65535,4,53,20,270,278,131071,2,262143,688,12,72,56,23,524287,125,260,-8,1048575,20,2097151,260,3,363,44,-7,4194303",
			"name": "Möbius transform of A323243, where A323243(n) = sigma(A156552(n)).",
			"comment": [
				"The first three zeros after a(1) occur at n = 192, 288, 3645.",
				"There are 630 negative terms among the first 4473 terms.",
				"Applying this function to the divisors of the first four terms of A324201 reveals the following pattern:",
				"----------------------------------------------------------------------------------",
				"A324201(n) divisors                             a(n) applied to each:         Sum",
				"        9: [1, 3, 9]                         -\u003e [0, 3, 9]                      12",
				"      125: [1, 5, 25, 125]                   -\u003e [0, 7, 21, 28]                 56",
				"   161051: [1, 11, 121, 1331, 14641, 161051] -\u003e [0, 31, 93, 124, 496, 248]    496",
				"410338673: [1, 17, 289, 4913, 83521, 1419857, 24137569, 410338673]",
				"                             -\u003e [0, 127, 381, 508, 2032, 1016, 9144, 3048]  16256.",
				"The second term (the first nonzero) of the latter list = A000668(n), and the sum is always twice the corresponding (even) perfect number, which forces either it or at least many of its divisors to be present. For example, in the fourth case, although 8128 = A000396(4) itself is not present, we still have 127, 508, 1016 and 2032 in the list. See also A329644."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A324543/b324543.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (based on Hans Havermann's factorization of A156552)",
				"\u003ca href=\"/index/Pri#prime_indices\"\u003eIndex entries for sequences computed from indices in prime factorization\u003c/a\u003e",
				"\u003ca href=\"/index/Si#SIGMAN\"\u003eIndex entries for sequences related to sigma(n)\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{d|n} A008683(n/d) * A323243(d).",
				"a(A000040(n)) = A000225(n).",
				"a(A001248(n)) = A173033(n) - A000225(n) = A068156(n) = 3*(2^n - 1).",
				"a(2*A000040(n)) = A324549(n).",
				"a(A002110(n)) = A324547(n).",
				"a(n) = 2*A297112(n) - A329644(n), and for n \u003e 1, a(n) = 2^A297113(n) - A329644(n). - _Antti Karttunen_, Dec 08 2019"
			],
			"mathematica": [
				"Table[DivisorSum[n, MoebiusMu[n/#] If[# == 1, 0, DivisorSigma[1, Floor@ Total@ Flatten@ MapIndexed[#1 2^(#2 - 1) \u0026, Flatten[Table[2^(PrimePi@ #1 - 1), {#2}] \u0026 @@@ FactorInteger@ #]]]] \u0026], {n, 79}] (* _Michael De Vlieger_, Mar 11 2019 *)"
			],
			"program": [
				"(PARI)",
				"A064989(n) = {my(f); f = factor(n); if((n\u003e1 \u0026\u0026 f[1,1]==2), f[1,2] = 0); for (i=1, #f~, f[i,1] = precprime(f[i,1]-1)); factorback(f)};",
				"A156552(n) = if(1==n, 0, if(!(n%2), 1+(2*A156552(n/2)), 2*A156552(A064989(n))));",
				"memoA323243 = Map();",
				"A323243(n) = if(1==n, 0, my(v); if(mapisdefined(memoA323243,n,\u0026v),v, v=sigma(A156552(n)); mapput(memoA323243,n,v); (v)));",
				"A324543(n) = sumdiv(n,d,moebius(n/d)*A323243(d));"
			],
			"xref": [
				"Cf. A000040, A000043, A000668, A000203, A000225, A000396, A008683, A068156, A156552, A173033, A297112, A297113, A323243, A323244, A324201, A324542, A324547, A324548, A324549, A324712, A329644."
			],
			"keyword": "sign",
			"offset": "1,3",
			"author": "_Antti Karttunen_, Mar 07 2019",
			"references": 15,
			"revision": 51,
			"time": "2019-12-08T07:44:24-05:00",
			"created": "2019-03-15T12:35:58-04:00"
		}
	]
}