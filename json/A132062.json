{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A132062",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 132062,
			"data": "1,0,1,0,1,1,0,3,3,1,0,15,15,6,1,0,105,105,45,10,1,0,945,945,420,105,15,1,0,10395,10395,4725,1260,210,21,1,0,135135,135135,62370,17325,3150,378,28,1,0,2027025,2027025,945945,270270,51975,6930,630,36,1,0",
			"name": "Sheffer triangle (1,1-sqrt(1-2*x)). Extended Bessel triangle A001497.",
			"comment": [
				"This is a Jabotinsky type exponential convolution triangle related to A001147 (double factorials). For Jabotinsky type triangles See the D. E. Knuth reference given under A039692.",
				"The subtriangle (n\u003e=m\u003e=1) is A001497(n,m) (Bessel).",
				"For the combinatorial interpretation in terms of unordered forests of increasing plane trees see the W. Lang comment and example under A001497.",
				"This is a special type of Sheffer triangle. See the S. Roman reference given under A048854 (the notation here differs).",
				"This triangle (or the A001497 subtriangle) appears as generalized Stirling numbers of the second kind, S2p(-1,n,m):=S2(-k;m,m)*(-1)^(n-m) for k=1, eqs. (27)-(29) of the W. Lang reference.",
				"Also the Bell transform of the double factorial of odd numbers A001147. For the Bell transform of the double factorial of even numbers A000165 see A039683. For the definition of the Bell transform see A264428. - _Peter Luschny_, Dec 20 2015"
			],
			"reference": [
				"Toufik Mansour, Matthias Schork and Mark Shattuck, On the Stirling numbers associated with the meromorphic Weyl algebra, Applied Mathematics Letters, Volume 25, Issue 11, November 2012, Pages 1767-1771. - From _N. J. A. Sloane_, Sep 15 2012",
				"Steven Roman, The Umbral Calculus, Pure and Applied Mathematics, 111, Academic Press, 1984. (p. 78)  [_Emanuele Munarini_, Oct 10 2017]"
			],
			"link": [
				"Leonard Carlitz, \u003ca href=\"https://doi.org/10.1215/S0012-7094-57-02421-3\"\u003eA Note on the Bessel Polynomials\u003c/a\u003e, Duke Math. J. 24 (2) (1957), 151-162. [_Emanuele Munarini_, Oct 10 2017]",
				"H. Han, S. Seo, \u003ca href=\"http://dx.doi.org/10.1016/j.ejc.2007.12.002\"\u003eCombinatorial proofs of inverse relations and log-concavity for Bessel numbers\u003c/a\u003e, Eur. J. Combinat. 29 (7) (2008) 1544-1554. [From _R. J. Mathar_, Mar 20 2009]",
				"W. Lang, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL3/LANG/lang.html\"\u003eOn generalizations of Stirling number triangles\u003c/a\u003e, J. Integer Seqs., Vol. 3 (2000), #00.2.4.",
				"W. Lang, \u003ca href=\"/A132062/a132062.txt\"\u003eFirst 10 rows\u003c/a\u003e."
			],
			"formula": [
				"a(n,m)=0 if n\u003cm; a(n,0)=1 if n=1, else 0; a(n,m)=(2*(n-1)-m)*a(n-1,m) + a(n-1,m-1).",
				"E.g.f. m-th column ((x*f2p(1;x))^m)/m!, m\u003e=0. with f2p(1;x):=1-sqrt(1-2*x)= x*c(x/2) with the o.g.f.of A000108 (Catalan).",
				"From _Emanuele Munarini_, Oct 10 2017: (Start)",
				"a(n,k) = binomial(2*n-2*k,n-k)*binomial(2*n-k-1,k-1)*(n-k)!/2^(n-k).",
				"The row polynomials p_n(x) (studied by Carlitz) satisfy the recurrence: p_{n+2}(x) - (2*n+1)*p_{n+1}(x) - x^2*p_n(x) = 0. (End)"
			],
			"example": [
				"[1]",
				"[0,      1]",
				"[0,      1,      1]",
				"[0,      3,      3,     1]",
				"[0,     15,     15,     6,     1]",
				"[0,    105,    105,    45,    10,    1]",
				"[0,    945,    945,   420,   105,   15,   1]",
				"[0,  10395,  10395,  4725,  1260,  210,  21,  1]",
				"[0, 135135, 135135, 62370, 17325, 3150, 378, 28, 1]"
			],
			"maple": [
				"# The function BellMatrix is defined in A264428.",
				"BellMatrix(n -\u003e doublefactorial(2*n-1), 9); # _Peter Luschny_, Jan 27 2016"
			],
			"mathematica": [
				"Table[If[k \u003c= n, Binomial[2n-2k,n-k] Binomial[2n-k-1,k-1] (n-k)!/2^(n-k), 0], {n, 0, 6}, {k, 0, n}] // Flatten (* _Emanuele Munarini_, Oct 10 2017 *)",
				"BellMatrix[f_Function, len_] := With[{t = Array[f, len, 0]}, Table[BellY[n, k, t], {n, 0, len - 1}, {k, 0, len - 1}]];",
				"rows = 10;",
				"M = BellMatrix[(2#-1)!!\u0026, rows];",
				"Table[M[[n, k]], {n, 1, rows}, {k, 1, n}] // Flatten (* _Jean-François Alcover_, Jun 23 2018, after _Peter Luschny_ *)"
			],
			"program": [
				"(Sage) # uses[bell_transform from A264428]",
				"def A132062_row(n):",
				"    a = sloane.A001147",
				"    dblfact = a.list(n)",
				"    return bell_transform(n, dblfact)",
				"[A132062_row(n) for n in (0..9)] # _Peter Luschny_, Dec 20 2015"
			],
			"xref": [
				"Columns m=1: A001147.",
				"Row sums give [1, A001515]. Alternating row sums give [1, -A000806].",
				"Cf. A122850. - _R. J. Mathar_, Mar 20 2009",
				"Cf. A039683, A264428."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,8",
			"author": "_Wolfdieter Lang_ Sep 14 2007",
			"references": 8,
			"revision": 34,
			"time": "2020-03-27T06:59:11-04:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}