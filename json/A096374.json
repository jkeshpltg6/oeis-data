{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A096374",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 96374,
			"data": "0,1,0,3,1,5,4,11,8,19,19,35,36,59,65,104,115,168,196,276,321,440,521,694,821,1072,1277,1644,1957,2477,2959,3705,4411,5472,6516,8014,9524,11620,13789,16724,19798,23860,28202,33815,39864,47579,55979,66520,78080",
			"name": "Number of partitions of n such that the least part occurs with even multiplicity.",
			"comment": [
				"Also number of partitions of n such that the difference between the two largest distinct parts is even (it is assumed that 0 is a part in each partition). Example: a(6)=5 because we have [6],[5,1],[4,2],[2,2,2] and [3,1,1,1]. - _Emeric Deutsch_, Apr 04 2006"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A096374/b096374.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e"
			],
			"formula": [
				"G.f.: Sum_{m\u003e=1} ((x^(2*m)/(1+x^m))/Product_{i\u003e=m}(1-x^i)).",
				"a(n) + A096375(n) = A000041(n)."
			],
			"example": [
				"a(6) = 5 because we have [4,1,1], [3,3], [2,2,1,1], [2,1,1,1,1] and [1,1,1,1,1,1]."
			],
			"maple": [
				"g:=sum(x^(2*k)/(1+x^k)/product(1-x^j,j=k..70),k=1..50): gser:=series(g,x=0,50): seq(coeff(gser,x,n),n=1..48); # _Emeric Deutsch_, Apr 04 2006",
				"# second Maple program:",
				"b:= proc(n, i) option remember; `if`(n=0 or i\u003c1, 0, `if`(irem(n, i, 'r')=0",
				"      and irem(r, 2)=0, 1, 0)+ add(b(n-i*j, i-1), j=0..n/i))",
				"    end:",
				"a:= n-\u003e b(n, n):",
				"seq(a(n), n=1..50);  # _Alois P. Heinz_, Feb 27 2013"
			],
			"mathematica": [
				"f[n_] := Block[{p = IntegerPartitions[n], l = PartitionsP[n], c = 0, k = 1}, While[k \u003c l + 1, If[ EvenQ[ Count[ p[[k]], p[[k]][[ -1]] ]], c++ ]; k++ ]; c]; Table[ f[n], {n, 50}] (* _Robert G. Wilson v_, Jul 23 2004 *)",
				"b[n_, i_] := b[n, i] = If[n == 0 || i \u003c 1, 0, {q, r} = QuotientRemainder[n, i]; If[r == 0 \u0026\u0026 Mod[q, 2] == 0, 1, 0] + Sum[b[n - i*j, i-1], {j, 0, n/i}]] ; a[n_] := b[n, n]; Table[a[n], {n, 1, 50}] (* _Jean-François Alcover_, Jan 24 2014, after _Alois P. Heinz_ *)",
				"Table[Count[IntegerPartitions[n],_?(EvenQ[Length[Split[#][[-1]]]]\u0026)],{n,50}] (* _Harvey P. Dale_, Jun 02 2019 *)"
			],
			"program": [
				"(PARI) {q=sum(m=1,100,(x^(2*m)/(1+x^m))/prod(i=m,100,1-x^i,1+O(x^60)), 1+O(x^60));for(n=1,48,print1(polcoeff(q,n),\",\"))} \\\\ _Klaus Brockhaus_, Jul 21 2004"
			],
			"xref": [
				"Cf. A000041, A096375."
			],
			"keyword": "easy,nonn",
			"offset": "1,4",
			"author": "_Vladeta Jovovic_, Jul 19 2004",
			"ext": [
				"Edited and extended by _Robert G. Wilson v_ and _Klaus Brockhaus_, Jul 21 2004"
			],
			"references": 3,
			"revision": 22,
			"time": "2019-06-02T14:03:49-04:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}