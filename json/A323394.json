{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A323394",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 323394,
			"data": "1,3,4,7,6,2,8,5,3,18,12,18,14,14,14,11,18,19,10,32,22,36,24,30,21,32,20,36,20,52,32,43,48,44,38,51,38,40,46,70,42,76,44,74,58,62,48,84,47,83,62,88,54,80,62,80,60,70,50,48,62,96,84,7,74,24,68,6",
			"name": "Carryless sum of divisors of n.",
			"comment": [
				"This sequence is a variant of A178910 for the base 10."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A323394/b323394.txt\"\u003eTable of n, a(n) for n = 1..19999\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A323394/a323394.png\"\u003eScatterplot of the first 1000000 terms\u003c/a\u003e",
				"\u003ca href=\"/index/Ca#CARRYLESS\"\u003eIndex entries for sequences related to carryless arithmetic\u003c/a\u003e",
				"\u003ca href=\"/index/Si#SIGMAN\"\u003eIndex entries for sequences related to sigma(n)\u003c/a\u003e"
			],
			"formula": [
				"a(n) \u003c= A000203(n)."
			],
			"example": [
				"For n = 42:",
				"- the divisors of 42 are: 1, 2, 3, 6, 7, 14, 21, 42,",
				"- the sum of the units is: 1 + 2 + 3 + 6 + 7 + 4 + 1 + 2 = 26 == 6 (mod 10),",
				"- the sum of the tens is: 1 + 2 + 4 = 7,",
				"- hence a(42) = 76.",
				"For n = 973:",
				"- the divisors of 973 are: 1, 7, 139, 973,",
				"- the sum of the units is: 1 + 7 + 9 + 3 = 20 == 0 (mod 10),",
				"- the sum of the tens is: 3 + 7 = 10 == 0 (mod 10),",
				"- the sum of the hundreds is: 1 + 9 = 10 == 0 (mod 10),",
				"- hence a(973) = 0."
			],
			"maple": [
				"f:= proc(n) local t,d,dd,m,i;",
				"t:= Vector(convert(n,base,10));",
				"for d in numtheory:-divisors(n) minus {n} do",
				"  dd:= convert(d,base,10);",
				"  m:= nops(dd);",
				"  t[1..m]:= t[1..m] + Vector(dd) mod 10;",
				"od:",
				"add(t[i]*10^(i-1),i=1..ilog10(n)+1)",
				"end proc:",
				"map(f, [$1..100]); # _Robert Israel_, Jan 15 2019"
			],
			"program": [
				"(PARI) a(n, base=10) = my (v=[]); fordiv (n, d, my (w=Vecrev(digits(d, base))); v=vector(max(#v, #w), k, (if (k\u003e#v, w[k], k\u003e#w, v[k], (v[k]+w[k])%base)))); fromdigits(Vecrev(v), base)"
			],
			"xref": [
				"Cf. A000203, A169890, A178910, A323414 (positions of zeros), A323415 (fixed points)."
			],
			"keyword": "nonn,base",
			"offset": "1,2",
			"author": "_Rémy Sigrist_, Jan 13 2019",
			"references": 3,
			"revision": 16,
			"time": "2019-01-18T13:00:17-05:00",
			"created": "2019-01-17T15:55:49-05:00"
		}
	]
}