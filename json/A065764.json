{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A065764",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 65764,
			"data": "1,7,13,31,31,91,57,127,121,217,133,403,183,399,403,511,307,847,381,961,741,931,553,1651,781,1281,1093,1767,871,2821,993,2047,1729,2149,1767,3751,1407,2667,2379,3937,1723,5187,1893,4123,3751,3871,2257,6643",
			"name": "Sum of divisors of square numbers.",
			"comment": [
				"Unlike A065765, the sums of divisors of squares give remainders r=1,3,5 modulo 6: sigma(4)==1, sigma(49)==3, sigma(2401)==5 (mod 6). See also A097022.",
				"a(n) is also the number of ordered pairs of positive integers whose LCM is n, (see LeVeque). - _Enrique Pérez Herrero_, Aug 26 2013",
				"Main diagonal of A319526. - _Omar E. Pol_, Sep 25 2018",
				"Subsequence of primes is A023195 \\ {3}; also, 31 is the only known prime to be twice in the data because 31 = sigma(16) = sigma(25) (see A119598 and Goormaghtigh conjecture link). - _Bernard Schott_, Jan 17 2021"
			],
			"reference": [
				"W. J. LeVeque, Fundamentals of Number Theory, pp. 125 Problem 4, Dover NY 1996."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A065764/b065764.txt\"\u003eTable of n, a(n) for n=1..10000\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Goormaghtigh_conjecture\"\u003eGoormaghtigh conjecture\u003c/a\u003e."
			],
			"formula": [
				"a(n) = sigma(n^2) = A000203(A000290(n)).",
				"Multiplicative with a(p^e) = (p^(2*e+1)-1)/(p-1). - _Vladeta Jovovic_, Dec 01 2001",
				"Dirichlet g.f.: zeta(s)*zeta(s-1)*zeta(s-2)/zeta(2*s-2), inverse Mobius transform of A000082. - _R. J. Mathar_, Mar 06 2011",
				"Dirichlet convolution of A001157 by the absolute terms of A055615. Also the Dirichlet convolution of A048250 by A000290. - _R. J. Mathar_, Apr 12 2011",
				"a(n) = Sum_{d|n} d*Psi(d), where Psi is A001615. - _Enrique Pérez Herrero_, Feb 25 2012",
				"a(n) \u003e= (n+1) * sigma(n) - n, where sigma is A000203, equality holds if n is in A000961. - _Enrique Pérez Herrero_, Apr 21 2012",
				"Sum_{k=1..n} a(k) ~ 5*Zeta(3) * n^3 / Pi^2. - _Vaclav Kotesovec_, Jan 30 2019",
				"Sum_{k\u003e=1} 1/a(k) = 1.3947708738535614499846243600124612760835313454790187655653356563282177118... - _Vaclav Kotesovec_, Sep 20 2020"
			],
			"maple": [
				"with(numtheory): [sigma(n^2)$n=1..50]; # _Muniru A Asiru_, Jan 01 2019"
			],
			"mathematica": [
				"Table[Plus@@Divisors[n^2], {n, 48}] (* _Alonso del Arte_, Feb 24 2012 *)",
				"f[p_, e_] := (p^(2*e + 1) - 1)/(p - 1); a[1] = 1; a[n_] := Times @@ f @@@ FactorInteger[n]; Array[a, 50] (* _Amiram Eldar_, Sep 10 2020 *)"
			],
			"program": [
				"(MuPAD) numlib::sigma(n^2)$ n=1..81 // _Zerinvary Lajos_, May 13 2008",
				"(Sage) [sigma(n^2,1)for n in range(1,49)] # _Zerinvary Lajos_, Jun 13 2009",
				"(PARI) a(n) = sigma(n^2); \\\\ _Harry J. Smith_, Oct 30 2009",
				"(MAGMA) [SumOfDivisors(n^2): n in [1..48]]; // _Bruno Berselli_, Apr 12 2011",
				"(GAP) a:=List([1..50],n-\u003eSigma(n^2));; Print(a); # _Muniru A Asiru_, Jan 01 2019"
			],
			"xref": [
				"Cf. A000203, A000290, A028982, A319526."
			],
			"keyword": "nonn,easy,mult",
			"offset": "1,2",
			"author": "_Labos Elemer_, Nov 19 2001",
			"references": 43,
			"revision": 79,
			"time": "2021-01-19T06:28:26-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}