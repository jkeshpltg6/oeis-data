{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A245032",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 245032,
			"data": "108,175,176,135,76,23,0,31,140,351,688,1175,1836,2695,3776,5103,6700,8591,10800,13351,16268,19575,23296,27455,32076,37183,42800,48951,55660,62951,70848,79375,88556,98415,108976,120263,132300,145111,158720,173151,188428",
			"name": "a(n) = 27*(n - 6)^2 + 4*(n - 6)^3 = ((n - 6)^2)*(4*n + 3).",
			"comment": [
				"The discriminant D of the Cardano Tartaglia cubic equation x^3 + p*x +q = 0 is D = -27*q^2 - 4*p^3, D \u003c 0. Let D = (-1)*D then D = 27*q^2 + 4*p^3, D \u003e 0, q = p \u003e -6. So D = 27*(n - 6)^2 + 4*(n - 6)^3, D \u003e 0, q = p = n, n \u003e= 0 for all real solutions of D as positive integers. The case D \u003c 0 is treated in A245033. Remark: ((n - 6)^2)*(4*n + 3) = ((6 - n)^2)*(4*n + 3)."
			],
			"link": [
				"Freimut Marschner, \u003ca href=\"/A245032/b245032.txt\"\u003eTable of n, a(n) for n = 0..9999\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-6,4,-1)."
			],
			"formula": [
				"a(n) = 27*(n - 6)^2 + 4*(n - 6)^3 = ((n - 6)^2)*(4*n + 3).",
				"G.f.: (49*x^3+124*x^2-257*x+108) / (x-1)^4. - _Colin Barker_, Jul 11 2014",
				"a(n) = (4n + 3) * (n - 6)^2 = A004767(n) * A000290(n-6). - _Wesley Ivan Hurt_, Jul 18 2014"
			],
			"example": [
				"n = 0, a(0) = 27*(-6)^2 + 4*(-6)^3 = 27*36 + 4*(-216) = 108;",
				"n = 7, a(7) = 27*(1)^2 + 4*(1)^3 = 27 + 4 = 31."
			],
			"maple": [
				"A245032:=n-\u003e(4*n+3)*(n-6)^2: seq(A245032(n), n=0..50); # _Wesley Ivan Hurt_, Jul 18 2014"
			],
			"mathematica": [
				"Table[(4 n + 3)(n - 6)^2, {n, 0, 50}] (* _Wesley Ivan Hurt_, Jul 18 2014 *)",
				"LinearRecurrence[{4,-6,4,-1},{108,175,176,135},50] (* _Harvey P. Dale_, Oct 13 2019 *)"
			],
			"program": [
				"(PARI) vector(100, n, (n-7)^2*(4*n-1)) \\\\ _Colin Barker_, Jul 11 2014",
				"(PARI) Vec((49*x^3+124*x^2-257*x+108)/(x-1)^4 + O(x^100)) \\\\ _Colin Barker_, Jul 11 2014",
				"(MAGMA) [(4*n+3)*(n-6)^2 : n in [0..50]]; // _Wesley Ivan Hurt_, Jul 18 2014"
			],
			"xref": [
				"Cf. A028347 (Discriminant of quadratic equation : a(n) = n^2 - 4*n, n \u003e 2), A245033 (a(n) = 4*(n + 7)^3 - 27*(n + 7)^2 = (4*n +1)*(n+7)^2), A245035 (a(n) = (-1)*( -27*(prime(n) - 7)^2 - 4*(prime(n) - 7)^3 ) = (prime(n) - 7)^2 * (4* prime(n) - 1) with n \u003e= 1), A245036 (a(n) = 4*prime(n)^3 - 27*prime(n)^2 = (prime(n)^2)*[4*prime(n) - 27], n \u003e= 4), A004767 (4*n+3)."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_Freimut Marschner_, Jul 10 2014",
			"references": 1,
			"revision": 37,
			"time": "2019-10-13T11:02:39-04:00",
			"created": "2014-08-21T23:40:50-04:00"
		}
	]
}