{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A221672",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 221672,
			"data": "1,2,3,5,8,13,16,23,27,36,41,52",
			"name": "Length of shortest non-constant arithmetic progression (AP) containing n squares.",
			"comment": [
				"Same as where records occur in A221671 (maximum number of squares in a non-constant AP of length n).",
				"González-Jiménez and Xarles (2013) conjecture that for n \u003e= 5 the sequence a(n)-1 equals the tail 7, 12, 15, 22, 26, 35, 40, 51, ... of A001318 (generalized pentagonal numbers k*(3*k-1)/2 for k = 0, +-1, +-2, ...). They prove it up to a(12)-1 = 51 = 6*(3*6-1)/2.",
				"See A221671 for additional comments.",
				"Also 8, 13, 16, 23, 27, 36, 41, 52 are where records occur for 8 \u003c= n \u003c= 52 in A193832 (number of squares in the arithmetic progression {24k + 1: 0 \u003c= k \u003c= n-1} [Granville]). - _Jonathan Sondow_, Dec 15 2017"
			],
			"reference": [
				"L. E. Dickson, History of the Theory of Numbers, Vol. II, Chelsea, New York, 1952, pp. 435-440."
			],
			"link": [
				"Enrique González-Jiménez and Xavier Xarles, \u003ca href=\"http://arxiv.org/abs/1301.5122\"\u003eOn a conjecture of Rudin on squares in Arithmetic Progressions\u003c/a\u003e, arXiv 2013."
			],
			"formula": [
				"A221671(a(n)) = n.",
				"a(n) \u003c= A001318(n)+1. (Proof. As 24*k*(3*k-1)/2 + 1 = (6*k-1)^2, a term in the AP 24*m+1 is a square when m is in A001318. Thus the AP 24*m+1 for m = 0, 1, ..., A001318(n) contains n squares and has length A001318(n)+1.)"
			],
			"example": [
				"The AP 1, 25, 49 = 1^2, 5^2, 7^2 shows that a(n) = n for n = 1, 2, 3 (see A216869).",
				"By Fermat and Euler, no four squares are in AP, so the AP 49, 169, 289, 409, 529 = 7^2, 13^2, 17^2, 409, 23^2 shows that a(4) = 5 (see Dickson and A216870).",
				"As k*(3*k-1)/2 = 0, 1, 2, 5, 7 for k = 0, +-1, +-2, and 24*k*(3*k-1)/2 + 1 = (6*k-1)^2 is a square, the AP 24*n+1 for the 8 numbers n = 0, 1, ..., 7 contains 5 squares, so a(5) \u003c= 8. González-Jiménez and Xarles (2013) prove a(5) \u003e 7, so a(5) = 8."
			],
			"xref": [
				"Cf. A001318, A080995, A193832, A216869, A216870, A221671."
			],
			"keyword": "nonn,hard,more",
			"offset": "1,2",
			"author": "_Jonathan Sondow_, Jan 28 2013",
			"references": 5,
			"revision": 14,
			"time": "2017-12-15T08:27:04-05:00",
			"created": "2013-01-29T16:20:21-05:00"
		}
	]
}