{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A328444",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 328444,
			"data": "1,2,3,5,4,9,13,11,6,17,23,8,31,39,7,46,53,33,43,19,62,27,89,29,59,22,81,103,92,15,107,61,12,73,85,79,41,10,51,34,95,129,14,143,157,20,177,197,187,16,203,219,211,86,99,37,68,21,18,24,42,66,36,102",
			"name": "Lexicographically earliest sequence of distinct positive numbers such that a(1) = 1, a(2) = 2, and for n \u003e 2, a(n) divides Sum_{i = n-k..n-1} a(i) with k \u003e 0 as small as possible.",
			"comment": [
				"When computing a(n) for n \u003e 2, there may be candidates for different values of k; we choose the candidate that minimizes k.",
				"This sequence is an infinite variant of A085947; a(n) = A085947(n) for n = 1..39."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A328444/b328444.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A328444/a328444.gp.txt\"\u003ePARI program for A328444\u003c/a\u003e"
			],
			"formula": [
				"a(n) \u003c= Sum_{k = 1..n-1} a(k) for any n \u003e 2."
			],
			"example": [
				"For n = 3:",
				"- the divisors of a(2) = 2 are all already in the sequence,",
				"- 3 is the least divisor of a(1) + a(2) = 1 + 2 = 3 not yet in the sequence,",
				"- so a(3) = 3.",
				"For n = 4:",
				"- the divisors of a(3) = 3 are all already in the sequence,",
				"- 5 is the least divisor of a(2) + a(3) = 2 + 3 = 5 not yet in the sequence,",
				"- so a(3) = 5.",
				"For n = 5:",
				"- the divisors of a(4) = 5 are all already in the sequence,",
				"- 4 is the least divisor of a(3) + a(4) = 3 + 5 = 8 not yet in the sequence,",
				"- so a(5) = 4."
			],
			"program": [
				"(PARI) See Links section."
			],
			"xref": [
				"See A328443 for a similar sequence.",
				"Cf. A085947."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Rémy Sigrist_, Oct 15 2019",
			"references": 3,
			"revision": 17,
			"time": "2019-10-19T10:27:15-04:00",
			"created": "2019-10-16T13:23:22-04:00"
		}
	]
}