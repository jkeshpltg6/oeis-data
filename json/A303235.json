{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A303235",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 303235,
			"data": "0,1,2,3,4,5,4,5,6,6,6,8,7,7,8,8,8,10,10,10,10,9,9,11,9,10,11,10,9,12,10,11,14,13,11,14,12,12,13,15,12,14,12,13,14,14,14,15,13,11,14,13,11,16,13,10,11,13,11,14",
			"name": "Number of ordered pairs (x, y) with 0 \u003c= x \u003c= y such that n - 2^x - 2^y can be written as the sum of two triangular numbers.",
			"comment": [
				"Conjecture: a(n) \u003e 0 for all n \u003e 1.",
				"Note that a nonnegative integer m is the sum of two triangular numbers if and only if 4*m+1 (or 8*m+2) can be written as the sum of two squares.",
				"We have verified a(n) \u003e 0 for all n = 2..4*10^8. See also the related sequences A303233 and A303234."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A303235/b303235.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2016.11.008\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, J. Number Theory 175(2017), 167-190.",
				"Zhi-Wei Sun, \u003ca href=\"http://maths.nju.edu.cn/~zwsun/179b.pdf\"\u003eNew conjectures on representations of integers (I)\u003c/a\u003e, Nanjing Univ. J. Math. Biquarterly 34(2017), no. 2, 97-120.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1701.05868\"\u003eRestricted sums of four squares\u003c/a\u003e, arXiv:1701.05868 [math.NT], 2017-2018."
			],
			"example": [
				"a(2) = 1 with 2 - 2^0 - 2^0 = 0*(0+1)/2 + 0*(0+1)/2.",
				"a(3) = 2 with 3 - 2^0 - 2^0 = 0*(0+1)/2 + 1*(1+1)/2 and 3 - 2^0 - 2^1 = 0*(0+1)/2 + 0*(0+1)/2."
			],
			"mathematica": [
				"f[n_]:=f[n]=FactorInteger[n];",
				"g[n_]:=g[n]=Sum[Boole[Mod[Part[Part[f[n],i],1],4]==3\u0026\u0026Mod[Part[Part[f[n],i],2],2]==1],{i,1,Length[f[n]]}]==0;",
				"QQ[n_]:=QQ[n]=(n==0)||(n\u003e0\u0026\u0026g[n]);",
				"tab={};Do[r=0;Do[If[QQ[4(n-2^k-2^j)+1],r=r+1],{k,0,Log[2,n]-1},{j,k,Log[2,n-2^k]}];tab=Append[tab,r],{n,1,60}];Print[tab]"
			],
			"xref": [
				"Cf. A000079, A000217, A271518, A281976, A299924, A299537, A299794, A300219, A300362, A300396, A300441, A301376, A301391, A301471, A301472, A302920, A302981, A302982, A302983, A302984, A302985, A303233, A303234."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Zhi-Wei Sun_, Apr 20 2018",
			"references": 5,
			"revision": 9,
			"time": "2018-04-20T13:18:08-04:00",
			"created": "2018-04-20T13:18:08-04:00"
		}
	]
}