{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A331447",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 331447,
			"data": "1,1,1,2,1,1,2,2,1,1,4,3,2,1,1,5,4,3,2,1,1,8,6,5,3,2,1,1,10,9,6,5,3,2,1,1,15,12,10,7,5,3,2,1,1,20,17,13,10,7,5,3,2,1,1,28,23,19,14,11,7,5,3,2,1,1,36,31,25,20,14,11,7,5,3,2,1,1,50,42",
			"name": "Triangle read by rows: T(n,k) (n \u003e= 0, -1 \u003c= k \u003c= n-1) = number of partitions of n into nonnegative integer parts with rank k.",
			"comment": [
				"In contrast to A063995 and A105806, here we allow parts that are zeros."
			],
			"link": [
				"Lars Blomberg, \u003ca href=\"/A331447/b331447.txt\"\u003eTable of n, a(n) for n = -1..5048\u003c/a\u003e",
				"Alexander Berkovich and Frank G. Garvan, \u003ca href=\"https://doi.org/10.1006/jcta.2002.3281\"\u003eSome observations on Dyson's new symmetries of partitions\u003c/a\u003e, Journal of Combinatorial Theory, Series A 100.1 (2002): 61-93.",
				"Freeman J. Dyson, \u003ca href=\"https://doi.org/10.1016/S0021-9800(69)80006-2\"\u003eA new symmetry of partitions\u003c/a\u003e, Journal of Combinatorial Theory 7.1 (1969): 56-61. See Table 2.",
				"Freeman J. Dyson, \u003ca href=\"https://doi.org/10.1016/0097-3165(89)90043-5\"\u003eMappings and symmetries of partitions\u003c/a\u003e, J. Combin. Theory Ser. A 51 (1989), 169-180."
			],
			"formula": [
				"See Dyson (1969)."
			],
			"example": [
				"Triangle begins:",
				"   1,",
				"   1, 1,",
				"   2, 1, 1,",
				"   2, 2, 1, 1,",
				"   4, 3, 2, 1, 1,",
				"   5, 4, 3, 2, 1, 1,",
				"   8, 6, 5, 3, 2, 1, 1,",
				"  10, 9, 6, 5, 3, 2, 1, 1,",
				"  ...",
				"If we include negative values of the rank k, we get the following table, taken from Dyson (1969):",
				"  n\\k| -6  -5  -4  -3  -2  -1   0   1   2   3   4   5   6",
				"  ---+---------------------------------------------------",
				"   0 |  1,  1,  1,  1,  1,  1,  0,  0,  0,  0,  0,  0,  0, ...",
				"   1 |  1,  1,  1,  1,  1,  1,  1,  0,  0,  0,  0,  0,  0, ...",
				"   2 |  2,  2,  2,  2,  2,  2,  1,  1,  0,  0,  0,  0,  0, ...",
				"   3 |  3,  3,  3,  3,  3,  2,  2,  1,  1,  0,  0,  0,  0, ...",
				"   4 |  5,  5,  5,  5,  4,  4,  3,  2,  1,  1,  0,  0,  0, ...",
				"   5 |  7,  7,  7,  6,  6,  5,  4,  3,  2,  1,  1,  0,  0, ...",
				"   6 | 11, 11, 10, 10,  9,  8,  6,  5,  3,  2,  1,  1,  0, ...",
				"   7 | 15, 14, 14, 13, 12, 10,  9,  6,  5,  3,  2,  1,  1, ...",
				"   ...",
				"Starting at column k=-1 gives the present triangle."
			],
			"xref": [
				"For the rank of a partition see A063995, A105806."
			],
			"keyword": "nonn,tabl",
			"offset": "-1,4",
			"author": "_N. J. A. Sloane_, Jan 23 2020",
			"ext": [
				"a(35) and beyond from _Lars Blomberg_, Jan 26 2020"
			],
			"references": 1,
			"revision": 23,
			"time": "2020-01-26T20:58:20-05:00",
			"created": "2020-01-23T17:24:37-05:00"
		}
	]
}