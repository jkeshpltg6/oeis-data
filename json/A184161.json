{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A184161",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 184161,
			"data": "1,3,6,6,10,10,11,11,15,15,15,17,17,17,21,20,17,25,20,24,24,21,25,30,28,25,36,28,24,34,21,37,28,24,32,44,30,30,34,41,25,40,28,32,48,36,34,55,37,45,32,40,37,64,36,49,41,34,24,59,44,28,57,70,44,44,30,37,48,53,41,81,40,44,63,49,41,56,32,74",
			"name": "Number of subtrees in the rooted tree with Matula-Goebel number n.",
			"comment": [
				"The Matula-Goebel number of a rooted tree can be defined in the following recursive manner: to the one-vertex tree there corresponds the number 1; to a tree T with root degree 1 there corresponds the t-th prime number, where t is the Matula-Goebel number of the tree obtained from T by deleting the edge emanating from the root; to a tree T with root degree m\u003e=2 there corresponds the product of the Matula-Goebel numbers of the m branches of T."
			],
			"link": [
				"É. Czabarka, L. Székely, and S. Wagner, \u003ca href=\"https://doi.org/10.1016/j.dam.2009.07.004\"\u003eThe inverse problem for certain tree parameters\u003c/a\u003e, Discrete Appl. Math., 157, 2009, 3314-3319.",
				"E. Deutsch, \u003ca href=\"https://arxiv.org/abs/1111.4288\"\u003eTree statistics from Matula numbers\u003c/a\u003e, arXiv preprint arXiv:1111.4288 [math.CO], 2011.",
				"F. Goebel, \u003ca href=\"https://doi.org/10.1016/0095-8956(80)90049-0\"\u003eOn a 1-1-correspondence between rooted trees and natural numbers\u003c/a\u003e, J. Combin. Theory, B 29 (1980), 141-143.",
				"I. Gutman and A. Ivic, \u003ca href=\"https://doi.org/10.1016/0012-365X(95)00182-V\"\u003eOn Matula numbers\u003c/a\u003e, Discrete Math., 150, 1996, 131-142.",
				"I. Gutman and Yeong-Nan Yeh, \u003ca href=\"http://www.emis.de/journals/PIMB/067/3.html\"\u003eDeducing properties of trees from their Matula numbers\u003c/a\u003e, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"D. W. Matula, \u003ca href=\"https://www.jstor.org/stable/2027327\"\u003eA natural rooted tree enumeration by prime factorization\u003c/a\u003e, SIAM Rev. 10 (1968) 273.",
				"\u003ca href=\"/index/Mat#matula\"\u003eIndex entries for sequences related to Matula-Goebel numbers\u003c/a\u003e"
			],
			"formula": [
				"Let b(n)=A184160(n) denote the number of those subtrees of the rooted tree with Matula-Goebel number n that contain the root. Then a(1)=1; if n=p(t) (=the t=th prime), then a(n)=1+a(t)+b(t); if n=rs (r,s,\u003e=2), then a(n)=a(r)+a(s)+(b(r)-1)(b(s)-1)-1. The Maple program is based on this recursive formula."
			],
			"example": [
				"a(4) = 6 because the rooted tree with Matula-Goebel number 4 is V; it has 6 subtrees (three 1-vertex subtrees, two 1-edge subtrees, and the tree itself)."
			],
			"maple": [
				"with(numtheory): a := proc (n) local r, s, b: r := proc (n) options operator, arrow: op(1, factorset(n)) end proc: s := proc (n) options operator, arrow: n/r(n) end proc: b := proc (n) if n = 1 then 1 elif bigomega(n) = 1 then 1+b(pi(n)) else b(r(n))*b(s(n)) end if end proc: if n = 1 then 1 elif bigomega(n) = 1 then a(pi(n))+b(pi(n))+1 else a(r(n))+a(s(n))+(b(r(n))-1)*(b(s(n))-1)-1 end if end proc: seq(a(n), n = 1 .. 80);"
			],
			"xref": [
				"Cf. A184160 (subtrees containing the root), A184164 (numbers not occurring as terms)."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Emeric Deutsch_, Oct 19 2011",
			"references": 4,
			"revision": 27,
			"time": "2021-05-17T07:34:08-04:00",
			"created": "2011-10-19T11:57:09-04:00"
		}
	]
}