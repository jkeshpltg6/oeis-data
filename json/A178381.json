{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A178381",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 178381,
			"data": "1,1,2,3,6,10,20,35,70,125,250,450,900,1625,3250,5875,11750,21250,42500,76875,153750,278125,556250,1006250,2012500,3640625,7281250,13171875,26343750,47656250,95312500,172421875,344843750",
			"name": "Number of paths of length n starting at initial node of the path graph P_9.",
			"comment": [
				"Counts all paths of length n, n\u003e=0, starting at initial node on the path graph P_9, see the Maple program.",
				"The a(n) represent the number of possible chess games, ignoring the fifty-move and the triple repetition rules, after n moves by White in the following position: White Ka1, Nh1, pawns a2, b6, c2, d6, f2, g3 and g4; Black Ka8, Bc8, pawns a3, b7, c3, d7, f3 and g5.",
				"The path graphs P_(2*p) have as limit(a(n+1)/a(n), n =infinity) = 2 resp. hypergeom([(p-1)/(2*p+1),(p+2)/(2*p+1)],[1/2],3/4) and the path graphs P_(2*p+1) have as limit(a(n+1)/a(n), n =infinity) = 1+cos(Pi/(p+1)), p\u003e=1; see the crossrefs. - _Johannes W. Meijer_, Jul 01 2010"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A178381/b178381.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Johann Cigler, \u003ca href=\"http://arxiv.org/abs/1501.04750\"\u003eSome remarks and conjectures related to lattice paths in strips along the x-axis\u003c/a\u003e, arXiv:1501.04750 [math.CO], 2015-2016.",
				"Nachum Dershowitz, \u003ca href=\"https://arxiv.org/abs/2006.06516\"\u003eBetween Broadway and the Hudson\u003c/a\u003e, arXiv:2006.06516 [math.CO], 2020.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/topics/TrigonometricIdentities.html\"\u003e Trigonometric Identities\u003c/a\u003e.",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,5,0,-5)."
			],
			"formula": [
				"G.f.: (1+x-3*x^2-2*x^3+x^4)/(1-5*x^2+5*x^4).",
				"a(n) = 5*a(n-2) - 5*a(n-4) for n\u003e=5 with a(0)=1, a(1)=1, a(2)=2, a(3)=3 and a(4)=6.",
				"G.f.: 1 / (1 - x / (1 - x / (1 + x / (1 + x / (1 - x / (1 - x / (1 + x / (1 + x)))))))). - _Michael Somos_, Feb 08 2015"
			],
			"example": [
				"G.f. = 1 + x + 2*x^2 + 3*x^3 + 6*x^4 + 10*x^5 + 20*x^6 + 35*x^7 + 70*x^8 + ..."
			],
			"maple": [
				"with(GraphTheory): P:=9: G:=PathGraph(P): A:= AdjacencyMatrix(G): nmax:=36; for n from 0 to nmax do B(n):=A^n; a(n):=add(B(n)[1,k],k=1..P); od: seq(a(n),n=0..nmax);",
				"r := j -\u003e (-1)^(j/10) - (-1)^(1-j/10):",
				"a := k -\u003e add((2 + r(j))*r(j)^k, j in [1, 3, 5, 7, 9])/10:",
				"seq(simplify(a(n)), n=0..30); # _Peter Luschny_, Sep 18 2020"
			],
			"mathematica": [
				"CoefficientList[Series[(1+x-3*x^2-2*x^3+x^4)/(1-5*x^2+5*x^4), {x,0,50}], x] (* _G. C. Greubel_, Sep 18 2018 *)"
			],
			"program": [
				"(PARI) x='x+O('x^50); Vec((1+x-3*x^2-2*x^3+x^4)/(1-5*x^2+5*x^4)) \\\\ _G. C. Greubel_, Sep 18 2018",
				"(MAGMA) m:=50; R\u003cx\u003e:=PowerSeriesRing(Integers(), m); Coefficients(R!((1+x-3*x^2-2*x^3+x^4)/(1-5*x^2+5*x^4))); // _G. C. Greubel_, Sep 18 2018"
			],
			"xref": [
				"This is row 9 of A094718.",
				"a(2*n) = A147748(n) and a(2*n+1) = A081567(n).",
				"a(4*n+2) = A109106(n) and a(4*n+3) = A179135(n).",
				"Cf. A000007 (P_1), A000012 (P_2), A016116 (P_3), A000045 (P_4), A038754 (P_5), A028495 (P_6), A030436 (P_7), A061551 (P_8), this sequence (P_9), A336675 (P_10), A336678 (P_11), and A001405 (P_infinity).",
				"Cf. A216212 (P_9 starting in the middle).",
				"Cf. A033191, A179131, A179132, A128052, A179133."
			],
			"keyword": "easy,nonn",
			"offset": "0,3",
			"author": "_Johannes W. Meijer_, May 27 2010, May 29 2010",
			"references": 18,
			"revision": 36,
			"time": "2020-10-18T04:32:58-04:00",
			"created": "2010-07-11T03:00:00-04:00"
		}
	]
}