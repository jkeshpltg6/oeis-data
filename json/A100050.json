{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A100050",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 100050,
			"data": "0,1,2,0,-4,-5,0,7,8,0,-10,-11,0,13,14,0,-16,-17,0,19,20,0,-22,-23,0,25,26,0,-28,-29,0,31,32,0,-34,-35,0,37,38,0,-40,-41,0,43,44,0,-46,-47,0,49,50,0,-52,-53,0,55,56,0,-58,-59,0,61,62,0,-64,-65,0,67,68,0,-70,-71,0,73,74,0,-76,-77,0,79,80,0,-82,-83,0,85,86,0",
			"name": "A Chebyshev transform of n.",
			"comment": [
				"A Chebyshev transform of x/(1-x)^2: if A(x) is the g.f. of a sequence, map it to ((1-x^2)/(1+x^2))A(x/(1+x^2))."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A100050/b100050.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"http://cis.csuohio.edu/~somos/rfmc.txt\"\u003eRational Function Multiplicative Coefficients\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,-3,2,-1)."
			],
			"formula": [
				"Euler transform of length 6 sequence [ 2, -3, -2, 0, 0, 2]. - _Michael Somos_, Mar 19 2011",
				"a(n) is multiplicative with a(2^e) = -(-2)^e if e\u003e0, a(3^e) = 0^e, a(p^e) = p^e if p == 1 (mod 6), a(p^e) = (-p)^e if p == 5 (mod 6). - _Michael Somos_, Mar 19 2011",
				"G.f.: x*(1 - x^2)^3 *(1 - x^3)^2 / ((1 - x)^2 *(1 - x^6)^2) = x *(1 + x)^2 *(1 - x^2) / (1 + x^3)^2. - _Michael Somos_, Mar 19 2011",
				"a(3*n) = 0, a(3*n + 1) = (-1)^n * (3*n + 1), a(3*n + 2) = (-1)^n * (3*n + 2). a(-n) = a(n). - _Michael Somos_, Mar 19 2011",
				"G.f.: x(1-x^2)/(1-x+x^2)^2.",
				"a(n) = 2*a(n-1) -3*a(n-2) +2*a(n-3) -a(n-4).",
				"a(n) = n*Sum_{k=0..floor(n/2)} (-1)^k*binomial(n-k,k)*(n-2k)/(n-k)."
			],
			"example": [
				"x + 2*x^2 - 4*x^4 - 5*x^5 + 7*x^7 + 8*x^8 - 10*x^10 - 11*x^11 + 13*x^13 + ..."
			],
			"mathematica": [
				"LinearRecurrence[{2, -3, 2, -1}, {0, 1, 2, 0},50] (* _G. C. Greubel_, Aug 08 2017 *)"
			],
			"program": [
				"(Sage) [lucas_number1(n,2,1)*lucas_number1(n,1,1) for n in range(0,88)] # _Zerinvary Lajos_, Jul 06 2008",
				"(PARI) {a(n) = n * (-1)^(n\\3) * sign( n%3)} /* _Michael Somos_, Mar 19 2011 */",
				"(PARI) {a(n) = local(A, p, e); if( abs(n)\u003c1, 0, A = factor(abs(n)); prod( k=1, matsize(A)[1], if( p=A[k,1], e=A[k,2]; if( p==2, -(-2)^e, (kronecker( -12, p) * p)^e))))} /* _Michael Somos_, Mar 19 2011 */"
			],
			"xref": [
				"Cf. A165202 (partial sums).",
				"Cf. A099837, A099443, A011655, A100047, A100048, A100051, A091684."
			],
			"keyword": "easy,sign,mult",
			"offset": "0,3",
			"author": "_Paul Barry_, Oct 31 2004",
			"references": 4,
			"revision": 26,
			"time": "2021-07-18T06:32:49-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}