{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A307531",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 307531,
			"data": "0,1,2,3,4,3,4,5,4,5,6,5,6,7,6,7,8,7,8,7,8,9,8,9,8,9,10,9,10,9,10,11,8,11,10,11,12,11,12,11,12,11,12,13,12,13,12,13,12,13,14,13,14,13,14,13,12,15,14,15,14,15,14,15,16,15,16,15,16,15,16,15",
			"name": "a(n) is the greatest sum i + j + k + l where i^2 + j^2 + k^2 + l^2 = n and 0 \u003c= i \u003c= j \u003c= k \u003c= l.",
			"comment": [
				"The sequence is well defined as every nonnegative integer can be represented as a sum of four squares in at least one way.",
				"It appears that a(n^2) = 2*n if n is even and 2*n-1 if n is odd. - _Robert Israel_, Apr 14 2019"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A307531/b307531.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A307531/a307531.txt\"\u003eC program for A307531\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Lagrange%27s_four-square_theorem\"\u003eLagrange's four-square theorem\u003c/a\u003e"
			],
			"example": [
				"For n = 34:",
				"- 34 can be expressed in 4 ways as a sum of four squares:",
				"    i^2 + j^2 + k^2 + l^2   i+j+k+l",
				"    ---------------------   -------",
				"    0^2 + 0^2 + 3^2 + 5^2         8",
				"    0^2 + 3^2 + 3^2 + 4^2        10",
				"    1^2 + 1^2 + 4^2 + 4^2        10",
				"    1^2 + 2^2 + 2^2 + 5^2        10",
				"- a(34) = max(8, 10) = 10."
			],
			"maple": [
				"g:= proc(n,k) option remember; local a;",
				"  if k = 1 then if issqr(n) then return sqrt(n) else return -infinity fi fi;",
				"  max(seq(a+procname(n-a^2,k-1),a=0..floor(sqrt(n))))",
				"end proc:",
				"seq(g(n,4), n=0..100); # _Robert Israel_, Apr 14 2019"
			],
			"mathematica": [
				"Array[Max[Total /@ PowersRepresentations[#, 4, 2]] \u0026, 68, 0] (* _Michael De Vlieger_, Apr 13 2019 *)"
			],
			"program": [
				"(C) See Links section."
			],
			"xref": [
				"See A307510 for the multiplicative variant.",
				"Cf. A002635."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Rémy Sigrist_, Apr 13 2019",
			"references": 2,
			"revision": 20,
			"time": "2019-04-15T13:02:16-04:00",
			"created": "2019-04-13T22:10:57-04:00"
		}
	]
}