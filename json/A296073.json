{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A296073",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 296073,
			"data": "1,2,2,3,2,4,2,5,6,7,2,8,2,9,10,11,2,12,2,13,14,15,2,16,17,18,19,20,2,21,2,22,23,24,25,26,2,27,28,29,2,30,2,31,32,33,2,34,35,36,37,38,2,39,40,41,42,43,2,44,2,45,46,47,48,49,2,50,51,52,2,53,2,54,55,56,57,58,2,59,60,61,2,62,63,64,65,66,2,67,68,69,70,71,72,73,2,74,75,76,2,77,2,78,79,80,2,81,2,82,83,84,2,85,86,87,88,89,90,91,92,93,94,95,33",
			"name": "Filter combining A296071(n) and A296072(n), related to the deficiencies of proper divisors of n.",
			"comment": [
				"Construction: Pack the values of A296071(n) and A296072(n) to a single value with any injective N x N -\u003e N packing function, like for example as f(n) = (1/2)*(2 + ((A296071(n)+A296072(n))^2) - A296071(n) - 3*A296072(n)) (the packing function here is the two-argument form of A000027). Then apply the restricted growth sequence transform to the sequence f(1), f(2), f(3), ... The transform assigns a unique increasing number for each newly encountered term of the sequence, and for any subsequent occurrences of the same term it gives the same number that term obtained for the first time.",
				"For all i, j: a(i) = a(j) =\u003e A296074(i) = A296074(j).",
				"Note that this is NOT restricted growth transform of A239968, which is A305800. Apart from 2's that occur at every prime, there are other duplicates also, first at a(125) = a(46) = 33."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A296073/b296073.txt\"\u003eTable of n, a(n) for n = 1..65536\u003c/a\u003e"
			],
			"example": [
				"To see that a(46) and a(125) have the same value (33), consider the proper divisors of 46 = 1, 2, 23 and of 125 = 1, 5, 25. Their deficiencies are 1, 1, 22 and 1, 4, 19 respectively. When we look at their balanced ternary representations [as here all elements are positive, it can be obtained as A007089(A117967(n)) with 2's standing for -1's]:",
				"   1 =    1",
				"   1 =    1",
				"  22 = 1211 (as 22 = 1*(3^3) + -1*(3^2) + 1*(3^1) + 1*(3^0))",
				"and",
				"   1 =    1",
				"   4 =   11",
				"  19 = 1201 (as 19 = 1*(3^3) + -1*(3^2) + 0*(3^1) + 1*(3^0)).",
				"we see that in each column there is an equal number of 1's and an equal number of 2's. Moreover, this then implies also that the sums of those two sequences of deficiencies {1, 1, 22} and {1, 4, 19} are equal, as A296074(n) is a function of (can be computed from) a(n)."
			],
			"program": [
				"(PARI)",
				"up_to = 65536;",
				"rgs_transform(invec) = { my(om = Map(), outvec = vector(length(invec)), u=1); for(i=1, length(invec), if(mapisdefined(om,invec[i]), my(pp = mapget(om, invec[i])); outvec[i] = outvec[pp] , mapput(om,invec[i],i); outvec[i] = u; u++ )); outvec; };",
				"write_to_bfile(start_offset,vec,bfilename) = { for(n=1, length(vec), write(bfilename, (n+start_offset)-1, \" \", vec[n])); }",
				"A019565(n) = {my(j,v); factorback(Mat(vector(if(n, #n=vecextract(binary(n), \"-1..1\")), j, [prime(j), n[j]])~))}; \\\\ This function from _M. F. Hasler_",
				"A117967(n) = if(n\u003c=1,n,if(!(n%3),3*A117967(n/3),if(1==(n%3),1+3*A117967((n-1)/3),2+3*A117967((n+1)/3))));",
				"A117968(n) = if(1==n,2,if(!(n%3),3*A117968(n/3),if(1==(n%3),2+3*A117968((n-1)/3),1+3*A117968((n+1)/3))));",
				"A289813(n) = { my (d=digits(n, 3)); from digits(vector(#d, i, if (d[i]==1, 1, 0)), 2); } \\\\ From _Rémy Sigrist_",
				"A289814(n) = { my (d=digits(n, 3)); from digits(vector(#d, i, if (d[i]==2, 1, 0)), 2); } \\\\ From _Rémy Sigrist_",
				"A295882(n) = { my(x = (2*n)-sigma(n)); if(x \u003e= 0,A117967(x),A117968(-x)); };",
				"A296071(n) = { my(m=1); fordiv(n,d,if(d \u003c n,m *= A019565(A289813(A295882(d))))); m; };",
				"A296072(n) = { my(m=1); fordiv(n,d,if(d \u003c n,m *= A019565(A289814(A295882(d))))); m; };",
				"Anotsubmitted3(n) = (1/2)*(2 + ((A296071(n)+A296072(n))^2) - A296071(n) - 3*A296072(n));",
				"write_to_bfile(1,rgs_transform(vector(up_to,n,Anotsubmitted3(n))),\"b296073.txt\");"
			],
			"xref": [
				"Cf. A019565, A033879, A117967, A117968, A295882, A296071, A296072, A296074.",
				"Cf. also A293226.",
				"Differs from A305800 for the first time at n=125."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Antti Karttunen_, Dec 04 2017",
			"ext": [
				"Data section extended up to a(125) by _Antti Karttunen_, Jun 14 2018"
			],
			"references": 7,
			"revision": 27,
			"time": "2020-01-20T21:43:23-05:00",
			"created": "2017-12-04T18:37:01-05:00"
		}
	]
}