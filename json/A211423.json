{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A211423",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 211423,
			"data": "1,5,17,21,33,37,49,53,73,85,97,101,121,125,137,141,161,165,193,197,209,213,225,229,257,277,289,301,313,317,337,341,377,381,393,397,433,437,449,453,481,485,497,501,513,525,537,541,569,597,641,645,657",
			"name": "Number of ordered triples (w,x,y) with all terms in {-n,...,0,...,n} and w^2 + 2*x*y = 0.",
			"comment": [
				"For a guide to related sequences, see A211422.",
				"From _David A. Corneth_, May 21 2020: (Start)",
				"Let (w, x, y) be a primitive solution to the 8 solutions {(w, x, y), (-w, x, y), (w, -x, -y), (-w, -x, -y), (w, y, x), (-w, y, x), (w, -y, -x), (-w, -y, -x)}. Then for any n \u003e 1 we have the primitive solution (0, 0, n) giving 4 solutions where abs(max(w, x, y)) = n. For even n we also have (n, 2, (n/2)^2) as a primitive solution. So a(n) - a(n-1) \u003e= 4 for n odd and a(n) - a(n-1) \u003e= 12 for n even and a(n) \u003e= 8*n - 3. (End)"
			],
			"link": [
				"Brandon Crofts, \u003ca href=\"/A211423/b211423.txt\"\u003eTable of n, a(n) for n = 0..20000\u003c/a\u003e",
				"Brandon Crofts, \u003ca href=\"/A211423/a211423_3.txt\"\u003eFaster Mathematica code for A211423\u003c/a\u003e"
			],
			"mathematica": [
				"t[n_] := t[n] = Flatten[Table[w^2 + 2 x*y, {w, -n, n}, {x, -n, n}, {y, -n, n}]]",
				"c[n_] := Count[t[n], 0]",
				"t = Table[c[n], {n, 0, 60}] (* A211423 *)",
				"(t - 1)/4                   (* integers *)"
			],
			"program": [
				"(PARI) first(n) = {n--; my(v = vector(n, i, 4)); forstep(w = 2, n, 2, for(x = 1, n, y = w^2/(2*x); if(denominator(y) == 1 \u0026\u0026 abs(y) \u003c= n, v[vecmax([x, y, w])]+=4 ) ) ); res = vector(n + 1); res[1] = 1; for(i = 2, n+1, res[i] = res[i-1] + v[i-1]); res } \\\\ _David A. Corneth_, May 21 2020",
				"(PARI) first(n) = { my(res = vector(n), d); res[1] = 1; for(i = 1, n-1, t = i\\(sqrtint(2*i*core(2*i)))*2+1; if(!bitand(i, 1), d = divisors(i^2/2); t += 2*(vecsearch(d, i) - #d\\2 - 1)); res[i+1] = res[i] + 4*t; ); res } \\\\ faster than PARI above \\\\ _David A. Corneth_, May 22 2020"
			],
			"xref": [
				"Cf. A211422."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Clark Kimberling_, Apr 10 2012",
			"references": 5,
			"revision": 55,
			"time": "2020-06-01T06:11:35-04:00",
			"created": "2012-04-15T14:16:59-04:00"
		}
	]
}