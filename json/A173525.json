{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A173525",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 173525,
			"data": "1,2,3,4,5,2,3,4,5,6,3,4,5,6,7,4,5,6,7,8,5,6,7,8,9,2,3,4,5,6,3,4,5,6,7,4,5,6,7,8,5,6,7,8,9,6,7,8,9,10,3,4,5,6,7,4,5,6,7,8,5,6,7,8,9,6,7,8,9,10,7,8,9,10,11,4,5,6,7,8,5,6,7,8,9,6,7,8,9,10,7,8,9,10,11,8,9,10,11,12",
			"name": "a(n) = 1 + A053824(n-1), where A053824 = sum of digits in base 5.",
			"comment": [
				"Also: a(n) = A053824(5^k+n-1) in the limit k-\u003einfinity, where k plays the role of a row index in A053824. (See the comment by _M. F. Hasler_ for the proof.)",
				"This means: if A053824 is regarded as a triangle then the rows converge to this sequence.",
				"See conjecture in the entry A000120, and the case of base 2 in A063787.",
				"From _R. J. Mathar_, Dec 09 2010: (Start)",
				"In base b=5, A053824 starts counting up from 1 each time the index wraps around a power of b: A053824(b^k)=1.",
				"Obvious recurrences are A053824(m*b^k+i) = m+A053824(i), 1 \u003c= m \u003c b-1, 0 \u003c= i \u003c b^(k-1).",
				"So A053824 can be decomposed into a triangle T(k,n) = A053824(b^k+n-1), assuming that column indices start at n=1; row lengths are (b-1)*b^k.",
				"There is a self-similarity in these sequences; a sawtooth structure of periodicity b is added algebraically on top of a sawtooth structure of periodicity b^2, on top of a periodicity b^3 etc. This leads to some \"fake\" finitely periodic substructures in the early parts of each row of T(.,.): often, but not always, a(n+b)=1+a(n). Often, but not always, a(n+b^2)=1+a(n) etc.",
				"The common part of the rows T(.,.) grows with the power of b as shown in the recurrence above, and defines a(n) in the limit of large row indices k. (End)",
				"The two definitions agree because the first 5^r terms in each row correspond to numbers 5^r, 5^r+1,...,5^r+(5^r-1), which are written in base 5 as a leading 1 plus the digits of 0,...,5^r-1. - _M. F. Hasler_, Dec 09 2010",
				"From _Omar E. Pol_, Dec 10 2010: (Start)",
				"In the scatter plots of these sequences, the basic structure is an element with b^2 points, where b is the associated base. (Scatter plots are created with the \"graph\" button of a sequence.) Sketches of these structures look as follows, the horizontal axis a squeezed version of the index n, b consecutive points packed vertically, and the vertical axis a(n):",
				"........................................................",
				"................................................ * .....",
				"............................................... ** .....",
				"..................................... * ...... *** .....",
				".................................... ** ..... **** .....",
				".......................... * ...... *** .... ***** .....",
				"......................... ** ..... **** ... ****** .....",
				"............... * ...... *** .... ***** ... ***** ......",
				".............. ** ..... **** .... **** .... **** .......",
				".... * ...... *** ..... *** ..... *** ..... *** ........",
				"... ** ...... ** ...... ** ...... ** ...... ** .........",
				"... * ....... * ....... * ....... * ....... * ..........",
				"........................................................",
				"... b=2 ..... b=3 ..... b=4 ..... b=5 ..... b=6 ........",
				". A000120 . A053735 . A053737 . A053824 . A053827 ......",
				". A063787 . A173523 . A173524 . A173525 . A173526 ......",
				"........................................................",
				"............................................. * ........",
				"............................................ ** ........",
				"........................... * ............. *** ........",
				".......................... ** ............ **** ........",
				"........... *............ *** ........... ***** ........",
				".......... ** .......... **** .......... ****** ........",
				"......... ***.......... ***** ......... ******* ........",
				"........ **** ........ ****** ........ ******** ........",
				"....... ***** ....... ******* ....... ********* ........",
				"...... ****** ...... ******** ....... ******** .........",
				"..... ******* ...... ******* ........ ******* ..........",
				"..... ****** ....... ****** ......... ****** ...........",
				"..... ***** ........ ***** .......... ***** ............",
				"..... **** ......... **** ........... **** .............",
				"..... *** .......... *** ............ *** ..............",
				"..... ** ........... ** ............. ** ...............",
				"..... * ............ * .............. * ................",
				"........................................................",
				"..... b=7 .......... b=8 ............ b=9 ..............",
				"... A053828 ...... A053829 ........ A053830 ............",
				"... A173527 ...... A173528 ........ A173529 ............(End)"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A173525/b173525.txt\"\u003eTable of n, a(n) for n = 1..3126=5^5+1\u003c/a\u003e",
				"Robert Walker, \u003ca href=\"http://robertinventor.com/ftswiki/Self_Similar_Sloth_Canon_Number_Sequences\"\u003eSelf Similar Sloth Canon Number Sequences\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A053824(5^k + n - 1) where k \u003e= ceiling(log_5(n/4)). - _R. J. Mathar_, Dec 09 2010"
			],
			"maple": [
				"A053825 := proc(n) add(d, d=convert(n,base,5)) ; end proc:",
				"A173525 := proc(n) local b,k; b := 5 ; if n \u003c b then n; else k := n/(b-1);   k := ceil(log(k)/log(b)) ; A053825(b^k+n-1) ; end if; end proc:",
				"seq(A173525(n),n=1..100) ;"
			],
			"mathematica": [
				"Total[IntegerDigits[#,5]]+1\u0026/@Range[0,100] (* _Harvey P. Dale_, Jun 14 2015 *)"
			],
			"program": [
				"(PARI) A173525(n)={ my(s=1); n--; until(!n\\=5, s+=n%5); s } \\\\ _M. F. Hasler_, Dec 09 2010",
				"(PARI) A173525(n)={ my(s=1+(n=divrem(n-1,5))[2]); while((n=divrem(n[1],5))[1],s+=n[2]); s+n[2] } \\\\ _M. F. Hasler_, Dec 09 2010",
				"(Haskell)",
				"a173525 = (+ 1) . a053824 . (subtract 1) -- _Reinhard Zumkeller_, Jan 31 2014"
			],
			"xref": [
				"Cf. A000120, A053824, A063787, A173523, A173524, A173526, A173527, A173528, A173529."
			],
			"keyword": "nonn,base,look",
			"offset": "1,2",
			"author": "_Omar E. Pol_, Feb 20 2010",
			"ext": [
				"More terms from _Vincenzo Librandi_, Aug 02 2010"
			],
			"references": 8,
			"revision": 39,
			"time": "2021-01-26T10:18:01-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}