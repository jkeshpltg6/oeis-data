{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001715",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1715,
			"id": "M3566 N1445",
			"data": "1,4,20,120,840,6720,60480,604800,6652800,79833600,1037836800,14529715200,217945728000,3487131648000,59281238016000,1067062284288000,20274183401472000,405483668029440000,8515157028618240000,187333454629601280000,4308669456480829440000",
			"name": "a(n) = n!/6.",
			"comment": [
				"The numbers (4, 20, 120, 840, 6720, ...) arise from the divisor values in the general formula a(n) = n*(n+1)*(n+2)*(n+3)* ... *(n+k)*(n*(n+k) + (k-1)*k/6)/((k+3)!/6) (which covers the following sequences: A000578, A000537, A024166, A101094, A101097, A101102). - _Alexander R. Povolotsky_, May 17 2008",
				"a(n) is also the number of decreasing 3-cycles in the decomposition of permutations as product of disjoint cycles, a(3)=1, a(4)=4, a(5)=20. - _Wenjin Woan_, Dec 21 2008",
				"Equals eigensequence of triangle A130128 reflected. - _Gary W. Adamson_, Dec 23 2008",
				"a(n) is the number of n-permutations having 1, 2, and 3 in three distinct cycles. - _Geoffrey Critzer_, Apr 26 2009",
				"From _Johannes W. Meijer_, Oct 20 2009: (Start)",
				"The asymptotic expansion of the higher order exponential integral E(x,m=1,n=4) ~ exp(-x)/x*(1 - 4/x + 20/x^2 - 120/x^3 + 840/x^4 - 6720/x^5 + 60480/x^6 - 604800/x^7 + ...) leads to the sequence given above. See A163931 and A130534 for more information.",
				"(End)"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A001715/b001715.txt\"\u003eTable of n, a(n) for n = 3..200\u003c/a\u003e",
				"Somaya Barati, Beáta Bényi, Abbas Jafarzadeh, and Daniel Yaqubi, \u003ca href=\"https://arxiv.org/abs/1812.02955\"\u003eMixed restricted Stirling numbers\u003c/a\u003e, arXiv:1812.02955 [math.CO], 2018.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=263\"\u003eEncyclopedia of Combinatorial Structures 263\u003c/a\u003e",
				"Wolfdieter Lang, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL3/LANG/lang.html\"\u003eOn generalizations of Stirling number triangles\u003c/a\u003e, J. Integer Seqs., Vol. 3 (2000), #00.2.4.",
				"D. S. Mitrinovic and R. S. Mitrinovic, \u003ca href=\"http://pefmath2.etf.rs/files/47/77.pdf\"\u003eTableaux d'une classe de nombres reliés aux nombres de Stirling\u003c/a\u003e, Univ. Beograd. Publ. Elektrotehn. Fak. Ser. Mat. Fiz. No. 77 1962, 77 pp.",
				"Alexsandar Petojevic, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL5/Petojevic/petojevic5.html\"\u003eThe Function vM_m(s; a; z) and Some Well-Known Sequences\u003c/a\u003e, Journal of Integer Sequences, Vol. 5 (2002), Article 02.1.7.",
				"A. N. Stokes, \u003ca href=\"https://doi.org/10.1017/S0004972700005219\"\u003eContinued fraction solutions of the Riccati equation\u003c/a\u003e, Bull. Austral. Math. Soc. Vol. 25 (1982), 207-214.",
				"\u003ca href=\"/index/Di#divseq\"\u003eIndex to divisibility sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Fa#factorial\"\u003eIndex entries for sequences related to factorial numbers\u003c/a\u003e"
			],
			"formula": [
				"E.g.f. if offset 0: 1/(1-x)^4.",
				"a(n) = A173333(n,3). - _Reinhard Zumkeller_, Feb 19 2010",
				"G.f.: G(0)/2, where G(k) = 1 + 1/(1 - x/(x + 1/(k+4)/G(k+1))); (continued fraction). - _Sergei N. Gladkovskii_, Jun 01 2013",
				"G.f.: W(0), where W(k) = 1 - x*(k+4)/( x*(k+4) - 1/(1 - x*(k+1)/( x*(k+1) - 1/W(k+1) ))); (continued fraction). - _Sergei N. Gladkovskii_, Aug 26 2013",
				"a(n) = A245334(n,n-3) / 4. - _Reinhard Zumkeller_, Aug 31 2014",
				"From _Peter Bala_, May 22 2017: (Start)",
				"The o.g.f. A(x) satisfies the Riccati equation x^2*A'(x) + (4*x - 1)*A(x) + 1 = 0.",
				"G.f. as an S-fraction: A(x) = 1/(1 - 4*x/(1 - x/(1 - 5*x/(1 - 2*x/(1 - 6*x/(1 - 3*x/(1 - ... - (n + 3)*x/(1 - n*x/(1 - ... ))))))))) (apply Stokes, 1982).",
				"A(x) = 1/(1 - 3*x - x/(1 - 4*x/(1 - 2*x/(1 - 5*x/(1 - 3*x/(1 - 6*x/(1 - ... - n*x/(1 - (n+3)*x/(1 - ... ))))))))). (End)",
				"H(x) = (1 - (1 + x)^(-3)) / 3 = x - 4 x^2/2! + 20 x^3/3! - ... is an e.g.f. of the signed sequence (n!/4!), which is the compositional inverse of G(x) = (1 - 3*x)^(-1/3) - 1, an e.g.f. for A007559. Cf. A094638, A001710 (for n!/2!), and A001720 (for n!/4!). Cf. columns of A094587, A173333, and A213936 and rows of A138533.- _Tom Copeland_, Dec 27 2019",
				"E.g.f.: x^3 / (3! * (1 - x)). - _Ilya Gutkovskiy_, Jul 09 2021"
			],
			"maple": [
				"f := proc(n) n!/6; end;",
				"BB:= [S, {S = Prod(Z,Z,C), C = Union(B,Z,Z), B = Prod(Z,C)}, labelled]: seq(combstruct[count](BB, size=n)/12, n=3..20); # _Zerinvary Lajos_, Jun 19 2008",
				"G(x):=1/(1-x)^4: f[0]:=G(x): for n from 1 to 18 do f[n]:=diff(f[n-1],x) od: x:=0: seq(f[n],n=0..16); # _Zerinvary Lajos_, Apr 01 2009"
			],
			"mathematica": [
				"a[n_]:=n!/6; (*_Vladimir Joseph Stephan Orlovsky_, Dec 13 2008 *)",
				"Range[3,30]!/6 (* _Harvey P. Dale_, Aug 12 2012 *)"
			],
			"program": [
				"(MAGMA) [Factorial(n)/6: n in [3..30]]; // _Vincenzo Librandi_, Jun 20 2011",
				"(PARI) a(n)=n!/6 \\\\ _Charles R Greathouse IV_, Jan 12 2012",
				"(Haskell)",
				"a001715 = (flip div 6) . a000142 -- _Reinhard Zumkeller_, Aug 31 2014"
			],
			"xref": [
				"a(n) = A049352(n-2, 1) (first column of triangle). Cf. A049458, A049460.",
				"Cf. A034472, A130128.",
				"Cf. A245334, A000142, A111530.",
				"Cf. A001710, A001720, A007759, A094638.",
				"Cf. A094587, A138533, A173333, A213936."
			],
			"keyword": "nonn,easy",
			"offset": "3,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Harvey P. Dale_, Aug 12 2012"
			],
			"references": 47,
			"revision": 83,
			"time": "2021-07-10T03:03:09-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}