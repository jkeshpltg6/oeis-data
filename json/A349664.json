{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A349664",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 349664,
			"data": "0,1,2,3,2,7,2,5,4,7,2,17,2,7,12,7,2,13,2,17,12,7,2,27,4,7,6,17,2,37,2,9,12,7,12,31,2,7,12,27,2,37,2,17,22,7,2,37,4,13,12,17,2,19,12,27,12,7,2,87,2,7,22,11,12,37,2,17,12,37,2,49,2,7,22",
			"name": "a(n) is the number of solutions for n^4 = z^2 - x^2 with {z,x} \u003e= 1.",
			"comment": [
				"If n is an odd prime^i, the number of solutions is 2*i.",
				"If n = 2^i, the number of solutions is 2*i-1.",
				"These two facts are not generally valid in reverse for terms \u003e 6.",
				"If a(n) = 2, n is an odd prime. This is generally valid in reverse.",
				"For more information about these facts see the link.",
				"The calculation of the terms is done with an algorithm of _Jon E. Schoenfield_, which is described in A349324.",
				"Conditions to be satisfied for a valid, countable solution:",
				"- z cannot be a square.",
				"- z must have at least one prime factor of the form p == 1 (mod 4), a Pythagorean prime (A002144).",
				"- If z has prime factors of the form p == 3 (mod 4), which are in A002145, then they must appear in the prime divisor sets of x and n too.",
				"- If z is even, x and n must be even too.",
				"- The lower limit of the ratio x/n is sqrt(2).",
				"- high limits of z and x:",
				"           |    n is odd      |    n is even",
				"  ---------+------------------+------------------",
				"  z limit  | (n^4 + 1)/2      |  (n^4 + 4)/4",
				"  x limit  | (n^4 + 1)/2 - 1  |  (n^4 + 4)/4 - 2"
			],
			"link": [
				"Karl-Heinz Hofmann, \u003ca href=\"/A349664/b349664.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Karl-Heinz Hofmann, \u003ca href=\"/A349664/a349664_1.pdf\"\u003eWhat the terms can tell about n\u003c/a\u003e."
			],
			"example": [
				"a(6) = 7 (solutions): 6^4 = 1296 = 325^2 - 323^2 = 164^2 - 160^2 = 111^2 - 105^2 = 85^2 - 77^2 = 60^2 - 48^2 = 45^2 - 27^2 = 39^2 - 15^2."
			],
			"mathematica": [
				"a[n_] := Length[Solve[n^4 == z^2 - x^2 \u0026\u0026 x \u003e= 1 \u0026\u0026 z \u003e= 1, {x, z}, Integers]]; Array[a, 75] (* _Amiram Eldar_, Dec 14 2021 *)"
			],
			"program": [
				"(PARI) a(n) = numdiv(if(n%2, n^4, n^4/4))\\2; \\\\ _Jinyuan Wang_, Dec 19 2021"
			],
			"xref": [
				"Cf. A000290, A000583, A271576, A349663, A002144, A002145, A346115.",
				"Cf. A345645, A345700, A345968, A346110, A348655, A349324."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Karl-Heinz Hofmann_, Dec 13 2021",
			"references": 2,
			"revision": 33,
			"time": "2021-12-20T02:44:23-05:00",
			"created": "2021-12-19T04:24:36-05:00"
		}
	]
}