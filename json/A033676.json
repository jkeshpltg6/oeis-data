{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A033676",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 33676,
			"data": "1,1,1,2,1,2,1,2,3,2,1,3,1,2,3,4,1,3,1,4,3,2,1,4,5,2,3,4,1,5,1,4,3,2,5,6,1,2,3,5,1,6,1,4,5,2,1,6,7,5,3,4,1,6,5,7,3,2,1,6,1,2,7,8,5,6,1,4,3,7,1,8,1,2,5,4,7,6,1,8,9,2,1,7,5,2,3",
			"name": "Largest divisor of n \u003c= sqrt(n).",
			"comment": [
				"a(n) = sqrt(n) is a new record if and only if n is a square. - _Zak Seidov_, Jul 17 2009",
				"a(n) = A060775(n) unless n is a square, when a(n) = A033677(n) = sqrt(n) is strictly larger than A060775(n). It would be nice to have an efficient algorithm to calculate these terms when n has a large number of divisors, as for example in A060776, A060777 and related problems such as A182987. - _M. F. Hasler_, Sep 20 2011",
				"a(n) = 1 when n = 1 or n is prime. - _Alonso del Arte_, Nov 25 2012",
				"a(n) is the smallest central divisor of n. Column 1 of A207375. - _Omar E. Pol_, Feb 26 2019"
			],
			"reference": [
				"G. Tenenbaum, pp. 268 ff, in: R. L. Graham et al., eds., Mathematics of Paul Erdős I."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A033676/b033676.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"A033677(n) * a(n) = n.",
				"a(n) = A161906(n,A038548(n)). - _Reinhard Zumkeller_, Mar 08 2013",
				"a(n) = A162348(2n-1). - _Daniel Forgues_, Sep 29 2014"
			],
			"maple": [
				"A033676 := proc(n) local a,d; a := 0 ; for d in numtheory[divisors](n) do if d^2 \u003c= n then a := max(a,d) ; end if; end do: a; end proc: # _R. J. Mathar_, Aug 09 2009"
			],
			"mathematica": [
				"largestDivisorLEQR[n_Integer] := Module[{dvs = Divisors[n]}, dvs[[Ceiling[Length@dvs/2]]]]; largestDivisorLEQR /@ Range[100] (* _Borislav Stanimirov_, Mar 28 2010 *)",
				"Table[Last[Select[Divisors[n],#\u003c=Sqrt[n]\u0026]],{n,100}] (* _Harvey P. Dale_, Mar 17 2017 *)"
			],
			"program": [
				"(PARI) A033676(n) = {local(d);if(n\u003c2,1,d=divisors(n);d[(length(d)+1)\\2])} \\\\ _Michael B. Porter_, Jan 30 2010",
				"(Haskell)",
				"a033676 n = last $ takeWhile (\u003c= a000196 n) $ a027750_row n",
				"-- _Reinhard Zumkeller_, Jun 04 2012",
				"(Python)",
				"from sympy import divisors",
				"def A033676(n):",
				"    d = divisors(n)",
				"    return d[(len(d)-1)//2]  # _Chai Wah Wu_, Apr 05 2021"
			],
			"xref": [
				"Cf. A033677, A000196, A027750, A056737, A219695, A282668 (positions of primes)",
				"From _Omar E. Pol_, Jul 05 2009: (Start)",
				"Sequence of corresponding indices:",
				".. 1 ..... A008578 (1 together with the prime numbers)",
				".. 2 ..... A161344",
				".. 3 ..... A161345",
				".. 4 ..... A161424",
				".. 5 ..... A161835",
				".. 6 ..... A162526",
				".. 7 ..... A162527",
				".. 8 ..... A162528",
				".. 9 ..... A162529",
				". 10 ..... A162530",
				". 11 ..... A162531",
				". 12 ..... A162532 (End)",
				"Cf. A207375."
			],
			"keyword": "nonn,easy,nice",
			"offset": "1,4",
			"author": "_N. J. A. Sloane_",
			"references": 122,
			"revision": 68,
			"time": "2021-04-05T13:43:29-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}