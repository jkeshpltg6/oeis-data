{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A187096",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 187096,
			"data": "1,0,-2,-2,3,0,-1,0,1,0,3,4,-4,0,-6,4,-3,0,1,-6,2,0,0,0,4,0,4,2,6,0,-4,0,-6,0,-3,-2,2,0,8,0,-6,0,-1,-6,3,0,-3,-8,-6,0,6,8,12,0,9,0,-2,0,-6,12,-1,0,-1,-8,-12,0,-4,6,0,0,6,0,-7,0,-8,-2,-3,0,8,12,-11,0,12,-4,-9,0,-12,0,12,0,4,0,8,0,3,0,8,0,3,-8,6",
			"name": "Coefficients of L-series for elliptic curve \"19a3\": y^2 + y = x^3 + x^2 + x.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A187096/b187096.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q * (psi(q^4) * phi(q^38) - q^2 * psi(q) * psi(q^19) + q^9 * phi(q^2) * psi(q^76))^2 in powers of q where phi(), psi() are Ramanujan theta functions.",
				"a(n) is multiplicative with a(19^e) = 1, a(p^e) = a(p) * a(p^(e-1)) - p * a(p^(e-2)) where a(p) = p+1 minus number of points of elliptic curve modulo p including point at infinity.",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (19 t)) = 19 (t/i)^2 f(t) where q = exp(2 Pi i t).",
				"Convolution square of A187097."
			],
			"example": [
				"G.f. = q - 2*q^3 - 2*q^4 + 3*q^5 - q^7 + q^9 + 3*q^11 + 4*q^12 - 4*q^13 + ...",
				"If p = 2, then the solutions to y^2 + y = x^3 + x^2 + x modulo 2 are (0,0), (0,1) and the point at infinity. Thus a(2) = 2+1-3 = 0."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (-EllipticTheta[ 2, 0, q] EllipticTheta[ 2, 0, q^19] + 2 (EllipticTheta[ 3, 0, q^76] EllipticTheta[ 2, 0, q^4] + EllipticTheta[ 3, 0, q^4] EllipticTheta[ 2, 0, q^76]))^2 / 16, {q, 0, 2 n}]; (* _Michael Somos_, Oct 17 2016 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, 0, ellak( ellinit([ 0, 1, 1, 1, 0], 1), n))};",
				"(PARI) {a(n) = my(phi1, psi1); if( n\u003c1, 0, n--; phi1 = 1 + 2 * sum( k=1, sqrtint( n), x^k^2, x * O(x^n)); psi1 = sum( k=1, ( sqrtint( 8*n + 1) + 1 ) \\ 2, x^((k^2 - k)/2), x * O(x^n)); polcoeff( sqr( subst( psi1 + x * O(x^(n \\ 4)), x, x^4) * subst( phi1 + x * O(x^(n \\ 38)), x, x^38) - x^2 * psi1 * subst( psi1 + x * O(x^(n \\ 19)), x, x^19) + x^9 * subst( phi1 + x * O(x^(n \\ 2)), x, x^2) * subst( psi1 + x * O(x^(n \\ 76)), x, x^76)), n))};",
				"(Sage) CuspForms( Gamma0(19), 2, prec=100).0; # _Michael Somos_, May 28 2013",
				"(MAGMA) Basis( CuspForms( Gamma0(19), 2), 100) [1]; /* _Michael Somos_, May 27 2014 */"
			],
			"xref": [
				"Cf. A187097."
			],
			"keyword": "sign,mult",
			"offset": "1,3",
			"author": "_Michael Somos_, Mar 04 2011",
			"references": 2,
			"revision": 24,
			"time": "2021-03-12T22:24:46-05:00",
			"created": "2011-03-04T12:12:29-05:00"
		}
	]
}