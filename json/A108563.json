{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A108563",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 108563,
			"data": "1,0,2,2,0,4,0,0,2,0,0,4,2,0,4,0,0,0,2,0,4,4,0,0,0,0,0,2,0,4,4,0,2,0,0,8,0,0,0,0,0,0,0,0,4,4,0,0,2,0,6,0,0,4,0,0,4,0,0,4,0,0,4,0,0,0,4,0,0,0,0,0,2,0,0,6,0,8,0,0,4,0,0,4,4,0,0,0,0,0,0,0,0,4,0,0,0,0,6,4,0,4",
			"name": "Number of representations of n as sum of twice a square plus thrice a square.",
			"comment": [
				"Number of solutions to n = 2*a^2 + 3*b^2 in integers.",
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"a(n) \u003e 0 if and only if n is in A002480. a(n) \u003c 2 if and only if n is in A002481. - _Michael Somos_, Mar 01 2011"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A108563/b108563.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"A. Berkovich and H. Yesilyurt, \u003ca href=\"https://arxiv.org/abs/math/0611300\"\u003eRamanujan's identities and representation of integers by certain binary and quaternary quadratic forms\u003c/a\u003e, arXiv:math/0611300 [math.NT], 2006-2007.",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"G.f.: 1 + Sum_{k\u003e0} x^k * (1 + x^(4*k)) * (1 + x^(6*k)) / (1 + x^(12*k)) - Sum_{k\u003e0} Kronecker( k, 3) * x^k * (1 - x^(2*k)) / (1 + x^(4*k)).",
				"G.f.: Sum_{i, j in Z} x^(2*i^2 + 3*j^2). - _Michael Somos_, Mar 01 2011",
				"Expansion of phi(q^2) * phi(q^3) in powers of q where phi() is a Ramanujan theta function. - _Michael Somos_, Mar 01 2011",
				"A115660(n) = A000377(n) - a(n). - _Michael Somos_, Mar 01 2011",
				"Euler transform of period 24 sequence [0, 2, 2, -3, 0, -1, 0, -1, 2, 2, 0, -4, 0, 2, 2, -1, 0, -1, 0, -3, 2, 2, 0, -2, ...]. - _Michael Somos_, Jan 20 2017",
				"Expansion of eta(q^4)^5 * eta(q^6)^5 / (eta(q^2)^2 * eta(q^3)^2 * eta(q^8)^2 * eta(q^12)^2) in powers of q. - _Michael Somos_, Jan 20 2017"
			],
			"example": [
				"G.f. = 1 + 2*x^2 + 2*x^3 + 4*x^5 + 2*x^8 + 4*x^11 + 2*x^12 + 4*x^14 + 2*x^18 + ...",
				"a(0) = 1 since 0 = 2*0^2 + 3*0^2, a(5) = 4 since 5 = 2*1^2 + 3*1^2 = 2*(-1)^2 + 3*1^2 = 2*1^2 + 3*(-1)^2 = 2*(-1^2) + 3*(-1)^2."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 3, 0, q^2] EllipticTheta[ 3, 0, q^3], {q, 0, n}]; (* _Michael Somos_, Apr 19 2015 *)",
				"a[n_] := Module[{a, b, r}, r = Reduce[n == 2a^2 + 3b^2, {a, b}, Integers]; Which[r === False, 0, r[[0]] === And, 1, r[[0]] === Or, Length[r]]];",
				"Table[a[n], {n, 0, 105}] (* _Jean-François Alcover_, Jan 09 2019 *)"
			],
			"program": [
				"(PARI) for(n=0,120,print1(if(n\u003c1,n==0,qfrep([2,0;0,3],n)[n]*2),\",\"))",
				"(PARI) {a(n) = my(G); if( n\u003c0, 0, G = [2, 0; 0, 3]; polcoeff( 1 + 2 * x * Ser( qfrep( G, n)), n))}; /* _Michael Somos_, Mar 01 2011 */",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^4 + A)^5 * eta(x^6 + A)^5 / (eta(x^2 + A)^2 * eta(x^3 + A)^2 * eta(x^8 + A)^2 * eta(x^12 + A)^2), n))}; /* _Michael Somos_, Jan 20 2017 */",
				"(Sage)",
				"Q = DiagonalQuadraticForm(ZZ,[3, 2])",
				"Q.representation_number_list(102) # _Peter Luschny_, Jun 20 2014"
			],
			"xref": [
				"Cf. A000377, A002480, A002481, A115660."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Ralf Stephan_, May 13 2007",
			"ext": [
				"Edited by _Charles R Greathouse IV_, Oct 28 2009",
				"Edited by _N. J. A. Sloane_, Mar 04 2011"
			],
			"references": 4,
			"revision": 35,
			"time": "2021-03-12T22:24:43-05:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}