{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A227216",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 227216,
			"data": "1,3,4,2,1,3,6,4,0,-1,4,6,4,2,2,2,3,4,2,0,1,6,8,2,0,3,6,0,-2,0,6,6,4,4,2,4,3,4,0,-2,0,6,8,2,2,-1,6,4,2,1,4,6,4,2,0,6,0,0,0,0,4,6,8,2,1,2,12,4,-2,-2,2,6,0,2,2,2,0,8,4,0,3,3,8,2",
			"name": "Expansion of f(-q^2, -q^3)^5 / f(-q)^3 in powers of q where f() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Zagier (2009) refers to Case D corresponding to the Apery numbers (A005258)."
			],
			"reference": [
				"D. Zagier, Integral solutions of Apery-like recurrence equations, in: Groups and Symmetries: from Neolithic Scots to John McKay, CRM Proc. Lecture Notes 47, Amer. Math. Soc., Providence, RI, 2009, pp. 349-366."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A227216/b227216.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"D. Zagier, \u003ca href=\"http://people.mpim-bonn.mpg.de/zagier/files/tex/AperylikeRecEqs/fulltext.pdf\"\u003eIntegral solutions of Apery-like recurrence equations\u003c/a\u003e."
			],
			"formula": [
				"Expansion of f(-q)^2 * (f(-q^5) / f(-q, -q^4))^5 = f(-q^2, -q^3)^2 * (f(-q^5) / f(-q, -q^4))^3 in powers of q where f() is a Ramanujan theta function.",
				"Euler transform of period 5 sequence [ 3, -2, -2, 3, -2, ...].",
				"Moebius transform is period 5 sequence [ 3, 1, -1, -3, 0, ...]. - _Michael Somos_, Jun 10 2014",
				"G.f. = g(t(q)) where g(), t() are the g.f. for A005258 and A078905.",
				"G.f.: (Product_{k\u003e0} (1 - x^k)^2) / (Product_{k\u003e0} (1 - x^(5*k - 1)) * (1 - x^(5*k - 4)))^5."
			],
			"example": [
				"G.f. = 1 + 3*q + 4*q^2 + 2*q^3 + q^4 + 3*q^5 + 6*q^6 + 4*q^7 - q^9 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 1, Boole[ n == 0], Sum[ Re[(3 - I) {1, I, -I, -1, 0}[[ Mod[ d, 5, 1] ]] ], {d, Divisors @ n}]];",
				"a[ n_] := SeriesCoefficient[ QPochhammer[ q]^2 / (QPochhammer[ q, q^5] QPochhammer[ q^4, q^5])^5, {q, 0, n}]; (* _Michael Somos_, Jun 10 2014 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n==0, sumdiv(n, d, real( (3 - I) * [ 0, 1, I, -I, -1][ d%5 + 1])))};",
				"(PARI) {a(n) = if( n\u003c0, 0, polcoeff( prod(k=1, n, (1 - x^k)^[ 2, -3, 2, 2, -3][k%5 + 1], 1 + x * O(x^n)), n))};",
				"(Sage) A = ModularForms( Gamma1(5), 1, prec=20) . basis(); A[0] + 3*A[1]; # _Michael Somos_, Jun 10 2014",
				"(MAGMA) A := Basis( ModularForms( Gamma1(5), 1), 20); A[1] + 3*A[2]; /* _Michael Somos_, Jun 10 2014 */"
			],
			"xref": [
				"Cf. A005258, A078905, A229802.",
				"The Apéry-like numbers [or Apéry-like sequences, Apery-like numbers, Apery-like sequences] include A000172, A000984, A002893, A002895, A005258, A005259, A005260, A006077, A036917, A063007, A081085, A093388, A125143 (apart from signs), A143003, A143007, A143413, A143414, A143415, A143583, A183204, A214262, A219692, A226535, A227216, A227454, A229111 (apart from signs), A260667, A260832, A262177, A264541, A264542, A279619, A290575, A290576. (The term \"Apery-like\" is not well-defined.)"
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Sep 21 2013",
			"references": 37,
			"revision": 36,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2013-09-21T17:31:12-04:00"
		}
	]
}