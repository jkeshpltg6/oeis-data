{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A306015",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 306015,
			"data": "0,1,1,1,2,1,4,6,3,1,15,24,12,4,1,76,120,60,20,5,1,455,720,360,120,30,6,1,3186,5040,2520,840,210,42,7,1,25487,40320,20160,6720,1680,336,56,8,1,229384,362880,181440,60480,15120,3024,504,72,9,1",
			"name": "Exponential series expansion of (exp(x*y) + sinh(x) - cosh(x))/(1 - x).",
			"comment": [
				"From _David Callan_, Dec 18 2021: (Start)",
				"For 0 \u003c= k \u003c= n, T(n,k) is the number of nonderangements of size n in which k of the fixed points are colored red. In particular, with D_n the derangement number A000166(n), T(n,0) = n! - D_n. For a general example, T(3,1) = 6 counts the colored permutations R23, R32, 1R3, 3R1, 12R, 21R where the red fixed points are indicated by \"R\".",
				"For n \u003e= k \u003e= 1, T(n,k) = n!/k!. Proof. In a colored permutation, such as 3R7R516 counted by T(n,k) with n = 7 and k = 2, the R's indicate (red) fixed points and so no information is lost by rank ordering the remaining entries while retaining the placement of the R's: 2R5R314. The result is a permutation of the set consisting of 1,2,...,n-k and k R's; there are n!/k! such permutations and the process is reversible. QED. (End)"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A306015/b306015.txt\"\u003eRows n=0..99 of triangle, flattened\u003c/a\u003e"
			],
			"example": [
				"  n |  k = 0       1       2      3      4     5    6   7  8  9",
				"  --+----------------------------------------------------------",
				"  0 |      0",
				"  1 |      1,      1",
				"  2 |      1,      2,      1",
				"  3 |      4,      6,      3,     1",
				"  4 |     15,     24,     12,     4,     1",
				"  5 |     76,    120,     60,    20,     5,    1",
				"  6 |    455,    720,    360,   120,    30,    6,   1",
				"  7 |   3186,   5040,   2520,   840,   210,   42,   7,  1",
				"  8 |  25487,  40320,  20160,  6720,  1680,  336,  56,  8, 1",
				"  9 | 229384, 362880, 181440, 60480, 15120, 3024, 504, 72, 9, 1"
			],
			"maple": [
				"gf := (exp(x*y) + sinh(x) - cosh(x))/(1 - x):",
				"ser := series(gf, x, 16): L := [seq(n!*coeff(ser, x, n), n=0..14)]:",
				"seq(seq(coeff(L[k+1], y, n), n=0..k), k=0..12);"
			],
			"mathematica": [
				"Join[{0}, With[{nmax = 15}, CoefficientList[CoefficientList[Series[ (Exp[x*y] + Sinh[x] - Cosh[x])/(1 - x), {x, 0, nmax}, {y, 0, nmax}], x], y ]*Range[0, nmax]!] // Flatten ] (* _G. C. Greubel_, Jul 18 2018 *)"
			],
			"xref": [
				"A094587 with an extra first column A002467.",
				"Row sums are A306150."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Peter Luschny_, Jun 23 2018",
			"references": 2,
			"revision": 23,
			"time": "2021-12-19T07:36:35-05:00",
			"created": "2018-06-28T17:07:05-04:00"
		}
	]
}