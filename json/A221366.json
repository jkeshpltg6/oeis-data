{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A221366",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 221366,
			"data": "1,5,1,45,1,320,1,2205,1,15125,1,103680,1,710645,1,4870845,1,33385280,1,228826125,1,1568397605,1,10749957120,1,73681302245,1,505019158605,1,3461452808000,1,23725150497405,1",
			"name": "The simple continued fraction expansion of F(x) := product {n = 0..inf} (1 - x^(4*n+3))/(1 - x^(4*n+1)) when x = 1/2*(7 - 3*sqrt(5)).",
			"comment": [
				"The function F(x) := product {n = 0..inf} (1 - x^(4*n+3))/(1 - x^(4*n+1)) is analytic for |x| \u003c 1. When x is a quadratic irrational of the form x = 1/2*(N - sqrt(N^2 - 4)), N an integer greater than 2, the real number F(x) has a predictable simple continued fraction expansion. The first examples of these expansions, for N = 2, 4, 6 and 8, are due to Hanna. See A174500 through A175503. The present sequence is the case N = 7. See also A221364 (N = 3), A221365 (N = 5) and A221367 (N = 9).",
				"If we denote the present sequence by [1, c(1), 1, c(2), 1, c(3), ...] then for k = 1, 2, ..., the simple continued fraction expansion of F({1/2*(7 - sqrt(45)}^k) is given by the sequence [1; c(k), 1, c(2*k), 1, c(3*k), 1, ...]. Examples are given below."
			],
			"link": [
				"P. Bala, \u003ca href=\"/A174500/a174500_2.pdf\"\u003eSome simple continued fraction expansions for an infinite product, Part 1\u003c/a\u003e",
				"\u003ca href=\"/index/Rec\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,8,0,-8,0,1)."
			],
			"formula": [
				"a(2*n-1) = (1/2*(7 + sqrt(45)))^n + (1/2*(7 - sqrt(45)))^n - 2 = A081070(n); a(2*n) = 1.",
				"a(4*n-1) = 45*A049682(n) = 45*(A004187(n))^2;",
				"a(4*n+1) = 5*(A033890(n))^2.",
				"a(n) = 8*a(n-2)-8*a(n-4)+a(n-6). G.f.: -(x^4+5*x^3-7*x^2+5*x+1) / ((x-1)*(x+1)*(x^2-3*x+1)*(x^2+3*x+1)). [_Colin Barker_, Jan 20 2013]"
			],
			"example": [
				"F(1/2*(7 - sqrt(45)) = 1.16725 98258 10214 95210 ... = 1 + 1/(5 + 1/(1 + 1/(45 + 1/(1 + 1/(320 + 1/(1 + 1/(2205 + ...))))))).",
				"F({1/2*(7 - sqrt(45)}^2) = 1.02173 93445 69104 86504 ... = 1 + 1/(45 + 1/(1 + 1/(2205 + 1/(1 + 1/(103680 + 1/(1 + 1/(4870845 + ...))))))).",
				"F({1/2*(7 - sqrt(45)}^3) = 1.00311 52648 91110 10148 ... = 1 + 1/(320 + 1/(1 + 1/(103680 + 1/(1 + 1/(33385280 + 1/(1 + 1/(10749957120 + ...)))))))."
			],
			"mathematica": [
				"LinearRecurrence[{0,8,0,-8,0,1},{1,5,1,45,1,320},40] (* or *) Riffle[ LinearRecurrence[{8,-8,1},{5,45,320},20],1,{1,-1,2}] (* _Harvey P. Dale_, Jan 04 2018 *)"
			],
			"xref": [
				"A004187, A033890, A049682, A081070, A174500 (N = 4), A221364 (N = 3), A221365 (N = 5), A221369 (N = 9)."
			],
			"keyword": "nonn,easy,cofr",
			"offset": "0,2",
			"author": "_Peter Bala_, Jan 15 2013",
			"references": 3,
			"revision": 14,
			"time": "2018-01-04T14:08:56-05:00",
			"created": "2013-01-16T09:37:54-05:00"
		}
	]
}