{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A339268",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 339268,
			"data": "11,549755813881,53,17,41,37,16349,89,977,197,193,181,173,937,929,149,137,389,1913,881,877,353,857,3917,317,821,3889,809,293,797,257,761,3821,65257,3797,3793,709,1721,3761,677,1048217,661,641,3709,3701,3697,601,577",
			"name": "The smallest prime that becomes 2 * prime(n), when all the bits in its binary expansion are inverted, or -1 if no such prime exists.",
			"comment": [
				"Conjecture: a(n) \u003e 0 for all n.",
				"a(6705), for prime(6705) = 67289, is either -1 or greater than (2^62500 - 1) * 262144 + 127565, which has 18820 digits. - _Michael S. Branicky_, Dec 05 2020"
			],
			"example": [
				"a(1) = 11, because 11 = 1011_2 -inv-\u003e 0100_2 = 4 = 2 * 2.",
				"a(2) = 549755813881, because 549755813881 = 111111111111111111111111111111111111001_2 -inv-\u003e 110_2 = 6 = 2 * 3. No smaller prime generates 3.",
				"a(3) = 53, because 53 = 110101_2 -inv-\u003e 001010_2 = 10 = 2 * 5."
			],
			"mathematica": [
				"a[n_] := Module[{q = 2*Prime[n], m, r}, m = 2^Ceiling@Log2[q]; r = m - q - 1; While[r \u003c q || ! PrimeQ[r], r += m; m *= 2]; r]; Array[a, 48] (* _Amiram Eldar_, Dec 04 2020 *)"
			],
			"program": [
				"(Python)",
				"from sympy import isprime",
				"from sympy import prime",
				"for i in range(1,50):",
				"  d=2*prime(i)",
				"  l=len(bin(d).lstrip('0b'))",
				"  xor=2**l-1",
				"  p=d^xor+2**l",
				"  while not isprime(p):",
				"   l+=1",
				"   p+=2**l",
				"  print(p,end=', ')",
				"(PARI) a(n) = {my(b = apply(x-\u003e1-x, binary(2*prime(n))), e=#b, q=fromdigits(b, 2)+2^e); while (!isprime(q), e++; q+=2^e; q); q;} \\\\ _Michel Marcus_, Dec 04 2020"
			],
			"xref": [
				"Cf. A035327, A339247."
			],
			"keyword": "nonn,base",
			"offset": "1,1",
			"author": "_Bob Andriesse_, Nov 29 2020",
			"references": 1,
			"revision": 34,
			"time": "2020-12-06T01:47:26-05:00",
			"created": "2020-12-06T01:47:26-05:00"
		}
	]
}