{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A113434",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 113434,
			"data": "4,9,10,25,49,65,289",
			"name": "Semi-Pierpont semiprimes which are also Pierpont semiprimes.",
			"comment": [
				"Semiprimes both of whose prime factors are Pierpont primes (A005109), which are primes of the form (2^K)*(3^L)+1 and where the semiprime is itself of the form (2^K)*(3^L)+1.",
				"No more under 10^50; what is the next element of this sequence?",
				"No more terms \u003c= 10^100. - _Robert Israel_, Mar 10 2017",
				"This sequence is complete, see Links. - _Charlie Neder_, Feb 04 2019"
			],
			"link": [
				"Chris Caldwell, \u003ca href=\"http://groups.yahoo.com/group/primeform/message/6588/\"\u003e\"Pierpont primes.\" primeform posting, Oct 25, 2005.\u003c/a\u003e",
				"Chris Caldwell, \u003ca href=\"/A113434/a113434.pdf\"\u003e\"Pierpont primes.\" primeform posting, Oct 25, 2005.\u003c/a\u003e [Cached copy]",
				"Charlie Neder, \u003ca href=\"/A113434/a113434.txt\"\u003eProof of the completeness of this sequence\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PierpontPrime.html\"\u003ePierpont Prime\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Semiprime.html\"\u003eSemiprime\u003c/a\u003e"
			],
			"formula": [
				"{a(n)} = intersection of A113432 and A113433. {a(n)} = Semiprimes A001358 of the form (2^K)*(3^L)+1 both of whose factors are of the form (2^K)*(3^L)+1. {a(n)} = {integers P such that, for nonnegative integers I, J, K, L, m, n there is a solution to (2^I)*(3^J)+1 = [(2^K)*(3^L)+1]*[(2^m)*(3^n)+1] where both [(2^K)*(3^L)+1] and [(2^m)*(3^n)+1] are prime}."
			],
			"example": [
				"a(1) = 4 = 2^2 = [(2^0)*(3^0)+1]*[(2^0)*(3^0)+1] = (2^0)*(3^1)+1.",
				"a(2) = 9 = 3^2 = [(2^1)*(3^0)+1]*[(2^1)*(3^0)+1] = (2^3)*(3^0)+1.",
				"a(3) = 10 = 2*5 = [(2^0)*(3^0)+1]*[(2^2)*(3^0)+1] = (2^0)*(3^2)+1.",
				"a(4) = 25 = 5^2 = [(2^2)*(3^0)+1]*[(2^2)*(3^0)+1] = (2^3)*(3^1)+1.",
				"a(5) = 49 = 7^2 = [(2^1)*(3^1)+1]*[(2^1)*(3^1)+1] = (2^4)*(3^1)+1.",
				"a(6) = 65 = 5*13 = [(2^2)*(3^0)+1]*[(2^2)*(3^1)+1] = (2^6)*(3^0)+1.",
				"a(7) = 289 = 17^2 = [(2^4)*(3^0)+1]*[(2^4)*(3^0)+1] = (2^5)*(3^2)+1."
			],
			"maple": [
				"N:= 10^100: # to get all terms \u003c= N",
				"PP:= select(isprime, {seq(seq(1+2^i*3^j, i=0..ilog2((N-1)/3^j)),j=0..floor(log[3](N-1)))}):",
				"SP:= select(t -\u003e t \u003c= N and t = 1+2^padic:-ordp(t-1,2)*3^padic:-ordp(t-1,3), [seq(seq(PP[i]*PP[j], j=1..i),i=1..nops(PP))]):",
				"sort(convert(SP,list)); # _Robert Israel_, Mar 10 2017"
			],
			"xref": [
				"Cf. A001358, A003586, A005109, A055600, A111153, A111206, A113432, A113433."
			],
			"keyword": "nonn,fini,full",
			"offset": "1,1",
			"author": "_Jonathan Vos Post_, Nov 01 2005",
			"references": 3,
			"revision": 21,
			"time": "2019-11-11T09:29:17-05:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}