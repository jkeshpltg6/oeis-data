{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A306697",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 306697,
			"data": "1,1,1,1,2,1,1,3,3,1,1,4,5,4,1,1,5,9,9,5,1,1,6,7,16,7,6,1,1,7,15,25,25,15,7,1,1,8,11,36,11,36,11,8,1,1,9,27,49,35,35,49,27,9,1,1,10,25,64,13,30,13,64,25,10,1,1,11,21,81,125,77,77,125,81",
			"name": "Square array T(n, k) read by antidiagonals, n \u003e 0 and k \u003e 0: T(n, k) is obtained by applying a Minkowski sum to sets related to the Fermi-Dirac factorizations of n and of k (see Comments for precise definition).",
			"comment": [
				"For any m \u003e 0:",
				"- let F(m) be the set of distinct Fermi-Dirac primes (A050376) with product m,",
				"- for any i \u003e=0 0 and j \u003e= 0, let f(prime(i+1)^(2^i)) be the lattice point with coordinates X=i and Y=j (where prime(k) denotes the k-th prime number),",
				"- f establishes a bijection from the Fermi-Dirac primes to the lattice points with nonnegative coordinates,",
				"- let P(m) = { f(p) | p in F(m) },",
				"- P establishes a bijection from the nonnegative integers to the set, say L, of finite sets of lattice points with nonnegative coordinates,",
				"- let Q be the inverse of P,",
				"- for any n \u003e 0 and k \u003e 0:",
				"    T(n, k) = Q(P(n) + P(k))",
				"              where \"+\" denotes the Minkowski addition on L.",
				"This sequence has similarities with A297845, and their data sections almost match; T(6, 6) = 30, however A297845(6, 6) = 90.",
				"This sequence has similarities with A067138; here we work on dimension 2, there in dimension 1.",
				"This sequence as a binary operation distributes over A059896, whereas A297845 distributes over multiplication (A003991) and A329329 distributes over A059897. See the comment in A329329 for further description of the relationship between these sequences. - _Peter Munn_, Dec 19 2019"
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A306697/a306697.gp.txt\"\u003ePARI program for A306697\u003c/a\u003e",
				"OEIS Wiki, \u003ca href=\"/wiki/%22Fermi-Dirac_representation%22_of_n\"\u003e\"Fermi-Dirac representation\" of n\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Distributive.html\"\u003eDistributive\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Minkowski_addition\"\u003eMinkowski addition\u003c/a\u003e"
			],
			"formula": [
				"For any m \u003e 0, n \u003e 0, k \u003e 0, i \u003e= 0, j \u003e= 0:",
				"- T(n, k) = T(k, n) (T is commutative),",
				"- T(m, T(n, k)) = T(T(m, n), k) (T is associative),",
				"- T(n, 1) = 1 (1 is an absorbing element for T),",
				"- T(n, 2) = n (2 is an identity element for T),",
				"- T(n, 3) = A003961(n),",
				"- T(n, 4) = n^2 (A000290),",
				"- T(n, 5) = A045966(n) (when n \u003e 1),",
				"- T(n, 7) = A045968(n) (when n \u003e 1),",
				"- T(n, 11) = A045970(n) (when n \u003e 1),",
				"- T(n, 2^(2^i)) = n^(2^i),",
				"- T(2^i, 2^j) = 2^A067138(i, j),",
				"- T(A019565(i), A019565(j)) = A019565(A067138(i, j)),",
				"- T(A000040(n), A000040(k)) = A000040(n + k - 1),",
				"- T(2^(2^i), 2^(2^j)) = 2^(2^(i + j)),",
				"- A001221(T(n, k)) \u003c= A001221(n) * A001221(k),",
				"- A064547(T(n, k)) \u003c= A064547(n) * A064547(k).",
				"From _Peter Munn_, Dec 05 2019:(Start)",
				"T(A329050(i_1, j_1), A329050(i_2, j_2)) = A329050(i_1+i_2, j_1+j_2).",
				"Equivalently, T(prime(i_1 - 1)^(2^(j_1)), prime(i_2 - 1)^(2^(j_2))) = prime(i_1+i_2 - 1)^(2^(j_1+j_2)), where prime(i) = A000040(i).",
				"T(A059896(i,j), k) = A059896(T(i,k), T(j,k)) (T distributes over A059896).",
				"T(A019565(i), 2^j) = A019565(i)^j.",
				"T(A225546(i), A225546(j)) = A225546(T(i,j)).",
				"(End)"
			],
			"example": [
				"Array T(n, k) begins:",
				"  n\\k|  1   2   3    4    5    6    7     8     9    10    11    12",
				"  ---+-------------------------------------------------------------",
				"    1|  1   1   1    1    1    1    1     1     1     1     1     1",
				"    2|  1   2   3    4    5    6    7     8     9    10    11    12",
				"    3|  1   3   5    9    7   15   11    27    25    21    13    45",
				"    4|  1   4   9   16   25   36   49    64    81   100   121   144",
				"    5|  1   5   7   25   11   35   13   125    49    55    17   175",
				"    6|  1   6  15   36   35   30   77   216   225   210   143   540",
				"    7|  1   7  11   49   13   77   17   343   121    91    19   539",
				"    8|  1   8  27   64  125  216  343   128   729  1000  1331  1728",
				"    9|  1   9  25   81   49  225  121   729   625   441   169  2025",
				"   10|  1  10  21  100   55  210   91  1000   441   110   187  2100",
				"   11|  1  11  13  121   17  143   19  1331   169   187    23  1573",
				"   12|  1  12  45  144  175  540  539  1728  2025  2100  1573   720"
			],
			"program": [
				"(PARI) See Links section."
			],
			"xref": [
				"Columns (some differing for term 1) and equivalently rows: A003961(3), A000290(4), A045966(5), A045968(7), A045970(11).",
				"Cf. A000040, A001221, A019565, A064547, A225546, A329050.",
				"Related binary operations: A067138, A059896, A297845/A003991, A329329/A059897."
			],
			"keyword": "nonn,tabl",
			"offset": "1,5",
			"author": "_Rémy Sigrist_, Mar 05 2019",
			"references": 12,
			"revision": 16,
			"time": "2020-01-14T01:09:35-05:00",
			"created": "2019-03-08T20:12:00-05:00"
		}
	]
}