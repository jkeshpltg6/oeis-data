{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A132623",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 132623,
			"data": "0,1,0,1,2,0,3,2,3,0,14,8,3,4,0,87,46,15,4,5,0,669,338,102,24,5,6,0,6098,2992,861,188,35,6,7,0,64050,30800,8589,1788,310,48,7,8,0,759817,360110,98238,19800,3275,474,63,8,9,0,10028799,4701734,1262208,248624",
			"name": "Triangle T, read by rows, where T(n,k) = Sum_{j=1..n-k-1} [T^j](n-1,k) with T(n+1,n) = n+1 and T(n,n)=0 for n\u003e=0, where T^n denotes the n-th matrix power of T.",
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A132623/b132623.txt\"\u003eRows n = 0..45, flattened.\u003c/a\u003e"
			],
			"formula": [
				"G.f. of column k: (k+1)*x^(k+1) = Sum_{n\u003e=0} T(n,k) * x^n * (1-x)^(n-k) / Product_{j=k+1..n-1} (1+j*x).",
				"T(n,k) = [x^n] { (k+1)*x^(k+1) - Sum_{m=k+1..n-1} T(m,k)*x^m*(1-x)^(m-k) / Product_{j=k+1..m-1} (1+j*x) } for n\u003ek with T(n,k)=0 when k\u003e=n."
			],
			"example": [
				"Triangle begins:",
				"0;",
				"1, 0;",
				"1, 2, 0;",
				"3, 2, 3, 0;",
				"14, 8, 3, 4, 0;",
				"87, 46, 15, 4, 5, 0;",
				"669, 338, 102, 24, 5, 6, 0;",
				"6098, 2992, 861, 188, 35, 6, 7, 0;",
				"64050, 30800, 8589, 1788, 310, 48, 7, 8, 0;",
				"759817, 360110, 98238, 19800, 3275, 474, 63, 8, 9, 0; ...",
				"-------------------------------------",
				"MATRIX POWER SERIES PROPERTY.",
				"[I - T]^-1 = Sum_{n\u003e=0} T^n and equals T shifted up 1 row",
				"(with '1's in the main diagonal):",
				"1;",
				"1, 1;",
				"3, 2, 1;",
				"14, 8, 3, 1;",
				"87, 46, 15, 4, 1;",
				"669, 338, 102, 24, 5, 1; ...",
				"-------------------------------------",
				"GENERATE T FROM MATRIX POWERS OF T.",
				"Matrix square T^2 begins:",
				"0;",
				"0, 0;",
				"2, 0, 0;",
				"5, 6, 0, 0;",
				"23, 14, 12, 0, 0;",
				"143, 78, 27, 20, 0, 0; ...",
				"so that T(4,1) = T(3,1) + [T^2](3,1) = 2 + 6 = 8;",
				"and T(3,0) = T(2,0) + [T^2](2,0) = 1 + 2 = 3.",
				"Matrix cube T^3 begins:",
				"0;",
				"0, 0;",
				"0, 0, 0;",
				"6, 0, 0, 0;",
				"26, 24, 0, 0, 0;",
				"165, 94, 60, 0, 0, 0; ...",
				"so that T(5,1) = T(4,1) + [T^2](4,1) + [T^3](4,1) = 8 + 14 + 24 = 46;",
				"and T(4,0) = T(3,0) + [T^2](3,0) + [T^3](3,0) = 3 + 5 + 6 = 14.",
				"-------------------------------------",
				"ILLUSTRATE G.F. FOR COLUMN k:",
				"k=0: x = T(1,0)*x*(1-x) + T(2,0)*x^2*(1-x)^2/((1+x)) + T(3,0)*x^3*(1-x)^3/((1+x)*(1+2*x)) + T(4,0)*x^4*(1-x)^4/((1+x)*(1+2*x)*(1+3*x)) +...",
				"k=1: 2*x^2 = T(2,1)*x^2*(1-x) + T(3,1)*x^3*(1-x)^2/((1+2*x)) + T(4,1)*x^4*(1-x)^3/((1+2*x)*(1+3*x)) + T(5,1)*x^5*(1-x)^4/((1+2*x)*(1+3*x)*(1+4*x)) +...",
				"k=2: 3*x^3 = T(3,2)*x^3*(1-x) + T(4,2)*x^4*(1-x)^2/((1+3*x)) + T(5,2)*x^5*(1-x)^3/((1+3*x)*(1+4*x)) + T(6,2)*x^6*(1-x)^4/((1+3*x)*(1+4*x)*(1+5*x)) +..."
			],
			"program": [
				"(PARI) /* Using the matrix power formula: */",
				"T(n,k)=local(M=if(n\u003c=0,Mat(1),matrix(n,n,r,c,if(r\u003e=c,T(r-1,c-1))))); if(n\u003ck || k\u003c0,0, if(n==k,0, if(n==k+1,n,sum(j=1,n-k-1,(M^j)[n,k+1]) )))",
				"(PARI) /* Using the g.f. formula for columns: */",
				"T(n,k)=if(n\u003ck+1, 0, polcoeff((k+1)*x^(k+1)-sum(m=k+1, n-1, T(m,k)*x^m*(1-x)^(m-k)/prod(j=k+1, m-1, 1+j*x+x*O(x^n))), n))",
				"for(n=0,15, for(k=0,n, print1(T(n,k),\", \")); print(\"\"))"
			],
			"xref": [
				"Cf. A132624 (column 0), A208676, A208677, A208678."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Paul D. Hanna_, Aug 25 2007",
			"references": 6,
			"revision": 15,
			"time": "2017-06-14T00:35:31-04:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}