{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A055099",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 55099,
			"data": "1,4,14,50,178,634,2258,8042,28642,102010,363314,1293962,4608514,16413466,58457426,208199210,741512482,2640935866,9405832562,33499369418,119309773378,424928058970,1513403723666,5390067288938",
			"name": "Expansion of g.f.: (1 + x)/(1 - 3*x - 2*x^2).",
			"comment": [
				"Row sums of triangle A054458.",
				"a(n) = term (1,1) in M^n, M = the 3 X 3 matrix [1,1,1; 1,1,1; 2,2,1]. - _Gary W. Adamson_, Mar 12 2009",
				"Equals the INVERT transform of A001333: (1, 3, 7, 17, 41, 99, ...). - _Gary W. Adamson_, Aug 14 2010",
				"a(n) is the number of one sided n-step walks taking steps from {(0,1), (-1,0), (1,0), (1,1)}. - _Shanzhen Gao_, May 13 2011",
				"Number of quaternary words of length n on {0,1,2,3} containing no subwords 03 or 30. - _Philippe Deléham_, Apr 27 2012",
				"Pisano period lengths: 1, 1, 4, 1, 24, 4, 48, 1, 12, 24, 30, 4, 12, 48, 24, 2, 272, 12, 18, 24, ... - _R. J. Mathar_, Aug 10 2012",
				"a(n) = A007481(2*n+1) - A007481(2*n) = A007481(2*(n+1)) - A007481(2*n+1). - _Reinhard Zumkeller_, Oct 25 2015"
			],
			"reference": [
				"I. P. Goulden and D. M. Jackson, Combinatorial Enumeration, Wiley, N.Y., 1983, (Problem 2.4.6)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A055099/b055099.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"M. Abrate, S. Barbero, U. Cerruti, and N. Murru, \u003ca href=\"http://dx.doi.org/10.1155/2013/543913\"\u003eConstruction and composition of rooted trees via descent functions\u003c/a\u003e, Algebra, Volume 2013 (2013), Article ID 543913, 11 pages.",
				"D. Birmajer, J. B. Gil, and M. D. Weiner, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL19/Gil/gil6.html\"\u003en the Enumeration of Restricted Words over a Finite Alphabet\u003c/a\u003e, J. Int. Seq. 19 (2016) # 16.1.3 , example 17",
				"A. S. Fraenkel, \u003ca href=\"https://arxiv.org/abs/math/9809074\"\u003eHeap games, numeration systems and sequences\u003c/a\u003e, arXiv:math/9809074 [math.CO], 1998; Annals of Combinatorics, 2 (1998), 197-210.",
				"Shanzhen Gao and Keh-Hsun Chen, \u003ca href=\"http://worldcomp-proceedings.com/proc/p2014/FCS2696.pdf\"\u003eTackling Sequences From Prudent Self-Avoiding Walks\u003c/a\u003e, FCS'14, The 2014 International Conference on Foundations of Computer Science.",
				"S. Gao and H. Niederhausen, \u003ca href=\"http://math.fau.edu/Niederhausen/HTML/Papers/Sequences%20Arising%20From%20Prudent%20Self-Avoiding%20Walks-February%2001-2010.pdf\"\u003eSequences Arising From Prudent Self-Avoiding Walks\u003c/a\u003e, 2010.",
				"Sergey Kitaev and Jeffrey Remmel, \u003ca href=\"http://arxiv.org/abs/1304.4286\"\u003e(a,b)-rectangle patterns in permutations and words\u003c/a\u003e, arXiv:1304.4286 [math.CO], 2013.",
				"Paul K. Stockmeyer, \u003ca href=\"http://arxiv.org/abs/1504.04404\"\u003eThe Pascal Rhombus and the Stealth Configuration\u003c/a\u003e, arXiv:1504.04404 [math.CO], 2015.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,2)."
			],
			"formula": [
				"a(n) = a*c^n - b*d^n, a := (5 + sqrt(17))/(2*sqrt(17)), b := (5 - sqrt(17))/(2*sqrt(17)), c := (3 + sqrt(17))/2, d := (3 - sqrt(17))/2.",
				"a(n) = Sum_{m=0..n} A054458(n, m).",
				"a(n) = F32(n) + F32(n-1) with F32(n) = A007482(n), n \u003e= 1, a(0) = 1.",
				"a(n) = A007482(n) + A007482(n-1) = 2*A007482(n) - A104934(n). - _R. J. Mathar_, Jul 23 2010",
				"a(n) = 3*a(n-1) + 2*a(n-2) with a(0) = 1, a(1) = 4. - _Vincenzo Librandi_, Dec 08 2010",
				"a(n) = (Sum_{k = 0..n} A202396(n,k)*3^k)/2^n. - _Philippe Deléham_, Feb 05 2012",
				"a(n) = (i*sqrt(2))^(n-1)*( i*sqrt(2)*ChebyshevU(n, -3*i/(2*sqrt(2))) + ChebyshevU(n-1, -3*i/(2*sqrt(2))) ). - _G. C. Greubel_, Jun 27 2021"
			],
			"example": [
				"a(3) = 50 because among the 4^3 = 64 quaternary words of length 3 only 14 namely 003, 030, 031, 032, 033, 103, 130, 203, 230, 300, 301, 302, 303, 330 contain the subwords 03 or 30. - _Philippe Deléham_, Apr 27 2012"
			],
			"maple": [
				"a := proc(n) option remember; `if`(n \u003c 2, [1, 4][n+1], (3*a(n-1) + 2*a(n-2))) end:",
				"seq(a(n), n=0..23); # _Peter Luschny_, Jan 06 2019"
			],
			"mathematica": [
				"max = 24; cv = ContinuedFraction[ Sqrt[2], max] // Convergents // Numerator; Series[ 1/(1 - cv.x^Range[max]), {x, 0, max}] // CoefficientList[#, x]\u0026 // Rest (* _Jean-François Alcover_, Jun 21 2013, after _Gary W. Adamson_ *)",
				"LinearRecurrence[{3, 2}, {1, 4}, 24] (* _Jean-François Alcover_, Sep 23 2017 *)"
			],
			"program": [
				"(Haskell)",
				"a055099 n = a007481 (2 * n + 1) - a007481 (2 * n)",
				"-- _Reinhard Zumkeller_, Oct 25 2015",
				"(MAGMA) I:=[1,4]; [n le 2 select I[n] else 3*Self(n-1) + 2*Self(n-2): n in [1..41]]; // _G. C. Greubel_, Jun 27 2021",
				"(Sage) [(i*sqrt(2))^(n-1)*( i*sqrt(2)*chebyshev_U(n, -3*i/(2*sqrt(2))) + chebyshev_U(n-1, -3*i/(2*sqrt(2))) ) for n in (0..40)] # _G. C. Greubel_, Jun 27 2021"
			],
			"xref": [
				"Column k=2 of A255256.",
				"Cf. A001333, A002203, A007481, A007482, A054458."
			],
			"keyword": "easy,nonn",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Apr 26 2000",
			"ext": [
				"Edited by _N. J. A. Sloane_, Jun 08 2010"
			],
			"references": 63,
			"revision": 96,
			"time": "2021-06-28T08:55:47-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}