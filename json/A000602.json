{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000602",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 602,
			"id": "M0718 N0267",
			"data": "1,1,1,1,2,3,5,9,18,35,75,159,355,802,1858,4347,10359,24894,60523,148284,366319,910726,2278658,5731580,14490245,36797588,93839412,240215803,617105614,1590507121,4111846763,10660307791,27711253769",
			"name": "Number of n-node unrooted quartic trees; number of n-carbon alkanes C(n)H(2n+2) ignoring stereoisomers.",
			"comment": [
				"Trees are unrooted, nodes are unlabeled. Every node has degree \u003c= 4.",
				"Ignoring stereoisomers means that the children of a node are unordered. They can be permuted in any way and it is still the same tree. See A000628 for the analogous sequence with stereoisomers counted.",
				"In alkanes every carbon has valence exactly 4 and every hydrogen has valence exactly 1. But the trees considered here are just the carbon \"skeletons\" (with all the hydrogen atoms stripped off) so now each carbon bonds to 1 to 4 other carbons. The degree of each node is then \u003c= 4."
			],
			"reference": [
				"K. Adam, Title?, MNU 1983, 36, 29 (in German).",
				"F. Bergeron, G. Labelle and P. Leroux, Combinatorial Species and Tree-Like Structures, Camb. 1998, p. 290.",
				"L. Bytautats, D. J. Klein, Alkane Isomere Combinatorics: Stereostructure enumeration and graph-invariant and molecylar-property distributions, J. Chem. Inf. Comput. Sci 39 (1999) 803, Table 1.",
				"A. Cayley, Über die analytischen Figuren, welche in der Mathematik Baeume genannt werden..., Chem. Ber. 8 (1875), 1056-1059.",
				"R. Davies and P. J. Freyd, C_{167}H_{336} is The Smallest Alkane with More Realizable Isomers than the Observable Universe has Particles, Journal of Chemical Education, Vol. 66, 1989, pp. 278-281.",
				"J. L. Faulon, D. Visco and D. Roe, Enumerating Molecules, In: Reviews in Computational Chemistry Vol. 21, Ed. K. Lipkowitz, Wiley-VCH, 2005.",
				"J. L. Gross and J. Yellen, eds., Handbook of Graph Theory, CRC Press, 2004; p. 529.",
				"Handbook of Combinatorics, North-Holland '95, p. 1963.",
				"J. B. Hendrickson and C. A. Parks, \"Generation and Enumeration of Carbon skeletons\", J. Chem. Inf. Comput. Sci, vol. 31 (1991) pp. 101-107. See Table 2, column 2 on page 103.",
				"M. D. Jackson and T. I. Bieber, Applications of degree distribution, 2: construction and enumeration of isomers in the alkane series, J. Chem. Info. and Computer Science, 33 (1993), 701-708.",
				"J. Lederberg et al., Applications of artificial intelligence for chemical systems, I: The number of possible organic compounds. Acyclic structures containing C, H, O and N, J. Amer. Chem. Soc., 91 (1969), 2973-2097.",
				"L. M. Masinter, Applications of artificial intelligence for chemical systems, XX, Exhaustive generation of cyclic and acyclic isomers, J. Amer. Chem. Soc., 96 (1974), 7702-7714.",
				"D. Perry, The number of structural isomers ..., J. Amer. Chem. Soc. 54 (1932), 2918-2920. [Gives a(60) correctly - compare first link below]",
				"M. Petkovsek and T. Pisanski, Counting disconnected structures: chemical trees, fullerenes, I-graphs and others, Croatica Chem. Acta, 78 (2005), 563-567.",
				"D. H. Rouvray, An introduction to the chemical applications of graph theory, Congress. Numerant., 55 (1986), 267-280. - _N. J. A. Sloane_, Apr 08 2012",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"Marten J. ten Hoor, Formula for Success?, Education in Chemistry, 2005, 42(1), 10.",
				"S. Wagner, Graph-theoretical enumeration and digital expansions: an analytic approach, Dissertation, Fakult. f. Tech. Math. u. Tech. Physik, Tech. Univ. Graz, Austria, Feb., 2006."
			],
			"link": [
				"Pontus von Brömssen, \u003ca href=\"/A000602/b000602.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e (terms n = 0..60 from N. J. A. Sloane)",
				"Jean-François Alcover, \u003ca href=\"/A000602/a000602_1.txt\"\u003eMathematica program translated from N. J. A. Sloane's Maple program for A000022, A000200, A000598, A000602, A000678\u003c/a\u003e",
				"M. van Almsick, H. Dolhaine and H. Honig, \u003ca href=\"http://dx.doi.org/10.1021/ci990121m\"\u003eEfficient algorithms to enumerate isomers and diamutamers with more than one type of substituent\u003c/a\u003e, J. Chem. Info. and Computer Science, 40 (2000), 956-966.",
				"R. Aringhieri, P. Hansen and F. Malucelli, \u003ca href=\"http://citeseer.ist.psu.edu/aringhieri99chemical.html\"\u003eChemical Tree Enumeration Algorithms\u003c/a\u003e, Report TR-99-09, Dept. Informatica, Univ. Pisa, 1999.",
				"A. T. Balaban, J. W. Kennedy and L. V. Quintas, \u003ca href=\"http://dx.doi.org/10.1021/ed065p304\"\u003eThe number of alkanes having n carbons and a longest chain of length d\u003c/a\u003e, J. Chem. Education, 65 (4 (1988), 304-313.",
				"H. Bottomley, \u003ca href=\"/A000602/a000602.gif\"\u003eIllustration of initial terms of A000022, A000200, A000602\u003c/a\u003e",
				"L. Bytautas and D. J. Klein, \u003ca href=\"http://dx.doi.org/10.1021/ci980095c\"\u003eChemical combinatorics for alkane-isomer enumeration and more\u003c/a\u003e, J. Chem. Inf. Comput. Sci., 38 (1998), 1063-1078.",
				"A. Cayley, \u003ca href=\"/A000022/a000022.pdf\"\u003eÜber die analytischen Figuren, welche in der Mathematik Bäume genannt werden und ihre Anwendung auf die Theorie chemischer Verbindungen\u003c/a\u003e, Chem. Ber. 8 (1875), 1056-1059. (Annotated scanned copy)",
				"P. Flajolet and R. Sedgewick, \u003ca href=\"http://algo.inria.fr/flajolet/Publications/books.html\"\u003eAnalytic Combinatorics\u003c/a\u003e, 2009; see page 478",
				"Alfred W. Francis, \u003ca href=\"http://pubs.acs.org/doi/pdf/10.1021/ja01198a507\"\u003eNumbers of Isomeric Alkylbenzenes\u003c/a\u003e, J. Am. Chem. Soc., 69:6 (1947), pp. 1536-1537.",
				"R. K. Guy, \u003ca href=\"/A000602/a000602.pdf\"\u003eLetter to N. J. A. Sloane, Mar. 1988\u003c/a\u003e",
				"F. Harary and R. Z. Norman, \u003ca href=\"https://doi.org/10.1090/S0002-9939-1960-0111699-6\"\u003eDissimilarity characteristic theorems for graphs\u003c/a\u003e, Proc. Amer. Math. Soc. 11 (1960), 332-334.",
				"H. R. Henze, C. M. Blair, \u003ca href=\"http://dx.doi.org/10.1021/ja01359a034\"\u003eThe number of isomeric hydrocarbons of the methane series\u003c/a\u003e, J. Amer. Chem. Soc., 53 (8) (1931), 3077-3085.",
				"H. R. Henze and C. M. Blair, \u003ca href=\"/A000601/a000601_1.pdf\"\u003eThe number of isomeric hydrocarbons of the methane series\u003c/a\u003e, J. Amer. Chem. Soc., 53 (1931), 3077-3085. (Annotated scanned copy)",
				"H. R. Henze, C. M. Blair, \u003ca href=\"http://dx.doi.org/10.1021/ja01329a033\"\u003eThe number of structurally isomeric Hydrocarbons of the Ethylene Series\u003c/a\u003e, J. Amer. Chem. Soc., 55 (2) (1933), 680-686.",
				"F. Hermann, \u003ca href=\"/A000602/a000602_3.pdf\"\u003eUeber das Problem, die Anzahl der isomeren Paraffine der Formel C_n H_{2n+2} zu bestimmung\u003c/a\u003e, Chem. Ber., 13 (1880), 792. [I (1880), Annotated scanned copy]",
				"F. Hermann, \u003ca href=\"/A000602/a000602_2.pdf\"\u003eUeber das Problem, die Anzahl der isomeren Paraffine der Formel C_n H_{2n+2} zu bestimmung/a\u003e, Chem. Ber., 30 (1897), 2423-2426. [II (1897), Annotated scanned copy]",
				"F. Hermann, \u003ca href=\"/A000602/a000602_4.pdf\"\u003eEntgegnung\u003c/a\u003e, Chem. Ber., 31 (1898), 91. [III (1898), Annotated scanned copy]",
				"Michael A. Kappler, \u003ca href=\"http://www.daylight.com/meetings/emug04/Kappler/GenSmi.html\"\u003eGENSMI: Exhaustive Enumeration of Simple Graphs.\u003c/a\u003e Daylight CIS, Inc., EuroMUG '04;4-Nov 05 2004.",
				"E. V. Konstantinova and M. V. Vidyuk, \u003ca href=\"http://dx.doi.org/10.1021/ci025659y\"\u003eDiscriminating tests of information and topological indices; animals and trees\u003c/a\u003e; J. Chem. Inf. Comput. Sci., 43 (2003), 1860-1871.",
				"J. Lederberg, \u003ca href=\"/A000602/a000602_7.pdf\"\u003eLetter to N. J. A. Sloane\u003c/a\u003e, Aug 13 1971",
				"J. Lederberg, \u003ca href=\"/A000602/a000602_8.pdf\"\u003eThe topology of molecules\u003c/a\u003e, in The Mathematical Sciences. Cambridge, Mass.: MIT Press, 1969, pages 37-51. [Annotated scanned copy]",
				"J. Lederberg, \u003ca href=\"/A000602/a000602_9.pdf\"\u003eDendral-64, I\u003c/a\u003e, Report to NASA, Dec 1964 [Annotated scanned copy]",
				"J. Lederberg, \u003ca href=\"/A000602/a000602_10.pdf\"\u003eDendral-64, II\u003c/a\u003e, Report to NASA, Dec 1965 [Annotated scanned copy]",
				"J. Lederberg, \u003ca href=\"/A000602/a000602_11.pdf\"\u003eDendral-64, III\u003c/a\u003e, Report to NASA, Dec 1968 [Annotated scanned copy]",
				"P. Leroux and B. Miloudi, \u003ca href=\"http://www.labmath.uqam.ca/~annales/volumes/16-1/PDF/053-080.pdf\"\u003eGénéralisations de la formule d'Otter\u003c/a\u003e, Ann. Sci. Math. Québec, Vol. 16, No. 1 (1992), pp. 53-80.",
				"P. Leroux and B. Miloudi, \u003ca href=\"/A000081/a000081_2.pdf\"\u003eGénéralisations de la formule d'Otter\u003c/a\u003e, Ann. Sci. Math. Québec, Vol. 16, No. 1, pp. 53-80, 1992. (Annotated scanned copy)",
				"S. M. Losanitsch, \u003ca href=\"http://dx.doi.org/10.1002/cber.189703002144\"\u003eDie Isomerie-Arten bei den Homologen der Paraffin-Reihe\u003c/a\u003e, Chem. Ber. 30 (1897), 1917-1926.",
				"S. M. Losanitsch, \u003ca href=\"/A000602/a000602_1.pdf\"\u003eDie Isomerie-Arten bei den Homologen der Paraffin-Reihe\u003c/a\u003e, Chem. Ber. 30 (1897), 1917-1926. (Annotated scanned copy)",
				"S. M. Losanitsch, \u003ca href=\"/A000602/a000602_12.pdf\"\u003eBemerkungen zu der Hermasnnschen Mitteillung: Die Anzahl...\u003c/a\u003e, Chem. Ber., 30 (1897), 3059-3060 [Annotated scanned copy]",
				"A Masoumi, M Antoniazzi, M Soutchanski, \u003ca href=\"http://www.cs.ryerson.ca/~mes/publications/MasoumiAntoniazziSoutchanskiModelOrganicChemistryPlanningOrganicSynthesis_GCAI2015.pdf\"\u003eModeling organic chemistry and planning organic synthesis\u003c/a\u003e, GCAI 2015. Global Conference on Artificial Intelligence (2015), pp. 176-195.",
				"W. R. Mueller et al., \u003ca href=\"http://dx.doi.org/10.1021/ci00066a011\"\u003eMolecular topological index\u003c/a\u003e, J. Chem. Inf. Comput. Sci., 30 (1990),160-163.",
				"R. Otter, \u003ca href=\"http://www.jstor.org/stable/1969046\"\u003eThe number of trees\u003c/a\u003e, Ann. of Math. (2) 49 (1948), 583-599 discusses asymptotics.",
				"D. Perry, \u003ca href=\"/A000602/a000602_6.pdf\"\u003eThe number of structural isomers of certain homologs of methane and methanol\u003c/a\u003e, J. Amer. Chem. Soc. 54 (1932), 2918-2920. [Gives a(60) correctly] (Annotated scanned copy)",
				"G. Polya, \u003ca href=\"http://dx.doi.org/10.1524/zkri.1936.93.1.415\"\u003eAlgebraische Berechnung der Anzahl der Isomeren einiger organischer Verbindungen\u003c/a\u003e, Zeit. f. Kristall., 93 (1936), 415-443",
				"G. Polya, \u003ca href=\"/A000598/a000598_3.pdf\"\u003eAlgebraische Berechnung der Anzahl der Isomeren einiger organischer Verbindungen\u003c/a\u003e,  Zeit. f. Kristall., 93 (1936), 415-443. (Annotated scanned copy)",
				"E. M. Rains and N. J. A. Sloane, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/cayley.html\"\u003eOn Cayley's Enumeration of Alkanes (or 4-Valent Trees)\u003c/a\u003e, J. Integer Sequences, Vol. 2 (1999), Article 99.1.1.",
				"R. C. Read, \u003ca href=\"/A000598/a000598.pdf\"\u003eThe Enumeration of Acyclic Chemical Compounds\u003c/a\u003e, pp. 25-61 of A. T. Balaban, ed., Chemical Applications of Graph Theory, Ac. Press, 1976. [Annotated scanned copy] See p. 28.",
				"R. W. Robinson, F. Harary and A. T. Balaban, \u003ca href=\"http://dx.doi.org/10.1016/0040-4020(76)80049-X\"\u003eNumbers of chiral and achiral alkanes and monosubstituted alkanes\u003c/a\u003e, Tetrahedron 32 (3) (1976), 355-361.",
				"R. W. Robinson, F. Harary and A. T. Balaban, \u003ca href=\"/A000625/a000625.pdf\"\u003eNumbers of chiral and achiral alkanes and monosubstituted alkanes\u003c/a\u003e, Tetrahedron 32 (3) (1976), 355-361. (Annotated scanned copy)",
				"Hugo Schiff, \u003ca href=\"/A002094/a002094.pdf\"\u003eZur Statistik chemischer Verbindungen\u003c/a\u003e, Berichte der Deutschen Chemischen Gesellschaft, Vol. 8, pp. 1542-1547, 1875. [Annotated scanned copy]",
				"Nicholas I. Shepherd-Barron, \u003ca href=\"https://arxiv.org/abs/1904.13344\"\u003eAsymptotic period relations for Jacobian elliptic surfaces\u003c/a\u003e, arXiv:1904.13344 [math.AG], 2019.",
				"N. J. A. Sloane, \u003ca href=\"/A000602/a000602.txt\"\u003eMaple program and first 60 terms for A000022, A000200, A000598, A000602, A000678\u003c/a\u003e",
				"N. Trinajstich, Z. Jerievi, J. V. Knop, W. R. Muller and K. Szymanski, \u003ca href=\"https://doi.org/10.1351/pac198855020379\"\u003eComputer Generation of Isomeric Structures\u003c/a\u003e, Pure \u0026 Appl. Chem., Vol. 55, No. 2, pp. 379-390, 1983.",
				"Stephan Wagner, \u003ca href=\"http://www.cs.sun.ac.za/~swagner/avwiener.pdf\"\u003eOn the average Wiener index of degree-restricted trees\u003c/a\u003e",
				"Sylvia Wenmackers, \u003ca href=\"http://www.sylviawenmackers.be/blog/2015/08/huiswerk-met-bijna-twintig-jaar-vertraging/\"\u003eHuiswerk (met bijna twintig jaar vertraging)\u003c/a\u003e (in Dutch), 2015.",
				"\u003ca href=\"/index/Tra#trees\"\u003eIndex entries for sequences related to trees\u003c/a\u003e",
				"\u003ca href=\"/index/Cor#core\"\u003eIndex entries for \"core\" sequences\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A010372(n) + A010373(n/2) for n even, a(n) = A010372(n) for n odd.",
				"Also equals A000022 + A000200 (n\u003e0), both of which have known generating functions. Also g.f. = A000678(x)-A000599(x)+A000598(x^2) = (x+x^2+2x^3...)-(x^2+x^3+3x^4...)+(1+x^2+x^4+...) = 1+x+x^2+x^3+2x^4+3x^5..."
			],
			"example": [
				"a(6)=5 because hexane has five isomers: n-hexane; 2-methylpentane; 3-methylpentane; 2,2-dimethylbutane; 2,3-dimethylbutane. - Michael Lugo (mtlugo(AT)mit.edu), Mar 15 2003 (corrected by _Andrey V. Kulsha_, Sep 22 2011)"
			],
			"maple": [
				"A000602 := proc(n)",
				"    if n=0 then",
				"        1",
				"    else",
				"        A000022(n)+A000200(n);",
				"    end if;",
				"end proc:"
			],
			"mathematica": [
				"n = 40; (* algorithm from Rains and Sloane *)",
				"S3[f_,h_,x_] := f[h,x]^3/6 + f[h,x] f[h,x^2]/2 + f[h,x^3]/3;",
				"S4[f_,h_,x_] := f[h,x]^4/24 + f[h,x]^2 f[h,x^2]/4 + f[h,x] f[h,x^3]/3 + f[h,x^2]^2/8 + f[h,x^4]/4;",
				"T[-1,z_] := 1;  T[h_,z_] := T[h,z] = Table[z^k, {k,0,n}].Take[CoefficientList[z^(n+1) + 1 + S3[T,h-1,z]z, z], n+1];",
				"Sum[Take[CoefficientList[z^(n+1) + S4[T,h-1,z]z - S4[T,h-2,z]z - (T[h-1,z] - T[h-2,z]) (T[h-1,z]-1),z], n+1], {h,1,n/2}] + PadRight[{1,1}, n+1] + Sum[Take[CoefficientList[z^(n+1) + (T[h,z] - T[h-1,z])^2/2 + (T[h,z^2] - T[h-1,z^2])/2, z],n+1], {h,0,n/2}] (* _Robert A. Russell_, Sep 15 2018 *)"
			],
			"xref": [
				"Column k=4 of A144528.",
				"A000602 = A000022 + A000200 for n\u003e0.",
				"Cf. A000598, A000625, A000628, A067608, A067609, A067610."
			],
			"keyword": "nonn,easy,core,nice",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Additional comments from Steve Strand (snstrand(AT)comcast.net), Aug 20 2003"
			],
			"references": 77,
			"revision": 181,
			"time": "2020-12-18T18:48:29-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}