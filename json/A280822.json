{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A280822",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 280822,
			"data": "1,-4,3,4,-1,-4,-11,20,-9,-8,8,12,16,-28,-3,-16,-7,0,37,-12,32,40,-37,-32,-65,44,-16,-8,41,48,-34,8,-39,4,64,-40,24,-4,3,-32,-33,-12,43,-88,81,-28,-67,44,103,32,50,88,-79,-48,-80,8,-176,-68,53,132,-16",
			"name": "Expansion of phi(-x) * f(-x)^2 * f(-x^6)^3 in powers of x where phi(), chi(), f() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A280822/b280822.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of phi(-x)^3 * f(-x^6)^3 / chi(-x)^2 in powers of x where phi(), chi(), f() are Ramanujan theta functions.",
				"Expansion of q^(-5/6) * eta(q)^4 * eta(q^6)^3 / eta(q^2) in powers of q.",
				"Euler transform of period 6 sequence [-4, -3, -4, -3, -4, -6, ...].",
				"G.f.: Product_{k\u003e0} (1 - x^k)^4 * (1 - x^(2*k))^2 * (1 + x^(2*k) + x^(4*k))^3.",
				"6 * a(n) = A116418(4*n + 3)."
			],
			"example": [
				"G.f. = 1 - 4*x + 3*x^2 + 4*x^3 - x^4 - 4*x^5 - 11*x^6 + 20*x^7 - 9*x^8 + ...",
				"G.f. = q^5 - 4*q^11 + 3*q^17 + 4*q^23 - q^29 - 4*q^35 - 11*q^41 + 20*q^47 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 4, 0, x] QPochhammer[ x]^2 QPochhammer[ x^6]^3, {x, 0, n}];",
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 4, 0, x]^3 QPochhammer[ -x, x]^2 QPochhammer[ x^6]^3, {x, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A)^4 * eta(x^6 + A)^3 / eta(x^2 + A), n))};",
				"(PARI) {a(n) = if( n\u003c0, 0, polcoeff( prod(k=1, n, (1 - x^k)^4 * (1 - x^(2*k))^2 * (1 + x^(2*k) + x^(4*k))^3, 1 + x * O(x^n)), n))};"
			],
			"xref": [
				"Cf. A116418."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Jan 08 2017",
			"references": 2,
			"revision": 9,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2017-01-08T15:55:15-05:00"
		}
	]
}