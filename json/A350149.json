{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A350149",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 350149,
			"data": "1,1,1,4,2,2,27,9,6,6,256,64,32,24,24,3125,625,250,150,120,120,46656,7776,2592,1296,864,720,720,823543,117649,33614,14406,8232,5880,5040,5040,16777216,2097152,524288,196608,98304,61440,46080,40320,40320",
			"name": "Triangle read by rows: T(n,k) = n^(n-k)*k!.",
			"comment": [
				"T(n,k) are the denominators in a double summation power series for the definite integral of x^x. First expand x^x = exp(x*log(x)) = Sum_{n\u003e=0} (x*log(x))^n/n!, then integrate each of the terms to get the double summation for F(x) = Integral_{t=0..x} t^t = Sum_{n\u003e=1} (Sum_{k=0..n-1} (-1)^(n+k+1)*x^n*(log(x))^k/T(n,k)).",
				"This is a definite integral, because lim {x-\u003e0} F(x) = 0.",
				"The value of F(1) = 0.78343... = A083648 is known humorously as the Sophomore's Dream (see Borwein et al.)."
			],
			"reference": [
				"Borwein, J., Bailey, D. and Girgensohn, R., Experimentation in Mathematics: Computational Paths to Discovery, A. K. Peters 2004.",
				"William Dunham, The Calculus Gallery, Masterpieces from Newton to Lebesgue, Princeton University Press, Princeton NJ 2005."
			],
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"https://mathworld.wolfram.com/SophomoresDream.html\"\u003eSophomore's dream\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Sophomore\u0026#39;s_dream\"\u003eSophomore's dream\u003c/a\u003e"
			],
			"example": [
				"Triangle T(n,k) begins:",
				"--------------------------------------------------------------------------",
				"n/k         0        1       2       3      4      5      6      7      8",
				"--------------------------------------------------------------------------",
				"0  |        1,",
				"1  |        1,       1,",
				"2  |        4,       2,      2,",
				"3  |       27,       9,      6,      6,",
				"4  |      256,      64,     32,     24,    24,",
				"5  |     3125,     625,    250,    150,   120,   120,",
				"6  |    46656,    7776,   2592,   1296,   864,   720,   720,",
				"7  |   823543,  117649,  33614,  14406,  8232,  5880,  5040,  5040,",
				"8  | 16777216, 2097152, 524288, 196608, 98304, 61440, 46080, 40320, 40320.",
				"..."
			],
			"maple": [
				"T := (n, k) -\u003e n^(n - k)*k!:",
				"seq(seq(T(n, k), k = 0..n), n = 0..9); # _Peter Luschny_, Jan 07 2022"
			],
			"mathematica": [
				"T[n_, k_] := n^(n - k)*k!; Table[T[n, k], {n, 0, 8}, {k, 0, n}] // Flatten (* _Amiram Eldar_, Dec 27 2021 *)"
			],
			"xref": [
				"Cf. A000312 (first column), A000169 (2nd column), A003308 (3rd column excluding first term), A000142 (main diagonal), A000142 (2nd diagonal excluding first term), A112541 (row sums).",
				"Values of the integral: A083648, A073009."
			],
			"keyword": "easy,nonn,tabl,new",
			"offset": "0,4",
			"author": "_Robert B Fowler_, Dec 27 2021",
			"references": 0,
			"revision": 79,
			"time": "2022-01-09T10:39:14-05:00",
			"created": "2022-01-09T10:39:14-05:00"
		}
	]
}