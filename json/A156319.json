{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A156319",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 156319,
			"data": "1,2,1,0,2,1,0,0,2,1,0,0,0,2,1,0,0,0,0,2,1,0,0,0,0,0,2,1,0,0,0,0,0,0,2,1,0,0,0,0,0,0,0,2,1,0,0,0,0,0,0,0,0,2,1,0,0,0,0,0,0,0,0,0,2,1,0,0,0,0,0,0,0,0,0,0,2,1,0,0,0,0,0,0,0,0,0,0,0,2,1",
			"name": "Triangle by columns: (1, 2, 0, 0, 0, ...) in every column.",
			"comment": [
				"Binomial transform of the triangle = A110813.",
				"Eigensequence of the triangle = A001045",
				"Inverse = a triangle with (1, -2, 4, -8, 16, ...) in every column.",
				"Triangle T(n,k), 0 \u003c= k \u003c= n, given by [2,-2,0,0,0,0,0,0,...] DELTA [1,0,0,0,0,0,0,0,...] where DELTA is the operator defined in A084938. - _Philippe Deléham_, Feb 08 2009"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A156319/b156319.txt\"\u003eRows n = 1..100 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"Triangle read by rows, T(n,k) = 1 if n=k, 2 if k = n-1, 0 otherwise.",
				"By columns, (1, 2, 0, 0, 0, ...) in every column.",
				"T(n,k) = A097806(n,k)*2^(n-k). - _Philippe Deléham_, Feb 08 2009",
				"G.f.: (1+2*x)*x*y/(1-x*y). - _R. J. Mathar_, Aug 12 2015"
			],
			"example": [
				"First few rows of the triangle:",
				"  1;",
				"  2, 1;",
				"  0, 2, 1;",
				"  0, 0, 2, 1;",
				"  0, 0, 0, 2, 1;",
				"  0, 0, 0, 0, 2, 1;",
				"  0, 0, 0, 0, 0, 2, 1;",
				"  0, 0, 0, 0, 0, 0, 2, 1;",
				"  0, 0, 0, 0, 0, 0, 0, 2, 1;",
				"..."
			],
			"maple": [
				"T:= proc (n) option remember;",
				"if k=n then 1",
				"elif k=n-1 then 2",
				"else 0 fi;",
				"end proc;",
				"seq(seq(T(n,k), k=1..n), n = 1..15); # _G. C. Greubel_, Sep 20 2019"
			],
			"mathematica": [
				"Table[If[k==n,1, If[k==n-1, 2, 0]], {n,15}, {k,n}]//Flatten (* _G. C. Greubel_, Sep 20 2019 *)"
			],
			"program": [
				"(PARI) T(n,k) = if(k==n, 1, if(k==n-1, 2, 0)); \\\\ _G. C. Greubel_, Sep 20 2019",
				"(MAGMA) T:= func\u003c n,k | k eq n select 1 else k eq n-1 select 2 else 0 \u003e;",
				"[T(n,k): k in [1..n], n in [1..15]]; // _G. C. Greubel_, Sep 20 2019",
				"(Sage)",
				"def T(n,k):",
				"    if (k==n): return 1",
				"    elif (k==n-1): return 2",
				"    else: return 0",
				"[[T(n,k) for k in (1..n)] for n in (1..15)] # _G. C. Greubel_, Sep 20 2019",
				"(GAP)",
				"T:= function(n,k)",
				"    if k=n then return 1;",
				"    elif k=n-1 then return 2;",
				"    else return 0;",
				"    fi;",
				"  end;",
				"Flat(List([1..15], n-\u003e List([1..n], k-\u003e T(n,k) ))); # _G. C. Greubel_, Sep 20 2019"
			],
			"xref": [
				"Cf. A001045, A097806, A110813."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "1,2",
			"author": "_Gary W. Adamson_, Feb 07 2009",
			"ext": [
				"More terms added by _G. C. Greubel_, Sep 20 2019"
			],
			"references": 2,
			"revision": 15,
			"time": "2020-01-26T01:03:54-05:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}