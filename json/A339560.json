{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A339560",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 339560,
			"data": "1,0,0,1,1,2,2,4,5,8,8,13,17,22,28,39,48,62,81,101,127,167,202,253,318,395,486,608,736,906,1113,1353,1637,2011,2409,2922,3510,4227,5060,6089,7242",
			"name": "Number of integer partitions of n that can be partitioned into distinct pairs of distinct parts, i.e., into a set of edges.",
			"comment": [
				"Naturally, such a partition must have an even number of parts. Its multiplicities form a graphical partition (A000569, A320922), and vice versa."
			],
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GraphicalPartition.html\"\u003eGraphical partition.\u003c/a\u003e"
			],
			"formula": [
				"A027187(n) = a(n) + A339559(n)."
			],
			"example": [
				"The a(3) = 1 through a(11) = 13 partitions (A = 10):",
				"  (21)  (31)  (32)  (42)  (43)    (53)    (54)    (64)    (65)",
				"              (41)  (51)  (52)    (62)    (63)    (73)    (74)",
				"                          (61)    (71)    (72)    (82)    (83)",
				"                          (3211)  (3221)  (81)    (91)    (92)",
				"                                  (4211)  (3321)  (4321)  (A1)",
				"                                          (4221)  (5221)  (4322)",
				"                                          (4311)  (5311)  (4331)",
				"                                          (5211)  (6211)  (4421)",
				"                                                          (5321)",
				"                                                          (5411)",
				"                                                          (6221)",
				"                                                          (6311)",
				"                                                          (7211)",
				"For example, the partition y = (4,3,3,2,1,1) can be partitioned into a set of edges in two ways:",
				"  {{1,2},{1,3},{3,4}}",
				"  {{1,3},{1,4},{2,3}},",
				"so y is counted under a(14)."
			],
			"mathematica": [
				"strs[n_]:=If[n\u003c=1,{{}},Join@@Table[Map[Prepend[#,d]\u0026,Select[strs[n/d],Min@@#\u003ed\u0026]],{d,Select[Rest[Divisors[n]],And[SquareFreeQ[#],PrimeOmega[#]==2]\u0026]}]];",
				"Table[Length[Select[IntegerPartitions[n],strs[Times@@Prime/@#]!={}\u0026]],{n,0,15}]"
			],
			"xref": [
				"A338916 allows equal pairs (x,x).",
				"A339559 counts the complement in even-length partitions.",
				"A339561 gives the Heinz numbers of these partitions.",
				"A339619 counts factorizations of the same type.",
				"A000070 counts non-multigraphical partitions of 2n, ranked by A339620.",
				"A000569 counts graphical partitions, ranked by A320922.",
				"A001358 lists semiprimes, with squarefree case A006881.",
				"A002100 counts partitions into squarefree semiprimes.",
				"A058696 counts partitions of even numbers, ranked by A300061.",
				"A209816 counts multigraphical partitions, ranked by A320924.",
				"A320655 counts factorizations into semiprimes.",
				"A320656 counts factorizations into squarefree semiprimes.",
				"A339617 counts non-graphical partitions of 2n, ranked by A339618.",
				"A339655 counts non-loop-graphical partitions of 2n, ranked by A339657.",
				"A339656 counts loop-graphical partitions, ranked by A339658.",
				"A339659 counts graphical partitions of 2n into k parts.",
				"The following count partitions of even length and give their Heinz numbers:",
				"- A027187 has no additional conditions (A028260).",
				"- A096373 cannot be partitioned into strict pairs (A320891).",
				"- A338914 can be partitioned into strict pairs (A320911).",
				"- A338915 cannot be partitioned into distinct pairs (A320892).",
				"- A338916 can be partitioned into distinct pairs (A320912).",
				"- A339559 cannot be partitioned into distinct strict pairs (A320894).",
				"Cf. A001055, A001221, A005117, A007717, A025065, A030229, A320893, A338899, A338903, A339564."
			],
			"keyword": "nonn,more",
			"offset": "0,6",
			"author": "_Gus Wiseman_, Dec 10 2020",
			"references": 25,
			"revision": 12,
			"time": "2020-12-18T07:58:59-05:00",
			"created": "2020-12-18T07:58:59-05:00"
		}
	]
}