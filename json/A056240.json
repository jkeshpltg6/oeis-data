{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A056240",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 56240,
			"data": "2,3,4,5,8,7,15,14,21,11,35,13,33,26,39,17,65,19,51,38,57,23,95,46,69,92,115,29,161,31,87,62,93,124,155,37,217,74,111,41,185,43,123,86,129,47,215,94,141,188,235,53,329,106,159,212,265,59,371,61,177,122",
			"name": "Smallest number whose prime divisors (taken with multiplicity) add to n.",
			"comment": [
				"a(n) = index of first occurrence of n in A001414.",
				"From _David James Sycamore_ and _Michel Marcus_, Jun 16 2017, Jun 28 2017: (Start)",
				"Recursive calculation of a(n):",
				"For prime p, a(p) = p.",
				"For even composite n, let P_n denote the largest prime \u003c n-1 such that n-P_n is prime (except if n = 6).",
				"For odd composite n, let P_n denote the largest prime \u003c n-1 such that n-3-P_n is prime.",
				"Conjecture: a(n) = min { q*a(n-q); q prime, P_n \u003c= q \u003c n-1 }.",
				"Examples:",
				"For n = 9998, P_9998 = 9967 and a(9998) = min { 9973*a(25), 9967*a(31) } = 9967*31 = 308977.",
				"For n = 875, P_875 = 859 and a(875) = min { 863*a(12), 859*a(16) } = 863*35 = 30205.",
				"Note: A000040 and A288313 are both subsequences of this sequence. (End)"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A056240/b056240.txt\"\u003eTable of n, a(n) for n = 2..10000\u003c/a\u003e",
				"H. Havermann, \u003ca href=\"http://chesswanks.com/seq/sopfr/\"\u003eTables of sum-of-prime-factors sequences (overview with links to the first 50000 sums).\u003c/a\u003e"
			],
			"formula": [
				"Trivial but essential: a(n) \u003e= n. - _David A. Corneth_, Mar 23 2018",
				"a(n) \u003e= n with equality iff n is prime. - _M. F. Hasler_, Jan 19 2019"
			],
			"example": [
				"a(8) = 15 = 3*5 because 15 is the smallest number whose prime divisors sum to 8.",
				"a(10000) = 586519: Let pp(n) be the largest prime \u003c n and the candidate being the current value that might be a(10000). Then we see that pp(10000 - 1) = 9967, giving a candidate 9967 * a(10000 - 9967) = 9967 * 62. p(9967) = 9949, giving the candidate 9949 * a(10000 - 9949) = 9962 * 188. This is larger than our candidate so we keep 9967 * 62 as our candidate. pp(9949) = 9941, giving a candidate 9941 * pp(10000 - 9941) = 9941 * 59. We see that (n - p) * a(p) \u003e= (n - p) * p \u003e candidate = 9941 * 59 for p \u003e 59 so we stop iterating to conclude a(10000) = 9941 * 59 = 586519. - _David A. Corneth_, Mar 23 2018, edited by _M. F. Hasler_, Jan 19 2019"
			],
			"mathematica": [
				"a = Table[0, {75}]; Do[b = Plus @@ Flatten[ Table[ #1, {#2}] \u0026 @@@ FactorInteger[n]]; If[b \u003c 76 \u0026\u0026 a[[b]] == 0, a[[b]] = n], {n, 2, 1000}]; a (* _Robert G. Wilson v_, May 04 2002 *)",
				"b[n_] := b[n] = Total[Times @@@ FactorInteger[n]];",
				"a[n_] := For[k = 2, True, k++, If[b[k] == n, Return[k]]];",
				"Table[a[n], {n, 2, 63}] (* _Jean-François Alcover_, Jul 03 2017 *)"
			],
			"program": [
				"(Haskell)",
				"a056240 = (+ 1) . fromJust . (`elemIndex` a001414_list)",
				"-- _Reinhard Zumkeller_, Jun 14 2012",
				"(PARI) isok(k, n) = my(f=factor(k)); sum(j=1, #f~, f[j,1]*f[j,2]) == n;",
				"a(n) = my(k=2); while(!isok(k, n), k++); k; \\\\ _Michel Marcus_, Jun 21 2017",
				"(PARI) a(n) = {if(n \u003c= 5, return(n)); my(p = precprime(n), res = p * (n - p)); if(p == n, return(p), p = precprime(n - 2); res = p * a(n - p); while(res \u003e (n - p) * p \u0026\u0026 p \u003e 2, p = precprime(p - 1); res = min(res, a(n - p) * p)); res)} \\\\ _David A. Corneth_, Mar 23 2018",
				"(PARI) A056240(n, p=n-1, m=oo)=if(n\u003c6 || isprime(n), n, n==6, 8, until(p\u003c3 || (n-p=precprime(p-1))*p \u003e= m=min(m,A056240(n-p)*p),); m) \\\\ _M. F. Hasler_, Jan 19 2019"
			],
			"xref": [
				"Cf. A001414, A064502, A000040, A288313.",
				"First column of array A064364, n\u003e=2.",
				"See A000792 for the maximal numbers whose prime factors sums up to n."
			],
			"keyword": "nonn,easy",
			"offset": "2,1",
			"author": "_Adam Kertesz_, Aug 19 2000",
			"ext": [
				"More terms from _James A. Sellers_, Aug 25 2000"
			],
			"references": 33,
			"revision": 76,
			"time": "2019-01-21T12:16:35-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}