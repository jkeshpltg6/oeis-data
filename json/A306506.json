{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A306506",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 306506,
			"data": "1,1,1,4,4,3,15,19,15,10,76,99,86,67,42,455,603,544,455,358,216,3186,4248,3934,3486,2921,2250,1320,25487,34115,32079,29296,25487,21514,16296,9360,229384,307875,292509,272064,245806,214551,179058,133800,75600",
			"name": "Number T(n,k) of permutations p of [n] having at least one index i with |p(i)-i| = k; triangle T(n,k), n\u003e=1, 0\u003c=k\u003c=n-1, read by rows.",
			"comment": [
				"T(n,k) is defined for n,k\u003e=0. The triangle contains only the terms with k\u003cn. T(n,k) = 0 for k\u003e=n."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A306506/b306506.txt\"\u003eRows n = 1..35, flattened\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Permutation\"\u003ePermutation\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = n! - A306512(n,k).",
				"T(2n,n) = T(2n,0) = A002467(2n) = (2n)! - A306535(n)."
			],
			"example": [
				"The 6 permutations p of [3]: 123, 132, 213, 231, 312, 321 have absolute displacement sets {|p(i)-i|, i=1..3}: {0}, {0,1}, {0,1}, {1,2}, {1,2}, {0,2}, respectively. Number 0 occurs four times, 1 occurs four times, and 2 occurs thrice. So row n=3 is [4, 4, 3].",
				"Triangle T(n,k) begins:",
				"      1;",
				"      1,     1;",
				"      4,     4,     3;",
				"     15,    19,    15,    10;",
				"     76,    99,    86,    67,    42;",
				"    455,   603,   544,   455,   358,   216;",
				"   3186,  4248,  3934,  3486,  2921,  2250,  1320;",
				"  25487, 34115, 32079, 29296, 25487, 21514, 16296, 9360;",
				"  ..."
			],
			"maple": [
				"b:= proc(s, d) option remember; (n-\u003e `if`(n=0, add(x^j, j=d),",
				"      add(b(s minus {i}, d union {abs(n-i)}), i=s)))(nops(s))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=0..n-1))(b({$1..n}, {})):",
				"seq(T(n), n=1..9);",
				"# second Maple program:",
				"T:= proc(n, k) option remember; n!-LinearAlgebra[Permanent](",
				"      Matrix(n, (i, j)-\u003e `if`(abs(i-j)=k, 0, 1)))",
				"    end:",
				"seq(seq(T(n, k), k=0..n-1), n=1..9);"
			],
			"mathematica": [
				"T[n_, k_] := n!-Permanent[Table[If[Abs[i-j]==k, 0, 1], {i, 1, n}, {j, 1, n} ]];",
				"Table[T[n, k], {n, 1, 9}, {k, 0, n-1}] // Flatten (* _Jean-François Alcover_, May 01 2019, from 2nd Maple program *)"
			],
			"xref": [
				"Columns k=0-3 give: A002467, A306511, A306524, A324366.",
				"T(n+2,n+1) gives A007680 (for n\u003e=0).",
				"T(2n,n) gives A306675.",
				"Cf. A000142, A010050, A306461, A306512, A306535."
			],
			"keyword": "nonn,tabl",
			"offset": "1,4",
			"author": "_Alois P. Heinz_, Feb 20 2019",
			"references": 7,
			"revision": 45,
			"time": "2021-05-03T07:16:49-04:00",
			"created": "2019-02-20T16:20:34-05:00"
		}
	]
}