{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A254764",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 254764,
			"data": "3,5,7,7,11,9,11,15,13,13,17,19,15,17,19,17,19,19,23,25,23,21,25,23,27,29,29,25,27,35,31,27,29,33,29,29,31,35,31,37,43,35,33,37,33,35,33,41,47,35",
			"name": "Fundamental positive solution x = x1(n) of the first class of the Pell equation x^2 - 2*y^2 = A007522(n), n \u003e=1 (primes congruent to 7 mod 8).",
			"comment": [
				"For the corresponding term y1(n) see A254765(n).",
				"For the positive fundamental proper (sometimes called primitive) solutions x2(n) and y2(n) of the second class of this (generalized) Pell equation see A254766(n) and A254929(n).",
				"The present solutions of the first class are the smallest positive ones.",
				"See the Nagell reference Theorem 111 p. 210 for the proof of the existence of solutions (the discriminant of this binary quadratic form is +8 hence it is an indefinite form with an infinitude of solutions if there exists at least one).",
				"See the Nagell reference Theorem 110, p. 208 for the proof that there are only two classes of solutions for this Pell equation, because the equation is solvable, and the primes A007522(n) do not divide 4.",
				"The present fundamental solutions are found according to the Nagell reference Theorem 108, p. 205, adapted to the case at hand, by scanning the following two inequalities for solutions x1(n) = 2*X1(n) + 1 and y1(n) = 2*Y1(n) + 1. The intervals for X1(n) and Y1(n) to be scanned are ceiling((sqrt(2+p(n))-1)/2) \u003c= X1(n) \u003c=  floor(sqrt((2*p(n))-1)/2), with p(n) = A007522(n) and 0 \u003c= Y1(n) \u003c= floor((sqrt(p(n)/2)-1)/2).",
				"The general positive proper solutions for both classes are obtained by applying positive powers of the matrix M = [[3,4],[2,3]] on the fundamental column vectors (x(n),y(m))^T.",
				"The least positive x solutions (that is the ones of the first class) for the primes +1 and -1 (mod 8) together (including also prime 2) are given in A002334."
			],
			"reference": [
				"T. Nagell, Introduction to Number Theory, Chelsea Publishing Company, New York, 1964."
			],
			"formula": [
				"a(n)^2 - 2*A254765(n)^2 = A007522(n) gives the smallest positive (proper) solution of this (generalized) Pell equation."
			],
			"example": [
				"The first pairs [x1(n), y1(n)] of the fundamental positive solutions of this first class are (we list the prime A007522(n) as first entry):",
				"  [7, [3, 1]], [23, [5, 1]], [31, [7, 3]], [47, [7, 1]], [71, [11, 5]], [79, [9, 1]], [103, [11, 3]], [127, [15, 7]], [151, [13, 3]], [167, [13, 1]], [191, [17, 7]], [199, [19, 9]], [223, [15, 1]], ...",
				"a(3)^2 - 2*A254765(3)^2 = 7^2 - 2*3^2 = 31 = A007522(3)."
			],
			"xref": [
				"Cf. A007522, A254765, A254766, A254929, A254760, A254761, A254762, A254763, A002334."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_Wolfdieter Lang_, Feb 12 2015",
			"references": 3,
			"revision": 18,
			"time": "2015-02-19T12:14:57-05:00",
			"created": "2015-02-14T23:43:32-05:00"
		}
	]
}