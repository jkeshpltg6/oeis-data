{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A245182",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 245182,
			"data": "1,1,1,2,1,3,6,4,1,12,26,20,7,1,60,140,121,51,11,1,360,894,849,410,110,16,1,2520,6594,6763,3634,1135,211,22,1,20160,55152,60304,35336,12404,2723,371,29,1,181440,515808,595404,374940,144613,36001,5866,610,37,1",
			"name": "Irregular triangle read by rows: T(n,k) (n\u003e=2, 1\u003c=k\u003c=n) gives number of arrangements of the elements from the multiset M(n, 2) into exactly k disjoint cycles.",
			"comment": [
				"The multiset M(n, 2) consists of 2 1's and one copy of each of 2..n-1. For example, M(5,2) is the multiset {1,1,2,3,4}. See Griffiths reference for formula. - _Andrew Howroyd_, Feb 24 2020"
			],
			"link": [
				"Martin Griffiths, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Griffiths/griffiths31.html\"\u003eGenerating Functions for Extended Stirling Numbers of the First Kind\u003c/a\u003e, Journal of Integer Sequences, 17 (2014), #14.6.4."
			],
			"formula": [
				"T(n,k) = Sum_{m=0..k} |Stirling1(n-2, m)| * Sum_{j=0, 2-k+m} binomial(n+j-3, j) * A008284(2-j, k-m) for n \u003e= 2. - _Andrew Howroyd_, Feb 24 2020"
			],
			"example": [
				"Triangle begins:",
				"  1 1",
				"  1 2 1",
				"  3 6 4 1",
				"  12 26 20 7 1",
				"  60 140 121 51 11 1",
				"  360 894 849 410 110 16 1",
				"  2520 6594 6763 3634 1135 211 22 1",
				"...",
				"From _Andrew Howroyd_, Feb 24 2020: (Start)",
				"For n = 4, arrangements of the multiset {1,1,2,3} into cycles are:",
				"T(4,1) = 3: (1123), (1132), (1213);",
				"T(4,2) = 6: (112)(3), (113)(2), (123)(1), (132)(1), (11)(23) (12)(13);",
				"T(4,3) = 4: (11)(2)(3), (12)(1)(3), (13)(1)(2), (23)(1)(1);",
				"T(4,4) = 1: (1)(1)(2)(3).",
				"(End)"
			],
			"program": [
				"T(n)={my(P=matrix(3, 3, n, k, #partitions(n-k, k-1))); matrix(n, n, n, k, if(n\u003c2, 0, sum(m=0, k, abs(stirling(n-2, m, 1)) * sum(j=0, 2-k+m, binomial(n+j-3, j)*P[1+2-j, 1+k-m]))))}",
				"{my(A=T(10)); for(n=2, #A, print(A[n, 1..n]))} \\\\ _Andrew Howroyd_, Feb 24 2020"
			],
			"xref": [
				"Cf. A008284, A245183, A245184."
			],
			"keyword": "nonn,tabf",
			"offset": "2,4",
			"author": "_N. J. A. Sloane_, Jul 17 2014",
			"ext": [
				"Terms a(36) and beyond from _Andrew Howroyd_, Feb 24 2020"
			],
			"references": 2,
			"revision": 15,
			"time": "2020-02-24T17:16:01-05:00",
			"created": "2014-07-17T23:38:43-04:00"
		}
	]
}