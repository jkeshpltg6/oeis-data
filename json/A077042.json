{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A077042",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 77042,
			"data": "1,0,1,0,1,1,0,1,1,1,0,1,2,1,1,0,1,3,3,1,1,0,1,6,7,4,1,1,0,1,10,19,12,5,1,1,0,1,20,51,44,19,6,1,1,0,1,35,141,155,85,27,7,1,1,0,1,70,393,580,381,146,37,8,1,1,0,1,126,1107,2128,1751,780,231,48,9,1,1,0,1,252,3139",
			"name": "Square array read by falling antidiagonals of central polynomial coefficients: largest coefficient in expansion of (1 + x + x^2 + ... + x^(n-1))^k = ((1-x^n)/(1-x))^k, i.e., the coefficient of x^floor(k*(n-1)/2) and of x^ceiling(k*(n-1)/2); also number of compositions of floor(k*(n+1)/2) into exactly k positive integers each no more than n.",
			"comment": [
				"From _Michel Marcus_, Dec 01 2012: (Start)",
				"A pair of numbers written in base n are said to be comparable if all digits of the first number are at least as big as the corresponding digit of the second number, or vice versa. Otherwise, this pair will be defined as uncomparable. A set of pairwise uncomparable integers will be called anti-hierarchic.",
				"T(n,k) is the size of the maximal anti-hierarchic set of integers written with k digits in base n.",
				"For example, for base n=2 and k=4 digits:",
				"- 0 (0000) and 15 (1111) are comparable, while 6 (0110) and 9 (1001) are uncomparable,",
				"- the maximal antihierarchic set is {3 (0011), 5 (0101), 6 (0110), 9 (1001), 10 (1010), 12 (1100)} with 6 elements that are all pairwise uncomparable. (End)"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A077042/b077042.txt\"\u003eTable of n, a(n) for the first 50 rows, flattened\u003c/a\u003e",
				"Denis Bouyssou, Thierry Marchant, Marc Pirlot, \u003ca href=\"https://arxiv.org/abs/1903.07569\"\u003eThe size of the largest antichains in products of linear orders\u003c/a\u003e, arXiv:1903.07569 [math.CO], 2019.",
				"J. W. Sander, \u003ca href=\"https://doi.org/10.1016/0012-365X(93)90515-U\"\u003eOn maximal antihierarchic sets of integers\u003c/a\u003e, Discrete Mathematics, Volume 113, Issues 1-3, 5 April 1993, Pages 179-189.",
				"\u003ca href=\"/index/Com#comp\"\u003eIndex entries for sequences related to compositions\u003c/a\u003e"
			],
			"formula": [
				"By the central limit theorem, T(n,k) is roughly n^(k-1)*sqrt(6/(Pi*k)).",
				"T(n,k) = Sum{j=0,h/n} (-1)^j*binomial(k,j)*binomial(k-1+h-n*j,k-1) with h=floor(k*(n-1)/2), k\u003e0. - _Michel Marcus_, Dec 01 2012"
			],
			"example": [
				"Rows of square array start:",
				"  1,    0,    0,    0,    0,    0,    0, ...",
				"  1,    1,    1,    1,    1,    1,    1, ...",
				"  1,    1,    2,    3,    6,   10,   20, ...",
				"  1,    1,    3,    7,   19,   51,  141, ...",
				"  1,    1,    4,   12,   44,  155,  580, ...",
				"  1,    1,    5,   19,   85,  381, 1751, ...",
				"  ...",
				"Read by antidiagonals:",
				"  1;",
				"  0, 1;",
				"  0, 1, 1;",
				"  0, 1, 1, 1;",
				"  0, 1, 2, 1, 1;",
				"  0, 1, 3, 3, 1, 1;",
				"  0, 1, 6, 7, 4, 1, 1;",
				"  ..."
			],
			"mathematica": [
				"t[n_, k_] := Max[ CoefficientList[ Series[ ((1-x^n) / (1-x))^k, {x, 0, k*(n-1)}], x]]; t[0, 0] = 1; t[0, _] = 0; Flatten[ Table[ t[n-k, k], {n, 0, 12}, {k, n, 0, -1}]] (* _Jean-François Alcover_, Nov 04 2011 *)"
			],
			"program": [
				"(PARI) T(n,k)=if(n\u003c1 || k\u003c1,k==0,vecmax(Vec(((1-x^n)/(1-x))^k)))"
			],
			"xref": [
				"Rows include A000007, A000012, A001405, A002426, A005190, A005191, A018901, A025012, A025013, A025014, A025015, A201549, A225779, A201550. Columns include A000012, A000012, A001477, A077043, A005900, A077044, A071816, A133458.",
				"Central diagonal is A077045, with A077046 and A077047 either side. Cf. A067059, A270918, A201552.",
				"Cf. A273975."
			],
			"keyword": "nonn,tabl",
			"offset": "0,13",
			"author": "_Henry Bottomley_, Oct 22 2002",
			"references": 12,
			"revision": 38,
			"time": "2019-04-29T20:34:47-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}