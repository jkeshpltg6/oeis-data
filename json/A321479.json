{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A321479",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 321479,
			"data": "1,2,3,4,6,3,4,6,4,3,4,6,5,10,3,4,6,6,12,6,3,4,6,7,8,8,14,3,4,6,8,6,4,6,8,3,4,6,9,12,18,9,12,18,3,4,6,10,30,6,12,6,15,10,3,4,6,11,5,10,12,12,5,10,22,3,4,6,12,12,12,6,4,6,12,12,12,3",
			"name": "Regular triangle read by rows: T(n,k) is the period of {A316269(k,m)} modulo n, 0 \u003c= k \u003c= n - 1.",
			"comment": [
				"The period of {A316269(k,m)} modulo n is the smallest l such that A316269(k,m) == A316269(k,m+l) (mod n) for every m \u003e= 0. Clearly, T(n,k) is divisible by A321478(n,k). Actually, the ratio is always 1 or 2.",
				"Though {A316269(0,m)} is not defined, it can be understood as the sequence 0, 1, 0, -1, 0, 1, 0, -1, ... So the first column of each row (apart from the first and second ones) is always 4.",
				"Though {A316269(1,m)} is not defined, it can be understood as the sequence 0, 1, 1, 0, -1, -1, 0, 1, 1, 0, -1, -1, ... So the second column of each row (apart from the second one) is always 6.",
				"T(n,k) is the LCM of A321478(n,k) and the multiplicative order of (k + sqrt(k^2 - 4))/2 modulo n, where the multiplicative order of u modulo z is the smallest positive integer l such that (u^l - 1)/z is an algebraic integer."
			],
			"formula": [
				"Let p be a prime \u003e= 5. (i) If ((k+2)/p) = 1, then T(p^e,k) is divisible by p^(e-1)*(p - ((k-2)/p))/2. Here (a/p) is the Legendre symbol (ii) If ((k+2)/p) = 1, then T(p^e,k) is divisible by p^(e-1)*(p + ((k-2)/p)), but not divisible by p^(e-1)*(p + ((k-2)/p))/2. (iii) If p divides k - 2, then T(p^e,k) = p^e. (iv) If p divides k + 2, then T(p^e,k) = 2*p^e.",
				"if p == 1 (mod 4), then T(p^e,k) is divisible by p^(e-1)*(p - 1), and T(p^e,k) is even; if p == 3 (mod 4), then T(p^e,k) is divisible by p^(e-1)*(p - 1) but not divisible by p^(e-1)*(p - 1)/2. Here (a/p) is the Legendre symbol. (ii) If ((k^2+4)/p) = -1, then T(p^e,k) is divisible by 2*p^(e-1)*(p + 1) but not divisible by p^(e-1)*(p + 1). (iii) If k^2 + 4 is divisible by p, then T(p^e,k) = 4*p^e.",
				"For e \u003e= 2 and k \u003e 1, T(2^e,k) = 3*2^(e-v(k^2-1,2)+1) for odd k and 2^(e-v(k,2)+1) for even k, where v(k,2) is the 2-adic valuation of k.",
				"For e \u003e 0 and k \u003e 1, T(3^e,k) = 4*3^(e-v(k,3)) for k divisible by 3, 2*3^(e-v(k-1,3)+1) for k == 1 (mod 3) and 3^(e-v(k+1,3)+1) for k == 2 (mod 3).",
				"If gcd(n_1,n_2) = 1, then T(n_1*n_2,k) = lcm(T(n_1,k mod n_1),T(n_2, k mod n_2)).",
				"If p is an odd prime such that ((k+2)/p) = -1, then T(p^e,k)/A321478(p^e,k) = 2.",
				"T(n,k) \u003c= 3*n."
			],
			"example": [
				"Table begins",
				"  1;",
				"  2,  3;",
				"  4,  6,  3;",
				"  4,  6,  4,  3;",
				"  4,  6,  5, 10,  3;",
				"  4,  6,  6, 12,  6,  3;",
				"  4,  6,  7,  8,  8, 14,  3;",
				"  4,  6,  8,  6,  4,  6,  8,  3;",
				"  4,  6,  9, 12, 18,  9, 12, 18,  3;",
				"  4,  6, 10, 30,  6, 12,  6, 15, 10,  3;",
				"  ..."
			],
			"program": [
				"(PARI) A316269(k, m) = ([k, -1; 1, 0]^m)[2, 1]",
				"T(n, k) = my(i=1); while(A316269(k, i)%n!=0||(A316269(k, i+1)-1)%n!=0, i++); i"
			],
			"xref": [
				"Cf. A316269, A321478 (ranks)."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Jianing Song_, Nov 11 2018",
			"references": 2,
			"revision": 9,
			"time": "2018-11-18T10:03:46-05:00",
			"created": "2018-11-18T10:03:46-05:00"
		}
	]
}