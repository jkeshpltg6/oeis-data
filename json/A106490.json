{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A106490",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 106490,
			"data": "0,1,1,2,1,2,1,2,2,2,1,3,1,2,2,3,1,3,1,3,2,2,1,3,2,2,2,3,1,3,1,2,2,2,2,4,1,2,2,3,1,3,1,3,3,2,1,4,2,3,2,3,1,3,2,3,2,2,1,4,1,2,3,3,2,3,1,3,2,3,1,4,1,2,3,3,2,3,1,4,3,2,1,4,2,2,2,3,1,4,2,3,2,2,2,3,1,3,3,4,1,3",
			"name": "Total number of bases and exponents in Quetian Superfactorization of n, excluding the unity-exponents at the tips of branches.",
			"comment": [
				"Quetian Superfactorization proceeds by factoring a natural number to its unique prime-exponent factorization (p1^e1 * p2^e2 * ... pj^ej) and then factoring recursively each of the (nonzero) exponents in similar manner, until unity-exponents are finally encountered."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A106490/b106490.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"A. Karttunen, \u003ca href=\"/A091247/a091247.scm.txt\"\u003eScheme-program for computing this sequence.\u003c/a\u003e",
				"\u003ca href=\"/index/Eu#epf\"\u003eIndex entries for sequences computed from exponents in factorization of n\u003c/a\u003e"
			],
			"formula": [
				"Additive with a(p^e) = 1 + a(e).",
				"a(1) = 0; for n \u003e 1, a(n) = 1 + a(A067029(n)) + a(A028234(n)). - _Antti Karttunen_, Mar 23 2017",
				"Other identities. For all n \u003e= 1:",
				"a(A276230(n)) = n.",
				"a(n) = A106493(A106444(n)).",
				"a(n) = A106491(n) - A064372(n)."
			],
			"example": [
				"a(64) = 3, as 64 = 2^6 = 2^(2^1*3^1) and there are three non-1 nodes in that superfactorization. Similarly, for 360 = 2^(3^1) * 3^(2^1) * 5^1 we get a(360) = 5. a(65536) = a(2^(2^(2^(2^1)))) = 4."
			],
			"maple": [
				"a:= proc(n) option remember; `if`(n=1, 0,",
				"      add(1+a(i[2]), i=ifactors(n)[2]))",
				"    end:",
				"seq(a(n), n=1..100);  # _Alois P. Heinz_, Nov 07 2014"
			],
			"mathematica": [
				"a[n_] := a[n] = If[n == 1, 0, Sum[1 + a[i[[2]]], {i,FactorInteger[n]}]]; Table[a[n], {n, 1, 100}] (* _Jean-François Alcover_, Nov 11 2015, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(Scheme, with memoization-macro definec)",
				"(definec (A106490 n) (if (= 1 n) 0 (+ 1 (A106490 (A067029 n)) (A106490 (A028234 n))))) ;; _Antti Karttunen_, Mar 23 2017",
				"(PARI)",
				"A067029(n) = if(n\u003c2, 0, factor(n)[1,2]);",
				"A028234(n) = my(f = factor(n)); if (#f~, f[1, 1] = 1); factorback(f); /* after _Michel Marcus_ */",
				"a(n) = if(n\u003c2, 0, 1 + a(A067029(n)) + a(A028234(n)));",
				"for(n=1, 150, print1(a(n),\", \")) \\\\ _Indranil Ghosh_, Mar 23 2017, after formula by _Antti Karttunen_"
			],
			"xref": [
				"Cf. A028234, A064372, A067029, A106444, A106491, A106492, A106493.",
				"Cf. A276230 (gives first k such that a(k) = n, i.e., this sequence is a left inverse of A276230).",
				"After n=1 differs from A038548 for the first time at n=24, where A038548(24)=4, while a(24)=3."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Antti Karttunen_, May 09 2005 based on _Leroy Quet_'s message ('Super-Factoring' An Integer) posted to SeqFan-mailing list on Dec 06 2003.",
			"references": 15,
			"revision": 44,
			"time": "2017-06-02T22:23:41-04:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}