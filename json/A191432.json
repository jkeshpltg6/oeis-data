{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A191432",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 191432,
			"data": "1,2,5,3,7,8,4,10,12,11,6,14,17,16,15,9,20,24,23,21,18,13,28,34,33,30,26,22,19,40,48,47,43,37,31,25,27,57,68,67,61,53,44,36,29,38,81,96,95,86,75,62,51,41,32,54,115,136,135,122,106,88,72,58,45,35,77,163,193,191,173,150,125,102,82,64,50,39",
			"name": "Dispersion of ([nx+1/x]), where x=sqrt(2) and [ ]=floor, by antidiagonals.",
			"comment": [
				"Background discussion:  Suppose that s is an increasing sequence of positive integers, that the complement t of s is infinite, and that t(1)=1.  The dispersion of s is the array D whose n-th row is (t(n), s(t(n)), s(s(t(n)), s(s(s(t(n)))), ...).  Every positive integer occurs exactly once in D, so that, as a sequence, D is a permutation of the positive integers.  The sequence u given by u(n)=(number of the row of D that contains n) is a fractal sequence.  Examples:",
				"(1) s=A000040 (the primes), D=A114537, u=A114538.",
				"(2) s=A022343 (without initial 0), D=A035513 (Wythoff array), u=A003603.",
				"(3) s=A007067, D=A035506 (Stolarsky array), u=A133299.",
				"More recent examples of dispersions: A191426-A191455."
			],
			"example": [
				"Northwest corner:",
				"1.....2....3....4....6....9",
				"5.....7....10...14...20...28",
				"8.....12...17...24...34...48",
				"11...16....23...33...47...67",
				"15...21....30...43...61...86"
			],
			"mathematica": [
				"(* Program generates the dispersion array T of increasing sequence f[n] *)",
				"r = 40; r1 = 12;  (* r=# rows of T, r1=# rows to show *)",
				"c = 40; c1 = 12;  (* c=# cols of T, c1=# cols to show *)",
				"x = Sqrt[2];",
				"f[n_] := Floor[n*x + 1/x] (* f(n) is complement of column 1 *)",
				"mex[list_] :=  NestWhile[#1 + 1 \u0026, 1, Union[list][[#1]] \u003c= #1 \u0026, 1,",
				"  Length[Union[list]]]",
				"rows = {NestList[f, 1, c]};",
				"Do[rows = Append[rows, NestList[f, mex[Flatten[rows]], r]], {r}];",
				"t[i_, j_] := rows[[i, j]];  TableForm[",
				"Table[t[i, j], {i, 1, 10}, {j, 1, 10}]] (* A191432 array *)",
				"Flatten[Table[t[k, n - k + 1], {n, 1, c1}, {k, 1, n}]] (* A191432 sequence *)",
				"(* Program by _Peter J. C. Moses_, Jun 01 2011 *)"
			],
			"xref": [
				"Cf. A114537, A035513, A035506."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Jun 03 2011",
			"references": 1,
			"revision": 10,
			"time": "2014-02-14T00:28:42-05:00",
			"created": "2011-06-05T06:29:31-04:00"
		}
	]
}