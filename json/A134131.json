{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A134131",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 134131,
			"data": "1,-1,0,1,-1,-1,2,-2,0,2,-2,-1,4,-4,0,5,-4,-2,8,-7,-1,9,-8,-3,14,-13,-2,16,-14,-5,24,-21,-3,27,-24,-8,39,-35,-6,45,-39,-13,62,-55,-10,71,-62,-19,96,-85,-16,111,-96,-29,146,-128,-25,168,-146,-42,218",
			"name": "Expansion of chi(-x) * chi(-x^9) / chi(-x^3)^2 in power of x where chi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A134131/b134131.txt\"\u003eTable of n, a(n) for n = 0..1500\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(1/6) * eta(q) * eta(q^6)^2 * eta(q^9) / (eta(q^2) * eta(q^3)^2 * eta(q^18)) in powers of q.",
				"Euler transform of period 18 sequence [ -1, 0, 1, 0, -1, 0, -1, 0, 0, 0, -1, 0, -1, 0, 1, 0, -1, 0, ...].",
				"Given g.f. A(x) then B(q) = A(q^6) / q satisfies 0 = f(B(q), B(q^2), B(q^4) ) where f(u, v, w) = (u^2 + v) * v + (u^2 - v) * w^2.",
				"Given g.f. A(x) then B(q) = A(q^3)^2 / q satisfies 0 = f(B(q), B(q^2)) where f(u, v) = (u - v^2) * (u^2 - v) * (1 + u * v) - (2 * u * v)^2.",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (648 t)) = g(t) where q = exp(2 Pi i t) and g() is the g.f. for A134132.",
				"G.f.: ( Product_{k\u003e0} (1 + x^k) * (1 + x^(9*k)) / (1 + x^(3*k))^2 )^(-1).",
				"a(n) = A112178(3*n). Convolution inverse of A134132."
			],
			"example": [
				"G.f. = 1 - x + x^3 - x^4 - x^5 + 2*x^6 - 2*x^7 + 2*x^9 - 2*x^10 - x^11 + 4*x^12 + ...",
				"G.f. = 1/q - q^5 + q^17 - q^23 - q^29 + 2*q^35 - 2*q^41 + 2*q^53 - 2*q^59 - q^65 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x, x^2] QPochhammer[ -x^3, x^3]^2 QPochhammer[ x^9, x^18] , {x, 0, n}]; (* _Michael Somos_, Oct 27 2015 *)",
				"eta[q_] := q^(1/24)*QPochhammer[q]; b := q^(1/6)*eta[q]*eta[q^6]^2* eta[q^9]/(eta[q^2]*eta[q^3]^2*eta[q^18]); a := CoefficientList[ Series[ b, {q, 0, 80}], q]; Table[a[[n]], {n, 1, 80}] (* _G. C. Greubel_, Jul 03 2018 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A) * eta(x^6 + A)^2 * eta(x^9 + A) / (eta(x^2 + A) * eta(x^3 + A)^2 * eta(x^18 + A)), n))};"
			],
			"xref": [
				"Cf. A112178, A134132."
			],
			"keyword": "sign",
			"offset": "0,7",
			"author": "_Michael Somos_, Oct 10 2007",
			"references": 2,
			"revision": 13,
			"time": "2021-03-12T22:24:45-05:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}