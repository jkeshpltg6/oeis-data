{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A101479",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 101479,
			"data": "1,1,1,1,1,1,3,2,1,1,19,9,3,1,1,191,70,18,4,1,1,2646,795,170,30,5,1,1,46737,11961,2220,335,45,6,1,1,1003150,224504,37149,4984,581,63,7,1,1,25330125,5051866,758814,92652,9730,924,84,8,1,1,735180292,132523155,18301950,2065146,199692,17226,1380,108,9,1,1",
			"name": "Triangular matrix T, read by rows, where row n equals row (n-1) of T^(n-1) after appending '1' for the main diagonal.",
			"comment": [
				"Remarkably, T equals the product of these triangular matrices: T = A107867*A107862^-1 = A107870*A107867^-1 = A107873*A107870^-1; reversing the order of these products yields triangle A107876."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A101479/b101479.txt\"\u003eRows n = 0..140, flattened\u003c/a\u003e (first 31 rows from Paul D. Hanna)"
			],
			"example": [
				"Triangle begins:",
				"1;",
				"1, 1;",
				"1, 1, 1;",
				"3, 2, 1, 1;",
				"19, 9, 3, 1, 1;",
				"191, 70, 18, 4, 1, 1;",
				"2646, 795, 170, 30, 5, 1, 1;",
				"46737, 11961, 2220, 335, 45, 6, 1, 1;",
				"1003150, 224504, 37149, 4984, 581, 63, 7, 1, 1;",
				"25330125, 5051866, 758814, 92652, 9730, 924, 84, 8, 1, 1;",
				"735180292, 132523155, 18301950, 2065146, 199692, 17226, 1380, 108, 9, 1, 1; ...",
				"Row 4 starts with row 3 of T^3 which begins:",
				"1;",
				"3, 1;",
				"6, 3, 1;",
				"19, 9, 3, 1; ...",
				"row 5 starts with row 4 of T^4 which begins:",
				"1;",
				"4, 1;",
				"10, 4, 1;",
				"34, 14, 4, 1;",
				"191, 70, 18, 4, 1; ...",
				"An ALTERNATE GENERATING METHOD is illustrated as follows.",
				"For row 4:",
				"Start with a '1' and append 2 zeros,",
				"take partial sums and append 1 zero,",
				"take partial sums thrice more, resulting in:",
				"1, 0, 0;",
				"1, 1, 1, 0;",
				"1, 2, 3, 3;",
				"1, 3, 6, 9;",
				"1, 4,10,19.",
				"Final nonzero terms form row 4: [19,9,3,1,1].",
				"For row 5:",
				"Start with a '1' and append 3 zeros,",
				"take partial sums and append 2 zeros,",
				"take partial sums and append 1 zero,",
				"take partial sums thrice more, resulting in:",
				"1, 0, 0, 0;",
				"1, 1, 1, 1, 0,  0;",
				"1, 2, 3, 4, 4,  4,  0;",
				"1, 3, 6,10,14, 18, 18;",
				"1, 4,10,20,34, 52, 70;",
				"1, 5,15,35,69,121,191;",
				"where the final nonzero terms form row 5: [191,70,18,4,1,1].",
				"Likewise, for row 6:",
				"1, 0, 0, 0,  0;",
				"1, 1, 1, 1,  1,  0,  0,  0;",
				"1, 2, 3, 4,  5,  5,  5,  5,   0,   0;",
				"1, 3, 6,10, 15, 20, 25, 30,  30,  30,   0;",
				"1, 4,10,20, 35, 55, 80,110, 140, 170, 170;",
				"1, 5,15,35, 70,125,205,315, 455, 625, 795;",
				"1, 6,21,56,126,251,456,771,1226,1851,2646;",
				"where the final nonzero terms form row 6: [2646,795,170,30,5,1,1].",
				"Continuing in this way generates all rows of this triangle."
			],
			"maple": [
				"b:= proc(n) option remember;",
				"      Matrix(n, (i,j)-\u003e T(i-1,j-1))^(n-1)",
				"    end:",
				"T:= proc(n,k) option remember;",
				"     `if`(n=k, 1, `if`(k\u003en, 0, b(n)[n,k+1]))",
				"    end:",
				"seq(seq(T(n,k), k=0..n), n=0..10);  # _Alois P. Heinz_, Apr 13 2020"
			],
			"mathematica": [
				"b[n_] := b[n] = MatrixPower[Table[T[i-1, j-1], {i, n}, {j, n}], n-1];",
				"T[n_, k_] := T[n, k] = If[n == k, 1, If[k \u003e n, 0, b[n][[n, k+1]]]];",
				"Table[Table[T[n, k], {k, 0, n}], {n, 0, 10}] // Flatten (* _Jean-François Alcover_, Apr 25 2020, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(PARI) {T(n,k) = my(A=Mat(1),B); for(m=1,n+1, B=matrix(m,m); for(i=1,m, for(j=1,i, if(j==i,B[i,j]=1, B[i,j] = (A^(i-2))[i-1,j]);)); A=B); return(A[n+1,k+1])}",
				"for(n=0,10, for(k=0,n, print1(T(n,k),\", \")); print(\"\"))",
				"(PARI) {T(n,k) = my(A=vector(n+1),p); A[1]=1; for(j=1,n-k-1, p=(n-1)*(n-2)/2-(n-j-1)*(n-j-2)/2; A = Vec((Polrev(A)+x*O(x^p))/(1-x))); A = Vec((Polrev(A) +x*O(x^p)) / (1-x) ); A[p+1]}",
				"for(n=0,10, for(k=0,n, print1(T(n,k),\", \")); print(\"\"))"
			],
			"xref": [
				"Columns are A101481, A101482, A101483, row sums form A101484.",
				"Cf. A107876 (dual triangle).",
				"Cf. A304184, A304185, A304186, A304187."
			],
			"keyword": "nonn,tabl",
			"offset": "0,7",
			"author": "_Paul D. Hanna_, Jan 21 2005, Jul 26 2006, May 27 2007",
			"references": 25,
			"revision": 26,
			"time": "2020-04-25T08:44:08-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}