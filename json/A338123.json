{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A338123",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 338123,
			"data": "3,4,15,19,33,31,63,55,78,82,120,67,162,154,189,175,261,217,327,259,360,370,456,283,534,514,579,523,705,619,807,703,858,874,1008,691,1122,1090,1185,1111,1365,1237,1503,1339,1572,1594,1776,1339,1926,1882,2007,1891",
			"name": "Place three points evenly spaced around a circle, draw n evenly spaced rays from each of the points, a(n) is the number of vertices thus created. See Comments for details.",
			"comment": [
				"The rays are evenly spaced around each point. The first ray from each point goes opposite to the direction to the center of the circle. Should a ray hit another point it is terminated there.",
				"See A338122 for illustrations."
			],
			"link": [
				"Lars Blomberg, \u003ca href=\"/A338123/b338123.txt\"\u003eTable of n, a(n) for n = 1..800\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 2160-a(n-4)+a(n-12)+a(n-16)+a(n-60)+a(n-64)-a(n-72)-a(n-76), n\u003e78. (conjectured)",
				"From _Lars Blomberg_, Oct 25 2020: (Start)",
				"Conjectured for 3 \u003c= n \u003c= 800.",
				"Select the row in the table below for which r = n mod m. Then a(n)=(a*n^2 + b*n + c)/d.",
				"+===========================================+",
				"|               r |   m | a |   b |   c | d |",
				"+-------------------------------------------+",
				"|               5 |   6 | 3 |  10 |   7 | 4 |",
				"|               1 |  12 | 3 |  10 |  11 | 4 |",
				"|           2, 10 |  12 | 3 |     |  28 | 4 |",
				"|               3 |  12 | 3 |   4 |  21 | 4 |",
				"|               6 |  12 | 3 | -10 |  76 | 4 |",
				"|               7 |  12 | 3 |  10 |  35 | 4 |",
				"|               9 |  12 | 3 |   4 |  33 | 4 |",
				"|           4, 20 |  24 | 3 | -12 |  76 | 4 |",
				"|           8, 16 |  24 | 3 | -12 | 124 | 4 |",
				"|               0 | 120 | 3 | -40 | -20 | 4 |",
				"| 12, 36, 84, 108 | 120 | 3 | -40 | 316 | 4 |",
				"|  24, 48, 72, 96 | 120 | 3 | -40 | 364 | 4 |",
				"|              60 | 120 | 3 | -40 | -68 | 4 |",
				"+===========================================+ (End)"
			],
			"example": [
				"For n=1 there are three rays that do not intersect, so a(1)=3."
			],
			"program": [
				"(PARI)",
				"a(n)=if( \\",
				"n%6==5,(3*n^2 + 10*n + 7)/4, \\",
				"n%12==1,(3*n^2 + 10*n + 11)/4, \\",
				"n%12==2||n%12==10,(3*n^2 + 28)/4, \\",
				"n%12==3,(3*n^2 + 4*n + 21)/4, \\",
				"n%12==6,(3*n^2 - 10*n + 76)/4, \\",
				"n%12==7,(3*n^2 + 10*n + 35)/4, \\",
				"n%12==9,(3*n^2 + 4*n + 33)/4, \\",
				"n%24==4||n%24==20,(3*n^2 - 12*n + 76)/4, \\",
				"n%24==8||n%24==16,(3*n^2 - 12*n + 124)/4, \\",
				"n%120==0,(3*n^2 - 40*n - 20)/4, \\",
				"n%120==12||n%120==36||n%120==84||n%120==108,(3*n^2 - 40*n + 316)/4, \\",
				"n%120==24||n%120==48||n%120==72||n%120==96,(3*n^2 - 40*n + 364)/4, \\",
				"n%120==60,(3*n^2 - 40*n - 68)/4, \\",
				"-1);",
				"vector(798, n, a(n+2))"
			],
			"xref": [
				"Cf. A338042 (two start points), A338122 (regions), A338124 (edges)."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Lars Blomberg_, Oct 11 2020",
			"references": 4,
			"revision": 12,
			"time": "2020-10-25T14:17:03-04:00",
			"created": "2020-10-11T14:29:13-04:00"
		}
	]
}