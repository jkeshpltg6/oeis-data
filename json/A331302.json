{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A331302",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 331302,
			"data": "0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1,0,0,0,0,2,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1,1,0,0,0,0,1,0,0,1,0,0,0,0,0,0,2,0,0,0,0,1,0,2,1,0,0,0,0,0,0,1,0,1,0,0,0,2,0,0,0,2,0,0,0,1,0,0,1,0,0,1",
			"name": "Number of 4k+3 composites encountered when traversing from n to the root of A005940-tree.",
			"comment": [
				"For numbers \u003e 1, iterate the map x -\u003e A252463(x) which divides even numbers by 2 and for odd numbers shifts every prime in the prime factorization one index step towards smaller primes with A064989. a(n) counts the composite numbers of the form 4k+3 (A091236) encountered until 1 has been reached, including in the count also n itself if it is of the same form."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A331302/b331302.txt\"\u003eTable of n, a(n) for n = 1..65537\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#prime_indices\"\u003eIndex entries for sequences computed from indices in prime factorization\u003c/a\u003e"
			],
			"formula": [
				"a(1) = 0, a(p) = 0 for all primes, otherwise a(n) = [n == 3 (mod 4)] + a(A252463(n)).",
				"a(2^k * p) [with k\u003e=0, p prime] = a(n^2) = a(2*(n^2)) = 0, with zeros occurring also on some other positions.",
				"a(n) \u003c= A292377(n)."
			],
			"example": [
				"Here -\u003e stands for transition x -\u003e A252463(x):",
				"For n = 35, 35 mod 4 = 3, 35 -\u003e 15 and 15 mod 4 = 3 also, but then 15 -\u003e 6 (with 6 mod 4 = 2), and 6 -\u003e 3, a prime, after which only noncomposites occur in the trajectory -\u003e 2 -\u003e 1, thus a(35) = 2 as there were exactly two 4k+3 composites on the whole path."
			],
			"mathematica": [
				"Array[Count[FixedPointList[Which[# == 1, 1, EvenQ@ #, #/2, True, Times @@ Power[Which[# == 1, 1, # == 2, 1, True, NextPrime[#, -1]] \u0026 /@ First@ #, Last@ #] \u0026@ Transpose@ FactorInteger@ #] \u0026, #], _?(And[CompositeQ@ #, Mod[#, 4] == 3] \u0026)] \u0026, 105] (* _Michael De Vlieger_, Feb 08 2020 *)"
			],
			"program": [
				"(PARI)",
				"A064989(n) = {my(f); f = factor(n); if((n\u003e1 \u0026\u0026 f[1,1]==2), f[1,2] = 0); for (i=1, #f~, f[i,1] = precprime(f[i,1]-1)); factorback(f)};",
				"A252463(n) = if(!(n%2),n/2,A064989(n));",
				"A331302(n) = if((1==n)||isprime(n),0,(3==(n%4))+A331302(A252463(n)));"
			],
			"xref": [
				"Cf. A005940, A064989, A091236, A252463, A292377.",
				"Subsequences of the indices of zeros: A093641, A028982 (see A292583 for the explanation of the latter)."
			],
			"keyword": "nonn",
			"offset": "1,35",
			"author": "_Antti Karttunen_, Feb 07 2020",
			"references": 1,
			"revision": 25,
			"time": "2020-02-08T20:44:32-05:00",
			"created": "2020-02-08T20:44:32-05:00"
		}
	]
}