{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A022598",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 22598,
			"data": "1,-3,3,-4,9,-12,15,-21,30,-43,54,-69,94,-123,153,-193,252,-318,391,-486,609,-754,918,-1119,1376,-1680,2019,-2432,2946,-3540,4220,-5034,6015,-7157,8463,-9999,11835,-13956,16374,-19206",
			"name": "Expansion of Product_{m\u003e=1} (1+q^m)^(-3).",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"reference": [
				"T. J. I'a. Bromwich, Introduction to the Theory of Infinite Series, Macmillan, 2nd. ed. 1949, p. 116, q_2^3."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A022598/b022598.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Vaclav Kotesovec, \u003ca href=\"http://arxiv.org/abs/1509.08708\"\u003eA method of finding the asymptotics of q-series based on the convolution of generating functions\u003c/a\u003e, arXiv:1509.08708 [math.CO], Sep 30 2015, p. 13.",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of chi(-x)^3 = phi(-x) / psi(x) in powers of x where phi(), psi(), chi() are Ramanujan theta functions. - _Michael Somos_, Aug 09 2015",
				"Expansion of q^(1/8) * (eta(q) / eta(q^2))^3 in powers of q. - _Michael Somos_, Apr 24 2015",
				"Euler transform of period 2 sequence [ -3, 0, ...]. - _Michael Somos_, Aug 09 2015",
				"Convolution cube of A081362. - _Michael Somos_, Apr 24 2015",
				"Convolution inverse of A022568. - _Michael Somos_, Aug 09 2015",
				"a(n) ~ (-1)^n * exp(Pi*sqrt(n/2)) / (2^(7/4) * n^(3/4)). - _Vaclav Kotesovec_, Aug 27 2015",
				"a(0) = 1, a(n) = -(3/n)*Sum_{k=1..n} A000593(k)*a(n-k) for n \u003e 0. - _Seiichi Manyama_, Apr 03 2017",
				"G.f.: exp(-3*Sum_{k\u003e=1} (-1)^(k+1)*x^k/(k*(1 - x^k))). - _Ilya Gutkovskiy_, Feb 06 2018"
			],
			"example": [
				"G.f. = 1 - 3*x + 3*x^2 - 4*x^3 + 9*x^4 - 12*x^5 + 15*x^6 - 21*x^7 + 30*x^8 + ...",
				"G.f. = 1/q - 3*q^7 + 3*q^15 - 4*q^23 + 9*q^31 - 12*q^39 + 15*q^47 - 21*q^55 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (QPochhammer[ x] / QPochhammer[x^2])^3, {x, 0, n}]; (* _Michael Somos_, Feb 22 2015 *)",
				"nmax = 50; CoefficientList[Series[Product[1/(1 + x^k)^3, {k, 1, nmax}], {x, 0, nmax}], x] (* _Vaclav Kotesovec_, Aug 27 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( (eta(x + A) / eta(x^2 + A))^3, n))};"
			],
			"xref": [
				"Cf. A022568, A081362.",
				"Column k=3 of A286352."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 5,
			"revision": 33,
			"time": "2021-03-12T22:24:41-05:00",
			"created": "1998-06-14T03:00:00-04:00"
		}
	]
}