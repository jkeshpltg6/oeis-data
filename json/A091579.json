{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A091579",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 91579,
			"data": "1,3,1,9,4,24,1,3,1,9,4,67,1,3,1,9,4,24,1,3,1,9,4,196,3,1,9,4,24,1,3,1,9,4,68,3,1,9,4,24,1,3,1,9,4,581,3,1,9,4,25,3,1,9,4,67,1,3,1,9,4,24,1,3,1,9,4,196,3,1,9,4,24,1,3,1,9,4,68,3,1,9,4,24,1,3,1,9,4,1731,3,1,9,4,24",
			"name": "Lengths of suffix blocks associated with A090822.",
			"comment": [
				"The suffix blocks are what is called \"glue string\" in the paper by Gijswijt et al (2007). Roughly speaking, these are the terms \u003e= 2 appended before the sequence (A090822) goes on with a(n+1) = 1 followed by all other initial terms a(2..n), cf. Example. The concatenation of these glue strings yields A091787. - _M. F. Hasler_, Aug 08 2018"
			],
			"link": [
				"Dion Gijswijt, \u003ca href=\"/A091579/b091579.txt\"\u003eTable of n, a(n) for n = 1..2000\u003c/a\u003e",
				"F. J. van de Bult, D. C. Gijswijt, J. P. Linderman, N. J. A. Sloane and Allan Wilks, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL10/Sloane/sloane55.html\"\u003eA Slow-Growing Sequence Defined by an Unusual Recurrence\u003c/a\u003e, J. Integer Sequences, Vol. 10 (2007), #07.1.2.",
				"\u003ca href=\"/index/Ge#Gijswijt\"\u003eIndex entries for sequences related to Gijswijt's sequence\u003c/a\u003e"
			],
			"example": [
				"From _M. F. Hasler_, Aug 09 2018:",
				"In sequence A090822, after the initial (1, 1) follows the first suffix block or glue string (2) of length a(1) = 1. This is followed by A090822(4) = 1 which indicates that the suffix block has ended, and the whole sequence A090822(1..3) up to and including this suffix block is repeated: A090822(4..6) = A090822(1..3).",
				"Then A090822 goes on with (2, 2, 3, 1, ...), which tells that the second suffix block is A090822(7..9) = (2, 2, 3) of length a(2) = 3, whereafter the sequence starts over again: A090822(10..18) = A090822(1..9). (End)"
			],
			"program": [
				"(Python)",
				"# compute curling number of L",
				"def curl(L):",
				"    n = len(L)",
				"    m = 1 #max nr. of repetitions at the end",
				"    k = 1 #length of repeating block",
				"    while(k*(m+1) \u003c= n):",
				"        good = True",
				"        i = 1",
				"        while(i \u003c= k and good):",
				"            for t in range(1, m+1):",
				"                if L[-i-t*k] != L[-i]:",
				"                    good = False",
				"            i = i+1",
				"        if good:",
				"            m = m+1",
				"        else:",
				"            k = k+1",
				"    return m",
				"# compute lengths of first n glue strings",
				"def A091579_list(n):",
				"    Promote = [1] #Keep track of promoted elements",
				"    L = [2]",
				"    while len(Promote) \u003c= n:",
				"        c = curl(L)",
				"        if c \u003c 2:",
				"            Promote = Promote+[len(L)+1]",
				"            c = 2",
				"        L = L+[c]",
				"    return [Promote[i+1]-Promote[i] for i in range(n)]",
				"# _Dion Gijswijt_, Oct 08 2015"
			],
			"xref": [
				"Cf. A090822, A091587 (records). For a smoothed version see A091839.",
				"Cf. A091787 for the concatenation of the glue strings."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Mar 05 2004",
			"references": 14,
			"revision": 34,
			"time": "2018-10-05T11:13:26-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}