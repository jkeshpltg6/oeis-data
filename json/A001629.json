{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001629",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1629,
			"id": "M1377 N0537",
			"data": "0,0,1,2,5,10,20,38,71,130,235,420,744,1308,2285,3970,6865,11822,20284,34690,59155,100610,170711,289032,488400,823800,1387225,2332418,3916061,6566290,10996580,18394910,30737759,51310978,85573315,142587180,237387960,394905492",
			"name": "Self-convolution of Fibonacci numbers.",
			"comment": [
				"Number of elements in all subsets of {1,2,...,n-1} with no consecutive integers. Example: a(5)=10 because the subsets of {1,2,3,4} that have no consecutive elements, i.e., {},{1},{2},{3},{4},{1,3},{1,4},{2,4}, the total number of elements is 10. - _Emeric Deutsch_, Dec 10 2003",
				"If g is either of the real solutions to x^2-x-1=0, g'=1-g is the other one and phi is any 2 X 2-matricial solution to the same equation, not of the form gI or g'I, then Sum'_{i+j=n-1} g^i phi^j = F_n + (A001629(n) - A001629(n-1)g')*(phi-g'I), where i,j \u003e= 0, F_n is the n-th Fibonacci number and I is the 2 X 2 identity matrix... - Michele Dondi (blazar(AT)lcm.mi.infn.it), Apr 06 2004",
				"Number of 3412-avoiding involutions containing exactly one subsequence of type 321.",
				"Number of binary sequences of length n with exactly one pair of consecutive 1's. - George J. Schaeffer (gschaeff(AT)andrew.cmu.edu), Sep 02 2004",
				"For this sequence the n-th term is given by (nF(n+1)-F(n)+nF(n-1))/5 where F(n) is the n-th Fibonacci number. - Mrs. J. P. Shiwalkar and M. N. Deshpande (dpratap_ngp(AT)sancharnet.in), Apr 20 2005",
				"If an unbiased coin is tossed n times then there are 2^n possible strings of H and T. Out of these, number of strings with exactly one 'HH' is given by a(n) where a(n) denotes n-th term of this sequence. - Mrs. J. P. Shiwalkar and M. N. Deshpande (dpratap_ngp(AT)sancharnet.in), May 04 2005",
				"a(n) is half the number of horizontal dominoes in all domino tilings of a horizontally aligned 2 X n rectangle; a(n+1) = the number of vertical dominoes in all domino tilings of a horizontally aligned 2 X n rectangle; thus 2*a(n)+a(n+1)=n*F(n+1) = the number of dominoes in all domino tilings of a 2 X n rectangle, where F=A000045, the Fibonacci sequence. - _Roberto Tauraso_, May 02 2005; _Graeme McRae_, Jun 02 2006",
				"a(n+1) = ((-i)^(n-1))*(d/dx)S(n,x)|_{x=i}, where i is the imaginary unit, n \u003e= 1. First derivative of Chebyshev S-polynomials evaluated at x=i multiplied by (-i)^(n-1). See A049310 for the S-polynomials. - _Wolfdieter Lang_, Apr 04 2007",
				"For n \u003e= 4, a(n) is the number of weak compositions of n-2 in which exactly one part is 0 and all other parts are either 1 or 2. - _Milan Janjic_, Jun 28 2010",
				"For n greater than 1, a(n) equals the absolute value of (1 - (1/2 - i/2)*(1 + (-1)^(n + 1))) times the x-coefficient of the characteristic polynomial of the (n-1) X (n-1) tridiagonal matrix with i's along the main diagonal (i is the imaginary unit), 1's along the superdiagonal and the subdiagonal and 0's everywhere else (see Mathematica code below). - _John M. Campbell_, Jun 23 2011",
				"a(n) = A214178(n,1) for n \u003e 0. - _Reinhard Zumkeller_, Jul 08 2012",
				"For n \u003e 0: a(n) = Sum_{k=1..n-1} (A039913(n-1,k)) / 2. - _Reinhard Zumkeller_, Oct 07 2012",
				"The right-hand side of a binomial-coefficient identity [Gauthier]. - _N. J. A. Sloane_, Apr 09 2013",
				"a(n) is the number of edges in the Fibonacci cube Gamma(n-1) (see the Klavzar 2005 reference, p. 149). Example: a(3)=2; indeed, the Fibonacci cube Gamma(2) is the path P(3) having 2 edges. - _Emeric Deutsch_, Aug 10 2014",
				"a(n) is the number of c(i)'s, including repetitions, in p(n), where p(n)/q(n) is the n-th convergent p(n)/q(n) of the formal infinite continued fraction [c(0),c(1),...]; e.g., the number of c(i)'s in p(3) = c(0)c(1)c(2)c(3) + c(0)c(1) + c(0)c(3) + c(2)c(3) + 1 is a(5) = 10. - _Clark Kimberling_, Dec 23 2015",
				"Also the number of maximal and maximum cliques in the (n-1)-Fibonacci cube graph. - _Eric W. Weisstein_, Sep 07 2017",
				"a(n+1) is the total number of fixed points in all permutations p on 1,2,...,n such that |k-p(k)| \u003c= 1 for 1 \u003c= k \u003c= n. - _Katharine Ahrens_, Sep 03 2019",
				"From _Steven Finch_, Mar 22 2020: (Start)",
				"a(n+1) is the total binary weight (cf. A000120) of all A000045(n+2) binary sequences of length n not containing any adjacent 1's.",
				"The only three 2-bitstrings without adjacent 1's are 00, 01 and 10. The bitsums of these are 0, 1 and 1. Adding these give a(3)=2.",
				"The only five 3-bitstrings without adjacent 1's are 000, 001, 010, 100 and 101. The bitsums of these are 0, 1, 1, 1 and 2. Adding these give a(4)=5.",
				"The only eight 4-bitstrings without adjacent 1's are 0000, 0001, 0010, 0100, 1000, 0101, 1010 and 1001. The bitsums of these are 0, 1, 1, 1, 1, 2, 2, and 2. Adding these give a(5)=10. (End)"
			],
			"reference": [
				"Donald E. Knuth, Fundamental Algorithms, Addison-Wesley, 1968, p. 83, Eq. 1.2.8--(17). - _Don Knuth_, Feb 26 2019",
				"Thomas Koshy, Fibonacci and Lucas Numbers with Applications, Chapter 15, page 187, \"Hosoya's Triangle\", and p. 375, eq. (32.13).",
				"J. Riordan, Combinatorial Identities, Wiley, 1968, p. 101.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"S. Vajda, Fibonacci and Lucas numbers and the Golden Section, Ellis Horwood Ltd., Chichester, 1989, p. 183, Nr.(98)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A001629/b001629.txt\"\u003eTable of n, a(n) for n = 0..500\u003c/a\u003e",
				"Marco Abrate, Stefano Barbero, Umberto Cerruti and Nadir Murru, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2014.06.026\"\u003eColored compositions, Invert operator and elegant compositions with the \"black tie\"\u003c/a\u003e, Discrete Math. 335 (2014), 1--7. MR3248794.",
				"Katharine A. Ahrens, \u003ca href=\"https://www.lib.ncsu.edu/resolver/1840.20/37364\"\u003eCombinatorial Applications of the k-Fibonacci Numbers: A Cryptographically Motivated Analysis\u003c/a\u003e, Ph. D. thesis, North Carolina State University (2020).",
				"Carlos Alirio Rico Acevedo and Ana Paula Chaves, \u003ca href=\"https://arxiv.org/abs/1903.07490\"\u003eDouble-Recurrence Fibonacci Numbers and Generalizations\u003c/a\u003e, arXiv:1903.07490 [math.NT], 2019.",
				"Ali Reza Ashrafi, Jernej Azarija, Khadijeh Fathalikhani, Sandi Klavžar and Marko Petkovšek, \u003ca href=\"http://arxiv.org/abs/1407.4962\"\u003eVertex and edge orbits of Fibonacci and Lucas cubes\u003c/a\u003e, arXiv:1407.4962 [math.CO], 2014. See Table 2.",
				"A. R. Ashrafi, J. Azarija, K. Fathalikhani, S. Klavzar, et al., \u003ca href=\"http://www.fmf.uni-lj.si/~klavzar/preprints/Fib-Luc-orbits-August-11-2014.pdf\"\u003eOrbits of Fibonacci and Lucas cubes, dihedral transformations, and asymmetric strings\u003c/a\u003e, 2014.",
				"Jean-Luc Baril, Sergey Kirgizov and Vincent Vajnovszki, \u003ca href=\"https://arxiv.org/abs/2010.09505\"\u003eGray codes for Fibonacci q-decreasing words\u003c/a\u003e, arXiv:2010.09505 [cs.DM], 2020.",
				"D. Birmajer, J. Gil and M. Weiner, \u003ca href=\"http://arxiv.org/abs/1405.7727\"\u003eLinear recurrence sequences and their convolutions via Bell polynomials\u003c/a\u003e, arXiv:1405.7727 [math.CO], 2014.",
				"D. Birmajer, J. B. Gil and M. D. Weiner, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL18/Gil/gil3.html\"\u003eLinear Recurrence Sequences and Their Convolutions via Bell Polynomials\u003c/a\u003e, Journal of Integer Sequences, 18 (2015), #15.1.2.",
				"Matthew Blair, Rigoberto Flórez and Antara Mukherjee, \u003ca href=\"https://arxiv.org/abs/1808.05278\"\u003eMatrices in the Hosoya triangle\u003c/a\u003e, arXiv:1808.05278 [math.CO], 2018.",
				"P. J. Cameron, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL3/groups.html\"\u003eSequences realized by oligomorphic permutation groups\u003c/a\u003e, J. Integ. Seqs. Vol. 3 (2000), #00.1.5.",
				"G. Castiglione, A. Restivo and M. Sciortino, \u003ca href=\"https://doi.org/10.1016/j.tcs.2009.07.018\"\u003eCircular Sturmian words and Hopcroft’s algorithm\u003c/a\u003e,  Theor. Comput. Sci. 410, No. 43, 4372-4381 (2009)",
				"É. Czabarka, R. Flórez and L. Junes, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL18/Florez/florez12.html\"\u003eA Discrete Convolution on the Generalized Hosoya Triangle\u003c/a\u003e, Journal of Integer Sequences, 18 (2015), #15.1.6.",
				"E. S. Egge, \u003ca href=\"https://arxiv.org/abs/math/0307050\"\u003eRestricted 3412-Avoiding Involutions\u003c/a\u003e, p. 16, arXiv:math/0307050 [math.CO], 2003.",
				"Sergio Falcon, \u003ca href=\"https://doi.org/10.7546/nntdm.2020.26.3.96-106\"\u003eHalf self-convolution of the k-Fibonacci sequence\u003c/a\u003e, Notes on Number Theory and Discrete Mathematics (2020) Vol. 26, No. 3, 96-106.",
				"Luca Ferrari and Emanuele Munarini, \u003ca href=\"http://arxiv.org/abs/1203.6792\"\u003eEnumeration of edges in some lattices of paths\u003c/a\u003e, arXiv:1203.6792 [math.CO], 2012.",
				"Steven Finch, \u003ca href=\"https://arxiv.org/abs/2003.09458\"\u003eCantor-solus and Cantor-multus distributions\u003c/a\u003e, arXiv:2003.09458 [math.CO], 2020.",
				"Rigoberto Flórez, Robinson Higuita and Alexander Ramírez, \u003ca href=\"https://arxiv.org/abs/1808.01264\"\u003eThe resultant, the discriminant, and the derivative of generalized Fibonacci polynomials\u003c/a\u003e, arXiv:1808.01264 [math.NT], 2018.",
				"N. Gauthier (Proposer), \u003ca href=\"http://www.fq.math.ca/Problems/November2012advanced.pdf\"\u003eProblem H-703\u003c/a\u003e, Fib. Quart., 50 (2012), 379-381.",
				"Martin Griffiths, \u003ca href=\"http://www.fq.math.ca/Papers1/48-2/Griffiths.pdf\"\u003eDigit Proportions in Zeckendorf Representations\u003c/a\u003e, Fibonacci Quart. 48 (2010), no. 2, 168-174.",
				"V. E. Hoggatt, Jr. and M. Bicknell-Johnson, \u003ca href=\"http://www.fq.math.ca/Scanned/15-2/hoggatt1.pdf\"\u003eFibonacci convolution sequences\u003c/a\u003e, Fib. Quart., 15 (1977), 117-122.",
				"M. Janjic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL13/Janjic/janjic33.html\"\u003eHessenberg Matrices and Integer Sequences \u003c/a\u003e, J. Int. Seq. 13 (2010) # 10.7.8, section 3.",
				"S. Klavzar, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2004.02.023\"\u003eOn median nature and enumerative properties of Fibonacci-like cubes\u003c/a\u003e, Disc. Math. 299 (2005), 145-153.",
				"S. Klavzar, \u003ca href=\"http://www.imfm.si/preprinti/PDF/01150.pdf\"\u003eStructure of Fibonacci cubes: a survey\u003c/a\u003e, Institute of Mathematics, Physics and Mechanics Jadranska 19, 1000 Ljubljana, Slovenia; Preprint series Vol. 49 (2011), 1150 ISSN 2232-2094. (See Section 4.)",
				"T. Mansour, \u003ca href=\"https://arxiv.org/abs/math/0301157\"\u003eGeneralization of some identities involving the Fibonacci numbers\u003c/a\u003e, arXiv:math/0301157 [math.CO], 2003.",
				"P. Moree, \u003ca href=\"https://arxiv.org/abs/math/0311205\"\u003eConvoluted convolved Fibonacci numbers\u003c/a\u003e, arXiv:math/0311205 [math.CO], 2003.",
				"Pieter Moree, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL7/Moree/moree12.htm\"\u003eConvoluted Convolved Fibonacci Numbers\u003c/a\u003e, Journal of Integer Sequences, Vol. 7 (2004), Article 04.2.2.",
				"Valentin Ovsienko, \u003ca href=\"https://arxiv.org/abs/2111.02553\"\u003eShadow sequences of integers, from Fibonacci to Markov and back\u003c/a\u003e, arXiv:2111.02553 [math.CO], 2021.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992; arXiv:0911.4975 [math.NT], 2009.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"Jeffrey B. Remmel and J. L. B. Tiefenbruck, \u003ca href=\"https://ajc.maths.uq.edu.au/pdf/64/ajc_v64_p166.pdf\"\u003eQ-analogues of convolutions of Fibonacci numbers\u003c/a\u003e, Australasian Journal of Combinatorics, Volume 64(1) (2016), Pages 166-193.",
				"J. Riordan, \u003ca href=\"/A001820/a001820.pdf\"\u003eNotes to N. J. A. Sloane, Jul. 1968\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/EdgeCount.html\"\u003eEdge Count\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/FibonacciCubeGraph.html\"\u003eFibonacci Cube Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/EdgeCount.html\"\u003eEdge Count\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/MaximalClique.html\"\u003eMaximal Clique\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/MaximumClique.html\"\u003eMaximum Clique\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,1,-2,-1)."
			],
			"formula": [
				"G.f.: x^2/(1 - x - x^2)^2. - _Simon Plouffe_ in his 1992 dissertation",
				"a(n) = 2*a(n-1) + a(n-2) - 2*a(n-3) - a(n-4), n \u003e 3.",
				"a(n) = Sum_{k=0..n} F(k)*F(n-k) where F(k)=A000045(k) (the Fibonacci sequence).",
				"a(n+1) = Sum_{i=0..F(n)} A007895(i), where F = A000045, the Fibonacci sequence. - Claude Lenormand (claude.lenormand(AT)free.fr), Feb 04 2001",
				"a(n) = Sum_{k=0..floor(n/2)-1} (k+1)*binomial(n-k-1, k+1). - _Emeric Deutsch_, Nov 15 2001",
				"a(n) = floor( (1/5)*(n-1/sqrt(5))*phi^n + 1/2 ) where phi=(1+sqrt(5))/2 is the golden ratio. - _Benoit Cloitre_, Jan 05 2003",
				"a(n) = a(n-1) + A010049(n-1) for n \u003e 0. - _Emeric Deutsch_, Dec 10 2003",
				"a(n) = Sum_{k=0..floor((n-2)/2)} (n-k-1)*binomial(n-k-2, k). - _Paul Barry_, Jan 25 2005",
				"a(n) = ((n-1)*F(n) + 2*n*F(n-1))/5, F(n)=A000045(n) (see Vajda and Koshy reference).",
				"F'(n, 1), the first derivative of the n-th Fibonacci polynomial evaluated at 1. - _T. D. Noe_, Jan 18 2006",
				"a(n) = a(n-1) + a(n-2) + F(n-1), where F=A000045, the Fibonacci sequence. - _Graeme McRae_, Jun 02 2006",
				"a(n) = (1/5)*(n-1/sqrt(5))*((1+sqrt(5))/2)^n + (1/5)*(n+1/sqrt(5))*((1-sqrt(5))/2)^n. - _Graeme McRae_, Jun 02 2006",
				"a(n) = A055244(n-1) - F(n-2). Example: a(6) = 20 = A055244(5) - F(3) = (23 - 3). - _Gary W. Adamson_, Jul 27 2007",
				"a(n) = term (1,3) in the 4 X 4 matrix [2,1,0,0; 1,0,1,0; -2,0,0,1; -1,0,0,0]^n. - _Alois P. Heinz_, Aug 01 2008",
				"a(n) = ((n+1)*F(n-1) + (n-1)*F(n+1))/5. - _Richard R. Forberg_, Aug 04 2014",
				"(n-2)*a(n) - (n-1)*a(n-1) - n*a(n-2) = 0, n \u003e 1. - _Michael D. Weiner_, Nov 18 2014",
				"a(n) = Sum_{i=0..n-1} Sum_{j=0..i} F(j-1)*F(i-j), where F(n) = A000045 Fibonacci Numbers. - _Carlos A. Rico A._, Jul 14 2016",
				"a(n) = (n*Lucas(n) - Fibonacci(n))/5, where Lucas = A000032, Fibonacci = A000045. - _Vladimir Reshetnikov_, Sep 27 2016",
				"a(n) = (n-1)*hypergeom([1-n/2, (3-n)/2], [1-n], -4) for n \u003e= 2. - _Peter Luschny_, Apr 10 2018",
				"a(n) = -(-1)^n a(-n) for all n in Z. - _Michael Somos_, Jun 24 2018",
				"E.g.f.: (1/50)*exp(-2*x/(1+sqrt(5)))*(2*sqrt(5)-5*(-1+sqrt(5))*x+exp(sqrt(5)*x)*(-2*sqrt(5)+5*(1+sqrt(5))*x)). - _Stefano Spezia_, Sep 03 2019"
			],
			"example": [
				"G.f. = x^2 + 2*x^3 + 5*x^4 + 10*x^5 + 20*x^6 + 38*x^7 + 71*x^8 + 130*x^9 + ... - _Michael Somos_, Jun 24 2018"
			],
			"maple": [
				"a:= n-\u003e (Matrix(4, (i,j)-\u003e if (i=j-1) then 1 elif j=1 then [2, 1, -2, -1][i] else 0 fi)^n)[1,3]: seq(a(n), n=0..40); # _Alois P. Heinz_, Aug 01 2008",
				"# Alternative:",
				"A001629 := n -\u003e `if`(n\u003c2, 0, (n-1)*hypergeom([1-n/2, (3-n)/2], [1-n], -4)):",
				"seq(simplify(A001629(n)), n=0..37); # _Peter Luschny_, Apr 10 2018"
			],
			"mathematica": [
				"Table[Sum[Binomial[n - i, i] i, {i, 0, n}], {n, 0, 34}] (* _Geoffrey Critzer_, May 04 2009 *)",
				"Table[Abs[(1 - (1/2 - I/2) (1 + (-1)^(n + 1)))* Coefficient[ CharacteristicPolynomial[Array[KroneckerDelta[#1, #2] I + KroneckerDelta[#1 + 1, #2] +  KroneckerDelta[#1 - 1, #2] \u0026, {n - 1, n - 1}], x], x]], {n, 2, 50}] (* _John M. Campbell_, Jun 23 2011 *)",
				"LinearRecurrence[{2, 1, -2, -1}, {0, 0, 1, 2}, 40] (* _Harvey P. Dale_, Aug 26 2013 *)",
				"CoefficientList[Series[x^2/(1 - x - x^2)^2, {x, 0, 40}], x] (* _Vincenzo Librandi_, Nov 19 2014 *)",
				"Table[(2 n Fibonacci[n - 1] + (n - 1) Fibonacci[n])/5, {n, 0, 40}] (* _Vladimir Reshetnikov_, May 08 2016 *)",
				"Table[With[{fibs=Fibonacci[Range[n]]},ListConvolve[fibs,fibs]],{n,-1,40}]//Flatten (* _Harvey P. Dale_, Aug 19 2018 *)"
			],
			"program": [
				"(Haskell)",
				"a001629 n = a001629_list !! (n-1)",
				"a001629_list = f [] $ tail a000045_list where",
				"   f us (v:vs) = (sum $ zipWith (*) us a000045_list) : f (v:us) vs",
				"-- _Reinhard Zumkeller_, Jan 18 2014, Oct 16 2011",
				"(PARI) Vec(1/(1-x-x^2)^2+O(x^99)) \\\\ _Charles R Greathouse IV_, Feb 03 2014",
				"(PARI) a(n)=([0,1,0,0; 0,0,1,0; 0,0,0,1; -1,-2,1,2]^n)[2,4] \\\\ _Charles R Greathouse IV_, Jul 20 2016",
				"(MAGMA) I:=[0,0,1,2]; [n le 4 select I[n] else 2*Self(n-1)+Self(n-2)-2*Self(n-3)-Self(n-4): n in [1..40]]; // _Vincenzo Librandi_, Nov 19 2014",
				"(GAP) List([0..40],n-\u003eSum([0..n],k-\u003eFibonacci(k)*Fibonacci(n-k))); # _Muniru A Asiru_, Jun 24 2018"
			],
			"xref": [
				"a(n) = A037027(n-1, 1), n \u003e= 1 (Fibonacci convolution triangle).",
				"Row sums of triangles A058071, A134510, A134836. First differences of A006478.",
				"Cf. A000045, A001628, A010049, A055244."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_",
			"references": 86,
			"revision": 333,
			"time": "2021-11-05T09:32:49-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}