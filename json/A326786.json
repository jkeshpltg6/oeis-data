{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A326786",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 326786,
			"data": "0,1,1,0,2,2,2,2,1,0,0,0,0,0,0,0,2,2,0,0,1,1,1,1,2,2,0,0,1,1,1,1,2,0,2,0,1,1,1,1,2,0,2,0,1,1,1,1,1,1,1,1,3,3,3,3,1,1,1,1,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3",
			"name": "Cut-connectivity of the set-system with BII-number n.",
			"comment": [
				"A binary index of n is any position of a 1 in its reversed binary expansion. The binary indices of n are row n of A048793. We define the set-system with BII-number n to be obtained by taking the binary indices of each binary index of n. Every finite set of finite nonempty sets has a different BII-number. For example, 18 has reversed binary expansion (0,1,0,0,1), and since the binary indices of 2 and 5 are {2} and {1,3} respectively, the BII-number of {{2},{1,3}} is 18.",
				"Elements of a set-system are sometimes called edges. The cut-connectivity of a set-system is the minimum number of vertices that must be removed (together with any resulting empty or duplicate edges) to obtain a disconnected or empty set-system. Except for cointersecting set-systems (A326853), this is the same as vertex-connectivity (A327051)."
			],
			"example": [
				"Positions of first appearances of each integer, together with the corresponding set-systems, are:",
				"     0: {}",
				"     1: {{1}}",
				"     4: {{1,2}}",
				"    52: {{1,2},{1,3},{2,3}}",
				"  2868: {{1,2},{1,3},{2,3},{1,4},{2,4},{3,4}}"
			],
			"mathematica": [
				"bpe[n_]:=Join@@Position[Reverse[IntegerDigits[n,2]],1];",
				"csm[s_]:=With[{c=Select[Tuples[Range[Length[s]],2],And[OrderedQ[#],UnsameQ@@#,Length[Intersection@@s[[#]]]\u003e0]\u0026]},If[c=={},s,csm[Sort[Append[Delete[s,List/@c[[1]]],Union@@s[[c[[1]]]]]]]]];",
				"vertConn[y_]:=If[Length[csm[bpe/@y]]!=1,0,Min@@Length/@Select[Subsets[Union@@bpe/@y],Function[del,Length[csm[DeleteCases[DeleteCases[bpe/@y,Alternatives@@del,{2}],{}]]]!=1]]];",
				"Table[vertConn[bpe[n]],{n,0,100}]"
			],
			"xref": [
				"Cf. A000120, A013922, A029931, A048793, A070939, A305078, A322388, A322389 (same for MM-numbers), A322390, A326031, A326701, A326749, A326753, A326787 (edge-connectivity), A327051 (vertex-connectivity)."
			],
			"keyword": "nonn",
			"offset": "0,5",
			"author": "_Gus Wiseman_, Jul 25 2019",
			"references": 30,
			"revision": 10,
			"time": "2019-09-02T02:17:25-04:00",
			"created": "2019-07-25T20:58:42-04:00"
		}
	]
}