{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005434",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5434,
			"id": "M0555",
			"data": "1,2,3,4,6,8,10,13,17,21,27,30,37,47,57,62,75,87,102,116,135,155,180,194,220,254,289,312,359,392,438,479,538,595,664,701,772,863,956,1005,1115,1205,1317,1414,1552,1677,1836,1920,2074,2249,2444",
			"name": "Number of distinct autocorrelations of binary words of length n.",
			"comment": [
				"Conjecture: a(n + 1) - a(n) \u003c a(n + 13) - a(n + 12) for all n \u003e= 1. - _Eric Rowland_, Nov 24 2021"
			],
			"reference": [
				"R. L. Graham, D. E. Knuth and O. Patashnik, Concrete Mathematics, Addison-Wesley Publ., 2nd Ed., 1994. Section 8.4: Flipping Coins",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"E-Hern Lee, \u003ca href=\"/A005434/b005434.txt\"\u003eTable of n, a(n) for n = 1..654\u003c/a\u003e",
				"L. J. Guibas, \u003ca href=\"http://dx.doi.org/10.1007/978-3-642-82456-2_17\"\u003ePeriodicities in Strings\u003c/a\u003e, Combinatorial Algorithms on Words 1985, NATO ASI Vol. F12, 257-269",
				"L. J. Guibas and A. M. Odlyzko, \u003ca href=\"http://dx.doi.org/10.1016/0097-3165(81)90038-8\"\u003ePeriods in Strings\u003c/a\u003e, Journal of Combinatorial Theory A 30:1 (1981) 19-42.",
				"Leo J. Guibas and Andrew M. Odlyzko, \u003ca href=\"http://dx.doi.org/10.1016/0097-3165(81)90005-4\"\u003eString overlaps, pattern matching and nontransitive games\u003c/a\u003e, Journal of Combinatorial Theory Series A, 30 (March 1981), 183-208.",
				"H. Harborth, \u003ca href=\"http://gdz.sub.uni-goettingen.de/dms/resolveppn/?PPN=GDZPPN002189852\"\u003eEndliche 0-1-Folgen mit gleichen Teilblöcken\u003c/a\u003e, Journal für Mathematik, 271 (1974) 139-154.",
				"A. Kaseorg, \u003ca href=\"https://codegolf.stackexchange.com/a/127038/39244\"\u003eRust program used to compute values for n up to 500\u003c/a\u003e",
				"E. H. Rivals and S. Rahmann, \u003ca href=\"http://dx.doi.org/10.1016/S0097-3165(03)00123-7\"\u003eCombinatorics of Periods in Strings\u003c/a\u003e, Journal of Combinatorial Theory - Series A, Vol. 104(1) (2003), pp. 95-113.",
				"E. H. Rivals, \u003ca href=\"http://www.lirmm.fr/~rivals/RESEARCH/PERIOD/\"\u003eAutocorrelation of Strings\u003c/a\u003e.",
				"E. H. Rivals and S. Rahmann \u003ca href=\"http://www.lirmm.fr/~rivals/PUBLI/FILES/RivalsRahmannJCTA03.pdf\"\u003eCombinatorics of Periods in Strings\u003c/a\u003e",
				"T. Sillke, \u003ca href=\"http://www.mathematik.uni-bielefeld.de/~sillke/SEQUENCES/autocorrelation-range.c\"\u003eAutocorrelation Range\u003c/a\u003e",
				"T. Sillke, \u003ca href=\"http://www.mathematik.uni-bielefeld.de/~sillke/SEQUENCES/kappa\"\u003ekappa sequence for words of length n\u003c/a\u003e",
				"T. Sillke, \u003ca href=\"http://www.mathematik.uni-bielefeld.de/~sillke/SEQUENCES/series018\"\u003eThe autocorrelation function\u003c/a\u003e"
			],
			"example": [
				"From _Eric Rowland_, Nov 22 2021: (Start)",
				"For n = 5 there are a(5) = 6 distinct autocorrelations of length-5 binary words:",
				"  00000 can overlap itself in 1, 2, 3, 4, or 5 letters. Its autocorrelation is 11111.",
				"  00100 can overlap itself in 1, 2, or 5 letters. Its autocorrelation is 10011.",
				"  01010 can overlap itself in 1, 3, or 5 letters. Its autocorrelation is 10101.",
				"  00010 can overlap itself in 1 or 5 letters. Its autocorrelation is 10001.",
				"  01001 can overlap itself in 2 or 5 letters. Its autocorrelation is 10010.",
				"  00001 can only overlap itself in 5 letters. Its autocorrelation is 10000.",
				"(End)"
			],
			"maple": [
				"A005434 := proc( n :: posint )",
				"    local    S := table();",
				"    for local c in Iterator:-BinaryGrayCode( n ) do",
				"        c := convert( c, 'list' );",
				"        S[ [seq]( evalb( c[ 1 .. i + 1 ] = c[ n - i .. n ] ), i = 0 .. n - 1 ) ] := 0",
				"    end do;",
				"    numelems( S )",
				"end proc: # _James McCarron_, Jun 21 2017"
			],
			"mathematica": [
				"Table[Length[Union[Map[Flatten[Position[Table[Take[#,n-i]==Drop[#,i],{i,0,n-1}],True]-1]\u0026,Tuples[{0,1},n]]]],{n,1,15}] (* _Geoffrey Critzer_, Nov 29 2013 *)"
			],
			"xref": [
				"Cf. A018819 (related to a lower bound for autocorrelations), A045690 (the number of binary strings sharing the same autocorrelation)."
			],
			"keyword": "nonn,nice",
			"offset": "1,2",
			"author": "_Simon Plouffe_, _N. J. A. Sloane_",
			"ext": [
				"More terms and additional references from torsten.sillke(at)lhsystems.com",
				"Definition clarified by _Eric Rowland_, Nov 22 2021"
			],
			"references": 8,
			"revision": 52,
			"time": "2021-12-04T12:38:24-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}