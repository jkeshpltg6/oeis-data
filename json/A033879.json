{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A033879",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 33879,
			"data": "1,1,2,1,4,0,6,1,5,2,10,-4,12,4,6,1,16,-3,18,-2,10,8,22,-12,19,10,14,0,28,-12,30,1,18,14,22,-19,36,16,22,-10,40,-12,42,4,12,20,46,-28,41,7,30,6,52,-12,38,-8,34,26,58,-48,60,28,22,1,46,-12,66,10,42,-4,70,-51",
			"name": "Deficiency of n, or 2n - (sum of divisors of n).",
			"comment": [
				"Records for the sequence of the absolute values are in A075728 and the indices of these records in A074918. - _R. J. Mathar_, Mar 02 2007",
				"a(n) = 1 iff n is a power of 2. a(n) = n - 1 iff n is prime. - _Omar E. Pol_, Jan 30 2014",
				"If a(n) = 1 then n is called a least deficient number or an almost perfect number. All the powers of 2 are least deficient numbers but it is not known if there exists a least deficient number that is not a power of 2. See A000079. - _Jianing Song_, Oct 13 2019",
				"It is not known whether there are any -1's in this sequence. See comment in A033880. - _Antti Karttunen_, Feb 02 2020"
			],
			"reference": [
				"R. K. Guy, Unsolved Problems in Number Theory, Section B2."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A033879/b033879.txt\"\u003eTable of n, a(n) for n = 1..16384\u003c/a\u003e (first 2000 terms from T. D. Noe)",
				"Nichole Davis, Dominic Klyve and Nicole Kraght, \u003ca href=\"http://dx.doi.org/10.2140/involve.2013.6.493\"\u003eOn the difference between an integer and the sum of its proper divisors\u003c/a\u003e, Involve, Vol. 6 (2013), No. 4, 493-504; DOI: 10.2140/involve.2013.6.493.",
				"Jose A. B. Dris, \u003ca href=\"https://arxiv.org/abs/1610.01868\"\u003eConditions Equivalent to the Descartes-Frenicle-Sorli Conjecture on Odd Perfect Numbers\u003c/a\u003e, arXiv preprint arXiv:1610.01868 [math.NT], 2016.",
				"Jose Arnaldo B. Dris, \u003ca href=\"https://arxiv.org/abs/1703.09077\"\u003eAnalysis of the Ratio D(n)/n\u003c/a\u003e, arXiv:1703.09077 [math.NT], 2017.",
				"Jose Arnaldo Bebita Dris, \u003ca href=\"http://nntdm.net/volume-23-2017/number-4/01-13/\"\u003eOn a curious biconditional involving the divisors of odd perfect numbers\u003c/a\u003e, Notes on Number Theory and Discrete Mathematics, 23(4) (2017), 1-13.",
				"Jose Arnaldo Bebita Dris, Immanuel Tobias San Diego, \u003ca href=\"https://arxiv.org/abs/2002.12139\"\u003eSome Modular Considerations Regarding Odd Perfect Numbers\u003c/a\u003e, arXiv:2002.12139 [math.NT], 2020.",
				"Jose Arnaldo Bebita Dris, Doli-Jane Uvales Tejada, \u003ca href=\"https://doi.org/10.7546/nntdm.2018.24.3.62-67\"\u003eConditions equivalent to the Descartes-Frenicle-Sorli Conjecture on odd perfect numbers - Part II\u003c/a\u003e, Notes on Number Theory and Discrete Mathematics (2018) Vol. 24, No. 3, 62-67.",
				"Jose Arnaldo Bebita Dris, Doli-Jane Uvales Tejada, \u003ca href=\"https://doi.org/10.7546/nntdm.2019.25.1.199-205\"\u003eA note on the OEIS sequence A228059\u003c/a\u003e, Notes on Number Theory and Discrete Mathematics (2019) Vol. 25, No. 1, 199-205.",
				"\u003ca href=\"/index/Si#SIGMAN\"\u003eIndex entries for sequences related to sigma(n)\u003c/a\u003e"
			],
			"formula": [
				"a(n) = -A033880(n).",
				"a(n) = A005843(n) - A000203(n). - _Omar E. Pol_, Dec 14 2008",
				"a(n) = n - A001065(n). - _Omar E. Pol_, Dec 27 2013",
				"G.f.: 2*x/(1 - x)^2 - Sum_{k\u003e=1} k*x^k/(1 - x^k). - _Ilya Gutkovskiy_, Jan 24 2017",
				"a(n) = A286385(n) - A252748(n). - _Antti Karttunen_, May 13 2017",
				"From _Antti Karttunen_, Dec 29 2017: (Start)",
				"a(n) = Sum_{d|n} A083254(d).",
				"a(n) = Sum_{d|n} A008683(n/d)*A296075(d).",
				"a(n) = A065620(A295881(n)) = A117966(A295882(n)).",
				"a(n) = A294898(n) + A000120(n).",
				"(End)",
				"From _Antti Karttunen_, Jun 03 2019: (Start)",
				"Sequence can be represented in arbitrarily many ways as a difference of the form (n - f(n)) - (g(n) - n), where f and g are any two sequences whose sum f(n)+g(n) = sigma(n). Here are few examples:",
				"a(n) = A325314(n) - A325313(n) = A325814(n) - A034460(n) = A325978(n) - A325977(n).",
				"a(n) = A325976(n) - A325826(n) = A325959(n) - A325969(n) = A003958(n) - A324044(n).",
				"a(n) = A326049(n) - A326050(n) = A326055(n) - A326054(n) = A326044(n) - A326045(n).",
				"a(n) = A326058(n) - A326059(n) = A326068(n) - A326067(n).",
				"a(n) = A326128(n) - A326127(n) = A066503(n) - A326143(n).",
				"a(n) = A318878(n) - A318879(n).",
				"a(A228058(n)) = A325379(n).",
				"(End)"
			],
			"example": [
				"For n = 10 the divisors of 10 are 1, 2, 5, 10, so the deficiency of 10 is 10 minus the sum of its proper divisors or simply 10 - 5 - 2 - 1 = 2. - _Omar E. Pol_, Dec 27 2013"
			],
			"maple": [
				"with(numtheory): A033879:=n-\u003e2*n-sigma(n): seq(A033879(n), n=1..100);"
			],
			"mathematica": [
				"Table[2n-DivisorSigma[1,n],{n,80}] (* _Harvey P. Dale_, Oct 24 2011 *)"
			],
			"program": [
				"(PARI) a(n)=2*n-sigma(n) \\\\ _Charles R Greathouse IV_, Oct 13 2016"
			],
			"xref": [
				"Cf. A000396 (positions of zeros), A005100 (of positive terms), A005101 (of negative terms).",
				"Cf. A000203, A033880, A074918, A075728, A192895, A286385, A286449, A295881, A295882.",
				"Cf. A083254 (Möbius transform), A228058, A296074, A296075, A323910, A325636, A325826, A325970, A325976.",
				"For this sequence applied to various permutations of natural numbers and some other sequences, see A323174, A323244, A324055, A324185, A324546, A324574, A324575, A324654, A325379."
			],
			"keyword": "sign,nice",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Definition corrected Jul 04 2005"
			],
			"references": 149,
			"revision": 130,
			"time": "2020-05-26T22:15:29-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}