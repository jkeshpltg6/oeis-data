{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A077592",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 77592,
			"data": "1,1,1,1,2,1,1,3,2,1,1,4,3,3,1,1,5,4,6,2,1,1,6,5,10,3,4,1,1,7,6,15,4,9,2,1,1,8,7,21,5,16,3,4,1,1,9,8,28,6,25,4,10,3,1,1,10,9,36,7,36,5,20,6,4,1,1,11,10,45,8,49,6,35,10,9,2,1,1,12,11,55,9,64,7,56,15,16,3,6,1",
			"name": "Table by antidiagonals of tau_k(n), the k-th Piltz function (see A007425), or n-th term of the sequence resulting from applying the inverse Möbius transform (k-1) times to the all-ones sequence.",
			"comment": [
				"As an array, also the number of length k - 1 chains of divisors of n. - _Gus Wiseman_, May 03 2021"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A077592/b077592.txt\"\u003eAntidiagonals n = 1..141, flattened\u003c/a\u003e",
				"Adolf Piltz, \u003ca href=\"https://gdz.sub.uni-goettingen.de/id/PPN271032898\"\u003eUeber das Gesetz, nach welchem die mittlere Darstellbarkeit der natürlichen Zahlen als Produkte einer gegebenen Anzahl Faktoren mit der Grösse der Zahlen wächst\u003c/a\u003e, Doctoral Dissertation, Friedrich-Wilhelms-Universität zu Berlin, 1881; the k-th Piltz function tau_k(n) is denoted by phi(n,k) and its recurrence and Dirichlet series appear on p. 6.",
				"Wikipedia, \u003ca href=\"https://de.wikipedia.org/wiki/Adolf_Piltz\"\u003eAdolf Piltz\u003c/a\u003e."
			],
			"formula": [
				"If n = Product_i p_i^e_i, then T(n,k) = Product_i C(k+e_i-1, e_i). T(n,k) = sum_d{d|n} T(n-1,d) = A077593(n,k) - A077593(n-1,k).",
				"Columns are multiplicative.",
				"Dirichlet g.f. for column k: Zeta(s)^k. - _Geoffrey Critzer_, Feb 16 2015",
				"A(n,k) = A334997(n,k-1)."
			],
			"example": [
				"T(6,3) = 9 because we have: 1*1*6, 1*2*3, 1*3*2, 1*6*1, 2*1*3, 2*3*1, 3*1*2, 3*2*1, 6*1*1. - _Geoffrey Critzer_, Feb 16 2015",
				"From _Gus Wiseman_, May 03 2021: (Start)",
				"Array begins:",
				"       n=1 n=2 n=3 n=4 n=5 n=6 n=7 n=8",
				"  k=1:  1   1   1   1   1   1   1   1",
				"  k=2:  1   2   2   3   2   4   2   4",
				"  k=3:  1   3   3   6   3   9   3  10",
				"  k=4:  1   4   4  10   4  16   4  20",
				"  k=5:  1   5   5  15   5  25   5  35",
				"  k=6:  1   6   6  21   6  36   6  56",
				"  k=7:  1   7   7  28   7  49   7  84",
				"  k=8:  1   8   8  36   8  64   8 120",
				"  k=9:  1   9   9  45   9  81   9 165",
				"The (4,6) = 21 chains are:",
				"  (1,1,1,1,1)  (4,2,1,1,1)  (4,4,2,2,2)",
				"  (2,1,1,1,1)  (4,2,2,1,1)  (4,4,4,1,1)",
				"  (2,2,1,1,1)  (4,2,2,2,1)  (4,4,4,2,1)",
				"  (2,2,2,1,1)  (4,2,2,2,2)  (4,4,4,2,2)",
				"  (2,2,2,2,1)  (4,4,1,1,1)  (4,4,4,4,1)",
				"  (2,2,2,2,2)  (4,4,2,1,1)  (4,4,4,4,2)",
				"  (4,1,1,1,1)  (4,4,2,2,1)  (4,4,4,4,4)",
				"The a(6,4) = 16 chains are:",
				"  (1,1,1)  (3,1,1)  (6,2,1)  (6,6,1)",
				"  (2,1,1)  (3,3,1)  (6,2,2)  (6,6,2)",
				"  (2,2,1)  (3,3,3)  (6,3,1)  (6,6,3)",
				"  (2,2,2)  (6,1,1)  (6,3,3)  (6,6,6)",
				"The triangular form T(n,k) = A(k,n-k) gives the number of length n - k chains of divisors of k. It begins:",
				"  1",
				"  1  1",
				"  1  2  1",
				"  1  3  2  1",
				"  1  4  3  3  1",
				"  1  5  4  6  2  1",
				"  1  6  5 10  3  4  1",
				"  1  7  6 15  4  9  2  1",
				"  1  8  7 21  5 16  3  4  1",
				"  1  9  8 28  6 25  4 10  3  1",
				"  1 10  9 36  7 36  5 20  6  4  1",
				"  1 11 10 45  8 49  6 35 10  9  2  1",
				"(End)"
			],
			"maple": [
				"with(numtheory):",
				"A:= proc(n,k) option remember; `if`(k=1, 1,",
				"      add(A(d, k-1), d=divisors(n)))",
				"    end:",
				"seq(seq(A(n, 1+d-n), n=1..d), d=1..14);  # _Alois P. Heinz_, Feb 25 2015"
			],
			"mathematica": [
				"tau[n_, 1] = 1; tau[n_, k_] := tau[n, k] = Plus @@ (tau[ #, k - 1] \u0026 /@ Divisors[n]); Table[tau[n - k + 1, k], {n, 14}, {k, n, 1, -1}] // Flatten (* _Robert G. Wilson v_ *)",
				"tau[1, k_] := 1; tau[n_, k_] := Times @@ (Binomial[Last[#] + k - 1, k - 1] \u0026 /@ FactorInteger[n]); Table[tau[k, n - k + 1], {n, 1, 13}, {k, 1, n}] // Flatten (* _Amiram Eldar_, Sep 13 2020 *)",
				"Table[Length[Select[Tuples[Divisors[k],n-k],And@@Divisible@@@Partition[#,2,1]\u0026]],{n,12},{k,1,n}] (* TRIANGLE, _Gus Wiseman_, May 03 2021 *)",
				"Table[Length[Select[Tuples[Divisors[k],n-1],And@@Divisible@@@Partition[#,2,1]\u0026]],{n,6},{k,6}] (* ARRAY, _Gus Wiseman_, May 03 2021 *)"
			],
			"xref": [
				"Columns include: A000012, A000005, A034695, A111217, A111218, A111219, A111220, A111221, A111306.",
				"Rows include (with multiplicity and some offsets) A000012, A000027, A000027, A000217, A000027, A000290, A000027, A000292, A000217, A000290, A000027, A002411, A000027, A000290, A000290, A000332 etc.",
				"Cf. A077593.",
				"Row k = 2 of the array is A007425.",
				"Row k = 3 of the array is A007426.",
				"Row k = 4 of the array is A061200.",
				"The transpose is A334997 (offset n = 1, k = 0).",
				"The diagonal n = k of the array (central column of the triangle) is A163767.",
				"The strict case is the transpose of A343662 (row sums: A337256).",
				"The version for multisets instead of chains is the transpose of A343658.",
				"A067824 counts strict chains of divisors starting with n.",
				"A074206 counts strict chains of divisors from n to 1.",
				"A146291 counts divisors of n with k prime factors (with multiplicity).",
				"A251683 counts strict length k + 1 chains of divisors from n to 1.",
				"A253249 counts nonempty chains of divisors of n.",
				"A334996 counts strict length-k chains of divisors from n to 1.",
				"A337255 counts strict length-k chains of divisors starting with n.",
				"Cf. A018892, A051026, A062319, A143773, A176029, A343656."
			],
			"keyword": "mult,nonn,tabl,look",
			"offset": "1,5",
			"author": "_Henry Bottomley_, Nov 08 2002",
			"ext": [
				"Typo in formula fixed by _Geoffrey Critzer_, Feb 16 2015"
			],
			"references": 23,
			"revision": 40,
			"time": "2021-05-05T08:46:27-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}