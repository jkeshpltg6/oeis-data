{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A319886",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 319886,
			"data": "3,6,6,0,-24,-114,-105,-42,390,378,258,-930,-915,-720,1800,1782,1494,-3096,-3075,-2676,4884,4860,4332,-7260,-7233,-6558,10290,10260,9420,-14070,-14037,-13014,18666,18630,17406,-24174,-24135,-22692,30660,30618,28938",
			"name": "a(n) = 3*2*1 - 6*5*4 + 9*8*7 - 12*11*10 + 15*14*13 - 18*17*16 + ... - (up to the n-th term).",
			"comment": [
				"For similar sequences that alternate in descending blocks of k natural numbers, we have: a(n) = (-1)^floor(n/k) * Sum_{j=1..k-1} (floor((n-j)/k) - floor((n-j-1)/k)) * (Product_{i=1..j} n-i-j+k+1) + Sum_{j=1..n} (-1)^(floor(j/k)+1) * (floor(j/k) - floor((j-1)/k)) * (Product_{i=1..k} j-i+1). Here, k=3."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A319886/b319886.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_13\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,0,-4,4,0,-6,6,0,-4,4,0,-1,1)."
			],
			"formula": [
				"From _Colin Barker_, Oct 01 2018: (Start)",
				"G.f.: 3*x*(1 + x + 2*x^3 - 4*x^4 - 30*x^5 + x^6 - 5*x^7 + 24*x^8) / ((1 - x)*(1 + x)^4*(1 - x + x^2)^4).",
				"a(n) = a(n-1) - 4*a(n-3) + 4*a(n-4) - 6*a(n-6) + 6*a(n-7) - 4*a(n-9) + 4*a(n-10) - a(n-12) + a(n-13) for n\u003e13.",
				"(End)"
			],
			"example": [
				"   a(1) = 3;",
				"   a(2) = 3*2 = 6;",
				"   a(3) = 3*2*1 = 6;",
				"   a(4) = 3*2*1 - 6 = 0;",
				"   a(5) = 3*2*1 - 6*5 = -24;",
				"   a(6) = 3*2*1 - 6*5*4 = -114;",
				"   a(7) = 3*2*1 - 6*5*4 + 9 = -105;",
				"   a(8) = 3*2*1 - 6*5*4 + 9*8 = -42;",
				"   a(9) = 3*2*1 - 6*5*4 + 9*8*7 = 390;",
				"  a(10) = 3*2*1 - 6*5*4 + 9*8*7 - 12 = 378;",
				"  a(11) = 3*2*1 - 6*5*4 + 9*8*7 - 12*11 = 258;",
				"  a(12) = 3*2*1 - 6*5*4 + 9*8*7 - 12*11*10 = -930;",
				"  a(13) = 3*2*1 - 6*5*4 + 9*8*7 - 12*11*10 + 15 = -915;",
				"  a(14) = 3*2*1 - 6*5*4 + 9*8*7 - 12*11*10 + 15*14 = -720;",
				"  a(15) = 3*2*1 - 6*5*4 + 9*8*7 - 12*11*10 + 15*14*13 = 1800;",
				"  a(16) = 3*2*1 - 6*5*4 + 9*8*7 - 12*11*10 + 15*14*13 - 18 = 1782;",
				"  a(17) = 3*2*1 - 6*5*4 + 9*8*7 - 12*11*10 + 15*14*13 - 18*17 = 1494;",
				"  a(18) = 3*2*1 - 6*5*4 + 9*8*7 - 12*11*10 + 15*14*13 - 18*17*16 = -3096;",
				"  etc."
			],
			"maple": [
				"a:=(n,k)-\u003e(-1)^(floor(n/k))* add((floor((n-j)/k)-floor((n-j-1)/k))*(mul(n-i-j+k+1,i=1..j)),j=1..k-1) + add( (-1)^(floor(j/k)+1)*(floor(j/k)-floor((j-1)/k))*(mul(j-i+1,i=1..k)),j=1..n): seq(a(n,3),n=1..50); # _Muniru A Asiru_, Sep 30 2018"
			],
			"program": [
				"(PARI) Vec(3*x*(1 + x + 2*x^3 - 4*x^4 - 30*x^5 + x^6 - 5*x^7 + 24*x^8) / ((1 - x)*(1 + x)^4*(1 - x + x^2)^4) + O(x^50)) \\\\ _Colin Barker_, Oct 01 2018"
			],
			"xref": [
				"For similar sequences, see: A001057 (k=1), A319885 (k=2), this sequence (k=3), A319887 (k=4), A319888 (k=5), A319889 (k=6), A319890 (k=7), A319891 (k=8), A319892 (k=9), A319893 (k=10)."
			],
			"keyword": "sign,easy",
			"offset": "1,1",
			"author": "_Wesley Ivan Hurt_, Sep 30 2018",
			"references": 9,
			"revision": 22,
			"time": "2018-10-05T08:25:09-04:00",
			"created": "2018-10-05T08:05:51-04:00"
		}
	]
}