{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A340061",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 340061,
			"data": "1,1,2,1,1,2,3,1,1,1,2,2,3,4,1,1,1,1,1,2,2,2,3,3,4,5,1,1,1,1,1,1,1,2,2,2,2,2,3,3,3,4,4,5,6,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,3,3,3,3,3,4,4,4,5,5,6,7,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2",
			"name": "Irregular triangle read by rows T(n,k) in which row n lists n blocks, where the m-th block consists of A000041(n-m) copies of m, with n \u003e= 1 and m \u003e= 1.",
			"comment": [
				"Conjecture: all divisors of all terms of row n are also all parts of all partitions of n.",
				"The conjecture gives a correspondence between divisors and partitions (see example).",
				"It is conjectured that every section of the set of partitions of n has essentially the same correspondence. For more information see A336811."
			],
			"example": [
				"Triangle begins:",
				"1;",
				"1, 2;",
				"1, 1, 2, 3;",
				"1, 1, 1, 2, 2, 3, 4;",
				"1, 1, 1, 1, 1, 2, 2, 2, 3, 3, 4, 5;",
				"1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 4, 4, 5, 6;",
				"1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 4, 4, 4, ...",
				"...",
				"For n = 6 the 6th row of the triangle consists of:",
				"  p(5) = 7 copies of 1, that is, [1, 1, 1, 1, 1, 1, 1],",
				"  p(4) = 5 copies of 2, that is, [2, 2, 2, 2, 2],",
				"  p(3) = 3 copies of 3, that is, [3, 3, 3],",
				"  p(2) = 2 copies of 4, that is, [4, 4],",
				"  p(1) = 1 copy   of 5, that is, [5],",
				"  p(0) = 1 copy   of 6, that is, [6],",
				"where p(j) is the j-th partition number A000041(j).",
				"About the conjecture we have that the divisors of the terms of the 6th row are:",
				".",
				".                                                                  1",
				".                                                         1, 1,    2",
				".                                 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 3",
				"6th row --\u003e  1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 4, 4, 5, 6",
				".                                                                             .",
				"There are nineteen 1's, eight 2's, four 3's, two 4's, one 5 and one 6.",
				"In total there are 19 + 8 + 4 + 2 + 1 + 1 = 35 divisors.",
				"On the other hand the partitions of 6 are:",
				".",
				"      Diagram          Parts",
				"    _ _ _ _ _ _",
				"   |_ _ _      |       6",
				"   |_ _ _|_    |       3 3",
				"   |_ _    |   |       4 2",
				"   |_ _|_ _|_  |       2 2 2",
				"   |_ _ _    | |       5 1",
				"   |_ _ _|_  | |       3 2 1",
				"   |_ _    | | |       4 1 1",
				"   |_ _|_  | | |       2 2 1 1",
				"   |_ _  | | | |       3 1 1 1",
				"   |_  | | | | |       2 1 1 1 1",
				"   |_|_|_|_|_|_|       1 1 1 1 1 1",
				".",
				"There are nineteen 1's, eight 2's, four 3's, two 4's, one 5 and one 6, as shown also the 6th row of A066633.",
				"In total there are 19 + 8 + 4 + 2 + 1 + 1 = A006128(6) = 35 parts.",
				"In accordance with the conjecture we can see that all divisors of all terms of the 6th row of triangle are the same positive integers as all parts of all partitions of 6."
			],
			"xref": [
				"Mirror of A176206.",
				"Row sums give A014153.",
				"Row n has length A000070(n-1).",
				"Right border gives A000027.",
				"Cf. A000041, A006128, A666133, A027750, A066186, A176206, A336811 (section)."
			],
			"keyword": "nonn,tabf",
			"offset": "1,3",
			"author": "_Omar E. Pol_, Dec 28 2020",
			"references": 13,
			"revision": 70,
			"time": "2021-01-10T11:22:50-05:00",
			"created": "2021-01-10T11:22:50-05:00"
		}
	]
}