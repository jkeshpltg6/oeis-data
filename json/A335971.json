{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A335971",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 335971,
			"data": "13,20,14,24,30,15,25,31,26,35,40,16,27,36,41,28,37,42,38,46,50,17,29,39,47,51,48,52,49,53,402,18,57,60,19,58,61,59,62,302,102,103,104,105,106,107,63,403,108,64,502,109,68,70,69,71,202,113,203,114,204,115,205,116,206",
			"name": "The Locomotive Pulling the Wagons to the Left sequence (see Comments lines for definition).",
			"comment": [
				"a(1) is the locomotive; a(2), a(3), a(4),... a(n),... are the successive wagons. To hook a wagon to its predecessor (on the left) you must be able to insert the leftmost digit of a(n) between the last two digits of a(n-1). In mathematical terms, the value of the leftmost digit of a(n) must be between (not equal to) the penultimate and the last digit of a(n-1). This is the lexicographically earliest sequence of distinct positive terms with this property."
			],
			"link": [
				"Carole Dubois, \u003ca href=\"/A335971/b335971.txt\"\u003eTable of n, a(n) for n = 1..5001\u003c/a\u003e"
			],
			"example": [
				"The sequence starts with 13, 20, 14, 24, 30,...",
				"a(1) = 13 as there is no earliest possible (pulling to the left) locomotive;",
				"a(2) = 20 starts with 2 and 1 \u003c 2 \u003c 3 [1 and 3 being the last two digits of a(1)];",
				"a(3) = 14 starts with 1 and 2 \u003e 1 \u003e 0 [2 and 0 being the last two digits of a(2)];",
				"a(4) = 24 starts with 2 and 1 \u003c 2 \u003c 4 [1 and 4 being the last two digits of a(3)]; etc."
			],
			"program": [
				"(Python)",
				"def dead_end(k): return abs((k//10)%10 - k%10) \u003c= 1",
				"def aupto(n, seed=13):",
				"  train, used = [seed], {seed}",
				"  for n in range(2, n+1):",
				"    caboose = train[-1]",
				"    h1, h2 = sorted([(caboose//10)%10, caboose%10])",
				"    hooks = set(range(h1+1, h2))",
				"    pow10 = 10",
				"    an = min(hooks)*pow10",
				"    while an in used: an += 1",
				"    hook = an//pow10",
				"    while True:",
				"      if hook in hooks:",
				"        if not dead_end(an):",
				"          train.append(an)",
				"          used.add(an)",
				"          break",
				"      else: pow10 *= 10",
				"      an = max(an+1, min(hooks)*pow10)",
				"      while an in used: an += 1",
				"      hook = an//pow10",
				"  return train    # use train[n-1] for a(n)",
				"print(aupto(65))  # _Michael S. Branicky_, Dec 14 2020"
			],
			"xref": [
				"Cf. A335972 (locomotive pushing to the right) and A335973 (two locomotives)."
			],
			"keyword": "base,nonn",
			"offset": "1,1",
			"author": "_Eric Angelini_ and _Carole Dubois_, Jul 03 2020",
			"references": 3,
			"revision": 19,
			"time": "2020-12-14T01:35:57-05:00",
			"created": "2020-07-14T23:38:32-04:00"
		}
	]
}