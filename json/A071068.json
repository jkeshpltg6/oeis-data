{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A071068",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 71068,
			"data": "0,1,1,2,1,2,2,3,2,2,2,4,3,3,3,5,4,4,3,6,4,5,4,7,5,5,5,7,5,5,5,8,6,7,6,11,7,7,7,11,8,8,9,13,10,8,8,13,10,8,7,14,10,10,7,13,10,11,9,15,11,11,11,15,11,11,11,18,12,13,11,21,13,14,13,20,14,13,14,20,16,13,13,22,15",
			"name": "Number of ways to write n as a sum of two unordered squarefree numbers.",
			"comment": [
				"The natural density of the squarefree numbers is 6/Pi^2, so An \u003c a(n) \u003c Bn for all large enough n with A \u003c 6/Pi^2 - 1/2 and B \u003e 3/Pi^2. The Schnirelmann density of the squarefree numbers is 53/88 \u003e 1/2, and so a(n) \u003e 0 for all n \u003e 1 (in fact, a(n+1) \u003e= 9n/88). It follows from Theoreme 3 bis. in Cohen, Dress, \u0026 El Marraki along with finite checking up to 16089908 that 0.10792n \u003c a(n) \u003c 0.303967n for n \u003e 36. (The lower bound holds for n \u003e 1.) - _Charles R Greathouse IV_, Feb 02 2016"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A071068/b071068.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Henri Cohen, Francois Dress, and Mahomed El Marraki, \u003ca href=\"https://projecteuclid.org/euclid.facm/1229618741\"\u003eExplicit estimates for summatory functions linked to the Möbius μ-function\u003c/a\u003e, Funct. Approx. Comment. Math. 37:1 (2007), pp. 51-63."
			],
			"formula": [
				"a(n) = sum(i=1..floor(n/2), abs(mu(i)*mu(n-i)) ). - _Wesley Ivan Hurt_, May 20 2013"
			],
			"example": [
				"12=1+11=2+10=5+7=6+6 hence a(12)=4."
			],
			"mathematica": [
				"Table[Sum[Abs[MoebiusMu[i] MoebiusMu[n - i]],{i, 1, Floor[n/2]}],{n, 1, 85}] (* _Indranil Ghosh_, Mar 10 2017 *)",
				"Table[Count[IntegerPartitions[n,{2}],_?(AllTrue[#,SquareFreeQ]\u0026)],{n,90}] (* Requires Mathematica version 10 or later *) (* _Harvey P. Dale_, Aug 13 2020 *)"
			],
			"program": [
				"(PARI) a(n)=sum(i=1,n\\2,issquarefree(i)\u0026\u0026issquarefree(n-i)) \\\\ _Charles R Greathouse IV_, May 21 2013",
				"(PARI) list(lim)=my(n=lim\\1); concat(0, ceil(Vec((Polrev(vector(n, k, issquarefree(k-1))) + O('x^(n+1)))^2)/2)) \\\\ _Charles R Greathouse IV_, May 21 2013"
			],
			"xref": [
				"Cf. A005117, A098235."
			],
			"keyword": "easy,nonn",
			"offset": "1,4",
			"author": "_Benoit Cloitre_, May 26 2002",
			"references": 23,
			"revision": 28,
			"time": "2020-08-13T12:35:42-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}