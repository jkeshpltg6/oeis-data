{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A326691",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 326691,
			"data": "1,2,3,1,5,2,7,1,1,2,11,3,13,2,1,1,17,2,19,1,3,2,23,1,1,2,1,1,29,30,31,1,1,2,1,1,37,2,3,1,41,2,43,1,1,2,47,3,1,2,1,1,53,2,5,7,3,2,59,1,61,2,1,1,1,6,67,1,1,2,71,1,73,2,3,1,1,2,79",
			"name": "a(n) = n/denominator(Sum_{prime p | n} 1/p - 1/n).",
			"comment": [
				"Denominator(Sum_{prime p | n} 1/p - 1/n) is a factor of n, since all primes in the sum divide n. So a(n) is an integer."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A326691/b326691.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e",
				"Christian Krause, et al, \u003ca href=\"https://github.com/loda-lang\"\u003eLODA, an assembly language, a computational model and a tool for mining integer sequences\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Giuga_number\"\u003eGiuga number\u003c/a\u003e"
			],
			"formula": [
				"a(n) = n/A326690(n).",
				"a(n) = n \u003e 1 iff n is either a prime or a Giuga number A007850.",
				"a(n) = gcd(n, 1+((n-1)*A003415(n))). [Conjectured, after an empirical formula found by LODA miner. This holds at least up to n=2^27] - _Antti Karttunen_, Mar 15 2021"
			],
			"example": [
				"a(18) = 18/denominator(Sum_{prime p | 18} 1/p - 1/18) = 18/denominator(1/2 + 1/3 - 1/18) = 18/denominator(7/9) = 18/9 = 2.",
				"a(30) = 30/denominator(Sum_{prime p | 30} 1/p - 1/30) = 30/denominator(1/2 + 1/3 + 1/5 - 1/30) = 30/denominator(1/1) = 30/1 = 30, and 30 is a Giuga number."
			],
			"mathematica": [
				"PrimeFactors[n_] := Select[Divisors[n], PrimeQ];",
				"f[n_] := Denominator[Sum[1/p, {p, PrimeFactors[n]}] - 1/n];",
				"Table[n/f[n], {n, 79}]"
			],
			"program": [
				"(PARI) A326691(n) = (n/A326690(n)); \\\\ _Antti Karttunen_, Mar 15 2021"
			],
			"xref": [
				"Cf. A003415, A007850, A326689, A326690, A326692, A326715."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Jonathan Sondow_, Jul 20 2019",
			"references": 4,
			"revision": 22,
			"time": "2021-12-04T12:29:38-05:00",
			"created": "2019-07-22T06:32:06-04:00"
		}
	]
}