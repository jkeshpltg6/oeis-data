{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A181187",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 181187,
			"data": "1,3,1,6,2,1,12,5,2,1,20,8,4,2,1,35,16,8,4,2,1,54,24,13,7,4,2,1,86,41,22,13,7,4,2,1,128,61,35,20,12,7,4,2,1,192,95,54,33,20,12,7,4,2,1,275,136,80,49,31,19,12,7,4,2,1,399,204,121,76,48,31,19,12,7,4,2,1,556,284",
			"name": "Triangle read by rows: T(n,k) = sum of k-th largest elements in all partitions of n.",
			"comment": [
				"For the connection with A066897 and A066898 see A206563. - _Omar E. Pol_, Feb 13 2012",
				"T(n,k) is also the total number of parts \u003e= k in all partitions of n. - _Omar E. Pol_, Feb 14 2012",
				"The first differences of row n together with 1 give the row n of triangle A066633. - _Omar E. Pol_, Feb 26 2012",
				"We define the k-th rank of a partition as the k-th part minus the number of parts \u003e= k. Since the first part of a partition is also the largest part of the same partition so the Dyson's rank of a partition is the case for k = 1. It appears that the sum of the k-th ranks of all partitions of n is equal to zero. - _Omar E. Pol_, Mar 04 2012",
				"T(n,k) is also the total number of divisors \u003e= k of all positive integers in a sequence with n blocks where the m-th block consists of A000041(n-m) copies of m, with 1 \u003c= m \u003c= n. - _Omar E. Pol_, Feb 05 2021"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A181187/b181187.txt\"\u003eRows n = 1..141, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = Sum_{j=1..n} A207031(j,k). - _Omar E. Pol_, May 02 2012"
			],
			"example": [
				"From _Omar E. Pol_, Feb 13 2012: (Start)",
				"Illustration of initial terms. First five rows of triangle as sums of columns from the partitions of the first five positive integers:",
				".",
				".                            5",
				".                            3+2",
				".                  4         4+1",
				".                  2+2       2+2+1",
				".          3       3+1       3+1+1",
				".     2    2+1     2+1+1     2+1+1+1",
				".  1  1+1  1+1+1   1+1+1+1   1+1+1+1+1",
				". -------------------------------------",
				".  1, 3,1, 6,2,1, 12,5,2,1, 20,8,4,2,1 --\u003e This triangle",
				".  |  |/|  |/|/|   |/|/|/|   |/|/|/|/|",
				".  1, 2,1, 4,1,1,  7,3,1,1, 12,4,2,1,1 --\u003e A066633",
				".",
				"For more information see A207031 and A206563.",
				"...",
				"Triangle begins:",
				"    1;",
				"    3,   1;",
				"    6,   2,   1;",
				"   12,   5,   2,  1;",
				"   20,   8,   4,  2,  1;",
				"   35,  16,   8,  4,  2,  1;",
				"   54,  24,  13,  7,  4,  2,  1;",
				"   86,  41,  22, 13,  7,  4,  2,  1;",
				"  128,  61,  35, 20, 12,  7,  4,  2, 1;",
				"  192,  95,  54, 33, 20, 12,  7,  4, 2, 1;",
				"  275, 136,  80, 49, 31, 19, 12,  7, 4, 2, 1;",
				"  399, 204, 121, 76, 48, 31, 19, 12, 7, 4, 2, 1;",
				"(End)"
			],
			"maple": [
				"p:= (f, g)-\u003e zip((x, y)-\u003e x+y, f, g, 0):",
				"b:= proc(n, i) option remember; local f, g;",
				"      if n=0 or i=1 then [1, n]",
				"    else f:= b(n, i-1); g:= `if`(i\u003en, [0], b(n-i, i));",
				"         p(p(f, g), [0$i, g[1]])",
				"      fi",
				"    end:",
				"T:= proc(n) local j, l, r, t;",
				"      l, r, t:= b(n, n), 1, 1;",
				"      for j from n to 2 by -1 do t:= t+l[j]; r:=r, t od;",
				"      seq([r][1+n-j], j=1..n)",
				"    end:",
				"seq(T(n), n=1..14); # _Alois P. Heinz_, Apr 05 2012"
			],
			"mathematica": [
				"Table[Plus @@ (PadRight[ #,n]\u0026 /@ IntegerPartitions[n]),{n,16}]",
				"(* Second program: *)",
				"T[n_, n_] = 1; T[n_, k_] /; k\u003cn := T[n, k] = T[n-k, k] + PartitionsP[n-k]; T[_, _] = 0; Table[Table[T[n, k], {k, n, 1, -1}] // Accumulate // Reverse, {n, 1, 16}] // Flatten (* _Jean-François Alcover_, Oct 10 2015, after _Omar E. Pol_ *)"
			],
			"xref": [
				"Row sums are A066186. First column is A006128. Reverse of each row converges to A000070.",
				"Columns 2-3: A096541, A207033. - _Omar E. Pol_, Feb 18 2012",
				"T(2n,n) gives A216053(n+1).",
				"Cf. A206283."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "1,2",
			"author": "_Wouter Meeussen_, Oct 09 2010",
			"ext": [
				"Better definition from _Omar E. Pol_, Feb 13 2012"
			],
			"references": 56,
			"revision": 107,
			"time": "2021-02-09T21:46:36-05:00",
			"created": "2010-10-20T03:00:00-04:00"
		}
	]
}