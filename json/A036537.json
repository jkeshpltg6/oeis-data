{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A036537",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 36537,
			"data": "1,2,3,5,6,7,8,10,11,13,14,15,17,19,21,22,23,24,26,27,29,30,31,33,34,35,37,38,39,40,41,42,43,46,47,51,53,54,55,56,57,58,59,61,62,65,66,67,69,70,71,73,74,77,78,79,82,83,85,86,87,88,89,91,93,94,95,97,101,102",
			"name": "Numbers whose number of divisors is a power of 2.",
			"comment": [
				"Primes and A030513(d(x)=4) are subsets; d(16k+4) and d(16k+12) have the form 3Q, so x=16k+4 or 16k-4 numbers are missing.",
				"A number m is a term if and only if all its divisors are infinitary, or A000005(m) = A037445(m). - _Vladimir Shevelev_, Feb 23 2017",
				"All exponents in the prime number factorization of a(n) have the form 2^k-1, k \u003e= 1. So it is an S-exponential sequence (see Shevelev link) with S={2^k-1}. Using Theorem 1, we obtain that a(n) ~ C*n, where C = Product((1-1/p)*(1 + Sum_{i\u003e=1} 1/p^(2^i-1))). - _Vladimir Shevelev_ Feb 27 2017",
				"This constant is C = 0.687827... . - _Peter J. C. Moses_, Feb 27 2017"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A036537/b036537.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Vladimir Shevelev, \u003ca href=\"https://www.math.bgu.ac.il/~shevelev/S_exp_numb.pdf\"\u003eS-exponential numbers\u003c/a\u003e, Acta Arithm., 175(2016), 385-395."
			],
			"formula": [
				"A209229(A000005(a(n))) = 1. - _Reinhard Zumkeller_, Nov 15 2012",
				"a(n) \u003c\u003c n. - _Charles R Greathouse IV_, Feb 25 2017"
			],
			"example": [
				"383, 384, 385, 386 have 1, 16, 8, 4 divisors, respectively, so they are consecutive terms of this sequence."
			],
			"mathematica": [
				"bi[ x_ ] := 1-Sign[ N[ Log[ 2, x ], 5 ]-Floor[ N[ Log[ 2, x ], 5 ] ] ]; ld[ x_ ] := Length[ Divisors[ x ] ]; Flatten[ Position[ Table[ bi[ ld[ x ] ], {x, 1, m} ], 1 ] ]",
				"Select[Range[110],IntegerQ[Log[2,DivisorSigma[0,#]]]\u0026] (* _Harvey P. Dale_, Nov 20 2016 *)"
			],
			"program": [
				"(Haskell)",
				"a036537 n = a036537_list !! (n-1)",
				"a036537_list = filter ((== 1) . a209229 . a000005) [1..]",
				"-- _Reinhard Zumkeller_, Nov 15 2012",
				"(PARI) is(n)=n=numdiv(n);n\u003e\u003evaluation(n,2)==1 \\\\ _Charles R Greathouse IV_, Mar 27 2013"
			],
			"xref": [
				"A005117, A030513, A058891, A175496, A336591 are subsequences.",
				"Complement of A162643; subsequence of A002035. - _Reinhard Zumkeller_, Jul 08 2009",
				"Subsequence of A162644, A337533.",
				"Cf. A000005, A036538."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Labos Elemer_",
			"references": 30,
			"revision": 59,
			"time": "2020-11-14T03:54:35-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}