{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A115978",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 115978,
			"data": "1,-2,0,-2,6,0,0,-4,0,-2,0,0,6,-4,0,0,6,0,0,-4,0,-4,0,0,0,-2,0,-2,12,0,0,-4,0,0,0,0,6,-4,0,-4,0,0,0,-4,0,0,0,0,6,-6,0,0,12,0,0,0,0,-4,0,0,0,-4,0,-4,6,0,0,-4,0,0,0,0,0,-4,0,-2,12,0,0,-4,0,-2,0,0,12,0,0,0,0,0,0,-8,0,-4,0,0,0,-4,0,0,6,0",
			"name": "Expansion of phi(-q) * phi(-q^3) in powers of q where phi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Cubic AGM theta functions: a(q) (see A004016), b(q) (A005928), c(q) (A005882)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A115978/b115978.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of theta_4(q) * theta_4(q^3) in powers of q.",
				"Expansion of (4 * a(q^4) - a(q)) / 3 = (4 * b(q^4) - b(q)) * b(q) / (3 * b(q^2)) in powers of q where a(), b() are cubic AGM theta functions. - _Michael Somos_, Nov 09 2013",
				"Expansion of (eta(q) * eta(q^3))^2 / (eta(q^2) * eta(q^6)) in powers of q.",
				"Euler transform of period 6 sequence [ -2, -1, -4, -1, -2, -2, ...].",
				"Moebius transform is period 12 sequence [ -2, 2, 0, 6, 2, 0, -2, -6, 0, -2, 2, 0, ...]. - _Michael Somos_, Nov 09 2013",
				"a(n) = -2*b(n) where b(n) is multiplicative and b(2^e) = -3 * (1 + (-1)^e) / 2 if e\u003e0, b(3^e) = 1, b(p^e) = 1+e if p == 1 (mod 6), b(p^e) = (1 +(-1)^e) / 2 if p == 5 (mod 6).",
				"Given g.f. A(x), then B(x) = A(x)^2 satisfies 0 = f(B(x), B(x^2), B(x^4)) where f(u, v, w) = v*(u + v)^2 - 4*u * (w^2 - v*w + v^2).",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (12 t)) = 192^(1/2) (t/i) g(t) where q = exp(2 Pi i t) and g() is the g.f. for A033762. - _Michael Somos_, Nov 09 2013",
				"G.f.: 1 - 2*(Sum_{k\u003e0} x^k / (1 + x^k + x^(2*k)) - 4 * x^(4*k) / (1 + x^(4*k) + x^(8*k))).",
				"G.f.: (Sum_{k in Z} (-x)^(k^2)) * (Sum_{k in Z} (-x)^(3*k^2)).",
				"a(n) = -2 * A115979(n) unless n=0. a(n) = (-1)^n * A033716(n).",
				"a(3*n + 2) = a(4*n + 2) = 0. a(3*n) = a(n). a(2*n + 1) = -2 * A033762(n). a(3*n + 1) = -2 * A122861(n). a(4*n) = A004016(n). a(4*n + 1) = -2 * A112604(n). a(6*n + 1) = -2 * A097195(n). - _Michael Somos_, Nov 09 2013"
			],
			"example": [
				"G.f. = 1 - 2*q - 2*q^3 + 6*q^4 - 4*q^7 - 2*q^9 + 6*q^12 - 4*q^13 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 4, 0, q] EllipticTheta[ 4, 0, q^3], {q, 0, n}] (* _Michael Somos_, Nov 09 2013 *)"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( (eta(x + A) * eta(x^3 + A))^2 / (eta(x^2 + A) * eta(x^6 + A)), n))}",
				"(PARI) {a(n) = local(A, p, e); if( n\u003c1, n==0, A = factor(n); -2 * prod( k=1, matsize(A)[1], if(p = A[k,1], e = A[k,2]; if( p==2, -3 * ((e+1)%2), if( p==3, 1, if( p%6==1, e+1, (e+1)%2))))))} /* _Michael Somos_, Nov 09 2013 */"
			],
			"xref": [
				"Cf. A004016, A033716, A033762, A097195, A112604, A122861."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Feb 09 2006",
			"references": 5,
			"revision": 14,
			"time": "2021-03-12T22:24:43-05:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}