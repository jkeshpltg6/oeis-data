{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A319996",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 319996,
			"data": "1,2,3,4,5,6,7,8,9,10,5,11,7,12,13,14,5,15,7,16,17,10,5,18,19,12,20,21,5,22,7,23,13,10,24,25,7,12,17,26,5,27,7,16,28,10,5,29,30,31,13,21,5,32,33,34,17,10,5,35,7,12,36,37,24,22,7,16,13,38,5,39,7,12,40,21,41,27,7,42,43,10,5,44,33,12,13,26,5,45,46,16,17,10,24,47,7,48,28,49,5,22,7,34",
			"name": "Let g = A006530(n), the largest prime factor of n. This filter sequence combines (g mod 6), n/g (A052126), and a single bit A319988(n) telling whether the largest prime factor is unitary.",
			"comment": [
				"Restricted growth sequence transform of triple [A010875(A006530(n)), A052126(n), A319988(n)], with a separate value allotted for a(1).",
				"Many of the same comments as given in A319717 apply also here, except for this filter, the \"blind spot\" area (where only unique values are possible for a(n)) is different, and contains at least all numbers in A070003. Because presence of 2 or 3 in the prime factorization of n do not force the value of a(n) unique, this is substantially less lax (i.e., more exact) filter than A319717. Here among the first 100000 terms, only 2393 have a unique value, compared to 74355 in A319717.",
				"For all i, j:",
				"  a(i) = a(j) =\u003e A002324(i) = A002324(j),",
				"  a(i) = a(j) =\u003e A067029(i) = A067029(j),",
				"  a(i) = a(j) =\u003e A071178(i) = A071178(j),",
				"  a(i) = a(j) =\u003e A077462(i) = A077462(j) =\u003e A101296(i) = A101296(j),",
				"  a(i) = a(j) =\u003e A319690(i) = A319690(j)."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A319996/b319996.txt\"\u003eTable of n, a(n) for n = 1..100000\u003c/a\u003e"
			],
			"example": [
				"For n = 15 (3*5) and n = 33 (3*11), the mod 6 residue of the largest prime factor is 5, also in both cases it is unitary (A319988(n) = 1), and the quotient n/A006530(n) is equal, in this case 3. Thus a(15) and a(33) are alloted the same running count (13 in this case) by rgs-transform.",
				"For n = 2275 (5^2 * 7 * 13), n = 3325 (5^2 * 7 * 19), 5425 (5^2 * 7 * 31) and 6475 (5^2 * 7 * 37), the largest prime factor = 1 (mod 6), and A052126(n) = 175, thus these numbers are allotted the same running count (394 in this case) by rgs-transform."
			],
			"program": [
				"(PARI)",
				"up_to = 100000;",
				"rgs_transform(invec) = { my(om = Map(), outvec = vector(length(invec)), u=1); for(i=1, length(invec), if(mapisdefined(om,invec[i]), my(pp = mapget(om, invec[i])); outvec[i] = outvec[pp] , mapput(om,invec[i],i); outvec[i] = u; u++ )); outvec; };",
				"A006530(n) = if(n\u003e1, vecmax(factor(n)[, 1]), 1);",
				"A052126(n) = (n/A006530(n));",
				"A319988(n) = ((n\u003e1)\u0026\u0026(factor(n)[omega(n),2]\u003e1));",
				"A319996aux(n) = if(1==n,0,[A006530(n)%6, A052126(n), A319988(n)]);",
				"v319996 = rgs_transform(vector(up_to,n,A319996aux(n)));",
				"A319996(n) = v319996[n];"
			],
			"xref": [
				"Cf. A006530, A052126, A319988, A319717.",
				"Cf. A007528 (positions of 5's), A002476 (of 7's), A112774 (after its initial term gives the position of 10's in this sequence).",
				"Cf. also A319994 (modulo 4 analog)."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Antti Karttunen_, Oct 05 2018",
			"references": 5,
			"revision": 14,
			"time": "2019-01-19T04:15:43-05:00",
			"created": "2018-10-05T11:11:17-04:00"
		}
	]
}