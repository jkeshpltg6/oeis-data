{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A064538",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 64538,
			"data": "1,2,6,4,30,12,42,24,90,20,66,24,2730,420,90,48,510,180,3990,840,6930,660,690,720,13650,1092,378,56,870,60,14322,7392,117810,7140,210,72,1919190,103740,8190,1680,94710,13860,99330,9240,217350,9660,9870,10080,324870",
			"name": "a(n) is the smallest positive integer such that a(n)*(1^n + 2^n + ... + x^n) is a polynomial in x with integer coefficients.",
			"comment": [
				"a(n) is a multiple of n+1. - _Vladimir Shevelev_, Dec 20 2011",
				"Let P_n(x) = 1^n + 2^n + ... + x^n = Sum_{i=1..n+1}c_i*x^i. Let P^*_n(x) = Sum_{i=1..n+1}(c_i/(i+1))*(x^(i+1)-x). Then b(n) = (n+1)*a(n+1)is the smallest positive integer such that b(n)*P^*_n(x) is a polynomial with integer coefficients. Proof follows from the recursion P_(n+1)(x) = x + (n+1)*P^*_n(x). As a corollary, note that, if p is the maximal prime divisor of a(n), then p\u003c=n+1. - _Vladimir Shevelev_, Dec 21 2011",
				"The recursion P_(n+1)(x) = x + (n+1)*P^*_n(x) is due to Abramovich (1973); see also Shevelev (2007). - _Jonathan Sondow_, Nov 16 2015",
				"The sum S_m(n) = Sum_{k=0..n} k^m can be written as S_m(n) = n(n+1)(2n+1)P_m(n)/a(m) for even m\u003e1, or S_m(n) = n^2*(n+1)^2*P_m(n)/a(m) for odd m\u003e1, where a(m) is the LCM of the denominators of the coefficients of the polynomial P_m/a(m), i.e., the smallest integer such that P_m defined in this way has integer coefficients. (Cf. Michon link.) - _M. F. Hasler_, Mar 10 2013",
				"a(n)/(n+1) is squarefree, by Faulhaber's formula and the von Staudt-Clausen theorem on the denominators of Bernoulli numbers. - _Kieren MacMillan_ and _Jonathan Sondow_, Nov 20 2015",
				"a(n) equals n+1 times the product of the primes p \u003c= (n+2)/(2+(n mod 2)) such that the sum of the base-p digits of n+1 is at least p. - Bernd C. Kellner and _Jonathan Sondow_, May 24 2017"
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprints), p. 804, Eq. 23.1.4."
			],
			"link": [
				"Chai Wah Wu, \u003ca href=\"/A064538/b064538.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e (n = 0..1000 from T. D. Noe)",
				"V. S. Abramovich, \u003ca href=\"http://kvant.mccme.ru/1973/05/summy_odinakovyh_stepenej_natu.htm\"\u003ePower sums of natural numbers\u003c/a\u003e, Kvant 5 (1973), 22-25. (in Russian)",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"Bernd C. Kellner, \u003ca href=\"https://arxiv.org/abs/1705.04303\"\u003eOn a product of certain primes\u003c/a\u003e, arXiv:1705.04303 [math.NT] 2017, J. Number Theory, 179 (2017), 126-141.",
				"Bernd C. Kellner, Jonathan Sondow, Power-Sum Denominators, Amer. Math. Monthly, 124 (2017), 695-709. doi:\u003ca href=\"https://doi.org/10.4169/amer.math.monthly.124.8.695\"\u003e10.4169/amer.math.monthly.124.8.695\u003c/a\u003e, arXiv:\u003ca href=\"https://arxiv.org/abs/1705.03857\"\u003e1705.03857\u003c/a\u003e",
				"Bernd C. Kellner and Jonathan Sondow, \u003ca href=\"https://arxiv.org/abs/1705.05331\"\u003eThe denominators of power sums of arithmetic progressions\u003c/a\u003e, arXiv:1705.05331 [math.NT], 2017, Integers, 18 (2018), article A95.",
				"Dr. Math, \u003ca href=\"http://mathforum.org/dr.math/problems/kijjaz11.24.98.html\"\u003eSumming n^k\u003c/a\u003e.",
				"R. Mestrovic, \u003ca href=\"http://arxiv.org/abs/1211.4570\"\u003eA congruence modulo n^3 involving two consecutive sums of powers and its applications\u003c/a\u003e, arXiv:1211.4570 [math.NT], 2012.",
				"R. Mestrovic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Mestrovic/mes4.html\"\u003eOn a Congruence Modulo n^3 Involving Two Consecutive Sums of Powers\u003c/a\u003e, Journal of Integer Sequences, Vol. 17 (2014), 14.8.4.",
				"G. Michon, \u003ca href=\"http://www.numericana.com/answer/numbers.htm#faulhaber\"\u003eFaulhaber's Formula\u003c/a\u003e on NUMERICANA.com.",
				"E. S. Rowland, \u003ca href=\"https://ericrowland.github.io/investigations/sumsofpowers.html\"\u003eSums of Consecutive Powers\u003c/a\u003e",
				"V. Shevelev, \u003ca href=\"http://arxiv.org/abs/0711.3692\"\u003eA Short Proof of a Known Relation for Consecutive Power Sums\u003c/a\u003e, arXiv:0711.3692 [math.CA], 2007.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PowerSum.html\"\u003ePower Sum\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Faulhaber\u0026#39;s_formula\"\u003eFaulhaber's Formula\u003c/a\u003e."
			],
			"formula": [
				"a(n) = (n+1)*A195441(n). - _Jonathan Sondow_, Nov 12 2015",
				"A001221(a(n)/(n+1)) = A001222(a(n)/(n+1)). - _Kieren MacMillan_ and _Jonathan Sondow_, Nov 20 2015"
			],
			"example": [
				"1^3 + 2^3 + ... + x^3 = (x(x+1))^2/4 so a(3)=4.",
				"1^4 + 2^4 + ... + x^4 = x(x+1)(2x+1)(3x^2+3x-1)/30, therefore a(4)=30."
			],
			"maple": [
				"A064538 := n -\u003e denom((bernoulli(n+1,x)-bernoulli(n+1))/(n+1)): # _Peter Luschny_, Aug 19 2011",
				"# Formula of Kellner and Sondow (2017):",
				"a := proc(n) local s; s := (p,n) -\u003e add(i,i=convert(n,base,p));",
				"select(isprime,[$2..(n+2)/(2+irem(n,2))]);",
				"(n+1)*mul(i,i=select(p-\u003es(p,n+1)\u003e=p,%)) end: seq(a(n), n=0..48); # _Peter Luschny_, May 14 2017"
			],
			"mathematica": [
				"A064538[n_] := Denominator[ Together[ (BernoulliB[n+1, x] - BernoulliB[n+1])/(n+1)]];",
				"Table[A064538[n], {n, 0, 44}] (* _Jean-François Alcover_, Feb 21 2012, after Maple *)"
			],
			"program": [
				"(PARI) a(n) = {my(vp = Vec(bernpol(n+1, x)-bernfrac(n+1))/(n+1)); lcm(vector(#vp, k, denominator(vp[k])));} \\\\ _Michel Marcus_, Feb 07 2016",
				"(Sage)",
				"A064538 = lambda n: (n+1)*mul([p for p in (2..(n+2)//(2+n%2)) if is_prime(p) and sum((n+1).digits(base=p)) \u003e= p])",
				"print([A064538(n) for n in (0..48)]) # _Peter Luschny_, May 14 2017",
				"(Python)",
				"from __future__ import division",
				"from sympy.ntheory.factor_ import digits, nextprime",
				"def A064538(n):",
				"    p, m = 2, n+1",
				"    while p \u003c= (n+2)//(2+ (n% 2)):",
				"        if sum(d for d in digits(n+1,p)[1:]) \u003e= p:",
				"            m *= p",
				"        p = nextprime(p)",
				"    return m # _Chai Wah Wu_, Mar 07 2018"
			],
			"xref": [
				"Cf. A195441, A256581, A286516, A286762, A286763."
			],
			"keyword": "nonn,nice,look,easy",
			"offset": "0,2",
			"author": "_Floor van Lamoen_, Oct 08 2001",
			"references": 15,
			"revision": 122,
			"time": "2021-03-09T06:04:45-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}