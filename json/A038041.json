{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A038041",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 38041,
			"data": "1,2,2,5,2,27,2,142,282,1073,2,32034,2,136853,1527528,4661087,2,227932993,2,3689854456,36278688162,13749663293,2,14084955889019,5194672859378,7905858780927,2977584150505252,13422745388226152,2,1349877580746537123,2",
			"name": "Number of ways to partition an n-set into subsets of equal size.",
			"comment": [
				"a(n) = 2 iff n is prime with a(p) = card{ 1|2|3|...|p-1|p, 123...p } = 2. - _Bernard Schott_, May 16 2019"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A038041/b038041.txt\"\u003eTable of n, a(n) for n = 1..250\u003c/a\u003e",
				"Gus Wiseman, \u003ca href=\"/A038041/a038041.txt\"\u003eSequences counting and ranking multiset partitions whose part lengths, sums, or averages are constant or strict.\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{d divides n} (n!/(d!*((n/d)!)^d)).",
				"E.g.f.: Sum_{k \u003e= 1} (exp(x^k/k!)-1)."
			],
			"example": [
				"a(4) = card{ 1|2|3|4, 12|34, 14|23, 13|24, 1234 } = 5.",
				"From _Gus Wiseman_, Jul 12 2019: (Start)",
				"The a(6) = 27 set partitions:",
				"  {{1}{2}{3}{4}{5}{6}}  {{12}{34}{56}}  {{123}{456}}  {{123456}}",
				"                        {{12}{35}{46}}  {{124}{356}}",
				"                        {{12}{36}{45}}  {{125}{346}}",
				"                        {{13}{24}{56}}  {{126}{345}}",
				"                        {{13}{25}{46}}  {{134}{256}}",
				"                        {{13}{26}{45}}  {{135}{246}}",
				"                        {{14}{23}{56}}  {{136}{245}}",
				"                        {{14}{25}{36}}  {{145}{236}}",
				"                        {{14}{26}{35}}  {{146}{235}}",
				"                        {{15}{23}{46}}  {{156}{234}}",
				"                        {{15}{24}{36}}",
				"                        {{15}{26}{34}}",
				"                        {{16}{23}{45}}",
				"                        {{16}{24}{35}}",
				"                        {{16}{25}{34}}",
				"(End)"
			],
			"maple": [
				"A038041 := proc(n) local d;",
				"add(n!/(d!*(n/d)!^d), d = numtheory[divisors](n)) end:",
				"seq(A038041(n),n = 1..29); # _Peter Luschny_, Apr 16 2011"
			],
			"mathematica": [
				"a[n_] := Block[{d = Divisors@ n}, Plus @@ (n!/(#! (n/#)!^#) \u0026 /@ d)]; Array[a, 29] (* _Robert G. Wilson v_, Apr 16 2011 *)",
				"Table[Sum[n!/((n/d)!*(d!)^(n/d)), {d, Divisors[n]}], {n, 1, 31}] (* _Emanuele Munarini_, Jan 30 2014 *)",
				"sps[{}]:={{}};sps[set:{i_,___}]:=Join@@Function[s,Prepend[#,s]\u0026/@sps[Complement[set,s]]]/@Cases[Subsets[set],{i,___}];",
				"Table[Length[Select[sps[Range[n]],SameQ@@Length/@#\u0026]],{n,0,8}] (* _Gus Wiseman_, Jul 12 2019 *)"
			],
			"program": [
				"(PARI)  /* compare to A061095 */",
				"mnom(v)=",
				"/* Multinomial coefficient s! / prod(j=1, n, v[j]!) where",
				"  s= sum(j=1, n, v[j]) and n is the number of elements in v[]. */",
				"sum(j=1, #v, v[j])! / prod(j=1, #v, v[j]!)",
				"A038041(n)={local(r=0);fordiv(n,d,r+=mnom(vector(d,j,n/d))/d!);return(r);}",
				"vector(33,n,A038041(n)) /* _Joerg Arndt_, Apr 16 2011 */",
				"(Maxima) a(n):= lsum(n!/((n/d)!*(d!)^(n/d)),d,listify(divisors(n)));",
				"makelist(a(n),n,1,40); /* _Emanuele Munarini_, Feb 03 2014 */"
			],
			"xref": [
				"Cf. A061095 (same but with labeled boxes), A005225, A236696, A055225, A262280, A262320.",
				"Column k=1 of A208437.",
				"Row sums of A200472 and A200473.",
				"Cf. A000110, A007837 (different lengths), A035470 (equal sums), A275780, A317583, A320324, A322794, A326512 (equal averages), A326513."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Christian G. Bower_",
			"ext": [
				"More terms from _Erich Friedman_"
			],
			"references": 97,
			"revision": 66,
			"time": "2019-07-23T04:24:28-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}