{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A197649",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 197649,
			"data": "0,1,7,31,115,390,1254,3893,11789,35045,102695,297516,853932,2432041,6881395,19361995,54214939,151164018,419910354,1162585565,3209268665,8835468881,24266461007,66501634776,181882282200,496539007825,1353272290399,3682496714743",
			"name": "Sum(k*Fibonacci(2*k), k=0..n), Fibonacci(n)=A000045(n)",
			"comment": [
				"There are only a small number of Fibonacci identities that can be solved for n. Some of these are",
				"  1. n = (5*sum(F(2*k-1)^2,k=1..n)-F(4*n))/2 (Vajda #95).",
				"  2. n = (sum(k*F(k),k=0..n)+F(n+3)-2)/F(n+2). (A104286)",
				"  3. n = (a(n)+F(2*n))/F(2*n+1).",
				"  4. n = F(n+4)- 3 - sum(F(k+2)-1,k=0..1) (A001924)",
				"n can also be expressed in terms of phi=(1+sqrt(5))/2:",
				"  5. n = floor(n*phi^3)-floor(2*n*phi).",
				"  6. n = (floor(2*n*phi^2)-floor(2*n*phi))/2."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A197649/b197649.txt\"\u003eTable of n, a(n) for n = 0..2384\u003c/a\u003e",
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/2011.10827\"\u003eNotes on the Hankel transform of linear combinations of consecutive pairs of Catalan numbers\u003c/a\u003e, arXiv:2011.10827 [math.CO], 2020.",
				"E. Pérez Herrero, \u003ca href=\"http://psychedelic-geometry.blogspot.com.es/2010/05/small-fibonacci-sum_13.html\"\u003eA small Fibonacci sum\u003c/a\u003e, Psychedelic Geometry Blogspot",
				"\u003ca href=\"/index/Rec\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (6,-11,6,-1)."
			],
			"formula": [
				"a(n) = n*F(2*n+1)-F(2*n), For F(n)= Fibonacci(n)",
				"a(n) = ((F(2*n+1)*((n-1)*h(n-1)-(n-1)*h(n-2))-h(n)*F(2*n))/h(n), n\u003e2. Where h(n)=n-th harmonic number",
				"G.f.  x*(1+x) / ( (x^2-3*x+1)^2 ). - _R. J. Mathar_, Oct 17 2011",
				"a(n) = A001871(n-1)+A001871(n-2). - _R. J. Mathar_, Oct 17 2011"
			],
			"maple": [
				"a:=n-\u003esum(k*fibonacci(2*k),n= 0..n):seq(a(n), n=0..25);"
			],
			"mathematica": [
				"Table[Sum[k*Fibonacci[2*k], {k, 0, n}], {n, 0, 50}] (* _T. D. Noe_, Oct 17 2011 *)"
			],
			"xref": [
				"Cf. A023619 (inverse binomial transform)"
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Gary Detlefs_, Oct 16 2011",
			"ext": [
				"Identity 4 added Dec 22 2012 by Gary Detlefs"
			],
			"references": 4,
			"revision": 31,
			"time": "2021-03-11T20:42:54-05:00",
			"created": "2011-10-17T01:51:56-04:00"
		}
	]
}