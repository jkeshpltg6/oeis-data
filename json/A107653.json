{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A107653",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 107653,
			"data": "1,-6,21,-68,198,-510,1248,-2904,6393,-13604,28044,-55956,108982,-207552,386622,-707216,1271970,-2250582,3925780,-6757272,11483232,-19290824,32057352,-52722744,85884503,-138644292,221885805,-352241792,554892894",
			"name": "Expansion of q / (chi(q) * chi(q^3))^6 in powers of q where chi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A107653/b107653.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Convolution inverse of A186829. - _Michael Somos_, Feb 27 2011",
				"Expansion of (eta(q) * eta(q^3) * eta(q^4) * eta(q^12) / (eta(q^2) * eta(q^6))^2)^6 in powers of q.",
				"Euler transform of period 12 sequence [-6, 6, -12, 0, -6, 12, -6, 0, -12, 6, -6, 0, ...]. - _Michael Somos_, Jun 13 2005",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (12 t)) = f(t) where q = exp(2 Pi i t). - _Michael Somos_, Feb 27 2011",
				"G.f.: x * (Product_{k\u003e0} ((1 + x^(2*k)) * (1 + x^(6*k))) / ((1 + x^k) * (1 + x^(3*k))))^6 =  x * (Product_{k\u003e0} (1 + x^(2*k-1)) * (1 + x^(6*k-3)))^-6.",
				"a(n) = -(-1)^n * A123653(n). - _Michael Somos_, Feb 27 2011"
			],
			"example": [
				"G.f. = q - 6*q^2 + 21*q^3 - 68*q^4 + 198*q^5 - 510*q^6 + 1248*q^7 + ..."
			],
			"mathematica": [
				"QP = QPochhammer; s = (QP[q]*QP[q^3]*QP[q^4]*(QP[q^12]/(QP[q^2]*QP[q^6])^2 ))^6 + O[q]^30; CoefficientList[s, q] (* _Jean-François Alcover_, Nov 14 2015, adapted from PARI *)",
				"a[n_]:= SeriesCoefficient[q/( QPochhammer[-q, q^2]* QPochhammer[-q^3, q^6])^6, {q, 0, n}]; Table[a[n], {n, 1, 50}] (* _G. C. Greubel_, Dec 09 2017 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c1, 0, n--; A = x * O(x^n); polcoeff( (eta(x + A) * eta(x^3 + A) * eta(x^4 + A) * eta(x^12 + A) / (eta(x^2 + A)^2 * eta(x^6 + A)^2))^6, n))}; /* _Michael Somos_, Jun 13 2005 */"
			],
			"xref": [
				"Cf. A123653, A186829."
			],
			"keyword": "sign",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Jun 07 2005",
			"ext": [
				"Revised by _Michael Somos_, Jun 12 2005"
			],
			"references": 4,
			"revision": 22,
			"time": "2021-03-12T22:24:43-05:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}