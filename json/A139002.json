{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A139002",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 139002,
			"data": "1,1,1,1,1,1,3,1,1,3,1,1,4,4,3,6,1,1,6,3,4,4,1,1,3,1,5,15,5,5,10,10,10,10,15,10,1,1,10,15,10,10,10,10,5,5,15,5,1,3,1,1,4,4,3,6,1,6,36,18,24,24,6,6,18,6,15,15,45,45,15,15,15,15,10,20,60,20,10,60,20,15,45,15,1,1",
			"name": "Weights of Connes-Moscovici Hopf subalgebra.",
			"comment": [
				"Gives multiplicity for tree shapes in \"naturally grown\" forests of rooted trees (from file for CM(t) referred to in Broadhurst).",
				"A refinement of the enumeration of the trees of the first few forests in terms of planar rather than nonplanar rooted trees is presented on p. 21 of the Munthe-Kaas and Lundervold paper. - _Tom Copeland_, Jul 16 2018 (The refinement is presented also in Lundervold and on p. 35 of the Lundervold and Munthe-Kaas paper. - _Tom Copeland_, Jul 21 2021)",
				"Enumerates the elementary differentials of the Butcher group that Cayley showed are in bijection with these nonplanar rooted trees when considering multivariable vector functions. When considering a scalar function of one independent variable, the associated differentials are no longer in bijection with the planar trees and are enumerated by A139605. Two nonplanar trees are considered equivalent if the branches of one may be rotated about its nodes to match those of the other. - _Tom Copeland_, Jul 21 2021"
			],
			"reference": [
				"J. Butcher, Numerical Methods for Ordinary Differential Equations, 3rd Ed., Wiley, 2016, Table 310(II) on p. 165.",
				"E. Hairer, C. Lubich, G. Wanner, Geometric Numerical Integration - Structure-Preserving Algorithms for Ordinary Differential Equations, 2nd Ed., Springer, 2006, pp. 52 and 53."
			],
			"link": [
				"S. Agarwala and C. Delaney, \u003ca href=\"http://arxiv.org/abs/1302.4004\"\u003eGeneralizing the Connes-Moscovici Hopf algebra to include all rooted trees\u003c/a\u003e, arXiv:1302.4004 [math-ph], 2015, pp. 2, 9, and 21.",
				"C. Bergbauer and D. Kreimer, \u003ca href=\"https://arxiv.org/pdf/hep-th/0506190.pdf\"\u003eHopf algebras in renormalization theory: Locality and Dyson-Schwinger equations from Hochschild cohomology\u003c/a\u003e, arXiv:hep-th/0506190 [hep-th], 2006, p. 14.",
				"D. J. Broadhurst and D. Kreimer, \u003ca href=\"http://arXiv.org/abs/hep-th/9810087\"\u003eRenormalization automated by Hopf algebra\u003c/a\u003e, arXiv:hep-th/9810087 [hep-th], 1998.",
				"C. Brouder, \u003ca href=\"http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.180.7535\u0026amp;rep=rep1\u0026amp;type=pdf\"\u003eTrees, renormalization, and differential equations\u003c/a\u003e, BIT Numerical Mathematics, 44: 425-438, 2004, p. 434.",
				"F. Chapoton, \u003ca href=\"http://arxiv.org/abs/math/0209104\"\u003eRooted trees and an exponential-like series\u003c/a\u003e, arXiv:math/0209104 [math.QA], 2002.",
				"A. Connes and D. Kreimer, \u003ca href=\"https://arxiv.org/abs/hep-th/9808042\"\u003eHopf algebras, renormalization, and noncommutative geometry\u003c/a\u003e, arXiv:hep-th/9808042 [hep-th], 1998, pp. 21 and 28.",
				"Tom Copeland, \u003ca href=\"http://tcjpn.spaces.live.com/default.aspx\"\u003eA Walk in the Woods with Cayley and Comtet\u003c/a\u003e, 2008.",
				"Tom Copeland, \u003ca href=\"http://tcjpn.wordpress.com/2008/06/12/mathemagical-forests/\"\u003eMathemagical Forests\u003c/a\u003e, 2008.",
				"L. Foissy, \u003ca href=\"http://arXiv.org/abs/0707.1204\"\u003eFaa di Bruno subalgebras of the Hopf algebra of planar trees from combinatorial Dyson-Schwinger equations\u003c/a\u003e, arXiv:0707.1204 [math.RA], 2007, p. 2.",
				"E. Getzler, \u003ca href=\"https://arxiv.org/abs/math/0404003\"\u003eLie theory for nilpotent L-infinity algebras\u003c/a\u003e, arXiv:math/0404003 [math.AT], 2004-2007, p. 22.",
				"M. Ginocchio, \u003ca href=\"https://doi.org/10.1007/BF00750066\"\u003eUniversal expansion of the powers of a derivation\u003c/a\u003e, Lett. Math. Phys. 34, 343-364, 1995, see table on p. 364.",
				"T. Krajewski and T. Martinetti, \u003ca href=\"https://arxiv.org/abs/0806.4309\"\u003eWilsonian renormalization, differential equations and Hopf algebras\u003c/a\u003e, arXiv:0806.4309 [hep-th], 2008, p. 14.",
				"D. Kreimer, \u003ca href=\"https://www2.mathematik.hu-berlin.de/~kreimer/wp-content/uploads/SkriptRGE.pdf\"\u003eRenormalization and Renormalization Group\u003c/a\u003e, lecture notes by L. Klaczynski of class lectures by Dirk Kreimer, 2012, p. 16.",
				"A. Lundervold, \u003ca href=\"https://alexander.lundervold.com/assets/files/phdthesis.pdf\"\u003eLie-Butcher series and geometric numerical integration on manifolds\u003c/a\u003e, Ph.D. thesis, Dept. of Math., Univ. of Bergen, 2011, pp. 8 and 10.",
				"A. Lundervold and H. Munthe-Kaas, \u003ca href=\"https://arxiv.org/abs/1112.4465\"\u003eOn algebraic structures of numerical integration on vector spaces and manifolds\u003c/a\u003e, arXiv:1112.4465 [math.QA], 2013. p. 6.",
				"Mathoverflow, \u003ca href=\"https://mathoverflow.net/questions/41039/formula-for-n-th-iteration-of-dx-dt-bx\"\u003eFormula for n-th iteration of dx/dt=B(x)\u003c/a\u003e, a question on MathOverflow posed by the user resolvent and answered by Tom Copeland, 2021.",
				"H. Munthe-Kaas and A. Lundervold, \u003ca href=\"https://arxiv.org/abs/1203.4738\"\u003eOn post-Lie algebras, Lie-Butcher series and moving frames\u003c/a\u003e, arXiv:1203.4738 [math.NA], 2013."
			],
			"formula": [
				"The table on p. 364 of Ginocchio contains the Connes-Moscovici weights correlated with associated derivatives D^k. From the relation of this entry to A139605 and A145271, the action of the weighted differentials on an exponential is associated with the operation exp(x g(u)D_u) e^(ut) = e^(t H^{(-1)}(H(u)+x)) with g(x) = 1/D(H(x)) and H^{(-1)} the compositional inverse of H. With H^{(-1)}(x) = -log(1-x), the inverse about x=0 is H(x) = 1-e^(-x), giving g(x) = e^x and the resulting action e^(-t log(1-x)) = (1-x)^(-t) for u=0, an e.g.f. for the unsigned Stirling numbers of the first kind A008275 and A048994. Consequently, summing the Connes-Moscovici weights over each associated derivative gives these Stirling numbers. E.g., the fifth row in the examples reduces to (1+3+1+1) D + (4+4+3) D^2 + 6 D^3 + D^4 = 6 D + 11 D^2 + 6 D^3 + D^4. - _Tom Copeland_, Jul 14 2021"
			],
			"example": [
				"From _Tom Copeland_, Dec 06 2017: (Start)",
				"The number of distinct rooted tree types, or shapes, with n nodes is given by A000081(n+1), so the multiplicities for the tree shapes of the forests of naturally grown trees given here may be grouped according to A000081. For example, A000081(5)=4 corresponds to the four tree types depicted in Fig. 6 of Mathemagical Forests with four nodes, or vertices, with the four multiplicities (1,1,3,1); A000081(4)=2 corresponds to the two tree types depicted in Fig. 3 with three nodes and the two multiplicities (1,1); A000081(3)=1, with one tree type with two nodes and multiplicity (1); and A000081(2)=1, with one tree type with one node and multiplicity (1). Then the sequence here begins (1)(1)(1,1)(1,1,3,1).",
				"First few rows (with last row reordered according to Fig. 7 of Mathemagical Forests):",
				"  1",
				"  1",
				"  1, 1",
				"  1, 1, 3, 1",
				"  1, 1, 3, 4, 4, 3, 6, 1, 1",
				"This last row corresponds to the one listed in Broadhurst as",
				"  1, 3, 1, 1, 4, 4, 3, 6, 1.",
				"(End)"
			],
			"xref": [
				"Cf. A000081, A008275, A048994, A139605, A145271."
			],
			"keyword": "nonn,tabf",
			"offset": "1,7",
			"author": "_Tom Copeland_, May 31 2008",
			"references": 2,
			"revision": 94,
			"time": "2021-08-23T22:34:32-04:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}