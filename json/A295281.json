{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A295281",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 295281,
			"data": "1,1,0,1,1,1,0,0,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,0,1,0,1,1,4,1,0,1,1,1,3,1,1,1,1,1,4,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,9,1,1,1,0,1,4,1,1,1,4,1,6,1,1,1,1,1,4,1,1,0,1,1,9,1,1,1,1,1,9,1",
			"name": "Number of complete strict tree-factorizations of n \u003e 1.",
			"comment": [
				"A strict tree-factorization (see A295279 for definition) is complete if its leaves are all prime numbers.",
				"From _Andrew Howroyd_, Nov 18 2018: (Start)",
				"a(n) depends only on the prime signature of n.",
				"This sequence is very similar but not identical to the number of complete orderless identity tree-factorizations of n. The first difference is at n=900 (square of three primes). Here a(n) = 191 whereas the other sequence would have 197. (End)"
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A295281/b295281.txt\"\u003eTable of n, a(n) for n = 2..10000\u003c/a\u003e"
			],
			"formula": [
				"a(product of n distinct primes) = A000311(n).",
				"Positions of zeros are proper prime powers A025475. Positions of nonzero entries are A085971."
			],
			"example": [
				"The a(72) = 6 complete strict tree-factorizations are: 2*3*(2*(2*3)), 2*(2*3*(2*3)), 2*(2*(3*(2*3))), 2*(3*(2*(2*3))), 3*(2*(2*(2*3))), (2*3)*(2*(2*3))."
			],
			"mathematica": [
				"postfacs[n_]:=If[n\u003c=1,{{}},Join@@Table[Map[Prepend[#,d]\u0026,Select[postfacs[n/d],Min@@#\u003e=d\u0026]],{d,Rest[Divisors[n]]}]];",
				"sftc[n_]:=Prepend[Join@@Function[fac,Tuples[sftc/@fac]]/@Select[postfacs[n],And[Length[#]\u003e1,UnsameQ@@#]\u0026],n];",
				"Table[Length[Select[sftc[n],FreeQ[#,_Integer?(!PrimeQ[#]\u0026)]\u0026]],{n,2,100}]"
			],
			"program": [
				"(PARI) seq(n)={my(v=vector(n), w=vector(n)); v[1]=1; for(k=2, n, w[k]=v[k]+isprime(k); forstep(j=n\\k*k, k, -k, v[j]+=w[k]*v[j/k])); w[2..n]} \\\\ _Andrew Howroyd_, Nov 18 2018"
			],
			"xref": [
				"Cf. A000311, A025475, A085971, A273873, A281113 A281118, A281119, A292505, A295279."
			],
			"keyword": "nonn",
			"offset": "2,29",
			"author": "_Gus Wiseman_, Nov 19 2017",
			"references": 7,
			"revision": 14,
			"time": "2018-11-19T03:11:43-05:00",
			"created": "2017-11-22T03:14:42-05:00"
		}
	]
}