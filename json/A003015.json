{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003015",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3015,
			"id": "M5374",
			"data": "1,120,210,1540,3003,7140,11628,24310,61218182743304701891431482520",
			"name": "Numbers that occur 5 or more times in Pascal's triangle.",
			"comment": [
				"The subject of a recent thread on sci.math. Apparently it has been known for many years that there are infinitely many numbers that occur at least 6 times in Pascal's triangle, namely the solutions to binomial(n,m-1) = binomial{n-1,m) given by n = F_{2k}F_{2k+1}; m = F_{2k-1}F_{2k} where F_i is the i-th Fibonacci number. The first of these outside the range of the existing database entry is {104 choose 39} = {103 choose 40} = 61218182743304701891431482520. - _Christopher E. Thompson_, Mar 09 2001",
				"It may be that there are no terms that appear exactly 5 times in Pascal's triangle, in which case the title could be changed to \"Numbers that occur 6 or more times in Pascal's triangle\". - _N. J. A. Sloane_, Nov 24 2004",
				"No other terms below 33*10^16 (_David W. Wilson_).",
				"61218182743304701891431482520 really is the next term. Weger shows this and I checked it. - _T. D. Noe_, Nov 15 2004",
				"Blokhuis et al. show that there are no other solutions less than 10^60, nor within the first 10^6 rows of Pascal's triangle other than those given by the parametric solution mentioned above. - _Christopher E. Thompson_, Jan 19 2018",
				"See the b-file of A090162 for the explicit numbers produced by the parametric formula. - _Jeppe Stig Nielsen_, Aug 23 2020"
			],
			"reference": [
				"L. Comtet, Advanced Combinatorics, Reidel, 1974, p. 93, #47.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Aart Blokhuis, Andries Brouwer, and Benne de Weger, \u003ca href=\"http://math.colgate.edu/~integers/vol17.html\"\u003eBinomial collisions and near collisions\u003c/a\u003e, INTEGERS, Volume 17, Article A64, 2017 (also available as \u003ca href=\"https://arxiv.org/abs/1707.06893\"\u003earXiv:1707.06893 [math.NT]\u003c/a\u003e).",
				"Jean-Marie de Koninck, Nicolas Doyon, and William Verreault, \u003ca href=\"http://math.colgate.edu/~integers/v34/v34.mail.html\"\u003eRepetitions of multinomial coefficients and a generalization of Singmaster's conjecture\u003c/a\u003e, Integers (2021) Vol. 21, #A34.",
				"B. M. M. de Weger, \u003ca href=\"http://hdl.handle.net/1765/1356\"\u003eEqual binomial coefficients: some elementary considerations\u003c/a\u003e, Econometric Institute Research Papers, No. EI 9536-/B, 1995.",
				"B. M. M. de Weger, \u003ca href=\"http://dx.doi.org/10.1006/jnth.1997.2109\"\u003eEqual binomial coefficients: some elementary considerations\u003c/a\u003e, Journal of Number Theory, Volume 63, Issue 2, April 1997, Pages 373-386.",
				"Zoe Griffiths, \u003ca href=\"https://www.youtube.com/watch?v=Z3xq4ODNeZs\"\u003eMy MegaFavNumber: 61,218,182,743,304,701,891,431,482,520\u003c/a\u003e, YouTube video (2020).",
				"R. K. Guy and V. Klee, \u003ca href=\"http://www.jstor.org/stable/2316321\"\u003eMonthly research problems\u003c/a\u003e, 1969-1971, Amer. Math. Monthly, 78 (1971), 1113-1122.",
				"D. A. Lind, \u003ca href=\"https://www.fq.math.ca/Scanned/6-3/6-3/lind.pdf\"\u003eThe quadratic field Q(sqrt(5)) and a certain diophantine equation\u003c/a\u003e, Fibonacci Quart. 6 (3) (1968), 86-93.",
				"Kaisa Matomäki, Maksym Radziwill, Xuancheng Shao, Terence Tao, and Joni Teräväinen, \u003ca href=\"https://arxiv.org/abs/2106.03335\"\u003eSingmaster's conjecture in the interior of Pascal's triangle\u003c/a\u003e, arXiv:2106.03335 [math.NT], 2021.",
				"David Singmaster, \u003ca href=\"http://www.fq.math.ca/Scanned/13-4/singmaster.pdf\"\u003eRepeated binomial coefficients and Fibonacci numbers\u003c/a\u003e, Fibonacci Quarterly 13 (1975) 295-298.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PascalsTriangle.html\"\u003ePascal's Triangle\u003c/a\u003e",
				"Tomohiro Yamada, \u003ca href=\"https://arxiv.org/abs/2002.07043\"\u003eNecessary conditions for binomial collisions\u003c/a\u003e, arXiv:2002.07043 [math.NT], 2020."
			],
			"xref": [
				"Cf. A003016, A059233.",
				"Cf. A182237, A098565 (subsequence).",
				"Cf. A090162 (easy subsequence)."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"references": 16,
			"revision": 69,
			"time": "2021-06-08T07:30:12-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}