{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097778",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97778,
			"data": "1,23,528,12121,278255,6387744,146639857,3366328967,77278926384,1774048977865,40725847564511,934920445005888,21462444387570913,492701300469125111,11310667466402306640,259652650426783927609",
			"name": "Chebyshev polynomials S(n,23) with Diophantine property.",
			"comment": [
				"All positive integer solutions of Pell equation b(n)^2 - 525*a(n)^2 = +4 together with b(n)=A090731(n+1), n\u003e=0. Note that D=525=21*5^2 is not squarefree.",
				"For positive n, a(n) equals the permanent of the n X n tridiagonal matrix with 23's along the main diagonal, and i's along the superdiagonal and the subdiagonal (i is the imaginary unit). - _John M. Campbell_, Jul 08 2011",
				"For n\u003e=1, a(n) equals the number of 01-avoiding words of length n-1 on alphabet {0,1,...,22}. - _Milan Janjic_, Jan 25 2015"
			],
			"link": [
				"Indranil Ghosh, \u003ca href=\"/A097778/b097778.txt\"\u003eTable of n, a(n) for n = 0..733\u003c/a\u003e",
				"R. Flórez, R. A. Higuita, A. Mukherjee, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Mukherjee/mukh2.html\"\u003eAlternating Sums in the Hosoya Polynomial Triangle\u003c/a\u003e, Article 14.9.5 Journal of Integer Sequences, Vol. 17 (2014).",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences relate d to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (23,-1)."
			],
			"formula": [
				"a(n) = S(n, 23) = U(n, 23/2) = S(2*n+1, sqrt(25))/sqrt(25) with S(n, x)=U(n, x/2) Chebyshev's polynomials of the 2nd kind, A049310. S(-1, x)= 0 = U(-1, x).",
				"a(n) = 23*a(n-1)-a(n-2), n \u003e= 1; a(0)=1, a(1)=23; a(-1)=0.",
				"a(n) = (ap^(n+1) - am^(n+1))/(ap-am) with ap := (23+5*sqrt(21))/2 and am := (23-5*sqrt(21))/2.",
				"G.f.: 1/(1-23*x+x^2).",
				"a(n) = Sum_{k, 0\u003c=k\u003c=n} A101950(n,k)*22^k. - _Philippe Deléham_, Feb 10 2012",
				"Product {n \u003e= 0} (1 + 1/a(n)) = 1/21*(21 + 5*sqrt(21)). - _Peter Bala_, Dec 23 2012",
				"Product {n \u003e= 1} (1 - 1/a(n)) = 1/46*(21 + 5*sqrt(21)). - _Peter Bala_, Dec 23 2012"
			],
			"example": [
				"(x,y) = (23;1), (527;23), (12098;528), ... give the positive integer solutions to x^2 - 21*(5*y)^2 =+4."
			],
			"mathematica": [
				"LinearRecurrence[{23,-1},{1,23},20] (* _Harvey P. Dale_, May 06 2016 *)"
			],
			"program": [
				"(Sage) [lucas_number1(n,23,1) for n in range(1,20)] # _Zerinvary Lajos_, Jun 25 2008"
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Aug 31 2004",
			"references": 3,
			"revision": 44,
			"time": "2019-12-07T12:18:24-05:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}