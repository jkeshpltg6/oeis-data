{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001639",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1639,
			"id": "M3353 N1349",
			"data": "1,1,4,9,16,22,36,65,112,186,309,522,885,1492,2509,4225,7124,12010,20236,34094,57453,96823,163163,274946,463316,780755,1315687,2217112,3736129,6295887,10609441,17878369,30127497,50768954,85552651,144167958,242942778",
			"name": "A Fielder sequence. a(n) = a(n-1) + a(n-3) + a(n-4) + a(n-5), n \u003e= 6.",
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A001639/b001639.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Daniel C. Fielder, \u003ca href=\"https://www.fq.math.ca/Scanned/6-3/fielder.pdf\"\u003eSpecial integer sequences controlled by three parameters\u003c/a\u003e, Fibonacci Quarterly 6, 1968, 64-70.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1, 0, 1, 1, 1)."
			],
			"formula": [
				"G.f.: x*(1+3*x^2+4*x^3+5*x^4)/(1-x-x^3-x^4-x^5)."
			],
			"maple": [
				"A001639:=-(1+3*z**2+4*z**3+5*z**4)/(-1+z+z**3+z**4+z**5); # conjectured by _Simon Plouffe_ in his 1992 dissertation"
			],
			"mathematica": [
				"Drop[CoefficientList[Series[x*(1+3*x^2+4*x^3+5*x^4)/(1-x-x^3-x^4-x^5),{x,0,40}], x], 1] (* _Stefan Steinerberger_, Apr 10 2006 *)",
				"LinearRecurrence[{1,0,1,1,1}, {1,1,4,9,16}, 30] (* _G. C. Greubel_, Jan 09 2018 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c0,0,polcoeff(x*(1+3*x^2+4*x^3+5*x^4)/(1-x-x^3-x^4-x^5)+x*O(x^n),n))",
				"(MAGMA) I:=[1, 1, 4, 9, 16]; [n le 5 select I[n] else Self(n-1) + Self(n-3) + Self(n-4) + Self(n-5): n in [1..30]]; // _G. C. Greubel_, Jan 09 2018"
			],
			"xref": [
				"Cf. A000570."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Edited by _Michael Somos_, Feb 17 2002"
			],
			"references": 1,
			"revision": 42,
			"time": "2021-03-12T22:32:35-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}