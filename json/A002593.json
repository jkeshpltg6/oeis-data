{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002593",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2593,
			"id": "M5199 N2262",
			"data": "0,1,28,153,496,1225,2556,4753,8128,13041,19900,29161,41328,56953,76636,101025,130816,166753,209628,260281,319600,388521,468028,559153,662976,780625,913276,1062153,1228528,1413721,1619100,1846081",
			"name": "a(n) = n^2*(2*n^2 - 1); also Sum_{k=0..n-1} (2k+1)^3.",
			"comment": [
				"The m-th term, for m = A065549(n), is perfect (A000396). - _Lekraj Beedassy_, Jun 04 2002",
				"Partial sums of A016755. - _Lekraj Beedassy_, Jan 06 2004",
				"Also, the k-th triangular number, where k = 2n^2 - 1 = A056220(n), i.e., a(n) = A000217(A056220(n)). - _Lekraj Beedassy_, Jun 11 2004",
				"Also, the j-th hexagonal number, where j = n^2 = A000290(n), i.e., a(n) = A000384(A000290(n)) and a(n) = A056220(n) * A000290(n) or j * k. This sequence is a subsequence of the hexagonal number sequence and retains the aspect intrinsic to the hexagonal number sequence that each number in this sequence can be found by multiplying its triangular number by its hexagonal number. - _Bruce J. Nicholson_, Aug 22 2017",
				"Odd numbers and their squares both having the form 2x-+1, we may write (2r+1)^3 = (2r+1)*(2s-1), where s = centered squares = (r+1)^2 + r^2. Since 2r+1 = (r+1)^2 - r^2, it follows immediately from summing telescopingly over n-1, the product 2*{(r+1)^4 - r^4} - {(r+1)^2 - r^2}, that Sum_{r=0..n-1} (2r+1)^3 = 2*n^4 - n^2 = n^2*(2n^2 - 1). - _Lekraj Beedassy_, Jun 16 2004",
				"a(n) is also the starting term in the sum of a number M(n) of consecutive cubed integers equaling a squared integer (A253724) for M(n) equal to twice a squared integer (A001105). Numbers a(n) such that a^3 + (a+1)^3 + ... + (a+M-1)^3 = c^2 has nontrivial solutions over the integers for M equal to twice a squared integer (A001105). If M is twice a squared integer, there always exists at least one nontrivial solution for the sum of M consecutive cubed integers starting from a^3 and equaling a squared integer c^2. For n \u003e= 1, M(n) = 2n^2 (A001105), a(n) = M(M-1)/2 = n^2(2n^2 - 1), and c(n) = sqrt(M/2) (M(M^2-1)/2) = n^3(4n^4 - 1). The trivial solutions with M \u003c 1 and a \u003c 2 are not considered. - _Vladimir Pletser_, Jan 10 2015",
				"Binomial transform of the sequence with offset 1 is (1, 27, 98, 120, 48, 0, 0, 0, ...). - _Gary W. Adamson_, Jul 23 2015"
			],
			"reference": [
				"L. Comtet, Advanced Combinatorics, Reidel, 1974, p. 169, #31.",
				"F. E. Croxton and D. J. Cowden, Applied General Statistics. 2nd ed., Prentice-Hall, Englewood Cliffs, NJ, 1955, p. 742.",
				"L. B. W. Jolley, Summation of Series. 2nd ed., Dover, NY, 1961, p. 7.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A002593/b002593.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"F. E. Croxton and D. J. Cowden, \u003ca href=\"/A000447/a000447.pdf\"\u003eApplied General Statistics\u003c/a\u003e, 2nd Ed., Prentice-Hall, Englewood Cliffs, NJ, 1955 [Annotated scans of just pages 742-743]",
				"Vladimir Pletser, \u003ca href=\"/A253724/a253724.txt\"\u003eFile Triplets (M,a,c) for M=2n^2\u003c/a\u003e",
				"V. Pletser, \u003ca href=\"http://arxiv.org/abs/1501.06098\"\u003eGeneral solutions of sums of consecutive cubed integers equal to squared integers\u003c/a\u003e, arXiv:1501.06098 [math.NT], 2015.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"G. Xiao, Sigma Server, \u003ca href=\"http://wims.unice.fr/~wims/en_tool~analysis~sigma.en.html\"\u003eOperate on \"(2*n-1)^3\"\u003c/a\u003e",
				"R. J. Stroeker, \u003ca href=\"http://www.numdam.org/item?id=CM_1995__97_1-2_295_0\"\u003eOn the sum of consecutive cubes being a perfect square\u003c/a\u003e, Compositio Mathematica, 97 no. 1-2 (1995), pp. 295-307.",
				"M. J. Zerger, \u003ca href=\"http://www.jstor.org/stable/2690925\"\u003eProof without words: The sum of consecutive odd cubes is a triangular number\u003c/a\u003e, Math. Mag., 68 (1995), 371.",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (5,-10,10,-5,1)."
			],
			"formula": [
				"G.f.: (-x^4 - 23*x^3 - 23*x^2 - x)/(x - 1)^5. - _Harvey P. Dale_, Mar 28 2011",
				"a(n) = n^2*(2n^2 - 1). - _Vladimir Pletser_, Jan 10 2015",
				"E.g.f.: exp(x)*x*(1 + 13*x + 24*x^2/2! + 12*x^3/3!). - _Wolfdieter Lang_, Mar 11 2017"
			],
			"maple": [
				"A002593:=-z*(z+1)*(z**2+22*z+1)/(z-1)**5; # conjectured by _Simon Plouffe_ in his 1992 dissertation",
				"a:= n-\u003e n^2*(2*n^2-1): seq(a(n), n=0..50);  # _Vladimir Pletser_, Jan 10 2015"
			],
			"mathematica": [
				"CoefficientList[Series[(-x^4-23x^3-23x^2-x)/(x-1)^5,{x,0, 80}],x]  (* or *)",
				"Table[ n^2 (2n^2-1),{n,0,80}]  (* _Harvey P. Dale_, Mar 28 2011 *)",
				"Join[{0},Accumulate[Range[1,91,2]^3]] (* or *) LinearRecurrence[{5,-10,10,-5,1},{0,1,28,153,496},40] (* _Harvey P. Dale_, Mar 22 2017 *)"
			],
			"program": [
				"(MAGMA) [n^2*(2*n^2 - 1): n in [0..40]]; // _Vincenzo Librandi_, Sep 07 2011",
				"(PARI) a(n) = n^2*(2*n^2 - 1) \\\\ _Charles R Greathouse IV_, Feb 07 2017"
			],
			"xref": [
				"Cf. A000290, A000384, A000447, A000583, A002309, A253724, A253725, A260810."
			],
			"keyword": "nonn,nice,easy",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"references": 15,
			"revision": 144,
			"time": "2021-05-08T23:30:20-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}