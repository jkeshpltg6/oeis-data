{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A286328",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 286328,
			"data": "4,3,24,60,14,9,180,264,20,480,19,84,924,1104,51,1740,155,2244,2520,2664,3120,3444,99,51,51,5304,5724,65,399,8064,8580,9384,9660,221,11400,12324,13284,13944,14964,16020,819,18240,194,99,19800,22260,24864,25764,26220",
			"name": "Least integer k such that the area of the triangle (prime(n), k, k+1) is integer.",
			"comment": [
				"The area A of a triangle whose sides have lengths a, b, and c is given by Heron's formula : A = sqrt(s*(s-a)*(s-b)*(s-c)), where s = (a+b+c)/2.",
				"The corresponding areas are 6, 6, 84, 330, 84, 36, 1710, 3036, 210,...",
				"The following table gives the first values of n, the sides (prime(n), k, k+1) and the area A of each triangle.",
				"+-----+---------+------+------+-------+",
				"|  n  | prime(n)|   k  |  k+1 |    A  |",
				"+-----+---------+------+------+-------+",
				"|  2  |    3    |   4  |   5  |    6  |",
				"|  3  |    5    |   3  |   4  |    6  |",
				"|  4  |    7    |  24  |  25  |   84  |",
				"|  5  |   11    |  60  |  61  |  330  |",
				"|  6  |   13    |  14  |  15  |   84  |",
				"|  7  |   17    |   9  |  10  |   36  |",
				"|  8  |   19    | 180  |  181 | 1710  |",
				"|  9  |   23    | 264  |  265 | 3036  |",
				"| 10  |   29    |  20  |   21 |  210  |",
				".......................................",
				"We observe triangles of sides (prime(m), prime(m)+ 1, prime(m)+ 2)) = (3, 4, 5), (13, 14, 15), (193, 194, 195), (37633, 37634, 37635), ... with the corresponding areas 6, 84, 16296, 613283664, ... (subsequence of A011945).",
				"We observe Pythagorean triangles for n = 2, 3, 4, 5, 8, 9, 10, ....",
				"In this case, if prime(n) \u003c k, the numbers k of the sequence such that prime(n) = sqrt(2k+1) are given by the numbers {4, 24, 60, 180, 264, ...}, subsequence of {A084921} = {4, 12, 24, 60, 84, 144, 180, 264, ...}. If prime(n) \u003e k, the numbers k of the sequence such that prime(n) = sqrt(2k^2+2k+1) are given by the numbers 3, 20, 4059, 23660, ....",
				"From _Chai Wah Wu_, May 15 2017: (Start)",
				"Assumes triangle has positive area.",
				"Let p = prime(n). Then",
				"(p+1)/2 \u003c= a(n) \u003c= (p^2-1)/2.",
				"a(n) = (p+1)/2 if n \u003e 1 is a term in A062325, i.e. p is of the form m^2+1 (A002496); otherwise, a(n) \u003e (p+1)/2.",
				"a(n) is the smallest k \u003e= (p+1)/2 such that sum_{i=(p+1)/2}^{k} i*(p^2-1)/2 is a square.",
				"These statements follow from the fact that the area of a triangle with sides of length p, k and k+1 is equal to (p^2-1)*((2k+1)^2-p^2)/16.",
				"(End)"
			],
			"link": [
				"Chai Wah Wu, \u003ca href=\"/A286328/b286328.txt\"\u003eTable of n, a(n) for n = 2..1000\u003c/a\u003e"
			],
			"example": [
				"a(4) = 24 because the area of the triangle (prime(4), 24, 25) = (7, 24, 25) = sqrt(28*(28-7)*(28-24)*(28-25)) = 84, where the semiperimeter 28 = (7+24+25)/2."
			],
			"maple": [
				"nn:=10^7:",
				"for n from 2 to 50 do:",
				"a:=ithprime(n):ii:=0:",
				"for k from 1 to nn while(ii=0) do:",
				"p:=(a+2*k+1)/2:q:=p*(p-a)*(p-k)*(p-k-1):",
				"if q\u003e0 and floor(sqrt(q))=sqrt(q) then",
				"       ii:=1: printf(`%d, `,k):",
				"      else",
				"      fi:",
				"     od:",
				"    od:"
			],
			"mathematica": [
				"Do[kk=0;Do[s=(Prime[n]+2k+1)/2;If[IntegerQ[s],area2=s(s-Prime[n])(s-k)(s-k-1);If[area2\u003e0\u0026\u0026kk==0\u0026\u0026IntegerQ[Sqrt[area2]],Print[n,\" \",k];kk=1]],{k,1,3*10^4}],{n,2,10}] (* or *)",
				"a[n_] := Block[{p = Prime@n, k}, k = (p + 1)/2; While[! IntegerQ@ Sqrt[(4 k^2 - p^2 + 4 k + 1) (p^2 - 1)/16], k++]; k]; a /@ Range[2, 50] (* _Giovanni Resta_, May 07 2017 *)"
			],
			"program": [
				"(Python)",
				"from __future__ import division",
				"from sympy import prime",
				"from gmpy2 import is_square",
				"def A286328(n): # assumes n \u003e= 2",
				"    p, area = prime(n), 0",
				"    k, q, kq = (p + 1)//2, (p**2 - 1)//2, (p - 1)*(p + 1)**2//4",
				"    while True:",
				"        area += kq",
				"        if is_square(area):",
				"            return k",
				"        k += 1",
				"        kq += q # _Chai Wah Wu_, May 15 2017"
			],
			"xref": [
				"Cf. A002496, A011945, A062325, A084921, A188158."
			],
			"keyword": "nonn",
			"offset": "2,1",
			"author": "_Michel Lagneau_, May 07 2017",
			"references": 1,
			"revision": 28,
			"time": "2017-05-15T23:05:16-04:00",
			"created": "2017-05-14T22:47:39-04:00"
		}
	]
}