{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008305",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8305,
			"data": "1,1,2,1,2,6,1,2,9,24,1,2,13,44,120,1,2,20,80,265,720,1,2,31,144,579,1854,5040,1,2,49,264,1265,4738,14833,40320,1,2,78,484,2783,12072,43387,133496,362880,1,2,125,888,6208,30818,126565,439792,1334961,3628800",
			"name": "Triangle read by rows: a(n,k) = number of permutations of [n] allowing i-\u003ei+j (mod n), j=0..k-1.",
			"comment": [
				"The point is, we are counting permutations of [n] = {1,2,...,n} with the restriction that i cannot move by more than k places. Hence the phrase \"permutations with restricted displacements\". - _N. J. A. Sloane_, Mar 07 2014",
				"The triangle could have been defined as an infinite square array by setting a(n,k) = n! for k \u003e= n."
			],
			"reference": [
				"H. Minc, Permanents, Encyc. Math. #6, 1978, p. 48"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A008305/b008305.txt\"\u003eRows n = 1..23, flattened\u003c/a\u003e",
				"Henry Beker and Chris Mitchell, \u003ca href=\"http://dx.doi.org/10.1137/0608029\"\u003ePermutations with restricted displacement\u003c/a\u003e, SIAM J. Algebraic Discrete Methods 8 (1987), no. 3, 338--363. MR0897734 (89f:05009)",
				"N. S. Mendelsohn, \u003ca href=\"http://dx.doi.org/10.4153/CMB-1961-005-4\"\u003ePermutations with confined displacement\u003c/a\u003e, Canad. Math. Bull., 4 (1961), 29-38.",
				"N. Metropolis, M. L. Stein, P. R. Stein, \u003ca href=\"http://dx.doi.org/10.1016/S0021-9800(69)80058-X\"\u003ePermanents of cyclic (0,1) matrices\u003c/a\u003e, J. Combin. Theory, 7 (1969), 291-321.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Permanent_(mathematics)\"\u003ePermanent (mathematics)\u003c/a\u003e"
			],
			"formula": [
				"a(n,k) = per(sum(P^j, j=0..k-1)), where P is n X n, P[ i, i+1 (mod n) ]=1, 0's otherwise.",
				"a(n,n) - a(n,n-1) = A002467(n). - _Alois P. Heinz_, Mar 06 2019"
			],
			"example": [
				"a(4,3) = 9 because 9 permutations of {1,2,3,4} are allowed if each i can be placed on 3 positions i+0, i+1, i+2 (mod 4): 1234, 1423, 1432, 3124, 3214, 3412, 4123, 4132, 4213.",
				"Triangle begins:",
				"  1,",
				"  1, 2,",
				"  1, 2,   6,",
				"  1, 2,   9,  24,",
				"  1, 2,  13,  44,  120,",
				"  1, 2,  20,  80,  265,   720,",
				"  1, 2,  31, 144,  579,  1854,   5040,",
				"  1, 2,  49, 264, 1265,  4738,  14833,  40320,",
				"  1, 2,  78, 484, 2783, 12072,  43387, 133496,  362880,",
				"  1, 2, 125, 888, 6208, 30818, 126565, 439792, 1334961, 3628800,",
				"  ..."
			],
			"maple": [
				"with(LinearAlgebra):",
				"a:= (n, k)-\u003e Permanent(Matrix(n,",
				"             (i, j)-\u003e `if`(0\u003c=j-i and j-i\u003ck or j-i\u003ck-n, 1, 0))):",
				"seq(seq(a(n, k), k=1..n), n=1..10);"
			],
			"mathematica": [
				"a[n_, k_] := Permanent[Table[If[0 \u003c= j-i \u0026\u0026 j-i \u003c k || j-i \u003c k-n, 1, 0], {i, 1,n}, {j, 1, n}]]; Table[Table[a[n, k], {k, 1, n}], {n, 1, 10}] // Flatten (* _Jean-François Alcover_, Mar 10 2014, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Diagonals (from the right): A000142, A000166, A000179, A000183, A004307, A189389, A184965.",
				"Diagonals (from the left): A000211 or A048162, 4*A000382 or A004306 or A000803, A000804, A000805.",
				"a(n,ceiling(n/2)) gives A306738.",
				"Cf. A002467, A306714."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Comments and more terms from _Len Smiley_",
				"More terms from _Vladeta Jovovic_, Oct 02 2003",
				"Edited by _Alois P. Heinz_, Dec 18 2010"
			],
			"references": 17,
			"revision": 75,
			"time": "2019-04-22T01:14:30-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}