{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A262747",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 262747,
			"data": "1,2,1,1,1,3,2,1,2,4,2,2,1,2,2,1,2,3,2,3,4,3,1,2,3,4,2,4,2,2,3,1,4,2,1,2,5,4,1,2,2,6,3,2,4,4,3,3,4,3,3,5,3,3,4,2,6,7,3,4,4,5,2,2,5,6,6,1,5,4,4,4,6,6,1,4,4,2,4,3,5,6,3,4,5,5,4,2,2,6,5,4,6,3,3,1,5,3,2,3",
			"name": "Number of ordered ways to write n as x^2 + y^2 + phi(z^2) with 0 \u003c= x \u003c= y, z \u003e 0, 2 | x*y*z, and phi(k^2) \u003c n for all 0 \u003c k \u003c z, where phi(.) is Euler's totient function given by A000010.",
			"comment": [
				"Conjecture: a(n) \u003e 0 for all n \u003e 0, and a(n) = 1 only for n = 1, 3, 4, 5, 8, 13, 16, 23, 32, 35, 39, 68, 75, 96, 215, 219, 243, 363, 471, 723, 759, 923, 1443, 1551, 1839, 2739, 2883.",
				"It is easy to see that all the numbers phi(n^2) = n*phi(n) (n = 1,2,3,...) are pairwise distinct. We have verified that a(n) \u003e 0 for all n = 1,...,10^7.",
				"See also A262311 for a related conjecture."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A262747/b262747.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"a(5) = 1 since 5 = 0^2 + 2^2 + phi(1^2).",
				"a(8) = 1 since 8 = 0^2 + 0^2 + phi(4^2) with 0*0*4 even, and phi(1^2) = 1, phi(2^2) = 2, phi(3^2) = 6 all smaller than 8.",
				"a(13) = 1 since 13 = 1^2 + 2^2 + phi(4^2).",
				"a(32) = 1 since 32 = 2^2 + 4^2 + phi(6^2).",
				"a(68) = 1 since 68 = 0^2 + 6^2 + phi(8^2).",
				"a(96) = 1 since 96 = 0^2 + 8^2 + phi(8^2).",
				"a(363) = 1 since 363 = 0^2 + 19^2 + phi(2^2).",
				"a(471) = 1 since 471 = 0^2 + 19^2 + phi(11^2).",
				"a(723) = 1 since 723 = 17^2 + 18^2 + phi(11^2).",
				"a(759) = 1 since 759 = 9^2 + 26^2 + phi(2^2).",
				"a(923) = 1 since 923 = 16^2 + 25^2 + phi(7^2).",
				"a(1443) = 1 since 1443 = 19^2 + 24^2 +phi(23^2).",
				"a(1551) = 1 since 1551 = 18^2 + 35^2 + phi(2^2).",
				"a(1839) = 1 since 1839 = 3^2 + 30^2 + phi(31^2).",
				"a(2739) = 1 since 2739 = 1^2 + 24^2 + phi(47^2).",
				"a(2883) = 1 since 2883 = 21^2 + 44^2 + phi(23^2)."
			],
			"mathematica": [
				"f[n_]:=EulerPhi[n^2]",
				"SQ[n_]:=IntegerQ[Sqrt[n]]",
				"Do[r=0;Do[If[f[x]\u003en,Goto[aa]];Do[If[SQ[n-f[x]-y^2]\u0026\u0026(Mod[x*y,2]==0||Mod[Sqrt[n-f[x]-y^2],2]==0),r=r+1],{y,0,Sqrt[(n-f[x])/2]}];Continue,{x,1,n}];Label[aa];Print[n,\" \",r];Continue,{n,1,100}]"
			],
			"xref": [
				"Cf. A000010, A001481, A002618, A262311, A262746, A262750, A262766."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Zhi-Wei Sun_, Sep 30 2015",
			"references": 5,
			"revision": 18,
			"time": "2015-10-01T06:33:23-04:00",
			"created": "2015-09-30T06:04:28-04:00"
		}
	]
}