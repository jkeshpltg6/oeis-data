{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A128580",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 128580,
			"data": "1,-1,-2,2,1,-2,0,2,0,0,-2,0,3,-1,-2,2,2,-4,0,0,0,0,-2,0,3,0,-2,4,0,-2,0,2,0,0,0,0,2,-3,-4,2,1,-2,0,2,0,0,-2,0,2,-2,-2,2,4,-2,0,0,0,0,0,0,3,0,-4,2,0,-2,0,2,0,0,0,0,4,-3,-2,2,0,-4,0,2,0,0,-4,0,1,0,-2,6,2,-2,0,0,0,0,-2,0,2,0,-2,2,0,-4,0,0,0",
			"name": "Expansion of phi(x^3) * psi(x^4) - x * phi(x) * psi(x^12) in powers of x where phi(), psi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A128580/b128580.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"a(n) = b(2*n + 1) where b() is multiplicative with b(2^e) = 0^e, b(3^e) = (-1)^e, b(p^e) = e+1 if p == 1, 7 (mod 24), b(p^e) = (e+1)* (-1)^e if p == 5, 11 (mod 24), b(p^e) = (1+(-1)^e)/2 if p == 13, 17, 19, 23 (mod 24).",
				"Euler transform of period 24 sequence [ -1, -2, 0, 0, -1, -1, -1, -1, 0, -2, -1, -2, -1, -2, 0, -1, -1, -1, -1, 0, 0, -2, -1, -2, ...].",
				"a(12*n + 6) = a(12*n + 8) = a(12*n + 9) = a(12*n + 11)= 0.",
				"G.f.: Product_{k\u003e0} (1 - x^(8*k)) * (1 - x^(12*k))^2 / ((1 + x^k) * (1 + x^(2*k))^2 * (1 - x^(3*k)) * (1 + x^(12*k))).",
				"G.f.: Sum_{k\u003e=0} a(k) * x^(2*k + 1) = Sum_{k\u003e0} (x^k - x^(3*k)) / (1 + x^(4*k)) * Kronecker(-12, k) = Sum_{k\u003e0} (x^k + x^(3*k)) / (1 + x^(2*k) + x^(4*k)) * Kronecker(2, k).",
				"a(n) = A128581(2*n + 1) = A115660(2*n + 1). a(3*n + 2) = -2 * A128582(n). a(12*n) = A113780(n).",
				"a(2*n) = A259668(n). a(3*n + 1) = - A128580(n). - _Michael Somos_, Jul 12 2015"
			],
			"example": [
				"G.f. = 1 - x - 2*x^2 + 2*x^3 + x^4 - 2*x^5 + 2*x^7 - 2*x^10 + 3*x^12 + ...",
				"G.f. = q - q^3 - 2*q^5 + 2*q^7 + q^9 - 2*q^11 + 2*q^15 - 2*q^21 + 3*q^25 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (EllipticTheta[ 3, 0, x^3] EllipticTheta[ 2, 0, x^2] - EllipticTheta[ 3, 0, x] EllipticTheta[ 2, 0, x^6]) / (2 x^(1/2)), {x, 0, n}]; (* _Michael Somos_, Jul 12 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, n = 2*n + 1; sumdiv(n, d, kronecker(-12, d) * kronecker(2, n/d)))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A) * eta(x^2 + A) * eta(x^8 + A) * eta(x^12 + A)^3 / (eta(x^3 + A) * eta(x^4 + A)^2 * eta(x^24 + A)), n))};"
			],
			"xref": [
				"Cf. A113780, A115660, A128581, A128582, A259668."
			],
			"keyword": "sign",
			"offset": "0,3",
			"author": "_Michael Somos_, Mar 11 2007",
			"references": 16,
			"revision": 17,
			"time": "2021-03-12T22:24:44-05:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}