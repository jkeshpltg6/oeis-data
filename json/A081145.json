{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A081145",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 81145,
			"data": "1,2,4,7,3,8,14,5,12,20,6,16,27,9,21,34,10,25,41,11,28,47,13,33,54,15,37,60,17,42,68,18,45,73,19,48,79,22,55,23,58,94,24,61,99,26,66,107,29,71,115,30,75,121,31,78,126,32,81,132,35,87,140,36,91,147,38,96,155,39",
			"name": "a(1)=1; thereafter, a(n) is the least positive integer which has not already occurred and is such that |a(n)-a(n-1)| is different from any |a(k)-a(k-1)| which has already occurred.",
			"comment": [
				"The sequence is a permutation of the positive integers. The inverse is A081146.",
				"Similar to A100707, except that when we subtract we use the largest possible k.",
				"The 1977 paper of Slater and Velez proves that this sequence is a permutation of positive integers and conjectures that its absolute difference sequence (see A308007) is also a permutation. If we call this the \"Slater-Velez permutation of the first kind\", then they also constructed another permutation (the 2nd kind), for which they are able to prove that both the sequence (A129198) and its absolute difference (A129199) are true permutations. - Ferenc Adorjan, Apr 03 2007",
				"The points appear to lie on three straight lines of slopes roughly 0.56, 1.40, 2.24 (click \"graph\", or see the Wilks link). I checked this for the first 10^6 terms using _Allan Wilks_'s C program. See A308009-A308015 for further information about the three lines. - _N. J. A. Sloane_, May 14 2019"
			],
			"reference": [
				"P. J. Slater and W. Y. Velez, Permutations of the Positive Integers with Restrictions on the Sequence of Differences, II, Pacific Journal of Mathematics, Vol. 82, No. 2, 1979, 527-531."
			],
			"link": [
				"Ferenc Adorjan, \u003ca href=\"/A081145/b081145.txt\"\u003eTable of n,a(n) for n = 1..5000\u003c/a\u003e",
				"Shalosh B. Ekhad and Doron Zeilberger, \u003ca href=\"http://sites.math.rutgers.edu/~zeilberg/mamarim/mamarimPDF/sv.pdf\"\u003eGuessing the Elusive Patterns in the Slater-Valez sequence (aka OEIS A081145)\u003c/a\u003e, June 2019; \u003ca href=\"/A081145/a081145.pdf\"\u003eLocal copy\u003c/a\u003e",
				"P. J. Slater and W. Y. Velez, \u003ca href=\"http://projecteuclid.org/euclid.pjm/1102811644\"\u003ePermutations of the Positive Integers with Restrictions on the Sequence of Differences\u003c/a\u003e, Pacific Journal of Mathematics, Vol. 71, No. 1, 1977, 193-196.",
				"William Y. Velez, \u003ca href=\"https://doi.org/10.1016/0012-365X(92)90724-T\"\u003eResearch problems 159-160\u003c/a\u003e, Discrete Math., 110 (1992), pp. 301-302.",
				"Allan Wilks, \u003ca href=\"/A081145/a081145_2.txt\"\u003eTable showing n, a(n), slope, line_number, for n=1..100000\u003c/a\u003e [The three lines are labeled 0 (lower), 1 (middle), 2 (upper).]",
				"Allan Wilks, \u003ca href=\"/A081145/a081145.c.txt\"\u003eC program for A081145\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"example": [
				"a(4)=7 because the previous term is 4 and the differences |3-4|, |5-4| and |6-4| have already occurred.",
				"After 7 we get 3 as the difference 4 has not occurred earlier. 5 follows 14 as the difference 9 has not occurred earlier."
			],
			"mathematica": [
				"f[s_] := Block[{d = Abs[Rest@s - Most@s], k = 1}, While[ MemberQ[d, Abs[k - Last@s]] || MemberQ[s, k], k++ ]; Append[s, k]]; NestList[s, {1}, 70] (* _Robert G. Wilson v_, Jun 09 2006 *)",
				"f[s_] := Block[{k = 1, d = Abs[Most@s - Rest@s], l = Last@s}, While[MemberQ[s, k] || MemberQ[d, Abs[l - k]], k++ ]; Append[s, k]]; Nest[f, {1}, 70] (* _Robert G. Wilson v_, Jun 13 2006 *)"
			],
			"program": [
				"(PARI){SV_p1(n)=local(x,v=6,d=2,j,k); /* Slater-Velez permutation - the first kind (by F. Adorjan)*/ x=vector(n);x[1]=1;x[2]=2; for(i=3,n,j=3;k=1;while(k,if(k=bittest(v,j)||bittest(d,abs(j-x[i-1])),j++,v+=2^j;d+=2^abs(j-x[i-1]);x[i]=j))); return(x)} \\\\ Ferenc Adorjan, Apr 03 2007",
				"(Python)",
				"A081145_list, l, s, b1, b2 = [1,2], 2, 3, set(), set([1])",
				"for n in range(3, 10**2):",
				"    i = s",
				"    while True:",
				"        m = abs(i-l)",
				"        if not (i in b1 or m in b2):",
				"            A081145_list.append(i)",
				"            b1.add(i)",
				"            b2.add(m)",
				"            l = i",
				"            while s in b1:",
				"                b1.remove(s)",
				"                s += 1",
				"            break",
				"        i += 1 # _Chai Wah Wu_, Dec 15 2014",
				"(Haskell)",
				"import Data.List (delete)",
				"a081145 n = a081145_list !! (n-1)",
				"a081145_list = 1 : f 1 [2..] [] where",
				"   f x vs ws = g vs where",
				"     g (y:ys) = if z `elem` ws then g ys else y : f y (delete y vs) (z:ws)",
				"                where z = abs (x - y)",
				"-- _Reinhard Zumkeller_, Jul 02 2015"
			],
			"xref": [
				"The sequence of differences is A099004 (see also A308007).",
				"Similar to Murthy's sequence A093903, Cald's sequence (A006509) and Recamán's sequence A005132. See also A100707 (another version).",
				"A308021 is an offspring of this sequence. - _N. J. A. Sloane_, May 13 2019",
				"Cf. A063733, A072007, A078783, A081146, A084331, A084335, A117622.",
				"See A308009-A308015 for the lines that the points lie on.",
				"A308172 gives smallest missing numbers."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Don Reble_, Mar 08 2003",
			"references": 50,
			"revision": 73,
			"time": "2021-03-28T23:36:58-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}