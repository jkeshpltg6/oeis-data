{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005990",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5990,
			"id": "M4551",
			"data": "0,1,8,60,480,4200,40320,423360,4838400,59875200,798336000,11416204800,174356582400,2833294464000,48819843072000,889218570240000,17072996548608000,344661117825024000,7298706024529920000,161787983543746560000",
			"name": "a(n) = (n-1)*(n+1)!/6.",
			"comment": [
				"Coefficients of Gandhi polynomials.",
				"a(n) = Sum_{pi in Symm(n)} Sum_{i=1..n} max(pi(i)-i,0), i.e., the total positive displacement of all letters in all permutations on n letters. - _Franklin T. Adams-Watters_, Oct 25 2006",
				"a(n) is also the sum of the excedances of all permutations of [n]. An excedance of a permutation p of [n] is an i (1 \u003c= i \u003c= n-1) such that p(i) \u003e i. Proof: i is an excedance if p(i) = i+1, i+2, ..., n (n-i possibilities), with the remaining values of p forming any permutation of [n]\\{p(i)} in the positions [n]\\{i} ((n-1)! possibilities). Summation of i(n-i)(n-1)! over i from 1 to n-1 completes the proof. Example: a(3)=8 because the permutations 123, 132, 213, 231, 312, 321 have excedances NONE, {2}, {1}, {1,2}, {1}, {1}, respectively. - _Emeric Deutsch_, Oct 26 2008",
				"a(n) is also the number of doubledescents in all permutations of {1,2,...,n-1}. We say that i is a doubledescent of a permutation p if p(i) \u003e p(i+1) \u003e p(i+2). Example: a(3)=8 because each of the permutations 1432, 4312, 4213, 2431, 3214, 3421 has one doubledescent, the permutation 4321 has two doubledescents and the remaining 17 permutations of {1,2,3,4} have no doubledescents. - _Emeric Deutsch_, Jul 26 2009",
				"Half of sum of abs(p(i+1) - p(i)) over all permutations on n, e.g., 42531 = 2 + 3 + 2 + 2 = 9, and the total over all permutations on {1,2,3,4,5} is 960. - _Jon Perry_, May 24 2013",
				"a(n) gives the number of non-occupied corners in tree-like tableaux of size n+1 (see Gao et al. link). - _Michel Marcus_, Nov 18 2015",
				"a(n) is the number of sequences of n+2 balls colored with at most n colors such that exactly three balls are the same color as some other ball in the sequence. - _Jeremy Dover_, Sep 26 2017",
				"a(n) is the number of triangles (3-cycles) in the (n+1)-alternating group graph. - _Eric W. Weisstein_, Jun 09 2019"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A005990/b005990.txt\"\u003eTable of n, a(n) for n = 1..300\u003c/a\u003e",
				"D. Dumont, \u003ca href=\"http://dx.doi.org/10.1215/S0012-7094-74-04134-9\"\u003eInterpretations combinatoires des nombres de Genocchi\u003c/a\u003e, Duke Math. J., 41 (1974), 305-318.",
				"D. Dumont, \u003ca href=\"/A001469/a001469_3.pdf\"\u003eInterprétations combinatoires des nombres de Genocchi\u003c/a\u003e, Duke Math. J., 41 (1974), 305-318. (Annotated scanned copy)",
				"Alice L. L. Gao, Emily X. L. Gao, Brian Y. Sun, \u003ca href=\"http://arxiv.org/abs/1511.05434\"\u003eZubieta's Conjecture on the Enumeration of Corners in Tree-like Tableaux\u003c/a\u003e, arXiv:1511.05434 [math.CO], 2015. The second version of this paper has a different title and different authors: A. L. L. Gao, E. X. L. Gao, P. Laborde-Zubieta, and B. Y. Sun, Enumeration of Corners in Tree-like Tableaux and a Conjectural (a,b)-analogue, arXiv preprint arXiv:1511.05434v2, 2015.",
				"Milan Janjic, \u003ca href=\"http://www.pmfbl.org/janjic/\"\u003eEnumerative Formulas for Some Functions on Finite Sets\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/AlternatingGroupGraph.html\"\u003eAlternating Group Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GraphCycle.html\"\u003eGraph Cycle\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A052571(n+2)/6. - _Zerinvary Lajos_, May 11 2007",
				"a(n) = Sum_{m=0..n} Sum_{k=-1..n} Sum_{j=1..n} n!/6, n \u003e= 0. - _Zerinvary Lajos_, May 11 2007",
				"If we define f(n,i,x) = Sum_{k=i..n} (Sum_{j=i..k} binomial(k,j)*stirling1(n,k)*stirling2(j,i)*x^(k-j)) then a(n+1) = (-1)^(n-1)*f(n,1,-4), (n \u003e= 1). - _Milan Janjic_, Mar 01 2009",
				"E.g.f.: (-1+3*x)/(3!*(1-x)^3), a(0) = -1/3!. Such e.g.f. computations resulted from e-mail exchange with _Gary Detlefs_. - _Wolfdieter Lang_, May 27 2010",
				"a(n) = ((n+3)!/2) * Sum_{j=i..k} (k+1)!/(k+3)!, with offset 0. - _Gary Detlefs_, Aug 05 2010",
				"a(n) = (n+2)!*Sum_{k=1..n-1} 1/((2*k+4)*(k+3)). - _Gary Detlefs_, Oct 09 2011",
				"a(n) = (n+2)!*(1 + 3*(H(n+1) - H(n+2)))/6, where H(n) is the n-th harmonic number. - _Gary Detlefs_, Oct 09 2011",
				"With offset = 0, e.g.f.: x/(1-x)^4. - _Geoffrey Critzer_, Aug 30 2013"
			],
			"maple": [
				"[ seq((n-1)*(n+1)!/6,n=1..40) ];",
				"a:=n-\u003esum(sum(sum(n!/6, j=1..n),k=-1..n),m=0..n): seq(a(n), n=0..19); # _Zerinvary Lajos_, May 11 2007",
				"seq(sum(mul(j,j=3..n), k=3..n)/3, n=2..21); # _Zerinvary Lajos_, Jun 01 2007",
				"restart: G(x):=x^3/(1-x)^2: f[0]:=G(x): for n from 1 to 21 do f[n]:=diff(f[n-1],x) od: x:=0: seq(f[n]/3!,n=2..21); # _Zerinvary Lajos_, Apr 01 2009"
			],
			"mathematica": [
				"Table[Sum[n!/6, {i, 3, n}], {n, 2, 21}] (* _Zerinvary Lajos_, Jul 12 2009 *)",
				"Table[(n - 1) (n + 1)!/6, {n, 20}] (* _Harvey P. Dale_, Apr 07 2019 *)",
				"Table[(n - 1) Pochhammer[4, n - 2], {n, 20}] (* _Eric W. Weisstein_, Jun 09 2019 *)",
				"Table[(n - 1) Gamma[n + 2]/6, {n, 20}] (* _Eric W. Weisstein_, Jun 09 2019 *)",
				"Range[0, 20]! CoefficientList[Series[x/(1 - x)^4, {x, 0, 20}], x] (* _Eric W. Weisstein_, Jun 09 2019 *)"
			],
			"program": [
				"(MAGMA) [(n-1)*Factorial(n+1)/6: n in [1..25]]; // _Vincenzo Librandi_, Oct 11 2011",
				"(PARI) a(n)=(n-1)*(n+1)!/6 \\\\ _Charles R Greathouse IV_, May 24 2013"
			],
			"xref": [
				"A090672(n)/2.",
				"Cf. A001715.",
				"Equals the second right hand column of A167568 divided by 2. - _Johannes W. Meijer_, Nov 12 2009"
			],
			"keyword": "nonn,easy",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Better definition from Robert Newstedt"
			],
			"references": 20,
			"revision": 71,
			"time": "2019-06-09T21:41:38-04:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}