{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A052239",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 52239,
			"data": "251,111497,74453,1397609,642427,5321191,23921257,55410683,400948369,253444777,1140813701,491525857,998051413,2060959049,4480114337,55140921491,38415872947,315392068463,15162919459,60600021611,278300877401,477836574947,1486135570643",
			"name": "Smallest prime p in set of 4 consecutive primes in arithmetic progression with common difference 6n.",
			"comment": [
				"See also the less restrictive A054701 where the gaps are multiples 6n. - _M. F. Hasler_, Nov 06 2018"
			],
			"link": [
				"\u003ca href=\"/index/Pri#primes_AP\"\u003eIndex entries for sequences related to primes in arithmetic progressions\u003c/a\u003e"
			],
			"example": [
				"a(5) = 642427, 642457, 642487, 642517 are the smallest consecutive primes with 3 consecutive gaps of 30, cf. A052243.",
				"From _M. F. Hasler_, Nov 06 2018: (Start)",
				"Other terms are also initial terms of corresponding sequences:",
				"a(1) = 251 = A033451(1) = A054800(1), start of first CPAP-4 with common gap of 6,",
				"a(2) = 111497 = A033447(1), start of first CPAP-4 with common gap of 12,",
				"a(3) = 74453 = A033448(1) = A054800(25), first CPAP-4 with common gap of 18,",
				"a(4) = 1397609 = A052242(1), start of first CPAP-4 with common gap of 24,",
				"a(5) = 642427 = A052243(1) = A052195(16), first CPAP-4 with common gap of 30,",
				"a(6) = 5321191 = A058252(1) = A161534(26), first CPAP-4 with common gap 36 = 6^2,",
				"a(7) = 23921257 = A058323(1), start of first CPAP-4 with common gap of 42,",
				"a(8) = 55410683 = A067388(1), start of first CPAP-4 with common gap of 48,",
				"a(9) = 400948369 = A259224(1), start of first CPAP-4 with common gap of 54,",
				"a(10) = 253444777 = A210683(1) = A089234(417), CPAP-4 with common gap of 60,",
				"a(11) = 1140813701 = A287547(1), start of first CPAP-4 with common gap of 66,",
				"a(12) = 491525857 = A287550(1), start of first CPAP-4 with common gap of 72,",
				"a(13) = 998051413 = A287171(1), start of first CPAP-4 with common gap of 78,",
				"a(14) = 2060959049 = A287593(1), start of first CPAP-4 with common gap of 84,",
				"a(15) = 4480114337 = A286817(1) = A204852(444), common distance 90. (End)"
			],
			"mathematica": [
				"Transpose[Flatten[Table[Select[Partition[Prime[Range[2000000]],4,1], Union[ Differences[ #]] =={6n}\u0026,1],{n,7}],1]][[1]] (* _Harvey P. Dale_, Aug 12 2012 *)"
			],
			"program": [
				"(PARI) a(n, p=[2, 0, 0], d=6*[n, n, n])={while(p+d!=p=[nextprime(p[1]+1), p[1], p[2]], ); p[3]-d[3]} \\\\ after _M. F. Hasler_ in A052243; Graziano Aglietti (mg5055(AT)mclink.it), Aug 22 2010, Corrected by _M. F. Hasler_, Nov 06 2018",
				"(PARI) A052239(n, p=2, c, o)={n*=6; forprime(q=p+1, , if(p+n!=p=q, next, q!=o+2*n, c=2, c++\u003e3, break); o=q-n); o-n} \\\\ _M. F. Hasler_, Nov 06 2018"
			],
			"xref": [
				"Initial terms of sequences A033451, A033447, A033448, A052242, A052243, A058252, A058323, A067388, A259224, A210683.",
				"Range is a subset of A054800: start of 4 consecutive primes in arithmetic progression (CPAP-4).",
				"Cf. A054701: gaps are possibly distinct multiples of 6n (not CPAP's)."
			],
			"keyword": "nice,nonn",
			"offset": "1,1",
			"author": "_Labos Elemer_, Jan 31 2000",
			"ext": [
				"More terms from _Labos Elemer_, Jan 04 2002",
				"a(7) corrected and more terms added by Graziano Aglietti (mg5055(AT)mclink.it), Aug 22 2010",
				"a(15)-a(20) from _Donovan Johnson_, Oct 05 2010",
				"a(21)-a(23) from _Donovan Johnson_, May 23 2011"
			],
			"references": 11,
			"revision": 34,
			"time": "2018-11-09T21:16:47-05:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}