{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A004718",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 4718,
			"data": "0,1,-1,2,1,0,-2,3,-1,2,0,1,2,-1,-3,4,1,0,-2,3,0,1,-1,2,-2,3,1,0,3,-2,-4,5,-1,2,0,1,2,-1,-3,4,0,1,-1,2,1,0,-2,3,2,-1,-3,4,-1,2,0,1,-3,4,2,-1,4,-3,-5,6,1,0,-2,3,0,1,-1,2,-2,3,1,0,3,-2,-4,5,0,1,-1,2,1,0",
			"name": "The Danish composer Per Nørgård's \"infinity sequence\", invented in an attempt to unify in a perfect way repetition and variation: a(2n) = -a(n), a(2n+1) = a(n) + 1, a(0) = 0.",
			"comment": [
				"Minima are at n=2^i-2, maxima at 2^i-1, zeros at A083866.",
				"a(n) has parity of Thue-Morse sequence on {0,1} (A010060).",
				"a(n) = A000120(n) for all n in A060142.",
				"The composer Per Nørgård's name is also written in the OEIS as Per Noergaard.",
				"Comment from Michael Nyvang on the \"iris\" score on the \"Voyage into the golden screen\" video, Dec 31 2018: That is A004718 on the cover in the 12-tone tempered chromatic scale. The music - as far as I recall - is constructed from this base by choosing subsequences out of this sequence in what Per calls 'wave lengths', and choosing different scales modulo (to-tone, overtones on one fundamental, etc). There quite a lot more to say about this, but I believe this is the foundation. - _N. J. A. Sloane_, Jan 05 2019",
				"From _Antti Karttunen_, Mar 09 2019: (Start)",
				"This sequence can be represented as a binary tree. After a(0) = 0 and a(1) = 1, each child to the left is obtained by negating the parent node's contents, and each child to the right is obtained by adding one to the parent's contents:",
				"                                      0",
				"                                      |",
				"                   ...................1...................",
				"                 -1                                       2",
				"        1......../ \\........0                  -2......../ \\........3",
				"       / \\                 / \\                 / \\                 / \\",
				"      /   \\               /   \\               /   \\               /   \\",
				"     /     \\             /     \\             /     \\             /     \\",
				"   -1       2           0       1           2      -1          -3       4",
				"  1   0  -2   3       0   1  -1   2      -2   3   1   0       3  -2  -4   5",
				"etc.",
				"Sequences A323907, A323908 and A323909 are in bijective correspondence with this sequence and their terms are all nonnegative.",
				"(End)"
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A004718/b004718.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"J.-P. Allouche and J. Shallit, \u003ca href=\"http://www.math.jussieu.fr/~allouche/kreg2.ps\"\u003eThe Ring of k-regular Sequences, II\u003c/a\u003e",
				"J.-P. Allouche and J. Shallit, \u003ca href=\"http://dx.doi.org/10.1016/S0304-3975(03)00090-2\"\u003eThe ring of k-regular sequences, II\u003c/a\u003e, Theoret. Computer Sci., 307 (2003), 3-29.",
				"Yu Hin Au, Christopher Drexler-Lemire and Jeffrey Shallit, \u003ca href=\"https://doi.org/10.1080/17459737.2017.1299807\"\u003eNotes and note pairs in Nørgård's infinity series\u003c/a\u003e, Journal of Mathematics and Music, Volume 11, 2017, Issue 1, pages 1-19. - _N. J. A. Sloane_, Dec 31 2018",
				"Christopher Drexler-Lemire and Jeffrey Shallit, \u003ca href=\"http://arxiv.org/abs/1402.3091\"\u003eNotes and Note-Pairs in Noergaard's Infinity Series\u003c/a\u003e, arXiv:1402.3091 [math.CO], 2014.",
				"Per Nørgård [Noergaard], \u003ca href=\"https://www.youtube.com/watch?v=Q_FGImH1RWE\"\u003eThe infinity series\u003c/a\u003e, on YouTube.",
				"Per Nørgård [Noergaard], \u003ca href=\"http://web.archive.org/web/20060224072530id_/http://www.pernoergaard.dk/ress/musexx/mu01.mp3\"\u003eFirst 128 notes of the infinity series (MP3 Recording)\u003c/a\u003e",
				"Per Nørgård [Noergaard], \u003ca href=\"https://www.youtube.com/watch?v=wc8GvMkjGBc\"\u003eVoyage into the golden screen\u003c/a\u003e, on YouTube.",
				"Per Nørgård [Noergaard], \u003ca href=\"http://web.archive.org/web/20060224072542id_/http://www.pernoergaard.dk/ress/musexx/m1110356.mp3\"\u003eVoyage into the golden screen (MP3 Recording)\u003c/a\u003e",
				"Ralf Stephan, \u003ca href=\"https://arxiv.org/abs/math/0307027\"\u003eDivide-and-conquer generating functions. I. Elementary sequences\u003c/a\u003e, arXiv:math/0307027 [math.CO], 2003.",
				"Robert Walker, \u003ca href=\"http://robertinventor.com/ftswiki/Self_Similar_Sloth_Canon_Number_Sequences\"\u003eSelf Similar Sloth Canon Number Sequences\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://de.wikipedia.org/wiki/Unendlichkeitsreihe\"\u003eUnendlichkeitsreihe\u003c/a\u003e",
				"Jon Wild, \u003ca href=\"/A004718/a004718_1.txt\"\u003eComments on the musical score in the YouTube illustrations for the \"Iris\" and \"Voyage into the golden screen\" videos\u003c/a\u003e",
				"\u003ca href=\"/index/Mu#music\"\u003eIndex entries for sequences related to music\u003c/a\u003e"
			],
			"formula": [
				"Write n in binary and read from left to right, starting with 0 and interpreting 1 as \"add 1\" and 0 as \"change sign\". For example 19 = binary 10011, giving 0 -\u003e 1 -\u003e -1 -\u003e 1 -\u003e 2 -\u003e 3, so a(19) = 3.",
				"G.f.: sum{k\u003e=0, x^(2^k)/[1-x^(2*2^k)] * prod{l=0, k-1, x^(2^l)-1}}.",
				"The g.f. satisfies F(x^2)*(1-x) = F(x)-x/(1-x^2).",
				"a(n) = (2 * (n mod 2) - 1) * a(floor(n/2)) + n mod 2. - _Reinhard Zumkeller_, Mar 20 2015",
				"Zumkeller's formula implies that a(2n) = -a(n), and so a(n) = a(4n) = a(16n) = .... - _N. J. A. Sloane_, Dec 31 2018",
				"From _Kevin Ryde_, Apr 17 2021: (Start)",
				"a(n) = (-1)^t * (t+1 - a(n-1)) where t = A007814(n) is the 2-adic valuation of n.",
				"a(n) = A343029(n) - A343030(n).",
				"(End)"
			],
			"maple": [
				"f:=proc(n) option remember; if n=0 then RETURN(0); fi; if n mod 2 = 0 then RETURN(-f(n/2)); else RETURN(f((n-1)/2)+1); fi; end;"
			],
			"mathematica": [
				"a[n_?EvenQ] := a[n]= -a[n/2]; a[0]=0; a[n_] := a[n]= a[(n-1)/2]+1; Table[a[n], {n, 0, 85}](* _Jean-François Alcover_, Nov 18 2011 *)",
				"Table[Fold[If[#2 == 0, -#1, #1 + 1] \u0026, IntegerDigits[n, 2]], {n, 0, 85}] (* _Michael De Vlieger_, Jun 30 2016 *)"
			],
			"program": [
				"(PARI) a=vector(100); a[1]=1; a[2]=-1; for(n=3,#a,a[n]=if(n%2,a[n\\2]+1,-a[n\\2])); a \\\\ _Charles R Greathouse IV_, Nov 18 2011",
				"(Haskell)",
				"import Data.List (transpose)",
				"a004718 n = a004718_list !! n",
				"a004718_list = 0 : concat",
				"   (transpose [map (+ 1) a004718_list, map negate $ tail a004718_list])",
				"-- _Reinhard Zumkeller_, Mar 19 2015, Nov 10 2012",
				"(Python) # from first formula",
				"from functools import reduce",
				"def f(s, b): return s + 1 if b == '1' else -s",
				"def a(n): return reduce(f, [0] + list(bin(n)[2:]))",
				"print([a(n) for n in range(86)]) # _Michael S. Branicky_, Apr 03 2021",
				"(Python) # via recursion",
				"from functools import lru_cache",
				"@lru_cache(maxsize=None)",
				"def a(n): return 0 if n == 0 else (a((n-1)//2)+1 if n%2 else -a(n//2))",
				"print([a(n) for n in range(86)]) # _Michael S. Branicky_, Apr 03 2021"
			],
			"xref": [
				"Cf. A083866 (indices of 0's), A256187 (first differences), A010060 (mod 2), A343029, A343030.",
				"Variants: A256184, A256185, A255723, A323886, A323887, A323907, A323908, A323909."
			],
			"keyword": "sign,nice,easy,hear",
			"offset": "0,4",
			"author": "Jorn B. Olsson (olsson(AT)math.ku.dk)",
			"ext": [
				"Edited by _Ralf Stephan_, Mar 07 2003"
			],
			"references": 21,
			"revision": 103,
			"time": "2021-04-17T21:53:13-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}