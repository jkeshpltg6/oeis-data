{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A288537",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 288537,
			"data": "1,3,1,2,3,1,2,2,3,1,8,2,2,3,1,4,8,2,2,3,1,3,4,8,2,2,3,1,2,3,2,8,2,2,3,1,0,2,3,4,2,2,2,3,1,28,0,2,3,4,8,2,2,3,1,90,28,8,2,6,2,8,2,2,3,1,8,90,28,0,2,3,4,8,2,2,3,1,72,8,90,28,0,2",
			"name": "Array A(b,n) by upward antidiagonals (b\u003e1, n\u003e0): the eventual period of the RATS sequence in base b starting from n; 0 is for infinity.",
			"comment": [
				"Eventual period of n under the mapping x-\u003eA288535(b,x), or 0 if there is a divergence and thus no eventual period.",
				"For b = 3*2^m - 2 with m\u003e1, row b contains all sufficiently large even integers if m is odd, or just all sufficiently large integers if m is even.",
				"For b = 1 or 10 (mod 18) or b = 1 (mod (2^q-1)^2) with q\u003e2, there are 0's in row b.",
				"Conway conjectured that in row (base) 10, all 0's correspond to the same divergent RATS sequence called the Creeper (A164338). In Thiel's terms, it is quasiperiodic with quasiperiod 2, i.e., after every 2 steps the number of one of the digits (in this case, 3 or 6) increases by 1 while other digits stay unchanged. In other bases, 0's may correspond to different divergent RATS sequences. Thiel conjectured that the divergent RATS sequences are always quasiperiodic."
			],
			"link": [
				"Curtis Cooper, \u003ca href=\"http://cs.ucmo.edu/~cnc8851/rats.html\"\u003eRATS\u003c/a\u003e.",
				"R. K. Guy, \u003ca href=\"https://doi.org/10.2307/2325149\"\u003eConway's RATS and other reversals\u003c/a\u003e, Amer. Math. Monthly, 96 (1989), 425-428.",
				"S. Shattuck and C. Cooper, \u003ca href=\"http://www.fq.math.ca/Scanned/39-2/shattuck.pdf\"\u003eDivergent RATS sequences\u003c/a\u003e, Fibonacci Quart., 39 (2001), 101-106.",
				"J. Thiel, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL15/Thiel/thiel2.pdf\"\u003eConway’s RATS Sequences in Base 3\u003c/a\u003e, Journal of Integer Sequences, 15 (2012), Article 12.9.2.",
				"J. Thiel, \u003ca href=\"https://www.emis.de/journals/INTEGERS/papers/o50/o50.pdf\"\u003eOn RATS sequences in general bases\u003c/a\u003e, Integers, 14 (2014), #A50.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RATSSequence.html\"\u003eRATS Sequence\u003c/a\u003e.",
				"\u003ca href=\"/index/Ra#RATS\"\u003eIndex entries for sequences related to RATS: Reverse, Add Then Sort\u003c/a\u003e"
			],
			"formula": [
				"A(2^t,1)=t.",
				"A(3,3^A134067(p)-1)=p+3."
			],
			"example": [
				"In base 3, the RATS mapping acts as 1 -\u003e 2 -\u003e 4 (11 in base 3) -\u003e 8 (22 in base 3) -\u003e 13 (112 in base 3) -\u003e 4, which has already been seen 3 steps ago, so A(3,1)=3.",
				"The array begins:",
				"1, 1, 1, 1, 1, 1, ...",
				"3, 3, 3, 3, 3, 3, ...",
				"2, 2, 2, 2, 2, 2, ...",
				"2, 2, 2, 2, 2, 2, ...",
				"8, 8, 8, 8, 2, 8, ...",
				"4, 4, 2, 4, 4, 2, ...",
				"3, 3, 3, 3, 6, 3, ...",
				"2, 2, 2, 2, 2, 2, ...",
				"0, 0, 8, 0, 0, 8, ...",
				"28, 28, 28, 28, 2, 28, ...",
				"90, 90, 90, 90, 90, 90 ..."
			],
			"xref": [
				"Cf. A004000, A036839, A114611 (row 10), A161593, A288535, A288536 (column 1)."
			],
			"keyword": "nonn,tabl,base",
			"offset": "2,2",
			"author": "_Andrey Zabolotskiy_, Jun 11 2017",
			"references": 4,
			"revision": 7,
			"time": "2017-06-15T17:11:28-04:00",
			"created": "2017-06-15T17:11:28-04:00"
		}
	]
}