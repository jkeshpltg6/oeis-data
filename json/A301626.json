{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A301626",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 301626,
			"data": "0,0,0,1,1,1,4,1,1,4,8,2,0,2,8,9,5,1,1,5,9,4,10,4,2,4,10,4,1,5,9,5,5,9,5,1,0,2,8,10,8,10,8,2,0,1,1,5,13,13,13,13,5,1,1,4,2,4,10,20,18,20,10,4,2,4,4,2,4,9,17,25,25,17,9,4,2,4,5,1,1",
			"name": "Square array T(n, k) read by antidiagonals, n \u003e= 0 and k \u003e= 0: T(n, k) = square of the distance from n + k*i to nearest cube of a Gaussian integer (where i denotes the root of -1 with positive imaginary part).",
			"comment": [
				"The distance between two Gaussian integers is not necessarily integer, hence the use of the square of the distance.",
				"This sequence is a complex variant of A074989.",
				"See A301636 for the square array dealing with squares of Gaussian integers."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A301626/b301626.txt\"\u003eTable of n, a(n) for n = 0..20300\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A301626/a301626.png\"\u003eColored scatterplot for abs(x) \u003c= 500 and abs(y) \u003c= 500\u003c/a\u003e (where the hue is function of sqrt(T(abs(x), abs(y))))",
				"Rémy Sigrist, \u003ca href=\"/A301626/a301626_1.png\"\u003eVoronoi diagram of the cubes of Gaussian integers for abs(x) \u003c= 500 and abs(y) \u003c= 500\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A301626/a301626_2.png\"\u003eScatterplot of (x, y) such that T(abs(x), abs(y)) is a square and abs(x) \u003c= 500 and abs(y) \u003c= 500\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A301626/a301626.gp.txt\"\u003ePARI program for A301626\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Gaussian_integer\"\u003eGaussian integer\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Voronoi_diagram\"\u003eVoronoi diagram\u003c/a\u003e",
				"\u003ca href=\"/index/Di#distance_to_the_nearest\"\u003eIndex entries for sequences related to distance to nearest element of some set\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = T(k, n).",
				"T(n, 0) \u003c= A074989(n)^2.",
				"T(n, 0) = 0 iff n is a cube (A000578).",
				"T(n, k) = 0 iff n + k*i = z^3 for some Gaussian integer z."
			],
			"example": [
				"Square array begins:",
				"  n\\k|    0    1    2    3    4    5    6    7    8    9   10",
				"  ---+-------------------------------------------------------",
				"    0|    0    0    1    4    8    9    4    1    0    1    4  --\u003e  A301639",
				"    1|    0    1    1    2    5   10    5    2    1    2    2",
				"    2|    1    1    0    1    4    9    8    5    4    4    1",
				"    3|    4    2    1    2    5   10   13   10    9    5    2",
				"    4|    8    5    4    5    8   13   20   17   13    8    5",
				"    5|    9   10    9   10   13   18   25   25   18   13   10",
				"    6|    4    5    8   13   20   25   32   32   25   20   17",
				"    7|    1    2    5   10   17   25   32   41   34   29   26",
				"    8|    0    1    4    9   13   18   25   34   45   40   37",
				"    9|    1    2    4    5    8   13   20   29   40   53   50",
				"   10|    4    2    1    2    5   10   17   26   37   50   65"
			],
			"program": [
				"(PARI) See Links section."
			],
			"xref": [
				"Cf. A000578, A074989, A301636, A301639 (first row/column)."
			],
			"keyword": "nonn,tabl,look",
			"offset": "0,7",
			"author": "_Rémy Sigrist_, Mar 24 2018",
			"references": 3,
			"revision": 23,
			"time": "2018-03-27T18:33:29-04:00",
			"created": "2018-03-26T20:04:39-04:00"
		}
	]
}