{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A264541",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 264541,
			"data": "0,1,65,13247,704707,660278641,357852111131,309349386395887,240498440880062263,148443546307725010253,61760947097005048531,13658972396318235617977,723464275788899734058353751,489812222050789870424202126629,2614176630672654770175367214389,204702102697072009862200307064701369",
			"name": "a(n) = numerator(Jtilde3(n)).",
			"comment": [
				"Jtilde3(n) are Apéry-like rational numbers that arise in the calculation of zetaQ(3), the spectral zeta function for the non-commutative harmonic oscillator using a Gaussian hypergeometric function."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A264541/b264541.txt\"\u003eTable of n, a(n) for n = 0..345\u003c/a\u003e",
				"Takashi Ichinose, Masato Wakayama, \u003ca href=\"http://doi.org/10.2206/kyushujm.59.39\"\u003eSpecial values of the spectral zeta function of the non-commutative harmonic oscillator and confluent Heun equations\u003c/a\u003e, Kyushu Journal of Mathematics, Vol. 59 (2005) No. 1 p. 39-100.",
				"Kazufumi Kimoto, Masato Wakayama, \u003ca href=\"http://doi.org/10.2206/kyushujm.60.383\"\u003eApéry-like numbers arising from special values of spectral zeta functions for non-commutative harmonic oscillators\u003c/a\u003e, Kyushu Journal of Mathematics, Vol. 60 (2006) No. 2 p. 383-404 (see Table 2)."
			],
			"formula": [
				"Jtilde3(n) = J3(n) - J3(0)*Jtilde2(n) (normalization).",
				"4n^2*J3(n) - (8n^2-8n+3)*J3(n-1) + 4(n-1)^2*J3(n-2) = 2^n*(n-1)!/(2n-1)!! with J3(0)=7*zeta(3) and J3(1)=21*zeta(3)/4 + 1/2."
			],
			"mathematica": [
				"Numerator[Table[-2*Sum[(-1)^k*Binomial[-1/2, k]^2*Binomial[n, k]*Sum[ 1/(Binomial[-1/2, j]^2*(2*j + 1)^3), {j, 0, k - 1}], {k, 0, n}], {n, 0, 50}]] (* _G. C. Greubel_, Oct 24 2017 *)"
			],
			"program": [
				"(PARI) a(n) = numerator(-2*sum(k=0, n, (-1)^k*binomial(-1/2, k)^2*binomial(n, k)*sum(j=0, k-1, 1/(binomial(-1/2,j)^2*(2*j+1)^3))));"
			],
			"xref": [
				"Cf. A002117 (zeta(3)), A260832 (Jtilde2), A264542 (denominators).",
				"The Apéry-like numbers [or Apéry-like sequences, Apery-like numbers, Apery-like sequences] include A000172, A000984, A002893, A002895, A005258, A005259, A005260, A006077, A036917, A063007, A081085, A093388, A125143 (apart from signs), A143003, A143007, A143413, A143414, A143415, A143583, A183204, A214262, A219692, A226535, A227216, A227454, A229111 (apart from signs), A260667, A260832, A262177, A264541, A264542, A279619, A290575, A290576. (The term \"Apery-like\" is not well-defined.)"
			],
			"keyword": "nonn,frac",
			"offset": "0,3",
			"author": "_Michel Marcus_, Nov 17 2015",
			"references": 35,
			"revision": 18,
			"time": "2017-10-24T03:35:06-04:00",
			"created": "2015-11-18T03:42:17-05:00"
		}
	]
}