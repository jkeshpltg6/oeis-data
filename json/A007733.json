{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007733",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7733,
			"data": "1,1,2,1,4,2,3,1,6,4,10,2,12,3,4,1,8,6,18,4,6,10,11,2,20,12,18,3,28,4,5,1,10,8,12,6,36,18,12,4,20,6,14,10,12,11,23,2,21,20,8,12,52,18,20,3,18,28,58,4,60,5,6,1,12,10,66,8,22,12,35,6,9,36,20,18,30,12,39,4,54,20,82,6",
			"name": "Period of binary representation of 1/n. Also, multiplicative order of 2 modulo the odd part of n (= A000265(n)).",
			"comment": [
				"Also sequence of period lengths for n's when you do primality testing and calculate \"2^k mod n\" from k = 0..n. - _Gottfried Helms_, Oct 05 2000",
				"Fractal sequence related to A002326: the even terms of this sequence are this sequence itself, constructed on A002326, whose terms are the odd terms of this sequence. - _Alexandre Wajnberg_, Apr 27 2005",
				"It seems that a(n) is also the sum of the terms in one period of the base-2 MR-expansion of 1/n (see A136042 for definition). - _John W. Layman_, Jan 22 2009",
				"Indices n such that a(n) divides n are listed in A068563. - _Max Alekseyev_, Aug 25 2013",
				"a(n) is the smallest k such that x^n - 1 factors into n linear polynomials over GF(2^k). For example, a(12) = 2, and x^12 - 1 = (x - 1)^4*(x - w)^4*(x - (w + 1))^4 in GF(4), where w^2 + w + 1 = 0. - _Jianing Song_, Jan 20 2019"
			],
			"reference": [
				"Simmons, G. J. The structure of the differentiation digraphs of binary sequences. Ars Combin. 35 (1993), A, 71-88, see Table 2. Math. Rev. 95f:05052."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A007733/b007733.txt\"\u003eTable of n, a(n) for n=1..1000\u003c/a\u003e",
				"\u003ca href=\"/index/1#1overn\"\u003eIndex entries for sequences related to decimal expansion of 1/n\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A002326((A000265(n) - 1)/2). - _Max Alekseyev_, Jun 11 2009"
			],
			"mathematica": [
				"f[n_] := MultiplicativeOrder[2, n/(2^IntegerExponent[n, 2])]; Array[f, 84] (* _Robert G. Wilson v_, Jun 10 2011 *)"
			],
			"program": [
				"(PARI) a(n) = znorder(Mod(2, n/2^valuation(n, 2))); \\\\ _Michel Marcus_, Apr 11 2015",
				"(Haskell)",
				"a007733 = a002326 . flip div 2 . subtract 1 . a000265",
				"-- _Reinhard Zumkeller_, Apr 13 2015"
			],
			"xref": [
				"Cf. A136042. - _John W. Layman_, Jan 22 2009",
				"Cf. A000265, A002326, A256607, A256757."
			],
			"keyword": "nonn,easy",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_, Hal Sampson (hals(AT)easynet.com)",
			"references": 51,
			"revision": 45,
			"time": "2019-01-20T10:02:16-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}