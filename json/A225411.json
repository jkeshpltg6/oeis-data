{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A225411",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 225411,
			"data": "3,2,5,3,5,5,4,1,6,5,5,3,1,6,8,4,8,9,4,8,3,0,3,8,6,8,6,2,7,7,5,3,5,5,6,1,6,4,6,6,4,3,7,4,7,6,7,1,3,6,5,8,1,0,3,2,9,5,1,6,3,2,8,8,3,3,4,7,9,2,5,9,0,7,1,9,6,9,6,9,2,6,0,2,0,2,6,5,3,4,6,8,6,9,9,1,2,5,4,2",
			"name": "10-adic integer x such that x^3 == (6*(10^(n+1)-1)/9)+1 mod 10^(n+1) for all n.",
			"comment": [
				"This is the 10's complement of A225402.",
				"Equivalently, the 10-adic cube root of 1/3, i.e., solution to 3*x^3 = 1 (mod 10^n) for all n. - _M. F. Hasler_, Jan 02 2019"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A225411/b225411.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"formula": [
				"Define the sequence {b(n)} by the recurrence b(0) = 0 and b(1) = 3, b(n) = b(n-1) + 9 * (3 * b(n-1)^3 - 1) mod 10^n for n \u003e 1, then a(n) = (b(n+1) - b(n))/10^n. - _Seiichi Manyama_, Aug 12 2019"
			],
			"example": [
				"       3^3 ==      7 (mod 10).",
				"      23^3 ==     67 (mod 10^2).",
				"     523^3 ==    667 (mod 10^3).",
				"    3523^3 ==   6667 (mod 10^4).",
				"   53523^3 ==  66667 (mod 10^5).",
				"  553523^3 == 666667 (mod 10^6)."
			],
			"maple": [
				"op([1,3],padic:-rootp(3*x^3  -1,  10, 101)); # _Robert Israel_, Aug 04 2019"
			],
			"program": [
				"(PARI) n=0; for(i=1, 100, m=(2*(10^i-1)/3)+1; for(x=0, 9, if(((n+(x*10^(i-1)))^3)%(10^i)==m, n=n+(x*10^(i-1)); print1(x\", \"); break)))",
				"(PARI) upto(N=100,m=1/3)=Vecrev(digits(lift(chinese(Mod((m+O(5^N))^m, 5^N), Mod((m+O(2^N))^m, 2^N)))),N) \\\\ Following _Andrew Howroyd_'s code for A319740. - _M. F. Hasler_, Jan 02 2019",
				"(Ruby)",
				"def A225411(n)",
				"  ary = [3]",
				"  a = 3",
				"  n.times{|i|",
				"    b = (a + 9 * (3 * a ** 3 - 1)) % (10 ** (i + 2))",
				"    ary \u003c\u003c (b - a) / (10 ** (i + 1))",
				"    a = b",
				"  }",
				"  ary",
				"end",
				"p A225411(100) # _Seiichi Manyama_, Aug 12 2019"
			],
			"xref": [
				"Cf. A225402, A309600, A319740 (10-adic cube root of 1/11)."
			],
			"keyword": "nonn,base",
			"offset": "0,1",
			"author": "_Aswini Vaidyanathan_, May 07 2013",
			"references": 10,
			"revision": 30,
			"time": "2019-08-13T08:12:50-04:00",
			"created": "2013-05-08T23:38:49-04:00"
		}
	]
}