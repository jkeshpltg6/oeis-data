{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A052957",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 52957,
			"data": "2,2,6,8,20,32,72,128,272,512,1056,2048,4160,8192,16512,32768,65792,131072,262656,524288,1049600,2097152,4196352,8388608,16781312,33554432,67117056,134217728,268451840,536870912,1073774592,2147483648",
			"name": "Expansion of 2*(1-x-x^2)/((1-2*x)*(1-2*x^2)).",
			"link": [
				"Colin Barker, \u003ca href=\"/A052957/b052957.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=1028\"\u003eEncyclopedia of Combinatorial Structures 1028\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,2,-4)."
			],
			"formula": [
				"G.f.: 2*(1-x-x^2)/((1-2*x)*(1-2*x^2)).",
				"a(n) = 2*a(n-1) + 2*a(n-2) - 4*a(n-3).",
				"a(n) = 2^n + Sum_{alpha=RootOf(-1+2*x^2)} alpha^(-n)/2.",
				"a(n) = 2*A051437(n+1), n \u003e 0. - _R. J. Mathar_, Nov 27 2011",
				"From _Colin Barker_, Sep 23 2016: (Start)",
				"a(n) = 2^(n/2) + 2^n for n even.",
				"a(n) = 2^n for n odd.",
				"(End)",
				"E.g.f.: (1/2)*(2*exp(2*x) + exp(-sqrt(2)*x) + exp(sqrt(2)*x)). - _Stefano Spezia_, Oct 22 2019"
			],
			"maple": [
				"spec:= [S,{S=Union(Sequence(Prod(Union(Z,Z),Z)),Sequence(Union(Z,Z)))}, unlabeled ]: seq(combstruct[count ](spec,size=n), n=0..20);",
				"seq(coeff(series(2*(1-x-x^2)/((1-2*x)*(1-2*x^2)), x, n+1), x, n), n = 0 .. 40); # _G. C. Greubel_, Oct 22 2019"
			],
			"mathematica": [
				"CoefficientList[Series[2*(1-x-x^2)/((1-2*x)*(1-2*x^2)), {x, 0, 31}], x] (* _Michael De Vlieger_, Sep 23 2016 *)",
				"Join[{2}, Table[2^n +2^((n-1)/2)*(1+(-1)^n)/Sqrt[2], {n, 30}]] (* _G. C. Greubel_, Oct 22 2019 *)",
				"LinearRecurrence[{2,2,-4},{2,2,6},40] (* _Harvey P. Dale_, Jul 19 2020 *)"
			],
			"program": [
				"(PARI) a(n)=2^n+if(n%2,,2^(n/2)) \\\\ _Charles R Greathouse IV_, Sep 23 2016",
				"(MAGMA) [2] cat [Round(2^n +2^((n-1)/2)*(1+(-1)^n)/Sqrt(2)): n in [1..30]]; // _G. C. Greubel_, Oct 22 2019",
				"(Sage) [2]+[2^n +2^((n-1)/2)*(1+(-1)^n)/sqrt(2) for n in (1..30)] # _G. C. Greubel_, Oct 22 2019",
				"(GAP) a:=[2,2,6];; for n in [4..30] do a[n]:=2*a[n-1]+2*a[n-2]-4*a[n-3]; od; a; # _G. C. Greubel_, Oct 22 2019"
			],
			"keyword": "easy,nonn",
			"offset": "0,1",
			"author": "encyclopedia(AT)pommard.inria.fr, Jan 25 2000",
			"ext": [
				"More terms from _James A. Sellers_, Jun 05 2000"
			],
			"references": 2,
			"revision": 35,
			"time": "2020-07-19T11:45:21-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}