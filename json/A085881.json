{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A085881",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 85881,
			"data": "1,1,1,3,6,3,15,45,45,15,105,420,630,420,105,945,4725,9450,9450,4725,945,10395,62370,155925,207900,155925,62370,10395,135135,945945,2837835,4729725,4729725,2837835,945945,135135,2027025,16216200,56756700,113513400,141891750,113513400,56756700,16216200,2027025",
			"name": "Triangle T(n,k) read by rows: multiply row n of Pascal's triangle (A007318) by A001147(n).",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A085881/b085881.txt\"\u003eRows n = 0..100 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"Triangle given by [1, 2, 3, 4, 5, 6, ...] DELTA [1, 2, 3, 4, 5, 6, ...] where DELTA is Deléham's operator defined in A084938.",
				"T(n,k) = A164961(n,k)/2^n. - _Philippe Deléham_, Jan 07 2012",
				"Recurrence equation: T(n+1,k) = (2*n+1)*(T(n,k) + T(n,k-1)). - _Peter Bala_, Jul 15 2012",
				"E.g.f.: 1/sqrt(1-2*x-2*x*y). - _Peter Bala_, Jul 15 2012",
				"G.f.: W(0), where W(k) = 1 - (k+1)*x*(1+y)/( (k+1)*x*(1+y) - 1/W(k+1) ); (continued fraction). - _Sergei N. Gladkovskii_, Nov 03 2013"
			],
			"example": [
				"Triangle starts:",
				"       1;",
				"       1,      1;",
				"       3,      6,       3;",
				"      15,     45,      45,      15;",
				"     105,    420,     630,     420,     105;",
				"     945,   4725,    9450,    9450,    4725,     945;",
				"   10395,  62370,  155925,  207900,  155925,   62370,  10395;",
				"  135135, 945945, 2837835, 4729725, 4729725, 2837835, 945945, 135135;",
				"  ..."
			],
			"maple": [
				"T:= (n, k)-\u003e n!/2^n*binomial(n, k)*binomial(2*n, n):",
				"seq(seq(T(n, k), k=0..n), n=0..10); # _Yu-Sheng Chang_, Jan 16 2020"
			],
			"mathematica": [
				"Table[Binomial[n, k]*(2*n-1)!!, {n,0,10}, {k,0,n}]//Flatten (* _G. C. Greubel_, Feb 07 2020 *)"
			],
			"program": [
				"(PARI) T(n,k) = binomial(n,k)*binomial(2*n,n)*n!/2^n;",
				"for(n=0,10, for(k=0,n, print1(T(n,k), \", \"))) \\\\ _G. C. Greubel_, Feb 07 2020",
				"(MAGMA) [Binomial(n,k)*Binomial(2*n,n)*Factorial(n)/2^n: k in [0..n], n in [0..10]]; // _G. C. Greubel_, Feb 07 2020",
				"(Sage) [[binomial(n,k)*(2*n-1).multifactorial(2) for k in (0..n)] for n in (0..10)] # _G. C. Greubel_, Feb 07 2020",
				"(GAP) Flat(List([0..10], n-\u003e List([0..n], k-\u003e Binomial(n,k)*Binomial(2*n,n) *Factorial(n)/2^n ))); # _G. C. Greubel_, Feb 07 2020"
			],
			"xref": [
				"Cf. A001147, A007318, A084938, A164961."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_, Aug 17 2003",
			"references": 3,
			"revision": 30,
			"time": "2020-02-07T16:49:44-05:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}