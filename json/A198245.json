{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A198245",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 198245,
			"data": "149,241,2946901,16467631,17613227,327784727,426369739,1062232319",
			"name": "Primes p that divide E(p - 3), where E(k) is the k-th Euler number.",
			"comment": [
				"The even-indexed Euler numbers are A028296, the odd-indexed Euler numbers are all zero.",
				"Numerous combinatorial congruences recently obtained by Z. W. Sun and by Z. H. Sun contain the Euler numbers E(p-3) with a prime p.",
				"Only three primes less than 3 * 10^6 satisfy this condition (the current members of the sequence).",
				"Such primes have been recently suggested by Z. W. Sun; namely, Sun found the first and the second such primes, 149 and 241, and used them to discover new congruences involving E(p - 3).",
				"This is reported by Zhi Wei Sun on Feb 08 2010 and the third prime was found by Romeo Mestrovic (on Sep 26 2011).",
				"Mestrovic (2012) computes that only three primes \u003c 10^7 are in the sequence, but he conjectures that the sequence is infinite. - _Jonathan Sondow_, Dec 18 2012",
				"If it exists, a(9) \u003e 2 * 10^9. - _Hiroaki Yamanouchi_, Aug 06 2017",
				"Hathi et al. give a(3) as 2124679 and claim that the terms 2124679, 16467631, 17613227 were reported in Cosgrave, Dilcher, 2013, but 2124679 does not appear in table 2 in that paper. How is 2124679 related to this sequence? Note that 2124679 is the second Wolstenholme prime (A088164). - _Felix Fröhlich_, Apr 27 2021"
			],
			"link": [
				"John B. Cosgrave and Karl Dilcher, \u003ca href=\"https://doi.org/10.4064/aa161-1-4\"\u003eOn a congruence of Emma Lehmer related to Euler numbers\u003c/a\u003e, Acta Arithmetica 161 (2013), 47-67.",
				"Shehzad Hathi, Michael J. Mossinghoff, and Timothy S. Trudgian, \u003ca href=\"https://arxiv.org/abs/2101.11157\"\u003eWolstenholme and Vandiver primes\u003c/a\u003e, arXiv:2101.11157 [math.NT], 2021.",
				"R. J. McIntosh and E. L. Roettger, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-07-01955-2\"\u003eA search for Fibonacci-Wieferich and Wolstenholme primes\u003c/a\u003e, Math. Comp. vol 76, no 260 (2007) pp 2087-2094.",
				"Romeo Mestrovic, \u003ca href=\"http://arxiv.org/abs/1109.2340\"\u003eAn extension of a congruence by Kohnen\u003c/a\u003e, arXiv: 1109.2340v3 [math.NT] (2011).",
				"Romeo Mestrovic, \u003ca href=\"http://arxiv.org/abs/1212.3602\"\u003eA search for primes p such that Euler number E(p-3) is divisible by p\u003c/a\u003e, arXiv: 1212.3602 [math.NT] (2012).",
				"Zhi Wei Sun, \u003ca href=\"https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;62875835.1002\"\u003eLetter to the Number Theory List\u003c/a\u003e, Feb 8 2010",
				"Zhi Wei Sun, \u003ca href=\"http://arxiv.org/abs/1001.4453\"\u003eSuper congruences and Euler numbers\u003c/a\u003e, Sci. China Math., 54 (2011), in press, arXiv: 1001.4453 [math.NT] (2011).",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/EulerNumber.html\"\u003eEuler Number\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Euler_number\"\u003eEuler Number\u003c/a\u003e.",
				"Hiroaki Yamanouchi, \u003ca href=\"/A198245/a198245.txt\"\u003ePrimes p (5 \u003c= p \u003c 2*10^9) such that E(p-3) == A (mod p) for some integer A in [-1000, 1000].\u003c/a\u003e"
			],
			"mathematica": [
				"Select[Prime[Range[2, 200]], IntegerQ[EulerE[# - 3]/#] \u0026] (* _Alonso del Arte_, Oct 31 2011 *)"
			],
			"xref": [
				"Cf. A000364, A088164, A092217, A092218, A120115, A120337."
			],
			"keyword": "nonn,more",
			"offset": "1,1",
			"author": "_Romeo Mestrovic_, Oct 22 2011",
			"ext": [
				"a(4)-a(8) from _Hiroaki Yamanouchi_, Aug 06 2017"
			],
			"references": 3,
			"revision": 55,
			"time": "2021-10-06T17:55:39-04:00",
			"created": "2011-10-31T18:20:17-04:00"
		}
	]
}