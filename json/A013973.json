{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A013973",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 13973,
			"data": "1,-504,-16632,-122976,-532728,-1575504,-4058208,-8471232,-17047800,-29883672,-51991632,-81170208,-129985632,-187132176,-279550656,-384422976,-545530104,-715608432,-986161176,-1247954400,-1665307728,-2066980608,-2678616864,-3243917376,-4159663200",
			"name": "Expansion of Eisenstein series E_6(q) (alternate convention E_3(q)).",
			"comment": [
				"Ramanujan Lambert series: P(q) (see A006352), Q(q) (A004009), R(q) (A013973)."
			],
			"reference": [
				"W. Ebeling, Lattices and Codes, Vieweg; 2nd ed., 2002, see p. 53.",
				"R. C. Gunning, Lectures on Modular Forms. Princeton Univ. Press, Princeton, NJ, 1962, p. 53.",
				"M. Kaneko and D. Zagier, Supersingular j-invariants, hypergeometric series and Atkin's orthogonal polynomials, pp. 97-126 of D. A. Buell and J. T. Teitelbaum, eds., Computational Perspectives on Number Theory, Amer. Math. Soc., 1998.",
				"N. Koblitz, Introduction to Elliptic Curves and Modular Forms, Springer-Verlag, 1984, see p. 111.",
				"Jean-Pierre Serre, \"A Course in Arithmetic\", Springer, 1978",
				"Joseph H. Silverman, \"Advanced Topics in the Arithmetic of Elliptic Curves\", Springer, 1994"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A013973/b013973.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e (terms 0..1000 from T. D. Noe)",
				"R. E. Borcherds, \u003ca href=\"https://math.berkeley.edu/~reb/papers/icm94/icm94.pdf\"\u003eAutomorphic forms on O_{s+2,2}(R)^{+} and generalized Kac-Moody algebras\u003c/a\u003e, pp. 744-752 of Proc. Intern. Congr. Math., Vol. 2, 1994.",
				"D. Bump, \u003ca href=\"https://doi.org/10.1017/CBO9780511609572\"\u003eAutomorphic Forms and Representations\u003c/a\u003e, Cambr. Univ. Press, 1997, p. 29.",
				"Heng Huat Chan, Shaun Cooper, and Pee Choon Toh, \u003ca href=\"http://unimodular.net/archive/RamEisenstein.pdf\"\u003eRamanujan's Eisenstein series and powers of Dedekind's eta-function\u003c/a\u003e, Journal of the London Mathematical Society 75.1 (2007): 225-242. See R(q).",
				"Masao Koike, \u003ca href=\"https://oeis.org/A004016/a004016.pdf\"\u003eModular forms on non-compact arithmetic triangle groups\u003c/a\u003e, Unpublished manuscript [Extensively annotated with OEIS A-numbers by N. J. A. Sloane, Feb 14 2021. I wrote 2005 on the first page but the internal evidence suggests 1997.]",
				"H. Ochiai, \u003ca href=\"http://arXiv.org/abs/math-ph/9909023\"\u003eCounting functions for branched covers of elliptic curves and quasi-modular forms\u003c/a\u003e, arXiv:math-ph/9909023, 1999.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/EisensteinSeries.html\"\u003eEisenstein Series.\u003c/a\u003e",
				"\u003ca href=\"/index/Ed#Eisen\"\u003eIndex entries for sequences related to Eisenstein series\u003c/a\u003e"
			],
			"formula": [
				"E6(q) = 1 - 504*Sum_{i\u003e=1} sigma_5(i)q^i where sigma_5(n) is A001160, the sum of fifth powers of the divisors of n. It can also be expressed as E6(q) = 1 - 504*Sum_{i\u003e=1} i^5*q^i/(1-q^i). - _Gene Ward Smith_, Aug 22 2006",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2), A(x^4)) where f(u, v, w) = u^2*v - 8*u^2*w - 66*u*v^2 + 592*u*v*w - 512*u*w^2 + 121*v^3 - 4224*v^2*w + 4096*v*w^2. - _Michael Somos_, Apr 10 2005",
				"Expansion of Ramanujan's function R(q) = 216*g3 (Weierstrass invariant).",
				"Expansion of (eta(q)^8 + 32 * eta(q^4)^8) * (eta(q)^16 - 512 * eta(q)^8 * eta(q^4)^8 - 8192 * eta(q^4)^16) / eta(q^2)^12 in powers of q. - _Michael Somos_, Dec 30 2008",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / t) = - (t/i)^6 * f(t) where q = exp(2 Pi i t). - _Michael Somos_, Dec 30 2008",
				"E6(q) = eta(q)^24 / eta(q^2)^12 - 480 * eta(q^2)^12 - 16896 * eta(q^2)^12 * eta(q^4)^8 / eta(q)^8 + 8192 * eta(q^4)^24 / eta(q^2)^12. - _Seiichi Manyama_, May 08 2017"
			],
			"example": [
				"G.f. = 1 - 504*q - 16632*q^2 - 122976*q^3 - 532728*q^4 - 1575504*q^5 + ..."
			],
			"maple": [
				"E := proc(k) local n,t1; t1 := 1-(2*k/bernoulli(k))*add(sigma[k-1](n)*q^n,n=1..60); series(t1,q,60); end; E(6);",
				"# alternative",
				"A013973 := proc(n)",
				"    if n = 0 then",
				"        1;",
				"    else",
				"        -504*numtheory[sigma][5](n) ;",
				"    end if;",
				"end proc:",
				"seq(A013973(n),n=0..10) ; # _R. J. Mathar_, Feb 22 2021"
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 1, Boole[n == 0], -504 DivisorSigma[ 5, n]]; (* _Michael Somos_, Apr 21 2013 *)",
				"a[ n_] := SeriesCoefficient[ With[ {t2 = EllipticTheta[ 2, 0, q]^4, t3 = EllipticTheta[ 3, 0, q]^4}, t2^3 - 33 (t2 + t3) t2 t3 + t3^3], {q, 0, n}]; (* _Michael Somos_, Apr 21 2013 *)",
				"a[ n_] := SeriesCoefficient[ With[ {t3 = EllipticTheta[ 3, 0, q]^4, t4 = EllipticTheta[ 4, 0, q]^4}, (t3^3 - 3 (t3 - t4)^2 (t3 + t4) + t4^3) / 2], {q, 0, 2 n}]; (* _Michael Somos_, Jun 04 2014 *)",
				"a[ n_] := SeriesCoefficient[ With[ {e1 = QPochhammer[ q]^8, e4 = 32 q QPochhammer[ q^4]^8}, (e1 + e4) (e1^2 - 16 e1 e4 - 8 e4^2) / QPochhammer[ q^2]^12], {q, 0, n}]; (* _Michael Somos_, Apr 01 2015 *)",
				"a[ n_] := SeriesCoefficient[ With[ {t2 = EllipticTheta[ 2, 0, q]^4, t3 = EllipticTheta[ 3, 0, q]^4}, t2^3 - 3/2 (t2 + t3) t2 t3 + t3^3], {q, 0, 2 n}]; (* _Michael Somos_, Jul 31 2016 *)",
				"terms = 25; E6[x_] = 1-(12/BernoulliB[6])*Sum[k^5*x^k/(1-x^k), {k, terms}]; CoefficientList[E6[x] + O[x]^terms, x] (* _Jean-François Alcover_, Feb 28 2018 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n==0, -504 * sigma( n, 5))};",
				"(PARI) {a(n) = my(A, A1, A4); if( n\u003c0, 0, A = x * O(x^n); A1 = eta(x + A)^8; A4 = 32 * x * eta(x^4 + A)^8; polcoeff( (A1 + A4) * (A1^2 - 16 * A1 * A4 - 8 * A4^2) / eta(x^2 + A)^12, n))}; /* _Michael Somos_, Dec 30 2008 */",
				"(Sage) ModularForms( Gamma1(1), 6, prec=25).0; # _Michael Somos_, Jun 04 2013",
				"(MAGMA) Basis( ModularForms( Gamma1(1), 6), 25); /* _Michael Somos_, Apr 01 2015 */"
			],
			"xref": [
				"Cf. A004009, A008410, A013974, A145095.",
				"Cf. A006352 (E_2), A004009 (E_4), A013973 (E_6), A008410 (E_8), A013974 (E_10), A029828 (E_12), A058550 (E_14), A029829 (E_16), A029830 (E_20), A029831 (E_24).",
				"Cf. A001160, A286346 (eta(q)^24 / eta(q^2)^12), A286399 (eta(q^2)^12 * eta(q^4)^8 / eta(q)^8)."
			],
			"keyword": "sign,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 149,
			"revision": 76,
			"time": "2021-02-22T09:49:36-05:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}