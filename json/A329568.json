{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A329568",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 329568,
			"data": "1,2,3,9,4,10,27,14,33,57,26,40,87,50,21,63,16,20,51,8,81,93,46,56,15,58,135,183,28,44,39,88,69,123,34,68,105,128,45,129,22,52,141,38,75,159,32,82,99,64,117,147,80,94,177,116,237,273,74,100,387,76,207,357,62,104,165,86,77,95",
			"name": "For all n \u003e= 1, exactly 9 sums are prime among a(n+i) + a(n+j), 0 \u003c= i \u003c j \u003c 6: lexicographically earliest such sequence of distinct positive numbers.",
			"comment": [
				"That is, there are nine primes, counted with multiplicity, among the 15 pairwise sums of any six consecutive terms. This is the maximum number of possible prime sums for any set of 6 numbers \u003e 1, see wiki page for details.",
				"Conjectured to be a permutation of the positive integers. See A329569 = (0, 1, 2, 5, 6, 11, 12, 17, ...) for the quite different variant for nonnegative integers.",
				"For n \u003e 6, a(n) is the smallest number not used earlier such that the set a(n) + {a(n-5}, ..., a(n-1)} has the same number of primes as a(n-6) + {a(n-5), ..., a(n-1)}. Such a number always exists, by definition of the sequence. (If it would not exist for a given n, the term a(n-1) (or earlier) \"was wrong and must be corrected\", so to say.) See the wiki page for further considerations about existence and surjectivity.",
				"For a(4), one must exclude the values {4, ..., 8} to get an infinite sequence, but for all other (at least several hundred) terms, the greedy choice gives the correct solution."
			],
			"link": [
				"Éric Angelini, \u003ca href=\"http://list.seqfan.eu/pipermail/seqfan/2019-November/020145.html\"\u003ePrime sums from neighbouring terms\u003c/a\u003e, SeqFan list, Nov 11 2019.",
				"M. F. Hasler, \u003ca href=\"/wiki/User:M._F._Hasler/Prime_sums_from_neighboring_terms\"\u003ePrime sums from neighboring terms\u003c/a\u003e, OEIS Wiki, Nov 23 2019."
			],
			"program": [
				"(PARI) {A329568(n,show=0,o=1,N=9,M=5,X=[[4,x]|x\u003c-[4..8]],p=[],u=o,U)=for(n=o+1,n, show\u003e0\u0026\u0026 print1(o\",\"); show\u003c0\u0026\u0026 listput(L,o); U+=1\u003c\u003c(o-u); U\u003e\u003e=-u+u+=valuation(U+1,2); p=concat(if(#p\u003e=M,p[^1],p),o); my(c=N-sum(i=2,#p, sum(j=1,i-1, isprime(p[i]+p[j])))); for(k=u,oo,bittest(U,k-u)|| min(c-#[0|x\u003c-p,isprime(x+k)],#p\u003e=M)|| setsearch(X,[n,k])|| [o=k,break])); show\u0026\u0026print([u]);o} \\\\ optional args: show=1: print a(o..n-1), show=-1: append them on global list L, in both cases print [least unused number] at the end. Parameters N,M,o,... allow to get other variants, see the wiki page for more."
			],
			"xref": [
				"Cf. A055265, A128280 (1 prime from 2 terms), A329333 (1 prime from 3 terms), A329405, ..., A329417 (N primes from M terms \u003e= 1), A329425, A329449, ..., A329581 (N primes from M terms \u003e= 0)."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_M. F. Hasler_, Feb 10 2020",
			"references": 4,
			"revision": 16,
			"time": "2020-03-07T02:03:11-05:00",
			"created": "2020-03-07T02:03:11-05:00"
		}
	]
}