{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A209815",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 209815,
			"data": "0,1,4,10,23,47,90,164,288,488,807,1303,2063,3210,4920,7434,11098,16380,23928,34624,49668,70667,99795,139935,194930,269857,371413,508363,692195,937838,1264685,1697810,2269557,3021462,4006812,5293650,6968730,9142306,11954194",
			"name": "Number of partitions of 2n in which every part is \u003cn; also, the number of partitions of 2 into rational numbers a/b such that 0\u003ca\u003cb\u003c=n and b divides n.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A209815/b209815.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A008284(3*n-1,n-1). - _Hans Loeblich_ Apr 18 2019"
			],
			"example": [
				"The 4 partitions of 6 with parts \u003c3:",
				"2+2+2, 2+2+1+1, 2+1+1+1+1, 1+1+1+1+1+1.",
				"Matching partitions of 2 into rationals as described:",
				"2/3 + 2/3 + 2/3",
				"2/3 + 2/3 + 1/3 + 1/3",
				"2/3 + 1/3 + 1/3 + 1/3 + 1/3",
				"1/3 + 1/3 + 1/3 + 1/3 + 1/3 + 1/3."
			],
			"maple": [
				"b:= proc(n, i) option remember; `if`(n=0, 1,",
				"      `if`(i\u003c1, 0, b(n, i-1)+`if`(i\u003en, 0, b(n-i, i))))",
				"    end:",
				"a:= n-\u003e b(2*n, n-1):",
				"seq(a(n), n=1..50);  # _Alois P. Heinz_, Jul 09 2012"
			],
			"mathematica": [
				"f[n_] := Length[Select[IntegerPartitions[2 n], First[#] \u003c= n - 1 \u0026]];  Table[f[n], {n, 1, 34}]  (* A209815 *)",
				"b[n_, i_] := b[n, i] = If[n==0, 1, If[i\u003c1, 0, b[n, i-1] + If[i\u003en, 0, b[n-i, i]]]]; a[n_] := b[2*n, n-1]; Table [a[n], {n, 1, 50}] (* _Jean-François Alcover_, Oct 28 2015, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(Haskell)",
				"a209815 n = p [1..n-1] (2*n) where",
				"   p _          0 = 1",
				"   p []         _ = 0",
				"   p ks'@(k:ks) m = if m \u003c k then 0 else p ks' (m - k) + p ks m",
				"-- _Reinhard Zumkeller_, Nov 14 2013"
			],
			"xref": [
				"Cf. A209816.",
				"Cf. A231429."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Clark Kimberling_, Mar 13 2012",
			"ext": [
				"More terms from _Alois P. Heinz_, Jul 09 2012"
			],
			"references": 4,
			"revision": 22,
			"time": "2019-04-18T03:23:12-04:00",
			"created": "2012-03-18T19:13:04-04:00"
		}
	]
}