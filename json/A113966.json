{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A113966",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 113966,
			"data": "1,3,5,2,6,10,4,7,9,11,8,13,15,17,12,19,14,18,22,16,21,23,20,26,29,24,31,25,27,32,35,33,28,34,30,37,39,41,36,43,38,42,46,40,47,44,49,45,51,53,48,55,52,57,50,54,58,61,56,59,62,65,63,67,60,68,71,64,69,73,66,70",
			"name": "a(1)=1; for n\u003e1, a(n) is the smallest positive integer not occurring earlier in the sequence such that |a(n)-a(n-1)| does not divide a(n).",
			"comment": [
				"Sequence is a permutation of the positive integers.",
				"Proof that every number must eventually appear: Suppose not, let k be smallest number that never appears. Then for every a(n-1) \u003e k, we have a(n-1)-k | k, i.e. i(a(n-1)-k) = k for some i with 1 \u003c= i \u003c= k. Therefore a(n-1) \u003c= (k+ik)/k \u003c= k(k+1). So once a(n-1) \u003e k(k+1), we will have a(n) = k, a contradiction. - _N. J. A. Sloane_, Jan 29 2006"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A113966/b113966.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"example": [
				"Among those positive integers not among the first 4 integers of the sequence, a(5) = 6 is the smallest such that |a(5)-a(4)| = |6-2| = 4 does not divide a(5) =6. 4, for example, is not among the first 4 terms of the sequence, but |4-2| = 2 does divide 4. So a(5) is not 4, but is instead 6."
			],
			"mathematica": [
				"f[l_] := Block[{k=1}, While[MemberQ[l, k] || Mod[k, Abs[k - Last[l]]] == 0, k++ ]; Return[Append[l, k]]; ]; Nest[f, {1}, 100] (* _Ray Chandler_, Nov 13 2005 *)"
			],
			"program": [
				"(Haskell)",
				"import Data.List (delete)",
				"a113966 n = a113966_list !! (n-1)",
				"a113966_list = 1 : f [2..] [1] where",
				"   f xs ys'@(y:ys) = y' : f (delete y' xs) (y':ys') where",
				"     y' = head [z | z \u003c- xs, y `mod` abs (z - y) \u003e 0]",
				"-- _Reinhard Zumkeller_, Feb 26 2013"
			],
			"xref": [
				"Cf. A113963, A113967.",
				"Cf. A222622 (inverse), A222623 (fixed points)."
			],
			"keyword": "nonn,nice",
			"offset": "1,2",
			"author": "_Leroy Quet_, Nov 10 2005",
			"ext": [
				"Extended by _Jim Nastos_ and _Ray Chandler_, Nov 13 2005"
			],
			"references": 5,
			"revision": 18,
			"time": "2019-10-11T16:31:44-04:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}