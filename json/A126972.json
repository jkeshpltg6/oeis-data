{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A126972",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 126972,
			"data": "1,1,2,4,11,21,36,57,85,121,166,221,287,365,456,561,681,817,970,1141,1331,1541,1772,2025,2301,2601,2926,3277,3655,4061,4496,4961,5457,5985,6546,7141,7771,8437,9140,9881,10661,11481,12342,13245,14191,15181,16216",
			"name": "Number of distinct values taken by the entropy for permutations of [1..n], where the entropy of a permutation pi is Sum_{k=1..n} (pi(k)-k)^2.",
			"comment": [
				"Also, number of distinct values taken by sum(k=1..n, k * pi(k) ). - _Joerg Arndt_, Apr 22 2011",
				"For n\u003e=4, sum(k=1..n, k * pi(k) ) takes every value in the interval [A000292(n),A000330(n)] (cf. A175929). - _Max Alekseyev_, Jan 28 2012"
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"https://arxiv.org/abs/1811.10503\"\u003eOn permutations of {1, ..., n} and related topics\u003c/a\u003e, arXiv:1811.10503 [math.CO], 2018.",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-6,4,-1)."
			],
			"formula": [
				"For n\u003e=4, a(n) = 1 + binomial(n+1,3) = 1 + A000330(n) - A000292(n) = 1 + A000292(n-1).",
				"G.f.: -(x^7-4*x^6+6*x^5-4*x^4+2*x^3-4*x^2+3*x-1)/(x-1)^4. - _M. F. Hasler_, Jan 12 2012"
			],
			"example": [
				"For 24 permutations of {1,2,3,4}, the set of sum(k=1..n, (pi(k)-k)^2) yields {0,2,4,6,8,10,12,14,16,18,20} (11 distinct values).",
				"For 120 permutations of {1,2,3,4,5}, the set of sum(k=1..n, (pi(k)-k)^2) yields {0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,36,38,40} (21 values)."
			],
			"mathematica": [
				"LinearRecurrence[{4,-6,4,-1},{1,1,2,4,11,21,36,57},50] (* _Harvey P. Dale_, Jun 01 2016; a(0)=1 prepended by _Georg Fischer_, Apr 10 2019 *)"
			],
			"program": [
				"(PARI) A126972(n)=(n!=3)+binomial(n+1,3)  \\\\ - _M. F. Hasler_, Jan 29 2012",
				"(PARI) /* the following inefficient code is for illustrative purpose only: */ A126972(n)={my(u=0,v=vector(n,i,i),t); sum(k=1,n!, !bittest(u,t=norml2(numtoperm(n,k)-v)) \u0026 u+=1\u003c\u003ct) } /* _M. F. Hasler_, Jan 29 2012 */"
			],
			"xref": [
				"Cf. A007920 (largest permutation entropy), A000292 (average permutation entropy), A135298, A175929."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "Jeff Boscole (jazzerciser(AT)hotmail.com), Mar 20 2007",
			"ext": [
				"Formula corrected by Joel Brewster Lewis, Aug 18 2009.",
				"Terms corrected, more terms added, and definition clarified by _Joerg Arndt_, Apr 22 2011.",
				"a(0)=1 prepended by _Alois P. Heinz_, Jan 22 2019"
			],
			"references": 4,
			"revision": 39,
			"time": "2019-04-10T13:26:08-04:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}