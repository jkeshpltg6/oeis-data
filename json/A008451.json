{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008451",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8451,
			"data": "1,14,84,280,574,840,1288,2368,3444,3542,4424,7560,9240,8456,11088,16576,18494,17808,19740,27720,34440,29456,31304,49728,52808,43414,52248,68320,74048,68376,71120,99456,110964,89936,94864,136080,145222",
			"name": "Number of ways of writing n as a sum of 7 squares.",
			"reference": [
				"E. Grosswald, Representations of Integers as Sums of Squares. Springer-Verlag, NY, 1985, p. 121.",
				"G. H. Hardy and E. M. Wright, An Introduction to the Theory of Numbers. 3rd ed., Oxford Univ. Press, 1954, p. 314."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A008451/b008451.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Philippe A. J. G. Chevalier, \u003ca href=\"https://www.researchgate.net/publication/236594822_On_the_discrete_geometry_of_physical_quantities\"\u003eOn the discrete geometry of physical quantities\u003c/a\u003e, 2013",
				"P. A. J. G. Chevalier, \u003ca href=\"http://www.researchgate.net/profile/Philippe_Chevalier2/publication/262067273_A_table_of_Mendeleev_for_physical_quantities/links/0c9605368f6d191478000000.pdf\"\u003eA \"table of Mendeleev\" for physical quantities?\u003c/a\u003e, Slides from a talk, May 14 2014, Leuven, Belgium.",
				"Shi-Chao Chen, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2010.01.011\"\u003eCongruences for rs(n)\u003c/a\u003e, Journal of Number Theory, Volume 130, Issue 9, September 2010, Pages 2028-2032.",
				"S. C. Milne, \u003ca href=\"http://dx.doi.org/10.1023/A:1014865816981\"\u003eInfinite families of exact sums of squares formulas, Jacobi elliptic functions, continued fractions and Schur functions\u003c/a\u003e, Ramanujan J., 6 (2002), 7-149.",
				"\u003ca href=\"/index/Su#ssq\"\u003eIndex entries for sequences related to sums of squares\u003c/a\u003e"
			],
			"formula": [
				"G.f.: theta_3(0,x)^7, where theta_3 is the third Jacobi theta function. - _Robert Israel_, Jul 16 2014",
				"a(n) = (14/n)*Sum_{k=1..n} A186690(k)*a(n-k), a(0) = 1. - _Seiichi Manyama_, May 27 2017"
			],
			"maple": [
				"series((sum(x^(m^2),m=-10..10))^7, x, 101);",
				"# Alternative",
				"#(requires at least Maple 17, and only works as long as a(n) \u003c= 10^16 or so):",
				"N:= 1000: # to get a(0) to a(N)",
				"with(SignalProcessing):",
				"A:= Vector(N+1,datatype=float[8],i-\u003e piecewise(i=1,1,issqr(i-1),2,0)):",
				"A2:= Convolution(A,A)[1..N+1]:",
				"A4:= Convolution(A2,A2)[1..N+1]:",
				"A5:= Convolution(A,A4)[1..N+1];",
				"A7:= Convolution(A2,A5)[1..N+1];",
				"map(round,convert(A7,list)); # _Robert Israel_, Jul 16 2014",
				"# Alternative",
				"A008451list := proc(len) series(JacobiTheta3(0, x)^7, x, len+1);",
				"seq(coeff(%,x,j), j=0..len-1) end: A008451list(37); # _Peter Luschny_, Oct 02 2018"
			],
			"mathematica": [
				"Table[SquaresR[7, n], {n, 0, 36}] (* _Ray Chandler_, Nov 28 2006 *)",
				"SquaresR[7,Range[0,50]] (* _Harvey P. Dale_, Aug 26 2011 *)"
			],
			"program": [
				"(Sage)",
				"Q = DiagonalQuadraticForm(ZZ, [1]*7)",
				"Q.representation_number_list(37) # _Peter Luschny_, Jun 20 2014"
			],
			"xref": [
				"Row d=7 of A122141 and of A319574, 7th column of A286815."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Extended by _Ray Chandler_, Nov 28 2006"
			],
			"references": 17,
			"revision": 55,
			"time": "2019-12-05T04:14:16-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}