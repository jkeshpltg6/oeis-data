{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A077610",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 77610,
			"data": "1,1,2,1,3,1,4,1,5,1,2,3,6,1,7,1,8,1,9,1,2,5,10,1,11,1,3,4,12,1,13,1,2,7,14,1,3,5,15,1,16,1,17,1,2,9,18,1,19,1,4,5,20,1,3,7,21,1,2,11,22,1,23,1,3,8,24,1,25,1,2,13,26,1,27,1,4,7,28,1,29,1,2,3,5,6,10,15,30",
			"name": "Triangle in which n-th row lists unitary divisors of n.",
			"comment": [
				"n-th row = n-th row of A165430 without repetitions. - _Reinhard Zumkeller_, Mar 04 2013",
				"Denominators of sequence of all positive rational numbers ordered as follows:  let m = p(i(1))^e(i(1))*...*p(i(k))^e(i(k)) be the prime factorization of m.  Let S(m) be the vector of rationals p(i(k+1-j))^e(i(k+1-j))/p(i(j))^e(i(j)) for j = 1..k.  The sequence (a(n)) is the concatenation of vectors S(m) for m = 1, 2, ...; for numerators see A229994. - _Clark Kimberling_, Oct 31 2013"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A077610/b077610.txt\"\u003eRows n=1..1000 of triangle, flattened\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/UnitaryDivisor.html\"\u003eUnitary Divisor\u003c/a\u003e",
				"\u003ca href=\"/index/Ra#rational\"\u003eIndex entries for sequences related to enumerating the rationals\u003c/a\u003e"
			],
			"example": [
				"1;",
				"1, 2;",
				"1, 3;",
				"1, 4;",
				"1, 5;",
				"1, 2, 3, 6;",
				"1, 7;",
				"1, 8;",
				"1, 9;",
				"1, 2, 5, 10;",
				"1, 11;"
			],
			"maple": [
				"with(numtheory);",
				"# returns the number of unitary divisors of n and a list of them, from _N. J. A. Sloane_, May 01 2013",
				"f:=proc(n)",
				"local ct,i,t1,ans;",
				"ct:=0; ans:=[];",
				"t1:=divisors(n);",
				"for i from 1 to nops(t1) do",
				"d:=t1[i];",
				"if igcd(d,n/d)=1 then ct:=ct+1; ans:=[op(ans),d]; fi;",
				"od:",
				"RETURN([ct,ans]);",
				"end;"
			],
			"mathematica": [
				"row[n_] := Select[ Divisors[n], GCD[#, n/#] == 1 \u0026]; Table[row[n], {n, 1, 30}] // Flatten (* _Jean-François Alcover_, Oct 22 2012 *)"
			],
			"program": [
				"(Haskell)",
				"a077610 n k = a077610_row n !! k",
				"a077610_row n = [d | d \u003c- [1..n], let (n',m) = divMod n d,",
				"                     m == 0, gcd d n' == 1]",
				"a077610_tabf = map a077610_row [1..]",
				"-- _Reinhard Zumkeller_, Feb 12 02",
				"(PARI) row(n)=my(f=factor(n),k=#f~); Set(vector(2^k,i, prod(j=1,k, if(bittest(i,j-1),1,f[j,1]^f[j,2]))))",
				"v=[];for(n=1,20,v=concat(v,row(n)));v \\\\ _Charles R Greathouse IV_, Sep 02 2015",
				"(PARI) row(n) = {my(d = divisors(n)); select(x-\u003e(gcd(x, n/x)==1), d);} \\\\ _Michel Marcus_, Oct 11 2015"
			],
			"xref": [
				"Cf. A037445, A027750, A034444 (row lengths), A034448 (row sums); A206778."
			],
			"keyword": "nonn,tabf",
			"offset": "1,3",
			"author": "_Eric W. Weisstein_, Nov 11 2002",
			"references": 58,
			"revision": 37,
			"time": "2020-03-21T04:06:32-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}