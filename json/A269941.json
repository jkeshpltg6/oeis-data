{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A269941",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 269941,
			"data": "1,0,-1,0,-1,1,0,-1,2,-1,0,-1,1,2,-3,1,0,-1,2,2,-3,-3,4,-1,0,-1,1,2,2,-1,-6,-3,6,4,-5,1,0,-1,2,2,2,-3,-3,-6,-3,4,12,4,-10,-5,6,-1",
			"name": "Triangle read by rows, the coefficients of the partial P-polynomials.",
			"comment": [
				"For the definition of the partial P-polynomials see the link 'P-transform'. The triangle of coefficients of the inverse partial P-polynomials is A269942."
			],
			"link": [
				"Peter Luschny, \u003ca href=\"http://oeis.org/wiki/User:Peter_Luschny/P-Transform\"\u003eThe P-transform\u003c/a\u003e."
			],
			"example": [
				"[[1]],",
				"[[0], [-1]],",
				"[[0], [-1], [1]],",
				"[[0], [-1], [2], [-1]],",
				"[[0], [-1], [1, 2], [-3], [1]],",
				"[[0], [-1], [2, 2], [-3, -3], [4], [-1]],",
				"[[0], [-1], [1, 2, 2], [-1, -6, -3], [6, 4], [-5], [1]],",
				"[[0], [-1], [2, 2, 2], [-3, -3, -6, -3], [4, 12, 4], [-10, -5], [6], [-1]]",
				"Replacing the sublists by their sums reduces the triangle to a signed version of the triangle A097805."
			],
			"maple": [
				"PTrans := proc(n, f, nrm:=NULL) local q, p, r, R;",
				"if n = 0 then return [1] fi; R := [seq(0,j=0..n)];",
				"for q in combinat:-partition(n) do",
				"   p := [op(ListTools:-Reverse(q)),0]; r := p[1]+1;",
				"   mul(binomial(p[j], p[j+1])*f(j)^p[j], j=1..nops(q));",
				"   R[r] := R[r]-(-1)^r*% od;",
				"if nrm = NULL then R else [seq(nrm(n,k)*R[k+1],k=0..n)] fi end:",
				"A269941_row := n -\u003e seq(coeffs(p), p in PTrans(n, n -\u003e x[n])):",
				"seq(lprint(A269941_row(n)), n=0..8);"
			],
			"program": [
				"(Sage)",
				"def PtransMatrix(dim, f, norm = None, inverse = False, reduced = False):",
				"    i = 1; F = [1]",
				"    if reduced:",
				"        while i \u003c= dim: F.append(f(i)); i += 1",
				"    else:",
				"        while i \u003c= dim: F.append(F[i-1]*f(i)); i += 1",
				"    C = [[0 for k in range(m+1)] for m in range(dim)]",
				"    C[0][0] = 1",
				"    if inverse:",
				"        for m in (1..dim-1):",
				"            C[m][m] = -C[m-1][m-1]/F[1]",
				"            for k in range(m-1, 0, -1):",
				"                C[m][k] = -(C[m-1][k-1]+sum(F[i]*C[m][k+i-1]",
				"                          for i in (2..m-k+1)))/F[1]",
				"    else:",
				"        for m in (1..dim-1):",
				"            C[m][m] = -C[m-1][m-1]*F[1]",
				"            for k in range(m-1, 0, -1):",
				"                C[m][k] = -sum(F[i]*C[m-i][k-1] for i in (1..m-k+1))",
				"    if norm == None: return C",
				"    for m in (1..dim-1):",
				"        for k in (1..m): C[m][k] *= norm(m,k)",
				"    return C",
				"def PMultiCoefficients(dim, norm = None, inverse = False):",
				"    def coefficient(p):",
				"        if p \u003c= 1: return [p]",
				"        return SR(p).fraction(ZZ).numerator().coefficients()",
				"    f = lambda n: var('x'+str(n))",
				"    P = PtransMatrix(dim, f, norm, inverse)",
				"    return [[coefficient(p) for p in L] for L in P]",
				"PMultiCoefficients(8)"
			],
			"xref": [
				"Cf. A097805, A268441, A268442, A269942."
			],
			"keyword": "sign,tabf",
			"offset": "0,9",
			"author": "_Peter Luschny_, Mar 08 2016",
			"references": 11,
			"revision": 18,
			"time": "2020-03-05T17:03:32-05:00",
			"created": "2016-03-29T03:39:18-04:00"
		}
	]
}