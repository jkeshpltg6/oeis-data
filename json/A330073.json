{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A330073",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 330073,
			"data": "1,2,15,3,20,4,25,5,1,3,20,4,25,5,1,4,25,5,1,5,1,6,40,8,50,10,2,15,3,20,4,25,5,1,7,45,9,55,11,70,14,85,17,105,21,130,26,160,32,195,39,235,47,285,57,345,69,415,83,500,100,20,4,25,5,1,8,50",
			"name": "Irregular triangle read as rows in which row n is the result of iterating the operation f(n) = n/5 if n == 0 (mod 5), otherwise f(n) = 5*(floor(n/5) + n + 1), terminating at the first occurrence of 1.",
			"comment": [
				"f(n) is the operation C(n,m) = n/m if n == 0 (mod m), m*(floor(n/m) + n + 1) otherwise where m = 5. C(n,2) is the operation in the Collatz problem (A070165) and C(n,8) is the operation in A329263.",
				"Conjecture: For any initial value n \u003e= 1, there is a number k such that f^{k}(n) = 1, where f^{0}(n) = n and f^{k}(n) = f(f^{k - 1}(n)).",
				"For any number n, if n is a power of 5 multiplied by 1, 2, 3, or 4, then there is a number k such that f^{k}(n) = 1. If n is congruent to 10, 15, 20, or 25 (mod 30) and there is a k such that f^{k}(n) = 1, then f^{k + 1}(floor(n/6)) = 1."
			],
			"formula": [
				"T(n,0) = n, T(n,k + 1) = T(n,k)/5 if T(n,k) == 0 (mod 5), 5*(T(n,k) + floor(T(n,k)/5) + 1) otherwise, for n \u003e= 1."
			],
			"example": [
				"The irregular array T(n,k) starts:",
				"n\\k   0   1   2   3   4   5   6   7   8   9   10   11   12   13 ...",
				"1:    1",
				"2:    2  15   3  20   4  25   5   1",
				"3:    3  20   4  25   5   1",
				"4:    4  25   5   1",
				"5:    5   1",
				"6:    6  40   8  50  10   2  15   3  20   4   25    5    1",
				"7:    7  45   9  55  11  70  14  85  17 105   21  130   26  160 ...",
				"8:    8  50  10   2  15   3  20   4  25   5    1",
				"9:    9  55  11  70  14  85  17 105  21 130   26  160   32  195 ...",
				"10:  10   2  15   3  20   4  25   5   1",
				"T(7,31) = 1 and T(9,29) = 1."
			],
			"mathematica": [
				"Array[NestWhileList[If[Mod[#, 5] == 0, #/5, 5 (Floor[#/5] + # + 1)] \u0026, #, # \u003e 1 \u0026] \u0026, 8] // Flatten (* _Michael De Vlieger_, Dec 01 2019 *)"
			],
			"program": [
				"(PARI) row(n)=my(N=[n],m=5);while(n\u003e1,N=concat(N,n=if(n%m,m*(n+floor(n/m)+1),n/m)));N"
			],
			"xref": [
				"Cf. A070165, A329263."
			],
			"keyword": "nonn,easy,tabf",
			"offset": "1,2",
			"author": "_Davis Smith_, Nov 30 2019",
			"references": 2,
			"revision": 16,
			"time": "2019-12-05T04:17:19-05:00",
			"created": "2019-12-05T04:17:19-05:00"
		}
	]
}