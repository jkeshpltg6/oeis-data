{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A167267",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 167267,
			"data": "1,3,2,7,5,4,12,10,8,6,19,16,14,11,9,28,24,21,18,15,13,38,34,30,26,23,20,17,50,45,41,36,32,29,25,22,63,58,53,48,43,39,35,31,27,78,72,67,61,56,51,46,42,37,33",
			"name": "Interspersion of the signature sequence of (1+sqrt(5))/2.",
			"comment": [
				"Row n is the ordered sequence of numbers k such that A084531(k)=n.  Is the difference sequence of column 1 equal to A019446? Is the difference sequence of row 1 essentially equal to A026351?",
				"As a sequence, A167267 is a permutation of the positive integers. As an array, A167267 is the joint-rank array (defined at A182801) of the numbers {i+j*r}, for i\u003e=1, j\u003e=1, where r = golden ratio = (1+sqrt(5))/2. - _Clark Kimberling_, Nov 10 2012",
				"This is a transposable interspersion; i.e., its transpose, A283734, is also an interspersion. - _Clark Kimberling_, Mar 16 2017"
			],
			"reference": [
				"Clark Kimberling, \"Fractal Sequences and Interspersions,\" Ars Combinatoria 45 (1997) 157-168."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A167267/b167267.txt\"\u003eAntidiagonals n = 1..60, flattened\u003c/a\u003e",
				"N. Carey, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL16/Carey/carey6.html\"\u003eLambda Words: A Class of Rich Words Defined Over an Infinite Alphabet\u003c/a\u003e, J. Int. Seq. 16 (2013) #13.3.4"
			],
			"formula": [
				"R(m,n) = sum{[(m-i+n+r)/r], i=1,2,...z(m,n)}, where r = (1+sqrt(5))/2 and z(m,n) = m + [(n-1)*r].  - _Clark Kimberling_, Nov 10 2012"
			],
			"example": [
				"Northwest corner:",
				"1....3....7....12...19...28...38",
				"2....5....10...16...24...34...45",
				"4....8....14...21...30...41...53",
				"6....11...18...26...36...48...61",
				"9....15...23...32...43...56...70",
				"13...20...29...39...51...65...80"
			],
			"mathematica": [
				"v = GoldenRatio;",
				"x = Table[Sum[Ceiling[i*v], {i, q}], {q, 0, end = 35}];",
				"y = Table[Sum[Ceiling[i*1/v], {i, q}], {q, 0, end}];",
				"tot[p_, q_] := x[[p + 1]] + p q + 1 + y[[q + 1]]",
				"row[r_] := Table[tot[n, r], {n, 0, (end - 1)/v}]",
				"Grid[Table[row[n], {n, 0, (end - 1)}]]",
				"(* _Norman Carey_, Jul 03 2012 *)"
			],
			"program": [
				"(PARI)",
				"\\\\ Produces the triangle when the array is read by antidiagonals",
				"r = (1+sqrt(5))/2;",
				"z = 100;",
				"s(n) = if(n\u003c1, 1, s(n - 1) + 1 + floor(n*r));",
				"p(n) = n + 1 + sum(k=0, n, floor((n - k)/r));",
				"u = v = vector(z + 1);",
				"for(n=1, 101, (v[n] = s(n - 1)));",
				"for(n=1, 101, (u[n] = p(n - 1)));",
				"w(i, j) = v[i] + u[j] + (i - 1) * (j - 1) - 1;",
				"tabl(nn) = {for(n=1, nn, for(k=1, n, print1(w(n - k + 1, k), \", \"); ); print(); ); };",
				"tabl(10) \\\\ _Indranil Ghosh_, Mar 26 2017",
				"(Python)",
				"# Produces the triangle when the array is read by antidiagonals",
				"import math",
				"from sympy import sqrt",
				"r=(1 + sqrt(5))/2",
				"def s(n): return 1 if n\u003c1 else s(n - 1) + 1 + int(math.floor(n*r))",
				"def p(n): return n + 1 + sum(int(math.floor((n - k)/r)) for k in range(n+1))",
				"v=[s(n) for n in range(101)]",
				"u=[p(n) for n in range(101)]",
				"def w(i, j): return u[i - 1] + v[j - 1] + (i - 1) * (j - 1) - 1",
				"for n in range(1, 11):",
				"    print([w(k, n - k + 1) for k in range(1, n + 1)]) # _Indranil Ghosh_, Mar 26 2017"
			],
			"xref": [
				"Cf. A001622, A084531, A084531, A283734."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Oct 31 2009",
			"references": 3,
			"revision": 31,
			"time": "2021-05-19T09:29:46-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}