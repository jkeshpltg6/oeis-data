{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A246935",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 246935,
			"data": "1,1,0,1,1,0,1,2,2,0,1,3,6,3,0,1,4,12,14,5,0,1,5,20,39,34,7,0,1,6,30,84,129,74,11,0,1,7,42,155,356,399,166,15,0,1,8,56,258,805,1444,1245,350,22,0,1,9,72,399,1590,4055,5876,3783,746,30,0",
			"name": "Number A(n,k) of partitions of n into k sorts of parts; square array A(n,k), n\u003e=0, k\u003e=0, read by antidiagonals.",
			"comment": [
				"In general, column k \u003e 1 is asymptotic to c * k^n, where c = Product_{j\u003e=1} 1/(1-1/k^j) = 1/QPochhammer[1/k,1/k]. - _Vaclav Kotesovec_, Mar 19 2015"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A246935/b246935.txt\"\u003eAntidiagonals n = 0..140, flattened\u003c/a\u003e"
			],
			"formula": [
				"G.f. of column k: Product_{i\u003e=1} 1/(1-k*x^i).",
				"T(n,k) = Sum_{i=0..k} C(k,i) * A255970(n,i)."
			],
			"example": [
				"A(2,2) = 6: [2a], [2b], [1a,1a], [1a,1b], [1b,1a], [1b,1b].",
				"Square array A(n,k) begins:",
				"  1,  1,   1,    1,     1,      1,      1,      1, ...",
				"  0,  1,   2,    3,     4,      5,      6,      7, ...",
				"  0,  2,   6,   12,    20,     30,     42,     56, ...",
				"  0,  3,  14,   39,    84,    155,    258,    399, ...",
				"  0,  5,  34,  129,   356,    805,   1590,   2849, ...",
				"  0,  7,  74,  399,  1444,   4055,   9582,  19999, ...",
				"  0, 11, 166, 1245,  5876,  20455,  57786, 140441, ...",
				"  0, 15, 350, 3783, 23604, 102455, 347010, 983535, ..."
			],
			"maple": [
				"b:= proc(n, i, k) option remember; `if`(n=0, 1, `if`(i\u003c1, 0,",
				"      b(n, i-1, k) +`if`(i\u003en, 0, k*b(n-i, i, k))))",
				"    end:",
				"A:= (n, k)-\u003e b(n$2, k):",
				"seq(seq(A(n, d-n), n=0..d), d=0..12);"
			],
			"mathematica": [
				"b[n_, i_, k_] := b[n, i, k] = If[n == 0, 1, If[i\u003c1, 0, b[n, i-1, k] + If[i\u003en, 0, k*b[n-i, i, k]]]]; A[n_, k_] := b[n, n, k];  Table[Table[A[n, d-n], {n, 0, d}], {d, 0, 12}] // Flatten (* _Jean-François Alcover_, Feb 03 2015, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Columns k=0-10 give: A000007, A000041, A070933, A242587, A246936, A246937, A246938, A246939, A246940, A246941, A246942.",
				"Rows n=0-4 give: A000012, A001477, A002378, A027444, A186636.",
				"Main diagonal gives A124577.",
				"Cf. A144064, A255970, A256130."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Alois P. Heinz_, Sep 08 2014",
			"references": 26,
			"revision": 23,
			"time": "2018-09-06T12:31:31-04:00",
			"created": "2014-09-08T06:11:44-04:00"
		}
	]
}