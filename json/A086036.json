{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A086036",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 86036,
			"data": "0,0,0,0,6,4,2,5,0,9,6,3,6,6,4,7,7,3,7,9,1,1,0,1,8,1,9,1,3,8,0,4,3,5,7,6,5,9,8,9,8,4,5,4,5,5,4,6,9,7,8,8,1,5,0,5,2,8,9,8,5,6,6,2,5,8,4,3,8,9,8,4,5,2,0,0,9,7,7,4,5,3,2,3,9,4,4,7,4,5,8,2,6,4,7,0,4,5,7,0,1,1,9,4,4",
			"name": "Decimal expansion of the prime zeta modulo function at 6 for primes of the form 4k+1.",
			"link": [
				"Jean-François Alcover, \u003ca href=\"/A086036/b086036.txt\"\u003eTable of n, a(n) for n = 0..1006\u003c/a\u003e",
				"X. Gourdon and P. Sebah, \u003ca href=\"http://numbers.computation.free.fr/Constants/Miscellaneous/constantsNumTheory.html\"\u003eSome Constants from Number theory\u003c/a\u003e.",
				"R. J. Mathar, \u003ca href=\"http://arxiv.org/abs/1008.2547\"\u003eTable of Dirichlet L-series and prime zeta modulo functions for small moduli\u003c/a\u003e, arXiv:1008.2547 [math.NT], 2010-2015, value P(m=4, n=1, s=6), page 21.",
				"\u003ca href=\"/index/Z#zeta_function\"\u003eOEIS index to entries related to the (prime) zeta function\u003c/a\u003e."
			],
			"formula": [
				"Zeta_Q(6) = Sum_{p in A002144} 1/p^6  where  A002144 = {primes p == 1 mod 4};",
				"= Sum_{odd m \u003e 0} mu(m)/2m * log(DirichletBeta(6m)*zeta(6m)/zeta(12m)/(1+2^(-6m))) [using Gourdon \u0026 Sebah, Theorem 11]. - _M. F. Hasler_, Apr 26 2021"
			],
			"example": [
				"6.4250963664773791101819138043576598984545546978815052898566258438984520...*10^-5"
			],
			"mathematica": [
				"digits = 1003; m0 = 50; dm = 10; dd = 10; Clear[f, g];",
				"b[s_] := (1 + 2^-s)^-1 DirichletBeta[s] Zeta[s]/Zeta[2s] // N[#, digits + dd]\u0026;",
				"f[n_] := f[n] = (1/2) MoebiusMu[2n + 1]*Log[b[(2n + 1)*6]]/(2n + 1);",
				"g[m_] := g[m] = Sum[f[n], {n, 0, m}]; g[m = m0]; g[m += dm];",
				"While[Abs[g[m] - g[m - dm]] \u003c 10^(-digits - dd), Print[m]; m += dm];",
				"Join[{0, 0, 0, 0}, RealDigits[g[m], 10, digits][[1]]] (* _Jean-François Alcover_, Jun 24 2011, after X. Gourdon and P. Sebah, updated May 08 2021 *)"
			],
			"program": [
				"(PARI) A086036_upto(N=100)={localprec(N+3); digits((PrimeZeta41(6)+1)\\.1^N)[^1]} \\\\ see A086032 for the PrimeZeta41 function. - _M. F. Hasler_, Apr 26 2021"
			],
			"xref": [
				"Cf. A085995 (same for primes 4k+3), A343626 (for primes 3k+1), A343616 (for primes 3k+2), A086032, ..., A086039 (for 1/p^2, ..., 1/p^9), A085966 (PrimeZeta(6)), A002144 (primes of the form 4k+1)."
			],
			"keyword": "cons,nonn",
			"offset": "0,5",
			"author": "Antonio G. Astudillo (afg_astudillo(AT)lycos.com), Jul 07 2003",
			"ext": [
				"Edited by _M. F. Hasler_, Apr 26 2021"
			],
			"references": 5,
			"revision": 23,
			"time": "2021-05-08T08:37:25-04:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}