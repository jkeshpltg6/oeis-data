{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A167424",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 167424,
			"data": "0,1,5,89,24305,1664474849,7382162541380960705,139566915517602820239076685726696149889,48426946216426731755940416722216940042029155625849753533402166195474237122305",
			"name": "Define a sequence of fractions by f(1) = 1/2, f(n+1) = (f(n)^2 + 1)/2; sequence gives numerators.",
			"comment": [
				"Suppose that U_1,U_2,... is a sequence of independent uniform(0,1) random variables, and define random variables X_1,X_2,... as follows: X_1 = U_1, and, for n\u003e=1, X_{n+1} = X_n or U_{n+1} according as U_{n+1} \u003c E(X_n) or U_{n+1} \u003e E(X_n), respectively, where E() denotes expectation. Then, the sequence E(X_n) is identical to the sequence f(n). Sketch of proof. E(X_1)=1/2; for n\u003e=1, by the law of total expectation, we have E(X_{n+1}) = E(X_n)*E(X_n) + (1-E(X_n))*(1+E(X_n))/2. Hence E(X_{n+1}) = (E(X_n)^2 + 1)/2. - Shai Covo (green355(AT)netvision.net.il), Mar 08 2010",
				"a(n) is the numerator of x_n where x_0 = 0 and x_{m+1} = (x_m)^2 + 1/4. - _Michael Somos_, May 12 2019"
			],
			"reference": [
				"Steven R. Finch, Mathematical Constants, Cambridge University Press, 2003, Section 5.15 Optimal Stopping Constants, p. 362."
			],
			"link": [
				"Richard Blecksmith, John Brillhart, and Irving Gerst, \u003ca href=\"https://doi.org/10.1090/S0025-5718-1990-0995206-9\"\u003eOn the mod 2 reciprocation of infinite modular-part products and the parity of certain partition functions\u003c/a\u003e, Mathematics of Computation 54.189 (1990): 345-376. The sequence appears in Prop. 21. - _N. J. A. Sloane_, Nov 28 2019",
				"Ji Chen, \u003ca href=\"http://www.artofproblemsolving.com/Forum/viewtopic.php?t=318151\"\u003eInspired by IMO Shortlist 2001 algebra problem 3\u003c/a\u003e"
			],
			"formula": [
				"a(n) + A076628(n+1) = 2^(2^n-1). - Shai Covo (green355(AT)netvision.net.il), Mar 17 2010",
				"a(n+1) = a(n)^2+4^(2^n-1), a(0) = 0. - _Henry Bottomley_, Aug 21 2018"
			],
			"example": [
				"0/1, 1/2, 5/8, 89/128, 24305/32768, 1664474849/2147483648, 7382162541380960705/9223372036854775808, ..."
			],
			"maple": [
				"f:=proc(n) option remember; if n = 1 then 1/2; else (f(n-1)^2+1)/2; fi; end;"
			],
			"mathematica": [
				"a[1]=0; a[n_] := a[n]=(a[n - 1]^2 + 1)/2; Numerator[Table[a[n], {n, 10}]] (* _José María Grau Ribas_, May 19 2013 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c2, n\u003e0, a(n-1)^2 + 4*(a(n-1) - a(n-2)^2)^2)} /* _Michael Somos_, Aug 16 2011 */",
				"(PARI) {a(n) = my(x=0); if( n\u003c1, 0, for(k=1, n, x = x^2 + 1/4); numerator(x))}; /* _Michael Somos_, May 12 2019 */"
			],
			"xref": [
				"Denominators are (essentially) A058891."
			],
			"keyword": "nonn,frac",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_, Dec 15 2009, following an email suggestion from Ji Chen",
			"references": 1,
			"revision": 36,
			"time": "2020-02-10T06:44:09-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}