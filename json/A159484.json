{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A159484",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 159484,
			"data": "0,0,110,1054,7097,41201,220171,1115862,5451131,25919515,120721773,553162595,2501388936,11188504443,49589159037,218081007181,952654230982,4137309942806,17876235129762,76889316253171,329384246847644,1405944884946771,5981601330173431",
			"name": "Upper bound arising in Hadwiger's conjecture.",
			"comment": [
				"The best known upper bound on the number of smaller copies needed to cover a given body in n-dimensional Euclidean space is a(n) according to Brass. In graph theory, the Hadwiger conjecture (or Hadwiger's conjecture) states that, if all proper colorings of an undirected graph G use k or more colors, then one can find k disjoint connected subgraphs of G such that each subgraph is connected by an edge to each other subgraph.",
				"Contracting the edges within each of these subgraphs so that each subgraph collapses to a single supervertex produces a complete graph K_k on k vertices as a minor of G. In combinatorial geometry, the Hadwiger conjecture states that any convex body in n-dimensional Euclidean space may be covered by at most 2n homothetic (scaled and translated but not rotated) copies of the same body, each of which has smaller size than the original. There is also an equivalent formulation in terms of the number of floodlights needed to illuminate the body."
			],
			"reference": [
				"Kostochka, A. V. (1984), \"Lower bound of the Hadwiger number of graphs by their average degree\", Combinatorica 4 (4): 307-316.",
				"Brass, Peter; Moser, William; Pach, Janos (2005), \"3.3 Levi-Hadwiger Covering Problem and Illumination\", Research Problems in Discrete Geometry, Springer-Verlag, pp. 136-142 ."
			],
			"link": [
				"Nathaniel Johnston, \u003ca href=\"/A159484/b159484.txt\"\u003eTable of n, a(n) for n = 0..250\u003c/a\u003e",
				"Hugo Hadwiger, \u003ca href=\"http://www.ngzh.ch/archiv/1943_88/88_2/88_17.pdf\"\u003eÜber eine Klassifikation der Streckenkomplexe\u003c/a\u003e, Vierteljschr. Naturforsch. ges. Zürich 88: 133-143 (1943).",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Hadwiger_conjecture_(combinatorial_geometry)\"\u003eHadwiger conjecture (combinatorial geometry)\u003c/a\u003e."
			],
			"formula": [
				"a(n) = floor((4^n)*(5*n*log(n)))."
			],
			"example": [
				"a(1) = (4^1) * (5 * 1 * log(1)) = 0.",
				"a(2) = floor ((4^2) * (5 * 2 * log(2))) = floor(110.903549) = 110.",
				"a(3) = floor(1054.6678) = 1054."
			],
			"mathematica": [
				"Table[If[n==0, 0, Floor[(4^n)*(5*n*Log[n])]], {n,0,30}] (* _G. C. Greubel_, Jun 12 2018 *)"
			],
			"program": [
				"(PARI) for(n=0, 30, print1(if(n==0, 0, floor((4^n)*(5*n*log(n)))) , \", \")) \\\\ _G. C. Greubel_, Jun 12 2018",
				"(MAGMA) [0] cat [ Floor((4^n)*(5*n*Log(n))) : n in [1..30]]; // _G. C. Greubel_, Jun 12 2018"
			],
			"keyword": "easy,nonn",
			"offset": "0,3",
			"author": "_Jonathan Vos Post_, Apr 14 2009",
			"ext": [
				"a(7)-a(22) from _Nathaniel Johnston_, Apr 26 2011"
			],
			"references": 1,
			"revision": 17,
			"time": "2019-10-11T11:14:42-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}