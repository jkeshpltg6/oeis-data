{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A302295",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 302295,
			"data": "1,1,2,1,3,2,3,1,4,3,2,4,4,4,4,1,5,4,3,5,5,2,5,5,5,5,5,3,5,5,5,1,6,5,4,6,3,6,6,6,6,6,2,6,6,3,6,6,6,6,6,4,6,6,3,6,6,6,6,6,6,6,6,1,7,6,5,7,4,7,7,7,7,3,7,7,7,7,7,7,7,7,7,7,7,2,7",
			"name": "a(n) is the period of the binary expansion of n (with leading zeros allowed).",
			"comment": [
				"Equivalently, a(n) is the least positive k such that n is a repdigit number in base 2^k.",
				"See A302291 for the variant where leading zeros are not allowed."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A302295/b302295.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"formula": [
				"a(2^n) = n + 1 for any n \u003e= 0.",
				"a(2^n - 1) = 1 for any n \u003e= 0.",
				"a(n) \u003c= A302291(n).",
				"A059711(n) \u003c= 2^a(n)."
			],
			"example": [
				"The first terms, alongside the binary expansion of n with periodic part in parentheses, are:",
				"  n  a(n)    bin(n)",
				"  -- ----    ------",
				"   0    1    (0)",
				"   1    1    (1)",
				"   2    2    (10)",
				"   3    1    (1)(1)",
				"   4    3    (100)",
				"   5    2    (01)(01)",
				"   6    3    (110)",
				"   7    1    (1)(1)(1)",
				"   8    4    (1000)",
				"   9    3    (001)(001)",
				"  10    2    (10)(10)",
				"  11    4    (1011)",
				"  12    4    (1100)",
				"  13    4    (1101)",
				"  14    4    (1110)",
				"  15    1    (1)(1)(1)(1)",
				"  16    5    (10000)",
				"  17    4    (0001)(0001)",
				"  18    3    (10)(10)",
				"  19    5    (10011)",
				"  20    5    (10100)"
			],
			"program": [
				"(PARI) a(n) = for (k=1, oo, if (#Set(digits(n, 2^k))\u003c=1, return (k)))"
			],
			"xref": [
				"Cf. A059711, A302291."
			],
			"keyword": "nonn,base,easy",
			"offset": "0,3",
			"author": "_Rémy Sigrist_, Apr 04 2018",
			"references": 2,
			"revision": 8,
			"time": "2018-04-07T03:56:39-04:00",
			"created": "2018-04-06T17:28:13-04:00"
		}
	]
}