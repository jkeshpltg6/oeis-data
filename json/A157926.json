{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A157926",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 157926,
			"data": "1,1,0,1,1,0,0,1,1,1,0,0,1,1,1,0,0,0,1,1,1,0,1,0,1,1,0,1,0,0,1,1,0,1,1,1,1,1,0,0,1,1,1,0,0,1,1,1,1,1,0,1,1,0,0,1,1,1,1,1,1,1,1,1,0,0,1,1,1,1,1,0,1,1,1,0,1,1,0,1,1,1,0,0,0,1,1,0,0,0,1,1,1,0,0,1,1,0,1,1,0,0,0,0,1",
			"name": "Coefficients of the first modulo two factor of polynomials of the type: p(x,n)=(x^Prime[n] + 1)/(x + 1).",
			"comment": [
				"Row sums are:",
				"{3, 5, 7, 3, 15, 11, 15, 15, 3, 23, 5, 17, 27, 21, 15,...}.",
				"This procedure gives more than the types with two factors:",
				"Mod[(x^Prime[n] + 1)/(x + 1),2]=f1(x)*f2(x);",
				"In each case the program picks just the first factor.",
				"The classic Golay factorization:",
				"Factor[PolynomialMod[(x^23 + 1)/((x + 1)), 2], Modulus -\u003e 2]",
				"(1 + x + x^5 + x^6 + x^7 + x^9 + x^11)",
				"(1 + x^2 + x^4 + x^5 + x^6 + x^10 + x^11)",
				"The other one mentioned by Sloane and Conway in \"Sphere Packings\":",
				"Factor[PolynomialMod[(x^47 + 1)/((x + 1)), 2], Modulus -\u003e 2]",
				"(1 + x + x^2 + x^3 + x^5 + x^6 + x^7 + x^9 + x^10 + x^12 + x^13 + x^14 + x^18 + x^19 + x^23)",
				"(1 + x^4 + x^5 + x^9 +x^10 + x^11 + x^13 + x^14 + x^16 + x^17 + x^18 + x^20 +x^21 + x^22 + x^23)"
			],
			"reference": [
				"J. H. Conway and N. J. A. Sloane, \"Sphere Packings, Lattices and Groups\", Springer-Verlag, pp. 231."
			],
			"formula": [
				"Mod[(x^Prime[n] + 1)/(x + 1),2]=Product[fi(x),{i,0,n}];",
				"Out_(n,m)=coefficients(f1(x))."
			],
			"example": [
				"{1, 1, 0, 1},",
				"{1, 0, 0, 1, 1, 1, 0, 0, 1},",
				"{1, 1, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1},",
				"{1, 0, 1, 0, 0, 1},",
				"{1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1},",
				"{1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1},",
				"{1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1},",
				"{1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1},",
				"{1, 1, 0, 0, 0, 0, 0, 0, 0, 1},",
				"{1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1},",
				"{1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1},",
				"{1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 1},",
				"{1, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 1},",
				"{1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1},",
				"{1, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1}"
			],
			"mathematica": [
				"a = Flatten[Table[If[ ( Factor[PolynomialMod[(x^Prime[n] + 1)/((x + 1)), 2], Modulus -\u003e 2] == PolynomialMod[(x^Prime[n] + 1)/((x + 1)), 2]) /. x -\u003e 2, {}, FactorList[PolynomialMod[( x^Prime[n] + 1)/((x + 1)), 2], Modulus -\u003e 2][[2]][[1]]], {n, 2, 30}]];",
				"Table[CoefficientList[a[[n]], x], {n, 1, Length[a]}];",
				"Flatten[%] Table[Apply[Plus, CoefficientList[a[[n]], x]], {n, 1, Length[a]}];"
			],
			"keyword": "nonn,tabf,uned,obsc",
			"offset": "2,1",
			"author": "_Roger L. Bagula_ and _Gary W. Adamson_, Mar 09 2009",
			"ext": [
				"I started to edit this, changing the definition to read: \"Irregular triangle read by rows: factorize (x^prime(n)+1)/(x+1) mod 2, then write down coefficients of lowest degree factor, with exponents in increasing order.\" This would make a perfectly reasonable sequence, which would begin {1,1}, {1,1,1}, {1,1,1,1,1}, {1,1,0,1}, ... Unfortunately this does not match the terms of the present sequence. Possibly the exponents should be 2^(prime(n)-1) rather than prime(n). I did not investigate this further. - _N. J. A. Sloane_, Dec 16 2010"
			],
			"references": 0,
			"revision": 8,
			"time": "2013-05-11T05:18:33-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}