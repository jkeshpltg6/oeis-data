{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A338801",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 338801,
			"data": "17,0,1,72,24,575,450,232,60,15,0,3,1728,1668,948,144,24,12,8799,10080,6321,3052,898,490,161,14,35,14,7,22688,24080,12784,4160,1248,272,80,32,78327,101142,70254,39708,19584,6894,2369,1062,351,54,27,18,27,36,11,165500,203220,134860,62520,21240,5720,1080,300,100,20",
			"name": "Irregular table read by rows: The number of k-faced polyhedra, where k\u003e=4, created when an n-prism, formed from two n-sided regular polygons joined by n adjacent rectangles, is internally cut by all the planes defined by any three of its vertices.",
			"comment": [
				"See A338783 for further details and images for this sequence.",
				"The author thanks Zach J. Shannon for assistance in producing the images for this sequence."
			],
			"link": [
				"Hyung Taek Ahn and Mikhail Shashkov, \u003ca href=\"https://cnls.lanl.gov/~shashkov/papers/ahn_geometry.pdf\"\u003eGeometric Algorithms for 3D Interface Reconstruction\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A338801/a338801.jpg\"\u003e3-prism, showing the 18 polyhedra post-cutting and exploded\u003c/a\u003e. Each piece has been moved away from the origin by a distance proportional to the average distance of its vertices from the origin. Red shows the seventeen 4-faced polyhedra, orange the single 6-faced polyhedron.",
				"Scott R. Shannon, \u003ca href=\"/A338801/a338801_1.jpg\"\u003e7-prism, showing the 8799 4-faced polyhedra\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A338801/a338801_2.jpg\"\u003e7-prism, showing the 10080 5-faced polyhedra\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A338801/a338801_3.jpg\"\u003e7-prism, showing the 6321 6-faced polyhedra\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A338801/a338801_4.jpg\"\u003e7-prism, showing the 3052 7-faced polyhedra\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A338801/a338801_5.jpg\"\u003e7-prism, showing the 898 8-faced polyhedra\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A338801/a338801_6.jpg\"\u003e7-prism, showing the 490 9-faced polyhedra\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A338801/a338801_7.jpg\"\u003e7-prism, showing the 161 10-faced polyhedra\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A338801/a338801_8.jpg\"\u003e7-prism, showing the 14 11-faced, 35 12-faced, 14 13-faced, 7 14-faced polyhedra\u003c/a\u003e. These are colored white, black, yellow, red respectively. None of these are visible on the surface.",
				"Scott R. Shannon, \u003ca href=\"/A338801/a338801_9.jpg\"\u003e7-prism, showing all 29871 polyhedra\u003c/a\u003e.  The 4,5,6,7,8,9,10 faced polyhedra are colored red, orange, yellow, green, blue, indigo, violet respectively. The 11,12,13,14 faced polyhedra are not visible on the surface.",
				"Scott R. Shannon, \u003ca href=\"/A338801/a338801_10.jpg\"\u003e10-prism, showing all 594560 polyhedra\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"https://mathworld.wolfram.com/Prism.html\"\u003ePrism\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Prism_(geometry)\"\u003ePrism (geometry)\u003c/a\u003e."
			],
			"formula": [
				"Sum of row n = A338783(n)."
			],
			"example": [
				"The triangular 3-prism is cut with 6 internal planes defined by all 3-vertex combinations of its 6 vertices. This leads to the creation of seventeen 4-faced polyhedra and one 6-faced polyhedra, eighteen pieces in all. The single 6-faced polyhedra lies at the very center of the original 3-prism.",
				"The 9-prism is cut with 207 internal planes leading to the creation of 319864 pieces. It is noteworthy in creating all k-faced polyhedra from k=4 to k=18.",
				"The table begins:",
				"17,0,1;",
				"72,24;",
				"575,450,232,60,15,0,3;",
				"1728,1668,948,144,24,12;",
				"8799,10080,6321,3052,898,490,161,14,35,14,7;",
				"22688,24080,12784,4160,1248,272,80,32;",
				"78327,101142,70254,39708,19584,6894,2369,1062,351,54,27,18,27,36,11;",
				"165500,203220,134860,62520,21240,5720,1080,300,100,20;"
			],
			"xref": [
				"Cf. A338783 (number of polyhedra), A338808 (antiprism), A338622 (Platonic solids), A333543 (n-dimensional cube)."
			],
			"keyword": "nonn,tabf",
			"offset": "3,1",
			"author": "_Scott R. Shannon_, Nov 10 2020",
			"references": 9,
			"revision": 38,
			"time": "2020-12-07T01:44:44-05:00",
			"created": "2020-12-07T01:44:44-05:00"
		}
	]
}