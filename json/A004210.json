{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A004210",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 4210,
			"id": "M2728",
			"data": "1,3,8,18,30,43,67,90,122,161,202,260,305,388,416,450,555,624,730,750,983,1059,1159,1330,1528,1645,1774,1921,2140,2289,2580,2632,2881,3158,3304,3510,3745,4086,4563,4741,4928,5052,5407,5864,6242,6528,6739,7253",
			"name": "\"Magic\" integers: a(n+1) is the smallest integer m such that there is no overlap between the sets {m, m-a(i), m+a(i): 1 \u003c= i \u003c= n} and {a(i), a(i)-a(j), a(i)+a(j): 1 \u003c= j \u003c i \u003c= n}.",
			"comment": [
				"The definition implies that the sets {a(i)} (A004210), {a(i)-a(j), j \u003c i} (A206522) and {a(i)+a(j), j \u003c i} (A206523) are disjoint. A206524 gives the complement of their union."
			],
			"reference": [
				"R. A. Bates, E. Riccomagno, R. Schwabe, H. P. Wynn, Lattices and dual lattices in optimal experimental design for Fourier models, Computational Statistics \u0026 Data Analysis Volume 28, Issue 3, 4 September 1998, Pages 283-296. See page 293.",
				"D. R. Hofstadter, \"Goedel, Escher, Bach: An Eternal Golden Braid\", Basic Books Incorporated, p. 73",
				"P. Mark Kayll, Well-spread sequences and edge-labelings with constant Hamiltonian weight, Disc. Math. \u0026 Theor. Comp. Sci 6 2 (2004) 401-408",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A004210/b004210.txt\"\u003eTable of n, a(n) for n = 1..150\u003c/a\u003e",
				"B. G. DeBoer, \u003ca href=\"/A004210/a004210.pdf\"\u003eLetter to N. J. A. Sloane\u003c/a\u003e, Dec 15 1978, with enclosure of Silvertom article.",
				"J. V. Silverton, \u003ca href=\"http://dx.doi.org/10.1107/S0567739478001308\"\u003eOn the generation of 'magic integrals'\u003c/a\u003e, Acta Cryst. A34 (1978) p. 634.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/MagicInteger.html\"\u003eMagic Integer.\u003c/a\u003e"
			],
			"formula": [
				"a(n+1) = min{ k | k and k +- a(i) are not equal to a(i) or a(i)-a(j) or a(i)+a(j) for any n+1 \u003e i \u003e j \u003e 0}. [Corrected by _T. D. Noe_, Sep 08 2008]"
			],
			"mathematica": [
				"a[1] = 1; a[n_] := a[n] = Module[{pairs = Flatten[ Table[{a[j] + a[k], a[k] - a[j]}, {j, 1, n-1}, {k, j+1, n-1}]], an = a[n-1] + 1}, While[ True, If[ Intersection[ Join[ Array[a, n-1], pairs], Prepend[ Flatten[ Table[{a[j] + an, an - a[j]}, {j, 1, n-1}]], an]] == {}, Break[], an++]]; an]; Table[a[n], {n, 1, 48}] (* _Jean-François Alcover_, Nov 10 2011 *)"
			],
			"program": [
				"(Haskell)",
				"import Data.List (intersect)",
				"a004210 n = a004210_list !! (n-1)",
				"a004210_list = magics 1 [0] [0] where",
				"   magics :: Integer -\u003e [Integer] -\u003e [Integer] -\u003e [Integer]",
				"   magics n ms tests",
				"      | tests `intersect` nMinus == [] \u0026\u0026 tests `intersect` nPlus == []",
				"      = n : magics (n+1) (n:ms) (nMinus ++ nPlus ++ tests)",
				"      | otherwise",
				"      = magics (n+1) ms tests",
				"      where nMinus = map (n -) ms",
				"            nPlus  = map (n +) ms",
				"-- magics works also as generator for a126428_list, cf. A126428.",
				"-- _Reinhard Zumkeller_, Mar 03 2011"
			],
			"xref": [
				"Cf. A000969, A005228, A206522, A206523, A206524."
			],
			"keyword": "easy,nonn,nice",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, following a suggestion from B. G. DeBoer, Dec 15 1978",
			"ext": [
				"Additional comments from Robert M. Burton, Jr. (bob(AT)oregonstate.edu), Feb 20 2005",
				"More terms from _Joshua Zucker_, May 04 2006",
				"Edited by _N. J. A. Sloane_, Sep 06 2008 at the suggestion of _R. J. Mathar_",
				"Edited by _N. J. A. Sloane_, Feb 08 2012"
			],
			"references": 5,
			"revision": 48,
			"time": "2019-02-24T11:21:00-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}