{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A117934",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 117934,
			"data": "27,32,125,128,2187,2197,6434856,6436343,312079600999,312079650687,328080401001,328080696273,11305786504384,11305787424768,62854898176000,62854912109375,79723529268319,79723537443243,4550858390629024",
			"name": "Perfect powers (A001597) that are close, that is, between consecutive squares.",
			"comment": [
				"It appears that all pairs of close powers involve a cube. For three pairs, the other power is a 7th power. For all remaining pairs, the other power is a 5th power. If this is true, then three powers are never close.",
				"For the first 360 terms, 176 pairs are a cube and a 5th power. The remaining four pairs are a cube and a 7th power. - _Donovan Johnson_, Feb 26 2011",
				"Loxton proves that the interval [n, n+sqrt(n)] contains at most exp(40 log log n log log log n) powers for n \u003e= 16, and hence there are at most 2*exp(40 log log n log log log n) between consecutive squares in the interval containing n. - _Charles R Greathouse IV_, Jun 25 2017"
			],
			"link": [
				"Donovan Johnson, \u003ca href=\"/A117934/b117934.txt\"\u003eTable of n, a(n) for n = 1..360\u003c/a\u003e",
				"Daniel J. Bernstein, \u003ca href=\"https://cr.yp.to/papers/powers.pdf\"\u003eDetecting perfect powers in essentially linear time\u003c/a\u003e, Mathematics of Computation 67 (1998), pp. 1253-1283.",
				"John H. Loxton, \u003ca href=\"https://eudml.org/doc/205990\"\u003eSome problems involving powers of integers\u003c/a\u003e, Acta Arithmetica 46:2 (1986), pp. 113-123. See Bernstein, Corollary 19.5, for a correction to the proof of Theorem 1.",
				"StackExchange MathOverflow, \u003ca href=\"https://mathoverflow.net/questions/62444/are-there-ever-three-perfect-powers-between-consecutive-squares/62479\"\u003eAre there ever three perfect powers between consecutive squares?\u003c/a\u003e Answers by Gjergji Zaimi and Felipe Voloch (2011)."
			],
			"example": [
				"27 and 32 are close because they are between 25 and 36."
			],
			"mathematica": [
				"nMax=10^14; lst={}; log2Max=Ceiling[Log[2,nMax]]; bases=Table[2,{log2Max}]; powers=bases^Range[log2Max]; powers[[1]]=Infinity; currPP=1; cnt=0; While[nextPP=Min[powers]; nextPP \u003c= nMax, pos=Flatten[Position[powers,nextPP]]; If[MemberQ[pos,2], cnt=0, cnt++ ]; If[cnt\u003e1, AppendTo[lst,{currPP,nextPP}]]; Do[k=pos[[i]]; bases[[k]]++; powers[[k]]=bases[[k]]^k, {i,Length[pos]}]; currPP=nextPP]; Flatten[lst]"
			],
			"xref": [
				"Cf. A097056, A117896 (number of perfect powers between consecutive squares n^2 and (n+1)^2).",
				"Cf. A340696, A340700."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_T. D. Noe_, Apr 03 2006",
			"references": 5,
			"revision": 18,
			"time": "2021-01-22T10:03:07-05:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}