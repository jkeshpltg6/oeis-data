{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A246146",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 246146,
			"data": "0,1,1,0,0,1,1,0,1,0,0,1,0,1,1,0,1,0,0,1,1,0,0,1,0,1,1,0,1,0,0,1,0,1,1,0,0,1,1,0,1,0,0,1,1,0,0,1,0,1,1,0,0,1,1,0,1,0,0,1,0,1,1,0,1,0,0,1,1,0,0,1,0,1,1,0,0,1,1,0,1,0,0,1,1,0",
			"name": "Limiting block extension of A010060 (Thue-Morse sequence) with first term as initial block.",
			"comment": [
				"Suppose S = (s(0), s(1), s(2), ...) is an infinite sequence such that every finite block of consecutive terms occurs infinitely many times in S.  (It is assumed that A010060 is such a sequence.)  Let B = B(m,k) = (s(m), s(m+1),...s(m+k)) be such a block, where m \u003e= 0 and k \u003e= 0.  Let m(1) be the least i \u003e m such that (s(i), s(i+1),...,s(i+k)) = B(m,k), and put B(m(1),k+1) = (s(m(1)), s(m(1)+1),...s(m(1)+k+1)).  Let m(2) be the least i \u003e m(1) such that (s(i), s(i+1),...,s(i+k)) = B(m(1),k+1), and put B(m(2),k+2) = (s(m(2)), s(m(2)+1),...s(m(2)+k+2)).  Continuing in this manner gives a sequence of blocks B'(n) = B(m(n),k+n), so that for n \u003e= 0, B'(n+1) comes from B'(n) by suffixing a single term; thus the limit of B'(n) is defined; we call it the \"limiting block extension of S with initial block B(m,k)\", denoted by S^  in case the initial block is s(0).",
				"The sequence (m(i)), where m(0) = 0, is the \"index sequence for limit-block extending S with initial block B(m,k)\", as in A246147.",
				"Limiting block extensions are analogous to limit-reverse sequences, S*, defined at A245920.  The essential difference is that S^ is formed by extending each new block one term to the right, whereas S* is formed by extending each new block one term to the left (and then reversing)."
			],
			"example": [
				"S = A010060, with B = (s(0)); that is, (m,k) = (0,0)",
				"S = (0,1,1,0,1,0,0,1,1,0,0,1,0,1,1,0,1,...)",
				"B'(0) = (0)",
				"B'(1) = (0,1)",
				"B'(2) = (0,1,1)",
				"B'(3) = (0,1,1,0)",
				"B'(4) = (0,1,1,0,0)",
				"B'(5) = (0,1,1,0,0,1)",
				"S^ = (0,1,1,0,0,1,1,0,1,0,0,1,0,1,1,...),",
				"with index sequence (0,3,6,12,20,30,36,68,...)"
			],
			"mathematica": [
				"seqPosition1[list_, seqtofind_] := If[Length[#] \u003e Length[list], {}, Last[Last[Position[Partition[list, Length[#], 1], Flatten[{___, #, ___}], 1, 1]]]] \u0026[seqtofind]; s = Nest[Flatten[# /. {0 -\u003e {0, 1}, 1 -\u003e {1, 0}}] \u0026, {0}, 14]; (* A010060 *)",
				"Take[s, 60]",
				"t = {{0}}; p[0] = seqPosition1[s, Last[t]]; s = Drop[s, p[0]]; Off[Last::nolast]; n = 1; While[(p[n] = seqPosition1[s, Last[t]]) \u003e 0, (AppendTo[t, Take[s, {#, # + Length[Last[t]]}]]; s = Drop[s, #]) \u0026[p[n]]; n++]; On[Last::nolast]; Last[t] (* A246146 *)",
				"-1 + Accumulate[Table[p[k], {k, 0, n - 1}]] (* A246147 *)"
			],
			"xref": [
				"Cf. A246147, A246128, A246141, A246143, A246145, A010060."
			],
			"keyword": "nonn",
			"offset": "0",
			"author": "_Clark Kimberling_ and _Peter J. C. Moses_, Aug 17 2014",
			"references": 5,
			"revision": 6,
			"time": "2014-08-22T10:22:07-04:00",
			"created": "2014-08-22T10:22:07-04:00"
		}
	]
}