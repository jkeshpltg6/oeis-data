{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A075680",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 75680,
			"data": "0,2,1,5,6,4,2,5,3,6,1,4,7,41,5,39,8,3,6,11,40,9,4,38,7,7,2,41,10,10,5,39,8,8,3,37,42,3,6,11,6,40,1,9,9,33,4,38,43,7,7,31,12,36,41,24,2,10,5,10,34,15,39,15,44,8,8,13,32,13,3,37,42,42,6,3,11,30,11,18,35,6,40,23",
			"name": "For odd numbers 2n-1, the minimum number of iterations of the reduced Collatz function R required to yield 1. The function R is defined as R(k) = (3k+1)/2^r, with r as large as possible.",
			"comment": [
				"See A075677 for the function R applied to the odd numbers once. The 3x+1 conjecture asserts that a(n) is a finite number for all n. The function R applied to the odd numbers shows the essential behavior of the 3x+1 iterations.",
				"Bisection of A006667. - _T. D. Noe_, Jun 01 2006"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A075680/b075680.txt\"\u003eTable of n, a(n) for n = 1..5000\u003c/a\u003e"
			],
			"example": [
				"a(4) = 5 because 7 is the fourth odd number and 5 iterations are needed: R(R(R(R(R(7)))))=1."
			],
			"mathematica": [
				"nextOddK[n_] := Module[{m=3n+1}, While[EvenQ[m], m=m/2]; m]; (* assumes odd n *) Table[m=n; cnt=0; If[n\u003e1, While[m=nextOddK[m]; cnt++; m!=1]]; cnt, {n, 1, 200, 2}]"
			],
			"program": [
				"(Haskell)",
				"a075680 n = snd $ until ((== 1) . fst)",
				"            (\\(x, i) -\u003e (a000265 (3 * x + 1), i + 1)) (2 * n - 1, 0)",
				"-- _Reinhard Zumkeller_, Jan 08 2014",
				"(Perl)",
				"sub a {",
				"  my $v = 2 * shift() - 1;",
				"  my $c = 0;",
				"  until (1 == $v) {",
				"    $v = 3 * $v + 1;",
				"    $v /= 2 until ($v \u0026 1);",
				"    $c += 1;",
				"  }",
				"  return $c;",
				"} # _Ruud H.G. van Tol_, Nov 16 2021",
				"(PARI) a(n)=my(s); n+=n-1; while(n\u003e1, n+=n\u003e\u003e1+1; if(n%2==0, n\u003e\u003e=valuation(n,2)); s++); s \\\\ _Charles R Greathouse IV_, Dec 22 2021"
			],
			"xref": [
				"Cf. A075677.",
				"Cf. A075684 for the largest number attained during the iteration.",
				"Cf. A000265.",
				"Cf. A060445 which also counts intermediate even steps."
			],
			"keyword": "easy,nonn",
			"offset": "1,2",
			"author": "_T. D. Noe_, Sep 25 2002",
			"references": 16,
			"revision": 24,
			"time": "2021-12-22T03:37:55-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}