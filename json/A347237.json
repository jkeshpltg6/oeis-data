{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A347237",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 347237,
			"data": "1,0,1,6,1,0,3,6,17,0,1,6,3,0,1,42,1,0,3,6,3,0,5,6,37,0,49,18,1,0,5,78,1,0,3,102,3,0,3,6,1,0,3,6,17,0,5,42,89,0,1,18,5,0,1,18,3,0,1,6,5,0,51,330,3,0,3,6,5,0,1,102,5,0,37,18,3,0,3,42,353,0,5,18,1,0,1,6,7,0,9,30,5,0,3,78,3,0,17",
			"name": "Möbius transform of A347236.",
			"comment": [
				"Dirichlet convolution of A003972 (prime shifted phi) with A061019.",
				"Dirichlet convolution of A003961 with A158523.",
				"Multiplicative because A003972 and A061019 are, and also because A347236 is.",
				"From _Antti Karttunen_, Aug 25 2021: (Start)",
				"All terms are nonnegative because sequence is multiplicative and a(p^k) \u003e= 0 for all primes p and k \u003e= 0.",
				"Proof: For any prime p, sequence a(p^k), k\u003e=0, is obtained as an ordinary convolution of sequences (-p)^k and the first differences of q^k, where q = A151800(p). (E.g., for powers of 2, the sequences convolved are A122803 and A025192, giving A102901.) This convolution is an alternating sum, with the terms 1*(q-1)*q^(k-1), -(p)*(q-1)*q^(k-2), (p^2)*(q-1)*q^(k-3), -(p^3)*(q-1)*q^(k-4), ..., (p^(k-1))*(q-1), -(p^k), for odd k, with sum of each consecutive pair being nonnegative because q \u003e= p+1, while with an even exponent k, the leftover term p^k at the end is also positive, thus the whole sum is nonnegative also in that case.",
				"(End)"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A347237/b347237.txt\"\u003eTable of n, a(n) for n = 1..16383\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#gaps\"\u003eIndex entries for primes, gaps between\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#prime_indices\"\u003eIndex entries for sequences computed from indices in prime factorization\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{d|n} A008683(n/d) * A347236(d).",
				"a(n) = Sum_{d|n} A003972(n/d) * A061019(d).",
				"a(n) = Sum_{d|n} A003961(n/d) * A158523(d).",
				"For all n \u003e= 1, a(A000040(n)) = A001223(n) - 1.",
				"For all n \u003e= 0, a(2^n) = A102901(n).",
				"For all n \u003e= 0, a(3^n) = A120612(n).",
				"Multiplicative with a(p^e) = (-p)^e + (A151800(p)-1)*(A151800(p)^e-(-p)^e)/(A151800(p)+p). - _Sebastian Karlsson_, Sep 02 2021"
			],
			"program": [
				"(PARI)",
				"A003961(n) = { my(f = factor(n)); for(i=1, #f~, f[i, 1] = nextprime(f[i, 1]+1)); factorback(f); };",
				"A061019(n) = (((-1)^bigomega(n))*n);",
				"A347237(n) = sumdiv(n,d,A061019(d)*eulerphi(A003961(n/d)));",
				"\\\\ Or alternatively as:",
				"A158523(n) = { my(f=factor(n)); prod(i=1, #f~, my(p=f[i,1], e=f[i,2]); ((-1)^e)*(p+1)*(p^(e-1))); };",
				"A347237(n) = sumdiv(n,d,A003961(n/d)*A158523(d));"
			],
			"xref": [
				"Cf. A000040, A001223, A003961, A003972, A008683, A008836, A016825 (positions of zeros), A061019, A102901, A120612, A158523, A347236.",
				"Cf. also A347137.",
				"Cf. A151800."
			],
			"keyword": "nonn,mult",
			"offset": "1,4",
			"author": "_Antti Karttunen_, Aug 24 2021",
			"references": 3,
			"revision": 39,
			"time": "2021-09-18T22:03:13-04:00",
			"created": "2021-08-24T21:09:13-04:00"
		}
	]
}