{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003583",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3583,
			"data": "1,5,26,130,628,2954,13612,61716,276200,1223002,5367676,23383100,101215576,435712580,1866667448,7963424104,33846062544,143373104378,605518549660,2550438016812,10716162617336",
			"name": "a(n) = (n+2)*2^(2*n-1) - (n/2)*binomial(2*n,n).",
			"comment": [
				"a(n) gives the number of open partitions of a tree made of two chains with n points each, that share an added root. (An open partition pi of a tree T is a partition of the vertices of T with the property that, for each block B of pi, the upset of B is a union of blocks of pi.) - _Pietro Codara_, Jan 14 2015"
			],
			"reference": [
				"Pietro Codara, Partitions of a finite partially ordered set, From Combinatorics to Philosophy: The Legacy of G.-C. Rota, Springer, New York (2009), 45-59."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A003583/b003583.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"N. J. Calkin, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(94)90394-8\"\u003eA curious binomial identity\u003c/a\u003e, Discr. Math., 131 (1994), 335-337.",
				"Pietro Codara, Ottavio D'Antona, Francesco Marigo, Corrado Monti, \u003ca href=\"http://arxiv.org/abs/1307.1348\"\u003eMaking simple proofs simpler\u003c/a\u003e, arXiv:1307.1348 [cs.MS], 2013.",
				"Zachary Hamaker, Eric Marberg, \u003ca href=\"https://arxiv.org/abs/1802.09805\"\u003eAtoms for signed permutations\u003c/a\u003e, arXiv:1802.09805 [math.CO], 2018.",
				"M. Hirschhorn, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(95)00086-C\"\u003eCalkin's binomial identity\u003c/a\u003e, Discr. Math., 159 (1996), 273-278.",
				"Jun Wang and Zhizheng Zhang, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(03)00206-1\"\u003eOn extensions of Calkin's binomial identities\u003c/a\u003e, Discrete Math., 274 (2004), 331-342."
			],
			"formula": [
				"Main diagonal of correlation matrix of A055248. a(n) = Sum_{k=0..n} ( Sum_{m=k..n} binomial(n, m))^2 ). - _Paul Barry_, Jun 05 2003",
				"Let S2 := (n, t)-\u003eadd( k^t * (add( binomial(n, j), j=0..k))^2, k=0..n); a(n) = S2(n, 0).",
				"From _Robert Israel_, Jan 13 2015: (Start)",
				"G.f.: (1-2*x)/(1-4*x)^2 - x/(1 - 4*x)^(3/2).",
				"E.g.f.: (2*x+1)*exp(4*x) - x*exp(2*x)*(I_0(2*x)+I_1(2*x)) where I_0 and I_1 are modified Bessel functions.",
				"a(n) ~ 4^n*(n/2 - sqrt(n)/(2*sqrt(Pi)) + 1 + O(n^(-1/2))).",
				"(End)"
			],
			"maple": [
				"seq((n+2)*2^(2*n-1)-(n/2)*binomial(2*n,n), n=0..50); # _Robert Israel_, Jan 13 2015"
			],
			"mathematica": [
				"Table[(n+2)*2^(2*n-1)-(n/2)*Binomial[2*n,n], {n,0,50}] (* _Pietro Codara_, Jan 14 2015 *)",
				"Table[Sum[Sum[Binomial[n-1,k-1]Binomial[n-1,j-1]Min[k,j],{j,1,n}],{k,1 n}],{n,1,51}] (* _Pietro Codara_, Jan 14 2015 *)"
			],
			"program": [
				"(PARI) x='x+O('x^50); Vec((1-2*x)/(1-4*x)^2 - x/(1 - 4*x)^(3/2)) \\\\ _G. C. Greubel_, Feb 15 2017"
			],
			"xref": [
				"If the exponent E in a(n) = Sum_{m=0..n} (Sum_{k=0..m} C(n,k))^E is 1, 2, 3, 4, 5 we get A001792, A003583, A007403, A294435, A294436 respectively."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 7,
			"revision": 41,
			"time": "2018-05-30T13:54:57-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}