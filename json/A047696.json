{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A047696",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 47696,
			"data": "1,91,728,2741256,6017193,1412774811,11302198488,137513849003496,424910390480793000,933528127886302221000",
			"name": "Smallest positive number that can be written in n ways as a sum of two (not necessarily positive) cubes.",
			"comment": [
				"Sometimes called cab-taxi (or cabtaxi) numbers.",
				"For a(10), see the C. Boyer link.",
				"Christian Boyer: After his recent work on Taxicab(6) confirming the number found as an upper bound by Randall Rathbun in 2002, Uwe Hollerbach (USA) confirmed this week that my upper bound constructed in Dec 2006 is really Cabtaxi(10). See his announcement. - _Jonathan Vos Post_, Jul 08 2008",
				"From _PoChi Su_, Aug 14 2014: (Start)",
				"An upper bound of a(42) was given by C. Boyer (see the C. Boyer link), denoted by",
				"BCa(42)= 2^9*3^9*5^9*7^7*11^3*13^6*17^3*19^3*29^3*31*37^4*43^4*",
				"        61^3*67^3*73*79^3*97^3*101^3*109^3*139^3*157*163^3*181^3*",
				"        193^3*223^3*229^3*307^3*397^3*457^3.",
				"We show that 503^3*BCa(42) is an upper bound of a(43) with an additional sum of x^3+y^3, with",
				"x=2^4*3^3*5^5*7*11*13^2*17*29*37*43*61*67*79*97*101*109*139*163*",
				"  181*193*223*229*307*397*457*2110099,",
				"y=2^3*3^4*5^3*7*11*13^2*17*29*37*41*43*61*67*79*97*101*109*139*163*",
				"  181*193*223*229*307*397*457*176899.",
				"(End)",
				"From _PoChi Su_, Aug 29 2014: (Start)",
				"An upper bound of a(43) was given by _PoChi Su_, denoted by",
				"SCa(43)= 2^9*3^9*5^9*7^7*11^3*13^6*17^3*19^3*29^3*31*37^4*43^4*",
				"        61^3*67^3*73*79^3*97^3*101^3*109^3*139^3*157*163^3*181^3*",
				"        193^3*223^3*229^3*307^3*397^3*457^3*503^3.",
				"We show that 1307^3*SCa(43) is an upper bound of a(44) with an additional sum of x^3+y^3, with",
				"x=2^3*3^4*5^3*7^2*11*13^2*17*19*23*29*37*43*61*79*101*109*139*163*",
				"  181*193*223*229*307*353*397*457*503*826583,",
				"y=-2^7*3^3*5^3*7^2*11*13^2*17*19^2*29*37*43*61*79*101*109*139*163*",
				"  181*193*223*229*307*397*457*503*58882897.",
				"(End)",
				"From _Sergey Pavlov_, Feb 18 2017: (Start)",
				"For 1 \u003c n \u003c= 10, each a(n) can be written as the product of not more than n distinct prime powers where one of the factors is a power of 7. For 1 \u003c n \u003c= 9, a(n) can be represented as the difference between two squares, b(n)^2 - c(n)^2, where b(n), c(n) are integers, b(n+1) \u003e b(n), and c(n+1) \u003e c(n):",
				"a(2) = 7 * 13 = 10^2 - 3^2 = 91,",
				"a(3) = 2^3 * 7 * 13 = 33^2 - 19^2,",
				"a(4) = 2^3 * 3^3 * 7^3 * 37 = 1659^2 - 105^2,",
				"a(5) = 3^3 * 7 * 13 * 31 * 79 = 2477^2 - 344^2,",
				"a(6) = 3^3 * 7^4 * 19 * 31 * 37 = 37590^2 - 483^2,",
				"a(7) = 2^3 * 3^3 * 7^4 * 19 * 31 * 37 = 106477^2 - 5929^2,",
				"a(8) = 2^3 * 3^3 * 7^4 * 19 * 23^3 * 31 * 37 = 11736739^2 - 487025^2,",
				"a(9) = 2^3 * 3^3 * 5^3 * 7^4 * 19 * 31 * 37 * 67^3 = 651858879^2 - 3099621^2,",
				"a(10) = 2^3 * 3^3 * 5^3 * 7^4 * 13^3 * 19 * 31 * 37 * 67^3.",
				"(End)"
			],
			"reference": [
				"C. Boyer, \"Les nombres Taxicabs\", in Dossier Pour La Science, pp. 26-28, Volume 59 (Jeux math') April/June 2008 Paris.",
				"R. K. Guy, Unsolved Problems in Number Theory, Section D1."
			],
			"link": [
				"D. J. Bernstein, \u003ca href=\"http://cr.yp.to/papers.html#sortedsums\"\u003eEnumerating solutions to p(a) + q(b) = r(c) + s(d)\u003c/a\u003e",
				"D. J. Bernstein, \u003ca href=\"http://pobox.com/~djb/papers/sortedsums.dvi\"\u003eEnumerating solutions to p(a) + q(b) = r(c) + s(d)\u003c/a\u003e",
				"C. Boyer, \u003ca href=\"http://www.christianboyer.com/taxicab\"\u003eNew upper bounds on Taxicab and Cabtaxi numbers\u003c/a\u003e",
				"C. Boyer, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL11/Boyer/boyer.html\"\u003eNew upper bounds for Taxicab and Cabtaxi numbers\u003c/a\u003e, JIS 11 (2008) 08.1.6",
				"Uwe Hollerbach, \u003ca href=\"https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;19ef9d82.0805\"\u003eThe tenth cabtaxi number is 933528127886302221000\u003c/a\u003e, May 14, 2008.",
				"Uwe Hollerbach, \u003ca href=\"http://www.korgwal.com/ramanujan/\"\u003eTaxi, Taxi!\u003c/a\u003e [Original link, broken]",
				"Uwe Hollerbach, \u003ca href=\"http://web.archive.org/web/20120203221114/http://www.korgwal.com/ramanujan\"\u003eTaxi, Taxi!\u003c/a\u003e [Replacement link to Wayback Machine]",
				"Uwe Hollerbach, \u003ca href=\"/A003825/a003825.html\"\u003eTaxi! Taxi!\u003c/a\u003e [Cached copy from Wayback Machine, html version of top page only]",
				"Po-Chi Su, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL19/Su/su3.html\"\u003eMore Upper Bounds on Taxicab and Cabtaxi Numbers\u003c/a\u003e, Journal of Integer Sequences, 19 (2016), #16.4.3.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/TaxicabNumber.html\"\u003eTaxicab Numbers\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CabtaxiNumber.html\"\u003eCabtaxi Number\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Cabtaxi_number\"\u003eCabtaxi number\u003c/a\u003e"
			],
			"example": [
				"91 = 6^3 - 5^3 = 4^3 + 3^3 (in two ways).",
				"Cabtaxi(9)=424910390480793000 = 645210^3 + 538680^3 = 649565^3 + 532315^3 = 752409^3 - 101409^3 = 759780^3 - 239190^3 = 773850^3 - 337680^3 = 834820^3 - 539350^3 = 1417050^3 - 1342680^3 = 3179820^3 - 3165750^3 = 5960010^3 - 5956020^3."
			],
			"xref": [
				"Cf. A011541, A047697."
			],
			"keyword": "nonn,nice,more,hard",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"a(9) (which was found on Jan 31 2005) from Duncan Moore (Duncan.Moore(AT)nnc.co.uk), Feb 01 2005"
			],
			"references": 6,
			"revision": 85,
			"time": "2019-01-26T14:20:44-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}