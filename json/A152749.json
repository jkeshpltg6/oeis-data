{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A152749",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 152749,
			"data": "0,2,4,10,14,24,30,44,52,70,80,102,114,140,154,184,200,234,252,290,310,352,374,420,444,494,520,574,602,660,690,752,784,850,884,954,990,1064,1102,1180,1220,1302,1344,1430,1474,1564,1610,1704,1752,1850,1900,2002",
			"name": "a(n) = (n+1)*(3*n+1)/4 for n odd, a(n) = n*(3*n+2)/4 for n even.",
			"comment": [
				"Interleaving of A049450 and A049451 (for n \u003e 0).",
				"Also, integer values of k*(k+1)/3. - _Charles R Greathouse IV_, Dec 11 2010",
				"The nonzero coefficients of the expansion of f(a) = Product_{k\u003e=1} (1-a^(2k)), see A194159, occur at the terms of the sequence given above, i.e., f(a) = 1 - a^2 - a^4 + a^10 + a^14 - a^24 - a^30 + a^44 + a^52 - a^70 - a^80 + ... = Sum_{n\u003e=0} (-1)^binomial(n+1,2)*a^A152749(n). - _Johannes W. Meijer_, Aug 21 2011",
				"Partial sums of A109043. - _Reinhard Zumkeller_, Mar 31 2012",
				"Nonnegative k such that 12*k+1 is a square. - _Vicente Izquierdo Gomez_, Jul 22 2013",
				"Equivalently, numbers of the form h*(3*h+1), where h = 0, -1, 1, -2, 2, -3, 3, -4, 4, ... (see also the fifth comment of A062717). - _Bruno Berselli_, Feb 02 2017",
				"For n \u003e 0, a(n-1) is the sum of the largest parts of the partitions of 2n into two even parts. - _Wesley Ivan Hurt_, Dec 19 2017"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A152749/b152749.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,2,-2,-1,1)."
			],
			"formula": [
				"From _R. J. Mathar_, Jan 03-06 2009: (Start)",
				"G.f.: 2*x*(1+x+x^2)/((1+x)^2*(1-x)^3).",
				"a(n) = a(n-1) + 2*a(n-2) - 2*a(n-3) - a(n-4) + a(n-5) = A003154(n+1)/8 - (-1)^n*A005408(n)/8.",
				"a(n) = 2*A001318(n) = ((6*n^2+6*n+1) - (2*n+1)*(-1)^n)/8. (End)"
			],
			"maple": [
				"A152749 := proc(n): if type(n,even) then n*(3*n+2)/4  else (n+1)*(3*n+1)/4 fi: end: seq(A152749(n), n=0..51); # _Johannes W. Meijer_, Aug 21 2011"
			],
			"mathematica": [
				"Table[If[OddQ[n],(n+1)*(3*n+1)/4,n*(3*n+2)/4],{n,0,60}] (* _Vladimir Joseph Stephan Orlovsky_, Feb 03 2012 *)",
				"LinearRecurrence[{1,2,-2,-1,1}, {0, 2, 4, 10, 14}, 50] (* _Vincenzo Librandi_, Feb 22 2012 *)",
				"Select[Range[1,1000], IntegerQ[Sqrt[12#+1]]\u0026] (* _Vicente Izquierdo Gomez_, Jul 22 2013 *)"
			],
			"program": [
				"(MAGMA) [IsOdd(n) select (n+1)*(3*n+1)/4 else n*(3*n+2)/4: n in [0..52]];",
				"(Haskell)",
				"a152749 n = a152749_list !! (n-1)",
				"a152749_list = scanl1 (+) a109043_list",
				"-- _Reinhard Zumkeller_, Mar 31 2012",
				"(MAGMA) f:=func\u003cn | n*(3*n+1)\u003e; [0] cat [f(n*m): m in [-1,1], n in [1..30]]; // _Bruno Berselli_, Nov 13 2012"
			],
			"xref": [
				"Cf. A049450 (n*(3*n-1)), A049451 (n*(3*n+1)), A153383 (12n+1 is not prime)."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Vincenzo Librandi_, Dec 31 2009",
			"ext": [
				"Edited, typo corrected and extended by _Klaus Brockhaus_, Jan 02 2009",
				"Leading term a(0)=0 added by _Johannes W. Meijer_, Aug 21 2011"
			],
			"references": 15,
			"revision": 57,
			"time": "2018-02-03T21:25:25-05:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}