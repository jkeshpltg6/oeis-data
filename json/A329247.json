{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A329247",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 329247,
			"data": "6,5,8,4,7,8,9,4,8,4,6,2,4,0,8,3,5,4,3,1,2,5,2,3,1,7,3,6,5,3,9,8,4,2,2,2,0,1,3,4,9,0,9,8,5,7,3,3,7,5,8,2,3,9,8,8,4,2,3,6,1,2,8,4,6,0,2,3,0,0,9,2,7,0,8,2,2,1,9,8,8,0,3,7,1,0,9,5,0,6,7",
			"name": "Decimal expansion of Sum_{k\u003e=1} cos(k*Pi/6)/k.",
			"comment": [
				"Sum_{k\u003e=1} cos(k*x)/k = Re(Sum_{k\u003e=1} exp(k*x*i)/k) = Re(-log(1-exp(x*i))) = -log(2*|sin(x/2)|), x != 2*m*Pi, where i is the imaginary unit.",
				"In general, for real s and complex z, let f(s,z) = Sum_{k\u003e=1} z^k/k^s, then:",
				"(a) if s \u003c= 0, then f(s,z) converges to Polylog(s,z) if |z| \u003c 1;",
				"(b) if 0 \u003c s \u003c= 1, then f(s,z) converges to Polylog(s,z) if z != 1;",
				"(c) if s \u003e 1, then f(s,z) converges to Polylog(s,z) if |z| \u003c= 1.",
				"As a result, let z = e^(i*x), then the series Sum_{k\u003e=1} (cos(k*x) + i*sin(k*x))/k^s converges to Polylog(s,e^(i*x)) if and only if s \u003e 1, or 0 \u003c s \u003c= 1 and x != 2*m*Pi."
			],
			"formula": [
				"Equals log(2 + sqrt(3))/2.",
				"Equals -log(2*|sin(Pi/12)|).",
				"Equals arccoth(sqrt(3)). - _Amiram Eldar_, Dec 05 2021"
			],
			"example": [
				"0.65847894846240835431252317365398422201349098573375..."
			],
			"maple": [
				"Digits := 100: (log(2 + sqrt(3))/2)*10^91:",
				"ListTools:-Reverse(convert(floor(%), base, 10)); # _Peter Luschny_, Nov 09 2019"
			],
			"mathematica": [
				"RealDigits[Log[2 + Sqrt[3]]/2, 10, 100][[1]] (* _Amiram Eldar_, Dec 05 2021 *)"
			],
			"program": [
				"(PARI) default(realprecision, 100); log(2 + sqrt(3))/2"
			],
			"xref": [
				"Similar sequences:",
				"A263192 (Sum_{k\u003e=1} cos(k)/sqrt(k) = Re(Polylog(1/2,exp(i))));",
				"A263193 (Sum_{k\u003e=1} sin(k)/sqrt(k) = Im(Polylog(1/2,exp(i))));",
				"this sequence (Sum_{k\u003e=1} cos(k*Pi/6)/k = Re(Polylog(1,exp(i*Pi/6))));",
				"A121225 (Sum_{k\u003e=1} cos(k)/k = Re(Polylog(1,exp(i))));",
				"A329246 (Sum_{k\u003e=1} cos(k*Pi/4)/k = Re(Polylog(1,exp(i*Pi/4))));",
				"A096444 (Sum_{k\u003e=1} sin(k)/k = Im(Polylog(1,exp(i))));",
				"A122143 (Sum_{k\u003e=1} cos(k)/k^2 = Re(Polylog(2,exp(i))));",
				"A096418 (Sum_{k\u003e=1} sin(k)/k^2 = Im(Polylog(2,exp(i))))."
			],
			"keyword": "nonn,cons",
			"offset": "0,1",
			"author": "_Jianing Song_, Nov 09 2019",
			"references": 1,
			"revision": 19,
			"time": "2021-12-05T03:25:41-05:00",
			"created": "2019-11-09T09:15:44-05:00"
		}
	]
}