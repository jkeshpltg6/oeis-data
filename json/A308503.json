{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A308503",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 308503,
			"data": "1,2,2,3,6,3,4,15,10,4,5,12,21,18,5,6,35,20,75,14,6,7,30,55,36,33,30,7,8,77,42,245,28,105,22,8,9,24,91,150,65,60,39,54,9,10,45,40,847,66,385,44,375,50,10,11,70,63,72,119,210,85,108,147,42,11,12",
			"name": "Square array T(n, k), n, k \u003e 0, read by antidiagonals upwards: T(n, k) = f(g(n) | g(k)), where f is defined over the set of finite sequences of nonnegative integers with no trailing zero as f(e) = Sum_{k = 1..#e} prime(k)^e_k, g is the inverse of f, and | denotes concatenation.",
			"comment": [
				"The function f establishes a natural bijection from the set of finite sequences of nonnegative integers with no trailing zero to the set of natural numbers based on prime factorization.",
				"If we consider the set of finite sequences of signed integers with no trailing zero, then we obtain a bijection to the set of positive rational numbers.",
				"The function g is defined by:",
				"- g(1) = () (the empty sequence),",
				"- g(n) = the n-th row of A067255 for any n \u003e 1."
			],
			"formula": [
				"For any m, n, k \u003e 0:",
				"- T(m, T(n, k)) = T(T(m, n), k) (T is associative),",
				"- T(n, 1) = T(1, n) = n (1 is a neutral element),",
				"- T(2, k) = 2*A003961(k),",
				"- h(T(n, k)) = h(n) + h(k) for h = A001221, A001222, A061395,",
				"- the function i -\u003e T(n, i)/n is completely multiplicative and equals the A061395(n)-th iterate of A003961."
			],
			"example": [
				"Array T(n, k) begins:",
				"  n\\k|   1   2    3    4    5     6    7     8     9    10",
				"  ---+----------------------------------------------------",
				"    1|   1   2    3    4    5     6    7     8     9    10",
				"    2|   2   6   10   18   14    30   22    54    50    42",
				"    3|   3  15   21   75   33   105   39   375   147   165",
				"    4|   4  12   20   36   28    60   44   108   100    84",
				"    5|   5  35   55  245   65   385   85  1715   605   455",
				"    6|   6  30   42  150   66   210   78   750   294   330",
				"    7|   7  77   91  847  119  1001  133  9317  1183  1309",
				"    8|   8  24   40   72   56   120   88   216   200   168",
				"    9|   9  45   63  225   99   315  117  1125   441   495",
				"   10|  10  70  110  490  130   770  170  3430  1210   910"
			],
			"program": [
				"(PARI) T(n,k) = { my (e=concat(apply(m -\u003e my (f=factor(m), w=#f~, v=vector(if (w, primepi(f[w,1]), 0))); for (j=1, w, v[primepi(f[j,1])]=f[j,2]); v, [n,k]))); prod (i=1, #e, if (e[i], prime(i)^e[i], 1)) }"
			],
			"xref": [
				"Cf. A001221, A001222, A003961, A061395, A067255."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Rémy Sigrist_, Jun 02 2019",
			"references": 1,
			"revision": 10,
			"time": "2019-06-03T09:14:28-04:00",
			"created": "2019-06-03T09:14:28-04:00"
		}
	]
}