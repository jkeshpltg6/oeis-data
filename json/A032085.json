{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A032085",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 32085,
			"data": "2,1,2,6,12,28,56,120,240,496,992,2016,4032,8128,16256,32640,65280,130816,261632,523776,1047552,2096128,4192256,8386560,16773120,33550336,67100672,134209536,268419072,536854528",
			"name": "Number of reversible strings with n beads of 2 colors. If more than 1 bead, not palindromic.",
			"comment": [
				"a(n) is also the number of induced subgraphs with odd number of edges in the path graph P(n) if n\u003e0. - Alessandro Cosentino (cosenal(AT)gmail.com), Feb 06 2009",
				"A common recurrence of the bisections A020522 and A006516 means a(n+4) = 6*a(n+2) - 8*a(n), n\u003e1. - _Yosu Yurramendi_, Aug 07 2008",
				"Also, the decimal representation of the diagonal from the origin to the corner of the n-th stage of growth of the two-dimensional cellular automaton defined by \"Rule 566\", based on the 5-celled von Neumann neighborhood, initialized with a single black (ON) cell at stage zero. - _Robert Price_, Jul 05 2017"
			],
			"reference": [
				"S. Wolfram, A New Kind of Science, Wolfram Media, 2002; p. 170."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A032085/b032085.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"M. Archibald, A. Blecher, A. Knopfmacher, M. E. Mays, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL23/Archibald/arch3.html\"\u003eInversions and Parity in Compositions of Integers\u003c/a\u003e, J. Int. Seq., Vol. 23 (2020), Article 20.4.1.",
				"C. G. Bower, \u003ca href=\"/transforms2.html\"\u003eTransforms (2)\u003c/a\u003e",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=1022\"\u003eEncyclopedia of Combinatorial Structures 1022\u003c/a\u003e",
				"S. J. Cyvin et al., \u003ca href=\"http://dx.doi.org/10.1021/ci00013a027\"\u003eTheory of polypentagons\u003c/a\u003e, J. Chem. Inf. Comput. Sci., 33 (1993), 466-474.",
				"N. J. A. Sloane, \u003ca href=\"http://arxiv.org/abs/1503.01168\"\u003eOn the Number of ON Cells in Cellular Automata\u003c/a\u003e, arXiv:1503.01168 [math.CO], 2015.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ElementaryCellularAutomaton.html\"\u003eElementary Cellular Automaton\u003c/a\u003e",
				"S. Wolfram, \u003ca href=\"http://wolframscience.com/\"\u003eA New Kind of Science\u003c/a\u003e",
				"Wolfram Research, \u003ca href=\"http://atlas.wolfram.com/\"\u003eWolfram Atlas of Simple Programs\u003c/a\u003e",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_2D_5-Neighbor_Cellular_Automata\"\u003eIndex to 2D 5-Neighbor Cellular Automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_Elementary_Cellular_Automata\"\u003eIndex to Elementary Cellular Automata\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,2,-4)."
			],
			"formula": [
				"\"BHK\" (reversible, identity, unlabeled) transform of 2, 0, 0, 0, ...",
				"a(n) = 2^(n-1)-2^floor((n-1)/2), n \u003e 1. - _Vladeta Jovovic_, Nov 11 2001",
				"G.f.: 2*x+x^2/((1-2*x)*(1-2*x^2)). - Mohammed Bouayoun (bouyao(AT)wanadoo.fr), Mar 25 2004",
				"a(n) = A005418(n+1)-A016116(n+2), n\u003e1. - _Yosu Yurramendi_, Aug 07 2008",
				"a(n+1) = A077957(n) + 2*a(n), n\u003e1. a(n+2) = A000079(n+1) + 2*a(n), n\u003e1. - _Yosu Yurramendi_, Aug 10 2008",
				"First differences: a(n+1)-a(n) = A007179(n) = A156232(n+2)/4, n\u003e1. - _Paul Curtz_, Nov 16 2009",
				"a(n) = 2*(a(n-1) bitwiseOR a(n-2)), n\u003e3. - _Pierre Charland_, Dec 12 2010",
				"a(n) = 2*a(n-1) + 2*a(n-2) - 4*a(n-3). - _Wesley Ivan Hurt_, Jul 03 2020"
			],
			"mathematica": [
				"Join[{2}, LinearRecurrence[{2, 2, -4}, {1, 2, 6}, 29]] (* _Jean-François Alcover_, Oct 11 2017 *)"
			],
			"program": [
				"(MAGMA) [2] cat [2^(n-1)-2^Floor((n-1)/2) : n in [2..40]]; // _Wesley Ivan Hurt_, Jul 03 2020"
			],
			"xref": [
				"Cf. A005418, A016116. Essentially the same as A122746.",
				"Row sums of triangle A034877.",
				"Cf. A289404, A289405, A052551."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Christian G. Bower_",
			"references": 14,
			"revision": 57,
			"time": "2020-07-03T23:33:45-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}