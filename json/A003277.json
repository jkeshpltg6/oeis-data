{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003277",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3277,
			"id": "M0650",
			"data": "1,2,3,5,7,11,13,15,17,19,23,29,31,33,35,37,41,43,47,51,53,59,61,65,67,69,71,73,77,79,83,85,87,89,91,95,97,101,103,107,109,113,115,119,123,127,131,133,137,139,141,143,145,149,151,157,159,161,163,167,173",
			"name": "Cyclic numbers: k such that k and phi(k) are relatively prime; also k such that there is just one group of order k, i.e., A000001(n) = 1.",
			"comment": [
				"Except for a(2)=2, all the terms in the sequence are odd. This is because of the existence of a non-cyclic dihedral group of order 2n for each n\u003e1. - Ahmed Fares (ahmedfares(AT)my-deja.com), May 09 2001",
				"Also gcd(n, A051953(n)) = 1. - _Labos Elemer_",
				"n such that x^n==1 (mod n) has no solution 2\u003c=x\u003c=n. - _Benoit Cloitre_, May 10 2002",
				"There is only one group (the cyclic group of order n) whose order is n. - _Gerard P. Michon_, Jan 08 2008  [This is a 1947 result of Tibor Szele. - _Charles R Greathouse IV_, Nov 23 2011]",
				"Any divisor of a Carmichael number (A002997) must be odd and cyclic. Conversely, G. P. Michon conjectured (c. 1980) that any odd cyclic number has at least one Carmichael multiple (if the conjecture is true, each of them has infinitely many such multiples). In 2007, Michon \u0026 Crump produced explicit Carmichael multiples of all odd cyclic numbers below 10000 (see link, cf. A253595). - _Gerard P. Michon_, Jan 08 2008",
				"Numbers n such that phi(n)^phi(n) == 1 (mod n). - _Michel Lagneau_, Nov 18 2012",
				"Contains A000040, and all members of A006094 except 6. - _Robert Israel_, Jul 08 2015",
				"Number m such that n^n == r (mod m) is solvable for any r. - _David W. Wilson_, Oct 01 2015",
				"Numbers n such that A074792(n) = n + 1. - _Thomas Ordowski_, Jul 16 2017"
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), p. 840.",
				"J. S. Rose, A Course on Group Theory, Camb. Univ. Press, 1978, see p. 7.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A003277/b003277.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"Max Alekseyev, \u003ca href=\"http://garden.irmacs.sfu.ca/?q=op/does_every_odd_number_coprime_to_its_euler_totient_divides_some_carmichael_number\"\u003eMichon's conjecture\u003c/a\u003e (Open Problem Garden, Aug. 2007).",
				"Keith Conrad, \u003ca href=\"https://kconrad.math.uconn.edu/blurbs/grouptheory/allgrouporderncyclic.pdf\"\u003eWhen are all groups of order n cyclic?\u003c/a\u003e, University of Connecticut, 2019.",
				"P. J. Dukes and J. Niezen, \u003ca href=\"http://ajc.maths.uq.edu.au/pdf/61/ajc_v61_p098.pdf\"\u003ePairwise balanced designs of dimension three\u003c/a\u003e, Australasian Journal Of Combinatorics, Volume 61(1) (2015), pages 98-113.",
				"Paul Erdős, \u003ca href=\"http://www.renyi.hu/~p_erdos/1948-11.pdf\"\u003eSome asymptotic formulas in number theory\u003c/a\u003e, J. Indian Math. Soc. (N.S.) 12 (1948), pp. 75-78.",
				"J. M. Grau, A. M. Oller-Marcen, M. Rodríguez, and D. Sadornil, \u003ca href=\"http://arxiv.org/abs/1401.4708\"\u003eFermat test with gaussian base and Gaussian pseudoprimes\u003c/a\u003e, arXiv preprint arXiv:1401.4708 [math.NT], 2014.",
				"Gerard P. Michon, \u003ca href=\"http://www.numericana.com/answer/modular.htm#carmdiv\"\u003eCarmichael Divisors\u003c/a\u003e",
				"G. P. Michon and J. K. Crump, \u003ca href=\"http://www.numericana.com/data/crump.htm\"\u003eCarmichael Multiples of Odd Cyclic Numbers\u003c/a\u003e (up to 10000)",
				"J. Pakianathan and K. Shankar, \u003ca href=\"http://www.math.ou.edu/%7Eshankar/papers/nil2.pdf\"\u003eNilpotent Numbers\u003c/a\u003e, Amer. Math. Monthly, 107, August-September 2000, \u003ca href=\"http://www.jstor.org/stable/2589118\"\u003e631-634\u003c/a\u003e.",
				"R. P. Stanley, \u003ca href=\"/A003277/a003277.pdf\"\u003eLetter to N. J. A. Sloane, c. 1991\u003c/a\u003e",
				"T. Szele, \u003ca href=\"http://doi.org/10.5169/seals-18061\"\u003eÜber die endlichen Ordnungszahlen, zu denen nur eine Gruppe gehört\u003c/a\u003e, Commentarii Mathematici Helvetici 20 (1947), pp. 265-267.",
				"\u003ca href=\"/index/Gre#groups\"\u003eIndex entries for sequences related to groups\u003c/a\u003e"
			],
			"formula": [
				"n = p_1*p_2*...*p_k (for some k \u003e= 0), where the p_i are distinct primes and no p_j-1 is divisible by any p_i.",
				"A000001(a(n)) = 1.",
				"Erdős proved that a(n) ~ e^gamma n log log log n, where e^gamma is A073004. - _Charles R Greathouse IV_, Nov 23 2011",
				"A000005(a(n)) = 2^k. - _Carlos Eduardo Olivieri_, Jul 07 2015"
			],
			"maple": [
				"select(t -\u003e igcd(t, numtheory:-phi(t))=1, [$1..1000]); # _Robert Israel_, Jul 08 2015"
			],
			"mathematica": [
				"Select[Range[175], GCD[#, EulerPhi[#]] == 1 \u0026] (* _Jean-François Alcover_, Apr 04 2011 *)",
				"Select[Range@175, FiniteGroupCount@# == 1 \u0026] (* _Robert G. Wilson v_, Feb 16 2017 *)"
			],
			"program": [
				"(PARI) isA003277(n) = gcd(n,eulerphi(n))==1 \\\\ _Michael B. Porter_, Feb 21 2010",
				"(Haskell)",
				"import Data.List (elemIndices)",
				"a003277 n = a003277_list !! (n-1)",
				"a003277_list = map (+ 1) $ elemIndices 1 a009195_list",
				"-- _Reinhard Zumkeller_, Feb 27 2012",
				"(MAGMA) [n: n in [1..200] | Gcd(n, EulerPhi(n)) eq 1]; // _Vincenzo Librandi_, Jul 09 2015",
				"(Sage) # Compare A050384.",
				"def isPrimeTo(n, m): return gcd(n, m) == 1",
				"def isCyclic(n): return isPrimeTo(n, euler_phi(n))",
				"[n for n in (1..173) if isCyclic(n)] # _Peter Luschny_, Nov 14 2018"
			],
			"xref": [
				"Subsequence of A051532.",
				"Cf. A000010, A009195, A050384 (the same sequence but with the primes removed). Also A000001(a(n)) = 1.",
				"Cf. A002997, A006094, A054395, A055561, A054396, A054397, A135850, A249550, A249551, A249552, A249553, A249554, A249555, A036537, A051953, A253595."
			],
			"keyword": "nonn,nice,easy",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_ and _Richard Stanley_",
			"ext": [
				"More terms from _Christian G. Bower_"
			],
			"references": 74,
			"revision": 139,
			"time": "2021-05-16T03:18:58-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}