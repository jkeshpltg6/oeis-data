{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A125857",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 125857,
			"data": "0,2,20,182,1640,14762,132860,1195742,10761680,96855122,871696100,7845264902,70607384120,635466457082,5719198113740,51472783023662,463255047212960,4169295424916642,37523658824249780,337712929418248022",
			"name": "Numbers whose base-9 representation is 22222222.......2.",
			"comment": [
				"If f(1) := 1/x and f(n+1) = (f(n) + 2/f(n))/3, then f(n) = 3^(1-n) * (1/x + a(n)*x + O(x^3)). - _Michael Somos_, Jul 28 2020"
			],
			"link": [
				"G. Benkart, D. Moon, \u003ca href=\"http://arxiv.org/abs/1409.8154\"\u003eA Schur-Weyl Duality Approach to Walking on Cubes\u003c/a\u003e, arXiv preprint arXiv:1409.8154 [math.RT], 2014 and \u003ca href=\"http://dx.doi.org/10.1007/s00026-016-0311-3\"\u003eAnn. Combin. 20 (3) (2016) 397-417\u003c/a\u003e",
				"E. Estrada and J. A. de la Pena, \u003ca href=\"http://arxiv.org/abs/1302.1176\"\u003eFrom Integer Sequences to Block Designs via Counting Walks in Graphs\u003c/a\u003e, arXiv preprint arXiv:1302.1176 [math.CO], 2013. - From _N. J. A. Sloane_, Feb 28 2013",
				"E. Estrada and J. A. de la Pena, \u003ca href=\"http://www.nntdm.net/papers/nntdm-19/NNTDM-19-3-78-84.pdf\"\u003eInteger sequences from walks in graphs\u003c/a\u003e, Notes on Number Theory and Discrete Mathematics, Vol. 19, 2013, No. 3, 78-84.",
				"R. J. Mathar, \u003ca href=\"/A102518/a102518.pdf\"\u003eCounting Walks on Finite Graphs\u003c/a\u003e, Nov 2020, Section 5.",
				"Vladimir Pletser, \u003ca href=\"http://arxiv.org/abs/1409.7969\"\u003eCongruence conditions on the number of terms in sums of consecutive squared integers equal to squared integers\u003c/a\u003e, arXiv:1409.7969 [math.NT], 2014.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (10,-9)."
			],
			"formula": [
				"a(n) = (9^(n-1) - 1)*2/8.",
				"a(n) = 9*a(n-1) + 2 (with a(1)=0). - _Vincenzo Librandi_, Sep 30 2010",
				"a(n) = 2 * A002452(n). - _Vladimir Pletser_, Mar 29 2014",
				"From _Colin Barker_, Sep 30 2014: (Start)",
				"a(n) = 10*a(n-1) - 9*a(n-2).",
				"G.f.: 2*x^2 / ((x-1)*(9*x-1)). (End)",
				"a(n) = -a(2-n) * 9^(n-1) for all n in Z. - _Michael Somos_, Jul 02 2017",
				"a(n) = A191681(n-1)/2. - _Klaus Purath_, Jul 03 2020"
			],
			"example": [
				"G.f. = 2*x^2 + 20*x^3 + 182*x^4 + 1640*x^5 + 14762*x^6 + 132860*x^7 + ... - _Michael Somos_, Jul 28 2020"
			],
			"maple": [
				"seq((9^n-1)*2/8, n=0..19);"
			],
			"mathematica": [
				"FromDigits[#, 9]\u0026/@Table[PadRight[{2}, n, 2], {n, 0, 20}] (* _Harvey P. Dale_, Feb 02 2011 *)",
				"Table[(9^(n - 1) - 1)*2/8, {n, 20}] (* _Wesley Ivan Hurt_, Mar 29 2014 *)"
			],
			"program": [
				"(PARI) Vec(2*x^2/((x-1)*(9*x-1)) + O(x^100)) \\\\ _Colin Barker_, Sep 30 2014",
				"(PARI) {a(n) = (9^(n-1) - 1)/4}; /* _Michael Somos_, Jul 02 2017 */"
			],
			"xref": [
				"Cf. A002452."
			],
			"keyword": "easy,nonn,base",
			"offset": "1,2",
			"author": "_Zerinvary Lajos_, Feb 03 2007",
			"references": 5,
			"revision": 66,
			"time": "2020-11-05T05:04:39-05:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}