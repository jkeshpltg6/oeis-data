{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A338161",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 338161,
			"data": "1,-2,4,-3,-5,11,-6,-7,8,9,10,12,13,-14,-16,-15,-17,18,19,-20,21,22,23,24,25,-26,-27,-34,-28,-29,-30,31,32,33,35,-36,37,38,39,40,41,-42,-43,-44,-57,-45,-46,-47,48,49,-50,51,52,53,-55,54,56,58,59,60,61,-62,-63,-64,-65,-83,-66,-67,-68,-69,70,71",
			"name": "Successive sums of successive terms produce the successive natural numbers (see the Comments section).",
			"comment": [
				"The 1st term sums up to 1;",
				"the next 2 terms sum up to 2;",
				"the next 3 terms sum up to 3;",
				"the next 4 terms sum up to 4;",
				"... the next k terms sum up to k."
			],
			"link": [
				"Carole Dubois, \u003ca href=\"/A338161/b338161.txt\"\u003eTable of n, a(n) for n = 1..406\u003c/a\u003e",
				"Carole Dubois, \u003ca href=\"/A338161/a338161_1.txt\"\u003eProgram (Python)\u003c/a\u003e"
			],
			"example": [
				"1 = 1 (1 term);",
				"2 = - 2 + 4 (2 terms);",
				"3 = - 3 - 5 + 11 (3 terms);",
				"4 = - 6 - 7 + 8 + 9 (4 terms);",
				"5 = 10 + 12 + 13 - 14 - 16 (5 terms);",
				"6 = - 15 - 17 + 18 + 19 - 20 + 21 (6 terms); etc.",
				"How are the plus and minus signs split between the terms to get the above six equations? Here is the method -- with an example:",
				"1) no absolute value of any term can be present twice or more in the sequence;",
				"2) to start a new equation, always use the set of smallest absolute values not yet used; say, for the above 5-term equation, that they are a, b, c, d and e;",
				"3) the set of unused values for a, b, c, d and e is here 10, 12, 13, 14, 15;",
				"4) try all the possible mix of values and signs to find one or more solutions (the try 5 = 10 + 12 - 13 - 14 + 15, for instance, doesn't work as we would get 5 = 10);",
				"5) if no such mix leads to a solution (which is the case here), add 1 to the biggest integer of the values' set and try again;",
				"6) the above set would then become 10, 12, 13, 14, 16 -- and a quick computer search gives the solution 5 = 10 + 12 + 13 - 14 - 16;",
				"7) had we found more than one solution, we would have kept the lexicographically earliest one (-10 comes before +10);",
				"8) if a new mix doesn't lead to a solution, add again 1 to the biggest integer of the values' set and try again; etc."
			],
			"program": [
				"(Python) # see Link section"
			],
			"xref": [
				"Cf. A330903."
			],
			"keyword": "sign",
			"offset": "1,2",
			"author": "_Eric Angelini_ and _Carole Dubois_, Oct 14 2020",
			"references": 1,
			"revision": 29,
			"time": "2021-05-24T00:09:01-04:00",
			"created": "2020-10-14T23:20:57-04:00"
		}
	]
}