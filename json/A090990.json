{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A090990",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 90990,
			"data": "5,9,16,29,52,94,169,305,549,990,1783,3214,5790,10435,18801,33881,61048,110009,198224,357194,643633,1159797,2089869,3765830,6785771,12227562,22033274,39702627,71541613,128913593,232294192,418579765",
			"name": "Number of meaningful differential operations of the n-th order on the space R^5.",
			"comment": [
				"Also number of meaningful compositions of the n-th order of the differential operations and Gateaux directional derivative on the space R^4. - Branko Malesevic and Ivana Jovovic (ivana121(AT)EUnet.yu), Jun 21 2007"
			],
			"reference": [
				"B. Malesevic: Some combinatorial aspects of differential operation composition on the space R^n, Univ. Beograd, Publ. Elektrotehn. Fak., Ser. Mat. 9 (1998), 29-33."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A090990/b090990.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"B. Malesevic, \u003ca href=\"https://arxiv.org/abs/0704.0750\"\u003eSome combinatorial aspects of differential operation composition on the space R^n\u003c/a\u003e, arXiv:0704.0750 [math.DG], 2007.",
				"B. Malesevic and I. Jovovic, \u003ca href=\"https://arxiv.org/abs/0706.0249\"\u003eThe Compositions of the Differential Operations and Gateaux Directional Derivative\u003c/a\u003e, arXiv:0706.0249 [math.CO], 2007.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,2,-1)."
			],
			"formula": [
				"a(n+3) = a(n+2) + 2*a(n+1) - a(n).",
				"G.f.: x*(5+4*x-3*x^2)/(1-x-2*x^2+x^3). - _Ralf Stephan_, Aug 19 2004"
			],
			"maple": [
				"NUM := proc(k :: integer) local i,j,n,Fun,Identity,v,A; n := 5; # \u003c- DIMENSION Fun := (i,j)-\u003epiecewise(((j=i+1) or (i+j=n+1)),1,0); Identity := (i,j)-\u003epiecewise(i=j,1,0); v := matrix(1,n,1); A := piecewise(k\u003e1,(matrix(n,n,Fun))^(k-1),k=1,matrix(n,n,Identity)); return(evalm(v\u0026*A\u0026*transpose(v))[1,1]); end:"
			],
			"mathematica": [
				"LinearRecurrence[{1, 2, -1}, {5, 9, 16}, 32] (* _Jean-François Alcover_, Nov 22 2017 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^40)); Vec(x*(5+4*x-3*x^2)/(1-x-2*x^2+x^3)) \\\\ _G. C. Greubel_, Feb 02 2019",
				"(MAGMA) m:=40; R\u003cx\u003e:=PowerSeriesRing(Integers(), m); Coefficients(R!(  x*(5+4*x-3*x^2)/(1-x-2*x^2+x^3) )); // _G. C. Greubel_, Feb 02 2019",
				"(Sage) a=(x*(5+4*x-3*x^2)/(1-x-2*x^2+x^3)).series(x, 40).coefficients(x, sparse=False); a[1:] # _G. C. Greubel_, Feb 02 2019",
				"(GAP) a:=[5,9,16];; for n in [4..30] do a[n]:=a[n-1]+2*a[n-2]-a[n-3]; od; a; # _G. C. Greubel_, Feb 02 2019"
			],
			"xref": [
				"Cf. A090989, A090991, A090992, A090993, A090994, A090995.",
				"Cf. A000079, A007283, A020701, A020714, A129638."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Branko Malesevic_, Feb 29 2004",
			"ext": [
				"More terms from _Ralf Stephan_, Aug 19 2004",
				"More terms from _Branko Malesevic_ and Ivana Jovovic (ivana121(AT)EUnet.yu), Jun 21 2007"
			],
			"references": 6,
			"revision": 30,
			"time": "2019-03-21T04:37:27-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}