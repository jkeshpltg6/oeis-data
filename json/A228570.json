{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A228570",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 228570,
			"data": "1,1,1,1,1,1,1,1,2,1,2,1,1,3,2,1,3,4,1,4,6,1,1,4,9,2,1,5,12,6,1,5,16,10,1,1,6,20,19,3,1,6,25,28,9,1,7,30,44,19,1,1,7,36,60,38,3,1,8,42,85,66,12,1,8,49,110,110,28,1",
			"name": "Triangle read by rows, formed from antidiagonals of triangle A102541. T(n, k) = A034851(n-2*k, k), n\u003e= 0 and 0 \u003c= k \u003c= floor(n/3).",
			"comment": [
				"The row sums of this triangle are A102543. The antidiagonal sums are given by A192928 and the backwards antidiagonal sums are given by A228571.",
				"Moving the terms in each column of this triangle, see the example, upwards to row 0 gives Losanitsch’s triangle A034851 as a square array.",
				"Also the number of equivalence classes of ways of placing k 3 X 3 tiles in an n X 3 rectangle under all symmetry operations of the rectangle. - _Christopher Hunt Gribble_, Feb 16 2014"
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A228570/b228570.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = A034851(n-2*k, k), n \u003e= 0 and 0 \u003c= k \u003c= floor(n/3).",
				"T(n, k) = T(n-1, k) + T(n-3, k-1) - C((n-4)/2 - 2*(k-1)/2, (k-1)/2) where the last term is present only if n even and k odd; T(0, 0) = 1, T(1, 0) = 1, T(2, 0) = 1, T(n, k) = 0 for n \u003c 0 and T(n, k) = 0 for k \u003c 0 and k  \u003e floor(n/3)."
			],
			"example": [
				"The first few rows of triangle T(n, k) are:",
				"   n/k: 0,  1,  2,  3",
				"   0:   1",
				"   1:   1",
				"   2:   1",
				"   3:   1,  1",
				"   4:   1,  1",
				"   5:   1,  2",
				"   6:   1,  2,  1",
				"   7:   1,  3,  2",
				"   8:   1,  3,  4",
				"   9:   1,  4,  6,  1",
				"  10:   1,  4,  9,  2",
				"  11:   1,  5, 12,  6"
			],
			"maple": [
				"T := proc(n,k) option remember: if n \u003c0 then return(0) fi: if k \u003c 0 or k \u003e floor(n/3) then return(0) fi: A034851(n-2*k, k) end: A034851 := proc(n, k) option remember; local t; if k = 0 or k = n then return(1) fi; if n mod 2 = 0 and k mod 2 = 1 then t := binomial(n/2-1, (k-1)/2) else t := 0; fi; A034851(n-1, k-1) + A034851(n-1, k) - t; end: seq(seq(T(n, k), k=0..floor(n/3)), n=0..18);  # End first program",
				"T := proc(n,k) option remember: if n=0 and k=0 or n=1 and k=0 or n=2 and k=0 then return(1) fi: if k \u003c0 or k \u003e floor(n/3) then return(0) fi: if type(n, even) and type(k, odd) then procname(n-1, k) + procname(n-3, k-1) - binomial((n-4)/2-2*(k-1)/2, (k-1)/2) else procname(n-1, k) + procname(n-3, k-1) fi: end: seq(seq(T(n,k), k=0..floor(n/3)), n=0..18); # End second program"
			],
			"mathematica": [
				"T[n_, k_] := (Binomial[n - 2k, k] + Boole[EvenQ[k] || OddQ[n]] Binomial[(n - 2k - Mod[n, 2])/2, Quotient[k, 2]])/2; Table[T[n, k], {n, 0, 20}, {k, 0, Quotient[n, 3]}] // Flatten (* _Jean-François Alcover_, Oct 06 2017, after _Andrew Howroyd_ *)"
			],
			"program": [
				"(PARI)",
				"T(n,k)={(binomial(n-2*k,k) + (k%2==0||n%2==1)*binomial((n-2*k-n%2)/2,k\\2))/2}",
				"for(n=1,20,for(k=0,(n\\3), print1(T(n,k), \", \"));print) \\\\ _Andrew Howroyd_, May 30 2017"
			],
			"xref": [
				"Cf. A034851, A102541, A228572, A102543, A192928, A228571, A005691."
			],
			"keyword": "nonn,easy,tabf",
			"offset": "0,9",
			"author": "_Johannes W. Meijer_, Aug 26 2013",
			"references": 28,
			"revision": 22,
			"time": "2017-10-06T04:27:44-04:00",
			"created": "2013-08-28T04:46:16-04:00"
		}
	]
}