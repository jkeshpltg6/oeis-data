{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A271237",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 271237,
			"data": "1,1,1,2,2,3,3,3,4,3,4,3,3,3,2,3,2,3,1,2,3,2,2,1,4,3,2,3,3,5,3,4,6,4,5,4,6,4,4,3,5,5,3,6,3,6,4,4,6,3,5,4,4,4,3,4,5,7,4,6,4,5,6,4,10,2,6,8,3,7,4,8,6,5,5,4,5,2,6,1,5,3,3,8,5,7,6,6,9,6,7,6,6,5,5,6,4,6,6,8,1",
			"name": "Number of ordered ways to write n as u^3 + 2*v^3 + 3*x^3 + 4*y^3 + 5*z^3, where u, v, x, y and z are nonnegative integers.",
			"comment": [
				"Conjecture: We have {u^3+a*v^3+b*x^3+c*y^3+d*z^3: u,v,x,y,z = 0,1,2,...} = {0,1,2,...} whenever (a,b,c,d) is among the following 32 quadruples: (1,2,2,3), (1,2,2,4), (1,2,3,4), (1,2,4,5), (1,2,4,6), (1,2,4,9), (1,2,4,10), (1,2,4,11), (1,2,4,18), (1,3,4,6), (1,3,4,9), (1,3,4,10), (2,2,4,5), (2,2,6,9), (2,3,4,5), (2,3,4,6), (2,3,4,7), (2,3,4,8), (2,3,4,9), (2,3,4,10), (2,3,4,12), (2,3,4,15), (2,3,4,18), (2,3,5,6), (2,3,6,12), (2,3,6,15), (2,4,5,6), (2,4,5,8), (2,4,5,9), (2,4,5,10), (2,4,6,7), (2,4,7,10).",
				"In particular, this implies that a(n) \u003e 0 for all n = 0,1,2,... We guess that a(n) = 1 only for n = 0, 1, 2, 18, 23, 79, 100.",
				"If {m*u^3+a*v^3+b*x^3+c*y^3+d*z^3: u,v,x,y,z = 0,1,2,...} = {0,1,2,...} with 1 \u003c= m \u003c= a \u003c= b \u003c= c \u003c= d,  then m = 1, and we can show that (a,b,c,d) must be among the 32 quadruples listed in the conjecture (cf. Theorem 1.2 of the linked 2017 paper).",
				"It is known that there are exactly 54 quadruples (a,b,c,d) with 1 \u003c= a \u003c= b \u003c= c \u003c= d such that {a*w^2+b*x^2+c*y^2+d*z^2: w,x,y,z = 0,1,2,...} = {0,1,2,...}.",
				"See also A271099 and A271169 for conjectures refining Waring's problem.",
				"We also conjecture that if P(u,v,x,y,z) is one of the four polynomials u^6+v^3+2*x^3+4*y^3+5*z^3 and a*u^6+v^3+2*x^3+3*y^3+4*z^3 (a = 5,8,12) then any natural number can be written as P(u,v,x,y,z) with u,v,x,y,z nonnegative integers. - _Zhi-Wei Sun_, Apr 06 2016"
			],
			"reference": [
				"S. Ramanujan, On the expression of a number in the form a*x^2 + b*y^2 + c*z^2 + d*w^2, Proc. Cambridge Philos. Soc. 19(1917), 11-21."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A271237/b271237.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"L. E. Dickson, \u003ca href=\"http://www.jstor.org/stable/2370770\"\u003eQuaternary quadratic forms representing all integers\u003c/a\u003e, Amer. J. Math. 49(1927), 39-56.",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2015.10.014\"\u003eA result similar to Lagrange's theorem\u003c/a\u003e, J. Number Theory 162(2016), 190-211.",
				"Zhi-Wei Sun, \u003ca href=\"http://maths.nju.edu.cn/~zwsun/179b.pdf\"\u003eNew conjectures on representations of integers (I)\u003c/a\u003e, Nanjing Univ. J. Math. Biquarterly 34(2017), no. 2, 97-120."
			],
			"example": [
				"a(2) = 1 since 2 = 0^3 + 2*1^3 + 3*0^3 + 4*0^3 + 5*0^3.",
				"a(18) = 1 since 18 = 2^3 + 2*1^3 + 3*1^3 + 4*0^3 + 5*1^3.",
				"a(23) = 1 since 23 = 0^3 + 2*2^3 + 3*1^3 + 4*1^3 + 5*0^3.",
				"a(79) = 1 since 79 = 1^3 + 2*3^3 + 3*2^3 + 4*0^3 + 5*0^3.",
				"a(100) = 1 since 100 = 2^3 + 2*1^3 + 3*3^3 + 4*1^3 + 5*1^3."
			],
			"mathematica": [
				"CQ[n_]:=CQ[n]=IntegerQ[n^(1/3)]",
				"Do[r=0;Do[If[CQ[n-5z^3-4y^3-3x^3-2v^3],r=r+1],{z,0,(n/5)^(1/3)},{y,0,((n-5z^3)/4)^(1/3)},{x,0,((n-5z^3-4y^3)/3)^(1/3)},{v,0,((n-5z^3-4y^3-3x^3)/2)^(1/3)}];Print[n,\" \",r];Continue,{n,0,100}]"
			],
			"xref": [
				"Cf. A000578, A002804, A001014, A271099, A271169."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Zhi-Wei Sun_, Apr 02 2016",
			"references": 4,
			"revision": 22,
			"time": "2017-12-30T09:43:42-05:00",
			"created": "2016-04-02T20:22:24-04:00"
		}
	]
}