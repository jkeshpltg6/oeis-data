{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A127687",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 127687,
			"data": "0,1,1,1,1,2,1,2,2,3,2,4,3,5,6,7,7,11,11,16,19,24,28,39,46,60,75,97,120,159,197,257,327,422,539,700,892,1157,1488,1928,2479,3219,4148,5383,6961,9029,11687,15184,19673,25564,33174,43125,56010,72868,94719,123283",
			"name": "Number of unlabeled maximal independent sets in the n-cycle graph.",
			"comment": [
				"Number of unlabeled (i.e., defined up to a rotation) maximal independent sets in the n-cycle graph. Also: Number of cyclic compositions of n in which each term is either 2 or 3.",
				"(a(n)*n - A001608(n)) mod 2 is a binary sequence of period 14: [0,0,0,0,0,1,0,0,0,1,0,1,0,1]. - _Richard Turk_, Aug 25 2017"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A127687/b127687.txt\"\u003eTable of n, a(n) for n = 1..5000\u003c/a\u003e",
				"R. Bisdorff and J.-L. Marichal, \u003ca href=\"https://arxiv.org/abs/math/0701647\"\u003eCounting non-isomorphic maximal independent sets of the n-cycle graph\u003c/a\u003e, arXiv:0701647 [math.CO] (2007) and \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL11/Marichal/marichal.html\"\u003eJIS 11 (2008) 08.5.7\u003c/a\u003e.",
				"P. Flajolet and M. Soria, \u003ca href=\"http://algo.inria.fr/flajolet/Publications/publist.html\"\u003eThe Cycle Construction\u003c/a\u003e In SIAM J. Discr. Math., vol. 4 (1), 1991, pp. 58-60."
			],
			"formula": [
				"a(n) = Sum_{d|n} A113788(d) = 2 * A127685(n) - A127682(n) = (1/n)*(Sum_{d|n} A000010(n/d) * A001608(d)).",
				"G.f.: Sum_{k\u003e=1} (phi(k)/k) * log( 1/(1-B(x^k)) ) where B(x) = x^2 + x^3. - _Joerg Arndt_, Jan 21 2013"
			],
			"mathematica": [
				"(* p = A001608 *) p[n_] := p[n] = p[n - 2] + p[n - 3]; p[0] = 3; p[1] = 0; p[2] = 2; A113788[n_] := (1/n)*Sum[MoebiusMu[n/d]*p[d], {d, Divisors[n]}]; a[n_] := Sum[A113788[d], {d, Divisors[n]}]; Table[a[n], {n, 1, 56}] (* _Jean-François Alcover_, Jul 16 2012, from formula *)"
			],
			"program": [
				"(PARI)",
				"N = 66;  x = 'x + O('x^N);",
				"f(x) = x^2+x^3;",
				"gf = 'c0 + sum(n=1,N, eulerphi(n)/n*log(1/(1-f(x^n)))  );",
				"v = Vec(gf); v[1]-='c0; v  /* includes a(0)=0 */",
				"/* _Joerg Arndt_, Jan 21 2013 */"
			],
			"xref": [
				"Cf. A001608, A113788, A127682, A127685."
			],
			"keyword": "easy,nonn",
			"offset": "1,6",
			"author": "Jean-Luc Marichal (jean-luc.marichal(AT)uni.lu), Jan 24 2007",
			"references": 3,
			"revision": 21,
			"time": "2017-09-02T21:20:55-04:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}