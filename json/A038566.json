{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A038566",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 38566,
			"data": "1,1,1,2,1,3,1,2,3,4,1,5,1,2,3,4,5,6,1,3,5,7,1,2,4,5,7,8,1,3,7,9,1,2,3,4,5,6,7,8,9,10,1,5,7,11,1,2,3,4,5,6,7,8,9,10,11,12,1,3,5,9,11,13,1,2,4,7,8,11,13,14,1,3,5,7,9,11,13,15,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16",
			"name": "Numerators in canonical bijection from positive integers to positive rationals \u003c= 1: arrange fractions by increasing denominator then by increasing numerator.",
			"comment": [
				"Row n has length A000010(n).",
				"Also numerators in canonical bijection from positive integers to all positive rational numbers: arrange fractions in triangle in which in the n-th row the phi(n) numbers are the fractions i/j with gcd(i,j) = 1, i+j=n, i=1..n-1, j=n-1..1. n\u003e=2. Denominators (A020653) are obtained by reversing each row.",
				"Also triangle in which n-th row gives phi(n) numbers between 1 and n that are relatively prime to n.",
				"A038610(n) = least common multiple of n-th row. - _Reinhard Zumkeller_, Sep 21 2013",
				"Row n has sum A023896(n). - _Jamie Morken_, Dec 17 2019",
				"This irregular triangle gives in row n the smallest positive reduced residue system modulo n, for n \u003e= 1. If one takes 0 for n = 1 it becomes the smallest nonnegative residue system modulo n. - _Wolfdieter Lang_, Feb 29 2020"
			],
			"reference": [
				"Richard Courant and Herbert Robbins. What Is Mathematics?, Oxford, 1941, pp. 79-80.",
				"H. Lauwerier, Fractals, Princeton Univ. Press, p. 23."
			],
			"link": [
				"David Wasserman, \u003ca href=\"/A038566/b038566.txt\"\u003eTable of n, a(n) for n = 1..100001\u003c/a\u003e",
				"Wolfdieter Lang, \u003ca href=\"/A038566/a038566.pdf\"\u003eRows of rationals, n=2..25.\u003c/a\u003e",
				"\u003ca href=\"/index/Cor#core\"\u003eIndex entries for \"core\" sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Ra#rational\"\u003eIndex entries for sequences related to enumerating the rationals\u003c/a\u003e",
				"\u003ca href=\"/index/St#Stern\"\u003eIndex entries for sequences related to Stern's sequences\u003c/a\u003e"
			],
			"formula": [
				"The n-th \"clump\" consists of the phi(n) integers \u003c= n and prime to n.",
				"a(n) = A002260(A169581(n)). - _Reinhard Zumkeller_, Dec 02 2009",
				"a(n+1) = A020652(n) for n \u003e 1. - _Georg Fischer_, Oct 27 2020"
			],
			"example": [
				"The beginning of the list of positive rationals \u003c= 1: 1/1, 1/2, 1/3, 2/3, 1/4, 3/4, 1/5, 2/5, 3/5, .... This is A038566/A038567.",
				"The beginning of the triangle giving all positive rationals: 1/1; 1/2, 2/1; 1/3, 3/1; 1/4, 2/3, 3/2, 4/1; 1/5, 5/1; 1/6, 2/5, 3/4, 4/3, 5/2, 6/1; .... This is A020652/A020653, with A020652(n) = A038566(n+1). [Corrected by _M. F. Hasler_, Mar 06 2020]",
				"The beginning of the triangle in which n-th row gives numbers between 1 and n that are relatively prime to n:",
				"n\\k 1 2 3  4  5  6  7  8 9 10 11 12 13 14 15 16 17 18",
				"1:  1",
				"2:  1",
				"3:  1 2",
				"4:  1 3",
				"5:  1 2 3  4",
				"6:  1 5",
				"7:  1 2 3  4  5  6",
				"8:  1 3 5  7",
				"9:  1 2 4  5  7  8",
				"10: 1 3 7  9",
				"11: 1 2 3  4  5  6  7  8 9 10",
				"12: 1 5 7 11",
				"13: 1 2 3  4  5  6  7  8 9 10 11 12",
				"14: 1 3 5  9 11 13",
				"15: 1 2 4  7  8 11 13 14",
				"16: 1 3 5  7  9 11 13 15",
				"17: 1 2 3  4  5  6  7  8 9 10 11 12 13 14 15 16",
				"18: 1 5 7 11 13 17",
				"19: 1 2 3  4  5  6  7  8 9 10 11 12 13 14 15 16 17 18",
				"20: 1 3 7  9 11 13 17 19",
				"... Reformatted. - _Wolfdieter Lang_, Jan 18 2017",
				"------------------------------------------------------"
			],
			"maple": [
				"s := proc(n) local i,j,k,ans; i := 0; ans := [ ]; for j while i\u003cn do for k to j do if gcd(j,k) = 1 then ans := [ op(ans),k ]; i := i+1 fi od od; RETURN(ans); end; s(100);"
			],
			"mathematica": [
				"Flatten[Table[Flatten[Position[GCD[Table[Mod[j, w], {j, 1, w-1}], w], 1]], {w, 1, 100}], 2]"
			],
			"program": [
				"(Haskell)",
				"a038566 n k = a038566_tabf !! (n-1) !! (k-1)",
				"a038566_row n = a038566_tabf !! (n-1)",
				"a038566_tabf=",
				"   zipWith (\\v ws -\u003e filter ((== 1) . (gcd v)) ws) [1..] a002260_tabl",
				"a038566_list = concat a038566_tabf",
				"-- _Reinhard Zumkeller_, Sep 21 2013, Feb 23 2012",
				"(PARI) first(n)=my(v=List(),i,j);while(i\u003cn,for(k=1,j,if(gcd(j,k)==1, listput(v,k);i++));j++);Vec(v) \\\\ _Charles R Greathouse IV_, Feb 07 2013",
				"(PARI) row(n) = select(x-\u003egcd(n, x)==1, [1..n]); \\\\ _Michel Marcus_, May 05 2020",
				"(SageMath)",
				"def aRow(n):",
				"    if n == 1: return 1",
				"    return [k for k in ZZ(n).coprime_integers(n+1)]",
				"print(flatten([aRow(n) for n in range(1, 18)])) # _Peter Luschny_, Aug 17 2020"
			],
			"xref": [
				"Cf. A020652, A020653, A038566, A038567, A038568, A038569, A000010 (row lengths), A002088, A060837, A071970, A002260.",
				"A054424 gives mapping to Stern-Brocot tree.",
				"Row sums give rationals A111992(n)/A069220(n), n\u003e=1.",
				"A112484 (primes, rows n \u003e=3)."
			],
			"keyword": "nonn,frac,core,nice,tabf",
			"offset": "1,4",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Erich Friedman_",
				"Offset corrected by _Max Alekseyev_, Apr 26 2010"
			],
			"references": 83,
			"revision": 81,
			"time": "2021-10-21T01:14:24-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}