{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A111035",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 111035,
			"data": "1,2,24,48,72,77,96,120,144,192,216,240,288,319,323,336,360,384,432,480,576,600,648,672,720,768,864,960,1008,1080,1104,1152,1200,1224,1296,1320,1344,1368,1440,1517,1536,1680,1728,1800,1920,1944,2016,2064,2160",
			"name": "Numbers n that divide the sum of the first n nonzero Fibonacci numbers.",
			"comment": [
				"The sum of the first n nonzero Fibonacci numbers is F(n+2)-1, sequence A000071. Knott discusses the factorization of these numbers. Most of the terms are divisible by 24. - _T. D. Noe_, Oct 10 2005, edited by _M. F. Hasler_, Mar 01 2020",
				"All terms are either multiples of 24 (cf. A124455) or odd (cf. A331976) or congruent to 2 (mod 12), cf. A331870 where this statement is proved. - _M. F. Hasler_, Mar 01 2020"
			],
			"link": [
				"Giovanni Resta, \u003ca href=\"/A111035/b111035.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (first 1000 terms from T. D. Noe)",
				"Ron Knott, \u003ca href=\"http://www.maths.surrey.ac.uk/hosted-sites/R.Knott/Fibonacci/fibmaths.html#noprimenghbrs\"\u003eThe Mathematical Magic of the Fibonacci Numbers\u003c/a\u003e",
				"Daniel Yaqubi and Amirali Fatehizadeh, \u003ca href=\"https://arxiv.org/abs/2001.11839\"\u003eSome results on average of Fibonacci and Lucas sequences\u003c/a\u003e, arXiv:2001.11839 [math.CO], 2020."
			],
			"formula": [
				"{n: n| A000071(n+2)}. - _R. J. Mathar_, Feb 05 2020"
			],
			"example": [
				"2 | 4, 24 | 121392, 48 | 12586269024, ... [Corrected by _M. F. Hasler_, Feb 06 2020]"
			],
			"maple": [
				"select(n-\u003e irem(combinat[fibonacci](n+2)-1, n)=0, [$1..3000])[]; # _G. C. Greubel_, Feb 03 2020"
			],
			"mathematica": [
				"Select[Range[3000], Mod[Fibonacci[ #+2]-1, # ]==0\u0026] (*  _T. D. Noe_, Oct 06 2005 *)"
			],
			"program": [
				"(PARI) is(n)=((Mod([1,1;1,0],n))^(n+2))[1,2]==1 \\\\ _Charles R Greathouse IV_, Feb 04 2013",
				"(MAGMA) [1] cat [n: n in [1..3000] | Fibonacci(n+2) mod n eq 1 ]; // _G. C. Greubel_, Feb 03 2020",
				"(Sage) [n for n in (1..3000) if mod(fibonacci(n+2), n)==1 ] # _G. C. Greubel_, Feb 03 2020",
				"(GAP) Filtered([1..3000], n-\u003e ((Fibonacci(n+2)-1) mod n)=0 ); # _G. C. Greubel_, Feb 03 2020"
			],
			"xref": [
				"See A101907 for another version.",
				"Cf. A111058 (the analog for Lucas numbers).",
				"Cf. A124455 (k for a(n) = 24k), A124456 (other a(n)), A331976 (odd a(n)), A331870 (even a(n) != 24k)."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Joseph L. Pe_, Oct 05 2005",
			"ext": [
				"More terms from _Rick L. Shepherd_ and _T. D. Noe_, Oct 06 2005"
			],
			"references": 11,
			"revision": 38,
			"time": "2020-03-01T12:15:13-05:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}