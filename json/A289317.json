{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A289317",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 289317,
			"data": "1,1,1,3,7,23,84,364,1792,9953,61455,417720,3098515,24902930,215538825,1998518430,19761943208,207571259703,2307812703419,27075591512866,334263981931669",
			"name": "The number of upper-triangular matrices whose nonzero entries are positive odd numbers summing to n and each row and each column contains a nonzero entry.",
			"comment": [
				"A Fishburn matrix of size n is defined to be an upper-triangular matrix with nonnegative integer entries which sum to n and each row and each column contains a nonzero entry. See A022493. Here we are considering Fishburn matrices where the nonzero entries are all odd.",
				"The g.f. for primitive Fishburn matrices (i.e., Fishburn matrices with entries restricted to the set {0,1}), is F(x) = Sum_{n\u003e=0} Product_{k=1..n} ( 1 - 1/(1 + x)^k ). See A138265. Let C(x) = x/(1 - x^2) = x + x^3 + x^5 + x^7 + .... Then applying Lemma 2.2.22 of Goulden and Jackson gives the g.f. for this sequence as the composition F(C(x))."
			],
			"reference": [
				"I. P. Goulden and D. M. Jackson, Combinatorial Enumeration, Wiley, N.Y., 1983, p. 42."
			],
			"link": [
				"Hsien-Kuei Hwang, Emma Yu Jin, \u003ca href=\"https://arxiv.org/abs/1911.06690\"\u003eAsymptotics and statistics on Fishburn matrices and their generalizations\u003c/a\u003e, arXiv:1911.06690 [math.CO], 2019."
			],
			"formula": [
				"G.f.: A(x) = Sum_{n \u003e= 0} Product_{k = 1..n} ( 1 - 1/(1 + x/(1 - x^2))^k )."
			],
			"example": [
				"a(4) = 7: The Fishburn matrices of size 4 with odd nonzero entries are",
				"/3 0\\ /1 0\\",
				"\\0 1/ \\0 3/",
				"/1 1 0\\ /1 0 1\\ /1 0 0\\",
				"|0 1 0| |0 1 0| |0 1 1|",
				"\\0 0 1/ \\0 0 1/ \\0 0 1/",
				"/1 1 0\\",
				"|0 0 1|",
				"\\0 0 1/",
				"/1 0 0 0\\",
				"|0 1 0 0|",
				"|0 0 1 0|",
				"\\0 0 0 1/"
			],
			"maple": [
				"C:= x -\u003e x/(1 - x^2):",
				"G:= add(mul( 1 - 1/(1 + C(x))^k, k=1..n), n=0..20):",
				"S:= series(G,x,21):",
				"seq(coeff(S,x,j),j=0..20);"
			],
			"xref": [
				"Cf. A022493, A138265, A289312, A289313, A289314, A289315, A289316."
			],
			"keyword": "nonn,easy",
			"offset": "0,4",
			"author": "_Peter Bala_, Jul 25 2017",
			"references": 1,
			"revision": 9,
			"time": "2020-02-07T13:41:44-05:00",
			"created": "2017-07-25T12:06:16-04:00"
		}
	]
}