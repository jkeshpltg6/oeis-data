{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A087903",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 87903,
			"data": "1,1,1,1,4,1,1,11,9,1,1,26,48,16,1,1,57,202,140,25,1,1,120,747,916,325,36,1,1,247,2559,5071,3045,651,49,1,1,502,8362,25300,23480,8260,1176,64,1,1,1013,26520,117962,159736,84456,19404,1968,81,1,1,2036,82509,525608",
			"name": "Triangle read by rows of the numbers T(n,k) (n \u003e 1, 0 \u003c k \u003c n) of set partitions of n of length k which do not have a proper subset of parts with a union equal to a subset {1,2,...,j} with j \u003c n.",
			"comment": [
				"T(n,n-1) = T(n,1) = 1; T(n,n-2) = (n-2)^2; T(n,2) = A000295(n).",
				"Another version of the triangle T(n,k), 0 \u003c= k \u003c= n, given by [1, 0, 2, 0, 3, 0, 4, 0, 5, 0, 6, ...] DELTA [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, ...] where DELTA is the operator defined in A084938; see also A086329 for a triangle transposed. - _Philippe Deléham_, Jun 13 2004"
			],
			"link": [
				"V. E. Adler, \u003ca href=\"http://arxiv.org/abs/1510.02900\"\u003eSet partitions and integrable hierarchies\u003c/a\u003e, arXiv:1510.02900 [nlin.SI], 2015.",
				"M. Rosas, B. Sagan, \u003ca href=\"http://arxiv.org/abs/math/0208168\"\u003eSymmetric functions in noncommuting variables\u003c/a\u003e, arXiv:math/0208168 [math.CO], 2002, 2004.",
				"M. C. Wolf, \u003ca href=\"http://dx.doi.org/10.1215/S0012-7094-36-00253-3\"\u003eSymmetric Functions of Non-commutative Elements\u003c/a\u003e, Duke Math. J., 2 (1936), 626-637."
			],
			"formula": [
				"T(n, k) = S2(n-1, k) + Sum_{j=0..n-2} Sum_{d=0..k-1} (k-d-1)*T(n-j-1, k-d)*S2(j, d), where S2(n, k) is the Stirling number of the second kind.",
				"Sum_{k = 1..n-1} T(n, k) = A074664(n). - _Philippe Deléham_, Jun 13 2004",
				"G.f.: 1-1/(1+add(add(q^n t^k S2(n, k), k=1..n), n \u003e= 1)) where S2(n, k) are the Stirling numbers of the 2nd kind A008277. - _Mike Zabrocki_, Sep 03 2005"
			],
			"example": [
				"T(2,1)=1 for {12};",
				"T(3,1)=1, T(3,2) = 1 for {123}; {13|2};",
				"T(4,1)=1, T(4,2)=4, T(4,3)=1 for {1234}; {14|23}, {13|24}, {124|3}, {134|2}; {14|2|3}.",
				"From _Philippe Deléham_, Jul 16 2007: (Start)",
				"Triangle begins:",
				"  1;",
				"  1,    1;",
				"  1,    4,     1;",
				"  1,   11,     9,      1;",
				"  1,   26,    48,     16,      1;",
				"  1,   57,   202,    140,     25,     1;",
				"  1,  120,   747,    916,    325,    36,     1;",
				"  1,  247,  2559,   5071,   3045,   651,    49,    1;",
				"  1,  502,  8362,  25300,  23480,  8260,  1176,   64,  1;",
				"  1, 1013, 26520, 117962, 159736, 84456, 19404, 1968, 81, 1;",
				"  ...",
				"Triangle T(n,k), 0 \u003c= k \u003c= n, given by [1,0,2,0,3,0,4,0,...] DELTA [0,1,0,1,0,1,0,...] begins:",
				"  1;",
				"  1,    0;",
				"  1,    1,     0;",
				"  1,    4,     1,      0;",
				"  1,   11,     9,      1,      0;",
				"  1,   26,    48,     16,      1,     0;",
				"  1,   57,   202,    140,     25,     1,     0;",
				"  1,  120,   747,    916,    325,    36,     1,    0;",
				"  1,  247,  2559,   5071,   3045,   651,    49,    1,  0;",
				"  1,  502,  8362,  25300,  23480,  8260,  1176,   64,  1, 0;",
				"  1, 1013, 26520, 117962, 159736, 84456, 19404, 1968, 81, 1, 0;",
				"  ...",
				"(End)"
			],
			"maple": [
				"A := proc(n,k) option remember; local j,ell; if n\u003c=0 or k\u003e=n then 0; elif k=1 or k=n-1 then 1; else S2(n-1,k)+add(add((k-ell-1)*A(n-j-1,k-ell)*S2(j,ell),ell=0..k-1),j=0..n-2); fi; end: S2 := (n,k)-\u003eif n\u003c0 or k\u003en then 0; elif k=n or k=1 then 1 else k*S2(n-1,k)+S2(n-1,k-1); fi:"
			],
			"mathematica": [
				"nmax = 12; t[n_, k_] := t[n, k] = StirlingS2[n-1, k] + Sum[ (k-d-1)*t[n-j-1, k-d]*StirlingS2[j, d], {d, 0, k-1}, {j, 0, n-2}]; Flatten[ Table[ t[n, k], {n, 2, nmax}, {k, 1, n-1}]] (* _Jean-François Alcover_, Oct 04 2011, after given formula *)"
			],
			"xref": [
				"Cf. A008277, A055106, A074664, A000110.",
				"Cf. A055105."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "2,5",
			"author": "_Mike Zabrocki_, Oct 14 2003",
			"references": 11,
			"revision": 27,
			"time": "2020-01-25T18:07:10-05:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}