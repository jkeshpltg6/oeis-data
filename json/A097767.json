{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097767",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97767,
			"data": "1,485,235709,114554089,55673051545,27056988496781,13149640736384021,6390698340894137425,3105866244033814404529,1509444603902092906463669,733586971630173118726938605,356521758767660233608385698361",
			"name": "Pell equation solutions (11*b(n))^2 - 122*a(n)^2 = -1 with b(n):=A097766(n), n \u003e= 0.",
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A097767/b097767.txt\"\u003eTable of n, a(n) for n = 0..372\u003c/a\u003e",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"Giovanni Lucca, \u003ca href=\"http://forumgeom.fau.edu/FG2019volume19/FG201902index.html\"\u003eInteger Sequences and Circle Chains Inside a Hyperbola\u003c/a\u003e, Forum Geometricorum (2019) Vol. 19, 11-16.",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (486,-1)."
			],
			"formula": [
				"a(n) = S(n, 2*243) - S(n-1, 2*243) = T(2*n+1, sqrt(122))/sqrt(122), with Chebyshev polynomials of the 2nd and first kind. See A049310 for the triangle of S(n, x)= U(n, x/2) coefficients. S(-1, x) := 0 =: U(-1, x); and A053120 for the T-triangle.",
				"a(n) = ((-1)^n)*S(2*n, 22*i) with the imaginary unit i and Chebyshev polynomials S(n, x) with coefficients shown in A049310.",
				"G.f.: (1-x)/(1-486*x+x^2).",
				"a(n) = 486*a(n-1) - a(n-2), n \u003e 1; a(0)=1, a(1)=485. - _Philippe Deléham_, Nov 18 2008"
			],
			"example": [
				"(x,y) = (11*1=11;1), (5357=11*487;485), (2603491=11*236681;235709), ... give the positive integer solutions to x^2 - 122*y^2 =-1."
			],
			"mathematica": [
				"LinearRecurrence[{486, -1},{1, 485},20] (* _Ray Chandler_, Aug 12 2015 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^20)); Vec((1-x)/(1-486*x+x^2)) \\\\ _G. C. Greubel_, Aug 01 2019",
				"(MAGMA) I:=[1,485]; [n le 2 select I[n] else 486*Self(n-1) - Self(n-2): n in [1..20]]; // _G. C. Greubel_, Aug 01 2019",
				"(Sage) ((1-x)/(1-486*x+x^2)).series(x, 20).coefficients(x, sparse=False) # _G. C. Greubel_, Aug 01 2019",
				"(GAP) a:=[1,485];; for n in [3..20] do a[n]:=486*a[n-1]-a[n-2]; od; a; # _G. C. Greubel_, Aug 01 2019"
			],
			"xref": [
				"Cf. A097765 for S(n, 486).",
				"Row 11 of array A188647."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Aug 31 2004",
			"references": 5,
			"revision": 35,
			"time": "2020-01-23T01:48:49-05:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}