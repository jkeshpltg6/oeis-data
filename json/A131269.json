{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A131269",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 131269,
			"data": "1,2,3,6,11,20,35,60,101,168,277,454,741,1206,1959,3178,5151,8344,13511,21872,35401,57292,92713,150026,242761,392810,635595,1028430,1664051,2692508,4356587,7049124,11405741,18454896,29860669,48315598,78176301,126491934",
			"name": "a(n) = 3*a(n-1) - 2*a(n-2) - a(n-3) + a(n-4) with n\u003e3, a(0)=1, a(1)=2, a(2)=3, a(3)=6.",
			"comment": [
				"Row sums of triangles A131268 and A131270.",
				"a(n)/a(n-1) tends to phi (A001622)."
			],
			"link": [
				"Bruno Berselli, \u003ca href=\"/A131269/b131269.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-2,-1,1)."
			],
			"formula": [
				"a(n) = a(n-2) + a(n-1) + n - 2 with n\u003e1, a(0)=1, a(1)=2. - _Alex Ratushnyak_, May 02 2012",
				"From _Bruno Berselli_, May 03 2012: (Start)",
				"G.f.: (1-x-x^2+2*x^3)/((1-x-x^2)*(1-x)^2). - _Bruno Berselli_, May 03 2012",
				"a(n) = A001595(n+1) - n = A006355(n+3) - n - 1 = ((1+sqrt(5))^(n+2) - (1-sqrt(5))^(n+2))/(2^(n+1)*sqrt(5))-n-1. (End)"
			],
			"example": [
				"a(4) = 11 = sum of row 4 terms of triangle A131268: ((1 + 1 + 5 + 3 + 1), or the reversed terms of triangle A131270, row 4."
			],
			"mathematica": [
				"LinearRecurrence[{3, -2, -1, 1}, {1, 2, 3, 6}, 41] (* _Bruno Berselli_, May 03 2012 *)",
				"Table[2*Fibonacci[n+2]-n-1, {n,0,40}] (* _G. C. Greubel_, Jul 09 2019 *)"
			],
			"program": [
				"(Python)",
				"prpr = 1",
				"prev = 2",
				"for n in range(2,99):",
				"    current = prpr + prev + n - 2",
				"    print(prpr, end=',')",
				"    prpr = prev",
				"    prev = current  # from _Alex Ratushnyak_, May 02 2012",
				"# Contribution from _Bruno Berselli_, May 03 2012: (Start)",
				"(PARI) Vec((1-x-x^2+2*x^3)/((1-x-x^2)*(1-x)^2)+O(x^40))",
				"(MAGMA) /* By the first comment: */ [\u0026+[2*Binomial(n-Floor((k+1)/2), Floor(k/2))-1: k in [0..n]]: n in [0..40]];",
				"(Maxima) makelist(expand(((1+sqrt(5))^(n+2)-(1-sqrt(5))^(n+2) )/(2^(n+1)*sqrt(5))-n-1), n, 0, 40); (End)",
				"(PARI) vector(40, n, n--; 2*fibonacci(n+2)-n-1) \\\\ _G. C. Greubel_, Jul 09 2019",
				"(MAGMA) [2*Fibonacci(n+2)-n-1: n in [0..40]]; // _G. C. Greubel_, Jul 09 2019",
				"(Sage) [2*fibonacci(n+2)-n-1 for n in (0..40)] # _G. C. Greubel_, Jul 09 2019",
				"(GAP) List([0..40], n-\u003e 2*Fibonacci(n+2)-n-1) # _G. C. Greubel_, Jul 09 2019"
			],
			"xref": [
				"Cf. A000045, A001622, A065941, A131268, A131270.",
				"Cf. A001595 (first differences)."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Gary W. Adamson_, Jun 23 2007",
			"ext": [
				"Better definition and more terms from _Bruno Berselli_, May 03 2012"
			],
			"references": 7,
			"revision": 34,
			"time": "2020-04-09T11:33:44-04:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}