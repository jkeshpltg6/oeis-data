{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A322483",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 322483,
			"data": "1,2,2,2,2,4,2,3,2,4,2,4,2,4,4,3,2,4,2,4,4,4,2,6,2,4,3,4,2,8,2,4,4,4,4,4,2,4,4,6,2,8,2,4,4,4,2,6,2,4,4,4,2,6,4,6,4,4,2,8,2,4,4,4,4,8,2,4,4,8,2,6,2,4,4,4,4,8,2,6,3,4,2,8,4,4,4",
			"name": "The number of semi-unitary divisors of n.",
			"comment": [
				"The notion of semi-unitary divisor was introduced by Chidambaraswamy in 1967.",
				"A semi-unitary divisor of n is defined as the largest divisor d of n such that the largest divisor of d that is a unitary divisor of n/d is 1. In terms of the relation defined in A322482, d is the largest divisor of n such that T(d, n/d) = 1 (the largest divisor d that is semi-prime to n/d)."
			],
			"reference": [
				"J. Chidambaraswamy, Sum functions of unitary and semi-unitary divisors, J. Indian Math. Soc., Vol. 31 (1967), pp. 117-126."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A322483/b322483.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Krishnaswami Alladi, \u003ca href=\"https://doi.org/10.1017/S1446788700017304\"\u003eOn arithmetic functions and divisors of higher order\u003c/a\u003e, Journal of the Australian Mathematical Society, Vol. 23, No. 1 (1977), pp. 9-27.",
				"Pentti Haukkanen, \u003ca href=\"https://www.researchgate.net/profile/Pentti_Haukkanen/publication/266363785_Basic_properties_of_the_bi-unitary_convolution_and_the_semi-unitary_convolution/links/5469efe50cf2397f782f4e3b/Basic-properties-of-the-bi-unitary-convolution-and-the-semi-unitary-convolution.pdf\"\u003eBasic properties of the bi-unitary convolution and the semi-unitary convolution\u003c/a\u003e, Indian J. Math, Vol. 40 (1998), pp. 305-315.",
				"D. Suryanarayana, \u003ca href=\"https://doi.org/10.1007/BFb0058797\"\u003eThe number of bi-unitary divisors of an integer\u003c/a\u003e, The theory of arithmetic functions. Springer, Berlin, Heidelberg, 1972, pp. 273-282.",
				"D. Suryanarayana and V. Siva Rama Prasad, \u003ca href=\"https://doi.org/10.1017/S1446788700012908\"\u003eSum functions of k-ary and semi-k-ary divisors\u003c/a\u003e, Journal of the Australian Mathematical Society, Vol. 15, No. 2 (1973), pp. 148-162.",
				"Laszlo Tóth, \u003ca href=\"http://annalesm.elte.hu/annales41-1998/Annales_1998_T-XLI.pdf#page=167\"\u003eSum functions of certain generalized divisors\u003c/a\u003e, Ann. Univ. Sci. Budap. Rolando Eötvös, Sect. Math., Vol. 41 (1998), pp. 165-180."
			],
			"formula": [
				"Multiplicative with a(p^e) = floor((e+3)/2).",
				"a(n) \u003c= A000005(n) with equality if and only if n is squarefree (A005117).",
				"a(n) = Sum_{d|n} mu(d/gcd(d, n/d))^2. - _Ilya Gutkovskiy_, Feb 21 2020"
			],
			"example": [
				"The semi-unitary divisors of 8 are 1, 2, 8 (4 is not semi-unitary divisor since the largest divisor of 4 that is a unitary divisor of 8/4 = 2 is 2 \u003e 1), and their number is 3, thus a(8) = 3."
			],
			"mathematica": [
				"f[p_, e_] := Floor[(e+3)/2]; sud[n_] := If[n==1, 1, Times @@ (f @@@ FactorInteger[n])]; Array[sud, 100]"
			],
			"program": [
				"(PARI) a(n) = {my(f = factor(n)); for (k=1, #f~, f[k,1] = (f[k,2]+3)\\2; f[k,2] = 1;); factorback(f);} \\\\ _Michel Marcus_, Dec 14 2018"
			],
			"xref": [
				"Cf. A000005, A005117, A034444, A286324, A322482."
			],
			"keyword": "nonn,mult",
			"offset": "1,2",
			"author": "_Amiram Eldar_, Dec 11 2018",
			"references": 7,
			"revision": 15,
			"time": "2020-02-22T02:10:39-05:00",
			"created": "2018-12-18T11:29:12-05:00"
		}
	]
}