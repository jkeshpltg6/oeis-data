{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A101449",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 101449,
			"data": "1,1,2,4,4,4,11,24,12,8,41,88,96,32,16,146,410,440,320,80,32,564,1752,2460,1760,960,192,64,2199,7896,12264,11480,6160,2688,448,128,8835,35184,63168,65408,45920,19712,7168,1024,256,35989,159030,316656,379008",
			"name": "Triangle read by rows: T(n,k) is number of noncrossing trees with n edges and having k nonroot nodes of degree 1.",
			"comment": [
				"Row n contains n terms.",
				"Row sums yield the ternary numbers (A001764).",
				"The average number of nonroot nodes of degree 1 over all noncrossing trees with n edges is 4n(n-1)(2n+1)/(3(3n-1)(3n-2)) ~ 8n/27."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A101449/b101449.txt\"\u003eTable of n, a(n) for n = 1..1275\u003c/a\u003e",
				"P. Flajolet and M. Noy, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(98)00372-0\"\u003eAnalytic combinatorics of non-crossing configurations\u003c/a\u003e, Discrete Math., 204, 203-229, 1999.",
				"M. Noy, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(97)00121-0\"\u003eEnumeration of noncrossing trees on a circle\u003c/a\u003e, Discrete Math., 180, 301-313, 1998."
			],
			"formula": [
				"T(n, k) = [2^k/(n-k)]*binomial(n-1, k)*Sum_{i=1..n-k} (-1)^(n-k-i)*2^(n-k-i)*binomial(n-k, i)*binomial(3i, i-1), 0 \u003c= k \u003c n).",
				"T(n,k) = 2^k*binomial(n-1,k)*A030981(n-k)."
			],
			"example": [
				"T(2,0)=1 (/\\); T(2,1)=2 (/_, _\\ ).",
				"Triangle begins:",
				"   1;",
				"   1,  2;",
				"   4,  4,  4;",
				"  11, 24, 12,  8;",
				"  41, 88, 96, 32, 16;"
			],
			"maple": [
				"T:=proc(n,k) if k\u003cn then 2^k*binomial(n-1,k)*sum((-1)^(n-k-i)*2^(n-k-i)*binomial(n-k,i)*binomial(3*i,i-1),i=1..n-k)/(n-k) else 0 fi end: for n from 1 to 10 do seq(T(n,k),k=0..n-1) od; # yields sequence in triangular form"
			],
			"mathematica": [
				"T[n_, k_] := If[k\u003cn, 2^k*Binomial[n-1, k]*Sum[(-1)^(n-k-i)*2^(n-k-i)* Binomial[n-k, i]*Binomial[3*i, i-1], {i, 1, n-k}]/(n-k), 0];",
				"Table[T[n, k], {n, 1, 10}, {k, 0, n-1}] // Flatten (* _Jean-François Alcover_, Jul 06 2018, from Maple *)"
			],
			"program": [
				"(PARI) T(n,k)={if(k\u003cn, 2^k*binomial(n-1, k)*sum(i=1, n-k, (-1)^(n-k-i)*2^(n-k-i)*binomial(n-k, i)*binomial(3*i, i-1))/(n-k))}",
				"for(n=1, 10, for(k=0, n-1, print1(T(n, k), \", \")); print); \\\\ _Andrew Howroyd_, Nov 06 2017"
			],
			"xref": [
				"Cf. A001764, A030981 (column 0)."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Emeric Deutsch_, Jan 17 2005",
			"references": 3,
			"revision": 19,
			"time": "2018-07-06T03:02:21-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}