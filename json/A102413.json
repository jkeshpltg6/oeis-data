{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A102413",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 102413,
			"data": "1,1,1,1,4,1,1,6,6,1,1,8,16,8,1,1,10,30,30,10,1,1,12,48,76,48,12,1,1,14,70,154,154,70,14,1,1,16,96,272,384,272,96,16,1,1,18,126,438,810,810,438,126,18,1,1,20,160,660,1520,2004,1520,660,160,20,1,1,22,198,946,2618,4334,4334,2618,946,198,22,1",
			"name": "Triangle read by rows: T(n,k) is the number of k-matchings in the n-sunlet graph (0 \u003c= k \u003c= n).",
			"comment": [
				"The n-sunlet graph is the corona C'(n) of the cycle graph C(n) and the complete graph K(1); in other words, C'(n) is the graph constructed from C(n) to which for each vertex v a new vertex v' and the edge vv' is added.",
				"Row n contains n+1 terms. Row sums yield A099425. T(n,k) = T(n,n-k).",
				"For n \u003e 2: same recurrence like A008288 and A128966. - _Reinhard Zumkeller_, Apr 15 2014"
			],
			"reference": [
				"J. L. Gross and J. Yellen, Handbook of Graph Theory, CRC Press, Boca Raton, 2004, p. 894.",
				"F. Harary, Graph Theory, Addison-Wesley, Reading, Mass., 1969, p. 167."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A102413/b102413.txt\"\u003eRows n = 0..125 of table, flattened\u003c/a\u003e",
				"Frédéric Bihan, Francisco Santos, Pierre-Jean Spaenlehauer, \u003ca href=\"https://arxiv.org/abs/1804.5683\"\u003eA Polyhedral Method for Sparse Systems with many Positive Solutions\u003c/a\u003e, arXiv:1804.05683 [math.CO], 2018.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Matching-GeneratingPolynomial.html\"\u003eMatching-Generating Polynomial\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/SunletGraph.html\"\u003eSunlet Graph\u003c/a\u003e"
			],
			"formula": [
				"G.f.: G(t,z) = (1 + t*z^2) / (1 - (1+t)*z - t*z^2).",
				"For n \u003e 2: T(n,k) = T(n-1,k-1) + T(n-1,k) + T(n-2,k-1), 0 \u003c k \u003c n. - _Reinhard Zumkeller_, Apr 15 2014 (corrected by _Andrew Woods_, Dec 08 2014)",
				"From _Peter Bala_, Jun 25 2015: (Start)",
				"The n-th row polynomial R(n,t) = [z^n] G(z,t)^n, where G(z,t) = 1/2*( 1 + (1 + t)*z + sqrt(1 + 2*(1 + t)*z + (1 + 6*t + t^2)*z^2) ).",
				"exp( Sum_{n \u003e= 1} R(n,t)*z^n/n ) = 1 + (1 + t)*z + (1 + 3*t + t^2)*z^2 + (1 + 5*t + 5*t^2 + t^3)*z^3 + ... is the o.g.f for A008288 read as a triangular array. (End)"
			],
			"example": [
				"T(3,2) = 6 because in the graph with vertex set {A,B,C,a,b,c} and edge set {AB,AC,BC,Aa,Bb,Cc} we have the following six 2-matchings: {Aa,BC}, {Bb,AC}, {Cc,AB}, {Aa,Bb}, {Aa,Cc} and {Bb,Cc}.",
				"The triangle starts:",
				"  1;",
				"  1, 1;",
				"  1, 4,  1;",
				"  1, 6,  6, 1;",
				"  1, 8, 16, 8, 1;",
				"From _Eric W. Weisstein_, Apr 03 2018: (Start)",
				"Rows as polynomials:",
				"  1",
				"  1 +    x,",
				"  1 +  4*x +    x^2,",
				"  1 +  6*x +  6*x^2 +    x^3,",
				"  1 +  8*x + 16*x^2 +  8*x^3 +    x^4,",
				"  1 + 10*x + 30*x^2 + 30*x^3 + 10*x^4 + x^5,",
				"  ... (End)"
			],
			"maple": [
				"G:=(1+t*z^2)/(1-(1+t)*z-t*z^2): Gser:=simplify(series(G,z=0,38)): P[0]:=1: for n from 1 to 11 do P[n]:=coeff(Gser,z^n) od:for n from 0 to 11 do seq(coeff(t*P[n],t^k),k=1..n+1) od; # yields sequence in triangular form"
			],
			"mathematica": [
				"CoefficientList[Table[2^-n ((1 + x - Sqrt[1 + x (6 + x)])^n + (1 + x + Sqrt[1 + x (6 + x)])^n), {n, 10}], x] // Flatten (* _Eric W. Weisstein_, Apr 03 2018 *)",
				"LinearRecurrence[{1 + x, x}, {1, 1 + x, 1 + 4 x + x^2}, 10] // Flatten (* _Eric W. Weisstein_, Apr 03 2018 *)",
				"Join[{1}, CoefficientList[CoefficientList[Series[(-1 - x - 2 x z)/(-1 + z + x z + x z^2), {z, 0, 10}], z], x]] // Flatten (* _Eric W. Weisstein_, Apr 03 2018 *)"
			],
			"program": [
				"(Haskell)",
				"a102413 n k = a102413_tabl !! n !! k",
				"a102413_row n = a102413_tabl !! n",
				"a102413_tabl = [1] : [1,1] : f [2] [1,1] where",
				"   f us vs = ws : f vs ws where",
				"             ws = zipWith3 (((+) .) . (+))",
				"                  ([0] ++ us ++ [0]) ([0] ++ vs) (vs ++ [0])",
				"-- _Reinhard Zumkeller_, Apr 15 2014"
			],
			"xref": [
				"Cf. A099425, A008288.",
				"Cf. A241023 (central terms)."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,5",
			"author": "_Emeric Deutsch_, Jan 07 2005",
			"ext": [
				"Row 0 in polynomials and Mathematica programs added by _Georg Fischer_, Apr 01 2019"
			],
			"references": 7,
			"revision": 32,
			"time": "2019-04-01T08:14:38-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}