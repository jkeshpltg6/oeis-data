{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A319862",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 319862,
			"data": "1,2,2,4,2,4,8,8,8,8,16,4,8,4,16,32,32,16,16,32,32,64,32,64,16,64,32,64,128,128,128,128,128,128,128,128,256,32,64,32,128,32,64,32,256,512,512,128,128,256,256,128,128,512,512,1024,512,1024,128,512,256,512,128,1024,512,1024",
			"name": "Triangle read by rows, 0 \u003c= k \u003c= n: T(n,k) is the denominator of the k-th Bernstein basis polynomial of degree n evaluated at the interval midpoint t = 1/2; numerator is A319861.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A319862/b319862.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e",
				"American Mathematical Society, \u003ca href=\"http://www.ams.org/publicoutreach/feature-column/fcarc-bezier\"\u003eFrom Bézier to Bernstein\u003c/a\u003e",
				"Rita T. Farouki, \u003ca href=\"https://doi.org/10.1016/j.cagd.2012.03.001\"\u003eThe Bernstein polynomial basis: A centennial retrospective\u003c/a\u003e, Computer Aided Geometric Design Vol. 29 (2012), 379-419.",
				"Ron Goldman, \u003ca href=\"https://doi.org/10.1016/B978-1-55860-354-7.X5000-4\"\u003ePyramid Algorithms. A Dynamic Programming Approach to Curves and Surfaces for Geometric Modeling\u003c/a\u003e, Morgan Kaufmann Publishers, 2002, Chap. 5.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/BernsteinPolynomial.html\"\u003eBernstein Polynomial\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Bernstein_polynomial\"\u003eBernstein polynomial\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = denominator of binomial(n,k)/2^n.",
				"T(n, k) = 2^n/A082907(n,k).",
				"A319862(n, k)/T(n, k) = binomial(n,k)/2^n.",
				"T(n, n-k) = T(n, k).",
				"T(n, 0) = 2^n.",
				"T(n, 1) = A075101(n)."
			],
			"example": [
				"Triangle begins:",
				"    1;",
				"    2,   2;",
				"    4,   2,   4;",
				"    8,   8,   8,   8;",
				"   16,   4,   8,   4,  16;",
				"   32,  32,  16,  16,  32,  32;",
				"   64,  32,  64,  16,  64,  32,  64;",
				"  128, 128, 128, 128, 128, 128, 128, 128;",
				"  256,  32,  64,  32, 128,  32,  64,  32, 256;",
				"  512, 512, 128, 128, 256, 256, 128, 128, 512, 512;",
				"  ..."
			],
			"maple": [
				"a:=(n,k)-\u003e2^n/gcd(binomial(n,k),2^n): seq(seq(a(n,k),k=0..n),n=0..11); # _Muniru A Asiru_, Sep 30 2018"
			],
			"mathematica": [
				"T[n_, k_] = 2^n/GCD[Binomial[n, k], 2^n];",
				"tabl[nn_] = TableForm[Table[T[n, k], {n, 0, nn}, {k, 0, n}]];"
			],
			"program": [
				"(Maxima)",
				"T(n, k) := 2^n/gcd(binomial(n, k), 2^n)$",
				"tabl(nn) := for n:0 thru nn do print(makelist(T(n, k), k, 0, n))$",
				"(GAP) Flat(List([0..11],n-\u003eList([0..n],k-\u003e2^n/Gcd(Binomial(n,k),2^n)))); # _Muniru A Asiru_, Sep 30 2018",
				"(Sage)",
				"def A319862(n,k): return denominator(binomial(n,k)/2^n)",
				"flatten([[A319862(n,k) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Jul 20 2021"
			],
			"xref": [
				"Cf. A007318, A082907, A128433, A128434, A319861."
			],
			"keyword": "nonn,easy,frac,tabl",
			"offset": "0,2",
			"author": "_Franck Maminirina Ramaharo_, Sep 29 2018",
			"references": 4,
			"revision": 20,
			"time": "2021-07-20T03:27:40-04:00",
			"created": "2018-10-06T05:18:57-04:00"
		}
	]
}