{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A347211",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 347211,
			"data": "1,-1,6,1,-54,36,-1,210,-264,216,1,-450,792,-1284,1296,-1,540,-1188,2940,-6204,7776,1,-252,792,-3000,10692,-29724,46656,-1,-252,0,960,-7128,37860,-140844,279936,1,540,0,168,792,-15600,129492,-657564,1679616",
			"name": "Square array T(n, k) read by ascending antidiagonals, T(n, k) = Sum_{j=0..n} (-1)^(n + j)*(6 - n + j)^k * binomial(12, n - j) if k \u003e 0 and (-1)^n otherwise. T(n, k) for 0 \u003c= n, k \u003c= 11.",
			"comment": [
				"T(n, k) are the numerators of the coefficients in the standardized probability distribution where the variable is the sum of 12 uniformly distributed random variables.",
				"Given that each of the twelve random variables in the sum is uniformly distributed in [-1/2..1/2], the expected value for the sum is 0, and its variance is 1, so that the distribution, whose graph is a bell curve, results \"naturally standardized\" [this only happens if we add 12 random variables, since the variance of the single variable is 1/12].",
				"The probability distribution is a piecewise-defined function; it consists of twelve 11th-degree polynomials for x in ranges [k-6, k-5], k=0..11.",
				"In each interval the distribution has the polynomial closed form",
				"P(n, x)= Sum_{k=0..11} R(n, k) * x^(11 - k), powers of x in decreasing order: x^11, x^10, x^9, ..., x, 1.",
				"The numerators of R(n, k) are the elements of the square array T(n, k); row n contains the numerators of the coefficients for the n-th interval [n-6, n-5].",
				"The denominators of R(n, k) are k!*(11 - k)! if k \u003e 0 and n!*(11 - n)! otherwise."
			],
			"reference": [
				"S. Brandt, \"Data Analysis: Statistical and Computational Methods for Scientists and Engineers\", Springer, 3rd edition (1998), 128-129.",
				"F. Martinelli, \"Somma di variabili aleatorie distribuite uniformemente: probabilità in forma chiusa\", Atti della Fondazione Giorgio Ronchi, Anno LXV n. 1 Gen-Feb 2010, 115-132. http://ronchi.isti.cnr.it/index.php/atti-della-fondazione"
			],
			"link": [
				"Franco Martinelli, \u003ca href=\"/A347211/b347211.txt\"\u003eAntidiagonals for n = 0..11, flattened.\u003c/a\u003e"
			],
			"formula": [
				"Simplified expressions: T(0, k) = 6^k; for k \u003e 0, T(1, k) = 6^k - 12*5^k.",
				"The square array is essentially symmetric:",
				"T(11 - n, k) = T(n, k) for odd k, T(11 - n, k) = -T(n, k) for even k."
			],
			"example": [
				"Upper left corner of the table (which has 12 rows and 12 columns):",
				"  n/k |  0        1        2        3        4        5        6",
				"  ==============================================================",
				"   0  |  1        6       36      216     1296     7776    46656",
				"   1  | -1      -54     -264    -1284    -6204   -29724  -140844",
				"   2  |  1      210      792     2940    10692    37860   129492",
				"   3  | -1     -450    -1188    -3000    -7128   -15600   -30888",
				"   4  |  1      540      792      960      792      240      792",
				"   5  | -1     -252        0      168        0     -552        0",
				"   6  |  1     -252        0      168        0     -552        0",
				".",
				"Let R(n, k) = T(n, k) / S(n, k), where S(n, k) = k!*(11 - k)! if k \u003e 0 and n!*(11 - n)! otherwise.",
				"T(3, 2) = -1188, the distribution in its 4th interval (n = 3) is a polynomial whose 9th-degree term coefficient R(3, 2) is -1188/725760 = -11/6720.",
				"For x in range [-3..-2]: P(3, x) = -(1/241920)*x^11 - (1/8064)*x^10 - (11/6720)*x^9 - (25/2016)*x^8 - (33/560)*x^7 - (13/72)*x^6 - (143/400)*x^5 - (239/504)*x^4 - (583/1120)*x^3 - (1619/3024)*x^2 - (781/5600)*x + 61297/166320.",
				"Row n = 5 for 0 \u003c= k \u003c= 11 is [-1, -252, 0, 168, 0, -552, 0, 5208, 0, -135912, 0, 15724248]. It stands for the polynomial P(5, x) = Sum_{k=0..11} R(5, k) * x^(11 - k). Explicitly this becomes P(5, x) = -(1/86400)*x^11 - (1/14400)*x^10 + (1/1440)*x^8 - (23/3600)*x^6 + (31/720)*x^4 - (809/4320)*x^2 + 655177/1663200 for x in range (-1,0).",
				"  The list of coefficients begins: 1/39916800, 1/604800, 1/20160, 1/1120, 3/280, 9/100, 27/50, 81/35, 243/35, 486/35, 2916/175, 17496/1925, -1/3628800, -1/67200, -11/30240, -107/20160, -517/10080, ...."
			],
			"maple": [
				"T := (n, k) -\u003e `if`(k = 0, (-1)^n, add((-1)^(n+j)*(6-n+j)^k*binomial(12, n-j), j=0..n)): for n from 0 to 10 do seq(T(n-k, k), k=0..n) od; for n from 0 to 11 do seq(T(11+n-k, k), k=n..11) od;"
			],
			"xref": [
				"Cf. A000400 (powers of 6)."
			],
			"keyword": "frac,sign,tabf,fini",
			"offset": "0,3",
			"author": "_Franco Martinelli_, Aug 23 2021",
			"references": 1,
			"revision": 52,
			"time": "2021-09-10T17:49:34-04:00",
			"created": "2021-09-02T09:03:08-04:00"
		}
	]
}