{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A198389",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 198389,
			"data": "5,10,13,15,17,20,25,26,25,29,30,34,37,35,41,39,40,50,45,52,51,50,61,53,55,65,58,60,65,65,65,68,75,74,85,70,82,78,73,75,80,85,85,85,89,91,101,87,100,90,113,95,104,97,102,100,111,122,106,105,123,109",
			"name": "Square root of second term of a triple of squares in arithmetic progression.",
			"comment": [
				"Apart from its initial 1, A001653 is a subsequence: for all n\u003e1 exists an m such that A198388(m)=1 and a(m)=A001653(n). [observed by _Zak Seidov_, _Reinhard Zumkeller_, Oct 25 2011]",
				"There is a connection to hypotenuses of Pythagorean triangles. See a comment for the primitive case on A198441 which applies here mutatis mutandis. - _Wolfdieter Lang_, May 23 2013"
			],
			"link": [
				"Ray Chandler, \u003ca href=\"/A198389/b198389.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Reinhard Zumkeller, \u003ca href=\"/A198384/a198384_2.txt\"\u003eTable of initial values\u003c/a\u003e",
				"Keith Conrad, \u003ca href=\"http://www.math.uconn.edu/~kconrad/blurbs/ugradnumthy/3squarearithprog.pdf\"\u003eArithmetic progressions of three squares\u003c/a\u003e"
			],
			"formula": [
				"A198385(n) = a(n)^2.",
				"A198440(n) = a(A198409(n))."
			],
			"example": [
				"Connection to Pythagorean triangle hypotenuses: a(20) = 10 because (in the notation of the Zumkeller link) (u,v,w) = 2*(1,5,7) and the Pythagorean triangle is 2*(x=(7-1)/2,y=(1+7)/2,5) = 2*(3,4,5) with hypotenuse 2*5 = 10. - _Wolfdieter Lang_, May 23 2013"
			],
			"mathematica": [
				"wmax = 1000;",
				"triples[w_] := Reap[Module[{u, v}, For[u = 1, u \u003c w, u++, If[IntegerQ[v = Sqrt[(u^2 + w^2)/2]], Sow[{u, v, w}]]]]][[2]];",
				"Flatten[DeleteCases[triples /@ Range[wmax], {}], 2][[All, 2]] (* _Jean-François Alcover_, Oct 20 2021 *)"
			],
			"program": [
				"(Haskell)",
				"a198389 n = a198389_list !! (n-1)",
				"a198389_list = map (\\(_,x,_) -\u003e x) ts where",
				"   ts = [(u,v,w) | w \u003c- [1..], v \u003c- [1..w-1], u \u003c- [1..v-1],",
				"                   w^2 - v^2 == v^2 - u^2]"
			],
			"xref": [
				"Cf. A198385, A198409, A198440."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Reinhard Zumkeller_, Oct 24 2011",
			"references": 5,
			"revision": 32,
			"time": "2021-10-20T09:21:02-04:00",
			"created": "2011-10-24T11:11:19-04:00"
		}
	]
}