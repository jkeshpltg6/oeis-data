{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A286941",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 286941,
			"data": "1,1,5,1,7,11,13,17,19,23,29,1,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97,101,103,107,109,113,121,127,131,137,139,143,149,151,157,163,167,169,173,179,181,187,191,193,197,199,209",
			"name": "Irregular triangle read by rows: the n-th row corresponds to the totatives of the n-th primorial, A002110(n).",
			"comment": [
				"Values in row n of a(n) are those of row n of A286942 complement those of row n of A279864.",
				"From _Michael De Vlieger_, May 18 2017: (Start)",
				"Numbers t \u003c p_n# such that gcd(t, p_n#) = 0, where p_n# = A002110(n).",
				"Numbers in the reduced residue system of A002110(n).",
				"A005867(n) = number of terms of a(n) in row n; local minimum of Euler's totient function.",
				"A048862(n) = number of primes in row n of a(n).",
				"A048863(n) = number of nonprimes in row n of a(n).",
				"Since 1 is coprime to all n, it delimits the rows of a(n).",
				"The prime A000040(n+1) is the second term in row n since it is the smallest prime coprime to A002110(n) by definition of primorial.",
				"The smallest composite in row n is A001248(n+1) = A000040(n+1)^2.",
				"The Kummer numbers A057588(n) = A002110(n) - 1 are the last terms of rows n, since (n - 1) is less than and coprime to all positive n. (End)"
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A286941/b286941.txt\"\u003eTable of n, a(n) for n = 1..6299\u003c/a\u003e (rows 1 \u003c= n \u003c= 6 flattened).",
				"Mathoverflow, \u003ca href=\"https://mathoverflow.net/questions/140104/lower-bound-for-eulers-totient-for-almost-all-integers\"\u003eLower bound for Euler's totient for almost all integers\u003c/a\u003e. - _Michael De Vlieger_, May 18 2017",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Totative.html\"\u003eTotative\u003c/a\u003e. - _Michael De Vlieger_, May 18 2017"
			],
			"example": [
				"The triangle starts",
				"1;",
				"1, 5;",
				"1, 7, 11, 13, 17, 19, 23, 29;",
				"1, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 121, 127, 131, 137, 139, 143, 149, 151, 157, 163, 167, 169, 173, 179, 181, 187, 191, 193, 197, 199, 209;"
			],
			"mathematica": [
				"Table[Function[P, Select[Range@ P, CoprimeQ[#, P] \u0026]]@ Product[Prime@ i, {i, n}], {n, 4}] // Flatten (* _Michael De Vlieger_, May 18 2017 *)"
			],
			"program": [
				"(PARI) row(n) = my(P=factorback(primes(n))); select(x-\u003e(gcd(x, P) == 1), [1..P]); \\\\ _Michel Marcus_, Jun 02 2020"
			],
			"xref": [
				"Cf. A002110, A005867, A048862, A057588, A279864, A286941, A286942, A309497, A038110, A058250, A329815.",
				"Cf. A335334 (row sums)."
			],
			"keyword": "nonn,tabf",
			"offset": "1,3",
			"author": "_Jamie Morken_ and _Michael De Vlieger_, May 16 2017",
			"ext": [
				"More terms from _Michael De Vlieger_, May 18 2017"
			],
			"references": 7,
			"revision": 43,
			"time": "2020-07-07T16:30:53-04:00",
			"created": "2017-05-17T22:44:59-04:00"
		}
	]
}