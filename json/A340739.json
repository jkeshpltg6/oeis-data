{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A340739",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 340739,
			"data": "4,3,2,3,35,2,3,34,3,4,2,32,5,2,28,5,26,3,2,3,9,2,3,4,3,25,2,18,5,2,4,8,5,3,2,3,19,2,3,12,3,17,2,4,4,2,15,6,5,3,2,3,13,2,3,5,3,6,2,10,6,2,5,7,4,3,2,3",
			"name": "The stopping time sequence for the 3x+1 function, restricted to its range and adjusted. Each term is the number of iterations of the function until it decreases.",
			"comment": [
				"The sequence is column c in the link funct.pdf with obvious adjustments.",
				"\"Adjusted\" means that, since for every four terms the first two are 1's they are omitted and then the sequence is relabeled. The 3x+1 function is defined:",
				"For x a positive integer. f(x) := 3x + 1 with all positive powers of 2 remove.Note 1 is a fixed point of f.",
				"The range of the 3x+1 function is two disjoint sets 6N+1 and 6N+5 for N nonnegative integers.",
				"See the link to paper ammprob-f4-1-2, for a proof for range of the 3x+1 function.",
				"Observations, Conjectures:",
				"The famous 3x+1 problem would be solved if and only if ALL stopping time values are finite.",
				"a(n)=2 iff Mod_{8}(n) is in {3, 6} a(n)=3 iff Mod_{16}(n) is in {7,9, 2,4} a(n)=4 iff Mod_{64}(n) is in {1,31,45,10,24,44}",
				"a(n)=5 iff Mod_{128}(n) is in {13,29,33,49,63,79,101,16,56,72,76,79,92,106,122}",
				"a(n)=6 iff Mod_{512}(n) is in {61,97,241,255,293,333,337,389,399,437,477,495,48,58,96,136,154,232,268,412,426,464,504,508}",
				"Pattern seems to be a(n)=c iff there exist k and sets A,B such that",
				"Mod_{2^k}(n) is in A union B, where |A|=|B| and A are odd and B are even numbers, where A is associated with 6N+1 and B with 6N+5.",
				"Conjecture: Ultimately every positive integer appears in the stopping time sequence. (verified up to 100, examples:  a(6802394)=160, a(31229269)=161) And each positive integer is in the sequence an infinite number of times."
			],
			"reference": [
				"Ultimate Challenge: the 3x+1 problem, J.C. Lagarias - editor, AMS 2010."
			],
			"link": [
				"Sam E. Speed, \u003ca href=\"/A340739/b340739.txt\"\u003eTable of n, a(n) for n = 1..8194\u003c/a\u003e",
				"Sam E. Speed, \u003ca href=\"/A340739/a340739.tex\"\u003eammprob-f4-1-2.tex, proof of range of 3x+1 function\u003c/a\u003e",
				"Sam E. Speed, \u003ca href=\"/A340739/a340739.mw.txt\"\u003eMaple 12 program cutoff.mw used to make b-file\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Collatz_conjecture\"\u003eCollatz conjecture\u003c/a\u003e",
				"\u003ca href=\"/index/3#3x1\"\u003eIndex entries for sequences related to 3x+1 (or Collatz) problem\u003c/a\u003e"
			],
			"formula": [
				"For n a positive integer,",
				"a(n) = Min_{e=1,2,...} f^e(x(n)) \u003c x(n), where f is the 3x+1 function defined above and",
				"x(n) = 6n+1 if n=1,3,5,.. (odd) and x(n) = 6n-1 if n=2,4,6,... (even).",
				"See original stopping time file, funct, before adjustments."
			],
			"maple": [
				"See links cutoff.mw."
			],
			"xref": [
				"Cf. A067745."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Sam E. Speed_, Jan 18 2021",
			"references": 1,
			"revision": 26,
			"time": "2021-03-17T15:56:32-04:00",
			"created": "2021-03-17T15:56:32-04:00"
		}
	]
}