{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A162299",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 162299,
			"data": "1,2,2,6,2,3,1,4,2,4,30,1,3,2,5,1,12,1,12,2,6,42,1,6,1,2,2,7,1,12,1,24,1,12,2,8,30,1,9,1,15,1,3,2,9,1,20,1,2,1,10,1,4,2,10,66,1,2,1,1,1,1,1,6,2,11,1,12,1,8,1,6,1,8,1,12,2,12,2730,1,3,1,10,1,7,1,6,1,1,2,13,1,420,1,12,1,20,1,28,1,60,1,12,2,14,6,1,90,1,6,1,10,1,18,1,30,1,6,2,15",
			"name": "Faulhaber's triangle: triangle T(k,y) read by rows, giving denominator of the coefficient [m^y] of the polynomial Sum_{x=1..m} x^(k-1).",
			"comment": [
				"There are many versions of Faulhaber's triangle: search the OEIS for his name. For example, A220862/A220963 is essentially the same as this triangle, except for an initial column of 0's. - _N. J. A. Sloane_, Jan 28 2017"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A162299/b162299.txt\"\u003eRows n = 0..140, flattened\u003c/a\u003e",
				"Mohammad Torabi-Dashti, \u003ca href=\"http://www.maa.org/programs/faculty-and-departments/classroom-capsules-and-notes/faulhaber-s-triangle\"\u003eFaulhaber’s Triangle\u003c/a\u003e, College Math. J., 42:2 (2011), 96-97.",
				"Mohammad Torabi-Dashti, \u003ca href=\"/A162298/a162298.pdf\"\u003eFaulhaber’s Triangle\u003c/a\u003e [Annotated scanned copy of preprint]",
				"Eric Weisstein's MathWorld, \u003ca href=\"http://mathworld.wolfram.com/PowerSum.html\"\u003ePower Sum\u003c/a\u003e"
			],
			"formula": [
				"Faulhaber's triangle of fractions H(n,k) (n \u003e= 0, 1 \u003c= k \u003c= n+1) is defined by: H(0,1)=1; for 2\u003c=k\u003c=n+1, H(n,k) = (n/k)*H(n-1,k-1) with H(n,1) = 1 - Sum_{i=2..n+1}H(n,i). - _N. J. A. Sloane_, Jan 28 2017",
				"Sum_{x=1..m} x^(k-1) = (Bernoulli(k,m+1)-Bernoulli(k))/k."
			],
			"example": [
				"The first few polynomials:",
				"    m;",
				"   m/2  + m^2/2;",
				"   m/6  + m^2/2 + m^3/3;",
				"    0   + m^2/4 + m^3/2 + m^4/4;",
				"  -m/30 +   0   + m^3/3 + m^4/2 + m^5/5;",
				"  ...",
				"Initial rows of Faulhaber's triangle of fractions H(n, k) (n \u003e= 0, 1 \u003c= k \u003c= n+1):",
				"    1;",
				"   1/2,  1/2;",
				"   1/6,  1/2,  1/3;",
				"    0,   1/4,  1/2,  1/4;",
				"  -1/30,  0,   1/3,  1/2,  1/5;",
				"    0,  -1/12,  0,   5/12, 1/2,  1/6;",
				"   1/42,  0,  -1/6,   0,   1/2,  1/2,  1/7;",
				"    0,   1/12,  0,  -7/24,  0,   7/12, 1/2,  1/8;",
				"  -1/30,  0,   2/9,   0,  -7/15,  0,   2/3,  1/2,  1/9;",
				"  ...",
				"The triangle starts in row k=1 with columns 1\u003c=y\u003c=k as",
				"     1",
				"     2   2",
				"     6   2  3",
				"     1   4  2  4",
				"    30   1  3  2  5",
				"     1  12  1 12  2  6",
				"    42   1  6  1  2  2  7",
				"     1  12  1 24  1 12  2  8",
				"    30   1  9  1 15  1  3  2  9",
				"     1  20  1  2  1 10  1  4  2 10",
				"    66   1  2  1  1  1  1  1  6  2 11",
				"     1  12  1  8  1  6  1  8  1 12  2 12",
				"  2730   1  3  1 10  1  7  1  6  1  1  2 13",
				"     1 420  1 12  1 20  1 28  1 60  1 12  2 14",
				"     6   1 90  1  6  1 10  1 18  1 30  1  6  2 15",
				"  ...",
				"Initial rows of triangle of fractions:",
				"    1;",
				"   1/2, 1/2;",
				"   1/6, 1/2,  1/3;",
				"    0,  1/4,  1/2,  1/4;",
				"  -1/30, 0,   1/3,  1/2,  1/5;",
				"    0, -1/12,  0,   5/12, 1/2,  1/6;",
				"   1/42, 0,  -1/6,   0,   1/2,  1/2,  1/7;",
				"    0,  1/12,  0,  -7/24,  0,   7/12, 1/2,  1/8;",
				"  -1/30, 0,   2/9,   0,  -7/15,  0,   2/3,  1/2,  1/9;",
				"  ..."
			],
			"maple": [
				"A162299 := proc(k,y) local gf,x; gf := sum(x^(k-1),x=1..m) ; coeftayl(gf,m=0,y) ; denom(%) ; end proc: # _R. J. Mathar_, Jan 24 2011",
				"# To produce Faulhaber's triangle of fractions H(n,k) (n \u003e= 0, 1 \u003c= k \u003c= n+1):",
				"H:=proc(n,k) option remember; local i;",
				"if n\u003c0 or k\u003en+1 then 0;",
				"elif n=0 then 1;",
				"elif k\u003e1 then (n/k)*H(n-1,k-1);",
				"else 1 - add(H(n,i),i=2..n+1); fi; end;",
				"for n from 0 to 10 do lprint([seq(H(n,k),k=1..n+1)]); od:",
				"for n from 0 to 12 do lprint([seq(numer(H(n,k)),k=1..n+1)]); od: # A162298",
				"for n from 0 to 12 do lprint([seq(denom(H(n,k)),k=1..n+1)]); od: # A162299 # _N. J. A. Sloane_, Jan 28 2017"
			],
			"xref": [
				"Cf. A000367, A162298 (numerators).",
				"See also A220962/A220963.",
				"Cf. A053382, A053383."
			],
			"keyword": "nonn,tabl,frac",
			"offset": "0,2",
			"author": "_Juri-Stepan Gerasimov_, Jun 30 2009 and Jul 02 2009",
			"ext": [
				"Offset set to 0 by _Alois P. Heinz_, Feb 19 2021"
			],
			"references": 7,
			"revision": 35,
			"time": "2021-02-19T12:03:24-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}