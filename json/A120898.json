{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A120898",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 120898,
			"data": "1,2,1,2,5,6,5,2,5,16,22,18,14,12,5,16,54,78,72,58,43,38,37,16,54,186,282,280,231,182,156,128,123,124,54,186,654,1030,1073,924,751,622,535,498,425,418,426,186,654,2338,3787,4100,3672,3048,2530,2190,1956,1766",
			"name": "Cascadence of 1+2x+x^2; a triangle, read by rows of 2n+1 terms, that retains its original form upon convolving each row with [1,2,1] and then letting excess terms spill over from each row into the initial positions of the next row such that only 2n+1 terms remain in row n for n\u003e=0.",
			"comment": [
				"In this case, the g.f. of column 0, H(x), satisfies: H(x) = H(x*G^2)*G/x where G satisfies: G = x*(1+2G+G^2), so that 1+G = g.f. of Catalan numbers (A000108). More generally, the cascadence of polynomial F(x) of degree d, F(0)=1, is a triangle with d*n+1 terms in row n where the g.f. H(x) of column 0 satisfies: H(x) = H(x*G^d)*G/x where G = x*F(G); thus G = series_reversion(x/F(x)), or, equivalently, [x^n] G = [x^n] x*F(x)^n/n for n\u003e=1.",
				"Further, the g.f. of the cascadence triangle for polynomial F(x) of degree d is given by: A(x,y) = ( x*H(x) - y*H(x*y^d) )/( x*F(y) - y ), where H(x) = G*H(x*G^d)/x and G = x*F(G). - _Paul D. Hanna_, Jul 17 2006"
			],
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A120898/b120898.txt\"\u003eTable of n, a(n) for n = 0..440; rows 0..20 of flattened triangle.\u003c/a\u003e"
			],
			"formula": [
				"G.f.: A(x,y) = ( x*H(x) - y*H(x*y^2) )/( x*F(y) - y ), where H(x) = G*H(x*G^2)/x, G = x*F(G), F(x)=1+2x+x^2. - _Paul D. Hanna_, Jul 17 2006"
			],
			"example": [
				"Triangle begins:",
				"1;",
				"2, 1, 2;",
				"5, 6, 5, 2, 5;",
				"16, 22, 18, 14, 12, 5, 16;",
				"54, 78, 72, 58, 43, 38, 37, 16, 54;",
				"186, 282, 280, 231, 182, 156, 128, 123, 124, 54, 186;",
				"654, 1030, 1073, 924, 751, 622, 535, 498, 425, 418, 426, 186, 654;",
				"2338, 3787, 4100, 3672, 3048, 2530, 2190, 1956, 1766, 1687, 1456, 1452, 1494, 654, 2338; ...",
				"Convolution of [1,2,1] with each row produces:",
				"[1,2,1]*[1] = [1,2,1];",
				"[1,2,1]*[2,1,2] = [2,5,6,5,2];",
				"[1,2,1]*[5,6,5,2,5] = [5,16,22,18,14,12,5];",
				"[1,2,1]*[16,22,18,14,12,5,16] = [16,54,78,72,58,43,38,37,16];",
				"These convoluted rows, when concatenated, yield the sequence:",
				"1,2,1, 2,5,6,5,2, 5,16,22,18,14,12,5, 16,54,78,72,58,43,38,37,16, ...",
				"which equals the concatenated rows of this original triangle:",
				"1, 2,1,2, 5,6,5,2,5, 16,22,18,14,12,5,16, 54,78,72,58,43,38,37,16,54,"
			],
			"program": [
				"(PARI) T(n,k)=if(2*n\u003ck || k\u003c0,0,if(n==0 \u0026\u0026 k==0,1,if(k==0,T(n-1,1)+2*T(n-1,0), if(k==2*n,T(n,0),T(n-1,k-1)+2*T(n-1,k)+T(n-1,k+1)))))",
				"for(n=0, 10, for(k=0, 2*n, print1(T(n, k), \", \")); print(\"\"))",
				"(PARI) /* Generated by the G.F.: */",
				"{T(n,k)=local(A,F=1+2*x+x^2,d=2,G=x,H=1+x,S=ceil(log(n+1)/log(d+1))); for(i=0,n,G=x*subst(F,x,G+x*O(x^n)));for(i=0,S,H=subst(H,x,x*G^d+x*O(x^n))*G/x); A=(x*H-y*subst(H,x,x*y^d +x*O(x^n)))/(x*subst(F,x,y)-y); polcoeff(polcoeff(A,n,x),k,y)}",
				"for(n=0, 10, for(k=0, 2*n, print1(T(n, k), \", \")); print()) \\\\ _Paul D. Hanna_, Jul 17 2006"
			],
			"xref": [
				"Cf. A120899 (column 0), A120901 (central terms), A120902 (row sums), A000108 (Catalan); variants: A092683, A092686, A120894."
			],
			"keyword": "nonn,tabf",
			"offset": "0,2",
			"author": "_Paul D. Hanna_, Jul 14 2006",
			"references": 10,
			"revision": 15,
			"time": "2017-06-13T23:47:38-04:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}