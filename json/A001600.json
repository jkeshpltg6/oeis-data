{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001600",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1600,
			"id": "M0609 N0220",
			"data": "1,2,3,5,6,5,8,9,11,10,7,15,15,14,17,24,24,21,13,19,27,25,29,26,44,44,29,46,39,46,27,42,47,47,54,35,41,60,51,37,48,45,49,50,49,53,77,86,86,51,96,75,70,80,99,110,81,84,13,102,82,96,114,53,108,115,105,116,91,85,105",
			"name": "Harmonic means of divisors of harmonic numbers.",
			"comment": [
				"Values of n*tau(n)/sigma(n) corresponding to terms of A001599, where tau(n) (A000005) is the number of divisors of n and sigma(n) is the sum of the divisors of n (A000203).",
				"Kanold (1957) proved that each term appears only a finite number of times. - _Amiram Eldar_, Jun 01 2020"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"R. J. Mathar, \u003ca href=\"/A001600/b001600.txt\"\u003eTable of n, a(n) for n = 1..937\u003c/a\u003e, extending the former b-file of T. D. Noe.",
				"Marco Abrate, Stefano Barbero, Umberto Cerruti, Nadir Murru, \u003ca href=\"http://arxiv.org/abs/1601.03081\"\u003eThe Biharmonic mean\u003c/a\u003e, arXiv:1601.03081 [math.NT], 2016.",
				"G. L. Cohen, \u003ca href=\"/A007340/a007340.pdf\"\u003eEmail to N. J. A. Sloane, Apr. 1994\u003c/a\u003e",
				"M. Garcia, \u003ca href=\"http://www.jstor.org/stable/2307792\"\u003eOn numbers with integral harmonic mean\u003c/a\u003e, Amer. Math. Monthly 61, (1954). 89-96.",
				"Takeshi Goto, \u003ca href=\"http://www.ma.noda.tus.ac.jp/u/tg/html/harmonic-e.html#mark1\"\u003eAll harmonic numbers less than 10^14\u003c/a\u003e",
				"Takeshi Goto, \u003ca href=\"http://www.ma.noda.tus.ac.jp/u/tg/files/list4\"\u003eTable of a(n) for n=1..937\u003c/a\u003e",
				"Hans-Joachim Kanold , \u003ca href=\"http://dx.doi.org/10.1007/BF01342887\"\u003eÜber das harmonische Mittel der Teiler einer natürlichen Zahl\u003c/a\u003e, Math. Ann., Vol. 133 (1957), pp. 371-374.",
				"O. Ore, \u003ca href=\"http://www.jstor.org/stable/2305616\"\u003eOn the averages of the divisors of a number\u003c/a\u003e, Amer. Math. Monthly, 55 (1948), 615-619.",
				"O. Ore, \u003ca href=\"/A001599/a001599.pdf\"\u003eOn the averages of the divisors of a number\u003c/a\u003e (annotated scanned copy)"
			],
			"mathematica": [
				"A001600 = Reap[Do[tau = DivisorSigma[0, n]; sigma = DivisorSigma[1, n]; h = n*tau/sigma; If[IntegerQ[h], Print[h]; Sow[h]], {n, 1, 90000000}]][[2, 1]](* _Jean-François Alcover_, May 11 2012 *)"
			],
			"program": [
				"(Haskell)",
				"a001600 n = a001600_list !! (n-1)",
				"a001600_list =",
				"   [numerator m | x \u003c- [1..], let m = hm x, denominator m == 1] where",
				"   hm x = genericLength divs * recip (sum $ map recip divs)",
				"          where divs = map fromIntegral $ a027750_row x",
				"-- _Reinhard Zumkeller_, Apr 01 2014",
				"(PARI) lista(nn) = for (n=1, nn, if (denominator(q=n*numdiv(n)/sigma(n)) == 1, print1(q, \", \"))); \\\\ _Michel Marcus_, Jan 13 2016"
			],
			"xref": [
				"Cf. A001599, A090240 (sorted values)."
			],
			"keyword": "nonn,nice,easy",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Matthew Conroy_, Jan 15 2006"
			],
			"references": 14,
			"revision": 47,
			"time": "2021-12-19T10:02:25-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}