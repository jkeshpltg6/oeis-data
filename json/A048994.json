{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A048994",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 48994,
			"data": "1,0,1,0,-1,1,0,2,-3,1,0,-6,11,-6,1,0,24,-50,35,-10,1,0,-120,274,-225,85,-15,1,0,720,-1764,1624,-735,175,-21,1,0,-5040,13068,-13132,6769,-1960,322,-28,1,0,40320,-109584,118124,-67284,22449,-4536,546,-36,1,0,-362880,1026576",
			"name": "Triangle of Stirling numbers of first kind, s(n,k), n \u003e= 0, 0 \u003c= k \u003c= n.",
			"comment": [
				"The unsigned numbers are also called Stirling cycle numbers: |s(n,k)| = number of permutations of n objects with exactly k cycles.",
				"Mirror image of the triangle A054654. - _Philippe Deléham_, Dec 30 2006",
				"Also the triangle gives coefficients T(n,k) of x^k in the expansion of C(x,n) = (a(k)*x^k)/n!. -  _Mokhtar Mohamed_, Dec 04 2012",
				"From _Wolfdieter Lang_, Nov 14 2018: (Start)",
				"This is the Sheffer triangle of Jabotinsky type (1, log(1 + x)). See the e.g.f. of the triangle below.",
				"This is the inverse Sheffer triangle of the Stirling2 Sheffer triangle A008275.",
				"The a-sequence of this Sheffer triangle (see a W. Lang link in A006232)",
				"is from the e.g.f. A(x) = x/(exp(x) -1) a(n) = Bernoulli(n) = A027641(n)/A027642(n), for n \u003e= 0. The z-sequence vanishes.",
				"The Boas-Buck sequence for the recurrences of columns has o.g.f. B(x) = Sum_{n\u003e=0} b(n)*x^n = 1/((1 + x)*log(1 + x)) - 1/x. b(n) = (-1)^(n+1)*A002208(n+1)/A002209(n+1), b = {-1/2, 5/12, -3/8, 251/720, -95/288, 19087/60480,...}. For the Boas-Buck recurrence of Riordan and Sheffer triangles see the Aug 10 2017 remark in A046521, adapted to the Sheffer case, also for two references. See the recurrence and example below. - _Wolfdieter Lang_, Nov 14 2018"
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), p. 833.",
				"L. Comtet, Advanced Combinatorics, Reidel, 1974; Chapter V, also p. 310.",
				"J. H. Conway and R. K. Guy, The Book of Numbers, Copernicus Press, NY, 1996, p. 93.",
				"F. N. David, M. G. Kendall and D. E. Barton, Symmetric Function and Allied Tables, Cambridge, 1966, p. 226.",
				"R. L. Graham, D. E. Knuth and O. Patashnik, Concrete Mathematics. Addison-Wesley, Reading, MA, 1990, p. 245.",
				"J. Riordan, An Introduction to Combinatorial Analysis, p. 48."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A048994/b048994.txt\"\u003eRows n=0..100 of triangle, flattened\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"Paul Barry, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Barry1/barry263.html\"\u003eGeneralized Stirling Numbers, Exponential Riordan Arrays, and Toda Chain Equations\u003c/a\u003e, Journal of Integer Sequences, 17 (2014), #14.2.3.",
				"R. M. Dickau, \u003ca href=\"http://mathforum.org/advanced/robertd/stirling1.html\"\u003eStirling numbers of the first kind\u003c/a\u003e. [Illustrates the unsigned Stirling cycle numbers A132393.]",
				"Askar Dzhumadil'daev and Damir Yeliussizov, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v22i4p10\"\u003eWalks, partitions, and normal ordering\u003c/a\u003e, Electronic Journal of Combinatorics, 22(4) (2015), #P4.10.",
				"Bill Gosper, \u003ca href=\"/A008275/a008275.png\"\u003eColored illustrations of triangle of Stirling numbers of first kind read mod 2, 3, 4, 5, 6, 7\u003c/a\u003e.",
				"Gergő Nemes, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL14/Nemes/nemes4.html\"\u003eAn Asymptotic Expansion for the Bernoulli Numbers of the Second Kind\u003c/a\u003e, J. Int. Seq. 14 (2011), #11.4.8.",
				"A. Hennessy and P. Barry, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL14/Barry6/barry161.html\"\u003eGeneralized Stirling Numbers, Exponential Riordan Arrays, and Orthogonal Polynomials\u003c/a\u003e, J. Int. Seq. 14 (2011), #11.8.2 (A-number typo A048894).",
				"NIST Digital Library of Mathematical Functions, \u003ca href=\"http://dlmf.nist.gov/26.8\"\u003eStirling Numbers\u003c/a\u003e",
				"Ken Ono, Larry Rolen, and Florian Sprung, \u003ca href=\"http://arxiv.org/abs/1602.00752\"\u003eZeta-Polynomials for modular form periods\u003c/a\u003e, p. 6, arXiv:1602.00752 [math.NT], 2016.",
				"Ricardo A. Podestá, \u003ca href=\"http://arxiv.org/abs/1603.09156\"\u003eNew identities for binary Krawtchouk polynomials, binomial coefficients and Catalan numbers\u003c/a\u003e, arXiv preprint arXiv:1603.09156 [math.CO], 2016.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Stirling_numbers_and_exponential_generating_functions\"\u003eStirling numbers and exponential generating functions\u003c/a\u003e."
			],
			"formula": [
				"s(n, k) = A008275(n,k) for n \u003e= 1, k = 1..n; column k = 0 is {1, repeat(0)}.",
				"s(n, k) = s(n-1, k-1) - (n-1)*s(n-1, k), n, k \u003e= 1; s(n, 0) = s(0, k) = 0; s(0, 0) = 1.",
				"The unsigned numbers a(n, k)=|s(n, k)| satisfy a(n, k)=a(n-1, k-1)+(n-1)*a(n-1, k), n, k \u003e= 1; a(n, 0) = a(0, k) = 0; a(0, 0) = 1.",
				"Triangle (signed) = [0, -1, -1, -2, -2, -3, -3, -4, -4, ...] DELTA [1, 0, 1, 0, 1, 0, 1, 0, ...]; Triangle(unsigned) = [0, 1, 1, 2, 2, 3, 3, 4, 4, ...] DELTA [1, 0, 1, 0, 1, 0, 1, 0, ...]; where DELTA is Deléham's operator defined in A084938.",
				"Sum_{k=0..n} (-m)^(n-k)*s(n, k) = A000142(n), A001147(n), A007559(n), A007696(n), ... for m = 1, 2, 3, 4, ... .- _Philippe Deléham_, Oct 29 2005",
				"A008275*A007318 as infinite lower triangular matrices. - _Gerald McGarvey_, Aug 20 2009",
				"T(n,k) = n!*[x^k]([t^n]exp(x*log(1+t))). - _Peter Luschny_, Dec 30 2010, updated Jun 07 2020",
				"From _Wolfdieter Lang_, Nov 14 2018: (Start)",
				"Recurrence from the Sheffer a-sequence (see a comment above): s(n, k) = (n/k)*Sum_{j=0..n-k} binomial(k-1+j, j)*Bernoulli(j)*s(n-1, k-1+j), for n \u003e= 1 and k \u003e= 1, with s(n, 0) = 0 if n \u003e= 1, and s(0,0) = 1.",
				"Boas-Buck type recurrence for column k: s(n, k) = (n!*k/(n - k))*Sum_{j=k..n-1} b(n-1-j)*s(j, k)/j!, for n \u003e= 1 and k = 0..n-1, with input s(n, n) = 1. For sequence b see the Boas-Buck comment above. (End)"
			],
			"example": [
				"Triangle begins:",
				"n\\k 0     1       2       3      4      5      6    7    8   9 ...",
				"0   1",
				"1   0     1",
				"2   0    -1       1",
				"3   0     2      -3       1",
				"4   0    -6      11      -6      1",
				"5   0    24     -50      35    -10      1",
				"6   0  -120     274    -225     85    -15      1",
				"7   0   720   -1764    1624   -735    175    -21    1",
				"8   0 -5040   13068  -13132   6769  -1960    322  -28    1",
				"9   0 40320 -109584  118124 -67284  22449  -4536  546  -36   1",
				"... - _Wolfdieter Lang_, Aug 22 2012",
				"------------------------------------------------------------------",
				"From _Wolfdieter Lang_, Nov 14 2018: (Start)",
				"Recurrence: s(5,2)= s(4, 1) - 4*s(4, 2) = -6 - 4*11 = -50.",
				"Recurrence from the a- and z-sequences: s(6, 3) = 2*(1*1*(-50) + 3*(-1/2)*35 + 6*(1/6)*(-10) + 10*0*1) = -225.",
				"Boas-Buck recurrence for column k = 3, with b = {-1/2, 5/12, -3/8, ...}:",
				"s(6, 3) = 6!*((-3/8)*1/3! + (5/12)*(-6)/4! + (-1/2)*35/5!) = -225. (End)"
			],
			"maple": [
				"A048994:= proc(n,k) combinat[stirling1](n,k) end: # _R. J. Mathar_, Feb 23 2009",
				"seq(print(seq(coeff(expand(k!*binomial(x,k)),x,i),i=0..k)),k=0..9); # _Peter Luschny_, Jul 13 2009",
				"A048994_row := proc(n) local k; seq(coeff(expand(pochhammer(x-n+1,n)), x,k), k=0..n) end: # _Peter Luschny_, Dec 30 2010"
			],
			"mathematica": [
				"Table[StirlingS1[n, m], {n, 0, 9}, {m, 0, n}] (* _Peter Luschny_, Dec 30 2010 *)"
			],
			"program": [
				"(PARI) a(n,k) = if(k\u003c0 || k\u003en,0, if(n==0,1,(n-1)*a(n-1,k)+a(n-1,k-1)))",
				"(Maxima) create_list(stirling1(n,k),n,0,12,k,0,n); /* _Emanuele Munarini_, Mar 11 2011 */",
				"(Haskell)",
				"a048994 n k = a048994_tabl !! n !! k",
				"a048994_row n = a048994_tabl !! n",
				"a048994_tabl = map fst $ iterate (\\(row, i) -\u003e",
				"(zipWith (-) ([0] ++ row) $ map (* i) (row ++ [0]), i + 1)) ([1], 0)",
				"-- _Reinhard Zumkeller_, Mar 18 2013",
				"(PARI) trg(nn)=for (n=0, nn-1, for (k=0, n, print1(stirling(n,k,1), \", \");); print();); \\\\ _Michel Marcus_, Jan 19 2015"
			],
			"xref": [
				"See especially A008275 which is the main entry for this triangle. A132393 is an unsigned version, and A008276 is another version.",
				"Cf. A008277, A039814-A039817, A048993, A084938.",
				"A000142(n) = Sum_{k=0..n} |s(n, k)| for n \u003e= 0.",
				"Row sums give A019590(n+1).",
				"Cf. A002209, A027641/A027642."
			],
			"keyword": "sign,tabl,nice",
			"offset": "0,8",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Offset corrected by _R. J. Mathar_, Feb 23 2009",
				"Formula corrected by _Philippe Deléham_, Sep 10 2009"
			],
			"references": 195,
			"revision": 109,
			"time": "2021-04-05T09:20:30-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}