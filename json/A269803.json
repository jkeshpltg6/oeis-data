{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A269803",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 269803,
			"data": "1,5,13,37,99,265,701,1849,4861,12761,33463,87697,229737,601693,1575629,4125661,10802107,28281881,74045509,193857841,507533181,1328750065,3478730543,9107463457,23843695249,62423679605,163427436301,427858779349,1120149144531",
			"name": "a(n) = F(n+1)*F(n+2) - F(n), where F = A000045 (Fibonacci numbers).",
			"comment": [
				"NI(F(n+1)/a(n)) = (n,n,n,n,n,...), where NI(x) denotes the r-nested-interval sequence of x, and r = (1/1, 1/2, 1/3, 1/5, 1/8, ...), the reciprocals of Fibonacci numbers.  Definitions follow.  Suppose that r = (r(n)) is a sequence satisfying (i) 1 = r(1) \u003e r(2) \u003e r(3) \u003e ... and (ii) r(n) -\u003e 0.  For x in (0,1], let n(1) be the index n such that r(n+1) , x \u003c= r(n), and let L(1) = r(n(1))-r(n(1)+1).  Let n(2) be the index n such that r(n(1)+1) \u003c x \u003c= r(n(1)+1) + L(1)r(n), and let L(2) = (r(n(2))-r(r(n)+1)L(1).  Continue inductively to obtain the sequence (n(1), n(2), n(3), ... ), the r-nested interval sequence of x.",
				"Conversely, given a sequence s= (n(1),n(2),n(3),...) of positive integers, the number x having satisfying NI(x) = s, is the sum of left-endpoints of nested intervals (r(n(k)+1), r(n(k))]; i.e., x = sum{L(k)r(n(k+1)+1), k \u003e=1}, where L(0) = 1.  Thus, for r = (1/F(n+1)), the number F(n+1)F(n+2)/a(n) is the only x for which NI(x) = (n,n,n,...)."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A269803/b269803.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,1,-5,-1,1)."
			],
			"formula": [
				"a(n) = F(n+1)F(n+2)-F(n), F = A000045 (Fibonacci numbers).",
				"a(n) = 3*a(n-1) + a(n-2) - 5*a(n-3) - a(n-4) + a(n-5).",
				"G.f.: x*(1+2*x-3*x^2-2*x^3+x^4) / ((1+x)*(1-3*x+x^2)*(1-x-x^2)). - _Colin Barker_, Mar 06 2016"
			],
			"mathematica": [
				"f[n_] := Fibonacci[n]; u = Table[f[n + 1] f[n + 2] - f[n], {n, 1, 40}]",
				"LinearRecurrence[{3,1,-5,-1,1},{1,5,13,37,99},40] (* _Harvey P. Dale_, Jul 27 2021 *)"
			],
			"program": [
				"(PARI) a(n) = fibonacci(n+1)*fibonacci(n+2) - fibonacci(n); \\\\ _Altug Alkan_, Mar 06 2016",
				"(MAGMA) [Fibonacci(n+1)*Fibonacci(n+2) - Fibonacci(n): n in [1..30]]; // _Vincenzo Librandi_, Mar 06 2016",
				"(PARI) Vec(x*(1+2*x-3*x^2-2*x^3+x^4)/((1+x)*(1-3*x+x^2)*(1-x-x^2)) + O(x^50)) \\\\ _Colin Barker_, Mar 06 2016"
			],
			"xref": [
				"Cf. A000045, A269802."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Mar 05 2016",
			"references": 3,
			"revision": 17,
			"time": "2021-07-27T10:22:22-04:00",
			"created": "2016-03-06T12:59:40-05:00"
		}
	]
}