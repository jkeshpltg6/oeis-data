{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A014707",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 14707,
			"data": "0,0,1,0,0,1,1,0,0,0,1,1,0,1,1,0,0,0,1,0,0,1,1,1,0,0,1,1,0,1,1,0,0,0,1,0,0,1,1,0,0,0,1,1,0,1,1,1,0,0,1,0,0,1,1,1,0,0,1,1,0,1,1,0,0,0,1,0,0,1,1,0,0,0,1,1,0,1,1,0,0,0,1,0,0,1,1,1,0,0,1,1,0",
			"name": "a(4n)=0, a(4n+2)=1, a(2n+1)=a(n).",
			"comment": [
				"The regular paper-folding (or dragon curve) sequence.",
				"It appears that the sequence of run lengths is A088431. - _Dimitri Hendriks_, May 06 2010",
				"Runs of three consecutive ones appear around positions n = 22, 46, 54, 86, 94, 118, 150, 174, 182,..., or for n of the form 2^(k+3)*(4*t+3)-2, k\u003e=0, t\u003e=0. - _Vladimir Shevelev_, Mar 19 2011",
				"a(A091072(n)+1) = 0; a(A091067(n)+1) = 1. - _Reinhard Zumkeller_, Sep 28 2011"
			],
			"reference": [
				"G. Melancon, Factorizing infinite words using Maple, MapleTech journal, vol. 4, no. 1, 1997, pp. 34-42, esp. p. 36."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A014707/b014707.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"J.-P. Allouche, M. Mendes France, A. Lubiw, A.J. van der Poorten and J. Shallit, \u003ca href=\"http://www.math.jussieu.fr/~allouche/bibliorecente.html\"\u003eConvergents of folded continued fractions\u003c/a\u003e",
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/2104.05593\"\u003eOn the Gap-sum and Gap-product Sequences of Integer Sequences\u003c/a\u003e, arXiv:2104.05593 [math.CO], 2021.",
				"G. J. Endrullis, D. Hendriks and J. W. Klop, \u003ca href=\"http://www.cs.vu.nl/~diem/publication/pdf/degrees.pdf\"\u003eDegrees of streams\u003c/a\u003e",
				"J.-Y. Kao et al., \u003ca href=\"http://arXiv.org/abs/math.CO/0608607\"\u003eWords avoiding repetitions in arithmetic progressions\u003c/a\u003e",
				"G. Melancon, \u003ca href=\"http://www.lirmm.fr/~melancon/\"\u003eHome page\u003c/a\u003e",
				"G. Melancon, \u003ca href=\"http://dx.doi.org/10.1007/3-540-60922-9_13\"\u003eLyndon factorization of infinite words\u003c/a\u003e, STACS 96 (Grenoble, 1996), 147-154, Lecture Notes in Comput. Sci., 1046, Springer, Berlin, 1996. Math. Rev. 98h:68188.",
				"\u003ca href=\"/index/Fo#fold\"\u003eIndex entries for sequences obtained by enumerating foldings\u003c/a\u003e"
			],
			"formula": [
				"a(n) = (1-jacobi(-1,n))/2 (cf. A034947). - _N. J. A. Sloane_, Jul 27 2012",
				"Set a=0, b=1, S(0)=a, S(n+1) = S(n)aF(S(n)), where F(x) reverses x and then interchanges a and b; sequence is limit S(infinity).",
				"a((2*n+1)*2^p-1) = n mod 2, p \u003e= 0. - _Johannes W. Meijer_, Jan 28 2013"
			],
			"maple": [
				"nmax:=92: for p from 0 to ceil(simplify(log[2](nmax))) do for n from 0 to ceil(nmax/(p+2))+1 do a((2*n+1)*2^p-1) := n mod 2 od: od: seq(a(n), n=0..nmax); # _Johannes W. Meijer_, Jan 28 2013"
			],
			"mathematica": [
				"a[n_ /; Mod[n, 4] == 0] = 0; a[n_ /; Mod[n, 4] == 2] = 1; a[n_ /; Mod[n, 2] == 1] := a[n] = a[(n - 1)/2]; Table[a[n],{n,0,92}] (* _Jean-François Alcover_, May 17 2011 *)"
			],
			"program": [
				"(Haskell)",
				"a014707 n = a014707_list !! n",
				"a014707_list = f 0 $ cycle [0,0,1,0] where",
				"   f i (x:_:xs) = x : a014707 i : f (i+1) xs",
				"-- _Reinhard Zumkeller_, Sep 28 2011",
				"(Python)",
				"def A014707(n):",
				"    s = bin(n+1)[2:]",
				"    m = len(s)",
				"    i = s[::-1].find('1')",
				"    return int(s[m-i-2]) if m-i-2 \u003e= 0 else 0 # _Chai Wah Wu_, Apr 08 2021",
				"(PARI) a(n)=n+=1;my(h=bitand(n,-n));n=bitand(n,h\u003c\u003c1);n!=0; \\\\ _Joerg Arndt_, Apr 09 2021"
			],
			"xref": [
				"Equals 1 - A014577, which see for further references. Also a(n) = A038189(n+1).",
				"The following are all essentially the same sequence: A014577, A014707, A014709, A014710, A034947, A038189, A082410.",
				"Cf. A220466"
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from Scott C. Lindhurst (ScottL(AT)alumni.princeton.edu)"
			],
			"references": 20,
			"revision": 66,
			"time": "2021-07-08T23:22:16-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}