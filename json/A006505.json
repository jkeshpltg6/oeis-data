{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006505",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6505,
			"id": "M4789",
			"data": "1,0,0,1,1,1,11,36,92,491,2557,11353,60105,362506,2169246,13580815,91927435,650078097,4762023647,36508923530,292117087090,2424048335917,20847410586719,185754044235873,1711253808769653,16272637428430152",
			"name": "Number of partitions of an n-set into boxes of size \u003e2.",
			"reference": [
				"J. Riordan, A budget of rhyme scheme counts, pp. 455 - 465 of Second International Conference on Combinatorial Mathematics, New York, April 4-7, 1978. Edited by Allan Gewirtz and Louis V. Quintas. Annals New York Academy of Sciences, 319, 1979.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A006505/b006505.txt\"\u003eTable of n, a(n) for n = 0..579\u003c/a\u003e (terms 0..250 from Alois P. Heinz)",
				"E. A. Enneking and J. C. Ahuja, \u003ca href=\"http://www.fq.math.ca/Scanned/14-1/enneking.pdf\"\u003eGeneralized Bell numbers\u003c/a\u003e, Fib. Quart., 14 (1976), 67-73.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=102\"\u003eEncyclopedia of Combinatorial Structures 102\u003c/a\u003e",
				"Vladimir Victorovich Kruchinin, \u003ca href=\"http://arxiv.org/abs/1009.2565\"\u003eComposition of ordinary generating functions\u003c/a\u003e, arXiv:1009.2565 [math.CO], 2010.",
				"I. Mezo, \u003ca href=\"http://arxiv.org/abs/1308.1637\"\u003ePeriodicity of the last digits of some combinatorial sequences\u003c/a\u003e, arXiv preprint arXiv:1308.1637 [math.CO], 2013.",
				"J. Riordan, \u003ca href=\"/A005000/a005000.pdf\"\u003eCached copy of paper\u003c/a\u003e [With permission]"
			],
			"formula": [
				"E.g.f.: exp ( exp x - 1 - x - (1/2)*x^2 ).",
				"a(n) = Sum_{k=1..[n/3]} A059022(n,k), n\u003e=3. - _R. J. Mathar_, Nov 08 2008",
				"a(n) = n! * sum(m=1..n, sum(k=0..m, k!*(-1)^(m-k) *binomial(m,k) *sum(i=0..n-m, stirling2(i+k,k) *binomial(m-k,n-m-i) *2^(-n+m+i)/ (i+k)!))/m!); a(0)=1. - _Vladimir Kruchinin_, Feb 01 2011",
				"Define polynomials g_n by g_0=1, g_1=g_2=0, g_3=g_4=g_5=x; g(n) = x*Sum_{i=0..n-3} binomial(n-1,i)*g_i; then a(n) = g_n(1). [Riordan]"
			],
			"maple": [
				"Copy ZL := [ B,{B=Set(Set(Z, card\u003e=3))}, labeled ]: [seq(combstruct[count](ZL, size=n), n=0..25)]; # _Zerinvary Lajos_, Mar 13 2007",
				"G:={P=Set(Set(Atom,card\u003e=3))}:combstruct[gfsolve](G,unlabeled,x):seq(combstruct[count]([P,G,labeled],size=i),i=0..25); # _Zerinvary Lajos_, Dec 16 2007",
				"g:=proc(n) option remember; if n=0 then RETURN(1); fi; if n\u003c=2 then RETURN(0); fi; if n\u003c=5 then RETURN(x); fi; expand(x*add(binomial(n-1,i)*g(i),i=0..n-3)); end; [seq(subs(x=1,g(n)),n=0..60)]; # _N. J. A. Sloane_, Jul 20 2011"
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 0, 0, n! SeriesCoefficient[ Exp[ Exp @ x - 1 - x - x^2 / 2], {x, 0, n}]] (* _Michael Somos_, Jul 20 2011 *)",
				"a[0] = 1; a[n_] := n!*Sum[Sum[k!*(-1)^(m-k)*Binomial[m, k]*Sum[StirlingS2[i+k, k]* Binomial[m-k, n-m-i]*2^(-n+m+i)/(i+k)!, {i, 0, n-m}], {k, 0, m}]/m!, {m, 1, n}]; Table[a[n], {n, 0, 25}] (* _Jean-François Alcover_, Apr 03 2015, after _Vladimir Kruchinin_ *)",
				"Table[Sum[(-1)^j * Binomial[n, j] * BellB[n-j] * 2^((j-1)/2) * HypergeometricU[(1 - j)/2, 3/2, 1/2], {j, 0, n}], {n, 0, 25}] (* _Vaclav Kotesovec_, Feb 09 2020 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, n! * polcoeff( exp( exp( x + x * O(x^n)) - 1 - x - x^2 / 2), n))} /* _Michael Somos_, Jul 20 2011 */"
			],
			"xref": [
				"Column k=2 of A293024.",
				"Cf. A000110, A000296, A057814, A057837.",
				"Cf. A293038."
			],
			"keyword": "nonn,easy",
			"offset": "0,7",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Christian G. Bower_, Nov 09 2000",
				"Edited by _N. J. A. Sloane_, Jul 20 2011"
			],
			"references": 13,
			"revision": 64,
			"time": "2020-02-09T06:21:09-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}