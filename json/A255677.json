{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A255677",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 255677,
			"data": "5,30,8458,18,252,25,1407,476,9098,108,1814,1868,153,1005,67,26532,1592,200,963,99,833,1356,3869,981,531,127,4961,366,1192,1873,41308,409,21756,194664,180,27071,7433,160179,2076,544,211,10639,19571,33483,603,68380,1517,47529,35923",
			"name": "Least integer k \u003e 1 such that pi(k)^2 + pi(k*n)^2 is a square, where pi(.) is the prime-counting function given by A000720.",
			"comment": [
				"Conjecture: Each positive rational number r \u003c 1 can be written as m/n with 1 \u003c m \u003c n such that pi(m)^2 + pi(n)^2 is a square. Also, any rational number r \u003e 1 can be written as m/n with m \u003e n \u003e 1 such that pi(m)^2 - pi(n)^2 is a square.",
				"For example, 23/24 = 19947716/20815008 with pi(19947716)^2 + pi(20815008)^2 = 1267497^2 + 1319004^2 = 1829295^2, and 7/3 = 26964/11556 with pi(26964)^2 - pi(11556)^2 = 2958^2 - 1392^2 = 2610^2."
			],
			"reference": [
				"Zhi-Wei Sun, Problems on combinatorial properties of primes, in: M. Kaneko, S. Kanemitsu and J. Liu (eds.), Number Theory: Plowing and Starring through High Wave Forms, Proc. 7th China-Japan Seminar (Fukuoka, Oct. 28 - Nov. 1, 2013), Ser. Number Theory Appl., Vol. 11, World Sci., Singapore, 2015, pp. 169-187."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A255677/b255677.txt\"\u003eTable of n, a(n) for n = 2..100\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"/A255677/a255677_1.txt\"\u003eChecking the conjecture for r = a/b, b/a with 1 \u003c= a \u003c b \u003c= 50\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1402.6641\"\u003eProblems on combinatorial properties of primes\u003c/a\u003e, arXiv:1402.6641 [math.NT], 2014."
			],
			"example": [
				"a(2) = 5 since pi(5)^2 + pi(5*2)^2 = 3^2 + 4^2 = 5^2.",
				"a(3) = 30 since pi(30)^2 + pi(30*3)^2 = 10^2 + 24^2 = 26^2.",
				"a(68) = 6260592 since pi(6260592)^2 + pi(6260592*68)^2 = 429505^2 + 22632876^2 = 22636951^2.",
				"a(95) = 7955506 since pi(7955506)^2 + pi(7955506*95)^2 = 536984^2 + 38985687^2 = 38989385^2."
			],
			"mathematica": [
				"SQ[n_]:=IntegerQ[Sqrt[n]]",
				"Do[k=1;Label[aa];k=k+1;If[SQ[PrimePi[k]^2+PrimePi[k*n]^2],Goto[bb],Goto[aa]];Label[bb];Print[n,\" \",k];Continue,{n,2,50}]"
			],
			"program": [
				"(PARI) a(n)={ k=2; while(!issquare(primepi(k)^2 + primepi(k*n)^2),k++); return(k);}",
				"main(size)={ v=vector(size); for(i=2, size+1, v[i-1]=a(i)); return(v);} /* _Anders Hellström_, Jul 11 2015 */"
			],
			"xref": [
				"Cf. A000720, A000290, A255679, A259531, A259789."
			],
			"keyword": "nonn",
			"offset": "2,1",
			"author": "_Zhi-Wei Sun_, Jul 10 2015",
			"references": 2,
			"revision": 33,
			"time": "2015-07-16T08:37:20-04:00",
			"created": "2015-07-11T00:57:45-04:00"
		}
	]
}