{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A033888",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 33888,
			"data": "0,3,21,144,987,6765,46368,317811,2178309,14930352,102334155,701408733,4807526976,32951280099,225851433717,1548008755920,10610209857723,72723460248141,498454011879264,3416454622906707,23416728348467685,160500643816367088,1100087778366101931,7540113804746346429",
			"name": "a(n) = Fibonacci(4n).",
			"comment": [
				"(x,y)=(a(n),a(n+1)) are solutions of (x+y)^2/(1+xy)=9, the other solutions are in A033890. - _Floor van Lamoen_, Dec 10 2001",
				"Sequence A033888 provides half of the solutions to the equation 5*x^2 + 4 is a square. The other half are found in A033890. Lim_{n-\u003einfinity} a(n)/a(n-1) = phi^4 = (7+3*sqrt(5))/2. - _Gregory V. Richardson_, Oct 13 2002",
				"Fibonacci numbers divisible by 3. - _Reinhard Zumkeller_, Aug 20 2011"
			],
			"link": [
				"Nathaniel Johnston, \u003ca href=\"/A033888/b033888.txt\"\u003eTable of n, a(n) for n = 0..300\u003c/a\u003e",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (7,-1)."
			],
			"formula": [
				"a(n) = 7*a(n-1) - a(n-2).",
				"a(n) = ((7+3*sqrt(5))^(n-1) - (7-3*sqrt(5))^(n-1)) / ((2^(n-1))*sqrt(5)). - _Gregory V. Richardson_, Oct 13 2002",
				"a(n) = Sum_{k=0..n} F(3n-k)*binomial(n, k). - _Benoit Cloitre_, Jun 07 2004",
				"a(n) = Lucas(2n) * Lucas(n) * Fibonacci(n). - _Ralf Stephan_, Sep 25 2004",
				"G.f.: 3*x/(1-7*x+x^2). - _Philippe Deléham_, Nov 17 2008",
				"a(n) = 3*A004187(n). - _R. J. Mathar_, Sep 03 2010",
				"a(n) = Fibonacci[(4*n + 2)] modulo Fibonacci[(4*n + 1)]. - _Artur Jasinski_, Nov 15 2011 (corrected by _Iain Fox_, Dec 18 2017)",
				"a(n) = (A337929(n) + A337928(n)) / 2. - _Flávio V. Fernandes_, Feb 06 2021",
				"E.g.f.: 2*exp(7*x/2)*sinh(3*sqrt(5)*x/2)/sqrt(5). - _Stefano Spezia_, Feb 07 2021"
			],
			"example": [
				"G.f. = 3*x + 21*x^2 + 144*x^3 + 987*x^4 + 6765*x^5 + 46368*x^6 + ..."
			],
			"maple": [
				"A033888:=n-\u003ecombinat[fibonacci](4*n): seq(A033888(n), n=0..30); # _Wesley Ivan Hurt_, Apr 26 2017"
			],
			"mathematica": [
				"Table[Fibonacci[4*n], {n,0,14}] (* _Vladimir Joseph Stephan Orlovsky_, Jul 21 2008 *)",
				"Table[Mod[Fibonacci[(4 n + 2)] , Fibonacci[(4 n + 1)]], {n, 1, 10}] (* _Artur Jasinski_, Nov 15 2011 (corrected by _Iain Fox_, Dec 18 2017) *)"
			],
			"program": [
				"(MuPAD) numlib::fibonacci(n*4) $ n = 0..30; // _Zerinvary Lajos_, May 08 2008",
				"(Sage) [lucas_number1(n,3,1)*lucas_number2(n,3,1) for n in range(0,21)] # _Zerinvary Lajos_, Jun 28 2008",
				"(Sage) [fibonacci(4*n) for n in range(0, 20)] # _Zerinvary Lajos_, May 15 2009",
				"(MAGMA) [ Fibonacci(4*n): n in [0..100]]; // _Vincenzo Librandi_, Apr 15 2011",
				"(PARI) a(n)=fibonacci(4*n) \\\\ _Charles R Greathouse IV_, Feb 03 2014",
				"(PARI) first(n) = Vec(3*x/(1 - 7*x + x^2) + O(x^n), -n) \\\\ _Iain Fox_, Dec 18 2017",
				"(PARI) a(n) = fibonacci(4*n + 2) % fibonacci(4*n + 1) \\\\ _Iain Fox_, Dec 18 2017"
			],
			"xref": [
				"Cf. A000045.",
				"Fourth column of array A102310."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 26,
			"revision": 74,
			"time": "2021-03-03T13:50:56-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}