{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A061177",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 61177,
			"data": "1,2,-2,3,-5,3,4,-8,8,-4,5,-10,11,-10,5,6,-10,6,-6,10,-6,7,-7,-14,29,-14,-7,7,8,0,-56,120,-120,56,0,-8,9,12,-126,288,-365,288,-126,12,9,10,30,-228,540,-770,770,-540,228,-30,-10,11,55,-363,858",
			"name": "Coefficients of polynomials ( (1 -x +sqrt(x))^(n+1) - (1 -x -sqrt(x))^(n+1) )/(2*sqrt(x)).",
			"comment": [
				"The row polynomial pFo(m,x) = Sum_{j=0..m} T(m, j)*x^j is the numerator of the g.f. for the m-th column sequence of A060921, the odd part of the bisected Fibonacci triangle."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A061177/b061177.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = coefficient of x^k of ( (1 -x +sqrt(x))^(n+1) - (1 -x -sqrt(x))^(n+1) )/(2*sqrt(x)).",
				"T(n, k) = Sum_{j=0..k} (-1)^(k-j)*binomial(n+1, 2*j+1)*binomial(n-2*j, k-j), if 0 \u003c= k \u003c= floor(n/2), T(n, k) = (-1)^n*T(n, n-k) if floor(n/2) \u003c k \u003c= n else 0.",
				"Sum_{k=0..n} T(n, k) = (1 + (-1)^n)/2 = A059841(n). - _G. C. Greubel_, Apr 06 2021"
			],
			"example": [
				"The first few polynomials are:",
				"pFo(0, x) = 1.",
				"pFo(1, x) = 2 -  2*x.",
				"pFo(2, x) = 3 -  5*x +  3*x^2.",
				"pFo(3, x) = 4 -  8*x +  8*x^2 -  4*x^3.",
				"pFo(4, x) = 5 - 10*x + 11*x^2 - 10*x^3 +  5*x^4.",
				"pFo(5, x) = 6 - 10*x +  6*x^2 -  6*x^3 + 10*x^4 - 6*x^5.",
				"Number triangle begins as:",
				"   1;",
				"   2,  -2;",
				"   3,  -5,    3;",
				"   4,  -8,    8,  -4;",
				"   5, -10,   11, -10,    5;",
				"   6, -10,    6,  -6,   10,  -6;",
				"   7,  -7,  -14,  29,  -14,  -7,    7;",
				"   8,   0,  -56, 120, -120,  56,    0,  -8;",
				"   9,  12, -126, 288, -365, 288, -126,  12,   9;",
				"  10,  30, -228, 540, -770, 770, -540, 228, -30, -10;"
			],
			"mathematica": [
				"T[n_, k_]:= Sum[(-1)^(k-j)*Binomial[n+1, 2*j+1]*Binomial[n-2*j, k-j], {j,0,k}];",
				"Table[T[n, k], {n, 0, 12}, {k, 0, n}]//Flatten (* _G. C. Greubel_, Apr 06 2021 *)"
			],
			"program": [
				"(Magma)",
				"A061177:= func\u003c n,k | (\u0026+[(-1)^(k+j)*Binomial(n+1,2*j+1)*Binomial(n-2*j,k-j): j in [0..k]]) \u003e;",
				"[A061177(n,k): k in [0..n], n in [0..15]]; // _G. C. Greubel_, Apr 06 2021",
				"(Sage)",
				"def A061177(n,k): return sum((-1)^(k+j)*binomial(n+1,2*j+1)*binomial(n-2*j,k-j) for j in (0..k))",
				"flatten([[A061177(n,k) for k in (0..n)] for n in (0..15)]) # _G. C. Greubel_, Apr 06 2021"
			],
			"xref": [
				"Cf. A059841, A060921, A061176 (companion triangle)."
			],
			"keyword": "sign,easy,tabl",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Apr 20 2001",
			"references": 7,
			"revision": 7,
			"time": "2021-04-06T23:09:33-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}