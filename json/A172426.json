{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A172426",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 172426,
			"data": "5,12,27,75,363,1587,2523,5043,8427,20667,23763,38307,51483,89787,96123,109443,162867,171363,189003,236883,257547",
			"name": "Number of nontrivial solutions (x,y,z) for each prime number p of the Fermat equation x^p + y^p + z^p = 0 mod (n) where n is prime of the form n = 2p + 1, and x, y, z are integers such that x \u003c = y.",
			"comment": [
				"Solution to a Diophantine equation in finite fields Z/n. Historical reminder: Sophie Germain's work led to Fermat's Last Theorem being broken into two cases: x^p + y^p= z^p has no integer solutions for which x,y and z are relatively prime to p, i.e., in which none of x,y and z are divisible by p, and then x^p + y^p = z^p has no integer solutions for which one of the three numbers is divisible by p.",
				"This result was presented by Legendre in an 1823 paper to the French Academy of Sciences and included in a supplement to his second edition of Theorie des Nombres, with a footnote crediting the result to Sophie Germain. Sophie Germain's Theorem introduce an auxiliary prime n satisfying the two conditions: x^p + y^p + z^p = 0 mod (n) implies that x = 0 mod n, or y = 0 mod n, or z = 0 mod n, and x^p = p mod n is impossible for any value of x. Then Case I of Fermat's Last Theorem is true for p. This sequence give solutions for each prime number p, and n = 2p + 1."
			],
			"reference": [
				"Del Centina, Andrea. \"Unpublished manuscripts of Sophie Germain and a revaluation of her work on Fermat's Last Theorem,\" Arch. Hist. Exact Sci., Vol 62 (2008), 349-392.",
				"Legendre, A. M., \"Recherches sur quelques objets d'analyse indeterminee et particulierement sur le theoreme de Fermat,\" Mem. Acad. Sci. Inst. France 6 (1823), 1-60.",
				"Sampson, J.H. \"Sophie Germain and the theory of numbers,\" Arch. Hist. Exact Sci. 41 (1990), 157-161.",
				"Schoof, \"Wiles' proof of the Taniyama-Weil conjecture for semi-stable elliptic curves over Q\", Chap. 14 in 'Ou En Sont Les Mathematiques ?' Soc. Math. de France (SMF), Vuibert, Paris 2002."
			],
			"link": [
				"C. K. Caldwell, The Prime Glossary,\u003ca href=\"http://primes.utm.edu/glossary/page.php?sort=FermatsLastTheorem\"\u003e Fermat's Last Theorem\u003c/a\u003e",
				"Del Centina, Andrea. \u003ca href=\"http://web.unife.it/progetti/geometria/Germain.html\"\u003eLetters of Sophie Germain preserved in Florence\u003c/a\u003e, Historia Mathematica, Vol. 32 (2005), 60-75."
			],
			"example": [
				"We consider the case p = 1, n = 3. We have 5 solutions mod 3: (0,1,2), (0,2,1), (1,1,1), (1,2,0), (2,2,2).",
				"With p = 2, n = 5, we have 12 solutions mod 5: (0,1,2), (0,1,3), (0,2,1), (0,2,4), (0,3,1), (0,3,4), (0,4,2), (0,4,3), (1,2,0), (1,3,0), (2,4,0), (3,4,0),",
				"With p = 3, n = 7, we have 27 solutions mod 7: (0,1,3), (0,1,5), (0,1,6), (0,2,3), (0,2,5), (0,2,6), (0,3,1), (0,3,2), (0,3,4), (0,4,3), (0,4,5), (0,4,6), (0,5,1), (0,5,2), (0,5,4), (0,6,1), (0,6,2), (0,6,4), (1,3,0), (1,5,0), (1,6,0), (2,3,0), (2,5,0), (2,6,0), (3,4,0), (4,5,0), (4,6,0)."
			],
			"xref": [
				"Cf. A019590 (Fermat's last theorem)."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Michel Lagneau_, Feb 02 2010",
			"references": 0,
			"revision": 6,
			"time": "2016-12-10T19:14:18-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}