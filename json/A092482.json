{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A092482",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 92482,
			"data": "1,2,3,6,7,14,15,17,18,36,37,39,40,45,46,48,49,98,99,101,102,107,108,110,111,125,126,128,129,134,135,137,138,276,277,279,280,285,286,288,289,303,304,306,307,312,313,315,316,357,358,360,361,366,367,369,370",
			"name": "Sequence contains no 3-term arithmetic progression, other than its initial terms 1,2,3.",
			"comment": [
				"a(1)=1, a(2)=2, a(3)=3; a(n) is least k such that no three terms of a(1), a(2), ..., a(n-1), k form an arithmetic progression, except for the first triple (1,2,3)."
			],
			"link": [
				"David A. Corneth, \u003ca href=\"/A092482/b092482.txt\"\u003eTable of n, a(n) for n = 1..8193\u003c/a\u003e (first 512 terms by Jean-François Alcover)",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/NonarithmeticProgressionSequence.html\"\u003eNonarithmetic Progression Sequence.\u003c/a\u003e",
				"\u003ca href=\"http://oeis.org/wiki/Index_to_OEIS:_Section_No#non_averaging\"\u003eIndex entries related to non-averaging sequences\u003c/a\u003e"
			],
			"formula": [
				"For n\u003e2, a(n+2) = 1 + 2^floor(log_2(n)) + Sum_{k=1..n} (3^A007814(n)+1)/2 = 1 + A053644(n) + A005836(n) (conjectured and checked up to n=512)."
			],
			"mathematica": [
				"a[n_] := a[n] = If[n \u003c 4, n, For[k = a[n - 1] + 1, True, k++, sp = SequencePosition[Append[Array[a, n - 1], k], {x_, ___, y_, ___, z_} /; y - x == z - y, 2]; If[sp == {{1, 3}}, Return[k]]]];",
				"Table[Print[n, \" \", a[n]]; a[n], {n, 1, 512}]",
				"(* Comparing with data from conjectured formula: *)",
				"b[n_] := If[n \u003c 4, n, 1 + 2^(Length[id = IntegerDigits[n - 2, 2]] - 1) + FromDigits[id, 3]];",
				"Table[b[n], {n, 1, 512}] (* _Jean-François Alcover_, Jan 15 2019 *)",
				"(* Second [much faster] program: *)",
				"upto[m_] := Module[{n, v, i, j}, n = Max[m, 3]; v = Table[1, {n}]; For[i = 3, i \u003c= n-1, i++, If[v[[i]] == 1, For[j = Max[1, 2i-n], j \u003c= Min[2n-i, i-1], j++, If[v[[j]] == 1, v[[2i-j]] = 0]]]]; Position[v, 1] // Flatten]; upto[12000] (* _Jean-François Alcover_, Jan 16 2019, after _David A. Corneth_ *)"
			],
			"program": [
				"(PARI) upto(n) = n=max(n,3); v=vector(n, i, 1); for(i=3, n-1, if(v[i]==1, for(j = max(1, 2*i-n), min(2*n-i,i-1), c=2*i - j; if(v[j]==1, v[2*i-j]=0; )))); select(x -\u003e x==1, v, 1) \\\\ _David A. Corneth_, Jan 15 2019"
			],
			"xref": [
				"Cf. A004793, A033157."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Ralf Stephan_, Apr 04 2004",
			"ext": [
				"Name clarified by _Charles R Greathouse IV_, Jan 30 2014"
			],
			"references": 14,
			"revision": 35,
			"time": "2021-06-27T07:54:03-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}