{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005183",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5183,
			"id": "M1434",
			"data": "1,2,5,13,33,81,193,449,1025,2305,5121,11265,24577,53249,114689,245761,524289,1114113,2359297,4980737,10485761,22020097,46137345,96468993,201326593,419430401,872415233,1811939329,3758096385,7784628225,16106127361,33285996545",
			"name": "a(n) = n*2^(n-1) + 1.",
			"comment": [
				"a(n-1) is the number of permutations of length n which avoid the patterns 132, 4312. - _Lara Pudwell_, Jan 21 2006",
				"Number of sequences (e(1), ..., e(n+1)), 0 \u003c= e(i) \u003c i, such that there is no triple i \u003c j \u003c k with e(i) \u003c= e(j) \u003e= e(k) and e(i) != e(k). [Martinez and Savage, 2.11] - _Eric M. Schmidt_, Jul 17 2017",
				"Indices of records in A066099. Also, indices of \"cusps\" in the graph of A030303 giving positions of 1's in the binary Champernowne word A030190. - _M. F. Hasler_, Oct 12 2020"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A005183/b005183.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Stephan Baier, Pallab Kanti Dey, \u003ca href=\"https://arxiv.org/abs/1905.13003\"\u003ePrime powers dividing products of consecutive integer values of x^2^n + 1\u003c/a\u003e, arXiv:1905.13003 [math.NT], 2019. See p. 7.",
				"Jean-Luc Baril, Sergey Kirgizov, Vincent Vajnovszki, \u003ca href=\"https://arxiv.org/abs/1803.06706\"\u003eDescent distribution on Catalan words avoiding a pattern of length at most three\u003c/a\u003e, arXiv:1803.06706 [math.CO], 2018.",
				"A. M. Baxter, L. K. Pudwell, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v22i1p58\"\u003eAscent sequences avoiding pairs of patterns\u003c/a\u003e, The Electronic Journal of Combinatorics, Volume 22, Issue 1 (2015) Paper #P1.58.",
				"Christian Bean, Bjarki Gudmundsson, Henning Ulfarsson, \u003ca href=\"https://arxiv.org/abs/1705.04109\"\u003eAutomatic discovery of structural rules of permutation classes\u003c/a\u003e, arXiv:1705.04109 [math.CO], 2017.",
				"R. K. Guy, \u003ca href=\"http://www.jstor.org/stable/2691503\"\u003eThe Second Strong Law of Small Numbers\u003c/a\u003e, Math. Mag, 63 (1990), no. 1, 3-20.",
				"R. K. Guy, \u003ca href=\"/A005347/a005347.pdf\"\u003eThe Second Strong Law of Small Numbers\u003c/a\u003e, Math. Mag, 63 (1990), no. 1, 3-20. [Annotated scanned copy]",
				"R. K. Guy and N. J. A. Sloane, \u003ca href=\"/A005180/a005180.pdf\"\u003eCorrespondence\u003c/a\u003e, 1988.",
				"V. Jelinek, T. Mansour, M. Shattuck, \u003ca href=\"http://dx.doi.org/10.1016/j.aam.2012.09.002\"\u003eOn multiple pattern avoiding set partitions\u003c/a\u003e, Adv. Appl. Math. 50 (2) (2013) 292-326, Example 4.16, H_{1223} and Example 4.17 L_{1232} and propositions 4.20 and 4.22, all shifted with an additional leading a(0)=1.",
				"Megan A. Martinez and Carla D. Savage, \u003ca href=\"https://arxiv.org/abs/1609.08106\"\u003ePatterns in Inversion Sequences II: Inversion Sequences Avoiding Triples of Relations\u003c/a\u003e, arXiv:1609.08106 [math.CO], 2016.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"Lara Pudwell, \u003ca href=\"http://faculty.valpo.edu/lpudwell/maple/webbook/bookmain.html\"\u003eSystematic Studies in Pattern Avoidance\u003c/a\u003e, 2005.",
				"L. Pudwell, \u003ca href=\"http://faculty.valpo.edu/lpudwell/slides/ascseq.pdf\"\u003ePattern-avoiding ascent sequences\u003c/a\u003e, Slides from a talk, 2015 Joint Mathematics Meetings, AMS Special Session on Enumerative Combinatorics, January 11, 2015.",
				"L. Pudwell, A. Baxter, \u003ca href=\"http://faculty.valpo.edu/lpudwell/slides/pp2014_pudwell.pdf\"\u003eAscent sequences avoiding pairs of patterns\u003c/a\u003e, Permutation Patterns 2014, East Tennessee State University, July 7, 2014.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (5,-8,4)."
			],
			"formula": [
				"Main diagonal of the array defined by T(0, j)=j+1 j\u003e=0, T(i, 0)=i+1 i\u003e=0, T(i, j)=T(i-1, j-1)+T(i-1, j)-1. - _Benoit Cloitre_, Jun 17 2003",
				"G.f.: (1 -3*x +3*x^2)/((1-x)*(1-2*x)^2). - _Lara Pudwell_, Jan 21 2006",
				"E.g.f.: exp(x) +x*exp(2*x). - _Joerg Arndt_, May 22 2013",
				"Binomial transform of A028310. a(n) = 1 + Sum{k=0..n} C(n, k)*k = 1 + A001787(n). - _Paul Barry_, Jul 21 2003",
				"a(n) = Sum_{k=0..2^n} A000120(k) = A000788(2^n). - _Benoit Cloitre_, Sep 25 2003",
				"Row sums of triangle A134399. - _Gary W. Adamson_, Oct 23 2007",
				"a(n) = A000788(A000079(n)). - _Reinhard Zumkeller_, Mar 04 2010",
				"a(n) = 2*a(n-1) +2^(n-1) -1 (with a(0)=1). - _Vincenzo Librandi_, Dec 31 2010"
			],
			"maple": [
				"A005183 := (1-3*z+3*z**2)/(1-z)/(1-2*z)**2; # Generating function conjectured by _Simon Plouffe_ in his 1992 dissertation."
			],
			"mathematica": [
				"Table[(n+1)*2^n+1,{n,1,30}] (* _Alexander Adamchuk_, Sep 09 2006 *)",
				"LinearRecurrence[{5,-8,4},{1,2,5},30] (* _Harvey P. Dale_, Jul 29 2015 *)"
			],
			"program": [
				"(PARI) a(n)=n*2^(n-1)+1 \\\\ _Charles R Greathouse IV_, Sep 24 2015",
				"(MAGMA) [n*2^(n-1)+1: n in [0..35]]; // _Vincenzo Librandi_, May 14 2017",
				"(Sage) [2^(n-1)*n+1 for n in (0..35)] # _G. C. Greubel_, May 31 2019"
			],
			"xref": [
				"Cf. A134399."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, _R. K. Guy_",
			"ext": [
				"More terms from _Lara Pudwell_, Jan 21 2006",
				"Edited by _N. J. A. Sloane_ at the suggestion of Jim Propp, Jul 14 2007"
			],
			"references": 28,
			"revision": 97,
			"time": "2021-03-12T22:32:36-05:00",
			"created": "1991-05-16T03:00:00-04:00"
		}
	]
}