{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A247564",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 247564,
			"data": "2,1,3,1,1,-1,-9,-7,-31,-17,-57,-23,-47,-1,87,89,449,271,999,457,1201,287,-393,-967,-5983,-4049,-16377,-8279,-25199,-8641,-10089,7193,70529,56143,251943,139657,473713,194399,413367,24569,-654751,-703889,-3617721",
			"name": "a(n) = 3*a(n-2) - 4*a(n-4) with a(0) = 2, a(1) = 1, a(2) = 3, a(3) = 1.",
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A247564/b247564.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,3,0,-4)."
			],
			"formula": [
				"G.f.: (2 + x - 3*x^2 - 2*x^3) / (1 - 3*x^2 + 4*x^4).",
				"a(n) = A247487(n) * 3^( n == 1 (mod 4) ) for all n in Z.",
				"a(2*n) = A247563(n). a(2*n + 1) = A247560(n).",
				"0 = a(n)*(+2*a(n+2)) + a(n+1)*(+2*a(n+1) - 8*a(n+2) + a(n+3)) + a(n+2)*(+a(n+2)) for all n in Z.",
				"a(n) = (-1)^floor(n/2)*H(n, n mod 2, 1/2)) for n \u003e= 3 where H(n, a, b) = hypergeom([a - n/2, b - n/2], [1 - n], 8). - _Peter Luschny_, Sep 03 2019"
			],
			"example": [
				"G.f. = 2 + x + 3*x^2 + x^3 + x^4 - x^5 - 9*x^6 - 7*x^7 - 31*x^8 - 17*x^9 + ..."
			],
			"maple": [
				"H := (n, a, b) -\u003e hypergeom([a - n/2, b - n/2], [1 - n], 8):",
				"a := n -\u003e `if`(n \u003c 3, [2, 1, 3][n+1], (-1)^iquo(n, 2)*H(n, irem(n, 2), 1/2)):",
				"seq(simplify(a(n)), n=0..42); # _Peter Luschny_, Sep 03 2019",
				"# second Maple program:",
				"a:= n-\u003e (\u003c\u003c0|1\u003e, \u003c-4|3\u003e\u003e^iquo(n, 2, 'r').\u003c[\u003c2, 3\u003e, \u003c1, 1\u003e][1+r]\u003e)[1,1]:",
				"seq(a(n), n=0..42);  # _Alois P. Heinz_, Sep 03 2019"
			],
			"mathematica": [
				"CoefficientList[Series[(2+x-3*x^2-2*x^3)/(1-3*x^2+4*x^4), {x,0,60}], x] (* _G. C. Greubel_, Aug 04 2018 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, n=-n; 2^-n, 1) * polcoeff( (2 + x - 3*x^2 - 2*x^3) / (1 - 3*x^2 + 4*x^4) + x * O(x^n), n)};",
				"(Haskell)",
				"a247564 n = a247564_list !! n",
				"a247564_list = [2,1,3,1] ++ zipWith (-) (map (* 3) $ drop 2 a247564_list)",
				"                                        (map (* 4) $ a247564_list)",
				"-- _Reinhard Zumkeller_, Sep 20 2014",
				"(MAGMA) m:=25; R\u003cx\u003e:=PowerSeriesRing(Integers(), m); Coefficients(R!((2+x-3*x^2-2*x^3)/(1-3*x^2+4*x^4)));  // _G. C. Greubel_, Aug 04 2018"
			],
			"xref": [
				"Cf. A247487, A247560, A247563."
			],
			"keyword": "sign,easy",
			"offset": "0,1",
			"author": "_Michael Somos_, Sep 20 2014",
			"references": 3,
			"revision": 31,
			"time": "2019-09-03T14:16:59-04:00",
			"created": "2014-09-20T00:53:39-04:00"
		}
	]
}