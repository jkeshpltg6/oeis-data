{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A172359",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 172359,
			"data": "1,1,1,1,1,1,1,1,1,1,1,5,5,5,1,1,5,25,25,5,1,1,9,45,225,45,9,1,1,25,225,1125,1125,225,25,1,1,29,725,6525,6525,6525,725,29,1,1,61,1769,44225,79605,79605,44225,1769,61,1,1,129,7869,228201,1141005,2053809,1141005,228201,7869,129,1",
			"name": "Triangle read by rows: T(n,k) = round(c(n)/(c(k)*c(n-k))) where c are partial products of a sequence defined in comments.",
			"comment": [
				"Start from the sequence 0, 1, 1, 1, 5, 5, 9, 25, 29, 61, 129, 177, 373, 693, 1081, 2185, 3853, ..., f(n) = f(n-2) + 4*f(n-3) and its partial products c(n) = 1, 1, 1, 1, 5, 25, 225, 5625, 163125, 9950625, ... . Then T(n,k) = round(c(n)/(c(k)*c(n-k)))."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A172359/b172359.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k, q) = round(c(n,q)/(c(k,q)*c(n-k,q)), where c(n,q) = Product_{j=1..n} f(j,q), f(n, q) = f(n-2, q) + q*f(n-3, q), f(0,q)=0, f(1,q) = f(2,q) = 1, and q = 4. - _G. C. Greubel_, May 09 2021"
			],
			"example": [
				"Triangle begins as:",
				"  1;",
				"  1,   1;",
				"  1,   1,    1;",
				"  1,   1,    1,      1;",
				"  1,   5,    5,      5,       1;",
				"  1,   5,   25,     25,       5,       1;",
				"  1,   9,   45,    225,      45,       9,       1;",
				"  1,  25,  225,   1125,    1125,     225,      25,      1;",
				"  1,  29,  725,   6525,    6525,    6525,     725,     29,    1;",
				"  1,  61, 1769,  44225,   79605,   79605,   44225,   1769,   61,   1;",
				"  1, 129, 7869, 228201, 1141005, 2053809, 1141005, 228201, 7869, 129, 1;"
			],
			"mathematica": [
				"f[n_, q_]:= f[n, q]= If[n\u003c3, Fibonacci[n], f[n-2, q] + q*f[n-3, q]];",
				"c[n_, q_]:= Product[f[j, q], {j,n}];",
				"T[n_, k_, q_]:= Round[c[n, q]/(c[k, q]*c[n-k, q])];",
				"Table[T[n, k, 4], {n,0,12}, {k,0,n}]//Flatten (* modified by _G. C. Greubel_, May 09 2021 *)"
			],
			"program": [
				"(Sage)",
				"@CachedFunction",
				"def f(n,q): return fibonacci(n) if (n\u003c3) else f(n-2, q) + q*f(n-3, q)",
				"def c(n,q): return product( f(j,q) for j in (1..n) )",
				"def T(n,k,q): return round(c(n, q)/(c(k, q)*c(n-k, q)))",
				"flatten([[T(n,k,4) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, May 09 2021"
			],
			"xref": [
				"Cf. A172353 (q=1), A172358 (q=2), this sequence (q=4), A172360 (q=5)."
			],
			"keyword": "nonn,tabl,less",
			"offset": "0,12",
			"author": "_Roger L. Bagula_, Feb 01 2010",
			"ext": [
				"Definition corrected to give integral terms by _G. C. Greubel_, May 09 2021"
			],
			"references": 3,
			"revision": 11,
			"time": "2021-05-10T03:45:16-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}