{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A236401",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 236401,
			"data": "15,-47,197,-1029,6439,-46927,390249,-3645737,37792331,-430400211,5341017373,-71724018781,1036207207363983,-16024176975479,264083895859409,-4620276321889617,85520275455047059,-1669635965205539227,34287733935303686661",
			"name": "Kurepa determinant K_n.",
			"comment": [
				"This is the value of a certain determinant of order n-4 (see Metrovic 2013 for definition)."
			],
			"link": [
				"Romeo Mestrovic, \u003ca href=\"http://arxiv.org/abs/1312.7037\"\u003eVariations of Kurepa's left factorial hypothesis\u003c/a\u003e, arXiv preprint arXiv:1312.7037 [math.NT], 2013-2014.",
				"Romeo Mestrovic, \u003ca href=\"https://doi.org/10.2298/FIL1510207M\"\u003eThe Kurepa-Vandermonde matrices arising from Kurepa's left factorial hypothesis\u003c/a\u003e, Filomat 29:10 (2015), 2207-2215; DOI 10.2298/FIL1510207M."
			],
			"formula": [
				"Conjecture: a(n) ~ -(-1)^n * n! * exp(-1) / n^4. - _Vaclav Kotesovec_, Nov 30 2017"
			],
			"maple": [
				"A236401 := proc(n)",
				"    local M,r,c ;",
				"    M := Matrix(n-4,n-4) ;",
				"    for r from 1 to n-4 do",
				"    for c from 1 to n-4 do",
				"        if r = 1 then",
				"            if c \u003c n-4 then",
				"                M[r,c] := 1 ;",
				"            else",
				"                M[r,c] := 3 ;",
				"            end if;",
				"        elif r = n-4 then",
				"            if c = n-4 then",
				"                M[r,c] := -4 ;",
				"            elif c = n-5 then",
				"                M[r,c] := 1 ;",
				"            else",
				"                M[r,c] := 0 ;",
				"            end if;",
				"        elif c = n-4 then",
				"            M[r,c] := 2 ;",
				"        elif r \u003e c+2 then",
				"            M[r,c] := 0 ;",
				"        elif r = c+2 then",
				"            M[r,c] := 1 ;",
				"        elif r = c+1 then",
				"            M[r,c] := r+1 ;",
				"        elif c = n-4 then",
				"            M[r,c] := 2 ;",
				"        else",
				"            M[r,c] := 1 ;",
				"        end if",
				"    end do:",
				"    end do:",
				"    LinearAlgebra[Determinant](M) ;",
				"end proc:",
				"seq(A236401(n),n=7..25) ;"
			],
			"mathematica": [
				"A236401[n_] := Det[Table[Which[",
				"   r == 1, If[c \u003c n - 4, 1, 3],",
				"   r == n - 4, Which[",
				"   c == n - 4, -4,",
				"   c == n - 5, 1,",
				"   True, 0],",
				"   c == n - 4, 2,",
				"   r \u003e c + 2, 0,",
				"   r == c + 2, 1,",
				"   r == c + 1, r + 1,",
				"   c == n - 4, 2,",
				"   True, 1],",
				"{r, 1, n - 4}, {c, 1, n - 4}]];",
				"Table[A236401[n], {n, 7, 25}] (* _Jean-François Alcover_, Nov 30 2017, from Maple *)"
			],
			"keyword": "sign",
			"offset": "7,1",
			"author": "_N. J. A. Sloane_, Jan 29 2014",
			"references": 1,
			"revision": 17,
			"time": "2017-11-30T03:55:42-05:00",
			"created": "2014-01-29T21:57:27-05:00"
		}
	]
}