{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A266700",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 266700,
			"data": "0,-10,-12,-44,-102,-280,-720,-1898,-4956,-12988,-33990,-89000,-232992,-609994,-1596972,-4180940,-10945830,-28656568,-75023856,-196415018,-514221180,-1346248540,-3524524422,-9227324744,-24157449792,-63245024650,-165577624140",
			"name": "Coefficient of x in minimal polynomial of the continued fraction [1^n,1/2,1,1,1,...], where 1^n means n ones.",
			"comment": [
				"See A265762 for a guide to related sequences."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A266700/b266700.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,2,-1)."
			],
			"formula": [
				"a(n) = 2*a(n-1) + 2*a(n-2) - a(n-3).",
				"G.f.: (2 (-5 x + 4 x^2))/(1 - 2 x - 2 x^2 + x^3).",
				"a(n) = (2^(-n)*(9*(-1)^n*2^(1+n) + (3-sqrt(5))^n*(-9+sqrt(5)) - (3+sqrt(5))^n*(9+sqrt(5))))/5. - _Colin Barker_, Oct 20 2016"
			],
			"example": [
				"Let p(n,x) be the minimal polynomial of the number given by the n-th continued fraction:",
				"[1/2,1,1,1,1,...] = sqrt(5))/2 has p(0,x) = -5 + 4 x^2, so a(0) = 1;",
				"[1,1/2,1,1,1,...] = (5 + 2 sqrt(5))/5 has p(1,x) = 1 - 10 x + 5 x^2, so a(1) = 19;",
				"[1,1,1/2,1,1,...] = 6 - 2 sqrt(5) has p(2,x) = 16 - 12 x + x^2, so a(2) = 29."
			],
			"mathematica": [
				"u[n_] := Table[1, {k, 1, n}]; t[n_] := Join[u[n], {1/2}, {{1}}];",
				"f[n_] := FromContinuedFraction[t[n]];",
				"t = Table[MinimalPolynomial[f[n], x], {n, 0, 20}]",
				"Coefficient[t, x, 0] (* A266699 *)",
				"Coefficient[t, x, 1] (* A266700 *)",
				"Coefficient[t, x, 2] (* A266699 *)",
				"LinearRecurrence[{2, 2, -1}, {0, -10, -12}, 30] (* _Vincenzo Librandi_, Jan 06 2016 *)"
			],
			"program": [
				"(MAGMA) I:=[0,-10,-12]; [n le 3 select I[n] else 2*Self(n-1)+2*Self(n-2)-Self(n-3): n in [1..30]]; // _Vincenzo Librandi_, Jan 06 2016",
				"(PARI) concat(0, Vec((-10*x+8*x^2)/(1-2*x-2*x^2+x^3) + O(x^100))) \\\\ _Altug Alkan_, Jan 07 2016"
			],
			"xref": [
				"Cf. A265762, A266699."
			],
			"keyword": "sign,easy",
			"offset": "0,2",
			"author": "_Clark Kimberling_, Jan 05 2016",
			"references": 3,
			"revision": 15,
			"time": "2016-10-20T14:40:10-04:00",
			"created": "2016-01-09T14:24:45-05:00"
		}
	]
}