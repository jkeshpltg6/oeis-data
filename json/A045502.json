{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A045502",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 45502,
			"data": "0,40,3960,388080,38027920,3726348120,365144087880,35780394264160,3506113493799840,343563341998120200,33665701402321979800,3298895174085555900240,323258061358982156243760,31675991118006165755988280,3103923871503245261930607720",
			"name": "Numbers k such that 2*k+1 and 3*k+1 are squares.",
			"comment": [
				"Problem 1 for the 3rd grade of the 38th Mathematics Competition of the Republic of Slovenia (1998) was to prove that if k is a natural number such that 2*k+1 and 3*k+1 are perfect squares, then k is divisible by 40 (see link with solution Crux Mathematicorum and formula Mar 25 2021). - _Bernard Schott_, Mar 25 2021"
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A045502/b045502.txt\"\u003eTable of n, a(n) for n = 0..500\u003c/a\u003e.",
				"John Albert, \u003ca href=\"http://www2.math.ou.edu/~jalbert/putnam/pell.pdf\"\u003ePell Equations\u003c/a\u003e, Putnam practice, November 17, 2004 (1-2).",
				"R. S. Luthar, \u003ca href=\"https://www.jstor.org/stable/2322070\"\u003eProblem E2606\u003c/a\u003e, Amer. Math. Monthly, 84 (1977), 823-824.",
				"R. E. Woodrow, \u003ca href=\"https://cms.math.ca/crux/v25/n4/page196-211.pdf\"\u003eProblem 1 for the third grade of the 38th Mathematics competition of the Republic of Slovenia (1998)\u003c/a\u003e, Crux Mathematicorum, The Olympiad Corner, p. 208, April 1999, Vol. 25, No. 4.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (99,-99,1).",
				"\u003ca href=\"/index/O#Olympiads\"\u003eIndex to sequences related to Olympiads and other Mathematical Competitions\u003c/a\u003e."
			],
			"formula": [
				"From _Colin Barker_, Mar 23 2017: (Start)",
				"O.g.f.: 40*x / ((1 - x)*(1 - 98*x + x^2)).",
				"a(n) = 99*a(n-1)- 99*a(n-2) + a(n-3) for n\u003e2.",
				"a(n) = (-10 + (5 - 2*sqrt(6))*(49 + 20*sqrt(6))^(-n) + (5 + 2*sqrt(6))*(49 + 20*sqrt(6))^n)/24. (End)",
				"From _G. C. Greubel_, Jan 13 2020: (Start)",
				"a(n) = 5*(ChebyshevT(n, 49) + 48*ChebyshevU(n-1, 48) - 1)/12.",
				"a(n) = 4*ChebyshevU(n-1, 5)*ChebyshevU(n, 5). (End)",
				"a(n) = 40*A278620(n). - _Bernard Schott_, Mar 25 2021"
			],
			"maple": [
				"seq(coeff(series(40*x/((1-x)*(x^2-98*x+1)), x,n+1),x,n),n=0..15); # _Muniru A Asiru_, Jul 17 2018"
			],
			"mathematica": [
				"f[0]=0; f[1]=2; f[n_]:= f[n]= 10*f[n-1] -f[n-2]; a[n_]:= f[n]*f[n+1];",
				"CoefficientList[Series[40x/((1-x)(1-98x+x^2)), {x,0,15}], x] (* _Michael De Vlieger_, Jul 20 2018 *)",
				"Table[5*(ChebyshevT[n, 49] +48*ChebyshevU[n-1, 49] -1)/12, {n,0,15}] (* _G. C. Greubel_, Jan 13 2020 *)"
			],
			"program": [
				"(PARI) concat(0, Vec(40*x/((1-x)*(1-98*x+x^2))+O(x^20))) \\\\ _Colin Barker_, Mar 23 2017",
				"(GAP) a:=[0,40,3960];; for n in [4..15] do a[n]:=99*a[n-1]-99*a[n-2]+a[n-3]; od; a; # _Muniru A Asiru_, Jul 17 2018",
				"(MAGMA) I:=[0,40,3960]; [n le 3 select I[n] else 99*Self(n-1) -99*Self(n-2) + Self(n-3): n in [1..15]]; // _G. C. Greubel_, Jan 13 2020",
				"(Sage) [4*chebyshev_U(n-1,5)*chebyshev_U(n,5) for n in (0..15)] # _G. C. Greubel_, Jan 13 2020"
			],
			"xref": [
				"Cf. A001078, A245031, A278620."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "Fred Schwab (fschwab(AT)nrao.edu)",
			"references": 5,
			"revision": 33,
			"time": "2021-03-26T12:33:59-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}