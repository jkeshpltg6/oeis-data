{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A191460",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 191460,
			"data": "1,1,-1,2,6,24,-480,720,13440,100800,-1874880,3628800,22619520,479001600,-36739422720,468583315200,6345334195200,20922789888000,-4289844445286400,6402373705728000,363873320191180800,18895538930171904000,-350378437544239104000",
			"name": "E.g.f.: A(x) = Sum_{n\u003e=0} binomial(x^n, n).",
			"comment": [
				"The definition of the e.g.f. A(x) is an application of the identity:",
				"* Sum_{n\u003e=0} log(1 + q^n*x)^n/n! = Sum_{n\u003e=0} binomial(q^n, n)*x^n at x=1, q=x."
			],
			"formula": [
				"E.g.f.: A(x) = Sum_{n\u003e=0} log(1 + x^n)^n/n!.",
				"E.g.f.: A(x) = Sum_{n\u003e=0} Sum_{k=0..n} Stirling1(n,k)*x^(n*k)/n!.",
				"|a(p)| = (p-1)! for prime p."
			],
			"example": [
				"E.g.f.: A(x) = 1 + x - x^2/2! + 2*x^3/3! + 6*x^4/4! + 24*x^5/5! +...",
				"Series expansions:",
				"* A(x) = 1 + log(1 + x) + log(1 + x^2)^2/2! + log(1 + x^3)^3/3! + log(1 + x^4)^4/4! +...+ log(1 + x^n)^n/n! +...",
				"* A(x) = 1 + x + x^2*(x^2-1)/2! + x^3*(x^3-1)*(x^3-2)/3! + x^4*(x^4-1)*(x^4-2)*(x^4-3)/4! +...+ binomial(x^n, n) +...",
				"Coefficients a(n)/n! in the series expansion of the e.g.f. begin:",
				"A(x) = 1 + x - 1/2*x^2 + 1/3*x^3 + 1/4*x^4 + 1/5*x^5 - 2/3*x^6 + 1/7*x^7 + 1/3*x^8 + 5/18*x^9 - 31/60*x^10 + 1/11*x^11 + 17/360*x^12 + 1/13*x^13 +...",
				"which illustrates the property |a(p)/p!| = 1/p for prime p."
			],
			"program": [
				"(PARI) {a(n)=local(A=1+x); A=sum(m=0, n, log(1+x^m +x*O(x^n))^m/m!); n!*polcoeff(A, n)}",
				"(PARI) {a(n)=local(A=1+x); A=sum(m=0, n, binomial(x^m +x*O(x^n), m)); n!*polcoeff(A, n)}",
				"(PARI) {Stirling1(n, k)=n!*polcoeff(binomial(x, n), k)}",
				"{a(n)=local(A=1+x); for(i=1, n, A=sum(m=0, n, sum(k=0, m, Stirling1(m, k)*x^(m*k))/m!)); n!*polcoeff(A, n)}"
			],
			"xref": [
				"Cf. A191461."
			],
			"keyword": "sign",
			"offset": "0,4",
			"author": "_Paul D. Hanna_, Jun 02 2011",
			"references": 1,
			"revision": 9,
			"time": "2012-03-30T18:37:26-04:00",
			"created": "2011-06-03T09:24:31-04:00"
		}
	]
}