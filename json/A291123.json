{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A291123",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 291123,
			"data": "1,2,2,1,3,3,1,3,3,2,5,3,2,4,2,3,4,4,2,3,4,2,4,1,2,8,4,2,3,5,3,5,5,2,4,4,4,6,5,3,8,4,3,6,1,5,7,6,2,5,6,3,8,3,3,5,5,5,4,4,5,6,3,2,7,8,4,8,5,2,8,5,3,8,7,4,6,4,3,4,8",
			"name": "Number of ways to write 4*n+1 as (p-1)^2 + q^2 + r^2, where p is prime and q and r are nonnegative integers with q \u003c= r.",
			"comment": [
				"Conjecture: (i) a(n) \u003e 0 for all n, and a(n) = 1 only for n = 0, 3, 6, 23, 44.",
				"(ii) Any positive integer relatively prime to 6 can be written as (p-1)^2 + q^2 + 3*r^2, where p is prime, and q and r are integers.",
				"(iii) Let n be any nonnegative integer. Then 4*n+1 can be written as x^2 + y^2 + z^2, where x,y,z are nonnegative integers with x + y + 2*z prime; 4*n+2 can be written as x^2 + y^2 + z^2, where x,y,z are nonnegative integers with x + 3*y prime. Also, we can write 4*n+3 as 2*x^2 + y^2 + z^2, where x,y,z are nonnegative integers with 2*x+1 prime.",
				"(iv) Let n be any nonnegative integer. Then we can write 4*n+1 as x^2 + y^2 + z^2 with x,y,z integers such that x^2 + 5*y^2 + 7*z^2 prime, and write 4*n+2 as x^2 + y^2 + z^2 with x,y,z integers such that x^2 + 2*y^2 + 5*z^2 prime. Also, we can write 8*n+3 as x^2 + y^2 + z^2 with x,y,z integers such that 2*x^2 + 4*y^2 + 5*z^2 is prime.",
				"Note that by the Gauss-Legendre theorem any positive integer not of the form 4^k*(8*m+7) (k,m = 0,1,2,...) can be written as the sum of three squares."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A291123/b291123.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2016.11.008\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, J. Number Theory 175(2017), 167-190.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1701.05868\"\u003eRestricted sums of four squares\u003c/a\u003e, arXiv:1701.05868 [math.NT], 2017."
			],
			"example": [
				"a(0) = 1 since 4*0+1 = (2-1)^2 + 0^2 + 0^2 with 2 prime.",
				"a(3) = 1 since 4*3+1 = (3-1)^2 + 0^2 + 3^2 with 3 prime.",
				"a(6) = 1 since 4*6+1 = (5-1)^2 + 0^2 + 3^2 with 5 prime.",
				"a(23) = 1 since 4*23+1 = (3-1)^2 + 5^2 + 8^2 with 3 prime.",
				"a(44) = 1 since 4*44+1 = (3-1)^2 + 2^2 + 13^2 with 3 prime."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]];",
				"Do[r=0;Do[If[PrimeQ[p]\u0026\u0026SQ[4n+1-(p-1)^2-q^2],r=r+1],{p,2,Sqrt[4n+1]+1},{q,0,Sqrt[(4n+1-(p-1)^2)/2]}];Print[n,\" \",r],{n,0,80}]"
			],
			"xref": [
				"Cf. A000040, A000290, A260418, A271518, A290472, A290491, A291047."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Zhi-Wei Sun_, Aug 17 2017",
			"references": 1,
			"revision": 13,
			"time": "2017-08-18T09:55:57-04:00",
			"created": "2017-08-18T09:55:57-04:00"
		}
	]
}