{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A254926",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 254926,
			"data": "1,2,3,4,5,6,7,7,9,10,11,12,13,14,15,14,17,18,19,20,21,22,23,21,25,26,26,28,29,30,31,28,33,34,35,36,37,38,39,35,41,42,43,44,45,46,47,42,49,50,51,52,53,52,55,49,57,58,59,60,61,62,63,56,65,66,67,68,69",
			"name": "There are a(n) numbers m such that 1 \u003c= m \u003c= n and gcd(m,n) is cubefree.",
			"comment": [
				"Dirichlet convolution of A000010 and A212793.",
				"Möbius transform of A254981."
			],
			"link": [
				"Álvar Ibeas, \u003ca href=\"/A254926/b254926.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Eckford Cohen, \u003ca href=\"https://msp.org/pjm/1959/9-1/pjm-v9-n1-s.pdf\"\u003eA class of residue systems (mod r) and related arithmetical functions. I. A generalization of the Moebius function\u003c/a\u003e, Pacific J. Math. 9(1) (1959), 13-24; see Section 6 where it is function Phi_3(m).",
				"Eckford Cohen, \u003ca href=\"https://www.jstor.org/stable/2688817\"\u003eA generalized Euler phi-function\u003c/a\u003e, Math. Mag. 41 (1968), 276-279; this is function phi_3(n).",
				"V. L. Klee, Jr., \u003ca href=\"https://www.jstor.org/stable/2304963\"\u003eA generalization of Euler's phi function\u003c/a\u003e, Amer. Math. Monthly, 55(6) (1948), 358-359; this is function Phi_3(n).",
				"Paul J. McCarthy, \u003ca href=\"https://www.jstor.org/stable/2309112\"\u003eOn a certain family of arithmetic functions\u003c/a\u003e, Amer. Math. Monthly 65 (1958), 586-590; this is function T_3(n).",
				"Franz Rogel, \u003ca href=\"https://www.zobodat.at/pdf/SB-Ges-Wiss-Prag_1896_2_0001-0687.pdf\"\u003eEntwicklung einiger zahlentheoreticher Funktionen in unendliche Reihen\u003c/a\u003e, S.-B. Kgl. Bohmischen Ges. Wiss. Article XLVI/XLIV (1897), Prague (26 pages). [This paper deals with arithmetic functions, especially the Euler phi function. It contains interesting generating functions for the function phi. It was continued three years later with the next paper, which contains his function phi_k(n). As stated at the end of the volume, in the table of contents, there is a mistake in numbering the article, so two Roman numerals appear in the literature for labeling this article! - _Petros Hadjicostas_, Jul 21 2019]",
				"Franz Rogel, \u003ca href=\"https://archive.org/details/vestnkkrlovs1900kn/page/n863\"\u003eEntwicklung einiger zahlentheoreticher Funktionen in unendliche Reihen\u003c/a\u003e, S.-B. Kgl. Bohmischen Ges. Wiss. Article XXX (1900), Prague (9 pages). [This is a continuation of the previous article, which was written three years earlier and has the same title. The numbering of the equations continues from the previous paper, but this paper is the one that introduces the function phi_k(n). In our current sequence, k = 3, and a(n) = phi_3(n). Cohen (1959) refers to this paper and correctly attributes this function to F. Rogel. - _Petros Hadjicostas_, Jul 21 2019]"
			],
			"formula": [
				"Multiplicative with a(p^e) = p^e, if e\u003c3; a(p^e) = p^e - p^(e-3), otherwise.",
				"Dirichlet g.f.: zeta(s-1) / zeta(3s).",
				"Sum_{k=1..n} a(k) ~ 945*n^2 / (2*Pi^6). - _Vaclav Kotesovec_, Feb 02 2019 [This is a special case of a general result by McCarthy (1958), which was re-proved later by Cohen (1968). - _Petros Hadjicostas_, Jul 20 2019]",
				"a(n) = Sum_{v \u003e= 1} mu(v) * [n, v^3] * (n/v^3), where [n, v^3] = 1 when n is a multiple of v^3, and = 0 otherwise. [This is Eq. (53) in Rogel (1900) and Eq. (6.1) in Cohen (1959).] - _Petros Hadjicostas_, Jul 21 2019",
				"From _Richard L. Ollerton_, May 07 2021: (Start)",
				"a(n) = Sum_{d|n} phi(d)*A212793(n/d), where phi = A000010.",
				"a(n) = Sum_{k=1..n} A212793(gcd(n,k)).",
				"a(n) = Sum_{k=1..n} A212793(n/gcd(n,k))*phi(gcd(n,k))/phi(n/gcd(n,k)). (End)",
				"G.f.: Sum_{k\u003e=1} mu(k) * x^(k^3) / (1 - x^(k^3))^2. - _Ilya Gutkovskiy_, Aug 20 2021"
			],
			"mathematica": [
				"f[p_, e_] := If[e \u003c 3, p^e, p^e - p^(e - 3)]; a[1] = 1; a[n_] := Times @@ f @@@ FactorInteger[n]; Array[a, 100] (* _Amiram Eldar_, Sep 17 2020 *)"
			],
			"program": [
				"(PARI) a(n) = {f = factor(n); for (i=1, #f~, if ((e=f[i,2])\u003e=3, f[i,1] = f[i,1]^e - f[i,1]^(e-3); f[i,2]=1);); factorback(f);} \\\\ _Michel Marcus_, Feb 10 2015"
			],
			"xref": [
				"Cf. A000010, A212793, A063659, A254981.",
				"Row 3 of A309287."
			],
			"keyword": "mult,nonn",
			"offset": "1,2",
			"author": "_Álvar Ibeas_, Feb 10 2015",
			"references": 4,
			"revision": 57,
			"time": "2021-08-20T09:02:23-04:00",
			"created": "2015-03-16T20:33:47-04:00"
		}
	]
}