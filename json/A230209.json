{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A230209",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 230209,
			"data": "1,-6,15,-20,15,-6,1,1,-5,9,-5,-5,9,-5,1,1,-4,4,4,-10,4,4,-4,1,1,-3,0,8,-6,-6,8,0,-3,1,1,-2,-3,8,2,-12,2,8,-3,-2,1,1,-1,-5,5,10,-10,-10,10,5,-5,-1,1,1,0,-6,0,15,0,-20,0,15,0,-6,0,1,1,1,-6",
			"name": "Trapezoid of dot products of row 6 (signs alternating) with sequential 7-tuples read by rows in Pascal's triangle A007318: T(n,k) is the linear combination of the 7-tuples (C(6,0), -C(6,1), ..., -C(6,5), C(6,6)) and (C(n-1,k-6), C(n-1,k-5), ..., C(n-1,k)), n \u003e= 1, 0 \u003c= k \u003c= n+5.",
			"comment": [
				"The array is trapezoidal rather than triangular because C(n,k) is not uniquely defined for all negative n and negative k.",
				"Row sums are 0.",
				"Coefficients of (x-1)^6 (x+1)^(n-1)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A230209/b230209.txt\"\u003eRows n=1..50 of trapezoid, flattened\u003c/a\u003e",
				"Isabel Cação, Helmuth R. Malonek, Maria Irene Falcão, Graça Tomaz, \u003ca href=\"https://www.emis.de/journals/JIS/VOL21/Falcao/falcao2.html\"\u003eCombinatorial Identities Associated with a Multidimensional Polynomial Sequence\u003c/a\u003e, J. Int. Seq., Vol. 21 (2018), Article 18.7.4."
			],
			"formula": [
				"T(n,k) = Sum_{i=0..n+m-1} (-1)^(i+m)*C(m,i)*C(n-1,k-i), n \u003e= 1, with T(n,0) = (-1)^m and m=6."
			],
			"example": [
				"Trapezoid begins:",
				"  1, -6, 15, -20,  15,  -6,   1;",
				"  1, -5,  9,  -5,  -5,   9,  -5,  1;",
				"  1, -4,  4,   4, -10,   4,   4, -4,  1;",
				"  1, -3,  0,   8,  -6,  -6,   8,  0, -3,  1;",
				"  1, -2, -3,   8,   2, -12,   2,  8, -3, -2,  1;",
				"  1, -1, -5,   5,  10, -10, -10, 10,  5, -5, -1, 1;",
				"  1,  0, -6,   0,  15,   0, -20,  0, 15,  0, -6, 0, 1;",
				"etc."
			],
			"mathematica": [
				"Flatten[Table[CoefficientList[(x - 1)^6 (x + 1)^n, x], {n, 0, 7}]] (* _T. D. Noe_, Oct 25 2013 *)",
				"m=6; Table[If[k == 0, (-1)^m, Sum[(-1)^(j+m)*Binomial[m, j]*Binomial[n-1, k-j], {j, 0, n+m-1}]], {n, 1, 10}, {k, 0, n+m-1}]//Flatten (* _G. C. Greubel_, Nov 28 2018 *)"
			],
			"program": [
				"(PARI) m=6; for(n=1, 10, for(k=0, n+m-1, print1(if(k==0, (-1)^m, sum(j=0, n+m-1, (-1)^(j+m)*binomial(m,j)*binomial(n-1,k-j))), \", \"))) \\\\ _G. C. Greubel_, Nov 28 2018",
				"(MAGMA) m:=6; [[k le 0 select (-1 )^m else (\u0026+[(-1)^(j+m)* Binomial(m,j) *Binomial(n-1,k-j): j in [0..(n+m-1)]]): k in [0..(n+m-1)]]: n in [1..10]]; // _G. C. Greubel_, Nov 28 2018",
				"(Sage) m=6; [[sum((-1)^(j+m)*binomial(m,j)*binomial(n-1,k-j) for j in range(n+m)) for k in range(n+m)] for n in (1..10)] # _G. C. Greubel_, Nov 28 2018"
			],
			"xref": [
				"Using row j of the alternating Pascal triangle as generator: A007318 (j=0), A008482 and A112467 (j=1 after the first term in each), A182533 (j=2 after the first two rows), A230206-A230208 (j=3 to j=5), A230210-A230212 (j=7 to j=9)."
			],
			"keyword": "easy,sign,tabf",
			"offset": "1,2",
			"author": "_Dixon J. Jones_, Oct 12 2013",
			"references": 3,
			"revision": 26,
			"time": "2018-11-29T01:23:50-05:00",
			"created": "2013-10-20T23:13:10-04:00"
		}
	]
}