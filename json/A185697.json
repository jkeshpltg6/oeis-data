{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A185697",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 185697,
			"data": "1,1,3,1,12,42,44,9,1,27,270,1278,3078,3888,2592,918,189,21,1,48,936,9744,60132,232560,585576,989424,1151910,949136,571608,261360,93540,26160,5400,720,45,1,75,2400,43400,496200,3812640,20493900,79452300,227904675,494715775,829703340,1096775400,1165877050,1017460800,744417900,465554460,252490875,119428125,49008500,17175000,5015100,1180200,213900,27900,2325,93",
			"name": "Array to be read by rows: Number of ways of placing n rods of length L in a LxLxL simple cubic lattice without any two rods intersecting. (Consecutive rows are for L\u003e=0; in each row, 0\u003c=n\u003c=L^2.)",
			"comment": [
				"A rod is 'placed' into a LxLxL lattice by occupying a row of L sites in the lattice, along any of the 3 orthogonal directions. Rods cannot intersect; that is, a site cannot belong to more than one rod.",
				"This appears in connection to the degeneracy of the ground states of certain models in lattice statistics.",
				"For each L, there can be at most L^2 rods in the cubic lattice. The terms in the data section cover the range 0\u003c=L\u003c=5. The adjoining b-file contains all terms up to L=16.",
				"The (ordinary) generating function f(L,x) is also formally the partition function for the statistics of the associated lattice model, if one regards the expansion variable x as the 'activity' (the physical interpretation of this parameter depends on the context in which the model appears)."
			],
			"link": [
				"Marcos Simoes, \u003ca href=\"/A185697/b185697.txt\"\u003eTable of n, a(n) for n = 0..1512\u003c/a\u003e"
			],
			"formula": [
				"G.f.: f(L,x) = Sum_{a=0..L} Sum_{b=0..L} Sum_{c=0..L} ( binomial(L,a) * binomial(L,b) * binomial(L,c) * ((1+x)^(L-c)-1)^a * ((1+x)^(L-a)-1)^b * ((1+x)^(L-b)-1)^c )."
			],
			"example": [
				"The values for L=0,1,2,3 read, respectively:",
				"1",
				"1 3",
				"1 12 42 44 9",
				"1 27 270 1278 3078 3888 2592 918 189 21"
			],
			"mathematica": [
				"Unprotect[Power]; Power[0,0]=1; Protect[Power]; (* This is necessary so that 0^0=1. *)",
				"(* This defines the generating function: *)",
				"f[L_,x_]:=Sum[Binomial[L, a]*Binomial[L, b]*Binomial[L, c]*((1 + x)^(L - c) - 1)^a*((1 + x)^(L - a) - 1)^b*((1 + x)^(L - b) - 1)^c, {a, 0, L}, {b, 0, L}, {c, 0, L}]",
				"(* The following computes a specific value of the sequence: *)",
				"a[L_,n_]=Coefficient[f[L,x],x,n]",
				"(* The array of coefficients up to L=16 may be generated by: *)",
				"Table[CoefficientList[f[L,x],x],{L,0,16}]"
			],
			"xref": [
				"Row sums of this sequence appear in A185702.",
				"Row L=3 gives A181323."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_Marcos Simoes_, Feb 10 2011",
			"references": 3,
			"revision": 14,
			"time": "2016-11-14T07:21:50-05:00",
			"created": "2011-02-01T12:43:51-05:00"
		}
	]
}