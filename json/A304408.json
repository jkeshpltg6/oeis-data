{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A304408",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 304408,
			"data": "1,2,4,3,8,8,12,4,6,16,20,12,24,24,32,5,32,12,36,24,48,40,44,16,12,48,8,36,56,64,60,6,80,64,96,18,72,72,96,32,80,96,84,60,48,88,92,20,18,24,128,72,104,16,160,48,144,112,116,96,120,120,72,7,192,160,132,96,176,192",
			"name": "If n = Product (p_j^k_j) then a(n) = Product ((p_j - 1)*(k_j + 1)).",
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A304408/b304408.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Ilya Gutkovskiy, \u003ca href=\"/A304408/a304408.jpg\"\u003eScatter plot of a(n) up to n=50000\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#prime_indices\"\u003eIndex entries for sequences computed from indices in prime factorization\u003c/a\u003e",
				"\u003ca href=\"/index/Eu#epf\"\u003eIndex entries for sequences computed from exponents in factorization of n\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A000005(n)*abs(A023900(n)) = A000005(n)*A173557(n) = A000005(n)*A000010(A007947(n)).",
				"a(p^k) = (p - 1)*(k + 1) where p is a prime and k \u003e 0.",
				"a(n) = 2^omega(n)*phi(n) if n is a squarefree (A005117), where omega() = A001221 and phi() = A000010."
			],
			"example": [
				"a(20) = a(2^2*5) = (2 - 1)*(2 + 1) * (5 - 1)*(1 + 1) = 24."
			],
			"maple": [
				"a:= n-\u003e mul((i[1]-1)*(i[2]+1), i=ifactors(n)[2]):",
				"seq(a(n), n=1..80);  # _Alois P. Heinz_, Jan 05 2021"
			],
			"mathematica": [
				"a[n_] := Times @@ ((#[[1]] - 1) (#[[2]] + 1) \u0026 /@ FactorInteger[n]); a[1] = 1; Table[a[n], {n, 70}]",
				"Table[DivisorSigma[0, n] EulerPhi[Last[Select[Divisors[n], SquareFreeQ]]], {n, 70}]"
			],
			"program": [
				"(PARI) a(n)={my(f=factor(n)); prod(i=1, #f~, my(p=f[i,1], e=f[i,2]); (p-1)*(e+1))} \\\\ _Andrew Howroyd_, Jul 24 2018"
			],
			"xref": [
				"Cf. A000005, A000010, A000026, A000040, A000302 (numbers n such that a(n) is odd), A001221, A006093, A007947, A023900, A034444, A059975, A062355, A173557, A304407, A304409, A304411, A304412."
			],
			"keyword": "nonn,mult",
			"offset": "1,2",
			"author": "_Ilya Gutkovskiy_, May 12 2018",
			"references": 6,
			"revision": 13,
			"time": "2021-01-05T22:29:51-05:00",
			"created": "2018-05-12T14:40:58-04:00"
		}
	]
}