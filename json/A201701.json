{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A201701",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 201701,
			"data": "1,1,0,2,1,0,4,3,0,0,8,8,1,0,0,16,20,5,0,0,0,32,48,18,1,0,0,0,64,112,56,7,0,0,0,0,128,256,160,32,1,0,0,0,0,256,576,432,120,9,0,0,0,0,0,512,1280,1120,400,50,1,0,0,0,0,0",
			"name": "Riordan triangle ((1-x)/(1-2x), x^2/(1-2x)).",
			"comment": [
				"Triangle T(n,k), read by rows, given by (1,1,0,0,0,0,0,0,0,...) DELTA (0,1,-1,0,0,0,0,0,0,0,...) where DELTA is the operator defined in A084938.",
				"Skewed version of triangle in A200139.",
				"Triangle without zeros: A207537.",
				"For the version with negative odd numbered columns, which is Riordan (((1-x)/(1-2*x), -x^2/(1-2*x) see comments on A028297 and A039991. - _Wolfdieter Lang_, Aug 06 2014",
				"This is an example of a stretched Riordan array in the terminology of Section 2 of Corsani et al. - _Peter Bala_, Jul 14 2015"
			],
			"link": [
				"C. Corsani, D. Merlini, R. Sprugnoli, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(97)00110-6\"\u003eLeft-inversion of combinatorial sums\u003c/a\u003e Discrete Mathematics, 180 (1998) 107-122."
			],
			"formula": [
				"T(n,k) = 2*T(n-1,k) + T(n-2,k-1) with T(0,0) = T(1,0) = 1 , T(1,1) = 0 and T(n,k)=0 for k\u003c0 or for n\u003ck .",
				"Sum_ {k, 0\u003c=k\u003c=n} T(n,k)^2 = A002002(n) for n\u003e0.",
				"Sum_ {k, 0\u003c=k\u003c=n} T(n,k)*x^k = A138229(n), A006495(n), A138230(n), A087455(n), A146559(n), A000012(n), A011782(n), A001333(n), A026150(n), A046717(n), A084057(n), A002533(n), A083098(n), A084058(n), A003665(n), A002535(n), A133294(n), A090042(n), A125816(n), A133343(n), A133345(n), A120612(n), A133356(n), A125818(n) for x = -6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17 respectively.",
				"G.f.: (1-x)/(1-2*x-y*x^2). - _Philippe Deléham_, Mar 03 2012",
				"From _Peter Bala_, Jul 14 2015: (Start)",
				"Factorizes as A034839 * A007318 = (1/(1 - x), x^2/(1 - x)^2) * (1/(1 - x), x/(1 - x)) as a product of Riordan arrays.",
				"T(n,k) = Sum_{i = k..floor(n/2)} binomial(n,2*i) *binomial(i,k). (End)"
			],
			"example": [
				"The triangle T(n,k) begins :",
				"n\\k      0     1     2     3     4    5   6  7 8 9 10 11 12 13 14 15 ...",
				"0:       1",
				"1:       1     0",
				"2:       2     1     0",
				"3:       4     3     0     0",
				"4:       8     8     1     0     0",
				"5:      16    20     5     0     0    0",
				"6:      32    48    18     1     0    0   0",
				"7:      64   112    56     7     0    0   0  0",
				"8:     128   256   160    32     1    0   0  0 0",
				"9:     256   576   432   120     9    0   0  0 0 0",
				"10:    512  1280  1120   400    50    1   0  0 0 0  0",
				"11:   1024  2816  2816  1232   220   11   0  0 0 0  0  0",
				"12:   2048  6144  6912  3584   840   72   1  0 0 0  0  0  0",
				"13:   4096 13312 16640  9984  2912  364  13  0 0 0  0  0  0  0",
				"14:   8192 28672 39424 26880  9408 1568  98  1 0 0  0  0  0  0  0",
				"15:  16384 61440 92160 70400 28800 6048 560 15 0 0  0  0  0  0  0  0",
				"...  reformatted and extended. - _Wolfdieter Lang_, Aug 06 2014",
				"-------------------------------------------------------------------------"
			],
			"mathematica": [
				"(* The function RiordanArray is defined in A256893. *)",
				"RiordanArray[(1 - #)/(1 - 2 #)\u0026, #^2/(1 - 2 #)\u0026, 11] // Flatten (* _Jean-François Alcover_, Jul 16 2019 *)"
			],
			"xref": [
				"Columns include A011782, A001792, A001793, A001794, A006974, A006975, A006976.",
				"Diagonals sums are in A052980.",
				"Cf. A028297, A081265, A124182, A131577, A039991 (zero-columns deleted, unsigned and zeros appended).",
				"Cf. A098158, A200139, A207537.",
				"Cf. A028297 (signed version, zeros deleted). Cf. A034839."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,4",
			"author": "_Philippe Deléham_, Dec 03 2011",
			"ext": [
				"Name changed, keyword:easy added, crossrefs A028297 and A039991 added, and g.f. corrected by _Wolfdieter Lang_, Aug 06 2014"
			],
			"references": 8,
			"revision": 31,
			"time": "2019-07-16T10:32:49-04:00",
			"created": "2011-12-04T15:27:09-05:00"
		}
	]
}