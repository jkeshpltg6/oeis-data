{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A083769",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 83769,
			"data": "2,6,8,12,16,10,4,30,26,22,24,14,50,42,18,64,46,60,32,36,20,34,28,108,48,44,68,282,90,54,76,62,180,66,132,86,74,38,58,106,120,52,244,94,100,82,138,156,98,72,172,150,248,154,166,114,162,126,124,208,222,324,212",
			"name": "a(1)=2; for n \u003e= 2, a(n) = smallest even number such that a(1)*a(2)*...*a(n) + 1 is prime.",
			"comment": [
				"Is this a permutation of the even numbers?",
				"For any even positive integers a_1, a_2, ..., a_n, there are infinitely many even positive integers t such that a_1 a_2 ... a_n t + 1 is prime: this follows from Dirichlet's theorem on primes in arithmetic progressions.  As far as I know there is no guarantee that the sequence defined here leads to a permutation of the even numbers, i.e. there might be some even integer that never appears in the sequence. However, if the partial products a_1 ... a_n grow like 2^n n!, heuristically the probability of a_1 ... a_n t + 1 being prime is on the order of 1/log(a_1 ... a_n) ~ 1/(n log n), and since sum_n 1/(n log n) diverges we might expect that there should be infinitely many n for which some a_1 ... a_n t + 1 is prime, and thus every even integer should occur. - _Robert Israel_, Dec 20 2012"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A083769/b083769.txt\"\u003eTable of n, a(n) for n = 1..200\u003c/a\u003e"
			],
			"example": [
				"2+1=3, 2*6+1=13, 2*6*8+1=97, 2*6*8*12+1=1153, etc. are primes.",
				"After 200 terms the prime is",
				"224198929826405912196464851358435330956778558123234657623126\\",
				"069546460095464785674042966210907411841359152393200850271694\\",
				"899718487202330385432243578646330245831108247815285116235792\\",
				"875886417750289946171599027675234787802312202111702704952223\\",
				"563058999855839876391430601719636148884060097930252529666254\\",
				"756431522481046758186320659298713737639441014068272279177710\\",
				"551232067814381240340990584869121776471244800000000000000000\\",
				"00000000000000000000000000000 (449 digits). - _Robert Israel_, Dec 21 2012"
			],
			"maple": [
				"  N := 200: # number of terms desired",
				"P := 2:",
				"a[1] := 2:",
				"C := {seq(2*j, j = 2 .. 10)}:",
				"Cmax := 20:",
				"for n from 2 to N do",
				"   for t in C do",
				"      if isprime(t*P+1) then",
				"        a[n]:= t;",
				"        P:= t*P;",
				"        C:= C minus {t};",
				"        break;",
				"      end if;",
				"   end do;",
				"   while not assigned(a[n]) do",
				"     t0:= Cmax+2;",
				"     Cmax:= 2*Cmax;",
				"     C:= C union {seq(j, j=t0 .. Cmax, 2)};",
				"     for t from t0 to Cmax by 2 do",
				"       if isprime(t*P+1) then",
				"         a[n]:= t;",
				"         P:= t*P;",
				"         C:= C minus {t};",
				"         break;",
				"       end if",
				"     end do;",
				"   end do;",
				"end do;",
				"[seq(a[n],n=1..N)];"
			],
			"mathematica": [
				"f[s_List] := Block[{k = 2, p = Times @@ s}, While[ MemberQ[s, k] || !PrimeQ[k*p + 1], k += 2]; Append[s, k]]; Nest[f, {2}, 62] (* _Robert G. Wilson v_, Dec 24 2012 *)"
			],
			"xref": [
				"Cf. A036013, A046966, A046972, A051957, A073673, A073674, A083769, A083770, A083771, A084401, A084402, A084724, A087338."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Amarnath Murthy_ and Meenakshi Srikanth (menakan_s(AT)yahoo.com), May 06 2003",
			"ext": [
				"More terms from _David Wasserman_, Nov 23 2004",
				"Edited by _N. J. A. Sloane_, Dec 20 2012",
				"Comment edited, Maple code and additional terms by _Robert Israel_, Dec 20 2012"
			],
			"references": 5,
			"revision": 40,
			"time": "2014-08-23T13:06:34-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}