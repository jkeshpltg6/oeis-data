{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A175639",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 175639,
			"data": "6,7,8,2,3,4,4,9,1,9,1,7,3,9,1,9,7,8,0,3,5,5,3,8,2,7,9,4,8,2,8,9,4,8,1,4,0,9,6,3,3,2,2,3,9,1,8,9,4,4,0,1,0,3,0,3,6,4,6,0,4,1,5,9,6,4,9,8,3,3,7,0,7,4,0,1,2,3,2,3,3,2,1,3,7,6,2,1,2,2,9,3,3,4,8,4,6,1,6,8,8,8,3,2,8",
			"name": "Decimal expansion of Product_{p prime} (1-3/p^3+2/p^4+1/p^5-1/p^6).",
			"comment": [
				"Equals (49/64)*(668/729)*(15304/15625)*(116724/117649)*... inserting p= A000040 = 2, 3, 5, 7.. into the factor. Slightly larger than Product_{p=primes} (1-3/p^3) = 0.534566872085103888416775..."
			],
			"link": [
				"Steven R. Finch, \u003ca href=\"/A000924/a000924.pdf\"\u003eClass number theory\u003c/a\u003e, 2005. [Cached copy, with permission of the author]",
				"Steven R. Finch, \u003ca href=\"https://doi.org/10.1017/9781316997741\"\u003eMathematical Constants II\u003c/a\u003e, Encyclopedia of Mathematics and Its Applications, Cambridge University Press, Cambridge, 2018, p. 85.",
				"Takashi Taniguchi, \u003ca href=\"https://aif.centre-mersenne.org/item/?id=AIF_2008__58_2_625_0\"\u003eA mean value theorem for the square of class number times regulator of quadratic extensions\u003c/a\u003e, Annales de l'institut Fourier. Vol. 58, No. 2 (2008), pp. 625-670; \u003ca href=\"https://arxiv.org/abs/math/0410531\"\u003earXiv preprint\u003c/a\u003e, arXiv:math/0410531 [math.NT], 2004-2006.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PrimeProducts.html\"\u003ePrime Products\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/TaniguchisConstant.html\"\u003eTaniguchi's Constant\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Euler_product\"\u003eEuler Product\u003c/a\u003e."
			],
			"example": [
				"0.678234491917391978035..."
			],
			"maple": [
				"read(\"transforms\") : efact := 1-3/p^3+2/p^4+1/p^5-1/p^6 ; Digits := 130 : tm := 310 : subs (p=1/x,1/efact) ; taylor(%,x=0,tm) : L := [seq(coeftayl(%,x=0,i),i=1..tm-1)] : Le := EULERi(L) : x := 1.0 :",
				"for i from 2 to nops(Le) do x := x/evalf(Zeta(i))^op(i,Le) ; x := evalf(x) ; print(x) ; end do:"
			],
			"mathematica": [
				"digits = 105; $MaxExtraPrecision = 400; m0 = 1000; dm = 100; Clear[s];",
				"LR = LinearRecurrence[{0, 0, 3, -2, -1, 1}, {0, 0, -9, 8, 5, -33}, 2 m0];",
				"r[n_Integer] := LR[[n]]; s[m_] := s[m] = NSum[r[n] PrimeZetaP[n]/n, {n, 3, m}, NSumTerms -\u003e m0, WorkingPrecision -\u003e 400] // Exp; s[m0]; s[m = m0 + dm]; While[RealDigits[s[m], 10, digits][[1]] != RealDigits[s[m-dm], 10, digits][[1]], Print[m]; m = m+dm]; RealDigits[s[m], 10, digits][[1]] (* _Jean-François Alcover_, Apr 15 2016 *)"
			],
			"program": [
				"(PARI) prodeulerrat(1-3/p^3+2/p^4+1/p^5-1/p^6) \\\\ _Amiram Eldar_, Mar 04 2021"
			],
			"xref": [
				"Cf. A256392, A330523."
			],
			"keyword": "cons,nonn",
			"offset": "0,1",
			"author": "_R. J. Mathar_, Aug 01 2010",
			"ext": [
				"More digits from _Jean-François Alcover_, Apr 15 2016"
			],
			"references": 3,
			"revision": 31,
			"time": "2021-06-13T05:13:04-04:00",
			"created": "2010-08-27T03:00:00-04:00"
		}
	]
}