{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A198325",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 198325,
			"data": "1,2,1,2,3,2,1,3,1,3,2,3,4,2,4,2,1,4,3,2,1,4,1,4,3,1,4,2,5,3,1,4,4,3,2,5,2,4,3,5,2,1,5,3,5,3,2,1,5,4,2,5,1,6,4,2,5,3,1,6,3,5,2,5,4,2,1,6,3,1,5,4,3,2,1,5,6,4,2,1,5,3,2,6,4,1,6,2,5,4,1,5,3,6,4,1,6,2,1,5,4,3,1,6,3,5,4,2,6,3,2,1,7,4,1",
			"name": "Irregular triangle read by rows: T(n,k) is the number of directed paths of length k (k\u003e=1) in the rooted tree having Matula-Goebel number n (n\u003e=2).",
			"comment": [
				"A directed path of length k in a rooted tree is a sequence of k+1 vertices v[1], v[2], ..., v[k], v[k+1], such that v[j] is a child of v[j-1] for j = 2,3,...,k+1.",
				"The Matula-Goebel number of a rooted tree can be defined in the following recursive manner: to the one-vertex tree there corresponds the number 1; to a tree T with root degree 1 there corresponds the t-th prime number, where t is the Matula-Goebel number of the tree obtained from T by deleting the edge emanating from the root; to a tree T with root degree m\u003e=2 there corresponds the product of the Matula-Goebel numbers of the m branches of T.",
				"Number of entries in row n is A109082(n) (n=2,3,...).",
				"Sum of entries in row n is A196047(n).",
				"Sum(k*T(n,k),k\u003e=1)=A198326(n)."
			],
			"reference": [
				"I. Gutman and Yeong-Nan Yeh, Deducing properties of trees from their Matula numbers, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"D. W. Matula, A natural rooted tree enumeration by prime factorization, SIAM Review, 10, 1968, 273."
			],
			"link": [
				"F. Goebel, \u003ca href=\"http://dx.doi.org/10.1016/0095-8956(80)90049-0\"\u003eOn a 1-1-correspondence between rooted trees and natural numbers\u003c/a\u003e, J. Combin. Theory, B 29 (1980), 141-143.",
				"I. Gutman and A. Ivic, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(95)00182-V\"\u003eOn Matula numbers\u003c/a\u003e, Discrete Math., 150, 1996, 131-142."
			],
			"formula": [
				"We give the recursive construction of the row generating polynomials P(n)=P(n,x): P(1)=0; if n=p(t) (=the t-th prime), then P(n)=x*E(n)+x*P(t), where E denotes number of edges (computed recursively and programmed in A196050); if n=rs (r,s\u003e=2), then P(n)=P(r)+P(s) (2nd Maple program yields P(n))."
			],
			"example": [
				"T(7,1)=3 and T(7,2)=2 because the rooted tree with Matula-Goebel number 7 is the rooted tree Y, having 3 directed paths of length 1 (the edges) and 2 directed paths of length 2.",
				"Triangle starts:",
				"1;",
				"2,1;",
				"2;",
				"3,2,1;",
				"3,1;",
				"3,2;",
				"3;"
			],
			"maple": [
				"with(numtheory): P := proc (n) local r, s, E: r := proc (n) options operator, arrow: op(1, factorset(n)) end proc: s := proc (n) options operator, arrow: n/r(n) end proc: E := proc (n) if n = 1 then 0 elif bigomega(n) = 1 then 1+E(pi(n)) else E(r(n))+E(s(n)) end if end proc: if n = 1 then 0 elif bigomega(n) = 1 then sort(expand(x*E(n)+x*P(pi(n)))) else sort(P(r(n)) +P(s(n))) end if end proc: T := proc (n, k) options operator, arrow: coeff(P(n), x, k) end proc: for n from 2 to 15 do seq(T(n, k), k = 1 .. degree(P(n))) end do; # yields sequence in triangular form",
				"P(987654321); # yields P(987654321)"
			],
			"xref": [
				"Cf. A109082, A196047, A196050, A198326."
			],
			"keyword": "nonn,tabf",
			"offset": "2,2",
			"author": "_Emeric Deutsch_, Nov 02 2011",
			"references": 1,
			"revision": 14,
			"time": "2012-03-30T17:36:29-04:00",
			"created": "2011-11-02T18:13:33-04:00"
		}
	]
}