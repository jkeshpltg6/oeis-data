{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A321118",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 321118,
			"data": "0,1,1,3,10,3,4,11,11,4,11,32,26,32,11,15,43,37,37,43,15,41,118,100,106,100,118,41,56,161,137,143,143,137,161,56,153,440,374,392,386,392,374,440,153,209,601,511,535,529,529,535,511,601,209",
			"name": "T(n,k) = A321119(n) - (-1)^k*A321119(n-2*k)/2 for 0 \u003c k \u003c n, with T(0,0) = 0 and T(n,0) = T(n,n) = A002530(n+1) for n \u003e 0, triangle read by rows; unreduced numerator of the weights of Holladay-Sard's quadrature formula.",
			"comment": [
				"The n-th row common denominator is factorized out and is given by A321119(n).",
				"Given a continuous function f over the interval [0,n], the best quadrature formula in the sense of Holladay-Sard is given by Integral_{x=0..n} f(x) dx = Sum_{k=0..n} T(n,k)*f(k)/A321119(n). The formula is exact if f belongs to the class of natural cubic splines."
			],
			"reference": [
				"Harold J. Ahlberg, Edwin N. Nilson and Joseph L. Walsh, The Theory of Splines and Their Applications, Academic Press, 1967. See p. 47, Table 2.5.2."
			],
			"link": [
				"Franck Maminirina Ramaharo, \u003ca href=\"/A321118/b321118.txt\"\u003eRows n = 0..150 of triangle, flattened\u003c/a\u003e",
				"Harold J. Ahlberg, Edwin N. Nilson and Joseph L. Walsh, \u003ca href=\"https://doi.org/10.1016/S0076-5392(08)61988-8\"\u003eChapter II The Cubic Spline\u003c/a\u003e, Mathematics in Science and Engineering Volume 38 (1967), p. 9-74.",
				"John C. Holladay, \u003ca href=\"https://doi.org/10.1090/S0025-5718-1957-0093894-6\"\u003eA smoothest curve approximation\u003c/a\u003e, Math. Comp. Vol. 11 (1957), 233-243.",
				"Peter Köhler, \u003ca href=\"https://doi.org/10.1007/BF02575942\"\u003eOn the weights of Sard's quadrature formulas\u003c/a\u003e, CALCOLO Vol. 25 (1988), 169-186.",
				"Leroy F. Meyers and Arthur Sard, \u003ca href=\"https://doi.org/10.1002/sapm1950291118\"\u003eBest approximate integration formulas\u003c/a\u003e, J. Math. Phys. Vol. 29 (1950), 118-123.",
				"Arthur Sard, \u003ca href=\"https://doi.org/10.2307/2372095\"\u003eBest approximate integration formulas; best approximation formulas\u003c/a\u003e, American Journal of Mathematics Vol. 71 (1949), 80-91.",
				"Isaac J. Schoenberg, \u003ca href=\"https://projecteuclid.org/euclid.bams/1183525790\"\u003eSpline interpolation and best quadrature formulae\u003c/a\u003e, Bull. Amer. Math. Soc. Vol. 70 (1964), 143-148.",
				"Frans Schurer, \u003ca href=\"https://research.tue.nl/en/publications/on-natural-cubic-splines-with-an-application-to-numerical-integra\"\u003eOn natural cubic splines, with an application to numerical integration formulae\u003c/a\u003e, EUT report. WSK, Dept. of Mathematics and Computing Science Vol. 70-WSK-04 (1970), 1-32."
			],
			"formula": [
				"T(n,k)/A321119(n) = (alpha^(n + 1) - (-alpha)^(-(n + 1)))/(2*sqrt(6)*(alpha^n + (-alpha)^(-n))) if k = 0 or k = n, and 1 - (-1)^k*(alpha^(n - 2*k) + (-alpha)^(2*k - n))/(2*(alpha^n + (-alpha)^(-n))) if 0 \u003c k \u003c n, where alpha = (sqrt(2) + sqrt(6))/2.",
				"T(n,k) = T(n,n-k).",
				"T(n,k) = 4*T(n-2,k) - T(n-4,k), n \u003e= k + 4.",
				"T(2*n+2,k)*A001834(n+1) = A001834(n)*T(2*n,k) + 2*A003500(n)*T(2*n+1,k) for k \u003c 2*n.",
				"T(2*n+3,k)*A003500(n+1) = A003500(n)*T(2*n+1,k) + 2*A001834(n+1)*T(2*n+2,k) for k \u003c 2*n + 1.",
				"Sum_{k=0..n} T(n,k)/A321119(n) = n."
			],
			"example": [
				"Triangle begins (denominator is factored out):",
				"    0;                                                 1/4",
				"    1,   1;                                            1/2",
				"    3,  10,   3;                                       1/8",
				"    4,  11,  11,   4;                                  1/10",
				"   11,  32,  26,  32,  11;                             1/28",
				"   15,  43,  37,  37,  43,  15;                        1/38",
				"   41, 118, 100, 106, 100, 118,  41;                   1/104",
				"   56, 161, 137, 143, 143, 137, 161,  56;              1/142",
				"  153, 440, 374, 392, 386, 392, 374, 440, 153;         1/388",
				"  209, 601, 511, 535, 529, 529, 535, 511, 601, 209;    1/530",
				"  ...",
				"If f is a continuous function over the interval [0,3], then the quadrature formula yields Integral_{x=0..3} f(x) d(x) = (1/10)*(4*f(0) + 11*f(1) + 11*f(2) + 4*f(3))."
			],
			"mathematica": [
				"alpha = (Sqrt[2] + Sqrt[6])/2; T[0,0] = 0;",
				"T[n_, k_] := If[n \u003e 0 \u0026\u0026 k == 0 || k == n, (alpha^(n + 1) - (-alpha)^(-(n + 1)))/(2*Sqrt[6]*(alpha^n + (-alpha)^(-n))), 1 - (-1)^k*(alpha^(n - 2*k) + (-alpha)^(2*k - n))/(2*(alpha^n + (-alpha)^(-n)))];",
				"a321119[n_] := 2^(-Floor[(n - 1)/2])*((1 - Sqrt[3])^n + (1 + Sqrt[3])^n);",
				"Table[FullSimplify[a321119[n]*T[n, k]],{n, 0, 10}, {k, 0, n}] // Flatten"
			],
			"program": [
				"(Maxima)",
				"(b[0] : 0, b[1] : 1, b[2] : 1, b[3] : 3, b[n] := 4*b[n-2] - b[n-4])$ /* A002530 */",
				"d(n) := 2^(-floor((n - 1)/2))*((1 - sqrt(3))^n + (1 + sqrt(3))^n) $ /* A321119 */",
				"T(n, k) := if n = 0 and k = 0 then 0 else if n \u003e 0 and k = 0 or k = n then b[n + 1] else d(n) - (-1)^k*d(n - 2*k)/2$",
				"create_list(ratsimp(T(n, k)), n, 0, 10, k, 0, n);"
			],
			"xref": [
				"Cf. A093735, A093736, A100641, A002176, A100640, A321119, A321120, A321121, A321122."
			],
			"keyword": "nonn,easy,tabl,frac",
			"offset": "0,4",
			"author": "_Franck Maminirina Ramaharo_, Nov 01 2018",
			"references": 4,
			"revision": 24,
			"time": "2019-01-05T05:20:20-05:00",
			"created": "2018-12-01T08:59:01-05:00"
		}
	]
}