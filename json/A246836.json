{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A246836",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 246836,
			"data": "1,2,-2,-4,3,2,-6,-4,4,6,-4,-4,7,8,-2,-8,8,4,-10,-4,4,10,-10,-8,9,4,-6,-12,8,6,-10,-12,4,14,-8,-4,16,10,-8,-8,9,10,-12,-12,8,12,-12,-4,20,10,-6,-20,8,6,-10,-12,8,20,-18,-8,11,12,-12,-16,8,6,-20",
			"name": "Expansion of phi(x) * psi(-x^2)^2 in powers of x where phi(), psi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A246836/b246836.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-1/2) * eta(q^2)^7 * eta(q^8)^2 / (eta(q)^2 * eta(q^4)^4) in powers of q.",
				"Euler transform of period 8 sequence [ 2, -5, 2, -1, 2, -5, 2, -3, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (64 t)) = 32 (t/i)^(3/2) g(t) where q = exp(2 Pi i t) and g() is the g.f. for A246835.",
				"a(n) = (-1)^floor(n/2) * A045828(n). a(n) = (-1)^n * A246815(n).",
				"a(2*n) = A246835(n). a(2*n + 1) = 2 * A246833(n)."
			],
			"example": [
				"G.f. = 1 + 2*x - 2*x^2 - 4*x^3 + 3*x^4 + 2*x^5 - 6*x^6 - 4*x^7 + 4*x^8 + ...",
				"G.f. = q + 2*q^3 - 2*q^5 - 4*q^7 + 3*q^9 + 2*q^11 - 6*q^13 - 4*q^15 + 4*q^17 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 2, Pi/4, x]^2 EllipticTheta[ 3, 0, x] / (2 x^(1/2)), {x, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^7 * eta(x^8 + A)^2 / (eta(x + A)^2 * eta(x^4 + A)^4), n))};"
			],
			"xref": [
				"Cf. A045828, A246833, A246835."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Sep 04 2014",
			"references": 3,
			"revision": 12,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2014-09-04T14:56:56-04:00"
		}
	]
}