{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006931",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6931,
			"id": "M5463",
			"data": "561,41041,825265,321197185,5394826801,232250619601,9746347772161,1436697831295441,60977817398996785,7156857700403137441,1791562810662585767521,87674969936234821377601,6553130926752006031481761,1590231231043178376951698401",
			"name": "Least Carmichael number with n prime factors, or 0 if no such number exists.",
			"comment": [
				"Alford, Grantham, Hayman, \u0026 Shallue construct large Carmichael numbers, finding upper bounds for a(3)-a(19565220) and a(10333229505). - _Charles R Greathouse IV_, May 30 2013"
			],
			"reference": [
				"J.-P. Delahaye, Merveilleux nombres premiers (\"Amazing primes\"), p. 269, Pour la Science, Paris 2000.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"David W. Wilson, \u003ca href=\"/A006931/b006931.txt\"\u003eTable of n, a(n) for n = 3..35\u003c/a\u003e",
				"W. R. Alford, Jon Grantham, Steven Hayman, Andrew Shallue, \u003ca href=\"http://arxiv.org/abs/1203.6664\"\u003eConstructing Carmichael numbers through improved subset-product algorithms\u003c/a\u003e, arXiv:1203.6664 [math.NT], 2012-2013.",
				"R. G. E. Pinch, \u003ca href=\"https://doi.org/10.1090/S0025-5718-1993-1202611-7\"\u003eThe Carmichael numbers up to 10^15\u003c/a\u003e, Math. Comp. 61 (1993), no. 203, 381-391.",
				"R. G. E. Pinch, \u003ca href=\"https://arxiv.org/abs/math/0504119\"\u003eThe Carmichael numbers up to 10^17\u003c/a\u003e, arXiv:math/0504119 [math.NT], 2005.",
				"R. G. E. Pinch, \u003ca href=\"http://arXiv.org/abs/math/0604376\"\u003eThe Carmichael numbers up to 10^18\u003c/a\u003e, arXiv:math/0604376 [math.NT], 2006.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CarmichaelNumber.html\"\u003eCarmichael Number\u003c/a\u003e",
				"\u003ca href=\"/index/Ca#Carmichael\"\u003eIndex entries for sequences related to Carmichael numbers.\u003c/a\u003e"
			],
			"mathematica": [
				"(* Program not suitable to compute more than a few terms *)",
				"A2997 = Select[Range[1, 10^6, 2], CompositeQ[#] \u0026\u0026 Mod[#, CarmichaelLambda[#] ] == 1\u0026];",
				"(First /@ Split[Sort[{PrimeOmega[#], #}\u0026 /@ A2997], #1[[1]] == #2[[1]]\u0026])[[All, 2]] (* _Jean-François Alcover_, Sep 11 2018 *)"
			],
			"program": [
				"(PARI) Korselt(n,f=factor(n))=for(i=1,#f[,1],if(f[i,2]\u003e1||(n-1)%(f[i,1]-1),return(0)));1",
				"a(n)=my(p=2,f);forprime(q=3,default(primelimit),forstep(k=p+2,q-2,2,f=factor(k);if(vecmax(f[,2])==1 \u0026\u0026 #f[,2]==n \u0026\u0026 Korselt(k,f), return(k)));p=q)",
				"\\\\ _Charles R Greathouse IV_, Apr 25 2012"
			],
			"xref": [
				"Cf. A002997, A135717, A135719, A135720, A135721.",
				"Cf. A087788, A141711, A074379, A112428, A112429, A112430, A112431, A112432."
			],
			"keyword": "nonn",
			"offset": "3,1",
			"author": "_N. J. A. Sloane_ and _Richard Pinch_",
			"ext": [
				"Corrected by _Lekraj Beedassy_, Dec 31 2002",
				"More terms from _Ralf Stephan_, from the Pinch paper, Apr 16 2005",
				"Edited by _N. J. A. Sloane_, May 16 2008 at the suggestion of _R. J. Mathar_.",
				"Escape clause added by _Jianing Song_, Dec 12 2021"
			],
			"references": 28,
			"revision": 45,
			"time": "2021-12-12T11:44:43-05:00",
			"created": "1994-04-28T03:00:00-04:00"
		}
	]
}