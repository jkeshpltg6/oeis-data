{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A270205",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 270205,
			"data": "0,0,6,36,108,240,450,756,1176,1728,2430,3300,4356,5616,7098,8820,10800,13056,15606,18468,21660,25200,29106,33396,38088,43200,48750,54756,61236,68208,75690,83700,92256",
			"name": "Number of 2 X 2 planar subsets in an n X n X n cube.",
			"comment": [
				"William H. Press looked at the hybrid structure of a most-perfect magic square and the Hilbert space filling curve and thought it might be the \"most uniform\" way of putting the consecutive integers in a 2-d square. He thought a definition of \"most uniform\" would be useful.",
				"Al Zimmermann suggested this: Start by defining the \"non-uniformity of a distribution of integers among the cells of a square [or cube or hypercube]\" to be the standard deviation of the sums of the 2 X 2 planar subsets. Then define a \"most uniform distribution of integers\" to be a distribution with the smallest non-uniformity. For both the most-perfect square and most-perfect cube the non-uniformity is 0 and so each is a most uniform distribution. (Of course, you'd want a better word for \"non-uniformity\". Skewness?) Perhaps use \"2 X 2 planar subset\" instead of \"2 X 2 partition\"?",
				"Comment from Dwane Campbell: For cubes, the definition of compact is that all 2 X 2 X 2 subcubes add to the same sum. That definition also includes wrap around. Your most perfect space cube is compact. It has the additional constraint that each orthogonal plane is also compact. There are 64 2 X 2 X 2 subcubes that add to 260 and 192 2 X 2 subsquares that add to 130 in your cube. I did not think either result was possible. Congratulations!",
				"The most-perfect order 4 cube and the reversible order 4 cube are the new findings to look at in the link section.",
				"Most-perfect magic squares require every 2 X 2 cell block to have the same sum. This sequence looks at that same subset in the cube.",
				"Most-perfect space is defined as a structure where all these 2 X 2 subsets have the same sum.",
				"What structure provides the most uniform distribution of integers in a cube?",
				"a(n+1) is the number of unit faces required to make an n X n X n cubic lattice. Number of unit edges required for the same is A059986(n). - _Mohammed Yaseen_, Aug 22 2021"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A270205/b270205.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Craig Knecht, \u003ca href=\"/A270205/a270205.pdf\"\u003eCube assembly from different 2x2 planar criteria\u003c/a\u003e",
				"Craig Knecht, \u003ca href=\"/A270205/a270205.png\"\u003eMost-perfect space\u003c/a\u003e",
				"Craig Knecht, \u003ca href=\"/A270205/a270205_4.txt\"\u003eF1 code most-perfect magic cube 960 examples\u003c/a\u003e",
				"Craig Knecht, \u003ca href=\"/A270205/a270205.jpg\"\u003emagic space\u003c/a\u003e",
				"Craig Knecht, \u003ca href=\"/A270205/a270205_5.txt\"\u003eF1 code reversible cube 960 examples\u003c/a\u003e",
				"Walter Trump, \u003ca href=\"/A270205/a270205_1.pdf\"\u003eMost-Perfect magic cube\u003c/a\u003e",
				"Walter Trump, \u003ca href=\"/A270205/a270205_2.pdf\"\u003e6 unique neighbors for the most-perfect magic cube\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Most-perfect_magic_square\"\u003eMost-perfect magic square translated to a cube via the Hilbert space filling curve\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-6,4,-1)."
			],
			"formula": [
				"a(n) = 3*n^3 - 6*n^2 + 3*n.",
				"From _Wesley Ivan Hurt_, Mar 13 2016: (Start)",
				"G.f.: 6*x^2*(1+2*x)/(x-1)^4.",
				"a(n) = 4*a(n-1) - 6*a(n-2) + 4*a(n-3) - a(n-4) for n\u003e3. (End)",
				"E.g.f.: 3*x^2*(1+x)*exp(x). - _G. C. Greubel_, May 10 2016",
				"a(n) = 6 * A002411(n-1) for n\u003e=1. - _Joerg Arndt_, May 11 2016",
				"a(n) = A118659((n-1)^3), n\u003e1. - _Mohammed Yaseen_, Aug 22 2021"
			],
			"example": [
				"The 2 X 2 X 2 cube labeled with the integers 1 to 8 has the following six 2 X 2 planar subsets each containing 4 cells: 1,2,3,4; 5,6,7,8; 1,2,5,6; 3,4,7,8; 1,4,5,8; 2,3,6,7."
			],
			"maple": [
				"A270205:=n-\u003e3*n^3-6*n^2+3*n: seq(A270205(n), n=0..50); # _Wesley Ivan Hurt_, Mar 13 2016"
			],
			"mathematica": [
				"Table[3*n^3 - 6*n^2 + 3*n, {n, 0, 50}] (* _Wesley Ivan Hurt_, Mar 13 2016 *)",
				"CoefficientList[Series[(6 (x^2 + 2 x^3))/(-1 + x)^4, {x, 0, 32}], x] (* _Michael De Vlieger_, Mar 15 2016 *)"
			],
			"program": [
				"(MAGMA) [3*n^3 - 6*n^2 + 3*n: n in [0..50]]; // _Wesley Ivan Hurt_, Mar 13 2016",
				"(PARI) concat([0, 0], Vec(6*x^2*(1+2*x)/(x-1)^4 + O(x^100))) \\\\ _Altug Alkan_, Mar 14 2016",
				"(PARI) a(n) = 3*n^3 - 6*n^2 + 3*n \\\\ _Charles R Greathouse IV_, Mar 15 2016"
			],
			"xref": [
				"Cf. A002411, A059986, A118659."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Craig Knecht_, Mar 13 2016",
			"references": 3,
			"revision": 87,
			"time": "2021-10-05T22:28:25-04:00",
			"created": "2016-03-15T16:07:20-04:00"
		}
	]
}