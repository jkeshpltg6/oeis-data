{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A222298",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 222298,
			"data": "12,12,260,12,236,28,28,28,28,236,20,44,44,20,20,36,76,12,12,4,12,4,36,36,36,3276,76,36,36,3276,84,20,12,12,20,36,36,2444,2444,36,44,1356,156,28,12,220,12,12,84,12,132,28,68,36,36,1044,20,20,28,1044,20",
			"name": "Length of the Gaussian prime spiral beginning at the n-th positive real Gaussian prime (A002145).",
			"comment": [
				"The Gaussian prime spiral is described in the short note by O'Rourke and Wagon. It is not known if every iteration is a closed loop. See A222299 for the number of distinct primes on the spiral. See A222300 for the length of the spiral (which is the same as the number of numbers tested for primality, without memory).",
				"This idea can be extended to any Gaussian prime. Sequences A222594, A222595, and A222596 show the results for first-quadrant Gaussian primes. - _T. D. Noe_, Feb 27 2013"
			],
			"reference": [
				"Joseph O'Rourke and Stan Wagon, Gaussian prime spirals, Mathematics Magazine, vol. 86, no. 1 (2013), p. 14."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A222298/b222298.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"T. D. Noe, \u003ca href=\"/A222298/a222298_5.png\"\u003ePlot beginning with 11\u003c/a\u003e (similar to the cover of Mathematics Magazine, vol. 86, no. 1 (2013))",
				"Joseph O'Rourke, \u003ca href=\"http://mathoverflow.net/questions/91423/gaussian-prime-spirals\"\u003eMathOverflow: Gaussian prime spirals\u003c/a\u003e"
			],
			"example": [
				"The loop beginning with 31 is {31, 43, 43 - 8i, 37 - 8i, 37 - 2i, 45 - 2i, 45 - 8i, 43 - 8i, 43, 47, 47 - 2i, 45 - 2i, 45 + 2i, 47 + 2i, 47, 43, 43 + 8i, 45 + 8i, 45 + 2i, 37 + 2i, 37 + 8i, 43 + 8i, 43, 31, 31 + 4i, 41 + 4i, 41 - 4i, 31 - 4i, 31}. The first and last numbers are the same. So only one is counted."
			],
			"mathematica": [
				"loop[n_] := Module[{p = n, direction = 1}, lst = {n}; While[While[p = p + direction; ! PrimeQ[p, GaussianIntegers -\u003e True]]; direction = direction*(-I); AppendTo[lst, p]; ! (p == n \u0026\u0026 direction == 1)]; Length[lst]]; cp = Select[Range[1000], PrimeQ[#, GaussianIntegers -\u003e True] \u0026]; Table[loop[p]-1, {p, cp}]"
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_T. D. Noe_, Feb 25 2013",
			"references": 6,
			"revision": 38,
			"time": "2013-03-04T23:54:35-05:00",
			"created": "2013-02-25T21:18:11-05:00"
		}
	]
}