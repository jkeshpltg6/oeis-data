{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A108481",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 108481,
			"data": "1,3,4,3,0,-5,-7,-2,8,16,12,-7,-29,-35,-10,37,70,53,-21,-106,-126,-38,119,226,164,-70,-326,-378,-106,353,652,469,-189,-885,-1015,-290,910,1664,1179,-483,-2205,-2492,-692,2212,3998,2809,-1120,-5119,-5754,-1598,4992,8968,6251,-2506,-11285,-12579",
			"name": "Expansion of q^(-1) * f(-q^2, -q^5)^2 * f(-q^3, -q^4) / f(-q^1, -q^6)^3 in powers of q where f() is Ramanujan's two-variable theta function.",
			"comment": [
				"Number 9 of the 15 generalized eta-quotients listed in Table I of Yang 2004. - _Michael Somos_, Jul 22 2014",
				"A generator (Hauptmodul) of the function field associated with the congruence subgroup Gamma_1(7). [Yang 2004] - _Michael Somos_, Jul 22 2014"
			],
			"reference": [
				"N. Elkies, The Klein quartic in number theory, pp. 51-101 of S. Levy, ed., The Eightfold Way, Cambridge Univ. Press, 1999. MR1722413 (2001a:11103). See p. 89 eq. (4.23)"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A108481/b108481.txt\"\u003eTable of n, a(n) for n = -1..10000\u003c/a\u003e (terms -1..1003 from G. A. Edgar)",
				"W. Duke, \u003ca href=\"http://dx.doi.org/10.1090/S0273-0979-05-01047-5\"\u003eContinued fractions and modular functions\u003c/a\u003e, Bull. Amer. Math. Soc. 42 (2005), 137-162. See page 156.",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"Y. Yang, \u003ca href=\"http://dx.doi.org/10.1112/S0024609304003510\"\u003eTransformation formulas for generalized Dedekind eta functions\u003c/a\u003e, Bull. London Math. Soc. 36 (2004), no. 5, 671-682. See p. 679, Table 1."
			],
			"formula": [
				"Euler transform of period 7 sequence [ 3, -2, -1, -1, -2, 3, 0, ...]. - _Michael Somos_, Jul 22 2014",
				"G.f.: (1/x) * Product_{k\u003e0} (1 - x^(7*k - 2))^2 * (1 - x^(7*k - 5))^2 * (1 - x^(7*k - 3)) * (1 - x^(7*k - 4)) / ((1 - x^(7*k - 1)) * (1 - x^(7*k - 6)))^3.",
				"G.f. A(q) satisfies 0 = f(A(q), A(q^2), A(q^4)) where f(u, v, w) = v - w + u^2 - 2*w*v - 3*u*w + 4*u*v + w^2*v + u*w^2 - u^2*v - u^2*w^2 + 4*u^2*w - 4*u^2*w*v - 5*u*v^2 + 5*u*w*v^2.",
				"G.f. A(q) satisfies 0 = f(A(q), A(q^2)) where f(u, v) = 2*v * (u - 1) * (3*u*v + v - 2*u - 1) - (u^2 - v) * (u*v^2 - 2*u*v + 2*v + u - 1). - _Michael Somos_, Jul 22 2014",
				"a(n) = A246713(n) unless n = 0. - _Michael Somos_, Sep 02 2014",
				"G.f.: T(q) = 1/q + 3 + 4*q + ... for this sequence is cubically related to T7B(q) of A052240: T7B = T - 3 - 1/(T-1) - 1/T. - _G. A. Edgar_, Apr 12 2017 [corrected by _Seiichi Manyama_, Oct 10 2018]"
			],
			"example": [
				"G.f. = 1/q + 3 + 4*q + 3*q^2 - 5*q^4 - 7*q^5 - 2*q^6 + 8*q^7 + 16*q^8 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c -1, 0, With[{m = n + 1}, SeriesCoefficient[ 1/q Product[ (1 - q^k)^{-3, 2, 1, 1, 2, -3, 0}[[Mod[k, 7, 1]]], {k, m}], {q, 0, n}]]]; (* _Michael Somos_, Jul 22 2014 *)",
				"a[ n_] := SeriesCoefficient[ 1/q (QPochhammer[ q^2, q^7] QPochhammer[ q^5, q^7])^2 QPochhammer[ q^3, q^7] QPochhammer[ q^4, q^7] / (QPochhammer[ q^1, q^7] QPochhammer[ q^6, q^7])^3, {q, 0, n}]; (* _Michael Somos_, Jul 22 2014 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c-1, 0, n++; polcoeff( prod(k=1, n, (1 - x^k + x * O(x^n))^[ 0, -3, 2, 1, 1, 2, -3][k%7 + 1]), n))};",
				"(MAGMA) A := Basis( ModularForms( Gamma1(7), 1), 58); B\u003cq\u003e := A[2] / A[3];  B; /* _Michael Somos_, Nov 09 2014 */"
			],
			"xref": [
				"Cf. A030181, A052240, A246713, A262933 (T/(T-1)), A305443 (1/T)."
			],
			"keyword": "sign",
			"offset": "-1,2",
			"author": "_Michael Somos_, Jun 04 2005",
			"references": 5,
			"revision": 46,
			"time": "2021-03-12T22:24:43-05:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}