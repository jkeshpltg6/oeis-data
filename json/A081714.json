{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A081714",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 81714,
			"data": "0,3,4,14,33,90,232,611,1596,4182,10945,28658,75024,196419,514228,1346270,3524577,9227466,24157816,63245987,165580140,433494438,1134903169,2971215074,7778742048,20365011075,53316291172,139583862446,365435296161,956722026042",
			"name": "a(n) = F(n)*L(n+1) where F=Fibonacci and L=Lucas numbers.",
			"comment": [
				"Also convolution of Fibonacci and Lucas numbers.",
				"For n\u003e2, a(n) represents twice the area of the triangle created by the three points ((L(n-3), L(n-2)), (L(n-1), L(n)) and (F(n+3), F(n+2)) where L(k)=A000032(k) and F(k)= A000045(k). - _J. M. Bergot_, May 20 2014",
				"For n\u003e1, a(n) is the remainder when F(n+3)*F(n+4) is divided by F(n+1)*F(n+2). - _J. M. Bergot_, May 24 2014"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A081714/b081714.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,2,-1)."
			],
			"formula": [
				"G.f.: x*(3-2*x)/((1+x)*(1-3*x+x^2)).",
				"a(n) = A122367(n) - (-1)^n. - _R. J. Mathar_, Jul 23 2010",
				"a(n) = (L(n+1)^2 - F(2*n+2))/2 = ( A001254(n+1) - A001906(n+1) )/2. - _Gary Detlefs_, Nov 28 2010",
				"a(n+1) = - A186679(2*n+1). - _Reinhard Zumkeller_, Feb 25 2011",
				"a(n) = A035513(1,n-1)*A035513(2,n-1). - _R. J. Mathar_, Sep 04 2016",
				"a(n)+a(n+1) = A005248(n+1). - _R. J. Mathar_, Sep 04 2016",
				"a(n) = (-(-1)^n+(2^(-1-n)*((3-sqrt(5))^n*(-1+sqrt(5))+(1+sqrt(5))*(3+sqrt(5))^n)) / sqrt(5)). - _Colin Barker_, Sep 28 2016"
			],
			"maple": [
				"with(combinat): F:=n-\u003e fibonacci(n): L:= n-\u003e F(n+1)+F(n-1):",
				"a:= n-\u003e F(n)*L(n+1): seq(a(n), n=0..30);"
			],
			"mathematica": [
				"Fibonacci[Range[0,50]]*LucasL[Range[0,50]+1] (* _Vladimir Joseph Stephan Orlovsky_, Mar 17 2011*)"
			],
			"program": [
				"(PARI) x='x+O('x^51);for(n=0,50,print1(polcoeff(serconvol(Ser((1+2*x)/(1-x-x*x)),Ser(x/(1-x-x*x))),n)\",\"))",
				"(PARI) a(n)=fibonacci(n)*(fibonacci(n+2)+fibonacci(n))",
				"(PARI) a(n) = round((-(-1)^n+(2^(-1-n)*((3-sqrt(5))^n*(-1+sqrt(5))+(1+sqrt(5))*(3+sqrt(5))^n))/sqrt(5))) \\\\ _Colin Barker_, Sep 28 2016",
				"(MAGMA) [Fibonacci(n)*Lucas(n+1): n in [0..30]]; // _Vincenzo Librandi_, Sep 08 2012",
				"(Sage) [fibonacci(n)*(fibonacci(n+2)+fibonacci(n)) for n in (0..30)] # _G. C. Greubel_, Jan 07 2019",
				"(GAP) List([0..30], n -\u003e Fibonacci(n)*(Fibonacci(n+2)+Fibonacci(n))); # _G. C. Greubel_, Jan 07 2019"
			],
			"xref": [
				"Cf. A000045, A000204."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Ralf Stephan_, Apr 03 2003",
			"ext": [
				"Simpler definition from _Michael Somos_, Mar 16 2004"
			],
			"references": 6,
			"revision": 52,
			"time": "2020-04-19T05:36:33-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}