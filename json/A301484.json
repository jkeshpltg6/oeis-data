{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A301484",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 301484,
			"data": "3,8,8,2,1,0,7,6,5,5,6,7,7,9,5,7,8,7,5,1,1,6,5,8,5,5,7,3,0,6,5,3,7,0,2,9,2,2,1,7,4,5,0,4,0,7,2,5,3,2,9,8,1,8,6,4,6,4,2,8,2,7,5,9,3,7,3,5,1,7,3,9,5,6,3,8,2,4,2,0,1,2,1,1,0,1,9,3,5,1,6,2,8,2,8,0,3,1,9,6,0,5,2,1,6",
			"name": "Decimal expansion of J_0(2)/J_1(2) = 1 - 1/(2 - 1/(3 - 1/(4 - ...))).",
			"comment": [
				"These are the first 105 decimal digits of the constant defined by the continued fraction 1 - 1/(2 - 1/(3 - 1/(4 - ... -1/m))) as m goes to infinity. The continued fraction appears to converge fairly rapidly. Just 50 terms, for instance, suffices to produce a numerical value that appears to be good to 100 digits, based on comparisons with more terms and higher precision. This sequence was brought to the author's attention by Beresford Parlett of U.C. Berkeley.",
				"Addendum: This sequence has been identified by Karl Dilcher. He noted that the sequence of continued fraction convergents is the same as A058797. In short, the real constant whose decimal expansion is given above is given by BesselJ[0,2]/BesselJ[1,2] (Mathematica expression). The comments at A058797 have quite a bit of additional information and references.",
				"Equivalent to the imaginary part of the infinite continued fraction i + 1/(2i + 1/(3i + ...)) where i = sqrt(-1). - _Matthew Niemiro_, Dec 22 2019"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A301484/b301484.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e"
			],
			"formula": [
				"Equals BesselJ(0,2)/BesselJ(1,2)."
			],
			"example": [
				"0.38821076556779578751165855730653702922174504072532981864642827593735174..."
			],
			"maple": [
				"evalf(BesselJ(0, 2)/BesselJ(1, 2), 100); # _G. C. Greubel_, Dec 31 2019"
			],
			"mathematica": [
				"1 +ContinuedFractionK[(-1)^(n+1)*n, {n,2,Infinity}]",
				"N[1+ContinuedFractionK[(-1)^(n+1)*n, {n,2,50}], 105] (* 105 decimals *)",
				"RealDigits[BesselJ[0, 2]/BesselJ[1, 2], 10, 100][[1]] (* _G. C. Greubel_, Dec 31 2019 *)"
			],
			"program": [
				"(PARI) default(realprecision, 100); besselj(0,2)/besselj(1,2) \\\\ _Altug Alkan_, Mar 22 2018",
				"(MAGMA) SetDefaultRealField(RealField(100)); BesselFunction(0, 2)/BesselFunction(1, 2); // _G. C. Greubel_, Dec 31 2019",
				"(Sage) numerical_approx(bessel_J(0,2)/bessel_J(1,2), digits=100) # _G. C. Greubel_, Dec 31 2019"
			],
			"xref": [
				"Cf. A058797, A091681, A296168."
			],
			"keyword": "nonn,cons",
			"offset": "0,1",
			"author": "_David H Bailey_, Mar 22 2018",
			"references": 2,
			"revision": 35,
			"time": "2020-01-18T01:04:26-05:00",
			"created": "2018-03-23T09:55:56-04:00"
		}
	]
}