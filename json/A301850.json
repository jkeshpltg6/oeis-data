{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A301850",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 301850,
			"data": "0,1,0,-1,0,1,1,0,0,1,0,-1,1,-1,1,0,0,1,0,-1,0,1,1,0,1,-1,0,-1,1,-1,1,0,0,1,0,-1,0,1,1,0,0,1,0,-1,1,-1,1,0,1,-1,0,-1,0,1,1,0,1,-1,0,-1,1,-1,1,0,0,1,0,-1,0,1,1,0,0,1,0,-1,1,-1,1,0,0,1,0,-1,0,1,1,0,1,-1,0,-1,1,-1,1,0,1,-1,0,-1",
			"name": "The Dakota sequence: a sequence with zero-free number-wall over ternary extension fields.",
			"comment": [
				"c(0), c(1), ... is the fixed point of inflation morphism 1 -\u003e 1 3, 2 -\u003e 2 3, 3 -\u003e 1 4, 4 -\u003e 2 4, starting from state 1;",
				"a(-1), a(0), ... is the image of c(n) under encoding morphism 1 -\u003e 0,+1; 2 -\u003e +1,-1; 3 -\u003e 0,-1; 4 -\u003e +1,0; where c(n) denotes A301848(n).",
				"The number-walls (signed Hankel determinants) over finite fields with characteristic 3 of sequence x + a(n) with x not in F_3 have been proved free of zeros."
			],
			"reference": [
				"Jean-Paul Allouche and Jeffrey O. Shallit, Automatic sequences, Cambridge, 2003."
			],
			"link": [
				"W. F. Lunnon, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL4/LUNNON/numbwall10.html\"\u003eThe number-wall algorithm: an LFSR cookbook\u003c/a\u003e, Journal of Integer Sequences 4 (2001), no. 1, 01.1.1.",
				"Fred Lunnon, \u003ca href=\"https://arxiv.org/abs/0906.3286\"\u003eThe Pagoda sequence: a ramble through linear complexity, number walls, D0L sequences, finite state automata, and aperiodic tilings\u003c/a\u003e, Electronic Proceedings in Theoretical Computer Science 1 (2009), 130-148."
			],
			"mathematica": [
				"b[n_] := b[n] = If[n == 0, 0, BitGet[n, IntegerExponent[n, 2] + 1]];",
				"c[n_] := b[2 n] - 2 b[2 n - 1] + 3;",
				"Array[c, 50, 0] /. {1 -\u003e {0, 1}, 2 -\u003e {1, -1}, 3 -\u003e {0, -1}, 4 -\u003e {1, 0}} // Flatten (* _Jean-François Alcover_, Dec 13 2018 *)"
			],
			"program": [
				"(Magma)",
				"function b (n)",
				"  if n eq 0 then return 0; // alternatively,  return 1;",
				"  else while IsEven(n) do n := n div 2; end while; end if;",
				"  return n div 2 mod 2; end function;",
				"function c (n)",
				"  return b(n+n) - 2*b(n+n-1) + 3; end function;",
				"PGF\u003cx\u003e := PolynomialRing(RationalField());  // polynomial in  x",
				"function xplusa (n, x)",
				"  return [ [x, x+1], [x+1, x-1], [x, x-1], [x+1, x] ]",
				"    [c(n div 2)][n mod 2+1];",
				"end function;",
				"function a (n)",
				"  return Coefficient(xplusa(n, x), 0); end function;",
				"  nlo := 0; nhi := 32;",
				"  [a(n) : n in [nlo..nhi] ];"
			],
			"xref": [
				"Cf. A038189, A301848, A301849."
			],
			"keyword": "sign",
			"offset": "0",
			"author": "_Fred Lunnon_, Mar 27 2018",
			"ext": [
				"More terms from _Jean-François Alcover_, Dec 13 2018"
			],
			"references": 3,
			"revision": 16,
			"time": "2018-12-13T08:03:49-05:00",
			"created": "2018-03-29T07:05:08-04:00"
		}
	]
}