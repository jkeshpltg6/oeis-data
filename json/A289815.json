{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A289815",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 289815,
			"data": "1,2,1,3,6,3,1,2,1,4,10,5,12,30,15,4,10,5,1,2,1,3,6,3,1,2,1,5,14,7,15,42,21,5,14,7,20,70,35,60,210,105,20,70,35,5,14,7,15,42,21,5,14,7,1,2,1,3,6,3,1,2,1,4,10,5,12,30,15,4,10,5,1,2,1,3,6",
			"name": "The first of a pair of coprime numbers whose factorizations depend on the ternary representation of n (see Comments for precise definition).",
			"comment": [
				"For n \u003e= 0, with ternary representation Sum_{i=1..k} t_i * 3^e_i (all t_i in {1, 2} and all e_i distinct and in increasing order):",
				"- let S(0) = A000961 \\ { 1 },",
				"- and S(i) = S(i-1) \\ { p^(f + j), with p^f = the (e_i+1)-th term of S(i-1) and j \u003e 0 } for any i=1..k,",
				"- then a(n) = Product_{i=1..k such that t_i=1} \"the (e_i+1)-th term of S(k)\".",
				"See A289816 for the second coprime number.",
				"See A289838 for the product of this sequence with A289816.",
				"By design, gcd(a(n), A289816(n)) = 1.",
				"Also, the number of distinct prime factors of a(n) equals the number of ones in the ternary representation of n.",
				"We also have a(n) = A289816(A004488(n)) for any n \u003e= 0.",
				"For each pair of coprime numbers, say x and y, there is a unique index, say n, such that a(n) = x and A289816(n) = y; in fact, n = A289905(x,y).",
				"This sequence combines features of A289813 and A289272.",
				"The scatterplot of the first terms of this sequence vs A289816 (both with logarithmic scaling) looks like a triangular cristal.",
				"For any t \u003e 0: we can adapt the algorithm used here and in A289816 in order to uniquely enumerate every tuple of t mutually coprime numbers (see Links section for corresponding program)."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A289815/b289815.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A289815/a289815.png\"\u003eScatterplot of the first 10000 terms of A289815 vs A289816 (both with logarithmic scaling)\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A289815/a289815.gp.txt\"\u003ePARI program to uniquely enumerate tuples of mutually coprime numbers\u003c/a\u003e"
			],
			"formula": [
				"a(A005836(n)) = A289272(n-1) for any n \u003e 0.",
				"a(2 * A005836(n)) = 1 for any n \u003e 0."
			],
			"example": [
				"For n=42:",
				"- 42 = 2*3^1 + 1*3^2 + 1*3^3,",
				"- S(0) = { 2, 3, 4, 5, 7, 8, 9, 11, 13, 16, 17, 19, 23, 25, 27, 29, ... },",
				"- S(1) = S(0) \\ { 3^(1+j) with j \u003e 0 }",
				"       = { 2, 3, 4, 5, 7, 8,    11, 13, 16, 17, 19, 23, 25,     29, ... },",
				"- S(2) = S(1) \\ { 2^(2+j) with j \u003e 0 }",
				"       = { 2, 3, 4, 5, 7,       11, 13,     17, 19, 23, 25,     29, ... },",
				"- S(3) = S(2) \\ { 5^(1+j) with j \u003e 0 }",
				"       = { 2, 3, 4, 5, 7,       11, 13,     17, 19, 23,         29, ... },",
				"- a(42) = 4 * 5 = 20."
			],
			"program": [
				"(PARI) a(n) =",
				"{",
				"    my (v=1, x=1);",
				"    for (o=2, oo,",
				"        if (n==0, return (v));",
				"        if (gcd(x,o)==1 \u0026\u0026 omega(o)==1,",
				"            if (n % 3,    x *= o);",
				"            if (n % 3==1, v *= o);",
				"            n \\= 3;",
				"        );",
				"    );",
				"}",
				"(Python)",
				"from sympy import gcd, primefactors",
				"def omega(n): return 0 if n==1 else len(primefactors(n))",
				"def a(n):",
				"    v, x, o = 1, 1, 2",
				"    while True:",
				"        if n==0: return v",
				"        if gcd(x, o)==1 and omega(o)==1:",
				"            if n%3: x*=o",
				"            if n%3==1:v*=o",
				"            n //= 3",
				"        o+=1",
				"print([a(n) for n in range(101)]) # _Indranil Ghosh_, Aug 02 2017"
			],
			"xref": [
				"Cf. A000961, A004488, A289272, A289813, A289816, A289838, A289905."
			],
			"keyword": "nonn,base,look",
			"offset": "0,2",
			"author": "_Rémy Sigrist_, Jul 12 2017",
			"references": 5,
			"revision": 44,
			"time": "2021-04-21T07:12:05-04:00",
			"created": "2017-07-16T07:31:44-04:00"
		}
	]
}