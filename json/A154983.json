{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A154983",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 154983,
			"data": "1,1,1,1,4,1,1,11,11,1,1,24,70,24,1,1,49,358,358,49,1,1,98,1559,4076,1559,98,1,1,195,6361,40003,40003,6361,195,1,1,388,25372,345692,862598,345692,25372,388,1,1,773,100640,2813688,16569442,16569442,2813688,100640,773,1",
			"name": "Triangle T(n, k, m) = coefficients of p(x, n, m) where p(x,n,m) = (x+1)*p(x, n-1, m) + (2^(m+n-1) + 2^(n-2)*[n\u003e=3])*x*p(x, n-2, m) and m=0, read by rows.",
			"comment": [
				"Row sums are: {1, 2, 6, 24, 120, 816, 7392, 93120, 1605504, 38969088, 1310965248, ...}."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A154983/b154983.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k, m) = coefficients of p(x, n, m) where p(x,n,m) = (x+1)*p(x, n-1, m) + (2^(m+n-1) + 2^(n-2)*[n\u003e=3])*x*p(x, n-2, m) and m=0.",
				"T(n, k, m) = T(n-1, k, m) + T(n-1, k-1, m) + (2^(n+m-1) + 2^(n-2)*[n\u003e=3])*T(n-2, k-1, m) with T(n, 0, m) = T(n, n, m) = 1 and m=0. - _G. C. Greubel_, Mar 01 2021"
			],
			"example": [
				"Triangle begins as:",
				"  1;",
				"  1,   1;",
				"  1,   4,      1;",
				"  1,  11,     11,       1;",
				"  1,  24,     70,      24,        1;",
				"  1,  49,    358,     358,       49,        1;",
				"  1,  98,   1559,    4076,     1559,       98,       1;",
				"  1, 195,   6361,   40003,    40003,     6361,     195,      1;",
				"  1, 388,  25372,  345692,   862598,   345692,   25372,    388,   1;",
				"  1, 773, 100640, 2813688, 16569442, 16569442, 2813688, 100640, 773, 1;"
			],
			"mathematica": [
				"(* First program *)",
				"p[x_, n_, m_]:= p[x,n,m] = If[n\u003c2, n*x+1, (x+1)*p[x, n-1, m] + 2^(m+n-1)*x*p[x, n-2, m] + Boole[n\u003e=3]*2^(n-2)*x*p[x, n-2, m] ];",
				"Table[CoefficientList[ExpandAll[p[x,n,0]], x], {n,0,10}]//Flatten (* modified by _G. C. Greubel_, Mar 01 2021 *)",
				"(* Second program *)",
				"T[n_, k_, m_]:= T[n, k, m] = If[k==0 || k==n, 1, T[n-1, k, m] + T[n-1, k-1, m] +(2^(m+n-1) + Boole[n\u003e=3]*2^(n-2))*T[n-2, k-1, m] ];",
				"Table[T[n, k, 0], {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, Mar 01 2021 *)"
			],
			"program": [
				"(Sage)",
				"def T(n,k,m):",
				"    if (k==0 or k==n): return 1",
				"    elif (n\u003c3): return T(n-1, k, m) + T(n-1, k-1, m) + 2^(n+m-1)*T(n-2, k-1, m)",
				"    else: return T(n-1, k, m) + T(n-1, k-1, m) + (2^(n+m-1) +2^(n-2))*T(n-2, k-1, m)",
				"flatten([[T(n,k,0) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Mar 01 2021",
				"(Magma)",
				"function T(n,k,m)",
				"  if k eq 0 or k eq n then return 1;",
				"  elif (n lt 3) then return T(n-1, k, m) + T(n-1, k-1, m) + 2^(n+m-1)*T(n-2, k-1, m);",
				"  else return T(n-1, k, m) + T(n-1, k-1, m) + (2^(n+m-1)+2^(n-2))*T(n-2, k-1, m);",
				"  end if; return T;",
				"end function;",
				"[T(n,k,0): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Mar 01 2021"
			],
			"xref": [
				"Cf. this sequence (m=0), A154984 (m=1), A154985 (m=3).",
				"Cf. A154979, A154980, A154982, A154986."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Roger L. Bagula_, Jan 18 2009",
			"ext": [
				"Edited by _G. C. Greubel_, Mar 01 2021"
			],
			"references": 4,
			"revision": 5,
			"time": "2021-03-01T21:51:07-05:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}