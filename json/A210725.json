{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A210725",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 210725,
			"data": "1,1,3,1,10,16,1,41,101,125,1,196,756,1176,1296,1,1057,6607,12847,16087,16807,1,6322,65794,160504,229384,257104,262144,1,41393,733833,2261289,3687609,4480569,4742649,4782969,1,293608,9046648,35464816,66025360,87238720,96915520,99637120,100000000",
			"name": "Triangle read by rows: T(n,k) = number of forests of labeled rooted trees with n nodes and height at most k (n\u003e=1, 0\u003c=k\u003c=n-1).",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A210725/b210725.txt\"\u003eRows n = 1..141, flattened\u003c/a\u003e",
				"J. Riordan, \u003ca href=\"http://dx.doi.org/10.1016/S0021-9800(68)80033-X\"\u003eForests of labeled trees\u003c/a\u003e, J. Combin. Theory, 5 (1968), 90-103."
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  1,    3;",
				"  1,   10,   16;",
				"  1,   41,  101,   125;",
				"  1,  196,  756,  1176,  1296;",
				"  1, 1057, 6607, 12847, 16087, 16807;",
				"  ..."
			],
			"maple": [
				"f:= proc(k) f(k):= `if`(k\u003c0, 1, exp(x*f(k-1))) end:",
				"T:= (n, k)-\u003e coeff(series(f(k), x, n+1), x, n) *n!:",
				"seq(seq(T(n, k), k=0..n-1), n=1..9); # _Alois P. Heinz_, May 30 2012",
				"# second Maple program:",
				"T:= proc(n, h) option remember; `if`(min(n, h)=0, 1, add(",
				"      binomial(n-1, j-1)*j*T(j-1, h-1)*T(n-j, h), j=1..n))",
				"    end:",
				"seq(seq(T(n, k), k=0..n-1), n=1..10);  # _Alois P. Heinz_, Aug 21 2017"
			],
			"mathematica": [
				"f[_?Negative] = 1; f[k_] := Exp[x*f[k-1]]; t[n_, k_] := Coefficient[Series[f[k], {x, 0, n+1}], x, n]*n!; Table[Table[t[n, k], {k, 0, n-1}], {n, 1, 9}] // Flatten (* _Jean-François Alcover_, Oct 30 2013, after Maple *)"
			],
			"program": [
				"(Python)",
				"from sympy.core.cache import cacheit",
				"from sympy import binomial",
				"@cacheit",
				"def T(n, h): return 1 if min(n, h)==0 else sum([binomial(n - 1, j - 1)*j*T(j - 1, h - 1)*T(n - j, h) for j in range(1, n + 1)])",
				"for n in range(1, 11): print([T(n, k) for k in range(n)]) # _Indranil Ghosh_, Aug 21 2017, after second Maple code"
			],
			"xref": [
				"Diagonals include A000248, A000949, A000950, A000951, A000272."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_, May 09 2012",
			"references": 7,
			"revision": 33,
			"time": "2021-04-26T11:19:44-04:00",
			"created": "2012-05-09T21:56:31-04:00"
		}
	]
}