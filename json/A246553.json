{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A246553",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 246553,
			"data": "1,2,7,7,7,43,5,16,19,87,25,31,1061,9,43,32815,565,63,61,16451,7,73,1048655,2131,91,97,131173,39,107,16777325,4209,127,4294967427,524425,171,149,134217879,4253,163,68719476903,1048749,187,181,536871103,2241,197,549755814087",
			"name": "Limiting sequence obtained by taking the sequence 0, 2, 3, 5, 7, 11, 13, ... and applying an infinite process which is described in the comments.",
			"comment": [
				"Consider the (0,1)-triangle T_0(P) with entries numbered 1,2,3,4,..., the rows of which, read in binary and converted to decimal, give the sequence 0 followed by consecutive primes, 0,2,3,5,7,...",
				"Let the operator A_k map every k-th entry to its binary opposite (1-\u003e0, 0-\u003e1), for k=1,2,... .",
				"Put T_inf(P) = ...*A_3*A_2*A_1(T_(0)P), with successive applications of the operators A_1, A_2, A_3, ...",
				"Note that the (0,1)-triangle T_inf(P) is well-defined, since the operator T_n does not affect entries in the first floor((sqrt(8*n-7) - 1)/2) rows.",
				"The sequence lists numbers obtained by reading rows of T_inf(P) in binary and converting them to decimal."
			],
			"link": [
				"Peter J. C. Moses, \u003ca href=\"/A246553/b246553.txt\"\u003eTable of n, a(n) for n = 1..500\u003c/a\u003e"
			],
			"formula": [
				"If we take the initial triangle T_0(O) to consist of all 0's, then in T_inf(O) the 1's are only on positions of squares of all positive numbers, i.e., 1,4,9,16,... . Indeed, in order to get an entry in the n-th position of T_inf(O), we should use all considered operators A_d, d|n. The number of these operators is the number of divisors of n which is odd iff n is a perfect square. Thus only in this case, we obtain that entry in the n-th position is flipped, beginning with 0, an odd number of times, such that in the n-th position of T_inf(O) we have 1, while, if n is nonsquare, in the n-th position we have 0.",
				"T_inf(O) begins:",
				"1",
				"00",
				"100",
				"0010",
				"00000",
				"100000",
				"0001000",
				"00000001",
				".........",
				"Now we have T_inf(P) = XNOR(T_0(P), T_inf(O))."
			],
			"example": [
				"T_0(P) begins:",
				"0",
				"10",
				"11",
				"101",
				"111",
				"1011",
				"1101",
				"10001",
				"........",
				"T_inf(P) begins:",
				"1",
				"10",
				"111",
				"0111",
				"00111",
				"101011",
				"0000101",
				"00010000",
				"000010011",
				"0001010111",
				"00000011001",
				"000000011111",
				"0010000100101",
				"............."
			],
			"mathematica": [
				"seq=Apply[BitXor,{Map[If[IntegerQ[Sqrt[#]],1,0]\u0026,Range[Length[#]]],#}\u0026[Flatten[Join[{{0}},Map[IntegerDigits[Prime[#],2,#+1]\u0026,Range[50]]]]]];",
				"Map[FromDigits[#,2]\u0026,MapThread[seq[[#1;;#2]]\u0026,({Join[{0},Most[#1]]+1,#1}\u0026)[#/2(#+1)\u0026[Range[NestWhile[#+1\u0026,1,((1+#1) (2+#1)\u003c=2Length[seq])\u0026]]]]]] (* _Peter J. C. Moses_, Nov 18 2014 *)"
			],
			"xref": [
				"Cf. A000040, A000225, A247092."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Vladimir Shevelev_ and _Peter J. C. Moses_, Nov 16 2014",
			"references": 2,
			"revision": 75,
			"time": "2016-02-15T14:53:15-05:00",
			"created": "2014-11-16T12:01:59-05:00"
		}
	]
}