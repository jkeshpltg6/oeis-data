{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A100062",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 100062,
			"data": "9,81,729,6561,59049,531441,4782969,43046721,387420489,3486784401,31381059609,282429536481,2541865828329,22876792454961,205891132094649,1853020188851841,16677181699666569,150094635296999121",
			"name": "Denominator of the probability that an integer n occurs in the cumulative sums of the decimal digits of a random real number between 0 and 1.",
			"comment": [
				"Essentially the same as A001019 = powers of 9.",
				"Also number of n-digit positive integers with no identical adjacent digits. Hence the numerator (with A052268 as denominator) of the probability that an n-digit positive integer has this property (e.g., 9/9, 81/90, 729/900, ..., where A100062(n)/A052268(n) reduces to A001019(n-1)/A011557(n-1)). - _Rick L. Shepherd_, Jun 08 2008"
			],
			"link": [
				"Stanislav Sykora, \u003ca href=\"/A100062/b100062.txt\"\u003eTable of n, a(n) for n = 1..666\u003c/a\u003e; previous Table by Rick L. Shepherd went up to n=30.",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/EvilNumber.html\"\u003eEvil Number\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_01\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (9)."
			],
			"formula": [
				"a(n) = 9^n. - _Max Alekseyev_, Mar 03 2007",
				"From _Philippe Deléham_, Nov 23 2008: (Start)",
				"a(n) = 9*a(n-1), n\u003e1; a(1)=9.",
				"G.f.: 9x/(1-9x). (End)",
				"a(n) = A001019(n) for n\u003e0. - _Wesley Ivan Hurt_, Apr 18 2016"
			],
			"example": [
				"1/9, 10/81, 100/729, 1000/6561, 10000/59049, ..."
			],
			"maple": [
				"A100062:=n-\u003e9^n: seq(A100062(n), n=1..30); # _Wesley Ivan Hurt_, Apr 18 2016"
			],
			"mathematica": [
				"9^Range[20] (* _Harvey P. Dale_, Dec 25 2012 *)"
			],
			"program": [
				"(PARI) \\\\ The 'old' approach, using the generating function:",
				"s = Vec(Ser((1-x^9)/(x^10-10*x+9),x,666));",
				"a = vector(#s,n,denominator(s[n])) \\\\ _Stanislav Sykora_, Apr 16 2016",
				"(MAGMA) [9^n : n in [1..30]]; // _Wesley Ivan Hurt_, Apr 18 2016"
			],
			"xref": [
				"Cf. A001019, A011557 A052268, A100061, A100062, A271880, A271881."
			],
			"keyword": "nonn,base,frac",
			"offset": "1,1",
			"author": "_Eric W. Weisstein_, Nov 01 2004",
			"ext": [
				"More terms from _Rick L. Shepherd_, Jun 08 2008"
			],
			"references": 4,
			"revision": 25,
			"time": "2017-03-16T14:22:46-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}