{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A274498",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 274498,
			"data": "1,3,3,6,9,18,9,36,36,27,108,108,27,162,324,216,81,486,972,648,81,648,1944,2592,1296,243,1944,5832,7776,3888,243,2430,9720,19440,19440,7776,729,7290,29160,58320,58320,23328,729,8748,43740,116640,174960,139968,46656",
			"name": "Triangle read by rows: T(n,k) is the number of ternary words of length n having degree of asymmetry equal to k (n\u003e=0; 0\u003c=k\u003c=n/2).",
			"comment": [
				"The degree of asymmetry of a finite sequence of numbers is defined to be the number of pairs of symmetrically positioned distinct entries. Example: the degree of asymmetry of (2,7,6,4,5,7,3) is 2, counting the pairs (2,3) and (6,5).",
				"A sequence is palindromic if and only if its degree of asymmetry is 0.",
				"Sum(kT(n,k),k\u003e=0) = A274499(n)."
			],
			"formula": [
				"T(n,k) = 2^k*3^ceiling(n/2)*binomial(floor(n/2),k).",
				"G.f.: G(t,z) = (1 + 3z)/(1 - 3(1 + 2t)z^2).",
				"The row generating polynomials P[n] satisfy P[n] = 3(1 + 2t)P[n-2] (n\u003e=2). Easy to see if we note that the ternary words of length n (n\u003e=2) are 0w0, 0w1, 0w2, 1w0, 1w1, 1w2, 2w0, 2w1, 2w2, where w is a ternary word of length n - 2."
			],
			"example": [
				"From _Andrew Howroyd_, Jan 10 2018: (Start)",
				"Triangle begins:",
				"   1;",
				"   3;",
				"   3,   6;",
				"   9,  18;",
				"   9,  36,   36;",
				"  27, 108,  108;",
				"  27, 162,  324,  216;",
				"  81, 486,  972,  648;",
				"  81, 648, 1944, 2592, 1296;",
				"  ...",
				"(End)",
				"T(2,0) = 3 because we have 00, 11, and 22.",
				"T(2,1) = 6 because we have 01, 02, 10, 12, 20, and 21."
			],
			"maple": [
				"T := proc(n,k) options operator, arrow: 2^k*3^ceil((1/2)*n)*binomial(floor((1/2)*n), k) end proc: for n from 0 to 15 do seq(T(n, k), k = 0 .. floor((1/2)*n)) end do; # yields sequence in triangular form"
			],
			"mathematica": [
				"T[n_, k_] := 2^k 3^Ceiling[n/2] Binomial[Floor[n/2], k];",
				"Table[T[n, k], {n, 0, 12}, {k, 0, Floor[n/2]}] // Flatten (* _Jean-François Alcover_, Jan 04 2021 *)"
			],
			"program": [
				"(PARI)",
				"T(n,k) = 2^k*3^ceil(n/2)*binomial(floor(n/2),k);",
				"for(n=0, 10, for(k=0, n\\2, print1(T(n, k), \", \")); print); \\\\ _Andrew Howroyd_, Jan 10 2018"
			],
			"xref": [
				"Cf. A274496, A274497, A274499."
			],
			"keyword": "nonn,tabf",
			"offset": "0,2",
			"author": "_Emeric Deutsch_, Jul 27 2016",
			"references": 3,
			"revision": 14,
			"time": "2021-01-04T06:22:05-05:00",
			"created": "2016-07-27T21:13:12-04:00"
		}
	]
}