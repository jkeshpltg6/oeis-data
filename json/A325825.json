{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A325825",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 325825,
			"data": "1,1,1,1,1,1,1,1,1,1,1,1,3,1,1,1,1,1,1,1,1,1,1,1,4,1,1,1,1,1,3,1,1,3,1,1,1,1,1,1,5,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,4,5,3,5,4,3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,5,1,1,1,1,1,1,1,1,3,4,1,3,1,1,3,1,4,3,1,1",
			"name": "Square array giving the monic polynomial q satisfying q = gcd(P(x),P(y)) where P(x) and P(y) are polynomials in ring GF(3)[X] with coefficients in {0,1,2} given by the ternary expansions of x and y. The polynomial q is converted back to a ternary number, and then expressed in decimal.",
			"comment": [
				"Array is symmetric, and is read by antidiagonals, with (x,y) = (1,1), (1,2), (2,1), (1,3), (2,2), (3,1), ...",
				"If there is a polynomial q that satisfies q = gcd(P(x),P(y)), then also polynomial -q (which is obtained by changing all nonzero coefficients of q as 1 \u003c--\u003e 2, see A004488) satisfies the same relation, because there are two units (+1 and -1) in polynomial ring GF(3)[X]. Here we always choose the polynomial that is monic (i.e., with a leading coefficient +1), thus its base-3 encoding has \"1\" as its most significant digit, and the terms given here are all included in A132141."
			],
			"example": [
				"The array begins as:",
				"   y",
				"x      1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11,  12,  ...",
				"   --+-----------------------------------------------------",
				"   1 | 1,  1,  1,  1,  1,  1,  1,  1,  1,   1,  1,  1,  ...",
				"   2 | 1,  1,  1,  1,  1,  1,  1,  1,  1,   1,  1,  1,  ...",
				"   3 | 1,  1,  3,  1,  1,  3,  1,  1,  3,   1,  1,  3,  ...",
				"   4 | 1,  1,  1,  4,  1,  1,  1,  4,  1,   1,  4,  4,  ...",
				"   5 | 1,  1,  1,  1,  5,  1,  5,  1,  1,   1,  5,  1,  ...",
				"   6 | 1,  1,  3,  1,  1,  3,  1,  1,  3,   1,  1,  3,  ...",
				"   7 | 1,  1,  1,  1,  5,  1,  5,  1,  1,   1,  5,  1,  ...",
				"   8 | 1,  1,  1,  4,  1,  1,  1,  4,  1,   1,  4,  4,  ...",
				"   9 | 1,  1,  3,  1,  1,  3,  1,  1,  9,   1,  1,  3,  ...",
				"  10 | 1,  1,  1,  1,  1,  1,  1,  1,  1,  10,  1,  1,  ...",
				"  11 | 1,  1,  1,  4,  5,  1,  5,  4,  1,   1, 11,  4,  ...",
				"  12 | 1,  1,  3,  4,  1,  3,  1,  4,  3,   1,  4, 12,  ..."
			],
			"program": [
				"(PARI)",
				"up_to = 105;",
				"A004488(n) = subst(Pol(apply(x-\u003e(3-x)%3, digits(n, 3)), 'x), 'x, 3);",
				"A325825sq(a,b) = { my(a=fromdigits(Vec(lift(gcd(Pol(digits(a,3))*Mod(1, 3),Pol(digits(b,3))*Mod(1, 3)))),3), b=A004488(a)); min(a,b); };",
				"A325825list(up_to) = { my(v = vector(up_to), i=0); for(a=1,oo, for(col=1,a, i++; if(i \u003e up_to, return(v)); v[i] = A325825sq(col,(a-(col-1))))); (v); };",
				"v325825 = A325825list(up_to);",
				"A325825(n) = v325825[n];"
			],
			"xref": [
				"Cf. A004488, A091255, A132141, A325820, A325821, A325827.",
				"Central diagonal: A330740 (after its initial zero)."
			],
			"keyword": "nonn,tabl",
			"offset": "1,13",
			"author": "_Antti Karttunen_, May 22 2019",
			"references": 6,
			"revision": 41,
			"time": "2020-01-13T20:39:14-05:00",
			"created": "2020-01-13T20:39:14-05:00"
		}
	]
}