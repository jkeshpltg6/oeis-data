{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A132582",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 132582,
			"data": "1,1,2,1,5,3,5,1,19,14,25,6,50,14,19,1,167,148,282,84,617,215,307,20,1692,714,1075,84,1692,148,167,1,7580,7413,14678,5573,34563,15476,23590,2008,109041,59273,95798,9673,163415,18452,21367,168,580655,387651,668175,82404,1226845,169396,201394,2008",
			"name": "First differences of A132581.",
			"comment": [
				"a(n) is the number of antichains containing n when the elements of the infinite Boolean lattice are represented by 0, 1, 2, ... - _Robert Israel_, Mar 08 2017, corrected and edited by _M. F. Hasler_, Jun 01 2021",
				"When the elements of the Boolean lattice are considered to be subsets of {0, 1, 2, ...} with the inclusion relation, then an integer n (as in \"containing n\" in the above comment) means the set whose characteristic function is the binary expansion of n, for example, n = 3 = 11[2] represents the set {0, 1} and n = 4 = 100[2] represents the set {2}. See A132581 for details and examples. - _M. F. Hasler_, Jun 01 2021",
				"The terms come in groups of length 2^k, k \u003e= 0, delimited by the '1's at indices 2^k-1. Within each group, there is a symmetry: a(2^k - 1 + 2^m) = a(2^(k+1) - 1 - 2^m) for 0 \u003c= m \u003c k. The smallest term within each group is exactly in the middle (m = k-1), and equals A000372(k-1). A similar pattern holds recursively: the smallest term within the first half (and also within the second half) of the group is again exactly in the middle of that range, and so on. - _M. F. Hasler_, Jun 01 2021"
			],
			"link": [
				"J. M. Aranda, \u003ca href=\"/A132582/b132582.txt\"\u003eTable of n, a(n) for n = 0..211\u003c/a\u003e (first 91 terms from Robert Israel; terms 91..160 from Peter Koehler)",
				"J. M. Aranda, \u003ca href=\"/A132582/a132582_1.cpp.txt\"\u003eC++ program\u003c/a\u003e",
				"J. Berman and P. Köhler, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL24/Koehler/koehler5.html\"\u003eOn Dedekind Numbers and Two Sequences of Knuth\u003c/a\u003e, J. Int. Seq., Vol. 24 (2021), Article 21.10.7."
			],
			"formula": [
				"From _M. F. Hasler_, Jun 01 2021: (Start)",
				"a(n) = A132581(n+1) - A132581(n) for n \u003e= 0, by definition.",
				"a(2^(k+1) - 2^m -1) = a(2^k + 2^m -1) = A132581(2^k - 2^m) for all k \u003e m \u003e= 0.",
				"a(3*2^k -1) = A132581(2^k) = A000372(k), for all k \u003e= 0.",
				"a(2^k -1) = A132581(0) =1, for all k\u003e=0.",
				"(End)",
				"From _J. M. Aranda_, Jun 09 2021: (Start)",
				"A132581(2^k) = a(2^k + 2^((k-1)/2) -1) + a(2^k +2^((k+1)/2) -1), for k odd, k\u003e0.",
				"A132581(2^k)= 2*a(2^k + 2^(k/2) -1), for k even, k\u003e=0.",
				"(End)"
			],
			"maple": [
				"N:= 63:",
				"Q:= [seq(convert(n+64,base,2),n=0..N)]:",
				"Incomp:= Array(0..N,0..N,proc(i,j) local d; d:= Q[i+1]-Q[j+1]; has(d,1) and",
				"  has(d,-1) end proc):",
				"AntichainCount:= proc(S) option cache; local t,r;",
				"1 + add(procname(select(s -\u003e Incomp[s,S[t]],S[1..t-1]))  , t = 1..nops(S));",
				"end proc:",
				"seq(AntichainCount(select(s -\u003e Incomp[s,n], [$1..n-1])), n=0..N); # _Robert Israel_, Mar 08 2017"
			],
			"program": [
				"(PARI) apply( A132582(n,e=exponent(n))={if(n++\u003c3 || n==2\u003c\u003ce, 1, setsearch([1,2]\u003c\u003ce+[1,-1]\u003c\u003cvaluation(n,2),n), A132581(2^e-2^valuation(n,2)), A132581(n)-A132581(n-1))}, [0..10]) \\\\ _M. F. Hasler_, Jun 03 2021"
			],
			"xref": [
				"See A132581 for further information."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Don Knuth_, Nov 18 2007",
			"ext": [
				"More terms from _Robert Israel_, Mar 08 2017",
				"Extended by _Peter Koehler_, Jul 07 2017"
			],
			"references": 3,
			"revision": 79,
			"time": "2021-12-29T09:51:27-05:00",
			"created": "2007-12-09T03:00:00-05:00"
		}
	]
}