{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008636",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8636,
			"data": "1,1,2,3,5,7,11,15,21,28,38,49,65,82,105,131,164,201,248,300,364,436,522,618,733,860,1009,1175,1367,1579,1824,2093,2400,2738,3120,3539,4011,4526,5102,5731,6430,7190,8033,8946,9953,11044,12241,13534,14950,16475,18138",
			"name": "Number of partitions of n into at most 7 parts.",
			"comment": [
				"Also, the number of partitions of n into parts \u003c= 7: a(n) = A026820(n, 7). - _Reinhard Zumkeller_, Jan 21 2010",
				"Counts unordered closed walks of weight n on a single vertex graph with 7 loops of weights 1, 2, 3, 4, 5, 6 and 7. - _David Neil McGrath_, Apr 11 2015",
				"Number of different distributions of n+28 identical balls in 7 boxes as x,y,z,p,q,m,n where 0 \u003c x \u003c y \u003c z \u003c p \u003c q \u003c m \u003c n. - _Ece Uslu_ and Esin Becenen, Jan 11 2016"
			],
			"reference": [
				"A. Cayley, Collected Mathematical Papers. Vols. 1-13, Cambridge Univ. Press, London, 1889-1897, Vol. 10, p. 415.",
				"H. Gupta et al., Tables of Partitions. Royal Society Mathematical Tables, Vol. 4, Cambridge Univ. Press, 1958, p. 2."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A008636/b008636.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=356\"\u003eEncyclopedia of Combinatorial Structures 356\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_28\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,1,0,0,-1,0,-1,-1,0,1,1,2,0,0,0,-2,-1,-1,0,1,1,0,1,0,0,-1,-1,1)."
			],
			"formula": [
				"G.f.: 1/((1-x)*(1-x^2)*(1-x^3)*(1-x^4)*(1-x^5)*(1-x^6)*(1-x^7)).",
				"a(n) = A008284(n+7, 7), n \u003e= 0.",
				"a(n) = a(n-1) + a(n-2) - a(n-5) - a(n-7) - a(n-8) + a(n-10) + a(n-11) + 2*a(n-12) - 2*a(n-16) - a(n-17) - a(n-18) + a(n-20) + a(n-21) + a(n-23) - a(n-26) - a(n-27) + a(n-28). - _David Neil McGrath_, Apr 11 2015",
				"a(n+7) = a(n) + A001402(n). - _Ece Uslu_, Esin Becenen, Jan 11 2016",
				"a(n) = A026813(n+7). - _R. J. Mathar_, Feb 13 2019"
			],
			"example": [
				"There are 28 partitions of 9 into parts less than or equal to 7. These are (72)(711)(63)(621)(6111)(54)(531)(522)(5211)(51111)(441)(432)(4311)(4221)(42111)(411111)(333)(3321)(33111)(3222)(32211)(321111)(3111111)(22221)(222111)(2211111)(21111111)(111111111). - _David Neil McGrath_, Apr 11 2015",
				"a(3) = 3, i.e., {1,2,3,4,5,7,9}, {1,2,3,4,6,7,8}, {1,2,3,4,5,6,10}. Number of different distributions of 31 identical balls in 7 boxes as x,y,z,p,q,m,n where 0 \u003c x \u003c y \u003c z \u003c p \u003c q \u003c m \u003c n. - _Ece Uslu_, Esin Becenen, Jan 11 2016"
			],
			"maple": [
				"with(combstruct):ZL8:=[S,{S=Set(Cycle(Z,card\u003c8))}, unlabeled]: seq(count(ZL8,size=n),n=0..48); # _Zerinvary Lajos_, Sep 24 2007",
				"B:=[S,{S = Set(Sequence(Z,1 \u003c= card),card \u003c=7)},unlabelled]: seq(combstruct[count](B, size=n), n=0..48); # _Zerinvary Lajos_, Mar 21 2009"
			],
			"mathematica": [
				"CoefficientList[ Series[ 1/ Product[ 1 - x^n, {n, 1, 7} ], {x, 0, 60} ], x ]"
			],
			"program": [
				"(PARI) {a(n)=(2*n^6+168*n^5+5530*n^4+90160*n^3+754299*n^2+(2988020+44800*(1-n%3))*n+6654375+1575*(3*n^2+84*n+511)*(-1)^n)\\7257600}; \\\\ _Tani Akinari_, May 27 2014"
			],
			"xref": [
				"Cf. A008284, A026820."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_, Mar 15 1996",
			"ext": [
				"More terms from _Robert G. Wilson v_, Dec 11 2000"
			],
			"references": 6,
			"revision": 55,
			"time": "2021-01-26T03:52:55-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}