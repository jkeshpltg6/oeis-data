{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A181318",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 181318,
			"data": "0,1,1,9,1,25,9,49,4,81,25,121,9,169,49,225,16,289,81,361,25,441,121,529,36,625,169,729,49,841,225,961,64,1089,289,1225,81,1369,361,1521,100,1681,441,1849,121,2025,529,2209,144,2401,625,2601,169,2809,729",
			"name": "a(n) = A060819(n)^2.",
			"comment": [
				"The first sequence, p=0, of the family A060819(n)*A060819(n+p).",
				"Hence array",
				"p=0:  0, 1, 1,  9,  1, 25,  9,  49,    a(n)=A060819(n)^2,",
				"p=1:  0, 1, 3,  3,  5, 15, 21,  14,     A064038(n),",
				"p=2:  0, 3, 1, 15,  3, 35,  6,  63,     A198148(n),",
				"p=3:  0, 1, 5,  9,  7, 10, 27,  35,     A160050(n),",
				"p=4:  0, 5, 3, 21,  2, 45, 15,  77,     A061037(n),",
				"p=5:  0, 3, 7,  6,  9, 25, 33,  21,     A178242(n),",
				"p=6:  0, 7, 2, 27,  5, 55,  9,  91,     A217366(n),",
				"p=7:  0, 2, 9, 15, 11, 15, 39,  49,     A217367(n),",
				"p=8:  0, 9, 5, 33,  3, 65, 21, 105,     A180082(n).",
				"Compare columns 2, 3 and 5, columns 4 and 7 and columns 6 and 8.",
				"From _Peter Bala_, Feb 19 2019: (Start)",
				"We make some general remarks about the sequence a(n) = numerator(n^2/(n^2 + k^2)) = (n/gcd(n,k))^2 for k a fixed positive integer (we suppress the dependence of a(n) on k). The present sequence corresponds to the case k = 4.",
				"a(n) is a quasi-polynomial in n. In fact, a(n) = n^2/b(n) where b(n) = gcd(n^2,k^2) is a purely periodic sequence in n.",
				"In addition to being multiplicative these sequences are also strong divisibility sequences, that is, gcd(a(n),a(m)) = a(gcd(n,m)) for n, m \u003e= 1. In particular, it follows that a(n) is a divisibility sequence: if n divides m then a(n) divides a(m).",
				"By the multiplicativeness and strong divisibility property of the sequence a(n) it follows that if gcd(n,m) = 1 then a( a(n)*a(m) ) = a(a(n)) * a(a(m)), a( a(a(n))*a(a(m)) ) = a(a(a(n))) * a(a(a(m))) and so on.",
				"The sequence a(n) has the rational generating function Sum_{d divides k} f(d)*F(x^d), where F(x) = x*(1 + x)/(1 - x)^3 = x + 4*x^2 + 9*x^3 + 16*x^4 + ... is the o.g.f. for the squares A000290, and where f(n) is the Dirichlet inverse of the Jordan totient function J_2(n) - see A007434. The function f(n) is multiplicative and is defined on prime powers p^k by f(p^k) = (1 - p^2). See A046970. Cf. A060819. (End)"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A181318/b181318.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_12\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,0,0,3,0,0,0,-3,0,0,0,1)."
			],
			"formula": [
				"a(2*n) = A168077(n), a(2*n+1) = A016754(n).",
				"a(n) = 3*a(n-4) - 3*a(n-8) + a(n-12).",
				"G.f.: x*(1 + x + 9*x^2 + x^3 + 22*x^4 + 6*x^5 + 22*x^6 + x^7 + 9*x^8 + x^9 + x^10)/(1-x^4)^3. - _R. J. Mathar_, Mar 10 2011",
				"From _Peter Bala_, Feb 19 2019: (Start)",
				"a(n) = numerator(n^2/(n^2 + 16)) = n^2/(gcd(n^2,16)) = (n/gcd(n,4))^2.",
				"a(n) = n^2/b(n), where b(n) = [1, 4, 1, 16, 1, 4, 1, 16, ...] is a purely periodic sequence of period 4.",
				"a(n) is a quasi-polynomial in n: a(4*n) = n^2; a(4*n + 1) = (4*n + 1)^2; a(4*n + 2) = (2*n + 1)^2; a(4*n + 3) = (4*n + 3)^2.",
				"O.g.f.: Sum_{d divides 4} A046970(d)*x^d*(1 + x^d)/(1 - x^d)^3 = x*(1 + x)/(1 - x)^3 - 3*x^2*(1 + x^2)/(1 - x^2)^3 - 3*x^4*(1 + x^4)/(1 - x^4)^3. (End)"
			],
			"maple": [
				"a:=n-\u003en^2/gcd(n,4)^2: seq(a(n),n=0..60); # _Muniru A Asiru_, Feb 20 2019"
			],
			"mathematica": [
				"Table[n^2/GCD[n,4]^2, {n,0,100}] (* _G. C. Greubel_, Sep 19 2018 *)"
			],
			"program": [
				"(PARI) a(n)=n^2/gcd(n,4)^2 \\\\ _Charles R Greathouse IV_, Dec 21 2011",
				"(MAGMA) [n^2/GCD(n,4)^2: n in [0..100]]; // _G. C. Greubel_, Sep 19 2018",
				"(Sage) [n^2/gcd(n, 4)^2 for n in (0..100)] # _G. C. Greubel_, Feb 20 2019"
			],
			"xref": [
				"Cf. A181829, A046970, A007434, A060819, A168077."
			],
			"keyword": "nonn,mult,easy",
			"offset": "0,4",
			"author": "_Paul Curtz_, Jan 26 2011",
			"ext": [
				"Edited by _Jean-François Alcover_, Oct 01 2012 and Jan 15 2013",
				"More terms from _Michel Marcus_, Jun 09 2014"
			],
			"references": 13,
			"revision": 49,
			"time": "2019-03-06T16:03:14-05:00",
			"created": "2010-11-12T14:30:23-05:00"
		}
	]
}