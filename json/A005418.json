{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005418",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5418,
			"id": "M0771",
			"data": "1,2,3,6,10,20,36,72,136,272,528,1056,2080,4160,8256,16512,32896,65792,131328,262656,524800,1049600,2098176,4196352,8390656,16781312,33558528,67117056,134225920,268451840,536887296,1073774592,2147516416,4295032832",
			"name": "Number of (n-1)-bead black-white reversible strings; also binary grids; also row sums of Losanitsch's triangle A034851; also number of caterpillar graphs on n nodes.",
			"comment": [
				"Equivalently, walks on triangle, visiting n+2 vertices, so length n+1, n \"corners\"; the symmetry group is S3, reversing a walk does not count as different. Walks are not self-avoiding. - _Colin Mallows_",
				"Slavik V. Jablan observes that this is also the number of rational knots and links with n+2 crossings (cf. A018240). See reference. [Corrected by _Andrey Zabolotskiy_, Jun 18 2020]",
				"Number of bit strings of length (n-1), not counting strings which are the end-for-end reversal or the 0-for-1 reversal of each other as different. - Carl Witty (cwitty(AT)newtonlabs.com), Oct 27 2001",
				"The formula given in page 1095 of the Balasubramanian reference can be used to derive this sequence. - _Parthasarathy Nambi_, May 14 2007",
				"Also number of compositions of n up to direction, where a composition is considered equivalent to its reversal, see example. - _Franklin T. Adams-Watters_, Oct 24 2009",
				"Number of normally non-isomorphic realizations of the associahedron of type I starting with dimension 2 in Ceballos et al. - _Tom Copeland_, Oct 19 2011",
				"Number of fibonacenes with n+2 hexagons. See the Balaban and the Dobrynin references. - _Emeric Deutsch_, Apr 21 2013",
				"From the point of view of binary grids, it is a (1,n)-rectangular grid. A225826 to A225834 are the numbers of binary pattern classes in the (m,n)-rectangular grid, 1 \u003c m \u003c 11. - _Yosu Yurramendi_, May 19 2013",
				"Number of n-vertex difference graphs (bipartite 2K_2-free graphs) [Peled \u0026 Sun, Thm. 9]. - _Falk Hüffner_, Jan 10 2016",
				"The offset should be 0, since the first row of A034851 is row 0. The name would then be: \"Number of n bead...\". - _Daniel Forgues_, Jul 26 2018",
				"a(n) is the number of non-isomorphic generalized rigid ladders with n cells. A generalized rigid ladder with n cells is a graph with vertex set is the union of {u_0, u_1, ..., u_n} and {v_0, v_1, ..., v_n}, and for every 0 \u003c= i \u003c= n-1, the edges are of the form {u_i,u_i+1}, {v_i, v_i+1}, {u_i,v_i} and either {u_i,v_i+1} or {u_i+1,v_i}. - _Christian Barrientos_, Jul 29 2018",
				"Also number of non-isomorphic stairs with n+1 cells. A stair is a snake polyomino allowing only two directions for adjacent cells: east and north. - _Christian Barrientos_ and _Sarah Minion_, Jul 29 2018",
				"From _Robert A. Russell_, Oct 28 2018: (Start)",
				"There are two different unoriented row colorings using two colors that give us very similar results here, a difference of one in the offset. In an unoriented row, chiral pairs are counted as one.",
				"a(n) is the number of color patterns (set partitions) of an unoriented row of length n using two or fewer colors (subsets). Two color patterns are equivalent if the colors are permutable.",
				"a(n+1) is the number of ways to color an unoriented row of length n using two noninterchangeable colors (one need not use both colors).",
				"See the examples below of these two different colorings. (End)",
				"Also arises from the enumeration of types of based polyhedra with exactly two triangular faces [Rademacher]. - _N. J. A. Sloane_, Apr 24 2020"
			],
			"reference": [
				"K. Balasubramanian, \"Combinatorial Enumeration of Chemical Isomers\", Indian J. Chem., (1978) vol. 16B, pp. 1094-1096. See page 1095.",
				"Wayne M. Dymacek, Steinhaus graphs. Proceedings of the Tenth Southeastern Conference on Combinatorics, Graph Theory and Computing (Florida Atlantic Univ., Boca Raton, Fla., 1979), pp. 399--412, Congress. Numer., XXIII-XXIV, Utilitas Math., Winnipeg, Man., 1979. MR0561065 (81f:05120)",
				"Jablan S. and Sazdanovic R., LinKnot: Knot Theory by Computer, World Scientific Press, 2007.",
				"Joseph S. Madachy: Madachy's Mathematical Recreations. New York: Dover Publications, Inc., 1979, p. 46 (first publ. by Charles Scribner's Sons, New York, 1966, under the title: Mathematics on Vacation)",
				"C. A. Pickover, Keys to Infinity, Wiley 1995, p. 75.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A005418/b005418.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"M. Archibald, A. Blecher, A. Knopfmacher, and M. E. Mays, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL23/Archibald/arch3.html\"\u003eInversions and Parity in Compositions of Integers\u003c/a\u003e, J. Int. Seq., Vol. 23 (2020), Article 20.4.1.",
				"Joerg Arndt, \u003ca href=\"http://www.jjj.de/fxt/#fxtbook\"\u003eMatters Computational (The Fxtbook)\u003c/a\u003e, pp. 151 and 733.",
				"Andrei Asinowski and Alon Regev, \u003ca href=\"https://www.emis.de/journals/INTEGERS/papers/q5/q5.Abstract.html\"\u003eTriangulations with Few Ears: Symmetry Classes and Disjointness\u003c/a\u003e, Integers 16 (2016), #A5.",
				"A. T. Balaban, \u003ca href=\"http://match.pmf.kg.ac.rs/electronic_versions/Match24/match24_29-38.pdf\"\u003eChemical graphs. Part 50. Symmetry and enumeration of fibonacenes (unbranched catacondensed benzenoids isoarithmic with helicenes and zigzag catafusenes)\u003c/a\u003e, MATCH: Commun. Math. Comput. Chem., 24 (1989) 29-38.",
				"A. P. Burger, M. Van Der Merwe, and J. H. Van Vuuren, \u003ca href=\"https://doi.org/10.1016/j.dam.2012.04.022\"\u003eAn asymptotic analysis of the evolutionary spatial prisoner’s dilemma on a path\u003c/a\u003e, Discrete Appl. Math. 160, No. 15, 2075-2088 (2012), Table 3.1.",
				"C. Ceballos, F. Santos, and G. Ziegler, \u003ca href=\"http://arxiv.org/abs/1109.5544\"\u003eMany Non-equivalent Realizations of the Associahedron\u003c/a\u003e, arXiv:1109.5544 [math.MG], 2011-2013; pp. 15 and 26.",
				"Jacob Crabtree, \u003ca href=\"https://arxiv.org/abs/1810.11744\"\u003eAnother Enumeration of Caterpillar Trees\u003c/a\u003e, arXiv:1810.11744 [math.CO], 2018.",
				"S. J. Cyvin, B. N. Cyvin, J. Brunvoll, E. Brendsdal, Zhang Fuji, Guo Xiaofeng, and R. Tosic, \u003ca href=\"http://dx.doi.org/10.1021/ci00013a027\"\u003eTheory of polypentagons\u003c/a\u003e, J. Chem. Inf. Comput. Sci., 33 (1993), 466-474.",
				"A. A. Dobrynin, \u003ca href=\"http://match.pmf.kg.ac.rs/electronic_versions/Match64/n3/match64n3_707-726.pdf\"\u003eOn the Wiener index of fibonacenes\u003c/a\u003e, MATCH: Commun. Math. Comput. Chem, 64 (2010), 707-726.",
				"Sahir Gill, \u003ca href=\"https://doi.org/10.12988/ijma.2018.8537\"\u003eBounds for Region Containing All Zeros of a Complex Polynomial\u003c/a\u003e, International Journal of Mathematical Analysis (2018), Vol. 12, No. 7, 325-333.",
				"T. A. Gittings, \u003ca href=\"http://www.arXiv.org/abs/math.GT/0401051\"\u003eMinimum braids: a complete invariant of knots and links\u003c/a\u003e, arXiv:math/0401051 [math.GT], 2004. - _N. J. A. Sloane_, Jan 18 2013",
				"R. K. Guy, \u003ca href=\"/A005418/a005418.pdf\"\u003eLetter to N. J. A. Sloane, Nov 1978\u003c/a\u003e.",
				"Frank Harary and Allen J. Schwenk, \u003ca href=\"https://doi.org/10.1016/0012-365X(73)90067-8\"\u003eThe number of caterpillars\u003c/a\u003e, Discrete Mathematics, Volume 6, Issue 4, 1973, 359-365.",
				"N. Hoffman, \u003ca href=\"http://www.jstor.org/stable/3026472\"\u003eBinary grids and a related counting problem\u003c/a\u003e, Two-Year College Math. J. 9 (1978), 267-272.",
				"S. V. Jablan, \u003ca href=\"http://www.emis.de/journals/NSJOM/Papers/29_3/NSJOM_29_3_121_139.pdf\"\u003eGeometry of Links\u003c/a\u003e, XII Yugoslav Geometric Seminar (Novi Sad, 1998), Novi Sad J. Math. 29 (1999), no. 3, 121-139.",
				"S. M. Losanitsch, \u003ca href=\"/A000602/a000602_1.pdf\"\u003eDie Isomerie-Arten bei den Homologen der Paraffin-Reihe\u003c/a\u003e, Chem. Ber. 30 (1897), 1917-1926. (Annotated scanned copy)",
				"Isaac B. Michael and M. R. Sepanski, \u003ca href=\"https://ajc.maths.uq.edu.au/pdf/66/ajc_v66_p192.pdf\"\u003eNet regular signed trees\u003c/a\u003e, Australasian Journal of Combinatorics, Volume 66(2) (2016), 192-204.",
				"U. N. Peled and F. Sun, \u003ca href=\"http://dx.doi.org/10.1016/0166-218X(94)00061-H\"\u003eEnumeration of difference graphs\u003c/a\u003e, Discrete Appl. Math., 60 (1995), 311-318.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"Hans Rademacher, \u003ca href=\"https://doi.org/10.1215/ijm/1256068140\"\u003eOn the number of certain types of polyhedra\u003c/a\u003e, Illinois Journal of Mathematics 9.3 (1965): 361-380. Reprinted in Coll. Papers, Vol II, MIT Press, 1974, pp. 544-564. See Theorem 8, Eq. 14.3.",
				"A. Regev, \u003ca href=\"http://arxiv.org/abs/1309.0743\"\u003eRemarks on two-eared triangulations\u003c/a\u003e, arXiv preprint arXiv:1309.0743 [math.CO], 2013-2014.",
				"N. J. A. Sloane, \u003ca href=\"/classic.html#LOSS\"\u003eClassic Sequences\u003c/a\u003e",
				"R. A. Sulanke, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL3/SULANKE/sulanke.html\"\u003eMoments of generalized Motzkin paths\u003c/a\u003e, J. Integer Sequences, Vol. 3 (2000), Article #00.1.1.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/BarkerCode.html\"\u003eBarker Code\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/BishopsProblem.html\"\u003eBishops Problem\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Caterpillar.html\"\u003eCaterpillar Graph\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/LosanitschsTriangle.html\"\u003eLosanitsch's Triangle\u003c/a\u003e.",
				"A. Yajima, \u003ca href=\"https://doi.org/10.1246/bcsj.20140204\"\u003eHow to calculate the number of stereoisomers of inositol-homologs\u003c/a\u003e, Bull. Chem. Soc. Jpn. 87 (2014), 1260-1264; see Tables 1 and 2 (and text). - _N. J. A. Sloane_, Mar 26 2015",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,2,-4)."
			],
			"formula": [
				"a(n) = 2^(n-2) + 2^(floor(n/2) - 1).",
				"G.f.: -x*(-1 + 3*x^2) / ( (2*x - 1)*(2*x^2 - 1) ). - _Simon Plouffe_ in his 1992 dissertation",
				"G.f.: x*(1+2*x)*(1-3*x^2)/((1-4*x^2)*(1-2*x^2)), not reduced. - _Wolfdieter Lang_, May 08 2001",
				"a(n) = 6*a(n - 2) - 8*a(n - 4). a(2*n) = A063376(n - 1) = 2*a(2*n - 1); a(2*n + 1) = A007582(n). - _Henry Bottomley_, Jul 14 2001",
				"a(n+2) = 2*a(n+1) - A077957(n) with a(1) = 1, a(2) = 2. - _Yosu Yurramendi_, Oct 24 2008",
				"a(n) = 2*a(n-1) + 2*a(n-2) - 4*a(n-3). - _Jaume Oliver Lafont_, Dec 05 2008",
				"Union of A007582 and A161168. Union of A007582 and A063376. - _Jaroslav Krizek_, Aug 14 2009",
				"G.f.: G(0); G(k) = 1 + 2*x/(1 - x*(1+2^(k+1))/(x*(1+2^(k+1)) + (1+2^k)/G(k+1))); (continued fraction). - _Sergei N. Gladkovskii_, Dec 12 2011",
				"a(2*n) = 2*a(2*n-1) and a(2*n+1) = a(2*n) + 4^(n-1) with a(1) = 1. - _Johannes W. Meijer_, Aug 26 2013",
				"From _Robert A. Russell_, Oct 28 2018: (Start)",
				"a(n) = (A131577(n) + A016116(n)) / 2 = A131577(n) - A122746(n-3) = A122746(n-3) + A016116(n), for set partitions with up to two subsets.",
				"a(n+1) = (A000079(n) + A060546(n)) / 2 = A000079(n) - A122746(n-2) = A122746(n-2) + A060546(n), for two colors that do not permute.",
				"a(n) = Sum_{j=0..k} (S2(n,j) + Ach(n,j)) / 2, where k=2 is the maximum number of colors, S2(n,k) is the Stirling subset number A008277, and Ach(n,k) = [n\u003e=0 \u0026 n\u003c2 \u0026 n==k] + [n\u003e1]*(k*Ach(n-2,k) + Ach(n-2,k-1) + Ach(n-2,k-2)).",
				"a(n+1) = (k^n + k^ceiling(n/2)) / 2, where k=2 is number of colors we can use. (End)"
			],
			"example": [
				"a(5) = 10 because there are 16 compositions of 5 (shown  as \u003cvectors\u003e) but only 10 equivalence classes (shown as {sets}):  {\u003c5\u003e}, {\u003c4,1\u003e,\u003c1,4\u003e}, {\u003c3,2\u003e,\u003c2,3\u003e}, {\u003c3,1,1\u003e,\u003c1,1,3\u003e}, {\u003c1,3,1\u003e},{\u003c2,2,1\u003e,\u003c1,2,2\u003e}, {\u003c2,1,2\u003e}, {\u003c2,1,1,1\u003e,\u003c1,1,1,2\u003e}, {\u003c1,2,1,1\u003e,\u003c1,1,2,1\u003e}, {\u003c1,1,1,1,1\u003e}. - _Geoffrey Critzer_, Nov 02 2012",
				"G.f. = x + 2*x^2 + 3*x^3 + 6*x^4 + 10*x^5 + 20*x^6 + 36*x^7 + 72*x^8 + ... - _Michael Somos_, Jun 24 2018",
				"From _Robert A. Russell_, Oct 28 2018: (Start)",
				"For a(5)=10, the 4 achiral patterns (set partitions) are AAAAA, AABAA, ABABA, and ABBBA. The 6 chiral pairs are AAAAB-ABBBB, AAABA-ABAAA, AAABB-AABBB, AABAB-ABABB, AABBA-ABBAA, and ABAAB-ABBAB. The colors are permutable.",
				"For n=4 and a(n+1)=10, the 4 achiral colorings are AAAA, ABBA, BAAB, and BBBB. The 6 achiral pairs are AAAB-BAAA, AABA-ABAA, AABB-BBAA, ABAB-BABA, ABBB-BBBA, and BABB-BBAB. The colors are not permutable. (End)"
			],
			"maple": [
				"A005418 := n-\u003e2^(n-2)+2^(floor(n/2)-1): seq(A005418(n), n=1..34);"
			],
			"mathematica": [
				"LinearRecurrence[{2,2,-4}, {1,2,3}, 40] (* or *) Table[2^(n-2)+2^(Floor[n/2]-1), {n,40}] (* _Harvey P. Dale_, Jan 18 2012 *)"
			],
			"program": [
				"(Haskell)",
				"a005418 n = sum $ a034851_row (n - 1) -- _Reinhard Zumkeller_, Jan 14 2012",
				"(PARI) A005418(n)= 2^(n-2) + 2^(n\\2-1); \\\\ _Joerg Arndt_, Sep 16 2013"
			],
			"xref": [
				"Column 2 of A320750 (set partitions).",
				"Cf. A131577 (oriented), A122746(n-3) (chiral), A016116 (achiral), for set partitions with up to two subsets.",
				"Column 2 of A277504, offset by one (colors not permutable).",
				"Cf. A000079 (oriented), A122746(n-2) (chiral), and A060546 (achiral), for a(n+1).",
				"Cf. A001998, A001444, A051436, A051437, A007582, A001445, A032085."
			],
			"keyword": "nonn,easy,nice",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, _R. K. Guy_",
			"references": 69,
			"revision": 261,
			"time": "2021-11-22T02:27:24-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}