{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A333713",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 333713,
			"data": "1,6,18,40,70,108,72,42,20,21,44,45,75,114,160,216,280,350,351,352,432,520,616,720,832,952,1080,1216,1360,1512,1672,1840,2016,2200,2392,2592,2800,3016,3240,3472,3710,3956,4212,4476,4746,5024,5310,5022,4743,4472,4473,4209,4208,3952,3705",
			"name": "Squares visited by a chess king moving on a square-spiral numbered board where the king moves to the adjacent unvisited square containing the spiral number with the most divisors. In case of a tie it chooses the square with the lowest spiral number.",
			"comment": [
				"This sequences gives the numbers of the squares visited by a chess king moving on a square-spiral numbered board where the king starts on the 1 numbered square and at each step moves to an adjacent unvisited square, out of the eight adjacent neighboring squares, which contains the number with the most divisors. If two or more adjacent squares exist with the same highest number of divisors then the square with the lowest spiral number is chosen. Note that if the king simply moves to the highest available number the sequence will be infinite as the king will step along the south-east diagonal from square 1 forever.",
				"The sequence is finite. After 1784 steps the square with number 1478 is visited, after which all adjacent neighboring squares have been visited.",
				"Due to the king's preference for squares with the most divisors it will avoid prime numbers unless no other choice exists. Of the 1784 visited squares only 27 contain prime numbers while 1757 contain composites. As even numbers \u003e= 6 will always contain 4 or more divisors the king will tend to visit more even numbers than odd numbers; in the 1784 visited squares 1289 contain an even number while 495 contain an odd number. As the even numbers are diagonally adjacent in the square spiral the king's path will be dominated by diagonal steps, often taking numerous diagonal steps is succession - see the attached link image.",
				"The largest visited square is a(390) = 17664. The lowest unvisited square is 2."
			],
			"link": [
				"Scott R. Shannon, \u003ca href=\"/A333713/b333713.txt\"\u003eTable of n, a(n) for n = 1..1785\u003c/a\u003e",
				"Scott R. Shannon, \u003ca href=\"/A333713/a333713.png\"\u003eImage showing the 1784 steps of the king's path\u003c/a\u003e. A green dot marks the starting 1 square and a red dot the final square with number 1478. The red dot is surrounded by eight blue dots to show the occupied neighboring squares. A yellow dots marks the smallest unvisited square with number 2."
			],
			"example": [
				"The board is numbered with the square spiral:",
				".",
				"  17--16--15--14--13   .",
				"   |               |   .",
				"  18   5---4---3  12   29",
				"   |   |       |   |   |",
				"  19   6   1---2  11   28",
				"   |   |           |   |",
				"  20   7---8---9--10   27",
				"   |                   |",
				"  21--22--23--24--25--26",
				".",
				"a(1) = 1, the starting square for the king.",
				"a(2) = 6. The eight unvisited squares around a(1) the king can move to are numbered 2,3,4,5,6,7,8,9. Of these 6 and 8 both have the maximum four divisors, and of those 6 is the smallest.",
				"a(3) = 18. The seven unvisited squares around a(2) = 6 the king can move to are numbered 4,5,18,19,20,7,8. Of these 18 and 20 have the maximum six divisors, and of those 18 is the smallest.",
				"a(603) = 821. This is the first prime number visited; a(602) = 939 has square 821 as the sole unvisited adjacent neighbor."
			],
			"xref": [
				"Cf. A333714 (choose highest spiral number in case of tie), A335816, A316667, A330008, A329520, A326922, A328928, A328929."
			],
			"keyword": "nonn,walk,fini,full",
			"offset": "1,2",
			"author": "_Scott R. Shannon_, Jul 02 2020",
			"references": 6,
			"revision": 48,
			"time": "2020-07-10T03:39:10-04:00",
			"created": "2020-07-02T23:53:34-04:00"
		}
	]
}