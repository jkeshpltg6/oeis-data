{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A260158",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 260158,
			"data": "1,3,4,6,7,6,10,12,13,15,14,18,18,21,22,18,25,27,28,24,26,33,34,42,37,30,36,42,43,45,38,48,49,42,54,42,56,57,58,60,43,63,64,66,67,63,70,60,73,84,62,78,79,72,72,66,90,87,88,90,74,78,98,96,97,78",
			"name": "Expansion of psi(x)^4 * psi(-x^3) / f(x) in powers of x where psi, f() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A260158/b260158.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-5/6) * eta(q^2)^5 * eta(q^3) * eta(q^4) * eta(q^12) / (eta(q)^3 * eta(q^6)) in powers of q.",
				"Euler transform of period 12 sequence [ 3, -2, 2, -3, 3, -2, 3, -3, 2, -2, 3, -4, ...].",
				"4 * a(n) = A260109(3*n + 2) = A124815(6*n + 5).",
				"a(2*n + 1) = 3 * A260295(n)."
			],
			"example": [
				"G.f. = 1 + 3*x + 4*x^2 + 6*x^3 + 7*x^4 + 6*x^5 + 10*x^6 + 12*x^7 + 13*x^8 + ...",
				"G.f. = q^7 + 3*q^23 + 4*q^39 + 6*q^55 + 7*q^71 + 6*q^87 + 10*q^103 + 12*q^119 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 0, 0, With[ {m = 6 n + 5}, DivisorSum[ m, m/# KroneckerSymbol[ 12, #]\u0026] / 4]];",
				"a[ n_] := SeriesCoefficient[ 2^(-9/2) x^(-7/8) EllipticTheta[ 2, 0, x^(1/2)]^4 EllipticTheta[ 2, Pi/4, x^(3/2)] / QPochhammer[ -x], {x, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = my(m); if( n\u003c0, 0, m = 6*n + 5; sumdiv( m, d, m/d * kronecker( 12, d)) / 4)};",
				"(PARI) {a(n) = if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^5 * eta(x^3 + A) * eta(x^4 + A) * eta(x^12 + A) / (eta(x + A)^3 * eta(x^6 + A)), n))};"
			],
			"xref": [
				"Cf. A124815, A260109, A260295."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Michael Somos_, Nov 09 2015",
			"references": 4,
			"revision": 17,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2015-11-09T11:47:00-05:00"
		}
	]
}