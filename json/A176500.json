{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A176500",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 176500,
			"data": "1,3,7,19,43,115,279,719,1879,4911,12659,33235,86715,226315,592767,1551791,4060203,10630767,27825227,72843667,190710291,499271047,1307051711,3421933647,8958716547,23453948495,61403187051,160755514791,420862602279,1101832758583",
			"name": "a(n) = 2*Farey(Fibonacci(n + 1)) - 3.",
			"comment": [
				"This sequence provides a strict upper bound of the set of equivalent resistances formed by any conceivable network (series/parallel or bridge, or non-planar) of n equal resistors. Consequently it provides an strict upper bound of the sequences: A048211, A153588, A174283, A174284, A174285 and A174286. A176502 provides a better strict upper bound but is harder to compute. [Corrected by _Antoine Mathys_, Jul 12 2019]",
				"Farey(n) = A005728(n). - _Franklin T. Adams-Watters_, May 12 2010",
				"The claim that this sequence is a strict upper bound for the number of representable resistance values of any conceivable network is incorrect for networks with more than 11 resistors, in which non-planar configurations can also occur. Whether the sequence provides at least a valid upper bound for planar networks with generalized bridge circuits (A337516) is difficult to decide on the basis of the insufficient number of terms in A174283 and A337516. See the linked illustrations of the respective quotients. - _Hugo Pfoertner_, Jan 24 2021"
			],
			"link": [
				"Antoine Mathys, \u003ca href=\"/A176500/b176500.txt\"\u003eTable of n, a(n) for n = 1..50\u003c/a\u003e",
				"Antoni Amengual, \u003ca href=\"https://doi.org/10.1119/1.19396\"\u003eThe intriguing properties of the equivalent resistances of n equal resistors combined in series and in parallel\u003c/a\u003e, American Journal of Physics, 68(2), 175-179 (February 2000).",
				"Sameen Ahmed Khan, \u003ca href=\"http://arxiv.org/abs/1004.3346/\"\u003eThe bounds of the set of equivalent resistances of n equal resistors combined in series and in parallel\u003c/a\u003e, arXiv:1004.3346v1 [physics.gen-ph], (Apr 20 2010).",
				"Sameen Ahmed KHAN, \u003ca href=\"/A176500/a176500a.nb\"\u003eMathematica notebook 1\u003c/a\u003e",
				"Sameen Ahmed KHAN, \u003ca href=\"/A176500/a176500b.nb\"\u003eMathematica notebook 2\u003c/a\u003e",
				"Hugo Pfoertner, \u003ca href=\"/plot2a?name1=A048211\u0026amp;name2=A176500\u0026amp;tform1=untransformed\u0026amp;tform2=untransformed\u0026amp;shift=0\u0026amp;radiop1=ratio\u0026amp;drawpoints=true\u0026amp;drawlines=true\"\u003eRatio for series-parallel networks\u003c/a\u003e, Plot2 of A048211(n)/a(n).",
				"Hugo Pfoertner, \u003ca href=\"/plot2a?name1=A337516\u0026amp;name2=A176500\u0026amp;tform1=untransformed\u0026amp;tform2=untransformed\u0026amp;shift=0\u0026amp;radiop1=ratio\u0026amp;drawpoints=true\u0026amp;drawlines=true\"\u003eRatio for planar networks with generalized bridges\u003c/a\u003e, Plot2 of A337516(n)/a(n).",
				"Hugo Pfoertner, \u003ca href=\"/plot2a?name1=A337517\u0026amp;name2=A176500\u0026amp;tform1=untransformed\u0026amp;tform2=untransformed\u0026amp;shift=0\u0026amp;radiop1=ratio\u0026amp;drawpoints=true\u0026amp;drawlines=true\"\u003eRatio for arbitrary networks\u003c/a\u003e, Plot2 of A337517(n)/a(n)."
			],
			"formula": [
				"a(n) = 2 * A176499(n) - 3."
			],
			"example": [
				"n = 5, m = Fibonacci(5 + 1) = 8, Farey(8) = 23, 2Farey(m) - 3 = 43."
			],
			"mathematica": [
				"a[n_] := 2 Sum[EulerPhi[k], {k, 1, Fibonacci[n+1]}] - 1;",
				"Table[an = a[n]; Print[an]; an, {n, 1, 30}] (* _Jean-François Alcover_, Nov 03 2018, from PARI *)"
			],
			"program": [
				"(PARI) a(n) = 2*sum(k=1,fibonacci(n+1),eulerphi(k))-1 \\\\ _Charles R Greathouse IV_, Oct 07 2016",
				"(MAGMA) [2*(\u0026+[EulerPhi(k):k in [1..Fibonacci(n+1)]])-1:n in [1..30]]; // _Marius A. Burtea_, Jul 26 2019"
			],
			"xref": [
				"Cf. A048211, A153588, A174283, A174284, A174285, A174286, A176499, A176501, A176502."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Sameen Ahmed Khan_, Apr 21 2010",
			"ext": [
				"a(26)-a(28) from _Sameen Ahmed Khan_, May 02 2010",
				"a(29)-a(30) from _Antoine Mathys_, Aug 06 2018"
			],
			"references": 13,
			"revision": 42,
			"time": "2021-01-24T13:21:35-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}