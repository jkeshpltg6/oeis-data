{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A349984",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 349984,
			"data": "1,2,6,15,10,12,21,14,18,33,22,20,35,28,24,39,26,30,51,34,36,45,40,38,57,42,44,55,50,46,69,48,52,65,60,56,63,54,58,87,66,62,93,72,68,85,70,74,111,75,80,76,95,90,78,91,77,88,82,123,84,86,129,96,92,115,100,94,141,99,110,98",
			"name": "Lexicographically earliest infinite sequence of distinct positive numbers such that, for n\u003e2, a(n) shares a prime factor with a(n-1) that a(n-1) does not share with a(n-2).",
			"comment": [
				"Similarly to A336957 no term a(n) can be a prime or a prime power as that would make it impossible to find a subsequent term a(n+1) that shared a prime factor with a(n) that a(n) did not share with a(n-1). Likewise each term a(n) must have at least one prime factor not in a(n-1) so that a(n+1) can share a prime factor with a(n) that is not in a(n-1).",
				"At most two consecutive terms can be even, but the number of consecutive odd terms is likely unlimited, although this is unknown. Up to 500000 terms the longest run of consecutive odd terms is ten, the first occurring at a(106376).",
				"For the terms studied it is found that each new prime that occurs in the prime factors that a(n) shares with a(n-1) appears in the natural order of all primes. It is likely this is true for all terms. Likewise for the terms studied the vast majority only share one prime factor with the previous term, although terms sharing multiple primes do occur. The first example is a(55) = 78 which shares prime factors 2 and 3 with a(54) = 90. The first terms to share three prime factors are a(8735) = 9630 and a(8734) = 9930, while the first to share four are a(248153) = 264810 and a(248152) = 265020.",
				"Similar to A337181 the terms are concentrated along lines that have a slight downward curvature. See the first linked image. The lines are distinguished by containing terms with a different lowest prime factor (lpf) in a similar fashion to A336957. The top line contains those with lpf = 3, and then each lower line contains the next largest prime as the lpf. These are interspersed with terms with lpf = 2, while the very lowest line contains the majority of terms with a large lpf. Interestingly the terms with lpf = 17 appear to form a line with a much higher concentration of terms than those with a lpf of 11, 13 or 19. See the second linked image."
			],
			"link": [
				"Scott R. Shannon, \u003ca href=\"/A349984/a349984.png\"\u003eImage of the first 100000 terms\u003c/a\u003e. The three green lines have gradient 1.074, 1.35, 1.61, showing the terms are concentrated along slightly downward curves.",
				"Scott R. Shannon, \u003ca href=\"/A349984/a349984_1.png\"\u003eColor image showing lowest prime factor of the first 100000 terms\u003c/a\u003e. The terms with lpf 2, 3, 5, 7, 11, 13, 17, 19 are shown as white, red, orange, yellow, green, blue, indigo, violet respectively, while all those with with lpf \u003e= 23 are shown as light gray."
			],
			"example": [
				"a(4) = 15 as a(3) = 6 shares the prime factor 2 with a(2) = 2, thus a(4) must share the prime factor 3 with a(3) while not having 2 as a factor. The numbers 3 and 9 are prime powers so cannot be chosen, while 12 also has 2 as a factor, thus 15 is the smallest valid number.",
				"a(19) = 51 as a(18) = 30 shares the prime factor 2 with a(17) = 26, while 51 shares the prime factor 3 with a(18) and it does not contain 2 as a factor. Note that 45 would also satisfy this criteria and is available but 45 only has prime factors 3 and 5 both of which it shares with 30, thus choosing 45 would make it impossible to find a(20).",
				"a(55) = 78 as a(54) = 90 shares the prime factor 5 with a(53) = 95, while 78 shares the prime factors 2 and 3 with a(54) and it does not contain 5 as a factor. This is the first time a number is chosen that shares two prime factors with the previous term."
			],
			"xref": [
				"Cf. A000961, A336957, A098550, A064413, A337181."
			],
			"keyword": "nonn,new",
			"offset": "1,2",
			"author": "_Scott R. Shannon_, Jan 08 2022",
			"references": 1,
			"revision": 30,
			"time": "2022-01-10T11:13:02-05:00",
			"created": "2022-01-10T11:13:02-05:00"
		}
	]
}