{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A065474",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 65474,
			"data": "3,2,2,6,3,4,0,9,8,9,3,9,2,4,4,6,7,0,5,7,9,5,3,1,6,9,2,5,4,8,2,3,7,0,6,6,5,7,0,9,5,0,5,7,9,6,6,5,8,3,2,7,0,9,9,6,1,8,1,1,2,5,2,4,5,3,2,5,0,0,6,3,4,8,6,2,4,4,6,0,9,8,8,4,5,2,3,4,8,1,5,6,8,5,6,3,7,5,5,2,1,7,7,2,7,3",
			"name": "Decimal expansion of Product_{p prime} (1 - 2/p^2).",
			"comment": [
				"Density of A007674, squarefree n such that n + 1 is squarefree. - _Charles R Greathouse IV_, Aug 10 2011",
				"Product_{k\u003e=1} (1 - 2/k^2) = sin(sqrt(2)*Pi) / (sqrt(2)*Pi). - _Vaclav Kotesovec_, May 23 2020"
			],
			"reference": [
				"Henri Cohen, Number Theory, Volume II: Analytic and Modern Tools, GTM Vol. 240, Springer, 2007; see pp. 208-209."
			],
			"link": [
				"Henri Cohen, \u003ca href=\"http://www.math.u-bordeaux.fr/~cohen/hardylw.dvi\"\u003eHigh precision computation of Hardy-Littlewood constants\u003c/a\u003e, (1998).",
				"Henri Cohen, \u003ca href=\"/A221712/a221712.pdf\"\u003eHigh-precision computation of Hardy-Littlewood constants\u003c/a\u003e. [pdf copy, with permission]",
				"Steven R. Finch, \u003ca href=\"https://doi.org/10.1017/9781316997741\"\u003eMathematical Constants II\u003c/a\u003e, Encyclopedia of Mathematics and Its Applications, Cambridge University Press, Cambridge, 2018, p. 163 and 184.",
				"R. J. Mathar, \u003ca href=\"http://arxiv.org/abs/0903.2514\"\u003eHardy-Littlewood constants embedded into infinite products over all positive integers\u003c/a\u003e, arXiv:0903.2514 [math.NT], 2009-2011, constant F_1^(2).",
				"G. Niklasch, \u003ca href=\"/A001692/a001692.html\"\u003eSome number theoretical constants: 1000-digit values\u003c/a\u003e. [Cached copy]",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Squarefree.html\"\u003eSquarefree\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PrimeProducts.html\"\u003ePrime Products\u003c/a\u003e.",
				"Zack Wolske, \u003ca href=\"http://blog.math.toronto.edu/GraduateBlog/files/2018/05/ZWolskePhDThesisJune14-1.pdf\"\u003eNumber Fields with Large Minimal Index\u003c/a\u003e, Ph.D. Thesis, University of Toronto, 2018."
			],
			"example": [
				"0.322634098939244670579531692548..."
			],
			"mathematica": [
				"$MaxExtraPrecision = 800; digits = 98; terms = 800; P[n_] := PrimeZetaP[n]; LR = Join[{0, 0}, LinearRecurrence[{0, 2}, {-4, 0}, terms + 10]]; r[n_Integer] := LR[[n]]; Exp[NSum[r[n]*P[n - 1]/(n - 1), {n, 3, terms}, NSumTerms -\u003e terms, WorkingPrecision -\u003e digits + 10]] // RealDigits[#, 10, digits]\u0026 // First (* _Jean-François Alcover_, Apr 18 2016 *)"
			],
			"program": [
				"(PARI) prodeulerrat(1 - 2/p^2) \\\\ _Amiram Eldar_, Mar 16 2021"
			],
			"xref": [
				"Cf. A007674, A058026, A065493, A074893."
			],
			"keyword": "cons,nonn",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_, Nov 19 2001",
			"ext": [
				"Edited by _Dean Hickerson_, Sep 10 2002",
				"More digits from _Vaclav Kotesovec_, Dec 18 2019"
			],
			"references": 36,
			"revision": 58,
			"time": "2021-06-13T13:34:51-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}