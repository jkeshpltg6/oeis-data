{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A014182",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 14182,
			"data": "1,0,-1,1,2,-9,9,50,-267,413,2180,-17731,50533,110176,-1966797,9938669,-8638718,-278475061,2540956509,-9816860358,-27172288399,725503033401,-5592543175252,15823587507881,168392610536153,-2848115497132448,20819319685262839",
			"name": "Expansion of e.g.f. exp(1-x-exp(-x)).",
			"comment": [
				"E.g.f. A(x) = y satisfies (y + y' + y'') * y - y'^2 = 0. - _Michael Somos_, Mar 11 2004",
				"The 10-adic sum: B(n) = Sum_{k\u003e=0} k^n*k! simplifies to: B(n) = A014182(n)*B(0) + A014619(n) for n\u003e=0, where B(0) is the 10-adic sum of factorials (A025016); a result independent of base. - _Paul D. Hanna_, Aug 12 2006",
				"Equals row sums of triangle A143987 and (shifted) = right border of A143987. [_Gary W. Adamson_, Sep 07 2008]",
				"From _Gary W. Adamson_, Dec 31 2008: (Start)",
				"Equals the eigensequence of the inverse of Pascal's triangle, A007318.",
				"Binomial transform shifts to the right: (1, 1, 0, -1, 1, 2, -9, ...).",
				"Double binomial transform = A109747. (End)",
				"Convolved with A154107 = A000110, the Bell numbers. - _Gary W. Adamson_, Jan 04 2009"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A014182/b014182.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e"
			],
			"formula": [
				"E.g.f.: exp(1-x-exp(-x)).",
				"a(n) = Sum_{k=0..n} (-1)^(n-k)*Stirling2(n+1,k+1). - _Paul D. Hanna_, Aug 12 2006",
				"A000587(n+1) = -a(n). - _Michael Somos_, May 12 2012",
				"G.f.: 1/x/(U(0)-x) -1/x  where U(k)= 1 - x + x*(k+1)/(1 - x/U(k+1)); (continued fraction). - _Sergei N. Gladkovskii_, Oct 12 2012",
				"G.f.: 1/(U(0) - x) where U(k) = 1 + x*(k+1)/(1 - x/U(k+1)); (continued fraction). - _Sergei N. Gladkovskii_, Nov 12 2012",
				"G.f.: (G(0) - 1)/(x-1) where G(k) = 1 - 1/(1+k*x+x)/(1-x/(x-1/G(k+1) )); (continued fraction). - _Sergei N. Gladkovskii_, Jan 17 2013",
				"G.f.: G(0)/(1+x)-1 where G(k) = 1 + 1/(1 + k*x - x*(1+k*x)*(1+k*x+x)/(x*(1+k*x+x) + (1+k*x+2*x)/G(k+1) )); (continued fraction). - _Sergei N. Gladkovskii_, Feb 09 2013",
				"G.f.: S-1 where S = Sum_{k\u003e=0} (2 + x*k)*x^k/Product_{i=0..k} (1+x+x*i). - _Sergei N. Gladkovskii_, Feb 09 2013",
				"G.f.: G(0)*x^2/(1+x)/(1+2*x) + 2/(1+x) - 1 where G(k) = 1 + 2/(x + k*x - x^3*(k+1)*(k+2)/(x^2*(k+2) + 2*(1+k*x+3*x)/G(k+1) )); (continued fraction). - _Sergei N. Gladkovskii_, Feb 09 2013",
				"G.f.: 1/(x*Q(0)) -1/x, where Q(k) = 1 - x/(1 + (k+1)*x/Q(k+1) ); (continued fraction). - _Sergei N. Gladkovskii_, Sep 27 2013",
				"G.f.: G(0)/(1-x)/x - 1/x, where G(k) = 1 - x^2*(k+1)/(x^2*(k+1) + (x*k + 1 - x)*(x*k + 1)/G(k+1) ); (continued fraction). - _Sergei N. Gladkovskii_, Feb 06 2014",
				"G.f.:  (1 - Sum_{k\u003e0} k * x^k / ((1 + x) * (1 + 2*x) + ... (1 + k*x))) / (1 - x). - _Michael Somos_, Nov 07 2014",
				"a(n) = exp(1) * (-1)^n * Sum_{k\u003e=0} (-1)^k * (k + 1)^n / k!. - _Ilya Gutkovskiy_, Dec 20 2019"
			],
			"example": [
				"G.f. = 1 - x^2 + x^3 + 2*x^4 - 9*x^5 + 9*x^6 + 50*x^7 - 267*x^8 + 413*x^9 + ..."
			],
			"maple": [
				"a:=series(exp(1-x-exp(-x)),x=0,27): seq(n!*coeff(a,x,n),n=0..26); # _Paolo P. Lava_, Mar 26 2019"
			],
			"mathematica": [
				"With[{nn=30},CoefficientList[Series[Exp[1-x-Exp[-x]],{x,0,nn}],x] Range[0,nn]!]  (* _Harvey P. Dale_, Jan 15 2012 *)",
				"a[ n_] := SeriesCoefficient[ (1 - Sum[ k / Pochhammer[ 1/x + 1, k], {k, n}]) / (1 - x), {x, 0, n} ]; (* _Michael Somos_, Nov 07 2014 *)"
			],
			"program": [
				"(PARI) {a(n)=sum(j=0,n,(-1)^(n-j)*Stirling2(n+1,j+1))}",
				"{Stirling2(n,k)=(1/k!)*sum(i=0,k,(-1)^(k-i)*binomial(k,i)*i^n)} \\\\ _Paul D. Hanna_, Aug 12 2006",
				"(PARI) {a(n) = if( n\u003c0, 0, n! * polcoeff( exp( 1 - x - exp( -x + x * O(x^n))), n))} /* _Michael Somos_, Mar 11 2004 */",
				"(Sage)",
				"def A014182_list(len):  # len\u003e=1",
				"    T = [0]*(len+1); T[1] = 1; R = [1]",
				"    for n in (1..len-1):",
				"        a,b,c = 1,0,0",
				"        for k in range(n,-1,-1):",
				"            r = a - k*b - (k+1)*c",
				"            if k \u003c n : T[k+2] = u;",
				"            a,b,c = T[k-1],a,b",
				"            u = r",
				"        T[1] = u; R.append(u)",
				"    return R",
				"A014182_list(27)  # _Peter Luschny_, Nov 01 2012"
			],
			"xref": [
				"Essentially same as A000587. See also A014619.",
				"Cf. A025016.",
				"Cf. A143987, A109747, A154107, A000110."
			],
			"keyword": "sign,easy,nice",
			"offset": "0,5",
			"author": "_Noam D. Elkies_",
			"references": 16,
			"revision": 68,
			"time": "2021-06-22T03:15:43-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}