{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007581",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7581,
			"id": "M1479",
			"data": "1,2,5,15,51,187,715,2795,11051,43947,175275,700075,2798251,11188907,44747435,178973355,715860651,2863377067,11453377195,45813246635,183252462251,733008800427,2932033104555,11728128223915,46912504507051,187650001250987",
			"name": "a(n) = (2^n+1)*(2^n+2)/6.",
			"comment": [
				"Number of palindromic structures using a maximum of four different symbols. - _Marks R. Nester_",
				"Dimension of the universal embedding of the symplectic dual polar space DSp(2n,2) (conjectured by A. Brouwer, proved by P. Li). - J. Taylor (jt_cpp(AT)yahoo.com), Apr 02 2004.",
				"Apart from initial term, same as A124303. - _Valery A. Liskovets_, Nov 16 2006",
				"Hankel transform is := [1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, ...]. - _Philippe Deléham_, Dec 04 2008",
				"a(n) is also the number of distinct solutions (avoiding permutations) to the equation: XOR(A,B,C)=0 where A,B,C are n-bit binary numbers. - _Ramasamy Chandramouli_, Jan 11 2009",
				"The rank of the fundamental group of the Z_p^n - cobordism category in dimension 1+1 for the case p=2 (see paper below). The expression for any prime p is (p^(2n-1)+p^(n+1)-p^(n-1)+p^2-p-1)/(p^2-1). - _Carlos Segovia Gonzalez_, Dec 05 2012",
				"The number of isomorphic classes of regular four coverings of a graph with respect to the identity automorphism (S. Hong and J. H. Kwak). - _Carlos Segovia Gonzalez_, Aug 01 2013",
				"The density of a language with four letters (N. Moreira and R. Reis). - _Carlos Segovia Gonzalez_, Aug 01 2013"
			],
			"reference": [
				"P. Li, On the Brouwer Conjecture for Dual Polar Spaces of Symplectic Type over GF(2). Preprint.",
				"M. R. Nester (1999). Mathematical investigations of some plant interaction designs. PhD Thesis. University of Queensland, Brisbane, Australia. [See A056391 for pdf file of Chap. 2]",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A007581/b007581.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"Joerg Arndt and N. J. A. Sloane, \u003ca href=\"/A278984/a278984.txt\"\u003eCounting Words that are in \"Standard Order\"\u003c/a\u003e",
				"Georgia Benkart and Tom Halverson, \u003ca href=\"https://hal.archives-ouvertes.fr/hal-02173744\"\u003eMcKay Centralizer Algebras\u003c/a\u003e, hal-02173744 [math.CO], 2020.",
				"A. Blokhuis and A. E. Brouwer, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(02)00545-9\"\u003eThe universal embedding dimension of the binary symplectic dual polar space\u003c/a\u003e, Discr. Math., 264 (2003), 3-11.",
				"P. J. Cameron, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL3/groups.html\"\u003eSequences realized by oligomorphic permutation groups\u003c/a\u003e, J. Integ. Seqs. Vol. 3 (2000), #00.1.5.",
				"Bruno Cisneros, Carlos Segovia, \u003ca href=\"https://arxiv.org/abs/1805.04633\"\u003eAn approximation for the number of subgroups\u003c/a\u003e, arXiv:1805.04633 [math.GT], 2018.",
				"B. N. Cooperstein and E. E. Shult, \u003ca href=\"http://www.emis.de/journals/AG/1-1/1_037.pdf\"\u003eA note on embedding and generating dual polar spaces\u003c/a\u003e. Adv. Geom. 1 (2001), 37-48.",
				"Yuanan Diao, Michael Finney, and Dawn Ray, \u003ca href=\"https://arxiv.org/abs/2007.02819\"\u003eThe number of oriented rational links with a given deficiency number\u003c/a\u003e, arXiv:2007.02819 [math.GT], 2020. See p. 16.",
				"A. M. Hinz, S. Klavžar, U. Milutinović, and C. Petr, \u003ca href=\"http://dx.doi.org/10.1007/978-3-0348-0237-6\"\u003eThe Tower of Hanoi - Myths and Maths\u003c/a\u003e, Birkhäuser 2013. See page 183. \u003ca href=\"http://tohbook.info\"\u003eBook's website\u003c/a\u003e",
				"S. Hong and J. H. Kwak, \u003ca href=\"http://dx.doi.org/10.1002/jgt.3190170509\"\u003eRegular fourfold covering with respect to the identity automorphism\u003c/a\u003e, J. Graph Theory, 17 (1993), 621-627.",
				"Masashi Kosuda and Manabu Oura, \u003ca href=\"http://arxiv.org/abs/1505.00318\"\u003eCentralizer algebras of the primitive unitary reflection group of order 96\u003c/a\u003e, arXiv:1505.00318 [math.RT], 2015.",
				"George S. Lueker, \u003ca href=\"http://www.ics.uci.edu/~lueker/papers/lcs/lcs.pdf\"\u003eImproved Bounds on the Average Length of Longest Common Subsequences\u003c/a\u003e (Jul 22, 2005) (Fig.1).",
				"N. Moreira and R. Reis, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL8/Moreira/moreira8.html\"\u003eOn the Density of Languages Representing Finite Set Partitions\u003c/a\u003e, Journal of Integer Sequences, Vol. 8 (2005), Article 05.2.8.",
				"C. Segovia, \u003ca href=\"https://www.matem.unam.mx/~csegovia/Arch/Poster.pdf\"\u003eUnexpected relations of cobordism categories with another [sic] subjects in mathematics\u003c/a\u003e, 2013.",
				"C. Segovia, \u003ca href=\"http://arxiv.org/abs/1211.2144\"\u003eThe classifying space of the 1+1 dimensional G-cobordism category\u003c/a\u003e, arXiv:1211.2144 [math.AT], 2012-2019.",
				"C. Segovia, \u003ca href=\"http://arxiv.org/abs/1307.2850\"\u003eNumerical computations in cobordism categories\u003c/a\u003e, arXiv:1307.2850 [math.AT], 2013.",
				"C. Segovia and M. Winklmeier, \u003ca href=\"http://arxiv.org/abs/1409.2067\"\u003eCombinatorial Computations in Cobordism Categories\u003c/a\u003e, arXiv preprint arXiv:1409.2067 [math.CO], 2014-2015.",
				"C. Segovia and M. Winklmeier, \u003ca href=\"https://doi.org/10.37236/4668\"\u003eOn the density of certain languages with p^2 letters\u003c/a\u003e, Electronic Journal of Combinatorics 22(3) (2015), #P3.16.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (7,-14,8)."
			],
			"formula": [
				"a(n) = (3*2^(n-1) + 2^(2*n-1) + 1)/3.",
				"a(n) = Sum_{k=1..4} Stirling2(n, k). - Winston Yang (winston(AT)cs.wisc.edu), Aug 23 2000",
				"Binomial transform of 3^n/6 + 1/2 + 0^n/3, i.e., of A007051 with an extra leading 1. a(n) = binomial(2^n+2, 2^n-1)/2^n. - _Paul Barry_, Jul 19 2003",
				"a(n) = C(2+2^n, 3)/2^n = a(n-1) + 2^(n-1) + 4^(n-3/2) = A092055(n)/A000079(n). - _Henry Bottomley_, Feb 19 2004",
				"Second binomial transform of A001045(n-1) + 0^n/2. G.f.: (1-5*x+5*x^2)/((1-x)*(1-2*x)*(1-4*x)). - _Paul Barry_, Apr 28 2004",
				"a(n) is the top entry of the vector M^n*[1,1,1,1,0,0,0,...], where M is an infinite bidiagonal matrix with M(r,r)=r, r \u003e= 1, as the main diagonal, M(r,r+1)=1, and the rest zeros. ([1,1,1,...] is a column vector and transposing gives the same in terms of a leftmost column term.) - _Gary W. Adamson_, Jun 24 2011",
				"a(0)=1, a(1)=2, a(2)=5, a(n) = 7*a(n-1) - 14*a(n-2) + 8*a(n-3). - _Harvey P. Dale_, Jul 24 2011",
				"E.g.f.: (exp(2*x) + 1/3*exp(4*x) + 2/3*exp(x))/2 = G(0)/2; G(k)=1 + (2^k)/(3 - 6/(2 + 4^k - 3*x*(8^k)/(3*x*(2^k) + (k+1)/G(k+1)))); (continued fraction). - _Sergei N. Gladkovskii_, Dec 08 2011"
			],
			"maple": [
				"A007581:=n-\u003e(2^n+1)*(2^n+2)/6; seq(A007581(n), n=0..50); # _Wesley Ivan Hurt_, Nov 25 2013"
			],
			"mathematica": [
				"Table[(3*2^(n-1)+2^(2n-1)+1)/3,{n,0,30}] (* or *) LinearRecurrence[ {7,-14,8},{1,2,5},31] (* _Harvey P. Dale_, Jul 24 2011 *)",
				"CoefficientList[Series[(1 - 5 x + 5 x^2) / ((1 - x) (1 - 2 x) (1 - 4 x)), {x, 0, 33}], x] (* _Vincenzo Librandi_, Aug 09 2018 *)"
			],
			"program": [
				"(PARI) a(n)=(3*2^(n-1)+2^(2*n-1)+1)/3; \\\\ _Charles R Greathouse IV_, Jun 24 2011",
				"(PARI) a(n)=if(n==0,1,(2\u003c\u003c(2*n--))\\/3+2^n); \\\\ _Charles R Greathouse IV_, Jun 24 2011",
				"(MAGMA) [(2^n+1)*(2^n+2)/6: n in [0..25]]; // _Vincenzo Librandi_, Aug 09 2018",
				"(GAP) List([0..30],n-\u003e(2^n+1)*(2^n+2)/6); # _Muniru A Asiru_, Aug 09 2018"
			],
			"xref": [
				"Cf. A056272, A056273, A007051, A000392, A056450, A028401, A060919.",
				"A row of the array in A278984."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,2",
			"author": "_Simon Plouffe_ and _N. J. A. Sloane_",
			"references": 39,
			"revision": 150,
			"time": "2020-08-29T23:48:25-04:00",
			"created": "1994-09-19T03:00:00-04:00"
		}
	]
}