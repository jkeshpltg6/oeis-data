{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A213547",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 213547,
			"data": "1,12,68,260,777,1960,4368,8856,16665,29524,49764,80444,125489,189840,279616,402288,566865,784092,1066660,1429428,1889657,2467256,3185040,4069000,5148585,6456996,8031492,9913708,12149985,14791712",
			"name": "Antidiagonal sums of the convolution array A213505.",
			"comment": [
				"Also, the antidiagonal sums of the convolution array A213555.",
				"An m-star is an m-antichain with a smallest element adjoined. Then, a(n) is the number of proper mergings of a 2-star and an (n-1)-chain, see example. - _Henri Mühle_, Jan 23 2013"
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A213547/b213547.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Henri Muehle, \u003ca href=\"http://arxiv.org/abs/1301.1654\"\u003eProper Mergings of Stars and Chains are Counted by Sums of Antidiagonals in Certain Convolution Arrays -- The Details\u003c/a\u003e, arXiv preprint arXiv:1301.1654, 2013.",
				"\u003ca href=\"/index/Rec#order_07\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (7,-21,35,-35,21,-7,1)."
			],
			"formula": [
				"a(n) = (n^6 + 6*n^5 + 15*n^4 + 20*n^3 + 14*n^2 + 4*n)/60.",
				"a(n) = 7*a(n-1) - 21*a(n-2) + 35*a(n-3) - 35*a(n-4) + 21*a(n-5) - 7*a(n-6) + a(n-7).",
				"G.f.: x*(1 + 5*x  + 5*x^2 + x^3)/(1 - x)^7.",
				"a(n) = a(-2-n) and a(n-1) = (n^6 - n^2) / 60 for all n in Z. - _Michael Somos_, Oct 08 2017"
			],
			"example": [
				"From _Henri Mühle_, Jan 23 2013: (Start)",
				"For n=2, let S=({s0,s1,s2},{(s0,s0),(s0,s1),(s0,s2),(s1,s1),(s2,s2)}) be a 2-star, and let C=({c},{(c,c)}) be a 1-chain. The a(2)=12 proper mergings of S and C are:",
				"({s0,s1,s2,c},{(s0,s0),(s0,s1),(s0,s2),(s1,s1),(s2,s2),(c,c)})",
				"({s0,s1,s2,c},{(c,s0),(c,s1),(c,s2),(s0,s0),(s0,s1),(s0,s2),(s1,s1),(s2,s2),(c,c)})",
				"({s0,s1,s2,c},{(c,s1),(s0,s0),(s0,s1),(s0,s2),(s1,s1),(s2,s2),(c,c)})",
				"({s0,s1,s2,c},{(c,s2),(s0,s1),(s0,s2),(s1,s1),(s2,s2),(c,c)})",
				"({s0,s1,s2,c},{(c,s1),(c,s2),(s0,s1),(s0,s2),(s1,s1),(s2,s2),(c,c)})",
				"({s0,s1,s2,c},{(s0,c),(s0,s0),(s0,s1),(s0,s2),(s1,s1),(s2,s2),(c,c)})",
				"({s0,s1,s2,c},{(s0,c),(c,s1),(s0,s0),(s0,s1),(s0,s2),(s1,s1),(s2,s2),(c,c)})",
				"({s0,s1,s2,c},{(s0,c),(c,s2),(s0,s0),(s0,s1),(s0,s2),(s1,s1),(s2,s2),(c,c)})",
				"({s0,s1,s2,c},{(s0,c),(c,s1),(c,s2),(s0,s0),(s0,s1),(s0,s2),(s1,s1),(s2,s2),(c,c)})",
				"({s0,s1,s2,c},{(s1,c),(s0,s0),(s0,s1),(s0,s2),(s1,s1),(s2,s2),(c,c)})",
				"({s0,s1,s2,c},{(s2,c),(s0,s0),(s0,s1),(s0,s2),(s1,s1),(s2,s2),(c,c)})",
				"({s0,s1,s2,c},{(s1,c),(s2,c),(s0,s0),(s0,s1),(s0,s2),(s1,s1),(s2,s2),(c,c)})",
				"(End)"
			],
			"mathematica": [
				"(See A213505.)"
			],
			"program": [
				"(PARI) {a(n) = n++; (n^6 - n^2) / 60}; /* _Michael Somos_, Oct 08 2017 */"
			],
			"xref": [
				"Cf. A213500, A213505."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Jun 16 2012",
			"references": 7,
			"revision": 31,
			"time": "2017-10-08T10:40:27-04:00",
			"created": "2012-07-11T18:47:57-04:00"
		}
	]
}