{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A268468",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 268468,
			"data": "4,507,1377,12608,55291,55290,55289,145645,104455,104454,336734,336733,336732,525907,1960873,1836239,2176265,2176264,2176263,2176262,2176261,2176260,2176259,2176258",
			"name": "Least k starting a chain or (2n+1)-tuple of consecutive integers {h(k+i)}, i=0,1,...,2n (excluding the trivial chain when h(k) = h(k+1) = ... = h(k+2n)) with symmetrical gaps about the center, where h(k) is the length of the finite set {k, f(k), f(f(k)),...,1} in the Collatz (or 3x + 1) problem.",
			"comment": [
				"It is interesting to search for symmetries in the sequence A006577 (number of halving and tripling steps to reach 1 in '3x+1' problem).  The symmetrical architecture is given by the following property: h(k) + h(k+2n) = h(k+1)+ h(k+2n-1)= ... = h(k+n-1)+ h(k+n+1) = 2*h(k+n) where h(k+n) is the symmetrical center.",
				"We observe two essential families of chains containing symmetries:",
				"(i) A majority of trivial chains are obtained when a(n) begins the first chain of 2n+1 consecutive positive integers where h(k) = h(k+1) = ... = h(k+2n). This case is not considered here, but is mentioned in A078441. If this case were to be considered, it would be found that a(n) = A078441(2n+1) = 28, 98, 943, 1680, 2987, 2987, 7083, 7083, ...",
				"(ii) Chains having several distinct values. This case is more interesting, with an important question: how many distinct values can contain such a set {h(k)}? It appears that this value is equal to 3, and the sequence of the reduced corresponding sets is {4, 5, 6}, {35, 48, 61}, {96, 127, 158}, {32, 63, 94}, {91, 122, 153}, {91, 122, 153}, {91, 122, 153}, {126, 188, 250}, ... The corresponding symmetrical centers are 5, 48, 127, 63, 122, 122, 122, 188, 172, 172, 184, 184, 184, 164, 184, 202, 210, 210, 210, 210, 210, 210, 210, 210, ..."
			],
			"example": [
				"a(1) = 4 because in the first 3-tuple {h(4),h(5),h(6)} = {2, 5, 8}, the numbers are symmetric w.r.t. the central h(5)= 5 since 2+8 = 2*5. Hence 4 belongs to the sequence.",
				"Alternatively, the symmetry can be seen from the differences between consecutive h(k). For {2,5,8}, the set of the differences is {3,3}.",
				"a(3) = 10136 because in 7-tuple of consecutive {h(k)} = {34, 34, 34, 60, 86, 86, 86}, the numbers are symmetric w.r.t. its central h(k+3) = 60, since 34+86 = 2*60, and this is the smallest such 7-tuple. Hence 10136 belongs to the sequence.",
				"Alternatively, the symmetry can be seen from the differences between consecutive h(k). From the set {34, 34, 34, 60, 86, 86, 86}, the set of the differences is {0,0,26,26,0,0}."
			],
			"maple": [
				"nn:=10^7:T:=array(1..nn):",
				"for j from 1 to 5*10^6 do:",
				"  k:=0:m:=j:it:=0:",
				"    for i from 1 to nn while(m\u003c\u003e1) do:",
				"     if irem(m,2)=0",
				"      then",
				"       m:=m/2:",
				"      else",
				"      m:=3*m+1:",
				"     fi:",
				"    it:=it+1:",
				"    od:",
				"    k:=j:T[j]:=it:",
				"   od:",
				"   for n from 3 by 2 to 50 do:",
				"   ii:=0:",
				"   for j from 1 to nn while(ii=0)do:",
				"     q:=T[j]+T[j+n-1]:",
				"     itr:=0:lst:={}:",
				"     for jj from 1 to (n-1)/2 do:",
				"      lst:=lst union {T[j+jj-1]} union {T[j+n-jj]}:",
				"      if T[j+jj-1]+T[j+n-jj]=q and T[j+(n-1)/2]=q/2",
				"       then",
				"       itr:=itr+1:",
				"       else fi:",
				"      od:",
				"      if itr=(n-1)/2 and nops(lst)\u003e1 then ii:=1:",
				"      printf(\"%d %d \\n\",n,j):",
				"      else",
				"      fi:",
				"     od:",
				"     od:"
			],
			"xref": [
				"Cf. A006577, A078441, A268253, A268268, A268288."
			],
			"keyword": "nonn,more",
			"offset": "1,1",
			"author": "_Michel Lagneau_, Feb 05 2016",
			"references": 1,
			"revision": 11,
			"time": "2020-01-05T07:17:58-05:00",
			"created": "2016-02-13T23:07:40-05:00"
		}
	]
}