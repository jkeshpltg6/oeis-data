{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A113547",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 113547,
			"data": "1,1,1,1,2,2,1,4,5,5,1,8,13,15,15,1,16,35,47,52,52,1,32,97,153,188,203,203,1,64,275,515,706,825,877,877,1,128,793,1785,2744,3479,3937,4140,4140,1,256,2315,6347,11002,15177,18313,20270,21147,21147,1,512,6817,23073,45368,68303,88033,102678,111835,115975,115975",
			"name": "Triangle read by rows: number of labeled partitions of n with maximin m.",
			"comment": [
				"The maximin of a partition is the maximum over all parts of the minimum label in each part. If the rows are reversed, the result is the number of partitions of n with minimax m.",
				"The number of restricted growth functions of length n where the maximum appears first at position m. The RGF's are defined here as f(1)=1 and f(i) \u003c=1+max_{1\u003c=j\u003ci} f(j). - _R. J. Mathar_, Mar 18 2016"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A113547/b113547.txt\"\u003eTable of n, a(n) for the first 50 rows, flattened\u003c/a\u003e",
				"Todd Tichenor, \u003ca href=\"https://arxiv.org/abs/1709.00393\"\u003e A note on graph compositions and their connection to minimax of set partitions\u003c/a\u003e arXiv:1709.00393 [math.CO], 2017."
			],
			"formula": [
				"T(n, m)=Sum_{k=1..m} S2(m-1, k-1)*k^(n-m), where S2 is the Stirling numbers of the second kind (A008277). T(n, n)=T(n, n-1)=B(n-1), where B is the Bell numbers (A000110). T(n, n-2)=B(n-1)-B(n-3).",
				"Conjectures: T(n,3) = A007689(n-3). T(n,4) = 2^(n-4)+3^(n-3)+4^(n-4).- _R. J. Mathar_, Mar 13 2016"
			],
			"example": [
				"Maximin [123]=max(1)=1, maximin [12|3]=max(1,3)=3, maximin [13|2]=max(1,2)=2, maximin [1|23]=max(1,2)=2 and maximin [1|2|3]=max(1,2,3)=3, so for n=3 the multiset of maximins is {1,2,2,3,3}, making the 3rd line 1,2,2.",
				"1;",
				"1,  1;",
				"1,  2,   2;",
				"1,  4,   5,   5;",
				"1,  8,  13,  15,  15;",
				"1, 16,  35,  47,  52,  52;",
				"1, 32,  97, 153, 188, 203, 203;",
				"1, 64, 275, 515, 706, 825, 877, 877;"
			],
			"maple": [
				"A113547 := proc(n,m)",
				"    add(combinat[stirling2](m-1,k-1)*k^(n-m),k=1..m) ;",
				"end proc:",
				"seq(seq( A113547(n,m),m=1..n),n=1..10) ; # _R. J. Mathar_, Mar 13 2016"
			],
			"mathematica": [
				"T[n_, n_] := BellB[n - 1]; T[n_, n_ - 1] := BellB[n - 1]; T[n_, n_ - 2] := BellB[n - 1] - BellB[n - 3]; T[n_, m_] := Sum[StirlingS2[m - 1, k - 1]*k^(n - m), {k, 1, m}]; Table[T[n, m], {n, 1, 5}, {m, 1, n}] (* _G. C. Greubel_, May 06 2017 *)"
			],
			"xref": [
				"Cf. A008277, A000110, A271466."
			],
			"keyword": "nonn,tabl",
			"offset": "1,5",
			"author": "_Franklin T. Adams-Watters_, Jan 13 2006",
			"references": 3,
			"revision": 26,
			"time": "2018-02-19T14:31:40-05:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}