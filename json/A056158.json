{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A056158",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 56158,
			"data": "-4,-2,-4,2,-20,86,-532,3706,-29668,266990,-2669924,29369138,-352429684,4581585862,-64142202100,962133031466,-15394128503492,261700184559326,-4710603322067908,89501463119290210,-1790029262385804244",
			"name": "Equivalent of the Kurepa hypothesis for left factorial.",
			"comment": [
				"For a prime p \u003e 2 we have !p == -a(p) mod p, where the left factorial !n = Sum_{k=0..n-1} k! (A003422)."
			],
			"link": [
				"James Spahlinger, \u003ca href=\"/A056158/b056158.txt\"\u003eTable of n, a(n) for n = 3..100\u003c/a\u003e",
				"Romeo Mestrovic, \u003ca href=\"http://arxiv.org/abs/1312.7037\"\u003eVariations of Kurepa's left factorial hypothesis\u003c/a\u003e, arXiv:1312.7037 [math.NT], 2013.",
				"Romeo Mestrovic, \u003ca href=\"https://doi.org/10.2298/FIL1510207M\"\u003eThe Kurepa-Vandermonde matrices arising from Kurepa's left factorial hypothesis\u003c/a\u003e, Filomat 29:10 (2015), 2207-2215; DOI 10.2298/FIL1510207M."
			],
			"formula": [
				"a(3) = -4, a(n) = -(n-3)*a(n-1) - 2*(n-1).",
				"a(n) = 2*(-1)^(n-1)*(n-3)!*Sum_{k=0..n-3} frac((k+2)*(-1)^(k+1))*k!.",
				"Conjecture: a(n) + (n-5)*a(n-1) + (-2*n+9)*a(n-2) + (n-5)*a(n-3) = 0. - _R. J. Mathar_, Jan 31 2014",
				"a(n) ~ (-1)^n * 2 * exp(-1) * (n-3)!. - _Vaclav Kotesovec_, Jan 05 2019",
				"G.f.: 2*x^2*(exp(-1+1/x) * Exponential-Integral((x-1)/x) + x/(x-1)). - _G. C. Greubel_, Mar 29 2019"
			],
			"mathematica": [
				"a[3] = -4; a[n_]:= -(n-3)*a[n-1] - 2*(n-1); Array[a, 30, 3] (* _James Spahlinger_, Feb 20 2016 *)",
				"Drop[CoefficientList[Series[2*x^2*(Exp[1/x -1]*ExpIntegralEi[(x-1)/x] + x/(x-1)), {x,0,15}, Assumptions -\u003e x \u003e 0], x],3] (* _G. C. Greubel_, Mar 29 2019 *)"
			],
			"program": [
				"(MAGMA) [n eq 3 select -4 else -(n-3)*Self(n-3)-2*(n-1): n in [3..30]]; // _Vincenzo Librandi_, Feb 22 2016",
				"(PARI) m=30; v=concat([-4], vector(m-1)); for(n=2, m, v[n]=-(n-1)*v[n-1] -2*(n+1)); v \\\\ _G. C. Greubel_, Mar 29 2019",
				"(Sage)",
				"@CachedFunction",
				"def Self(n):",
				"   if n == 3 : return -4",
				"   return -(n-3)*Self(n-1) - 2*(n-1)",
				"[Self(n) for n in (3..30)] # _G. C. Greubel_, Mar 29 2019"
			],
			"keyword": "sign,easy",
			"offset": "3,1",
			"author": "Aleksandar Petojevic (apetoje(AT)ptt.yu), Jul 31 2000",
			"ext": [
				"More terms from Larry Reeves (larryr(AT)acm.org), Oct 03 2000"
			],
			"references": 2,
			"revision": 34,
			"time": "2019-03-31T00:16:35-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}