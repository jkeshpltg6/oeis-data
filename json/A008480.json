{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008480",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8480,
			"data": "1,1,1,1,1,2,1,1,1,2,1,3,1,2,2,1,1,3,1,3,2,2,1,4,1,2,1,3,1,6,1,1,2,2,2,6,1,2,2,4,1,6,1,3,3,2,1,5,1,3,2,3,1,4,2,4,2,2,1,12,1,2,3,1,2,6,1,3,2,6,1,10,1,2,3,3,2,6,1,5,1,2,1,12,2,2,2,4,1,12,2,3,2,2,2,6,1,3,3,6,1",
			"name": "Number of ordered prime factorizations of n.",
			"comment": [
				"a(n) depends only on the prime signature of n (cf. A025487). So a(24) = a(375) since 24 = 2^3 * 3 and 375 = 3 * 5^3 both have prime signature (3,1).",
				"Multinomial coefficients in prime factorization order. - _Max Alekseyev_, Nov 07 2006",
				"The Dirichlet inverse is given by A080339, negating all but the A080339(1) element in A080339. - _R. J. Mathar_, Jul 15 2010",
				"Number of (distinct) permutations of the multiset of prime factors. - _Joerg Arndt_, Feb 17 2015",
				"Number of not divisible chains in the divisor lattice of n. - _Peter Luschny_, Jun 15 2013"
			],
			"reference": [
				"A. Knopfmacher, J. Knopfmacher and R. Warlimont, \"Ordered factorizations for integers and arithmetical semigroups\", in Advances in Number Theory, (Proc. 3rd Conf. Canadian Number Theory Assoc., 1991), Clarendon Press, Oxford, 1993, pp. 151-165.",
				"S. R. Finch, Mathematical Constants, Cambridge, 2003, pp. 292-295."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A008480/b008480.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"S. R. Finch, \u003ca href=\"/A001055/a001055.pdf\"\u003eKalmar's composition constant\u003c/a\u003e, June 5, 2003. [Cached copy, with permission of the author]",
				"Carl-Erik Fröberg, \u003ca href=\"https://doi.org/10.1007/BF01933420\"\u003eOn the prime zeta function\u003c/a\u003e, BIT Numerical Mathematics, Vol. 8, No. 3 (1968), pp. 187-202.",
				"Gordon Hamilton's MathPickle, \u003ca href=\"http://mathpickle.com/project/fractal-multiplication/\"\u003eFractal Multiplication\u003c/a\u003e (visual presentation of non-commutative multiplication).",
				"Maxie D. Schmidt, \u003ca href=\"https://arxiv.org/abs/2102.05842\"\u003eNew characterizations of the summatory function of the Moebius function\u003c/a\u003e, arXiv:2102.05842 [math.NT], 2021.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/MultinomialCoefficient.html\"\u003eMultinomial Coefficient\u003c/a\u003e"
			],
			"formula": [
				"If n = Product (p_j^k_j) then a(n) = ( Sum (k_j) )! / Product (k_j !).",
				"Dirichlet g.f.: 1/(1-B(s)) where B(s) is D.g.f. of characteristic function of primes.",
				"a(p^k) = 1 if p is a prime.",
				"a(A002110(n)) = A000142(n) = n!.",
				"a(n) = A050382(A101296(n)). - _R. J. Mathar_, May 26 2017",
				"a(n) = 1 \u003c=\u003e n in { A000961 }. - _Alois P. Heinz_, May 26 2018",
				"G.f. A(x) satisfies: A(x) = x + A(x^2) + A(x^3) + A(x^5) + ... + A(x^prime(k)) + ... - _Ilya Gutkovskiy_, May 10 2019",
				"a(n) = C(k, n) for k = A001222(n) where C(k, n) is defined as the k-fold Dirichlet convolution of A001221(n) with itself, and where C(0, n) is the multiplicative identity with respect to Dirichlet convolution.",
				"The average order of a(n) is asymptotic (up to an absolute constant) to 2A sqrt(2*Pi) log(n) / sqrt(log(log(n))) for some absolute constant A \u003e 0 - _Maxie D. Schmidt_, May 28 2021",
				"The sums of a(n) for n\u003c=x and k\u003e=1 such that A0012222(n)=k have asymptotic order of the form x(log(log(x)))^(k+1/2) / (2k+1) / (k-1)! - _Maxie D. Schmidt_, Feb 12 2021",
				"Other DGFs include: (1+P(s))^(-1) in terms of the prime zeta function for Re(s) \u003e 1 where the + version weights the sequence by A008836(n), see the reference by Fröberg on P(s). - _Maxie D. Schmidt_, Feb 12 2021",
				"The bivariate DGF (1+zP(s))^(-1) has coefficients a(n) / n^s (-1)^(A001221(n)) z^(A001222(n)) for Re(s) \u003e 1 and 0 \u003c |z| \u003c 2 - _Maxie D. Schmidt_, Feb 12 2021",
				"The distribution of the distinct values of the sequence for n\u003c=x as x-\u003einfinity satisfy a CLT-type Erdős-Kac theorem analog proved by M. D. Schmidt, 2021. - _Maxie D. Schmidt_, Feb 12 2021"
			],
			"maple": [
				"a:= n-\u003e (l-\u003e add(i, i=l)!/mul(i!, i=l))(map(i-\u003e i[2], ifactors(n)[2])):",
				"seq(a(n), n=1..100);  # _Alois P. Heinz_, May 26 2018"
			],
			"mathematica": [
				"Prepend[ Array[ Multinomial @@ Last[ Transpose[ FactorInteger[ # ] ] ]\u0026, 100, 2 ], 1 ]",
				"(* Second program: *)",
				"a[n_] := With[{ee = FactorInteger[n][[All, 2]]}, Total[ee]!/Times @@ (ee!)]; Array[a, 101] (* _Jean-François Alcover_, Sep 15 2019 *)"
			],
			"program": [
				"(Sage)",
				"def A008480(n):",
				"    S = [s[1] for s in factor(n)]",
				"    return factorial(sum(S)) // prod(factorial(s) for s in S)",
				"[A008480(n) for n in (1..101)]  # _Peter Luschny_, Jun 15 2013",
				"(Haskell)",
				"a008480 n = foldl div (a000142 $ sum es) (map a000142 es)",
				"            where es = a124010_row n",
				"-- _Reinhard Zumkeller_, Nov 18 2015",
				"(PARI) a(n)={my(sig=factor(n)[,2]); vecsum(sig)!/vecprod(apply(k-\u003ek!, sig))} \\\\ _Andrew Howroyd_, Nov 17 2018"
			],
			"xref": [
				"Cf. A000040, A000142, A000961, A002110, A002033, A050382.",
				"Cf. A036038, A036039, A036040, A080575, A102189.",
				"Cf. A099848, A099849.",
				"Cf. A124010, record values and where they occur: A260987, A260633."
			],
			"keyword": "nonn",
			"offset": "1,6",
			"author": "_Olivier Gérard_",
			"ext": [
				"Edited by _N. J. A. Sloane_ at the suggestion of _Andrew S. Plewe_, Jun 17 2007"
			],
			"references": 186,
			"revision": 96,
			"time": "2021-05-29T05:58:31-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}