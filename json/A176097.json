{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A176097",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 176097,
			"data": "1,4,36,272,2150,16992,134848,1072192,8536914,68036600,542607560,4329671040,34561892560,275979195520,2204266118400,17609217372416,140698273234634,1124340854572296,8985828520591912,71822662173752800",
			"name": "Degree of the hyperdeterminant of the cubic format (k+1) X (k+1) X (k+1).",
			"reference": [
				"I. M. Gelfand, M. M. Kapranov and A. V. Zelevinsky, Discriminants, Resultants and Multidimensional Determinants, Birkhauser, 2008, p. 456 (Ch. 14, Corollary 2.9)."
			],
			"link": [
				"Arthur Cayley, \u003ca href=\"https://babel.hathitrust.org/cgi/pt?id=hvd.hxjnj3\u0026amp;view=1up\u0026amp;seq=211\"\u003eOn the theory of linear transformations\u003c/a\u003e, The Cambridge Mathematical Journal, Vol. IV, No. XXIII, February 1845, pp. 193-209. [Accessible only in the USA through the \u003ca href=\"https://www.hathitrust.org/accessibility\"\u003eHathi Trust Digital Library\u003c/a\u003e.]",
				"Arthur Cayley, \u003ca href=\"https://quod.lib.umich.edu/u/umhistmath/ABS3153.0001.001\"\u003eOn the theory of linear transformations\u003c/a\u003e, The collected mathematical papers of Arthur Cayley, Cambridge University Press (1889-1897), pp. 80-94. [Accessible through the \u003ca href=\"https://quod.lib.umich.edu/u/umhistmath/\"\u003eUniversity of Michigan Historical Math Collection\u003c/a\u003e; click on pp. 80 through 94.]",
				"Arthur Cayley, \u003ca href=\"https://babel.hathitrust.org/cgi/pt?id=uc1.a0002837516\u0026amp;view=1up\u0026amp;seq=114\"\u003eOn linear transformations\u003c/a\u003e, Cambridge and Dublin Mathematical Journal, Vol. I, 1846, pp. 104-122. [Accessible only in the USA through the \u003ca href=\"https://www.hathitrust.org/accessibility\"\u003eHathi Trust Digital Library\u003c/a\u003e.]",
				"Arthur Cayley, \u003ca href=\"https://quod.lib.umich.edu/u/umhistmath/ABS3153.0001.001\"\u003eOn linear transformations\u003c/a\u003e, The collected mathematical papers of Arthur Cayley, Cambridge University Press (1889-1897), pp. 95-112. [Accessible through the \u003ca href=\"https://quod.lib.umich.edu/u/umhistmath/\"\u003eUniversity of Michigan Historical Math Collection\u003c/a\u003e; click on pp. 95 through 112.]",
				"I. M. Gelfand, M. M. Kapranov, and A. V. Zelevinsky, \u003ca href=\"https://doi.org/10.1016/0001-8708(92)90056-Q\"\u003eHyperdeterminants\u003c/a\u003e, Advances in Mathematics 96(2) (1992), 226-263; see Corollary 3.9 (p. 246).",
				"David G. Glynn, \u003ca href=\"https://doi.org/10.1017/S0004972700031890\"\u003eThe modular counterparts of Cayley's hyperdeterminants\u003c/a\u003e, Bulletin of the Australian Mathematical Society 57(3) (1998), 479-492.",
				"Giorgio Ottaviani, Luca Sodomaco, and Emuanuele Ventura, \u003ca href=\"https://arxiv.org/abs/2008.11670\"\u003eAsymptotics of degrees and ED degrees of Segre products\u003c/a\u003e, arXiv:2008.11670 [math.AG], 2020.",
				"Ludwig Schläfli, \u003ca href=\"http://opacplus.bsb-muenchen.de/title/BV020984320/ft/bsb10942369?page=5\"\u003eÜber die Resultante eines Systemes mehrerer algebraischen Gleichungen, ein Beitrag zur Theorie der Elimination\u003c/a\u003e, Denkschr. der Kaiserlicher Akad. der Wiss. math-naturwiss. Klasse, 4 Band, 1852.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Hyperdeterminant.html\"\u003eHyperdeterminant\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Hyperdeterminant\"\u003eHyperdeterminants\u003c/a\u003e."
			],
			"formula": [
				"a(n) = Sum_{j = 0..n/2} ( (j+n+1)! * 2^(n-2j) )/((j!)^3 * (n-2j)!).",
				"a(n) = (n+1)^2*(8*A000172(n)-A000172(n+1))/6. - _Mark van Hoeij_, Jul 02 2010",
				"G.f.: hypergeom([-1/3, 1/3],[1],27*x^2/(1-2*x)^3)*(1-2*x)/((x+1)^2*(1-8*x)). - _Mark van Hoeij_, Apr 11 2014",
				"a(n) ~ 8^(n+1) / (Pi * 3^(3/2)). - _Vaclav Kotesovec_, Sep 12 2019",
				"a(n) = (a(n-1)*(21*n^3 - 10*n^2 - 9*n + 6) + a(n-2)*(24*n^3 + 16*n^2))/((3*n - 1)*n^2) for n \u003e= 2. - _Peter Luschny_, Sep 12 2019"
			],
			"example": [
				"For k=1, the hyperdeterminant of the matrix (a_ijk) (for 0 \u003c= i,j,k \u003c= 1) is (a_000 * a_111)^2 + (a001 * a110)^2 + (a_010 * a_101)^2 + (a_011 * a_100)^2 -2(a_000 * a_001 * a_110 * a_111 + a_000 * a_010 * a_101 * a_111 + a_000 * a_011 * a_100 * a_111 + a_001 * a_010 * a_101 * a_110 + a_001 * a_011 * a_110 * a_100 + a_010 * a_011 * a_101 * a_100) + 4(a_000 * a_011 * a_101 * a_110 + a_001 * a_010 * a_100 * a_111) (see Gelfand, Kapranov \u0026 Zelevinsky, pp. 2 and 448.) [Corrected by _Petros Hadjicostas_, Sep 12 2019]"
			],
			"maple": [
				"a:= k-\u003e add((j+k+1)! /(j!)^3 /(k-2*j)! *2^(k-2*j), j=0..floor(k/2)): seq(a(n), n=0..20);",
				"# Second program:",
				"a := proc(n) option remember; if n = 0 then return 1 elif n = 1 then return 4 fi;",
				"(a(n-1)*(21*n^3-10*n^2-9*n+6)+a(n-2)*(24*n^3+16*n^2))/((3*n-1)*n^2) end:",
				"seq(a(n), n=0..19); # _Peter Luschny_, Sep 12 2019"
			],
			"mathematica": [
				"Table[Sum[(j + n + 1)!*2^(n - 2*j)/(j!^3*(n - 2*j)!), {j, 0, n/2}], {n, 0, 20}] (* _Vaclav Kotesovec_, Sep 12 2019 *)"
			],
			"xref": [
				"Cf. A045899, A050269, A086302, A087981."
			],
			"keyword": "easy,nonn",
			"offset": "0,2",
			"author": "_Benjamin J. Young_, Apr 08 2010",
			"references": 1,
			"revision": 62,
			"time": "2021-11-30T11:38:23-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}