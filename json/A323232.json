{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A323232",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 323232,
			"data": "1,3,9,51,225,1083,5049,23811,111825,525963,2472489,11625171,54655425,256967643,1208146329,5680180131,26705711025,125558574123,590321410569,2775432824691,13048869758625,61350071873403,288441173689209,1356124096054851,6375901677678225",
			"name": "a(n) = 2^n*J(n, 1/2) where J(n, x) are the Jacobsthal polynomials as defined in A322942.",
			"comment": [
				"Is it true that p prime and p not 2 or 5 implies that a(p) is squarefree?"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A323232/b323232.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,8)."
			],
			"formula": [
				"a(n) = 3*a(n-1) + 8*a(n-2) for n \u003e= 3.",
				"a(n) is an odd integer and 3 | a(n) if n \u003e 0.",
				"a(n) = Sum_{k=0..n} 2^(n - k)*A322942(n, k).",
				"a(n) = [x^n] (8*x^2 - 1)/(8*x^2 + 3*x - 1).",
				"Let s = sqrt(41), u = -1/(s+3) and v = 1/(s-3); then",
				"a(n) = (3/s)*16^n*(v^n - u^n) for n \u003e= 1.",
				"a(n) = n! [x^n](1 + (6*exp(3*x/2)*sinh(s*x/2))/s).",
				"a(n) = n! [x^n](1 + (3/s)*(exp((3 + s)*x/2) - exp((3 - s)*x/2))).",
				"a(n)/a(n+1) -\u003e 2/(sqrt(41) + 3) = (sqrt(41) - 3)/16 for n -\u003e oo."
			],
			"example": [
				"The first few prime factorizations of a(n):",
				"   1| 3;",
				"   2| 3^2;",
				"   3| 3   * 17;",
				"   4| 3^2 * 5^2;",
				"   5| 3   * 19^2;",
				"   6| 3^3 * 11 * 17;",
				"   7| 3   * 7937;",
				"   8| 3^2 * 5^2 * 7 * 71;",
				"   9| 3   * 17 * 10313;",
				"  10| 3^2 * 19^2 * 761;",
				"  11| 3   * 3875057;",
				"  12| 3^3 * 5^2 * 11 * 17 * 433;",
				"  13| 3   * 85655881;",
				"  14| 3^2 * 13 * 1301 * 7937;",
				"  15| 3   * 17 * 19^2 * 308521;",
				"  16| 3^2 * 5^2 * 7 * 71 * 79 * 3023;",
				"  17| 3   * 67 * 624669523;",
				"  18| 3^4 * 11 * 17 * 3779 * 10313;",
				"  19| 3   * 419 * 2207981563;"
			],
			"maple": [
				"a := proc(n) option remember:",
				"    if n \u003c 3 then return [1, 3, 9][n+1] fi;",
				"    8*a(n-2) + 3*a(n-1) end:",
				"seq(a(n), n=0..24);"
			],
			"mathematica": [
				"LinearRecurrence[{3, 8}, {1, 3, 9}, 25]"
			],
			"program": [
				"(Sage)",
				"def a():",
				"    yield 1",
				"    yield 3",
				"    c = 3; b = 9",
				"    while True:",
				"        yield b",
				"        a = (b \u003c\u003c 2) + (c \u003c\u003c 3) - b",
				"        c = b",
				"        b = a",
				"A323232 = a()",
				"[next(A323232) for _ in range(30)]",
				"(MAGMA) [1] cat [n le 2 select 3^n else 3*Self(n-1) +8*Self(n-2): n in [1..30]]; // _G. C. Greubel_, Dec 27 2021"
			],
			"xref": [
				"Cf. A015525, A322942."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Peter Luschny_, Jan 07 2019",
			"references": 3,
			"revision": 14,
			"time": "2021-12-28T07:30:18-05:00",
			"created": "2019-01-08T04:12:30-05:00"
		}
	]
}