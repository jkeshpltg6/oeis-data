{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A093445",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 93445,
			"data": "1,3,3,6,9,6,10,18,17,10,15,30,33,27,15,21,45,54,51,39,21,28,63,80,82,72,53,28,36,84,111,120,114,96,69,36,45,108,147,165,165,150,123,87,45,55,135,188,217,225,215,190,153,107,55,66,165,234,276,294,291,270,234",
			"name": "The triangular triangle.",
			"comment": [
				"The n-th row of the triangular table begins by considering n triangular numbers (A000217) in order. Now segregate them into n groups beginning with n members in the first group, n-1 members in the second group, etc. Now sum each group. Thus the first term is the sum of first n numbers = n(n+1)/2, the second term is the sum of the next n-1 terms (from n+1 to 2n-1), the third term is the sum of the next n-2 terms (2n to 3n-3), etc. and the last term is simply n(n+1)/2. This triangle can be called a triangular triangle. The sequence contains the triangle by rows."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A093445/b093445.txt\"\u003eRows n = 1..100 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n) = A000217(n) is the n-th Triangular number. TT(n, k) is the k-th term of the n-th row, 0 \u003c k \u003c= n.",
				"TT(n, k) = T(k*n - T(k - 1)) - T((k - 1)*n - T(k - 2)).",
				"TT(n, 1) = TT(n, n) = T(n) = A000217(n)."
			],
			"example": [
				"Triangle begins:",
				"   1",
				"   3,  3",
				"   6,  9,   6",
				"  10, 18,  17,  10",
				"  15, 30,  33,  27,  15",
				"  21, 45,  54,  51,  39, 21",
				"  28, 63,  80,  82,  72, 53, 28",
				"  36, 84, 111, 120, 114, 96, 69, 36",
				"The row for n = 4 is (1+2+3+4), (5+6+7), (8+9), 10 =\u003e 10 18 17 10."
			],
			"maple": [
				"A093445 := proc(n,k)",
				"    A000217(k*n-A000217(k-1))-A000217((k-1)*n-A000217(k-2)) ;",
				"end proc:",
				"seq(seq(A093445(n,k),k=1..n),n=1..10) ; # _R. J. Mathar_, Dec 09 2015"
			],
			"mathematica": [
				"T[n_] := n(n + 1)/2; TT[n_, k_] := T[k*n - T[k - 1]] - T[(k - 1)*n - T[k - 2]]; Flatten[ Table[ TT[n, k], {n, 1, 11}, {k, 1, n}]] (* _Robert G. Wilson v_, Apr 24 2004 *)",
				"Table[Total/@TakeList[Range[(n(n+1))/2],Range[n,1,-1]],{n,20}]//Flatten (* Requires Mathematica version 10 or later *) (* _Harvey P. Dale_, Feb 15 2019 *)"
			],
			"program": [
				"(Haskell)",
				"a093445 n k = a093445_row n !! (k-1)",
				"a093445_row n = f [n, n - 1 .. 1] [1 ..] where",
				"   f [] _      = []",
				"   f (x:xs) ys = sum us : f xs vs where (us,vs) = splitAt x ys",
				"a093445_tabl = map a093445_row [1 ..]",
				"-- _Reinhard Zumkeller_, Oct 03 2012"
			],
			"xref": [
				"Cf. A000217, A093446. TT(n, 2) = A045943. TT(n, n-1) = A014209. TT(0, k) = A027480.",
				"Cf. A005920 (central terms), A002817 (row sums)."
			],
			"keyword": "nonn,nice,tabl",
			"offset": "1,2",
			"author": "_Amarnath Murthy_, Apr 02 2004",
			"ext": [
				"Edited, corrected and extended by _Robert G. Wilson v_, Apr 24 2004"
			],
			"references": 6,
			"revision": 22,
			"time": "2019-02-15T13:53:10-05:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}