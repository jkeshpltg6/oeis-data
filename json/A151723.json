{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A151723",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 151723,
			"data": "0,1,7,13,31,37,55,85,127,133,151,181,235,289,331,409,499,505,523,553,607,661,715,817,967,1069,1111,1189,1327,1489,1603,1789,1975,1981,1999,2029,2083,2137,2191,2293,2443,2545,2599,2701,2875,3097,3295",
			"name": "Total number of ON states after n generations of cellular automaton based on hexagons.",
			"comment": [
				"Analog of A151725, but here we are working on the triangular lattice (or the A_2 lattice) where each hexagonal cell has six neighbors.",
				"A cell is turned ON if exactly one of its six neighbors is ON. An ON cell remains ON forever.",
				"We start with a single ON cell.",
				"It would be nice to find a recurrence for this sequence!",
				"Has a behavior similar to A182840 and possibly to A182632. - _Omar E. Pol_, Jan 15 2016"
			],
			"reference": [
				"S. M. Ulam, On some mathematical problems connected with patterns of growth of figures, pp. 215-224 of R. E. Bellman, ed., Mathematical Problems in the Biological Sciences, Proc. Sympos. Applied Math., Vol. 14, Amer. Math. Soc., 1962 (see Example 6, page 224)."
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A151723/b151723.txt\"\u003eTable of n, a(n) for n = 0..4095\u003c/a\u003e [First 1026 terms from David Applegate and N. J. A. Sloane]",
				"David Applegate, \u003ca href=\"/A139250/a139250.anim.html\"\u003eThe movie version\u003c/a\u003e",
				"David Applegate and N. J. A. Sloane, \u003ca href=\"/A151723/a151723.txt\"\u003eTable of n, A151724(n), A151723(n) for n = 0..1025\u003c/a\u003e",
				"David Applegate, Omar E. Pol and N. J. A. Sloane, \u003ca href=\"/A000695/a000695_1.pdf\"\u003eThe Toothpick Sequence and Other Sequences from Cellular Automata\u003c/a\u003e, Congressus Numerantium, Vol. 206 (2010), 157-191. [There is a typo in Theorem 6: (13) should read u(n) = 4.3^(wt(n-1)-1) for n \u003e= 2.]",
				"Bradley Klee, \u003ca href=\"/A151723/a151723.png\"\u003eLog-periodic coloring, over the half-hexagon tiling\u003c/a\u003e.",
				"N. J. A. Sloane, \u003ca href=\"/wiki/Catalog_of_Toothpick_and_CA_Sequences_in_OEIS\"\u003eCatalog of Toothpick and Cellular Automata Sequences in the OEIS\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"https://www.youtube.com/watch?v=9ogbsh8KuEM\"\u003eExciting Number Sequences\u003c/a\u003e (video of talk), Mar 05 2021"
			],
			"formula": [
				"a(n) = 6*A169780(n) - 6*n + 1 (this is simply the definition of A169780).",
				"a(n) = 1 + 6*A169779(n-2), n \u003e= 2. - _Omar E. Pol_, Mar 19 2015",
				"It appears that a(n) = a(n-2) + 3*(A256537(n) - 1), n \u003e= 3. - _Omar E. Pol_, Apr 04 2015"
			],
			"mathematica": [
				"A151723[0] = 0; A151723[n_] := Total[CellularAutomaton[{10926, {2, {{2, 2, 0}, {2, 1, 2}, {0, 2, 2}}}, {1, 1}}, {{{1}}, 0}, {{{n - 1}}}], 2]; Array[A151723, 47, 0](* _JungHwan Min_, Sep 01 2016 *)",
				"A151723L[n_] := Prepend[Total[#, 2] \u0026 /@ CellularAutomaton[{10926, {2, {{2, 2, 0}, {2, 1, 2}, {0, 2, 2}}}, {1, 1}}, {{{1}}, 0}, n - 1], 0]; A151723L[46] (* _JungHwan Min_, Sep 01 2016 *)"
			],
			"xref": [
				"Cf. A147562, A151724, A151725, A161206, A161644, A169779, A169780, A170898, A170905, A182632, A182840, A256536, A256537."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_David Applegate_ and _N. J. A. Sloane_, Jun 13 2009",
			"ext": [
				"Edited by _N. J. A. Sloane_, Jan 10 2010"
			],
			"references": 40,
			"revision": 73,
			"time": "2021-03-23T10:41:45-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}