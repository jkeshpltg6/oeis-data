{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A278572",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 278572,
			"data": "1,1,1,2,1,3,1,3,-1,1,4,3,2,3,5,-1,5,1,4,7,-1,3,5,6,3,7,9,-1,3,5,2,7,1,5,9,-1,3,7,-1,-1,1,3,9,13,2,1,9,3,6,7,13,-1,10,13,7,2,9,11,15,-1,-1,4,8,14,-1",
			"name": "Irregular triangle read by rows: row n lists values of k in range 1 \u003c= k \u003c= n/2 such x^n + x^k + 1 is irreducible (mod 2), or -1 if no such k exists.",
			"comment": [
				"This is the format used by John Brillhart (1968) and Zierler and Brillhart (1968)."
			],
			"reference": [
				"Alanen, J. D., and Donald E. Knuth. \"Tables of finite fields.\" Sankhyā: The Indian Journal of Statistics, Series A (1964): 305-328.",
				"John Brillhart, On primitive trinomials (mod 2), unpublished Bell Labs Memorandum, 1968.",
				"Marsh, Richard W. Table of irreducible polynomials over GF (2) through degree 19. Office of Technical Services, US Department of Commerce, 1957.",
				"Mossige, S. \"Table of irreducible polynomials over 𝐺𝐹[2] of degrees 10 through 20.\" Mathematics of Computation 26.120 (1972): 1007-1009."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A278572/b278572.txt\"\u003eTable of n, a(n) for n = 2..4328\u003c/a\u003e (rows 2 to 2170, flattened)",
				"Joerg Arndt, \u003ca href=\"http://www.jjj.de/mathdata/all-trinomial-primpoly.txt\"\u003eComplete list of primitive trinomials over GF(2) up to degree 400\u003c/a\u003e. (Lists primitive trinomials only.)",
				"Joerg Arndt, \u003ca href=\"/A001153/a001153.txt\"\u003eComplete list of primitive trinomials over GF(2) up to degree 400\u003c/a\u003e [Cached copy, with permission]",
				"R. P. Brent, \u003ca href=\"http://maths.anu.edu.au/~brent/trinomlg.html\"\u003eTrinomial Log Files and Certificates\u003c/a\u003e",
				"A. J. Menezes, P. C. van Oorschot and S. A. Vanstone, \u003ca href=\"http://www.cacr.math.uwaterloo.ca/hac/\"\u003eHandbook of Applied Cryptography\u003c/a\u003e, CRC Press, 1996; see Table 4.6.",
				"N. Zierler and J. Brillhart, \u003ca href=\"http://dx.doi.org/10.1016/S0019-9958(68)90973-X\"\u003eOn primitive trinomials (mod 2)\u003c/a\u003e, Information and Control 13 1968 541-554.",
				"N. Zierler and J. Brillhart, \u003ca href=\"http://dx.doi.org/10.1016/S0019-9958(69)90356-8\"\u003eOn primitive trinomials (mod 2), II\u003c/a\u003e, Information and Control 14 1969 566-569.",
				"\u003ca href=\"/index/Tri#trinomial\"\u003eIndex entries for sequences related to trinomials over GF(2)\u003c/a\u003e"
			],
			"example": [
				"Triangle begins:",
				"1,",
				"1,",
				"1,",
				"2,",
				"1, 3,",
				"1, 3,",
				"-1,",
				"1, 4,",
				"3,",
				"2,",
				"3, 5,",
				"-1,",
				"5,",
				"1, 4, 7,",
				"-1,",
				"3, 5, 6,",
				"..."
			],
			"maple": [
				"T:= proc(n) local L; L:= select(k -\u003e Irreduc(x^n+x^k+1) mod 2, [$1..n/2]); if L = [] then -1 else op(L) fi",
				"end proc:",
				"map(T, [$2..100]); # _Robert Israel_, Mar 28 2017"
			],
			"mathematica": [
				"DeleteCases[#, 0] \u0026 /@ Table[Boole[IrreduciblePolynomialQ[x^n + x^# + 1, Modulus -\u003e 2]] # \u0026 /@ Range[Floor[n/2]], {n, 2, 40}] /. {} -\u003e {-1} // Flatten (* _Michael De Vlieger_, Mar 28 2017 *)"
			],
			"xref": [
				"Cf. A001153, A057646, A057774, A073571, A073646, A073726, A074743, A278573.",
				"Rows n that contain particular numbers: 1 (A002475), 2 (A057460), 3 (A057461), 4 (A057463), 5 (A057474), 6 (A057476), 7 (A057477), 8 (A057478), 9 (A057479), 10 (A057480), 11 (A057481), 12 (A057482), 13 (A057483)."
			],
			"keyword": "sign,tabf,more",
			"offset": "2,4",
			"author": "_N. J. A. Sloane_, Nov 27 2016",
			"references": 2,
			"revision": 49,
			"time": "2017-03-29T02:52:23-04:00",
			"created": "2016-11-27T19:34:49-05:00"
		}
	]
}