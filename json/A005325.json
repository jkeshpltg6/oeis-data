{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005325",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5325,
			"id": "M4176",
			"data": "1,6,27,104,369,1242,4037,12804,39897,122694,373581,1128816,3390582,10136556,30192102,89662216,265640691,785509362,2319218869,6839057544,20147488020,59306494520,174466248840,512987904000,1507780192035,4430417492826,13015498076181",
			"name": "Column of Motzkin triangle.",
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A005325/b005325.txt\"\u003eTable of n, a(n) for n = 5..1000\u003c/a\u003e",
				"R. Donaghey and L. W. Shapiro, \u003ca href=\"http://dx.doi.org/10.1016/0097-3165(77)90020-6\"\u003eMotzkin numbers\u003c/a\u003e, J. Combin. Theory, Series A, 23 (1977), 291-301.",
				"Nickolas Hein, Jia Huang, \u003ca href=\"https://arxiv.org/abs/1807.04623\"\u003eVariations of the Catalan numbers from some nonassociative binary operations\u003c/a\u003e, arXiv:1807.04623 [math.CO], 2018.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0912.0072\"\u003eUne méthode pour obtenir la fonction génératrice d'une série\u003c/a\u003e, arXiv:0912.0072 [math.NT], 2009; FPSAC 1993, Florence. Formal Power Series and Algebraic Combinatorics."
			],
			"formula": [
				"G.f.: z^5*M^6, where M=1+z*M+z^2*M^2 is the g.f. for the Motzkin numbers (A001006). - _Emeric Deutsch_, Aug 13 2004",
				"a(n) = (sqrt(-3)/81)*((-1)^n*n*(4*n^3-15*n^2-55*n+102)/(n+7)/(n+3)/(n+2)*hypergeom([1/2, n+7],[3],4/3)-(-1)^n*(4*n^4-17*n^3+23*n^2+ 242*n-288)/(n+7)/(n+3)/(n+2)*hypergeom([1/2, n+6],[3],4/3)). - _Mark van Hoeij_, Oct 29 2011.",
				"a(n) (n + 11) (n - 1) = (n + 4) (3 n + 9) a(n - 2) + (n + 4) (2 n + 9) a(n - 1). - _Simon Plouffe_, Feb 09 2012",
				"a(n) ~ 3^(n+5/2)/(n^(3/2)*sqrt(Pi)). - _Vaclav Kotesovec_, Oct 05 2012",
				"a(n) = 6*sum(j=ceiling((n-5)/2)..(n+1), C(j,2*j-n+5)*C(n+1,j))/(n+1). - _Vladimir Kruchinin_, Mar 17 2014"
			],
			"mathematica": [
				"RecurrenceTable[{3(-1+n)*n*a[-2+n]+n*(1+2n)*a[-1+n]-(-5+n)*(7+n)*a[n]==0, a[5]==1,a[6]==6}, a,{n,5,20}] (* _Vaclav Kotesovec_, Oct 05 2012 *)",
				"a = DifferenceRoot[Function[{b, n}, {(-2n^2 - 25n - 78)b[n+1] - 3(n+5)(n+6) b[n] + (n+1)(n+13)b[n+2] == 0, b[1] == 1, b[2] == 6}]][# - 4]\u0026;",
				"Table[a[n], {n, 5, 31}] (* _Jean-François Alcover_, Jan 24 2019 *)"
			],
			"program": [
				"(Maxima)",
				"a(n) := 6*sum(binomial(j,2*j-n+5)*binomial(n+1,j),j,ceiling((n-5)/2),(n+1))/(n+1);",
				"/* _Vladimir Kruchinin_, Mar 18 2014 */"
			],
			"xref": [
				"Cf. A026300, A026126, A001006.",
				"A diagonal of triangle A020474."
			],
			"keyword": "nonn",
			"offset": "5,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Vincenzo Librandi_, May 03 2013"
			],
			"references": 4,
			"revision": 62,
			"time": "2021-03-12T22:32:36-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}