{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A258803",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 258803,
			"data": "2,2,5,3,2,13,11,2,23,3,11,29,19,397,2,67,131,31,5,2,5,7,1039,5,7,67,3,787,2,13,83,149,2,89,47,43,31,809,3,5,2,307,5,61,41,5,67,19,11,1447,101,13,881,2,37,31,331,11,1033,3,19,839,2,61,163,59,41,1163,3,353,67,7,313,11,7,7,101,2,71,19,7,127,409,53,149,401,283,3,2,191,43,157,163,13,2,31,89,19,5,3",
			"name": "Least prime p such that n = (prime(q)-1)/(prime(p)-1) for some prime q.",
			"comment": [
				"Conjecture: a(n) exists for any n \u003e 0. Moreover, for any integers s and t with |s| = |t| = 1, each positive rational number r can be written as (prime(p) + s)/(prime(q) + t) with p and q both prime. - Sun",
				"I have verified the conjecture for all those rational numbers r = a/b with a, b = 1, ..., 500. - _Zhi-Wei Sun_, Jun 13 2015"
			],
			"reference": [
				"Zhi-Wei Sun, Problems on combinatorial properties of primes, in: M. Kaneko, S. Kanemitsu and J. Liu (eds.), Number Theory: Plowing and Starring through High Wave Forms, Proc. 7th China-Japan Seminar (Fukuoka, Oct. 28-Nov. 1, 2013), Ser. Number Theory Appl., Vol. 11, World Sci., Singapore, 2015, pp. 169-187."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A258803/b258803.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"/A258803/a258803.txt\"\u003eChecking the conjecture for r = n/m with 1 \u003c= n \u003c= m \u003c= 500\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/14\"\u003eProblems on combinatorial properties of primes\u003c/a\u003e, arXiv:1402.6641."
			],
			"example": [
				"a(1) = 2 since 1 = (prime(2) - 1)/(prime(2) - 1) with 2 prime.",
				"a(2) = 2 since 2 = (prime(3) - 1)/(prime(2) - 1) with 2 and 3 both prime.",
				"a(14) = 397 since 14 = (prime(4021) - 1)/(prime(397) - 1) = (38053 - 1)/(2719 - 1) with 379 and 4021 both prime.",
				"a(23) = 1039 since 23 = (prime(17209) - 1)/(prime(1039) - 1) = (190579 - 1)/(8287 - 1) with 1039 and 17209 both prime."
			],
			"mathematica": [
				"PQ[n_]:=PrimeQ[n] \u0026\u0026 PrimeQ[PrimePi[n]];",
				"Do[k = 0; Label[bb]; k = k + 1; If[PQ[n * (Prime[Prime[k]] - 1) + 1], Goto[aa], Goto[bb]]; Label[aa]; Print[n, \" \", Prime[k]]; Continue, {n, 1, 100}] (* Sun *)",
				"pq[n_] := PrimeQ@n \u0026\u0026 PrimeQ@ PrimePi@ n; a[n_] := Block[{k = 1}, While[!pq[1 + n*(Prime@ Prime@ k - 1)], k++]; Prime@k]; Array[a, 100] (* _Giovanni Resta_, Jun 11 2015 *)"
			],
			"xref": [
				"Cf. A000040."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Zhi-Wei Sun_, Jun 10 2015",
			"references": 10,
			"revision": 21,
			"time": "2015-06-15T04:12:25-04:00",
			"created": "2015-06-11T04:52:29-04:00"
		}
	]
}