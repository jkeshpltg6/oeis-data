{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A344857",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 344857,
			"data": "0,0,1,4,16,42,99,176,352,540,925,1152,2016,2534,3871,4608,6784,6984,11097,12580,17200,19250,25531,26016,36576,39988,50869,55076,68992,63570,91575,97920,119296,127024,152881,155088,193104,203946,240787,253360,296800,289044,362061,378884,437536,456918",
			"name": "Number of polygons formed when every pair of vertices of a regular n-gon are joined by an infinite line.",
			"comment": [
				"For odd n, a(n) is given by the equation in the Formula section below. See also A344866. For even n no such equation is currently known, although one similar to the general formula for the number of polygons inside an n-gon, see A007678, likely exists.",
				"The number of open regions, those outside the polygons with unbounded area and two edges that go to infinity, for n \u003e= 3 is given by n*(n-1) = A002378(n-1).",
				"See A345025 for the total number of all areas, both polygons and open regions."
			],
			"link": [
				"Scott R. Shannon, \u003ca href=\"/A344857/b344857.txt\"\u003eTable of n, a(n) for n = 1..100\u003c/a\u003e",
				"J. F. Rigby, \u003ca href=\"https://doi.org/10.1007/BF00147438\"\u003eMultiple intersections of diagonals of regular polygons, and related topics\u003c/a\u003e, Geom. Dedicata 9 (1980), 207-238.",
				"Scott R. Shannon, \u003ca href=\"/A344857/a344857.gif\"\u003eImage for n = 3\u003c/a\u003e. In this and other images the n-gon vertices are highlighted as white dots while the outer open regions, which are not counted, are darkened. The key for the edge-number coloring is shown at the top-left of the image.",
				"Scott R. Shannon, \u003ca href=\"/A344857/a344857_1.gif\"\u003eImage for n = 4\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A344857/a344857_2.gif\"\u003eImage for n = 5\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A344857/a344857_3.gif\"\u003eImage for n = 6\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A344857/a344857_4.gif\"\u003eImage for n = 7\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A344857/a344857_5.gif\"\u003eImage for n = 8\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A344857/a344857_6.gif\"\u003eImage for n = 9\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A344857/a344857_7.gif\"\u003eImage for n = 10\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A344857/a344857_8.gif\"\u003eImage for n = 11\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A344857/a344857_9.gif\"\u003eImage for n = 12\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A344857/a344857_10.gif\"\u003eImage for n = 13\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A344857/a344857_11.gif\"\u003eImage for n = 14\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A344857/a344857_12.gif\"\u003eImage for n = 15\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A344857/a344857_18.gif\"\u003eImage for n = 19\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A344857/a344857_16.gif\"\u003eImage for n = 21\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A344857/a344857_17.gif\"\u003eImage for n = 24\u003c/a\u003e.",
				"Alexander Sidorenko, \u003ca href=\"/A344857/a344857.txt\"\u003eExplicit Formulas for Odd-Indexed Terms in A344899, A146212, and A344857.\u003c/a\u003e"
			],
			"formula": [
				"For odd n, a(n) = (n^4 - 7*n^3 + 19*n^2 - 21*n + 8)/8 = (n-1)^2*(n^2-5*n+8)/8.  This was conjectured by _Scott R. Shannon_ and proved by Alexander Sidorenko on Sep 10 2021 (see link). - _N. J. A. Sloane_, Sep 12 2021",
				"See also A344866.",
				"a(n) = A344311(n) + A007678(n)."
			],
			"example": [
				"a(1) = a(2) = 0 as no polygon can be formed by one or two connected points.",
				"a(3) = 1 as the connected vertices form a triangle, while the six regions outside the triangle are open.",
				"a(4) = 4 as the four connected vertices form four triangles inside the square. Twelve open regions outside these polygons are also formed.",
				"a(5) = 16 as the five connected vertices form eleven polygons inside the regular pentagon while also forming five triangles outside the pentagon, giving sixteen polygons in total. Twenty open regions outside these polygons are also formed.",
				"a(6) = 42 as the six connected vertices form twenty-four polygons inside the regular hexagon while also forming eighteen polygons outside the hexagon, giving forty-two polygons in total. Thirty open regions outside these polygons are also formed.",
				"See the linked images above for further examples."
			],
			"xref": [
				"Cf. A344311 (number outside the n-gon), A007678 (number inside the n-gon), A345025 (total number of regions), A344866 (number for odd n), A146212 (number of vertices), A344899 (number of edges), A344938 (number of k-gons), A002378 (number of open regions for (n-1)-gon).",
				"Bisections: A344866, A347320."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Scott R. Shannon_, May 30 2021",
			"references": 15,
			"revision": 69,
			"time": "2021-09-12T12:18:14-04:00",
			"created": "2021-06-11T18:40:26-04:00"
		}
	]
}