{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A265428",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 265428,
			"data": "1,2,2,4,4,5,5,7,7,8,8,10,10,11,11,13,13,14,14,16,16,17,17,19,19,20,20,22,22,23,23,25,25,26,26,28,28,29,29,31,31,32,32,34,34,35,35,37,37,38,38,40,40,41,41,43,43,44,44,46,46,47,47,49,49,50,50",
			"name": "Number of ON (black) cells in the n-th iteration of the \"Rule 188\" elementary cellular automaton starting with a single ON (black) cell.",
			"reference": [
				"S. Wolfram, A New Kind of Science, Wolfram Media, 2002; p. 55."
			],
			"link": [
				"Robert Price, \u003ca href=\"/A265428/b265428.txt\"\u003eTable of n, a(n) for n = 0..999\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ElementaryCellularAutomaton.html\"\u003eElementary Cellular Automaton\u003c/a\u003e",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_Elementary_Cellular_Automata\"\u003eIndex to Elementary Cellular Automata\u003c/a\u003e"
			],
			"formula": [
				"Conjectures from _Colin Barker_, Dec 09 2015 and Apr 16 2019: (Start)",
				"a(n) = 1/8*(6*n-3*(-1)^n+(1-i)*(-i)^n+(1+i)*i^n+9) where i = sqrt(-1).",
				"a(n) = a(n-1) + a(n-4) - a(n-5) for n \u003e 4.",
				"G.f.: (1+x+2*x^3-x^4) / ((1-x)^2*(1+x)*(1+x^2)). (End)",
				"a(n) = (6*n+9+2*cos(n*Pi/2)-3*cos(n*Pi)-2*sin(n*Pi/2))/8. - _Wesley Ivan Hurt_, Oct 02 2017"
			],
			"example": [
				"From _Michael De Vlieger_, Dec 09 2015: (Start)",
				"First 12 rows, replacing \"0\" with \".\", ignoring \"0\" outside of range of 1's for better visibility of ON cells, with total number of 1's in the row to the left of the chart:",
				"1  =   1",
				"2  =   1 1",
				"2  =   1 . 1",
				"4  =   1 1 1 1",
				"4  =   1 1 1 . 1",
				"5  =   1 1 . 1 1 1",
				"5  =   1 . 1 1 1 . 1",
				"7  =   1 1 1 1 . 1 1 1",
				"7  =   1 1 1 . 1 1 1 . 1",
				"8  =   1 1 . 1 1 1 . 1 1 1",
				"8  =   1 . 1 1 1 . 1 1 1 . 1",
				"10 =   1 1 1 1 . 1 1 1 . 1 1 1",
				"10 =   1 1 1 . 1 1 1 . 1 1 1 . 1",
				"(End)"
			],
			"mathematica": [
				"rule = 188; rows = 30; Table[Total[Table[Take[CellularAutomaton[rule, {{1},0},rows-1,{All,All}][[k]], {rows-k+1, rows+k-1}], {k,1,rows}][[k]]], {k,1,rows}]",
				"Count[#, n_ /; n == 1] \u0026 /@ CellularAutomaton[188, {{1}, 0}, 66] (* _Michael De Vlieger_, Dec 09 2015 *)"
			],
			"xref": [
				"Cf. A118174."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Robert Price_, Dec 08 2015",
			"references": 2,
			"revision": 24,
			"time": "2019-04-16T15:24:11-04:00",
			"created": "2015-12-13T08:09:33-05:00"
		}
	]
}