{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A320842",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 320842,
			"data": "1,7,3,127,123,30,4369,6822,3579,630,243649,532542,439899,162630,22680,20036983,56717781,64697499,37155267,10735470,1247400,2280356863,7959325221,11656842609,9165745647,4079027880,973580580,97297200,343141433761,1427877062076,2563294235106,2572662311496,1558544277681,569674791180,116270210700,10216206000",
			"name": "Regular triangle whose rows are the coefficients of the Dominici expansion of f(t,x) = (1/2)*(1 - t^2)^(-x) with respect to t.",
			"comment": [
				"It appears that the first column (7, 127, 4369, ...) is from the sequence A002067.",
				"It appears that the diagonal (3, 30, 630, ...) is from the sequence A007019.",
				"It appears as though the unsigned row sum (10, 280, 15400, ...) is from the sequence A025035.",
				"It appears as though the alternating sign row sum (sum(7, -3) = 4, sum(-127, 123, -30) = -34, ...) is from the sequence A002105.",
				"This triangular array arises as the coefficients from terms in the inverse expansion of the function f(t,x) = (1/2)*(1 - t^2)^(-x) with respect to t evaluated at t = 0 for even values of the operation, using a method of Dominici's (nested derivatives, referenced below).",
				"Without proof, appears to be related to computing the 'critical t-value' of Student's t-distribution. (conj.) Critical t-value t_(v, beta) is equal to: sqrt((v/(1-S^2)) - v) where S = (1/2)*Sum_{k\u003e=1} (D^(2*k-2)[f]_(0)*(1/(2*k-1)!)*(B(1/2, v/2)*(1-2*beta))^(2*k-1)); where (1 - beta) is the confidence interval 'atta' (for a one-tailed distribution such that 'cumulative probability' = t_atta, where beta = 1-atta), x = 1 - (v/2), v: degrees of freedom, B(1/2, v/2) = gamma(1/2)*gamma(v/2)/gamma(1/2 + v/2), D^(2*k - 2)[f]_(0) is a polynomial function of 'x' whose coefficients are the terms of this sequence as computed using a method of Dominici's on f(t,x) with respect to t (referenced below)."
			],
			"link": [
				"Diego Dominici, \u003ca href=\"http://dx.doi.org/10.1155/S0161171203303291\"\u003eNested derivatives: a simple method for computing series expansions of inverse functions\u003c/a\u003e, International Journal of Mathematics and Mathematical Sciences, Volume 2003, Issue 58, Pages 3699-3715.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Student%27s_t-distribution\"\u003eStudent's t-distribution\u003c/a\u003e"
			],
			"example": [
				"Given D^k[f]_(b) = (d/dt [f(t)*D^(k-1)[f](t)])_t = b where D^0[f](b) = 1, then for f(t,x) = (1/2)*(1 - t^2)^(-x) where f(0) = 1/2 one obtains: D^2[f]_(0) = -x/2, D^4[f]_(0) = (x/4)*(7*x - 3), D^6[f]_(0) = -(x/8)*(127*x^2 - 123*x + 30), etc., where b is an arbitrary constant.",
				"Triangle begins:",
				"           1;",
				"           7,          3;",
				"         127,        123,          30;",
				"        4369,       6822,        3579,        630;",
				"      243649,     532542,      439899,     162630,      22680;",
				"    20036983,   56717781,    64697499,   37155267,   10735470,   1247400;",
				"  2280356863, 7959325221, 11656842609, 9165745647, 4079027880, 973580580, 97297200;",
				"         ..."
			],
			"xref": [
				"Cf. A002067, A007019, A025035, A002105."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Matthew Miller_, Dec 11 2018",
			"references": 0,
			"revision": 87,
			"time": "2019-01-23T09:17:52-05:00",
			"created": "2019-01-23T09:17:52-05:00"
		}
	]
}