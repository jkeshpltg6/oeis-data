{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A208250",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 208250,
			"data": "0,1,6,51,544,7145,112356,2066323,43574336,1036922769,27486891100,803137535321,25642631336400,888148407804853,33165208812574216,1328185604750416875,56783630865774075136,2581268127178259819297,124322489582200453748268,6324172127062894070727625",
			"name": "The sum of the largest preimage over all functions f:{1,2,...,n}-\u003e{1,2,...,n}.",
			"comment": [
				"n labeled balls are placed in n labeled urns.  The maximum number of balls in an urn is summed over all n^n possible configurations. a(n) is this sum."
			],
			"reference": [
				"R. Sedgewick and P. Flajolet, Analysis of Algorithms, Addison and Wesley, 1996, page 435."
			],
			"link": [
				"Robert Gerbicz, \u003ca href=\"/A208250/b208250.txt\"\u003eTable of n, a(n) for n = 0..386\u003c/a\u003e",
				"D. R. L. Brown, \u003ca href=\"https://ia.cr/2015/375\"\u003eBounds on surmising remixed keys\u003c/a\u003e, IACR, Report 2015/375, 2015-2016. See Table 1.",
				"Robert Gerbicz, \u003ca href=\"/A208250/a208250.txt\"\u003ea(n) for n = 0..1024\u003c/a\u003e (an a-file)",
				"Robert Gerbicz, \u003ca href=\"/A208250/a208250.c.txt\"\u003egmp code\u003c/a\u003e",
				"G. H. Gönnet, \u003ca href=\"https://cs.uwaterloo.ca/research/tr/1978/CS-78-46.pdf\"\u003eExpected length of the longest probe sequence in hash code searching\u003c/a\u003e, Journal of the ACM, 28:2 (1981), pp. 289-304.",
				"Michael Mitzenmacher, Andréa W. Richa, and Ramesh Sitaraman, \u003ca href=\"http://www.eecs.harvard.edu/~michaelm/postscripts/handbook2001.pdf\"\u003eThe Power of Two Random Choices: A Survey of Techniques and Results\u003c/a\u003e"
			],
			"formula": [
				"E.g.f.: Sum_{j\u003e=0} exp(x)^n - (Sum_{i=0..j} x^i/i!)^n.",
				"a(n) ~ n^n log n/log log n. More precisely, a(n)/n^n = Gamma^(-1)(n) - 3/2 + o(1) where Gamma^(-1) is the inverse of the gamma function. See Gönnet section 4 or Mitzenmacher et al. - _Charles R Greathouse IV_, Feb 20 2013"
			],
			"example": [
				"a(2) = 6.  The functions f:{1,2}-\u003e{1,2} written as words are: 11, 12, 21, 22 and we sum respectively 2 + 1 + 1 + 2 = 6."
			],
			"mathematica": [
				"f[n_] := n! Coefficient[ Series[ Sum[ Exp[n*x] - Sum[x^i/i!, {i, 0, j}]^n, {j, 0, n}], {x, 0, n}], x^n]; f[0] = 0; Array[f, 19, 0] (* modified by _Robert G. Wilson v_, Feb 20 2013 *)"
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Geoffrey Critzer_, Jan 15 2013",
			"references": 2,
			"revision": 56,
			"time": "2018-02-11T03:07:31-05:00",
			"created": "2013-01-16T07:35:53-05:00"
		}
	]
}