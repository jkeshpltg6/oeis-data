{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A295827",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 295827,
			"data": "-1,-1,3,-1,13,3,3,-1,57,13,35,3,21,3,3,-1,241,57,7,13,13,35,39,3,169,21,5,3,21,3,3,-1,993,241,11,57,7,7,5,13,3197,13,9,35,3,39,13,3,21,169,3,21,39,5,47,3,27,21,5,3,13,3,3,-1,4033,993,491,241",
			"name": "a(n) = least odd k \u003e 1 such that n and n*k have the same Hamming weight, or -1 if no such k exists.",
			"comment": [
				"The Hamming weight of a number n is given by A000120(n).",
				"Apparently, a(n) = -1 iff n = 2^k for some k \u003e= 0.",
				"Apparently, a(2^n + 1) = A020515(n) for any n \u003e 1.",
				"a(2^n - 1) = 3 for any n \u003e 1.",
				"a(n) = 3 iff n = A077459(k) for some k \u003e 1.",
				"This sequence has similarities with A292849: here we want A000120(n*a(n)) = A000120(n), there we want A000120(n*a(n)) = A000120(a(n)).",
				"For any n \u003e 0, if a(n) \u003e 0 then A292849(a(n)) \u003c= n."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A295827/b295827.txt\"\u003eTable of n, a(n) for n = 1..8192\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A295827/a295827.png\"\u003eLogarithmic scatterplot of the sequence for n=1..2^17 and a(n) \u003c 10^18\u003c/a\u003e"
			],
			"formula": [
				"a(2*n) = a(n) for any n \u003e 0."
			],
			"example": [
				"The first terms, alongside the binary representations of n and of n*a(n), are:",
				"  n     a(n)     bin(n)         bin(n*a(n))",
				"  --    ----     ------         -----------",
				"   1      -1          1                  -1",
				"   2      -1         10                 -10",
				"   3       3         11                1001",
				"   4      -1        100                -100",
				"   5      13        101             1000001",
				"   6       3        110               10010",
				"   7       3        111               10101",
				"   8      -1       1000               -1000",
				"   9      57       1001          1000000001",
				"  10      13       1010            10000010",
				"  11      35       1011           110000001",
				"  12       3       1100              100100",
				"  13      21       1101           100010001",
				"  14       3       1110              101010",
				"  15       3       1111              101101",
				"  16      -1      10000              -10000",
				"  17     241      10001       1000000000001",
				"  18      57      10010         10000000010",
				"  19       7      10011            10000101",
				"  20      13      10100           100000100"
			],
			"maple": [
				"f:= proc(n) local k,w;",
				"  if n = 2^padic:-ordp(n,2) then return -1 fi;",
				"  w:= convert(convert(n,base,2),`+`);",
				"  for k from 3 by 2 do",
				"    if convert(convert(n*k,base,2),`+`)=w then return k fi",
				"  od",
				"end proc:",
				"map(f, [$1..100]); # _Robert Israel_, Nov 28 2017"
			],
			"mathematica": [
				"Table[SelectFirst[Range[3, 10^4 + 1, 2], SameQ @@ Map[DigitCount[#, 2, 1] \u0026, {n, n #}] \u0026] /. m_ /; MissingQ@ m -\u003e -1, {n, 68}] (* _Michael De Vlieger_, Nov 28 2017 *)"
			],
			"program": [
				"(PARI) A057168(n)=n+bitxor(n, n+n=bitand(n, -n))\\n\\4+n \\\\ after _M. F. Hasler_ at A057168",
				"a(n) = n\\=2^valuation(n,2); if (n==1, -1, my(w=(n-1)/2); while(1, w=A057168(w); if((2*w+1)%n==0, return((2*w+1)/n))))"
			],
			"xref": [
				"Cf. A000120, A020515, A057168, A077459, A292849."
			],
			"keyword": "sign,base",
			"offset": "1,3",
			"author": "_Rémy Sigrist_, Nov 28 2017",
			"references": 2,
			"revision": 20,
			"time": "2017-12-02T21:57:30-05:00",
			"created": "2017-12-02T03:09:14-05:00"
		}
	]
}