{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A317451",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 317451,
			"data": "0,2,16,92,464,2182,9824,42936,183648,772746,3209968,13196564,53791408,217700110,875718080,3504277360,13959102912,55383875346,218965651152,862983998924,3391602170512,13295446717334,51999641009696,202948920530728,790569797639456,3074179492922778",
			"name": "a(n) = (n*A003500(n) - A231896(n))/2.",
			"comment": [
				"Derivative of Chebyshev second kind polynomials evaluated at 2."
			],
			"reference": [
				"R. Flórez, N. McAnally, and A. Mukherjees, Identities for the generalized Fibonacci polynomial, Integers, 18B (2018), Paper No. A2.",
				"R. Flórez, R. Higuita and A. Mukherjees, Characterization of the strong divisibility property for generalized Fibonacci polynomials, Integers, 18 (2018), Paper No. A14."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A317451/b317451.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Rigoberto Flórez, Robinson Higuita, and Alexander Ramírez, \u003ca href=\"https://arxiv.org/abs/1808.01264\"\u003e The resultant, the discriminant, and the derivative of generalized Fibonacci polynomials\u003c/a\u003e, arXiv:1808.01264 [math.NT], 2018.",
				"Rigoberto Flórez, Robinson Higuita, and Antara Mukherjee, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL21/Florez2/florez8.html\"\u003eStar of David and other patterns in the Hosoya-like polynomials triangles\u003c/a\u003e, Journal of Integer Sequences, Vol. 21 (2018), Article 18.4.6.",
				"R. Flórez, N. McAnally, and A. Mukherjees, \u003ca href=\"http://math.colgate.edu/~integers/s18b2/s18b2.Abstract.html\"\u003eIdentities for the generalized Fibonacci polynomial\u003c/a\u003e, Integers, 18B (2018), Paper No. A2.",
				"R. Flórez, R. Higuita and A. Mukherjees, \u003ca href=\"http://math.colgate.edu/~integers/s14/s14.Abstract.html\"\u003eCharacterization of the strong divisibility property for generalized Fibonacci polynomials\u003c/a\u003e, Integers, 18 (2018), Paper No. A14.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ChebyshevPolynomialoftheFirstKind.html\"\u003eChebyshev Polynomial of the First Kind\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (8,-18,8,-1)."
			],
			"formula": [
				"From _Colin Barker_, Aug 06 2018: (Start)",
				"G.f.: 2*x / (1 - 4*x + x^2)^2.",
				"a(n) = (sqrt(3)*((2-sqrt(3))^n - (2+sqrt(3))^n) + 3*((2-sqrt(3))^(1+n) + (2+sqrt(3))^(1+n))*n) / 18.",
				"a(n) = 8*a(n-1) - 18*a(n-2) + 8*a(n-3) - a(n-4) for n\u003e3.",
				"(End)"
			],
			"mathematica": [
				"CoefficientList[ Series[2 x/(x^2 - 4x + 1)^2, {x, 0, 25}], x] (* _Robert G. Wilson v_, Aug 07 2018 *)"
			],
			"program": [
				"(PARI) a(n) = subst(deriv(polchebyshev(n, 2)), x, 2); \\\\ _Michel Marcus_, Jul 29 2018.",
				"(PARI) concat(0, Vec(2*x / (1 - 4*x + x^2)^2 + O(x^40))) \\\\ _Colin Barker_, Aug 06 2018"
			],
			"xref": [
				"Cf. A003500, A231896, A133156 (Chebyshev polynomials of the second kind)."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Rigoberto Florez_, Jul 28 2018",
			"references": 5,
			"revision": 34,
			"time": "2019-04-25T13:30:13-04:00",
			"created": "2018-08-26T11:30:24-04:00"
		}
	]
}