{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A317613",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 317613,
			"data": "2,3,0,1,4,5,6,7,10,11,8,9,12,13,14,15,18,19,16,17,20,21,22,23,26,27,24,25,28,29,30,31,34,35,32,33,36,37,38,39,42,43,40,41,44,45,46,47,50,51,48,49,52,53,54,55,58,59,56,57,60,61,62,63,66,67,64",
			"name": "Permutation of the nonnegative integers: lodumo_4 of A047247.",
			"comment": [
				"Write n in base 8, then apply the following substitution to the rightmost digit: '0'-\u003e'2, '1'-\u003e'3', and vice versa. Convert back to decimal.",
				"A self-inverse permutation: a(a(n)) = n.",
				"Array whose columns are, in this order, A047463, A047621, A047451 and A047522, read by rows."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A317613/b317613.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"OEIS wiki, \u003ca href=\"https://oeis.org/wiki/Lodumo_transform\"\u003eLodumo transform\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_08\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,-2,2,-2,2,-2,2,-1)"
			],
			"formula": [
				"a(n) = 2*a(n-1) - 2*a(n-2) + 2*a(n-3) - 2*a(n-4) + 2*a(n-5) - 2*a(n-6) + 2*a(n-7) - a(n-8), n \u003e 7.",
				"a(n) = (4*(floor(((2*n + 4) mod 8)/4) - floor(((n + 2) mod 8)/4)) + 2*n)/2.",
				"a(n) = lod_4(A047247(n+1)).",
				"a(4*n) = A047463(n+1).",
				"a(4*n+1) = A047621(n+1).",
				"a(4*n+2) = A047451(n+1).",
				"a(4*n+3) = A047522(n+1).",
				"a(A042948(n)) = A047596(n+1).",
				"a(A042964(n+1)) = A047551(n+1).",
				"G.f.: (x^7 + x^5 + 3*x^3 - 2*x^2 - x + 2)/(x^8 - 2*x^7 + 2*x^6 - 2*x^5 + 2*x^4 - 2*x^3 + 2*x^2 - 2*x + 1).",
				"E.g.f.: x*exp(x) + cos(x) + sin(x) + cos(x/sqrt(2))*cosh(x/sqrt(2)) + (sqrt(2)*cos(x/sqrt(2)) - sin(x/sqrt(2)))*sinh(x/sqrt(2))."
			],
			"example": [
				"a(25) = a('3'1') = '3'3' = 27.",
				"a(26) = a('3'2') = '3'0' = 24.",
				"a(27) = a('3'3') = '3'1' = 25.",
				"a(28) = a('3'4') = '3'4' = 28.",
				"a(29) = a('3'5') = '3'5' = 29.",
				"The sequence as array read by rows:",
				"  A047463, A047621, A047451, A047522;",
				"        2,       3,       0,       1;",
				"        4,       5,       6,       7;",
				"       10,      11,       8,       9;",
				"       12,      13,      14,      15;",
				"       18,      19,      16,      17;",
				"       20,      21,      22,      23;",
				"       26,      27,      24,      25;",
				"       28,      29,      30,      31;",
				"  ..."
			],
			"mathematica": [
				"Table[(4*(Floor[1/4 Mod[2*n + 4, 8]] - Floor[1/4 Mod[n + 2, 8]]) + 2*n)/2, {n, 0, 100}]",
				"f[n_] := Block[{id = IntegerDigits[n, 8]}, FromDigits[ Join[Most@ id /. {{} -\u003e {0}}, {id[[-1]] /. {0 -\u003e 2, 1 -\u003e 3, 2 -\u003e 0, 3 -\u003e 1}}], 8]]; Array[f, 67, 0] (* or *)",
				"CoefficientList[ Series[(x^7 + x^5 + 3x^3 - 2x^2 - x + 2)/((x - 1)^2 (x^6 + x^4 + x^2 + 1)), {x, 0, 70}], x] (* or *)",
				"LinearRecurrence[{2, -2, 2, -2, 2, -2, 2, -1}, {2, 3, 0, 1, 4, 5, 6, 7}, 70] (* _Robert G. Wilson v_, Aug 01 2018 *)"
			],
			"program": [
				"(Maxima) makelist((4*(floor(mod(2*n + 4, 8)/4) - floor(mod(n + 2, 8)/4)) + 2*n)/2, n, 0, 100);",
				"(PARI) x='x+O('x^100); Vec((x^7+x^5+3*x^3-2*x^2-x+2)/((1-x)^2*(x^6+x^4+ x^2+1))) \\\\ _G. C. Greubel_, Sep 25 2018",
				"(MAGMA) m:=100; R\u003cx\u003e:=PowerSeriesRing(Integers(), m); Coefficients(R!((x^7+x^5+3*x^3-2*x^2-x+2)/((1-x)^2*(x^6+x^4+ x^2+1)))); // _G. C. Greubel_, Sep 25 2018"
			],
			"xref": [
				"Cf. A047225, A047243, A047257, A064429, A080412, A159959, A026185."
			],
			"keyword": "nonn,easy,base",
			"offset": "0,1",
			"author": "_Franck Maminirina Ramaharo_, Aug 01 2018",
			"references": 1,
			"revision": 21,
			"time": "2020-02-02T04:40:15-05:00",
			"created": "2018-09-07T03:43:53-04:00"
		}
	]
}