{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A090991",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 90991,
			"data": "6,10,16,26,42,68,110,178,288,466,754,1220,1974,3194,5168,8362,13530,21892,35422,57314,92736,150050,242786,392836,635622,1028458,1664080,2692538,4356618,7049156,11405774,18454930,29860704,48315634,78176338,126491972",
			"name": "Number of meaningful differential operations of the n-th order on the space R^6.",
			"comment": [
				"Apparently a(n) = A054886(n+2) for n=1..1000. - _Georg Fischer_, Oct 06 2018"
			],
			"link": [
				"Indranil Ghosh, \u003ca href=\"/A090991/b090991.txt\"\u003eTable of n, a(n) for n = 1..4772\u003c/a\u003e",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"B. Malesevic, \u003ca href=\"https://www.jstor.org/stable/43666958\"\u003eSome combinatorial aspects of differential operation composition on the space R^n\u003c/a\u003e, Univ. Beograd, Publ. Elektrotehn. Fak., Ser. Mat. 9 (1998), 29-33.",
				"Branko Malesevic, \u003ca href=\"http://arxiv.org/abs/0704.0750\"\u003eSome combinatorial aspects of differential operation compositions on space R^n\u003c/a\u003e, arXiv:0704.0750 [math.DG], 2007.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,1)."
			],
			"formula": [
				"a(k+4) = 3*a(k+2) - a(k).",
				"a(k) = 2*Fibonacci(k+3).",
				"From _Philippe Deléham_, Nov 19 2008: (Start)",
				"a(n) = a(n-1) + a(n-2), n\u003e2, where a(1)=6, a(2)=10.",
				"G.f.: 2*x*(3+2*x)/(1-x-x^2). (End)"
			],
			"maple": [
				"NUM := proc(k :: integer) local i,j,n,Fun,Identity,v,A; n := 6; # \u003c- DIMENSION Fun := (i,j)-\u003epiecewise(((j=i+1) or (i+j=n+1)),1,0); Identity := (i,j)-\u003epiecewise(i=j,1,0); v := matrix(1,n,1); A := piecewise(k\u003e1,(matrix(n,n,Fun))^(k-1),k=1,matrix(n,n,Identity)); return(evalm(v\u0026*A\u0026*transpose(v))[1,1]); end:"
			],
			"mathematica": [
				"CoefficientList[Series[2*(3+2z)/(1-z-z^2), {z, 0, 40}], z] (* _Vladimir Joseph Stephan Orlovsky_, Jun 11 2011 *)"
			],
			"program": [
				"(GAP) a:=[6,10];; for n in [3..40] do a[n]:=a[n-1]+a[n-2]; od; a; # _Muniru A Asiru_, Oct 06 2018",
				"(PARI) my(x='x+O('x^40)); Vec(2*x*(3+2*x)/(1-x-x^2)) \\\\ _G. C. Greubel_, Feb 02 2019",
				"(MAGMA) m:=40; R\u003cx\u003e:=PowerSeriesRing(Integers(), m); Coefficients(R!(  2*x*(3+2*x)/(1-x-x^2) )); // _G. C. Greubel_, Feb 02 2019",
				"(Sage) (2*(3+2*x)/(1-x-x^2)).series(x, 40).coefficients(x, sparse=False) # _G. C. Greubel_, Feb 02 2019"
			],
			"xref": [
				"Cf. A055389, A068922, A078642, A090989-A090995.",
				"Essentially the same as A006355."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_Branko Malesevic_, Feb 29 2004",
			"references": 8,
			"revision": 34,
			"time": "2019-03-21T11:35:38-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}