{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A098099",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 98099,
			"data": "1357911131517192,12,32,52,72,931333537394,14,34,54,74,951535557596,16,36,56,76,971737577798,18,38,58,78,9919395979910,110,310,510,710,911111311511711912,112,312,512,712,913113313513713914,114,314,514,714",
			"name": "Consider the succession of single digits of the positive odd integers: 1 3 5 7 9 1 1 1 3 1 5 1 7 1 9 2 1 2 3 2 5 ... (A031312). This sequence is the lexicographically earliest sequence of distinct positive even integers that produces the same succession of digits.",
			"comment": [
				"Original name: \"Write each even integer \u003e0 on a single label. Put the labels in numerical order to form an infinite sequence L. Now consider the succession of single digits of A005408 (odd numbers): 1 3 5 7 9 1 1 1 3 1 5 1 7 1 9 2 1 2 3 2 5 2 7 2 9 3 1 3 3 3 5 3 7 3 9... The sequence S gives a rearrangement of the labels that reproduces the same succession of digits, subject to the constraint that the smallest label must be used that does not lead to a contradiction.\"",
				"This could be roughly rephrased like this: Rewrite in the most economical way the \"odd numbers pattern\" using only even numbers, but rearranged. All the numbers of the sequence must be different one from another."
			],
			"reference": [
				"E. Angelini, \"Jeux de suites\", in Dossier Pour La Science, pp. 32-35, Volume 59 (Jeux math'), April/June 2008, Paris."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A098099/b098099.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"We must begin with \"1,3,5...\" and we cannot use \"1\" or \"13\" or \"135\" (only even terms are available), so the first possibility is \"1357911131517192\". For \"199,201,203...\" we won't be allowed to use \"1992\", for instance, since no term begins with a 0."
			],
			"mathematica": [
				"f[lst_List, k_] := Block[{L = lst, g, w, a = {}, m}, g[x_] := First@ FirstPosition[x, i_ /; EvenQ@ i]; Do[w = Take[L, g@ L]; L = Drop[L, Length@ w]; m = Take[L, g@ L]; While[Or[MemberQ[a, FromDigits@ w], IntegerLength@ FromDigits@ m \u003c Length@ m], w = Join[w, m]; L = Drop[L, Length@ m]; m = Take[L, g@ L]]; AppendTo[a, FromDigits@ w], {k}]; a]; f[Flatten@ Map[IntegerDigits, Range[1, 1000, 2]], 35] (* _Michael De Vlieger_, Nov 28 2015, Version 10 *)"
			],
			"xref": [
				"Cf. A005408, A005843, A097487, A097968."
			],
			"keyword": "base,easy,nonn",
			"offset": "1,1",
			"author": "_Eric Angelini_, Sep 22 2004",
			"ext": [
				"Name and Example edited by _Danny Rorabaugh_, Nov 28 2015"
			],
			"references": 4,
			"revision": 28,
			"time": "2015-11-28T21:48:36-05:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}