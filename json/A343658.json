{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A343658",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 343658,
			"data": "1,1,1,1,2,1,1,3,2,1,1,4,3,3,1,1,5,4,6,2,1,1,6,5,10,3,4,1,1,7,6,15,4,10,2,1,1,8,7,21,5,20,3,4,1,1,9,8,28,6,35,4,10,3,1,1,10,9,36,7,56,5,20,6,4,1,1,11,10,45,8,84,6,35,10,10,2,1",
			"name": "Array read by antidiagonals where A(n,k) is the number of ways to choose a multiset of k divisors of n.",
			"comment": [
				"First differs from A343656 at A(4,2) = 6, A343656(4,2) = 5.",
				"As a triangle, T(n,k) = number of ways to choose a multiset of n - k divisors of k."
			],
			"formula": [
				"A(n,k) = ((A000005(n), k)) = A007318(A000005(k) + k - 1, k).",
				"T(n,k) = ((A000005(k), n - k)) = A007318(A000005(k) + n - k - 1, n - k)."
			],
			"example": [
				"Array begins:",
				"       k=0 k=1 k=2 k=3 k=4 k=5 k=6 k=7 k=8",
				"  n=1:  1   1   1   1   1   1   1   1   1",
				"  n=2:  1   2   3   4   5   6   7   8   9",
				"  n=3:  1   2   3   4   5   6   7   8   9",
				"  n=4:  1   3   6  10  15  21  28  36  45",
				"  n=5:  1   2   3   4   5   6   7   8   9",
				"  n=6:  1   4  10  20  35  56  84 120 165",
				"  n=7:  1   2   3   4   5   6   7   8   9",
				"  n=8:  1   4  10  20  35  56  84 120 165",
				"  n=9:  1   3   6  10  15  21  28  36  45",
				"Triangle begins:",
				"   1",
				"   1   1",
				"   1   2   1",
				"   1   3   2   1",
				"   1   4   3   3   1",
				"   1   5   4   6   2   1",
				"   1   6   5  10   3   4   1",
				"   1   7   6  15   4  10   2   1",
				"   1   8   7  21   5  20   3   4   1",
				"   1   9   8  28   6  35   4  10   3   1",
				"   1  10   9  36   7  56   5  20   6   4   1",
				"   1  11  10  45   8  84   6  35  10  10   2   1",
				"For example, row n = 6 counts the following multisets:",
				"  {1,1,1,1,1}  {1,1,1,1}  {1,1,1}  {1,1}  {1}  {}",
				"               {1,1,1,2}  {1,1,3}  {1,2}  {5}",
				"               {1,1,2,2}  {1,3,3}  {1,4}",
				"               {1,2,2,2}  {3,3,3}  {2,2}",
				"               {2,2,2,2}           {2,4}",
				"                                   {4,4}",
				"Note that for n = 6, k = 4 in the triangle, the two multisets {1,4} and {2,2} represent the same divisor 4, so they are only counted once under A343656(4,2) = 5."
			],
			"mathematica": [
				"multchoo[n_,k_]:=Binomial[n+k-1,k];",
				"Table[multchoo[DivisorSigma[0,k],n-k],{n,10},{k,n}]"
			],
			"xref": [
				"Row k = 1 of the array is A000005.",
				"Column n = 4 of the array is A000217.",
				"Column n = 6 of the array is A000292.",
				"Row k = 2 of the array is A184389.",
				"The distinct products of these multisets are counted by A343656.",
				"Antidiagonal sums of the array (or row sums of the triangle) are A343661.",
				"A000312 = n^n.",
				"A009998(n,k) = n^k (as an array, offset 1).",
				"A007318 counts k-sets of elements of {1..n}.",
				"A059481 counts k-multisets of elements of {1..n}.",
				"Cf. A000169, A062319, A067824, A143773, A146291, A176029, A285572, A326077, A327527, A334996, A343652, A343657."
			],
			"keyword": "nonn,tabl",
			"offset": "1,5",
			"author": "_Gus Wiseman_, Apr 29 2021",
			"references": 12,
			"revision": 12,
			"time": "2021-04-30T17:15:40-04:00",
			"created": "2021-04-30T17:15:40-04:00"
		}
	]
}