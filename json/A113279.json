{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A113279",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 113279,
			"data": "2,-1,1,-2,-1,3,1,-4,2,-1,5,-5,1,-6,9,-2,-1,7,-14,7,1,-8,20,-16,2,-1,9,-27,30,-9,1,-10,35,-50,25,-2,-1,11,-44,77,-55,11,1,-12,54,-112,105,-36,2,-1,13,-65,156,-182,91,-13,1,-14,77,-210,294,-196,49,-2,-1,15,-90,275,-450,378,-140,15",
			"name": "Triangle T(n,k) of coefficients of r_1^n+r_2^n in terms of p and q, where r_1,r_2 are the roots of x^2+px+q=0.",
			"comment": [
				"The triangle begins: 2 .-1 1..-2 .-1..3 1..-4.2",
				"From _Tom Copeland_, Nov 07 2015: (Start)",
				"The row polynomials are the power sums p_n = x1^n + x2^n of the solutions of 0 = (x-x1)(x-x2) = x^2 - (x1+x2) x + x1x2 = x^2 + px + q, so -p = x1+x2 = e1(x1,x2), the first order elementary symmetric polynomial for two variables, or indeterminates, and q = x1*x2 = e2(x1,x2), the second order elementary symmetric polynomial.",
				"The Girard-Newton-Waring identities (Wikipedia) express the power sums in terms of the elementary symmetric polynomials, giving",
				"p_0 = x1^0 + x0^0 = 2",
				"p_1 = x1 + x2 = e1 = -p = F(1,p,q,0,..)",
				"p_2 = x1^2 + x2^2 =  e1^2 - 2 e2 = p^2 - 2 q = F(2,p,q,0,..)",
				"p_3 = e1^3 - 3 e2 e1 = -p^3 + 3 pq = F(3,p,q,0,..)",
				"p_4 = p^4 - 4 p^2 q + 2 q^2 = F(4,p,q,0,..)",
				"... .",
				"These bivariate partition polynomials are the Faber polynomials F(n,b1,b2,..,bn) of A263916 with b1 = -e1 = p, b2 = e2 = q, and all other indeterminates set to zero.",
				"Let p = q = t, then",
				"F(1,t,t,0,..)/t = -1",
				"F(2,t,t,0,..)/t = -2 + t",
				"F(3,t,t,0,..)/t^2 = 3 - t",
				"F(4,t,t,0,..)/t^2 = 2 - 4 t + t^2",
				"... .",
				"Or,",
				"t * F(1,1/t,1/t,0,..) = -1",
				"t^2 * F(2,1/t,1/t,0,..) = 1 -2 t",
				"t^3 * F(3,1/t,1/t,0,..) = -1 + 3 t",
				"t^4 * F(4,1/t,1/t,0,..) = 1 - 4 t + 2 t^2",
				"... .",
				"The sequence of Faber polynomials F(n,1/t,1/t,0,..) is obtained from the logarithmic generator -log[1+(x+x^2)/t] = sum{n\u003e=1, F(n,1/t,1/t,0,..) x^n/n}, so",
				"  2-(2xt+1)xt/(t+xt+(xt)^2) = 2 + sum{n\u003e=1, t^n F(n,1//t,1/t,0,..) x^n} is an o.g.f. for the row polynomials of this entry.",
				"(End)"
			],
			"reference": [
				"M. Herkenhoff Konersmann, Sprokkel XXXI: x_1^n+x_2^n, Nieuw Tijdschr. Wisk, 42 (1954-55) 180."
			],
			"link": [
				"T. Copeland, \u003ca href=\"http://tcjpn.wordpress.com/2015/10/12/the-elliptic-lie-triad-kdv-and-ricattt-equations-infinigens-and-elliptic-genera/\"\u003eAddendum to Elliptic Lie Triad\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Newton%27s_identities\"\u003eNewton's identities\u003c/a\u003e."
			],
			"formula": [
				"T(n, k) = (-1)^(n+k)*A034807(n, k).",
				"O.g.f.: 2-(2xt+1)xt/(t+xt+(xt)^2) = (2+x)/(1+x+x^2/t). - _Tom Copeland_, Nov 07 2015"
			],
			"example": [
				"x_1^5+x_2^5 = -p^5 + 5p^3q - 5pq^2, so row 5 reads -1, 5, -5."
			],
			"xref": [
				"Cf. A034807, A263916."
			],
			"keyword": "easy,sign,tabf",
			"offset": "0,1",
			"author": "_Floor van Lamoen_, Oct 22 2005",
			"references": 3,
			"revision": 20,
			"time": "2016-01-14T05:08:58-05:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}