{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A326987",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 326987,
			"data": "0,0,1,0,1,2,1,0,2,2,1,3,1,2,3,0,1,4,1,3,3,2,1,4,2,2,3,3,1,6,1,0,3,2,3,6,1,2,3,4,1,6,1,3,5,2,1,5,2,4,3,3,1,6,3,4,3,2,1,9,1,2,5,0,3,6,1,3,3,6,1,8,1,2,5,3,3,6,1,5,4,2,1,9,3,2,3,4,1,10,3,3,3,2,3,6,1,4,5,6",
			"name": "Number of nonpowers of 2 dividing n.",
			"comment": [
				"In other words: a(n) is the number of divisors of n that are not powers of 2.",
				"a(n) is also the number of odd divisors \u003e 1 of n, multiplied by the number of divisors of n that are powers of 2.",
				"a(n) = 0 iff n is a power of 2.",
				"a(n) = 1 iff n is an odd prime.",
				"From _Bernard Schott_, Sep 12 2019: (Start)",
				"a(n) = 2 iff n is an even semiprime \u003e= 6 or n is a square of prime \u003e= 9 (Aug 26 2019).",
				"a(n) = 3 iff n is an odd squarefree semiprime, or n is an odd prime multiplied by 4, or n is a cube of odd prime (End)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A326987/b326987.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A000005(n) - A001511(n).",
				"a(n) = (A001227(n) - 1)*A001511(n).",
				"a(n) = A069283(n)*A001511(n)."
			],
			"example": [
				"For n = 18 the divisors of 18 are [1, 2, 3, 6, 9, 18]. There are four divisors of 18 that are not powers of 2, they are [3, 6, 9, 18], so a(18) = 4. On the other hand, there are two odd divisors \u003e 1 of 18, they are [3, 9], and there are two divisors of 18 that are powers of 2, they are [1, 2], then we have that 2*2 = 4, so a(18) = 4."
			],
			"maple": [
				"a:= n-\u003e numtheory[tau](n)-padic[ordp](2*n, 2):",
				"seq(a(n), n=1..100);  # _Alois P. Heinz_, Aug 24 2019"
			],
			"mathematica": [
				"a[n_] := DivisorSigma[0, n] - IntegerExponent[n, 2] - 1; Array[a, 100] (* _Amiram Eldar_, Aug 31 2019 *)"
			],
			"program": [
				"(MAGMA) sol:=[];  m:=1;  for n in [1..100] do v:=Set(Divisors(n)) diff {2^k:k in [0..Floor(Log(2,n))]};  sol[m]:=#v; m:=m+1; end for; sol; // _Marius A. Burtea_, Aug 24 2019",
				"(PARI) ispp2(n) = (n==1) || (isprimepower(n, \u0026p) \u0026\u0026 (p==2));",
				"a(n) = sumdiv(n, d, ispp2(d) == 0); \\\\ _Michel Marcus_, Aug 26 2019"
			],
			"xref": [
				"Cf. A000005, A000079, A001227, A001248, A001511, A057716, A065091, A069283, A100484, A326988 (sum), A326989."
			],
			"keyword": "nonn,easy",
			"offset": "1,6",
			"author": "_Omar E. Pol_, Aug 18 2019",
			"references": 6,
			"revision": 62,
			"time": "2019-09-17T19:16:49-04:00",
			"created": "2019-09-12T16:19:16-04:00"
		}
	]
}