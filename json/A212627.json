{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A212627",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 212627,
			"data": "1,2,1,1,1,1,0,3,0,3,1,0,1,1,0,1,0,3,1,0,3,1,0,3,1,0,1,2,0,1,2,0,1,2,0,1,4,1,0,0,1,0,1,2,0,0,5,1,0,0,1,0,2,1,1,0,2,1,1,0,1,4,0,0,5,0,1,0,2,0,0,6,1,0,0,5,0,0,7,1,0,0,2,1,0,2,1,1,0,0,4,2,0,1,4,1,0,0,0,1,0,0,6,1,0,2,1,1,0,1,1,3,0,0,1,4,0,1,0,2,0,1,0,2,0,0,4,2,0,2,0,1,1,0,0,5,0,0,2,3,0,0,2,1,0,1,1,3,0,0,3,6",
			"name": "Irregular triangle read by rows: T(n,k) is the number of maximal independent vertex subsets with k vertices of the rooted tree with Matula-Goebel number n (n\u003e=1, k\u003e=1).",
			"comment": [
				"A vertex subset in a tree is said to be independent if no pair of vertices is connected by an edge. The empty set is considered to be independent. An independent vertex subset S of a tree is said to be maximal if every vertex that is not in S is joined by an edge to at least one vertex of S.",
				"The Matula-Goebel number of a rooted tree can be defined in the following recursive manner: to the one-vertex tree there corresponds the number 1; to a tree T with root degree 1 there corresponds the t-th prime number, where t is the Matula-Goebel number of the tree obtained from T by deleting the edge emanating from the root; to a tree T with root degree m\u003e=2 there corresponds the product of the Matula-Goebel numbers of the m branches of T.",
				"Number of entries in row n is A212625(n).",
				"Sum of entries in row n = A212628(n).",
				"Sum(k*T(n,k), k\u003e=1) = A212629(n)."
			],
			"reference": [
				"F. Goebel, On a 1-1 correspondence between rooted trees and natural numbers, J. Combin. Theory, B 29 (1980), 141-143.",
				"I. Gutman and A. Ivic, On Matula numbers, Discrete Math., 150, 1996, 131-142.",
				"I. Gutman and Y-N. Yeh, Deducing properties of trees from their Matula numbers, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"D. W. Matula, A natural rooted tree enumeration by prime factorization, SIAM Review, 10, 1968, 273.",
				"É. Czabarka, L. Székely, and S. Wagner, The inverse problem for certain tree parameters, Discrete Appl. Math., 157, 2009, 3314-3319.",
				"H. S. Wilf, The number of maximal independent sets in a tree, SIAM J. Alg. Disc. Math., 7, 1986, 125-130."
			],
			"link": [
				"E. Deutsch, \u003ca href=\"http://arxiv.org/abs/1111.4288\"\u003e Rooted tree statistics from Matula numbers\u003c/a\u003e, arXiv:1111.4288.",
				"\u003ca href=\"/index/Mat#matula\"\u003eIndex entries for sequences related to Matula-Goebel numbers\u003c/a\u003e"
			],
			"formula": [
				"Let A(n)=A(n,x), B(n)=B(n,x), C(n)=C(n,x) be the generating polynomial with respect to size of the maximal independent sets that contain the root, the maximal independent sets that do not contain the root, and the independent sets which are not maximal but become maximal if the root is removed, respectively. We have : A(1)=x, B(1)=0, C(1)=1, A(t-th prime) = x[B(t) + C(t)], B(t-th prime) = A(t), C(t-th prime)=B(t), A(rs)=A(r)A(s)/x, B(rs)=B(r)B(s)+B(r)C(s)+B(s)C(r), C(rs)=C(r)C(s) (r,s\u003e=2). The generating polynomial of the maximal independent vertex subsets of the r oo ted tree with Matula-Goebel number n, with respect to number of vertices, is P(n)=P(n,x)=A(n)+B(n). The Maple program is based on these relations."
			],
			"example": [
				"Row 11 is 0, 3, 1 because the rooted tree with Matula-Goebel number 11 is the path tree on 5 vertices R - A - B - C - D; the maximal independent vertex subsets are {R,C}, {A,C}, {A,D}, and {R,B,D}, i.e. none of size 1, three of size 2, and  one of size 3.",
				"Triangle starts:",
				"1;",
				"2;",
				"1,2;",
				"1,1;",
				"0,3;"
			],
			"maple": [
				"with(numtheory): P := proc (n) local r, s, A, B, C: r := proc (n) options operator, arrow: op(1, factorset(n)) end proc: s := proc (n) options operator, arrow: n/r(n) end proc: A := proc (n) if n = 1 then x elif bigomega(n) = 1 then x*(B(pi(n))+C(pi(n))) else A(r(n))*A(s(n))/x end if end proc: B := proc (n) if n = 1 then 0 elif bigomega(n) = 1 then A(pi(n)) else sort(expand(B(r(n))*B(s(n))+B(r(n))*C(s(n))+B(s(n))*C(r(n)))) end if end proc: C := proc (n) if n = 1 then 1 elif bigomega(n) = 1 then B(pi(n)) else expand(C(r(n))*C(s(n))) end if end proc: if n = 1 then x else sort(expand(A(n)+B(n))) end if end proc: for n to 12 do seq(coeff(P(n), x, j), j = 1 .. degree(P(n))) end do; # yields sequence in triangular form"
			],
			"xref": [
				"Cf. A212618, A212619, A212620, A212621, A212622, A212623, A212624, A212625, A212626, A212628, A212629, A212630, A212631, A212632."
			],
			"keyword": "nonn,tabf",
			"offset": "1,2",
			"author": "_Emeric Deutsch_, Jun 08 2012",
			"references": 11,
			"revision": 17,
			"time": "2017-03-07T11:22:58-05:00",
			"created": "2012-06-09T03:35:54-04:00"
		}
	]
}