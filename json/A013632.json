{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A013632",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 13632,
			"data": "2,1,1,2,1,2,1,4,3,2,1,2,1,4,3,2,1,2,1,4,3,2,1,6,5,4,3,2,1,2,1,6,5,4,3,2,1,4,3,2,1,2,1,4,3,2,1,6,5,4,3,2,1,6,5,4,3,2,1,2,1,6,5,4,3,2,1,4,3,2,1,2,1,6,5,4,3,2,1,4,3,2,1,6,5,4,3,2,1,8,7,6,5,4,3,2,1,4,3,2,1,2,1,4,3",
			"name": "Difference between n and the next prime greater than n.",
			"comment": [
				"Alternatively, a(n) is the smallest positive k such that n + k is prime. - _N. J. A. Sloane_, Nov 18 2015",
				"Except for a(0) and a(1), a(n) is the least k such that gcd(n!, n + k) = 1. - _Robert G. Wilson v_, Nov 05 2010",
				"This sequence uses the \"strictly larger\" variant A151800 of the nextprime function, rather than A007918. Therefore all terms are positive and a(n) = 1 if and only if n + 1 is a prime. - _M. F. Hasler_, Sep 09 2015",
				"For n \u003e 0, a(n) and n are of opposite parity. Also, by Bertrand's postulate (actually a theorem), for n \u003e 1, a(n) \u003c n. - _Zak Seidov_, Dec 27 2018"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A013632/b013632.txt\"\u003eTable of n, a(n) for n = 0..5000\u003c/a\u003e",
				"Brăduţ Apostol, Laurenţiu Panaitopol, Lucian Petrescu, and László Tóth, \u003ca href=\"http://arxiv.org/abs/1503.01086\"\u003eSome properties of a sequence defined with the aid of prime numbers\u003c/a\u003e, arXiv:1503.01086 [math.NT], 2015.",
				"Brăduţ Apostol, Laurenţiu Panaitopol, Lucian Petrescu, and László Tóth, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL18/Toth/toth21.html\"\u003eSome Properties of a Sequence Defined with the Aid of Prime Numbers\u003c/a\u003e, J. Int. Seq. 18 (2015) # 15.5.5."
			],
			"example": [
				"a(30) = 1 because 31 is the next prime greater than 30 and 31 - 30 = 1.",
				"a(31) = 6 because 37 is the next prime greater than 31 and 37 - 31 = 6."
			],
			"maple": [
				"[ seq(nextprime(i)-i,i=0..100) ];"
			],
			"mathematica": [
				"Array[NextPrime[#] - # \u0026, 105, 0] (* _Robert G. Wilson v_, Nov 05 2010 *)"
			],
			"program": [
				"(PARI) a(n) = nextprime(n+1) - n; \\\\ _Michel Marcus_, Mar 04 2015",
				"(MAGMA) [NextPrime(n) - n: n in [0..100]]; // _Vincenzo Librandi_, Dec 27 2018"
			],
			"xref": [
				"Cf. A007918, A151800, A007920."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Incorrect comment removed by _Charles R Greathouse IV_, Mar 18 2010",
				"More terms from _Robert G. Wilson v_, Nov 05 2010"
			],
			"references": 41,
			"revision": 49,
			"time": "2020-11-14T07:15:40-05:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}