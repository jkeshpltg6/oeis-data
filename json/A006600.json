{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006600",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6600,
			"id": "M4513",
			"data": "1,8,35,110,287,632,1302,2400,4257,6956,11297,17234,25935,37424,53516,73404,101745,136200,181279,236258,306383,389264,495650,620048,772785,951384,1167453,1410350,1716191,2058848,2463384,2924000,3462305,4067028,4776219,5568786,6479551",
			"name": "Total number of triangles visible in regular n-gon with all diagonals drawn.",
			"comment": [
				"Place n equally-spaced points on a circle, join them in all possible ways; how many triangles can be seen?"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A006600/b006600.txt\"\u003eTable of n, a(n) for n=3..1000\u003c/a\u003e",
				"Sascha Kurz, \u003ca href=\"http://www.mathe2.uni-bayreuth.de/sascha/oeis/drawing/drawing.html\"\u003em-gons in regular n-gons\u003c/a\u003e",
				"Victor Meally, \u003ca href=\"/A006556/a006556.pdf\"\u003eLetter to N. J. A. Sloane\u003c/a\u003e, no date.",
				"B. Poonen and M. Rubinstein, \u003ca href=\"http://epubs.siam.org:80/sam-bin/dbq/article/28124\"\u003eNumber of Intersection Points Made by the Diagonals of a Regular Polygon\u003c/a\u003e, SIAM J. Discrete Mathematics, Vol. 11, pp. 135-156.",
				"B. Poonen and M. Rubinstein, \u003ca href=\"http://math.mit.edu/~poonen/papers/ngon.pdf\"\u003eThe number of intersection points made by the diagonals of a regular polygon\u003c/a\u003e, SIAM J. on Discrete Mathematics, Vol. 11, No. 1, 135-156 (1998).",
				"B. Poonen and M. Rubinstein, \u003ca href=\"http://arXiv.org/abs/math.MG/9508209\"\u003eThe number of intersection points made by the diagonals of a regular polygon\u003c/a\u003e, arXiv version, which has fewer typos than the SIAM version.",
				"B. Poonen and M. Rubinstein, \u003ca href=\"http://math.mit.edu/~poonen/papers/ngon.m\"\u003eMathematica programs for these sequences\u003c/a\u003e",
				"M. Rubinstein, \u003ca href=\"/A006561/a006561_3.pdf\"\u003eDrawings for n=4,5,6,...\u003c/a\u003e",
				"T. Sillke, \u003ca href=\"http://www.mathematik.uni-bielefeld.de/~sillke/SEQUENCES/triangle_counting\"\u003eNumber of triangles for convex n-gon\u003c/a\u003e",
				"S. E. Sommars and T. Sommars, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/sommars/newtriangle.html\"\u003eNumber of Triangles Formed by Intersecting Diagonals of a Regular Polygon\u003c/a\u003e, J. Integer Sequences, 1 (1998), #98.1.5.",
				"\u003ca href=\"/index/Pol#Poonen\"\u003eSequences formed by drawing all diagonals in regular polygon\u003c/a\u003e"
			],
			"formula": [
				"a(2n-1) = A005732(2n-1) for n \u003e 1; a(2n) = A005732(2n) - A260417(n) for n \u003e 1. - _Jonathan Sondow_, Jul 25 2015"
			],
			"example": [
				"a(4) = 8 because in a quadrilateral the diagonals cross to make four triangles, which pair up to make four more."
			],
			"mathematica": [
				"del[m_,n_]:=If[Mod[n,m]==0,1,0]; Tri[n_]:=n(n-1)(n-2)(n^3+18n^2-43n+60)/720 - del[2,n](n-2)(n-7)n/8 - del[4,n](3n/4) - del[6,n](18n-106)n/3 + del[12,n]*33n + del[18,n]*36n + del[24,n]*24n - del[30,n]*96n - del[42,n]*72n - del[60,n]*264n - del[84,n]*96n - del[90,n]*48n - del[120,n]*96n - del[210,n]*48n; Table[Tri[n], {n,3,1000}] (* _T. D. Noe_, Dec 21 2006 *)"
			],
			"xref": [
				"Often confused with A005732.",
				"Cf. A203016, A260417.",
				"Sequences related to chords in a circle: A001006, A054726, A006533, A006561, A006600, A007569, A007678. See also entries for chord diagrams in Index file."
			],
			"keyword": "nonn,easy,nice",
			"offset": "3,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"a(3)-a(8) computed by Victor Meally (personal communication to _N. J. A. Sloane_, circa 1975); later terms and recurrence from S. Sommars and T. Sommars."
			],
			"references": 17,
			"revision": 46,
			"time": "2021-06-18T11:37:08-04:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}