{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A228469",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 228469,
			"data": "2,8,13,49,80,302,493,1861,3038,11468,18721,70669,115364,435482,710905,2683561,4380794,16536848,26995669,101904649,166354808,627964742,1025124517,3869693101,6317101910,23846123348,38927735977,146946433189,239883517772,905524722482",
			"name": "a(n) = 6*a(n-2) + a(n-4), where a(0) = 2, a(1) = 8, a(2) = 13, a(3) = 49.",
			"comment": [
				"The classical Euclidean algorithm iterates the mapping u(x,y) = (y, (x mod y)) until reaching g = GCD(x,y) in a pair ( . , g).  In much the same way, the modified  algorithm (A228247) iterates the mapping v(x,y) = (y, y - (x mod y)).  The accelerated Euclidean algorithm uses w(x,y) = min(u(x,y),v(x,y)).  Let s(x,y) be the number of applications of u, starting with (x,y) -\u003e u(x,y) needed to reach ( . , g), and let u'(x,y) be the number of applications of w to reach ( . , g).  Then u'(x,y) \u003c= u(x,y) for all (x,y).",
				"Starting with a pair (x,y), at each application of w, record 0 if w( . , . ) = u( . , . ) and 1 otherwise.  The trace of (x,y), denoted by trace(x,y), is the resulting 01 word, whose length is the total number of applications of w.",
				"Conjecture:  a(n) is the least positive integer c for which there is a positive integer b for which trace(b,c) consists of the first n letters of 01010101010101..."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A228469/b228469.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,6,0,1)."
			],
			"formula": [
				"G.f.: (x^3 + x^2 + 8*x + 2)/(1 - 6*x^2 - x^4). - _Ralf Stephan_, Aug 24 2013",
				"a(n) = (3+sqrt(10))^(1/4*((-1)^n-1)+1/2*n)*(5/2+sqrt(10)*(4/5-9/20*(-1)^n)-3/2*(-1)^n)+(3-sqrt(10))^(1/4*((-1)^n-1)+1/2*n)*(5/2- sqrt(10)*(4/5-9/20*(-1)^n)-3/2*(-1)^n). - _Paolo P. Lava_, Sep 12 2013"
			],
			"example": [
				"a(3) = 13 because trace(18/13) = 010, and 13 is the least c for which there is a number b such that trace(b/c) = 010.  Successive applications of w are indicated by (18,13)-\u003e(13,5)-\u003e(5,2)-\u003e(2,1).  Whereas w finds GCD in 3 steps, u takes 4 steps, as indicated by (18,3)-\u003e(13,5)-\u003e(5,3)-\u003e(3,2)-\u003e(2,1)."
			],
			"maple": [
				"P:=proc(q) local n; for n from 0 to q do print(evalf((3+sqrt(10))^(1/4*((-1)^n-1)+1/2*n)*(5/2+sqrt(10)*(4/5-9/20*(-1)^n)-3/2*(-1)^n)+(3-sqrt(10))^(1/4*((-1)^n-1)+1/2*n)*(5/2- sqrt(10)*(4/5-9/20*(-1)^n)-3/2*(-1)^n))); od; end: P(100); # _Paolo P. Lava_, Sep 12 2013"
			],
			"mathematica": [
				"c1 = CoefficientList[Series[(2 + 8 x + x^2 + x^3)/(1 - 6 x^2 - x^4), {x, 0, 40}], x]; c2 = CoefficientList[Series[(3 + 11 x + 2 x^3)/(1 - 6 x^2 - x^4), {x, 0, 40}], x]; pairs = Transpose[CoefficientList[Series[{-((3 + 11 x + 2 x^3)/(-1 + 6 x^2 + x^4)), -((2 + 8 x + x^2 + x^3)/(-1 + 6 x^2 + x^4))}, {x, 0, 20}], x]]; t[{x_, y_, _}] := t[{x, y}]; t[{x_, y_}] := Prepend[If[# \u003e y - #, {y - #, 1}, {#, 0}], y] \u0026[Mod[x, y]]; userIn2[{x_, y_}] := Most[NestWhileList[t, {x, y}, (#[[2]] \u003e 0) \u0026]]; Map[Map[#[[3]] \u0026, Rest[userIn2[#]]] \u0026, pairs] (* _Peter J. C. Moses_, Aug 20 2013 *)",
				"LinearRecurrence[{0, 6, 0, 1}, {2, 8, 13, 49}, 30] (* _T. D. Noe_, Aug 23 2013 *)"
			],
			"program": [
				"(PARI) Vec((x^3+x^2+8*x+2)/(1-6*x^2-x^4)+O(x^99)) \\\\ _Charles R Greathouse IV_, Jun 12 2015"
			],
			"xref": [
				"Cf. A179237 (bisection), A228470, A228471, A228487, A228488."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_Clark Kimberling_, Aug 22 2013",
			"references": 5,
			"revision": 32,
			"time": "2015-07-25T11:19:02-04:00",
			"created": "2013-08-23T12:55:05-04:00"
		}
	]
}