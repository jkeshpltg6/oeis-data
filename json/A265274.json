{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A265274",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 265274,
			"data": "5,8,8,7,0,9,5,5,4,3,6,3,6,6,5,4,9,4,2,7,4,0,9,5,7,1,9,1,1,4,0,6,7,9,4,7,9,0,6,0,9,6,8,7,5,0,5,1,5,9,0,4,8,4,8,9,5,5,9,2,1,5,5,2,0,3,9,0,2,8,0,4,1,6,6,5,4,7,5,7,7,1,0,5,0,8,5,8,7,3,2,5,8,3,0,5,3,6,2,9,2,9,1,5,4,1,5,3,1,4,4,4,5,0,9,7,8,8,5,3,9,6,8,8,6,0,4,4,4,4,8,0,5,7,4,4,6,4,8,7,8,3,2,6,1,3,6,1,3,4,4,8,1,6,6,4,0,0,1,3,9,2,7,7,9,7,2,3,0,6,3,8,4,6,2,1,0,0,0,7,5,0,2,0,8,2,3,1,0,7,9,2,0,6,2,6,7,0,5,6",
			"name": "Least real z \u003e 1/2 such that 1/2 = Sum_{n\u003e=1} {n*z} / 2^n, where {x} denotes the fractional part of x.",
			"comment": [
				"This constant is transcendental.",
				"The rational approximation z ~ 50081870146959747811507711449530545577/85070591730234615865843651857942052860 is accurate to many thousands of digits.",
				"This constant is one of 6 solutions to the equation 1/2 = Sum_{n\u003e=1} {n*z}/2^n, where z is in the interval (0,1) - see cross-references for other solutions.",
				"The complement to this constant is given by A265273."
			],
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DevilsStaircase.html\"\u003eDevil's Staircase\u003c/a\u003e.",
				"\u003ca href=\"/index/Tra#transcendental\"\u003eIndex entries for transcendental numbers\u003c/a\u003e"
			],
			"formula": [
				"The constant z satisfies:",
				"(1) 2*z - 1/2 = Sum_{n\u003e=1} [n*z] / 2^n,",
				"(2) 2*z - 1/2 = Sum_{n\u003e=1} 1 / 2^[n/z],",
				"(3) 3/2 - 2*z = Sum_{n\u003e=1} 1 / 2^[n/(1-z)],",
				"(4) 3/2 - 2*z = Sum_{n\u003e=1} [n*(1-z)] / 2^n,",
				"(5) 1/2 = Sum_{n\u003e=1} {n*(1-z)} / 2^n,",
				"where [x] denotes the integer floor function of x."
			],
			"example": [
				"z = 0.58870955436366549427409571911406794790609687505159048489559215520...",
				"where z satisfies",
				"(0) 1/2 = {z}/2 + {2*z}/2^2 + {3*z}/2^3 + {4*z}/2^4 + {5*z}/2^5 +...",
				"(1) 2*z - 1/2 = [z]/2 + [2*z]/2^2 + [3*z]/2^3 + [4*z]/2^4 + [5*z]/2^5 +...",
				"(2) 2*z - 1/2 = 1/2^[1/z] + 1/2^[2/z] + 1/2^[3/z] + 1/2^[4/z] + 1/2^[5/z] +...",
				"The continued fraction of the constant z begins:",
				"[0; 1, 1, 2, 3, 7, 528, 2, 1, 1, 1, 20282564347337181724466999721987, 2, 1, 2, ...]",
				"(the next partial quotient has too many digits to show).",
				"The convergents of the continued fraction of z begin:",
				"[0/1, 1/1, 1/2, 3/5, 10/17, 73/124, 38554/65489, 77181/131102, 115735/196591, 192916/327693, 308651/524284, 6260233768369968476438463931191202453/10633823966279326983230456482242560001, ...]",
				"The partial quotients of the continued fraction of 2*z - 1/2 are as follows:",
				"[0; 1, 2, 10, 4228, 162260514778697453795735997775904, ..., Q_n, ...]",
				"where",
				"Q_1 : 2^0*(2^(1*1) - 1)/(2^1 - 1) = 1;",
				"Q_2 : 2^1*(2^(1*1) - 1)/(2^1 - 1) = 2;",
				"Q_3 : 2^1*(2^(2*2) - 1)/(2^2 - 1) = 10;",
				"Q_4 : 2^2*(2^(3*5) - 1)/(2^5 - 1) = 4228;",
				"Q_5 : 2^5*(2^(7*17) - 1)/(2^17 - 1) = 162260514778697453795735997775904;",
				"Q_6 : 2^17*(2^(528*124) - 1)/(2^124 - 1) ;",
				"Q_7 : 2^124*(2^(2*65489) - 1)/(2^65489 - 1) ;",
				"Q_8 : 2^65489*(2^(1*131102) - 1)/(2^131102 - 1) ;",
				"Q_9 : 2^131102*(2^(1*196591) - 1)/(2^196591 - 1) ;",
				"Q_10 : 2^196591*(2^(1*327693) - 1)/(2^327693 - 1) ;",
				"Q_10 = 2^327693*(2^(20282564347337181724466999721987*524284) - 1)/(2^524284 - 1) ; ...",
				"These partial quotients can be calculated from the simple continued fraction of z and the denominators in the convergents of the continued fraction of z; see the Mathworld link entitled \"Devil's Staircase\" for more details."
			],
			"xref": [
				"Cf. A265271, A265272, A265273, A265275, A265276."
			],
			"keyword": "nonn,cons",
			"offset": "0,1",
			"author": "_Paul D. Hanna_, Dec 12 2015",
			"references": 5,
			"revision": 13,
			"time": "2019-05-14T21:25:08-04:00",
			"created": "2015-12-12T11:25:34-05:00"
		}
	]
}