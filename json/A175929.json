{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A175929",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 175929,
			"data": "1,1,1,1,2,0,2,1,1,3,1,4,2,2,2,4,1,3,1,1,4,3,6,7,6,4,10,6,10,6,10,6,10,4,6,7,6,3,4,1,1,5,6,9,16,12,14,24,20,21,23,28,24,34,20,32,42,29,29,42,32,20,34,24,28,23,21,20,24,14,12,16,9,6,5,1,1,6,10,14,29,26,35,46,55",
			"name": "Triangle T(n,v) read by rows: the number of permutations of [n] with \"entropy\" equal to 2*v.",
			"comment": [
				"Define the \"entropy\" (or variance) of a permutation pi to be Sum_{i=1..n} (pi(i)-i)^2 = A006331(n) - 2*Sum_i i*pi(i), as in A126972.",
				"This characteristic is obviously an even number, 2*v(pi).",
				"Row n of the triangle shows the statistics (frequency distribution) of v for the n! = A000142(n) possible permutations of [n].",
				"T(n,0)=1 arises the identity permutation where v=0.",
				"T(n,1)=n-1 arises from the n-1 different ways of creating an entropy of 2 by swapping a pair of adjacent entries in the identity permutation.",
				"The final 1 in each row arises from the permutation with maximal entropy, that is the permutation with integers reversed relative to the identity permutation.",
				"Row n has 1+A000292(n-1) entries. Row sums are sum_{v=0..A000292(n-1)} T(n,v) = n!.",
				"Removing zeros in A135298 creates a sequence which is similar in the initial terms, because contributions to A135298(n) stem from permutations of some unique [j] if n is not too large, which establishes a 1-to-1 correspondence between the term A006331(n)-2*sum_i i*pi(i) mentioned above and the defining formula in A135298.",
				"The rows of this triangle have a geometric interpretation. Let P_n be the n-dimensional permutohedron, the Voronoi cell of the lattice A_n* (Conway-Sloane, 1993, p. 474), which is a polytope with (n+1)! vertices. Start at any vertex, and count how many vertices there are at squared-distance v from the starting vertex: this is T(n+1,v). For example, in three dimensions the permutohedron is a truncated octahedron, the squared distances from a vertex to all the vertices are (when suitably scaled) 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, and the numbers of vertices at these distances are 1, 3, 1, 4, 2, 2, 2, 4, 1, 3, 1, which is row 4 of the array. See Chap. 21, Section 3.F, op. cit., for further details. - _N. J. A. Sloane_, Oct 13 2015"
			],
			"reference": [
				"J. H. Conway and N. J. A. Sloane, \"Sphere Packings, Lattices and Groups\", Springer-Verlag, 3rd. ed., 1993."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A175929/b175929.txt\"\u003eRows n = 1..14, flattened\u003c/a\u003e",
				"FindStat - Combinatorial Statistic Finder, \u003ca href=\"http://www.findstat.org/StatisticsDatabase/St000055\"\u003eThe rank of the permutation inside the alternating sign matrix lattice\u003c/a\u003e."
			],
			"example": [
				"The triangle starts in row n=1 and column v=0 as follows:",
				"1;",
				"1,1;",
				"1,2,0,2,1;",
				"1,3,1,4,2,2,2,4,1,3,1;",
				"1,4,3,6,7,6,4,10,6,10,6,10,6,10,4,6,7,6,3,4,1;",
				"..."
			],
			"maple": [
				"with(combinat):",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, j), j=ldegree(p)..degree(p)))",
				"        (add(x^add(i*l[i], i=1..n), l=permute(n))):",
				"seq(T(n), n=1..7);  # _Alois P. Heinz_, Aug 28 2014"
			],
			"keyword": "nonn,tabf",
			"offset": "1,5",
			"author": "_Emeric Deutsch_ and _R. J. Mathar_, Oct 22 2010",
			"ext": [
				"Corrected row length term - _R. J. Mathar_, Oct 23 2010"
			],
			"references": 3,
			"revision": 33,
			"time": "2015-10-15T09:34:13-04:00",
			"created": "2010-11-10T03:00:00-05:00"
		}
	]
}