{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A332647",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 332647,
			"data": "3,2,4,11,24,52,115,254,560,1235,2724,6008,13251,29226,64460,142171,313568,691596,1525363,3364294,7420184,16365731,36095756,79611696,175589123,387274002,854159700,1883908523,4155091048,9164341796,20212592115,44580275278,98324892352",
			"name": "a(n) = 2*a(n-1) + a(n-3) with a(0) = 3, a(1) = 2, a(2) = 4.",
			"comment": [
				"a(n) is the number of ways to tile a bracelet of length n with black trominos, and black or white squares."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A332647/b332647.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Greg Dresden and Michael Tulskikh, \u003ca href=\"https://dresden.academic.wlu.edu/files/2021/01/TwoByNWeb.pdf\"\u003eTilings of 2 X n boards with dominos and L-shaped trominos\u003c/a\u003e, Washington \u0026 Lee University (2021).",
				"Helmut Prodinger, \u003ca href=\"https://arxiv.org/abs/2011.04388\"\u003eOn third-order Pell polynomials\u003c/a\u003e, arXiv:2011.04388 [math.NT], 2020.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,0,1)."
			],
			"formula": [
				"a(n) = 2*a(n-1) + a(n-3).",
				"a(n) = w1^n + w2^n + w3^n where w1,w2,w3 are the three roots of x^3-2x^2-1=0.",
				"For n\u003e2, a(n) = round(w1^n) for w1 the single real root of x^3-2x^2-1=0.",
				"G.f.: (3 - 4*x) / (1 - 2*x - x^3). - _Colin Barker_, Feb 18 2020",
				"a(n) = A008998(n) + 2*A008998(n-3) = 3*A008998(n) - 4*A008998(n-1).",
				"a(n) = (5*b(n) - b(n-1) - b(n-2))/2 where b(n) = A052980(n). - _Greg Dresden_, Mar 10 2020",
				"a(n) = A080204(n) + 1. - _Greg Dresden_, May 27 2020"
			],
			"mathematica": [
				"LinearRecurrence[{2, 0, 1}, {3, 2, 4}, 50]"
			],
			"program": [
				"(MAGMA) a:=[3,2,4]; [n le 3 select a[n] else 2*Self(n-1)+Self(n-3):n in [1..33]]; // _Marius A. Burtea_, Feb 18 2020",
				"(PARI) Vec((3 - 4*x) / (1 - 2*x - x^3) + O(x^30)) \\\\ _Colin Barker_, Feb 18 2020",
				"(PARI) polsym(x^3-2*x^2-1, 44) \\\\ _Joerg Arndt_, May 28 2020"
			],
			"xref": [
				"Cf. A008998, A052980. Equals one more than A080204."
			],
			"keyword": "easy,nonn",
			"offset": "0,1",
			"author": "_Greg Dresden_, Feb 18 2020",
			"references": 2,
			"revision": 46,
			"time": "2021-05-06T21:22:37-04:00",
			"created": "2020-02-22T22:29:41-05:00"
		}
	]
}