{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A098156",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 98156,
			"data": "1,2,5,13,32,76,176,400,896,1984,4352,9472,20480,44032,94208,200704,425984,901120,1900544,3997696,8388608,17563648,36700160,76546048,159383552,331350016,687865856,1426063360,2952790016,6106906624",
			"name": "Interleave n+1 and 2n+1 and take binomial transform.",
			"comment": [
				"Binomial transform of A029579.",
				"An elephant sequence, see A175655. For the central square 16 A[5] vectors, with decimal values between 59 and 440, lead to this sequence (without a(1)). For the corner squares these vectors lead to the companion sequence A066373 (with a leading 1 added). - _Johannes W. Meijer_, Aug 15 2010"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A098156/b098156.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"David Anderson, E. S. Egge, M. Riehl, L. Ryan, R. Steinke, Y. Vaughan, \u003ca href=\"http://arxiv.org/abs/1605.06825\"\u003ePattern Avoiding Linear Extensions of Rectangular Posets\u003c/a\u003e, arXiv:1605.06825 [math.CO], 2016.",
				"Colin Defant, \u003ca href=\"https://arxiv.org/abs/1905.02309\"\u003eProofs of Conjectures about Pattern-Avoiding Linear Extensions\u003c/a\u003e, arXiv:1905.02309 [math.CO], 2019.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-4)."
			],
			"formula": [
				"G.f.: (1-2*x+x^2+x^3)/(1-2*x)^2.",
				"a(n) = (2 * 0^n + Sum_{k=0..n} (-1)^(n-k)*k*binomial(n,k) + 2^(n+1) + 3*n*2^(n-1) )/4.",
				"a(n) = Sum_{j=0..n} Sum_{k=0..n} binomial(n, 2*(k-j)).",
				"a(n) = Sum_{k=0..n} Sum_{j=0..k} C(n, 2*j). - _Paul Barry_, Jan 13 2005",
				"a(n) = 2^(n-3)*(3*n+4) for n\u003e=2. - _Philip B. Zhang_, May 25 2016",
				"E.g.f.: (2 + x + (2 + 3*x)*exp(2*x))/4. - _Ilya Gutkovskiy_, May 31 2016"
			],
			"mathematica": [
				"CoefficientList[Series[(1-2x+x^2+x^3)/(1-2x)^2, {x, 0, 40}], x] (* _Vincenzo Librandi_, Jul 21 2013 *)"
			],
			"program": [
				"(PARI) {a(n) = if(n==0,1, if(n==1,2, 2^(n-3)*(3*n+4)))}; \\\\ _G. C. Greubel_, May 08 2019",
				"(MAGMA) [1,2] cat [2^(n-3)*(3*n+4): n in [2..40]]; // _G. C. Greubel_, May 08 2019",
				"(Sage) [1,2]+[2^(n-3)*(3*n+4) for n in (2..40)] # _G. C. Greubel_, May 08 2019",
				"(GAP) Concatenation([1,2], List([2..40], n-\u003e 2^(n-3)*(3*n+4))) # _G. C. Greubel_, May 08 2019"
			],
			"keyword": "easy,nonn",
			"offset": "0,2",
			"author": "_Paul Barry_, Aug 29 2004",
			"references": 5,
			"revision": 39,
			"time": "2019-05-08T04:37:09-04:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}