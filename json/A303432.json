{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A303432",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 303432,
			"data": "0,1,2,3,3,3,2,3,4,5,4,4,2,3,3,4,5,7,5,5,4,4,4,7,5,4,3,2,2,4,5,7,8,7,5,7,5,7,7,7,4,4,2,3,5,7,6,9,7,6,5,6,5,7,7,3,3,3,3,5,7,7,8,7,6,8,5,8,8,8,5,7,4,6,7,9,8,9,7,8",
			"name": "Number of ways to write n as a*(2*a-1) + b*(2*b-1) + 2^c + 2^d, where a,b,c,d are nonnegative integers with a \u003c= b and c \u003c= d.",
			"comment": [
				"Conjecture 1: a(n) \u003e 0 for all n \u003e 1. In other words, any integer n \u003e 1 can be written as the sum of two hexagonal numbers and two powers of 2.",
				"Conjecture 2: Any integer n \u003e 1 can be written as a*(2*a+1) + b*(2*b+1) + 2^c + 2^d with a,b,c,d nonnegative integers.",
				"Conjecture 3: Each integer n \u003e 1 can be written as a*(2*a-1) + b*(2*b+1) + 2^c + 2^d with a,b,c,d nonnegative integers.",
				"All the three conjectures hold for n = 2..2*10^6. Note that either of them is stronger than the conjecture in A303233.",
				"See also A303363, A303389 and  A303401 for similar conjectures."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A303432/b303432.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://math.scichina.com:8081/sciAe/EN/abstract/abstract517007.shtml\"\u003eOn universal sums of polygonal numbers\u003c/a\u003e, Sci. China Math. 58(2015), no. 7, 1367-1396.",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2016.11.008\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, J. Number Theory 175(2017), 167-190.",
				"Zhi-Wei Sun, \u003ca href=\"http://maths.nju.edu.cn/~zwsun/179b.pdf\"\u003eNew conjectures on representations of integers (I)\u003c/a\u003e, Nanjing Univ. J. Math. Biquarterly 34(2017), no. 2, 97-120."
			],
			"example": [
				"a(2) = 1 with 2 = 0*(2*0-1) + 0*(2*0-1) + 2^0 + 2^0.",
				"a(7) = 2 with 7 = 1*(2*1-1) + 1*(2*1-1) + 2^0 + 2^2 = 0*(2*0-1) + 1*(2*1-1) + 2^1 + 2^2."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]];",
				"HexQ[n_]:=HexQ[n]=SQ[8n+1]\u0026\u0026(n==0||Mod[Sqrt[8n+1]+1,4]==0);",
				"f[n_]:=f[n]=FactorInteger[n];",
				"g[n_]:=g[n]=Sum[Boole[Mod[Part[Part[f[n],i],1],4]==3\u0026\u0026Mod[Part[Part[f[n],i],2],2]==1],{i,1,Length[f[n]]}]==0;",
				"QQ[n_]:=QQ[n]=(n==0)||(n\u003e0\u0026\u0026g[n]);",
				"tab={};Do[r=0;Do[If[QQ[4(n-2^j-2^k)+1],Do[If[HexQ[n-2^j-2^k-x(2x-1)],r=r+1],{x,0,(Sqrt[4(n-2^j-2^k)+1]+1)/4}]],{j,0,Log[2,n/2]},{k,j,Log[2,n-2^j]}];tab=Append[tab,r],{n,1,80}];Print[tab]"
			],
			"xref": [
				"Cf. A000079, A000384, A014105, A303233, A303338, A303363, A303389, A303393, A303399, A303401."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Zhi-Wei Sun_, Apr 23 2018",
			"references": 27,
			"revision": 9,
			"time": "2018-04-24T05:51:51-04:00",
			"created": "2018-04-24T05:51:51-04:00"
		}
	]
}