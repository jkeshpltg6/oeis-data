{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A085261",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 85261,
			"data": "1,1,-2,-1,5,3,-9,-5,18,10,-30,-16,53,29,-85,-44,139,73,-215,-110,335,172,-502,-253,755,382,-1104,-550,1614,805,-2312,-1142,3305,1631,-4650,-2277,6525,3193,-9041,-4395,12486,6063,-17070,-8247,23255,11218,-31414,-15090,42289,20285",
			"name": "Expansion of chi(x) / phi(x^2) in powers of x where phi(), chi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A085261/b085261.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"George E. Andrews, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v11i2r1\"\u003eOn a Partition Function of Richard Stanley\u003c/a\u003e, The Electronic Journal of Combinatorics, Volume 11, Issue 2 (2004-6) (The Stanley Festschrift volume), #R1.",
				"M. Ishikawa and J. Zeng, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2007.12.064\"\u003eThe Andrews-Stanley partition function and Al-Salam-Chihara polynomials\u003c/a\u003e, Disc. Math., 309 (2009), 151-175. (See t(n) p. 151. Note that there is a typo in the g.f. for f(n) - see A144558.) [Added by _N. J. A. Sloane_, Jan 25 2009.]",
				"O. P. Lossers, \u003ca href=\"http://www.jstor.org/stable/4145085\"\u003eComparing Odd Parts in Conjugate Partitions: Solution 10969\u003c/a\u003e, Amer. Math. Monthly, 111 (Jun-Jul 2004), pp. 536-539.",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"R. P. Stanley, \u003ca href=\"http://www.jstor.org/stable/3072410\"\u003eProblem 10969\u003c/a\u003e, Amer. Math. Monthly, 109 (2002), 760."
			],
			"formula": [
				"Expansion of psi(x) / f(x^2)^2 in powers of x where psi(), f() are Ramanujan theta functions. - _Michael Somos_, Sep 02 2014",
				"Expansion of q^(1/24) * eta(q^2)^4 * eta(q^8)^2 / (eta(q) * eta(q^4)^6) in powers of q.",
				"Euler transform of period 8 sequence [1, -3, 1, 3, 1, -3, 1, 1, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (2304 t)) = 24^(-1/2) (t/i)^(-1/2) g(t) where q = exp(2 Pi i t) and g() is the g.f. for A246712. - _Michael Somos_, Sep 02 2014",
				"G.f.: Product_{k\u003e0} (1 + x^(2*k - 1)) / ((1 - x^(4*k)) * (1 + x^(4*k - 2))^2)."
			],
			"example": [
				"G.f. = 1 + x - 2*x^2 - x^3 + 5*x^4 + 3*x^5 - 9*x^6 - 5*x^7 + 18*x^8 + 10*x^9 - ...",
				"G.f. = 1/q + q^23 - 2*q^47 - q^71 + 5*q^95 + 3*q^119 - 9*q^143 - 5*q^167 + 18*q^191 + ..."
			],
			"maple": [
				"t1:=mul( (1+q^(2*n-1))/((1-q^(4*n))*(1+q^(4*n-2))^2), n=1..100): t2:=series(t1,q,100): f:=n-\u003ecoeff(t2,q,n); # _N. J. A. Sloane_, Jan 25 2009"
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ -x, x^2] / EllipticTheta[ 3, 0, x^2], {x, 0, n}]; (* _Michael Somos_, Jun 01 2014 *)",
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 2, 0, x^(1/2)] / (2 x^(1/8) QPochhammer[ -x^2]^2), {x, 0, n}]; (* _Michael Somos_, Sep 02 2014 *)"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^4 * eta(x^8 + A)^2 / eta(x + A) / eta(x^4 + A)^6, n))};",
				"(PARI) {a(n) = polcoeff( prod( k=1,( n+1)\\2, 1 + x^(2*k - 1), 1 + x * O(x^n)) / prod(k=1, (n+2)\\4, (1 - x^(4*k)) * (1 + x^(4*k - 2))^2, 1 + x * O(x^n)), n)};"
			],
			"xref": [
				"Cf. A000041, A097566, A190101, A246712."
			],
			"keyword": "sign",
			"offset": "0,3",
			"author": "_Michael Somos_, Jun 23 2003",
			"references": 5,
			"revision": 23,
			"time": "2021-03-12T22:24:42-05:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}