{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005669",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5669,
			"id": "M1193",
			"data": "1,2,4,9,24,30,99,154,189,217,1183,1831,2225,3385,14357,30802,31545,40933,103520,104071,149689,325852,1094421,1319945,2850174,6957876,10539432,10655462,20684332,23163298,64955634,72507380,112228683",
			"name": "Indices of primes where largest gap occurs.",
			"comment": [
				"Conjecture: log a(n) ~ n/2. That is, record prime gaps occur about twice as often as records in an i.i.d. random sequence of comparable length (see arXiv:1709.05508 for a heuristic explanation). - _Alexei Kourbatov_, Mar 28 2018"
			],
			"reference": [
				"H. Riesel, Prime numbers and computer methods for factorization, Progress in Mathematics, Vol. 57, Birkhauser, Boston, 1985, Chap. 4, see pp. 381-384.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"John W. Nicholson, \u003ca href=\"/A005669/b005669.txt\"\u003eTable of n, a(n) for n = 1..80\u003c/a\u003e [Added data from Thomas R. Nicely site. - _John W. Nicholson_, Oct 27 2021. First 77 terms from Charles R Greathouse IV]",
				"Jens Kruse Andersen, \u003ca href=\"http://primerecords.dk/primegaps/gaps20.htm\"\u003eThe Top-20 Prime Gaps\u003c/a\u003e",
				"Jens Kruse Andersen, \u003ca href=\"http://primerecords.dk/primegaps/megagap2.htm\"\u003eNew record prime gap\u003c/a\u003e",
				"Jens Kruse Andersen, \u003ca href=\"http://primerecords.dk/primegaps/maximal.htm\"\u003eMaximal gaps\u003c/a\u003e",
				"R. K. Guy, \u003ca href=\"/A005667/a005667.pdf\"\u003eLetter to N. J. A. Sloane, 1987\u003c/a\u003e",
				"Alexei Kourbatov, \u003ca href=\"http://arxiv.org/abs/1309.4053\"\u003eTables of record gaps between prime constellations\u003c/a\u003e, arXiv preprint arXiv:1309.4053 [math.NT], 2013.",
				"Alexei Kourbatov, \u003ca href=\"https://arxiv.org/abs/1709.05508\"\u003eOn the nth record gap between primes in an arithmetic progression\u003c/a\u003e, arXiv:1709.05508 [math.NT], 2017; \u003ca href=\"https://doi.org/10.12988/imf.2018.712103\"\u003eInt. Math. Forum, 13 (2018), 65-78\u003c/a\u003e.",
				"Alexei Kourbatov and Marek Wolf, \u003ca href=\"http://arxiv.org/abs/1901.03785\"\u003ePredicting maximal gaps in sets of primes\u003c/a\u003e, arXiv preprint arXiv:1901.03785 [math.NT], 2019.",
				"Thomas R. Nicely, \u003ca href=\"https://faculty.lynchburg.edu/~nicely/gaps/gaplist.html\"\u003eFirst occurrence prime gaps\u003c/a\u003e [For local copy see A000101]",
				"Matt Visser, \u003ca href=\"https://arxiv.org/abs/1904.00499\"\u003eVerifying the Firoozbakht, Nicholson, and Farhadian conjectures up to the 81st maximal prime gap\u003c/a\u003e, arXiv:1904.00499 [math.NT], 2019.",
				"R. G. Wilson, V, \u003ca href=\"/A005250/a005250.pdf\"\u003eNotes (no date)\u003c/a\u003e",
				"J. Young and A. Potler, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1989-0947470-1\"\u003eFirst occurrence prime gaps\u003c/a\u003e, Math. Comp., 52 (1989), 221-224.",
				"\u003ca href=\"/index/Pri#gaps\"\u003eIndex entries for primes, gaps between\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A000720(A002386(n)).",
				"a(n) = A107578(n) - 1. - _Jens Kruse Andersen_, Oct 19 2010"
			],
			"mathematica": [
				"f[n_] := Block[{d, i, m = 0}, Reap@ For[i = 1, i \u003c= n, i++, d = Prime[i + 1] - Prime@ i; If[d \u003e m, m = d; Sow@ i, False]] // Flatten // Rest]; f@ 1000000 (* _Michael De Vlieger_, Mar 24 2015 *)"
			],
			"xref": [
				"Cf. A000101, A002386, A005250."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"references": 29,
			"revision": 69,
			"time": "2021-10-28T12:43:15-04:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}