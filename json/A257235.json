{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A257235",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 257235,
			"data": "1,6,3,4,3,6,5,2,9,3,0,1,3,5,4,3,3,2,3,3,6,8,2,8,4,4,5,6,9,7,8,2,5,2,2,1,0,3,3,7,2,0,4,7,0,3,7,5,4,0,4,7,2,8,1,7,6,9,5,7,4,6,1,2,9,6,2,2,3,1,7,7,9,3,3,3,5,7,3,4,8,6,1,2,0,4,6,1,2,4,9,3,7,9,0,8,8",
			"name": "Decimal expansion of the real root of x^3 + x - 6.",
			"comment": [
				"This appears in the solution of the first of thirty problems posed by Antonio Maria Fiore in the year 1535 to Niccolò Tartaglia. See the Alten et al. reference, p. 272.",
				"See also the Fauvel and Gray reference, p. 254. Note that there this first problem is translated as \"Find me a number such that when its cube root is added to it, the result is six, that is 6\", and this is explained by [This is equivalent to the equation x^3 + x = 6.]  In the Alten et al. reference this 'cube root' is given as 'Kubus'. In the cube root case (z-6)^3 + z = 0 would lead with y = z - 6, to y^3 + y + 6 = 0, hence the real solution would be y = - x1, and z = 6 - x1 = 4.36563470698645... So one should consult the original in order to see which translation is correct. My guess is that Fiore used only positive numbers a and b in the cubic x^3  + a*x = b (usually written this way to have only positive coefficients).",
				"In Tartaglia's account on his contest with Fiore given in Quesiti et Inventioni Diverse (1546) under XXV in discussion with Zuanne di Tonini da Coi one finds, for example, his third problem posed to Fiore: find me a number which when added to the fourfold of its cube root gives 13. This is z + 4*z^(1/3) = 13, or x^3 + 4*x = 13, where z = x^3. Therefore it seems that the above Fauvel and Gray version of the first problem posed by Fiore to Tartaglia is correct, and z + z^(1/3) = 6 has the (real) solution z = x^3 of x^3 + x = 6, namely z = x1^3 = 6 - x1 = 4.36563470698645... See the Katscher reference (in German), p. 15.  - _Wolfdieter Lang_, May 21 2015"
			],
			"reference": [
				"H.-W. Alten et al., 4000 Jahre Algebra, 2. Auflage, Springer, 2014, p. 272.",
				"John Fauvel and Jeremy Gray (eds.), The History of Mathematics: A Reader, Macmillan Press, The Open University, 1988.",
				"Friedrich Katscher, Die Kubischen Gleichungen bei Nicolo Tartaglia, Verlag der Österreichischen Akademie der Wissenschaften, 2001, Wien, Aufgabe XXV, pp. 13-16."
			],
			"link": [
				"MacTutor History of Mathematics, \u003ca href=\"http://www-history.mcs.st-andrews.ac.uk/Biographies/Tartaglia.html\"\u003eNicolo Tartaglia\u003c/a\u003e.",
				"Niccolò Tartaglia, \u003ca href=\"https://play.google.com/books/reader?id=dRtaAAAAcAAJ\u0026amp;printsec=frontcover\u0026amp;output=reader\u0026amp;hl=de\u0026amp;pg=GBS.PT124\"\u003eQuesiti et inventioni diverse\u003c/a\u003e, Venetia, Venturino Ruffinelli, 1546. (Page 195 of the Google scan.)"
			],
			"formula": [
				"The real solution x1 to x^3 + x - 6 = 0 is",
				"x1 = (1/3)*((3*(27 + 2*sqrt(183)))^(1/3) - (3*(-27 + 2*sqrt(183)))^(1/3)).",
				"The two complex solutions are a + b*i and a - b*i, with a = -x1/2 and b = sqrt(3)*y1/2 where y1 = (1/3)*((3*(27 + 2*sqrt(183)))^(1/3) + (3*(-27 + 2*sqrt(183)))^(1/3)).",
				"x1 = 6^(1/3)*Sum(k\u003e=0, Gamma((2*k-1)/3)*Gamma((k+1)/3)*sin((k-2)*Pi/3)*6^(-2*k/3)/(3*Pi*k!)). - _Robert Israel_, May 21 2015"
			],
			"example": [
				"x1 = 1.63436529301354332336828445697825221...",
				"y1=  2.00112049720664713840588222759158154..."
			],
			"maple": [
				"a:=(27+2*sqrt(183))^(1/3): b:=3^(1/3): a/b^2-1/(b*a): evalf(%,99); # _Peter Luschny_, May 21 2015"
			],
			"mathematica": [
				"RealDigits[N[Solve[x^3 + x - 6==0, x][[1]][[1, 2]], 120]][[1]] (* _Vincenzo Librandi_, May 09 2015 *)"
			],
			"program": [
				"(PARI) polrootsreal(x^3 + x - 6)[1] \\\\ _Charles R Greathouse IV_, May 11 2015"
			],
			"xref": [
				"Cf. A257236, A257237."
			],
			"keyword": "nonn,easy,cons",
			"offset": "1,2",
			"author": "_Wolfdieter Lang_, May 08 2015",
			"references": 4,
			"revision": 39,
			"time": "2015-09-18T08:20:22-04:00",
			"created": "2015-05-11T04:06:53-04:00"
		}
	]
}