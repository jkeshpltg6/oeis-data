{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A326042",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 326042,
			"data": "1,1,2,11,1,2,2,3,29,1,5,22,4,2,2,49,3,29,2,11,4,5,6,6,34,4,22,22,1,2,17,55,10,3,2,319,10,2,8,3,7,4,2,55,29,6,8,98,85,34,6,44,6,22,5,6,4,1,29,22,13,17,58,1091,4,10,4,33,12,2,31,87,3,10,68,22,10,8,10,49,469,7,12,44,3,2,2,15,25,29,8,66,34,8",
			"name": "a(n) = A064989(sigma(A003961(n))), where A003961 shifts the prime factorization of n one step towards larger primes, and A064989 shifts it back towards smaller primes.",
			"comment": [
				"For any other number n than those in A326182 we have a(n) \u003c A003961(n).",
				"Fixed points k (for which a(k) = k) satisfy A003973(k) = 2^e * A003961(k) for some exponent e \u003e= 0. Applying A003961 to such numbers gives the odd terms in A336702, of which there are likely to be just a single instance, its initial 1. (Clarified Nov 07 2021).",
				"Conjecture: There are no other fixed points than a(1) = 1. If true, then there are no odd perfect numbers. This condition is equivalent to the condition that if A161942 has no fixed points larger than one, then there are no odd perfect numbers. This follows as whenever k is a fixed point, that is, a(k) = k, then we should also have A003961(a(k)) = A003961(A064989(sigma(A003961(k)))) = A161942(A003961(k)) = A003961(k). Note that A003961 is an injective and surjective mapping from natural numbers to odd numbers, A064989 is its (left) inverse, and composition A003961(A064989(n)) is equivalent to A000265(n).",
				"From _Antti Karttunen_, Aug 05 2020: (Start)",
				"For any hypothetical odd perfect number x, we would have A003973(k) = 2 * A003961(k), with k = A064989(x) and x = A003961(k). Thus  we would have a(k) = A064989(sigma(A003961(k)) = A064989(sigma(x)) = A064989(2*x) = A064989(x) = k. On the other hand, A003973(k) = sigma(A003961(k)) \u003c A003961(A003961(k)) [see A286385 for the reason why], so a necessary condition for this is that x should be one of the terms of A246282. (Clarified Dec 01 2020).",
				"(End)"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A326042/b326042.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#prime_indices\"\u003eIndex entries for sequences computed from indices in prime factorization\u003c/a\u003e",
				"\u003ca href=\"/index/Si#SIGMAN\"\u003eIndex entries for sequences related to sigma(n)\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A064989(A003973(n)) = A064989(sigma(A003961(n))).",
				"For k in A000037, a(k) = A064989(A003973(k)/2) = A064989((1/2)*sigma(A003961(k))).",
				"Multiplicative with a(p^e) = A064989((q^(e+1)-1)/(q-1)), where q = nextPrime(p). - _Antti Karttunen_, Nov 05 2021"
			],
			"mathematica": [
				"f1[p_, e_] := NextPrime[p]^e; a1[1] = 1; a1[n_] := Times @@ f1 @@@ FactorInteger[n]; f2[2, e_] := 1; f2[p_, e_] := NextPrime[p, -1]^e; a2[1] = 1; a2[n_] := Times @@ f2 @@@ FactorInteger[n]; a[n_] := a2[DivisorSigma[1, a1[n]]]; Array[a, 100] (* _Amiram Eldar_, Nov 07 2021 *)"
			],
			"program": [
				"(PARI)",
				"A003961(n) = my(f = factor(n)); for (i=1, #f~, f[i, 1] = nextprime(f[i, 1]+1)); factorback(f); \\\\ From A003961",
				"A064989(n) = {my(f); f = factor(n); if((n\u003e1 \u0026\u0026 f[1,1]==2), f[1,2] = 0); for (i=1, #f~, f[i,1] = precprime(f[i,1]-1)); factorback(f)};",
				"A326042(n) = A064989(sigma(A003961(n)));"
			],
			"xref": [
				"Cf. A000037, A000203, A000265, A000593, A003961, A003973, A064989, A161942, A162284, A246282, A286385, A326041, A326182, A336702.",
				"Cf. A348736 [n - a(n)], A348738 [a(n) \u003c n], A348739 [a(n) \u003e n], A348750 [= A064989(a(A003961(n)))], A348940 [gcd(n,a(n))], A348941, A348942.",
				"Cf. also A332223 for another conjugation of sigma."
			],
			"keyword": "nonn,mult",
			"offset": "1,3",
			"author": "_Antti Karttunen_, Jun 16 2019",
			"ext": [
				"Keyword:mult added by _Antti Karttunen_, Nov 05 2021"
			],
			"references": 34,
			"revision": 59,
			"time": "2021-11-08T08:17:53-05:00",
			"created": "2019-06-17T08:49:02-04:00"
		}
	]
}