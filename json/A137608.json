{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A137608",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 137608,
			"data": "1,-1,1,-1,0,-1,2,-1,1,0,0,-1,2,-2,0,-1,0,-1,2,0,2,0,0,-1,1,-2,1,-2,0,0,2,-1,0,0,0,-1,2,-2,2,0,0,-2,2,0,0,0,0,-1,3,-1,0,-2,0,-1,0,-2,2,0,0,0,2,-2,2,-1,0,0,2,0,0,0,0,-1,2,-2,1,-2,0,-2,2,0,1,0,0,-2,0,-2,0,0,0,0,4,0,2,0,0,-1,2,-3,0,-1,0,0,2,-2,0",
			"name": "Expansion of (1 - psi(-q)^3 / psi(-q^3)) / 3 in powers of q where psi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Cubic AGM theta functions: a(q) (see A004016), b(q) (A005928), c(q) (A005882)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A137608/b137608.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of (1 - b(q^2)^2 / b(-q) ) / 3 in powers of q where b() is a cubic AGM function.",
				"Moebius transform is period 12 sequence [ 1, -2, 0, 0, -1, 0, 1, 0, 0, 2, -1, 0, ...].",
				"a(n) is multiplicative with a(2^e) = -1 unless e=0, a(3^e) = 1, a(p^e) = e + 1 if p == 1 (mod 6), a(p^e) = (1 + (-1)^e) / 2 if p == 5 (mod 6).",
				"G.f.: Sum_{k\u003e0} (-1)^k * (x^k + x^(3*k)) / (1 + x^k + x^(2*k)).",
				"G.f.: ( Sum_{k\u003e0} x^(6*k-5) / ( 1 + x^(6*k-5) ) - x^(6*k-1) / ( 1 + x^(6*k-1) )).",
				"a(n) = -(-1)^n * A035178(n). -3 * a(n) = A132973(n) unless n = 0.",
				"a(2*n) = -A035178(n). a(2*n + 1) = A033762(n). a(3*n) = a(n). a(3*n + 1) = A227696(n).",
				"a(4*n + 1) + A112604(n). a(4*n + 3) = A112605(n). a(6*n + 1) = A097195(n). a(6*n + 5) = 0.",
				"a(8*n + 1) = A112606(n). a(8*n + 3) = A112608(n). a(8*n + 5) = 2 * A112607(n-1). a(8*n + 7) = 2 * A112609(n).",
				"a(12*n + 1) = A123884(n). a(12*n + 7) = 2 * A121361(n).",
				"a(24*n + 1) = A131961(n). a(24*n + 7) = 2 * A131962(n). a(24*n + 13) = 2 * A131963(n). a(24*n + 19) = 2 * A131964(n)."
			],
			"example": [
				"G.f. = q - q^2 + q^3 - q^4 - q^6 + 2*q^7 - q^8 + q^9 - q^12 + 2*q^13 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 1, 0, -(-1)^n DivisorSum[n, KroneckerSymbol[ -12, #] \u0026]]; (* _Michael Somos_, May 06 2015 *)",
				"a[ n_] := SeriesCoefficient[ (4 + EllipticTheta[ 2, Pi/4, q^(1/2)]^3 / EllipticTheta[ 2, Pi/4, q^(3/2)]) / 6, {q, 0, n}]; (* _Michael Somos_, May 06 2015 *)",
				"a[ n_] := If[ n \u003c 1, 0, DivisorSum[ n, {1, -2, 0, 0, -1, 0, 1, 0, 0, 2, -1, 0}[[Mod[#, 12, 1]]] \u0026]]; (* _Michael Somos_, May 07 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, 0, -(-1)^n * sumdiv(n, d, kronecker(-12, d)))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( (1 - eta(x + A)^3 * eta(x^4 + A)^3 * eta(x^6 + A) / (eta(x^2 + A)^3 * eta(x^3 + A) * eta(x^12 + A))) / 3, n))}; /* _Michael Somos_, May 06 2015 */"
			],
			"xref": [
				"Cf. A033762, A097195, A112604, A112605, A112606, A112607, A112608, A112609, A121361, A123884, A131961, A131962, A131963, A131964, A132973, A227696.",
				"Cf. A035178, A093829, A113447 are same up to sign."
			],
			"keyword": "sign,mult",
			"offset": "1,7",
			"author": "_Michael Somos_, Jan 29 2008",
			"references": 3,
			"revision": 13,
			"time": "2021-03-12T22:24:45-05:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}