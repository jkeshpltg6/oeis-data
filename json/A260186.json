{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A260186",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 260186,
			"data": "1,-4,12,-32,80,-184,400,-832,1664,-3220,6056,-11104,19904,-34968,60320,-102336,171008,-281800,458428,-736928,1171552,-1843328,2872368,-4435392,6790656,-10313180,15544136,-23259968,34568576,-51042392,74901984,-109268224,158507008",
			"name": "Expansion of (phi(q^4) / phi(q))^2 in powers of q where phi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A260186/b260186.txt\"\u003eTable of n, a(n) for n = 0..2500\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of (eta(q) / eta(q^16))^4 * (eta(q^8) / eta(q^2))^10 in powers of q.",
				"Euler transform of period 16 sequence [ -4, 6, -4, 6, -4, 6, -4, -4, -4, 6, -4, 6, -4, 6, -4, 0, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (16 t)) = (1/4) g(t) where q = exp(2 Pi i t) and g() is the g.f. for A216060.",
				"Convolution inverse is A216060. Convolution square of A112128.",
				"a(n) ~ (-1)^n * exp(Pi*sqrt(2*n)) / (32 * 2^(1/4) * n^(3/4)). - _Vaclav Kotesovec_, Nov 15 2017"
			],
			"example": [
				"G.f. = 1 - 4*x + 12*x^2 - 32*x^3 + 80*x^4 - 184*x^5 + 400*x^6 - 832*x^7 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (EllipticTheta[ 3, 0, q^4] / EllipticTheta[ 3, 0, q])^2, {q, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( (eta(x + A) / eta(x^16 + A))^4 * (eta(x^8 + A) / eta(x^2 + A))^10, n))};"
			],
			"xref": [
				"Cf. A112128, A216060."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Jul 17 2015",
			"references": 3,
			"revision": 13,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2015-07-17T23:21:25-04:00"
		}
	]
}