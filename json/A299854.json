{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A299854",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 299854,
			"data": "36,-144,864,-6048,46080,-370656,3096576,-26604864,233570304,-2086063200,18893242368,-173111152320,1601754365952,-14945262816960,140461536706560,-1328495714939520,12635295568625664,-120770748612067680,1159474181591531520,-11176086736640548800,108114032779214192640,-1049288769526156568640,10214201953998140866560,-99701511082612622322048",
			"name": "G.f. S(x) satisfies C(x)^(1/2) - S(x)^(1/2) = 1 such that C'(x)*S(x)^(1/2) = S'(x)*C(x)^(1/2) = 72*x.",
			"comment": [
				"a(n) = -(-1)^n * 36 * A214377(n-2) for n\u003e=2."
			],
			"formula": [
				"The functions C = C(x) and S = S(x) satisfy:",
				"(1a) sqrt(C) - sqrt(S) = 1.",
				"(1b) C'*sqrt(S) = S'*sqrt(C) = 72*x.",
				"(1c) C' = 72*x/sqrt(S).",
				"(1d) S' = 72*x/sqrt(C).",
				"Integrals.",
				"(2a) C = 1 + Integral 72*x/sqrt(S) dx.",
				"(2b) S = Integral 72*x/sqrt(C) dx.",
				"(2c) C = 1 + Integral S'*sqrt(C/S) dx.",
				"(2d) S = Integral C'*sqrt(S/C) dx.",
				"Exponentials.",
				"(3a) sqrt(C) = exp( Integral 36*x/(C*sqrt(S)) dx ).",
				"(3b) sqrt(S) = 6*x*exp( Integral 36*x/(S*sqrt(C)) - 1/x dx ).",
				"(3c) C - S = exp( Integral 72*x/(C*sqrt(S) + S*sqrt(C)) dx ).",
				"(3d) C - S = exp( Integral C'*S'/(C*S' + S*C') dx).",
				"Functional equations.",
				"(4a) C = 1/3 - 36*x^2 + (2/3)*C^(3/2).",
				"(4b) S = 36*x^2 - (2/3)*S^(3/2).",
				"Explicit solutions.",
				"(5a) C(x) = 1 + Sum_{n\u003e=1} 2*(-4)^n*binomial(3*n/2,n)/((3*n-2)*(3*n-4)) * x^n.",
				"(5b) S(x) = 36*x^2 + Sum_{n\u003e=3} 18*(-4)^n*(3*n-3)*binomial(3*n/2-2,n)/((3*n-4)*(3*n-6)) * x^n.",
				"(5c) sqrt(C(x)) = 1 + Sum_{n\u003e=1} -(-4)^n * binomial(3*n/2,n)/(3*n-2) * x^n.",
				"Formulas for terms.",
				"a(n) = 18*(-4)^n * (3*n-3) * binomial(3*n/2-2,n) / ((3*n-4)*(3*n-6)) for n\u003e=3, with a(2) = 36."
			],
			"example": [
				"G.f.: S(x) = 36*x^2 - 144*x^3 + 864*x^4 - 6048*x^5 + 46080*x^6 - 370656*x^7 + 3096576*x^8 - 26604864*x^9 + 233570304*x^10 + ...",
				"RELATED SERIES.",
				"C(x) = 1 + 12*x + 12*x^2 - 24*x^3 + 96*x^4 - 504*x^5 + 3072*x^6 - 20592*x^7 + 147456*x^8 - 1108536*x^9 + 8650752*x^10 + ...",
				"C(x)^(1/2) = 1 + 6*x - 12*x^2 + 60*x^3 - 384*x^4 + 2772*x^5 - 21504*x^6 + 175032*x^7 - 1474560*x^8 + 12748164*x^9 - 112459776*x^10 + ...",
				"sqrt(S(x)) = 6*x - 12*x^2 + 60*x^3 - 384*x^4 + 2772*x^5 - 21504*x^6 + 175032*x^7 - 1474560*x^8 + 12748164*x^9 - 112459776*x^10 + ...",
				"where C(x)^(1/2) - S(x)^(1/2) = 1",
				"and C'*sqrt(S) = S'*sqrt(C) = 72*x."
			],
			"program": [
				"(PARI) {a(n) = my(C=1, S=x^2); for(i=0, n, C = 1 + intformal( 72*x/sqrt(S +x^3*O(x^n)) ); S = intformal( 72*x/sqrt(C) ) ); polcoeff(S, n)}",
				"for(n=2,30,print1(a(n),\", \"))",
				"(PARI) {a(n) = if(n\u003c2,0,n==2,36, 18*(-4)^n * (3*n-3) * binomial(3*n/2-2,n) / ((3*n-4)*(3*n-6)) )}",
				"for(n=2,30,print1(a(n),\", \"))"
			],
			"xref": [
				"Cf. A299853, A299855, A214377."
			],
			"keyword": "sign",
			"offset": "2,1",
			"author": "_Paul D. Hanna_, Feb 20 2018",
			"references": 2,
			"revision": 7,
			"time": "2018-05-05T20:55:39-04:00",
			"created": "2018-02-20T19:27:05-05:00"
		}
	]
}