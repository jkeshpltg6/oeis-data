{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A062355",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 62355,
			"data": "1,2,4,6,8,8,12,16,18,16,20,24,24,24,32,40,32,36,36,48,48,40,44,64,60,48,72,72,56,64,60,96,80,64,96,108,72,72,96,128,80,96,84,120,144,88,92,160,126,120,128,144,104,144,160,192,144,112,116,192,120,120,216,224",
			"name": "a(n) = d(n) * phi(n), where d(n) is the number of divisors function.",
			"comment": [
				"a(n) = sum of gcd(k-1,n) for 1 \u003c= k \u003c= n and gcd(k,n)=1 (Menon's identity).",
				"For n = 2^(4*k^2 - 1), k \u003e= 1, the terms of the sequence are square and for n = 2^((3*k + 2)^3 - 1), k \u003e= 1, the terms of the sequence are cubes. - _Marius A. Burtea_, Nov 14 2019",
				"Sum_{k\u003e=1} 1/a(k) diverges. - _Vaclav Kotesovec_, Sep 20 2020"
			],
			"reference": [
				"D. M. Burton, Elementary Number Theory, Allyn and Bacon Inc., Boston MA, 1976, Prob. 7.2 12, p. 141.",
				"P. K. Menon, On the sum gcd(a-1,n) [(a,n)=1], J. Indian Math. Soc. (N.S.), 29 (1965), 155-163.",
				"József Sándor, On Dedekind's arithmetical function, Seminarul de teoria structurilor (in Romanian), No. 51, Univ. Timișoara, 1988, pp. 1-15. See p. 11.",
				"József Sándor, Some diophantine equations for particular arithmetic functions (in Romanian), Seminarul de teoria structurilor, No. 53, Univ. Timișoara, 1989, pp. 1-10. See p. 8."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A062355/b062355.txt\"\u003eTable of n, a(n) for n = 1..65537\u003c/a\u003e (terms 1..1000 from Harry J. Smith)",
				"Pentti Haukkanen and László Tóth, \u003ca href=\"https://arxiv.org/abs/1911.05411\"\u003eMenon-type identities again: Note on a paper by Li, Kim and Qiao\u003c/a\u003e, arXiv:1911.05411 [math.NT], 2019.",
				"Vaclav Kotesovec, \u003ca href=\"/A062355/a062355.jpg\"\u003eGraph - the asymptotic ratio\u003c/a\u003e (250000000 terms)",
				"R. J. Mathar, \u003ca href=\"http://arxiv.org/abs/1106.4038\"\u003eSurvey of Dirichlet series of multiplicative arithmetic functions\u003c/a\u003e, arXiv:1106.4038 [math.NT], 2011-2012, Section 3.15.",
				"R. Sivaramakrishnan, \u003ca href=\"http://www.jstor.org/stable/2315622\"\u003eProblem E 1962\u003c/a\u003e, Elementary Problems, The American Mathematical Monthly, Vol. 74, No. 2 (1967), p. 198; \u003ca href=\"http://www.jstor.org/stable/2314747\"\u003eSolution\u003c/a\u003e, ibid., Vol. 75, No. 5 (1968), p. 550.",
				"Marius Tarnauceanu, \u003ca href=\"http://arxiv.org/abs/1109.2198\"\u003eA generalization of the Menon's identity\u003c/a\u003e, arXiv:1109.2198 [math.GR], 2011-2012.",
				"Laszlo Toth, \u003ca href=\"http://www.seminariomatematico.polito.it/rendiconti/69-1/97.pdf\"\u003eMenon's identity and arithmetical sums representing functions of several variables\u003c/a\u003e, Rend. Sem. Mat. Univ. Politec. Torino, 69 (2011), 97-110.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Arithmetic_function#Menon\u0026#39;s_identity\"\u003eArithmetic function (Menon's identity)\u003c/a\u003e."
			],
			"formula": [
				"Dirichlet convolution of A047994 and A000010. - _R. J. Mathar_, Apr 15 2011",
				"a(n) = A000005(n)*A000010(n). Multiplicative with a(p^e) = (e+1)*(p-1)*p^(e-1). - _R. J. Mathar_, Jun 23 2018",
				"a(n) = A173557(n) * A318519(n) = A003557(n) * A304408(n). - _Antti Karttunen_, Sep 16 2018 \u0026 Sep 20 2019",
				"From _Vaclav Kotesovec_, Jun 15 2020: (Start)",
				"Let f(s) = Product_{primes p} (1 - 2*p^(-s) + p^(1-2*s)).",
				"Dirichlet g.f.: zeta(s-1)^2 * f(s).",
				"Sum_{k=1..n} a(k) ~ n^2 * (f(2)*(log(n)/2 + gamma - 1/4) + f'(2)/2), where f(2) = A065464 = Product_{primes p} (1 - 2/p^2 + 1/p^3) = 0.42824950567709444...,",
				"f'(2) = 2 * A065464 * A335707 = f(2) * Sum_{primes p} 2*log(p) / (p^2 + p - 1) = 0.35866545223424232469545420783620795... and gamma is the Euler-Mascheroni constant A001620. (End)",
				"From _Amiram Eldar_, Mar 02 2021: (Start)",
				"a(n) \u003e= n (Sivaramakrishnan, 1967).",
				"a(n) \u003e= sigma(n), for odd n (Sándor, 1988).",
				"a(n) \u003e= phi(n) + n - 1 (Sándor, 1989) (End)",
				"From _Richard L. Ollerton_, May 07 2021: (Start)",
				"a(n) = Sum_{k=1..n} uphi(gcd(n,k)), where uphi(n) = A047994(n).",
				"a(n) = Sum_{k=1..n} uphi(n/gcd(n,k))*phi(gcd(n,k))/phi(n/gcd(n,k)). (End)"
			],
			"maple": [
				"seq(tau(n)*phi(n), n=1..64); # _Zerinvary Lajos_, Jan 22 2007"
			],
			"mathematica": [
				"Table[EulerPhi[n] DivisorSigma[0, n], {n, 80}] (* _Carl Najafi_, Aug 16 2011 *)",
				"f[p_, e_] := (e+1)*(p-1)*p^(e-1); a[1] = 1; a[n_] := Times @@ f @@@ FactorInteger[n]; Array[a, 100] (* _Amiram Eldar_, Sep 21 2020 *)"
			],
			"program": [
				"(PARI) a(n)=numdiv(n)*eulerphi(n); vector(150,n,a(n))",
				"(PARI) { for (n=1, 1000, write(\"b062355.txt\", n, \" \", numdiv(n)*eulerphi(n)) ) } \\\\ _Harry J. Smith_, Aug 05 2009",
				"(PARI) for(n=1, 100, print1(direuler(p=2, n, (1 - 2*X + p*X^2)/(1 - p*X)^2)[n], \", \")) \\\\ _Vaclav Kotesovec_, Jun 15 2020",
				"(MAGMA) [NumberOfDivisors(n)*EulerPhi(n):n in [1..65]]; // _Marius A. Burtea_, Nov 14 2019"
			],
			"xref": [
				"Cf. A003557, A173557, A061468, A062816, A079535, A062949 (inverse Mobius transform), A304408, A318519, A327169 (number of times n occurs in this sequence).",
				"Cf. A062354, A064840."
			],
			"keyword": "easy,nonn,mult",
			"offset": "1,2",
			"author": "_Jason Earls_, Jul 06 2001",
			"references": 25,
			"revision": 107,
			"time": "2021-05-07T06:49:34-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}