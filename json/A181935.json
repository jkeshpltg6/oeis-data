{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A181935",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 181935,
			"data": "1,1,1,2,2,1,1,3,3,1,2,2,2,1,1,4,4,1,1,2,2,2,1,3,3,1,2,2,2,1,1,5,5,1,1,2,2,2,1,3,3,1,3,2,2,2,1,4,4,1,1,2,2,2,2,3,3,1,2,2,2,1,1,6,6,1,1,2,2,2,1,3,3,2,2,2,2,1,1,4,4,1,2,2,2,3",
			"name": "Curling number of binary expansion of n.",
			"comment": [
				"Given a string S, write it as S = XYY...Y = XY^k, where X may be empty, and k is as large as possible; then k is the curling number of S.",
				"A212439(n) = 2 * n + a(n) mod 2. - _Reinhard Zumkeller_, May 17 2012"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A181935/b181935.txt\"\u003eTable of n, a(n) for n = 0..8191\u003c/a\u003e",
				"Benjamin Chaffin and N. J. A. Sloane, \u003ca href=\"http://neilsloane.com/doc/CNC.pdf\"\u003eThe Curling Number Conjecture\u003c/a\u003e, preprint."
			],
			"example": [
				"731 = 1011011011 in binary, which we could write as XY^2 with X = 10110110 and Y = 1, or as XY^3 with X = 1 and Y = 011. The latter is better, giving k = 3, so a(713) = 3."
			],
			"program": [
				"(Haskell)",
				"import Data.List (unfoldr, inits, tails, stripPrefix)",
				"import Data.Maybe (fromJust)",
				"a181935 0 = 1",
				"a181935 n = curling $ unfoldr",
				"   (\\x -\u003e if x == 0 then Nothing else Just $ swap $ divMod x 2) n where",
				"   curling zs = maximum $ zipWith (\\xs ys -\u003e strip 1 xs ys)",
				"                          (tail $ inits zs) (tail $ tails zs) where",
				"      strip i us vs | vs' == Nothing = i",
				"                    | otherwise      = strip (i + 1) us $ fromJust vs'",
				"                    where vs' = stripPrefix us vs",
				"-- _Reinhard Zumkeller_, May 16 2012"
			],
			"xref": [
				"Cf. A212412 (parity), A212440, A212441, A007088, A090822, A224764/A224765 (fractional curling number)."
			],
			"keyword": "nonn,base",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_, Apr 02 2012",
			"references": 8,
			"revision": 22,
			"time": "2013-07-13T12:03:44-04:00",
			"created": "2012-04-02T23:19:34-04:00"
		}
	]
}