{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A052841",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 52841,
			"data": "1,0,2,6,38,270,2342,23646,272918,3543630,51123782,811316286,14045783798,263429174190,5320671485222,115141595488926,2657827340990678,65185383514567950,1692767331628422662,46400793659664205566,1338843898122192101558,40562412499252036940910",
			"name": "E.g.f.: 1/(exp(x)*(2-exp(x))).",
			"comment": [
				"From _Michael Somos_, Mar 04 2004: (Start)",
				"Stirling transform of A005359(n)=[0,2,0,24,0,720,...] is a(n)=[0,2,6,38,270,...].",
				"Stirling transform of -(-1)^n*A052657(n-1)=[0,0,2,-6,48,-240,...] is a(n-1)=[0,0,2,6,38,270,...].",
				"Stirling transform of -(-1)^n*A052558(n-1)=[1,-1,4,-12,72,-360,...] is a(n-1)=[1,0,2,6,38,270,...].",
				"Stirling transform of 2*A052591(n)=[2,4,24,96,...] is a(n+1)=[2,6,38,270,...].",
				"(End)",
				"Also the central moments of a Geometric(1/2) random variable (for example the number of coin tosses until the first head). - _Svante Janson_, Dec 10 2012",
				"Also the number of ordered set partitions of {1..n} with no cyclical adjacencies (successive elements in the same block, where 1 is a successor of n). - _Gus Wiseman_, Feb 13 2019",
				"Also the number of ordered set partitions of {1..n} with an even number of blocks. - _Geoffrey Critzer_, Jul 04 2020"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A052841/b052841.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"C. G. Bower, \u003ca href=\"/transforms2.html\"\u003eTransforms (2)\u003c/a\u003e",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=808\"\u003eEncyclopedia of Combinatorial Structures 808\u003c/a\u003e",
				"Svante Janson, \u003ca href=\"http://arxiv.org/abs/1305.3512\"\u003eEuler-Frobenius numbers and rounding\u003c/a\u003e, preprint arXiv:1305.3512 [math.PR], 2013.",
				"Lukas Spiegelhofer, \u003ca href=\"https://arxiv.org/abs/1910.13170\"\u003eA lower bound for Cusick's conjecture on the digits of n+t\u003c/a\u003e, arXiv:1910.13170 [math.NT], 2019."
			],
			"formula": [
				"O.g.f.: Sum_{n\u003e=0} (2*n)! * x^(2*n) / Product_{k=1..2*n} (1-k*x). - _Paul D. Hanna_, Jul 20 2011",
				"a(n) = (A000670(n) + (-1)^n)/2 = Sum_{k\u003e=0} (k-1)^n/2^(k+1). - _Vladeta Jovovic_, Feb 02 2003",
				"Also, a(n) = Sum_{k=0..[n/2]} (2k)!*Stirling2(n, 2k). - _Ralf Stephan_, May 23 2004",
				"a(n) = D^n*(1/(1-x^2)) evaluated at x = 0, where D is the operator (1+x)*d/dx. Cf. A000670 and A005649. - _Peter Bala_, Nov 25 2011",
				"E.g.f.: 1/2/G(0) where G(k) = 1 - 2^k/(2 - 4*x/(2*x - 2^k*(k+1)/G(k+1) )); (recursively defined continued fraction). - _Sergei N. Gladkovskii_, Dec 22 2012",
				"a(n) ~ n!/(4*(log(2))^(n+1)). - _Vaclav Kotesovec_, Aug 10 2013",
				"a(n) = (h(n)+(-1)^n)/2 where h(n) = Sum_{k=0..n} E(n,k)*2^k and E(n,k) the Eulerian numbers A173018 (see also A156365). - _Peter Luschny_, Sep 19 2015",
				"a(n) = (-1)^n + Sum_{k=0..n-1} binomial(n,k) * a(k). - _Ilya Gutkovskiy_, Jun 11 2020"
			],
			"example": [
				"From _Gus Wiseman_, Feb 13 2019: (Start)",
				"The a(4) = 38 ordered set partitions with no cyclical adjacencies:",
				"  {{1}{2}{3}{4}}  {{1}{24}{3}}  {{13}{24}}",
				"  {{1}{2}{4}{3}}  {{1}{3}{24}}  {{24}{13}}",
				"  {{1}{3}{2}{4}}  {{13}{2}{4}}",
				"  {{1}{3}{4}{2}}  {{13}{4}{2}}",
				"  {{1}{4}{2}{3}}  {{2}{13}{4}}",
				"  {{1}{4}{3}{2}}  {{2}{4}{13}}",
				"  {{2}{1}{3}{4}}  {{24}{1}{3}}",
				"  {{2}{1}{4}{3}}  {{24}{3}{1}}",
				"  {{2}{3}{1}{4}}  {{3}{1}{24}}",
				"  {{2}{3}{4}{1}}  {{3}{24}{1}}",
				"  {{2}{4}{1}{3}}  {{4}{13}{2}}",
				"  {{2}{4}{3}{1}}  {{4}{2}{13}}",
				"  {{3}{1}{2}{4}}",
				"  {{3}{1}{4}{2}}",
				"  {{3}{2}{1}{4}}",
				"  {{3}{2}{4}{1}}",
				"  {{3}{4}{1}{2}}",
				"  {{3}{4}{2}{1}}",
				"  {{4}{1}{2}{3}}",
				"  {{4}{1}{3}{2}}",
				"  {{4}{2}{1}{3}}",
				"  {{4}{2}{3}{1}}",
				"  {{4}{3}{1}{2}}",
				"  {{4}{3}{2}{1}}",
				"(End)"
			],
			"maple": [
				"spec := [S,{B=Prod(C,C),C=Set(Z,1 \u003c= card),S=Sequence(B)},labeled]: seq(combstruct[count](spec,size=n), n=0..20);",
				"P := proc(n,x) option remember; if n = 0 then 1 else",
				"(n*x+2*(1-x))*P(n-1,x)+x*(1-x)*diff(P(n-1,x),x); expand(%) fi end:",
				"A052841 := n -\u003e subs(x=2, P(n,x)):",
				"seq(A052841(n), n=0..21); # _Peter Luschny_, Mar 07 2014",
				"h := n -\u003e add(combinat:-eulerian1(n, k)*2^k, k=0..n):",
				"a := n -\u003e (h(n)+(-1)^n)/2: seq(a(n), n=0..21); # _Peter Luschny_, Sep 19 2015"
			],
			"mathematica": [
				"a[n_] := If[n == 0, 1, (PolyLog[-n, 1/2]/2 + (-1)^n)/2]; (* or *)",
				"a[n_] := HurwitzLerchPhi[1/2, -n, -1]/2; Table[a[n], {n, 0, 21}] (* _Jean-François Alcover_, Feb 19 2016, after _Vladeta Jovovic_ *)",
				"With[{nn=30},CoefficientList[Series[1/(Exp[x](2-Exp[x])),{x,0,nn}],x] Range[ 0,nn]!] (* _Harvey P. Dale_, Apr 08 2019 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c0,0,n!*polcoeff(subst(1/(1-y^2),y,exp(x+x*O(x^n))-1),n))",
				"(PARI) {a(n)=polcoeff(sum(m=0,n,(2*m)!*x^(2*m)/prod(k=1,2*m,1-k*x+x*O(x^n))),n)} /* _Paul D. Hanna_, Jul 20 2011 */"
			],
			"xref": [
				"Inverse binomial transform of A000670.",
				"Cf. A052558, A052591, A052657, A005359, A173018, A156365.",
				"Cf. A000126, A000296, A001610, A032032, A052841, A124323, A169985, A187784, A324011.",
				"Main diagonal of A122101."
			],
			"keyword": "easy,nonn",
			"offset": "0,3",
			"author": "encyclopedia(AT)pommard.inria.fr, Jan 25 2000",
			"ext": [
				"Edited by _N. J. A. Sloane_, Sep 06 2013"
			],
			"references": 43,
			"revision": 86,
			"time": "2020-07-04T14:51:25-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}