{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A307188",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 307188,
			"data": "0,9,11,14,16,18,20,22,30,47,49,51,53,59,61,63,66,68,70,72,74,76,88,90,92,100,117,119,121,123,125,127,137,139,141,147,149,151,153,155,158,160,162,178,180,182,184,186,194,196,198,210,219,221,223,225,227",
			"name": "Positions of 0's in the square spiral in A275609.",
			"comment": [
				"This sequence and A307189-A307192 together partition the nonnegative numbers.",
				"Comment from _N. J. A. Sloane_, Mar 31 2019 (Start)",
				"Both Rémy Sigrist's illustration and mine show the same block structure. A block B(r,s) will mean an array of r X s 0's separated by rows and columns of blanks. Here is B(3,5), with blanks represented by dots:",
				"......",
				"0.0.0.",
				"......",
				"0.0.0.",
				"......",
				"0.0.0.",
				"The dots are always to the right of or above the 0's.",
				"The block B(r,s) has size 2r X 2s.",
				"The block containing the center point is a B(3,5).",
				"Looking at the blocks along the x-axis, moving from West to East, the successive r-values are:",
				"... 23, 19, 15, 11, 7, (3), 5, 9, 13, 17, 21, ...",
				"Looking at the blocks along the y-axis, moving from South to North, the successive s-values are:",
				"... 24, 20, 16, 12, 8, (5), 6, 10, 14, 18, 22, ...",
				"Except at the central block B(3,5), the differences between adjacent r- and s-values always have magnitude 4.",
				"Furthermore, as we move North, each block is displaced by one step to the East from the block below it. As we move East, each block is displaced by one step to the South from the block to the West of it.",
				"These displacements can be seen in both illustrations. In Sigrist's illustration, notice how the blocks gradually shift to the East along the y-xis, and to the South along along the x-axis.",
				"(End)"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A307188/b307188.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e (first 1240 terms from N. J. A. Sloane)",
				"Rémy Sigrist, \u003ca href=\"/A307188/a307188.png\"\u003eColored scatterplot of (x,y) such that A275609(x,y) = 0 and -500 \u003c= x \u003c= 500 and -500 \u003c= y \u003c= 500\u003c/a\u003e (where the color is function of (x mod 2) + 2 * (y mod 2))",
				"N. J. A. Sloane, \u003ca href=\"/A307188/a307188.txt\"\u003eCentral portion of spiral in A275609 showing just the 0's\u003c/a\u003e [Entries 1,2,3,4 in the spiral have been replaced by dots. The central 0 has been changed to an X. The illustration shows cells (x,y) with -35 \u003c= x \u003c= 35, -33 \u003c= y \u003c= 36.]"
			],
			"xref": [
				"Cf. A275609, A307189-A307192."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Mar 28 2019",
			"references": 6,
			"revision": 28,
			"time": "2019-03-31T23:39:09-04:00",
			"created": "2019-03-28T15:25:48-04:00"
		}
	]
}