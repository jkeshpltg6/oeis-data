{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A173953",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 173953,
			"data": "0,16,928,119344,3078464,1132669904,606887707616,49610806397296,48006150564413056,48265162121607952,8192066749392160288,15200753287254377716912,33677610844789597790454208",
			"name": "a(n) = numerator of (Zeta(2, 3/4) - Zeta(2, n-1/4)), where Zeta is the Hurwitz Zeta function.",
			"comment": [
				"All numbers in this sequence are divisible by 16. For A173953/16 see A173955.",
				"a(n+2)/A173954(n+2) is, for n \u003e= 0, the partial sum Sum_{k=0..n} 1/(k + 3/4)^2 = 16*Sum_{k=0..n} 1/(4*k + 3)^2. The limit n -\u003e infinity is given in A282824 as Zeta(2, 3/4) = Psi(1, 3/4) = Pi^2 - 8*Catalan, with the trigamma function Psi(1, z) and the Catalan constant A006752."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A173953/b173953.txt\"\u003eTable of n, a(n) for n = 1..250\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HurwitzZetaFunction.html\"\u003eHurwitz Zeta Function\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/TrigammaFunction.html\"\u003eTrigamma Function\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Numerator of (Pi^2 - 8*Catalan - Zeta(2, (4 n - 1)/4)).",
				"Numerator of 128*n*Sum_{k\u003e=1} (4*k - 1 + 2*n) / ((4*k - 1)^2 * (4*k - 1 + 4*n)^2). - _Vaclav Kotesovec_, Nov 14 2017",
				"Numerator of 16*Sum_{k=0..n-2} 1/(4*k + 3)^2, n \u003e= 2, with a(1) = 0. See a comment above. - _Wolfdieter Lang_, Nov 14 2017"
			],
			"example": [
				"The rationals r(n) = Zeta(2, 3/4) - Zeta(2, n-1/4) begin:  0/1, 16/9, 928/441, 119344/53361, 3078464/1334025, 1132669904/481583025, 606887707616/254757420225, 49610806397296/20635351038225, ... - _Wolfdieter Lang_, Nov 14 2017"
			],
			"maple": [
				"r := n -\u003e Zeta(0, 2, 3/4) - Zeta(0, 2, n-1/4):",
				"seq(numer(simplify(r(n))), n=1..13); # _Peter Luschny_, Nov 14 2017"
			],
			"mathematica": [
				"Table[Numerator[FunctionExpand[Pi^2 - 8*Catalan - Zeta[2, (4*n - 1)/4]]], {n, 1, 20}] (* _Vaclav Kotesovec_, Nov 14 2017 *)",
				"Numerator[Table[128*n*Sum[(4*k - 1 + 2*n) / ((4*k - 1)^2 * (4*k - 1 + 4*n)^2), {k, 1, Infinity}], {n, 0, 20}]] (* _Vaclav Kotesovec_, Nov 14 2017 *)",
				"Numerator[Table[16*Sum[1/(4*k + 3)^2, {k, 0, n-1}], {n, 1, 20}]] (* _Vaclav Kotesovec_, Nov 15 2017 *)"
			],
			"program": [
				"(PARI) for(n=1,20, print1(numerator(16*sum(k=0,n-2, 1/(4*k+3)^2)), \", \")) \\\\ _G. C. Greubel_, Aug 23 2018",
				"(MAGMA) [0] cat [Numerator((\u0026+[16/(4*k+3)^2: k in [0..n-2]])): n in [2..20]]; // _G. C. Greubel_, Aug 23 2018"
			],
			"xref": [
				"Denominators are in A173954.",
				"Cf. A006752, A120268, A173945, A173947, A173948, A173949, A173955."
			],
			"keyword": "frac,nonn,easy",
			"offset": "1,2",
			"author": "_Artur Jasinski_, Mar 03 2010",
			"ext": [
				"Name simplified by _Peter Luschny_, Nov 14 2017"
			],
			"references": 11,
			"revision": 44,
			"time": "2018-08-24T00:39:26-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}