{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A336532",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 336532,
			"data": "1,2,1,2,2,1,2,1,1,1,2,2,1,2,1,2,1,2,2,2,1,2,1,1,2,1,2,1,2,1,2,1,1,2,1,1,2,2,1,2,1,1,1,2,1,2,1,2,2,2,1,1,2,2,1,2,1,1,2,2,2,1,2,1,2,1,2,1,2,1,2,1,1,2,1,1,2,1,2,1,2,2,1,2,1,2,1,2,2,2,1,2,1,1,1,2,1,2,2,2,1,2,2,2,1",
			"name": "Square array read by antidiagonals upwards showing a stained glass windows with two colors and a hidden curve from the toothpick cellular automaton of A139250 (see Comments lines for definition).",
			"comment": [
				"Inspired by Neil Sloane's presentation at Rutgers' Experimental Mathematics Seminar (see the Links section).",
				"Beneath the familiar image of every cellular automaton lies an infinite world of hidden patterns, stained-glass windows, gaskets, curves and fractals.",
				"As an example of this statement we will focus on the \"toothpick\" cellular automata. In general after 2^k stages, k \u003e= 2, these structures looks like the framework of a stained-glass window (without the colored glass). Toothpicks represent the cames of the structure. Now the idea is to put the stained glass.",
				"Here we will use the \"toothpick\" cellular automaton of A139250.",
				"After every stage the square cells of the newly formed regions will be colored.",
				"We have two colors. If n is odd, they are painted with the color 1. If n is even, they are painted with the color 2.",
				"Note that there are infinitely many rules for coloring a cellular automaton since there are infinitely many colors related to infinitely many sequences, however, the rule used here seems quite natural, since the number of colors coincides with the number of letters of the \"word\" of this cellular automaton, which is \"ab\". So here we have toothpicks on the two axes of the infinite square grid, two associated sounds (tick-tock) and two colors.",
				"After 2^k stages, k \u003e= 2, a rectangular-stained-glass window with two colors will have been formed.",
				"Conjecture 1: after 2^k stages the number of cells of color 1 is equal to the number of cells of color 2.",
				"Conjecture 2: after 2^k stages, k \u003e= 2, in the structure there are essentially one major region of color 1 and two major regions of color 2.",
				"It appears that there are certain sub-quadrants that have the complementary structure and the opposite colors of other sub-quadrants.",
				"This sequence is a square array read by antidiagonals upwards that represents the colors (1 or 2) of every cell in the fourth quadrant of the stained-glass windows. The corner of the array represents the cell whose upper-left vertex is the point (0,0) of the fourth quadrant of the structure.",
				"For a binary sequence the 2's should be replaced with 0's.",
				"Note that for the toothpick cellular automaton on triangular grid of A296510 (whose word is \"abc\") three colors should be used there. Same for the C.A. of A299476 and of A299478.",
				"For more information on the \"word\" of a cellular automaton see A296612 and see ALSO the third triangle in the example section of A139251.",
				"The following three steps refer to the visualization of hidden gaskets and hidden curves from the stained-glass windows of the toothpick structures.",
				"First, a growth limit is set until the final stage 2^k.",
				"Then the line segments other than the border between the two colors are removed.",
				"Finally the colors are also removed.",
				"In this case, two curves will be formed. One curve on the first and second quadrant and the other curve on the third and fourth quadrant. One curve is the reflection of the other.",
				"After studying and analyzing the curve, a sequence and an animation could be made to represent it, from the stage 1 to n.",
				"The curve obtained resembles the Hilbert curve and the Moore curve, but apparently here the curve is a bit more complex (see the example)."
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"https://vimeo.com/457349959\"\u003eConant's Gasket, Recamán Variations, the Enots Wolley Sequence, and Stained Glass Windows\u003c/a\u003e, Experimental Math Seminar, Rutgers University, Sep 10 2020 (video of Zoom talk)."
			],
			"example": [
				"The corner of the square array is as follows:",
				"1, 1, 1, 1, 1, 1, 1, 1, ...",
				"2, 2, 1, 2, 2, 2, 1, 2, ...",
				"2, 1, 1, 2, 1, 2, 1, 2, ...",
				"2, 2, 2, 2, 1, 1, 1, 2, ...",
				"2, 1, 1, 1, 1, 1, 1, 2, ...",
				"2, 1, 2, 2, 2, 2, 1, 2, ...",
				"2, 1, 1, 2, 2, 1, 1, 2, ...",
				"2, 2, 2, 2, 2, 2, 2, 2, ...",
				"...",
				"The above array represents the fourth quadrant of the stained-glass windows.",
				"Below, the toothpick structure and two of its hidden patterns after 16 stages:",
				".   _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _             _ _ _ _ _ _ _ _ _ _ _ _ _ _",
				".    |_ _|   |_ _|   |_ _|   |_ _|             |_ _     _ _     _ _     _ _|",
				".    | |_|_ _|_| |   | |_|_ _|_| |             |  _|   |_  |   |  _|   |_  |",
				".    |_|_|_ _|_|_|   |_|_|_ _|_|_|             | |_ _ _ _| |   | |_ _ _ _| |",
				".    |   | |_|_ _|_ _|_ _|_| |   |             |      _ _ _|   |_ _ _      |",
				".    |_ _|_|_|_ _|   |_ _|_|_|_ _|             |  _  |  _ _  2  _ _  |  _  |",
				".    | |_|_| | |_|_ _|_| | |_|_| |             | | |_| |  _|   |_  | |_| | |",
				".    |_|_ _|_|_|_|_ _|_|_|_|_ _|_|             | |_ _ _| |_ _ _ _| |_ _ _| |",
				".    |       |   | | |   |       |             |             1             |",
				".    |_ _ _ _|_ _|_|_|_ _|_ _ _ _|             |  _ _ _   _ _ _ _   _ _ _  |",
				".    | |_ _| | |_|_ _|_| | |_ _| |             | |  _  | |_     _| |  _  | |",
				".    |_|_|_|_|_|_|   |_|_|_|_|_|_|             | |_| | |_ _|   |_ _| | |_| |",
				".    |   | |_|_ _|_ _|_ _|_| |   |             |     |_ _ _  2  _ _ _|     |",
				".    |_ _|_|_|_ _|   |_ _|_|_|_ _|             |  _ _ _ _  |   |  _ _ _ _  |",
				".    | |_|_ _|_| |   | |_|_ _|_| |             | |_     _| |   | |_     _| |",
				".    |_|_|   |_|_|   |_|_|   |_|_|             |_ _|   |_ _|   |_ _|   |_ _|",
				".   _|_ _|_ _|_ _|_ _|_ _|_ _|_ _|_            |_ _ _ _ _ _ _ _ _ _ _ _ _ _|",
				".",
				".              Figure  1                                  Figure 2",
				".       The toothpick structure                    The hidden curves are",
				".             of A139250.                          the boundaries between",
				".                                                  the colors 1 and 2.",
				".",
				".     _ _     _ _     _ _     _ _",
				".       _|   |_  |   |  _|   |_",
				".      |_ _ _ _| |   | |_ _ _ _|",
				".           _ _ _|   |_ _ _",
				".       _  |  _ _     _ _  |  _",
				".      | |_| |  _|   |_  | |_| |",
				".      |_ _ _| |_ _ _ _| |_ _ _|",
				".",
				".       _ _ _   _ _ _ _   _ _ _",
				".      |  _  | |_     _| |  _  |",
				".      |_| | |_ _|   |_ _| | |_|",
				".          |_ _ _     _ _ _|",
				".       _ _ _ _  |   |  _ _ _ _",
				".      |_     _| |   | |_     _|",
				".     _ _|   |_ _|   |_ _|   |_ _",
				".",
				".               Figure  3",
				".          The hidden curves.",
				".",
				"Below, the hidden curve in the fourth quadrant after 32 stages of the cellular automaton:",
				"      _ _   _ _ _   _ _ _ _ _ _ _",
				".       _| |  _  | |  _ _     _  |",
				".      |_ _| | |_| | |  _|   | |_|",
				".       _ _ _|     | | |_ _  |",
				".      |  _ _ _ _  | |_ _  | |  _",
				".      | |_     _| |  _  | | |_| |",
				".      |_ _|   |_ _| | |_| |_ _ _|",
				".       _ _ _ _ _ _ _|",
				"       |  _ _ _   _ _ _ _   _ _ _",
				".      | |  _  | |_     _| |  _  |",
				".      | |_| | |_ _|   |_ _| | |_|",
				".      |     |_ _ _     _ _ _|",
				".      |  _ _ _ _  |   |  _ _ _ _",
				".      | |_     _| |   | |_     _|",
				".      |_ _|   |_ _|   |_ _|   |_ _",
				".",
				".               Figure  4",
				".           The hidden curve.",
				"."
			],
			"xref": [
				"Cf. A139250, A139251, A296612.",
				"Cf. A160120 (word \"a\"), A139250 (word \"ab\"), A296510 (word \"abc\"), A299476 (word \"abcb\"), A299478 (word \"abcbc\")."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Omar E. Pol_, Oct 04 2020",
			"references": 1,
			"revision": 82,
			"time": "2020-10-13T23:50:41-04:00",
			"created": "2020-10-13T23:50:41-04:00"
		}
	]
}