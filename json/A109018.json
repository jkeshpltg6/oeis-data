{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A109018",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 109018,
			"data": "13,15,117,126,270,2576,8820,16560,21168,46848,295245,441600,846720,4078080,80663040,40590720,2173236480,4011724800,21122906112,40915058688,274148425728,63769149440,2707602702336,6167442456576",
			"name": "Least n-almost prime number which gives a different n-almost prime number when digits are reversed.",
			"comment": [
				"An emirp (\"prime\" spelled backwards) is a prime whose (base 10) reversal is also prime, but which is not a palindromic prime. The first few are 13, 17, 31, 37, 71, 73, 79, 97, 107, 113, 149, 157, ... (A006567).",
				"An emirpimes (\"semiprime\" spelled backwards) is a semiprime whose (base 10) reversal is a different semiprime. A list of the first emirpimeses (or \"semirpimes\") are 15, 26, 39, 49, 51, 58, 62, 85, 93, 94, 115, 122, 123, ... (A097393).",
				"An \"emirp tsomla-3\" (\"3-almost prime\" spelled backwards) is the k=3 sequence of the series for which k=1 are emirps and k=2 are emirpimes, a list of these being A109023. The union of these for k=1 through k = 13 is A109019.",
				"The primes correspond to the \"1-almost prime\" numbers 2, 3, 5, 7, 11, ... (A000040). The 2-almost prime numbers correspond to semiprimes 4, 6, 9, 10, 14, 15, 21, 22, ... (A001358).",
				"The first few 3-almost primes are 8, 12, 18, 20, 27, 28, 30, 42, 44, 45, 50, 52, 63, 66, 68, 70, 75, 76, 78, 92, 98, 99, ... (A014612). The first few 4-almost primes are 16, 24, 36, 40, 54, 56, 60, 81, 84, 88, 90, 100, ... (A014613).",
				"The first few 5-almost primes are 32, 48, 72, 80, ... (A014614).",
				"The Mathematica code for this was written by Ray Chandler who has coauthorship credit for this sequence."
			],
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/AlmostPrime.html\"\u003eAlmost Prime\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Emirp.html\"\u003eEmirp\u003c/a\u003e.",
				"Eric Weisstein and Jonathan Vos Post, \u003ca href=\"http://mathworld.wolfram.com/Emirpimes.html\"\u003eEmirpimes.\u003c/a\u003e"
			],
			"example": [
				"a(1) = 13 because 13 is the smallest \"emirp\" (prime which, digits reversed, becomes a different prime) since reverse(13) = 31 is prime.",
				"a(2) = 15 because 15 is the smallest emirpimes (\"semiprime\" spelled backwards) as a semiprime whose (base 10) reversal is a different semiprime. The first such number is 15, since 15 reversed is 51 and both 15 and 51 are semiprimes (i.e. 15 = 3 * 5 and 51 = 3 * 17).",
				"a(3) = 117 because 117 is the smallest \"emirp tsomla-3\" (\"3-almost prime\" spelled backwards) since 117 reversed is 711 and 117 = 3^2 * 13 and 711 = 3^2 * 79."
			],
			"mathematica": [
				"kAlmost[n_] := Plus @@ Last /@ FactorInteger@n; fQ[n_] := Block[{id = IntegerDigits@n, k = kAlmost@n}, If[id != Reverse@id \u0026\u0026 k == kAlmost@FromDigits@Reverse@id, k, -1]]; t = Table[0, {20}]; Do[ a = fQ@n; If[a \u003c 20 \u0026\u0026 t[[a]] == 0, t[[a]] = n; Print[{a, n}]], {n, 10, 150000000}] (* _Robert G. Wilson v_ *)"
			],
			"xref": [
				"Cf. A006567, A097393, A109023, A109023, A109024, A109025, A109026, A109027, A109028, A109029, A109030, A109031."
			],
			"keyword": "base,nonn,less",
			"offset": "1,1",
			"author": "_Jonathan Vos Post_, Jun 16 2005",
			"ext": [
				"a(14)-a(16) from _Robert G. Wilson v_, Jan 06 2008",
				"a(17)-a(24) from _Donovan Johnson_, Nov 17 2008"
			],
			"references": 11,
			"revision": 8,
			"time": "2013-10-31T12:17:27-04:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}