{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A303427",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 303427,
			"data": "2,0,1,1,3,1,4,2,7,3,11,5,18,8,29,13,47,21,76,34,123,55,199,89,322,144,521,233,843,377,1364,610,2207,987,3571,1597,5778,2584,9349,4181,15127,6765,24476,10946,39603,17711,64079,28657,103682,46368,167761",
			"name": "Interleaved Lucas and Fibonacci numbers.",
			"link": [
				"Harvey P. Dale, \u003ca href=\"/A303427/b303427.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,1,0,1)."
			],
			"formula": [
				"a(n) = a(n-2) + a(n-4).",
				"G.f.: -(x+1)*(x^2-2*x+2)/(x^4+x^2-1). - _Alois P. Heinz_, Apr 23 2018"
			],
			"example": [
				"a(8) = Lucas(4) = 7;",
				"a(9) = Fibonacci(4) = 3."
			],
			"maple": [
				"a:= n-\u003e (\u003c\u003c0|1\u003e, \u003c1|1\u003e\u003e^iquo(n, 2, 'r'). \u003c\u003c2*(1-r), 1\u003e\u003e)[1, 1]:",
				"seq(a(n), n=0..60);  # _Alois P. Heinz_, Apr 23 2018"
			],
			"mathematica": [
				"LinearRecurrence[{0, 1, 0, 1}, {2, 0, 1, 1}, 60] (* _Vincenzo Librandi_, Apr 25 2018 *)",
				"With[{nn=30},Riffle[LucasL[Range[0,nn]],Fibonacci[Range[0,nn]]]] (* _Harvey P. Dale_, Feb 25 2021 *)"
			],
			"program": [
				"(MATLAB)",
				"F = zeros(1,N);",
				"L = ones(1,N);",
				"F(2) = 1;",
				"L(1) = 2",
				"for  n = 3:N",
				"    F(n) = F(n-1) + F(n-2);",
				"    L(n) = L(n-1) + L(n-2);",
				"end",
				"A = F;",
				"B = L;",
				"C=[B; A];",
				"C=C(:)';",
				"C",
				"(MAGMA) [IsEven(n) select Lucas(n div 2) else Fibonacci((n-1) div 2): n in [0..70]]; // _Vincenzo Librandi_, Apr 25 2018",
				"(PARI) a(n) = if(n%2, fibonacci(n\\2), fibonacci(n/2-1)+fibonacci(n/2+1)); \\\\ _Altug Alkan_, Apr 25 2018"
			],
			"xref": [
				"Cf. A000045, A000032, A302126."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_Craig P. White_, Apr 23 2018",
			"references": 1,
			"revision": 26,
			"time": "2021-02-25T14:40:38-05:00",
			"created": "2018-04-27T22:04:49-04:00"
		}
	]
}