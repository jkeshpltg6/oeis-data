{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A222068",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 222068,
			"data": "6,1,6,8,5,0,2,7,5,0,6,8,0,8,4,9,1,3,6,7,7,1,5,5,6,8,7,4,9,2,2,5,9,4,4,5,9,5,7,1,0,6,2,1,2,9,5,2,5,4,9,4,1,4,1,5,0,8,3,4,3,3,6,0,1,3,7,5,2,8,0,1,4,0,1,2,0,0,3,2,7,6,8,7,6,1,0,8,3,7,7,3,2,4,0,9,5,1,4,4,8,9,0,0",
			"name": "Decimal expansion of (1/16)*Pi^2.",
			"comment": [
				"Conjectured to be density of densest packing of equal spheres in four dimensions (achieved for example by the D_4 lattice).",
				"From _Hugo Pfoertner_, Aug 29 2018: (Start)",
				"Also decimal expansion of Sum_{k\u003e=0} (-1)^k*d(2*k+1)/(2*k+1), where d(n) is the number of divisors of n A000005(n).",
				"Ramanujan's question 770 in the Journal of the Indian Mathematical Society (VIII, 120) asked \"If d(n) denotes the number of divisors of n, show that d(1) - d(3)/3 + d(5)/5 - d(7)/7 + d(9)/9 - ... is a convergent series ...\".",
				"A summation of the first 2*10^9 terms performed by _Hans Havermann_ yields 0.6168503077..., which is close to (Pi/4)^2=0.616850275...",
				"(End)",
				"From _Robert Israel_, Aug 31 2018: (Start)",
				"Modulo questions about rearrangement of conditionally convergent series, which I expect a more careful treatment would handle, Sum_{k\u003e=0} (-1)^k*d(2*k+1)/(2*k+1) should indeed be Pi^2/16.",
				"Sum_{k\u003e=0} (-1)^k d(2k+1)/(2k+1)",
				"  = Sum_{k\u003e=0} Sum_{2i+1 | 2k+1} (-1)^k/(2k+1)",
				"(letting 2k+1=(2i+1)(2j+1): note that k == i+j (mod 2))",
				"  = Sum_{i\u003e=0} Sum_{j\u003e=0} (-1)^(i+j)/((2i+1)(2j+1))",
				"  = (Sum_{i\u003e=0} (-1)^i/(2i+1))^2 = (Pi/4)^2. (End)",
				"Volume bounded by the surface (x+y+z)^2-2(x^2+y^2+z^2)=4xyz, the ellipson (see Wildberger, p. 287). - _Patrick D McLean_, Dec 03 2020"
			],
			"reference": [
				"J. H. Conway and N. J. A. Sloane, \"Sphere Packings, Lattices and Groups\", Springer, 3rd. ed., 1998. See p. xix.",
				"S. D. Chowla, Solution and Remarks on Question 770, J. Indian Math. Soc. 17 (1927-28), 166-171.",
				"S. Ramanujan, Coll. Papers, Chelsea, 1962, Question 770, page 333.",
				"G. N. Watson, Solution to Question 770, J. Indian Math. Soc. 18 (1929-30), 294-298."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A222068/b222068.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"B. C. Berndt, Y. S. Choi and S. Y. Kang, \u003ca href=\"https://faculty.math.illinois.edu/~berndt/jims.ps\"\u003eThe problems submitted by Ramanujan to the Journal of Indian Math. Soc.\u003c/a\u003e, in: Continued fractions, Contemporary Math., 236 (1999), 15-56 (see Q770, JIMS VIII).",
				"J. H. Conway and N. J. A. Sloane, \u003ca href=\"http://dx.doi.org/10.1007/BF02574051\"\u003eWhat are all the best sphere packings in low dimensions?\u003c/a\u003e, Discr. Comp. Geom., 13 (1995), 383-403.",
				"Mathematics StackExchange, \u003ca href=\"https://math.stackexchange.com/questions/2903015/sum-k-1k-frac-tau2k12k1\"\u003eSum_k (-1)^k tau(2k+1)/(2k+1)\u003c/a\u003e.",
				"G. Nebe and N. J. A. Sloane, \u003ca href=\"http://www.math.rwth-aachen.de/~Gabriele.Nebe/LATTICES/D4.html\"\u003eHome page for D_4 lattice\u003c/a\u003e.",
				"N. J. A. Sloane, \u003ca href=\"/A093825/a093825.txt\"\u003eTable of maximal density of a packing of equal spheres in n-dimensional Euclidean space (for n\u003e3 the values are only conjectural)\u003c/a\u003e.",
				"N. J. Wildberger, \u003ca href=\"https://www.researchgate.net/publication/266738365_Divine_Proportions_Rational_Trigonometry_to_Universal_geometry\"\u003eDivine Proportions: Rational Trigonometry to Universal Geometry\u003c/a\u003e, Wild Egg Books, Sydney 2005.",
				"\u003ca href=\"/index/Tra#transcendental\"\u003eIndex entries for transcendental numbers\u003c/a\u003e."
			],
			"formula": [
				"Equals A003881^2. - _Bruno Berselli_, Feb 11 2013",
				"Equals A123092+1/2. - _R. J. Mathar_, Feb 15 2013",
				"Equals Integral_{x\u003e0} x^2*log(x)/((1+x)^2*(1+x^2)) dx. - _Jean-François Alcover_, Apr 29 2013",
				"Equals the Bessel moment integral_{x\u003e0} x*I_0(x)*K_0(x)^3. - _Jean-François Alcover_, Jun 05 2016",
				"Equals Sum_{k\u003e=1} zeta(2*k)*k/4^k. - _Amiram Eldar_, May 29 2021"
			],
			"example": [
				"0.6168502750680849136771556874922594459571..."
			],
			"mathematica": [
				"RealDigits[N[Gamma[3/2]^4, 104]] (* _Fred Daniel Kline_, Feb 19 2017 *)",
				"RealDigits[N[Pi^2/16, 100]][[1]] (* _Vincenzo Librandi_, Feb 20 2017 *)",
				"Integrate[Boole[(x+y+z)^2-2(x^2+y^2+z^2)\u003e4x y z],{x,0,1},{y,0,1},{z,0,1}] (* _Patrick D McLean_, Dec 03 2020 *)"
			],
			"program": [
				"(PARI) (Pi/4)^2 \\\\ _Charles R Greathouse IV_, Oct 31 2014",
				"(MAGMA) pi:=Pi(RealField(110)); Reverse(Intseq(Floor((1/16)*10^100*pi^2))); // _Vincenzo Librandi_, Feb 20 2017"
			],
			"xref": [
				"Cf. A000005.",
				"Related constants: A020769, A020789, A093766, A093825, A222066, A222067, A222069, A222070, A222071, A222072, A222073, A222074, A222075."
			],
			"keyword": "nonn,cons",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_, Feb 10 2013",
			"references": 25,
			"revision": 74,
			"time": "2021-05-29T04:09:16-04:00",
			"created": "2013-02-10T20:15:11-05:00"
		}
	]
}