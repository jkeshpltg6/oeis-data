{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A343387",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 343387,
			"data": "1,1,2,2,3,2,3,1,3,2,3,4,3,4,4,3,3,1,6,4,3,3,4,3,3,2,4,5,4,4,3,2,3,4,5,6,5,4,6,2,6,4,4,7,5,3,4,1,5,4,8,8,2,5,5,1,5,4,3,8,5,6,2,3,5,4,6,4,6,4,5,3,5,4,4,5,8,2,7,2,3,7,6,9,3,6,10,5,5,5,5,8,3,5,3,6,7,3,9,8,6",
			"name": "Number of ways to write n as x^2 + [y^2/2] + [z^4/8], where [.] is the floor function, x is a nonnegative integer, and y and z are positive integers.",
			"comment": [
				"Conjecture: a(n) \u003e 0 for all n \u003e= 0.",
				"This has been verified for all n = 0..10^5.",
				"We also conjecture that each n = 0,1,... can be written as x^2 + [y^2/3] + [z^4/7] with x,y,z nonnegative integers.",
				"See also A343391 for a similar conjecture."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A343387/b343387.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1504.01608\"\u003eNatural numbers represented by [x^2/a] + [y^2/b] + [z^2/c]\u003c/a\u003e, arXiv:1504.01608 [math.NT], 2015."
			],
			"example": [
				"a(0) = 1 with 0 = 0^2 + [1^2/2] + [1^4/8].",
				"a(47) = 1 with 47 = 5^2 + [5^2/2] + [3^4/8].",
				"a(55) = 1 with 55 = 7^2 + [3^2/2] + [2^4/8].",
				"a(217) = 1 with 217 = 11^2 + [6^2/2] + [5^4/8].",
				"a(377) = 1 with 377 9^2 + [23^2/2] + [4^4/8].",
				"a(392) = 1 with 392 = 0^2 + [28^2/2] + [1^4/8].",
				"a(734) = 1 with 734 = 12^2 + [32^2/2] + [5^4/8].",
				"a(1052) = 1 with 1052 = 32^2 + [6^2/2] + [3^4/8].",
				"a(1054) = 1 with 1054 = 30^2 + [17^2/2] + [3^4/8].",
				"a(1817) = 1 with 1817 = 39^2 + [23^2/2] + [4^4/8]."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]];",
				"tab={};Do[r=0;Do[If[SQ[n-Floor[x^2/2]-Floor[y^4/8]],r=r+1],{x,1,Sqrt[2n+1]},{y,1,(8(n-Floor[x^2/2])+7)^(1/4)}];tab=Append[tab,r],{n,0,100}];Print[tab]"
			],
			"xref": [
				"Cf. A000290, A000583, A343326, A343368, A343384, A343391, A343397, A343411."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Zhi-Wei Sun_, Apr 13 2021",
			"references": 8,
			"revision": 17,
			"time": "2021-04-15T04:45:07-04:00",
			"created": "2021-04-14T22:43:33-04:00"
		}
	]
}