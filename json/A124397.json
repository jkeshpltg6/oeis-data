{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A124397",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 124397,
			"data": "1,3,21,17,99,2223,12039,56763,59337,286961,7358781,36088473,183146521,181066401,36534213,4535753121,22798981683,113528187171,113891192583,568042152363,14228623114839,71035463999307,355598139789279",
			"name": "Numerators of partial sums of a series for sqrt(5)/3.",
			"comment": [
				"Denominators are given by A124398.",
				"The alternating sums over central binomial coefficients scaled by powers of 5, r(n) = Sum_{k=0..n} (-1)^k*binomial(2*k,k)/5^k, have the limit s = lim_{n-\u003e infinity} r(n) = sqrt(5)/3. From the expansion of 1/sqrt(1+x) for x=4/5."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A124397/b124397.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Wolfdieter Lang, \u003ca href=\"/A124397/a124397.txt\"\u003eRationals and more\u003c/a\u003e."
			],
			"formula": [
				"a(n) = numerator(r(n)) with the rationals r(n) = Sum_{k=0..n} (-1)^k * binomial(2*k,k)/5^k in lowest terms.",
				"r(n) = Sum_{k=0..n} (-1)^k*((2*k-1)!!/((2*k)!!)*(4/5)^k, n\u003e=0, with the double factorials A001147 and A000165."
			],
			"example": [
				"a(3)=17 because r(3) = 1 - 2/5 + 6/25 - 4/25 = 17/25 = a(3)/A124398(3)."
			],
			"maple": [
				"seq(numer(add((-1)^k*binomial(2*k, k)/5^k, k = 0..n)), n = 0..20); # _G. C. Greubel_, Dec 25 2019"
			],
			"mathematica": [
				"Table[Numerator[Sum[(-1)^k*(k+1)*CatalanNumber[k]/5^k, {k,0,n}]], {n,0,20}] (* _G. C. Greubel_, Dec 25 2019 *)"
			],
			"program": [
				"(PARI) a(n) = numerator(sum(k=0, n, ((-1)^k)*binomial(2*k,k)/5^k)); \\\\ _Michel Marcus_, Aug 11 2019",
				"(MAGMA) [Numerator(\u0026+[(-1)^k*(k+1)*Catalan(k)/5^k: k in [0..n]]): n in [0..20]]; // _G. C. Greubel_, Dec 25 2019",
				"(Sage) [numerator(sum((-1)^k*(k+1)*catalan_number(k)/5^k for k in (0..n))) for n in (0..20)] # _G. C. Greubel_, Dec 25 2019",
				"(GAP) List([0..20], n-\u003e NumeratorRat(Sum([0..n], k-\u003e (-1)^k*Binomial(2*k, k)/5^k)) ); # _G. C. Greubel_, Dec 25 2019"
			],
			"xref": [
				"Cf. A123747/A123748 partial sums for a series for sqrt(5).",
				"Cf. A123749/A124396 partial sums for a series for 3/sqrt(5).",
				"Cf. A124398 (denominators), A208899 (sqrt(5)/3)."
			],
			"keyword": "nonn,frac,easy",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Nov 10 2006",
			"references": 2,
			"revision": 16,
			"time": "2019-12-26T05:33:12-05:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}