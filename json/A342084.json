{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A342084",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 342084,
			"data": "1,1,1,1,1,2,1,2,1,2,1,4,1,2,2,3,1,4,1,4,2,2,1,9,1,2,2,4,1,7,1,6,2,2,2,10,1,2,2,9,1,6,1,4,4,2,1,19,1,4,2,4,1,8,2,9,2,2,1,20,1,2,4,10,2,6,1,4,2,7,1,29,1,2,4,4,2,6,1,19,3,2,1,19,2",
			"name": "Number of chains of distinct strictly superior divisors starting with n.",
			"comment": [
				"We define a divisor d|n to be strictly superior if d \u003e n/d. Strictly superior divisors are counted by A056924 and listed by A341673.",
				"These chains have first-quotients (in analogy with first-differences) that are term-wise \u003c their decapitation (maximum element removed). Equivalently, x \u003c y^2 for all adjacent x, y. For example, the divisor chain q = 30/6/3 has first-quotients (5,2), which are \u003c (6,3), so q is counted under a(30).",
				"Also the number of ordered factorizations of n where each factor is less than the product of all previous factors."
			],
			"formula": [
				"a(2^n) = A045690(n)."
			],
			"example": [
				"The a(n) chains for n = 2, 6, 12, 16, 24, 30, 32, 36:",
				"  2  6    12      16      24         30       32         36",
				"     6/3  12/4    16/8    24/6       30/6     32/8       36/9",
				"          12/6    16/8/4  24/8       30/10    32/16      36/12",
				"          12/6/3          24/12      30/15    32/8/4     36/18",
				"                          24/6/3     30/6/3   32/16/8    36/12/4",
				"                          24/8/4     30/10/5  32/16/8/4  36/12/6",
				"                          24/12/4    30/15/5             36/18/6",
				"                          24/12/6                        36/18/9",
				"                          24/12/6/3                      36/12/6/3",
				"                                                         36/18/6/3",
				"The a(n) ordered factorizations for n = 2, 6, 12, 16, 24, 30, 32, 36:",
				"  2  6    12     16     24       30     32       36",
				"     3*2  4*3    8*2    6*4      6*5    8*4      9*4",
				"          6*2    4*2*2  8*3      10*3   16*2     12*3",
				"          3*2*2         12*2     15*2   4*2*4    18*2",
				"                        3*2*4    3*2*5  8*2*2    4*3*3",
				"                        4*2*3    5*2*3  4*2*2*2  6*2*3",
				"                        4*3*2    5*3*2           6*3*2",
				"                        6*2*2                    9*2*2",
				"                        3*2*2*2                  3*2*2*3",
				"                                                 3*2*3*2"
			],
			"mathematica": [
				"ceo[n_]:=Prepend[Prepend[#,n]\u0026/@Join@@ceo/@Select[Most[Divisors[n]],#\u003en/#\u0026],{n}];",
				"Table[Length[ceo[n]],{n,100}]"
			],
			"xref": [
				"The restriction to powers of 2 is A045690, with reciprocal version A040039.",
				"The inferior version is A337135.",
				"The strictly inferior version is A342083.",
				"The superior version is A342085.",
				"The additive version allowing equality is A342094 or A342095.",
				"The additive version is A342096 or A342097.",
				"A000005 counts divisors.",
				"A001055 counts factorizations.",
				"A003238 counts divisibility chains summing to n-1, with strict case A122651.",
				"A038548 counts inferior (or superior) divisors.",
				"A056924 counts strictly inferior (or strictly superior) divisors.",
				"A067824 counts strict chains of divisors starting with n.",
				"A074206 counts strict chains of divisors from n to 1 (also ordered factorizations).",
				"A167865 counts strict chains of divisors \u003e 1 summing to n.",
				"A207375 lists central divisors.",
				"A253249 counts strict chains of divisors.",
				"A334996 counts ordered factorizations by product and length.",
				"A334997 counts chains of divisors of n by length.",
				"- Inferior: A033676, A063962, A066839, A072499, A161906.",
				"- Superior: A033677, A070038, A161908, A341591.",
				"- Strictly Inferior: A060775, A070039, A333806, A341674.",
				"- Strictly Superior: A064052/A048098, A140271, A238535, A341642, A341673.",
				"Cf. A000203, A000929, A001248, A006530, A018819, A020639, A169594, A337105, A342098."
			],
			"keyword": "nonn",
			"offset": "1,6",
			"author": "_Gus Wiseman_, Feb 28 2021",
			"references": 23,
			"revision": 16,
			"time": "2021-03-03T21:32:44-05:00",
			"created": "2021-03-03T21:32:44-05:00"
		}
	]
}