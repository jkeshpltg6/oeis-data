{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A055137",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 55137,
			"data": "1,0,1,-1,0,1,-2,-3,0,1,-3,-8,-6,0,1,-4,-15,-20,-10,0,1,-5,-24,-45,-40,-15,0,1,-6,-35,-84,-105,-70,-21,0,1,-7,-48,-140,-224,-210,-112,-28,0,1,-8,-63,-216,-420,-504,-378,-168,-36,0,1,-9,-80,-315,-720",
			"name": "Regard triangle of rencontres numbers (see A008290) as infinite matrix, compute inverse, read by rows.",
			"comment": [
				"The n-th row consists of coefficients of the characteristic polynomial of the adjacency matrix of the complete n-graph.",
				"Triangle of coefficients of det(M(n)) where M(n) is the n X n matrix m(i,j)=x if i=j, m(i,j)=i/j otherwise. - _Benoit Cloitre_, Feb 01 2003",
				"T is an example of the group of matrices outlined in the table in A132382--the associated matrix for rB(0,1). The e.g.f. for the row polynomials is exp(x*t) * exp(x) *(1-x). T(n,k) = Binomial(n,k)* s(n-k) where s = (1,0,-1,-2,-3,...) with an e.g.f. of exp(x)*(1-x) which is the reciprocal of the e.g.f. of A000166. - _Tom Copeland_, Sep 10 2008",
				"Row sums are: {1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, ...}. - _Roger L. Bagula_, Feb 20 2009",
				"T is related to an operational calculus connecting an infinitesimal generator for fractional integro-derivatives with the values of the Riemann zeta function at positive integers (see MathOverflow links). - _Tom Copeland_, Nov 02 2012",
				"The submatrix below the null subdiagonal is signed and row reversed A127717. The submatrix below the diagonal is A074909(n,k)s(n-k) where s(n)= -n, i.e., multiply the n-th diagonal by -n. A074909 and its reverse A135278 have several combinatorial interpretations. - _Tom Copeland_, Nov 04 2012"
			],
			"reference": [
				"Norman Biggs, Algebraic Graph Theory, 2nd ed. Cambridge University Press, 1993. p. 17.",
				"J. Riordan, An Introduction to Combinatorial Analysis, Wiley, 1958, p.184 problem 3."
			],
			"link": [
				"Problem B6, \u003ca href=\"http://amc.maa.org/a-activities/a7-problems/putnam/-pdf/2005.pdf\"\u003eThe 66th William Lowell Putnam Mathematical Competition Saturday, Dec 03 2005\u003c/a\u003e",
				"M. Bhargava, K. Kedlaya, and L. Ng, \u003ca href=\"http://amc.maa.org/a-activities/a7-problems/putnam/-pdf/2005s.pdf\"\u003eSolutions to the 66th William Lowell Putnam Mathematical Competition Saturday, Dec 03 2005\u003c/a\u003e",
				"T. Copeland, \u003ca href=\"http://mathoverflow.net/questions/111770/cycling-through-the-zeta-garden-zeta-functions-for-graphs-cycle-index-polynomi\"\u003eCycling through the Zeta Garden: Zeta functions for graphs, cycle index polynomials, and determinants\u003c/a\u003e",
				"T. Copeland, \u003ca href=\"http://mathoverflow.net/questions/111165/riemann-zeta-function-at-positive-integers-and-an-appell-sequence-of-polynomials\"\u003eRiemann zeta function at positive integers and an Appell sequence of polynomials related to fractional calculus\u003c/a\u003e"
			],
			"formula": [
				"G.f.: (x-n+1)*(x+1)^(n-1) = Sum_(k=0..n) T(n,k) x^k.",
				"T(n, k) = (1-n+k)*binomial(n, k).",
				"k-th column has o.g.f. x^k(1-(k+2)x)/(1-x)^(k+2). k-th row gives coefficients of (x-k)(x+1)^k. - _Paul Barry_, Jan 25 2004",
				"T(n,k) = Coefficientslist[Det[Table[If[i == j, x, 1], {i, 1, n}, {k, 1, n}],x]. - _Roger L. Bagula_, Feb 20 2009",
				"From _Peter Bala_, Aug 08 2011: (Start)",
				"Given a permutation p belonging to the symmetric group S_n, let fix(p) be the number of fixed points of p and sign(p) its parity. The row polynomials R(n,x) have a combinatorial interpretation as R(n,x) = (-1)^n*Sum_{permutations p in S_n} sign(p)*(-x)^(fix(p)). An example is given below.",
				"Note: The polynomials P(n,x) = Sum_{permutations p in S_n} x^(fix(p)) are the row polynomials of the rencontres numbers A008290. The integral results Integral_{x = 0..n} R(n,x) dx = n/(n+1) = Integral_{x = 0..-1} R(n,x) dx lead to the identities Sum_{p in S_n} sign(p)*(-n)^(1 + fix(p))/(1 + fix(p)) = (-1)^(n+1)*n/(n+1) = Sum_{p in S_n} sign(p)/(1 + fix(p)). The latter equality was Problem B6 in the 66th William Lowell Putnam Mathematical Competition 2005. (End)",
				"From _Tom Copeland_, Jul 26 2017: (Start)",
				"The e.g.f. in Copeland's 2008 comment implies this entry is an Appell sequence of polynomials P(n,x) with lowering and raising operators L = d/dx and R = x + d/dL log[exp(L)(1-L)] = x+1 - 1/(1-L) = x - L - L^2 - ... such that L P(n,x) = n P(n-1,x) and R P(n,x) = P(n+1,x).",
				"P(n,x) = (1-L) exp(L) x^n = (1-L) (x+1)^n = (x+1)^n - n (x+1)^(n-1) = (x+1-n)(x+1)^(n-1) = (x+s.)^n umbrally, where (s.)^n = s_n = P(n,0).",
				"The formalism of A133314 applies to the pair of entries A008290 and A055137.",
				"The polynomials of this pair P_n(x) and Q_n(x) are umbral compositional inverses; i.e., P_n(Q.(x)) = x^n = Q_n(P.(x)), where, e.g., (Q.(x))^n = Q_n(x).",
				"The exponential infinitesimal generator (infinigen) of this entry is the negated infinigen of A008290, the matrix (M) noted by Bala, related to A238363. Then e^M = [the lower triangular A008290], and e^(-M) = [the lower triangular A055137]. For more on the infinigens, see A238385. (End)",
				"From the row g.f.s corresponding to Bagula's matrix example below, the n-th row polynomial has a zero of multiplicity n-1 at x = 1 and a zero at x = -n+1. Since this is an Appell sequence dP_n(x)/dx = n P_{n-1}(x), the critical points of P_n(x) have the same abscissas as the zeros of P_{n-1}(x); therefore, x = 1 is an inflection point for the polynomials of degree \u003e 2 with P_n(1) = 0, and the one local extremum of P_n has the abscissa x = -n + 2 with the value (-n+1)^{n-1}, signed values of A000312. - _Tom Copeland_, Nov 15 2019"
			],
			"example": [
				"1; 0,1; -1,0,1; -2,-3,0,1; -3,-8,-6,0,1; ...",
				"(Bagula's matrix has a different sign convention from the list.)",
				"From _Roger L. Bagula_, Feb 20 2009: (Start)",
				"  { 1},",
				"  { 0,   1},",
				"  {-1,   0,    1},",
				"  { 2,  -3,    0,    1},",
				"  {-3,   8,   -6,    0,     1},",
				"  { 4, -15,   20,  -10,     0,    1},",
				"  {-5,  24,  -45,   40,   -15,    0,    1},",
				"  { 6, -35,   84, -105,    70,  -21,    0,   1},",
				"  {-7,  48, -140,  224,  -210,  112,  -28,   0,   1},",
				"  { 8, -63,  216, -420,   504, -378,  168, -36,   0, 1},",
				"  {-9,  80, -315,  720, -1050, 1008, -630, 240, -45, 0, 1}",
				"(End)",
				"R(3,x) = (-1)^3*Sum_{permutations p in S_3} sign(p)*(-x)^(fix(p)).",
				"    p   | fix(p) | sign(p) | (-1)^3*sign(p)*(-x)^fix(p)",
				"========+========+=========+===========================",
				"  (123) |    3   |    +1   |      x^3",
				"  (132) |    1   |    -1   |       -x",
				"  (213) |    1   |    -1   |       -x",
				"  (231) |    0   |    +1   |       -1",
				"  (312) |    0   |    +1   |       -1",
				"  (321) |    1   |    -1   |       -x",
				"========+========+=========+===========================",
				"                           | R(3,x) = x^3 - 3*x - 2",
				"- _Peter Bala_, Aug 08 2011"
			],
			"mathematica": [
				"M[n_] := Table[If[i == j, x, 1], {i, 1, n}, {j, 1, n}]; a = Join[{{1}}, Flatten[Table[CoefficientList[Det[M[n]], x], {n, 1, 10}]]] (* _Roger L. Bagula_, Feb 20 2009 *)",
				"t[n_, k_] := (k-n+1)*Binomial[n, k]; Table[t[n, k], {n, 0, 10}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Nov 29 2013, after Pari *)"
			],
			"program": [
				"(PARI) T(n,k)=(1-n+k)*if(k\u003c0 || k\u003en,0,n!/k!/(n-k)!)"
			],
			"xref": [
				"Cf. A005563, A005564 (absolute values of columns 1, 2).",
				"Cf. A008290, A133314, A238363, A238385.",
				"Cf. A000312."
			],
			"keyword": "sign,tabl",
			"offset": "0,7",
			"author": "_Christian G. Bower_, Apr 25 2000",
			"ext": [
				"Additional comments from _Michael Somos_, Jul 04 2002"
			],
			"references": 9,
			"revision": 90,
			"time": "2019-11-16T01:38:06-05:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}