{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A336882",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 336882,
			"data": "1,3,5,15,7,21,35,105,9,27,45,135,63,189,315,945,11,33,55,165,77,231,385,1155,99,297,495,1485,693,2079,3465,10395,13,39,65,195,91,273,455,1365,117,351,585,1755,819,2457,4095,12285,143,429,715,2145,1001",
			"name": "a(0) = 1; for k \u003e= 0, 0 \u003c= i \u003c 2^k, a(2^k + i) = m_k * a(i), where m_k is the least odd number not in terms 0..2^k - 1.",
			"comment": [
				"A permutation of the odd numbers.",
				"Every positive integer, m, is the product of a unique subset of the terms of A050376. The members of the subset are often known as the Fermi-Dirac factors of m. In this sequence, the odd numbers appear lexicographically according to their Fermi-Dirac factors (with those factors listed in decreasing order). The equivalent sequence for all positive integers is A052330.",
				"The sequence has a conditional exponential identity shown in the formula section. This relies on the offset being 0, as in related sequences, notably A019565 and A052330."
			],
			"link": [
				"Sean A. Irvine, \u003ca href=\"https://github.com/archmageirvine/joeis/blob/master/src/irvine/oeis/a336/A336882.java\"\u003eJava program\u003c/a\u003e (github)"
			],
			"formula": [
				"a(2^k) = min({ 2*m+1 : m \u003e= 0, 2*m+1 \u003c\u003e a(j), 0 \u003c= j \u003c 2^k }) = A062090(k+2).",
				"If x AND y = 0, a(x+y) = a(x) * a(y), where AND denotes the bitwise operation, A004198(.,.).",
				"a(x XOR y) = A059897(a(x), a(y)), where XOR denotes bitwise exclusive-or, A003987(.,.).",
				"a(x OR y) = A059896(a(x), a(y)), where OR denotes the bitwise operation, A003986(.,.)."
			],
			"example": [
				"a(0) = 1, as specified explicitly.",
				"m_0 = 3, the least odd number not in terms 0..0.",
				"So a(1) = a(2^0 + 0) = m_0 * a(0) = 3 * 1 = 3.",
				"m_1 = 5, the least odd number not in terms 0..1.",
				"So a(2) = a(2^1 + 0) = m_1 * a(0) = 5 * 1 = 5;",
				"and a(3) = a(2^1 + 1) = m_1 * a(1) = 5 * 3 = 15.",
				"The initial terms are tabulated below, equated with the product of their Fermi-Dirac factors to exhibit the lexicographic order. We start with 1, since 1 is factored as the empty product and the empty list is first in lexicographic order.",
				"   n     a(n)",
				"   0    1,",
				"   1    3 = 3,",
				"   2    5 = 5,",
				"   3   15 = 5 * 3,",
				"   4    7 = 7,",
				"   5   21 = 7 * 3,",
				"   6   35 = 7 * 5,",
				"   7  105 = 7 * 5 * 3,",
				"   8    9 = 9,",
				"   9   27 = 9 * 3,",
				"  10   45 = 9 * 5,",
				"  11  135 = 9 * 5 * 3,",
				"  12   63 = 9 * 7."
			],
			"xref": [
				"Permutation of A005408.",
				"Subsequence of A052330.",
				"Subsequences: A062090, A332382 (squarefree terms).",
				"A003986, A003987, A004198, A059896, A059897 are used to express relationship between terms of this sequence.",
				"Cf. A019565, A050376."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Peter Munn_, Aug 16 2020",
			"references": 2,
			"revision": 29,
			"time": "2020-09-20T17:02:53-04:00",
			"created": "2020-09-19T23:16:57-04:00"
		}
	]
}