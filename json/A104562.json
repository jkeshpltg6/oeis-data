{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A104562",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 104562,
			"data": "1,-1,1,0,-2,1,1,1,-3,1,-1,2,3,-4,1,0,-4,2,6,-5,1,1,2,-9,0,10,-6,1,-1,3,9,-15,-5,15,-7,1,0,-6,3,24,-20,-14,21,-8,1,1,3,-18,-6,49,-21,-28,28,-9,1,-1,4,18,-36,-35,84,-14,-48,36,-10,1,0,-8,4,60,-50,-98,126,6,-75,45,-11,1,1,4,-30,-20,145,-36,-210,168,45,-110,55",
			"name": "Inverse of the Motzkin triangle A064189.",
			"comment": [
				"Or, triangle read by rows: T(0, 0) = 1; for n \u003e= 1 T(n, k) is the coefficient of x^k in the monic characteristic polynomial of the n X n tridiagonal matrix with 1's on the main, sub- and superdiagonal (0 \u003c= k \u003c= n). The characteristic polynomial has a root 1 + 2*cos(Pi/(n + 1)). - _Gary W. Adamson_, Nov 19 2006",
				"Row sums have g.f. 1/(1 + x^2); diagonal sums are (-1)^n. Riordan array (1/(1 + x + x^2), x/(1 + x + x^2)).",
				"Or, triangle read by rows in which row n gives coefficients of characteristic polynomial of the n X n tridiagonal matrix with 1's on the main diagonal and -1's on the two adjacent diagonals. For example: M(3) = {{1, -1, 0}, {-1, 1, -1}, {0, -1, 1}}. - _Roger L. Bagula_, Mar 15 2008",
				"Subtriangle of the triangle given by [0,-1,1,-1,0,0,0,0,0,0,0,...) DELTA [1,0,0,0,0,0,0,0,...] where DELTA is the operator defined in A084938. - _Philippe Deléham_, Jan 27 2010",
				"Triangle of coefficients of Chebyshev's S(n, x-1) polynomials (exponents of x in increasing order). - _Philippe Deléham_, Feb 19 2012"
			],
			"reference": [
				"Anthony Ralston and Philip Rabinowitz, A First Course in Numerical Analysis, 1978, ISBN 0070511586, see p. 256."
			],
			"link": [
				"Paul Barry, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL15/Barry4/bern2.html\"\u003eRiordan-Bernstein Polynomials, Hankel Transforms and Somos Sequences\u003c/a\u003e, Journal of Integer Sequences, Vol. 15 2012, #12.8.2.",
				"A. Luzón, D. Merlini, M. A. Morón, R. Sprugnoli, \u003ca href=\"http://dx.doi.org/10.1016/j.dam.2014.03.005\"\u003eComplementary Riordan arrays\u003c/a\u003e, Discrete Applied Mathematics, 172 (2014) 75-87."
			],
			"formula": [
				"T(n, k) = Sum_{j=0..n} (-1)^(k-j)*(-1)^((n-j)/2) C((n+j)/2, j)(1+(-1)^(n+j))C(j, k)/2.",
				"T(n,k) = (-1)^(n-k)*A101950(n,k). - _Philippe Deléham_, Feb 19 2012",
				"T(n,k) = T(n-1,k-1) - T(n-1,k) - T(n-2,l). - _Philippe Deléham_, Feb 19 2012",
				"A104562*A007318 = A049310 as infinite lower triangular matrices. - _Philippe Deléham_, Feb 19 2012",
				"G.f.: 1/(1+x+x^2-y*x). - _Philippe Deléham_, Feb 19 2012",
				"T(n, k) = (-1)^(n - k)*C(n, k)*hypergeom([(k - n)/2, (k - n + 1)/2], [-n], 4)) for n \u003e= 1. - _Peter Luschny_, Apr 25 2016"
			],
			"example": [
				"Triangle starts:",
				"[0]  1;",
				"[1] -1,  1;",
				"[2]  0, -2,   1;",
				"[3]  1,  1,  -3,   1;",
				"[4] -1,  2,   3,  -4,   1;",
				"[5]  0, -4,   2,   6,  -5,   1;",
				"[6]  1,  2,  -9,   0,  10,  -6,   1;",
				"[7] -1,  3,   9, -15,  -5,  15,  -7,  1;",
				"[8]  0, -6,   3,  24, -20, -14,  21, -8,  1;",
				"[9]  1,  3, -18,  -6,  49, -21, -28, 28, -9, 1.",
				".",
				"Triangle [0,-1,1,-1,0,0,0,0,0,...] DELTA [1,0,0,0,0,0,0,0,...] begins : 1 ; 0,1 ; 0,-1,1 ; 0,0,-2,1 ; 0,1,1,-3,1 ; 0,-1,2,3,-4,1 ; ... - _Philippe Deléham_, Jan 27 2010"
			],
			"maple": [
				"with(linalg): m:=proc(i,j) if abs(i-j)\u003c=1 then 1 else 0 fi end: T:=(n,k)-\u003ecoeff(charpoly(matrix(n,n,m),x),x,k): 1; for n from 1 to 12 do seq(T(n,k),k=0..n) od; # yields sequence in triangular form",
				"# Alternatively:",
				"T := (n,k) -\u003e `if`(n=0,1,(-1)^(n-k)*binomial(n,k)*hypergeom([(k-n)/2, (k-n+1)/2], [-n], 4)): seq(seq(simplify(T(n,k)),k=0..n),n=0..10); # _Peter Luschny_, Apr 25 2016"
			],
			"program": [
				"(Sage)",
				"@CachedFunction",
				"def A104562(n,k):",
				"    if n\u003c 0: return 0",
				"    if n==0: return 1 if k == 0 else 0",
				"    return A104562(n-1,k-1)-A104562(n-2,k)-A104562(n-1,k)",
				"for n in (0..9): [A104562(n,k) for k in (0..n)] # _Peter Luschny_, Nov 20 2012",
				"(Sage) # Alternatively as coefficients of polynomials:",
				"def S(n,x):",
				"    if n==0: return 1",
				"    if n==1: return x-1",
				"    return (x-1)*S(n-1,x)-S(n-2,x)",
				"for n in (0..7): print(S(n,x).list()) # _Peter Luschny_, Jun 23 2015"
			],
			"xref": [
				"Apart from signs identical to A101950.",
				"Cf. A125090."
			],
			"keyword": "easy,sign,tabl",
			"offset": "0,5",
			"author": "_Paul Barry_, Mar 15 2005",
			"ext": [
				"Edited by _N. J. A. Sloane_, Apr 10 2008",
				"Typo correction in the _Roger L. Bagula_ comment and Mathematica section by _Wolfdieter Lang_, Nov 22 2011"
			],
			"references": 13,
			"revision": 53,
			"time": "2021-05-23T10:20:41-04:00",
			"created": "2005-04-09T03:00:00-04:00"
		}
	]
}