{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A066437",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 66437,
			"data": "1,1,3,3,12,3,12,12,12,12,18,12,28,12,15,15,72,12,72,15,28,18,36,15,42,28,72,28,72,15,72,72,42,72,72,28,252,72,72,72,90,28,252,42,72,36,72,72,252,42,252,72,252,72,90,72,252,72,90,72,168,72,252,252,168,42",
			"name": "a(n) = max_{k} {T(n,k)} where T(n,k) is the \"phi/sigma tug-of-war sequence with seed n\" defined by T(n,1) = phi(n), T(n,2) = sigma(phi(n)), T(n,3) = phi(sigma(phi(n))), ..., T(n,k) = phi(T(n,k-1)) if k is odd and = sigma(T(n,k-1)) if k is even.",
			"comment": [
				"Conjecture: a(n) is always finite; i.e. the sequence {T(n,k)} is eventually periodic for every n.",
				"a(n) \u003e= sigma(phi(n)) \u003e= phi(n); since phi(n) -\u003e infinity with n, so does a(n).",
				"Sequence is otherwise like A096864, except here the initial value n where the iteration is started from is ignored. - _Antti Karttunen_, Dec 06 2017"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A066437/b066437.txt\"\u003eTable of n, a(n) for n = 1..16384\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A096864(A062402(n)). - _Antti Karttunen_, Dec 06 2017"
			],
			"example": [
				"For n=11, the sequence is 11, 10, 18, 6, 12, 4, 7, 6, 12, ..., whose maximum value is 18. Hence a(11) = 18."
			],
			"mathematica": [
				"a[ n_ ] := For[ m=n; max=0; seq={}, True, AppendTo[ seq, m ], If[ (m=DivisorSigma[ 1, EulerPhi[ m ] ])\u003emax, max=m ]; If[ MemberQ[ seq, m ], Return[ max ] ] ]"
			],
			"program": [
				"(Scheme) (define (A066437 n) (let loop ((visited (list n)) (i 1) (m 1)) (let ((next ((if (odd? i) A000010 A000203) (car visited)))) (cond ((member next (reverse visited)) =\u003e (lambda (start_of_cyclic_part) (cond ((even? (length start_of_cyclic_part)) (max m next)) (else (loop (cons next visited) (+ 1 i) (max m next)))))) (else (loop (cons next visited) (+ 1 i) (max m next))))))) ;; _Antti Karttunen_, Dec 06 2017"
			],
			"xref": [
				"Cf. A000010, A000203, A036840, A036845, A062402, A096864."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Joseph L. Pe_, Jan 08 2002",
			"ext": [
				"Edited by _Dean Hickerson_, Jan 18 2002"
			],
			"references": 4,
			"revision": 15,
			"time": "2017-12-06T20:08:01-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}