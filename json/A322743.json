{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A322743",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 322743,
			"data": "8,4,6,15,21,45,111,261,1605,1995,4935,8295,69825,268155,550725,4574955,13996605,12024855,39867135,398467245,1698754365,16351800465,72026408685,120554434875",
			"name": "Least composite such that complementing one bit in its binary representation at a time produces exactly n primes.",
			"formula": [
				"From _Chai Wah Wu_, Jan 03 2019: (Start)",
				"a(n) \u003e= 2^n+1, if it exists. a(n) is odd for n \u003e 2, if it exists.",
				"It is clear these are true for n \u003c= 2. Suppose n \u003e 2. If a(n) is even, then complementing any bits that is not LSB or MSB will result in an even nonprime number. If a(n) is odd, then complementing the LSB will result in an even nonprime number. So either case shows that a(n) has n+1 or more binary bits. It also shows that a(n) must be odd.",
				"Conjecture: a(n) mod 10 == 5 for n \u003e 7. (End)"
			],
			"example": [
				"a(1) = 4 because 4 in base 2 is 100 and 000 is 0, 110 is 6 and 101 is 5: hence only one prime.",
				"a(2) = 6 because 6 in base 2 is 110 and 010 is 2, 100 is 4 and 111 is 7: hence two primes."
			],
			"maple": [
				"a:= proc(n) local k; for k from 2^n+1 while isprime(k) or n\u003c\u003eadd(",
				"      `if`(isprime(Bits[Xor](k, 2^j)), 1, 0), j=0..ilog2(k)) do od; k",
				"    end:",
				"seq(a(n), n=0..12);  # _Alois P. Heinz_, Jan 03 2019"
			],
			"program": [
				"(Python)",
				"from sympy import isprime",
				"def A322743(n):",
				"    i = 4 if n \u003c= 1 else 2**n+1",
				"    j = 1 if n \u003c= 2 else 2",
				"    while True:",
				"        if not isprime(i):",
				"            c = 0",
				"            for m in range(len(bin(i))-2):",
				"                if isprime(i^(2**m)):",
				"                    c += 1",
				"                if c \u003e n:",
				"                    break",
				"            if c == n:",
				"                return i",
				"        i += j # _Chai Wah Wu_, Jan 03 2019"
			],
			"xref": [
				"Cf. A002808, A137985."
			],
			"keyword": "nonn,base",
			"offset": "0,1",
			"author": "_Paolo P. Lava_, Dec 24 2018",
			"ext": [
				"Definition clarified by _Chai Wah Wu_, Jan 03 2019",
				"a(19) added and a(0) corrected by _Rémy Sigrist_, Jan 03 2019",
				"a(20)-a(23) from _Giovanni Resta_, Jan 03 2019"
			],
			"references": 1,
			"revision": 49,
			"time": "2019-01-03T15:12:45-05:00",
			"created": "2018-12-31T15:26:52-05:00"
		}
	]
}