{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A234691",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 234691,
			"data": "119,36,93,109,46,107,123,39,127,111,4727,4644,4701,4717,4654,4715,4731,4647,4735,4719,12023,11940,11997,12013,11950,12011,12027,11943,12031,12015,14071,13988,14045,14061,13998,14059,14075,13991,14079,14063,6007,5924,5981,5997",
			"name": "Decimal value of the bitmap of active segments in 7-segment display of the number n, variant 1: bits 0-6 refer to segments from top to bottom, left to right.",
			"comment": [
				"The bits 0-6 are assigned to the segments as follows: top (bit 0), upper left, upper right, middle, lower left, lower right, bottom (bit 6). Other conventions are common in engineering (as well for the segment-to-bit correspondence as for the glyphs), see sequence A234692 (the so-called ABCDEF coding), Wikipedia and the Example for a(7).",
				"For n \u003e 9, the decimal representation of n is encoded using a group of 7 bits for each digit, e.g., a(10) = a(1)*2^7 + a(0).",
				"The sequence could have been extended differently to indices \u003e 9, for example, by coding 7-segment representations of upper- and/or lowercase letters, as used for hexadecimal and base-64 representation.",
				"The Hamming weight A000120 of the terms of this sequence yields the count of lit segments, A010371(n) = A000120(A234691(n)) = A000120(A234692(n)). For that sequence, 4 other variants are in OEIS, depending on the number of segments used to represent digits 6, 7 and 9: A063720 (6', 7', 9'), A277116 (7', 9'), A074458 (9') and A006942 (7'), where x' means that the \"sans serif\" variant (one segment less than here) is used for digit x. - _M. F. Hasler_, Jun 17 2020"
			],
			"link": [
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Seven-segment_display\"\u003eSeven-segment display\u003c/a\u003e."
			],
			"formula": [
				"a(n) = a(n%10) + a(floor(n/10))*2^7 for n \u003e 9, where % is the modulo operator. - _M. F. Hasler_, Jun 17 2020"
			],
			"example": [
				"The digits are assumed to be rendered as follows:",
				"   _     _  _       _   _   _   _   _",
				"  | | |  _| _| |_| |_  |_  | | |_| |_|",
				"  |_| | |_  _|   |  _| |_|   | |_|  _|",
				"For example, a(7) = 39 = 2^0 + 2^1 + 2^2 + 2^5, because the nonzero bits are",
				"   _  : bit 0,",
				"  | | : bits 1+2,",
				"    | : bit 5,",
				"while bits 3 (central '-'), 4 (lower left '|') and 6 (bottom '_') remain 0.",
				"For the digit '7', the \"sans serif\" representation (without the upper left '|') is also very common; it would correspond to a(7) = 37. Similarly, digits 6 and 9 are sometimes represented without the top resp. bottom bar, see comments."
			],
			"program": [
				"(PARI) /* Bitmaps computed from 7-character strings. Extended beyond 9 with characters most frequently used to code hexadecimal digits A-F, chessboard coordinates a-h, and other letters. Alternate glyphs used more or less frequently: sans serif \" 6 6666\", \"7 7  7 \", \"9999 9 \"; \"a aaaaa\", B same as 8, \"eeeee e\", g same as 9, H same as X, \"i   i i\" or \"     i \", \"J J JJJ\" or \"J J  JJ\", S same as 5, \"TT  T  \", \" YYY Y \". Note: Z below is the same as 2! */",
				"bitmaps = { apply( s2b = s-\u003esum(i=1,#s=Vec(s), if(s[i]\u003e\" \",2^(i-1))), [\"000 000\", \"  1  1 \", \"2 222 2\", \"3 33 33\", \" 444 4 \", \"55 5 55\", \"66 6666\", \"777  7 \", \"8888888\", \"9999 99\", \"AAAAAA \", \" b bbbb\", \"CC   CC\", \"  ddddd\",",
				"  \"EE EEEE\", \"FF FF  \", \"GG  GGG\", \" h hhh \", \" I  I  \", \"  J  JJ\", \"KK KKK \",",
				"  \" L  L L\", \"MMM MM \", \"   nnn \", \"   oooo\", \"PPPPP  \", \"qqqq q \", \"   rr  \", \"   s ss\", \" t tt t\", \"    uuu\", \" VV VVV\", \" XXXXX \", \" YYY YY\", \"Z ZZZ Z\"])} \\\\ - Extended to letters by _M. F. Hasler_, Jun 16 2020",
				"apply( {A234691(n)=bitmaps[n%10+1]+if(n\u003e9,self()(n\\10)\u003c\u003c7)}, [0..99]) \\\\ Extended to n \u003e 9 by _M. F. Hasler_, Jun 17 2020"
			],
			"xref": [
				"Cf. A234692 for the \"abcdefg\" coding variant (clockwise from top and ending with bit 6 for the central \"-\").",
				"Cf. A010371 (number of segments lit), and variants A063720, A277116, A074458: see comments.",
				"Cf. A000120 (Hamming weight)."
			],
			"keyword": "nonn,base",
			"offset": "0,1",
			"author": "_M. F. Hasler_, Dec 29 2013",
			"ext": [
				"a(6) corrected thanks to _Kevin Ryde_, _M. F. Hasler_, Jun 16 2020",
				"Edited and extended to n \u003e 9 by _M. F. Hasler_, Jun 17 2020"
			],
			"references": 11,
			"revision": 37,
			"time": "2020-12-30T18:02:10-05:00",
			"created": "2014-01-01T07:56:29-05:00"
		}
	]
}