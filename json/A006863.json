{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006863",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6863,
			"id": "M5150",
			"data": "1,24,240,504,480,264,65520,24,16320,28728,13200,552,131040,24,6960,171864,32640,24,138181680,24,1082400,151704,5520,1128,4455360,264,12720,86184,13920,1416,6814407600,24",
			"name": "Denominator of B_{2n}/(-4n), where B_m are the Bernoulli numbers.",
			"comment": [
				"Carmichael defines lambda(n) to be the exponent of the group U(n) of units of the integers mod n. He shows that given m there is a number lambda^*(m) such that lambda(n) divides m if and only if n divides lambda^*(m). He gives a formula for lambda^*(m), equivalent to the one I've quoted for even m. (We have lambda^*(m)=2 for any odd m.) The present sequence gives the values of lambda^*(2m) for positive integers m. - _Peter J. Cameron_, Mar 25 2002",
				"(-1)^n*B_{2n}/(-4n) = Integral_{t\u003e=0} t^(2n-1)/(exp(2*Pi*t) - 1)dt. - _Benoit Cloitre_, Apr 04 2002",
				"Michael Lugo (see link) conjectures, and Peter McNamara proves, that a(n) = gcd_{ primes p \u003e 2n+1 } (p^(2n) - 1). - _Tanya Khovanova_, Feb 21 2009 [edited by _Charles R Greathouse IV_, Dec 03 2014]"
			],
			"reference": [
				"Bruce Berndt, Ramanujan's Notebooks Part II, Springer-Verlag; see Integrals and Asymptotic Expansions, p. 220.",
				"F. Hirzebruch et al., Manifolds and Modular Forms, Vieweg, 2nd ed. 1994, p. 130.",
				"J. W. Milnor and J. D. Stasheff, Characteristic Classes, Princeton, 1974, p. 286.",
				"Douglas C. Ravenel, Complex cobordism theory for number theorists, Lecture Notes in Mathematics, 1326, Springer-Verlag, Berlin-New York, 1988, pp. 123-133.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"R. C. Vaughan and T. D. Wooley, Waring's problem: a survey, pp. 285-324 of Surveys in Number Theory (Urbana, May 21, 2000), ed. M. A. Bennett et al., Peters, 2003. (The function K(2n), see p. 303.)"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A006863/b006863.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"P. J. Cameron and D. A. Preece, \u003ca href=\"http://www.maths.qmul.ac.uk/~pjc/csgnotes/lambda.pdf\"\u003eNotes on primitive lambda-roots\u003c/a\u003e",
				"R. D. Carmichael, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9904-1910-01892-9\"\u003eNote on a new number theory function\u003c/a\u003e, Bull. Amer. Math. Soc. 16 (1909-10), 232-238.",
				"G. Everest, Y. Puri and T. Ward, \u003ca href=\"https://arxiv.org/abs/math/0204173\"\u003eInteger sequences counting periodic points\u003c/a\u003e, arXiv:math/0204173 [math.NT], 2002.",
				"Michael Lugo, \u003ca href=\"http://godplaysdice.blogspot.com/2008/05/little-number-theory-problem.html\"\u003eA little number theory problem\u003c/a\u003e (2008)",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/EisensteinSeries.html\"\u003eEisenstein Series.\u003c/a\u003e",
				"\u003ca href=\"/index/Be#Bernoulli\"\u003eIndex entries for sequences related to Bernoulli numbers.\u003c/a\u003e"
			],
			"formula": [
				"B_{2k}/(4k) = -(1/2)*zeta(1-2k). For n \u003e 0, a(n) = gcd k^L (k^{2n}-1) where k ranges over all the integers and L is as large as necessary.",
				"Product of 2^{a+2} (where 2^a exactly divides 2*n) and p^{a+1} (where p is an odd prime such that p-1 divides 2*n and p^a exactly divides 2*n). - _Peter J. Cameron_, Mar 25 2002"
			],
			"maple": [
				"1,seq(denom(bernoulli(2*n)/(-4*n)), n=1 .. 100); # _Robert Israel_, Dec 03 2014"
			],
			"mathematica": [
				"a[n_] := Denominator[BernoulliB[2n]/(-4n)]; Table[a[n], {n, 0, 31}] (* _Jean-François Alcover_, Mar 20 2011 *)"
			],
			"program": [
				"(PARI) a(n) = if (n == 0, 1, denominator(bernfrac(2*n)/(-4*n))); \\\\ _Michel Marcus_, Sep 10 2013",
				"(MAGMA) [1] cat [Denominator(Bernoulli(2*n)/(-4*n)):n in [1..35]]; // _G. C. Greubel_, Sep 19 2019",
				"(Sage) [1]+[denominator(bernoulli(2*n)/(-4*n)) for n in (1..35)] # _G. C. Greubel_, Sep 19 2019",
				"(GAP) Concatenation([1], List([1..35], n-\u003e DenominatorRat(Bernoulli(2*n)/(-4*n)) )); # _G. C. Greubel_, Sep 19 2019"
			],
			"xref": [
				"Numerators are A001067.",
				"Cf. A000367/A002445, A002322, A079612."
			],
			"keyword": "nonn,easy,frac,nice",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, _Jeffrey Shallit_, _Simon Plouffe_",
			"ext": [
				"Thanks to _Michael Somos_ for helpful comments."
			],
			"references": 13,
			"revision": 64,
			"time": "2019-09-20T09:01:16-04:00",
			"created": "1994-04-28T03:00:00-04:00"
		}
	]
}