{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A058491",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 58491,
			"data": "1,5,-5,9,-14,19,-34,55,-69,104,-164,209,-283,413,-539,712,-968,1248,-1642,2167,-2731,3526,-4592,5736,-7244,9255,-11520,14378,-18018,22238,-27556,34132,-41701,51184,-62900,76323,-92771,113002,-136421,164673,-198842",
			"name": "Coefficients of replicable function number 12c.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Cubic AGM theta functions: a(q) (see A004016), b(q) (A005928), c(q) (A005882)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A058491/b058491.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"D. Alexander, C. Cummins, J. McKay and C. Simons, \u003ca href=\"http://oeis.org/A007242/a007242_1.pdf\"\u003eCompletely Replicable Functions\u003c/a\u003e, LMS Lecture Notes, 165, ed. Liebeck and Saxl (1992), 87-98, annotated and scanned copy.",
				"D. Ford, J. McKay and S. P. Norton, \u003ca href=\"http://dx.doi.org/10.1080/00927879408825127\"\u003eMore on replicable functions\u003c/a\u003e, Commun. Algebra 22, No. 13, 5175-5193 (1994).",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"\u003ca href=\"/index/Mat#McKay_Thompson\"\u003eIndex entries for McKay-Thompson series for Monster simple group\u003c/a\u003e"
			],
			"formula": [
				"Expansion of a(x) / (psi(x) * psi(x^3)) in powers of x where psi() is a Ramanujan theta function and a() is a cubic AGM theta function. - _Michael Somos_, Aug 20 2012",
				"Expansion of q^(1/2) * (eta(q)^3 + 9 * q * eta(q^9)^3) * eta(q) / (eta(q^2) * eta(q^6))^2 in powers of q. - _Michael Somos_, Aug 20 2012",
				"a(n) = A186930(2*n - 1) = A187045(2*n - 1). - _Michael Somos_, Aug 20 2012"
			],
			"example": [
				"G.f. = 1 + 5*x - 5*x^2 + 9*x^3 - 14*x^4 + 19*x^5 - 34*x^6 + 55*x^7 - 69*x^8 + ...",
				"T12c = 1/q + 5*q - 5*q^3 + 9*q^5 - 14*q^7 + 19*q^9 - 34*q^11 + 55*q^13 - ..."
			],
			"mathematica": [
				"a[n_]:= SeriesCoefficient[(QPochhammer[x]^3 + 9*x*QPochhammer[x^9]^3)* QPochhammer[x]/(QPochhammer[x^2]*QPochhammer[x^6])^2, {x, 0, n}]; (* _Michael Somos_, Sep 14 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( (eta(x + A)^3 + 9 * x * eta(x^9 + A)^3) * eta(x + A) / (eta(x^2 + A) * eta(x^6 + A))^2, n))}; /* _Michael Somos_, Aug 20 2012 */",
				"(PARI) { my(q='q+O('q^66)); Vec( (eta(q)^3 + 9 * q * eta(q^9)^3) * eta(q) / (eta(q^2) * eta(q^6))^2 ) } \\\\ _Joerg Arndt_, Apr 16 2017"
			],
			"xref": [
				"Cf. A000521, A007240, A014708, A007241, A007267, A045478, etc.",
				"Cf. A186930, A187045."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Nov 27 2000",
			"ext": [
				"Name changed by _N. J. A. Sloane_, Jun 10 2015 at the suggestion of _Yang-Hui He_"
			],
			"references": 4,
			"revision": 33,
			"time": "2021-03-12T22:24:42-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}