{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A048788",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 48788,
			"data": "0,1,2,3,8,11,30,41,112,153,418,571,1560,2131,5822,7953,21728,29681,81090,110771,302632,413403,1129438,1542841,4215120,5757961,15731042,21489003,58709048,80198051,219105150,299303201,817711552,1117014753",
			"name": "a(2n+1) = a(2n) + a(2n-1), a(2n) = 2*a(2n-1) + a(2n-2); a(n) = n for n = 0, 1.",
			"comment": [
				"Numerators of continued fraction convergents to sqrt(3) - 1 (A160390). See A002530 for denominators. - _N. J. A. Sloane_, Dec 17 2007",
				"Convergents are 1, 2/3, 3/4, 8/11, 11/15, 30/41, 41/56, 112/153, ... - _Clark Kimberling_, Sep 21 2013",
				"A strong divisibility sequence, that is gcd(a(n),a(m)) = a(gcd(n,m)) for all positive integers n and m. - _Peter Bala_, Jun 06 2014"
			],
			"reference": [
				"Russell Lyons, A bird's-eye view of uniform spanning trees and forests, in Microsurveys in Discrete Probability, AMS, 1998."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A048788/b048788.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"P. Bala, \u003ca href=\"/A243469/a243469_1.pdf\"\u003eNotes on 2-periodic continued fractions and Lehmer sequences\u003c/a\u003e",
				"Marcia Edson, Scott Lewis and Omer Yayenie, \u003ca href=\"http://www.emis.de/journals/INTEGERS/papers/l32/l32.Abstract.html\"\u003eThe k-periodic Fibonacci sequence and an extended Binet's formula\u003c/a\u003e, INTEGERS 11 (2011) #A32.",
				"D. Panario, M. Sahin, Q. Wang, \u003ca href=\"http://www.emis.de/journals/INTEGERS/papers/n78/n78.Abstract.html\"\u003eA family of Fibonacci-like conditional sequences\u003c/a\u003e, INTEGERS, Vol. 13, 2013, #A78.",
				"\u003ca href=\"/index/Di#divseq\"\u003eIndex to divisibility sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,4,0,-1)."
			],
			"formula": [
				"G.f.: x*(1+2*x-x^2)/(1-4*x^2+x^4). - _Paul Barry_, Sep 18 2009",
				"a(n) = 4*a(n-2) - a(n-4). - _Vincenzo Librandi_, Dec 10 2013",
				"a(2*n-1) = A001835(n); a(2*n) = 2*A001353(n). - _Peter Bala_, Jun 06 2014",
				"From _Gerry Martens_, Jul 11 2015: (Start)",
				"Interspersion of 2 sequences [a1(n-1),a0(n)] for n\u003e0:",
				"a0(n) = ((3+sqrt(3))*(2-sqrt(3))^n-((-3+sqrt(3))*(2+sqrt(3))^n))/6.",
				"a1(n) = 2*Sum_{i=1..n} a0(i). (End)",
				"a(n) = ((r + (-1)^n/r)*s^n/2^(n/2) - (1/r + (-1)^n*r)*2^(n/2)/s^n)*sqrt(6)/12, where r = 1 + sqrt(2), s = 1 + sqrt(3). - _Vladimir Reshetnikov_, May 11 2016",
				"a(n) = 2*ChebyshevU(n-1, 2) if n is even and ChebyshevU(n, 2) - ChebyshevU(n-1, 2) if n in odd. - _G. C. Greubel_, Dec 23 2019",
				"a(n) = -(-1)^n*a(-n) for all n in Z. - _Michael Somos_, Sep 17 2020"
			],
			"maple": [
				"seq( simplify( `if`(`mod`(n,2)=0, 2*ChebyshevU((n-2)/2, 2), ChebyshevU((n-1)/2, 2) - ChebyshevU((n-3)/2, 2)) ), n=0..40); # _G. C. Greubel_, Dec 23 2019"
			],
			"mathematica": [
				"Numerator[NestList[(2/(2 + #))\u0026, 0, 40]] (* _Vladimir Joseph Stephan Orlovsky_, Apr 13 2010 *)",
				"CoefficientList[Series[x(1+2x-x^2)/(1-4x^2+x^4), {x, 0, 40}], x] (* _Vincenzo Librandi_, Dec 10 2013 *)",
				"a0[n_]:= ((3+Sqrt[3])*(2-Sqrt[3])^n-((-3+Sqrt[3])*(2+Sqrt[3])^n))/6 // Simplify",
				"a1[n_]:= 2*Sum[a0[i], {i, 1, n}]",
				"Flatten[MapIndexed[{a1[#-1],a0[#]}\u0026,Range[20]]] (* _Gerry Martens_, Jul 10 2015 *)",
				"Round@Table[With[{r=1+Sqrt[2], s=1+Sqrt[3]}, ((r + (-1)^n/r) s^n/2^(n/2) - (1/r + (-1)^n r) 2^(n/2)/s^n) Sqrt[6]/12], {n, 0, 20}] (* or *) LinearRecurrence[ {0,4,0,-1}, {0,1,2,3}, 40] (* _Vladimir Reshetnikov_, May 11 2016 *)",
				"Table[If[EvenQ[n], 2*ChebyshevU[(n-2)/2, 2], ChebyshevU[(n-1)/2, 2] - ChebyshevU[(n-3)/2, 2]], {n, 0, 40}] (* _G. C. Greubel_, Dec 23 2019 *)"
			],
			"program": [
				"(MAGMA) I:=[0,1,2,3]; [n le 4 select I[n] else 4*Self(n-2)-Self(n-4): n in [1..40]]; // _Vincenzo Librandi_, Dec 10 2013",
				"(PARI) main(size)=v=vector(size); v[1]=0;v[2]=1;v[3]=2;v[4]=3;for(i=5, size, v[i]=4*v[i-2] - v[i-4]); v; \\\\ _Anders Hellström_, Jul 11 2015",
				"(PARI) a=vector(50); a[1]=1; a[2]=2; for(n=3, #a, if(n%2==1, a[n]=a[n-1]+a[n-2], a[n]=2*a[n-1]+a[n-2])); concat(0, a) \\\\ _Colin Barker_, Jan 30 2016",
				"(PARI) a(n)=([0,1,0,0;0,0,1,0;0,0,0,1;-1,0,4,0]^n*[0;1;2;3])[1,1] \\\\ _Charles R Greathouse IV_, Mar 16 2017",
				"(PARI) apply( {A048788(n)=imag((2+quadgen(12))^(n\\/2)*if(bittest(n, 0), quadgen(12)-1, 2))}, [0..30]) \\\\ _M. F. Hasler_, Nov 04 2019",
				"(PARI) {a(n) = my(s=1,m=n); if(n\u003c0,s=-(-1)^n; m=-n); polcoeff(x*(1+2*x-x^2)/(1-4*x^2+x^4) + x*O(x^m), m)*s}; /* _Michael Somos_, Sep 17 2020 */",
				"(Sage)",
				"@CachedFunction",
				"def a(n):",
				"    if (mod(n,2)==0): return 2*chebyshev_U((n-2)/2, 2)",
				"    else: return chebyshev_U((n-1)/2, 2) - chebyshev_U((n-3)/2, 2)",
				"[a(n) for n in (0..40)] # _G. C. Greubel_, Dec 23 2019",
				"(GAP) a:=[0,1,2,3];; for n in [5..40] do a[n]:=4a[n-1]-a[n-2]; od; a; # _G. C. Greubel_, Dec 23 2019"
			],
			"xref": [
				"Cf. A002530, A002531.",
				"Bisections are A001835 and A052530."
			],
			"keyword": "nonn,easy,frac",
			"offset": "0,3",
			"author": "Robin Trew (trew(AT)hcs.harvard.edu), Dec 11 1999",
			"ext": [
				"Denominator of g.f. corrected by _Paul Barry_, Sep 18 2009",
				"Incorrect g.f. deleted by _Colin Barker_, Aug 10 2012"
			],
			"references": 12,
			"revision": 88,
			"time": "2020-09-17T16:51:08-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}