{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A131027",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 131027,
			"data": "4,3,1,0,1,3,4,3,1,0,1,3,4,3,1,0,1,3,4,3,1,0,1,3,4,3,1,0,1,3,4,3,1,0,1,3,4,3,1,0,1,3,4,3,1,0,1,3,4,3,1,0,1,3,4,3,1,0,1,3,4,3,1,0,1,3,4,3,1,0,1,3,4,3,1,0,1,3,4,3,1,0,1,3,4,3,1,0,1,3,4,3,1,0,1,3,4,3,1,0,1,3,4,3,1",
			"name": "Period 6: repeat [4, 3, 1, 0, 1, 3].",
			"comment": [
				"Third column of triangular array T defined in A131022.",
				"a(n) = abs(A078070(n+1)).",
				"Determinants of the spiral knots S(3,k,(1,1)). a(k+4) = det(S(3,k,(1,1))). These knots are also the torus knots T(3,k). - _Ryan Stees_, Dec 13 2014"
			],
			"link": [
				"A. Breiland, L. Oesper, and L. Taalman, \u003ca href=\"http://educ.jmu.edu/~taalmala/breil_oesp_taal.pdf\"\u003ep-Coloring classes of torus knots\u003c/a\u003e, Online Missouri J. Math. Sci., 21 (2009), 120-126.",
				"N. Brothers, S. Evans, L. Taalman, L. Van Wyk, D. Witczak, and C. Yarnall, \u003ca href=\"http://projecteuclid.org/euclid.mjms/1312232716\"\u003eSpiral knots\u003c/a\u003e, Missouri J. of Math. Sci., 22 (2010).",
				"M. DeLong, M. Russell, and J. Schrock, \u003ca href=\"http://dx.doi.org/10.2140/involve.2015.8.361\"\u003eColorability and determinants of T(m,n,r,s) twisted torus knots for n equiv. +/-1(mod m)\u003c/a\u003e, Involve, Vol. 8 (2015), No. 3, 361-384.",
				"Seong Ju Kim, R. Stees, L. Taalman, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL19/Stees/stees4.html\"\u003eSequences of Spiral Knot Determinants\u003c/a\u003e, Journal of Integer Sequences, Vol. 19 (2016), #16.1.4.",
				"Ryan Stees, \u003ca href=\"https://commons.lib.jmu.edu/honors201019/84\"\u003eSequences of Spiral Knot Determinants\u003c/a\u003e, Senior Honors Projects, Paper 84, James Madison Univ., May 2016.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,-2,1)."
			],
			"formula": [
				"a(1) = 4, a(2) = a(6) = 3, a(3) = a(5) = 1, a(4) = 0, a(6) = 1; for n \u003e 6, a(n) = a(n-6).",
				"G.f.: (4-5*x+3*x^2)/((1-x)*(1-x+x^2)).",
				"a(n) = 1/30*{-(n mod 6)-6*[(n+1) mod 6]-[(n+2) mod 6]+9*[(n+3) mod 6]+14*[(n+4) mod 6]+9*[(n+5) mod 6]}, with n\u003e=0. - _Paolo P. Lava_, Jun 19 2007",
				"a(n) = 2+cos(n*Pi/3)+sqrt(3)*sin(n*Pi/3) = 2+(-1)^((n-1)/3)+(-1)^((1-n)/3). - _Wesley Ivan Hurt_, Sep 11 2014",
				"a(k+4) = det(S(3,k,(1,1))) = (b(k+4))^2, where b(5)=1, b(6)=sqrt(3), b(k)=sqrt(3)*b(k-1) - b(k-2) = b(6)*b(k-1) - b(k-2). - _Ryan Stees_, Dec 13 2014",
				"a(n) = 2 + 2*cos(Pi/3*(n-1)) for n \u003e= 1. - _Werner Schulte_, Jul 18 2017"
			],
			"example": [
				"For k=3, b(7)=sqrt(3)b(6)-b(5)=3-1=2, so det(S(3,3,(1,1)))=2^2=4."
			],
			"maple": [
				"A131027:=n-\u003e2+cos(n*Pi/3)+sqrt(3)*sin(n*Pi/3): seq(A131027(n), n=1..100); # _Wesley Ivan Hurt_, Sep 11 2014"
			],
			"mathematica": [
				"Table[2 + Cos[n*Pi/3] + Sqrt[3]*Sin[n*Pi/3], {n, 30}] (* _Wesley Ivan Hurt_, Sep 11 2014 *)"
			],
			"program": [
				"(PARI) {m=105; for(n=1, m, r=(n-1)%6; print1(if(r==0, 4, if(r==1||r==5, 3, if(r==3, 0, 1))), \",\"))}",
				"(MAGMA) m:=105; [ [4, 3, 1, 0, 1, 3][(n-1) mod 6 + 1]: n in [1..m] ];",
				"(Sage) [(lucas_number2(n,2,1)-lucas_number2(n-1,1,1)) for n in range(4, 109)] # _Zerinvary Lajos_, Nov 10 2009"
			],
			"xref": [
				"Cf. A131022, A078070. Other columns of T are in A088911, A131026, A131028, A131029, A131030."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_Klaus Brockhaus_, following a suggestion of _Paul Curtz_, Jun 10 2007",
			"references": 12,
			"revision": 54,
			"time": "2020-11-22T12:19:31-05:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}