{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A078010",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 78010,
			"data": "1,0,1,3,4,9,19,36,73,147,292,585,1171,2340,4681,9363,18724,37449,74899,149796,299593,599187,1198372,2396745,4793491,9586980,19173961,38347923,76695844,153391689,306783379,613566756,1227133513,2454267027,4908534052",
			"name": "Expansion of (1-x)/(1 - x - x^2 - 2*x^3).",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A078010/b078010.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,1,2)."
			],
			"formula": [
				"a(0)=1, a(1)=0, a(2)=1, a(n) = a(n-1) + a(n-2) + 2*a(n-3) for n \u003e 2. - _Philippe Deléham_, Sep 19 2006",
				"a(n) + a(n+1) = A122552(n+1). - _Philippe Deléham_, Sep 25 2006",
				"If p[1]=0, p[2]=1, p[i]=3, (i\u003e2), and if A is Hessenberg matrix of order n defined by: A[i,j]=p[j-i+1], (i\u003c=j), A[i,j]=-1, (i=j+1), and A[i,j]=0 otherwise. Then, for n \u003e= 1, a(n)=det A. - _Milan Janjic_, May 02 2010",
				"For n \u003e 3, a(n) = A077947(n-2) + 2*A077947(n-3), with A077947 beginning (1, 2, 5, 9, 18, 37, ...); \"1\" has offset 1. - _Gary W. Adamson_, May 13 2013",
				"a(n) = 2^(n-1) - 3*floor((2^(n-1))/7) - 1, for n \u003e= 1. - _Ridouane Oudra_, Dec 02 2019",
				"G.f.: (1 - x) / ((1 - 2*x) * (1 + x + x^2)). - _Michael Somos_, Nov 18 2020"
			],
			"example": [
				"a(6) = 19 = A077947(4) + 2*A077947(3) = 9 + 2*5 = 19.",
				"G.f. = 1 + x^2 + 3*x^3 + 4*x^4 + 9*x^5 + 19*x^6 + 36*x^7 + 73*x^8 + ... - _Michael Somos_, Nov 18 2020"
			],
			"mathematica": [
				"CoefficientList[Series[(1-x)/(1-x-x^2-2*x^3), {x,0,50}],x]  (* _Harvey P. Dale_, Mar 17 2011 *)",
				"LinearRecurrence[{1, 1, 2}, {1, 0, 1}, 50] (* _Vladimir Joseph Stephan Orlovsky_, Feb 24 2012 *)"
			],
			"program": [
				"(PARI) Vec((1-x)/(1-x-x^2-2*x^3)+O(x^50)) \\\\ _Charles R Greathouse IV_, Sep 26 2012",
				"(PARI) {a(n) = ([0, 1, 1; 1, 1, 0; 0, 2, 0]^n)[1, 1]}; /* _Michael Somos_, Nov 18 2020 */",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Integers(), 50); Coefficients(R!( (1-x)/(1-x-x^2-2*x^3) )); // _G. C. Greubel_, Jun 28 2019",
				"(Sage) ((1-x)/(1-x-x^2-2*x^3)).series(x, 50).coefficients(x, sparse=False) # _G. C. Greubel_, Jun 28 2019",
				"(GAP) a:=[1,0,1];; for n in [4..50] do a[n]:=a[n-1]+a[n-2]+2*a[n-3]; od; a; # _G. C. Greubel_, Jun 28 2019"
			],
			"xref": [
				"Cf. A033138, A077947."
			],
			"keyword": "nonn,easy",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_, Nov 17 2002",
			"references": 5,
			"revision": 40,
			"time": "2020-11-18T14:57:35-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}