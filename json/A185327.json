{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A185327",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 185327,
			"data": "1,0,0,0,0,0,0,1,1,1,1,1,1,1,2,2,3,3,4,4,5,6,7,8,10,11,13,15,18,20,24,27,32,36,42,48,56,63,73,83,96,108,125,141,162,183,209,236,270,304,346,390,443,498,565,635,719,807,911,1022,1153,1291,1453,1628,1829,2045",
			"name": "Number of partitions of n into parts \u003e= 7.",
			"comment": [
				"a(n) is also the number of not necessarily connected 2-regular graphs on n-vertices with girth at least 7 (all such graphs are simple). The integer i corresponds to the i-cycle; addition of integers corresponds to disconnected union of cycles.",
				"By removing a single part of size 7, an A026800 partition of n becomes an A185327 partition of n - 7. Hence this sequence is essentially the same as A026800."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A185327/b185327.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Jason Kimberley, \u003ca href=\"/wiki/User:Jason_Kimberley/E_k-reg_girth_ge_g_index\"\u003eIndex of sequences counting not necessarily connected k-regular simple graphs with girth at least g\u003c/a\u003e"
			],
			"formula": [
				"G.f.: Product_{m\u003e=7} 1/(1-x^m).",
				"a(n) = p(n) - p(n-1) - p(n-2) + p(n-5) + 2*p(n-7) - p(n-9) - p(n-10) - p(n-11) - p(n-12) + 2*p(n-14) + p(n-16) - p(n-19) - p(n-20) + p(n-21) where p(n)=A000041(n). - _Shanzhen Gao_, Oct 28 2010 [moved/copied from A026800 by _Jason Kimberley_, Feb 03 2011]",
				"This sequence is the Euler transformation of A185117.",
				"a(n) ~ exp(Pi*sqrt(2*n/3)) * 5*Pi^6 / (6*sqrt(3)*n^4). - _Vaclav Kotesovec_, Jun 02 2018",
				"G.f.: Sum_{k\u003e=0} x^(7*k) / Product_{j=1..k} (1 - x^j). - _Ilya Gutkovskiy_, Nov 28 2020"
			],
			"example": [
				"The  a(0)=1 empty partition vacuously has each part \u003e= 7.",
				"The  a(7)=1 partition is 7.",
				"The  a(8)=1 partition is 8.",
				"............................",
				"The a(13)=1 partition is 13.",
				"The a(14)=2 partitions are 7+7 and 14."
			],
			"maple": [
				"seq(coeff(series(1/mul(1-x^(m+7), m = 0..80), x, n+1), x, n), n = 0..70); # _G. C. Greubel_, Nov 03 2019"
			],
			"mathematica": [
				"f[1, 1] = f[0, k_] = 1; f[n_, k_] := f[n, k] = If[n \u003c 0, 0, If[k \u003e n, 0, If[k == n, 1, f[n, k + 1] + f[n - k, k]]]]; Table[ f[n, 7], {n, 0, 65}] (* _Robert G. Wilson v_, Jan 31 2011 *) (* moved from A026800 by _Jason Kimberley_, Feb 03 2011 *)",
				"Join[{1},Table[Count[IntegerPartitions[n],_?(Min[#]\u003e=7\u0026)],{n,0,70}]] (* _Harvey P. Dale_, Oct 16 2011 *)",
				"CoefficientList[Series[1/QPochhammer[x^7, x], {x, 0, 75}], x] (* _G. C. Greubel_, Nov 03 2019 *)"
			],
			"program": [
				"(MAGMA) p :=  func\u003c n | n lt 0 select 0 else NumberOfPartitions(n) \u003e;",
				"A185327 := func\u003c n | p(n)-p(n-1)-p(n-2)+p(n-5)+2*p(n-7)-p(n-9)-p(n-10)- p(n-11)-p(n-12)+2*p(n-14)+p(n-16)-p(n-19)-p(n-20)+p(n-21) \u003e;",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Integers(), 70); Coefficients(R!( 1/(\u0026*[1-x^(m+7): m in [0..80]]) )); // _G. C. Greubel_, Nov 03 2019",
				"(PARI) my(x='x+O('x^70)); Vec(1/prod(m=0,80, 1-x^(m+7))) \\\\ _G. C. Greubel_, Nov 03 2019",
				"(Sage)",
				"def A185327_list(prec):",
				"    P.\u003cx\u003e = PowerSeriesRing(ZZ, prec)",
				"    return P( 1/product((1-x^(m+7)) for m in (0..80)) ).list()",
				"A185327_list(70) # _G. C. Greubel_, Nov 03 2019"
			],
			"xref": [
				"2-regular simple graphs with girth at least 7: A185117 (connected), A185227 (disconnected), this sequence (not necessarily connected).",
				"Not necessarily connected 2-regular graphs with girth at least g [partitions into parts \u003e= g]: A026807 (triangle); chosen g: A000041 (g=1 -- multigraphs with loops allowed), A002865 (g=2 -- multigraphs with loops forbidden), A008483 (g=3), A008484 (g=4), A185325(g=5), A185326 (g=6), this sequence (g=7), A185328 (g=8), A185329 (g=9).",
				"Not necessarily connected 2-regular graphs with girth exactly g [partitions with smallest part g]: A026794 (triangle); chosen g: A002865 (g=2), A026796 (g=3), A026797 (g=4), A026798 (g=5), A026799 (g=6), A026800(g=7), A026801 (g=8), A026802 (g=9), A026803 (g=10)."
			],
			"keyword": "nonn,easy",
			"offset": "0,15",
			"author": "_Jason Kimberley_, Feb 03 2011",
			"references": 20,
			"revision": 39,
			"time": "2020-11-29T05:38:23-05:00",
			"created": "2011-01-25T22:47:42-05:00"
		}
	]
}