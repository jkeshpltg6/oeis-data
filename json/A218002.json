{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A218002",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 218002,
			"data": "1,0,1,2,3,44,55,1434,3913,39752,392481,5109290,34683451,914698212,5777487703,91494090674,1504751645265,31764834185744,379862450767873,12634073744624082,132945783064464691,2753044719709341980,64135578414076991031,1822831113987975441482",
			"name": "E.g.f.: exp( Sum_{n\u003e=1} x^prime(n) / prime(n) ).",
			"comment": [
				"Conjecture: a(n) = number of degree-n permutations of prime order.",
				"The conjecture is false. Cf. A214003. This sequence gives the number of n-permutations whose cycle lengths are restricted to the prime numbers. - _Geoffrey Critzer_, Nov 08 2015"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A218002/b218002.txt\"\u003eTable of n, a(n) for n = 0..450\u003c/a\u003e",
				"Ljuben Mutafchiev, \u003ca href=\"https://arxiv.org/abs/2108.05291\"\u003eA Note on the Number of Permutations whose Cycle Lengths Are Prime Numbers\u003c/a\u003e, arXiv:2108.05291 [math.CO], 2021."
			],
			"example": [
				"E.g.f.: A(x) = 1 + x^2/2! + 2*x^3/3! + 3*x^4/4! + 44*x^5/5! + 55*x^6/6! + 1434*x^7/7! + ...",
				"where",
				"log(A(x)) = x^2/2 + x^3/3 + x^5/5 + x^7/7 + x^11/11 + x^13/13 + x^17/17 + x^19/19 + x^23/23 + x^29/29 + ... + x^prime(n)/prime(n) + ...",
				"a(5) = 44 because there are 5!/5 = 24 permutations that are 5-cycles and there are 5!/(2*3) = 20 permutations that are the disjoint product of a 2-cycle and a 3-cycle. - _Geoffrey Critzer_, Nov 08 2015"
			],
			"maple": [
				"a:= proc(n) option remember; `if`(n=0, 1, add(`if`(isprime(j),",
				"      a(n-j)*(j-1)!*binomial(n-1, j-1), 0), j=1..n))",
				"    end:",
				"seq(a(n), n=0..25);  # _Alois P. Heinz_, May 12 2016"
			],
			"mathematica": [
				"f[list_] :=Total[list]!/Apply[Times, list]/Apply[Times, Map[Length, Split[list]]!]; Table[Total[Map[f, Select[Partitions[n], Apply[And, PrimeQ[#]] \u0026]]], {n, 0,23}] (* _Geoffrey Critzer_, Nov 08 2015 *)"
			],
			"program": [
				"(PARI) {a(n)=n!*polcoeff(exp(sum(k=1,n,x^prime(k)/prime(k))+x*O(x^n)),n)}",
				"for(n=0,31,print1(a(n),\", \"))"
			],
			"xref": [
				"Cf. A000040, A214003, A273001, A273998, A317131, A329944."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Paul D. Hanna_, Oct 17 2012",
			"references": 8,
			"revision": 35,
			"time": "2021-08-12T05:40:44-04:00",
			"created": "2012-10-17T18:49:24-04:00"
		}
	]
}