{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A207538",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 207538,
			"data": "1,2,4,1,8,4,16,12,1,32,32,6,64,80,24,1,128,192,80,8,256,448,240,40,1,512,1024,672,160,10,1024,2304,1792,560,60,1,2048,5120,4608,1792,280,12,4096,11264,11520,5376,1120,84,1,8192,24576,28160,15360",
			"name": "Triangle of coefficients of polynomials v(n,x) jointly generated with A207537; see Formula section.",
			"comment": [
				"As triangle T(n,k) with 0\u003c=k\u003c=n and with zeros omitted, it is the triangle given by (2, 0, 0, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (0, 1/2, -1/2, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Mar 04 2012",
				"The numbers in rows of the triangle are along \"first layer\" skew diagonals pointing top-left in center-justified triangle given in A013609 ((1+2*x)^n) and  along (first layer) skew diagonals pointing top-right in center-justified triangle given in A038207 ((2+x)^n), see links. - _Zagros Lalo_, Jul 31 2018",
				"If s(n) is the row sum at n, then the ratio s(n)/s(n-1) is approximately 2.414213562373095... (A014176: Decimal expansion of the silver mean, 1+sqrt(2)), when n approaches infinity. - _Zagros Lalo_, Jul 31 2018"
			],
			"reference": [
				"Shara Lalo and Zagros Lalo, Polynomial Expansion Theorems and Number Triangles, Zana Publishing, 2018, ISBN: 978-1-9995914-0-3, pp. 80-83, 357-358"
			],
			"link": [
				"S. Halici, \u003ca href=\"http://www.emis.de/journals/AUA/acta29/Paper9-Acta29-2012.pdf\"\u003eOn some Pell polynomials \u003c/a\u003e, Acta Universitatis Apulensis, No. 29/2012, pp. 105-112.",
				"Zagros Lalo, \u003ca href=\"/A207538/a207538.pdf\"\u003eFirst layer skew diagonals in center-justified triangle of coefficients in expansion of (1 + 2x)^n\u003c/a\u003e",
				"Zagros Lalo, \u003ca href=\"/A207538/a207538_1.pdf\"\u003eFirst layer skew diagonals in center-justified triangle of coefficients in expansion of (2 + x)^n\u003c/a\u003e"
			],
			"formula": [
				"u(n,x) = u(n-1,x)+(x+1)*v(n-1,x), v(n,x) = u(n-1,x)+v(n-1,x), where u(1,x) = 1, v(1,x) = 1. Also, A207538 = |A133156|.",
				"From _Philippe Deléham_, Mar 04 2012: (Start)",
				"With 0\u003c=k\u003c=n:",
				"Mirror image of triangle in A099089.",
				"Skew version of A038207.",
				"Riordan array (1/(1-2*x), x^2/(1-2*x)).",
				"G.f.: 1/(1-2*x-y*x^2).",
				"Sum_{k, 0\u003c=k\u003c=n} T(n,k)*x^k = A190958(n+1), A127357(n), A090591(n), A089181(n+1), A088139(n+1), A045873(n+1), A088138(n+1), A088137(n+1), A099087(n), A000027(n+1), A000079(n), A000129(n+1), A002605(n+1), A015518(n+1), A063727(n), A002532(n+1), A083099(n+1), A015519(n+1), A003683(n+1), A002534(n+1), A083102(n), A015520(n+1), A091914(n) for x = -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 respectively.",
				"T(n,k) = 2*T(n-1,k) + T(-2,k-1) with T(0,0) = 1, T(1,0) = 2, T(1,1) = 0 and T(n, k) = 0 if k\u003c0 or if k\u003en. (End)",
				"T(n,k) = A013609(n-k, n-2*k+1). - _Johannes W. Meijer_, Sep 05 2013",
				"From _Tom Copeland_, Feb 11 2016: (Start)",
				"A053117 is a reflected, aerated and signed version of this entry. This entry belongs to a family discussed in A097610 with parameters h1 = -2 and h2 = -y.",
				"Shifted o.g.f.: G(x,t) = x / (1 - 2 x - t x^2).",
				"The compositional inverse of G(x,t) is Ginv(x,t) = -[(1 + 2x) - sqrt[(1+2x)^2 + 4t x^2]] / (2tx) = x - 2 x^2 + (4-t) x^3 - (8-6t) x^4 + ..., a shifted o.g.f. for A091894 (mod signs with A091894(0,0) = 0).",
				"(End)"
			],
			"example": [
				"First seven rows:",
				"1",
				"2",
				"4...1",
				"8...4",
				"16..12..1",
				"32..32..6",
				"64..80..24..1",
				"(2, 0, 0, 0, 0, ...) DELTA (0, 1/2, -1/2, 0, 0, 0, ...) begins:",
				"    1",
				"    2,   0",
				"    4,   1,  0",
				"    8,   4,  0, 0",
				"   16,  12,  1, 0, 0",
				"   32,  32,  6, 0, 0, 0",
				"   64,  80, 24, 1, 0, 0, 0",
				"  128, 192, 80, 8, 0, 0, 0, 0"
			],
			"mathematica": [
				"u[1, x_] := 1; v[1, x_] := 1; z = 16;",
				"u[n_, x_] := u[n - 1, x] + (x + 1)*v[n - 1, x]",
				"v[n_, x_] := u[n - 1, x] + v[n - 1, x]",
				"Table[Factor[u[n, x]], {n, 1, z}]",
				"Table[Factor[v[n, x]], {n, 1, z}]",
				"cu = Table[CoefficientList[u[n, x], x], {n, 1, z}];",
				"TableForm[cu]",
				"Flatten[%]  (* A207537, |A028297| *)",
				"Table[Expand[v[n, x]], {n, 1, z}]",
				"cv = Table[CoefficientList[v[n, x], x], {n, 1, z}];",
				"TableForm[cv]",
				"Flatten[%]  (* A207538, |A133156| *)",
				"t[0, 0] = 1; t[n_, k_] := t[n, k] = If[n \u003c 0 || k \u003c 0, 0, 2 t[n - 1, k] + t[n - 2, k - 1]]; Table[t[n, k], {n, 0, 15}, {k, 0, Floor[n/2]}] // Flatten (* _Zagros Lalo_, Jul 31 2018 *)",
				"t[n_, k_] := t[n, k] = 2^(n - 2 k) * (n -  k)!/((n - 2 k)! k!) ; Table[t[n, k], {n, 0, 15}, {k, 0, Floor[n/2]} ]  // Flatten (* _Zagros Lalo_, Jul 31 2018 *)"
			],
			"xref": [
				"Cf. A028297, A207537, A133156, A038207, A099089.",
				"Cf. A053117, A097610, A091894.",
				"Cf. A013609, A038207.",
				"Cf. A128099."
			],
			"keyword": "nonn,tabf",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Feb 18 2012",
			"references": 15,
			"revision": 43,
			"time": "2018-08-19T15:48:31-04:00",
			"created": "2012-02-18T20:47:36-05:00"
		}
	]
}