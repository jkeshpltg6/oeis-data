{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A322370",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 322370,
			"data": "15,30,21,42,119,33,35,78,209,133,51,57,299,65,138,217,77,399,87,93,295,105,210,111,222,230,258,266,141,143,451,155,161,330,505,177,183,185,195,390,201,203,215,1342,231,462,237,721,1209,255,518,267,273,546",
			"name": "For any n \u003e 4: let p be the n-th prime number; a(n) is the least squarefree p-smooth integer congruent to 4 modulo p.",
			"comment": [
				"This sequence is well-defined per the work of Booker and Pomerance.",
				"The number 4 in the congruence in the name could be replaced by any value; this number was chosen for being the first integer that is not squarefree."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A322370/b322370.txt\"\u003eTable of n, a(n) for n = 5..10000\u003c/a\u003e",
				"Andrew R. Booker, Carl Pomerance, \u003ca href=\"https://arxiv.org/abs/1607.01557\"\u003eSquarefree smooth numbers and Euclidean prime generators\u003c/a\u003e, arXiv:1607.01557 [math.NT], 2016-2017.",
				"Andrew R. Booker and Carl Pomerance, \u003ca href=\"https://doi.org/10.1090/proc/13576\"\u003eSquarefree smooth numbers and Euclidean prime generators\u003c/a\u003e, Proceedings of the American Mathematical Society 145 (2017), 5035-5042.",
				"Rémy Sigrist, \u003ca href=\"/A322370/a322370.png\"\u003eColored scatterplot of (n, a(n)) for n = 5..1000000\u003c/a\u003e (where the color is function of (a(n)-4)/A000040(n))."
			],
			"formula": [
				"a(n) = A261144(n, k) for some k in 1..2^n."
			],
			"example": [
				"For n = 7:",
				"- the 7th prime is 17,",
				"- the first squarefree 17-smooth integers s, alongside (s-4) mod 17, are:",
				"     s              1   2   3  5  6  7  10  11  13  14  15  17  21",
				"     ------------  --  --  --  -  -  -  --  --  --  --  --  --  --",
				"     (s-4) mod 17  14  15  16  1  2  3   6   7   9  10  11  13   0",
				"- hence a(7) = 21."
			],
			"mathematica": [
				"a[n_] := Module[{p = Prime[n], k = 4}, While[! SquareFreeQ[k] || FactorInteger[k][[-1, 1]] \u003e p, k += p; Continue[]]; k]; Array[a, 100, 5] (* _Amiram Eldar_, Dec 08 2018 *)"
			],
			"program": [
				"(PARI) a(n) = my (p=prime(n)); forstep (v=4, oo, p, if (issquarefree(v), my (f=factor(v)); if (f[#f~,1] \u003c= p, return (v))))"
			],
			"xref": [
				"Cf. A000040, A005117, A261144."
			],
			"keyword": "nonn",
			"offset": "5,1",
			"author": "_Rémy Sigrist_, Dec 05 2018",
			"references": 1,
			"revision": 27,
			"time": "2019-02-04T07:35:55-05:00",
			"created": "2018-12-08T20:43:49-05:00"
		}
	]
}