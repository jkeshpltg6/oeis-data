{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A242062",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 242062,
			"data": "0,1,0,0,1,1,0,1,2,1,1,2,2,1,2,3,2,2,3,3,2,3,4,3,3,4,4,3,4,5,4,4,5,5,4,5,6,5,5,6,6,5,6,7,6,6,7,7,6,7,8,7,7,8,8,7,8,9,8,8,9,9,8,9,10,9,9,10,10,9,10,11,10,10,11,11,10,11,12,11,11,12",
			"name": "Expansion of x * (1 - x^12) / ((1 - x^3) * (1 - x^4) * (1 - x^7)) in powers of x.",
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A242062/b242062.txt\"\u003eTable of n, a(n) for n = 0..5000\u003c/a\u003e"
			],
			"formula": [
				"Euler transform of length 12 sequence [0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, -1].",
				"G.f.: x * (1 - x + x^3 - x^5 + x^6) / (1 - x - x^7 + x^8).",
				"G.f.: x / (1 - x^3 / (1 - x / (1 + x / (1 - x^5 / (1 - x / (1 + x^2 / (1 - x^2))))))).",
				"a(n) = -a(-n) = a(n-7) + 1 = a(n-1) + a(n-7) - a(n-8) for all n in Z.",
				"0 = 2*a(n) - a(n+1) + a(n+2) - 2*a(n+3) + (a(n+1) - a(n+2))^2 for all n in Z.",
				"a(n+1) - a(n) = A131372(n)."
			],
			"example": [
				"G.f. = x + x^4 + x^5 + x^7 + 2*x^8 + x^9 + x^10 + 2*x^11 + 2*x^12 + x^13 + ..."
			],
			"maple": [
				"a:= n -\u003e [0, 1, 0, 0, 1, 1, 0][n mod 7 + 1] + floor(n/7):",
				"seq(a(n), n=0..20); # _Robert Israel_, Aug 13 2014"
			],
			"mathematica": [
				"a[ n_] := Quotient[ n+3, 7] + {1, 0, 0, 0, 0, -1, 0}[[Mod[ n, 7, 1]]];",
				"a[ n_] := Sign[n] * SeriesCoefficient[ x * (1 - x^12) / ((1 - x^3) * (1 - x^4) * (1 - x^7)), {x, 0, Abs[n]}];",
				"CoefficientList[Series[x (1 - x + x^3 - x^5 + x^6)/(1 - x - x^7 + x^8), {x, 0, 100}], x] (* _Vincenzo Librandi_, Aug 14 2014 *)"
			],
			"program": [
				"(PARI) {a(n) = (n+3)\\7 + (n%7==1) - (n%7==6)};",
				"(PARI) {a(n) = sign(n) * polcoeff( x * (1 - x^12) / ((1 - x^3) * (1 - x^4) * (1 - x^7)) + x * O(x^abs(n)), abs(n))};",
				"(MAGMA) m:=50; R\u003cx\u003e:=PowerSeriesRing(Integers(), m); Coefficients(R!(x*(1 -x+x^3-x^5+x^6)/(1-x-x^7+x^8))); // _G. C. Greubel_, Aug 05 2018"
			],
			"xref": [
				"Cf. A131372."
			],
			"keyword": "nonn",
			"offset": "0,9",
			"author": "_Michael Somos_, Aug 13 2014",
			"references": 3,
			"revision": 18,
			"time": "2018-08-05T23:38:32-04:00",
			"created": "2014-08-13T13:20:10-04:00"
		}
	]
}