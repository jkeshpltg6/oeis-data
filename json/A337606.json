{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A337606",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 337606,
			"data": "4,8,7,6,2,2,7,7,8,1,1,1,5,7,1,7,6,8,6,1,1,6,4,6,3,9,1,4,5,2,3,8,8,4,2,3,1,3,1,6,7,7,1,2,4,4,2,9,7,3,5,7,6,3,7,7,0,1,8,1,5,8,2,9,7,2,3,6,5,6,9,0,3,4,5,4,0,0,9,2,3,4,9,8,1,0,6,6,6,1,7,4,6,4,8,5,1,9,1,4,3,3,2,8,4,1",
			"name": "Decimal expansion of the Gaussian twin prime constant: the Hardy-Littlewood constant for A096012.",
			"comment": [
				"The name of this constant was suggested by Finch (2003).",
				"Gaussian twin primes on the line x + i in the complex plane are Gaussian primes pair of the form (m - 1 + i, m + 1 + i). The numbers m are numbers such that (m-1)^2 + 1 and (m+1)^2 + 1 are both primes (A096012 plus 1).",
				"Shanks (1960) conjectured that the number of these pairs with m \u003c= x is asymptotic to c * li_2(x), where li_2(x) = Integral_{t=2..n} (1/log(t)^2) dt, and c is this constant. He defined c as in the formula section and evaluated it by 0.4876.",
				"The first 100 digits of 4*c were calculated by Ettahri et al. (2019)."
			],
			"reference": [
				"Steven R. Finch, Mathematical Constants, Cambridge University Press, 2003, p. 90."
			],
			"link": [
				"Keith Conrad, \u003ca href=\"https://doi.org/10.1007/978-1-4615-0304-0_15\"\u003eHardy-Littlewood constants\u003c/a\u003e in: Mathematical properties of sequences and other combinatorial structures, Jong-Seon No et al. (eds.), Kluwer, Boston/Dordrecht/London, 2003, pp. 133-154, \u003ca href=\"https://kconrad.math.uconn.edu/articles/hlconst.pdf\"\u003ealternative link\u003c/a\u003e.",
				"Salma Ettahri, Olivier Ramaré, Léon Surel, \u003ca href=\"https://arxiv.org/abs/1908.06808\"\u003eFast multi-precision computation of some Euler products\u003c/a\u003e, arXiv:1908.06808 [math.NT], 2019 (Section 8).",
				"Daniel Shanks, \u003ca href=\"https://doi.org/10.1090/S0025-5718-1960-0111724-0\"\u003e, A note on Gaussian twin primes\u003c/a\u003e, Mathematics of Computation, Vol. 14, No. 70 (1960), pp. 201-203."
			],
			"formula": [
				"Equals (Pi^2/8) * Product_{primes p == 1 (mod 4)} (1 - 4/p)*((p + 1)/(p - 1))^2."
			],
			"example": [
				"0.487622778111571768611646391452388423131677124429735..."
			],
			"mathematica": [
				"S[m_, n_, s_] := (t = 1; sums = 0; difs = 1; While[Abs[difs] \u003e 10^(-digits - 5) || difs == 0, difs = (MoebiusMu[t]/t) * Log[If[s*t == 1, DirichletL[m, n, s*t], Sum[Zeta[s*t, j/m]*DirichletCharacter[m, n, j]^t, {j, 1, m}]/m^(s*t)]]; sums = sums + difs; t++]; sums);",
				"P[m_, n_, s_] := 1/EulerPhi[m] * Sum[Conjugate[DirichletCharacter[m, r, n]] * S[m, r, s], {r, 1, EulerPhi[m]}] + Sum[If[GCD[p, m] \u003e 1 \u0026\u0026 Mod[p, m] == n, 1/p^s, 0], {p, 1, m}];",
				"Z[m_, n_, s_] := (w = 1; sumz = 0; difz = 1; While[Abs[difz] \u003e 10^(-digits - 5), difz = P[m, n, s*w]/w; sumz = sumz + difz; w++]; Exp[sumz]);",
				"Zs[m_, n_, s_] := (w = 2; sumz = 0; difz = 1; While[Abs[difz] \u003e 10^(-digits - 5), difz = (s^w - s) * P[m, n, w]/w; sumz = sumz + difz; w++]; Exp[-sumz]);",
				"$MaxExtraPrecision = 1000; digits = 121; RealDigits[Chop[N[Pi^2/8 * Zs[4, 1, 4]/Z[4, 1, 2]^2, digits]], 10, digits-1][[1]] (* _Vaclav Kotesovec_, Jan 15 2021 *)"
			],
			"xref": [
				"Cf. A002496, A005574, A096012, A206328.",
				"Similar constants: A005597, A331941, A337607, A337608."
			],
			"keyword": "nonn,cons",
			"offset": "0,1",
			"author": "_Amiram Eldar_, Sep 04 2020",
			"ext": [
				"More digits from _Vaclav Kotesovec_, Jan 15 2021"
			],
			"references": 2,
			"revision": 12,
			"time": "2021-01-15T10:26:32-05:00",
			"created": "2020-09-04T21:23:58-04:00"
		}
	]
}