{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A131569",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 131569,
			"data": "1,2,8,24,71,198,541,1452,3862,10208,26885,70644,185369,485982,1273420,3335640,8735707,22875050,59895221,156819960,410579786,1074943872,2814291433,7367994504,19289795761,50501560538,132215157296,346144350552,906218605007",
			"name": "a(n) = (1/2)*(F(n+2)-1)*(F(n+2)-2) + F(n), where F() are the Fibonacci numbers.",
			"comment": [
				"Consider the infinite array M, containing the positive integers by antidiagonals from lower left to upper right: M(j,k) = (k+j-1)*(k+j)/2-(j-1); j, k \u003e= 1. a(n) is the element in row F(n+1) and column F(n), i.e., a(n) = M(F(n+1),F(n))."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A131569/b131569.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-2,-6,4,2,-1)."
			],
			"formula": [
				"From _Colin Barker_, Feb 21 2015: (Start)",
				"a(n) = 4*a(n-1)-2*a(n-2)-6*a(n-3)+4*a(n-4)+2*a(n-5)-a(n-6).",
				"G.f.: -x*(x^4-2*x^3-2*x^2+2*x-1) / ((x-1)*(x+1)*(x^2-3*x+1)*(x^2+x-1)).",
				"(End)"
			],
			"example": [
				"Upper left 6 X 6 submatrix of M is",
				"[ 1 3 6 10 15 21]",
				"[ 2 5 9 14 20 27]",
				"[ 4 8 13 19 26 34]",
				"[ 7 12 18 25 33 42]",
				"[11 17 24 32 41 51]",
				"[16 23 31 40 50 61]",
				"F(0) through F(7) are 0, 1, 1, 2, 3, 5, 8, 13. a(4) = M(F(5),F(4)) = M(5,3) = 24."
			],
			"mathematica": [
				"LinearRecurrence[{4,-2,-6,4,2,-1},{1,2,8,24,71,198},30] (* _Harvey P. Dale_, Aug 08 2021 *)"
			],
			"program": [
				"(PARI) for(n=1, 27, print1((1/2)*(fibonacci(n+2)-1)*(fibonacci(n+2)-2)+fibonacci(n), \",\")) /* _Klaus Brockhaus_, Aug 29 2007 */",
				"(MAGMA) z:=15; m:=Fibonacci(z+1); M:=Matrix(IntegerRing(), m, m, [ [ (k+j-1)*(k+j)/2-(j-1): k in [1..m] ]: j in [1..m] ] ); [ M[Fibonacci(n+1), Fibonacci(n)]: n in [1..z] ] /* _Klaus Brockhaus_, Aug 29 2007 */"
			],
			"xref": [
				"Cf. A000045 (Fibonacci numbers)."
			],
			"keyword": "easy,nonn",
			"offset": "1,2",
			"author": "Philippe LALLOUET (philip.lallouet(AT)wanadoo.fr), Aug 27 2007",
			"ext": [
				"Edited and extended by _Klaus Brockhaus_, Aug 29 2007"
			],
			"references": 1,
			"revision": 22,
			"time": "2021-08-08T19:13:55-04:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}