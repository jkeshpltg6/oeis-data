{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A242394",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 242394,
			"data": "6,18,30,42,54,66,66,102,114,126,138,150,150,162,198,210,222,234,222,270,258,294,306,318,330,330,366,354,390,402,390,426,450,462,450,486,474,486,510,546,558,546,558,594,606,630,642,654,618,678,690,690,726,738,750,738,750",
			"name": "Number of equilateral triangles (sides length = 1) that intersect the circumference of a circle of radius n centered at (0,0).",
			"comment": [
				"For all n, there are at least 6 points where the transit of circumference occurs exactly at the corners. The rare case is when the transit occurs at 2 corners of a triangle, i.e., at n = 1, 13, 181, 35113, ... , (A001570(n)). The pattern repeats itself at every Pi/3 sector along the circumference. The triangle count per half sector by rows can be arranged as an irregular triangle as shown in the illustration. The rows count (A242396) is equal to the case centered at (1/2,0), A242395."
			],
			"link": [
				"Kival Ngaokrajang, \u003ca href=\"/A242394/a242394_2.pdf\"\u003eIllustration of initial terms\u003c/a\u003e",
				"Kival Ngaokrajang, \u003ca href=\"/A242394/a242394_1.pdf\"\u003eIllustration for rare cases\u003c/a\u003e"
			],
			"program": [
				"(Small Basic)",
				"For n =1 To 100",
				"  r6=n*math.Sin(30*Math.Pi/180)/(Math.Power(3,0.5)/2)",
				"  r6a=math.Round(r6)",
				"  If r6-math.Floor(r6) \u003e0.5 Then",
				"    last=1",
				"  Else",
				"    last=2",
				"  EndIf",
				"  'find corner intersecting points-----------------------",
				"    k=0",
				"    ic=0",
				"    h=Math.Power(1-0.5*0.5,0.5)",
				"    c=math.Floor(n/h)",
				"    For i = h To c Step h",
				"      For j = 0.5 To n Step 0.5",
				"        r=Math.Power(i*i+j*j,0.5)",
				"        If r = n Then",
				"          k=k+1",
				"        EndIf",
				"      EndFor",
				"    EndFor",
				"    if k \u003e 1 then",
				"      ic=math.Floor(k/3)",
				"    EndIf",
				"  '------------------------------------------------------",
				"  a=0",
				"  b=0",
				"  For ii=1 To r6a",
				"    If ii=1 Then",
				"      a=a+1",
				"    Else",
				"      If ii = r6a Then",
				"        a=a+last",
				"      Else",
				"        a=a+2",
				"      EndIf",
				"    EndIf",
				"    b=a",
				"  EndFor",
				"  if n =1 then",
				"    aa = 1",
				"  Else",
				"    aa =1*(a-2*ic)*2+1",
				"  endif",
				"  TextWindow.Write(6*aa+\", \")",
				"EndFor"
			],
			"xref": [
				"Cf. A001570, A242118."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Kival Ngaokrajang_, May 13 2014",
			"references": 4,
			"revision": 13,
			"time": "2014-05-18T11:13:02-04:00",
			"created": "2014-05-18T11:13:02-04:00"
		}
	]
}