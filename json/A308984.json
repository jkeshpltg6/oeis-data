{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A308984",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 308984,
			"data": "0,1,1,2,1,3,2,5,3,6,3,5,9,4,5,8,3,9,14,5,10,5,7,2,19,17,2,5,11,6,26,20,6,9,25,16,9,12,3,25,30,5,19,37,18,19,22,3,12,23,11,33,22,28,6,28,30,46,16,39,23,34,11,23,26,60,34,39,47,8,62,54,8,11",
			"name": "If a(n) is not a term of a(0)..a(n-1), then a(n+1) = |a(n) - a(n-1)|, otherwise a(n+1) = a(n) + n - m, where a(m) = a(n), m \u003c n, and m is maximal. a(0)=0, a(1)=1.",
			"comment": [
				"In other words, if the last term has not appeared previously, take the absolute difference between the previous term and itself to obtain the next one; otherwise, add to the last term the difference between its index and that of the last identical term to obtain the next one.",
				"The sequence is infinite, because loops are impossible."
			],
			"link": [
				"Sean A. Irvine, \u003ca href=\"https://github.com/archmageirvine/joeis/blob/master/src/irvine/oeis/a308/A308984.java\"\u003eJava program\u003c/a\u003e (github)"
			],
			"example": [
				"a(0)=0 (given).",
				"a(1)=1 (given).",
				"a(2)=1: a(1) is not a term of a(0..0), therefore a(2) = |a(1) - a(0)| = 1 - 0 = 1.",
				"a(3)=2: a(2)=a(1), therefore a(3) = a(2) + 2 - 1 = 1 + 2 - 1 = 2.",
				"a(4)=1: a(3) is not a term of a(0..2), therefore a(4) = |a(3) - a(2)| = 2 - 1 = 1.",
				"a(5)=3: a(4)=a(2), therefore a(5) = a(4) + 4 - 2 = 1 + 4 - 2 = 3.",
				"a(6)=2: a(5) is not a term of a(0..4), therefore a(6) = |a(5) - a(4)| = 3 - 1 = 2."
			],
			"maple": [
				"P:=proc(q) local a,k,m,n: a:=[0,1]: for n from 2 to q do",
				"if numboccur(subsop([n]=NULL,a),a[n])=0 then a:=[op(a),abs(a[n]-a[n-1])]:",
				"else for m from n-1 by -1 to 1 do if a[m]=a[n] then break: fi: od:",
				"a:=[op(a),a[n]+n-m]: fi: od: op(a); end: P(73); # _Paolo P. Lava_, Jul 05 2019"
			],
			"mathematica": [
				"Nest[Function[{a, n}, Append[a, If[FreeQ[Most@ a, Last@ a], Abs[Subtract @@ a[[-2 ;; -1]] ], Last[a] + n - Position[Most@ a, _?(# == Last@ a \u0026)][[-1, 1]] ]]] @@ {#, Length@ #} \u0026, {0, 1}, 72] (* _Michael De Vlieger_, Jul 08 2019 *)"
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Marc Morgenegg_, Jul 04 2019",
			"references": 1,
			"revision": 34,
			"time": "2019-07-27T22:53:29-04:00",
			"created": "2019-07-27T22:53:29-04:00"
		}
	]
}