{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A064027",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 64027,
			"data": "1,3,10,19,26,30,50,83,91,78,122,190,170,150,260,339,290,273,362,494,500,366,530,830,651,510,820,950,842,780,962,1363,1220,870,1300,1729,1370,1086,1700,2158,1682,1500,1850,2318,2366,1590,2210,3390,2451,1953",
			"name": "a(n) = (-1)^n*Sum_{d|n} (-1)^d*d^2.",
			"reference": [
				"G. H. Hardy, Ramanujan: twelve lectures on subjects suggested by his life and work, AMS Chelsea Publishing, Providence, Rhode Island, 2002, p. 142."
			],
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A064027/b064027.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Heekyoung Hahn, \u003ca href=\"http://arxiv.org/abs/1507.04426\"\u003eConvolution sums of some functions on divisors\u003c/a\u003e, arXiv:1507.04426 [math.NT], 2015."
			],
			"formula": [
				"Multiplicative with a(2^e) = (4^(e+1)-7)/3, a(p^e) = (p^(2*e+2)-1)/(p^2-1), p \u003e 2.",
				"a(n) = (-1)^n*(A001157(n) - 2*A050999(n)).",
				"Logarithmic derivative of A224364. - _Paul D. Hanna_, Apr 04 2013",
				"Bisection: a(2*k-1) = A001157(2*k-1), a(2*k) = 4*A001157(k) - A050999(2*k), k \u003e= 1. In the Hardy reference a(n) = sigma^*_2(n). - _Wolfdieter Lang_, Jan 07 2017",
				"G.f.: Sum_{k\u003e=1} k^2*x^k/(1 - (-x)^k). - _Ilya Gutkovskiy_, Nov 09 2018",
				"Conjecture: Sum_{k=1..n} a(k) ~ 7 * Zeta(3) * n^3 / 24. - _Vaclav Kotesovec_, Nov 10 2018"
			],
			"example": [
				"L.g.f.: L(x) = x + 3*x^2/2 + 10*x^3/3 + 19*x^4/4 + 26*x^5/5 + 30*x^6/6 + ...",
				"where exp(L(x)) = 1 + x + 2*x^2 + 5*x^3 + 10*x^4 + 18*x^5 + 32*x^6 + 59*x^7 + 106*x^8 + 181*x^9 + ... + A224364(n)*x^n + ... - _Paul D. Hanna_, Apr 04 2013"
			],
			"mathematica": [
				"a[n_] := (-1)^n DivisorSum[n, (-1)^# * #^2 \u0026]; Array[a, 50] (* _Jean-François Alcover_, Dec 23 2015 *)"
			],
			"program": [
				"(PARI) {a(n)=if(n\u003c1, 0, (-1)^n*sumdiv(n^1, d, (-1)^d*d^2))} \\\\ _Paul D. Hanna_, Apr 04 2013",
				"(MAGMA) m:=60; R\u003cx\u003e:=PowerSeriesRing(Integers(), m); Coefficients(R!( (\u0026+[k^2*x^k/(1-(-x)^k): k in [1..m]]) )); // _G. C. Greubel_, Nov 09 2018"
			],
			"xref": [
				"Cf. A001157, A002129, A008457, A050999, A224364.",
				"Cf. A321543 - A321565, A321807 - A321836 for related sequences."
			],
			"keyword": "mult,nonn",
			"offset": "1,2",
			"author": "_Vladeta Jovovic_, Sep 11 2001",
			"references": 8,
			"revision": 34,
			"time": "2018-11-26T11:03:25-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}