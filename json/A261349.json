{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A261349",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 261349,
			"data": "0,0,1,0,3,1,2,0,7,1,6,3,4,2,5,0,15,1,14,3,12,2,13,6,9,7,8,5,10,4,11,0,31,1,30,3,28,2,29,6,25,7,24,5,26,4,27,12,19,13,18,15,16,14,17,10,21,11,20,9,22,8,23,0,63,1,62,3,60,2,61,6,57,7,56,5",
			"name": "T(n,k) is the decimal equivalent of a code for k that maximizes the sum of the Hamming distances between (cyclical) adjacent code words; triangle T(n,k), n\u003e=0, 0\u003c=k\u003c=2^n-1, read by rows.",
			"comment": [
				"This code might be called \"Anti-Gray code\".",
				"The sum of the Hamming distances between (cyclical) adjacent code words of row n gives 0, 2, 6, 20, 56, 144, 352, ... = A014480(n-1) for n\u003e1."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A261349/b261349.txt\"\u003eRows n = 0..13, flattened\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Gray_code\"\u003eGray code\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Hamming_distance\"\u003eHamming distance\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = A003188(k/2) if k even, T(n,k) = 2^n-1-A003188((k-1)/2) else.",
				"A101080(T(n,2k),T(n,2k+1)) = n, A101080(T(n,2k),T(n,2k-1)) = n-1.",
				"T(n,2^n-1) = A083329(n-1) for n\u003e0.",
				"T(n,2^n-2) = A000079(n-2) for n\u003e1.",
				"T(2n,2n) = A003188(n).",
				"T(2n+1,2n+1) = 2*4^n - 1 - A003188(n) = A083420(n) - A003188(n)."
			],
			"example": [
				"Triangle T(n,k) begins:",
				"  0;",
				"  0,  1;",
				"  0,  3, 1,  2;",
				"  0,  7, 1,  6, 3,  4, 2,  5;",
				"  0, 15, 1, 14, 3, 12, 2, 13, 6,  9, 7,  8, 5, 10, 4, 11;",
				"  0, 31, 1, 30, 3, 28, 2, 29, 6, 25, 7, 24, 5, 26, 4, 27, 12, 19, ... ;",
				"  0, 63, 1, 62, 3, 60, 2, 61, 6, 57, 7, 56, 5, 58, 4, 59, 12, 51, ... ;"
			],
			"maple": [
				"g:= n-\u003e Bits[Xor](n, iquo(n, 2)):",
				"T:= (n, k)-\u003e (t-\u003e `if`(m=0, t, 2^n-1-t))(g(iquo(k, 2, 'm'))):",
				"seq(seq(T(n, k), k=0..2^n-1), n=0..6);"
			],
			"xref": [
				"Columns k=0-3 give: A000004, A000225, A000012 (for n\u003e1), A000918 (for n\u003e1).",
				"Row lengths give A000079.",
				"Row sums give A006516.",
				"Cf. A003188, A014480, A083329, A083420, A101080."
			],
			"keyword": "nonn,look,tabf,easy",
			"offset": "0,5",
			"author": "_Alois P. Heinz_, Nov 18 2015",
			"references": 1,
			"revision": 34,
			"time": "2018-09-05T16:16:22-04:00",
			"created": "2015-11-19T16:32:56-05:00"
		}
	]
}