{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A309735",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 309735,
			"data": "2,5,3,4,3,8,3,2,4,7,2,5,9,4,9,6,7,2,4,21,2,5,7,3,5,3,8,2,4,3,2,5,11,4,5,7,8,2,6,23,2,5,6,14,3,16,3,2,3,14,2,4,15,17,5,7,4,2,11,18,2,4,47,14,5,6,4,2,7,3,2,3,13,3,5,15,4,8,6,9,2,4,11,6,5,22,4",
			"name": "a(n) is the least positive integer k such that k^n starts with 2.",
			"comment": [
				"For n \u003e 1, take integer d \u003e -n log_10(3^(1/n)-2^(1/n)).",
				"Then 10^(d/n) \u003e 1/(3^(1/n) - 2^(1/n))",
				"so (3*10^d)^(1/n) - (2*10^d)^(1/n) \u003e 1",
				"and therefore a(n) \u003c= ceiling((2*10^d)^(1/n)).",
				"In particular, a(n) always exists."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A309735/b309735.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A067443(n)^(1/n).",
				"A000030(a(n)^n)=2."
			],
			"example": [
				"a(5) = 3 because 3^5 = 243 starts with 2, while 1^5=1 and 2^5=32 do not start with 2."
			],
			"maple": [
				"f:= proc(n) local x,y;",
				"  for x from 2  do",
				"    y:= x^n;",
				"      if floor(y/10^ilog10(y)) = 2 then return x fi",
				"  od",
				"end proc:",
				"map(f, [$1..100]);"
			],
			"program": [
				"(PARI) a(n) = for(k=1, oo, if(digits(k^n)[1]==2, return(k))) \\\\ _Felix Fröhlich_, Aug 14 2019",
				"(Python)",
				"n = 1",
				"while n \u003c 100:",
				"    k, s = 2, str(2**n)",
				"    while s[0] != \"2\":",
				"        k = k+1",
				"        s = str(k**n)",
				"    print(n,k)",
				"    n = n+1 # _A.H.M. Smeets_, Aug 14 2019",
				"(MAGMA) m:=1; sol:=[]; for n in [1..100] do k:=2; while Reverse(Intseq(k^n))[1] ne 2 do k:=k+1; end while; sol[m]:=k; m:=m+1; end for; sol; // _Marius A. Burtea_, Aug 15 2019"
			],
			"xref": [
				"Cf. A000030, A067443, A309707."
			],
			"keyword": "nonn,base",
			"offset": "1,1",
			"author": "_Robert Israel_, Aug 14 2019",
			"references": 1,
			"revision": 28,
			"time": "2019-11-24T21:44:08-05:00",
			"created": "2019-08-15T23:28:30-04:00"
		}
	]
}