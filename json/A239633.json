{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A239633",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 239633,
			"data": "1,1,1,1,31,1,1,242,242,1,1,992,7744,992,1,1,3124,99968,99968,3124,1,1,7502,756008,3099008,756008,7502,1,1,16806,4067052,52501944,52501944,4067052,16806,1,1,31744,17209344,533489664,1680062208,533489664,17209344,31744",
			"name": "Triangle read by rows: T(n,k) = A059384(n)/(A059384(k)*A059384(n-k)).",
			"comment": [
				"We assume that A059384(0)=1 since it would be the empty product.",
				"These are the generalized binomial coefficients associated with the Jordan totient function J_5 given in A059378.",
				"Another name might be the 5-totienomial coefficients."
			],
			"link": [
				"Tom Edgar, \u003ca href=\"http://www.emis.de/journals/INTEGERS/papers/o62/o62.Abstract.html\"\u003eTotienomial Coefficients\u003c/a\u003e, INTEGERS, 14 (2014), #A62.",
				"Tom Edgar and Michael Z. Spivey, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL19/Edgar/edgar3.html\"\u003eMultiplicative functions, generalized binomial coefficients, and generalized Catalan numbers\u003c/a\u003e, Journal of Integer Sequences, Vol. 19 (2016), Article 16.1.6.",
				"Donald E. Knuth and Herbert S. Wilf, \u003ca href=\"http://www.math.upenn.edu/~wilf/website/dm36.pdf\"\u003eThe power of a prime that divides a generalized binomial coefficient\u003c/a\u003e, J. Reine Angew. Math., 396:212-219, 1989."
			],
			"formula": [
				"T(n,k) = A059384(n)/(A059384(k)* A059384(n-k)).",
				"T(n,k) = prod_{i=1..n} A059378(i)/(prod_{i=1..k} A059378(i)*prod_{i=1..n-k} A059378(i)).",
				"T(n,k) = A059378(n)/n*(k/A059378(k)*T(n-1,k-1)+(n-k)/A059378(n-k)*T(n-1,k))."
			],
			"example": [
				"The first five terms in the fifth Jordan totient function are 1,31,242,992,3124 and so T(4,2) = 992*242*31*1/((31*1)*(31*1))=7744 and T(5,3) = 3124*992*242*31*1/((242*31*1)*(31*1))=99968.",
				"The triangle begins",
				"1",
				"1 1",
				"1 31   1",
				"1 242  242   1",
				"1 992  7744  992   1",
				"1 3124 99968 99968 3124 1"
			],
			"program": [
				"(Sage)",
				"q=100 #change q for more rows",
				"P=[0]+[i^5*prod([1-1/p^5 for p in prime_divisors(i)]) for i in [1..q]]",
				"[[prod(P[1:n+1])/(prod(P[1:k+1])*prod(P[1:(n-k)+1])) for k in [0..n]] for n in [0..len(P)-1]] #generates the triangle up to q rows."
			],
			"xref": [
				"Cf. A059378, A059384, A238453, A238688, A238743, A238754."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Tom Edgar_, Mar 22 2014",
			"references": 1,
			"revision": 17,
			"time": "2016-01-25T14:21:42-05:00",
			"created": "2014-03-25T07:02:05-04:00"
		}
	]
}