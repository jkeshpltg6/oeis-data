{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A343067",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 343067,
			"data": "15,28,45,66,91,120,40,153,190,231,276,84,325,378,435,496,144,77,561,630,703,104,780,220,861,946,1035,1128,312,1225,170,1326,1431,1540,126,420,209,1653,1770,1891,2016,544,2145,2278,299,2415,2556,198,684,2701,350,2850,3003,3160",
			"name": "Perimeter of integer-sided primitive triangles (a, b, c) whose angle B = 2*C.",
			"comment": [
				"The triples (a, b, c) are listed in increasing order of side a, and if sides a coincide then in increasing order of side b.",
				"This sequence is nonincreasing: a(7) = 40 \u003c a(6) = 120.",
				"If in triangle ABC, B = 2*C, then the corresponding metric relation between sides is a*c + c^2 = c * (a + c) = b^2.",
				"As the metric relation is equivalent to a = m^2 - k^2, b = m*k, c = k^2, with gcd(m,k) = 1 and k \u003c m \u003c 2k, so all terms are of the form m^2 + m*k = m * (m+k) with gcd(m,k) = 1 and k \u003c m \u003c 2k. These perimeters are in increasing order in A106499.",
				"For the corresponding primitive triples and miscellaneous properties and references, see A343063."
			],
			"formula": [
				"a(n) = A343063(n, 1) + A343063(n, 2) + A343063(n, 3).",
				"a(n) = A343064(n) + A343065(n) + A343066(n)."
			],
			"example": [
				"According to inequalities between a, b, c, there exist 3 types of such triangles:",
				"a(1) = 15 with c \u003c a \u003c b for the first triple (5, 6, 4);",
				"a(7) = 40 with c \u003c b \u003c a for the seventh triple (16, 15, 9);",
				"a(8) = 153 with a \u003c c \u003c b for the eighth triple (17, 72, 64)."
			],
			"maple": [
				"a from 2 to 100 do",
				"for c from 3 to floor(a^2/2) do",
				"d := c*(a+c);",
				"if issqr(d) and igcd(a, sqrt(d), c)=1 and abs(a-c)\u003csqrt(d) and sqrt(d)\u003ca+c then print(a+sqrt(d)+c); end if;",
				"end do;",
				"end do;"
			],
			"xref": [
				"Cf. A335897 (similar for A \u003c B \u003c C in arithmetic progression).",
				"Cf. A343063 (triples), A343064 (side a), A343065 (side b), A343066 (side c), A106499 (perimeters in increasing order)."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Bernard Schott_, Apr 15 2021",
			"references": 5,
			"revision": 22,
			"time": "2021-09-06T01:45:37-04:00",
			"created": "2021-04-15T23:49:18-04:00"
		}
	]
}