{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A253315",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 253315,
			"data": "0,0,1,1,2,2,3,3,3,3,2,2,1,1,0,0,4,4,5,5,6,6,7,7,7,7,6,6,5,5,4,4,5,5,4,4,7,7,6,6,6,6,7,7,4,4,5,5,1,1,0,0,3,3,2,2,2,2,3,3,0,0,1,1,6,6,7,7,4,4,5,5,5,5,4,4,7,7,6,6,2,2,3,3,0,0,1,1,1,1,0,0,3,3,2,2,3,3,2,2,1,1,0,0,0",
			"name": "a(n) = bitwise XOR of all the bit numbers for the bits that are set in n.",
			"comment": [
				"The least significant bit is numbered 0.",
				"For any x \u003c 2^m, for any y \u003c m, there exist x' \u003c 2^m s.t. x' differs from x by a single bit and a(x') = y.",
				"Because of the above property, sequence a is a solution to the \"coins on a chessboard\" problem which states: given an 8x8 chessboard filled with coins randomly flipped \"head\" or \"tail\" and a cell number (from 0 to 63) find a way to communicate the cell number by flipping a single coin.",
				"See A261283(n) = a(2n) for the version where the terms are not duplicated, which is equivalent to number the bits starting with 1 for the LSB. - _M. F. Hasler_, Aug 14 2015"
			],
			"link": [
				"Philippe Beaudoin, \u003ca href=\"/A253315/b253315.txt\"\u003eTable of n, a(n) for n = 0..4095\u003c/a\u003e",
				"O. Nash, \u003ca href=\"http://ocfnash.wordpress.com/2009/10/31/yet-another-prisoner-puzzle/\"\u003eYet another prisoner puzzle\u003c/a\u003e, coins on a chessboard problem."
			],
			"formula": [
				"a(n) = f(0,0,n) where f(z,y,x) = if x = 0 then y else f (z+1) (y XOR (z * (x mod 2))) floor(x/2). - _Reinhard Zumkeller_, Jan 18 2015"
			],
			"example": [
				"a(12) = a(0b1100) = XOR(2, 3) = XOR(0b10, 0b11) = 1, where the prefix \"0b\" means that the number is written in binary."
			],
			"maple": [
				"# requires Maple 12 or later",
				"b:= proc(n) local t, L,i;",
				"  L:= convert(n,base,2);",
				"  t:= 0;",
				"  for i from 1 to nops(L) do if L[i]=1 then",
				"    t:= Bits:-Xor(t,i-1)",
				"  fi od;",
				"  t;",
				"end proc:",
				"seq(b(n),n=0..100); # _Robert Israel_, Dec 30 2014"
			],
			"mathematica": [
				"a[n_] := BitXor @@ Flatten[Position[IntegerDigits[n, 2] // Reverse, 1] - 1]; Table[a[n], {n, 0, 100}] (* _Jean-François Alcover_, Jan 07 2015 *)"
			],
			"program": [
				"(Python)",
				"def a(n):",
				"..r = 0",
				"..b = 0",
				"..while n \u003e 0:",
				"....if (n \u0026 1):",
				"......r = r ^ b",
				"....b = b + 1",
				"....n = n \u003e\u003e 1",
				"..return r",
				"(Haskell)",
				"import Data.Bits (xor)",
				"a253315 :: Integer -\u003e Integer",
				"a253315 = f 0 0 where",
				"   f _ y 0 = y",
				"   f z y x = f (z + 1) (y `xor` b * z) x' where (x', b) = divMod x 2",
				"-- _Reinhard Zumkeller_, Jan 18 2015",
				"(PARI) A253315(n, b=bittest(n, 1))={for(i=2, #binary(n), bittest(n, i)\u0026\u0026b=bitxor(b, i)); b} \\\\ _M. F. Hasler_, Aug 14 2015"
			],
			"keyword": "nonn,base,easy",
			"offset": "0,5",
			"author": "_Philippe Beaudoin_, Dec 30 2014",
			"references": 4,
			"revision": 55,
			"time": "2018-02-23T09:30:15-05:00",
			"created": "2015-01-16T10:46:22-05:00"
		}
	]
}