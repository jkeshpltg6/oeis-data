{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005324",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5324,
			"id": "M3902",
			"data": "1,5,20,70,230,726,2235,6765,20240,60060,177177,520455,1524120,4453320,12991230,37854954,110218905,320751445,933149470,2714401580,7895719634,22969224850,66829893650,194486929650,566141346225,1648500576021",
			"name": "Column of Motzkin triangle A026300.",
			"comment": [
				"a(n) = number of (s(0), s(1), ..., s(n)) such that s(i) is a nonnegative integer and |s(i) - s(i-1)| \u003c= 1 for i = 1,2,...,n, s(0) = 0, s(n) = 4. - _Clark Kimberling_",
				"a(n) = T(n,n-4), where T is the array in A026300."
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"R. De Castro, A. L. Ramírez and J. L. Ramírez, \u003ca href=\"http://arxiv.org/abs/1310.2449\"\u003eApplications in Enumerative Combinatorics of Infinite Weighted Automata and Graphs\u003c/a\u003e, arXiv preprint arXiv:1310.2449 [cs.DM], 2013.",
				"R. Donaghey and L. W. Shapiro, \u003ca href=\"http://dx.doi.org/10.1016/0097-3165(77)90020-6\"\u003eMotzkin numbers\u003c/a\u003e, J. Combin. Theory, Series A, 23 (1977), 291-301.",
				"Nickolas Hein, Jia Huang, \u003ca href=\"https://arxiv.org/abs/1807.04623\"\u003eVariations of the Catalan numbers from some nonassociative binary operations\u003c/a\u003e, arXiv:1807.04623 [math.CO], 2018.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0912.0072\"\u003eUne méthode pour obtenir la fonction génératrice d'une série\u003c/a\u003e, arXiv:0912.0072 [math.NT], 2009; FPSAC 1993, Florence. Formal Power Series and Algebraic Combinatorics."
			],
			"formula": [
				"G.f.: z^4*M^5, where M is g.f. of Motzkin numbers (A001006).",
				"a(n) = (-5*I*(-1)^n*(n^4-6*n^3-43*n^2-24*n+36)*3^(1/2)*hypergeom([1/2, n+2],[1],4/3)+15*I*(-1)^n*(n^4+6*n^3+17*n^2+24*n-12)*3^(1/2)*hypergeom([1/2, n+1],[1],4/3))/(6*(n+3)*(n+2)*(n+4)*(n+5)*(n+6)). - _Mark van Hoeij_, Oct 29 2011",
				"a(n) (n + 9) (n - 1) = (n + 3) (3 n + 6) a(n - 2) + (n + 3) (2 n + 7) a(n - 1). - _Simon Plouffe_, Feb 09 2012",
				"a(n) =  5*sum(j=ceiling((n-4)/2)..(n+1), binomial(j,2*j-n+4)*binomial(n+1,j))/(n+1). - _Vladimir Kruchinin_, Mar 17 2014"
			],
			"mathematica": [
				"T[n_, k_] := Sum[m = 2j+n-k; Binomial[n, m] (Binomial[m, j] - Binomial[m, j-1]), {j, 0, k/2}];",
				"a[n_] := T[n, n-4];",
				"Table[a[n], {n, 4, 30}] (* _Jean-François Alcover_, Jul 27 2018 *)"
			],
			"program": [
				"(Maxima) a(n) := 5*sum(binomial(j,2*j-n+4)*binomial(n+1,j),j,ceiling((n-4)/2),(n+1))/(n+1); /* _Vladimir Kruchinin_, Mar 18 2014 */"
			],
			"xref": [
				"Cf. A026300.",
				"A diagonal of triangle A020474."
			],
			"keyword": "nonn,easy",
			"offset": "4,2",
			"author": "_N. J. A. Sloane_",
			"references": 3,
			"revision": 53,
			"time": "2021-03-12T22:32:36-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}