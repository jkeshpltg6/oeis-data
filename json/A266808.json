{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A266808",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 266808,
			"data": "-168,-560,-101124,-3288624,-180132168,-7998247028,-384048485640,-17892957477264,-843263161727364,-39567408316416848,-1859687400468342888,-87350263553726629620,-4103880417768964672104,-192790045902230868971504,-9057117701582885083841028",
			"name": "Coefficient of x in the minimal polynomial of the continued fraction [1^n,sqrt(2)+sqrt(3),1,1,...], where 1^n means n ones.",
			"comment": [
				"See A265762 for a guide to related sequences."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A266808/b266808.txt\"\u003eTable of n, a(n) for n = 0..595\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_09\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (34, 714, -4641, -12376, 12376, 4641, -714, -34, 1)."
			],
			"formula": [
				"a(n) = 34*a(n-1) + 714*a(n-2) - 4641*a(n-3) - 12376*a(n-4) + 12376*a(n-5) + 4641*a(n-6) - 714*a(n-7) - 34*a(n-8) + a(n-9).",
				"G.f.:  -((4 (-42 + 1288 x + 9467 x^2 - 57564 x^3 - 198636 x^4 + 39086 x^5 - 5774 x^6 - 48 x^7 + 3 x^8))/(-1 + 34 x + 714 x^2 - 4641 x^3 - 2376 x^4 + 12376 x^5 + 4641 x^6 - 714 x^7 - 34 x^8 + x^9))."
			],
			"example": [
				"Let u = sqrt(2) and v = sqrt(3), and let p(n,x) be the minimal polynomial of the number given by the n-th continued fraction:",
				"[u+v,1,1,1,...] has p(0,x) = 49 - 168 x - 50 x^2 + 212 x^3 + 47 x^4 - 68 x^5 - 18 x^6 + 4 x^7 + x^8, so that a(0) = -168.",
				"[1,u+v,1,1,1,...] has p(1,x) = 49 - 560 x + 2498 x^2 - 5760 x^3 + 7547 x^4 - 5760 x^5 + 2498 x^6 - 560 x^7 + 49 x^8, so that a(1) = -560;",
				"[1,1,u+v,1,1,1...] has p(2,x) = 25281 - 101124 x + 173262 x^2 - 165852 x^3 + 96847 x^4 - 35252 x^5 + 7790 x^6 - 952 x^7 + 49 x^8, so that a(2) = -101124."
			],
			"mathematica": [
				"u[n_] := Table[1, {k, 1, n}]; t[n_] := Join[u[n], {Sqrt[2] + Sqrt[3]}, {{1}}];",
				"f[n_] := FromContinuedFraction[t[n]];",
				"t = Table[MinimalPolynomial[f[n], x], {n, 0, 40}];",
				"Coefficient[t, x, 0];  (* A266803 *)",
				"Coefficient[t, x, 1];  (* A266808 *)",
				"Coefficient[t, x, 2];  (* A267061 *)",
				"Coefficient[t, x, 3];  (* A267062 *)",
				"Coefficient[t, x, 4];  (* A267063 *)",
				"Coefficient[t, x, 5];  (* A267064 *)",
				"Coefficient[t, x, 6];  (* A267065 *)",
				"Coefficient[t, x, 7];  (* A267066 *)",
				"Coefficient[t, x, 8];  (* A266803 *)"
			],
			"xref": [
				"Cf. A265762, A266803, A267061, A267062, A267063, A267064, A267065, A267066."
			],
			"keyword": "sign,easy",
			"offset": "0,1",
			"author": "_Clark Kimberling_, Jan 10 2016",
			"references": 9,
			"revision": 8,
			"time": "2017-09-23T03:18:10-04:00",
			"created": "2016-01-10T18:14:03-05:00"
		}
	]
}