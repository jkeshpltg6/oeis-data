{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001088",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1088,
			"data": "1,1,2,4,16,32,192,768,4608,18432,184320,737280,8847360,53084160,424673280,3397386240,54358179840,326149079040,5870683422720,46965467381760,563585608581120,5635856085811200,123988833887846400,991910671102771200,19838213422055424000",
			"name": "Product of totient function: a(n) = Product_{k=1..n} phi(k) (cf. A000010).",
			"comment": [
				"a(n) is also the determinant of the symmetric n X n matrix M defined by M(i,j) = gcd(i,j) for 1 \u003c= i,j \u003c= n [Smith and Mansion]. - Avi Peretz (njk(AT)netvision.net.il), Mar 20 2001",
				"The matrix M(i,j) = gcd(i,j) is sequence A003989. - _Michael Somos_, Jun 25 2012"
			],
			"reference": [
				"E. C. Catalan, Théorème de MM. Smith et Mansion, Nouvelle correspondance mathématique, 4 (1878) 103-112. [_Philippe Deléham_, Dec 22 2003]",
				"D. E. Knuth, The Art of Computer Programming. Addison-Wesley, Reading, MA, Vol. 2, p. 598.",
				"M. Petkovsek et al., A=B, Peters, 1996, p. 21."
			],
			"link": [
				"Antoine Mathys, \u003ca href=\"/A001088/b001088.txt\"\u003eTable of n, a(n) for n = 1..496\u003c/a\u003e (first 100 terms by T. D. Noe)",
				"Antal Bege, \u003ca href=\"http://www.emis.de/journals/AUSM/C1-1/MATH1-4.PDF\"\u003eHadamard product of GCD matrices\u003c/a\u003e, Acta Univ. Sapientiae, Mathematica, 1, 1 (2009) 43-49.",
				"Warren P. Johnson, \u003ca href=\"http://www.jstor.org/stable/3654887\"\u003eAn LDU Factorization in Elementary Number Theory\u003c/a\u003e, Mathematics Magazine, 76 (2003), 392-394.",
				"P. Mansion, \u003ca href=\"https://archive.org/stream/messengermathem01glaigoog#page/n94/mode/2up\"\u003eOn an Arithmetical Theorem of Professor Smith's\u003c/a\u003e, Messenger of Mathematics, (1878), pp. 81-82.",
				"Mathoverflow, \u003ca href=\"http://mathoverflow.net/questions/230318/asymptotics-of-product-of-eulers-totient-function-a001088\"\u003eAsymptotics of product of Euler's totient function\u003c/a\u003e, 2016.",
				"H. J. S. Smith, \u003ca href=\"http://plms.oxfordjournals.org/content/s1-7/1/208.extract\"\u003eOn the value of a certain arithmetical determinant\u003c/a\u003e, Proc. London Math. Soc. 7 (1875-1876), pp. 208-212.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/LePaigesTheorem.html\"\u003eLe Paige's Theorem\u003c/a\u003e",
				"\u003ca href=\"/index/Di#divseq\"\u003eIndex to divisibility sequences\u003c/a\u003e"
			],
			"formula": [
				"a(n) = phi(1) * phi(2) * ... * phi(n).",
				"Limit_{n-\u003einfinity} a(n)^(1/n) / n  = exp(-1) * A124175 = 0.205963050288186353879675428232497466485878059342058515016427881513657493... (see Mathoverflow link). - _Vaclav Kotesovec_, Jun 09 2021"
			],
			"example": [
				"a(2) = 1 because the matrix M is: [1,1; 1,2] and det(A) = 1."
			],
			"maple": [
				"with(numtheory,phi); A001088 := proc(n) local i; mul(phi(i),i=1..n); end;"
			],
			"mathematica": [
				"A001088[n_]:=Times@@EulerPhi/@Range[n]; Table[A001088[n], {n, 30}] (* _Enrique Pérez Herrero_, Sep 19 2010 *)",
				"Rest[FoldList[Times,1,EulerPhi[Range[30]]]] (* _Harvey P. Dale_, Dec 09 2011 *)"
			],
			"program": [
				"(Haskell)",
				"a001088 n = a001088_list !! (n-1)",
				"a001088_list = scanl1 (*) a000010_list",
				"-- _Reinhard Zumkeller_, Mar 04 2012",
				"(PARI) a(n)=prod(k=1,n,eulerphi(k)) \\\\ _Charles R Greathouse IV_, Mar 04 2012",
				"(GAP) List([1..30],n-\u003eProduct([1..n],i-\u003ePhi(i))); # _Muniru A Asiru_, Jul 31 2018"
			],
			"xref": [
				"Cf. A000010, A060238, A060239, A059381, A059382, A059383, A059384, A002088.",
				"Cf. A003989."
			],
			"keyword": "nonn,nice,easy",
			"offset": "1,3",
			"author": "_Simon Plouffe_",
			"references": 33,
			"revision": 76,
			"time": "2021-06-09T07:31:09-04:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}