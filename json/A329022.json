{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A329022",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 329022,
			"data": "1,14,7,3,6,4,8,5,10,2,9,17,28,43,13,15,26,24,11,20,32,48,29,44,63,25,12,21,34,18,30,45,27,16,31,19,35,22,39,57,36,23,37,54,75,51,71,95,68,91,65,46,69,49,33,53,74,50,70,47,66,89,116,42,40,58,80,55,38,56,77,102,131,52,72,96,124",
			"name": "Squares visited by a knight moving on a diagonal spiral numbered board and moving to the lowest available unvisited square at each step.",
			"comment": [
				"This sequence uses a diagonal spiral of numbers to enumerate the squares on the board. The knight starts on the square with number 1. At each step the knight goes to an unvisited square with the smallest number.",
				"The sequence if finite. After 3722 steps the square with number 3541 is visited, after which all neighboring squares have been visited."
			],
			"link": [
				"Scott R. Shannon, \u003ca href=\"/A329022/b329022.txt\"\u003eTable of n, a(n) for n = 1..3723\u003c/a\u003e",
				"Scott R. Shannon, \u003ca href=\"/A329022/a329022.png\"\u003eImage showing the 3722 steps of the knight's path\u003c/a\u003e. The green dot is the starting square and the red dot the final square. Blue dots show the eight occupied squares surrounding the final square.",
				"N. J. A. Sloane and Brady Haran, \u003ca href=\"https://www.youtube.com/watch?v=RGQe8waGJ4w\"\u003eThe Trapped Knight\u003c/a\u003e, Numberphile video (2019)."
			],
			"example": [
				"The board is numbered in a spiral moving along the diagonals of the square grid:",
				".",
				"                       19",
				"                     /   \\",
				"                   /       \\",
				"                 20    9     18",
				"               /     /   \\     \\",
				"             /     /       \\     \\",
				"           21    10    3     8     17",
				"         /     /     /   \\     \\     \\",
				"       /     /     /       \\     \\     \\",
				"     22    11    4     1 --- 2     7     16",
				"       \\     \\     \\             /     /     .",
				"         \\     \\     \\         /     /     .",
				"           23    12    5 --- 6     15    28",
				"             \\     \\             /     /",
				"               \\     \\         /     /",
				"                 24    13 -- 14    27",
				"                   \\             /",
				"                     \\         /",
				"                       25 -- 26",
				".",
				"    +----+----+----+----+----+----+----+",
				"    | 76 | 53 | 34 | 19 | 32 | 49 | 70 |",
				"    +----+----+----+----+----+----+----+",
				"    | 54 | 35 | 20 |  9 | 18 | 31 | 48 |",
				"    +----+----+----+----+----+----+----+",
				"    | 36 | 21 | 10 |  3 |  8 | 17 | 30 |",
				"    +----+----+----+----+----+----+----+",
				"    | 22 | 11 |  4 |  1 |  2 |  7 | 16 |",
				"    +----+----+----+----+----+----+----+",
				"    | 38 | 23 | 12 |  5 |  6 | 15 | 28 |",
				"    +----+----+----+----+----+----+----+",
				"    | 58 | 39 | 24 | 13 | 14 | 27 | 44 |",
				"    +----+----+----+----+----+----+----+",
				"    | 82 | 59 | 40 | 25 | 26 | 43 | 64 |",
				"    +----+----+----+----+----+----+----+",
				"."
			],
			"xref": [
				"Cf. A316667.",
				"Cf. A010751(n), A305258(n) for coordinates of point number n+1."
			],
			"keyword": "nonn,fini,full,walk",
			"offset": "1,2",
			"author": "_Scott R. Shannon_, Nov 02 2019",
			"references": 4,
			"revision": 13,
			"time": "2020-07-02T08:49:17-04:00",
			"created": "2019-11-06T17:52:31-05:00"
		}
	]
}