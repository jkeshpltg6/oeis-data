{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A123531",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 123531,
			"data": "1,1,1,4,8,9,4,1,7,25,57,87,89,56,16,1,10,51,171,411,735,986,977,684,304,64,1,13,86,378,1219,3027,5930,9254,11485,11185,8304,4448,1536,256,1,16,130,705,2835,8918,22618,47055,81005,115630,136300,131225,101140",
			"name": "Triangle read by rows: CP(n,i) for n\u003e=0 and 3n+1 \u003e= i \u003e= 0, gives the absolute value of the coefficients of the chromatic polynomial of C_3 X P_(n+1) factored in the form x(x-1)^i.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A123531/b123531.txt\"\u003eRows n = 0..81, flattened\u003c/a\u003e",
				"T. Pfaff and J. Walker, \u003ca href=\"https://projecteuclid.org/euclid.mjms/1316032776\"\u003eThe Chromatic Polynomial of P_2 X P_n and C_3 X P_n\u003c/a\u003e,  Missouri J. Math. Sci., 20, Issue 3 (2008), 169-177."
			],
			"formula": [
				"CP(n,i) = CP(n-1,i) +3*CP(n-1,i-1) +5*CP(n-1,i-2) +4*CP(n-1,i-3), with CP(0,0) = CP(0,1) = 1; n\u003e=0 and 3n+1 \u003e= i \u003e= 0."
			],
			"example": [
				"The chromatic polynomial of C_3 X P_2 is: x(x-1)^5 -4*x(x-1)^4 +8*x(x-1)^3 -9*x(x-1)^2 +4*x(x-1)^1 and so CP(1,0) = 1, CP(1,1) = 4, CP(1,2) = 8, CP(1,3) = 9 and CP(1,4) = 4.",
				"Triangle begins:",
				"1,  1;",
				"1,  4,  8,   9,    4;",
				"1,  7, 25,  57,   87,   89,   56,   16;",
				"1, 10, 51, 171,  411,  735,  986,  977,   684,   304,   64;",
				"1, 13, 86, 378, 1219, 3027, 5930, 9254, 11485, 11185, 8304, 4448, 1536, 256;"
			],
			"maple": [
				"CP:= proc(n, i) option remember;",
				"       `if`(n=0 and (i=0 or i=1), 1, `if`(n\u003c0 or i\u003c0, 0,",
				"        CP(n-1, i) +3*CP(n-1, i-1) +5*CP(n-1, i-2) +4*CP(n-1, i-3)))",
				"     end:",
				"seq(seq(CP(n, i), i=0..3*n+1), n=0..6); # _Alois P. Heinz_, Apr 30 2012"
			],
			"mathematica": [
				"CP[0, 0] = CP[0, 1] = 1;",
				"CP[n_ /; n \u003e= 0, i_] /; 0 \u003c= i \u003c= 3n+1 := CP[n, i] =",
				"     CP[n-1, i] + 3 CP[n-1, i-1] + 5 CP[n-1, i-2] + 4 CP[n-1, i-3];",
				"CP[_, _] = 0;",
				"Table[CP[n, i], {n, 0, 6}, {i, 0, 3n+1}] // Flatten (* _Jean-François Alcover_, Feb 14 2021 *)"
			],
			"xref": [
				"Cf. A027907."
			],
			"keyword": "nonn,tabf",
			"offset": "0,4",
			"author": "Thomas J. Pfaff (tpfaff(AT)ithaca.edu), Oct 02 2006",
			"ext": [
				"Corrected and extended by _Alois P. Heinz_, Apr 30 2012"
			],
			"references": 2,
			"revision": 17,
			"time": "2021-02-14T13:03:11-05:00",
			"created": "2006-10-09T03:00:00-04:00"
		}
	]
}