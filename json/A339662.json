{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A339662",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 339662,
			"data": "0,0,1,0,2,0,3,0,1,2,4,0,5,3,1,0,6,0,7,2,3,4,8,0,2,5,1,3,9,0,10,0,4,6,2,0,11,7,5,2,12,3,13,4,1,8,14,0,3,2,6,5,15,0,4,3,7,9,16,0,17,10,3,0,5,4,18,6,8,2,19,0,20,11,1,7,3,5,21,2,1,12",
			"name": "Greatest gap in the partition with Heinz number n.",
			"comment": [
				"We define the greatest gap of a partition to be the greatest nonnegative integer less than the greatest part and not in the partition.",
				"The Heinz number of a partition (y_1,...,y_k) is prime(y_1)*...*prime(y_k). This gives a bijective correspondence between positive integers and integer partitions.",
				"Also the index of the greatest prime, up to the greatest prime index of n, not dividing n. A prime index of n is a number m such that prime(m) divides n."
			],
			"link": [
				"George E. Andrews and David Newman, \u003ca href=\"https://doi.org/10.1007/s00026-019-00427-w\"\u003ePartitions and the Minimal Excludant\u003c/a\u003e, Annals of Combinatorics, Volume 23, May 2019, Pages 249-254.",
				"FindStat, \u003ca href=\"http://www.findstat.org/StatisticsDatabase/St000474/\"\u003eDyson's crank of a partition\u003c/a\u003e.",
				"Brian Hopkins, James A. Sellers, and Dennis Stanton, \u003ca href=\"https://arxiv.org/abs/2009.10873\"\u003eDyson's Crank and the Mex of Integer Partitions\u003c/a\u003e, arXiv:2009.10873 [math.CO], 2020.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Mex_(mathematics)\"\u003eMex (mathematics)\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A000720(A079068(n))."
			],
			"mathematica": [
				"primeMS[n_]:=If[n==1,{},Flatten[Cases[FactorInteger[n],{p_,k_}:\u003eTable[PrimePi[p],{k}]]]];",
				"maxgap[q_]:=Max@@Complement[Range[0,If[q=={},0,Max[q]]],q];",
				"Table[maxgap[primeMS[n]],{n,100}]"
			],
			"xref": [
				"Positions of first appearances are A000040.",
				"Positions of 0's are A055932.",
				"The version for positions of 1's in reversed binary expansion is A063250.",
				"The prime itself (not just the index) is A079068.",
				"The version for crank is A257989.",
				"The minimal instead of maximal version is A257993.",
				"The version for greatest difference is A286469 or A286470.",
				"Positive integers by Heinz weight and image are counted by A339737.",
				"Positions of 1's are A339886.",
				"A000070 counts partitions with a selected part.",
				"A006128 counts partitions with a selected position.",
				"A015723 counts strict partitions with a selected part.",
				"A056239 adds up prime indices, row sums of A112798.",
				"A073491 lists numbers with gap-free prime indices.",
				"A238709/A238710 count partitions by least/greatest difference.",
				"A342050/A342051 have prime indices with odd/even least gap.",
				"Cf. A001223, A001522, A005117, A018818, A029707, A064391, A098743, A264401, A325351, A333214, A342192."
			],
			"keyword": "nonn",
			"offset": "1,5",
			"author": "_Gus Wiseman_, Apr 20 2021",
			"references": 8,
			"revision": 19,
			"time": "2021-04-22T01:42:43-04:00",
			"created": "2021-04-22T01:42:43-04:00"
		}
	]
}