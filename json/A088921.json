{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A088921",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 88921,
			"data": "1,2,5,13,33,80,185,411,885,1862,3853,7881,15993,32284,64945,130359,261293,523282,1047397,2095781,4192721,8386792,16775145,33552083,67106213,134214750,268432125,536867201,1073737705,2147479092",
			"name": "The number of 321- and 2143-avoiding permutations of length n.",
			"comment": [
				"That is, both the patterns 321 and 2143 are to be avoided. - _N. J. A. Sloane_, Oct 21 2010.",
				"321- and 2143-avoiding permutations of length n are in one-to-one correspondence with simple Dyck paths of semilength n (a Dyck path is simple if it has at most one long upward edge or at most one long downward edge, an edge being \"long\" if it consists of at least two steps). They are the Grassmannian permutations and their inverses. They can also be characterized as those permutations whose essential set is contained in one row or one column. This sequence also enumerates the cyclic arrangements of 1, 2, ... n+1 which avoid the cyclic arrangement 1234.",
				"Also, number of 1324-avoiding circular permutations on [n+1].",
				"Number of sequences (e(1), ..., e(n)), 0 \u003c= e(i) \u003c i, such that there is no triple i \u003c j \u003c k with e(i) != e(j) \u003c e(k) and e(i) != e(k). [Martinez and Savage, 2.9] - _Eric M. Schmidt_, Jul 17 2017"
			],
			"link": [
				"Christian Bean, Bjarki Gudmundsson and Henning Ulfarsson, \u003ca href=\"https://arxiv.org/abs/1705.04109\"\u003eAutomatic discovery of structural rules of permutation classes\u003c/a\u003e, arXiv:1705.04109 [math.CO], 2017.",
				"S. Billey, W. Jockusch and R. P. Stanley, \u003ca href=\"http://www.math.washington.edu/~billey/papers/bjs.pdf\"\u003eSome combinatorial properties of Schubert polynomials\u003c/a\u003e, Journal of Algebraic Combinatorics 2(4):345-374, 1993.",
				"D. Callan, \u003ca href=\"http://arXiv.org/abs/math.CO/0210014\"\u003ePattern avoidance in circular permutations\u003c/a\u003e, arXiv:math/0210014 [math.CO], 2002.",
				"K. Eriksson and S. Linusson, \u003ca href=\"http://dx.doi.org/10.1215/S0012-7094-96-08502-6\"\u003eCombinatorics of Fulton's essential set\u003c/a\u003e, Duke Mathematical Journal 85(1) (1996) 61-76.",
				"Juan B. Gil and Jessica A. Tomasko, \u003ca href=\"https://arxiv.org/abs/2112.03338\"\u003eRestricted Grassmannian permutations\u003c/a\u003e, arXiv:2112.03338 [math.CO], 2021.",
				"Megan A. Martinez and Carla D. Savage, \u003ca href=\"https://arxiv.org/abs/1609.08106\"\u003ePatterns in Inversion Sequences II: Inversion Sequences Avoiding Triples of Relations\u003c/a\u003e, arXiv:1609.08106 [math.CO], 2016.",
				"A. Vella, \u003ca href=\"https://doi.org/10.37236/1690\"\u003ePattern avoidance in permutations: linear and cyclic orders\u003c/a\u003e, Electron. J. Combin. 9 (2002/03), no. 2, #R18, 43 pp.",
				"Chunyan Yan and Zhicong Lin, \u003ca href=\"https://arxiv.org/abs/1912.03674\"\u003eInversion sequences avoiding pairs of patterns\u003c/a\u003e, arXiv:1912.03674 [math.CO], 2019."
			],
			"formula": [
				"a(n) = 2^(n+1) - binomial(n+1, 3) - 2*n - 1.",
				"G.f.: x*(2*x^4-5*x^3+7*x^2-4*x+1)/((1-2*x)*(1-x)^4). - _Emeric Deutsch_, Feb 22 2004"
			],
			"mathematica": [
				"Table[2^(n + 1) - Binomial[n + 1, 3] - 2 n - 1, {n, 30}] (* or *)",
				"Rest@ CoefficientList[Series[x (2 x^4 - 5 x^3 + 7 x^2 - 4 x + 1)/((1 - 2 x) (1 - x)^4), {x, 0, 30}], x] (* _Michael De Vlieger_, May 13 2017 *)"
			],
			"program": [
				"(PARI) a(n) = 2^(n+1) - binomial(n+1, 3) - 2*n - 1 \\\\ _Michel Marcus_, Jul 11 2013"
			],
			"xref": [
				"Cf. A000325."
			],
			"keyword": "easy,nonn",
			"offset": "1,2",
			"author": "Antoine Vella (avella(AT)math.uwaterloo.ca), Oct 23 2003",
			"references": 2,
			"revision": 35,
			"time": "2021-12-10T11:17:51-05:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}