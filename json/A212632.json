{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A212632",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 212632,
			"data": "1,1,1,1,2,2,1,1,2,2,2,2,2,2,2,1,2,3,1,2,2,2,3,2,3,3,3,2,2,3,2,1,3,2,2,3,2,2,3,2,3,3,2,2,3,3,3,2,2,3,2,3,1,4,3,2,2,3,2,3,3,3,3,1,3,3,2,2,3,3,2,3,3,3,3,2,3,4,2,2,4,3,3,3,3,3,3,2",
			"name": "The domination number of the rooted tree with Matula-Goebel number n.",
			"comment": [
				"The domination number of a simple graph G is the minimum cardinality of a dominating subset of G.",
				"The Matula-Goebel number of a rooted tree can be defined in the following recursive manner: to the one-vertex tree there corresponds the number 1; to a tree T with root degree 1 there corresponds the t-th prime number, where t is the Matula-Goebel number of the tree obtained from T by deleting the edge emanating from the root; to a tree T with root degree m\u003e=2 there corresponds the product of the Matula-Goebel numbers of the m branches of T."
			],
			"link": [
				"S. Alikhani and Y. H. Peng, \u003ca href=\"http://arxiv.org/abs/0905.2251\"\u003eIntroduction to domination polynomial of a graph\u003c/a\u003e, arXiv:0905.2251 [math.CO], 2009.",
				"É. Czabarka, L. Székely, and S. Wagner, \u003ca href=\"http://dx.doi.org/10.1016/j.dam.2009.07.004\"\u003eThe inverse problem for certain tree parameters\u003c/a\u003e, Discrete Appl. Math., 157, 2009, 3314-3319.",
				"E. Deutsch, \u003ca href=\"http://arxiv.org/abs/1111.4288\"\u003eRooted tree statistics from Matula numbers\u003c/a\u003e, arXiv:1111.4288 [math.CO], 2011.",
				"F. Goebel, \u003ca href=\"http://dx.doi.org/10.1016/0095-8956(80)90049-0\"\u003eOn a 1-1-correspondence between rooted trees and natural numbers\u003c/a\u003e, J. Combin. Theory, B 29 (1980), 141-143.",
				"I. Gutman and A. Ivic, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(95)00182-V\"\u003eOn Matula numbers\u003c/a\u003e, Discrete Math., 150, 1996, 131-142.",
				"I. Gutman and Yeong-Nan Yeh, \u003ca href=\"http://www.emis.de/journals/PIMB/067/3.html\"\u003eDeducing properties of trees from their Matula numbers\u003c/a\u003e, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"D. W. Matula, \u003ca href=\"http://www.jstor.org/stable/2027327\"\u003eA natural rooted tree enumeration by prime factorization\u003c/a\u003e, SIAM Rev. 10 (1968) 273.",
				"\u003ca href=\"/index/Mat#matula\"\u003eIndex entries for sequences related to Matula-Goebel numbers\u003c/a\u003e"
			],
			"formula": [
				"In A212630 one gives the domination polynomial P(n)=P(n,x) of the rooted tree with Matula-Goebel number n. We have a(n) = least exponent in P(n,x)."
			],
			"example": [
				"a(5)=2 because the rooted tree with Matula-Goebel number 5 is the path tree R - A - B - C; {A,B} is a dominating subset and there is no dominating subset of smaller cardinality."
			],
			"maple": [
				"with(numtheory): P := proc (n) local r, s, A, B, C:",
				"r := n -\u003e op(1, factorset(n)): s := n-\u003e n/r(n):",
				"A := proc (n) if n = 1 then x elif bigomega(n) = 1 then x*(A(pi(n))+B(pi(n))+C(pi(n))) else A(r(n))*A(s(n))/x end if end proc:",
				"B := proc (n) if n = 1 then 0 elif bigomega(n) = 1 then A(pi(n)) else sort(expand(B(r(n))*B(s(n))+B(r(n))*C(s(n))+B(s(n))*C(r(n)))) end if end proc:",
				"C := proc (n) if n = 1 then 1 elif bigomega(n) = 1 then B(pi(n)) else expand(C(r(n))*C(s(n))) end if end proc:",
				"sort(expand(A(n)+B(n))) end proc:",
				"A212632 := n-\u003edegree(P(n))-degree(numer(subs(x = 1/x, P(n)))): seq(A212632(n), n = 1 .. 120);"
			],
			"mathematica": [
				"A[n_] := Which[n == 1, x, PrimeOmega[n] == 1, x*(A[PrimePi[n]] + B[PrimePi[n]] + c[PrimePi[n]]), True, A[r[n]]*A[s[n]]/x];",
				"B[n_] := Which[n == 1, 0, PrimeOmega[n] == 1, A[PrimePi[n]], True, Expand[B[r[n]]*B[s[n]] + B[r[n]]*c[s[n]] + B[s[n]]*c[r[n]]]];",
				"c[n_] := Which[n == 1, 1, PrimeOmega[n] == 1, B[PrimePi[n]], True, Expand[c[r[n]]*c[s[n]]]];",
				"r[n_] :=  FactorInteger[n][[1, 1]];",
				"s[n_] := n/r[n];",
				"P[n_] := Expand[A[n] + B[n]];",
				"a[n_] := Exponent[P[n], x] - Exponent[Numerator[P[n] /. x -\u003e 1/x // Together], x];",
				"Array[a, 100] (* _Jean-François Alcover_, Nov 14 2017, after _Emeric Deutsch_ *)"
			],
			"xref": [
				"Cf. A212618 - A212631."
			],
			"keyword": "nonn",
			"offset": "1,5",
			"author": "_Emeric Deutsch_, Jun 11 2012",
			"references": 15,
			"revision": 20,
			"time": "2021-10-04T13:12:56-04:00",
			"created": "2012-06-13T13:01:49-04:00"
		}
	]
}