{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A342089",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 342089,
			"data": "5,12,16,23,30,34,41,45,52,59,63,70,77,81,88,92,99,106,110,117,121,128,135,139,146,153,157,164,168,175,182,186,193,200,204,211,215,222,229,233,240,244,251,258,262,269,276,280,287,291,298,305,309,316,320,327",
			"name": "Numbers that have two representations as the sum of distinct non-consecutive Lucas numbers (A000032).",
			"comment": [
				"Brown (1969) proved that every positive number has a unique representation as a sum of non-consecutive Lucas numbers, if L(0) = 2 and L(2) = 3 do not appear simultaneously in the representation.",
				"Chu et al. (2020) proved that if L(0) and L(2) are allowed to appear simultaneously, then each positive number can have at most two representations. The terms with two representations are listed in this sequence. They found that the number of terms that are not exceeding 10^k, for k = 1, 2, ..., are 1, 17, 171, 1708, 17082, 170820, ..., and proved that the asymptotic density of this sequence is 1/(3*phi+1) = 0.1708203932... (A176015 - 1), where phi is the golden ratio (A001622)."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A342089/b342089.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"John L. Brown, Jr., \u003ca href=\"https://www.fq.math.ca/Scanned/7-3/brown.pdf\"\u003eUnique representation of integers as sums of distinct Lucas numbers\u003c/a\u003e, The Fibonacci Quarterly, Vol. 7, No. 3 (1969), pp. 243-252.",
				"Hung V. Chu, David C. Luo and Steven J. Miller, \u003ca href=\"https://arxiv.org/abs/2004.08316\"\u003eOn Zeckendorf Related Partitions Using the Lucas Sequence\u003c/a\u003e, arXiv:2004.08316 [math.NT], 2020-2021.",
				"David C. Luo, \u003ca href=\"https://github.com/dluo6745/Zeckendorf-Partitions/blob/master/ZP.java\"\u003eJava code for calculating non-consecutive partitions of natural numbers in any infinite integer sequence given by a second-order linear recurrence\u003c/a\u003e, GitHub."
			],
			"example": [
				"5 is a term since is has two representations: L(0) + L(2) = 2 + 3 and L(1) + L(3) = 1 + 4.",
				"12 is a term since is has two representations: L(1) + L(5) = 1 + 11 and L(0) + L(2) + L(4) = 2 + 3 + 7."
			],
			"maple": [
				"L:= [seq(combinat:-fibonacci(n+1)+combinat:-fibonacci(n-1), n=0..40)]:",
				"f1:= proc(n, m) option remember;",
				"      if n = 0 then return 1 fi;",
				"      if m \u003c= 0 then 0",
				"      elif L[m] \u003c= n then procname(n - L[m],m-2) + procname(n, m-1)",
				"      else procname(n,m-1)",
				"      fi",
				"end proc:",
				"filter:= n -\u003e f1(n,ListTools:-BinaryPlace(L,n+1))=2:",
				"select(filter, [$1..1000]); # _Robert Israel_, Mar 10 2021"
			],
			"program": [
				"(Java) See David C. Luo's GitHub link."
			],
			"xref": [
				"Cf. A000032, A001622, A130310, A116543, A176015."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Amiram Eldar_, Feb 27 2021",
			"references": 1,
			"revision": 8,
			"time": "2021-03-11T02:46:53-05:00",
			"created": "2021-02-27T21:29:09-05:00"
		}
	]
}