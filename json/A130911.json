{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A130911",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 130911,
			"data": "1,0,-1,0,1,2,1,2,1,0,1,2,3,2,3,2,3,4,5,4,5,6,5,4,5,4,5,6,7,6,7,8,9,8,7,8,9,8,9,10,11,12,13,14,13,14,15,16,17,18,19,20,21,22,21,20,19,20,19,18,19,18,19,18,19,18,19,18,17,16,15,14,15,14,15,14,13,14,13,14,15,16,17,18,19,20,19,20,19,20,19,18,19,20,21,20,19",
			"name": "a(n) is the number of primes with odd binary weight among the first n primes minus the number with an even binary weight.",
			"comment": [
				"Prime race between evil primes (A027699) and odious primes (A027697).",
				"Shevelev conjectures that a(n) \u003e= 0 for n \u003e 3. Surprisingly, the conjecture also appears to be true if we count zeros instead of ones in the binary representation of prime numbers.",
				"The conjecture is true for primes up to at least 10^13. Mauduit and Rivat prove that half of all primes are evil. - _T. D. Noe_, Feb 09 2009"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A130911/b130911.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"CNRS Press release, \u003ca href=\"http://www2.cnrs.fr/en/1732.htm\"\u003eThe sum of digits of prime numbers is evenly distributed\u003c/a\u003e, May 12, 2010.",
				"Christian Mauduit and Joël Rivat, \u003ca href=\"http://annals.math.princeton.edu/2010/171-3/p04\"\u003eSur un problème de Gelfond: la somme des chiffres des nombres premiers\u003c/a\u003e, Annals Math., 171 (2010), 1591-1646.",
				"ScienceDaily, \u003ca href=\"http://www.sciencedaily.com/releases/2010/05/100512172533.htm\"\u003eSum of Digits of Prime Numbers Is Evenly Distributed: New Mathematical Proof of Hypothesis\u003c/a\u003e, May 12, 2010.",
				"Vladimir Shevelev, \u003ca href=\"https://arxiv.org/abs/0706.0786\"\u003eA conjecture on primes and a step towards justification\u003c/a\u003e, arXiv:0706.0786 [math.NT], 2007.",
				"Vladimir Shevelev, \u003ca href=\"https://www.arxiv.org/abs/0707.1761\"\u003eOn excess of odious primes\u003c/a\u003e, arXiv:0707.1761 [math.NT], 2007."
			],
			"formula": [
				"a(n) = (number of odious primes \u003c= prime(n)) - (number of evil primes \u003c= prime(n)).",
				"a(n) = A200247(n) - A200246(n)."
			],
			"mathematica": [
				"cnt=0; Table[p=Prime[n]; If[EvenQ[Count[IntegerDigits[p,2],1]], cnt--, cnt++ ]; cnt, {n,10000}]",
				"Accumulate[If[OddQ[DigitCount[#,2,1]],1,-1]\u0026/@Prime[Range[100]]] (* _Harvey P. Dale_, Aug 09 2013 *)"
			],
			"program": [
				"(PARI)f(p)={v=binary(p);s=0;for(k=1,#v,if(v[k]==1,s++)); return(s%2)};nO=0;nE=0;forprime(p=2,520,if(f(p),nO++, nE++);an=nO-nE;print1(an,\", \")) \\\\ _Washington Bomfim_, Jan 14 2011",
				"(Python)",
				"from sympy import nextprime",
				"from itertools import islice",
				"def agen():",
				"    p, evod = 2, [0, 1]",
				"    while True:",
				"        yield evod[1] - evod[0]",
				"        p = nextprime(p); evod[bin(p).count('1')%2] += 1",
				"print(list(islice(agen(), 97))) # _Michael S. Branicky_, Dec 21 2021"
			],
			"xref": [
				"Cf. A095005, A095006.",
				"Cf. A199399, A027697, A027698, A027699, A027700, A200244, A200245, A200246, A200247.",
				"Cf. A156549 (race between primes having an odd/even number of zeros in binary)."
			],
			"keyword": "nice,sign,base",
			"offset": "1,6",
			"author": "_T. D. Noe_, Jun 08 2007",
			"ext": [
				"Edited by _N. J. A. Sloane_, Nov 16 2011"
			],
			"references": 5,
			"revision": 29,
			"time": "2021-12-22T02:18:24-05:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}