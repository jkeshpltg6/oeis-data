{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A129448",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 129448,
			"data": "1,-1,0,1,-2,0,0,-1,0,2,0,0,2,0,0,1,-2,0,0,-2,0,0,0,0,3,-2,0,0,-2,0,0,-1,0,2,0,0,2,0,0,2,-2,0,0,0,0,0,0,0,1,-3,0,2,-2,0,0,0,0,2,0,0,2,0,0,1,-4,0,0,-2,0,0,0,0,2,-2,0,0,0,0,0,-2,0,2,0,0,4,0,0,0,-2,0,0,0,0,0,0,0,2,-1,0,3,-2,0,0,-2,0",
			"name": "Expansion of q * psi(-q) * chi(q^3)^2 * psi(-q^9) in powers of q where psi(), chi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Number 50 of the 74 eta-quotients listed in Table I of Martin (1996)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A129448/b129448.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Y. Martin, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-96-01743-6\"\u003eMultiplicative eta-quotients\u003c/a\u003e, Trans. Amer. Math. Soc. 348 (1996), no. 12, 4825-4856, see page 4852 Table I.",
				"Michael Somos, \u003ca href=\"/A030203/a030203.txt\"\u003eIndex to Yves Martin's list of 74 multiplicative eta-quotients and their A-numbers\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of eta(q) * eta(q^4) * eta(q^6)^4 * eta(q^9) * eta(q^36) / (eta(q^2) * eta(q^3)^2 * eta(q^12)^2 * eta(q^18)) in powers of q.",
				"Euler transform of period 36 sequence [ -1, 0, 1, -1, -1, -2, -1, -1, 0, 0, -1, -1, -1, 0, 1, -1, -1, -2, -1, -1, 1, 0, -1, -1, -1, 0, 0, -1, -1, -2, -1, -1, 1, 0, -1, -2, ...].",
				"a(n) is multiplicative with a(2^e) = (-1)^e, a(3^e) = 0^e, a(p^e) = (1 + (-1)^e) / 2 if p == 7, 11 (mod 12), a(p^e) = e+1 if p == 1 (mod 12), a(p^e) = (-1)^e * (e+1) if p == 5 (mod 12).",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (36 t)) = 6 (t/i) f(t) where q = exp(2 Pi i t).",
				"G.f.: Sum_{k\u003e0} Kronecker(12, k) * x^k/ (1 + x^k + x^(2*k)).",
				"|a(n)| = A091400(n). a(3*n) = a(4*n + 3) = 0. a(2*n) = -a(n). a(3*n + 1) = A122865(n). a(3*n + 2) = - A122856(n). a(4*n + 1) = A121363(n). a(12*n + 1) = A002175(n). a(12*n + 5) = -2 * A121444(n)."
			],
			"example": [
				"G.f. = q - q^2 + q^4 - 2*q^5 - q^8 + 2*q^10 + 2*q^13 + q^16 - 2*q^17 - 2*q^20 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 1, 0, Sum[ KroneckerSymbol[ 12, d] KroneckerSymbol[ -3, n/d], {d, Divisors[ n]}]]; (* _Michael Somos_, Jul 09 2015 *)",
				"a[ n_] := SeriesCoefficient[ q QPochhammer[ -q^3, q^6]^2 EllipticTheta[ 2, Pi/4, q^(1/2)] EllipticTheta[ 2, Pi/4, q^(9/2)] / (2 q^(5/4)), {q, 0, n}]; (* _Michael Somos_, Jul 09 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, 0, sumdiv(n, d, kronecker( 12, d) * kronecker( -3, n/d)))};",
				"(PARI) {a(n) = my(A, p, e); if( n\u003c1, 0, A = factor(n); prod( k=1, matsize(A)[1], [p, e] = A[k, ]; if( p==3, 0, p==2, (-1)^e, p%12\u003e6, !(e%2), (-1)^(e * (p%12==5)) * (e+1))))};",
				"(PARI) {a(n) = my(A); if( n\u003c1, 0, n--; A = x * O(x^n); polcoeff( eta(x + A) * eta(x^4 + A) * eta(x^6 + A)^4 * eta(x^9 + A) * eta(x^36 + A) / (eta(x^2 + A) * eta(x^3 + A)^2 * eta(x^12 + A)^2 * eta(x^18 + A)), n))};",
				"(MAGMA) A := Basis( ModularForms( Gamma1(36), 1), 82); A[2] - A[3] + A[5] - 2*A[6] - A[9] + 2*A[11] + 2*A[14] + A[17] - 2*A[18]; /* _Michael Somos_, Jul 09 2015 */"
			],
			"xref": [
				"Cf. A002175, A091400, A121363, A121444, A122856, A122865."
			],
			"keyword": "sign,mult",
			"offset": "1,5",
			"author": "_Michael Somos_, Apr 16 2007",
			"references": 6,
			"revision": 25,
			"time": "2021-03-12T22:24:44-05:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}