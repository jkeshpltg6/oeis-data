{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A033585",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 33585,
			"data": "0,10,36,78,136,210,300,406,528,666,820,990,1176,1378,1596,1830,2080,2346,2628,2926,3240,3570,3916,4278,4656,5050,5460,5886,6328,6786,7260,7750,8256,8778,9316,9870,10440",
			"name": "a(n) = 2*n*(4*n + 1).",
			"comment": [
				"If Y is a fixed 3-subset of a (4n+1)-set X then a(n) is the number of (4n-1)-subsets of X intersecting Y. - _Milan Janjic_, Oct 28 2007",
				"Sequence found by reading the line from 0, in the direction 0, 10, ..., in the square spiral whose vertices are the triangular numbers A000217. - _Omar E. Pol_, Sep 03 2011"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A033585/b033585.txt\"\u003eTable of n, a(n) for n = 0..5000\u003c/a\u003e",
				"Milan Janjic, \u003ca href=\"http://www.pmfbl.org/janjic/\"\u003eTwo Enumerative Functions\u003c/a\u003e.",
				"Amelia Carolina Sparavigna, \u003ca href=\"https://doi.org/10.5281/zenodo.3471358\"\u003eThe groupoids of Mersenne, Fermat, Cullen, Woodall and other Numbers and their representations by means of integer sequences\u003c/a\u003e, Politecnico di Torino, Italy (2019), [math.NT].",
				"Amelia Carolina Sparavigna, \u003ca href=\"https://doi.org/10.5281/zenodo.3470205\"\u003eThe groupoid of the Triangular Numbers and the generation of related integer sequences\u003c/a\u003e, Politecnico di Torino, Italy (2019).",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1)."
			],
			"formula": [
				"a(n) = 2*A007742(n).",
				"a(n) = A000217(4*n) = A014105(2*n). - _Reinhard Zumkeller_, Sep 17 2008",
				"a(n) = 16*n + a(n-1) - 6 with a(0) = 0. - _Vincenzo Librandi_, Aug 05 2010",
				"a(n) = A005843(n)*A016813(n). - _Omar E. Pol_, Oct 31 2013",
				"G.f.: -2*x*(5+3*x)/(x-1)^3 . - _R. J. Mathar_, Feb 06 2017",
				"E.g.f.: (8*x^2 + 10*x)*exp(x). - _G. C. Greubel_, Jul 18 2017",
				"From _Amiram Eldar_, Jul 22 2020: (Start)",
				"Sum_{n\u003e=1} 1/a(n) = 2 - Pi/4 - 3*log(2)/2.",
				"Sum_{n\u003e=1} (-1)^(n+1)/a(n) = sqrt(2)*Pi/4 + sqrt(2)*arcsinh(1)/2 + log(2)/2 - 2. (End)"
			],
			"maple": [
				"seq(binomial(4*n+1,2), n=0..36); # _Zerinvary Lajos_, Jan 21 2007"
			],
			"mathematica": [
				"f[n_]:=2*n*(4*n+1);f[Range[0,60]] (* _Vladimir Joseph Stephan Orlovsky_, Feb 05 2011 *)"
			],
			"program": [
				"(PARI) a(n)=2*n*(4*n+1) \\\\ _Charles R Greathouse IV_, Jun 16 2017"
			],
			"xref": [
				"Cf. A081266, A144312, A144314. - _Reinhard Zumkeller_, Sep 17 2008"
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 24,
			"revision": 58,
			"time": "2021-05-04T01:03:39-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}