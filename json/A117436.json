{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A117436",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 117436,
			"data": "1,0,1,4,0,1,0,12,0,1,80,0,24,0,1,0,400,0,40,0,1,3904,0,1200,0,60,0,1,0,27328,0,2800,0,84,0,1,354560,0,109312,0,5600,0,112,0,1,0,3191040,0,327936,0,10080,0,144,0,1,51733504,0,15955200,0,819840,0,16800,0,180,0,1",
			"name": "Triangle related to exp(x)*sec(2*x).",
			"comment": [
				"Inverse is A117435.",
				"Conjecture: The d-th diagonal (starting with d=0) is proportional to the sequence of generalized binomial coefficients binomial(-x, d) where x is the column index. - _Søren G. Have_, Feb 26 2017"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A117436/b117436.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"Number triangle whose k-th column has e.g.f. (x^k/k!)*sec(2*x).",
				"T(n, 0) = A002436(n).",
				"Sum_{k=0..n} T(n, k) = A117437(n).",
				"T(n, k) = binomial(n,k) * (2*i)^(n-k) * E(n-k), where E(n) are the Euler numbers with E(2*n) = A000364(n) and E(2*n+1) = 0. - _G. C. Greubel_, Jun 01 2021"
			],
			"example": [
				"Triangle begins as:",
				"         1;",
				"         0,       1;",
				"         4,       0,        1;",
				"         0,      12,        0,      1;",
				"        80,       0,       24,      0,      1;",
				"         0,     400,        0,     40,      0,     1;",
				"      3904,       0,     1200,      0,     60,     0,     1;",
				"         0,   27328,        0,   2800,      0,    84,     0,   1;",
				"    354560,       0,   109312,      0,   5600,     0,   112,   0,   1;",
				"         0, 3191040,        0, 327936,      0, 10080,     0, 144,   0, 1;",
				"  51733504,       0, 15955200,      0, 819840,     0, 16800,   0, 180, 0, 1;"
			],
			"mathematica": [
				"T[n_, k_]:= Binomial[n, k]*(2*I)^(n-k)*EulerE[n-k];",
				"Table[T[n, k], {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, Jun 01 2021 *)"
			],
			"program": [
				"(Sage) flatten([[binomial(n,k)*(2*i)^(n-k)*euler_number(n-k) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Jun 01 2021"
			],
			"xref": [
				"Cf. A000364, A002436 (1st column), A117435 (inverse), A117437 (row sums)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Paul Barry_, Mar 16 2006",
			"references": 3,
			"revision": 23,
			"time": "2021-06-02T22:17:08-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}