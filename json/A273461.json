{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A273461",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 273461,
			"data": "1,2,9,40,484,9717,338724,21624680,2504301849,520443847520,195145309791364,131850659243316222,160668896658179472676,352891729183598844656996,1397187513066371784602204416,9972288382286063615850619475640",
			"name": "Number of physically stable n X n placements of water source-blocks in Minecraft.",
			"comment": [
				"In Minecraft worlds, a source block of water can be reacted with another source block, two blocks away. This reaction creates a third \"infinite\" source block in the unoccupied intermediate block, so called because if the intermediate water source is destroyed or picked up by a player using a bucket, it will immediately regenerate itself.",
				"A placement of water at several positions in an n X n board is said to be *stable* if no infinite water physics can in fact occur (under otherwise optimal conditions). This means that the total quantity of water in the system is held constant.",
				"In short, no two source blocks can be graph-distance 2 from each other. - _Gus Wiseman_, Nov 27 2019",
				"Often incorrectly described as cellular automata, the observed behaviors of liquids within a board are inseparable in certain ways from states of affair outside of the board and events outside of the system. This aspect of Minecraft is poorly understood."
			],
			"link": [
				"EthosLab, \u003ca href=\"https://youtu.be/9uIRD9sMBU0\"\u003eMinecraft - Tutorial: Water\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Distance_(graph_theory)\"\u003eDistance (graph theory)\u003c/a\u003e",
				"Gus Wiseman, \u003ca href=\"/A273461/a273461.png\"\u003eExample of a physically stable arrangement of water source-blocks (n=11)\u003c/a\u003e",
				"Christopher Cormier, \u003ca href=\"/A273461/a273461.cs.txt\"\u003eC# Program\u003c/a\u003e"
			],
			"example": [
				"a(2) = 9: {{}, {(2,2)}, {(2,1)}, {(2,1),(2,2)}, {(1,2)}, {(1,2),(2,2)}, {(1,1)}, {(1,1),(2,1)}, {(1,1),(1,2)}}."
			],
			"mathematica": [
				"stableSets[u_,Q_]:=If[Length[u]===0,{{}},With[{w=First[u]},Join[stableSets[DeleteCases[u,w],Q],Prepend[#,w]\u0026/@stableSets[DeleteCases[u,r_/;r===w||Q[r,w]||Q[w,r]],Q]]]];",
				"allflows[n_]:=stableSets[Join@@Array[List,{n,n}],Function[{v,w},Plus@@Abs/@(w-v)===2]];",
				"Table[Length[allflows[i]],{i,6}] (* _Gus Wiseman_, May 23 2016 *)"
			],
			"xref": [
				"The one-dimensional version is A006498.",
				"Dominated by A329871.",
				"Cf. A002416, A005251, A027624, A114901."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Gus Wiseman_, May 23 2016",
			"ext": [
				"a(7) from _Tae Lim Kook_, May 25 2016",
				"a(8) from _Tae Lim Kook_, May 29 2016",
				"a(7)-a(8) corrected by _Christopher Cormier_, Dec 17 2019",
				"a(9)-a(15) from _Christopher Cormier_, Dec 19 2019"
			],
			"references": 4,
			"revision": 41,
			"time": "2019-12-19T14:20:39-05:00",
			"created": "2016-05-23T08:08:00-04:00"
		}
	]
}