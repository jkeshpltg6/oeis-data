{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A053615",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 53615,
			"data": "0,1,0,1,2,1,0,1,2,3,2,1,0,1,2,3,4,3,2,1,0,1,2,3,4,5,4,3,2,1,0,1,2,3,4,5,6,5,4,3,2,1,0,1,2,3,4,5,6,7,6,5,4,3,2,1,0,1,2,3,4,5,6,7,8,7,6,5,4,3,2,1,0,1,2,3,4,5,6,7,8,9,8,7,6,5,4,3,2,1,0,1,2,3,4,5,6,7,8,9,10,9,8,7",
			"name": "Pyramidal sequence: distance to nearest product of two consecutive integers (promic or heteromecic numbers).",
			"comment": [
				"a(A002378(n)) = 0; a(n^2) = n.",
				"Table A049581 T(n,k) = |n-k| read by sides of squares from T(1,n) to T(n,n), then from T(n,n) to T(n,1). - _Boris Putievskiy_, Jan 29 2013"
			],
			"link": [
				"Harvey P. Dale, \u003ca href=\"/A053615/b053615.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Boris Putievskiy, \u003ca href=\"http://arxiv.org/abs/1212.2732\"\u003eTransformations [of] Integer Sequences And Pairing Functions\u003c/a\u003e arXiv:1212.2732 [math.CO], 2012.",
				"\u003ca href=\"/index/Di#distance_to_the_nearest\"\u003eIndex entries for sequences related to distance to nearest element of some set\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A004738(n+1) - 1.",
				"Let u(1)=1, u(n) = n - u(n-sqrtint(n)) (cf. A037458); then a(0)=0 and for n \u003e 0 a(n) = 2*u(n) - n. - _Benoit Cloitre_, Dec 22 2002",
				"a(0)=0 then a(n) = floor(sqrt(n)) - a(n - floor(sqrt(n))). - _Benoit Cloitre_, May 03 2004",
				"a(n) = |A196199(n)|. a(n) = |n - t^2 - t|, where t = floor(sqrt(n)). - _Boris Putievskiy_, Jan 29 2013 [corrected by _Ridouane Oudra_, May 11 2019]",
				"a(n) = A000194(n) - A053188(n) = t - |t^2 - n|, where t = floor(sqrt(n)+1/2). - _Ridouane Oudra_, May 11 2019"
			],
			"example": [
				"a(10) = |10 - 3*4| = 2.",
				"From _Boris Putievskiy_, Jan 29 2013: (Start)",
				"The start of the sequence as table:",
				"  0, 1, 2, 3, 4, 5, 6, 7, ...",
				"  1, 0, 1, 2, 3, 4, 5, 6, ...",
				"  2, 1, 0, 1, 2, 3, 4, 5, ...",
				"  3, 2, 1, 0, 1, 2, 3, 4, ...",
				"  4, 3, 2, 1, 0, 1, 2, 3, ...",
				"  5, 4, 3, 2, 1, 0, 1, 2, ...",
				"  6, 5, 4, 3, 2, 1, 0, 1, ...",
				"  ...",
				"The start of the sequence as triangle array read by rows:",
				"  0;",
				"  1, 0, 1;",
				"  2, 1, 0, 1, 2;",
				"  3, 2, 1, 0, 1, 2, 3;",
				"  4, 3, 2, 1, 0, 1, 2, 3, 4;",
				"  5, 4, 3, 2, 1, 0, 1, 2, 3, 4, 5;",
				"  6, 5, 4, 3, 2, 1, 0, 1, 2, 3, 4, 5, 6;",
				"  7, 6, 5, 4, 3, 2, 1, 0, 1, 2, 3, 4, 5, 6, 7;",
				"  ...",
				"Row number r contains 2*r-1 numbers: r-1, r-2, ..., 0, 1, 2, ..., r-1. (End)"
			],
			"maple": [
				"A053615 := proc(n)",
				"    A004738(n+1)-1 ; # reuses code of A004738",
				"end proc:",
				"seq(A053615(n),n=0..30) ; # _R. J. Mathar_, Feb 14 2019"
			],
			"mathematica": [
				"a[0] = 0; a[n_] := Floor[Sqrt[n]] - a[n - Floor[Sqrt[n]]]; Table[a[n], {n, 0, 103}] (* _Jean-François Alcover_, Dec 16 2011, after _Benoit Cloitre_ *)",
				"Join[{0},Module[{nn=150,ptci},ptci=Times@@@Partition[Range[nn/2+1],2,1];Table[Abs[n-Nearest[ptci,n]],{n,nn}][[All,1]]]] (* _Harvey P. Dale_, Aug 29 2020 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c1,0,sqrtint(n)-a(n-sqrtint(n)))"
			],
			"xref": [
				"Cf. A002262, A002378, A004738, A049581, A053188, A196199."
			],
			"keyword": "easy,nice,nonn",
			"offset": "0,5",
			"author": "_Henry Bottomley_, Mar 20 2000",
			"references": 11,
			"revision": 51,
			"time": "2020-08-29T14:01:24-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}