{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A057506",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 57506,
			"data": "0,1,3,2,8,6,7,5,4,22,19,20,15,14,21,16,18,13,11,17,12,10,9,64,60,61,52,51,62,53,55,41,39,54,40,38,37,63,56,57,43,42,59,47,50,36,33,48,34,29,28,58,44,49,35,30,46,32,27,25,45,31,26,24,23,196,191,192,178,177",
			"name": "Signature-permutation of a Catalan Automorphism: (inverse of) \"Donaghey's map M\", acting on the parenthesizations encoded by A014486.",
			"comment": [
				"This is inverse of A057505, which is a signature permutation of Catalan automorphism (bijection) known as \"Donaghey's map M\". See A057505 for more comments, links and references."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A057506/b057506.txt\"\u003eTable of n, a(n) for n = 0..23713\u003c/a\u003e",
				"A. Karttunen (et al) at OEIS Wiki, \u003ca href=\"https://oeis.org/wiki/Source_code_for_Catalan_ranking_and_unranking_functions#Implementation_in_Maple\"\u003eMaple implementations of CatalanRankGlobal, CatalanSequences, car, cdr, binexp2pars and pars2binexp functions\u003c/a\u003e",
				"A. Karttunen at OEIS WIki, \u003ca href=\"https://oeis.org/wiki/Source_code_for_Catalan_ranking_and_unranking_functions#Implementation_in_Scheme_for_S-expressions\"\u003eScheme implementations of CatalanRankSexp and CatalanUnrankSexp\u003c/a\u003e",
				"Indranil Ghosh, \u003ca href=\"/A057506/a057506.txt\"\u003ePython program for computing this sequence\u003c/a\u003e (after the functions mentioned in the OEIS wiki)",
				"\u003ca href=\"/index/Per#IntegerPermutationCatAuto\"\u003eIndex entries for signature-permutations of Catalan automorphisms\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A057163(A057164(n)).",
				"a(n) = A057163(A057505(A057163(n))) = A057164(A057505(A057164(n)))."
			],
			"maple": [
				"map(CatalanRankGlobal,map(DonagheysA057506,CatalanSequences(196))); # Where CatalanSequences(n) gives the terms A014486(0..n).",
				"DonagheysA057506 := n -\u003e pars2binexp(deepreverse(DonagheysA057505(deepreverse(binexp2pars(n)))));",
				"DonagheysA057505 := h -\u003e `if`((0 = nops(h)), h, [op(DonagheysA057505(car(h))), DonagheysA057505(cdr(h))]);",
				"# The following corresponds to automorphism A057164:",
				"deepreverse := proc(a) if 0 = nops(a) or list \u003c\u003e whattype(a) then (a) else [op(deepreverse(cdr(a))), deepreverse(a[1])]; fi; end;",
				"# The rest of required Maple-functions: see the given OEIS Wiki page."
			],
			"program": [
				"(Scheme)",
				"(define (A057506 n) (CatalanRankSexp (*A057506 (CatalanUnrankSexp n))))",
				"(define (*A057506 bt) (let loop ((lt bt) (nt (list))) (cond ((not (pair? lt)) nt) (else (loop (cdr lt) (cons nt (*A057506 (car lt))))))))",
				";; Functions CatalanRankSexp and CatalanUnrankSexp can be found at OEIS Wiki page."
			],
			"xref": [
				"Inverse: A057505.",
				"Cf. A057161, A057162, A057163, A057164, A057501, A057502, A057503, A057504 (for similar signature permutations of simple Catalan automorphisms).",
				"Cf. A057507 (cycle counts).",
				"The 2nd, 3rd, 4th, 5th and 6th \"powers\" of this permutation: A071662, A071664, A071666, A071668, A071670.",
				"Row 12 of table A122287.",
				"Cf. also A014486, A080300, A080069, A080070."
			],
			"keyword": "nonn,look",
			"offset": "0,3",
			"author": "_Antti Karttunen_, Sep 03 2000",
			"ext": [
				"Entry revised by _Antti Karttunen_, May 30 2017"
			],
			"references": 41,
			"revision": 39,
			"time": "2017-06-23T08:58:01-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}