{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A171100",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 171100,
			"data": "1,2,3,4,5,9,6,7,13,8,11,19,10,17,27,12,23,35,14,15,29,16,21,37,18,25,43,20,31,51,22,39,61,24,41,65,26,33,59,28,45,73,30,47,77,32,49,81,34,53,87,36,55,91,38,63,101,40,57,97,42,67,109,44,69,113,46,71,117,48,79",
			"name": "Natural numbers arranged into triples a, b, c with no common factor.",
			"comment": [
				"Regard the sequence S as a succession of triple [a,b,c]:",
				"1,2,3,",
				"4,5,9,",
				"6,7,13,",
				"8,11,19,",
				"10,17,27,",
				"12,23,35,",
				"14,15,29,",
				"16,21,37,",
				"18,25,43,",
				"...",
				"Rule 1) a+b=c",
				"Rule 2) \"a\" and \"b\" share no common factor (except 1), \"b\" and \"c\" share no common factor (except 1), \"c\" and \"a\" share no common factor (except 1)",
				"Rule 3) S is a permutation of the natural numbers.",
				"To build S is easy:",
				"- write down N",
				"- start from the left and:",
				"-\u003e put a \"+\" on top of two as yet unmarked integers which will satisfy rules (1) and (2) (always start with the smallest unmarked integer)",
				"-\u003e put a \"=\" on top of the result taking the same rules into account",
				"We have:",
				"N = 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 ...",
				"... + + =",
				"N = 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 ...",
				"giving the first triple [1,2,3]",
				"(used integers will be marked with a circle \"o\" from now on)",
				"....o.o.o.+.+.......=",
				"N = 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 ...",
				"giving the second triple [4,5,9]",
				"....o.o.o.o.o.+.+...o..........=",
				"N = 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 ...",
				"giving the third triple [6,7,13]",
				"....o.o.o.o.o.o.o.+.o....+.....o.................=",
				"N = 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 ...",
				"giving the fourth triple [8,11,19]",
				"....o.o.o.o.o.o.o.o.o.+..o.....o...........+.....o.......................=",
				"N = 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 ...",
				"giving the fifth triple [10,17,27]",
				"....o.o.o.o.o.o.o.o.o.o..o..+..o...........o.....o...........+............o......................=",
				"N = 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 ...",
				"giving the sixth triple [12,23,35]",
				"etc.",
				"Is the sequence infinite?",
				"Contribution from _William Rex Marshall_, Nov 07 2010: (Start)",
				"Sequence is infinite. A new triple is always possible as there are always infinitely many candidate \"b\" values coprime to the smallest unused integer \"a\", and previous triples can only rule out a finite number of them and their sums (which are necessarily pairwise coprime to \"a\" and \"b\").",
				"It appears that the ratios a(n-2):a(n-1):a(n), when n is a multiple of 3, tend to 1:phi:phi^2 as n tends to infinity, where phi is the golden ratio (A001622). Is there a simple proof of this? (End)"
			],
			"link": [
				"W.R. Marshall, \u003ca href=\"/A171100/b171100.txt\"\u003eTable of n, a(n) for n=1..10000\u003c/a\u003e",
				"Eric Angelini, \u003ca href=\"http://www.cetteadressecomportecinquantesignes.com/TripletsPermut.htm\"\u003eNaturals permuted in triple a, b, c \"with no common factor\"\u003c/a\u003e",
				"E. Angelini, \u003ca href=\"/A171100/a171100.pdf\"\u003eNaturals permuted in triple a, b, c \"with no common factor\"\u003c/a\u003e [Cached copy, with permission]"
			],
			"xref": [
				"Cf. A171101 (values of c)."
			],
			"keyword": "nonn,tabf",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Sep 24 2010, based on a posting by Eric Angelini to the Sequence Fans Mailing List, Sep 16 2010",
			"ext": [
				"More terms from _R. J. Mathar_, Sep 25 2010"
			],
			"references": 2,
			"revision": 10,
			"time": "2016-06-09T22:18:30-04:00",
			"created": "2010-10-02T03:00:00-04:00"
		}
	]
}