{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A014190",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 14190,
			"data": "0,1,2,4,8,10,13,16,20,23,26,28,40,52,56,68,80,82,91,100,112,121,130,142,151,160,164,173,182,194,203,212,224,233,242,244,280,316,328,364,400,412,448,484,488,524,560,572,608,644,656,692,728,730,757",
			"name": "Palindromes in base 3 (written in base 10).",
			"comment": [
				"Rajasekaran, Shallit, \u0026 Smith prove that this sequence is an additive basis of order (exactly) 3. - _Charles R Greathouse IV_, May 03 2020"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A014190/b014190.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Patrick De Geest, \u003ca href=\"http://www.worldofnumbers.com/nobase10.htm\"\u003ePalindromic numbers beyond base 10\u003c/a\u003e.",
				"Phakhinkon Phunphayap and Prapanpong Pongsriiam, \u003ca href=\"https://doi.org/10.13140/RG.2.2.23202.79047\"\u003eEstimates for the Reciprocal Sum of b-adic Palindromes\u003c/a\u003e, 2019.",
				"Aayush Rajasekaran, Jeffrey Shallit, Tim Smith, \u003ca href=\"https://arxiv.org/abs/1706.10206\"\u003eSums of palindromes: an approach via automata\u003c/a\u003e, arXiv:1706.10206 [cs.FL], 2017.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PalindromicNumber.html\"\u003ePalindromic Number\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Ternary.html\"\u003eTernary\u003c/a\u003e.",
				"\u003ca href=\"/index/Ab#basis_03\"\u003eIndex entries for sequences that are an additive basis\u003c/a\u003e, order 3."
			],
			"formula": [
				"Sum_{n\u003e=2} 1/a(n) = 2.61676111... (Phunphayap and Pongsriiam, 2019). - _Amiram Eldar_, Oct 17 2020"
			],
			"maple": [
				"isA014190 := proc(n)",
				"    local L;",
				"    L := convert(n,base,3) ;",
				"    ListTools[Reverse](L) = L ;",
				"end proc:",
				"for n from 0 to 500 do",
				"    if isA014190(n) then",
				"        printf(\"%d,\",n) ;",
				"    end if;",
				"end do: # _R. J. Mathar_, Jul 07 2015"
			],
			"mathematica": [
				"f[n_,b_] := Module[{i=IntegerDigits[n,b]}, i==Reverse[i]]; lst={}; Do[If[f[n,3], AppendTo[lst,n]], {n,1000}]; lst (* _Vladimir Joseph Stephan Orlovsky_, Jul 08 2009 *)"
			],
			"program": [
				"(MAGMA) [n: n in [0..800] | Intseq(n, 3) eq Reverse(Intseq(n, 3))]; // _Vincenzo Librandi_, Sep 09 2015",
				"(Sage)",
				"[n for n in (0..757) if Word(n.digits(3)).is_palindrome()] # _Peter Luschny_, Sep 13 2018",
				"(PARI) ispal(n,b=3)=my(d=digits(n,b)); d==Vecrev(d) \\\\ _Charles R Greathouse IV_, May 03 2020"
			],
			"xref": [
				"Cf. A007089, A118594, A134027, A330312 (first differences).",
				"Palindromes in bases 2 through 10: A006995, A014190, A014192, A029952, A029953, A029954, A029803, A029955, A002113."
			],
			"keyword": "nonn,base,easy",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_.",
			"references": 39,
			"revision": 36,
			"time": "2020-10-17T04:22:56-04:00",
			"created": "1998-06-14T03:00:00-04:00"
		}
	]
}