{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A016116",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 16116,
			"data": "1,1,2,2,4,4,8,8,16,16,32,32,64,64,128,128,256,256,512,512,1024,1024,2048,2048,4096,4096,8192,8192,16384,16384,32768,32768,65536,65536,131072,131072,262144,262144,524288,524288,1048576,1048576,2097152",
			"name": "a(n) = 2^floor(n/2).",
			"comment": [
				"Powers of 2 doubled up. The usual OEIS policy is to omit the duplicates in such cases (when this would become A000079). This is an exception.",
				"Number of symmetric compositions of n: e.g., 5 = 2+1+2 = 1+3+1 = 1+1+1+1+1 so a(5) = 4; 6 = 3+3 = 2+2+2 = 1+4+1 = 2+1+1+2 = 1+2+2+1 = 1+1+2+1+1 = 1+1+1+1+1+1 so a(6) = 8. - _Henry Bottomley_, Dec 10 2001",
				"This sequence is the number of digits of each term of A061519. - _Dmitry Kamenetsky_, Jan 17 2009",
				"Starting with offset 1 = binomial transform of [1, 1, -1, 3, -7, 17, -41, ...]; where A001333 = (1, 1, 3, 7, 17, 41, ...). - _Gary W. Adamson_, Mar 25 2009",
				"a(n+1) is the number of symmetric subsets of [n]={1,2,...,n}. A subset S of [n] is symmetric if k is an element of S implies (n-k+1) is an element of S. - _Dennis P. Walsh_, Oct 27 2009",
				"INVERT and inverse INVERT transforms give A006138, A039834(n-1).",
				"The Kn21 sums, see A180662, of triangle A065941 equal the terms of this sequence. - _Johannes W. Meijer_, Aug 15 2011",
				"First differences of A027383. - _Jason Kimberley_, Nov 01 2011",
				"Run lengths in A079944. - _Jeremy Gardiner_, Nov 21 2011",
				"Number of binary palindromes (A006995) between 2^(n-1) and 2^n (for n\u003e1). - _Hieronymus Fischer_, Feb 17 2012",
				"Pisano period lengths: 1, 1, 4, 1, 8, 4, 6, 1, 12, 8, 20, 4, 24, 6, 8, 1, 16, 12, 36, 8, ... . - _R. J. Mathar_, Aug 10 2012",
				"Range of row n of the Circular Pascal array of order 4. - _Shaun V. Ault_, May 30 2014",
				"a(n) is the number of permutations of length n avoiding both 213 and 312 in the classical sense which are breadth-first search reading words of increasing unary-binary trees. For more details, see the entry for permutations avoiding 231 at A245898. - _Manda Riehl_, Aug 05 2014",
				"Also, the decimal representation of the diagonal from the origin to the corner (and from the corner to the origin except for the initial term) of the n-th stage of growth of the two-dimensional cellular automaton defined by \"Rule 190\", based on the 5-celled von Neumann neighborhood when initialized with a single black (ON) cell at stage zero. - _Robert Price_, May 10 2017",
				"a(n + 1) + n - 1, n \u003e 0, is the number of maximal subsemigroups of the monoid of partial order-preserving or -reversing mappings on a set with n elements. See the East et al. link. - _James Mitchell_ and _Wilf A. Wilson_, Jul 21 2017",
				"Number of symmetric stairs with n cells. A stair is a snake polyomino allowing only two directions for adjacent cells: east and north. See A005418. - _Christian Barrientos_, May 11 2018",
				"For n \u003e= 4, a(n) is the exponent of the group of the Gaussian integers in a reduced system modulo (1+i)^(n+2). See A302254. - _Jianing Song_, Jun 27 2018",
				"a(n) is the number of length-(n+1) binary sequences, denoted \u003cs(1),...,s(n+1)\u003e, with s(1)=1 and with s(i+1)=s(i) for odd i. - _Dennis P. Walsh_, Sep 06 2018",
				"a(n+1) is the number of subsets of {1,2,..,n} in which all differences between successive elements of subsets are even.  For example, for n = 7, a(6) = 8 and the 8 subsets are {7}, {1,7}, {3,7}, {5,7}, {1,3,7}, {1,5,7}, {3,5,7}, {1,3,5,7}.  For odd differences between elements see Comment in A000045 (Fibonacci numbers). - _Enrique Navarrete_, Jul 01 2020"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A016116/b016116.txt\"\u003eTable of n, a(n) for n = 0..5000\u003c/a\u003e",
				"Shaun V. Ault and Charles Kicey, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2014.05.020\"\u003eCounting paths in corridors using circular Pascal arrays\u003c/a\u003e, Discrete Mathematics, Volume 332, 6 October 2014, Pages 45-54.",
				"Shaun V. Ault and Charles Kicey, \u003ca href=\"http://arxiv.org/abs/1407.2197\"\u003eCounting paths in corridors using circular Pascal arrays\u003c/a\u003e, arXiv:1407.2197 [math.CO], 2014.",
				"Arvind Ayyer, Amritanshu Prasad, and Steven Spallone, \u003ca href=\"http://arxiv.org/abs/1601.01776\"\u003eOdd partitions in Young's lattice\u003c/a\u003e, arXiv:1601.01776 [math.CO], 2016. See Theorem 6 p. 12.",
				"Paul Barry, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL9/Barry/barry91.html\"\u003eOn Integer-Sequence-Based Constructions of Generalized Pascal Triangles\u003c/a\u003e, J. Integer Sequ., Vol. 9 (2006), Article 06.2.4.",
				"Francesco Battistoni and Giuseppe Molteni, \u003ca href=\"https://arxiv.org/abs/2101.06163\"\u003eAn elementary proof for a generalization of a Pohst's inequality\u003c/a\u003e, arXiv:2101.06163 [math.NT], 2021.",
				"Johann Cigler, \u003ca href=\"http://arxiv.org/abs/1501.04750\"\u003eSome remarks and conjectures related to lattice paths in strips along the x-axis\u003c/a\u003e, arXiv:1501.04750 [math.CO], 2015.",
				"Emeric Deutsch, \u003ca href=\"http://www.jstor.org/stable/2691040\"\u003eProblem 1633\u003c/a\u003e, Math. Mag., 74 #5 (2001), p. 403.",
				"James East, Jitender Kumar, James D. Mitchell, and Wilf A. Wilson, \u003ca href=\"https://arxiv.org/abs/1706.04967\"\u003eMaximal subsemigroups of finite transformation and partition monoids\u003c/a\u003e, arXiv:1706.04967 [math.GR], 2017.",
				"A. Goupil, H. Cloutier, and F. Nouboud, \u003ca href=\"https://doi.org/10.1016/j.dam.2010.08.011\"\u003eEnumeration of polyominoes inscribed in a rectangle\u003c/a\u003e Discrete Applied Mathematics 158(2010), 2014-2023.",
				"S. Heubach and T. Mansour, \u003ca href=\"https://arxiv.org/abs/math/0310197\"\u003eCounting rises, levels and drops in compositions\u003c/a\u003e, arXiv:math/0310197 [math.CO], 2003.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=1067\"\u003eEncyclopedia of Combinatorial Structures 1067\u003c/a\u003e",
				"D. Levin, L. Pudwell, M. Riehl, and A. Sandberg, \u003ca href=\"http://www.etsu.edu/cas/math/pp2014/documents/talks/riehl.pdf\"\u003ePattern Avoidance on k-ary Heaps\u003c/a\u003e, Slides of Talk, 2014.",
				"D. Merlini, F. Uncini and M. C. Verri, \u003ca href=\"http://www.emis.de/journals/INTEGERS/papers/e23/e23.Abstract.html\"\u003eA unified approach to the study of general and palindromic compositions\u003c/a\u003e, Integers 4 (2004), A23, 26 pp.",
				"Agustín Moreno Cañadas, Hernán Giraldo, and Robinson Julian Serna Vanegas, \u003ca href=\"http://dx.doi.org/10.17654/MS101122745\"\u003eSome integer partitions induced by orbits of Dynkin type\u003c/a\u003e, Far East Journal of Mathematical Sciences (FJMS), Vol. 101, No. 12 (2017), pp. 2745-2766.",
				"Laurent Noé, \u003ca href=\"http://www.lifl.fr/~noe/files/expose_JOV13.pdf\"\u003eSpaced seed design on profile HMMs for precise HTS read-mapping efficient sliding window product on the matrix semi-group\u003c/a\u003e, in Rapide Bilan 2012-2013, Laurent, LIFL, Université Lille 1 - INRIA Journées au vert 11 et 12 juin 2013, Laurent, Année 2012-2013.",
				"Valentin Ovsienko, \u003ca href=\"http://images.math.cnrs.fr/Villes-paires-et-impaires-Oddtown-2470.html\"\u003eVilles paires et impaires (Oddtown and Eventown) I\u003c/a\u003e, Images des Mathématiques, CNRS, 2013 (in French).",
				"Dennis Walsh, \u003ca href=\"http://capone.mtsu.edu/dwalsh/SYMMSUB.pdf\"\u003eNotes on symmetric subsets of {1, 2, ..., n}\u003c/a\u003e [From _Dennis P. Walsh_, Oct 27 2009]",
				"A. Yajima, \u003ca href=\"https://doi.org/10.1246/bcsj.20140204\"\u003eHow to calculate the number of stereoisomers of inositol-homologs\u003c/a\u003e, Bull. Chem. Soc. Jpn. 2014, 87, 1260-1264 | doi:10.1246/bcsj.20140204. See Tables 1 and 2 (and text). - _N. J. A. Sloane_, Mar 26 2015",
				"\u003ca href=\"/index/Di#divseq\"\u003eIndex to divisibility sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,2)."
			],
			"formula": [
				"a(n) = a(n-1)*a(n-2)/a(n-3) = 2*a(n-2) = 2^A004526(n).",
				"G.f.: (1+x)/(1-2*x^2).",
				"a(n) = (1/2 + sqrt(1/8))*sqrt(2)^n + (1/2 - sqrt(1/8))*(-sqrt(2))^n. - _Ralf Stephan_, Mar 11 2003",
				"E.g.f.: cosh(sqrt(2)*x) + sinh(sqrt(2)*x)/sqrt(2). - _Paul Barry_, Jul 16 2003",
				"The signed sequence (-1)^n*2^floor(n/2) has a(n) = (sqrt(2))^n(1/2 - sqrt(2)/4) + (-sqrt(2))^n(1/2 + sqrt(2)/4). It is the inverse binomial transform of A000129(n-1). - _Paul Barry_, Apr 21 2004",
				"Diagonal sums of A046854. a(n) = Sum_{k=0..n} binomial(floor(n/2), k). - _Paul Barry_, Jul 07 2004",
				"a(n) = a(n-2) + 2^floor((n-2)/2). - _Paul Barry_, Jul 14 2004",
				"a(n) = Sum_{k=0..floor(n/2)} binomial(floor(n/2), floor(k/2)). - _Paul Barry_, Jul 15 2004",
				"E.g.f.: cosh(asinh(1) + sqrt(2)*x)/sqrt(2). - _Michael Somos_, Feb 28 2005",
				"a(n) = Sum_{k=0..n} A103633(n,k). - _Philippe Deléham_, Dec 03 2006",
				"a(n) = 2^(n/2)*((1 + (-1)^n)/2 + (1-(-1)^n)/(2*sqrt(2))). - _Paul Barry_, Nov 12 2009",
				"a(n) = 2^((2*n - 1 + (-1)^n)/4). - _Luce ETIENNE_, Sep 20 2014"
			],
			"example": [
				"For n=5 the a(5)=4 symmetric subsets of [4] are {1,4}, {2,3}, {1,2,3,4} and the empty set. - _Dennis P. Walsh_, Oct 27 2009",
				"For n=5 the a(5)=4 length-6 binary sequences are \u003c1,1,0,0,0,0\u003e, \u003c1,1,0,0,1,1\u003e, \u003c1,1,1,1,0,0\u003e and \u003c1,1,1,1,1,1\u003e. - _Dennis P. Walsh_, Sep 06 2018"
			],
			"maple": [
				"A016116:= proc(n): 2^floor(n/2) end: seq(A016116(n), n=0..42); # _Dennis P. Walsh_, Oct 27 2009"
			],
			"mathematica": [
				"Table[ 2^Floor[n/2], {n, 0, 42}] (* _Robert G. Wilson v_, Jun 05 2004 *)",
				"With[{c=2^Range[0,30]},Riffle[c,c]] (* _Harvey P. Dale_, Jan 23 2015 *)",
				"CoefficientList[Series[(1+x)/(1-2*x^2), {x, 0, 50}], x] (* _Stefano Spezia_, Sep 07 2018 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c0,0,2^(n\\2))",
				"(MAGMA) [2^Floor(n/2): n in [0..50]]; // _Vincenzo Librandi_, Aug 16 2011",
				"(Maxima) makelist(2^floor(n/2), n, 0, 50); /* _Martin Ettl_, Oct 17 2012 */",
				"(Sage)",
				"def A016116():",
				"    x, y = -1, 0",
				"    while True:",
				"        yield -x",
				"        x, y = x + y, x - y",
				"a = A016116(); [next(a) for i in range(40)]  # _Peter Luschny_, Jul 11 2013",
				"(GAP) List([0..45],n-\u003e2^Int(n/2)); # _Muniru A Asiru_, Apr 03 2018"
			],
			"xref": [
				"Cf. A006995, A057148, A079944, A112030, A112033.",
				"a(n) = A094718(3, n).",
				"Cf. A001333.",
				"See A052955 for partial sums (without the initial term).",
				"A000079 gives the odd-indexed terms of a(n)."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_, Dec 11 1999",
			"references": 168,
			"revision": 234,
			"time": "2021-02-14T12:35:57-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}