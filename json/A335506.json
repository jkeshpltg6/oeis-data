{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A335506",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 335506,
			"data": "1,5,25,125,625,3125,15625,8125,40625,203125,1015625,508125,2540625,1203125,6015625,3008125,15040625,5203125,26015625,13008125,65040625,325203125,1626015625,813008125,4065040625,20325203125,101626015625,50813008125,254065040625",
			"name": "Start with a(0) = 1; thereafter a(n) is obtained from 5*a(n-1) by removing all 7's.",
			"comment": [
				"This sequence is a rare non-periodic case of the recurrence where x(0)=1 and x(n+1) is obtained from m*x(n) by removing all digits k and all trailing zeros in base b. In fact, except for (m, b, k) = (5, 10, 7) (this sequence), x is eventually periodic whenever m \u003c= 5 and 2 \u003c= b \u003c= 32, or m \u003c= 16 and 2 \u003c= b \u003c= 16. However, for negative b it seems that x is non-periodic more frequently, for example when (m, b, k) is (2, -5, 1) or (2, -8, 1)."
			],
			"link": [
				"\u003ca href=\"/index/Rec#order_20\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,100000,0,0,0,-100000)."
			],
			"formula": [
				"a(n) = a(n-4) + 100000*(a(n-16) - a(n-20)) for n \u003e 22.",
				"Recurrence (for n \u003e 2):",
				"  a(n+1) = 5*a(n) if n is 1, 3, 4, 5, 7, 8, 9, 11, 13, or 15 mod 16 (these are the cases where 5*a(n) doesn't contain any 7's);",
				"  a(n+1) = (a(n) + 625)/2 if n is 2, 6, 10, or 14 mod 16;",
				"  a(n+1) = 5*a(n) - 7*10^(5*n/16 + 2) if n is 0 mod 16;",
				"  a(n+1) = 5*a(n) - 115*10^(5*(n + 4)/16) if n is 12 mod 16.",
				"Explicit expressions:",
				"  a(16*n) = (37*2^4*10^(5*n - 2) - 1)*5^5/123 for n \u003e= 1;",
				"  a(16*n + 1) = (2^12*10^(5*n - 4) - 1)*5^6/123 for n \u003e= 1;",
				"  a(16*n + 2) = (2^12*10^(5*n - 4) - 1)*5^7/123 for n \u003e= 1;",
				"  a(16*n + 3) = (2^8*10^(5*n - 1) - 1)*5^4/123 for n \u003e= 0;",
				"  a(16*n + 4) = (2^8*10^(5*n - 1) - 1)*5^5/123 for n \u003e= 0;",
				"  a(16*n + 5) = (2^8*10^(5*n - 1) - 1)*5^6/123 for n \u003e= 0;",
				"  a(16*n + 6) = (2^8*10^(5*n - 1) - 1)*5^7/123 for n \u003e= 0;",
				"  a(16*n + 7) = (2^4*10^(5*n + 2) - 1)*5^4/123 for n \u003e= 0;",
				"  a(16*n + 8) = (2^4*10^(5*n + 2) - 1)*5^5/123 for n \u003e= 0;",
				"  a(16*n + 9) = (2^4*10^(5*n + 2) - 1)*5^6/123 for n \u003e= 0;",
				"  a(16*n + 10) = (2^4*10^(5*n + 2) - 1)*5^7/123 for n \u003e= 0;",
				"  a(16*n + 11) = (10^(5*n + 5) - 1)*5^4/123 for n \u003e= 0;",
				"  a(16*n + 12) = (10^(5*n + 5) - 1)*5^5/123 for n \u003e= 0;",
				"  a(16*n + 13) = (37*2^8*10^(5*n) - 1)*5^6/123 for n \u003e= 0;",
				"  a(16*n + 14) = (37*2^8*10^(5*n) - 1)*5^7/123 for n \u003e= 0;",
				"  a(16*n + 15) = (37*2^4*10^(5*n + 3) - 1)*5^4/123 for n \u003e= 0."
			],
			"example": [
				"For n \u003c= 6, no 7's occur in 5^n, so a(n) = 5^n. Deleting the 7 in 5*a(6) = 78125, we obtain a(7) = 8125."
			],
			"mathematica": [
				"Remove7[n_] := FromDigits[Select[IntegerDigits[n], # != 7 \u0026]]; a[0] = 1; a[n_] := a[n] = Remove7[5 * a[n - 1]]; Array[a, 29, 0] (* _Amiram Eldar_, Jun 20 2020 *)"
			],
			"program": [
				"(Python)",
				"from sympy.ntheory.factor_ import digits",
				"from functools import reduce",
				"def drop(x,n,k):",
				"  # Drop all digits k from x in base n.",
				"  return reduce(lambda x,j:n*x+j if j!=k else x,digits(x, n)[1:],0)",
				"def A335506(n):",
				"  if n==0:",
				"    return 1",
				"  else:",
				"    return drop(5*A335506(n-1),10,7)"
			],
			"xref": [
				"Cf. A335505."
			],
			"keyword": "nonn,base",
			"offset": "0,2",
			"author": "_Pontus von Brömssen_, Jun 13 2020",
			"references": 5,
			"revision": 20,
			"time": "2020-07-13T04:52:44-04:00",
			"created": "2020-07-11T02:51:40-04:00"
		}
	]
}