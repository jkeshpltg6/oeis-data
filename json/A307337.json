{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A307337",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 307337,
			"data": "6,5,4,3,2,1,0,6,6,6,6,6,6,6,5,4,3,2,1,0,6,6,5,4,3,2,1,6,6,0,6,6,6,5,6,6,4,6,5,4,3,3,2,2,1,0,6,1,6,5,6,0,2,6,6,4,6,3,6,5,1,6,6,6,6,0,6,6,6,6,5,6,4,4,6,5,4,3,3,6,5,2,2,4,0,6,1,3,1,3,6,5,6,0,6,2,2,2,6,6,3,4,1,0,1,6,5,6,1,6,6",
			"name": "A fractal septenary sequence: For all n \u003e= 1, underline the term with index n + a(n) + 1; then the two subsequences of underlined terms and of non-underlined terms are both equal to the sequence itself.",
			"comment": [
				"This is defined to be the lexicographically earliest septenary sequence with the following property:",
				"If a(n) = 0, underline a(n+1); if a(n) = 1, underline a(n+2); if a(n) = 2, underline a(n+3); if a(n) = 3, underline a(n+4); if a(n) = 4, underline a(n+5); if a(n) = 5, underline a(n+6); if a(n) = 6, underline a(n+7). Now, the subsequence of (once or more) underlined terms must be equal to the original sequence (copy #1), and the subsequence of non-underlined terms must also reproduce the original sequence (copy #2)."
			],
			"link": [
				"Carole Dubois, \u003ca href=\"/A307337/b307337.txt\"\u003eTable of n, a(n) for n = 1..2005\u003c/a\u003e"
			],
			"example": [
				"The sequence starts (6,5,4,3,2,1,0,6,6,6,6,6,6,6,5,4,3,2,1,0,6,6,...)",
				"Instead of underlining terms, we will put parentheses around the terms we want to emphasize:",
				"a(1) = 6 produces parentheses around a(1 + 7 = 8):",
				"6,5,4,3,2,1,0,(6),6,6,6,6,6,6,5,4,3,2,1,0,...",
				"a(2) = 5 produces parentheses around a(2 + 6 = 8), which is now already done. Then,",
				"a(3) = 4 produces parentheses around a(3 + 5 = 8), which is already done. Then,",
				"a(4) = 3 produces parentheses around a(4 + 4 = 8) - already done. Then,",
				"a(5) = 2 produces parentheses around a(5 + 3 = 8) - already done. Then,",
				"a(6) = 1 produces parentheses around a(6 + 2 = 8) - already done. Then,",
				"a(7) = 0 produces parentheses around a(7 + 1 = 8) - already done. Then,",
				"a(8) = 6 produces parentheses around a(8 + 7 = 15):",
				"6,5,4,3,2,1,0,(6),6,6,6,6,6,6,(5),4,3,2,1,0,...",
				"a(9) = 6 produces parentheses around a(9 + 7 = 16):",
				"6,5,4,3,2,1,0,(6),6,6,6,6,6,6,(5),(4),3,2,1,0,...",
				"a(10) = 6 produces parentheses around a(10 + 7 = 17):",
				"6,5,4,3,2,1,0,(6),6,6,6,6,6,6,(5),(4),(3),2,1,0,...",
				"a(11) = 6 produces parentheses around a(11 + 7 = 18):",
				"6,5,4,3,2,1,0,(6),6,6,6,6,6,6,(5),(4),(3),(2),1,0,...",
				"a(12) = 6 produces parentheses around a(12 + 7 = 19):",
				"6,5,4,3,2,1,0,(6),6,6,6,6,6,6,(5),(4),(3),(2),(1),0,...",
				"a(13) = 6 produces parentheses around a(13 + 7 = 20):",
				"6,5,4,3,2,1,0,(6),6,6,6,6,6,6,(5),(4),(3),(2),(1),(0),...",
				"a(14) = 6 produces parentheses around a(14 + 7 = 21):",
				"6,5,4,3,2,1,0,(6),6,6,6,6,6,6,(5),(4),(3),(2),(1),(0),(6)...",
				"a(15) = 5 produces parentheses around a(15 + 6 = 21) - already done. Then,",
				"a(16) = 4 produces parentheses around a(16 + 5 = 21) - already done. Then,",
				"a(17) = 3 produces parentheses around a(17 + 4 = 21) - already done.",
				"Etc.",
				"We see in this small example that the parenthesized terms reproduce the initial sequence:",
				"(6),(5),(4),(3),(2),(1),(0),(6)...",
				"The same is true for the subsequence of non-parenthesized terms:",
				"6, 5, 4, 3, 2, 1, 0, 6, 6, 6, 6, 6, 6,..."
			],
			"xref": [
				"Cf. A307183 (first binary example of such fractal sequences), A307332 (ternary), A307333 (quaternary), A307335 (quinary), A307336 (senary), A307338 (octal), A307339 (nonary), A307340 (decimal)."
			],
			"keyword": "nonn,base",
			"offset": "1,1",
			"author": "_Eric Angelini_ and _Carole Dubois_, Apr 02 2019",
			"references": 9,
			"revision": 12,
			"time": "2019-04-04T22:49:57-04:00",
			"created": "2019-04-04T22:49:57-04:00"
		}
	]
}