{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A328277",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 328277,
			"data": "304,153,157,197,124,97,221,156,69,171,73,88,142,68,69,129,73,81,86,62,46,189,88,40,67,48,51,24,89,80,77,31,63,68,41,20,0,132,80,90,58,32,63,99,37,0,106,69,79,50,30,45,30,38,0,76,0,96,31,62,54,27,31,49,41,27,84,72,0,31,58,47,26,23,34,43,25,20",
			"name": "Triangle T(m,n) = # { k | concat(mk,nk) has no digit twice or more }, m \u003e n \u003e 0.",
			"comment": [
				"Row m has columns numbered n = 1 .. m-1, with m \u003e= 2.",
				"For an extension to m \u003e= n \u003e= 0, see A328288, and A328287 for column 0.",
				"One consider T(m,n) defined for all m, n \u003e= 0, which would yield a symmetric, infinite square array T(m,n), see formula.",
				"The table is finite in the sense that T(m,n) = 0 for m \u003e 987654321 (even if the multiple isn't pandigital, (mk, nk) cannot have more than 9+1 distinct digits), but also whenever the total number of digits of m and n exceeds 10."
			],
			"link": [
				"M. F. Hasler, in reply to E. Angelini, \u003ca href=\"http://list.seqfan.eu/pipermail/seqfan/2019-October\"\u003eFractions with no repeated digits\u003c/a\u003e, SeqFan list, Oct. 10, 2020."
			],
			"formula": [
				"T(m,n) = 0 whenever m = n (mod 10).",
				"T(m,n) = T(n,m) for all m, n \u003e= 0, if the condition m \u003e n is dropped."
			],
			"example": [
				"The table reads:",
				"  304,     (m=2)",
				"  153, 157,",
				"  197, 124,  97,",
				"  221, 156,  69, 171,",
				"   73,  88, 142,  68, 69,",
				"  129,  73,  81,  86, 62, 46,",
				"  189,  88,  40,  67, 48, 51, 24,",
				"   89,  80,  77,  31, 63, 68, 41, 20,",
				"    0, 132,  80,  90, 58, 32, 63, 99, 37,",
				"    0, 106,  69,  79, 50, 30, 45, 30, 38,  0,    (m = 11)",
				"   76,   0,  96,  31, 62, 54, 27, 31, 49, 41, 27,",
				"   84,  72,   0,  31, 58, 47, 26, 23, 34, 43, 25, 20,",
				"  100,  64,  52,   0, 51, 44, 51, 42, 22, 38, 27, 18, 20",
				"  ...",
				"The terms corresponding to T(2,1) = 304 and T(3,1) = 153 are given in Eric Angelini's post to the SeqFan list.",
				"T(8,7) = 24 = #{1, 5, 7, 9, 12, 51, 71, 76, 105, 107, 122, 128, 132, 134, 262, 627, 674, 853, 1172, 1188, 1282, 1321, 2622, 5244}: For these numbers k, 8k and 7k don't share any digit and have no digit twice; e.g., 5244*(8,7) = (41952, 36708)."
			],
			"program": [
				"(PARI) A328277(m,n)={my(S,s); for(L=1,10,S\u003c(S+=sum( k=10^(L-1),10^L-1, #Set(Vecsmall(s=Str(m*k, n*k)))==#s))||L\u003c3||return(S))} \\\\ Using concat(digits...) would take about 50% more time."
			],
			"xref": [
				"Cf. A328288 (variant m \u003e= n \u003e= 0), A328287 (column 0)."
			],
			"keyword": "nonn,base,tabl,fini",
			"offset": "2,1",
			"author": "_M. F. Hasler_, Oct 10 2019",
			"references": 4,
			"revision": 20,
			"time": "2019-10-14T08:41:24-04:00",
			"created": "2019-10-10T20:30:04-04:00"
		}
	]
}