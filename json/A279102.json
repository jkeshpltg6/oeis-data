{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A279102",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 279102,
			"data": "9,15,25,35,45,49,50,70,77,91,98,110,121,130,135,143,154,169,170,182,187,190,209,221,225,238,242,247,266,286,289,299,315,322,323,338,350,361,374,391,405,418,437,442,484,493,494,506,527,529,550,551,572,578,589,598,638,646,650,667,675,676,682",
			"name": "Numbers n having three parts in the symmetric representation of sigma(n).",
			"comment": [
				"Let n = 2^m * q with m \u003e= 0 and q odd, let row(n) = floor(sqrt(8*n+1) - 1)/2), and let 1 = d_1 \u003c ... \u003c d_h \u003c= row(n) \u003c d_(h+1) \u003c ... \u003c d_k = q be the k odd divisors of n.",
				"The symmetric representation of sigma(n) consists of 3 parts precisely when there is a unique i, 1 \u003c= i \u003c h, such that 2^(m+1) * d_i \u003c d_(i+1) and d_h \u003c= row(n) \u003c 2^(m+1) * d_h.",
				"This property of the odd divisors of n is equivalent to the n-th row of the irregular triangle of A249223 consisting of a block of positive numbers, followed by a block of zeros, followed in turn by a block of positive numbers, i.e., determining the first part and the left half of the center part of the symmetric representation of sigma(n), resulting in 3 parts.",
				"Let n be the product of two primes p and q satisfying 2 \u003c p \u003c q \u003c 2*p. Then n satisfies the property above so that the odd numbers in A087718 form a subsequence."
			],
			"example": [
				"a(4) = 35 = 5*7 is in the sequence since 1 \u003c 2 \u003c 5 \u003c row(35) = 7 \u003c 10;",
				"a(8) = 70 = 2*5*7 is in the sequence since 1 \u003c 4 \u003c 5 \u003c row(70) = 11 \u003c 20;",
				"140 = 4*5*7 is not in the sequence since 1 \u003c 5 \u003c 7 \u003c 8 \u003c row(140) = 16 \u003c 20;",
				"a(506) = 5950 = 2*25*7*17 is in the sequence since 1*4 \u003c 5 is the only pair of odd divisors 1 \u003c 5 \u003c 7 \u003c 17 \u003c 25 \u003c 35 \u003c 85 \u003c row(5950) = 108 satisfying the property (see A251820)."
			],
			"mathematica": [
				"(* support functions are defined in A237048 and A262045 *)",
				"segmentsSigma[n_] := Length[Select[SplitBy[a262045[n], #!=0\u0026], First[#]!=0\u0026]]",
				"a279102[m_, n_] := Select[Range[m, n], segmentsSigma[#]==3\u0026]",
				"a279102[1, 700] (* sequence data *)",
				"(* An equivalent, but slower computation is based on A237271 *)",
				"a279102[m_, n_] := Select[Range[m, n], a237271[#]==3\u0026]",
				"a279102[1,700] (* sequence data *)"
			],
			"xref": [
				"Column 3 of A240062.",
				"Cf. A087718, A174973 (column 1), A237048, A237270, A237271, A237593, A239929 (column 2), A249223, A251820, A262045, A279102."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Hartmut F. W. Hoft_, Dec 06 2016",
			"references": 7,
			"revision": 13,
			"time": "2016-12-06T22:16:00-05:00",
			"created": "2016-12-06T22:16:00-05:00"
		}
	]
}