{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A067276",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 67276,
			"data": "2,-1,-78,880,-4656,-14304,-423936,8342720,711956736,-615707136,21057138688,-4663930678272,211912980656128,-9178450735677440,40005919124799488,83013253447139328,-8525111273818357760,-800258888289188708352,-15170733077495639179264",
			"name": "Determinant of n X n matrix containing the first n^2 primes in increasing order.",
			"comment": [
				"The first column contains the first n primes in increasing order, the second column contains the next n primes in increasing order, etc. Equivalently, first row contains first n primes in increasing order, second row contains next n primes in increasing order, etc. Sequences of determinants of matrices specifically containing primes include A024356 (Hankel matrix), A067549 (first n primes on diagonal, other elements 1), A066933 (cyclic permutations of first n primes in each row) and A067551 (first n primes on diagonal, other elements 0)."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A067276/b067276.txt\"\u003eTable of n, a(n) for n = 1..459\u003c/a\u003e"
			],
			"example": [
				"a(3) = -78 because det[[2,7,17],[3,11,19],[5,13,23]] = -78 (= det[[2,3,5],[7,11,13],[17,19,23]], the determinant of the transpose.)."
			],
			"maple": [
				"seq(LinearAlgebra:-Determinant(Matrix(n,n,(i,j) -\u003e ithprime(n*(i-1)+j))),n=1..20); # _Robert Israel_, Jul 12 2017"
			],
			"mathematica": [
				"Table[ Det[ Partition[ Array[Prime, n^2], n]], {n, 19}] (* _Robert G. Wilson v_, May 26 2006 *)"
			],
			"program": [
				"(PARI) for(n=1,20,k=0; m=matrix(n,n,x,y, prime(k=k+1)); print1(matdet(m), \", \")) /* The matrix initialization command above fills columns first: Variables (such as) x and y take on values 1 through n for rows and columns, respectively, with x changing more rapidly and they must be specified even though the 5th argument is not an explicit function of them here. */",
				"(MAGMA) [ Determinant( Matrix(n, n, [ NthPrime(k): k in [1..n^2] ]) ): n in [1..19] ]; // _Klaus Brockhaus_, May 12 2010",
				"(Python)",
				"from sympy.matrices import Matrix",
				"from sympy import sieve",
				"def a(n):",
				"    sieve.extend_to_no(n**2)",
				"    return Matrix(n, n, sieve[1:n**2+1]).det()",
				"print([a(n) for n in range(1, 20)]) # _Indranil Ghosh_, Jul 31 2017"
			],
			"xref": [
				"Cf. A024356, A067549, A066933, A067551, A114533.",
				"Cf. A119894, A118770, A118772, A118779."
			],
			"keyword": "easy,sign",
			"offset": "1,1",
			"author": "_Rick L. Shepherd_, Feb 21 2002",
			"references": 15,
			"revision": 33,
			"time": "2021-08-13T04:22:00-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}