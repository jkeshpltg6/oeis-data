{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A341894",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 341894,
			"data": "0,1,1,0,2,2,2,2,0,3,2,2,4,2,2,0,2,2,3,2,4,4,2,2,0,3,2,4,4,2,4,2,2,2,2,0,2,2,2,4,4,2,4,2,4,6,2,2,0,3,3,4,4,2,4,2,4,4,2,2,8,2,2,0,2,4,4,2,4,4,4,2,6,2,2,6,4,4,2,2,0,3,2,2,8,4,2,4,4,2,6,4,4,4,2,4,4,2,2,0,2,2,4,2,4,8,2,2,8,2",
			"name": "For square n \u003e 0, a(n) = 0; for nonsquare n \u003e 0, a(n) is the rank r such that t(r) + t(r-1) = u(r) - u(r-1) - 1, where u(r) and t(r) are indices of some triangular numbers in the Diophantine relation T(u(r)) = n*T(t(r)).",
			"comment": [
				"Let t(i) and u(j) be the indices of triangular numbers that satisfy the Diophantine relation T(u(j)) = n*T(t(i)) for some integers i and j. The number of solutions (t(i), u(j)) of T(u(j)) = n*T(t(i)) is 0 or 1 for square n, and an infinity for nonsquare n.",
				"For square n, a(n) is arbitrarily set to 0.",
				"For nonsquare n, a(n) is the index r in the sequence of t(i) and u(j) such that t(r) + t(r-1) = u(r) - u(r-1) - 1.",
				"Alternatively, for nonsquare n, a(n) is the index r such that the ratio t(i)/t(i-r) is decreasing monotonically without jumps for increasing values of i.",
				"Alternatively, for n \u003e 4, a(n) is the index r such that the ratio t(r)/t(r-1) varies between (s+1)/(s-1) and (s+2)/s, with s = [sqrt(n)], where [x] = floor(x).",
				"Alternatively, for nonsquare n, a(n) is the number of fundamental solutions (X_f, Y_f) of the generalized Pell equation X^2 - n*Y^2 = 1 - n providing odd solutions, i.e., with X_f odd and Y_f odd (or Y_f even if y_f is odd, where y_f is the fundamental solution of the associated simple Pell equation x^2 - n*y^2 = 1)."
			],
			"reference": [
				"T. Breiteig, \"Quotients of triangular numbers\", The Mathematical Gazette, 99, 2015, 243-255.",
				"J. S. Chahal and H. D'Souza, \"Some remarks on triangular numbers\", in A.D. Pollington and W. Mean, eds., Number Theory with an Emphasis on the Markov Spectrum, Lecture Notes in Pure Math, Dekker, New York, 1993, 61-67.",
				"K. R. Matthews, \"The Diophantine Equation x^2 - Dy^2 = N, D \u003e 0, in integers\", Expositiones Mathematicae, 18, 2000, 323-331."
			],
			"link": [
				"Vladimir Pletser, \u003ca href=\"/A341894/b341894.txt\"\u003eTable of n, a(n) for n = 1..256\u003c/a\u003e",
				"Keith Matthews, \u003ca href=\"http://www.numbertheory.org/php/main_pell.html\"\u003eQuadratic Diophantine equations BCMATH programs\u003c/a\u003e, 2020.",
				"Vladimir Pletser, \u003ca href=\"https://www.researchgate.net/publication/349788977\"\u003eSearching for Multiple of Triangular Numbers being Triangular Numbers\u003c/a\u003e, ResearchGate, DOI: 10.13140/RG.2.2.35428.91527, 2021.",
				"Vladimir Pletser, \u003ca href=\"http://arxiv.org/abs/2102.13494\"\u003eTriangular Numbers Multiple of Triangular Numbers and Solutions of Pell Equations\u003c/a\u003e, arXiv: 2102.13494 [math.NT], 2021.",
				"Vladimir Pletser, \u003ca href=\"http://arxiv.org/abs/2101.00998\"\u003eRecurrent Relations for Multiple of Triangular Numbers being Triangular Numbers\u003c/a\u003e, arXiv: 2101.00998 [math.NT], 2021."
			],
			"example": [
				"The following table gives the first values of nonsquare n and a(n) and the sequences yielding the values of t, u, T(t) and T(u) such that T(u) = n*T(t).",
				"n       2       3       5       6       7       8      10",
				"a(n)    1       1       2       2       2       2       3",
				"t    A053141 A061278 A077259 A077288 A077398 A336623  A341893*",
				"u    A001652 A001571 A077262 A077291 A077401 A336625* A341895*",
				"T(t) A075528 A076139 A077260 A077289 A077399 A336624  A068085*",
				"T(u) A029549 A076140 A077261 A077290 A077400 A336626*   -",
				"With a(n) = r, the definition t(r) + t(r-1) = u(r) - u(r-1) - 1 yields:",
				"- For n = 2, a(n) = 1: A053141(1) + A053141(0) = A001652(1) - A001652(0) - 1, i.e., 2 + 0 = 3 + 0 - 1 = 2.",
				"- For n = 5, a(n) = 2: A077259(2) + A077259(1) = A077262(2) - A077262(1) - 1, i.e., 6 + 2 = 14 - 5 - 1 = 8.",
				"- For n = 10, a(n) = 3: A341893(3+1*) + A341893(2+1*) = A341895(3+1*) - A341895(2+1*) - 1, i.e., 12 + 6 = 39 - 20 - 1 = 18.",
				"Note that for those sequences marked with an *, the first term 0 appears for n = 1, contrary to all the other sequences, where the first term 0 appears for n = 0; the numbering must therefore be adapted and 1 must be added to compensate for this shift in indices.",
				"The monotonic decrease of t(i)/t(i-r) can be seen also as:",
				"- For n = 2, a(n) = 1: for 1 \u003c= i \u003c= 6, A053141(i)/A053141(i-1) decreases monotonically from 7 to 5.829.",
				"- For n = 5, a(n) = 2: for 3 \u003c= i \u003c= 8, A077259(i)/A077259(i-2) decreases monotonically from 22 to 17.948, while A077259(i)/A077259(i-1) takes values alternatively varying between 3 and 2.618 and between 7.333 and 6.855.",
				"- For n = 10, a(n) = 3: for 4 \u003c= i \u003c= 10, A341893(i)/A341893(i-3) decreases monotonically from 55 to 38, while A077259(i) / A077259(i-1) takes values alternatively varying between 6 and 4.44 and between 2 and 1.925.",
				"For n \u003e 4, the relation (s+1)/(s-1) \u003c=  t(r)/t(r-1) \u003c= (s+2)/s, with s = [sqrt(n)], yields:",
				"- For n = 5, a(n) = 2: A077259(2)/A077259(1) = 6/2 = 3, and s = [sqrt(5)] = 2, (s+1)/(s-1) = 3 and (s+2)/s = 2.",
				"- For n = 10, a(n) = 3: A077259(3+1*)/A077259(2+1*) = 12/6 = 2, and s = [sqrt(10)] = 3, (s+1)/(s-1) = 2 and (s+2)/s = 5/3 = 1.667.",
				"Finally, the number of fundamental solutions of the generalized Pell equation is as follows.",
				"- For n = 2, X^2 - 2*Y^2 = -1 has a single fundamental solution, (X_f, Y_f) = (1, 1), and the rank a(n) is 1.",
				"- For n = 5, X^2 - 5*Y^2 = -4 has two fundamental solutions, (X_f, Y_f) = (1, 1) and (-1, 1), and the rank a(n) is 2.",
				"- For n = 10, X^2 - 10*Y^2 = -9 has three fundamental solutions, (X_f, Y_f) = (1, 1), (-1, 1), and (9, 3), and the rank a(n) is 3."
			],
			"xref": [
				"Cf. A068085, A053141, A061278, A077259, A077288, A077398, A336623, A341893, A001652, A001571, A077262, A077291, A077401, A336625, A341895, A075528, A076139, A077260, A077289, A077399, A336624, A068085, A029549, A076140, A077261, A077290, A077400, A336626, A000217, A341895, A341896."
			],
			"keyword": "nonn",
			"offset": "1,5",
			"author": "_Vladimir Pletser_, Mar 06 2021",
			"references": 1,
			"revision": 21,
			"time": "2021-03-27T15:18:18-04:00",
			"created": "2021-03-07T18:47:54-05:00"
		}
	]
}