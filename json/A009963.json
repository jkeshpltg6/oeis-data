{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A009963",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 9963,
			"data": "1,1,1,1,2,1,1,6,6,1,1,24,72,24,1,1,120,1440,1440,120,1,1,720,43200,172800,43200,720,1,1,5040,1814400,36288000,36288000,1814400,5040,1,1,40320,101606400,12192768000,60963840000,12192768000,101606400,40320,1",
			"name": "Triangle of numbers n!(n-1)!...(n-k+1)!/(1!2!...k!).",
			"comment": [
				"Product of all matrix elements of n X k matrix M(i,j) = i+j (i=1..n-k, j=1..k). - _Peter Luschny_, Nov 26 2012",
				"These are the generalized binomial coefficients associated to the sequence A000178. - _Tom Edgar_, Feb 13 2014"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A009963/b009963.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = T(n-1,k-1)*A008279(n,n-k) = A000178(n)/(A000178(k)*A000178(n-k)) i.e., a \"supercombination\" of \"superfactorials\". - _Henry Bottomley_, May 22 2002",
				"Equals ConvOffsStoT transform of the factorials starting (1, 2, 6, 24, ...); e.g., ConvOffs transform of (1, 2, 6, 24) = (1, 24, 72, 24, 1). Note that A090441 = ConvOffsStoT transform of the factorials, A000142. - _Gary W. Adamson_, Apr 21 2008",
				"Asymptotic: T(n,k) ~ exp((3/2)*k^2 - zeta'(-1) + 3/4 - (3/2)*n*k)*(1+n)^((1/2)*n^2 + n + 5/12)*(1+k)^(-(1/2)*k^2 - k - 5/12)*(1 + n - k)^(-(1/2)*n^2 + n*k - (1/2)*k^2 - n + k - 5/12)/(sqrt(2*Pi). - _Peter Luschny_, Nov 26 2012",
				"T(n,k) = (n-k)!*C(n-1,k-1)*T(n-1,k-1) + k!*C(n-1,k)*T(n-1,k) where C(i,j) is given by A007318. - _Tom Edgar_, Feb 13 2014",
				"T(n,k) = Product_{i=1..k} (n+1-i)!/i!. - _Alois P. Heinz_, Jun 07 2017",
				"T(n, k) = BarnesG(n+2)/(BarnesG(k+2)*GarnesG(n-k+2)). - _G. C. Greubel_, Jan 04 2022"
			],
			"example": [
				"Rows start:",
				"  1;",
				"  1,   1;",
				"  1,   2,    1;",
				"  1,   6,    6,    1;",
				"  1,  24,   72,   24,   1;",
				"  1, 120, 1440, 1440, 120, 1;  etc."
			],
			"mathematica": [
				"(* First program *)",
				"row[n_]:= Table[Product[i+j, {i,1,n-k}, {j,1,k}], {k,0,n}];",
				"Array[row, 9, 0] // Flatten (* _Jean-François Alcover_, Jun 01 2019, after _Peter Luschny_ *)",
				"(* Second program *)",
				"T[n_, k_]:= BarnesG[n+2]/(BarnesG[k+2]*BarnesG[n-k+2]);",
				"Table[T[n, k], {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, Jan 04 2022 *)"
			],
			"program": [
				"(Sage)",
				"def A009963_row(n):",
				"    return [mul(mul(i+j for j in (1..k)) for i in (1..n-k)) for k in (0..n)]",
				"for n in (0..7): A009963_row(n)  # _Peter Luschny_, Nov 26 2012",
				"(Sage)",
				"def triangle_to_n_rows(n): #changing n will give you the triangle to row n.",
				"    N=[[1]+n*[0]]",
				"    for i in [1..n]:",
				"        N.append([])",
				"        for j in [0..n]:",
				"            if i\u003e=j:",
				"                N[i].append(factorial(i-j)*binomial(i-1,j-1)*N[i-1][j-1]+factorial(j)*binomial(i-1,j)*N[i-1][j])",
				"            else:",
				"                N[i].append(0)",
				"    return [[N[i][j] for j in [0..i]] for i in [0..n]]",
				"    # _Tom Edgar_, Feb 13 2014",
				"(MAGMA)",
				"A009963:= func\u003c n,k | (1/Factorial(n+1))*(\u0026*[ Factorial(n-j+1)/Factorial(j): j in [0..k]]) \u003e;",
				"[A009963(n,k): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Jan 04 2022"
			],
			"xref": [
				"Cf. A000178, A007318, A060854, A090441.",
				"Central column is A079478.",
				"Columns include A010796, A010797, A010798, A010799, A010800.",
				"Row sums give A193520."
			],
			"keyword": "nonn,tabl,changed",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_",
			"references": 17,
			"revision": 51,
			"time": "2022-01-05T10:36:48-05:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}