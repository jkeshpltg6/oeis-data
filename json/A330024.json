{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A330024",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 330024,
			"data": "0,0,0,0,0,0,0,0,0,0,10,11,12,0,0,0,0,17,0,0,20,21,22,23,0,0,26,0,0,29,30,0,0,0,0,0,0,0,38,0,40,41,21,14,44,45,46,47,48,0,50,0,26,17,27,27,28,57,58,29,30,20,31,31,32,65,66,0,68,23,23,71,0",
			"name": "a(n) = floor(n/z) where z is the number of zeros in the decimal expansion of 2^n, and a(n)=0 when z=0.",
			"comment": [
				"Is a(229)=229 the largest term?",
				"a(8949)=41; is 8949 the largest n such that a(n) \u003e= 41?",
				"Is 79391 the largest n such that a(n) \u003c= 30?",
				"Is 30 \u003c= a(n) \u003c= 36 true for all n \u003e= 713789?",
				"Conjecture: For every sequence which can be named as \"digit k appears m times in the decimal expansion of 2^n\", the sequences are finite for 0 \u003c= k \u003c= 9 and any given m \u003e= 0. Every digit from 0 to 9 are inclined to appear an equal number of times in the decimal expansion of 2^n as n increases."
			],
			"link": [
				"Metin Sariyar, \u003ca href=\"/A330024/b330024.txt\"\u003eTable of n, a(n) for n = 0..32000\u003c/a\u003e"
			],
			"formula": [
				"Conjecture: a(n) = 33 (= floor(10/log_10(2))) for all sufficiently large n. - _Pontus von Brömssen_, Jul 23 2021"
			],
			"example": [
				"a(11) = 11 because 2^11 = 2048, there is 1 zero in 2048 and the integer part of 11/1 is 11."
			],
			"maple": [
				"f:= proc(n) local z;",
				"  z:= numboccur(0,convert(2^n,base,10));",
				"  if z = 0 then 0 else floor(n/z) fi",
				"end proc:",
				"map(f, [$1..100]); # _Robert Israel_, Nov 28 2019"
			],
			"mathematica": [
				"Do[z=DigitCount[2^n,10,0];an=IntegerPart[n/z];If[z==0,Print[0],Print[an]],{n,0,8000}]"
			],
			"program": [
				"(MAGMA) a:=[0]; for n in [1..72] do z:=Multiplicity(Intseq(2^n),0); if z ne 0 then  Append(~a,Floor(n/z)); else Append(~a,0); end if; end for; a; // _Marius A. Burtea_, Nov 27 2019",
				"(PARI) a(n) = my(z=#select(d-\u003e!d, digits(2^n))); if (z, n\\z, 0); \\\\ _Michel Marcus_, Jan 07 2020",
				"(Python)",
				"def A330024(n):",
				"  z=str(2**n).count('0')",
				"  return n//z if z else 0 # _Pontus von Brömssen_, Jul 24 2021"
			],
			"xref": [
				"Cf. A007377, A027870."
			],
			"keyword": "nonn,base",
			"offset": "0,11",
			"author": "_Metin Sariyar_, Nov 27 2019",
			"references": 1,
			"revision": 41,
			"time": "2021-07-24T13:29:57-04:00",
			"created": "2020-02-11T12:07:36-05:00"
		}
	]
}