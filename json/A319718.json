{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A319718",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 319718,
			"data": "331,233,10177,224,10314,10323,210,203,110,10717,84700,420,121,340,210,206,236,10182,10454,112,206,99300,10217,10323,420,212,103,326,10033,136,212,110,206,110,10033,270,117,470,1008224,43400,170,11000,10024,21400,14201,206,410,420,212,236,1004644,10066,224,32100,10043,121",
			"name": "Underline the central digit of all terms: the underlined digits reconstruct the starting sequence. This is also true if one translates the sequence in French and underlines the central letter of each word: the underlined letters spell the (French) sequence again. This is the lexicographically earliest sequence where repeated terms are admitted.",
			"comment": [
				"By construction, all integers have here an odd number of digits and an odd number of letters in their French translation.",
				"The method used by the author was first to build a 21 X 10 array (10 digits X 21 letters) and to fill the 210 squares with terms t[$,d] that have the central letter $ and the central digit d.",
				"The Graner link explains why the author's array didn't take into account the letters B, J, K, W and Y.",
				"The second step was to find a(1). This term had to be the smallest one starting with a digit d and having d in its center, PLUS the  French translation of this term had to start with a letter $ and show the same letter $ in its center. This term is 331 (the digit 3 starts the integer and the digit 3 stands in the middle of it PLUS the letter T starts TROIS CENT TRENTE ET UN and the letter T stands in the middle of the word TROIS CENT (T)RENTE ET UN -- hyphens and spaces don't count as they are not letters).",
				"The rest of the sequence comes now automatically; a(2) for instance must be the smallest term t[R,3] when translated in French. This is [DEUX CENT T(R)ENTE-TROIS,233].",
				"Similarly a(3) must be the smallest term t[O,1] when translated in French and this is [DIX MILLE CENT S(O)IXANTE-DIX-SEPT,10177], etc.",
				"The first term diverging from A319921 is a(21) = [DEUXC(E)NTSIX, 206] and not a(21) = [TROISC(E)NTDEUX, 302] as repeated terms are allowed here.",
				"It might be of interest to show the equivalent sequences in other languages (English, German, Spanish, Italian, etc.) [Note: I think the French version is enough! - _N. J. A. Sloane_, Sep 27 2018]"
			],
			"reference": [
				"Eric Angelini, Maths étonnant, tangente, No. 189, juillet-août 2019, p. 29."
			],
			"link": [
				"Jean-Marc Falcoz, \u003ca href=\"/A319718/b319718.txt\"\u003eTable of n, a(n) for n = 1..1001\u003c/a\u003e",
				"Nicolas Graner, \u003ca href=\"https://www.graner.net/nicolas/nombres/nom.php\"\u003eLes grands nombres en français\u003c/a\u003e (in French)."
			],
			"example": [
				"The sequence starts with 331, 233, 10177, 224, 10314, and the central (underlined) digits are 3,3,1,2,3,... which are precisely the digits starting the sequence itself; now the successive 5 unique central letters of the above 5 French terms are T, R, O, I, S and this spells the beginning of TROIS CENT TRENTE ET UN, the term a(1)."
			],
			"xref": [
				"Cf. A319921 (repeated terms are forbidden, in contrast to this sequence)."
			],
			"keyword": "nonn,base,word",
			"offset": "1,1",
			"author": "_Eric Angelini_ and _Jean-Marc Falcoz_, Sep 26 2018",
			"references": 2,
			"revision": 43,
			"time": "2019-07-09T09:08:52-04:00",
			"created": "2018-09-27T12:46:14-04:00"
		}
	]
}