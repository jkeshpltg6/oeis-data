{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A039650",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 39650,
			"data": "2,2,3,3,5,3,7,5,7,5,11,5,13,7,7,7,17,7,19,7,13,11,23,7,13,13,19,13,29,7,31,17,13,17,13,13,37,19,13,17,41,13,43,13,13,23,47,17,43,13,13,13,53,19,41,13,37,29,59,17,61,31,37,13,43,13,67,13,13,13,71,13,73,37,41",
			"name": "Prime reached by iterating f(x) = phi(x)+1 on n.",
			"comment": [
				"Or, a(n) = lim_k {s(k,n)} where s(k,n) is defined inductively on k by: s(1,n) = n; s(k+1,n) = 1 + phi(s(k,n)). - _Joseph L. Pe_, Apr 30 2002",
				"Sequence A229487 gives the conjectured largest number that converges to prime(n). - _T. D. Noe_, Oct 17 2013",
				"For n\u003e1, phi(n) \u003c= n-1, with equality iff n is prime. So the trajectory decreases until it hits a prime. So a(n) always exists. - _N. J. A. Sloane_, Sep 22 2017"
			],
			"reference": [
				"Alexander S. Karpenko, Lukasiewicz Logics and Prime Numbers, Luniver Press, Beckington, 2006, p. 51."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A039650/b039650.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"s(24,1) = 24, s(24,2) = 1 + phi(24) = 1 + 8 = 9, s(24,3) = 1 + phi(9) = 1 + 6 = 7, s(24,4) = 1 + phi(7) = 1 + 6 = 7,.... Therefore a(24) = lim_k {s(24,k)} = 7."
			],
			"maple": [
				"A039650 := proc(n)",
				"    local nitr,niitr ;",
				"    niitr := n ;",
				"    while true do:",
				"        nitr := 1+numtheory[phi](niitr) ;",
				"        if nitr = niitr then",
				"            return nitr ;",
				"        end if;",
				"        niitr := nitr ;",
				"    end do:",
				"end proc:",
				"seq(A039650(n),n=1..40) ; # _R. J. Mathar_, Dec 11 2019"
			],
			"mathematica": [
				"f[n_] := FixedPoint[1 + EulerPhi[ # ] \u0026, n]; Table[ f[n], {n, 1, 75}]"
			],
			"xref": [
				"Cf. A039649, A039650, A039651, A039652, A039653, A039654, A039655, A039656, A229487."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_David W. Wilson_",
			"references": 15,
			"revision": 19,
			"time": "2019-12-11T08:19:33-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}