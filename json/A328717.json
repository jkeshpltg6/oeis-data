{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A328717",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 328717,
			"data": "7,0,6,2,1,1,4,0,3,2,5,9,7,4,0,9,6,9,9,3,1,0,0,3,1,7,5,7,6,2,5,6,4,0,2,7,6,6,0,2,4,6,4,7,1,8,5,2,9,4,6,8,6,3,9,4,2,1,1,7,4,0,2,1,6,5,6,7,7,6,0,4,4,3,8,3,8,3,0,0,7,6,8,3,3,7,4,5,6,6,4",
			"name": "Decimal expansion of Sum_{k\u003e=1} Kronecker(5,k)/k^2.",
			"comment": [
				"Let Chi() be a primitive character modulo d, the so-called Dirichlet L-series L(s,Chi) is the analytic continuation (see the functional equations involving L(s,Chi) in the MathWorld link entitled Dirichlet L-Series) of the sum Sum_{k\u003e=1} Chi(k)/k^s, Re(s)\u003e0 (if d = 1, the sum converges requires Re(s)\u003e1).",
				"If s != 1, we can represent L(s,Chi) in terms of the Hurwitz zeta function by L(s,Chi) = (Sum_{k=1..d} Chi(k)*zeta(s,k/d))/d^s.",
				"L(s,Chi) can also be represented in terms of the polylog function by L(s,Chi) = (Sum_{k=1..d} Chi'(k)*polylog(s,u^k))/(Sum_{k=1..d} Chi'(k)*u^k), where Chi' is the complex conjugate of Chi, u is any primitive d-th root of unity.",
				"If m is a positive integer, we have L(m,Chi) = (Sum_{k=1..d} Chi(k)*polygamma(m-1,k/d))/((-d)^m*(m-1)!).",
				"In this sequence we have Chi = A080891 and s = 2."
			],
			"link": [
				"M. W. Coffey, \u003ca href=\"https://arxiv.org/abs/1701.07064\"\u003eSummatory relations and prime products for the Stieltjes constants and other related results\u003c/a\u003e, arXiv:1701.07064, eq. (2.1)",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DirichletL-Series.html\"\u003eDirichlet L-Series\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PolygammaFunction.html\"\u003ePolygamma Function\u003c/a\u003e"
			],
			"formula": [
				"Equals 4*Pi^2/(25*sqrt(5)).",
				"Equals (zeta(2,1/5) - zeta(2,2/5) - zeta(2,3/5) + zeta(2,4/5))/25, where zeta(s,a) is the Hurwitz zeta function.",
				"Equals (polylog(2,u) - polylog(2,u^2) - polylog(2,u^3) + polylog(2,u^4))/sqrt(5), where u = exp(2*Pi*i/5) is a 5th primitive root of unity, i = sqrt(-1).",
				"Equals (polygamma(1,1/5) - polygamma(1,2/5) - polygamma(1,3/5) - polygamma(1,4/5))/25."
			],
			"example": [
				"1 - 1/2^2 - 1/3^2 + 1/4^2 + 1/6^2 - 1/7^2 - 1/8^2 + 1/9^2 + ... = 4*Pi^2/(25*sqrt(5)) = 0.7062114032..."
			],
			"mathematica": [
				"RealDigits[4*Pi^2/(25*Sqrt[5]), 10, 102] // First"
			],
			"program": [
				"(PARI) default(realprecision, 100); 4*Pi^2/(25*sqrt(5))"
			],
			"xref": [
				"Cf. A080891.",
				"Decimal expansion of Sum_{k\u003e=1} Kronecker(d,k)/k^2, where d is a fundamental discriminant: A309710 (d=-8), A103133 (d=-7), A006752 (d=-4), A086724 (d=-3), A013661 (d=1), this sequence (d=5), A328895 (d=8), A258414 (d=12).",
				"Decimal expansion of Sum_{k\u003e=1} Kronecker(5,k)/k^s: A086466 (s=1), this sequence (s=2), A328723 (s=3)."
			],
			"keyword": "nonn,cons",
			"offset": "0,1",
			"author": "_Jianing Song_, Nov 19 2019",
			"references": 4,
			"revision": 51,
			"time": "2021-01-15T10:36:19-05:00",
			"created": "2019-11-21T07:30:36-05:00"
		}
	]
}