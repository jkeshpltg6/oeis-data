{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A288318",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 288318,
			"data": "1,0,1,0,0,1,0,2,0,1,0,0,0,0,1,0,4,3,0,0,1,0,6,6,0,0,0,1,0,8,0,4,0,0,0,1,0,24,9,20,0,0,0,0,1,0,52,54,20,5,0,0,0,0,1,0,96,138,0,45,0,0,0,0,0,1,0,212,207,16,105,6,0,0,0,0,0,1,0,504,360,200,70,84,0,0,0,0,0,0,1",
			"name": "Number T(n,k) of Dyck paths of semilength n such that each positive level has exactly k peaks; triangle T(n,k), n\u003e=0, 0\u003c=k\u003c=n, read by rows.",
			"comment": [
				"T(n,k) is defined for all n,k \u003e= 0.  The triangle contains only the terms for k\u003c=n. T(0,k) = 1 and T(n,k) = 0 if k \u003e n \u003e 0."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A288318/b288318.txt\"\u003eRows n = 0..140, flattened\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Lattice_path#Counting_lattice_paths\"\u003eCounting lattice paths\u003c/a\u003e"
			],
			"formula": [
				"T(n,n) = 1.",
				"T(n+1,n) = 0.",
				"T(2*n+1,n) = (n+1) for n\u003e0.",
				"T(2*n+2,n) = A005564(n+1) for n\u003e1.",
				"T(3*n,n) = A000984(n) = binomial(2*n,n).",
				"T(3*n+1,n) = 0.",
				"T(3*n+2,n) = (n+1)^2 for n\u003e0."
			],
			"example": [
				". T(5,1) = 4:",
				".               /\\        /\\          /\\        /\\",
				".            /\\/  \\      /  \\/\\    /\\/  \\      /  \\/\\",
				".         /\\/      \\  /\\/      \\  /      \\/\\  /      \\/\\ .",
				".",
				". T(5,2) = 3:",
				".              /\\/\\      /\\/\\      /\\/\\",
				".         /\\/\\/    \\  /\\/    \\/\\  /    \\/\\/\\  .",
				".",
				"Triangle T(n,k) begins:",
				"  1;",
				"  0,  1;",
				"  0,  0,  1;",
				"  0,  2,  0,  1;",
				"  0,  0,  0,  0, 1;",
				"  0,  4,  3,  0, 0, 1;",
				"  0,  6,  6,  0, 0, 0, 1;",
				"  0,  8,  0,  4, 0, 0, 0, 1;",
				"  0, 24,  9, 20, 0, 0, 0, 0, 1;",
				"  0, 52, 54, 20, 5, 0, 0, 0, 0, 1;"
			],
			"maple": [
				"b:= proc(n, k, j) option remember;",
				"     `if`(n=j, 1, add(b(n-j, k, i)*(binomial(i, k)",
				"      *binomial(j-1, i-1-k)), i=1..min(j+k, n-j)))",
				"    end:",
				"T:= (n, k)-\u003e `if`(n=0, 1, b(n, k$2)):",
				"seq(seq(T(n, k), k=0..n), n=0..14);"
			],
			"mathematica": [
				"b[n_, k_, j_] := b[n, k, j] = If[n == j, 1, Sum[b[n - j, k, i]*(Binomial[i, k]*Binomial[j - 1, i - 1 - k]), {i, 1, Min[j + k, n - j]}]];",
				"T[n_, k_] := If[n == 0, 1, b[n, k, k]];",
				"Table[T[n, k], {n, 0, 14}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, May 25 2018, translated from Maple *)"
			],
			"xref": [
				"Columns k=0-10 give: A000007, A287846, A287845, A288319, A288320, A288321, A288322, A288323, A288324, A288325, A288326.",
				"Row sums give A287987.",
				"Cf. A000108, A000984, A005564, A288108, A288940."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Alois P. Heinz_, Jun 07 2017",
			"references": 14,
			"revision": 22,
			"time": "2018-10-18T15:44:38-04:00",
			"created": "2017-06-07T22:00:19-04:00"
		}
	]
}