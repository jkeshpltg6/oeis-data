{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A081215",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 81215,
			"data": "1,0,1,5,41,434,5713,90075,1657009,34867844,826446281,21794641505,633095889817,20088655029078,691413758034721,25657845139503479,1021273028302258913,43404581642184336392,1961870762757168078553",
			"name": "a(n) = (n^(n+1)+(-1)^n)/(n+1)^2.",
			"comment": [
				"From _Mathew Englander_, Oct 19 2020: (Start)",
				"The sum of two adjacent terms of the sequence cannot be prime.",
				"In base n, a(n) has n-1 digits, which are (beginning from the left): n-2, 2, n-4, 4, and so on, except that if n is even the rightmost digit is 1 instead of 0. In that case, the other digits form a palindrome with every even digit from 2 to n-2 appearing twice. For example, a(14) in base 14 is c2a486684a2c1. If n is odd, then all digits from 1 to n-1 occur exactly once. For example, a(15) in base 15 is d2b496785a3c1e.",
				"For any positive integer k, any prime p, and any positive integer h such that h*p \u003e 2, a(h*p^k - 2) == (-1)^h * (1 - 2^(h-1)) (mod p). For example, a(7*p^k - 2) == 63 (mod p); a(10*p^k - 2) == -511 (mod p).",
				"Suppose k and m are positive integers. If k is even, then a(k*m) == 1, a(k*m+1) == 0, and a(k*m-1) == -1 (all mod m). If k is odd, then a(k*m) == (-1)^m and a(k*m+1) == ceiling(m/2) (both mod m), while a(k*m-1) == m/2 - 1 for m even, and a(k*m-1) == 1 for m odd (mod m).",
				"For proofs of the above, see the Englander link. (End)"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A081215/b081215.txt\"\u003eTable of n, a(n) for n = 0..387\u003c/a\u003e",
				"Mathew Englander, \u003ca href=\"/A081215/a081215.pdf\"\u003eComments on OEIS A081215\u003c/a\u003e"
			],
			"formula": [
				"a(n) = (-1)^n + Sum_{k=1..n} (-1)^(k+1)*(n+1)^(n-k)*C(n+1,n+2-k). - _Gionata Neri_, May 19 2016",
				"E.g.f.: (Ei(1,x) - Ei(1,-LambertW(-x)))/x. - _Robert Israel_, May 19 2016",
				"For n \u003e 1, a(n) = Sum_{k=1..floor(n/2)} (n^(n-2*k) * (2*k/n + n - 2*k)). - _Mathew Englander_, Oct 19 2020"
			],
			"maple": [
				"seq((j^(j+1)+(-1)^j)/(j+1)^2, j=0..50); # _Robert Israel_, May 19 2016"
			],
			"mathematica": [
				"Array[(#^(# + 1) + (-1)^#)/(# + 1)^2 \u0026, 19, 0] (* _Michael De Vlieger_, Nov 13 2020 *)"
			],
			"program": [
				"(PARI) a(n) = (n^(n+1)+(-1)^n)/(n+1)^2; \\\\ _Michel Marcus_, Oct 20 2020"
			],
			"xref": [
				"Cf. A081209, A110567, A076951, A273319, A193746, and A060073."
			],
			"keyword": "easy,nonn",
			"offset": "0,4",
			"author": "_Vladeta Jovovic_, Apr 17 2003",
			"references": 4,
			"revision": 26,
			"time": "2020-12-12T15:52:36-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}