{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A124761",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 124761,
			"data": "0,0,0,0,0,1,0,0,0,1,0,1,0,1,0,0,0,1,1,1,0,1,1,1,0,1,0,1,0,1,0,0,0,1,1,1,0,2,1,1,0,1,0,1,1,2,1,1,0,1,1,1,0,1,1,1,0,1,0,1,0,1,0,0,0,1,1,1,1,2,1,1,0,1,1,2,1,2,1,1,0,1,1,1,0,1,1,1,1,2,1,2,1,2,1,1,0,1,1,1,0,2,1,1,0",
			"name": "Number of falls for compositions in standard order.",
			"comment": [
				"The standard order of compositions is given by A066099.",
				"A composition of n is a finite sequence of positive integers summing to n. The k-th composition in standard order (row k of A066099) is obtained by taking the set of positions of 1's in the reversed binary expansion of k, prepending 0, taking first differences, and reversing again. a(n) is one fewer than the number of maximal weakly increasing runs in this composition. Alternatively, a(n) is the number of strict descents in the same composition. For example, the weakly increasing of runs of the 1234567th composition are ((3),(2),(1,2,2),(1,2,5),(1,1,1)), so a(1234567) = 5 - 1 = 4. The 4 strict descents together with the weak ascents are: 3 \u003e 2 \u003e 1 \u003c= 2 \u003c= 2 \u003e 1 \u003c= 2 \u003c= 5 \u003e 1 \u003c= 1 \u003c= 1. - _Gus Wiseman_, Apr 08 2020"
			],
			"formula": [
				"For a composition b(1),...,b(k), a(n) = Sum_{1\u003c=i=1\u003ck, b(i)\u003eb(i+1)} 1.",
				"For n \u003e 0, a(n) = A124766(n) - 1. - _Gus Wiseman_, Apr 08 2020"
			],
			"example": [
				"Composition number 11 is 2,1,1; 2\u003e1\u003c=1, so a(11) = 1.",
				"The table starts:",
				"  0",
				"  0",
				"  0 0",
				"  0 1 0 0",
				"  0 1 0 1 0 1 0 0",
				"  0 1 1 1 0 1 1 1 0 1 0 1 0 1 0 0",
				"  0 1 1 1 0 2 1 1 0 1 0 1 1 2 1 1 0 1 1 1 0 1 1 1 0 1 0 1 0 1 0 0"
			],
			"mathematica": [
				"stc[n_]:=Differences[Prepend[Join@@Position[Reverse[IntegerDigits[n,2]],1],0]]//Reverse;",
				"Table[Length[Select[Partition[stc[n],2,1],Greater@@#\u0026]],{n,0,100}] (* _Gus Wiseman_, Apr 08 2020 *)"
			],
			"xref": [
				"Cf. A066099, A124760, A124763, A124764, A011782 (row lengths), A045883 (row sums), A333213, A333220, A333379.",
				"Positions of zeros are A225620.",
				"Compositions of n with k strict descents are A238343.",
				"All of the following pertain to compositions in standard order (A066099):",
				"- Length is A000120.",
				"- Sum is A070939.",
				"- Weakly decreasing compositions are A114994.",
				"- Adjacent equal pairs are counted by A124762.",
				"- Weakly decreasing runs are counted by A124765.",
				"- Weakly increasing runs are counted by A124766.",
				"- Equal runs are counted by A124767.",
				"- Strictly increasing runs are counted by A124768.",
				"- Strictly decreasing runs are counted by A124769.",
				"- Weakly increasing compositions are A225620.",
				"- Reverse is A228351 (triangle).",
				"- Strict compositions are A233564.",
				"- Initial intervals are A246534.",
				"- Constant compositions are A272919.",
				"- Normal compositions are A333217.",
				"- Permutations are A333218.",
				"- Strictly decreasing compositions are A333255.",
				"- Strictly increasing compositions are A333256.",
				"- Runs-resistance is A333628."
			],
			"keyword": "easy,nonn,tabf",
			"offset": "0,38",
			"author": "_Franklin T. Adams-Watters_, Nov 06 2006",
			"references": 11,
			"revision": 7,
			"time": "2020-04-09T00:54:54-04:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}