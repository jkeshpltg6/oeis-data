{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A093709",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 93709,
			"data": "1,1,1,0,1,0,0,0,1,1,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0",
			"name": "Characteristic function of squares or twice squares.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Partial sums of a(n) for n \u003e= 1 are A071860(n+1). - _Jaroslav Krizek_, Oct 18 2009",
				"For n \u003e 0, this is also the number of different triangular polyabolos that can be formed from n congruent isosceles right triangles (illustrated at A245676). - _Douglas J. Durian_, Sep 10 2017"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A093709/b093709.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"S. Cooper and M. Hirschhorn, \u003ca href=\"http://dx.doi.org/10.1216/rmjm/1008959672\"\u003eOn some infinite product identities\u003c/a\u003e, Rocky Mountain J. Math., 31 (2001) 131-139. see p. 133 Theorem 1.",
				"John S. Rutherford, \u003ca href=\"http://dx.doi.org/10.1107/S010876730804333X\"\u003eSublattice enumeration. IV. Equivalence classes of plane sublattices by parent Patterson symmetry and colour lattice group type\u003c/a\u003e, Acta Cryst. (2009). A65, 156-163. - From _N. J. A. Sloane_, Feb 23 2009",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"\u003ca href=\"/index/Ch#char_fns\"\u003eIndex entries for characteristic functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of psi(q^4) * f(-q^3, -q^5) / f(-q, -q^7) in powers of q where psi(), f() are Ramanujan theta functions.",
				"Expansion of f(-q^3, -q^5)^2 / psi(-q) in powers of q where psi(), f() are Ramanujan theta functions. - _Michael Somos_, Jan 01 2015",
				"Euler transform of period 8 sequence [ 1, 0, -1, 1, -1, 0, 1, -1, ...].",
				"G.f. A(x) satisfies A(x^2) = (A(x) + A(-x)) / 2. a(2*n) = a(n).",
				"Given g.f. A(x), then A(x) / A(x^2) = 1 + x*A092869(x^2).",
				"Given g.f. A(x), then B(x) = A(x^2) / A(x) satisfies 0 = f(B(x), B(x^2)) where f(u, v) = u^2 + v - 2(u + u^2)*v + 2*(u*v)^2.",
				"Multiplicative with a(0) = a(2^e) = 1, a(p^e) = 1 if e even, 0 otherwise.",
				"a(n) = A053866(n) unless n=0. Characteristic function of A028982 union 0.",
				"G.f.: (theta_3(q) + theta_3(q^2)) / 2 = 1 + (Sum_{k\u003e0} x^(k^2) + x^(2*k^2)).",
				"Dirichlet g.f.: zeta(2*s) * (1 + 2^-s).",
				"For n\u003e0: a(n) = A010052(n) + A010052(A004526(n))*A059841(n). - _Reinhard Zumkeller_, Nov 14 2009",
				"a(n) = A000035(A000203(n)) = A000035(A000593(n)) = A000035(A001227(n)), if n\u003e0. - _Omar E. Pol_, Apr 05 2016",
				"Sum_{k=1..n} a(k) ~ (1 + 1/sqrt(2)) * sqrt(n). - _Vaclav Kotesovec_, Oct 16 2020"
			],
			"example": [
				"G.f. = 1 + q + q^2 + q^4 + q^8 + q^9 + q^16 + q^18 + q^25 + q^32 + q^36 + q^49 + ..."
			],
			"maple": [
				"seq(`if`(issqr(n) or issqr(n/2),1,0), n=0..100); # _Robert Israel_, Apr 05 2016"
			],
			"mathematica": [
				"Table[Boole[IntegerQ[Sqrt[n]] || IntegerQ[Sqrt[2*n]]], {n, 0, 104}] (* _Jean-François Alcover_, Dec 05 2013 *)",
				"a[ n_] := If[ n \u003c 0, 0, Boole[ OddQ [ Length @ Divisors[ n]] || OddQ [ Length @ Divisors[ 2 n]]]]; (* _Michael Somos_, Jan 01 2015 *)",
				"a[ n_] := SeriesCoefficient[ (EllipticTheta[ 3, 0, q] + EllipticTheta[ 3, 0, q^2]) / 2, {q, 0, n}]; (* _Michael Somos_, Jan 01 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = issquare(n) || issquare(2*n)};",
				"(MAGMA) A := Basis( ModularForms( Gamma1(8), 1/2), 104); A[1] + A[2]; /* _Michael Somos_, Jan 01 2015 */"
			],
			"xref": [
				"Cf. A000203, A000593, A001227, A028982, A053866, A092869."
			],
			"keyword": "nonn,mult",
			"offset": "0,1",
			"author": "_Michael Somos_, Apr 11 2004",
			"references": 12,
			"revision": 52,
			"time": "2021-03-12T22:24:42-05:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}