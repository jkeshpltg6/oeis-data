{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A341840",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 341840,
			"data": "0,0,0,0,1,0,0,1,1,0,0,0,2,0,0,0,0,3,3,0,0,0,1,3,3,3,1,0,0,1,2,3,3,2,1,0,0,0,1,3,4,3,1,0,0,0,0,0,0,4,4,0,0,0,0,0,1,0,0,7,5,7,0,0,1,0,0,1,1,0,7,6,6,7,0,1,1,0,0,0,2,0,7,7,6,7,7,0,2,0,0",
			"name": "Square array T(n, k), n, k \u003e= 0, read by antidiagonals; for any number m with runs in binary expansion (r_1, ..., r_j), let R(m) = {r_1 + ... + r_j, r_2 + ... + r_j, ..., r_j}; T(n, k) is the unique number t such that R(t) is the intersection of R(n) and of R(k).",
			"comment": [
				"For any m \u003e 0, R(m) contains the partial sums of the m-th row of A227736; by convention, R(0) = {}.",
				"The underlying idea is to merge in an optimal way the runs in binary expansions of n and of k so that they match, hence the relationship with A003188."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A341840/b341840.txt\"\u003eTable of n, a(n) for n = 0..10010\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A341840/a341840.png\"\u003eColored representation of the table for n, k \u003c 2^10\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A341840/a341840.gp.txt\"\u003ePARI program for A341840\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = T(k, n).",
				"T(m, T(n, k)) = T(T(m, n), k).",
				"T(n, n) = n.",
				"T(n, 0) = 0.",
				"A070939(T(n, k)) \u003c= min(A070939(n), A070939(k)).",
				"A003188(T(n, k)) = A003188(n) AND A003188(k) (where AND denotes the bitwise AND operator)."
			],
			"example": [
				"Array T(n, k) begins:",
				"  n\\k|  0  1  2  3  4  5  6  7   8   9  10  11  12  13  14  15",
				"  ---+--------------------------------------------------------",
				"    0|  0  0  0  0  0  0  0  0   0   0   0   0   0   0   0   0",
				"    1|  0  1  1  0  0  1  1  0   0   1   1   0   0   1   1   0",
				"    2|  0  1  2  3  3  2  1  0   0   1   2   3   3   2   1   0",
				"    3|  0  0  3  3  3  3  0  0   0   0   3   3   3   3   0   0",
				"    4|  0  0  3  3  4  4  7  7   7   7   4   4   3   3   0   0",
				"    5|  0  1  2  3  4  5  6  7   7   6   5   4   3   2   1   0",
				"    6|  0  1  1  0  7  6  6  7   7   6   6   7   0   1   1   0",
				"    7|  0  0  0  0  7  7  7  7   7   7   7   7   0   0   0   0",
				"    8|  0  0  0  0  7  7  7  7   8   8   8   8  15  15  15  15",
				"    9|  0  1  1  0  7  6  6  7   8   9   9   8  15  14  14  15",
				"   10|  0  1  2  3  4  5  6  7   8   9  10  11  12  13  14  15",
				"   11|  0  0  3  3  4  4  7  7   8   8  11  11  12  12  15  15",
				"   12|  0  0  3  3  3  3  0  0  15  15  12  12  12  12  15  15",
				"   13|  0  1  2  3  3  2  1  0  15  14  13  12  12  13  14  15",
				"   14|  0  1  1  0  0  1  1  0  15  14  14  15  15  14  14  15",
				"   15|  0  0  0  0  0  0  0  0  15  15  15  15  15  15  15  15"
			],
			"program": [
				"(PARI) See Links section."
			],
			"xref": [
				"Cf. A003188, A003987, A005811, A070939, A227736, A341839, A341840, A341841."
			],
			"keyword": "nonn,tabl,base",
			"offset": "0,13",
			"author": "_Rémy Sigrist_, Feb 21 2021",
			"references": 4,
			"revision": 13,
			"time": "2021-02-24T08:20:36-05:00",
			"created": "2021-02-23T12:38:14-05:00"
		}
	]
}