{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A249588",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 249588,
			"data": "1,1,5,49,856,22376,842536,42409480,2782192064,229357803456,23289083584704,2851295406197184,414855423241758720,70695451937596732416,13958230719814052097024,3159974451734082088897536,813380358295803762813321216,236172126115504055456155975680",
			"name": "G.f.: Product_{n\u003e=1} 1/(1 - x^n/n^2) = Sum_{n\u003e=0} a(n)*x^n/n!^2.",
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A249588/b249588.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e",
				"MathOverflow, \u003ca href=\"https://mathoverflow.net/questions/404805/asymptotic-growth-of-a-sum-involving-partitions\"\u003easymptotic growth of a sum involving partitions\u003c/a\u003e, Sep 26 2021."
			],
			"formula": [
				"a(n) = Sum_{k=1..n} n!*(n-1)!/(n-k)!^2 * b(k) * a(n-k), where b(k) = Sum_{d|k} d^(1-2*k/d) and a(0) = 1 (after Vladeta Jovovic in A007841).",
				"a(n) ~ 2 * n!^2. - _Vaclav Kotesovec_, Mar 05 2016"
			],
			"example": [
				"G.f.: A(x) = 1 + x + 5*x^2/2!^2 + 49*x^3/3!^2 + 856*x^4/4!^2 +...",
				"where",
				"A(x) = 1/((1-x)*(1-x^2/4)*(1-x^3/9)*(1-x^4/16)*(1-x^5/25)*...)."
			],
			"mathematica": [
				"b[k_] := b[k] = DivisorSum[k, #^(1-2*k/#) \u0026]; a[0] = 1; a[n_] := a[n] = Sum[n!*(n-1)!/(n-k)!^2*b[k]*a[n-k], {k, 1, n}]; Table[a[n], {n, 0, 20}] (* _Jean-François Alcover_, Dec 23 2015, adapted from PARI *)",
				"Table[n!^2 * SeriesCoefficient[Product[1/(1 - x^m/m^2), {m, 1, n}], {x, 0, n}], {n, 0, 20}] (* _Vaclav Kotesovec_, Mar 05 2016 *)"
			],
			"program": [
				"(PARI) {a(n)=n!^2*polcoeff(prod(k=1, n, 1/(1-x^k/k^2 +x*O(x^n))),n)}",
				"for(n=0,20,print1(a(n),\", \"))",
				"(PARI) /* Using logarithmic derivative: */",
				"{b(k) = sumdiv(k,d, d^(1-2*k/d))}",
				"{a(n) = if(n==0,1,sum(k=1,n, n!*(n-1)!/(n-k)!^2 * b(k) * a(n-k)))}",
				"for(n=0,20,print1(a(n),\", \"))"
			],
			"xref": [
				"Cf. A249590, A249607, A007841, A249593, A249592, A269791, A269793, A269794."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Paul D. Hanna_, Nov 01 2014",
			"ext": [
				"Name clarified by _Vaclav Kotesovec_, Mar 05 2016"
			],
			"references": 13,
			"revision": 30,
			"time": "2021-09-27T08:13:26-04:00",
			"created": "2014-11-01T22:56:50-04:00"
		}
	]
}