{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A255739",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 255739,
			"data": "1,2,3,9,51,473,3233,7657,7722,20002,124170,126137",
			"name": "Indices of nontrivial zeros of Riemann zeta function whose imaginary part sets a record for the absolute minimal difference from an integer.",
			"comment": [
				"We consider here the imaginary part of 1/2 + iy = z, for which Zeta(z) is a zero.",
				"No more terms below 600000. - _Robert G. Wilson v_, Sep 30 2015",
				"Is there an Im(rho_k) that is also an positive integer?. Is there a minimum gap between an Im(rho_k) and an positive integer?. At present it is not known whether this sequence is finite or infinite. - _Omar E. Pol_, Oct 13 2015"
			],
			"link": [
				"Andrew M. Odlyzko, \u003ca href=\"http://www.dtc.umn.edu/~odlyzko/zeta_tables\"\u003eTables of zeros of the Riemann zeta function\u003c/a\u003e",
				"Andrew M. Odlyzko, \u003ca href=\"http://www.dtc.umn.edu/~odlyzko/doc/arch/zeta.zero.spacing.pdf\"\u003eOn the distribution of spacings between zeros of the zeta function\u003c/a\u003e",
				"\u003ca href=\"/index/Z#zeta_function\"\u003eIndex entries for zeta function\u003c/a\u003e"
			],
			"formula": [
				"A255742(n) = A002410(a(n))."
			],
			"example": [
				"-------------------------------------------------------------------",
				"                                     Absolute      New",
				"k      Im(rho_k)       A002410(k)   difference   record   n   a(n)",
				"-------------------------------------------------------------------",
				"1    14.134725142    \u003e    14        0.134725142    Yes    1    1",
				"2    21.022039639    \u003e    21        0.022039639    Yes    2    2",
				"3    25.010857580    \u003e    25        0.010857580    Yes    3    3",
				"4    30.424876126    \u003e    30        0.424876126    Not",
				"5    32.935061588    \u003c    33        0.064938412    Not",
				"6    37.586178159    \u003c    38        0.413821841    Not",
				"7    40.918719012    \u003c    41        0.081280988    Not",
				"8    43.327073281    \u003e    43        0.327073281    Not",
				"9    48.005150881    \u003e    48        0.005150881    Yes    4    9",
				"10   49.773832478    \u003c    50        0.226167522    Not",
				"...",
				"Where rho_k is the k-th nontrivial zero of Riemann zeta function.",
				"We compute all decimal places of Im(rho_k), but in above table appear only 9 decimal places."
			],
			"mathematica": [
				"mn = Infinity; k = 1; lst = {}; While[k \u003c 2501, a = N[ Abs[ Im[ ZetaZero[",
				"k]] - Round[ Im[ ZetaZero[ k]] ]], 32]; If[a \u003c mn, AppendTo[lst, k];",
				"Print[k]; mn = a]; k++]; lst (* _Robert G. Wilson v_, Sep 29 2015 *)"
			],
			"xref": [
				"Cf. A002410, A255742."
			],
			"keyword": "nonn,hard,more",
			"offset": "1,2",
			"author": "_Omar E. Pol_, Mar 17 2015",
			"ext": [
				"a(6)-a(10) from _Robert G. Wilson v_, Sep 29 2015",
				"a(11)-a(12) from _Robert G. Wilson v_, Sep 30 2015"
			],
			"references": 10,
			"revision": 22,
			"time": "2015-11-02T09:33:16-05:00",
			"created": "2015-03-19T07:16:22-04:00"
		}
	]
}