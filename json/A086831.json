{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A086831",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 86831,
			"data": "1,1,-1,-2,-1,-1,-1,0,0,-1,-1,2,-1,-1,1,0,-1,0,-1,2,1,-1,-1,0,0,-1,0,2,-1,1,-1,0,1,-1,1,0,-1,-1,1,0,-1,1,-1,2,0,-1,-1,0,0,0,1,2,-1,0,1,0,1,-1,-1,-2,-1,-1,0,0,1,1,-1,2,1,1,-1,0,-1,-1,0,2,1,1,-1,0,0,-1,-1,-2,1,-1,1,0,-1,0,1,2,1,-1,1,0,-1,0,0,0,-1,1,-1,0,-1",
			"name": "Ramanujan sum c_n(2).",
			"comment": [
				"Mobius transform of 1,2,0,0,0,0,... (A130779). - _R. J. Mathar_, Mar 24 2012"
			],
			"reference": [
				"T. M. Apostol, Introduction to Analytic Number Theory, Springer-Verlag, 1976.",
				"E. C. Titchmarsh and D. R. Heath-Brown, The theory of the Riemann zeta-function, 2nd edn., 1986."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A086831/b086831.txt\"\u003eTable of n, a(n) for n = 1..65537\u003c/a\u003e",
				"Tom M. Apostol, \u003ca href=\"https://projecteuclid.org/download/pdf_1/euclid.pjm/1102968273\"\u003eArithmetical properties of generalized Ramanujan sums\u003c/a\u003e, Pacific J. Math. 41 (1972), 281-293.",
				"Eckford Cohen, \u003ca href=\"https://dx.doi.org/10.1073/pnas.41.11.939\"\u003eA class of arithmetic functions\u003c/a\u003e, Proc. Natl. Acad. Sci. USA 41 (1955), 939-944.",
				"A. Elashvili, M. Jibladze, and D. Pataraia, \u003ca href=\"http://dx.doi.org/10.1023/A:1018727630642\"\u003eCombinatorics of necklaces and \"Hermite reciprocity\"\u003c/a\u003e, J. Algebraic Combin. 10 (1999), 173-188.",
				"M. L. Fredman, \u003ca href=\"https://doi.org/10.1016/0097-3165(75)90008-4\"\u003eA symmetry relationship for a class of partitions\u003c/a\u003e, J. Combinatorial Theory Ser. A 18 (1975), 199-202.",
				"Otto Hölder, \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/pmf/pmf43/pmf4312.pdf\"\u003eZur Theorie der Kreisteilungsgleichung K_m(x)=0\u003c/a\u003e, Prace mat.-fiz. 43 (1936), 13-23.",
				"C. A. Nicol, \u003ca href=\"https://dx.doi.org/10.1073/pnas.39.9.963\"\u003eOn restricted partitions and a generalization of the Euler phi number and the Moebius function\u003c/a\u003e, Proc. Natl. Acad. Sci. USA 39(9) (1953), 963-968.",
				"C. A. Nicol and H. S. Vandiver, \u003ca href=\"https://dx.doi.org/10.1073/pnas.40.9.825 \"\u003eA von Sterneck arithmetical function and restricted partitions with respect to a modulus\u003c/a\u003e, Proc. Natl. Acad. Sci. USA 40(9) (1954), 825-835.",
				"K. G. Ramanathan, \u003ca href=\"https://www.ias.ac.in/article/fulltext/seca/020/01/0062-0069\"\u003eSome applications of Ramanujan's trigonometrical sum C_m(n)\u003c/a\u003e, Proc. Indian Acad. Sci., Sect. A 20 (1944), 62-69.",
				"Srinivasa Ramanujan, \u003ca href=\"http://ramanujan.sirinudi.org/Volumes/published/ram21.pdf\"\u003eOn certain trigonometric sums and their applications in the theory of numbers\u003c/a\u003e, Trans. Camb. Phil. Soc. 22 (1918), 259-276.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Ramanujan%27s_sum\"\u003eRamanujan's sum\u003c/a\u003e.",
				"Aurel Wintner, \u003ca href=\"https://www.jstor.org/stable/2371672\"\u003eOn a statistics of the Ramanujan sums\u003c/a\u003e, Amer. J. Math., 64(1) (1942), 106-114."
			],
			"formula": [
				"For a general k \u003e= 1, c_n(k) = phi(n)*mu(n/gcd(n, k)) / phi(n/gcd(n, k)); so c_n(1) = mu(n) = A008683(n).",
				"a(n) = phi(n)*mu(n/gcd(n, 2)) / phi(n/gcd(n, 2)).",
				"Dirichlet g.f.: (1+2^(1-s))/zeta(s). [Titchmarsh eq. (1.5.4)] - _R. J. Mathar_, Mar 26 2011"
			],
			"example": [
				"a(4) = -2 because the primitive fourth roots of unity are i and -i.  We sum their squares to get i^2 + (-i)^2 = -1 + -1 = -2. - _Geoffrey Critzer_, Dec 30 2015"
			],
			"maple": [
				"with(numtheory):a:=n-\u003ephi(n)*mobius(n/gcd(n,2))/phi(n/gcd(n,2)): seq(a(n),n=1..130); # _Emeric Deutsch_, Dec 23 2004"
			],
			"mathematica": [
				"f[list_, i_] := list[[i]]; nn = 105; a = Table[MoebiusMu[n], {n, 1, nn}]; b =Table[If[IntegerQ[2/n], n, 0], {n, 1,nn}];Table[DirichletConvolve[f[a, n], f[b, n], n, m], {m, 1, nn}] (* _Geoffrey Critzer_, Dec 30 2015 *)"
			],
			"program": [
				"(PARI) A086831(n) = (eulerphi(n)*moebius(n/gcd(n, 2))/eulerphi(n/gcd(n, 2))); \\\\ _Antti Karttunen_, Sep 27 2018"
			],
			"xref": [
				"Cf. A000010, A008683, A054532, A054533, A054534, A054535.",
				"Cf. A085097, A085384, A085639, A085906 for Ramanujan sums c_n(3), c_n(4), c_n(5), c_n(6)."
			],
			"keyword": "sign,easy,mult",
			"offset": "1,4",
			"author": "Yuval Dekel (dekelyuval(AT)hotmail.com), Aug 07 2003",
			"ext": [
				"Corrected and extended by _Emeric Deutsch_, Dec 23 2004"
			],
			"references": 7,
			"revision": 33,
			"time": "2021-03-22T05:48:44-04:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}