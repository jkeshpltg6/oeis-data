{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A337874",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 337874,
			"data": "12,14,48,62,60,70,112,124,132,154,160,189,156,182,192,254,204,238,228,266,240,310,276,322,315,351,300,350,348,406,336,372,434,448,508,444,518,492,574,516,602,564,658,528,682,560,620,636,742",
			"name": "Table read by rows, in which the n-th row lists all the preimages k, in increasing order, such that k*sigma(k) = A337873(n).",
			"comment": [
				"The application k -\u003e k*sigma(k) = m is not injective (A064987) and this sequence proposes, in increasing order of m, the preimages of the integers m that have more than one preimage.",
				"If 2^p-1 and 2^r-1 are distinct Mersenne primes (A000668), then k = (2^p-1) * 2^(r-1) and q = (2^r-1) * 2^(p-1) satisfy k*sigma(k) = q*sigma(q) = m = (2^p-1) * (2^r-1) * 2^(p+r-1) [see 2 first examples]."
			],
			"reference": [
				"Richard K. Guy, Unsolved Problems in Number Theory, 3rd Edition, Springer, 2004, Section B11, p. 101-102."
			],
			"example": [
				"The table begins:",
				"   12,  14;",
				"   48,  62;",
				"   60,  70;",
				"  112, 124;",
				"  132, 154;",
				"  160, 189;",
				"  ...",
				"1st row is (12, 14) because 12 * sigma(12) = 14 * sigma(14) = 336 = A337873(1) with p = 2 and r = 3.",
				"2nd row is (48, 62) because 48 * sigma(48) = 62 * sigma(62) = 5952 = A337873(2) with p = 2 and r = 5.",
				"16th row is (336, 372, 434) because 336 * sigma(336) = 372 * sigma(372) = 434 * sigma(434) = 333312 = A337873(16)."
			],
			"mathematica": [
				"m = 10^6; v = Table[{}, {m}]; Do[i = n*DivisorSigma[1, n]; If[i \u003c= m, AppendTo[v[[i]], n]], {n, 1, Floor@Sqrt[m]}]; Select[v, Length[#] \u003e 1 \u0026] // Flatten (* _Amiram Eldar_, Oct 06 2020 *)"
			],
			"program": [
				"(PARI)  upto(n) = {m = Map(); res = List(); n = sqrtint(n); w = []; for(i = 1, n, c = i*sigma(i); if(mapisdefined(m, c), listput(res, c); l = mapget(m, c); listput(l, i); mapput(m, c, l) , mapput(m, c, List(i)); ) ); listsort(res, 1); v = select(x -\u003e x \u003c= (n+1)^2, res); for(i = 1, #v, w = concat(w, Vec(mapget(m, v[i]))) ); w } \\\\ _David A. Corneth_, Oct 07 2020"
			],
			"xref": [
				"Cf. A000203, A064987, A327153.",
				"Cf. A337873, A337875, A337876."
			],
			"keyword": "nonn,tabf",
			"offset": "1,1",
			"author": "_Bernard Schott_, Oct 06 2020",
			"references": 4,
			"revision": 26,
			"time": "2020-10-09T03:49:52-04:00",
			"created": "2020-10-09T03:49:52-04:00"
		}
	]
}