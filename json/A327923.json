{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A327923",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 327923,
			"data": "1,1,-2,-1,10,-24,16,1,-42,504,-2640,7040,-9984,7168,-2048,-1,170,-8568,201552,-2687360,22573824,-127339520,502081536,-1417641984,2901606400,-4310958080,4600627200,-3435134976,1702887424,-503316480,67108864",
			"name": "Irregular triangle read by rows: Coefficients of Schick's polynomials P(n, y^2), for n \u003e= 1.",
			"comment": [
				"The length of row n of this irregular triangle t(n, k) is 2^(n-1) = A000079(n-1), n \u003e= 1.",
				"These polynomials P(n, y^2) = Sum_{k=0..2^(n-1)-1} t(n, k)*y^2 appear in table 2 (Tabelle 2), p. 157, of Carl Schick's book as x_n/(2^n*x*y), for n \u003e= 1, and  x_0 = x. The polynomials y_n of Tabelle 1 on p. 156 appear as y_n = -Sum_{n=0..2^(n-1)} A321369(n, k)*y^(2*k), for n \u003e= 1, with y_0 = y, and are related to the n-th iteration of the Chebyshev polynomial -T(2, y) = 1 - 2*x^2.",
				"Originally the polynomials y_n and x_n are defined in Schick's rare notebook as: y_n = 1 - 2*(y_{n-1})^2, for n \u003e= 1, with y_0 = y, and x_n/(2^n*x*y) = Product_{j=1..n-1} y_j, for n \u003e= 1, and x_0 = x. The (x_n, y_n) come from coordinates of points on the unit circle with x_n = cos(phi_n) and y_n = sin(phi_n), with some initial condition (x_0, y_0) = (x, y)."
			],
			"reference": [
				"Carl Schick, Trigonometrie und unterhaltsame Zahlentheorie, Bokos Druck, Zürich, 2003 (ISBN 3-9522917-0-6)."
			],
			"formula": [
				"Row polynomials: P(n, y^2) = Product_{j=1..n-1} y_j(y^2), for n \u003e= 1 (the empty product equals 1), where y_j(y^2) = -T^{[j]}(2, y) = -T(2^j, y), the j-th iteration of -T(2, y) = - 1 + 2*y^2, with Chebyshev's T polynomials (A053120).",
				"Row polynomials linearized in T: P(n, y^2) = (-1)^(n-1)*(1/2^(n-2)) * Sum_{m=1..2^(n-2)} T(2*(2^(n-1) + 1 - 2*m), y), for n \u003e= 2, and  P(1, y^2) = 1. See the irregular triangle A261693(n-1, m) = 2^(n-1) + 1 - 2*m, n \u003e= 2, 1 \u003c= m \u003c= 2^(n-2).",
				"Irregular triangle: t(n, k) = [y^(2*k)] P(n, y^2), n \u003e= 1, k = 0, 1, ..., 2^(n-1)-1."
			],
			"example": [
				"The irregular triangle T(n, k) begins:",
				"n\\k   0    1    2      3     4      5     6      7 ...",
				"------------------------------------------------------",
				"1:    1",
				"2:    1   -2",
				"3:   -1   10  -24     16",
				"4:    1  -42  504  -2640  7040  -9984  7168  -2048",
				"...",
				"Row n = 5: -1  170  -8568  201552  -2687360  22573824  -127339520 502081536  -1417641984  2901606400  -4310958080 4600627200  -3435134976  1702887424  -503316480  67108864.",
				"...",
				"---------------------------------------------------------------------------"
			],
			"xref": [
				"Cf. A000079, A053120, A321369."
			],
			"keyword": "sign,easy,tabf",
			"offset": "1,3",
			"author": "_Wolfdieter Lang_, Nov 19 2019",
			"references": 0,
			"revision": 12,
			"time": "2019-11-21T04:56:24-05:00",
			"created": "2019-11-19T06:47:05-05:00"
		}
	]
}