{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A049297",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 49297,
			"data": "1,2,3,6,6,20,14,46,51,140,108,624,352,1400,2172,4262,4116,22040,14602,68016,88376,209936,190746,1062592,839094,2797000,3728891,11276704,9587580,67195520,35792568",
			"name": "Number of nonisomorphic circulant digraphs (i.e., Cayley digraphs for the cyclic group) of order n.",
			"comment": [
				"Terms may be computed by filtering potentially isomorphic graphs of A056391 through nauty. Terms computed in this way for a(25), a(27) agree with theoretical calculations by others. - _Andrew Howroyd_, Apr 23 2017"
			],
			"link": [
				"V. A. Liskovets, \u003ca href=\"https://arxiv.org/abs/math/0104131\"\u003eSome identities for enumerators of circulant graphs\u003c/a\u003e, arXiv:math/0104131 [math.CO], 2001.",
				"V. A. Liskovets and R. Poeschel, \u003ca href=\"http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.84.89\"\u003eOn the enumeration of circulant graphs of prime-power and squarefree orders\u003c/a\u003e.",
				"R. Poeschel, \u003ca href=\"http://www.math.tu-dresden.de/~poeschel/Publikationen.html\"\u003ePublications\u003c/a\u003e.",
				"V. Gatt, \u003ca href=\"https://arxiv.org/abs/1703.06038\"\u003eOn the Enumeration of Circulant Graphs of Prime-Power Order: the case of p^3\u003c/a\u003e, arXiv:1703.06038 [math.CO], 2017.",
				"Victoria Gatt, Mikhail Klin, Josef Lauri, Valery Liskovets, \u003ca href=\"https://doi.org/10.1007/978-3-030-32808-5_2\"\u003eFrom Schur Rings to Constructive and Analytical Enumeration of Circulant Graphs with Prime-Cubed Number of Vertices\u003c/a\u003e, in Isomorphisms, Symmetry and Computations in Algebraic Graph Theory, (Pilsen, Czechia, WAGT 2016) Vol. 305, Springer, Cham, 37-65.",
				"Brendan McKay, \u003ca href=\"http://users.cecs.anu.edu.au/~bdm/nauty/\"\u003eNauty home page\u003c/a\u003e."
			],
			"formula": [
				"There is an easy formula for prime orders. Formulae are also known for squarefree and prime-squared orders.",
				"From _Andrew Howroyd_, Apr 23 2017: (Start)",
				"a(n) \u003c= A056391(n).",
				"a(n) = A056391(n) for squarefree n.",
				"a(A000040(n)^2) = A038777(n).",
				"(End)"
			],
			"program": [
				"(GAP)",
				"LoadPackage(\"grape\");",
				"CirculantDigraphCount:= function(n) local g; # slow for n \u003e= 10",
				"g:=Graph( Group(()), [1..n], OnPoints, function(x,y) return (y-x) mod n = 1; end,false);",
				"return Length(GraphIsomorphismClassRepresentatives(List(Combinations([1..n]), s-\u003eDistanceGraph(g,s))));",
				"end; # _Andrew Howroyd_, Apr 23 2017"
			],
			"xref": [
				"Cf. A049287-A049289, A056391, A038777."
			],
			"keyword": "nonn,nice",
			"offset": "1,2",
			"author": "_Valery A. Liskovets_",
			"ext": [
				"Further values for (twice) squarefree and (twice) prime-squared orders can be found in the Liskovets reference.",
				"a(14) corrected by _Andrew Howroyd_, Apr 23 2017",
				"a(16)-a(31) from _Andrew Howroyd_, Apr 23 2017"
			],
			"references": 12,
			"revision": 24,
			"time": "2020-04-02T19:30:00-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}