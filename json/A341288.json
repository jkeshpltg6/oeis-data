{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A341288",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 341288,
			"data": "0,0,0,0,1,0,0,2,2,0,0,3,1,3,0,0,4,3,3,4,0,0,5,8,0,8,5,0,0,6,10,12,12,10,6,0,0,7,9,15,1,15,9,7,0,0,8,11,15,5,5,15,11,8,0,0,9,4,12,9,0,9,12,4,9,0,0,10,6,12,13,15,15,13,12,6,10,0",
			"name": "Square array T(n, k), read by antidiagonals, n, k \u003e= 0; T(n, k) = XOR_{u in B(n), v in B(k)) 2^(u XOR v) where XOR denotes the bitwise XOR operator and B(n) gives the exponents in expression for n as a sum of powers of 2.",
			"comment": [
				"For any x \u003e= 0, the function n -\u003e T(n, 2^x) is a self-inverse permutation of the nonnegative integers.",
				"The set of nonnegative integers equipped with T forms a commutative monoid; its invertible elements are the odious numbers (A000069).",
				"Hence A000069 equipped with T forms a group."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A341288/b341288.txt\"\u003eTable of n, a(n) for n = 0..10010\u003c/a\u003e (rows 0..140)",
				"Rémy Sigrist, \u003ca href=\"/A341288/a341288.png\"\u003eColored representation of the table for n, k \u003c 2^10\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A341288/a341288_1.png\"\u003eColored representation of the table over the first 128 odious numbers\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A341288/a341288_2.png\"\u003eColored representation of the table over the first 1024 evil numbers\u003c/a\u003e (white pixels correspond to 0's)"
			],
			"formula": [
				"T(n, k) = T(k, n) (T is commutative).",
				"T(m, T(n, k)) = T(T(m, n), k) (T is associative).",
				"T(n, 0) = 0 (0 is an absorbing element for T).",
				"T(n, 1) = n (1 is the neutral element for T).",
				"T(n, 2) = A057300(n).",
				"T(n, 4) = A126006(n).",
				"T(n, n) = A010060(n).",
				"A010060(T(n, k)) = A010060(n) * A010060(k)."
			],
			"example": [
				"Array T(n, k) begins:",
				"  n\\k|  0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15",
				"  ---+---------------------------------------------------------------",
				"    0|  0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0",
				"    1|  0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15",
				"    2|  0   2   1   3   8  10   9  11   4   6   5   7  12  14  13  15 -\u003e A057300",
				"    3|  0   3   3   0  12  15  15  12  12  15  15  12   0   3   3   0",
				"    4|  0   4   8  12   1   5   9  13   2   6  10  14   3   7  11  15 -\u003e A126006",
				"    5|  0   5  10  15   5   0  15  10  10  15   0   5  15  10   5   0",
				"    6|  0   6   9  15   9  15   0   6   6   0  15   9  15   9   6   0",
				"    7|  0   7  11  12  13  10   6   1  14   9   5   2   3   4   8  15",
				"    8|  0   8   4  12   2  10   6  14   1   9   5  13   3  11   7  15",
				"    9|  0   9   6  15   6  15   0   9   9   0  15   6  15   6   9   0",
				"   10|  0  10   5  15  10   0  15   5   5  15   0  10  15   5  10   0",
				"   11|  0  11   7  12  14   5   9   2  13   6  10   1   3   8   4  15",
				"   12|  0  12  12   0   3  15  15   3   3  15  15   3   0  12  12   0",
				"   13|  0  13  14   3   7  10   9   4  11   6   5   8  12   1   2  15",
				"   14|  0  14  13   3  11   5   6   8   7   9  10   4  12   2   1  15",
				"   15|  0  15  15   0  15   0   0  15  15   0   0  15   0  15  15   0",
				"                                                                     \\",
				"                                                                      v",
				"                                                                    A010060"
			],
			"program": [
				"(PARI) B(n) = { my (b=vector(hammingweight(n))); for (k=1, #b, n -= 2^(b[k] = valuation(n, 2))); b }",
				"T(n,k) = { my (nn=B(n), kk=B(k), v=0); for (i=1, #nn, for (j=1, #kk, v=bitxor(v, 2^bitxor(nn[i], kk[j])))); v }"
			],
			"xref": [
				"Cf. A000069, A010060, A057300, A126006, A133457."
			],
			"keyword": "nonn,tabl,look,base",
			"offset": "0,8",
			"author": "_Rémy Sigrist_, Feb 08 2021",
			"references": 2,
			"revision": 16,
			"time": "2021-02-12T16:50:53-05:00",
			"created": "2021-02-10T13:53:18-05:00"
		}
	]
}