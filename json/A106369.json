{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A106369",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 106369,
			"data": "1,1,2,2,3,6,7,11,18,29,42,73,111,183,299,491,796,1333,2188,3652,6073,10155,16959,28500,47813,80508,135621,228967,386749,654535,1108353,1879478,3189495,5418556,9212099,15676275,26694509,45493327,77580915",
			"name": "Number of circular compositions of n such that no two adjacent parts are equal.",
			"comment": [
				"By \"circular compositions\" here we mean equivalence classes of compositions with parts on a circle such that two compositions are equivalent if one is a cyclic shift of the other. - _Petros Hadjicostas_, Oct 15 2017"
			],
			"link": [
				"Vaclav Kotesovec, \u003ca href=\"/A106369/b106369.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"P. Hadjicostas, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL20/Hadjicostas/hadji5.html\"\u003eCyclic, dihedral and symmetric Carlitz compositions of a positive integer\u003c/a\u003e, Journal of Integer Sequences, 20 (2017), Article 17.8.5.",
				"Petros Hadjicostas, \u003ca href=\"/A106369/a106369.pdf\"\u003eProof of an explicit formula for Bower's CycleBG transform\u003c/a\u003e",
				"\u003ca href=\"/index/Ne#necklaces\"\u003eIndex entries for sequences related to necklaces\u003c/a\u003e"
			],
			"formula": [
				"CycleBG transform of (1, 1, 1, 1, ...).",
				"CycleBG transform T(A) = invMOEBIUS(invEULER(Carlitz(A)) + A(x^2) - A) + A.",
				"Carlitz transform T(A(x)) has g.f. 1/(1 - Sum_{k\u003e0} (-1)^(k+1)*A(x^k)).",
				"G.f.: x/(1-x) - Sum_{s\u003e=1} (phi(s)/s)*f(x^s), where f(x) = log(1 - Sum_{n\u003e=1} x^n/(1 + x^n)) + Sum_{n\u003e=1} log(1 + x^n) and phi(s)=A000010 is Euler's totient function. - _Petros Hadjicostas_, Sep 06 2017",
				"Conjecture: a(n) ~ A241902^n / n. - _Vaclav Kotesovec_, Sep 06 2017",
				"General formula for the CycleBG transform: T(A)(x) = A(x) - Sum_{k\u003e=0} A(x^(2k+1)) + Sum_{k\u003e=1} (phi(k)/k)*log(Carlitz(A)(x^k)). For a proof, see the links above. (For this sequence, A(x) = x/(1-x).) - _Petros Hadjicostas_, Oct 08 2017",
				"G.f.: -Sum_{s\u003e=1} x^(2s+1)/(1-x^(2s+1)) - Sum_{s\u003e=1} (phi(s)/s)*g(x^s), where g(x) = log(1 + Sum_{n\u003e=1} (-x)^n/(1 - x^n)). (This formula can be proved from the general formula for the CycleBG transform given above.) - _Petros Hadjicostas_, Oct 10 2017"
			],
			"example": [
				"a(6) = 6 because the 6 circular compositions of 6: 6, 5+1, 4+2, 3+2+1, 3+1+2, 2+1+2+1."
			],
			"mathematica": [
				"nmax = 40; Rest[CoefficientList[Series[x/(1-x) - Sum[EulerPhi[s]/s*(Log[1 - Sum[x^(s*n)/(1 + x^(s*n)), {n, 1, nmax}]] + Sum[Log[1 + x^(s*n)], {n, 1, nmax}]), {s, 1, nmax}], {x, 0, nmax}], x]] (* _Vaclav Kotesovec_, Sep 06 2017, after _Petros Hadjicostas_ *)"
			],
			"xref": [
				"Cf. A000031, A008965, A212322, A292906."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Christian G. Bower_, Apr 29 2005",
			"ext": [
				"Name clarified by _Andrew Howroyd_, Oct 12 2017"
			],
			"references": 6,
			"revision": 53,
			"time": "2017-10-26T09:28:51-04:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}