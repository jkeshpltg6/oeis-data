{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003842",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3842,
			"data": "1,2,1,1,2,1,2,1,1,2,1,1,2,1,2,1,1,2,1,2,1,1,2,1,1,2,1,2,1,1,2,1,1,2,1,2,1,1,2,1,2,1,1,2,1,1,2,1,2,1,1,2,1,2,1,1,2,1,1,2,1,2,1,1,2,1,1,2,1,2,1,1,2,1,2,1,1,2,1,1,2,1,2,1,1,2,1,1,2,1,2,1,1,2,1,2,1,1,2,1,1,2,1,2,1",
			"name": "The infinite Fibonacci word: start with 1, repeatedly apply the morphism 1-\u003e12, 2-\u003e1, take limit; or, start with S(0)=2, S(1)=1, and for n\u003e1 define S(n)=S(n-1)S(n-2), then the sequence is S(oo).",
			"comment": [
				"Or, fixed point of the morphism 1-\u003e12, 2-\u003e1, starting from a(1) = 2.",
				"A Sturmian word, as are all versions of this sequence. This means that if one slides a window of length n along the sequence, one sees exactly n+1 different subwords (see A213975). For a proof, see for example Chap. 2 of Lothaire (2002).",
				"The limiting mean of the first n terms is 3 - phi, where phi is the golden ratio (A001622); the limiting variance is 2 - phi. - _Clark Kimberling_, Mar 12 2014",
				"The Wikipedia article on L-system Example 1 is \"Algae\" given by the axiom: A and rules: A -\u003e AB, B -\u003e A. The sequence G(n) = G(n-1)G(n-2) yields this sequence when A -\u003e 1, B -\u003e 2. - _Michael Somos_, Jan 12 2015",
				"In the limit #1's : #2's = phi : 1. - _Frank M Jackson_, Mar 12 2018"
			],
			"reference": [
				"J.-P. Allouche and J. Shallit, Automatic Sequences, Cambridge Univ. Press, 2003.",
				"Berstel, Jean. \"Fibonacci words—a survey.\" In The book of L, pp. 13-27. Springer Berlin Heidelberg, 1986.",
				"J. Berstel and J. Karhumaki, Combinatorics on words - a tutorial, Bull. EATCS, #79 (2003), pp. 178-228.",
				"E. Bombieri and J. Taylor, Which distribution of matter diffracts? An initial investigation, in International Workshop on Aperiodic Crystals (Les Houches, 1986), J. de Physique, Colloq. C3, 47 (1986), C3-19 to C3-28.",
				"de Luca, Aldo; Varricchio, Stefano. Finiteness and regularity in semigroups and formal languages. Monographs in Theoretical Computer Science. An EATCS Series. Springer-Verlag, Berlin, 1999. x+240 pp. ISBN: 3-540-63771-0 MR1696498 (2000g:68001). See p. 25.",
				"J. C. Lagarias, Number Theory and Dynamical Systems, pp. 35-72 of S. A. Burr, ed., The Unreasonable Effectiveness of Number Theory, Proc. Sympos. Appl. Math., 46 (1992). Amer. Math. Soc. - see p. 64.",
				"G. Melancon, Factorizing infinite words using Maple, MapleTech journal, vol. 4, no. 1, 1997, pp. 34-42, esp. p. 36."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A003842/b003842.txt\"\u003eTable of n, a(n) for n=0..10945\u003c/a\u003e (20 iterations)",
				"J.-P. Allouche and M. Mendes France, \u003ca href=\"https://webusers.imj-prg.fr/~jean-paul.allouche/allmendeshouches.pdf\"\u003eAutomata and Automatic Sequences\u003c/a\u003e, in: Axel F. and Gratias D. (eds), Beyond Quasicrystals. Centre de Physique des Houches, vol 3. Springer, Berlin, Heidelberg, pp. 293-367, 1995; DOI https://doi.org/10.1007/978-3-662-03130-8_11.",
				"J.-P. Allouche and M. Mendes France, \u003ca href=\"/A003842/a003842.pdf\"\u003eAutomata and Automatic Sequences\u003c/a\u003e, in: Axel F. and Gratias D. (eds), Beyond Quasicrystals. Centre de Physique des Houches, vol 3. Springer, Berlin, Heidelberg, pp. 293-367, 1995; DOI https://doi.org/10.1007/978-3-662-03130-8_11. [Local copy]",
				"Scott Balchin and Dan Rust, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL20/Rust/rust3.html\"\u003eComputations for Symbolic Substitutions\u003c/a\u003e, Journal of Integer Sequences, Vol. 20 (2017), Article 17.4.1.",
				"Jean Berstel, \u003ca href=\"http://www-igm.univ-mlv.fr/~berstel/\"\u003eHome Page\u003c/a\u003e",
				"Julien Cassaigne, \u003ca href=\"http://www.numdam.org/item?id=ITA_2008__42_4_701_0\"\u003eOn extremal properties of the Fibonacci word\u003c/a\u003e, RAIRO-Theor. Inf. Appl. 42 (2008) 701-715.",
				"J. Endrullis, D. Hendriks and J. W. Klop, \u003ca href=\"http://www.cs.vu.nl/~diem/publication/pdf/degrees.pdf\"\u003eDegrees of streams\u003c/a\u003e.",
				"S. Ferenczi, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(98)00400-2\"\u003eComplexity of sequences and dynamical systems\u003c/a\u003e, Discrete Math., 206 (1999), 145-154.",
				"J. Grytczuk, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(95)00297-A\"\u003eInfinite semi-similar words\u003c/a\u003e, Discrete Math. 161 (1996), 133-141.",
				"A. Hof, O. Knill and B. Simon, \u003ca href=\"http://projecteuclid.org/euclid.cmp/1104275098\"\u003eSingular continuous spectrum for palindromic Schrödinger operators\u003c/a\u003e, Commun. Math. Phys. 174 (1995), 149-159.",
				"C. Kimberling, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL3/goldentext.html\"\u003eA Self-Generating Set and the Golden Mean\u003c/a\u003e, J. Integer Sequences, 3 (2000), #00.2.8.",
				"Clark Kimberling, \u003ca href=\"https://doi.org/10.4171/EM/468\"\u003eIntriguing infinite words composed of zeros and ones\u003c/a\u003e, Elemente der Mathematik (2021).",
				"M. Lothaire, \u003ca href=\"http://www-igm.univ-mlv.fr/~berstel/Lothaire/\"\u003eAlgebraic Combinatorics on Words\u003c/a\u003e, Cambridge, 2002, see p. 41, etc.",
				"G. Melancon, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(99)00123-5\"\u003eLyndon factorization of sturmian words\u003c/a\u003e, Discr. Math., 210 (2000), 137-149.",
				"F. Mignosi, A. Restivo and M. Sciortino, \u003ca href=\"http://dx.doi.org/10.1016/S0304-3975(00)00436-9\"\u003eWords and forbidden factors\u003c/a\u003e, WORDS (Rouen, 1999). Theoret. Comput. Sci. 273 (2002), no. 1-2, 99--117. MR1872445 (2002m:68096) - From _N. J. A. Sloane_, Jul 10 2012",
				"F. Mignosi and L. Q. Zamboni, \u003ca href=\"http://dx.doi.org/10.4064/aa101-2-4\"\u003eOn the number of Arnoux-Rauzy words\u003c/a\u003e, Acta arith., 101 (2002), no. 2, 121-129.",
				"T. D. Noe, \u003ca href=\"/A003849/a003849.txt\"\u003eThe first 1652 subwords of A003849, including leading zeros.\u003c/a\u003e",
				"Giuseppe Pirillo, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(94)00236-C\"\u003eFibonacci numbers and words\u003c/a\u003e, Discrete Math. 173 (1997), no. 1-3, 197--207. MR1468849 (98g:68135).",
				"Patrice Séébold, \u003ca href=\"http://www.numdam.org/item?id=ITA_2008__42_4_729_0\"\u003eLook and Say Fibonacci\u003c/a\u003e, RAIRO-Theor. Inf. Appl. 42 (2008) 729-746.",
				"N. J. A. Sloane, \u003ca href=\"/A003842/a003842.txt\"\u003eThe first 10946 terms, concatenated\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/A115004/a115004.txt\"\u003eFamilies of Essentially Identical Sequences\u003c/a\u003e, Mar 24 2021 (Includes this sequence)",
				"P. Steinbach, \u003ca href=\"http://www.jstor.org/stable/2691048\"\u003eGolden fields: a case for the heptagon\u003c/a\u003e, Math. Mag. 70 (1997), no. 1, 22-31.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GoldenRatio.html\"\u003eGolden Ratio\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/L-system#Example_1:_Algae\"\u003eL-system\u003c/a\u003e Example 1: Algae",
				"\u003ca href=\"/index/Fi#FIXEDPOINTS\"\u003eIndex entries for sequences that are fixed points of mappings\u003c/a\u003e"
			],
			"formula": [
				"Define strings S(0)=2, S(1)=1, S(n)=S(n-1)S(n-2); iterate. Sequence is S(infinity).",
				"a(n) = n + 2 - A120613(n+1). - _Benoit Cloitre_, Jul 28 2005 [Corrected by _N. J. A. Sloane_, Jun 30 2018]"
			],
			"example": [
				"Over the alphabet {a,b} this is the sequence a, b, a, a, b, a, b, a, a, b, a, a, b, a, b, a, a, b, a, b, a, a, b, a, a, b, a, b, a, a, b, a, a, b, a, b, a, a, b, a, b, a, a, b, a, a, b, a, b, a, a, b, a, b, a, a, b, a, a, b, a, b, a, a, b, a, a, b, a, b, a, a, b, a, b, a, a, b, a, a, b, a, b, a, a, b, a, a, b, a, b, a, a, b, a, b, a, a, b, a, a, b, a, b, a, ..."
			],
			"mathematica": [
				"Nest[ Flatten[ # /. {1 -\u003e {1, 2}, 2 -\u003e {1}}] \u0026, {1}, 10] (* _Robert G. Wilson v_, Mar 04 2005 *)",
				"Table[n + 1 - Floor[((1 + Sqrt[5])/2)*Floor[2*(n + 1)/(1 + Sqrt[5])]], {n, 1, 50}] (* _G. C. Greubel_, May 18 2017 *)"
			],
			"program": [
				"(Haskell)",
				"a003842 n = a003842_list !! n",
				"a003842_list = tail $ concat fws where",
				"   fws = [2] : [1] : (zipWith (++) fws $ tail fws)",
				"-- _Reinhard Zumkeller_, Oct 26 2013",
				"(PARI) for(n=1,50, print1(n+1 - floor(((1+sqrt(5))/2)*floor(2*(n+1)/(1+sqrt(5)))), \", \")) \\\\ _G. C. Greubel_, May 18 2017"
			],
			"xref": [
				"A003849 is another common version of this sequence.",
				"See also A014675, A005614, A001468, A106750, A213975, A213976, A214317, A214318, A182028, A120613.",
				"The following sequences are all essentially the same, in the sense that they are simple transformations of each other, with A000201 as the parent: A000201, A001030, A001468, A001950, A003622, A003842, A003849, A004641, A005614, A014675, A022342, A088462, A096270, A114986, A124841. - _N. J. A. Sloane_, Mar 11 2021"
			],
			"keyword": "nonn,nice,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Entry revised by _N. J. A. Sloane_, Jul 03 2012"
			],
			"references": 50,
			"revision": 126,
			"time": "2021-12-17T01:06:51-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}