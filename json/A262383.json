{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A262383",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 262383,
			"data": "12,720,15120,11200,332640,908107200,4324320,2940537600,175991175360,512143632000,1427794368,7795757249280,107084577600,279490747536000,200143324310529600,1178332991611776000,157531148611200,906996615309386784000,5828652498614400,262872227687509440000",
			"name": "Denominators of a semi-convergent series leading to the first Stieltjes constant gamma_1.",
			"comment": [
				"gamma_1 = - 1/12 + 11/720 - 137/15120 + 121/11200 - 7129/332640 + 57844301/908107200 - ..., see formulas (46)-(47) in the reference below."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A262383/b262383.txt\"\u003eTable of n, a(n) for n = 1..500\u003c/a\u003e",
				"Iaroslav V. Blagouchine, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2015.06.012\"\u003eExpansions of generalized Euler's constants into the series of polynomials in 1/pi^2 and into the formal enveloping series with rational coefficients only.\u003c/a\u003e Journal of Number Theory (Elsevier), vol. 158, pp. 365-396, 2016. \u003ca href=\"http://arxiv.org/abs/1501.00740\"\u003earXiv version\u003c/a\u003e, arXiv:1501.00740 [math.NT], 2015."
			],
			"formula": [
				"a(n) = denominator(-B_{2n}*H_{2n-1}/(2n)), where B_n and H_n are Bernoulli and harmonic numbers respectively.",
				"a(n) = denominator(Zeta(1 - 2*n)*(Psi(2*n) + gamma)), where gamma is Euler's gamma. - _Peter Luschny_, Apr 19 2018"
			],
			"example": [
				"Denominators of -1/12, 11/720, -137/15120, 121/11200, -7129/332640, 57844301/908107200, ..."
			],
			"maple": [
				"a := n -\u003e denom(Zeta(1 - 2*n)*(Psi(2*n) + gamma)):",
				"seq(a(n), n=1..20); # _Peter Luschny_, Apr 19 2018"
			],
			"mathematica": [
				"a[n_] := Denominator[-BernoulliB[2*n]*HarmonicNumber[2*n - 1]/(2*n)]; Table[a[n], {n, 1, 20}]"
			],
			"program": [
				"(PARI) a(n) = denominator(-bernfrac(2*n)*sum(k=1,2*n-1,1/k)/(2*n)); \\\\ _Michel Marcus_, Sep 23 2015"
			],
			"xref": [
				"Cf. A001620, A002206, A195189, A075266, A262235, A001067, A006953, A082633, A262382 (numerators of this series), A086279, A086280, A262387."
			],
			"keyword": "nonn,frac",
			"offset": "1,1",
			"author": "_Iaroslav V. Blagouchine_, Sep 20 2015",
			"references": 8,
			"revision": 27,
			"time": "2018-08-29T00:07:22-04:00",
			"created": "2015-10-09T18:17:56-04:00"
		}
	]
}