{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A326925",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 326925,
			"data": "1,-1,0,2,-1,1,0,3,-1,1,4,0,4,-1,2,5,10,0,5,-1,3,12,15,20,0,6,-1,5,21,42,35,35,0,7,-1,8,40,84,112,70,56,0,8,-1,13,72,180,252,252,126,84,0,9,-1,21,130,360,600,630,504,210,120,0,10,-1,34,231,715",
			"name": "Irregular triangular array read by rows: row n shows the coefficients of this polynomial of degree n: (1/n!)*(numerator of n-th derivative of (1-x)/(1-x-x^2)).",
			"comment": [
				"Column 1: Fibonacci numbers, F(m), for m \u003e= -1, as in A000045. For n \u003e= 0, the n-th row sum = F(2n), as in A001906.",
				"Conjecture: The odd degree polynomials are irreducible; the even degree (= 2k) polynomials have exactly two irreducible factors, each of degree k."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A326925/b326925.txt\"\u003eTable of n, a(n) for n = 1..10010\u003c/a\u003e"
			],
			"formula": [
				"G.f. as array: ((y^2 + y - 1)*x - y + 1)/(1 + (y^2 + y - 1)*x^2 + (-2*y - 1)*x). - _Robert Israel_, Oct 31 2019"
			],
			"example": [
				"First 7 rows:",
				"1    -1",
				"0     2   -1",
				"1     0    3   -1",
				"1     4    0    4   -1",
				"2     5    0   10    5   -1",
				"3    12   15   20    0    6   -1",
				"5    21   42   35   35    0    7   -1",
				"First 7 polynomials:",
				"1 - x",
				"2 x - x^2",
				"1 + 3 x^2 - x^3",
				"1 + 4 x + 4 x^3 - x^4",
				"2 + 5 x + 10 x^2 + 5 x^4 - x^5",
				"3 + 12 x + 15 x^2 + 20 x^3 + 6 x^5 - x^6",
				"5 + 21 x + 42 x^2 + 35 x^3 + 35 x^4 + 7 x^6 - x^7",
				"Factorizations of even-degree polynomials:",
				"degree 2:  (2 - x)*x",
				"degree 4:  (1 + x^2)*(1 + 4x - x^2)",
				"degree 6:  (1 + 3x + x^3)*(3 + 3x + 6x^2 - x^3)",
				"degree 8:  (2 + 4x + 6x^2 + x^4)*(4 + 12 x + 6x^2 + 8x^3 - x^4)",
				"degree 10: (3 + 10 x + 10 x^2 + 10 x^3 + x^5)*(7 + 20 x + 30 x^2 + 10 x^3 + 10 x^4 - x^5)",
				"It appears that the constant terms of the factors are Fibonacci numbers (A000045) and Lucas numbers (A000032)."
			],
			"mathematica": [
				"g[x_, n_] := Numerator[(-1)^(n + 1) Factor[D[(1 - x)/(1 - x - x^2), {x, n}]]]",
				"Column[Expand[Table[g[x, n]/n!, {n, 0, 12}]]] (* polynomials *)",
				"h[n_] := CoefficientList[g[x, n]/n!, x] (* A326925 *)",
				"Table[h[n], {n, 0, 10}]",
				"Column[%]"
			],
			"xref": [
				"Cf. A000045, A001906, A094440, A094441, A328610, A328611."
			],
			"keyword": "tabf,sign",
			"offset": "1,4",
			"author": "_Clark Kimberling_, Oct 22 2019",
			"references": 6,
			"revision": 13,
			"time": "2019-10-31T21:41:49-04:00",
			"created": "2019-10-31T21:41:49-04:00"
		}
	]
}