{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A322504",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 322504,
			"data": "1,-2,4,-9,22,-57,153,-419,1160,-3230,9021,-25234,70643,-197849,554233,-1552742,4350420,-12189221,34152882,-95693445,268125913,-751270435,2105010556,-5898105006,16526117921,-46305146110,129744125671,-363534946433,1018602262609,-2854060085466,7996898607604",
			"name": "a(n) = -4*a(n-1) - 3*a(n-2) + a(n-3), a(0) = 1, a(1) = -2, a(2) = 4.",
			"comment": [
				"Let {X,Y,Z} be the roots of the cubic equation t^3 + at^2 + bt + c = 0 where {a, b, c} are integers.",
				"Let {u, v, w} be three numbers such that {u + v + w, u*X + v*Y + w*Z, u*X^2 + v*Y^2 + w*Z^2} are integers.",
				"Then {p(n) = u*X^n + v*Y^n + w*Z^n | n = 0, 1, 2, ...} is an integer sequence with the recurrence relation: p(n) = -a*p(n-1) - b*p(n-2) - c*p(n-3).",
				"Let k = Pi/7.",
				"This sequence has (a, b, c) = (4, 3, -1), (u, v, w) = (1/(sqrt(7)*tan(8k)), 1/(sqrt(7)*tan(2k)), 1/(sqrt(7)*tan(4k))).",
				"A215404: (a, b, c) = (4, 3, -1), (u, v, w) = (1/(sqrt(7)*tan(2k)), 1/(sqrt(7)*tan(4k)), 1/(sqrt(7)*tan(8k))).",
				"A136776: (a, b, c) = (4, 3, -1), (u, v, w) = (1/(sqrt(7)*tan(4k)), 1/(sqrt(7)*tan(8k)), 1/(sqrt(7)*tan(2k))).",
				"X = (sin(2k)*sin(2k))/(sin(4k)*sin(8k)),  Y = (sin(4k)*sin(4k))/(sin(8k)*sin(2k)), Z = (sin(8k)*sin(8k))/(sin(2k)*sin(4k))."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A322504/b322504.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (-4,-3,1)."
			],
			"formula": [
				"G.f.: (1 + 2*x - x^2) / (1 + 4*x + 3*x^2 - x^3). - _Colin Barker_, Jan 11 2019"
			],
			"mathematica": [
				"LinearRecurrence[{-4,-3,1},{1,-2,4},50] (* _Stefano Spezia_, Jan 11 2019 *)",
				"RecurrenceTable[{a[0]==1, a[1]==-2, a[2]==4, a[n]==-4 a[n-1]-3 a[n-2]+a[n-3]}, a, {n, 30}] (* _Vincenzo Librandi_, Jan 13 2019 *)"
			],
			"program": [
				"(PARI) Vec((1 + 2*x - x^2) / (1 + 4*x + 3*x^2 - x^3) + O(x^30)) \\\\ _Colin Barker_, Jan 11 2019",
				"(MAGMA) I:=[1,-2,4]; [n le 3 select I[n] else -4*Self(n-1) - 3*Self(n-2) + Self(n-3): n in [1..31]]; // _Vincenzo Librandi_, Jan 13 2019"
			],
			"xref": [
				"Cf. A215404, A136776."
			],
			"keyword": "sign,easy",
			"offset": "0,2",
			"author": "_Kai Wang_, Jan 10 2019",
			"references": 1,
			"revision": 38,
			"time": "2019-02-19T09:20:16-05:00",
			"created": "2019-02-17T09:45:57-05:00"
		}
	]
}