{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A075504",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 75504,
			"data": "1,9,1,81,27,1,729,567,54,1,6561,10935,2025,90,1,59049,203391,65610,5265,135,1,531441,3720087,1974861,255150,11340,189,1,4782969,67493007,57041334,11160261,765450,21546,252,1",
			"name": "Stirling2 triangle with scaled diagonals (powers of 9).",
			"comment": [
				"This is a lower triangular infinite matrix of the Jabotinsky type. See the Knuth reference given in A039692 for exponential convolution arrays.",
				"The row polynomials p(n,x) := Sum_{m=1..n} a(n,m)x^m, n \u003e= 1, have e.g.f. J(x; z)= exp((exp(9*z) - 1)*x/9) - 1.",
				"Row sums give A075508(n), n \u003e= 1. The columns (without leading zeros) give A001019 (powers of 9), A076008-A076013 for m=1..7."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A075504/b075504.txt\"\u003eTable of n, a(n) for n = 1..1275\u003c/a\u003e"
			],
			"formula": [
				"a(n, m) = (9^(n-m)) * stirling2(n, m).",
				"a(n, m) = Sum_{p=0..m-1} (A075513(m, p)*((p+1)*9)^(n-m))/(m-1)! for n \u003e= m \u003e= 1, else 0.",
				"a(n, m) = 9m*a(n-1, m) + a(n-1, m-1), n \u003e= m \u003e= 1, else 0, with a(n, 0) := 0 and a(1, 1)=1.",
				"G.f. for m-th column: (x^m)/Product_{k=1..m}(1-9k*x), m \u003e= 1.",
				"E.g.f. for m-th column: (((exp(9x) - 1)/9)^m)/m!, m \u003e= 1."
			],
			"example": [
				"[1]; [9,1]; [81,27,1]; ...; p(3,x) = x(81 + 27*x + x^2).",
				"From _Andrew Howroyd_, Mar 25 2017: (Start)",
				"Triangle starts",
				"*       1",
				"*       9        1",
				"*      81       27        1",
				"*     729      567       54        1",
				"*    6561    10935     2025       90      1",
				"*   59049   203391    65610     5265    135     1",
				"*  531441  3720087  1974861   255150  11340   189   1",
				"* 4782969 67493007 57041334 11160261 765450 21546 252 1",
				"(End)"
			],
			"mathematica": [
				"Flatten[Table[9^(n - m) StirlingS2[n, m], {n, 11}, {m, n}]] (* _Indranil Ghosh_, Mar 25 2017 *)"
			],
			"program": [
				"(PARI) for(n=1, 11, for(m=1, n, print1(9^(n - m) * stirling(n, m, 2),\", \");); print();) \\\\ _Indranil Ghosh_, Mar 25 2017"
			],
			"xref": [
				"Cf. A075503, A075505.",
				"Columns 2-7 are A076008-A076013."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "1,2",
			"author": "_Wolfdieter Lang_, Oct 02 2002",
			"references": 10,
			"revision": 17,
			"time": "2017-03-25T20:47:30-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}