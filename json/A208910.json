{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A208910",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 208910,
			"data": "1,1,3,1,3,8,1,3,10,22,1,3,12,32,60,1,3,14,42,100,164,1,3,16,52,144,308,448,1,3,18,62,192,480,936,1224,1,3,20,72,244,680,1568,2816,3344,1,3,22,82,300,908,2352,5040,8400,9136,1,3,24,92,360,1164,3296",
			"name": "Triangle of coefficients of polynomials v(n,x) jointly generated with A208755; see the Formula section.",
			"comment": [
				"For a discussion and guide to related arrays, see A208510.",
				"Subtriangle of the triangle given by (1, 0, -1, 1, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (0, 3, -1/3, -2/3, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Apr 01 2012"
			],
			"formula": [
				"u(n,x) = u(n-1,x) + 2x*v(n-1,x),",
				"v(n,x) = x*u(n-1,x) + 2x*v(n-1,x)+1,",
				"where u(1,x)=1, v(1,x)=1.",
				"From _Philippe Deléham_, Apr 01 2012: (Start)",
				"As DELTA-triangle T(n,k) with 0 \u003c= k \u003c= n:",
				"G.f.: (1 - 2*y*x + 3*y*x^2 - 2*y^2*x^2)/(1 - x - 2*y*x + 2*y*x^2 - 2*y^2*x^2).",
				"T(n,k) = T(n-1,k) + 2*T(n-1,k-1) - 2*T(n-2,k-1) + 2*T(n-2,k-2), T(0,0) = T(1,0) = T(2,0) = 1, T(2,1) = 3, T(1,1) = T(2,2) = 0 and T(n,k) = 0 if k \u003c 0 or if k \u003e n. (End)",
				"T(n,k) = 2*A208757(n,k) - A208332(n,k). - _Philippe Deléham_, Apr 15 2012"
			],
			"example": [
				"First five rows:",
				"  1;",
				"  1,  3;",
				"  1,  3,  8;",
				"  1,  3, 10, 22;",
				"  1,  3, 12, 32, 60;",
				"First five polynomials v(n,x):",
				"  1",
				"  1 + 3x",
				"  1 + 3x +  8x^2",
				"  1 + 3x + 10x^2 + 22x^3",
				"  1 + 3x + 12x^2 + 32x^3 + 60x^4",
				"From _Philippe Deléham_, Apr 01 2012: (Start)",
				"(1, 0, -1, 1, 0, 0, ...) DELTA (0, 3, -1/3, -2/3, 0, 0, ...) begins:",
				"  1;",
				"  1,   0;",
				"  1,   3,   0;",
				"  1,   3,   8,   0;",
				"  1,   3,  10,  22,   0;",
				"  1,   3,  12,  32,  60,   0;",
				"  1,   3,  14,  42, 100, 164,   0; (End)"
			],
			"mathematica": [
				"u[1, x_] := 1; v[1, x_] := 1; z = 16;",
				"u[n_, x_] := u[n - 1, x] + 2 x*v[n - 1, x];",
				"v[n_, x_] := x*u[n - 1, x] + 2 x*v[n - 1, x] + 1;",
				"Table[Expand[u[n, x]], {n, 1, z/2}]",
				"Table[Expand[v[n, x]], {n, 1, z/2}]",
				"cu = Table[CoefficientList[u[n, x], x], {n, 1, z}];",
				"TableForm[cu]",
				"Flatten[%]  (* A208755 *)",
				"Table[Expand[v[n, x]], {n, 1, z}]",
				"cv = Table[CoefficientList[v[n, x], x], {n, 1, z}];",
				"TableForm[cv]",
				"Flatten[%]  (* A208910 *)"
			],
			"xref": [
				"Cf. A208755, A208510."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Clark Kimberling_, Mar 03 2012",
			"references": 2,
			"revision": 16,
			"time": "2020-01-26T01:08:22-05:00",
			"created": "2012-03-03T19:24:28-05:00"
		}
	]
}