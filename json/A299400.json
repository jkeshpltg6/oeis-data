{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A299400",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 299400,
			"data": "0,11,21,12,31,1121,41,13,22,1131,51,1221,61,1141,2131,14,71,1122,81,1231,2141,1151,91,1321,32,1161,23,1241,101,112131,111,15,2151,1171,3141,1222,121,1181,2161,1331,131,112141,141,1251,2231,1191,151,1421,42,1132,2171",
			"name": "a(n) = concatenation of all (i, e_i) with e_i \u003e 0, when n = Product_{i \u003e= 1} prime(i)^e_i.",
			"comment": [
				"The conventional a(1) = 0 represents the empty concatenation.",
				"Due to simple concatenation, this encoding of the positive integers becomes ambiguous from n = 613 = prime(112)^1 on, which has the same encoding a(n) = 1121 as 6 = prime(1)^1*prime(2)^1. To get a unique encoding, one could use, e.g., the digit 9 as delimiter to separate indices and exponents, written in base 9 as to use only digits 0..8, as soon as a term would be the duplicate of an earlier term (or for all n \u003e= 613). Then one would have, e.g., a(613) = prime(134_9)^1 = 13491.",
				"Sequence A067599 is based on the same idea, but uses the primes instead of their indices. In A037276 the prime factors are repeated, instead of giving the exponent. In A080670 exponents 1 are omitted. In A124010 only the prime signature is given. In A054841 the sum e_i*10^(i-1) is given, i.e., exponents are used as digits in base 10, while they are listed individually in the rows of A067255."
			],
			"example": [
				"2 = prime(1)^1 =\u003e a(2) = 11,",
				"3 = prime(2)^1 =\u003e a(3) = 21,",
				"4 = prime(1)^2 =\u003e a(4) = 12,",
				"5 = prime(3)^1 =\u003e a(5) = 31,",
				"6 = prime(1)^1*prime(2)^1 =\u003e a(1) = 1121,",
				"7 = prime(3)^1 =\u003e a(7) = 41,",
				"8 = prime(1)^3 =\u003e a(8) = 13, and so on."
			],
			"maple": [
				"a:= n-\u003e `if`(n=1, 0, parse(cat(seq([numtheory[pi]",
				"       (i[1]), i[2]][], i=sort(ifactors(n)[2]))))):",
				"seq(a(n), n=1..60);  # _Alois P. Heinz_, Mar 16 2018"
			],
			"mathematica": [
				"Array[FromDigits@ Flatten@ Map[{PrimePi@ #1, #2} \u0026 @@ # \u0026, FactorInteger@ #] \u0026, 51] (* _Michael De Vlieger_, Mar 16 2018 *)"
			],
			"program": [
				"(PARI) A299400(n)=if(n=factor(n),eval(concat(apply(f-\u003eStr(primepi(f[1]),f[2]), Col(n)~))))"
			],
			"xref": [
				"Cf. A067599 (decimal encoding of prime factorization)."
			],
			"keyword": "nonn,base",
			"offset": "1,2",
			"author": "_M. F. Hasler_, Mar 08 2018",
			"references": 1,
			"revision": 24,
			"time": "2019-12-11T08:53:34-05:00",
			"created": "2018-03-24T10:22:48-04:00"
		}
	]
}