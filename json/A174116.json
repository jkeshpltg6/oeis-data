{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A174116",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 174116,
			"data": "1,1,1,1,1,1,1,3,3,1,1,6,18,6,1,1,10,60,60,10,1,1,15,150,300,150,15,1,1,21,315,1050,1050,315,21,1,1,28,588,2940,4900,2940,588,28,1,1,36,1008,7056,17640,17640,7056,1008,36,1,1,45,1620,15120,52920,79380,52920,15120,1620,45,1",
			"name": "Triangle T(n, k) = (n/2)*binomial(n-1, k-1)*binomial(n-1, k) with T(n, 0) = T(n, n) = 1, read by rows.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A174116/b174116.txt\"\u003eRows n = 0..100 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"Let c(n) = Product_{j=2..n} binomial(j,2) for n \u003e 1 otherwise 1 then the number triangle is given by T(n, k) = c(n)/(c(k)*c(n-k)).",
				"From _G. C. Greubel_, Feb 11 2021: (Start)",
				"T(n, k) = (n/2)*binomial(n-1, k-1)*binomial(n-1, k) with T(n, 0) = T(n, n) = 1.",
				"T(n, k) = binomial(n-k+1, 2)*A001263(n, k) with T(n, 0) = T(n, n) = 1.",
				"Sum_{k=0..n} T(n,k) = binomial(n, 2)*C_{n-1} + 2 - [n=0], where C_{n} are the Catalan numbers (A000108) and [] is the Iverson bracket. (End)"
			],
			"example": [
				"Triangle begins as:",
				"  1;",
				"  1,  1;",
				"  1,  1,    1;",
				"  1,  3,    3,     1;",
				"  1,  6,   18,     6,     1;",
				"  1, 10,   60,    60,    10,     1;",
				"  1, 15,  150,   300,   150,    15,     1;",
				"  1, 21,  315,  1050,  1050,   315,    21,     1;",
				"  1, 28,  588,  2940,  4900,  2940,   588,    28,    1;",
				"  1, 36, 1008,  7056, 17640, 17640,  7056,  1008,   36,  1;",
				"  1, 45, 1620, 15120, 52920, 79380, 52920, 15120, 1620, 45, 1;"
			],
			"mathematica": [
				"(* First program *)",
				"c[n_]:= If[n\u003c2, 1, Product[Binomial[j,2], {j, 2, n}]];",
				"T[n_, k_]:= c[n]/(c[k]*c[n-k]);",
				"Table[T[n, k], {n,0,12}, {k,0,n}]//Flatten",
				"(* Second program *)",
				"T[n_, k_]:= If[k==0 || k==n, 1, (n/2)*Binomial[n-1, k-1]*Binomial[n-1, k]];",
				"Table[T[n, k], {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, Feb 11 2021 *)"
			],
			"program": [
				"(Sage)",
				"def T(n,k): return 1 if (k==0 or k==n) else (n/2)*binomial(n-1, k-1)*binomial(n-1, k)",
				"flatten([[T(n,k) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Feb 11 2021",
				"(Magma)",
				"T:= func\u003c n,k | k eq 0 or k eq n select 1 else (n/2)*Binomial(n-1, k-1)*Binomial(n-1, k) \u003e;",
				"[T(n,k): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Feb 11 2021"
			],
			"xref": [
				"Cf. A174117, A174119, A174124, A174125.",
				"Cf. A000108, A001263."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Roger L. Bagula_, Mar 08 2010",
			"ext": [
				"Edited by _G. C. Greubel_, Feb 11 2021"
			],
			"references": 5,
			"revision": 5,
			"time": "2021-02-11T22:57:14-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}