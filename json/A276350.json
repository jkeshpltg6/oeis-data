{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A276350",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 276350,
			"data": "1,1,2,3,5,1,11,3,7,4,25,2,42,10,4,6,65,6,94,2,7,24,125,1,26,46,18,10,182,4,265,14,33,65,9,5,394,84,33,3,449,8,471,25,6,121,570,5,48,21,62,40,619,13,18,8,85,150,847,2,1020,263,9,13,38,31,1064,60",
			"name": "Sum of digits of 1/n in factorial base.",
			"comment": [
				"See the Wikipedia link for the construction method of 1/n in factorial base.",
				"For n\u003e1, A002034(n) gives the number of significant digits of 1/n in factorial base."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A276350/b276350.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A276350/a276350.png\"\u003eColored logarithmic scatterplot of the first 25000 terms\u003c/a\u003e (where the color is function of A052126(n))",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Factorial_number_system#Fractional_values\"\u003eFactorial number system (Fractional values)\u003c/a\u003e",
				"\u003ca href=\"/index/Fa#facbase\"\u003eIndex entries for sequences related to factorial base representation\u003c/a\u003e"
			],
			"formula": [
				"a(n!)=1 for any n\u003e0."
			],
			"example": [
				"n     1/n in factorial base           a(n)",
				"--    ----------------------------    ----",
				"1     1                               1",
				"2     0.0 1                           1",
				"3     0.0 0 2                         2",
				"4     0.0 0 1 2                       3",
				"5     0.0 0 1 0 4                     5",
				"6     0.0 0 1                         1",
				"7     0.0 0 0 3 2 0 6                 11",
				"8     0.0 0 0 3                       3",
				"9     0.0 0 0 2 3 2                   7",
				"10    0.0 0 0 2 2                     4",
				"11    0.0 0 0 2 0 5 3 1 4 0 10        25",
				"12    0.0 0 0 2                       2",
				"13    0.0 0 0 1 4 1 2 5 4 8 5 0 12    42",
				"14    0.0 0 0 1 3 3 3                 10",
				"15    0.0 0 0 1 3                     4"
			],
			"mathematica": [
				"f[n_] := Block[{s = 0, r = 1, a = 1/n}, While[a \u003e 0, s += Floor[a]; r++; a = FractionalPart[a]*r]; s] (* after Rémy Sigrist *); Array[f, 70] (* _Robert G. Wilson v_, Feb 01 2018 *)"
			],
			"program": [
				"(PARI) {a(n) = my(s=0, r=1, f=1/n); while (f\u003e0, s+= floor(f); r++; f = frac(f)*r); s}"
			],
			"xref": [
				"Cf. A002034, A007623, A034968, A052126."
			],
			"keyword": "nonn,base",
			"offset": "1,3",
			"author": "_Rémy Sigrist_, Dec 12 2016",
			"references": 5,
			"revision": 31,
			"time": "2018-12-06T15:51:04-05:00",
			"created": "2016-12-12T09:02:15-05:00"
		}
	]
}