{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A236830",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 236830,
			"data": "1,1,1,4,2,1,16,7,3,1,65,27,11,4,1,267,108,43,16,5,1,1105,440,173,65,22,6,1,4597,1812,707,267,94,29,7,1,19196,7514,2917,1105,398,131,37,8,1,80380,31307,12111,4597,1680,575,177,46,9,1,337284,130883,50503,19196,7085,2488,808,233,56,10,1",
			"name": "Riordan array (1/(1-x*C(x)^3), x*C(x)), C(x) the g.f. of A000108.",
			"comment": [
				"T(n+3,n) = A011826(n+5)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A236830/b236830.txt\"\u003eRows n= 0..100 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"Sum_{k=0..n} T(n,k) = A026726(n).",
				"G.f.: 1/((x^2*C(x)^4-x*C(x))*y-x*C(x)^3+1), where C(x) the g.f. of A000108. - _Vladimir Kruchinin_, Apr 22 2015",
				"From _Peter Bala_, Feb 18 2018: (Start)",
				"T(n,k) = Sum_{i = 0..n-k} Fibonacci(2*i-1)*binomial(2*n-2-k-i,n-k-i).",
				"The n-th row polynomial of row reverse triangle is the n-th degree Taylor polynomial of the rational function (1 - 3*x + 2*x^2)/(1 - 3*x + x^2) * 1/(1 - x)^n about 0. For example, for n = 4, (1 - 3*x + 2*x^2)/(1 - 3*x + x^2) * 1/(1 - x)^4 = 1 + 4*x + 11*x^2 + 27*x^3 + 65*x^4 + O(x^5), giving row 4 as (65, 27, 11, 4, 1). (End)"
			],
			"example": [
				"Triangle begins:",
				"      1;",
				"      1,    1;",
				"      4,    2,    1;",
				"     16,    7,    3,    1;",
				"     65,   27,   11,    4,   1;",
				"    267,  108,   43,   16,   5,   1;",
				"   1105,  440,  173,   65,  22,   6,  1;",
				"   4597, 1812,  707,  267,  94,  29,  7, 1;",
				"  19196, 7514, 2917, 1105, 398, 131, 37, 8, 1;",
				"Production matrix is:",
				"   1  1",
				"   3  1   1",
				"   6  1   1   1",
				"  10  1   1   1   1",
				"  15  1   1   1   1   1",
				"  21  1   1   1   1   1   1",
				"  28  1   1   1   1   1   1   1",
				"  36  1   1   1   1   1   1   1   1",
				"  45  1   1   1   1   1   1   1   1   1",
				"  55  1   1   1   1   1   1   1   1   1   1",
				"  66  1   1   1   1   1   1   1   1   1   1   1",
				"  78  1   1   1   1   1   1   1   1   1   1   1   1",
				"  91  1   1   1   1   1   1   1   1   1   1   1   1   1",
				"  ..."
			],
			"maple": [
				"A236830 := (n,k) -\u003e add(combinat:-fibonacci(2*i-1)*binomial(2*n-2-k-i,n-k-i), i = 0..n-k): seq(seq(A236830(n, k), k = 0..n), n = 0..10); # _Peter Bala_, Feb 18 2018"
			],
			"mathematica": [
				"(* The function RiordanArray is defined in A256893. *)",
				"c[x_] := (1 - Sqrt[1 - 4 x])/(2 x);",
				"RiordanArray[1/(1 - # c[#]^3)\u0026, # c[#]\u0026, 11] // Flatten (* _Jean-François Alcover_, Jul 16 2019 *)",
				"Table[Sum[Binomial[2*n-k-j-2, n-k-j]*Fibonacci[2*j-1], {j,0,n-k}], {n, 0, 12}, {k, 0, n}]//Flatten (* _G. C. Greubel_, Jul 18 2019 *)"
			],
			"program": [
				"(PARI) T(n,k) = sum(j=0,n-k, binomial(2*n-k-j-2, n-k-j)*fibonacci(2*j -1));",
				"for(n=0,12, for(k=0,n, print1(T(n,k), \", \"))) \\\\ _G. C. Greubel_, Jul 18 2019",
				"(MAGMA) [(\u0026+[Binomial(2*n-k-j-2, n-k-j)*Fibonacci(2*j-1): j in [0..n-k]]): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Jul 18 2019",
				"(Sage) [[sum( binomial(2*n-k-j-2, n-k-j)*fibonacci(2*j -1) for j in (0..n-k) ) for k in (0..n)] for n in (0..12)] # _G. C. Greubel_, Jul 18 2019",
				"(GAP) Flat(List([0..12], n-\u003e List([0..n], k-\u003e Sum([0..n-k], j-\u003e Binomial(2*n-k-j-2, n-k-j)*Fibonacci(2*j-1) )))); # _G. C. Greubel_, Jul 18 2019"
			],
			"xref": [
				"Cf. (columns): A165201, A026726, A026671, A026674, A026672, A026675, A026673, A026843, A026842 or A026846 or A026849, A026844, A026841 or A026848."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Philippe Deléham_, Feb 01 2014",
			"references": 12,
			"revision": 35,
			"time": "2019-07-18T15:44:20-04:00",
			"created": "2014-02-01T15:30:21-05:00"
		}
	]
}