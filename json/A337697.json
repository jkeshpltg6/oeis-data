{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A337697",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 337697,
			"data": "0,0,0,0,0,2,0,4,2,4,8,8,14,10,16,12,30,38,46,46,48,52,62,152,96,156,112,190,256,338,420,394,326,402,734,622,1150,802,946,898,1730,1946,2524,2200,2328,2308,3356,5816,4772,5350,4890,6282,6316,12092,8902",
			"name": "Number of pairwise coprime compositions of n with no 1's, where a singleton is not considered coprime.",
			"comment": [
				"A composition of n is a finite sequence of positive integers summing to n. These compositions must be strict."
			],
			"formula": [
				"For n \u003e 1, the version where singletons are considered coprime is a(n) + 1."
			],
			"example": [
				"The a(5) = 2 through a(12) = 14 compositions (empty column indicated by dot):",
				"  (2,3)  .  (2,5)  (3,5)  (2,7)  (3,7)    (2,9)  (5,7)",
				"  (3,2)     (3,4)  (5,3)  (4,5)  (7,3)    (3,8)  (7,5)",
				"            (4,3)         (5,4)  (2,3,5)  (4,7)  (2,3,7)",
				"            (5,2)         (7,2)  (2,5,3)  (5,6)  (2,7,3)",
				"                                 (3,2,5)  (6,5)  (3,2,7)",
				"                                 (3,5,2)  (7,4)  (3,4,5)",
				"                                 (5,2,3)  (8,3)  (3,5,4)",
				"                                 (5,3,2)  (9,2)  (3,7,2)",
				"                                                 (4,3,5)",
				"                                                 (4,5,3)",
				"                                                 (5,3,4)",
				"                                                 (5,4,3)",
				"                                                 (7,2,3)",
				"                                                 (7,3,2)"
			],
			"mathematica": [
				"Table[Length[Join@@Permutations/@Select[IntegerPartitions[n],!MemberQ[#,1]\u0026\u0026CoprimeQ@@#\u0026]],{n,0,30}]"
			],
			"xref": [
				"A022340 intersected with A333227 is a ranking sequence (using standard compositions A066099) for these compositions.",
				"A212804 does not require coprimality, with unordered version A002865.",
				"A337450 is the relatively prime instead of pairwise coprime version, with strict case A337451 and unordered version A302698.",
				"A337462 allows 1's, with strict case A337561 (or A101268 with singletons), unordered version A327516 with Heinz numbers A302696, and 3-part case A337461.",
				"A337485 is the unordered version (or A007359 with singletons considered coprime), with Heinz numbers A337984.",
				"A337563 is the case of unordered triples.",
				"Cf. A078374, A178472, A302568, A302697, A305713, A307719, A332004, A337562."
			],
			"keyword": "nonn",
			"offset": "0,6",
			"author": "_Gus Wiseman_, Oct 06 2020",
			"references": 8,
			"revision": 11,
			"time": "2020-10-13T14:33:52-04:00",
			"created": "2020-10-13T14:33:52-04:00"
		}
	]
}