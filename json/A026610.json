{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A026610",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 26610,
			"data": "0,2,0,2,1,1,1,0,1,2,1,1,1,0,2,0,2,1,1,0,2,0,2,0,2,1,0,2,1,1,1,0,2,0,2,1,1,0,2,0,2,0,2,1,1,0,2,0,2,1,1,1,0,2,1,0,2,0,2,0,2,1,1,0,2,0,2,1,1,1,0,1,2,1,1,1,0,2,0,2,0,2,1,1,1,0,2,0,2,1",
			"name": "a(n) = number of 1's between n-th 2 and (n+1)st 2 in A026600.",
			"comment": [
				"From _Michel Dekking_, Apr 16 2019: (Start)",
				"(a(n)) is a morphic sequence, i.e., a letter-to-letter projection of a fixed point of a morphism. A proof of this is more involved than the proof for the case of the closely related sequence A026609. The reason is that 2 is not the first letter of A026600.",
				"There are several ways to tackle this. We first remark that it suffices to prove that (a(n)) is the image of a fixed point of a morphism by a morphism delta (instead of a letter-to-letter projection), see Corollary 7.7.5 in the book by Allouche and Shallit.",
				"The sequence A026600 is fixed point of the 3-symbol Thue-Morse morphism mu given by mu: 1-\u003e123, 2-\u003e231, 3-\u003e312. Since the first 2 in (a(n)) is at position 2, we consider the 2-block 3-symbol Thue-Morse morphism mu_2 defined on the set of all nine 2-blocks ij by",
				"      1j-\u003e12,23,3j,   2j -\u003e23,31,1j , 3j-\u003e31,12,2j  for j=1,2,3.",
				"We then consider the unique fixed point x = 12,23,32,23,31,13,...  of mu_2. The return words of the 'letter' 12 in x are",
				"      A:=12,21, B:=12,23,32,23,31,13,31, C:=12,23,32,23,31,11,",
				"      D:=12,23,31, E:=12,23,33,31, F:=12,22,23,31,13,31,",
				"      G:=12,23,32,23,31, and H:=12,23,31,13,31.",
				"[See Justin \u0026 Vuillon (2000) for definition of return word. - _N. J. A. Sloane_, Sep 23 2019]",
				"The morphism mu_2 induces a morphism beta on the return words given by",
				"     A-\u003eC, B-\u003eBFAEA, C-\u003eBFAD, D-\u003eBA, E-\u003e BDA, F-\u003eGHAEA, G-\u003eBFA, H-\u003eBAEA.",
				"Counting 3's between 2's in the j's of the ij's in the return words followed by 12 yields the morphism delta given by",
				"      delta: A-\u003e1, B-\u003e02, C-\u003e02, D-\u003e1, E-\u003e1, F-\u003e02, G-\u003e01, H-\u003e2.",
				"Let y = BFAEAGHAEACBD... be the unique fixed point of beta. Then clearly (a(n)) = delta(y).",
				"(End)",
				"The frequencies of 0's, 1's and 2's in (a(n)) are 4/13, 5/13 and 4/13.",
				"This follows from an eigenvector computation, but can also be deduced from the frequency result for the sequence A026609: since the 3-symbol Thue Morse morphism generating sequence A026600 is symmetric under the permutation 1-\u003e2-\u003e3-\u003e1, the two sequences A026609 and A026610 generate the same language, and in particular all subwords have the same frequencies. - _Michel Dekking_, Apr 16 2019"
			],
			"reference": [
				"J.-P. Allouche and J. Shallit, Automatic Sequences, Cambridge Univ. Press, 2003."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A026610/b026610.txt\"\u003eTable of n, a(n) for n = 1..19683\u003c/a\u003e",
				"Jacques Justin and Laurent Vuillon, \u003ca href=\"http://www.numdam.org/item/ITA_2000__34_5_343_0/\"\u003eReturn words in Sturmian and episturmian words\u003c/a\u003e, RAIRO-Theoretical Informatics and Applications 34.5 (2000): 343-356."
			],
			"example": [
				"delta(B)=02, since there is no 11, 21, or 31 between B(1)=12 and  B(3)=32, and there is twice a 31 among B(4)=23,...,B(7)=31."
			],
			"mathematica": [
				"Rest@ Map[Count[#, 1] \u0026, DeleteCases[SplitBy[#, # == 2 \u0026], _?(# == {2} \u0026)]] \u0026@ Nest[Flatten[# /. {1 -\u003e {1, 2, 3}, 2 -\u003e {2, 3, 1}, 3 -\u003e {3, 1, 2}}] \u0026, {1}, 6] (* _Michael De Vlieger_, Apr 16 2019, after _Robert G. Wilson v_ at A026600 *)"
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Clark Kimberling_",
			"references": 3,
			"revision": 21,
			"time": "2019-10-03T09:00:15-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}