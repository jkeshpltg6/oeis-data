{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A347441",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 347441,
			"data": "0,1,1,1,1,1,1,2,1,1,1,2,1,1,1,2,1,2,1,2,1,1,1,2,1,1,2,2,1,1,1,4,1,1,1,4,1,1,1,2,1,1,1,2,2,1,1,5,1,2,1,2,1,3,1,2,1,1,1,2,1,1,2,5,1,1,1,2,1,1,1,6,1,1,2,2,1,1,1,5,2,1,1,2,1,1,1",
			"name": "Number of odd-length factorizations of n with integer alternating product.",
			"comment": [
				"A factorization of n is a weakly increasing sequence of positive integers \u003e 1 with product n.",
				"We define the alternating product of a sequence (y_1,...,y_k) to be Product_i y_i^((-1)^(i-1))."
			],
			"formula": [
				"a(2^n) = A027193(n)."
			],
			"example": [
				"The a(n) factorizations for n = 2, 8, 32, 48, 54, 72, 108:",
				"  2   8       32          48          54      72          108",
				"      2*2*2   2*2*8       2*4*6       2*3*9   2*6*6       2*6*9",
				"              2*4*4       3*4*4       3*3*6   3*3*8       3*6*6",
				"              2*2*2*2*2   2*2*12              2*2*18      2*2*27",
				"                          2*2*2*2*3           2*3*12      2*3*18",
				"                                              2*2*2*3*3   3*3*12",
				"                                                          2*2*3*3*3"
			],
			"mathematica": [
				"facs[n_]:=If[n\u003c=1,{{}},Join@@Table[Map[Prepend[#,d]\u0026,Select[facs[n/d],Min@@#\u003e=d\u0026]],{d,Rest[Divisors[n]]}]];",
				"altprod[q_]:=Product[q[[i]]^(-1)^(i-1),{i,Length[q]}];",
				"Table[Length[Select[facs[n],OddQ[Length[#]]\u0026\u0026IntegerQ[altprod[#]]\u0026]],{n,100}]"
			],
			"xref": [
				"The restriction to powers of 2 is A027193.",
				"Positions of 1's are A167207 = A005117 \\/ A001248.",
				"Allowing any alternating product gives A339890.",
				"Allowing even-length factorizations gives A347437.",
				"The even-length instead of odd-length version is A347438.",
				"The additive version is A347444, ranked by A347453.",
				"A038548 counts possible reverse-alternating products of factorizations.",
				"A273013 counts ordered factorizations of n^2 with alternating product 1.",
				"A339846 counts even-length factorizations.",
				"A347439 counts factorizations with integer reciprocal alternating product.",
				"A347440 counts factorizations with alternating product \u003c 1.",
				"A347442 counts factorizations with integer reverse-alternating product.",
				"A347456 counts factorizations with alternating product \u003e= 1.",
				"A347463 counts ordered factorizations with integer alternating product.",
				"Cf. A062312, A119620, A236913, A330972, A347445, A347446, A347447, A347451, A347458, A347460."
			],
			"keyword": "nonn",
			"offset": "1,8",
			"author": "_Gus Wiseman_, Sep 07 2021",
			"references": 16,
			"revision": 7,
			"time": "2021-09-15T09:52:29-04:00",
			"created": "2021-09-15T09:52:29-04:00"
		}
	]
}