{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A184011",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 184011,
			"data": "0,1,2,2,0,8,-56,32,10176,-215808,-78784,150990912,-3405688576,-139041794560,10385778676736,130003936220160,-43016304236761088,526545841919713280,266085261164348628992,-12347306589339686547456",
			"name": "Coefficients of the formal power series of a half-iterate of exp(x)-1 (rescaled).",
			"comment": [
				"Consider the formal power series for the real half-iterate of exp(x)-1 = Sum_{k\u003e=0} c_k*x^k with c_1 = +1 then a(k) = c_k*k!*4^{k-1} and all a(k) seem to be integer.",
				"For the general technique of finding the half-iterate of power series, see for instance the Comtet reference."
			],
			"reference": [
				"Comtet, L; Advanced Combinatorics (1974 edition), D. Reidel Publishing Company, Dordrecht - Holland, pp. 147-148."
			],
			"link": [
				"Gottfried Helms, \u003ca href=\"http://go.helms-net.de/math/tetdocs/CoefficientsForUTetration.htm\"\u003eCoefficients for fractional iterates exp(x)-1\u003c/a\u003e",
				"Dmitry Kruchinin, Vladimir Kruchinin, \u003ca href=\"http://arxiv.org/abs/1302.1986\"\u003eMethod for solving an iterative functional equation $A^{2^n}(x)=F(x)$\u003c/a\u003e, arXiv:1302.1986 [math.CO], 2013."
			],
			"formula": [
				"G.f. f(x) where f(f(x)) = exp(x)-1 with f'(0)=1.",
				"T(n,m) = if n=m then 1 else (stirling2(n,m)*m!/n!-sum(i=m+1..n-1, T(n,i)*T(i,m)))/2; a(n) = 4^(n-1)*n!*T(n,1). - _Vladimir Kruchinin_, Nov 09 2011",
				"E.g.f. A(x), satisfies A(A(x))=(exp(4*x)-1)/4, T(n,m)=1/2*(4^(n-m)*stirling2(n,m)-sum(i=m+1..n-1, T(n,i)*T(i,m))), T(n,n)=1, a(n)=T(n,1), a(0)=0. - _Dmitry Kruchinin_, Dec 04 2012"
			],
			"example": [
				"f(x) = x + 1/4*x^2 + 1/48*x^3 + 1/3840*x^5 - 7/92160*x^6 + 1/645120*x^7 + O(x^8)",
				"so   c_3  = 1/48",
				"and  a(3) = c_3 * 4^2*3! = 16*6/48 = 2"
			],
			"mathematica": [
				"max = 19; f[x_] := Sum[c[k]*x^k, {k, 0, max}]; c[0] = 0; c[1] = 1; coes = CoefficientList[ Series[f[f[x]] - Exp[x] - 1, {x, 0, max}], x]; sol = Solve[Thread[coes == 0] // Rest] // First; Table[c[n]*4^(n-1)*n!, {n, 0, max}] /. sol (* _Jean-François Alcover_, Feb 11 2013 *)"
			],
			"program": [
				"(PARI) {a(n)=local(A=x+x^2,B=x);for(i=1,n,B=serreverse(A+x*O(x^n));A=(A+exp(B)-1)/2);4^(n-1)*n!*polcoeff(A,n)} \\\\ _Paul D. Hanna_",
				"(PARI)",
				"{trisqrt(m) = local(tmp, rs=rows(m), cs=cols(m), c);",
				"\\\\ computes sqrt of lower triangular matrix with unit-diagonal",
				"   tmp=matid(#m);",
				"   for(d=1,rs-1,",
				"        for(r=d+1,rs,",
				"              c=r-d;",
				"              tmp[r,c]=(m[r,c]-sum(k=c+1,r-1,tmp[r,k]*tmp[k,c]))",
				"                        /(tmp[c,c]+tmp[r,r])",
				"           );",
				"      );",
				"return(tmp);}",
				"ff = exp(x)-1",
				"Mff = matrix(6,6,r,c,polcoeff(ff^(c-1),(r-1))) \\\\ create Bell-matrix for ff",
				"Mf =  trisqrt ( Mff )  \\\\ = Mff^(1/2) is Bellmatrix for f",
				"f = Ser(Mf[,2])  \\\\ coefficients of power series for half-iterate of exp(x)-1 from second column in Mf"
			],
			"xref": [
				"Cf. A052122."
			],
			"keyword": "sign",
			"offset": "0,3",
			"author": "_Gottfried Helms_, Feb 13 2011",
			"references": 3,
			"revision": 43,
			"time": "2015-12-21T12:50:53-05:00",
			"created": "2011-01-08T16:59:05-05:00"
		}
	]
}