{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A028491",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 28491,
			"id": "M2643",
			"data": "3,7,13,71,103,541,1091,1367,1627,4177,9011,9551,36913,43063,49681,57917,483611,877843,2215303,2704981,3598867",
			"name": "Numbers k such that (3^k - 1)/2 is prime.",
			"comment": [
				"If k is in the sequence and m=3^(k-1) then m is a term of A033632 (phi(sigma(m)) = sigma(phi(m)), so 3^(A028491-1) is a subsequence of A033632. For example since 9551 is in the sequence, phi(sigma(3^9550)) = sigma(phi(3^9550)). - _Farideh Firoozbakht_, Feb 09 2005",
				"Salas lists these, except 3, in \"Open Problems\" p. 6 [March 2012], and proves that the Cantor primes \u003e 3 are exactly the prime-valued cyclotomic polynomials of the form Phi_s(3^{s^j}) == 1 (mod 4).",
				"Also, k such that 3^k-1 is a semiprime - see also A080892. - _M. F. Hasler_, Mar 19 2013",
				"a(22) \u003e 5000000."
			],
			"reference": [
				"J. Brillhart et al., Factorizations of b^n +- 1. Contemporary Mathematics, Vol. 22, Amer. Math. Soc., Providence, RI, 2nd edition, 1985; and later supplements.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Antal Bege and Kinga Fogarasi, \u003ca href=\"https://arxiv.org/abs/1008.0155\"\u003eGeneralized perfect numbers\u003c/a\u003e, arXiv:1008.0155 [math.NT], 2010. See p. 81.",
				"Paul Bourdelais, \u003ca href=\"https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;417ab0d6.0906\"\u003eA Generalized Repunit Conjecture\u003c/a\u003e, Posting in NMBRTHRY@LISTSERV.NODAK.EDU, Jun 25, 2009.",
				"J. Brillhart et al., \u003ca href=\"http://dx.doi.org/10.1090/conm/022\"\u003eFactorizations of b^n +- 1\u003c/a\u003e, Contemporary Mathematics, Vol. 22, Amer. Math. Soc., Providence, RI, 3rd edition, 2002.",
				"H. Dubner, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1993-1185243-9\"\u003eGeneralized repunit primes\u003c/a\u003e, Math. Comp., 61 (1993), 927-930.",
				"H. Dubner, \u003ca href=\"/A028491/a028491.pdf\"\u003eGeneralized repunit primes\u003c/a\u003e, Math. Comp., 61 (1993), 927-930. [Annotated scanned copy]",
				"H. Lifchitz, \u003ca href=\"http://www.primenumbers.net/Henri/us/MersFermus.htm\"\u003eMersenne and Fermat primes field\u003c/a\u003e",
				"Christian Salas, \u003ca href=\"http://arxiv.org/abs/1203.3969\"\u003eCantor Primes as Prime-Valued Cyclotomic Polynomials\u003c/a\u003e, arXiv:1203.3969v1 [math.NT], Mar 18, 2012.",
				"S. S. Wagstaff, Jr., \u003ca href=\"http://www.cerias.purdue.edu/homes/ssw/cun/index.html\"\u003eThe Cunningham Project\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Repunit.html\"\u003eRepunit\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#primepop\"\u003eIndex to primes in various ranges\u003c/a\u003e, form ((k+1)^n-1)/k"
			],
			"mathematica": [
				"Do[If[PrimeQ[(3^n-1)/2], Print[n]], {n, 10000}] (* _Farideh Firoozbakht_, Feb 09 2005 *)"
			],
			"program": [
				"(PARI) forprime(p=2,1e5,if(ispseudoprime(3^p\\2),print1(p\", \"))) \\\\ _Charles R Greathouse IV_, Jul 15 2011"
			],
			"xref": [
				"Cf. A076481, A033632."
			],
			"keyword": "nonn,more,hard",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_, Jean-Yves Perrier (nperrj(AT)ascom.ch)",
			"ext": [
				"a(13) from _Farideh Firoozbakht_, Mar 27 2005",
				"a(14)-a(16) from _Robert G. Wilson v_, Apr 11 2005",
				"a(17) corresponds to a probable prime discovered by _Paul Bourdelais_, Feb 08 2010",
				"a(18) corresponds to a probable prime discovered by _Paul Bourdelais_, Jul 06 2010",
				"a(19) corresponds to a probable prime discovered by _Paul Bourdelais_, Feb 05 2019",
				"a(20) and a(21) correspond to probable primes discovered by _Ryan Propper_, Dec 2021"
			],
			"references": 72,
			"revision": 93,
			"time": "2021-12-29T19:54:04-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}