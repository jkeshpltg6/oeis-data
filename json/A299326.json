{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A299326",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 299326,
			"data": "2,5,7,8,12,16,11,18,26,34,14,24,38,54,70,20,30,50,78,110,142,22,42,62,102,158,222,286,28,46,86,126,206,318,446,574,32,58,94,174,254,414,638,894,1150,36,66,118,190,350,510,830,1278,1790,2302",
			"name": "Rectangular array by antidiagonals: row n gives the ranks of {2,3}-power towers that start with n 3's, for n \u003e=0; see Comments.",
			"comment": [
				"Suppose that S is a set of real numbers.  An S-power-tower, t, is a number t = x(1)^x(2)^...^x(k), where k \u003e= 1 and x(i) is in S for i = 1..k.  We represent t by (x(1),x(2),...x(k), which for k \u003e 1 is defined as (x(1),((x(2),...,x(k-1)); (2,3,2) means 2^9.  The number k is the *height* of t.  If every element of S exceeds 1 and all the power towers are ranked in increasing order, the position of each in the resulting sequence is its *rank*.  See A299229 for a guide to related sequences.",
				"As sequences, this one and A299325 partition the positive integers."
			],
			"reference": [
				"1"
			],
			"example": [
				"Northwest corner:",
				"2     5    8   11   14   20   22",
				"7    12   18   24   30   42   46",
				"16   26   38   50   62   86   94",
				"34   54   78  102  126  174  190",
				"70  110  158  206  254  350  382"
			],
			"mathematica": [
				"t[1] = {2}; t[2] = {3}; t[3] = {2, 2}; t[4] = {2, 3}; t[5] = {3, 2};",
				"t[6] = {2, 2, 2}; t[7] = {3, 3};",
				"t[8] = {3, 2, 2}; t[9] = {2, 2, 3}; t[10] = {2, 3, 2};",
				"t[11] = {3, 2, 3}; t[12] = {3, 3, 2};",
				"z = 500; g[k_] := If[EvenQ[k], {2}, {3}];",
				"f = 6; While[f \u003c 13, n = f; While[n \u003c z, p = 1;",
				"   While[p \u003c 17, m = 2 n + 1; v = t[n]; k = 0;",
				"   While[k \u003c 2^p, t[m + k] = Join[g[k], t[n + Floor[k/2]]]; k = k + 1];",
				"   p = p + 1; n = m]]; f = f + 1]",
				"s = Select[Range[60000], Count[First[Split[t[#]]], 2] == 0 \u0026 ];",
				"r[n_] := Select[s, Length[First[Split[t[#]]]] == n \u0026, 12]",
				"TableForm[Table[r[n], {n, 1, 10}]]  (* A299326, array *)",
				"w[n_, k_] := r[n][[k]];",
				"Table[w[n - k + 1, k], {n, 10}, {k, n, 1, -1}] // Flatten (* A299326, sequence *)"
			],
			"xref": [
				"Cf. A299229, A299325."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "1,1",
			"author": "_Clark Kimberling_, Feb 08 2018",
			"references": 2,
			"revision": 4,
			"time": "2018-02-08T21:49:32-05:00",
			"created": "2018-02-08T21:49:32-05:00"
		}
	]
}