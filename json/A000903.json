{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000903",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 903,
			"id": "M1761 N0698",
			"data": "1,1,2,7,23,115,694,5282,46066,456454,4999004,59916028,778525516,10897964660,163461964024,2615361578344,44460982752488,800296985768776,15205638776753680,304112757426239984,6386367801916347184",
			"name": "Number of inequivalent ways of placing n nonattacking rooks on n X n board up to rotations and reflections of the board.",
			"reference": [
				"L. C. Larson, The number of essentially different nonattacking rook arrangements, J. Recreat. Math., 7 (No. 3, 1974), circa pages 180-181.",
				"R. C. Read, personal communication.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"Z. Stankova and J. West, A new class of Wilf-equivalent permutations, J. Algeb. Combin., 15 (2002), 271-290."
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A000903/b000903.txt\"\u003eTable of n, a(n) for n = 1..100\u003c/a\u003e",
				"P. J. Cameron, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL3/groups.html\"\u003eSequences realized by oligomorphic permutation groups\u003c/a\u003e, J. Integ. Seqs. Vol. 3 (2000), #00.1.5.",
				"L. C. Larson, \u003ca href=\"/A000900/a000900_1.pdf\"\u003eThe number of essentially different nonattacking rook arrangements\u003c/a\u003e, J. Recreat. Math., 7 (No. 3, 1974), circa pages 180-181. [Annotated scan of pages 180 and 181 only]",
				"E. Lucas, \u003ca href=\"http://gallica.bnf.fr/ark:/12148/bpt6k29021h\"\u003eThéorie des Nombres\u003c/a\u003e, Gauthier-Villars, Paris, 1891, Vol. 1, p. 222.",
				"E. Lucas, \u003ca href=\"/A000899/a000899.pdf\"\u003eThéorie des nombres\u003c/a\u003e (annotated scans of a few selected pages)",
				"R. C. Read, \u003ca href=\"/A000684/a000684_1.pdf\"\u003eLetter to N. J. A. Sloane, Oct. 29, 1976\u003c/a\u003e",
				"R. W. Robinson, \u003ca href=\"http://dx.doi.org/10.1007/BFb0097382\"\u003eCounting arrangements of bishops\u003c/a\u003e, pp. 198-214 of Combinatorial Mathematics IV (Adelaide 1975), Lect. Notes Math., 560 (1976).",
				"R. W. Robinson, \u003ca href=\"/A000899/a000899_1.pdf\"\u003eCounting arrangements of bishops\u003c/a\u003e, pp. 198-214 of Combinatorial Mathematics IV (Adelaide 1975), Lect. Notes Math., 560 (1976). (Annotated scanned copy)",
				"Zvezdelina Stankova-Frenkel and Julian West, \u003ca href=\"http://arxiv.org/abs/math/0103152\"\u003eA new class of Wilf-equivalent permutations\u003c/a\u003e, arXiv:math/0103152 [math.CO], 2001. See Fig. 9.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RookNumber.html\"\u003eRook Number.\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RooksProblem.html\"\u003eRooks Problem.\u003c/a\u003e"
			],
			"formula": [
				"If n\u003e1 then a(n) = 1/8 * (F(n) + C(n) + 2 * R(n) + 2 * D(n)), where F(n) = A000142(n) [all solutions, i.e., factorials], C(n) = A037223(n) [central symmetric solutions], R(n) = A037224(n) [rotationally symmetric solutions] and D(n) = A000085(n) [symmetric solutions by reflection at a diagonal]. - _Matthias Engelhardt_, Apr 05 2000",
				"For asymptotics see the Robinson paper."
			],
			"example": [
				"For n=4 the 7 solutions may be taken to be 1234,1243,1324,1423,1432,2143,2413."
			],
			"maple": [
				"Maple programs for A000142, A037223, A122670, A001813, A000085, A000898, A000407, A000902, A000900, A000901, A000899, A000903",
				"P:=n-\u003en!; # Gives A000142",
				"G:=proc(n) local k; k:=floor(n/2); k!*2^k; end; # Gives A037223, A000165",
				"R:=proc(n) local m; if n mod 4 = 2 or n mod 4 = 3 then RETURN(0); fi; m:=floor(n/4); (2*m)!/m!; end; # Gives A122670, A001813",
				"unprotect(D); D:=proc(n) option remember; if n \u003c= 1 then 1 else D(n-1)+(n-1)*D(n-2); fi; end; # Gives A000085",
				"B:=proc(n) option remember; if n \u003c= 1 then RETURN(1); fi; if n mod 2 = 1 then RETURN(B(n-1)); fi; 2*B(n-2) + (n-2)*B(n-4); end; # Gives A000898 (doubled up)",
				"rho:=n-\u003eR(n)/2; # Gives A000407, aerated",
				"beta:=n-\u003eB(n)/2; # Gives A000902, doubled up",
				"delta:=n-\u003e(D(n)-B(n))/2; # Gives A000900",
				"unprotect(gamma); gamma:=n-\u003e if n \u003c= 1 then RETURN(0) else (G(n)-B(n)-R(n))/4; fi; # Gives A000901, doubled up",
				"alpha:=n-\u003eP(n)/8-G(n)/8+B(n)/4-D(n)/4; # Gives A000899",
				"unprotect(sigma); sigma:=n-\u003e if n \u003c= 1 then RETURN(1); else P(n)/8+G(n)/8+R(n)/4+D(n)/4; fi; #Gives A000903"
			],
			"mathematica": [
				"c[n_] := Floor[n/2]!2^Floor[n/2]; r[n_] := If[Mod[n, 4] \u003e 1, 0, m = Floor[n/4]; If[m == 0, 1, (2 m)!/m!]]; d[0] = d[1] = 1; d[n_] := d[n] = (n - 1)d[n - 2] + d[n - 1]; a[1] = 1; a[n_] := (n! + c[n] + 2r[n] + 2d[n])/8; Array[a, 21] (* _Jean-François Alcover_, Apr 06 2011, after _Matthias Engelhardt_, further improved by _Robert G. Wilson v_ *)"
			],
			"xref": [
				"Cf. A000142, A037223, A037224, A000085, A005635, A099952, A263685."
			],
			"keyword": "nonn,nice",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _David W. Wilson_, Jul 13 2003"
			],
			"references": 13,
			"revision": 65,
			"time": "2019-04-08T03:50:17-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}