{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A276807",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 276807,
			"data": "2,4,7,7,7,15,15,23,31,23,23,31,47,39,47,55,55,63,71,63,63,87,87,95,95,119,87,119,111,95,135,135,143,151,135,167,159,151,143,167,167,175,191,191,215,183,231,231,215,207,223,255,223,231,255,271,279,263,303,255",
			"name": "Number of solutions of the congruence y^2  == x^3 - x^2 - 4*x + 4 (mod p) as p runs through the primes.",
			"comment": [
				"This elliptic curve corresponds to a weight 2 newform which is an eta-quotient, namely, eta(2t)*eta(4t)*eta(6t)*eta(12t), see Theorem 2 in Martin \u0026 Ono."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A276807/b276807.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Yves Martin and Ken Ono, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9939-97-03928-2\"\u003eEta-Quotients and Elliptic Curves\u003c/a\u003e, Proc. Amer. Math. Soc. 125, No 11 (1997), 3169-3176."
			],
			"formula": [
				"a(n) gives the number of solutions of the congruence y^2 == x^3 - x^2 - 4*x + 4 (mod prime(n)), n \u003e= 1."
			],
			"example": [
				"The first nonnegative complete residue system {0, 1, ..., prime(n)-1} is used.",
				"The solutions (x, y) of y^2 == x^3 - x^2 - 4*x + 4 (mod prime(n)) begin:",
				"n, prime(n), a(n)  solutions (x, y)",
				"1,   2,       2:   (0, 0), (1, 0)",
				"2,   3,       4:   (0, 1), (0, 2), (1, 0),",
				"                   (2, 0)",
				"3,   5,       7:   (0, 2), (0, 3), (1, 0),",
				"                   (2, 0), (3, 0), (4, 1),",
				"                   (4, 4)",
				"4,   7,       7:   (0, 2), (0, 5), (1, 0),",
				"                   (2, 0), (4, 1), (4, 6),",
				"                   (5, 0)"
			],
			"program": [
				"(Ruby)",
				"require 'prime'",
				"def A(a3, a2, a4, a6, n)",
				"  ary = []",
				"  Prime.take(n).each{|p|",
				"    a = Array.new(p, 0)",
				"    (0..p - 1).each{|i| a[(i * i + a3 * i) % p] += 1}",
				"    ary \u003c\u003c (0..p - 1).inject(0){|s, i| s + a[(i * i * i + a2 * i * i + a4 * i + a6) % p]}",
				"  }",
				"  ary",
				"end",
				"def A276807(n)",
				"  A(0, -1, -4, 4, n)",
				"end"
			],
			"xref": [
				"Cf. A276649."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Seiichi Manyama_, Sep 17 2016",
			"references": 2,
			"revision": 19,
			"time": "2018-10-24T02:32:39-04:00",
			"created": "2016-09-18T13:59:36-04:00"
		}
	]
}