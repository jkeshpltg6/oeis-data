{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A010554",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 10554,
			"data": "1,1,1,1,2,1,2,2,2,2,4,2,4,2,4,4,8,2,6,4,4,4,10,4,8,4,6,4,12,4,8,8,8,8,8,4,12,6,8,8,16,4,12,8,8,10,22,8,12,8,16,8,24,6,16,8,12,12,28,8,16,8,12,16,16,8,20,16,20,8,24,8",
			"name": "a(n) = phi(phi(n)), where phi is the Euler totient function.",
			"comment": [
				"If n has a primitive root, then it has exactly phi(phi(n)) of them (Burton 1989, p. 188), which means that if p is a prime number, then there are exactly phi(p-1) incongruent primitive roots of p (Burton 1989). - _Jonathan Vos Post_, Sep 10 2010",
				"See A046144 for the number of primitive roots mod n. - _Wolfdieter Lang_, Mar 09 2012"
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), p. 840.",
				"Burton, D. M. \"The Order of an Integer Modulo n,\" \"Primitive Roots for Primes,\" and \"Composite Numbers Having Primitive Roots.\" Sections 8.1-8.3 in Elementary Number Theory, 4th ed. Dubuque, IA: William C. Brown Publishers, pp. 184-205, 1989."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A010554/b010554.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"Paul Erdős, Andrew Granville, Carl Pomerance and Claudia Spiro, \u003ca href=\"http://math.dartmouth.edu/~carlp/iterate.pdf\"\u003eOn the normal behavior of the iterates of some arithmetic functions\u003c/a\u003e, Analytic number theory, Birkhäuser Boston, 1990, pp. 165-204.",
				"Paul Erdos, Andrew Granville, Carl Pomerance and Claudia Spiro, \u003ca href=\"/A000010/a000010_1.pdf\"\u003eOn the normal behavior of the iterates of some arithmetic functions\u003c/a\u003e, Analytic number theory, Birkhäuser Boston, 1990, pp. 165-204. [Annotated copy with A-numbers]",
				"S. R. Finch, \u003ca href=\"http://arXiv.org/abs/math.NT/0605019\"\u003eIdempotents and Nilpotents Modulo n\u003c/a\u003e (arXiv:math.NT/0605019)",
				"Boris Putievskiy, \u003ca href=\"https://arxiv.org/abs/1212.2732\"\u003eTransformations [Of] Integer Sequences And Pairing Functions\u003c/a\u003e, arXiv preprint arXiv:1212.2732 [math.CO], 2012.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PrimitiveRoot.html\"\u003ePrimitive Root.\u003c/a\u003e"
			],
			"maple": [
				"with(numtheory): f := n-\u003ephi(phi(n));"
			],
			"mathematica": [
				"Table[EulerPhi[EulerPhi[n]],{n,0,200}] (* _Vladimir Joseph Stephan Orlovsky_, Nov 10 2009 *)"
			],
			"program": [
				"(Haskell)",
				"a010554 = a000010 . a000010  -- _Reinhard Zumkeller_, Dec 26 2012",
				"(PARI) a(n)=eulerphi(eulerphi(n)) \\\\ _Charles R Greathouse IV_, Feb 06 2017",
				"(MAGMA) [EulerPhi(EulerPhi(n)): n in [1..100]]; // _Vincenzo Librandi_, Feb 24 2018"
			],
			"xref": [
				"Cf. A000010, A049099, A049100, A049107, A077197."
			],
			"keyword": "nonn,nice",
			"offset": "1,5",
			"author": "_N. J. A. Sloane_",
			"references": 36,
			"revision": 48,
			"time": "2018-05-08T15:11:54-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}