{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A258926",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 258926,
			"data": "1,-120,-21859200,-131273353728000,-6725237593471119360000,-1653993087378574357912780800000,-1405832822961504544259161592168448000000,-3334380558587161259470375739654344298987520000000,-18982929854690021819576777610944622891185796965990400000000",
			"name": "E.g.f.: S(x) = Series_Reversion( Integral 1/(1-x^6)^(1/6) dx ), where the constant of integration is zero.",
			"formula": [
				"Let e.g.f. C(x) = Sum_{n\u003e=0} a(n)*x^(6*n)/(6*n)! and e.g.f. S(x) = Sum_{n\u003e=0} a(n)*x^(6*n+1)/(6*n+1)!, then C(x) and S(x) satisfy:",
				"(1) C(x)^6 + S(x)^6 = 1,",
				"(2) S'(x) = C(x),",
				"(3) C'(x) = -S(x)^5/C(x)^4,",
				"(4) C(x)^5 * C'(x) + S(x)^5 * S'(x) = 0,",
				"(5) S(x)/C(x) = Integral 1/C(x)^6 dx,",
				"(6) S(x)/C(x) = Series_Reversion( Integral 1/(1+x^6) dx ) = Series_Reversion( Sum_{n\u003e=0} (-1)^n * x^(6*n+1)/(6*n+1) ).",
				"(7) S(x)^3/C(x)^3 = tan( 3 * Integral S(x)^2/C(x)^2 dx ).",
				"(8) C(x)^3 + I*S(x)^3 = exp( 3*I * Integral S(x)^2/C(x)^2 dx )."
			],
			"example": [
				"E.g.f. with offset 0 is C(x) and e.g.f. with offset 1 is S(x) where:",
				"C(x) = 1 - 120*x^6/6! - 21859200*x^12/12! - 131273353728000*x^18/18! -...",
				"S(x) = x - 120*x^7/7! - 21859200*x^13/13! - 131273353728000*x^19/19! -...",
				"such that C(x)^6 + S(x)^6 = 1:",
				"C(x)^6 = 1 - 720*x^6/6! + 68428800*x^12/12! + 80406577152000*x^18/18! +...",
				"S(x)^6 = 720*x^6/6! - 68428800*x^12/12! - 80406577152000*x^18/18! -...",
				"Related Expansions.",
				"(1) The series reversion of S(x) is Integral 1/(1-x^6)^(1/6) dx:",
				"Series_Reversion(S(x)) = x + 120*x^7/7! + 46569600*x^13/13! + 449549388288000*x^19/19! +...",
				"1/(1-x^6)^(1/6) = 1 + 120*x^6/6! + 46569600*x^12/12! + 449549388288000*x^18/18! +...",
				"(2) d/dx S(x)/C(x) = 1/C(x)^6:",
				"1/C(x)^6 = 1 + 720*x^6/6! + 410572800*x^12/12! + 4492717498368000*x^18/18! +...",
				"S(x)/C(x) = x + 720*x^7/7! + 410572800*x^13/13! + 4492717498368000*x^19/19! + 348990783113936240640000*x^25/25! +...+ A258927(n)*x^(6*n+1)/(6*n+1)! +...",
				"where",
				"Series_Reversion(S(x)/C(x)) = x - x^7/7 + x^13/13 - x^19/19 + x^25/25 - x^31/31 +..."
			],
			"mathematica": [
				"nmax = 8; a[n_] := SeriesCoefficient[ InverseSeries[ Integrate[1/(1 - x^6)^(1/6), x] + O[x]^(6nmax+2), x], 6n+1]*(6n+1)!; Table[a[n], {n, 0, nmax}] (* _Jean-François Alcover_, Apr 26 2017 *)"
			],
			"program": [
				"(PARI) /* E.g.f. Series_Reversion(Integral 1/(1-x^6)^(1/6) dx): */",
				"{a(n)=local(S=x); S = serreverse( intformal(  1/(1-x^6 +x*O(x^(6*n)))^(1/6) )); (6*n+1)!*polcoeff(S, 6*n+1)}",
				"for(n=0, 15, print1(a(n), \", \"))",
				"(PARI) /* E.g.f. C(x) with offset 0: */",
				"{a(n)=local(S=x, C=1+x); for(i=1, n, S=intformal(C +x*O(x^(6*n))); C=1-intformal(S^5/C^4 +x*O(x^(6*n))); ); (6*n)!*polcoeff(C,6*n)}",
				"for(n=0, 21, print1(a(n), \", \"))",
				"(PARI) /* E.g.f. S(x) with offset 1: */",
				"{a(n)=local(S=x, C=1+x); for(i=1, n+1, S=intformal(C +x*O(x^(6*n+1))); C=1-intformal(S^5/C^4 +x*O(x^(6*n+1))); ); (6*n+1)!*polcoeff(S,6*n+1)}",
				"for(n=0, 21, print1(a(n), \", \"))"
			],
			"xref": [
				"Cf. A258927 (S/C), A258878, A258900, A258924."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Paul D. Hanna_, Jun 14 2015",
			"references": 1,
			"revision": 14,
			"time": "2017-04-26T08:49:37-04:00",
			"created": "2015-06-14T23:28:56-04:00"
		}
	]
}