{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A051909",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 51909,
			"data": "1,11,24,30,31,32,37,38,43,52,53,54,55,59,60,61,65,73,75,80,91",
			"name": "Subset of strict Egyptian numbers - there is a unique representation of 1 as the sum of distinct unit fractions with the sum of denominators being these numbers.",
			"comment": [
				"Note the word \"unique\" in the definition, which makes this different from A052428. - _N. J. A. Sloane_, Jul 13 2017",
				"From _Juhani Heino_, Jul 15 2017: (Start)",
				"McCranie conjectured that the sequence is finite. He was right. Numbers 92-269 all have at least two representations without 15 appearing among the reciprocals (list attached). In the same way that Graham did for strict Egyptian numbers, we can transform each representation (a_n) into an even (2, (2a_n)) and an odd (3, 9, 30, 45, (2a_n)). Because there are no 15's in the original representations, the odd transformation has distinct unit fractions. The same is trivially true for the even transformation because there are no 1's either. So if the original sum is s, the new sums are 2s+2 and 2s+87. 2*92+2 = 186, so the even sums can be obtained from transformations after that. 2*92+87 = 271, so the same holds for odd sums from that point. And as the pairs of original representations were distinct, so are the transformed pairs.",
				"If my programming and reasoning were correct, I proved that the sequence is full (no more members after 91), and my program in the raw mode (not ignoring the 15's) corroborates exactly with McCranie's results. Could somebody prove the uniqueness?",
				"(End)"
			],
			"link": [
				"R. L. Graham, \u003ca href=\"https://doi.org/10.1017/S1446788700039045\"\u003eA theorem on partitions\u003c/a\u003e, J. Austral. Math. Soc. 3:4 (1963), pp. 435-441.",
				"Juhani Heino, \u003ca href=\"/A051909/a051909.txt\"\u003eList of double representations from 92 to 269\u003c/a\u003e",
				"\u003ca href=\"/index/Ed#Egypt\"\u003eIndex entries for sequences related to Egyptian fractions\u003c/a\u003e"
			],
			"example": [
				"1 = 1/2 + 1/3 + 1/6, 2 + 3 + 6 = 11, there is no other representation of 1 as the sum of distinct unit fractions whose numerators sum to 11, so 11 is in the sequence."
			],
			"xref": [
				"A subset of A052428. Cf. A051882."
			],
			"keyword": "full,nonn,fini",
			"offset": "1,2",
			"author": "_Jud McCranie_, Dec 16 1999",
			"ext": [
				"Name clarified by _Juhani Heino_, Jul 14 2017"
			],
			"references": 4,
			"revision": 33,
			"time": "2017-10-03T02:19:50-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}