{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A262031",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 262031,
			"data": "1,4,5,31,247,389,1307,15637,13327,187111,199123,353201,6364777,127056883,23083451,24191987,579694957,535076383,13912332463,43224283189,40355946289,1210479158981,38689398709811,72866186391697,75054119011297,77117026909777,73105817107177,2777117009412349",
			"name": "Numerator of partial sums of a reordered alternating harmonic series.",
			"comment": [
				"For the denominators see A262022.",
				"The reordered alternating harmonic series considered here is 1 + 1/3 - 1/2 + 1/5 + 1/7 - 1/4 + 1/9 + 1/11 - 1/6 + ... + ... - ...",
				"The limit n -\u003e infinity of the partial sums s(n) = a(n)/A262031(n) is 3*log(2)/2, approximately 1.03972077083991... For the decimal expansion see A262023.",
				"Combining three consecutive terms of this series leads to the series b(0) + b(1) + ..., with b(k) = (1/2)*(8*k+5)/((4*k+1)*(4*k+3)*(k+1)). This produces partial sums 5/6, 13/140, 7/198, 29/1560, 37/3230, ..., which are given by s(3*n+2), n = 0, 1, .... Therefore, the limit is the same as the one given above, and it is obtained from Sum_{k=0..n} b(k) = (1/4)*Psi(n+5/4) + (1/4)*Psi(n+7/4) - (1/2)*Psi(n+2) + (3/2)*log(2), with the digamma function Psi(x).",
				"This reordered alternating harmonic series appears as an example in the famous Dirichlet article, p. 319 (Werke I). Martin Ohm showed that for the reordering with alternating m consecutive positive terms followed by n negative terms (here n = 2 and m = 1) the sum becomes log(2) + (1/2)*log(m/n). See the reference, paragraph 8. p. 12-14. See also the Pringsheim reference."
			],
			"link": [
				"P. G. Lejeune Dirichlet, \u003ca href=\"http://gallica.bnf.fr/ark:/12148/bpt6k99435r/f332.image\"\u003eBeweis des Satzes, dass jede unbegrenzte arithmetische Progression, deren erstes Glied und Differenz ganze Zahlen ohne gemeinschaftlichen Faktor sind, unendlich viele Primzahlen enthält\u003c/a\u003e, Abh. Preuss. Akad. Wiss. (1837) 45 -81; Werke I, 313-342.",
				"M. Ohm, \u003ca href=\"https://books.google.de/books?id=sO09AAAAcAAJ\u0026amp;printsec=frontcover\"\u003eDe nonnullis seriebus infinitis summandis\u003c/a\u003e, Berolini, 1839, Typis Trowitzschii et filli.",
				"A. Pringsheim, \u003ca href=\"http://www.digizeitschriften.de/dms/img/?PID=GDZPPN002247755\"\u003eUeber die Werthveränderungen bedingt convergenter Reihen und Producte\u003c/a\u003e, Math. Ann. 22 (1838) 455-503."
			],
			"formula": [
				"a(n) = numerator(s(n)) with s(n) = Sum_{k=0..n} c(k), where c(k) = 3/(4*k+3), 3/(4*k+5), -3/(2*(k+1)) if k == 0, 1, 2 (mod 3), respectively."
			],
			"example": [
				"The first fractions s(n) (in lowest terms) are 1, 4/3, 5/6, 31/30, 247/210, 389/420, 1307/1260, 15637/13860, 13327/13860, 187111/180180, 199123/180180, 353201/360360, ...",
				"The values s(10^n), for n=0..6, are (Maple 10 digits) [1.333333333, 1.105133755, 1.047114258, 1.040469694, 1.039795760, 1.039728271, 1.039721521], to be compared with 3*log(2)/2 (approximately 1.039720771)."
			],
			"mathematica": [
				"Table[Numerator@ Sum[Which[Mod[k, 3] == 0, 3/(4 k + 3), Mod[k, 3] == 1, 3/(4 k + 5), True, -3/(2 (k + 1))], {k, 0, n} ], {n, 0, 27}] (* _Michael De Vlieger_, Jul 26 2016 *)"
			],
			"program": [
				"(PARI) lista(nn) = {my(s = 0); for (k=0, nn, if (k%3==2, t = -3/(2*(k+1)), if (k%3==1, t = 3/(4*k+5), t = 3/(4*k+3))); s += t; print1(numerator(s), \", \"););} \\\\ _Michel Marcus_, Sep 13 2015"
			],
			"xref": [
				"Cf. A262022 (denominator), A262023, A058313, A058312, A002162."
			],
			"keyword": "nonn,frac,easy",
			"offset": "0,2",
			"author": "_Wolfdieter Lang_, Sep 08 2015",
			"references": 3,
			"revision": 41,
			"time": "2016-07-27T03:10:20-04:00",
			"created": "2015-09-14T07:51:09-04:00"
		}
	]
}