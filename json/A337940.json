{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A337940",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 337940,
			"data": "6,9,10,12,14,15,15,18,20,21,18,22,25,27,28,21,26,30,33,35,36,24,30,35,39,42,44,45,27,34,40,45,49,52,54,55,30,38,45,51,56,60,63,65,66,33,42,50,57,63,68,72,75,77,78,36,46,55,63,70,76,81,85,88,90,91",
			"name": "Triangle read by rows: T(n, k) = T(n+2) - T(n-k), with the triangular numbers T = A000217, for n \u003e= 1, k = 1, 2, ..., n.",
			"comment": [
				"This number triangle results from the array A(n, m) = T(n+m+1) - T(n-1), with T = A000217, for n, m \u003e= 1. For this array see the example by _Bob Selcoe_, in A111774 (but with rows continued). The present triangle is obtained by reading the array by upwards antidiagonals: T(n, k) = A(n+1-k, k). See also the Jul 09 2019 comment by _Ralf Steiner_ with the formula c_k(n) (rows k \u003e= 1, columns n \u003e= 3), rewritten for A(n, m) = (m+2)*(2*n+m+1)/2, leading to T(n, k) = (k+2)*(2*n-k+3)/2.",
				"Therefore this triangle is related to the problem of giving the numbers which are sums of at least three consecutive positive integers given as sequence A111774. It allows us to find the multiplicities for the numbers of A111774. They are given in A338428(n).",
				"To obtain the multiplicity for number N (\u003e= 6) from A111774 one has to consider only the triangle rows n = 1, 2, ..., floor((N-3)/3).",
				"The row reversed triangle, considered by _Bob Selcoe_ in A111774, is T(n, n-k+1) = T(n+2) - T(k-1), for n \u003e= 1, and k=1, 2, ..., n.",
				"This triangle contains no odd prime numbers and no exact powers 2^m, for m \u003e= 0. This can be seen by considering the diagonal sequences D(d, k), for d \u003e= 1, k \u003e= 1 or the row sequences of the array A(n, m), for n \u003e= 1 and m \u003e= 1. The result is A(r+1, s-2) = s*(s + 2*r + 1)/2, for r \u003e= 0 and s \u003e= 3 (from the g.f. of the diagonals of T given below). This is also given in the Jul 09 2019 comment by _Ralf Steiner_ in A111774. Therefore A(r+1, s-2) is a product of two numbers \u003e= 2, hence not a prime. And in both cases (i) s/2 integer or (ii) (s + 2*r + 1)/2 integer not both numbers can be powers of 2 by simple parity arguments.",
				"The previous comment means that each T(n, k) has at least one odd prime as a proper divisor.",
				"A number N appears in this triangle, or in A111774, if and only if floor(N/2) - delta(N) \u003e= 1, where delta(N) = A055034(N). For the sequence b(n) := floor(n/2) - delta(n), for n \u003e= 2, see A219839(n), b(1) = -1. See a W. Lang comment in A111774 for the proof."
			],
			"formula": [
				"T(n, k) = ((n+2)*(n+3) - (n-k)*(n-k+1))/2, for n \u003e= 1 and k = 1, 2, ..., n (see the name).",
				"T(n, k) = (k+2)*(2*n-k+3)/2 (factorized).",
				"G.f. columns k = 2*j+1, for j \u003e= 0: Go(j, x) = x^(2*j+1)*(2*j+3)*(j+2 - (j+1)*x)/(1-x)^2,",
				"G.f. columns k = 2*j, for j \u003e= 1: Ge(j, x) = x^(2*j)*(j+1)*(2*j+3 - (2*j+1)*x)/(1-x)^2.",
				"G.f. row polynomials: G(z,x) = z*x*(1 + z*x)^3*{3*(2-z) - (8-3*z)*(z*x) + (3-z)*(z*x)^2}/((1 - z)^2*(1 - (z*x)^2)^3).",
				"G.f. diagonals d \u003e= 1: GD(d, x) = ((d+1)*3 - (5*d+3)*x + (2*d+1)*x^2)/(1-x)^3.",
				"G.f. of GD(d, x): GGD(z,x) = (6-8*x+3*x^2 - (3-3*x+x^2)*z)/((1-x)^3*(1-z)^2)."
			],
			"example": [
				"The triangle T(n, k) begins:",
				"n \\ k  1  2  3  4  5   6   7   8   9  10  11  12  13  14  15 ...",
				"1:     6",
				"2:     9 10",
				"3:    12 14 15",
				"4:    15 18 20 21",
				"5:    18 22 25 27 28",
				"6:    21 26 30 33 35  36",
				"7:    24 30 35 39 42  44  45",
				"8:    27 34 40 45 49  52  54  55",
				"9:    30 38 45 51 56  60  63  65  66",
				"10:   33 42 50 57 63  68  72  75  77  78",
				"11:   36 46 55 63 70  76  81  85  88  90  91",
				"12:   39 50 60 69 77  84  90  95  99 102 104 105",
				"13:   42 54 65 75 84  92  99 105 110 114 117 119 120",
				"14:   45 58 70 81 91 100 108 115 121 126 130 133 135 136",
				"15:   48 62 75 87 98 108 117 125 132 138 143 147 150 152 153",
				"...",
				"N = 15 appears precisely twice from the sums 4+5+6 = A(4, 1) = T(4, 1), and (1+2+3)+4+5 = A(1, 3) = T(3, 3), i.e., with a sum of 3 and 5 consecutive positive integers.",
				"N = 42 appears three times from the sums 13+14+15 = A(13, 1) = T(13, 1), 9+10+11 +12 = A(9, 2) = T(10, 2), 3+4+5+6+7+8+9 = A(3, 5) = T(7, 5); i.e., 42 can be written as a sum of 3, 4 and 7 consecutive positive integers."
			],
			"mathematica": [
				"Flatten[Table[((n+2)*(n+3)-(n-k)*(n-k+1))/2,{n,11},{k,n}]] (* _Stefano Spezia_, Nov 24 2020 *)"
			],
			"xref": [
				"Cf. A055034, A111774, A338428 (multiplicities), A219839.",
				"For columns k = 1, 2, ..., 10 see A008585, A016825, A008587, A016945, A008589, A017113, A008591, A017329, A008593, A017593.",
				"For diagonals d = 1, 2, ..., 10 see A000217, A000096, A055998, A055999, A056000, A056115, A056119 , A056121, A056126, A051942."
			],
			"keyword": "nonn,tabl,easy,changed",
			"offset": "1,1",
			"author": "_Wolfdieter Lang_, Nov 23 2020",
			"references": 4,
			"revision": 19,
			"time": "2022-01-07T19:36:09-05:00",
			"created": "2020-11-28T20:05:49-05:00"
		}
	]
}