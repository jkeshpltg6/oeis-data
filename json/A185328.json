{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A185328",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 185328,
			"data": "1,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,2,2,3,3,4,4,5,5,7,7,9,10,12,13,16,17,21,23,27,30,36,39,46,51,60,66,77,85,99,110,126,140,162,179,205,228,260,289,329,365,415,461,521,579,655,726,818,909,1022,1134,1273,1411",
			"name": "Number of partitions of n with parts \u003e= 8.",
			"comment": [
				"a(n) is also the number of not necessarily connected 2-regular graphs on n-vertices with girth at least 8 (all such graphs are simple). The integer i corresponds to the i-cycle; addition of integers corresponds to disconnected union of cycles.",
				"By removing a single part of size 8, an A026801 partition of n becomes an A185328 partition of n - 8. Hence this sequence is essentially the same as A026801."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A185328/b185328.txt\"\u003eTable of n, a(n) for n = 0..2000\u003c/a\u003e",
				"Jason Kimberley, \u003ca href=\"/wiki/User:Jason_Kimberley/E_k-reg_girth_ge_g_index\"\u003eIndex of sequences counting not necessarily connected k-regular simple graphs with girth at least g\u003c/a\u003e"
			],
			"formula": [
				"G.f.: Product_{m\u003e=8} 1/(1-x^m).",
				"a(n) = p(n) - p(n-1) - p(n-2) + p(n-5) + p(n-7) + p(n-8) - p(n-10) - p(n-11) - 2*p(n-12) + 2*p(n-16) + p(n-17) + p(n-18) - p(n-20) - p(n-21) - p(n-23) + p(n-26) + p(n-27) - p(n-28) where p(n)=A000041(n). - _Shanzhen Gao_",
				"This sequence is the Euler transformation of A185118.",
				"a(n) ~ exp(Pi*sqrt(2*n/3)) * 35*Pi^7 / (18*sqrt(2)*n^(9/2)). - _Vaclav Kotesovec_, Jun 02 2018",
				"G.f.: Sum_{k\u003e=0} x^(8*k) / Product_{j=1..k} (1 - x^j). - _Ilya Gutkovskiy_, Nov 28 2020"
			],
			"maple": [
				"N:= 100: # for a(0)..a(N)",
				"g:= mul(1/(1-x^m),m=8..N):",
				"S:= series(g,x,N+1):",
				"seq(coeff(S,x,n),n=0..N); # _Robert Israel_, Dec 19 2017"
			],
			"mathematica": [
				"CoefficientList[Series[1/QPochhammer[x^8, x], {x,0,75}], x] (* _G. C. Greubel_, Nov 03 2019 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^70)); Vec(1/prod(m=0,80, 1-x^(m+8))) \\\\ _G. C. Greubel_, Nov 03 2019",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Integers(), 70); Coefficients(R!( 1/(\u0026*[1-x^(m+8): m in [0..80]]) )); // _G. C. Greubel_, Nov 03 2019",
				"(Sage)",
				"def A185328_list(prec):",
				"    P.\u003cx\u003e = PowerSeriesRing(ZZ, prec)",
				"    return P( 1/product((1-x^(m+8)) for m in (0..80)) ).list()",
				"A185328_list(70) # _G. C. Greubel_, Nov 03 2019"
			],
			"xref": [
				"Not necessarily connected 2-regular graphs with girth at least g [partitions into parts \u003e= g]: A026807 (triangle); chosen g: A000041 (g=1 -- multigraphs with loops allowed), A002865 (g=2 -- multigraphs with loops forbidden), A008483 (g=3), A008484 (g=4), A185325(g=5), A185326 (g=6), A185327 (g=7), this sequence (g=8), A185329 (g=9).",
				"Not necessarily connected 2-regular graphs with girth exactly g [partitions with smallest part g]: A026794 (triangle); chosen g: A002865 (g=2), A026796 (g=3), A026797 (g=4), A026798 (g=5), A026799 (g=6), A026800(g=7), A026801 (g=8), A026802 (g=9), A026803 (g=10)."
			],
			"keyword": "nonn,easy",
			"offset": "0,17",
			"author": "_Jason Kimberley_, Jan 31 2012",
			"references": 20,
			"revision": 33,
			"time": "2020-11-29T05:38:28-05:00",
			"created": "2012-01-31T12:09:29-05:00"
		}
	]
}