{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A213536",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 213536,
			"data": "14,21,22,23,24,25,26,39,40,45,48,49,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,111,112,113,114,115,116,117,118,119,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143",
			"name": "Cousin prime recurrence sequence: a(1)=14, and for n\u003e1, a(n) = a(n-1) + gcd(n+5, a(n-1)), if n is even, else a(n) = a(n-1) + gcd(n+1, a(n-1)).",
			"comment": [
				"Conjecture: Record differences a(n)-a(n-1) (A213537) are a strict subset of the smaller of cousin primes (A023200). (Cousin primes differ by 4.)",
				"Conjecture: Record differences are an infinite sequence. It is widely believed there are infinitely many cousin primes. (Similarly, by Dickson's conjecture and second Hardy-Littlewood conjecture, there are infinitely many pairs of (not necessarily consecutive) primes (p,p+2k) for each natural number k.)",
				"Conjecture: The following pattern makes sequences for every (necessarily even) difference (slight change for 2). For difference d, p is first prime \u003ed that is the smaller of a prime pair (p,p+d). a(1)=2p and a(n)=gcd(n+p-2,a(n-1)) for even n, else gcd(n+p-2-d,a(n-1))."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A213536/b213536.txt\"\u003eTable of n, a(n) for n = 1..1273\u003c/a\u003e",
				"G. H. Hardy and J. E. Littlewood, \u003ca href=\"https://dx.doi.org/10.1007/BF02403921\"\u003eSome problems of 'Partitio numerorum'; III: on the expression of a number as a sum of primes\u003c/a\u003e, Acta Mathematica, Vol. 44, pp. 1-70, 1923.",
				"E. S. Rowland, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL11/Rowland/rowland21.html\"\u003eA natural prime-generating recurrence\u003c/a\u003e, Journal of Integer Sequences, Vol. 11 (2008), Article 08.2.8.",
				"Pascal Sebah and Xavier Gourdon, \u003ca href=\"http://numbers.computation.free.fr/Constants/Primes/twin.html\"\u003eIntroduction to twin primes and Brun’s constant\u003c/a\u003e",
				"V. Shevelev, \u003ca href=\"http://arXiv.org/abs/0910.4676v8\"\u003eA new generator of primes based on the Rowland idea\u003c/a\u003e",
				"V. Shevelev, \u003ca href=\"http://arXiv.org/abs/0911.5478v11\"\u003eThree theorems on twin primes\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Dickson\u0026#39;s_conjecture\"\u003eDickson's conjecture\u003c/a\u003e"
			],
			"maple": [
				"A213536 := proc(n)",
				"    option remember;",
				"    if n = 1 then",
				"        14;",
				"    elif type(n,'even') then",
				"        procname(n-1)+gcd(n+5,procname(n-1)) ;",
				"    else",
				"        procname(n-1)+gcd(n+1,procname(n-1)) ;",
				"    end if;",
				"end proc: # _R. J. Mathar_, Jul 20 2012"
			],
			"mathematica": [
				"nxt[{n_,a_}]:={n+1,If[OddQ[n],a+GCD[a,n+6],a+GCD[a,n+2]]}; Transpose[ NestList[nxt,{1,14},60]][[2]] (* _Harvey P. Dale_, Jun 22 2013 *)"
			],
			"xref": [
				"Cf. A213537 (record differences)."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Joseph Benstock_, Jun 27 2012",
			"references": 2,
			"revision": 27,
			"time": "2018-07-04T05:47:30-04:00",
			"created": "2012-06-28T18:39:42-04:00"
		}
	]
}