{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003763",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3763,
			"data": "1,6,1072,4638576,467260456608,1076226888605605706,56126499620491437281263608,65882516522625836326159786165530572,1733926377888966183927790794055670829347983946,1020460427390768793543026965678152831571073052662428097106",
			"name": "Number of (undirected) Hamiltonian cycles on 2n X 2n square grid of points.",
			"comment": [
				"Orientation of the path is not important; you can start going either clockwise or counterclockwise.",
				"The number is zero for a 2n+1 X 2n+1 grid (but see A222200).",
				"These are also called \"closed rook tours\"."
			],
			"reference": [
				"F. Faase, On the number of specific spanning subgraphs of the graphs G X P_n, Ars Combin. 49 (1998), 129-154."
			],
			"link": [
				"Artem M. Karavaev and N. J. A. Sloane, \u003ca href=\"/A003763/b003763.txt\"\u003eTable of n, a(n) for n=1..13\u003c/a\u003e [First 11 terms from _Artem M. Karavaev_, Sep 29 2010; a(12) and a(13) from Pettersson, 2014, added by _N. J. A. Sloane_, Jun 05 2015]",
				"F. Faase, \u003ca href=\"http://www.iwriteiam.nl/Cpaper.zip\"\u003eOn the number of specific spanning subgraphs of the graphs G X P_n\u003c/a\u003e, Preliminary version of paper that appeared in Ars Combin. 49 (1998), 129-154.",
				"J. L. Jacobsen, \u003ca href=\"http://dx.doi.org/10.1088/1751-8113/40/49/003\"\u003eExact enumeration of Hamiltonian circuits, walks and chains in two and three dimensions\u003c/a\u003e, J. Phys. A: Math. Theor. 40 (2007) 14667-14678",
				"Artem M. Karavaev, \u003ca href=\"https://web.archive.org/web/20161024010518/http://flowproblem.ru/cycles/hamilton-cycles\"\u003eHamilton Cycles\u003c/a\u003e.",
				"Ville H. Pettersson, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v21i4p7\"\u003eEnumerating Hamiltonian Cycles\u003c/a\u003e, The Electronic Journal of Combinatorics, Volume 21, Issue 4, 2014.",
				"Ville Pettersson, \u003ca href=\"https://aaltodoc.aalto.fi/handle/123456789/17688\"\u003eGraph Algorithms for Constructing and Enumerating Cycles and Related Structures\u003c/a\u003e, Dissertation, Aalto, Finland, 2015.",
				"A. Pönitz, \u003ca href=\"http://dx.doi.org/10.1016/S0378-4754(99)00052-X\"\u003eComputing invariants in graphs of small bandwidth\u003c/a\u003e, Mathematics in Computers and Simulation, 49(1999), 179-191",
				"A. P\u0026ouml;nitz, \u003ca href=\"http://www.qucosa.de/fileadmin/data/qucosa/documents/2080/MathematikPXnitzAndrX756952.pdf\"\u003e\u0026Uuml;ber eine Methode zur Konstruktion...\u003c/a\u003e PhD Thesis (2004) C.3.",
				"T. G. Schmalz, G. E. Hite and D. J. Klein, \u003ca href=\"http://dx.doi.org/10.1088/0305-4470/17/2/029\"\u003eCompact self-avoiding circuits on two-dimensional lattices\u003c/a\u003e, J. Phys. A 17 (1984), 445-453.",
				"N. J. A. Sloane, \u003ca href=\"/A003763/a003763.jpg\"\u003eIllustration of a(2) = 6\u003c/a\u003e",
				"Peter Tittmann, \u003ca href=\"http://www.htwm.de/~peter/research/enumeration.html\"\u003eEnumeration in graphs: counting Hamiltonian cycles\u003c/a\u003e [Broken link?]",
				"Peter Tittmann, \u003ca href=\"http://web.archive.org/web/20101127064650/https://www.staff.hs-mittweida.de/~peter/research/enumeration.html\"\u003eEnumeration in graphs: counting Hamiltonian cycles\u003c/a\u003e [Backup copy of top page only, on the Internet Archive]",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GridGraph.html\"\u003eGrid Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HamiltonianCycle.html\"\u003eHamiltonian Cycle\u003c/a\u003e",
				"Ed Wynn, \u003ca href=\"http://arxiv.org/abs/1402.0545\"\u003eEnumeration of nonisomorphic Hamiltonian cycles on square grid graphs\u003c/a\u003e, arXiv preprint arXiv:1402.0545 [math.CO], 2014.",
				"\u003ca href=\"/index/Gra#graphs\"\u003eIndex entries for sequences related to graphs, Hamiltonian\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A321172(2n,2n). - _Robert FERREOL_, Apr 01 2019"
			],
			"example": [
				"a(1) = 1 because there is only one such path visiting all nodes of a square."
			],
			"xref": [
				"Other enumerations of Hamiltonian cycles on a square grid: A120443, A140519, A140521, A222200, A222201."
			],
			"keyword": "nonn,walk",
			"offset": "1,2",
			"author": "_Jeffrey Shallit_, Feb 14 2002",
			"ext": [
				"Two more terms from Andre Poenitz [André Pönitz] and Peter Tittmann (poenitz(AT)htwm.de), Mar 03 2003",
				"a(8) from Herman Jamke (hermanjamke(AT)fastmail.fm), Nov 21 2006",
				"a(9) and a(10) from Jesper L. Jacobsen (jesper.jacobsen(AT)u-psud.fr), Dec 12 2007"
			],
			"references": 42,
			"revision": 83,
			"time": "2021-02-17T10:52:19-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}