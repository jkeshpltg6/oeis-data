{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A061777",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 61777,
			"data": "1,4,10,22,40,70,112,178,268,406,592,874,1252,1822,2584,3730,5260,7558,10624,15226,21364,30574,42856,61282,85852,122710,171856,245578,343876,491326,687928,982834,1376044,1965862,2752288,3931930,5504788",
			"name": "Start with a single triangle; at n-th generation add a triangle at each vertex, allowing triangles to overlap; sequence gives total population of triangles at n-th generation.",
			"comment": [
				"From the definition, assign label value \"1\" to an origin triangle; at n-th generation add a triangle at each vertex. Each non-overlapping triangle will have the same label value as that of the predecessor triangle to which it is connected; for the overlapping ones, the label value will be the sum of the label values of predecessors. a(n) is the sum of all label values at the n-th generation. The triangle count is A005448. See illustration. For n \u003e= 1, (a(n) - a(n-1))/3 is A027383. - _Kival Ngaokrajang_, Sep 05 2014"
			],
			"reference": [
				"R. Reed, The Lemming Simulation Problem, Mathematics in School, 3 (#6, Nov. 1974), front cover and pp. 5-6."
			],
			"link": [
				"Kival Ngaokrajang, \u003ca href=\"/A061777/a061777.pdf\"\u003eIllustration of initial terms\u003c/a\u003e",
				"R. Reed, \u003ca href=\"/A005448/a005448_1.pdf\"\u003eThe Lemming Simulation Problem\u003c/a\u003e, Mathematics in School, 3 (#6, Nov. 1974), front cover and pp. 5-6. [Scanned photocopy of pages 5, 6 only, with annotations by R. K. Guy and N. J. A. Sloane]",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,1,-4,2)."
			],
			"formula": [
				"If n even, 21*2^(n/2)-6*n-20; if n odd, 30*2^((n-1)/2)-6*(n-1)-26.",
				"a(n) = 2*a(n-1)+a(n-2)-4*a(n-3)+2*a(n-4). G.f.: (1+2*x)*(1+x^2)/((1-x)^2*(1-2*x^2)). - _Colin Barker_, May 08 2012",
				"a(n) = -20 - 6*n + (21+15*sqrt(2))*sqrt(2)^(n-2) + (21-15*sqrt(2))*(-sqrt(2))^(n-2). a(n) = 2*a(n-2) + ((3*n-2)/(3*n-5))*(a(n-1)-2*a(n-3)). - _Robert Israel_, Sep 14 2014"
			],
			"maple": [
				"seq(`if`(n::even, 21*2^(n/2) - 6*n-20, 30*2^((n-1)/2)-6*n-20),n=0..100); # _Robert Israel_, Sep 14 2014"
			],
			"mathematica": [
				"Table[If[EvenQ[n],21 2^(n/2)-6n-20,30 2^((n-1)/2)-6(n-1)-26],{n,0,40}] (* _Harvey P. Dale_, Nov 06 2011 *)"
			],
			"program": [
				"(PARI) a(n)=if(n%2, 30, 21)\u003c\u003c(n\\2) - 6*n - 20 \\\\ _Charles R Greathouse IV_, Sep 19 2014"
			],
			"xref": [
				"Partial sums of A061776.",
				"Cf. A005448, A027383. - _Kival Ngaokrajang_, Sep 19 2014"
			],
			"keyword": "nonn,nice,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, _R. K. Guy_, Jun 23 2001",
			"ext": [
				"Corrected by _T. D. Noe_, Nov 08 2006"
			],
			"references": 16,
			"revision": 49,
			"time": "2017-11-27T12:22:48-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}