{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A132276",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 132276,
			"data": "1,1,1,3,2,1,6,7,3,1,16,18,12,4,1,40,53,37,18,5,1,109,148,120,64,25,6,1,297,430,369,227,100,33,7,1,836,1244,1146,760,385,146,42,8,1,2377,3656,3519,2518,1391,606,203,52,9,1,6869,10796,10839,8188,4900,2346,903,272",
			"name": "Triangle read by rows: T(n,k) is the number of paths in the first quadrant from (0,0) to (n,k), consisting of steps U=(1,1), D=(1,-1), h=(1,0) and H=(2,0) (0\u003c=k\u003c=n).",
			"comment": [
				"Mirror image of A059397. - _Emeric Deutsch_, Aug 18 2007",
				"Row sums yield A059398.",
				"Riordan matrix (g(x),x*g(x)), where g(x) = (1-x-x^2-sqrt(1-2*x-5*x^2+2*x^3+x^4))/(2*x^2). - _Emanuele Munarini_, May 05 2011"
			],
			"reference": [
				"Lin Yang and S.-L. Yang, The parametric Pascal rhombus. Fib. Q., 57:4 (2019), 337-346.",
				"Sheng-Liang Yang et al., The Pascal rhombus and Riordan arrays, Fib. Q., 56:4 (2018), 337-347. See Fig. 1."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A132276/b132276.txt\"\u003eTable of n, a(n) for the first 50 rows, flattened\u003c/a\u003e",
				"W. F. Klostermeyer, M. E. Mays, L. Soltes and G. Trapp, \u003ca href=\"http://www.fq.math.ca/Scanned/35-4/klostermeyer.pdf\"\u003eA Pascal rhombus\u003c/a\u003e, Fibonacci Quarterly, 35 (1997), 318-328."
			],
			"formula": [
				"T(n,0) = A128720(n).",
				"G.f.: G(t,z) = g/(1-t*z*g), where g = 1 +z*g +z^2*g +z^2*g^2 or g = c(z^2/(1-z-z^2)^2)/(1-z-z^2), where c = ((1-sqrt(1-4*z))/(2*z) is the Catalan function.",
				"T(n,k) = T(n-1,k-1) + T(n-1,k) + T(n-1,k+1) + T(n-2,k). - _Emeric Deutsch_, Aug 18 2007",
				"Column k has g.f. z^k*g^(k+1), where g = 1 +z*g +z^2*g +z^2*g^2 = (1 -z-z^2 -sqrt((1+z-z^2)*(1-3*z-z^2)))/(2*z^2).",
				"T(n,k) = Sum_{i=0..(n-k)/2} (binomial(2*i+k,i)*(k+1)/(i+k+1)* Sum_{j=0..(n-k-2*i)} binomial(i+j+k,i+k)*binomial(j,n-k-2*i-j). - _Emanuele Munarini_, May 05 2011"
			],
			"example": [
				"Triangle begins:",
				"1,",
				"1,1,",
				"3,2,1,",
				"6,7,3,1,",
				"16,18,12,4,1,",
				"40,53,37,18,5,1,",
				"109,148,120,64,25,6,1,",
				"T(3,2)=3 because we have UUh, UhU and hUU."
			],
			"maple": [
				"g:=((1-z-z^2-sqrt((1+z-z^2)*(1-3*z-z^2)))*1/2)/z^2: G:=simplify(g/(1-t*z*g)): Gser:=simplify(series(G,z=0,13)): for n from 0 to 10 do P[n]:=sort(coeff(Gser, z,n)) end do: for n from 0 to 10 do seq(coeff(P[n], t, j), j = 0 .. n) end do; # yields sequence in triangular form"
			],
			"mathematica": [
				"Flatten[Table[Sum[Binomial[2i+k,i(k+1)/(i+k+1)*Sum[Binomial[i+j+k,i+k]* Binomial[j,n-k-2i-j],{j,0,n-k-2i}],{i,0,(n-k)/2}],{n,0,15},{k,0,n}]] (* _Emanuele Munarini_, May 05 2011 *)",
				"c[x_] := (1 - Sqrt[1 - 4*x])/(2*x); g[z_] := c[z^2/(1 - z - z^2)^2]/(1 - z - z^2); G[t_, z_] := g[z]/(1 - t*z*g[z]); CoefficientList[ CoefficientList[Series[G[t, x], {x, 0, 49}, {t, 0, 49}], x], t]//Flatten (* _G. C. Greubel_, Dec 02 2017 *)"
			],
			"program": [
				"(Maxima) create_list(sum(binomial(2*i+k,i) * (k+1)/(i+k+1) * sum(binomial(i+j+k,i+k) * binomial(j,n-k-2*i-j),j,0,n-k-2*i), i,0,(n-k)/2), n,0,15, k,0,n); /* _Emanuele Munarini_, May 05 2011 */",
				"(PARI) for(n=0,10, for(k=0,n, print1(sum(i=0, (n-k)/2, (binomial(2*i+k,i) *(k+1)/(i+k+1)*sum(j=0, (n-k-2*i), binomial(i+j+k,i+k)*binomial(j,n-k-2*i-j)))), \", \"))) \\\\ _G. C. Greubel_, Nov 29 2017"
			],
			"xref": [
				"Cf. A059397, A128720 (the leading diagonal).",
				"Cf. A059398."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Emeric Deutsch_, Aug 16 2007, Sep 03 2007",
			"references": 6,
			"revision": 26,
			"time": "2020-01-15T18:39:08-05:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}