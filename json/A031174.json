{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A031174",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 31174,
			"data": "117,252,480,132,231,1100,1020,880,855,2475,2035,2295,5796,748,6300,4576,4888,1672,9152,9828,3536,1100,4901,1881,11753,8820,10560,16929,15400,14160,18368,17157,24080,17472,15232,23751,10725,13728",
			"name": "Intermediate edge b of smallest (measured by the longest edge) primitive Euler bricks (a, b, c, sqrt(a^2 + b^2), sqrt(b^2 + c^2), sqrt(a^2 + c^2) are integers).",
			"comment": [
				"Primitive means that gcd(a,b,c) = 1.",
				"See A031173 for a list of the 3356 primitive bricks with c \u003c b \u003c a \u003c 5*10^8. - _Giovanni Resta_, Mar 23 2014"
			],
			"reference": [
				"Calculated by F. Helenius (fredh(AT)ix.netcom.com)."
			],
			"link": [
				"Giovanni Resta, \u003ca href=\"/A031174/b031174.txt\"\u003eTable of n, a(n) for n = 1..3556\u003c/a\u003e",
				"R. R. Gallyamov, I. R. Kadyrov, D. D. Kashelevskiy, \u003ca href=\"https://arxiv.org/abs/1601.00636\"\u003eA fast modulo primes algorithm for searching perfect cuboids and its implementation\u003c/a\u003e, arXiv preprint arXiv:1601.00636 [math.NT], 2016.",
				"A. A. Masharov and R. A. Sharipov, \u003ca href=\"http://arxiv.org/abs/1504.07161\"\u003eA strategy of numeric search for perfect cuboids in the case of the second cuboid conjecture\u003c/a\u003e, arXiv preprint, 2015.",
				"J. Ramsden and H. Sharipov, \u003ca href=\"http://arxiv.org/abs/1207.6764\"\u003eInverse problems associated with perfect cuboids\u003c/a\u003e, arXiv preprint arXiv:1207.6764, 2012. - From _N. J. A. Sloane_, Dec 23 2012",
				"J. Ramsden and H. Sharipov, \u003ca href=\"http://arxiv.org/abs/1208.1859\"\u003eOn singularities of the inverse problems associated with perfect cuboids\u003c/a\u003e, arXiv preprint arXiv:1208.1859, 2012. - From _N. J. A. Sloane_, Dec 25 2012",
				"Ruslan Sharipov, \u003ca href=\"http://arxiv.org/abs/1108.5348\"\u003ePerfect cuboids and irreducible polynomials\u003c/a\u003e, arXiv:1108.5348, 2011",
				"Ruslan Sharipov, \u003ca href=\"http://arxiv.org/abs/1109.2534\"\u003eA note on the first cuboid conjecture\u003c/a\u003e, arXiv:1109.2534, 2011",
				"Ruslan Sharipov, \u003ca href=\"http://arxiv.org/abs/1201.1229\"\u003eA note on the second cuboid conjecture. Part I\u003c/a\u003e, arXiv:1201.1229, 2012",
				"Ruslan Sharipov, \u003ca href=\"http://arxiv.org/abs/1205.3135\"\u003ePerfect cuboids and multisymmetric polynomials\u003c/a\u003e, arXiv preprint arXiv:1205.3135, 2012. - From _N. J. A. Sloane_, Oct 22 2012",
				"Ruslan Sharipov, \u003ca href=\"http://arxiv.org/abs/1206.6769\"\u003eOn an ideal of multisymmetric polynomials associated with perfect cuboids\u003c/a\u003e, arXiv preprint arXiv:1206.6769, 2012. - From _N. J. A. Sloane_, Dec 17 2012",
				"Ruslan Sharipov, \u003ca href=\"http://arxiv.org/abs/1207.2102\"\u003eOn the equivalence of cuboid equations and their factor equations\u003c/a\u003e, arXiv preprint arXiv:1207.2102, 2012. - From _N. J. A. Sloane_, Dec 22 2012",
				"Ruslan Sharipov, \u003ca href=\"http://arxiv.org/abs/1207.4081\"\u003eA biquadratic Diophantine equation associated with perfect cuboids\u003c/a\u003e, arXiv preprint arXiv:1207.4081, 2012. - From _N. J. A. Sloane_, Dec 23 2012",
				"Ruslan Sharipov, \u003ca href=\"http://arxiv.org/abs/1208.0308\"\u003eOn a pair of cubic equations associated with perfect cuboids\u003c/a\u003e, arXiv preprint arXiv:1208.0308, 2012. - From _N. J. A. Sloane_, Dec 23 2012",
				"Ruslan Sharipov, \u003ca href=\"http://arxiv.org/abs/1208.1227\"\u003eOn two elliptic curves associated with perfect cuboids\u003c/a\u003e, arXiv preprint arXiv:1208.1227, 2012. - From _N. J. A. Sloane_, Dec 24 2012",
				"Ruslan Sharipov, \u003ca href=\"http://arxiv.org/abs/1505.02745\"\u003eAsymptotic estimates for roots of the cuboid characteristic equation in the linear region\u003c/a\u003e, arXiv preprint, 2015.",
				"Ruslan Sharipov, \u003ca href=\"http://arxiv.org/abs/1505.00724\"\u003eReverse asymptotic estimates for roots of the cuboid characteristic equation in the case of the second cuboid conjecture\u003c/a\u003e, arXiv preprint, 2015.",
				"Ruslan Sharipov, \u003ca href=\"http://arxiv.org/abs/1507.01861\"\u003eA note on invertible quadratic transformations of the real plane,\u003c/a\u003e arXiv preprint arXiv:1507.01861, 2015",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/EulerBrick.html\"\u003eEuler Brick\u003c/a\u003e",
				"\u003ca href=\"/index/Br#bricks\"\u003eIndex entries for sequences related to bricks\u003c/a\u003e"
			],
			"xref": [
				"Cf. A031173, A031175."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Eric W. Weisstein_",
			"references": 18,
			"revision": 59,
			"time": "2020-02-02T04:16:14-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}