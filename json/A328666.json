{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A328666",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 328666,
			"data": "0,1,2,2,3,5,4,6,4,10,5,6,6,17,13,8,7,18,8,22,10,26,9,42,6,37,12,18,10,70,11,40,29,50,25,20,12,65,20,24,13,105,14,54,34,82,15,32,8,38,53,38,16,78,34,114,34,101,17,30,18,122,12,48,15",
			"name": "A recursively defined integer-valued function of integer multisets.",
			"comment": [
				"For singletons {k}, F({k}) = k. For multisets {k_1,...,k_r} with r\u003e1, F is defined recursively by",
				"F({k_1,...,k_r}) = min F({k'_1,...,k'_{r'}})*F({k\"_1,...,k\"_{r-r'}})*K/g, where the minimum is taken over all 2-partitions",
				"{k_1,...,k_r} = {k'_1,...,k'_{r'}} union {k\"_1,...,k\"_{r-r'}}, where 1 \u003c= r' \u003c r.",
				"Here K = Sum_{i=1..r} {k_i}^2 and g = gcd(K\"*k'_1,...,K\"*k'_{r'},K'*k\"_1,...,K'*k\"_{r-r'}), where K' = Sum_{i=1..r'} {k'_i}^2 and K\" = Sum_{i=1..(r-r')} {k\"_i}^2.",
				"The function F is then encoded as an integer sequence by a(n)= F({k_1,..,k_r}), where n=p_{k_1}p_{k_2}..p_{k_r},  p_k being the k-th prime (Heinz encoding).",
				"Also a(1)=0.",
				"The significance of this sequence is that for given multiset {k_1,...,k_r} there is an r X r integer matrix with all rows pairwise orthogonal whose top row is {k_1,...,k_r} and whose determinant is F({k_1,...,k_r}).",
				"See the Pinner/Smyth link for the construction of these matrices."
			],
			"link": [
				"Chris Pinner and Chris Smyth, \u003ca href=\"https://www.maths.ed.ac.uk/~chris/papers/MinimalLattices040919.pdf\"\u003eLattices of minimal index in Z^n having an orthogonal basis containing a given basis vector\u003c/a\u003e"
			],
			"example": [
				"For r=2 only allowable 2-partition of {k_1,k_2} is {k_1} union {k_2}, giving K = {k_1}^2+{k_2}^2, K' = {k_1}^2, K\" = {k_2}^2, g = k_1*k_2*gcd(k_1,k_2), n =  p_{k_1}p_{k_2}, F({k_i}) = k_i (i=1,2), and so a(n) = F({k_1,k_2}) = F({k_1})F({k_2})K/g = ({k_1}^2+{k_2}^2)/gcd(k_1,k_2). Thus for example a(10) = a(p_1p_3) = 1^2+3^2 = 10."
			],
			"xref": [
				"Cf. A327267."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Christopher J. Smyth_, Oct 24 2019",
			"references": 1,
			"revision": 7,
			"time": "2019-11-16T14:11:34-05:00",
			"created": "2019-11-16T14:11:34-05:00"
		}
	]
}