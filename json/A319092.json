{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A319092",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 319092,
			"data": "1,1,2,3,4,5,6,1,4,10,20,35,56,70,76,73,60,36,1,6,21,56,126,252,441,684,954,1204,1365,1344,1169,882,540,216,1,8,36,120,330,792,1688,3232,5619,8944,13088,17568,21642,24456,25236,23528,19489,14232,8856,4320,1296,1,10,55,220,715,2002,4970",
			"name": "Triangle read by rows: T(0,0) = 1; T(n,k) = T(n-1, k) + 2*T(n-1, k-1) + 3*T(n-1, k-2) + 4*T(n-1, k-3) + 5*T(n-1, k-4) + 6*T(n-1, k-5)) for k = 0..5*n; T(n,k)=0 for n or k \u003c 0.",
			"comment": [
				"Row n gives the coefficients in the expansion of (1 + 2*x + 3*x^2 + 4*x^3 + 5*x^4 + 6*x^5)^n, where n is a nonnegative integer.",
				"The row sum is s(n)=21^n (see A009965).",
				"In the center-justified triangle, the sum of numbers along \"first layer\" skew diagonals pointing top-right are the coefficients in the expansion of 1/(1 - x - 2*x^2 -3*x^3 - 4*x^4 - 5*x^5 - 6*x^6) and the sum of numbers along \"first layer\" skew diagonals pointing top-left are the coefficients in the expansion of 1/(1 - 6*x - 5*x^2 - 4*x^3 - 3*x^4 - 2*x^5 - x^6), see links."
			],
			"reference": [
				"Shara Lalo and Zagros Lalo, Polynomial Expansion Theorems and Number Triangles, Zana Publishing, 2018, ISBN: 978-1-9995914-0-3."
			],
			"link": [
				"Shara Lalo, \u003ca href=\"/A319092/a319092.pdf\"\u003eTriangle of coefficients in expansions of (1 + 2*x + 3*x^2 + 4*x^3 + 5*x^4 + 6*x^5)^n.\u003c/a\u003e",
				"Shara Lalo, \u003ca href=\"/A319092/a319092_1.pdf\"\u003eFirst layer skew diagonals in center-justified triangle of coefficients in expansion of (1 + 2*x + 3*x^2 + 4*x^3 + 5*x^4 + 6*x^5)^n.\u003c/a\u003e",
				"Shara Lalo, \u003ca href=\"/A319092/a319092_3.pdf\"\u003eA formula for the coefficients in expansions of (1 + 2*x + 3*x^2 + 4*x^3 + 5*x^4 + 6*x^5 )^n.\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = Sum_{i=0..k} Sum_{j=2*i..k} Sum_{q=3*i..k} Sum_{r=4*i..k}(f) for k=0..5*n; f=((2^(k + q - 2*r)*3^(j + r - 2*q)*4^(i + q - 2*j)*5^(j - 2*i)*6^i*n!)/((n - k + r)!*(k + q - 2*r)!*(j + r - 2*q)!*(i + q - 2*j)!*(j - 2*i)!*i!) ); f=0 for (n - k + r)\u003c0 or (k + q - 2*r)\u003c0; (j + r - 2*q)\u003c0 or (i + q - 2*j) \u003c0 or (j - 2*i)\u003c0. A novel formula proven by Shara Lalo and Zagros Lalo. Also see formula in Links section.",
				"G.f.: 1/(1 - x*t- 2*x^2*t - 3*x^3*t - 4*x^4*t - 5*x^5*t - 6*x^6*t)."
			],
			"example": [
				"Triangle begins:",
				"1;",
				"1, 2,  3,  4,   5,   6;",
				"1, 4, 10, 20,  35,  56,  70,  76,  73,  60,    36;",
				"1, 6, 21, 56, 126, 252, 441, 684, 954, 1204, 1365, 1344, 1169, 882, 540, 216;",
				"..."
			],
			"mathematica": [
				"t[n_, k_] := t[n, k] = Sum[(2^(k + q - 2*r)*3^(j + r - 2*q)*4^(i + q - 2*j)*5^(j - 2*i)*6^i*n!)/((n - k + r)!*(k + q - 2*r)!*(j + r - 2*q)!*(i + q -2*j)!*(j - 2*i)!*i!), {i, 0, k}, {j, 2*i, k}, {q, 3*i, k}, {r, 4*i, k}]; Flatten[Table[t[n, k], {n, 0, 5}, {k, 0, 5 n}]]",
				"t[0, 0] = 1; t[n_, k_] :=  t[n, k] =   If[n \u003c 0 || k \u003c 0, 0, t[n - 1, k] + 2 t[n - 1, k - 1] + 3 t[n - 1, k - 2] + 4 t[n - 1, k - 3] + 5 t[n - 1, k - 4] + 6 t[n - 1, k - 5]]; Table[t[n, k], {n, 0, 5}, {k, 0, 5 n}  ]  // Flatten"
			],
			"program": [
				"(PARI) row(n) = Vecrev((1+2*x+3*x^2+4*x^3+5*x^4+6*x^5)^n);",
				"tabl(nn) = for (n=0, nn, print(row(n))); \\\\ _Michel Marcus_, Oct 15 2018"
			],
			"xref": [
				"Cf. A009965, A319093, A319094, A319095."
			],
			"keyword": "tabf,nonn,easy",
			"offset": "0,3",
			"author": "_Shara Lalo_, Oct 01 2018",
			"references": 2,
			"revision": 35,
			"time": "2019-05-12T03:43:21-04:00",
			"created": "2018-11-24T18:10:59-05:00"
		}
	]
}