{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A089231",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 89231,
			"data": "1,1,2,1,6,6,1,12,36,24,1,20,120,240,120,1,30,300,1200,1800,720,1,42,630,4200,12600,15120,5040,1,56,1176,11760,58800,141120,141120,40320,1,72,2016,28224,211680,846720,1693440,1451520,362880",
			"name": "Triangular array A066667 or A008297 unsigned and transposed.",
			"comment": [
				"Row sums: A000262.",
				"T(n, k) is also the number of nilpotent partial one-one bijections (of an n-element set) of height k (height(alpha) = |Im(alpha)|). - _Abdullahi Umar_, Sep 14 2008",
				"T(n, k) is also the number of acyclic directed graphs on n labeled nodes with k-1 edges with all indegrees and outdegrees at most 1. - _Felix A. Pahl_, Dec 25 2012",
				"For n \u003e 1, the n-th derivative of exp(1/x) is of the form (exp(1/x)/x^(2*n))*(P(n-1,x)) where P(n-1,x) is a polynomial of degree n-1 with n terms. The term of degree k in P(n-1,x) has a coefficient given by T(n-1,k). Example: The third derivative of exp(1/x) is (exp(1/x)/x^6)*(1+6x+6x^2) and the 3rd row of this triangle is 1, 6, 6, which corresponds to this coefficients of the polynomial 1+6x+6x^2. - _Derek Orr_, Nov 06 2014",
				"For another context for this array see the Callan (2008) article. -  _Ron L.J. van den Burg_, Dec 12 2021"
			],
			"reference": [
				"A. T. Benjamin and J. J. Quinn, Proofs that really count: the art of combinatorial proof, M.A.A. 2003, id. 203."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A089231/b089231.txt\"\u003eTable of n, a(n) for the first 50 rows, flattened\u003c/a\u003e",
				"David Callan, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL11/Callan/callan412.html\"\u003eSets, Lists and Noncrossing Partitions\u003c/a\u003e, Journal of Integer Sequences, Vol. 11 (2008), Article 08.1.3. Also \u003ca href=\"http://arxiv.org/abs/0711.4841\"\u003eon arXiv\u003c/a\u003e, arXiv:0711.4841 [math.CO], 2007-2008.",
				"Tom Copeland, \u003ca href=\"https://tcjpn.wordpress.com/2011/04/11/lagrange-a-la-lah/\"\u003eLagrange a la Lah\u003c/a\u003e, 2011.",
				"Tom Copeland, \u003ca href=\"http://tcjpn.wordpress.com/2015/12/21/generators-inversion-and-matrix-binomial-and-integral-transforms/\"\u003eGenerators, Inversion, and Matrix, Binomial, and Integral Transforms\u003c/a\u003e, 2015",
				"Olexandr Ganyushkin and Volodymyr Mazorchuk, \u003ca href=\"http://dx.doi.org/10.1007/s00026-004-0213-7\"\u003eCombinatorics of nilpotents in symmetric inverse semigroups\u003c/a\u003e, Ann. Comb. 8 (2004), no. 2, 161--175. [From _Abdullahi Umar_, Sep 14 2008]",
				"F. Hivert, J.-C. Novelli and J.-Y. Thibon, \u003ca href=\"http://arXiv.org/abs/math.CO/0605262\"\u003eCommutative combinatorial Hopf algebras\u003c/a\u003e, arXiv:math/0605262 [math.CO], 2006.",
				"Matthieu Josuat-Vergès, \u003ca href=\"http://arxiv.org/abs/1601.02212\"\u003eStammering tableaux - Tableaux bégayants\u003c/a\u003e, arXiv:1601.02212 [math.CO], 2016. See Lemma 7.1 p. 16.",
				"A. Laradji and A. Umar, \u003ca href=\"http://dx.doi.org/10.1081/AGB-120038637\"\u003eOn the number of nilpotents in the partial symmetric semigroup\u003c/a\u003e, Comm. Algebra 32 (2004), 3017-3023.",
				"Jair Taylor, \u003ca href=\"http://math.stackexchange.com/questions/263945\"\u003eNumber of acyclic digraphs on [n] with k edges and each indegree, outdegree \u003c=1\u003c/a\u003e (question on StackExchange)"
			],
			"formula": [
				"T(n, k) = A001263(n, k)*k!; A001263 = triangle of Narayana.",
				"T(n, k) = C(n, n-k+1)*(n-1)!/(n-k)! = Sum_{i=n-k+1..n} |S1(n, i)*S2(i, n-k+1)| , with S1, S2 the Stirling numbers.",
				"From _Derek Orr_, Mar 12 2015: (Start)",
				"Each row represents a polynomial:",
				"P(1,x) = 1;",
				"P(2,x) = 1 + 2x;",
				"P(3,x) = 1 + 6x + 6x^2;",
				"P(4,x) = 1 + 12x + 36x^2 + 24x^3;",
				"...",
				"They are related through P(n+1,x) = x^2*P'(n,x) - (1+2*n*x)*P(n,x) with P(1,x) = 1.",
				"(End)",
				"From _Peter Bala_, Jul 04 2016: (Start)",
				"Working with an offset of 0:",
				"G.f.: exp(x*t)*I_1(2*sqrt(x)) = 1 + (1 + 2*t)*x/(1!*2!) + (1 + 6*t + 6*t^2)*x^2/(2!*3!) + (1 + 12*t + 36*t^2 + 24*t^3)*x^3/(3!*4!) + ..., where I_1(x) = Sum_{n \u003e= 0} (x/2)^(2*n)/(n!*(n+1)!) is a modified Bessel function of the first kind.",
				"The row polynomials R(n,t) satisfy R(n,t + u) = Sum_{k = 0..n} T(n,k)*t^k*R(n-k,u).",
				"R(n,t) = 1 + Sum_{k = 0..n-1} (-1)^(n-k+1)*(n+1)!/(k+1)!* binomial(n,k)*t^(n-k)*R(k,t). Cf. A144084. (End)",
				"From _Peter Bala_, Oct 05 2019: (Start)",
				"The following formulas use a column index k starting at 0:",
				"E.g.f.: exp(x/(1 - t*x)) - 1 = x + (1 + 2*t)*x^2/2! + (1 + 6*t + 6*t^2)*x^3/3! + ....",
				"Recurrence for row polynomials: R(n+1,t) = (1 + 2*n*t)R(n,t) - n*(n-1)*t^2*R(n-1,t), with R(1,t) = 1 and R(2,t) = 1 + 2*t.",
				"R(n+1,t) equals the numerator polynomial of the finite continued fraction 1 + n*t/(1 + n*t/(1 + (n-1)*t/(1 + (n-1)*t/(1 + ... + 2*t/(1 + 2*t/(1 + t/(1 + t/(1)))))))). The denominator polynomial is the n-th row polynomial of A144084. (End)",
				"T(n,k) = A105278(n,n-k). - _Ron L.J. van den Burg_, Dec 12 2021"
			],
			"example": [
				"1;",
				"1,  2;",
				"1,  6,    6;",
				"1, 12,   36,    24;",
				"1, 20,  120,   240,    120;",
				"1, 30,  300,  1200,   1800,    720;",
				"1, 42,  630,  4200,  12600,  15120,    5040;",
				"1, 56, 1176, 11760,  58800, 141120,  141120,   40320;",
				"1, 72, 2016, 28224, 211680, 846720, 1693440, 1451520, 362880;"
			],
			"maple": [
				"P := n -\u003e simplify(hypergeom([-n,-n+1],[],1/t));",
				"seq(print(seq(coeff(expand(t^k*P(k)),t,k-j+1),j=1..k)),k=1..n); # _Peter Luschny_, Oct 29 2014"
			],
			"mathematica": [
				"Table[(Binomial[n - 1, k - 1] Binomial[n, k - 1]/k) k!, {n, 9}, {k, n}] // Flatten (* _Michael De Vlieger_, Jul 04 2016 *)"
			],
			"program": [
				"(PARI) tabl(nn) = {for (n=0, nn, for (k=0, n, print1((n+1)!*binomial(n,k)/(n-k+1)!, \", \");); print(););} \\\\ _Michel Marcus_, Jan 12 2016"
			],
			"xref": [
				"Cf. A000262 (row sums), A008297, A066667, A144084, row mirror of A105278."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "1,3",
			"author": "_Philippe Deléham_, Dec 10 2003",
			"ext": [
				"StackExchange link added by _Felix A. Pahl_, Dec 25 2012"
			],
			"references": 9,
			"revision": 87,
			"time": "2021-12-17T20:54:28-05:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}