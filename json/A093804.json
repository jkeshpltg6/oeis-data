{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A093804",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 93804,
			"data": "2,3,11,37,41,73,26951,110059,150209",
			"name": "Primes p such that p! + 1 is also prime.",
			"comment": [
				"Or, numbers n such that Sum_{d|n} d! is prime.",
				"The prime 26951 from A002981 (n!+1 is prime) is a member since Sum_{d|n} d! = n!+1 if n is prime. - _Jonathan Sondow_, Jan 30 2005",
				"a(n) are the primes in A002981[n] = {0, 1, 2, 3, 11, 27, 37, 41, 73, 77, 116, 154, 320, 340, 399, 427, 872, 1477, 6380, 26951, ...} Numbers n such that n! + 1 is prime. Corresponding primes of the form p! + 1 are listed in A103319[n] = {3, 7, 39916801, 13763753091226345046315979581580902400000001, 33452526613163807108170062053440751665152000000001, ...}. - _Alexander Adamchuk_, Sep 23 2006"
			],
			"link": [
				"Chris K. Caldwell, The List of Largest Known Primes, \u003ca href=\"http://primes.utm.edu/primes/page.php?id=100445\"\u003e110059! + 1\u003c/a\u003e",
				"R. Mestrovic, \u003ca href=\"http://arxiv.org/abs/1202.3670\"\u003eEuclid's theorem on the infinitude of primes: a historical survey of its proofs (300 BC--2012) and another new proof\u003c/a\u003e, arXiv:1202.3670 [math.HO], 2012. - From N. J. A. Sloane, Jun 13 2012",
				"Apoloniusz Tyszka, \u003ca href=\"https://hal.archives-ouvertes.fr/hal-01614087v5/document\"\u003eA common approach to the problem of the infinitude of twin primes, primes of the form n! + 1, and primes of the form n! - 1\u003c/a\u003e, 2018.",
				"Apoloniusz Tyszka, \u003ca href=\"https://philarchive.org/archive/TYSDASv8\"\u003eA new approach to solving number theoretic problems\u003c/a\u003e, 2018."
			],
			"example": [
				"Sum_{d|3} d! = 1! + 3! = 7 is prime, so 3 is a member."
			],
			"maple": [
				"seq(`if`(isprime(ithprime(n)!+1), ithprime(n), NULL),n=1..25); # _Nathaniel Johnston_, Jun 28 2011"
			],
			"mathematica": [
				"Select[Prime[Range[5! ]],PrimeQ[ #!+1]\u0026] (* _Vladimir Joseph Stephan Orlovsky_, Nov 17 2009 *)"
			],
			"program": [
				"(PARI) isok(n) = ispseudoprime(n) \u0026\u0026 ispseudoprime(n!+1); \\\\ _Jinyuan Wang_, Jan 20 2020"
			],
			"xref": [
				"Cf. A062363, A002981, A038507, A088332, A103317, A103319."
			],
			"keyword": "nonn,more,hard",
			"offset": "1,1",
			"author": "_Jason Earls_, May 19 2004",
			"ext": [
				"One more term from _Alexander Adamchuk_, Sep 23 2006",
				"a(8)=110059 (found on Jun 11 2011, by PrimeGrid), added by _Arkadiusz Wesolowski_, Jun 28 2011",
				"a(9)=150209 (found on Jun 09 2012, by Rene Dohmen), added by _Jinyuan Wang_, Jan 20 2020"
			],
			"references": 4,
			"revision": 46,
			"time": "2020-01-20T09:43:26-05:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}