{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A265416",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 265416,
			"data": "0,1,1,2,6,1,5,3,5,7,9,2,4,6,6,4,59,6,61,8,63,10,14,3,12,5,5,7,7,7,58,5,10,60,11,7,10,62,13,9,13,64,42,11,11,15,66,4,55,13,13,6,68,6,57,8,15,8,10,8,48,59,10,6,10,10,14,61,72,12,12,8,12,10",
			"name": "Number of steps needed to reach 1 or to enter the cycle in the \"sqrt(Pi)*x+1\" problem.",
			"comment": [
				"The sqrt(Pi)*x+1 problem is as follows: start with a number x. If x is even, divide it by 2, otherwise multiply it by sqrt(Pi) and add 1, and then take the integer part.",
				"There are three possible behaviors for such trajectories when n\u003e0:",
				"(i) The trajectory reaches 1 (and enters the \"trivial\" cycle 2-1-2-1-2...).",
				"(ii) Cyclic trajectory. The trajectory becomes periodic and the period does not contain a 1.",
				"(iii) The trajectory is divergent (I conjecture that this cannot occur).",
				"For many numbers, the element of the trivial cycle is 1, except for the numbers: 3, 6, 7, 12, 13, 14, 15, 23, 24, 26, 27, 28, 29, 30, 33, 35, 37, 39, 41, 46, 48, 52, 54, 56, 58, 59, 60, 63, ..."
			],
			"link": [
				"Michel Lagneau, \u003ca href=\"/A265416/b265416.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"a(5) = 6 because 5 -\u003e 9 -\u003e 16 -\u003e 8 -\u003e 4 -\u003e 2 -\u003e 1 with 6 iterations where:",
				"5 -\u003e floor(5*sqrt(Pi)+1) = 9;",
				"9 -\u003e floor(9*sqrt(Pi)+1) = 16;",
				"16 -\u003e 16/2 = 8 -\u003e 8/2 = 4 -\u003e 4/2 = 2 -\u003e 2/2 = 1, the end of the cycle.",
				"a(6) = 1 because 6 -\u003e 3 with 1 iteration where:",
				"6 -\u003e 3;",
				"3 -\u003e floor(3*sqrt(Pi)+1) = 6, the end of the cycle."
			],
			"maple": [
				"A265416:= proc(n)",
				"    local cyc, x;",
				"    x := n;",
				"    cyc := {x} ;",
				"    for s from 0 do",
				"        if {1} intersect cyc = {1} then",
				"            return s;",
				"        end if;",
				"        if type(x, 'even') then",
				"            x := x/2 ;",
				"        else",
				"            x := floor(evalf(sqrt(Pi))*x+1) ;",
				"        end if;",
				"        if {x} intersect cyc = {x} and s \u003e 0 then",
				"            return s;",
				"        end if;",
				"        cyc := cyc union {x} ;",
				"    end do:",
				"end proc: (* Program from R.J. Mathar adapted for this sequence - see A264789 *)."
			],
			"mathematica": [
				"Table[Length@ NestWhileList[If[EvenQ@ #, #/2, Floor[# Sqrt@ Pi + 1]] \u0026, n, UnsameQ, All] - 2, {n, 0, 72}] (* Program from Michael De Vlieger adapted for this sequence - see A264789 *)."
			],
			"xref": [
				"Cf. A006577, A264789."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Michel Lagneau_, Dec 08 2015",
			"references": 1,
			"revision": 10,
			"time": "2015-12-13T07:57:47-05:00",
			"created": "2015-12-13T07:57:47-05:00"
		}
	]
}