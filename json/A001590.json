{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001590",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1590,
			"id": "M0784 N0296",
			"data": "0,1,0,1,2,3,6,11,20,37,68,125,230,423,778,1431,2632,4841,8904,16377,30122,55403,101902,187427,344732,634061,1166220,2145013,3945294,7256527,13346834,24548655,45152016,83047505,152748176,280947697,516743378,950439251",
			"name": "Tribonacci numbers: a(n) = a(n-1) + a(n-2) + a(n-3) with a(0)=0, a(1)=1, a(2)=0.",
			"comment": [
				"Dimensions of the homogeneous components of the higher order peak algebra associated to cubic roots of unity (Hilbert series = 1 + 1*t + 2*t^2 + 3*t^3 + 6*t^4 + 11*t^5 ...). - Jean-Yves Thibon (jyt(AT)univ-mlv.fr), Oct 22 2006",
				"Starting with offset 3: (1, 2, 3, 6, 11, 10, 37, ...) = row sums of triangle A145579. - _Gary W. Adamson_, Oct 13 2008",
				"Starting (1, 2, 3, 6, 11, ...) = INVERT transform of the periodic sequence (1, 1, 0, 1, 1, 0, 1, 1, 0, ...). - _Gary W. Adamson_, May 04 2009",
				"The comment of May 04 2009 is equivalent to: The numbers of ordered compositions of n using integers that are not multiples of 3 is equal to (1, 2, 3, 6, 11, ...) for n = (1, 2, 3, ...). - _Gary W. Adamson_, May 13 2013",
				"Primes in the sequence are 2, 3, 11, 37, 634061, 7256527, 1424681173049, ... in A231574. - _R. J. Mathar_, Aug 09 2012",
				"Pisano period lengths: 1, 2, 13, 8, 31, 26, 48, 16, 39, 62,110,104,168, 48,403, 32, 96, 78, 360, 248, ... . - _R. J. Mathar_, Aug 10 2012",
				"a(n+1) is the top left entry of the n-th power of any of 3 X 3 matrices [0, 1, 0; 1, 1, 1; 1, 0, 0], [0, 1, 1; 1, 1, 0; 0, 1, 0], [0, 1, 1; 0, 0, 1; 1, 0, 1] or [0, 0, 1; 1, 0, 0; 1, 1, 1]. - _R. J. Mathar_, Feb 03 2014",
				"a(n+3) equals the number of n-length binary words avoiding runs of zeros of lengths 3i+2, (i=0,1,2,...). - _Milan Janjic_, Feb 26 2015",
				"Sums of pairs of successive terms of A000073. - _N. J. A. Sloane_, Oct 30 2016",
				"The power Q^n, for n \u003e= 0, of the tribonacci Q-matrix Q = matrix([1, 1, 1], [1, 0, 0], [0, 1, 0]) is, by the Cayley-Hamilton theorem, Q^n = matrix([a(n+2), a(n+1) + a(n), a(n+1)], [a(n+1), a(n) + a(n-1), a(n)], [a(n), a(n-1) + a(n-2), a(n-1)], with a(-2) = -1 and a(-1) = 1. One can use a(n) = a(n-1) + a(n-2) + a(n-3) in order to obtain a(-1) and a(-2). - _Wolfdieter Lang_, Aug 13 2018",
				"a(n+2) is the number of entries n, for n\u003e=1, in the sequence {A278038(k)}_{k\u003e=1} (without A278038(0) = 1). - _Wolfdieter Lang_, Sep 11 2018",
				"In terms of the tribonacci numbers T(n) = A000073(n) the nonnegative powers of the Q-matrix (from the Aug 13 2018 comment) are Q^n = T(n)*Q^2 + (T(n-1) + T(n-2))*Q + T(n-1)*1_3, for n \u003e= 0, with T(-1) = 1, T(-2) = -1. This is equivalent to the powers t^n of the tribonacci constant t = A058255 (or also for powers of the complex solutions). - _Wolfdieter Lang_, Oct 24 2018"
			],
			"reference": [
				"Kenneth Edwards, Michael A. Allen, A new combinatorial interpretation of the Fibonacci numbers squared, Part II, Fib. Q., 58:2 (2020), 169-177.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A001590/b001590.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"Barry Balof, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL15/Balof/balof19.html\"\u003eRestricted tilings and bijections\u003c/a\u003e, J. Integer Seq. 15 (2012), no. 2, Article 12.2.3, 17 pp.",
				"Matthias Beck, Neville Robbins, \u003ca href=\"http://arxiv.org/abs/1403.0665\"\u003eVariations on a Generatingfunctional Theme: Enumerating Compositions with Parts Avoiding an Arithmetic Sequence\u003c/a\u003e, arXiv:1403.0665 [math.NT], 2014.",
				"Martin Burtscher, Igor Szczyrba, Rafał Szczyrba, \u003ca href=\"http://www.emis.de/journals/JIS/VOL18/Szczyrba/sz3.html\"\u003eAnalytic Representations of the n-anacci Constants and Generalizations Thereof\u003c/a\u003e, Journal of Integer Sequences, Vol. 18 (2015), Article 15.4.5.",
				"M. Feinberg, \u003ca href=\"http://www.fq.math.ca/Scanned/1-3/feinberg.pdf\"\u003eFibonacci-Tribonacci\u003c/a\u003e, Fib. Quart. 1(3) (1963), 71-74.",
				"M. Feinberg, \u003ca href=\"http://www.fq.math.ca/Scanned/2-3/feinberg.pdf\"\u003eNew slants\u003c/a\u003e, Fib. Quart. 2 (1964), 223-227.",
				"W. Florek, \u003ca href=\"http://doi.org/10.1016/j.amc.2018.06.014\"\u003eA class of generalized Tribonacci sequences applied to counting problems\u003c/a\u003e, Appl. Math. Comput., 338 (2018), 809-821.",
				"P. Hadjicostas, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL19/Hadjicostas/hadji2.html\"\u003eCyclic Compositions of a Positive Integer with Parts Avoiding an Arithmetic Sequence\u003c/a\u003e, Journal of Integer Sequences, 19 (2016), Article 16.8.2.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=401\"\u003eEncyclopedia of Combinatorial Structures 401\u003c/a\u003e",
				"Milan Janjic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL19/Janjic/janjic73.html\"\u003eBinomial Coefficients and Enumeration of Restricted Words\u003c/a\u003e, Journal of Integer Sequences, 2016, Vol 19, #16.7.3.",
				"Tamara Kogan, L. Sapir, A. Sapir, A. Sapir, \u003ca href=\"https://doi.org/10.1016/j.apnum.2016.08.012\"\u003eThe Fibonacci family of iterative processes for solving nonlinear equations\u003c/a\u003e, Applied Numerical Mathematics 110 (2016) 148-158.",
				"D. Krob and J.-Y. Thibon, \u003ca href=\"https://arxiv.org/abs/math/0411407\"\u003eHigher order peak algebras\u003c/a\u003e, arXiv:math/0411407 [math.CO], 2004.",
				"Wolfdieter Lang, \u003ca href=\"https://arxiv.org/abs/1810.09787\"\u003eThe Tribonacci and ABC Representations of Numbers are Equivalent\u003c/a\u003e, arXiv preprint arXiv:1810.09787 [math.NT], 2018.",
				"Sepideh Maleki, Martin Burtscher, \u003ca href=\"https://doi.org/10.1145/3173162.3173168\"\u003eAutomatic Hierarchical Parallelization of Linear Recurrences\u003c/a\u003e, Proceedings of the 23rd International Conference on Architectural Support for Programming Languages and Operating Systems, ACM, 2018.",
				"M. A. Nyblom, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL16/Nyblom/nyblom13.html\"\u003eCounting Palindromic Binary Strings Without r-Runs of Ones\u003c/a\u003e, J. Int. Seq. 16 (2013) #13.8.7.",
				"H. Prodinger, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Prodinger2/prod31.html\"\u003eCounting Palindromes According to r-Runs of Ones Using Generating Functions\u003c/a\u003e, J. Int. Seq. 17 (2014) # 14.6.2, even length, r=2.",
				"Neville Robbins, \u003ca href=\"https://www.fq.math.ca/Papers1/52-1/NRobbins.pdf\"\u003eOn Tribonacci Numbers and 3-Regular Compositions\u003c/a\u003e, Fibonacci Quart. 52 (2014), no. 1, 16-19. See Adamson's comments.",
				"Bo Tan and Zhi-Ying Wen, \u003ca href=\"http://dx.doi.org/10.1016/j.ejc.2006.07.007\"\u003eSome properties of the Tribonacci sequence\u003c/a\u003e, European Journal of Combinatorics, 28 (2007) 1703-1719.",
				"M. E. Waddill and L. Sacks, \u003ca href=\"http://www.fq.math.ca/Scanned/5-3/waddill.pdf\"\u003eAnother generalized Fibonacci sequence\u003c/a\u003e, Fib. Quart., 5 (1967), 209-222.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/TribonacciNumber.html\"\u003eTribonacci Number\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,1,1)."
			],
			"formula": [
				"G.f.: x*(1-x)/(1-x-x^2-x^3).",
				"Limit a(n)/a(n-1) = t where t is the real solution of t^3 = 1 + t + t^2, t = A058265 = 1.839286755... . If T(n) = A000073(n) then t^n  = T(n-1) + a(n)*t + T(n)*t^2, for n \u003e= 0, with T(-1) = 1.",
				"a(3*n) = Sum_{k+l+m=n} (n!/k!l!m!)*a(l+2*m). Example: a(12)=a(8)+4a(7)+10a(6)+16a(5)+19a(4)+16a(3)+10a(2)+4a(1)+a(0) The coefficients are the trinomial coefficients. T(n) and T(n-1) also satisfy this equation. (T(-1)=1)",
				"From _Reinhard Zumkeller_, May 22 2006: (Start)",
				"a(n) = A000073(n+1)-A000073(n);",
				"a(n) = A000073(n-1)+A000073(n-2) for n\u003e1;",
				"A000213(n-2) = a(n+1)-a(n) for n\u003e1. (End)",
				"a(n) + a(n+1) = A000213(n). - _Philippe Deléham_, Sep 25 2006",
				"If p[1]=0, p[i]=2, (i\u003e1), and if A is Hessenberg matrix of order n defined by: A[i,j]=p[j-i+1], (i\u003c=j), A[i,j]=-1, (i=j+1), and A[i,j]=0 otherwise. Then, for n\u003e=1, a(n+1)=det A. - _Milan Janjic_, May 02 2010",
				"For n\u003e=4, a(n)=2*a(n-1)-a(n-4). - _Bob Selcoe_, Feb 18 2014",
				"a(-1-n) = -A078046(n). - _Michael Somos_, Jun 01 2014"
			],
			"example": [
				"a(12)=a(11)+a(10)+a(9): 230=125+68+37.",
				"G.f. = x + x^3 + 2*x^4 + 3*x^5 + 6*x^6 + 11*x^7 + 20*x^8 + 37*x^9 + 68*x^10 + ..."
			],
			"maple": [
				"seq(coeff(series(x*(1-x)/(1-x-x^2-x^3),x,n+1), x, n), n = 0 .. 40); # _Muniru A Asiru_, Oct 24 2018"
			],
			"mathematica": [
				"LinearRecurrence[{1,1,1}, {0,1,0}, 50] (* _Vladimir Joseph Stephan Orlovsky_, Jan 28 2012 *)",
				"RecurrenceTable[{a[0]==0, a[1]==1, a[2]==0, a[n]==a[n-1]+a[n-2]+a[n-3]}, a, {n, 40}] (* _Vincenzo Librandi_, Apr 19 2018 *)"
			],
			"program": [
				"(PARI) a(n)=([0,1,0; 0,0,1; 1,1,1]^n*[0;1;0])[1,1] \\\\ _Charles R Greathouse IV_, Jul 28 2015",
				"(Sage)",
				"def A001590():",
				"    W = [0, 1, 0]",
				"    while True:",
				"        yield W[0]",
				"        W.append(sum(W))",
				"        W.pop(0)",
				"a = A001590(); [next(a) for _ in range(38)]  # _Peter Luschny_, Sep 12 2016",
				"(MAGMA) I:=[0,1,0]; [n le 3 select I[n]  else Self(n-1)+Self(n-2)+Self(n-3): n in [1..40]]; // _Vincenzo Librandi_, Apr 19 2018",
				"(GAP) a:=[0,1,0];; for n in [4..40] do a[n]:=a[n-1]+a[n-2]+a[n-3]; od; a; # _Muniru A Asiru_, Oct 24 2018"
			],
			"xref": [
				"Cf. A000045, A000073, A027907, A027053, A078042, A145579, A278038."
			],
			"keyword": "nonn,easy",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Additional comments from _Miklos Kristof_, Jul 03 2002"
			],
			"references": 122,
			"revision": 175,
			"time": "2020-07-03T14:55:04-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}