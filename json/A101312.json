{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A101312",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 101312,
			"data": "2,1,3,1,2,2,2,2,1,1,2,2,1,3,1,1,2,2,1,2,1,2,2,1,3,1,1,3,2,1,3,1,2,2,2,2,1,1,2,2,1,3,1,1,2,2,1,2,1,2,2,1,3,1,1,3,2,1,3,1,2,2,2,2,1,1,2,2,1,3,1,1,2,2,1,2,1,2,2,1,3,1,1,3,2,1,3,1,2,2,2,2,1,1,2,2,1,3,1,1,2,2,1,2,1",
			"name": "Number of \"Friday the 13ths\" in year n (starting at 1901).",
			"comment": [
				"This sequence is basically periodic with period 28 [example: a(1901) = a(1929) = a(1957)], with \"jumps\" when it passes a non-leap-year century such as 2100 [all centuries which are not multiples of 400].",
				"At these points [for example, a(2101)], the sequence simply \"jumps\" to a different point in the same pattern, \"dropping back\" 12 entries [or equivalently, \"skipping ahead\" 16 entries], but still continuing with the same repeating [period 28] pattern.",
				"Every year has at least 1 \"Friday the 13th,\" and no year has more than 3.",
				"On average, 171 of every 400 years (42.75%) have 1 \"Friday the 13th,\" 170 of every 400 years (42.5%) have 2 of them and only 59 in 400 years (14.75%) have 3 of them. [Corrected by _Pontus von Brömssen_, Sep 09 2021]",
				"Conjecture: The same basic repeating pattern results if we seek the number of \"Sunday the 22nds\" or \"Wednesday the 8ths\" or anything else similar, with the only difference being that the sequence starts at a different point in the repeating pattern.",
				"Periodic with period 400. (Because the number of days in 400 years is 400*365 + 97 = 146097, which happens to be divisible by 7.) - _Pontus von Brömssen_, Sep 09 2021"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A101312/b101312.txt\"\u003eTable of n, a(n) for n = 1901..3000\u003c/a\u003e",
				"J. R. Stockton, \u003ca href=\"http://www.merlyn.demon.co.uk/zel-86px.htm\"\u003eRektor Chr. Zeller's 1886 Paper \"Kalender-Formeln\"\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Triskaidekaphobia.html\"\u003eTriskaidekaphobia\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Triskaidekaphobia\"\u003eTriskaidekaphobia\u003c/a\u003e",
				"Chr. Zeller, \u003ca href=\"http://dx.doi.org/10.1007/BF02406733\"\u003eKalender-Formeln\u003c/a\u003e, Acta Mathematica, 9 (1886), 131-136.",
				"\u003ca href=\"/index/Ca#calendar\"\u003eIndex entries for sequences related to calendars\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_400\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, order 400."
			],
			"formula": [
				"a(A190651(n)) = 1; a(A190652(n)) = 2; a(A190653(n)) = 3. - _Reinhard Zumkeller_, May 16 2011",
				"1 \u003c= a(n) \u003c= 3. - _Michael S. Branicky_, Sep 09 2021"
			],
			"example": [
				"a(1902) = 1, since only Jun 13 1902 fell on a Friday.",
				"a(2004) = 2, since there were 2 \"Friday the 13ths\" that year: Feb 13 2004 and Aug 13 2004 each fell on a Friday.",
				"a(2012) = 3, since Jan 13 2012, Apr 13 2012, and Jul 13 2012 are all Fridays."
			],
			"mathematica": [
				"(*Load \u003c\u003cMiscellaneous`Calendar` package first*) s={};For[n=1901, n\u003c=2200, t=0;For[m=1, m\u003c=12, If[DayOfWeek[{n, m, 13}]===Friday, t++ ];m++ ];AppendTo[s, t];n++ ];s"
			],
			"program": [
				"(Haskell)",
				"a101312 n = f 1 {- January -} where",
				"   f 13                = 0",
				"   f m | h n m 13 == 6 = (f $ succ m) + 1",
				"       | otherwise     = f $ succ m",
				"   h year month day  -- cf. Zeller reference.",
				"     | month \u003c= 2 = h  (year - 1)  (month + 12)  day",
				"     | otherwise  = (day + 26 * (month + 1) `div` 10 + y + y `div` 4",
				"                    + century `div` 4 - 2 * century) `mod` 7",
				"       where (century, y) = divMod year 100",
				"-- _Reinhard Zumkeller_, May 16 2011",
				"(Python)",
				"from datetime import date",
				"def a(n):",
				"    return sum(date.isoweekday(date(n, m, 13)) == 5 for m in range(1, 13))",
				"print([a(n) for n in range(1901, 2006)]) # _Michael S. Branicky_, Sep 09 2021"
			],
			"xref": [
				"Cf. A011763, A157962, A188528, A190651, A190652, A190653."
			],
			"keyword": "nonn,easy",
			"offset": "1901,1",
			"author": "Adam M. Kalman (mocha(AT)clarityconnect.com), Dec 22 2004",
			"references": 7,
			"revision": 53,
			"time": "2021-09-09T16:32:10-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}