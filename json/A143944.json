{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A143944",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 143944,
			"data": "4,2,12,14,8,2,24,34,32,20,8,2,40,62,68,60,40,20,8,2,60,98,116,116,100,70,40,20,8,2,84,142,176,188,180,154,112,70,40,20,8,2,112,194,248,276,280,262,224,168,112,70,40,20,8,2,144,254,332,380,400,394,364,312,240",
			"name": "Triangle read by rows: T(n,k) is the number of unordered pairs of vertices at distance k from each other in the grid P_n X P_n (1 \u003c= k \u003c= 2n-2), where P_n is the path graph on n vertices.",
			"comment": [
				"Row n contains 2n-2 entries.",
				"Sum of entries in row n = n^2*(n^2 - 1)/2 = A083374(n).",
				"The entries in row n are the coefficients of the Wiener (Hosoya) polynomial of the grid P_n X P_n.",
				"Sum_{k=1..2n-2} k*T(n,k) = n^3*(n^3 - 1)/3 = A143945(n) = the Wiener index of the grid P_n X P_n.",
				"The average of all distances in the grid P_n X P_n is 2n/3."
			],
			"link": [
				"D. Stevanovic, \u003ca href=\"https://doi.org/10.1016/S0012-365X(00)00277-6\"\u003eHosoya polynomial of composite graphs\u003c/a\u003e, Discrete Math., 235 (2001), 237-244.",
				"B.-Y. Yang and Y.-N. Yeh, \u003ca href=\"http://www.iis.sinica.edu.tw/papers/byyang/2387-F.pdf\"\u003eWiener polynomials of some chemically interesting graphs\u003c/a\u003e, International Journal of Quantum Chemistry, 99 (2004), 80-91.",
				"Y.-N. Yeh and I. Gutman, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(93)E0092-I\"\u003eOn the sum of all distances in composite graphs\u003c/a\u003e, Discrete Math., 135 (1994), 359-365."
			],
			"formula": [
				"Generating polynomial of row n is (2q(1-q^n) - n(1-q^2))^2/(2(1-q)^4) - n^2/2."
			],
			"example": [
				"T(2,2)=2 because P_2 X P_2 is a square and there are 2 pairs of vertices at distance 2.",
				"Triangle starts:",
				"   4,  2;",
				"  12, 14,  8,  2;",
				"  24, 34, 32, 20,  8,  2;",
				"  40, 62, 68, 60, 40, 20,  8,  2;"
			],
			"maple": [
				"for n from 2 to 10 do Q[n]:=sort(expand(simplify((1/2)*(2*q*(1-q^n)-n*(1-q^2))^2/(1-q)^4-(1/2)*n^2))) end do: for n from 2 to 9 do seq(coeff(Q[n],q,j),j= 1..2*n-2) end do;"
			],
			"xref": [
				"Cf. A083374, A143945."
			],
			"keyword": "nonn,tabf",
			"offset": "2,1",
			"author": "_Emeric Deutsch_, Sep 19 2008",
			"references": 1,
			"revision": 10,
			"time": "2017-07-21T11:19:41-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}