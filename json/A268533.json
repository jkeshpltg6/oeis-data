{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A268533",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 268533,
			"data": "1,1,1,1,-1,1,2,1,1,0,-1,1,-2,1,1,3,3,1,1,1,-1,-1,1,-1,-1,1,1,-3,3,-1,1,4,6,4,1,1,2,0,-2,-1,1,0,-2,0,1,1,-2,0,2,-1,1,-4,6,-4,1,1,5,10,10,5,1,1,3,2,-2,-3,-1,1,1,-2,-2,1,1,1,-1,-2,2,1,-1,1,-3,2,2,-3,1,1,-5,10,-10,5,-1",
			"name": "Pascal's difference pyramid read first by blocks and then by rows: T(n,k,m) = 1/(m!) * (d/dx)^m((1-x)^k*(1+x)^(n-k))|_{x=0}.",
			"comment": [
				"T(n,k,m) is a pyramidal stack of (n+1)-by-(n+1) dimensional matrices, or an infinite-dimensional matrix in block-diagonal form (see examples).",
				"Define triangular slices T_x(i,j) = T(2x+i,x,x+j) with i in {0,1,...} and j in {0,1,... i}. T_0 is Pascal's triangle, and it appears that T_{x} is a triangle of first differences T_{x}(i,j) = T_{x-1}(i+1,j+1)-T_{x-1}(i+1,j) (cf. A007318, A214292).",
				"The so-called \"quantum Pascal's pyramid\", denoted QT(n,k,m), is obtained from Pascal's pyramid by a complexification of matrix elements: QT(n,k,m) = (-1)^(3m/2) T(n,k,m). QT(n,k,m) effects a Hermite-Cartesian (cf. A066325) to Laguerre-polar change of coordinates (see examples).",
				"Row reversal is complex conjugation: QT(n,n-k,m) = QT(n,k,m)*.",
				"To construct the \"normalized quantum Pascal's pyramid\", NQT(n,k,m), we need normalization numerators, NumT(n,k,m) as in A269301, and denominators, DenT(n,k,m) as in A269302; then, NQT(n,k,m) = sqrt(NumT(n,k,m) / DenT(n,k,m)) QT(n,k,m). In the context of physics NQT(n,k,m) acting as matrix conjugation effects a cyclic permutation of the infinite-dimensional generators of rotation, so NQT(n,k,m) is essentially equivalent to an infinite-dimensional rotation with (z,y,z) Euler angles (0,Pi/2,Pi/2) (Harter, Klee, see examples).",
				"Normalization or no, Pascal's pyramid also arises in laser optics (Allen et al.) as the paraxial wave equation often admits a useful analogy to the Schrödinger equation for the two-dimensional isotropic quantum harmonic oscillator."
			],
			"reference": [
				"L. Allen, S. M. Barnett, and M. J. Padgett, Optical angular momentum, Institute of Physics Publishing, Bristol, 2003."
			],
			"link": [
				"L. Allen et al., \u003ca href=\"https://web.archive.org/web/20190510134139/http://www.science.uva.nl/research/aplp/eprints/AllBeiSpr92.pdf\"\u003eOrbital angular momentum of light and the transformation of Laguerre-Gaussian laser modes\u003c/a\u003e, Physical Review A, 45 (1992), 8185-8190.",
				"W. Harter, \u003ca href=\"https://web.archive.org/web/20161110062527/http://www.uark.edu/ua/modphys/markup/PSDS_Info.html/\"\u003ePrinciples of Symmetry, Dynamics, Spectroscopy\u003c/a\u003e, Wiley, 1993, Ch. 5, page 345-348.",
				"B. Klee, \u003ca href=\"http://demonstrations.wolfram.com/QuantumAngularMomentumMatrices/\"\u003eQuantum Angular Momentum Matrices\u003c/a\u003e, Wolfram Demonstrations Project, 2016."
			],
			"formula": [
				"T(n,k,m) = (1/(m!)) * (d/dx)^m((1-x)^k*(1+x)^(n-k))|_{x=0}."
			],
			"example": [
				"First few blocks:",
				"1",
				".  1,  1",
				".  1, -1",
				". . . . .  1,  2,  1",
				". . . . .  1,  0, -1",
				". . . . .  1, -2,  1",
				". . . . . . . . . . .  1,  3,  3,  1",
				"Second triangle . . .  1,  1, -1, -1",
				"slice, T_1: . . . . .  1, -1, -1,  1",
				"0 . . . . . . . . . .  1, -3,  3, -1",
				"1  -1 . . . . . . . . . . . . . . . .  1,  4,  6,  4,  1",
				"2   0  -2 . . . . . . . . . . . . . .  1,  2,  0, -2, -1",
				"3,  2, -2, -3 . . . . . . . . . . . .  1,  0, -2,  0,  1",
				"4,  5,  0, -5, -4 . . . . . . . . . .  1, -2,  0,  2, -1",
				"5,  9,  5, -5, -9, -5 . . . . . . . .  1, -4,  6, -4,  1",
				"n=2 Cartesian/Polar coordinate change using quantum Pascal's pyramid:",
				"| 1  -2 i  -1 |   | y^2 - 1 |    | - (r exp[ I \\phi])^2 |",
				"| 1   0     1 | * |   x*y   | =  |      r^2  -  2       |",
				"| 1   2 i  -1 |   | x^2 - 1 |    | - (r exp[-I \\phi])^2 |",
				"When: x = r cos[\\phi], y= r sin[\\phi].",
				"Permutation of Pauli Matrices, \\sigma_i, using normalized quantum Pascal's pyramid:",
				"                  | 1  -i |",
				"R = (1/sqrt[2]) * | 1   i |",
				"Then, R * \\sigma_j * R^{\\dagger} = \\sigma_{pi(j)},",
				"where pi(j) is a cyclic permutation: { 1 -\u003e 2, 2 -\u003e 3, 3 -\u003e 1 }."
			],
			"mathematica": [
				"PascalsPyramid[Block_] := Outer[Simplify[Function[{n, k, m},1/(m!)(D[(1 - x)^k*(1 + x)^(n - k), {x, m}] /. x -\u003e 0)][Block, #1, #2]] \u0026, Range[0, Block], Range[0, Block]]; PascalsPyramid /@ Range[0, 10]"
			],
			"xref": [
				"Cf. A007318, A214292, A269301, A269302."
			],
			"keyword": "sign",
			"offset": "0,7",
			"author": "_Bradley Klee_, Feb 22 2016",
			"references": 2,
			"revision": 38,
			"time": "2021-09-23T02:24:47-04:00",
			"created": "2016-03-13T10:27:30-04:00"
		}
	]
}