{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A268829",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 268829,
			"data": "1,1,3,1,5,3,1,15,0,1,1,9,7,1,3,1,27,7,1,5,1,1,29,0,1,0,0,3,1,23,3,1,5,9,1,3,1,17,3,1,3,15,15,5,3,1,51,0,1,3,0,0,15,0,1,1,53,7,1,13,31,11,9,1,1,3,1,63,7,1,0,21,7,27,9,0,5,3,1,57,0,1,13,0,5,29,0,13,1,0,3,1,43,3,1,3,53,15,23,9,25,1,7,1,1",
			"name": "Square array A(row,col) = B(row,(2*col)-1), where B(p,q) = 0 if gcd(p,q) \u003e 1, and 1 + 2*F(p,q) otherwise, where F is defined as in A269158. Array is read by descending antidiagonals as A(1,1), A(1,2), A(2,1), A(1,3), A(2,2), A(3,1), ...",
			"formula": [
				"A(i,j) = B(i,(2*j)-1), where B(p,q) = 0 if gcd(p,q) \u003e 1, and 1 + 2*F(p,q) = 1 + 2*A269158(p,(q+1)/2) otherwise, where function F is defined as in A269158."
			],
			"example": [
				"The top left [1 .. 16] x [1 .. 25] section of the array:",
				"1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1",
				"3,  5, 15,  9, 27, 29, 23, 17, 51, 53, 63, 57, 43, 45, 39, 33",
				"3,  0,  7,  7,  0,  3,  3,  0,  7,  7,  0,  3,  3,  0,  7,  7",
				"1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1",
				"3,  5,  0,  5,  3,  3, 13,  0, 13,  3, 11, 13,  0, 13, 11, 11",
				"1,  0,  9, 15,  0, 31, 21,  0, 53, 51,  0, 59, 41,  0, 33, 39",
				"3,  1, 15,  0, 11,  7,  5, 15,  5,  3,  0,  7,  3,  9, 11,  9",
				"3,  5, 15,  9, 27, 29, 23, 17, 51, 53, 63, 57, 43, 45, 39, 33",
				"3,  0,  1,  9,  0,  9, 19,  0, 25,  3,  0,  1, 25,  0,  9, 19",
				"1,  1,  0, 13, 25, 31, 27,  0, 63, 55, 53, 53,  0, 33, 45, 43",
				"3,  5,  1,  1, 27,  0, 15, 23, 29, 27, 29,  7, 17, 21, 21, 31",
				"3,  0,  7,  7,  0,  3,  3,  0,  7,  7,  0,  3,  3,  0,  7,  7",
				"3,  1,  7, 15,  1, 29,  0, 13,  3, 23, 29, 17, 17, 19, 25, 23",
				"1,  5,  1,  0, 17, 27, 19, 31, 55, 55,  0, 63, 41, 37, 45, 41",
				"3,  0,  0,  1,  0,  1, 23,  0, 19,  7,  0, 31,  0,  0,  5, 31",
				"1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1",
				"3,  5, 15,  7, 27, 31,  1, 17,  0, 17, 35, 23, 17, 29, 37, 21",
				"1,  0, 15,  1,  0, 21,  5,  0, 43, 55,  0, 57, 51,  0, 47, 51",
				"3,  1,  1,  5,  1, 29, 21,  1, 51,  0, 23, 39, 17, 19, 21, 33",
				"3,  5,  0,  5,  3,  3, 13,  0, 13,  3, 11, 13,  0, 13, 11, 11",
				"3,  0,  1,  0,  0, 31, 23,  0,  1, 53,  0, 21, 35,  0, 21, 31",
				"1,  1, 15,  9,  1,  0, 25,  7, 47, 47, 35, 63, 59, 57, 51, 63",
				"3,  5,  7,  9,  3,  1, 27, 17, 53,  1, 63,  0, 27, 39, 17, 23",
				"1,  0,  9, 15,  0, 31, 21,  0, 53, 51,  0, 59, 41,  0, 33, 39",
				"3,  1,  0,  1, 11,  3,  3,  0, 51, 51,  1, 57,  0, 25, 51, 27"
			],
			"program": [
				"(Scheme)",
				"(define (A268829 n) (let ((p (A002260 n)) (q (+ -1 (* 2 (A004736 n))))) (if (\u003c 1 (gcd p q)) 0 (+ 1 (* 2 (A269158auxbi p q)))))) ;; This one uses the code of A269158.",
				";; The following is a more stand-alone implementation:",
				"(define (A268829 n) (A268829auxbi (A002260 n) (+ -1 (* 2 (A004736 n)))))",
				"(define (A268829auxbi p q) (if (not (odd? q)) (error \"A268829auxbi: the second argument should be odd: \" p q) (let loop ((p p) (q q) (s 0)) (cond ((zero? p) 0) ((= 1 p) (+ 1 (* 2 s))) ((odd? p) (loop (modulo q p) p (A003987bi s (A004198bi p q)))) (else (loop (/ p 2) q (A003987bi s (A003987bi q (/ (- q 1) 2)))))))))"
			],
			"xref": [
				"Cf. arrays A268728, A269158."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Antti Karttunen_, Feb 20 2016",
			"references": 6,
			"revision": 6,
			"time": "2016-02-28T07:57:14-05:00",
			"created": "2016-02-28T07:57:14-05:00"
		}
	]
}