{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A063524",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 63524,
			"data": "0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0",
			"name": "Characteristic function of 1.",
			"comment": [
				"The identity function for Dirichlet multiplication (see Apostol).",
				"Sum of the Moebius function mu(d) of the divisors d of n. - _Robert G. Wilson v_, Sep 30 2006",
				"-a(n) is the Hankel transform of A000045(n), n \u003e= 0 (Fibonacci numbers). See A055879 for the definition of Hankel transform. - _Wolfdieter Lang_, Jan 23 2007",
				"a(A000012(n)) = 1; a(A087156(n)) = 0. - _Reinhard Zumkeller_, Oct 11 2008",
				"a(n) for n \u003e= 1 is the Dirichlet convolution of following functions b(n), c(n), a(n) = Sum_{d|n} b(d)*c(n/d)): a(n) = A008683(n) * A000012(n), a(n) = A007427(n) * A000005(n), a(n) = A007428(n) * A007425(n). - _Jaroslav Krizek_, Mar 03 2009",
				"From _Christopher Hunt Gribble_, Jul 11 2013: (Start)",
				"a(n) for 1 \u003c= n \u003c= 4 and conjectured for n \u003e 4 is the number of Hamiltonian circuits in a 2n X 2n square lattice of nodes, reduced for symmetry, where the orbits under the symmetry group of the square, D4, have 1 element: When n=1, there is only 1 Hamiltonian circuit in a 2 X 2 square lattice, as illustrated below.  The circuit is the same when rotated and/or reflected and so has only 1 orbital element under the symmetry group of the square.",
				"     o--o",
				"     |  |",
				"     o--o  (End)",
				"Convolution property: For any sequence b(n), the sequence c(n)=b(n)*a(n) has the following values: c(1)=0, c(n+1)=b(n) for all n \u003e 1. In other words, the sequence b(n) is shifted 1 step to the right. - _David Neil McGrath_, Nov 10 2014"
			],
			"reference": [
				"T. M. Apostol, Introduction to Analytic Number Theory, Springer-Verlag, 1976, page 30."
			],
			"link": [
				"G. P. Michon, \u003ca href=\"http://www.numericana.com/answer/numbers.htm#multiplicative\"\u003eMultiplicative Functions\u003c/a\u003e.",
				"\u003ca href=\"/index/Rec#order_01\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1).",
				"\u003ca href=\"/index/Ch#char_fns\"\u003eIndex entries for characteristic functions\u003c/a\u003e"
			],
			"formula": [
				"a(n) = (n!^2 mod (n+1))*((n+1)!^2 mod (n+2)), with n \u003e= 0. - _Paolo P. Lava_, Apr 24 2007",
				"a(n) = C((n+1)^2,n+3) mod 2 = C((n+13)^4,n+15) mod 2 = C((n+61)^6,n+63) mod 2 etc. - _Paolo P. Lava_, Aug 31 2007",
				"Any sequence formed from zeros and a unique 1 can be produced using the formula a(n) = C(n^2k,n+2) mod 2, where k is a positive integer and n \u003e= 0. The sequence is formed by [2^2k-2 initial zeros] U [1] U [infinitely many zeros]. If we want to have 1 in a specific position the formula must be modified: a(n) = C((n+m)^2k,n+2+m) mod 2, where k and m are positive integers and n \u003e= 0. In this way we have {2^2k-2-m initial zeros} U {1} U {infinitely many zeros}. Of course we must have 2^2k-2 \u003e m. Then if we want the unique 1 in the position r, the minimum power k we can use is given by the relation 2^2k-1 \u003e= r, namely k \u003e= (1/2)*log_2(r+1). - _Paolo P. Lava_, Aug 31 2007",
				"From _Philippe Deléham_, Nov 25 2008: (Start)",
				"G.f.: x.",
				"E.g.f.: x. (End)",
				"a(n) = mu(n^2). - _Enrique Pérez Herrero_, Sep 04 2009",
				"a(n) = floor(n/A000203(n)) for n \u003e 0. - _Enrique Pérez Herrero_, Nov 11 2009",
				"a(n) = (1-(-1)^(2^abs(n-1)))/2 = (1-(-1)^(2^((n-1)^2)))/2. - _Luce ETIENNE_, Jun 05 2015",
				"a(n) = n*(A057427(n) - A057427(n-1)) = A000007(abs(n-1)). - _Chayim Lowen_, Aug 01 2015",
				"a(n) = A010051(p*n) for any prime p (where A010051(0)=0). - _Chayim Lowen_, Aug 05 2015"
			],
			"maple": [
				"A063524 := proc(n) if n = 1 then 1 else 0; fi; end;"
			],
			"mathematica": [
				"Table[If[n == 1, 1, 0], {n, 0, 104}] (* _Robert G. Wilson v_, Sep 30 2006 *)",
				"LinearRecurrence[{1},{0,1,0},106] (* _Ray Chandler_, Jul 15 2015 *)"
			],
			"program": [
				"(Haskell)",
				"a063524 = fromEnum . (== 1)  -- _Reinhard Zumkeller_, Apr 01 2012",
				"(PARI) a(n)=n==1; \\\\ _Charles R Greathouse IV_, Apr 01 2012"
			],
			"xref": [
				"Cf. A000007, A008683, A000012, A007427, A000005, A007428, A007425, A227005, A227257, A227301, A003763, A209077."
			],
			"keyword": "easy,nonn,mult",
			"offset": "0,1",
			"author": "_Labos Elemer_, Jul 30 2001",
			"references": 139,
			"revision": 98,
			"time": "2020-01-20T21:40:55-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}