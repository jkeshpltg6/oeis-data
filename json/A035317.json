{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A035317",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 35317,
			"data": "1,1,1,1,2,2,1,3,4,2,1,4,7,6,3,1,5,11,13,9,3,1,6,16,24,22,12,4,1,7,22,40,46,34,16,4,1,8,29,62,86,80,50,20,5,1,9,37,91,148,166,130,70,25,5,1,10,46,128,239,314,296,200,95,30,6,1,11,56,174,367,553,610,496,295,125",
			"name": "Pascal-like triangle associated with A000670.",
			"comment": [
				"From _Johannes W. Meijer_, Jul 20 2011: (Start)",
				"The triangle sums, see A180662 for their definitions, link this \"Races with Ties\" triangle with several sequences, see the crossrefs. Observe that the Kn4 sums lead to the golden rectangle numbers A001654 and that the Fi1 and Fi2 sums lead to the Jacobsthal sequence A001045.",
				"The series expansion of G(x, y) = 1/((y*x-1)*(y*x+1)*((y+1)*x-1)) as function of x leads to this sequence, see the second Maple program. (End)",
				"T(2*n+1,n) = A014301(n+1); T(2*n+1,n+1) = A026641(n+1). - _Reinhard Zumkeller_, Jul 19 2012"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A035317/b035317.txt\"\u003eRows n = 0..100, flattened\u003c/a\u003e",
				"A. Hlavác, M. Marvan, \u003ca href=\"https://arxiv.org/abs/1602.06861\"\u003eNonlocal conservation laws of the constant astigmatism equation\u003c/a\u003e, arXiv preprint arXiv:1602.06861 [nlin.SI], 2016.",
				"E. Mendelson, \u003ca href=\"http://www.jstor.org/stable/2690085\"\u003eRaces with Ties\u003c/a\u003e, Math. Mag. 55 (1982), 170-175.",
				"\u003ca href=\"/index/Pas#Pascal\"\u003eIndex entries for triangles and arrays related to Pascal's triangle\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = Sum_{j=0..floor(n/2)} binomial(n-2j, k-2j). - _Paul Barry_, Feb 11 2003",
				"From _Johannes W. Meijer_, Jul 20 2011: (Start)",
				"T(n, k) = Sum_{i=0..k}((-1)^(i+k) * binomial(i+n-k+1,i)). (Mendelson)",
				"T(n, k) = T(n-1, k-1) + T(n-1, k) with T(n, 0) = 1 and T(n, n) = floor(n/2) + 1. (Mendelson)",
				"Sum_{k = 0..n}((-1)^k * (n-k+1)^n * T(n, k)) = A000670(n). (Mendelson)",
				"T(n, n-k) = A128176(n, k); T(n+k, n-k) = A158909(n, k); T(2*n-k, k) = A092879(n, k). (End)"
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  1,  1;",
				"  1,  2,  2;",
				"  1,  3,  4,   2;",
				"  1,  4,  7,   6,   3;",
				"  1,  5, 11,  13,   9,   3;",
				"  1,  6, 16,  24,  22,  12,   4;",
				"  1,  7, 22,  40,  46,  34,  16,   4;",
				"  1,  8, 29,  62,  86,  80,  50,  20,  5;",
				"  1,  9, 37,  91, 148, 166, 130,  70, 25,  5;",
				"  1, 10, 46, 128, 239, 314, 296, 200, 95, 30, 6;",
				"  ..."
			],
			"maple": [
				"A035317 := proc(n,k): add((-1)^(i+k) * binomial(i+n-k+1, i), i=0..k) end: seq(seq(A035317(n,k), k=0..n), n=0..10); # _Johannes W. Meijer_, Jul 20 2011",
				"A035317 := proc(n,k): coeff(coeftayl(1/((y*x-1)*(y*x+1)*((y+1)*x-1)), x=0, n), y, k) end: seq(seq(A035317(n,k), k=0..n), n=0..10); # _Johannes W. Meijer_, Jul 20 2011"
			],
			"mathematica": [
				"t[n_, k_] := (-1)^k*(((-1)^k*(n+2)!*Hypergeometric2F1[1, n+3, k+2, -1])/((k+1)!*(n-k+1)!) + 2^(k-n-2)); Flatten[ Table[ t[n, k], {n, 0, 11}, {k, 0, n}]] (* _Jean-François Alcover_, Dec 14 2011, after _Johannes W. Meijer_ *)"
			],
			"program": [
				"(Haskell)",
				"a035317 n k = a035317_tabl !! n !! k",
				"a035317_row n = a035317_tabl !! n",
				"a035317_tabl = map snd $ iterate f (0, [1]) where",
				"   f (i, row) = (1 - i, zipWith (+) ([0] ++ row) (row ++ [i]))",
				"-- _Reinhard Zumkeller_, Jul 09 2012",
				"(PARI) {T(n,k)=if(n==k,(n+2)\\2,if(k==0,1,if(n\u003ek,T(n-1,k-1)+T(n-1,k))))}",
				"for(n=0,12,for(k=0,n,print1(T(n,k),\",\"));print(\"\")) \\\\ _Paul D. Hanna_, Jul 18 2012",
				"(Sage)",
				"def A035317_row(n):",
				"    @cached_function",
				"    def prec(n, k):",
				"        if k==n: return 1",
				"        if k==0: return 0",
				"        return -prec(n-1,k-1)-sum(prec(n,k+i-1) for i in (2..n-k+1))",
				"    return [(-1)^k*prec(n+2, k) for k in (1..n)]",
				"for n in (1..11): print(A035317_row(n)) # _Peter Luschny_, Mar 16 2016"
			],
			"xref": [
				"Row sums are A000975, diagonal sums are A080239.",
				"Central terms are A014300.",
				"Similar to the triangles A059259, A080242, A108561, A112555.",
				"Cf. A059260.",
				"Triangle sums (see the comments): A000975 (Row1), A059841 (Row2), A080239 (Kn11), A052952 (Kn21), A129696 (Kn22), A001906 (Kn3), A001654 (Kn4), A001045 (Fi1, Fi2), A023435 (Ca2), Gi2 (A193146), A190525 (Ze2), A193147 (Ze3), A181532 (Ze4). - _Johannes W. Meijer_, Jul 20 2011",
				"Cf. A181971."
			],
			"keyword": "nonn,easy,tabl,nice",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _James A. Sellers_"
			],
			"references": 24,
			"revision": 50,
			"time": "2020-02-26T13:48:42-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}