{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A327322",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 327322,
			"data": "1,2,5,7,20,25,26,105,150,125,521,2600,5250,5000,3125,434,2605,6500,8750,6250,3125,13021,91140,273525,455000,459375,262500,109375,8138,65105,227850,455875,568750,459375,218750,78125,36169,325520,1302100,3038000,4558750",
			"name": "Triangular array read by rows: row n shows the coefficients of the polynomial p(x,n) constructed as in Comments; these polynomials form a strong divisibility sequence.",
			"comment": [
				"Suppose q is a rational number such that the number r = sqrt(q) is irrational. The function (r x + r)^n - (r x - 1/r)^n of x can be represented as k*p(x,n), where k is a constant and p(x,n) is a product of nonconstant polynomials having gcd = 1; the sequence p(x,n) is a strong divisibility sequence of polynomials; i.e., gcd(p(x,h),p(x,k)) = p(x,gcd(h,k)).  For A327320, r = sqrt(5).  If x is an integer, then p(x,n) is a strong divisibility sequence of integers."
			],
			"example": [
				"p(x,3) = (1/k)((18 (7 + 20 x + 25 x^2))/(5 sqrt(5))), where k = 18/(5 sqrt(5)).",
				"First six rows:",
				"    1;",
				"    2,    5;",
				"    7,   20,   25;",
				"   26,  105,  150,  125;",
				"  521, 2600, 5250, 5000, 3125;",
				"  434, 2605, 6500, 8750, 6250, 3125;",
				"The first six polynomials, not factored:",
				"1, 2 + 5 x, 7 + 20 x + 25 x^2, 26 + 105 x + 150 x^2 + 125 x^3, 521 + 2600 x + 5250 x^2 + 5000 x^3 + 3125 x^4, 434 + 2605 x + 6500 x^2 + 8750 x^3 + 6250 x^4 + 3125 x^5.",
				"The first six polynomials, factored:",
				"1, 2 + 5 x, 7 + 20 x + 25 x^2, (2 + 5 x) (13 + 20 x + 25 x^2), 521 + 2600 x + 5250 x^2 + 5000 x^3 + 3125 x^4, (2 + 5 x) (7 + 20 x + 25 x^2) (31 + 20 x + 25 x^2)."
			],
			"mathematica": [
				"c[poly_] := If[Head[poly] === Times, Times @@ DeleteCases[(#1 (Boole[",
				"MemberQ[#1, x] || MemberQ[#1, y] || MemberQ[#1, z]] \u0026) /@",
				"Variables /@ #1 \u0026)[List @@ poly], 0], poly];",
				"r = Sqrt[5]; f[x_, n_] := c[Factor[Expand[(r x + r)^n - (r x - 1/r)^n]]];",
				"Table[f[x, n], {n, 1, 6}]",
				"Flatten[Table[CoefficientList[f[x, n], x], {n, 1, 12}]]  (* A327322 *)",
				"(* _Peter J. C. Moses_, Nov 01 2019 *)"
			],
			"xref": [
				"Cf. A327320, A327321."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Nov 08 2019",
			"references": 5,
			"revision": 12,
			"time": "2019-11-23T13:39:45-05:00",
			"created": "2019-11-23T13:39:45-05:00"
		}
	]
}