{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A050322",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 50322,
			"data": "1,1,2,2,3,4,5,7,5,7,9,12,11,11,16,19,21,15,29,26,30,15,31,38,22,47,52,45,36,57,64,30,77,98,67,74,97,66,105,42,109,118,92,109,171,97,141,162,137,165,56,212,181,52,198,189,289,139,250,257,269,254,77,382,267",
			"name": "Number of factorizations indexed by prime signatures: A001055(A025487).",
			"comment": [
				"For A025487(m) = 2^k = A000079(k), we have a(m) = A000041(k).",
				"Is a(k) = A000110(k) for A025487(m) = A002110(k)?"
			],
			"link": [
				"R. J. Mathar and Michael De Vlieger, \u003ca href=\"/A050322/b050322.txt\"\u003eTable of n, a(n) for n = 1..5000\u003c/a\u003e (First 300 terms from _R. J. Mathar_)",
				"R. E. Canfield, P. Erdős and C. Pomerance, \u003ca href=\"http://math.dartmouth.edu/~carlp/PDF/paper39.pdf\"\u003eOn a Problem of Oppenheim concerning \"Factorisatio Numerorum\"\u003c/a\u003e, J. Number Theory 17 (1983), 1-28.",
				"Jun Kyo Kim, \u003ca href=\"https://doi.org/10.1006/jnth.1998.2238\"\u003eOn highly factorable numbers\u003c/a\u003e, Journal Of Number Theory, Vol. 72, No. 1 (1998), pp. 76-91."
			],
			"example": [
				"From _Gus Wiseman_, Jan 13 2020: (Start)",
				"The a(1) = 1 through a(11) = 9 factorizations:",
				"  {}  2  4    6    8      12     16       24       30     32         36",
				"         2*2  2*3  2*4    2*6    2*8      3*8      5*6    4*8        4*9",
				"                   2*2*2  3*4    4*4      4*6      2*15   2*16       6*6",
				"                          2*2*3  2*2*4    2*12     3*10   2*2*8      2*18",
				"                                 2*2*2*2  2*2*6    2*3*5  2*4*4      3*12",
				"                                          2*3*4           2*2*2*4    2*2*9",
				"                                          2*2*2*3         2*2*2*2*2  2*3*6",
				"                                                                     3*3*4",
				"                                                                     2*2*3*3",
				"(End)"
			],
			"maple": [
				"A050322 := proc(n)",
				"    A001055(A025487(n)) ;",
				"end proc: # _R. J. Mathar_, May 25 2017"
			],
			"mathematica": [
				"c[1, r_] := c[1, r] = 1; c[n_, r_] := c[n, r] = Module[{d, i}, d = Select[Divisors[n], 1 \u003c # \u003c= r \u0026]; Sum[c[n/d[[i]], d[[i]]], {i, 1, Length[d]}]]; Map[c[#, #] \u0026, Union@ Table[Times @@ MapIndexed[If[n == 1, 1, Prime[First@ #2]]^#1 \u0026, Sort[FactorInteger[n][[All, -1]], Greater]], {n, Product[Prime@ i, {i, 6}]}]] (* _Michael De Vlieger_, Jul 10 2017, after _Dean Hickerson_ at A001055 *)",
				"facs[n_]:=If[n\u003c=1,{{}},Join@@Table[Map[Prepend[#,d]\u0026,Select[facs[n/d],Min@@#\u003e=d\u0026]],{d,Rest[Divisors[n]]}]];",
				"Length/@facs/@First/@GatherBy[Range[1000],If[#==1,{},Sort[Last/@FactorInteger[#]]]\u0026] (* _Gus Wiseman_, Jan 13 2020 *)"
			],
			"xref": [
				"Cf. A000041, A000079, A000110, A001055, A002110, A025487.",
				"The version indexed by unsorted prime signature is A331049.",
				"The version indexed by prime shadow (A181819, A181821) is A318284.",
				"This sequence has range A045782 (same as A001055).",
				"Cf. A033833, A045778, A045783, A070175, A181821, A325238, A330972, A330973, A330976, A330989, A330990, A330998, A331050."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Christian G. Bower_, Oct 15 1999",
			"references": 8,
			"revision": 19,
			"time": "2020-01-13T13:54:31-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}