{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A295112",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 295112,
			"data": "-1,-1,1,5,13,29,63,139,317,749,1827,4575,11699,30419,80161,213573,574253,1556077,4244835,11647151,32122231,88995879,247573565,691246369,1936445619,5441165699,15331341373,43308322049,122624939677,347957102909,989335822559,2818200111867",
			"name": "a(n) = Sum_{k=0..n} binomial(n,2*k)*binomial(2*k,k)/(2*k-1).",
			"comment": [
				"As binomial(2*k,k) = 2*(2*k-1)*A000108(k-1) for all k = 1,2,..., we see that a(n) is always an odd integer. Clearly, a(n) \u003e 0 for all n \u003e 1. a(n) can be viewed as an analog of Motzkin numbers, which should have some combinatorial interpretations.",
				"Conjecture: The sequence a(n+1)/a(n) (n = 5,6,...) is strictly increasing with limit 3, and the sequence a(n+1)^(1/(n+1))/a(n)^(1/n) (n = 9,10,...) is strictly decreasing to the limit 1.",
				"See also A295113 for a conjecture involving the current sequence."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A295112/b295112.txt\"\u003eTable of n, a(n) for n = 0..400\u003c/a\u003e",
				"Min Bian, Olivia X. M. Yao, Yan Zhang, Alina F. Y. Zhao, \u003ca href=\"https://doi.org/10.11575/cdm.v14i1.62700\"\u003eProof of a conjecture of Z. W. Sun\u003c/a\u003e, Contributions to Discrete Mathematics (2019) Vol. 14, No. 1, 214-221.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1208.2683\"\u003eConjectures involving arithmetical sequences\u003c/a\u003e, arXiv:1208.2683 [math.CO], 2012-2013; in: Number Theory: Arithmetic in Shangri-La (eds., S. Kanemitsu, H. Li and J. Liu), Proc. 6th China-Japan Seminar  (Shanghai, August 15-17, 2011), World Sci., Singapore, 2013, pp. 244-258."
			],
			"formula": [
				"Via the Zeilberger algorithm we have the recurrence: (n+3)*a(n+3) = (3n+7)*a(n+2) + (n-5)*a(n+1) - 3*(n+1)*a(n) for any nonnegative integer n.",
				"a(n) = -hypergeom([-1/2, 1/2 - n/2, -n/2], [1/2, 1], 4). - _Peter Luschny_, Nov 15 2017",
				"a(n) ~ 3^(n + 3/2) / (4*sqrt(Pi)*n^(3/2)). - _Vaclav Kotesovec_, Nov 15 2017"
			],
			"example": [
				"a(3) = 5 since binomial(3,2*0)*binomial(2*0,0)/(2*0-1) + binomial(3,2*1)*binomial(2*1,1)/(2*1-1) = -1 + 3*2 = 5."
			],
			"maple": [
				"a := n -\u003e -hypergeom([-1/2, 1/2 - n/2, -n/2], [1/2, 1], 4):",
				"seq(simplify(a(n)), n=0..31); # _Peter Luschny_, Nov 15 2017"
			],
			"mathematica": [
				"W[n_]:=W[n]=Sum[Binomial[n,2k]Binomial[2k,k]/(2k-1),{k,0,n/2}]; Table[W[n],{n,0,35}]",
				"a[n_] := -AppellF1[-n, -1/2, -1/2, 1, 2, -2]; Table[a[n], {n,0,31}] (* _Peter Luschny_, Nov 15 2017 *)"
			],
			"xref": [
				"Cf. A000108, A001006, A295113."
			],
			"keyword": "sign,easy",
			"offset": "0,4",
			"author": "_Zhi-Wei Sun_, Nov 14 2017",
			"references": 3,
			"revision": 40,
			"time": "2020-03-13T16:44:11-04:00",
			"created": "2017-11-15T03:53:03-05:00"
		}
	]
}