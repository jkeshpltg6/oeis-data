{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A115994",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 115994,
			"data": "1,2,3,4,1,5,2,6,5,7,8,8,14,9,20,1,10,30,2,11,40,5,12,55,10,13,70,18,14,91,30,15,112,49,16,140,74,1,17,168,110,2,18,204,158,5,19,240,221,10,20,285,302,20,21,330,407,34,22,385,536,59,23,440,698,94,24,506,896,149,25",
			"name": "Triangle read by rows: T(n,k) is number of partitions of n with Durfee square of size k (n\u003e=1; 1\u003c=k\u003c=floor(sqrt(n))).",
			"comment": [
				"Row n has floor(sqrt(n)) terms. Row sums yield A000041. Column 2 yields A006918. sum(k*T(n,k),k=1..floor(sqrt(n)))=A115995.",
				"T(n,k) is number of partitions of n-k^2 into parts of 2 kinds with at most k of each kind.",
				"The limit of the diagonals is A000712 (partitions into parts of two kinds). In particular, if 0\u003c=m\u003c=n, T(n(n+1)/2 + m, n) = A000712(m). These partitions in this range can be viewed as an equilateral right triangle of side n, with one partition appended on the top (at the left) and another appended on the right. - _Franklin T. Adams-Watters_, Jan 11 2006",
				"Successive columns approach closer and closer to A000712. - _N. J. A. Sloane_, Mar 10 2007"
			],
			"reference": [
				"G. E. Andrews, The Theory of Partitions, Addison-Wesley, 1976 (pp. 27-28).",
				"G. E. Andrews and K. Eriksson, Integer Partitions, Cambridge Univ. Press, 2004 (pp. 75-78)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A115994/b115994.txt\"\u003eRows n = 1..620, flattened\u003c/a\u003e",
				"E. R. Canfield, \u003ca href=\"http://dx.doi.org/10.1016/j.aam.2004.08.008\"\u003eFrom recursions to asymptotics: Durfee and dilogarithmic deductions\u003c/a\u003e, Advances in Applied Mathematics, Volume 34, Issue 4, May 2005, Pages 768-797",
				"E. R. Canfield, S. Corteel, C. D. Savage, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v5i1r32\"\u003eDurfee Polynomials\u003c/a\u003e, Electronic Journal of Combinatorics 5 (1998), #R32.",
				"S. B. Ekhad, D. Zeilberger, \u003ca href=\"http://arxiv.org/abs/1411.0002\"\u003eA Quick Empirical Reproof of the Asymptotic Normality of the Hirsch Citation Index (First proved by Canfield, Corteel, and Savage)\u003c/a\u003e, arXiv preprint arXiv:1411.0002, 2014.",
				"P. Flajolet and R. Sedgewick, \u003ca href=\"http://algo.inria.fr/flajolet/Publications/AnaCombi/anacombi.html\"\u003eAnalytic Combinatorics\u003c/a\u003e, 2009, page 45",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DurfeeSquare.html\"\u003eDurfee Square.\u003c/a\u003e"
			],
			"formula": [
				"G.f.: sum(k\u003e=1, t^k*q^(k^2)/product(j=1..k, (1-q^j)^2 ) ).",
				"T(n,k) = Sum_{i=0}^{n-k^2} P*(i,k)*P*(n-k^2-i), where P*(n,k) = P(n+k,k) is the number of partitions of n objects into at most k parts."
			],
			"example": [
				"T(5,2) = 2 because the only partitions of 5 having Durfee square of size 2 are [3,2] and [2,2,1]; the other five partitions ([5], [4,1], [3,1,1], [2,1,1,1] and [1,1,1,1,1]) have Durfee square of size 1.",
				"Triangle starts:",
				"  1;",
				"  2;",
				"  3;",
				"  4,  1;",
				"  5,  2;",
				"  6,  5;",
				"  7,  8;",
				"  8, 14;",
				"  9, 20,  1;",
				"  ..."
			],
			"maple": [
				"g:=sum(t^k*q^(k^2)/product((1-q^j)^2,j=1..k),k=1..40): gser:=series(g,q=0,32): for n from 1 to 27 do P[n]:=coeff(gser,q^n) od: for n from 1 to 27 do seq(coeff(P[n],t^j),j=1..floor(sqrt(n))) od; # yields sequence in triangular form",
				"# second Maple program:",
				"b:= proc(n, i) option remember;",
				"      `if`(n=0, 1, `if`(i\u003c1, 0, b(n, i-1)+`if`(i\u003en, 0, b(n-i, i))))",
				"    end:",
				"T:= (n, k)-\u003e add(b(m, k)*b(n-k^2-m, k), m=0..n-k^2):",
				"seq(seq(T(n, k), k=1..floor(sqrt(n))), n=1..30); # _Alois P. Heinz_, Apr 09 2012"
			],
			"mathematica": [
				"Map[Select[#,#\u003e0\u0026]\u0026,Drop[Transpose[Table[CoefficientList[ Series[x^(n^2)/Product[1-x^i,{i,1,n}]^2,{x,0,nn}],x],{n,1,10}]],1]] //Grid (* _Geoffrey Critzer_, Sep 27 2013 *)",
				"b[n_, i_] := b[n, i] = If[n==0, 1, If[i\u003c1, 0, b[n, i-1] + If[i\u003en, 0, b[n-i, i]]]]; T[n_, k_] := Sum[b[m, k]*b[n-k^2-m, k], {m, 0, n-k^2}]; Table[T[n, k], {n, 1, 30}, {k, 1, Sqrt[n]}] // Flatten (* _Jean-François Alcover_, Dec 25 2015, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"For another version see A115720. Row lengths A000196.",
				"Cf. A115995, A115721, A115722, A008284, A006918."
			],
			"keyword": "nonn,tabf",
			"offset": "1,2",
			"author": "_Emeric Deutsch_, Feb 11 2006",
			"ext": [
				"Edited and verified by _Franklin T. Adams-Watters_, Mar 11 2006"
			],
			"references": 41,
			"revision": 41,
			"time": "2019-04-10T17:18:33-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}