{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A163937",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 163937,
			"data": "1,1,2,2,10,3,6,52,43,4,24,308,472,136,5,120,2088,4980,2832,369,6,720,16056,53988,49808,13638,918,7,5040,138528,616212,826160,381370,57540,2167,8,40320,1327392,7472952,13570336,9351260,2469300,222908,4948,9",
			"name": "Triangle related to the o.g.f.s. of the right-hand columns of A028421 (E(x,m=2,n)).",
			"comment": [
				"The asymptotic expansions of the higher-order exponential integral E(x,m=2,n) lead to triangle A028421, see A163931 for information on E(x,m,n). The o.g.f.s. of the right-hand columns of triangle A028421 have a nice structure: gf(p) = W2(z,p)/(1-z)^(2*p) with p = 1 for the first right-hand column, p = 2 for the second right-hand column, etc. The coefficients of the W2(z,p) polynomials lead to the triangle given above, n \u003e= 1 and 1 \u003c= m \u003c= n. The row sums of this triangle lead to A001147 (minus a(0)), see A163936 for more information."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A163937/b163937.txt\"\u003eTable of n, a(n) for the first 50 rows, flattened\u003c/a\u003e"
			],
			"formula": [
				"a(n,m) = Sum_{k=0..(m-1)} (-1)^(n+k+1)*((m-k)/1!)*binomial(2*n,k)*Stirling1(m+n-k-1,m-k), 1 \u003c= m \u003c= n."
			],
			"example": [
				"The first few W2(z,p) polynomials are",
				"W2(z,p=1) = 1/(1-z)^2;",
				"W2(z,p=2) = (1 +  2*z)/(1-z)^4;",
				"W2(z,p=3) = (2 + 10*z +  3*z^2)/(1-z)^6;",
				"W2(z,p=4) = (6 + 52*z + 43*z^2 + 4*z^3)/(1-z)^8."
			],
			"maple": [
				"with(combinat): a := proc(n, m): add((-1)^(n+k+1)*((m-k)/1!)*binomial(2*n, k)*stirling1(m+n-k-1, m-k), k=0..m-1) end: seq(seq(a(n, m), m=1..n), n=1..9);  # _Johannes W. Meijer_, revised Nov 27 2012"
			],
			"mathematica": [
				"Table[Sum[(-1)^(n + k + 1)*((m - k)/1!)*Binomial[2*n, k]*StirlingS1[m + n - k - 1, m - k], {k, 0, m - 1}], {n, 1, 10}, {m, 1, n}] // Flatten (* _G. C. Greubel_, Aug 13 2017 *)"
			],
			"program": [
				"(PARI) for(n=1,10, for(m=1,n, print1(sum(k=0,m-1, (-1)^(n+k+1)*((m-k)/1!)*binomial(2*n,k) *stirling1(m+n-k-1,m-k)), \", \"))) \\\\ _G. C. Greubel_, Aug 13 2017"
			],
			"xref": [
				"Row sums equal A001147 (n\u003e=1).",
				"A000142, 2*A001705, are the first two left hand columns.",
				"A000027 is the first right hand column.",
				"Cf. A163931 (E(x,m,n)) and A028421.",
				"Cf. A163936 (E(x,m=1,n)), A163938 (E(x,m=3,n)) and A163939 (E(x,m=4,n))."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "1,3",
			"author": "_Johannes W. Meijer_, Aug 13 2009",
			"references": 5,
			"revision": 11,
			"time": "2019-05-11T18:37:20-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}