{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A300840",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 300840,
			"data": "1,1,2,3,4,2,5,3,7,4,9,6,11,5,8,13,16,7,17,12,10,9,19,6,23,11,14,15,25,8,29,13,18,16,20,21,31,17,22,12,37,10,41,27,28,19,43,26,47,23,32,33,49,14,36,15,34,25,53,24,59,29,35,39,44,18,61,48,38,20,67,21,71,31,46,51,45,22,73,52,79,37,81,30,64,41,50,27",
			"name": "Fermi-Dirac factorization prime shift towards smaller terms: a(n) = A052330(floor(A052331(n)/2)).",
			"comment": [
				"With n having a unique factorization as fdp(i) * fdp(j) * ... * fdp(k), with i, j, ..., k all distinct, a(n) = fdp(i-1) * fdp(j-1) * ... * fdp(k-1), where fdp(0) = 1 and fdp(n) = A050376(n) for n \u003e= 1.",
				"Multiplicative because for coprime m and n the Fermi-Dirac factorizations of m and n are disjoint and their union is the Fermi-Dirac factorization of m * n. - _Andrew Howroyd_, Aug 02 2018"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A300840/b300840.txt\"\u003eTable of n, a(n) for n = 1..65537\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A052330(floor(A052331(n)/2)).",
				"For all n \u003e= 1, a(A300841(n)) = n.",
				"a(A059897(n,k)) = A059897(a(n), a(k)). - _Peter Munn_, Nov 30 2019"
			],
			"program": [
				"(PARI)",
				"up_to_e = 8192;",
				"v050376 = vector(up_to_e);",
				"A050376(n) = v050376[n];",
				"ispow2(n) = (n \u0026\u0026 !bitand(n,n-1));",
				"i = 0; for(n=1,oo,if(ispow2(isprimepower(n)), i++; v050376[i] = n); if(i == up_to_e,break));",
				"A052330(n) = { my(p=1,i=1); while(n\u003e0, if(n%2, p *= A050376(i)); i++; n \u003e\u003e= 1); (p); };",
				"A052331(n) = { my(s=0,e); while(n \u003e 1, fordiv(n, d, if(((n/d)\u003e1)\u0026\u0026ispow2(isprimepower(n/d)), e = vecsearch(v050376, n/d); if(!e, print(\"v050376 too short!\"); return(1/0)); s += 2^(e-1); n = d; break))); (s); };",
				"A300840(n) = A052330(A052331(n)\u003e\u003e1);"
			],
			"xref": [
				"A left inverse of A300841.",
				"Cf. A050376, A052330, A052331, A059897.",
				"Cf. also A064989."
			],
			"keyword": "nonn,mult",
			"offset": "1,3",
			"author": "_Antti Karttunen_, Apr 13 2018",
			"references": 10,
			"revision": 16,
			"time": "2020-01-20T21:43:30-05:00",
			"created": "2018-04-13T07:46:25-04:00"
		}
	]
}