{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A226302",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 226302,
			"data": "1,-1,2,-4,6,-14,20,-48,70,-166,252,-584,924,-2092,3432,-7616,12870,-28102,48620,-104824,184756,-394404,705432,-1494240,2704156,-5692636,10400600,-21785872,40116600,-83688344,155117520,-322494208,601080390,-1246068806,2333606220",
			"name": "a(n) = P_n(-1), where P_n(x) is a certain polynomial arising in the enumeration of tatami mat coverings.",
			"comment": [
				"See Erickson-Ruskey for precise definition. The polynomials P_n(x) are described as \"mysterious\".",
				"Bisections give A082590 and A000984."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A226302/b226302.txt\"\u003eTable of n, a(n) for n = 2..1000\u003c/a\u003e",
				"A. Erickson and F. Ruskey, \u003ca href=\"http://arxiv.org/abs/1304.0070\"\u003eEnumerating maximal tatami mat coverings of square grids with v vertical dominoes\u003c/a\u003e, arXiv:1304.0070 [math.CO], 2013"
			],
			"formula": [
				"Conjecture: (-n+2)*a(n) +(-n+2)*a(n-1) +2*(3*n-11)*a(n-2) +2*(3*n-14)*a(n-3) +4*(-2*n+9)*a(n-4) +8*(-n+6)*a(n-5)=0. - _R. J. Mathar_, Nov 06 2013",
				"G.f. (for offset 0): 1/sqrt(1-4*x^2) - x/((1-2*x^2)*sqrt(1-4*x^2)) = 1 - x/W(0), where W(k)= 1 - 2*x^2 - 2*x*(1 - 2*x^2)^2*(2*k+1)/( 2*x*(1 - 2*x^2)*(2*k+1) - (k+1)/(1 - x/W(k+1) )); (continued fraction). - _Sergei N. Gladkovskii_, Jun 13 2015",
				"Recurrence (for n\u003e5): (n-5)*(n-2)*a(n) = -2*(n-4)*a(n-1) + 2*(n-5)*(3*n-10)*a(n-2) + 4*(n-4)*a(n-3) - 8*(n-5)*(n-4)*a(n-4). - _Vaclav Kotesovec_, Jun 14 2015",
				"a(n) ~ (-1)^n * 2^(n-3/2) / sqrt(Pi*n). - _Vaclav Kotesovec_, Jun 14 2015"
			],
			"maple": [
				"A226302 := proc(n)",
				"    if type(n,even) then",
				"        A000984(n/2-1) ;",
				"    else",
				"        -A082590((n-3)/2) ;",
				"    end if;",
				"end proc: # _R. J. Mathar_, Nov 06 2013"
			],
			"mathematica": [
				"Rest[Rest[CoefficientList[Series[x^2*(1/Sqrt[1-4*x^2] - x/((1-2*x^2)*Sqrt[1-4*x^2])), {x, 0, 30}], x]]] (* _Vaclav Kotesovec_, Jun 14 2015, after _Sergei N. Gladkovskii_ *)",
				"max = 30; Clear[g]; g[max + 2] = 1; g[k_] := g[k] = 1 - 2*x^2 - 2*x*(1 - 2*x^2)^2*(2*k+1)/( 2*x*(1 - 2*x^2)*(2*k+1) - (k+1)/(1 - x/g[k+1] )); gf = 1 - x/g[0]; CoefficientList[Series[gf, {x, 0, max}], x] (* _Vaclav Kotesovec_, Jun 14 2015, after _Sergei N. Gladkovskii_ *)",
				"a = DifferenceRoot[Function[{a, n}, {(-(6*n^2) + 2*n + 4)*a[n+2] + (n^2 + n - 2)*a[n+4] + 8*(n - 1)*n*a[n] - 4*n*a[n+1] + 2*n*a[n+3] == 0, a[2] == 1, a[3] == -1, a[4] == 2, a[5] == -4}]]; Table[a[n], {n, 2, 36}] (* _Jean-François Alcover_, Feb 23 2019 *)"
			],
			"program": [
				"(PARI) Vec(x^2*(1/sqrt(1-4*x^2) - x/((1-2*x^2)*sqrt(1-4*x^2))) + O(x^50)) \\\\ _G. C. Greubel_, Jan 29 2017"
			],
			"xref": [
				"Cf. A226303, A226304, A082590, A000984."
			],
			"keyword": "sign",
			"offset": "2,3",
			"author": "_N. J. A. Sloane_, Jun 06 2013",
			"references": 5,
			"revision": 29,
			"time": "2019-02-23T04:28:25-05:00",
			"created": "2013-06-06T17:19:53-04:00"
		}
	]
}