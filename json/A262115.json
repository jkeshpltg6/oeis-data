{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A262115",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 262115,
			"data": "0,0,1,0,1,0,2,1,2,0,2,1,0,3,2,4,1,2,0,5,1,1,1,2,5,1,4,2,8,5,7,1,6,3,1,8,6,10,3,5,1,11,2,2,2,4,9,2,7,4,14,9,12,2,10,5,2,13,10,16,5,8,2,17,3,3,3,6,13,3,10,6,20,13,17,3,14,7,3,18,14,22,7,11,3,23,4,4,4,8,17",
			"name": "Irregular triangle read by rows: row b (b \u003e= 2) gives periodic part of digits of the base-b expansion of 1/7.",
			"comment": [
				"The number of terms associated with a particular value of b are cyclical: 3, 5, 3, 5, 2, 1, 1, repeat. This is because the values are associated with b (mod 7), starting with 2 (mod 7).",
				"The expansion of 1/7 either terminates after one digit when b == 0 (mod 7) or is purely recurrent in all other cases of b (mod 7), since 7 is prime and must either divide or be coprime to b.",
				"The period for purely recurrent expansions of 1/7 must be a divisor of Euler's totient of 7 = 6, i.e., one of {1, 2, 3, 6}.",
				"b == 0 (mod 7): 1 (terminating)",
				"b == 1 (mod 7): 1 (purely recurrent)",
				"b == 2 (mod 7): 3 (purely recurrent)",
				"b == 3 (mod 7): 6 (purely recurrent)",
				"b == 4 (mod 7): 3 (purely recurrent)",
				"b == 5 (mod 7): 6 (purely recurrent)",
				"b == 6 (mod 7): 2 (purely recurrent)",
				"The expansion of 1/7 has a full-length period 6 when base b is a primitive root of p = 7.",
				"Digits of 1/7 for the following bases:",
				"2    0, 0, 1",
				"3    0, 1, 0, 2, 1, 2",
				"4    0, 2, 1",
				"5    0, 3, 2, 4, 1, 2",
				"6    0, 5",
				"7*   1",
				"8    1",
				"9    1, 2, 5",
				"10   1, 4, 2, 8, 5, 7",
				"11   1, 6, 3",
				"12   1, 8, 6, 10, 3, 5",
				"13   1, 11",
				"14*  2",
				"15   2",
				"16   2, 4, 9",
				"17   2, 7, 4, 14, 9, 12",
				"18   2, 10, 5",
				"19   2, 13, 10, 16, 5, 8",
				"20   2, 17",
				"21*  3",
				"...",
				"Asterisks above denote terminating expansion; all other entries are digits of purely recurrent reptends.",
				"Each entry associated with base b with more than one term has a second term greater than the first except for b = 2, where the first two terms are 0, 0.",
				"Entries for b == 0 (mod 7) (i.e., integer multiples of 7) appear at 21, 43, 65, ..., every 22nd term thereafter."
			],
			"reference": [
				"U. Dudley, Elementary Number Theory, 2nd ed., Dover, 2008, pp. 119-126.",
				"G. H. Hardy and E. M. Wright, An Introduction to the Theory of Numbers. 6th ed., Oxford Univ. Press, 2008, pp. 138-148.",
				"Oystein Ore, Number Theory and Its History, Dover, 1988, pp. 311-325."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A262115/b262115.txt\"\u003eTable of n, a(n) for n = 2..10000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DecimalPeriod.html\"\u003eDecimal Period\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RepeatingDecimal.html\"\u003eRepeating Decimal\u003c/a\u003e."
			],
			"formula": [
				"Conjectures from _Colin Barker_, Oct 09 2015: (Start)",
				"a(n) = 2*a(n-22) - a(n-44) for n\u003e44.",
				"G.f.: x^3*(x^39 +x^38 +x^37 +x^36 +2*x^35 +2*x^34 +2*x^33 +x^32 +x^31 +2*x^30 +x^29 +3*x^28 +3*x^27 +4*x^26 +2*x^25 +2*x^24 +x^23 +3*x^22 +2*x^21 +x^20 +x^19 +x^18 +5*x^17 +2*x^15 +x^14 +4*x^13 +2*x^12 +3*x^11 +x^9 +2*x^8 +2*x^6 +x^5 +2*x^4 +x^2 +1) / (x^44 -2*x^22 +1).",
				"(End)",
				"From _Robert Israel_, Dec 04 2015: (Start)",
				"To prove the recursion, note that if a(n) is the k'th digit in the base-b expansion of 1/7, then a(n+22) and a(n+44) are the corresponding digits in the base-(b+7) and base-(b+14) expansions.",
				"The one digit in the base-(7k) expansion of 1/7 is k.",
				"For each d from 1 to 6, one can show that the digits in the base-(7k+d) expansion of ((7k+d)^p - 1)/7 where p is the order of d mod 7, and thus the digits of 1/7, are linear expressions in k.",
				"Thus for d=3, these digits are [5k+2, 4k+1, 6k+2, 2k, 3k+1, k], since those are nonnegative integers \u003c 7k+3 and (5k+2) + (4k+1)*(7k+3) + (6k+2)*(7k+3)^2 + (2k)*(7k+3)^3 + (3k+1)*(7k+3)^4 + k*(7k+3)^5 = ((7*k+3)^6 - 1)/7.",
				"The g.f. follows from the recursion. (End)"
			],
			"example": [
				"For b = 8, 1/7 = .111..., contributing the term 1 to the sequence.",
				"For b = 9, 1/7 = .125125..., thus 1, 2, 5 are the next terms in the sequence.",
				"For b = 10, 1/7 = .142857142857..., thus 1, 4, 2, 8, 5, 7 are terms that follow in the sequence."
			],
			"maple": [
				"F:= proc(N) # to get rows for bases 2 to N, flattened.",
				"  local b, R, p, L;",
				"  R:= NULL;",
				"  for b from 2 to N do",
				"    if b mod 7 = 0 then",
				"      R:= R, b/7",
				"    else",
				"      p:= numtheory:-order(b, 7);",
				"      L:= convert((b^p-1)/7, base, b);",
				"      if nops(L) \u003c p then L:= [op(L), 0$ (p - nops(L))] fi;",
				"      R:= R, op(ListTools:-Reverse(L));",
				"    fi",
				"  od:",
				"  R;",
				"end proc:",
				"F(100); # _Robert Israel_, Dec 04 2015"
			],
			"mathematica": [
				"RotateLeft[Most@ #, Last@ #] \u0026@ Flatten@ RealDigits[1/7, #] \u0026 /@ Range[2, 30] // Flatten (* _Michael De Vlieger_, Sep 11 2015 *)"
			],
			"xref": [
				"Cf. A004526 Digits of expansions of 1/2.",
				"Cf. A026741 Full reptends of 1/3.",
				"Cf. A130845 Digits of expansions of 1/3 (eliding first 2 terms).",
				"Cf. A262114 Digits of expansions of 1/5."
			],
			"keyword": "nonn,base,tabf,easy",
			"offset": "2,7",
			"author": "_Michael De Vlieger_, Sep 11 2015",
			"references": 2,
			"revision": 27,
			"time": "2018-01-17T16:43:33-05:00",
			"created": "2015-12-04T23:16:43-05:00"
		}
	]
}