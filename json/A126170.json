{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A126170",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 126170,
			"data": "126,846,1260,7920,8460,11760,10856,14595,17700,43632,45888,49308,83142,62700,71145,73962,96576,83904,107550,88730,178800,112672,131100,125856,168730,149952,196650,203432,206752,224928,306612,365700,399592,419256,460640,548550",
			"name": "Larger member of an infinitary amicable pair.",
			"comment": [
				"A divisor of n is called infinitary if it is a product of divisors of the form p^{y_a 2^a}, where p^y is a prime power dividing n and sum_a y_a 2^a is the binary representation of y."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A126170/b126170.txt\"\u003eTable of n, a(n) for n = 1..7916\u003c/a\u003e",
				"Jan Munch Pedersen, \u003ca href=\"http://62.198.248.44/aliquot/tables.htm\"\u003eTables of Aliquot Cycles\u003c/a\u003e."
			],
			"formula": [
				"The values of n for which isigma(m)=isigma(n)=m+n and n\u003em."
			],
			"example": [
				"a(5)=8460 because the fifth infinitary amicable pair is (5940,8460) and 8460 is its largest member."
			],
			"mathematica": [
				"ExponentList[n_Integer, factors_List] := {#, IntegerExponent[n, # ]} \u0026 /@ factors; InfinitaryDivisors[1] := {1}; InfinitaryDivisors[n_Integer?Positive] := Module[ { factors = First /@ FactorInteger[n], d = Divisors[n] }, d[[Flatten[Position[ Transpose[ Thread[Function[{f, g}, BitOr[f, g] == g][ #, Last[ # ]]] \u0026 /@ Transpose[Last /@ ExponentList[ #, factors] \u0026 /@ d]], _?( And @@ # \u0026), {1}]] ]] ] Null; properinfinitarydivisorsum[k_] := Plus @@ InfinitaryDivisors[k] - k; InfinitaryAmicableNumberQ[k_] := If[Nest[properinfinitarydivisorsum, k, 2] == k \u0026\u0026 ! properinfinitarydivisorsum[k] == k, True, False]; data1 = Select[ Range[10^6], InfinitaryAmicableNumberQ[ # ] \u0026]; data2 = properinfinitarydivisorsum[ # ] \u0026 /@ data1; data3 = Table[{data1[[k]], data2[[k]]}, {k, 1, Length[data1]}]; data4 = Select[data3, First[ # ] \u003c Last[ # ] \u0026]; Table[Last[data4[[k]]], {k, 1, Length[data4]}]",
				"fun[p_, e_] := Module[{b = IntegerDigits[e, 2]}, m = Length[b]; Product[If[b[[j]] \u003e 0, 1 + p^(2^(m - j)), 1], {j, 1, m}]]; infs[n_] := Times @@ (fun @@@ FactorInteger[n]) - n; s = {}; Do[k = infs[n]; If[k \u003e n \u0026\u0026 infs[k] == n, AppendTo[s, k]], {n, 2, 10^5}]; s (* _Amiram Eldar_, Jan 22 2019 *)"
			],
			"xref": [
				"Cf. A126169, A049417, A126168, A037445."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Ant King_, Dec 21 2006",
			"ext": [
				"a(33)-a(36) from _Amiram Eldar_, Jan 22 2019"
			],
			"references": 14,
			"revision": 12,
			"time": "2019-01-24T03:42:11-05:00",
			"created": "2007-01-12T03:00:00-05:00"
		}
	]
}