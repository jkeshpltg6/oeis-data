{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A232433",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 232433,
			"data": "1,1,2,1,6,6,2,1,24,36,22,14,6,2,1,120,240,210,160,104,56,32,14,6,2,1,720,1800,2040,1830,1448,992,674,408,232,128,68,32,14,6,2,1,5040,15120,21000,21840,19824,15834,12144,8758,5904,3860,2442,1482,870,492,260,142,68,32,14,6,2,1",
			"name": "E.g.f. satisfies: A(x,q) = exp( Integral A(x,q)*A(q*x,q) dx ).",
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A232433/b232433.txt\"\u003eTable of n, a(n) for n = 0..9919; rows 0..39 in flattened triangle.\u003c/a\u003e",
				"Miguel A. Mendez, \u003ca href=\"https://arxiv.org/abs/1610.03602\"\u003eCombinatorial differential operators in: Faà di Bruno formula, enumeration of ballot paths, enriched rooted trees and increasing rooted trees\u003c/a\u003e, arXiv:1610.03602 [math.CO], 2016."
			],
			"formula": [
				"E.g.f. satisfies: d/dx A(x,q) = A(x,q)^2 * A(q*x,q).",
				"Row sums equal the odd double factorials.",
				"Limit of reversed rows yield A232434."
			],
			"example": [
				"E.g.f.: A(x,q) = 1 + (1)*x + (2 + q)*x^2/2! + (6 + 6*q + 2*q^2 + q^3)*x^3/3!",
				"+ (24 + 36*q + 22*q^2 + 14*q^3 + 6*q^4 + 2*q^5 + q^6)*x^4/4!",
				"+ (120 + 240*q + 210*q^2 + 160*q^3 + 104*q^4 + 56*q^5 + 32*q^6 + 14*q^7 + 6*q^8 + 2*q^9 + q^10)*x^5/5! +...",
				"The triangle of coefficients T(n,k) of x^n*q^k, for n\u003e=0, k=0..n*(n-1)/2, in e.g.f. A(x,q) begins:",
				"[1];",
				"[1];",
				"[2, 1];",
				"[6, 6, 2, 1];",
				"[24, 36, 22, 14, 6, 2, 1];",
				"[120, 240, 210, 160, 104, 56, 32, 14, 6, 2, 1];",
				"[720, 1800, 2040, 1830, 1448, 992, 674, 408, 232, 128, 68, 32, 14, 6, 2, 1];",
				"[5040, 15120, 21000, 21840, 19824, 15834, 12144, 8758, 5904, 3860, 2442, 1482, 870, 492, 260, 142, 68, 32, 14, 6, 2, 1];",
				"[40320, 141120, 231840, 275520, 280056, 251496, 212112, 170424, 129716, 95248, 67632, 46616, 31280, 20576, 13142, 8232, 5004, 2954, 1706, 966, 524, 276, 142, 68, 32, 14, 6, 2, 1]; ...",
				"The limit of the reversed rows (A232434) begins:",
				"[1, 2, 6, 14, 32, 68, 142, 276, 542, 1022, 1876, 3394, 6066, 10628, ...]."
			],
			"mathematica": [
				"nmax = 8; A[_, _] = 0; Do[A[x_, q_] = Exp[Integrate[A[x, q] A[q x, q], x]] + O[x]^n // Normal // Simplify, {n, nmax}];",
				"CoefficientList[#, q]\u0026 /@ (CoefficientList[A[x, q], x] Range[0, nmax-1]!) // Flatten (* _Jean-François Alcover_, Oct 27 2018 *)"
			],
			"program": [
				"(PARI) {T(n,k)=local(A=1+x);for(i=1,n,A=exp(intformal(A*subst(A,x,x*y +x*O(x^n)),x)));n!*polcoeff(polcoeff(A,n,x),k,y)}",
				"for(n=0,12,for(k=0,n*(n-1)/2,print1(T(n,k),\", \"));print(\"\"))"
			],
			"xref": [
				"Cf. A232434, A126470, A001286, A075183."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_Paul D. Hanna_, Nov 23 2013",
			"references": 2,
			"revision": 14,
			"time": "2018-10-27T06:19:32-04:00",
			"created": "2013-11-23T18:04:41-05:00"
		}
	]
}