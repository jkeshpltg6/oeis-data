{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A129467",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 129467,
			"data": "1,0,1,0,-2,1,0,12,-8,1,0,-144,108,-20,1,0,2880,-2304,508,-40,1,0,-86400,72000,-17544,1708,-70,1,0,3628800,-3110400,808848,-89280,4648,-112,1,0,-203212800,177811200,-48405888,5808528,-349568,10920,-168,1,0,14631321600,-13005619200",
			"name": "Orthogonal polynomials with all zeros integers from 2*A000217.",
			"comment": [
				"The row polynomials p(n,x)=sum(a(n,m)*x^m,m=0..n) have the n integer zeros 2*A000217(j),j=0..n-1.",
				"The row polynomials satisfy a three-term recurrence relation which qualify them as orthogonal polynomials w.r.t. some (as yet unknown) positive measure.",
				"Column sequences (without leading zeros) give A000007, A010790(n-1)*(-1)^(n-1), A084915(n-1)*(-1)^(n-2), A130033 for m=0..3.",
				"Apparently this is the triangle read by rows of Legendre-Stirling numbers of the first kind. See the Andrews-Gawronski-Littlejohn paper, table 2. The mirror version is the triangle A191936. - _Omar E. Pol_, Jan 10 2012"
			],
			"link": [
				"G. E. Andrews, W. Gawronski and L. L. Littlejohn, \u003ca href=\"http://www.math.psu.edu/andrews/pdf/283.pdf\"\u003eThe Legendre-Stirling Numbers\u003c/a\u003e",
				"M. Bruschi, F. Calogero and R. Droghei, \u003ca href=\"http://dx.doi.org/10.1088/1751-8113/40/14/005\"\u003eProof of certain Diophantine conjectures and identification of remarkable classes of orthogonal polynomials\u003c/a\u003e, J. Physics A, 40(2007), pp. 3815-3829.",
				"M. W. Coffey, M. C. Lettington, \u003ca href=\"http://arxiv.org/abs/1510.05402\"\u003eOn Fibonacci Polynomial Expressions for Sums of mth Powers, their implications for Faulhaber's Formula and some Theorems of Fermat\u003c/a\u003e, arXiv:1510.05402 [math.NT], 2015.",
				"W. Lang, \u003ca href=\"/A129467/a129467.txt\"\u003eFirst ten rows and more.\u003c/a\u003e"
			],
			"formula": [
				"Row polynomials p(n,x):=product(x-m*(m-1),m=1..n), n\u003e=1, p(0,x):=1.",
				"Row polynomials p(n,x):= p(n,v=n,x) with the recurrence: p(n,v,x) = (x+2*(n-1)^2-2*(v-1)*(n-1)-v+1)*p(n-1,v,x) -((n-1)^2)*((n-1-v)^2)*p(n-2,v,x)) with p(-1,v,x)=0 and p(0,v,x)=1.",
				"a(n,m)=[x^m] p(n,n,x), n\u003e=m\u003e=0, else 0."
			],
			"example": [
				"Triangle starts:",
				"[1];",
				"[0,1];",
				"[0,-2,1];",
				"[0,12,-8,1];",
				"[0,-144,108,-20,1];",
				"[0,2880,-2304,508,-40,1];",
				"...",
				"n=3: [0,12,-8,1]. p(3,x)=x*(12-8*x+x^2)= x*(x-2)*(x-6).",
				"n=5: [0,2880,-2304,508,-40,1]. p(5,x)=x*(2880-2304*x+508*x^2-40*x^3+x^4)=x*(x-2)*(x-6)*(x-12)*(x-20)."
			],
			"xref": [
				"Row sums give A130031. Unsigned row sums give A130032.",
				"Cf. A129462 (v=2 member), A129065 (v=1 member), A191936 (row reversed?)."
			],
			"keyword": "sign,tabl,easy",
			"offset": "0,5",
			"author": "_Wolfdieter Lang_, May 04 2007",
			"references": 12,
			"revision": 29,
			"time": "2019-08-28T16:34:37-04:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}