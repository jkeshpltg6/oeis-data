{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A173763",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 173763,
			"data": "1,16,-156,256,870,-2496,-952,4096,4653,13920,-56148,-39936,178094,-15232,-135720,65536,-247662,74448,315380,222720,148512,-898368,204504,-638976,-1196225,2849504,2344680,-243712,-3840450,-2171520,-1309408,1048576,8759088,-3962592,-828240,1191168,4307078",
			"name": "Expansion of (eta(q^2)^7 / eta(q^4)^2)^4 + 16 * q * (eta(q)^2 * eta(q^2) * eta(q^4)^2)^4 in powers of q.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A173763/b173763.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q * (psi(q)^3 * phi(-q)^2)^4 * ((phi(q) / psi(q))^4 + 16 * q * (psi(q) / phi(q))^4) in powers of q where phi(), psi() are Ramanujan theta functions.",
				"a(n) is multiplicative with a(2^e) = 16^e, a(p^e) = a(p) * a(p^(e-1)) - p^9 * a(p^(e-2)) if p\u003e2.",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (2 t)) = 32 (t / i)^10 f(t) where q = exp(2 Pi i t)."
			],
			"example": [
				"G.f. = q + 16*q^2 - 156*q^3 + 256*q^4 + 870*q^5 - 2496*q^6 - 952*q^7 + 4096*q^8 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ q (QPochhammer[ q^2]^7 / QPochhammer[ q^4]^2)^4 + 16 q^2 (QPochhammer[ q]^2 QPochhammer[ q^2] QPochhammer[ q^4]^2)^4, {q, 0, n}]; (* _Michael Somos_, May 28 2013 *)"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c1, 0, n--; A = x * O(x^n); polcoeff( (eta(x^2 + A)^7 / eta(x^4 + A)^2)^4 + 16 * x * (eta(x + A)^2 * eta(x^2 + A) * eta(x^4 + A)^2)^4, n))};",
				"(PARI) q='q+O('q^99); Vec((eta(q^2)^7/eta(q^4)^2)^4+16*q*(eta(q)^2*eta(q^2)*eta(q^4)^2)^4) \\\\ _Altug Alkan_, Apr 18 2018",
				"(Sage) CuspForms( Gamma1(2), 10, prec=50).0; # _Michael Somos_, May 28 2013",
				"(MAGMA) Basis( CuspForms( Gamma1(2), 10), 50) [1]; /* _Michael Somos_, May 27 2014 */"
			],
			"xref": [
				"Cf. A002288."
			],
			"keyword": "sign,mult",
			"offset": "1,2",
			"author": "_Michael Somos_, Feb 23 2010",
			"ext": [
				"Name corrected by _Altug Alkan_, Apr 18 2018"
			],
			"references": 1,
			"revision": 22,
			"time": "2021-03-12T22:24:46-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}