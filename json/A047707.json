{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A047707",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 47707,
			"data": "0,0,0,2,64,1090,14000,153762,1533504,14356610,128722000,1119607522,9528462944,79817940930,660876543600,5424917141282,44246078560384,359144709794050,2904688464582800,23429048035827042,188593339362097824",
			"name": "Number of monotone Boolean functions of n variables with 3 mincuts. Also Sperner systems with 3 blocks.",
			"comment": [
				"The paper by G. Kilibarda, Enumeration of certain classes of antichains, Publications de l'Institut Mathematique, Nouvelle série, 97 (111) (2015), mentions many sequences, but since only very condensed formulas are given, it is hard to match them with entries in the OEIS. It would be nice to add this reference to all the sequences that it mentions. - _N. J. A. Sloane_, Jan 01 2016",
				"Term a(1108) has 1000 decimal digits. - _Michael De Vlieger_, Jan 26 2016"
			],
			"reference": [
				"L. Comtet, Advanced Combinatorics, Reidel, 1974, p. 292, #8, s(n,3)."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A047707/b047707.txt\"\u003eTable of n, a(n) for n = 0..1107\u003c/a\u003e",
				"K. S. Brown, \u003ca href=\"http://www.mathpages.com/home/kmath030.htm\"\u003eDedekind's problem.\u003c/a\u003e",
				"Vladeta Jovovic, \u003ca href=\"/A047707/a047707.pdf\"\u003eIllustration for A016269, A047707, A051112-A051118\u003c/a\u003e",
				"G. Kilibarda, \u003ca href=\"http://dx.doi.org/10.2298/PIM140406001K\"\u003eEnumeration of certain classes of antichains\u003c/a\u003e, Publications de l'Institut Mathematique, Nouvelle série, 97 (111) (2015), 69-87 DOI: 10.2298/PIM140406001K. See page 86, formula for alpha^hat(3,n).",
				"Goran Kilibarda and Vladeta Jovovic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL7/Kilibarda/kili2.html\"\u003eAntichains of Multisets\u003c/a\u003e, J. Integer Seqs., Vol. 7, 2004.",
				"\u003ca href=\"/index/Bo#Boolean\"\u003eIndex entries for sequences related to Boolean functions\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (28,-315,1820,-5684,9072,-5760)."
			],
			"formula": [
				"a(n) = (2^n)*(2^n - 1)*(2^n - 2)/6 - (6^n - 5^n - 4^n + 3^n).",
				"G.f.: -2*x^3*(36*x^2-4*x-1)/((2*x-1)*(3*x-1)*(4*x-1)*(5*x-1)*(6*x-1)*(8*x-1)). - _Colin Barker_, Jul 31 2012",
				"a(n) = Binomial(2^n,3) - (6^n - 5^n - 4^n + 3^n). - _Ross La Haye_, Jan 26 2016"
			],
			"mathematica": [
				"Table[Binomial[2^n, 3] - (6^n - 5^n - 4^n + 3^n), {n, 20}] (* or *)",
				"CoefficientList[Series[-2 x^3 (36 x^2 - 4 x - 1)/((2 x - 1) (3 x - 1) (4 x - 1) (5 x - 1) (6 x - 1) (8 x - 1)), {x, 0, 20}], x] (* _Michael De Vlieger_, Jan 26 2016 *)"
			],
			"program": [
				"(PARI) a(n)=binomial(2^n,3)-(6^n-5^n-4^n+3^n) \\\\ _Charles R Greathouse IV_, Apr 08 2016"
			],
			"xref": [
				"Cf. A016269, A051112."
			],
			"keyword": "nonn,easy",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_",
			"references": 37,
			"revision": 45,
			"time": "2020-03-07T01:28:54-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}