{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A292357",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 292357,
			"data": "1,1,1,1,5,1,1,15,15,1,1,39,111,39,1,1,97,649,649,97,1,1,237,3495,7943,3495,237,1,1,575,18189,86995,86995,18189,575,1,1,1391,93231,910667,1890403,910667,93231,1391,1",
			"name": "Array read by antidiagonals: T(m,n) is the number of fixed polyominoes that have a width of m and height of n.",
			"comment": [
				"Equivalently, the number of m X n binary arrays with all 1's connected and at least one 1 on each edge.",
				"Also, the number of m X n binary arrays with a path of adjacent 1's and no path of adjacent 0's from top row to bottom row."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A292357/b292357.txt\"\u003eTable of n, a(n) for n = 1..435\u003c/a\u003e",
				"Andrew Howroyd, \u003ca href=\"/A292357/a292357.txt\"\u003eFixed polyominoes by width, height and number of cells\u003c/a\u003e",
				"\u003ca href=\"/index/Pol#polyominoes\"\u003eIndex entries for sequences related to polyominoes\u003c/a\u003e"
			],
			"formula": [
				"T(m, n) = U(m, n) - 2*U(m, n-1) + U(m, n-2) where U(m, n) = V(m, n) - 2*V(m-1, n) + V(m-2, n) and V(m, n) = A287151(m, n)."
			],
			"example": [
				"Array begins:",
				"===============================================================",
				"m\\n| 1   2     3       4         5           6             7",
				"---|-----------------------------------------------------------",
				"1  | 1   1     1       1         1           1             1...",
				"2  | 1   5    15      39        97         237           575...",
				"3  | 1  15   111     649      3495       18189         93231...",
				"4  | 1  39   649    7943     86995      910667       9339937...",
				"5  | 1  97  3495   86995   1890403    38916067     782256643...",
				"6  | 1 237 18189  910667  38916067  1562052227   61025668579...",
				"7  | 1 575 93231 9339937 782256643 61025668579 4617328590967...",
				"...",
				"T(2,2) = 5 counts 4 3-ominoes of shape 2x2 and 1 4-omino of shape 2x2.",
				"T(3,2) = 15 counts 8 4-ominoes of shape 3x2, 6 5-ominoes of shape 3x2, and 1 6-omino of shape 3x2.",
				"T(4,2) = 39 counts 12 5-ominoes of shape 4x2, 18 6-ominoes of shape 4x2, 8 7-ominoes of shape 4x2, and 1 8-omino of shape 4x2."
			],
			"mathematica": [
				"A287151 = Import[\"https://oeis.org/A287151/b287151.txt\", \"Table\"][[All, 2]];",
				"imax = Length[A287151];",
				"mmax = Sqrt[2 imax] // Ceiling;",
				"Clear[V]; VV = Table[V[m-n+1, n], {m, 1, mmax}, {n, 1, m}] // Flatten;",
				"Do[Evaluate[VV[[i]]] = A287151[[i]], {i, 1, imax}];",
				"V[0, _] = V[_, 0] = 0;",
				"T[m_, n_] := If[m == 1 || n == 1, 1, U[m, n] - 2 U[m, n-1] + U[m, n-2]];",
				"U[m_, n_] := V[m, n] - 2 V[m-1, n] + V[m-2, n];",
				"Table[T[m-n+1, n], {m, 1, mmax}, {n, 1, m}] // Flatten // Take[#, imax]\u0026 (* _Jean-François Alcover_, Sep 22 2019 *)"
			],
			"xref": [
				"Rows 2..4 are A034182, A034184, A034187.",
				"Main diagonal is A268404.",
				"Cf. A268371 (nonequivalent), A287151, A308359."
			],
			"keyword": "nonn,tabl",
			"offset": "1,5",
			"author": "_Andrew Howroyd_, Oct 02 2017",
			"references": 10,
			"revision": 46,
			"time": "2020-07-01T15:20:14-04:00",
			"created": "2017-10-02T21:34:36-04:00"
		}
	]
}