{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A004251",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 4251,
			"id": "M1250",
			"data": "1,1,2,4,11,31,102,342,1213,4361,16016,59348,222117,836315,3166852,12042620,45967479,176005709,675759564,2600672458,10029832754,38753710486,149990133774,581393603996,2256710139346,8770547818956,34125389919850,132919443189544,518232001761434,2022337118015338,7898574056034636,30873421455729728",
			"name": "Number of graphical partitions (degree-vectors for simple graphs with n vertices, or possible ordered row-sum vectors for a symmetric 0-1 matrix with diagonal values 0).",
			"comment": [
				"In other words, a(n) is the number of graphic sequences of length n, where a graphic sequence is a sequence of numbers which can be the degree sequence of some graph.",
				"In the article by A. Iványi, G. Gombos, L. Lucz, and T. Matuszka, \"Parallel enumeration of degree sequences of simple graphs II\", in Table 4 on page 260 the values a(30) = 7898574056034638 and a(31) = 30873429530206738 are incorrect due to the incorrect Gz(30) = 5876236938019300 and Gz(31) = 22974847474172100. - _Wang Kai_, Jun 05 2016"
			],
			"reference": [
				"R. A. Brualdi and H. J. Ryser, Combinatorial Matrix Theory, Cambridge Univ. Press, 1992.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"P. R. Stein, On the number of graphical partitions, pp. 671-684 of Proc. 9th S-E Conf. Combinatorics, Graph Theory, Computing, Congr. Numer. 21 (1978)."
			],
			"link": [
				"Wang Kai, \u003ca href=\"/A004251/b004251.txt\"\u003eTable of n, a(n) for n = 0..79\u003c/a\u003e. [Terms 1 through 31 were computed by various authors; terms 32 through 34 by Axel Kohnert and Wang Kai; terms 35 to 79 by Wang Kai]",
				"T. M. Barnes and C. D. Savage, \u003ca href=\"https://doi.org/10.37236/1205\"\u003eA recurrence for counting graphical partitions\u003c/a\u003e, Electronic J. Combinatorics, 2 (1995), #R11.",
				"B. A. Chat, S. Pirzada, and A. Iványi, \u003ca href=\"http://dx.doi.org/10.1515/ausi-2015-0007\"\u003eRecognition of split-graphic sequences\u003c/a\u003e, Acta Universitatis Sapientiae, Informatica, 6, 2 (2014) 252-286.",
				"D. Dimitrov, \u003ca href=\"http://arxiv.org/abs/1305.1155\"\u003eEfficient computation of trees with minimal atom-bond connectivity index\u003c/a\u003e, arXiv:1305.1155 [cs.DM], 2013.",
				"A. Iványi, L. Lucz, T. F. Móri and P. Sótér, \u003ca href=\"http://www.acta.sapientia.ro/acta-info/C3-2/info32-7.pdf\"\u003eOn Erdős-Gallai and Havel-Hakimi algorithms\u003c/a\u003e, Acta Univ. Sapientiae, Inform. 3(2) (2011), 230-268.",
				"A. Ivanyi, L. Lucz, T. Matuszka, and S. Pirzada, \u003ca href=\"http://www.acta.sapientia.ro/acta-info/C4-2/info42-7.pdf\"\u003eParallel enumeration of degree sequences of simple graphs\u003c/a\u003e, Acta Univ. Sapientiae, Informatica, 4, 2 (2012), 260-288. - From _N. J. A. Sloane_, Feb 15 2013",
				"A. Ivanyi and J. E. Schoenfield, \u003ca href=\"http://www.acta.sapientia.ro/acta-info/C4-1/info41-7.pdf\"\u003eDeciding football sequences\u003c/a\u003e, Acta Univ. Sapientiae, Informatica, 4, 1 (2012), 130-183. - From _N. J. A. Sloane_, Dec 22 2012 [Disclaimer: I am not one of the authors of this paper; I was unpleasantly surprised to find my name on it, as explained \u003ca href=\"https://oeis.org/wiki/User:Jon_E._Schoenfield\"\u003ehere\u003c/a\u003e. - _Jon E. Schoenfield_, Nov 26 2016]",
				"Wang Kai, \u003ca href=\"https://arxiv.org/abs/1604.04148\"\u003eEfficient Counting of Degree Sequences\u003c/a\u003e, arXiv:1604.04148 [math.CO], 2016. Gives 79 terms.",
				"P. W. Mills, R. P. Rundle, V. M. Dwyer, T. Tilma, and S. J. Devitt, \u003ca href=\"https://arxiv.org/abs/1711.09842\"\u003eA proposal for an efficient quantum algorithm solving the graph isomorphism problem\u003c/a\u003e, arXiv 1711.09842, 2017.",
				"P. R. Stein, \u003ca href=\"/A004250/a004250.pdf\"\u003eOn the number of graphical partitions\u003c/a\u003e, pp. 671-684 of Proc. 9th S-E Conf. Combinatorics, Graph Theory, Computing, Congr. Numer. 21 (1978). [Annotated scanned copy]",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DegreeSequence.html\"\u003eDegree Sequence\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GraphicSequence.html\"\u003eGraphic Sequence\u003c/a\u003e.",
				"Gus Wiseman, \u003ca href=\"/A339741/a339741_1.txt\"\u003eCounting and ranking factorizations, factorability, and vertex-degree partitions for groupings into pairs.\u003c/a\u003e",
				"\u003ca href=\"/index/Gra#graph_part\"\u003eIndex entries for sequences related to graphical partitions\u003c/a\u003e"
			],
			"formula": [
				"G.f. = 1 + x + 2*x^2 + 4*x^3 + 11*x^4 + 31*x^5 + 102*x^6 + 342*x^7 + 1213*x^8 + ..."
			],
			"example": [
				"For n = 3, there are 4 different graphic sequences possible: 0 0 0; 1 1 0; 2 1 1; 2 2 2. - Daan van Berkel (daan.v.berkel.1980(AT)gmail.com), Jun 25 2010",
				"From _Gus Wiseman_, Dec 31 2020: (Start)",
				"The a(0) = 1 through a(4) = 11 sorted degree sequences:",
				"  ()  (0)  (0,0)  (0,0,0)  (0,0,0,0)",
				"           (1,1)  (0,1,1)  (0,0,1,1)",
				"                  (1,1,2)  (0,1,1,2)",
				"                  (2,2,2)  (0,2,2,2)",
				"                           (1,1,1,1)",
				"                           (1,1,1,3)",
				"                           (1,1,2,2)",
				"                           (1,2,2,3)",
				"                           (2,2,2,2)",
				"                           (2,2,3,3)",
				"                           (3,3,3,3)",
				"For example, the graph {{2,3},{2,4}} has degrees (0,2,1,1), so (0,1,1,2) is counted under a(4).",
				"(End)"
			],
			"mathematica": [
				"Table[Length[Union[Sort[Table[Count[Join@@#,i],{i,n}]]\u0026/@Subsets[Subsets[Range[n],{2}]]]],{n,0,5}] (* _Gus Wiseman_, Dec 31 2020 *)"
			],
			"xref": [
				"Counting the positive partitions by sum gives A000569, ranked by A320922.",
				"The version with half-loops is A029889, with covering case A339843.",
				"The covering case (no zeros) is A095268.",
				"Covering simple graphs are ranked by A309356 and A320458.",
				"Non-graphical partitions are counted by A339617 and ranked by A339618.",
				"The version with loops is A339844, with covering case A339845.",
				"A006125 counts simple graphs, with covering case A006129.",
				"A027187 counts partitions of even length, ranked by A028260.",
				"A058696 counts partitions of even numbers, ranked by A300061.",
				"A320921 counts connected graphical partitions.",
				"A322353 counts factorizations into distinct semiprimes.",
				"A339659 counts graphical partitions of 2n into k parts.",
				"A339661 counts factorizations into distinct squarefree semiprimes.",
				"Cf. A004250, A007717, A181819, A320894, A320923, A339560, A339561, A339842."
			],
			"keyword": "nonn,nice",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from Torsten Sillke, torsten.sillke(AT)lhsystems.com, using Cor. 6.3.3, Th. 6.3.6, Cor. 6.2.5 of Brualdi-Ryser.",
				"a(19) from Herman Jamke (hermanjamke(AT)fastmail.fm), May 19 2007",
				"a(20)-a(23) from _Nathann Cohen_, Jul 09 2011",
				"a(24)-a(29) from _Antal Iványi_, Nov 15 2011",
				"a(30) and a(31) corrected by _Wang Kai_, Jun 05 2016"
			],
			"references": 24,
			"revision": 111,
			"time": "2021-01-01T12:01:23-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}