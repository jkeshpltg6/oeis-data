{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A274019",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 274019,
			"data": "1,4,10,23,66,192,636,2092,7228,25175,89212,318808,1150444,4177908,15268494,56078527,206903020,766342160,2848351388,10619472284,39702648534,148806583111,558999381656,2104255629608,7936108068008,29982733437844,113456750715426,429964269551767,1631663320986086",
			"name": "Number of n-bead quaternary necklaces (no turning over allowed) that avoid the subsequence 110.",
			"comment": [
				"The pattern in this enumeration must be contiguous (all three values next to each other in one sequence of three letters).",
				"Because A(x) = Sum_{n\u003e=1} a(n)*x^n = 1 - Sum_{n\u003e=1} (phi(n)/n)*log(1-B(x^n)), where B(x) = q*x - x^3 and q = 4, we may find sequence (c(n): n\u003e=1) that satisfies a(n) = (1/n)*Sum_{d|n} phi(n/d)*c(d) for n\u003e=1 by using the formula Sum_{n\u003e=1} c(n)*x^n = C(x) = x*(dB/dx)/(1-B(x)). In our case, C(x) = x*(d(q*x-x^3)/dx)/(1-(q*x-x^3)) = (q*x - 3*x^3)/(1 - q*x + x^3). This implies that c(1) = q, c(2) = q^2, c(3) = q^3 - 3, and c(n) = q*c(n-1) - c(n-3) for n\u003e=4. This comment applies not only to this sequence, but also to sequences A274017, A274018 and A274020 as well (corresponding to cases q=2, 3, and 5, respectively). - _Petros Hadjicostas_, Jan 31 2018"
			],
			"link": [
				"P. Hadjicostas and L. Zhang, \u003ca href=\"https://doi.org/10.1016/j.disc.2018.03.007\"\u003eOn cyclic strings avoiding a pattern\u003c/a\u003e, Discrete Mathematics, 341 (2018), 1662-1674.",
				"Math Stackexchange, Marko Riedel et al., \u003ca href=\"http://math.stackexchange.com/questions/1812920/\"\u003eCounting circular sequences\u003c/a\u003e.",
				"Marko Riedel, \u003ca href=\"/A274017/a274017.maple.txt\"\u003eMaple code for this sequence\u003c/a\u003e."
			],
			"formula": [
				"G.f.: 1 - Sum_{n\u003e=1} (phi(n)/n)*log(x^(3*n)-q*x^n+1), where q=4 is the number of symbols in the alphabet we are using. - _Petros Hadjicostas_, Sep 12 2017",
				"Define sequence (c(n): n\u003e=1) by c(1) = q, c(2) = q^2, c(3) = q^3-3, and c(n) = q*c(n-1) - c(n-3) for n\u003e=4. Then a(n) = (1/n)*Sum_{d|n} phi(n/d)*c(d) for n\u003e=1. (Here q=4.) - _Petros Hadjicostas_, Jan 29 2018"
			],
			"example": [
				"The following necklace",
				".   1-1",
				".  /   \\",
				". 0     0",
				". |     |",
				". 1     3",
				".  \\   /",
				".   0-2",
				"contains one instance of the subsequence starting in the upper left corner. Unlike a bracelet, the necklace is oriented."
			],
			"xref": [
				"Cf. A000031, A274017, A274018, A274020."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Marko Riedel_, Jun 06 2016",
			"references": 3,
			"revision": 53,
			"time": "2018-07-04T14:19:12-04:00",
			"created": "2016-06-09T06:10:12-04:00"
		}
	]
}