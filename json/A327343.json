{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A327343",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 327343,
			"data": "1,1,2,2,2,2,2,2,2,2,2,4,4,4,2,2,4,2,4,2,4,4,4,2,2,4,2,4,4,4,2,4,8,8,4,2,4,2,2,4,4,4,4,8,2,2,8,2,2,4,8,4,4,4,8,4,2,8,8,4,8,2,8,8,4,4,4,8,2,4,4,4,4,4,4,2,8,4,2,16,2,4,4,16,4,2,8,8,16,8,2,2,4,4,4,2,8,4,8,4",
			"name": "a(n) gives the number of representative parallel primitive forms for binary quadratic forms of discriminant Disc(n) = 9*m(n)^2  - 4 and representation of -m(n)^2, with m(n) = A002559(n) (Markoff numbers).",
			"comment": [
				"For the definition of parallel forms for an indefinite binary quadratic form with discriminant Disc and representation of an integer k see, e.g., the Buell, Scholz-Schoeneberg references or the W. Lang link, section 3, with a scanning prescription.",
				"For the Markoff case Disc(n) = 9*m(n) - 4 = b(n)*(b(n)+2), with m = A002559 and b = A324250.",
				"The Markoff form MF(n;x,y) = x^2 - 3*m(n)*x*y + y^2, also written as MF(n) = [1, -3*m(n), 1], representing -m(n)^2, has as first reduced form the principal form F_p(n;X,Y) = X^2 + b(n)*X*Y - b(n)*Y^2, or F_p(n) = [1, b(n), -b(n)], where the connection is X = x-y, Y = x, or x = Y, y = Y - X. Hence X \u003c= 0 for x \u003c= y.",
				"Only proper solutions (gcd(X, Y) = 1) are of interest. Also only primitive representative parallel forms FPa(n;i), for  i = 1, 2, ..., #FPa(n), are considered.",
				"In the present case it is possible to give directly the prescription for the primitive representative parallel forms (rpapfs). This is done for the even m(n) == 2 (mod 32) case and the odd case m(n) == 1 (mod 4) separately.",
				"These rpapfs are written as FPa(n;i) = [-m(n)^2, B(n,i), -C(n,i)]. Their number a(n) =  #FPa(n) can be found from congruences with an application of the Chinese remainder theorem and the lifting theorem (see Apostol, Theorem 5.26, pp. 118-119, and Theorem 5.30, pp. 121-122 (only part (a) is effective here)). The existence of two solutions for each odd prime modulus is important as input for the lifting to higher prime powers. For each of the singular cases m(1) = 1 and m(2) = 2, without odd prime divisors, there is only one rpapf.",
				"The Frobenius-Markoff uniqueness conjecture is certainly true for m(n) if a(n) = 1 or a(n) = 2. In the latter case the two rpapfs have to be equivalent to the principal form F_p(n), because the known solution implied by the ordered triple MT(n) = (x(n), y(n), m(n)) has an unordered partner solution which after ordering becomes (x(n), y'(n), m(n')) with y'(n) = m(n) and m(n') = 3*x(n)*m(n) - y(n) \u003e= m(n).",
				"See A327344 for details on the congruences which determine the rpapfs."
			],
			"reference": [
				"Martin Aigner, Markov's Theorem and 100 Years of the Uniqueness Conjecture, Springer, 2013.",
				"Tom M. Apostol, Introduction to Analytic Number Theory, 1976, Springer.",
				"D. A. Buell, Binary quadratic forms, 1989, Springer, p. 49 (f').",
				"A. Scholz and B. Schoeneberg, Einführung in die Zahlentheorie, 5. Aufl., de Gruyter, Berlin, New York, 1973, p. 105, eq. 129."
			],
			"link": [
				"Wolfdieter Lang, \u003ca href=\"/A324251/a324251.pdf\"\u003eCycles of reduced Pell forms, general Pell equations and Pell graphs\u003c/a\u003e, section 3."
			],
			"formula": [
				"a(n) = 2^A327342(n), n \u003e= 1, where A327342(n) is the number of distinct odd primes dividing m(n).",
				"a(n) = number of representative parallel primitive forms (rpapfs) for discriminant Disc(n) = 9*m(n)^2 - 4 = b(n)*(b(n) + 4), with m(n) = A002559(n) and b(n) = A324250(n)."
			],
			"example": [
				"n = 6: m(6) = 34 = 2*17, a(6) = 2. The (primitive) reduced principal form is F_p(6) = [1, 100, -100], and both representative parallel primitive forms are connected to this form via an equivalence transformation. The two proper fundamental solutions with X \u003c 0 of F_p(6) = -34^2 are (X, Y)_1 = (-12, 1) and (X, Y)_2 = (-88, 1). They belong to the ordered Markoff triple MT(6) = (1, 13, 34) and the unordered one (1, 89, 34), respectively. The latter triple has 89 = 3*1*34 - 13, and is the ordered triple (1, 34, 89), not of interest in the search for ordered solution with maximum m(6).",
				"Note that there are other proper fundamental positive solutions coming from the imprimitive form F = [4, 96, -74], namely (X, Y)_3 = (19, 26) and (X, Y)_4 = (133, 178) which are not counted here.",
				"n = 12: m(12) = 610 = 2*5*61, a(12) = 4. The reduced principal form F_p(12) = [1, 1828, -1828], representing -610^2, has only two proper fundamental solutions with X \u003c 0, Y \u003e 0: (X, Y)_1 = (-232, 1), corresponding to the ordered Markoff triple MT(12) = (1, 233, 610), and (X, Y)_2 = (-1596, 1), corresponding to the unordered triple (1, 1597, 610). These solutions follow from the rpapfs [-372100, 742836, -370735] with t-tuple (-1, 231) and [-372100, 1364, 1] with t-tuple (1596), respectively.  The other two such proper fundamental solutions are (X, Y)_3 = (-6, 25) for the reduced form F(12) = [625, 1664, -232], and (X, Y)_4 = (-25, 6) for the associated form Fbar(12) = [-232, 1664, 625], both  representing -m(12)^2. These last two reduced forms belong to different (associated) 8-cycles. The corresponding rpapfs are [-372100, 623764, -261407] and [-372100, 120436, -9743]."
			],
			"xref": [
				"Cf. A002559, A324250, A327342, A327344."
			],
			"keyword": "nonn,easy",
			"offset": "1,3",
			"author": "_Wolfdieter Lang_, Sep 13 2019",
			"references": 2,
			"revision": 22,
			"time": "2021-12-12T22:51:39-05:00",
			"created": "2019-09-13T22:42:12-04:00"
		}
	]
}