{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A140434",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 140434,
			"data": "1,2,4,4,8,4,12,8,12,8,20,8,24,12,16,16,32,12,36,16,24,20,44,16,40,24,36,24,56,16,60,32,40,32,48,24,72,36,48,32,80,24,84,40,48,44,92,32,84,40,64,48,104,36,80,48,72,56,116,32,120",
			"name": "Number of new visible points created at each step in an n X n grid.",
			"comment": [
				"Equals row sums of triangle A143467. - _Gary W. Adamson_, Aug 17 2008",
				"Equals first differences of A018805: (1, 3, 7, 11, 19, 23, 35,...). - _Gary W. Adamson_, Aug 17 2008",
				"a(n) is the number of rationals p/q such that |p| + |q| = n. - _Geoffrey Critzer_, Oct 11 2011",
				"a(n) is the number of nonempty lists of positive integers whose continuants are equal to n. For example, for n = 6 these continuants are [6], [5,1], [1,5], and [1,4,1]. - _Jeffrey Shallit_, May 18 2016"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A140434/b140434.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/A115004/a115004.txt\"\u003eFamilies of Essentially Identical Sequences\u003c/a\u003e, Mar 24 2021 (Includes this sequence)"
			],
			"formula": [
				"a(n) = 2 phi(n), where phi is Euler's phi function, A000010, for n \u003e= 2.",
				"Sum_{k=1..n} a(k)*floor(n/k) = n^2. - _Benoit Cloitre_, Nov 09 2016",
				"G.f.: Sum_{k\u003e=1} mu(k) * x^k * (1 + x^k)/(1 - x^k)^2. - _Seiichi Manyama_, May 24 2021"
			],
			"example": [
				"G.f. = x + 2*x^2 + 4*x^3 + 4*x^4 + 8*x^5 + 4*x^6 + 12*x^7 + 8*x^8 + 12*x^9 + ..."
			],
			"mathematica": [
				"f[n_] := FoldList[Plus, 1, 2 Array[EulerPhi, n, 2]] // Differences // Prepend[#, 1]\u0026",
				"a[ n_] := If[ n \u003c 3, Max[0, n], Sum[ MoebiusMu[d] (2 n/d - 1 - Mod[n/d, 2]), {d, Divisors@n}]]; (* _Michael Somos_, Jul 24 2015 *)"
			],
			"program": [
				"(Haskell)",
				"a140434 n = a140434_list !! (n-1)",
				"a140434_list = 1 : zipWith (-) (tail a018805_list) a018805_list",
				"-- _Reinhard Zumkeller_, May 04 2014",
				"(PARI) {a(n) = if( n\u003c3, max(0, n), sumdiv(n, d, moebius(d) * (2*n/d - 1 - (n/d)%2)))}; /* _Michael Somos_, Jul 24 2015 */",
				"(PARI) my(N=66, x='x+O('x^N)); Vec(sum(k=1, N, moebius(k)*x^k*(1+x^k)/(1-x^k)^2)) \\\\ _Seiichi Manyama_, May 24 2021"
			],
			"xref": [
				"Cf. A018805, A100613, A140435. Equals twice A000010 (for n \u003e= 2)."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Gregg Whisler_, Jun 25 2008, Jun 28 2008",
			"ext": [
				"Mathematica simplified by _Jean-François Alcover_, Jun 06 2013"
			],
			"references": 10,
			"revision": 52,
			"time": "2021-05-24T11:16:04-04:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}