{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A192883",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 192883,
			"data": "2,0,5,8,26,63,170,440,1157,3024,7922,20735,54290,142128,372101,974168,2550410,6677055,17480762,45765224,119814917,313679520,821223650,2149991423,5628750626,14736260448,38580030725,101003831720,264431464442,692290561599",
			"name": "Constant term in the reduction by (x^2 -\u003e x + 1) of the polynomial F(n+3)*x^n, where F = A000045 (Fibonacci sequence).",
			"comment": [
				"See A192872.",
				"a(n) is also the area of the triangle with vertices at (F(n),F(n+1)), (F(n+1),F(n)), and (F(n+3),F(n+4)) where F(n) = A000045(n). - _J. M. Bergot_, May 22 2014"
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A192883/b192883.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,2,-1)."
			],
			"formula": [
				"a(n) = 2*a(n-1) + 2*a(n-2) - a(n-3).",
				"a(n) = Fibonacci(n-1) * Fibonacci(n+3). - _Gary Detlefs_, Oct 19 2011",
				"a(n) = Fibonacci(n+1)^2 + (-1)^n. - _Gary Detlefs_, Oct 19 2011",
				"G.f.: ( 2-4*x+x^2 ) / ( (1+x)*(1-3*x+x^2) ). - _R. J. Mathar_, May 07 2014",
				"a(n) = (2^(-1-n)*(7*(-1)^n*2^(1+n) + (3-sqrt(5))^(1+n) + (3+sqrt(5))^(1+n)))/5. - _Colin Barker_, Sep 29 2016",
				"From _Amiram Eldar_, Oct 06 2020: (Start)",
				"Sum_{n\u003e=2} 1/a(n) = 7/18.",
				"Sum_{n\u003e=2} (-1)^n/a(n) = (4/phi - 13/6)/3, where phi is the golden ratio (A001622). (End)"
			],
			"maple": [
				"with(combinat):seq(fibonacci(n-1)*fibonacci(n+3), n=0..27): # _Gary Detlefs_, Oct 19 2011"
			],
			"mathematica": [
				"q = x^2; s = x + 1; z = 28;",
				"p[0, x_] := 2; p[1, x_] := 3 x;",
				"p[n_, x_] := p[n - 1, x]*x + p[n - 2, x]*x^2;",
				"Table[Expand[p[n, x]], {n, 0, 7}]",
				"reduce[{p1_, q_, s_, x_}] := FixedPoint[(s PolynomialQuotient @@ #1 + PolynomialRemainder @@ #1 \u0026)[{#1, q, x}] \u0026, p1]",
				"t = Table[reduce[{p[n, x], q, s, x}], {n, 0, z}];",
				"u1 = Table[Coefficient[Part[t, n], x, 0], {n, 1, z}] (* A192883 *)",
				"u2 = Table[Coefficient[Part[t, n], x, 1], {n, 1, z}] (* minus A121646 *)",
				"LinearRecurrence[{2,2,-1}, {2,0,5}, 30] (* _G. C. Greubel_, Jan 09 2019 *)"
			],
			"program": [
				"(PARI) a(n) = round((2^(-1-n)*(7*(-1)^n*2^(1+n)+(3-sqrt(5))^(1+n)+(3+sqrt(5))^(1+n)))/5) \\\\ _Colin Barker_, Sep 29 2016",
				"(PARI) Vec((2+x^2-4*x)/((1+x)*(x^2-3*x+1)) + O(x^40)) \\\\ _Colin Barker_, Sep 29 2016",
				"(MAGMA) m:=30; R\u003cx\u003e:=PowerSeriesRing(Integers(), m); Coefficients(R!( ( 2-4*x+x^2)/((1+x)*(1-3*x+x^2)) )); // _G. C. Greubel_, Jan 09 2019",
				"(Sage) ((2-4*x+x^2 )/((1+x)*(1-3*x+x^2))).series(x, 30).coefficients(x, sparse=False) # _G. C. Greubel_, Jan 09 2019",
				"(GAP) a:=[2,0,5];; for n in [4..30] do a[n]:=2*a[n-1]+2*a[n-2]-a[n-3]; od; a; # _G. C. Greubel_, Jan 09 2019"
			],
			"xref": [
				"Cf. A001622, A192232, A192744, A192872."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_Clark Kimberling_, Jul 12 2011",
			"references": 5,
			"revision": 44,
			"time": "2020-10-06T04:00:33-04:00",
			"created": "2011-07-13T20:20:29-04:00"
		}
	]
}