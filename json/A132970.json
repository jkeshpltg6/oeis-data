{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A132970",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 132970,
			"data": "1,-3,2,-1,5,-5,3,-5,6,-10,10,-8,13,-15,15,-16,23,-27,25,-30,35,-40,42,-45,55,-66,68,-70,86,-95,100,-110,125,-141,150,-161,185,-207,215,-235,266,-293,310,-335,375,-410,438,-470,521,-575,610,-653,725,-785,835,-900,983,-1070,1140,-1220,1331",
			"name": "Expansion of phi(-x) * chi(-x) in powers of x where phi(), chi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"reference": [
				"N. J. Fine, Basic Hypergeometric Series and Applications, Amer. Math. Soc., 1988; p. 60, Eqs. (26.64),(26.65),(26.66)"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A132970/b132970.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of phi(-q) + 2 * psi(-q) in powers of q where phi(), psi() are Ramanujan 3rd order mock theta functions.",
				"Expansion of q^(1/24) * eta(q)^3 / eta(q^2)^2 in powers of q.",
				"Euler transform of period 2 sequence [ -3, -1, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (288 t)) = 48^(1/2) (t/i)^(1/2) g(t) where q = exp(2 Pi i t) and g() is the g.f. for A085140.",
				"G.f.: ( Sum_{k in Z} (-1)^k * x^k^2 ) / ( Product_{k\u003e0} (1 + x^k) ).",
				"G.f.: Product_{k\u003e0} (1 - x^k) / (1 + x^k)^2.",
				"a(n) = (-1)^n * A132969(n). a(n) = A124226(n) unless n=1.",
				"a(n) ~ (-1)^n * exp(Pi*sqrt(n/6)) / (2*sqrt(n)). - _Vaclav Kotesovec_, Oct 14 2017"
			],
			"example": [
				"G.f. = 1 - 3*x + 2*x^2 - x^3 + 5*x^4 - 5*x^5 + 3*x^6 - 5*x^7 + 6*x^8 + ...",
				"G.f. = 1/q - 3*q^23 + 2*q^47 - q^71 + 5*q^95 - 5*q^119 + 3*q^143 - 5*q^167 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 4, 0, x] QPochhammer[ x, x^2], {x, 0, n}]; (* _Michael Somos_, Jul 20 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, polcoeff( prod(k=1, (n+1)\\2, 1 - x^(2*k-1), 1 + x * O(x^n)) * sum(k=1, sqrtint(n), 2 * (-1)^k * x^k^2, 1), n))};",
				"(PARI) {a(n) = my(A) ; if( n\u003c0, 0, A = x * O(x^n) ; polcoeff( eta(x + A)^3 / eta(x^2 + A)^2, n))};"
			],
			"xref": [
				"CF. A124226, A132969."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Sep 04 2007",
			"references": 4,
			"revision": 14,
			"time": "2021-03-12T22:24:44-05:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}