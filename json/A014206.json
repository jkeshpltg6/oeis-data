{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A014206",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 14206,
			"data": "2,4,8,14,22,32,44,58,74,92,112,134,158,184,212,242,274,308,344,382,422,464,508,554,602,652,704,758,814,872,932,994,1058,1124,1192,1262,1334,1408,1484,1562,1642,1724,1808,1894,1982,2072,2164,2258,2354,2452,2552",
			"name": "a(n) = n^2 + n + 2.",
			"comment": [
				"Draw n + 1 circles in the plane; sequence gives maximal number of regions into which the plane is divided (a(n) = A002061(n+1) + 1 for n \u003e= 0). Cf. A051890.",
				"Number of binary (zero-one) bitonic sequences of length n + 1. - Johan Gade (jgade(AT)diku.dk), Oct 15 2003",
				"Also the number of permutations of n + 1 which avoid the patterns 213, 312, 13452 and 34521. Example: the permutations of 4 which avoid 213, 312 (and implicitly 13452 and 34521) are 1234, 1243, 1342, 1432, 2341, 2431, 3421, 4321. - _Mike Zabrocki_, Jul 09 2007",
				"If Y is a 2-subset of an n-set X then, for n \u003e= 3, a(n-3) is equal to the number of (n-3)-subsets and (n-1)-subsets of X having exactly one element in common with Y. - _Milan Janjic_, Dec 28 2007",
				"With a different offset, competition number of the complete tripartite graph K_{n, n, n}. [Kim, Sano] - _Jonathan Vos Post_, May 14 2009. Cf. A160450, A160457.",
				"A related sequence is A241119. - _Avi Friedlich_, Apr 28 2015",
				"From _Avi Friedlich_, Apr 28 2015: (Start)",
				"This sequence, which also represents the number of Hamiltonian paths in K_2 X P_n (A200182), may be represented by interlacing recursive polynomials in arithmetic progression (discriminant =-63). For example:",
				"a(3*k-3) = 9*k^2 - 15*k + 8,",
				"a(3*k-2) = 9*k^2 - 9*k  + 4,",
				"a(3*k-1) = 9*k^2 - 3*k  + 2,",
				"a(3*k)   = 3*(k+1)^2  - 1. (End)",
				"a(n+1) is the area of a triangle with vertices at (n+3, n+4), ((n-1)*n/2, n*(n+1)/2),((n+1)^2, (n+2)^2) with n \u003e= -1. - _J. M. Bergot_, Feb 02 2018",
				"For prime p and any integer k, k^a(p-1) == k^2 (mod p^2). - _Jianing Song_, Apr 20 2019",
				"From _Bernard Schott_, Jan 01 2021: (Start)",
				"For n \u003e= 1, a(n-1) is the number of solutions x in the interval 0 \u003c= x \u003c= n of the equation x^2 - [x^2] = (x - [x])^2, where [x] = floor(x). For n = 3, the a(2) = 8 solutions in the interval [0, 3] are 0, 1, 3/2, 2, 9/4, 5/2, 11/4 and 3.",
				"This is a variant of the 4th problem proposed during the 20th British Mathematical Olympiad in 1984 (see A002061). The interval [1, n] of the Olympiad problem becomes here [0, n], and only the new solution x = 0 is added. (End)"
			],
			"reference": [
				"K. E. Batcher, Sorting Networks and their Applications. Proc. AFIPS Spring Joint Comput. Conf., Vol. 32, pp. 307-314 (1968). [for bitonic sequences]",
				"L. Comtet, Advanced Combinatorics, Reidel, 1974, p. 73, Problem 3.",
				"T. H. Cormen, C. E. Leiserson and R. L. Rivest, Introduction to Algorithms. MIT Press / McGraw-Hill (1990) [for bitonic sequences]",
				"Indiana School Mathematics Journal, vol. 14, no. 4, 1979, p. 4.",
				"D. E. Knuth, The Art of Computer Programming, vol3: Sorting and Searching, Addison-Wesley (1973) [for bitonic sequences]",
				"J. D. E. Konhauser et al., Which Way Did the Bicycle Go?, MAA 1996, p. 177.",
				"Derrick Niederman, Number Freak, From 1 to 200 The Hidden Language of Numbers Revealed, A Perigee Book, NY, 2009, p. 83.",
				"A. M. Yaglom and I. M. Yaglom, Challenging Mathematical Problems with Elementary Solutions. Vol. I. Combinatorial Analysis and Probability Theory. New York: Dover Publications, Inc., 1987, p. 13, #44 (First published: San Francisco: Holden-Day, Inc., 1964)"
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A014206/b014206.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"A. Burstein, S. Kitaev, and T. Mansour, \u003ca href=\"http://puma.dimai.unifi.it/19_2_3/3.pdf\"\u003ePartially ordered patterns and their combinatorial interpretations\u003c/a\u003e, PU. M. A. Vol. 19 (2008), No. 2-3, pp. 27-38.",
				"Guo-Niu Han, \u003ca href=\"/A196265/a196265.pdf\"\u003eEnumeration of Standard Puzzles\u003c/a\u003e, 2011. [Cached copy]",
				"Guo-Niu Han, \u003ca href=\"https://arxiv.org/abs/2006.14070\"\u003eEnumeration of Standard Puzzles\u003c/a\u003e, arXiv:2006.14070 [math.CO], 2020.",
				"S.-R. Kim and Y. Sano, \u003ca href=\"http://dx.doi.org/10.1016/j.dam.2008.04.009\"\u003eThe competition numbers of complete tripartite graphs\u003c/a\u003e, Discrete Appl. Math., 156 (2008) 3522-3524.",
				"Hans Werner Lang, \u003ca href=\"http://www.iti.fh-flensburg.de/lang/algorithmen/sortieren/bitonic/bitonicen.htm\"\u003eBitonic sequences\u003c/a\u003e.",
				"Daniel Q. Naiman and Edward R. Scheinerman, \u003ca href=\"https://arxiv.org/abs/1709.07446\"\u003eArbitrage and Geometry\u003c/a\u003e, arXiv:1709.07446 [q-fin.MF], 2017.",
				"Jean-Christoph Novelli and Anne Schilling, \u003ca href=\"http://arXiv.org/abs/0706.2996\"\u003eThe Forgotten Monoid\u003c/a\u003e, arXiv 0706.2996 [math.CO], 2007.",
				"Parabola, \u003ca href=\"https://www.parabola.unsw.edu.au/files/articles/1980-1989/volume-24-1988/issue-1/vol24_no1_p.pdf\"\u003eProblem #Q736\u003c/a\u003e, 24(1) (1988), p. 22.",
				"Franck Ramaharo, \u003ca href=\"https://arxiv.org/abs/1712.06543\"\u003eEnumerating the states of the twist knot\u003c/a\u003e, arXiv:1712.06543 [math.CO], 2017.",
				"Franck Ramaharo, \u003ca href=\"https://arxiv.org/abs/1802.07701\"\u003eStatistics on some classes of knot shadows\u003c/a\u003e, arXiv:1802.07701 [math.CO], 2018.",
				"Franck Ramaharo, \u003ca href=\"https://arxiv.org/abs/1902.08989\"\u003eA generating polynomial for the two-bridge knot with Conway's notation C(n,r)\u003c/a\u003e, arXiv:1902.08989 [math.CO], 2019.",
				"Yoshio Sano, \u003ca href=\"http://arxiv.org/abs/0905.1763\"\u003eThe competition numbers of regular polyhedra\u003c/a\u003e, arXiv:0905.1763 [math.CO], 2009.",
				"Jeffrey Shallit, \u003ca href=\"http://recursed.blogspot.com/2012/10\"\u003eRecursivity:  An Interesting but Little-Known Function\u003c/a\u003e, 2012. [Mentions this function in a blog post as the solution for small n to a problem involving Boolean matrices whose values for larger n are unknown.]",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PlaneDivisionbyCircles.html\"\u003ePlane Division by Circles\u003c/a\u003e.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1)."
			],
			"formula": [
				"G.f.: 2*(x^2 - x + 1)/(1 - x)^3.",
				"n hyperspheres divide R^k into at most C(n-1, k) + Sum_{i = 0..k} C(n, i) regions.",
				"a(n) = A002061(n+1) + 1 for n \u003e= 0. - _Rick L. Shepherd_, May 30 2005",
				"Equals binomial transform of [2, 2, 2, 0, 0, 0, ...]. - _Gary W. Adamson_, Jun 18 2008",
				"a(n) = A003682(n+1), n \u003e 0. - _R. J. Mathar_, Oct 28 2008",
				"a(n) = a(n-1) + 2*n (with a(0) = 2). - _Vincenzo Librandi_, Nov 20 2010",
				"a(0) = 2, a(1) = 4, a(2) = 8, a(n) = 3*a(n-1) - 3*a(n-2) + a(n-3) for n \u003e= 3. - _Harvey P. Dale_, May 14 2011",
				"a(n + 1) = n^2 + 3*n + 4. - _Alonso del Arte_, Apr 12 2015",
				"a(n) = Sum_{i=n-2..n+2} i*(i + 1)/5. - _Bruno Berselli_, Oct 20 2016",
				"Sum_{n\u003e=0} 1/a(n) = Pi*tanh(Pi*sqrt(7)/2)/sqrt(7). - _Amiram Eldar_, Jan 09 2021",
				"From _Amiram Eldar_, Jan 29 2021: (Start)",
				"Product_{n\u003e=0} (1 + 1/a(n)) = cosh(sqrt(11)*Pi/2)*sech(sqrt(7)*Pi/2).",
				"Product_{n\u003e=0} (1 - 1/a(n)) = cosh(sqrt(3)*Pi/2)*sech(sqrt(7)*Pi/2). (End)",
				"a(n) = 2*A000124(n). - _R. J. Mathar_, Mar 14 2021"
			],
			"example": [
				"a(0) = 0^2 + 0 + 2 = 2.",
				"a(1) = 1^2 + 1 + 2 = 4.",
				"a(2) = 2^2 + 2 + 2 = 8.",
				"a(6) = 4*5/5 + 5*6/5 + 6*7/5 + 7*8/5 + 8*9/5 = 44. - _Bruno Berselli_, Oct 20 2016"
			],
			"maple": [
				"A014206 := n-\u003en^2+n+2;"
			],
			"mathematica": [
				"Table[n^2 + n + 2, {n, 0, 50}] (* _Stefan Steinerberger_, Apr 08 2006 *)",
				"LinearRecurrence[{3, -3, 1}, {2, 4, 8}, 50] (* _Harvey P. Dale_, May 14 2011 *)",
				"CoefficientList[Series[2 (x^2 - x + 1)/(1 - x)^3, {x, 0, 50}], x] (* _Vincenzo Librandi_, Apr 29 2015 *)"
			],
			"program": [
				"(PARI) a(n)=n^2+n+2 \\\\ _Charles R Greathouse IV_, Jul 31 2011",
				"(PARI) x='x+O('x^100); Vec(2*x*(x^2-x+1)/(1-x)^3) \\\\ _Altug Alkan_, Nov 01 2015",
				"(MAGMA) [n^2+n+2: n in [0..50]]; // _Vincenzo Librandi_, Apr 29 2015"
			],
			"xref": [
				"Cf. A014206 (dim 2), A046127 (dim 3), A059173 (dim 4), A059174 (dim 5). A row of A059250.",
				"Cf. A000124, A051890, A002522, A241119, A033547 (partial sums).",
				"Cf. A002061 (central polygonal numbers)."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Stefan Steinerberger_, Apr 08 2006"
			],
			"references": 43,
			"revision": 179,
			"time": "2021-03-14T10:28:10-04:00",
			"created": "1998-06-14T03:00:00-04:00"
		}
	]
}