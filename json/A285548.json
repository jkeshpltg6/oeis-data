{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A285548",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 285548,
			"data": "1,1,2,1,3,3,1,4,6,4,1,6,10,10,5,1,6,21,20,15,6,1,13,24,55,35,21,7,1,10,92,76,120,56,28,8,1,24,78,430,201,231,84,36,9,1,22,327,460,1505,462,406,120,45,10,1,45,443,2605,2015,4291,952,666,165,55,11",
			"name": "Array read by antidiagonals: T(m,n) = number of step cyclic shifted sequences of length n using a maximum of m different symbols.",
			"comment": [
				"See A056371, A002729 for an explanation of step shifts. Under step cyclic shifts, abcde, bdace, bcdea, cdeab and daceb etc. are equivalent.",
				"Equivalently, the number of mappings with domain {0..n-1} and codomain {1..m} up to equivalence.  Mappings A and B are equivalent if there is a d, prime to n, and a t such that A(i) = B((i*d + t) mod n) for i in {0..n-1}.",
				"All column sequences are polynomials of order n."
			],
			"reference": [
				"M. R. Nester (1999). Mathematical investigations of some plant interaction designs. PhD Thesis. University of Queensland, Brisbane, Australia. [See A056391 for pdf file of Chap. 2]"
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A285548/b285548.txt\"\u003eTable of n, a(n) for n = 1..1275\u003c/a\u003e",
				"R. C. Titsworth, \u003ca href=\"http://projecteuclid.org/euclid.ijm/1256059671\"\u003eEquivalence classes of periodic sequences\u003c/a\u003e, Illinois J. Math., 8 (1964), 266-270."
			],
			"example": [
				"Table starts:",
				"1  1  1   1   1     1     1      1      1       1 ...",
				"2  3  4   6   6    13    10     24     22      45 ...",
				"3  6 10  21  24    92    78    327    443    1632 ...",
				"4 10 20  55  76   430   460   2605   5164   26962 ...",
				"5 15 35 120 201  1505  2015  14070  37085  246753 ...",
				"6 21 56 231 462  4291  6966  57561 188866 1519035 ...",
				"7 28 84 406 952 10528 20140 192094 752087 7079800 ...",
				"..."
			],
			"mathematica": [
				"IsLeastPoint[s_, f_] := Module[{t=f[s]}, While[t\u003es, t=f[t]]; Boole[s==t]];",
				"c[n_, k_, t_] := Sum[IsLeastPoint[u, Mod[#*k+t, n]\u0026], {u, 0, n-1}];",
				"a[n_, x_] := Sum[If[GCD[k, n] == 1, x^c[n, k, t], 0], {t, 0, n-1}, {k, 1,",
				"n}] / (n*EulerPhi[n]);",
				"Table[a[n-m+1, m], {n, 1, 11}, {m, 1, n}] // Flatten (* _Jean-François Alcover_, Jun 05 2017, translated from PARI *)"
			],
			"program": [
				"(PARI)",
				"IsLeastPoint(s,f)={my(t=f(s)); while(t\u003es,t=f(t));s==t}",
				"C(n,k,t)=sum(u=0,n-1,IsLeastPoint(u,v-\u003e(v*k+t)%n));",
				"a(n,x)=sum(t=0, n-1, sum(k=1, n, if (gcd(k, n)==1, x^C(n,k,t),0)))/(n * eulerphi(n));",
				"for(m=1, 7, for(n=1, 10, print1( a(n,m), \", \") ); print(); );"
			],
			"xref": [
				"Rows 2-6 are A002729, A056411, A056412, A056413, A056414.",
				"Cf. A285522, A132191."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Andrew Howroyd_, Apr 20 2017",
			"references": 10,
			"revision": 19,
			"time": "2017-10-02T15:20:03-04:00",
			"created": "2017-04-24T00:23:45-04:00"
		}
	]
}