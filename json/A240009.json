{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A240009",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 240009,
			"data": "1,1,1,0,0,1,1,1,0,1,1,1,0,1,1,0,1,1,2,1,1,1,0,1,1,1,1,1,2,2,1,1,0,1,1,2,3,2,2,2,1,1,0,1,1,1,2,2,2,4,3,2,2,1,1,0,1,1,2,4,5,3,4,4,2,2,1,1,0,1,1,1,2,3,3,5,7,5,4,4,2,2,1,1,0,1,1,2,4,7,7,6,8,6,4,4,2,2,1,1,0,1",
			"name": "Number T(n,k) of partitions of n, where k is the difference between the number of odd parts and the number of even parts; triangle T(n,k), n\u003e=0, -floor(n/2)+(n mod 2)\u003c=k\u003c=n, read by rows.",
			"comment": [
				"T(n,k) = T(n+k,-k).",
				"Sum_{k=-floor(n/2)+(n mod 2)..-1} T(n,k) = A108949(n).",
				"Sum_{k=-floor(n/2)+(n mod 2)..0} T(n,k) = A171966(n).",
				"Sum_{k=1..n} T(n,k) = A108950(n).",
				"Sum_{k=0..n} T(n,k) = A130780(n).",
				"Sum_{k=-1..1} T(n,k) = A239835(n).",
				"Sum_{k\u003c\u003e0} T(n,k) = A171967(n).",
				"T(n,-1) + T(n,1) = A239833(n).",
				"Sum_{k=-floor(n/2)+(n mod 2)..n} k * T(n,k) = A209423(n)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A240009/b240009.txt\"\u003eRows n = 0..120, flattened\u003c/a\u003e"
			],
			"formula": [
				"G.f.: 1 / prod(n\u003e=1, 1 - e(n)*q^n ) = 1 + sum(n\u003e=1, e(n)*q^n / prod(k=1..n, 1-e(k)*q^k) ) where e(n) = u if n odd, otherwise 1/u; see Pari program. [_Joerg Arndt_, Mar 31 2014]"
			],
			"example": [
				"T(5,-1) = 1: [2,2,1].",
				"T(5,0) = 2: [4,1], [3,2].",
				"T(5,1) = 1: [5].",
				"T(5,2) = 1: [2,1,1,1].",
				"T(5,3) = 1: [3,1,1].",
				"T(5,5) = 1: [1,1,1,1,1].",
				"Triangle T(n,k) begins:",
				": n\\k : -5 -4 -3 -2 -1  0  1  2  3  4  5  6  7  8  9 10 ...",
				"+-----+----------------------------------------------------",
				":  0  :                 1;",
				":  1  :                    1;",
				":  2  :              1, 0, 0, 1;",
				":  3  :                 1, 1, 0, 1;",
				":  4  :           1, 1, 0, 1, 1, 0, 1;",
				":  5  :              1, 2, 1, 1, 1, 0, 1;",
				":  6  :        1, 1, 1, 1, 2, 2, 1, 1, 0, 1;",
				":  7  :           1, 2, 3, 2, 2, 2, 1, 1, 0, 1;",
				":  8  :     1, 1, 2, 2, 2, 4, 3, 2, 2, 1, 1, 0, 1;",
				":  9  :        1, 2, 4, 5, 3, 4, 4, 2, 2, 1, 1, 0, 1;",
				": 10  :  1, 1, 2, 3, 3, 5, 7, 5, 4, 4, 2, 2, 1, 1, 0, 1;"
			],
			"maple": [
				"b:= proc(n, i) option remember; `if`(n=0, 1, `if`(i\u003c1, 0,",
				"      expand(b(n, i-1)+`if`(i\u003en, 0, b(n-i, i)*x^(2*irem(i, 2)-1)))))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=ldegree(p)..degree(p)))(b(n$2)):",
				"seq(T(n), n=0..14);"
			],
			"mathematica": [
				"b[n_, i_] := b[n, i] = If[n == 0, 1, If[i\u003c1, 0, b[n, i-1] + If[i\u003en, 0, b[n-i, i]*x^(2*Mod[i, 2]-1)]]]; T[n_] := (degree = Exponent[b[n, n], x]; ldegree = -Exponent[b[n, n] /. x -\u003e 1/x, x]; Table[Coefficient[b[n, n], x, i], {i, ldegree, degree}]); Table[T[n], {n, 0, 14}] // Flatten (* _Jean-François Alcover_, Jan 06 2015, translated from Maple *)"
			],
			"program": [
				"(PARI) N=20; q='q+O('q^N);",
				"e(n) = if(n%2!=0, u, 1/u);",
				"gf = 1 / prod(n=1,N, 1 - e(n)*q^n );",
				"V = Vec( gf );",
				"{ for (j=1, #V,  \\\\ print triangle, including leading zeros",
				"    for (i=0, N-j, print1(\"   \"));  \\\\ padding",
				"    for (i=-j+1, j-1, print1(polcoeff(V[j], i, u),\", \"));",
				"    print();",
				"); }",
				"/* _Joerg Arndt_, Mar 31 2014 */"
			],
			"xref": [
				"Columns k=(-1)-10 give: A239832, A045931, A240010, A240011, A240012, A240013, A240014, A240015, A240016, A240017, A240018, A240019.",
				"Row sums give A000041.",
				"T(2n,n) gives A002865.",
				"T(4n,2n) gives A182746.",
				"T(4n+2,2n+1) gives A182747.",
				"Row lengths give A016777(floor(n/2)).",
				"Cf. A240021 (the same for partitions into distinct parts), A242618 (the same for parts counted without multiplicity).",
				"Cf. A209423."
			],
			"keyword": "nonn,tabf",
			"offset": "0,19",
			"author": "_Alois P. Heinz_, Mar 30 2014",
			"references": 24,
			"revision": 44,
			"time": "2018-10-23T20:14:10-04:00",
			"created": "2014-03-30T17:39:52-04:00"
		}
	]
}