{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A292123",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 292123,
			"data": "3,2,15,82,759,6698,83355,1111018,17804811,311922962,6167999175,132938646082,3142313821119,80223941595578,2209482997765395,65134627503574618,2049303263312162451,68484112629314107682,2423689374810702823935,90531454062326770264882,3559791311479931284873479,146968393568442835837277258,6356752152236511568747181835,287439789050158514775171177418,13562654176000911126785202632091",
			"name": "E.g.f. C(x) satisfies: C'(x) = A(x)*B(x) such that C(x)^2 - A(x)^2 = 8 and C(x)^2 - B(x)^2 = 5, where A(x) and B(x) are the e.g.f.s of A292121 and A292122, respectively.",
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A292123/b292123.txt\"\u003eTable of n, a(n) for n = 0..300\u003c/a\u003e"
			],
			"formula": [
				"E.g.f. C(x) and related functions A(x) and B(x) satisfy:",
				"(1a) A(x) = 1 + Integral B(x)*C(x) dx.",
				"(1b) B(x) = 2 + Integral A(x)*C(x) dx.",
				"(1c) C(x) = 3 + Integral A(x)*B(x) dx.",
				"(2a) B(x)^2 - A(x)^2 = 3.",
				"(2b) C(x)^2 - A(x)^2 = 8.",
				"(2c) C(x)^2 - B(x)^2 = 5.",
				"(3a) A(x) = (B'(x) + C'(x))/(B(x) + C(x)).",
				"(3b) B(x) = (C'(x) + A'(x))/(C(x) + A(x)).",
				"(3c) C(x) = (A'(x) + B'(x))/(A(x) + B(x)).",
				"(4a) A(x) + B(x) = 3 * exp( Integral C(x) dx ).",
				"(4b) A(x) + C(x) = 4 * exp( Integral B(x) dx ).",
				"(4c) B(x) + C(x) = 5 * exp( Integral A(x) dx ).",
				"(5a) A(x) = (-5*exp(Integral A(x) dx) + 4*exp(Integral B(x) dx) + 3*exp(Integral C(x) dx))/2.",
				"(5b) B(x) = (5*exp(Integral A(x) dx) - 4*exp(Integral B(x) dx) + 3*exp(Integral C(x) dx))/2.",
				"(5c) C(x) = (5*exp(Integral A(x) dx) + 4*exp(Integral B(x) dx) - 3*exp(Integral C(x) dx))/2.",
				"(6a) A(x)^m = 1 + Integral m * A(x)^(m-1) * B(x) * C(x) dx.",
				"(6b) B(x)^m = 2^m + Integral m * A(x) * B(x)^(m-1) * C(x) dx.",
				"(6c) C(x)^m = 3^m + Integral m * A(x) * B(x) * C(x)^(m-1) dx."
			],
			"example": [
				"E.g.f. C(x) = 3 + 2*x + 15*x^2/2! + 82*x^3/3! + 759*x^4/4! + 6698*x^5/5! + 83355*x^6/6! + 1111018*x^7/7! + 17804811*x^8/8! + 311922962*x^9/9! + 6167999175*x^10/10! +...",
				"Related series.",
				"A(x) = 1 + 6*x + 13*x^2/2! + 102*x^3/3! + 653*x^4/4! + 7134*x^5/5! + 80257*x^6/6! + 1138638*x^7/7! + 17577977*x^8/8! + 314204406*x^9/9! + 6141247573*x^10/10! +...",
				"where C(x)^2 - A(x)^2 = 8.",
				"B(x) = 2 + 3*x + 20*x^2/2! + 78*x^3/3! + 736*x^4/4! + 6672*x^5/5! + 83360*x^6/6! + 1113072*x^7/7! + 17810944*x^8/8! + 311847168*x^9/9! + 6167567360*x^10/10! +...",
				"where C(x)^2 - B(x)^2 = 5.",
				"A(x) + B(x) = 3 + 9*x + 33*x^2/2! + 180*x^3/3! + 1389*x^4/4! + 13806*x^5/5! + 163617*x^6/6! + 2251710*x^7/7! + 35388921*x^8/8! + 626051574*x^9/9! + 12308814933*x^10/10! +...",
				"where A(x) + B(x) =  (A'(x) + B'(x)) / C(x).",
				"A(x) + C(x) = 4 + 8*x + 28*x^2/2! + 184*x^3/3! + 1412*x^4/4! + 13832*x^5/5! + 163612*x^6/6! + 2249656*x^7/7! + 35382788*x^8/8! + 626127368*x^9/9! + 12309246748*x^10/10! +...",
				"where A(x) + C(x) =  (A'(x) + C'(x)) / B(x).",
				"B(x) + C(x) = 5 + 5*x + 35*x^2/2! + 160*x^3/3! + 1495*x^4/4! + 13370*x^5/5! + 166715*x^6/6! + 2224090*x^7/7! + 35615755*x^8/8! + 623770130*x^9/9! + 12335566535*x^10/10! +...",
				"where B(x) + C(x) =  (B'(x) + C'(x)) / A(x)."
			],
			"program": [
				"(PARI) {a(n) = my(A=1,B=2,C=3); for(i=0,n, A = 1 + intformal(B*C +x*O(x^n)); B = 2 + intformal(A*C); C = 3 + intformal(A*B)); n!*polcoeff(C,n)}",
				"for(n=0,30,print1(a(n),\", \"))"
			],
			"xref": [
				"Cf. A292120 (A+B+C), A292121 (A), A292122 (B), A292124 (A*B*C)."
			],
			"keyword": "nonn",
			"offset": "0,1",
			"author": "_Paul D. Hanna_, Sep 08 2017",
			"references": 5,
			"revision": 26,
			"time": "2017-09-09T09:49:40-04:00",
			"created": "2017-09-08T21:54:38-04:00"
		}
	]
}