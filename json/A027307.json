{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A027307",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 27307,
			"data": "1,2,10,66,498,4066,34970,312066,2862562,26824386,255680170,2471150402,24161357010,238552980386,2375085745978,23818652359682,240382621607874,2439561132029314,24881261270812490,254892699352950850",
			"name": "Number of paths from (0,0) to (3n,0) that stay in first quadrant (but may touch horizontal axis) and where each step is (2,1), (1,2) or (1,-1).",
			"comment": [
				"These are the 3-Schroeder numbers according to Yang-Jiang (2021). - _N. J. A. Sloane_, Mar 28 2021",
				"Equals row sums of triangle A104978 which has g.f. F(x,y) that satisfies: F = 1 + x*F^2 + x*y*F^3. - _Paul D. Hanna_, Mar 30 2005",
				"a(n) counts ordered complete ternary trees with 2*n-1 leaves, where the internal vertices come in two colors and such that each vertex and its rightmost child have different colors. See [Drake, Example 1.6.9]. An example is given below. - _Peter Bala_, Sep 29 2011"
			],
			"reference": [
				"Sheng-Liang Yang and Mei-yang Jiang, The m-Schröder paths and m-Schröder numbers, Disc. Math. (2021) Vol. 344, Issue 2, 112209. doi:10.1016/j.disc.2020.112209. See Table 1."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A027307/b027307.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"Alexander Burstein and Louis W. Shapiro, \u003ca href=\"https://arxiv.org/abs/2112.11595\"\u003ePseudo-involutions in the Riordan group\u003c/a\u003e, arXiv:2112.11595 [math.CO], 2021.",
				"Gi-Sang Cheon, S.-T. Jin, L. W. Shapiro, \u003ca href=\"http://dx.doi.org/10.1016/j.laa.2015.03.015\"\u003eA combinatorial equivalence relation for formal power series\u003c/a\u003e, Linear Algebra and its Applications, Available online 30 March 2015.",
				"Emeric Deutsch, \u003ca href=\"http://www.jstor.org/stable/2589192\"\u003eProblem 10658\u003c/a\u003e, American Math. Monthly, 107, 2000, 368-370.",
				"B. Drake, \u003ca href=\"http://people.brandeis.edu/~gessel/homepage/students/drakethesis.pdf\"\u003eAn inversion theorem for labeled trees and some limits of areas under lattice paths (Example 1.6.9)\u003c/a\u003e, A dissertation presented to the Faculty of the Graduate School of Arts and Sciences of Brandeis University.",
				"Elżbieta Liszewska, Wojciech Młotkowski, \u003ca href=\"https://arxiv.org/abs/1907.10725\"\u003eSome relatives of the Catalan sequence\u003c/a\u003e, arXiv:1907.10725 [math.CO], 2019.",
				"J. Winter, M. M. Bonsangue and J. J. M. M. Rutten, \u003ca href=\"http://oai.cwi.nl/oai/asset/21313/21313A.pdf\"\u003eContext-free coalgebras\u003c/a\u003e, 2013.",
				"Anssi Yli-Jyrä and Carlos Gómez-Rodríguez, \u003ca href=\"https://arxiv.org/abs/1706.03357\"\u003eGeneric Axiomatization of Families of Noncrossing Graphs in Dependency Parsing\u003c/a\u003e, arXiv:1706.03357 [cs.CL], 2017."
			],
			"formula": [
				"G.f.: (2/3)*sqrt((z+3)/z)*sin((1/3)*arcsin(sqrt(z)*(z+18)/(z+3)^(3/2)))-1/3.",
				"a(n) = (1/n) * Sum_{i=0..n-1} 2^(i+1)*binomial(2*n, i)*binomial(n, i+1)), n\u003e0.",
				"a(n) = 2*A034015(n-1), n\u003e0.",
				"a(n) = Sum_{k=0..n} C(2*n+k, n+2*k)*C(n+2*k, k)/(n+k+1). - _Paul D. Hanna_, Mar 30 2005",
				"Given g.f. A(x), y=A(x)x satisfies 0=f(x, y) where f(x, y)=x(x-y)+(x+y)y^2 . - _Michael Somos_, May 23 2005",
				"Series reversion of x(Sum_{k\u003e=0} a(k)x^k) is x(Sum_{k\u003e=0} A085403(k)x^k).",
				"G.f. A(x) satisfies A(x)=A006318(x*A(x)). - _Vladimir Kruchinin_, Apr 18 2011",
				"The function B(x) = x*A(x^2) satisfies B(x) = x+x*B(x)^2+B(x)^3 and hence B(x) = compositional inverse of x*(1-x^2)/(1+x^2) = x+2*x^3+10*x^5+66*x^7+.... Let f(x) = (1+x^2)^2/(1-4*x^2+x^4) and let D be the operator f(x)*d/dx. Then a(n) equals 1/(2*n+1)!*D^(2*n)(f(x)) evaluated at x = 0. For a refinement of this sequence see A196201. - _Peter Bala_, Sep 29 2011",
				"D-finite with recurrence: 2*n*(2*n+1)*a(n) = (46*n^2-49*n+12)*a(n-1) - 3*(6*n^2-26*n+27)*a(n-2) - (n-3)*(2*n-5)*a(n-3). - _Vaclav Kotesovec_, Oct 08 2012",
				"a(n) ~ sqrt(50+30*sqrt(5))*((11+5*sqrt(5))/2)^n/(20*sqrt(Pi)*n^(3/2)). - _Vaclav Kotesovec_, Oct 08 2012. Equivalently, a(n) ~ phi^(5*n + 1) / (2 * 5^(1/4) * sqrt(Pi) * n^(3/2)), where phi = A001622 is the golden ratio. - _Vaclav Kotesovec_, Dec 07 2021",
				"a(n) = 2*hypergeom([1 - n, -2*n], [2], 2) for n \u003e= 1. - _Peter Luschny_, Nov 08 2021"
			],
			"example": [
				"a(3) = 10. Internal vertices colored either b(lack) or w(hite); 5 uncolored leaf vertices shown as o.",
				"........b...........b.............w...........w.....",
				"......./|\\........./|\\.........../|\\........./|\\....",
				"....../.|.\\......./.|.\\........./.|.\\......./.|.\\...",
				".....b..o..o.....o..b..o.......w..o..o.....o..w..o..",
				"..../|\\............/|\\......../|\\............/|\\....",
				".../.|.\\........../.|.\\....../.|.\\........../.|.\\...",
				"..o..o..o........o..o..o....o..o..o........o..o..o..",
				"....................................................",
				"........b...........b.............w...........w.....",
				"......./|\\........./|\\.........../|\\........./|\\....",
				"....../.|.\\......./.|.\\........./.|.\\......./.|.\\...",
				".....w..o..o.....o..w..o.......b..o..o.....o..b..o..",
				"..../|\\............/|\\......../|\\............/|\\....",
				".../.|.\\........../.|.\\....../.|.\\........../.|.\\...",
				"..o..o..o........o..o..o....o..o..o........o..o..o..",
				"....................................................",
				"........b...........w..........",
				"......./|\\........./|\\.........",
				"....../.|.\\......./.|.\\........",
				".....o..o..w.....o..o..b.......",
				"........../|\\........./|\\......",
				"........./.|.\\......./.|.\\.....",
				"........o..o..o.....o..o..o....",
				"..............................."
			],
			"mathematica": [
				"a[n_] := ((n+1)*(2n)!*Hypergeometric2F1[-n, 2n+1, n+2, -1]) / (n+1)!^2;",
				"Table[a[n], {n, 0, 19}] (* _Jean-François Alcover_, Nov 14 2011, after Pari *)",
				"a[n_] := If[n == 0, 1, 2*Hypergeometric2F1[1 - n, -2 n, 2, 2]];",
				"Table[a[n], {n, 0, 19}]  (* _Peter Luschny_, Nov 08 2021 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c1,n==0,sum(i=0,n-1,2^(i+1)*binomial(2*n,i)*binomial(n,i+1))/n)",
				"(PARI) a(n)=sum(k=0,n,binomial(2*n+k,n+2*k)*binomial(n+2*k,k)/(n+k+1)) \\\\ _Paul D. Hanna_",
				"(PARI) a(n)=sum(k=0,n, binomial(n,k)*binomial(2*n+k+1,n)/(2*n+k+1) ) /* _Michael Somos_, May 23 2005 */"
			],
			"xref": [
				"Cf. A104978. A196201.",
				"The sequences listed in Yang-Jiang's Table 1 appear to be A006318, A001003, A027307, A034015, A144097, A243675, A260332, A243676. - _N. J. A. Sloane_, Mar 28 2021",
				"Apart from first term, this is 2*A034015. - _N. J. A. Sloane_, Mar 28 2021"
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Emeric Deutsch_",
			"references": 63,
			"revision": 77,
			"time": "2021-12-23T06:07:28-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}