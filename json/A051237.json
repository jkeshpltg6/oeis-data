{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A051237",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 51237,
			"data": "1,1,2,1,2,3,1,2,3,4,1,4,3,2,5,1,4,3,2,5,6,1,4,3,2,5,6,7,1,2,3,4,7,6,5,8,1,2,3,4,7,6,5,8,9,1,2,3,4,7,6,5,8,9,10,1,2,3,4,7,10,9,8,5,6,11,1,2,3,4,7,10,9,8,5,6,11,12,1,2,3,4,7,6,5,12,11,8,9,10,13,1,2,3,4,7,6,13,10",
			"name": "Lexicographically earliest prime pyramid, read by rows.",
			"comment": [
				"Row n begins with 1, ends with n and sum of any two adjacent entries is prime.",
				"From _Daniel Forgues_, May 17 2011 and May 18 2011: (Start)",
				"Since the sum of any two adjacent entries is at least 3, the sum is an odd prime, which implies that any two consecutive entries have opposite parity.",
				"Since the first and last entries of row n are fixed at 1 and n, we have to find n-2 entries, where ceiling((n-2)/2) of them are even and floor((n-2)/2) are odd, so for row n the number of possible arrangements is",
				"  (ceiling((n-2)/2))! * (floor((n-2)/2))! (Cf. A010551(n-2), n \u003e= 2.)",
				"The number of ways of arranging row n to get a prime pyramid is given by A036440. List them in lexicographic order and pick the first (earliest) to get row n of lexicographically earliest prime pyramid.",
				"Prime pyramids are also (more fittingly?) called prime triangles. (End)",
				"It appears that the limit of the rows of the lexicographically earliest prime pyramid is A055265 (see comment in that sequence).",
				"Assuming Dickson's conjecture (or the later Hardy-Littlewood Conjecture B), no backtracking is needed: if the first n-2 elements in each row are chosen greedily, a penultimate member can be chosen such that its sums are prime. - _Charles R Greathouse IV_, May 18 2011"
			],
			"reference": [
				"R. K. Guy, Unsolved Problems Number Theory, Section C1."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A051237/b051237.txt\"\u003eRows n=1..100 of triangle, flattened\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PrimeTriangle.html\"\u003ePrime Triangle\u003c/a\u003e",
				"OEIS Wiki, \u003ca href=\"/wiki/Prime_triangles\"\u003ePrime triangles\u003c/a\u003e"
			],
			"example": [
				"Triangle begins:",
				"1;",
				"1, 2;",
				"1, 2, 3;",
				"1, 2, 3, 4;",
				"1, 4, 3, 2, 5;",
				"1, 4, 3, 2, 5, 6;",
				"1, 4, 3, 2, 5, 6, 7;",
				"1, 2, 3, 4, 7, 6, 5, 8;",
				"1, 2, 3, 4, 7, 6, 5, 8, 9;",
				"1, 2, 3, 4, 7, 6, 5, 8, 9, 10;",
				"1, 2, 3, 4, 7, 10, 9, 8, 5, 6, 11;",
				"1, 2, 3, 4, 7, 10, 9, 8, 5, 6, 11, 12;",
				"1, 2, 3, 4, 7, 6, 5, 12, 11, 8, 9, 10, 13;"
			],
			"mathematica": [
				"(* first do *) Needs[\"Combinatorica`\"] (* then *) f[n_] := Block[{r = Range@ n}, While[ Union[ PrimeQ[ Plus @@@ Partition[r, 2, 1]]][[1]] == False, r = NextPermutation@ r]; r]; f[1] = 1; Array[f, 13] // Flatten (* _Robert G. Wilson v_ *)"
			],
			"xref": [
				"See A187869 for the concatenation of the numbers for each row.",
				"Cf. A036440, A051239, A055265."
			],
			"keyword": "tabl,nice,nonn,easy",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Jud McCranie_"
			],
			"references": 11,
			"revision": 41,
			"time": "2017-03-07T09:45:41-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}