{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A074785",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 74785,
			"data": "3,6,6,5,1,2,9,2,0,5,8,1,6,6,4,3,2,7,0,1,2,4,3,9,1,5,8,2,3,2,6,6,9,4,6,9,4,5,4,2,6,3,4,4,7,8,3,7,1,0,5,2,6,3,0,5,3,6,7,7,7,1,3,6,7,0,5,6,1,6,1,5,3,1,9,3,5,2,7,3,8,5,4,9,4,5,5,8,2,2,8,5,6,6,9,8,9,0,8,3,5,8,3,0",
			"name": "Decimal expansion of -log(log(2)).",
			"comment": [
				"The function f(p) = Integral_{x = 2..infinity} 1/(x*log(x)^p) has a minimum of -e*log(log(2)) = 0.996285... at p = 1 - 1/log(log(2)) = 3.728416... - _Jean-François Alcover_, May 24 2013",
				"log(log(2)) also equals the median of the Gumbel distribution with location parameter 0 and scale parameter 1. - _Jean-François Alcover_, Jul 29 2014"
			],
			"reference": [
				"Donald Knuth, The Art of Computer Programming, 3rd Edition, Volume 1. Boston: Addison-Wesley Professional (1997): 619, Table 1 of Appendix A."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A074785/b074785.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Iaroslav V. Blagouchine, \u003ca href=\"http://dx.doi.org/10.1016/j.jmaa.2016.04.032\"\u003eTwo series expansions for the logarithm of the gamma function involving Stirling numbers and containing only rational coefficients for certain arguments related to Pi^(-1)\u003c/a\u003e, Journal of Mathematical Analysis and Applications, Vol. 442, No. 2 (2016), pp. 404-434.",
				"Dmitrii Kouznetsov and Henryk Trappmann, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-10-02342-2\"\u003ePortrait of the four regular super-exponentials to base sqrt(2)\u003c/a\u003e, Math. Comp., Vol. 79, No. 271 (2010), pp. 1727-1756, eq. (3.2).",
				"Simon Plouffe, \u003ca href=\"http://www.worldwideschool.org/library/books/sci/math/MiscellaneousMathematicalConstants/chap64.html\"\u003elog(log(2))\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GumbelDistribution.html\"\u003eGumbel Distribution\u003c/a\u003e."
			],
			"formula": [
				"Equals Sum_{n\u003e=1} ((-1)^n/(n*n!) * (Sum_{k=1..n} abs(S1(n,k))/(k+1))), where S1(n,k) are the Stirling numbers of the first kind (Blagouchine, 2016). Without the absolute value the formula gives -gamma (= -A001620). - _Amiram Eldar_, Jun 12 2021"
			],
			"example": [
				"log(log(2)) = -0.36651292058166432701243915823266946945..."
			],
			"mathematica": [
				"RealDigits[-Log[Log[2]], 10, 120][[1]] (* _Harvey P. Dale_, Nov 24 2013 *)"
			],
			"program": [
				"(PARI) -log(log(2)) \\\\ _Charles R Greathouse IV_, Jan 04 2016"
			],
			"xref": [
				"Cf. A001620, A059200."
			],
			"keyword": "cons,easy,nonn",
			"offset": "0,1",
			"author": "_Benoit Cloitre_, Sep 07 2002",
			"references": 9,
			"revision": 44,
			"time": "2021-06-12T09:00:13-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}