{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A112544",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 112544,
			"data": "1,1,2,1,1,3,1,2,3,4,1,1,1,2,5,1,2,3,4,5,6,1,1,3,1,5,3,7,1,2,1,4,5,2,7,8,1,1,3,2,1,3,7,4,9,1,2,3,4,5,6,7,8,9,10,1,1,1,1,5,1,7,2,3,5,11,1,2,3,4,5,6,7,8,9,10,11,12,1,1,3,2,5,3,1,4,9,5,11,6,13,1,2,1,4,1,2,7,8,3,2,11,4,13,14",
			"name": "Denominators of fractions n/k in array by antidiagonals.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A112544/b112544.txt\"\u003eAntidiagonals n = 1..50, flattened\u003c/a\u003e"
			],
			"formula": [
				"From _G. C. Greubel_, Jan 12 2022: (Start)",
				"A(n, k) = denominator(n/k) (array).",
				"T(n, k) = denominator((n-k+1)/k) (antidiagonal triangle).",
				"Sum_{k=1..n} T(n, k) = A332049(n+1).",
				"T(n, k) = A112543(n, n-k). (End)"
			],
			"example": [
				"Array begins as:",
				"  1, 2, 3, 4, 5, 6, 7, 8, 9, 10, ...;",
				"  1, 1, 3, 2, 5, 3, 7, 4, 9,  5, ...;",
				"  1, 2, 1, 4, 5, 2, 7, 8, 3, 10, ...;",
				"  1, 1, 3, 1, 5, 3, 7, 2, 9,  5, ...;",
				"  1, 2, 3, 4, 1, 6, 7, 8, 9,  2, ...;",
				"  1, 1, 1, 2, 5, 1, 7, 4, 3,  5, ...;",
				"  1, 2, 3, 4, 5, 6, 1, 8, 9, 10, ...;",
				"  1, 1, 3, 1, 5, 3, 7, 1, 9,  5, ...;",
				"  1, 2, 1, 4, 5, 2, 7, 8, 1, 10, ...;",
				"  1, 1, 3, 2, 1, 3, 7, 4, 9,  1, ...;",
				"Antidiagonal triangle begins as:",
				"  1;",
				"  1, 2;",
				"  1, 1, 3;",
				"  1, 2, 3, 4;",
				"  1, 1, 1, 2, 5;",
				"  1, 2, 3, 4, 5, 6;",
				"  1, 1, 3, 1, 5, 3, 7;",
				"  1, 2, 1, 4, 5, 2, 7, 8;",
				"  1, 1, 3, 2, 1, 3, 7, 4, 9;",
				"  1, 2, 3, 4, 5, 6, 7, 8, 9, 10;"
			],
			"mathematica": [
				"Table[Denominator[(n-k+1)/k], {n,20}, {k,n}]//Flatten (* _G. C. Greubel_, Jan 12 2022 *)"
			],
			"program": [
				"(PARI)",
				"t1(n) = binomial(floor(3/2+sqrt(2*n)),2) -n+1;",
				"t2(n) = n-binomial(floor(1/2+sqrt(2*n)),2);",
				"vector(100,n,t2(n)/gcd(t1(n),t2(n)))",
				"(MAGMA) [Denominator((n-k+1)/k): k in [1..n], n in [1..20]]; // _G. C. Greubel_, Jan 12 2022",
				"(Sage) flatten([denominator((n-k+1)/k) for k in (1..n)] for n in (1..20)]) # _G. C. Greubel_, Jan 12 2022"
			],
			"xref": [
				"Numerators in A112543. See comments and references there.",
				"Cf. A332049."
			],
			"keyword": "easy,frac,nonn,tabl,changed",
			"offset": "1,3",
			"author": "_Franklin T. Adams-Watters_, Sep 11 2005",
			"ext": [
				"Keyword tabl added by _Franklin T. Adams-Watters_, Sep 02 2009"
			],
			"references": 1,
			"revision": 25,
			"time": "2022-01-14T02:00:02-05:00",
			"created": "2005-09-21T03:00:00-04:00"
		}
	]
}