{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A059067",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 59067,
			"data": "1,2,3,0,1,80,192,216,128,96,0,8,12096,46656,81648,93960,69984,40824,11664,5832,0,216,4783104,25214976,62705664,98648064,109859328,87588864,54411264,23887872,9455616,1769472,663552,0",
			"name": "Card-matching numbers (Dinner-Diner matching numbers).",
			"comment": [
				"This is a triangle of card matching numbers. Two decks each have 3 kinds of cards, n of each kind. The first deck is laid out in order. The second deck is shuffled and laid out next to the first. A match occurs if a card from the second deck is next to a card of the same kind from the first deck. Triangle T(n,k) is the number of ways of achieving exactly k matches (k=0..3n). The probability of exactly k matches is T(n,k)/(3n)!.",
				"rows are of length 1,4,7,10,..."
			],
			"reference": [
				"F. N. David and D. E. Barton, Combinatorial Chance, Hafner, NY, 1962, Ch. 7 and Ch. 12.",
				"J. Riordan, An Introduction to Combinatorial Analysis, Wiley, 1958, pp. 174-178.",
				"R. P. Stanley, Enumerative Combinatorics Volume I, Cambridge University Press, 1997, p. 71."
			],
			"link": [
				"F. F. Knudsen and I. Skau, \u003ca href=\"http://www.jstor.org/stable/2691467\"\u003eOn the Asymptotic Solution of a Card-Matching Problem\u003c/a\u003e, Mathematics Magazine 69 (1996), 190-197.",
				"Barbara H. Margolius, \u003ca href=\"http://academic.csuohio.edu/bmargolius/homepage/dinner/dinner/cardentry.htm\"\u003eDinner-Diner Matching Probabilities\u003c/a\u003e",
				"B. H. Margolius, \u003ca href=\"http://www.jstor.org/stable/3219303\"\u003eThe Dinner-Diner Matching Problem\u003c/a\u003e, Mathematics Magazine, 76 (2003), 107-118.",
				"S. G. Penrice, \u003ca href=\"http://www.jstor.org/stable/2324927\"\u003eDerangements, permanents and Christmas presents\u003c/a\u003e, The American Mathematical Monthly 98(1991), 617-620.",
				"\u003ca href=\"/index/Ca#cardmatch\"\u003eIndex entries for sequences related to card matching\u003c/a\u003e"
			],
			"formula": [
				"G.f.: sum(coeff(R(x, n, k), x, j)*(t-1)^j*(n*k-j)!, j=0..n*k) where n is the number of kinds of cards (3 in this case), k is the number of cards of each kind and R(x, n, k) is the rook polynomial given by R(x, n, k)=(k!^2*sum(x^j/((k-j)!^2*j!))^n (see Stanley or Riordan). coeff(R(x, n, k), x, j) indicates the coefficient for x^j of the rook polynomial."
			],
			"example": [
				"There are 216 ways of matching exactly 2 cards when there are 2 cards of each kind and 3 kinds of card so T(2,2)=216."
			],
			"maple": [
				"p := (x,k)-\u003ek!^2*sum(x^j/((k-j)!^2*j!),j=0..k); R := (x,n,k)-\u003ep(x,k)^n; f := (t,n,k)-\u003esum(coeff(R(x,n,k),x,j)*(t-1)^j*(n*k-j)!,j=0..n*k);",
				"for n from 0 to 5 do seq(coeff(f(t,3,n),t,m),m=0..3*n); od;"
			],
			"mathematica": [
				"p[x_, k_] := k!^2*Sum[x^j/((k - j)!^2*j!), {j, 0, k}]; r[x_, n_, k_] := p[x, k]^n; f[t_, n_, k_] := Sum[Coefficient[r[x, n, k], x, j]*(t - 1)^j*(n*k - j)!, {j, 0, n*k}]; Table[ Coefficient[f[t, 3, n], t, m], {n, 0, 5}, {m, 0, 3*n}] // Flatten (* _Jean-François Alcover_, Oct 21 2013, after Maple *)"
			],
			"xref": [
				"Cf. A008290, A059056-A059071."
			],
			"keyword": "nonn,tabf,nice",
			"offset": "0,2",
			"author": "Barbara Haas Margolius (margolius(AT)math.csuohio.edu)",
			"references": 0,
			"revision": 16,
			"time": "2017-09-26T05:09:25-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}