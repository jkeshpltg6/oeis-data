{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A257653",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 257653,
			"data": "1,0,-3,2,0,-6,6,0,-3,12,0,-6,2,0,-12,0,0,-12,18,0,-6,12,0,0,6,0,-18,14,0,-18,12,0,-3,12,0,-12,12,0,-18,0,0,-24,12,0,-6,36,0,0,2,0,-21,12,0,-18,42,0,-12,12,0,-18,0,0,-24,0,0,-24,24,0,-12,24,0",
			"name": "Expansion of f(-x^2)^3 * phi(x^3) / f(-x^6) in powers of x where phi(), f() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A257653/b257653.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of eta(q^2)^3 * eta(q^6)^4 / (eta(q^3)^2 * eta(q^12)^2) in powers of q.",
				"Euler transform of period 12 sequence [ 0, -3, 2, -3, 0, -5, 0, -3, 2, -3, 0, -3, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (36 t)) = 18^(3/2) (t/i)^(3/2) g(t) where q = exp(2 Pi i t) and g() is the g.f. for A257651.",
				"G.f.: Product_{k\u003e0} (1 - x^(2*k))^3 * (1 + x^(3*k))^2 / (1 + x^(6*k))^2.",
				"a(3*n) = A014452(n). a(3*n + 1) = 0. a(3*n + 2) = -3 * A257651(n)."
			],
			"example": [
				"G.f. = 1 - 3*x^2 + 2*x^3 - 6*x^5 + 6*x^6 - 3*x^8 + 12*x^9 - 6*x^11 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x^2]^3 EllipticTheta[ 3, 0, x^3] / QPochhammer[ x^6], {x, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^3 * eta(x^6 + A)^4 / (eta(x^3 + A)^2 * eta(x^12 + A)^2), n))};",
				"(MAGMA) A := Basis( ModularForms( Gamma0(36), 3/2), 71); A[1] - 3*A[3] + 2*A[4] + 6*A[6];"
			],
			"xref": [
				"Cf. A014452, A257651."
			],
			"keyword": "sign",
			"offset": "0,3",
			"author": "_Michael Somos_, Jul 25 2015",
			"references": 1,
			"revision": 14,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2015-07-25T17:03:57-04:00"
		}
	]
}