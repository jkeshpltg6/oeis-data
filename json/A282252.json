{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A282252",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 282252,
			"data": "1,0,1,4,0,1,0,12,0,1,36,0,24,0,1,0,180,0,40,0,1,400,0,540,0,60,0,1,0,2800,0,1260,0,84,0,1,4900,0,11200,0,2520,0,112,0,1,0,44100,0,33600,0,4536,0,144,0,1,63504,0,220500,0,84000,0,7560,0,180,0,1",
			"name": "Exponential Riordan array [Bessel_I(0,2*x)^2, x].",
			"comment": [
				"Bessel_I(0,2*x) = Sum_{n \u003e= 0} binomial(2*n,n)*x^(2*n)/(2*n)! is a modified Bessel function of the first kind.",
				"Consider the infinite 2-dimensional square lattice Z x Z with an oriented self-loop at each vertex. Then the triangle entry T(n,k) equals the number of walks of length n from the origin to itself having k loops. An example is given below.",
				"See A069466 for walks an infinite 2-dimensional square lattice without self-loops.",
				"This is the square of triangle A109187, whose entries give the number of walks of length n from a vertex to itself having k loops on a 1-dimensional integer lattice with an oriented self-loop at each vertex.",
				"A109187 is the exponential Riordan array [Bessel_I(0,2*x), x]. Note that Bessel_I(0,2*x)^2 = (Sum_{n \u003e= 0} binomial(2*n,n)* x^(2*n)/(2*n)!)^2 = Sum_{n \u003e= 0} binomial(2*n,n)^2*x^(2*n) /(2*n)!."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A282252/b282252.txt\"\u003eTable of n, a(n) for the first 50 rows, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = binomial(n,k)*binomial(n-k,floor((n-k)/2))^2*(1 + (-1)^(n-k))/2.",
				"T(n,n-2*k) = n/(n - 2*k)*T(n-1,n-2*k-1).",
				"T(n,k) = the coefficient of t^k in the expansion of (t + X + 1/X + Y + 1/Y)^n.",
				"T(n,k) = 1/Pi^2 * Integral_{y = 0..Pi} Integral_{x = 0..Pi} ( t + 2*cos(x) + 2*cos(y) )^n dx dy.",
				"E.g.f.: exp(x*t)*Bessel_I(0,2*x)^2 = 1 + t*x + (4 + t^2)*x^2/2! + (12*t + t^3)*x^3/3! + (36 + 24*t^2 + t^4)*x^4/4! + ....",
				"The n-th row polynomial R(n,t) = Sum_{k = 0..floor(n/2)} binomial(n,2*k)*binomial(2*k,k)^2 * t^(n-2*k).",
				"Recurrence: n^2*R(n,t) = t*(3*n^2 - 3*n + 1)*R(n-1,t) + (16 - 3*t^2)*(n - 1)^2*R(n-2,t) + t*(t^2 - 16)*(n - 1)*(n - 2)*R(n-3,t) with R(0,t) = 1, R(1,t) = t and R(2,t) = 4 + t^2.",
				"d/dt(R(n,t)) = n*R(n-1,t).",
				"The zeros of the row polynomials appear to lie on the imaginary axis in the complex plane. Also, the zeros of R(n,t) and R(n+1,t) appear to interlace on the imaginary axis."
			],
			"example": [
				"The triangle begins",
				"    1;",
				"    0,   1;",
				"    4,   0,   1;",
				"    0,  12,   0,   1;",
				"   36,   0,  24,   0,   1;",
				"    0, 180,   0,  40,   0,   1;",
				"  400,   0, 540,   0,  60,   0,   1;",
				"  ...",
				"T(3,1) = 12: on the square lattice, let L, R, U, D denote a left step, right step, up step and down step respectively. The 12 walks of length 3 containing a single loop are",
				"    loop L R, loop R L, loop U D, loop D U,",
				"    L loop R, R loop L, U loop D, D loop U,",
				"    L R loop, R L loop, U D loop, D U loop.",
				"The infinitesimal generator of this array has integer entries and begins",
				"      0;",
				"      0,    0;",
				"      4,    0,    0;",
				"      0,   12,    0,    0;",
				"    -12,    0,   24,    0,    0;",
				"      0,  -60,    0,   40,    0,    0;",
				"    160,    0, -180,    0,   60,    0,    0;",
				"      0, 1120,    0, -420,    0,   84,    0,    0;",
				"  -4620,    0, 4480,    0, -840,    0,  112,    0,    0;",
				"  ...",
				"It is the generalized exponential Riordan array [ 2*log(Bessel_I(0,2*x)), x ]."
			],
			"maple": [
				"T := (n, k) -\u003e (1/2)*binomial(n, k)*binomial(n-k, floor((1/2)*n-(1/2)*k))^2*(1+(-1)^(n-k)):",
				"seq(seq(T(n, k), k = 0..n), n = 0..9);"
			],
			"mathematica": [
				"Table[Binomial[n, k] Binomial[n - k, Floor[(n - k)/2]]^2*(1 + (-1)^(n - k))/2, {n, 0, 10}, {k, 0, n}] // Flatten (* _Michael De Vlieger_, Feb 12 2017 *)"
			],
			"program": [
				"(PARI) for(n=0,10, for(k=0,n, print1(binomial(n,k)*binomial(n-k,floor((n-k)/2))^2*(1 + (-1)^(n-k))/2, \", \"))) \\\\ _G. C. Greubel_, Aug 16 2017"
			],
			"xref": [
				"A201805 gives row sums. Cf. A069466, A109187."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,4",
			"author": "_Peter Bala_, Feb 12 2017",
			"references": 3,
			"revision": 20,
			"time": "2017-08-21T05:49:19-04:00",
			"created": "2017-02-12T21:17:47-05:00"
		}
	]
}