{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A118239",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 118239,
			"data": "1,2,12,30,56,90,132,182,240,306,380,462,552,650,756,870,992,1122,1260,1406,1560,1722,1892,2070,2256,2450,2652,2862,3080,3306,3540,3782,4032,4290,4556,4830,5112,5402,5700,6006,6320,6642,6972,7310,7656,8010,8372",
			"name": "Engel expansion of cosh(1).",
			"comment": [
				"Differs from A002939 only in first term.",
				"This sequence is also the Pierce expansion of cos(1). - _G. C. Greubel_, Nov 14 2016"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A118239/b118239.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/EngelExpansion.html\"\u003eEngel Expansion\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PierceExpansion.html\"\u003ePierce Expansion\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1)."
			],
			"formula": [
				"a(n) = A002939(n-1) = 2*(n-1)*(2*n-3) for n\u003e1.",
				"From _Colin Barker_, Apr 13 2012: (Start)",
				"a(n) = 3*a(n-1) - 3*a(n-2) + a(n-3).",
				"G.f.: x*(1 - x + 9*x^2 - x^3)/(1-x)^3. (End)",
				"E.g.f.: -6 + x + 2*(3 - 3*x + 2*x^2)*exp(x). - _G. C. Greubel_, Oct 27 2016"
			],
			"mathematica": [
				"Join[{1}, Table[(2 n - 2) (2 n - 3), {n, 2, 50}]] (* _Bruno Berselli_, Aug 04 2015 *)",
				"Join[{1}, LinearRecurrence[{3,-3,1},{2,12,30},25]] (* _G. C. Greubel_, Oct 27 2016 *)",
				"PierceExp[A_, n_] := Join[Array[1 \u0026, Floor[A]], First@Transpose@ NestList[{Floor[1/Expand[1 - #[[1]] #[[2]]]], Expand[1 - #[[1]] #[[2]]]} \u0026, {Floor[1/(A - Floor[A])], A - Floor[A]}, n - 1]]; PierceExp[N[Cos[1] , 7!], 50] (* _G. C. Greubel_, Nov 14 2016 *)"
			],
			"program": [
				"(PARI) a(n)=max(4*n^2-10*n+6, 1) \\\\ _Charles R Greathouse IV_, Oct 22 2014",
				"(Sage)",
				"A118239 = lambda n: falling_factorial(n*2,2) if n\u003e0 else 1",
				"print([A118239(n) for n in (0..46)]) # _Peter Luschny_, Aug 04 2015"
			],
			"xref": [
				"Cf. A006784, A002939, A068377."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Eric W. Weisstein_, Apr 17 2006",
			"references": 4,
			"revision": 33,
			"time": "2020-03-07T08:50:49-05:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}