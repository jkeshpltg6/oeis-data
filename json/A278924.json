{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A278924",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 278924,
			"data": "4,296,36772,1288688,96641548,26576092808,8637277012172,1079658805128928,91770997994914276,43591225139846360008",
			"name": "A sequence showing numerators in ratios tending to the constant Pi/4 = 0.785398163397448... .",
			"comment": [
				"The ratios c(n)/d(n) rapidly tend to the constant Pi/4 = 0.785398163397448... with increasing index n: abs(Pi/4 - c(1)/d(1)) \u003e abs(Pi/4 - c(2)/d(2)) \u003e abs(Pi/4 - c(3)/d(3)) \u003e abs(Pi/4 - c(4)/d(4)) ..., where c(n) = A278924(n) and d(n) = A278364(n) are even and odd positive integers, respectively."
			],
			"link": [
				"Sanjar Abrarov, \u003ca href=\"/A278924/b278924.txt\"\u003eTable of n, a(n) for n = 1..49\u003c/a\u003e",
				"S. M. Abrarov and B. M. Quine, \u003ca href=\"https://arxiv.org/abs/1610.07713\"\u003eA generalized Viéte's-like formula for pi with rapid convergence\u003c/a\u003e, arXiv:1610.07713 [math.GM], (2016)."
			],
			"formula": [
				"arctan(1) = I*lim_{M -\u003e inf}Sum_{m = 1..floor(M/2) + 1}(1/(2*m - 1))*(1/(1 + 2*I)^(2*m - 1) - 1/(1 - 2*I)^(2*m - 1))"
			],
			"example": [
				"------------------------------------------------",
				"n    c(n)                   d(n)",
				"------------------------------------------------",
				"1    4                      5",
				"2    296                    375",
				"3    36772                  46875",
				"4    1288688                1640625",
				"5    96641548               123046875",
				"6    26576092808            33837890625",
				"7    8637277012172          10997314453125",
				"8    1079658805128928       1374664306640625",
				"9    91770997994914276      116846466064453125",
				"10   43591225139846360008   55502071380615234375",
				"------------------------------------------------",
				"At n = 6 the ratio c(6)/d(6) = 26576092808/33837890625 is close to Pi/4. However, at n = 10 the ratio c(10)/d(10) = 43591225139846360008/55502071380615234375 becomes more closer to Pi/4."
			],
			"mathematica": [
				"x := 1;(* argument x *)",
				"M := 1;(* initial value for the integer M *)",
				"n := 1; (* index *)",
				"(* Note that arctan(1) = Pi/4 *)",
				"atan := I*Sum[(1/(2*m - 1))*(1/(1 + 2*(I/x))^(2*m - 1) - 1/(1 - 2*(I/x))^(2*m - 1)),{m, 1, Floor[M/2] + 1}];",
				"sqn := {};(* initiate the sequence *)",
				"AppendTo[sqn,{\"Index n\", \"Numerators\", \"Denominators\"}];",
				"While[M \u003c= 20, AppendTo[sqn,{n, Numerator[atan], Denominator[atan]}];",
				"{M = M + 2, n++}];",
				"Print[MatrixForm[sqn]]"
			],
			"xref": [
				"Cf. A278364, A003881, A096954, A096955."
			],
			"keyword": "nonn,frac",
			"offset": "1,1",
			"author": "_Sanjar Abrarov_, Dec 01 2016",
			"references": 2,
			"revision": 26,
			"time": "2016-12-07T11:24:26-05:00",
			"created": "2016-12-07T11:24:26-05:00"
		}
	]
}