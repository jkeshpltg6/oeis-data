{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A339803",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 339803,
			"data": "2002,30003,131003,200200,231213,300131,312132,400004,420024,1312132,1410004,2002000,2002002,2312131,2312132,3000300,4000141,5000005,5300035,12132003,13100300,14100141,14130043,15100005,15120025,20020000,23121300,23421314,25121005,25320035,30003000,30013100,30023121,31213200",
			"name": "Base 10 super-weak Skolem-Langford numbers.",
			"comment": [
				"Pick any digit d of a(n): there are exactly d digits between d and the closest duplicate of d (either before or after) inside a(n).",
				"There are infinitely many such terms.",
				"From _M. F. Hasler_, Dec 19 2020: (Start)",
				"If N is a term of the sequence, then:",
				"(1) Any digit of N must be present at least twice in N (Cf. A115853).",
				"(2) N*10^k is also a term of the sequence, for all k \u003e= 2.",
				"(3) The reversal R(N) = A004086(N) is also a term (with leading zeros deleted). (End)"
			],
			"link": [
				"David A. Corneth, \u003ca href=\"/A339803/b339803.txt\"\u003eTable of n, a(n) for n = 1..4080\u003c/a\u003e (terms \u003c= 10^12).",
				"Kevin Ryde, \u003ca href=\"/A339803/a339803.foma.txt\"\u003eFoma script creating DFA, and testing some properties\u003c/a\u003e"
			],
			"example": [
				"a(1) = 2002 and in 2002 the closest duplicate of the first 2 is 2 positions away to the right, the closest duplicate of the first 0 is 0 position away to the right, the closest duplicate of the second 0 is 0 position away to the left, the closest duplicate of the second 2 is 2 positions away to the left;",
				"a(2) = 30003 and in 30003 the closest duplicate of the first 3 is 3 positions away to the right, the closest duplicate of the first 0 is 0 position away to the right, the closest duplicate of the second 0 is 0 position away (either to the left or to the right), the closest duplicate of the third 0 is 0 position away to the left, the closest duplicate of the second 3 is 3 positions away to the left;",
				"a(13) = 2312131: if you pick any digit 1, the closest duplicate of this 1 is 1 position away (either to the left or to the right), if you pick any 2, the closest duplicate of this 2 is 2 positions away, if you pick any 3, the closest duplicate of this 3 is 3 positions away, etc."
			],
			"program": [
				"(Python)",
				"def nn(ti, t, s):",
				"  li = s.rfind(t, 0, max(ti, 0))",
				"  ri = s.find(t, min(ti+1, len(s)), len(s))",
				"  if li==-1: li = -11",
				"  if ri==-1: ri = len(s)+11",
				"  return min(ti-li, ri-ti) - 1",
				"def ok(n):",
				"  strn = str(n)",
				"  if any(strn.count(c)==1 for c in set(strn)): return False",
				"  for i, c in enumerate(strn):",
				"    if nn(i, c, strn) != int(c): return False",
				"  return True",
				"for n in range(6*10**6):",
				"  if ok(n): print(n, end=\", \") # _Michael S. Branicky_, Dec 17 2020",
				"(PARI) is_A339803(n)={!for(i=1,#n=digits(n), (i\u003en[i]+1 \u0026\u0026 n[i-n[i]-1]==n[i])||(i+n[i]\u003c#n \u0026\u0026 n[i+n[i]+1]==n[i])||return; for(j=max(i-n[i],1), min(i+n[i],#n), n[j]==n[i] \u0026\u0026 j!=i \u0026\u0026 return))} \\\\ _M. F. Hasler_, Dec 19 2020"
			],
			"xref": [
				"Cf. base-10 Skolem-Langford numbers: A108116 (weak), A132291 (strong).",
				"Cf. A339611 (same idea turned into a different sequence).",
				"Cf. A115853."
			],
			"keyword": "base,nonn",
			"offset": "1,1",
			"author": "_Eric Angelini_ and _Carole Dubois_, Dec 17 2020",
			"references": 2,
			"revision": 34,
			"time": "2021-02-01T10:21:52-05:00",
			"created": "2020-12-18T04:18:27-05:00"
		}
	]
}