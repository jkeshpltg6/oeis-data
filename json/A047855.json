{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A047855",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 47855,
			"data": "1,2,12,112,1112,11112,111112,1111112,11111112,111111112,1111111112,11111111112,111111111112,1111111111112,11111111111112,111111111111112,1111111111111112,11111111111111112,111111111111111112,1111111111111111112,11111111111111111112",
			"name": "a(n) = T(7, n), array T given by A047848.",
			"comment": [
				"Range of A164898, apart from first term. - _Reinhard Zumkeller_, Aug 30 2009",
				"a(n) is the number of integers less than or equal to 10^n, whose initial digit is 1. - _Michel Marcus_, Jul 04 2019",
				"a(n) is 2^n represented in bijective base-2 numeration. - _Alois P. Heinz_, Aug 26 2019",
				"This sequence proves both A028842 (numbers with prime product of digits) and A028843 (numbers with prime iterated product of digits) are infinite. Proof: Suppose either of those sequences is finite. Label as omega the supposed last term. Compute n = ceiling(log_10 omega) + 1. Then a(n) \u003e omega. The product of digits of a(n) is 2, contradicting the assumption that omega is the final term of either A028842 or A028843. - _Alonso del Arte_, Apr 14 2020"
			],
			"link": [
				"Ivan Panchenko, \u003ca href=\"/A047855/b047855.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Bijective_numeration\"\u003eBijective numeration\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (11,-10)."
			],
			"formula": [
				"a(n) = (10^n + 8)/9. - _Ralf Stephan_, Feb 14 2004",
				"a(0) = 1, a(1) = 2, a(n) = 11*a(n - 1) - 10*a(n - 2) for n \u003e 1. - Lambert Klasen (lambert.klasen(AT)gmx.net), Jan 28 2005",
				"G.f.: (1 - 9*x)/(1 - 11*x + 10*x^2). - _Philippe Deléham_, Oct 05 2009",
				"a(n) = 10*a(n-1) - 8 (with a(0) = 1). - _Vincenzo Librandi_, Aug 06 2010"
			],
			"maple": [
				"a[0]:=0:a[1]:=1:for n from 2 to 50 do a[n]:=10*a[n-1]+1 od: seq(a[n]+1, n=0..18); # _Zerinvary Lajos_, Mar 20 2008"
			],
			"mathematica": [
				"Join[{1}, Table[FromDigits[PadLeft[{2}, n, 1]], {n, 30}]] (* _Harvey P. Dale_, Apr 17 2013 *)",
				"(10^Range[0, 29] + 8)/9 (* _Alonso del Arte_, Apr 12 2020 *)"
			],
			"program": [
				"(PARI) a(n)=if(n==0,1,if(n==1,2,11*a(n-1)-10*a(n-2)))",
				"for(i=0,10,print1(a(i),\",\")) \\\\ Lambert Klasen, Jan 28 2005",
				"(Sage) [gaussian_binomial(n,1,10)+1 for n in range(17)] # _Zerinvary Lajos_, May 29 2009",
				"(Scala) (List.fill(20)(10: BigInt)).scanLeft(1: BigInt)(_ * _).map(n =\u003e (n + 8)/9) // _Alonso del Arte_, Apr 12 2020"
			],
			"xref": [
				"n-th difference of a(n), a(n-1), ..., a(0) is 9^(n-1) for n = 1, 2, 3, ..."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Clark Kimberling_",
			"ext": [
				"More terms from _Harvey P. Dale_, Apr 17 2013"
			],
			"references": 16,
			"revision": 52,
			"time": "2020-10-18T01:51:09-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}