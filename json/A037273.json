{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A037273",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 37273,
			"data": "-1,0,0,2,0,1,0,13,2,4,0,1,0,5,4,4,0,1,0,15,1,1,0,2,3,4,4,1,0,2,0,2,1,5,3,2,0,2,1,9,0,2,0,9,6,1,0,15",
			"name": "Number of steps to reach a prime under \"replace n with concatenation of its prime factors\", or -1 if no prime is ever reached.",
			"comment": [
				"Starting with 49, no prime has been reached after 79 steps.",
				"a(49) \u003e 118, see A056938 and FactorDB link. - _Michael S. Branicky_, Nov 19 2020"
			],
			"link": [
				"Patrick De Geest, \u003ca href=\"http://www.worldofnumbers.com/topic1.htm\"\u003eHome Primes\u003c/a\u003e",
				"FactorDB, \u003ca href=\"http://factordb.com/sequences.php?se=10\u0026amp;aq=49\u0026amp;action=all\u0026amp;fr=0\u0026amp;to=100\"\u003eHome prime base 10\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HomePrime.html\"\u003eHome Prime\u003c/a\u003e"
			],
			"example": [
				"13 is already prime, so a(13) = 0.",
				"Starting with 14 we get 14 = 2*7, 27 = 3*3*3, 333 = 3*3*37, 3337 = 47*71, 4771 = 13*367, 13367 is prime; so a(14) = 5."
			],
			"mathematica": [
				"nxt[n_] := FromDigits[Flatten[IntegerDigits/@Table[#[[1]], {#[[2]]}]\u0026/@ FactorInteger[n]]]; Table[Length[NestWhileList[nxt, n, !PrimeQ[#]\u0026]] - 1, {n, 48}] (* _Harvey P. Dale_, Jan 03 2013 *)"
			],
			"program": [
				"(Haskell)",
				"a037273 1 = -1",
				"a037273 n = length $ takeWhile ((== 0) . a010051) $",
				"   iterate (\\x -\u003e read $ concatMap show $ a027746_row x :: Integer) n",
				"-- _Reinhard Zumkeller_, Jan 08 2013",
				"(PARI) row_a027746(n, o=[1])=if(n\u003e1, concat(apply(t-\u003evector(t[2], i, t[1]), Vec(factor(n)~))), o) \\\\ after _M. F. Hasler_ in A027746",
				"tonum(vec) = my(s=\"\"); for(k=1, #vec, s=concat(s, Str(vec[k]))); eval(s)",
				"a(n) = if(n==1, return(-1)); my(x=n, i=0); while(1, if(ispseudoprime(x), return(i)); x=tonum(row_a027746(x)); i++) \\\\ _Felix Fröhlich_, May 17 2021",
				"(Python)",
				"from sympy import factorint",
				"def a(n):",
				"    if n \u003c 2: return -1",
				"    klst, f = [n], sorted(factorint(n, multiple=True))",
				"    while len(f) \u003e 1:",
				"        klst.append(int(\"\".join(map(str, f))))",
				"        f = sorted(factorint(klst[-1], multiple=True))",
				"    return len(klst) - 1",
				"print([a(n) for n in range(1, 49)]) # _Michael S. Branicky_, Aug 02 2021"
			],
			"xref": [
				"Cf. A037271, A037272, A037274, A037275, A056938.",
				"Cf. A010051, A027746."
			],
			"keyword": "sign,nice,hard,base,more",
			"offset": "1,4",
			"author": "_N. J. A. Sloane_, _Jeff Burch_",
			"ext": [
				"Edited by _Charles R Greathouse IV_, Apr 23 2010",
				"a(1) = -1 by _Reinhard Zumkeller_, Jan 08 2013",
				"Name edited by _Felix Fröhlich_, May 17 2021"
			],
			"references": 30,
			"revision": 40,
			"time": "2021-08-02T06:43:52-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}