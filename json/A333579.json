{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A333579",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 333579,
			"data": "1,2,8,32,128,502,1904,6862,22784,64832,120008,-223606,-4311424,-33271366,-205802344,-1142307968,-5919738880,-29159028386,-137718099760,-626077804826,-2740865583872,-11523690799904,-46214332516520,-174358991625134,-601230820510720",
			"name": "a(n) = [x^n] ( (1 + x + x^2)/(1 - x + x^2) )^n.",
			"comment": [
				"If F(x) = 1 + f(1)*x + f(2)*x^2 + ... is a formal power series with integer coefficients then the sequence u(n) := [x^n] F(x)^n is an integer sequence satisfying the Gauss congruences: u(n*p^k) == u(n*p^(k-1)) ( mod p^k ) for all prime p and positive integers n and k. In particular u(p^k) == u(p^(k-1)) ( mod p^k ) for all prime p and positive integer k. For certain power series F(x) we may get stronger congruences.",
				"According to Zhi-Wei Sun (see his comment in A002426 posted Nov 30, 2016), the central trinomial coefficients A002426(n) = [x^n] (1 + x + x^2)^n, satisfy the congruences A002426(p) == 1 ( mod p^2 ) for prime p \u003e= 5. More generally, calculation suggests that the congruences A002426(p^k) == A002426(p^(k-1)) ( mod p^(2*k) ) hold for prime p \u003e= 5 and any positive integer k.",
				"We conjecture that the present sequence satisfies the congruences a(p) == 2 ( mod p^3 ) for prime p \u003e= 5 (checked up to p = 499). Calculation suggests that a(p^k) == a(p^(k-1)) ( mod p^(2*k) ) for prime p \u003e= 5 and k \u003e 1.",
				"More generally, if c(m,x) denotes the m-th cyclotomic polynomial then the sequences a(m,n) := [x^n] c(m,x)^n and b(m,n) := [x^n] ( c(m,x)/c(m,-x) )^n may satisfy the congruences a(m,p) == a(m,1) ( mod p^2 ) and b(m,p) == b(m,1) ( mod p^3 ), both for prime p \u003e= 5, p not a divisor of m. The present sequence is b(3,n). Note that b(2,n) = A002003(n)."
			],
			"link": [
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Cyclotomic_polynomial\"\u003eCyclotomic polynomial\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{0 \u003c= i,j,k \u003c= n} (-1)^(n-k-i-j)*C(n,k)*C(k,i)*C(n+j-1,j)*C(j,n-k-i-j)."
			],
			"example": [
				"Examples of congruences a(p) - a(1) == 0 ( mod p^3 ):",
				"a(11) - a(1) = -223606 - 2 = -(2^3)*3*7*11^3 == 0 ( mod 11^3 )",
				"a(19) - a(1) = -626077804826 - 2 = -(2^2)*7*(19^3)*151*21589 == 0 ( mod 19^3 )"
			],
			"maple": [
				"seq(add(add(add((-1)^(n-k-i-j)*binomial(n, k)*binomial(k, i)*binomial(n+j-1, j)*binomial(j, n-k-i-j), j = 0..n-k-i), i = 0..n-k), k = 0..n), n = 0..25);",
				"#alternative program",
				"G := x -\u003e (1 + x + x^2)/(1 - x + x^2):",
				"H := (x,n) -\u003e series(G(x)^n, x, n+1):",
				"a:= n -\u003e coeff(H(x, n), x, n):",
				"seq(a(n), n = 0..25);"
			],
			"program": [
				"(PARI) a(n) = polcoeff(((1 + x + x^2)/(1 - x + x^2))^n+ O(x^(n+1)), n, x); \\\\ _Michel Marcus_, Mar 31 2020"
			],
			"xref": [
				"Cf. A002003, A002426, A187925, A318114."
			],
			"keyword": "sign,easy",
			"offset": "0,2",
			"author": "_Peter Bala_, Mar 29 2020",
			"references": 0,
			"revision": 17,
			"time": "2021-10-06T14:39:46-04:00",
			"created": "2020-04-02T11:46:51-04:00"
		}
	]
}