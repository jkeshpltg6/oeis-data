{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A111918",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 111918,
			"data": "1,9,89,721,18601,2089,103961,832913,68093153,68347169,8320810649,8331482849,1414167788681,1416817979081,1421435199689,11373510649537,3295255574810593,366551352989977,132591913780524097,132652127531625601",
			"name": "Numerator of x(n) = Sum_{k=1..n} ((odd part of k)/(k^3)).",
			"comment": [
				"Denominator of x(n) = A111919(n);",
				"x(n) = a(n)/A111919(n) ---\u003e Pi*Pi/7 = 6*zeta(2)/7."
			],
			"reference": [
				"G. Pólya and G. Szegő, Problems and Theorems in Analysis II (Springer 1924, reprinted 1972), Part Eight, Chap. 1, Sect. 6, Problem 50."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A111918/b111918.txt\"\u003eTable of n, a(n) for n = 1..1150\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/OddPart.html\"\u003eOdd Part\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RiemannZetaFunctionZeta2.html\"\u003eRiemann Zeta Function zeta(2)\u003c/a\u003e"
			],
			"example": [
				"a(50) = 429245027972423430658635002176171233144054521,",
				"A111919(50) = 307330458857514095936081844184308729630720000:",
				"x(50) = a(50)/A111919(50) = 1.39668..., x(50)*7/6 = 1.62946...."
			],
			"maple": [
				"S:= 0: Res:= NULL:",
				"for k from 1 to 25 do",
				"  S:= S + 1/k^2/2^padic:-ordp(k,2);",
				"  Res:= Res, numer(S)",
				"od:",
				"Res; # _Robert Israel_, Jan 13 2020"
			],
			"mathematica": [
				"oddPart[n_] := n/2^IntegerExponent[n, 2];",
				"x[n_] := Sum[oddPart[k]/k^3, {k, 1, n}];",
				"a[n_] := Numerator[x[n]];",
				"Array[a, 20] (* _Jean-François Alcover_, Dec 13 2021 *)"
			],
			"program": [
				"(MAGMA) val:=func\u003cn|n/2^Valuation(n,2)\u003e; [Numerator(\u0026+[val(k)/(k^3):k in [1..n]]):n in [1..20]]; // _Marius A. Burtea_, Jan 13 2020"
			],
			"xref": [
				"Cf. A000265, A013661, A111919, A111929, A111920, A111922."
			],
			"keyword": "nonn,frac",
			"offset": "1,2",
			"author": "_Reinhard Zumkeller_, Aug 21 2005",
			"references": 6,
			"revision": 22,
			"time": "2021-12-13T08:05:31-05:00",
			"created": "2005-09-21T03:00:00-04:00"
		}
	]
}