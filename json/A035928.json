{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A035928",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 35928,
			"data": "2,10,12,38,42,52,56,142,150,170,178,204,212,232,240,542,558,598,614,666,682,722,738,796,812,852,868,920,936,976,992,2110,2142,2222,2254,2358,2390,2470,2502,2618,2650,2730,2762,2866,2898,2978,3010,3132,3164,3244",
			"name": "Numbers n such that BCR(n) = n, where BCR = binary-complement-and-reverse = take one's complement then reverse bit order.",
			"comment": [
				"Numbers n such that A036044(n) = n.",
				"Also: numbers such that n+BR(n) is in A000225={2^k-1} (with BR = binary reversed). - _M. F. Hasler_, Dec 17 2007"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A035928/b035928.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Aayush Rajasekaran, Jeffrey Shallit, and Tim Smith, \u003ca href=\"https://arxiv.org/abs/1706.10206\"\u003eSums of Palindromes: an Approach via Nested-Word Automata\u003c/a\u003e, preprint arXiv:1706.10206 [cs.FL], June 30 2017."
			],
			"formula": [
				"If offset were 0, a(2n+1) - a(2n) = 2^floor(log_2(n)+1)."
			],
			"example": [
				"38 is such a number because 38=100110; complement to get 011001, then reverse bit order to get 100110."
			],
			"maple": [
				"[seq(ReflectBinSeq(j,(floor_log_2(j)+1)),j=1..256)];",
				"ReflectBinSeq := (x,n) -\u003e (((2^n)*x)+binrevcompl(x));",
				"binrevcompl := proc(nn) local n,z; n := nn; z := 0; while(n \u003c\u003e 0) do z := 2*z + ((n+1) mod 2); n := floor(n/2); od; RETURN(z); end;",
				"floor_log_2 := proc(n) local nn,i: nn := n; for i from -1 to n do if(0 = nn) then RETURN(i); fi: nn := floor(nn/2); od: end; # Computes essentially the same as floor(log[2](n))",
				"# alternative Maple program:",
				"q:= n-\u003e (l-\u003e is(n=add((1-l[-i])*2^(i-1), i=1..nops(l))))(Bits[Split](n)):",
				"select(q, [$1..3333])[];  # _Alois P. Heinz_, Feb 10 2021"
			],
			"mathematica": [
				"bcrQ[n_]:=Module[{idn2=IntegerDigits[n,2]},Reverse[idn2/.{1-\u003e0,0-\u003e1}] == idn2]; Select[Range[3200],bcrQ] (* _Harvey P. Dale_, May 24 2012 *)"
			],
			"program": [
				"(PARI) for(n=1,1000,l=length(binary(n)); b=binary(n); if(sum(i=1,l,abs(component(b,i)-component(b,l+1-i)))==l,print1(n,\",\")))",
				"(PARI) for(i=1,999,if(Set(vecextract(t=binary(i),\"-1..1\")+t)==[1],print1(i\",\"))) \\\\ _M. F. Hasler_, Dec 17 2007",
				"(PARI) a(n) = my (b=binary(n)); (n+1)*2^#b-fromdigits(Vecrev(b),2)-1 \\\\ _Rémy Sigrist_, Mar 15 2021",
				"(Haskell)",
				"a035928 n = a035928_list !! (n-1)",
				"a035928_list = filter (\\x -\u003e a036044 x == x) [0,2..]",
				"-- _Reinhard Zumkeller_, Sep 16 2011",
				"(Python)",
				"def comp(s): z, o = ord('0'), ord('1'); return s.translate({z:o, o:z})",
				"def BCR(n): return int(comp(bin(n)[2:])[::-1], 2)",
				"def aupto(limit): return [m for m in range(limit+1) if BCR(m) == m]",
				"print(aupto(3244)) # _Michael S. Branicky_, Feb 10 2021"
			],
			"xref": [
				"Cf. A061855.",
				"Cf. A000225.",
				"Intersection of A195064 and A195066; cf. A195063, A195065."
			],
			"keyword": "nonn,nice,easy,base",
			"offset": "1,1",
			"author": "Mike Keith (domnei(AT)aol.com)",
			"ext": [
				"More terms from _Erich Friedman_"
			],
			"references": 27,
			"revision": 38,
			"time": "2021-07-27T15:47:30-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}