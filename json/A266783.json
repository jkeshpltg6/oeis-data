{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A266783",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 266783,
			"data": "1,5,14,30,55,91,140,204,285,385,506,651,823,1024,1256,1521,1821,2158,2534,2952,3415,3925,4485,5098,5766,6491,7275,8120,9028,10002,11046,12162,13351,14616,15960,17385,18893,20486,22167,23939,25805,27768,29829,31989,34251,36618,39092,41675,44370,47180,50106,53150,56315,59602,63012",
			"name": "The growth series for the affine Coxeter (or Weyl) group [3,3,5] (or H_4).",
			"reference": [
				"N. Bourbaki, Groupes et Algèbres de Lie, Chap. 4, 5 and 6, Hermann, Paris, 1968. See Chap. VI, Section 4, Problem 10b, page 231, W_a(t).",
				"H. S. M. Coxeter and W. O. J. Moser, Generators and Relations for Discrete Groups, 4th ed., Springer-Verlag, NY, reprinted 1984, Table 10.",
				"J. E. Humphreys, Reflection Groups and Coxeter Groups, Cambridge, 1990. See under Poincaré polynomial; also Table 3.1 page 59."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A266783/b266783.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e"
			],
			"formula": [
				"G.f. = t1/t2 where t1 is (1 + t)*(1 + t + t^2 + t^3 + t^4 + t^5 + t^6 + t^7 + t^8 + t^9 + t^10 + t^11)*(1 + t + t^2 + t^3 + t^4 + t^5 + t^6 + t^7 + t^8 + t^9 + t^10 + t^11 + t^12 + t^13 + t^14 + t^15 + t^16 + t^17 + t^18 + t^19)*(1 + t + t^2 + t^3 + t^4 + t^5 + t^6 + t^7 + t^8 + t^9 + t^10 + t^11 + t^12 + t^13 + t^14 + t^15 + t^16 + t^17 + t^18 + t^19 + t^20 + t^21 + t^22 + t^23 + t^24 + t^25 + t^26 + t^27 + t^28 + t^29) and t2 = (1 - t)*(1 - t^11)*(1 - t^19)*(1 - t^29).",
				"G.f.: (1 - x^2)*(1 - x^12)*(1 - x^20)*(1 - x^30)/((1 - x)^5*(1 - x^11)*(1 - x^19)*(1 - x^29)). - _G. C. Greubel_, Feb 04 2020"
			],
			"maple": [
				"m:=60; S:=series((1-x^2)*(1-x^12)*(1-x^20)*(1-x^30)/((1-x)^5*(1-x^11)*(1-x^19)*(1-x^29)), x, m+1): seq(coeff(S, x, j), j=0..m); # _G. C. Greubel_, Feb 04 2020"
			],
			"mathematica": [
				"CoefficientList[Series[(1 + t)^4 * (1 + t^2) * (1 + t^2 + t^4) * (1 + t^4 + t^8) * (1 + t^2 + t^4 + t^6 + t^8) * (1 + t^6 + t^10 + t^12 + t^16 + t^18 + t^22 + t^24 + t^28 + t^34)/((1 - t) * (1 - t^11) * (1 - t^19) * (1 - t^29)), {t, 0, 60}], t] (* _Wesley Ivan Hurt_, Apr 12 2017; modified by _G. C. Greubel_, Feb 04 2020 *)"
			],
			"program": [
				"(PARI) Vec( (1-x^2)*(1-x^12)*(1-x^20)*(1-x^30)/((1-x)^5*(1-x^11)*(1-x^19)*(1-x^29)) +O('x^60) ) \\\\ _G. C. Greubel_, Feb 04 2020",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Integers(), 60); Coefficients(R!( (1-x^2)*(1-x^12)*(1-x^20)*(1-x^30)/((1-x)^5*(1-x^11)*(1-x^19)*(1-x^29)) )); // _G. C. Greubel_, Feb 04 2020",
				"(Sage)",
				"def A266783_list(prec):",
				"    P.\u003cx\u003e = PowerSeriesRing(ZZ, prec)",
				"    return P( (1-x^2)*(1-x^12)*(1-x^20)*(1-x^30)/((1-x)^5*(1-x^11)*(1-x^19)*(1-x^29)) ).list()",
				"A266783_list(60) # _G. C. Greubel_, Feb 04 2020"
			],
			"xref": [
				"For the growth series for the finite group see A162497."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Jan 11 2016",
			"references": 1,
			"revision": 33,
			"time": "2020-02-06T14:55:43-05:00",
			"created": "2016-01-11T22:33:58-05:00"
		}
	]
}