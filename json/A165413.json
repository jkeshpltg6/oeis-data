{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A165413",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 165413,
			"data": "1,1,1,2,1,2,1,2,2,1,2,1,2,2,1,2,2,2,2,2,1,2,2,2,2,2,2,2,2,2,1,2,2,2,3,2,2,2,3,2,2,1,2,2,2,2,2,2,3,2,1,2,2,2,3,1,3,2,3,2,2,2,1,2,2,2,3,3,2,3,2,3,2,2,2,2,2,3,3,2,2,2,2,2,1,2,2,3,2,2,2,3,2,2,2,2,3,3,2,2,2,2,2,3,2",
			"name": "a(n) is the number of distinct lengths of runs in the binary representation of n.",
			"comment": [
				"Least k whose value is n: 1, 4, 35, 536, 16775, 1060976, ..., = A165933. - _Robert G. Wilson v_, Sep 30 2009"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A165413/b165413.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 1 for n in A140690. - _Robert G. Wilson v_, Sep 30 2009"
			],
			"example": [
				"92 in binary is 1011100. There is a run of one 1, followed by a run of one 0, then a run of three 1's, then finally a run of two 0's. The run lengths are therefore (1,1,3,2). The distinct values of these run lengths are (1,3,2). Since there are 3 distinct values, then a(92) = 3."
			],
			"mathematica": [
				"f[n_] := Length@ Union@ Map[ Length, Split@ IntegerDigits[n, 2]]; Array[f, 105] (* _Robert G. Wilson v_, Sep 30 2009 *)"
			],
			"program": [
				"(Haskell)",
				"import Data.List (group, nub)",
				"a165413 = length . nub . map length . group . a030308_row",
				"-- _Reinhard Zumkeller_, Mar 02 2013",
				"(PARI)",
				"binruns(n) = {",
				"  if (n == 0, return([1, 0]));",
				"  my(bag = List(), v=0);",
				"  while(n != 0,",
				"        v = valuation(n,2); listput(bag, v); n \u003e\u003e= v; n++;",
				"        v = valuation(n,2); listput(bag, v); n \u003e\u003e= v; n--);",
				"  return(Vec(bag));",
				"};",
				"a(n) = #Set(select(k-\u003ek, binruns(n)));",
				"vector(105, i, a(i))  \\\\ _Gheorghe Coserea_, Sep 17 2015",
				"(Python)",
				"from itertools import groupby",
				"def a(n): return len(set([len(list(g)) for k, g in groupby(bin(n)[2:])]))",
				"print([a(n) for n in range(1, 106)]) # _Michael S. Branicky_, Jan 04 2021"
			],
			"xref": [
				"Cf. A140690 (locations of 1's), A165933 (locations of new highs).",
				"Cf. A005811, A165414.",
				"Cf. A030308, A044813."
			],
			"keyword": "base,nonn",
			"offset": "1,4",
			"author": "_Leroy Quet_, Sep 17 2009",
			"ext": [
				"More terms from _Robert G. Wilson v_, Sep 30 2009"
			],
			"references": 5,
			"revision": 30,
			"time": "2021-01-04T17:03:55-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}