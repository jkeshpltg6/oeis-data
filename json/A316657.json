{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A316657",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 316657,
			"data": "0,1,0,-1,0,2,3,2,1,2,-1,0,-1,-2,-1,-2,-1,-2,-3,-2,1,2,1,0,1,3,4,3,2,3,5,6,5,4,5,2,3,2,1,2,1,2,1,0,1,4,5,4,3,4,-4,-3,-4,-5,-4,-2,-1,-2,-3,-2,-5,-4,-5,-6,-5,-6,-5,-6,-7,-6,-3,-2,-3,-4,-3",
			"name": "For any n \u003e= 0 with base-5 expansion Sum_{k=0..w} d_k * 5^k, let f(n) = Sum_{k=0..w} [d_k \u003e 0] * (2 + i)^k * i^(d_k - 1) (where [] is an Iverson bracket and i denotes the imaginary unit); a(n) equals the real part of f(n).",
			"comment": [
				"See A316658 for the imaginary part of f.",
				"See A316707 for the square of the modulus of f.",
				"The function f has nice fractal features (see scatterplot in Links section).",
				"It appears that f defines a bijection from the nonnegative integers to the Gaussian integers."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A316657/b316657.txt\"\u003eTable of n, a(n) for n = 0..15624\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A316657/a316657.png\"\u003eColored scatterplot of (a(n), A316658(n)) for n=0..5^8-1\u003c/a\u003e (where the hue is function of n)",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Gaussian_integer\"\u003eGaussian integer\u003c/a\u003e"
			],
			"formula": [
				"a(5^n) = A139011(n) for any n \u003e= 0.",
				"a(3 * 5^n) = -A139011(n) for any n \u003e= 0."
			],
			"mathematica": [
				"a[n_] := Module[{d, z}, d = IntegerDigits[n, 5] // Reverse; z = Sum[ If[d[[i]]\u003e0, (2+I)^(i-1)*I^(d[[i]]-1), 0], {i, 1, Length[d]}]; Re[z]];",
				"Table[a[n], {n, 0, 100}] (* _Jean-François Alcover_, Nov 06 2021, after PARI code *)"
			],
			"program": [
				"(PARI) a(n) = my (d=Vecrev(digits(n, 5)), z=sum(i=1, #d, if (d[i], (2+I)^(i-1) * I^(d[i]-1), 0))); real(z)"
			],
			"xref": [
				"Cf. A139011, A316658, A316707."
			],
			"keyword": "sign,base",
			"offset": "0,6",
			"author": "_Rémy Sigrist_, Jul 09 2018",
			"references": 11,
			"revision": 19,
			"time": "2021-11-06T18:09:27-04:00",
			"created": "2018-07-11T06:38:34-04:00"
		}
	]
}