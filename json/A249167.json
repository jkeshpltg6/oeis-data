{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A249167",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 249167,
			"data": "1,2,3,8,15,4,5,12,10,21,18,7,6,28,22,20,11,24,55,14,33,26,27,13,9,39,36,30,44,32,52,16,40,48,34,57,17,19,51,38,60,46,35,23,42,92,50,64,25,56,75,58,69,29,54,116,45,68,63,76,70,100,62,84,31,66,124,74,93,37,78,148,65,72,80",
			"name": "a(n) = n if n \u003c= 3, otherwise the smallest number not occurring earlier having at least one common Fermi-Dirac factor with a(n-2), but none with a(n-1).",
			"comment": [
				"Fermi-Dirac analog of A098550. Recall that every positive digit has a unique Fermi-Dirac representation as a product of distinct terms of A050376.",
				"Conjecture: the sequence is a permutation of the positive integers.",
				"Conjecture is true. The proof is similar to that for A098550 with minor changes. - _Vladimir Shevelev_, Jan 26 2015",
				"It is interesting that while the first 10000 points (n, A098550(n)) lie on about 8 roughly straight lines, the first 10000 points (n,a(n)) here lie on only about 6 lines (cf. scatterplots of these sequences). - _Vladimir Shevelev_, Jan 26 2015"
			],
			"link": [
				"Peter J. C. Moses, \u003ca href=\"/A249167/b249167.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"David L. Applegate, Hans Havermann, Bob Selcoe, Vladimir Shevelev, N. J. A. Sloane, and Reinhard Zumkeller, \u003ca href=\"http://arxiv.org/abs/1501.01669\"\u003eThe Yellowstone Permutation\u003c/a\u003e, arXiv preprint arXiv:1501.01669, 2015 and \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL18/Sloane/sloane9.html\"\u003eJ. Int. Seq. 18 (2015) 15.6.7\u003c/a\u003e..",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"example": [
				"a(4) is not 4, since 2 and 4 have no common Fermi-Dirac divisor; it is not 6, since a(3)=3 and 6 have the common divisor 3. So, a(4)=8, having the Fermi-Dirac representation 8=2*4."
			],
			"program": [
				"(Haskell)",
				"import Data.List (delete, intersect)",
				"a249167 n = a249167_list !! (n-1)",
				"a249167_list = 1 : 2 : 3 : f 2 3 [4..] where",
				"   f u v ws = g ws where",
				"     g (x:xs) | null (intersect fdx $ a213925_row u) ||",
				"                not (null $ intersect fdx $ a213925_row v) = g xs",
				"              | otherwise =  x : f v x (delete x ws)",
				"              where fdx = a213925_row x",
				"-- _Reinhard Zumkeller_, Mar 11 2015"
			],
			"xref": [
				"Cf. A098550, A050376.",
				"Cf. A213925, A255940 (inverse)."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Vladimir Shevelev_, Dec 15 2014",
			"ext": [
				"More terms from _Peter J. C. Moses_, Dec 15 2014"
			],
			"references": 6,
			"revision": 40,
			"time": "2018-05-15T16:39:42-04:00",
			"created": "2014-12-16T13:27:58-05:00"
		}
	]
}