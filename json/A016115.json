{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A016115",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 16115,
			"data": "4,1,15,0,93,0,668,0,5172,0,42042,0,353701,0,3036643,0,27045226,0,239093865,0,2158090933,0,19742800564,0",
			"name": "Number of prime palindromes with n digits.",
			"comment": [
				"Every palindrome with an even number of digits is divisible by 11 and therefore is composite (not prime). Hence there is only one palindromic prime with an even number of digits, namely 11 itself. - _Martin Renner_, Apr 15 2006"
			],
			"link": [
				"K. S. Brown, \u003ca href=\"http://www.mathpages.com/home/kmath359.htm\"\u003eOn General Palindromic Numbers\u003c/a\u003e",
				"Patrick De Geest, \u003ca href=\"http://www.worldofnumbers.com/palpri.htm\"\u003eWorld!Of Palindromic Primes\u003c/a\u003e",
				"Shyam Sunder Gupta, \u003ca href=\"http://listserv.nodak.edu/cgi-bin/wa.exe?A1=ind0602\u0026amp;L=nmbrthry\"\u003ePalindromic Primes up to 10^19\u003c/a\u003e.",
				"Shyam Sunder Gupta, \u003ca href=\"https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;b79493a6.1310\"\u003ePalindromic Primes up to 10^23\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PalindromicPrime.html\"\u003ePalindromic Prime.\u003c/a\u003e"
			],
			"formula": [
				"a(2n) = 0 for n \u003e 1. - _Chai Wah Wu_, Nov 21 2021"
			],
			"maple": [
				"# A016115 Gets numbers of base-10 palindromic primes with exactly d digits, 1 \u003c= d \u003c= 13 (say), in the list \"lis\"",
				"lis:=[4,1];",
				"for d from 3 to 13 do",
				"if d::even then",
				"    lis:=[op(lis),0];",
				"else",
				"    m:= (d-1)/2:",
				"    Res2 := [seq(seq(n*10^(m+1)+y*10^m+digrev(n), y=0..9), n=10^(m-1)..10^m-1)]:",
				"    ct:=0; for x in Res2 do if isprime(x) then ct:=ct+1; fi: od:",
				"    lis:=[op(lis),ct];",
				"fi:",
				"lprint(d,lis);",
				"od:",
				"lis; # _N. J. A. Sloane_, Oct 18 2015"
			],
			"mathematica": [
				"A016115[n_] := Module[{i}, If[EvenQ[n] \u0026\u0026 n \u003e 2, Return[0]]; Return[Length[Select[Range[10^(n - 1), 10^n - 1], # == IntegerReverse[#] \u0026\u0026 PrimeQ[#] \u0026]]]];",
				"Table[A016115[n], {n, 6}] (* _Robert Price_, May 25 2019 *)",
				"(* -OR-  A less straightforward implementation, but more efficient in that the palindromes are constructed instead of testing every number in the range. *)",
				"A016115[n_] := Module[{c, f, t0, t1},",
				"   If[n == 2, Return[1]];",
				"   If[EvenQ[n], Return[0]];",
				"   c = 0; t0 = 10^((n - 1)/2); t1 = t0*10;",
				"   For[f = t0, f \u003c t1, f++,",
				"    If[n != 1 \u0026\u0026 MemberQ[{2,4,5,6,8}, Floor[f/t0]], f = f + t0 - 1; Continue[]];",
				"    If[PrimeQ[f*t0 + IntegerReverse[Floor[f/10]]], c++]]; Return[c]];",
				"Table[A016115[n], {n, 1, 12}] (* _Robert Price_, May 25 2019 *)"
			],
			"program": [
				"(Python)",
				"from sympy import isprime",
				"from itertools import product",
				"def pals(d, base=10): # all d-digit palindromes",
				"    digits = \"\".join(str(i) for i in range(base))",
				"    for p in product(digits, repeat=d//2):",
				"        if d \u003e 1 and p[0] == \"0\": continue",
				"        left = \"\".join(p); right = left[::-1]",
				"        for mid in [[\"\"], digits][d%2]: yield int(left + mid + right)",
				"def a(n): return int(n==2) if n%2 == 0 else sum(isprime(p) for p in pals(n))",
				"print([a(n) for n in range(1, 13)]) # _Michael S. Branicky_, Jun 23 2021"
			],
			"xref": [
				"Cf. A002113 (palindromes), A002385 (palindromic primes), A040025 (bisection), A050251 (partial sums)."
			],
			"keyword": "nonn,hard,base,more",
			"offset": "1,1",
			"author": "_Robert G. Wilson v_",
			"ext": [
				"Corrected and extended by _Patrick De Geest_, Jun 15 1998",
				"a(17) = 27045226 was found in collaboration with Martin Eibl (M.EIBL(AT)LINK-R.de), _Carlos Rivera_, _Warut Roonguthai_",
				"a(19) from _Shyam Sunder Gupta_, Feb 12 2006",
				"a(21)-a(22) from _Shyam Sunder Gupta_, Mar 13 2009",
				"a(23)-a(24) from _Shyam Sunder Gupta_, Oct 05 2013"
			],
			"references": 6,
			"revision": 52,
			"time": "2021-11-21T18:31:14-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}