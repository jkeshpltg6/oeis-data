{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A143524",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 143524,
			"data": "3,1,5,7,1,8,4,5,2,0,5,3,8,9,0,0,7,6,8,5,1,0,8,5,2,5,1,4,7,3,7,0,6,5,7,1,9,9,0,5,9,2,6,8,7,6,7,8,7,2,4,3,9,2,6,1,3,7,0,3,0,2,0,9,5,9,9,4,3,2,1,5,8,8,0,2,9,6,4,6,1,2,2,2,8,0,4,4,3,1,8,5,7,5,0,0,0,9,8,4,6,3,0,1",
			"name": "Decimal expansion of the (negated) constant in the expansion of the prime zeta function about s = 1.",
			"comment": [
				"This constant appears in Franz Mertens's publication from 1874 on p. 58 (see link). - _Artur Jasinski_, Mar 17 2021"
			],
			"reference": [
				"Henri Cohen, Number Theory, Volume II: Analytic and Modern Tools, GTM Vol. 240, Springer, 2007; see pp. 208-209."
			],
			"link": [
				"Henri Cohen, \u003ca href=\"http://www.math.u-bordeaux.fr/~cohen/hardylw.dvi\"\u003eHigh precision computation of Hardy-Littlewood constants\u003c/a\u003e, preprint, 1998.",
				"Henri Cohen, \u003ca href=\"/A221712/a221712.pdf\"\u003eHigh-precision computation of Hardy-Littlewood constants\u003c/a\u003e. [pdf copy, with permission]",
				"Carl-Erik Fröberg, \u003ca href=\"https://doi.org/10.1007/BF01933420\"\u003eOn the prime zeta function\u003c/a\u003e, BIT Numerical Mathematics, Vol. 8, No. 3 (1968), pp. 187-202.",
				"R. J. Mathar, \u003ca href=\"http://arxiv.org/abs/0803.0900\"\u003eSeries of reciprocal powers of k-almost primes\u003c/a\u003e, arXiv:0803.0900 [math.NT], 2008-2009, Table 2.",
				"Franz Mertens, \u003ca href=\"http://gdz.sub.uni-goettingen.de/dms/resolveppn/?PPN=GDZPPN002155656\"\u003eEin Beitrag zur analytischen Zahlentheorie\u003c/a\u003e, J. Reine Angew. Math. 78 (1874), pp. 46-62 p. 58.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PrimeZetaFunction.html\"\u003ePrime Zeta Function\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Prime_zeta_function\"\u003ePrime zeta function\u003c/a\u003e."
			],
			"formula": [
				"Equals A077761 minus A001620. - _R. J. Mathar_, Jan 22 2009",
				"From _Amiram Eldar_, Aug 08 2020: (Start)",
				"Equals -Sum{k\u003e=2} mu(k) * log(zeta(k)) / k.",
				"Equals -Sum_{p prime} (1/p + log(1 - 1/p))",
				"Equals Sum_{k\u003e=2} P(k)/k, where P is the prime zeta function. (End)"
			],
			"example": [
				"-0.315718452053890076851... [corrected by _Georg Fischer_, Jul 29 2021]"
			],
			"mathematica": [
				"digits = 104; S = NSum[PrimeZetaP[n]/n, {n, 2, Infinity}, WorkingPrecision -\u003e digits + 10, NSumTerms -\u003e 3*digits]; RealDigits[S, 10, digits] // First (* _Jean-François Alcover_, Sep 11 2015 *)"
			],
			"xref": [
				"Cf. A001620, A077761."
			],
			"keyword": "nonn,cons",
			"offset": "0,1",
			"author": "_Eric W. Weisstein_, Aug 22 2008",
			"ext": [
				"Digits changed to agree with A077761 and A001620 by _R. J. Mathar_, Oct 30 2009",
				"Last digits corrected by _Jean-François Alcover_, Sep 11 2015"
			],
			"references": 2,
			"revision": 34,
			"time": "2021-07-29T11:44:09-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}