{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A111800",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 111800,
			"data": "1,3,5,5,7,7,7,7,7,9,9,9,9,9,11,7,9,9,9,11,11,11,9,11,9,11,9,11,11,13,11,9,13,11,13,11,11,11,13,13,11,13,11,13,13,11,13,11,9,11,13,13,9,11,15,13,13,13,11,15,11,13,13,9,15,15,11,13,13,15,13,13,13,13,13,13,15,15",
			"name": "Order of the rote (rooted odd tree with only exponent symmetries) for n.",
			"comment": [
				"A061396(n) gives the number of times that 2n+1 appears in this sequence."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A111800/b111800.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"J. Awbrey, \u003ca href=\"/A061396/a061396a.txt\"\u003eIllustrations of Rotes for Small Integers\u003c/a\u003e",
				"J. Awbrey, \u003ca href=\"https://oeis.org/wiki/Riffs_and_Rotes\"\u003eRiffs and Rotes\u003c/a\u003e"
			],
			"formula": [
				"a(Prod(p_i^e_i)) = 1 + Sum(a(i) + a(e_i)), product over nonzero e_i in prime factorization of n."
			],
			"example": [
				"Writing prime(i)^j as i:j and using equal signs between identified nodes:",
				"2500 = 4 * 625 = 2^2 5^4 = 1:2 3:4 has the following rote:",
				"` ` ` ` ` ` ` `",
				"` ` ` o-o ` o-o",
				"` ` ` | ` ` | `",
				"` o-o o-o o-o `",
				"` | ` | ` | ` `",
				"o-o ` o---o ` `",
				"| ` ` | ` ` ` `",
				"O=====O ` ` ` `",
				"` ` ` ` ` ` ` `",
				"So a(2500) = a(1:2 3:4) = a(1)+a(2)+a(3)+a(4)+1 = 1+3+5+5+1 = 15."
			],
			"maple": [
				"with(numtheory):",
				"a:= proc(n) option remember;",
				"      1+add(a(pi(i[1]))+a(i[2]), i=ifactors(n)[2])",
				"    end:",
				"seq(a(n), n=1..100);  # _Alois P. Heinz_, Feb 25 2015"
			],
			"mathematica": [
				"a[1] = 1; a[n_] := a[n] = 1+Sum[a[PrimePi[i[[1]] ] ] + a[i[[2]] ], {i, FactorInteger[n]}]; Table[a[n], {n, 1, 100}] (* _Jean-François Alcover_, Nov 11 2015, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A061396, A062504, A062537, A062860, A106177, A109300, A109301."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "Jon Awbrey, Aug 17 2005, based on calculations by _David W. Wilson_",
			"references": 19,
			"revision": 14,
			"time": "2015-11-11T03:45:22-05:00",
			"created": "2005-09-21T03:00:00-04:00"
		}
	]
}