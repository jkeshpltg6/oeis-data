{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A244232",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 244232,
			"data": "0,1,1,2,3,1,2,2,3,4,4,5,6,4,1,2,2,3,4,2,3,3,4,5,5,6,7,5,6,6,7,8,8,9,10,8,5,6,6,7,8,6,1,2,2,3,4,2,3,3,4,5,5,6,7,5,2,3,3,4,5,3,4,4,5,6,6,7,8,6,7,7,8,9,9,10,11,9,6,7,7,8,9,7,8,8,9,10,10,11,12,10,11,11,12,13,13,14,15,13,10,11,11,12,13,11,6,7,7,8,9,7,8,8,9,10,10,11,12,10,7,8,8,9,10,8,9,9,10,11,11,12,1",
			"name": "Sum of \"digit values\" in Semigreedy Catalan Representation of n, A244159.",
			"comment": [
				"Note that a(33604) = A000217(10) = 55 because the sum is computed from the underlying list (vector) of numbers, and thus is not subject to any corruption by decimal representation as A244159 itself is.",
				"Equivalent description: partition n \"greedily\" as terms of A197433, i.e. n = A197433(i) + A197433(j) + ... + A197433(k), always using the largest term of A197433 that still \"fits in\" (i.e. is \u003c= n remaining). Then a(n) = A000120(i) + A000120(j) + ... + A000120(k)."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A244232/b244232.txt\"\u003eTable of n, a(n) for n = 0..16796\u003c/a\u003e"
			],
			"formula": [
				"If A176137(n) = 1, a(n) = A000120(A244230(n)), otherwise a(n) = A000120(A244230(n)-1) + a(n-A197433(A244230(n)-1)).",
				"For all n, a(A000108(n)) = 1. [And moreover, Catalan numbers, A000108, give all such k that a(k) = 1].",
				"For all n, a(A014138(n)) = n and a(A014143(n)) = A000217(n+1)."
			],
			"example": [
				"For n=18, using the alternative description, we see that it is partitioned  into the terms of A197433 as a greedy sum A197433(11) + A197433(1) = 17 + 1. Thus a(18) = A000120(11) + A000120(1) = 3+1 = 4.",
				"For n=128, we see that is likewise represented as A197433(31) + A197433(31) = 64 + 64. Thus a(128) = 2*A000120(31) = 10."
			],
			"program": [
				"(Scheme, two alternative implementations)",
				";; One based on recurrence:",
				"(definec (A244232 n) (if (not (zero? (A176137 n))) (A000120 (A244230 n)) (+ (A000120 (-1+ (A244230 n))) (A244232 (- n (A197433 (-1+ (A244230 n))))))))",
				";; Another using function A244159raw given in A244159:",
				"(define (A244232 n) (apply + (vector-\u003elist (A244159raw n))))"
			],
			"xref": [
				"Cf. A000108, A000120, A176137, A197433, A244230, A244159, A244231, A244233, A244234, A014420, A236855."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Antti Karttunen_, Jun 25 2014",
			"references": 7,
			"revision": 18,
			"time": "2014-07-05T14:10:29-04:00",
			"created": "2014-07-05T14:10:29-04:00"
		}
	]
}