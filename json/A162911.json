{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A162911",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 162911,
			"data": "1,1,2,2,3,1,3,3,5,1,4,3,4,2,5,5,8,2,7,4,5,3,7,4,7,1,5,5,7,3,8,8,13,3,11,7,9,5,12,5,9,1,6,7,10,4,11,7,11,3,10,5,6,4,9,7,12,2,9,8,11,5,13,13,21,5,18,11,14,8,19,9,16,2,11,12,17,7,19,9,14,4,13,6,7,5,11,10,17,3,13",
			"name": "Numerators of drib tree fractions, where drib is the bit-reversal permutation tree of the Bird tree.",
			"comment": [
				"The drib tree is an infinite binary tree labeled with rational numbers. It is generated by the following iterative process: start with the rational 1; for the left subtree increment and then reciprocalize the current rational; for the right subtree interchange the order of the two steps: the rational is first reciprocalized and then incremented. Like the Stern-Brocot and the Bird tree, the drib tree enumerates all the positive rationals (A162911(n)/A162912(n)).",
				"From _Yosu Yurramendi_, Jul 11 2014: (Start)",
				"If the terms (n\u003e0) are written as an array (left-aligned fashion) with rows of length 2^m, m = 0,1,2,3,...",
				"1,",
				"1, 2,",
				"2, 3,1, 3,",
				"3, 5,1, 4, 3, 4,2, 5,",
				"5, 8,2, 7, 4, 5,3, 7,4, 7,1, 5, 5, 7,3, 8,",
				"8,13,3,11, 7, 9,5,12,5, 9,1, 6, 7,10,4,11,7,11,3,10,5,6,4, 9, 7,12,2, 9, 8,11,5,13,",
				"...",
				"then the sum of the m-th row is 3^m (m = 0,1,2,), each column k is a Fibonacci-type sequence.",
				"If the rows are written in a right-aligned fashion:",
				"                                                                                  1",
				"                                                                               1, 2",
				"                                                                          2, 3,1, 3",
				"                                                               3, 5,1, 4, 3, 4,2, 5",
				"                                          5, 8,2, 7,4, 5,3, 7, 4, 7,1, 5, 5, 7,3, 8",
				"                                                                                ...",
				"then each column k also is a Fibonacci-type sequence.",
				"If the sequence is considered by blocks of length 2^m, m = 0,1,2,..., the blocks of this sequence are the reverses of blocks of A162912 (a(2^m+k) = A162912(2^(m+1)-1-k), m = 0,1,2,..., k = 0..2^m-1).",
				"(End)",
				"From _Yosu Yurramendi_, Jan 12 2017: (Start)",
				"a(2^(m+2m'  )     + A020988(m'))   = A000045(m+1), m\u003e=0, m'\u003e=0",
				"a(2^(m+2m'+1)     + A020989(m'))   = A000045(m+3), m\u003e=0, m'\u003e=0",
				"a(2^(m+2m'  ) - 1 - A002450(m'))   = A000045(m+1), m\u003e=0, m'\u003e=0",
				"a(2^(m+2m'+1) - 1 - A072197(m'-1)) = A000045(m+3), m\u003e=0, m'\u003e0",
				"a(2^(m+1) -1) = A000045(m+2), m\u003e=0. (End)"
			],
			"link": [
				"R. Hinze, \u003ca href=\"http://www.cs.ox.ac.uk/ralf.hinze/publications/Bird.pdf\"\u003eFunctional pearls: the bird tree\u003c/a\u003e, J. Funct. Programming 19 (2009), no. 5, 491-508.",
				"\u003ca href=\"/index/Fo#fraction_trees\"\u003eIndex entries for fraction trees\u003c/a\u003e"
			],
			"formula": [
				"a(n) where a(1) = 1; a(2n) = b(n); a(2n+1) = a(n) + b(n); and b(1) = 1; b(2n) = a(n) + b(n); b(2n+1) = a(n).",
				"a(2^(m+1)+2*k) = a(2^(m+1)-k-1), a(2^(m+1)+2*k+1) = a(2^(m+1)-k-1) + a(2^m+k), a(1) = 1, m\u003e=0, k=0..2^m-1. - _Yosu Yurramendi_, Jul 11 2014",
				"a(2^(m+1) + 2*k) = A162912(2^m + k), m \u003e= 0, 0 \u003c= k \u003c 2^m.",
				"a(2^(m+1) + 2*k + 1) = a(2^m + k) + A162912(2^m + k), m \u003e= 0, 0 \u003c= k \u003c 2^m. - _Yosu Yurramendi_, Mar 30 2016",
				"a(n*2^m + A176965(m)) = A268087(n), n \u003e 0, m \u003e 0. - _Yosu Yurramendi_, Feb 20 2017",
				"a(n) = A002487(A258996(n)), n \u003e 0. - _Yosu Yurramendi_, Jun 23 2021"
			],
			"example": [
				"The first four levels of the drib tree: [1/1], [1/2, 2/1], [2/3, 3/1, 1/3, 3/2], [3/5, 5/2, 1/4, 4/3, 3/4, 4/1, 2/5, 5/3]."
			],
			"program": [
				"(Haskell) import Ratio; drib :: [Rational]; drib = 1 : map (recip . succ) drib \\/ map (succ . recip) drib; (a : as) \\/ bs = a : (bs \\/ as); a162911 = map numerator drib; a162912 = map denominator drib",
				"(R) blocklevel \u003c- 6 # arbitrary",
				"a \u003c- 1",
				"for(m in 0:blocklevel) for(k in 0:(2^m-1)){",
				"  a[2^(m+1)+2*k  ] \u003c- a[2^(m+1)-1-k]",
				"  a[2^(m+1)+2*k+1] \u003c- a[2^(m+1)-1-k] + a[2^m+k]",
				"}",
				"a",
				"# _Yosu Yurramendi_, Jul 11 2014"
			],
			"xref": [
				"This sequence is the composition of A162909 and A059893: a(n) = A162909(A059893(n)). This sequence is a permutation of A002487(n+1)."
			],
			"keyword": "easy,frac,nonn",
			"offset": "1,3",
			"author": "Ralf Hinze (ralf.hinze(AT)comlab.ox.ac.uk), Aug 05 2009",
			"references": 13,
			"revision": 60,
			"time": "2021-07-11T03:21:08-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}