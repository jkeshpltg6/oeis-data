{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A118969",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 118969,
			"data": "1,2,11,80,665,5980,56637,556512,5620485,57985070,608462470,6474009360,69682358811,757366074080,8300675584120,91634565938880,1018002755977245,11372548404732930,127677890035721025,1439777493407492640",
			"name": "a(n) = 2*binomial(5*n+1,n)/(4*n+2).",
			"comment": [
				"A quadrisection of A118968.",
				"If y = x + 2*x^3 + x^5, the series reversion is x = y - 2*y^3 + 11*y^5 - 80*y^7 + 665*y^9 - ... - _R. J. Mathar_, Sep 29 2012"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A118969/b118969.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e",
				"Karol A. Penson and Karol Zyczkowski, \u003ca href=\"http://arxiv.org/abs/1103.3453/\"\u003eProduct of Ginibre matrices: Fuss-Catalan and Raney distribution\u003c/a\u003e, arXiv:1103.3453 [math-ph], 2011.",
				"Karol A. Penson and Karol Zyczkowski, \u003ca href=\"http://dx.doi.org/10.1103/PhysRevE.83.061118\"\u003eProduct of Ginibre matrices: Fuss-Catalan and Raney distribution\u003c/a\u003e, Phys. Rev. E 83, 061118 (2011)."
			],
			"formula": [
				"From _Gary W. Adamson_, Aug 11 2011: (Start)",
				"a(n) is sum of top row terms in M^n, where M is an infinite square production matrix with the tetrahedral series in each column (A000292), as follows:",
				"   1,  1,  0,  0,  0,  0, ...",
				"   4,  1,  1,  0,  0,  0, ...",
				"  10, 10,  4,  1,  0,  0, ...",
				"  20, 20, 10,  4,  1,  0, ...",
				"  35, 35, 20, 10,  4,  1, ...",
				"  ... (End)",
				"G.f.: hypergeom([1/5, 2/5, 3/5, 4/5],[1/2, 3/4, 5/4], 3125*x/256)^2. - _Mark van Hoeij_, Apr 19 2013",
				"a(n) = 2*binomial(5n+1,n-1)/n for n\u003e0, a(0)=1. - _Bruno Berselli_, Jan 19 2014",
				"8*n*(4*n+1)*(2*n+1)*(4*n-1)*a(n) - 5*(5*n+1)*(5*n-3)*(5*n-2)*(5*n-1)*a(n-1) = 0. - _R. J. Mathar_, Oct 10 2014",
				"G.f. A(x) satisfies: A(x) = 1 / (1 - x * A(x)^2)^2. - _Ilya Gutkovskiy_, Nov 13 2021"
			],
			"example": [
				"a(3) = 80 = sum of top row terms in M^n = (35 + 35 + 9 + 1)."
			],
			"mathematica": [
				"Table[2*Binomial[5n+1,n]/(4n+2),{n,0,20}] (* _Harvey P. Dale_, Aug 21 2011 *)"
			],
			"program": [
				"(MAGMA) [2*Binomial(5*n+1,n)/(4*n+2): n in [0..20]]; // _Vincenzo Librandi_, Aug 12 2011",
				"(PARI) a(n)=2*binomial(5*n+1,n)/(4*n+2); \\\\ _Joerg Arndt_, Apr 20 2013"
			],
			"xref": [
				"Cf. A000292, A118968."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Paul Barry_, May 07 2006",
			"references": 8,
			"revision": 48,
			"time": "2021-11-14T06:24:59-05:00",
			"created": "2006-05-19T03:00:00-04:00"
		}
	]
}