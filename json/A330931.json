{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A330931",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 330931,
			"data": "1,20,68,80,115,155,184,204,260,272,284,320,344,355,395,404,424,464,555,564,595,623,624,636,664,675,804,835,846,847,864,875,888,904,972,1028,1040,1075,1088,1124,1164,1182,1211,1224,1239,1266,1280,1304,1315,1424",
			"name": "Numbers k such that both k and k + 1 are Niven numbers in base 2 (A049445).",
			"comment": [
				"Cai proved that there are infinitely many runs of 4 consecutive Niven numbers in base 2. Therefore this sequence is infinite."
			],
			"reference": [
				"József Sándor and Borislav Crstici, Handbook of Number theory II, Kluwer Academic Publishers, 2004, Chapter 4, p. 382."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A330931/b330931.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Tianxin Cai, \u003ca href=\"https://www.fq.math.ca/Scanned/34-2/cai1.pdf\"\u003eOn 2-Niven numbers and 3-Niven numbers\u003c/a\u003e, Fibonacci Quarterly, Vol. 34, No. 2 (1996), pp. 118-120.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Harshad_number\"\u003eHarshad number\u003c/a\u003e.",
				"Brad Wilson \u003ca href=\"http://www.fq.math.ca/Scanned/35-2/wilson.pdf\"\u003eConstruction of 2n consecutive n-Niven numbers\u003c/a\u003e, Fibonacci Quarterly, Vol. 35, No. 2 (1997), pp. 122-128."
			],
			"example": [
				"20 is a term since 20 and 20 + 1 = 21 are both Niven numbers in base 2."
			],
			"mathematica": [
				"binNivenQ[n_] := Divisible[n, Total @ IntegerDigits[n, 2]]; bnq1 = binNivenQ[1]; seq = {}; Do[bnq2 = binNivenQ[k]; If[bnq1 \u0026\u0026 bnq2, AppendTo[seq, k - 1]]; bnq1 = bnq2, {k, 2, 10^4}]; seq"
			],
			"program": [
				"(MAGMA) f:=func\u003cn|n mod \u0026+Intseq(n,2) eq 0\u003e; a:=[]; for k in [1..1500] do  if forall{m:m in [0..1]|f(k+m)} then Append(~a,k); end if; end for; a; // _Marius A. Burtea_, Jan 03 2020",
				"(Python)",
				"def sbd(n): return sum(map(int, str(bin(n)[2:])))",
				"def niv2(n): return n%sbd(n) == 0",
				"def aupto(nn): return [k for k in range(1, nn+1) if niv2(k) and niv2(k+1)]",
				"print(aupto(1424)) # _Michael S. Branicky_, Jan 20 2021"
			],
			"xref": [
				"Cf. A049445, A328205, A328209, A328213, A330713, A330927, A330932, A330933."
			],
			"keyword": "nonn,base",
			"offset": "1,2",
			"author": "_Amiram Eldar_, Jan 03 2020",
			"references": 16,
			"revision": 17,
			"time": "2021-01-20T10:20:10-05:00",
			"created": "2020-01-03T20:16:37-05:00"
		}
	]
}