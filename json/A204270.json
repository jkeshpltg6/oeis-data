{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A204270",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 204270,
			"data": "1,4,10,36,58,280,338,1632,2955,9512,11482,83160,66922,323128,780100,2354160,2273378,16465260,13250218,95966568,154455860,372889432,450117362,4346717760,3935214363,12667263848,30581480180,110745336312,89120964298",
			"name": "a(n) = tau(n)*Pell(n), where tau(n) = A000005(n), the number of divisors of n.",
			"comment": [
				"Compare g.f. to the Lambert series identity: Sum_{n\u003e=1} x^n/(1-x^n) = Sum_{n\u003e=1} tau(n)*x^n.",
				"Related identities:",
				"(1) Sum_{n\u003e=1} n^k*Pell(n)*x^n/(1 - A002203(n)*x^n + (-1)^n*x^(2*n)) = Sum_{n\u003e=1} sigma_{k}(n)*Pell(n)*x^n for k\u003e=0.",
				"(2) Sum_{n\u003e=1} phi(n)*Pell(n)*x^n/(1 - A002203(n)*x^n + (-1)^n*x^(2*n)) = Sum_{n\u003e=1} n*Pell(n)*x^n.",
				"(3) Sum_{n\u003e=1} moebius(n)*Pell(n)*x^n/(1 - A002203(n)*x^n + (-1)^n*x^(2*n)) = x.",
				"(4) Sum_{n\u003e=1} lambda(n)*Pell(n)*x^n/(1 - A002203(n)*x^n + (-1)^n*x^(2*n)) = Sum_{n\u003e=1} Pell(n^2)*x^(n^2)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A204270/b204270.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e"
			],
			"formula": [
				"G.f.: Sum_{n\u003e=1} Pell(n)*x^n/(1 - A002203(n)*x^n + (-1)^n*x^(2*n)) = Sum_{n\u003e=1} tau(n)*Pell(n)*x^n, where Pell(n) = A000129(n) and A002203 is the companion Pell numbers."
			],
			"example": [
				"G.f.: A(x) = 1 + 4*x + 10*x^2 + 36*x^3 + 58*x^4 + 280*x^5 + 338*x^6 +...",
				"where A(x) = x/(1-2*x-x^2) + 2*x^2/(1-6*x^2+x^4) + 5*x^3/(1-14*x^3-x^6) + 12*x^4/(1-34*x^4+x^8) + 29*x^5/(1-82*x^5-x^10) + 70*x^6/(1-198*x^6+x^12) +...+ Pell(n)*x^n/(1 - A002203(n)*x^n + (-1)^n*x^(2*n)) +..."
			],
			"mathematica": [
				"Table[DivisorSigma[0, n] Fibonacci[n, 2], {n, 1, 50}] (* _G. C. Greubel_, Jan 05 2018 *)"
			],
			"program": [
				"(PARI) /* Subroutines used in PARI programs below: */",
				"{Pell(n)=polcoeff(x/(1-2*x-x^2+x*O(x^n)), n)}",
				"{A002203(n)=polcoeff(2*(1-x)/(1-2*x-x^2+x*O(x^n)), n)}",
				"(PARI) {a(n)=sigma(n,0)*Pell(n)}",
				"(PARI) {a(n)=polcoeff(sum(m=1,n,Pell(m)*x^m/(1-A002203(m)*x^m+(-1)^m*x^(2*m)+x*O(x^n))),n)}"
			],
			"xref": [
				"Cf. A203847, A204271, A204272, A204273, A204274, A204275, A000005 (tau), A002203, A000045."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Paul D. Hanna_, Jan 14 2012",
			"references": 23,
			"revision": 10,
			"time": "2018-01-07T04:14:00-05:00",
			"created": "2012-01-14T13:58:18-05:00"
		}
	]
}