{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A082652",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 82652,
			"data": "1,2,5,3,8,14,4,11,20,30,5,14,26,40,55,6,17,32,50,70,91,7,20,38,60,85,112,140,8,23,44,70,100,133,168,204,9,26,50,80,115,154,196,240,285,10,29,56,90,130,175,224,276,330,385,11,32,62,100,145,196,252,312,375,440,506",
			"name": "Triangle read by rows: T(n,k) is the number of squares that can be found in a k X n rectangular grid of little squares, for 1 \u003c= k \u003c= n.",
			"comment": [
				"T(n,k) also is the total number of balls in a pyramid of balls on an n X k rectangular base. - _N. J. A. Sloane_, Nov 17 2007. For example, if the base is 4 X 2, the total number of balls is 4*2 + 3*1 = 11 = T(4,2).",
				"  1",
				"  2   5",
				"  3   8  14",
				"  4  11  20  30",
				"  5  14  26  40  55",
				"  6  17  32  50  70  91",
				"  7  20  38  60  85 112 140",
				"Here the squares being counted have sides parallel to the gridlines; for all squares, see A130684.",
				"Row sums give A001296. - _Vincenzo Librandi_ Mar 26 2019"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A082652/b082652.txt\"\u003eTable of n, a(n) for n = 1..10011\u003c/a\u003e",
				"Antonio Bernini, Matteo Cervetti, Luca Ferrari, Einar Steingrimsson, \u003ca href=\"https://arxiv.org/abs/1910.00299\"\u003eEnumerative combinatorics of intervals in the Dyck pattern poset\u003c/a\u003e, arXiv:1910.00299 [math.CO], 2019. See p. 5."
			],
			"formula": [
				"T(n, k) = ( k + 3*k*n + 3*k^2*n - k^3 ) / 6.",
				"T(n, k) = Sum_{i=0..min(n,k)} (n-i)*(k-i). - _N. J. A. Sloane_, Nov 17 2007",
				"G.f.: (1+x*y-2*x^2*y)*x*y/((1-x*y)^4*(1-x)^2). - _Robert Israel_, Dec 20 2017"
			],
			"example": [
				"Let X represent a small square. Then T(3,2) = 8 because here",
				"XXX",
				"XXX",
				"we can see 8 squares, 6 of side 1, 2 of side 2."
			],
			"maple": [
				"f:=proc(m,n) add((m-i)*(n-i),i=0..min(m,n)); end;"
			],
			"mathematica": [
				"T[n_, k_] := Sum[(n-i)(k-i), {i, 0, Min[n, k]}];",
				"Table[T[n, k], {n, 1, 11}, {k, 1, n}] // Flatten (* _Jean-François Alcover_, Mar 25 2019 *)"
			],
			"program": [
				"(MAGMA) /* As triangle */ [[(k+3*k*n+3*k^2*n-k^3)/6: k in [1..n]]: n in [1.. 15]]; // _Vincenzo Librandi_, Mar 26 2019"
			],
			"xref": [
				"Cf. A083003, A083487. Right side of triangle gives A000330.",
				"Main diagonal is A000330, row sums are A001296. - _Paul D. Hanna_ and other correspondents, May 28 2003",
				"Cf. A130684. - _Joel B. Lewis_"
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "Artemario Tadeu Medeiros da Silva (artemario(AT)uol.com.br), May 16 2003",
			"ext": [
				"Edited by _Robert Israel_, Dec 20 2017"
			],
			"references": 6,
			"revision": 40,
			"time": "2021-09-11T19:08:18-04:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}