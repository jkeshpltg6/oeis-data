{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A238412",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 238412,
			"data": "2,9,9,36,36,64,64,144,144,144,240,240,240,576,625,240,900,625,960,960,576,900,2250,2304,900,3375,1536,960,3600,576,7776,2304,960,3840,8100,2250,2250,3600,9000,900,5760,1536,3840,13500,3375,3600,27216,6400,14400,3840,5760,7776,29160,9216,14000,9000",
			"name": "The multiplicative sum Zagreb index of the rooted tree with Matula number n (n \u003e= 2).",
			"comment": [
				"The multiplicative sum Zagreb index of a graph is defined as the product of d(u) + d(v) over all edges uv of G, where d(w) denotes the degree of the vertex w.",
				"The Matula-Goebel number of a rooted tree can be defined in the following recursive manner: to the one-vertex tree there corresponds the number 1; to a tree T with root degree 1 there corresponds the t-th prime number, where t is the Matula-Goebel number of the tree obtained from T by deleting the edge emanating from the root; to a tree T with root degree m\u003e=2 there corresponds the product of the Matula-Goebel numbers of the m branches of T."
			],
			"reference": [
				"E. Deutsch, Rooted tree statistics from Matula numbers, Discrete Applied Math., 160, 2012, 2314-2322.",
				"M. Eliasi, A. Iranmanesh and I. Gutman, Multiplicative versions of first Zagreb index, Comm. Math. Comp. Chem. (MATCH), 68, 2012, 217-230.",
				"F. Goebel, On a 1-1 correspondence between rooted trees and natural numbers, J. Combin. Theory, B 29 (1980), 141-143.",
				"I. Gutman and A. Ivic, On Matula numbers, Discrete Math., 150, 1996, 131-142.",
				"I. Gutman and Y-N. Yeh, Deducing properties of trees from their Matula numbers, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"D. W. Matula, A natural rooted tree enumeration by prime factorization, SIAM Review, 10, 1968, 273.",
				"K. Xu and K. Ch. Das, Trees, unicyclic, and bicyclic graphs extremal with respect to multiplicative sum Zagreb index, Comm. Math. Comp. Chem. (MATCH), 68, 2012, 257-272."
			],
			"link": [
				"E. Deutsch, \u003ca href=\"http://arxiv.org/abs/1111.4288\"\u003eRooted tree statistics from Matula numbers\u003c/a\u003e, arXiv1111.4288.",
				"\u003ca href=\"/index/Mat#matula\"\u003eIndex entries for sequences related to Matula-Goebel numbers\u003c/a\u003e"
			],
			"formula": [
				"There are recurrence relations that give the multiplicative sum Zagreb index of an \"elevated\" rooted tree (attach a new vertex to the root which becomes the root of the new tree) and of the merge of two rooted trees (identify the two roots). They make use of the sequence of the degrees of the level 1 vertices (denoted by DL in the Maple program)."
			],
			"example": [
				"a(5)=36; indeed the rooted tree with Matula number 5 is the path PQRS (rooted at P). The edges PQ and RS have endpoints of degrees 1 and 2 and the edge QR has endpoints of degrees 2 and 2; consequently, the contributions of these 3 edges to the multiplicative sum Zagreb index are 3, 3, 4; 3*3*4 = 36. a(987654321) = 92501790267801600000; the corresponding tree is the 29-vertex tree given in Fig. 2 of the Deutsch reference."
			],
			"maple": [
				"f := proc (x, y) options operator, arrow: x+y end proc; c := 2; with(numtheory): a := proc (n) local DL, r, s: DL := proc (n) if n = 2 then [1] elif bigomega(n) = 1 then [1+bigomega(pi(n))] else [op(DL(op(1, factorset(n)))), op(DL(n/op(1, factorset(n))))] end if end proc: r := proc (n) options operator, arrow: op(1, factorset(n)) end proc: s := proc (n) options operator, arrow; n/r(n) end proc: if n = 2 then c elif bigomega(n) = 1 then a(pi(n))*(2+bigomega(pi(n)))*(product(f(DL(pi(n))[j], 1+bigomega(pi(n)))/f(DL(pi(n))[j], bigomega(pi(n))), j = 1 .. bigomega(pi(n)))) else a(r(n))*a(s(n))*(product(f(DL(n)[j], bigomega(n)), j = 1 .. bigomega(n)))/((product(f(DL(r(n))[j], bigomega(r(n))), j = 1 .. bigomega(r(n))))*(product(f(DL(s(n))[j], bigomega(s(n))), j = 1 .. bigomega(s(n))))) end if end proc: seq(a(n), n = 2 .. 70);"
			],
			"keyword": "nonn",
			"offset": "2,1",
			"author": "_Emeric Deutsch_, Feb 28 2014",
			"references": 0,
			"revision": 10,
			"time": "2017-03-07T11:24:07-05:00",
			"created": "2014-03-01T16:03:39-05:00"
		}
	]
}