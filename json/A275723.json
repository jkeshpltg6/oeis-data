{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A275723",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 275723,
			"data": "1,1,2,1,2,3,1,2,2,4,1,2,3,4,5,1,2,2,4,5,6,1,2,3,4,3,4,7,1,2,2,4,2,6,7,8,1,2,3,4,2,4,7,8,9,1,2,2,4,2,6,7,8,4,10,1,2,3,4,5,4,7,8,9,10,11,1,2,2,4,5,6,7,8,4,6,11,12,1,2,3,4,3,4,5,8,9,4,11,8,13,1,2,2,4,2,6,5,8,4,4,11,12,13,14,1,2,3,4,2,4,3,8,9,4,11,8,13,14,15",
			"name": "Square array A(n,k) = prime factorization of n (= 1..) completely reduced by factorial base representation of k (= 0..), read by descending antidiagonals as A(1,0), A(1,1), A(2,0), A(1,2), A(2,1), A(3,0), etc. See the Comments section for the meaning of reduction in this context.",
			"comment": [
				"The reduction is done by scanning the factorial base representation of k [see A007623] from its most significant end [where the most significant digit occurs at the one-based position A084558(k)], by adding the exponent of prime(1+A084558(k)) of n to the current exponent (possibly zero) of prime(1+A084558(k)-A099563(k)) in the prime factorization of n, after which the exponent of prime(1+A084558(k)) is changed to zero. Thus the total number of prime factors of n [A001222(n)] never changes. This single step of reduction is performed with a bivariate function A273673. The reduction then proceeds to the next digit to the right, effectively skipping any zeros until all factorial base digits of k have been scanned through and the prime factorization of n has been changed accordingly. See the examples.",
				"This bivariate function is used to compute A275725."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A275723/b275723.txt\"\u003eTable of n, a(n) for n = 1..5050; the first 100 antidiagonals of array\u003c/a\u003e",
				"Indranil Ghosh, \u003ca href=\"/A275723/a275723.txt\"\u003ePython program for computing this sequence\u003c/a\u003e",
				"\u003ca href=\"/index/Fa#facbase\"\u003eIndex entries for sequences related to factorial base representation\u003c/a\u003e"
			],
			"formula": [
				"A(n,0) = n, and for k \u003e 0, A(n,k) = A(A273673(n,k), A257687(k)).",
				"Other identities. For all n \u003e= 1 and k \u003e= 0:",
				"A001222(A(n,k)) = A001222(n). [This reduction doesn't change the total number of prime factors of n.]"
			],
			"example": [
				"The top left 7 X 15 corner of the array:",
				"   1,  1,  1,  1,  1,  1,  1",
				"   2,  2,  2,  2,  2,  2,  2",
				"   3,  2,  3,  2,  3,  2,  3",
				"   4,  4,  4,  4,  4,  4,  4",
				"   5,  5,  3,  2,  2,  2,  5",
				"   6,  4,  6,  4,  6,  4,  6",
				"   7,  7,  7,  7,  7,  7,  5",
				"   8,  8,  8,  8,  8,  8,  8",
				"   9,  4,  9,  4,  9,  4,  9",
				"  10, 10,  6,  4,  4,  4, 10",
				"  11, 11, 11, 11, 11, 11, 11",
				"  12,  8, 12,  8, 12,  8, 12",
				"  13, 13, 13, 13, 13, 13, 13",
				"  14, 14, 14, 14, 14, 14, 10",
				"  15, 10,  9,  4,  6,  4, 15",
				"For row 15 (above), we have 15 = 3*5 = prime(2)*prime(3) and the terms for columns 0 - 6 (in factorial base: 0, 1, 10, 11, 20, 21, 100, see A007623) are computed as:",
				"When k=0, we do nothing and n stays as n (thus column 0 of array is A000027).",
				"When k=1 (with the length 1), we transfer the exponent of prime(2) to prime(1), to get prime(1)*prime(3) = 2*5 = 10.",
				"When k=2, in factorial base \"10\", with the length 2, we transfer (add) the exponent of prime(3) to prime(2), to get prime(2)*prime(2) = 9.",
				"When k=3, in factorial base \"11\", we first do as above, to get 9 = prime(2)^2, and for the least significant one, we transfer (add) the exponent of prime(2) to prime(1), to get prime(1)*prime(1) = 4.",
				"When k=4, in factorial base \"20\", with the length 2, we transfer (add) the exponent of prime(3) to prime(1), to get prime(2)*prime(1) = 6.",
				"When k=5, in factorial base \"21\", we first do as above, to get 6 = prime(2)*prime(1), and for the remaining \"1\" in factorial base representation of k, we transfer (add) the exponent of prime(2) to prime(1), to get prime(1)*prime(1) = 4.",
				"When k=6, in factorial base \"100\", with the length 3, we transfer (add) the exponent of prime(4) to prime(3), but prime(4) = 7 whose exponent is zero in 15, thus the result is also 15."
			],
			"program": [
				"(Scheme)",
				"(define (A275723 n) (A275723bi (A002260 n) (- (A004736 n) 1)))",
				"(define (A275723bi n fex) (let loop ((n n) (fex fex)) (cond ((zero? fex) n) (else (loop (A273673bi n fex) (A257687 fex))))))",
				";; Code for A273673bi given in A273673."
			],
			"xref": [
				"Transpose: A275724.",
				"Column 0: A000027.",
				"Cf. A001222, A002260, A004736, A007623, A257687, A273673, A275725."
			],
			"keyword": "nonn,base,tabl",
			"offset": "1,3",
			"author": "_Antti Karttunen_, Aug 09 2016",
			"references": 4,
			"revision": 35,
			"time": "2017-06-24T05:37:49-04:00",
			"created": "2016-08-10T12:00:20-04:00"
		}
	]
}