{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A294660",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 294660,
			"data": "0,1,2,3,4,5,6,7,9,8,10,15,12,16,20,11,22,13,18,14,28,19,17,21,23,26,29,24,30,25,33,58,27,34,47,38,45,31,48,41,50,37,52,44,65,40,57,76,32,63,35,60,39,62,36,88,46,67,51,183,75,43,55,42,53,56,70,61,64,85,59,77,69,73,78,89",
			"name": "Least nonnegative integer not occurring earlier whose square has no digit in common with the square of the previous term, a(0) = 0.",
			"comment": [
				"This is not a permutation of the nonnegative integers, since numbers whose square has all digits '1' through '9' (cf. A294661, e.g., 11826 with 11826^2 = 139854276) can never appear - and these numbers have asymptotic density 1.",
				"Will all integers whose square does not have all of the digits 1-9, eventually appear? Or might the sequence be finite? Since a(n)^2 has no digits in common with a(n-1)^2, it is sufficient for a(n+1) to exist, to find a number whose square has a subset of the digits of a(n-1)^2. Is this always possible? This problem sometimes has only \"sporadic k-digital solutions\", see, e.g., A058430, A030175, ... and the link to De Geest's page."
			],
			"link": [
				"M. F. Hasler, \u003ca href=\"/A294660/b294660.txt\"\u003eTable of n, a(n) for n = 0..4829\u003c/a\u003e",
				"P. De Geest, \u003ca href=\"http://www.worldofnumbers.com/threedigits.htm\"\u003eSquares containing at most three distinct digits, Index entries for related sequences\u003c/a\u003e"
			],
			"example": [
				"Since a(7)^2 = 7^2 = 49, the subsequent term cannot be 8, since 8^2 = 64 has the digit 4 in common with 49. Therefore, a(8) = 9, with 9^2 = 81 having no common digit with 49.",
				"a(1201) = 1037. So the square of the next term must not have any of the digits in {0, 1, 3, 5, 6, 7, 9}, only 2, 4, 8 are allowed. The least such number that has not been used before is a(1202) = 210912978, with a(1202)^2 = 210912978^2 = 44484284288828484. - _Alois P. Heinz_, Nov 09 2017"
			],
			"program": [
				"(PARI) {u=a=0; for(n=0, 99, print1(a\", \"); u+=1\u003c\u003ca; D=Set(if(a, digits(a^2))); for(k=1, oo, bittest(u, k)\u0026\u0026next; #setintersect(D, Set(digits(k^2)))\u0026\u0026next; a=k; break)); a}",
				"(PARI) {u=[a=0]; for(n=0, 99, print1(a\", \"); D=Set(if(a, digits(a^2))); for(k=u[1]+1, oo, setsearch(u, k)\u0026\u0026next; #setintersect(D, Set(digits(k^2)))\u0026\u0026next; u=setunion(u,[a=k]); break); while(#u\u003e1\u0026\u0026u[2]==u[1]+1,u=u[^1])); a}"
			],
			"xref": [
				"Cf. A030287 (strictly increasing), A067581 (do not take squares)."
			],
			"keyword": "nonn,base,look",
			"offset": "0,3",
			"author": "_M. F. Hasler_, Nov 08 2017",
			"references": 2,
			"revision": 22,
			"time": "2019-10-25T19:54:11-04:00",
			"created": "2017-11-10T15:31:36-05:00"
		}
	]
}