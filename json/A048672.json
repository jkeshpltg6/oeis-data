{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A048672",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 48672,
			"data": "0,1,2,4,3,8,5,16,32,9,6,64,128,10,17,256,33,512,7,1024,18,65,12,2048,129,34,4096,11,8192,257,16384,66,32768,20,130,513,65536,131072,1025,36,19,262144,258,13,524288,1048576,2049,24,35,2097152,4097,4194304,68",
			"name": "Binary encoding of squarefree numbers (A005117): A048640(n)/2.",
			"comment": [
				"Permutation of nonnegative integers. Note the indexing, the domain starts from 1, although the range includes also 0.",
				"A246353 gives the inverse of this sequence, in a sense that a(A246353(n)) = n for all n \u003e= 0, and A246353(a(n)) = n for all n \u003e= 1. When one is subtracted from the latter, another permutation of nonnegative integers is obtained: A064273. - _Antti Karttunen_, Aug 23 2014 based on comment from _Howard A. Landman_, Sep 25 2001",
				"Also index of n-th term of A019565 when its terms are sorted in increasing order. For example: a(6) = 8. The smallest values of A019565 are 1,2,3,5,6,7 . The 6th is 7 which is A019565(8). - Philippe Lallouet (philip.lallouet(AT)orange.fr), Apr 28 2008",
				"a(n) is the number whose binary indices are the prime indices of the n-th squarefree number (row n of A329631), where a binary index of n is any position of a 1 in its reversed binary expansion, and a prime index of n is a number m such that prime(m) divides n. The binary indices of n are row n of A048793, while the prime indices of n are row n of A112798. - _Gus Wiseman_, Nov 30 2019"
			],
			"link": [
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 2^(i1-1)+2^(i2-1)+...+2^(iz-1), where A005117(n) = p_i1*p_i2*p_i3*...*p_iz.",
				"A019565(a(n)) = A005117(n). - _Peter Munn_, Nov 19 2019",
				"A000120(a(n)) = A072047(n). - _Gus Wiseman_, Nov 30 2019"
			],
			"example": [
				"From _Gus Wiseman_, Nov 30 2019: (Start)",
				"The sequence of squarefree numbers together with their prime indices (A329631) and the number a(n) with those binary indices begins:",
				"   1 -\u003e  {}      -\u003e   0",
				"   2 -\u003e  {1}     -\u003e   1",
				"   3 -\u003e  {2}     -\u003e   2",
				"   5 -\u003e  {3}     -\u003e   4",
				"   6 -\u003e  {1,2}   -\u003e   3",
				"   7 -\u003e  {4}     -\u003e   8",
				"  10 -\u003e  {1,3}   -\u003e   5",
				"  11 -\u003e  {5}     -\u003e  16",
				"  13 -\u003e  {6}     -\u003e  32",
				"  14 -\u003e  {1,4}   -\u003e   9",
				"  15 -\u003e  {2,3}   -\u003e   6",
				"  17 -\u003e  {7}     -\u003e  64",
				"  19 -\u003e  {8}     -\u003e 128",
				"  21 -\u003e  {2,4}   -\u003e  10",
				"  22 -\u003e  {1,5}   -\u003e  17",
				"  23 -\u003e  {9}     -\u003e 256",
				"  26 -\u003e  {1,6}   -\u003e  33",
				"  29 -\u003e  {10}    -\u003e 512",
				"  30 -\u003e  {1,2,3} -\u003e   7",
				"(End)"
			],
			"maple": [
				"encode_sqrfrees := proc(upto_n) local b,i; b := [ ]; for i from 1 to upto_n do if(0 \u003c\u003e mobius(i)) then b := [ op(b), bef(i) ]; fi; od: RETURN(b); end; # see A048623 for bef"
			],
			"mathematica": [
				"Join[{0}, Total[2^(PrimePi[FactorInteger[#][[All, 1]]] - 1)]\u0026 /@ Select[ Range[2, 100], SquareFreeQ]] (* _Jean-François Alcover_, Mar 15 2016 *)"
			],
			"program": [
				"(PARI) lista(nn) = {for (n=1, nn, if (issquarefree(n), if (n==1, x = 0, f = factor(n); x = sum(k=1, #f~, 2^(primepi(f[k, 1])-1))); print1(x, \", \"); ); ); } \\\\ _Michel Marcus_, Oct 02 2015"
			],
			"xref": [
				"Inverse: A246353 (see also A064273).",
				"Cf. A005117, A048639, A048640, A048623.",
				"Cf. A019565.",
				"A similar encoding of set-systems is A329661.",
				"Cf. A000120, A048793, A056239, A070939, A072047, A112798, A329631."
			],
			"keyword": "easy,nonn",
			"offset": "1,3",
			"author": "_Antti Karttunen_, Jul 14 1999",
			"references": 16,
			"revision": 31,
			"time": "2019-12-01T23:19:20-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}