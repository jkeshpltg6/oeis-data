{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A292382",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 292382,
			"data": "0,1,2,2,4,5,8,4,4,9,16,10,32,17,10,8,64,9,128,18,18,33,256,20,8,65,8,34,512,21,1024,16,34,129,20,18,2048,257,66,36,4096,37,8192,66,20,513,16384,40,16,17,130,130,32768,17,36,68,258,1025,65536,42,131072,2049,36,32,68,69,262144,258,514,41,524288,36,1048576,4097,18,514,40",
			"name": "Base-2 expansion of a(n) encodes the steps where numbers of the form 4k+2 are encountered when map x -\u003e A252463(x) is iterated down to 1, starting from x=n.",
			"link": [
				"Antti Karttunen, \u003ca href=\"/A292382/b292382.txt\"\u003eTable of n, a(n) for n = 1..2048\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A292272(A156552(n)).",
				"a(1) = 0; for n \u003e 1, a(n) = 2*a(A252463(n)) + [n == 2 (mod 4)], where the last part of the formula is Iverson bracket, giving 1 only if n is of the form 4k+2, and 0 otherwise.",
				"a(n) = A292372(A292384(n)).",
				"Other identities. For n \u003e= 1:",
				"a(n) AND A292380(n) = 0, where AND is a bitwise-AND (A004198).",
				"a(n) + A292380(n) = A156552(n).",
				"A000120(a(n)) + A000120(A292380(n)) = A001222(n)."
			],
			"mathematica": [
				"Table[FromDigits[Reverse@ NestWhileList[Function[k, Which[k == 1, 1, EvenQ@ k, k/2, True, Times @@ Power[Which[# == 1, 1, # == 2, 1, True, NextPrime[#, -1]] \u0026 /@ First@ #, Last@ #] \u0026@ Transpose@ FactorInteger@ k]], n, # \u003e 1 \u0026] /. k_ /; IntegerQ@ k :\u003e If[Mod[k, 4] == 2, 1, 0], 2], {n, 77}] (* _Michael De Vlieger_, Sep 21 2017 *)"
			],
			"program": [
				"(PARI)",
				"A064989(n) = {my(f); f = factor(n); if((n\u003e1 \u0026\u0026 f[1,1]==2), f[1,2] = 0); for (i=1, #f~, f[i,1] = precprime(f[i,1]-1)); factorback(f)};",
				"A252463(n) = if(!(n%2),n/2,A064989(n));",
				"A292382(n) = if(1==n,0,(if(2==(n%4),1,0)+(2*A292382(A252463(n)))));",
				"(PARI) a(n) = my(m=factor(n),k=-2); sum(i=1,matsize(m)[1], 1 \u003c\u003c (primepi(m[i,1]) + (k+=m[i,2]))); \\\\ _Kevin Ryde_, Dec 11 2020",
				"(Scheme) (define (A292382 n) (A292372 (A292384 n)))",
				"(Python)",
				"from sympy.core.cache import cacheit",
				"from sympy.ntheory.factor_ import digits",
				"from sympy import factorint, prevprime",
				"from operator import mul",
				"from functools import reduce",
				"def a292372(n):",
				"    k=digits(n, 4)[1:]",
				"    return 0 if n==0 else int(\"\".join(['1' if i==2 else '0' for i in k]), 2)",
				"def a064989(n):",
				"    f=factorint(n)",
				"    return 1 if n==1 else reduce(mul, [1 if i==2 else prevprime(i)**f[i] for i in f])",
				"def a252463(n): return 1 if n==1 else n//2 if n%2==0 else a064989(n)",
				"@cacheit",
				"def a292384(n): return 1 if n==1 else 4*a292384(a252463(n)) + n%4",
				"def a(n): return a292372(a292384(n))",
				"print([a(n) for n in range(1, 111)]) # _Indranil Ghosh_, Sep 21 2017"
			],
			"xref": [
				"Cf. A005940, A156552, A292272, A292372, A292380, A292381, A292383, A292384."
			],
			"keyword": "nonn,base",
			"offset": "1,3",
			"author": "_Antti Karttunen_, Sep 15 2017",
			"references": 8,
			"revision": 29,
			"time": "2021-05-15T06:18:22-04:00",
			"created": "2017-09-18T09:26:45-04:00"
		}
	]
}