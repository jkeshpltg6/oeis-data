{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A288814",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 288814,
			"data": "4,6,8,10,15,14,21,28,35,22,33,26,39,52,65,34,51,38,57,76,95,46,69,92,115,184,161,58,87,62,93,124,155,248,217,74,111,148,185,82,123,86,129,172,215,94,141,188,235,376,329,106,159,212,265,424,371,118,177,122,183,244,305,488,427,134,201,268,335,142",
			"name": "a(n) is the smallest composite number whose prime divisors (with multiplicity) sum to n.",
			"comment": [
				"Agrees with A056240(n) if n is composite (but not if n is prime).",
				"For n prime, let P_n = greatest prime \u003c n such that A056240(n-P_n) = A288313(m) for some m; then a(n) = Min{q*a(n-q): q prime, n-1 \u003e q \u003e= P_n}.",
				"In most cases q is the greatest prime \u003c p, but there are exceptions; e.g., p=211 is the smallest prime for which q (=197) is the second prime removed from 211, not the first. 541 is the next prime with this property (q=521). The same applies to p=16183, for which q=16139, the second prime removed from p. These examples all arise with q being the lesser of a prime pair.",
				"For p prime, a(p) = q*a(p-q) for some prime q \u003c p as described above. Then a(p-q) = 2,4,8 or 3*r for some prime r.",
				"The subsequence of terms (4, 6, 8, 10, 14, 21, 22, 26, 34, ...), where for all m \u003e n, a(m) \u003e a(n) is the same as sequence A088686, and the sequence of its indices (4, 5, 6, 7, 9, 10, 13, 19, ...) is the same as A088685. - _David James Sycamore_, Jun 30 2017",
				"Records are in A088685. - _Robert G. Wilson v_, Feb 26 2018",
				"Number of terms less than 10^k, k=1,2,3,...: 3, 32, 246, 2046, 17053, 147488, ..., . - _Robert G. Wilson v_, Feb 26 2018"
			],
			"link": [
				"David A. Corneth, \u003ca href=\"/A288814/b288814.txt\"\u003eTable of n, a(n) for n = 4..10003\u003c/a\u003e (terms 4..1000 from Michel Marcus)",
				"David A. Corneth, \u003ca href=\"/A288814/a288814.gp.txt\"\u003ePARI program\u003c/a\u003e"
			],
			"example": [
				"a(5) = 6 = 2*3 is the smallest composite number whose prime divisors add to 5.",
				"a(7) = 10 = 2*5 is the smallest composite number whose prime divisors add to 7.",
				"12 = 2 * 2 * 3 is not in the sequence, since the sum of its prime divisors is 7, a value already obtained by the lesser 10. - _David A. Corneth_, Jun 22 2017"
			],
			"maple": [
				"N:= 100: # to get a(4)..a(N)",
				"V:= Array(4..N): count:= 0:",
				"for k from 4 while count \u003c N-3 do",
				"  if isprime(k) then next fi;",
				"  s:= add(t[1]*t[2], t = ifactors(k)[2]);",
				"if s \u003c= N and V[s]=0 then",
				"    V[s]:= k; count:= count+1;",
				"fi",
				"od:",
				"convert(V,list); # _Robert Israel_, Feb 26 2018"
			],
			"mathematica": [
				"Function[s, Table[FirstPosition[s, _?(# == n \u0026)][[1]], {n, 4, 73}]]@ Table[Boole[CompositeQ@ n] Total@ Flatten@ Map[ConstantArray[#1, #2] \u0026 @@ # \u0026, FactorInteger[n]], {n, 10^3}] (* _Michael De Vlieger_, Jun 19 2017 *)",
				"f[n_] := If[ PrimeQ@ n, 0, spf = Plus @@ Flatten[ Table[#1, {#2}] \u0026 @@@ FactorInteger@ n]]; t[_] := 0; k = 1; While[k \u003c 500, If[ t[f[k]] == 0, t[f[k]] = k]; k++]; t@# \u0026 /@ Range[4, 73] (* _Robert G. Wilson v_, Feb 26 2018 *)"
			],
			"program": [
				"(PARI) isok(k, n) = my(f=factor(k)); sum(j=1, #f~, f[j,1]*f[j,2]) == n;",
				"a(n) = forcomposite(k=1,, if (isok(k, n), return(k))); \\\\ _Michel Marcus_, Jun 21 2017",
				"(PARI) lista(n) = {my(res = vector(n), s, todo); if(n \u003c 4, return([]), todo = n-3); forcomposite(k=4, , f=factor(k); s = sum(j=1, #f~, f[j, 1]*f[j, 2]); if(s\u003c=n, if(res[s]==0, res[s]=k; todo--; if(todo==0, return(vector(n-3, i, res[i+3]))))))} \\\\ _David A. Corneth_, Jun 21 2017",
				"(PARI) See PARI-link \\\\ _David A. Corneth_, Mar 23 2018"
			],
			"xref": [
				"Cf. A046343, A056240, A088685, A288313."
			],
			"keyword": "nonn",
			"offset": "4,1",
			"author": "_David James Sycamore_, Jun 16 2017",
			"references": 17,
			"revision": 97,
			"time": "2019-03-06T04:09:43-05:00",
			"created": "2017-06-20T11:27:55-04:00"
		}
	]
}