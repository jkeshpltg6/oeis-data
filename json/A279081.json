{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A279081",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 279081,
			"data": "1,3,4,6,4,8,12,16,8,12,8,12,8,20,16,20,8,24,16,24,8,16,18,24,18,36,24,24,8,24,20,24,16,48,32,24,8,32,24,32,8,24,32,48,16,20,24,45,18,36,16,36,24,96,48,32,8,24,16,24,16,56,96,56,16,24,16,48,16",
			"name": "Number of divisors of the n-th tetrahedral number.",
			"comment": [
				"The n-th tetrahedral number is A000292(n) = n*(n+1)*(n+2)/6. The only odd-valued terms are a(1)=1, a(2)=3, and a(48)=45, corresponding to the only nonzero tetrahedral numbers that are also square, i.e., A000292(1)=1, A000292(2)=4, and A000292(48)=19600.",
				"We can write n*(n+1)*(n+2)/6 as the product of three pairwise coprime integers A, B, and C as follows, depending on the value of n mod 12:",
				".",
				"n mod 12   A      B        C     factor that can be even",
				"========  ===  =======  =======  =======================",
				"    0     n/3    n+1    (n+2)/2           A",
				"    1      n   (n+1)/2  (n+2)/3             B",
				"    2     n/2  (n+1)/3    n+2                 C",
				"    3     n/3  (n+1)/2    n+2               B",
				"    4      n     n+1    (n+2)/6           A",
				"    5      n   (n+1)/6    n+2               B",
				"    6     n/6    n+1      n+2                 C",
				"    7      n   (n+1)/2  (n+2)/3             B",
				"    8      n   (n+1)/3  (n+2)/2           A",
				"    9     n/3  (n+1)/2    n+2               B",
				"   10     n/2    n+1    (n+2)/3               C",
				"   11      n   (n+1)/6    n+2               B",
				".",
				"For all n \u003e 6, A, B, and C are all greater than 1 and share no prime factors, so their product must contain at least three distinct prime factors; consequently, its number of divisors cannot be prime or semiprime. The only semiprimes in this sequence are a(3), a(4), and a(5), and the only prime is a(2)."
			],
			"link": [
				"Michel Marcus, \u003ca href=\"/A279081/b279081.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A000005(A000292(n)) = A000005(n*(n+1)*(n+2)/6)."
			],
			"example": [
				"a(48) = tau(48*59*50/6) = tau(19600) = tau(2^4 * 5^2 * 7^2) = (4+1)*(2+1)*(2+1) = 5*3*3 = 45."
			],
			"program": [
				"(PARI) a(n) = numdiv(n*(n+1)*(n+2)/6); \\\\ _Michel Marcus_, Jan 07 2017"
			],
			"xref": [
				"Cf. A000292, A279082, A279083."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Jon E. Schoenfield_, Jan 06 2017",
			"references": 3,
			"revision": 13,
			"time": "2017-01-22T15:24:07-05:00",
			"created": "2017-01-08T12:25:12-05:00"
		}
	]
}