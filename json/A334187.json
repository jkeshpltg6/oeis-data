{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A334187",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 334187,
			"data": "1,1,1,1,2,1,1,3,3,1,4,6,2,1,5,10,6,1,1,6,15,14,4,1,7,21,26,10,1,8,28,44,25,1,9,36,68,51,4,1,10,45,100,98,24,1,11,55,140,165,64,7,1,12,66,190,267,144,25,1,13,78,250,407,284,78,6,1,14,91,322,601,520,188,22,1",
			"name": "Number T(n,k) of k-element subsets of [n] avoiding 3-term arithmetic progressions; triangle T(n,k), n\u003e=0, 0\u003c=k\u003c=A003002(n), read by rows.",
			"comment": [
				"T(n,k) is defined for all n \u003e= 0 and k \u003e= 0.  The triangle contains only elements with 0 \u003c= k \u003c= A003002(n).  T(n,k) = 0 for k \u003e A003002(n)."
			],
			"link": [
				"Fausto A. C. Cariboni, \u003ca href=\"/A334187/b334187.txt\"\u003eRows n = 0..70, flattened\u003c/a\u003e (rows n = 0..40 from Alois P. Heinz)",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/NonaveragingSequence.html\"\u003eNonaveraging Sequence\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Arithmetic_progression\"\u003eArithmetic progression\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Salem-Spencer_set\"\u003eSalem-Spencer set\u003c/a\u003e",
				"\u003ca href=\"/index/No#non_averaging\"\u003eIndex entries related to non-averaging sequences\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = Sum_{j=0..n} A334892(j,k).",
				"T(n,A003002(n)) = A262347(n)."
			],
			"example": [
				"Triangle T(n,k) begins:",
				"  1;",
				"  1,  1;",
				"  1,  2,   1;",
				"  1,  3,   3;",
				"  1,  4,   6,   2;",
				"  1,  5,  10,   6,    1;",
				"  1,  6,  15,  14,    4;",
				"  1,  7,  21,  26,   10;",
				"  1,  8,  28,  44,   25;",
				"  1,  9,  36,  68,   51,    4;",
				"  1, 10,  45, 100,   98,   24;",
				"  1, 11,  55, 140,  165,   64,   7;",
				"  1, 12,  66, 190,  267,  144,  25;",
				"  1, 13,  78, 250,  407,  284,  78,   6;",
				"  1, 14,  91, 322,  601,  520, 188,  22,  1;",
				"  1, 15, 105, 406,  849,  862, 386,  64,  4;",
				"  1, 16, 120, 504, 1175, 1394, 763, 164, 14;",
				"  ..."
			],
			"maple": [
				"b:= proc(n, s) option remember; `if`(n=0, 1, b(n-1, s)+ `if`(",
				"      ormap(j-\u003e 2*j-n in s, s), 0, expand(x*b(n-1, s union {n}))))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=0..degree(p)))(b(n, {})):",
				"seq(T(n), n=0..16);"
			],
			"mathematica": [
				"b[n_, s_] := b[n, s] = If[n == 0, 1, b[n-1, s] + If[AnyTrue[s, MemberQ[s, 2 # - n]\u0026], 0, Expand[x b[n-1, s ~Union~ {n}]]]];",
				"T[n_] := Function[p, Table[Coefficient[p, x, i], {i, 0, Exponent[p, x]}]][ b[n, {}]];",
				"T /@ Range[0, 16] // Flatten (* _Jean-François Alcover_, May 30 2020, after Maple *)"
			],
			"xref": [
				"Columns k=0-4 give: A000012, A000027, A000217(n-1), A212964(n-1), A300760.",
				"Row sums give A051013.",
				"Last elements of rows give A262347.",
				"Cf. A003002, A334892."
			],
			"keyword": "nonn,tabf",
			"offset": "0,5",
			"author": "_Alois P. Heinz_, May 14 2020",
			"references": 7,
			"revision": 52,
			"time": "2020-09-30T01:59:25-04:00",
			"created": "2020-05-16T15:31:30-04:00"
		}
	]
}