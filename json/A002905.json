{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002905",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2905,
			"id": "M2486 N0985",
			"data": "1,1,1,3,5,12,30,79,227,710,2322,8071,29503,112822,450141,1867871,8037472,35787667,164551477,779945969,3804967442,19079312775,98211456209,518397621443,2802993986619,15510781288250,87765472487659,507395402140211,2994893000122118,18035546081743772,110741792670074054,692894304050453139",
			"name": "Number of connected graphs with n edges.",
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Max Alekseyev, \u003ca href=\"/A002905/b002905.txt\"\u003eTable of n, a(n) for n = 0..60\u003c/a\u003e",
				"G. A. Baker et al., \u003ca href=\"http://dx.doi.org/10.1103/PhysRev.164.800\"\u003eHigh-temperature expansions for the spin-1/2 Heisenberg model\u003c/a\u003e, Phys. Rev., 164 (1967), 800-817.",
				"Nicolas Borie, \u003ca href=\"http://arxiv.org/abs/1511.05843\"\u003eThe Hopf Algebra of graph invariants\u003c/a\u003e, arXiv preprint arXiv:1511.05843 [math.CO], 2015.",
				"P. J. Cameron, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL3/groups.html\"\u003eSequences realized by oligomorphic permutation groups\u003c/a\u003e, J. Integ. Seqs. Vol. 3 (2000), #00.1.5.",
				"Anjan Dutta, Hichem Sahbi, \u003ca href=\"https://arxiv.org/abs/1803.00425\"\u003eGraph Kernels based on High Order Graphlet Parsing and Hashing\u003c/a\u003e, arXiv:1803.00425 [cs.CV], 2018.",
				"Gordon Royle, \u003ca href=\"http://staffhome.ecm.uwa.edu.au/~00013890/remote/graphs/\"\u003eSmall graphs\u003c/a\u003e",
				"M. L. Stein and P. R. Stein, \u003ca href=\"http://dx.doi.org/10.2172/4180737\"\u003eEnumeration of Linear Graphs and Connected Linear Graphs up to p = 18 Points\u003c/a\u003e. Report LA-3775, Los Alamos Scientific Laboratory of the University of California, Los Alamos, NM, Oct 1967",
				"Peter Steinbach, \u003ca href=\"/A000664/a000664_1.pdf\"\u003eField Guide to Simple Graphs, Volume 4\u003c/a\u003e, Part 1 (For Volumes 1, 2, 3, 4 of this book see A000088, A008406, A000055, A000664, respectively.)",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Polynema.html\"\u003ePolynema.\u003c/a\u003e"
			],
			"formula": [
				"A000664 and this sequence are an Euler transform pair. - _N. J. A. Sloane_, Aug 30 2016"
			],
			"example": [
				"a(3) = 3 since the three connected graphs with three edges are a path, a triangle and a \"Y\".",
				"The first difference between this sequence and A046091 is for n=9 edges where we see K_{3,3}, the well-known \"utility graph\"."
			],
			"mathematica": [
				"A000664 = Cases[Import[\"https://oeis.org/A000664/b000664.txt\", \"Table\"], {_, _}][[All, 2]];",
				"(* EulerInvTransform is defined in A022562 *)",
				"Join[{1}, EulerInvTransform[Rest @ A000664]] (* _Jean-François Alcover_, May 10 2019, updated Mar 17 2020 *)"
			],
			"xref": [
				"Column sums of A054924 or equivalently row sums of A054923.",
				"Cf. A000664, A046091 (for connected planar graphs), A275421 (multisets).",
				"Apart from a(3), same as A003089."
			],
			"keyword": "nonn,nice",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Vladeta Jovovic_, Jan 12 2000",
				"More terms from _Gordon F. Royle_, Jun 05 2003",
				"a(25)-a(26) from _Max Alekseyev_, Sep 19 2009",
				"a(27)-a(60) from _Max Alekseyev_, Sep 07 2016"
			],
			"references": 26,
			"revision": 61,
			"time": "2021-12-26T20:34:38-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}