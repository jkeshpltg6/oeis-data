{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A109041",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 109041,
			"data": "1,-9,27,-9,-117,216,27,-450,459,-9,-648,1080,-117,-1530,1350,216,-1845,2592,27,-3258,2808,-450,-3240,4752,459,-5409,4590,-9,-5850,7560,-648,-8658,7371,1080,-7776,10800,-117,-12330,9774,-1530,-11016,15120,1350,-16650",
			"name": "Expansion of eta(q)^9 / eta(q^3)^3 in powers of q.",
			"comment": [
				"Number 4 of the 74 eta-quotients listed in Table I of Martin (1996).",
				"Cubic AGM theta functions: a(q) (see A004016), b(q) (A005928), c(q) (A005882)."
			],
			"reference": [
				"G. E. Andrews and B. C. Berndt, Ramanujan's lost notebook, Part I, Springer, New York, 2005, MR2135178 (2005m:11001) See p. 313, Equ. (14.2.13)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A109041/b109041.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"G. E. Andrews and B. C. Berndt, \u003ca href=\"http://www.ams.org/notices/200801/tx080100018p.pdf\"\u003eYour Hit Parade: The Top Ten Most Fascinating Formulas in Ramanujan's Lost Notebook\u003c/a\u003e, Notices Amer. Math. Soc., 55 (No. 1, 2008), 18-30. See p. 23, Equation (27).",
				"J. M. Borwein and P. B. Borwein, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-1991-1010408-0\"\u003eA cubic counterpart of Jacobi's identity and the AGM\u003c/a\u003e, Trans. Amer. Math. Soc., 323 (1991), no. 2, 691-701. See p. 697.",
				"Y. Martin, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-96-01743-6\"\u003eMultiplicative eta-quotients\u003c/a\u003e, Trans. Amer. Math. Soc. 348 (1996), no. 12, 4825-4856, see page 4852 Table I.",
				"Michael Somos, \u003ca href=\"/A030203/a030203.txt\"\u003eIndex to Yves Martin's list of 74 multiplicative eta-quotients and their A-numbers\u003c/a\u003e"
			],
			"formula": [
				"G.f.: Product_{k\u003e0} (1 - x^k)^9 / (1 - x^3)^3 = 1 - 9 * Sum_{k\u003e0} x^k * (1 - x^k -6 * x^(2*k) - x^(3*k) + x^(4*k)) / (1 + x^k + x^(2*k))^3.",
				"Expansion of b(q)^3 in powers of q where b() is a cubic AGM theta function.",
				"Euler transform of period 3 sequence [ -9, -9, -6, ...].",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2), A(x^4)) where f(u, v, w) = v^3 + u*w * (u + 6*v - 8*w).",
				"Given A = A0 + A1 + A2 is the 3-section, then 0 = A1^3 + A2^3 - 3*A0*A1*A2. A0 = A(q^3) = b(q^3)^3, A1 = -3 * a(q^3)^2 * c(q^3), A2 = 3 * a(q^3) * c(q^3)^2 where a(), b(), c() are cubic AGM theta functions.",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (3 t)) = 19683^(1/2) (t/i)^3 g(t) where q = exp(2 Pi i t) and g() is the g.f. for A106402. - _Michael Somos_, Mar 11 2012",
				"a(n) = -9 * A103440(n) unless n = 0. a(6*n + 5) = 216 * A134340(n).",
				"A008654(n) = a(n) + 27 * A106402(n) is the identity a(q)^3 = b(q)^3 + c(q)^3. - _Michael Somos_, Jul 19 2012",
				"a(n) = -9 * b(n) where b(n) is multiplicative with a(0) = 1, b(p^e) = 1, if p=3, b(p^e) = b(p) * b(p^(e-1)) + Kronecker(-3, p) * p^2 * b(p^(e-2)) otherwise. - _Michael Somos_, May 18 2015",
				"Convolution cube of A005928. - _Michael Somos_, May 18 2015"
			],
			"example": [
				"G.f. = 1 - 9*q + 27*q^2 - 9*q^3 - 117*q^4 + 216*q^5 + 27*q^6 - 450*q^7 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 1, Boole[ n == 0], - 9 DivisorSum[ n, #^2 KroneckerSymbol[ -3, #] \u0026]]; (* _Michael Somos_, Jul 19 2012 *)",
				"a[ n_] := SeriesCoefficient[ (QPochhammer[ q]^3 / QPochhammer[ q^3])^3, {q, 0, n}]; (* _Michael Somos_, Jul 19 2012 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n==0, -9 * sumdiv( n, d, d^2 * kronecker(-3, d)))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A)^9 / eta(x^3 + A)^3, n))};",
				"(MAGMA) A := Basis( ModularForms( Gamma1(3), 3), 44); A[1] - 9*A[2]; /* _Michael Somos_, May 18 2015 */"
			],
			"xref": [
				"Cf. A005928, A008654, A103440, A106402, A134340."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Jun 17 2005",
			"references": 10,
			"revision": 37,
			"time": "2018-11-22T21:50:11-05:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}