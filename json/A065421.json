{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A065421",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 65421,
			"data": "1,9,0,2,1,6,0,5,8",
			"name": "Decimal expansion of Viggo Brun's constant B, also known as the twin primes constant B_2: Sum (1/p + 1/q) as (p,q) runs through the twin primes.",
			"comment": [
				"The calculation of Brun's constant is \"based on heuristic considerations about the distribution of twin primes\" (Ribenboim, 1989).",
				"Another constant related to the twin primes is the twin primes constant C_2 (sometimes also denoted PI_2) A005597 defined in connection with the Hardy-Littlewood conjecture concerning the distribution pi_2(x) of the twin primes.",
				"Comment from _Hans Havermann_, Aug 06 2018: \"I don't think the last three (or possibly even four) OEIS terms [he is referring to the sequence at that date - it has changed since then] are necessarily warranted. P. Sebah (see link below) (http://numbers.computation.free.fr/Constants/Primes/twin.html) gives 1.902160583104... as the value for primes to 10^16 followed by a suggestion that the (final) value 'should be around 1.902160583...'\" - added by _N. J. A. Sloane_, Aug 06 2018"
			],
			"reference": [
				"R. Crandall and C. Pomerance, Prime Numbers: A Computational Perspective, Springer, NY, 2001; see p. 14.",
				"S. R. Finch, Mathematical Constants, Cambridge, 2003, pp. 133-135.",
				"P. Ribenboim, The Book of Prime Number Records, 2nd. ed., Springer-Verlag, New York, 1989, p. 201."
			],
			"link": [
				"V. Brun, La série 1/5 + 1/7 + 1/11 + 1/13 + 1/17 + 1/19 + 1/29 + 1/31 + 1/41 + 1/43 + 1/59 + 1/61 + ... où les dénominateurs sont \"nombres premiers jumeaux\" est convergente ou finie, Bull Sci. Math. 43 (1919), \u003ca href=\"https://gallica.bnf.fr/ark:/12148/bpt6k96292009/f104.image\"\u003e100-104\u003c/a\u003e and \u003ca href=\"https://gallica.bnf.fr/ark:/12148/bpt6k96292009/f128.image\"\u003e124-128\u003c/a\u003e.",
				"C. K. Caldwell, The Prime Glossary, \u003ca href=\"http://primes.utm.edu/glossary/page.php?sort=BrunsConstant\"\u003eBrun's constant\u003c/a\u003e",
				"Steven R. Finch, \u003ca href=\"http://www.people.fas.harvard.edu/~sfinch/constant/brun/brun.html\"\u003eBrun's Constant\u003c/a\u003e [Broken link]",
				"Steven R. Finch, \u003ca href=\"http://web.archive.org/web/20010603070928/http://www.mathsoft.com/asolve/constant/brun/brun.html\"\u003eBrun's Constant\u003c/a\u003e [From the Wayback machine]",
				"Thomas R. Nicely, \u003ca href=\"https://faculty.lynchburg.edu/~nicely/twins/twins.html\"\u003eEnumeration to 10^14 of the twin primes and Brun's constant\u003c/a\u003e, Virginia Journal of Science, 46:3 (Fall, 1995), 195-204.",
				"Thomas R. Nicely, \u003ca href=\"/A001359/a001359.pdf\"\u003eEnumeration to 10^14 of the twin primes and Brun's constant\u003c/a\u003e [Local copy, pdf only]",
				"Thomas R. Nicely, \u003ca href=\"https://faculty.lynchburg.edu/~nicely/counts.html\"\u003ePrime Constellations Research Project\u003c/a\u003e",
				"P. Sebah, \u003ca href=\"http://numbers.computation.free.fr/Constants/Primes/twin.html\"\u003eNumbers, constants and computation\u003c/a\u003e",
				"D. Shanks and J. W. Wrench, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1974-0352022-X\"\u003eBrun's constant\u003c/a\u003e, Math. Comp. 28 (1974) 293-299; 28 (1974) 1183; Math. Rev. 50 #4510.",
				"H. Tronnolone, \u003ca href=\"https://www.semanticscholar.org/paper/A-tale-of-two-primes-Tronnolone/2576b80d487c909639c98a1e3cb255658c40d699\"\u003eA tale of two primes\u003c/a\u003e, COLAUMS Space, #3, 2013.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Brun%27s_constant\"\u003eBrun's constant\u003c/a\u003e"
			],
			"formula": [
				"Equals Sum_{n\u003e=1} 1/A077800(n).",
				"From _Dimitris Valianatos_, Dec 21 2013: (Start)",
				"(1/5) + Sum_{n\u003e=1, excluding twin primes 3,5,7,11,13,...} mu(n)/n =",
				"(1/5) + 1 - 1/2 + 1/6 + 1/10 + 1/14 + 1/15 + 1/21 + 1/22 - 1/23 + 1/26 - 1/30 + 1/33 + 1/34 + 1/35 - 1/37 + 1/38 + 1/39 - 1/42 ... = 1.902160583... (End)"
			],
			"example": [
				"(1/3 + 1/5) + (1/5 + 1/7) + (1/11 + 1/13) + ... = 1.902160583209 +- 0.000000000781 [Nicely]"
			],
			"xref": [
				"Cf. A005597 (twin prime constant Product_{ p prime \u003e= 3 } (1-1/(p-1)^2)).",
				"Cf. A077800 (twin primes)."
			],
			"keyword": "hard,more,nonn,cons,nice",
			"offset": "1,2",
			"author": "_Robert G. Wilson v_, Sep 08 2000",
			"ext": [
				"Corrected by _N. J. A. Sloane_, Nov 16 2001",
				"More terms computed by Pascal Sebah (pascal_sebah(AT)ds-fr.com), Jul 15 2001",
				"Further terms computed by Pascal Sebah (psebah(AT)yahoo.fr), Aug 22 2002",
				"Commented and edited by _Daniel Forgues_, Jul 28 2009",
				"Commented and reference added by _Jonathan Sondow_, Nov 26 2010",
				"Unsound terms after a(9) removed by _Gord Palameta_, Sep 06 2018"
			],
			"references": 23,
			"revision": 90,
			"time": "2021-10-28T13:18:32-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}