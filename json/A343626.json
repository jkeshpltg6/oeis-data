{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A343626",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 343626,
			"data": "0,0,0,0,0,8,7,3,0,0,1,1,0,2,3,1,9,8,1,6,7,0,1,2,0,4,2,7,7,9,1,4,5,2,3,1,9,4,9,5,6,1,0,7,9,7,6,4,5,3,9,1,8,3,6,9,8,9,7,1,7,7,1,3,8,1,3,6,2,9,8,3,2,9,4,5,3,8,7,6,4,9,6,9,9,3,6,1,8,5,8,6,2,3,2,9,3,3,4,5",
			"name": "Decimal expansion of the Prime Zeta modulo function P_{3,1}(6) = Sum 1/p^6 over primes p == 1 (mod 3).",
			"comment": [
				"The Prime Zeta modulo function at 6 for primes of the form 3k+1 is Sum_{primes in A002476} 1/p^6 = 1/7^6 + 1/13^6 + 1/19^6 + 1/31^6 + ...",
				"The complementary Sum_{primes in A003627} 1/p^6 is given by P_{3,2}(6) = A085966 - 1/3^6 - (this value here) = 0.015689614727130461563527666... = A343606."
			],
			"link": [
				"R. J. Mathar, \u003ca href=\"http://arxiv.org/abs/1008.2547\"\u003eTable of Dirichlet L-series and Prime Zeta Modulo Functions for Small Moduli\u003c/a\u003e, arXiv:1008.2547 [math.NT], 2010-2015, p.21.",
				"\u003ca href=\"/index/Z#zeta_function\"\u003eOEIS index to entries related to the (prime) zeta function\u003c/a\u003e."
			],
			"example": [
				"P_{3,1}(6) = 8.7300110231981670120427791452319495610797645391837...*10^-8"
			],
			"mathematica": [
				"With[{s=6}, Do[Print[N[1/2 * Sum[(MoebiusMu[2*n + 1]/(2*n + 1)) * Log[(Zeta[s + 2*n*s]*(Zeta[s + 2*n*s, 1/6] - Zeta[s + 2*n*s, 5/6])) / ((1 + 2^(s + 2*n*s))*(1 + 3^(s + 2*n*s)) * Zeta[2*(1 + 2*n)*s])], {n, 0, m}], 120]], {m, 100, 500, 100}]] (* adopted from _Vaclav Kotesovec_'s code in A175645 *)"
			],
			"program": [
				"(PARI) s=0; forprimestep(p=1, 1e8, 3, s+=1./p^6); s \\\\ For illustration: primes up to 10^N give 5N+2 (= 42 for N=8) correct digits.",
				"(PARI) A343626_upto(N=100)={localprec(N+5);digits((PrimeZeta31(6)+1)\\.1^N)[^1]} \\\\ cf. A175644 for PrimeZeta31"
			],
			"xref": [
				"Cf. A175645, A343624 - A343629 (P_{3,1}(3..9): same for 1/p^n, n=3..9), A343606 (P_{3,2}(6): same for p==2 (mod 3)), A086036 (P_{4,1}(6): same for p==1 (mod 4)).",
				"Cf. A085966 (PrimeZeta(6)), A002476 (primes of the form 3k+1)."
			],
			"keyword": "cons,nonn",
			"offset": "0,6",
			"author": "_M. F. Hasler_, Apr 23 2021",
			"references": 3,
			"revision": 8,
			"time": "2021-04-24T03:12:48-04:00",
			"created": "2021-04-23T21:04:45-04:00"
		}
	]
}