{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A107667",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 107667,
			"data": "1,4,2,45,9,3,816,112,16,4,20225,2200,225,25,5,632700,58176,4860,396,36,6,23836540,1920163,138817,9408,637,49,7,1048592640,75683648,4886464,290816,16576,960,64,8,52696514169,3460349970,203451912,10948203,553473",
			"name": "Triangular matrix T, read by rows, that satisfies: T = D + SHIFT_LEFT(T^2) where SHIFT_LEFT shifts each row 1 place to the left and D is the diagonal matrix {1, 2, 3, ...}.",
			"formula": [
				"Matrix diagonalization method: define the triangular matrix P by P(n, k) = ((n+1)^2)^(n-k)/(n-k)! for n \u003e=k \u003e= 0 and the diagonal matrix D by D(n, n) = n+1 for n \u003e= 0; then T is given by T = P^-1*D*P.",
				"Rows read in reverse form the initial terms of the g.f.: (n+1) = Sum_{k\u003e=0} T(n, n-k) * x^k * Product_{j=0..k} (1-(n+1-j)*x) = T(n, n)*(1-(n+1)*x) + T(n, n-1)*x*(1-(n+1)*x)*(1-n*x) + T(n, n-2)*x^2*(1-(n+1)*x)*(1-n*x)*(1-(n-1)*x) + ... [Corrected by _Petros Hadjicostas_, Mar 11 2021]"
			],
			"example": [
				"Reverse of rows form the initial terms of g.f.s below.",
				"Row n=0: 1 = 1*(1-x) + 1*x*(1-x) + ...",
				"Row n=1: 2 = 2*(1-2*x) + 4*x*(1-2*x)*(1-x) + 12*x^2*(1-2*x)*(1-x) + ...",
				"Row n=2: 3 = 3*(1-3*x) + 9*x*(1-3*x)*(1-2*x)",
				"           + 45*x^2*(1-3*x)*(1-2*x)*(1-x)",
				"           + 216*x^3*(1-3*x)*(1-2*x)*(1-x) + ...",
				"Row n=3: 4 = 4*(1-4*x) + 16*x*(1-4*x)*(1-3*x)",
				"           + 112*x^2*(1-4*x)*(1-3*x)*(1-2*x)",
				"           + 816*x^3*(1-4*x)*(1-3*x)*(1-2*x)*(1-x)",
				"           + 5248*x^4*(1-4*x)*(1-3*x)*(1-2*x)*(1-x) + ...",
				"Triangle T begins:",
				"           1;",
				"           4,        2;",
				"          45,        9,       3;",
				"         816,      112,      16,      4;",
				"       20225,     2200,     225,     25,     5;",
				"      632700,    58176,    4860,    396,    36,   6;",
				"    23836540,  1920163,  138817,   9408,   637,  49,  7;",
				"  1048592640, 75683648, 4886464, 290816, 16576, 960, 64, 8;",
				"  ...",
				"The matrix square T^2 shifts each row right 1 place, dropping the diagonal D and putting A006689 in column 0:",
				"          1;",
				"         12,        4;",
				"        216,       45,       9;",
				"       5248,      816,     112,     16;",
				"     160675,    20225,    2200,    225,   25;",
				"    5931540,   632700,   58176,   4860,  396,  36;",
				"  256182290, 23836540, 1920163, 138817, 9408, 637, 49;",
				"  ..."
			],
			"program": [
				"(PARI) {T(n,k)=local(P=matrix(n+1,n+1,r,c,if(r\u003e=c,(r^2)^(r-c)/(r-c)!)), D=matrix(n+1,n+1,r,c,if(r==c,r)));if(n\u003e=k,(P^-1*D*P)[n+1,k+1])}"
			],
			"xref": [
				"Cf. A006689, A107668 (column 0), A107669, A107670 (matrix square)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,2",
			"author": "_Paul D. Hanna_, Jun 07 2005",
			"references": 7,
			"revision": 19,
			"time": "2021-03-11T03:03:35-05:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}