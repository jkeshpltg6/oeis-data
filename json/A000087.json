{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000087",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 87,
			"id": "M1240 N0474",
			"data": "2,1,2,4,10,37,138,628,2972,14903,76994,409594,2222628,12281570,68864086,391120036,2246122574,13025721601,76194378042,449155863868,2666126033850,15925105028685,95664343622234,577651490729530",
			"name": "Number of unrooted nonseparable planar maps with n edges and a distinguished face.",
			"comment": [
				"The number of unrooted non-separable n-edge maps in the plane (planar with a distinguished outside face). - _Valery A. Liskovets_, Mar 17 2005"
			],
			"reference": [
				"V. A. Liskovets and T. R. Walsh, Enumeration of unrooted maps on the plane, Rapport technique, UQAM, No. 2005-01, Montreal, Canada, 2005.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A000087/b000087.txt\"\u003eTable of n, a(n) for n=1..200\u003c/a\u003e",
				"W. G. Brown, \u003ca href=\"http://dx.doi.org/10.4153/CJM-1963-056-7\"\u003eEnumeration of non-separable planar maps\u003c/a\u003e, Canad. J. Math., 15 (1963), 526-545.",
				"W. G. Brown, \u003ca href=\"/A000087/a000087.pdf\"\u003eEnumeration of non-separable planar maps\u003c/a\u003e",
				"V. A. Liskovets and T. R. Walsh, \u003ca href=\"http://dx.doi.org/10.1016/j.aam.2005.03.006\"\u003eCounting unrooted maps on the plane\u003c/a\u003e, Advances in Applied Math., 36, No.4 (2006), 364-387."
			],
			"formula": [
				"a(n) = (1/3n)[(n+2)binomial(3n, n)/((3n-2)(3n-1)) + Sum_{0\u003ck\u003cn, k|n}phi(n/k)binomial(3k, k)]+q(n) where phi is the Euler function A000010, q(n)=0 if n is even and q(n)=2(n+1)binomial(3(n+1)/2, (n+1)/2)/(3(3n-1)(3n+1)) if n is odd. - _Valery A. Liskovets_, Mar 17 2005"
			],
			"mathematica": [
				"q[n_] := If[EvenQ[n], 0, 2(n+1)Binomial[3(n+1)/2, (n+1)/2]/(3(3n-1)(3n+1)) ]; a[n_] := (1/(3n))((n+2)Binomial[3n, n]/((3n-2)(3n-1)) + Sum[EulerPhi[ n/k] Binomial[3k, k], {k, Divisors[n] // Most}]) + q[n]; Array[a, 30] (* _Jean-François Alcover_, Feb 04 2016, after _Valery A. Liskovets_ *)"
			],
			"program": [
				"(PARI) q(n) = if(n%2, 2*(n + 1)*binomial(3*(n + 1)/2, (n + 1)/2) / (3*(3*n - 1)*(3*n + 1)), 0);",
				"a(n) = (1/(3*n)) * ((n + 2) * binomial(3*n, n)/((3*n - 2) * (3*n - 1)) + sum(k=1, n - 1, if(Mod(n, k)==0, eulerphi(n/k) * binomial(3*k, k)))) + q(n); \\\\ _Indranil Ghosh_, Apr 04 2017"
			],
			"xref": [
				"Row sums of A046653.",
				"Cf. A006402, A103938."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _T. D. Noe_, Mar 14 2007",
				"Name corrected by _Cyril Banderier_, Apr 04 2017",
				"Name clarified by _Andrew Howroyd_, Mar 29 2021"
			],
			"references": 11,
			"revision": 47,
			"time": "2021-03-30T12:32:03-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}