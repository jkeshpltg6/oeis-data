{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008654",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8654,
			"data": "1,18,108,234,234,864,756,900,1836,2178,1296,4320,3042,3060,5400,6048,3690,10368,6588,6516,11232,11700,6480,19008,12852,10818,18360,19674,11700,30240,16848,17316,29484,30240,15552,43200,28314,24660,39096",
			"name": "Theta series of direct sum of 3 copies of hexagonal lattice.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Cubic AGM theta functions: a(q) (see A004016), b(q) (A005928), c(q) (A005882).",
				"The hexagonal lattice is the familiar 2-dimensional lattice in which each point has 6 neighbors. This is sometimes called the triangular lattice."
			],
			"reference": [
				"J. H. Conway and N. J. A. Sloane, \"Sphere Packings, Lattices and Groups\", Springer-Verlag, p. 110.",
				"B. C. Berndt, Ramanujan's Notebooks Part V, Springer-Verlag, see p. 124, Equation (7.19)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A008654/b008654.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"G. Nebe and N. J. A. Sloane, \u003ca href=\"http://www.math.rwth-aachen.de/~Gabriele.Nebe/LATTICES/A2.html\"\u003eHome page for hexagonal (or triangular) lattice A2\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of (theta_3(z)*theta_3(3z) + theta_2(z)*theta_2(3z))^3.",
				"Expansion of a(q)^3 in powers of q where a() is a cubic AGM function. - _Michael Somos_, Sep 04 2008",
				"Expansion of (eta(q)^12 + 27 * eta(q^3)^12) / (eta(q) * eta(q^3))^3 in powers of q. - _Michael Somos_, Sep 04 2008",
				"Expansion of (f(-q)^12 + 27 * q * f(-q^3)^12) / (f(-q) * f(-q^3))^3 in powers of q where f() is a Ramanujan theta function. - _Michael Somos_, Sep 04 2008",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (3 t)) = 3^(3/2) (t/i)^3 f(t) where q = exp(2 Pi i t). - _Michael Somos_, Sep 04 2008"
			],
			"example": [
				"G.f. = 1 + 18*q + 108*q^2 + 234*q^3 + 234*q^4 + 864*q^5 + 756*q^6 + 900*q^7 + ..."
			],
			"mathematica": [
				"a[ n_] := With[ {A = QPochhammer[ q]^3, A3 = QPochhammer[ q^3]^3}, SeriesCoefficient[ (A^4 + 27 q A3^4) / (A A3), {q, 0, n}]]; (* _Michael Somos_, Oct 22 2017 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A, A3); if( n\u003c0, 0, A = x * O(x^n); A3 = eta(x^3 + A)^3; A = eta(x + A)^3; polcoeff( (A^4 + 27 * x * A3^4) / (A * A3), n))}; /* _Michael Somos_, Sep 04 2008 */",
				"(MAGMA) A := Basis( ModularForms( Gamma1(3), 3), 39); A[1] + 18*A[2]; /* _Michael Somos_, Aug 26 2015 */"
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Michael Somos_, Sep 04 2008"
			],
			"references": 4,
			"revision": 28,
			"time": "2021-03-12T22:24:41-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}