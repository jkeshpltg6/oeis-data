{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A054390",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 54390,
			"data": "1,1,1,2,1,1,2,1,1,3,2,2,3,1,1,2,1,1,3,2,2,3,1,1,2,1,1,4,3,3,5,2,2,4,2,2,5,3,3,4,1,1,2,1,1,3,2,2,3,1,1,2,1,1,4,3,3,5,2,2,4,2,2,5,3,3,4,1,1,2,1,1,3,2,2,3,1,1,2,1,1,5,4,4,7,3,3,6,3,3,8,5,5,7,2,2,4,2,2,6,4,4,6,2,2",
			"name": "Number of ways of writing n as a sum of powers of 3, each power being used at most three times.",
			"comment": [
				"Let M = an infinite matrix with (1, 1, 1, 1, 0, 0, 0,...) in each column shifted down thrice from the previous column (for k\u003e0). Then A054390 = Lim_{n-\u003einf} M^n, the left-shifted vector considered as a sequence. - _Gary W. Adamson_, Apr 14 2010"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A054390/b054390.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Karl Dilcher, Larry Ericksen, \u003ca href=\"https://www.emis.de/journals/JIS/VOL21/Dilcher/dilcher7.html\"\u003ePolynomials Characterizing Hyper b-ary Representations\u003c/a\u003e, J. Int. Seq., Vol. 21 (2018), Article 18.4.3.",
				"Timothy B. Flowers, \u003ca href=\"https://www.emis.de/journals/JIS/VOL20/Flowers/flowers3.html\"\u003eExtending a Recent Result on Hyper m-ary Partition Sequences\u003c/a\u003e, Journal of Integer Sequences, Vol. 20 (2017), #17.6.7."
			],
			"formula": [
				"a(0)=1, a(1)=1, a(2)=1 and, for n\u003e0, a(3n)=a(n)+a(n-1), a(3n+1)=a(n), a(3n+2)=a(n).",
				"G.f.: Product_{j \u003e= 0} (1+x^(3^j)+x^(2*(3^j))+x^(3*(3^j))). - _Emeric Deutsch_, Apr 02 2006",
				"G.f. A(x) satisfies: A(x) = (1 + x + x^2 + x^3) * A(x^3). - _Ilya Gutkovskiy_, Jul 09 2019"
			],
			"example": [
				"a(33) = 4 because we have 33=27+3+3=27+3+1+1+1=9+9+9+3+3=9+9+9+3+1+1+1."
			],
			"maple": [
				"a[0]:=1: a[1]:=1: a[2]:=1: for n from 1 to 35 do a[3*n]:=a[n]+a[n-1]: a[3*n+1]:=a[n]: a[3*n+2]:=a[n] od: A:=[seq(a[n],n=0..104)]; # _Emeric Deutsch_, Apr 02 2006",
				"g:=product((1+x^(3^j)+x^(2*(3^j))+x^(3*(3^j))),j=0..10): gser:=series(g,x=0,125): seq(coeff(gser,x,n),n=0..104); # _Emeric Deutsch_, Apr 02 2006",
				"# third Maple program:",
				"b:= proc(n, i) option remember; `if`(n=0, 1, `if`(i\u003c0, 0,",
				"       add(`if`(n-j*3^i\u003c0, 0, b(n-j*3^i, i-1)), j=0..3)))",
				"    end:",
				"a:= n-\u003e b(n, ilog[3](n)):",
				"seq(a(n), n=0..100);  # _Alois P. Heinz_, Jun 21 2012"
			],
			"mathematica": [
				"a[0]=1; a[1]=1; a[2]=1; For[n=1, n \u003c= 35, n++, a[3*n] = a[n] + a[n-1]; a[3*n+1] = a[n]; a[3*n+2] = a[n]]; Table[a[n], {n, 0, 104}] (* _Jean-François Alcover_, Dec 20 2016, after _Emeric Deutsch_ *)"
			],
			"xref": [
				"Cf. A002487."
			],
			"keyword": "nonn,look",
			"offset": "0,4",
			"author": "_John W. Layman_, May 09 2000",
			"references": 11,
			"revision": 33,
			"time": "2019-07-09T13:29:40-04:00",
			"created": "2000-06-15T03:00:00-04:00"
		}
	]
}