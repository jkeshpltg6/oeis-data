{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A049681",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 49681,
			"data": "0,1,2,7,20,56,152,407,1080,2851,7502,19702,51680,135461,354902,929567,2434320,6374236,16689752,43697227,114405500,299525051,784179002,2053027082,5374926720,14071792681",
			"name": "a(n) = (Lucas(2*n) - Lucas(n))/2.",
			"comment": [
				"Create a triangle with T(n,1) = L(n-1) for L a Lucas number and the other side T(n,n) = L(2*(n-1)). Interior elements are defined as T(r,c) = T(r-1,c-1) + T(r-1,c). Half the sum of the terms in row(n)=a(n) for n=1,2,3... - _J. M. Bergot_, Dec 15 2012"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A049681/b049681.txt\"\u003eTable of n, a(n) for n = 0..2380\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-3,-2,1)."
			],
			"formula": [
				"G.f.: x*(1-2*x+2*x^2)/( (1-x-x^2)*(1-3*x+x^2) ). - _R. J. Mathar_, Dec 17 2012",
				"a(n) = Lucas(n)*(Lucas(n) - 1)/2 - (-1)^n = binomial(Lucas(n), 2) - (-1)^n. - _Vladimir Reshetnikov_, Sep 27 2016",
				"E.g.f.: (1/2)*exp(-2*x/(1+sqrt(5)))*(-1 + exp(x))*(1 + exp(sqrt(5)*x)). - _Stefano Spezia_, Dec 15 2019"
			],
			"maple": [
				"Lucas:= n -\u003e combinat:-fibonacci(n+1)+combinat:-fibonacci(n-1):",
				"seq((Lucas(2*n)-Lucas(n))/2,n=0..100); # _Robert Israel_, Sep 15 2016"
			],
			"mathematica": [
				"Table[(LucasL[2n] - LucasL[n])/2, {n, 0, 20}] (* _Vladimir Reshetnikov_, Sep 15 2016 *)"
			],
			"program": [
				"(PARI) x='x+O('x^30); concat([0], Vec(x*(1-2*x+2*x^2)/((1-x-x^2)*(1-3*x+x^2)) )) \\\\ _G. C. Greubel_, Dec 02 2017",
				"(MAGMA) [(Lucas(2*n) - Lucas(n))/2: n in [0..30]]; // _G. C. Greubel_, Dec 02 2017",
				"(Sage) [(lucas_number2(2*n,1,-1) - lucas_number2(n,1,-1))/2 for n in (0..30)] # _G. C. Greubel_, Dec 15 2019",
				"(GAP) List([0..30], n-\u003e (Lucas(1,-1,2*n)[2] - Lucas(1,-1,n)[2])/2 ); # _G. C. Greubel_, Dec 15 2019"
			],
			"xref": [
				"Cf. A000032, A000045, A094292."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Clark Kimberling_",
			"ext": [
				"Corrected by _Franklin T. Adams-Watters_, Oct 25 2006",
				"Corrected by _T. D. Noe_, Nov 01 2006"
			],
			"references": 2,
			"revision": 35,
			"time": "2019-12-21T15:29:52-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}