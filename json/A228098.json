{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A228098",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 228098,
			"data": "1,2,1,3,1,2,1,1,2,1,3,2,1,1,2,2,1,3,2,1,2,1,1,3,2,1,2,1,1,4,1,2,1,3,1,2,2,1,2,2,1,4,1,2,1,2,4,2,1,1,2,1,2,2,2,2,1,3,2,1,1,4,2,1,1,2,1,3,1,1,1,2,2,2,1,1,2,1,1,2,1,3,1,2,1,1,3",
			"name": "Number of primes p \u003e prime(n) and such that prime(n)*p \u003c prime(n+1)^2.",
			"comment": [
				"For n \u003e 1, a(n)+1 is the number of composite numbers \u003c prime(n+1)^2 and removed at the n-th step of Eratosthenes's sieve. The exception for n=1 comes from prime(1)^3 = 2^3 = 8 \u003c prime(2)^2 = 9. This does not occur any more because prime(n)^3 \u003e prime(n+1)^2 for all n \u003e 1.",
				"a(n) is related to the distribution of primes around prime(n+1). High values correspond to a large gap before prime(n+1) followed by several small gaps after prime(n+1).",
				"a(n) \u003e= 1 for all n, because prime(n+1) always trivially satisfies the condition. The sequence tends to alternate high and low values, and takes its minimum value 1 about half the time.",
				"a(n) is \u003e= and almost always equal to a'(n), defined as the number of primes between prime(n+1) (inclusive) and prime(n+1) + gap(n) (inclusive), with gap(n) = prime(n+1) - prime(n) = A001223(n).",
				"An exception is 7, for which a(7) = 3, while the following prime is 11, thus gap(7) = 4, and there are only two primes between 11 and 11 + 4 = 15. It is probably the only one, as it is easily seen that a(n) = a'(n) if gap(n) \u003c= sqrt(2*prime(n)), which is a condition a little stronger than Andrica's Conjecture: gap(n) \u003c 2*sqrt(prime(n))+1. 7 is probably a record for the ratio gap(n)/sqrt(prime(n)), and the only prime for which it is \u003e sqrt(2) (see A079296 for an ordering of primes according to Andrica's conjecture)."
			],
			"link": [
				"Jean-Christophe Hervé, \u003ca href=\"/A228098/b228098.txt\"\u003eTable of n, a(n) for n = 1..9999\u003c/a\u003e",
				"C. K. Caldwell, \u003ca href=\"http://www.utm.edu/research/primes/notes/gaps.html\"\u003eGaps between primes\u003c/a\u003e.",
				"Eric W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/AndricasConjecture.html\"\u003eAndrica's Conjecture\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Andrica\u0026#39;s_conjecture\"\u003eAndrica's conjecture\u003c/a\u003e",
				"Marek Wolf, \u003ca href=\"http://arxiv.org/abs/1010.3945\"\u003eA note on the Andrica conjecture\u003c/a\u003e, arXiv:1010.3945 [math.NT], 2010."
			],
			"example": [
				"a(4)=3 because prime(4)=7, prime(5)=11, 11^2=121, and 7*11 \u003c 7*13 \u003c 7*17 \u003c 121 \u003c 7*19."
			],
			"mathematica": [
				"Table[PrimePi[Prime[n + 1]^2/Prime[n]] - n, {n, 100}] (* _T. D. Noe_, Oct 29 2013 *)"
			],
			"program": [
				"(Sage)",
				"P = Primes()",
				"def a(n):",
				"    p=P.unrank(n-1)",
				"    p1=P.unrank(n)",
				"    L=[q for q in [p+1..p1^2] if q in Primes() and p*q\u003cp1^2]",
				"    return len(L)",
				"k=100 #change k for more terms",
				"[a(m) for m in [1..k]] # _Tom Edgar_, Oct 29 2013"
			],
			"xref": [
				"Cf. A000040, A001223, A083140, A079296."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Jean-Christophe Hervé_, Oct 26 2013",
			"references": 5,
			"revision": 45,
			"time": "2021-05-24T00:16:04-04:00",
			"created": "2013-10-29T15:32:45-04:00"
		}
	]
}