{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A102086",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 102086,
			"data": "1,1,2,3,4,3,16,20,9,4,127,156,63,16,5,1363,1664,648,144,25,6,18628,22684,8703,1840,275,36,7,311250,378572,144243,29824,4200,468,49,8,6173791,7504640,2849400,582640,79775,8316,735,64,9,142190703,172785512",
			"name": "Triangular matrix, read by rows, that satisfies: T(n,k) = [T^2](n-1,k) when n\u003ek\u003e=0, with T(n,n) = (n+1).",
			"comment": [
				"Column 0 forms A082161. Column 1 forms A102087. Row sums form A102088."
			],
			"formula": [
				"T(n, 0) = A082161(n) for n\u003e0, with T(0, 0) = 1.",
				"G.f. for column k: T(k, k) = k+1 = Sum_{n\u003e=0} T(n+k, k)*x^n*prod_{j=1, n+1} (1-(j+k)*x)."
			],
			"example": [
				"Rows of T begin:",
				"[1],",
				"[1,2],",
				"[3,4,3],",
				"[16,20,9,4],",
				"[127,156,63,16,5],",
				"[1363,1664,648,144,25,6],",
				"[18628,22684,8703,1840,275,36,7],",
				"[311250,378572,144243,29824,4200,468,49,8],",
				"[6173791,7504640,2849400,582640,79775,8316,735,64,9],...",
				"Matrix square T^2 equals T excluding the main diagonal:",
				"[1],",
				"[3,4],",
				"[16,20,9],",
				"[127,156,63,16],",
				"[1363,1664,648,144,25],...",
				"G.f. for column 0: 1 = (1-x) + 1*x*(1-x)(1-2x) + 3*x^2*(1-x)(1-2x)(1-3x) + ... + T(n,0)*x^n*(1-x)(1-2x)(1-3x)*..*(1-(n+1)*x) + ...",
				"G.f. for column 1: 2 = 2(1-2x) + 4*x*(1-2x)(1-3x) + 20*x^2*(1-2x)(1-3x)(1-4x) + ... + T(n+1,1)*x^n*(1-2x)(1-3x)(1-4x)*..*(1-(n+2)*x) + ...",
				"G.f. for column 2: 3 = 3(1-3x) + 9*x*(1-3x)(1-4x) + 63*x^2*(1-3x)(1-4x)(1-5x) + ... + T(n+2,2)*x^n*(1-3x)(1-4x)(1-5x)*..*(1-(n+3)*x) + ..."
			],
			"maple": [
				"{T(n,k)=local(A=matrix(1,1),B);A[1,1]=1; for(m=2,n+1,B=matrix(m,m);for(i=1,m, for(j=1,i, if(j==i,B[i,j]=j,if(j==1,B[i,j]=(A^2)[i-1,1], B[i,j]=(A^2)[i-1,j]));));A=B);return(A[n+1,k+1])}"
			],
			"mathematica": [
				"T[n_, n_] := n+1; T[n_, k_] /; k\u003en = 0; T[n_, k_] /; k == n-1 := n^2; T[n_, k_] := T[n, k] = Coefficient[1-Sum[T[i, k]*x^i*Product[1-(j+k)*x, {j, 1, i-k+1}], {i, k, n-1}], x, n]; Table[T[n, k], {n, 0, 9}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Dec 15 2014, after PARI script *)"
			],
			"program": [
				"(PARI) {T(n,k)=if(n\u003ck,0,if(n==k,k+1, polcoeff(1-sum(i=k,n-1,T(i,k)*x^i*prod(j=1,i-k+1,1-(j+k)*x+x*O(x^n))),n)))}"
			],
			"xref": [
				"Cf. A082161, A102087, A102088.",
				"Cf. A102316."
			],
			"keyword": "nonn,tabl",
			"offset": "0,3",
			"author": "_Paul D. Hanna_, Dec 29 2004",
			"references": 11,
			"revision": 8,
			"time": "2014-12-15T05:17:46-05:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}