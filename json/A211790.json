{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A211790",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 211790,
			"data": "1,7,1,23,7,1,54,22,7,1,105,51,22,7,1,181,97,50,22,7,1,287,166,96,50,22,7,1,428,263,163,95,50,22,7,1,609,391,255,161,95,50,22,7,1,835,554,378,253,161,95,50,22,7,1,1111,756,534,374,252,161,95,50,22,7",
			"name": "Rectangular array:  R(k,n) = number of ordered triples (w,x,y) with all terms in {1,...,n} and w^k\u003cx^k+y\u003ck.",
			"comment": [
				"Row 1:  A004068",
				"Row 2:  A211635",
				"Row 3:  A211650",
				"Limiting row sequence: A002412",
				"...",
				"Let R be the array in A211790 and let R' be the array in A211793.  Then R(k,n)+R'(k,n)=3^(n-1).  Moreover, (row k of R) =(row k of A211796) for k\u003e2, by Fermat's last theorem; likewise, (row k of R')=(row k of A211799) for k\u003e2.",
				"...",
				"Generalizations:  Suppose that b,c,d are nonzero integers, and let U(k,n) be the number of ordered triples (w,x,y) with all terms in {1,...,n} and b*w*k \u003cR\u003e c*x^k+d*y^k, where the relation \u003cR\u003e is one of these: \u003c, \u003e=, \u003c=, \u003e.  What additional assumptions force the limiting row sequence to be essentially one of these: A002412, A000330, A016061, A174723, A051925?",
				"In the following guide to related arrays and sequences, U(k,n) denotes the number of (w,x,y) as described in the preceding paragraph:",
				"A211790:  w^k \u003c x^k+y^k",
				"first 3 rows: A004068, A211635, A211650",
				"limiting row sequence:  A002412",
				"A211793:  w^k \u003e= x^k+y^k",
				"first 3 rows: A000292, A211636, A211651",
				"limiting row sequence:  A000330",
				"A211796:  w^k \u003c= x^k+y^k",
				"first 3 rows: A002413, A211634, A211650",
				"limiting row sequence:  A002412",
				"A211799:  w^k \u003e x^k+y^k",
				"first 3 rows: A000292, A211637, A211651",
				"limiting row sequence:  A000330",
				"A211802:  2w^k \u003c x^k+y^k",
				"first 3 rows: A182260, A211800, A211801",
				"limiting row sequence:  A016061",
				"A211805:  2w^k \u003e= x^k+y^k",
				"first 3 rows: A055232, A211803, A211804",
				"limiting row sequence:  A000330",
				"A211808:  2w^k \u003c= x^k+y^k",
				"first 3 rows: A055232, A211806, A211807",
				"limiting row sequence:  A174723",
				"A182259:  2w^k \u003e x^k+y^k",
				"first 3 rows: A182260, A211810, A211811",
				"limiting row sequence:  A051925"
			],
			"formula": [
				"R(k,n)=n(n-1)(4n+1)/6 for 1\u003c=k\u003c=n, and",
				"R(k,n)=Sum{Sum{floor[(x^k+y^k)^(1/k)] : 1\u003c=x\u003c=n, 1\u003c=y\u003c=n}} for 1\u003c=k\u003c=n."
			],
			"example": [
				"Northwest corner:",
				"1...7...23...54...105...181...287...428...609",
				"1...7...22...51...97....166...263...391...554",
				"1...7...22...50...96....163...255...378...534",
				"1...7...22...50...95....161...253...374...528",
				"1...7...22...50...95....161...252...373...527",
				"For n=2 and k\u003e=1, the 7 triples (w,x,y) are (1,1,1), (1,1,2), (1,2,1), (1,2,2), (2,1,2), (2,2,1), (2,2,2)."
			],
			"mathematica": [
				"z = 48;",
				"t[k_, n_] := Module[{s = 0},",
				"   (Do[If[w^k \u003c x^k + y^k, s = s + 1],",
				"       {w, 1, #}, {x, 1, #}, {y, 1, #}] \u0026[n]; s)];",
				"Table[t[1, n], {n, 1, z}]  (* A004068 *)",
				"Table[t[2, n], {n, 1, z}]  (* A211635 *)",
				"Table[t[3, n], {n, 1, z}]  (* A211650 *)",
				"TableForm[Table[t[k, n], {k, 1, 12}, {n, 1, 16}]]",
				"Flatten[Table[t[k, n - k + 1], {n, 1, 12}, {k, 1, n}]] (* A211790 *)",
				"Table[n (n + 1) (4 n - 1)/6,",
				"  {n, 1, z}] (* row-limit sequence, A002412 *)",
				"(* _Peter J. C. Moses_, Apr 13 2012 *)"
			],
			"xref": [
				"Cf. A211793, A211796, A211799, A211802, A211805, A211808, A182259"
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Apr 21 2012",
			"references": 18,
			"revision": 14,
			"time": "2016-12-04T19:46:28-05:00",
			"created": "2012-04-26T14:53:40-04:00"
		}
	]
}