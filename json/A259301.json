{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A259301",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 259301,
			"data": "0,0,1,1,3,3,3,2,4,4,3,4,5,7,8,5,8,7,8,9,10,10,11,12,12,14,13,13,12,15,14,14,17,14,19,17,12,18,13,19,20,22,20,23,21,15,21,21,23,25,26,23,26,26,19,23,27,24,29,27,26,28,31,29,30,25,30,29,34,30",
			"name": "Taken over all those prime-partitionable numbers m for which there exists a 2-partition of the set of primes \u003c m that has one subset containing two primes only, a(n) is the frequency with which the smaller prime occurs, where n is the prime index.",
			"comment": [
				"A number n is called a prime partitionable number if there is a partition {P1,P2} of the primes less than n such that for any composition n1+n2=n, either there is a prime p in P1 such that p | n1 or there is a prime p in P2 such that p | n2.",
				"To demonstrate that a positive integer m is prime-partitionable, a suitable 2-partition {P1, P2} of the set of primes \u003c m must be found. In this sequence we are interested in prime-partitionable numbers such that P1 contains 2 odd primes.",
				"Conjecture: If P1 = {p1a, p1b} with p1a and p1b odd primes, p1a \u003c p1b and p1b = 2*k*p1a + 1 for some positive integer k such that 2*k \u003c= p1a - 3 and if m = p1a + p1b then m is prime-partitionable."
			],
			"link": [
				"Christopher Hunt Gribble, \u003ca href=\"/A259301/b259301.txt\"\u003eTable of n, a(n) for n = 1..9592\u003c/a\u003e"
			],
			"example": [
				"The table below shows all p1a and p1b pairs for p1a \u003c= 29 that demonstrate that m is prime-partitionable.",
				". n    p1a    p1b     2k      m",
				". 3      5     11      2     16",
				". 4      7     29      4     36",
				". 5     11     23      2     34",
				".       11     67      6     78",
				".       11     89      8    100",
				". 6     13     53      4     66",
				".       13     79      6     92",
				".       13    131     10    144",
				". 7     17    103      6    120",
				".       17    137      8    154",
				".       17    239     14    256",
				". 8     19    191     10    210",
				".       19    229     12    248",
				". 9     23     47      2     70",
				".       23    139      6    162",
				".       23    277     12    300",
				".       23    461     20    484",
				".10     29     59      2     88",
				".       29    233      8    262",
				".       29    349     12    378",
				".       29    523     18    552",
				"By examining the p1a column it can be seen that",
				"a(1) = 0, a(2) = 0, a(3) = 1, a(4) = 1, a(5) = 3, a(6) = 3,",
				"a(7) = 3, a(8) = 2, a(9) = 4, a(10) = 4."
			],
			"maple": [
				"# Makes use of conjecture in COMMENTS section.",
				"ppgen := proc (ub)",
				"  local freq_p1a, i, j, k, nprimes, p1a, p1b, pless;",
				"  # Construct set of primes \u003c ub in pless.",
				"  pless := {};",
				"  for i from 3 to ub do",
				"    if isprime(i) then",
				"      pless := `union`(pless, {i});",
				"    end if",
				"  end do;",
				"  nprimes := numelems(pless);",
				"  # Determine frequency of each p1a.",
				"  printf(\"0, \");    # For prime 2.",
				"  for j to nprimes do",
				"    p1a := pless[j];",
				"    freq_p1a := 0;",
				"    for k to (p1a-3)/2 do",
				"      p1b := 2*k*p1a+1;",
				"      if isprime(p1b) then",
				"        freq_p1a := freq_p1a+1;",
				"      end if;",
				"    end do;",
				"    printf(\"%d, \", freq_p1a);",
				"  end do;",
				"end proc:",
				"ub := 1000:",
				"ppgen(ub):"
			],
			"xref": [
				"Cf. A059756, A245664."
			],
			"keyword": "nonn",
			"offset": "1,5",
			"author": "_Christopher Hunt Gribble_, Jun 23 2015",
			"references": 1,
			"revision": 16,
			"time": "2015-08-27T10:58:55-04:00",
			"created": "2015-08-27T10:58:55-04:00"
		}
	]
}