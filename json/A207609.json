{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A207609",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 207609,
			"data": "1,1,3,1,8,3,1,15,17,3,1,24,54,26,3,1,35,130,120,35,3,1,48,265,398,213,44,3,1,63,483,1071,909,333,53,3,1,80,812,2492,3074,1744,480,62,3,1,99,1284,5208,8802,7138,2984,654,71,3,1,120,1935,10020,22230,24408,14370,4710,855,80,3",
			"name": "Triangle of coefficients of polynomials v(n,x) jointly generated with A207608; see Formula section.",
			"comment": [
				"Subtriangle of the triangle given by (1, 0, 2/3, 1/3, 0, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (0, 3, -2, 0, 0, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Mar 03 2012"
			],
			"formula": [
				"u(n,x)=u(n-1,x)+v(n-1,x),",
				"v(n,x)=2x*u(n-1,x)+(x+1)v(n-1,x),",
				"where u(1,x)=1, v(1,x)=1.",
				"T(n,k) = 2*T(n-1,k) + T(n-1,k-1) + T(n-2,k-1) - T(n-2,k), n\u003e2. - _Philippe Deléham_, Mar 03 2012",
				"Sum_{k, 0\u003c=k\u003c=n, n\u003e=1} T(n,k)*x^k = A000012(n), A052156(n-1), A048876(n-1) for x = 0, 1, 2 respectively. - _Philippe Deléham_, Mar 03 2012",
				"G.f.: -(1-x+2*x*y)*x*y/(-1+2*x+x*y+x^2*y-x^2). - _R. J. Mathar_, Aug 11 2015"
			],
			"example": [
				"First five rows:",
				"1",
				"1...3",
				"1...8....3",
				"1...15...17...3",
				"1...24...54...26...3",
				"Triangle (1, 0, 2/3, 1/3, 0, 0, 0, ...) DELTA (0, 3, -2, 0, 0, 0, ...) begins :",
				"1",
				"1, 0",
				"1, 3, 0",
				"1, 8, 3, 0",
				"1, 15, 17, 3, 0",
				"1, 24, 54, 26, 3, 0",
				"1, 35, 130, 120, 35, 3, 0"
			],
			"mathematica": [
				"u[1, x_] := 1; v[1, x_] := 1; z = 16;",
				"u[n_, x_] := u[n - 1, x] + v[n - 1, x]",
				"v[n_, x_] := 2 x*u[n - 1, x] + (x + 1) v[n - 1, x]",
				"Table[Factor[u[n, x]], {n, 1, z}]",
				"Table[Factor[v[n, x]], {n, 1, z}]",
				"cu = Table[CoefficientList[u[n, x], x], {n, 1, z}];",
				"TableForm[cu]",
				"Flatten[%]    (* A207608 *)",
				"Table[Expand[v[n, x]], {n, 1, z}]",
				"cv = Table[CoefficientList[v[n, x], x], {n, 1, z}];",
				"TableForm[cv]",
				"Flatten[%]    (* A207609 *)"
			],
			"program": [
				"(Python)",
				"from sympy import Poly",
				"from sympy.abc import x",
				"def u(n, x): return 1 if n==1 else u(n - 1, x) + v(n - 1, x)",
				"def v(n, x): return 1 if n==1 else 2*x*u(n - 1, x) + (x + 1)*v(n - 1, x)",
				"def a(n): return Poly(v(n, x), x).all_coeffs()[::-1]",
				"for n in range(1, 13): print(a(n)) # _Indranil Ghosh_, May 28 2017"
			],
			"xref": [
				"Cf. A207608."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Clark Kimberling_, Feb 19 2012",
			"references": 3,
			"revision": 20,
			"time": "2020-04-13T09:32:25-04:00",
			"created": "2012-02-19T21:12:03-05:00"
		}
	]
}