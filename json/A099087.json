{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A099087",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 99087,
			"data": "1,2,2,0,-4,-8,-8,0,16,32,32,0,-64,-128,-128,0,256,512,512,0,-1024,-2048,-2048,0,4096,8192,8192,0,-16384,-32768,-32768,0,65536,131072,131072,0,-262144,-524288,-524288,0,1048576,2097152,2097152,0,-4194304,-8388608,-8388608,0,16777216",
			"name": "Expansion of 1/(1 - 2*x + 2*x^2).",
			"comment": [
				"Yet another variation on A009545.",
				"Row sums of Krawtchouk triangle A098593. Partial sums of e.g.f. exp(x)cos(x), or 2^(n/2)cos(Pi*n/2). See A009116.",
				"Binomial transform of A057077. - _R. J. Mathar_, Nov 04 2008",
				"Partial sums of A146559. - _Philippe Deléham_, Dec 01 2008",
				"Pisano period lengths: 1, 1, 8, 1, 4, 8, 24, 1, 24, 4, 40, 8, 12, 24, 8, 1, 16, 24, 72, 4, ... - _R. J. Mathar_, Aug 10 2012",
				"Also the inverse Catalan transform of A000079. - _Arkadiusz Wesolowski_, Oct 26 2012"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A099087/b099087.txt\"\u003eTable of n, a(n) for n = 0..5000\u003c/a\u003e",
				"Karl Dilcher and Maciej Ulas, \u003ca href=\"https://arxiv.org/abs/2008.13475\"\u003eDivisibility and Arithmetic Properties of a Class of Sparse Polynomials\u003c/a\u003e, arXiv:2008.13475 [math.NT], 2020. See Table 1, 1st column, p. 3.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,-2)."
			],
			"formula": [
				"E.g.f.: exp(x)*(cos(x) + sin(x)).",
				"a(n) = 2^(n/2)*(cos(Pi*n/4) + sin(Pi*n/4)).",
				"a(n) = Sum_{k=0..n} Sum_{i=0..k} binomial(n-k, k-i)*binomial(n, i) *(-1)^(k-i).",
				"a(n) = 2*(a(n-1) - a(n-2)).",
				"From _R. J. Mathar_, Apr 18 2008: (Start)",
				"a(n) = (1-i)^(n-1) + (1+i)^(n-1) where i=sqrt(-1).",
				"a(n) = 2 Sum_{k=0..(n-1)/2} (-1)^k*binomial(n-1,2k) if n\u003e0. (End)",
				"a(n) = Sum_{k=0..n} A109466(n,k)*2^k. - _Philippe Deléham_, Oct 28 2008",
				"E.g.f.: (cos(x)+sin(x))*exp(x) = G(0); G(k)=1+2*x/(4*k+1-x*(4*k+1)/(2*(2*k+1)+x-2*(x^2)*(2*k+1)/((x^2)-(2*k+2)*(4*k+3)/G(k+1)))); (continued fraction). - _Sergei N. Gladkovskii_, Nov 26 2011",
				"G.f.: U(0) where U(k)= 1 + x*(k+3) - x*(k+1)/U(k+1) ; (continued fraction, 1-step). - _Sergei N. Gladkovskii_, Oct 10 2012",
				"a(n) = Re((1+i)^n) + Im((1+i)^n) where i = sqrt(-1) = A146559(n) + A009545(n). - _Philippe Deléham_, Feb 13 2013",
				"a(n) = Sum_{j=0..n} binomial(n, j)*(-1)^binomial(j, 2); this is the case m=2 and z=-1 of f(m,n)(z) = Sum_{j=0..n} binomial(n, j)*z^binomial(j, m). See Dilcher and Ulas. - _Michel Marcus_, Sep 01 2020"
			],
			"mathematica": [
				"CoefficientList[Series[1/(1 -2x +2x^2), {x, 0, 50}], x] (* _Michael De Vlieger_, Dec 24 2015 *)"
			],
			"program": [
				"(Sage) [lucas_number1(n,2,2) for n in range(1, 50)] # _Zerinvary Lajos_, Apr 23 2009",
				"(PARI) x='x+O('x^50); Vec(1/(1-2*x+2*x^2)) \\\\ _Altug Alkan_, Dec 24 2015",
				"(MAGMA) I:=[1,2]; [n le 2 select I[n] else 2*(Self(n-1) - Self(n-2)): n in [1..50]]; // _G. C. Greubel_, Mar 16 2019",
				"(GAP) a:=[1,2];; for n in [3..50] do a[n]:=2*a[n-1]-2*a[n-2]; od; a; # _G. C. Greubel_, Mar 16 2019"
			],
			"xref": [
				"Cf. A009545, A146559."
			],
			"keyword": "easy,sign",
			"offset": "0,2",
			"author": "_Paul Barry_, Sep 24 2004",
			"ext": [
				"Signs added by _N. J. A. Sloane_, Nov 14 2006"
			],
			"references": 20,
			"revision": 64,
			"time": "2020-09-01T17:07:10-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}