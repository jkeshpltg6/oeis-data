{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A334640",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 334640,
			"data": "0,0,9,19,72,324,1595,8307,44982,250648,1427679,8274825,48644310,289334160,1738043892,10529070020,64252519830,394601627376,2437058926871,15126463230165,94306717535940,590318477063700,3708527622652755,23374587898663155,147770791807427880",
			"name": "a(n) is the total number of down steps between the 2nd and 3rd up steps in all 2-Dyck paths of length 3*n. A 2-Dyck path is a nonnegative lattice path with steps (1, 2), (1, -1) that starts and ends at y = 0.",
			"comment": [
				"For n = 2, there is no 3rd up step, a(2) = 9 enumerates the total number of down steps between the 2nd up step and the end of the path."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A334640/b334640.txt\"\u003eTable of n, a(n) for n = 0..1212\u003c/a\u003e",
				"A. Asinowski, B. Hackl, and S. Selkirk, \u003ca href=\"https://arxiv.org/abs/2007.15562\"\u003eDown step statistics in generalized Dyck paths\u003c/a\u003e, arXiv:2007.15562 [math.CO], 2020."
			],
			"formula": [
				"a(0) = a(1) = 0 and a(n) = 2*Sum_{j=1..2} binomial(3*j+1,j) * binomial(3*(n-j),n-j) / ((3*j+1)*(n-j+1)) for n \u003e 1."
			],
			"example": [
				"For n = 2, there are the 2-Dyck paths UUDDDD, UDUDDD, UDDUDD. Between the 2nd up step and the end of the path there are a(2) = 4 + 3 + 2 = 9 down steps in total."
			],
			"maple": [
				"b:= proc(x, y, u, c) option remember; `if`(x=0, c,",
				"     `if`(y+2\u003cx, b(x-1, y+2, min(u+1,3), c), 0)+",
				"     `if`(y\u003e0, b(x-1, y-1, u, c+`if`(u=2, 1, 0)), 0))",
				"    end:",
				"a:= n-\u003e b(3*n, 0$3):",
				"seq(a(n), n=0..24);  # _Alois P. Heinz_, May 09 2020",
				"# second Maple program:",
				"a:= proc(n) option remember; `if`(n\u003c3, [0$2, 9][n+1],",
				"     (3*(n-1)*(3*n-8)*(3*n-7)*(13*n-20)*a(n-1))/",
				"     (2*(13*n-33)*(n-2)*(2*n-3)*n))",
				"    end:",
				"seq(a(n), n=0..24);  # _Alois P. Heinz_, May 09 2020"
			],
			"mathematica": [
				"a[0] = a[1] = 0; a[n_] := 2 * Sum[Binomial[3*j + 1, j] * Binomial[3*(n - j), n - j]/((3*j + 1)*(n - j + 1)), {j, 1, 2}]; Array[a, 25, 0] (* _Amiram Eldar_, May 09 2020 *)"
			],
			"program": [
				"(PARI) a(n) = if (n\u003c=1, 0, 2*sum(j=1, 2, binomial(3*j+1,j) * binomial(3*(n-j),n-j)/((3*j+1)*(n-j+1)))); \\\\ _Michel Marcus_, May 09 2020"
			],
			"xref": [
				"Cf. A007226, A007228, A124724, A334641, A334642, A334682."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Benjamin Hackl_, May 07 2020",
			"references": 4,
			"revision": 30,
			"time": "2020-08-08T01:42:37-04:00",
			"created": "2020-05-09T21:14:35-04:00"
		}
	]
}