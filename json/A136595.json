{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A136595",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 136595,
			"data": "1,0,1,0,-1,1,0,7,-3,1,0,-61,31,-6,1,0,751,-375,85,-10,1,0,-11821,5911,-1350,185,-15,1,0,226927,-113463,26341,-3710,350,-21,1,0,-5142061,2571031,-603246,87381,-8610,602,-28,1,0,134341711,-67170855,15887845,-2346330,240051,-17766",
			"name": "Matrix inverse of triangle A136590.",
			"comment": [
				"A136590 is the triangle of trinomial logarithmic coefficients.",
				"Column 1 is signed A048287, which is the number of semiorders on n labeled nodes whose incomparability graph is connected.",
				"The Bell transform of (-1)^n*A048287(n+1). For the definition of the Bell transform see A264428. - _Peter Luschny_, Jan 18 2016"
			],
			"formula": [
				"T(n,k) = Sum_{i=0..n-1} (-1)^i * (k+i)! * Stirling2(n,k+i) * Catalan(k,i)/k!, where Stirling2(n,k) = A008277(n,k); Catalan(k,i) = C(2i+k,i)*k/(2i+k) = coefficient of x^i in C(x)^k with C(x) = (1-sqrt(1-4x))/(2x)."
			],
			"example": [
				"Triangle begins:",
				"1;",
				"0, 1;",
				"0, -1, 1;",
				"0, 7, -3, 1;",
				"0, -61, 31, -6, 1;",
				"0, 751, -375, 85, -10, 1;",
				"0, -11821, 5911, -1350, 185, -15, 1;",
				"0, 226927, -113463, 26341, -3710, 350, -21, 1;",
				"0, -5142061, 2571031, -603246, 87381, -8610, 602, -28, 1;",
				"0, 134341711, -67170855, 15887845, -2346330, 240051, -17766, 966, -36, 1; ..."
			],
			"maple": [
				"# The function BellMatrix is defined in A264428.",
				"BellMatrix(n -\u003e (-1)^n*A048287(n+1), 9); # _Peter Luschny_, Jan 27 2016"
			],
			"mathematica": [
				"BellMatrix[f_Function, len_] := With[{t = Array[f, len, 0]}, Table[BellY[n, k, t], {n, 0, len - 1}, {k, 0, len - 1}]];",
				"rows = 11;",
				"M = BellMatrix[Sum[(-1)^(k+1) k! StirlingS2[#+1, k] CatalanNumber[k-1], {k, 1, #+1}]\u0026, rows];",
				"Table[M[[n, k]], {n, 1, rows}, {k, 1, n}] // Flatten (* _Jean-François Alcover_, Jun 24 2018, after _Peter Luschny_ *)"
			],
			"program": [
				"(PARI) {T(n,k) = if(n\u003ck||k\u003c0,0,if(n==k,1,if(k==0,0,n!/(k-1)!* sum(i=0,n-1,(-1)^i * polcoeff(((exp(x + x*O(x^n)) - 1)^(k+i)),n) * binomial(2*i+k,i)/(2*i+k)))))}",
				"for(n=0,10,for(k=0,n,print1(T(n,k),\", \"));print(\"\"))",
				"(PARI) /* Define Stirling2: */",
				"{Stirling2(n,k) = n!*polcoeff(((exp(x+x*O(x^n))-1)^k)/k!,n)}",
				"/* Define Catalan(m,n) = [x^n] C(x)^m: */",
				"{CATALAN(m,n) = binomial(2*n+m,n) * m/(2*n+m)}",
				"/* Define this triangle: */",
				"{T(n,k) = if(n\u003ck||k\u003c0,0,if(n==k,1,if(k==0,0, sum(i=0,n-1,(-1)^i*(k+i)!*Stirling2(n,k+i) * CATALAN(k,i)/k!))))}",
				"for(n=0,10,for(k=0,n,print1(T(n,k),\", \"));print(\"\"))",
				"(Sage) # uses[bell_matrix from A264428]",
				"bell_matrix(lambda n: (-1)^n*A048287(n+1), 10) # _Peter Luschny_, Jan 18 2016"
			],
			"xref": [
				"Cf. columns: A048287, A136596, A136597; A136590 (matrix inverse); A136588, A136589."
			],
			"keyword": "sign,tabl",
			"offset": "0,8",
			"author": "_Paul D. Hanna_, Jan 10 2008",
			"references": 6,
			"revision": 19,
			"time": "2020-03-23T17:29:58-04:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}