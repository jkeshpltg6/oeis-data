{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A062892",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 62892,
			"data": "1,1,0,0,1,0,0,0,0,1,1,0,0,0,0,0,1,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,1,0,0,1,0,0,1,0,0,0,0,0,0,0,0,1,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,2,0,0,0,0",
			"name": "Number of squares that can be obtained by permuting the digits of n.",
			"comment": [
				"The original definition was ambiguous (it did not specify how repeated digits or leading zeros are to be handled). Here is a precise version (based on the Mathematica code):",
				"Suppose the decimal expansion of n has d digits. Apply all d! permutations, discard duplicates, but keep any with leading zeros; now ignore leading zeros; a(n) is the number of squares on the resulting list. For example, if n = 100 we end up with 100, 010, 001, and both 100 and 1 are squares, so a(100)=2. If n=108 we get 6 numbers but only (0)81 is a square, so a(108)=1. - _N. J. A. Sloane_, Jan 16 2014"
			],
			"link": [
				"David A. Corneth, \u003ca href=\"/A062892/b062892.txt\"\u003eTable of n, a(n) for n = 0..9999\u003c/a\u003e"
			],
			"formula": [
				"a(A096600(n))=0; a(A007937(n))\u003e0; a(A096599(n))=1; a(A096598(n))\u003e1. - _Reinhard Zumkeller_, Jun 29 2004"
			],
			"example": [
				"a(169) = 3; the squares obtained by permuting the digits are 169, 196, 961."
			],
			"mathematica": [
				"Table[t1=Table[FromDigits[k],{k,Permutations[IntegerDigits[n]]}]; p=Length[Select[t1,IntegerQ[Sqrt[#]]\u0026]], {n,0,104}] (* _Jayanta Basu_, May 17 2013 *)"
			],
			"program": [
				"(PARI) a(n) = {my(d = vecsort(digits(n)), res = 0); forperm(d, p, res += issquare(fromdigits(Vec(p)))); res } \\\\ _David A. Corneth_, Oct 18 2021",
				"(Python)",
				"from math import isqrt",
				"from sympy.utilities.iterables import multiset_permutations as mp",
				"def sqr(n): return isqrt(n)**2 == n",
				"def a(n):",
				"    s = str(n)",
				"    perms = (int(\"\".join(p)) for p in mp(s, len(s)))",
				"    return len(set(p for p in perms if sqr(p)))",
				"print([a(n) for n in range(105)] ) # _Michael S. Branicky_, Oct 18 2021"
			],
			"xref": [
				"A096599 gives the squares k^2 such that a(k^2) = 1."
			],
			"keyword": "base,nonn,easy",
			"offset": "0,101",
			"author": "_Amarnath Murthy_, Jun 29 2001",
			"ext": [
				"Corrected and extended by Larry Reeves (larryr(AT)acm.org), Jul 02 2001"
			],
			"references": 6,
			"revision": 26,
			"time": "2021-10-19T23:51:16-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}