{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A294746",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 294746,
			"data": "1,1,1,1,1,1,1,1,3,1,1,1,10,13,1,1,1,35,217,75,1,1,1,126,4245,8317,525,1,1,1,462,90376,1239823,487630,4347,1,1,1,1716,2019836,216456376,709097481,40647178,41245,1,1,1,6435,46570140,41175714454,1303699790001,701954099115,4561368175,441675,1",
			"name": "Number A(n,k) of compositions (ordered partitions) of 1 into exactly k*n+1 powers of 1/(k+1); square array A(n,k), n\u003e=0, k\u003e=0, read by antidiagonals.",
			"comment": [
				"Row r \u003e= 2, is asymptotic to r^(r*n + 3/2) / (2*Pi*n)^((r-1)/2). - _Vaclav Kotesovec_, Sep 20 2019"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A294746/b294746.txt\"\u003eAntidiagonals n = 0..45, flattened\u003c/a\u003e"
			],
			"formula": [
				"A(n,k) = [x^((k+1)^n)] (Sum_{j=0..k*n+1} x^((k+1)^j))^(k*n+1) for k\u003e0, A(n,0) = 1."
			],
			"example": [
				"A(3,1) = 13: [1/4,1/4,1/4,1/4], [1/2,1/4,1/8,1/8], [1/2,1/8,1/4,1/8], [1/2,1/8,1/8,1/4], [1/4,1/2,1/8,1/8], [1/4,1/8,1/2,1/8], [1/4,1/8,1/8,1/2], [1/8,1/2,1/4,1/8], [1/8,1/2,1/8,1/4], [1/8,1/4,1/2,1/8], [1/8,1/4,1/8,1/2], [1/8,1/8,1/2,1/4], [1/8,1/8,1/4,1/2].",
				"Square array A(n,k) begins:",
				"  1,   1,      1,         1,             1,                1, ...",
				"  1,   1,      1,         1,             1,                1, ...",
				"  1,   3,     10,        35,           126,              462, ...",
				"  1,  13,    217,      4245,         90376,          2019836, ...",
				"  1,  75,   8317,   1239823,     216456376,      41175714454, ...",
				"  1, 525, 487630, 709097481, 1303699790001, 2713420774885145, ..."
			],
			"maple": [
				"b:= proc(n, r, p, k) option remember;",
				"      `if`(n\u003cr, 0, `if`(r=0, `if`(n=0, p!, 0), add(",
				"       b(n-j, k*(r-j), p+j, k)/j!, j=0..min(n, r))))",
				"    end:",
				"A:= (n, k)-\u003e `if`(k=0, 1, b(k*n+1, 1, 0, k+1)):",
				"seq(seq(A(n, d-n), n=0..d), d=0..10);"
			],
			"mathematica": [
				"b[n_, r_, p_, k_] := b[n, r, p, k] = If[n \u003c r, 0, If[r == 0, If[n == 0, p!, 0], Sum[b[n - j, k*(r - j), p + j, k]/j!, {j, 0, Min[n, r]}]]];",
				"A[n_, k_] := If[k == 0, 1, b[k*n + 1, 1, 0, k + 1]];",
				"Table[A[n, d - n], {d, 0, 10}, {n, 0, d}] // Flatten (* _Jean-François Alcover_, Apr 30 2018, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Columns k=0-10 give: A000012, A007178(n+1), A294850, A294851, A294852, A294853, A294854, A294855, A294856, A294857, A294858.",
				"Rows n=0+1, 2-10 give: A000012, A001700, A294982, A294983, A294984, A294985, A294986, A294987, A294988, A294989.",
				"Main diagonal gives: A294747.",
				"Cf. A294775."
			],
			"keyword": "nonn,tabl",
			"offset": "0,9",
			"author": "_Alois P. Heinz_, Nov 07 2017",
			"references": 21,
			"revision": 27,
			"time": "2019-09-20T05:13:09-04:00",
			"created": "2017-11-09T15:48:06-05:00"
		}
	]
}