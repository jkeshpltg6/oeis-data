{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A309282",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 309282,
			"data": "5,1,5,4,2,7,3,1,7,8,0,2,5,8,7,9,9,6,2,4,9,2,8,3,5,5,3,9,1,1,3,3,4,1,9,5,5,2,8,7,9,7,2,2,3,5,7,0,8,6,6,1,8,2,0,7,2,9,7,2,0,0,0,2,0,5,3,9,4,3,8,1,1,3,6,1,1,0,4,6,2,2,8,4,7,8,5",
			"name": "Decimal expansion of the circumference of a golden ellipse with a unit semi-major axis.",
			"comment": [
				"A golden ellipse is an ellipse inscribed in a golden rectangle. The concept of a golden ellipse was introduced by H. E. Huntley in 1970.",
				"The aesthetic preferences of rectangles and ellipses with relation to the golden ratio were studied by Gustav Fechner in 1876. His results for ellipses were published by Witmer in 1893.",
				"A golden ellipse with a semi-major axis 1 has a minor semi-axis 1/phi and an eccentricity 1/sqrt(phi), where phi is the golden ratio (A001622)."
			],
			"link": [
				"H. E. Huntley, \u003ca href=\"https://archive.org/details/divineproportion0000hunt\"\u003eThe Divine Proportion: A Study in Mathematical Beauty\u003c/a\u003e, Dover, New York, 1970, page 65.",
				"H. E. Huntley, \u003ca href=\"https://www.fq.math.ca/Scanned/12-1/huntley1.pdf\"\u003eThe Golden Ellipse\u003c/a\u003e, The Fibonacci Quarterly, Vol. 12, No. 1 (1974), pp. 38-40.",
				"Thomas Koshy, \u003ca href=\"https://doi.org/10.1002/9781118033067.ch26\"\u003eThe Golden Ellipse and Hyperbola\u003c/a\u003e, in the book Fibonacci and Lucas Numbers with Applications, Wiley, 2001, chapter 26.",
				"M. C. Monzingo, \u003ca href=\"https://www.fq.math.ca/Scanned/14-5/monzingo1.pdf\"\u003eA Note on the Golden Ellipse\u003c/a\u003e, The Fibonacci Quarterly, Vol. 14, No. 5 (1974), p. 388.",
				"A. D. Rawlins, \u003ca href=\"http://www.jstor.org/stable/3620006\"\u003eA Note on the Golden Ratio\u003c/a\u003e, The Mathematical Gazette, Vol. 79, No. 484 (1995), p. 104.",
				"Stanislav Sýkora, \u003ca href=\"http://dx.doi.org/10.3247/SL2Math08.001\"\u003eMathematical Constants\u003c/a\u003e, Stan's Library, Vol.II.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"https://mathworld.wolfram.com/Ellipse.html\"\u003eEllipse\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Ellipse#Circumference\"\u003eEllipse\u003c/a\u003e.",
				"Lightner Witmer, \u003ca href=\"http://echo.mpiwg-berlin.mpg.de/MPIWG:BBE1NSH8\"\u003eZur experimentellen Aesthetik einfacher räumlicher Formverhältnisse\u003c/a\u003e, Philosophische Studien, Vol. 9 (1893), pp. 96-144."
			],
			"formula": [
				"Equals 4*E(1/phi), where E(x) is the complete elliptic integral of the second kind."
			],
			"example": [
				"5.154273178025879962492835539113341955287972235708661..."
			],
			"mathematica": [
				"RealDigits[4 * EllipticE[1/GoldenRatio], 10, 100][[1]]"
			],
			"xref": [
				"Cf. A001622 (phi), A094881 (area of the golden ellipse), A197762 (eccentricity of the golden ellipse).",
				"Similar sequences: A138500, A274014."
			],
			"keyword": "nonn,cons",
			"offset": "1,1",
			"author": "_Amiram Eldar_, Jul 05 2020",
			"references": 1,
			"revision": 32,
			"time": "2020-07-05T23:36:58-04:00",
			"created": "2020-07-05T23:36:58-04:00"
		}
	]
}