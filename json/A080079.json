{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A080079",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 80079,
			"data": "1,2,1,4,3,2,1,8,7,6,5,4,3,2,1,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,64,63,62,61,60,59,58,57,56,55,54,53,52,51,50,49,48,47",
			"name": "Least number causing the longest carry sequence when adding numbers \u003c= n to n in binary representation.",
			"comment": [
				"T(n,k) \u003c T(n,a(n)) = A070940(n) for 1 \u003c= k \u003c a(n) and T(n,k) \u003c= T(n,a(n)) for a(n) \u003c= k \u003c= n, where T is defined as in A080080.",
				"a(n) gives the distance from n to the nearest 2^t \u003e n. - _Ctibor O. Zizka_, Apr 09 2020"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A080079/b080079.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"formula": [
				"From _Benoit Cloitre_, Feb 22 2003: (Start)",
				"a(n) = A004755(n) - 2*n.",
				"a(n) = -n + 2*2^floor(log(n)/log(2)). (End)",
				"From _Ralf Stephan_, Jun 02 2003: (Start)",
				"a(n) = n iff n = 2^k, otherwise a(n) = A035327(n-1).",
				"a(n) = A062383(n) - n. (End)",
				"a(0) = 0, a(2*n) = 2*a(n), a(2*n+1) = 2*a(n)-1 + 2*[n==0]. - _Ralf Stephan_, Jan 04 2004",
				"a(n) = A240769(n,1); A240769(n, a(n)) = 1. - _Reinhard Zumkeller_, Apr 13 2014",
				"a(n) = n + 1 - A006257(n). - _Reinhard Zumkeller_, Apr 14 2014"
			],
			"maple": [
				"# _Alois P. Heinz_ observes in A327489:",
				"A080079 := n -\u003e 1 + Bits:-Nor(n, n):",
				"# Likewise:",
				"A080079 := n -\u003e 1 + Bits:-Nand(n, n):",
				"seq(A080079(n), n=1..81); # _Peter Luschny_, Sep 23 2019"
			],
			"mathematica": [
				"Flatten@Table[Nest[Most[RotateRight[#]] \u0026, Range[n], n - 1], {n, 30}] (* _Birkas Gyorgy_, Feb 07 2011 *)",
				"Table[FromDigits[(IntegerDigits[n, 2] /. {0 -\u003e 1, 1 -\u003e 0}), 2] +",
				"1, {n, 30}] (* _Birkas Gyorgy_, Feb 07 2011 *)",
				"Table[BitXor[n, 2^IntegerPart[Log[2, n] + 1] - 1] + 1, {n, 30}] (* _Birkas Gyorgy_, Feb 07 2011 *)",
				"Table[2 2^Floor[Log[2, n]] - n, {n, 30}] (* _Birkas Gyorgy_, Feb 07 2011 *)",
				"Flatten@Table[Reverse@Range[2^n], {n, 0, 4}] (* _Birkas Gyorgy_, Feb 07 2011 *)"
			],
			"program": [
				"(Haskell)",
				"a080079 n = (length $ takeWhile (\u003c a070940 n) (a080080_row n)) + 1",
				"-- _Reinhard Zumkeller_, Apr 22 2013",
				"(MAGMA) [-n+2*2^Floor(Log(n)/Log(2)): n in [1..80]]; // _Vincenzo Librandi_, Dec 01 2016"
			],
			"xref": [
				"Cf. A327489, A004755, A062383, A080080, A240769, A006257."
			],
			"keyword": "nonn,base",
			"offset": "1,2",
			"author": "_Reinhard Zumkeller_, Jan 26 2003",
			"references": 22,
			"revision": 41,
			"time": "2020-09-26T18:49:29-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}