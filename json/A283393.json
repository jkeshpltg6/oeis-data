{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A283393",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 283393,
			"data": "1,10,1,2,5,2,5,2,1,10,1,10,1,2,5,2,5,2,1,10,1,10,1,2,5,2,5,2,1,10,1,10,1,2,5,2,5,2,1,10,1,10,1,2,5,2,5,2,1,10,1,10,1,2,5,2,5,2,1,10,1,10,1,2,5,2,5,2,1,10,1,10,1,2,5,2,5,2,1,10,1,10,1,2,5,2,5,2,1,10",
			"name": "a(n) = gcd(n^2-1, n^2+9).",
			"comment": [
				"Periodic with period 10.",
				"Similar sequences with formula gcd(n^2-1, n^2+k):",
				"k= 1:  1,  2, 1, 2, 1,  2, 1,  2, 1,  2, 1,  2, 1, ...  (A000034)",
				"k= 3:  1,  4, 1, 4, 1,  4, 1,  4, 1,  4, 1,  4, 1, ...  (A010685)",
				"k= 5:  1,  6, 3, 2, 3,  6, 1,  6, 3,  2, 3,  6, 1, ...  (A129203, start 6)",
				"k= 7:  1,  8, 1, 8, 1,  8, 1,  8, 1,  8, 1,  8, 1, ...  (A010689)",
				"k= 9:  1, 10, 1, 2, 5,  2, 5,  2, 1, 10, 1, 10, 1, ...  (this sequence)",
				"k=11:  1, 12, 3, 4, 3, 12, 1, 12, 3,  4, 3, 12, 1, ...  (A129197, start 12)",
				"Connection between the values of a(n) and the last digit of n:",
				". if n ends with 0, 2 or 8, then a(n) = 1;",
				". if n ends with 1 or 9, then a(n) = 10;",
				". if n ends with 3, 5 or 7, then a(n) = 2;",
				". if n ends with 4 or 6, then a(n) = 5.",
				"Also, continued fraction expansion of (57 + sqrt(4579))/114."
			],
			"link": [
				"\u003ca href=\"/index/Rec#order_10\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,0,0,0,0,0,0,0,0,1)."
			],
			"formula": [
				"G.f.: (1 + 10*x + x^2 + 2*x^3 + 5*x^4 + 2*x^5 + 5*x^6 + 2*x^7 + x^8 + 10*x^9)/(1 - x^10).",
				"a(n) = (1/75)*(74*(n mod 10) - 61*((n+1) mod 10) + 14*((n+2) mod 10) + 29*((n+3) mod 10) - 16*((n+4) mod 10) + 29*((n+5) mod 10) - 16*((n+6) mod 10) - ((n+7) mod 10) + 74*((n+8) mod 10) - 61*((n+9) mod 10)). - _Paolo P. Lava_, Mar 08 2017"
			],
			"maple": [
				"P:=proc(q) gcd(q^2-1,q^2+9); end: seq(P(i),i=0..200); # _Paolo P. Lava_, Mar 08 2017"
			],
			"mathematica": [
				"Table[PolynomialGCD[n^2 - 1, n^2 + 9], {n, 0, 100}]",
				"LinearRecurrence[{0, 0, 0, 0, 0, 0, 0, 0, 0, 1}, {1, 10, 1, 2, 5, 2, 5, 2, 1, 10}, 100]"
			],
			"program": [
				"(Python) [1, 10, 1, 2, 5, 2, 5, 2, 1, 10]*10",
				"(Sage) [gcd(n^2-1, n^2+9) for n in range(100)]",
				"(MAGMA) \u0026cat [[1, 10, 1, 2, 5, 2, 5, 2, 1, 10]^^10];",
				"(Maxima) makelist(gcd(n^2-1, n^2+9), n, 0, 100);",
				"(PARI) Vec((1 + 10*x + x^2 + 2*x^3 + 5*x^4 + 2*x^5 + 5*x^6 + 2*x^7 + x^8 + 10*x^9)/(1 - x^10) + O(x^100)) \\\\ _Colin Barker_, Mar 08 2017"
			],
			"xref": [
				"Cf. A000034, A010685, A010689, A129197, A129203."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Bruno Berselli_, Mar 07 2017",
			"references": 5,
			"revision": 30,
			"time": "2019-12-07T12:18:28-05:00",
			"created": "2017-03-09T09:56:10-05:00"
		}
	]
}