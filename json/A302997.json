{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A302997",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 302997,
			"data": "1,1,1,1,3,1,1,5,5,1,1,7,13,7,1,1,9,33,29,9,1,1,11,89,123,49,11,1,1,13,221,425,257,81,13,1,1,15,485,1343,1281,515,113,15,1,1,17,953,4197,5913,3121,925,149,17,1,1,19,1713,12435,23793,16875,6577,1419,197,19,1,1,21,2869,33809,88273,84769,42205,11833,2109,253,21,1",
			"name": "Square array A(n,k), n \u003e= 0, k \u003e= 0, read by antidiagonals: A(n,k) = [x^(n^2)] theta_3(x)^k/(1 - x), where theta_3() is the Jacobi theta function.",
			"comment": [
				"A(n,k) is the number of integer lattice points inside the k-dimensional hypersphere of radius n."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A302997/b302997.txt\"\u003eTable of n, a(n) for n = 0..1274\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/JacobiThetaFunctions.html\"\u003eJacobi Theta Functions\u003c/a\u003e",
				"\u003ca href=\"/index/Su#ssq\"\u003eIndex entries for sequences related to sums of squares\u003c/a\u003e"
			],
			"formula": [
				"A(n,k) = [x^(n^2)] (1/(1 - x))*(Sum_{j=-infinity..infinity} x^(j^2))^k."
			],
			"example": [
				"Square array begins:",
				"  1,   1,   1,    1,     1,      1,  ...",
				"  1,   3,   5,    7,     9,     11,  ...",
				"  1,   5,  13,   33,    89,    221,  ...",
				"  1,   7,  29,  123,   425,   1343,  ...",
				"  1,   9,  49,  257,  1281,   5913,  ...",
				"  1,  11,  81,  515,  3121,  16875,  ..."
			],
			"mathematica": [
				"Table[Function[k, SeriesCoefficient[EllipticTheta[3, 0, x]^k/(1 - x), {x, 0, n^2}]][j - n], {j, 0, 11}, {n, 0, j}] // Flatten",
				"Table[Function[k, SeriesCoefficient[1/(1 - x) Sum[x^i^2, {i, -n, n}]^k, {x, 0, n^2}]][j - n], {j, 0, 11}, {n, 0, j}] // Flatten"
			],
			"program": [
				"(PARI) T(n,k)={if(k==0, 1, polcoef(((1 + 2*sum(j=1, n, x^(j^2)) + O(x*x^(n^2)))^k)/(1-x), n^2))} \\\\ _Andrew Howroyd_, Sep 14 2019"
			],
			"xref": [
				"Columns k=0..10 give A000012, A005408, A000328, A000605, A055410, A055411, A055412, A055413, A055414, A055415, A055416.",
				"Rows k=0..10 give A000012, A005408, A055426, A055427, A055428, A055429, A055430, A055431, A055432, A055433, A055434.",
				"Main diagonal gives A302861.",
				"Cf. A000122, A122510, A302996, A302998."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Ilya Gutkovskiy_, Apr 17 2018",
			"references": 21,
			"revision": 13,
			"time": "2019-09-29T08:39:09-04:00",
			"created": "2018-04-28T17:13:35-04:00"
		}
	]
}