{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A227739",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 227739,
			"data": "1,1,1,2,2,2,1,1,1,1,2,3,3,3,1,2,2,1,1,1,1,2,2,2,2,3,1,1,2,1,3,4,4,4,1,3,3,1,1,2,2,2,3,3,2,2,2,2,1,1,1,1,1,1,2,2,2,3,3,3,3,4,1,2,3,1,1,1,2,2,2,3,2,4,1,1,3,1,4,5,5,5,1,4,4,1,1",
			"name": "Irregular table where row n lists in nondecreasing order the parts of unordered partition encoded in the runlengths of binary expansion of n; nonzero terms of A227189.",
			"comment": [
				"Row n has A005811(n) elements. Each row contains a unique (unordered) partition of some integer, and all possible partitions of finite natural numbers eventually occur. The first partition that sums to k occurs at row A227368(k) and the last at row A000225(k).",
				"Other similar tables of unordered partitions: A036036, A036037, A080576, A080577 and A112798."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A227739/b227739.txt\"\u003eThe rows 1..1023 of the table, flattened\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A227189(A227737(n),A227740(n))."
			],
			"example": [
				"Rows are constructed as:",
				"  Row    n in   Runlengths  With one     Partial sums   The row sums",
				"   n    binary  collected   subtracted   of which give  to, i.e. is",
				"                from lsb-   from all     terms on       a partition of",
				"                to msb-end  except 1st   that row       of A227183(n)",
				"   1       \"1\"        [1]        [1]     1;             1",
				"   2      \"10\"      [1,1]      [1,0]     1, 1;          2",
				"   3      \"11\"        [2]        [2]     2;             2",
				"   4     \"100\"      [2,1]      [2,0]     2, 2;          4",
				"   5     \"101\"    [1,1,1]    [1,0,0]     1, 1, 1;       3",
				"   6     \"110\"      [1,2]      [1,1]     1, 2;          3",
				"   7     \"111\"        [3]        [3]     3;             3",
				"   8    \"1000\"      [3,1]      [3,0]     3, 3;          6",
				"   9    \"1001\"    [1,2,1]    [1,1,0]     1, 2, 2;       5",
				"  10    \"1010\"  [1,1,1,1]  [1,0,0,0]     1, 1, 1, 1;    4",
				"  11    \"1011\"    [2,1,1]    [2,0,0]     2, 2, 2;       6",
				"  12    \"1100\"      [2,2]      [2,1]     2, 3;          5",
				"  13    \"1101\"    [1,1,2]    [1,0,1]     1, 1, 2;       4",
				"  14    \"1110\"      [1,3]      [1,2]     1, 3;          4",
				"  15    \"1111\"        [4]        [4]     4;             4",
				"  16   \"10000\"      [4,1]      [4,0]     4, 4;          8"
			],
			"mathematica": [
				"Table[Function[b, Accumulate@ Prepend[If[Length@ b \u003e 1, Rest[b] - 1, {}], First@ b]]@ Map[Length, Split@ Reverse@ IntegerDigits[n, 2]], {n, 34}] // Flatten (* _Michael De Vlieger_, May 09 2017 *)"
			],
			"program": [
				"(Scheme) (define (A227739 n) (A227189bi (A227737 n) (A227740 n))) ;; The Scheme-code for A227189bi has been given in A227189."
			],
			"xref": [
				"Row sums: A227183, row products: A227184, the initial (smallest) term of each row: A136480, the last (largest) term: A227185.",
				"Cf. also A227189, A227738, A227736."
			],
			"keyword": "nonn,base,tabf",
			"offset": "1,4",
			"author": "_Antti Karttunen_, Jul 25 2013",
			"references": 15,
			"revision": 25,
			"time": "2020-01-20T21:42:17-05:00",
			"created": "2013-08-12T03:59:52-04:00"
		}
	]
}