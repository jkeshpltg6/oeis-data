{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A156146",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 156146,
			"data": "1,1,2,6,1,3,58,11,2,4,5829,1056,16,2,5,58292915,10555528,1608,21,3,6,5829291479146458,1055552805277764,16080804,2111,27,3,7,58292914791464577914645739573229,10555528052777640527776402638882",
			"name": "Table T(m,n) = round( c(m,n)/2 ), where c(m,n) is the concatenation of all preceding terms in row m, T(m,1)...T(m,n-1) and T(m,1)=m.",
			"comment": [
				"Originally, round( c/2 ) was formulated as \"rank of c in the sequence of odd resp. even (positive) numbers\". Each of the rows has some characteristics reminiscent of Thue-Morse type sequences.",
				"It is interesting that the number of digits of T(1,k) for k\u003e2 equals to 2^(k-3). And for i\u003e1 \u0026 k\u003e1 [and i\u003c20 - _M. F. Hasler_] the number of digits of T(i,k) equals to 2^(k-2). - _Farideh Firoozbakht_"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A156146/b156146.txt\"\u003eTable of n, a(n) for n=1..78\u003c/a\u003e",
				"E. Angelini, \u003ca href=\"http://www.cetteadressecomportecinquantesignes.com/ThueMorseRank.htm\"\u003eRang dans les Pairs/Impairs\u003c/a\u003e",
				"E. Angelini, \u003ca href=\"/A156146/a156146.pdf\"\u003eRang dans les Pairs/Impairs\u003c/a\u003e [Cached copy, with permission]",
				"E. Angelini et al., \u003ca href=\"http://list.seqfan.eu/pipermail/seqfan/2009-February/000740.html\"\u003eRank of n in the Odd/Even sequence\u003c/a\u003e and follow-up messages on the SeqFan list, Feb 03 2009"
			],
			"example": [
				"T(2,2) = 1 since T(2,1) = 2 is the first even number. T(2,3) = 11 since concat(T(2,1),T(2,2)) = 21 is the 11th odd number.",
				"Table begins:",
				"  1, 1,  6,   58,     5829,         58292915, ...",
				"  2, 1, 11, 1056, 10555528, 1055552805277764, ...",
				"  3, 2, 16, 1608, 16080804, 1608080408040402, ...",
				"  4, 2, 21, 2111, 21106056, 2110605560553028, ...",
				"  5, 3, 27, 2664, 26636332, 2663633213318166, ...",
				"  6, 3, 32, 3166, 31661583, 3166158315830792, ..."
			],
			"maple": [
				"rank:= n-\u003e `if`(irem(n,2)=0, n/2, (n+1)/2); a:= proc(n,k) option remember; if n=1 then k else rank(parse(cat(seq(a(j,k), j=1..n-1)))) fi end; seq(seq(a(d-k,k), k=1..d-1), d=1..10); # _Alois P. Heinz_"
			],
			"mathematica": [
				"Si[1]=i;Si[n_]:=Si[n]=(v={};Do[v= Join[v,IntegerDigits[Si[k]]],{k,n-1}]; Floor[(1+FromDigits[v])/2]) (* _Farideh Firoozbakht_ *)"
			],
			"program": [
				"(PARI) T(m,n)={ local(t=round(m/2)); n\u003e1 | return(m); while( n--\u003e1, t=round(1/2*m=eval(Str(m,t)))); t }",
				"A156146=concat( vector( 12,d,vector( d,k, T(k,d-k+1)))) /* _M. F. Hasler_ */"
			],
			"xref": [
				"Cf. A156147 (first row of the table)."
			],
			"keyword": "base,easy,nonn,tabl",
			"offset": "1,3",
			"author": "_Eric Angelini_, _Alois P. Heinz_, _Farideh Firoozbakht_ and _M. F. Hasler_, Feb 04 2009",
			"ext": [
				"Typos fixed by _Charles R Greathouse IV_, Oct 28 2009"
			],
			"references": 4,
			"revision": 34,
			"time": "2018-10-04T20:07:39-04:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}