{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A246720",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 246720,
			"data": "1,1,0,1,1,0,1,0,1,0,1,1,1,1,0,1,0,2,0,1,0,1,1,0,2,1,1,0,1,0,1,1,3,0,1,0,1,1,0,2,0,3,1,1,0,1,0,1,0,2,0,4,0,1,0,1,0,1,1,1,2,1,4,1,1,0,1,1,0,1,2,0,3,0,5,0,1,0,1,1,2,0,1,2,0,3,0,5,1,1,0",
			"name": "Number A(n,k) of partitions of n into parts of the k-th list of distinct parts in the order given by A246688; square array A(n,k), n\u003e=0, k\u003e=0, read by antidiagonals.",
			"comment": [
				"The first lists of distinct parts in the order given by A246688 are: 0:[], 1:[1], 2:[2], 3:[1,2], 4:[3], 5:[1,3], 6:[4], 7:[1,4], 8:[2,3], 9:[5], 10:[1,2,3], 11:[1,5], 12:[2,4], 13:[6], 14:[1,2,4], 15:[1,6], 16:[2,5], 17:[3,4], 18:[7], 19:[1,2,5], 20:[1,3,4], ... ."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A246720/b246720.txt\"\u003eAntidiagonals n = 0..140, flattened\u003c/a\u003e"
			],
			"example": [
				"Square array A(n,k) begins:",
				"  1, 1, 1, 1, 1, 1, 1, 1, 1, 1,  1, 1, 1, 1,  1, ...",
				"  0, 1, 0, 1, 0, 1, 0, 1, 0, 0,  1, 1, 0, 0,  1, ...",
				"  0, 1, 1, 2, 0, 1, 0, 1, 1, 0,  2, 1, 1, 0,  2, ...",
				"  0, 1, 0, 2, 1, 2, 0, 1, 1, 0,  3, 1, 0, 0,  2, ...",
				"  0, 1, 1, 3, 0, 2, 1, 2, 1, 0,  4, 1, 2, 0,  4, ...",
				"  0, 1, 0, 3, 0, 2, 0, 2, 1, 1,  5, 2, 0, 0,  4, ...",
				"  0, 1, 1, 4, 1, 3, 0, 2, 2, 0,  7, 2, 2, 1,  6, ...",
				"  0, 1, 0, 4, 0, 3, 0, 2, 1, 0,  8, 2, 0, 0,  6, ...",
				"  0, 1, 1, 5, 0, 3, 1, 3, 2, 0, 10, 2, 3, 0,  9, ...",
				"  0, 1, 0, 5, 1, 4, 0, 3, 2, 0, 12, 2, 0, 0,  9, ...",
				"  0, 1, 1, 6, 0, 4, 0, 3, 2, 1, 14, 3, 3, 0, 12, ..."
			],
			"maple": [
				"b:= proc(n, i) b(n, i):= `if`(n=0, [[]], `if`(i\u003en, [],",
				"      [map(x-\u003e[i, x[]], b(n-i, i+1))[], b(n, i+1)[]]))",
				"    end:",
				"f:= proc() local i, l; i, l:=0, [];",
				"      proc(n) while n\u003e=nops(l)",
				"        do l:=[l[], b(i, 1)[]]; i:=i+1 od; l[n+1]",
				"      end",
				"    end():",
				"g:= proc(n, l) option remember; `if`(n=0, 1, `if`(l=[], 0,",
				"      add(g(n-l[-1]*j, subsop(-1=NULL, l)), j=0..n/l[-1])))",
				"    end:",
				"A:= (n, k)-\u003e g(n, f(k)):",
				"seq(seq(A(n, d-n), n=0..d), d=0..16);"
			],
			"mathematica": [
				"b[n_, i_] := b[n, i] = If[n == 0, {{}}, If[i \u003e n, {}, Join[Prepend[#, i]\u0026 /@ b[n - i, i + 1], b[n, i + 1]]]];",
				"f = Module[{i = 0, l = {}}, Function[n, While[ n \u003e= Length[l], l = Join[l, b[i, 1]]; i++ ]; l[[n + 1]]]];",
				"g[n_, l_] := g[n, l] = If[n == 0, 1, If[l == {}, 0, Sum[g[n - l[[-1]] j, ReplacePart[l, -1 -\u003e Nothing]], {j, 0, n/l[[-1]]}]]];",
				"A[n_, k_] := g[n, f[k]];",
				"Table[Table[A[n, d - n], {n, 0, d}], {d, 0, 16}] // Flatten (* _Jean-François Alcover_, Dec 07 2020, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Columns k=0-11, 13-20, 23 give: A000007, A000012, A059841, A008619, A079978, A008620, A121262, A008621, A103221, A079998, A001399, A002266(n+5), A079979, A008642, A097992, A008616, A008679, A082784, A000115, A025767, A008676.",
				"Main diagonal gives A246721.",
				"Cf. A246688, A246690 (the same for compositions)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,18",
			"author": "_Alois P. Heinz_, Sep 02 2014",
			"references": 3,
			"revision": 18,
			"time": "2020-12-07T10:07:50-05:00",
			"created": "2014-09-02T05:11:54-04:00"
		}
	]
}