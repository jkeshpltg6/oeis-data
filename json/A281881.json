{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A281881",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 281881,
			"data": "1,2,6,3,18,36,4,36,144,240,5,60,360,1200,1800,6,90,720,3600,10800,15120,7,126,1260,8400,37800,105840,141120,8,168,2016,16800,100800,423360,1128960,1451520",
			"name": "Triangle read by rows: T(n,k) (n\u003e=1, 2\u003c=k\u003c=n+1) is the number of k-sequences of balls colored with at most n colors such that exactly one ball is of a color seen previously in the sequence.",
			"comment": [
				"Number of k-sequences of balls colored with at most n colors such that exactly two balls are the same color as some other ball in the sequence (necessarily each other, _Jeremy Dover_, Sep 26 2017"
			],
			"link": [
				"Jeremy Dover, \u003ca href=\"/A281881/b281881.txt\"\u003eTable of n, a(n) for n = 1..999\u003c/a\u003e",
				"Jeremy Dover, \u003ca href=\"http://math.stackexchange.com/a/2109013/342250\"\u003eAnswer to Cumulative Distribution Function of Collision Counts\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = binomial(k,2)*n!/(n+1-k)!",
				"T(n,k) = n*T(n-1,k-1) + (k-1)*n!/(n+1-k)!"
			],
			"example": [
				"n=1 =\u003e AA -\u003e T(1,2) = 1.",
				"n=2 =\u003e AA, BB -\u003e T(2,2) = 2; AAB, ABA, BAA, BBA, BAB, ABB -\u003e T(2,3) = 6.",
				"Triangle starts:",
				"   1",
				"   2,   6",
				"   3,  18,   36",
				"   4,  36,  144,   240",
				"   5,  60,  360,  1200,   1800",
				"   6,  90,  720,  3600,  10800,   15120",
				"   7, 126, 1260,  8400,  37800,  105840,   141120",
				"   8, 168, 2016, 16800, 100800,  423360,  1128960,  1451520",
				"   9, 216, 3024, 30240, 226800, 1270080,  5080320, 13063680,  16329600",
				"  10, 270, 4320, 50400, 453600, 3175200, 16934400, 65318400, 163296000, 199584000"
			],
			"mathematica": [
				"Table[Binomial[k, 2] n!/(n + 1 - k)!, {n, 8}, {k, 2, n + 1}] // Flatten (* _Michael De Vlieger_, Feb 02 2017 *)"
			],
			"xref": [
				"Columns of table:",
				"  T(n,2) = A000027(n)",
				"  T(n,3) = A028896(n)",
				"Other sequences in table:",
				"  T(n,n+1) = A001286(n)",
				"  T(n,n) = A001804(n), n\u003e=2"
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Jeremy Dover_, Feb 01 2017",
			"references": 3,
			"revision": 25,
			"time": "2017-09-27T09:30:59-04:00",
			"created": "2017-02-03T09:22:50-05:00"
		}
	]
}