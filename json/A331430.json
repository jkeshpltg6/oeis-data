{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A331430",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 331430,
			"data": "-1,-1,2,-1,6,-6,-1,12,-30,20,-1,20,-90,140,-70,-1,30,-210,560,-630,252,-1,42,-420,1680,-3150,2772,-924,-1,56,-756,4200,-11550,16632,-12012,3432,-1,72,-1260,9240,-34650,72072,-84084,51480,-12870,-1,90,-1980,18480,-90090,252252,-420420,411840,-218790,48620,-1,110,-2970,34320,-210210,756756,-1681680,2333760,-1969110,923780,-184756",
			"name": "Triangle read by rows: T(n, k) = (-1)^(k+1)*binomial(n,k)*binomial(n+k,k) (n \u003e= k \u003e= 0).",
			"comment": [
				"This is Table I of Ser (1933), page 92.",
				"From _Petros Hadjicostas_, Jul 09 2020: (Start)",
				"Essentially Ser (1933) in his book (and in particular for Tables I-IV) finds triangular arrays that allow him to express the coefficients of various kinds of series in terms of the coefficients of other series.",
				"He uses Newton's series (or some variation of it), factorial series, and inverse factorial series. Unfortunately, he uses unusual notation, and as a result it is difficult to understand what he is actually doing.",
				"Rivoal (2008, 2009) essentially uses factorial series and transformations to other kinds of series to provide new proofs of the irrationality of log(2), zeta(2), and zeta(3). As a result, the triangular array T(n,k) appears in various parts of his papers.",
				"We believe Table I (p. 92) in Ser (1933), regarding the numbers T(n,k), corresponds to four different formulas. We have deciphered the first two of them. (End)"
			],
			"reference": [
				"J. Ser, Les Calculs Formels des Séries de Factorielles. Gauthier-Villars, Paris, 1933, pp. 92-93."
			],
			"link": [
				"A. Buhl, \u003ca href=\"https://www.e-periodica.ch/digbib/view?pid=ens-001:1933:32::554#554\"\u003eBook review: J. Ser - Les calculs formels des séries de factorielles\u003c/a\u003e, L'Enseignement Mathématique, 32 (1933), p. 275.",
				"L. A. MacColl, \u003ca href=\"https://projecteuclid.org/euclid.bams/1183498080\"\u003eReview: J. Ser, Les calculs formels des séries de factorielles\u003c/a\u003e, Bull. Amer. Math. Soc., 41(3) (1935), p. 174.",
				"L. M. Milne-Thomson, \u003ca href=\"https://www.jstor.org/stable/3605643\"\u003eReview of Les calculs formels des séries de factorielles. By J. Ser. Pp. vii, 98. 20 fr. 1933. (Gauthier-Villars)\u003c/a\u003e, The Mathematical Gazette, Vol. 18, No. 228 (May, 1934), pp. 136-137.",
				"J. Ser, \u003ca href=\"/A002720/a002720.pdf\"\u003eLes Calculs Formels des Séries de Factorielles\u003c/a\u003e (Annotated scans of some selected pages)",
				"Tanguy Rivoal, \u003ca href=\"http://rivoal.perso.math.cnrs.fr/articles/lagrange.pdf\"\u003eApplications arithmétiques de l'interpolation lagrangienne\u003c/a\u003e, preprint (2008); see pp. 1 and 15.",
				"Tanguy Rivoal, \u003ca href=\"https://doi.org/10.1142/S1793042109001992\"\u003eApplications arithmétiques de l'interpolation lagrangienne\u003c/a\u003e, Int. J. Number Theory 5.2 (2009), 185-208; see pp. 185 and 199."
			],
			"formula": [
				"T(n,k) can also be written as (-1)^(k+1)*(n+k)!/(k!*k!*(n-k)!).",
				"From _Petros Hadjicostas_, Jul 09 2020: (Start)",
				"Ser's first formula from his Table I (p. 92) is the following:",
				"Sum_{k=0..n} T(n,k)*k!/(x*(x+1)*...*(x+k)) = -(x-1)*(x-2)*...*(x-n)/(x*(x+1)*...*(x+n)).",
				"As a result, Sum_{k=0..n} T(n,k)/binomial(m+k, k) = 0 for m = 1..n.",
				"Ser's second formula from his Table I appears also in Rivoal (2008, 2009) in a slightly different form:",
				"Sum_{k=0..n} T(n,k)/(x + k) = (-1)^(n+1)*(x-1)*(x-2)*...*(x-n)/(x*(x+1)*...*(x+n)).",
				"As a result, for m = 1..n, Sum_{k=0..n} T(n,k)/(m + k) = 0. (End)",
				"T(n,k) = (-1)^(k+1)*FallingFactorial(n+k,2*k)/(k!)^2. - _Peter Luschny_, Jul 09 2020",
				"From _Petros Hadjicostas_, Jul 10 2020: (Start)",
				"_Peter Luschny_'s formula above is essentially the way the numbers T(n,k) appear in Eq. (7) on p. 86 of Ser's (1933) book. Eq. (7) is essentially equivalent to the first formula above (related to Table I on p. 92).",
				"By inverting that formula (in some way), he gets",
				"n!/(x*(x+1)*...*(x+n)) = Sum_{p=0..n} (-1)^p*(2*p+1)*f_p(n+1)*f_p(x), where f_p(x) = (x-1)*...*(x-p)/(x*(x+1)*...*(x+p)). This is equivalent to Eq. (8) on p. 86 of Ser's book.",
				"The rational coefficients A(n,p) = (2*p+1)*f_p(n+1) = (2*p+1)*(n*(n-1)*...*(n+1-p))/((n+1)*...*(n+1+p)) appear in Table II on p. 92 of Ser's book.",
				"If we consider the coefficients T(n,k) and (-1)^(p+1)*A(n,p) as infinite lower triangular matrices, then they are inverses of one another (see the example below). This means that, for m \u003e= s,",
				"Sum_{k=s..m} T(m,k)*(-1)^(s+1)*A(k,s) = I(s=m) = Sum_{k=s..m} (-1)^(k+1)*A(m,k)*T(k,s), where I(s=m) = 1, if s = m, and = 0, otherwise.",
				"Without the (-1)^p, we get the formula",
				"1/(x+n) = Sum_{p=0..n} (2*p+1)*f_p(n+1)*f_p(x),",
				"which apparently is the inversion of the second of Ser's formulas (related to Table I on p. 92).",
				"In all of the above formulas, an empty product is by definition 1, so f_0(x) = 1/x. (End)"
			],
			"example": [
				"Triangle T(n,k) (with rows n \u003e= 0 and columns k=0..n) begins:",
				"  -1;",
				"  -1,  2;",
				"  -1,  6,   -6;",
				"  -1, 12,  -30,   20;",
				"  -1, 20,  -90,  140,    -70;",
				"  -1, 30, -210,  560,   -630,   252;",
				"  -1, 42, -420, 1680,  -3150,  2772,   -924;",
				"  -1, 56, -756, 4200, -11550, 16632, -12012, 3432;",
				"  ...",
				"From _Petros Hadjicostas_, Jul 11 2020: (Start)",
				"Its inverse (from Table II, p. 92) is",
				"  -1;",
				"  -1/2, 1/2;",
				"  -1/3, 1/2,   -1/6;",
				"  -1/4, 9/20,  -1/4,  1/20;",
				"  -1/5, 2/5,   -2/7,  1/10, -1/70;",
				"  -1/6, 5/14, -25/84, 5/36, -1/28,  1/252;",
				"  -1/7, 9/28, -25/84, 1/6,  -9/154,  1/84, -1/924;",
				"   ... (End)"
			],
			"mathematica": [
				"Table[CoefficientList[-Hypergeometric2F1[-n, n + 1, 1, x], x], {n, 0, 9}] // Flatten (* _Georg Fischer_, Jan 18 2020 after Peter Luschny in A063007 *)"
			],
			"program": [
				"(MAGMA) /* As triangle: */ [[(-1)^(k+1) * Factorial(n+k) / (Factorial(k) * Factorial(k) * Factorial(n-k)): k in [0..n]]: n in [0.. 10]]; // _Vincenzo Librandi_, Jan 19 2020",
				"(SageMath)",
				"def T(n,k): return (-1)^(k+1)*falling_factorial(n+k,2*k)/factorial(k)^2",
				"flatten([[T(n,k) for k in (0..n)] for n in (0..10)]) # _Peter Luschny_, Jul 09 2020"
			],
			"xref": [
				"A063007 is the same triangle without the minus signs, and has much more information.",
				"Columns 1 and 2 are A002378 and A033487; the last three diagonals are A002544, A002457, A000984.",
				"Cf. A331431, A331432."
			],
			"keyword": "sign,tabl",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_, Jan 17 2020",
			"ext": [
				"Thanks to _Bob Selcoe_, who noticed a typo in one of the entries, which, when corrected, led to an explicit formula for the whole of Ser's Table I."
			],
			"references": 6,
			"revision": 99,
			"time": "2020-07-12T03:34:15-04:00",
			"created": "2020-01-17T17:42:22-05:00"
		}
	]
}