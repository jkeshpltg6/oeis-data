{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A072590",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 72590,
			"data": "1,1,1,1,4,1,1,12,12,1,1,32,81,32,1,1,80,432,432,80,1,1,192,2025,4096,2025,192,1,1,448,8748,32000,32000,8748,448,1,1,1024,35721,221184,390625,221184,35721,1024,1,1,2304,139968,1404928,4050000,4050000",
			"name": "Table T(n,k) giving number of spanning trees in complete bipartite graph K(n,k), read by antidiagonals.",
			"reference": [
				"J. W. Moon, Counting Labeled Trees, Issue 1 of Canadian mathematical monographs, Canadian Mathematical Congress, 1970.",
				"R. P. Stanley, Enumerative Combinatorics, Cambridge, Vol. 2, 1999; see Exercise 5.66."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A072590/b072590.txt\"\u003eAntidiagonals d=1..50, flattened\u003c/a\u003e",
				"H. I. Scoins, \u003ca href=\"https://doi.org/10.1017/S0305004100036173\"\u003eThe number of trees with nodes of alternate parity\u003c/a\u003e, Proc. Cambridge Philos. Soc. 58 (1962) 12-16.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CompleteBipartiteGraph.html\"\u003eComplete Bipartite Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/SpanningTree.html\"\u003eSpanning Tree\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = n^(k-1) * k^(n-1).",
				"E.g.f.: A(x,y) - 1, where: A(x,y) = exp( x*exp( y*A(x,y) ) ) = Sum_{n\u003e=0} Sum_{k=0..n} (n-k)^k * (k+1)^(n-k-1) * x^(n-k)/(n-k)! * y^k/k!. - _Paul D. Hanna_, Jan 22 2019"
			],
			"example": [
				"From _Andrew Howroyd_, Oct 29 2019: (Start)",
				"Array begins:",
				"============================================================",
				"n\\k | 1   2     3       4        5         6           7",
				"----+-------------------------------------------------------",
				"  1 | 1   1     1       1        1         1           1 ...",
				"  2 | 1   4    12      32       80       192         448 ...",
				"  3 | 1  12    81     432     2025      8748       35721 ...",
				"  4 | 1  32   432    4096    32000    221184     1404928 ...",
				"  5 | 1  80  2025   32000   390625   4050000    37515625 ...",
				"  6 | 1 192  8748  221184  4050000  60466176   784147392 ...",
				"  7 | 1 448 35721 1404928 37515625 784147392 13841287201 ...",
				"  ...",
				"(End)"
			],
			"mathematica": [
				"t[n_, k_] := n^(k-1) * k^(n-1); Table[ t[n-k+1, k], {n, 1, 10}, {k, 1, n}] // Flatten (* _Jean-François Alcover_, Feb 21 2013 *)"
			],
			"program": [
				"(PARI) {T(n, k) = if( n\u003c1 || k\u003c1, 0, n^(k-1) * k^(n-1))}"
			],
			"xref": [
				"Columns 2..3 are A001787, A069996.",
				"Main diagonal is A068087.",
				"Antidiagonal sums are A132609.",
				"Cf. A070285, A328887, A328888."
			],
			"keyword": "nonn,tabl,easy,nice",
			"offset": "1,5",
			"author": "_Michael Somos_, Jun 23 2002",
			"ext": [
				"Scoins reference from _Philippe Deléham_, Dec 22 2003"
			],
			"references": 10,
			"revision": 38,
			"time": "2019-10-29T21:06:25-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}