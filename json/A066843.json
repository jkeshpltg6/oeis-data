{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A066843",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 66843,
			"data": "1,2,4,12,24,96,192,768,2304,9216,18432,110592,221184,884736,3538944,17694720,35389440,212336640,424673280,2548039680,10192158720,40768634880,81537269760,652298158080,1956894474240,7827577896960,31310311587840",
			"name": "a(n) = Product_{k=1..n} d(k); d(k) is the number of positive divisors of k.",
			"comment": [
				"a(n) is also the determinant of the symmetric n X n matrix M defined by M(i,j) = d_3(gcd(i,j)) for 1 \u003c= i,j \u003c= n, where d_3(n) is A007425. - _Enrique Pérez Herrero_, Aug 12 2011",
				"a(n) is the number of integer sequences of length n where a(m) divides m for every term. - _Franklin T. Adams-Watters_, Oct 29 2017"
			],
			"link": [
				"Harry J. Smith, \u003ca href=\"/A066843/b066843.txt\"\u003eTable of n, a(n) for n = 1..200\u003c/a\u003e",
				"Antal Bege, \u003ca href=\"http://www.emis.de/journals/AUSM/C1-1/MATH1-4.PDF\"\u003eHadamard product of GCD matrices\u003c/a\u003e, Acta Univ. Sapientiae, Mathematica, 1, 1 (2009) 43-49",
				"Mathoverflow, \u003ca href=\"https://mathoverflow.net/questions/208827/product-of-tauk\"\u003eProduct of tau(k)\u003c/a\u003e, 2015.",
				"Ramanujan's Papers, \u003ca href=\"https://web.archive.org/web/20200124035942/http://ramanujan.sirinudi.org/Volumes/published/ram17.html\"\u003eSome formulas in the analytic theory of numbers\u003c/a\u003e, Messenger of Mathematics, XLV, 1916, 81-84, Formula (10)."
			],
			"formula": [
				"a(n) = product{p=primes\u003c=n} product{1\u003c=k\u003c=log(n)/log(p)} (1 +1/k)^floor(n/p^k). - _Leroy Quet_, Mar 20 2007"
			],
			"maple": [
				"with(numtheory):seq(mul(tau(k),k=1..n), n=1..26); # _Zerinvary Lajos_, Jan 11 2009",
				"with(numtheory):a[1]:=1: for n from 2 to 26 do a[n]:=a[n-1]*tau(n) od: seq(a[n], n=1..26); # _Zerinvary Lajos_, Mar 21 2009"
			],
			"mathematica": [
				"A066843[n_] := Product[DivisorSigma[0,i], {i,1,n}]; Array[A066843,20] (* _Enrique Pérez Herrero_, Aug 12 2011 *)",
				"FoldList[Times, Array[DivisorSigma[0, #] \u0026, 27]] (* _Michael De Vlieger_, Nov 01 2017 *)"
			],
			"program": [
				"(PARI) { p=1; for (n=1, 200, p*=length(divisors(n)); write(\"b066843.txt\", n, \" \", p) ) } \\\\ _Harry J. Smith_, Apr 01 2010"
			],
			"xref": [
				"Cf. A000005, A001088, A066780."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Leroy Quet_, Jan 20 2002",
			"references": 14,
			"revision": 49,
			"time": "2021-06-09T07:52:13-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}