{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A319840",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 319840,
			"data": "1,2,2,3,4,3,4,6,6,4,5,8,8,8,5,6,10,10,10,10,6,7,12,12,12,12,12,7,8,14,14,14,14,14,14,8,9,16,16,16,16,16,16,16,9,10,18,18,18,18,18,18,18,18,10,11,20,20,20,20,20,20,20,20,20,11,12,22,22,22",
			"name": "Table read by antidiagonals: T(n, k) is the number of elements on the perimeter of an n X k matrix.",
			"comment": [
				"The table T(n, k) can be indifferently read by ascending or descending antidiagonals."
			],
			"formula": [
				"T(n, k) = n*k - (n - 2)*(k - 2)*H(min(n, k) - 3), where H(x) is the Heaviside step function, taking H(0) = 1.",
				"G.f. as rectangular array: (x*y - x^3*y^3)/((-1 + x)^2*(-1 + y)^2).",
				"X(n, k) = A131821(n, k)*A318274(n - 1, k)*A154325(n - 1, k). - _Franck Maminirina Ramaharo_, Nov 18 2018"
			],
			"example": [
				"The table T starts in row n=1 with columns k \u003e= 1 as:",
				"   1   2   3   4   5   6   7   8   9  10 ...",
				"   2   4   6   8  10  12  14  16  18  20 ...",
				"   3   6   8  10  12  14  16  18  20  22 ...",
				"   4   8  10  12  14  16  18  20  22  24 ...",
				"   5  10  12  14  16  18  20  22  24  26 ...",
				"   6  12  14  16  18  20  22  24  26  28 ...",
				"   7  14  16  18  20  22  24  26  28  30 ...",
				"   8  16  18  20  22  24  26  28  30  32 ...",
				"   9  18  20  22  24  26  28  30  32  34 ...",
				"  10  20  22  24  26  28  30  32  34  36 ...",
				"  ...",
				"The triangle X(n, k) begins",
				"  n\\k|   1   2   3   4   5   6   7   8   9  10",
				"  ---+----------------------------------------",
				"   1 |   1",
				"   2 |   2   2",
				"   3 |   3   4   3",
				"   4 |   4   6   6   4",
				"   5 |   5   8   8   8   5",
				"   6 |   6  10  10  10  10   6",
				"   7 |   7  12  12  12  12  12   7",
				"   8 |   8  14  14  14  14  14  14   8",
				"   9 |   9  16  16  16  16  16  16  16   9",
				"  10 |  10  18  18  18  18  18  18  18  18  10",
				"  ..."
			],
			"maple": [
				"a := (n, k) -\u003e (n+1-k)*k-(n-1-k)*(k-2)*(limit(Heaviside(min(n+1-k, k)-3+x), x = 0, right)): seq(seq(a(n, k), k = 1 .. n), n = 1 .. 20)"
			],
			"mathematica": [
				"Flatten[Table[(n + 1 - k) k-(n-1-k)*(k-2)Limit[HeavisideTheta[Min[n+1-k,k]-3+x], x-\u003e0, Direction-\u003e\"FromAbove\"  ],{n, 20}, {k, n}]] (* or *)",
				"f[n_] := Table[SeriesCoefficient[(x y - x^3 y^3)/((-1 + x)^2 (-1 + y)^2), {x, 0, i + 1 - j}, {y, 0, j}], {i, n, n}, {j, 1, n}]; Flatten[Array[f,20]]"
			],
			"program": [
				"(MAGMA)",
				"[[k lt 3 or n+1-k lt 3 select (n+1-k)*k else 2*n-2: k in [1..n]]: n in [1..10]]; // triangle output",
				"(PARI)",
				"T(n, k) = if ((n+1-k\u003c3) || (k\u003c3), (n+1-k)*k, 2*n-2);",
				"tabl(nn) = for(i=1, nn, for(j=1, i, print1(T(i, j), \", \")); print);",
				"tabl(20) \\\\ triangle output"
			],
			"xref": [
				"Cf. A000027 (1st column/oblique side of the triangle or 1st row/column of the table), A005843 (2nd row/column of the table, or 2nd column of the triangle), A008574 (main diagonal of the table), A005893 (row sum of the triangle).",
				"Cf. A003991 (the number of elements in an n X k matrix).",
				"Cf. A131821, A318274, A154325."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Stefano Spezia_, Sep 29 2018",
			"references": 2,
			"revision": 92,
			"time": "2018-11-19T11:20:19-05:00",
			"created": "2018-11-19T11:20:19-05:00"
		}
	]
}