{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A342527",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 342527,
			"data": "1,1,2,4,6,8,11,12,16,17,21,20,29,24,31,32,38,32,46,36,51,46,51,44,69,51,61,60,73,56,87,60,84,74,81,76,110,72,91,88,115,80,123,84,117,112,111,92,153,101,132,116,139,104,159,120,161,130,141,116,205,120,151,156,178,142,195,132,183,158",
			"name": "Number of compositions of n with alternating parts equal.",
			"comment": [
				"These are finite sequences q of positive integers summing to n such that q(i) = q(i+2) for all possible i."
			],
			"link": [
				"Gus Wiseman, \u003ca href=\"/A069916/a069916.txt\"\u003eSequences counting and ranking partitions and compositions by their differences and quotients\u003c/a\u003e."
			],
			"formula": [
				"a(n) = 1 + n + A000203(n) - 2*A000005(n).",
				"a(n) = A065608(n) + A062968(n)."
			],
			"example": [
				"The a(1) = 1 through a(8) = 16 compositions:",
				"  (1)  (2)   (3)    (4)     (5)      (6)       (7)        (8)",
				"       (11)  (12)   (13)    (14)     (15)      (16)       (17)",
				"             (21)   (22)    (23)     (24)      (25)       (26)",
				"             (111)  (31)    (32)     (33)      (34)       (35)",
				"                    (121)   (41)     (42)      (43)       (44)",
				"                    (1111)  (131)    (51)      (52)       (53)",
				"                            (212)    (141)     (61)       (62)",
				"                            (11111)  (222)     (151)      (71)",
				"                                     (1212)    (232)      (161)",
				"                                     (2121)    (313)      (242)",
				"                                     (111111)  (12121)    (323)",
				"                                               (1111111)  (1313)",
				"                                                          (2222)",
				"                                                          (3131)",
				"                                                          (21212)",
				"                                                          (11111111)"
			],
			"mathematica": [
				"Table[Length[Select[Join@@Permutations/@IntegerPartitions[n],SameQ@@Plus@@@Reverse/@Partition[#,2,1]\u0026]],{n,0,15}]"
			],
			"xref": [
				"The odd-length case is A062968.",
				"The even-length case is A065608.",
				"The version with alternating parts unequal is A224958 (unordered: A000726).",
				"The version with alternating parts weakly decreasing is A342528.",
				"A000005 counts constant compositions.",
				"A000041 counts weakly increasing (or weakly decreasing) compositions.",
				"A000203 adds up divisors.",
				"A002843 counts compositions with all adjacent parts x \u003c= 2y.",
				"A003242 counts anti-run compositions.",
				"A175342 counts compositions with constant differences.",
				"A342495 counts compositions with constant first quotients.",
				"A342496 counts partitions with constant first quotients (strict: A342515, ranking: A342522).",
				"Cf. A001522, A008965, A048004, A059966, A064410, A064428, A069916, A114921, A167606, A325545, A325557."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Gus Wiseman_, Mar 24 2021",
			"references": 18,
			"revision": 10,
			"time": "2021-03-24T22:19:32-04:00",
			"created": "2021-03-24T22:19:32-04:00"
		}
	]
}