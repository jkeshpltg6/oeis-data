{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A009995",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 9995,
			"data": "0,1,2,3,4,5,6,7,8,9,10,20,21,30,31,32,40,41,42,43,50,51,52,53,54,60,61,62,63,64,65,70,71,72,73,74,75,76,80,81,82,83,84,85,86,87,90,91,92,93,94,95,96,97,98,210,310,320,321,410,420,421,430,431,432,510,520,521,530",
			"name": "Numbers with digits in strictly decreasing order. From the Macaulay expansion of n.",
			"comment": [
				"There are precisely 1023 terms (corresponding to every nonempty subset of {0..9}).",
				"A178788(a(n)) = 1. - _Reinhard Zumkeller_, Jun 30 2010",
				"A193581(a(n)) \u003e 0 for n \u003e 9. - _Reinhard Zumkeller_, Aug 10 2011",
				"A227362(a(n)) = a(n). - _Reinhard Zumkeller_, Jul 09 2013",
				"For a fixed natural number r, any natural number n has a unique \"Macaulay expansion\" n = C(a_r,r)+C(a_{r-1},r-1)+...+C(a_1,1) with a_r \u003e a_{r-1} \u003e ... \u003e a_1 \u003e= 0. If r=10, concatenating the digits a_r, ..., a_1 gives the present sequence. The representation is valid for all n, but the concatenation only makes sense if all the a_i are \u003c 10. - _N. J. A. Sloane_, Apr 05 2014",
				"a(n) = A262557(A263327(n)); a(A263328(n)) = A262557(n). - _Reinhard Zumkeller_, Oct 15 2015"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A009995/b009995.txt\"\u003eTable of n, a(n) for n = 1..1023\u003c/a\u003e",
				"B. Sury, \u003ca href=\"http://www.jstor.org/stable/10.4169/amer.math.monthly.121.04.359\"\u003eMacaulay Expansion\u003c/a\u003e, Amer. Math. Monthly 121 (2014), no. 4, 359--360. MR3183022. [See p. 359. - _N. J. A. Sloane_, Apr 05 2014]",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Digit.html\"\u003eDigit\u003c/a\u003e."
			],
			"mathematica": [
				"Sort@ Flatten@ Table[FromDigits /@ Subsets[ Range[9, 0, -1], {n}], {n, 10}] (* _Zak Seidov_, May 10 2006 *)"
			],
			"program": [
				"(Haskell)",
				"import Data.Set (fromList, minView, insert)",
				"a009995 n = a009995_list !! n",
				"a009995_list = 0 : f (fromList [1..9]) where",
				"   f s = case minView s of",
				"         Nothing     -\u003e []",
				"         Just (m,s') -\u003e m : f (foldl (flip insert) s' $",
				"                              map (10*m +) [0..m `mod` 10 - 1])",
				"-- _Reinhard Zumkeller_, Aug 10 2011",
				"(PARI) is(n)=fromdigits(vecsort(digits(n),,12))==n \\\\ _Charles R Greathouse IV_, Apr 16 2015"
			],
			"xref": [
				"Cf. A009993.",
				"Cf. A262557 (sorted lexicographically), A263327, A263328."
			],
			"keyword": "nonn,fini,full,base,look",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_.",
			"references": 22,
			"revision": 51,
			"time": "2015-10-15T07:48:40-04:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}