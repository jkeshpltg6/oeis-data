{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A182310",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 182310,
			"data": "0,1,2,4,7,5,8,13,12,11,15,9,14,10,16,25,22,30,18,28,19,27,23,29,20,31,17,26,24,21,32,49,42,64,97,82,124,67,99,83,123,71,101,88,117,80,121,70,102,86,126,66,100,87,125,68,103,85,128,193,162,244,143",
			"name": "a(0)=0, a(n+1) = (a(n) XOR floor(a(n)/2)) + 1, where XOR is the bitwise exclusive-or operator",
			"comment": [
				"As n -\u003e infinity, a(n) -\u003e infinity."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A182310/b182310.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/XOR.html\"\u003eXOR\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Binary_and#XOR\"\u003eBitwise operation XOR\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A105081(a(n-1)+1). - _Jon Maiga_, Jun 27 2021"
			],
			"example": [
				"a(5) = ( a(4) XOR floor(a(4)/2) ) + 1 = (7 XOR 3) + 1 = 4+1 = 5."
			],
			"maple": [
				"a:= proc(n) option remember; `if`(n=0, 0, 1+",
				"     (t-\u003e Bits[Xor](t, iquo(t, 2)))(a(n-1)))",
				"    end:",
				"seq(a(n), n=0..100);  # _Alois P. Heinz_, Jun 29 2021"
			],
			"mathematica": [
				"NestList[BitXor[#,Floor[#/2]]+1\u0026,0,70] (* _Harvey P. Dale_, Apr 18 2015 *)"
			],
			"program": [
				"(C)",
				"#include \u003cstdio.h\u003e",
				"#include \u003cmath.h\u003e",
				"typedef unsigned long long ULL;",
				"int main(int argc, char **argv) {",
				"  ULL a=0, i=0, p, prev;",
				"  while(1) {",
				"    prev = a,   a = (a^(a/2)) + 1;",
				"  #if 0  // \"if 1\" to print indices of 2^x",
				"    ++i;",
				"    if ( (i \u0026 ((1\u003c\u003c30)-1))==0 )  printf(\".\");",
				"    if ((a^prev) \u003e prev)",
				"       printf(\"\\n%llu at %llu,  log2: %llu \", a, i, (ULL)(100.0*log2(i)));",
				"  #else",
				"    printf(\"%llu, \", prev);",
				"  #endif",
				"    // Test reversion:",
				"    p=a-1;",
				"    ULL t=p/2;",
				"    while (t)  p^=t, t/=2;",
				"    if (p!=prev) printf(\"Reversion failed!\"), exit(1);",
				"  }",
				"  return 0;",
				"}  // from _Alex Ratushnyak_, Apr 26 2012",
				"(Haskell)",
				"import Data.Bits (xor)",
				"a182310 n = a182310_list !! n",
				"a182310_list = 0 : map (+ 1)",
				"   (zipWith xor a182310_list $ map (`div` 2) a182310_list) :: [Integer]",
				"-- _Reinhard Zumkeller_, Apr 25 2012",
				"(PARI) terms(n) = my(x=0, i=0); while(i \u003c n, print1(x, \", \"); x=bitxor(x, floor(x/2)) + 1; i++)",
				"/* Print initial 200 terms as follows: */",
				"terms(200) \\\\ _Felix Fröhlich_, Jun 29 2021"
			],
			"xref": [
				"Cf. A105081, A182417."
			],
			"keyword": "nonn,base",
			"offset": "0,3",
			"author": "_Alex Ratushnyak_, Apr 24 2012",
			"references": 4,
			"revision": 39,
			"time": "2021-06-29T12:43:51-04:00",
			"created": "2012-04-25T12:38:17-04:00"
		}
	]
}