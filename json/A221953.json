{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A221953",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 221953,
			"data": "1,10,300,15000,1050000,94500000,10395000000,1351350000000,202702500000000,34459425000000000,6547290750000000000,1374931057500000000000,316234143225000000000000,79058535806250000000000000,21345804667687500000000000000,6190283353629375000000000000000,1918987839625106250000000000000000",
			"name": "a(n) = 5^(n-1) * n! * Catalan(n-1).",
			"comment": [
				"a(n+1) is the number of square roots of any permutation in S_{20*n} whose disjoint cycle decomposition consists of 2*n cycles of length 10. - _Luis Manuel Rivera Martínez_, Feb 26 2015"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A221953/b221953.txt\"\u003eTable of n, a(n) for n = 1..200\u003c/a\u003e",
				"W. van der Aalst, J. Buijs and B. van Dongen, \u003ca href=\"https://hal.inria.fr/hal-01515548\"\u003eTowards Improving the Representational Bias of Process Mining\u003c/a\u003e, 2012.",
				"Jesús Leaños, Rutilo Moreno and Luis Manuel Rivera-Martínez, \u003ca href=\"http://arxiv.org/abs/1005.1531\"\u003eOn the number of mth roots of permutations\u003c/a\u003e, arXiv:1005.1531 [math.CO], 2010-2011.",
				"Jesús Leaños, Rutilo Moreno and Luis Manuel Rivera-Martínez, \u003ca href=\"http://ajc.maths.uq.edu.au/pdf/52/ajc_v52_p041.pdf\"\u003eOn the number of mth roots of permutations\u003c/a\u003e, Australas. J. Combin., Vol. 52 (2012), pp. 41-54 (Theorem 1)."
			],
			"formula": [
				"a(n) = 10*(2*n-3)*a(n-1) with a(1)=1. - _Bruno Berselli_, Mar 11 2013",
				"E.g.f.: (1 - sqrt(1-20*x))/10. - _Luis Manuel Rivera Martínez_, Mar 04 2015",
				"a(1) = 1; a(n) = 5 * Sum_{k=1..n-1} binomial(n,k) * a(k) * a(n-k). - _Ilya Gutkovskiy_, Jul 10 2020",
				"From _Amiram Eldar_, Jan 08 2022: (Start)",
				"Sum_{n\u003e=1} 1/a(n) = 1 + e^(1/20)*sqrt(Pi)*erf(1/(2*sqrt(5)))/(2*sqrt(5)), where erf is the error function.",
				"Sum_{n\u003e=1} (-1)^(n+1)/a(n) = 1 - e^(-1/20)*sqrt(Pi)*erfi(1/(2*sqrt(5)))/(2*sqrt(5)), where erfi is the imaginary error function. (End)"
			],
			"maple": [
				"A221953:= n-\u003e (5^(n-1)*n!/(2*(2*n-1))*binomial(2*n,n); seq(A221953(n), n=1..30); # _G. C. Greubel_, Apr 02 2021"
			],
			"mathematica": [
				"Table[CatalanNumber[n - 1]  5^(n-1)  n!, {n, 20}] (* _Vincenzo Librandi_, Mar 11 2013 *)"
			],
			"program": [
				"(MAGMA) [Catalan(n-1)*5^(n-1)*Factorial(n): n in [1..20]]; // _Vincenzo Librandi_, Mar 11 2013",
				"(PARI) my(x='x+O('x^22)); Vec(serlaplace((1-sqrt(1-20*x))/10)) \\\\ _Michel Marcus_, Mar 04 2015",
				"(Sage) [5^(n-1)*factorial(n)*catalan_number(n-1) for n in (1..30)] # _G. C. Greubel_, Apr 02 2021"
			],
			"xref": [
				"Sequences of the form m^(n-1)*n!*Catalan(n-1): A001813 (m=1), A052714 (or A144828) (m=2), A221954 (m=3), A052734 (m=4), this sequence (m=5), A221955 (m=6).",
				"Cf. A000108."
			],
			"keyword": "nonn,changed",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Feb 03 2013",
			"references": 7,
			"revision": 58,
			"time": "2022-01-08T05:10:03-05:00",
			"created": "2013-02-03T13:35:57-05:00"
		}
	]
}