{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A220468",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 220468,
			"data": "1,10,2,4,3,2,5,4,2,6,1,10,2,4,3,2,5,4,2,6,1,10,2,4,3,2,5,4,2,6,1,10,2,4,3,2,5,4,2,6,1,10,2,4,3,2,5,4,2,6,1,10,2,4,3,2,5,4,2,6,1,10,2,4,3,2,5,4,2,6,1,10,2,4,3,2,5,4,2,6,1,10,2,4,3,2,5,4,2,6",
			"name": "Number of cyclotomic cosets of n mod 10.",
			"comment": [
				"In other words, number of different cycles mod 10 obtained by repeatedly multiplying by n, with different starting elements. There are four different cycles for n = 3: {1, 3, 9, 7}, {2, 6, 8, 4}, {5}, {0}. By starting with a random number below 10, the numbers obtained by repeatedly multiplying by 3 and then taking modulo 10 repeat through one of these four different cycles.",
				"There are two different cycles for n = 2: {2, 4, 8, 6}, {0}. Note that this does not cover all the positive integers less than 10. The elements 1, 3, 7, 9 belong to the cycle {2, 4, 8, 6} since on starting with them, the numbers obtained by the process of repeatedly multiplying by 2 and then taking modulo 10 repeat through the elements of this cycle. Likewise the element 5 belongs to the cycle {0}. For n coprime to 10, the different cycles will cover all the elements less than 10 and will form different equivalence classes by themselves. For other values of n, the cycles will not cover all the elements less than 10.",
				"This sequence is periodic with period of 10, since x^i == (x + 10)^i mod 10."
			],
			"link": [
				"\u003ca href=\"/index/Rec\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,0,0,0,0,0,0,0,0,1)."
			],
			"formula": [
				"G.f.: -(6*x^9+2*x^8+4*x^7+5*x^6+2*x^5+3*x^4+4*x^3+2*x^2+10*x+1) / (x^10-1). - _Colin Barker_, Apr 13 2013"
			],
			"example": [
				"The following are the different cycles obtained by repeatedly multiplying by n, and then taking mod 10, with different starting elements.",
				"n = 0: {0}.",
				"n = 1: {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}.",
				"n = 2: {2, 4, 8, 6}, {0}.",
				"n = 3: {1, 3, 9, 7}, {2, 6, 8, 4}, {5}, {0}.",
				"n = 4: {4, 6}, {2, 8}, {0}.",
				"n = 5: {5}, {0}.",
				"n = 6: {2}, {4}, {6}, {8}, {0}.",
				"n = 7: {1, 7, 9, 3}, {2, 4, 8, 6}, {5}, {0}.",
				"n = 8: {8, 4, 2, 6}, {0}.",
				"n = 9: {1, 9}, {3, 7}, {2, 8}, {4, 6}, {5}, {0}."
			],
			"mathematica": [
				"iter[n_] := Table[ FixedPoint[ Union[#, Mod[n*#, 10]] \u0026, {m}], {m, 0, 9}]; classes[n_] := iter[n] //. {a___List, b_List, c___List, d_List, e___List} /; Intersection[b, d] != {} :\u003e {a, Union[b, d], c, e}; a[n_] := Length[classes[n]]; Table[a[n], {n, 0, 89}] (* _Jean-François Alcover_, Jan 08 2013 *)"
			],
			"program": [
				"(PARI) k=10; j=1; for(i=0, 100, m=0; n=vector(k, X, -1); for(l=0, k-1, if(n[((l*i^j)%k)+1]\u003e=0, n[l+1]=n[((l*i^j)%k)+1]; continue, n[l+1]=m; p=l; for(o=1, eulerphi(k), p=(p*i)%k; if(n[p+1]\u003e-1, break); n[p+1]=m); m++)); print1(m\", \"))",
				"(PARI) a(n)=[1, 10, 2, 4, 3, 2, 5, 4, 2, 6][n%10+1] \\\\ _Charles R Greathouse IV_, Jan 08 2013"
			],
			"xref": [
				"Cf. A173635, A220022-A220026."
			],
			"keyword": "nonn,base,less,easy",
			"offset": "0,2",
			"author": "_V. Raman_, Jan 03 2013",
			"references": 4,
			"revision": 71,
			"time": "2017-07-25T02:33:22-04:00",
			"created": "2013-01-28T13:41:36-05:00"
		}
	]
}