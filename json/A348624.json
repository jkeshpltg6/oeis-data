{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A348624",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 348624,
			"data": "31,21,2555,2805,3315,17391,38893,104857575,59363,2097120,31713,376809,117440484,18790481885,197132241,2885681109,42991575,4966055899,13153337295,3959422917,120946279055305,4191888080835,3729543441416139,321057395310519,84662395338675,294669116243901",
			"name": "a(n) = sum of row n of A348433 expressed as an irregular triangle.",
			"comment": [
				"The binary expansion w of a(n) has an interesting appearance shown by the bitmap in links. We may divide w with length m into 3 parts: the most significant part includes all bits including the last 0 before the middle of the word, m/2, a central run of k 1's that includes all but the last 1 before a 0, and a least significant part that includes the last 1 in the central run of 1s and an assortment of 0's. For example, a(3) = 2555 -\u003e 100.11111.1011, which we may partition as shown by \".\" so as to preserve the otherwise-leading 0 in the last part. The central run of 1s generally increases in length as n increases."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A348624/b348624.txt\"\u003eTable of n, a(n) for n = 1..1540\u003c/a\u003e",
				"Michael De Vlieger, \u003ca href=\"/A348624/a348624.png\"\u003eLog log scatterplot of a(n)\u003c/a\u003e for n=1..120.",
				"Michael De Vlieger, \u003ca href=\"/A348624/a348624_1.png\"\u003eLog log scatterplot of a(n)\u003c/a\u003e for n=1..3961.",
				"Michael De Vlieger, \u003ca href=\"/A348624/a348624_2.png\"\u003eBig-endian bitmap of the binary expansion of a(n)\u003c/a\u003e for n=1..3961 showing 1s in black and 0s in white."
			],
			"example": [
				"Table showing the first 5 rows of A348433 each having A348408(n) terms, and their sum a(n):",
				"n\\k 1  2  3   4   5   6   7    8    9     a(n)   binary(a(n))",
				"--------------------------------------------------------------",
				"1:  1  2  4   8  16                   -\u003e   31 -\u003e        11111",
				"2:  7 14                              -\u003e   21 -\u003e        10101",
				"3:  5 10 20  40  80 160 320  640 1280 -\u003e 2555 -\u003e 100111111011",
				"4: 11 22 44  88 176 352 704 1408      -\u003e 2805 -\u003e 101011110101",
				"5: 13 26 52 104 208 416 832 1664      -\u003e 3315 -\u003e 110011110011"
			],
			"mathematica": [
				"c[1] = m = q = 1; Most@ Reap[Do[If[IntegerQ[c[#]], Set[n, 2 m], Set[n, #]] \u0026@ Total@ IntegerDigits[m]; If[m \u003e n, Sow[q]; Set[q, n], q += n]; Set[c[n], 1]; m = n, 650]][[-1, -1]]",
				"(* Extract up to 3961 terms from bitmap: *)",
				"Block[{s = ImageData[ColorNegate@ Import[\"https://oeis.org/A348624/a348624_2.png\"], \"Bit\"]}, Array[FromDigits[s[[#]], 2] \u0026, 26]] (* _Michael De Vlieger_, Oct 26 2021 *)"
			],
			"xref": [
				"Cf. A348408, A348433."
			],
			"keyword": "nonn,base",
			"offset": "1,1",
			"author": "_Michael De Vlieger_, Oct 25 2021",
			"references": 1,
			"revision": 12,
			"time": "2021-10-27T09:53:57-04:00",
			"created": "2021-10-26T07:47:45-04:00"
		}
	]
}