{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001864",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1864,
			"id": "M2138 N0850",
			"data": "0,2,24,312,4720,82800,1662024,37665152,952401888,26602156800,813815035000,27069937855488,972940216546896,37581134047987712,1552687346633913000,68331503866677657600,3191386068123595166656,157663539876436721860608",
			"name": "Total height of rooted trees with n labeled nodes.",
			"comment": [
				"a(n) is the total number of nonrecurrent elements mapped into a recurrent element in all functions f:{1,2,...,n}-\u003e{1,2,...,n}.  a(n) = Sum_{k=1..n-1} A216971(n,k)*k. - _Geoffrey Critzer_, Jan 01 2013",
				"a(n) is the sum of the lengths of all cycles over all functions f:{1,2,...,n}-\u003e{1,2,...,n}.  Fixed points are taken to have length zero.  a(n) = Sum_{k=2..n} A066324(n,k)*(k-1). - _Geoffrey Critzer_, Aug 19 2013"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A001864/b001864.txt\"\u003eTable of n, a(n) for n = 1..100\u003c/a\u003e",
				"Hien D. Nguyen, G. J. McLachlan, \u003ca href=\"https://arxiv.org/abs/1607.04807\"\u003eProgress on a Conjecture Regarding the Triangular Distribution\u003c/a\u003e, arXiv preprint arXiv:1607.04807 [stat.OT], 2016.",
				"J. Riordan, \u003ca href=\"/A001863/a001863.pdf\"\u003eLetter to N. J. A. Sloane, Aug. 1970\u003c/a\u003e",
				"J. Riordan and N. J. A. Sloane, \u003ca href=\"http://dx.doi.org/10.1017/S1446788700007527\"\u003eEnumeration of rooted trees by total height\u003c/a\u003e, J. Austral. Math. Soc., vol. 10 pp. 278-282, 1969.",
				"N. J. A. Sloane, \u003ca href=\"/A000435/a000435.pdf\"\u003eIllustration of terms a(3) and a(4) in A000435\u003c/a\u003e",
				"D. Zvonkine, \u003ca href=\"http://www.math.jussieu.fr/~zvonkine/\"\u003eHome Page\u003c/a\u003e",
				"D. Zvonkine, \u003ca href=\"http://arxiv.org/abs/math/0403092\"\u003eAn algebra of power series arising in the intersection theory of moduli spaces of curves and in the enumeration of ramified coverings of the sphere\u003c/a\u003e, arXiv:0403092v2 [math.AG], 2004.",
				"D. Zvonkine, \u003ca href=\"http://arxiv.org/abs/math/0506248\"\u003eEnumeration of ramified coverings of the sphere and 2-dimensional gravity\u003c/a\u003e, arXiv:math/0506248 [math.AG], 2005.",
				"D. Zvonkine, \u003ca href=\"http://mi.mathnet.ru/eng/mmj274\"\u003eCounting ramified coverings and intersection theory on Hurwitz spaces II (local structure of Hurwitz spaces and combinatorial results)\u003c/a\u003e, Moscow Mathematical Journal, vol. 7 (2007), no. 1, 135-162.",
				"\u003ca href=\"/index/Ro#rooted\"\u003eIndex entries for sequences related to rooted trees\u003c/a\u003e",
				"\u003ca href=\"/index/Tra#trees\"\u003eIndex entries for sequences related to trees\u003c/a\u003e"
			],
			"formula": [
				"a(n) = n*A000435(n).",
				"E.g.f: (LambertW(-x)/(1+LambertW(-x)))^2. - _Vladeta Jovovic_, Apr 10 2001",
				"a(n) = sum(k=1, n-1, binomial(n, k)*(n-k)^(n-k)*k^k). - _Benoit Cloitre_, Mar 22 2003",
				"a(n) ~ sqrt(Pi/2)*n^(n+1/2). - _Vaclav Kotesovec_, Aug 07 2013"
			],
			"maple": [
				"A001864 := proc(n) local k; add(n!*n^k/k!, k=0..n-2); end;"
			],
			"mathematica": [
				"Table[Sum[Binomial[n,k](n-k)^(n-k) k^k,{k,1,n-1}],{n,20}] (* _Harvey P. Dale_, Oct 10 2011 *)",
				"a[n_] := n*(n-1)*Exp[n]*Gamma[n-1, n] // Round; Table[a[n], {n, 1, 18}]  (* _Jean-François Alcover_, Jun 24 2013 *)"
			],
			"program": [
				"(PARI) a(n)=sum(k=1,n-1,binomial(n,k)*(n-k)^(n-k)*k^k)"
			],
			"xref": [
				"Cf. A000435, A001863."
			],
			"keyword": "nonn,easy,nice",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"references": 7,
			"revision": 70,
			"time": "2017-12-27T10:40:58-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}