{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A220554",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 220554,
			"data": "0,2,3,2,2,2,2,3,3,3,1,1,2,3,3,1,2,3,4,3,4,2,2,2,3,1,3,3,5,3,1,2,2,2,5,2,1,2,2,5,1,2,4,3,4,4,3,5,4,4,1,2,2,2,4,4,4,4,6,6,4,2,6,4,4,4,2,2,5,6,3,2,3,5,5,4,3,2,4,4,2,4,4,4,4,3,4,3,5,6,3,4,5,5,3,1,2,5,3,4",
			"name": "Number of ways to write 2n = p+q (q\u003e0) with p, 2p+1 and (p-1)^2+q^2 all prime",
			"comment": [
				"Conjecture: a(n)\u003e0 for all n\u003e1.",
				"This has been verified for n up to 2*10^8. It implies that there are infinitely many Sophie Germain primes.",
				"Note that Ming-Zhi Zhang asked (before 1990) whether any odd integer greater than 1 can be written as x+y (x,y\u003e0) with x^2+y^2 prime, see A036468.",
				"Zhi-Wei Sun also made the following related conjectures:",
				"(1) Any integer n\u003e2 can be written as x+y (x,y\u003e=0) with 3x-1, 3x+1 and x^2+y^2-3(n-1 mod 2) all prime.",
				"(2) Each integer n\u003e3 not among 20, 40, 270 can be written as x+y (x,y\u003e0) with 3x-2, 3x+2 and x^2+y^2-3(n-1 mod 2) all prime.",
				"(3) Any integer n\u003e4 can be written as x+y (x,y\u003e0) with 2x-3, 2x+3 and x^2+y^2-3(n-1 mod 2) all prime. Also, every n=10,11,... can be written as x+y (x,y\u003e=0) with x-3, x+3 and x^2+y^2-3(n-1 mod 2) all prime.",
				"(4) Any integer n\u003e97 can be written as p+q (q\u003e0) with p, 2p+1, n^2+pq all prime. Also, each integer n\u003e10 can be written as p+q (q\u003e0) with p, p+6, n^2+pq all prime.",
				"(5) Every integer n\u003e3 different from 8 and 18 can be written as x+y (x\u003e0, y\u003e0) with 3x-2, 3x+2 and n^2-xy all prime."
			],
			"reference": [
				"R. K. Guy, Unsolved Problems in Number Theory, 2nd Edition, Springer, New York, 2004, p. 161."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A220554/b220554.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1211.1588\"\u003eConjectures involving primes and quadratic forms\u003c/a\u003e, arXiv:1211.1588."
			],
			"example": [
				"a(16)=1 since 32=11+21 with 11, 2*11+1=23 and (11-1)^2+21^2=541 all prime."
			],
			"mathematica": [
				"a[n_]:=a[n]=Sum[If[PrimeQ[p]==True\u0026\u0026PrimeQ[2p+1]==True\u0026\u0026PrimeQ[(p-1)^2+(2n-p)^2]==True,1,0],{p,1,2n-1}]",
				"Do[Print[n,\" \",a[n]],{n,1,1000}]"
			],
			"xref": [
				"Cf. A005384, A005385, A036468, A220455, A220431, A218867, A219055, A220419, A220413, A220272, A219842, A219864, A219923."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Zhi-Wei Sun_, Dec 15 2012",
			"references": 5,
			"revision": 12,
			"time": "2013-01-02T14:18:34-05:00",
			"created": "2012-12-16T00:17:54-05:00"
		}
	]
}