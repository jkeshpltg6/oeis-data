{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A286349",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 286349,
			"data": "-1,-1,0,-1,1,0,-1,1,0,0,0,-1,1,1,0,0,0,0,-1,1,1,0,0,-1,0,0,0,0,0,-1,1,1,1,0,-1,0,0,0,0,0,0,0,0,0,-1,1,1,1,0,0,-1,-1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,-1,1,1,1,1,0,-1,-1,0,0,-1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0",
			"name": "Möbius (or Moebius) partition function of partitions listed in the Abramowitz-Stegun order.",
			"comment": [
				"The sequence of row lengths of this array is [1,2,3,5,7,11,15,22,30,42,56,77,...] from A000041(n), n\u003e=1 (partition numbers)."
			],
			"link": [
				"Ken Ono, Robert Schneider, and Ian Wagner, \u003ca href=\"https://arxiv.org/abs/1704.06636\"\u003ePartition-Theoretic Formulas for Arithmetic Densities\u003c/a\u003e, arXiv:1704.06636 [math.CO], April 21 2017."
			],
			"formula": [
				"a(n,k) = Möbius partition function of the k-th partition of n in Abramowitz-Stegun order (see reference). The Möbius partition function muP(p) of a partition p is defined by: muP(p) = (-1)^k if p has k distinct parts; otherwise muP(p) = 0 (p in the table of Abramowitz-Stegun)."
			],
			"example": [
				"[-1];",
				"[-1,0];",
				"[-1,1,0];",
				"[-1,1,0,0,0];",
				"[-1,1,1,0,0,0,0];",
				"[-1,1,1,0,0,-1,0,0,0,0,0];",
				"...",
				"Row 5 for partitions of 5 in the mentioned order: 5, 41, 32, 311, 221, 2111, 11111 with Möbius partition function values -1,1,1,0,0,0,0 because 5 has one part, 31 and 32 have two parts, and the rest have repeated parts."
			],
			"mathematica": [
				"PartitionMu[p_] := 0 /; Sort@p != Union@p;",
				"PartitionMu[p_] := (-1)^Length@p /; Sort@p == Union@p;",
				"table@T_ :=",
				"Map[",
				"PartitionMu,",
				"  Table[",
				"   Apply[Join, Reverse@*Sort /@ Table[IntegerPartitions[n, {k}],",
				"   {k, n}]",
				"  ],",
				"  {n, T}],",
				"{2}];",
				"Flatten@table@10"
			],
			"xref": [
				"Row sums are A010815. - _Seiichi Manyama_, May 10 2017",
				"Cf. A000009, A032020, A007837, A008683."
			],
			"keyword": "sign",
			"offset": "1",
			"author": "_George Beck_, May 07 2017",
			"references": 1,
			"revision": 18,
			"time": "2018-12-26T16:56:21-05:00",
			"created": "2017-05-10T08:23:06-04:00"
		}
	]
}