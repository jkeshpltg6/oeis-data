{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A046688",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 46688,
			"data": "1,1,1,1,2,1,1,3,3,1,1,4,5,5,1,1,5,7,9,9,1,1,6,9,13,17,17,1,1,7,11,17,25,33,33,1,1,8,13,21,33,49,65,65,1,1,9,15,25,41,65,97,129,129,1,1,10,17,29,49,81,129,193,257,257,1,1,11,19,33,57,97,161,257,385,513,513,1",
			"name": "Antidiagonals of square array in which k-th row (k\u003e0) is an arithmetic progression of difference 2^(k-1).",
			"reference": [
				"G. H. Hardy, A Theorem Concerning the Infinite Cardinal Numbers, Quart. J. Math., 35 (1904), p. 90 = Collected Papers, Vol. VII, p. 430."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A046688/b046688.txt\"\u003eTable of n, a(n) for n = 0..1325\u003c/a\u003e"
			],
			"formula": [
				"A(m,n) = 1 + n*2^(m-1) for m \u003e 1. - _Andrew Howroyd_, Mar 07 2020",
				"As a triangle, T(n,k) = A(k,n-k) = 1 + (n-k)*2^(k-1). - _Gus Wiseman_, May 08 2021"
			],
			"example": [
				"From _Gus Wiseman_, May 08 2021: (Start):",
				"Array A(m,n) = 1 + n*2^(m-1) begins:",
				"       n=0: n=1: n=2: n=3: n=4: n=5: n=6: n=7: n=8: n=9:",
				"  m=0:   1    1    1    1    1    1    1    1    1    1",
				"  m=1:   1    2    3    5    9   17   33   65  129  257",
				"  m=2:   1    3    5    9   17   33   65  129  257  513",
				"  m=3:   1    4    7   13   25   49   97  193  385  769",
				"  m=4:   1    5    9   17   33   65  129  257  513 1025",
				"  m=5:   1    6   11   21   41   81  161  321  641 1281",
				"  m=6:   1    7   13   25   49   97  193  385  769 1537",
				"  m=7:   1    8   15   29   57  113  225  449  897 1793",
				"  m=8:   1    9   17   33   65  129  257  513 1025 2049",
				"  m=9:   1   10   19   37   73  145  289  577 1153 2305",
				"Triangle T(n,k) = 1 + (n-k)*2^(k-1) begins:",
				"   1",
				"   1   1",
				"   1   2   1",
				"   1   3   3   1",
				"   1   4   5   5   1",
				"   1   5   7   9   9   1",
				"   1   6   9  13  17  17   1",
				"   1   7  11  17  25  33  33   1",
				"   1   8  13  21  33  49  65  65   1",
				"   1   9  15  25  41  65  97 129 129   1",
				"   1  10  17  29  49  81 129 193 257 257   1",
				"   1  11  19  33  57  97 161 257 385 513 513   1",
				"(End)"
			],
			"mathematica": [
				"Table[If[k==0,1,n*2^(k-1)+1],{n,0,9},{k,0,9}] (* ARRAY, _Gus Wiseman_, May 08 2021 *)",
				"Table[If[k==0,1,1+(n-k)*2^(k-1)],{n,0,10},{k,0,n}] (* TRIANGLE, _Gus Wiseman_, May 08 2021 *)"
			],
			"program": [
				"(PARI) A(m,n)={if(m\u003e0, 1+n*2^(m-1), 1)}",
				"{ for(m=0, 10, for(n=0, 10, print1(A(m,n), \", \")); print) } \\\\ _Andrew Howroyd_, Mar 07 2020"
			],
			"xref": [
				"Row sums are A000079.",
				"Diagonal n = m + 1 of the array is A002064.",
				"Diagonal n = m of the array is A005183.",
				"Column m = 1 of the array is A094373.",
				"Diagonal n = m - 1 of the array is A131056.",
				"A002109 gives hyperfactorials (sigma: A260146, omega: A303281).",
				"A009998(k,n) = n^k.",
				"A009999(n,k) = n^k.",
				"A057156 = (2^n)^(2^n).",
				"A062319 counts divisors of n^n.",
				"Cf. A000169, A000272, A000312, A036289, A343656, A343658."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from Larry Reeves (larryr(AT)acm.org), Apr 06 2000"
			],
			"references": 2,
			"revision": 16,
			"time": "2021-05-08T08:32:15-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}