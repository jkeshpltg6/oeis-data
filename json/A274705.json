{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A274705",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 274705,
			"data": "1,1,-2,1,-3,3,1,-4,25,-4,1,-5,133,-427,5,1,-6,621,-15130,12465,-6,1,-7,2761,-437593,4101799,-555731,7,1,-8,11999,-12012016,1026405753,-2177360656,35135945,-8,1,-9,51465,-325204171,243458990271,-6054175060941,1999963458217,-2990414715,9",
			"name": "Rectangular array read by ascending antidiagonals. Row n has the exponential generating function 1/M_{n}(z^n) where M_{n}(z) is the n-th Mittag-Leffler function, nonzero coefficients only, for n\u003e=1.",
			"link": [
				"L. Carlitz, \u003ca href=\"https://doi.org/10.1007/BF01360145\"\u003eSome arithmetic properties of the Olivier functions\u003c/a\u003e, Math. Ann. 128 (1954), 412-419.",
				"H. J. Haubold, A. M. Mathai, and R. K. Saxena, \u003ca href=\"http://dx.doi.org/10.1155/2011/298628\"\u003eMittag-Leffler Functions and Their Applications\u003c/a\u003e, Journal of Applied Mathematics, vol. 2011, Article ID 298628, 51 pages.",
				"L. Olivier, \u003ca href=\"https://gdz.sub.uni-goettingen.de/id/PPN243919689_0002?tify=%7B%22pages%22:[257]%7D\"\u003eBemerkungen über eine Art von Functionen, welche ähnliche Eigenschaften haben, wie der Cosinus und Sinus\u003c/a\u003e, J. Reine Angew. Math. 2 (1827), 243-251.",
				"Eric Weisstein's MathWorld, \u003ca href=\"http://mathworld.wolfram.com/GeneralizedHyperbolicFunctions.html\"\u003eGeneralized hyperbolic functions\u003c/a\u003e."
			],
			"formula": [
				"Recurrence for the m-th row: R(m, n) = -Sum_{k=0..n-1} binomial(m*n+1, m*k+1)*R(m, k) for n \u003e= 1. See Carlitz (1.3)."
			],
			"example": [
				"Array starts:",
				"n=1: {1, -2,  3, -4, 5, -6, 7, -8,  9, -10,  11,...} [A181983]",
				"n=2: {1, -3, 25, -427, 12465, -555731, 35135945,...} [A009843]",
				"n=3: {1, -4, 133, -15130, 4101799,  -2177360656,...} [A274703]",
				"n=4: {1, -5, 621, -437593, 1026405753, -6054175060941,...} [A274704]",
				"n=5: {1, -6, 2761, -12012016, 243458990271, ...}"
			],
			"maple": [
				"ibn := proc(m, k) local w, om, t;",
				"w := exp(2*Pi*I/m); om := m*x/add(exp(x*w^j), j=0..m-1);",
				"t := series(om, x, k+m); simplify(k!*coeff(t,x,k)) end:",
				"seq(seq(ibn(n-k+2, n*k-n-k^2+3*k-1), k=1..n+1),n=0..8);"
			],
			"mathematica": [
				"A274705Row[m_] := Module[{c}, c = CoefficientList[Series[1/MittagLefflerE[m,z^m],",
				"{z,0,12*m}],z]; Table[Factorial[m*n+1]*c[[m*n+1]], {n,0,9}] ]",
				"Table[Print[A274705Row[n]], {n,1,6}]"
			],
			"program": [
				"(Sage)",
				"def ibn(m, k):",
				"    w = exp(2*pi*I/m)",
				"    om = m*x/sum(exp(x*w^j) for j in range(m))",
				"    t = taylor(om, x, 0, k + m)",
				"    return simplify(factorial(k)*t.list()[k])",
				"def A274705_row(m, size):",
				"    return [ibn(m, k) for k in range(1, m*size, m)]",
				"for n in (1..4): print(A274705_row(n, 8))"
			],
			"xref": [
				"Cf. A009843, A181983, A274703, A274704."
			],
			"keyword": "sign,tabl",
			"offset": "0,3",
			"author": "_Peter Luschny_, Jul 03 2016",
			"references": 2,
			"revision": 41,
			"time": "2020-03-08T10:14:40-04:00",
			"created": "2016-07-04T03:35:52-04:00"
		}
	]
}