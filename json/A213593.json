{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A213593",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 213593,
			"data": "2,1,2,-3,10,-45,250,-1645,12490,-107415,1031690,-10943955,127058690,-1602400085,21812913650,-318763741725,4977247397650,-82695799908975,1456703469048850,-27117356172328675,531930264143933050",
			"name": "Stirling transform of the first kind of the Lucas numbers A000032.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A213593/b213593.txt\"\u003eTable of n, a(n) for n = 0..445\u003c/a\u003e",
				"Jiaqiang Pan, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL15/Pan/pan19.html\"\u003eMatrix Decomposition of the Unified Generalized Stirling Numbers and Inversion of the Generalized Factorial Matrices\u003c/a\u003e, Journal of Integer Sequences 15 (2012) #12.6.6"
			],
			"formula": [
				"a(0)=2; for n=1,2,3, ..., a(n) = r*(r-1)*(r-2)*...*(r-n+1) + s*(s-1)*(s-2)*...*(s-n+1), where r=(1+sqrt(5))/2 and s=(1-sqrt(5))/2.",
				"From _Vladimir Reshetnikov_, Oct 20 2015: (Start)",
				"Let phi=(1+sqrt(5))/2.",
				"a(n) = Gamma(2-phi)/Gamma(2-phi-n)+Gamma(1+phi)/Gamma(1+phi-n).",
				"Recurrence: a(0)=2, a(1)=1, a(n+2) = (1+n-n^2)*a(n) - 2*n*a(n+1).",
				"E.g.f.: (1+(x+1)^sqrt(5))/(x+1)^(1/phi).",
				"(End)",
				"a(n) ~ (-1)^n * n! * n^((sqrt(5)-3)/2) / Gamma(2/(1+sqrt(5))). - _Vaclav Kotesovec_, Oct 21 2015",
				"a(n) = Sum_{k=1..n} Stirling1(n,k)*Lucas(k). - _G. C. Greubel_, Jul 06 2019"
			],
			"example": [
				"For n=4, a(4) = r*(r-1)*(r-2)*(r-3) + s*(s-1)*(s-2)*(s-3) = 10."
			],
			"maple": [
				"A000032 := proc(n)",
				"        combinat[fibonacci](n+1)+combinat[fibonacci](n-1) ;",
				"end proc:",
				"A213593 := proc(n)",
				"        add(combinat[stirling1](n,i)*A000032(i),i=0..n) ;",
				"end proc:",
				"seq(A213593(n),n=0..20) ; # _R. J. Mathar_, Jun 26 2012"
			],
			"mathematica": [
				"Expand@FunctionExpand@Table[Gamma[2 - GoldenRatio]/Gamma[2 - GoldenRatio - n] + Gamma[1 + GoldenRatio]/Gamma[1 - n + GoldenRatio], {n, 0, 20}] (* _Vladimir Reshetnikov_, Oct 20 2015 *)",
				"Table[If[n==0, 2, Sum[StirlingS1[n, k]*LucasL[k], {k, n}]], {n, 0, 25}] (* _G. C. Greubel_, Jul 06 2019 *)"
			],
			"program": [
				"(PARI) lucas(n) = fibonacci(n+1) + fibonacci(n-1);",
				"vector(25, n, n--; if(n==0, 2, sum(k=1,n, stirling(n,k,1)*lucas(k)) )) \\\\ _G. C. Greubel_, Jul 06 2019",
				"(MAGMA) [2] cat [(\u0026+[StirlingFirst(n,k)*Lucas(k): k in [1..n]]): n in [1..25]]; // _G. C. Greubel_, Jul 06 2019",
				"(Sage) [2]+[sum((-1)^(n-k)*stirling_number1(n,k)*lucas_number2(k,1,-1) for k in (1..n)) for n in (1..25)] # _G. C. Greubel_, Jul 06 2019",
				"(GAP) Concatenation([2], List([1..25], n-\u003e Sum([1..n], k-\u003e (-1)^(n-k)* Stirling1(n, k)*Lucas(1,-1,k)[2] ))) # _G. C. Greubel_, Jul 06 2019"
			],
			"xref": [
				"Cf. A000032 (Lucas numbers), the general-term formula of which is L(n) = r^n + s^n.",
				"Cf. A008275, A323620."
			],
			"keyword": "sign",
			"offset": "0,1",
			"author": "_Jiaqiang Pan_, Jun 15 2012",
			"references": 3,
			"revision": 24,
			"time": "2021-07-27T07:23:41-04:00",
			"created": "2012-06-21T03:13:25-04:00"
		}
	]
}