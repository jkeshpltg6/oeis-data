{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A080764",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 80764,
			"data": "1,1,0,1,1,0,1,1,1,0,1,1,0,1,1,1,0,1,1,0,1,1,0,1,1,1,0,1,1,0,1,1,1,0,1,1,0,1,1,0,1,1,1,0,1,1,0,1,1,1,0,1,1,0,1,1,1,0,1,1,0,1,1,0,1,1,1,0,1,1,0,1,1,1,0,1,1,0,1,1,0,1,1,1,0,1,1,0,1,1,1,0,1,1,0,1,1,1,0,1,1,0,1,1,0",
			"name": "First differences of A049472, floor(n/sqrt(2)).",
			"comment": [
				"Fixed point of the morphism 0-\u003e1, 1-\u003e110. - _Benoit Cloitre_, May 31 2004",
				"As binary constant 0.1101101110110... = 0.85826765646... (A119812), see Fxtbook link. - _Joerg Arndt_, May 15 2011",
				"Characteristic word with slope 1/sqrt(2) [see J. L. Ramirez et al.]. - _R. J. Mathar_, Jul 09 2013",
				"From _Peter Bala_, Nov 22 2013: (Start)",
				"Sturmian word: equals the limit word S(infinity) where S(0) = 0, S(1) = 1 and for n \u003e= 1, S(n+1) = S(n)S(n)S(n-1).",
				"More generally, for k = 0,1,2,..., we can define a sequence of words S_k(n) by S_k(0) = 0, S_k(1) = 0...01 (k 0's) and for n \u003e= 1, S_k(n+1) = S_k(n)S_k(n)S_k(n-1). Then the limit word S_k(infinity) is a Sturmian word whose terms are given by a(n) = floor((n + 2)/(k + sqrt(2))) - floor((n + 1)/(k + sqrt(2))).",
				"This sequence corresponds to the case k = 0. See A159684 (case k = 1) and A171588 (case k = 2). Compare with the Fibonacci words A005614, A221150, A221151 and A221152. See also A230901. (End)",
				"For n \u003e 0: a(A001951(n)) = 1, a(A001952(n)) = 0. - _Reinhard Zumkeller_, Jul 03 2015",
				"Binary complement of the Pell word A171588. - _Michel Dekking_, Feb 22 2018"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A080764/b080764.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Joerg Arndt, \u003ca href=\"http://www.jjj.de/fxt/#fxtbook\"\u003eMatters Computational (The Fxtbook)\u003c/a\u003e, section 38.12, pp. 757-758.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Sturmian_word\"\u003eSturmian word\u003c/a\u003e",
				"\u003ca href=\"/index/Fi#FIXEDPOINTS\"\u003eIndex entries for sequences that are fixed points of mappings\u003c/a\u003e"
			],
			"formula": [
				"a(n) = floor((n+2)*sqrt(2)/2) - floor((n+1)*sqrt(2)/2).",
				"a(n) = A188295(n+2) for all n in Z. - _Michael Somos_, Aug 19 2018"
			],
			"example": [
				"From _Peter Bala_, Nov 22 2013: (Start)",
				"The first few Sturmian words S(n) are",
				"S(0) = 0",
				"S(1) = 1",
				"S(2) = 110",
				"S(3) = 110 110 1",
				"S(4) = 1101101 1101101 110",
				"S(5) = 11011011101101110 11011011101101110 1101101",
				"The lengths of the words are [1, 1, 3, 7, 17, 41, ...] = A001333.  (End)"
			],
			"maple": [
				"A080764 := proc(n)",
				"    alpha := 1/sqrt(2) ;",
				"    floor((n+2)*alpha)-floor((n+1)*alpha) ;",
				"end proc: # _R. J. Mathar_, Jul 09 2013"
			],
			"mathematica": [
				"Nest[ Flatten[ # /. {0 -\u003e 1, 1 -\u003e {1, 1, 0}}] \u0026, {1}, 7] (* _Robert G. Wilson v_, Apr 16 2005 *)",
				"NestList[ Flatten[ # /. {0 -\u003e {1}, 1 -\u003e {1, 0, 1}}] \u0026, {1}, 5] // Flatten (* or *)",
				"t = Table[Floor[n/Sqrt[2]], {n, 111}]; Drop[t, 1] - Drop[t, -1] (* _Robert G. Wilson v_, Nov 03 2005 *)",
				"a[ n_] := With[{m = n + 1}, Floor[(m + 1) / Sqrt[2]] - Floor[m / Sqrt[2]]]; (* _Michael Somos_, Aug 19 2018 *)"
			],
			"program": [
				"(Haskell)",
				"a080764 n = a080764_list !! n",
				"a080764_list = tail $ zipWith (-) (tail a049472_list) a049472_list",
				"-- _Reinhard Zumkeller_, Jul 03 2015",
				"(PARI) {a(n) = n++; my(k = sqrtint(n*n\\2)); n*(n+2) \u003e 2*k*(k+2)}; /* _Michael Somos_, Aug 19 2018 */"
			],
			"xref": [
				"Cf. A005614, A159684, A171588, A221150, A221151, A221152, A230901.",
				"Cf. A049472, A001951, A001952, A188295."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_Matthew Vandermast_, Mar 25 2003",
			"references": 40,
			"revision": 47,
			"time": "2018-08-19T17:13:00-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}