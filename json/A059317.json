{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A059317",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 59317,
			"data": "1,1,1,1,1,2,4,2,1,1,3,8,9,8,3,1,1,4,13,22,29,22,13,4,1,1,5,19,42,72,82,72,42,19,5,1,1,6,26,70,146,218,255,218,146,70,26,6,1,1,7,34,107,261,476,691,773,691,476,261,107,34,7,1,1,8,43,154,428,914,1574,2158",
			"name": "Pascal's \"rhombus\" (actually a triangle T(n,k), n \u003e= 0, 0\u003c=k\u003c=2n) read by rows: each entry is sum of 3 terms above it in previous row and one term above it two rows back.",
			"comment": [
				"The rows have lengths 1, 3, 5, 7, ...; cf. A005408.",
				"T(n,k) is the number of paths in the right half-plane from (0,0) to (n,k-n), consisting of steps U=(1,1), D=(1,-1), h=(1,0) and H=(2,0). Example: T(3,4)=8 because we have hhU, HU, hUh, Uhh, UH, DUU, UDU and UUD. Row sums yield A006190. - _Emeric Deutsch_, Sep 03 2007",
				"Let p(n,x) denote the Fibonacci polynomial, defined by p(1,x) = 1, p(2,x) = x, p(n,x) = x*p(n-1,x) + p(n-2,x). The coefficients of the numerator polynomial of the rational function p(n, x + 1 + 1/x) form row n of the triangle A059317; the first three numerator polynomials are 1, 1 + x + x^2, 1 + 2*x + 4*x^2 + 2*x^3 + x^4. - _Clark Kimberling_, Nov 04 2013"
			],
			"reference": [
				"Lin Yang and S.-L. Yang, The parametric Pascal rhombus. Fib. Q., 57:4 (2019), 337-346.",
				"Sheng-Liang Yang et al., The Pascal rhombus and Riordan array, Fib. Q., 56:4 (2018), 337-347."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A059317/b059317.txt\"\u003eRows n = 0..100 of triangle, flattened\u003c/a\u003e",
				"S. R. Finch, P. Sebah and Z.-Q. Bai, \u003ca href=\"http://arXiv.org/abs/0802.2654\"\u003eOdd Entries in Pascal's Trinomial Triangle\u003c/a\u003e arXiv:0802.2654 [math.NT], 2008.",
				"J. Goldwasser et al., \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(98)00373-2\"\u003eThe density of ones in Pascal's rhombus\u003c/a\u003e, Discrete Math., 204 (1999), 231-236.",
				"W. F. Klostermeyer, M. E. Mays, L. Soltes and G. Trapp, \u003ca href=\"http://www.fq.math.ca/Scanned/35-4/klostermeyer.pdf\"\u003eA Pascal rhombus\u003c/a\u003e, Fibonacci Quarterly, 35 (1997), 318-328.",
				"Y. Moshe, \u003ca href=\"http://dx.doi.org/10.1016/S0022-314X(03)00103-3\"\u003eThe density of 0's in recurrence double sequences\u003c/a\u003e, J. Number Theory, 103 (2003), 109-121.",
				"José L. Ramírez, \u003ca href=\"http://arxiv.org/abs/1511.04577\"\u003eThe Pascal Rhombus and the Generalized Grand Motzkin Paths\u003c/a\u003e, arXiv:1511.04577 [math.CO], 2015.",
				"Paul K. Stockmeyer, \u003ca href=\"http://arxiv.org/abs/1504.04404\"\u003eThe Pascal Rhombus and the Stealth Configuration\u003c/a\u003e, arXiv:1504.04404 [math.CO], 2015.",
				"\u003ca href=\"/index/Pas#Pascal\"\u003eIndex entries for triangles and arrays related to Pascal's triangle\u003c/a\u003e"
			],
			"formula": [
				"T(n+1, k) = T(n, k-1) + T(n, k) + T(n, k+1) + T(n-1, k).",
				"Another definition: T(i, j) is defined for i \u003e= 0, -infinity \u003c= j \u003c= infinity; T(i, j) = T(i-1, j) + T(i-1, j-1) + T(i-1, j-2) + T(i-2, j-2) for i \u003e= 2, all j; T(0, 0) = T(1, 1) = T(1, 1) = T(1, 2) = 1; T(0, j) = 0 for j != 0; T(1, j) = 0 for j != 0, 1, 2.",
				"G.f.: Sum_{n\u003e=0, k=0..2*n} T(n, k)*z^n*w^k = 1/(1-z-z*w-z*w^2-z^2*w^2).",
				"There does not seem to be a simple expression for T(n, k). [That may have been true in 2001, but it is no longer true, as the following formulas show. - _N. J. A. Sloane_, Jan 22 2016]",
				"If the rows of the sequence are displayed in the shape of an isosceles triangle, then, for k\u003e=0, columns k and -k have g.f. z^k*g^k/sqrt((1+z-z^2)(1-3z-z^2)), where g=1+zg+z^2*g+z^2*g^2=[1-z-z^2-sqrt((1+z-z^2)(1-3z--z^2))]/(2z^2). - _Emeric Deutsch_, Sep 03 2007",
				"T(i,j) = Sum_{m=0..i} Sum_{l=0..i-j-2*m} binomial(2*m+j,m)*binomial(l+j+2*m,l)*binomial(l,i-j-2*m-l) (see Ramirez link). - _José Luis Ramírez Ramírez_, Nov 18 2015",
				"The e.g.f of the j-th column of the Pascal rhombus is L_j(x)=(F(x)^(j+1)*C(F(x)^2)^j)/(x*(1-2*F(x)^2*C(F(x)^2))), where F(x) and C(x) are the generating function of the Fibonacci numbers and Catalan numbers. - _José Luis Ramírez Ramírez_, Nov 18 2015"
			],
			"example": [
				"Triangle begins:",
				"1;",
				"1, 1, 1;",
				"1, 2, 4, 2, 1;",
				"1, 3, 8, 9, 8, 3, 1;",
				"..."
			],
			"maple": [
				"r:=proc(i,j) option remember; if i=0 then 0 elif i=1 and abs(j)\u003e0 then 0 elif i=1 and j=0 then 1 elif i\u003e=1 then r(i-1,j)+r(i-1,j-1)+r(i-1,j+1)+r(i-2,j) else 0 fi end: seq(seq(r(i,j),j=-i+1..i-1),i=0..9); # _Emeric Deutsch_, Jun 06 2004",
				"g:=1/(1-z-z*w-z*w^2-z^2*w^2): gser:=simplify(series(g,z=0,10)): for n from 0 to 8 do P[n]:=sort(coeff(gser,z,n)) end do: for n from 0 to 8 do seq(coeff(P[n],w,k),k=0..2*n) end do; # yields sequence in triangular form; _Emeric Deutsch_, Sep 03 2007"
			],
			"mathematica": [
				"t[0, 0] = t[1, 0] = t[1, 1] = t[1, 2] = 1; t[n_ /; n \u003e= 0, k_ /; k \u003e= 0] /; k \u003c= 2n := t[n, k] = t[n-1, k] + t[n-1, k-1] + t[n-1, k-2] + t[n-2, k-2]; t[n_, k_] /; n \u003c 0 || k \u003c 0 || k \u003e 2n = 0; Flatten[ Table[ t[n, k], {n, 0, 8}, {k, 0, 2n}]] (* _Jean-François Alcover_, Feb 01 2012 *)"
			],
			"program": [
				"(Haskell)",
				"-- import Data.List (zipWith4)",
				"a059317 n k = a059317_tabf !! n !! k",
				"a059317_row n = a059317_tabf !! n",
				"a059317_tabf = [1] : [1,1,1] : f [1] [1,1,1] where",
				"   f ws vs = vs' : f vs vs' where",
				"     vs' = zipWith4 (\\r s t x -\u003e r + s + t + x)",
				"           (vs ++ [0,0]) ([0] ++ vs ++ [0]) ([0,0] ++ vs)",
				"           ([0,0] ++ ws ++ [0,0])",
				"-- _Reinhard Zumkeller_, Jun 30 2012"
			],
			"xref": [
				"Cf. A059318, A007318. Row sums give A006190. Central column is A059345.",
				"Other columns: A106050, A106053, A034856, A106058, A106113, A106150, A106173, A267192.",
				"Cf. also A006190, A140750."
			],
			"keyword": "tabf,easy,nice,nonn",
			"offset": "0,6",
			"author": "_N. J. A. Sloane_, Jan 26 2001",
			"ext": [
				"More terms from Larry Reeves (larryr(AT)acm.org), Jan 30 2001"
			],
			"references": 21,
			"revision": 64,
			"time": "2019-12-13T03:25:45-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}