{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A327244",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 327244,
			"data": "1,0,1,0,1,2,0,3,10,8,0,3,27,54,31,0,5,70,255,336,147,0,11,223,1222,2692,2580,899,0,13,508,4467,15512,25330,19566,5777,0,19,1193,15540,78819,194075,248976,160377,41024,0,27,2822,52981,375440,1303250,2463534,2593339,1430288,322488",
			"name": "Number T(n,k) of colored compositions of n using all colors of a k-set such that all parts have different color patterns and the patterns for parts i are sorted and have i colors in (weakly) increasing order; triangle T(n,k), n\u003e=0, 0\u003c=k\u003c=n, read by rows.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A327244/b327244.txt\"\u003eRows n = 0..140, flattened\u003c/a\u003e"
			],
			"formula": [
				"Sum_{k=1..n} k * T(n,k) = A327595(n)."
			],
			"example": [
				"T(3,1) = 3: 3aaa, 2aa1a, 1a2aa.",
				"T(3,2) = 10: 3aab, 3abb, 2aa1b, 2ab1a, 2ab1b, 2bb1a, 1a2ab, 1a2bb, 1b2aa, 1b2ab.",
				"T(3,3) = 8: 3abc, 2ab1c, 2ac1b, 2bc1a, 1a2bc, 1b2ac, 1c2ab, 1a1b1c.",
				"Triangle T(n,k) begins:",
				"  1;",
				"  0,  1;",
				"  0,  1,    2;",
				"  0,  3,   10,     8;",
				"  0,  3,   27,    54,    31;",
				"  0,  5,   70,   255,   336,    147;",
				"  0, 11,  223,  1222,  2692,   2580,    899;",
				"  0, 13,  508,  4467, 15512,  25330,  19566,   5777;",
				"  0, 19, 1193, 15540, 78819, 194075, 248976, 160377, 41024;",
				"  ..."
			],
			"maple": [
				"C:= binomial:",
				"b:= proc(n, i, k, p) option remember; `if`(n=0, p!, `if`(i\u003c1, 0, add(",
				"      b(n-i*j, min(n-i*j, i-1), k, p+j)/j!*C(C(k+i-1,i),j), j=0..n/i)))",
				"    end:",
				"T:= (n, k)-\u003e add(b(n$2, i, 0)*(-1)^(k-i)*C(k, i), i=0..k):",
				"seq(seq(T(n, k), k=0..n), n=0..10);"
			],
			"mathematica": [
				"c = Binomial;",
				"b[n_, i_, k_, p_] := b[n, i, k, p] = If[n == 0, p!, If[i \u003c 1, 0, Sum[b[n - i*j, Min[n - i*j, i-1], k, p+j]/j!*c[c[k + i - 1, i], j], {j, 0, n/i}]]];",
				"T[n_, k_] := Sum[b[n, n, i, 0]*(-1)^(k-i)*c[k, i], {i, 0, k}];",
				"Table[T[n, k], {n, 0, 10}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Apr 28 2020, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Columns k=0-2 give: A000007, A032020 (for n\u003e0), A327841.",
				"Main diagonal gives A120774.",
				"Row sums give A309670.",
				"T(2n,n) gives A327596.",
				"Cf. A327245, A327595."
			],
			"keyword": "nonn,tabl",
			"offset": "0,6",
			"author": "_Alois P. Heinz_, Sep 14 2019",
			"references": 9,
			"revision": 39,
			"time": "2020-04-28T07:32:32-04:00",
			"created": "2019-09-14T16:12:20-04:00"
		}
	]
}