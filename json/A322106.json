{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A322106",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 322106,
			"data": "2,2,50,8,10,10,1250,29,40,40,2738,72,82,82,176900,17810,1709690,178,11300,260,290,290,568690,416,2418050,488,3479450,629,2674061,730",
			"name": "Numerator of the least possible squared diameter of an enclosing circle of a strictly convex lattice n-gon.",
			"comment": [
				"If the smallest possible enclosing circle is essentially determined by 3 vertices of the polygon, the squared diameter may be rational and thus A322107(n) \u003e 1.",
				"The first difference of the sequences A321693(n) / A322029(n) from a(n) / A322107(n) occurs for n = 12.",
				"The ratio (A321693(n)/A322029(n)) / (a(n)/A322107(n)) will grow for larger n due to the tendency of the minimum area polygons to approach elliptical shapes with increasing aspect ratio, whereas the polygons leading to small enclosing circles will approach circular shape.",
				"For n\u003e=19, polygons with different areas may fit into the enclosing circle of minimal diameter. See examples in pdf at Pfoertner link."
			],
			"reference": [
				"See A063984."
			],
			"link": [
				"Hugo Pfoertner, \u003ca href=\"/A322106/a322106.pdf\"\u003eIllustration of convex n-gons fitting into smallest circle\u003c/a\u003e, (2018).",
				"Hugo Pfoertner, \u003ca href=\"/A322106/a322106_1.pdf\"\u003eIllustration of convex n-gons fitting into smallest circle, n = 27..32\u003c/a\u003e, (2018)."
			],
			"example": [
				"By n-gon a convex lattice n-gon is meant, area is understood omitting the factor 1/2. The following picture shows a comparison between the minimum area polygon and the polygon fitting in the smallest possible enclosing circle for n=12:.",
				"    0 ----- 1 ----- 2 ------ 3 ------ 4 ------ 5 ------ 6",
				"  6                          H ##### Gxh +++++ g",
				"  |                     #        +      #    *   +",
				"  |                 #       +              #        +",
				"  |             #       +                 *   #        +",
				"  5         I       i                          F        f",
				"  |       #       +                    *        #       +",
				"  |     #       +                                #      +",
				"  |   #       +                     *             #     +",
				"  4 J       j                                      #    e",
				"  | #     @+                     *                  #  +",
				"  | #     +      @                                   #+",
				"  | #    +              @     *                      +#",
				"  3 K   +                     @                     +   E",
				"  |  # +                   *         @             +    #",
				"  |   #                                    @      +     #",
				"  |  + #                *                        +@     #",
				"  2 k   #                                      d        D",
				"  | +    #           *                       +        #",
				"  | +     #                                +        #",
				"  | +      #       *                    +         #",
				"  1 l       L                         c        C",
				"  |   +       # *                +        #",
				"  |     +       #           +        #",
				"  |       +  *    #     +        #",
				"  0         a ++++ Axb ##### B",
				"    0 ----- 1 ----- 2 ------ 3 ------ 4 ------ 5 ------ 6",
				".",
				"The 12-gon ABCDEFGHIJKLA with area 52 fits into a circle of squared diameter 40, e.g. determined by the distance D - J, indicated by @@@. No convex 12-gon with a smaller enclosing circle exists. Therefore a(n) = 40 and A322107(12) = 1.",
				"For comparison, the 12-gon abcdefghijkla with minimal area A070911(12) = 48 requires a larger enclosing circle with squared diameter A321693(12)/A322029(12) = 52/1, e.g. determined by the distance a - g, indicated by ***."
			],
			"xref": [
				"Cf. A063984, A070911, A321693, A322029, A322107 (corresponding denominators)."
			],
			"keyword": "nonn,frac,more",
			"offset": "3,1",
			"author": "_Hugo Pfoertner_, Nov 26 2018",
			"ext": [
				"a(27)-a(32) from _Hugo Pfoertner_, Dec 19 2018"
			],
			"references": 2,
			"revision": 27,
			"time": "2021-07-01T06:06:16-04:00",
			"created": "2018-12-12T14:43:18-05:00"
		}
	]
}