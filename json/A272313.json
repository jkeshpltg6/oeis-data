{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A272313",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 272313,
			"data": "1,4,9,28,25,84,49,140,89,260,113,380,161,532,233,620,345,868,369,1116,417,1396,489,1612,609,1956,681,2236,825,2548,1041,2604,1369,3108,1393,3612,1441,4148,1513,4620,1633,5220,1705,5756,1849,6324,2065,6636,2401",
			"name": "Number of active (ON,black) cells in n-th stage of growth of two-dimensional cellular automaton defined by \"Rule 465\", based on the 5-celled von Neumann neighborhood.",
			"comment": [
				"Initialized with a single black (ON) cell at stage zero."
			],
			"reference": [
				"S. Wolfram, A New Kind of Science, Wolfram Media, 2002; p. 170."
			],
			"link": [
				"Robert Price, \u003ca href=\"/A272313/b272313.txt\"\u003eTable of n, a(n) for n = 0..128\u003c/a\u003e",
				"Robert Price, \u003ca href=\"/A272313/a272313.tmp.txt\"\u003eDiagrams of the first 20 stages\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"http://arxiv.org/abs/1503.01168\"\u003eOn the Number of ON Cells in Cellular Automata\u003c/a\u003e, arXiv:1503.01168 [math.CO], 2015",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ElementaryCellularAutomaton.html\"\u003eElementary Cellular Automaton\u003c/a\u003e",
				"S. Wolfram, \u003ca href=\"http://wolframscience.com/\"\u003eA New Kind of Science\u003c/a\u003e",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_2D_5-Neighbor_Cellular_Automata\"\u003eIndex to 2D 5-Neighbor Cellular Automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_Elementary_Cellular_Automata\"\u003eIndex to Elementary Cellular Automata\u003c/a\u003e"
			],
			"mathematica": [
				"CAStep[rule_,a_]:=Map[rule[[10-#]]\u0026,ListConvolve[{{0,2,0},{2,1,2},{0,2,0}},a,2],{2}];",
				"code=465; stages=128;",
				"rule=IntegerDigits[code,2,10];",
				"g=2*stages+1; (* Maximum size of grid *)",
				"a=PadLeft[{{1}},{g,g},0,Floor[{g,g}/2]]; (* Initial ON cell on grid *)",
				"ca=a;",
				"ca=Table[ca=CAStep[rule,ca],{n,1,stages+1}];",
				"PrependTo[ca,a];",
				"(* Trim full grid to reflect growth by one cell at each stage *)",
				"k=(Length[ca[[1]]]+1)/2;",
				"ca=Table[Table[Part[ca[[n]][[j]],Range[k+1-n,k-1+n]],{j,k+1-n,k-1+n}],{n,1,k}];",
				"Map[Function[Apply[Plus,Flatten[#1]]],ca] (* Count ON cells at each stage *)"
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Robert Price_, Apr 25 2016",
			"references": 3,
			"revision": 9,
			"time": "2016-04-27T13:25:15-04:00",
			"created": "2016-04-27T03:42:05-04:00"
		}
	]
}