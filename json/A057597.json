{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A057597",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 57597,
			"data": "0,0,1,-1,0,2,-3,1,4,-8,5,7,-20,18,9,-47,56,0,-103,159,-56,-206,421,-271,-356,1048,-963,-441,2452,-2974,81,5345,-8400,3136,10609,-22145,14672,18082,-54899,51489,21492,-127880,157877,-8505,-277252,443634,-174887,-545999,1164520,-793408,-917111",
			"name": "a(n) = -a(n-1) - a(n-2) + a(n-3), a(0)=0, a(1)=0, a(2)=1.",
			"comment": [
				"Reflected (A074058) tribonacci numbers A000073: A000073(n) = a(1-n).",
				"There is an alternative way to produce this sequence, from A000073, which is 0,0,1,1,2,4,7,13,24,44,... Call this {b(n)}. Taking x1 = (b(2))^2 - b(1)*b(3) = 0; x2 = (b(3))^2 - b(2)*b(4) = 1; x3 = (b(4))^2 - b(3)*b(5) = -1; x4 = 0, x5 = 2, we generate (0),0,1,-1,0,2,-3,1. - _John McNamara_, Jan 02 2004",
				"Pisano period lengths: 1, 4, 13, 8, 31, 52, 48, 16, 39, 124, 110, 104, 168, 48, 403, 32, 96, 156, 360, 248, ... - _R. J. Mathar_, Aug 10 2012",
				"The negative powers of the tribonacci constant t = A058265 are t^(-n) = a(n+1)*t^2 + b(n)*t + a(n+2)*1, for n \u003e= 0, with b(n) = A319200(n) = -(a(n+1) - a(n)), for n \u003e= 0. 1/t =  t^2 - t - 1 = A192918. See the example in A319200 for the first powers. - _Wolfdieter Lang_, Oct 23 2018"
			],
			"reference": [
				"Petho Attila, Posting to Number Theory List (NMBRTHRY(AT)LISTSERV.NODAK.EDU), Oct 06 2000."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A057597/b057597.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"M. Janjic, \u003ca href=\"http://arxiv.org/abs/1112.2466\"\u003eRecurrence Relations and Determinants\u003c/a\u003e, arXiv preprint arXiv:1112.2466 [math.CO], 2011.",
				"M. Janjic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL15/Janjic/janjic42.html\"\u003eDeterminants and Recurrence Sequences\u003c/a\u003e, Journal of Integer Sequences, 15 (2012), Article 12.3.5. - From _N. J. A. Sloane_, Sep 16 2012.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (-1,-1,1)."
			],
			"formula": [
				"G.f.: x^2/(1+x+x^2-x^3).",
				"G.f.: Q(0)*x^2/2, where Q(k) = 1 + 1/(1 - x*(4*k+1 + x - x^2)/( x*(4*k+3 + x - x^2) - 1/Q(k+1) )); (continued fraction). - _Sergei N. Gladkovskii_, Sep 09 2013",
				"G.f. -x*T(1/x), where T is the g.f. of A000073. - _Wolfdieter Lang_, Oct 26 2018"
			],
			"maple": [
				"seq(coeff(series(x^2/(1+x+x^2-x^3),x,n+1), x, n), n = 0 .. 50); # _Muniru A Asiru_, Oct 23 2018"
			],
			"mathematica": [
				"CoefficientList[Series[x^2/(1+x+x^2-x^3), {x, 0, 50}], x]"
			],
			"program": [
				"(PARI) {a(n) = polcoeff( if( n\u003c0, x / ( 1 - x - x^2 - x^3), x^2 / ( 1 + x + x^2 - x^3) ) + x*O(x^abs(n)), abs(n))} /* _Michael Somos_, Sep 03 2007 */",
				"(Haskell)",
				"a057597 n = a057597_list !! n",
				"a057597_list = 0 : 0 : 1 : zipWith3 (\\x y z -\u003e - x - y + z)",
				"               (drop 2 a057597_list) (tail a057597_list) a057597_list",
				"-- _Reinhard Zumkeller_, Oct 07 2012",
				"(GAP) a:=[0,0,1];;  for n in [4..55] do a[n]:=-a[n-1]-a[n-2]+a[n-3]; od; a; # _Muniru A Asiru_, Oct 23 2018"
			],
			"xref": [
				"Cf. A000073, A058265, A319200. First differences of A077908."
			],
			"keyword": "sign,easy",
			"offset": "0,6",
			"author": "_N. J. A. Sloane_, Oct 06 2000",
			"ext": [
				"Deleted certain dangerous or potentially dangerous links. - _N. J. A. Sloane_, Jan 30 2021"
			],
			"references": 14,
			"revision": 64,
			"time": "2021-01-30T15:02:57-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}