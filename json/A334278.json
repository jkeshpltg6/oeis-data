{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A334278",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 334278,
			"data": "0,1,0,-1,1,0,-3,6,-4,1,0,-133,423,-572,441,-214,66,-12,1,0,-3040575,14412776,-31680240,43389646,-41821924,30276984,-17100952,7701952,-2794896,818036,-191600,35264,-4936,496,-32,1",
			"name": "Irregular table read by rows: T(n, k) is the coefficient of x^k in the chromatic polynomial of the cubical graph Q_n, 0 \u003c= k \u003c= 2^n.",
			"comment": [
				"Conjecture: The sums of the absolute values of the entries in each row gives A334247, the number of acyclic orientations of edges of the n-cube."
			],
			"link": [
				"Peter Kagey, \u003ca href=\"/A334278/b334278.txt\"\u003eTable of n, a(n) for n = 0..68\u003c/a\u003e (rows 0..5; row 5 from Andrew Howroyd's file)",
				"Andrew Howroyd, \u003ca href=\"/A334159/a334159.txt\"\u003eChromatic Polynomials of Hypercubes Q_0 to Q_5\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ChromaticPolynomial.html\"\u003eChromatic Polynomial\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/HypercubeGraph.html\"\u003eHypercube Graph\u003c/a\u003e."
			],
			"formula": [
				"T(n,0) = 0.",
				"T(n,k) = Sum_{i=1..2^n}, Stirling1(i,k) * A334159(n,i). - _Andrew Howroyd_, Apr 25 2020"
			],
			"example": [
				"Table begins:",
				"n/k| 0     1    2     3    4     5   6    7  8",
				"---+-------------------------------------------",
				"  0| 0,    1",
				"  1| 0,   -1,   1",
				"  2| 0,   -3,   6,   -4,   1",
				"  3| 0, -133, 423, -572, 441, -214, 66, -12, 1"
			],
			"mathematica": [
				"T[n_, k_] := Coefficient[ChromaticPolynomial[HypercubeGraph[n], x], x, k]"
			],
			"xref": [
				"Cf. A296914 is the reverse of row 3.",
				"Cf. A334279 is analogous for the n-dimensional cross-polytope, the dual of the n-cube.",
				"Cf. A002378, A091940, A140986, A158348.",
				"Cf. A334159, A334247, A334358."
			],
			"keyword": "sign,more,tabf",
			"offset": "0,7",
			"author": "_Peter Kagey_, Apr 21 2020",
			"references": 5,
			"revision": 27,
			"time": "2021-02-28T19:56:44-05:00",
			"created": "2020-04-28T00:45:34-04:00"
		}
	]
}