{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A015440",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 15440,
			"data": "1,1,6,11,41,96,301,781,2286,6191,17621,48576,136681,379561,1062966,2960771,8275601,23079456,64457461,179854741,502142046,1401415751,3912125981,10919204736,30479834641,85075858321,237475031526",
			"name": "Generalized Fibonacci numbers.",
			"comment": [
				"The compositions of n in which each natural number is colored by one of p different colors are called p-colored compositions of n. For n \u003e= 2, 6*a(n-2) equals the number of 6-colored compositions of n with all parts \u003e= 2, such that no adjacent parts have the same color. - _Milan Janjic_, Nov 26 2011",
				"Pisano period lengths: 1, 3, 6, 6, 1, 6, 21, 12, 18, 3, 40, 6, 56, 21, 6, 24, 16, 18, 360, 6, .... - _R. J. Mathar_, Aug 10 2012"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A015440/b015440.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Joerg Arndt, \u003ca href=\"http://www.jjj.de/fxt/#fxtbook\"\u003eMatters Computational (The Fxtbook)\u003c/a\u003e, section 14.8, pp. 317-318",
				"M. Janjic, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL18/Janjic/janjic63.html\"\u003eOn Linear Recurrence Equations Arising from Compositions of Positive Integers\u003c/a\u003e, Journal of Integer Sequences, Vol. 18 (2015), Article 15.4.7.",
				"A. G. Shannon, J. V. Leyendekkers, \u003ca href=\"http://nntdm.net/volume-21-2015/number-2/35-42/\"\u003eThe Golden Ratio family and the Binet equation\u003c/a\u003e, Notes on Number Theory and Discrete Mathematics, Vol. 21, No. 2, (2015), 35-42.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,5)."
			],
			"formula": [
				"a(n) = a(n-1) + 5 a(n-2).",
				"a(n) = (( (1+sqrt(21))/2 )^(n+1) - ( (1-sqrt(21))/2 )^(n+1))/sqrt(21).",
				"a(n) = Sum_{k=0..ceiling(n/2)} 5^k*binomial(n-k, k). - _Benoit Cloitre_, Mar 06 2004",
				"G.f.: 1/(1 - x - 5x^2). - _R. J. Mathar_, Sep 03 2008",
				"a(n) = Sum_{k=0..n} A109466(n,k)*(-5)^(n-k). - _Philippe Deléham_, Oct 26 2008",
				"From _Jeffrey R. Goodwin_, May 28 2011: (Start)",
				"A special case of a more general class of Lucas sequences given by",
				"U(n) = U(n-1) + (4^(m-1)-1)/3 U(n-2).",
				"U(n) = (( (1+sqrt((4^(m)-1)/3))/2 )^(n+1) - ( (1-sqrt((4^(m)-1)/3))/2 )^(n+1))/sqrt((4^(m)-1)/3). Fix m = 2 to get the formula for the Fibonacci sequence, fix m = 3 to get the formula for a(n). (End)",
				"G.f.: G(0)/(2-x), where G(k)= 1 + 1/(1 - x*(21*k-1)/(x*(21*k+20) - 2/G(k+1))); (continued fraction). - _Sergei N. Gladkovskii_, Jun 20 2013",
				"G.f.: Q(0)/x -1/x, where Q(k) = 1 + 5*x^2 + (k+2)*x - x*(k+1 + 5*x)/Q(k+1); (continued fraction). - _Sergei N. Gladkovskii_, Oct 06 2013",
				"a(n) = (Sum_{k=1..n+1, k odd} binomial(n+1,k)*21^((k-1)/2))/2^n. - _Vladimir Shevelev_, Feb 05 2014"
			],
			"maple": [
				"A015440 := proc(n)",
				"    if n \u003c= 1 then",
				"        1;",
				"    else",
				"        procname(n-1)+5*procname(n-2) ;",
				"    end if;",
				"end proc: # _R. J. Mathar_, May 15 2016"
			],
			"mathematica": [
				"a[n_]:=(MatrixPower[{{1,3},{1,-2}},n].{{1},{1}})[[2,1]]; Table[Abs[a[n]],{n,-1,40}] (* _Vladimir Joseph Stephan Orlovsky_, Feb 19 2010 *)",
				"LinearRecurrence[{1, 5}, {1, 1}, 100] (* _Vincenzo Librandi_, Nov 06 2012 *)"
			],
			"program": [
				"(Sage) [lucas_number1(n,1,-5) for n in range(1, 28)] # _Zerinvary Lajos_, Apr 22 2009",
				"(MAGMA) [n le 2 select 1 else Self(n-1)+5*Self(n-2): n in [1..30]]; // _Vincenzo Librandi_, Nov 06 2012",
				"(PARI) a(n)=abs([1,3;1,-2]^n*[1;1])[2,1] \\\\ _Charles R Greathouse IV_, Feb 03 2014"
			],
			"xref": [
				"Cf. A006130, A006131, A015441."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Olivier Gérard_",
			"references": 22,
			"revision": 77,
			"time": "2019-12-07T12:18:19-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}