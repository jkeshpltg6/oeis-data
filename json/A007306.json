{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007306",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7306,
			"id": "M0437",
			"data": "1,1,2,3,3,4,5,5,4,5,7,8,7,7,8,7,5,6,9,11,10,11,13,12,9,9,12,13,11,10,11,9,6,7,11,14,13,15,18,17,13,14,19,21,18,17,19,16,11,11,16,19,17,18,21,19,14,13,17,18,15,13,14,11,7,8,13,17,16,19,23,22,17,19,26,29,25,24",
			"name": "Denominators of Farey tree fractions (i.e., the Stern-Brocot subtree in the range [0,1]).",
			"comment": [
				"Also number of odd entries in n-th row of triangle of Stirling numbers of the second kind (A008277). - _Benoit Cloitre_, Feb 28 2004",
				"Apparently (except for the first term) the number of odd entries in the alternated diagonals of Pascal's triangle at 45 degrees slope. - Javier Torres (adaycalledzero(AT)hotmail.com), Jul 26 2009",
				"The Kn3 and Kn4 triangle sums, see A180662 for their definitions, of Sierpiński's triangle A047999 equal a(n+1). - _Johannes W. Meijer_, Jun 05 2011",
				"From _Yosu Yurramendi_, Jun 23 2014: (Start)",
				"If the terms (n\u003e1) are written as an array:",
				"  2,",
				"  3,  3,",
				"  4,  5,  5,  4,",
				"  5,  7,  8,  7,  7,  8,  7,  5,",
				"  6,  9, 11, 10, 11, 13, 12,  9,  9, 12, 13, 11, 10, 11,  9,  6,",
				"  7, 11, 14, 13, 15, 18, 17, 13, 14, 19, 21, 18, 17, 19, 16, 11, 11, 16, 19,17,18,",
				"then the sum of the k-th row is 2*3^(k-2), each column is an arithmetic progression. The differences of the arithmetic progressions give the sequence itself (a(2^(m+1)+1+k) - a(2^m+1+k) = a(k+1), m \u003e= 1, 1 \u003c= k \u003c= 2^m), because a(n) = A002487(2*n-1) and A002487 has these properties. A071585 also has these properties. Each row is a palindrome: a(2^(m+1)+1-k) = a(2^m+k), m \u003e= 0, 1 \u003c= k \u003c= 2^m.",
				"If the terms (n\u003e0) are written in this way:",
				"  1,",
				"  2, 3,",
				"  3, 4,  5,  5,",
				"  4, 5,  7,  8,  7,  7,  8,  7,",
				"  5, 6,  9, 11, 10, 11, 13, 12,  9,  9, 12, 13, 11, 10, 11,  9,",
				"  6, 7, 11, 14, 13, 15, 18, 17, 13, 14, 19, 21, 18, 17, 19, 16, 11, 11, 16, 19,",
				"each column is an arithmetic progression and the steps also give the sequence itself (a(2^(m+1)+k) - a(2^m+k) = a(k), m \u003e= 0, 0 \u003c= k \u003c 2^m). Moreover, by removing the first term of each column:",
				"a(2^(m+1)+k) = A049448(2^m+k+1), m \u003e= 0, 0 \u003c= k \u003c 2^m.",
				"(End)",
				"k \u003e 1 occurs in this sequence phi(k) = A000010(k) times. - _Franklin T. Adams-Watters_, May 25 2015",
				"Except for the initial 1, this is the odd bisection of A002487. The even bisection of A002487 is A002487 itself. - _Franklin T. Adams-Watters_, May 25 2015",
				"For all m \u003e= 0, max_{k=1..2^m} a(2^m+k) = A000045(m+3) (Fibonacci sequence). - _Yosu Yurramendi_, Jun 05 2016",
				"For all n \u003e= 2, max(m: a(2^m+k) = n, 1\u003c=k\u003c=2^m) = n-2. - _Yosu Yurramendi_, Jun 05 2016",
				"a(2^m+1) = m+2, m \u003e= 0; a(2^m+2) = 2m+1, m\u003e=1; min_{m\u003e=0, k=1..2^m} a(2^m+k) = m+2; min_{m\u003e=2, k=2..2^m-1} a(2^m+k) = 2m+1. - _Yosu Yurramendi_, Jun 06 2016",
				"a(2^(m+2) + 2^(m+1) - k) - a(2^(m+1) + 2^m-k) = 2*a(k+1), m \u003e= 0, 0 \u003c= k \u003c= 2^m. - _Yosu Yurramendi_, Jun 09 2016",
				"If the initial 1 is omitted, this is the number of nonzero entries in row n of the generalized Pascal triangle P_2, see A282714 [Leroy et al., 2017]. - _N. J. A. Sloane_, Mar 02 2017",
				"Apparently, this sequence was introduced by Johann Gustav Hermes in 1894. His paper gives a strong connection between this sequence and the so-called \"Gaussian brackets\" (\"Gauss'schen Klammer\"). For an independent discussion about Gaussian brackets, see the relevant MathWorld article and the article by Herzberger (1943). Srinivasan (1958) gave another, more modern, explanation of the connection between this sequence and the Gaussian brackets. (Parenthetically, J. G. Hermes is the mathematician who completed or constructed the regular polygon with 65537 sides.) - _Petros Hadjicostas_, Sep 18 2019"
			],
			"reference": [
				"P. Bachmann, Niedere Zahlentheorie (1902, 1910), reprinted Chelsea, NY, 1968, vol. 2, p. 61.",
				"L. E. Dickson, History of the Theory of Numbers. Carnegie Institute Public. 256, Washington, DC, Vol. 1, 1919; Vol. 2, 1920; Vol. 3, 1923, see vol. 1, p. 158.",
				"J. C. Lagarias, Number Theory and Dynamical Systems, pp. 35-72 of S. A. Burr, ed., The Unreasonable Effectiveness of Number Theory, Proc. Sympos. Appl. Math., 46 (1992). Amer. Math. Soc.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A007306/b007306.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Suayb S. Arslan, \u003ca href=\"https://arxiv.org/abs/1709.07446\"\u003e Asymptotically MDS Array BP-XOR Codes\u003c/a\u003e, arXiv:1709.07949 [cs.IT], 2017.",
				"Alexander Bogomolny, \u003ca href=\"http://www.cut-the-knot.org/blue/Stern.shtml\"\u003eStern-Brocot tree\u003c/a\u003e.",
				"J. Hermes, \u003ca href=\"https://eudml.org/doc/157736\"\u003eAnzahl der Zerlegungen einer ganzen, rationalen Zahl in Summanden (The number of partitions of a rational integer, Part I)\u003c/a\u003e, Mathematischen Annalen 45(3) (1894), 371-380.",
				"J. Hermes, \u003ca href=\"https://doi.org/10.1007/BF01446684\"\u003eAnzahl der Zerlegungen einer ganzen, rationalen Zahl in Summanden (The number of partitions of a rational integer, Part I)\u003c/a\u003e, Mathematischen Annalen 45(3) (1894), 371-380.",
				"J. Hermes, \u003ca href=\"https://doi.org/10.1007/BF01447271\"\u003eAnzahl der Zerlegungen einer ganzen, rationalen Zahl in Summanden, II (The number of partitions of a rational integer, Part II)\u003c/a\u003e, Mathematischen Annalen 47(2-3) (1896), 281-297.",
				"M. Herzberger, \u003ca href=\"https://doi.org/10.1364/JOSA.33.000651\"\u003eGaussian optics and Gaussian brackets\u003c/a\u003e, Journal of the Optical Society of America 33(12) (1943), 651-655. [This paper gives a clear description of Gaussian brackets that are related to this sequence as explained by Hermes (1894).]",
				"Jennifer Lansing, \u003ca href=\"http://hdl.handle.net/2142/49483\"\u003eOn the Stern sequence and a related sequence\u003c/a\u003e, Ph.D. dissertation in Mathematics, University of Illinois at Urbana-Champaign, 2014. [This doctoral dissertation discusses the so-called Stern sequence on which Hermes' papers are based (according to Srinivasan (1958)).]",
				"Julien Leroy, Michel Rigo, and Manon Stipulanti, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2017.01.003\"\u003eCounting the number of non-zero coefficients in rows of generalized Pascal triangles\u003c/a\u003e, Discrete Mathematics 340 (2017), 862-881.",
				"Julien Leroy, Michel Rigo, and Manon Stipulanti, \u003ca href=\"http://math.colgate.edu/~integers/sjs13/sjs13.Abstract.html\"\u003eCounting subword occurrences in base-b expansions\u003c/a\u003e, Integers (2018) 18A, Article #A13.",
				"G. Melancon, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(99)00123-5\"\u003eLyndon factorization of sturmian words\u003c/a\u003e, Discr. Math. 210 (2000), 137-149.",
				"N. J. A. Sloane, \u003ca href=\"/stern_brocot.html\"\u003eStern-Brocot or Farey Tree\u003c/a\u003e.",
				"M. S. Srinivasan, \u003ca href=\"https://www.ias.ac.in/article/fulltext/seca/047/01/0012-0024\"\u003eThe enumeration of positive rational numbers\u003c/a\u003e, Proc. Indian Acad. Sci. Sect. A 47 (1958), 12-24.",
				"M. Stern, \u003ca href=\"https://eudml.org/doc/147729\"\u003eÜber eine zahlentheoretische Function\u003c/a\u003e, Journal für die reine und angewandte Mathematik 55 (1858), 193-220. [According to Srinivasan (1958), Hermes's (1894) paper, where this sequence is introduced, is based on Stern's sequence.]",
				"Manon Stipulanti, \u003ca href=\"https://arxiv.org/abs/1801.03287\"\u003eConvergence of Pascal-Like Triangles in Parry-Bertrand Numeration Systems\u003c/a\u003e, arXiv:1801.03287 [math.CO], 2018.",
				"Javier Torres Suarez, \u003ca href=\"https://www.youtube.com/watch?v=__Re3zKM9n8\"\u003eNumber theory - geometric connection (part 2)\u003c/a\u003e (YouTube video that mentions this sequence - link sent by Pacha Nambi, Aug 26 2009).",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GaussianBrackets.html\"\u003eGaussian brackets\u003c/a\u003e; they are related to this sequence.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Johann_Gustav_Hermes\"\u003eJohann Gustav Hermes\u003c/a\u003e. [He is the person who introduced this sequence and the person who completed or constructed a regular polygon with 65537 sides.]",
				"\u003ca href=\"/index/Fo#fraction_trees\"\u003eIndex entries for fraction trees\u003c/a\u003e",
				"\u003ca href=\"/index/St#Stern\"\u003eIndex entries for sequences related to Stern's sequences\u003c/a\u003e"
			],
			"formula": [
				"Recurrence: a(0) to a(8) are 1, 1, 2, 3, 3, 4, 5, 5, 4; thereafter a(n) = a(n-2^p) + a(2^(p+1)-n+1), where 2^p \u003c n \u003c= 2^(p+1). [J. Hermes, Math. Ann., 1894; quoted by Dickson, Vol. 1, p. 158] - _N. J. A. Sloane_, Mar 24 2019",
				"For n \u003e 0, a(n) = A002487(n-1) + A002487(n) = A002487(2*n-1).",
				"a(0) = 1; a(n) = Sum_{k=0..n-1} C(n-1+k, n-1-k) mod 2, n \u003e 0. - _Benoit Cloitre_, Jun 20 2003",
				"a(n+1) = Sum_{k=0..n} binomial(2*n-k, k) mod 2; a(n) = 0^n + Sum_{k=0..n-1} binomial(2(n-1)-k, k) mod 2. - _Paul Barry_, Dec 11 2004",
				"a(n) = Sum_{k=0..n} C(n+k,2*k) mod 2. - _Paul Barry_, Jun 12 2006",
				"a(0) = a(1) = 1; a(n) = a(A003602(n-1)) + a(A003602(n)), n \u003e 1. - _Alessandro De Luca_, May 08 2014",
				"a(n) = A007305(n+(2^m-1)), m=A029837(n), n=1,2,3,... . - _Yosu Yurramendi_, Jul 04 2014",
				"a(n) = A007305(2^(m+1)-n) - A007305(2^m-n), m \u003e= (A029837(n)+1), n=1,2,3,... - _Yosu Yurramendi_, Jul 05 2014",
				"a(2^m) = m+1, a(2^m+1) = m+2 for m \u003e= 0. - _Yosu Yurramendi_, Jan 01 2015",
				"a(n+2) = A007305(n+2) + A047679(n) n \u003e= 0. - _Yosu Yurramendi_, Mar 30 2016",
				"a(2^m+2^r+k) = a(2^r+k)(m-r+1) - a(k), m \u003e= 2, 0 \u003c= r \u003c= m-1, 0 \u003c= k \u003c 2^r. Example: a(73) = a(2^6+2^3+1) = a(2^3+1)*(6-3+1) - a(1) = 5*4 - 1 = 19 . - _Yosu Yurramendi_, Jul 19 2016",
				"From _Antti Karttunen_, Mar 21 2017 \u0026 Apr 12 2017: (Start)",
				"For n \u003e 0, a(n) = A001222(A277324(n-1)) = A001222(A260443(n-1)*A260443(n)).",
				"The following decompositions hold for all n \u003e 0:",
				"a(n) = A277328(n-1) + A284009(n-1).",
				"a(n) = A283986(n) + A283988(n) = A283987(n) + 2*A283988(n).",
				"a(n) = 2*A284265(n-1) + A284266(n-1).",
				"a(n) = A284267(n-1) + A284268(n-1).",
				"a(n) = A284565(n-1) + A284566(n-1).",
				"a(n) = A285106(n-1) + A285108(n-1) = A285107(n-1) + 2*A285108(n-1). (End)",
				"a(A059893(n)) = a(n+1) for n \u003e 0. - _Yosu Yurramendi_, May 30 2017",
				"a(n) = A287731(n) + A287732(n) for n \u003e 0. - _I. V. Serov_, Jun 09 2017",
				"a(n) = A287896(n) + A288002(n) for n \u003e 1.",
				"a(n) = A287896(n-1) + A002487(n-1) - A288002(n-1) for n \u003e 1.",
				"a(n) = a(n-1) + A002487(n-1) - 2*A288002(n-1) for n \u003e 1. - _I. V. Serov_, Jun 14 2017",
				"From _Yosu Yurramendi_, May 14 2019: (Start)",
				"For m \u003e= 0, M \u003e= m, 0 \u003c= k \u003c 2^m,",
				"a((2^(m+1) + A119608(2^m+k+1))*2^(M-m) - A000035(2^m+k)) =",
				"a((2^(m+2) - A119608(2^m+k+1))*2^(M-m) - A000035(2^m+k)-1) =",
				"a(2^(M+2) - (2^m+k)) = a(2^(M+1) + (2^m+k) + 1) =",
				"a(2^m+k+1)*(M-m) + a(2^(m+1)+2^m+k+1). (End)",
				"a(k) = sqrt(A007305(2^(m+1)+k)*A047679(2^(m+1)+k-2) - A007305(2^m+k)*A047679(2^m+k-2)), m \u003e= 0, 0 \u003c= k \u003c 2^m. - _Yosu Yurramendi_, Jun 09 2019",
				"G.f.: 1 + x * (1 + x) * Product_{k\u003e=0} (1 + x^(2^k) + x^(2^(k+1))). - _Ilya Gutkovskiy_, Jul 19 2019"
			],
			"example": [
				"[ 0/1; 1/1; ] 1/2; 1/3, 2/3; 1/4, 2/5, 3/5, 3/4; 1/5, 2/7, 3/8, 3/7, 4/7, 5/8, 5/7, 4/5; ..."
			],
			"maple": [
				"A007306 := proc(n): if n=0 then 1 else A002487(2*n-1) fi: end: A002487 := proc(m) option remember: local a, b, n; a := 1; b := 0; n := m; while n\u003e0 do if type(n, odd) then b := a + b else a := a + b end if; n := floor(n/2); end do; b; end proc: seq(A007306(n),n=0..77); # _Johannes W. Meijer_, Jun 05 2011"
			],
			"mathematica": [
				"a[0] = 1; a[n_] := Sum[ Mod[ Binomial[n+k-1, 2k] , 2], {k, 0, n}]; Table[a[n], {n, 0, 77}] (* _Jean-François Alcover_, Dec 16 2011, after _Paul Barry_ *)",
				"a[0] = 0; a[1] = 1;",
				"Flatten[{1,Table[a[2*n] = a[n]; a[2*n + 1] = a[n] + a[n + 1], {n, 0, 50}]}] (* _Horst H. Manninger_, Jun 09 2021 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n==0, n--; sum( k=0, n, binomial( n+k, n-k)%2))};",
				"(PARI) {a(n) = my(m); if( n\u003c2, n\u003e=0, m = 2^length( binary( n-1)); a(n - m/2) + a(m-n+1))}; /* _Michael Somos_, May 30 2005 */",
				"(Sage)",
				"@CachedFunction",
				"def a(n):",
				"    return a((odd_part(n-1)+1)/2)+a((odd_part(n)+1)/2) if n\u003e1 else 1",
				"[a(n) for n in (0..77)] # after _Alessandro De Luca_, _Peter Luschny_, May 20 2014",
				"(Sage)",
				"def A007306(n):",
				"    if n == 0: return 1",
				"    M = [1, 1]",
				"    for b in (n-1).bits():",
				"        M[b] = M[0] + M[1]",
				"    return M[1]",
				"print([A007306(n) for n in (0..77)]) # _Peter Luschny_, Nov 28 2017",
				"(R)",
				"maxrow \u003c- 6 # by choice",
				"a \u003c- c(1,2)",
				"for(m in 0:maxrow) for(k in 1:2^m){",
				"  a[2^(m+1)+k  ] \u003c- a[2^m+k] + a[k]",
				"  a[2^(m+1)-k+1] \u003c- a[2^m+k]",
				"}",
				"a",
				"# _Yosu Yurramendi_, Jan 05 2015",
				"(R)",
				"# Given n, compute directly a(n)",
				"# by taking into account the binary representation of n-1",
				"# aa \u003c- function(n){",
				"  b \u003c- as.numeric(intToBits(n))",
				"  l \u003c- sum(b)",
				"  m \u003c- which(b == 1)-1",
				"  d \u003c- 1",
				"  if(l \u003e 1) for(j in 1:(l-1)) d[j] \u003c- m[j+1]-m[j]+1",
				"  f \u003c- c(1,m[1]+2) # In A002487: f \u003c- c(0,1)",
				"  if(l \u003e 1) for(j in 3:(l+1)) f[j] \u003c- d[j-2]*f[j-1]-f[j-2]",
				"  return(f[l+1])",
				"}",
				"# a(0) = 1, a(1) = 1, a(n) = aa(n-1)   n \u003e 1",
				"#",
				"# Example",
				"n \u003c- 73",
				"aa(n-1)",
				"#",
				"# _Yosu Yurramendi_, Dec 15 2016",
				"(Scheme) (define (A007306 n) (if (zero? n) 1 (A002487 (+ n n -1)))) ;; Code for A002487 given in that entry. - _Antti Karttunen_, Mar 21 2017",
				"(Python)",
				"from sympy import binomial",
				"def a(n):",
				"    return 1 if n\u003c1 else sum(binomial(n + k - 1, 2*k) % 2 for k in range(n + 1))",
				"print([a(n) for n in range(101)]) # _Indranil Ghosh_, Mar 22 2017",
				"(MAGMA) [1] cat [\u0026+[Binomial(n+k,2*k) mod 2: k in [0..n]]: n in [0..80]]; // _Vincenzo Librandi_, Jun 10 2019"
			],
			"xref": [
				"For numerators see A007305.",
				"Cf. A001222, A002487, A006842, A006843, A047679, A054424, A065674-A065675, A065810, A260443, A277324, A277328, A283986, A283987, A283988, A284009, A284265, A284266, A284267, A284268, A284565, A284566, A285106, A285107, A285108, A287731, A287732.",
				"See also A282714."
			],
			"keyword": "nonn,frac,tabf,nice",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Formula fixed and extended by _Franklin T. Adams-Watters_, Jul 07 2009",
				"Incorrect Maple program removed by _Johannes W. Meijer_, Jun 05 2011"
			],
			"references": 89,
			"revision": 282,
			"time": "2021-06-12T09:24:06-04:00",
			"created": "1994-04-28T03:00:00-04:00"
		}
	]
}