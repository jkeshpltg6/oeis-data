{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A288869",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 288869,
			"data": "1,-1,4,-21,136,-1045,9276,-93289,1047376,-12975561,175721140,-2581284541,40864292184,-693347907421,12548540320876,-241253367679185,4909234733857696,-105394372192969489,2380337795595885156",
			"name": "Numerators of z-sequence for the Sheffer matrix T = P*Lah = A271703 = A007318*A271703 = (exp(t), t/(1-t)).",
			"comment": [
				"The denominators seem to be the natural numbers A000027.",
				"The z-sequence gives the recurrence for column k=0 entries of the triangle T = A271703 (using also lower rows of T): T(n, 0) = Sum_{j=0..n-1} z(j)*T(n-1, j), n \u003e=1, with T(0, 0) = 1. Because column k=0 has e.g.f. exp(x) all entries T(n, 0) = 1, and one obtains rational representations of 1 by the z-sequence recurrence. See the examples."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A288869/b288869.txt\"\u003eTable of n, a(n) for n = 0..250\u003c/a\u003e",
				"\u003ca href=\"/index/La#Laguerre\"\u003eIndex entries for sequences related to Laguerre polynomials\u003c/a\u003e"
			],
			"formula": [
				"E.g.f. for the rationals r(n): ((1+x)/x)*(1 - exp(-x/(1+x))).",
				"a(n) = numerator(r(n)) (in lowest terms).",
				"From _G. C. Greubel_, Jan 09 2022: (Start)",
				"a(n) = (-1)^n * (n+1)! * Laguerre(n+1, -2, -1), with a(0) = 1.",
				"a(n) = (-1)^n * (n+1)! * Sum_{j=0..n-1} binomial(n-1, j)/(j+2)!, with a(0) = 1. (End)"
			],
			"example": [
				"The rationals r(n) begin: 1, -1/2, 4/3, -21/4, 136/5, -1045/6, 9276/7, -93289/8, 1047376/9, -12975561/10, 175721140/11, -2581284541/12, 40864292184/13, -693347907421/14, 12548540320876/15, ...",
				"Recurrence with T= P*Lah = A271703, rational representations of 1:",
				"  1 = T(2, 0) = 2*(1*T(1, 0) + (-1/2)*T(1, 1)) = 2*(1 - 1/2) = 1.",
				"  1 = T(3, 0) = 3*(1*1 + (-1/2)*4 + (4/3)*1) = 1.",
				"  1 = T(4, 0) = 4*(1*1 + (-1/2)*15 + (4/3)*9 + (-21/4)*1) = 1.",
				"  ..."
			],
			"mathematica": [
				"Table[If[n==0, 1, (-1)^n*(n+1)!*LaguerreL[n+1, -2, -1]], {n, 0, 30}] (* _G. C. Greubel_, Jan 09 2022 *)"
			],
			"program": [
				"(MAGMA) [1] cat [(-1)^n*Factorial(n+1)*Evaluate(LaguerrePolynomial(n+1, -2), -1): n in [1..30]]; // _G. C. Greubel_, Jan 09 2022",
				"(Sage) [1]+[(-1)^n*factorial(n+1)*gen_laguerre(n+1, -2, -1) for n in (1..30)] # _G. C. Greubel_, Jan 09 2022",
				"(PARI) a(n) = if (n, (-1)^n*(n+1)!*subst(pollaguerre(n+1, -2), x, -1), 1); \\\\ _Michel Marcus_, Jan 09 2022"
			],
			"xref": [
				"Cf. A000027, A007318, A271703."
			],
			"keyword": "sign,easy,frac,changed",
			"offset": "0,3",
			"author": "_Wolfdieter Lang_, Jun 20 2017",
			"references": 2,
			"revision": 14,
			"time": "2022-01-11T22:00:41-05:00",
			"created": "2017-06-20T23:19:12-04:00"
		}
	]
}