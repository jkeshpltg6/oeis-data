{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001011",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1011,
			"id": "M1455 N0576",
			"data": "1,1,2,5,14,38,120,353,1148,3527,11622,36627,121622,389560,1301140,4215748,14146335,46235800,155741571,512559195,1732007938,5732533570,19423092113,64590165281,219349187968,732358098471,2492051377341,8349072895553,28459491475593",
			"name": "Number of ways to fold a strip of n blank stamps.",
			"reference": [
				"M. Gardner, Mathematical Games, Sci. Amer. Vol. 209 (No. 3, Mar. 1963), p. 262.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence - see entry 576, Fig. 17, and the front cover).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A001011/b001011.txt\"\u003eTable of n, a(n) for n = 1..45\u003c/a\u003e [from S. Legendre, 2013]",
				"B. Bobier and J. Sawada, \u003ca href=\"http://www.cis.uoguelph.ca/~sawada/papers/meander.pdf\"\u003eA fast algorithm to generate open meandric systems and meanders\u003c/a\u003e, Transactions on Algorithms, Vol. 6 No. 2 (2010) 12 pages.",
				"S. P. Castell, \u003ca href=\"/A001011/a001011_3.pdf\"\u003eComputer Puzzles\u003c/a\u003e, Computer Bulletin, March 1975, pages 3, 33, 34. [Annotated scanned copy]",
				"CombOS - Combinatorial Object Server, \u003ca href=\"http://combos.org/meander\"\u003eGenerate meanders and stamp foldings\u003c/a\u003e",
				"Santo Diano, \u003ca href=\"/A001011/a001011_2.pdf\"\u003eLetter to N. J. A. Sloane, circa Dec 01 1979\u003c/a\u003e",
				"R. Dickau, \u003ca href=\"http://www.robertdickau.com/stampfolding.html\"\u003eStamp Folding\u003c/a\u003e",
				"R. Dickau, \u003ca href=\"/A000136/a000136_2.pdf\"\u003eStamp Folding\u003c/a\u003e [Cached copy, pdf format, with permission]",
				"R. Dickau, \u003ca href=\"http://www.robertdickau.com/unlabeledfoldings.html\"\u003eUnlabeled Stamp Foldings\u003c/a\u003e",
				"R. Dickau, \u003ca href=\"/A001011/a001011_1.pdf\"\u003eUnlabeled Stamp Foldings\u003c/a\u003e [Cached copy, pdf format, with permission]",
				"R. K. Guy, \u003ca href=\"/A005347/a005347.pdf\"\u003eThe Second Strong Law of Small Numbers\u003c/a\u003e, Math. Mag, 63 (1990), no. 1, 3-20. [Annotated scanned copy]",
				"J. E. Koehler, \u003ca href=\"http://dx.doi.org/10.1016/S0021-9800(68)80048-1\"\u003eFolding a strip of stamps\u003c/a\u003e, J. Combin. Theory, 5 (1968), 135-152.",
				"J. E. Koehler, \u003ca href=\"/A001011/a001011_4.pdf\"\u003eFolding a strip of stamps\u003c/a\u003e, J. Combin. Theory, 5 (1968), 135-152. [Annotated, corrected, scanned copy]",
				"S. Legendre, \u003ca href=\"http://arxiv.org/abs/1302.2025\"\u003eFoldings and Meanders\u003c/a\u003e, arXiv preprint arXiv:1302.2025 [math.CO], 2013.",
				"S. Legendre, \u003ca href=\"http://ajc.maths.uq.edu.au/pdf/58/ajc_v58_p275.pdf\"\u003eFoldings and Meanders\u003c/a\u003e, Aust. J. Comb. 58(2), 275-291, 2014.",
				"David Orden, \u003ca href=\"http://mappingignorance.org/2014/07/07/many-ways-can-fold-strip-stamps/\"\u003eIn how many ways can you fold a strip of stamps?\u003c/a\u003e, 2014.",
				"Frank Ruskey, \u003ca href=\"http://combos.org/meander\"\u003eInformation on Stamp Foldings\u003c/a\u003e",
				"J. Sawada and R. Li, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v19i2p43\"\u003eStamp foldings, semi-meanders, and open meanders: fast generation algorithms\u003c/a\u003e, Electronic Journal of Combinatorics, Volume 19 No. 2 (2012), P#43 (16 pages).",
				"N. J. A. Sloane, \u003ca href=\"/A001011/a001011.pdf\"\u003eIllustration of initial terms\u003c/a\u003e (Fig. 17 of the 1973 Handbook of Integer Sequences. The initial terms are also embossed on the front cover.)",
				"N. J. A. Sloane, \u003ca href=\"http://neilsloane.com/doc/sg.txt\"\u003eMy favorite integer sequences\u003c/a\u003e, in Sequences and their Applications (Proceedings of SETA '98).",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/StampFolding.html\"\u003eStamp Folding.\u003c/a\u003e",
				"\u003ca href=\"/index/Fo#fold\"\u003eIndex entries for sequences obtained by enumerating foldings\u003c/a\u003e"
			],
			"formula": [
				"a(n) = (A001010(n) + A000136(n)) / 4. - _Andrew Howroyd_, Dec 07 2015"
			],
			"mathematica": [
				"A000136 = Import[\"https://oeis.org/A000136/b000136.txt\", \"Table\"][[All, 2]];",
				"A001010 = Cases[Import[\"https://oeis.org/A001010/b001010.txt\", \"Table\"], {_, _}][[All, 2]];",
				"a[n_] := If[n == 1, 1, (A001010[[n]] + A000136[[n]])/4];",
				"Array[a, 45] (* _Jean-François Alcover_, Sep 04 2019 *)"
			],
			"xref": [
				"Cf. A000682, A086441."
			],
			"keyword": "nonn,nice",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_, _Stéphane Legendre_",
			"ext": [
				"a(17) and a(20) corrected by _Sean A. Irvine_, Mar 17 2013"
			],
			"references": 8,
			"revision": 102,
			"time": "2019-09-05T04:09:47-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}