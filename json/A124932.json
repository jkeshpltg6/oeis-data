{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A124932",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 124932,
			"data": "1,2,3,3,9,6,4,18,24,10,5,30,60,50,15,6,45,120,150,90,21,7,63,210,350,315,147,28,8,84,336,700,840,588,224,36,9,108,504,1260,1890,1764,1008,324,45,10,135,720,2100,3780,4410,3360,1620,450,55",
			"name": "Triangle read by rows: T(n,k) = k*(k+1)*binomial(n,k)/2 (1 \u003c= k \u003c= n).",
			"comment": [
				"Row sums = A001793: (1, 5, 18, 56, 160, 432, ...).",
				"Triangle is P*M, where P is the Pascal triangle as an infinite lower triangular matrix and M is an infinite bidiagonal matrix with (1,3,6,10,...) in the main diagonal and in the subdiagonal.",
				"This number triangle can be used as a control sequence when listing combinations of subsets as in Pascals triangle by assigning a number to each element that corresponds to the n:th subset that the element belongs to. One then gets number blocks whose sums are the terms in this number triangle. - _Mats Granvik_, Jan 14 2009"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A124932/b124932.txt\"\u003eRows n = 1..100 of triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = binomial(k+1,2)*binomial(n,k). - _G. C. Greubel_, Nov 19 2019"
			],
			"example": [
				"First few rows of the triangle:",
				"  1;",
				"  2,   3;",
				"  3,   9,   6;",
				"  4,  18,  24,  10;",
				"  5,  30,  60,  50,  15;",
				"  6,  45, 120, 150,  90,  21;",
				"  7,  63, 210, 350, 315, 147,  28;",
				"  ...",
				"From _Mats Granvik_, Dec 18 2009: (Start)",
				"The numbers in this triangle are sums of the following recursive number blocks:",
				"1................................",
				".................................",
				"11.....12........................",
				".................................",
				"111....112....123................",
				".......122.......................",
				".................................",
				"1111...1112...1123...1234........",
				".......1122...1223...............",
				".......1222...1233...............",
				".................................",
				"11111..11112..11123..11234..12345",
				".......11122..11223..12234.......",
				".......11222..12223..12334.......",
				".......12222..11233..12344.......",
				"..............12233..............",
				"..............12333..............",
				".................................",
				"(End)"
			],
			"maple": [
				"T:=(n,k)-\u003ek*(k+1)*binomial(n,k)/2: for n from 1 to 12 do seq(T(n,k),k=1..n) od; # yields sequence in triangular form"
			],
			"mathematica": [
				"Table[Binomial[k + 1, 2]*Binomial[n, k], {n,12}, {k,n}]//Flatten (* _G. C. Greubel_, Nov 19 2019 *)"
			],
			"program": [
				"(PARI) T(n,k) = binomial(k+1,2)*binomial(n,k); \\\\ _G. C. Greubel_, Nov 19 2019",
				"(MAGMA) B:=Binomial; [B(k+1,2)*B(n,k): k in [1..n], n in [1..12]]; // _G. C. Greubel_, Nov 19 2019",
				"(Sage) b=binomial; [[b(k+1,2)*b(n,k) for k in (1..n)] for n in (1..12)] # _G. C. Greubel_, Nov 19 2019",
				"(GAP) B:=Binomial;; Flat(List([1..12], n-\u003e List([1..n], k-\u003e B(k+1,2)* B(n,k) ))); # _G. C. Greubel_, Nov 19 2019"
			],
			"xref": [
				"Cf. A001793."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Gary W. Adamson_, Nov 12 2006",
			"ext": [
				"Edited by _N. J. A. Sloane_, Nov 24 2006"
			],
			"references": 2,
			"revision": 14,
			"time": "2019-11-19T16:35:15-05:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}