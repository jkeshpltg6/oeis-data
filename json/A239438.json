{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A239438",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 239438,
			"data": "1,1,3,4,6,7,10,12,15,19,22,26,31,35,40,46,51,57,64,70,77,85,92,100,109,117,126,136,145,155,166,176,187,199,210,222,235,247,260,274,287,301,316,330,345,361,376,392,409",
			"name": "Maximal number of points that can be placed on a triangular grid of side n so that there is no pair of adjacent points.",
			"comment": [
				"In other words, the independence number of the (n-1)-triangular grid graph.",
				"Apart from a(3) and a(5) same as A007997(n+4) and A058212(n+2). - _Eric W. Weisstein_, Jun 14 2017",
				"Also the independence number of the n-triangular honeycomb king graph. - _Eric W. Weisstein_, Sep 06 2017"
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A239438/b239438.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"A. V. Geramita, D. Gregory, and L. Roberts, \u003ca href=\"http://dx.doi.org/10.1016/0022-4049(86)90029-0\"\u003eMonomial ideals and points in projective space\u003c/a\u003e, J. Pure Applied Alg 40 (1986), pp. 33-62.",
				"Stan Wagon, \u003ca href=\"http://www.jstor.org/stable/10.4169/college.math.j.45.4.278\"\u003eGraph Theory Problems from Hexagonal and Traditional Chess\u003c/a\u003e, The College Mathematics Journal, Vol. 45, No. 4, September 2014, pp. 278-287.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/IndependenceNumber.html\"\u003eIndependence Number\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/TriangularGridGraph.html\"\u003eTriangular Grid Graph\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,-1,1,-2,1)."
			],
			"formula": [
				"a(n) = ceiling(n(n+1)/6) for n \u003e 5, see Geramita, Gregory, \u0026 Roberts theorem 5.4. - _Charles R Greathouse IV_, Dec 04 2014",
				"G.f.: x*(x^9-2*x^8+2*x^7-3*x^6+3*x^5-2*x^4+2*x^3-2*x^2+x-1) / ((x-1)^3*(x^2+x+1)). - _Colin Barker_, Feb 08 2015"
			],
			"example": [
				"On a triangular grid of side 5 at most a(5) = 6 points (X) can be placed so that there is no pair of adjacent points.",
				"      X",
				"     . .",
				"    X . X",
				"   . . . .",
				"  X . X . X"
			],
			"mathematica": [
				"Table[1/18 (Piecewise[{{28, n == 2 || n == 4}}, 10] + 3 n (3 + n) + 8 Cos[(2 n Pi)/3]), {n, 0, 20}] (* _Eric W. Weisstein_, Jun 14 2017 *)"
			],
			"program": [
				"(PARI) Vec(x*(x^9-2*x^8+2*x^7-3*x^6+3*x^5-2*x^4+2*x^3-2*x^2+x-1)/((x-1)^3*(x^2+x+1)) + O(x^100)) \\\\ _Colin Barker_, Feb 08 2015"
			],
			"xref": [
				"Cf. A007997, A058212, A239567."
			],
			"keyword": "nonn,easy",
			"offset": "1,3",
			"author": "_Heinrich Ludwig_, Mar 18 2014",
			"ext": [
				"Extended by _Charles R Greathouse IV_, Dec 04 2014"
			],
			"references": 5,
			"revision": 55,
			"time": "2017-09-06T21:10:01-04:00",
			"created": "2014-03-20T04:22:04-04:00"
		}
	]
}