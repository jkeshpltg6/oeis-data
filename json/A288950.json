{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A288950",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 288950,
			"data": "1,0,1,2,15,140,1575,20790,315315,5405400,103378275,2182430250,50414138775,1264936572900,34258698849375,996137551158750,30951416768146875,1023460181133390000,35885072600989486875,1329858572860198631250,51938365373373313209375",
			"name": "Number of relaxed compacted binary trees of right height at most one with empty initial and final sequence on level 0.",
			"comment": [
				"A relaxed compacted binary tree of size n is a directed acyclic graph consisting of a binary tree with n internal nodes, one leaf, and n pointers. It is constructed from a binary tree of size n, where the first leaf in a post-order traversal is kept and all other leaves are replaced by pointers. These links may point to any node that has already been visited by the post-order traversal. The right height is the maximal number of right-edges (or right children) on all paths from the root to any leaf after deleting all pointers. The number of unbounded relaxed compacted binary trees of size n is A082161(n). The number of relaxed compacted binary trees of right height at most one of size n is A001147(n). See the Genitrini et al. and Wallner link. - _Michael Wallner_, Apr 20 2017",
				"a(n) is the number of plane increasing trees with n+1 nodes where node 3 is at depth 1 on the right of node 2 and where the node n+1 has a left sibling. See the Wallner link. - _Michael Wallner_, Apr 20 2017"
			],
			"link": [
				"Antoine Genitrini, Bernhard Gittenberger, Manuel Kauers and Michael Wallner, \u003ca href=\"https://arxiv.org/abs/1703.10031\"\u003eAsymptotic Enumeration of Compacted Binary Trees\u003c/a\u003e, arXiv:1703.10031 [math.CO], 2017.",
				"Michael Wallner, \u003ca href=\"https://arxiv.org/abs/1703.10031\"\u003eA bijection of plane increasing trees with relaxed binary trees of right height at most one\u003c/a\u003e, arXiv:1706.07163 [math.CO], 2017."
			],
			"formula": [
				"E.g.f.: z + (1-z)/3*(2-z+(1-2*z)^(-1/2))."
			],
			"example": [
				"Denote by L the leaf and by o nodes. Every node has exactly two out-going edges or pointers. Internal edges are denoted by - or |. Pointers are omitted and may point to any node further right. The root is at level 0 at the very left.",
				"The general structure is",
				"  L-o-o-o-o-o-o-o-o-o",
				"    |       |     | |",
				"    o   o-o-o   o-o o.",
				"For n=0 the a(0)=1 solution is L.",
				"For n=1 we have a(1)=0 because we need nodes on level 0 and level 1.",
				"For n=2 the a(2)=1 solution is",
				"     L-o",
				"       |",
				"       o",
				"and the pointers of the node on level 1 both point to the leaf.",
				"For n=3 the a(3)=2 solutions have the structure",
				"     L-o",
				"       |",
				"     o-o",
				"where the pointers of the last node have to point to the leaf, but the pointer of the next node has 2 choices: the leaf of the previous node."
			],
			"mathematica": [
				"terms = 21; (z + (1 - z)/3*(2 - z + (1 - 2z)^(-1/2)) + O[z]^terms // CoefficientList[#, z] \u0026) Range[0, terms-1]! (* _Jean-François Alcover_, Dec 04 2018 *)"
			],
			"xref": [
				"Cf. A001147 (relaxed compacted binary trees of right height at most one).",
				"Cf. A082161 (relaxed compacted binary trees of unbounded right height).",
				"Cf. A000032, A000246, A001879, A051577, A177145, A213527, A288950, A288952, A288953, A288954 (subclasses of relaxed compacted binary trees of right height at most one, see the Wallner link).",
				"Cf. A000166, A000255, A000262, A052852, A123023, A130905, A176408, A201203 (variants of relaxed compacted binary trees of right height at most one, see the Wallner link)."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Michael Wallner_, Jun 20 2017",
			"references": 6,
			"revision": 13,
			"time": "2018-12-04T08:42:30-05:00",
			"created": "2017-06-26T01:00:24-04:00"
		}
	]
}