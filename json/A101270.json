{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A101270",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 101270,
			"data": "1,0,1,-1,0,3,0,-1,0,2,1,0,-30,0,45,0,7,0,-60,0,72,-1,0,21,0,-105,0,105,0,-149,0,2142,0,-7560,0,6480,-43,0,-2220,0,20790,0,-56700,0,42525,0,53,0,-2280,0,15120,0,-33600,0,22400,-43,0,561,0,-9900,0,49896,0,-93555,0,56133",
			"name": "T(n, k) is the coefficient of z^k in the numerator of the polynomial part of z^n*exp(-n*s), where s = hypergeom([1, 1, 3/2], [2, 5/2], 1/z^2)/(6z^2); related to Chebyshev's quadrature. Triangle read by rows, T(n,k) for 0 \u003c= k \u003c= n.",
			"comment": [
				"Without the zeros and with the powers of the coefficients in reverse order (in each row), this array is essentially the same as A324123. For Maple programs to generate the rows of this array, see the link and the program section. - _Petros Hadjicostas_, Oct 28 2019"
			],
			"link": [
				"Petros Hadjicostas, \u003ca href=\"/A101270/a101270_1.txt\"\u003eAlternative Maple program\u003c/a\u003e.",
				"H. E. Salzer, \u003ca href=\"https://doi.org/10.1002/sapm1947261191\"\u003eTables for facilitating the use of Chebyshev's quadrature formula\u003c/a\u003e, Journal of Mathematics and Physics, 26 (1947), 191-194.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ChebyshevQuadrature.html\"\u003eChebyshev Quadrature\u003c/a\u003e."
			],
			"example": [
				"T(4,0) = 1, T(4,1)=0, T(4,2) = -30, T(4,3) = 0, T(4,4) = 45 because",
				"z^4*exp(-4s) = z^4 - 2*z^2/3 + 1/45 - 32/(2835*z^2) + O(1/z^4) = (45*z^4 - 30*z^2 + 1)/45 - 32/(2835*z^2) + O(1/z^4).",
				"Triangle T(n,k) (with rows n \u003e= 0 and columns k \u003e= 0) begins as follows:",
				"   1",
				"   0,  1;",
				"  -1,  0,   3;",
				"   0, -1,   0,   2;",
				"   1,  0, -30,   0,   45;",
				"   0,  7,   0, -60,    0, 72;",
				"  -1,  0,  21,   0, -105,  0, 105;",
				"   ..."
			],
			"maple": [
				"gf := n -\u003e exp(n*(arctanh(z)/z + 1/2*log(1-z^2) - 1)):",
				"ser := n -\u003e series(gf(n), z, n + 2):",
				"g := n -\u003e ilcm(seq(denom(coeff(ser(n), z, k)), k = 0..n)):",
				"a := proc(n) local G,S; G:=g(n); S:=ser(n); seq(g(n)*coeff(S, z, n-m), m=0..n) end:",
				"seq(a(n), n = 0..10); # _Petros Hadjicostas_, Oct 28 2019"
			],
			"xref": [
				"T(n, n) = A002680(n).",
				"Cf. A324123 (same without the zeros)."
			],
			"keyword": "sign,tabl",
			"offset": "0,6",
			"author": "_Emeric Deutsch_, Jan 24 2005",
			"ext": [
				"T(0,0) = 1 prepended by _Petros Hadjicostas_, Oct 28 2019"
			],
			"references": 3,
			"revision": 53,
			"time": "2019-10-29T09:00:36-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}