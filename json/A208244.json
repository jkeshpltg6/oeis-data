{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A208244",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 208244,
			"data": "1,2,1,2,2,1,3,2,2,1,2,3,1,2,1,3,2,3,3,1,3,3,3,2,2,2,3,2,3,4,3,2,4,3,2,3,3,3,3,4,2,4,3,2,3,4,2,4,3,1,4,3,2,3,2,4,6,2,2,4,4,1,5,4,2,4,4,3,4,4,2,4,3,2,5,3,2,4,4,2,5,4,2,6,4,3,5,3,1,6,3,3,5,5,3,5,3,3,5,4",
			"name": "Number of ways to write n as the sum of a practical number (A005153) and a triangular number (A000217).",
			"comment": [
				"Conjecture: a(n)\u003e0 for all n\u003e0.",
				"The author has verified this for n up to 10^8, and also guessed the following refinement: If n\u003e6 is not among 20, 104, 272, 464, 1664, then n can be written as p+q with p an even practical number and q a positive triangular number."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A208244/b208244.txt\"\u003eTable of n, a(n) for n = 1..50000\u003c/a\u003e",
				"G. Melfi, \u003ca href=\"http://dx.doi.org/10.1006/jnth.1996.0012\"\u003eOn two conjectures about practical numbers\u003c/a\u003e, J. Number Theory 56 (1996) 205-210 [\u003ca href=\"http://www.ams.org/mathscinet-getitem?mr=1370203\"\u003eMR96i:11106\u003c/a\u003e].",
				"Z. W. Sun, \u003ca href=\"http://maths.nju.edu.cn/~zwsun/109p.pdf\"\u003eOn sums of primes and triangular numbers\u003c/a\u003e, J. Comb. Number Theory 1(2009), 65-76.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1211.1588\"\u003eConjectures involving primes and quadratic forms\u003c/a\u003e, arxiv:1211.1588 [math.NT], 2012-2017."
			],
			"example": [
				"a(15)=1 since 15=12+3 with 12 a practical number and 3 a triangular number."
			],
			"mathematica": [
				"f[n_]:=f[n]=FactorInteger[n]",
				"Pow[n_,i_]:=Pow[n,i]=Part[Part[f[n],i],1]^(Part[Part[f[n],i],2])",
				"Con[n_]:=Con[n]=Sum[If[Part[Part[f[n],s+1],1]\u003c=DivisorSigma[1,Product[Pow[n,i],{i,1,s}]]+1,0,1],{s,1,Length[f[n]]-1}]",
				"pr[n_]:=pr[n]=n\u003e0\u0026\u0026(n\u003c3||Mod[n,2]+Con[n]==0)",
				"a[n_]:=a[n]=Sum[If[pr[n-k(k+1)/2]==True,1,0],{k,0,(Sqrt[8n+1]-1)/2}]",
				"Do[Print[n,\" \",a[n]],{n,1,100}]"
			],
			"xref": [
				"Cf. A000040, A005153, A208243, A132399, A187785."
			],
			"keyword": "nonn,look",
			"offset": "1,2",
			"author": "_Zhi-Wei Sun_, Jan 11 2013",
			"references": 21,
			"revision": 22,
			"time": "2018-12-08T11:21:17-05:00",
			"created": "2013-01-11T21:32:23-05:00"
		}
	]
}