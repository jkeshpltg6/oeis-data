{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002741",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2741,
			"id": "M0037 N0010",
			"data": "0,1,-1,2,0,9,35,230,1624,13209,120287,1214674,13469896,162744945,2128047987,29943053062,451123462672,7245940789073,123604151490591,2231697509543362,42519034050101744,852495597142800377,17942811657908144163,395553947953212635718,9114871523102565301544,219135339782236105192745",
			"name": "Logarithmic numbers: expansion of -log(1-x) e^(-x).",
			"reference": [
				"J. M. Gandhi, On logarithmic numbers, Math. Student, 31 (1963), 73-83.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A002741/b002741.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e",
				"J. M. Gandhi, \u003ca href=\"/A002741/a002741.pdf\"\u003eOn logarithmic numbers\u003c/a\u003e, Math. Student, 31 (1963), 73-83. [Annotated scanned copy]",
				"\u003ca href=\"/index/Lo#logarithmic\"\u003eIndex entries for sequences related to logarithmic numbers\u003c/a\u003e"
			],
			"formula": [
				"E.g.f.: -log(1-x) / e^x. a(n) = (n-2) * a(n-1) + (n-1) * a(n-2) - (-1)^n, n \u003e 0. A000757(n) = (-1)^n + a(n). - _Michael Somos_, Jun 21 2002",
				"a(n) = n-th forward difference of [0, 1, 1, 2, 6, 24, ...] (factorials A000142 with 0 prepended). - _Michael Somos_, Mar 28 2011",
				"a(n) ~ exp(-1)*(n-1)!. - _Vaclav Kotesovec_, Mar 10 2014",
				"From _Vladimir Reshetnikov_, Oct 29 2015: (Start)",
				"Recurrence: a(0) = 0, a(1) = 1, a(2) = -1, a(n) = (n-3)*a(n-1) + 2*(n-2)*a(n-2) + (n-2)*a(n-3).",
				"a(n) + a(n+1) = A000166(n). (End)",
				"a(n) = (-1)^(n-1)*n*hypergeom([1,1,1-n], [2], 1). - _Peter Luschny_, May 09 2017"
			],
			"example": [
				"a(3) = 2 = 2! - 3*1! + 3*0! - 0. a(4) = 0 = 3! - 4*2! + 6*1! - 4*0! + 0. - _Michael Somos_, Mar 28 2011"
			],
			"maple": [
				"a := n -\u003e (-1)^(n-1)*n*hypergeom([1,1,1-n], [2], 1):",
				"seq(simplify(a(n)), n = 0..25); # _Peter Luschny_, May 09 2017"
			],
			"mathematica": [
				"a[n_] := Sum[(-1)^k*n!/((n-k)*k!), {k, 0, n-1}]; Table[a[n], {n, 0, 19}](* _Jean-François Alcover_, Nov 21 2011 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, sum( k=0, n-1, (-1)^k * binomial( n, k) * (n - k - 1)!))} /* _Michael Somos_, Jun 21 2002 */"
			],
			"xref": [
				"Cf. A000142, A000166, A000757."
			],
			"keyword": "sign,easy,nice",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Jeffrey Shallit_",
				"More terms from _Joerg Arndt_, Sep 02 2013"
			],
			"references": 34,
			"revision": 50,
			"time": "2019-05-08T08:36:10-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}