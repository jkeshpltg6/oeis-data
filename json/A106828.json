{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A106828",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 106828,
			"data": "1,0,0,1,0,2,0,6,3,0,24,20,0,120,130,15,0,720,924,210,0,5040,7308,2380,105,0,40320,64224,26432,2520,0,362880,623376,303660,44100,945,0,3628800,6636960,3678840,705320,34650,0,39916800,76998240,47324376,11098780,866250,10395",
			"name": "Triangle T(n,k) read by rows: associated Stirling numbers of first kind (n \u003e= 0 and 0 \u003c= k \u003c= floor(n/2)).",
			"comment": [
				"Another version of the triangle is in A008306.",
				"A signed version of this triangle is given by the exponential Riordan array [1, log(1+t)-t]. Its row sums are (-1)^n*(1-n). Another version is [1, log(1-t)+t], whose row sums are 1-n. - _Paul Barry_, May 10 2008"
			],
			"reference": [
				"L. Comtet, Advanced Combinatorics, Reidel, 1974, p. 256.",
				"J. Riordan, An Introduction to Combinatorial Analysis, Wiley, 1958, p. 75."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A106828/b106828.txt\"\u003eRows n = 0..124 of table, flattened\u003c/a\u003e",
				"Feng-Zhen Zhao, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL11/Zhao/zhao79.html\"\u003eSome Properties of Associated Stirling Numbers\u003c/a\u003e, Journal of Integer Sequences, Article 08.1.7, 2008."
			],
			"formula": [
				"T(n, k) = Sum_{j=0..n-k} binomial(j, n-2*k)*E2(n-k, j), where E2 are the second-order Eulerian numbers (A008517). - _Peter Luschny_, Jan 13 2016",
				"Also the Bell transform of the sequence g(k) = k! if k\u003e0 else 0. For the definition of the Bell transform see A264428. - _Peter Luschny_, Jan 13 2016"
			],
			"example": [
				"Rows 0 though 7 are:",
				"  1;",
				"  0,",
				"  0,   1;",
				"  0,   2,",
				"  0,   6,   3;",
				"  0,  24,  20,",
				"  0, 120, 130,  15;",
				"  0, 720, 924, 210;"
			],
			"maple": [
				"A106828 := (n,k) -\u003e add(binomial(j,n-2*k)*combinat:-eulerian2(n-k,j),j=0..n-k):",
				"seq(print(seq(A106828(n,k),k=0..iquo(n,2))),n=0..9); # _Peter Luschny_, Apr 20 2011 (revised Jan 13 2016)",
				"# Second program, after David Callan in A008306:",
				"A106828 := proc(n, k) option remember; if k = 0 then k^n elif k = 1 then (n-1)! elif n \u003c= 2*k-1 then 0 else (n-1)*(procname(n-1, k) + procname(n-2, k-1)) fi end: seq((seq(A106828(n, k), k = 0..iquo(n, 2))), n=0..12); # _Peter Luschny_, Aug 24 2021"
			],
			"mathematica": [
				"Eulerian2[n_, k_] := Eulerian2[n, k] = If[k == 0, 1, If[k == n, 0, Eulerian2[n - 1, k] (k + 1) + Eulerian2[n - 1, k - 1] (2 n - k - 1)]];",
				"T[n_, k_] := Sum[Binomial[j, n - 2 k] Eulerian2[n - k, j], {j, 0, n - k}];",
				"Table[T[n, k], {n, 0, 12}, {k, 0, n/2}] (* _Jean-François Alcover_, Jun 13 2019 *)"
			],
			"program": [
				"(Haskell)",
				"a106828 n k = a106828_tabf !! n !! k",
				"a106828_row n = a106828_tabf !! n",
				"a106828_tabf = map (fst . fst) $ iterate f (([1], [0]), 1) where",
				"   f ((us, vs), x) =",
				"     ((vs, map (* x) $ zipWith (+) ([0] ++ us) (vs ++ [0])), x + 1)",
				"-- _Reinhard Zumkeller_, Aug 05 2013",
				"(Sage) # uses[bell_transform from A264428]",
				"# Computes the full triangle 0\u003c=k\u003c=n.",
				"def A106828_row(n):",
				"    g = lambda k: factorial(k) if k\u003e0 else 0",
				"    s = [g(k) for k in (0..n)]",
				"    return bell_transform(n, s)",
				"[A106828_row(n) for n in (0..8)] # _Peter Luschny_, Jan 13 2016",
				"(PARI) E2(n, m) = sum(k=0, n-m, (-1)^(n+k)*binomial(2*n+1, k)*stirling(2*n-m-k+1, n-m-k+1, 1)); \\\\ A008517",
				"T(n, k) = if ((n==0) \u0026\u0026 (k==0), 1, sum(j=0, n-k, binomial(j, n-2*k)*E2(n-k, j+1))); \\\\ _Michel Marcus_, Dec 07 2021",
				"(Python)",
				"from math import factorial",
				"def A106828(n, k):",
				"    return k**n if k == 0 else factorial(n-1) if k == 1 else 0 if n \u003c= 2*k - 1 else (n - 1)*(A106828(n-1, k) + A106828(n-2, k-1))",
				"for n in range(14): print([A106828(n, k) for k in range(n//2 + 1)])",
				"# _Mélika Tebni_, Dec 07 2021, after second Maple script."
			],
			"xref": [
				"See A008306 for more information.",
				"Cf. A008619 (row lengths), A000166 (row sums)."
			],
			"keyword": "tabf,nonn,easy",
			"offset": "0,6",
			"author": "_N. J. A. Sloane_, May 22 2005",
			"ext": [
				"Removed extra 0 in row 1 from _Michael Somos_, Jan 19 2011"
			],
			"references": 8,
			"revision": 53,
			"time": "2021-12-07T11:32:28-05:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}