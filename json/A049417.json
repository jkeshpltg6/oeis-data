{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A049417",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 49417,
			"data": "1,3,4,5,6,12,8,15,10,18,12,20,14,24,24,17,18,30,20,30,32,36,24,60,26,42,40,40,30,72,32,51,48,54,48,50,38,60,56,90,42,96,44,60,60,72,48,68,50,78,72,70,54,120,72,120,80,90,60,120,62,96,80,85,84,144,68,90",
			"name": "a(n) = isigma(n): sum of infinitary divisors of n.",
			"comment": [
				"A divisor of n is called infinitary if it is a product of divisors of the form p^{y_a 2^a}, where p^y is a prime power dividing n and sum_a y_a 2^a is the binary representation of y.",
				"Multiplicative: If e = Sum_{k \u003e= 0} d_k 2^k (binary representation of e), then a(p^e) = Product_{k \u003e= 0} (p^(2^k*{d_k+1}) - 1)/(p^(2^k) - 1). - _Christian G. Bower_ and _Mitch Harris_, May 20 2005",
				"This sequence is an infinitary analog of the Dedekind psi function A001615. Indeed, a(n) = Product_{q in Q_n}(q+1) =  n*Product_{q in Q_n} (1+1/q), where {q} are terms of A050376 and Q_n is the set of distinct q's whose product is n. - _Vladimir Shevelev_, Apr 01 2014"
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A049417/b049417.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (terms 1..7417 from R. J. Mathar)",
				"Graeme L. Cohen, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1990-0993927-5\"\u003eOn an integer's infinitary divisors\u003c/a\u003e, Math. Comp. 54 (189) (1990) 395-411.",
				"Steven R. Finch, \u003ca href=\"/A007947/a007947.pdf\"\u003eUnitarism and Infinitarism\u003c/a\u003e, February 25, 2004. [Cached copy, with permission of the author]",
				"J. O. M. Pedersen, \u003ca href=\"http://amicable.homepage.dk/tables.htm\"\u003eTables of Aliquot Cycles\u003c/a\u003e [Broken link]",
				"J. O. M. Pedersen, \u003ca href=\"http://web.archive.org/web/20140502102524/http://amicable.homepage.dk/tables.htm\"\u003eTables of Aliquot Cycles\u003c/a\u003e [Via Internet Archive Wayback-Machine]",
				"J. O. M. Pedersen, \u003ca href=\"/A063990/a063990.pdf\"\u003eTables of Aliquot Cycles\u003c/a\u003e [Cached copy, pdf file only]",
				"Tomohiro Yamada, \u003ca href=\"https://arxiv.org/abs/1705.10933\"\u003eInfinitary superperfect numbers\u003c/a\u003e, arXiv:1705.10933 [math.NT], 2017."
			],
			"formula": [
				"Let n = Product(q_i) where {q_i} is a set of distinct terms of A050376. Then a(n) = Product(q_i + 1). - _Vladimir Shevelev_, Feb 19 2011",
				"If n is squarefree, then a(n) = A001615(n). - _Vladimir Shevelev_, Apr 01 2014",
				"a(n) = Sum_{k\u003e=1} A077609(n,k). - _R. J. Mathar_, Oct 04 2017",
				"a(n) = A126168(n)+n. - _R. J. Mathar_, Oct 05 2017"
			],
			"example": [
				"If n = 8: 8 = 2^3 = 2^\"11\" (writing 3 in binary) so the infinitary divisors are 2^\"00\" = 1, 2^\"01\" = 2, 2^\"10\" = 4 and 2^\"11\" = 8; so a(8) = 1+2+4+8 = 15.",
				"n = 90 = 2*5*9, where 2, 5, 9 are in A050376; so a(n) = 3*6*10 = 180. - _Vladimir Shevelev_, Feb 19 2011"
			],
			"maple": [
				"isidiv := proc(d, n)",
				"    local n2, d2, p, j;",
				"    if n mod d \u003c\u003e 0 then",
				"        return false;",
				"    end if;",
				"    for p in numtheory[factorset](n) do",
				"        padic[ordp](n,p) ;",
				"        n2 := convert(%, base, 2) ;",
				"        padic[ordp](d,p) ;",
				"        d2 := convert(%, base, 2) ;",
				"        for j from 1 to nops(d2) do",
				"            if op(j, n2) = 0 and op(j, d2) \u003c\u003e 0 then",
				"                return false;",
				"            end if;",
				"        end do:",
				"    end do;",
				"    return true;",
				"end proc:",
				"idivisors := proc(n)",
				"    local a, d;",
				"    a := {} ;",
				"    for d in numtheory[divisors](n) do",
				"        if isidiv(d, n) then",
				"            a := a union {d} ;",
				"        end if;",
				"    end do:",
				"    a ;",
				"end proc:",
				"A049417 := proc(n)",
				"    local d;",
				"    add(d, d=idivisors(n)) ;",
				"end proc:",
				"seq(A049417(n),n=1..100) ; # _R. J. Mathar_, Feb 19 2011"
			],
			"mathematica": [
				"bitty[k_] := Union[Flatten[Outer[Plus, Sequence @@ ({0, #1} \u0026 ) /@ Union[2^Range[0, Floor[Log[2, k]]]*Reverse[IntegerDigits[k, 2]]]]]]; Table[Plus@@((Times @@ (First[it]^(#1 /. z -\u003e List)) \u0026 ) /@ Flatten[Outer[z, Sequence @@ bitty /@ Last[it = Transpose[FactorInteger[k]]], 1]]), {k, 2, 120}]",
				"(* Second program: *)",
				"a[n_] := If[n == 1, 1, Sort @ Flatten @ Outer[ Times, Sequence @@ (FactorInteger[n] /. {p_, m_Integer} :\u003e p^Select[Range[0, m], BitOr[m, #] == m \u0026])]] // Total;",
				"Array[a, 100] (* _Jean-François Alcover_, Mar 23 2020, after Paul Abbott in A077609 *)"
			],
			"program": [
				"(PARI) A049417(n) = {my(b, f=factorint(n)); prod(k=1, #f[,2], b = binary(f[k,2]); prod(j=1, #b, if(b[j], 1+f[k,1]^(2^(#b-j)), 1)))} \\\\ _Andrew Lelechenko_, Apr 22 2014",
				"(Haskell)",
				"a049417 1 = 1",
				"a049417 n = product $ zipWith f (a027748_row n) (a124010_row n) where",
				"   f p e = product $ zipWith div",
				"           (map (subtract 1 . (p ^)) $",
				"                zipWith (*) a000079_list $ map (+ 1) $ a030308_row e)",
				"           (map (subtract 1 . (p ^)) a000079_list)",
				"-- _Reinhard Zumkeller_, Sep 18 2015"
			],
			"xref": [
				"Cf. A037445, A004607, A127661, A293355.",
				"Cf. A049418 (3-infinitary), A074847 (4-infinitary), A097863 (5-infinitary).",
				"Cf. A000079, A030308, A027748, A124010."
			],
			"keyword": "nonn,mult",
			"offset": "1,2",
			"author": "_Yasutoshi Kohmoto_, Dec 11 1999",
			"ext": [
				"More terms from _Wouter Meeussen_, Sep 02 2001"
			],
			"references": 73,
			"revision": 90,
			"time": "2020-03-23T06:45:58-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}