{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007799",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7799,
			"data": "1,1,1,1,2,2,1,1,3,6,9,5,1,4,12,30,44,26,3,1,5,20,70,170,250,169,35,1,6,30,135,460,1110,1689,1254,340,15,1,7,42,231,1015,3430,8379,13083,10408,3409,315,1,8,56,364,1960,8540,28994,71512,114064,96116,36260",
			"name": "Irregular triangle read by rows: Whitney numbers of the second kind a(n,k), n \u003e= 1, k \u003e= 0, for the star poset.",
			"comment": [
				"Row sums are factorials. - _N. J. A. Sloane_, Mar 05 2017",
				"a(n,k) is the number of permutations of 1..n that can be reached from the identity permutation in k steps using only the n-1 transpositions (1 2) (1 3) .. (1 n). The maximum value of k is given by floor(3*(n-1)/2). - _Andrew Howroyd_, May 13 2017"
			],
			"reference": [
				"S. Grusea, A. Labarre, Asymptotic normality and combinatorial aspects of the prefix exchange distance distribution, Advances in Applied Mathematics, Elsevier, 2016, 78, pp. 94-113; https://hal.archives-ouvertes.fr/hal-01242140"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A007799/b007799.txt\"\u003eTable of n, a(n) for n = 1..1875\u003c/a\u003e",
				"S. Grusea, A. Labarre, \u003ca href=\"https://arxiv.org/abs/1604.04766\"\u003eAsymptotic normality and combinatorial aspects of the prefix exchange distance distribution\u003c/a\u003e, arXiv:1604.04766 [math.CO], 2016.",
				"Navid Imani, Hamid Sarbazi-Azad, and Selim G. Akl, \u003ca href=\"https://doi.org/10.1016/j.disc.2008.08.007\"\u003eSome topological properties of star graphs: The surface area and volume\u003c/a\u003e,  Discrete Mathematics 309.3 (2009): 560-569. See Table 1.",
				"F. J. Portier and T. P. Vaughan, \u003ca href=\"https://doi.org/10.1016/S0195-6698(13)80127-8\"\u003eWhitney numbers of the second kind for the star poset\u003c/a\u003e, Europ. J. Combin., 11 (1990), 277-288.",
				"K. Qiu and S. G. Akl, \u003ca href=\"http://dx.doi.org/10.1155/1995/61390\"\u003eOn some properties of the star graph\u003c/a\u003e, VLSI Design, Vol. 2, No. 4 (1995), 389-396.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PermutationStarGraph.html\"\u003ePermutation Star Graph\u003c/a\u003e"
			],
			"formula": [
				"a(n,0) = 1, a(n,1) = n-1, a(n,2) = (n-1)(n-2), a(n,k) = a(n-1, k) + (n-1)a(n-1, k-1) - (n-2)a(n-2, k-1) + (n-2)a(n-2, k-3) for k \u003e= 3.",
				"a(n,0) = 1, a(n,1) = n - 1, a(n,2) = (n-1)(n-2); a(n,i) = (n-1)a(n-1, i-1) + Sum_{j=1 .. n-2} j a(j, i-3). For 0 \u003c= i \u003c= ceiling(3(n-1)/2) and n \u003e= 1 we have Sum_{k=0 .. i+1} (-1)^k binomial(i+1, k) a(n+i+1-k, i) = 0. For example, for i=2, we have a(n+3, 2) - 3a(n+2, 2) + 3a(n+1, 2) - a(n, 2) = 0. - Ke Qiu (kqiu(AT)brocku.ca), Feb 06 2005"
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  1,    1;",
				"  1,    2,    2,    1;",
				"  1,    3,    6,    9,    5;",
				"  1,    4,   12,   30,   44,   26,    3;",
				"  1,    5,   20,   70,  170,  250,  169,   35;",
				"  1,    6,   30,  135,  460, 1110, 1689, 1254,  340,   15;",
				"  ..."
			],
			"mathematica": [
				"nmax = 9; a[n_, 0] = 1; a[n_, 1] = n - 1; a[n_, 2] = (n - 1) (n - 2); a[n_, k_ /; k \u003e= 2] := a[n, k] = (n - 1) a[n - 1, k - 1] + Sum[j a[j, k-3], {j, 1, n - 2}]; Flatten[Table[a[n, k], {n, 1, nmax}, {k, 0, Floor[3 (n - 1)/2]}]] (* _Jean-François Alcover_, Nov 10 2011, after Ke Qiu *)",
				"Table[Sum[Binomial[n - 1, k] Binomial[n - 1 - k, t] StirlingS1[k + 1, i - k + 1 - 2 t] (-1)^(i + 2 - t), {k, 0, Min[n - 1, i + 1]}, {t, Max[0, Ceiling[(i - 2 k)/2]], Min[n - 1 - k, Floor[(i + 1 - k)/2]]}], {n, 9}, {i, 0, Floor[3 (n - 1)/2]}] // Flatten (* _Eric W. Weisstein_, Dec 09 2017 *)"
			],
			"xref": [
				"Cf. A192837."
			],
			"keyword": "nonn,tabf,easy,nice",
			"offset": "1,5",
			"author": "Frederick J. Portier [fportier(AT)msmary.edu]",
			"ext": [
				"More terms from Larry Reeves (larryr(AT)acm.org), Mar 22 2000"
			],
			"references": 6,
			"revision": 40,
			"time": "2017-12-09T20:19:41-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}