{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A186742",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 186742,
			"data": "1,1,0,0,0,0,0,0,0,0,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0",
			"name": "Expansion of f(x, x^11) in powers of x where f(, ) is Ramanujan's general theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A186742/b186742.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Euler transform of period 24 sequence [1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 1, -1, 1, 0, 0, 0, 0, 0, 0, 0, 0, -1, 1, -1, ...].",
				"a(n) is the characteristic function of A195818. a(n) = max( 0, -A010815(n + 1)).",
				"G.f.: Sum_{k in Z} x^(6*k^2 - 5*k) = Product_{k\u003e0} (1 + x^(12*k - 11)) * (1 + x^(12*k - 1)) * (1 - x^(12*k)).",
				"Expansion of (f(x, x^2) - f(-x, -x^2)) / (2*x) in powers of x. - _Michael Somos_, Aug 28 2017"
			],
			"example": [
				"G.f. = 1 + x + x^11 + x^14 + x^34 + x^39 + x^69 + x^76 + x^116 + x^125 + ...",
				"G.f. = q^25 + q^49 + q^289 + q^361 + q^841 + q^961 + q^1681 + q^1849 + ..."
			],
			"mathematica": [
				"a[ n_] := With[{m = Sqrt[24 n + 25]}, If[ n \u003e= 0 \u0026\u0026 IntegerQ @ m, Boole[ Mod[m, 12] == 5 || Mod[m, 12] == 7], 0]]; (* _Michael Somos_, Aug 28 2017 *)",
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x^12] QPochhammer[ -x^1, x^12] QPochhammer[ -x^11, x^12], {x, 0, n}]; (* _Michael Somos_, Aug 28 2017 *)"
			],
			"program": [
				"(PARI) {a(n) = my(m); n++; if( !issquare( 24*n + 1, \u0026m), 0, m%12 == 5 || m%12 == 7)};",
				"(PARI) {a(n) = my(A); n = 3*n + 3; if( n\u003c0, 0, A = x * O(x^n); polcoeff( (eta(x^2 + A)^2 / eta(x + A) - eta(x^3 + A) - x * eta(x^18 + A)^2 / eta(x^9 + A)) / 2, n))}; /* _Michael Somos_, Aug 28 2017 */"
			],
			"xref": [
				"Cf. A010815, A195818."
			],
			"keyword": "nonn",
			"offset": "0,1",
			"author": "_Michael Somos_, Jan 21 2012",
			"references": 4,
			"revision": 20,
			"time": "2021-03-12T22:24:46-05:00",
			"created": "2012-01-21T22:54:11-05:00"
		}
	]
}