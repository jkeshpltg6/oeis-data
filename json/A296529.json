{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A296529",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 296529,
			"data": "1,0,1,0,1,1,0,1,2,1,0,2,3,3,2,0,2,5,6,5,2,0,5,6,13,13,6,5,0,10,10,16,32,16,10,10,0,28,26,36,51,51,36,26,28,0,24,50,62,74,76,74,62,50,24,0,50,50,134,138,161,161,138,134,50,50,0,124,120,146,302,345,386,345,302,146,120,124",
			"name": "Number T(n,k) of non-averaging permutations of [n] with first element k; triangle T(n,k), n \u003e= 0, k = 0..n, read by rows.",
			"comment": [
				"A non-averaging permutation avoids any 3-term arithmetic progression.",
				"T(0,0) = 1 by convention."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A296529/b296529.txt\"\u003eRows n = 0..99, flattened\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/NonaveragingSequence.html\"\u003eNonaveraging Sequence\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Arithmetic_progression\"\u003eArithmetic progression\u003c/a\u003e",
				"\u003ca href=\"/index/No#non_averaging\"\u003eIndex entries related to non-averaging sequences\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = T(n,n+1-k) \u003e 0 for k=1..n."
			],
			"example": [
				"T(5,1) = 2: 15324, 15342.",
				"T(5,2) = 5: 21453, 24153, 24315, 24351, 24513.",
				"T(5,3) = 6: 31254, 31524, 31542, 35124, 35142, 35412.",
				"T(5,4) = 5: 42153, 42315, 42351, 42513, 45213.",
				"T(5,5) = 2: 51324, 51342.",
				"Triangle T(n,k) begins:",
				"  1;",
				"  0,  1;",
				"  0,  1,  1;",
				"  0,  1,  2,   1;",
				"  0,  2,  3,   3,   2;",
				"  0,  2,  5,   6,   5,   2;",
				"  0,  5,  6,  13,  13,   6,   5;",
				"  0, 10, 10,  16,  32,  16,  10,  10;",
				"  0, 28, 26,  36,  51,  51,  36,  26,  28;",
				"  0, 24, 50,  62,  74,  76,  74,  62,  50, 24;",
				"  0, 50, 50, 134, 138, 161, 161, 138, 134, 50, 50;",
				"  ..."
			],
			"maple": [
				"b:= proc(s) option remember; local n, r, ok, i, j, k;",
				"      if nops(s) = 1 then 1",
				"    else n, r:= max(s), 0;",
				"         for j in s minus {n} do ok, i, k:= true, j-1, j+1;",
				"           while ok and i\u003e=0 and k\u003cn do ok, i, k:=",
				"             not i in s xor k in s, i-1, k+1 od;",
				"           r:= r+ `if`(ok, b(s minus {j}), 0)",
				"         od; r",
				"      fi",
				"    end:",
				"T:= (n, k)-\u003e `if`(k=0, 0^n, b({$0..n} minus {k-1})):",
				"seq(seq(T(n, k), k=0..n), n=0..14);"
			],
			"mathematica": [
				"b[s_List] := b[s] = Module[{n = Max[s], r = 0, ok, i, j, k}, If[Length[s] == 1, 1, Do[{ok, i, k} = {True, j-1, j+1}; While[ok \u0026\u0026 i \u003e= 0 \u0026\u0026 k \u003c n, {ok, i, k} = {FreeQ[s, i] ~Xor~ MemberQ[s, k], i-1, k+1}]; r = r + If[ok, b[s ~Complement~ {j}], 0], {j, s ~Complement~ {n}}]; r]];",
				"T[0, 0]=1; T[n_, k_] := If[k==0, 0^n, b[Range[0, n] ~Complement~ {k-1}]];",
				"Table[Table[T[n, k], {k, 0, n}], {n, 0, 14}] // Flatten (* _Jean-François Alcover_, Dec 18 2017, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Columns k=0-1 give: A000007, A296530 (for n\u003e0).",
				"Row sums give A003407.",
				"T(n,n) gives A296530.",
				"T(n,ceiling(n/2)) gives A296531.",
				"Cf. A292523."
			],
			"keyword": "nonn,tabl,look",
			"offset": "0,9",
			"author": "_Alois P. Heinz_, Dec 14 2017",
			"references": 4,
			"revision": 33,
			"time": "2021-11-02T21:27:35-04:00",
			"created": "2017-12-15T09:43:24-05:00"
		}
	]
}