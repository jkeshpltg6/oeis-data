{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A239428",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 239428,
			"data": "0,0,0,0,0,0,0,1,0,1,1,2,2,2,3,2,3,3,2,4,2,4,2,2,3,1,4,2,3,3,2,4,1,5,4,5,6,4,6,4,5,4,3,5,2,4,2,2,2,1,2,1,2,3,3,2,3,3,5,6,4,6,5,7,4,5,5,4,5,3,5,6,5,6,4,6,4,6,5,6",
			"name": "Number of ordered ways to write n = k + m with 0 \u003c k \u003c= m such that pi(2*k) - pi(k) and pi(2*m) - pi(m) are both prime, where pi(x) denotes the number of primes not exceeding x.",
			"comment": [
				"Conjecture: a(n) \u003e 0 for all n \u003e 9, and a(n) = 1 only for n = 8, 10, 11, 26, 33, 50, 52.",
				"This implies that there are infinitely many positive integers n with pi(2*n) - pi(n) prime.",
				"Recall that Bertrand's postulate proved by Chebyshev in 1850 asserts that pi(2*n) - pi(n) \u003e 0 for all n \u003e 0."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A239428/b239428.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1402.6641\"\u003eProblems on combinatorial properties of primes\u003c/a\u003e, arXiv:1402.6641, 2014."
			],
			"example": [
				"a(11) = 1 since 11 = 4 + 7 with pi(2*4) - pi(4) = 4 - 2 = 2 and pi(2*7) - pi(7) = 6 - 4 = 2 both prime.",
				"a(26) = 1 since 26 = 13 + 13 with pi(2*13) - pi(13) = 9 - 6 = 3 prime.",
				"a(33) = 1 since 33 = 6 + 27 with pi(2*6) - pi(6) = 5 - 3 = 2 and pi(2*27) - pi(27) = 16 - 9 = 7 both prime.",
				"a(50) = 1 since 50 = 23 + 27 with pi(2*23) - pi(23) = 14 - 9 = 5 and pi(2*27) - pi(27) = 16 - 9 = 7 both prime.",
				"a(52) = 1 since 52 = 21 + 31 with pi(2*21) - pi(21) = 13 - 8 = 5 and pi(2*31) - pi(31) = 18 - 11 = 7 both prime."
			],
			"mathematica": [
				"p[n_]:=PrimeQ[PrimePi[2n]-PrimePi[n]]",
				"a[n_]:=Sum[If[p[k]\u0026\u0026p[n-k],1,0],{k,1,n/2}]",
				"Table[a[n],{n,1,80}]"
			],
			"xref": [
				"Cf. A000040, A000720, A108954."
			],
			"keyword": "nonn",
			"offset": "1,12",
			"author": "_Zhi-Wei Sun_, Mar 20 2014",
			"references": 3,
			"revision": 19,
			"time": "2014-03-20T13:41:25-04:00",
			"created": "2014-03-20T13:41:25-04:00"
		}
	]
}