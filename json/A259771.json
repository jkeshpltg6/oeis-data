{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A259771",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 259771,
			"data": "1,0,1,0,1,1,1,1,2,1,2,1,3,2,3,3,4,4,5,4,6,5,7,7,9,8,10,10,12,12,15,14,18,17,20,20,24,24,28,28,33,33,38,38,44,45,50,52,59,60,68,69,78,80,89,92,102,105,116,120,133,137,151,156,171,178,194,201",
			"name": "Expansion of x * psi(x^5) * f(-x^10) / f(-x^2,-x^8) in powers of x where psi(), f() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"The g.f. for this sequence is the last term of the 14th equation on page 20 of Ramanujan 1988."
			],
			"reference": [
				"Srinivasa Ramanujan, The Lost Notebook and Other Unpublished Papers, Narosa Publishing House, New Delhi, 1988, p. 20"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A259771/b259771.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Euler transform of period 10 sequence [ 0, 1, 0, 0, 1, 0, 0, 1, 0, -1, ...].",
				"a(n) = A053265(n-1) - A053267(n)."
			],
			"example": [
				"G.f. = x + x^3 + x^5 + x^6 + x^7 + x^8 + 2*x^9 + x^10 + 2*x^11 + x^12 + ...",
				"G.f. = q^49 + q^289 + q^529 + q^649 + q^769 + q^889 + 2*q^1009 + q^1129 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ x Product[ (1 - x^k)^{ 0, -1, 0, 0, -1, 0, 0, -1, 0, 1}[[Mod[k, 10, 1]]], {k, n}], {x, 0, n}];",
				"QP:= QPochhammer; a[n_]:= SeriesCoefficient[ x*QP[x^10]/(QP[x^5, x^10]* QP[x^2, x^10]*QP[x^8, x^10]), {x, 0, n}]; Table[a[n], {n, 1, 100}] (* _G. C. Greubel_, Mar 16 2018 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, 0, n--; polcoeff( prod(k=1, n, (1 - x^k + x * O(x^n))^[ 1, 0, -1, 0, 0, -1, 0, 0, -1, 0][k%10 + 1]), n))};"
			],
			"xref": [
				"Cf. A053265, A053267."
			],
			"keyword": "nonn",
			"offset": "1,9",
			"author": "_Michael Somos_, Jul 04 2015",
			"references": 1,
			"revision": 14,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2015-07-04T22:20:51-04:00"
		}
	]
}