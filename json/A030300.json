{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A030300",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 30300,
			"data": "1,0,0,1,1,1,1,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1",
			"name": "Runs have lengths 2^n, n \u003e= 0.",
			"comment": [
				"An example of a sequence with property that the fraction of 1's in the first n terms does not converge to a limit. - _N. J. A. Sloane_, Sep 24 2007",
				"Image, under the coding sending a,d,e -\u003e 1 and b,c -\u003e 0, of the fixed point, starting with a, of the morphism a -\u003e ab, b -\u003e cd, c -\u003e ee, d -\u003e eb, e -\u003e cc. - _Jeffrey Shallit_, May 14 2016",
				"This sequence taken as digits of a base-b fraction is g(1/b) = Sum_{n\u003e=1} a(n)/b^n = b/(b-1) * Sum_{k\u003e=0} (-1)^k/b^(2^k) per the generating function below.  With initial 0, it is binary expansion .01001111 = A275975.  With initial 0 and digits 2*a(n), it is ternary expansion .02002222 = A160386.  These and in general g(1/b) for any integer b\u003e=2 are among forms which Kempner showed are transcendental. - _Kevin Ryde_, Sep 07 2019"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A030300/b030300.txt\"\u003eTable of n, a(n) for n = 1..65537\u003c/a\u003e",
				"Aubrey J. Kempner, \u003ca href=\"http://www.jstor.org/stable/1988833\"\u003eOn Transcendental Numbers\u003c/a\u003e, Transactions of the American Mathematical Society, volume 17, number 4, October 1916, pages 476-482.",
				"Kevin Ryde, \u003ca href=\"http://oeis.org/plot2a?name1=A079947\u0026amp;name2=A000027\u0026amp;tform1=untransformed\u0026amp;tform2=untransformed\u0026amp;shift=0\u0026amp;radiop1=ratio\u0026amp;drawpoints=false\u0026amp;drawlines=true\"\u003ePlot of A079947(n)/n\u003c/a\u003e, illustrating proportion of 1s in the first n terms here does not converge (but oscillates with rises and falls by hyperbolas)",
				"Ralf Stephan, \u003ca href=\"/somedcgf.html\"\u003eSome divide-and-conquer sequences ...\u003c/a\u003e",
				"Ralf Stephan, \u003ca href=\"/A079944/a079944.ps\"\u003eTable of generating functions\u003c/a\u003e",
				"Ralf Stephan, \u003ca href=\"https://arxiv.org/abs/math/0307027\"\u003eDivide-and-conquer generating functions. I. Elementary sequences\u003c/a\u003e, arXiv:math/0307027 [math.CO], 2003.",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e",
				"\u003ca href=\"/index/Ch#char_fns\"\u003eIndex entries for characteristic functions\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A065359(n) + A083905(n).",
				"a(n) = (1/2)*(1+(-1)^floor(log_2(n))). - _Benoit Cloitre_, Feb 22 2003",
				"G.f.: 1/(1-x) * Sum_{k\u003e=0} (-1)^k*x^2^k. - _Ralf Stephan_, Jul 12 2003",
				"a(n) = 1 - a(floor(n/2)). - _Vladeta Jovovic_, Aug 04 2003",
				"a(n) = A115253(2n, n) mod 2. - _Paul Barry_, Jan 18 2006",
				"a(n) = 1 - A030301(n). - _Antti Karttunen_, Oct 10 2017"
			],
			"maple": [
				"f0 := n-\u003e[seq(0,i=1..2^n)]; f1 := n-\u003e[seq(1,i=1..2^n)]; s := []; for i from 0 to 4 do s := [op(s), op(f1(2*i)), op(f0(2*i+1))]; od: A030300 := s;"
			],
			"mathematica": [
				"nMax = 6; Table[1 - Mod[n, 2], {n, 0, nMax}, {2^n}] // Flatten (* _Jean-François Alcover_, Oct 20 2016 *)"
			],
			"program": [
				"(PARI) a(n) = if(n, !(logint(n,2)%2)); /* _Kevin Ryde_, Aug 02 2019 */"
			],
			"xref": [
				"Cf. A030301. Partial sums give A079947.",
				"Cf. A065359, A083905.",
				"Characteristic function of A053738."
			],
			"keyword": "nonn,base,easy",
			"offset": "1,1",
			"author": "_Jean-Paul Delahaye_",
			"references": 12,
			"revision": 54,
			"time": "2021-12-17T03:13:03-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}