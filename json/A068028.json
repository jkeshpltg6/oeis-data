{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A068028",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 68028,
			"data": "3,1,4,2,8,5,7,1,4,2,8,5,7,1,4,2,8,5,7,1,4,2,8,5,7,1,4,2,8,5,7,1,4,2,8,5,7,1,4,2,8,5,7,1,4,2,8,5,7,1,4,2,8,5,7,1,4,2,8,5,7,1,4,2,8,5,7,1,4,2,8,5,7,1,4,2,8,5,7,1,4,2,8,5,7,1,4,2,8,5,7,1,4,2,8,5,7,1,4,2,8,5,7,1,4",
			"name": "Decimal expansion of 22/7.",
			"comment": [
				"This is an approximation to Pi. It is accurate to 0.04025%.",
				"Consider the recurring part of 22/7 and the sequences R(i) = 2, 1, 4, 2, 3, 0, 2, ... and Q(i) = 1, 4, 2, 8, 5, 7, 1, .... For i \u003e 0, let X(i) = 10*R(i) + Q(i). Then Q(i+1) = floor(X(i)/Y); R(i+1) = X(i) - Y*Q(i+1); here Y=5; X(0)=X=7. Note 1/7 = 7/49 = X/(10*Y-1). Similar comment holds elsewhere. If we consider the sequences R(i) = 3, 2, 3, 5, 5, 1, 4, 0, 6, 4, 6, 3, 4, 3, 1, 1, 5, 2, 6, 0, 2, 0, 3, ... and Q(i) = A021027, we have X=3; Y=7 (attributed to Vedic literature). - _K.V.Iyer_, Jun 16 2010, Jun 18 2010",
				"The sequence of convergents of the continued fraction of Pi begins [3, 22/7, 333/106, 355/113, 103993/33102, ...]. 22/7 is the second convergent. The summation 240*Sum_{n \u003e= 1} 1/((4*n+1)*(4*n+2)*(4*n+3)*(4*n+5)(4*n+6)*(4*n+7)) = 22/7 - Pi shows that 22/7 is an over-approximation to Pi. - _Peter Bala_, Oct 12 2021"
			],
			"link": [
				"D. Castellanos, \u003ca href=\"http://www.jstor.org/stable/2690037\"\u003eThe ubiquitous pi\u003c/a\u003e, Math. Mag., 61 (1988), 67-98 and 148-163. - _N. J. A. Sloane_, Mar 24 2012",
				"D. P. Dalzell, \u003ca href=\"https://doi.org/10.1112/jlms/19.75_Part_3.133\"\u003eOn 22/7\u003c/a\u003e, J. London Math. Soc. 19, 133-134, 1944.",
				"Dale Winham, \u003ca href=\"http://www.oocities.org/siliconvalley/pines/5945/facts.html\"\u003eFacts about Pi\u003c/a\u003e",
				"\u003ca href=\"/index/Ph#Pi314\"\u003eIndex entries for sequences related to the number Pi\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1, 0, -1, 1)."
			],
			"formula": [
				"a(0)=3, a(n) = floor(714285/10^(5-(n mod 6)). - _Sascha Kurz_, Mar 23 2002.",
				"Equals 100*A021018 - 4 = 3 + A020806. - _R. J. Mathar_, Sep 30 2008",
				"For n\u003e1 a(n) = A020806(n-2) (note offset=0 in A020806 and offset=1 in A068028). - _Zak Seidov_, Mar 26 2015",
				"G.f.: x*(3-2*x+3*x^2+x^3+4*x^4)/((1-x)*(1+x)*(1-x+x^2)). - _Vincenzo Librandi_, Mar 27 2015"
			],
			"mathematica": [
				"CoefficientList[Series[(3 - 2 x + 3 x^2 + x^3 + 4 x^4) / ((1 - x) (1 + x) (1 - x + x^2)), {x, 0, 100}], x] (* _Vincenzo Librandi_, Mar 27 2015 *)",
				"Join[{3},LinearRecurrence[{1, 0, -1, 1},{1, 4, 2, 8},104]] (* _Ray Chandler_, Aug 26 2015 *)",
				"RealDigits[22/7,10,120][[1]] (* _Harvey P. Dale_, Oct 04 2021 *)"
			],
			"program": [
				"(MAGMA) I:=[3,1,4,2,8]; [n le 5 select I[n] else Self(n-1)-Self(n-3)+Self(n-4): n in [1..100]]; // _Vincenzo Librandi_, Mar 27 2015",
				"(PARI) a(n)=22/7. \\\\ _Charles R Greathouse IV_, Oct 07 2015"
			],
			"xref": [
				"Cf. A068079, A068089, A002485, A002486, A046965, A046947."
			],
			"keyword": "easy,nonn,cons",
			"offset": "1,1",
			"author": "_Nenad Radakovic_, Mar 22 2002.",
			"ext": [
				"More terms from _Sascha Kurz_, Mar 23 2002",
				"Alternative to broken link added by _R. J. Mathar_, Jun 18 2010"
			],
			"references": 13,
			"revision": 42,
			"time": "2021-10-18T11:16:15-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}