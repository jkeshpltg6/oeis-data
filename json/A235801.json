{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A235801",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 235801,
			"data": "0,1,2,3,7,5,6,7,8,9,17,11,12,13,14,15,27,17,18,19,20,21,37,23,24,25,26,27,47,29,30,31,32,33,57,35,36,37,38,39,67,41,42,43,44,45,77,47,48,49,50,51,87,53,54,55,56,57,97,59,60,61,62,63,107,65,66",
			"name": "Length of n-th horizontal line segment in a diagram of a two-dimensional version of the 3x+1 (or Collatz) problem.",
			"comment": [
				"In the diagram every cycle is represented by a directed graph.",
				"After (3x + 1) the next step is (3y + 1).",
				"After (x/2) the next step is (y/2).",
				"A235800(n) gives the length of n-th vertical line segment, from left to right, in the same diagram."
			],
			"link": [
				"Chai Wah Wu, \u003ca href=\"/A235801/b235801.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"\u003ca href=\"/index/3#3x1\"\u003eIndex entries for sequences related to 3x+1 (or Collatz) problem\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 10*k - 3, if n is of the form (6*k-2), k\u003e=1, otherwise a(n) = n.",
				"From _Chai Wah Wu_, Sep 26 2016: (Start)",
				"a(n) = 2*a(n-6) - a(n-12) for n \u003e 11.",
				"G.f.: x*(x^2 + 1)*(x^3 + 2*x^2 + 1)*(x^5 + x^4 + 2*x + 1)/(x^12 - 2*x^6 + 1). (End)"
			],
			"example": [
				"The first part of the diagram in the first quadrant:",
				". . . . . . . . . . . . . . . . . . . . . . . .",
				".              _ _|_ _|_ _|_ _|_ _|_ _|_ _|_ _.",
				".             |   |   |   |   |   |   |   |_|_.",
				".             |   |   |   |   |   |   |  _ _|_.",
				".             |   |   |   |   |   |   |_|_ _|_.",
				".             |   |   |   |   |   |  _ _|_ _|_.",
				".             |   |   |   |   |   |_|_ _|_ _|_.",
				".          _ _|_ _|_ _|_ _|_ _|_ _ _|_ _|_ _|_.",
				".         |   |   |   |   |   |_|_ _|_ _|_ _|_.",
				".         |   |   |   |   |  _ _|_ _|_ _|_ _|_.",
				".         |   |   |   |   |_|_ _|_ _|_ _|_ _|_.",
				".         |   |   |   |  _ _|_ _|_ _|_ _|_ _|_.",
				".         |   |   |   |_|_ _|_ _|_ _|_ _|_ _| . 11",
				".      _ _|_ _|_ _|_ _ _|_ _|_ _|_ _|_ _|     . 17",
				".     |   |   |   |_|_ _|_ _|_ _|_ _|         .  9",
				".     |   |   |  _ _|_ _|_ _|_ _|             .  8",
				".     |   |   |_|_ _|_ _|_ _|                 .  7",
				".     |   |  _ _|_ _|_ _|                     .  6",
				".     |   |_|_ _|_ _|                         .  5",
				".  _ _|_ _ _|_ _|                             .  7",
				". |   |_|_ _|                                 .  3",
				". |  _ _|                                     .  2",
				". |_|                                         .  1",
				". . . . . . . . . . . . . . . . . . . . . . . .  0",
				".                                              a(n)",
				".",
				"For an explanation of this diagram as the skeleton of a piping model see A235800. - _Omar E. Pol_, Dec 30 2021"
			],
			"program": [
				"(Python)",
				"from __future__ import division",
				"A235801_list = [n if n % 6 != 4 else 10*(n//6)+7 for n in range(10**4)] # _Chai Wah Wu_, Sep 26 2016"
			],
			"xref": [
				"Cf. A347270 (all 3x+1 sequences).",
				"Companion of A235800.",
				"Cf. A000027, A004767, A006370, A014682, A016957, A070165, A235795."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Omar E. Pol_, Jan 15 2014",
			"references": 4,
			"revision": 30,
			"time": "2022-01-02T00:32:36-05:00",
			"created": "2014-03-15T14:25:46-04:00"
		}
	]
}