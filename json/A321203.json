{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A321203",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 321203,
			"data": "2,3,6,20,15,20,105,168,70,84,504,1260,252,1320,2310,495,7920,924,12870,10296,10010,45045,3432,3003,100100,45045,120120,240240,12870,74256,680680,194480,18564,1113840,1225224,48620,1058148,4232592,831402,542640,8817900,6046560,184756",
			"name": "Irregular triangle T giving the coefficients of x^n = x^{2*e2 + 3*e3} of (1 + x^2  + x^3)^n, with the pair of nonnegative numbers [e2, e3] listed in row n of A321201, for n \u003e= 2.",
			"comment": [
				"The row length is r(n), with r(n) = A008615(n+2) for n \u003e= 2:  [1, 1, 1, 1, 2, 1, 2, 2, 2, 2, 3, 2, 3, 3, 3, 3, 4, 3, 4, ...].",
				"The row sums give A176806(n).",
				"For n = 0 with the trivial [e2, e3] = [0, 0] solution the multinomial is 1 with the row sum A176806(0) = 1. For n = 1 there is no solution (with row sum set to A176806(1) = 0).",
				"This multinomial array for pairs [e2, e3] with 2*2e2 + 3*e3 = n, with nonnegative numbers e2 and e3, is obtained from the multinomial array n!/(e1!*e2!*e3!) with n = e1 + e2 + e3, giving the coefficient x_1^{e1}* x_2^{e2}*x_3^{e3} of (x_1 + x_2 + x_3)^n. Here, in order to find the coefficients of (1 + x^2 + x^3)^n, one sets x_1 = 1, x_2 = x^2 and x_3 = x^3. Hence n = e1 + e2 + e3, and the power of x^n becomes n = 2*e2 + 3*e3. Therefore, e1 = n - (e2 + e3), and the array gives n!/((n-(e2+e3))!*e2!*e3!)."
			],
			"formula": [
				"T(n, m) is obtained from the pair(s) [e2, e3] given in row n of A321201 by n!/((n - (e2 +e3))!*e2!*e3!), for n \u003e= 2 and  m = 1, 2, ..., A008615(n+2)."
			],
			"example": [
				"The triangle T(n, m), and the row sums begin:",
				"n\\m        0        1        2       3  ...  Row sums A176806(n)",
				"2:         2                                         2",
				"3:         3                                         3",
				"4:         6                                         6",
				"5:        20                                        20",
				"6:        15       20                               35",
				"7:       105                                       105",
				"8:       168       70                              238",
				"9:        84      504                              588",
				"10:     1260      252                             1512",
				"11:     1320     2310                             3630",
				"12:      495     7920      924                    9339",
				"13:    12870    10296                            23166",
				"14:    10010    45045     3432                   58487",
				"15:     3003   100100    45045                  148148",
				"16:   120120   240240    12870                  373230",
				"17:    74256   680680   194480                  949416",
				"18:    18564  1113840  1225224   48620         2406248",
				"19:  1058148  4232592   831402                 6122142",
				"20:   542640  8817900  6046560  184756        15591856",
				"...",
				"------------------------------------------------------------------------------",
				"n = 8: (1 + x^2 + x^3)^8 has coefficients 238 of x^n arising from the two [e2, e3] pairs [1, 2] and [4, 0], given in row n = 8 of A321201. The multinomial values are 8!/((8-3)!*1!*2!) = 168 and 8!/((8-4)!*4!*0!) = 70, summing to 238."
			],
			"xref": [
				"Cf. A008615, A130561, A176806, A321201, A319204."
			],
			"keyword": "nonn,tabf",
			"offset": "2,1",
			"author": "_Wolfdieter Lang_, Nov 05 2018",
			"references": 4,
			"revision": 13,
			"time": "2018-12-23T23:48:51-05:00",
			"created": "2018-11-09T21:53:24-05:00"
		}
	]
}