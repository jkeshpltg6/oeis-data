{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007367",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7367,
			"id": "M2163",
			"data": "2,44,56,92,104,116,140,164,204,212,260,296,332,344,356,380,392,444,452,476,524,536,564,584,588,620,632,684,692,716,744,764,776,836,860,884,932,956,980,1004,1016,1112,1124,1136,1172,1196,1284,1292,1304",
			"name": "Numbers k such that phi(x) = k has exactly 3 solutions.",
			"comment": [
				"From _Torlach Rush_, Jul 23 2018: (Start)",
				"For known terms:",
				"- The greatest common divisor of the three solutions is the distance of the middle solution from the least solution and is half the distance of the middle solution to the largest solution.",
				"- If the number of distinct prime factors of k equals the number of solutions of k = phi(x), then the greatest common divisor of the solutions is the least solution divided by the number of solutions.",
				"- Except for a(1), if the largest prime factor is the same for all solutions and is equal to the greatest common divisor of all solutions then the distance from a(n) to the least solution is gcd({k: phi(k) = a(n)}) + 2.  (End)",
				"By Ford's theorem on Euler totient function, this sequence is infinite. - _Jianing Song_, Jul 18 2018"
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), p. 840.",
				"J.-M. De Koninck, Ces nombres qui nous fascinent, Entry 44, p. 17, Ellipses, Paris 2008.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A007367/b007367.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Euler%27s_totient_function#Ford\u0026#39;s_theorem\"\u003eFord's theorem\u003c/a\u003e",
				"R. G. Wilson, V, \u003ca href=\"/A007015/a007015.pdf\"\u003eLetter to N. J. A. Sloane, Jul. 1992\u003c/a\u003e"
			],
			"example": [
				"phi(69) = phi(92) = phi(138) = 44, so 44 is a term."
			],
			"mathematica": [
				"a = Table[ 0, {1500} ]; Do[ p = EulerPhi[ n ]; If[ p \u003c 1501, a[ [ p ] ]++ ], {n, 1, 1500} ]; Select[ Range[ 1500 ], a[ [ # ] ] == 3 \u0026 ]",
				"Take[Select[Tally[EulerPhi[Range[50000]]],#[[2]]==3\u0026][[All,1]],50]//Sort (* _Harvey P. Dale_, Apr 02 2018 *)"
			],
			"program": [
				"(Haskell)",
				"a007367 n = a007367_list !! (n-1)",
				"a007367_list = map fst $",
				"               filter ((== 3) . snd) $ zip a002202_list a058277_list",
				"-- _Reinhard Zumkeller_, Nov 25 2015"
			],
			"xref": [
				"Cf. A000010, A007366, A060667-A060671.",
				"Cf. A002202, A058277, A085713."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_, _Mira Bernstein_, _Robert G. Wilson v_",
			"references": 14,
			"revision": 70,
			"time": "2018-07-24T10:01:12-04:00",
			"created": "1994-05-09T03:00:00-04:00"
		}
	]
}