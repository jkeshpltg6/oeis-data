{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A133232",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 133232,
			"data": "1,1,2,1,2,3,1,1,3,4,1,1,3,4,5,1,1,3,4,5,1,1,1,3,4,5,1,7,1,1,3,1,5,1,7,8,1,1,1,1,5,1,7,8,9,1,1,1,1,5,1,7,8,9,1,1,1,1,1,5,1,7,8,9,1,11,1,1,1,1,5,1,7,8,9,1,11,1,1,1,1,1,5,1,7,8,9,1,11,1,13,1,1,1,1,5,1,7,8,9,1,11,1",
			"name": "Triangle T(n,k) read by rows with a minimum number of prime powers A100994 for which the least common multiple of T(n,1),..,T(n,n) is A003418(n).",
			"comment": [
				"Checked up to 28th row. The rest of the ones in the table are there for the least common multiple to calculate correctly."
			],
			"link": [
				"Mats Granvik, Oct 13 2007, \u003ca href=\"/A133232/b133232.txt\"\u003eTable of n, a(n) for n = 1..406\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = if n\u003ck+k*|A120112(k-1)| then k, else 1 (1\u003c=k\u003c=n).",
				"T(n,k) = if n \u003c A014963(k)*A100994(k) then A100994(k), else 1 (1\u003c=k\u003c=n). - _Mats Granvik_, Jan 21 2008"
			],
			"example": [
				"2 occurs 2*1 = 2 times in column 2.",
				"3 occurs 3*2 = 6 times in column 3.",
				"4 occurs 4*1 = 4 times in column 4.",
				"5 occurs 5*4 = 20 times in column 5.",
				"k occurs A133936(k) times in column k. The first rows of the triangle and the least common multiple of the rows are:",
				"lcm{1} = 1",
				"lcm{1, 2} = 2",
				"lcm{1, 2, 3} = 6",
				"lcm{1, 1, 3, 4} = 12",
				"lcm{1, 1, 3, 4, 5} = 60",
				"lcm{1, 1, 3, 4, 5, 1} = 60",
				"lcm{1, 1, 3, 4, 5, 1, 7} = 420",
				"lcm{1, 1, 3, 1, 5, 1, 7, 8} = 840",
				"lcm{1, 1, 1, 1, 5, 1, 7, 8, 9} = 2520"
			],
			"maple": [
				"A120112 := proc(n) 1-ilcm(seq(i,i=1..n+1))/ilcm(seq(i,i=1..n)) ; end proc:",
				"A133232 := proc(n) if n \u003c k*(1+abs(A120112(k-1))) then k else 1; end if; end proc:",
				"seq(seq(A133232(n,k),k=1..n),n=1..15) ; # _R. J. Mathar_, Nov 23 2010"
			],
			"mathematica": [
				"b[n_] := b[n] = If[n == 0, 1, LCM @@ Range[n]];",
				"c[n_] := 1 - b[n+1]/b[n];",
				"T[n_, k_] := If[n \u003c k*(1+Abs[c[k-1]]), k, 1];",
				"Table[T[n, k], {n, 1, 10}, {k, 1, n}] // Flatten (* _Jean-François Alcover_, Mar 01 2021 *)"
			],
			"program": [
				"(Excel) =if(and(row()\u003e=column();row()\u003ccolumn()+column()*abs(A120112));column();1)",
				"(Excel) =if(and(n\u003e=k; n \u003c A014963*A100994); A100994; 1) - _Mats Granvik_, Jan 21 2008"
			],
			"xref": [
				"Cf. A003418, A120112, A014963, A134579."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Mats Granvik_, Oct 13 2007",
			"ext": [
				"Indices added to formulas by _R. J. Mathar_, Nov 23 2010"
			],
			"references": 7,
			"revision": 18,
			"time": "2021-03-01T17:54:12-05:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}