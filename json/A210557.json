{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A210557",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 210557,
			"data": "1,1,2,1,3,5,1,4,10,12,1,5,16,30,29,1,6,23,56,87,70,1,7,31,91,185,245,169,1,8,40,136,334,584,676,408,1,9,50,192,546,1158,1784,1836,985,1,10,61,260,834,2052,3850,5312,4925,2378,1,11,73,341,1212,3366",
			"name": "Triangle of coefficients of polynomials u(n,x) jointly generated with A210558; see the Formula section.",
			"comment": [
				"Row sums: powers of 3 (see A000244).",
				"For a discussion and guide to related arrays, see A208510.",
				"Subtriangle of (1, 0, -1/2, 1/2, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (0, 2, 1/2, -1/2, 0, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Mar 23 2012",
				"Up to reflection at the vertical axis, this triangle coincides with the triangle given in A164981, i.e., the numbers are the same just read row-wise in the opposite direction. - _Christine Bessenrodt_, Jul 20 2012"
			],
			"formula": [
				"u(n,x) = x*u(n-1,x) + x*v(n-1,x)+1,",
				"v(n,x) = 2x*u(n-1,x) + (x+1)v(n-1,x)+1,",
				"where u(1,x)=1, v(1,x)=1.",
				"From _Philippe Deléham_, Mar 23 2012. (Start)",
				"As DELTA-triangle T(n,k) with 0 \u003c= k \u003c= n:",
				"G.f.: (1 - 2*y*x + y*x^2 - y^2*x^2)/(1 - x - 2*y*x + y*x^2 - y^2*x^2).",
				"T(n,k) = T(n-1,k) + 2*T(n-1,k-1) - T(n-2,k-1) + T(n-2,k-2), T(0,0) = T(1,0) = T(2,0) = 1, T(1,1) = T(2,2) = 0, T(2,1) = 2 and T(n,k) = 0 if k \u003c 0 or if k \u003e n."
			],
			"example": [
				"First five rows:",
				"  1;",
				"  1, 2;",
				"  1, 3,  5;",
				"  1, 4, 10, 12;",
				"  1, 5, 16, 30, 29;",
				"First three polynomials u(n,x): 1, 1 + 2x, 1 + 3x + 5x^2.",
				"From _Philippe Deléham_, Mar 23 2012: (Start)",
				"(1, 0, -1/2, 1/2, 0, 0, ...) DELTA (0, 2, 1/2, -1/2, 0, 0, ...) begins:",
				"  1;",
				"  1, 0;",
				"  1, 2,  0;",
				"  1, 3,  5,  0;",
				"  1, 4, 10, 12,  0;",
				"  1, 5, 16, 30, 29, 0;"
			],
			"mathematica": [
				"u[1, x_] := 1; v[1, x_] := 1; z = 16;",
				"u[n_, x_] := x*u[n - 1, x] + x*v[n - 1, x] + 1;",
				"v[n_, x_] := 2 x*u[n - 1, x] + (x + 1)*v[n - 1, x] + 1;",
				"Table[Expand[u[n, x]], {n, 1, z/2}]",
				"Table[Expand[v[n, x]], {n, 1, z/2}]",
				"cu = Table[CoefficientList[u[n, x], x], {n, 1, z}];",
				"TableForm[cu]",
				"Flatten[%]   (* A210557 *)",
				"Table[Expand[v[n, x]], {n, 1, z}]",
				"cv = Table[CoefficientList[v[n, x], x], {n, 1, z}];",
				"TableForm[cv]",
				"Flatten[%]   (* A210558 *)"
			],
			"xref": [
				"Cf. A210558, A208510, A164981."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Clark Kimberling_, Mar 22 2012",
			"references": 4,
			"revision": 20,
			"time": "2020-01-22T05:15:26-05:00",
			"created": "2012-03-22T17:42:55-04:00"
		}
	]
}