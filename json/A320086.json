{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A320086",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 320086,
			"data": "1,1,1,1,1,1,4,4,4,4,2,1,1,1,2,16,16,8,8,16,16,16,4,16,1,16,4,16,64,64,64,64,64,64,64,64,16,8,8,8,1,8,8,8,16,256,256,64,64,128,128,64,64,256,256,256,32,256,16,128,1,128,16,256,32,256",
			"name": "Triangle read by rows, 0 \u003c= k \u003c= n: T(n,k) is the denominator of the derivative of the k-th Bernstein basis polynomial of degree n evaluated at the interval midpoint t = 1/2; numerator is A320085.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A320086/b320086.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e",
				"Rita T. Farouki, \u003ca href=\"https://doi.org/10.1016/j.cagd.2012.03.001\"\u003eThe Bernstein polynomial basis: A centennial retrospective\u003c/a\u003e, Computer Aided Geometric Design Vol. 29 (2012), 379-419.",
				"Ron Goldman, \u003ca href=\"https://doi.org/10.1016/B978-1-55860-354-7.X5000-4\"\u003ePyramid Algorithms. A Dynamic Programming Approach to Curves and Surfaces for Geometric Modeling\u003c/a\u003e, Morgan Kaufmann Publishers, 2002, Chap. 5.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/BernsteinPolynomial.html\"\u003eBernstein Polynomial\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Bernstein_polynomial\"\u003eBernstein polynomial\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = denominator of 2*A141692(n,k)/A000079(n).",
				"T(n, k) = 2^(n-1)/gcd(n*(binomial(n-1, k-1) - binomial(n-1, k)), 2^(n-1)).",
				"T(n, n-k) = T(n,k).",
				"T(n, 0) = A084623(n), n \u003e 0.",
				"T(2*n+1, 1) = A000302(n)."
			],
			"example": [
				"Triangle begins:",
				"    1;",
				"    1,   1;",
				"    1,   1,   1;",
				"    4,   4,   4,  4;",
				"    2,   1,   1,  1,   2;",
				"   16,  16,   8,  8,  16,  16;",
				"   16,   4,  16,  1,  16,   4,  16;",
				"   64,  64,  64, 64,  64,  64,  64, 64;",
				"   16,   8,   8,  8,   1,   8,   8,  8,  16;",
				"  256, 256,  64, 64, 128, 128,  64, 64, 256, 256;",
				"  256,  32, 256, 16, 128,   1, 128, 16, 256,  32, 256;",
				"  ..."
			],
			"maple": [
				"T:=proc(n,k) 2^(n-1)/gcd(n*(binomial(n-1,k-1)-binomial(n-1,k)),2^(n-1)); end proc: seq(seq(T(n,k),k=0..n),n=1..11); # _Muniru A Asiru_, Oct 06 2018"
			],
			"mathematica": [
				"Table[Denominator[n*(Binomial[n-1, k-1] - Binomial[n-1, k])/2^(n-1)], {n, 0, 12}, {k, 0, n}]//Flatten"
			],
			"program": [
				"(Maxima)",
				"T(n, k) := 2^(n - 1)/gcd(n*(binomial(n - 1, k - 1) - binomial(n - 1, k)), 2^(n - 1))$",
				"tabl(nn) := for n:0 thru nn do print(makelist(T(n, k), k, 0, n))$",
				"(Sage)",
				"def A320086(n,k): return denominator(n*(binomial(n-1, k-1) - binomial(n-1, k))/2^(n-1))",
				"flatten([[A320086(n,k) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Jul 19 2021"
			],
			"xref": [
				"Inspired by A141692.",
				"Cf. A007318, A128433, A128434, A319861, A319862, A320085."
			],
			"keyword": "nonn,tabl,easy,frac",
			"offset": "0,7",
			"author": "_Franck Maminirina Ramaharo_, Oct 05 2018",
			"references": 2,
			"revision": 13,
			"time": "2021-07-20T03:27:58-04:00",
			"created": "2018-10-06T14:40:40-04:00"
		}
	]
}