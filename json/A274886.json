{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A274886",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 274886,
			"data": "1,1,1,1,1,1,1,0,1,1,1,2,2,2,1,1,1,0,1,1,1,0,1,1,1,2,3,4,4,5,4,4,3,2,1,1,1,0,1,1,2,1,2,1,2,1,1,0,1,1,1,2,3,5,6,8,9,11,11,12,11,11,9,8,6,5,3,2,1,1,1,0,1,1,2,2,3,2,4,3,4,3,4,2,3,2,2,1,1,0,1",
			"name": "Triangle read by rows, the q-analog of the extended Catalan numbers A057977.",
			"comment": [
				"The q-analog of the extended Catalan numbers A057977 are univariate polynomials over the integers with degree floor((n+1)/2)*(floor((n+1)/2)-1)+1.",
				"The q-analog of the Catalan numbers are A129175.",
				"For a combinatorial interpretation in terms of the major index statistic of orbitals see A274888 and the link 'Orbitals'."
			],
			"link": [
				"Peter Luschny, \u003ca href=\"https://oeis.org/wiki/User:Peter_Luschny/Orbitals\"\u003eOrbitals\u003c/a\u003e"
			],
			"formula": [
				"q-extCatalan(n,q) = (p*P(n,q))/(P(h,q)*P(h+1,q)) with P(n,q) =  q-Pochhammer(n,q), h = floor(n/2) and p = 1-q if n is even else 1."
			],
			"example": [
				"The polynomials start:",
				"[0] 1",
				"[1] 1",
				"[2] 1",
				"[3] q^2 + q + 1",
				"[4] q^2 + 1",
				"[5] (q^2 + 1) * (q^4 + q^3 + q^2 + q + 1)",
				"[6] (q^2 - q + 1) * (q^4 + q^3 + q^2 + q + 1)",
				"The coefficients of the polynomials are:",
				"[ 0] [1]",
				"[ 1] [1]",
				"[ 2] [1]",
				"[ 3] [1, 1, 1]",
				"[ 4] [1, 0, 1]",
				"[ 5] [1, 1, 2, 2, 2, 1, 1]",
				"[ 6] [1, 0, 1, 1, 1, 0, 1]",
				"[ 7] [1, 1, 2, 3, 4, 4, 5, 4, 4, 3, 2, 1, 1]",
				"[ 8] [1, 0, 1, 1, 2, 1, 2, 1, 2, 1, 1, 0, 1]",
				"[ 9] [1, 1, 2, 3, 5, 6, 8, 9, 11, 11, 12, 11, 11, 9, 8, 6, 5, 3, 2, 1, 1]",
				"[10] [1, 0, 1, 1, 2, 2, 3, 2, 4, 3, 4, 3, 4, 2, 3, 2, 2, 1, 1, 0, 1]"
			],
			"maple": [
				"QExtCatalan := proc(n) local h, p, P;",
				"P := x -\u003e QDifferenceEquations:-QPochhammer(q,q,x);",
				"h := iquo(n, 2): p := `if`(n::even, 1-q, 1); (p*P(n))/(P(h)*P(h+1));",
				"expand(simplify(expand(%))); seq(coeff(%, q, j), j=0..degree(%)) end:",
				"seq(QExtCatalan(n, q), n=0..10);"
			],
			"mathematica": [
				"(* Function QBinom1 is defined in A274885. *)",
				"QExtCatalan[n_] := QBinom1[n] / QBinomial[n+1,1,q]; Table[CoefficientList[ QExtCatalan[n] // FunctionExpand,q], {n,0,10}] // Flatten"
			],
			"program": [
				"(Sage) # uses[q_binom1 from A274885]",
				"from sage.combinat.q_analogues import q_int",
				"def q_ext_catalan_number(n): return q_binom1(n)//q_int(n+1)",
				"for n in (0..10): print([n], q_ext_catalan_number(n).list())",
				"(Sage) # uses[unit_orbitals from A274709]",
				"# Brute force counting",
				"def catalan_major_index(n):",
				"    S = [0]*(((n+1)//2)^2 + ((n+1) % 2) - (n//2))",
				"    for u in unit_orbitals(n):",
				"        if any(x \u003e 0 for x in accumulate(u)): continue # never rise above 0",
				"        L = [i+1 if u[i+1] \u003c u[i] else 0 for i in (0..n-2)]",
				"        #    i+1 because u is 0-based whereas convention assumes 1-base.",
				"        S[sum(L)] += 1",
				"    return S",
				"for n in (0..10): print(catalan_major_index(n))"
			],
			"xref": [
				"Cf. A057977, A129175, A274885, A274888."
			],
			"keyword": "nonn,tabf",
			"offset": "0,12",
			"author": "_Peter Luschny_, Jul 20 2016",
			"references": 4,
			"revision": 28,
			"time": "2020-03-28T11:00:47-04:00",
			"created": "2016-08-01T05:52:20-04:00"
		}
	]
}