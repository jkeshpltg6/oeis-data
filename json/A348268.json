{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A348268",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 348268,
			"data": "1,2,3,4,5,6,7,8,11,10,9,12,13,14,17,16,19,22,15,20,29,18,21,24,23,26,37,28,31,34,41,32,43,38,33,44,25,30,35,40,53,58,27,36,67,42,51,48,47,46,39,52,61,74,49,56,59,62,73,68,71,82,79,64,83,86,57,76,55,66,77",
			"name": "Mapping between Lyndon factorization and prime factorization.",
			"comment": [
				"A Lyndon word is a finite sequence that is lexicographically strictly less than all of its cyclic rotations.",
				"We define the Lyndon product of two or more finite sequences to be the lexicographically maximal sequence obtainable by shuffling the sequences together. For example, the Lyndon product of (231) with (213) is (232131), the product of (221) with (213) is (222131), and the product of (122) with (2121) is (2122121). A Lyndon word is a finite sequence that is prime with respect to the Lyndon product. Every finite sequence has a unique (orderless) factorization into Lyndon words, and if these factors are arranged in lexicographically decreasing order, their concatenation is equal to their Lyndon product. For example, (1001) has sorted Lyndon factorization (001)(1).",
				"We use Lyndon factorization on the reversed order binary expansion of n, then we use the mapping from Lyndon words (A328596(k) reversed binary expansion) to positive integers that have no divisors other than 1 and itself (A008578(k+1)). a(n) has factors in A008578 as the binary expansion of n has in A328596."
			],
			"link": [
				"Thomas Scheuerle, \u003ca href=\"/A348268/b348268.txt\"\u003eTable of n, a(n) for n = 0..5000\u003c/a\u003e",
				"Thomas Scheuerle, \u003ca href=\"/A348268/a348268.m.txt\"\u003eProgram (MATLAB)\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(Lyndonproduct(n,m)) = a(n)*a(m).",
				"a(1 + 2*n)/a(n) = 2.",
				"all a(A329399(n)) are in A000961 (powers of primes).",
				"all a(A328595(n)) (reversed binary expansion is a necklace) are in A329131 (prime signature is a Lyndon word)."
			],
			"example": [
				"We map Lyndon-words to positive integers that have no divisors other than 1 and itself: [] -\u003e 1, 1 -\u003e 2, 01 -\u003e 3, 001 -\u003e 5, 011 -\u003e 7, 0001 -\u003e 11, ...",
				"9 is in reversed order binary: 1001, has the factors (1)(001) -\u003e a(9) = 2*5 = 10.",
				"10 is in reversed order binary: 0101, has the factors (01)(01) -\u003e a(10) = 3*3 = 9."
			],
			"program": [
				"(MATLAB) See link from Thomas Scheuerle."
			],
			"xref": [
				"Cf. A329313, A328596, A008578, A329399, A000961, A328595, A328595."
			],
			"keyword": "nonn,base",
			"offset": "0,2",
			"author": "_Thomas Scheuerle_, Oct 09 2021",
			"references": 3,
			"revision": 33,
			"time": "2021-10-12T07:59:25-04:00",
			"created": "2021-10-11T18:38:58-04:00"
		}
	]
}