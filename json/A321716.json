{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A321716",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 321716,
			"data": "1,1,1,1,1,2,1,1,5,42,1,1,14,462,24024,1,1,42,6006,1662804,701149020,1,1,132,87516,140229804,396499770810,1671643033734960,1,1,429,1385670,13672405890,278607172289160,9490348077234178440,475073684264389879228560",
			"name": "Triangle read by rows: T(n,k) is the number of n X k Young tableaux, where 0 \u003c= k \u003c= n.",
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A321716/b321716.txt\"\u003eRows n = 0..30, flattened\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Hook_length_formula\"\u003eHook length formula\u003c/a\u003e",
				"\u003ca href=\"/index/Y#Young\"\u003eIndex entries for sequences related to Young tableaux.\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = (n*k)! / (Product_{i=1..n} Product_{j=1..k} (i+j-1)).",
				"T(n, k) = A060854(n,k) for n,k \u003e 0.",
				"T(n, n) = A039622(n).",
				"T(n, k) = (n*k)!*BarnesG(n+1)*BarnesG(k+1)/BarnesG(n+k+1), where BarnesG(n) = A000178. - _G. C. Greubel_, May 04 2021"
			],
			"example": [
				"T(4,3) = 12! / ((6*5*4)*(5*4*3)*(4*3*2)*(3*2*1)) = 462.",
				"Triangle begins:",
				"  1;",
				"  1, 1;",
				"  1, 1,   2;",
				"  1, 1,   5,    42;",
				"  1, 1,  14,   462,     24024;",
				"  1, 1,  42,  6006,   1662804,    701149020;",
				"  1, 1, 132, 87516, 140229804, 396499770810, 1671643033734960;"
			],
			"mathematica": [
				"T[n_, k_]:= (n*k)!/Product[Product[i+j-1, {j,1,k}], {i,1,n}]; Table[T[n, k], {n, 0, 7}, {k, 0, n}] // Flatten (* _Amiram Eldar_, Nov 17 2018 *)",
				"T[n_, k_]:= (n*k)!*BarnesG[n+1]*BarnesG[k+1]/BarnesG[n+k+1];",
				"Table[T[n, k], {n, 0, 5}, {k, 0, n}] //Flatten (* _G. C. Greubel_, May 04 2021 *)"
			],
			"program": [
				"(Magma)",
				"A321716:= func\u003c n,k | n eq 0 select 1 else Factorial(n*k)/(\u0026*[ Round(Gamma(j+k)/Gamma(j)): j in [1..n]]) \u003e;",
				"[A321716(n,k): k in [0..n], n in [0..12]]; // _G. C. Greubel_, May 04 2021",
				"(Sage)",
				"def A321716(n,k): return factorial(n*k)/product( gamma(j+k)/gamma(j) for j in (1..n) )",
				"flatten([[A321716(n,k) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, May 04 2021"
			],
			"xref": [
				"Cf. A000178, A005789, A005790, A005791, A039622, A060854"
			],
			"keyword": "nonn,tabl",
			"offset": "0,6",
			"author": "_Seiichi Manyama_, Nov 17 2018",
			"references": 2,
			"revision": 31,
			"time": "2021-05-07T00:51:41-04:00",
			"created": "2018-11-17T19:58:17-05:00"
		}
	]
}