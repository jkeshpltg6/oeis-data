{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A098983",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 98983,
			"data": "0,0,0,1,2,2,2,2,4,3,3,1,4,4,4,3,5,4,6,4,6,4,6,3,9,5,7,3,7,4,7,4,8,7,9,4,10,6,8,6,10,6,11,7,12,8,11,5,13,8,11,6,11,8,13,6,10,7,13,6,16,7,13,8,16,7,14,7,13,10,15,7,18,10,17,10,18,9,17,8,17,12,17,8,21,12,15,9,18,13",
			"name": "Number of ways of writing n as a sum of a prime and a squarefree number.",
			"comment": [
				"From a posting by Hugh Montgomery to the Number Theory mailing list, Oct 05 2004: \"Estermann, JLMS (1931), established an asymptotic formula for a(n). Page, PLMS (1935), gave a quantitative version of this, with an error term roughly (log n)^5 smaller than the main term. Walfisz, Zur additiven Zahlentheorie II, Math. Z. 40 (1936), 592-607, established what we know today as the \"Siegel-Walfisz theorem\" in a series of lemmas and used this new tool to give the formula for a(n) with an error term that is smaller by a factor (log n)^c for any c.\""
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A098983/b098983.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Adrian Dudek, \u003ca href=\"http://arxiv.org/abs/1410.7459\"\u003eOn the Sum of a Prime and a Square-free Number\u003c/a\u003e, arXiv:1410.7459 [math.NT], 2014.",
				"T. Estermann, \u003ca href=\"http://jlms.oxfordjournals.org/content/s1-6/3/219.extract\"\u003eOn the representations of a number as the sum of a prime and a quadratfrei number\u003c/a\u003e, J. London Math. Soc., S1-6(3):219, 1931.",
				"A. Page, \u003ca href=\"https://doi.org/10.1112/plms/s2-39.1.116\"\u003eOn the Number of Primes in an Arithmetic Progression\u003c/a\u003e, Proc London Math Soc (1935) s2-39 (1): 116-141.",
				"A. Walfisz, \u003ca href=\"http://gdz.sub.uni-goettingen.de/dms/resolveppn/?PPN=GDZPPN002376350\"\u003eZur additiven Zahlentheorie II\u003c/a\u003e, Math. Z. 40 (1936), 592-607."
			],
			"formula": [
				"G.f.: (x^2+x^3+x^5+x^7+x^11+x^13+x^17+x^19+...)(x+x^2+x^3+x^5+x^6+x^7+x^10+x^11+x^13+x^14+x^15+x^17+x^19+...).",
				"a(n+1) = Sum(A008966(k)*A010051(n-k+1): 1\u003c=k\u003c=n) for n\u003e0. [_Reinhard Zumkeller_, Nov 04 2009]",
				"Dudek shows that a(n) \u003e 0 for n \u003e 2. - _Charles R Greathouse IV_, Dec 23 2020"
			],
			"example": [
				"a(8) = 4: 8=2+6=3+5=5+3=7+1."
			],
			"mathematica": [
				"m = 90; sf = Total[ x^Select[Range[m], SquareFreeQ] ]; pp = Sum[x^Prime[n], {n, 1, PrimePi @ Exponent[sf[[-1]], x]}]; CoefficientList[Series[pp * sf, {x, 0, m-1}], x] (* _Jean-François Alcover_, Jul 20 2011 *)"
			],
			"program": [
				"(Haskell)",
				"a098983 n = sum $ map (a008966 . (n -)) $ takeWhile (\u003c n) a000040_list",
				"-- _Reinhard Zumkeller_, Sep 14 2011",
				"(PARI) a(n)=my(s);forprime(p=2,n,s+=issquarefree(n-p));s \\\\ _Charles R Greathouse IV_, Jun 20 2013",
				"(PARI) a(n)=my(s);forsquarefree(k=1,n-2,if(isprime(n-k[1]),s++));s \\\\ _Charles R Greathouse IV_, Dec 23 2020"
			],
			"keyword": "nonn,nice",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_, Oct 24 2004",
			"references": 9,
			"revision": 31,
			"time": "2020-12-23T23:39:21-05:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}