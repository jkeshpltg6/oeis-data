{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A180013",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 180013,
			"data": "1,0,2,0,3,3,0,8,12,4,0,30,55,30,5,0,144,300,210,60,6,0,840,1918,1575,595,105,7,0,5760,14112,12992,5880,1400,168,8,0,45360,117612,118188,60921,17640,2898,252,9,0,403200,1095840,1181240,672840,224490,45360,5460,360,10",
			"name": "Triangular array read by rows: T(n,k) = number of fixed points in the permutations of {1,2,...,n} that have exactly k cycles; n\u003e=1, 1\u003c=k\u003c=n.",
			"comment": [
				"Row sums = n! which is the number of fixed points in all the permutations of {1,2,...,n}.",
				"It appears that column k = 2 is A001048 (with different offset).",
				"From _Olivier Gérard_, Oct 23 2012: (Start)",
				"This is a multiple of the triangle of Stirling numbers of the first kind, A180013(n,k) = (n)*A132393(n-1,k).",
				"Another interpretation is : T(n,n-k) is the total number of ways to insert the symbol n among the cycles of permutations of [n-1] with (n+1-k) cycles to form a canonical cycle representation of a permutation of [n]. For each cycle of length c, there are c places to insert a symbol, and for each permutation there is the possibility to create a new cycle (a fixed point).",
				"(End)"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A180013/b180013.txt\"\u003eTable of n, a(n) for n = 1..5050\u003c/a\u003e"
			],
			"formula": [
				"E.g.f.: for column k: x*(log(1/(1-x)))^(k-1)/(k-1)!.",
				"T(n, k) = [x^k] (n+1)!*hypergeom([-n,1-x],[1],1)) for n\u003e0. # _Peter Luschny_, Jan 28 2016"
			],
			"example": [
				"T(4,3)= 12 because there are 12 fixed points in the permutations of 4 that have 3 cycles: (1)(2)(4,3); (1)(3,2)(4); (1)(4,2)(3); (2,1)(3)(4); (3,1)(2)(4); (4,1)(2)(3) where the permutations are represented in their cycle notation.",
				"1",
				"0   2",
				"0   3    3",
				"0   8   12    4",
				"0  30   55   30   5",
				"0 144  300  210  60    6",
				"0 840 1918 1575 595  105   7"
			],
			"maple": [
				"egf:= k-\u003e x * (log(1/(1-x)))^(k-1) / (k-1)!:",
				"T:= (n,k)-\u003e n! * coeff(series(egf(k), x, n+1), x, n):",
				"seq(seq(T(n, k), k=1..n), n=1..10); # _Alois P. Heinz_, Jan 16 2011",
				"# As coefficients of polynomials:",
				"with(PolynomialTools): with(ListTools): A180013_row := proc(n)",
				"`if`(n=0, 1,(n+1)!*hypergeom([-n,1-x],[1],1)); CoefficientList(simplify(%),x) end: FlattenOnce([seq(A180013_row(n), n=0..9)]); # _Peter Luschny_, Jan 28 2016"
			],
			"mathematica": [
				"Flatten[Table[Table[(n + 1) Abs[StirlingS1[n, k]], {k, 0, n}], {n, 0, 9}],1] (* _Olivier Gérard_, Oct 23 2012 *)"
			],
			"xref": [
				"Cf. A000142, A001048. Diagonal, lower diagonal give: A000027, A027480(n+1)."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Geoffrey Critzer_, Jan 13 2011",
			"ext": [
				"More terms from _Alois P. Heinz_, Jan 16 2011"
			],
			"references": 1,
			"revision": 38,
			"time": "2016-01-30T04:07:24-05:00",
			"created": "2010-11-12T14:29:23-05:00"
		}
	]
}