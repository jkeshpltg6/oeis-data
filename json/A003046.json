{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003046",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3046,
			"id": "M1987",
			"data": "1,1,2,10,140,5880,776160,332972640,476150875200,2315045555222400,38883505145515430400,2285805733484270091494400,475475022233529990271933132800,353230394017289429773019124357120000,944693494975599542562153266945656012800000",
			"name": "Product of first n Catalan numbers.",
			"comment": [
				"The volume of a certain polytope (see Chan et al. reference). However, no combinatorial explanation for this is known."
			],
			"reference": [
				"H. W. Gould, A class of binomial sums and a series transformation, Utilitas Math., 45 (1994), 71-83.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A003046/b003046.txt\"\u003eTable of n, a(n) for n = 0..60\u003c/a\u003e",
				"V. Baldoni and M. Vergne, \u003ca href=\"https://doi.org/10.1007/s00031-008-9019-8\"\u003eKostant Partitions Functions and Flow Polytopes\u003c/a\u003e, Transform. Groups. 13 (2008), 447-469.",
				"C. S. Chan et al., \u003ca href=\"http://projecteuclid.org/euclid.em/1046889594\"\u003eOn the volume of a certain polytope\"\u003eOn the volume of a certain polytope\u003c/a\u003e, Experimental Mathematics, 9 (2000), 91-99.",
				"S. Corteel, J. S. Kim and K. Mészáros, \u003ca href=\"https://doi.org/10.1016/j.crma.2017.01.007\"\u003eFlow polytopes with Catalan volumes\u003c/a\u003e, C. R. Math., 355 (2017), 248-259.",
				"K. Mészáros and A. H. Morales, \u003ca href=\"https://doi.org/10.1093/imrn/rnt212\"\u003eFlow polytopes of signed graphs and the Kostant partition function\u003c/a\u003e, IMRN 3 (2015), 830-871.",
				"J. W. Moon and M. Sobel, \u003ca href=\"http://dx.doi.org/10.1016/0095-8956(77)90030-2\"\u003eEnumerating a class of nested group testing procedures\u003c/a\u003e, J. Combin. Theory, B23 (1977), 184-188.",
				"J. W. Moon, R. K. Guy, and N. J. A. Sloane, \u003ca href=\"/A003046/a003046.pdf\"\u003eCorrespondence, 1973\u003c/a\u003e",
				"D. Zeilberger, \u003ca href=\"http://arXiv.org/abs/math.CO/9811108\"\u003eProof of a Conjecture of Chan, Robbins and Yuen\u003c/a\u003e, arXiv:math/9811108 [math.CO], 1998.",
				"\u003ca href=\"/index/Di#divseq\"\u003eIndex to divisibility sequences\u003c/a\u003e"
			],
			"formula": [
				"a(n) = C(0)*C(1)*...*C(n), C() = A000108 = Catalan numbers.",
				"a(n) = sqrt((2^n)*A069640(n)/(2*n+1)!/n!), n\u003e0, where A069640(n) is an inverse determinant of the n X n Hilbert-like Matrix with elements M(i,j)=1/(i+j+1). - _Alexander Adamchuk_, May 17 2006",
				"a(n) ~ A^(3/2) * 2^(n^2 + n - 19/24) * exp(3*n/2 - 1/8) / (n^(3*n/2 + 15/8) * Pi^(n/2+1)), where A = 1.2824271291... is the Glaisher-Kinkelin constant (see A074962). - _Vaclav Kotesovec_, Nov 13 2014",
				"a(n) = A^(3/2)*2^(n^2 + n - 1/24)*BarnesG(n+3/2) / (exp(1/8)*Pi^(n/2 + 1/4)*BarnesG(n+3)), where BarnesG( ) is the Barnes G-function and A is the Glaisher-Kinkelin constant (A074962). - _Ilya Gutkovskiy_, Mar 16 2017"
			],
			"maple": [
				"seq(mul(binomial(2*k, k)/(1+k), k=0..n), n=0..13); # _Zerinvary Lajos_, Jul 02 2008"
			],
			"mathematica": [
				"a[n_] := Product[ CatalanNumber[k], {k, 0, n}]; Table[a[n], {n, 0, 13}] (* _Jean-François Alcover_, Dec 05 2012 *)",
				"FoldList[Times,1,CatalanNumber[Range[20]]] (* _Harvey P. Dale_, Apr 29 2013 *)",
				"Table[(2^(n^2+n-1/24) Glaisher^(3/2) BarnesG[n+3/2])/(Exp[1/8] Pi^(n/2+1/4) BarnesG[n+3]), {n, 0, 20}] (* _Vladimir Reshetnikov_, Nov 11 2015 *)"
			],
			"program": [
				"(Haskell)",
				"a003046 n = a003046_list !! n",
				"a003046_list = scanl1 (*) a000108_list",
				"-- _Reinhard Zumkeller_, Oct 01 2012",
				"(PARI) a(n) = prod(k=0, n, binomial(2*k,k)/(k+1)); \\\\ _Michel Marcus_, Sep 06 2021"
			],
			"xref": [
				"Cf. A003047, A000108, A055746, A069640, A005249, A051575, A067689, A074962."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"a(15) added by _Harvey P. Dale_, Apr 29 2013",
				"Typo in second formula corrected by _Vaclav Kotesovec_, Nov 13 2014",
				"Links added by _Alejandro H. Morales_, Jan 26 2020"
			],
			"references": 23,
			"revision": 78,
			"time": "2021-09-06T11:54:38-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}