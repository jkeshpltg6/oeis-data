{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A318560",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 318560,
			"data": "1,1,2,2,3,4,5,3,8,7,7,8,11,12,15,5,15,17,22,14,27,19,30,13,27,30,33,26,42,37,56,7,44,45,51,34,77,67,72,25",
			"name": "Number of combinatory separations of a multiset whose multiplicities are the prime indices of n in weakly decreasing order.",
			"comment": [
				"A multiset is normal if it spans an initial interval of positive integers. The type of a multiset is the unique normal multiset that has the same sequence of multiplicities when its entries are taken in increasing order. For example the type of 335556 is 112223. A (headless) combinatory separation of a multiset m is a multiset of normal multisets {t_1,...,t_k} such that there exist multisets {s_1,...,s_k} with multiset union m and such that s_i has type t_i for each i = 1...k.",
				"The prime indices of n are the n-th row of A296150."
			],
			"example": [
				"The a(18) = 17 combinatory separations of {1,1,2,2,3}:",
				"  {11223}",
				"  {1,1122} {1,1123} {1,1223} {11,112} {12,112} {12,122} {12,123}",
				"  {1,1,112} {1,1,122} {1,1,123} {1,11,11} {1,11,12} {1,12,12}",
				"  {1,1,1,11} {1,1,1,12}",
				"  {1,1,1,1,1}"
			],
			"mathematica": [
				"sps[{}]:={{}};sps[set:{i_,___}]:=Join@@Function[s,Prepend[#,s]\u0026/@sps[Complement[set,s]]]/@Cases[Subsets[set],{i,___}];",
				"mps[set_]:=Union[Sort[Sort/@(#/.x_Integer:\u003eset[[x]])]\u0026/@sps[Range[Length[set]]]];",
				"nrmptn[n_]:=Join@@MapIndexed[Table[#2[[1]],{#1}]\u0026,If[n==1,{},Flatten[Cases[FactorInteger[n]//Reverse,{p_,k_}:\u003eTable[PrimePi[p],{k}]]]]];",
				"normize[m_]:=m/.Rule@@@Table[{Union[m][[i]],i},{i,Length[Union[m]]}];",
				"Table[Length[Union[Sort/@Map[normize,mps[nrmptn[n]],{2}]]],{n,20}]"
			],
			"xref": [
				"Cf. A007716, A056239, A255906, A269134, A296150, A305936.",
				"Cf. A317791, A318283, A318284, A318285, A318559, A318562, A318566, A318567."
			],
			"keyword": "nonn,more",
			"offset": "1,3",
			"author": "_Gus Wiseman_, Aug 28 2018",
			"references": 5,
			"revision": 8,
			"time": "2018-08-29T16:49:47-04:00",
			"created": "2018-08-29T16:49:47-04:00"
		}
	]
}