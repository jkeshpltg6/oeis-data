{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A337182",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 337182,
			"data": "1,2,4,6,12,8,10,20,14,28,16,18,3,9,15,30,60,22,44,24,26,52,32,34,68,36,42,84,38,76,40,46,92,48,50,5,25,35,70,140,54,21,63,27,33,66,132,56,58,116,62,124,64,72,74,148,78,156,80,82,164,86,172,88,90,39,117,45,51,102,204",
			"name": "a(1) = 1, a(2) = 2; for n\u003e2, a(n) is the smallest number not already used which is a multiple of the product of the most frequently occurring distinct prime factors in a(n-1).",
			"comment": [
				"The terms are concentrated along lines, similar to A098550 and A336957, but show more complicated behavior. The sparse lines show wavy and random variations, some approaching each other before separating again. More intriguingly the data up to 3.5 million terms show at least three well defined lines that curve slowly upward and cross other lines which they initially start below. See the first and second linked image. Interestingly, terms with 3 and 7 as factors do not appear to be part of these three curving lines, while other primes examined up to 19 are part of these lines. See the last link images.",
				"The even terms dominate the higher values for a given range of n. The image for the next least prime factor of these even terms shows terms with 2 and 3 spread across multiple lines, some well defined and others sparse. Terms with 2 and 5 are in only four lines along with two other lines that appear to contain terms with 2 and all other primes as factors. See the third linked image.",
				"The lower terms for a given range of n are predominantly those with only one, two or three factors, while as the values increase they tend to have more and more factors. However, there are several lines on which terms with different numbers of factors all lie along. See the fourth linked image.",
				"For the first 3.5 million terms the primes appear in their natural order. See A338222 for the index where the primes appear. Although unproven it is highly probable that this is true for all primes. It is also likely all numbers eventually appear, i.e., this is a permutation of the positive integers. The lowest unseen number after 3.5 million terms is 1523. In the same range the only fixed point, other than the initial terms 1 and 2, is 15."
			],
			"link": [
				"Scott R. Shannon, \u003ca href=\"/A337182/b337182.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e",
				"Scott R. Shannon, \u003ca href=\"/A337182/a337182.png\"\u003eImage of the first 3500000 terms\u003c/a\u003e. The green line is a(n)=n.",
				"Scott R. Shannon, \u003ca href=\"/A337182/a337182_1.png\"\u003eImage of the first 3500000 terms showing the least prime factor\u003c/a\u003e. The terms with at least prime factor of 2,3,5,7,11,13 or 17 are shown as red, orange, yellow, green, blue, indigo and violet respectively, while terms with a least prime factor \u003e= 19 are shown as white.",
				"Scott R. Shannon, \u003ca href=\"/A337182/a337182_2.png\"\u003eImage of the first 3500000 terms showing the least prime factor other than 2 for the even terms\u003c/a\u003e. The even terms with a next least prime factor of 3,5,7,11,13 or 17 are shown as orange, yellow, green, blue, indigo and violet respectively, while terms with a least prime factor \u003e= 19 are shown as white.",
				"Scott R. Shannon, \u003ca href=\"/A337182/a337182_3.png\"\u003eImage of the first 3500000 terms showing the number of prime divisors for each term\u003c/a\u003e. The terms with 1,2,3,4,5,6,7 or 8 prime factors are shown as white, red, orange, yellow, green, blue, indigo and violet respectively, while terms with 9 or more are show in grey.",
				"Scott R. Shannon, \u003ca href=\"/A337182/a337182_4.png\"\u003eImage of the first 3500000 terms with 3 as a factor\u003c/a\u003e. Note none of the lines are curved.",
				"Scott R. Shannon, \u003ca href=\"/A337182/a337182_5.png\"\u003eImage of the first 3500000 terms with 5 as a factor\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A337182/a337182_6.png\"\u003eImage of the first 3500000 terms with 7 as a factor\u003c/a\u003e. Note none of the lines are curved.",
				"Scott R. Shannon, \u003ca href=\"/A337182/a337182_7.png\"\u003eImage of the first 3500000 terms with 11 as a factor\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A337182/a337182_8.png\"\u003eImage of the first 3500000 terms with 13 as a factor\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A337182/a337182_9.png\"\u003eImage of the first 3500000 terms with 17 as a factor\u003c/a\u003e.",
				"Scott R. Shannon, \u003ca href=\"/A337182/a337182_10.png\"\u003eImage of the first 3500000 terms with 19 as a factor\u003c/a\u003e."
			],
			"example": [
				"a(4) = 6 as a(3) = 4 = 2*2, and since 2 is the only prime factor, a(4) must be the smallest unused multiple of 2, which is 6.",
				"a(5) = 12 as a(4) = 6 = 2*3, thus as 2 and 3 both occur once, a(5) must be the smallest unused multiple of 2*3 = 6, which is 12.",
				"a(6) = 8 as a(5) = 12 = 2*2*3, thus as 2 is the most frequently occurring factor, a(6) must be the smallest unused multiple of 2, which is 8.",
				"a(13) = 3 as a(12) = 18 = 2*3*3, thus as 3 is the most frequently occurring factor, a(13) must be the smallest unused multiple of 3, which is 3.",
				"a(17) = 60 as a(16) = 30 = 2*3*5, thus as 2,3 and 5 all occur once, a(17) must be the smallest unused multiple of 2*3*5 = 30, which is 60."
			],
			"xref": [
				"Cf. A338222 (index of the prime terms), A098550, A336957, A064413, A027746."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Scott R. Shannon_, Jan 29 2021",
			"references": 3,
			"revision": 38,
			"time": "2021-02-24T04:24:15-05:00",
			"created": "2021-02-01T13:44:43-05:00"
		}
	]
}