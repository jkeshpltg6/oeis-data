{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A295224",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 295224,
			"data": "1,1,1,1,1,1,1,1,2,4,1,1,2,7,6,1,1,3,12,25,19,1,1,3,19,57,108,49,1,1,4,26,118,366,492,150,1,1,4,35,203,931,2340,2431,442,1,1,5,46,332,1989,7756,16252,12371,1424,1,1,5,57,494,3766,20254,68685,115940,65169,4522",
			"name": "Array read by antidiagonals: T(n,k) = number of nonequivalent dissections of a polygon into n k-gons by nonintersecting diagonals up to rotation (k \u003e= 3).",
			"comment": [
				"The polygon prior to dissection will have n*(k-2)+2 sides.",
				"In the Harary, Palmer and Read reference these are the sequences called H."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A295224/b295224.txt\"\u003eTable of n, a(n) for n = 1..1275\u003c/a\u003e",
				"F. Harary, E. M. Palmer and R. C. Read, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(75)90041-2\"\u003eOn the cell-growth problem for arbitrary polygons\u003c/a\u003e, Discr. Math. 11 (1975), 371-389.",
				"E. Krasko, A. Omelchenko, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v22i1p17\"\u003eBrown's Theorem and its Application for Enumeration of Dissections and Planar Trees\u003c/a\u003e, The Electronic Journal of Combinatorics, 22 (2015), #P1.17.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Fuss%E2%80%93Catalan_number\"\u003eFuss-Catalan number\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) ~ A295222(n,k)/n for fixed k."
			],
			"example": [
				"Array begins:",
				"  =====================================================",
				"  n\\k|    3     4      5       6        7         8",
				"  ---|-------------------------------------------------",
				"   1 |    1     1      1       1        1         1 ...",
				"   2 |    1     1      1       1        1         1 ...",
				"   3 |    1     2      2       3        3         4 ...",
				"   4 |    4     7     12      19       26        35 ...",
				"   5 |    6    25     57     118      203       332 ...",
				"   6 |   19   108    366     931     1989      3766 ...",
				"   7 |   49   492   2340    7756    20254     45448 ...",
				"   8 |  150  2431  16252   68685   219388    580203 ...",
				"   9 |  442 12371 115940  630465  2459730   7684881 ...",
				"  10 | 1424 65169 854981 5966610 28431861 104898024 ...",
				"  ..."
			],
			"mathematica": [
				"u[n_, k_, r_] := r*Binomial[(k - 1)*n + r, n]/((k - 1)*n + r);",
				"T[n_, k_] := u[n, k, 1] + (If[EvenQ[n], u[n/2, k, 1], 0] - u[n, k, 2])/2 + DivisorSum[GCD[n - 1, k], EulerPhi[#]*u[(n - 1)/#, k, k/#]\u0026]/k;",
				"Table[T[n - k + 1, k], {n, 1, 13}, {k, n, 3, -1}] // Flatten (* _Jean-François Alcover_, Nov 21 2017, after _Andrew Howroyd_ *) *)"
			],
			"program": [
				"(PARI) \\\\ here u is Fuss-Catalan sequence with p = k+1.",
				"u(n, k, r)={r*binomial((k - 1)*n + r, n)/((k - 1)*n + r)}",
				"T(n,k) = u(n,k,1) + (if(n%2==0, u(n/2,k,1))-u(n,k,2))/2 + sumdiv(gcd(n-1,k), d, eulerphi(d)*u((n-1)/d,k,k/d))/k;",
				"for(n=1, 10, for(k=3, 8, print1(T(n, k), \", \")); print);",
				"(Python)",
				"from sympy import binomial, gcd, totient, divisors",
				"def u(n, k, r): return r*binomial((k - 1)*n + r, n)//((k - 1)*n + r)",
				"def T(n, k): return u(n, k, 1) + ((u(n//2, k, 1) if n%2==0 else 0) - u(n, k, 2))//2 + sum([totient(d)*u((n - 1)//d, k, k//d) for d in divisors(gcd(n - 1, k))])//k",
				"for n in range(1, 11): print([T(n, k) for k in range(3, 9)]) # _Indranil Ghosh_, Dec 13 2017, after PARI code"
			],
			"xref": [
				"Columns k=3..6 are A001683(n+3), A005034, A005038, A221184(n-1).",
				"Cf. A033282, A070914, A295222, A295259, A295260."
			],
			"keyword": "nonn,tabl",
			"offset": "1,9",
			"author": "_Andrew Howroyd_, Nov 17 2017",
			"references": 12,
			"revision": 13,
			"time": "2017-12-29T10:31:55-05:00",
			"created": "2017-11-19T19:07:35-05:00"
		}
	]
}