{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A109812",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 109812,
			"data": "1,2,4,3,8,5,10,16,6,9,18,12,17,14,32,7,24,33,20,11,36,19,40,21,34,13,48,15,64,22,41,66,25,38,65,26,37,72,23,96,27,68,35,28,67,44,80,39,88,128,29,98,129,30,97,130,45,82,132,42,69,50,73,52,74,49,70,56,71,136,51",
			"name": "a(1)=1; thereafter a(n) = smallest positive integer not among the earlier terms of the sequence such that a(n) and a(n-1) have no common 1-bits in their binary representations.",
			"comment": [
				"Theorem: Sequence is a permutation of the positive integers. - _Leroy Quet_, Aug 16 2005",
				"Proof of theorem: It is clear that the sequence is infinite.",
				"Suppose M is the smallest number than does not occur. Choose n_0 such that for all n \u003e n_0, a(n) \u003e M. Let M_1 = max {a(i), 1 \u003c= i \u003c= n_0}, and let 2^k be smallest power of 2 \u003e M_1.",
				"For some n \u003e n_0, a(n) \u003e= 2^k. The first such term will be a(n) = 2^k. Then a(n+1) = smallest number not among a(1), ..., a(n), which is M. Contradiction. QED - _N. J. A. Sloane_, Jun 02 2018",
				"Inverse permutation = A113233; A113232 = a(a(n)). - _Reinhard Zumkeller_, Oct 19 2005",
				"Sequence of fixed points, where a(n)=n, is A340016. - _Thomas Scheuerle_, Dec 24 2020"
			],
			"link": [
				"Paul Tek, \u003ca href=\"/A109812/b109812.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Thomas Scheuerle, \u003ca href=\"/A109812/a109812.svg\"\u003eGraph of A109812 from n=0...10^7\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"formula": [
				"It would be nice to have a formula or recurrence. - _N. J. A. Sloane_, Jun 02 2018"
			],
			"example": [
				"a(6) = 5, which is 101 in binary. Of the terms not among (1,2,4,3,8,5), the earlier terms of the series, 10 (decimal) = 1010 (binary) is the smallest positive integer with no common 1-bits with the binary representation of 5.",
				"Of the other positive integers not occurring earlier in the sequence (6 = 110 binary, 7 = 111 binary, 9 = 1001 binary), each has at least one 1-bit in common with 5 = 101 in binary."
			],
			"program": [
				"(Haskell)",
				"import Data.Bits ((.\u0026.)); import Data.List (delete)",
				"a109812 n = a109812_list !! (n-1)",
				"a109812_list = f 0 [1..] :: [Int] where",
				"   f v ws = g ws where",
				"     g (x:xs) = if v .\u0026. x == 0 then x : f x (delete x ws) else g xs",
				"-- _Reinhard Zumkeller_, Sep 15 2014",
				"(Python)",
				"A109812_list, l1, s, b = [1], 1, 2, set()",
				"for _ in range(10**6):",
				"    i = s",
				"    while True:",
				"        if not (i in b or i \u0026 l1):",
				"            A109812_list.append(i)",
				"            l1 = i",
				"            b.add(i)",
				"            while s in b:",
				"                b.remove(s)",
				"                s += 1",
				"            break",
				"        i += 1 # _Chai Wah Wu_, Jun 04 2018"
			],
			"xref": [
				"Cf. A115510, A113232, A113233, A113234, A340016.",
				"For positions of powers of 2 see A305370.",
				"See also A305369.",
				"The graphs of A109812, A252867, A305369, A305372 all have roughly the same, mysterious, fractal-like structure. - _N. J. A. Sloane_, Jun 03 2018"
			],
			"keyword": "nonn,look",
			"offset": "1,2",
			"author": "_Leroy Quet_, Aug 16 2005",
			"ext": [
				"More terms from _John W. Layman_, Aug 18 2005",
				"Edited by _N. J. A. Sloane_, Jun 02 2018"
			],
			"references": 17,
			"revision": 58,
			"time": "2021-12-05T03:32:25-05:00",
			"created": "2005-09-21T03:00:00-04:00"
		}
	]
}