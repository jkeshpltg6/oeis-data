{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A068996",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 68996,
			"data": "6,3,2,1,2,0,5,5,8,8,2,8,5,5,7,6,7,8,4,0,4,4,7,6,2,2,9,8,3,8,5,3,9,1,3,2,5,5,4,1,8,8,8,6,8,9,6,8,2,3,2,1,6,5,4,9,2,1,6,3,1,9,8,3,0,2,5,3,8,5,0,4,2,5,5,1,0,0,1,9,6,6,4,2,8,5,2,7,2,5,6,5,4,0,8,0,3,5,6",
			"name": "Decimal expansion of 1 - 1/e.",
			"comment": [
				"From the \"derangements\" problem: this is the probability that if a large number of people are given their hats at random, at least one person gets their own hat.",
				"1-1/e is the limit to which (1 - !n/n!) {= 1 - A000166(n)/A000142(n) = A002467(n)/A000142(n)} converges as n tends to infinity. - _Lekraj Beedassy_, Apr 14 2005",
				"Also, this is lim_{n-\u003einf} P(n), where P(n) is the probability that a random rooted forest on [n] is not connected. - _Washington Bomfim_, Nov 01 2010",
				"Also equals the mode of a Gompertz distribution when the shape parameter is less than 1. - _Jean-François Alcover_, Apr 17 2013",
				"The asymptotic density of numbers with an even number of trailing zeros in their factorial base representation (A232744). - _Amiram Eldar_, Feb 26 2021"
			],
			"reference": [
				"Steven R. Finch, Mathematical Constants, Cambridge, 2003, Section 1.3, pp. 12-17.",
				"Anders Hald, A History of Probability and Statistics and Their Applications before 1750, Wiley, NY, 1990 (Chapter 19).",
				"John Riordan, An Introduction to Combinatorial Analysis, Wiley, 1958, p. 65."
			],
			"link": [
				"Brian Conrey and Tom Davis, \u003ca href=\"http://www.geometer.org/mathcircles/derange.pdf\"\u003eDerangements\u003c/a\u003e.",
				"MathOverflow, \u003ca href=\"https://mathoverflow.net/questions/128676/what-is-the-effect-of-adding-1-2-to-a-continued-fraction\"\u003eWhat is the effect of adding 1/2 to a continued fraction?\u003c/a\u003e.",
				"Jonathan Sondow and Eric Weisstein, \u003ca href=\"http://mathworld.wolfram.com/e.html\"\u003ee\u003c/a\u003e, MathWorld.",
				"Bala Subramanian, \u003ca href=\"https://electronics.stackexchange.com/questions/396653/why-time-constant-is-63-2-not-a-50-or-70\"\u003eWhy time constant is 63.2% not a 50 or 70%?\u003c/a\u003e (2018)."
			],
			"formula": [
				"Equals Integral_{x = 0 .. 1} exp(-x) dx. - _Alonso del Arte_, Jul 06 2012",
				"Equals -Sum_{k\u003e=1} (-1)^k/k!. - _Bruno Berselli_, May 13 2013",
				"Equals Sum_{k\u003e=0} ((1/((2*k+2)*(2*k)!). - _Fred Daniel Kline_, Mar 03 2016",
				"From _Peter Bala_, Nov 27: 2019: (Start)",
				"1 - 1/e =  Sum_{n \u003e= 0} n!/(A(n)*A(n+1)), where A(n) = A000522(n).",
				"Continued fraction expansion: [0; 1, 1, 1, 2, 1, 1, 4, 1, 1, 6, 1, 1, 8, ...].",
				"Related continued fraction expansions include",
				"2*(1 - 1/e) = [1; 3, 1, 3, 1, 1, 1, 3, 3, 3, 1, 3, 1, 3, 5, 3, 1, 5, ..., 1, 3, 2*n + 1, 3, 1, 2*n + 1, ...];",
				"(1/2)*(1 - 1/e) = [0; 3, 6, 10, 14, 18, ..., 4*n + 2, ...];",
				"4*(1 - 1/e) = [2; 1, 1, 8, 3, 1, 1, 1, 1, 7, 1, 1, 2, 1, 1, 1, 2, 7, 1, 2, 2, 1, 1, 1, 3, ..., 7, 1, n, 2, 1, 1, 1, n+1, ...];",
				"(1/4)*(1 - 1/e) = [0; 6, 3, 20, 7, 36, 11, 52, 15, ..., 16*n + 4, 4*n + 3, ...]. (End)",
				"Equals Integral_{x=0..1} x * cosh(x) dx. - _Amiram Eldar_, Aug 14 2020"
			],
			"example": [
				"0.6321205588285576784044762..."
			],
			"mathematica": [
				"RealDigits[1 - 1/E, 10, 100][[1]] (* _Alonso del Arte_, Jul 06 2012 *)"
			],
			"program": [
				"(PARI) 1 - exp(-1) \\\\ _Michel Marcus_, Mar 04 2016"
			],
			"xref": [
				"Cf. A000166, A068985, A185393, A232744."
			],
			"keyword": "nonn,cons,easy",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_, Apr 08 2002",
			"references": 13,
			"revision": 72,
			"time": "2021-02-26T20:12:02-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}