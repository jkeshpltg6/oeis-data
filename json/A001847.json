{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001847",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1847,
			"id": "M4793 N2045",
			"data": "1,11,61,231,681,1683,3653,7183,13073,22363,36365,56695,85305,124515,177045,246047,335137,448427,590557,766727,982729,1244979,1560549,1937199,2383409,2908411,3522221,4235671,5060441,6009091,7095093,8332863,9737793,11326283",
			"name": "Crystal ball sequence for 5-dimensional cubic lattice.",
			"comment": [
				"Number of nodes degree 10 in virtual, optimal chordal graphs of diameter d(G)=n - S. Bujnowski \u0026 B. Dubalski (slawb(AT)atr.bydgoszcz.pl), Mar 07 2002",
				"If Y_i (i=1,2,3,4,5) are 2-blocks of a (n+5)-set X then a(n-5) is the number of 10-subsets of X intersecting each Y_i (i=1,2,3,4,5). - _Milan Janjic_, Oct 28 2007",
				"Equals binomial transform of [1, 10, 40, 80, 80, 32, 0, 0, 0,...] where (1, 10, 40, 80, 80, 32) = row 5 of the Chebyshev triangle A013609. - _Gary W. Adamson_, Jul 19 2008"
			],
			"reference": [
				"L. Comtet, Advanced Combinatorics, Reidel, 1974, p. 81.",
				"E. Deza and M. M. Deza, Figurate numbers, World Scientific Publishing (2012), page 231.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A001847/b001847.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"D. Bump, K. Choi, P. Kurlberg, and J. Vaaler, \u003ca href=\"http://www.cecm.sfu.ca/~choi/paper/lrh.pdf\"\u003eA local Riemann hypothesis, I\u003c/a\u003e pages 16 and 17",
				"J. H. Conway and N. J. A. Sloane, Low-Dimensional Lattices VII: Coordination Sequences, Proc. Royal Soc. London, A453 (1997), 2369-2389 (\u003ca href=\"http://neilsloane.com/doc/Me220.pdf\"\u003epdf\u003c/a\u003e).",
				"Milan Janjic, \u003ca href=\"http://www.pmfbl.org/janjic/\"\u003eTwo Enumerative Functions\u003c/a\u003e",
				"G. Kreweras, \u003ca href=\"http://www.numdam.org/item?id=BURO_1973__20__3_0\"\u003eSur les hiérarchies de segments\u003c/a\u003e, Cahiers du Bureau Universitaire de Recherche Opérationnelle, Institut de Statistique, Université de Paris, #20 (1973).",
				"G. Kreweras, \u003ca href=\"/A001844/a001844.pdf\"\u003eSur les hiérarchies de segments\u003c/a\u003e, Cahiers du Bureau Universitaire de Recherche Opérationnelle, Institut de Statistique, Université de Paris, #20 (1973). (Annotated scanned copy)",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"R. G. Stanton and D. D. Cowan, \u003ca href=\"http://dx.doi.org/10.1137/1012049\"\u003eNote on a \"square\" functional equation\u003c/a\u003e, SIAM Rev., 12 (1970), 277-279.",
				"\u003ca href=\"/index/Cor#crystal_ball\"\u003eIndex entries for crystal ball sequences\u003c/a\u003e"
			],
			"formula": [
				"G.f.: (1+x)^5 /(1-x)^6.",
				"a(n) = (4*n^5+10*n^4+40*n^3+50*n^2+46*n+15)/15. - S. Bujnowski \u0026 B. Dubalski (slawb(AT)atr.bydgoszcz.pl), Mar 07 2002",
				"a(n) = sum(k=0..min(5,n), 2^k * binomial(5,k)* binomial(n,k) ). See Bump et al. - _Tom Copeland_, Sep 05 2014"
			],
			"example": [
				"a(5)=1683, (4*5^5+10*5^4+40*5^3+50*5^2+46*5+15)/15 = (12500+6250+5000+230+15)/15 = 25245/15 = 1683."
			],
			"maple": [
				"for n from 1 to k do eval((4*n^5+10*n^4+40*n^3+50*n^2+46*n+15)/15) od;",
				"A001847:=(z+1)**5/(z-1)**6; # conjectured by _Simon Plouffe_ in his 1992 dissertation"
			],
			"mathematica": [
				"CoefficientList[Series[(z+1)^5/(z-1)^6, {z, 0, 200}], z] (* _Vladimir Joseph Stephan Orlovsky_, Jun 19 2011 *)"
			],
			"xref": [
				"Cf. A005408, A001844, A001845, A001846, A013609.",
				"Cf. A240876."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 10,
			"revision": 59,
			"time": "2021-03-12T22:32:35-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}