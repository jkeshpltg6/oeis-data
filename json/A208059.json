{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A208059",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 208059,
			"data": "0,1,1,1,1,1,1,1,1,1,3,48,14,76793,385,12,232,98,24,4,6,10952,8,575702095,18,82,39,16,7,6,26,9,8,30,12,13,182,449,25,62",
			"name": "Start with n, successively subtract each digit of the resulting sequence (the digits of a negative term being the negatives of that term's digits): a(n) is the number of steps needed to get to the first zero.",
			"comment": [
				"This is the same procedure used in A207505 with an allowance made to continue the process if we miss zero and enter negative territory on our initial downward run. A downward run will succumb to an upward run, and vice versa, with each run presenting another opportunity to hit zero. (A random-digit trail entails, per run, a 1 in 5 chance of hitting zero.)",
				"a(23) was first computed by Nicolas Berr. According to his calculations, a(40) does not hit zero in its first 15 sign-change crossings. The 16th crossing is ~2*10^15.",
				"If a negative number appears in column a, say -107, then in column b we write successively -1, 0, -7.",
				"As far as I know, it is only a conjecture that for any starting value n we always reach a 0. - _N. J. A. Sloane_, Jun 01 2012",
				"a(40) (at least 10^15) is the smallest unknown value."
			],
			"link": [
				"Eric Angelini, \u003ca href=\"http://www.cetteadressecomportecinquantesignes.com/MissNumbers.htm\"\u003eMiss Numbers\u003c/a\u003e, Feb 2012",
				"Eric Angelini, \u003ca href=\"/A208059/a208059.pdf\"\u003eMiss Numbers\u003c/a\u003e [Cached copy, with permission]",
				"Christopher Creutzig, \u003ca href=\"http://chesswanks.com/num/a208059/analogues.txt\"\u003eA208059 analogues (bases 2-16)\u003c/a\u003e",
				"Christopher Creutzig, \u003ca href=\"/A208059/a208059.txt\"\u003eA208059 analogues (bases 2-16)\u003c/a\u003e [Cached copy made Jun 01 2012]",
				"Hans Havermann, \u003ca href=\"http://chesswanks.com/num/a208059/\"\u003eA208059 evolutions\u003c/a\u003e"
			],
			"example": [
				"When successively subtracting its own digit-trail, 12 requires 14 steps to hit its first zero, achieved on its first upward run, thus making a(12) = 14:",
				".a.....b......c",
				"12  -  1  =  11",
				"11  -  2  =   9",
				"9   -  1  =   8",
				"8   -  1  =   7",
				"7   -  9  =  -2",
				"-2  -  8  = -10",
				"-10 -  7  = -17",
				"-17 -(-2) = -15",
				"-15 -(-1) = -14",
				"-14 -(-0) = -14",
				"-14 -(-1) = -13",
				"-13 -(-7) =  -6",
				"-6  -(-1) =  -5",
				"-5  -(-5) =   0",
				"etc., ad infinitum.",
				"We get column b by reading column a digit-by-digit."
			],
			"mathematica": [
				"f[n_] := Module[{x = n, l, c}, c = 0; l = IntegerDigits[x];",
				"   While[x != 0, c++;  x = x - First[l];",
				"    l = Join[Rest[l], Sign[x]*IntegerDigits[x]]; ]; c] ;",
				"Table[f[n], {n, 0, 22}] (* _Robert Price_, Apr 04 2020 *)"
			],
			"program": [
				"(PARI) A208059(n,v=0/*verbose: print all terms if \u003e0*/,a=[])={ v\u0026print1(n); a=eval(Vec(Str(n))); for(c=0,9e9, n|return(c); a=concat(vecextract(a,\"^1\"), eval(Vec(Str(abs(n-=a[1]))))*sign(n)); v\u0026print1(\", \"n)) }  \\\\ _M. F. Hasler_, Mar 03 2012"
			],
			"xref": [
				"Cf. A207505, A207506."
			],
			"keyword": "nonn,base,more",
			"offset": "0,11",
			"author": "_Hans Havermann_, Feb 23 2012",
			"ext": [
				"More terms from _Hans Havermann_, Mar 06 2012",
				"Edited by _N. J. A. Sloane_, Jun 01 2012"
			],
			"references": 16,
			"revision": 59,
			"time": "2021-01-23T05:31:31-05:00",
			"created": "2012-02-23T16:53:00-05:00"
		}
	]
}