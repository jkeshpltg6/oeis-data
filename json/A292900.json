{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A292900",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 292900,
			"data": "1,0,1,0,-1,1,0,1,-4,0,0,-1,47,-10,-1,0,1,-221,205,-209,0,0,-1,953,-5495,10789,-427,1,0,1,-3953,123445,-8646163,177093,-22807,0,0,-1,16097,-2534735,22337747,-356249173,3440131,-46212,-1",
			"name": "Triangle read by rows, a generalization of the Bernoulli numbers, the numerators for n\u003e=0 and 0\u003c=k\u003c=n.",
			"comment": [
				"The diagonal B(n, n) gives the Bernoulli numbers B_n = B_n(1). The formula is due to L. Kronecker and the generalization to Fukuhara, Kawazumi and Kuno."
			],
			"link": [
				"S. Fukuhara, N. Kawazumi and Y. Kuno, \u003ca href=\"https://arxiv.org/abs/1505.04840\"\u003eGeneralized Kronecker formula for Bernoulli numbers and self-intersections of curves on a surface\u003c/a\u003e, arXiv:1505.04840 [math.NT], 2015.",
				"L. Kronecker, \u003ca href=\"http://www.digizeitschriften.de/dms/img/?PID=GDZPPN002158752\"\u003eÜber die Bernoullischen Zahlen\u003c/a\u003e, J. Reine Angew. Math. 94 (1883), 268-269."
			],
			"formula": [
				"B(n, k) = Sum_{j=0..k}(((-1)^(j-n)/(j+1))*binomial(k+1, j+1)*Sum_{i=0..j}(i^n*(j-i+1)^(k-n))) if n \u003e= 1 and B(0, 0) = 1.",
				"B_n = B(n, n) = Sum_{j=0..n}((-1)^(n-j)/(j+1))*binomial(n+1,j+1)*(Sum_{i=0..j}i^n).",
				"T(n, k) = numerator(B(n, k))."
			],
			"example": [
				"The triangle T(n, k) begins:",
				"[0], 1",
				"[1], 0,  1",
				"[2], 0, -1,     1",
				"[3], 0,  1,    -4,        0",
				"[4], 0, -1,    47,      -10,       -1",
				"[5], 0,  1,  -221,      205,     -209,          0",
				"[6], 0, -1,   953,    -5495,    10789,       -427,       1",
				"[7], 0,  1, -3953,   123445, -8646163,     177093,  -22807,      0",
				"[8], 0, -1, 16097, -2534735, 22337747, -356249173, 3440131, -46212, -1",
				"The rational triangle B(n, k) begins:",
				"[0], 1",
				"[1], 0,  1/2",
				"[2], 0, -1/2,      1/6",
				"[3], 0,  1/2,     -4/3,          0",
				"[4], 0, -1/2,    47/12,      -10/3,         -1/30",
				"[5], 0,  1/2,  -221/24,      205/9,       -209/20,          0",
				"[6], 0, -1/2,   953/48,   -5495/54,      10789/80,    -427/10,       1/42",
				"[7], 0,  1/2, -3953/96, 123445/324, -8646163/8640, 177093/200, -22807/105, 0"
			],
			"maple": [
				"B := (n, k) -\u003e `if`(n = 0, 1, add(((-1)^(j-n)/(j+1))*binomial(k+1, j+1)*add(i^n*(j-i+1)^(k-n), i=0..j), j=0..k)):",
				"for n from 0 to 8 do seq(numer(B(n,k)), k=0..n) od;"
			],
			"mathematica": [
				"B[0, 0] = 1; B[n_, k_] := Sum[(-1)^(j-n)/(j+1)*Binomial[k+1, j+1]* Sum[i^n*(j-i+1)^(k-n) , {i, 0, j}] , {j, 0, k}];",
				"Table[B[n, k] // Numerator, {n, 0, 8}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Jul 19 2018, from Maple *)"
			],
			"xref": [
				"Cf. A292901 (denominators), B(n, n) = A164555(n)/A027642(n), A215083."
			],
			"keyword": "sign,tabl,frac",
			"offset": "0,9",
			"author": "_Peter Luschny_, Oct 01 2017",
			"references": 1,
			"revision": 20,
			"time": "2018-07-19T08:23:29-04:00",
			"created": "2017-10-01T16:18:48-04:00"
		}
	]
}