{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A340784",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 340784,
			"data": "1,4,9,10,16,21,22,25,34,36,39,40,46,49,55,57,62,64,81,82,84,85,87,88,90,91,94,100,111,115,118,121,129,133,134,136,144,146,155,156,159,160,166,169,183,184,187,189,194,196,198,203,205,206,210,213,218,220",
			"name": "Heinz numbers of even-length integer partitions of even numbers.",
			"comment": [
				"The Heinz number of a partition (y_1,...,y_k) is prime(y_1)*...*prime(y_k), so these are positive integers whose number of prime indices and sum of prime indices are both even, counting multiplicity in both cases."
			],
			"formula": [
				"Intersection of A028260 and A300061."
			],
			"example": [
				"The sequence of partitions together with their Heinz numbers begins:",
				"      1: ()            57: (8,2)            118: (17,1)",
				"      4: (1,1)         62: (11,1)           121: (5,5)",
				"      9: (2,2)         64: (1,1,1,1,1,1)    129: (14,2)",
				"     10: (3,1)         81: (2,2,2,2)        133: (8,4)",
				"     16: (1,1,1,1)     82: (13,1)           134: (19,1)",
				"     21: (4,2)         84: (4,2,1,1)        136: (7,1,1,1)",
				"     22: (5,1)         85: (7,3)            144: (2,2,1,1,1,1)",
				"     25: (3,3)         87: (10,2)           146: (21,1)",
				"     34: (7,1)         88: (5,1,1,1)        155: (11,3)",
				"     36: (2,2,1,1)     90: (3,2,2,1)        156: (6,2,1,1)",
				"     39: (6,2)         91: (6,4)            159: (16,2)",
				"     40: (3,1,1,1)     94: (15,1)           160: (3,1,1,1,1,1)",
				"     46: (9,1)        100: (3,3,1,1)        166: (23,1)",
				"     49: (4,4)        111: (12,2)           169: (6,6)",
				"     55: (5,3)        115: (9,3)            183: (18,2)"
			],
			"mathematica": [
				"primeMS[n_]:=If[n==1,{},Flatten[Cases[FactorInteger[n],{p_,k_}:\u003eTable[PrimePi[p],{k}]]]];",
				"Select[Range[100],EvenQ[PrimeOmega[#]]\u0026\u0026EvenQ[Total[primeMS[#]]]\u0026]"
			],
			"xref": [
				"Note: A-numbers of Heinz-number sequences are in parentheses below.",
				"The case of prime powers is A056798.",
				"These partitions are counted by A236913.",
				"The odd version is A160786 (A340931).",
				"A000009 counts partitions into odd parts (A066208).",
				"A001222 counts prime factors.",
				"A047993 counts balanced partitions (A106529).",
				"A056239 adds up prime indices.",
				"A058695 counts partitions of odd numbers (A300063).",
				"A061395 selects the maximum prime index.",
				"A072233 counts partitions by sum and length.",
				"A112798 lists the prime indices of each positive integer.",
				"- Even -",
				"A027187 counts partitions of even length/maximum (A028260/A244990).",
				"A034008 counts compositions of even length.",
				"A035363 counts partitions into even parts (A066207).",
				"A058696 counts partitions of even numbers (A300061).",
				"A067661 counts strict partitions of even length (A030229).",
				"A339846 counts factorizations of even length.",
				"A340601 counts partitions of even rank (A340602).",
				"A340785 counts factorizations into even factors.",
				"A340786 counts even-length factorizations into even factors.",
				"Cf. A026424, A257541, A300272, A326837, A326845, A340385 (A340386), A340604."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Gus Wiseman_, Jan 30 2021",
			"references": 10,
			"revision": 11,
			"time": "2021-02-01T09:14:29-05:00",
			"created": "2021-02-01T09:14:29-05:00"
		}
	]
}