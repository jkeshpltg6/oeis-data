{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005732",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5732,
			"id": "M4514",
			"data": "1,8,35,111,287,644,1302,2430,4257,7084,11297,17381,25935,37688,53516,74460,101745,136800,181279,237083,306383,391644,495650,621530,772785,953316,1167453,1419985,1716191,2061872,2463384,2927672,3462305,4075512,4776219,5574087,6479551",
			"name": "a(n) = binomial(n+3,6) + binomial(n+1,5) + binomial(n,5).",
			"comment": [
				"Place n points in general position on a circle, join them in all possible ways; how many triangles can be seen?",
				"Equals binomial transform of [1, 7, 20, 29, 22, 8, 1, 0, 0, 0, ...]. - _Gary W. Adamson_, Jun 13 2008"
			],
			"reference": [
				"C. L. Liu, Introduction to Combinatorial Analysis. McGraw-Hill, NY, 1968, p. 20.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A005732/b005732.txt\"\u003eTable of n, a(n) for n = 3..1000\u003c/a\u003e",
				"R. J. Cormier and R. B. Eggleton, \u003ca href=\"http://www.jstor.org/stable/2690116\"\u003eCounting by correspondence\u003c/a\u003e, Math. Mag., 49 (1976), 181-186.",
				"R. K. Guy \u0026 M. E. Larsen, \u003ca href=\"/A005732/a005732.pdf\"\u003eCorrespondence, 1986-87\u003c/a\u003e",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"T. Sillke, \u003ca href=\"http://www.mathematik.uni-bielefeld.de/~sillke/SEQUENCES/triangle_counting\"\u003eNumber of triangles for a convex n-gon\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_07\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (7,-21,35,-35,21,-7,1)."
			],
			"formula": [
				"G.f.: x^3*(-1-x+x^3) / (x-1)^7 . - _Simon Plouffe_ in his 1992 dissertation",
				"a(2n-1) = A006600(2n-1) for n \u003e 1; a(2n) = A006600(2n) + A260417(n) for n \u003e 1. - _Jonathan Sondow_, Jul 25 2015"
			],
			"mathematica": [
				"Table[Binomial[n+3,6]+Binomial[n+1,5]+Binomial[n,5],{n,3,40}]  (* _Harvey P. Dale_, Apr 09 2011 *)"
			],
			"program": [
				"(MAGMA) [Binomial(n+3, 6) + Binomial(n+1, 5) +Binomial(n,5): n in [3..100]]; // _Vincenzo Librandi_, Apr 10 2011",
				"(Haskell)",
				"a005732 n = a005732_list !! (n-3)",
				"a005732_list = 1 : 8 : f (drop 5 a007318_tabl) where",
				"   f (us:pss@(vs:_:ws:_)) = (us !! 5 + vs !! 5 + ws !! 6) : f pss",
				"-- _Reinhard Zumkeller_, Mar 11 2014",
				"(PARI) a(n)=binomial(n+3,6) + binomial(n+1,5) + binomial(n,5) \\\\ _Charles R Greathouse IV_, Feb 19 2017"
			],
			"xref": [
				"Often confused with A006600.",
				"Cf. A007318, A260417."
			],
			"keyword": "nonn,easy,nice",
			"offset": "3,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Thanks to _Joshua Zucker_, Ted Alper and Joe Keane for clarifying the connection with A006600."
			],
			"references": 4,
			"revision": 52,
			"time": "2021-03-12T22:32:36-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}