{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A049963",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 49963,
			"data": "1,2,4,9,25,43,93,220,617,1016,2039,4112,8401,17598,38292,90070,252612,415156,830319,1660672,3321521,6643838,13290772,26595030,53262532,106850150,214945816,434874798,889700788,1859656696",
			"name": "a(n) = a(1) + a(2) + ... + a(n-1) + a(m) for n \u003e= 4, where m = 2*n - 2 - 2^(p+1) and p is the unique integer such that 2^p \u003c n-1 \u003c= 2^(p+1), with a(1) = 1, a(2) = 2 and a(3) = 4.",
			"comment": [
				"The number m in the definition of the sequence equals 2*n - 2 - x, where x is the smallest power of 2 \u003e= n-1. It turns out that m = 1 + A006257(n-2), where the sequence b(n) = A006257(n) satisfies b(2*n) = 2*b(n) - 1 and b(2*n + 1) = 2*b(n) + 1, and it is related to the so-called Josephus's problem. - _Petros Hadjicostas_, Sep 25 2019"
			],
			"link": [
				"\u003ca href=\"/index/J#Josephus\"\u003eIndex entries for sequences related to the Josephus Problem\u003c/a\u003e"
			],
			"formula": [
				"a(n) = a(1 + A006257(n-2)) + Sum_{i = 1..n-1} a(i) for n \u003e= 4 with a(1) = 1, a(2) = 2 and a(3) = 4. - _Petros Hadjicostas_, Sep 25 2019"
			],
			"example": [
				"From _Petros Hadjicostas_, Sep 25 2019: (Start)",
				"a(4) = a(1 + A006257(4-2)) + a(1) + a(2) + a(3) = a(2) + a(1) + a(2) + a(3) = 9.",
				"a(7) = a(1 + A006257(7-2)) + a(1) + a(2) + a(3) + a(4) + a(5) + a(6) = a(4) + a(1) + a(2) + a(3) + a(4) + a(5) + a(6) = 93.",
				"(End)"
			],
			"maple": [
				"a := proc(n) local i; option remember; if n \u003c 4 then return [1, 2, 4][n]; end if; add(a(i), i = 1 .. n - 1) + a(2*n - 3 - Bits:-Iff(n - 2, n - 2)); end proc;",
				"seq(a(n), n = 1..40); # _Petros Hadjicostas_, Sep 25 2019, courtesy of _Peter Luschny_"
			],
			"xref": [
				"Cf. A006257, A049920, A049939, A049960, A049964, A049979.",
				"Cf. A049914 (similar with minus a(m/2)), A049915 (similar with minus a(m)), A049962 (similar with plus a(m/2))."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Clark Kimberling_",
			"ext": [
				"Name edited by _Petros Hadjicostas_, Sep 25 2019"
			],
			"references": 3,
			"revision": 31,
			"time": "2020-06-23T01:24:26-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}