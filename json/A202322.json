{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A202322",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 202322,
			"data": "4,4,2,8,5,4,4,0,1,0,0,2,3,8,8,5,8,3,1,4,1,3,2,7,9,9,9,9,9,9,3,3,6,8,1,9,7,1,6,2,6,2,1,2,9,3,7,3,4,7,9,6,8,4,7,1,7,7,3,3,0,7,6,9,8,2,0,1,5,9,9,2,1,4,2,0,0,4,0,7,8,4,9,0,8,6,5,9,2,4,8,1,7,8,7,3,9,5,5",
			"name": "Decimal expansion of x satisfying x+2=exp(-x).",
			"comment": [
				"For many choices of u and v, there is just one value of x satisfying u*x+v=e^(-x).  Guide to related sequences, with graphs included in Mathematica programs:",
				"u.... v.... x",
				"1.... 2.... A202322",
				"1.... 3.... A202323",
				"2.... 2.... A202353",
				"2.... e.... A202354",
				"1... -1.... A202355",
				"1.... 0.... A030178",
				"2.... 0.... A202356",
				"e.... 0.... A202357",
				"3.... 0.... A202392",
				"Suppose that f(x,u,v) is a function of three real variables and that g(u,v) is a function defined implicitly by f(g(u,v),u,v)=0.  We call the graph of z=g(u,v) an implicit surface of f.",
				"For an example related to A202322, take f(x,u,v)=x+2-e^(-x) and g(u,v) = a nonzero solution x of f(x,u,v)=0.  If there is more than one nonzero solution, care must be taken to ensure that the resulting function g(u,v) is single-valued and continuous.  A portion of an implicit surface is plotted by Program 2 in the Mathematica section."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A202322/b202322.txt\"\u003eTable of n, a(n) for n = 0..5000\u003c/a\u003e"
			],
			"formula": [
				"x(u,v) = W(e^(v/u)/u) - v/u, where W = ProductLog = LambertW. - _Jean-François Alcover_, Feb 14 2013",
				"Equals A226571 - 2 = LambertW(exp(2))-2. - _Vaclav Kotesovec_, Jan 09 2014"
			],
			"example": [
				"x=-0.442854401002388583141327999999336819716262..."
			],
			"mathematica": [
				"(* Program 1:  A202322 *)",
				"u = 1; v = 2;",
				"f[x_] := u*x + v; g[x_] := E^-x",
				"Plot[{f[x], g[x]}, {x, -1, 2}, {AxesOrigin -\u003e {0, 0}}]",
				"r = x /. FindRoot[f[x] == g[x], {x, -.45, -.44}, WorkingPrecision -\u003e 110]",
				"RealDigits[r]  (* A202322 *)",
				"(* Program 2: implicit surface of u*x+v=e^(-x) *)",
				"f[{x_, u_, v_}] := u*x + v - E^-x;",
				"t = Table[{u, v, x /. FindRoot[f[{x, u, v}] == 0, {x, 1, 2}]}, {v, 1, 3}, {u, 1, 3}];",
				"ListPlot3D[Flatten[t, 1]] (* for A202322 *)",
				"RealDigits[ ProductLog[E^2] - 2, 10, 99] // First (* _Jean-François Alcover_, Feb 14 2013 *)"
			],
			"program": [
				"(PARI) lambertw(exp(2)) - 2 \\\\ _G. C. Greubel_, Jun 10 2017"
			],
			"xref": [
				"Cf. A202320."
			],
			"keyword": "nonn,cons",
			"offset": "0,1",
			"author": "_Clark Kimberling_, Dec 18 2011",
			"ext": [
				"Digits from a(84) on corrected by _Jean-François Alcover_, Feb 14 2013"
			],
			"references": 9,
			"revision": 21,
			"time": "2017-06-10T22:35:41-04:00",
			"created": "2011-12-19T17:53:48-05:00"
		}
	]
}