{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A072411",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 72411,
			"data": "1,1,1,2,1,1,1,3,2,1,1,2,1,1,1,4,1,2,1,2,1,1,1,3,2,1,3,2,1,1,1,5,1,1,1,2,1,1,1,3,1,1,1,2,2,1,1,4,2,2,1,2,1,3,1,3,1,1,1,2,1,1,2,6,1,1,1,2,1,1,1,6,1,1,2,2,1,1,1,4,4,1,1,2,1,1,1,3,1,2,1,2,1,1,1,5,1,2,2,2,1,1,1,3,1,1,1,6,1,1,1,4,1,1,1,2,2,1,1,3",
			"name": "LCM of exponents in prime factorization of n, a(1) = 1.",
			"link": [
				"Antti Karttunen, \u003ca href=\"/A072411/b072411.txt\"\u003eTable of n, a(n) for n = 1..10080\u003c/a\u003e",
				"\u003ca href=\"/index/Eu#epf\"\u003eIndex entries for sequences computed from exponents in factorization of n\u003c/a\u003e",
				"\u003ca href=\"/index/Lc#lcm\"\u003eIndex entries for sequences related to lcm's\u003c/a\u003e"
			],
			"formula": [
				"a(1) = 1; for n \u003e 1, a(n) = lcm(A067029(n), a(A028234(n)). - _Antti Karttunen_, Aug 09 2016",
				"From _Antti Karttunen_, Aug 22 2017: (Start)",
				"a(n) = A284569(A156552(n)).",
				"a(n) = A290103(A181819(n)).",
				"a(A289625(n)) = A002322(n).",
				"a(A290095(n)) = A055092(n).",
				"a(A275725(n)) = A060131(n).",
				"a(A260443(n)) = A277326(n).",
				"a(A283477(n)) = A284002(n). (End)"
			],
			"example": [
				"n = 288 = 2*2*2*2*2*3*3; lcm(5,2) = 10; Product(5,2) = 10, max(5,2) = 5;",
				"n = 180 = 2*2*3*3*5; lcm(2,2,1) = 2; Product(2,2,1) = 4; max(2,2,1) = 2; it deviates both from maximum of exponents (A051903, for the first time at n=72), and product of exponents (A005361, for the first time at n=36).",
				"For n = 36 = 2*2*3*3 = 2^2 * 3^2 we have a(36) = lcm(2,2) = 2.",
				"For n = 72 = 2*2*2*3*3 = 2^3 * 3^2 we have a(72) = lcm(2,3) = 6.",
				"For n = 144 = 2^4 * 3^2 we have a(144) = lcm(2,4) = 4.",
				"For n = 360 = 2^3 * 3^2 * 5^1 we have a(360) = lcm(1,2,3) = 6."
			],
			"mathematica": [
				"Table[LCM @@ Last /@ FactorInteger[n], {n, 2, 100}] (* _Ray Chandler_, Jan 24 2006 *)"
			],
			"program": [
				"(Scheme, with memoization-macro definec)",
				"(definec (A072411 n) (if (= 1 n) 1 (lcm (A067029 n) (A072411 (A028234 n))))) ;; _Antti Karttunen_, Aug 09 2016",
				"(Python)",
				"from sympy import lcm, factorint",
				"def a(n):",
				"    l=[]",
				"    f=factorint(n)",
				"    for i in f: l+=[f[i],]",
				"    return lcm(l)",
				"print([a(n) for n in range(1, 151)]) # _Indranil Ghosh_, Mar 25 2017",
				"(PARI) a(n) = lcm(factor(n)[,2]); \\\\ _Michel Marcus_, Mar 25 2017"
			],
			"xref": [
				"Cf. A028234, A067029, A072412-A072414, A273058, A284569, A290103.",
				"Similar sequences: A001222 (sum of exponents), A005361 (product), A051903 (maximal exponent), A051904 (minimal exponent), A052409 (gcd of exponents), A267115 (bitwise-and), A267116 (bitwise-or), A268387 (bitwise-xor).",
				"Cf. also A055092, A060131.",
				"Differs from A290107 for the first time at n=144.",
				"After the initial term, differs from A157754 for the first time at n=360."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Labos Elemer_, Jun 17 2002",
			"ext": [
				"a(1) = 1 prepended and the data section filled up to 120 terms by _Antti Karttunen_, Aug 09 2016"
			],
			"references": 32,
			"revision": 49,
			"time": "2020-04-09T10:51:28-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}