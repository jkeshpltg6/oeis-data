{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A286784",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 286784,
			"data": "1,1,1,2,4,1,5,15,9,1,14,56,56,16,1,42,210,300,150,25,1,132,792,1485,1100,330,36,1,429,3003,7007,7007,3185,637,49,1,1430,11440,32032,40768,25480,7840,1120,64,1,4862,43758,143208,222768,179928,77112,17136,1836,81,1,16796,167960,629850,1162800,1162800,651168,203490,34200,2850,100,1",
			"name": "Triangle T(n,k) read by rows: coefficients of polynomials P_n(t) defined in Formula section.",
			"comment": [
				"T(n,k) is the number of Feynman's diagrams with k fermionic loops in the order n of the perturbative expansion in dimension zero for the GW approximation of the self-energy function in a many-body theory of fermions with two-body interaction (see Molinari link)."
			],
			"link": [
				"Gheorghe Coserea, \u003ca href=\"/A286784/b286784.txt\"\u003eRows n=0..122, flattened\u003c/a\u003e",
				"Luca G. Molinari, \u003ca href=\"https://arxiv.org/abs/cond-mat/0401500\"\u003eHedin's equations and enumeration of Feynman's diagrams\u003c/a\u003e, arXiv:cond-mat/0401500 [cond-mat.str-el], 2005."
			],
			"formula": [
				"y(x;t) = Sum_{n\u003e=0} P_n(t)*x^n satisfies y*(1-x*y)^2 = 1 + (t-1)*x*y, where P_n(t) = Sum_{k=0..n} T(n,k)*t^k.",
				"A000108(n) = T(n,0), A001791(n) = T(n,1), A002055(n+3) = T(n,2), A000290(n) = T(n,n-1), A006013(n) = P_n(1), A003169(n+1) = P_n(2).",
				"T(n,m) = C(2*n,n+m)*C(n+1,m)/(n+1). - _Vladimir Kruchinin_, Sep 23 2018"
			],
			"example": [
				"A(x;t) = 1 + (1 + t)*x + (2 + 4*t + t^2)*x^2 + (5 + 15*t + 9*t^2 + t^3)*x^3 + ...",
				"Triangle starts:",
				"n\\k  [0]   [1]    [2]     [3]     [4]     [5]    [6]    [7]   [8] [9]",
				"[0]  1;",
				"[1]  1,    1;",
				"[2]  2,    4,     1;",
				"[3]  5,    15,    9,      1;",
				"[4]  14,   56,    56,     16,     1;",
				"[5]  42,   210,   300,    150,    25,     1;",
				"[6]  132,  792,   1485,   1100,   330,    36,    1;",
				"[7]  429,  3003,  7007,   7007,   3185,   637,   49,    1;",
				"[8]  1430, 11440, 32032,  40768,  25480,  7840,  1120,  64,   1;",
				"[9]  4862, 43758, 143208, 222768, 179928, 77112, 17136, 1836, 81, 1;",
				"[10] ..."
			],
			"mathematica": [
				"Flatten@Table[Binomial[2 n, n + m] Binomial[n + 1, m] / (n + 1), {n, 0, 10}, {m, 0, n}] (* _Vincenzo Librandi_, Sep 23 2018 *)"
			],
			"program": [
				"(PARI)",
				"A286784_ser(N,t='t) = my(x='x+O('x^N)); serreverse(Ser(x*(1-x)^2/(1+(t-1)*x)))/x;",
				"concat(apply(p-\u003eVecrev(p), Vec(A286784_ser(12))))",
				"\\\\ test: y=A286784_ser(50); y*(1-x*y)^2 == 1 + ('t-1)*x*y",
				"(Maxima)",
				"T(n,m):=(binomial(2*n,n+m)*binomial(n+1,m))/(n+1); /* _Vladimir Kruchinin_, Sep 23 2018 */",
				"(MAGMA) /* As triangle */ [[(Binomial(2*n, n+m)*Binomial(n+1, m))/(n+1): m in [0..n]]: n in [0.. 15]]; // _Vincenzo Librandi_, Sep 23 2018"
			],
			"xref": [
				"Cf. A286781, A286782, A286783."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Gheorghe Coserea_, May 14 2017",
			"references": 7,
			"revision": 33,
			"time": "2018-09-23T04:33:21-04:00",
			"created": "2017-05-17T22:52:20-04:00"
		}
	]
}