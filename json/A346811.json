{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A346811",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 346811,
			"data": "0,0,0,0,3,0,0,9,15,0,0,18,45,45,0,0,30,90,135,105,0,0,45,150,270,315,210,0,0,63,225,450,630,630,378,0,0,84,315,675,1050,1260,1134,630,0,0,108,420,945,1575,2100,2268,1890,990,0",
			"name": "Square array read by antidiagonals upwards in which T(n, k) is the number of essentially different relations from the first proportional segment theorem for n lines, k parallel and n-k intersecting in a common point.",
			"comment": [
				"The theorem of proportional segments (aka: theorem of Thales, intercept theorem, theorem of similarity, Ger.: \"Strahlensatz\") considers an arrangement of 2 X 2 lines, the first two intersecting in a point, the other two parallel and except for that in general position. There are several versions of this theorem; this table is concerned with the version that relates (lengths of) segments on one of the intersecting lines to segments on the other intersecting line. See the links below for more information. Other versions give similar relations involving segments on the parallel lines; these are ignored and not included in this count.",
				"Using common notation one of the considered relations reads SA:AB=SA':A'B' (equality of ratios, a proportion). Using only algebra and no geometry (e.g., no additivity of length) one can derive essentially equivalent relations easily: SA*A'B'=SA'*AB, SA:SA'=AB:A'B', AB:SA=A'B':SA', etc. However in the basic setup there are two more essentially different relations that cannot be derived with algebra alone from any other single essentially different relation: SA:SB=SA':SB' and SB:AB=SA':A'B'. Furthermore, combining two of these relations via algebra alone we can derive the third, i.e., there is a \"relation of relations\" or syzygy, but this table does not consider such higher relations.",
				"Now assume we have a configuration of i X j lines, the first i lines intersecting in a common point and the other j lines mutually parallel and otherwise in general position. Then a generalization of the basic theorem relates two segments on one of the i intersecting lines to two corresponding segments on another of the i lines. T(n,k)=T(i+j,j) is the number of essentially different relations explained by this generalized theorem.",
				"The count can be made explicit by first choosing 2 distinct out of the i commonly intersecting lines, giving a factor of binomial(i,2), and independently counting pairs of segments on one of these lines. The common intersection point S and the j intersections with the parallel lines add up to j+1 marked points. A segment is specified by choosing 2 out of these j+1 points. From these binomial(j+1,2) segments we choose 2, leading to another factor of binomial(binomial(j+1,2),2) in the overall count T(i+j,i). This leads to the formula below."
			],
			"link": [
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Intercept_theorem\"\u003eIntercept theorem\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = binomial(n-k,2)*binomial(binomial(k+1,2),2) = (1/16)*(n-k-1)*(n-k)*(k-1)*k*(k+1)*(k+2)."
			],
			"example": [
				"   n\\k | 1    2    3     4     5     6      7      8      9     10 ...",
				"  --------------------------------------------------------------------",
				"   1   | 0,   0,   0,    0,    0,    0,     0,     0,     0,     0",
				"   2   | 0,   3,  15,   45,  105,  210,   378,   630,   990,  1485",
				"   3   | 0,   9,  45,  135,  315,  630,  1134,  1890,  2970,  4455",
				"   4   | 0,  18,  90,  270,  630, 1260,  2268,  3780,  5940,  8910",
				"   5   | 0,  30, 150,  450, 1050, 2100,  3780,  6300,  9900, 14850",
				"   6   | 0,  45, 225,  675, 1575, 3150,  5670,  9450, 14850, 22275",
				"   7   | 0,  63, 315,  945, 2205, 4410,  7938, 13230, 20790, 31185",
				"   8   | 0,  84, 420, 1260, 2940, 5880, 10584, 17640, 27720, 41580",
				"   9   | 0, 108, 540, 1620, 3780, 7560, 13608, 22680, 35640, 53460",
				"  10   | 0, 135, 675, 2025, 4725, 9450, 17010, 28350, 44550, 66825",
				"...",
				"T(7,4)=135. Denoting the common intersection point of the i=7-4=3 intersecting lines by S and the intersections of these 3 by the j=4 parallel lines by A,B,C,D, A',B',C',D', A'',B'',C'',D'' one of the counted relations is SA:SC=SA'':SC''; another is A'B':C'D'=A''B'':C''D''; and yet a third is SC:BD=SC'':B''D''."
			],
			"maple": [
				"T:=(n,k)-\u003ebinomial(n-k,2)*binomial(binomial(k+1,2),2);"
			],
			"xref": [
				"Row for i=2 intersecting lines are tritriangular numbers: A050534.",
				"Every row is a multiple of the sequence of tritriangular numbers: A050534.",
				"Column for j=2 parallel lines are triangular matchstick numbers: A045943.",
				"Every column is a multiple of the sequence of triangular numbers: A000217."
			],
			"keyword": "tabl,nonn",
			"offset": "2,5",
			"author": "_Thomas Preu_, Aug 05 2021",
			"references": 0,
			"revision": 30,
			"time": "2021-08-27T02:15:31-04:00",
			"created": "2021-08-27T02:15:31-04:00"
		}
	]
}