{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001025",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1025,
			"id": "M5021 N2164",
			"data": "1,16,256,4096,65536,1048576,16777216,268435456,4294967296,68719476736,1099511627776,17592186044416,281474976710656,4503599627370496,72057594037927936,1152921504606846976,18446744073709551616,295147905179352825856,4722366482869645213696,75557863725914323419136,1208925819614629174706176",
			"name": "Powers of 16: a(n) = 16^n.",
			"comment": [
				"Convolution-square (auto-convolution) of A098430. - _R. J. Mathar_, May 22 2009",
				"Subsequence of A161441: A160700(a(n)) = 1. - _Reinhard Zumkeller_, Jun 10 2009",
				"The compositions of n in which each natural number is colored by one of p different colors are called p-colored compositions of n. For n \u003e= 1, a(n) equals the number of 16-colored compositions of n such that no adjacent parts have the same color. - _Milan Janjic_, Nov 17 2011",
				"Right-hand side of the identity ( Sum_{k = 0..n} (2*k + 1)*binomial(2*n + 1, n - k) ) * ( Sum_{k = 0..n} (-1)^k/(2*k + 1)*binomial(2*n + 1, n - k) ) = 16^n. - _Peter Bala_, Feb 12 2019"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Muniru A Asiru, \u003ca href=\"/A001025/b001025.txt\"\u003eTable of n, a(n) for n = 0..820\u003c/a\u003e (terms n = 0..100 from T. D. Noe)",
				"P. J. Cameron, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL3/groups.html\"\u003eSequences realized by oligomorphic permutation groups\u003c/a\u003e, J. Integ. Seqs. Vol. 3 (2000), #00.1.5.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=280\"\u003eEncyclopedia of Combinatorial Structures 280\u003c/a\u003e",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992",
				"Y. Puri and T. Ward, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/index.html\"\u003eArithmetic and growth of periodic orbits\u003c/a\u003e, J. Integer Seqs., Vol. 4 (2001), #01.2.1.",
				"\u003ca href=\"/index/Rec#order_01\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (16)."
			],
			"formula": [
				"G.f.: 1/(1-16*x).",
				"E.g.f.: exp(16*x).",
				"From _Muniru A Asiru_, Nov 07 2018: (Start)",
				"a(n) = 16^n.",
				"a(0) = 1, a(n) = 16*a(n-1). (End)",
				"a(n) = 4^A005843(n) = 2^A008586(n) = A000302(n)^2 = A000079(n)*A001018(n). - _Muniru A Asiru_, Nov 10 2018"
			],
			"maple": [
				"A001025:=-1/(-1+16*z); # _Simon Plouffe_ in his 1992 dissertation"
			],
			"mathematica": [
				"Table[4^(2*n), {n,0,20}] (* _Vladimir Joseph Stephan Orlovsky_, Mar 01 2009 *)"
			],
			"program": [
				"(Sage) [lucas_number1(n,16,0) for n in range(1, 18)] # _Zerinvary Lajos_, Apr 29 2009",
				"(PARI) a(n)=1\u003c\u003c(4*n) \\\\ _Charles R Greathouse IV_, Feb 01 2012",
				"(Maxima) A001025(n):=16^n$",
				"makelist(A001025(n),n,0,30); /* _Martin Ettl_, Nov 05 2012 */",
				"(Haskell)",
				"a001025 = (16 ^)",
				"a001025_list = iterate (* 16) 1  -- _Reinhard Zumkeller_, Nov 07 2012",
				"(GAP) List([0..20],n-\u003e16^n); # _Muniru A Asiru_, Nov 07 2018",
				"(Python) print([16**n for n in range(20)]) # _Stefano Spezia_, Nov 10 2018"
			],
			"xref": [
				"Partial sums give A131865.",
				"Cf. A000079, A000302, A001018, A005843, A008586."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 59,
			"revision": 97,
			"time": "2021-03-22T05:49:32-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}