{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A010683",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 10683,
			"data": "1,2,7,28,121,550,2591,12536,61921,310954,1582791,8147796,42344121,221866446,1170747519,6216189936,33186295681,178034219986,959260792775,5188835909516,28167068630713,153395382655222",
			"name": "Let S(x,y) = number of lattice paths from (0,0) to (x,y) that use the step set { (0,1), (1,0), (2,0), (3,0), ....} and never pass below y = x. Sequence gives S(n-1,n) = number of 'Schröder' trees with n+1 leaves and root of degree 2.",
			"comment": [
				"a(n) is the number of compound propositions \"on the negative side\" that can be made from n simple propositions.",
				"Convolution of A001003 (the little Schröder numbers) with itself. - _Emeric Deutsch_, Dec 27 2003",
				"Number of dissections of a convex polygon with n+3 sides that have a triangle over a fixed side (the base) of the polygon. - _Emeric Deutsch_, Dec 27 2003",
				"a(n-1) = number of royal paths from (0,0) to (n,n), A006318, with exactly one diagonal step on the line y=x. - _David Callan_, Mar 14 2004",
				"Number of short bushes (i.e., ordered trees with no vertices of outdegree 1) with n+2 leaves and having root of degree 2. Example: a(2)=7 because, in addition to the five binary trees with 6 edges (they do have 4 leaves) we have (i) two edges rb, rc hanging from the root r with three edges hanging from vertex b and (ii) two edges rb, rc hanging from the root r with three edges hanging from vertex c. - _Emeric Deutsch_, Mar 16 2004",
				"The a(n) equal the Fi2 sums, see A180662, of Schröder triangle A033877. - _Johannes W. Meijer_, Mar 26 2012",
				"Row sums of A144944 and of A186826. - _Reinhard Zumkeller_, May 11 2013"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A010683/b010683.txt\"\u003eTable of n, a(n) for n=0..200\u003c/a\u003e",
				"A. Bacher, \u003ca href=\"http://arxiv.org/abs/1301.1365\"\u003eDirected and multi-directed animals on the square lattice with next nearest neighbor edges\u003c/a\u003e, arXiv preprint arXiv:1301.1365 [math.CO], 2013-2015. See R(t).",
				"D. Birmajer, J. B. Gil, and M. D. Weiner, \u003ca href=\"http://arxiv.org/abs/1503.05242\"\u003eColored partitions of a convex polygon by noncrossing diagonals\u003c/a\u003e, arXiv preprint arXiv:1503.05242 [math.CO], 2015.",
				"Kevin Brown, \u003ca href=\"http://www.mathpages.com/home/kmath397/kmath397.htm\"\u003eHipparchus on Compound Statements\u003c/a\u003e, 1994-2010.",
				"Shishuo Fu, Zhicong Lin, and Yaling Wang, \u003ca href=\"https://arxiv.org/abs/2009.04269\"\u003eRefined Wilf-equivalences by Comtet statistics\u003c/a\u003e, arXiv:2009.04269 [math.CO], 2020.",
				"Laurent Habsieger, Maxim Kazarian and Sergei Lando, \u003ca href=\"http://www.jstor.org/stable/3109806\"\u003eOn the second number of Plutarch\u003c/a\u003e, Am. Math. Monthly, Vol. 105, No. 5 (May, 1998), p. 446.",
				"H. Kwong, \u003ca href=\"http://www.fq.math.ca/Abstracts/48-4/kwong.pdf\"\u003eOn recurrences of Fahr and Ringel: An Alternate Approach\u003c/a\u003e, Fib. Quart., 48 (2010), 363-365; see p. 364.",
				"J. W. Meijer, \u003ca href=\"https://www.ucbcba.edu.bo/Publicaciones/revistas/actanova/documentos/v4n4/Ensayos_Meijer2010_PI_.3r.pdf\"\u003eFamous numbers on a chessboard\u003c/a\u003e, Acta Nova, Volume 4, No.4, December 2010. pp. 589-598.",
				"E. Pergola and R. A. Sulanke, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/PergolaSulanke/\"\u003eSchroeder Triangles, Paths and Parallelogram Polyominoes\u003c/a\u003e, J. Integer Sequences, 1 (1998), #98.1.7.",
				"D. G. Rogers and L. W. Shapiro, \u003ca href=\"http://dx.doi.org/10.1007/BFb0091826\"\u003eDeques, trees and lattice paths\u003c/a\u003e, in Combinatorial Mathematics VIII: Proceedings of the Eighth Australian Conference. Lecture Notes in Mathematics, Vol. 884 (Springer, Berlin, 1981), pp. 293-303. Math. Rev., 83g, 05038; Zentralblatt, 469(1982), 05005. See Figs. 7a and 8b.",
				"R. P. Stanley, \u003ca href=\"http://www-math.mit.edu/~rstan/papers/hip.pdf\"\u003eHipparchus, Plutarch, Schröder and Hough\u003c/a\u003e, Am. Math. Monthly, Vol. 104, No. 4, p. 344, 1997.",
				"\u003ca href=\"/index/Tra#trees\"\u003eIndex entries for sequences related to trees\u003c/a\u003e"
			],
			"formula": [
				"G.f.: ((1-t)^2-(1+t)*sqrt(1-6*t+t^2))/(8*t^2) = A(t)^2, with o.g.f. A(t) of A001003.",
				"a(n) = (2/n)*sum(binomial(n, k)*binomial(n+k+1, k-1), k=1..n) = 2*hypergeom([1-n, n+3], [2], -1), n\u003e=1. a(0)=1. - _Wolfdieter Lang_, Sep 12 2005.",
				"a(n) = ((2*n+1)*LegendreP(n+1,3) - (2*n+3)*LegendreP(n,3)) / (4*n*(n+2)) for n\u003e0. - _Mark van Hoeij_, Jul 02 2010",
				"From _Gary W. Adamson_, Jul 08 2011: (Start)",
				"Let M = the production matrix:",
				"  1, 2, 0, 0, 0, 0,...",
				"  1, 2, 1, 0, 0, 0,...",
				"  1, 2, 1, 2, 0, 0,...",
				"  1, 2, 1, 2, 1, 0,...",
				"  1, 2, 1, 2, 1, 2,...",
				"  ...",
				"a(n) is the upper entry in the vector (M(T))^n * [1,0,0,0,...]; where T is the transpose operation. (End)",
				"D-finite with recurrence: (n+2)*(2*n-1)*a(n) = 6*(2*n^2-1)*a(n-1)-(n-2)*(2*n+1)*a(n-2). - _Vaclav Kotesovec_, Oct 07 2012",
				"a(n) ~ sqrt(48+34*sqrt(2))*(3+2*sqrt(2))^n/(4*sqrt(Pi)*n^(3/2)). - _Vaclav Kotesovec_, Oct 07 2012",
				"Recurrence (an alternative): (n+2)*a(n)  = (4-n)*a(n-4) + 2*(2*n-5)*a(n-3) + 10*(n-1)*a(n-2) + 2*(2*n+1)*a(n-1), n\u003e=4. - _Fung Lam_, Feb 18 2014",
				"a(n) = (n+1)*hypergeom([1-n, -n], [3], 2). - _Peter Luschny_, Nov 19 2014",
				"a(n) = (A001003(n) + A001003(n+1))/2 = sum(A001003(k) * A001003(n-k), k=0..n). - _Johannes W. Meijer_, Apr 29 2015"
			],
			"maple": [
				"a := proc(n) local k: if n=0 then 1 else (2/n)*add(binomial(n, k)* binomial(n+k+1, k-1), k=1..n) fi: end:",
				"seq(a(n), n=0..21); # _Johannes W. Meijer_, Mar 26 2012, revised Mar 31 2015"
			],
			"mathematica": [
				"f[ x_, y_ ] := f[ x, y ] = Module[ {return}, If[ x == 0, return = 1, If[ y == x-1, return = 0, return = f[ x, y-1 ] + Sum[ f[ k, y ], {k, 0, x-1} ] ] ]; return ];",
				"(* Do[Print[Table[f[ k, j ], {k, 0, j}]], {j, 10, 0, -1}] *)",
				"Table[f[x, x + 1], {x, 0, 21}]",
				"(* Second program: *)",
				"a[n_] := 2*Hypergeometric2F1[1-n, n+3, 2, -1]; a[0]=1;",
				"Table[a[n], {n, 0, 21}] (* _Jean-François Alcover_, Dec 09 2014, after _Wolfdieter Lang_ *)"
			],
			"program": [
				"(Haskell)",
				"a010683 = sum . a144944_row  -- _Reinhard Zumkeller_, May 11 2013",
				"(Sage)",
				"a = lambda n: (n+1)*hypergeometric([1-n, -n], [3], 2)",
				"[simplify(a(n)) for n in range(22)] # _Peter Luschny_, Nov 19 2014",
				"(PARI) x='x+O('x^100); Vec(((1-x)^2-(1+x)*sqrt(1-6*x+x^2))/(8*x^2)) \\\\ _Altug Alkan_, Dec 19 2015"
			],
			"xref": [
				"Cf. A001003.",
				"Second right-hand column of triangle A011117.",
				"A177010 has a closely-related g.f."
			],
			"keyword": "nonn,nice,easy",
			"offset": "0,2",
			"author": "Robert Sulanke (sulanke(AT)diamond.idbsu.edu), _N. J. A. Sloane_",
			"ext": [
				"Minor edits by _Johannes W. Meijer_, Mar 26 2012"
			],
			"references": 13,
			"revision": 97,
			"time": "2020-12-10T12:56:39-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}