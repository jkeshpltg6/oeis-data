{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A048996",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 48996,
			"data": "1,1,1,1,1,2,1,1,2,1,3,1,1,2,2,3,3,4,1,1,2,2,1,3,6,1,4,6,5,1,1,2,2,2,3,6,3,3,4,12,4,5,10,6,1,1,2,2,2,1,3,6,6,3,3,4,12,6,12,1,5,20,10,6,15,7,1,1,2,2,2,2,3,6,6,3,3,6,1,4,12,12,12,12,4,5,20,10,30,5,6,30,20,7,21,8,1",
			"name": "Irregular triangle read by rows. Preferred multisets: numbers refining A007318 using format described in A036038.",
			"comment": [
				"This array gives in row n\u003e=1 the multinomial numbers (call them M_0 numbers) m!/product((a_j)!,j=1..n) with the exponents of the partitions of n with number of parts m:=sum(a_j,j=1..n), given in the Abramowitz-Stegun order. See p. 831 of the given reference. See also the arrays for the M_1, M_2 and M_3 multinomial numbers A036038, A036039 and A036040 (or A080575).",
				"For a signed version see A111786.",
				"These M_0 multinomial numbers give the number of compositions of n \u003e= 1 with parts corresponding to the partitions of n (in A-St order). See an n = 5 example below. The triangle with the summed entries of like number of parts m is A007318(n-1, m-1) (Pascal). - _Wolfdieter Lang_, Jan 29 2021"
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A048996/b048996.txt\"\u003eTable of n, a(n) for n = 0..2713\u003c/a\u003e (rows 0..20)",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards Applied Math. Series 55, 1972.",
				"Wolfdieter Lang, \u003ca href=\"/A048996/a048996_2.pdf\"\u003eFirst 10 rows and more.\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = A036040(n,k) * Factorial(A036043(n,k)) / A036038(n,k) = A049019(n,k) / A036038(n,k).",
				"If the n-th partition is P, a(n) is the multinomial coefficient of the signature of P. - _Franklin T. Adams-Watters_, May 30 2006",
				"T(n,k) = A309004(A036035(n,k)). - _Andrew Howroyd_, Oct 19 2020"
			],
			"example": [
				"Table starts:",
				"[1]",
				"[1]",
				"[1, 1]",
				"[1, 2, 1]",
				"[1, 2, 1, 3, 1]",
				"[1, 2, 2, 3, 3, 4, 1]",
				"[1, 2, 2, 1, 3, 6, 1, 4, 6,  5, 1]",
				"[1, 2, 2, 2, 3, 6, 3, 3, 4, 12, 4, 5, 10, 6, 1]",
				".",
				"T(5,6) = 4 because there are four multisets using the first four digits {0,1,2,3}: 32100, 32110, 32210 and 33210",
				"T(5,6) = 4 because there are 4 compositions of 5 that can be formed from the partition 2+1+1+1. - _Geoffrey Critzer_, May 19 2013",
				"These 4 compositions 2+1+1+1, 1+2+1+1, 1+1+2+1 and 1+1+1+2 of 5 correspond to the 4 set partitions of [5] :={1,2,3,4,5}, with 4 blocks of consecutive numbers, namely {1,2},{3},{4},{5} and {1},{2,3},{4},{5} and {1},{2},{3,4},{5} and {1},{2},{3},{4,5}. - _Wolfdieter Lang_, May 30 2018"
			],
			"maple": [
				"nmax:=9: with(combinat): for n from 1 to nmax do P(n):=sort(partition(n)): for r from 1 to numbpart(n) do B(r):=P(n)[r] od: for m from 1 to numbpart(n) do s:=0: j:=0: while s\u003cn do j:=j+1: s:=s+B(m)[j]: x(j):=B(m)[j]: end do; jmax:=j; for r from 1 to n do q(r):=0 od: for r from 1 to n do for j from 1 to jmax do if x(j)=r then q(r):=q(r)+1 fi: od: od: A036040(n, m) := (add(q(t), t=1..n))!/(mul(q(t)!, t=1..n)); od: od: seq(seq(A036040(n, m), m=1..numbpart(n)), n=1..nmax); # _Johannes W. Meijer_, Jul 14 2016"
			],
			"program": [
				"(SageMath) from collections import Counter",
				"def A048996_row(n):",
				"    h = lambda p: product(map(factorial, Counter(p).values()))",
				"    return [factorial(len(p))//h(p) for k in (0..n) for p in Partitions(n, length=k)]",
				"for n in (1..10): print(A048996_row(n)) # _Peter Luschny_, Nov 02 2019",
				"(PARI)",
				"C(sig)={my(S=Set(sig)); (#sig)!/prod(k=1, #S, (#select(t-\u003et==S[k], sig))!)}",
				"Row(n)={apply(C, [Vecrev(p) | p\u003c-partitions(n)])}",
				"{ for(n=0, 7, print(Row(n))) } \\\\ _Andrew Howroyd_, Oct 18 2020"
			],
			"xref": [
				"Cf. A000670, A007318, A036035, A036038, A019538, A115621, A309004, A000079 (row sums), A000041 (row lengths)."
			],
			"keyword": "nonn,tabf",
			"offset": "0,6",
			"author": "_Alford Arnold_",
			"ext": [
				"More terms from Antonio G. Astudillo (afg_astudillo(AT)hotmail.com), Jun 17 2001",
				"a(0)=1 prepended by _Andrew Howroyd_, Oct 19 2020"
			],
			"references": 37,
			"revision": 69,
			"time": "2021-02-03T23:21:34-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}