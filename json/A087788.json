{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A087788",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 87788,
			"data": "561,1105,1729,2465,2821,6601,8911,10585,15841,29341,46657,52633,115921,162401,252601,294409,314821,334153,399001,410041,488881,512461,530881,1024651,1152271,1193221,1461241,1615681,1857241,1909001,2508013",
			"name": "3-Carmichael numbers: Carmichael numbers equal to the product of 3 primes: n=pqr, where p\u003cq\u003cr are primes such that a^{n-1} == 1 (mod n) if a is prime to n.",
			"comment": [
				"It is interesting that most of the numbers have the last digit 1. For example 530881, 3581761, 7207201, etc.",
				"Granville \u0026 Pomerance conjecture that there are ~ c x^{1/3}/(log x)^3 members of this sequence up to x. Heath-Brown proves that, for any e \u003e 0, there are O(x^{7/20 + e}) members of this sequence up to x. - _Charles R Greathouse IV_, Nov 19 2012"
			],
			"reference": [
				"O. Ore, Number Theory and Its History, McGraw-Hill, 1948, Reprinted by Dover Publications, 1988, Chapter 14."
			],
			"link": [
				"R. J. Mathar and Charles R Greathouse IV, \u003ca href=\"/A087788/b087788.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (first 3284 terms from Mathar)",
				"F. Arnault, \u003ca href=\"http://dx.doi.org/10.1006/jsco.1995.1042\"\u003eConstructing Carmichael numbers which are strong pseudoprimes to several bases\u003c/a\u003e, Journal of Symbolic Computation, vol. 20, no 2, Aug. 1995, pp. 151-161.",
				"Harvey Dubner, Journal of Integer Sequences, Vol. 5 (2002) Article 02.2.1, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL5/Dubner/dubner6.html\"\u003eCarmichael Numbers of the form (6m+1)(12m+1)(18m+1).\u003c/a\u003e",
				"A. Granville and C. Pomerance, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-01-01355-2\"\u003eTwo contradictory conjectures concerning Carmichael numbers\u003c/a\u003e, Math. Comp. 71 (2002), pp. 883-90.",
				"D. R. Heath-Brown, \u003ca href=\"http://eprints.maths.ox.ac.uk/680/1/carmichael.pdf\"\u003eCarmichael numbers with three prime factors\u003c/a\u003e, Hardy-Ramanujan Journal 30 (2007), pp. 6-12.",
				"G. Jaeschke, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1990-1023763-5\"\u003eThe Carmichael numbers to 10^12\u003c/a\u003e, Math. Comp., 55 (1990), 383-389.",
				"Math Reference Project, \u003ca href=\"http://www.mathreference.com/num-mod,ccm.html\"\u003eCarmichael Numbers\u003c/a\u003e",
				"R. G. E. Pinch, \u003ca href=\"ftp://ftp.dpmms.cam.ac.uk/pub/Carmichael/\"\u003eCarmichael numbers up to 10^16 (FTP)\u003c/a\u003e",
				"Rosetta Code, \u003ca href=\"http://rosettacode.org/wiki/Carmichael_3_strong_pseudoprimes\"\u003ePrograms for finding 3-Carmichael numbers\u003c/a\u003e"
			],
			"formula": [
				"n is composite and squarefree and for p prime, p|n =\u003e p-1|n-1. A composite odd number n is a Carmichael number if and only if n is squarefree and p-1 divides n-1 for every prime p dividing n (Korselt, 1899) n=pqr, p-1|n-1, q-1|n-1, r-1|n-1."
			],
			"example": [
				"a(6)=6601=7*23*41: 7-1|6601-1, 23-1|6601-1, 41-1|6601-1, i.e. 6|6600, 22|6600, 40|6600."
			],
			"program": [
				"(PARI) list(lim)=my(v=List());forprime(p=3,(lim)^(1/3), forprime(q=p+1, sqrt(lim\\p),forprime(r=q+1,lim\\(p*q),if((q*r-1)%(p-1)||(p*r-1)%(q-1)||(p*q-1)%(r-1),,listput(v,p*q*r)))));vecsort(Vec(v)) \\\\ _Charles R Greathouse IV_, Nov 19 2012"
			],
			"xref": [
				"Cf. A002997, A162290."
			],
			"keyword": "easy,nonn",
			"offset": "1,1",
			"author": "_Miklos Kristof_, Oct 07 2003",
			"ext": [
				"Minor edit to definition by _N. J. A. Sloane_, Sep 14 2009"
			],
			"references": 67,
			"revision": 24,
			"time": "2014-06-22T16:27:56-04:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}