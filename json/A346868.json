{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A346868",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 346868,
			"data": "4,6,8,18,12,14,24,18,20,32,36,24,42,40,30,32,48,54,38,60,56,42,44,84,72,48,72,98,54,72,80,90,60,62,96,84,68,126,96,72,74,114,124,140,168,80,126,84,108,132,120,90,168,128,144,120,98,102,216,104,192,162,108,110",
			"name": "Sum of divisors of the numbers with no middle divisors.",
			"comment": [
				"The characteristic shape of the symmetric representation of a(n) consists in that in the main diagonal of the diagram the width is equal to zero.",
				"So knowing this characteristic shape we can know if a number has middle divisors  (or not) just by looking at the diagram, even ignoring the concept of middle divisors.",
				"Therefore we can see a geometric pattern of the distribution of the numbers with no middle divisors in the stepped pyramid described in A245092.",
				"For the definition of \"width\" see A249351.",
				"All terms are even numbers."
			],
			"formula": [
				"a(n) = A000203(A071561(n))."
			],
			"example": [
				"a(4) = 18 because the sum of divisors of the fourth number with no middle divisors (i.e., 10) is 1 + 2 + 5 + 10 = 18.",
				"On the other hand we can see that in the main diagonal of every diagram the width is equal to zero as shown below.",
				"Illustration of initial terms:",
				"m(n) = A071561(n).",
				".",
				"   n   m(n) a(n)   Diagram",
				".                      _   _   _     _ _   _ _     _   _   _ _ _     _",
				"                      | | | | | |   | | | | | |   | | | | | | | |   | |",
				"                   _ _|_| | | | |   | | | | | |   | | | | | | | |   | |",
				"   1    3    4    |_ _|  _|_| | |   | | | | | |   | | | | | | | |   | |",
				"                   _ _ _|    _|_|   | | | | | |   | | | | | | | |   | |",
				"   2    5    6    |_ _ _|  _|    _ _| | | | | |   | | | | | | | |   | |",
				"                   _ _ _ _|     |  _ _|_| | | |   | | | | | | | |   | |",
				"   3    7    8    |_ _ _ _|  _ _|_|    _ _|_| |   | | | | | | | |   | |",
				"                            |  _|     |  _ _ _|   | | | | | | | |   | |",
				"                   _ _ _ _ _| |      _|_|    _ _ _|_| | | | | | |   | |",
				"   4   10   18    |_ _ _ _ _ _|  _ _|       |    _ _ _|_| | | | |   | |",
				"   5   11   12    |_ _ _ _ _ _| |  _|      _|   |  _ _ _ _|_| | |   | |",
				"                   _ _ _ _ _ _ _| |      _|  _ _| | |  _ _ _ _|_|   | |",
				"   6   13   14    |_ _ _ _ _ _ _| |  _ _|  _|    _| | |    _ _ _ _ _| |",
				"   7   14   24    |_ _ _ _ _ _ _ _| |     |     |  _|_|   |  _ _ _ _ _|",
				"                                    |  _ _|  _ _|_|       | |",
				"                   _ _ _ _ _ _ _ _ _| |  _ _|  _|        _|_|",
				"   8   17   18    |_ _ _ _ _ _ _ _ _| | |_ _ _|         |",
				"                   _ _ _ _ _ _ _ _ _ _| |  _ _|        _|",
				"   9   19   20    |_ _ _ _ _ _ _ _ _ _| | |        _ _|",
				"                   _ _ _ _ _ _ _ _ _ _ _| |  _ _ _|",
				"  10   21   32    |_ _ _ _ _ _ _ _ _ _ _| | |  _ _|",
				"  11   22   36    |_ _ _ _ _ _ _ _ _ _ _ _| | |",
				"  12   23   24    |_ _ _ _ _ _ _ _ _ _ _ _| | |",
				"                                            | |",
				"                   _ _ _ _ _ _ _ _ _ _ _ _ _| |",
				"  13   26   42    |_ _ _ _ _ _ _ _ _ _ _ _ _ _|",
				"."
			],
			"mathematica": [
				"s[n_] := Module[{d = Divisors[n]}, If[AnyTrue[d, Sqrt[n/2] \u003c= # \u003c Sqrt[n*2] \u0026], 0, Plus @@ d]]; Select[Array[s, 110], # \u003e 0 \u0026] (* _Amiram Eldar_, Aug 19 2021 *)"
			],
			"program": [
				"(PARI) is(n) = fordiv(n, d, if(sqrt(n/2) \u003c= d \u0026\u0026 d \u003c sqrt(2*n), return(0))); 1; \\\\ A071561 apply(sigma, select(is, [1..150])) \\\\ _Michel Marcus_, Aug 19 2021"
			],
			"xref": [
				"Cf. A000203, A067742, A071090, A071561, A071562, A237591, A237593, A245092, A249351, A262626, A281007, A299777, A346864.",
				"Some sequences that gives sum of divisors: A000225 (of powers of 2), A008864 (of prime numbers), A065764 (of squares), A073255 (of composites), A074285 (of triangular numbers, also of generalized hexagonal numbers), A139256 (of perfect numbers), A175926 (of cubes), A224613 (of multiples of 6), A346865 (of hexagonal numbers), A346866 (of second hexagonal numbers), A346867 (of numbers with middle divisors)."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Omar E. Pol_, Aug 18 2021",
			"references": 8,
			"revision": 32,
			"time": "2021-08-20T00:22:52-04:00",
			"created": "2021-08-20T00:22:52-04:00"
		}
	]
}