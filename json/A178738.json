{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A178738",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 178738,
			"data": "1,-1,-1,1,2,-3,-5,9,15,-27,-49,89,164,-304,-565,1057,1987,-3745,-7085,13445,25575,-48771,-93210,178481,342392,-657935,-1266205,2440323,4709403,-9099507,-17602325,34087058,66076421,-128207979,-248983641",
			"name": "Moebius inversion of a sequence related to powers of 2.",
			"comment": [
				"Only odd indices make sense. The given sequence is a(1), a(3), a(5), etc.",
				"This should be related to the Coxeter transformations for the posets of diagonally symmetric paths in an n*n grid. - _F. Chapoton_, Jun 11 2010",
				"Start from 1, 1, -2, -2, -4, -4, 8, 8, 16, 16, -32, -32, -64, -64, 128, ... which is A016116(n-1) with negative signs in blocks of 4, assuming offset 1. The Mobius transform of that sequence is b(n) = 1, 0, -3, -3, -5, -2, 7, 10, 18, 20, -33, -25, -65, -72, 135, 120, ... for n \u003e= 1, and the current sequence is a(n) = b(2n-1)/(2n-1). - _R. J. Mathar_, Oct 29 2011"
			],
			"example": [
				"b(1)=1*1; b(3)=-1*3; ...; b(9)=2*9."
			],
			"maple": [
				"A := proc(n)",
				"        (-1)^binomial(floor((n+1)/2),2) * 2^floor((n-1)/2) ;",
				"end proc:",
				"L := [seq(A(n),n=1..40)] ;",
				"b := MOBIUS(L) ;",
				"for i from 1 to nops(b) by 2 do",
				"        printf(\"%d,\", op(i,b)/i) ;",
				"end do: # _R. J. Mathar_, Oct 29 2011"
			],
			"mathematica": [
				"b[n_] := Sum[(-1)^Binomial[(d+1)/2, 2]*2^((d-1)/2)*MoebiusMu[n/d], {d, Divisors[n]}]/n;",
				"a[n_] := b[2n - 1];",
				"a /@ Range[35] (* _Jean-François Alcover_, Mar 16 2020 *)"
			],
			"program": [
				"(Sage)",
				"def suite(n):",
				"    return sum((-1)**binomial(((d+1)//2), 2) * 2**((d-1)//2) * moebius(n//d) for d in divisors(n)) // n",
				"[suite(n) for n in range(1,22,2)]"
			],
			"xref": [
				"Similar to A022553 and A131868",
				"Also related to A178749. - _F. Chapoton_, Jun 11 2010"
			],
			"keyword": "sign,uned",
			"offset": "1,5",
			"author": "_F. Chapoton_, Jun 08 2010",
			"ext": [
				"I would like a more precise definition. - _N. J. A. Sloane_, Jun 08 2010"
			],
			"references": 3,
			"revision": 18,
			"time": "2020-03-17T19:37:48-04:00",
			"created": "2010-07-11T03:00:00-04:00"
		}
	]
}