{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A226452",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 226452,
			"data": "1,2,2,4,6,12,20,36,62,116,204,364,664,1220,2240,4132,7646,14244,26644,49984,94132,177788,336756,639720,1218228,2325048,4446776,8520928,16356260,31447436,60552616,116753948,225404486,435677408,843029104,1632918624,3165936640",
			"name": "Number of closed binary words of length n.",
			"comment": [
				"A word is closed if it contains a proper factor that occurs both as a prefix and as a suffix but does not have internal occurrences.",
				"a(n+1) \u003c= 2*a(n); for n \u003e 1: a(n) \u003c= A094536(n). - _Reinhard Zumkeller_, Jun 15 2013"
			],
			"link": [
				"Lars Blomberg, \u003ca href=\"/A226452/b226452.txt\"\u003eTable of n, a(n) for n = 0..38\u003c/a\u003e",
				"G. Badkobeh, G. Fici, Z. Lipták, \u003ca href=\"http://dx.doi.org/10.1007/978-3-319-15579-1_29\"\u003eOn the number of closed factors in a word\u003c/a\u003e, in A.-H. Dediu et al., eds., LATA 2015, LNCS 8977, 2015, pp. 381-390. Available at \u003ca href=\"http://arxiv.org/abs/1305.6395\"\u003earXiv\u003c/a\u003e, arXiv:1305.6395 [cs.FL], 2013-2014.",
				"Gabriele Fici, \u003ca href=\"http://bulletin.eatcs.org/index.php/beatcs/article/viewFile/508/497\"\u003eOpen and Closed Words\u003c/a\u003e, in Giovanni Pighizzini, ed., The Formal Language Theory Column, Bulletin of EATCS, 2017."
			],
			"example": [
				"a(4) = 6 because the only closed binary words of length 4 are 0000, 0101, 0110, and their complements."
			],
			"program": [
				"(Haskell)",
				"import Data.List (inits, tails, isInfixOf)",
				"a226452 n = a226452_list !! n",
				"a226452_list = 1 : 2 : f [[0,0],[0,1],[1,0],[1,1]] where",
				"   f bss = sum (map h bss) : f ((map (0 :) bss) ++ (map (1 :) bss)) where",
				"   h bs = fromEnum $ or $ zipWith",
				"           (\\xs ys -\u003e xs == ys \u0026\u0026 not (xs `isInfixOf` (init $ tail bs)))",
				"           (init $ inits bs) (reverse $ tails $ tail bs)",
				"-- _Reinhard Zumkeller_, Jun 15 2013"
			],
			"xref": [
				"Cf. A297183, A297184, A297185."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Jeffrey Shallit_, Jun 07 2013",
			"ext": [
				"a(17)-a(23) from _Reinhard Zumkeller_, Jun 15 2013",
				"a(24)-a(36) from _Lars Blomberg_, Dec 28 2015"
			],
			"references": 5,
			"revision": 41,
			"time": "2019-09-18T04:58:33-04:00",
			"created": "2013-06-13T15:11:00-04:00"
		}
	]
}