{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A091891",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 91891,
			"data": "1,1,2,1,4,1,2,1,10,3,2,1,5,1,2,1,36,6,12,1,11,3,2,1,24,3,3,1,5,1,2,1,202,67,55,9,93,4,5,1,112,8,13,1,10,3,2,1,304,22,18,1,20,3,3,1,34,3,3,1,5,1,2,1,1828,1267,1456,71,1629,77,100,2,2342,99,123,9,132,4,3,1",
			"name": "Number of partitions of n into parts which are a sum of exactly as many distinct powers of 2 as n has 1's in its binary representation.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A091891/b091891.txt\"\u003eTable of n, a(n) for n = 0..16384\u003c/a\u003e (terms n = 1..1000 from Andrew Howroyd)"
			],
			"formula": [
				"a(A000079(n)) = A018819(n);",
				"a(A018900(n)) = A091889(n);",
				"a(A014311(n)) = A091890(n);",
				"a(A091892(n)) = 1."
			],
			"example": [
				"a(9) = 3 because there are 3 partitions of 9 into parts of size 3, 5, 6, 9 which are the numbers that have two 1's in their binary representations. The 3 partitions are: 9, 6 + 3 and 3 + 3 + 3. - _Andrew Howroyd_, Apr 20 2021"
			],
			"maple": [
				"H:= proc(n) option remember; add(i, i=Bits[Split](n)) end:",
				"v:= proc(n, k) option remember; `if`(n\u003c1, 0,",
				"      `if`(H(n)=k, n, v(n-1, k)))",
				"    end:",
				"b:= proc(n, i, k) option remember; `if`(n=0, 1, `if`(i\u003c1, 0,",
				"      b(n, v(i-1, k), k)+b(n-i, v(min(n-i, i), k), k)))",
				"    end:",
				"a:= n-\u003e b(n$2, H(n)):",
				"seq(a(n), n=0..80);  # _Alois P. Heinz_, Dec 12 2021"
			],
			"mathematica": [
				"etr[p_] := Module[{b}, b[n_] := b[n] = If[n == 0, 1, Sum[Sum[d*p[d], {d, Divisors[j]}] b[n - j], {j, 1, n}]/n]; b];",
				"EulerT[v_List] := With[{q = etr[v[[#]]\u0026]}, q /@ Range[Length[v]]];",
				"a[n_] := EulerT[Table[DigitCount[k, 2, 1] == DigitCount[n, 2, 1] // Boole, {k, 1, n}]][[n]];",
				"Array[a, 100] (* _Jean-François Alcover_, Dec 12 2021, after _Andrew Howroyd_ *)"
			],
			"program": [
				"(PARI) EulerT(v)={Vec(exp(x*Ser(dirmul(v,vector(#v,n,1/n))))-1, -#v)}",
				"a(n) = {EulerT(vector(n,k,hammingweight(k)==hammingweight(n)))[n]} \\\\ _Andrew Howroyd_, Apr 20 2021"
			],
			"xref": [
				"Cf. A000079, A000120, A000041, A018819, A018900, A014311.",
				"Cf. A091889, A091890, A091892, A091893."
			],
			"keyword": "nonn,look",
			"offset": "0,3",
			"author": "_Reinhard Zumkeller_, Feb 10 2004",
			"ext": [
				"a(0)=1 prepended by _Alois P. Heinz_, Dec 12 2021"
			],
			"references": 5,
			"revision": 22,
			"time": "2021-12-12T18:04:49-05:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}