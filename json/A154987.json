{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A154987",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 154987,
			"data": "-2,4,4,13,20,13,41,69,69,41,183,268,264,268,183,1099,1405,1080,1080,1405,1099,7943,9486,5970,4080,5970,9486,7943,65547,75775,43806,20370,20370,43806,75775,65547,604831,685672,384552,149520,77280,149520,384552,685672,604831",
			"name": "Triangle read by rows: T(n,k) = t(n,k) + t(n,n-k), where t(n,k) = 2*(n!/k!)*(2*(n + k) - 1).",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A154987/b154987.txt\"\u003eRows n = 0..100 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = t(n,k) + t(n,n-k), where t(n,k) = 2*n!*Gamma(n + k + 1/2)/(k!*Gamma(n + k - 1/2)).",
				"T(n,k) = t(n,k) + t(n,n-k), where t(n,k) = 2*(n+k-1/2)*(n!/k!). - _Yu-Sheng Chang_, Apr 13 2020",
				"From _G. C. Greubel_, May 28 2020: (Start)",
				"T(n,k) = binomial(n,k)*( (2*n+2*k-1)*(n-k)! + (4*n-2*k-1)*k! ).",
				"T(n,n-k) = T(n,k), for k \u003e= 0.",
				"Sum_{k=0..n} T(n,k) = 2*( 2*n-1 + (2*n+1)*n!*e_{n-1}(1) ), where e_{n}(x) is the finite exponential function = Sum_{k=0..n} x^k/k!.",
				"Sum_{k=0..n} T(n,k) = 2*( 2*n-1 + (2*n+1)*A007526(n) ).",
				"T(n,0) = A175925(n-1) + 2*n.",
				"T(n,1) = A007680(n) + A001107(n). (End)"
			],
			"example": [
				"     -2;",
				"      4,     4;",
				"     13,    20,    13;",
				"     41,    69,    69,    41;",
				"    183,   268,   264,   268,   183;",
				"   1099,  1405,  1080,  1080,  1405,  1099;",
				"   7943,  9486,  5970,  4080,  5970,  9486,  7943;",
				"  65547, 75775, 43806, 20370, 20370, 43806, 75775, 65547;",
				"  ..."
			],
			"maple": [
				"t:= proc(n,k) option remember; ## simplified t;",
				"2*(n+k-1/2)*(n!/k!);",
				"end proc:",
				"A154987:= proc(n,k) ## n \u003e= 0 and k = 0 .. n",
				"t(n,k) + t(n,n-k)",
				"end proc: # _Yu-Sheng Chang_, Apr 13 2020"
			],
			"mathematica": [
				"(* First program *)",
				"t[n_, k_]:= 2*n!*Gamma[n+k+1/2]/(k!*Gamma[n+k-1/2]);",
				"T[n_, k_]:= t[n, k] + t[n,n-k];",
				"Table[T[n,k], {n,0,10}, {k,0,n}]//Flatten",
				"(* Second Program *)",
				"T[n_, k_]:= Binomial[n, k]*((n-k)!*(2*n+2*k-1) + k!*(4*n-2*k-1));",
				"Table[T[n, k], {n,0,10}, {k,0,n}]//Flatten (* _G. C. Greubel_, May 28 2020 *)"
			],
			"program": [
				"(Sage)",
				"def T(n, k): return binomial(n, k)*(factorial(n-k)*(2*n+2*k-1) + factorial(k)*(4*n-2*k-1))",
				"[[T(n, k) for k in (0..n)] for n in (0..10)] # _G. C. Greubel_, May 28 2020"
			],
			"keyword": "sign,tabl",
			"offset": "0,1",
			"author": "_Roger L. Bagula_, Jan 18 2009",
			"ext": [
				"Partially edited by _Andrew Howroyd_, Mar 26 2020",
				"Additionally edited by _G. C. Greubel_, May 28 2020"
			],
			"references": 1,
			"revision": 19,
			"time": "2020-05-28T03:04:40-04:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}