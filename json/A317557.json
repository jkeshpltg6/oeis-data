{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A317557",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 317557,
			"data": "0,-1,3,6,9,13,14,17,19,20,23,20,25,20,33,37,35,38,41,43,45,43,47,48,52,54,58,61,68,70,74,77,78,81,86,89,92,93,92,99,105,109,113,116,118,121,127,133,136,135,139,141,145,149,154,159,161,165,171,173,172,180",
			"name": "Number of binary digits to which the n-th convergent of the continued fraction expansion of log(2) matches the correct value.",
			"comment": [
				"Binary expansion of log(2) in A068426.",
				"For number of correct decimal digits see A317558.",
				"For the similar case of number of correct binary digits of Pi see A305879.",
				"The denominator of the k-th convergent obtained from a continued fraction satisfying the Gauss-Kuzmin distribution will tend to exp(k*A100199), A100199 being the inverse of Lévy's constant; the error between the k-th convergent and the constant itself tends to exp(-2*k*A100199), or in binary digits 2*k*A100199/log(2) bits after the binary point.",
				"The sequence for quaternary digits is obtained by floor(a(n)/2), the sequence for octal digits is obtained by floor(a(n)/3), and the sequence for hexadecimal digits is obtained by floor(a(n)/4)."
			],
			"link": [
				"A.H.M. Smeets, \u003ca href=\"/A317557/b317557.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e"
			],
			"formula": [
				"Lim_{n -\u003e oo} a(n)/n = 2*log(A086702)/log(2) = 2*A100199/log(2) = 2*A305607."
			],
			"example": [
				"   n   convergent         binary expansion         a(n)",
				"  ==  ============  =============================  ====",
				"   1     0 / 1      0.0                              0",
				"   2     1 / 1      1.0                             -1",
				"   3     2 / 3      0.1010...                        3",
				"   4     7 / 10     0.1011001...                     6",
				"   5     9 / 13     0.1011000100...                  9",
				"   6    61 / 88     0.10110001011101...             13",
				"   7   192 / 277    0.101100010111000...            14",
				"   8   253 / 365    0.101100010111001001...         17",
				"   9   445 / 642    0.10110001011100100000...       19",
				"  10  1143 / 1649   0.101100010111001000011...      20",
				"  oo  lim = log(2)  0.101100010111001000010111...   --"
			],
			"mathematica": [
				"a[n_] := Block[{k = 1, a = RealDigits[ Log@2, 2, 4 + 10][[1]], b = RealDigits[ FromContinuedFraction@ ContinuedFraction[Log@2, n + 1], 2, 4n + 10][[1]]}, While[ a[[k]] == b[[k]], k++]; k - 1]; a[1] = 0; a[2] = -1; Array[a, 61] (* _Robert G. Wilson v_, Aug 09 2018 *)"
			],
			"xref": [
				"Cf. A016730, A068426, A086702, A100199, A305607, A317558."
			],
			"keyword": "sign,base",
			"offset": "1,3",
			"author": "_A.H.M. Smeets_, Jul 31 2018",
			"ext": [
				"a(40) onward from _Robert G. Wilson v_, Aug 09 2018"
			],
			"references": 3,
			"revision": 29,
			"time": "2019-11-21T00:11:24-05:00",
			"created": "2018-08-11T20:55:54-04:00"
		}
	]
}