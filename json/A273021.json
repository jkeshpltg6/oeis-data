{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A273021",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 273021,
			"data": "1,1,2,2,2,3,2,1,2,2,1,3,3,4,2,2,3,5,2,2,4,1,1,3,3,4,7,4,4,1,1,1,4,4,2,4,4,6,5,2,5,7,3,3,3,4,1,3,5,4,5,6,2,8,1,4,4,4,3,2,5,5,4,2,5,7,2,3,4,5,1,5,4,5,6,5,3,4,3,2",
			"name": "Number of ordered ways to write n as x^2 + y^2 + z^2 + w^2 with 2*x*y + y*z - z*w - w*x a square, where w is a positive integer and x,y,z are nonnegative integers with x \u003c= y.",
			"comment": [
				"Conjecture: (i) a(n) \u003e 0 for all n \u003e 0, and a(n) = 1 only for n = 1, 11, 31, 47, 55, 71, 105, 115, 119, 253, 383, 385, 4^k*m (k = 0,1,2,... and m = 2, 22, 23, 30, 330).",
				"(ii) Each n = 0,1,2,... can be written as x^2 + y^2 + z^2 + w^2 with (x+y)*(z+w) a square, where w is an integer and x,y,z are nonnegative integers with x \u003c= y \u003e= z \u003e= |w|.",
				"See arXiv:1604.06723 for more conjectural refinements of Lagrange's four-square theorem."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A273021/b273021.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1604.06723\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, arXiv:1604.06723 [math.GM], 2016."
			],
			"example": [
				"a(1) = 1 since 1 = 0^2 + 0^2 + 0^2 + 1^2 with 0 = 0 and 2*0*0 + 0*0 - 0*1 - 1*0 = 0^2.",
				"a(2) = 1 since 2 = 0^2 + 1^2 + 0^2 + 1^2 with 0 \u003c 1 and 2*0*1 + 1*0 - 0*1 - 1*0 = 0^2.",
				"a(11) = 1 since 11 = 0^2 + 1^2 + 3^2 + 1^2 with 0 \u003c 1 and 2*0*1 + 1*3 - 3*1 - 1*0 = 0^2.",
				"a(22) = 1 since 22 = 0^2 + 3^2 + 2^2 + 3^2 with 0 \u003c 3 and 2*0*3 + 3*2 - 2*3 - 3*0 = 0^2.",
				"a(23) = 1 since 23 = 2^2 + 3^2 + 3^2 + 1^2 with 2 \u003c 3 and 2*2*3 + 3*3 - 3*1 - 1*2 = 4^2.",
				"a(30) = 1 since 30 = 1^2 + 3^2 + 2^2 + 4^2 with 1 \u003c 3 and 2*1*3 + 3*2 - 2*4 - 4*1 = 0^2.",
				"a(31) = 1 since 31 = 3^2 + 3^2 + 2^2 + 3^2 with 3 = 3 and",
				"2*3*3 + 3*2 - 2*3 -3*3 = 3^2.",
				"a(47) = 1 since 47 = 3^2 + 5^2 + 2^2 + 3^2 with 3 \u003c 5 and 2*3*5 + 5*2 - 2*3 - 3*3 = 5^2.",
				"a(55) = 1 since 55 = 1^2 + 7^2 + 2^2 + 1^2 with 1 \u003c 7 and 2*1*7 + 7*2 - 2*1 - 1*1 = 5^2.",
				"a(71) = 1 since 71 = 1^2 + 5^2 + 3^2 + 6^2 with 1 \u003c 5 and 2*1*5 + 5*3 - 3*6 - 6*1 = 1^2.",
				"a(105) = 1 since 105 = 1^2 + 6^2 + 2^2 + 8^2 with 1 \u003c 6 and 2*1*6 + 6*2 - 2*8 - 8*1 = 0^2.",
				"a(115) = 1 since 115 = 1^2 + 8^2 + 7^2 + 1^2 with 1 \u003c 8 and 2*1*8 + 8*7 - 7*1 - 1*1 = 8^2.",
				"a(119) = 1 since 119 = 1^2 + 6^2 + 1^2 + 9^2 with 1 \u003c 6 and 2*1*6 + 6*1 - 1*9 - 9*1 = 0^2.",
				"a(253) = 1 since 253 = 2^2 + 8^2 + 11^2 + 8^2 with 2 \u003c 8 and 2*2*8 + 8*11 - 11*8 - 8*2 = 4^2.",
				"a(330) = 1 since 330 = 4^2 + 13^2 + 8^2 + 9^2 with 4 \u003c 13 and 2*4*13 + 13*8 - 8*9 - 9*4 = 10^2.",
				"a(383) = 1 since 383 = 9^2 + 14^2 + 5^2 + 9^2 with 9 \u003c 14 and 2*9*14 + 14*5 - 5*9 - 9*9 = 14^2.",
				"a(385) = 1 since 385 = 4^2 + 12^2 + 0^2 + 15^2 with 4 \u003c 12 and 2*4*12 + 12*0 - 0*15 - 15*4 = 6^2."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]]",
				"Do[r=0;Do[If[SQ[n-x^2-y^2-z^2]\u0026\u0026SQ[y*(2x+z)-Sqrt[n-x^2-y^2-z^2]*(x+z)],r=r+1],{x,0,Sqrt[(n-1)/2]},{y,x,Sqrt[n-1-x^2]},{z,0,Sqrt[n-1-x^2-y^2]}];Print[n,\" \",r];Continue,{n,1,80}]"
			],
			"xref": [
				"Cf. A000118, A000290, A260625, A261876, A262357, A267121, A268197, A268507, A269400, A270073, A271510, A271513, A271518, A271608, A271665, A271714, A271721, A271724, A271775, A271778, A271824, A272084, A272332, A272351, A272620, A272888, A272977."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Zhi-Wei Sun_, May 13 2016",
			"references": 16,
			"revision": 8,
			"time": "2016-05-13T22:40:49-04:00",
			"created": "2016-05-13T22:40:49-04:00"
		}
	]
}