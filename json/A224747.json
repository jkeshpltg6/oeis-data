{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A224747",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 224747,
			"data": "1,0,1,1,3,5,12,23,52,105,232,480,1049,2199,4777,10092,21845,46377,100159,213328,460023,981976,2115350,4522529,9735205,20836827,44829766,96030613,206526972,442675064,951759621,2040962281,4387156587,9411145925,20226421380",
			"name": "Number of lattice paths from (0,0) to (n,0) that do not go below the x-axis and consist of steps U=(1,1), D=(1,-1) and H=(1,0), where H-steps are only allowed if y=1.",
			"comment": [
				"Apparently A125187 is even bisection. - _R. J. Mathar_, Jul 27 2013"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A224747/b224747.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"C. Banderier and M. Wallner, \u003ca href=\"http://www.emis.de/journals/SLC/wpapers/s77vortrag/wallner.pdf\"\u003eLattice paths with catastrophes\u003c/a\u003e, SLC 77, Strobl - 12.09.2016, H(x).",
				"Cyril Banderier and Michael Wallner, \u003ca href=\"https://arxiv.org/abs/1707.01931\"\u003eLattice paths with catastrophes\u003c/a\u003e, arXiv:1707.01931 [math.CO], 2017.",
				"Jean-Luc Baril and Sergey Kirgizov, \u003ca href=\"https://arxiv.org/abs/2104.01186\"\u003eBijections from Dyck and Motzkin meanders with catastrophes to pattern avoiding Dyck paths\u003c/a\u003e, arXiv:2104.01186 [math.CO], 2021."
			],
			"formula": [
				"a(n) = Sum_{k=0..floor((n-2)/2)} A009766(2*n-3*k-3, k) for n \u003e= 2. - _Johannes W. Meijer_, Jul 22 2013",
				"HANKEL transform is A000012. HANKEL transform omitting a(0) is a period 4 sequence [0, -1, 0, 1, ...] = -A101455. - _Michael Somos_, Jan 14 2014",
				"Given g.f. A(x), then 0 = A(x)^2 * (x^3 + 2*x^2 + x - 1) + A(x) * (-2*x^2 - 3*x + 2) + (2*x - 1). - _Michael Somos_, Jan 14 2014",
				"0 = a(n)*(a(n+1) +2*a(n+2) +a(n+3) -a(n+4)) +a(n+1)*(2*a(n+1) +5*a(n+2) +a(n+3) -2*a(n+4)) +a(n+2)*(2*a(n+2) -a(n+3) -a(n+4)) +a(n+3)*(-a(n+3) +a(n+4)). - _Michael Somos_, Jan 14 2014",
				"G.f.: (2 - 3*x - 2*x^2 + x * sqrt(1 - 4*x^2)) / (2 * (1 - x - 2*x^2 - x^3)). - _Michael Somos_, Jan 14 2014",
				"a(2*n) = A125187(n).",
				"D-finite with recurrence (-n+1)*a(n) +(n-1)*a(n-1) +6*(n-3)*a(n-2) +3*(-n+5)*a(n-3) +8*(-n+4)*a(n-4) +4*(-n+4)*a(n-5)=0. - _R. J. Mathar_, Sep 15 2021"
			],
			"example": [
				"a(5) = 5: UHHHD, UDUHD, UUDHD, UHDUD, UHUDD.",
				"a(6) = 12: UHHHHD, UDUHHD, UUDHHD, UHDUHD, UHUDHD, UHHDUD, UDUDUD, UUDDUD, UHHUDD, UDUUDD, UUDUDD, UUUDDD.",
				"G.f. = 1 + x^2 + x^3 + 3*x^4 + 5*x^5 + 12*x^6 + 23*x^7 + 52*x^8 + 105*x^9 + ..."
			],
			"maple": [
				"a:= proc(n) option remember; `if`(n\u003c5, [1, 0, 1, 1, 3][n+1],",
				"      a(n-1)+ (6*(n-3)*a(n-2) -3*(n-5)*a(n-3)",
				"      -8*(n-4)*a(n-4) -4*(n-4)*a(n-5))/(n-1))",
				"    end:",
				"seq(a(n), n=0..40);"
			],
			"mathematica": [
				"a[n_] := a[n] = If[n \u003c 5, {1, 0, 1, 1, 3}[[n+1]], a[n-1] + (6*(n-3)*a[n-2] - 3*(n-5)*a[n-3] - 8*(n-4)*a[n-4] - 4*(n-4)*a[n-5])/(n-1)]; Table[a[n], {n, 0, 34}] (* _Jean-François Alcover_, Jun 20 2013, translated from Maple *)",
				"a[ n_] := SeriesCoefficient[ (2 - 3 x - 2 x^2 + x Sqrt[1 - 4 x^2]) / (2 (1 - x - 2 x^2 - x^3)), {x, 0, n}] (* _Michael Somos_, Jan 14 2014 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, polcoeff( (2 - 3*x - 2*x^2 + x * sqrt(1 - 4*x^2 + x * O(x^n)) ) / (2 * (1 - x - 2*x^2 - x^3)) n))} /* _Michael Somos_, Jan 14 2014 */"
			],
			"xref": [
				"Cf. A000108 (without H-steps), A001006 (unrestricted H-steps), A057977 (\u003c=1 H-step).",
				"Cf. A000012, A101455, A125187, A001405 (invert transform)."
			],
			"keyword": "nonn",
			"offset": "0,5",
			"author": "_Alois P. Heinz_, Apr 17 2013",
			"references": 2,
			"revision": 53,
			"time": "2021-10-04T09:02:04-04:00",
			"created": "2013-04-18T07:25:47-04:00"
		}
	]
}