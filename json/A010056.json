{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A010056",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 10056,
			"data": "1,1,1,1,0,1,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0",
			"name": "Characteristic function of Fibonacci numbers: a(n) = 1 if n is a Fibonacci number, otherwise 0.",
			"comment": [
				"Understood as a binary number, Sum_{k\u003e=0} a(k)/2^k, the resulting decimal expansion is 1.910278797207865891... = Fibonacci_binary+0.5 (see A084119) or Fibonacci_binary_constant-0.5 (see A124091), respectively. - _Hieronymus Fischer_, May 14 2007",
				"a(n)=1 if and only if there is an integer m such that x=n is a root of p(x)=25*x^4-10*m^2*x^2+m^4-16. Also a(n)=1 iff floor(s)\u003c\u003efloor(c) or ceiling(s)\u003c\u003eceiling(c) where s=arcsinh(sqrt(5)*n/2)/log(phi), c=arccosh(sqrt(5)*n/2)/log(phi) and phi=(1+sqrt(5))/2. - _Hieronymus Fischer_, May 17 2007",
				"a(A000045(n)) = 1; a(A001690(n)) = 0. - _Reinhard Zumkeller_, Oct 10 2013",
				"Image, under the map sending a,b,c -\u003e 1, d,e,f -\u003e 0, of the fixed point, starting with a, of the morphism sending a -\u003e ab, b -\u003e c, c -\u003e cd, d -\u003e d, e -\u003e ef, f -\u003e e. - _Jeffrey Shallit_, May 14 2016"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A010056/b010056.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Jean-Paul Allouche, Julien Cassaigne, Jeffrey Shallit, and Luca Q. Zamboni, \u003ca href=\"https://arxiv.org/abs/1711.10807\"\u003eA Taxonomy of Morphic Sequences\u003c/a\u003e, arXiv preprint arXiv:1711.10807 [cs.FL], Nov 29 2017.",
				"D. Bailey et al., \u003ca href=\"https://doi.org/10.5802/jtnb.457\"\u003eOn the binary expansions of algebraic numbers\u003c/a\u003e, Journal de Théorie des Nombres de Bordeaux (2004), Volume: 16, Issue: 3, page 487-518.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Fibonacci_number\"\u003eFibonacci number\u003c/a\u003e",
				"\u003ca href=\"/index/Ch#char_fns\"\u003eIndex entries for characteristic functions\u003c/a\u003e"
			],
			"formula": [
				"G.f.: (Sum_{k\u003e=0} x^A000045(k)) - x. - _Hieronymus Fischer_, May 17 2007"
			],
			"maple": [
				"a:= n-\u003e (t-\u003e `if`(issqr(t+4) or issqr(t-4), 1, 0))(5*n^2):",
				"seq(a(n), n=0..144);  # _Alois P. Heinz_, Dec 06 2020"
			],
			"mathematica": [
				"Join[{1},With[{fibs=Fibonacci[Range[15]]},If[MemberQ[fibs,#],1,0]\u0026 /@Range[100]]]  (* _Harvey P. Dale_, May 02 2011 *)"
			],
			"program": [
				"(PARI) a(n)=my(k=n^2);k+=(k+1)\u003c\u003c2; issquare(k) || (n\u003e0 \u0026\u0026 issquare(k-8)) \\\\ _Charles R Greathouse IV_, Jul 30 2012",
				"(Haskell)",
				"import Data.List (genericIndex)",
				"a010056 = genericIndex a010056_list",
				"a010056_list = 1 : 1 : ch [2..] (drop 3 a000045_list) where",
				"   ch (x:xs) fs'@(f:fs) = if x == f then 1 : ch xs fs else 0 : ch xs fs'",
				"-- _Reinhard Zumkeller_, Oct 10 2013"
			],
			"xref": [
				"Cf. A000045, A001690, A104162, A108852, A124091, A130233, A130234.",
				"Decimal expansion of Fibonacci binary is in A084119.",
				"Sequences mentioned in the Allouche et al. \"Taxonomy\" paper, listed by example number: 1: A003849, 2: A010060, 3: A010056, 4: A020985 and A020987, 5: A191818, 6: A316340 and A273129, 18: A316341, 19: A030302, 20: A063438, 21: A316342, 22: A316343, 23: A003849 minus its first term, 24: A316344, 25: A316345 and A316824, 26: A020985 and A020987, 27: A316825, 28: A159689, 29: A049320, 30: A003849, 31: A316826, 32: A316827, 33: A316828, 34: A316344, 35: A043529, 36: A316829, 37: A010060."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_",
			"references": 57,
			"revision": 64,
			"time": "2021-02-28T17:59:41-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}