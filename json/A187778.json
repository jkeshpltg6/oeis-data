{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A187778",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 187778,
			"data": "1,6,12,18,24,36,48,54,72,96,108,144,162,192,216,288,324,384,432,486,576,648,768,864,972,1152,1296,1458,1536,1728,1944,2304,2592,2916,3072,3456,3888,4374,4608,5184,5832,6144,6912,7776,8748,9216,10368,11664,12288,13122,13824,15552,17496,18432,20736,23328",
			"name": "Numbers k dividing psi(k), where psi is the Dedekind psi function (A001615).",
			"comment": [
				"This sequence is closed under multiplication.",
				"Also 1 and the numbers where psi(n)/n = 2, or n/phi(n)=3, or psi(n)/phi(n)=6.",
				"Also 1 and the numbers of the form 2^i*3^j with i, j \u003e= 1 (A033845).",
				"If M(n) is the n X n matrix whose elements m(i,j) = 2^i*3^j, with i, j \u003e= 1, then det(M(n))=0.",
				"Numbers n such that Product_{i=1..q} (1 + 1/d(i)) is an integer where q is the number of the distinct prime divisors d(i) of n. - _Michel Lagneau_, Jun 17 2016",
				"1 and numbers k such that k = phi(k) + phi(2*k). - _Paolo P. Lava_, Aug 07 2017"
			],
			"reference": [
				"S. Ramanujan, Collected Papers, Ed. G. H. Hardy et al., Cambridge 1927; Chelsea, NY, 1962, p. xxiv."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A187778/b187778.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (terms 1..191 from Vincenzo Librandi)",
				"R. Blecksmith, M. McCallum and J. L. Selfridge, \u003ca href=\"http://www.jstor.org/stable/2589404\"\u003e3-smooth representations of integers\u003c/a\u003e, Amer. Math. Monthly, 105 (1998), 529-543.",
				"E. Deutsch, \u003ca href=\"http://arxiv.org/abs/1111.4288\"\u003eTree statistics from Matula numbers\u003c/a\u003e, arXiv:1111.4288 [math.CO], 2011.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/SmoothNumber.html\"\u003eSmooth Number\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Closure_(mathematics)\"\u003eClosure\u003c/a\u003e"
			],
			"formula": [
				"For n \u003e 1, a(n) = 6 * A003586(n).",
				"Sum_{n\u003e0} 1/a(n)^k) = 1 + Sum_{i\u003e0} Sum_{j\u003e0} 1/(2^i * 3^j)^k = 1 + 1/((2^k-1)*(3^k-1))."
			],
			"example": [
				"psi(48) = 96 and 96/48 = 2 so 48 is in this sequence."
			],
			"mathematica": [
				"Select[Range[10^4],#/EulerPhi[#]==3 || #==1\u0026]",
				"Join[{1}, 6 Select[Range@4000, Last@Map[First, FactorInteger@#]\u003c=3 \u0026]] (* _Vincenzo Librandi_, Jan 11 2019 *)"
			],
			"program": [
				"(PARI) dedekindpsi(n) = if( n\u003c1, 0, direuler( p=2, n, (1 + X) / (1 - p*X)) [n]);",
				"k=0; n=0; while(k\u003c10000,n++; if( dedekindpsi(n) % n== 0, k++; print1(n, \", \")));",
				"(MAGMA) [6*n: n in [1..3000] | PrimeDivisors(n) subset [2, 3]]; // _Vincenzo Librandi_, Jan 11 2019"
			],
			"xref": [
				"Cf. A003586, A001615, A007694, A033950, A074946, A075592."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Enrique Pérez Herrero_, Jan 05 2013",
			"references": 4,
			"revision": 53,
			"time": "2020-02-05T04:03:00-05:00",
			"created": "2013-01-05T21:15:58-05:00"
		}
	]
}