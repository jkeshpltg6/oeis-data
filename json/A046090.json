{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A046090",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 46090,
			"data": "1,4,21,120,697,4060,23661,137904,803761,4684660,27304197,159140520,927538921,5406093004,31509019101,183648021600,1070379110497,6238626641380,36361380737781,211929657785304,1235216565974041",
			"name": "Consider all Pythagorean triples (X,X+1,Z) ordered by increasing Z; sequence gives X+1 values.",
			"comment": [
				"Solution to a(a-1) = 2b(b-1) in natural numbers: a = a(n), b = b(n) = A011900(n).",
				"n such that n^2 = (1/2)*(n+floor(sqrt(2)*n*floor(sqrt(2)*n))). - _Benoit Cloitre_, Apr 15 2003",
				"a(n) = A001109(n+1) - A053141(n). - _Manuel Valdivia_, Apr 03 2010",
				"a(n) balls in an urn; b(n) = A011900(n) are red; draw 2 balls without replacement; 2*Probability(2 red balls) = Probability(2 balls); this is equivalent to the Pell equation A(n)^2-2*B(n)^2 = -1 with a(n) = (A(n)+1)/2; b(n) = (B(n)+1)/2; and the fundamental solution (7;5) and the solution (3;2) for the unit form. - _Paul Weisenhorn_, Aug 03 2010",
				"With b(n) = A001109, a(n)*(a(n)-1)/2 = b(n)*b(n+1) and b(n) + b(n+1) = 2*a(n) - 1. - _Kenneth J Ramsey_, Apr 24 2011",
				"Positive integers n such that n^2 is a centered square number (A001844). - _Colin Barker_, Feb 12 2015"
			],
			"reference": [
				"A. H. Beiler, Recreations in the Theory of Numbers. New York: Dover, pp. 122-125, 1964."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A046090/b046090.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"T. W. Forget and T. A. Larkin, \u003ca href=\"http://www.fq.math.ca/Scanned/6-3/forget.pdf\"\u003ePythagorean triads of the form X, X+1, Z described by recurrence sequences\u003c/a\u003e, Fib. Quart., 6 (No. 3, 1968), 94-104.",
				"L. J. Gerstein, \u003ca href=\"http://www.jstor.org/stable/30044157\"\u003ePythagorean triples and inner products\u003c/a\u003e, Math. Mag., 78 (2005), 205-213.",
				"H. J. Hindin, \u003ca href=\"/A006062/a006062.pdf\"\u003eStars, hexes, triangular numbers and Pythagorean triples\u003c/a\u003e, J. Rec. Math., 16 (1983/1984), 191-193. (Annotated scanned copy)",
				"Ron Knott, \u003ca href=\"http://www.maths.surrey.ac.uk/hosted-sites/R.Knott/Pythag/pythag.html\"\u003ePythagorean Triples and Online Calculators\u003c/a\u003e",
				"S. Northshield, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL18/Northshield/north4.html\"\u003eAn Analogue of Stern's Sequence for Z[sqrt(2)]\u003c/a\u003e, Journal of Integer Sequences, 18 (2015), #15.11.6.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PythagoreanTriple.html\"\u003ePythagorean Triple\u003c/a\u003e",
				"\u003ca href=\"/index/Tu#2wis\"\u003eIndex entries for two-way infinite sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (7,-7,1)."
			],
			"formula": [
				"a(n) = (1+sqrt(1+8*b(n)*(b(n)+1)))/2 with b(n) = A011900(n).",
				"a(n) = 6*a(n-1) - a(n-2) - 2, n \u003e= 2, a(0) = 1, a(1) = 4.",
				"a(n) = (A(n+1) - 3*A(n) + 2)/4 with A(n) = A001653(n).",
				"From _Barry E. Williams_, May 03 2000: (Start)",
				"G.f.: (1-3*x)/((1-6*x+x^2)*(1-x)).",
				"a(n) = partial sums of A001541(n). (End)",
				"A001652(n)*A001652(n+1) + a(n)*a(n+1) = A001542(n+1)^2 = A084703(n+1). - _Charlie Marion_, Jul 01 2003",
				"a(n) = 1/2 + ((1-2^{1/2})/4)*(3 - 2^{3/2})^n + ((1+2^{1/2})/4)*(3 + 2^{3/2})^n. - _Antonio Alberto Olivares_, Oct 13 2003",
				"Let a(n) = A001652(n), b(n) = this sequence and c(n) = A001653(n). Then for k\u003ej, c(i)*(c(k) - c(j)) = a(k+i)+...+a(i+j+1) + a(k-i-1)+...+a(j-i) + k - j. For n\u003c0, a(n) = -b(-n-1). Also a(n)*a(n+2k+1) + b(n)*b(n+2k+1) + c(n)*c(n+2k+1) = (a(n+k+1) - a(n+k))^2; a(n)*a(n+2k) + b(n)*b(n+2k) + c(n)*c(n+2k) = 2*c(n+k)^2. - _Charlie Marion_, Jul 01 2003",
				"2*a(n) = 2*A084159(n) + 1 + (-1)^(n+1)=2*A046729(n) + 1 - (-1)^(n+1). - _Lekraj Beedassy_, Jul 16 2004",
				"From _Paul Weisenhorn_, Aug 03 2010: (Start)",
				"a(n+1) = round((1+(7+5*sqrt(2))*(3+2*sqrt(2))^n)/2);",
				"b(n+1) = round((2+(10+7*sqrt(2))*(3+2*sqrt(2))^n)/4) = A011900(n+1).",
				"(End)",
				"Let T(n) be the n-th triangular number; then T(a(n)) = A011900(n)^2 + A001109(n). See also A001653. - _Charlie Marion_, Apr 25 2011",
				"a(0)=1, a(1)=4, a(2)=21, a(n) = 7*a(n-1) - 7*a(n-2) + a(n-3). - _Harvey P. Dale_, Apr 13 2012",
				"Lim_{n-\u003einfinity} a(n+1)/a(n) = 3 + 2*sqrt(2) = A156035. - _Ilya Gutkovskiy_, Jul 10 2016",
				"a(n) = A001652(n)+1. - _Dimitri Papadopoulos_, Jul 06 2017"
			],
			"example": [
				"For n=4: a(4)=697; b(4)=493; 2*binomial(493,2)=485112=binomial(697,2). - _Paul Weisenhorn_, Aug 03 2010"
			],
			"maple": [
				"Digits:=100: seq(round((1+(7+5*sqrt(2))*(3+2*sqrt(2))^(n-1))/2)/2, n=0..20); # _Paul Weisenhorn_, Aug 03 2010"
			],
			"mathematica": [
				"Join[{1},#+1\u0026/@With[{c=3+2Sqrt[2]},NestList[Floor[c #]+3\u0026,3,20]]] (* _Harvey P. Dale_, Aug 19 2011 *)",
				"LinearRecurrence[{7,-7,1},{1,4,21},25] (* _Harvey P. Dale_, Apr 13 2012 *)",
				"a[n_] := (2-ChebyshevT[n, 3]+ChebyshevT[n+1, 3])/4; Array[a, 21, 0] (* _Jean-François Alcover_, Jul 10 2016, adapted from PARI *)"
			],
			"program": [
				"(PARI) a(n)=(2-subst(poltchebi(abs(n))-poltchebi(abs(n+1)),x,3))/4",
				"(PARI) x='x+O('x^30); Vec((1-3*x)/((1-6*x+x^2)*(1-x))) \\\\ _G. C. Greubel_, Jul 15 2018",
				"(Haskell)",
				"a046090 n = a046090_list !! n",
				"a046090_list = 1 : 4 : map (subtract 2)",
				"   (zipWith (-) (map (* 6) (tail a046090_list)) a046090_list)",
				"-- _Reinhard Zumkeller_, Jan 10 2012",
				"(MAGMA) m:=30; R\u003cx\u003e:=PowerSeriesRing(Integers(), m); Coefficients(R!((1-3*x)/((1-6*x+x^2)*(1-x)))); // _G. C. Greubel_, Jul 15 2018"
			],
			"xref": [
				"Other 2 sides are A001652 and A001653.",
				"Cf. A011900, A001541. A001652(n) = -a(-1-n).",
				"See comments in A301383."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,2",
			"author": "_Eric W. Weisstein_",
			"ext": [
				"Additional comments from _Wolfdieter Lang_"
			],
			"references": 37,
			"revision": 103,
			"time": "2018-07-15T22:11:23-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}