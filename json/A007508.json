{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007508",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7508,
			"id": "M1855",
			"data": "2,8,35,205,1224,8169,58980,440312,3424506,27412679,224376048,1870585220,15834664872,135780321665,1177209242304,10304195697298,90948839353159,808675888577436",
			"name": "Number of twin prime pairs below 10^n.",
			"comment": [
				"\"At the present time (2001), Thomas Nicely has reached pi_2(3*10^15) and his value is confirmed by Pascal Sebah who made a new computation from scratch and up to pi_2(5*10^15) [ = 5357875276068] with an independent implementation.\"",
				"Though the first paper contributed by D. A. Goldston was reported to be flawed, the more recent one (with other coauthors) maintains and substantiates the result. - _Lekraj Beedassy_, Aug 19 2005",
				"Theorem. While g is even, g \u003e 0, number of primes p \u003c x (x is integer) such that p' = p + g is also prime, could be written as qpg(x) = qcc(x) - (x - pi(x) - pi(x + g) + 1) where qcc(x) is the number of \"common composite numbers\" c \u003c= x such that c and c' = c + g both are composite (see Example below; I propose it here as a theorem only not to repeat for so-called \"cousin\"-primes (p; p+4), \"sexy\"-primes (p; p+6), etc.). - _Sergey Pavlov_, Apr 08 2021"
			],
			"reference": [
				"P. Ribenboim, The Book of Prime Number Records. Springer-Verlag, NY, 2nd ed., 1989, p. 202.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Richard P. Brent, \u003ca href=\"https://doi.org/10.1090/S0025-5718-1975-0369287-1\"\u003eIrregularities in the distribution of primes and twin primes\u003c/a\u003e, Math. Comp. 29 (1975), 43-56.",
				"C. K. Caldwell, \u003ca href=\"http://www.utm.edu/~caldwell/preprints/Heuristics.pdf\"\u003eAn amazing prime heuristic\u003c/a\u003e, Table 1.",
				"C. K. Caldwell, The Prime Glossary, \u003ca href=\"http://primes.utm.edu/glossary/page.php/TwinPrimeConjecture.html\"\u003eTwin prime conjecture\u003c/a\u003e",
				"T. H. Chan, \u003ca href=\"https://arxiv.org/abs/math/0503441\"\u003eA note on Primes in Short Intervals\u003c/a\u003e, arXiv:math/0503441 [math.NT], 2005.",
				"P. Erdős, \u003ca href=\"http://dx.doi.org/10.1307/mmj/1028997963\"\u003eSome Unsolved Problems\u003c/a\u003e, Michigan Math. J., Volume 4, Issue 3 (1957), 291-300.",
				"G. H. Gadiyar \u0026 R. Padma, \u003ca href=\"http://arXiv.org/abs/hep-th/9806061\"\u003eRenormalisation and the density of prime pairs\u003c/a\u003e, arXiv:hep-th/9806061, 1998.",
				"G. H. Gadiyar \u0026 R. Padma, \u003ca href=\"http://empslocal.ex.ac.uk/people/staff/mrwatkin/zeta/padma.pdf\"\u003eRamanujan-Fourier series, the Wiener-Khintchine formula and the distribution of prime pairs\u003c/a\u003e, Physica A 269 (1999) 503-510.",
				"D. A. Goldston, J. Pintz \u0026 C. Y. Yildirim, \u003ca href=\"https://arxiv.org/abs/math/0508185\"\u003ePrimes in Tuples, I\u003c/a\u003e, arXiv:math/0508185 [math.NT], 2005.",
				"D. A. Goldston, J. Pintz \u0026 C. Y. Yildirim, \u003ca href=\"http://www.math.sjsu.edu/~goldston/gpy02-08-05.pdf\"\u003eSmall Gaps Between Primes, II\u003c/a\u003e",
				"D. A. Goldston, J. Pintz \u0026 C. Y. Yildirim, \u003ca href=\"http://arXiv.org/abs/math/0512436\"\u003eThe Path to Recent Progress on Small Gaps Between Primes\u003c/a\u003e, arXiv:math/0512436 [math.NT], 2005-2006.",
				"D. A. Goldston \u0026 C. Y. Yildirim, \u003ca href=\"https://arxiv.org/abs/math/0504336\"\u003eSmall Gaps Between Primes, I\u003c/a\u003e, arXiv:math/0504336 [math.NT], 2005.",
				"D. A. Goldston \u0026 C. Yildirim, \u003ca href=\"https://web.archive.org/web/20080309063536/http://aimath.org/primegaps/goldston_tech/\"\u003eSmall gaps between consecutive primes\u003c/a\u003e",
				"D. A. Goldston et al., \u003ca href=\"https://arxiv.org/abs/math/0506067\"\u003eSmall gaps between primes or almost primes\u003c/a\u003e, arXiv:math/0506067 [math.NT], 2005.",
				"D. A. Goldston et al., \u003ca href=\"https://arxiv.org/abs/math/0505300\"\u003eSmall Gaps between Primes Exist\u003c/a\u003e, arXiv:math/0505300 [math.NT], 2005.",
				"Xavier Gourdon and Pascal Sebah, \u003ca href=\"http://numbers.computation.free.fr/Constants/Primes/twin.html\"\u003eIntroduction to Twin Primes and Brun's Constant\u003c/a\u003e",
				"A. Granville \u0026 K. Soundararajan, \u003ca href=\"https://web.archive.org/web/20110724230750/http://aimath.org/primegaps/residueerror/\"\u003eOn the error in Goldston and Yildirim's \"Small gaps between consecutive primes\"\u003c/a\u003e",
				"Gareth A. Jones and Alexander K. Zvonkin, \u003ca href=\"https://arxiv.org/abs/2104.12015\"\u003eKlein's ten planar dessins of degree 11, and beyond\u003c/a\u003e, arXiv:2104.12015 [math.GR], 2021. See p. 24.",
				"P. F. Kelly \u0026 F. Pilling, \u003ca href=\"https://arxiv.org/abs/math/0103191\"\u003eCharacterization of the Distribution of Twin Primes\u003c/a\u003e, arXiv:math/0103191 [math.NT], 2001.",
				"P. F. Kelly \u0026 T. Pilling, \u003ca href=\"https://arxiv.org/abs/math/0104205\"\u003eImplications of a New Characterization of the Distribution of Twin Primes\u003c/a\u003e, arXiv:math/0104205 [math.NT], 2001.",
				"P. F. Kelly \u0026 T. Pilling, \u003ca href=\"https://arxiv.org/abs/math/0106223\"\u003eDiscrete Reanalysis of a New Model of the Distribution of Twin Primes\u003c/a\u003e, arXiv:math/0106223 [math.NT], 2001.",
				"James Maynard, \u003ca href=\"https://arxiv.org/abs/1910.14674\"\u003eOn the Twin Prime Conjecture\u003c/a\u003e, arXiv:1910.14674 [math.NT], 2019, p. 2.",
				"Thomas R. Nicely, \u003ca href=\"https://faculty.lynchburg.edu/~nicely/gaps/gaplist.html\"\u003eFirst occurrence prime gaps\u003c/a\u003e [For local copy see A000101]",
				"Thomas R. Nicely, \u003ca href=\"https://faculty.lynchburg.edu/~nicely/twins/twins.html\"\u003eEnumeration to 10^14 of the twin primes and Brun's constant\u003c/a\u003e, Virginia Journal of Science, 46:3 (Fall, 1995), 195-204.",
				"Thomas R. Nicely, \u003ca href=\"/A001359/a001359.pdf\"\u003eEnumeration to 10^14 of the twin primes and Brun's constant\u003c/a\u003e [Local copy, pdf only]",
				"Nova Science, \u003ca href=\"http://www.pbs.org/wgbh/nova/sciencenow/3302/02.html\"\u003eTwin Prime Conjecture\u003c/a\u003e",
				"Tomás Oliveira e Silva, \u003ca href=\"http://sweet.ua.pt/tos/primes.html\"\u003eTables of values of pi(x) and of pi2(x)\u003c/a\u003e [From _M. F. Hasler_, Dec 18 2008]",
				"J. Richstein, \u003ca href=\"https://web.archive.org/web/20040911002922/http://www.informatik.uni-giessen.de/staff/richstein/res/tp-en.html\"\u003eComputing the number of twin primes up to 10^14\u003c/a\u003e",
				"J. Richstein, \u003ca href=\"https://web.archive.org/web/20070711220924/http://www.mscs.dal.ca/~joerg/res/tp-en.html\"\u003eComputing the number of twin primes up to 10^14\u003c/a\u003e",
				"J. Sondow, J. W. Nicholson, and T. D. Noe, \u003ca href=\"http://arxiv.org/abs/1105.2249\"\u003e Ramanujan Primes: Bounds, Runs, Twins, and Gaps\u003c/a\u003e, arXiv:1105.2249 [math.NT], 2011; J. Integer Seq. 14 (2011) Article 11.6.2.",
				"Jonathan P. Sorenson, Jonathan Webster, \u003ca href=\"https://arxiv.org/abs/1807.08777\"\u003eTwo Algorithms to Find Primes in Patterns\u003c/a\u003e, arXiv:1807.08777 [math.NT], 2018.",
				"K. Soundararajan, \u003ca href=\"https://arxiv.org/abs/math/0606408\"\u003eThe distribution of prime numbers\u003c/a\u003e, arXiv:math/0606408 [math.NT], 2006.",
				"K. Soundararajan, \u003ca href=\"https://arxiv.org/abs/math/0605696\"\u003eSmall gaps between prime numbers:The work of Goldston-Pintz-Yildirim\u003c/a\u003e, arXiv:math/0605696 [math.NT], 2006.",
				"K. Soundararajan, \u003ca href=\"http://dx.doi.org/10.1090/S0273-0979-06-01142-6\"\u003eSmall gaps between prime numbers:The work of Goldston-Pintz-Yildirim\u003c/a\u003e, Bull. Amer. Math. Soc. 44 (2007), 1-18.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/TwinPrimes.html\"\u003eTwin Primes\u003c/a\u003e",
				"Eric Weisstein, Mathworld Headline News, \u003ca href=\"http://mathworld.wolfram.com/news/2004-06-09/twinprimes\"\u003eTwin Primes Proof Proffered\u003c/a\u003e",
				"M. Wolf, \u003ca href=\"http://arXiv.org/abs/math/0105211\"\u003eSome Remarks on the Distribution of twin Primes\u003c/a\u003e, arXiv:math/0105211 [math.NT], 2001.",
				"C. Yildirim \u0026 D. Goldston, \u003ca href=\"http://www.math.boun.edu.tr/instructors/yildirim/yildirimtech.htm\"\u003eSmall gaps between consecutive primes\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#primepop\"\u003eIndex entries for sequences related to numbers of primes in various ranges\u003c/a\u003e"
			],
			"formula": [
				"Partial sums of A070076(n). - _Lekraj Beedassy_, Jun 11 2004",
				"For 1 \u003c n \u003c 19, a(n) ~ e * pi(10^n) / (5*n - 5) = e * A006880(n) / (5*n - 5) where e is Napier's constant, see A001113 (probably, so is for any n \u003e 18; we use n \u003e 1 to avoid division by zero). - _Sergey Pavlov_, Apr 07 2021",
				"For any n, a(n) = qcc(x) - (10^n - pi(10^n) - pi(10^n + 2) + 1) where qcc(x) is the number of \"common composite numbers\" c \u003c= 10^n such that c and c' = c + 2 both are composite (trivial). - _Sergey Pavlov_, Apr 08 2021"
			],
			"example": [
				"For x = 10, qcc(x) = 4 (since 2 is prime; 4, 6, 8, 10 are even, and no odd 0 \u003c d \u003c 25 such that both d and d' = d + 2 are composite), pi(10) = 4, pi(10 + 2) = 5, but, while v = 2+2 or v = 2-2 would be even, we must add 1; hence, a(1) = qcc(10^1) - (10^1 - pi(10^1) - pi(10^1 + 2) + 1) = 4 - (10 - 4 - 5 + 1) = 2 (trivial). - _Sergey Pavlov_, Apr 08 2021"
			],
			"mathematica": [
				"ile = 2; Do[Do[If[(PrimeQ[2 n - 1]) \u0026\u0026 (PrimeQ[2 n + 1]), ile = ile + 1], {n, 5*10^m, 5*10^(m + 1)}]; Print[{m, ile}], {m, 0, 7}] (* _Artur Jasinski_, Oct 24 2011 *)"
			],
			"program": [
				"(PARI) a(n)=my(s,p=2);forprime(q=3,10^n,if(q-p==2,s++);p=q);s \\\\ _Charles R Greathouse IV_, Mar 21 2013"
			],
			"xref": [
				"Cf. A001097.",
				"Cf. A173081 and A181678 (number of twin Ramanujan prime pairs below 10^n).",
				"Cf. A152051, A347278, A347279."
			],
			"keyword": "nonn,nice,hard,more",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_, _Robert G. Wilson v_",
			"ext": [
				"pi2(10^15) due to Nicely and Szymanski, contributed by _Eric W. Weisstein_",
				"pi2(10^16) due to Pascal Sebah, contributed by _Robert G. Wilson v_, Aug 22 2002",
				"Added a(17)-a(18) computed by Tomás Oliveira e Silva and link to his web site. - _M. F. Hasler_, Dec 18 2008",
				"Definition corrected by _Max Alekseyev_, Oct 25 2010",
				"a(16) corrected by _Dana Jacobsen_, Mar 28 2014"
			],
			"references": 47,
			"revision": 108,
			"time": "2021-10-28T13:17:39-04:00",
			"created": "1994-09-19T03:00:00-04:00"
		}
	]
}