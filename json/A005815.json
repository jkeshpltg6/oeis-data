{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005815",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5815,
			"id": "M4991",
			"data": "1,0,0,0,0,1,15,465,19355,1024380,66462606,5188453830,480413921130,52113376310985,6551246596501035,945313907253606891,155243722248524067795,28797220460586826422720",
			"name": "Number of 4-valent labeled graphs with n nodes.",
			"reference": [
				"Miklos Bona, editor, Handbook of Enumerative Combinatorics, CRC Press, 2015, p. 411.",
				"L. Comtet, Advanced Combinatorics, Reidel, 1974, p. 279.",
				"I. P. Goulden and D. M. Jackson, Combinatorial Enumeration, John Wiley and Sons, N.Y., 1983.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi and Vaclav Kotesovec, \u003ca href=\"/A005815/b005815.txt\"\u003eTable of n, a(n) for n = 0..260\u003c/a\u003e (first 100 terms from Vincenzo Librandi)",
				"I. P. Goulden, D. M. Jackson, and J. W. Reilly, \u003ca href=\"https://uwaterloo.ca/math/sites/ca.math/files/uploads/files/gjsiad1983.pdf\"\u003eThe Hammond series of a symmetric function and its application to P-recursiveness\u003c/a\u003e, SIAM J. Algebraic Discrete Methods 4 (1983), no. 2, 179-193.",
				"Atabey Kaygun, \u003ca href=\"https://arxiv.org/abs/2101.02299\"\u003eEnumerating Labeled Graphs that Realize a Fixed Degree Sequence\u003c/a\u003e, arXiv:2101.02299 [math.CO], 2021.",
				"Vaclav Kotesovec, \u003ca href=\"/A005815/a005815.txt\"\u003eRecurrence (of order 10)\u003c/a\u003e",
				"R. C. Read and N. C. Wormald, \u003ca href=\"http://dx.doi.org/10.1002/jgt.3190040208\"\u003eNumber of labeled 4-regular graphs\u003c/a\u003e, J. Graph Theory, 4 (1980), 203-212."
			],
			"formula": [
				"From _Vladeta Jovovic_, Mar 26 2001: (Start)",
				"E.g.f. f(x) = Sum_{n \u003e= 0} a(n)*x^n/(n)! satisfies the differential equation 16*x^2*(x - 1)^2*(x + 2)^2*(x^5 + 2*x^4 + 2*x^2 + 8*x - 4)*(d^2/dx^2)y(x) - 4*(x^13 + 4*x^12 - 16*x^10 - 10*x^9 - 36*x^8 - 220*x^7 - 348*x^6 - 48*x^5 + 200*x^4 - 336*x^3 - 240*x^2 + 416*x - 96)*(d/dx)y(x) - x^4*(x^5 + 2*x^4 + 2*x^2 + 8*x - 4)^2*y(x) = 0.",
				"Recurrence: a(n) = - 1/384*(( - 256*n^2 - 896*n + 1152)*a(n - 1) + (768*n^3 - 3648*n^2 + 5568*n - 2688)*a(n - 2) + ( - 192*n^4 + 3264*n^3 - 14784*n^2 + 24384*n - 12672)*a(n - 3) + (224*n^6 - 4512*n^5 + 36304*n^4 - 148160*n^3 + 320016*n^2 - 341728*n + 137856)*a(n - 5) + ( - 640*n^5 + 8800*n^4 - 46400*n^3 + 116000*n^2 - 135360*n + 57600)*a(n - 4) + ( - 24*n^10 + 1320*n^9 - 31680*n^8 + 435600*n^7 - 3786552*n^6 + 21649320*n^5 - 82006320*n^4 + 201828000*n^3 - 306085824*n^2 + 255087360*n - 87091200)*a(n - 11) + (64*n^10 - 3480*n^9 + 82692*n^8 - 1127232*n^7 + 9726024*n^6 - 55255032*n^5 + 208179908*n^4 - 510068208*n^3 + 770738352*n^2 - 640484928*n + 218211840)*a(n - 9) + (16*n^11 - 992*n^10 + 27256*n^9 - 437160*n^8 + 4536288*n^7 - 31876656*n^6 + 154182488*n^5 - 510784360*n^4 + 1128552896*n^3 - 1570313952*n^2 + 1223830656*n - 397716480)*a(n - 10) + ( - 128*n^8 + 5488*n^7 - 94576*n^6 + 864976*n^5 - 4606672*n^4 + 14604352*n^3 - 26753984*n^2 + 25611264*n - 9630720)*a(n - 7) + (16*n^9 - 576*n^8 + 8704*n^7 - 71680*n^6 + 348880*n^5 - 1013824*n^4 + 1673376*n^3 - 1333120*n^2 + 226944*n + 161280)*a(n - 8) + (128*n^7 - 2192*n^6 + 12048*n^5 - 8240*n^4 - 151248*n^3 + 565312*n^2 - 765248*n + 349440)*a(n - 6) + ( - 4*n^13 + 364*n^12 - 14924*n^11 + 364364*n^10 - 5897892*n^9 + 66678612*n^8 - 540145892*n^7 + 3163772612*n^6 - 13344475144*n^5 + 39830815024*n^4 - 81255012384*n^3 + 106386868224*n^2 - 79211036160*n + 24908083200)*a(n - 14) + ( - 4*n^13 + 360*n^12 - 14612*n^11 + 353496*n^10 - 5674812*n^9 + 63680760*n^8 - 512439356*n^7 + 2983811688*n^6 - 12520194544*n^5 + 37201987680*n^4 - 75598952832*n^3 + 98660630016*n^2 - 73265264640*n + 22992076800)*a(n - 13) + ( - 16*n^12 + 1244*n^11 - 43208*n^10 + 884620*n^9 - 11860728*n^8 + 109396452*n^7 - 709293464*n^6 + 3243764260*n^5 - 10331326456*n^4 + 22203205904*n^3 - 30301280928*n^2 + 23300910720*n - 7504358400)*a(n - 12) + ( - n^14 + 105*n^13 - 5005*n^12 + 143325*n^11 - 2749747*n^10 + 37312275*n^9 - 368411615*n^8 + 2681453775*n^7 - 14409322928*n^6 + 56663366760*n^5 - 159721605680*n^4 + 310989260400*n^3 - 392156797824*n^2 + 283465647360*n - 87178291200)*a(n - 15)). (End)",
				"a(n) = Sum_{d=0..floor(n/2), c=0..floor(n/2-d), b=0..(n-2c-2d), f=0..(n-2c-2d-b), k=0..min(n-b-2c-2d-f, 2n-2f-2b-3c-4d), j=0..floor(k/2+f)} ((-1)^(k+2f-j+d)*n!*(k+2f)!(2(2n-k-2f-2b-3c-4d))!) / (2^(5n-2k-2f-3b-8c-7d) * 3^(n-b-c-2d-k-f)*(2n-k-2f-2b-3c-4d)!*(k+2f-2j)!*j!*b!*c!*d!*k!*f!*(n-b-2c-2d-k-f)!). - _Shanzhen Gao_, Jun 05 2009",
				"E.g.f.: (1+x-(1/3)*x^2-(1/6)*x^3)^(-1/2)*hypergeom([1/4, 3/4],[],-12*x*(x+2)*(x-1)/(x^3+2*x^2-6*x-6)^2)*exp(-x*(x^2-6)/(8*x+16)). - _Mark van Hoeij_, Nov 07 2011",
				"a(n) ~ n^(2*n) * 2^(n+1/2) / (3^n * exp(2*n+15/4)). - _Vaclav Kotesovec_, Mar 11 2014"
			],
			"maple": [
				"egf := (1+x-(1/3)*x^2-(1/6)*x^3)^(-1/2)*hypergeom([1/4, 3/4],[],-12*x*(x+2)*(x-1)/(x^3+2*x^2-6*x-6)^2)*exp(-x*(x^2-6)/(8*x+16));",
				"ser := convert(series(egf,x=0,40),polynom):",
				"seq(coeff(ser,x,i)*i!, i=0..degree(ser)); # _Mark van Hoeij_, Nov 07 2011"
			],
			"mathematica": [
				"max = 17; f[x_] := HypergeometricPFQ[{1/4, 3/4}, {}, -12*x*(x + 2)*(x - 1)/(x^3 + 2*x^2 - 6*x - 6)^2]*Exp[-x*(x^2 - 6)/(8*x + 16)]/(1 + x - x^2/3 - x^3/6)^ (1/2); CoefficientList[Series[f[x], {x, 0, max}], x]*Range[0, max]! (* _Jean-François Alcover_, Jun 19 2012, from e.g.f. *)"
			],
			"xref": [
				"Cf. A005814, A002829, A005816, A272905 (connected). A diagonal of A059441."
			],
			"keyword": "nonn,nice,easy",
			"offset": "0,7",
			"author": "_Simon Plouffe_",
			"ext": [
				"More terms from _Vladeta Jovovic_, Mar 26 2001"
			],
			"references": 13,
			"revision": 61,
			"time": "2021-04-22T17:54:57-04:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}