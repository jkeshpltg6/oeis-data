{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A181418",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 181418,
			"data": "1,4,60,1120,24220,567504,14030016,360222720,9513014940,256758913840,7051260776560,196403499277440,5535202897806400,157551884911456000,4522682234563776000,130783762623673221120,3806221127760278029980",
			"name": "a(n) = A000984(n)*A000172(n), which is the term-wise product of the Central binomial coefficients and Franel numbers, respectively.",
			"comment": [
				"This sequence is s_6 in Cooper's paper. - _Jason Kimberley_, Nov 25 2012",
				"Diagonal of the rational function R(x,y,z,w)=1/(1-(w*x*y+w*z+x*y+x*z+y+z)). - _Gheorghe Coserea_, Jul 13 2016"
			],
			"link": [
				"Jason Kimberley, \u003ca href=\"/A181418/b181418.txt\"\u003eTable of n, a(n) for n = 0..226\u003c/a\u003e",
				"S. Cooper, \u003ca href=\"http://dx.doi.org/10.1007/s11139-011-9357-3\"\u003eSporadic sequences, modular forms and new series for 1/pi\u003c/a\u003e, Ramanujan J. (2012)."
			],
			"formula": [
				"a(n) = C(2n,n) * Sum_{k=0..n} C(n,k)^3.",
				"E.g.f.: Sum_{n\u003e=0} a(n)*x^n/(n!*(2*n)!) = ( Sum_{n\u003e=0} x^n/n!^3 )^2.",
				"1/Pi",
				"= 2/25 Sum {n\u003e=0} a(n)(9n+2)/50^n [Cooper, equation (5)]",
				"= 2/25 Sum {n\u003e=0} a(n)A017185(n)/A165800(n) - _Jason Kimberley_, Nov 26 2012",
				"G.f.: 4*hypergeom([1/6, 1/3],[1],(27/2)*(1+(1-32*x)^(1/2))*(1-(1-32*x)^(1/2))^2/(3+(1-32*x)^(1/2))^3)^2/(3+(1-32*x)^(1/2)). - _Mark van Hoeij_, May 07 2013",
				"Recurrence: n^3*a(n) = 2*(2*n-1)*(7*n^2 - 7*n + 2)*a(n-1) + 32*(n-1)*(2*n-3)*(2*n-1)*a(n-2). - _Vaclav Kotesovec_, Mar 06 2014",
				"a(n) ~ 2^(5*n+1) / (sqrt(3) * (Pi*n)^(3/2)). - _Vaclav Kotesovec_, Mar 06 2014",
				"0 = (-x^2+28*x^3+128*x^4)*y''' + (-3*x+126*x^2+768*x^3)*y'' + (-1+92*x+864*x^2)*y' + (4+96*x)*y, where y is g.f. - _Gheorghe Coserea_, Jul 13 2016"
			],
			"example": [
				"E.g.f.: A(x) = 1 + 4*x/2! + 60*x^2/(2!*4!) + 1120*x^3/(3!*6!) + 24220*x^4/(4!*8!) + 567504*x^5/(5!*10!) +....",
				"where A(x)^(1/2) = 1 + x + x^2/2!^3 + x^3/3!^3 + x^4/4!^3 +x^5/5!^3 +..."
			],
			"mathematica": [
				"Table[Binomial[2n,n]*Sum[Binomial[n,k]^3,{k,0,n}],{n,0,20}] (* _Vaclav Kotesovec_, Mar 06 2014 *)"
			],
			"program": [
				"(PARI) {a(n)=binomial(2*n,n)*sum(k=0,n,binomial(n,k)^3)}",
				"(PARI) {a(n)=(2*n)!*n!*polcoeff(sum(m=0, n, x^m/m!^3+x*O(x^n))^2, n)}"
			],
			"xref": [
				"Cf. A000984, A000172, A199813.",
				"Related to diagonal of rational functions: A268545-A268555."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Paul D. Hanna_, Jan 28 2011",
			"references": 4,
			"revision": 33,
			"time": "2016-07-14T08:33:39-04:00",
			"created": "2010-11-12T14:30:34-05:00"
		}
	]
}