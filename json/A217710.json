{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A217710",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 217710,
			"data": "1,1,1,1,1,1,1,2,1,1,1,1,2,2,2,2,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3",
			"name": "Cardinality of the set of possible heights of AVL trees with n (leaf-) nodes.",
			"comment": [
				"a(n) increases at Fibonacci numbers (A000045) and decreases at powers of 2 plus 1 (A000051) for n\u003e=8.",
				"a(n) is the height (number of nonzero elements) of column n of triangles A143897, A217298."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A217710/b217710.txt\"\u003eTable of n, a(n) for n = 1..50000\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/AVL_tree\"\u003eAVL tree\u003c/a\u003e",
				"\u003ca href=\"/index/Ro#rooted\"\u003eIndex entries for sequences related to rooted trees\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A072649(n) - A029837(n)."
			],
			"example": [
				"a(8) = 2: We have 1 AVL tree with n=8 (leaf-) nodes of height 3 and 16 of height 4 (8 is both Fibonacci number and power of 2):",
				"           o              o",
				"         /   \\          /   \\",
				"       o       o      o       o",
				"      / \\     / )    / \\     / \\",
				"     o   o   o  N   o   o   o   o",
				"    / ) ( ) ( )    ( ) ( ) ( ) ( )",
				"   o  N N N N N    N N N N N N N N",
				"  ( )",
				"  N N"
			],
			"maple": [
				"a:= proc(n) local j, p; for j from ilog[(1+sqrt(5))/2](n)",
				"       while combinat[fibonacci](j+1)\u003c=n do od;",
				"       p:= ilog2(n);",
				"       j-p-`if`(2^p\u003cn, 2, 1)",
				"    end:",
				"seq(a(n), n=1..120);",
				"# second Maple program:",
				"a:= proc(n) option remember; `if`(n=0, 1, a(n-1)+",
				"     `if`((t-\u003e issqr(t+4) or issqr(t-4))(5*n^2), 1, 0)-",
				"     `if`((t-\u003e is(2^ilog2(t)=t))(n-1), 1, 0))",
				"    end:",
				"seq(a(n), n=1..120);  # _Alois P. Heinz_, Aug 14 2021"
			],
			"mathematica": [
				"a[n_] := Module[{j, p}, For[j = Log[(1+Sqrt[5])/2, n] // Floor, Fibonacci[j+1] \u003c= n, j++]; p = Log[2, n] // Floor; j-p-If[2^p \u003c n, 2, 1]]; Table[a[n], {n, 1, 120}] (* _Jean-François Alcover_, Dec 30 2013, translated from Maple *)"
			],
			"xref": [
				"Cf. A000045, A000051, A000079, A029837, A072649, A143897, A217298."
			],
			"keyword": "nonn",
			"offset": "1,8",
			"author": "_Alois P. Heinz_, Mar 20 2013",
			"references": 6,
			"revision": 39,
			"time": "2021-08-14T18:34:24-04:00",
			"created": "2013-03-20T12:11:58-04:00"
		}
	]
}