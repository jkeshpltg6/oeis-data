{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007323",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7323,
			"id": "M1064",
			"data": "1,1,2,4,7,12,23,39,67,118,204,343,592,1001,1693,2857,4806,8045,13467,22464,37396,62194,103246,170963,282828,467224,770832,1270267,2091030,3437839,5646773,9266788,15195070,24896206,40761087,66687201,109032500",
			"name": "The number of numerical semigroups of \"genus\" n; conjecturally also the number of power sum bases for symmetric functions in n variables.",
			"comment": [
				"From Don Zagier's email of Apr 11 1994: (Start)",
				"Given n, one knows that the field of symmetric functions in n variables a_1,...,a_n is the field Q(sigma_1,...,sigma_n), where sigma_i is the i-th elementary symmetric polynomial. Here one has no choice, because sigma_i=0 for i\u003en and fewer than n sigma's would not suffice.",
				"But, by Newton's formulas, the field is also given as Q(s_1,...,s_n) where s_i is the i-th power sum, and now one can ask whether some other sequence s_{j_1},...,s_{j_n} (0\u003cj_1\u003c...\u003cj_n) also works.",
				"For n=1 the only possibility is clearly s_1, since Q(s_i) = Q(a^i) does not coincide with Q(a) for i\u003e1, but for n=2 one has two possibilities Q(s_1,s_2) or Q(s_1,s_3), since from s_1=a+b and s_3=a^3+b^3 one can reconstruct s_2 = (s_1^3+2s_3)/3s_1.",
				"Similarly, for n=3 one has the possibilities (123), (124), (125), and (135) (the formula in the last case is s_2 = (s_1^5+5s_1^2s_3-6s_5)/5(s_1^3-s_3); one can find the corresponding formulas in the other cases easily) and for n=4 there are 7: 1234, 1235, 1236, 1237, 1245, 1247, and 1357.",
				"A theorem of Kakutani (I do not know a reference) says that the sequences which occur are exactly the finite subsets of N whose complements are additive semigroups (for instance, the complement of {1,2,4,7} is 3,5,6,8,9,..., which is closed under addition).",
				"This is a really beautiful theorem. I wrote a simple program to count the sets of cardinality n which have the property in question for n = 1, ..., 16. (End)",
				"This sequence relates to numerical semigroups, which are basic fundamental objects but little known: A numerical semigroup S \u003c N is defined by being: closed under addition, contains zero and N \\ S is finite. [John McKay, Jun 09 2011]",
				"The theorem alluded to in the email by Zagier is due to Kakeya, not Kakutani (see references.) The theorem states that if a sequence of n positive integers k1, k2,..., kn forms the complement of a numerical semigroup, then the power sums p_k1, p_k2,..., p_kn forms a basis for the rational function field of symmetric functions in n variables. Kakeya conjectures that every power sum basis of the symmetric functions has this property, but this is still an open problem. Thanks to user Gjergji Zaimi on Math Overflow for the references. [Trevor Hyde, Oct 18 2018]"
			],
			"reference": [
				"Sean Clark, Anton Preslicka, Josh Schwartz and Radoslav Zlatev, Some combinatorial conjectures on a family of toric ideals: A report from the MSRI 2011 Commutative Algebra graduate workshop.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Maria Bras-Amorós, \u003ca href=\"/A007323/b007323.txt\"\u003eTable of n, a(n) for n = 0..72\u003c/a\u003e [For history of these computations see the Extensions section - _N. J. A. Sloane_, Aug 16 2019]",
				"Matheus Bernardini, \u003ca href=\"https://arxiv.org/abs/1906.07310\"\u003eCounting numerical semigroups by genus and even gaps via Kunz-coordinate vectors\u003c/a\u003e, arXiv:1906.07310 [math.CO], 2019.",
				"Matheus Bernardini and Gilberto Brito, \u003ca href=\"https://arxiv.org/abs/2106.13296\"\u003eOn Pure k-sparse gapsets\u003c/a\u003e, arXiv:2106.13296 [math.CO], 2021.",
				"Matheus Bernardini and Fernando Torres, \u003ca href=\"https://arxiv.org/abs/1612.01212\"\u003eCounting numerical semigroups by genus and even gaps\u003c/a\u003e, arXiv:1612.01212 [math.CO], 2016-2017.",
				"Matheus Bernardini and Fernando Torres, \u003ca href=\"https://doi.org/10.1016/j.disc.2017.08.001\"\u003eCounting numerical semigroups by genus and even gaps\u003c/a\u003e, Discrete Mathematics 340.12 (2017): 2853-2863.",
				"Victor Blanco and Justo Puerto, \u003ca href=\"http://arxiv.org/abs/0901.1228\"\u003eComputing the number of numerical semigroups using generating functions\u003c/a\u003e, arXiv:0901.1228 [math.CO], 2009.",
				"Maria Bras-Amoros, \u003ca href=\"http://crises-deim.urv.cat/~mbras/\"\u003eHome Page\u003c/a\u003e [Has many of these references]",
				"M. Bras-Amoros, \u003ca href=\"http://www.singacom.uva.es/oldsite/seminarios/WorkshopSG/workshop2/Bras_SG_2007.pdf\"\u003eAlgebraic Geometry, Coding and Computing \u003c/a\u003e, Segovia, Spain, 2007.",
				"M. Bras-Amoros, \u003ca href=\"https://cmup.fc.up.pt/cmup/ASA/numsgps_meeting/schedule_slides.html\"\u003eIMNS 2018 \u003c/a\u003e, Granada, Spain, 2008.",
				"M. Bras-Amoros, \u003ca href=\"http://dx.doi.org/10.1007/s00233-007-9014-8\"\u003eFibonacci-Like Behavior of the Number of Numerical Semigroups of a Given Genus\u003c/a\u003e, Semigroup Forum, 76 (2008), 379-384. See \u003ca href=\"https://arxiv.org/abs/1706.05230\"\u003ealso\u003c/a\u003e, arXiv:1706.05230 [math.NT], 2017.",
				"M. Bras-Amoros, \u003ca href=\"http://dx.doi.org/10.1016/j.jpaa.2008.11.012\"\u003eBounds on the Number of Numerical Semigroups of a Given Genus\u003c/a\u003e, Journal of Pure and Applied Algebra, vol. 213, n. 6 (2009), pp. 997-1001. arXiv:0802.2175.",
				"M. Bras-Amoros and S. Bulygin, \u003ca href=\"http://arxiv.org/abs/0810.1619\"\u003eTowards a Better Understanding of the Semigroup Tree\u003c/a\u003e, arXiv:0810.1619 [math.CO], 2008; \u003ca href=\"http://dx.doi.org/10.1007/s00233-009-9175-8\"\u003eSemigroup Forum 79 (2009) 561-574\u003c/a\u003e.",
				"M. Bras-Amoros and A. de Mier, \u003ca href=\"http://arxiv.org/abs/math/0612634\"\u003eRepresentation of Numerical Semigroups by Dyck Paths\u003c/a\u003e, arXiv:math/0612634 [math.CO], 2006; \u003ca href=\"http://dx.doi.org/10.1007/s00233-007-0717-7\"\u003eSemigroup Forum 75 (2007) 676-681\u003c/a\u003e.",
				"Maria Bras-Amorós and J. Fernández-González, \u003ca href=\"https://arxiv.org/abs/1607.01545\"\u003earXiv version\u003c/a\u003e, arXiv:1607.01545 [math.CO], 2016-2017; \u003ca href=\"https://doi.org/10.1090/mcom/3292\"\u003eComputation of numerical semigroups by means of seeds\u003c/a\u003e, Mathematics of Computation 87 (313), American Mathematical Society, 2539-2550, September 2018.",
				"Maria Bras-Amorós and J. Fernández-González, \u003ca href=\"https://arxiv.org/abs/1911.03173\"\u003eThe right-generators descendant of a numerical semigroup\u003c/a\u003e, arXiv:1911.03173 [math.CO], 2019. To appear in Mathematics of Computation, American Mathematical Society, 2020.",
				"CombOS - Combinatorial Object Server, \u003ca href=\"http://combos.org/sgroup.html\"\u003eGenerate numerical semigroups\u003c/a\u003e",
				"Shalom Eliahou and Jean Fromentin, \u003ca href=\"http://images.math.cnrs.fr/Semigroupes-numeriques-et-nombre-d-or-I.html\"\u003eSemigroupes Numériques et Nombre d’or\u003c/a\u003e, Images des Mathématiques, CNRS, 2018. In French.",
				"Sergi Elizalde, \u003ca href=\"http://arxiv.org/abs/0905.0489\"\u003eImproved bounds on the number of numerical semigroups of a given genus\u003c/a\u003e, arXiv:0905.0489 [math.CO], 2009. [Maria Bras-Amoros, Sep 01 2009]",
				"G. Failla, C. Peterson, and R. Utano, \u003ca href=\"http://dx.doi.org/10.1007/s00233-015-9690-8\"\u003eAlgorithms and basic asymptotics for generalized numerical semigroups in N^d\u003c/a\u003e, Semigroup Forum, Jan 31 2015, DOI 10.1007/s00233-015-9690-8.",
				"S. R. Finch, \u003ca href=\"http://www.people.fas.harvard.edu/~sfinch/\"\u003eMonoids of natural numbers\u003c/a\u003e",
				"S. R. Finch, \u003ca href=\"/A066062/a066062.pdf\"\u003eMonoids of natural numbers\u003c/a\u003e, March 17, 2009. [Cached copy, with permission of the author]",
				"Jean Fromentin, \u003ca href=\"http://arxiv.org/abs/1305.3831\"\u003eExploring the tree of numerical semigroups\u003c/a\u003e, arXiv:1305.3831 [math.CO], 2013-2015. See \u003ca href=\"https://hal.archives-ouvertes.fr/hal-00823339\"\u003ealso\u003c/a\u003e, hal-00823339.",
				"Jean Fromentin and Shalom Eliahou, \u003ca href=\"http://images.math.cnrs.fr/Semigroupes-numeriques-et-nombre-d-or-II.html\"\u003eSemigroupes numériques et nombre d’or (II)\u003c/a\u003e, (in French), Images des Mathématiques, CNRS, 2018.",
				"Florent Hivert, \u003ca href=\"https://dx.doi.org/10.1145/3115936.3115938\"\u003eHigh Performance Computing Experiments in Enumerative and Algebraic Combinatorics\u003c/a\u003e, PASCO 2017 Proceedings of the International Workshop on Parallel Symbolic Computation, Article No. 2.",
				"Trevor Hyde, \u003ca href=\"https://mathoverflow.net/questions/310210/reference-for-kakutani-result-on-power-sum-bases-of-symmetric-functions\"\u003eMath Overflow post on reference for result mentioned in Zagier's email.\u003c/a\u003e",
				"S. Kakeya, \u003ca href=\"https://doi.org/10.4099/jjm1924.2.0_69\"\u003eOn fundamental systems of symmetric functions\u003c/a\u003e, Jap. J. Math., 2, (1925), 69-80.",
				"S. Kakeya, \u003ca href=\"https://doi.org/10.4099/jjm1924.4.0_77\"\u003eOn fundamental systems of symmetric functions II\u003c/a\u003e, Jap. J. Math., 4, (1927), 77-85.",
				"Nathan Kaplan, \u003ca href=\"https://arxiv.org/abs/1707.02551\"\u003eCounting Numerical Semigroups\u003c/a\u003e, arXiv:1707.02551 [math.CO], 2017. Also Amer. Math. Monthly, 124 (2017), 862-875.",
				"Jiryo Komeda, \u003ca href=\"http://dx.doi.org/10.1007/PL00005972\"\u003eNon-Weierstrass numerical semigroups\u003c/a\u003e. Semigroup Forum 57 (1998), no. 2, 157-185. [Maria Bras-Amoros, Sep 01 2009]",
				"Nivaldo Medeiros, \u003ca href=\"http://www.impa.br/~nivaldo/algebra/semigroups/index.html\"\u003eNumerical Semigroups\u003c/a\u003e",
				"Alex Zhai, \u003ca href=\"https://doi.org/10.1007/s00233-012-9456-5\"\u003eFibonacci-like growth of numerical semigroups of a given genus\u003c/a\u003e, Semigroup Forum, 86 (2013), 634-662. See also, arXiv:\u003ca href=\"https://arxiv.org/abs/1111.3142\"\u003e1111.3142\u003c/a\u003e [math.CO], 2011.",
				"Y. Zhao, \u003ca href=\"http://dx.doi.org/10.1007/s00233-009-9190-9\"\u003eConstructing numerical semigroups of a given genus\u003c/a\u003e, Semigroup Forum 80 (2010) 242-254.",
				"\u003ca href=\"/index/Se#semigroups\"\u003eIndex entries for sequences related to semigroups\u003c/a\u003e"
			],
			"formula": [
				"Conjectures: A) a(n) \u003e= a(n-1)+a(n-2); B) a(n)/(a(n-1)+a(n-2)) approaches 1; C) a(n)/a(n-1) approaches the golden ratio;  D) a(n) \u003e= a(n-1). Conjectures A, B, C, D were presented by M. Bras-Amorós in the seminar Algebraic Geometry, Coding and Computing, in Segovia, Spain, in 2007, and at IMNS 2018 in Granada, Spain, in 2008. Conjectures A, B, C were then published in the Semigroup Forum, 76 (2008), 379-384. Conjectures B and C are proved in by Zhai, 2011. - _Maria Bras-Amorós_, Oct 24 2007, corrected Aug 31 2009"
			],
			"example": [
				"G.f. = x + 2*x^2 + 4*x^3 + 7*x^4 + 12*x^5 + 23*x^6 + 39*x^7 + 67*x^8 + ...",
				"a(1) = 1 because the unique numerical semigroup with genus 1 is N \\ {1}",
				"a(3) = 4 because the four numerical semigroups with genus 3 are N \\ {1,2,3}, N \\ {1,2,4}, N \\ {1,2,5}, and N \\ {1,3,5}"
			],
			"xref": [
				"Row sums of A199711. [corrected by _Jonathan Sondow_, Nov 05 2017]"
			],
			"keyword": "nonn,nice",
			"offset": "0,3",
			"author": "Don Zagier (don.zagier(AT)mpim-bonn.mpg.de), Apr 11 1994",
			"ext": [
				"The terms from a(17) to a(52) were contributed (in the context of semigroups) by Maria Bras-Amoros (maria.bras(AT)gmail.com), Oct 24 2007. The computations were done with the help of Jordi Funollet and Josep M. Mondelo.",
				"Terms a(53)-a(60) were taken from the Fromentin (2013) paper. - _N. J. A. Sloane_, Sep 05 2013",
				"Terms a(61) to a(70) were taken from https://github.com/hivert/NumericMonoid.",
				"Terms a(71) and a(72) were computed by J. Fernández-González and _Maria Bras-Amorós_."
			],
			"references": 6,
			"revision": 160,
			"time": "2021-10-19T15:23:52-04:00",
			"created": "1994-04-28T03:00:00-04:00"
		}
	]
}