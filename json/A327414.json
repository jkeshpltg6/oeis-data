{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A327414",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 327414,
			"data": "0,2,6,20,56,120,792,364,560,8568,1140,1540,42504,2600,98280,2035800,4960,5984,376992,12620256,9880,850668,13244,15180,1712304,19600,2598960,177100560,27720,4582116,386206920,37820,41664,8936928,969443904,54740,13991544",
			"name": "Minimal prime partition representation of even integers.",
			"comment": [
				"A partition is prime if all parts are primes. A partition of an even integer n \u003e 2 is minimal if it has at most two parts, one of which is the greatest prime less than n - 1. The terms of the sequence are the multinomials of these partition. By convention a(0) = 0 and a(1) = 2."
			],
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PrimePartition.html\"\u003ePrime Partition\u003c/a\u003e"
			],
			"formula": [
				"For n \u003e= 2 let a(n) be the multinomial of P where P is the partition [p, 2n - p] with p the greatest prime less than 2n - 1."
			],
			"example": [
				"n   2n  partition a(n)",
				"2   4 :  [2,  2]   6",
				"3   6 :  [3,  3]   20",
				"4   8 :  [5,  3]   56",
				"5   10:  [7,  3]   120",
				"6   12:  [7,  5]   792",
				"7   14:  [11, 3]   364",
				"8   16:  [13, 3]   560",
				"9   18:  [13, 5]   8568",
				"10  20:  [17, 3]   1140",
				"11  22:  [19, 3]   1540",
				"12  24:  [19, 5]   42504",
				"13  26:  [23, 3]   2600",
				"14  28:  [23, 5]   98280",
				"15  30:  [23, 7]   2035800"
			],
			"program": [
				"(SageMath)",
				"def a(n):",
				"    if n \u003c 2: return 2*n",
				"    p = previous_prime(2*n - 1)",
				"    return multinomial([p, 2*n - p])",
				"print([a(n) for n in range(40)])"
			],
			"xref": [
				"Cf. A327413."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Peter Luschny_, Sep 07 2019",
			"references": 1,
			"revision": 23,
			"time": "2020-02-26T07:16:04-05:00",
			"created": "2019-09-09T12:01:42-04:00"
		}
	]
}