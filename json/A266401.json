{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A266401",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 266401,
			"data": "1,2,5,4,3,10,17,8,13,6,11,20,9,34,71,16,7,26,19,12,23,22,21,40,41,18,227,68,31,142,29,32,53,14,67,52,61,38,107,24,25,46,59,44,65,42,73,80,49,82,197,36,33,454,55,136,137,62,43,284,37,58,571,64,45,106,35,28,89,134,15,104,47",
			"name": "Self-inverse permutation of natural numbers: a(n) = A064989(A263273(A003961(n))).",
			"comment": [
				"Shift primes in the prime factorization of n one step towards larger primes (A003961), then apply the bijective base-3 reverse (A263273) to the resulting odd number, which yields another (or same) odd number, then shift primes in the prime factorization of that second odd number one step back towards smaller primes (A064989)."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A266401/b266401.txt\"\u003eTable of n, a(n) for n = 1..6560\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A064989(A263273(A003961(n))).",
				"As a composition of related permutations:",
				"a(n) = A064216(A264996(A048673(n))).",
				"Other identities. For all n \u003e= 0:",
				"A000035(a(n)) = A000035(n). [This permutation preserves the parity of n.]"
			],
			"mathematica": [
				"f[n_] := Block[{g, h}, g[x_] := x/3^IntegerExponent[x, 3]; h[x_] := x/g@ x; If[n == 0, 0, FromDigits[Reverse@ IntegerDigits[#, 3], 3] \u0026@ g[n] h[n]]]; g[p_?PrimeQ] := g[p] = Prime[PrimePi@ p + 1]; g[1] = 1; g[n_] := g[n] = Times @@ (g[First@ #]^Last@ # \u0026) /@ FactorInteger@ n; h[n_] := Times @@ Power[Which[# == 1, 1, # == 2, 1, True, NextPrime[#, -1]] \u0026 /@ First@ #, Last@ #] \u0026@ Transpose@ FactorInteger@ n; Table[h@ f@ g@ n, {n, 82}] (* _Michael De Vlieger_, Jan 04 2016, after _Jean-François Alcover_ at A003961 and A263273 *)"
			],
			"program": [
				"(PARI)",
				"A030102(n) = { my(r=[n%3]); while(0\u003cn\\=3, r=concat(n%3, r)); subst(Polrev(r),x,3); } \\\\ After _M. F. Hasler_'s Nov 04 2011 code in A030102.",
				"A263273 = n -\u003e if(!n,n,A030102(n/(3^valuation(n,3))) * (3^valuation(n, 3))); \\\\ Taking of the quotient probably unnecessary.",
				"A003961(n) = my(f = factor(n)); for (i=1, #f~, f[i, 1] = nextprime(f[i, 1]+1)); factorback(f); \\\\ Using code of Michel Marcus",
				"A064989(n) = {my(f); f = factor(n); if((n\u003e1 \u0026\u0026 f[1,1]==2), f[1,2] = 0); for (i=1, #f~, f[i,1] = precprime(f[i,1]-1)); factorback(f)};",
				"A266401 = n -\u003e A064989(A263273(A003961(n)));",
				"for(n=1, 6560, write(\"b266401.txt\", n, \" \", A266401(n)));",
				"(Scheme) (define (A266401 n) (A064989 (A263273 (A003961 n))))"
			],
			"xref": [
				"Cf. A000035, A003961, A064989, A263273.",
				"Cf. A265369, A265904, A266190, A266403 (other conjugates or similar sequences derived from A263273).",
				"Cf. also A048673, A064216, A264996, A266402, A266407, A266408."
			],
			"keyword": "nonn,base",
			"offset": "1,2",
			"author": "_Antti Karttunen_, Jan 02 2016",
			"references": 9,
			"revision": 19,
			"time": "2016-01-09T14:27:14-05:00",
			"created": "2016-01-09T14:27:14-05:00"
		}
	]
}