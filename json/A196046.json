{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A196046",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 196046,
			"data": "0,1,2,2,2,2,3,3,2,2,2,3,3,3,2,4,3,3,4,3,3,2,3,4,2,3,3,3,3,3,2,5,2,3,3,4,4,4,3,4,3,3,3,3,3,3,3,5,3,3,3,3,5,4,2,4,4,3,3,4,4,2,3,6,3,3,4,3,3,3,4,5,3,4,3,4,3,3,3,5,4,3,3,4,3,3,3,4,5,4,3,3,2,3,4,6,3,3,3,4,3,3,4,4,3,5,4,5,3,3",
			"name": "Maximum vertex-degree in the rooted tree with Matula-Goebel number n.",
			"comment": [
				"The Matula-Goebel number of a rooted tree is defined in the following recursive manner: to the one-vertex tree there corresponds the number 1; to a tree T with root degree 1 there corresponds the t-th prime number, where t is the Matula-Goebel number of the tree obtained from T by deleting the edge emanating from the root; to a tree T with root degree m\u003e=2 there corresponds the product of the Matula-Goebel numbers of the m branches of T."
			],
			"reference": [
				"F. Goebel, On a 1-1-correspondence between rooted trees and natural numbers, J. Combin. Theory, B 29 (1980), 141-143.",
				"I. Gutman and A. Ivic, On Matula numbers, Discrete Math., 150, 1996, 131-142.",
				"I. Gutman and Yeong-Nan Yeh, Deducing properties of trees from their Matula numbers, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"D. W. Matula, A natural rooted tree enumeration by prime factorization, SIAM Review, 10, 1968, 273."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A196046/b196046.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"E. Deutsch, \u003ca href=\"http://arxiv.org/abs/1111.4288\"\u003eTree statistics from Matula numbers\u003c/a\u003e, arXiv preprint arXiv:1111.4288, 2011",
				"\u003ca href=\"/index/Mat#matula\"\u003eIndex entries for sequences related to Matula-Goebel numbers\u003c/a\u003e"
			],
			"formula": [
				"a(1)=0; if n=p_t (=the t-th prime), then a(n) = max(a(t), 1+G(t)); if n=rs (r,s\u003e=2), then a(n)=max(a(r),a(s), G(r)+G(s)); G(m) is the number of prime divisors of m counted with multiplicity. The Maple program is based on this recursive formula.",
				"The Gutman et al. references contain a different recursive formula."
			],
			"example": [
				"a(7)=3 because the rooted tree with Matula-Goebel number 7 is the rooted tree Y.",
				"a(2^m) = m because the rooted tree with Matula-Goebel number 2^m is a star with m edges."
			],
			"maple": [
				"with(numtheory): a := proc (n) local r, s: r := proc (n) options operator, arrow: op(1, factorset(n)) end proc: s := proc (n) options operator, arrow: n/r(n) end proc: if n = 1 then 0 elif bigomega(n) = 1 then max(a(pi(n)), 1+bigomega(pi(n))) else max(a(r(n)), a(s(n)), bigomega(r(n))+bigomega(s(n))) end if end proc: seq(a(n), n = 1 .. 110);"
			],
			"program": [
				"(Haskell)",
				"import Data.List (genericIndex)",
				"a196046 n = genericIndex a196046_list (n - 1)",
				"a196046_list = 0 : g 2 where",
				"  g x = y : g (x + 1) where",
				"    y | t \u003e 0     = max (a196046 t) (a001222 t + 1)",
				"      | otherwise = maximum [a196046 r, a196046 s, a001222 r + a001222 s]",
				"      where t = a049084 x; r = a020639 x; s = x `div` r",
				"-- _Reinhard Zumkeller_, Sep 03 2013"
			],
			"xref": [
				"Cf. A049084, A020639, A001222."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Emeric Deutsch_, Sep 26 2011",
			"references": 3,
			"revision": 17,
			"time": "2017-03-07T06:19:00-05:00",
			"created": "2011-09-26T21:39:11-04:00"
		}
	]
}