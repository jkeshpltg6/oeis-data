{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A214697",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 214697,
			"data": "2,3,5,17,7,2,89,125,3,215,269,13,10,8,11,27,719,815,21,57,316,11,26,1517,17,1799,30,26,7,5,2609,11,2975,10,2,76,3779,1251,208,4445,115,4919,1045,5417,11,17,1205,6485,38,2860,7349,18,25,8267,8585,8909",
			"name": "Least k \u003e 1 such that tri(n)+ ... + tri(n+k-1) is a triangular number.",
			"comment": [
				"tri(n) = n*(n+1)/2 is the n-th triangular number, A000217(n).",
				"a(n) is how many consecutive triangular numbers starting from tri(n) are needed to sum up to tri(x) for some x. The requirement a(n) \u003e 1 is needed, because otherwise all a(n) = 1.",
				"Because an oblong number (A002378) is twice a triangular number, this sequence is also the least k \u003e 1 such that oblong(n) + ... + oblong(n+k-1) is an oblong number.",
				"a(n) is least k \u003e 1 such that 12*k^3 + 36*n*k^2 + 36*k*n^2 - 12*k + 9 is a perfect square. - _Chai Wah Wu_, Mar 01 2016",
				"a(n) \u003c= 3*n^2 - 3*n - 1 for n \u003e 1, since 12*k^3 + 36*n*k^2 + 36*k*n^2 - 12*k + 9 is a square when k = 3*n^2 - 3*n - 1. - _Robert Israel_, Mar 03 2016"
			],
			"link": [
				"Chai Wah Wu, \u003ca href=\"/A214697/b214697.txt\"\u003eTable of n, a(n) for n = 0..5000\u003c/a\u003e"
			],
			"example": [
				"0+1 = 1 is a triangular number, two summands, so a(0)=2.",
				"1+3+6 = 10 is a triangular number, three summands, so a(1)=3.",
				"3+6+10+15+21 = 55 is a triangular number, five summands, so a(2)=5.",
				"Starting from Triangular(5)=15:  15+21=36 is a triangular number, two summands, so a(5)=2."
			],
			"maple": [
				"f:= proc(n) local k;",
				"    for k from 2 do if issqr(12*k^3+36*k^2*n+36*k*n^2-12*k+9) then return k fi od",
				"end proc:",
				"map(f, [$0..100]); # _Robert Israel_, Mar 03 2016"
			],
			"mathematica": [
				"triQ[n_] := IntegerQ[Sqrt[1+8*n]]; Table[k = n+1; s = k^2; While[! triQ[s], k++; s = s + k*(k+1)/2]; k - n + 1, {n, 0, 55}] (* _T. D. Noe_, Jul 26 2012 *)"
			],
			"program": [
				"(Python)",
				"for n in range(77):",
				"    i = ti = n",
				"    sum = 0",
				"    tn_gte_sum = 0  # least oblong  number \u003e= sum",
				"    while i-n\u003c=1 or tn_gte_sum!=sum:",
				"        sum += i*(i+1)",
				"        i+=1",
				"        while tn_gte_sum\u003csum:",
				"            tn_gte_sum = ti*(ti+1)",
				"            ti+=1",
				"    print i-n,",
				"(Python)",
				"from math import sqrt",
				"def A214697(n):",
				"    k, a1, a2, m = 2, 36*n, 36*n**2 - 12, n*(72*n + 144) + 81",
				"    while int(round(sqrt(m)))**2 != m:",
				"        k += 1",
				"        m = k*(k*(12*k + a1) + a2) + 9",
				"    return k # _Chai Wah Wu_, Mar 01 2016"
			],
			"xref": [
				"Cf. A000217, A000292, A214648, A214696."
			],
			"keyword": "nonn,look",
			"offset": "0,1",
			"author": "_Alex Ratushnyak_, Jul 26 2012",
			"references": 2,
			"revision": 30,
			"time": "2016-03-03T22:52:26-05:00",
			"created": "2012-07-26T13:23:51-04:00"
		}
	]
}