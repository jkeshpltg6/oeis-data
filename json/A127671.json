{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A127671",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 127671,
			"data": "1,1,-1,1,-3,2,1,-4,-3,12,-6,1,-5,-10,20,30,-60,24,1,-6,-15,-10,30,120,30,-120,-270,360,-120,1,-7,-21,-35,42,210,140,210,-210,-1260,-630,840,2520,-2520,720,1,-8,-28,-56,-35,56,336,560,420,560,-336,-2520,-1680,-5040,-630,1680,13440,10080,-6720",
			"name": "Cumulant expansion numbers: Coefficients in expansion of log(1 + Sum_{k\u003e=1} x[k]*(t^k)/k!).",
			"comment": [
				"Connected objects from general (disconnected) objects.",
				"The row lengths of this array is p(n):=A000041(n) (partition numbers).",
				"In row n the partitions of n are taken in the Abramowitz-Stegun order.",
				"One could call the unsigned numbers |a(n,k)| M_5 (similar to M_0, M_1, M_2, M_3 and M_4 given in A111786, A036038, A036039, A036040 and A117506, resp.).",
				"The inverse relation (disconnected from connected objects) is found in A036040.",
				"(d/da(1))p_n[a(1),a(2),...,a(n)] = n b_(n-1)[a(1),a(2),...,a(n-1)], where p_n are the partition polynomials of the cumulant generator A127671 and b_n are the partition polynomials for A133314. - _Tom Copeland_, Oct 13 2012",
				"See notes on relation to Appell sequences in a differently ordered version of this array A263634. - _Tom Copeland_, Sep 13 2016",
				"Given a binomial Sheffer polynomial sequence defined by the e.g.f. exp[t * f(x)] = Sum_{n \u003e= 0} p_n(t) * x^n/n!, the cumulants formed from these polynomials are the Taylor series coefficients of f(x) multipied by t. An example is the sequence of the Stirling polynomials of the first kind A008275 with f(x) = log(1+x), so the n-th cumulant is (-1)^(n-1) * (n-1)! * t. - _Tom Copeland_, Jul 25 2019",
				"From _Tom Copeland_, Oct 15 2020: (Start)",
				"With a_n = n! * b_n = (n-1)! * c_n for n  \u003e 0, represent a function with f(0) = a_0 = b_0 = 1 as an",
				"A) exponential generating function (e.g.f), or formal Taylor series: f(x) = e^{a.x} = 1 + Sum_{n \u003e 0} a_n * x^n/n!",
				"B) ordinary generating function (o.g.f.), or formal power series: f(x)  = 1/(1-b.x) = 1 + Sum_{n \u003e 0}  b_n * x^n",
				"C) logarithmic generating function (l.g.f): f(x) = 1 - log(1 - c.x) = 1 + Sum_{n \u003e 0}  c_n * x^n /n.",
				"Expansions of log(f(x)) are given in",
				"I) A127671 and A263634 for the e.g.f: log[ e^{a.*x} ] =  e^{L.(a_1,a_2,...)x} = Sum_{n \u003e 0} L_n(a_1,...,a_n) * x^n/n!, the logarithmic polynomials, cumulant expansion polynomials",
				"II) A263916 for the o.g.f.: log[ 1/(1-b.x) ] =  log[ 1 - F.(b_1,b_2,...)x ] = -Sum_{n \u003e 0} F_n(b_1,...,b_n) * x^n/n, the Faber polynomials.",
				"Expansions of exp(f(x)-1) are given in",
				"III) A036040 for an e.g.f: exp[ e^{a.x} - 1 ] = e^{BELL.(a_1,...)x}, the Bell/Touchard/exponential partition polynomials, a.k.a. the Stirling partition polynomials of the second kind",
				"IV) A130561 for an o.g.f.: exp[ b.x/(1-b.x) ] = e^{LAH.(b.,...)x}, the Lah partition polynomials",
				"V) A036039 for an l.g.f.: exp[ -log(1-c.x) ] =  e^{CIP.(c_1,...)x}, the cycle index polynomials of the symmetric groups S_n, a.k.a. the Stirling partition polynomials of the first kind.",
				"Since exp and log are a compositional inverse pair, one can extract the indeterminates of the log set of partition polynomials from the exp set and vice versa. For a discussion of the relations among these polynomials and the combinatorics of connected and disconnected graphs/maps, see Novak and LaCroix on classical moments and cumulants and the two books on statistical mechanics referenced in A036040. (End)",
				" Ignoring signs, these polynomials appear in Schröder in the set of equations (II) on p. 343 and in Stewart's translation on p. 31. - _Tom Copeland_, Aug 25 2021"
			],
			"reference": [
				"C. Itzykson and J.-M. Drouffe, Statistical field theory, vol. 2, p. 413, eq.(13), Cambridge University Press, (1989)."
			],
			"link": [
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards Applied Math. Series 55, Tenth Printing, 1972, pp. 831-2.",
				"Tom Copeland, \u003ca href=\"http://tcjpn.wordpress.com/2015/11/21/the-creation-raising-operators-for-appell-sequences/\"\u003eThe creation / raising operators for Appell sequences\u003c/a\u003e.",
				"Wolfdieter Lang, \u003ca href=\"/A127671/a127671.pdf\"\u003eFirst 10 rows of cumulant numbers and polynomials\u003c/a\u003e.",
				"E. Schröder, \u003ca href=\"http://www.digizeitschriften.de/dms/img/?PID=GDZPPN002240548\"\u003eUeber unendlich viele Algorithmen zur Auflösung der Gleichungen\u003c/a\u003e, Mathematische Annalen vol. 2, 317-365, 1870.",
				"G. Stewart, \u003ca href=\"https://drum.lib.umd.edu/handle/1903/577\"\u003eOn infinitely many algorithms for solving equations\u003c/a\u003e, 1993, (translation into English of Schröder's paper above)."
			],
			"formula": [
				"E.g.f. for multivariate row polynomials A(t) := log(1 + Sum_{k\u003e=1} x[k]*(t^k)/k!).",
				"Row n polynomial p_n(x[1],...,x[n]) = [(t^n)/n!]A(t).",
				"a(n,m) = A264753(n, m) * M_3(n,m) with M_3(n,m) = A036040(n,m) (Abramowitz-Stegun M_3 numbers). - corrected by _Johannes W. Meijer_, Jul 12 2016",
				"p_n(x[1],...,x[n]) = -(n-1)!*F(n,x[1],x[2]/2!,..,x[n]/n!) in terms of the Faber polynomials F(n,b1,..,bn) of A263916. - _Tom Copeland_, Nov 17 2015",
				"With D = d/dz and M(0) = 1, the differential operator R = z + d(log(M(D))/dD = z + d(log(1 + x[1] D + x[2] D^2/2! + ...))/dD = z + p.*exp(p.D) = z + Sum_{n\u003e=0} p_(n+1)(x[1],..,x[n]) D^n/n! is the raising operator for the Appell sequence A_n(z) = (z + x[.])^n = Sum_{k=0..n} binomial(n,k) x[n-k] z^k with the e.g.f. M(t) e^(zt), i.e., R A_n(z) = A_(n+1)(z) and dA_n(z)/dz = n A_(n-1)(z). The operator Q = z - p.*exp(p.D) generates the Appell sequence with e.g.f. e^(zt) / M(t). - _Tom Copeland_, Nov 19 2015"
			],
			"example": [
				"Row n=3: [1,-3,2] stands for the polynomial 1*x[3] - 3*x[1]*x[2] + 2*x[1]^3 (the Abramowitz-Stegun order of the p(3)=3 partitions of n=3 is [3],[1,2],[1^3])."
			],
			"xref": [
				"Cf. A133314, A263916, A263634.",
				"Cf. A008275.",
				"Cf. A036039, A036040, A130561."
			],
			"keyword": "sign,easy,tabf",
			"offset": "1,5",
			"author": "_Wolfdieter Lang_, Jan 23 2007",
			"references": 17,
			"revision": 67,
			"time": "2021-10-02T16:19:22-04:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}