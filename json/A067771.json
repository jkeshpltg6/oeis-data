{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A067771",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 67771,
			"data": "3,6,15,42,123,366,1095,3282,9843,29526,88575,265722,797163,2391486,7174455,21523362,64570083,193710246,581130735,1743392202,5230176603,15690529806,47071589415,141214768242,423644304723,1270932914166",
			"name": "Number of vertices in Sierpiński triangle of order n.",
			"comment": [
				"This sequence represents another link from the product factor space Q X Q / {(1,1), (-1, -1)} to Sierpiński's triangle. The first \"link\" found was to sequence A048473. - _Creighton Dement_, Aug 05 2004",
				"a(n) equals the number of orbits of the finite group PSU(3,3^n) on subsets of size 3 of the 3^(3n)+1 isotropic points of a unitary 3 space. - _Paul M. Bradley_, Jan 31 2017",
				"For n\u003e=1, number of edges in a planar Apollonian graph at iteration n. - _Andrew D. Walker_, Jul 08 2017"
			],
			"reference": [
				"Peter Wessendorf and Kristina Downing, personal communication."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A067771/b067771.txt\"\u003eTable of n, a(n) for n = 0..600\u003c/a\u003e",
				"Paul Bradley and Peter Rowley, \u003ca href=\"http://eprints.ma.man.ac.uk/2167/01/covered/MIMS_ep2014_42.pdf\"\u003eOrbits on k-subsets of 2-transitive Simple Lie-type Groups\u003c/a\u003e, 2014.",
				"András Kaszanyitzky, \u003ca href=\"https://arxiv.org/abs/1710.09475\"\u003eTriangular fractal approximating graphs and their covering paths and cycles\u003c/a\u003e, arXiv:1710.09475 [math.CO], 2017. See Table 2.",
				"C. Lanius, \u003ca href=\"http://math.rice.edu/~lanius/fractals/\"\u003eFractals\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/SierpinskiGraph.html\"\u003eSierpiński Graph\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-3)."
			],
			"formula": [
				"a(n) = 3 + 3^1 + 3^2 + 3^3 + 3^4 +...+ 3^n = 3 + Sum_{k=1..n} 3^n.",
				"a(n) = 3*A007051(n).",
				"a(0) = 3, a(n) = a(n-1) + 3^n. a(n) = (3/2)*(1+3^n). - _Zak Seidov_, Mar 19 2007",
				"a(n) = 4*a(n-1) - 3*a(n-2).",
				"G.f.: 3*(1-2*x)/((1-x)*(1-3*x)). - _Colin Barker_, Jan 10 2012",
				"a(n) = A233774(2^n). - _Omar E. Pol_, Dec 16 2013",
				"a(n) = 3*a(n-1) - 3. - _Zak Seidov_, Oct 26 2014",
				"E.g.f.: 3*(exp(x) + exp(3*x))/2. - _Stefano Spezia_, Feb 09 2021"
			],
			"mathematica": [
				"LinearRecurrence[{4, -3}, {3, 6}, 26] (* or *)",
				"CoefficientList[Series[3 (1 - 2 x)/((1 - x) (1 - 3 x)), {x, 0, 25}], x] (* _Michael De Vlieger_, Feb 02 2017 *)"
			],
			"program": [
				"(MAGMA) [(3/2)*(1+3^n): n in [0..30]]; // _Vincenzo Librandi_, Jun 20 2011"
			],
			"xref": [
				"Cf. A048473.",
				"Cf. A003462, A007051, A034472, A024023. - _Vladimir Joseph Stephan Orlovsky_, Dec 25 2008"
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "Martin Wessendorf (martinw(AT)mail.ahc.umn.edu), Feb 09 2002",
			"ext": [
				"More terms from _Benoit Cloitre_, Feb 22 2002"
			],
			"references": 19,
			"revision": 76,
			"time": "2021-08-04T07:17:17-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}