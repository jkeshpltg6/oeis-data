{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A342011",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 342011,
			"data": "1,2,1,3,1,2,4,1,5,6,1,2,3,7,8,9,1,10,11,4,2,12,13,14,1,15,16,17,3,18,19,20,21,5,22,1,23,2,24,25,6,26,27,28,29,30,31,32,33,34,1,35,36,4,37,38,39,7,40,41,42,43,44,45,46,8,47,2,48,49,50,3,51,52,53,54,1,55,56,57,58,59,60,61,62,9,63,64",
			"name": "Lexicographically earliest infinite sequence such that a(i) = a(j) =\u003e f(i) = f(j), for all i, j \u003e= 1, with f(1) = 2 and f(n) = A004490(n)/A004490(n-1) when n \u003e 1, where A004490(n) is the n-th colossally abundant number.",
			"comment": [
				"This is also the restricted growth sequence transform of A073751, provided that quotient A004490(1+n)/A004490(n) is always prime, which is implied by a conjecture mentioned in Lagarias' paper. Note that the b-file of A073751 is computed based on the knowledge that the conjecture holds at least for the first 10^7 quotients."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A342011/b342011.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (computed from the b-file of A073751 provided by T. D. Noe)",
				"J. C. Lagarias, \u003ca href=\"https://arxiv.org/abs/math/0008177\"\u003eAn elementary problem equivalent to the Riemann hypothesis\u003c/a\u003e, arXiv:math/0008177 [math.NT], 2000-2001; Am. Math. Monthly 109 (#6, 2002), 534-543."
			],
			"formula": [
				"a(n) = A000720(A073751(n)), up to the first n where A004490(n)/A004490(n-1) is not a prime."
			],
			"program": [
				"(PARI)",
				"rgs_transform(invec) = { my(om = Map(), outvec = vector(length(invec)), u=1); for(i=1, length(invec), if(mapisdefined(om,invec[i]), my(pp = mapget(om, invec[i])); outvec[i] = outvec[pp] , mapput(om,invec[i],i); outvec[i] = u; u++ )); outvec; };",
				"v073751 = readvec(\"b073751_to.txt\"); \\\\ Prepared with gawk '{ print $2 }' \u003c b073751.txt \u003e b073751_to.txt",
				"v342011 = rgs_transform(v073751);",
				"A342011(n) = v342011[n];",
				"for(n=1,#v342011,write(\"b342011.txt\", n, \" \", A342011(n)));"
			],
			"xref": [
				"Cf. A000720, A004490, A073751, A342010, A342012, A342013."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Antti Karttunen_, Mar 08 2021",
			"references": 3,
			"revision": 14,
			"time": "2021-03-12T23:11:50-05:00",
			"created": "2021-03-09T20:16:14-05:00"
		}
	]
}