{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A056609",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 56609,
			"data": "1,1,2,1,3,1,2,3,5,1,1,1,7,5,2,1,3,1,5,7,11,1,1,5,13,3,7,1,1,1,2,11,17,7,1,1,19,13,1,1,7,1,11,1,23,1,1,7,5,17,13,1,3,11,1,19,29,1,1,1,31,1,2,13,11,1,17,23,1,1,1,1,37,5,19,11,13,1,1,3,41,1,1,17,43,29,11,1,1,13",
			"name": "a(n) = rad(n!)/rad(A001142(n)) where rad(n) is the squarefree kernel of n, A007947(n).",
			"comment": [
				"The previous name, which does not match the data as observed by _Luc Rousseau_, was: Quotient of squarefree kernels of A002944(n) and A001405.",
				"a(n) is the unique prime p not greater than n missing in the prime factorization of A001142(n), if such a prime exists; a(n) is 1 otherwise. - _Luc Rousseau_, Jan 01 2019"
			],
			"link": [
				"Luc Rousseau, \u003ca href=\"/A056609/b056609.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e (first 90 terms from Labos Elemer)"
			],
			"example": [
				"From _Luc Rousseau_, Jan 02 2019: (Start)",
				"In Pascal's triangle,",
				"- row n=3 (1 3 3 1) contains no number with prime factor 2, so a(3) = 2;",
				"- row n=4 (1 4 6 4 1) contains, for all p prime \u003c= 4, a multiple of p, so a(4) = 1;",
				"- row n=5 (1 5 10 10 5 1) contains no number with prime factor 3, so a(5) = 3;",
				"etc.",
				"(End)"
			],
			"mathematica": [
				"L[n_] := Table[Binomial[n, k], {k, 1, Floor[n/2]}]",
				"c[n_] := Complement[Prime /@ Range[PrimePi[n]], First /@ FactorInteger[Times @@ L[n]]]",
				"a[n_] := Module[{x = c[n]}, If[x == {}, 1, First[x]]]",
				"Table[a[n], {n, 1, 100}]",
				"(* _Luc Rousseau_, Jan 01 2019 *)"
			],
			"program": [
				"(PARI) rad(n) = factorback(factorint(n)[, 1]); \\\\ A007947",
				"b(n) = prod(m=1, n, binomial(n, m)); \\\\ A001142",
				"a(n) = rad(n!)/rad(b(n)); \\\\ _Michel Marcus_, Jan 02 2019"
			],
			"xref": [
				"Cf. A002944, A001405, A003418, A002110, A001142."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Labos Elemer_, Aug 07 2000",
			"ext": [
				"Definition and example changed by _Luc Rousseau_, Jan 02 2019"
			],
			"references": 1,
			"revision": 21,
			"time": "2019-01-24T02:03:00-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}