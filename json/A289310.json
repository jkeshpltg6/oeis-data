{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A289310",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 289310,
			"data": "1,1,1,-3,1,-5,1,-11,-8,-9,1,-15,1,-13,-14,-7,1,-20,1,-23,-20,-21,1,-5,-24,-25,-26,-31,1,-30,1,41,-32,-33,-34,0,1,-37,-38,-1,1,-40,1,-47,-38,-45,1,65,-48,-44,-50,-55,1,10,-54,3,-56,-57,1,10,1,-61,-50",
			"name": "Let f be the multiplicative function satisfying f(p^k) = (1 + p*I)^k for any prime p and k \u003e 0 (where I^2 = -1); a(n) = the real part of f(n).",
			"comment": [
				"See A289311 for the imaginary part of f.",
				"See A289320 for the square of the norm of f.",
				"a(p) = 1 for any prime p.",
				"If a(n) = 0, then a(n^(2*k-1)) = 0 and A289311(n^(2*k)) = 0 for any k \u003e 0.",
				"a(n) = 0 iff Sum_{i=1...k} ( arctan(p_i) * e_i } = Pi/2 * (2*j + 1) for some integer j (where Product_{i=1..k} p_i^e_i is the prime factorization of n).",
				"a(n) = 0 for n = 36, 3969, 13608, 46656, 1500282, 5143824, 6718383, ...",
				"As a(36) = 0 and 36 = 2^2 * 3^3, we have arctan(2)*2 + arctan(3)*2 = Pi/2 * (2*j + 1) (with j = 1).",
				"If |a(n)| = |A289311(n)|, then |a(n^(2k-1))| = |A289311(n^(2k-1))| for any k \u003e 0.",
				"|a(n)| = |A289311(n)| iff Sum_{i=1...k} ( arctan(p_i) * e_i } = Pi/4 * (2*j + 1) for some integer j (where Product_{i=1..k} p_i^e_i is the prime factorization of n).",
				"|a(n)| = |A289311(n)| for n = 6, 63, 216, 2268, 7776, 23814, 81648, 106641, 250047, 279936, 312273, 857304, ...",
				"As |a(63)| = |A289311(63)| and 63 = 3^2 * 7, we have arctan(3)*2 + arctan(7) = Pi/4 * (2*j + 1) (with j=1).",
				"The scatterplot of this sequence vs A289311 is interesting (see Links section)."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A289310/b289310.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A289310/a289310.png\"\u003eScatterplot of the first 1000000 terms of A289310 vs A289311\u003c/a\u003e"
			],
			"example": [
				"f(12) = f(2^2 * 3) = (1 + 2*I)^2 * (1 + 3*I) = -15 - 5*I, hence a(12) = -15."
			],
			"mathematica": [
				"Array[Re[Times @@ Map[(1 + #1 I)^#2 \u0026 @@ # \u0026, FactorInteger@ #]] \u0026, 63] (* _Michael De Vlieger_, Jul 03 2017 *)"
			],
			"program": [
				"(PARI) a(n) = my (f=factor(n)); real (prod(i=1, #f~, (1 + f[i,1]*I) ^ f[i,2]))"
			],
			"xref": [
				"Cf. A289311, A289320."
			],
			"keyword": "sign",
			"offset": "1,4",
			"author": "_Rémy Sigrist_, Jul 02 2017",
			"references": 3,
			"revision": 23,
			"time": "2017-07-10T04:11:42-04:00",
			"created": "2017-07-10T04:11:42-04:00"
		}
	]
}