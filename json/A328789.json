{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A328789",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 328789,
			"data": "1,0,2,2,3,4,7,6,11,14,17,22,32,34,49,60,72,90,117,132,171,206,245,298,369,422,522,620,728,868,1043,1198,1439,1688,1962,2304,2717,3114,3668,4258,4909,5698,6627,7566,8788,10112,11574,13310,15317,17410,20010",
			"name": "Expansion of (chi(x^3) / chi(-x^2))^2 in powers of x where chi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Convolution square of A097242.",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (36 t)) = 1/2 g(t) where q = exp(2 Pi i t) and g() is g.f. for A328795."
			],
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(1/12) * (eta(q^4) * eta(q^6)^2)^2 / (eta(q^2) * eta(q^3) * eta(q^12))^2 in powers of q.",
				"Euler transform of period 12 sequence [0, 2, 2, 0, 0, 0, 0, 0, 2, 2, 0, 0, ...].",
				"G.f.: Product_{k\u003e=1} (1 + x^(6*k - 3))^2 / (1 - x^(4*k - 2))^2.",
				"a(n) = A112206(2*n).",
				"a(n) ~ exp(2*Pi*sqrt(n)/3) / (4*sqrt(3)*n^(3/4)). - _Vaclav Kotesovec_, Oct 31 2019"
			],
			"example": [
				"G.f. = 1 + 2*x^2 + 2*x^3 + 3*x^4 + 4*x^5 + 7*x^6 + 6*x^7 + 11*x^8 + ...",
				"G.f. = q^-1 + 2*q^23 + 2*q^35 + 3*q^47 + 4*q^59 + 7*q^71 + 6*q^83 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (QPochhammer[ -x^2, x^2] QPochhammer[ -x^3, x^6])^2, {x, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n \u003c 0, 0, A = x * O(x^n); polcoeff( (eta(x^4 + A) * eta(x^6 + A)^2)^2 / (eta(x^2 + A) * eta(x^3 + A) * eta(x^12 + A))^2, n))};"
			],
			"xref": [
				"Cf. A097242, A112206, A328795."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Michael Somos_, Oct 27 2019",
			"references": 5,
			"revision": 8,
			"time": "2019-10-31T17:37:34-04:00",
			"created": "2019-10-27T19:12:58-04:00"
		}
	]
}