{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A049311",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 49311,
			"data": "1,3,6,16,34,90,211,558,1430,3908,10725,30825,90156,273234,848355,2714399,8909057,30042866,103859678,368075596,1335537312,4958599228,18820993913,72980867400,288885080660,1166541823566,4802259167367,20141650236664",
			"name": "Number of (0,1) matrices with n ones and no zero rows or columns, up to row and column permutations.",
			"comment": [
				"Also the number of bipartite graphs with n edges, no isolated vertices and a distinguished bipartite block, up to isomorphism.",
				"The EULERi transform (A056156) is also interesting.",
				"a(n) is also the number of non-isomorphic set multipartitions (multisets of sets) of weight n. - _Gus Wiseman_, Mar 17 2017"
			],
			"link": [
				"Aliaksandr Siarhei, \u003ca href=\"/A049311/b049311.txt\"\u003eTable of n, a(n) for n = 1..102\u003c/a\u003e",
				"P. J. Cameron, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL3/groups.html\"\u003eSequences realized by oligomorphic permutation groups\u003c/a\u003e, J. Integ. Seqs. Vol. 3 (2000), #00.1.5.",
				"P. J. Cameron, D. A. Gewurz and F. Merola, \u003ca href=\"http://www.maths.qmw.ac.uk/~pjc/preprints/product.pdf\"\u003eProduct action\u003c/a\u003e, Discrete Math., 308 (2008), 386-394.",
				"P. J. Cameron, \u003ca href=\"http://www.maths.qmw.ac.uk/~pjc/pgprob.html\"\u003eSolution of problem 3 on Cameron's list of permutation group problems\u003c/a\u003e",
				"\u003ca href=\"/index/Mat#binmat\"\u003eIndex entries for sequences related to binary matrices\u003c/a\u003e"
			],
			"formula": [
				"Calculate number of connected bipartite graphs + number of connected bipartite graphs with no duality automorphism, then apply EULER transform.",
				"a(n) is the coefficient of x^n in the cycle index Z(S_n X S_n; 1+x, 1+x^2, ...), where S_n X S_n is Cartesian product of symmetric groups S_n of degree n."
			],
			"example": [
				"E.g. a(2) = 3: two ones in same row, two ones in same column, or neither.",
				"a(3) = 6 is coefficient of x^3 in (1/36)*((1 + x)^9 + 6*(1 + x)^3*(1 + x^2)^3 + 8*(1 + x^3)^3 + 9*(1 + x)*(1 + x^2)^4 + 12*(1 + x^3)*(1 + x^6))=1 + x + 3*x^2 + 6*x^3 + 7*x^4 + 7*x^5 + 6*x^6 + 3*x^7 + x^8 + x^9.",
				"There are a(3) = 6 binary matrices with 3 ones, with no zero rows or columns, up to row and column permutation:",
				"[1 0 0] [1 1 0] [1 0] [1 1] [1 1 1] [1]",
				"[0 1 0] [0 0 1] [1 0] [1 0] ....... [1].",
				"[0 0 1] ....... [0 1] ............. [1]",
				"Non-isomorphic representatives of the a(3)=6 set multipartitions are: ((123)), ((1)(23)), ((2)(12)), ((1)(1)(1)), ((1)(2)(2)), ((1)(2)(3)). - _Gus Wiseman_, Mar 17 2017"
			],
			"xref": [
				"Cf. A049312, A048194, A028657, A055192, A055599, A052371, A052370, A053304, A053305, A007716, A002724.",
				"Cf. A057149, A057150, A057151, A057152.",
				"Cf. A034691, A056156, A089259, A116540, A283877."
			],
			"keyword": "nonn,nice",
			"offset": "1,2",
			"author": "_Peter J. Cameron_",
			"ext": [
				"More terms and formula from _Vladeta Jovovic_, Jul 29 2000",
				"a(19)-a(28) from _Max Alekseyev_, Jul 22 2009",
				"a(29)-a(102) from _Aliaksandr Siarhei_, Dec 13 2013",
				"Name edited by _Gus Wiseman_, Dec 18 2018"
			],
			"references": 129,
			"revision": 39,
			"time": "2018-12-20T22:43:23-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}