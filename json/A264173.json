{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A264173",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 264173,
			"data": "1,1,2,6,23,1,110,10,632,86,2,4229,782,29,32337,7571,407,5,278204,78726,5856,94,2659223,882997,84351,2215,14,27959880,10657118,1251246,48234,322,320706444,137977980,19318314,984498,14322,42,3985116699,1910131680,311306106",
			"name": "Number T(n,k) of permutations of [n] with exactly k (possibly overlapping) occurrences of the consecutive pattern 1324; triangle T(n,k), n\u003e=0, 0\u003c=k\u003c=max(0,floor(n/2-1)), read by rows.",
			"comment": [
				"Pattern 4231 gives the same triangle."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A264173/b264173.txt\"\u003eRows n = 0..120, flattened\u003c/a\u003e"
			],
			"formula": [
				"Sum_{k\u003e0} k * T(n,k) = ceiling((n-3)*n!/4!) = A061206(n-3) (for n\u003e3)."
			],
			"example": [
				"T(4,1) = 1: 1324.",
				"T(6,2) = 2: 132546, 142536.",
				"T(8,3) = 5: 13254768, 13264758, 14253768, 14263758, 15263748.",
				"T(10,4) = 14: 132547698(10), 132548697(10), 132647598(10), 132648597(10), 132748596(10), 142537698(10), 142538697(10), 142637598(10), 142638597(10), 142738596(10), 152637498(10), 152638497(10), 152738496(10), 162738495(10).",
				"Triangle T(n,k) begins:",
				"00 :        1;",
				"01 :        1;",
				"02 :        2;",
				"03 :        6;",
				"04 :       23,      1;",
				"05 :      110,     10;",
				"06 :      632,     86,     2;",
				"07 :     4229,    782,    29;",
				"08 :    32337,   7571,   407,    5;",
				"09 :   278204,  78726,  5856,   94;",
				"10 :  2659223, 882997, 84351, 2215, 14;"
			],
			"maple": [
				"b:= proc(u, o, t) option remember; expand(`if`(u+o=0, 1,",
				"      add(b(u-j, o+j-1, `if`(t\u003e0 and j\u003ct, -j, 0)), j=1..u)+",
				"      add(b(u+j-1, o-j, j)*`if`(t\u003c0 and -j\u003c=t, x, 1), j=1..o)))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=0..degree(p)))(b(n, 0$2)):",
				"seq(T(n), n=0..14);"
			],
			"mathematica": [
				"b[u_, o_, t_] := b[u, o, t] = Expand[If[u + o == 0, 1, Sum[b[u - j, o + j - 1, If[t \u003e 0 \u0026\u0026 j \u003c t, -j, 0]], {j, 1, u}] + Sum[b[u + j - 1, o - j, j] * If[t \u003c 0 \u0026\u0026 -j \u003c= t, x, 1], {j, 1, o}]]];",
				"T[n_] := Function[p, Table[Coefficient[p, x, i], {i, 0, Exponent[p, x]}]][ b[n, 0, 0]];",
				"Table[T[n], {n, 0, 14}] // Flatten (* _Jean-François Alcover_, Apr 30 2017, translated from Maple *)"
			],
			"xref": [
				"Columns k=0-10 give: A113228, A264174, A264175, A264176, A264177, A264178, A264179, A264180, A264181, A264182, A264183.",
				"Row sums give A000142.",
				"T(2n+2,n) gives A000108(n) for n\u003e0.",
				"Cf. A004526, A061206, A264319 (pattern 3412)."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_Alois P. Heinz_, Nov 06 2015",
			"references": 14,
			"revision": 28,
			"time": "2018-05-09T09:56:02-04:00",
			"created": "2015-11-06T15:50:08-05:00"
		}
	]
}