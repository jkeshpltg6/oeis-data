{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A316491",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 316491,
			"data": "0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,1,0,2,1,0,1,0,1,2,1,0,2,1,1,2,3,0,2,2,0,3,2,2,3,1,2,2,2,3,3,4,0,4,3,0,6,3,3,4,3,1,4,4,3,4,4,2,6,4,3,6,3,3,6,4,3,7,5,4,5,6,1,6,6,2,10,4,5,7,5",
			"name": "Number of ways to represent 8*n + 4 as the sum of four distinct odd squares.",
			"comment": [
				"Every odd square is a number of the form 8*k + 1, so every sum of four odd squares is a number of the form 8*k + 4.",
				"A316489 lists all positive numbers of the form 8*k + 4 that cannot be expressed as the sum of four distinct odd squares; for each such number, a(k)=0.",
				"A316834 lists all numbers that can be expressed in only one way as the sum of four distinct odd squares; each such number is of the form 8*k + 4, and for each such number, a(k)=1."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A316491/b316491.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"example": [
				"n=1: 8*1 + 4 = 12 cannot be expressed as the sum of four distinct odd squares, so a(1)=0.",
				"n=10: 8*10 + 4 = 84 can be expressed as the sum of four distinct odd squares in only 1 way (84 = 1^2 + 3^2 + 5^2 + 7^2), so a(10)=1.",
				"n=19: 8*19 + 4 = 156 can be expressed as the sum of four distinct odd squares in exactly 2 ways (156 = 1^2 + 3^2 + 5^2 + 11^2 = 1^2 + 5^2 + 7^2 + 9^2), so a(19)=2."
			],
			"maple": [
				"b:= proc(n, i, t) option remember; `if`(n=0, `if`(t=0, 1, 0),",
				"      `if`(min(i, t)\u003c1, 0, b(n, i-2, t)+",
				"      `if`(i^2\u003en, 0, b(n-i^2, i-2, t-1))))",
				"    end:",
				"a:= n-\u003e (m-\u003e b(m, (r-\u003e r+1-irem(r, 2))(isqrt(m)), 4))(8*n+4):",
				"seq(a(n), n=0..100);  # _Alois P. Heinz_, Aug 05 2018"
			],
			"mathematica": [
				"a[n_] := Count[ IntegerPartitions[8 n + 4, {4}, Range[1, Sqrt[8 n + 4], 2]^2], w_ /; Max@Differences@w \u003c 0]; Array[a, 87, 0] (* _Giovanni Resta_, Aug 12 2018 *)",
				"b[n_, i_, t_] := b[n, i, t] = If[n == 0, If[t == 0, 1, 0],",
				"     If[Min[i, t] \u003c 1, 0, b[n, i-2, t] +",
				"     If[i^2 \u003e n, 0, b[n-i^2, i-2, t-1]]]];",
				"a[n_] := Function[m, b[m, Function[r, r+1-Mod[r, 2]][Floor@Sqrt[m]], 4]][8n+4];",
				"a /@ Range[0, 100] (* _Jean-François Alcover_, May 30 2021, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A316834, A316489, A316490."
			],
			"keyword": "nonn",
			"offset": "0,20",
			"author": "_Jon E. Schoenfield_, Jul 29 2018",
			"references": 1,
			"revision": 18,
			"time": "2021-05-30T11:11:12-04:00",
			"created": "2018-08-12T17:29:41-04:00"
		}
	]
}