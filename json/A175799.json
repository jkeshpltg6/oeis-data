{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A175799",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 175799,
			"data": "0,1,0,1,0,1,2,1,2,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,1,1,0,1,0,1,0,1,2,1,0,1,0,1,0,1,0,1,1,1,0,1,1,1,0,1,0,1,0,1,0,1,0,0,0,1,2,1,2,2,0,1,0,1,0,0,0,1,0,1,0,1,0,0,0,1,0,1,0,1,0,1,0,1,0,0,0",
			"name": "Number of real zeros of the polynomial whose coefficients are the decimal expansion of Pi truncated to n places (A011545).",
			"comment": [
				"a(n) = number of real zeros of the polynomial P(n,x) = sum_{k=0..n-1} d(k) x^k, where d(k) are the digits of the decimal expansion of floor(Pi*10^n), n=0,1,2,...",
				"From _Robert Israel_, Dec 19 2018: (Start)",
				"If d(n) = 0 then P(n,x)=P(n-1,x) so a(n)=a(n-1).",
				"If d(n) \u003c\u003e 0 and P(n,x) has nonzero discriminant, then a(n) == n (mod 2).",
				"Conjecture: P(n,x) has nonzero discriminant for all n \u003e= 1.",
				"Record values: a(0)=0, a(1)=1, a(6)=2, a(135)=3, a(374)=4. (End)"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A175799/b175799.txt\"\u003eTable of n, a(n) for n = 0..545\u003c/a\u003e"
			],
			"example": [
				"a(0) = 0 because 3 =\u003e P(0,x)=3 is a constant and has 0 real root;",
				"a(1) = 1 because 31 =\u003e P(1,x) = 1+3x has 1 real root;",
				"a(6) = 2 because 3141592 =\u003e P(6,x) = 2 + 9x + 5x^2 + x^3 + 4x^4 + x^5 + 3x^6 has 2 real roots."
			],
			"maple": [
				"L:= convert(floor(10^100*Pi),base,10):",
				"f:= proc(n) local P, x,i;",
				"  P:=add(L[-i]*x^(i-1),i=1..n+1);",
				"  sturm(sturmseq(P,x),x,-infinity,infinity)",
				"end proc:",
				"map(f, [$0..100]); # _Robert Israel_, Dec 19 2018"
			],
			"program": [
				"(PARI) A175799(n)={ default(realprecision)\u003en | default(realprecision,n+1); sum(k=1, #n=factor(1.*Pol(eval(Vec(Str(Pi*10^n\\1)))))~, (poldegree(n[1, k])==1)*n[2, k] )} /* factorization over the reals =\u003e linear factor for each root. poldegree()==1 could be replaced by poldisc()\u003e=0 */ \\\\ _M. F. Hasler_, Dec 04 2010"
			],
			"xref": [
				"Cf. A011545, A173667."
			],
			"keyword": "nonn,base",
			"offset": "0,7",
			"author": "_Michel Lagneau_, Dec 04 2010",
			"ext": [
				"Corrected and extended by _Robert Israel_, Dec 19 2018"
			],
			"references": 2,
			"revision": 22,
			"time": "2018-12-19T22:17:43-05:00",
			"created": "2010-11-12T14:24:37-05:00"
		}
	]
}