{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A329576",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 329576,
			"data": "1,2,3,4,5,8,11,26,15,9,14,32,17,20,21,27,10,16,19,7,12,13,24,6,23,35,25,37,18,36,22,31,61,28,30,39,40,43,33,64,38,45,34,29,63,50,44,53,42,59,47,54,48,41,90,49,55,52,108,58,46,51,121,73,78,76,100,79,81,151,60,67,112,70,69",
			"name": "For all n \u003e= 1, exactly seven sums are prime among a(n+i) + a(n+j), 0 \u003c= i \u003c j \u003c 6; lexicographically earliest such sequence of distinct positive numbers.",
			"comment": [
				"That is, there are 7 primes, counted with multiplicity, among the 15 pairwise sums of any 6 consecutive terms.",
				"Conjectured to be a permutation of the positive integers."
			],
			"link": [
				"M. F. Hasler, \u003ca href=\"/wiki/User:M._F._Hasler/Prime_sums_from_neighboring_terms\"\u003ePrime sums from neighboring terms\u003c/a\u003e, OEIS wiki, Nov. 23, 2019"
			],
			"example": [
				"For n = 1, we must forbid the greedy choice for a(6) which would be 6, which leads to a dead end: there is no possibility to find a subsequent term that would give 7 prime sums together with {2, 3, 4, 5, 6}. If we take the next larger possibility, a(6) = 8, then it works for the next and all subsequent terms."
			],
			"program": [
				"(PARI) {A329576(n,show=1,o=1,N=7,M=5,X=[[6,6]],p=[],u=o,U)=for(n=o+1,n, show\u003e0\u0026\u0026 print1(o\",\"); show\u003c0\u0026\u0026 listput(L,o); U+=1\u003c\u003c(o-u); U\u003e\u003e=-u+u+=valuation(U+1,2); p=concat(if(#p\u003e=M,p[^1],p),o); my(c=N-sum(i=2,#p, sum(j=1,i-1, isprime(p[i]+p[j])))); if(#p\u003cM\u0026\u0026sum(i=1,#p,isprime(p[i]+u))\u003c=c,o=u)|| for(k=u,oo,bittest(U,k-u)|| sum(i=1,#p,isprime(p[i]+k))!=c|| setsearch(X,[n,k])|| [o=k,break]));show\u0026\u0026print([u]);o} \\\\ optional args: show=1: print a(o..n-1), show=-1: append them on global list L, in both cases print [least unused number] at the end. See the wiki page for more."
			],
			"xref": [
				"Cf. A329425 (6 primes using 5 consecutive terms), A329566 (6 primes using 6 consecutive terms).",
				"Cf. A329449 (4 primes using 4 consecutive terms), A329456 (4 primes using 5 consecutive terms).",
				"Cf. A329454 (3 primes using 4 consecutive terms), A329455 (3 primes using 5 consecutive terms).",
				"Cf. A329411 (2 primes using 3 consecutive terms), A329452 (2 primes using 4 consecutive terms), A329453 (2 primes using 5 consecutive terms).",
				"Cf. A329333 (1 (odd) prime using 3 terms), A128280 \u0026 A055265 (1 prime using 2 terms); A055266 \u0026 A253074 (0 primes using 2 terms), A329405 \u0026 A329450 (0 primes using 3 terms), A329406 - A329416, A329563 - A329581: other variants."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_M. F. Hasler_, Feb 09 2020",
			"references": 2,
			"revision": 5,
			"time": "2020-02-11T12:50:01-05:00",
			"created": "2020-02-11T12:50:01-05:00"
		}
	]
}