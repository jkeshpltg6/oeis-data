{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A213727",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 213727,
			"data": "0,0,1,0,0,1,1,0,0,1,3,0,1,1,1,0,0,1,3,0,1,1,5,0,1,3,0,1,1,1,1,0,0,1,3,0,1,1,5,0,1,3,0,1,1,1,0,7,1,0,5,1,1,0,3,1,3,0,1,1,1,1,1,0,0,1,3,0,1,1,5,0,1,3,0,1,1,1,0,7,1,0,5,1,1,0,3",
			"name": "a(n) = 0 if n is in the infinite trunk of the \"binary beanstalk\", otherwise number of nodes (including leaves and the node n itself) in that finite branch of the beanstalk.",
			"comment": [
				"a(n) tells, for each natural number n, whether it belongs to the infinite trunk of the binary beanstalk (in A179016, when a(n)=0), or if it is one of the terminal nodes (i.e., leaves, A055938, when a(n)=1), or otherwise, when a(n) \u003e 1, tells from how many different numbers one can arrive at this n by repeatedly subtracting their bit-count (A000120) from them (as explained in A071542), and including also n itself as one of the cases.",
				"a(n) is always odd unless it is zero. In particular, each a(A213717(n)) is an odd number greater than one."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A213727/b213727.txt\"\u003eTable of n, a(n) for n = 0..16384\u003c/a\u003e"
			],
			"formula": [
				"If A079559(n)=0, a(n)=1; otherwise, if A213719(n)=1, a(n)=0; otherwise a(n) = 1+a(A213723(n))+a(A213724(n)).",
				"Other identities. For all n:",
				"a(A179016(n)) = 0, a(A055938(n)) = 1, and a(A213717(n)) \u003e= 3.",
				"a(A213717(n)) = (2*A213726(A213717(n)))-1."
			],
			"example": [
				"a(10) = 3 because we include 10 itself (\"1010\" in binary) and the two numbers n for which it is true that n - A000120(n) = 10, i.e., 12 and 13 (\"1100\" and \"1101\" in binary). Furthermore, there do not exist any such numbers for 12 or 13, as both are members of A055938 (see also the comment at A213717).",
				"Similarly, a(22) = 5 as there are the following five cases: 22 itself, 24 as 24-A000120(24) = 24-2 = 22 (note that 24 is in A055938), 25 as 25-A000120(25) = 25-3 = 22, and the two terminal nodes (leaves) branching from 25, that is, 28 \u0026 29 (as 28-A000120(28) = 28-3 = 25, and 29-A000120(29) = 29-4=25)."
			],
			"program": [
				"(Scheme with memoization-macro definec)",
				"(definec (A213727 n) (cond ((zero? (A079559 n)) 1) ((not (zero? (A213719 n))) 0) (else (+ 1 (A213727 (A213723 n)) (A213727 (A213724 n))))))"
			],
			"xref": [
				"Cf. A179016.",
				"Cf. also A055938, A079559, A213717, A213719, A213723, A213724, A213725, A213726, A213731.",
				"Analogous sequences computed for other \"beanstalk\" or similar tree systems: A227643, A230427, A255327."
			],
			"keyword": "nonn",
			"offset": "0,11",
			"author": "_Antti Karttunen_, Nov 01 2012",
			"references": 12,
			"revision": 32,
			"time": "2015-02-25T12:24:04-05:00",
			"created": "2012-11-02T11:57:46-04:00"
		}
	]
}