{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A229899",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 229899,
			"data": "0,0,0,0,1,0,2,1,1,0,1,2,2,2,4,6,7,0,1,2,2,1,10,10,8,4,4,13,4,6,3,5,10,3,20,2,6,6,19,18,22,4,11,6,16,4,3,7,28,8,28,16,4,16,32,31,30,5,8,16,13,32,7,17,6,40,7,2,43,8,36,43,10,12,8,46,44,8,30,16,39,8,24,20,11,39,30,14,22,9,58,58,22,17,22,61,60,30,21,10",
			"name": "a(n) = |{0\u003cg\u003cp_n: g-1, g, g+1 are primitive roots mod p_n}|, where p_n denotes the n-th prime.",
			"comment": [
				"Conjecture: a(n) \u003e 0 except for n = 1, 2, 3, 4, 6, 10, 18. In other words, for any prime p \u003e 7 not equal to 13 or 29 or 61, there are three consecutive integers which are primitive roots modulo p.",
				"Let p be an odd prime. For any integer c, define S(c,p) to be the sum of the Legendre symbols ((g+c)/p) over all primitive roots g modulo p among 1, ..., p-1. If g is a primitive root modulo p, then so is the inverse g^{-1} of g modulo p, and -((g^{-1}+c)/p) = (g*(g^{-1}+c)/p) = ((1+c*g)/p). So S(1,p) = 0, and also S(-1,p) = 0 when p == 1 (mod 4). The author also showed that S(-c,p) = S(c,p) if p == 1 (mod 4), and that S(c,p) = 0 if p is a Fermat prime and c is a quadratic residue modulo p.",
				"Zhi-Wei Sun also made the following conjectures:",
				"(i) Let p \u003e 13 be a prime not equal to 19 or 31, and let a,b,c be integers with a or c not divisible by p. If p does not divide b^2-4*a*c, then there is a primitive root g modulo p such that a*g^2+b*g+c is a quadratic residue modulo p, and there is also a primitive root h modulo p such that a*h^2+b*h+c is a quadratic nonresidue modulo p.",
				"(ii) Let p be any odd prime, and let a,b,c be integers with a or c not divisible by p. If p does not divide b^2-4*a*c, then the absolute value of the sum of the Legendre symbols ((a*g^2+b*g+c)/p) over all primitive roots g modulo p among 1, ..., p-1 is smaller than 2*sqrt(p)."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A229899/b229899.txt\"\u003eTable of n, a(n) for n = 1..400\u003c/a\u003e"
			],
			"example": [
				"a(5) = 1 since 6, 7, 8 are primitive roots modulo p_5 = 11.",
				"a(7) = 2 since 5, 6, 7, 10, 11, 12 are primitive roots modulo p_7 = 17.",
				"a(8) = 1 since 13, 14, 15 are primitive roots modulo p_8 = 19."
			],
			"mathematica": [
				"gp[g_,p_]:=gp[g,p]=Length[Union[Table[Mod[g^k, p],{k,1,p-1}]]]==p-1",
				"a[n_]:=Sum[If[gp[g,Prime[n]]\u0026\u0026gp[g-1,Prime[n]]\u0026\u0026gp[g+1,Prime[n]],1,0],{g,1,Prime[n]-1}]",
				"Table[a[n],{n,1,100}]"
			],
			"xref": [
				"Cf. A001918."
			],
			"keyword": "nonn",
			"offset": "1,7",
			"author": "_Zhi-Wei Sun_, Oct 02 2013",
			"references": 2,
			"revision": 25,
			"time": "2013-10-03T03:14:25-04:00",
			"created": "2013-10-03T00:58:49-04:00"
		}
	]
}