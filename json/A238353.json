{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A238353",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 238353,
			"data": "1,1,0,2,0,0,2,1,0,0,3,1,1,0,0,2,3,1,1,0,0,4,3,2,1,1,0,0,2,6,3,2,1,1,0,0,4,6,6,2,2,1,1,0,0,3,10,6,5,2,2,1,1,0,0,4,11,11,6,4,2,2,1,1,0,0,2,16,13,10,5,4,2,2,1,1,0,0,6,17,19,12,9,4,4,2,2,1,1,0,0,2,24,24,18,11,8,4,4,2,2,1,1,0,0",
			"name": "Triangle T(n,k) read by rows: T(n,k) is the number of partitions of n (as weakly ascending list of parts) with maximal ascent k, n \u003e= 0, 0 \u003c= k \u003c= n.",
			"comment": [
				"Reversed rows and also the columns converge to A002865 (setting A002865(0)=0).",
				"Column k=0 is A000005 (n\u003e=1), column k=1 is A237665.",
				"Row sums are A000041.",
				"Sum_{i=0..k} T(n,i) for k=0-9 gives: A000005, A034296, A224956, A238863, A238864, A238865, A238866, A238867, A238868, A238869."
			],
			"link": [
				"Joerg Arndt and Alois P. Heinz, \u003ca href=\"/A238353/b238353.txt\"\u003eRows 0..140, flattened\u003c/a\u003e"
			],
			"formula": [
				"G.f. for column k\u003e=1: sum(j\u003e=1, q^j/(1-q^j) * (prod(i=1..j-1, (1-q^((k+1)*i))/(1-q^i) ) - prod(i=1..j-1, (1-q^(k*i))/(1-q^i) ) )  ), see the comment about the g.f. in A238863."
			],
			"example": [
				"Triangle starts:",
				"00:  1;",
				"01:  1,  0;",
				"02:  2,  0,  0;",
				"03:  2,  1,  0,  0;",
				"04:  3,  1,  1,  0,  0;",
				"05:  2,  3,  1,  1,  0,  0;",
				"06:  4,  3,  2,  1,  1,  0, 0;",
				"07:  2,  6,  3,  2,  1,  1, 0, 0;",
				"08:  4,  6,  6,  2,  2,  1, 1, 0, 0;",
				"09:  3, 10,  6,  5,  2,  2, 1, 1, 0, 0;",
				"10:  4, 11, 11,  6,  4,  2, 2, 1, 1, 0, 0;",
				"11:  2, 16, 13, 10,  5,  4, 2, 2, 1, 1, 0, 0;",
				"12:  6, 17, 19, 12,  9,  4, 4, 2, 2, 1, 1, 0, 0;",
				"13:  2, 24, 24, 18, 11,  8, 4, 4, 2, 2, 1, 1, 0, 0;",
				"14:  4, 27, 34, 22, 17, 10, 7, 4, 4, 2, 2, 1, 1, 0, 0;",
				"15:  4, 35, 39, 33, 20, 15, 9, 7, 4, 4, 2, 2, 1, 1, 0, 0;",
				"...",
				"The 7 partitions of 5 and their maximal ascents are:",
				"1:  [ 1 1 1 1 1 ]   0",
				"2:  [ 1 1 1 2 ]   1",
				"3:  [ 1 1 3 ]   2",
				"4:  [ 1 2 2 ]   1",
				"5:  [ 1 4 ]   3",
				"6:  [ 2 3 ]   1",
				"7:  [ 5 ]   0",
				"There are 2 rows with 0 ascents, 3 with 1 ascent, 1 for ascents 2 and 3, giving row 5 of the triangle."
			],
			"maple": [
				"b:= proc(n, i, t) option remember; `if`(n=0, 1,",
				"      `if`(i\u003c1, 0, b(n, i-1, t)+`if`(i\u003en, 0, (p-\u003e",
				"      `if`(t=0 or t-i=0, p, add(coeff(p, x, j)*x^",
				"      max(j, t-i), j=0..degree(p))))(b(n-i, i, i)))))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, k), k=0..n))(b(n$2, 0)):",
				"seq(T(n), n=0..15);"
			],
			"mathematica": [
				"b[n_, i_, t_] := b[n, i, t] = If[n == 0, 1, If[i\u003c1, 0, b[n, i-1, t] + If[i\u003en, 0, Function[{p}, If[t == 0 || t-i == 0, p, Sum[Coefficient[p, x, j]*x^ Max[j, t-i], {j, 0, Exponent[p, x]}]]][b[n-i, i, i]]]]]; T[n_] := Function[{p}, Table[Coefficient[p, x, k], {k, 0, n}]][b[n, n, 0]]; Table[T[n], {n, 0, 15}] // Flatten (* _Jean-François Alcover_, Jan 06 2015, translated from Maple *)"
			],
			"xref": [
				"Cf. A238354 (partitions by minimal ascent)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Joerg Arndt_ and _Alois P. Heinz_, Feb 26 2014",
			"references": 15,
			"revision": 26,
			"time": "2015-01-06T12:11:47-05:00",
			"created": "2014-03-02T12:36:44-05:00"
		}
	]
}