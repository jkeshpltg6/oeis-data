{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A256276",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 256276,
			"data": "1,2,0,1,4,0,0,2,0,2,0,0,2,0,0,1,4,0,0,4,0,0,0,0,3,4,0,0,4,0,0,2,0,2,0,0,2,0,0,2,4,0,0,0,0,0,0,0,1,6,0,2,4,0,0,0,0,2,0,0,2,0,0,1,8,0,0,4,0,0,0,0,2,4,0,0,0,0,0,4,0,2,0,0,4,0,0",
			"name": "Expansion of q * phi(q) * chi(q^3) * psi(-q^9) in powers of q where phi(), psi(), chi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A256276/b256276.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of eta(q^2)^5 * eta(q^6)^2 * eta(q^9) * eta(q^36) / (eta(q)^2 * eta(q^4)^2 * eta(q^3) * eta(q^12) * eta(q^18)) in powers of q.",
				"Euler transform of period 36 sequence [ 2, -3, 3, -1, 2, -4, 2, -1, 2, -3, 2, -1, 2, -3, 3, -1, 2, -4, 2, -1, 3, -3, 2, -1, 2, -3, 2, -1, 2, -4, 2, -1, 3, -3, 2, -2, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (36 t)) = 6 (t/i) g(t) where q = exp(2 Pi i t) and g() is the g.f. for A256269.",
				"a(3*n) = a(4*n + 3) = 0. a(3*n + 1) = A122865(n). a(3*n + 2) = 2 * A122856(n). a(4*n + 1) = a(n). a(4*n) = a(n). a(6*n + 2) = 2 * A122865(n). a(6*n + 4) = A122856(n)."
			],
			"example": [
				"G.f. = q + 2*q^2 + q^4 + 4*q^5 + 2*q^8 + 2*q^10 + 2*q^13 + q^16 + 4*q^17 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 2, Pi/4, q^(9/2)] / (2^(1/2) q^(1/8)) QPochhammer[ -q^3, q^6] EllipticTheta[ 3, 0, q], {q, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c1, 0, n--; A = x * O(x^n); polcoeff( eta(x^2 + A)^5 * eta(x^6 + A)^2 * eta(x^9 + A) * eta(x^36 + A) / (eta(x + A)^2 * eta(x^4 + A)^2 * eta(x^3 + A) * eta(x^12 + A) * eta(x^18 + A)), n))};",
				"(MAGMA) A := Basis( ModularForms( Gamma1(36), 1), 89); A[2] + 2*A[3]",
				"+ A[5] + 4*A[6] + 2*A[9] + 2*A[11] + 2*A[14] + A[17] + 4*A[18];"
			],
			"xref": [
				"Cf. A122856, A122865, A256269."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Michael Somos_, Jun 02 2015",
			"references": 4,
			"revision": 11,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2015-06-02T09:23:49-04:00"
		}
	]
}