{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005120",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5120,
			"id": "M3770",
			"data": "0,1,-1,1,-1,-1,5,-8,7,1,-19,43,-55,27,64,-211,343,-307,-85,911,-1919,2344,-989,-3151,9625,-15049,12609,5671,-42496,85609,-100225,33977,154007,-437009,657901,-513512,-335665,1974097,-3808891,4265379",
			"name": "A sixth-order linear divisibility sequence: a(n+6) = -3*a(n+5) - 5*a(n+4) - 5*a(n+3) - 5*a(n+2) - 3*a(n+1) - a(n).",
			"comment": [
				"This is a divisibility sequence. If d divides n then a(d) divides a(n). - _Michael Somos_, Aug 02 2002",
				"This is a generalized Lucas sequence of order 3 as defined by Roettger, Section 3.3. - _Peter Bala_, Mar 04 2014",
				"The sequence is denoted by C(n) and its expression in terms of the roots of the cubic x^3 + x^2 - 1 = 0 is given in Williams 1998 page 454. Table 17.4.1 Values of C(n) for n=-2 to n=30 is on page 455 and he notes that a(20) == a(25) == 0 (mod 101). - _Michael Somos_, Nov 13 2018"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"H. C. Williams, Edouard Lucas and Primality Testing, Wiley, 1998, p. 455. Math. Rev. 2000b:11139"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A005120/b005120.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"M. Duboue, \u003ca href=\"http://dx.doi.org/10.1016/S0195-6698(83)80013-4\"\u003eUne suite récurrente remarquable\u003c/a\u003e, Europ. J. Combin., 4 (1983), 205-214.",
				"E. L. F. Roettger, \u003ca href=\"http://people.ucalgary.ca/~hwilliam/files/A_Cubic_Extention_of_the_Lucas_Functions.pdf\"\u003eA cubic extension of the Lucas functions\u003c/a\u003e, Thesis, Dept. of Mathematics and Statistics, Univ. of Calgary, 2009.",
				"\u003ca href=\"/index/Di#divseq\"\u003eIndex to divisibility sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (-3,-5,-5,-5,-3,-1)."
			],
			"formula": [
				"G.f.: x * (1 + 2*x + 3*x^2 + 2*x^3 + x^4) / (1 + 3*x + 5*x^2 + 5*x^3 + 5*x^4 + 3*x^5 + x^6). - _Michael Somos_, Aug 02 2002",
				"a(n) = (a^n - b^n)*(b^n - c^n)*(c^n - a^n)/((a - b)*(b - c)*(c - a)), where a, b, c denote the roots of the cubic equation x^3 + x^2 - 1 = 0. - _Peter Bala_, Mar 04 2014",
				"a(n) = -3*a(n-1) - 5*a(n-2) - 5*a(n-3) - 5*a(n-4) - 3*a(n-5) - a(n-6) for n\u003e5. - _Vincenzo Librandi_, Jun 20 2014",
				"a(n) = -a(-n) for all n in Z. - _Michael Somos_, Nov 13 2018"
			],
			"example": [
				"G.f. = x - x^2 + x^3 - x^4 - x^5 + 5*x^6 - 8*x^7 + 7*x^8 + ... - _Michael Somos_, Nov 13 2018"
			],
			"mathematica": [
				"LinearRecurrence[{-3,-5,-5,-5,-3,-1},{0,1,-1,1,-1,-1},40] (* _Harvey P. Dale_, Jun 19 2014 *)",
				"CoefficientList[Series[x (1 + 2 x + 3 x^2 + 2 x^3 + x^4)/(1 + 3 x + 5 x^2 + 5 x^3 + 5 x^4 + 3 x^5 + x^6), {x, 0, 40}], x] (* _Vincenzo Librandi_, Jun 20 2014 *)",
				"a[ n_] := Sign[n] SeriesCoefficient[ x (1 + x + x^2)^2 / (1 + 3 x + 5 x^2 + 5 x^3 + 5 x^4 + 3 x^5 + x^6), {x, 0, Abs @ n}]; (* _Michael Somos_, Nov 13 2018 *)",
				"a[ n_] := Module[ {a, b, c}, {a, b, c} = Table[ Root[#^3 + #^2 - 1 \u0026, k], {k, 3}]; (a^n - b^n) (b^n - c^n) (c^n - a^n) / ((a - b) (b - c) (c - a)) // FullSimplify]; (* _Michael Somos_, Nov 13 2018 *)"
			],
			"program": [
				"(PARI) {a(n) = sign(n) * polcoeff( x*(1 + 2*(x + x^3) + 3*x^2 + x^4) / (1 + 3*(x + x^5) + 5*(x^2  + x^3 + x^4) + x^6) + x * O(x^abs(n)), abs(n))}; /* _Michael Somos_, Aug 02 2002 */",
				"(MAGMA) I:=[0,1,-1,1,-1,-1]; [n le 6 select I[n] else -3*Self(n-1)-5*Self(n-2)-5*Self(n-3)-5*Self(n-4)-3*Self(n-5)-Self(n-6): n in [1..40]]; // _Vincenzo Librandi_, Jun 20 2014"
			],
			"xref": [
				"Cf. A001608."
			],
			"keyword": "sign,easy",
			"offset": "0,7",
			"author": "_N. J. A. Sloane_.",
			"ext": [
				"Edited by _Michael Somos_, Aug 02 2002"
			],
			"references": 1,
			"revision": 41,
			"time": "2019-03-20T12:27:00-04:00",
			"created": "1991-05-16T03:00:00-04:00"
		}
	]
}