{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A155864",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 155864,
			"data": "1,1,1,1,2,1,1,6,6,1,1,12,24,12,1,1,20,60,60,20,1,1,30,120,180,120,30,1,1,42,210,420,420,210,42,1,1,56,336,840,1120,840,336,56,1,1,72,504,1512,2520,2520,1512,504,72,1,1,90,720,2520,5040,6300,5040,2520,720,90,1",
			"name": "Triangle T(n,k) = n*(n-1)*binomial(n-2, k-1) for 1 \u003c= k \u003c= n-1, n \u003e= 2, and T(n,0) = T(n,n) = 1 for n \u003e= 0, read by rows.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A155864/b155864.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = coefficients of p(n, x), where p(n, x) = 1 + x^n + x*((d/dx)^2 (1+x)^n), with T(0, 0) = 1.",
				"From _Franck Maminirina Ramaharo_, Dec 04 2018: (Start)",
				"T(n, k) = n*(n-1)*binomial(n-2, k-1) with T(n, 0) = T(n, n) = 1.",
				"n-th row polynomial is x^n + n*(n - 1)*x*(x + 1)^(n - 2) + (1 + (-1)^(2^n))/2.",
				"G.f.: 1/(1 - y) + 1/(1 - x*y) + 2*x*y^2/(1 - y - x*y)^3 - 1.",
				"E.g.f.: exp(y) + exp(x*y) + x*y^2*exp(y + x*y) - 1. (End)",
				"Sum_{k=0..n} T(n, k) = 2 - [n=0] + A001815(n). - _G. C. Greubel_, Jun 04 2021"
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  1,  1;",
				"  1,  2,   1;",
				"  1,  6,   6,    1;",
				"  1, 12,  24,   12,    1;",
				"  1, 20,  60,   60,   20,    1;",
				"  1, 30, 120,  180,  120,   30,    1;",
				"  1, 42, 210,  420,  420,  210,   42,    1;",
				"  1, 56, 336,  840, 1120,  840,  336,   56,   1;",
				"  1, 72, 504, 1512, 2520, 2520, 1512,  504,  72,  1;",
				"  1, 90, 720, 2520, 5040, 6300, 5040, 2520, 720, 90, 1;",
				"  ..."
			],
			"mathematica": [
				"(* First program *)",
				"p[n_, x_]:= p[n,x]= If[n==0, 1, 1 + x^n + x*D[(x+1)^(n), {x, 2}]];",
				"Flatten[Table[CoefficientList[p[n,x], x], {n, 0, 12}]]",
				"(* Second program *)",
				"Table[If[k==0 || k==n, 1, 2*Binomial[n, 2]*Binomial[n-2, k-1]], {n,0,12}, {k,0,n}] //Flatten (* _G. C. Greubel_, Jun 04 2021 *)"
			],
			"program": [
				"(Maxima) T(n, k) := ratcoef(x^n + n*(n-1)*x*(x+1)^(n-2) + (1 + (-1)^(2^n))/2, x, k)$ create_list(T(n, k), n, 0, 12, k, 0, n); /* _Franck Maminirina Ramaharo_, Dec 04 2018 */",
				"(MAGMA)",
				"A155864:= func\u003c n,k | k eq 0 or k eq n select 1 else n*(n-1)*Binomial(n-2, k-1) \u003e;",
				"[A155864(n,k): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Jun 04 2021",
				"(Sage)",
				"def A155864(n,k): return 1 if (k==0 or k==n) else n*(n-1)*binomial(n-2,k-1)",
				"flatten([[A155864(n,k) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Jun 04 2021"
			],
			"xref": [
				"Cf. A001815, A155863, A155865."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,5",
			"author": "_Roger L. Bagula_, Jan 29 2009",
			"ext": [
				"Edited and name clarified by _Franck Maminirina Ramaharo_, Dec 04 2018"
			],
			"references": 4,
			"revision": 10,
			"time": "2021-06-04T22:07:18-04:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}