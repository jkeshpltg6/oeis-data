{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A229232",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 229232,
			"data": "0,0,0,1,0,2,1,2,2,8",
			"name": "Number of undirected circular permutations pi(1), ..., pi(n) of 1, ..., n with the n numbers pi(1)*pi(2)-1, pi(2)*pi(3)-1, ..., pi(n-1)*pi(n)-1, pi(n)*pi(1)-1 all prime.",
			"comment": [
				"Conjecture: a(n) \u003e 0 for all n \u003e 5 with n not equal to 13.",
				"Zhi-Wei Sun also made the following conjectures:",
				"(1) For any integer n \u003e 1, there is a permutation pi(1), ..., pi(n) of 1, ..., n such that the n numbers 2*pi(1)*pi(2)-1, ..., 2*pi(n-1)*pi(n)-1, 2*pi(n)*pi(1)-1 are all prime. Also, for any positive integer n not equal to 4, there is a permutation pi(1), ..., pi(n) of 1, ..., n such that the n numbers 2*pi(1)*pi(2)+1, ..., 2*pi(n-1)*pi(n)+1, 2*pi(n)*pi(1)+1 are all prime.",
				"(2) Let F be a finite field with q \u003e 7 elements. Then, there is a circular permutation a_1,...,a_{q-1} of the q-1 nonzero elements of F such that all the q-1 elements a_1*a_2-1, a_2*a_3-1, ..., a_{q-2}*a_{q-1}-1, a_{q-1}*a_1-1 are primitive elements of the field F (i.e., generators of the multiplicative group F\\{0}). Also, there is a circular permutation b_1,...,b_{q-1} of the q-1 nonzero elements of F such that all the q-1 elements b_1*b_2+1, b_2*b_3+1, ..., b_{q-2}*b_{q-1}+1, b_{q-1}*b_1+1 are primitive elements of the field F."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1309.1679\"\u003eSome new problems in additive combinatorics, preprint, arXiv:1309.1679 [math.NT], 2013-2014."
			],
			"example": [
				"a(4) = 1 due to the circular permutation (1,3,2,4).",
				"a(6) = 2 due to the circular permutations",
				"   (1,3,2,4,5,6) and (1,3,2,6,5,4).",
				"a(7) = 1 due to the circular permutation (1,3,2,7,6,5,4).",
				"a(8) = 2 due to the circular permutations",
				"   (1,3,2,7,6,5,4,8) and (1,4,5,6,7,2,3,8).",
				"a(9) = 2 due to the circular permutations",
				"   (1,3,4,5,6,7,2,9,8) and (1,3,8,9,2,7,6,5,4).",
				"a(10) = 8 due to the circular permutations",
				"   (1,3,4,5,6,7,2,9,10,8), (1,3,4,5,6,7,2,10,9,8),",
				"   (1,3,8,9,10,2,7,6,5,4), (1,3,8,10,9,2,7,6,5,4),",
				"   (1,3,10,8,9,2,7,6,5,4), (1,3,10,9,2,7,6,5,4,8),",
				"   (1,4,5,6,7,2,3,10,9,8), (1,4,5,6,7,2,9,10,3,8).",
				"a(13) = 0 since 8 is the unique j among 1 ,..., 12 with 13*j-1 prime."
			],
			"mathematica": [
				"(* A program to compute required circular permutations for n = 8. To get \"undirected\" circular permutations, we should identify a circular permutation with the one of the opposite direction; for example, (1,8,4,5,6,7,2,3) is identical to (1,3,2,7,6,5,4,8) if we ignore direction. Thus, a(8) is half of the number of circular permutations yielded by this program. *)",
				"V[i_]:=V[i]=Part[Permutations[{2,3,4,5,6,7,8}],i]",
				"f[i_,j_]:=f[i,j]=PrimeQ[i*j-1]",
				"m=0",
				"Do[Do[If[f[If[j==0,1,Part[V[i],j]],If[j\u003c7,Part[V[i],j+1],1]]==False,Goto[aa]],{j,0,7}];",
				"m=m+1;Print[m,\":\",\" \",1,\" \",Part[V[i],1],\" \",Part[V[i],2],\" \",Part[V[i],3],\" \",Part[V[i],4],\" \",Part[V[i],5],\" \",Part[V[i],6],\" \",Part[V[i],7]];Label[aa];Continue,{i,1,7!}]"
			],
			"xref": [
				"Cf. A051252, A227456, A228917, A228956, A229082."
			],
			"keyword": "nonn,more,hard",
			"offset": "1,6",
			"author": "_Zhi-Wei Sun_, Sep 16 2013",
			"references": 0,
			"revision": 19,
			"time": "2019-08-04T11:40:58-04:00",
			"created": "2013-09-17T03:09:31-04:00"
		}
	]
}