{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A273894",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 273894,
			"data": "1,-1,1,1,0,-2,1,-1,1,2,-5,2,4,-4,1,1,0,-4,2,12,-14,-20,48,-14,-50,60,-10,-28,24,-8,1,-1,1,4,-10,-8,54,-24,-180,270,270,-960,150,2064,-2040,-2352,5871,-1566,-7236,8880,120,-9120,7980,120,-5340,4212,-756",
			"name": "Coefficients of iterations of polynomial x^2-x",
			"comment": [
				"Table T(n,k), n \u003e=0, k=1..2^n, listed by rows.",
				"Let p(0) = t, p(n) = p(n-1)^2 - p(n-1) for i \u003e= 1.",
				"T(n,k) is coefficient of t^k in p(n).",
				"Rows sum to 0, except for row 0. - _David A. Corneth_, Jun 02 2016"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A273894/b273894.txt\"\u003eTable of n, a(n) for n = 0..11212\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = -T(n-1,k) + Sum_{j=1..k-1} T(n-1,j) T(n-1,k-j).",
				"Column k is of the form",
				"T(n,k) = b_k(n) + (-1)^n*c_k(n)",
				"where b_k and c_k seem to be polynomials of degree floor(k/2) - 1 and floor((k-1)/2) respectively (except b_1 = 0).",
				"Leading coefficient of b_k(n) + (-1)^n*c_k(n) seems to be",
				"  -(-2)^(k/2-2) - binomial(-3/2,k/2-1)*2^(k/2-2)*(-1)^n if k is even,",
				"  2^((k-1)/2)*binomial(-1/2,(k-1)/2)*(-1)^n if k is odd.",
				"T(n,1) = (-1)^n = A033999(n).",
				"T(n,2) = 1/2 + (-1)^n/2 = A000035(n)",
				"T(n,3) = -1/2 + (-n + 1/2)*(-1)^n = -A137501(n).",
				"T(n,4) = -n + 5/4 + (3*n/2 - 5/4)*(-1)^n)",
				"  = A001477(n/2) if n is even, -5*A001477((n-1)/2) if n is odd.",
				"T(n,5) = 2*n - 11/4 + (3*n^2/2 - 5*n + 11/4)*(-1)^n",
				"  = 12*A161680(n/2) if n is even, -2*A270710((n-3)/2) if n \u003e= 3 is odd.",
				"T(n, 2^n) = 1 = A000012(n). - _David A. Corneth_, Jun 02 2016"
			],
			"example": [
				"Table starts",
				"1",
				"-1, 1",
				"1, 0, -2, 1",
				"-1, 1, 2, -5, 2, 4, -4, 1",
				"1, 0, -4, 2, 12, -14, -20, 48, -14, -50, 60, -10, -28, 24, -8, 1"
			],
			"maple": [
				"P[0]:= t:",
				"for n from 1 to 8 do",
				"  P[n]:= expand(P[n-1]^2 - P[n-1])",
				"od:",
				"seq(seq(coeff(P[n],t,j),j=1..2^n),n=0..8);"
			],
			"mathematica": [
				"CoefficientList[NestList[Expand[#^2-#]\u0026, x, 5]/x, x] // Flatten (* _Jean-François Alcover_, Apr 29 2019 *)"
			],
			"xref": [
				"Cf. A000035, A001477, A033999, A137501, A161680, A270710."
			],
			"keyword": "sign,tabf",
			"offset": "0,6",
			"author": "_Robert Israel_, Jun 02 2016",
			"references": 1,
			"revision": 27,
			"time": "2019-04-29T08:24:06-04:00",
			"created": "2016-06-03T14:51:05-04:00"
		}
	]
}