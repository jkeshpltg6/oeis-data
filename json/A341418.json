{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A341418",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 341418,
			"data": "1,1,1,0,2,1,0,1,3,1,-1,0,3,4,1,0,-2,1,6,5,1,-1,-2,-3,4,10,6,1,0,-2,-6,-3,10,15,7,1,0,-2,-6,-12,0,20,21,8,1,0,1,-6,-16,-19,9,35,28,9,1,0,0,0,-16,-35,-24,28,56,36,10,1,1,2,3,-6,-40,-65,-21,62,84,45,11",
			"name": "Triangle read by rows: T(n, m) gives the sum of the weights of weighted compositions of n with m parts from generalized pentagonal numbers {A001318(k)}_{k\u003e=1}.",
			"comment": [
				"The sums of row n are given in A000041(n), for n \u003e= 1 (number of partitions).",
				"A differently signed triangle is A047265.",
				"One could add a column m = 0 starting at n = 0 with T(0, 0) = 1 and T(n, 0) = 0 otherwise, by including the empty partition with no parts.",
				"For the weights w of positive integer numbers n see a comment in A339885. It is  w(n) = -A010815(n), for n \u003e= 0. Also  w(n) = A257028(n), for n \u003e= 1.",
				"The weight of a composition is the one of the respective partition, obtained by the product of the weights of the parts.",
				"That the row sums give the number of partitions follows from the pentagonal number theorem. See also the Apr 04 2013 conjecture in A000041 by _Gary W. Adamson_, and the hint for the proof by _Joerg Arndt_. The INVERT map of A = {1, 1, 0, 0, -5, -7, ...}, with offset 1, gives the A000041(n) numbers, for n \u003e= 0.",
				"If the above mentioned column for m = 0, starting at n = 0 is added this is an ordinary convolution triangle of the Riordan type R(1, f(x)), with f(x) = -(Product_{j\u003e=1} (1 - x^j) - 1), generating {A257628(n)_{n\u003e=0}. See the formulae below. - _Wolfdieter Lang_, Feb 16 2021"
			],
			"link": [
				"Wikipedia, \u003ca href=\"https://www.wikipedia.org/wiki/Pentagonal_number_theorem\"\u003ePentagonal number theorem\u003c/a\u003e."
			],
			"formula": [
				"T(n, m) = Sum_{j=1..p(n,m)} w(Part(n, m, j))*M0(n, m, j), where p(n, m) = A008284(n, m), M0(n, m, j) are the multinomials from A048996, i.e., m!/Prod_{k=1..m} e(n,m,j,k)! with the exponents of the parts, and the ternary weight of the j-th partition of n with m parts Part(n,m,j), in Abramowitz-Stegun order, is defined as the product of the weights of the parts, using w(n) = -A010815(n), for n \u003e= 1, and m = 1, 2, ..., n.",
				"From _Wolfdieter Lang_, Feb 16 2021: (Start)",
				"G.f. column m: G(m, x) =  ( -(Product_{j\u003e=1} (1 - x^j) - 1) )^m, for  m \u003e= 1.",
				"G.f. of row polynomials R(n, x) = Sum_{m=1..n}, that is g. f. of the triangle:",
				"  GfT(z, x) = 1/(1 - x*G(1, z)) - 1. Riordan triangle (without m = 0 column). (End)"
			],
			"example": [
				"The triangle T(n, m) begins:",
				"n\\m   1  2  3   4   5   6    7    8    9   10  11  12  13  14  15 16 17 ... A41",
				"-------------------------------------------------------------------------------",
				"1:    1                                                                       1",
				"2:    1  1                                                                    2",
				"3:    0  2  1                                                                 3",
				"4:    0  1  3   1                                                             5",
				"5:   -1  0  3   4   1                                                         7",
				"6:    0 -2  1   6   5   1                                                    11",
				"7:   -1 -2 -3   4  10   6    1                                               15",
				"8:    0 -2 -6  -3  10  15    7   1                                           22",
				"9:    0 -2 -6 -12   0  20   21   8     1                                     30",
				"10:   0  1 -6 -16 -19   9   35   28    9    1                                42",
				"11:   0  0  0 -16 -35 -24   28   56   36   10   1                            56",
				"12:   1  2  3  -6 -40 -65  -21   62   84   45  11   1                        77",
				"13:   0  2  6   8 -25 -90 -105    0  117  120  55  12   1                   101",
				"14:   0  3  9  18  10 -75 -181 -148   54  200 165  66  13  1                135",
				"15:   1  0  8  28  45  -6 -189 -328 -177  162 319 220  78  14  1            176",
				"16:   0  2  6  26  75  90  -77 -419 -540 -160 352 483 286  91  15  1        231",
				"17:   0  0  0  20  80 180  140 -280 -837 -810 -44 660 702 364 105 16 1      297",
				"...",
				"n = 18: 0 0 0 2 60 220 385 140 -755 -1530 -1100 252 1131 987 455 120 17 1,",
				"summing to 385.",
				"n = 19:   0 -2 -6 -12 15 180 546 728 -54 -1730 -2585 -1320 845 1820 1350 560 136",
				"18 1, summing to 490;",
				"n = 20:   0 -2 -6 -23 -45 66 511 1232 1197 -749 -3542 -4059 -1300 1897 2793",
				"1804 680 153 19 1, summing to 627.",
				"...",
				"----------------------------------------------------------------------------------",
				"n = 6: The relevant weighted partitions with parts from the pentagonal numbers and",
				"number of compositions are: m=2: 2*(1,-5)= -2*(1,5), m=3: 1*(2^3), m=4: 3*(1^2,2^2), m=5: 1*(1^4,2), m=6\" 1*(1^6). The other partitions have weight 0.",
				"---------------------------------------------------------------------------------"
			],
			"xref": [
				"Cf. A000041, A008284, A010815, A047265, A257028, -A307059 (alternating row sums), A339885 (for partitions)."
			],
			"keyword": "sign,tabl,easy",
			"offset": "1,5",
			"author": "_Wolfdieter Lang_, Feb 15 2021",
			"references": 4,
			"revision": 18,
			"time": "2021-02-17T03:36:16-05:00",
			"created": "2021-02-15T22:40:30-05:00"
		}
	]
}