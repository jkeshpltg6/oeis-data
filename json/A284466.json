{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A284466",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 284466,
			"data": "1,1,1,2,1,2,6,2,1,20,8,2,60,2,10,450,1,2,726,2,140,3321,14,2,5896,572,16,26426,264,2,394406,2,1,226020,20,51886,961584,2,22,2044895,38740,2,20959503,2,676,478164163,26,2,56849086,31201,652968,184947044,980,2,1273706934,6620376,153366,1803937344",
			"name": "Number of compositions (ordered partitions) of n into odd divisors of n.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A284466/b284466.txt\"\u003eTable of n, a(n) for n = 0..2000\u003c/a\u003e",
				"\u003ca href=\"/index/Com#comp\"\u003eIndex entries for sequences related to compositions\u003c/a\u003e"
			],
			"formula": [
				"a(n) = [x^n] 1/(1 - Sum_{d|n, d positive odd} x^d).",
				"a(n) = 1 if n is a power of 2.",
				"a(n) = 2 if n is an odd prime."
			],
			"example": [
				"a(10) = 8 because 10 has 4 divisors {1, 2, 5, 10} among which 2 are odd {1, 5} therefore we have [5, 5], [5, 1, 1, 1, 1, 1], [1, 5, 1, 1, 1, 1], [1, 1, 5, 1, 1, 1], [1, 1, 1, 5, 1, 1], [1, 1, 1, 1, 5, 1], [1, 1, 1, 1, 1, 5] and [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]."
			],
			"maple": [
				"with(numtheory):",
				"a:= proc(n) option remember; local b, l;",
				"      l, b:= select(x-\u003e is(x:: odd), divisors(n)),",
				"      proc(m) option remember; `if`(m=0, 1,",
				"         add(`if`(j\u003em, 0, b(m-j)), j=l))",
				"      end; b(n)",
				"    end:",
				"seq(a(n), n=0..60);  # _Alois P. Heinz_, Mar 30 2017"
			],
			"mathematica": [
				"Table[d = Divisors[n]; Coefficient[Series[1/(1 - Sum[Boole[Mod[d[[k]], 2] == 1] x^d[[k]], {k, Length[d]}]), {x, 0, n}], x, n], {n, 0, 57}]"
			],
			"program": [
				"(Python)",
				"from sympy import divisors",
				"from sympy.core.cache import cacheit",
				"@cacheit",
				"def a(n):",
				"    l=[x for x in divisors(n) if x%2]",
				"    @cacheit",
				"    def b(m): return 1 if m==0 else sum(b(m - j) for j in l if j \u003c= m)",
				"    return b(n)",
				"print([a(n) for n in range(61)]) # _Indranil Ghosh_, Aug 01 2017, after Maple code"
			],
			"xref": [
				"Cf. A000045, A005408, A032021, A100346, A171565."
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Ilya Gutkovskiy_, Mar 27 2017",
			"references": 2,
			"revision": 20,
			"time": "2021-04-21T11:25:22-04:00",
			"created": "2017-03-28T14:58:48-04:00"
		}
	]
}