{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A320962",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 320962,
			"data": "0,1,-2,10,-90,1248,-24360,631440,-20865600,852647040,-42085008000,2462394816000,-168193308729600,13241729554099200,-1188734048799897600,120563962753538304000,-13704613258628388096000,1733764260005567741952000,-242606144946628642443264000",
			"name": "a(n) = (-1)^(n-1)*(n-1)!*Sum_{i=0..n} Stirling2(n, i) if n \u003e 0 and 0 otherwise.",
			"comment": [
				"Also the exponential limit as defined in A320956 of log(x + 1)."
			],
			"example": [
				"Illustration of the convergence in the sense of A320956:",
				"  [0] 0, 0,  0,  0,   0,    0,      0,      0,         0, ...",
				"  [1] 0, 1, -1,  2,  -6,   24,   -120,    720,     -5040, ... A133942",
				"  [2] 0, 1, -2,  8, -48,  384,  -3840,  46080,   -645120, ... A000165",
				"  [3] 0, 1, -2, 10, -84,  984, -14640, 262800,  -5513760, ... A321398",
				"  [4] 0, 1, -2, 10, -90, 1224, -22440, 514800, -14086800, ...",
				"  [5] 0, 1, -2, 10, -90, 1248, -24240, 615600, -19378800, ...",
				"  [6] 0, 1, -2, 10, -90, 1248, -24360, 630720, -20719440, ...",
				"  [7] 0, 1, -2, 10, -90, 1248, -24360, 631440, -20860560, ...",
				"  [8] 0, 1, -2, 10, -90, 1248, -24360, 631440, -20865600, ..."
			],
			"maple": [
				"a := n -\u003e `if`(n=0, 0, (-1)^(n-1)*(n-1)!*add(Stirling2(n, i), i=0..n)):",
				"seq(a(n), n=0..19);",
				"# Alternatively use the function ExpLim defined in A320956.",
				"ExpLim(19, x -\u003e ln(x+1));"
			],
			"mathematica": [
				"a[n_] := If[n == 0, 0, (-1)^(n - 1)*(n - 1)!*Sum[StirlingS2[n, i], {i, 0, n}]]; Array[a, 19, 0] (* _Amiram Eldar_, Nov 07 2018 *)"
			],
			"program": [
				"(PARI) a(n) = if (n\u003e0, (-1)^(n-1)*(n-1)!*sum(i=0, n, stirling(n, i, 2)), 0); \\\\ _Michel Marcus_, Nov 07 2018"
			],
			"xref": [
				"Cf. A320956, A133942 (n=1), A000165 (n=2), A321398 (n=3)."
			],
			"keyword": "sign",
			"offset": "0,3",
			"author": "_Peter Luschny_, Nov 07 2018",
			"references": 7,
			"revision": 16,
			"time": "2018-11-10T16:36:16-05:00",
			"created": "2018-11-07T18:56:29-05:00"
		}
	]
}