{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A336818",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 336818,
			"data": "4,8,4,8,12,4,8,32,12,4,8,64,36,12,4,8,104,96,36,12,4,8,176,240,100,36,12,4,8,296,520,280,100,36,12,4,0,496,1048,728,284,100,36,12,4,0,848,2104,1816,776,184,100,36,12,4,0,1392,4168,4176,2112,780,284,100,36,12,4",
			"name": "Table read by antidiagonals: T(b,n) is the number of n-step self avoiding walks on a 2D square grid confined inside a square box of size 2b X 2b where the walk starts at the middle of the box.",
			"link": [
				"A. R. Conway et al., \u003ca href=\"http://dx.doi.org/10.1088/0305-4470/26/7/012\"\u003eAlgebraic techniques for enumerating self-avoiding walks on the square lattice\u003c/a\u003e, J. Phys A 26 (1993) 1519-1534.",
				"A. J. Guttmann and A. R. Conway, \u003ca href=\"http://dx.doi.org/10.1007/PL00013842\"\u003eSelf-Avoiding Walks and Polygons\u003c/a\u003e, Annals of Combinatorics 5 (2001) 319-345."
			],
			"formula": [
				"For n \u003c= b, T(b,n) = A001411(n).",
				"For n \u003e= b^2, T(b,n) = 0 as the walks have more steps than there are free grid points inside the box."
			],
			"example": [
				"T(1,3) = 8. The one 3-step walk taking a first step to the right followed by a step upward is:",
				".",
				"+--+",
				"   |",
				"*--+",
				".",
				"This walk can take a downward second step, and also have a first step in the four possible directions, given a total of 1*2*4 = 8 total walks.",
				".",
				"The table begins:",
				".",
				"4  8  8   8   8   8    8    8     0     0      0      0      0       0       0...",
				"4 12 32  64 104 176  296  496   848  1392   2280   3624   5472    8200   10920...",
				"4 12 36  96 240 520 1048 2104  4168  8288  16488  32536  64680  126560  248328...",
				"4 12 36 100 280 728 1816 4176  9304 20400  44216  95680 206104  442984  953720...",
				"4 12 36 100 284 776 2112 5448 13704 32824  77232 178552 409144  932152 2113736...",
				"4 12 36 100 284 780 2168 5848 15672 40472 102816 252992 615328 1472808 3501200...",
				"4 12 36 100 284 780 2172 5912 16192 43360 115328 298856 765864 1919328 4770784...",
				"4 12 36 100 284 780 2172 5916 16264 44016 119392 318328 843848 2194920 5664648...",
				"4 12 36 100 284 780 2172 5916 16268 44096 120200 323856 872920 2321600 6146400...",
				"4 12 36 100 284 780 2172 5916 16268 44100 120288 324832 880232 2363520 6344240...",
				"4 12 36 100 284 780 2172 5916 16268 44100 120292 324928 881392 2372968 6402928...",
				"4 12 36 100 284 780 2172 5916 16268 44100 120292 324932 881496 2374328 6414896...",
				"4 12 36 100 284 780 2172 5916 16268 44100 120292 324932 881500 2374440 6416472...",
				"4 12 36 100 284 780 2172 5916 16268 44100 120292 324932 881500 2374444 6416592...",
				"4 12 36 100 284 780 2172 5916 16268 44100 120292 324932 881500 2374444 6416596...",
				"..."
			],
			"xref": [
				"Cf. A001411 (b-\u003einfinity), A336872 (start on edge of box), A116903, A038373."
			],
			"keyword": "nonn,walk,tabl",
			"offset": "1,1",
			"author": "_Scott R. Shannon_, Aug 06 2020",
			"references": 2,
			"revision": 28,
			"time": "2021-02-21T02:08:54-05:00",
			"created": "2020-08-09T22:38:22-04:00"
		}
	]
}