{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A189226",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 189226,
			"data": "-11,21,24,28,40,52,61,157,76,85,96,117,120,132,181,213,237,376,388,397,132,156,160,189,204,205,216",
			"name": "Curvatures in the nickel-dime-quarter Apollonian circle packing, ordered first by generation and then by size.",
			"comment": [
				"For a circle, curvature = 1/radius. The curvatures of a quarter, nickel, and dime are approximately proportional to 21, 24, and 28, respectively. Three mutually tangent circles with curvatures 21, 24, 28 can be inscribed in a circle of curvature 11.",
				"Apollonius's and Descartes's Theorems say that, given three mutually tangent circles of curvatures a, b, c, there are exactly two circles tangent to all three, and their curvatures are a + b + c +- 2*sqrt(ab + ac + bc). (Here negative curvature of one of the two circles means that the three circles are inscribed in it.)",
				"Fuchs (2009) says \"An Apollonian circle packing ... is made by repeatedly inscribing circles into the triangular interstices in a Descartes configuration of four mutually tangent circles. Remarkably, if the original four circles have integer curvature, all of the circles in the packing will have integer curvature as well.\" That is because if a + b + c - 2s*qrt(ab + ac + bc) is an integer, then so is a + b + c + 2*sqrt(ab + ac + bc).",
				"For n \u003e 1, the n-th generation of the packing has 4*3^(n-2) circles.",
				"Infinitely many of the curvatures are prime numbers A189227. In fact, in any integral Apollonian circle packing that is primitive (i.e., the curvatures have no common factor), the prime curvatures constitute a positive fraction of all primes (Bourgain 2012) and there are infinitely many pairs of tangent circles both of whose curvatures are prime (Sarnak 2007, 2011).",
				"Fuchs and Sanden (2012) report on experiments with the nickel-dime-quarter Apollonian circle packing, which they call the coins packing P_C."
			],
			"link": [
				"D. Austin, \u003ca href=\"http://www.ams.org/samplings/feature-column/fcarc-kissing\"\u003eWhen Kissing Involves Trigonometry\u003c/a\u003e, AMS feature column March 2006.",
				"J. Bourgain, \u003ca href=\"http://arxiv.org/abs/1105.5127\"\u003eIntegral Apollonian circle packings and prime curvatures\u003c/a\u003e, arXiv:1105.5127 [math.NT], 2011-2012.",
				"J. Bourgain and A. Kontorovich, \u003ca href=\"http://arxiv.org/abs/1205.4416\"\u003eOn the Strong Density Conjecture for Integral Apollonian Circle Packings\u003c/a\u003e, arXiv:1205.4416 [math.NT], 2012-2013.",
				"S. Butler, R. Graham, G. Guettler and C. Mallows, \u003ca href=\"http://www.math.ucsd.edu/~ronspubs/10_07_irreducible.pdf\"\u003eIrreducible Apollonian configurations and packings\u003c/a\u003e, Discrete \u0026 Computational Geometry, 44 (2010), 487-507.",
				"E. Fuchs, \u003ca href=\"http://math.berkeley.edu/~efuchs/efuchsthesis.pdf\"\u003eArithmetic Properties of Apollonian Circle Packings\u003c/a\u003e, Ph.D. thesis 2009.",
				"E. Fuchs and K. Sanden, \u003ca href=\"http://arxiv.org/abs/1001.1406\"\u003eSome experiments with integral Apollonian circle packings\u003c/a\u003e, Experiment. Math. 20 (2011), 380-399.",
				"R. L. Graham, J. C. Lagarias, C. L. Mallows, Allan Wilks, and C. H. Yan, \u003ca href=\"http://arxiv.org/abs/math/0009113\"\u003eApollonian Circle Packings: Number Theory\u003c/a\u003e, J. Number Theory, 100 (2003), 1-45.",
				"R. L. Graham, J. C. Lagarias, C. L. Mallows, Allan Wilks, and C. H. Yan, \u003ca href=\"http://arxiv.org/abs/math/0010298\"\u003eApollonian Circle Packings: Geometry and Group Theory I. The Apollonian Group.\u003c/a\u003e, Discrete \u0026 Computational Geometry, 34 (2005), no. 4, 547-585.",
				"K. E. Hirst, \u003ca href=\"http://jlms.oxfordjournals.org/content/s1-42/1/281.extract\"\u003eThe Apollonian Packing of Circles\u003c/a\u003e, J. London Math. Soc. s1-42(1) (1967), 281-291.",
				"E. Kasner and F. Supnick, \u003ca href=\"http://www.pnas.org/content/29/11/378\"\u003eThe Apollonian packing of circles\u003c/a\u003e, Proc. Nat. Acad. Sci. U.S.A. 29 (1943), 378-384.",
				"A. Kontorovich, \u003ca href=\"https://doi.org/10.1090/S0273-0979-2013-01402-2\"\u003eFrom Apollonius to Zaremba: Local-global phenomena in thin orbits\u003c/a\u003e, Bull. Amer. Math. Soc., 50 (2013), 187-228.",
				"J. C. Lagarias, C. L. Mallows, and Allan Wilks, \u003ca href=\"http://arxiv.org/abs/math/0101066\"\u003eBeyond the Descartes Circle Theorem\u003c/a\u003e, Amer. Math Monthly, 109 (2002), 338-361.",
				"L. Levine, W. Pegden, C. K. Smart, \u003ca href=\"http://arxiv.org/abs/1208.4839\"\u003eApollonian Structure in the Abelian Sandpile\u003c/a\u003e, arXiv:1208.4839 [math.AP], 2012-2014.",
				"D. Mackenzie, \u003ca href=\"https://www.tcd.ie/Physics/research/groups/foams/media/gasket.pdf\"\u003eA Tisket, a Tasket, an Apollonian Gasket\u003c/a\u003e, American Scientist, 98 (2010).",
				"C. L. Mallows, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL12/Mallows/mallows8.html\"\u003eGrowing Apollonian Packings\u003c/a\u003e, J. Integer Sequences, 12 (2009), article 09.2.1.",
				"I. Peterson, \u003ca href=\"http://www.ac-noumea.nc/maths/amc/docs/circle_game.pdf\"\u003eCircle game\u003c/a\u003e, Science News, 4/21/01.",
				"I. Peterson, \u003ca href=\"https://www.sciencenews.org/article/temple-circles\"\u003eTemple circles\u003c/a\u003e, Math Trek, 4/23/01.",
				"P. Sarnak, \u003ca href=\"http://web.math.princeton.edu/sarnak/AppolonianPackings.pdf\"\u003eLetter to Lagarias on integral Apollonian packings\u003c/a\u003e, June, 2007.",
				"P. Sarnak, \u003ca href=\"http://web.math.princeton.edu/sarnak/InternalApollonianPackings09.pdf\"\u003eIntegral Apollonian packings\u003c/a\u003e, MAA Lecture, Jan 2009.",
				"P. Sarnak, \u003ca href=\"http://www.jstor.org/stable/10.4169/amer.math.monthly.118.04.291\"\u003eIntegral Apollonian packings\u003c/a\u003e, Amer. Math. Monthly, 118 (2011), 291-306.",
				"K. E. Stange, \u003ca href=\"http://arxiv.org/abs/1208.4836\"\u003eThe sensual Apollonian circle packing\u003c/a\u003e, arXiv:1208.4836 [math.NT], 2012-2014.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Apollonian_gasket#Integral_Apollonian_circle_packings\"\u003eIntegral Apollonian circle packings\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Descartes_theorem\"\u003eDescartes' theorem\u003c/a\u003e"
			],
			"formula": [
				"a(n) == 0, 4, 12, 13, 16, or 21 (mod 24)."
			],
			"example": [
				"The 1st-generation curvatures are -11, 21, 24, 28, the 2nd are 40, 52, 61, 157, and the 3rd are 76, 85, 96, 117, 120, 132, 181, 213, 237, 376, 388, 397. The 4th generation begins 132, 156, 160, 189, 204, 205, 216, ....",
				"As 21 + 24 + 28 +- 2*sqrt(21*24 + 21*28 + 24*28) = 157 or -11, the sequence begins -11, 21, 24, 28, ... and 157 is in it.",
				"The primes 157 and 397 are the curvatures of two circles that are tangent."
			],
			"xref": [
				"Cf. A042944, A042946, A042945, A045506, A045673, A045864, A052483, A060790, A135849, A137246, A154636, A154637, A154638, A171090, A189227, A218155, A248938, A265495, A294510."
			],
			"keyword": "sign,more",
			"offset": "1,1",
			"author": "_Jonathan Sondow_, Apr 18 2011",
			"references": 10,
			"revision": 78,
			"time": "2021-05-24T00:16:27-04:00",
			"created": "2011-04-20T17:14:44-04:00"
		}
	]
}