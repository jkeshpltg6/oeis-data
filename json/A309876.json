{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A309876",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 309876,
			"data": "1,1,1,1,2,1,1,3,3,1,1,4,10,4,1,1,5,33,33,5,1,1,6,155,2135,155,6,1,1,7,1043,7013319,7013319,1043,7,1,1,8,12345,1788782616655,29281354514767167,1788782616655,12345,8,1",
			"name": "Number T(n,k) of k-uniform hypergraphs on n unlabeled nodes with at least one (possibly empty) hyperedge; triangle T(n,k), n\u003e=0, 0\u003c=k\u003c=n, read by rows.",
			"comment": [
				"A hypergraph is called k-uniform if all hyperedges have the same cardinality k.",
				"T(n,k) is defined for n,k \u003e= 0.  The triangle contains only the terms with k\u003c=n.  T(n,k) = 0 for k\u003en."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A309876/b309876.txt\"\u003eRows n = 0..14, flattened\u003c/a\u003e",
				"Jianguo Qian, \u003ca href=\"https://doi.org/10.1016/j.disc.2014.03.005\"\u003eEnumeration of unlabeled uniform hypergraphs\u003c/a\u003e, Discrete Math. 326 (2014), 66--74. MR3188989.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Hypergraph\"\u003eHypergraph\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = A309865(n,k) - 1 = A309858(n,k) - 1.",
				"T(n,k) = T(n,n-k) for 0 \u003c= k \u003c= n."
			],
			"example": [
				"T(3,0) = 1: {{}}.",
				"T(3,1) = 3: {1}, {1,2}, {1,2,3}.",
				"T(3,2) = 3: {12}, {12,13}, {12,13,23}.",
				"T(3,3) = 1: {123}.",
				"(Non-isomorphic representatives of the hypergraphs are given.)",
				"Triangle T(n,k) begins:",
				"  1;",
				"  1, 1;",
				"  1, 2,    1;",
				"  1, 3,    3,       1;",
				"  1, 4,   10,       4,       1;",
				"  1, 5,   33,      33,       5,    1;",
				"  1, 6,  155,    2135,     155,    6, 1;",
				"  1, 7, 1043, 7013319, 7013319, 1043, 7, 1;",
				"  ..."
			],
			"maple": [
				"g:= (l, i, n)-\u003e `if`(i=0, `if`(n=0, [[]], []), [seq(map(x-\u003e",
				"     [x[], j], g(l, i-1, n-j))[], j=0..min(l[i], n))]):",
				"h:= (p, v)-\u003e (q-\u003e add((s-\u003e add(`if`(andmap(i-\u003e irem(k[i], p[i]",
				"     /igcd(t, p[i]))=0, [$1..q]), mul((m-\u003e binomial(m, k[i]*m",
				"     /p[i]))(igcd(t, p[i])), i=1..q), 0), t=1..s)/s)(ilcm(seq(",
				"    `if`(k[i]=0, 1, p[i]), i=1..q))), k=g(p, q, v)))(nops(p)):",
				"b:= (n, i, l, v)-\u003e `if`(n=0 or i=1, 2^((p-\u003e h(p, v))([l[], 1$n]))",
				"     /n!, add(b(n-i*j, i-1, [l[], i$j], v)/j!/i^j, j=0..n/i)):",
				"T:= proc(n, k) option remember; `if`(k\u003en-k,",
				"      T(n, n-k), b(n$2, [], k)-1)",
				"    end:",
				"seq(seq(T(n, k), k=0..n), n=0..9);"
			],
			"xref": [
				"Columns k=0-1 give: A000012, A001477.",
				"Row sums give A309868.",
				"T(2n,n) gives A328157.",
				"Cf. A309858, A309865."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Peter Dolland_ and _Alois P. Heinz_, Aug 21 2019",
			"references": 3,
			"revision": 22,
			"time": "2019-10-05T21:06:43-04:00",
			"created": "2019-08-22T20:38:45-04:00"
		}
	]
}