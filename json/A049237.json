{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A049237",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 49237,
			"data": "1,2,2,3,2,3,2,3,3,2,3,3,3,2,3,3,3,2,3,3,3,3,2,3,3,3,3,3,2,3,3,3,3,3,2,3,3,3,3,3,3,2,3,3,3,3,3,3,2,3,3,3,3,3,3,3,2,3,3,3,3,3,3,3,3,2,3,3,3,3,3,3,3,3,2,3,3,3,3,3,3,3,3,3,2,3,3,3,3,3,3,3,3,3,3,2",
			"name": "Quotient n/phi(n) for n in A007694.",
			"comment": [
				"Here phi(n) denotes Euler's totient function A000010.",
				"As n increases, the proportion of 3's seems to approach 100 percent (it is 40 percent for the first 10 results; 82 percent for 100 results; 87.5 percent for 200 results while up to 200 million, for the first 235 results, is 88.51 percent). - _Zoltan Galantai_, Jul 28 2019",
				"From _Bernard Schott_, Jul 30 2019: (Start)",
				"According to [Ecker and Beslin], the quotients n/phi(n) when phi(n) divides n can take only 3 distinct values:",
				"n/phi(n) = 1 iff n = 1,",
				"n/phi(n) = 2 iff n = 2^w, w \u003e= 1,",
				"n/phi(n) = 3 iff n = 2^w * 3^u, w \u003e= 1, u \u003e= 1.",
				"The previous comment follows because between 2^k and 2^(k+1) there are two consecutive integers for which n/phi(n) = 2, and there are floor(k*(log(2)/log(3)) integers of the form 2^b*3^c (b and c\u003e=1) for which n/phi(n) = 3. (End)"
			],
			"reference": [
				"Sárközy A. and Suranyi J., Number Theory Problem Book (in Hungarian), Tankonyvkiado, Budapest, 1972."
			],
			"link": [
				"Jinyuan Wang, \u003ca href=\"/A049237/b049237.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Michael W. Eckert and Scott J. Beslin, \u003ca href=\"https://www.jstor.org/stable/2322340\"\u003eFind all positive integers n such that phi(n) divides n\u003c/a\u003e, AMM, Vol. 93, No 8, Oct. 86, 656-657, E 3037."
			],
			"formula": [
				"n/phi(n) is an integer iff n = 1 or n = 2^w*3^u for w = 1, 2, ... and u = 0, 1, 2, ..."
			],
			"example": [
				"For powers of 2 the quotient is 2.",
				"a(95) = 124416/phi(124416) = 124416/41472 = 3."
			],
			"mathematica": [
				"Select[#/EulerPhi@ # \u0026 /@ Range[10^6], IntegerQ] (* _Michael De Vlieger_, Jul 02 2016 *)"
			],
			"program": [
				"(MAGMA) v:=[m:m in [1..150000]|m mod EulerPhi(m) eq 0];[v[k]/EulerPhi(v[k]):k in [1..#v]]; // _Marius A. Burtea_, Jul 28 2019",
				"(PARI) lista(NN) = for(n=1,NN,if(n%eulerphi(n)==0,print1(n/eulerphi(n),\", \"))); \\\\ _Jinyuan Wang_, Jul 31 2019"
			],
			"xref": [
				"Cf. A000010, A007694, A062356."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Labos Elemer_",
			"ext": [
				"Edited by _M. F. Hasler_, Jul 02 2016"
			],
			"references": 7,
			"revision": 60,
			"time": "2020-01-18T23:02:50-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}