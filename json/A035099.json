{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A035099",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 35099,
			"data": "1,40,276,-2048,11202,-49152,184024,-614400,1881471,-5373952,14478180,-37122048,91231550,-216072192,495248952,-1102430208,2390434947,-5061476352,10487167336,-21301241856,42481784514,-83300614144",
			"name": "McKay-Thompson series of class 2B for the Monster group with a(0) = 40.",
			"comment": [
				"Also Fourier coefficients of j_2 where j_2 is an analytic isomorphism H/\\Gamma_0(2) -\u003e\\hat{C}.",
				"\"The function j_2 is analogous to j because it is modular (weight zero) for \\Gamma_0(2), holomorphic on the upper half-plane, has a simple pole at infinity, generates the field of \\Gamma_0(2)-modular functions, and defines a bijection of a \\Gamma_0(2) fundamental set with C.\" from the Brent article page 260 using his notation of j_2. - _Michael Somos_, Mar 08 2011",
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"reference": [
				"G. Hoehn, Selbstduale Vertexoperatorsuperalgebren und das Babymonster, Bonner Mathematische Schriften, Vol. 286 (1996), 1-85."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A035099/b035099.txt\"\u003eTable of n, a(n) for n=-1..1000\u003c/a\u003e",
				"R. E. Borcherds, \u003ca href=\"http://www.math.berkeley.edu/~reb/papers/\"\u003eIntroduction to the monster Lie algebra\u003c/a\u003e, pp. 99-107 of M. Liebeck and J. Saxl, editors, Groups, Combinatorics and Geometry (Durham, 1990). London Math. Soc. Lect. Notes 165, Cambridge Univ. Press, 1992.",
				"B. Brent, \u003ca href=\"http://www.emis.de/journals/EM/expmath/volumes/7/7.html\"\u003eQuadratic Minima and Modular Forms, Experimental Mathematics\u003c/a\u003e, v.7 no.3, 257-274.",
				"J. H. Conway and S. P. Norton, \u003ca href=\"https://doi.org/10.1112/blms/11.3.308\"\u003eMonstrous Moonshine\u003c/a\u003e, Bull. Lond. Math. Soc. 11 (1979) 308-339.",
				"D. Ford, J. McKay and S. P. Norton, \u003ca href=\"http://dx.doi.org/10.1080/00927879408825127\"\u003eMore on replicable functions\u003c/a\u003e, Commun. Algebra 22, No. 13, 5175-5193 (1994).",
				"G. Hoehn (gerald(AT)math.ksu.edu), Selbstduale Vertexoperatorsuperalgebren und das Babymonster, Doctoral Dissertation, Univ. Bonn, Jul 15 1995 (\u003ca href=\"http://www.math.ksu.edu/~gerald/papers/dr.pdf\"\u003epdf\u003c/a\u003e, \u003ca href=\"http://www.math.ksu.edu/~gerald/papers/dr.ps.gz\"\u003eps\u003c/a\u003e).",
				"Masao Koike, \u003ca href=\"https://oeis.org/A004016/a004016.pdf\"\u003eModular forms on non-compact arithmetic triangle groups\u003c/a\u003e, Unpublished manuscript [Extensively annotated with OEIS A-numbers by N. J. A. Sloane, Feb 14 2021. I wrote 2005 on the first page but the internal evidence suggests 1997.]",
				"J. McKay and H. Strauss, \u003ca href=\"http://dx.doi.org/10.1080/00927879008823911\"\u003eThe q-series of monstrous moonshine and the decomposition of the head characters\u003c/a\u003e, Comm. Algebra 18 (1990), no. 1, 253-278.",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/MonsterGroup.html\"\u003eMonster Group\u003c/a\u003e",
				"\u003ca href=\"/index/Cor#core\"\u003eIndex entries for \"core\" sequences\u003c/a\u003e"
			],
			"formula": [
				"Expansion of 64 + q^(-1) * (phi(-q) / psi(q))^8 in powers of q where phi(), psi() are Ramanujan theta functions. - _Michael Somos_, Mar 08 2011",
				"Expansion of 64 + (eta(q) / eta(q^2))^24 in powers of q. - _Michael Somos_, Mar 08 2011",
				"j_2 = E_{\\gamma, 2}^2 / E_{\\infty, 4} in the notation of Brent where E_{\\gamma, 2} is g.f. for A004011 and E_{\\infty, 4} is g.f. for A007331. - _Michael Somos_, Mar 08 2011",
				"G.f.: 64 + x^(-1) * (Product_{k\u003e0} 1 + x^k)^(-24). - _Michael Somos_, Mar 08 2011",
				"a(n) ~ (-1)^(n+1) * exp(2*Pi*sqrt(n)) / (2*n^(3/4)). - _Vaclav Kotesovec_, Nov 16 2016"
			],
			"example": [
				"j_2 = 1/q + 40 + 276*q - 2048*q^2 + 11202*q^3 - 49152*q^4 + 184024*q^5 + ..."
			],
			"mathematica": [
				"max = 21; f[x_] := Product[ 1 + x^k, {k, 1, max}]^(-24); coes = CoefficientList[ Series[ f[x], {x, 0, max} ], x]; a[n_] := coes[[n+2]]; a[0] = 40; Table[a[n], {n, -1, max-1}] (* _Jean-François Alcover_, Nov 03 2011, after _Michael Somos_ *)",
				"QP = QPochhammer; s = 64*q + (QP[q]/QP[q^2])^24 + O[q]^30; CoefficientList[s, q] (* _Jean-François Alcover_, Nov 15 2015, after _Michael Somos_ *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c-1, 0, n++; A = x * O(x^n); polcoeff( 64 * x + (eta(x + A) / eta(x^2 + A))^24, n))}; /* _Michael Somos_, Mar 08 2011 */"
			],
			"xref": [
				"Cf. A134786, A045479, A007191, A097340, A035099, A007246, A107080 are all essentially the same sequence."
			],
			"keyword": "easy,sign,nice,core",
			"offset": "-1,2",
			"author": "Barry Brent (barryb(AT)primenet.com)",
			"references": 7,
			"revision": 51,
			"time": "2021-03-12T22:24:42-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}