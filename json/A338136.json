{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A338136",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 338136,
			"data": "6,4,2,3,4,3,3,7,8,9,2,2,10,8,3,15,16,17,4,3,11,21,2,23,20,25,6,27,4,4,7,3,32,24,4,27,9,5,2,39,4,5,10,8,44,45,6,47,48,5,12,51,52,8,3,6,56",
			"name": "a(n) is the least k \u003e= 2 such that (n+1)^k mod n^j is a perfect power \u003e 1 for some j \u003e= 2 with n^j \u003c (n+1)^k.",
			"comment": [
				"a(n) \u003c= n-2 for n \u003e= 4, because (n+1)^(n-2) == (n-1)^2 (mod n^2).",
				"If 2*n+1 is a perfect power, a(n) = 2.",
				"Is there a characterization of the numbers n where a(n) = n-2? Conjecture I: if n \u003c\u003e 7 is a safe prime (A005385), then a(n) = n-2. Conjecture II: if n \u003c\u003e 22, n \u003c\u003e 1822 and n+1 is a safe prime, then a(n) = n-2. - _Chai Wah Wu_, Oct 13 2020",
				"If n is in the sequence with recurrence b(n+2) = 98*b(n+1)-b(n)+32, b(0)=0, b(1)=36, then (n+1)^4 mod n^3 = 1 + 4*n + 6*n^2 is a square, so a(n) \u003c= 4. - _Robert Israel_, Oct 14 2020"
			],
			"link": [
				"Chai Wah Wu, \u003ca href=\"/A338136/b338136.txt\"\u003eTable of n, a(n) for n = 2..5853\u003c/a\u003e"
			],
			"formula": [
				"(n+1)^a(n) mod n^A338150(n) = A338151(n)."
			],
			"example": [
				"a(2) = 6 because 3^6 mod 2^4 = 3^2.",
				"a(13) = 2 because 14^2 mod 13^2 = 3^3."
			],
			"maple": [
				"f:= proc(n) local k,x,j,F;",
				"  for k from 2 to n-2 do",
				"   x:= (n+1)^k;",
				"    for j from 2 to floor(k*log[n](n+1)) do",
				"     F:= ifactors(x mod (n^j))[2];",
				"     if igcd(op(map(t -\u003e t[2],F))) \u003e 1 then return k fi",
				"  od od",
				"end proc:",
				"f(2):= 6: f(3):= 4:",
				"map(f, [$2..40]);"
			],
			"program": [
				"(Python)",
				"from gmpy2 import is_power",
				"def A338136(n):",
				"    k, n2, m = 2,  n**2, (n+1)**2",
				"    while True:",
				"        j, nj = 2, n2",
				"        while nj \u003c m:",
				"            r = m % nj",
				"            if r \u003e 1 and is_power(r):",
				"                return k",
				"            nj *= n",
				"        k += 1",
				"        m *= n+1 # _Chai Wah Wu_, Oct 13 2020"
			],
			"xref": [
				"Cf. A338150, A338151."
			],
			"keyword": "nonn",
			"offset": "2,1",
			"author": "_J. M. Bergot_ and _Robert Israel_, Oct 12 2020",
			"references": 3,
			"revision": 33,
			"time": "2020-10-19T16:00:29-04:00",
			"created": "2020-10-12T20:35:16-04:00"
		}
	]
}