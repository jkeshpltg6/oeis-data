{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A128924",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 128924,
			"data": "1,1,2,2,3,3,1,3,1,1,4,4,4,4,4,2,6,3,4,3,6,2,4,2,1,1,2,4,2,3,2,1,0,3,0,1,2,5,2,2,2,2,2,2,5,4,8,4,8,4,8,4,8,4,8,1,3,2,1,0,1,0,0,1,0,1,2,5,2,2,1,5,0,1,1,2,2,1,4,4,2,2,0,4,0,0,4,0,2,2,4,2,8,2,2,1,4,4,4,4,4,1,2,2,8",
			"name": "T(n,m) is the number of m's in the fundamental period of Fibonacci numbers mod n.",
			"comment": [
				"T(n,m) is the triangle read by rows, 0\u003c=m\u003cn. First column is A001176. Row sums are A001175.",
				"A118965 and A066853 give numbers of zeros and nonzeros in n-th row, respectively. - _Reinhard Zumkeller_, Jan 16 2014",
				"T(n,n) = A235715(n). - _Reinhard Zumkeller_, Jan 17 2014"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A128924/b128924.txt\"\u003eRows n = 1..125 of triangle, flattened\u003c/a\u003e",
				"G. Darvasi and St. Eckmann, \u003ca href=\"https://eudml.org/doc/141577\"\u003eZur Verteilung der Reste der Fibonacci-Folge modulo 5c\u003c/a\u003e, Elemente der Mathematik 50 (1995) pp. 76-80."
			],
			"example": [
				"{F(k) mod 4} has fundamental period (0,1,1,2,3,1), see A079343, with",
				"T(4,0)=1 zero, T(4,1)=3 ones, T(4,2)=1 two's, T(4,3)=1 three's. The triangle starts",
				"1,",
				"1, 2,",
				"2, 3, 3,",
				"1, 3, 1, 1,",
				"4, 4, 4, 4, 4,",
				"2, 6, 3, 4, 3, 6,",
				"2, 4, 2, 1, 1, 2, 4,",
				"2, 3, 2, 1, 0, 3, 0, 1,",
				"2, 5, 2, 2, 2, 2, 2, 2, 5,",
				"4, 8, 4, 8, 4, 8, 4, 8, 4, 8,",
				"1, 3, 2, 1, 0, 1, 0, 0, 1, 0, 1,",
				"2, 5, 2, 2, 1, 5, 0, 1, 1, 2, 2, 1,",
				"4, 4, 2, 2, 0, 4, 0, 0, 4, 0, 2, 2, 4,",
				"2, 8, 2, 2, 1, 4, 4, 4, 4, 4, 1, 2, 2, 8,",
				"2, 3, 3, 2, 3, 3, 2, 3, 3, 2, 3, 3, 2, 3, 3,",
				"2, 3, 4, 1, 0, 3, 0, 1, 2, 3, 0, 1, 0, 3, 0, 1,",
				"4, 4, 2, 2, 4, 2, 0, 0, 2, 2, 0, 0, 2, 4, 2, 2, 4,"
			],
			"maple": [
				"A128924 := proc(m,h)",
				"    local resul,k,M ;",
				"    resul :=0 ;",
				"    for k from 0 to A001175(m)-1 do",
				"        M := combinat[fibonacci](k) mod m ;",
				"        if M = h then",
				"            resul := resul+1 ;",
				"        end if ;",
				"    end do;",
				"    resul ;",
				"end proc:",
				"seq(seq(A128924(m,h),h=0..m-1),m=1..17) ;"
			],
			"mathematica": [
				"A001175[1] = 1; A001175[n_] := For[k = 1, True, k++, If[Mod[Fibonacci[k], n] == 0 \u0026\u0026 Mod[Fibonacci[k+1], n] == 1, Return[k]]]; T[m_, h_] := Module[{resul, k, M}, resul = 0; For[k = 0, k \u003c= A001175[m]-1, k++, M = Mod[Fibonacci[k], m]; If[ M == h, resul++]]; Return[resul]]; Table[T[m, h], {m, 1, 17}, {h, 0, m-1}] // Flatten (* _Jean-François Alcover_, Feb 11 2015, after Maple code *)"
			],
			"program": [
				"(Haskell)",
				"import Data.List (group, sort)",
				"a128924 n k = a128924_tabl !! (n-1) !! (k-1)",
				"a128924_tabl = map a128924_row [1..]",
				"a128924_row 1 = [1]",
				"a128924_row n = f [0..n-1] $ group $ sort $ g 1 ps where",
				"   f []     _                            = []",
				"   f (v:vs) wss'@(ws:wss) | head ws == v = length ws : f vs wss",
				"                          | otherwise    = 0 : f vs wss'",
				"   g 0 (1 : xs) = []",
				"   g _ (x : xs) = x : g x xs",
				"   ps = 1 : 1 : zipWith (\\u v -\u003e (u + v) `mod` n) (tail ps) ps",
				"-- _Reinhard Zumkeller_, Jan 16 2014"
			],
			"xref": [
				"Cf. A053029, A053030, A053031."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_R. J. Mathar_, Apr 25 2007",
			"references": 9,
			"revision": 18,
			"time": "2015-12-09T04:49:03-05:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}