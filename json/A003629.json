{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003629",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3629,
			"id": "M2472",
			"data": "3,5,11,13,19,29,37,43,53,59,61,67,83,101,107,109,131,139,149,157,163,173,179,181,197,211,227,229,251,269,277,283,293,307,317,331,347,349,373,379,389,397,419,421,443,461,467,491,499,509,523,541,547,557,563",
			"name": "Primes p == +- 3 (mod 8), or, primes p such that 2 is not a square mod p.",
			"comment": [
				"Complement of A038873 relative to A000040.",
				"Also primes p such that p divides 2^((p-1)/2) + 1. - _Cino Hilliard_, Sep 04 2004",
				"Primes p such that p^2 == 25 (mod 48), n \u003e 1. - _Gary Detlefs_, Dec 29 2011",
				"This sequence gives the primes p which satisfy C(p, x = 0) = -1, where C(p, x) is the minimal polynomial of 2*cos(Pi/p) (see A187360). For a proof see a comment on C(n, 0) in A230075. - _Wolfdieter Lang_, Oct 24 2013",
				"Except for the initial 3, these are the primes p such that Fibonacci(p) mod 6 = 5. - _Gary Detlefs_, May 26 2014",
				"Inert rational primes in the field Q(sqrt(2)). - _N. J. A. Sloane_, Dec 26 2017",
				"If a prime p is congruent to 3 or 5 (mod 8) and r \u003e 1, then 2^((p-1)*p^(r-1)/2) == -1 (mod p^r). - _Marina Ibrishimova_, Sep 29 2018",
				"For the proofs or the comments by Cino Hilliard and Marina Ibrishimova, see link below. - _Robert Israel_, Apr 24 2019"
			],
			"reference": [
				"Milton Abramowitz and Irene A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), p. 870.",
				"Ronald S. Irving, Integers, Polynomials, and Rings. New York: Springer-Verlag (2004): 274.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Ray Chandler, \u003ca href=\"/A003629/b003629.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (first 1000 terms from T. D. Noe)",
				"Milton Abramowitz and Irene A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"Robert Israel, \u003ca href=\"/A003629/a003629.pdf\"\u003eProof of comments by Hilliard (also conjectured by Detlefs) and Ibrishimova\u003c/a\u003e",
				"\u003ca href=\"/index/Pri#primes_decomp_of\"\u003eIndex to sequences related to decomposition of primes in quadratic fields\u003c/a\u003e"
			],
			"maple": [
				"for n from 2 to 563 do if(ithprime(n)^2 mod 48 = 25) then print(ithprime(n)) fi od. # _Gary Detlefs_, Dec 29 2011"
			],
			"mathematica": [
				"Select[Prime @ Range[2, 105], JacobiSymbol[2, # ] == -1 \u0026] (* _Robert G. Wilson v_, Dec 15 2005 *)",
				"Select[Union[8Range[100] - 5, 8Range[100] - 3], PrimeQ[#] \u0026] (* _Alonso del Arte_, May 22 2016 *)"
			],
			"program": [
				"(PARI) is(n)=isprime(n) \u0026\u0026 (n%8==3 || n%8==5) \\\\ _Charles R Greathouse IV_, Mar 21 2016",
				"(MAGMA) [3] cat [p: p in PrimesUpTo (600) | p^2 mod 48 eq 25]; // _Vincenzo Librandi_, May 23 2016"
			],
			"xref": [
				"Cf. A001132 (complement from the odd primes), A007521 (subsequence), A038873, A226523."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_, _Mira Bernstein_",
			"references": 24,
			"revision": 96,
			"time": "2021-06-09T03:25:08-04:00",
			"created": "1994-09-19T03:00:00-04:00"
		}
	]
}