{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A103710",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 103710,
			"data": "2,2,9,5,5,8,7,1,4,9,3,9,2,6,3,8,0,7,4,0,3,4,2,9,8,0,4,9,1,8,9,4,9,0,3,8,7,5,9,7,8,3,2,2,0,3,6,3,8,5,8,3,4,8,3,9,2,9,9,7,5,3,4,6,6,4,4,1,0,9,6,6,2,6,8,4,1,3,3,1,2,6,6,8,4,0,9,4,4,2,6,2,3,7,8,9,7,6,1,5,5,9,1,7,5",
			"name": "Decimal expansion of the ratio of the length of the latus rectum arc of any parabola to its semi latus rectum: sqrt(2) + log(1 + sqrt(2)).",
			"comment": [
				"The universal parabolic constant, equal to the ratio of the latus rectum arc of any parabola to its focal parameter. Like Pi, it is transcendental.",
				"Just as all circles are similar, all parabolas are similar. Just as the ratio of a semicircle to its radius is always Pi, the ratio of the latus rectum arc of any parabola to its semi latus rectum is sqrt(2) + log(1 + sqrt(2)).",
				"Note the remarkable similarity to sqrt(2) - log(1 + sqrt(2)), the universal equilateral hyperbolic constant A222362, which is a ratio of areas rather than of arc lengths. Lockhart (2012) says \"the arc length integral for the parabola .. is intimately connected to the hyperbolic area integral ... I think it is surprising and wonderful that the length of one conic section is related to the area of another.\"",
				"Is it a coincidence that the universal parabolic constant is equal to 6 times the expected distance A103712 from a randomly selected point in the unit square to its center? (Reese, 2004; Finch, 2012)"
			],
			"reference": [
				"H. Dörrie, 100 Great Problems of Elementary Mathematics, Dover, 1965, Problems 57 and 58.",
				"P. Lockhart, Measurement, Harvard University Press, 2012, p. 369.",
				"C. E. Love, Differential and Integral Calculus, 4th ed., Macmillan, 1950, pp. 286-288.",
				"C. S. Ogilvy, Excursions in Geometry, Oxford Univ. Press, 1969, p. 84.",
				"S. Reese, A universal parabolic constant, 2004, preprint."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A103710/b103710.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"J. L. Diaz-Barrero and W. Seaman, \u003ca href=\"http://www.jstor.org/stable/pdfplus/27646363.pdf?acceptTC=true\"\u003eA limit computed by integration\u003c/a\u003e, Problem 810 and Solution, College Math. J., 37 (2006), 316-318, equation (5).",
				"S. R. Finch, \u003ca href=\"http://arxiv.org/abs/2001.00578\"\u003eMathematical Constants, Errata and Addenda\u003c/a\u003e, 2012, section 8.1.",
				"M. Hajja, \u003ca href=\"https://zbmath.org/?q=an:1291.51018\"\u003eReview Zbl 1291.51018\u003c/a\u003e, zbMATH 2015.",
				"M. Hajja, \u003ca href=\"https://zbmath.org/?q=an:1291.51016\"\u003eReview Zbl 1291.51016\u003c/a\u003e, zbMATH 2015.",
				"H. Khelif, \u003ca href=\"http://images.math.cnrs.fr/L-arbelos-Partie-II.html#nb4\"\u003eL’arbelos, Partie II, Généralisations de l’arbelos\u003c/a\u003e, Images des Mathématiques, CNRS, 2014.",
				"J. Pahikkala, \u003ca href=\"http://planetmath.org/arbelosandparbelos\"\u003eArc Length Of Parabola\u003c/a\u003e, PlanetMath.",
				"S. Reese, \u003ca href=\"http://gaia.adelphi.edu/cgi-bin/makehtmlmov-css.pl?rtsp://gaia.adelphi.edu:554/General_Lectures/Pohle_Colloquiums/pohle200502.mov,pohle200502.mov,256,200\"\u003ePohle Colloquium Video Lecture: The universal parabolic constant, Feb 02 2005\u003c/a\u003e",
				"S. Reese, J. Sondow, Eric W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/UniversalParabolicConstant.html\"\u003eMathWorld: Universal Parabolic Constant\u003c/a\u003e",
				"J. Sondow, \u003ca href=\"http://arxiv.org/abs/1210.2279\"\u003eThe parbelos, a parabolic analog of the arbelos\u003c/a\u003e, arXiv 2012, Amer. Math. Monthly, 120 (2013), 929-935.",
				"E. Tsukerman, \u003ca href=\"http://arxiv.org/abs/1210.5580\"\u003eSolution of Sondow's problem: a synthetic proof of the tangency property of the parbelos\u003c/a\u003e, arXiv 2012, Amer. Math. Monthly, 121 (2014), 438-443.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Universal_parabolic_constant\"\u003eUniversal parabolic constant\u003c/a\u003e",
				"\u003ca href=\"/index/Tra#transcendental\"\u003eIndex entries for transcendental numbers\u003c/a\u003e"
			],
			"formula": [
				"Equals 2*Integral_{x = 0..1} sqrt(1 + x^2) dx. - _Peter Bala_, Feb 28 2019"
			],
			"example": [
				"2.29558714939263807403429804918949038759783220363858348392997534664..."
			],
			"mathematica": [
				"RealDigits[ Sqrt[2] + Log[1 + Sqrt[2]], 10, 111][[1]] (* _Robert G. Wilson v_ Feb 14 2005 *)"
			],
			"program": [
				"(Maxima) fpprec: 100$ ev(bfloat(sqrt(2) + log(1 + sqrt(2)))); /* _Martin Ettl_, Oct 17 2012 */",
				"(PARI) sqrt(2)+log(1+sqrt(2)) \\\\ _Charles R Greathouse IV_, Mar 08 2013"
			],
			"xref": [
				"A002193 + A091648.",
				"Cf. A103711, A103712, A222362, A232716, A232717."
			],
			"keyword": "cons,easy,nonn",
			"offset": "1,1",
			"author": "Sylvester Reese and _Jonathan Sondow_, Feb 13 2005",
			"references": 11,
			"revision": 87,
			"time": "2020-01-17T03:28:03-05:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}