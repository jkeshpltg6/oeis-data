{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A182918",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 182918,
			"data": "1,2,6,1,120,1,1512,1,17280,1,190080,1,1415232000,1,21772800,1,829108224000,1,105082151731200,1,4345502515200000,1,19989311569920000,1,626378114550988800000,1,17896517558599680000,1,944578196742891110400000",
			"name": "Denominators of the swinging Bernoulli number b_n.",
			"comment": [
				"Let zeta(n) denote the Riemann zeta function, B_n the Bernoulli numbers and let [n even] be 1 if n is even, 0 otherwise.",
				"Then 2 zeta(n) [n even] = (2 Pi)^n | B_n | / n! for n \u003e= 2.",
				"Replacing in this formula the factorial of n by the swinging factorial of n (A056040) defines the 'swinging Bernoulli number' b_n.",
				"Then 2 zeta(n) [n even] = (2 Pi)^n b_n / n$ for n \u003e= 2.",
				"Let additionally b_0 = 1 and b_1 = 1/2. The b_n are rational numbers like the Bernoulli numbers; unlike the Bernoulli numbers the swinging Bernoulli numbers are unsigned, bounded in the interval [0,1] and approach 0 for n -\u003e infinity. The numerators of the swinging Bernoulli numbers b_n are abs(A120082(n))."
			],
			"link": [
				"Peter Luschny, \u003ca href=\"/A180000/a180000.pdf\"\u003eDie schwingende Fakultät und Orbitalsysteme\u003c/a\u003e, August 2011."
			],
			"example": [
				"1, 1/2, 1/6, 0, 1/120, 0, 1/1512, 0, 1/17280, 0, 1/190080, .."
			],
			"maple": [
				"swbern:= proc(n) local swfact;",
				"swfact := n -\u003e n!/iquo(n,2)!^2;",
				"if n=0 then 1 elif n=1 then 1/2 else",
				"   if n mod 2 = 1 then 0",
				"   else 2*Zeta(n)*swfact(n)/(2*Pi)^n fi",
				"fi end:",
				"Abs_A120082 := n -\u003e numer(swbern(n));",
				"A182918 := n -\u003e denom(swbern(n));",
				"seq(A182918(i),i=0..20);"
			],
			"mathematica": [
				"sf[n_] := With[{f = Floor[n/2]}, Pochhammer[f+1, n-f]/f!]; a[1] = 2; a[_?OddQ] = 1; a[n_] := 2*Zeta[n]*sf[n]/(2*Pi)^n // Denominator; Table[a[n], {n, 0, 28}] (* _Jean-François Alcover_, Jul 26 2013 *)"
			],
			"xref": [
				"Cf. A120082."
			],
			"keyword": "nonn,frac",
			"offset": "0,2",
			"author": "_Peter Luschny_, Feb 03 2011",
			"references": 2,
			"revision": 16,
			"time": "2020-05-08T17:51:01-04:00",
			"created": "2010-12-13T20:53:31-05:00"
		}
	]
}