{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A108617",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 108617,
			"data": "0,1,1,1,2,1,2,3,3,2,3,5,6,5,3,5,8,11,11,8,5,8,13,19,22,19,13,8,13,21,32,41,41,32,21,13,21,34,53,73,82,73,53,34,21,34,55,87,126,155,155,126,87,55,34,55,89,142,213,281,310,281,213,142,89,55,89,144,231,355,494",
			"name": "Triangle read by rows: T(n,k) = T(n-1,k-1) + T(n-1,k) for 0 \u003c k \u003c n, T(n,0) = T(n,n) = n-th Fibonacci number.",
			"comment": [
				"Sum of n-th row = 2*A027934(n). - _Reinhard Zumkeller_, Oct 07 2012"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A108617/b108617.txt\"\u003eRows n = 0..120 of triangle, flattened\u003c/a\u003e",
				"Hacéne Belbachir and László Szalay, \u003ca href=\"http://siauliaims.su.lt/pdfai/2014/Belb_Szal_2014.pdf\"\u003eOn the Arithmetic Triangles\u003c/a\u003e, Šiauliai Mathematical Seminar, Vol. 9 (17), 2014. See Fig. 1 p. 18.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/FibonacciNumber.html\"\u003eFibonacci Number\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PascalsTriangle.html\"\u003ePascals Triangle\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://www.wikipedia.org/wiki/Fibonacci_number\"\u003eFibonacci number\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Pascal\u0026#39;s_triangle\"\u003ePascal's triangle\u003c/a\u003e",
				"\u003ca href=\"/index/Pas#Pascal\"\u003eIndex entries for triangles and arrays related to Pascal's triangle\u003c/a\u003e"
			],
			"formula": [
				"T(n,0) = T(n,n) = A000045(n);",
				"T(n,1) = T(n,n-1) = A000045(n+1) for n\u003e0;",
				"T(n,2) = T(n,n-2) = A000045(n+2) - 2 = A001911(n-1) for n\u003e1;",
				"sum_{k=0..n} T(n,k) = 2*A027934(n-1) for n\u003e0."
			],
			"example": [
				"Triangle begins:",
				"   0;",
				"   1,   1;",
				"   1,   2,   1;",
				"   2,   3,   3,   2;",
				"   3,   5,   6,   5,   3;",
				"   5,   8,  11,  11,   8,   5;",
				"   8,  13,  19,  22,  19,  13,   8;",
				"  13,  21,  32,  41,  41,  32,  21,  13;",
				"  21,  34,  53,  73,  82,  73,  53,  34,  21;",
				"  34,  55,  87, 126, 155, 155, 126,  87,  55,  34;",
				"  55,  89, 142, 213, 281, 310, 281, 213, 142,  89,  55;"
			],
			"maple": [
				"A108617 := proc(n,k)",
				"    if k = 0 or k=n then",
				"        combinat[fibonacci](n) ;",
				"    elif k \u003c0 or k \u003e n then",
				"        0 ;",
				"    else",
				"        procname(n-1,k-1)+procname(n-1,k) ;",
				"    end if;",
				"end proc: # _R. J. Mathar_, Oct 05 2012"
			],
			"mathematica": [
				"a[1]:={0};a[n_]:=a[n]=Join[{Fibonacci[#]},Map[Total,Partition[a[#],2,1]],{Fibonacci[#]}]\u0026[n-1];Flatten[Map[a,Range[15]]] (* _Peter J. C. Moses_, Apr 11 2013 *)"
			],
			"program": [
				"(Haskell)",
				"a108617 n k = a108617_tabl !! n !! k",
				"a108617_row n = a108617_tabl !! n",
				"a108617_tabl = [0] : iterate f [1,1] where",
				"   f row@(u:v:_) = zipWith (+) ([v - u] ++ row) (row ++ [v - u])",
				"-- _Reinhard Zumkeller_, Oct 07 2012"
			],
			"xref": [
				"Cf. A108037, A007318, A074829."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,5",
			"author": "_Reinhard Zumkeller_, Jun 12 2005",
			"references": 9,
			"revision": 29,
			"time": "2021-09-14T03:46:42-04:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}