{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A073524",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 73524,
			"data": "0,1,2,3,18,2,3,4,6,7,26,4,9,3,4,8,6,4,56,11,3,4,42,4,33,7,5,4,38,5,79,6,4,15,14,8,200,29,13,5,36,3,4,5,7,10,11,8,6,20,47,27,43,9,41,9,10,23,37,17,18,6,7,6,32,15,225,7,73,11,20,12,182,9,16,7,10,15,196,8",
			"name": "Number of steps to reach an integer starting with (n+1)/n and using the map x -\u003e x*ceiling(x); or -1 if no integer is ever reached.",
			"comment": [
				"Computed by doing all computations over the integers (multiply by n) and by truncating modulo n^250. This avoids the explosion of the integers (of order 2^(2^k) after k iterations) and gives the correct answer if the final index i(n) is \u003c 250 (or perhaps 249 or 248). If the algorithm does not stop before 245 one should increase precision (work with n^500 or even higher). - Roland Bacher",
				"Always reaches an integer for n \u003c= 100. - _Roland Bacher_, Aug 30 2002",
				"Always reaches an integer for n \u003c= 200. - _N. J. A. Sloane_, Sep 04 2002",
				"Always reaches an integer for n \u003c= 500 by comparing results with index 1000 and index 2500. - _Robert G. Wilson v_, Sep 11 2002",
				"Always reaches an integer for n \u003c= 3000. The Mathematica program automatically adjusts the modulus m required to find the first integral iterate. - _T. D. Noe_, Apr 10 2006",
				"Always reaches an integer for n \u003c= 5000. - _Ben Branman_, Feb 12 2011"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A073524/b073524.txt\"\u003eTable of n, a(n) for n=1..3000\u003c/a\u003e",
				"J. C. Lagarias and N. J. A. Sloane, Approximate squaring (\u003ca href=\"http://neilsloane.com/doc/apsq.pdf\"\u003epdf\u003c/a\u003e, \u003ca href=\"http://neilsloane.com/doc/apsq.ps\"\u003eps\u003c/a\u003e), Experimental Math., 13 (2004), 113-128."
			],
			"example": [
				"a(7) = 3 since 8/7 -\u003e 16/7 -\u003e 48/7 -\u003e 48."
			],
			"mathematica": [
				"Table[{n, First[Flatten[Position[Map[Denominator, NestList[ # Ceiling[ # ] \u0026, (n + 1)/n, 20]], 1]]]}, {n, 1, 20}]",
				"f[n_] := Block[{k = (n + 1)/n, c = 0}, While[ !IntegerQ[k], c++; k = Mod[k*Ceiling[k], n^250]]; c]; Table[ f[n], {n, 1, 100}]",
				"Table[lim=50; While[k=0; x=1+1/n; m=n^lim; While[k\u003clim-3 \u0026\u0026 !IntegerQ[x], x=Mod[x*Ceiling[x],m]; k++ ]; k==lim-3, lim=2*lim]; k, {n,1000}] (* _T. D. Noe_, Apr 10 2006 *)"
			],
			"xref": [
				"Cf. A073528, A073529, A068119, A001511, A072340, A075102, A073341."
			],
			"keyword": "nonn,nice",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_, Aug 29 2002",
			"ext": [
				"a(5)-a(10), a(12)-a(18), a(20) = 11 from _Ed Pegg Jr_, Aug 29 2002",
				"_T. D. Noe_ also found a(5) and remarks that the final integer is 9.5329600...*10^57734. - Aug 29 2002",
				"a(11) from _T. D. Noe_, who remarks that the final integer is 5.131986636061311...*10^13941166 - Aug 29 2002",
				"a(19) and a(21) onwards from _Roland Bacher_, Aug 30 2002"
			],
			"references": 24,
			"revision": 29,
			"time": "2020-11-20T09:10:50-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}