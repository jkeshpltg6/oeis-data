{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A161293",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 161293,
			"data": "1,0,1,1,2,1,4,2,5,5,8,6,13,10,17,17,24,22,37,32,48,49,66,64,94,88,121,126,162,163,222,218,283,298,370,381,491,498,621,659,798,834,1035,1070,1297,1384,1642,1734,2093,2192,2600,2785,3252,3457,4085,4316,5034,5406,6232",
			"name": "Number of partitions of n into numbers not divisible by 4 where every part appears at least 2 times.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Number of partitions of n into parts divisible by 2 or 3 except multiples of 8 and 12 are excluded. - _Michael Somos_, Jul 08 2009"
			],
			"link": [
				"R. H. Hardin, \u003ca href=\"/A161293/b161293.txt\"\u003eTable of n, a(n) for n=0..802\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"G.f. Product[1+x^(2j)/(1-x^j), j=1..infinity]/Product[1+x^(8j)/(1-x^(4j)), j=1..infinity]. - _Emeric Deutsch_, Jun 21 2009",
				"Expansion of chi(x^3) * chi(x^6) / (chi(-x^2) * chi(-x^4)) in powers of x where chi() is a Ramanujan theta function. - _Michael Somos_, Jul 08 2009",
				"Expansion of q^(1/8) * eta(q^6) * eta(q^8) * eta(q^12)/ ( eta(q^2) * eta(q^3) * eta(q^24) ) in powers of q. - _Michael Somos_, Jul 08 2009",
				"Euler transform of period 24 sequence [ 0, 1, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 1, 0, 0, ...]. - _Michael Somos_, Jul 08 2009",
				"G.f.: Product_{k\u003e0} (1 - x^k + x^(2*k)) * (1 - x^(4*k)) / ( (1 - x^k) * (1 - x^(4*k) + x^(8*k)) ). - _Michael Somos_, Jul 08 2009",
				"Expansion of psi(x^3) * f(-x^8) / (psi(-x^6) * f(-x^2)) in powers of x where psi(), f() are Ramanujan theta functions. - _Michael Somos_, Sep 02 2015",
				"Expansion of psi(x^3) * psi(x^4) / (psi(-x^6) * psi(-x^2)) in powers of x where psi() is a Ramanujan theta function. - _Michael Somos_, Sep 02 2015"
			],
			"example": [
				"a(8) = 5 because we have 3311, 22211, 221111, 2(1^6), and 1^8. - _Emeric Deutsch_, Jun 21 2009",
				"G.f. = 1 + x^2 + x^3 + 2*x^4 + x^5 + 4*x^6 + 2*x^7 + 5*x^8 + 5*x^9 + 8*x^10 + ...",
				"G.f. = 1/q + q^15 + q^23 + 2*q^31 + q^39 + 4*q^47 + 2*q^55 + 5*q^63 + 5*q^71 + ..."
			],
			"maple": [
				"g := (product(1+x^(2*j)/(1-x^j), j = 1 .. 60))/(product(1+x^(8*j)/(1-x^(4*j)), j = 1 .. 60)): gser := series(g, x = 0, 70): seq(coeff(gser, x, n), n = 0 .. 60); # _Emeric Deutsch_, Jun 21 2009"
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ -x^3, x^6] QPochhammer[ -x^6, x^12] QPochhammer[ -x^2, x^2] QPochhammer[-x^4, x^4], {x, 0, n}]; (* _Michael Somos_, Sep 02 2015 *)",
				"a[ n_] := SeriesCoefficient[ (1/2) x^(1/8) EllipticTheta[ 2, 0, x^(3/2)] EllipticTheta[ 2, 0, x^2] / (EllipticTheta[ 2, Pi/4, x] EllipticTheta[ 2, Pi/4, x^3]), {x, 0, n}]; (* _Michael Somos_, Sep 02 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^6 + A) * eta(x^8 + A) * eta(x^12 + A)/ ( eta(x^2 + A) * eta(x^3 + A) * eta(x^24 + A) ), n))}; /* _Michael Somos_, Jul 08 2009 */"
			],
			"keyword": "nonn",
			"offset": "0,5",
			"author": "_R. H. Hardin_, Jun 06 2009",
			"ext": [
				"a(0) = 1 added by _N. J. A. Sloane_, Sep 13 2009"
			],
			"references": 1,
			"revision": 22,
			"time": "2021-03-12T22:24:45-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}