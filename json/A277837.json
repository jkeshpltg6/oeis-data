{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A277837",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 277837,
			"data": "0,0,1,22,343,4664,58985,713307,8367637,96022049,1083677281,12071340713,133059086145,1454047651577,15775044417009,170096123182441,1824418021947873,19478748120713314,207133160219478837,2194788392318245180,23182451824417019723",
			"name": "Number of '7' digits in the set of all numbers from 0 to A014824(n) = Sum_{i=1..n} i*10^(n-i) = (0, 1, 12, 123, 1234, 12345, ...).",
			"comment": [
				"This sequence is very similar (actually equal, for 1 \u003c= n \u003c= 9) to A277635, which was the original motivation for considering the family A277830 - A277838 and A277849. The main difference is that A277635 is based on A007908 (where 123456789 is followed by 12345678910) while this family is based on A014824, starts as the latter at offset 0, and therefore has a strongly different growth for n \u003e 9."
			],
			"link": [
				"David A. Corneth, \u003ca href=\"/A277837/b277837.txt\"\u003eTable of n, a(n) for n = 0..998\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A277849(n) = A083449(n) = A277830(n) - 1 for n \u003c 7,",
				"a(n) = A277836(n) - 8*10^(n-7) [for n \u003e= 7] = A277838(n) + 9*10^(n-8) [for n \u003e= 8].",
				"More generally, for m = 0, ..., 9, let a[m] denote A277830, ..., A277838 and A277849, respectively. Then a[0](n) = a[n](n) = a[m](n) + 1 for all m \u003e n \u003e= 0, and a[m-1](n) = a[m](n) + (m+1)*10^(n-m) for all n \u003e= m \u003e 1."
			],
			"example": [
				"For n=2 there is only one digit '7' in the sequence 0, 1, 2, ..., 12.",
				"For n=3 there are 11 + 10 = 21 more digits '7' in { 17, 27, ..., 67, 70, ..., 79, 87, 97, 107, 117 }, where 77 accounts for two '7's."
			],
			"program": [
				"(PARI) print1(c=N=0);for(n=1,8,print1(\",\"c+=sum(k=N+1,N=N*10+n,#select(d-\u003ed==7,digits(k)))))",
				"(PARI) A277837(n,m=7)=if(n\u003e16,error(\"n\u003e16 not yet implemented\"), n\u003em,A277837(n,m+1)+(m+2)*10^(n-m-1),(9*n-11)*(10^n+1)\\729+2-(m\u003en)) \\\\ Edited by _M. F. Hasler_, Dec 29 2020"
			],
			"xref": [
				"Cf. A277830 - A277838, A277849, A277635, A272525, A083449, A014824."
			],
			"keyword": "nonn,base",
			"offset": "0,4",
			"author": "_M. F. Hasler_, Nov 01 2016",
			"ext": [
				"More terms from _Lars Blomberg_, Nov 05 2016",
				"Removed incorrect b-file. - _David A. Corneth_, Dec 31 2020"
			],
			"references": 4,
			"revision": 21,
			"time": "2021-01-01T11:53:33-05:00",
			"created": "2016-11-03T01:37:16-04:00"
		}
	]
}