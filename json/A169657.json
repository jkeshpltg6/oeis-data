{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A169657",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 169657,
			"data": "12,4320,87091200,158018273280000,37845502865178624000000,1649653134695488211543654400000000,17257672962657131355854388575443353600000000000",
			"name": "The classical Lie superfactorial of type Dr ~ SO(2r) : When a Lie group G is simply laced, the classical Lie superfactorial sf_G is the product of s! where s belongs to the multiset E of exponents of G. Here G=Dr.",
			"comment": [
				"To every simple Lie group G one can associate both a quantum and a classical superfactorial of type G.",
				"The classical Lie superfactorial of type G, denoted sf_G, is defined as the classical limit (q--\u003e1) of the quantum Weyl denominator of G.",
				"If G is simply laced (ADE Dynkin diagrams) ie Ar,Dr,E6,E7,E8 cases, the integer sf_G is the product of s!, where s runs over the multiset of exponents of G.",
				"The usual superfactorial r --\u003e sf[r] is recovered as the Lie superfactorial r --\u003e sf_{Ar} of type Ar [nonascii characters here] SU(r+1), sequence A000178.",
				"The given sequence is the Lie superfactorial of type Dr: r --\u003e sf_{Dr} = (g/2)! Product_{s in 1,3,5,... g-1} s! , with g = 2r-2.",
				"If G is exceptional of type E, the Lie superfactorial does not define an infinite sequence (see A169667).",
				"If G is not simply laced, ie (Br, Cr, G2, F4) cases, the Lie superfactorial is also simply related to the product of factorials s! where s belongs to the multiset E of exponents of G. See sequences A169668.",
				"The classical Lie superfactorial of type G enters the asymptotic expression giving the global dimension of a monoidal category of type G at level k, when k is large.",
				"Call r the rank of G, gamma its Coxeter number, Delta the determinant of the fundamental quadratic form, and dim(G) its dimension, the asymptotic expression reads : k^dim(G) / ((2 pi)^(r gamma) Delta (sf_G)^2 )."
			],
			"link": [
				"R. Coquereaux, \u003ca href=\"http://arxiv.org/abs/1003.2589\"\u003eGlobal dimensions for Lie groups at level k and their conformally exceptional quantum subgroups\u003c/a\u003e, arxiv:1003.2589",
				"R. Coquereaux, \u003ca href=\"http://arxiv.org/abs/1209.6621\"\u003eQuantum McKay correspondence and global dimensions for fusion and module-categories associated with Lie groups\u003c/a\u003e, arXiv preprint arXiv:1209.6621, 2012. - From _N. J. A. Sloane_, Dec 29 2012"
			],
			"mathematica": [
				"sfD[r_] := Factorial[(2 r - 2)/2] Product[Factorial[s], {s, 1, (2 r - 2) - 1, 2}]"
			],
			"xref": [
				"A000178 gives sf_G for G=Ar=SU(r+1). A169667 gives sf_G for G=E6, E7, E8. A169668 describes sf_G for non-simply laced series."
			],
			"keyword": "easy,nonn",
			"offset": "3,1",
			"author": "_Robert Coquereaux_, Apr 05 2010",
			"references": 2,
			"revision": 10,
			"time": "2015-04-01T17:23:20-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}