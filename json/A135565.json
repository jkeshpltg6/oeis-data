{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A135565",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 135565,
			"data": "0,1,3,8,20,42,91,136,288,390,715,756,1508,1722,2835,3088,4896,4320,7923,8360,12180,12782,17963,16344,25600,26494,35451,36456,47908,38310,63395,64800,82368,84082,105315,99972,132756,135014,165243,167720",
			"name": "Number of line segments in regular n-gon with all diagonals drawn.",
			"comment": [
				"A line segment (or edge) is considered to end at any vertex where two or more chords meet.",
				"I.e., edge count of the n-polygon diagonal intersection graph. - _Eric W. Weisstein_, Mar 08 2018"
			],
			"link": [
				"David W. Wilson, \u003ca href=\"/A135565/b135565.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"N. J. A. Sloane (in collaboration with Scott R. Shannon), \u003ca href=\"/A331452/a331452.pdf\"\u003eArt and Sequences\u003c/a\u003e, Slides of guest lecture in Math 640, Rutgers Univ., Feb 8, 2020. Mentions this sequence.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/EdgeCount.html\"\u003eEdge Count\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PolygonDiagonalIntersectionGraph.html\"\u003ePolygon Diagonal Intersection Graph\u003c/a\u003e",
				"\u003ca href=\"/index/Pol#Poonen\"\u003eSequences formed by drawing all diagonals in regular polygon\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A007569(n) + A007678(n) - 1. - _Max Alekseyev_"
			],
			"mathematica": [
				"del[m_, n_] := Boole[Mod[n, m] == 0];",
				"A007569[n_] :=",
				"If[n \u003c 4, n,",
				"  n + Binomial[n, 4] + del[2, n] (-5 n^3 + 45 n^2 - 70 n + 24)/24 -",
				"   del[4, n] (3 n/2) + del[6, n] (-45 n^2 + 262 n)/6 +",
				"   del[12, n]*42 n + del[18, n]*60 n + del[24, n]*35 n -",
				"   del[30, n]*38 n - del[42, n]*82 n - del[60, n]*330 n -",
				"   del[84, n]*144 n - del[90, n]*96 n - del[120, n]*144 n -",
				"   del[210, n]*96 n];",
				"A007678[n_] :=",
				"  If[n \u003c 3,",
				"   0, (n^4 - 6 n^3 + 23 n^2 - 42 n + 24)/24 +",
				"    del[2, n] (-5 n^3 + 42 n^2 - 40 n - 48)/48 - del[4, n] (3 n/4) +",
				"    del[6, n] (-53 n^2 + 310 n)/12 + del[12, n] (49 n/2) +",
				"    del[18, n]*32 n + del[24, n]*19 n - del[30, n]*36 n -",
				"    del[42, n]*50 n - del[60, n]*190 n - del[84, n]*78 n -",
				"    del[90, n]*48 n - del[120, n]*78 n - del[210, n]*48 n];",
				"a[n_] := A007569[n] + A007678[n] - 1;",
				"Array[a, 40] (* _Jean-François Alcover_, Sep 07 2017, after _Max Alekseyev_, using _T. D. Noe_'s code for A007569 and A007678 *)"
			],
			"xref": [
				"Sequences related to chords in a circle: A001006, A054726, A006533, A006561, A006600, A007569, A007678. See also entries for chord diagrams in Index file."
			],
			"keyword": "easy,nice,nonn",
			"offset": "1,3",
			"author": "_Franklin T. Adams-Watters_, Feb 23 2008",
			"references": 19,
			"revision": 18,
			"time": "2020-03-07T11:20:21-05:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}