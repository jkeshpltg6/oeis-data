{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A346722",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 346722,
			"data": "0,2,3,5,6,7,8,9,11,13,14,15,17,37,43,49,55,61,62,64,66,68,69,71,74,76,77,79,81,83,84,86,89,103,107,121,125,128,131,133,135,138,142,145,149,152,154,156,159,163,166,173,175,177,179,197,199,201,203",
			"name": "Use the cells of a hexagonal grid to represent the algebraic integers in the integer ring of Q(sqrt(-11)) as explained in the comments. Number the cells along the counterclockwise hexagonal spiral that starts with cells 0 and 1 representing integers 0 and 1. List the cells that represent 0 or a prime in the ring.",
			"comment": [
				"In this entry we use \"rational integers\" to refer to integers in their usual sense as whole numbers - they form a subset of the algebraic integers that form the ring, which we denote \"R\".",
				"The algebraic integers in R (the elements of R) are specifically quadratic integers of the form z = x + y*sqrt(-11) or z = (x+0.5) + (y+0.5)*sqrt(-11) where x and y are rational integers. Plotted as points on a plane, they can be joined in a grid of isosceles triangles or be seen as the center points of hexagonal regions. Adjusting the regions to be regular hexagons makes for appealing diagrams, which we will come to shortly.",
				"(To be precise, we map each element, z, to the region of the complex plane containing the points that have z as their nearest ring element, then map these (hexagonal) regions continuously to the cells of a (regular) hexagonal grid.)",
				"R is one of 9 related rings that are unique factorization domains, meaning their elements factorize into prime elements in a unique way, just as with rational integers and prime numbers. See the Wikipedia link or the Stark reference, for example.",
				"This set of sequences is inspired by tilings: see the Wichmann link. Each tiling represents one of the 9 rings and shows the primes as distinctively colored squares or hexagons as appropriate.",
				"6 other rings (of the 9) can be mapped to the hexagonal grid in the same way. See the comments entitled \"General properties of the related hexagonal spiral sequences\" in A346721."
			],
			"reference": [
				"L. W. Reid, The Elements of the Theory of Algebraic Numbers, MacMillan, NY, 1910.",
				"H. M. Stark, An Introduction to Number Theory. Markham, Chicago, 1970; Theorem 8.22 on page 295 lists the nine UFDs of the form Q(sqrt(-d)), cf. A003173."
			],
			"link": [
				"OEIS Wiki, \u003ca href=\"http://oeis.org/wiki/Algebraic_integers\"\u003eAlgebraic integers\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ComplexPlane.html\"\u003eComplex Plane\u003c/a\u003e, \u003ca href=\"http://mathworld.wolfram.com/HexagonalGrid.html\"\u003eHexagonal Grid\u003c/a\u003e, \u003ca href=\"http://mathworld.wolfram.com/RingofIntegers.html\"\u003eRing of Integers\u003c/a\u003e.",
				"Brian Wichmann, \u003ca href=\"http://www.tilingsearch.org/special/ufd.pdf\"\u003eTiling for Unique Factorization Domains\u003c/a\u003e, Jul 22 2019. See Figure 6.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Quadratic_integer\"\u003eQuadratic integer\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Unique_factorization_domain\"\u003eUnique factorization domain\u003c/a\u003e."
			],
			"formula": [
				"m is a term if and only if A345764(m) is a term."
			],
			"example": [
				"The sequence is constructed in the same way as A346721, but the relevant prime is 11 instead of 7. See the example section of A346721."
			],
			"xref": [
				"Cf. A003173, A296920, A345764.",
				"Norms of primes in R: A341785.",
				"Equivalent sequences for other Q(sqrt(D)): A345436 (D=-1), A345437 (D=-2), A345435 (D=-3), A346721 (D=-7), A346723 (D=-19), A346724 (D=-43), A346725 (D=-67), A346726 (D=-163)."
			],
			"keyword": "nonn,more",
			"offset": "1,2",
			"author": "_Peter Munn_, Jul 30 2021",
			"references": 5,
			"revision": 24,
			"time": "2021-12-24T12:23:22-05:00",
			"created": "2021-12-24T12:23:22-05:00"
		}
	]
}