{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A349039",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 349039,
			"data": "0,1,1,4,1,4,9,3,3,9,16,7,4,7,16,25,13,7,7,13,25,36,21,12,9,12,21,36,49,31,19,13,13,19,31,49,64,43,28,19,16,19,28,43,64,81,57,39,27,21,21,27,39,57,81,100,73,52,37,28,25,28,37,52,73,100,121,91,67,49,37,31,31,37,49,67,91,121",
			"name": "Square array T(n, k) read by antidiagonals, n, k \u003e= 0; T(n, k) = n^2 - n*k + k^2.",
			"comment": [
				"T(n, k) is the norm of the Eisenstein integer n + k*w (where w = -1/2 + sqrt(-3)/2 is a primitive cube root of unity).",
				"All terms belong to A003136."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A349039/b349039.txt\"\u003eTable of n, a(n) for n = 0..10010\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Eisenstein_integer#Euclidean_domain\"\u003eEisenstein integers: Euclidean domain\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = T(k, n).",
				"T(n, 0) = T(n, n) = n^2.",
				"T(n, k) = A048147(n, k) - A004247(n, k).",
				"G.f.: (x - 5*x*y + y*(1 + y) + x^2*(1 + y^2))/((1 - x)^3*(1 - y)^3). - _Stefano Spezia_, Nov 08 2021"
			],
			"example": [
				"Array T(n, k) begins:",
				"  n\\k|    0   1   2   3   4   5   6   7   8   9   10",
				"  ---+----------------------------------------------",
				"    0|    0   1   4   9  16  25  36  49  64  81  100",
				"    1|    1   1   3   7  13  21  31  43  57  73   91",
				"    2|    4   3   4   7  12  19  28  39  52  67   84",
				"    3|    9   7   7   9  13  19  27  37  49  63   79",
				"    4|   16  13  12  13  16  21  28  37  48  61   76",
				"    5|   25  21  19  19  21  25  31  39  49  61   75",
				"    6|   36  31  28  27  28  31  36  43  52  63   76",
				"    7|   49  43  39  37  37  39  43  49  57  67   79",
				"    8|   64  57  52  49  48  49  52  57  64  73   84",
				"    9|   81  73  67  63  61  61  63  67  73  81   91",
				"   10|  100  91  84  79  76  75  76  79  84  91  100"
			],
			"mathematica": [
				"T[n_, k_] := n^2 - n*k + k^2; Table[T[k, n - k], {n, 0, 11}, {k, 0, n}] // Flatten (* _Amiram Eldar_, Nov 08 2021 *)"
			],
			"program": [
				"(PARI) T(n,k) = n^2 - n*k + k^2"
			],
			"xref": [
				"Cf. A003136, A004247, A048147, A073254."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,4",
			"author": "_Rémy Sigrist_, Nov 06 2021",
			"references": 1,
			"revision": 14,
			"time": "2021-11-09T15:02:28-05:00",
			"created": "2021-11-09T05:37:51-05:00"
		}
	]
}