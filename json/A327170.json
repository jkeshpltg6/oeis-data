{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A327170",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 327170,
			"data": "1,1,0,0,0,1,0,1,0,0,0,2,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,2,0,1,0,0,0,0,0,2,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,1,0,0,0,0,0,0,0,0,0,1",
			"name": "Number of divisors d of n such that A327171(d) (= phi(d)*core(d)) is equal to n.",
			"comment": [
				"From any solution (*) to A327171(d) = d*phi(d) = n, we obtain a solution for core(d')*phi(d') = n by forming a \"pumped up\" version d' of d, by replacing each exponent e_i in the prime factorization of d = p_1^e_1 * p_2^e_2 * ... * p_k^e_k, with exponent 2*e_i - 1 so that d' = p_1^(2*e_1 - 1) * p_2^(2*e_2 - 1)* ... * p_k^(2*e_k - 1) = A102631(d) = d*A003557(d), and this d' is also a divisor of n, as n = d' * A173557(d). Generally, any product m = p_1^(2*e_1 - x) * p_2^(2*e_2 - y)* ... * p_k^(2*e_k - z), where each x, y, ..., z is either 0 or 1 gives a solution for core(m)*phi(m) = n, thus every nonzero term in this sequence is a power of 2, even though not all such m's might be divisors of n.",
				"  (* by necessity unique, see _Franz Vrabec_'s Dec 12 2012 comment in A002618).",
				"On the other hand, if we have any solution d for core(d)*phi(d) = n, we can find the unique such divisor e of d that e*phi(e) = n by setting e = A019554(d).",
				"Thus, it follows that the nonzero terms in this sequence occur exactly at positions given by A082473.",
				"Records (1, 2, 4, 8, 16, ...) occur at n = 1, 12, 504, 223200, 50097600, ..."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A327170/b327170.txt\"\u003eTable of n, a(n) for n = 1..65537\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{d|n} [A000010(d)*A007913(d) == n], where [ ] is the Iverson bracket."
			],
			"example": [
				"For n = 504 = 2^3 * 3^2 * 7, it has 24 divisors, out of which four divisors: 42 (= 2^1 * 3^1 * 7^1), 84 (= 2^2 * 3^1 * 7^1), 126 (= 2^1 * 3^2 * 7^1), 252 (= 2^2 * 3^2 * 7^1) are such that A007913(d)*A000010(d) = 504, thus a(504) = 4."
			],
			"mathematica": [
				"With[{s = Array[EulerPhi[#] (Sqrt@ # /. (c_: 1) a_^(b_: 0) :\u003e (c a^b)^2) \u0026, 120]}, Table[DivisorSum[n, 1 \u0026, s[[#]] == n \u0026], {n, Length@ s}]] (* _Michael De Vlieger_, Sep 29 2019, after _Bill Gosper_ at A007913 *)"
			],
			"program": [
				"(PARI) A327170(n) = sumdiv(n,d,eulerphi(d)*core(d) == n);"
			],
			"xref": [
				"Cf. A000010, A002618, A003557, A007913, A019554, A082473, A102631, A173557, A194507, A327171.",
				"Cf. also A327153, A327166, A327169."
			],
			"keyword": "nonn",
			"offset": "1,12",
			"author": "_Antti Karttunen_, Sep 28 2019",
			"references": 3,
			"revision": 39,
			"time": "2019-09-29T20:23:05-04:00",
			"created": "2019-09-29T20:23:05-04:00"
		}
	]
}