{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097854",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97854,
			"data": "1,1,0,1,0,1,1,0,1,2,1,0,2,2,4,1,0,4,4,4,8,1,0,9,8,8,8,17,1,0,21,18,16,16,17,38,1,0,51,42,36,32,34,38,89,1,0,127,102,84,72,68,76,89,216,1,0,323,254,204,168,153,152,178,216,539,1,0,835,646,508,408,357,342,356,432,539,1374",
			"name": "Triangle read by rows: T(n,k) = number of Motzkin paths of length n and having abscissa of first return (i.e., first down step hitting the x-axis) equal to k (k\u003e0); T(n,0)=1 (accounts for the paths consisting only of level steps).",
			"comment": [
				"Row sums are the Motzkin numbers (A001006)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A097854/b097854.txt\"\u003eTable of n, a(n) for the first 50 rows, flattened\u003c/a\u003e"
			],
			"formula": [
				"G.f.: (1-t*z+t^2*z^2*M(t*z)*M(z) - t^2*z^3*M(t*z)*M(z))/(1-z-t*z+t*z^2), where M(z)=(1-z-sqrt(1-2*z-3*z^2))/(2*z^2) is the g.f. of the Motzkin numbers.",
				"T(n,k) = m(n-k)*Sum_{j=0..k-2} m(j), where m(n) = A001006(n) are the Motzkin numbers."
			],
			"example": [
				"Triangle starts:",
				"  1;",
				"  1, 0;",
				"  1, 0, 1;",
				"  1, 0, 1, 2;",
				"  1, 0, 2, 2, 4;",
				"  1, 0, 4, 4, 4, 8;",
				"Row n has n+1 terms.",
				"T(5,3)=4 because the Motzkin paths of length 5 and having abscissa of first return equal to 3 are HU(D)HH, HU(D)UD, UH(D)HH and UH(D)UD (first returns to axis shown between parentheses); here U=(1,1), H=(1,0) and D=(1,-1)."
			],
			"maple": [
				"G:=(1-t*z+t^2*z^2*M(t*z)*M(z)-t^2*z^3*M(t*z)*M(z))/(1-z-t*z+t*z^2): M:=z-\u003e(1-z-sqrt(1-2*z-3*z^2))/2/z^2: Gser:=simplify(series(G,z=0,14)): P[0]:=1: for n from 1 to 13 do P[n]:=coeff(Gser,z^n) od: seq(seq(coeff(t*P[n],t^k),k=1..n+1),n=0..12); M:=(1-z-sqrt(1-2*z-3*z^2))/2/z^2: Mser:=series(M,z=0,15): m[0]:=1: for n from 1 to 12 do m[n]:=coeff(Mser,z^n) od: T:=proc(n,k) if k=0 then 1 elif k\u003c=n then m[n-k]*sum(m[j],j=0..k-2) else 0 fi end: TT:=(n,k)-\u003eT(n-1,k-1): matrix(11,11,TT); # generates the triangle:"
			],
			"mathematica": [
				"(* m = MotzkinNumber *) m[0] = 1; m[n_] := m[n] = m[n - 1] + Sum[m[k]*m[n - 2 - k], {k, 0, n - 2}]; t[n_, 0] = 1; t[n_, k_] := m[n - k]*Sum[m[j], {j, 0, k - 2}]; Table[t[n, k], {n, 0, 11}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Jul 10 2013 *)"
			],
			"xref": [
				"Cf. A001006."
			],
			"keyword": "nonn,tabl",
			"offset": "0,10",
			"author": "_Emeric Deutsch_, Aug 31 2004",
			"ext": [
				"Keyword tabf changed to tabl by _Michel Marcus_, Apr 09 2013",
				"Terms a(75) and beyond from _G. C. Greubel_, Oct 23 2017"
			],
			"references": 1,
			"revision": 27,
			"time": "2017-10-24T09:31:11-04:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}