{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003273",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3273,
			"id": "M3747",
			"data": "5,6,7,13,14,15,20,21,22,23,24,28,29,30,31,34,37,38,39,41,45,46,47,52,53,54,55,56,60,61,62,63,65,69,70,71,77,78,79,80,84,85,86,87,88,92,93,94,95,96,101,102,103,109,110,111,112,116,117,118,119,120,124,125,126",
			"name": "Congruent numbers: positive integers k for which there exists a right triangle having area k and rational sides.",
			"comment": [
				"Positive integers k such that x^2 + k*y^2 = z^2 and x^2 - k*y^2 = t^2 have simultaneous integer solutions. In other words, k is the difference of an arithmetic progression of three rational squares: (t/y)^2, (x/y)^2, (z/y)^2. Values of k corresponding to y=1 (i.e., an arithmetic progression of three integer squares) form A256418.",
				"Tunnell shows that if a number is squarefree and congruent, then the ratio of the number of solutions of a pair of equations is 2. If the Birch and Swinnerton-Dyer conjecture is assumed, then determining whether a squarefree number k is congruent requires counting the solutions to a pair of equations. For odd k, see A072068 and A072069; for even k see A072070 and A072071.",
				"If a number k is congruent, there are an infinite number of right triangles having rational sides and area k. All congruent numbers can be obtained by multiplying a primitive congruent number A006991 by a positive square number A000290.",
				"Conjectured asymptotics (based on random matrix theory) on p. 453 of Cohen's book. - _Steven Finch_, Apr 23 2009"
			],
			"reference": [
				"Alter, Ronald; Curtz, Thaddeus B.; Kubota, K. K. Remarks and results on congruent numbers. Proceedings of the Third Southeastern Conference on Combinatorics, Graph Theory and Computing (Florida Atlantic Univ., Boca Raton, Fla., 1972), pp. 27-35. Florida Atlantic Univ., Boca Raton, Fla., 1972. MR0349554 (50 #2047)",
				"H. Cohen, Number Theory. I, Tools and Diophantine Equations, Springer-Verlag, 2007, p. 454. [From _Steven Finch_, Apr 23 2009]",
				"R. Cuculière, \"Mille ans de chasse aux nombres congruents\", in Pour la Science (French edition of 'Scientific American'), No. 7, 1987, pp. 14-18.",
				"L. E. Dickson, History of the Theory of Numbers, Vol. 2, pp. 459-472, AMS Chelsea Pub. Providence RI 1999.",
				"R. K. Guy, Unsolved Problems in Number Theory, D27.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A003273/b003273.txt\"\u003eCongruent numbers up to 10000; table of n, a(n) for n = 1..5742\u003c/a\u003e",
				"R. Alter, \u003ca href=\"/A003273/a003273_1.pdf\"\u003eLetter to N. J. A. Sloane, Sep 1975\u003c/a\u003e",
				"R. Alter, \u003ca href=\"http://www.jstor.org/stable/2320381\"\u003eThe congruent number problem\u003c/a\u003e, Amer. Math. Monthly, 87 (1980), 43-45.",
				"R. Alter and T. B. Curtz, \u003ca href=\"http://www.jstor.org/stable/2005838\"\u003eA note on congruent numbers\u003c/a\u003e, Math. Comp., 28 (1974), 303-305 and 30 (1976), 198.",
				"A. Alvarado and E. H. Goins, \u003ca href=\"http://arxiv.org/abs/1210.6612\"\u003eArithmetic progressions on conic sections\u003c/a\u003e, arXiv:1210.6612 [math.NT], 2012. [From _Jonathan Sondow_, Oct 25 2012]",
				"E. Brown, Three Fermat Trails to Elliptic Curves, \u003ca href=\"https://intranet.math.vt.edu/people/brown/doc/ellip.pdf\"\u003e5. Congruent Numbers and Elliptic Curves (pp 8-11/17)\u003c/a\u003e",
				"Graeme Brown, \u003ca href=\"http://www.graemebrownart.com/congruent.pdf\"\u003eThe Congruent Number Problem\u003c/a\u003e, 2014.",
				"B. Cipra, \u003ca href=\"http://sciencenow.sciencemag.org/cgi/content/full/2009/923/3?etoc\"\u003eTallying the class of congruent numbers\u003c/a\u003e, ScienceNOW, Sep 23 2009.",
				"Clay Mathematics Institute, \u003ca href=\"http://www.claymath.org/millenium-problems/birch-and-swinnerton-dyer-conjecture\"\u003eThe Birch and Swinnerton-Dyer Conjecture\u003c/a\u003e",
				"Raiza Corpuz, \u003ca href=\"https://arxiv.org/abs/2006.08113\"\u003eConstructing congruent number elliptic curves using 2-descent\u003c/a\u003e, arXiv:2006.08113 [math.NT], 2020.",
				"R. Cuculière, \u003ca href=\"http://www.numdam.org/item/SPHM_1988___2_A1_0\"\u003eMille ans de chasse aux nombres congruents\u003c/a\u003e, Séminaire de Philosophie et Mathématiques, 2, 1988, p. 1-17.",
				"Department of Pure Maths., Univ. Sheffield, \u003ca href=\"https://web.archive.org/web/20040206183520/http://www.shef.ac.uk:80/~puremath/theorems/congruent.html\"\u003ePythagorean triples and the congruent number problem\u003c/a\u003e [Cached copy]",
				"A. Dujella, A. S.Janfeda, S. Salami, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL12/Janfada/janfada3.html\"\u003eA Search for High Rank Congruent Number Elliptic Curves\u003c/a\u003e, JIS 12 (2009) 09.5.8.",
				"E. V. Eikenberg, \u003ca href=\"http://www.uwyo.edu/bshader/mathtlcalgebra/congruentnumbers.pdf\"\u003eThe Congruent Number Problem\u003c/a\u003e",
				"David Goldberg, \u003ca href=\"https://arxiv.org/abs/2106.07373\"\u003eTriangle Sides for Congruent Numbers less than 10000\u003c/a\u003e, arXiv:2106.07373 [math.NT], 2021.",
				"Lorenz Halbeisen, Norbert Hungerbühler, \u003ca href=\"https://arxiv.org/abs/1809.02037\"\u003eCongruent number elliptic curves with rank at least two\u003c/a\u003e, arXiv:1809.02037 [math.NT], 2018.",
				"Lorenz Halbeisen, Norbert Hungerbühler, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL22/Halbeisen/halb4.html\"\u003e Congruent Number Elliptic Curves Related to Integral Solutions of m^2 = n^2 + nl + n^2\u003c/a\u003e, J. Int. Seq., Vol. 22 (2019), Article 19.3.1.",
				"Alvaro Lozano-Robledo, \u003ca href=\"https://www.youtube.com/watch?v=Q-tNzDFnzKI\"\u003eMy #MegaFavNumber: 224,403,517,704,336,969,924,557,513,090,674,863,160,948,472,041\u003c/a\u003e, video (2020) [discusses congruent numbers and a(157)]",
				"W. F. Hammond, \u003ca href=\"http://math.albany.edu:8000/math/pers/hammond/Presen/rsumo.html\"\u003eA Reading of Karl Rubin's SUMO Slides on Rational Right Triangles and Elliptic Curves\u003c/a\u003e",
				"Bill Hart, \u003ca href=\"http://aimath.org/news/congruentnumbers/\"\u003eA Trillion Triangles\u003c/a\u003e, American Institute of Mathematics.",
				"T. Komatsu, \u003ca href=\"http://www.fq.math.ca/Abstracts/50-3/komatsu.pdf\"\u003eCongruent numbers and continued fractions\u003c/a\u003e, Fib. Quart., 50 (2012), 222-226. - From _N. J. A. Sloane_, Mar 04 2013",
				"S. Komoto, T. Watanabe and H. Wada, \u003ca href=\"http://jant.jsiam.org/pub/ac05/Wada/wada.pdf\"\u003e42553 is a congruent number\u003c/a\u003e.",
				"G. Kramarz, \u003ca href=\"http://dx.doi.org/10.1007/BF01451411\"\u003eAll congruent numbers less than 2000\u003c/a\u003e, Math. Annalen, 273 (1986), 337-340.",
				"G. Kramarz, \u003ca href=\"/A003273/a003273.pdf\"\u003e All congruent numbers less than 2000\u003c/a\u003e, Math. Annalen, 273 (1986), 337-340. [Annotated, corrected, scanned copy]",
				"Allan J. MacLeod, \u003ca href=\"https://arxiv.org/abs/2005.02615\"\u003eThe congruent number descent of Komotu, Watanabe and Wada\u003c/a\u003e, arXiv:2005.02615 [math.NT], 2020.",
				"MathDL, \u003ca href=\"http://mathdl.maa.org/mathDL/?pa=mathNews\u0026amp;sa=view\u0026amp;newsId=680\"\u003eFive Mathematicians Capture Record Number of Congruent Numbers\u003c/a\u003e",
				"Fidel Ronquillo Nemenzo, \u003ca href=\"https://doi.org/10.3792/pjaa.74.29\"\u003eAll congruent numbers less than 40000\u003c/a\u003e, Proc. Japan Acad. Ser. A Math. Sci., Volume 74, Number 1 (1998), 29-31.",
				"Karl Rubin, \u003ca href=\"http://www.math.uci.edu/~krubin/lectures/rossweb.pdf\"\u003eRight triangles and elliptic curves\u003c/a\u003e",
				"W. A. Stein, \u003ca href=\"http://wstein.org/edu/Fall2001/124/lectures/lecture33/html\"\u003eIntroduction to the Congruent Number Problem\u003c/a\u003e",
				"W. A. Stein, \u003ca href=\"http://wstein.org/simuw06\"\u003eThe Congruent Number Problem\u003c/a\u003e",
				"Ye Tian, \u003ca href=\"http://arxiv.org/abs/1210.8231\"\u003eCongruent Numbers and Heegner Points\u003c/a\u003e, arXiv:1210.8231 [math.NT], 2012.",
				"J. B. Tunnell, \u003ca href=\"http://dx.doi.org/10.1007/BF01389327\"\u003eA classical Diophantine problem and modular forms of weight 3/2\u003c/a\u003e, Invent. Math., 72 (1983), 323-334.",
				"D. J. Wright, \u003ca href=\"http://www.math.okstate.edu/~wrightd/4713/nt_essay/node7.html\"\u003eThe Congruent Number Problem\u003c/a\u003e"
			],
			"example": [
				"24 is congruent because 24 is the area of the right triangle with sides 6,8,10.",
				"5 is congruent because 5 is the area of the right triangle with sides 3/2, 20/3, 41/6 (although not of any right triangle with integer sides -- see A073120). - _Jonathan Sondow_, Oct 04 2013"
			],
			"mathematica": [
				"(* The following Mathematica code assumes the truth of the Birch and Swinnerton-Dyer conjecture and uses the list of primitive congruent numbers produced by the Mathematica code in A006991: *)",
				"For[cLst={}; i=1, i\u003c=Length[lst], i++, n=lst[[i]]; j=1; While[n j^2\u003c=maxN, cLst=Union[cLst, {n j^2}]; j++ ]]; cLst"
			],
			"xref": [
				"Cf. A006991, A072068, A072069, A072070, A072071, A073120, A165564, A182429, A256418, A259680-A259687."
			],
			"keyword": "nonn,nice",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Guy gives a table up to 1000.",
				"Edited by _T. D. Noe_, Jun 14 2002",
				"Comments revised by _Max Alekseyev_, Nov 15 2008",
				"Comment corrected by _Jonathan Sondow_, Oct 10 2013"
			],
			"references": 42,
			"revision": 129,
			"time": "2021-06-15T01:49:54-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}