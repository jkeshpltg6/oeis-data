{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A061397",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 61397,
			"data": "0,2,3,0,5,0,7,0,0,0,11,0,13,0,0,0,17,0,19,0,0,0,23,0,0,0,0,0,29,0,31,0,0,0,0,0,37,0,0,0,41,0,43,0,0,0,47,0,0,0,0,0,53,0,0,0,0,0,59,0,61,0,0,0,0,0,67,0,0,0,71,0,73,0,0,0,0,0,79,0,0,0,83,0,0,0,0,0,89,0,0,0,0,0",
			"name": "Characteristic function sequence of primes multiplied componentwise by N, the natural numbers.",
			"comment": [
				"Frequently, holes in a sequence are filled with zeros. This is a canonical way to do this and applied here to primes(A000040). A pre-scalar product when summation is omitted.",
				"Equals row sums of triangle A143536. - _Gary W. Adamson_, Aug 23 2008",
				"Mobius transform of sum of the distinct primes dividing n (A008472). - _Steven Foster Clark_, Jun 26 2020"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A061397/b061397.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PrimeZetaFunction.html\"\u003ePrime zeta function primezeta(s)\u003c/a\u003e."
			],
			"formula": [
				"a(n) = A010051(n)*A000027(n).",
				"Dirichlet generating function: primezeta(s-1). - _Franklin T. Adams-Watters_, Sep 11 2005",
				"a(1)=0; for n\u003e=1, a(n)=0, if either p_1|n or p_2|n or...or p_i|n, when n is in [p_i^2,p_(i+1)^2), i=1,2,..], where p_i is the i-th prime; otherwise a(n)=n. - _Vladimir Shevelev_, Apr 24 2010",
				"a(n) = n*floor(gcd(((n-1)! + 1)/n,2)). - _José de Jesús Camacho Medina_, Apr 30 2016",
				"a(n) = n*floor(1/A001065(n)); for n\u003e1. - _José de Jesús Camacho Medina_, Aug 07 2016",
				"G.f.: x*f'(x), where f(x) = Sum_{k\u003e=1} x^prime(k). - _Ilya Gutkovskiy_, Apr 10 2017",
				"a(n) = (2*n-1)! mod n^2, by Wilson's theorem. - _Thomas Ordowski_, Dec 27 2017"
			],
			"example": [
				"If 1\u003cn\u003c=8, a(n)=0 iff it is even on interval [4,9); if 9\u003c=n\u003c=25, then a(n)=0 iff n is either even or multiple of 3 on interval [9,25) etc. - _Vladimir Shevelev_, Apr 24 2010"
			],
			"maple": [
				"seq(`if`(isprime(n),n,0), n=1..100); # _Robert Israel_, May 02 2016"
			],
			"mathematica": [
				"If[PrimeQ@ #, #, 0] \u0026 /@ Range@ 94 (* or *)",
				"Replace[#, n_ /; ! PrimeQ@ n -\u003e 0] \u0026 /@ Range@ 94 (* _Michael De Vlieger_, May 02 2016 *)",
				"Table[n*Floor[GCD[((n-1)! + 1)/n, 2]], {n, 2, 100}] (* _José de Jesús Camacho Medina_, Apr 30 2016 *)"
			],
			"program": [
				"(PARI) a(n)=if(isprime(n),n) \\\\ _Charles R Greathouse IV_, Oct 29 2011",
				"(Haskell)",
				"a061397 n = (fromIntegral $ a010051 n) * n  -- _Reinhard Zumkeller_, Mar 21 2014"
			],
			"xref": [
				"Cf. A000040, A010051, A143536.",
				"Cf. A034387 (partial sums)."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Labos Elemer_, Jun 07 2001",
			"references": 31,
			"revision": 69,
			"time": "2020-07-30T02:43:47-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}