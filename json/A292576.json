{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A292576",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 292576,
			"data": "3,1,2,4,7,5,6,8,11,9,10,12,15,13,14,16,19,17,18,20,23,21,22,24,27,25,26,28,31,29,30,32,35,33,34,36,39,37,38,40,43,41,42,44,47,45,46,48,51,49,50,52,55,53,54,56,59,57,58,60,63,61,62",
			"name": "Permutation of the natural numbers partitioned into quadruples [4k-1, 4k-3, 4k-2, 4k], k \u003e 0.",
			"comment": [
				"Partition the natural number sequence into quadruples starting with (1,2,3,4); swap the second and third elements, then swap the first and the second element; repeat for all quadruples."
			],
			"link": [
				"Guenther Schrack, \u003ca href=\"/A292576/b292576.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,0,0,1,-1).",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(1)=3, a(2)=1, a(3)=2, a(4)=4, a(n) = a(n-4) + 4 for n \u003e 4.",
				"O.g.f.: (2*x^3 + x^2 - 2*x + 3)/(x^5 - x^4 - x + 1).",
				"a(n) = n + ((-1)^(n*(n-1)/2)*(2-(-1)^n) - (-1)^n)/2.",
				"a(n) = n + (cos(n*Pi/2) - cos(n*Pi) + 3*sin(n*Pi/2))/2.",
				"a(n) = n + n mod 2 + (ceiling(n/2)) mod 2 - 2*(floor(n/2) mod 2).",
				"Linear recurrence: a(n) = a(n-1) + a(n-4) - a(n-5) for n\u003e5.",
				"First Differences, periodic: (-2, 1, 2, 3), repeat; also (-1)^A130569(n)*A068073(n+2) for n \u003e 0."
			],
			"program": [
				"(MATLAB) a = [3 1 2 4]; % Generate b-file",
				"max = 10000;",
				"for n := 5:max",
				"   a(n) = a(n-4) + 4;",
				"end;",
				"(PARI) for(n=1, 10000, print1(n + ((-1)^(n*(n-1)/2)*(2 - (-1)^n) - (-1)^n)/2, \", \"))"
			],
			"xref": [
				"Inverse: A056699(n+1) - 1 for n \u003e 0.",
				"Sequence of fixed points: A008586(n) for n \u003e 0.",
				"Subsequences:",
				"   elements with odd index: A042964(A103889(n)) for n \u003e 0.",
				"   elements with even index: A042948(n) for n \u003e 0.",
				"   odd elements: A166519(n) for n\u003e0.",
				"   indices of odd elements: A042963(n) for n \u003e 0.",
				"   even elements: A005843(n) for n\u003e0.",
				"   indices of even elements: A014601(n) for n \u003e 0.",
				"Sum of pairs of elements:",
				"   a(n+2) + a(n) = A163980(n+1) = A168277(n+2) for n \u003e 0.",
				"Difference between pairs of elements:",
				"   a(n+2) - a(n) = (-1)^A011765(n+3)*A091084(n+1) for n \u003e 0.",
				"Compound relations:",
				"   a(n) = A284307(n+1) - 1 for n \u003e 0.",
				"   a(n+2) - 2*a(n+1) + a(n) = (-1)^A011765(n)*A132400(n+1) for n \u003e 0.",
				"Compositions:",
				"   a(n) = A116966(A080412(n)) for n \u003e 0.",
				"   a(A284307(n)) = A256008(n) for n \u003e 0.",
				"   a(A042963(n)) = A166519(n-1) for n \u003e 0.",
				"   A256008(a(n)) = A056699(n) for n \u003e 0."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Guenther Schrack_, Sep 19 2017",
			"references": 3,
			"revision": 29,
			"time": "2018-02-19T22:02:47-05:00",
			"created": "2017-10-01T00:44:13-04:00"
		}
	]
}