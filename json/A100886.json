{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A100886",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 100886,
			"data": "0,1,3,3,5,10,14,23,39,61,99,162,260,421,683,1103,1785,2890,4674,7563,12239,19801,32039,51842,83880,135721,219603,355323,574925,930250,1505174,2435423,3940599,6376021,10316619,16692642,27009260,43701901",
			"name": "Expansion of x*(1+3*x+2*x^2)/((1+x+x^2)*(1-x-x^2)).",
			"comment": [
				"This sequence was investigated in cooperation with _Paul Barry_.",
				"Generating floretion: - 0.5'i - 0.5'k - 0.5j' - 0.5'ii' + 0.5'jj' - 0.5'kk' + 0.5'ik' - 0.5'ki' (\"tes\")."
			],
			"link": [
				"Wojciech Florek, \u003ca href=\"/A100886/b100886.txt\"\u003eTable of n, a(n) for n = 0..1001\u003c/a\u003e",
				"W. Florek, \u003ca href=\"http://doi.org/10.1016/j.amc.2018.06.014\"\u003eA class of generalized Tribonacci sequences applied to counting problems\u003c/a\u003e, Appl. Math. Comput., 338 (2018), 809-821.",
				"W. O. J. Moser, \u003ca href=\"http://www.fq.math.ca/Scanned/31-1/moser.pdf\"\u003eCyclic binary strings without long runs of like (alternating) bits\u003c/a\u003e, Fibonacci Quart. 31 (1993), no. 1, 2-6.",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,1,2,1)."
			],
			"formula": [
				"(1/2)*(a(n) + A100887(n) - A100888(n)) = A061347(n+3).",
				"a(n) = (L(n+1)-A061347(n+1))/2, L=A000032; [corrected by _Wojciech Florek_, Feb 26 2018]",
				"a(n) = a(n-2)+2a(n-3)+a(n-4), a(0) = 0, a(1) = 1, a(2) = 3, a(3) = 3.",
				"a(n) = n*sum(j=1,floor(n/2), binomial(2*j,n-2*j)/(2*j) ). - _Vladimir Kruchinin_, Apr 09 2011 (with offset 1, cf. Pari code)",
				"a(n) = floor(phi^(n+1)/2), n mod 3 = 0,1; a(n) = floor((phi^(n+1)+3)/2), n mod 3 = 2, phi = (1 + sqrt(5))/2; from Binet's formula or the relation to the Lucas numbers A000032. - _Wojciech Florek_, Mar 03 2018"
			],
			"mathematica": [
				"a[0] = 0; a[1] = 1; a[2] = 3; a[3] = 3; a[n_] := a[n] = a[n - 2] + 2a[n - 3] + a[n - 4]; Table[ a[n], {n, 0, 36}]",
				"(* Or *) CoefficientList[ Series[x(1 + 3x + 2x^2)/((1 + x + x^2)(1 - x - x^2)), {x, 0, 36}], x] (* _Robert G. Wilson v_, Nov 26 2004 *)",
				"LinearRecurrence[{0,1,2,1},{0,1,3,3},40] (* _Harvey P. Dale_, Apr 04 2016 *)"
			],
			"program": [
				"(Maxima) a(n):=n*sum(binomial(k,n-k)*(if oddp(k) then 0 else 1/k),k,1,n) /* _Vladimir Kruchinin_, Apr 09 2011 */",
				"(PARI)",
				"a(n)=n*sum(j=1,n\\2,k=2*j;binomial(k,n-k)/k);",
				"vector(66,n,a(n)) /* _Joerg Arndt_, Apr 09 2011 */",
				"(PARI)",
				"concat([0],Vec(x*(1+3*x+2*x^2)/((1+x+x^2)*(1-x-x^2))+O(x^66))) /* _Joerg Arndt_, Apr 09 2011 */",
				"(MAGMA) I:=[0,1,3,3]; [n le 4 select I[n] else Self(n-2)+2*Self(n-3)+Self(n-4): n in [1..40]]; // _Vincenzo Librandi_, Jul 30 2015"
			],
			"xref": [
				"Cf. A000032, A007040, A087204, A100887, A100888, A100889, A100890."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Creighton Dement_, Nov 21 2004",
			"ext": [
				"More terms from _Robert G. Wilson v_, Nov 26 2004"
			],
			"references": 6,
			"revision": 59,
			"time": "2018-07-31T22:43:52-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}