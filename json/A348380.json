{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A348380",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 348380,
			"data": "0,0,0,1,0,0,0,1,1,0,0,0,0,0,0,2,0,0,0,0,0,0,0,1,1,0,1,0,0,0,0,2,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,2,1,0,0,0,0,1,0,1,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,2,2,0,0,0,0,0,0",
			"name": "Number of factorizations of n without an alternating permutation. Includes all twins (x*x).",
			"comment": [
				"First differs from A333487 at a(216) = 4, A333487(216) = 3.",
				"A factorization of n is a weakly increasing sequence of positive integers \u003e 1 with product n.",
				"A sequence is alternating if it is alternately strictly increasing and strictly decreasing, starting with either. For example, the partition (3,2,2,2,1) has no alternating permutations, even though it does have the anti-run permutations (2,3,2,1,2) and (2,1,2,3,2). Alternating permutations of multisets are a generalization of alternating or up-down permutations of {1..n}."
			],
			"link": [
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Alternating_permutation\"\u003eAlternating permutation\u003c/a\u003e"
			],
			"formula": [
				"a(2^n) = A345165(n)."
			],
			"example": [
				"The a(n) factorizations for n = 96, 144, 192, 384:",
				"  (2*2*2*12)     (12*12)        (3*4*4*4)        (4*4*4*6)",
				"  (2*2*2*2*6)    (2*2*2*18)     (2*2*2*24)       (2*2*2*48)",
				"  (2*2*2*2*2*3)  (2*2*2*2*9)    (2*2*2*2*12)     (2*2*2*2*24)",
				"                 (2*2*2*2*3*3)  (2*2*2*2*2*6)    (2*2*2*2*3*8)",
				"                                (2*2*2*2*3*4)    (2*2*2*2*4*6)",
				"                                (2*2*2*2*2*2*3)  (2*2*2*2*2*12)",
				"                                                 (2*2*2*2*2*2*6)",
				"                                                 (2*2*2*2*2*3*4)",
				"                                                 (2*2*2*2*2*2*2*3)"
			],
			"mathematica": [
				"facs[n_]:=If[n\u003c=1,{{}},Join@@Table[Map[Prepend[#,d]\u0026,Select[facs[n/d],Min@@#\u003e=d\u0026]],{d,Rest[Divisors[n]]}]];",
				"wigQ[y_]:=Or[Length[y]==0,Length[Split[y]]==Length[y]\u0026\u0026Length[Split[Sign[Differences[y]]]]==Length[y]-1];",
				"Table[Length[Select[facs[n],Select[Permutations[#],wigQ]=={}\u0026]],{n,100}]"
			],
			"xref": [
				"The inseparable case is A333487, complement A335434, without twins A348381.",
				"Non-twin partitions of this type are counted by A344654, ranked by A344653.",
				"Twins and partitions not of this type are counted by A344740, ranked by A344742.",
				"Partitions of this type are counted by A345165, ranked by A345171.",
				"Partitions not of this type are counted by A345170, ranked by A345172.",
				"The case without twins is A347706.",
				"The complement is counted by A348379, with twins A347050.",
				"Numbers with a factorization of this type are A348609.",
				"An ordered version is A348613, complement A348610.",
				"A001055 counts factorizations, strict A045778, ordered A074206.",
				"A001250 counts alternating permutations.",
				"A025047 counts alternating or wiggly compositions, ranked by A345167.",
				"A325535 counts inseparable partitions, ranked by A335448.",
				"A339846 counts even-length factorizations.",
				"A339890 counts odd-length factorizations.",
				"Cf. A038548, A049774, A119620, A289553, A325534, A336107, A344614, A345192, A347437, A347438, A347439, A347442, A347458, A348383, A348611."
			],
			"keyword": "nonn",
			"offset": "1,16",
			"author": "_Gus Wiseman_, Oct 28 2021",
			"references": 21,
			"revision": 8,
			"time": "2021-11-03T10:48:57-04:00",
			"created": "2021-11-03T10:48:57-04:00"
		}
	]
}