{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A016655",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 16655,
			"data": "3,4,6,5,7,3,5,9,0,2,7,9,9,7,2,6,5,4,7,0,8,6,1,6,0,6,0,7,2,9,0,8,8,2,8,4,0,3,7,7,5,0,0,6,7,1,8,0,1,2,7,6,2,7,0,6,0,3,4,0,0,0,4,7,4,6,6,9,6,8,1,0,9,8,4,8,4,7,3,5,7,8,0,2,9,3,1,6,6,3,4,9,8,2,0,9,3,4,3",
			"name": "Decimal expansion of log(32) = 5*log(2).",
			"comment": [
				"Log(32) = 10*log(2)/2 = 5*log(2) = 5*A002162, so 10*(1/2 - 1/4 + 1/6 - 1/8 + 1/10 - 1/2n ...) = log(32). - _Eric Desbiaux_, Nov 26 2008",
				"-log(2)/2 is the limit for n -\u003e infinity of ((Sum_{k=2..n} arctanh(1/k)) - log(n)). - _Jean-François Alcover_, Aug 07 2014, after Steven Finch",
				"Equals log(sqrt(2)) with offset 0. - _Michel Marcus_, Feb 19 2017"
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), p. 2."
			],
			"link": [
				"Harry J. Smith, \u003ca href=\"/A016655/b016655.txt\"\u003eTable of n, a(n) for n = 1..20000\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"R. S. Melham and A. G. Shannon, \u003ca href=\"https://www.fq.math.ca/Scanned/33-1/melham2.pdf\"\u003eInverse Trigonometric Hyperbolic Summation Formulas Involving Generalized Fibonacci Numbers\u003c/a\u003e, The Fibonacci Quarterly, Vol. 33, No. 1 (1995), pp. 32-40.",
				"Steven R. Finch, \u003ca href=\"http://arxiv.org/abs/2001.00578\"\u003eErrata and Addenda to Mathematical Constants\u003c/a\u003e, arXiv:2001.00578 [math.HO], 2020-2021.",
				"\u003ca href=\"/index/Tra#transcendental\"\u003eIndex entries for transcendental numbers\u003c/a\u003e."
			],
			"formula": [
				"log(2)/2 = (1 - 1/2 - 1/4) + (1/3 - 1/6 - 1/8) + (1/5 - 1/10 - 1/12) + ... [Jolley, Summation of Series, Dover (1961) eq (73)]",
				"Equals (5/4) Sum_{k=1..4} (-1)^(k+1) gamma(0, k/4) where gamma(n,x) denotes the generalized Stieltjes constants. - _Peter Luschny_, May 16 2018",
				"From _Amiram Eldar_, Jun 29 2020: (Start)",
				"log(2)/2 = arctanh(1/3) = arcsinh(1/sqrt(8)).",
				"log(2)/2 = Integral_{x=0..Pi/4} tan(x) dx.",
				"log(2)/2 = Sum_{k\u003e=0} (-1)^k/(2*k+2).",
				"log(2)/2 = Sum_{k\u003e=1} 1/A060851(k). (End)",
				"log(2)/2 = Sum_{k\u003e=1} (-1)^(k+1) * arctanh(Lucas(2*k+3)/Fibonacci(2*k+3)^2) (Melham and Shannon, 1995). - _Amiram Eldar_, Jan 15 2022"
			],
			"example": [
				"3.465735902799726547086160607290882840377500671801276270603400047466968..."
			],
			"mathematica": [
				"RealDigits[5 N [Log[2], 100]] [[1]] (* _Vincenzo Librandi_, Jan 02 2016 *)"
			],
			"program": [
				"(PARI) log(32) \\\\ _Charles R Greathouse IV_, Jan 24 2012",
				"(MAGMA) [5*Log(2)]; // _Vincenzo Librandi_, Jan 02 2016"
			],
			"xref": [
				"Cf. A000045, A000032, A060851, A195909, A195913, A195697, A016460 (continued fraction)."
			],
			"keyword": "nonn,cons,changed",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_",
			"references": 14,
			"revision": 66,
			"time": "2022-01-15T09:58:23-05:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}