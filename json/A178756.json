{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A178756",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 178756,
			"data": "1,4,3,12,18,6,32,81,48,10,80,324,288,100,15,192,1215,1536,750,180,21,448,4374,7680,5000,1620,294,28,1024,15309,36864,31250,12960,3087,448,36,2304,52488,172032,187500,97200,28812,5376,648,45",
			"name": "Rectangular array T(n,k) = binomial(n,2)*k*n^(k-1) read by antidiagonals.",
			"comment": [
				"T(n,k) is the sum of the digits in all n-ary words of length k. That is, sequences of k digits taken on an alphabet of {0,1,2,...,n-1}.",
				"Note the rectangle is indexed begining from n = 2 (binary sequences) which is A001787."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A178756/b178756.txt\"\u003eAntidiagonals n=2..101, flattened\u003c/a\u003e"
			],
			"formula": [
				"E.g.f. for row n: binomial(n,2)*x*exp(n*x)."
			],
			"example": [
				"1,4,12,32,80,192,448,1024",
				"3,18,81,324,1215,4374,15309,52488",
				"6,48,288,1536,7680,36864,172032,786432",
				"10,100,750,5000,31250,187500,1093750,6250000",
				"15,180,1620,12960,97200,699840,4898880,33592320"
			],
			"maple": [
				"T:= (n, k)-\u003e binomial(n, 2)*k*n^(k-1):",
				"seq(seq(T(n, 1+d-n), n=2..d), d=2..14); # _Alois P. Heinz_, Jan 17 2013"
			],
			"mathematica": [
				"Table[Range[8]! Rest[CoefficientList[Series[Binomial[n,2]x Exp[n x],{x,0,8}],x]],{n,2,10}]//Grid",
				"T[n_, k_]:= Binomial[n, 2]*k*n^(k-1); Table[T[k,n-k], {n,2,10}, {k, 2, n-1}]//Flatten (* _G. C. Greubel_, Jan 24 2019 *)"
			],
			"program": [
				"(PARI) {T(n,k) = binomial(n,2)*k*n^(k-1)};",
				"for(n=2,10, for(k=2,n-1, print1(T(k,n-k), \", \"))) \\\\ _G. C. Greubel_, Jan 24 2019",
				"(MAGMA) [[Binomial(k,2)*(n-k)*k^(n-k-1): k in [2..n-1]]: n in [3..10]]; // _G. C. Greubel_, Jan 24 2019",
				"(Sage) [[binomial(k,2)*(n-k)*k^(n-k-1) for k in (2..n-1)] for n in (3..10)] # _G. C. Greubel_, Jan 24 2019",
				"(GAP) T:=Flat(List([3..10], n-\u003e List([2..n-1], k-\u003e Binomial(k,2)*(n-k)* k^(n-k-1) ))); # _G. C. Greubel_, Jan 24 2019"
			],
			"xref": [
				"Cf. A036290 (ternary sequences), A034967 (decimal digits)."
			],
			"keyword": "nonn,tabl",
			"offset": "2,2",
			"author": "_Geoffrey Critzer_, Dec 26 2010",
			"references": 2,
			"revision": 26,
			"time": "2020-04-13T05:56:51-04:00",
			"created": "2010-11-12T14:27:39-05:00"
		}
	]
}