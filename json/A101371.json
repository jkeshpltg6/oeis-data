{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A101371",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 101371,
			"data": "1,0,1,2,0,1,7,4,0,1,34,14,6,0,1,171,72,21,8,0,1,905,370,114,28,10,0,1,4952,1995,597,160,35,12,0,1,27802,11064,3278,852,210,42,14,0,1,159254,62774,18420,4762,1135,264,49,16,0,1,927081,362614,105618,27104,6455,1446,322,56,18,0,1",
			"name": "Triangle read by rows: T(n,k) is the number of noncrossing trees with n edges and k leaves at level 1.",
			"comment": [
				"Row n has n+1 terms. Row sums give A001764. Column k=0 gives A023053."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A101371/b101371.txt\"\u003eTable of n, a(n) for n = 0..1274\u003c/a\u003e",
				"Naiomi Cameron, J. E. McLeod, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL19/McLeod/mcleod3.html\"\u003eReturns and Hills on Generalized Dyck Paths\u003c/a\u003e, Journal of Integer Sequences, Vol. 19, 2016, #16.6.1.",
				"P. Flajolet and M. Noy, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(98)00372-0\"\u003eAnalytic combinatorics of non-crossing configurations\u003c/a\u003e, Discrete Math., 204, 203-229, 1999.",
				"M. Noy, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(97)00121-0\"\u003eEnumeration of noncrossing trees on a circle\u003c/a\u003e, Discrete Math., 180, 301-313, 1998."
			],
			"formula": [
				"T(n, k) = Sum_{i=0..n-k} (-1)^i*((k+i+1)/(2n-k-i+1)) binomial(k+i, i) binomial(3n-2k-2i, n-k-i) for 0 \u003c= k \u003c= n.",
				"G.f.: g/(1+z*g-t*z*g), where g = 1+z*g^3."
			],
			"example": [
				"Triangle begins:",
				"    1;",
				"    0,  1;",
				"    2,  0,  1;",
				"    7,  4,  0, 1;",
				"   34, 14,  6, 0, 1;",
				"  171, 72, 21, 8, 0, 1;",
				"  ..."
			],
			"maple": [
				"T:=proc(n,k) if k\u003c=n then sum((-1)^i*(k+i+1)*binomial(k+i,i)*binomial(3*n-2*k-2*i,n-k-i)/(2*n-k-i+1),i=0..n-k) else 0 fi end: for n from 0 to 10 do seq(T(n,k),k=0..n) od; # yields sequence in triangular form"
			],
			"mathematica": [
				"t[n_, k_] := Sum[(-1)^i*(k + i + 1)/(2n - k - i + 1)*Binomial[k + i, i]* Binomial[3n - 2k - 2i, n - k - i], {i, 0, n - k}]; Table[t[n, k], {n, 0, 10}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Jan 21 2013, after Maple *)"
			],
			"program": [
				"(PARI) T(n, k) = sum(i=0, n-k, (-1)^i*(k+i+1)*binomial(k+i, i)*binomial(3*n-2*k-2*i, n-k-i)/(2*n-k-i+1)); \\\\ _Andrew Howroyd_, Nov 06 2017"
			],
			"xref": [
				"Cf. A001764, A023053."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Emeric Deutsch_, Jan 14 2005",
			"references": 2,
			"revision": 20,
			"time": "2017-11-07T18:38:27-05:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}