{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A173778",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 173778,
			"data": "1,1,1,1,1,1,1,16,16,1,1,16,256,16,1,1,136,2176,2176,136,1,1,256,34816,34816,34816,256,1,1,1216,311296,2646016,2646016,311296,1216,1,1,3136,3813376,61014016,518619136,61014016,3813376,3136,1,1,11776,36929536,2806644736,44906315776,44906315776,2806644736,36929536,11776,1",
			"name": "Triangle T(n, k, q) = round(c(n,q)/(c(k,q)*c(n-k,q))), where c(n,q) = Product_{j=0..n} v(j, q)((1)), v(n, q) = M*v(n-1, q), v(0, q) = {1, 1, 1}, M = {{0, 1, 0}, {0, 0, 1}, {q^3, q^3, 0}}, and q = 2, read by rows.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A173778/b173778.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k, q) = round( c(n,q)/(c(k,q)*c(n-k,q)) ), where c(n,q) = Product_{j=0..n} v(j, q)((1)), v(n, q) = M*v(n-1, q), v(0, q) = {1, 1, 1}, M = {{0, 1, 0}, {0, 0, 1}, {q^3, q^3, 0}}, and q = 2.",
				"T(n, k, q) = round( (1/f(k,q))*Product_{j=0..n-k} f(j+k,q)/f(j,q) ), where f(n, q) = 6*q*f(n-2, q) + 8*q^3*f(n-3, q), and f(0,q) = f(1,q) = f(2,q) = 1, and q = 2. - _G. C. Greubel_, Jul 06 2021"
			],
			"example": [
				"Triangle begins as:",
				"  1;",
				"  1,    1;",
				"  1,    1,       1;",
				"  1,   16,      16,        1;",
				"  1,   16,     256,       16,         1;",
				"  1,  136,    2176,     2176,       136,        1;",
				"  1,  256,   34816,    34816,     34816,      256,       1;",
				"  1, 1216,  311296,  2646016,   2646016,   311296,    1216,    1;",
				"  1, 3136, 3813376, 61014016, 518619136, 61014016, 3813376, 3136, 1;"
			],
			"mathematica": [
				"f[n_, q_]:= f[n, q] = If[n\u003c3, 1, q^3*f[n-2, q] + q^3*f[n-3, q]];",
				"c[n_, q_]:= Product[f[j, q], {j, 0, n}];",
				"T[n_, k_, q_]:= Round[c[n, q]/(c[k, q]*c[n-k, q])];",
				"Table[T[n, k, 2], {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, Jul 06 2021 *)"
			],
			"program": [
				"(MAGMA)",
				"function f(n,k)",
				"  if n lt 3 then return 1;",
				"  else return k^3*f(n-2,k) + k^3*f(n-3,k);",
				"  end if; return f;",
				"end function;",
				"T:= func\u003c n,k,q | Round( (\u0026*[f(j+k, q)/f(j,q): j in [0..n-k]])/f(k,q) ) \u003e;",
				"[T(n,k,2): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Jul 06 2021",
				"(Sage)",
				"@CachedFunction",
				"def f(n,q): return 1 if (n\u003c3) else q^3*f(n-2, q) + q^3*f(n-3, q)",
				"def T(n,k,q): return round( product( f(j+k,q)/f(j,q) for j in (0..n-k))/f(k,q) )",
				"flatten([[T(n, k, 2) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Jul 06 2021"
			],
			"xref": [
				"Cf. this sequence (q=2), A173779 (q=4).",
				"Cf. A173747, A173749, A173779."
			],
			"keyword": "nonn,tabl,less",
			"offset": "0,8",
			"author": "_Roger L. Bagula_, Feb 24 2010",
			"ext": [
				"Definition corrected and edited by _G. C. Greubel_, Jul 06 2021"
			],
			"references": 4,
			"revision": 14,
			"time": "2021-07-14T20:31:31-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}