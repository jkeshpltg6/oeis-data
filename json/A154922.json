{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A154922",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 154922,
			"data": "4,7,7,29,40,29,133,280,280,133,641,2030,2800,2030,641,3157,14630,28000,28000,14630,3157,15689,102560,278400,360000,278400,102560,15689,78253,694540,2699900,4557000,4557000,2699900,694540,78253,390881,4549810,25191300,58464000,68040000,58464000,25191300,4549810,390881",
			"name": "Triangle T(n,m,p,q) = (p^(n-k)*q^k + p^k*q^(n-k))*(StirlingS2(n, k) + StirlingS2(n, n-k)) with p=2 and q=5, read by rows.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A154922/b154922.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/BellPolynomial.html\"\u003eBell Polynomial\u003c/a\u003e"
			],
			"formula": [
				"T(n,m,p,q) = (p^(n-k)*q^k + p^k*q^(n-k))*(StirlingS2(n, k) + StirlingS2(n, n-k)) with p=2 and q=5.",
				"Sum_{k=0..n} T(n,k,p,q) = 2*p^n*( T_{n}(q/p) + (q/p)^n*T_{n}(p/q) ), with p=2 and q=5, where T_{n}(x) are the Touchard polynomials (sometimes named Bell polynomials). - _G. C. Greubel_, Mar 02 2021"
			],
			"example": [
				"Triangle begins as:",
				"      4;",
				"      7,      7;",
				"     29,     40,      29;",
				"    133,    280,     280,     133;",
				"    641,   2030,    2800,    2030,     641;",
				"   3157,  14630,   28000,   28000,   14630,    3157;",
				"  15689, 102560,  278400,  360000,  278400,  102560,  15689;",
				"  78253, 694540, 2699900, 4557000, 4557000, 2699900, 694540, 78253;"
			],
			"maple": [
				"A154922:= (n,k,p,q) -\u003e (p^(n-k)*q^k + p^k*q^(n-k))*(combinat[stirling2](n, k) + combinat[stirling2](n, n-k));",
				"seq(seq(A154922(n,k,2,5), k=0..n), n=0..12); # _G. C. Greubel_, Mar 02 2021"
			],
			"mathematica": [
				"T[n_, k_, p_, q_]:= (p^(n-k)*q^k + p^k*q^(n-k))*(StirlingS2[n, k] + StirlingS2[n, n-k]);",
				"Table[T[n, k, 2, 5], {n,0,12}, {k,0,n}]//Flatten (* modified by _G. C. Greubel_, Mar 02 2021 *)"
			],
			"program": [
				"(Sage)",
				"def A154922(n,k,p,q): return (p^(n-k)*q^k + p^k*q^(n-k))*(stirling_number2(n, k) + stirling_number2(n, n-k))",
				"flatten([[A154922(n,k,2,5) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Mar 02 2021",
				"(Magma)",
				"A154922:= func\u003c n,k,p,q | (p^(n-k)*q^k + p^k*q^(n-k))*(StirlingSecond(n, k) + StirlingSecond(n, n-k)) \u003e;",
				"[A154922(n,k,2,5): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Mar 02 2021"
			],
			"xref": [
				"Cf. A154915 (q=1), A154916 (q=3), this sequence (q=5).",
				"Cf. A008277, A048993, A154913, A154914."
			],
			"keyword": "nonn,tabl,easy,less",
			"offset": "0,1",
			"author": "_Roger L. Bagula_, Jan 17 2009",
			"ext": [
				"Edited by _G. C. Greubel_, Mar 02 2021"
			],
			"references": 5,
			"revision": 9,
			"time": "2021-03-02T09:23:10-05:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}