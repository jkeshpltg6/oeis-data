{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A161492",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 161492,
			"data": "1,1,1,1,2,1,1,4,3,1,1,6,8,4,1,1,9,17,13,5,1,1,12,32,34,19,6,1,1,16,55,78,58,26,7,1,1,20,89,160,154,90,34,8,1,1,25,136,305,365,269,131,43,9,1,1,30,200,544,794,716,433,182,53,10,1,1,36,284,923,1609,1741,1271,657,244,64,11,1",
			"name": "Triangle T(n,m) read by rows: the number of skew Ferrers diagrams with area n and m columns.",
			"comment": [
				"Row sums give A006958, sums along falling diagonals give A227309. [_Joerg Arndt_, Mar 23 2014]",
				"A coin fountain is an arrangement of coins in numbered rows such that the bottom row (row 0) contains contiguous coins and such that each coin in a higher row touches exactly two coins in the next lower row. See A005169. T(n,m) equals the number of coin fountains with exactly n coins in the even-numbered rows and n - m coins in the odd-numbered rows of the fountain. See the illustration in the Links section. - _Peter Bala_, Jul 21 2019"
			],
			"link": [
				"Peter Bala, \u003ca href=\"/A161492/a161492.pdf\"\u003eIllustration for the terms of row 4\u003c/a\u003e",
				"Peter Bala, \u003ca href=\"/A161492/a161492_1.pdf\"\u003eFountains of coins and skew Ferrers diagrams\u003c/a\u003e",
				"M. P. Delest, J. M. Fedou, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(93)90224-H\"\u003eEnumeration of skew Ferrers diagrams\u003c/a\u003e, Discrete Mathematics. vol.112, no.1-3, pp.65-79, (1993)"
			],
			"formula": [
				"From _Peter Bala_, Jul 21 2019: (Start)",
				"The following formulas all include an initial term T(0,0) = 1.",
				"O.g.f. as a ratio of q-series: A(q,t) = N(q,t)/D(q,t) = 1 + q*t + q^2*(t + t^2) + q^3*(t + 2*t^2 + t^3) + ..., where N(q,t) = Sum_{n \u003e= 0} (-1)^n*q^((n^2 + 3*n)/2)*t^n/Product_{k = 1..n} (1 - q^k)^2 and D(q,t) = Sum_{n \u003e= 0} (-1)^n*q^((n^2 + n)/2)*t^n/Product_{k = 1..n} (1 - q^k)^2.",
				"Continued fraction representations:",
				"A(q,t) = 1/(1 - q*t/(1 - q/(1 - q^2*t/(1 - q^2/(1 - q^3*t/(1 - q^3/(1 - (...) ))))))).",
				"A(q,t) = 1/(1 - q*t/(1 + q*(t - 1) - q*t/(1 + q*(t - q) - q*t/( 1 + q*(t - q^2) - q*t/( (...) ))))).",
				"A(q,t) = 1/(1 - q*t - q^2*t/(1 - q*(1 + q*t) - q^4*t/(1 - q^2*(1 + q*t) - q^6*t/(1 - q^3*(1 + q*t) - q^8*t/( (...) ))))).",
				"A(q,t) =  1/(1 - q*t - q^2*t/(1 - q^2*t - q/(1 - q^3*t - q^5*t/(1 - q^4*t - q^2/(1 - q^5*t - q^8*t/ (1 - q^6*t - q^3/(1 - q^7*t - q^11*t/(1 - q^8*t - (...) )))))))). (End)"
			],
			"example": [
				"T(4,2)=4 counts the following 4 diagrams with area equal to 4 and 2 columns:",
				"   .X..XX...X..XX",
				"   XX..XX...X..X.",
				"   X.......XX..X.",
				"From _Joerg Arndt_, Mar 23 2014: (Start)",
				"Triangle begins:",
				"01:  1",
				"02:  1   1",
				"03:  1   2    1",
				"04:  1   4    3    1",
				"05:  1   6    8    4     1",
				"06:  1   9   17   13     5     1",
				"07:  1  12   32   34    19     6     1",
				"08:  1  16   55   78    58    26     7    1",
				"09:  1  20   89  160   154    90    34    8   1",
				"10:  1  25  136  305   365   269   131   43   9   1",
				"11:  1  30  200  544   794   716   433  182  53  10  1",
				"12:  1  36  284  923  1609  1741  1271  657 ...",
				"(End)"
			],
			"maple": [
				"qpoch := proc(a,q,n)",
				"    mul( 1-a*q^k,k=0..n-1) ;",
				"end proc:",
				"A161492 := proc(n,m)",
				"    local N,N2,ns ;",
				"    N := 0 ;",
				"    for ns from 0 to n+2 do",
				"        N := N+ (-1)^ns *q^binomial(ns+1,2) / qpoch(q,q,ns) / qpoch(q,q,ns+1) *q^(ns+1) *t^(ns+1) ;",
				"        N := taylor(N,q=0,n+1) ;",
				"    end do:",
				"    N2 := 0 ;",
				"    for ns from 0 to n+2 do",
				"        N2 := N2+ (-1)^ns*q^binomial(ns,2)/(qpoch(q,q,ns))^2*q^ns*t^ns ;",
				"        N2 := taylor(N2,q=0,n+1) ;",
				"    end do:",
				"    coeftayl(N/N2,q=0,n) ;",
				"    coeftayl(%,t=0,m) ;",
				"end proc:",
				"for a from 1 to 20 do",
				"    for c from 1 to a do",
				"        printf(\"%d \", A161492(a,c)) ;",
				"    od:",
				"od:"
			],
			"mathematica": [
				"nmax = 13;",
				"qn[n_] := Product[1 - q^k, {k, 1, n}];",
				"nm = Sum[(-1)^n q^(n(n+1)/2)/(qn[n] qn[n+1])(t q)^(n+1) + O[q]^nmax, {n, 0, nmax}];",
				"dn = Sum[(-1)^n q^(n(n-1)/2)/(qn[n]^2)(t q)^n + O[q]^nmax, {n, 0, nmax}];",
				"Rest[CoefficientList[#, t]]\u0026 /@ Rest[CoefficientList[nm/dn, q]] // Flatten (* _Jean-François Alcover_, Dec 19 2019, after _Joerg Arndt_ *)"
			],
			"program": [
				"(PARI) /* formula from the Delest/Fedou reference: */",
				"N=20;  q='q+O('q^N);  t='t;",
				"qn(n) = prod(k=1, n, 1-q^k );",
				"nm = sum(n=0, N, (-1)^n* q^(n*(n+1)/2) / ( qn(n) * qn(n+1) ) * (t*q)^(n+1) );",
				"dn = sum(n=0, N, (-1)^n* q^(n*(n-1)/2) / ( qn(n)^2 ) * (t*q)^n );",
				"v=Vec(nm/dn);",
				"for(n=1,N-1,print(Vec(polrecip(Pol(v[n])))));  \\\\ print triangle",
				"\\\\ _Joerg Arndt_, Mar 23 2014"
			],
			"xref": [
				"Row sums A006958. Cf. A005169, A227309."
			],
			"keyword": "nonn,tabl",
			"offset": "1,5",
			"author": "_R. J. Mathar_, Jun 11 2009",
			"references": 8,
			"revision": 27,
			"time": "2019-12-19T07:50:24-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}