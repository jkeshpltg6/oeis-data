{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A067180",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 67180,
			"data": "0,2,3,13,5,0,7,17,0,19,29,0,67,59,0,79,89,0,199,389,0,499,599,0,997,1889,0,1999,2999,0,4999,6899,0,17989,8999,0,29989,39989,0,49999,59999,0,79999,98999,0,199999,389999,0,598999,599999,0,799999,989999,0,2998999,2999999,0,4999999",
			"name": "Smallest prime with digit sum n, or 0 if no such prime exists.",
			"link": [
				"Robert Israel, \u003ca href=\"/A067180/b067180.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e (first 175 terms from Robert G. Wilson v)"
			],
			"formula": [
				"a(3k) = 0 for k \u003e 1.",
				"a(3k-2) =  A067523(2k-1), a(3k-1) = A067523(2k), for all k \u003e 1. - _M. F. Hasler_, Nov 04 2018"
			],
			"example": [
				"a(68) = 59999999 because 59999999 is the smallest prime with digit sum = 68;",
				"a(100) = 298999999999 because 298999999999 is the smallest prime with digit sum = 100."
			],
			"maple": [
				"g:= proc(s,d) # integers of \u003c=d digits with sum s",
				"  if s \u003e 9*d then return [] fi;",
				"  if d = 1 then return [s] fi;",
				"  [seq(op(map(t -\u003e j*10^(d-1)+ t, g(s-j,d-1))),j=0..9)];",
				"end proc:",
				"f:= proc(n) local d, j,x,y;",
				"  if n mod 3 = 0 then return 0 fi;",
				"  for d from ceil(n/9) do",
				"    if d = 1 then",
				"      if isprime(n) and n \u003c 10 then return n",
				"      else next",
				"      fi",
				"    fi;",
				"    for j from 1 to 9 do",
				"      for y in g(n-j,d-1) do",
				"        x:= 10^(d-1)*j + y;",
				"        if isprime(x) then return x fi;",
				"  od od od;",
				"end proc:",
				"f(1):= 0: f(3):= 3:",
				"map(f, [$1..100]); # _Robert Israel_, Dec 13 2020"
			],
			"mathematica": [
				"a = Table[0, {100}]; Do[b = Apply[ Plus, IntegerDigits[ Prime[n]]]; If[b \u003c 101 \u0026\u0026 a[[b]] == 0, a[[b]] = Prime[n]], {n, 1, 10^7} ]; a",
				"f[n_] :=  If[n \u003e 5 \u0026\u0026 Mod[n, 3] == 0, 0, Block[{k = 1, lmt, lst = {}, ip = IntegerPartitions[n, Round[1 + n/9], {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}]}, lmt = 1 + Length@ ip; While[k \u003c lmt, AppendTo[lst, Select[ FromDigits@# \u0026 /@ Permutations@ ip[[k]], PrimeQ[#] \u0026]]; k++]; Min@ Flatten@ lst]]; f[1] = 0; f[4] = 13; Array[f, 70] (* _Robert G. Wilson v_, Sep 28 2014 *)"
			],
			"program": [
				"(PARI) A067180(n)={if(n\u003c2, 0, n\u003c4, n, n%3, my(d=divrem(n,9)); forprime(p=d[2]*10^d[1]-1,,sumdigits(p)==n\u0026\u0026return(p)))} \\\\ _M. F. Hasler_, Nov 04 2018"
			],
			"xref": [
				"Cf. A054750.",
				"Removal of the 0 terms from this sequence leaves A067523."
			],
			"keyword": "easy,nonn,base",
			"offset": "1,2",
			"author": "_Amarnath Murthy_, Jan 09 2002",
			"ext": [
				"Edited and extended by _Robert G. Wilson v_, Mar 01 2002",
				"Edited by _Ray Chandler_, Apr 24 2007"
			],
			"references": 13,
			"revision": 25,
			"time": "2020-12-14T01:36:37-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}