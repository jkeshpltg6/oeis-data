{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A300401",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 300401,
			"data": "0,1,1,2,2,2,3,4,4,3,4,7,8,7,4,5,11,14,14,11,5,6,16,22,24,22,16,6,7,22,32,37,37,32,22,7,8,29,44,53,56,53,44,29,8,9,37,58,72,79,79,72,58,37,9,10,46,74,94,106,110,106,94,74,46,10,11,56,92,119",
			"name": "Array T(n,k) = n*(binomial(k, 2) + 1) + k*(binomial(n, 2) + 1) read by antidiagonals.",
			"comment": [
				"Antidiagonal sums are given by 2*A055795.",
				"Rows/columns n are binomial transform of {n, A152947(n+1), n, 0, 0, 0, ...}.",
				"Some primes in the array are",
				"n = 1: {2, 7, 11, 29, 37, 67, 79, 137, 191, 211, 277, 379, ...} = A055469, primes of the form k*(k + 1)/2 + 1;",
				"n = 3: {3, 7, 37, 53, 479, 653, 1249, 1619, 2503, 3727, 4349, 5737, 7109, 8179, 9803, 11839, 12107, ...};",
				"n = 4: {11, 37, 79, 137, 211, 821, 991, 1597, 1831, 2081, 2347, ...} = A188382, primes of the form 8*(2*k - 1)^2 + 2*(2*k - 1) + 1."
			],
			"reference": [
				"Miklós Bóna, Introduction to Enumerative Combinatorics, McGraw-Hill, 2007.",
				"L. Comtet, Advanced Combinatorics: The Art of Finite and Infinite Expansions, Reidel Publishing Company, 1974.",
				"R. P. Stanley, Enumerative Combinatorics, second edition, Cambridge University Press, 2011."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A300401/b300401.txt\"\u003eTable of n, a(n) for n = 0..11475\u003c/a\u003e (rows 0 \u003c= n \u003c= 150, flattened).",
				"Cheyne Homberger, \u003ca href=\"https://arxiv.org/abs/1410.2657\"\u003ePatterns in Permutations and Involutions: A Structural and Enumerative Approach\u003c/a\u003e, arXiv preprint 1410.2657 [math.CO], 2014.",
				"Franck Ramaharo, \u003ca href=\"https://arxiv.org/abs/1902.08989\"\u003eA generating polynomial for the two-bridge knot with Conway's notation C(n,r)\u003c/a\u003e, arXiv:1902.08989 [math.CO], 2019."
			],
			"formula": [
				"T(n,k) = T(k,n) = n*A152947(k+1) + k*A152947(n+1).",
				"T(n,0) = A001477(n).",
				"T(n,1) = A000124(n).",
				"T(n,2) = A014206(n).",
				"T(n,3) = A273465(3*n+2).",
				"T(n,4) = A084849(n+1).",
				"T(n,n) = A179000(n-1,n), n \u003e= 1.",
				"T(2*n,2*n) = 8*A081436(n-1), n \u003e= 1.",
				"T(2*n+1,2*n+1) = 2*A006000(2*n+1).",
				"T(n,n+1) = A188377(n+3).",
				"T(n,n+2) = A188377(n+2), n \u003e= 1.",
				"Sum_{k=0..n} T(k,n-k) = 2*(binomial(n, 4) + binomial(n, 2)).",
				"G.f.: -((2*x*y - y - x)*(2*x*y - y - x + 1))/(((x - 1)*(y - 1))^3).",
				"E.g.f.: (1/2)*(x + y)*(x*y + 2)*exp(x + y)."
			],
			"example": [
				"The array T(n,k) begins",
				"0     1    2    3    4     5     6     7     8     9    10    11  ...",
				"1     2    4    7   11    16    22    29    37    46    56    67  ...",
				"2     4    8   14   22    32    44    58    74    92   112   134  ...",
				"3     7   14   24   37    53    72    94   119   147   178   212  ...",
				"4    11   22   37   56    79   106   137   172   211   254   301  ...",
				"5    16   32   53   79   110   146   187   233   284   340   401  ...",
				"6    22   44   72  106   146   192   244   302   366   436   512  ...",
				"7    29   58   94  137   187   244   308   379   457   542   634  ...",
				"8    37   74  119  172   233   302   379   464   557   658   767  ...",
				"9    46   92  147  211   284   366   457   557   666   784   911  ...",
				"10   56  112  178  254   340   436   542   658   784   920  1066  ...",
				"11   67  134  212  301   401   512   634   767   911  1066  1232  ...",
				"12   79  158  249  352   467   594   733   884  1047  1222  1409  ...",
				"13   92  184  289  407   538   682   839  1009  1192  1388  1597  ...",
				"14  106  212  332  466   614   776   952  1142  1346  1564  1796  ...",
				"15  121  242  378  529   695   876  1072  1283  1509  1750  2006  ...",
				"16  137  274  427  596   781   982  1199  1432  1681  1946  2227  ...",
				"17  154  308  479  667   872  1094  1333  1589  1862  2152  2459  ...",
				"18  172  344  534  742   968  1212  1474  1754  2052  2368  2702  ...",
				"19  191  382  592  821  1069  1336  1622  1927  2251  2594  2956  ...",
				"20  211  422  653  904  1175  1466  1777  2108  2459  2830  3221  ...",
				"...",
				"The inverse binomial transforms of the columns are",
				"0     1    2    3    4     5     6     7     8     9    10    11  ...  A001477",
				"1     1    2    4    7    11    22    29    37    45    56    67  ...  A152947",
				"0     1    2    3    4     5     6     7     8     9    10    11  ...  A001477",
				"0     0    0    0    0     0     0     0     0     0     0     0  ...",
				"0     0    0    0    0     0     0     0     0     0     0     0  ...",
				"0     0    0    0    0     0     0     0     0     0     0     0  ...",
				"..."
			],
			"maple": [
				"T := (n, k) -\u003e n*(binomial(k, 2) + 1) + k*(binomial(n, 2) + 1);",
				"for n from 0 to 20 do seq(T(n, k), k = 0 .. 20) od;"
			],
			"mathematica": [
				"T[n_, k_] := n (Binomial[k, 2] + 1) + k (Binomial[n, 2] + 1);",
				"Table[T[n - k, k], {n, 0, 11}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Dec 07 2018 *)"
			],
			"program": [
				"(Maxima)",
				"T(n, k) := n*(binomial(k, 2) + 1) + k*(binomial(n, 2) + 1)$",
				"for n:0 thru 20 do",
				"  print(makelist(T(n, k), k, 0, 20));",
				"(PARI) T(n, k) = n*(binomial(k,2) + 1) + k*(binomial(n,2) + 1);",
				"tabl(nn) = for (n=0, nn, for (k=0, nn, print1(T(n, k), \", \")); print); \\\\ _Michel Marcus_, Mar 12 2018"
			],
			"xref": [
				"Cf. A000124, A001477, A006000, A008815, A014206, A051601, A055469, A077028, A081436, A084849, A131074, A134394, A139600, A141387, A179000, A188377, A188382, A273465."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Franck Maminirina Ramaharo_, Mar 05 2018",
			"references": 5,
			"revision": 61,
			"time": "2019-04-10T21:54:35-04:00",
			"created": "2018-05-18T12:13:58-04:00"
		}
	]
}