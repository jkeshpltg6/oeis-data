{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A124323",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 124323,
			"data": "1,0,1,1,0,1,1,3,0,1,4,4,6,0,1,11,20,10,10,0,1,41,66,60,20,15,0,1,162,287,231,140,35,21,0,1,715,1296,1148,616,280,56,28,0,1,3425,6435,5832,3444,1386,504,84,36,0,1,17722,34250,32175,19440,8610,2772,840,120,45,0,1",
			"name": "Triangle read by rows: T(n,k) is the number of partitions of an n-set having k singleton blocks (0\u003c=k\u003c=n).",
			"comment": [
				"Row sums are the Bell numbers (A000110). T(n,0)=A000296(n). T(n,k) = binomial(n,k)*T(n-k,0). Sum(k*T(n,k),k=0..n) = A052889(n) = n*B(n-1), where B(q) are the Bell numbers (A000110).",
				"Exponential Riordan array [exp(exp(x)-1-x),x]. - _Paul Barry_, Apr 23 2009",
				"Sum_{k=0..n} T(n,k)*2^k = A000110(n+1) is the number of binary relations on an n-set that are both symmetric and transitive. - _Geoffrey Critzer_, Jul 25 2014",
				"Also the number of set partitions of {1, ..., n} with k cyclical adjacencies (successive elements in the same block, where 1 is a successor of n). Unlike A250104, we count {{1}} as having 1 cyclical adjacency. - _Gus Wiseman_, Feb 13 2019"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A124323/b124323.txt\"\u003eRows n = 0..140, flattened\u003c/a\u003e",
				"David Callan, \u003ca href=\"https://arxiv.org/abs/math/0508052\"\u003eOn conjugates for set partitions and integer compositions\u003c/a\u003e, arXiv:math/0508052 [math.CO], 2005.",
				"T. Mansour, A. O. Munagi, \u003ca href=\"https://doi.org/10.1016/j.ejc.2014.06.008\"\u003eSet partitions with circular successions\u003c/a\u003e, European Journal of Combinatorics, 42 (2014), 207-216."
			],
			"formula": [
				"T(n,k) = binomial(n,k)*[(-1)^(n-k)+sum((-1)^(j-1)*B(n-k-j), j=1..n-k)], where B(q) are the Bell numbers (A000110).",
				"E.g.f.: G(t,z) = exp(exp(z)-1+(t-1)*z)).",
				"G.f.: 1/(1-xy-x^2/(1-xy-x-2x^2/(1-xy-2x-3x^2/(1-xy-3x-4x^2/(1-... (continued fraction). - _Paul Barry_, Apr 23 2009"
			],
			"example": [
				"T(4,2)=6 because we have 12|3|4, 13|2|4, 14|2|3, 1|23|4, 1|24|3 and 1|2|34 (if we take {1,2,3,4} as our 4-set).",
				"Triangle starts:",
				"     1",
				"     0    1",
				"     1    0    1",
				"     1    3    0    1",
				"     4    4    6    0    1",
				"    11   20   10   10    0    1",
				"    41   66   60   20   15    0    1",
				"   162  287  231  140   35   21    0    1",
				"   715 1296 1148  616  280   56   28    0    1",
				"  3425 6435 5832 3444 1386  504   84   36    0    1",
				"From _Gus Wiseman_, Feb 13 2019: (Start)",
				"Row n = 5 counts the following set partitions by number of singletons:",
				"  {{1234}}    {{1}{234}}  {{1}{2}{34}}  {{1}{2}{3}{4}}",
				"  {{12}{34}}  {{123}{4}}  {{1}{23}{4}}",
				"  {{13}{24}}  {{124}{3}}  {{12}{3}{4}}",
				"  {{14}{23}}  {{134}{2}}  {{1}{24}{3}}",
				"                          {{13}{2}{4}}",
				"                          {{14}{2}{3}}",
				"... and the following set partitions by number of cyclical adjacencies:",
				"  {{13}{24}}      {{1}{2}{34}}  {{1}{234}}  {{1234}}",
				"  {{1}{24}{3}}    {{1}{23}{4}}  {{12}{34}}",
				"  {{13}{2}{4}}    {{12}{3}{4}}  {{123}{4}}",
				"  {{1}{2}{3}{4}}  {{14}{2}{3}}  {{124}{3}}",
				"                                {{134}{2}}",
				"                                {{14}{23}}",
				"(End)",
				"From _Paul Barry_, Apr 23 2009: (Start)",
				"Production matrix is",
				"0, 1,",
				"1, 0, 1,",
				"1, 2, 0, 1,",
				"1, 3, 3, 0, 1,",
				"1, 4, 6, 4, 0, 1,",
				"1, 5, 10, 10, 5, 0, 1,",
				"1, 6, 15, 20, 15, 6, 0, 1,",
				"1, 7, 21, 35, 35, 21, 7, 0, 1,",
				"1, 8, 28, 56, 70, 56, 28, 8, 0, 1 (End)"
			],
			"maple": [
				"G:=exp(exp(z)-1+(t-1)*z): Gser:=simplify(series(G,z=0,14)): for n from 0 to 11 do P[n]:=sort(n!*coeff(Gser,z,n)) od: for n from 0 to 11 do seq(coeff(P[n],t,k),k=0..n) od; # yields sequence in triangular form",
				"# Program from _R. J. Mathar_, Jan 22 2015:",
				"A124323 := proc(n,k)",
				"    binomial(n,k)*A000296(n-k) ;",
				"end proc:"
			],
			"mathematica": [
				"Flatten[CoefficientList[Range[0,10]! CoefficientList[Series[Exp[x y] Exp[Exp[x] - x - 1], {x, 0,10}], x], y]] (* _Geoffrey Critzer_, Nov 24 2011 *)",
				"sps[{}]:={{}};sps[set:{i_,___}]:=Join@@Function[s,Prepend[#,s]\u0026/@sps[Complement[set,s]]]/@Cases[Subsets[set],{i,___}];",
				"Table[Length[Select[sps[Range[n]],Count[#,{_}]==k\u0026]],{n,0,9},{k,0,n}] (* _Gus Wiseman_, Feb 13 2019 *)"
			],
			"xref": [
				"Cf. A000110, A052889, A124324.",
				"A250104 is an essentially identical triangle, differing only in row 1.",
				"For columns see A000296, A250105, A250106, A250107.",
				"Cf. A000126, A001610, A032032, A052841, A066982, A080107, A169985, A187784, A324011, A324014, A324015."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Emeric Deutsch_, Oct 28 2006",
			"references": 18,
			"revision": 35,
			"time": "2020-03-30T03:18:23-04:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}