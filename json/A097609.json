{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097609",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97609,
			"data": "1,0,1,1,0,1,1,2,0,1,3,2,3,0,1,6,7,3,4,0,1,15,14,12,4,5,0,1,36,37,24,18,5,6,0,1,91,90,67,36,25,6,7,0,1,232,233,165,106,50,33,7,8,0,1,603,602,438,264,155,66,42,8,9,0,1,1585,1586,1147,719,390,215,84,52,9,10,0,1",
			"name": "Triangle read by rows: T(n,k) is number of Motzkin paths of length n having k horizontal steps at level 0.",
			"comment": [
				"Row sums give the Motzkin numbers (A001006).",
				"Column 0 is A005043.",
				"Riordan array ((1+x-sqrt(1-2*x-3*x^2))/(2*x*(1-x)), (1+x-sqrt(1-2*x-3*x^2))/(2*(1-x))). - _Paul Barry_, Jun 21 2008",
				"Inverse of Riordan array ((1-x)/(1-x+x^2), x*(1-x)/(1-x+x^2)), which is A104597. - _Paul Barry_, Jun 21 2008",
				"Triangle read by rows, product of A064189 and A130595 considered as infinite lower triangular arrays; A097609 = A064189*A130195 = B*A053121*B^(-1) where B = A007318. - _Philippe Deléham_, Dec 07 2009",
				"T(n+1,1) = A187306(n). - _Philippe Deléham_, Jan 28 2014",
				"The number of lattice paths from (0,0) to (n,k) that do not cross below the x-axis and use up-step=(1,1) and down-steps=(1,-z) where z is a positive integer. For example, T(4,0) = 3: [(1,1)(1,1)(1,-1)(1,-1)], [(1,1)(1,-1)(1,1)(1,-1)] and [(1,1)(1,1)(1,1)(1,-3)]. - _Nicholas Ham_, Aug 20 2015"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A097609/b097609.txt\"\u003eRows n = 0..100 of triangle, flattened\u003c/a\u003e",
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/1912.01126\"\u003eRiordan arrays, the A-matrix, and Somos 4 sequences\u003c/a\u003e, arXiv:1912.01126 [math.CO], 2019.",
				"I. Dolinka, J. East, R. D. Gray, \u003ca href=\"http://arxiv.org/abs/1512.02279\"\u003eMotzkin monoids and partial Brauer monoids\u003c/a\u003e, arXiv preprint arXiv:1512.02279 [math.GR], 2015.",
				"D. Merlini, R. Sprugnoli and M. C. Verri, \u003ca href=\"http://dx.doi.org/10.1007/978-3-0348-8405-1_11\"\u003eAn algebra for proper generating trees\u003c/a\u003e, Trends in Mathematics 2000, pp 127-139."
			],
			"formula": [
				"G.f.: 2/(1 -2*t*z +z +sqrt(1-2*z-3*z^2)).",
				"T(n,k) = T(n-1,k-1)+ Sum_{j\u003e=1} T(n-1,k+j) with T(0,0)=1. - _Philippe Deléham_, Jan 23 2010",
				"T(n,k) = (k/n)*Sum_{j=k..n} (-1)^(n-j)*C(n,j)*C(2*j-k-1,j-1), n\u003e0. - _Vladimir Kruchinin_, Feb 05 2011"
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  0, 1;",
				"  1, 0, 1;",
				"  1, 2, 0, 1;",
				"  3, 2, 3, 0, 1;",
				"  6, 7, 3, 4, 0, 1;",
				"Row n has n+1 terms.",
				"T(5,2) = 3 because (HH)UHD,(H)UHD(H) and UHD(HH) are the only Motzkin paths of length 5 with 2 horizontal steps at level 0 (shown between parentheses); here U=(1,1), H=(1,0) and D=(1,-1).",
				"Production matrix begins",
				"  0, 1;",
				"  1, 0, 1;",
				"  1, 1, 0, 1;",
				"  1, 1, 1, 0, 1;",
				"  1, 1, 1, 1, 0, 1;",
				"  1, 1, 1, 1, 1, 0, 1;",
				"  1, 1, 1, 1, 1, 1, 0, 1;",
				"  1, 1, 1, 1, 1, 1, 1, 0, 1;",
				"  1, 1, 1, 1, 1, 1, 1, 1, 0, 1;",
				"... - _Philippe Deléham_, Mar 02 2013"
			],
			"maple": [
				"G:=2/(1-2*t*z+z+sqrt(1-2*z-3*z^2)): Gser:=simplify(series(G,z=0,13)): P[0]:=1: for n from 1 to 12 do P[n]:=sort(coeff(Gser,z^n)) od: seq(seq(coeff(t*P[n], t^k),k=1..n+1),n=0..12);"
			],
			"mathematica": [
				"nmax = 12; t[n_, k_] := ((-1)^(n+k)*k*n!*HypergeometricPFQ[{(k+1)/2, k/2, k-n}, {k, k+1}, 4])/(n*k!*(n-k)!); Flatten[ Table[t[n, k], {n, 0, nmax}, {k, 1, n}]] (* _Jean-François Alcover_, Nov 14 2011, after _Vladimir Kruchinin_ *)"
			],
			"program": [
				"(PARI) T(n,k) = ((k+1)/(n+1))*sum(j=k+1, n+1, (-1)^(n-j+1)*binomial(n+1,j)* binomial(2*j-k-2,j-1) ); \\\\ _G. C. Greubel_, Feb 18 2020",
				"(MAGMA) [((k+1)/(n+1))*(\u0026+[(-1)^(n-j+1)*Binomial(n+1,j)*Binomial(2*j-k-2,j-1): j in [k+1..n+1]]): k in [0..n], n in [0..10]]; // _G. C. Greubel_, Feb 18 2020",
				"(Sage) [[((k+1)/(n+1))*sum( (-1)^(n-j+1)*binomial(n+1,j)* binomial(2*j-k-2,j-1) for j in (k+1..n+1)) for k in (0..n)] for n in (0..10)] # _G. C. Greubel_, Feb 18 2020"
			],
			"xref": [
				"Cf. A001006, A005043, A187306."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Emeric Deutsch_, Aug 30 2004",
			"references": 12,
			"revision": 48,
			"time": "2020-02-19T07:25:52-05:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}