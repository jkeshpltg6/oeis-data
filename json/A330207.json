{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A330207",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 330207,
			"data": "14,35,119,169,385,434,574,741,779,899,935,961,1105,1106,1121,1189,1443,1479,2001,2419,2555,2915,3059,3107,3383,3605,3689,3741,3781,3827,4199,4795,4879,4901,5719,6061,6083,6215,6265,6441,6479,6601,6895,6929,6931,6965",
			"name": "Chebyshev pseudoprimes to base 3: composite numbers k such that T(k, 3) == 3 (mod k), where T(k, x) is the k-th Chebyshev polynomial of the first kind.",
			"comment": [
				"Bang proved that T(p, a) == a (mod p) for every a \u003e 0 and every odd prime. Rayes et al. (1999) defined Chebyshev pseudoprimes to base a as composite numbers k such that T(k, a) == a (mod k)."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A330207/b330207.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Thøger Bang, \u003ca href=\"https://www.jstor.org/stable/24489044\"\u003eCongruence properties of Tchebycheff polynomials\u003c/a\u003e, Mathematica Scandinavica, Vol. 2, No. 2 (1955), pp. 327-333, \u003ca href=\"https://www.mscand.dk/article/download/10418/8439\"\u003ealternative link\u003c/a\u003e,",
				"David Pokrass Jacobs, Mohamed O. Rayes, and Vilmar Trevisan. \u003ca href=\"http://mi.mathnet.ru/eng/adm159\"\u003eCharacterization of Chebyshev Numbers\u003c/a\u003e, Algebra and Discrete Mathematics, Vol. 2 (2008), pp. 65-82.",
				"Mohamed O. Rayes, Vilmar Trevisan, and Paul S. Wangy, \u003ca href=\"http://icm.mcs.kent.edu/reports/1999/chebpol.pdf\"\u003eChebyshev Polynomials and Primality Tests\u003c/a\u003e, ICM Technical Report, Kent State University, Kent, Ohio, 1999. See page 8.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ChebyshevPolynomialoftheFirstKind.html\"\u003eChebyshev Polynomial of the First Kind\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Chebyshev_polynomials\"\u003eChebyshev polynomials\u003c/a\u003e."
			],
			"example": [
				"14 is in the sequence since it is composite and T(14, 3) = 26102926097 == 3 (mod 14)."
			],
			"mathematica": [
				"Select[Range[1000], CompositeQ[#] \u0026\u0026 Divisible[ChebyshevT[#, 3] - 3, #] \u0026]"
			],
			"xref": [
				"Cf. A053120, A175530, A330206, A330208."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Amiram Eldar_, Dec 05 2019",
			"references": 3,
			"revision": 17,
			"time": "2019-12-06T10:29:37-05:00",
			"created": "2019-12-06T09:24:39-05:00"
		}
	]
}