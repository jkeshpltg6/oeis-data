{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A035168",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 35168,
			"data": "1,1,0,1,0,0,0,1,1,0,1,0,2,0,0,1,0,1,2,0,0,1,2,0,1,2,0,0,2,0,2,1,0,0,0,1,0,2,0,0,0,0,2,1,0,2,2,0,1,1,0,2,0,0,0,0,0,2,0,0,2,2,0,1,0,0,0,0,0,0,2,1,0,0,0,2,0,0,0,0,1",
			"name": "a(n) = Sum_{d|n} Kronecker(-22, d).",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A035168/b035168.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of (phi(q) * phi(q^22) + phi(q^2) * phi(q^11)) / 2 - 1 in powers of q where phi() is a Ramanujan theta function. - _Michael Somos_, May 05 2015",
				"a(n) is multiplicative with a(p^e) = 1 if p = 2 or 11, a(p^e) = e+1 if Kronecker(-22, p) = +1, a(p^e) = (1 + (-1)^e)/2 if Kronecker(-22, p) = -1. - _Michael Somos_, May 05 2015",
				"G.f.: Sum_{k\u003e0} x^k / (1 - x^k) * Kronecker(-22, k). - _Michael Somos_, May 05 2015",
				"a(n) = A255647(n) unless n = 0. - _Michael Somos_, May 05 2015"
			],
			"example": [
				"G.f. = q + q^2 + q^4 + q^8 + q^9 + q^11 + 2*q^13 + q^16 + q^18 + 2*q^19 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 1, 0, DivisorSum[ n, KroneckerSymbol[ -22, #] \u0026]]; (* _Michael Somos_, May 05 2015 *)",
				"a[ n_] := If[ n \u003c 1, 0, Sum[ KroneckerSymbol[ -22, d], { d, Divisors[ n]}]]; (* _Michael Somos_, May 05 2015 *)",
				"a[ n_] := SeriesCoefficient[ (EllipticTheta[ 3, 0, q] EllipticTheta[ 3, 0, q^22] + EllipticTheta[ 3, 0, q^2] EllipticTheta[ 3, 0, q^11]) / 2 - 1, {q, 0, n}]; (* _Michael Somos_, May 05 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, 0, sumdiv(n, d, kronecker( -22, d)))}; /* _Michael Somos_, May 05 2015 */",
				"(PARI) {a(n) = if( n\u003c1, 0, direuler(p=2, n, 1 / ((1 - X) * (1 - kronecker( -22, p) * X)))[n])}; /* _Michael Somos_, May 05 2015 */",
				"(PARI) {a(n) = my(A, p, e); if( n\u003c1, 0, A = factor(n); prod( k=1, matsize(A)[1], [p, e] = A[k, ]; if( p==2 || p==11, 1, kronecker( -22, p) == 1, e+1, 1-e%2)))}; /* _Michael Somos_, May 05 2015 */"
			],
			"xref": [
				"Cf. A255647."
			],
			"keyword": "nonn,mult",
			"offset": "1,13",
			"author": "_N. J. A. Sloane_",
			"references": 3,
			"revision": 16,
			"time": "2021-03-12T22:24:42-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}