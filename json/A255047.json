{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A255047",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 255047,
			"data": "1,1,3,7,15,31,63,127,255,511,1023,2047,4095,8191,16383,32767,65535,131071,262143,524287,1048575,2097151,4194303,8388607,16777215,33554431,67108863,134217727,268435455,536870911,1073741823,2147483647,4294967295",
			"name": "1 together with the positive terms of A000225.",
			"comment": [
				"Also, right border of A246674 arranged as an irregular triangle.",
				"Essentially the same as A168604, A126646 and A000225.",
				"Total number of lambda-parking functions induced by all partitions of n. a(0)=1: [], a(1)=1: [1], a(2)=3: [1], [2], [1,1], a(4)=7: [1], [2], [3], [1,1], [1,2], [2,1], [1,1,1]. - _Alois P. Heinz_, Dec 04 2015",
				"Also, the decimal representation of the diagonal from the origin to the corner of the n-th stage of growth of the two-dimensional cellular automaton defined by \"Rule 645\", based on the 5-celled von Neumann neighborhood, initialized with a single black (ON) cell at stage zero. - _Robert Price_, Jul 19 2017",
				"Also number of multiset partitions of {1,1} U [n] into exactly 2 nonempty parts.  a(2) = 3: 111|2, 11|12, 1|112. - _Alois P. Heinz_, Aug 18 2017",
				"Also, the number of unlabeled connected P-series (equivalently, connected P-graphs) with n+1 elements. - _Salah Uddin Mohammad_, Nov 19 2021"
			],
			"reference": [
				"S. Wolfram, A New Kind of Science, Wolfram Media, 2002; p. 170."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A255047/b255047.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"http://arxiv.org/abs/1503.01168\"\u003eOn the Number of ON Cells in Cellular Automata\u003c/a\u003e, arXiv:1503.01168 [math.CO], 2015.",
				"R. Stanley, \u003ca href=\"http://math.mit.edu/~rstan/transparencies/parking.pdf\"\u003eParking Functions\u003c/a\u003e, 2011.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ElementaryCellularAutomaton.html\"\u003eElementary Cellular Automaton\u003c/a\u003e",
				"S. Wolfram, \u003ca href=\"http://wolframscience.com/\"\u003eA New Kind of Science\u003c/a\u003e",
				"Wolfram Research, \u003ca href=\"http://atlas.wolfram.com/\"\u003eWolfram Atlas of Simple Programs\u003c/a\u003e",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_2D_5-Neighbor_Cellular_Automata\"\u003eIndex to 2D 5-Neighbor Cellular Automata\u003c/a\u003e",
				"\u003ca href=\"https://oeis.org/wiki/Index_to_Elementary_Cellular_Automata\"\u003eIndex to Elementary Cellular Automata\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-2)."
			],
			"formula": [
				"From _Alois P. Heinz_, Feb 19 2015: (Start)",
				"O.g.f.: (1 -2*x +2*x^2)/((1-x)*(1-2*x)).",
				"E.g.f.: exp(2*x) - exp(x) + 1. (End)",
				"a(n) = A078485(n+1) for n \u003e 2. - _Georg Fischer_, Oct 22 2018"
			],
			"mathematica": [
				"CoefficientList[Series[(1 -2*x +2*x^2)/((1-x)*(1-2*x)), {x, 0, 33}], x] (* or *) LinearRecurrence[{3, -2}, {1,1,3}, 40] (* _Vincenzo Librandi_, Jul 20 2017 *)",
				"Table[2^n -1 +Boole[n==0], {n, 0, 40}] (* _G. C. Greubel_, Feb 07 2021 *)"
			],
			"program": [
				"(Sage) [1]+[2^n -1 for n in (1..40)] # _G. C. Greubel_, Feb 07 2021",
				"(Magma) [1] cat [2^n -1: n in [1..40]]; // _G. C. Greubel_, Feb 07 2021"
			],
			"xref": [
				"Cf. A000225, A011782, A028310, A246674, A253909, A265007, A265202, A349276.",
				"Row n=1 of A263159.",
				"Column k=2 of A291117.",
				"Cf. A078485."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Omar E. Pol_, Feb 15 2015",
			"references": 8,
			"revision": 53,
			"time": "2021-12-28T00:02:00-05:00",
			"created": "2015-02-17T00:13:40-05:00"
		}
	]
}