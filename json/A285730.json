{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A285730",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 285730,
			"data": "1,1,2,1,2,3,1,2,3,2,1,2,3,4,5,1,2,3,4,5,3,1,2,3,4,5,6,7,1,2,3,4,5,6,7,2,1,2,3,4,5,6,7,4,3,1,2,3,4,5,6,7,8,9,5,1,2,3,4,5,6,7,8,9,10,11,1,2,3,4,5,6,7,8,9,10,11,3,1,2,3,4,5,6,7,8,9,10,11,6,13,1,2,3,4,5,6,7,8,9,10,11,12,13,7",
			"name": "Square array: If A001222(n) \u003c k, then A(n,k) = n, otherwise A(n,k) = product of k largest prime factors of n (taken with multiplicity), read by descending antidiagonals.",
			"comment": [
				"Square array A(n,k) [where n is row and k is column] is read by descending antidiagonals: A(1,1), A(1,2), A(2,1), A(1,3), A(2,2), A(3,1), etc"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A285730/b285730.txt\"\u003eTable of n, a(n) for n = 1..10440; the first 144 antidiagonals of array\u003c/a\u003e"
			],
			"formula": [
				"A(n,1) = A006530(n), for k \u003e 1, A(n,k) = A006530(n) * A(n/A006530(n),k-1)."
			],
			"example": [
				"The top left 5x18 corner of the array:",
				"   1,  1,  1,  1,  1",
				"   2,  2,  2,  2,  2",
				"   3,  3,  3,  3,  3",
				"   2,  4,  4,  4,  4",
				"   5,  5,  5,  5,  5",
				"   3,  6,  6,  6,  6",
				"   7,  7,  7,  7,  7",
				"   2,  4,  8,  8,  8",
				"   3,  9,  9,  9,  9",
				"   5, 10, 10, 10, 10",
				"  11, 11, 11, 11, 11",
				"   3,  6, 12, 12, 12",
				"  13, 13, 13, 13, 13",
				"   7, 14, 14, 14, 14",
				"   5, 15, 15, 15, 15",
				"   2,  4,  8, 16, 16",
				"  17, 17, 17, 17, 17",
				"   3,  9, 18, 18, 18",
				"For A(18,1) we take just the largest prime factor of 18 = 2*3*3, thus A(18,1) = 3.",
				"For A(18,2) we take the product of two largest prime factors of 18 (duplicates not discarded), thus A(18,2) = 3*3 = 9.",
				"For A(18,3) we take the product of three largest prime factors of 18, thus A(18,2) = 3*3*2 = 18."
			],
			"mathematica": [
				"With[{nn = 14}, Function[s, Table[s[[#, k]] \u0026[n - k + 1], {n, nn}, {k, n, 1, -1}]]@ MapIndexed[PadRight[#1, nn, First@ #2] \u0026, Table[FoldList[Times, Reverse@ Flatten[FactorInteger[n] /. {p_, e_} /; e \u003e 0 :\u003e ConstantArray[p, e]]], {n, nn}]]] // Flatten (* _Michael De Vlieger_, Apr 28 2017 *)"
			],
			"program": [
				"(Scheme)",
				"(define (A285730 n) (A285730bi (A002260 n) (A004736 n)))",
				"(define (A285730bi row col) (let loop ((n row) (k col) (m 1)) (if (zero? k) m (loop (/ n (A006530 n)) (- k 1) (* m (A006530 n))))))",
				";; Alternatively, implemented with the given recurrence formula:",
				"(define (A285730bi row col) (if (= 1 col) (A006530 row) (* (A006530 row) (A285730bi (A052126 row) (- col 1)))))",
				"(Python)",
				"from sympy import primefactors",
				"def a006530(n): return 1 if n==1 else max(primefactors(n))",
				"def A(n, k): return a006530(n) if k==1 else a006530(n)*A(n/a006530(n), k - 1)",
				"for n in range(1, 21): print [A(k, n - k + 1) for k in range(1, n + 1)] # _Indranil Ghosh_, Apr 28 2017"
			],
			"xref": [
				"Transpose: A285731.",
				"Cf. A006530 (the leftmost column).",
				"Cf. A001222, A052126."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Antti Karttunen_, Apr 28 2017",
			"references": 3,
			"revision": 15,
			"time": "2019-12-07T12:18:29-05:00",
			"created": "2017-04-30T07:55:28-04:00"
		}
	]
}