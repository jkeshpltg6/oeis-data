{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A114717",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 114717,
			"data": "1,1,1,1,1,2,1,1,1,2,1,5,1,2,2,1,1,5,1,5,2,2,1,14,1,2,1,5,1,48,1,1,2,2,2,42,1,2,2,14,1,48,1,5,5,2,1,42,1,5,2,5,1,14,2,14,2,2,1,2452,1,2,5,1,2,48,1,5,2,48,1,462,1,2,5,5,2,48,1,42,1,2,1,2452,2,2,2,14,1,2452,2",
			"name": "Number of linear extensions of the divisor lattice of n.",
			"comment": [
				"Notice that only the powers of the primes determine a(n), so a(12) = a(75) = 5.",
				"For prime powers, the lattice is a chain, so there is 1 linear extension.",
				"a(p^1*q^n) = A000108(n+1), the Catalan numbers.",
				"Alternatively, the number of ways to arrange the divisors of n in such a way that no divisor has any of its own divisors following it. E.g. for 12, the following five arrangements are possible: 1,2,3,4,6,12; 1,2,3,6,4,12; 1,2,4,3,6,12; 1,3,2,4,6,12 and 1,3,2,6,4,12. But 1,2,6,4,3,12 is not possible because 3 divides 6 but follows it. Thus a(12)=5. - _Antti Karttunen_, Jan 11 2006",
				"For n = p1^r1 * p2^r2, the lattice is a grid (r1+1)*(r2+1), whose linear extensions are counted by ((r1+1)(r2+1))!/Product[(r1+1+k)!/k!,{k,0,r2}]. Cf. A060854."
			],
			"reference": [
				"Stanley, R., Enumerative Combinatorics, Vol. 2, Proposition 7.10.3 and Vol. 1, Sec 3.5 Chains in Distributive Lattices."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A114717/b114717.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Graham Brightwell and Peter Winkler, \u003ca href=\"https://doi.org/10.1007/BF00383444\"\u003eCounting linear extensions\u003c/a\u003e, Order 8 (1991), no. 3, 225-242.",
				"Gary Pruesse and Frank Ruskey, \u003ca href=\"http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.52.3057\"\u003eGenerating linear extensions fast\u003c/a\u003e, SIAM J.Comput.23 (1994), no. 2, 373-386.",
				"\u003ca href=\"/index/Eu#epf\"\u003eIndex entries for sequences computed from exponents in factorization of n\u003c/a\u003e"
			],
			"maple": [
				"with(numtheory):",
				"b:= proc(s) option remember;",
				"      `if`(nops(s)\u003c2, 1, add(`if`(nops(select(y-\u003e",
				"       irem(y, x)=0, s))=1, b(s minus {x}), 0), x=s))",
				"    end:",
				"a:= proc(n) local l, m;",
				"      l:= sort(ifactors(n)[2], (x, y)-\u003e x[2]\u003ey[2]);",
				"      m:= mul(ithprime(i)^l[i][2], i=1..nops(l));",
				"      b(divisors(m) minus {1, m})",
				"    end:",
				"seq(a(n), n=1..100);  # _Alois P. Heinz_, Jun 29 2012"
			],
			"mathematica": [
				"b[s_List] := b[s] = If[Length[s]\u003c2, 1, Sum[If[Length[Select[s, Mod[#, x] == 0 \u0026]] == 1, b[Complement[s, {x}]], 0], {x, s}]]; a[n_] := Module[{l, m}, l = Sort[ FactorInteger[n], #1[[2]] \u003e #2[[2]] \u0026]; m = Product[Prime[i]^l[[i]][[2]], {i, 1, Length[l]}]; b[Divisors[m] // Rest // Most]]; Table[a[n], {n, 1, 100}] (* _Jean-François Alcover_, May 28 2015, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A060854, A114714, A114715, A114716, A119842."
			],
			"keyword": "nonn,hard",
			"offset": "1,6",
			"author": "_Mitch Harris_ and _Antti Karttunen_, Dec 27 2005",
			"references": 12,
			"revision": 32,
			"time": "2019-02-28T11:46:44-05:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}