{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A103435",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 103435,
			"data": "0,2,4,16,48,160,512,1664,5376,17408,56320,182272,589824,1908736,6176768,19988480,64684032,209321984,677380096,2192048128,7093616640,22955425792,74285318144,240392339456,777925951488,2517421260800",
			"name": "a(n) = 2^n * Fibonacci(n).",
			"comment": [
				"Cardinality of set of bracelets of size at most n that are tiled with two types of colored squares and four types of colored dominoes.",
				"a(n) is also the diagonal element of the matrix A(i,j) whose first row (i=1) and first column (j=1) are the Fibonacci numbers: A(1,k)=A(k,1)=fib(k) and whose generic element is the sum of element in adjacent (preceding) row and column minus the absolute value of their difference. So a(n) = A(n,n) = A(i-1,j)+A(i,j-1)-abs(A(i-1,j)-A(i,j-1)). - _Carmine Suriano_, May 13 2010",
				"a(n) is the coefficient of x in the reduction by x^2-\u003ex+1 of the polynomial p(n,x) given for d=sqrt(x+1) by p(n,x)=((x+d)^n-(x-d)^n)/(2d), for n\u003e=1.  The constant terms under this reduction are the absolute values of terms of A086344.  See A192232 for a discussion of reduction. - _Clark Kimberling_, Jun 29 2011",
				"The exponential convolution of A000032 and A000045. - _Vladimir Reshetnikov_, Oct 06 2016"
			],
			"reference": [
				"Arthur T. Benjamin and Jennifer J. Quinn, Proofs that really count: the art of combinatorial proof, M.A.A., 2003, identity 236, p. 131."
			],
			"link": [
				"Tom Edgar, \u003ca href=\"http://www.fq.math.ca/Abstracts/54-1/edgar.pdf\"\u003eExtending Some Fibonacci-Lucas Relations\u003c/a\u003e, The Fibonacci Quarterly, Vol. 54, No. 1 (2016), p. 79; \u003ca href=\"https://community.plu.edu/~edgartj/fiblucas.pdf\"\u003ealternative link\u003c/a\u003e.",
				"Harris Kwong, \u003ca href=\"http://dx.doi.org/10.4169/amer.math.monthly.121.06.514\"\u003eAn Alternate Proof of Sury's Fibonacci-Lucas Relation\u003c/a\u003e, The American Mathematical Monthly, 121(6), (2014), p. 514.",
				"Diego Marques, \u003ca href=\"http://www.jstor.org/stable/10.4169/amer.math.monthly.122.7.683\"\u003eA new Fibonacci-Lucas relation\u003c/a\u003e, Amer. Math. Monthly, Vol. 122, No. 7 (2015), p. 683.",
				"Ivica Martinjak and Helmut Prodinger, \u003ca href=\"http://arxiv.org/abs/1508.04949\"\u003eComplementary Families of the Fibonacci-Lucas Relations\u003c/a\u003e, arXiv:1508.04949 [math.CO], 2015-2016.",
				"B. Sury, \u003ca href=\"http://www.jstor.org/stable/10.4169/amer.math.monthly.121.03.236\"\u003eA polynomial parent to a Fibonacci-Lucas relations\u003c/a\u003e, Amer. Math. Monthly, Vol. 121, No. 3 (2014), p. 236.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,4)."
			],
			"formula": [
				"G.f.: 2*x / (1 - 2*x - 4*x^2).",
				"a(n) = Sum_{i=0..n-1}( 2^i * Lucas(i) ).",
				"a(n) = 2*a(n-1) + 4*a(n-2). - _Carmine Suriano_, May 13 2010",
				"a(n) = a(-n) * -(-4)^n for all n in Z. - _Michael Somos_, Sep 20 2014",
				"E.g.f.: 2*sinh(sqrt(5)*x)*exp(x)/sqrt(5). - _Ilya Gutkovskiy_, May 10 2016",
				"Sum_{n\u003e=1} 1/a(n) = (1/2) * A269991. - _Amiram Eldar_, Nov 17 2020",
				"a(n) == 2*n (mod 10). - _Amiram Eldar_, Jan 15 2022"
			],
			"example": [
				"a(5)=160=A(5,5)=A(4,5)+A(5,4)-abs[A(4,5)+A(5,4)]=80+80-0. - _Carmine Suriano_, May 13 2010",
				"G.f. = 2*x + 4*x^2 + 16*x^3 + 48*x^4 + 160*x^5 + 512*x^6 + 1664*x^7 + ..."
			],
			"mathematica": [
				"Expand[Table[((1 + Sqrt[5])^n - (1 - Sqrt[5])^n)5/(5 Sqrt[5]), {n, 0, 25}]] (* _Zerinvary Lajos_, Mar 22 2007 *)",
				"Table[2^n Fibonacci[n],{n,0,40}] (* or *) LinearRecurrence[{2,4},{0,2},40] (* _Harvey P. Dale_, Oct 14 2020 *)"
			],
			"program": [
				"(MAGMA) [2^n *Fibonacci(n): n in [0..50]]; // _Vincenzo Librandi_, Apr 04 2011",
				"(PARI) a(n)=fibonacci(n)\u003c\u003cn \\\\ _Charles R Greathouse IV_, Feb 03 2014",
				"(PARI) concat(0, Vec(2*x/(1-2*x-4*x^2) + O(x^99))) \\\\ _Altug Alkan_, May 11 2016"
			],
			"xref": [
				"a(n) = A006483(n) + 1 = 2*A085449(n) = 2*A063727(n-1), n\u003e0.",
				"First differences of A014334. Partial sums of A087131.",
				"Cf. A269991."
			],
			"keyword": "nonn,easy,changed",
			"offset": "0,2",
			"author": "_Ralf Stephan_, Feb 08 2005",
			"references": 25,
			"revision": 73,
			"time": "2022-01-15T10:00:24-05:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}