{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A051626",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 51626,
			"data": "0,0,1,0,0,1,6,0,1,0,2,1,6,6,1,0,16,1,18,0,6,2,22,1,0,6,3,6,28,1,15,0,2,16,6,1,3,18,6,0,5,6,21,2,1,22,46,1,42,0,16,6,13,3,2,6,18,28,58,1,60,15,6,0,6,2,33,16,22,6,35,1,8,3,1,18,6,6,13,0,9,5,41,6,16,21,28,2,44,1",
			"name": "Period of decimal representation of 1/n, or 0 if 1/n terminates.",
			"comment": [
				"Essentially same as A007732.",
				"For any prime number p: if a(p) \u003e 0, a(p) divides p-1. - _David Spitzer_, Jan 09 2017"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A051626/b051626.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Project Euler, \u003ca href=\"https://projecteuler.net/problem=26\"\u003eReciprocal cycles: Problem 26\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RepeatingDecimal.html\"\u003eRepeating Decimal\u003c/a\u003e",
				"\u003ca href=\"/index/1#1overn\"\u003eIndex entries for sequences related to decimal expansion of 1/n\u003c/a\u003e"
			],
			"formula": [
				"a(n)=A132726(n,1); a(n)=a(A132740(n)); a(A132741(n))=a(A003592(n))=0. - _Reinhard Zumkeller_, Aug 27 2007"
			],
			"example": [
				"From _M. F. Hasler_, Dec 14 2015: (Start)",
				"a(1) = a(2) = 0 because 1/1 = 1 and 1/2 = 0.5 have a finite decimal expansion.",
				"a(3) = a(6) = a(9) = a(12) = 1 because 1/3 = 0.{3}*, 1/6 = 0.1{6}*, 1/9 = 0.{1}*, 1/12 = 0.08{3}* where the sequence of digits {...}* which repeats indefinitely is of length 1.",
				"a(7) = 6 because 1/7 = 0.{142857}* with a period of 6.",
				"a(17) = 16 because 1/17 = 0.{0588235294117647}* with a period of 16.",
				"a(19) = 18 because 1/19 = 0.{052631578947368421}* with a period of 18. (End)"
			],
			"maple": [
				"A051626 := proc(n) local lpow,mpow ;",
				"    if isA003592(n) then",
				"       RETURN(0) ;",
				"    else",
				"       lpow:=1 ;",
				"       while true do",
				"          for mpow from lpow-1 to 0 by -1 do",
				"              if (10^lpow-10^mpow) mod n =0 then",
				"                 RETURN(lpow-mpow) ;",
				"              fi ;",
				"          od ;",
				"          lpow := lpow+1 ;",
				"       od ;",
				"    fi ;",
				"end: # _R. J. Mathar_, Oct 19 2006"
			],
			"mathematica": [
				"r[x_]:=RealDigits[1/x]; w[x_]:=First[r[x]]; f[x_]:=First[w[x]]; l[x_]:=Last[w[x]]; z[x_]:=Last[r[x]];",
				"d[x_] := Which[IntegerQ[l[x]], 0, IntegerQ[f[x]]==False, Length[f[x]], True, Length[l[x]]]; Table[d[i], {i,1,90}] (* _Hans Havermann_, Oct 19 2006 *)",
				"fd[n_] := Block[{q},q = Last[First[RealDigits[1/n]]];If[IntegerQ[q], q = {}]; Length[q]];Table[fd[n], {n, 100}] (* _Ray Chandler_, Dec 06 2006 *)",
				"Table[Length[RealDigits[1/n][[1,-1]]],{n,90}] (* _Harvey P. Dale_, Jul 03 2011 *)"
			],
			"program": [
				"(PARI) A051626(n)=if(1\u003cn/=5^valuation(n,5)\u003c\u003cvaluation(n,2),znorder(Mod(10,n)),0) \\\\ _M. F. Hasler_, Dec 14 2015",
				"(Python)",
				"def A051626(n):",
				"    if isA003592(n):",
				"        return 0",
				"    else:",
				"        lpow=1",
				"        while True:",
				"            for mpow in range(lpow-1,-1,-1):",
				"                if (10**lpow-10**mpow) % n == 0:",
				"                    return lpow-mpow",
				"            lpow += 1 # _Kenneth Myers_, May 06 2016"
			],
			"xref": [
				"Essentially same as A007732. Cf. A002371, A048595, A006883, A036275, A114205, A114206, A001913."
			],
			"keyword": "nonn,base,easy",
			"offset": "1,7",
			"author": "_J. Lowell_",
			"ext": [
				"More terms from _James A. Sellers_"
			],
			"references": 33,
			"revision": 76,
			"time": "2020-05-14T05:09:54-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}