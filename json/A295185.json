{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A295185",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 295185,
			"data": "6,10,28,22,52,34,76,184,58,248,148,82,172,376,424,118,488,268,142,584,316,664,1335,388,202,412,214,436,3729,508,1048,274,2919,298,1208,1256,652,1336,1384,358,3801,382,772,394,6501,7385,892,454,916,1864,478,5061,2008,2056,2104,538,2168,1108,562,5943,9669",
			"name": "a(n) is the smallest composite number whose prime divisors (with multiplicity) sum to prime(n); n \u003e= 3.",
			"comment": [
				"Sequence is undefined for n=1,2 since no composites exist whose prime divisors sum to 2, 3. For n \u003e= 3, a(n) = A288814(prime(n)) = prime(n-k)*B(prime(n) - prime(n-k)) where B=A056240, and k \u003e= 1 is the \"type\" of prime(n), indicated as prime(n)~k(g1,g2,...,gk) where gi = prime(n-(i-1)) - prime(n-i); 1 \u003c= i \u003c= k. Thus: 5~1(2), 211~2(12,2), 4327~3(30,8,6) etc. The sequence relates to gaps between odd primes, and in particular to the sequence of k prime gaps below prime(n). The even-indexed terms of B are relevant, as are those of subsequences:",
				"  C=A288313, 2,4 plus terms B(n) where n-3 is prime (A298252),",
				"  D=A297150, terms B(n) where n-5 is prime and n-3 is composite (A297925) and",
				"  E=A298615, terms B(n) where both n-3 and n-5 are composite (A298366).",
				"  The above sequences of indices 2m form a partition of the even numbers and the corresponding terms B(2m) form a partition of the even-indexed terms of A056240. The union of D and E is the sequence A292081 = B-C.",
				"Let g(n,t) = prime(n) - prime(n-t), t \u003c n, and h(n,t) = g(n,t) - g(n,1), 1 \u003c t \u003c n. If g1=g(n,1) is a term in A298252 (g1-3 is prime), then B(g1) is a term in C, so k=1. If g1 belongs to A297925 or A298366 then B(g1) is a term in D or E and the value of k depends on subsequent gaps below prime(n), within a range dependent on g1.",
				"Let range R1(g1) = u - g(n,1) where u is the index in B of the greatest term in C such that C(u) \u003c B(g1). Let range R2(g1) = v-g(n,1) where v is the index in B of the greatest term in D such that D(v) \u003c= B(g1). For all n, R2 \u003c R1, and if g1 is a term in D then R2(g1)=0. Examples: R1(12)=2, R2(12)=0, R1(30)=26, R2(30)=6.",
				"k \u003e= 1 is the smallest integer such that B(g(n,k)) \u003c= B(g(n,t)) for all t satisfying g1 \u003c= g(n,t) \u003c= g1 + R1(g1). For g1-3 prime, k=1. If g1-3 is composite, let z be least integer \u003e 1 such that g(n,z)-3 is prime, and let w be least integer \u003e= 1 such that g(n,w)-5 is prime. Then z \"complies\" if h(n,z) \u003c= R1, and w \"complies\" if h(n,w) \u003c= R2. If g1-5 is prime then R2=w=0 and only z is relevant.",
				"B(g1) must belong to C,D or E. If in C (g1-3 is prime) then k=1. If in D (g1-5 is prime), k=z if z complies, otherwise k=1. If B(g1) is in E and z complies but not w then k=z, or if w complies but not z then k=w. If B(g1) is in E and z,w both comply then k=z if 3*(g(n,z)-3) \u003c 5*(g(n,w)-5), otherwise k=w. If neither z nor w comply, then k=1.",
				"Conjecture: For all n \u003e= 3, a(n) \u003e= A288189(n)."
			],
			"link": [
				"Giovanni Resta, \u003ca href=\"/A295185/b295185.txt\"\u003eTable of n, a(n) for n = 3..10000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A288814(prime(n)) = prime(n-k)*A056240(prime(n) - prime(n-k)) for some k \u003e= 1 and prime(n-k) = gpf(A288814(prime(n)).",
				"a(n) \u003e= A288189(n)."
			],
			"example": [
				"5=prime(3), g(3,1)=5-3=2, a term in C; k=1, and a(3)=3*B(5-3)=3*2=6; 5~1(2).",
				"17=prime(7), g(7,1)=17-13=4, a term in C; k=1, a(7)=13*B(17-13)=13*4=52; 17~1(4).",
				"211=prime(47); g(47,1)=12, a term in D, R1=2, R2=0, k=z=2, a(47)=197*b(211-197)=197*33=6501; 211~2(12,2), and 211 is first prime of type k=2.",
				"8923=prime(1109); g(1109,1)=30, a term in E. R1=26, R2=6, z=3 and w=2 both comply  but 3*(g(n,3)-3)=159 \u003e 5*(g(n,2)-5)=155, so k=w=2. Therefore a(1109)=8887*b(8923-8887)=8887*b(36)=8887*155=1377485; 8923~2(30,6).",
				"40343=prime(4232); g(4232,1)=54, a term in E. R1=58, R2=12,z=6 and w=3, both comply, 3*(g(n,z)-3)=309 and 5*(g(n,w)-5)=305 therefore k=w=3 and a(4232) = 40277*b(40343-40277)=40277*b(66)=40277*305=12284485; 40343~3(54,6,6).",
				"81611=prime(7981); g(81611,1)=42, a term in D, R1=22, R2=0; z complies, k=z=6, a(7981)=81547*b(81611-81547)=81546*b(64)=81546*183=14923101; 81611~6(42,6,4,6,2,4) and is the first prime of type k=6.",
				"If p is the greater of twin/cousin primes then p~1(2), p~1(4), respectively."
			],
			"mathematica": [
				"b[n_] := b[n] = Total[Times @@@ FactorInteger[n]];",
				"a[n_] := For[k = 2, True, k++, If[CompositeQ[k], If[b[k] == Prime[n], Return[k]]]];",
				"Table[a[n], {n, 3, 63}] (* _Jean-François Alcover_, Feb 23 2018 *)"
			],
			"program": [
				"(PARI) a(n) = { my(p=prime(n)); forcomposite(x=6, , my(f=factor(x)); if(f[, 1]~*f[, 2]==p, return(x))); } \\\\ _Iain Fox_, Dec 08 2017"
			],
			"xref": [
				"Cf. A000040, A056240, A288814, A292081, A289993, A288313, A297150, A298615, A298252, A297925, A298366, A288189."
			],
			"keyword": "nonn",
			"offset": "3,1",
			"author": "_David James Sycamore_, Nov 16 2017",
			"references": 11,
			"revision": 33,
			"time": "2019-12-10T00:17:54-05:00",
			"created": "2018-02-17T12:21:22-05:00"
		}
	]
}