{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000289",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 289,
			"id": "M3316 N1333",
			"data": "1,4,7,31,871,756031,571580604871,326704387862983487112031,106735757048926752040856495274871386126283608871,11392521832807516835658052968328096177131218666695418950023483907701862019030266123104859068031",
			"name": "A nonlinear recurrence: a(n) = a(n-1)^2 - 3*a(n-1) + 3 (for n\u003e1).",
			"comment": [
				"An infinite coprime sequence defined by recursion. - _Michael Somos_, Mar 14 2004",
				"This is the special case k=3 of sequences with exact mutual k-residues. In general, a(1)=k+1 and a(n)=min{m | m\u003ea(n-1), mod(m,a(i))=k, i=1,...,n-1}. k=1 gives Sylvester's sequence A000058 and k=2 Fermat sequence A000215. - _Seppo Mustonen_, Sep 04 2005"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"John Cerkan, \u003ca href=\"/A000289/b000289.txt\"\u003eTable of n, a(n) for n = 0..12\u003c/a\u003e",
				"A. V. Aho and N. J. A. Sloane, \u003ca href=\"https://www.fq.math.ca/Scanned/11-4/aho-a.pdf\"\u003eSome doubly exponential sequences\u003c/a\u003e, Fibonacci Quarterly, Vol. 11, No. 4 (1973), pp. 429-437, \u003ca href=\"http://neilsloane.com/doc/doubly.html\"\u003ealternative link\u003c/a\u003e.",
				"S. W. Golomb, \u003ca href=\"http://www.jstor.org/stable/2311857\"\u003eOn certain nonlinear recurring sequences\u003c/a\u003e, Amer. Math. Monthly 70 (1963), 403-405.",
				"R. Mestrovic, \u003ca href=\"http://arxiv.org/abs/1202.3670\"\u003eEuclid's theorem on the infinitude of primes: a historical survey of its proofs (300 BC--2012) and another new proof\u003c/a\u003e, arXiv preprint arXiv:1202.3670 [math.HO], 2012 - From N. J. A. Sloane, Jun 13 2012",
				"S. Mustonen, \u003ca href=\"http://www.survo.fi/papers/resseq.pdf\"\u003eOn integer sequences with mutual k-residues\u003c/a\u003e",
				"Seppo Mustonen, \u003ca href=\"/A000215/a000215.pdf\"\u003eOn integer sequences with mutual k-residues\u003c/a\u003e [Local copy]",
				"\u003ca href=\"/index/Aa#AHSL\"\u003eIndex entries for sequences of form a(n+1)=a(n)^2 + ...\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A005267(n) + 2 (for n\u003e0).",
				"a(n) = ceiling(c^(2^n)) + 1 where c = A077141. - _Benoit Cloitre_, Nov 29 2002",
				"For n\u003e0, a(n) = 3 + Product_{i=0..n-1} a(i). - _Vladimir Shevelev_, Dec 08 2010"
			],
			"mathematica": [
				"Join[{1}, RecurrenceTable[{a[n] == a[n-1]^2 - 3*a[n-1] + 3, a[1] == 4}, a, {n, 1, 9}]] (* _Jean-François Alcover_, Feb 06 2016 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c2,max(0,1+3*n),a(n-1)^2-3*a(n-1)+3)"
			],
			"xref": [
				"Cf. A000058."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 11,
			"revision": 53,
			"time": "2021-04-07T02:49:57-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}