{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A338757",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 338757,
			"data": "0,1,1,1,1,0,1,2,1,0,1,0,1,0,0,2,1,0,1,0,0,0,1,0,1,0,1,0,1,0,1,5,0,0,0,0,1,0,0,0,1,0,1,0,0,0,1,1,1,0,0,0,1,0,0,0,0,0,1,1,1,0,0,19,0,0,1,0,0,0,1,0,1,0,0,0,0,0,1,0,2,0,1,0,0,0,0,0,1",
			"name": "Number of splitting-simple groups of order n; number of nontrivial groups of order n that are not semidirect products of proper subgroups.",
			"comment": [
				"The other names for groups of this kind include \"semidirectly indecomposable groups\" or \"inseparable groups\". Note that the following are equivalent definitions for a nontrivial group to be a splitting-simple group:",
				"- It is not the (internal) semidirect product of proper subgroups;",
				"- It is not isomorphic to the (external) semidirect product of nontrivial groups;",
				"- It has no proper nontrivial normal subgroups with a permutable complement.",
				"- It is the non-split extension of every proper nontrivial normal subgroup by the corresponding quotient group.",
				"Also note that being simple is a stronger condition than being splitting-simple, while being directly indecomposable (see A090751) is weaker.",
				"a(p^e) \u003e= 1 since C_p^e cannot be written as the semidirect product of proper subgroups. For e \u003e= 3, a(2^e) \u003e= 2 by the existence of the generalized quaternion group of order 2^e, which is the only non-split extension of C_2^(e-1) by C_2 other than C_2^e.",
				"The smallest numbers here with a(n) \u003e 0 that are not prime powers are 48, 60, 120, 144, 168, 192, 240, 320, 336, 360 and so on. Are there any odd numbers n that are not prime powers satisfying a(n) \u003e 0 ?",
				"Conjecture: a(n) = 0 for squarefree n which is not a prime."
			],
			"link": [
				"Jianing Song, \u003ca href=\"/A338757/b338757.txt\"\u003eTable of n, a(n) for n = 1..255\u003c/a\u003e",
				"Tim Dokchitser, \u003ca href=\"https://people.maths.bris.ac.uk/~matyd/GroupNames/extensions.html\"\u003eGroup extensions\u003c/a\u003e",
				"The Group Properties Wiki, \u003ca href=\"https://groupprops.subwiki.org/wiki/Splitting-simple_group\"\u003eSplitting-simple group\u003c/a\u003e",
				"The Group Properties Wiki, \u003ca href=\"https://groupprops.subwiki.org/wiki/Permutable_complements\"\u003ePermutable complements\u003c/a\u003e",
				"Jianing Song, \u003ca href=\"/A338757/a338757_1.txt\"\u003eList of splitting-simple groups of order up to 255 by ID in GAP's SmallGroup library\u003c/a\u003e",
				"\u003ca href=\"/index/Gre#groups\"\u003eIndex entries for sequences related to groups\u003c/a\u003e"
			],
			"formula": [
				"For primes p != q:",
				"a(p) = a(p^2) = 1; a(p^3) = 2 for p = 2, 1 otherwise;",
				"a(p^4) = 2 for p = 2 or 3, 1 otherwise;",
				"a(pq) = 0;",
				"a(4p) = a(8p) = 0, p \u003e 2.",
				"a(n) \u003c= A090751(n) for all n, and the equality holds if n = 1, p, p^2 for primes p or n = pq for primes p \u003c q and p does not divide q-1.",
				"a(A001034(k)) \u003e= 1, since A001034 lists the orders of (non-Abelian) simple groups."
			],
			"example": [
				"a(48) = 1 because the binary octahedral group, which is of order 48, cannot be written as the semidirect product of proper subgroups.",
				"a(16) = 2, and the corresponding groups are C_16 and Q_16 (generalized quaternion group of order 16).",
				"a(81) = 2, and the corresponding groups are C_81 and SmallGroup(81,10).",
				"a(64) = 19, and the corresponding groups are SmallGroup(64,i) for i = 1, 11, 13, 14, 19, 22, 37, 43, 45, 49, 54, 79, 81, 82, 160, 168, 172, 180 and 245.",
				"For n = 60 or 168, the unique simple group is the only group of order n that cannot be written as the semidirect product of proper subgroups, hence a(60) = a(168) = 1. [The unique simple groups are respectively Alt(5) and PSL(2,7). - _Bernard Schott_, Nov 08 2020]",
				"For n = 12, we have C_12 = C_3 X C_4, C_6 X C_2 = C_6 X C_2, D_6 = C_6 : C_2, Dic_12 = C_3 : C_4 and A_4 = (C_2 X C_2) : C_3, all of which can be written as the semidirect product of nontrivial groups. So a(12) = 0."
			],
			"program": [
				"(GAP)",
				"IsSplittingSimple := function(G)",
				"  local c, l, i;",
				"  c := NormalSubgroups(G);",
				"  l := Length(c);",
				"  if l \u003e 1 then",
				"    for i in [2..l-1] do",
				"    if Length(ComplementClassesRepresentatives(G,c[i])) \u003e 0 then",
				"      return false;",
				"    fi;",
				"    od;",
				"    return true;",
				"  else",
				"    return false;",
				"  fi;",
				"end;",
				"A338757 := n -\u003e Length(AllSmallGroups( n, IsSplittingSimple ));"
			],
			"xref": [
				"Cf. A000001, A090751 (number of directly indecomposable groups of order n), A001034."
			],
			"keyword": "nonn,hard",
			"offset": "1,8",
			"author": "_Jianing Song_, Nov 07 2020",
			"references": 2,
			"revision": 37,
			"time": "2021-04-26T06:28:27-04:00",
			"created": "2020-11-07T11:39:38-05:00"
		}
	]
}