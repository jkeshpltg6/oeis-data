{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000608",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 608,
			"id": "M2864 N1152",
			"data": "1,1,1,3,10,44,238,1650,14512,163341,2360719,43944974,1055019099,32664984238,1303143553205,66900392672168,4413439778321689",
			"name": "Number of connected partially ordered sets with n unlabeled elements.",
			"reference": [
				"K. K.-H. Butler and G. Markowsky, Enumeration of finite topologies, Proc. 4th S-E Conf. Combin., Graph Theory, Computing, Congress. Numer. 8 (1973), 169-184.",
				"E. N. Gilbert, A catalog of partially ordered systems, unpublished memorandum, Aug 08, 1961.",
				"G. Melançon, personal communication.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Gunnar Brinkmann and Brendan D. McKay, \u003ca href=\"http://users.cecs.anu.edu.au/~bdm/papers/topologies.pdf\"\u003eCounting unlabeled topologies and transitive relations\u003c/a\u003e.",
				"G. Brinkmann and B. D. McKay, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL8/McKay/mckay170.html\"\u003eCounting unlabeled topologies and transitive relations\u003c/a\u003e, J. Integer Sequences, Volume 8, 2005.",
				"G. Brinkmann and B. D. McKay, \u003ca href=\"http://users.cecs.anu.edu.au/~bdm/papers/posets.pdf\"\u003ePosets on up to 16 Points\u003c/a\u003e [On Brendan McKay's home page]",
				"G. Brinkmann and B. D. McKay, \u003ca href=\"http://dx.doi.org/10.1023/A:1016543307592\"\u003ePosets on up to 16 Points\u003c/a\u003e, Order 19 (2) (2002) 147-179.",
				"K. K.-H. Butler and G. Markowsky, \u003ca href=\"http://www.laptop.maine.edu/Enumeration.pdf\"\u003eEnumeration of finite topologies\u003c/a\u003e, Proc. 4th S-E Conf. Combin., Graph Theory, Computing, Congress. Numer. 8 (1973), 169-184.",
				"K. K.-H. Butler and G. Markowsky, \u003ca href=\"/A000798/a000798_1.pdf\"\u003eEnumeration of finite topologies\u003c/a\u003e, Proc. 4th S-E Conf. Combin., Graph Theory, Computing, Congress. Numer. 8 (1973), 169-184. [Annotated scan of pages 180 and 183 only]",
				"P. J. Cameron, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL3/groups.html\"\u003eSequences realized by oligomorphic permutation groups\u003c/a\u003e, J. Integ. Seqs. Vol. 3 (2000), #00.1.5.",
				"E. N. Gilbert, \u003ca href=\"/A000798/a000798_9.pdf\"\u003eA catalog of partially ordered systems\u003c/a\u003e, unpublished memorandum, Aug 08, 1961. [Annotated scanned copy]",
				"Henry Sharp, Jr., \u003ca href=\"/A001930/a001930_1.pdf\"\u003eQuasi-orderings and topologies on finite sets\u003c/a\u003e, Proceedings of the American Mathematical Society 17.6 (1966): 1344-1349. [Annotated scanned copy]",
				"N. J. A. Sloane, \u003ca href=\"/A000112/a000112_2.pdf\"\u003eList of sequences related to partial orders, circa 1972\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/transforms.txt\"\u003eTransforms\u003c/a\u003e",
				"Peter Steinbach, \u003ca href=\"/A000664/a000664_10.pdf\"\u003eField Guide to Simple Graphs, Volume 4\u003c/a\u003e, Part 10 (For Volumes 1, 2, 3, 4 of this book see A000088, A008406, A000055, A000664, respectively.)",
				"N. L. White, \u003ca href=\"/A000798/a000798_6.pdf\"\u003eTwo letters to N. J. A. Sloane, 1970, with hand-drawn enclosure\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://commons.wikimedia.org/wiki/File:Hasse4Conn.jpg\"\u003eIllustration n=4\u003c/a\u003e, \u003ca href=\"https://commons.wikimedia.org/wiki/File:Hasse5Conn.jpg\"\u003eIllustration n=5\u003c/a\u003e, \u003ca href=\"https://commons.wikimedia.org/wiki/File:Hasse6Conn.jpg\"\u003eIllustration n=6\"\u003c/a\u003e (2021)",
				"J. A. Wright, \u003ca href=\"/A000798/a000798_3.pdf\"\u003eThere are 718 6-point topologies, quasiorderings and transgraphs\u003c/a\u003e, Preprint, 1970. [Annotated scanned copy]",
				"J. A. Wright, \u003ca href=\"https://www.ams.org/journals/notices/197006/197006FullIssue.pdf\"\u003eThere are 718 6-point topologies, quasi-orderings and transgraphs\u003c/a\u003e, Notices Amer. Math. Soc., 17 (1970), p. 646, Abstract #70T-A106.",
				"J. A. Wright, \u003ca href=\"/A000798/a000798_5.pdf\"\u003eTwo related abstracts, 1970 and 1972\u003c/a\u003e [Annotated scanned copies]",
				"J. A. Wright, \u003ca href=\"/A000798/a000798_4.pdf\"\u003eLetter to N. J. A. Sloane, Apr 06 1972, listing 18 sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Pos#posets\"\u003eIndex entries for sequences related to posets\u003c/a\u003e"
			],
			"mathematica": [
				"A000112 = Cases[Import[\"https://oeis.org/A000112/b000112.txt\", \"Table\"], {_, _}][[All, 2]];",
				"(* EulerInvTransform is defined in A022562 *)",
				"{1} ~Join~ EulerInvTransform[Rest[A000112]] (* Jean-François Alcover, Dec 04 2019, updated Mar 17 2020 *)"
			],
			"xref": [
				"Inverse Euler transform of A000112.",
				"Cf. A263864 (multiset transform), A342500 (refined by rank)."
			],
			"keyword": "hard,more,nonn,nice",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_, _Simon Plouffe_, J. A. Wright",
			"ext": [
				"More terms from _Christian G. Bower_, who pointed out connection with A000112, Jan 21 1998 and Dec 12 2001",
				"More terms from _Vladeta Jovovic_, Jan 04 2006; corrected Jan 15 2006"
			],
			"references": 10,
			"revision": 81,
			"time": "2021-03-24T08:24:09-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}