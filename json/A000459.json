{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000459",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 459,
			"id": "M4750 N2032",
			"data": "1,0,1,10,297,13756,925705,85394646,10351036465,1596005408152,305104214112561,70830194649795010,19629681235869138841,6401745422388206166420,2427004973632598297444857,1058435896607583305978409166,526149167104704966948064477665",
			"name": "Number of multiset permutations of {1, 1, 2, 2, ..., n, n} with no fixed points.",
			"comment": [
				"Original definition: Number of permutations with no hits on 2 main diagonals. (Identical to definition of A000316.) - _M. F. Hasler_, Sep 27 2015",
				"Card-matching numbers (Dinner-Diner matching numbers): A deck has n kinds of cards, 2 of each kind. The deck is shuffled and dealt in to n hands with 2 cards each. A match occurs for every card in the j-th hand of kind j. A(n) is the number of ways of achieving no matches. The probability of no matches is A(n)/((2n)!/2!^n).",
				"Also, Penrice's Christmas gift numbers (see Penrice 1991).",
				"a(n) is the maximal number of totally mixed Nash equilibria in games of n players, each with 3 pure options. - _Raimundas Vidunas_, Jan 22 2014"
			],
			"reference": [
				"F. N. David and D. E. Barton, Combinatorial Chance, Hafner, NY, 1962, Ch. 7 and Ch. 12.",
				"J. Riordan, An Introduction to Combinatorial Analysis, Wiley, 1958, pp. 174-178.",
				"R. P. Stanley, Enumerative Combinatorics Volume I, Cambridge University Press, 1997, p. 71.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A000459/b000459.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e",
				"Per Alexandersson and Frether Getachew, \u003ca href=\"https://arxiv.org/abs/2105.08455\"\u003eAn involution on derangements\u003c/a\u003e, arXiv:2105.08455 [math.CO], 2021.",
				"Shalosh B. Ekhad, Christoph Koutschan, and Doron Zeilberger, \u003ca href=\"https://arxiv.org/abs/2101.10147\"\u003eThere are EXACTLY 1493804444499093354916284290188948031229880469556 Ways to Derange a Standard Deck of Cards (ignoring suits) [and many other such useful facts]\u003c/a\u003e, arXiv:2101.10147 [math.CO], 2021.",
				"F. F. Knudsen and I. Skau, \u003ca href=\"http://www.jstor.org/stable/2691467\"\u003eOn the Asymptotic Solution of a Card-Matching Problem\u003c/a\u003e, Mathematics Magazine 69 (1996), 190-197.",
				"P. A. MacMahon, \u003ca href=\"https://openlibrary.org/books/OL23289465M/Combinatory_analysis\"\u003eCombinatory Analysis\u003c/a\u003e Cambridge: The University Press 1915-1916",
				"B. H. Margolius, \u003ca href=\"http://www.jstor.org/stable/3219303\"\u003eThe Dinner-Diner Matching Problem\u003c/a\u003e, Mathematics Magazine, 76 (2003), 107-118.",
				"B. H. Margolius, \u003ca href=\"http://academic.csuohio.edu/bmargolius/homepage/dinner/dinner/cardentry.htm\"\u003eDinner-Diner Matching Probabilities\u003c/a\u003e",
				"R. D. McKelvey and A. McLennan, \u003ca href=\"http://dx.doi.org/10.1006/jeth.1996.2214\"\u003eThe maximal number of regular totally mixed Nash equilibria\u003c/a\u003e, J. Economic Theory, 72:411--425, 1997.",
				"L. I. Nicolaescu, \u003ca href=\"http://nyjm.albany.edu/j/2004/10-7.pdf\"\u003eDerangements and asymptotics of the Laplace transforms of large powers of a polynomial\u003c/a\u003e, New York J. Math. 10 (2004) 117-131.",
				"S. G. Penrice, \u003ca href=\"https://www.jstor.org/stable/2324927\"\u003eDerangements, permanents and Christmas presents\u003c/a\u003e, The American Mathematical Monthly 98 (1991), 617-620.",
				"John Riordan and N. J. A. Sloane, \u003ca href=\"/A003471/a003471_1.pdf\"\u003eCorrespondence, 1974\u003c/a\u003e",
				"R. Vidunas, \u003ca href=\"http://arxiv.org/abs/1401.5400\"\u003eCounting derangements and Nash equilibria\u003c/a\u003e, arXiv preprint arXiv:1401.5400 [math.CO], 2014-2016.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Permutation#Permutations_of_multisets\"\u003ePermutations of multisets\u003c/a\u003e",
				"\u003ca href=\"/index/Ca#cardmatch\"\u003eIndex entries for sequences related to card matching\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A000316(n)/2^n.",
				"a(n) = Sum_{k=0..n} Sum_{m=0..n-k} (-1)^k * n!/(k!*m!*(n-k-m)!) * 2^(2*k+m-n) * (2*n-2*m-k)!. - _Max Alekseyev_, Oct 06 2016",
				"G.f.: Sum_{j=0..n*k} coeff(R(x, n, k), x, j)*(t-1)^j*(n*k-j)! where n is the number of kinds of cards, k is the number of cards of each kind (2 in this case) and coeff(R(x, n, k), x, j) is the coefficient of x^j of the rook polynomial R(x, n, k) = (k!^2*sum(x^j/((k-j)!^2*j!))^n (see Riordan or Stanley).",
				"D-finite with recurrence a(n) = n*(2*n-1)*a(n-1)+2*n*(n-1)*a(n-2)-(2*n-1), a(1) = 0, a(2) = 1.",
				"a(n) = round(2^(n/2 + 3/4)*Pi^(-1/2)*exp(-2)*n!*BesselK(1/2+n,2^(1/2))). - _Mark van Hoeij_, Oct 30 2011",
				"(2*n+3)*a(n+3)=(2*n+5)^2*(n+2)*a(n+2)+(2*n+3)*(n+2)*a(n+1)-2*(2*n+5)*(n+1)*(n+2)*a(n). - _Vaclav Kotesovec_, Aug 31 2012",
				"Asymptotic: a(n) ~ n^(2*n)*2^(n+1)*sqrt(Pi*n)/exp(2*n+2), _Vaclav Kotesovec_, Aug 31 2012",
				"a(n) = (1/2^n)*A000316(n) = int_{0..inf} exp(-x)*(1/2*x^2 - 2*x + 1)^n dx. Asymptotic: a(n) ~ ((2*n)!/2^n)*exp(-2)*( 1 - 1/(2*n) - 23/(96*n^2) + O(1/n^3) ). See Nicolaescu. - _Peter Bala_, Jul 07 2014",
				"Let S = x_1 + ... + x_n. a(n) equals the coefficient of (x_1*...*x_n)^2 in the expansion of product {i = 1..n} (S - x_i)^2 (MacMahon, Chapter III). - _Peter Bala_, Jul 08 2014"
			],
			"example": [
				"There are 297 ways of achieving zero matches when there are 2 cards of each kind and 4 kinds of card so a(4)=297.",
				"From _Peter Bala_, Jul 08 2014: (Start)",
				"a(3) = 10: the 10 permutations of the multiset {1,1,2,2,3,3} that have no fixed points are",
				"{2,2,3,3,1,1}, {3,3,1,1,2,2}",
				"{2,3,1,3,1,2}, {2,3,1,3,2,1}",
				"{2,3,3,1,1,2}, {2,3,3,1,2,1}",
				"{3,2,1,3,1,2}, {3,2,1,3,2,1}",
				"{3,2,3,1,1,2}, {3,2,3,1,2,1}",
				"(End)"
			],
			"maple": [
				"p := (x,k)-\u003ek!^2*sum(x^j/((k-j)!^2*j!),j=0..k); R := (x,n,k)-\u003ep(x,k)^n; f := (t,n,k)-\u003esum(coeff(R(x,n,k),x,j)*(t-1)^j*(n*k-j)!,j=0..n*k); seq(f(0,n,2)/2!^n,n=0..18);"
			],
			"mathematica": [
				"RecurrenceTable[{(2*n+3)*a[n+3]==(2*n+5)^2*(n+2)*a[n+2]+(2*n+3)*(n+2)*a[n+1]-2*(2*n+5)*(n+1)*(n+2)*a[n],a[1]==0,a[2]==1,a[3]==10},a,{n,1,25}] (* _Vaclav Kotesovec_, Aug 31 2012 *)",
				"a[n_] := a[n] = n*(2*n-1)*a[n-1] + 2*n*(n-1)*a[n-2] - (2*n-1); a[0] = 1; a[1] = 0; a[2] = 1; Table[a[n], {n, 0, 14}] (* _Jean-François Alcover_, Mar 04 2013 *)"
			],
			"program": [
				"(MAGMA) I:=[0,1]; [n le 2 select I[n] else n*(2*n-1)*Self(n-1)+2*n*(n-1)*Self(n-2)-(2*n-1): n in [1..30]]; // _Vincenzo Librandi_, Sep 28 2015",
				"(PARI) a(n) = (2^n*round(2^(n/2+3/4)*Pi^(-1/2)*exp(-2)*n!*besselk(1/2+n,2^(1/2))))/2^n;",
				"vector(15, n, a(n))\\\\ _Altug Alkan_, Sep 28 2015",
				"(PARI) { A000459(n) = sum(m=0,n, sum(k=0,n-m, (-1)^k * binomial(n,k) * binomial(n-k,m) * 2^(2*k+m-n) * (2*n-2*m-k)! )); } \\\\ _Max Alekseyev_, Oct 06 2016"
			],
			"xref": [
				"Cf. A000166, A008290, A059056-A059071, A033581, A059073."
			],
			"keyword": "nonn,nice,easy",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms and edited by Barbara Haas Margolius (margolius(AT)math.csuohio.edu), Dec 22 2000",
				"Edited by _M. F. Hasler_, Sep 27 2015",
				"a(0)=1 prepended by _Max Alekseyev_, Oct 06 2016"
			],
			"references": 14,
			"revision": 102,
			"time": "2021-05-20T10:56:44-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}