{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A271230",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 271230,
			"data": "0,1,-2,0,-4,-2,2,4,8,6,-8,6,-6,-4,0,-2,-4,-2,4,-8,10,8,4,-6,2,-18,-16,12,-2,18,8,4,-6,12,14,16,-2,-12,-24,6,-12,6,0,2,-18,-16,20,8,-12,22,10,16,18,-20,2,8,-10,-8,-26,26",
			"name": "P-defects p - N(p) of the congruence y^2 == x^3 + x^2 + x (mod p) for primes p, where N(p) is the number of solutions.",
			"comment": [
				"The modularity pattern series is the expansion of the (corrected) Nr. 54 modular cusp form of weight 2 and level N=48 given in the table 1 of the Martin reference, i.e., (eta(4*z) * eta(12*z)^4 / (eta(2*z) * eta(6*z) * eta(8*z) * eta(24*z)) in powers of q = exp(2*Pi*i*z), with Im(z) \u003e 0, where i is the imaginary unit. Here eta(z) = q^{1/24}*Product_{n\u003e=1} (1-q^n) is the Dedekind eta function. See A271231 for this expansion. Note that also for the possibly bad prime 2 and the bad prime 3 (the discriminant of this elliptic curve is -3) this expansion gives the correct p-defect.",
				"The identical p-defects occur for the elliptic curve y^2 = x^3 + x^2 - 4*x - 4 taken modulo prime(n). See the Martin and Ono reference, p. 3173, row Conductor 48, and A271231 (checked up to prime(100) = 541). - _Wolfdieter Lang_, Apr 21 2016"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A271230/b271230.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Y. Martin, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-96-01743-6\"\u003eMultiplicative eta-quotients\u003c/a\u003e, Trans. Amer. Math. Soc. 348 (1996), no. 12, 4825-4856, see page 4852 Table I.",
				"Yves Martin and Ken Ono, \u003ca href=\"http://www.ams.org/journals/proc/1997-125-11/S0002-9939-97-03928-2/\"\u003eEta-Quotients and Elliptic Curves\u003c/a\u003e, Proc. Amer. Math. Soc. 125, No 11 (1997), 3169-3176.",
				"Michael Somos, \u003ca href=\"/A030203/a030203.txt\"\u003eIndex to Yves Martin's list of 74 multiplicative eta-quotients and their A-numbers\u003c/a\u003e",
				"Haode Yan, Yongbo Xia, Chunlei Li, Tor Helleseth, Maosheng Xiong and Jinquan Luo, \u003ca href=\"https://arxiv.org/abs/2108.03088\"\u003eThe Differential Spectrum of the Power Mapping x^p^(n-3))\u003c/a\u003e, arXiv:2108.03088 [cs.IT], 2021. See Table II p. 7."
			],
			"formula": [
				"a(n) = prime(n) - A271229(n), n \u003e= 1, where A271229(n) is the number of solutions of the congruence y^2 == x^3 + x^2 + x (mod prime(n)).",
				"a(n) = A271231(prime(n)), n \u003e=1."
			],
			"example": [
				"See the example section of A271229.",
				"n = 3, prime(3) = 5, A271229(5) = 7, a(3) = 5 - 7 = -2."
			],
			"xref": [
				"Cf. A159819, A271229, A271231."
			],
			"keyword": "sign,easy",
			"offset": "1,3",
			"author": "_Wolfdieter Lang_, Apr 18 2016",
			"references": 5,
			"revision": 23,
			"time": "2021-08-09T12:24:59-04:00",
			"created": "2016-04-19T21:30:28-04:00"
		}
	]
}