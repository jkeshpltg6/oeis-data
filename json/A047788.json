{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A047788",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 47788,
			"data": "1,1,1,7,809,1847,55601,6921461,126235201,8806171927,2288629046003,80348736972167,10111159088668001,40453941942593304589,258227002122139705201,51215766794507248883047,34747165199239302488636803,2962605017328303351107945687",
			"name": "Numerators of Glaisher's I-numbers.",
			"comment": [
				"Conjecture: L(2n+1, chi3) = a(n)/A047789(n) * (2*Pi)^(2n+1)/((2n)!*3^(2n+3/2)), where L(s, chi3) = Sum_{k\u003e=1} Legendre(k,3)/k^s = Sum_{k\u003e=1} A102283(k)/k^s is the Dirichlet L-function for the non-principal character modulo 3. - _Jianing Song_, Nov 17 2019"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A047788/b047788.txt\"\u003eTable of n, a(n) for n = 0..255\u003c/a\u003e",
				"J. W. L. Glaisher, \u003ca href=\"https://doi.org/10.1112/plms/s1-31.1.216\"\u003eOn a set of coefficients analogous to the Eulerian numbers\u003c/a\u003e, Proc. London Math. Soc., 31 (1899), 216-235.",
				"\u003ca href=\"/index/Ge#Glaisher\"\u003eIndex entries for sequences related to Glaisher's numbers\u003c/a\u003e"
			],
			"formula": [
				"E.g.f. for (-1)^n*I(n) is (3/2)/(1 + 2*cosh(x))."
			],
			"example": [
				"1/2, 1/3, 1, 7, 809/9, 1847, 55601, 6921461/3, ..."
			],
			"maple": [
				"S:= series(3/(2+4*cos(x)),x,101):",
				"seq(numer(coeff(S,x,2*j)*(2*j)!),j=0..50); # _Robert Israel_, Aug 14 2018"
			],
			"mathematica": [
				"terms = 20; CoefficientList[(3/2)/(1+Exp[x]+Exp[-x]) + O[x]^(2terms), x]* Range[0, 2terms-2]! // Abs // Numerator // DeleteCases[#, 0]\u0026 (* _Jean-François Alcover_, Feb 28 2019 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c1,(n==0),n*=2;numerator(n!* polcoeff(3/(2+4*cos(x+O(x^n) )), n))) /* _Michael Somos_, Feb 26 2004 */",
				"(MAGMA) m:=60; R\u003cx\u003e:=PowerSeriesRing(Rationals(), m); b:=Coefficients(R!( 3/(2*(1+2*Cosh(x))) )); [Numerator((-1)^(n+1)*Factorial(2*n-2)* b[2*n-1]): n in [1..Floor((m-2)/2)]]; // _G. C. Greubel_, May 17 2019",
				"(Sage) [numerator( (-1)^n*factorial(2*n)*( 3/(2*(1+2*cosh(x))) ).series(x, 2*n+2).list()[2*n]) for n in (0..30)] # _G. C. Greubel_, May 17 2019"
			],
			"xref": [
				"Cf. A047789, A002111.",
				"Cf. A102283."
			],
			"keyword": "nonn,frac",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_",
			"references": 5,
			"revision": 33,
			"time": "2019-11-18T01:17:37-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}