{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003320",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3320,
			"id": "M1198",
			"data": "1,1,1,2,4,9,27,81,256,1024,4096,16384,78125,390625,1953125,10077696,60466176,362797056,2176782336,13841287201,96889010407,678223072849,4747561509943,35184372088832,281474976710656,2251799813685248",
			"name": "a(n) = max_{k=0..n} k^(n-k).",
			"comment": [
				"For n \u003e 0: a(n+1) = largest term of row n in triangles A051129 and A247358. - _Reinhard Zumkeller_, Sep 14 2014"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"I. Tomescu, Introducere in Combinatorica. Editura Tehnica, Bucharest, 1972, p. 231."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A003320/b003320.txt\"\u003eTable of n, a(n) for n=0..100\u003c/a\u003e",
				"D. Easdown, \u003ca href=\"http://www.maths.usyd.edu.au/u/pubs/publist/preprints/2015/easdown-19.pdf\"\u003eMinimal faithful permutation and transformation representations of groups and semigroups\u003c/a\u003e, Contemporary Math. (1992), Vol. 131 (Part 3), 75-84.",
				"R. Gray and J. D. Mitchell, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2007.08.075\"\u003eLargest subsemigroups of the full transformation monoid\u003c/a\u003e, Discrete Math., 308 (2008), 4801-4810.",
				"W. S. Gray and M. Thitsa, \u003ca href=\"https://doi.org/10.1109/SSST.2013.6524939\"\u003eSystem Interconnections and Combinatorial Integer Sequences\u003c/a\u003e, in: System Theory (SSST), 2013 45th Southeastern Symposium on, Date of Conference: 11-11 March 2013, Digital Object Identifier: 10.1109/SSST.2013.6524939.",
				"R. K. Guy, \u003ca href=\"/A003320/a003320.pdf\"\u003eLetter to N. J. A. Sloane, Mar 1974\u003c/a\u003e",
				"I. Tomescu, \u003ca href=\"/A003320/a003320_4.pdf\"\u003eExcerpts from \"Introducese in Combinatorica\" (1972)\u003c/a\u003e, pp. 230-1, 44-5, 128-9. (Annotated scanned copy)"
			],
			"formula": [
				"a(n) = A056155(n-1)^(n - A056155(n-1)), for n\u003e=2. - _Ridouane Oudra_, Dec 09 2020"
			],
			"example": [
				"a(5) = max(5^0, 4^1, 3^2, 2^3, 1^4, 0^5) = max(1,4,9,8,1,0) = 9."
			],
			"mathematica": [
				"Join[{1},Max[#]\u0026/@Table[k^(n-k),{n,25},{k,n}]] (* _Harvey P. Dale_, Jun 20 2011 *)"
			],
			"program": [
				"(Haskell)",
				"a003320 n = maximum $ zipWith (^) [0 .. n] [n, n-1 ..]",
				"-- _Reinhard Zumkeller_, Jun 24 2013",
				"(PARI) a(n) = vecmax(vector(n+1, k, (k-1)^(n-k+1))); \\\\ _Michel Marcus_, Jun 13 2017"
			],
			"xref": [
				"Cf. A003992, A031435, A056155."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_, _R. K. Guy_",
			"ext": [
				"Easdown reference from Michail Kats (KatsMM(AT)info.sgu.ru)",
				"More terms from _James A. Sellers_, Aug 21 2000"
			],
			"references": 10,
			"revision": 52,
			"time": "2020-12-11T06:22:27-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}