{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A269021",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 269021,
			"data": "1,2,23,588,24553,1438112,108469917,9996042284,1086997811325,136102249609224,19269396089593156,3042212958893941456,529708789768374664407,100813134967124531098768,20816198414187782633783462,4634136282168760818748363080",
			"name": "Number of permutations of [2n] containing an increasing subsequence of length n.",
			"link": [
				"Alois P. Heinz and Vaclav Kotesovec, \u003ca href=\"/A269021/b269021.txt\"\u003eTable of n, a(n) for n = 0..41\u003c/a\u003e (terms 0..30 from Alois P. Heinz)"
			],
			"formula": [
				"a(n) = A214152(2n,n).",
				"a(n) = (2n)! - A269042(n).",
				"a(n) ~ 16^n * (n-1)! / (Pi * exp(2)). - _Vaclav Kotesovec_, Mar 27 2016"
			],
			"example": [
				"a(1) = 2: 12, 21.",
				"a(2) = 23: all 4! permutations of {1,2,3,4} with the exception of 4321."
			],
			"maple": [
				"h:= proc(l) (n-\u003e add(i, i=l)!/mul(mul(1+l[i]-j+add(`if`(",
				"      l[k]\u003e=j, 1, 0), k=i+1..n), j=1..l[i]), i=1..n))(nops(l))",
				"    end:",
				"g:= (n, i, l)-\u003e `if`(n=0 or i=1, h([l[], 1$n])^2, `if`(i\u003c1, 0,",
				"                 add(g(n-i*j, i-1, [l[], i$j]), j=0..n/i))):",
				"a:= n-\u003e `if`(n=0, 1, (2*n)!-g(2*n, n-1, [])):",
				"seq(a(n), n=0..16);"
			],
			"mathematica": [
				"h[l_] := Function[n, Total[l]!/Product[Product[1+l[[i]]-j+Sum[If[l[[k]] \u003e= j, 1, 0], {k, i+1, n}], {j, 1, l[[i]]}], {i, 1, n}]][Length[l]];",
				"g[n_, i_, l_] := If[n == 0 || i == 1, h[Join[l, Table[1, {n}]]]^2, If[i \u003c 1, 0, Sum[g[n - i*j, i - 1, Join[l, Table[i, {j}]]], {j, 0, n/i}]]];",
				"a[n_] := If[n == 0, 1, (2n)! - g[2n, n-1, {}]];",
				"Table[a[n], {n, 0, 16}] (* _Jean-François Alcover_, Apr 01 2017, translated from Maple *)"
			],
			"xref": [
				"Cf. A010050, A214152, A269042."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Alois P. Heinz_, Feb 17 2016",
			"references": 3,
			"revision": 18,
			"time": "2017-04-01T19:38:42-04:00",
			"created": "2016-02-18T08:51:42-05:00"
		}
	]
}