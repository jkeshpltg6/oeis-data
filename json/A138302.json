{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A138302",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 138302,
			"data": "1,2,3,4,5,6,7,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,25,26,28,29,30,31,33,34,35,36,37,38,39,41,42,43,44,45,46,47,48,49,50,51,52,53,55,57,58,59,60,61,62,63,65,66,67,68,69,70,71,73,74,75,76,77,78,79,80,81",
			"name": "Exponentially 2^n-numbers: 1 together with positive integers k such that all exponents in prime factorization of k are powers of 2.",
			"comment": [
				"Previous name: sequence consists of products of distinct relatively prime terms of A084400. - _Vladimir Shevelev_, Sep 24 2015",
				"These numbers are also called \"compact integers.\"",
				"The density of this sequence exists and equals 0.872497...",
				"There exist only seven compact factorials A000142(n) for n=1,2,3,6,7,10 and 11.",
				"For a general definition of exponentially S-numbers, see comments in A209061. - _Vladimir Shevelev_, Sep 24 2015",
				"The first 1000 digits of the density of the sequence were calculated by _Juan Arias-de-Reyna_ in A271727. - _Vladimir Shevelev_, Apr 18 2016",
				"A225546 maps the set of terms 1:1 onto A268375. - _Peter Munn_, Jan 26 2020",
				"Numbers whose sets of unitary divisors (A077610) and infinitary divisors (A077609) coincide. - _Amiram Eldar_, Dec 23 2020"
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A138302/b138302.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"S. Litsyn and V. Shevelev, \u003ca href=\"http://www.emis.de/journals/INTEGERS/papers/h33/h33.Abstract.html\"\u003eOn Factorization of Integers with Restrictions on the Exponents\u003c/a\u003e, INTEGERS: The Electronic Journal of Combinatorial Number Theory 7 (2007), #A33.",
				"Vladimir Shevelev, \u003ca href=\"http://dx.doi.org/10.4064/aa126-3-1\"\u003eCompact integers and factorials\u003c/a\u003e, Acta Arithmetica 126:3 (2007), pp. 195-236.",
				"Vladimir Shevelev, \u003ca href=\"http://arxiv.org/abs/1511.03860\"\u003eSet of all densities of exponentially S-numbers\u003c/a\u003e, arXiv preprint arXiv:1511.03860 [math.NT], 2015-2016."
			],
			"formula": [
				"Identities arising from the calculation of the density h of the sequence (cf. [Shevelev] and comment for a generalization in A209061):",
				"h = Product_{prime p} Sum_{j in {0 and 2^k}}(p-1)/p^(j+1) = Product_{prime p} (1 + Sum_{j\u003e=2} (u(j) - u(j-1))/p^j) = (1/zeta(2))* Product_{p}(1 + 1/(p+1))*Sum_{i\u003e=1}p^(-(2^i-1)), where u(n) is the characteristic function of set {2^k, k\u003e=0}. - _Vladimir Shevelev_, Sep 24 2015"
			],
			"example": [
				"60 = 2^(2^1)*3^(2^0)*5^(2^0)."
			],
			"maple": [
				"isA000079 := proc(n)",
				"    if n = 1 then",
				"        true;",
				"    else",
				"        type(n,'even') and nops(numtheory[factorset](n))=1 ;",
				"        simplify(%) ;",
				"    end if;",
				"end proc:",
				"isA138302 := proc(n)",
				"    local p;",
				"    if n = 1 then",
				"        return true;",
				"    end if;",
				"    for p in ifactors(n)[2] do",
				"        if not isA000079(op(2,p)) then",
				"            return false;",
				"        end if;",
				"    end do:",
				"    true ;",
				"end proc:",
				"for n from 1 to 100 do",
				"    if isA138302(n) then",
				"        printf(\"%d,\",n) ;",
				"    end if;",
				"end do: # _R. J. Mathar_, Sep 27 2016"
			],
			"mathematica": [
				"lst={}; Do[p=Prime[n]; s=p^(1/3); f=Floor[s]; a=f^3; d=p-a; AppendTo[lst,d], {n,100}]; Union[lst] (* _Vladimir Joseph Stephan Orlovsky_, Mar 11 2009 *)",
				"selQ[n_] := AllTrue[FactorInteger[n][[All, 2]], IntegerQ[Log[2, #]]\u0026];",
				"Select[Range[100], selQ] (* _Jean-François Alcover_, Oct 29 2018 *)"
			],
			"program": [
				"(PARI) is(n)=if(n\u003c8, n\u003e0, vecmin(apply(n-\u003en\u003e\u003evaluation(n,2)==1, factor(n)[,2]))) \\\\ _Charles R Greathouse IV_, Dec 07 2012"
			],
			"xref": [
				"Cf. A000142, A005117, A050376, A084400, A209061, A271727.",
				"Related to A268375 via A225546."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Vladimir Shevelev_, May 07 2008",
			"ext": [
				"Incorrect comment removed by _Charles R Greathouse IV_, Dec 07 2012",
				"Simpler name from _Vladimir Shevelev_, Sep 24 2015",
				"Edited by _N. J. A. Sloane_, Nov 07 2015"
			],
			"references": 13,
			"revision": 71,
			"time": "2020-12-23T04:10:29-05:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}