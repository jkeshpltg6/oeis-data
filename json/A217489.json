{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A217489",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 217489,
			"data": "2,3,4,5,6,7,8,9,20,23,25,27,29,33,22,35,26,37,32,43,34,38,28,39,40,30,44,42,45,46,47,50,24,49,53,52,57,36,55,48,54,58,59,56,62,63,64,65,67,68,69,70,60,73,74,66,75,72,79,76,80,77,78,82,83,85,84,86,87,89,92,93,88,90",
			"name": "Least positive integer without a digit 1, not listed earlier and not divisible by any digit of the preceding term.",
			"comment": [
				"This sequence contains all terms of A052383 that are not divisible by 2520. - _Peter Kagey_, Nov 04 2015",
				"From _Robert Israel_, Jan 03 2016: (Start)",
				"Here is a proof of Peter Kagey's comment:",
				"Any number x in A052383 will eventually appear in the sequence if there are infinitely many members of the sequence containing no digit that divides x.",
				"If k in A052383 is coprime to 210 (and thus not divisible by any digit \u003e 1), then k is in the sequence.",
				"The numbers 2...23 with number of 2's not divisible by 3, and 5...57 with number of 5's == 2,4 or 5 (mod 6) are coprime to 210, and thus are in the sequence.",
				"The repunits k...k with k = 5 or 7 and an even number of digits are not divisible by 2 or 3, and thus they are in the sequence.",
				"The repunits k...k with k = 2,3,4,6,8, or 9 and number of digits not divisible by 6 are not divisible by 5 or 7, and thus they are in the sequence. Any x in A052383 not divisible by 2520 is not divisible by one of the digits 2,3,...9, and thus is in the sequence. (End)"
			],
			"link": [
				"Peter Kagey, \u003ca href=\"/A217489/b217489.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"E. Angelini, \u003ca href=\"http://www.cetteadressecomportecinquantesignes.com/NotDivisible.htm\"\u003e(n+1) is not divisible by any digit of n\u003c/a\u003e, Oct 04 2012",
				"E. Angelini, \u003ca href=\"/A217489/a217489.pdf\"\u003e(n+1) is not divisible by any digit of n\u003c/a\u003e [Cached copy, with permission]"
			],
			"maple": [
				"N:= 1000: # to get all terms before the first that exceeds N",
				"A[1]:= 2:",
				"Av:= remove(t -\u003e has(convert(t,base,10),1),{$3..N}):",
				"for n from 2 do",
				"  d:= convert(convert(A[n-1],base,10),set) minus {0};",
				"  Ad:= remove(t -\u003e ormap(y -\u003e t mod y = 0, d) , Av);",
				"  if nops(Ad) = 0 then break fi;",
				"  A[n]:= min(Ad);",
				"  Av:= Av minus {A[n]};",
				"od:",
				"seq(A[i],i=1..n-1); # _Robert Israel_, Jan 03 2016"
			],
			"mathematica": [
				"a = {2}; Do[k = 1; While[Or[First@ DigitCount@ k \u003e 0, MemberQ[a, k], Total[Boole@ Divisible[k, #] \u0026 /@ (IntegerDigits@ a[[n - 1]] /. 0 -\u003e Nothing)] \u003e 0], k++]; AppendTo[a, k], {n, 2, 74}]; a (* _Michael De Vlieger_, Nov 05 2015 *)"
			],
			"program": [
				"(PARI \u003e= 2.6) A217489_vect(Nmax)={my(a=[],d=[0],u=0,nd); while( #a\u003cNmax, for( t=2,9e9, bittest(u,t) \u0026 next; for(i=1+!d[1],#d, t%d[i] || next(2)); nd=Set(digits(t)); setsearch(nd,1) \u0026 next; a=concat(a,t); u+=1\u003c\u003ct; d=nd; break));a}"
			],
			"xref": [
				"Cf. A002275, A038603, A052383.",
				"Sequence A217491 is a variant of the same idea (where injectivity is strengthened to strict monotonicity)."
			],
			"keyword": "nonn,base,easy,look",
			"offset": "1,1",
			"author": "_M. F. Hasler_ and _Eric Angelini_, Oct 04 2012",
			"references": 2,
			"revision": 49,
			"time": "2016-06-10T00:21:00-04:00",
			"created": "2012-10-05T12:07:25-04:00"
		}
	]
}