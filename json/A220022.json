{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A220022",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 220022,
			"data": "1,1,20,20,10,1,5,4,20,10,1,10,20,20,10,2,5,20,4,10,1,5,20,20,2,1,1,20,20,10,1,10,4,20,10,2,5,20,20,10,1,5,20,4,10,1,5,20,20,2,1,2,20,20,10,2,5,4,20,10,1,5,20,20,10,1,5,20,4,10,1,10,20,20,2,2,1",
			"name": "The period with which the powers of n repeat mod 100.",
			"comment": [
				"a(n) will always be a divisor of Phi(100) = 40.",
				"This sequence is periodic with a period of 100 because n^i mod 100 = (n + 100)^i mod 100.",
				"For the odd numbers n ending in {1, 3, 7, 9} which are coprime to 10, we can expect the powers of n mod 100 to loop back to 1, with the value of n^a(n) mod 100 = 1, but for the other numbers n that are not coprime to 10, they do not loop back to 1.",
				"For the even numbers n ending in {2, 4, 6, 8}, the value of n^a(n) mod 100 will be equal to 76.",
				"For the numbers n that are congruent to 5 (mod 20), the value of n^i mod 100 will be equal to 25, for all i \u003e= 2.",
				"For the numbers n that are congruent to 15 (mod 20), the value of n^a(n) mod 100 will be equal to 25, with n^i mod 100 = 25 for the even values of i and n^i mod 100 = 75 for the odd values of i, for all i \u003e= 2.",
				"For the numbers n ending in 0, i.e., that are congruent to 0 (mod 10), the value of n^i mod 100 will be equal to 0, for all i \u003e= 2."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A220022/b220022.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e"
			],
			"example": [
				"a(2) = 20 since 2^i mod 100, for i = 2..22 = {04, 08, 16, 32, 64, 28, 56, 12, 24, 48, 96, 92, 84, 68, 36, 72, 44, 88, 76, 52, 04}.",
				"a(3) = 20 since 3^i mod 100, for i = 0..20 = {01, 03, 09, 27, 81, 43, 29, 87, 61, 83, 49, 47, 41, 23, 69, 07, 21, 63, 89, 67, 01}.",
				"But a(7) = 4 since 7^i mod 100, for i = 0..4 = {01, 07, 49, 43, 01}."
			],
			"mathematica": [
				"Flatten[Table[s = Table[PowerMod[n, e, 100], {e, 2, 100}]; Union[Differences[Position[s, s[[1]]]]], {n, 0, 100}]] (* _T. D. Noe_, Dec 14 2012 *)"
			],
			"program": [
				"(PARI) for(n=0, 100, x=(n*n)%100; y=(n*n*n)%100; z=1; while(x!=y, x=(x*n)%100; y=(y*n*n)%100; z++); print1(z\", \"))"
			],
			"xref": [
				"Cf. A173635 (period with which the powers of n repeat mod 10)."
			],
			"keyword": "nonn,base",
			"offset": "0,3",
			"author": "_V. Raman_, Dec 13 2012",
			"references": 6,
			"revision": 32,
			"time": "2017-07-25T02:49:26-04:00",
			"created": "2012-12-14T11:56:50-05:00"
		}
	]
}