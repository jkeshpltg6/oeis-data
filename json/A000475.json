{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000475",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 475,
			"id": "M4969 N2132",
			"data": "1,0,15,70,630,5544,55650,611820,7342335,95449640,1336295961,20044438050,320711010620,5452087178160,98137569209940,1864613814984984,37292276299704525,783137802293789040,17229031650463366195,396267727960657413630",
			"name": "Rencontres numbers: number of permutations of [n] with exactly 4 fixed points.",
			"reference": [
				"J. Riordan, An Introduction to Combinatorial Analysis, Wiley, 1958, p. 65.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A000475/b000475.txt\"\u003eTable of n, a(n) for n=4..100\u003c/a\u003e",
				"FindStat - Combinatorial Statistic Finder, \u003ca href=\"http://www.findstat.org/St000022\"\u003eThe number of fixed points of a permutation\u003c/a\u003e"
			],
			"formula": [
				"a(n) = sum((-1)^j*n!/(4!*j!), j=2..n-4).",
				"a(n) = A000166(n)*binomial(n+4, 4). - Robert Goodhand (robert(AT)rgoodhand.fsnet.co.uk), Nov 08 2001",
				"E.g.f.: (exp(-x)/(1-x))*(x^4/4!). In general, for k fixed points:(exp(-x)/(1-x)) * (x^k/k!). - _Wenjin Woan_, Nov 22 2008",
				"a(n) ~ n! * exp(-1)/24, in general a(n) ~ n! * exp(-1)/k!. - _Vaclav Kotesovec_, Mar 16 2014",
				"a(n) = n*a(n-1) + (-1^n)*binomial(n,4) with a(n) = 0 for n = 0,1,2,3. - _Chai Wah Wu_, Nov 01 2014",
				"Conjecture: (-n+4)*a(n) +n*(n-5)*a(n-1) +n*(n-1)*a(n-2)=0. - _R. J. Mathar_, Nov 02 2015",
				"O.g.f.: (1/24)*Sum_{k\u003e=4} k!*x^k/(1 + x)^(k+1). - _Ilya Gutkovskiy_, Apr 13 2017"
			],
			"maple": [
				"a:=n-\u003esum(n!*sum((-1)^k/(k-3)!, j=0..n), k=3..n): seq(-a(n)/4!, n=3..22); # _Zerinvary Lajos_, May 25 2007",
				"G(x):=exp(-x)/(1-x)*(x^4/4!): f[0]:=G(x): for n from 1 to 26 do f[n]:=diff(f[n-1],x) od: x:=0: seq(f[n],n=4..23); # _Zerinvary Lajos_, Apr 03 2009"
			],
			"mathematica": [
				"Table[Subfactorial[n - 4]*Binomial[n, 4], {n, 4, 23}] (* _Zerinvary Lajos_, Jul 10 2009 *)"
			],
			"program": [
				"(PARI) x='x+O('x^66); Vec( serlaplace(exp(-x)/(1-x)*(x^4/4!)) ) \\\\ _Joerg Arndt_, Feb 19 2014",
				"(Python)",
				"from sympy import binomial",
				"A000475_list, m, x = [], 1, 0",
				"for n in range(4,100):",
				"    x, m = x*n + m*binomial(n,4), -m",
				"    A000475_list.append(x) # _Chai Wah Wu_, Nov 01 2014"
			],
			"xref": [
				"Cf. A008290, A000166, A000240, A000387, A000449, A129135.",
				"A diagonal of A008291.",
				"Cf. A170942."
			],
			"keyword": "nonn",
			"offset": "4,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Formula corrected by _Sean A. Irvine_, Oct 26 2010"
			],
			"references": 21,
			"revision": 54,
			"time": "2021-05-13T11:25:57-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}