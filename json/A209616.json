{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A209616",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 209616,
			"data": "0,1,2,4,7,12,18,29,42,63,89,128,176,246,333,453,603,807,1058,1393,1807,2346,3011,3867,4915,6248,7879,9926,12421,15529,19297,23954,29585,36486,44802,54937,67096,81831,99459,120700,146026,176410,212512,255636,306734",
			"name": "Sum of positive Dyson's ranks of all partitions of n.",
			"comment": [
				"The Dyson's rank of a partition is the largest part minus the number of parts."
			],
			"reference": [
				"F. J. Dyson, Some guesses in the theory of partitions, Eureka (Cambridge) 8 (1944), 10-15."
			],
			"link": [
				"G. E. Andrews, S. H. G. Chan, and B. Kim, \u003ca href=\"http://www.math.psu.edu/andrews/pdf/292.pdf\"\u003eThe odd moments of ranks and cranks\u003c/a\u003e (See the function R_1), Journal of Combinatorial Theory, Series A, Volume 120, Issue 1, January 2013, Pages 77-91.",
				"Frank Garvan, \u003ca href=\"http://www.combinatorics.net/conf/A75/Slides/02_03_Garvan.pdf\"\u003eDyson's rank function and Andrews's SPT-function\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A115995(n) - A195012(n). - _Omar E. Pol_, Apr 06 2012"
			],
			"example": [
				"For n = 5 we have:",
				"--------------------------",
				"Partitions        Dyson's",
				"of 5               rank",
				"--------------------------",
				"5               5 - 1 =  4",
				"4+1             4 - 2 =  2",
				"3+2             3 - 2 =  1",
				"3+1+1           3 - 3 =  0",
				"2+2+1           2 - 3 = -1",
				"2+1+1+1         2 - 4 = -2",
				"1+1+1+1+1       1 - 5 = -4",
				"--------------------------",
				"The sum of positive Dyson's ranks of all partitions of 5 is 4+2+1 = 7 so a(5) = 7."
			],
			"maple": [
				"# Maple program based on Theorem 1 of Andrews-Chan-Kim:",
				"M:=101;",
				"qinf:=mul(1-q^i,i=1..M);",
				"qinf:=series(qinf,q,M);",
				"R1:=add((-1)^(n+1)*q^(n*(3*n+1)/2)/(1-q^n),n=1..M);",
				"R1:=series(R1/qinf,q,M);",
				"seriestolist(%); # _N. J. A. Sloane_, Sep 04 2012"
			],
			"mathematica": [
				"M = 101;",
				"qinf = Product[1-q^i, {i, 1, M}];",
				"qinf = Series[qinf, {q, 0, M}];",
				"R1 = Sum[(-1)^(n+1) q^(n(3n+1)/2)/(1-q^n), {n, 1, M}];",
				"R1 = Series[R1/qinf, {q, 0, M}];",
				"CoefficientList[R1, q] // Rest (* _Jean-François Alcover_, Aug 18 2018, translated from Maple *)"
			],
			"xref": [
				"Column 1 of triangle A208482.",
				"Cf. A063995, A092269, A105805, A194547, A194549, A195822, A208478."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Omar E. Pol_, Mar 10 2012",
			"ext": [
				"More terms from _Alois P. Heinz_, Mar 10 2012"
			],
			"references": 16,
			"revision": 36,
			"time": "2018-08-18T08:37:10-04:00",
			"created": "2012-03-11T09:54:28-04:00"
		}
	]
}