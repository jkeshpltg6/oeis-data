{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A242351",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 242351,
			"data": "1,1,1,1,1,4,1,11,3,1,26,25,1,57,128,17,1,120,525,229,2,1,247,1901,1819,172,1,502,6371,11172,3048,53,1,1013,20291,58847,33065,2751,7,1,2036,62407,280158,275641,56905,1422,1,4083,187272,1242859,1945529,771451,61966,436",
			"name": "Number T(n,k) of isoscent sequences of length n with exactly k ascents; triangle T(n,k), n\u003e=0, 0\u003c=k\u003c=n+3-ceiling(2*sqrt(n+2)), read by rows.",
			"comment": [
				"An isoscent sequence of length n is an integer sequence [s(1),...,s(n)] with s(1) = 0 and 0 \u003c= s(i) \u003c= 1 plus the number of level steps in [s(1),...,s(i)].",
				"Columns k=0-10 give: A000012, A000295, A243228, A243229, A243230, A243231, A243232, A243233, A243234, A243235, A243236.",
				"Row sums give A000110.",
				"Last elements of rows give A243237."
			],
			"link": [
				"Joerg Arndt and Alois P. Heinz, \u003ca href=\"/A242351/b242351.txt\"\u003eRows n = 0..100, flattened\u003c/a\u003e"
			],
			"example": [
				"T(4,0) = 1: [0,0,0,0].",
				"T(4,1) = 11: [0,0,0,1], [0,0,0,2], [0,0,0,3], [0,0,1,0], [0,0,1,1], [0,0,2,0], [0,0,2,1], [0,0,2,2], [0,1,0,0], [0,1,1,0], [0,1,1,1].",
				"T(4,2) = 3: [0,0,1,2], [0,1,0,1], [0,1,1,2].",
				"Triangle T(n,k) begins:",
				"  1;",
				"  1;",
				"  1,    1;",
				"  1,    4;",
				"  1,   11,     3;",
				"  1,   26,    25;",
				"  1,   57,   128,    17;",
				"  1,  120,   525,   229,     2;",
				"  1,  247,  1901,  1819,   172;",
				"  1,  502,  6371, 11172,  3048,   53;",
				"  1, 1013, 20291, 58847, 33065, 2751, 7;",
				"  ..."
			],
			"maple": [
				"b:= proc(n, i, t) option remember; `if`(n\u003c1, 1, expand(add(",
				"      `if`(j\u003ei, x, 1) *b(n-1, j, t+`if`(j=i, 1, 0)), j=0..t+1)))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=0..degree(p)))(b(n-1, 0$2)):",
				"seq(T(n), n=0..15);"
			],
			"mathematica": [
				"b[n_, i_, t_] := b[n, i, t] = If[n\u003c1, 1, Expand[Sum[If[j\u003ei, x, 1]*b[n-1, j, t + If[j == i, 1, 0]], {j, 0, t+1}]]]; T[n_] := Function[{p}, Table[ Coefficient[p, x, i], {i, 0, Exponent[p, x]}]][b[n-1, 0, 0]]; Table[T[n], {n, 0, 15}] // Flatten (* _Jean-François Alcover_, Feb 09 2015, after Maple *)"
			],
			"xref": [
				"Cf. A048993 (for counting level steps), A242352 (for counting descents), A137251 (ascent sequences counting ascents), A238858 (ascent sequences counting descents), A242153 (ascent sequences counting level steps), A083479."
			],
			"keyword": "nonn,tabf",
			"offset": "0,6",
			"author": "_Joerg Arndt_ and _Alois P. Heinz_, May 11 2014",
			"references": 12,
			"revision": 33,
			"time": "2019-05-04T11:24:34-04:00",
			"created": "2014-05-19T11:17:36-04:00"
		}
	]
}