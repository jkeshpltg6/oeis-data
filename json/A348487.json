{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A348487",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 348487,
			"data": "1,11,39,41,101,111,119,121,129,131,139,141,319,321,329,331,349,351,359,361,369,371,379,381,389,391,399,401,409,411,419,421,429,431,439,441,1001,1009,1011,1019,1021,1029,1031,1039,1041,1099,1101,1109,1111,1119,1121,1129,1131,1139",
			"name": "Positive numbers whose square starts and ends with exactly one 1.",
			"comment": [
				"When a square ends with 1, this square ends with exactly one 1.",
				"Sequences A000533 and A253213 show that there are an infinity of terms. The square of their terms, for n \u003e= 3, starts and ends with exactly one 1. Also, the numbers 119, 1119, 11119, ..., ((10^k + 71) / 9)^2, (k \u003e= 3) are terms. The squares ((10^k + 71) / 9)^2, have the last digit 1 and because 12*10^(2*k - 3) \u003c ((10^k + 71) / 9)^2 \u003c13*10^(2*k - 3), for k \u003e= 3, the squares ((10^k + 71) / 9)^2, k \u003e= 4, start with 12. - _Marius A. Burtea_, Oct 21 2021"
			],
			"example": [
				"39 is a term since 39^2 = 1521.",
				"109 is not a term since 109^2 = 11881.",
				"119 is a term since 119^2 = 14161."
			],
			"mathematica": [
				"Join[{1}, Select[Range[11, 1200], (d = IntegerDigits[#^2])[[1]] == d[[-1]] == 1 \u0026\u0026 d[[2]] != 1 \u0026]] (* _Amiram Eldar_, Oct 21 2021 *)"
			],
			"program": [
				"(Python)",
				"from itertools import count, takewhile",
				"def ok(n):",
				"  s = str(n*n); return len(s.rstrip(\"1\")) == len(s.lstrip(\"1\")) == len(s)-1",
				"def aupto(N):",
				"  r = takewhile(lambda x: x\u003c=N, (10*i+d for i in count(0) for d in [1, 9]))",
				"  return [k for k in r if ok(k)]",
				"print(aupto(1140)) # _Michael S. Branicky_, Oct 21 2021",
				"(PARI) isok(k) = my(d=digits(sqr(k))); (d[1]==1) \u0026\u0026 (d[#d]==1) \u0026\u0026 if (#d\u003e2, (d[2]!=1) \u0026\u0026 (d[#d-1]!=1), 1); \\\\ _Michel Marcus_, Oct 21 2021",
				"(MAGMA) [1] cat [n:n in [2..1200]|Intseq(n*n)[1] eq 1 and Intseq(n*n)[#Intseq(n*n)] eq 1 and Intseq(n*n)[-1+#Intseq(n*n)] ne 1]; // _Marius A. Burtea_, Oct 21 2021"
			],
			"xref": [
				"Cf. A045855, A090771, A253213, A273372 (squares ending with 1), A017281, A017377.",
				"Cf. A000533, A253213 for n \u003e= 2 (subsequences).",
				"Subsequence of A305719."
			],
			"keyword": "nonn,base",
			"offset": "1,2",
			"author": "_Bernard Schott_, Oct 21 2021",
			"references": 4,
			"revision": 29,
			"time": "2021-10-22T23:50:13-04:00",
			"created": "2021-10-22T23:50:13-04:00"
		}
	]
}