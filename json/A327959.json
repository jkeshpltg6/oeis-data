{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A327959",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 327959,
			"data": "1,-248,4124,-34752,213126,-1057504,4530744,-17333248,60655377,-197230000,603096260,-1749556736,4848776870,-12908659008,33161242504,-82505707520,199429765972,-469556091240,1079330385764,-2426800117504,5346409013164,-11558035326944",
			"name": "Expansion of (-j(1/2 + t))^(1/3) * q^(1/3) in powers of q = exp(2 Pi i t) where j is the modular j-function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Given g.f. A(x), then B(q) = A(q^3) / q satisfies J_n = B(sqrt(-n)/2)/32 where a few values of J_n as given in Ramanujan, Notebooks, Vol. 2, page 392."
			],
			"reference": [
				"S. Ramanujan, Notebooks, Tata Institute of Fundamental Research, Bombay 1957 Vol. 2. See page 392."
			],
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of chi(x)^8 - 256 * x / chi(x)^16 in powers of x where chi() is a Ramanujan theta function.",
				"Expansion of (phi(x)^8 - (2 * phi(x) * phi(-x))^4 + 16 * phi(-x)^8) / f(x)^8 in powers of x where phi(), f() are Ramanujan theta functions.",
				"Expansion of q^(1/3) * (eta(q)^2 / (eta(q) * eta(q^4)))^8 + 256 * (eta(q) * eta(q^4) / eta(q^2))^16 in powers of q.",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (4 t)) = f(t) where q = exp(2 Pi i t).",
				"a(n) = (-1)^n * A007245(n)."
			],
			"example": [
				"G.f. = 1 - 248*x + 4124*x^2 - 34752*x^3 + 213126*x^4 - 1057504*x^5 + ...",
				"G.f. = q^-1 - 248*q^2 + 4124*q^5 - 34752*q^8 + 213126*q^11 - 1057504*q^14 + ...",
				"If J_n := (-j(1/2 + sqrt(-n)/2))^(1/3) / 32, then J_3 = 0, j_11 = 1, j_19 = 3, J_43 = 30, J_67 = 165, J_163 = 20010."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ With[ {m = InverseEllipticNomeQ[q]}, (1 - 16 m (1 - m)) / (4 m (1 - m))^(1/3)] 4 (-q)^(1/3), {q, 0, n}] // Simplify;"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, polcoeff( (-x * ellj( -x + x^2 * O(x^n)))^(1/3), n))};"
			],
			"xref": [
				"Cf. A007245."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Sep 30 2019",
			"references": 0,
			"revision": 7,
			"time": "2019-10-31T18:23:43-04:00",
			"created": "2019-09-30T20:40:51-04:00"
		}
	]
}