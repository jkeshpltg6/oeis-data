{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A196199",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 196199,
			"data": "0,-1,0,1,-2,-1,0,1,2,-3,-2,-1,0,1,2,3,-4,-3,-2,-1,0,1,2,3,4,-5,-4,-3,-2,-1,0,1,2,3,4,5,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8",
			"name": "Count up from -n to n for n = 0, 1, 2, ... .",
			"comment": [
				"This sequence contains every integer infinitely often, hence all integer sequences are subsequences.",
				"This is a fractal sequence.",
				"Indeed, if all terms (a(n),a(n+1)) such that a(n+1) does NOT equal a(n)+1 (\u003c=\u003e a(n+1) \u003c a(n)) are deleted, the same sequence is recovered again. See A253580 for an \"opposite\" yet similar \"fractal tree\" construction. - _M. F. Hasler_, Jan 04 2015"
			],
			"reference": [
				"Miklós Laczkovich, Conjecture and Proof, TypoTex, Budapest, 1998. See Chapter 10."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A196199/b196199.txt\"\u003eRows n=0..100 of triangle, flattened\u003c/a\u003e",
				"Boris Putievskiy, \u003ca href=\"http://arxiv.org/abs/1212.2732\"\u003eTransformations [of] Integer Sequences And Pairing Functions\u003c/a\u003e arXiv:1212.2732 [math.CO], 2012."
			],
			"formula": [
				"a(n) = n - t*t - t - 1, where t = floor(sqrt(n-1)). - _Boris Putievskiy_, Jan 28 2013",
				"G.f.: x/(x-1)^2 + 1/(x-1)*sum(k \u003e= 1, 2*k*x^(k^2)).  The series is related to Jacobi theta functions. - _Robert Israel_, Jan 05 2015"
			],
			"example": [
				"Table starts:",
				"            0,",
				"        -1, 0, 1,",
				"    -2, -1, 0, 1, 2,",
				"-3, -2, -1, 0, 1, 2, 3,",
				"...",
				"The sequence of fractions A196199/A004737 = 0/1, -1/1, 0/2, 1/1, -2/1, -1/2, 0/3, 1/2, 2/1, -3/1, -2/2, -1/3, 0/4, 1/3, 2/2, 3/1, -4/4. -3/2, ... contains every rational number (infinitely often) [Laczkovich]. - _N. J. A. Sloane_, Oct 09 2013"
			],
			"maple": [
				"seq(seq(j-k-k^2, j=k^2 .. (k+1)^2-1), k = 0 .. 10); # _Robert Israel_, Jan 05 2015",
				"# Alternatively, as a table with rows -n\u003c=k\u003c=n (compare A257564):",
				"r := n -\u003e (n-(n mod 2))/2: T := (n, k) -\u003e r(n+k) - r(n-k):",
				"seq(print(seq(T(n, k), k=-n..n)), n=0..6); # _Peter Luschny_, May 28 2015"
			],
			"mathematica": [
				"Table[Range[-n, n], {n, 0, 9}] // Flatten",
				"(* or *)",
				"a[n_] := With[{t = Floor[Sqrt[n]]}, n - t (t + 1)];",
				"Table[a[n], {n, 0, 99}] (* _Jean-François Alcover_, Jul 13 2018, after _Boris Putievskiy_ *)"
			],
			"program": [
				"(PARI) r=[];for(k=0,8,r=concat(r,vector(2*k+1,j,j-k-1)));r",
				"(Haskell)",
				"a196199 n k = a196199_row n !! k",
				"a196199_tabf = map a196199_row [0..]",
				"a196199_row n = [-n..n]",
				"b196199 = bFile' \"A196199\" (concat $ take 101 a196199_tabf) 0",
				"-- _Reinhard Zumkeller_, Sep 30 2011"
			],
			"xref": [
				"Cf. absolute values A053615, A002262, A002260, row lengths A005408, row sums A000004, A071797.",
				"Cf. A004737."
			],
			"keyword": "sign,tabf,easy,frac,look",
			"offset": "0,5",
			"author": "_Franklin T. Adams-Watters_, Sep 29 2011",
			"references": 8,
			"revision": 39,
			"time": "2018-07-13T11:58:38-04:00",
			"created": "2011-09-29T23:17:50-04:00"
		}
	]
}