{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A227428",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 227428,
			"data": "0,0,1,0,0,2,1,2,4,0,0,2,0,0,4,2,4,8,1,2,4,2,4,8,4,8,13,0,0,2,0,0,4,2,4,8,0,0,4,0,0,8,4,8,16,2,4,8,4,8,16,8,16,26,1,2,4,2,4,8,4,8,13,2,4,8,4,8,16,8,16,26,4,8,13,8,16,26,13,26,40",
			"name": "Number of twos in row n of triangle A083093.",
			"comment": [
				"\"The number of entries with value r in the n-th row of Pascal's triangle modulo k is found to be 2^{#_r^k (n)}, where now #_r^k (n) gives the number of occurrences of the digit r in the base-k representation of the integer n.\" [Wolfram] - _R. J. Mathar_, Jul 26 2017 [This is not correct: there are entries in the sequence that are not powers of 2. - _Antti Karttunen_, Jul 26 2017]"
			],
			"link": [
				"Reinhard Zumkeller (terms 0..1000) \u0026 Antti Karttunen, \u003ca href=\"/A227428/b227428.txt\"\u003eTable of n, a(n) for n = 0..19683\u003c/a\u003e",
				"R. Garfield and H. S. Wilf, \u003ca href=\"https://dx.doi.org/10.1016/0022-314X(92)90078-4\"\u003eThe distribution of the binomial coefficients modulo p\u003c/a\u003e, J. Numb. Theory 41 (1) (1992) 1-5.",
				"Marcus Jaiclin, et al. \u003ca href=\"https://web.archive.org/web/20170823000349/http://pyrrho.wsc.ma.edu/math/faculty/jaiclin/writings/research/pascals_triangle/\"\u003ePascal's Triangle, Mod 2,3,5\u003c/a\u003e",
				"D. L. Wells, \u003ca href=\"http://dx.doi.org/10.1007/978-94-009-0223-7_42\"\u003eResidue counts modulo three for the fibonacci triangle\u003c/a\u003e, Appl. Fib. Numbers, Proc. 6th Int Conf Fib. Numbers, Pullman, 1994 (1996) 521-536.",
				"Avery Wilson, \u003ca href=\"http://www.appliedprobability.org/data/files/MS%20issues/Vol47_No2.pdf\"\u003ePascal's Triangle Modulo 3\u003c/a\u003e, Mathematics Spectrum, 47-2 - January 2015, pp. 72-75.",
				"S. Wolfram, \u003ca href=\"http://dx.doi.org/10.2307/2323743\"\u003eGeometry of binomial coefficients\u003c/a\u003e, Am. Math. Monthly 91 (9) (1984) 566-571."
			],
			"formula": [
				"a(n) = A006047(n) - A206424(n) = n + 1 - A062296(n) - A206424(n).",
				"a(n) = 2^(N_1-1)*(3^N_2-1) where N_1 = A062756(n), N_2 = A081603(n). [Wilson, Theorem 2, Wells] - _R. J. Mathar_, Jul 26 2017",
				"a(n) = A206424(n) * ((3^A081603(n))-1) / ((3^A081603(n))+1). - _Antti Karttunen_, Jul 27 2017",
				"a(n) = (1/2)*Sum_{k = 0..n} mod(C(n,k)^2 - C(n,k), 3). - _Peter Bala_, Dec 17 2020"
			],
			"example": [
				"Example of Wilson's formula: a(26) = 13 = 2^(0-1)*(3^3-1) = 26/2, where A062756(26)=0, A081603(26)=3, 26=(222)_3. - _R. J. Mathar_, Jul 26 2017"
			],
			"maple": [
				"A227428 := proc(n)",
				"    local a;",
				"    a := 0 ;",
				"    for k from 0 to n do",
				"        if A083093(n,k) = 2 then",
				"            a := a+1 ;",
				"        end if;",
				"    end do:",
				"    a ;",
				"end proc:",
				"seq(A227428(n),n=0..20) ; # _R. J. Mathar_, Jul 26 2017"
			],
			"mathematica": [
				"Table[Count[Mod[Binomial[n, Range[0, n]], 3], 2], {n, 0, 99}] (* _Alonso del Arte_, Feb 07 2012 *)"
			],
			"program": [
				"(Haskell)",
				"a227428 = sum . map (flip div 2) . a083093_row",
				"(PARI) A227428(n) = sum(k=0,n,2==(binomial(n,k)%3)); \\\\ (Naive implementation, from the description) _Antti Karttunen_, Jul 26 2017",
				"(Python)",
				"from sympy import binomial",
				"def a(n):",
				"    return sum(1 for k in range(n + 1) if binomial(n, k) % 3 == 2)",
				"print([a(n) for n in range(101)]) # _Indranil Ghosh_, Jul 26 2017",
				"(Scheme) (define (A227428 n) (* (A000079 (- (A062756 n) 1)) (+ -1 (A000244 (A081603 n))))) ;; After Wilson's direct formula, _Antti Karttunen_, Jul 26 2017"
			],
			"xref": [
				"Cf. A006047, A062296, A062756, A083093, A081603, A206424, A206428."
			],
			"keyword": "nonn",
			"offset": "0,6",
			"author": "_Reinhard Zumkeller_, Jul 11 2013",
			"references": 9,
			"revision": 58,
			"time": "2021-03-26T09:25:13-04:00",
			"created": "2013-07-11T17:07:00-04:00"
		}
	]
}