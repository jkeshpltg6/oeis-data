{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A060432",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 60432,
			"data": "1,3,5,8,11,14,18,22,26,30,35,40,45,50,55,61,67,73,79,85,91,98,105,112,119,126,133,140,148,156,164,172,180,188,196,204,213,222,231,240,249,258,267,276,285,295,305,315,325,335,345,355,365,375,385,396,407,418",
			"name": "Partial sums of A002024.",
			"comment": [
				"In other words, first differences give A002024.",
				"Equals A010054 convolved with [1, 2, 3, ...]. - _Gary W. Adamson_, Mar 16 2010"
			],
			"link": [
				"Harry J. Smith, \u003ca href=\"/A060432/b060432.txt\"\u003eTable of n, a(n) for n=1..1000\u003c/a\u003e",
				"Gorka Zamora-López, Romain Brasselet, \u003ca href=\"https://arxiv.org/1810.12825\"\u003eSizing the length of complex networks\u003c/a\u003e, arXiv:1810.12825 [physics.soc-ph], 2018."
			],
			"formula": [
				"Let f(n) = floor(1/2 + sqrt(2*n)), then this function is S(n) = f(1) + f(2) + f(3) + ... + f(n).",
				"a(n) is asymptotic to c*n^(3/2) with c=0.9428.... - _Benoit Cloitre_, Dec 18 2002",
				"a(n) is asymptotic to c*n^(3/2) with c = (2/3)*sqrt(2) = .942809.... - _Franklin T. Adams-Watters_, Sep 07 2006",
				"Set R = round(sqrt(2*n),0), then a(n) = ((6*n+1)*R-R^3)/6. - _Gerald Hillier_, Nov 28 2008",
				"G.f.: W(0)/(2*(1-x)^2), where W(k) = 1 + 1/( 1 - x^(k+1)/( x^(k+1) + 1/W(k+1) )); (continued fraction). - _Sergei N. Gladkovskii_, Aug 21 2013",
				"a(n) = A000330(A003056(n)) + (A003056(n) + 1) * (n - A057944(n)). This represents a closed form, because all of the constituent sequences (i.e., A003056, A000330, A057944) have a known closed form. - _Peter Kagey_, Jan 28 2016",
				"G.f.: x^(7/8)*Theta_2(0,x^(1/2))/(2*(1-x)^2) where Theta_2 is a Jacobi theta function. - _Robert Israel_, Jan 28 2016",
				"G.f.: (x/(1 - x)^2)*Product_{k\u003e=1} (1 - x^(2*k))/(1 - x^(2*k-1)). - _Ilya Gutkovskiy_, May 30 2017",
				"a(n) = n*(k+1)-k*(k+1)*(k+2)/6 where k = A003056(n) is the largest integer such that k*(k+1)/2 \u003c= n. - _Bogdan Blaga_, Feb 04 2021"
			],
			"example": [
				"a(7) = 1 + 2 + 2 + 3 + 3 + 3 + 4 = 18."
			],
			"maple": [
				"ListTools:-PartialSums([seq(n$n,n=1..10)]); # _Robert Israel_, Jan 28 2016"
			],
			"mathematica": [
				"a[n_] := Sum[Floor[1/2 + Sqrt[2*k]], {k, 1, n}]; Array[a, 60] (* _Jean-François Alcover_, Jan 10 2016 *)"
			],
			"program": [
				"(PARI) f(n) = floor(1/2+sqrt(2*n)) for(n=1,100,print1(sum(k=1,n,f(k)),\",\"))",
				"(PARI) { default(realprecision, 100); for (n=1, 1000, a=sum(k=1, n, floor(1/2 + sqrt(2*k))); write(\"b060432.txt\", n, \" \", a); ) } \\\\ _Harry J. Smith_, Jul 05 2009",
				"(Haskell)",
				"a060432 n = sum $ zipWith (*) [n,n-1..1] a010054_list",
				"-- _Reinhard Zumkeller_, Dec 17 2011"
			],
			"xref": [
				"Cf. A002024, A006463, A010054."
			],
			"keyword": "easy,nonn",
			"offset": "1,2",
			"author": "Robert A. Stump (bobess(AT)netzero.net), Apr 06 2001",
			"ext": [
				"More terms from _Jason Earls_, Jan 08 2002"
			],
			"references": 13,
			"revision": 60,
			"time": "2021-02-27T21:24:15-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}