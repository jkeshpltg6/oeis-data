{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A159913",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 159913,
			"data": "1,3,3,7,3,7,7,15,3,7,7,15,7,15,15,31,3,7,7,15,7,15,15,31,7,15,15,31,15,31,31,63,3,7,7,15,7,15,15,31,7,15,15,31,15,31,31,63,7,15,15,31,15,31,31,63,15,31,31,63,31,63,63,127,3,7,7,15,7,15,15,31,7,15,15,31,15,31,31",
			"name": "a(n) = 2^(A000120(n)+1)-1, where A000120(n) = number of nonzero bits in n.",
			"comment": [
				"Essentially the same sequence as A117973 and A001316. The latter entry has much more information. - _N. J. A. Sloane_, Jun 05 2009",
				"First differences of A159912; every other term of A038573.",
				"From _Gary W. Adamson_, Oct 16 2009: (Start)",
				"Equals Sierpinski's gasket, A047999; as an infinite lower triangular matrix",
				"* [1,2,2,2,...] as a vector. (End)",
				"a(n) is also the number of cells turned ON at n-th generation in the outward corner version of the Ulam-Warburton cellular automaton of A147562, and a(n) is also the number of Y-toothpicks added at n-th generation in the outward corner version of the Y-toothpick structure of A160120. - _David Applegate_ and _Omar E. Pol_, Jan 24 2016"
			],
			"link": [
				"David Applegate, \u003ca href=\"/A139250/a139250.anim.html\"\u003eThe movie version\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/wiki/Catalog_of_Toothpick_and_CA_Sequences_in_OEIS\"\u003eCatalog of Toothpick and Cellular Automata Sequences in the OEIS\u003c/a\u003e",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e",
				"\u003ca href=\"/index/To#toothpick\"\u003eIndex entries for sequences related to toothpick sequences\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 2^A000120(2n+1)-1 = A038573(2n+1) = 2*A038573(n)+1 = A159912(n+1) - A159912(n).",
				"a(n) = A160019(n,n). - _Philippe Deléham_, Nov 15 2011",
				"a(n) = n - Sum_{k=0..n} (-1)^binomial(n, k). - _Peter Luschny_, Jan 14 2018"
			],
			"example": [
				"From _Michael De Vlieger_, Jan 25 2016: (Start)",
				"The number n converted to binary, \"0\" represented by \".\" for better visibility of 1's, totaling the 1's and calculating the sequence:",
				"n    Binary   Total                         a(n)",
				"0 -\u003e .     -\u003e     0, thus 2^(0+1)-1 =  2-1 =  1",
				"1 -\u003e 1     -\u003e     1,   \"  2^(1+1)-1 =  4-1 =  3",
				"2 -\u003e 1.    -\u003e     1,   \"  2^(1+1)-1 =  4-1 =  3",
				"3 -\u003e 11    -\u003e     2,   \"  2^(2+1)-1 =  8-1 =  7",
				"4 -\u003e 1..   -\u003e     1,   \"  2^(1+1)-1 =  4-1 =  3",
				"5 -\u003e 1.1   -\u003e     2,   \"  2^(2+1)-1 =  8-1 =  7",
				"6 -\u003e 11.   -\u003e     2,   \"  2^(2+1)-1 =  8-1 =  7",
				"7 -\u003e 111   -\u003e     3,   \"  2^(3+1)-1 = 16-1 = 15",
				"8 -\u003e 1...  -\u003e     1,   \"  2^(1+1)-1 =  4-1 =  3",
				"9 -\u003e 1..1  -\u003e     2,   \"  2^(2+1)-1 =  8-1 =  7",
				"10-\u003e 1.1.  -\u003e     2,   \"  2^(2+1)-1 =  8-1 =  7",
				"(End)"
			],
			"mathematica": [
				"Table[2^(DigitCount[n, 2][[1]] + 1) - 1, {n, 0, 78}] (* or *)",
				"Table[2^(Total@ IntegerDigits[n, 2] + 1) - 1, {n, 0, 78}] (* _Michael De Vlieger_, Jan 25 2016 *)"
			],
			"program": [
				"(PARI) A159913(n)=2\u003c\u003cnorml2(binary(n))-1"
			],
			"xref": [
				"Rows of triangle in A038573 converge to this sequence. - _N. J. A. Sloane_, Jun 05 2009",
				"Cf. A000120, A038573, A047999, A159912, A117973, A001316, A147582, A160121."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_M. F. Hasler_, May 03 2009",
			"references": 4,
			"revision": 27,
			"time": "2018-01-14T13:56:14-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}