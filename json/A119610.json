{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A119610",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 119610,
			"data": "1,2,4,8,16,33,66,132,264,528,1057,2114,4228,8456,16912,33825,67650,135300,270600,541200,1082401,2164802,4329604,8659208,17318416,34636833,69273666,138547332,277094664,554189328,1108378657,2216757314",
			"name": "Number of cases in which the first player is killed in a Russian roulette game where 5 players use a gun with n chambers and the number of bullets can be from 1 to n. Players do not rotate the cylinder after the game starts.",
			"comment": [
				"Here we let p = 5 to produce the above sequence, but p can be an arbitrary natural number. By letting p = 2, 3, 4, 6, 7 we can produce sequences A000975, A033138, A083593, A195904 and A117302. We denote by U(p,n,m) the number of the cases in which the first player is killed in a Russian roulette game where p players use a gun with n chambers and m bullets. They never rotate the cylinder after the game starts. The chambers can be represented by the list {1,2,...,n}.",
				"We calculate the number of cases for each of the situations identified below by (0), (1), ..., (t) separately, where t = floor((n-m)/p):",
				"(0) The first player is killed when one bullet is in the first chamber and the remaining (m-1) bullets are in chambers {2,3,...,n}. We have binomial(n-1,m-1) cases for this.",
				"(1) The first player is killed when one bullet is in the (p+1)-th chamber and the rest of the bullets are in chambers {p+2,..,n}. We have binomial(n-p-1,m-1) cases for this.",
				"...",
				"(t) The first player is killed when one bullet is in the (p*t+1)-th chamber and the remaining bullets are in chambers {p*t+2,...,n}. We have binomial(n-p*t-1,m-1) cases for this.",
				"Therefore U(p,n,m) = Sum_{z=0..t} binomial(n-p*z-1,m-1), where t = floor((n-m)/p). Let A(p,n) be the number of the cases in which the first player is killed when p players use a gun with n chambers and the number of bullets can be from 1 to n. Then A(p,n) = Sum_{m=1..n} U(p,n,m)."
			],
			"reference": [
				"R. Miyadera, \"General Theory of Russian Roulette.\" Mathematica source.",
				"R. Miyadera, Mathematical Theory of Magic Fruits Archimedes-lab."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A119610/b119610.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"R. Miyadera, \u003ca href=\"http://library.wolfram.com/infocenter/MathSource/5710/\"\u003eGeneral Theory of Russian Roulette\u003c/a\u003e, Mathematica source.",
				"R. Miyadera, \u003ca href=\"http://www.archimedes-lab.org/fraction_patt/Magic_fruits.html\"\u003eInteresting patterns of fractions\u003c/a\u003e, Archimedes-lab",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,0,0,0,1,-2)."
			],
			"formula": [
				"Let p = 5 and a(n) = (2^(n + p-1) - 2^((n-1) mod p))/(2^p - 1). In the following we present the formula based on the theory we used to define our sequence as a Mathematica code. The above formula is much easier to use, but the Mathematica code has an important mathematical meaning in it.",
				"a(n) = floor(2^(n+4)/31), which is obtained by letting p=5 in the formula above.",
				"From _Joerg Arndt_, Jan 08 2011: (Start)",
				"G.f.: x / ( (x-1)*(2*x-1)*(x^4+x^3+x^2+x+1) ).",
				"a(n) = +2*a(n-1) +a(n-5) -2*a(n-6). (End)"
			],
			"example": [
				"If the number of chambers is 3, then the number of the bullets can be 1, 2, or 3. The first player is killed when one bullet is in the first chamber, and the remaining bullets are in the second and third chambers. The only cases are {{1, 0, 0}, {1, 1, 0}, {1, 0, 1}, {1, 1, 1}}, where we denote by 1 a chamber that contains a bullet. Therefore a(3) = 4."
			],
			"maple": [
				"seq(floor(2^(n+4)/31), n = 1..32); # _Mircea Merca_, Dec 22 2010"
			],
			"mathematica": [
				"U[p_,n_,m_,v_]:=Block[{t},t=Floor[(1+p-m+n-v)/p]; Sum[Binomial[n-v-p*z,m-1], {z,0,t-1}]];A[p_,n_,v_]:=Sum[U[p,n,k,v],{k,1,n}]; (* Here we let p = 5 to produce the above sequence, but this code can produce A000975, A033138, A083593, A195904, A117302 for p = 2, 3, 4, 6, 7. *) Table[B[5,n,1],{n,1,20}]",
				"CoefficientList[ Series[ 1/(2x^6 - x^5 - 2x + 1), {x, 0, 32}], x] (* or *)",
				"LinearRecurrence[{2, 0, 0, 0, 1, -2}, {1, 2, 4, 8, 16, 33}, 32] (* _Robert G. Wilson v_, Mar 12 2015 *)"
			],
			"program": [
				"(MAGMA) I:=[1,2,4,8,16,33]; [n le 6 select I[n] else 2*Self(n-1)+Self(n-5)-2*Self(n-6): n in [1..40]]; // _Vincenzo Librandi_, Mar 18 2015",
				"(PARI) for(n=1,50, print1(floor(2^(n+4)/31), \", \")) \\\\ _G. C. Greubel_, Oct 11 2017"
			],
			"xref": [
				"Cf. A000975, A033138, A083593, A195904, A117302."
			],
			"keyword": "easy,nonn,uned",
			"offset": "1,2",
			"author": "_Ryohei Miyadera_, Jun 04 2006",
			"references": 2,
			"revision": 48,
			"time": "2017-10-11T21:58:48-04:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}