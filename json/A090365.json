{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A090365",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 90365,
			"data": "1,1,3,11,47,225,1177,6625,39723,251939,1681535,11764185,86002177,655305697,5193232611,42726002123,364338045647,3215471252769,29331858429241,276224445794785,2682395337435723,26832698102762435,276221586866499839,2923468922184615897",
			"name": "Shifts 1 place left under the INVERT transform of the BINOMIAL transform of this sequence.",
			"comment": [
				"The Hankel transform of this sequence is A000178(n+1); example: det([1,1,3; 1,3,11; 3,11,47]) = 12. - _Philippe Deléham_, Mar 02 2005",
				"a(n) appears to be the number of indecomposable permutations (A003319) of [n+1] that avoid both of the dashed patterns 32-41 and 41-32. - _David Callan_, Aug 27 2014",
				"This is true: A nonempty permutation avoids 32-41 and 41-32 if and only if all its components do so. So if A(x) denotes the gf for indecomposable {32-41,41-32}-avoiders, then F(x):=1/(1-A(x)) is the gf for all {32-41,41-32}-avoiders. From A074664, F(x)=1/x(1-1/B(x)) where B(x) is the ogf for the Bell numbers. Solve for A(x). - _David Callan_, Jul 21 2017"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A090365/b090365.txt\"\u003eTable of n, a(n) for n = 0..225\u003c/a\u003e"
			],
			"formula": [
				"G.f.: A(x) satisfies A(x) = 1/(1 - A(x/(1-x))*x/(1-x) ).",
				"a(n) = Sum_{k = 0..n} A085838(n, k). - _Philippe Deléham_, Jun 04 2004",
				"G.f.: 1/x-1-1/(B(x)-1) where B(x) = g.f. for A000110 the Bell numbers. - _Vladeta Jovovic_, Aug 08 2004",
				"a(n) = Sum_{k=0..n} A094456(n,k). - _Philippe Deléham_, Nov 07 2007",
				"G.f.: 1/(1-x/(1-2x/(1-x/(1-3x/(1-x/(1-4x/(1-x/(1-5x/(1-... (continued fraction). - _Paul Barry_, Feb 25 2010",
				"G.f.: 1 - x/(G(0)+x); G(k) = x - 1 + x*k + x*(x-1+x*k)/G(k+1); (continued fraction, 1-step). - _Sergei N. Gladkovskii_, Jan 06 2012",
				"G.f.: 1/x - 1/2 + (x^2-4)/(4*U(0)-2*x^2+8) where U(k) = k*(2*k+3)*x^2 + x - 2 - (2-x+2*k*x)*(2+3*x+2*k*x)*(k+1)*x^2/U(k+1); (continued fraction, 1-step). - _Sergei N. Gladkovskii_, Sep 28 2012",
				"G.f.: 1/x+1/(U(0)-1)  where U(k) = -x*k + 1 - x - x^2*(k+1)/U(k+1); (continued fraction, 1-step). - _Sergei N. Gladkovskii_, Sep 29 2012",
				"G.f.: (1 - U(0))/x - 1 where U(k) = 1 - x*(k+2) - x^2*(k+1)/U(k+1); (continued fraction, 1-step). - _Sergei N. Gladkovskii_, Oct 11 2012",
				"G.f.: (1 - U(0))/x where U(k) = 1 - x*(k+1)/(1-x/U(k+1)); (continued fraction, 1-step). - _Sergei N. Gladkovskii_, Nov 11 2012",
				"G.f.: 1/x + 1/( G(0)-1) where G(k) = 1 - x/(1 - x*(2*k+1)/(1 - x/(1 - x*(2*k+2)/G(k+1) ))); (continued fraction). - _Sergei N. Gladkovskii_, Dec 14 2012",
				"G.f.:1/x + 1/( G(0) - 1 ) where G(k) = 1 - x/(1 - x*(k+1)/G(k+1) ); (continued fraction). - _Sergei N. Gladkovskii_, Feb 03 2013",
				"G.f.: (1 - Q(0))/x where Q(k) = 1 + x/(x*k - 1 )/Q(k+1); (continued fraction). - _Sergei N. Gladkovskii_, Feb 23 2013",
				"G.f.: 1/x - 1/x/Q(0), where Q(k) = 1 + x/(1 - x + x*(k+1)/(x - 1/Q(k+1))); (continued fraction). - _Sergei N. Gladkovskii_, May 12 2013"
			],
			"maple": [
				"bintr:= proc(p) proc(n) add(p(k) *binomial(n,k), k=0..n) end end:",
				"invtr:= proc(p) local b;",
				"           b:= proc(n) option remember; local i;",
				"                `if`(n\u003c1, 1, add(b(n-i) *p(i-1), i=1..n+1))",
				"               end;",
				"        end:",
				"b:= invtr(bintr(a)):",
				"a:= n-\u003e `if`(n\u003c0, 0, b(n-1)):",
				"seq(a(n), n=0..30);  # _Alois P. Heinz_, Jun 28 2012"
			],
			"mathematica": [
				"a[n_] := Module[{A, B}, A = 1+x; For[k=1, k \u003c= n, k++, B = (A /. x -\u003e x/(1 - x))/(1-x) + O[x]^n // Normal; A = 1 + x*A*B]; SeriesCoefficient[A, {x, 0, n}]]; Table[a[n], {n, 0, 23}] (* _Jean-François Alcover_, Oct 23 2016, adapted from PARI *)"
			],
			"program": [
				"(PARI) {a(n)=local(A); if(n\u003c0,0,A=1+x+x*O(x^n); for(k=1,n,B=subst(A,x, x/(1-x))/(1-x)+x*O(x^n); A=1+x*A*B);polcoeff(A,n,x))}"
			],
			"xref": [
				"Cf. A090366, A090367.",
				"Cf. A074664."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Paul D. Hanna_, Nov 26 2003",
			"references": 6,
			"revision": 72,
			"time": "2020-01-22T02:44:41-05:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}