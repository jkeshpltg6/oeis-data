{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A198338",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 198338,
			"data": "1,1,2,1,2,3,1,2,2,4,1,2,3,5,1,2,2,3,4,6,1,2,3,3,7,1,2,2,2,4,4,4,8,1,2,2,3,3,4,6,6,9,1,2,2,3,4,5,6,10,1,2,3,5,11,1,2,2,2,3,4,4,4,6,6,8,12,1,2,3,3,4,6,6,7,15,1,2,2,3,3,4,5,6,6",
			"name": "Irregular triangle read by rows: row n is the sequence of Matula numbers of the rooted subtrees of the rooted tree with Matula-Goebel number n. A root subtree of a rooted tree T is a subtree of T containing the root.",
			"comment": [
				"The Matula-Goebel number of a rooted tree can be defined in the following recursive manner: to the one-vertex tree there corresponds the number 1; to a tree T with root degree 1 there corresponds the t-th prime number, where t is the Matula-Goebel number of the tree obtained from T by deleting the edge emanating from the root; to a tree T with root degree m\u003e=2 there corresponds the product of the Matula-Goebel numbers of the m branches of T.",
				"Number of entries in row n is A184160(n). Row n\u003e=2 can be easily identified: it starts with the entry following the first occurrence of n-1 and it ends with the first occurrence of n."
			],
			"reference": [
				"F. Goebel, On a 1-1-correspondence between rooted trees and natural numbers, J. Combin. Theory, B 29 (1980), 141-143.",
				"I. Gutman and A. Ivic, On Matula numbers, Discrete Math., 150, 1996, 131-142.",
				"I. Gutman and Yeong-Nan Yeh, Deducing properties of trees from their Matula numbers, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"D. W. Matula, A natural rooted tree enumeration by prime factorization, SIAM Review, 10, 1968, 273."
			],
			"link": [
				"E. Deutsch, \u003ca href=\"http://arxiv.org/abs/1111.4288\"\u003e Rooted tree statistics from Matula numbers\u003c/a\u003e, arXiv:1111.4288"
			],
			"formula": [
				"Row 1 is [1]; if n = p(t) (= the t-th prime), then row n is [1, p(a), p(b), ... ], where [a,b,...] is row t; if n=rs (r,s \u003e=2), then row n consists of the numbers r[i]*s[j], where [r[1], r[2],...] is row r and [s[1], s[2], ...] is row s. The Maple program, based on this recursive procedure, yields row n (\u003c=1000; adjustable) with the command MRST(n)."
			],
			"example": [
				"Row 4 is [1,2,2,4] because the rooted tree with Matula-Goebel number 4 is V and its root subtrees are *, |, |, and V.",
				"Triangle starts:",
				"1;",
				"1,2;",
				"1,2,3;",
				"1,2,2,4;",
				"1,2,3,5;",
				"1,2,2,3,4,6;"
			],
			"maple": [
				"with(numtheory): MRST := proc (n) local r, s: r := proc (n) options operator, arrow: op(1, factorset(n)) end proc: s := proc (n) options operator, arrow; n/r(n) end proc: if n = 1 then [1] elif bigomega(n) = 1 then sort([1, seq(ithprime(mrst[pi(n)][i]), i = 1 .. nops(mrst[pi(n)]))]) else sort([seq(seq(mrst[r(n)][i]*mrst[s(n)][j], i = 1 .. nops(mrst[r(n)])), j = 1 .. nops(mrst[s(n)]))]) end if end proc: for n to 1000 do mrst[n] := MRST(n) end do;"
			],
			"xref": [
				"Cf. A198339."
			],
			"keyword": "nonn,tabf",
			"offset": "1,3",
			"author": "_Emeric Deutsch_, Dec 04 2011",
			"references": 1,
			"revision": 9,
			"time": "2013-01-07T13:05:55-05:00",
			"created": "2011-12-04T15:15:05-05:00"
		}
	]
}