{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A229913",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 229913,
			"data": "2,4,4,8,6,7,8,9,10,12,16,6,8,9,10",
			"name": "Irregular triangle in which row n lists the possible sizes of n-qubit unextendible product bases.",
			"comment": [
				"An unextendible product basis (UPB) is a set of mutually orthogonal product states such that there is no product state orthogonal to every member of the set. An n-qubit UPB is a UPB on the space C^2 tensored with itself n times, where C is the field of complex numbers.",
				"Row n also gives the values of m such that there exists an n X m matrix M with the following three properties: (1) every entry of M is a nonzero integer; (2) the sum of any two columns of M contains a 0 entry; and (3) there is no way to append an (m+1)st column to M so that M still has property (2).",
				"The first entry in row n is A211390(n).",
				"The last entry in row n is 2^n.",
				"The next-to-last entry in row n is 2^n - 4 (for n \u003e= 3)."
			],
			"link": [
				"R. Augusiak, T. Fritz, M. Kotowski, M. Kotowski, M. Pawlowski, M. Lewenstein, and A. Acín, \u003ca href=\"http://arxiv.org/abs/1112.3238\"\u003eTight Bell inequalities with no quantum violation from qubit unextendible product bases\u003c/a\u003e. arXiv:1112.3238 [quant-ph], 2011-2012.",
				"R. Augusiak, T. Fritz, M. Kotowski, M. Kotowski, M. Pawlowski, M. Lewenstein, and A. Acín, \u003ca href=\"http://dx.doi.org/10.1103/PhysRevA.85.042113\"\u003eTight Bell inequalities with no quantum violation from qubit unextendible product bases\u003c/a\u003e. Phys. Rev. A, 85:042113, 2012.",
				"D. P. DiVincenzo, T. Mor, P. W. Shor, J. A. Smolin, and B. M. Terhal, \u003ca href=\"http://arxiv.org/abs/quant-ph/9908070\"\u003eUnextendible product bases, uncompletable product bases and bound entanglement\u003c/a\u003e, arXiv:quant-ph/9908070, 1999-2000.",
				"D. P. DiVincenzo, T. Mor, P. W. Shor, J. A. Smolin, and B. M. Terhal, \u003ca href=\"http://dx.doi.org/10.1007/s00220-003-0877-6\"\u003eUnextendible product bases, uncompletable product bases and bound entanglement\u003c/a\u003e, Commun. Math. Phys., 238:379-410, 2003.",
				"N. Johnston, \u003ca href=\"http://www.njohnston.ca/2013/10/in-search-of-a-4-by-11-matrix/\"\u003eIn Search of a 4-by-11 Matrix\u003c/a\u003e",
				"N. Johnston, \u003ca href=\"http://arxiv.org/abs/1401.7920\"\u003eThe Structure of Qubit Unextendible Product Bases\u003c/a\u003e, arXiv:1401.7920 [quant-ph], 2014."
			],
			"example": [
				"Triangle begins:",
				"2",
				"4",
				"4 8",
				"6 7 8 9 10 12 16",
				"6 8 9 10",
				"The 5th row of the triangle also contains 12--26, 28, and 32, but it is unknown if it contains 11 or 27.",
				"The 3rd row of the triangle contains the value 4 because there is a 3-qubit unextendible product basis consisting of 4 states. If we use \"ket\" notation from quantum mechanics, then one such UPB is: |0\u003e|0\u003e|0\u003e, |+\u003e|1\u003e|-\u003e, |1\u003e|-\u003e|+\u003e, |-\u003e|+\u003e|1\u003e. This is the \"shifts\" UPB from the DiVincenzo et al. paper.",
				"Equivalently, the 3rd row of the triangle contains the value 4 because there is a 3 X 4 matrix M with the three properties given in the Comments section:",
				"1 -1  2 -2",
				"1 -2 -1  2",
				"1  2 -2 -1"
			],
			"xref": [
				"Cf. A211390."
			],
			"keyword": "nonn,tabf,hard,more",
			"offset": "1,1",
			"author": "_Nathaniel Johnston_, Oct 03 2013",
			"ext": [
				"a(10)-a(15) from _Nathaniel Johnston_, Jan 30 2014"
			],
			"references": 1,
			"revision": 17,
			"time": "2014-12-03T05:24:08-05:00",
			"created": "2013-10-05T10:28:43-04:00"
		}
	]
}