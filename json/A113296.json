{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A113296",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 113296,
			"data": "1,1,2,6,48,720,34560,3628800,1393459200,1316818944000,5056584744960000,52563198423859200000,2422112183371431936000000,327312129899898454671360000000,211155601241022491077587763200000000",
			"name": "Cumulative product of double factorial A006882.",
			"link": [
				"Harvey P. Dale, \u003ca href=\"/A113296/b113296.txt\"\u003eTable of n, a(n) for n = 0..57\u003c/a\u003e",
				"Alejandro H. Morales, Igor Pak and Greta Panova, \u003ca href=\"https://doi.org/10.5802/alco.67\"\u003eHook formulas for skew shapes III. Multivariate and product formulas\u003c/a\u003e, Algebraic Combinatorics, Vol. 2, No. 5 (2019), pp. 815-861; \u003ca href=\"https://arxiv.org/abs/1707.00931\"\u003earXiv preprint\u003c/a\u003e, arXiv:1707.00931 [math.CO], 2017-2020.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DoubleFactorial.html\"\u003eDouble Factorial\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Glaisher-KinkelinConstant.html\"\u003eGlaisher-Kinkelin Constant\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/BarnesG-Function.html\"\u003eBarnes G-Function\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Barnes_G-function\"\u003eBarnes G-function\u003c/a\u003e."
			],
			"formula": [
				"a(n) = Product_{k=0..n} k!!.",
				"a(n) = n!! * a(n-1) where a(0) = 0, a(1) = 1 and n \u003e= 2.",
				"a(n) = n*(n-2)!! * a(n-1) where a(0) = 0, a(1) = 1 and n \u003e= 2.",
				"a(n) = 2^((6*n^2+12*n+2-3*(-1)^n)/24) * Pi^(((-1)^n-2*n-3)/8) * exp(-1/8) * A^(3/2) * G((2n+7+(-1)^n)/4) * G((2n+7-(-1)^n)/4), where A is the Glaisher-Kinkelin constant (A074962), G(x) is the Barnes G-function. - _Vladimir Reshetnikov_, Nov 11 2015",
				"Sum_{n\u003e=0} 1/a(n) = 1/A137989. - _Amiram Eldar_, Nov 09 2020",
				"Sum_{n\u003e=0} (-1)^n/a(n) = A137988. - _Amiram Eldar_, Apr 12 2021"
			],
			"example": [
				"a(10) = 1!! * 2!! * 3!! * 4!! * 5!! * 6!! * 7!! * 8!! * 9!! * 10!!",
				"= 1 * 2 * 3 * 8 * 15 * 48 * 105 * 384 * 945 * 3840",
				"= 5056584744960000 = 2^23 x 3^9 x 5^4 x 7^2."
			],
			"mathematica": [
				"Table[Product[k!!,{k,0,n}],{n,0,20}] (* _Vaclav Kotesovec_, Jul 17 2015 *)",
				"Table[2^((6n^2+12n+2-3(-1)^n)/24) Pi^(((-1)^n-2n-3)/8) Exp[-1/8] Glaisher^(3/2) BarnesG[(2n+7+(-1)^n)/4] BarnesG[(2n+7-(-1)^n)/4], {n, 0, 20}] (* _Vladimir Reshetnikov_, Nov 11 2015 *)",
				"FoldList[Times,Range[0,20]!!] (* _Harvey P. Dale_, Oct 29 2019 *)"
			],
			"xref": [
				"Cf. A006882, A074962, A137988, A137989."
			],
			"keyword": "easy,nonn",
			"offset": "0,3",
			"author": "_Jonathan Vos Post_, Feb 18 2006",
			"references": 1,
			"revision": 37,
			"time": "2021-04-12T04:38:13-04:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}