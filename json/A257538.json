{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A257538",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 257538,
			"data": "1,3,11,9,127,33,83,27,121,381,5381,99,773,249,1397,81,3001,363,563,1143,913,16143,4943,297,16129,2319,1331,747,23563,4191,648391,243,59191,9003,10541,1089,3761,1689,8503,3429,57943,2739,13297,48429",
			"name": "The Matula number of the rooted tree obtained from the rooted tree T having Matula number n by replacing each edge of T with a path of length 2.",
			"comment": [
				"The Matula (or Matula-Goebel) number of a rooted tree can be defined in the following recursive manner: to the one-vertex tree there corresponds the number 1; to a tree T with root degree 1 there corresponds the t-th prime number, where t is the Matula  number of the tree obtained from T by deleting the edge emanating from the root; to a tree T with root degree m\u003e=2 there corresponds the product of the Matula numbers of the m branches of T.",
				"Fully multiplicative with a(prime(n)) = prime(prime(a(n))). - _Antti Karttunen_, Mar 09 2017"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A257538/b257538.txt\"\u003eTable of n, a(n) for n = 1..708\u003c/a\u003e",
				"E. Deutsch, \u003ca href=\"http://dx.doi.org/10.1016/j.dam.2012.05.012\"\u003eRooted tree statistics from Matula numbers\u003c/a\u003e, Discrete Appl. Math., 160, 2012, 2314-2322.",
				"F. Goebel, \u003ca href=\"http://dx.doi.org/10.1016/0095-8956(80)90049-0\"\u003eOn a 1-1-correspondence between rooted trees and natural numbers\u003c/a\u003e, J. Combin. Theory, B 29 (1980), 141-143.",
				"I. Gutman and A. Ivic, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(95)00182-V\"\u003eOn Matula numbers\u003c/a\u003e, Discrete Math., 150, 1996, 131-142.",
				"I. Gutman and Yeong-Nan Yeh, \u003ca href=\"http://www.emis.de/journals/PIMB/067/3.html\"\u003eDeducing properties of trees from their Matula numbers\u003c/a\u003e, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"D. W. Matula, \u003ca href=\"http://www.jstor.org/stable/2027327\"\u003eA natural rooted tree enumeration by prime factorization\u003c/a\u003e, SIAM Rev. 10 (1968) 273.",
				"\u003ca href=\"/index/Di#divseq\"\u003eIndex to divisibility sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Mat#matula\"\u003eIndex entries for sequences related to Matula-Goebel numbers\u003c/a\u003e"
			],
			"formula": [
				"Let p(n) denote the n-th prime (= A000040(n)). We have the recursive equations: a(p(n)) = p(p(a(n))), a(rs) = a(r)a(s), a(1) = 1. The Maple program is based on this.",
				"From _Antti Karttunen_, Mar 09 2017: (Start)",
				"a(1) = 1; for n\u003e1, a(n) = A000040(A000040(a(A055396(n)))) * a(A032742(n)).",
				"A046523(a(n)) = A046523(n). [Preserves the prime-signature of n].",
				"(End)"
			],
			"example": [
				"a(3)=11; indeed, 3 is the Matula number of the path of length 2 and 11 is the Matula number of the path of length 4."
			],
			"maple": [
				"with(numtheory): a := proc (n) local r, s: r := proc (n) options operator, arrow: op(1, factorset(n)) end proc: s := proc (n) options operator, arrow: n/r(n) end proc: if n = 1 then 1 elif bigomega(n) = 1 then ithprime(ithprime(a(pi(n)))) else a(r(n))*a(s(n)) end if end proc: seq(a(n), n = 1 .. 60);"
			],
			"program": [
				"A257538(n) = { my(f = factor(n)); for (i=1, #f~, f[i, 1] = prime(prime(A257538(primepi(f[i, 1]))))); factorback(f); }; \\\\ Nonmemoized implementation by _Antti Karttunen_, Mar 09 2017",
				"(Scheme, with memoization-macro definec)",
				"(definec (A257538 n) (cond ((= 1 n) 1) (else (* (A000040 (A000040 (A257538 (A055396 n)))) (A257538 (A032742 n))))))",
				";; _Antti Karttunen_, Mar 09 2017"
			],
			"xref": [
				"Cf. A000040, A000720, A006450, A032742, A046523, A049084, A055396."
			],
			"keyword": "nonn,mult",
			"offset": "1,2",
			"author": "_Emeric Deutsch_, May 01 2015",
			"ext": [
				"Formula corrected by _Antti Karttunen_, Mar 09 2017"
			],
			"references": 3,
			"revision": 28,
			"time": "2017-03-30T22:05:04-04:00",
			"created": "2015-05-02T06:57:39-04:00"
		}
	]
}