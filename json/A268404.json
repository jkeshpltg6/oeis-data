{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A268404",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 268404,
			"data": "1,5,111,7943,1890403,1562052227,4617328590967,49605487608825311,1951842619769780119767,282220061839181920696642671,150134849621798165832163223922131,293909551918134914019004192289440616787,2116817972794640259940977362779552773322908743",
			"name": "Number of fixed polyominoes that have a width and height of n.",
			"comment": [
				"Iwan Jensen originally provided this sequence.",
				"The sequence also describes the water patterns of lakes in the water retention model.",
				"A lake is defined as a body of water with dimensions of n X n when the size of the square is (n+2) X (n+2). All other bodies of water are ponds.",
				"The 3 X 3 square serves as a tutorial for the following three nomenclatures: 1) The total number of unique water patterns equals 102 and includes lakes and ponds.  2) The number of free lake-type polyominoes equals 24.  3) The number of fixed lake-type polyominoes equals 111.  See the explanatory graphics in the link section.",
				"John Mason has looked at free polyominoes in rectangles A268371.",
				"Anna Skelt initiated the discussion on the definition of a lake."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A268404/b268404.txt\"\u003eTable of n, a(n) for n = 1..15\u003c/a\u003e",
				"Craig Knecht, \u003ca href=\"/A268404/a268404_2.jpg\"\u003e4x4 minimal lake area patterns\u003c/a\u003e",
				"Craig Knecht, \u003ca href=\"/A268404/a268404_1.jpg\"\u003e5x5 minimal lake area patterns\u003c/a\u003e",
				"Craig Knecht, \u003ca href=\"/A268404/a268404_2.png\"\u003e6x6 minimal lake area patterns\u003c/a\u003e",
				"Craig Knecht, \u003ca href=\"/A268404/a268404_3.png\"\u003e7x7 minimal lake area patterns\u003c/a\u003e",
				"Craig Knecht, \u003ca href=\"/A268404/a268404_4.png\"\u003e24 free lake-type polyominoes 3x3\u003c/a\u003e",
				"Craig Knecht, \u003ca href=\"/A268311/a268311.pdf\"\u003ePolyominoe enumeration\u003c/a\u003e",
				"Craig Knecht, \u003ca href=\"/A268404/a268404.png\"\u003eWalter Trump's 111 fixed lake-type polyominoes 3x3\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Water retention on mathematical surfaces\"\u003eWater Retention on Mathematical Surfaces\u003c/a\u003e"
			],
			"example": [
				"There are many interesting ways to connect all boundaries of the square with the fewest number of edge-joined cells.",
				"0 0 0 0 1 0",
				"0 0 0 0 1 1",
				"0 0 1 1 1 0",
				"0 0 1 0 0 0",
				"1 1 1 0 0 0",
				"0 1 0 0 0 0"
			],
			"mathematica": [
				"A292357 = Cases[Import[\"https://oeis.org/A292357/b292357.txt\", \"Table\"], {_, _}][[All, 2]];",
				"a[n_] := A292357[[2n^2 - 2n + 1]];",
				"Array[a, 15] (* _Jean-François Alcover_, Sep 10 2019 *)"
			],
			"xref": [
				"Main diagonal of A292357.",
				"Cf. A054247 (all unique water retention patterns for an n X n square), A268311 (free polyominoes that connect all boundaries on a square), A268339 (lake patterns that are invariant to all transformations)."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Craig Knecht_, Feb 03 2016",
			"ext": [
				"a(12)-a(13) from _Andrew Howroyd_, Oct 02 2017"
			],
			"references": 5,
			"revision": 48,
			"time": "2019-09-10T09:14:40-04:00",
			"created": "2016-04-04T16:02:23-04:00"
		}
	]
}