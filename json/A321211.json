{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A321211",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 321211,
			"data": "1,2,3,3,4,4,5,4,6,6,6,7,7,7,8,7,8,9,9,9,9,8,10,9,10,11,11,11,11,12,12,12,11,12,11,12,13,13,13,14,14,14,13,14,14,14,15,14,14,12,14,15,14,15,16,17,17,16,16,16,16,17,16,16,17,17,16,16,15,17,19",
			"name": "Let S be the sequence of integer sets defined by these rules: S(1) = {1}, and for any n \u003e 1, S(n) = {n} U S(pi(n)) U S(n - pi(n)) (where X U Y denotes the union of the sets X and Y and pi is the prime counting function); a(n) = the number of elements of S(n).",
			"comment": [
				"The prime counting function corresponds to A000720.",
				"This sequence has similarities with A294991; a(n) gives approximately the number of intermediate terms to consider in order to compute A316434(n) using the formula of its definition."
			],
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A321211/b321211.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A321211/a321211_1.png\"\u003eIllustration of a(42)\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A321211/a321211.png\"\u003eDensity plot of the first 100000000 terms\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A321211/a321211.txt\"\u003eC++ program for A321211\u003c/a\u003e"
			],
			"example": [
				"The first terms, alongside pi(n) and S(n), are:",
				"  n   a(n)  pi(n)  S(n)",
				"  --  ----  -----  ----------------------",
				"   1     1      0  {1}",
				"   2     2      1  {1, 2}",
				"   3     3      2  {1, 2, 3}",
				"   4     3      2  {1, 2, 4}",
				"   5     4      3  {1, 2, 3, 5}",
				"   6     4      3  {1, 2, 3, 6}",
				"   7     5      4  {1, 2, 3, 4, 7}",
				"   8     4      4  {1, 2, 4, 8}",
				"   9     6      4  {1, 2, 3, 4, 5, 9}",
				"  10     6      4  {1, 2, 3, 4, 6, 10}",
				"  11     6      5  {1, 2, 3, 5, 6, 11}",
				"  12     7      5  {1, 2, 3, 4, 5, 7, 12}"
			],
			"program": [
				"(C++) See Links section.",
				"(PARI) a(n) = my (v=Set([-1, -n]), i=1); while (v[i]!=-1, my (pi=primepi(-v[i])); v=setunion(v, Set([v[i]+pi, -pi])); i++); #v"
			],
			"xref": [
				"Cf. A000720, A294991, A316434."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Altug Alkan_ and _Rémy Sigrist_, Oct 31 2018",
			"references": 1,
			"revision": 16,
			"time": "2018-11-04T18:23:52-05:00",
			"created": "2018-11-04T14:44:37-05:00"
		}
	]
}