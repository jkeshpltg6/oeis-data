{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A307092",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 307092,
			"data": "0,1,2,2,3,2,3,3,4,2,3,3,4,4,5,4,5,2,3,3,4,4,5,4,5,5,6,5,6,3,4,5,6,2,3,3,4,4,5,4,5,3,4,5,6,6,7,5,6,6,7,6,7,7,8,4,5,6,7,4,5,5,6,6,7,2,3,3,4,4,5,4,5,5,6,5,6,6,7,5,6,6,7,3,4,5,6,6,7,5,6,7,8,8,9,6,7,7,8,7",
			"name": "a(n) is the minimum number of iterations of the form x -\u003e x + x^j (where j is a nonnegative integer and need not be identical in each iteration) required to reach n starting from 1.",
			"comment": [
				"The background of this sequence is an \"execute-summon\" problem in Minecraft command system ver. 1.13+. In Minecraft v1.13+, if you just summon an entity using the \"summon\" command, you get one more. However, when you combine the \"summon\" command with nested \"execute\" commands that target all entities, you will get x^j more entities, where x is the number of entities before the command and j is the number of times the command is nested. To obtain a given number of entities, we are interested in the minimal number of iterations using such commands. The minimal number of iterations to get n is the n-th term of this sequence. [Clarified by _Peter Kagey_, Aug 24 2019]",
				"From _Peter Kagey_ and Chris Quisling, Aug 22 2019: (Start)",
				"In Minecraft there are several commands, such as /summon, which summons a creature, and /execute which can be combined with other commands to behave like a for-loop.",
				"For example, running \"/summon minecraft:cat\" places a cat into the game, and running \"/execute at @e run summon minecraft:cat\" places one cat into the game for every creature in the game.",
				"If there are x creatures in the game, nesting the \"execute\" command k times has the effect of creating x^k new creatures, resulting in a total of x + x^k creatures.",
				"For example, running \"/execute at @e run execute at @e run summon minecraft:cat\" places x^2 new creatures into the game.",
				"(End)",
				"a(n) \u003c= 2*A000523(n), as we can always get from floor(n/2) to n by applying the map(s) x -\u003e x + x (and x -\u003e x + 1 if n is odd). - _Ely Golden_, Aug 19 2020"
			],
			"link": [
				"Peter Kagey, \u003ca href=\"/A307092/b307092.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Ely Golden, \u003ca href=\"/A307092/a307092_1.py.txt\"\u003ePython program for generating terms of this sequence\u003c/a\u003e",
				"Minecraft Wiki, \u003ca href=\"https://minecraft.gamepedia.com/Commands/execute\"\u003eExecute command\u003c/a\u003e"
			],
			"example": [
				"For n = 43, a(43) = 4 because we can reach 43 by the four iterations below, but not in less iterations:",
				"- 1 -\u003e 2 by setting j=0, 1 + 1^0 = 2,",
				"- 2 -\u003e 6 by setting j=2, 2 + 2^2 = 6,",
				"- 6 -\u003e 42 by setting j=2, 6 + 6^2 = 42,",
				"- 42 -\u003e 43 by setting j=0, 42 + 42^0 = 43.",
				"From _Peter Kagey_, Aug 22 2019: (Start)",
				"So if there is exactly one creature in the game, running the following four commands will result in 43 creatures in the game:",
				"/summon minecraft:cat",
				"/execute at @e run execute at @e run summon minecraft:cat",
				"/execute at @e run execute at @e run summon minecraft:cat",
				"/summon minecraft:cat",
				"Which have 0, 2, 2, and 0 nested \"execute\" commands respectively, and four is the fewest number of commands that can be run to create exactly 43 creatures in the game.",
				"(End)"
			],
			"mathematica": [
				"(* To get more terms of the sequence, increase terms, maxa and maxx, and then set maxi=trunc(lb(maxx)) *)",
				"maxi=16; maxx=65536; maxa=10; terms=100;",
				"a = NestList[",
				"  Function[list,",
				"   DeleteDuplicates[",
				"    Join[list,",
				"     Flatten[Table[If[# + #^i \u003c= maxx, # + #^i, 1], {i, 0, maxi}] \u0026 /@",
				"       list]]]], {1}, maxa];",
				"b = Prepend[Table[Complement[a[[i + 1]], a[[i]]], {i, Length[a] - 1}],",
				"   First[a]];",
				"c = SparseArray[",
				"   Flatten[b] -\u003e",
				"    Flatten[Table[",
				"      ConstantArray[i, Length[b[[i]]]], {i, Length[b]}]]] // Normal;",
				"Take[c, terms] - 1"
			],
			"xref": [
				"Cf. A000523, A307074."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Yancheng Lu_, Mar 24 2019",
			"references": 6,
			"revision": 56,
			"time": "2021-01-11T22:59:56-05:00",
			"created": "2019-06-25T23:47:25-04:00"
		}
	]
}