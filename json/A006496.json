{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006496",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6496,
			"id": "M0933",
			"data": "0,2,4,-2,-24,-38,44,278,336,-718,-3116,-2642,10296,33802,16124,-136762,-354144,-24478,1721764,3565918,-1476984,-20783558,-34182196,35553398,242017776,306268562,-597551756,-2726446322,-2465133864,8701963882,29729597084,15949374758",
			"name": "Imaginary part of (1+2i)^n.",
			"comment": [
				"The absolute values of these numbers are the even numbers x such that x^2 + y^2 = 5^n with x and y coprime. See A098122. - _T. D. Noe_, Apr 14 2011"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A006496/b006496.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"George Berzsenyi, \u003ca href=\"http://www.fq.math.ca/Scanned/15-3/berzsenyi.pdf\"\u003eGaussian Fibonacci numbers\u003c/a\u003e, Fib. Quart., Vol. 15, No. 3 (1977), pp. 233-236.",
				"\u003ca href=\"/index/Ga#gaussians\"\u003eIndex entries for Gaussian integers and primes\u003c/a\u003e.",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,-5)."
			],
			"formula": [
				"a(n) = 2*a(n-1) - 5*a(n-2); a(0)=0, a(1)=2. - _T. D. Noe_, Nov 09 2006",
				"a(n) = - [M^n]_1,2, where M = [1, -2; 2, 1]. - _Simone Severini_, Apr 25 2007",
				"A000351(n) = A006495(n)^2 + a(n)^2. - _Fabrice Baubet_, May 28 2007",
				"From _R. J. Mathar_, Apr 06 2008: (Start)",
				"O.g.f.: 2*x/(1 - 2*x + 5*x^2).",
				"a(n) = 2*A045873(n). (End)",
				"a(n) = (1/2)*i*(1-2*i)^n - (1/2)*i*(1+2*i)^n, with n\u003e=0 and i=sqrt(-1). - _Paolo P. Lava_, Oct 03 2008",
				"E.g.f.: exp(x)*sin(2*x). - _Sergei N. Gladkovskii_, Jul 22 2012",
				"a(n)/A006495(n) = -tan(2*n*arctan(phi)), where phi is the golden ratio (A001622). - _Amiram Eldar_, Jan 13 2022"
			],
			"mathematica": [
				"LinearRecurrence[{2,-5},{0,2},30] (* _Vincenzo Librandi_, Dec 21 2011 *)"
			],
			"program": [
				"(MAGMA) I:=[0,2]; [n le 2 select I[n] else 2*Self(n-1)-5*Self(n-2): n in [1..40]]; // _Vincenzo Librandi_, Dec 21 2011",
				"(PARI) a(n)=([1, -2; 2, 1]^n)[1,2] \\\\ _Charles R Greathouse IV_, Dec 22 2011"
			],
			"xref": [
				"Cf. A000351, A001622, A006495, A098122, A045873."
			],
			"keyword": "sign,easy,changed",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Signs from _Christian G. Bower_, Nov 15 1998",
				"Corrected by _T. D. Noe_, Nov 09 2006",
				"More terms from _R. J. Mathar_, Apr 06 2008"
			],
			"references": 9,
			"revision": 64,
			"time": "2022-01-13T09:59:54-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}