{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A230277",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 230277,
			"data": "1,2,0,-4,8,0,0,8,0,16,0,0,-10,0,0,-16,-16,0,0,-32,0,0,0,0,39,-20,0,0,-40,0,0,32,0,-32,0,0,-70,0,0,64,80,0,0,0,0,0,0,0,49,78,0,40,56,0,0,0,0,-80,0,0,-22,0,0,-64,-80,0,0,64,0,0,0,0,110",
			"name": "Expansion of f(-q^6)^6 * ( q * chi(-q^3)^4 + 2*q^2 / chi(-q^3)^4 ) in powers of q where chi(), f() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A230277/b230277.txt\"\u003eTable of n, a(n) for n = 1..2500\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of (eta(q^3)^2 * eta(q^6))^2 + 2 * (eta(q^6)^5 / eta(q^3)^2)^2 in powers of q.",
				"a(n) is multiplicative with a(2^e) = -(-2)^e if e\u003e0,  a(3^e) = 0^e, a(p^e) = p^e * (1 + (-1)^e)/2 if p == 3, 7 (mod 8), a(p^e) = a(p)*a(p^(e-1)) - p^2*a(p^(e-2)) and a(p) = t * (-1)^(t mod 3) if p == 1, 5 (mod 8) where t = 2 * if( p == 5 (mod 6) then 4*x*y else p - 8*y^2 ) and p = x^2 + 4*y^2.",
				"a(2*n) = -2 * a(n). a(3*n) = a(4*n + 3) = 0.",
				"a(3*n + 1) = A230280(n). a(3*n + 2) = 2 * A230278(n)."
			],
			"example": [
				"G.f. = q + 2*q^2 - 4*q^4 + 8*q^5 + 8*q^8 + 16*q^10 - 10*q^13 - 16*q^16 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ With[{t = QPochhammer[ q^3, q^6]^4}, QPochhammer[ q^6]^6 (q t + 2 q^2 / t)], {q, 0, n}]"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c1, 0, n--; A = x * O(x^n); polcoeff( (eta(x^3 + A)^2 * eta(x^6 + A))^2 + 2 * x * (eta(x^6 + A)^5 / eta(x^3 + A)^2)^2, n))}",
				"(PARI) {a(n) = local(A, p, e, x, y, a0, a1); if( n\u003c1, 0, A = factor( n); prod( k=1, matsize(A)[1], if( p = A[k, 1], e = A[k, 2]; if( p==3, 0, if( p==2, -(-2)^e, if( p%8==3 || p%8==7, if(e%2, 0, p^e), for( i = 1, sqrtint( p\\4), if( issquare( p - 4*i^2, \u0026x), y = i; break)); y = 2 * if( p%6 == 5, 4*x*y, p - 8*y^2); a1 = y = if( y%3==2, y, -y); a0 = 1; for( i=2, e, x = y*a1 - p^2*a0; a0 = a1; a1 = x); a1))))))}"
			],
			"xref": [
				"Cf. A230278, A230280."
			],
			"keyword": "sign,mult",
			"offset": "1,2",
			"author": "_Michael Somos_, Oct 15 2013",
			"references": 3,
			"revision": 10,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2013-10-15T11:37:25-04:00"
		}
	]
}