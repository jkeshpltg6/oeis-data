{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A330492",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 330492,
			"data": "0,0,1,0,2,0,3,4,4,0,5,0,6,8,7,0,8,0,9,12,10,0,11,16,12,16,13,0,14,0,15,20,16,24,17,0,18,24,19,0,20,0,21,28,22,0,23,36,24,32,25,0,26,40,27,36,28,0,29,0,30,40,31,48,32,0,33,44,34,0,35,0,36",
			"name": "a(n) = sum of second differences of the sorted divisors of n.",
			"comment": [
				"The sums of the first differences of the divisors of n are given by the sequence b(n) = n - 1.",
				"Let the set {D(i)} = {d(i + 1) - d(i)} where the d(i) are the divisors of an integer m listed in ascending order with i = 1, 2 , ..., tau(n)-1. The sequence is given by a(n) = Sum_{k = 1..tau(n)-2} (D(k + 1) - D(k))."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A330492/b330492.txt\"\u003eTable of n, a(n) for n = 2..8191\u003c/a\u003e",
				"Antti Karttunen, \u003ca href=\"/A330492/a330492.txt\"\u003eData supplement: n, a(n) computed for n = 2..65537\u003c/a\u003e"
			],
			"formula": [
				"a(n) = d(tau(n)) - d(tau(n) - 1) + d(1) - d(2) where d(i) are the divisors of n.",
				"a(prime(n)) = 0 and a(2k) = k-1, k = 1, 2, ...",
				"a(p^2) = (p-1)^2 if p prime, with the generalization a(p^m) = (p-1)(p^(m-1) - 1).",
				"a(n) = (n/p-1)*(p-1), where p is the least prime factor of n. - _Nathaniel Gregg_, Apr 04 2021"
			],
			"example": [
				"a(12) = 5 because the divisors of 12 are {1, 2, 3, 4, 6, 12} and {D(i)} = {d(i+1)-d(i)} ={1, 1, 1, 2, 6}, Sum_{D(i), i = 1..4} {D(i+1)-D(i)} = 0 + 0 + 1 + 4 = 5."
			],
			"maple": [
				"with(numtheory):nn:=100:",
				"for n from 2 to nn do:",
				"d:=divisors(n):n0:=nops(d):T:=array(1..n0-1,[0$n0-1]):",
				"  for j from 1 to n0-1 do:",
				"   T[j]:=d[j+1]-d[j]:",
				"  od:",
				"   s:=sum(‘T[i+1]-T[i] ’,‘i’=1..n0-2): printf(`%d, `,s):",
				"od:",
				"*** alternative program using the formula ***",
				"with(numtheory):nn:=100:",
				"for n from 2 to nn do:",
				"d:=divisors(n):t:=tau(n):s:=d[t]-d[t-1]+d[1]-d[2] :",
				"  printf(`%d, `,s):",
				"od:"
			],
			"mathematica": [
				"Array[Total@ Differences[Divisors@ #, 2] \u0026, 73, 2] (* _Michael De Vlieger_, Dec 16 2019 *)"
			],
			"program": [
				"(PARI) a(n) = my(d=divisors(n)); d[#d] - d[#d-1] + d[1] - d[2]; \\\\ _Michel Marcus_, Feb 05 2020",
				"(Python)",
				"from sympy import primefactors",
				"def a(n): p = primefactors(n)[0]; return (n//p - 1) * (p - 1)",
				"print([a(n) for n in range(2, 75)]) # _Michael S. Branicky_, Apr 04 2021"
			],
			"xref": [
				"Cf. A000005, A027750, A036263, A193829."
			],
			"keyword": "nonn",
			"offset": "2,5",
			"author": "_Michel Lagneau_, Dec 16 2019",
			"references": 2,
			"revision": 34,
			"time": "2021-04-06T06:01:51-04:00",
			"created": "2020-02-23T16:13:35-05:00"
		}
	]
}