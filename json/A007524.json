{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007524",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7524,
			"id": "M2196",
			"data": "3,0,1,0,2,9,9,9,5,6,6,3,9,8,1,1,9,5,2,1,3,7,3,8,8,9,4,7,2,4,4,9,3,0,2,6,7,6,8,1,8,9,8,8,1,4,6,2,1,0,8,5,4,1,3,1,0,4,2,7,4,6,1,1,2,7,1,0,8,1,8,9,2,7,4,4,2,4,5,0,9,4,8,6,9,2,7,2,5,2,1,1,8,1,8,6,1,7,2,0,4,0,6,8,4",
			"name": "Decimal expansion of log_10 2.",
			"comment": [
				"Log_10 (2) is the probability that 1 be first significant digit occurring in data collections (Benford's law). - _Lekraj Beedassy_, Jan 21 2005",
				"When adding two sound power sources of x decibels, the resulting sound power is x + 10*log_10(2), that is x + 3.01... decibels. - _Jean-François Alcover_, Jun 21 2013",
				"In engineering (all branches, but particularly electronic and electrical) power and amplitude ratios are measured rigorously in decibels (dB). This constant, with offset 1 (i.e., 3.01... = 10*A007524) is the dB equivalent of a 2:1 power ratio or, equivalently, sqrt(2):1 amplitude ratio. - _Stanislav Sykora_, Dec 11 2013"
			],
			"reference": [
				"T. Hill, \"Manipulation, or the First Significant Numeral Determines the Law\", in 'La Recherche', No. 2 1999 pp. 72-76 (or No. 116 1999 pp. 72-75), Paris.",
				"M. E. Lines, A Number For Your Thought, pp. 43-52 Institute of Physics Pub. London 1990.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"I. Stewart, L'univers des nombres, \"1 est plus probable que 9\", pp. 57-61, Belin-Pour La Science, Paris 2000."
			],
			"link": [
				"Harry J. Smith, \u003ca href=\"/A007524/b007524.txt\"\u003eTable of n, a(n) for n = 0..20000\u003c/a\u003e",
				"K. Brown, \u003ca href=\"http://www.mathpages.com/home/kmath302/kmath302.htm\"\u003eBenford's Law\u003c/a\u003e",
				"C. K. Caldwell, The Prime Glossary, \u003ca href=\"http://primes.utm.edu/glossary/page.php/BenfordsLaw.html\"\u003eBenford's law\u003c/a\u003e",
				"I. Gent \u0026 T. Walsh, \u003ca href=\"http://www.dcs.st-and.ac.uk/~apes/reports/apes-25-2001.pdf\"\u003eBenford's Law\u003c/a\u003e",
				"T. P. Hill, \u003ca href=\"http://www.mccombs.utexas.edu/faculty/jonathan.koehler/docs/sta309h/Benford_1998.pdf\"\u003eThe first digital phenomenon\u003c/a\u003e",
				"T. P. Hill, \u003ca href=\"https://web.archive.org/web/20080830025404/http://www.math.gatech.edu/~hill/publications/cv.dir/1st-dig.pdf\"\u003eThe First-Digit Phenomenon\u003c/a\u003e",
				"T. P. Hill, \u003ca href=\"https://web.archive.org/web/20070417184651/http://www.math.gatech.edu/~hill/publications/cv.dir/1st-fig.pdf\"\u003eThe First-Digit Phenomenon (Accompanying Diagrams)\u003c/a\u003e",
				"R. Matthews, \u003ca href=\"http://www.fortunecity.com/emachines/e11/86/one.html\"\u003eThe Power of One\u003c/a\u003e",
				"S. J. Miller, \u003ca href=\"http://www.math.brown.edu/~sjmiller/math/handouts/BenfordTreatiseShort.pdf\"\u003eSome Thoughts on benford's Law\u003c/a\u003e",
				"M. J. Nigrini, \u003ca href=\"http://www.nigrini.com/Benford\u0026#39;s_law.htm\"\u003eBenford's Law\u003c/a\u003e",
				"I. Peterson, Mathtrek, \u003ca href=\"http://www.maa.org/mathland/mathtrek_6_29_98.html\"\u003eFirst Digits\u003c/a\u003e",
				"L. Pietronero et al., \u003ca href=\"http://arXiv.org/abs/cond-mat/9808305\"\u003eThe Uneven Distribution of Numbers in Nature\u003c/a\u003e",
				"Simon Plouffe, \u003ca href=\"http://www.worldwideschool.org/library/books/sci/math/MiscellaneousMathematicalConstants/chap57.html\"\u003eThe log10 of 2 to 2000 digits\u003c/a\u003e",
				"Simon Plouffe, \u003ca href=\"http://www.plouffe.fr/simon/constants/log102.txt\"\u003eThe LOG of 2(in base 10)\u003c/a\u003e",
				"J. Walthoe, \u003ca href=\"http://plus.maths.org/issue9/features/benford\"\u003eLooking out for number one\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/BenfordsLaw.html\"\u003eBenford's Law\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/MersenneNumber.html\"\u003eMersenne Number\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Benford\u0026#39;s_law\"\u003eBenford's law\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Decibel\"\u003eDecibel\u003c/a\u003e",
				"\u003ca href=\"/index/Be#Benford\"\u003eIndex entries for sequences related to Benford's law\u003c/a\u003e",
				"\u003ca href=\"/index/Tra#transcendental\"\u003eIndex entries for transcendental numbers\u003c/a\u003e"
			],
			"formula": [
				"log_10(2) = log(2)/log(10) = log(2)/(log(2) + log(5))."
			],
			"example": [
				"0.3010299956639811952137388947244930267681898814621085413104274611271..."
			],
			"mathematica": [
				"RealDigits[Log[10, 2], 10, 120][[1]] (* _Harvey P. Dale_, Dec 19 2011 *)"
			],
			"program": [
				"(PARI) default(realprecision, 20080); x=log(2)/log(10); d=0; for (n=0, 20000, x=(x-d)*10; d=floor(x); write(\"b007524.txt\", n, \" \", d)); \\\\ _Harry J. Smith_, Apr 15 2009"
			],
			"xref": [
				"Cf. decimal expansion of log_10(m): this sequence, A114490 (m = 3), A114493 (m = 4), A153268 (m = 5), A153496 (m = 6), A153620 (m = 7), A153790 (m = 8), A104139 (m = 9),  A154182 (m = 11), A154203 (m = 12), A154368 (m = 13), A154478 (m = 14), A154580 (m = 15), A154794 (m = 16), A154860 (m = 17), A154953 (m = 18), A155062 (m = 19), A155522 (m = 20), A155677 (m = 21), A155746 (m = 22), A155830 (m = 23), A155979 (m = 24)."
			],
			"keyword": "nonn,cons",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Definition corrected by _Franklin T. Adams-Watters_, Apr 13 2006",
				"Final digits of sequence corrected using the b-file. - _N. J. A. Sloane_, Aug 30 2009"
			],
			"references": 30,
			"revision": 64,
			"time": "2021-12-26T21:43:36-05:00",
			"created": "1994-09-19T03:00:00-04:00"
		}
	]
}