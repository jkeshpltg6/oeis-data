{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A159896",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 159896,
			"data": "785,839,901,3809,4195,4621,22069,24331,26825,128605,141791,156329,749561,826415,911149,4368761,4816699,5310565,25463005,28073779,30952241,148409269,163625975,180402881,864992609,953682071,1051465045",
			"name": "Positive numbers y such that y^2 is of the form x^2+(x+839)^2 with integer x.",
			"comment": [
				"(-56, a(1)) and (A130647(n), a(n+1)) are solutions (x, y) to the Diophantine equation x^2+(x+839)^2 = y^2.",
				"lim_{n -\u003e infinity} a(n)/a(n-3) = 3+2*sqrt(2).",
				"lim_{n -\u003e infinity} a(n)/a(n-1) = (843+58*sqrt(2))/839 for n mod 3 = {0, 2}.",
				"lim_{n -\u003e infinity} a(n)/a(n-1) = (1760979+1141390*sqrt(2))/839^2 for n mod 3 = 1.",
				"For the generic case x^2+(x+p)^2=y^2 with p=m^2-2 a prime number in A028871, m\u003e=5, the x values are given by the sequence defined by: a(n) = 6*a(n-3) -a(n-6) +2*p with a(1)=0, a(2) = 2*m+2, a(3) = 3*m^2 -10*m +8, a(4) = 3*p, a(5) = 3*m^2 +10*m +8, a(6) = 20*m^2 -58*m +42. Y values are given by the sequence defined by: b(n) = 6*b(n-3) -b(n-6) with b(1)=p, b(2)= m^2 +2*m +2, b(3)= 5*m^2 -14*m +10, b(4)= 5*p, b(5)= 5*m^2 +14*m +10, b(6)= 29*m^2 -82*m +58. - _Mohamed Bouhamida_, Sep 09 2009"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A159896/b159896.txt\"\u003eTable of n, a(n) for n = 1..3895\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,0,6,0,0,-1)."
			],
			"formula": [
				"a(n) = 6*a(n-3) -a(n-6) for n \u003e 6; a(1)=785, a(2)=839, a(3)=901, a(4)=3809, a(5)=4195, a(6)=4621.",
				"G.f.: (1-x)*(785+1624*x+2525*x^2+1624*x^3+785*x^4)/(1-6*x^3+x^6).",
				"a(3*k-1) = 839*A001653(k) for k \u003e= 1."
			],
			"example": [
				"(-56, a(1)) = (-56, 785) is a solution: (-56)^2+(-56+839)^2 = 3136+613089 = 616225 = 785^2.",
				"(A130647(1), a(2)) = (0, 839) is a solution: 0^2+(0+839)^2 = 703921 = 839^2.",
				"(A130647(3), a(4)) = (2241, 3809) is a solution: 2241^2+(2241+839)^2 = 5022081+9486400 = 14508481 = 3809^2."
			],
			"mathematica": [
				"LinearRecurrence[{0,0,6,0,0,-1}, {785,839,901,3809,4195,4621}, 30] (* _Harvey P. Dale_, Mar 03 2013 *)"
			],
			"program": [
				"(PARI) {forstep(n=-56, 10000000, [1, 3], if(issquare(2*n^2+1678*n+703921, \u0026k), print1(k, \",\")))}",
				"(MAGMA) I:=[785,839,901,3809,4195,4621]; [n le 6 select I[n] else 6*Self(n-3) -Self(n-6): n in [1..30]]; // _G. C. Greubel_, May 17 2018",
				"(PARI) is(n,p=839)=for(m=sqrtint((max(n,984)^2-p^2)\\2)-p\\2,n,m^2+(m+p)^2\u003cn^2||return(m^2+(m+p)^2==n^2))",
				"A159896(n)=(matrix(6,6,i,j,if(i\u003c6,i+1==j,j==4,6,j==1,-1))^n*[785,839,901,3809,4195,4621]~)[1] \\\\ _M. F. Hasler_, May 17 2018"
			],
			"xref": [
				"Cf. A130647, A001653, A156035 (decimal expansion of 3+2*sqrt(2)), A159897 (decimal expansion of (843+58*sqrt(2))/839), A159898 (decimal expansion of (1760979+1141390*sqrt(2))/839^2)."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_Klaus Brockhaus_, Apr 30 2009",
			"references": 4,
			"revision": 18,
			"time": "2020-02-15T10:52:27-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}