{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A208675",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 208675,
			"data": "1,1,5,37,309,2751,25493,242845,2360501,23301307,232834755,2349638259,23905438725,244889453043,2523373849701,26132595017037,271826326839477,2838429951771795,29740725671232119,312573076392760183,3294144659048391059,34802392680979707121",
			"name": "Number of words, either empty or beginning with the first letter of the ternary alphabet, where each letter of the alphabet occurs n times and letters of neighboring word positions are equal or neighbors in the alphabet.",
			"comment": [
				"Also the number of (3*n-1)-step walks on 3-dimensional cubic lattice from (1,0,0) to (n,n,n) with positive unit steps in all dimensions such that the absolute difference of the dimension indices used in consecutive steps is \u003c= 1."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A208675/b208675.txt\"\u003eTable of n, a(n) for n = 0..300\u003c/a\u003e"
			],
			"formula": [
				"(n+1)^2 * (1 -4*n +5*n^2) * a(n+1) = (5 -5*n -26*n^2 +11*n^3 +55*n^4) * a(n) + (n-1)^2 * (2 +6*n +5*n^2) * a(n-1). - _Michael Somos_, Jun 03 2012",
				"a(n) = A108625(n-1, n). - _Michael Somos_, Jun 03 2012",
				"a(n) ~ sqrt((5-sqrt(5))/10)/(2*Pi*n) * ((1+sqrt(5))/2)^(5*n). - _Vaclav Kotesovec_, Dec 06 2012. Equivalently, a(n) ~ phi^(5*n - 1/2) / (2 * 5^(1/4) * Pi * n), where phi = A001622 is the golden ratio. - _Vaclav Kotesovec_, Dec 07 2021",
				"exp( Sum_{n \u003e= 1} a(n)*x^n/n ) = 1 + x + 3*x^2 + 15*x^3 + 94*x^4 + 668*x^5 + 5144*x^6 + 41884*x^7 + 355307*x^8 + ... appears to have integer coefficients. Cf. A108628. - _Peter Bala_, Jan 12 2016"
			],
			"example": [
				"a(2) = 5 = |{aabbcc, aabcbc, aabccb, ababcc, abccba}|.",
				"a(3) = 37 = |{aaabbbccc, aaabbcbcc, aaabbccbc, aaabbcccb, aaabcbbcc, aaabcbcbc, aaabcbccb, aaabccbbc, aaabccbcb, aaabcccbb, aababbccc, aababcbcc, aababccbc, aababcccb, aabbabccc, aabbcccba, aabcbabcc, aabcbccba, aabccbabc, aabccbcba, aabcccbab, aabcccbba, abaabbccc, abaabcbcc, abaabccbc, abaabcccb, abababccc, ababcccba, abbaabccc, abbcccbaa, abcbaabcc, abcbccbaa, abccbaabc, abccbcbaa, abcccbaab, abcccbaba, abcccbbaa}|."
			],
			"maple": [
				"a:= n-\u003e add(binomial(n-1, k)^2 *binomial(2*n-1-k, n-k), k=0..n):",
				"seq(a(n), n=0..30);  # _Alois P. Heinz_, Jun 26 2012"
			],
			"mathematica": [
				"a[ n_] := HypergeometricPFQ[{1 - n, -n, n}, {1, 1}, 1] (* _Michael Somos_, Jun 03 2012 *)"
			],
			"xref": [
				"Column k=3 of A208673. Cf. A108628."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Alois P. Heinz_, Feb 29 2012",
			"references": 5,
			"revision": 33,
			"time": "2021-12-07T04:12:17-05:00",
			"created": "2012-03-02T02:46:22-05:00"
		}
	]
}