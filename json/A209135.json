{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A209135",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 209135,
			"data": "1,2,1,3,5,3,4,14,16,5,5,30,54,39,11,6,55,144,171,98,21,7,91,329,561,503,229,43,8,140,672,1534,1928,1380,532,85,9,204,1260,3690,6106,6084,3636,1203,171,10,285,2208,8058,16852,21890,18060,9249,2694",
			"name": "Triangle of coefficients of polynomials u(n,x) jointly generated with A209136; see the Formula section.",
			"comment": [
				"For a discussion and guide to related arrays, see A208510.",
				"Subtriangle of the triangle given by (1, 1, -1, 1, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (0, 1, 2, -2, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Apr 11 2012"
			],
			"formula": [
				"u(n,x) = u(n-1,x) + (x+1)*v(n-1,x),",
				"v(n,x) = 2x*u(n-1,x) + (x+1)*v(n-1,x),",
				"where u(1,x)=1, v(1,x)=1.",
				"From _Philippe Deléham_, Apr 11 2012: (Start)",
				"As DELTA-triangle T(n,k) with 0 \u003c= k \u003c= n:",
				"G.f.: (1-x-y*x+x^2-y*x^2-2*y^2*x^2)/(1-2*x-y*x+x^2-y*x^2-2*y^2*x^2).",
				"T(n,k) = 2*T(n-1,k) + T(n-1,k-1) - T(n-2,k) + T(n-2,k-1) + 2*T(n-2,k-2), T(0,0) = T(1,0) = T(2,1) = 1, T(2,0) = 2, T(1,1) = T(2,2) = 0 and T(n,k) = 0 if k \u003c 0 or if k \u003e n. (End)"
			],
			"example": [
				"First five rows:",
				"  1;",
				"  2,  1;",
				"  3,  5,  3;",
				"  4, 14, 16,  5;",
				"  5, 30, 54, 39, 11;",
				"First three polynomials u(n,x):",
				"  1",
				"  2 + x",
				"  3 + 5x + 3x^2",
				"From _Philippe Deléham_, Apr 11 2012: (Start)",
				"(1, 1, -1, 1, 0, 0, 0, ...) DELTA (0, 1, 2, -2, 0, 0, 0, ...) begins:",
				"  1;",
				"  1,  0;",
				"  2,  1,  0;",
				"  3,  5,  3,  0;",
				"  4, 14, 16,  5,  0;",
				"  5, 30, 54, 39, 11,  0; (End)"
			],
			"mathematica": [
				"u[1, x_] := 1; v[1, x_] := 1; z = 16;",
				"u[n_, x_] := u[n - 1, x] + (x + 1)*v[n - 1, x];",
				"v[n_, x_] := 2 x*u[n - 1, x] + (x + 1)*v[n - 1, x];",
				"Table[Expand[u[n, x]], {n, 1, z/2}]",
				"Table[Expand[v[n, x]], {n, 1, z/2}]",
				"cu = Table[CoefficientList[u[n, x], x], {n, 1, z}];",
				"TableForm[cu]",
				"Flatten[%]    (* A209135 *)",
				"Table[Expand[v[n, x]], {n, 1, z}]",
				"cv = Table[CoefficientList[v[n, x], x], {n, 1, z}];",
				"TableForm[cv]",
				"Flatten[%]    (* A209136 *)"
			],
			"xref": [
				"Cf. A209136, A208510."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Mar 05 2012",
			"references": 3,
			"revision": 13,
			"time": "2020-01-24T03:25:45-05:00",
			"created": "2012-03-06T11:51:42-05:00"
		}
	]
}