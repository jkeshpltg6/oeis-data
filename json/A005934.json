{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005934",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5934,
			"id": "M3333",
			"data": "1,4,8,16,32,64,128,144,216,288,432,864,1296,1728,2592,3456,5184,7776,10368,15552,20736,31104,41472,62208,86400,108000,129600,194400,216000,259200,324000,432000,518400,648000,972000,1296000,1944000,2592000",
			"name": "Highly powerful numbers: numbers with record value of the product of the exponents in prime factorization (A005361).",
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Amiram Eldar, \u003ca href=\"/A005934/b005934.txt\"\u003eTable of n, a(n) for n = 1..609\u003c/a\u003e (terms 1..300 from T. D. Noe)",
				"R. K. Guy, \u003ca href=\"/A001599/a001599_1.pdf\"\u003eLetter to N. J. A. Sloane with attachment, Jun. 1991\u003c/a\u003e",
				"G. E. Hardy and M. V. Subbarao, \u003ca href=\"/A005934/a005934.pdf\"\u003eHighly powerful numbers\u003c/a\u003e, Congress. Numer., Vol. 37 (1983), pp. 277-307. (Annotated scanned copy)",
				"C. B. Lacampagne and J. L. Selfridge, \u003ca href=\"https://doi.org/10.1090/S0002-9939-1984-0740165-6\"\u003eLarge highly powerful numbers are cubeful\u003c/a\u003e, Proc. Amer. Math. Soc., Vol. 91, No. 2 (1984), pp. 173-181.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Highly_powerful_number\"\u003eHighly powerful number\u003c/a\u003e",
				"\u003ca href=\"/index/Pow#powerful\"\u003eIndex entries for sequences related to powerful numbers\u003c/a\u003e"
			],
			"formula": [
				"For n = Product p_i^e_i, let b(n) = Product e_i; then n is highly powerful if b(n) sets a new record."
			],
			"mathematica": [
				"a = {1}; b = {1}; f[n_] := Times @@ Last /@ FactorInteger[n]; Do[If[f@ n \u003e Max[b], And[AppendTo[b, f@ n], AppendTo[a, n]]], {n, 1000000}]; a (* _Michael De Vlieger_, Aug 28 2015 *)",
				"With[{s = Array[Times @@ FactorInteger[#][[All, -1]] \u0026, 3*10^6]}, Map[FirstPosition[s, #][[1]] \u0026, Union@ FoldList[Max, s]]] (* _Michael De Vlieger_, Oct 15 2017 *)"
			],
			"program": [
				"(PARI) {prdex(n)=local(s,fac); s=1; fac=factor(n); for(k=1,matsize(fac)[1],s=s*fac[k,2]); return(s)} {hp(m)=local(rec); rec=0; for(n=1,m,if(prdex(n)\u003erec,rec=prdex(n); print1(n\",\")))}"
			],
			"xref": [
				"Cf. A036965, A001694, A007532, A005361, A005188, A003321, A014576, A023052, A046074.",
				"Cf. A085629."
			],
			"keyword": "nonn,nice,easy",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Hardy and Subbarao give an extensive table.",
				"Corrected and extended by _Jason Earls_, Jul 10 2003"
			],
			"references": 26,
			"revision": 42,
			"time": "2019-05-13T11:19:43-04:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}