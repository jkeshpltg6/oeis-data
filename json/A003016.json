{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003016",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3016,
			"id": "M0227",
			"data": "0,3,1,2,2,2,3,2,2,2,4,2,2,2,2,4,2,2,2,2,3,4,2,2,2,2,2,2,4,2,2,2,2,2,2,4,4,2,2,2,2,2,2,2,2,4,2,2,2,2,2,2,2,2,2,4,4,2,2,2,2,2,2,2,2,2,4,2,2,2,3,2,2,2,2,2,2,2,4,2,2,2,2,2,4,2,2,2,2,2,2,4,2,2,2,2,2,2,2,2,2,2",
			"name": "Number of occurrences of n as an entry in rows \u003c= n of Pascal's triangle (A007318).",
			"comment": [
				"Or, number of occurrences of n as a binomial coefficient.",
				"Sequence A138496 gives record values and where they occur. - _Reinhard Zumkeller_, Mar 20 2008"
			],
			"reference": [
				"L. Comtet, Advanced Combinatorics, Reidel, 1974, p. 93, #47.",
				"C. S. Ogilvy, Tomorrow's Math. 2nd ed., Oxford Univ. Press, 1972, p. 96.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"R. Zumkeller, \u003ca href=\"/A003016/b003016.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"H. L. Abbott, P. Erdős and D. Hanson, \u003ca href=\"http://www.jstor.org/stable/2319526\"\u003eOn the numbers of times an integer occurs as a binomial coefficient\u003c/a\u003e, Amer. Math. Monthly, (1974), 256-261.",
				"Daniel Kane, \u003ca href=\"http://www.emis.de/journals/INTEGERS/papers/e7/e7.Abstract.html\"\u003eNew Bounds on the Number of Representations of t as a Binomial Coefficient\u003c/a\u003e, INTEGERS, Electronic J. of Combinatorial Number Theory, Vol. 4, Paper A7, 2004.",
				"Kaisa Matomäki, Maksym Radziwill, Xuancheng Shao, Terence Tao, and Joni Teräväinen, \u003ca href=\"https://arxiv.org/abs/2106.03335\"\u003eSingmaster's conjecture in the interior of Pascal's triangle\u003c/a\u003e, arXiv:2106.03335 [math.NT], 2021.",
				"D. Singmaster, \u003ca href=\"http://www.jstor.org/stable/2316907\"\u003eHow often does an integer occur as a binomial coefficient?\u003c/a\u003e, Amer. Math. Monthly, 78 (1971), 385-386.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PascalsTriangle.html\"\u003ePascal's Triangle\u003c/a\u003e",
				"\u003ca href=\"/index/Pas#Pascal\"\u003eIndex entries for triangles and arrays related to Pascal's triangle\u003c/a\u003e"
			],
			"mathematica": [
				"a[0] = 0; t = {{1}}; a[n_] := Count[ AppendTo[t, Table[ Binomial[n, k], {k, 0, n}]], n, {2}]; Table[a[n], {n, 0, 101}] (* _Jean-François Alcover_, Feb 20 2012 *)"
			],
			"program": [
				"(Haskell)",
				"a003016 n = sum $ map (fromEnum . (== n)) $",
				"                      concat $ take (fromInteger n + 1) a007318_tabl",
				"-- _Reinhard Zumkeller_, Apr 12 2012",
				"(PARI) f(n,k)=my(g=lngamma(k+1)+log(n)); binomial(round(solve(N=k,n+k, lngamma(N+1) - lngamma(N-k+1) - g)),k)==n",
				"a(n)=if(n\u003c3,[0,3,1][n+1], 2+2*sum(k=2,(n-1)\\2, f(n,k))+if(n%2,,f(n,n/2))) \\\\ _Charles R Greathouse IV_, Oct 22 2013"
			],
			"xref": [
				"Cf. A003015, A059233, A138496, A180058."
			],
			"keyword": "nonn,nice,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Erich Friedman_",
				"Edited by _N. J. A. Sloane_, Nov 18 2007, at the suggestion of _Max Alekseyev_"
			],
			"references": 16,
			"revision": 49,
			"time": "2021-12-22T02:19:30-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}