{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A274706",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 274706,
			"data": "1,1,0,2,0,4,2,2,0,2,0,2,6,4,6,4,4,4,2,0,6,0,6,0,4,0,2,0,2,6,24,16,20,14,16,12,8,6,8,4,4,2,8,0,14,0,14,0,10,0,10,0,6,0,4,0,2,0,2,36,52,68,48,64,48,48,40,44,32,36,24,22,16,16,8,10,8,4,4,2",
			"name": "Irregular triangle read by rows. T(n,k) (n \u003e= 0) is a statistic on orbital systems over n sectors: the number of orbitals which have an integral whose absolute value is k.",
			"comment": [
				"For the combinatorial definitions see A232500. The absolute integral of an orbital w over n sectors is abs(Sum_{1\u003c=k\u003c=n} Sum_{1\u003c=i\u003c=k} w(i)))) where w(i) are the jumps of the orbital represented by -1, 0, 1.",
				"An orbital is balanced if its integral is 0 (A241810)."
			],
			"link": [
				"Peter Luschny, \u003ca href=\"https://oeis.org/wiki/User:Peter_Luschny/Orbitals\"\u003eOrbitals\u003c/a\u003e"
			],
			"example": [
				"The length of row n is 1+floor(n^2//4).",
				"The triangle begins:",
				"[n] [k=0,1,2,...] [row sum]",
				"[0] [1] 1",
				"[1] [1] 1",
				"[2] [0, 2] 2",
				"[3] [0, 4, 2] 6",
				"[4] [2, 0, 2, 0, 2] 6",
				"[5] [6, 4, 6, 4, 4, 4, 2] 30",
				"[6] [0, 6, 0, 6, 0, 4, 0, 2, 0, 2] 20",
				"[7] [6, 24, 16, 20, 14, 16, 12, 8, 6, 8, 4, 4, 2] 140",
				"[8] [8, 0, 14, 0, 14, 0, 10, 0, 10, 0, 6, 0, 4, 0, 2, 0, 2] 70",
				"T(5, 4) = 4 because the integral of four orbitals have the absolute value 4:",
				"Integral([-1, -1, 1, 1, 0]) = -4, Integral([0, -1, -1, 1, 1]) = -4,",
				"Integral([0, 1, 1, -1, -1]) = 4, Integral([1, 1, -1, -1, 0]) = 4."
			],
			"program": [
				"# Brute force counting, function unit_orbitals defined in A274709.",
				"def orbital_integral(n):",
				"    if n == 0: return [1]",
				"    S = [0]*(1+floor(n^2//4))",
				"    for u in unit_orbitals(n):",
				"        L = list(accumulate(accumulate(u)))",
				"        S[abs(L[-1])] += 1",
				"    return S",
				"for n in (0..8): print orbital_integral(n)"
			],
			"xref": [
				"Cf. A056040 (row sum), A232500, A241810 (col. 0), A242087.",
				"Other orbital statistics: A241477 (first zero crossing), A274708 (number of peaks), A274709 (max. height), A274710 (number of turns), A274878 (span), A274879 (returns), A274880 (restarts), A274881 (ascent)."
			],
			"keyword": "nonn,tabf",
			"offset": "0,4",
			"author": "_Peter Luschny_, Jul 10 2016",
			"references": 10,
			"revision": 24,
			"time": "2016-07-17T20:41:36-04:00",
			"created": "2016-07-17T20:41:06-04:00"
		}
	]
}