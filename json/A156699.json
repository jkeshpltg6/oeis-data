{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A156699",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 156699,
			"data": "1,1,1,1,3,2,1,4,-9,6,1,5,-32,-135,24,1,6,-75,-2048,18225,120,1,7,-144,-12375,1835008,31984875,720,1,8,-245,-48384,38795625,32883343360,-954268745625,5040,1,9,-384,-145775,390168576,3283855678125,-15321007338291200,-597882768540159375,40320",
			"name": "Triangle T(n, k) = Product_{j=1..k} Product_{i=0..j-1} ( 1 - (n-k+1)*(2*i-1) ) with T(n, 0) = 1 and T(n, n) = n!, read by rows.",
			"comment": [
				"Row sums are: 1, 2, 6, 2, -137, 16229, 33808092, -921346650220, -613200491632709703, 9136424641471148255125435, ..."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A156699/b156699.txt\"\u003eRows n = 0..30 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"Let the square array t(n, k) be given by t(n, k) = Product_{j=1..n} Product_{i=0..j-1} ( 1 - (k+1)*(2*i -1) ) with t(n, 0) = n!. The number triangle, T(n, k), is the downward antidiagonals, i.e. T(n, k) = t(k, n-k).",
				"T(n, k) = (-2*(n-k+1))^binomial(k, 2)*(n-k+2)^k*Product_{j=1..k} Pochhammer( (n-k)/(2*(n-k+1)), j-1) with T(n, 0) = 1 and T(n, n) = n!. - _G. C. Greubel_, Feb 25 2021"
			],
			"example": [
				"Triangle begins as:",
				"  1;",
				"  1, 1;",
				"  1, 3,    2;",
				"  1, 4,   -9,      6;",
				"  1, 5,  -32,   -135,       24;",
				"  1, 6,  -75,  -2048,    18225,         120;",
				"  1, 7, -144, -12375,  1835008,    31984875,           720;",
				"  1, 8, -245, -48384, 38795625, 32883343360, -954268745625, 5040;"
			],
			"mathematica": [
				"(* First program *)",
				"t[n_, k_]:= If[k==0, n!, Product[1 -(2*i-1)*(k+1), {j, n}, {i,0,j-1}]];",
				"Table[t[k, n-k], {n, 0, 12}, {k, 0, n}]//Flatten (* modified by _G. C. Greubel_, Feb 25 2021 *)",
				"(* Second program *)",
				"T[n_, k_] = If[k==0, 1, If[k==n, n!, (-2*(n-k+1))^Binomial[k, 2]*(n-k+2)^k *Product[Pochhammer[(n-k)/(2*(n-k+1)), j-1], {j,k}] ]];",
				"Table[T[n, k], {n, 0, 12}, {k, 0, n}]//Flatten (* _G. C. Greubel_, Feb 25 2021 *)"
			],
			"program": [
				"(Sage)",
				"@CachedFunction",
				"def T(n, k):",
				"    if (k==0): return 1",
				"    elif (k==n): return factorial(n)",
				"    else: return (-2*(n-k+1))^binomial(k, 2)*(n-k+2)^k*product( rising_factorial( (n-k)/(2*(n-k+1)), j-1) for j in (1..k))",
				"flatten([[T(n, k) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Feb 24 2021",
				"(Magma)",
				"function T(n,k)",
				"  if k eq 0 then return 1;",
				"  elif k eq n then return Factorial(n);",
				"  else return (\u0026*[ (\u0026*[1 - (n-k+1)*(2*m-1): m in [0..j-1]]) :j in [1..k]]);",
				"  end if; return T;",
				"end function;",
				"[T(n,k): k in [0..n], n in [0..12]]; // _G. C. Greubel_, Feb 25 2021"
			],
			"xref": [
				"Cf. A156698, A156730."
			],
			"keyword": "sign,tabl",
			"offset": "0,5",
			"author": "_Roger L. Bagula_, Feb 13 2009",
			"ext": [
				"Edited by _G. C. Greubel_, Feb 25 2021"
			],
			"references": 6,
			"revision": 10,
			"time": "2021-02-25T08:31:53-05:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}