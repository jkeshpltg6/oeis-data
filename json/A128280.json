{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A128280",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 128280,
			"data": "0,2,1,4,3,8,5,6,7,10,9,14,15,16,13,18,11,12,17,20,21,22,19,24,23,30,29,32,27,26,33,28,25,34,37,36,31,40,39,44,35,38,41,42,47,50,51,46,43,54,49,48,53,56,45,52,55,58,69,62,65,66,61,70,57,74,63,64,67,60,71,68,59",
			"name": "a(n) is the least number not occurring earlier such that a(n)+a(n-1) is prime, a(0) = 0.",
			"comment": [
				"Original definition: start with a(1) = 2. See A055265 for start with a(1) = 1.",
				"The sequence may well be a rearrangement of natural numbers. Interestingly, subsets of first n terms are permutations of 1..n for n = {2, 4, 8, 10, 18, 22, 24, 56, ...}. E.g., first 56 terms: {2, 1, 4, 3, 8, 5, 6, 7, 10, 9, 14, 15, 16, 13, 18, 11, 12, 17, 20, 21, 22, 19, 24, 23, 30, 29, 32, 27, 26, 33, 28, 25, 34, 37, 36, 31, 40, 39, 44, 35, 38, 41, 42, 47, 50, 51, 46, 43, 54, 49, 48, 53, 56, 45, 52, 55} are a permutation of 1..56.",
				"Without altering the definition nor the existing values, one can as well start with a(0) = 0 and get (conjecturally) a permutation of the nonnegative integers. This sequence is in some sense the \"arithmetic\" analog of the \"digital\" variant A231433: Here we add subsequent terms, there the digits are concatenated. - _M. F. Hasler_, Nov 09 2013",
				"The sequence is also a particular case of \"among the pairwise sums of any M consecutive terms, N are prime\", with M = 2, N = 1. For other M, N see A329333, A329405 ff, A329449 ff and the OEIS Wiki page. - _M. F. Hasler_, Nov 24 2019"
			],
			"link": [
				"Lars Blomberg, \u003ca href=\"/A128280/b128280.txt\"\u003eTable of n, a(n) for n = 0..9999\u003c/a\u003e",
				"M. F. Hasler, \u003ca href=\"/wiki/User:M._F._Hasler/Prime_sums_from_neighboring_terms\"\u003ePrime sums from neighboring terms\u003c/a\u003e, OEIS wiki, Nov. 23, 2019."
			],
			"formula": [
				"a(2n-1) = A055265(2n-1) + 1, a(2n) = A055265(2n) - 1, for all n \u003e= 1. - _M. F. Hasler_, Feb 11 2020"
			],
			"program": [
				"(PARI) {a=0;u=0; for(n=1, 99, u+=1\u003c\u003ca; print1(a\", \"); for(k=1, 9e9, bittest(u, k)\u0026\u0026next; isprime(a+k)\u0026\u0026(a=k)\u0026\u0026next(2)))}"
			],
			"xref": [
				"Cf. A083236.",
				"Cf. A055265 for the variant starting with a(1) = 1, and A329333, A329405, ..., A329425 and A329449, ..., A329581 for other variants. - _M. F. Hasler_, Nov 24 2019"
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Zak Seidov_, May 03 2007",
			"ext": [
				"Initial a(0) = 0 prefixed by _M. F. Hasler_, Nov 09 2013"
			],
			"references": 19,
			"revision": 27,
			"time": "2021-07-19T01:19:58-04:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}