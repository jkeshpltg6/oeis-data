{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A178301",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 178301,
			"data": "1,1,3,1,8,10,1,15,45,35,1,24,126,224,126,1,35,280,840,1050,462,1,48,540,2400,4950,4752,1716,1,63,945,5775,17325,27027,21021,6435,1,80,1540,12320,50050,112112,140140,91520,24310,1,99,2376,24024,126126,378378,672672,700128,393822,92378",
			"name": "Triangle T(n,k) = binomial(n,k)*binomial(n+k+1,n+1) read by rows, 0 \u003c= k \u003c= n.",
			"comment": [
				"Antidiagonal sums are given by A113682. - _Johannes W. Meijer_, Mar 24 2013"
			],
			"link": [
				"David A. Corneth, \u003ca href=\"/A178301/b178301.txt\"\u003eTable of n, a(n) for n = 0..10010\u003c/a\u003e",
				"Author?, \u003ca href=\"https://dxdy.ru/topic12925.html\"\u003eNorm of a continuous function\u003c/a\u003e, dxdy.ru (in Russian)"
			],
			"formula": [
				"T(n,k) = A007318(n,k) * A178300(n+1,k+1).",
				"From _Peter Bala_, Jun 18 2015: (Start)",
				"n-th row polynomial R(n,x) = Sum_{k = 0..n} binomial(n,k)*binomial(n+k+1,n+1)*x^k = Sum_{k = 0..n} (-1)^(n+k)*binomial(n+1,k+1)*binomial(n+k+1,n+1)*(1 + x)^k.",
				"Recurrence: (2*n - 1)*(n + 1)*R(n,x) = 2*(4*n^2*x + 2*n^2 - x - 1)*R(n-1,x) - (2*n + 1)(n - 1)*R(n-2,x) with R(0,x) = 1, R(1,x) = 1 + 3*x.",
				"A182626(n) = -R(n-1,-2) for n \u003e= 1. (End)",
				"From _Peter Bala_, Jul 20 2015: (Start)",
				"n-th row polynomial R(n,x) = Jacobi_P(n,0,1,2*x + 1).",
				"(1 + x)*R(n,x) gives the row polynomials of A123160.",
				"(End)",
				"G.f.: (1+x-sqrt(1-2*x+x^2-4*x*y))/(2*(1+y)*x*sqrt(1-2*x+x^2-4*x*y)). - _Emanuele Munarini_, Dec 16 2016",
				"R(n,x) = Sum_{k=0..n} (-1)^(n+k)*(2*k+1)*P(k,2*x+1)/(n+1), where P(k,x) is the k-th Legendre polynomial (cf. A100258) and P(k,2*x+1) is the k-th shifted Legendre polynomial (cf. A063007). - _Max Alekseyev_, Jun 28 2018; corrected by _Peter Bala_, Aug 08 2021",
				"Polynomial g(n,x) = R(n,-x)/(n+1) delivers the maximum of f(1)^2/(Integral_{x=0..1} f(x)^2 dx) over all polynomials f(x) with real coefficients and deg(f(x)) \u003c= n. This maximum equals (n+1)^2. See dxdy.ru link. - _Max Alekseyev_, Jun 28 2018"
			],
			"example": [
				"n=0: 1;",
				"n=1: 1,  3;",
				"n=2: 1,  8,  10;",
				"n=3: 1, 15,  45,   35;",
				"n=4: 1, 24, 126,  224,   126;",
				"n=5: 1, 35, 280,  840,  1050,   462;",
				"n=6: 1, 48, 540, 2400,  4950,  4752,  1716;",
				"n=7: 1, 63, 945, 5775, 17325, 27027, 21021, 6435;"
			],
			"maple": [
				"A178301 := proc(n,k)",
				"        binomial(n,k)*binomial(n+k+1,n+1) ;",
				"end proc: # _R. J. Mathar_, Mar 24 2013",
				"R := proc(n) add((-1)^(n+k)*(2*k+1)*orthopoly:-P(k,2*x+1)/(n+1), k=0..n) end:",
				"for n from 0 to 6 do seq(coeff(R(n), x, k), k=0..n) od; # _Peter Luschny_, Aug 25 2021"
			],
			"mathematica": [
				"Flatten[Table[Binomial[n,k]Binomial[n+k+1,n+1],{n,0,10},{k,0,n}]] (* _Harvey P. Dale_, Aug 23 2014 *)"
			],
			"program": [
				"(Maxima) create_list(binomial(n,k)*binomial(n+k+1,n+1),n,0,12,k,0,n); _Emanuele Munarini_, Dec 16 2016",
				"(PARI) R(n,x) = sum(k=0,n, (-1)^(n+k) * (2*k+1) * pollegendre(k,2*x+1)) / (n+1); \\\\ _Max Alekseyev_, Aug 25 2021"
			],
			"xref": [
				"Cf. A007318, A047781 (row sums), A178300, A182626, A123160."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,3",
			"author": "_Alford Arnold_, May 30 2010",
			"references": 4,
			"revision": 48,
			"time": "2021-08-26T08:09:00-04:00",
			"created": "2010-07-11T03:00:00-04:00"
		}
	]
}