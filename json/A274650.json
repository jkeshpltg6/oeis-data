{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A274650",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 274650,
			"data": "0,1,2,3,0,1,2,4,3,5,5,1,0,2,3,4,3,5,1,6,7,6,7,2,0,5,4,8,8,5,9,4,7,2,10,6,7,10,8,3,0,6,9,5,4,11,6,12,7,1,8,3,10,9,13,9,8,4,11,2,0,1,12,6,7,10,10,11,7,12,4,3,2,9,8,14,13,15,12,9,10,6,8,1,0,11,7,4,16,14,17",
			"name": "Triangle read by rows: T(n,k), (0\u003c=k\u003c=n), in which each term is the least nonnegative integer such that no row, column, diagonal, or antidiagonal contains a repeated term.",
			"comment": [
				"Similar to A274528, but here this is a right triangle.",
				"The same rule applied to an equilateral triangle gives A274528.",
				"By analogy; the offset for both rows and columns is the same as the offset of A274528.",
				"We construct the triangle by reading from left to right in each row, starting with T(0,0) = 0.",
				"Presumably every diagonal and every column is also a permutation of the nonnegative integers, but the proof does not seem so straightforward. Of course neither the rows nor the antidiagonals are permutations of the nonnegative integers, since they are finite in length.",
				"_Omar E. Pol_'s conjecture that every column and every diagonal of the triangle is a permutation of the nonnegative integers is true: see the link. - _N. J. A. Sloane_, Jun 07 2017",
				"It appears that the numbers generally appear for the first time in or near the right border of the triangle.",
				"Theorem 1: the middle diagonal gives A000004 (the zero sequence).",
				"Proof: T(0,0) = 0 by definition. For the next rows we have that in row 1 there are no zeros because the first term belongs to a column that contains a zero and the second term belongs to a diagonal that contains a zero. In row 2 the unique zero is T(2,1) = 0 because the preceding term belongs to a column that contains a zero and the following term belongs to a diagonal that contains a zero. Then we have two recurrences for all rows of the triangle:",
				"a) If T(n,k) = 0 then row n+1 does not contain a zero because every term belongs to a column that contains a zero or it belongs to a diagonal that contains a zero.",
				"b) If T(n,k) = 0 the next zero is T(n+2,k+1) because every preceding term in row n+2 is a positive integer because it belongs to a column that contains a zero and, on the other hand, the column, the diagonal and the antidiagonal of T(n+2,k+1) do not contain zeros.",
				"Finally since both T(n,k) = 0 and T(n+2,k+1) = 0 are located in the middle diagonal so the other terms of the middle diagonal are zeros, or in other words: the middle diagonal gives A000004 (the zero sequence). QED",
				"Theorem 2: all zeros are in the middle diagonal.",
				"Proof: consider the first n rows of the triangle. Every element located above or at the right-hand side of the middle diagonal must be positive because it belongs to a diagonal that contains one of the zeros of the middle diagonal. On the other hand every element located below the middle diagonal must be positive because it belongs to a column that contains one of the zeros of the middle diagonal, hence there are no zeros outside of the middle diagonal, or in other words: all zeros are in the middle diagonal. QED",
				"From _Hartmut F. W. Hoft_, Jun 12 2017: (Start)",
				"T(2k,k) = 0, for all k\u003e=0, and T(n,{(n-1)/2,(n+2)/2,(n-2)/2,(n+1)/2}) = 1, for all n\u003e=0 with mod(n,8) = {1,2,4,5} respectively, and no 0's or 1's occur in other positions. The triangle of positions of 0's and 1's for this sequence is the triangle in the Comment section of A274651 with row and column indices and values shifted down by one.",
				"The sequence of rows containing 1's is A047613 (n mod 8 = {1,2,4,5}), those containing only 1's is A016813 (n mod 8 = {1,5}), those containing both 0's and 1's is A047463 (n mod 8 = {2,4}), those containing only 0's is A047451 (n mod 8 = {0,6}), and those containing neither 0's nor 2's is A004767 (n mod 8 = {3,7}).",
				"(End)"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A274650/b274650.txt\"\u003eRows n = 0..200, flattened\u003c/a\u003e",
				"F. Michel Dekking, Jeffrey Shallit, and N. J. A. Sloane, \u003ca href=\"https://www.combinatorics.org/ojs/index.php/eljc/article/view/v27i1p52/8039\"\u003eQueens in exile: non-attacking queens on infinite chess boards\u003c/a\u003e, Electronic J. Combin., 27:1 (2020), #P1.52.",
				"Rémy Sigrist, \u003ca href=\"/A274650/a274650.png\"\u003eColored representation of the rows n = 0..999\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A274650/a274650.gp.txt\"\u003ePARI program for A274650\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/A274650/a274650.txt\"\u003eNotes on A274650 and Proof by Non-Attacking Queens\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = A274651(n+1,k+1) - 1."
			],
			"example": [
				"Triangle begins:",
				"   0;",
				"   1,  2;",
				"   3,  0,  1;",
				"   2,  4,  3,  5;",
				"   5,  1,  0,  2,  3;",
				"   4,  3,  5,  1,  6,  7;",
				"   6,  7,  2,  0,  5,  4,  8;",
				"   8,  5,  9,  4,  7,  2, 10,  6;",
				"   7, 10,  8,  3,  0,  6,  9,  5,  4;",
				"  11,  6, 12,  7,  1,  8,  3, 10,  9, 13;",
				"   9,  8,  4, 11,  2,  0,  1, 12,  6,  7, 10;",
				"  10, 11,  7, 12,  4,  3,  2,  9,  8, 14, 13, 15;",
				"  12,  9, 10,  6,  8,  1,  0, 11,  7,  4, 16, 14, 17;",
				"  ...",
				"From _Omar E. Pol_, Jun 07 2017: (Start)",
				"The triangle may be reformatted as an isosceles triangle so that the zero sequence (A000004) appears in the central column (but note that this is NOT the way the triangle is constructed!):",
				".",
				".                    0;",
				".                  1,  2;",
				".                3,  0,  1;",
				".              2,  4,  3,  5;",
				".            5,  1,  0,  2,  3;",
				".          4,  3,  5,  1,  6,  7;",
				".        6,  7,  2,  0,  5,  4,  8;",
				".     8,   5,  9,  4,  7,  2, 10,  6;",
				".   7,  10,  8,  3,  0,  6,  9,  5,  4;",
				"...",
				"(End)"
			],
			"mathematica": [
				"(* function a274651[] is defined in A274651 *)",
				"(* computation of rows 0 ... n-1 *)",
				"a274650[n_] := a274651[n]-1",
				"Flatten[a274650[13]] (* data *)",
				"TableForm[a274650[13]] (* triangle *)",
				"(* _Hartmut F. W. Hoft_, Jun 12 2017 *)"
			],
			"program": [
				"(PARI) See Links section."
			],
			"xref": [
				"Cf. A000004 (middle diagonal).",
				"Cf. A046092 (indices of the zeros).",
				"Every diagonal and every column of the right triangle is a permutation of A001477.",
				"The left and right edges of the right triangle give A286294 and A286295.",
				"Cf. A274651 is the same triangle but with 1 added to every entry.",
				"Other sequences of the same family are A269526, A274528, A274820, A274821, A286297, A288530, A288531.",
				"Sequences mentioned in _N. J. A. Sloane_'s proof are A000170, A274616 and A287864.",
				"Cf. A288384.",
				"Cf. A004767, A016813, A047451, A047463, A047613. - _Hartmut F. W. Hoft_, Jun 12 2017",
				"See A308179, A308180 for a very similar triangle."
			],
			"keyword": "nonn,tabl,look",
			"offset": "0,3",
			"author": "_Omar E. Pol_, Jul 02 2016",
			"references": 15,
			"revision": 129,
			"time": "2020-03-07T13:50:20-05:00",
			"created": "2016-07-02T22:08:06-04:00"
		}
	]
}