{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A318173",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 318173,
			"data": "2,-11,158,-6513,202790,-12710761,578257422,-45608219247,8774909485920,-579515898830751,115918088707226940,-16737522590543449641,1282860173728469083872,-189053227741259934603831,55171097827950314187327460,-16235234399834578732807710581",
			"name": "The determinant of an n X n Toeplitz matrix M(n) whose first row consists of successive prime numbers prime(1), ..., prime(n) and whose first column consists of prime(1), prime(n + 1), ..., prime(2*n - 1).",
			"comment": [
				"The trace of the matrix M(n) is A005843(n).",
				"The sum of the first row of the matrix M(n) is A007504(n).",
				"The permanent of the matrix M(n) is A306457(n).",
				"For n \u003e 1, the subdiagonal sum of the matrix M(n) is A306192(n)."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A318173/b318173.txt\"\u003eTable of n, a(n) for n = 1..302\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Toeplitz_matrix\"\u003eToeplitz Matrix\u003c/a\u003e"
			],
			"example": [
				"For n = 1 the matrix M(1) is",
				"   2",
				"with determinant Det(M(1)) = 2.",
				"For n = 2 the matrix M(2) is",
				"   2, 3",
				"   5, 2",
				"with Det(M(2)) = -11.",
				"For n = 3 the matrix M(3) is",
				"   2, 3, 5",
				"   7, 2, 3",
				"  11, 7, 2",
				"with Det(M(3)) = 158."
			],
			"maple": [
				"f:= proc(n) uses LinearAlgebra;",
				"Determinant(ToeplitzMatrix([seq(ithprime(i),i=2*n-1..n+1,-1),seq(ithprime(i),i=1..n)]))",
				"end proc:",
				"map(f, [$1..20]); # _Robert Israel_, Aug 30 2018"
			],
			"mathematica": [
				"p[i_]:=Prime[i]; a[n_]:=Det[ToeplitzMatrix[Join[{p[1]},Array[p,n-1,{n+1,2*n-1}]],Array[p,n]]]; Array[a,20]"
			],
			"program": [
				"(PARI) tm(n) = {my(m = matrix(n, n, i, j, if (i==1, prime(j), if (j==1, prime(n+i-1))))); for (i=2, n, for (j=2, n, m[i,j] = m[i-1, j-1];);); m;}",
				"a(n) = matdet(tm(n)); \\\\ _Michel Marcus_, Mar 17 2019"
			],
			"xref": [
				"Cf. A005843, A000040, A007504, A306457, A306192."
			],
			"keyword": "sign",
			"offset": "1,1",
			"author": "_Stefano Spezia_, Aug 20 2018",
			"references": 7,
			"revision": 41,
			"time": "2019-03-27T03:59:20-04:00",
			"created": "2018-08-29T10:11:38-04:00"
		}
	]
}