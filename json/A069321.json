{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A069321",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 69321,
			"data": "1,1,5,31,233,2071,21305,249271,3270713,47580151,760192505,13234467511,249383390393,5057242311031,109820924003705,2542685745501751,62527556173577273,1627581948113854711,44708026328035782905,1292443104462527895991,39223568601129844839353",
			"name": "Stirling transform of A001563: a(0) = 1 and a(n) = Sum_{k=1..n} Stirling2(n,k)*k*k! for n \u003e= 1.",
			"comment": [
				"The number of compatible bipartitions of a set of cardinality n for which at least one subset is not underlined. E.g., for n=2 there are 5 such bipartitions: {1 2}, {1}{2}, {2}{1}, _{1}_{2}, _{2}_{1}. A005649 is the number of bipartitions of a set of cardinality n. A000670 is the number of bipartitions of a set of cardinality n with none of the subsets underlined. - _Kyle Petersen_, Mar 31 2005",
				"a(n) is the cardinality of the image set summed over \"all surjections\". All surjections means: onto functions f:{1, 2, ..., n} -\u003e {1, 2, ..., k} for every k, 1 \u003c= k \u003c= n.  a(n) = Sum_{k=1..n} A019538(n, k)*k. - _Geoffrey Critzer_, Nov 12 2012"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A069321/b069321.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"Benoit Cloitre, \u003ca href=\"https://web.archive.org/web/20150418172931/http://bcmathematics.monsite-orange.fr/FractalOrderOfPrimes.pdf\"\u003eOn the fractal behavior of primes\u003c/a\u003e, 2011. [internet archive]",
				"Benoit Cloitre, \u003ca href=\"https://www.docsity.com/pt/fractal-order-of-primes/4838318/\"\u003eOn the fractal behavior of primes\u003c/a\u003e, 2011.",
				"D. Foata and D. Zeilberger, \u003ca href=\"http://arxiv.org/abs/math/9406220\"\u003eThe Graphical Major Index\u003c/a\u003e, arXiv:math/9406220 [math.CO], 1994.",
				"D. Foata and D. Zeilberger, \u003ca href=\"http://dx.doi.org/10.1016/0377-0427(95)00254-5\"\u003eGraphical major indices\u003c/a\u003e, J. Comput. Appl. Math. 68 (1996), no. 1-2, 79-101."
			],
			"formula": [
				"Representation as an infinite series, in Maple notation: a(0) = 1 and a(n) = Sum_{k\u003e=2} (k^n*(k-1)/(2^k))/4 for n \u003e= 1. This is a Dobinski-type summation formula.",
				"E.g.f.: (exp(x) - 1)/((2 - exp(x))^2).",
				"a(n) = (1/2)*(A000670(n+1) - A000670(n)).",
				"O.g.f.: 1 + Sum_{n \u003e= 1} (2*n-1)!/(n-1)! * x^n / (Product_{k=1..n} (1 + (n + k - 1)*x)). - _Paul D. Hanna_, Oct 28 2013",
				"a(n) = (A000629(n+1) - A000629(n))/4. - _Benoit Cloitre_, Oct 20 2002",
				"a(n) = A232472(n-1)/2. - _Vincenzo Librandi_, Jan 03 2016",
				"a(n) ~ n! * n / (4 * (log(2))^(n+2)). - _Vaclav Kotesovec_, Jul 01 2018"
			],
			"maple": [
				"b:= proc(n) option remember; `if`(n=0, 1,",
				"      add(b(n-j)*binomial(n, j), j=1..n))",
				"    end:",
				"a:= n-\u003e `if`(n=0, 2, b(n+1)-b(n))/2:",
				"seq(a(n), n=0..30);  # _Alois P. Heinz_, Feb 02 2018"
			],
			"mathematica": [
				"max = 20; t = Sum[n^(n - 1)x^n/n!, {n, 1, max}]; Range[0, max]!CoefficientList[Series[D[1/(1 - y(Exp[x] - 1)), y] /. y -\u003e 1, {x, 0, max}], x] (* _Geoffrey Critzer_, Nov 12 2012 *)",
				"Prepend[Table[Sum[StirlingS2[n, k]*k*k!, {k, n}], {n, 18}], 1] (* _Michael De Vlieger_, Jan 03 2016 *)",
				"a[n_] := (PolyLog[-n-1, 1/2] - PolyLog[-n, 1/2])/4; a[0] = 1; Table[a[n], {n, 0, 20}] (* _Jean-François Alcover_, Mar 30 2016 *)"
			],
			"program": [
				"(PARI) {a(n)=polcoeff(1+sum(m=1, n, (2*m-1)!/(m-1)!*x^m/prod(k=1, m, 1+(m+k-1)*x+x*O(x^n))), n)} \\\\ _Paul D. Hanna_, Oct 28 2013"
			],
			"xref": [
				"Cf. A000629, A000670, A001563, A005649, A232472."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Karol A. Penson_, Mar 14 2002",
			"references": 11,
			"revision": 57,
			"time": "2020-06-29T15:44:36-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}