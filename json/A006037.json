{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006037",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6037,
			"id": "M5339",
			"data": "70,836,4030,5830,7192,7912,9272,10430,10570,10792,10990,11410,11690,12110,12530,12670,13370,13510,13790,13930,14770,15610,15890,16030,16310,16730,16870,17272,17570,17990,18410,18830,18970,19390,19670",
			"name": "Weird numbers: abundant (A005101) but not pseudoperfect (A005835).",
			"comment": [
				"OProject@Home in subproject Weird Engine calculates and stores the weird numbers.",
				"There are no odd weird numbers \u003c 10^17. - Robert A. Hearn (rah(AT)ai.mit.edu), May 25 2005",
				"From _Alois P. Heinz_, Oct 30 2009: (Start)",
				"The first weird number that has more than one decomposition of its divisors set into two subsets with equal sum (and thus is not a member of A083209) is 10430:",
				"  1+5+7+10+14+35+298+10430 = 2+70+149+745+1043+1490+2086+5215",
				"  2+70+298+10430 = 1+5+7+10+14+35+149+745+1043+1490+2086+5215. (End)",
				"There are no odd weird numbers \u003c 1.8*10^19. - _Wenjie Fang_, Sep 04 2013",
				"S. Benkowski and P. Erdős (1974) proved that the asymptotic density W of weird numbers is positive. It can be shown that W \u003c 0.0101 (see A005835). - _Jaycob Coleman_, Oct 26 2013",
				"No odd weird number exists below 10^21. The search is done on the volunteer computing project yoyo@home. - _Wenjie Fang_, Feb 23 2014",
				"No odd weird number with abundance less than 10^14 exists below 10^28. See Odd Weird Search link - _Wenjie Fang_, Feb 25 2015",
				"A weird number n multiplied with a prime p \u003e sigma(n) is again weird. Primitive weird numbers (A002975) are those which are not a multiple of a smaller term, i.e., don't have a weird proper divisor.-- Sequence A065235 lists odd numbers that can be written in only one way as sum of their divisors, and A122036 lists those which are not in A136446, i.e., not sum of proper divisors \u003e 1. - _M. F. Hasler_, Jul 30 2016"
			],
			"reference": [
				"J.-M. De Koninck, Ces nombres qui nous fascinent, Entry 70, p. 24, Ellipses, Paris 2008.",
				"R. K. Guy, Unsolved Problems in Number Theory, B2.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"L. Swierczewski and Donovan Johnson, \u003ca href=\"/A006037/b006037.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (first 4901 terms from L. Swierczewski)",
				"Gianluca Amato, Maximilian Hasler, Giuseppe Melfi and Maurizio Parton, \u003ca href=\"https://arxiv.org/abs/1803.00324\"\u003ePrimitive weird numbers having more than three distinct prime factors\u003c/a\u003e, Riv. Mat. Univ. Parma, 7(1), (2016), 153-163, arXiv:1803.00324 [math.NT], 2018.",
				"S. Benkoski, \u003ca href=\"http://www.jstor.org/stable/2316276\"\u003eAre All Weird Numbers Even?\u003c/a\u003e, Problem E2308, Amer. Math. Monthly, 79 (7) (1972), 774.",
				"S. J. Benkoski and P. Erdős, \u003ca href=\"https://doi.org/10.1090/S0025-5718-1974-0347726-9\"\u003eOn weird and pseudoperfect numbers\u003c/a\u003e, Math. Comp., 28 (1974), pp. 617-623. \u003ca href=\"http://www.renyi.hu/~p_erdos/1974-24.pdf\"\u003eAlternate link\u003c/a\u003e; \u003ca href=\"https://doi.org/10.1090/S0025-5718-1975-0360452-6\"\u003e1975 corrigendum\u003c/a\u003e",
				"David Eppstein, \u003ca href=\"http://www.ics.uci.edu/~eppstein/numth/egypt/odd-one.html\"\u003eEqyptian Fractions\u003c/a\u003e",
				"R. K. Guy, \u003ca href=\"/A001599/a001599_1.pdf\"\u003eLetter to N. J. A. Sloane with attachment, Jun. 1991\u003c/a\u003e",
				"H. J. Hindin, \u003ca href=\"http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=\u0026amp;arnumber=1090205\"\u003eQuasipractical numbers\u003c/a\u003e, IEEE Communications Magazine, March 1980, pp. 41-45.",
				"Odd Weird Search, \u003ca href=\"https://www.rechenkraft.net/forum/viewtopic.php?f=57\u0026amp;t=15230\"\u003eReport on the recently completed batch\u003c/a\u003e, Feb 23 2015.",
				"OProject, \u003ca href=\"http://web.archive.org/web/20131206135920/http://oproject.info/weird_list.php\"\u003eWeird numbers list\u003c/a\u003e",
				"J. Sandor and B. Crstici, \u003ca href=\"http://bib.tiera.ru/ShiZ/math/other/Handbook%20Of%20Number%20Theory%20II%20-%20J.%20Sandor,%20B.%20Crstici%20(Kluwer,%202004).pdf\"\u003eHandbook of number theory II\u003c/a\u003e, chapter 1.8.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/WeirdNumber.html\"\u003eWeird Number\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Weird_number\"\u003eWeird number\u003c/a\u003e",
				"Robert G. Wilson v, \u003ca href=\"/A006037/a006037.pdf\"\u003eLetter to N. J. A. Sloane, Jan. 1992\u003c/a\u003e",
				"Robert G. Wilson v, \u003ca href=\"/A007376/a007376.pdf\"\u003eLetter to N. J. A. Sloane, Oct. 1993\u003c/a\u003e"
			],
			"maple": [
				"isA006037 := proc(n)",
				"    isA005101(n) and not isA005835(n) ;",
				"end proc:",
				"for n from 1 do",
				"    if isA006037(n) then",
				"        print(n);",
				"    end if;",
				"end do: # _R. J. Mathar_, Jun 18 2015"
			],
			"mathematica": [
				"(* first do *) Needs[\"DiscreteMath`Combinatorica`\"] (* then *) fQ[n_] := Block[{d, l, t, i}, If[ DivisorSigma[1, n] \u003e 2n \u0026\u0026 Mod[n, 6] != 0, d = Take[Divisors[n], {1, -2}]; l = 2^Length[d]; t = Table[ NthSubset[j, d], {j, l - 1}]; i = 1; While[i \u003c l \u0026\u0026 Plus @@ t[[i]] != n, i++ ]]; If[i == l, True, False]]; Select[ Range[ 20000], fQ[ # ] \u0026] (* _Robert G. Wilson v_, May 20 2005 *)"
			],
			"program": [
				"(PARI) is_A006037(n,d=divisors(n),s=vecsum(d)-n,m=#d-1)={ m||return; while(d[m]\u003en, s-=d[m]; m--); d[m]\u003cn \u0026\u0026 if(s\u003en, is_A006037(n-d[m], d, s-d[m], m-1) \u0026\u0026 is_A006037(n, d, s-d[m], m-1), s\u003cn \u0026\u0026 m\u003c#d-1)} \\\\  _M. F. Hasler_, Mar 30 2008; improved and updated to current PARI syntax by _M. F. Hasler_, Jul 15 2016",
				"(PARI) is_A006037(n, d=divisors(n)[^-1], s=vecsum(d))={s\u003en \u0026\u0026 !is_A005835(n,d,s)} \\\\ Equivalent but slightly faster than the self-contained version above.-- For efficiency, ensure that the argument is even or add \"!bittest(n,0) \u0026\u0026 ...\" to check this first. - _M. F. Hasler_, Jul 17 2016",
				"(PARI) t=0; A006037=vector(100,i, until( is_A006037(t+=2),); t) \\\\ _M. F. Hasler_, Mar 30 2008",
				"(Haskell)",
				"a006037 n = a006037_list !! (n-1)",
				"a006037_list = filter ((== 0) . a210455) a005101_list",
				"-- _Reinhard Zumkeller_, Jan 21 2013"
			],
			"xref": [
				"Cf. A002975, A005101, A005835, A005100, A138850, A087167.",
				"Cf. A210455."
			],
			"keyword": "nonn,nice",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Jud McCranie_, Oct 21 2001"
			],
			"references": 58,
			"revision": 144,
			"time": "2021-10-26T16:52:15-04:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}