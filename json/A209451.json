{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A209451",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 209451,
			"data": "1,4,8,20,240,696,280,5408,21216,3940,57072,275568,277200,1873816,2585024,4680600,54616512,81841608,10976840,530008720,1919331360,1235646880,4474673184,21605633376,28253665440,162655527004,177341693872,30581480180,2953208968320",
			"name": "a(n) = Pell(n)*A034896(n) for n \u003e= 1, with a(0)=1, where A034896 lists the number of solutions to a^2 + b^2 + 3*c^2 + 3*d^2 = n.",
			"comment": [
				"Compare g.f. to the Lambert series of A034896:",
				"1 + 4*Sum_{n\u003e=1} Chi(n,3)*n*x^n/(1 - (-x)^n).",
				"Here Chi(n,3) = principal Dirichlet character modulo 3."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A209451/b209451.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e"
			],
			"formula": [
				"G.f.: 1 + 4*Sum_{n\u003e=1} Pell(n)*Chi(n,3)*n*x^n/(1 - A002203(n)*(-x)^n + (-1)^n*x^(2*n)), where A002203(n) = Pell(n-1) + Pell(n+1)."
			],
			"example": [
				"G.f.: A(x) = 1 + 4*x + 8*x^2 + 20*x^3 + 240*x^4 + 696*x^5 + 280*x^6 + ...",
				"where A(x) = 1 + 1*4*x + 2*4*x^2 + 5*4*x^3 + 12*20*x^4 + 29*24*x^5 + 70*4*x^6 + ... + Pell(n)*A034896(n)*x^n + ...",
				"The g.f. is also given by the identity:",
				"A(x) = 1 + 4*( 1*1*x/(1+2*x-x^2) + 2*2*x^2/(1-6*x^2+x^4) + 12*4*x^4/(1-34*x^4+x^8) + 29*5*x^5/(1+82*x^5-x^10) + 169*7*x^7/(1+478*x^7-x^14) + 408*8*x^8/(1-1154*x^8+x^16) + ...).",
				"The values of the Dirichlet character Chi(n,3) repeat [1,1,0,...]."
			],
			"mathematica": [
				"A034896[n_]:= SeriesCoefficient[(EllipticTheta[3, 0, q]*EllipticTheta[3, 0, q^3])^2, {q, 0, n}]; Join[{1}, Table[Fibonacci[n, 2]*A034896[n], {n, 1, 50}]] (* _G. C. Greubel_, Dec 24 2017 *)"
			],
			"program": [
				"(PARI) {Pell(n)=polcoeff(x/(1-2*x-x^2+x*O(x^n)),n)}",
				"{A002203(n)=Pell(n-1)+Pell(n+1)}",
				"{a(n)=polcoeff(1 + 4*sum(m=1,n,Pell(m)*kronecker(m,3)^2*m*x^m/(1-A002203(m)*(-x)^m+(-1)^m*x^(2*m) +x*O(x^n))),n)}",
				"for(n=0,61,print1(a(n),\", \"))"
			],
			"xref": [
				"Cf. A034896, A205971, A205884, A209447, A209450, A209452, A204270, A000129 (Pell), A002203."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Paul D. Hanna_, Mar 10 2012",
			"references": 4,
			"revision": 10,
			"time": "2017-12-25T04:06:15-05:00",
			"created": "2012-03-10T11:19:56-05:00"
		}
	]
}