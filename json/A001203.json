{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001203",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1203,
			"id": "M2646 N1054",
			"data": "3,7,15,1,292,1,1,1,2,1,3,1,14,2,1,1,2,2,2,2,1,84,2,1,1,15,3,13,1,4,2,6,6,99,1,2,2,6,3,5,1,1,6,8,1,7,1,2,3,7,1,2,1,1,12,1,1,1,3,1,1,8,1,1,2,1,6,1,1,5,2,2,3,1,2,4,4,16,1,161,45,1,22,1,2,2,1,4,1,2,24,1,2,1,3,1,2,1",
			"name": "Simple continued fraction expansion of Pi.",
			"comment": [
				"The first 5821569425 terms were computed by _Eric W. Weisstein_ on Sep 18 2011.",
				"The first 10672905501 terms were computed by _Eric W. Weisstein_ on Jul 17 2013.",
				"The first 15000000000 terms were computed by _Eric W. Weisstein_ on Jul 27 2013.",
				"The first 30113021586 terms were computed by _Syed Fahad_ on Apr 27 2021."
			],
			"reference": [
				"P. Beckmann, \"A History of Pi\".",
				"C. Brezinski, History of Continued Fractions and Padé Approximants, Springer-Verlag, 1991; pp. 151-152.",
				"J. R. Goldman, The Queen of Mathematics, 1998, p. 50.",
				"R. S. Lehman, A Study of Regular Continued Fractions. Report 1066, Ballistic Research Laboratories, Aberdeen Proving Ground, Maryland, Feb 1959.",
				"G. Lochs, Die ersten 968 Kettenbruchnenner von Pi. Monatsh. Math. 67 1963 311-316.",
				"C. D. Olds, Continued Fractions, Random House, NY, 1963; front cover of paperback edition.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A001203/b001203.txt\"\u003eTable of n, a(n) for n = 0..19999\u003c/a\u003e [from the Plouffe web page]",
				"James Barton, \u003ca href=\"http://www.virtuescience.com/pi-in-other-bases.html\"\u003eSimple Continued Fraction Expansion of Pi\u003c/a\u003e [From _Lekraj Beedassy_, Oct 27 2008]",
				"E. Bombieri and A. J. van der Poorten, \u003ca href=\"http://www-centre.mpce.mq.edu.au/alfpapers/a113.pdf\"\u003eContinued fractions of algebraic numbers\u003c/a\u003e",
				"K. Y. Choong, D. E. Daykin and C. R. Rathbone, \u003ca href=\"https://doi.org/10.1090/S0025-5718-71-99719-5\"\u003eRegular continued fractions for pi and gamma\u003c/a\u003e, Math. Comp., 25 (1971), 403.",
				"Exploratorium, \u003ca href=\"http://chesswanks.com/seq/cfpi/\"\u003e180 million terms of the simple CFE of pi\u003c/a\u003e",
				"Syed Fahad, \u003ca href=\"https://drive.google.com/drive/folders/1--Qh9Xxq1i6oeHnTXzKrQ9FoguHreBKy\"\u003e30 billion terms of the simple continued fraction of Pi\u003c/a\u003e",
				"Bill Gosper, answer to: \u003ca href=\"https://hsm.stackexchange.com/a/11620\"\u003eDid Gosper or the Borweins first prove Ramanujans formula?\u003c/a\u003e, StackExchange, April 2020.",
				"Bill Gosper and Julian Ziegler Hunts, \u003ca href=\"/A001203/a001203.gif\"\u003eAnimation\u003c/a\u003e",
				"B. Gourevitch, \u003ca href=\"http://www.pi314.net\"\u003eL'univers de Pi\u003c/a\u003e",
				"Hans Havermann, \u003ca href=\"http://chesswanks.com/pxp/cfpi.html\"\u003eSimple Continued Fraction for Pi\u003c/a\u003e [a 483 MB file containing 180 million terms]",
				"Hans Havermann, \u003ca href=\"/A001203/a001203.png\"\u003eBinary plot of 2^10 terms\u003c/a\u003e",
				"Maxim Sølund Kirsebom, \u003ca href=\"https://doi.org/10.3390/e23070840\"\u003eExtreme Value Theory for Hurwitz Complex Continued Fractions\u003c/a\u003e, Entropy (2021) Vol. 23, No. 7, 840.",
				"Antony Lee, \u003ca href=\"http://lup.lub.lu.se/luur/download?func=downloadFile\u0026amp;recordOId=9006930\u0026amp;fileOId=9006936\"\u003eDiophantine Approximation and Dynamical Systems\u003c/a\u003e, Master's Thesis, Lund University (Sweden 2020).",
				"Sophie Morier-Genoud and Valentin Ovsienko, \u003ca href=\"https://arxiv.org/abs/1908.04365\"\u003eOn q-deformed real numbers\u003c/a\u003e, arXiv:1908.04365 [math.QA], 2019.",
				"Ed Pegg, Jr., \u003ca href=\"http://www.mathpuzzle.com/MAA/07-Sequence%20Pictures/mathgames_12_08_03.html\"\u003eSequence Pictures\u003c/a\u003e, Math Games column, Dec 08 2003.",
				"Ed Pegg, Jr., \u003ca href=\"/A000043/a000043_2.pdf\"\u003eSequence Pictures\u003c/a\u003e, Math Games column, Dec 08 2003 [Cached copy, with permission (pdf only)]",
				"Simon Plouffe, \u003ca href=\"http://www.plouffe.fr/simon/constants/\"\u003e20 megaterms of this sequence as computed by Hans Havermann\u003c/a\u003e, starting in file CFPiTerms20aa.txt",
				"Denis Roegel, \u003ca href=\"https://hal.archives-ouvertes.fr/hal-02984214\"\u003eLambert's proof of the irrationality of Pi: Context and translation\u003c/a\u003e, hal-02984214 [math.HO], 2020.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PiContinuedFraction.html\"\u003ePi Continued Fraction\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Pi.html\"\u003ePi\u003c/a\u003e",
				"G. Xiao, \u003ca href=\"http://wims.unice.fr/~wims/en_tool~number~contfrac.en.html\"\u003eContfrac\u003c/a\u003e",
				"\u003ca href=\"/index/Con#confC\"\u003eIndex entries for continued fractions for constants\u003c/a\u003e",
				"\u003ca href=\"/index/Ph#Pi314\"\u003eIndex entries for sequences related to the number Pi\u003c/a\u003e"
			],
			"example": [
				"Pi = 3.1415926535897932384...",
				"   = 3 + 1/(7 + 1/(15 + 1/(1 + 1/(292 + ...))))",
				"   = [a_0; a_1, a_2, a_3, ...] = [3; 7, 15, 1, 292, ...]."
			],
			"maple": [
				"cfrac (Pi,70,'quotients'); # _Zerinvary Lajos_, Feb 10 2007"
			],
			"mathematica": [
				"ContinuedFraction[Pi, 98]"
			],
			"program": [
				"(PARI) contfrac(Pi) \\\\ contfracpnqn(%) is also useful!",
				"(PARI) { allocatemem(932245000); default(realprecision, 21000); x=contfrac(Pi); for (n=1, 20000, write(\"b001203.txt\", n, \" \", x[n])); } \\\\ _Harry J. Smith_, Apr 14 2009",
				"(Sage) continued_fraction(RealField(333)(pi)) # _Peter Luschny_, Feb 16 2015"
			],
			"xref": [
				"Cf. A000796 for decimal expansion. See A007541 or A033089, A033090 for records.",
				"Cf. A097545, A097546."
			],
			"keyword": "nonn,nice,cofr",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Word \"Simple\" added to the title by _David Covert_, Dec 06 2016"
			],
			"references": 52,
			"revision": 136,
			"time": "2021-10-22T11:34:50-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}