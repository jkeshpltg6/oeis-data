{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A071093",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 71093,
			"data": "1,2,6,2196,37004,2317631400,216893681800,2326335506123418128,1208982377794384163088,2220650888749669503773432361504,6408743336016148761893699822360672,2015895925780490675949731718780144934779733312,32307672245407537492814937397129549558917000333504",
			"name": "Number of perfect matchings in triangle graph with n nodes per side as n runs through numbers congruent to 0 or 3 mod 4.",
			"reference": [
				"J. Propp, Enumeration of matchings: problems and progress, pp. 255-291 in L. J. Billera et al., eds, New Perspectives in Algebraic Combinatorics, Cambridge, 1999 (see Problem 17)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A071093/b071093.txt\"\u003eTable of n, a(n) for n = 0..44\u003c/a\u003e",
				"J. Propp, \u003ca href=\"http://arxiv.org/abs/math.CO/9801061\"\u003eTwenty open problems in enumeration of matchings\u003c/a\u003e, arXiv:math/9801061 [math.CO], 1998-1999.",
				"J. Propp, \u003ca href=\"http://faculty.uml.edu/jpropp/update.pdf\"\u003eUpdated article\u003c/a\u003e",
				"J. Propp, Enumeration of matchings: problems and progress, in L. J. Billera et al. (eds.), \u003ca href=\"http://www.msri.org/publications/books/Book38/contents.html\"\u003eNew Perspectives in Algebraic Combinatorics\u003c/a\u003e"
			],
			"formula": [
				"a(2n) = A039907(4n) = A178446(4n), a(2n+1) = A039907(4n+3) = A178446(4n+3). - _Andrew Howroyd_, Mar 06 2016"
			],
			"maple": [
				"with(LinearAlgebra): b:= proc(n) option remember; local l, ll, i, j, h0, h1, M; if n=0 then return 1 fi; if n\u003c0 or member(irem(n, 4), [1, 2]) then return 0 fi; l:= []; for j from 1 to n-1 do h0:= j*(j-1)/2+1; h1:= j*(j+1)/2+1; for i from 1 to j do l:= [l[], [h1, h1+1]]; if irem(i, 2)=1 then l:= [l[], [h1, h0]]; h1:= h1+1; l:=[l[], [h1, h0]]; h0:=h0+1 else l:= [l[], [h0, h1]]; h1:= h1+1; l:=[l[], [h0, h1]]; h0:=h0+1 fi od od; M:= Matrix((n+1)*n/2); for ll in l do M[ll[1], ll[2]]:= 1; M[ll[2], ll[1]]:= -1 od: isqrt(Determinant(M)); end: a:= n-\u003e b(2*n +irem(n, 2)): seq(a(n), n=0..10); # _Alois P. Heinz_, May 08 2010"
			],
			"mathematica": [
				"b[n_] := b[n] = Module[{l, ll, i, j, h0, h1, M}, If[n == 0 , Return[1]]; If[n\u003c0 || MatchQ[Mod[n, 4], 1|2] , Return[0]]; l = {}; For[j = 1, j \u003c= n-1, j++, h0 = j*(j-1)/2+1; h1 = j*(j+1)/2+1; For[i = 1, i \u003c= j, i++, AppendTo[l, {h1, h1+1}]; If[Mod[i, 2] == 1, AppendTo[l, {h1, h0}]; h1++; AppendTo[l, {h1, h0}]; h0++ , AppendTo[l, {h0, h1}]; h1++; AppendTo[l, {h0, h1}]; h0++ ]]]; M[_, _] = 0; (M[#[[1]], #[[2]]] = 1; M[#[[2]], #[[1]]] = -1)\u0026 /@ l; Sqrt[Det[Array[M, {n*(n+1)/2, n*(n+1)/2}]]]]; a[n_] := b[2*n + Mod[n, 2]]; Table[a[n], {n, 0, 12}] (* _Jean-François Alcover_, Apr 29 2014, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A039907, A178446, A269869."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"a(9)-a(10) from _Alois P. Heinz_, May 08 2010",
				"a(11)-a(12) from _Alois P. Heinz_, Jan 12 2014"
			],
			"references": 3,
			"revision": 31,
			"time": "2020-03-05T23:57:03-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}