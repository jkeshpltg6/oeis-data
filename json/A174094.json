{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A174094",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 174094,
			"data": "2,2,3,5,4,6,12,10,14,26,26,34,68,48,72,120,120,168,336,264,396,792,624,816,1632,1632,2208,3616,3616,5056,10112,6592,9888,19776,19776,24384,48768,48768,73152,112320,76032,114048,228096,190080,264960,529920",
			"name": "Number of ways to choose n positive integers less than or equal to 2n such that none of the n integers divides another.",
			"comment": [
				"a(n) \u003e= 2^(1+floor((n-1)/3)). - _Robert Israel_, Aug 25 2015"
			],
			"reference": [
				"F. Caldarola, G. d'Atri, M.Pellegrini, Combinatorics on n-sets: Arithmetic properties and numerical results. In: Sergeyev Y., Kvasov D. (eds) Numerical Computations: Theory and Algorithms. NUMTA 2019. Lecture Notes in Computer Science, vol. 11973. Springer, Cham, 389-401."
			],
			"link": [
				"Bertram Felgenhauer, \u003ca href=\"/A174094/b174094.txt\"\u003eTable of n, a(n) for n = 1..3400\u003c/a\u003e (terms 1..100 from Marco Pellegrini)",
				"C. Bindi, L. Bussoli, M. Grazzini, M. Pellegrini, G. Pirillo, M. A. Pirillo, A. Troise, \u003ca href=\"http://parsec2.unina2.it/ojs/index.php/periodico/article/view/285\"\u003eSu un risultato di uno studente di Erdös\u003c/a\u003e, Periodico di matematiche 1 (2016), Vol 8, No 1 (2016): Serie XII Anno CXXVI.",
				"Bertram Felgenhauer, \u003ca href=\"/A174094/a174094.hs.txt\"\u003eHaskell program for computing A174094\u003c/a\u003e.",
				"Hong Liu, Péter Pál Pach, Richárd Palincza, \u003ca href=\"https://arxiv.org/abs/1805.06341\"\u003eThe number of maximum primitive sets of integers\u003c/a\u003e, arXiv:1805.06341 [math.CO], 2018.",
				"Sujith Vijay, \u003ca href=\"https://arxiv.org/abs/1804.01740\"\u003eOn large primitive subsets of {1,2,...,2n}\u003c/a\u003e, arXiv:1804.01740 [math.CO], 2018."
			],
			"example": [
				"a(1) = 2 because we can choose {1}, {2}.",
				"a(2) = 2 because we can choose {2, 3}, {3, 4}.",
				"a(3) = 3 because we can choose {2, 3, 5}, {3, 4, 5}, {4, 5, 6}."
			],
			"maple": [
				"F:= proc(S,m)",
				"  option remember;",
				"  local s,S1,S2;",
				"  if nops(S) \u003c m then return 0 fi;",
				"  if m = 1 then return nops(S) fi;",
				"  s:= min(S);",
				"  S1:= S minus {s};",
				"  S2:= S minus {seq(j*s,j=1..floor(max(S)/s))};",
				"  F(S1, m) + F(S2, m-1);",
				"end proc;",
				"seq(F({$1..2*n},n), n=1..37); # _Robert Israel_, Aug 25 2015"
			],
			"mathematica": [
				"F[S_List, m_] := F[S, m] = Module[{s, S1, S2}, If[Length[S]\u003cm, Return[0]]; If[m == 1, Return[Length[S]]]; s = Min[S]; S1 = S ~Complement~ {s}; S2 = S ~Complement~ Table[j*s, {j, 1, Floor[Max[S]/s]}]; F[S1, m] + F[S2, m - 1]];",
				"a[n_] := F[Range[2n], n];",
				"Table[an = a[n]; Print[\"a(\", n, \") = \", an]; an, {n, 1, 37}] (* _Jean-François Alcover_, Jul 10 2018, after _Robert Israel_ *)"
			],
			"xref": [
				"The smallest n integers possible is A174063."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_David Brown_, Mar 07 2010",
			"ext": [
				"More terms from _David Brown_, Mar 14 2010",
				"a(30)-a(46) from _Ray Chandler_, Mar 19 2010"
			],
			"references": 4,
			"revision": 46,
			"time": "2020-03-09T14:42:08-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}