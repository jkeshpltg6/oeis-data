{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A288797",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 288797,
			"data": "0,1,1,4,4,4,9,5,5,9,16,12,12,12,16,25,17,13,13,17,25,36,28,20,24,20,28,36,49,37,33,25,25,33,37,49,64,52,40,36,40,36,40,52,64,81,65,53,45,41,41,45,53,65,81,100,84,72,64,52,60,52,64,72,84,100",
			"name": "Square array a(p,q) = p^2 + q^2 - 2*p - 2*q + 2*gcd(p,q), p \u003e= 1, q \u003e= 1, read by antidiagonals.",
			"comment": [
				"In the Cartesian plane, let r(p,q) denote the rotation with center origin and angle associated to slope p/q (p: number of units upwards, q: number of units towards the right).",
				"Let R(p,q) be the square of area p^2 + q^2 = R^2, with vertices (0,0), (0,R), (R,R), (R,0).",
				"The natural unit squares (i.e., the (a,a+1) X (b,b+1) Cartesian products, with a and b integers) are transformed by r(p,q) into rotated unit squares.",
				"a(p,q) is the number of rotated unit squares that fully land inside R(p,q)."
			],
			"link": [
				"Luc Rousseau, \u003ca href=\"/A288797/a288797_example2.png\"\u003eIllustration for p=6, q=4\u003c/a\u003e"
			],
			"formula": [
				"a(p,q) = p^2 + q^2 - 2*p - 2*q + 2*gcd(p,q).",
				"a(p,1) = a(1,p) = (p-1)^2 = A000290(p-1).",
				"a(p,p) = 2*p*(p-1) = 4*A000217(p-1).",
				"a(p,p+1) = 2*p*(p-1)+1 = A001844(p-1).",
				"a(p,p+2) = 2*p^2+2*gcd(2,p) = 2*p^2+3+(-1)^(p) = 4*A099392(p-1) = 4*A080827(p).",
				"a(p,p+3) = 2*p^2+2*p+5+4*A079978(p) = 1+4*(1+A143101(p)).",
				"a(p,2*p) = p*(5*p-4) = A051624(p).",
				"a(p,3*p) = 2*p*(5*p-3) = 4*A000566(p)."
			],
			"example": [
				"Table begins:",
				"    0   1   4   9  16  25 ...",
				"    1   4   5  12  17  28 ...",
				"    4   5  12  13  20  33 ...",
				"    9  12  13  24  25  36 ...",
				"   16  17  20  25  40  41 ...",
				"   25  28  33  36  41  60 ...",
				"  ... ... ... ... ... ... ..."
			],
			"mathematica": [
				"A[p_, q_] := p^2 + q^2 - 2*p - 2*q + 2*GCD[p, q];",
				"(* or, checking without the formula: *)",
				"okQ[{a_, b_}, p_, q_] := Module[{r2 = p^2 + q^2}, 0 \u003c= a*q - b*p \u003c= r2 \u0026\u0026 0 \u003c= a*p + b*q \u003c= r2 \u0026\u0026 0 \u003c= a*q - b*p + q \u003c= r2 \u0026\u0026 0 \u003c= a*p + b*q + p \u003c= r2 \u0026\u0026 0 \u003c= a*q - (b + 1)*p \u003c= r2 \u0026\u0026 0 \u003c= a*p + b*q + q \u003c= r2 \u0026\u0026 0 \u003c= (a + 1)*q - (b + 1)*p \u003c= r2 \u0026\u0026 0 \u003c= a*p + b*q + p + q \u003c= r2];",
				"A[p_, q_] := Module[{r}, r = Reduce[okQ[{a, b}, p, q], {a, b}, Integers]; If[r[[0]] === And, 1, Length[r]]];",
				"Flatten[Table[A[p - q + 1, q], {p, 1, 11}, {q, 1, p}]] (* _Jean-François Alcover_, Jun 17 2017 *)"
			],
			"program": [
				"(PARI)",
				"a(p,q)=p^2+q^2-2*p-2*q+2*gcd(p,q)",
				"for(n=2,12,for(p=1,n-1,{q=n-p;print(a(p,q))}))"
			],
			"xref": [
				"Cf. A000217, A000290, A000566, A001844, A003989, A051624, A080827, A099392, A143101."
			],
			"keyword": "tabl,nonn,easy",
			"offset": "1,4",
			"author": "_Luc Rousseau_, Jun 16 2017",
			"references": 1,
			"revision": 39,
			"time": "2018-03-31T14:48:03-04:00",
			"created": "2017-06-20T23:38:10-04:00"
		}
	]
}