{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A320060",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 320060,
			"data": "-1,-1,-1,1,5,-4,-1,1,1,1,-6,1,-1,-1,-23,-1,1,1,1,27,-1,-1,1,22,-1,1,1,-1,15,-1,1,37,-1,-1,1,28,-1,-1,80,-1,-1,1,-81,14,-1,1,1,1,1,-89,-1,1,1,16,1,-1,1,60,1,-1,-138,1,1,-25,-114,1,148,1,1,-42,-1,-1,-104,-1,1,-1,63,-1,-1",
			"name": "Least residue greater than -prime(n)/2 of the product Product_{i,j}(i^2-(i+j)*j) modulo prime(n), where i and j run over {1,...,(prime(n)-1)/2} with i^2-(i+j)*j not divisible by prime(n).",
			"comment": [
				"Conjecture 1. Let p be an odd prime and let f(p) be the product of i^2-(i+j)*j, where i and j run over {1,...,(p-1)/2} with i^2-(i+j)*j not divisible by p. If p == 1, 9 (mod 20), then f(p) == -5^((p-1)/4) (mod p). If p == 11 (mod 20) or p == 23, 27 (mod 40), then f(p) == 1 (mod p). If p == 19 (mod 20) or p == 3, 7 (mod 40), then f(p) == -1 (mod p). If p == 13 (mod 20) then f(p) == (-1)^(floor((p+10)/20))*5^((p-1)/4) (mod p); if p == 17 (mod 20) then f(p) == (-1)^(floor((p-10)/20))*5^((p-1)/4) (mod p).",
				"Conjecture 2. Let p be any prime congruent to 1 modulo 4, and let a and b be integers with a*b == -1 (mod p). Let F(a,b) be the product of (i-a*j)*(i-b*j), where i and j run over {1,...,(p-1)/2} with (i-a*j)(i-b*j) not divisible by p. If a-b is not divisible by p, then -F(a,b) is congruent to the Legendre symbol ((a-b)/p) modulo p. If a == b (mod p) and p == 1 (mod 8), then F(a,b) == (-1)^((p+7)/8)*((p-1)/2)！(mod p). If p == 5 (mod 8) and a == b == (-1)^k*((p-1)/2)! (mod p) with k in {0,1}, then F(a,b) == (-1)^(k+(p-5)/8) (mod p).",
				"These two conjectures were motivated by Theorem 1.2(ii) in the author's preprint arXiv:1809.07766.",
				"The author has proved Conjecture 2 fully and Conjecture 1 in the case p == 1,9 (mod 10) in arXiv:1810.12102. Here we add a new general conjecture which implies Conjecture 1 in the case p == 3,7 (mod 10).",
				"Conjecture 3. Let A be an integer, and let p be an odd prime with A^2+4 a quadratic nonresidue modulo p. Let f(p,A) be the product Prod_{i,j=1,...,(p-1)/2}(i^2-Aij-j^2). If p == 1 (mod 4), then f(p,A) == (-A^2-4)^((p-1)/4) (mod p). If p == 3 (mod 4) then f(p,A) == (-A^2-4)^((p+1)/4)*u_{(p+1)/2}(A)/2 (mod p), where u_0(A) = 0, u_1(A) = 1, and u_{n+1}(A) = A*u_n(A) + u_{n-1}(A) for n = 1,2,3,.... - _Zhi-Wei Sun_, Oct 30 2018"
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A320060/b320060.txt\"\u003eTable of n, a(n) for n = 2..400\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1809.07766\"\u003eQuadratic residues and related permutations and identities\u003c/a\u003e, arXiv:1809.07766 [math.NT], 2018.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1810.12102\"\u003eOn quadratic residues and quartic residues modulo primes\u003c/a\u003e, arXiv:1810.12102 [math.NT], 2018."
			],
			"example": [
				"a(4) = -1 since prime(4) = 7 does not divide i^2-(i+j)*j for any i,j = 1,2,3, and Prod_{i,j = 1,2,3}(i^2-(i+j)*j) = -108900 == -1 (mod 7)."
			],
			"mathematica": [
				"rMod[m_,n_]:=rMod[m,n]=Mod[m,n,-n/2];",
				"a[p_]:=a[p]=rMod[Product[If[rMod[i^2-(i+j)*j,p]==0,1,i^2-(i+j)*j],{i,1,(p-1)/2},{j,1,(p-1)/2}],p];",
				"Table[a[Prime[n]],{n,2,80}]"
			],
			"xref": [
				"Cf. A000040, A000290, A319311."
			],
			"keyword": "sign",
			"offset": "2,5",
			"author": "_Zhi-Wei Sun_, Oct 18 2018",
			"references": 1,
			"revision": 36,
			"time": "2021-01-03T15:07:01-05:00",
			"created": "2018-10-19T03:27:27-04:00"
		}
	]
}