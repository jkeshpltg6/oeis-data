{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002859",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2859,
			"id": "M2303 N0909",
			"data": "1,3,4,5,6,8,10,12,17,21,23,28,32,34,39,43,48,52,54,59,63,68,72,74,79,83,98,99,101,110,114,121,125,132,136,139,143,145,152,161,165,172,176,187,192,196,201,205,212,216,223,227,232,234,236,243,247,252,256,258",
			"name": "a(1) = 1, a(2) = 3; for n \u003e= 3, a(n) is smallest number that is uniquely of the form a(j) + a(k) with 1 \u003c= j \u003c k \u003c n.",
			"comment": [
				"An Ulam-type sequence - see A002858 for many further references, comments, etc."
			],
			"reference": [
				"S. R. Finch, Mathematical Constants, Cambridge, 2003, pp. 145-151.",
				"R. K. Guy, Unsolved Problems in Number Theory, Section C4.",
				"R. K. Guy, \"s-Additive sequences,\" preprint, 1994.",
				"C. Pickover, Mazes for the Mind, St. Martin's Press, NY, 1992, p. 358.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"S. M. Ulam, Problems in Modern Mathematics, Wiley, NY, 1960, p. ix."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A002859/b002859.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Steven R. Finch, \u003ca href=\"/FinchSadd.html\"\u003eUlam s-Additive Sequences\u003c/a\u003e [From Steven Finch, Apr 20 2019]",
				"Raymond Queneau, \u003ca href=\"https://doi.org/10.1016/0097-3165(72)90083-0\"\u003eSur les suites s-additives\u003c/a\u003e, J. Combin. Theory A 12(1) (1972), 31-71.",
				"N. J. A. Sloane, \u003ca href=\"/A001149/a001149.pdf\"\u003eHandwritten notes on Self-Generating Sequences, 1970\u003c/a\u003e (note that A1148 has now become A005282)",
				"S. M. Ulam, \u003ca href=\"/A002858/a002858.pdf\"\u003eOn some mathematical problems connected with patterns of growth of figures\u003c/a\u003e, pp. 215-224 of R. E. Bellman, ed., Mathematical Problems in the Biological Sciences, Proc. Sympos. Applied Math., Vol. 14, Amer. Math. Soc., 1962. [Annotated scanned copy]",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/UlamSequence.html\"\u003eUlam Sequence\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Ulam_number\"\u003eUlam number\u003c/a\u003e.",
				"\u003ca href=\"/index/U#Ulam_num\"\u003eIndex entries for Ulam numbers\u003c/a\u003e."
			],
			"example": [
				"7 is missing since 7 = 1 + 6 = 3 + 4; but 8 is present since 8 = 3 + 5 has a unique representation."
			],
			"mathematica": [
				"s = {1, 3}; Do[ AppendTo[s, n = Last[s]; While[n++; Length[ DeleteCases[ Intersection[s, n-s], n/2, 1, 1]] != 2]; n], {60}]; s (* _Jean-François Alcover_, Oct 20 2011 *)"
			],
			"program": [
				"(Haskell)",
				"a002859 n = a002859_list !! (n-1)",
				"a002859_list = 1 : 3 : ulam 2 3 a002859_list",
				"-- Function ulam as defined in A002858.",
				"-- _Reinhard Zumkeller_, Nov 03 2011"
			],
			"xref": [
				"Cf. A002858 (version beginning 1,2), A199118, A199119."
			],
			"keyword": "nonn,nice",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, _Mira Bernstein_",
			"references": 10,
			"revision": 44,
			"time": "2021-12-22T00:07:54-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}