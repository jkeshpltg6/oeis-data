{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A307707",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 307707,
			"data": "0,1,1,1,2,1,2,2,2,2,2,3,2,3,2,3,3,3,3,3,3,3,4,3,4,3,4,3,4,4,4,4,4,4,4,4,4,5,4,5,4,5,4,5,4,5,5,5,5,5,5,5,5,5,5,5,6,5,6,5,6,5,6,5,6,5,6,6,6,6,6,6,6,6,6,6,6,6,6,7,6,7,6,7,6,7,6,7,6,7,6,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,8",
			"name": "Lexicographically earliest sequence starting with a(1) = 0 such that a(n) is the number of pairs of contiguous terms whose sum is a(n).",
			"comment": [
				"In order to avoid the trivial case \"0 followed by 1's\", the sum of every pair of consecutive terms must appear in the sequence. - _Rémy Sigrist_, Apr 24 2019",
				"From _Paul Curtz_, Apr 27 2019: This can be written as a triangle:",
				"                    0",
				"                  1   1",
				"                1   2   1",
				"              2   2   2   2",
				"            2   3   2   3   2",
				"          3   3   3   3   3   3",
				"        3   4   3   4   3   4   3",
				"..."
			],
			"link": [
				"Jean-Marc Falcoz, \u003ca href=\"/A307707/b307707.txt\"\u003eTable of n, a(n) for n = 1..11326\u003c/a\u003e"
			],
			"formula": [
				"a(n) + a(n+1) = A002024(n). - _Rémy Sigrist_, Apr 24 2019"
			],
			"example": [
				"The sequence starts with 0,1,1,1,2,1,2,2,2,2,2,3,2,3,2...",
				"a(1) = 0 means that the sum [a(n) + a(n+1)] is never = 0;",
				"a(2) = 1 means that the sum [a(n) + a(n+1)] = 1 is true only once [this is the sum a(1) + a(2) = 0 + 1 = 1] ;",
				"...",
				"a(5) = 2 means that the sum [a(n) + a(n+1)] = 2 is true only twice [those are the sums a(2) + a(3) = 1 + 1 = 2 and a(3) + a(4) = 1 + 1 = 2];",
				"...",
				"a(12) = 3 means that the sum [a(n) + a(n+1)] = 3 is true only three times [those are the three sums a(4) + a(5) = 1 + 2 = 3; a(5) + a(6) = 2 + 1 = 3 and a(6) + a(7) = 1 + 2 = 3]; etc."
			],
			"mathematica": [
				"m = 107; a[1]=0;",
				"a24[n_] := Ceiling[(Sqrt[8n+1]-1)/2];",
				"Array[a, m] /. Solve[Table[a[n] + a[n+1] == a24[n], {n, 1, m-1}]][[1]] (* _Jean-François Alcover_, Jun 02 2019, after _Rémy Sigrist_'s formula *)"
			],
			"program": [
				"(PARI) v=0; rem=wanted=1; for (n=1, 107, print1 (v\", \"); v=wanted-v; if (rem--==0, rem=wanted++)) \\\\ _Rémy Sigrist_, Apr 23 2019"
			],
			"xref": [
				"Cf. A002024.",
				"Cf. also A007590, A057353, A106466 and A238410.",
				"A multiplicative version: A307720."
			],
			"keyword": "nonn,look",
			"offset": "1,5",
			"author": "_Eric Angelini_ and _Jean-Marc Falcoz_, Apr 23 2019",
			"references": 2,
			"revision": 29,
			"time": "2021-10-25T13:09:14-04:00",
			"created": "2019-04-24T00:02:09-04:00"
		}
	]
}