{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007226",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7226,
			"id": "M4701",
			"data": "2,3,10,42,198,1001,5304,29070,163438,937365,5462730,32256120,192565800,1160346492,7048030544,43108428198,265276342782,1641229898525,10202773534590,63698396932170,399223286267190,2510857763851185,15842014607109600",
			"name": "a(n) = 2*det(M(n; -1))/det(M(n; 0)), where M(n; m) is the n X n matrix with (i,j)-th element equal to 1/binomial(n + i + j + m, n).",
			"comment": [
				"For n \u003e= 1, a(n) is the number of distinct perforation patterns for deriving (v,b) = (n+1,n) punctured convolutional codes from (3,1). [Edited by _Petros Hadjicostas_, Jul 27 2020]",
				"Apparently Bégin's (1992) paper was presented at a poster session at the conference and was never published.",
				"a(n) is the total number of down steps between the first and second up steps in all 2-Dyck paths of length 3*(n+1). A 2-Dyck path is a nonnegative lattice path with steps (1,2), (1,-1) that starts and ends at y = 0. - _Sarah Selkirk_, May 07 2020",
				"From _Petros Hadjicostas_, Jul 27 2020: (Start)",
				"\"A punctured convolutional code is a high-rate code obtained by the periodic elimination (i.e., puncturing) of specific code symbols from the output of a low-rate encoder. The resulting high-rate code depends on both the low-rate code, called the original code, and the number and specific positions of the punctured symbols.\" (The quote is from Haccoun and Bégin (1989).)",
				"A high-rate code (v,b) (written as R = b/v) can be constructed from a low-rate code (v0,1) (written as R = 1/v0) by deleting from every v0*b code symbols a number of v0*b - v symbols (so that the resulting rate is R = b/v).",
				"Even though my formulas below do not appear in the two published papers in the IEEE Transactions on Communications, from the theory in those two papers, it makes sense to replace \"k|b\" with \"k|v0*b\" (and \"k|gcd(v,b)\" with \"k|gcd(v,v0*b)\"). Pab Ter, however, uses \"k|b\" in the Maple programs in the related sequences A007223, A007224, A007225, A007227, and A007229. (End)"
			],
			"reference": [
				"Guy Bégin, On the enumeration of perforation patterns for punctured convolutional codes, Séries Formelles et Combinatoire Algébrique}, 4th colloquium, 15-19 Juin 1992, Montréal, Université du Québec à Montréal, pp. 1-10.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A007226/b007226.txt\"\u003eTable of n, a(n) for n = 0..198\u003c/a\u003e",
				"A. Asinowski, B. Hackl, and S. Selkirk, \u003ca href=\"https://arxiv.org/abs/2007.15562\"\u003eDown step statistics in generalized Dyck paths\u003c/a\u003e, arXiv:2007.15562 [math.CO], 2020.",
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/2104.01644\"\u003eCentered polygon numbers, heptagons and nonagons, and the Robbins numbers\u003c/a\u003e, arXiv:2104.01644 [math.CO], 2021.",
				"Guy Bégin and David Haccoun, \u003ca href=\"https://doi.org/10.1109/26.44210\"\u003eHigh rate punctured convolutions codes: Structure properties and construction techniques\u003c/a\u003e, IEEE Transactions on Communications 37(12) (1989), 1381-1385.",
				"Ryan C. Chen, Yujin H. Kim, Jared D. Lichtman, Steven J. Miller, Shannon Sweitzer, and Eric Winsor, \u003ca href=\"https://arxiv.org/abs/1803.08127\"\u003eSpectral Statistics of Non-Hermitian Random Matrix Ensembles\u003c/a\u003e, arXiv:1803.08127 [math-ph], 2018.",
				"M. Dziemianczuk, \u003ca href=\"http://arxiv.org/abs/1410.5747\"\u003eOn Directed Lattice Paths With Additional Vertical Steps\u003c/a\u003e, arXiv:1410.5747 [math.CO], 2014.",
				"M. Dziemianczuk, \u003ca href=\"https://doi.org/10.1016/j.disc.2015.11.001\"\u003eOn Directed Lattice Paths With Additional Vertical Steps\u003c/a\u003e, Discrete Mathematics, 339(3) (2016), 1116-1139.",
				"I. Gessel and G. Xin, \u003ca href=\"https://arxiv.org/abs/math/0505217\"\u003eThe generating function of ternary trees and continued fractions\u003c/a\u003e, arXiv:math/0505217 [math.CO], 2005.",
				"N. S. S. Gu, H. Prodinger, and S. Wagner, \u003ca href=\"https://doi.org/10.1016/j.ejc.2009.10.007\"\u003eBijections for a class of labeled plane trees\u003c/a\u003e, Eur. J. Combinat. 31 (2010), 720-732, Theorem 2 at k = 2.",
				"David Haccoun and Guy Bégin, \u003ca href=\"https://doi.org/10.1109/26.46505\"\u003eHigh rate punctured convolutional codes for Viterbi and sequential coding\u003c/a\u003e, IEEE Transactions on Communications, 37(11) (1989), 1113-1125; see Section II.",
				"W. Mlotkowski and K. A. Penson, \u003ca href=\"http://arxiv.org/abs/1304.6544\"\u003eThe probability measure corresponding to 2-plane trees\u003c/a\u003e, arXiv:1304.6544 [math.PR], 2013.",
				"Jocelyn Quaintance, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2006.09.041\"\u003eCombinatoric Enumeration of Two-Dimensional Proper Arrays\u003c/a\u003e, Discrete Math., 307 (2007), 1844-1864."
			],
			"formula": [
				"a(n) = (2/(n + 1))*binomial(3*n, n).",
				"a(n) = 2*C(3*n, n) - C(3*n, n+1) for n \u003e= 0. - _David Callan_, Oct 25 2004",
				"a(n) = C(3*n, n)/(2*n + 1) + C(3*n + 1, n)/(n + 1) = C(3*n, n)/(2*n + 1) + 2*C(3*n + 1, n)/(2*n + 2) for n \u003e= 0. - _Paul Barry_, Nov 05 2006",
				"G.f.: g*(2 - g)/x, where g*(1 - g)^2 = x. - _Mark van Hoeij_, Nov 08 2011 [Thus, g = (4/3)*sin((1/3)*arcsin(sqrt(27*x/4)))^2. - _Petros Hadjicostas_, Jul 27 2020]",
				"Recurrence: 2*(n+1)*(2*n-1)*a(n) - 3*(3*n-1)*(3*n-2)*a(n-1) = 0 for n \u003e= 1. - _R. J. Mathar_, Nov 26 2012",
				"G.f.: (1 - 1/B(x))/x, where B(x) is the g.f. of A006013. [_Vladimir Kruchinin_, Mar 05 2013]",
				"G.f.: ( -16 * sin(asin((3^(3/2) * sqrt(x))/2)/3)^4 + 24 * sin(asin((3^(3/2) * sqrt(x))/2)/3)^2 ) / (9*x). [_Vladimir Kruchinin_, Nov 16 2013]",
				"From _Petros Hadjicostas_, Jul 27 2020: (Start)",
				"The number of perforation patterns to derive high-rate convolutional code (v,b) (written as R = b/v) from a given low-rate convolutional code (v0, 1) (written as R = 1/v0) is (1/b)*Sum_{k|gcd(v,b)} phi(k)*binomial(v0*b/k, v/k).",
				"According to Pab Ter's Maple code in the related sequences (see above), this is the coefficient of z^v in the polynomial (1/b)*Sum_{k|b} phi(k)*(1 + z^k)^(v0*b/k).",
				"Here (v,b) = (n+1,n) and (v0,1) = (3,1), so for n \u003e= 1,",
				"a(n) = (1/n)*Sum_{k|gcd(n+1,n)} phi(k)*binomial(3*n/k, (n+1)/k).",
				"This simplifies to",
				"a(n) = (1/n)*binomial(3*n, n+1) for n \u003e= 1. (End)"
			],
			"maple": [
				"A007226:=n-\u003e2*binomial(3*n,n)-binomial(3*n,n+1): seq(A007226(n), n=0..30); # _Wesley Ivan Hurt_, Aug 11 2014"
			],
			"mathematica": [
				"Table[2*Binomial[3n,n]-Binomial[3n,n+1], {n,0,20}] (* _Harvey P. Dale_, Aug 10 2014 *)"
			],
			"program": [
				"(MAGMA) [Binomial(3*n,n)/(2*n+1)+Binomial(3*n+1,n)/(n+1): n in [0..25]]; // _Vincenzo Librandi_, Aug 10 2014",
				"(PARI) a(n) = {my(M1=matrix(n,n)); my(M0=matrix(n,n)); for(i=1, n, for(j=1, n, M1[i,j] = 1/binomial(n+i+j-1,n); M0[i,j] = 1/binomial(n+i+j,n);)); 2*matdet(M1)/matdet(M0);} \\\\ _Petros Hadjicostas_, Jul 27 2020"
			],
			"xref": [
				"Cf. A006013, A007223, A007224, A007225, A007227, A007228, A007229, A124724."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_Simon Plouffe_",
			"ext": [
				"Edited following a suggestion of _Ralf Stephan_, Feb 07 2004",
				"Offset changed to 0 and all formulas checked by _Petros Hadjicostas_, Jul 27 2020"
			],
			"references": 19,
			"revision": 124,
			"time": "2021-06-28T16:25:56-04:00",
			"created": "1994-04-28T03:00:00-04:00"
		}
	]
}