{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A334110",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 334110,
			"data": "1,4,9,36,25,100,225,900,49,196,441,1764,1225,4900,11025,44100,121,484,1089,4356,3025,12100,27225,108900,5929,23716,53361,213444,148225,592900,1334025,5336100,169,676,1521,6084,4225,16900,38025,152100,8281,33124,74529,298116,207025,828100,1863225,7452900,20449,81796,184041",
			"name": "The squares of squarefree numbers (A062503), ordered lexicographically according to their prime factors. a(n) = Product_{k in I} prime(k+1)^2, where I are the indices of nonzero binary digits in n = Sum_{k in I} 2^k.",
			"comment": [
				"For the lexicographic ordering, the prime factors must be written in nonincreasing order. If we write the factors in nondecreasing order, we get a lexicographically ordered set with an order type that is greater than a natural number index - the resulting sequence does not include all qualifying numbers. (Note also that the symbols used for the lexicographic order are the prime numbers, not their digits.)",
				"a(n) is the n-th power of 4 in the monoid defined in A331590.",
				"Conjecture: a(n) is the position of the first occurrence of n in A334109."
			],
			"formula": [
				"a(n) = A019565(n)^2.",
				"For n \u003e= 1, a(A000079(n-1)) = A001248(n).",
				"For all n \u003e= 0, A334109(a(n)) = n.",
				"a(n+k) = A331590(a(n), a(k)).",
				"a(n XOR k) = A059897(a(n), a(k)), where XOR denotes bitwise exclusive-or, A003987.",
				"a(n) = A225546(3^n).",
				"a(2n) = A003961(a(n)).",
				"a(2n+1) = 4 * a(2n).",
				"a(2^k-1) = A061742(k).",
				"A267116(a(n)) = 2.",
				"A048675(a(n)) = 2n.",
				"A097248(a(n)) = A332382(n) = A019565(2n)."
			],
			"example": [
				"The initial terms are shown below, equated with the product of their prime factors to exhibit the lexicographic ordering. The list starts with 1, since 1 is factored as the empty product and the empty list is first in lexicographic order.",
				"    1 = .",
				"    4 = 2*2.",
				"    9 = 3*3.",
				"   36 = 3*3*2*2.",
				"   25 = 5*5.",
				"  100 = 5*5*2*2.",
				"  225 = 5*5*3*3.",
				"  900 = 5*5*3*3*2*2.",
				"   49 = 7*7.",
				"  196 = 7*7*2*2.",
				"  441 = 7*7*3*3."
			],
			"mathematica": [
				"Array[If[# == 0, 1, Times @@ Flatten@ Map[Function[{p, e}, Map[Prime[Log2@ # + 1]^(2^(PrimePi@ p - 1)) \u0026, DeleteCases[NumberExpand[e, 2], 0]]] @@ # \u0026, FactorInteger[3^#]]] \u0026, 51, 0] (* _Michael De Vlieger_, May 26 2020 *)"
			],
			"program": [
				"(PARI) A334110(n) = { my(p=2,m=1); while(n, if(n%2, m *= p^2); n \u003e\u003e= 1; p = nextprime(1+p)); (m); };"
			],
			"xref": [
				"Cf. A000079, A019565 (square roots), A048675, A097248, A225546, A267116, A332382, A334109 (a left inverse).",
				"Column 2 of A329332. Permutation of A062503.",
				"After 1, the right children of the leftmost edge of A334860, or respectively, the left children of the rightmost edge of A334866.",
				"Subsequences: A001248, A061742, A166329.",
				"Subsequence of A052330.",
				"A003961, A003987, A059897, A331590 are used to express relationship between terms of this sequence."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Antti Karttunen_ and _Peter Munn_, May 01 2020",
			"references": 4,
			"revision": 40,
			"time": "2020-06-11T23:43:11-04:00",
			"created": "2020-06-11T23:43:11-04:00"
		}
	]
}