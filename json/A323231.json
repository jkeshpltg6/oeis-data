{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A323231",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 323231,
			"data": "1,2,1,1,2,1,1,2,2,1,1,2,3,2,1,1,2,4,4,2,1,1,2,5,7,5,2,1,1,2,6,11,11,6,2,1,1,2,7,16,21,16,7,2,1,1,2,8,22,36,36,22,8,2,1,1,2,9,29,57,71,57,29,9,2,1,1,2,10,37,85,127,127,85,37,10,2,1",
			"name": "A(n, k) = [x^k] (1/(1-x) + x/(1-x)^n), square array read by descending antidiagonals for n, k \u003e= 0.",
			"reference": [
				"R. L. Graham, D. E. Knuth and O. Patashnik, Concrete Mathematics. Addison-Wesley, Reading, MA, 1990, p. 154."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A323231/b323231.txt\"\u003eAntidiagonals n = 0..50, flattened\u003c/a\u003e"
			],
			"formula": [
				"A(n, k) = binomial(n + k - 2, k - 1) + 1. Note that binomial(n, n) = 0 if n \u003c 0.",
				"A(n, k) = A(k, n) with the exception A(1,0) != A(0,1).",
				"A(n, n) = binomial(2*n-2, n-1) + 1 = A323230(n).",
				"From _G. C. Greubel_, Dec 27 2021: (Start)",
				"T(n, k) = binomial(n-2, k-1) + 1 with T(n, 0) = 1 + [n=1], T(n, n) = 1.",
				"T(2*n, n) = A323230(n).",
				"Sum_{k=0..n} T(n,k) = n + 1 + 2^(n-2) - [n=0]/4 + [n=1])/2. (End)"
			],
			"example": [
				"Array starts:",
				"[0] 1, 2,  1,  1,   1,   1,    1,    1,    1,     1,     1, ...",
				"[1] 1, 2,  2,  2,   2,   2,    2,    2,    2,     2,     2, ... A040000",
				"[2] 1, 2,  3,  4,   5,   6,    7,    8,    9,    10,    11, ... A000027",
				"[3] 1, 2,  4,  7,  11,  16,   22,   29,   37,    46,    56, ... A000124",
				"[4] 1, 2,  5, 11,  21,  36,   57,   85,  121,   166,   221, ... A050407",
				"[5] 1, 2,  6, 16,  36,  71,  127,  211,  331,   496,   716, ... A145126",
				"[6] 1, 2,  7, 22,  57, 127,  253,  463,  793,  1288,  2003, ... A323228",
				"[7] 1, 2,  8, 29,  85, 211,  463,  925, 1717,  3004,  5006, ...",
				"[8] 1, 2,  9, 37, 121, 331,  793, 1717, 3433,  6436, 11441, ...",
				"[9] 1, 2, 10, 46, 166, 496, 1288, 3004, 6436, 12871, 24311, ...",
				".",
				"Read as a triangle (by descending antidiagonals):",
				"                                     1",
				"                                  2,   1",
				"                                1,   2,   1",
				"                             1,   2,   2,   1",
				"                           1,   2,   3,   2,   1",
				"                        1,   2,   4,   4,   2,   1",
				"                      1,   2,   5,   7,   5,   2,  1",
				"                    1,  2,   6,  11,  11,   6,   2,  1",
				"                  1,  2,   7,  16,  21,  16,   7,  2,  1",
				"                1,  2,  8,  22,  36,  36,  22,   8,  2,  1",
				"              1,  2,  9,  29,  57,  71,  57,  29,  9,  2,  1",
				".",
				"A(0, 1) = C(-1, 0) + 1 = 2 because C(-1, 0) = 1. A(1, 0) = C(-1, -1) + 1 = 1 because C(-1, -1) = 0. Warning: Some computer algebra programs (for example Maple and Mathematica) return C(n, n) = 1 for n \u003c 0. This contradicts the definition given by Graham et al. (see reference). On the other hand this definition preserves symmetry."
			],
			"maple": [
				"Binomial := (n, k) -\u003e `if`(n \u003c 0 and n = k, 0, binomial(n,k)):",
				"A := (n, k) -\u003e Binomial(n + k - 2, k - 1) + 1:",
				"seq(lprint(seq(A(n, k), k=0..10)), n=0..10);"
			],
			"mathematica": [
				"T[n_, k_]:= If[k==0, 1 + Boole[n==1], If[k==n, 1, Binomial[n-2, k-1] + 1]];",
				"Table[T[n, k], {n,0,15}, {k,0,n}]//Flatten (* _G. C. Greubel_, Dec 27 2021 *)"
			],
			"program": [
				"(Sage)",
				"def Arow(n):",
				"    R.\u003cx\u003e = PowerSeriesRing(ZZ, 20)",
				"    gf = 1/(1-x) + x/(1-x)^n",
				"    return gf.padded_list(10)",
				"for n in (0..9): print(Arow(n))",
				"(Julia)",
				"using AbstractAlgebra",
				"function Arow(n, len)",
				"    R, x = PowerSeriesRing(ZZ, len+2, \"x\")",
				"    gf = inv(1-x) + divexact(x, (1-x)^n)",
				"    [coeff(gf, k) for k in 0:len-1] end",
				"for n in 0:9 println(Arow(n, 11)) end"
			],
			"xref": [
				"Differs from A323211 only in the second term.",
				"Rows include: A040000, A000027, A000124, A050407, A145126, A323228.",
				"Diagonals A(n, n+d): A323230 (d=0), A260878 (d=1), A323229 (d=2).",
				"Antidiagonal sums are A323227(n) if n!=1.",
				"Cf. A007318 (Pascal's triangle)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,2",
			"author": "_Peter Luschny_, Feb 10 2019",
			"references": 2,
			"revision": 22,
			"time": "2021-12-27T08:24:10-05:00",
			"created": "2019-02-13T04:21:35-05:00"
		}
	]
}