{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A112851",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 112851,
			"data": "0,0,3,21,81,231,546,1134,2142,3762,6237,9867,15015,22113,31668,44268,60588,81396,107559,140049,179949,228459,286902,356730,439530,537030,651105,783783,937251,1113861,1316136,1546776,1808664,2104872,2438667,2813517,3233097",
			"name": "a(n) = (n-1)*n*(n+1)*(n+2)*(2*n+1)/40.",
			"comment": [
				"A112851 is the fourth sequence in A112852.",
				"Also the Wiener index of the (n-1)-triangular grid graph (indexed so the 0-triangular grid graph is the singleton). - _Eric W. Weisstein_, Sep 08 2017"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A112851/b112851.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/TriangularGridGraph.html\"\u003eTriangular Grid Graph\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/WienerIndex.html\"\u003eWiener Index\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_06\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (6,-15,20,-15,6,-1)."
			],
			"formula": [
				"4*a(n+1) = 1*2^2*3 + 2*3^2*4 + 3*4^2*5 + ... (n terms). [Jolley, Summation of Series (1961), eq (54) page 10]",
				"a(n) = Sum_{i=0..n} A000217(i-1)*A000217(i), where A000217(-1)=0. - _Bruno Berselli_, Feb 05 2014",
				"a(n) = 6*a(n-1) - 15*a(n-2) + 20*a(n-3) - 15*a(n-4) + 6*a(n-5) - a(n-6) for n\u003e5, a(0)=a(1)=0, a(2)=3, a(3)=21, a(4)=81, a(5)=231. - _Harvey P. Dale_, Oct 28 2014",
				"G.f.: 3*x^2*(1 + x) / (1 - x)^6. - _Colin Barker_, Sep 08 2017",
				"a(n) = (1/2) * Sum_{k=0..n} C(k^2,2). - _Wesley Ivan Hurt_, Sep 23 2017",
				"a(n) = Sum_{i=0..n} A000217(i)*A033428(n-i). - _Bruno Berselli_, Mar 06 2018"
			],
			"maple": [
				"a:=n-\u003esum(j^4-j^2, j=0..n)/4: seq(a(n), n=0..36); # _Zerinvary Lajos_, May 08 2008"
			],
			"mathematica": [
				"Table[(n - 1) n (n + 1)(n + 2)(2 n + 1)/40, {n, 0, 30}] (* Josh Locker *)",
				"LinearRecurrence[{6, -15, 20, -15, 6, -1}, {0, 0, 3, 21, 81, 231}, 40] (* _Harvey P. Dale_, Oct 28 2014 *)"
			],
			"program": [
				"(MAGMA) [(n-1)*n*(n+1)*(n+2)*(2*n+1)/40: n in [0..40]]; // _Vincenzo Librandi_, Feb 06 2014",
				"(PARI) for(n=0,50, print1((n-1)*n*(n+1)*(n+2)*(2*n+1)/40, \", \")) \\\\ _G. C. Greubel_, Jul 23 2017",
				"(PARI) concat(vector(2), Vec(3*x^2*(1 + x) / (1 - x)^6 + O(x^30))) \\\\ _Colin Barker_, Sep 08 2017"
			],
			"xref": [
				"Partial sums of sequence A006011.",
				"Cf. A000217, A033428, A112852."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Alford Arnold_, Sep 24 2005",
			"ext": [
				"More terms from Josh Locker (jlocker(AT)mail.rochester.edu) and Michael W. Motily (mwm5036(AT)psu.edu), Oct 04 2005"
			],
			"references": 3,
			"revision": 45,
			"time": "2018-03-06T06:25:09-05:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}