{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A292876",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 292876,
			"data": "2,3,5,7,4,6,11,13,10,17,19,8,14,15,23,29,21,31,37,20,26,41,43,22,33,35,47,34,53,18,24,38,59,61,67,9,12,16,25,27,28,30,39,46,51,55,71,73,57,79,44,65,83,40,58,89",
			"name": "Irregular table whose n-th row lists all k such that A039654(k) = prime(n).",
			"comment": [
				"This can also be considered as a list of all orbits of A039653, ordered by their maximal element p = A039654(k) for any k of this orbit.",
				"Indeed, A039653(x) \u003e= x with equality iff x is prime, and all orbits of A039653 are conjectured to end in such a fixed point prime p = A039654(k) for any k in this orbit.",
				"Row lengths are given by A177343.",
				"This sequence is also a permutation of all integers \u003e 1, where each prime p(k) is immediately preceded by A177343(k)-1 composite numbers less than p(k). It follows that each composite is preceded either by a smaller composite or by a larger prime, and followed by a larger composite or prime. Thus, the primes appear in their natural order, but the composites do not.",
				"The first element of each row (i.e., the first column of this table) is given by A292874.",
				"We see (cf. a-file) that powers of 2 are often the first element (or at least part) of relatively long orbits: A177343(A000720(A039654(2^k))) = (1, 3, 4, 12, 25, 5, 10, 35, 61, 143, 143, 220, 365, ...)"
			],
			"link": [
				"M. F. Hasler, \u003ca href=\"/A292876/a292876_1.txt\"\u003eTable rows n = 1..1229\u003c/a\u003e"
			],
			"example": [
				"The table starts:",
				"    n  p(n)  { k | A039654(k) = p(n) }",
				"    1    2   { 2 }",
				"    2    3   { 3 }",
				"    3    5   { 5 }",
				"    4    7   { 7 }",
				"    5   11   { 4, 6, 11 }",
				"    6   13   { 13 }",
				"    7   17   { 10, 17 }",
				"    8   19   { 19 }",
				"    9   23   { 8, 14, 15, 23 }"
			],
			"program": [
				"(PARI) A292876(n,p=prime(n))=select(k-\u003eA039654(k)==p,[2..p]) \\\\ Not optimized nor efficient; mainly for illustrational purpose. - _M. F. Hasler_, Sep 25 2017"
			],
			"xref": [
				"Cf. A039654, A039653, A177343, A292874, A292112, A292113."
			],
			"keyword": "nonn,tabf",
			"offset": "1,1",
			"author": "_M. F. Hasler_, Sep 25 2017",
			"references": 1,
			"revision": 15,
			"time": "2017-09-27T09:22:00-04:00",
			"created": "2017-09-27T09:22:00-04:00"
		}
	]
}