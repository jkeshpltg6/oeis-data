{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A146291",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 146291,
			"data": "1,1,1,1,1,1,1,1,1,1,1,2,1,1,1,1,1,1,1,1,1,1,1,2,1,1,1,1,2,2,1,1,1,1,2,1,1,2,1,1,1,1,1,1,1,1,1,2,2,1,1,1,1,2,2,1,1,2,1,1,2,1,1,1,1,2,2,2,1,1,1,1,1,2,1,1,1,1,1,1,2,2,1,1,1,1,3,3,1,1,1,1,1,1,1,1,1,1,2,1,1,2,1,1,2",
			"name": "Triangle T(n,m) read by rows (n \u003e= 1, 0 \u003c= m \u003c= A001222(n)), giving the number of divisors of n with m prime factors (counted with multiplicity).",
			"comment": [
				"All rows are palindromic. T(n,0) = T(n,A001222(n)) = 1.",
				"Two numbers have identical rows in the table if and only if they have the same prime signature.",
				"If n is a perfect square then Sum_{even m} T(n,m) = 1 + Sum_{odd m} T(n,m), otherwise Sum_{even m} T(n,m) = Sum_{odd m} T(n,m). - _Geoffrey Critzer_, Feb 08 2015"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A146291/b146291.txt\"\u003eRows n = 1..2500, flattened\u003c/a\u003e",
				"Anonymous?, \u003ca href=\"http://xrjunque.nom.es/precis/polycalc.aspx\"\u003ePolynomial calculator\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Roundness.html\"\u003eRoundness\u003c/a\u003e",
				"G. Xiao, WIMS server, \u003ca href=\"http://wims.unice.fr/~wims/en_tool~algebra~factor.en.html\"\u003eFactoris\u003c/a\u003e (both expands and factors polynomials)"
			],
			"formula": [
				"If the canonical factorization of n into prime powers is the product of p^e(p), then T(n,m) is the coefficient of k^m in the polynomial expansion of Product_p (sum_{i=0..e} k^i)."
			],
			"example": [
				"Rows begin:",
				"1;",
				"1, 1;",
				"1, 1;",
				"1, 1, 1;",
				"1, 1;",
				"1, 2, 1;",
				"1, 1;",
				"1, 1, 1, 1;",
				"1, 1, 1;",
				"1, 2, 1;",
				"...",
				"12 has 1 divisor with 0 total prime factors (1), 2 with 1 (2 and 3), 2 with 2 (4 and 6) and 1 with 3 (12), for a total of 6. The 12th row of the table therefore reads (1, 2, 2, 1). These are the positive coefficients of the polynomial 1 + 2k + 2k^2 + (1)k^3 = (1 + k + k^2)(1 + k), derived from the prime factorization of 12 (namely, 2^2*3^1)."
			],
			"maple": [
				"with(numtheory):",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=0..degree(p)))(",
				"         add(x^bigomega(d), d=divisors(n))):",
				"seq(T(n), n=1..100);  # _Alois P. Heinz_, Feb 25 2015"
			],
			"mathematica": [
				"Join[{{1}},",
				"Table[nn = DivisorSigma[0, n];",
				"  CoefficientList[",
				"   Series[Product[(1 - x^i)/(1 - x), {i,",
				"FactorInteger[n][[All, 2]] + 1}], {x, 0, nn}], x], {n, 2, 100}]] (* _Geoffrey Critzer_, Jan 01 2015 *)"
			],
			"xref": [
				"Row sums equal A000005(n). T(n,1) = A001221(n) for n\u003e1.",
				"Row n of A007318 is identical to row A002110(n) of this table and also identical to the row for any squarefree number with n prime factors.",
				"Cf. A146292, A146289, A146290."
			],
			"keyword": "nonn,tabf",
			"offset": "1,12",
			"author": "_Matthew Vandermast_, Nov 11 2008",
			"references": 23,
			"revision": 22,
			"time": "2015-02-25T11:20:15-05:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}