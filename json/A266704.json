{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A266704",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 266704,
			"data": "-3,-19,17,-75,-165,-463,-1181,-3123,-8145,-21355,-55877,-146319,-383037,-1002835,-2625425,-6873483,-17994981,-47111503,-123339485,-322906995,-845381457,-2213237419,-5794330757,-15169754895,-39714933885,-103975046803,-272210206481",
			"name": "Coefficient of x^2 in minimal polynomial of the continued fraction [1^n,2/3,1,1,1,...], where 1^n means n ones.",
			"comment": [
				"See A265762 for a guide to related sequences."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A266704/b266704.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,2,-1)."
			],
			"formula": [
				"a(n) = 2*a(n-1) - 2*a(n-2) + a(n-3).",
				"G.f.:  (-3 - 13 x + 61 x^2 - 74 x^3 - 68 x^4 + 34 x^5)/(1 - 2 x - 2 x^2 + x^3).",
				"a(n) = 2^(-n)*(43*(-2)^n+2*(3-sqrt(5))^n*(-7+sqrt(5))-2*(3+sqrt(5))^n*(7+sqrt(5)))/5 for n\u003e2. - _Colin Barker_, Sep 29 2016"
			],
			"example": [
				"Let p(n,x) be the minimal polynomial of the number given by the n-th continued fraction:",
				"[2/3,1,1,1,...] = (1+3*sqrt(5))/6 has p(0,x) = -11 - 3 x + 9 x^2, so a(0) = 9;",
				"[1,2/3,1,1,...] = (19+9*sqrt(5))/22 has p(1,x) = -1 - 19 x + 11 x^2, so a(1) = 11;",
				"[1,1,2/3,1,...] = (-17+9*sqrt(5))/2 has p(2,x) = -29 + 17 x + x^2, so a(2) = 1."
			],
			"mathematica": [
				"u[n_] := Table[1, {k, 1, n}]; t[n_] := Join[u[n], {2/3}, {{1}}];",
				"f[n_] := FromContinuedFraction[t[n]];",
				"t = Table[MinimalPolynomial[f[n], x], {n, 0, 20}]",
				"Coefficient[t, x, 0] (* A266703 *)",
				"Coefficient[t, x, 1] (* A266704 *)",
				"Coefficient[t, x, 2] (* A266703 *)"
			],
			"program": [
				"(PARI) Vec(-(3+13*x-61*x^2+74*x^3+68*x^4-34*x^5)/((1+x)*(1-3*x+x^2)) + O(x^30)) \\\\ _Colin Barker_, Sep 29 2016"
			],
			"xref": [
				"Cf. A265762, A266703."
			],
			"keyword": "sign,easy",
			"offset": "0,1",
			"author": "_Clark Kimberling_, Jan 09 2016",
			"references": 3,
			"revision": 12,
			"time": "2016-09-29T14:56:36-04:00",
			"created": "2016-01-09T19:58:25-05:00"
		}
	]
}