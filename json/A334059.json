{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A334059",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 334059,
			"data": "1,0,1,1,2,0,5,8,2,0,36,49,19,1,0,329,414,180,22,0,0,3655,4398,1986,344,12,0,0,47844,55897,25722,5292,377,3,0,0,721315,825056,384366,87296,8746,246,0,0,0,12310199,13856570,6513530,1577350,192250,9436,90,0,0,0",
			"name": "Triangle read by rows: T(n,k) is the number of perfect matchings on {1, 2, ..., 2n} with k disjoint strings of adjacent short pairs.",
			"comment": [
				"Number of configurations with k connected components (consisting of domino matchings) in the game of memory played on the path of length 2n, see [Young]."
			],
			"link": [
				"Donovan Young, \u003ca href=\"https://arxiv.org/abs/2004.06921\"\u003ePolyomino matchings in generalised games of memory and linear k-chord diagrams\u003c/a\u003e, arXiv:2004.06921 [math.CO], 2020."
			],
			"formula": [
				"G.f.: Sum_{j\u003e=0} (2*j)! * y^j * (1-(1-z)*y)^(2*j+1) / (j! * 2^j * (1-(1-z)*y^2)^(2*j+1))."
			],
			"example": [
				"Triangle begins:",
				"   1;",
				"   0,  1;",
				"   1,  2,  0;",
				"   5,  8,  2, 0;",
				"  36, 49, 19, 1  0;",
				"  ...",
				"For n=2 and k=1 the configurations are (1,4),(2,3) (i.e. a single short pair) and (1,2),(3,4) (i.e. two adjacent short pairs); hence T(2,1) = 2."
			],
			"mathematica": [
				"CoefficientList[Normal[Series[Sum[y^j*(2*j)!/2^j/j!*((1-y*(1-z))/(1-y^2*(1-z)))^(2*j+1), {j, 0, 20}], {y, 0, 20}]], {y, z}]"
			],
			"program": [
				"(PARI)",
				"T(n)={my(v=Vec(sum(j=0, n, (2*j)! * x^j * (1-(1-y)*x + O(x*x^n))^(2*j+1) / (j! * 2^j * (1-(1-y)*x^2 + O(x*x^n))^(2*j+1))))); vector(#v, i, Vecrev(v[i], i))}",
				"{ my(A=T(8)); for(n=1, #A, print(A[n])) } \\\\ _Andrew Howroyd_, May 25 2020"
			],
			"xref": [
				"Row sums are A001147.",
				"Column k=0 is A278990 (which is also column 0 of A079267).",
				"Cf. A079267, A334056, A334057, A334058, A325753."
			],
			"keyword": "nonn,tabl",
			"offset": "0,5",
			"author": "_Donovan Young_, May 25 2020",
			"references": 2,
			"revision": 17,
			"time": "2020-05-27T04:33:22-04:00",
			"created": "2020-05-27T04:33:22-04:00"
		}
	]
}