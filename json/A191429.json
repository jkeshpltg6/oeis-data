{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A191429",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 191429,
			"data": "1,3,2,6,4,5,10,7,9,8,16,11,14,13,12,24,17,21,20,18,15,35,26,31,30,27,23,19,51,38,45,44,40,34,28,22,74,55,65,64,58,50,41,33,25,106,79,93,92,84,72,59,48,37,29,151,113,133,132,120,103,85,69,54,43,32,215,161,190,188,171,147,122,99,78,62,47,36",
			"name": "Dispersion of ([n*sqrt(2)+2]), where [ ]=floor, by antidiagonals.",
			"comment": [
				"Background discussion:  Suppose that s is an increasing sequence of positive integers, that the complement t of s is infinite, and that t(1)=1.  The dispersion of s is the array D whose n-th row is (t(n), s(t(n)), s(s(t(n)), s(s(s(t(n)))), ...).  Every positive integer occurs exactly once in D, so that, as a sequence, D is a permutation of the positive integers.  The sequence u given by u(n)=(number of the row of D that contains n) is a fractal sequence.  Examples:",
				"(1) s=A000040 (the primes), D=A114537, u=A114538.",
				"(2) s=A022343 (without initial 0), D=A035513 (Wythoff array), u=A003603.",
				"(3) s=A007067, D=A035506 (Stolarsky array), u=A133299.",
				"More recent examples of dispersions: A191426-A191455."
			],
			"example": [
				"Northwest corner:",
				"1...3...6...10..16",
				"2...4...7...11..17",
				"5...9...14..21..31",
				"8...13..20..30..44",
				"12..18..27..40..58"
			],
			"mathematica": [
				"(* Program generates the dispersion array T of increasing sequence f[n] *)",
				"r = 40; r1 = 12;  (* r=# rows of T to compute, r1=# rows to show *)",
				"c = 40; c1 = 12;   (* c=# cols to compute, c1=# cols to show *)",
				"x = Sqrt[2];",
				"f[n_] := Floor[n*x + 2] (* f(n) is complement of column 1 *)",
				"mex[list_] :=",
				"NestWhile[#1 + 1 \u0026, 1, Union[list][[#1]] \u003c= #1 \u0026, 1,",
				"  Length[Union[list]]]",
				"rows = {NestList[f, 1, c]};",
				"Do[rows = Append[rows, NestList[f, mex[Flatten[rows]], r]], {r}];",
				"t[i_, j_] := rows[[i, j]];  (* the array T *)",
				"TableForm[",
				"Table[t[i, j], {i, 1, 10}, {j, 1, 10}]] (* A191429 array *)",
				"Flatten[Table[",
				"  t[k, n - k + 1], {n, 1, c1}, {k, 1, n}]] (* A191429 sequence *)",
				"(* Program by _Peter J. C. Moses_, Jun 01 2011 *)"
			],
			"xref": [
				"Cf. A114537, A035513, A035506."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Clark Kimberling_, Jun 03 2011",
			"references": 2,
			"revision": 10,
			"time": "2014-02-14T00:28:02-05:00",
			"created": "2011-06-06T12:41:45-04:00"
		}
	]
}