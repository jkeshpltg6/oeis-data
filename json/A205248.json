{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A205248",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 205248,
			"data": "16,40,112,328,976,2920,8752,26248,78736,236200,708592,2125768,6377296,19131880,57395632,172186888,516560656,1549681960,4649045872,13947137608,41841412816,125524238440,376572715312,1129718145928,3389154437776",
			"name": "Number of (n+1) X 2 0..1 arrays with the number of clockwise edge increases in every 2 X 2 subblock the same.",
			"comment": [
				"Also, the number of cliques in the n-Apollonian network. Cliques in this graph have a maximum size of 4. - _Andrew Howroyd_, Sep 02 2017"
			],
			"link": [
				"R. H. Hardin, \u003ca href=\"/A205248/b205248.txt\"\u003eTable of n, a(n) for n = 1..210\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ApollonianNetwork.html\"\u003eApollonian Network\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Clique.html\"\u003eClique\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-3)."
			],
			"formula": [
				"a(n) = 4*a(n-1) - 3*a(n-2).",
				"From _Andrew Howroyd_, Sep 02 2017: (Start)",
				"a(n) = 4*(3^n + 1).",
				"G.f.: 8*x*(2 - 3*x)/((1 - x)*(1 - 3*x)).",
				"a(n) = 8*A007051(n).",
				"a(n) = 1 + A289521(n) + A067771(n) + A003462(n+1) + A003462(n).",
				"(End)"
			],
			"example": [
				"Some solutions for n=4:",
				"  1  0    0  1    1  1    0  1    1  1    1  1    1  0    1  0    1  1    1  1",
				"  0  1    0  0    1  1    0  1    0  1    0  1    0  1    0  0    1  1    1  1",
				"  1  0    1  1    1  1    0  1    0  1    0  0    1  0    0  1    1  1    1  1",
				"  0  1    1  0    1  1    0  0    0  1    1  0    0  1    1  1    1  1    1  1",
				"  1  0    0  0    1  1    0  1    1  1    1  1    1  0    0  1    1  1    1  1"
			],
			"mathematica": [
				"Table[4*(3^n + 1), {n, 1, 25}] (* _Jean-François Alcover_, Nov 01 2017, after _Andrew Howroyd_ *)",
				"4 (3^Range[30] + 1) (* _Eric W. Weisstein_, Nov 29 2017 *)",
				"LinearRecurrence[{4, -3}, {16, 40}, 30] (* _Eric W. Weisstein_, Nov 29 2017 *)",
				"CoefficientList[Series[-8 (-2 + 3 x)/(1 - 4 x + 3 x^2), {x, 0, 30}], x] (* _Eric W. Weisstein_, Nov 29 2017 *)"
			],
			"program": [
				"(PARI) Vec(8*(2 - 3*x)/((1 - x)*(1 - 3*x)) + O(x^40)) \\\\ _Andrew Howroyd_, Sep 02 2017"
			],
			"xref": [
				"Column 1 of A205255.",
				"Cf. A003462, A007051, A067771, A289521."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_R. H. Hardin_, Jan 24 2012",
			"references": 1,
			"revision": 25,
			"time": "2017-11-30T08:34:46-05:00",
			"created": "2012-01-24T09:34:58-05:00"
		}
	]
}