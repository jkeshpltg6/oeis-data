{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A322006",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 322006,
			"data": "0,0,0,0,1,2,2,3,3,4,3,3,4,4,4,4,6,5,5,4,6,5,7,4,8,5,8,5,9,4,7,4,8,7,9,4,11,5,9,6,11,6,11,6,11,8,12,4,13,6,12,8,13,6,14,5,13,8,13,4,16,5,15,9,16,7,16,6,14,9,16,5,18,6,16,10,19,7,19,6,17,10,18,4,21,9,17,9,19,8",
			"name": "a(n) = number of primes of the form p = n - q, where q is a prime or semiprime.",
			"comment": [
				"Related to Chen's theorem (Chen 1966, 1973) which states that every sufficiently large even number is the sum of a prime and another prime or semiprime. Yamada (2015) has proved that this holds for all even numbers larger than exp(exp(36)).",
				"In terms of this sequence, Chen's theorem with Yamada's bound is equivalent to say that a(2*n) \u003e 0 for all n \u003e 1.7 * 10^1872344071119348 (exponent ~ 1.8*10^15).",
				"Sequence A322007(n) = a(2n) lists the bisection corresponding to even numbers only.",
				"A235645 lists the number of decompositions of 2n into a prime p and a prime or semiprime q; this is less than a(2n) because p + q and q + p is the same decomposition (if q is a prime), but this sequence will count the two distinct primes 2n - q and 2n - p (if q \u003c\u003e p)."
			],
			"reference": [
				"Chen, J. R. (1966). \"On the representation of a large even integer as the sum of a prime and the product of at most two primes\". Kexue Tongbao. 11 (9): 385-386.",
				"Chen, J. R. (1973). \"On the representation of a larger even integer as the sum of a prime and the product of at most two primes\". Sci. Sinica. 16: 157-176."
			],
			"link": [
				"Y. C. Cai, \u003ca href=\"http://doi.org/10.1007/s101140200168\"\u003eChen's Theorem with Small Primes\u003c/a\u003e, Acta Mathematica Sinica 18, no. 3 (2002), pp. 597-604. doi:10.1007/s101140200168.",
				"P. M. Ross, \u003ca href=\"http://doi.org/10.1112/jlms/s2-10.4.500\"\u003eOn Chen's theorem that each large even number has the form (p1+p2) or (p1+p2p3)\u003c/a\u003e, J. London Math. Soc. Series 2 vol. 10, no. 4 (1975), pp. 500-506. doi:10.1112/jlms/s2-10.4.500.",
				"Tomohiro Yamada, \u003ca href=\"http://arxiv.org/abs/1511.03409\"\u003eExplicit Chen's theorem\u003c/a\u003e, preprint arXiv:1511.03409 [math.NT] (2015)."
			],
			"example": [
				"a(4) = 1 is the first nonzero term corresponding to 4 = 2 + 2 or, rather, to the prime 2 = 4 - 2.",
				"a(5) = 2 because the primes 2 = 5 - 3 and 3 = 5 - 2 are of the required form n - q where q = 3 resp. q = 2 are primes.",
				"a(6) = 2 because the primes 2 = 6 - 4 and 3 = 6 - 3 are of the required form n - q, since q = 4 is a semiprime and q = 3 is a prime."
			],
			"program": [
				"(PARI) A322006(n,s=0)=forprime(p=2,n-2,bigomega(n-p)\u003c3\u0026\u0026s++);s}"
			],
			"xref": [
				"Cf. A322007, A235645, A045917, A130588, A241539."
			],
			"keyword": "nonn",
			"offset": "0,6",
			"author": "_M. F. Hasler_, Jan 06 2019",
			"references": 2,
			"revision": 14,
			"time": "2019-07-09T05:54:53-04:00",
			"created": "2019-01-07T04:44:23-05:00"
		}
	]
}