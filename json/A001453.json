{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001453",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1453,
			"id": "M3464 N1409",
			"data": "1,4,13,41,131,428,1429,4861,16795,58785,208011,742899,2674439,9694844,35357669,129644789,477638699,1767263189,6564120419,24466267019,91482563639,343059613649,1289904147323,4861946401451,18367353072151,69533550916003,263747951750359",
			"name": "Catalan numbers - 1.",
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi and Alois P. Heinz, \u003ca href=\"/A001453/b001453.txt\"\u003eTable of n, a(n) for n = 2..500\u003c/a\u003e (first 170 terms from Vincenzo Librandi)",
				"R. M. Baer and P. Brock, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1968-0228216-8\"\u003eNatural sorting over permutation spaces\u003c/a\u003e, Math. Comp. 22 1968 385-410.",
				"J. M. Hammersley, \u003ca href=\"http://projecteuclid.org/euclid.bsmsp/1200514101\"\u003eA few seedings of research\u003c/a\u003e, in Proc. Sixth Berkeley Sympos. Math. Stat. and Prob., ed. L. M. le Cam et al., Univ. Calif. Press, 1972, Vol. I, pp. 345-394.",
				"Piera Manara and Claudio Perelli Cippo, \u003ca href=\"http://www.mat.unisi.it/newsito/puma/public_html/22_2/manara_perelli-cippo.pdf\"\u003eThe fine structure of 4321 avoiding involutions and 321 avoiding involutions\u003c/a\u003e, PU. M. A. Vol. 22 (2011), 227-238.",
				"Murray Tannock, \u003ca href=\"https://skemman.is/bitstream/1946/25589/1/msc-tannock-2016.pdf\"\u003eEquivalence classes of mesh patterns with a dominating pattern\u003c/a\u003e, MSc Thesis, Reykjavik Univ., May 2016."
			],
			"formula": [
				"a(n) = A000108(n) - 1 = binomial(2*n,n)/(n+1) - 1.",
				"D-finite with recurrence: (n+1)*a(n) +2*(-3*n+1)*a(n-1) +(9*n-13)*a(n-2) +2*(-2*n+5)*a(n-3)=0. - _R. J. Mathar_, Sep 04 2013",
				"a(n) = Sum_{k=1..floor(n/2)} (C(n,k)-C(n,k-1))^2. - _J. M. Bergot_, Sep 17 2013",
				"a(n) = Sum_{k=1..n-1} A000245(n-k-1). - _John M. Campbell_, Dec 28 2016",
				"From _Ilya Gutkovskiy_, Dec 28 2016: (Start)",
				"O.g.f.: (1 - sqrt(1 - 4*x))/(2*x) - 1/(1 - x).",
				"E.g.f.: exp(x)*(exp(x)*(BesselI(0,2*x) - BesselI(1,2*x)) - 1). (End)",
				"a(n)= 3*Sum_{k=1..n} binomial(2*k-2,k)/(k+1). - _Gary Detlefs_, Feb 14 2020"
			],
			"maple": [
				"with(combstruct): bin := {B=Union(Z,Prod(B,B))}: seq(count([B,bin, unlabeled], size=n+1)-1, n=2..30); # _Zerinvary Lajos_, Dec 05 2007"
			],
			"mathematica": [
				"Array[CatalanNumber, 30, 2] - 1 (* _Jean-François Alcover_, Mar 11 2014 *)"
			],
			"program": [
				"(MuPAD) combinat::dyckWords::count(n)-1 $ n = 2..26; // _Zerinvary Lajos_, May 08 2008",
				"(MAGMA) [Catalan(n)-1: n in [2..30]]; // _Vincenzo Librandi_, May 22 2011",
				"(PARI) a(n)=(2*n)!/n!/(n+1)!-1 \\\\ _Charles R Greathouse IV_, Apr 17 2012"
			],
			"xref": [
				"Cf. A000108, A001454. Column k=2 of A047874.",
				"A141364 is essentially the same sequence.",
				"All of A000108, A001453, A246604, A273526, A120304, A289615, A289616, A289652, A289653, A289654 are very similar sequences."
			],
			"keyword": "nonn,easy",
			"offset": "2,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _James A. Sellers_, Sep 08 2000"
			],
			"references": 34,
			"revision": 94,
			"time": "2020-02-14T10:50:34-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}