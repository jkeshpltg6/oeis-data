{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A322744",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 322744,
			"data": "1,2,2,3,6,3,4,8,8,4,5,12,11,12,5,6,14,16,16,14,6,7,18,19,24,19,18,7,8,20,24,28,28,24,20,8,9,24,27,36,33,36,27,24,9,10,26,32,40,42,42,40,32,26,10,11,30,35,48,47,54,47,48,35,30,11,12,32,40,52,56,60,60,56,52,40,32,12",
			"name": "Array T(n,k) = (3*n*k - A319929(n,k))/2, n \u003e= 1, k \u003e= 1, read by antidiagonals.",
			"comment": [
				"Associative multiplication-like table whose values depend on whether n and k are odd or even.",
				"Associativity is proved by checking the formula with eight cases of three odd and even arguments. T(n,k) is distributive as long as partitioning an even number into two odd numbers is not allowed."
			],
			"link": [
				"David Lovler, \u003ca href=\"/A322744/b322744.txt\"\u003eTable of n, a(n) for n = 1..861\u003c/a\u003e (Antidiagonals n = 1..41, flattened)"
			],
			"formula": [
				"T(n,k) = (3*n*k - (n + k - 1))/2 if n is odd and k is odd;",
				"T(n,k) = (3*n*k - n)/2 if n is even and k is odd;",
				"T(n,k) = (3*n*k - k)/2 if n is odd and k is even;",
				"T(n,k) = 3*n*k/2 if n is even and k is even.",
				"T(n,k) = 6*floor(n/2)*floor(k/2) + A319929(n,k)."
			],
			"example": [
				"Array T(n,k) begins:",
				"   1   2   3   4   5   6   7   8   9  10",
				"   2   6   8  12  14  18  20  24  26  30",
				"   3   8  11  16  19  24  27  32  35  40",
				"   4  12  16  24  28  36  40  48  52  60",
				"   5  14  19  28  33  42  47  56  61  70",
				"   6  18  24  36  42  54  60  72  78  90",
				"   7  20  27  40  47  60  67  80  87 100",
				"   8  24  32  48  56  72  80  96 104 120",
				"   9  26  35  52  61  78  87 104 113 130",
				"  10  30  40  60  70  90 100 120 130 150"
			],
			"mathematica": [
				"Table[Function[n, (3 n k - If[OddQ@ n, If[OddQ@ k, n + k - 1, k], If[OddQ@ k, n, 0]])/2][m - k + 1], {m, 12}, {k, m}] // Flatten (* _Michael De Vlieger_, Apr 21 2019 *)"
			],
			"program": [
				"(PARI) T319929(n, k) = if (n%2, if (k%2, n+k-1, k), if (k%2, n, 0));",
				"T(n,k) = (3*n*k - T319929(n,k))/2;",
				"matrix(6, 6, n, k, T(n, k)) \\\\ _Michel Marcus_, Dec 27 2018"
			],
			"xref": [
				"Equals A003991 + A322630 - A319929.",
				"Cf. A327263, A307001, A307002, A340746, A340747."
			],
			"keyword": "nonn,tabl,easy,changed",
			"offset": "1,2",
			"author": "_David Lovler_, Dec 24 2018",
			"references": 11,
			"revision": 37,
			"time": "2022-01-15T10:04:45-05:00",
			"created": "2019-03-10T14:54:17-04:00"
		}
	]
}