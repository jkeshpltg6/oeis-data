{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A184156",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 184156,
			"data": "0,0,0,0,1,1,0,0,2,2,2,2,2,2,3,0,2,4,0,3,3,3,4,3,4,4,6,4,3,5,3,0,4,3,4,6,3,3,5,4,4,6,4,4,7,6,5,4,4,6,4,6,0,9,5,6,4,5,3,7,6,4,8,0,6,6,3,4,7,7,4,8,6,6,8,6,5,8,4,5,12,5,6,9,5,6,6,5,4,10,6,8,5,7,5,5,6,8,8,8,6,6,9,8,9,4,6,12,5,7",
			"name": "The Wiener polarity index of the rooted tree with Matula-Goebel number n.",
			"comment": [
				"The Wiener polarity index of a connected graph G is the number of unordered pairs {i,j} of vertices of G such that the distance between i and j is 3.",
				"The Matula-Goebel number of a rooted tree is defined in the following recursive manner: to the one-vertex tree there corresponds the number 1; to a tree T with root degree 1 there corresponds the t-th prime number, where t is the Matula-Goebel number of the tree obtained from T by deleting the edge emanating from the root; to a tree T with root degree m\u003e=2 there corresponds the product of the Matula-Goebel numbers of the m branches of T."
			],
			"reference": [
				"H. Deng, H. Xiao and F. Tang, On the extremal Wiener polarity index of trees with a given diameter, MATCH, Commun. Math. Comput. Chem., 63, 2010, 257-264.",
				"W. Du, X. Li and Y. Shi, Algorithms and extremal problem on Wiener polarity index, MATCH, Commun. Math. Comput. Chem., 62, 2009, 235-244.",
				"F. Goebel, On a 1-1-correspondence between rooted trees and natural numbers, J. Combin. Theory, B 29 (1980), 141-143.",
				"I. Gutman and A. Ivic, On Matula numbers, Discrete Math., 150, 1996, 131-142.",
				"I. Gutman and Yeong-Nan Yeh, Deducing properties of trees from their Matula numbers, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"D. W. Matula, A natural rooted tree enumeration by prime factorization, SIAM Review, 10, 1968, 273."
			],
			"link": [
				"E. Deutsch, \u003ca href=\"http://arxiv.org/abs/1111.4288\"\u003eTree statistics from Matula numbers\u003c/a\u003e, arXiv preprint arXiv:1111.4288, 2011",
				"\u003ca href=\"/index/Mat#matula\"\u003eIndex entries for sequences related to Matula-Goebel numbers\u003c/a\u003e"
			],
			"formula": [
				"a(n) is the coefficient of x^3 in the Wiener polynomial of the rooted tree with Matula-Goebel number n. The coefficients of these Wiener polynomials are given in A196059. The Maple program is based on the above."
			],
			"example": [
				"a(7)=0 because the rooted tree with Matula-Goebel number 7 is the rooted tree Y with no pair of vertices at distance 3.",
				"a(11) = 2 because the rooted tree with Matula-Goebel number 7 is a path on 5 vertices, say a, b, c, d, e, with each of the pairs {a,d} and {b,e} at distance 3."
			],
			"maple": [
				"with(numtheory): WP := proc (n) local r, s, R: r := proc (n) options operator, arrow: op(1, factorset(n)) end proc: s := proc (n) options operator, arrow: n/r(n) end proc: R := proc (n) if n = 1 then 0 elif bigomega(n) = 1 then sort(expand(x*R(pi(n))+x)) else sort(expand(R(r(n))+R(s(n)))) end if end proc: if n = 1 then 0 elif bigomega(n) = 1 then sort(expand(WP(pi(n))+x*R(pi(n))+x)) else sort(expand(WP(r(n))+WP(s(n))+R(r(n))*R(s(n)))) end if end proc: a := proc (n) options operator, arrow: coeff(WP(n), x, 3) end proc: seq(a(n), n = 1 .. 110);"
			],
			"xref": [
				"Cf. A196059"
			],
			"keyword": "nonn",
			"offset": "1,9",
			"author": "_Emeric Deutsch_, Oct 12 2011",
			"references": 0,
			"revision": 13,
			"time": "2017-03-07T11:32:43-05:00",
			"created": "2011-10-13T00:41:15-04:00"
		}
	]
}