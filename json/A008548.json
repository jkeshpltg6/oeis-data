{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A008548",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 8548,
			"data": "1,1,6,66,1056,22176,576576,17873856,643458816,26381811456,1213563326976,61891729675776,3465936861843456,211422148572450816,13953861805781753856,990724188210504523776,75295038303998343806976",
			"name": "Quintuple factorial numbers: Product_{k=0..n-1} (5*k+1).",
			"comment": [
				"a(n), n\u003e=1, enumerates increasing sextic (6-ary) trees with n vertices. - _Wolfdieter Lang_, Sep 14 2007",
				"Hankel transform is A169620. - _Paul Barry_, Dec 03 2009"
			],
			"link": [
				"T. D. Noe and Vincenzo Librandi, \u003ca href=\"/A008548/b008548.txt\"\u003eTable of n, a(n) for n = 0..300\u003c/a\u003e (first 50 terms from T. D. Noe).",
				"Martin Burtscher, Igor Szczyrba, Rafał Szczyrba, \u003ca href=\"http://www.emis.de/journals/JIS/VOL18/Szczyrba/sz3.html\"\u003eAnalytic Representations of the n-anacci Constants and Generalizations Thereof\u003c/a\u003e, Journal of Integer Sequences, Vol. 18 (2015), Article 15.4.5.",
				"W. Lang, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL3/LANG/lang.html\"\u003eOn generalizations of Stirling number triangles\u003c/a\u003e, J. Integer Seqs., Vol. 3 (2000), #00.2.4.",
				"J.-C. Novelli, J.-Y. Thibon, \u003ca href=\"http://arxiv.org/abs/1403.5962\"\u003eHopf Algebras of m-permutations,(m+1)-ary trees, and m-parking functions\u003c/a\u003e, arXiv preprint arXiv:1403.5962 [math.CO], 2014.",
				"M. D. Schmidt, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL13/Schmidt/multifact.html\"\u003eGeneralized j-Factorial Functions, Polynomials, and Applications \u003c/a\u003e, J. Int. Seq. 13 (2010), 10.6.7, Table 6.3"
			],
			"formula": [
				"E.g.f.: (1-5*x)^(-1/5).",
				"a(n) ~ 2^(1/2)*Pi^(1/2)*gamma(1/5)^-1*n^(-3/10)*5^n*e^-n*n^n*{1 + 1/300*n^-1 - ...}. - Joe Keane (jgk(AT)jgk.org), Nov 24 2001",
				"a(n) = Sum_{k=0..n} (-5)^(n-k)*A048994(n, k). - _Philippe Deléham_, Oct 29 2005",
				"G.f.: 1/(1-x/(1-5x/(1-6x/(1-10x/(1-11x/(1-15x/(1-16x/(1-20x/(1-21x/(1-25x/(1-.../(1-A008851(n+1)*x/(1-... (continued fraction). - _Paul Barry_, Dec 03 2009",
				"a(n)=(-4)^n*Sum_{k=0..n} (5/4)^k*s(n+1,n+1-k), where s(n,k) are the Stirling numbers of the first kind, A048994. - _Mircea Merca_, May 03 2012",
				"G.f.: 1/Q(0) where Q(k) = 1 - x*(5*k+1)/(1 - x*(5*k+5)/Q(k+1) ); (continued fraction). - _Sergei N. Gladkovskii_, Mar 20 2013",
				"G.f.: G(0)/2, where G(k)= 1  + 1/(1 - (5*k+1)*x/((5*k+1)*x + 1/G(k+1))); (continued fraction). - _Sergei N. Gladkovskii_, Jun 14 2013",
				"a(n) = (10n-18)*a(n-2) + (5n-6)*a(n-1), n\u003e=2. - _Ivan N. Ianakiev_, Aug 12 2013",
				"Let T(x) = 1/(1 - 4*x)^(1/4) be the e.g.f. for the sequence of triple factorial numbers A007696. Then the e.g.f. A(x) for the quintuple factorial numbers satisfies T( int {0..x} A(t) dt ) = A(x). Cf. A007559 and A007696. - _Peter Bala_, Jan 02 2015",
				"O.g.f.: hypergeom([1, 1/5], [], 5*x). - _Peter Luschny_, Oct 08 2015",
				"a(n) = 5^n * Gamma(n + 1/5) / Gamma(1/5). - _Artur Jasinski_, Aug 23 2016",
				"D-finite with recurrence: a(n) +(-5*n+4)*a(n-1)=0. - _R. J. Mathar_, Jan 17 2020"
			],
			"maple": [
				"a := n -\u003e mul(5*k+1, k=0..n-1);",
				"G(x):=(1-5*x)^(-1/5): f[0]:=G(x): for n from 1 to 29 do f[n]:=diff(f[n-1],x) od: x:=0: seq(f[n],n=0..16); # _Zerinvary Lajos_, Apr 03 2009",
				"H := hypergeom([1, 1/5], [], 5*x):",
				"seq(coeff(series(H,x,20),x,n),n=0..16); # _Peter Luschny_, Oct 08 2015"
			],
			"mathematica": [
				"Table[Product[5k+1,{k,0,n-1}],{n,0,20}]  (* _Harvey P. Dale_, Apr 23 2011 *)",
				"FoldList[Times,1,NestList[#+5\u0026,1,20]] (* _Ray Chandler_, Apr 23 2011 *)",
				"FoldList[Times,1,5Range[0, 25] + 1] (* _Vincenzo Librandi_, Jun 10 2013 *)"
			],
			"program": [
				"(PARI) x='x+O('x^33); Vec(serlaplace((1-5*x)^(-1/5))) \\\\ _Joerg Arndt_, Apr 24 2011",
				"(PARI) vector(20, n, n--; prod(k=0, n-1, 5*k+1)) \\\\ _Altug Alkan_, Oct 08 2015",
				"(MAGMA) [(\u0026*[5*k+1: k in [0..n]]): n in [0..20]]; // _G. C. Greubel_, Aug 16 2019",
				"(Sage) [product(5*k+1 for k in (0..n)) for n in (0..20)] # _G. C. Greubel_, Aug 16 2019",
				"(GAP) List([0..20], n-\u003e Product([0..n], k-\u003e 5*k+1)); # _G. C. Greubel_, Aug 16 2019"
			],
			"xref": [
				"Cf. A001147, A007559, A007696, A016861, A034687, A034688, A052562, A047055, A051150.",
				"a(n)= A049385(n, 1) (first column of triangle)."
			],
			"keyword": "nonn,nice,easy",
			"offset": "0,3",
			"author": "Joe Keane (jgk(AT)jgk.org)",
			"references": 57,
			"revision": 91,
			"time": "2020-01-30T21:29:14-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}