{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002039",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2039,
			"id": "M2465 N0979",
			"data": "1,3,5,10,25,64,160,390,940,2270,5515,13440,32735,79610,193480,470306,1143585,2781070,6762990,16445100,39987325,97232450,236432060,574915770,1397981470,3399360474,8265943685,20099618590,48874630750",
			"name": "Convolution inverse of A143348.",
			"comment": [
				"Gandhi denotes f(-x) by Phi(x) and a(n) by G(n)."
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A002039/b002039.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"J. M. Gandhi, \u003ca href=\"http://www.jstor.org/stable/2317132\"\u003eOn numbers related to partitions of a number\u003c/a\u003e, Amer. Math. Monthly, 76 (1969), 1033-1036.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Function.\u003c/a\u003e"
			],
			"formula": [
				"G.f.: -x / (Sum_{k\u003e0} k * (-x)^k / (1 - (-x)^k)) = 1 / (log( f(x) )') where f(-x) = Product_{k\u003e0} (1 - x^k) is one of Ramanujan's theta functions. - _Michael Somos_, Apr 08 2003",
				"a(n) ~ c * d^n, where d = -1/A143441 = 2.43161993449532399475429572773256778... and c = 0.765603960074106532799232452562411022387973764575133091283490410339311... - _Vaclav Kotesovec_, Jun 02 2018",
				"a(0) = 1; a(n) = Sum_{k=1..n} (-1)^(k+1) * sigma(k+1) * a(n-k). - _Ilya Gutkovskiy_, May 27 2020"
			],
			"example": [
				"1 + 3*x + 5*x^2 + 10*x^3 + 25*x^4 + 64*x^5 + 160*x^6 + 390*x^7 + 940*x^8 + ..."
			],
			"mathematica": [
				"max = 28; f[x_] := -x / Sum[ k*(-x)^k/(1-(-x)^k), {k, 1, max+1}]; CoefficientList[ Series[ f[x], {x, 0, max}], x] (* _Jean-François Alcover_, Nov 07 2011, after _Michael Somos_ *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, polcoeff( 1 / log( eta( -x + x^2 * O(x^n)))', n))} /* _Michael Somos_, Apr 05 2003 */"
			],
			"xref": [
				"Cf. A002040, A143348."
			],
			"keyword": "nonn,nice,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, _Simon Plouffe_",
			"references": 7,
			"revision": 40,
			"time": "2020-05-27T20:16:24-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}