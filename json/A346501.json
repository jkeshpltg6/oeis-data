{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A346501",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 346501,
			"data": "0,0,6,10,48,90,210,34,416,570,58,2652,930,1984,1184,1376,2820,118,1062,1830,3660,4020,2190,2370,1602,5340,9006,12702,6208,3090,8502,12198,3810,7620,4448,298,21372,17880,4710,15386,7014,21376,22836,11584,11946,394,16548,40596,13926,454,7136,6870",
			"name": "With p = prime(n), a(n) is the least composite k such that A001414(k) = p and k+p is prime, or 0 if there is no such k.",
			"comment": [
				"All terms are even.",
				"Conjecture: a(n) \u003e 0 for n \u003e= 3."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A346501/b346501.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"a(5) = 48 because A001414(48) = 11 = prime(5) and 48+11 = 59 is prime, and 48 is the least composite that works for this prime."
			],
			"maple": [
				"spf:= proc(n) local t;",
				"  add(t[1]*t[2],t=ifactors(n)[2])",
				"end proc:",
				"N:= 500: # for a(1)..a(N)",
				"count:= 2:",
				"V:= Vector(N):",
				"for k from 4 by 2 while count \u003c N do",
				"  if isprime(k) then next fi;",
				"  p:= spf(k);",
				"  if isprime(p) then",
				"    m:= numtheory:-pi(p);",
				"    if m \u003c= N and V[m] = 0 and isprime(p+k) then",
				"        V[m]:= k; count:= count+1;",
				"    fi",
				"  fi",
				"od:",
				"convert(V,list);"
			],
			"mathematica": [
				"sopfr[1] = 0; sopfr[n_] := Plus @@ Times @@@ FactorInteger[n]; seq[max_] := Module[{s = Table[0, {max}], c = 2, k = 3, p, ip}, While[c \u003c max, k++; If[CompositeQ[k] \u0026\u0026 PrimeQ[(p = sopfr[k])] \u0026\u0026 PrimeQ[k + p] \u0026\u0026 (ip = PrimePi[p]) \u003c= max \u0026\u0026 s[[ip]] == 0, c++; s[[ip]] = k]]; s]; seq[50] (* _Amiram Eldar_, Jul 22 2021 *)"
			],
			"program": [
				"(PARI) sopfr(n) = (n=factor(n))[, 1]~*n[, 2]; \\\\ A001414",
				"a(n) = if (n\u003c=2, return(0)); my(p=prime(n)); forcomposite(k=2,, if ((sopfr(k)==p) \u0026\u0026 isprime(k+p), return (k))); \\\\ _Michel Marcus_, Jul 22 2021"
			],
			"xref": [
				"Cf. A001414."
			],
			"keyword": "nonn,look",
			"offset": "1,3",
			"author": "_J. M. Bergot_ and _Robert Israel_, Jul 20 2021",
			"references": 1,
			"revision": 14,
			"time": "2021-07-23T09:02:27-04:00",
			"created": "2021-07-22T04:54:50-04:00"
		}
	]
}