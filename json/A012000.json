{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A012000",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 12000,
			"data": "1,2,-2,-28,-74,92,1324,3656,-4826,-70228,-197372,267896,3921724,11126936,-15347432,-225505648,-643622906,897078476,13214495764,37869162392,-53170602284,-784672445368,-2255295815192,3183829452272,47051201187676,135537088268792,-192142210448216",
			"name": "Expansion of 1/sqrt(1 - 4*x + 16*x^2).",
			"link": [
				"T. D. Noe, \u003ca href=\"/A012000/b012000.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"Hacène Belbachir and Abdelghani Mehdaoui, \u003ca href=\"https://doi.org/10.2989/16073606.2020.1729269\"\u003eRecurrence relation associated with the sums of square binomial coefficients\u003c/a\u003e, Quaestiones Mathematicae (2021) Vol. 44, Issue 5, 615-624.",
				"Hacène Belbachir, Abdelghani Mehdaoui, and László Szalay, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL22/Szalay/szalay42.html\"\u003eDiagonal Sums in the Pascal Pyramid, II: Applications\u003c/a\u003e, J. Int. Seq., Vol. 22 (2019), Article 19.3.5.",
				"Tony D. Noe, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL9/Noe/noe35.html\"\u003eOn the Divisibility of Generalized Central Trinomial Coefficients\u003c/a\u003e, Journal of Integer Sequences, Vol. 9 (2006), Article 06.2.7."
			],
			"formula": [
				"G.f.: 1/sqrt(1 - 4*x + 16*x^2). - _Vladeta Jovovic_, May 13 2003",
				"Scaled Legendre polynomials evaluated at 1/2: 2^(2n)P(n, 1/2). - _Michael Somos_, Dec 03 2001",
				"a(n) = (-1)^n*Sum_{k=0..n} C(n,k)^2*(-3)^k. - _Benoit Cloitre_, Oct 25 2003",
				"a(n) = Sum_{k=0..floor(n/2)} C(n,k)*C(2(n-k),n)(-4)^k. - _Paul Barry_, Sep 08 2004",
				"D-finite with recurrence: n*a(n) + 2*(1-2*n)*a(n-1) + 16*(n-1)*a(n-2) = 0. - _R. J. Mathar_, Nov 14 2011",
				"G.f.: G(0), where G(k) = 1 + x*(1-x)*(4*k+1)/( 4*k+2 - x*(1-x)*(4*k+2)*(4*k+3)/(x*(1-x)*(4*k+3) + 4*(k+1)/G(k+1) )); (continued fraction). - _Sergei N. Gladkovskii_, Jul 06 2013",
				"a(n) ~ 2^(2*n+1)*sin(Pi/12*(4*n+5))/(3^(1/4)*sqrt(Pi*n)). - _Vaclav Kotesovec_, Jul 30 2013",
				"a(n) = [x^n] ((1 - x)*(1 + 3*x))^n. - _Peter Bala_, Aug 16 2015",
				"a(n) = 4^n*hypergeom([-n,1+n], [1], 1/4). - _Peter Luschny_, May 09 2016",
				"From _Peter Bala_, Nov 28 2021: (Start)",
				"a(n) = 4^(2*n+1)*Sum_{k \u003e= n} (-1)^(n+k)*C(k,n)^2*(1/3)^(k+1).",
				"a(n) = (-1)^n*hypergeom([-n,-n], [1], -3).",
				"a(n) = (4/3)*(16/3)^n*hypergeom([n+1,n+1], [1], -1/3). (End)"
			],
			"example": [
				"G.f. = 1 + 2*x - 2*x^2 - 28*x^3 - 74*x^4 + 92*x^5 + 1324*x^6 + 3656*x^7 + ..."
			],
			"maple": [
				"a := n -\u003e 4^n*hypergeom([-n,1+n],[1],1/4);",
				"seq(simplify(a(n)),n=0..26); # _Peter Luschny_, May 09 2016"
			],
			"mathematica": [
				"Table[ 2^(2n) LegendreP[ n, 1/2 ], {n, 12} ]"
			],
			"program": [
				"(PARI) {a(n) = 2^(2*n) * subst( pollegendre(n), x, 1/2)} /* _Michael Somos_, Dec 03 2001 */",
				"(PARI) a(n) = polcoeff(((1 - x)*(1 + 3*x))^n, n); \\\\ _Michel Marcus_, Aug 16 2015"
			],
			"xref": [
				"Cf. A116091, A098332, A126869."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Wouter Meeussen_",
			"ext": [
				"More terms from _Vladeta Jovovic_, May 13 2003"
			],
			"references": 6,
			"revision": 68,
			"time": "2021-12-04T12:25:07-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}