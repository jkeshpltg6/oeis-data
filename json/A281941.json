{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A281941",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 281941,
			"data": "1,2,3,5,4,1,5,2,3,4,5,1,3,1,4,2,2,1,10,5,2,2,6,1,7,7,5,7,4,3,7,1,3,12,9,4,2,2,5,3,5,5,9,10,1,5,5,1,5,3,6,8,2,4,9,4,4,8,5,3,3,4,5,3,4,5,10,4,1,5,7,1,7,10,6,8,3,2,10,2,1",
			"name": "Number of ways to write n as w^2 + x^2 + y^2 + z^2 with w and w + x + y + z both squares, where w,x,y,z are integers with |x| \u003c= |y| \u003c= |z|.",
			"comment": [
				"Conjecture: (i) a(n) \u003e 0 for all n = 0,1,2,....",
				"(ii) Any nonnegative integer n can be written as x^2 + y^2 + z^2 + w^2 with x - y + z and z + w both squares, where x,w are integers and y,z are nonnegative integers.",
				"The author has proved that every n = 0,1,2,... is the sum of a fourth power and three squares. Y.-C. Sun and the author have shown that any nonnegative integer can be written as w^2 + x^2 + y^2 + z^2 with w,x,y,z integers such that w + x + y + z is a square."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A281941/b281941.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Yu-Chen Sun and Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1605.03074\"\u003eSome refinements of Lagrange's four-square theorem\u003c/a\u003e, arXiv:1605.03074 [math.NT], 2016-2017.",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2016.11.008\"\u003eRefining Lagrange's four-square theorem\u003c/a\u003e, J. Number Theory 175(2017), 167-190."
			],
			"example": [
				"a(5) = 1 since 5 = 0^2 + 0^2 + (-1)^2 + 2^2 with 0 = 0^2 and 0 + 0 + (-1) + 2 = 1^2.",
				"a(23) = 1 since 23 = 1^2 + 2^2 + 3^2 + 3^2 with 1 = 1^2 and 1 + 2 + 3 + 3 = 3^2.",
				"a(47) = 1 since 47 = 1^2 + (-1)^2 + 3^2 + 6^2 with 1 = 1^2 and 1 + (-1) + 3 + 6 = 3^2.",
				"a(157) = 1 since 157 = 4^2 + (-2)^2 + (-4)^2 + 11^2 with 4 = 2^2 and 4 + (-2) + (-4) + 11 = 3^2.",
				"a(284) = 1 since 284 = 9^2 + 3^2 + 5^2 + (-13)^2 with 9 = 3^2 and 9 + 3 + 5 + (-13) = 2^2.",
				"a(628) = 1 since 628 = 9^2 + (-5)^2 + (-9)^2 + 21^2 with 9 = 3^2 and 9 + (-5) + (-9) + 21 = 4^2."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]];",
				"Do[r=0;Do[If[SQ[n-x^4-y^2-z^2]\u0026\u0026SQ[x^2+(-1)^i*y+(-1)^j*z+(-1)^k*Sqrt[n-x^4-y^2-z^2]],r=r+1],{x,0,n^(1/4)},{y,0,Sqrt[(n-x^4)/3]},{i,0,Min[y,1]},{z,y,Sqrt[(n-x^4-y^2)/2]},{j,0,Min[z,1]},{k,0,Min[Sqrt[n-x^4-y^2-z^2],1]}];Print[n,\" \",r];Continue,{n,0,80}]"
			],
			"xref": [
				"Cf. A000118, A000290, A270969, A272620, A281939."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Zhi-Wei Sun_, Feb 02 2017",
			"references": 9,
			"revision": 7,
			"time": "2017-02-03T02:57:12-05:00",
			"created": "2017-02-03T02:57:12-05:00"
		}
	]
}