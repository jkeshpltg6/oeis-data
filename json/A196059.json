{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A196059",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 196059,
			"data": "1,2,1,2,1,3,2,1,3,2,1,3,3,3,3,4,3,2,1,4,3,2,1,4,3,2,1,4,4,2,4,4,2,4,4,2,5,4,3,2,1,4,6,4,4,2,5,5,4,1,4,6,5,5,3,2,5,5,3,2,5,4,3,2,1,5,5,4,1,5,7,3,6,5,4,3,2,1,5,5,4,1,6,6,6,3,5,6,4,5,5,3,2,6,6,5,3,1,5,4,3,2,1,5,10,6,5,4,3,2,1,5,5,3,2,6,6,4,3,2",
			"name": "Triangle read by rows: T(n,k) is the number of pairs of nodes at distance k in the rooted tree having Matula-Goebel number n (n\u003e=2).",
			"comment": [
				"The Matula-Goebel number of a rooted tree can be defined in the following recursive manner: to the one-vertex tree there corresponds the number 1; to a tree T with root degree 1 there corresponds the t-th prime number, where t is the Matula-Goebel number of the tree obtained from T by deleting the edge emanating from the root; to a tree T with root degree m\u003e=2 there corresponds the product of the Matula-Goebel numbers of the m branches of T.",
				"Number of entries in row n is A196058(n) (n=2,3,...).",
				"The generating polynomial of row n is the Wiener polynomial of the rooted tree having Matula-Goebel number n."
			],
			"reference": [
				"F. Goebel, On a 1-1-correspondence between rooted trees and natural numbers, J. Combin. Theory, B 29 (1980), 141-143.",
				"I. Gutman and A. Ivic, On Matula numbers, Discrete Math., 150, 1996, 131-142.",
				"I. Gutman and Yeong-Nan Yeh, Deducing properties of trees from their Matula numbers, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"D. W. Matula, A natural rooted tree enumeration by prime factorization, SIAM Review, 10, 1968, 273.",
				"B. E. Sagan, Y-N. Yeh and P. Zhang, The Wiener Polynomial of a Graph, Internat. J. of Quantum Chem., 60, 1996, 959-969."
			],
			"link": [
				"E. Deutsch, \u003ca href=\"http://arxiv.org/abs/1111.4288\"\u003eTree statistics from Matula numbers\u003c/a\u003e, arXiv preprint arXiv:1111.4288, 2011",
				"\u003ca href=\"/index/Mat#matula\"\u003eIndex entries for sequences related to Matula-Goebel numbers\u003c/a\u003e"
			],
			"formula": [
				"We give the recursive construction of the row generating polynomials W(n)=W(n,x) (the Wiener polynomials). Let R(n) be the partial Wiener polynomial with respect to the root (defined, computed and programmed in A196056). W(1)=0; if n = p(t) (=the t-th prime), then W(n)=W(t)+x*R(t) + x; if n=rs (r,s\u003e=2), then W(n)=W(r)+W(s)+R(r)R(s) (2nd Maple program yields the Wiener polynomial W(n))."
			],
			"example": [
				"Row n=7 is [3,3] because the rooted tree with Matula-Goebel number 7 is the rooted tree Y, having distances 1,1,1,2,2,2.",
				"Row n=2^m is [m, m(m-1)/2] because the rooted tree with Matula-Goebel number 2^m is a star with m edges; there are m distances 1 and m(m-1)/2 distances 2.",
				"Triangle starts:",
				"1;",
				"2,1;",
				"2,1;",
				"3,2,1;",
				"3,2,1;",
				"3,3;",
				"3,3;",
				"4,3,2,1;",
				"4,3,2,1;"
			],
			"maple": [
				"with(numtheory): W := proc (n) local r, s, R: r := proc (n) options operator, arrow: op(1, factorset(n)) end proc: s := proc (n) options operator, arrow: n/r(n) end proc; R := proc (n) if n = 1 then 0 elif bigomega(n) = 1 then sort(expand(x*R(pi(n))+x)) else sort(expand(R(r(n))+R(s(n)))) end if end proc; if n = 1 then 0 elif bigomega(n) = 1 then sort(expand(W(pi(n))+x*R(pi(n))+x)) else sort(expand(W(r(n))+W(s(n))+R(r(n))*R(s(n)))) end if end proc: for n from 2 to 35 do seq(coeff(W(n), x, k), k = 1 .. degree(W(n))) end do; # yields sequence in triangular form",
				"with(numtheory): W := proc (n) local r, s, R: r := proc (n) options operator, arrow: op(1, factorset(n)) end proc: s := proc (n) options operator, arrow: n/r(n) end proc: R := proc (n) if n = 1 then 0 elif bigomega(n) = 1 then sort(expand(x*R(pi(n))+x)) else sort(expand(R(r(n))+R(s(n)))) end if end proc; if n = 1 then 0 elif bigomega(n) = 1 then sort(expand(W(pi(n))+x*R(pi(n))+x)) else sort(expand(W(r(n))+W(s(n))+R(r(n))*R(s(n)))) end if end proc: W(987654321);"
			],
			"xref": [
				"Cf. A196056, A196058, A196060."
			],
			"keyword": "nonn,tabf",
			"offset": "2,2",
			"author": "_Emeric Deutsch_, Sep 30 2011",
			"references": 8,
			"revision": 23,
			"time": "2017-03-07T11:30:41-05:00",
			"created": "2011-09-30T16:07:02-04:00"
		}
	]
}