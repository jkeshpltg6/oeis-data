{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A267177",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 267177,
			"data": "1,2,1,2,3,3,1,3,3,4,2,4,1,4,2,4,5,4,4,5,1,5,4,4,5,6,3,2,3,6,1,6,3,2,3,6,7,5,5,5,5,7,1,7,5,5,5,5,7,8,4,5,2,5,4,8,1,8,4,5,2,5,4,8,9,6,3,6,6,3,6,9,1,9,6,3,6,6,3,6,9,10,5,6,4,2,4,6,5,10,1,10,5,6,4,2,4,6,5",
			"name": "Irregular triangle read by rows: successive bottom and right-hand borders of the infinite square array in A072030 (which gives number of subtraction steps needed to compute GCD).",
			"comment": [
				"Officially the borders are read starting at the bottom left, reading horizontally until the main diagonal is reached, and then reading vertically upwards until the top row is reached.",
				"However, in this case both borders are symmetric about their midpoints, and the bottom border is the same as the right-hand border, so the direction in which the borders are read is less critical."
			],
			"link": [
				"R. J. Mathar, \u003ca href=\"/A267177/b267177.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"The array in A072030 begins:",
				"1, 2, 3, 4, 5, 6, 7, 8, 9, 10, ...",
				"2, 1, 3, 2, 4, 3, 5, 4, 6, 5, ...",
				"3, 3, 1, 4, 4, 2, 5, 5, 3, 6, ...",
				"4, 2, 4, 1, 5, 3, 5, 2, 6, 4, ...",
				"5, 4, 4, 5, 1, 6, 5, 5, 6, 2, ...",
				"6, 3, 2, 3, 6, 1, 7, 4, 3, 4, ...",
				"7, 5, 5, 5, 5, 7, 1, 8, 6, 6, ...",
				"8, 4, 5, 2, 5, 4, 8, 1, 9, 5, ...",
				"9, 6, 3, 6, 6, 3, 6, 9, 1, 10, ...",
				"10, 5, 6, 4, 2, 4, 6, 5, 10, 1, ...",
				"...",
				"The successive bottom and right-hand borders are:",
				"1,",
				"2, 1, 2,",
				"3, 3, 1, 3, 3,",
				"4, 2, 4, 1, 4, 2, 4,",
				"5, 4, 4, 5, 1, 5, 4, 4, 5,",
				"6, 3, 2, 3, 6, 1, 6, 3, 2, 3, 6,",
				"7, 5, 5, 5, 5, 7, 1, 7, 5, 5, 5, 5, 7,",
				"..."
			],
			"maple": [
				"A267177 := proc(n,k)",
				"    if k \u003c= n then",
				"        A072030(n,k) ;",
				"    else",
				"        A072030(2*n-k,n) ;",
				"    end if;",
				"end proc:",
				"seq(seq(A267177(n,k),k=1..2*n-1),n=1..10) ; # _R. J. Mathar_, May 07 2016"
			],
			"program": [
				"(PARI, based on _Michel Marcus_'s program for A049834)",
				"tabl(nn) = {for (n=1, nn,",
				"for (k=1, n, a = n; b = k; r = 1; s = 0; while (r, q = a\\b; r = a - b*q; s += q; a = b; b = r); print1(s, \", \"); );",
				"for (k=1, n-1, a = n; b = n-k; r = 1; s = 0; while (r, q = a\\b; r = a - b*q; s += q; a = b; b = r); print1(s, \", \"); );",
				"print(); ); }",
				"tabl(12)"
			],
			"xref": [
				"Cf. A072030, A049834, A267178 (parity)."
			],
			"keyword": "nonn,tabf,easy",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Jan 14 2016",
			"references": 3,
			"revision": 24,
			"time": "2016-05-07T11:51:02-04:00",
			"created": "2016-01-14T17:58:24-05:00"
		}
	]
}