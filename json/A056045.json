{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A056045",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 56045,
			"data": "1,3,4,11,6,42,8,107,94,308,12,1718,14,3538,3474,14827,18,68172,20,205316,117632,705686,24,3587174,53156,10400952,4689778,41321522,30,185903342,32,611635179,193542210,2333606816,7049188,10422970784,38",
			"name": "a(n) = Sum_{d|n} binomial(n,d).",
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A056045/b056045.txt\"\u003eTable of n, a(n) for n = 1..3329\u003c/a\u003e (terms 1..500 from T. D. Noe)",
				"Y. Puri and T. Ward, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL4/WARD/short.html\"\u003eArithmetic and growth of periodic orbits\u003c/a\u003e, J. Integer Seqs., Vol. 4 (2001), #01.2.1."
			],
			"formula": [
				"L.g.f.: A(x) = Sum_{n\u003e=1} log( G(x^n,n) ) where G(x,n) = 1 + x*G(x,n)^n. L.g.f. A(x) satisfies: exp(A(x)) = g.f. of A110448. - _Paul D. Hanna_, Nov 10 2007",
				"a(n) = Sum_{k=1..A000005(n)} A007318(n, A027750(k)). - _Reinhard Zumkeller_, Aug 13 2013",
				"a(n) = Sum_{k=1..n} binomial(n,gcd(n,k))/phi(n/gcd(n,k)) = Sum_{k=1..n} binomial(n,n/gcd(n,k))/phi(n/gcd(n,k)) where phi = A000010. - _Richard L. Ollerton_, Nov 08 2021",
				"a(n) = n+1 iff n is prime. - _Bernard Schott_, Nov 30 2021"
			],
			"example": [
				"A(x) = log(1/(1-x) * G(x^2,2) * G(x^3,3) * G(x^4,4) * ...)",
				"where the functions G(x,n) are g.f.s of well-known sequences:",
				"G(x,2) = g.f. of A000108 = 1 + x*G(x,2)^2;",
				"G(x,3) = g.f. of A001764 = 1 + x*G(x,3)^3;",
				"G(x,4) = g.f. of A002293 = 1 + x*G(x,4)^4; etc."
			],
			"mathematica": [
				"f[n_] := Sum[ Binomial[n, d], {d, Divisors@ n}]; Array[f, 37] (* _Robert G. Wilson v_, Apr 23 2005 *)",
				"Total[Binomial[#,Divisors[#]]]\u0026/@Range[40] (* _Harvey P. Dale_, Dec 08 2018 *)"
			],
			"program": [
				"(PARI) {a(n)=n*polcoeff(sum(m=1,n,log(1/x*serreverse(x/(1+x^m +x*O(x^n))))),n)} /* _Paul D. Hanna_, Nov 10 2007 */",
				"(PARI) {a(n)=sumdiv(n,d,binomial(n,d))} /* _Paul D. Hanna_, Nov 10 2007 */",
				"(Haskell)",
				"a056045 n = sum $ map (a007318 n) $ a027750_row n",
				"-- _Reinhard Zumkeller_, Aug 13 2013"
			],
			"xref": [
				"Cf. A110448 (exp(A(x)); A000108 (Catalan numbers), A001764, A002293, A174462.",
				"Cf. A000010 (comments on Dirichlet sum formulas).",
				"Cf. A308943 (similar with Product)."
			],
			"keyword": "nice,nonn",
			"offset": "1,2",
			"author": "_Labos Elemer_, Jul 25 2000",
			"references": 26,
			"revision": 52,
			"time": "2021-11-30T17:04:00-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}