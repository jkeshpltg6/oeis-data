{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A244415",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 244415,
			"data": "0,1,0,1,0,1,0,2,0,1,0,1,0,1,0,2,0,1,0,1,0,1,0,2,0,1,0,1,0,1,0,3,0,1,0,1,0,1,0,2,0,1,0,1,0,1,0,2,0,1,0,1,0,1,0,2,0,1,0,1,0,1,0,3,0,1,0,1,0,1,0,2,0,1,0,1,0,1,0,2,0,1,0,1,0,1,0,2,0,1,0,1,0,1,0,3,0,1,0,1",
			"name": "Exponent of 4 appearing in the 4-adic value of 1/n, n \u003e= 1, given in A240226(n).",
			"comment": [
				"See the comment under A240226 for g-adic value of x and the Mahler reference, p. 7, where this exponent is called f.",
				"Note that the exponent used in the g-adic value of 1/n is also called g-adic valuation of n if g is prime. See e.g., A007814 (g=2) and A007949 (g=3) and the corresponding A006519 and A038500 for the 2-adic and 3-adic value of 1/n, respectively."
			],
			"reference": [
				"K. Mahler, p-adic numbers and their functions, second ed., Cambridge University Press, 1981."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A244415/b244415.txt\"\u003eTable of n, a(n) for n = 1..65537\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 0 if n is odd, and if n is even a(n) = f(1/n) with f(1/n) the smallest positive integer such that the highest power of 2 in n (that is A006519(n)) divides 4^f(1/n).",
				"a(n) = valuation(2*n, 4). - _Andrew Howroyd_, Jul 31 2018"
			],
			"example": [
				"n = 2: A006519(2) = 1, 2 divides 4^1, hence f(1/2) = 1 = a(2).",
				"n = 4: A006519(4) = 2^2, 4 divides 4^1, hence f(1/4) = 1 = a(4).",
				"n = 8: A006519(8) = 2^3, 8 does not divide 4^1 but 4^2, hence f(1/8) = 2 = a(8)."
			],
			"mathematica": [
				"Array[IntegerExponent[2 #, 4] \u0026, 105] (* _Michael De Vlieger_, Nov 06 2018 *)"
			],
			"program": [
				"(PARI) a(n) = valuation(2*n, 4); \\\\ _Andrew Howroyd_, Jul 31 2018"
			],
			"xref": [
				"Cf. A240226, A007814 (case g=2), A007949 (case g=3)."
			],
			"keyword": "nonn,easy",
			"offset": "1,8",
			"author": "_Wolfdieter Lang_, Jun 28 2014",
			"references": 3,
			"revision": 21,
			"time": "2018-11-06T21:18:40-05:00",
			"created": "2014-07-07T00:35:24-04:00"
		}
	]
}