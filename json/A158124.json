{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A158124",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 158124,
			"data": "294001,505447,584141,604171,929573,971767,1062599,1282529,1524181,2017963,2474431,2690201,3070663,3085553,3326489,4393139,5152507,5285767,5564453,5575259,5974249,6173731,6191371,6236179,6463267",
			"name": "Weakly prime numbers (or isolated primes): changing any one decimal digit always produces a composite number, with restriction that first digit may not be changed to a 0.",
			"comment": [
				"The definition could be restated as \"primes p with d digits such that there is no prime q with at most d digits at Hamming distance 1 from p (in base 10)\". - _N. J. A. Sloane_, May 06 2019",
				"For the following values of k, 5, 6, 7, 8, 9, 10, the number of terms \u003c 10^k in this sequence is 0, 6, 43, 406, 3756, 37300. - _Jean-Marc Rebert_, Nov 10 2015"
			],
			"link": [
				"Jean-Marc Rebert, \u003ca href=\"/A158124/b158124.txt\"\u003eTable of n, a(n) for n = 1..3756\u003c/a\u003e",
				"C. Rivera, \u003ca href=\"http://www.primepuzzles.net/puzzles/puzz_017.htm\"\u003eWeakly Primes\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/WeaklyPrime.html\"\u003eWeakly Prime\u003c/a\u003e"
			],
			"maple": [
				"filter:= proc(n)",
				"  local L,i,d,ds;",
				"  if not isprime(n) then return false fi;",
				"  L:= convert(n,base,10);",
				"  for i from 1 to nops(L) do",
				"    if i = nops(L) then ds:= {$1..9} minus {L[i]}",
				"    elif i = 1 then ds:= {1,3,7,9} minus {L[i]}",
				"    else ds:= {$0..9} minus {L[i]}",
				"    fi;",
				"    for d in ds do",
				"      if isprime(n + (d - L[i])*10^(i-1)) then return false fi;",
				"    od",
				"  od;",
				"  true",
				"end proc:",
				"select(filter, [seq(i,i=11..10^6,2)]); # _Robert Israel_, Dec 15 2015"
			],
			"mathematica": [
				"Select[Prime@ Range[10^5], Function[n, Function[w, Total@ Map[Boole@ PrimeQ@ # \u0026, DeleteCases[#, n]] \u0026@ Union@ Flatten@ Map[Function[d, FromDigits@ ReplacePart[w, d -\u003e #] \u0026 /@ If[d == 1, #, Prepend[#, 0]] \u0026@ Range@ 9], Range@ Length@ w] == 0]@ IntegerDigits@ n]] (* _Michael De Vlieger_, Dec 13 2016 *)"
			],
			"program": [
				"(PARI) isokp(n) = {v = digits(n); for (k=1, #v, w = v; if (k==1, idep = 1, idep=0); for (j=idep, 9, if (j != v[k], w[k] = j; ntest = subst(Pol(w), x, 10); if (isprime(ntest), return(0));););); return (1);}",
				"lista(nn) = {forprime(p=2, nn, if (isokp(p), print1(p, \", \")););} \\\\ _Michel Marcus_, Dec 15 2015"
			],
			"xref": [
				"Cf. A050249, A158125 (weakly primes), A186995, A192545."
			],
			"keyword": "nonn,base",
			"offset": "1,1",
			"author": "_Eric W. Weisstein_, Mar 13 2009",
			"ext": [
				"Edited by _Charles R Greathouse IV_, Aug 02 2010",
				"Missing a(3385) inserted into b-file by _Andrew Howroyd_, Feb 23 2018"
			],
			"references": 8,
			"revision": 39,
			"time": "2019-05-06T10:41:05-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}