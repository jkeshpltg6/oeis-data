{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A238453",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 238453,
			"data": "1,1,1,1,1,1,1,2,2,1,1,2,4,2,1,1,4,8,8,4,1,1,2,8,8,8,2,1,1,6,12,24,24,12,6,1,1,4,24,24,48,24,24,4,1,1,6,24,72,72,72,72,24,6,1,1,4,24,48,144,72,144,48,24,4,1,1,10,40,120,240,360,360,240,120",
			"name": "Triangle read by rows: T(n,k) = A001088(n)/(A001088(k)*A001088(n-k)).",
			"comment": [
				"We assume that A001088(0)=1 since it would be the empty product.",
				"These are the generalized binomial coefficients associated with Euler's totient function A000010.",
				"Another name might be the totienomial coefficients."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A238453/b238453.txt\"\u003eRows n = 0..125 of triangle, flattened\u003c/a\u003e",
				"Tom Edgar, \u003ca href=\"http://www.emis.de/journals/INTEGERS/papers/o62/o62.Abstract.html\"\u003eTotienomial Coefficients\u003c/a\u003e, INTEGERS, 14 (2014), #A62.",
				"Tom Edgar and Michael Z. Spivey, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL19/Edgar/edgar3.html\"\u003eMultiplicative functions, generalized binomial coefficients, and generalized Catalan numbers\u003c/a\u003e, Journal of Integer Sequences, Vol. 19 (2016), Article 16.1.6.",
				"Donald E. Knuth and Herbert S. Wilf, \u003ca href=\"http://www.math.upenn.edu/~wilf/website/dm36.pdf\"\u003eThe power of a prime that divides a generalized binomial coefficient\u003c/a\u003e, J. Reine Angew. Math., 396:212-219, 1989."
			],
			"formula": [
				"T(n,k) = A001088(n)/(A001088(k)*A001088(n-k)).",
				"T(n,k) = prod_{i=1..n} A000010(i)/(prod_{i=1..k} A000010(i)*prod_{i=1..n-k} A000010(i)).",
				"T(n,k) = A000010(n)/n*(k/A000010(k)*T(n-1,k-1)+(n-k)/A000010(n-k)*T(n-1,k)).",
				"T(n+1, 2) = A083542(n). - _Michael Somos_, Aug 26 2014",
				"T(n,k) = Product_{i=1..k} (phi(n+1-i)/phi(i)), where phi is Euler's totient function (A000010). - _Werner Schulte_, Nov 14 2018"
			],
			"example": [
				"The first five terms in Euler's totient function are 1,1,2,2,4 and so T(4,2) = 2*2*1*1/((1*1)*(1*1))=4 and T(5,3) = 4*2*2*1*1/((2*1*1)*(1*1))=8.",
				"The triangle begins",
				"1",
				"1 1",
				"1 1 1",
				"1 2 2 1",
				"1 2 4 2 1",
				"1 4 8 8 4 1",
				"1 2 8 8 8 2 1"
			],
			"mathematica": [
				"f[n_] := Product[EulerPhi@ k, {k, n}]; Table[f[n]/(f[k] f[n - k]), {n, 0, 11}, {k, 0, n}] // Flatten (* _Michael De Vlieger_, Apr 19 2016 *)"
			],
			"program": [
				"(Sage)",
				"q=100 #change q for more rows",
				"P=[euler_phi(i) for i in [0..q]]",
				"[[prod(P[1:n+1])/(prod(P[1:k+1])*prod(P[1:(n-k)+1])) for k in [0..n]] for n in [0..len(P)-1]] #generates the triangle up to q rows.",
				"(Haskell)",
				"a238453 n k = a238453_tabl !! n !! k",
				"a238453_row n = a238453_tabl !! n",
				"a238453_tabl = [1] : f [1] a000010_list where",
				"   f xs (z:zs) = (map (div y) $ zipWith (*) ys $ reverse ys) : f ys zs",
				"     where ys = y : xs; y = head xs * z",
				"-- _Reinhard Zumkeller_, Feb 27 2014",
				"(PARI) T(n,k)={prod(i=1, k, eulerphi(n+1-i)/eulerphi(i))} \\\\ _Andrew Howroyd_, Nov 13 2018"
			],
			"xref": [
				"Cf. A000010, A001088, A083542."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Tom Edgar_, Feb 26 2014",
			"references": 12,
			"revision": 47,
			"time": "2018-11-14T15:27:15-05:00",
			"created": "2014-02-27T07:58:08-05:00"
		}
	]
}