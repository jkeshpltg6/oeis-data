{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A300148",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 300148,
			"data": "1,3,9,27,81,243,729,2187,6561,51,153,459,1377,13,39,117,7,21,63,189,567,1701,70,210,630,1890,5670,17010,7,21,63,189,567,1701,70,210,630,1890,5670,17010,7,21,63,189,567,1701,70,210,630,1890,5670,17010,7,21,63,189,567,1701,70,210,630,1890,5670,17010,7",
			"name": "Trajectory of the \"Erase or triple\" protocol applied to 1 (see Comments section for how the protocol works).",
			"comment": [
				"The \"Erase or triple\" protocol describes how to transform an integer K into an integer L: if K has 2 or more identical digits, erase them to get L (1201331 becomes 20); if K has no duplicate digits, triple K to get L (20 becomes 60).",
				"Some integers disappear immediately (like 11, 2002 or 1919188 - see the Crossref section), other enter into a loop if you apply this protocol to the successive results.",
				"Note that 1102 is transformed into 2 because no leading zeroes are admitted.",
				"It seems that all integers up to 10^6 enter into one of those four loops:",
				"Loop zero: 0, 0, 0, 0, 0,...",
				"Loop five: 5, 15, 45, 135, 405, 1215, 25, 75, 225, 5, ...",
				"Loop seven: 7, 21, 63, 189, 567, 1701, 70, 210, 630, 1890, 5670, 17010, 7, ...",
				"Loop eighty-nine : 89, 267, 801, 2403, 7209, 21627, 167, 501,1503, 4509, 13527, 40581, 121743, 2743, 8229, 89, ...",
				"Since L \u003c= 9876543210 if K \u003e 9876543210, it suffices to only look at trajectories for 0 \u003c= K \u003c= 9876543210. Exhaustive search shows every trajectory converges to one of the four loops above. - _Chai Wah Wu_, Feb 11 2019"
			],
			"example": [
				"1 becomes 3 (the triple of 1), then 3 becomes 9 (the triple of 3), then 6 becomes 27 (the triple of 9) and now 81, 243, 729, 2187, 6561, 51 (because the pair 6-6 is erased), 153 (the triple of 54), ... until 17010 which will turn into a term already in the sequence (7). At this point the trajectory cycles."
			],
			"mathematica": [
				"NestWhileList[Function[n, If[#1 == #2, 3 FromDigits@ #1, FromDigits@ #2] \u0026 @@ {#, Select[#, DigitCount[n, 10, #] == 1 \u0026]} \u0026@ IntegerDigits@ n], 1, Unequal, All] (* _Michael De Vlieger_, Feb 28 2018 *)"
			],
			"xref": [
				"Cf. A300149 which shows the first 33333 integers that disappear."
			],
			"keyword": "nonn,base",
			"offset": "1,2",
			"author": "_Eric Angelini_ and _Jean-Marc Falcoz_, Feb 27 2018",
			"references": 4,
			"revision": 27,
			"time": "2019-02-11T13:27:47-05:00",
			"created": "2018-02-28T13:58:59-05:00"
		}
	]
}