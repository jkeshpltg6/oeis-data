{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A119473",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 119473,
			"data": "1,1,1,2,2,3,4,1,5,8,3,8,15,8,1,13,28,19,4,21,51,42,13,1,34,92,89,36,5,55,164,182,91,19,1,89,290,363,216,60,6,144,509,709,489,170,26,1,233,888,1362,1068,446,92,7,377,1541,2580,2266,1105,288,34,1,610,2662,4830",
			"name": "Triangle read by rows: T(n,k) is number of binary words of length n and having k runs of 0's of odd length, 0 \u003c= k \u003c= ceiling(n/2). (A run of 0's is a subsequence of consecutive 0's of maximal length.)",
			"comment": [
				"Row n has 1+ceiling(n/2) terms.",
				"T(n,0) = Fibonacci(n+1) = A000045(n+1).",
				"T(n,1) = A029907(n).",
				"Sum_{k\u003e=0} k*T(n,k) = A059570(n).",
				"Triangle, with zeros included, given by (1,1,-1,0,0,0,0,0,0,0,...) DELTA (1,-1,0,0,0,0,0,0,0,0,...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Dec 07 2011",
				"T(n,k) is the number of compositions of n+1 that have exactly k even parts. - _Geoffrey Critzer_, Mar 03 2012"
			],
			"reference": [
				"I. Goulden and D. Jackson, Combinatorial Enumeration, John Wiley and Sons, 1983, page 54."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A119473/b119473.txt\"\u003eRows n = 0..200, flattened\u003c/a\u003e",
				"R. Grimaldi and S. Heubach, \u003ca href=\"https://www.researchgate.net/publication/220621147_Binary_Strings_Without_Odd_Runs_of_Zeros\"\u003eBinary strings without odd runs of zeros\u003c/a\u003e, Ars Combinatoria 75 (2005), 241-255."
			],
			"formula": [
				"G.f.: (1+t*z)/(1-z-z^2-t*z^2).",
				"G.f. of column k (k\u003e=1): z^(2*k-1)*(1-z^2)/(1-z-z^2)^(k+1).",
				"T(n,k) = T(n-1,k) + T(n-2,k) + T(n-2,k-1). - _Philippe Deléham_, Dec 07 2011",
				"Sum_{k=0..n} T(n,k)*x^k = A000045(n+1), A000079(n), A105476(n+1), A159612(n+1), A189732(n+1) for x = 0, 1, 2, 3, 4 respectively. - _Philippe Deléham_, Dec 07 2011",
				"G.f.: (1+x*y)*T(0)/2, where T(k) = 1 + 1/(1 - (2*k+1+ x*(1+y))*x/((2*k+2+ x*(1+y))*x + 1/T(k+1) )); (continued fraction). - _Sergei N. Gladkovskii_, Nov 06 2013"
			],
			"example": [
				"T(5,2)=8 because we have 00010, 01000, 01011, 01101, 01110, 10101, 10110 and 11010.",
				"T(5,2)=8 because there are 8 compositions of 6 that have 2 even parts: 4+2, 2+4, 2+2+1+1, 2+1+2+1, 2+1+1+2, 1+2+2+1, 1+2+1+2, 1+1+2+2. - _Geoffrey Critzer_, Mar 03 2012",
				"Triangle starts:",
				"  1;",
				"  1,  1;",
				"  2,  2;",
				"  3,  4,  1;",
				"  5,  8,  3;",
				"  8, 15,  8,  1;",
				"From _Philippe Deléham_, Dec 07 2011: (Start)",
				"Triangle (1,1,-1,0,0,0...) DELTA (1,-1,0,0,0,...) begins:",
				"   1;",
				"   1,  1;",
				"   2,  2,  0;",
				"   3,  4,  1,  0;",
				"   5,  8,  3,  0,  0;",
				"   8, 15,  8,  1,  0,  0;",
				"  13, 28, 19,  4,  0,  0,  0;",
				"  21, 51, 42, 13,  1,  0,  0,  0;",
				"  34, 92, 89, 36,  5,  0,  0,  0,  0; ... (End)"
			],
			"maple": [
				"G:=(1+t*z)/(1-z-z^2-t*z^2): Gser:=simplify(series(G,z=0,18)): P[0]:=1: for n from 1 to 14 do P[n]:=sort(coeff(Gser,z^n)) od: for n from 0 to 14 do seq(coeff(P[n],t,j),j=0..ceil(n/2)) od; # yields sequence in triangular form",
				"# second Maple program:",
				"b:= proc(n) option remember; local j; if n=0 then 1",
				"      else []; for j to n do zip((x, y)-\u003ex+y, %,",
				"      [`if`(irem(j, 2)=0, 0, NULL), b(n-j)], 0) od; %[] fi",
				"    end:",
				"T:= n-\u003e b(n+1):",
				"seq(T(n), n=0..14);  # _Alois P. Heinz_, May 23 2013"
			],
			"mathematica": [
				"f[list_] := Select[list, # \u003e 0 \u0026]; nn = 15; a = (x + y x^2)/(1 - x^2); Map[f, Drop[CoefficientList[Series[1/(1 - a), {x, 0, nn}], {x, y}], 1]] // Flatten  (* _Geoffrey Critzer_, Mar 03 2012 *)"
			],
			"xref": [
				"Cf. A000045, A029907, A059570."
			],
			"keyword": "nonn,tabf",
			"offset": "0,4",
			"author": "_Emeric Deutsch_, May 22 2006",
			"references": 4,
			"revision": 26,
			"time": "2019-11-16T20:08:45-05:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}