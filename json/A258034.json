{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A258034",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 258034,
			"data": "1,2,0,0,2,0,0,0,0,4,4,0,0,4,0,0,2,0,4,0,0,0,0,0,0,6,0,0,0,0,0,0,0,0,4,0,4,4,0,0,4,0,0,0,0,8,0,0,0,2,0,0,4,0,0,0,0,0,4,0,0,4,0,0,2,0,0,0,0,0,0,0,4,4,0,0,0,0,0,0,0,4,4,0,0,8,0",
			"name": "Expansion of phi(q) * phi(q^9) in powers of q where phi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A258034/b258034.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e (terms 0..1000 from G. C. Greubel)",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of eta(q^2)^5 * eta(q^18)^5 / (eta(q) * eta(q^4) * eta(q^9) * eta(q^36))^2 in powers of q.",
				"Euler transform of period 36 sequence [2, -3, 2, -1, 2, -3, 2, -1, 4, -3, 2, -1, 2, -3, 2, -1, 2, -6, 2, -1, 2, -3, 2, -1, 2, -3, 4, -1, 2, -3, 2, -1, 2, -3, 2, -2, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (36 t)) = 6 (t/i) f(t) where q = exp(2 Pi i t).",
				"a(n) = (-1)^n * A258322(n). a(4*n) = a(n).",
				"a(3*n + 2) = a(4*n + 3) = a(8*n + 6) =  a(9*n + 3) = a(9*n + 6) = 0.",
				"a(3*n + 1) = 2 * A122865(n).  a(6*n + 4) = 2 * A122856(n). a(9*n) = A004018(n). a(12*n + 1) = 2 * A002175(n).",
				"a(2*n) = A028601(n). - _Michael Somos_, Jul 04 2015"
			],
			"example": [
				"G.f. = 1 + 2*q + 2*q^4 + 4*q^9 + 4*q^10 + 4*q^13 + 2*q^16 + 4*q^18 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 3, 0, q] EllipticTheta[ 3, 0, q^9], {q, 0, n}];",
				"a[ n_] := Which[ n \u003c 1, Boole[n == 0], Mod[n, 3] == 2, 0, True, 2 DivisorSum[ n, If[ Mod[n/#, 9] \u003e 0, 1, 2] KroneckerSymbol[ -4, #] \u0026]]; (* _Michael Somos_, Jul 04 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n==0, (n+1)%3 * sumdiv(n, d, [0, 1, 2, -1][d%4 + 1] * if(d%9, 1, 4) * (-1)^((d%8==6) + n+d)))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^5 * eta(x^18 + A)^5 / (eta(x + A) * eta(x^4 + A) * eta(x^9 + A) * eta(x^36 + A))^2, n))};",
				"(PARI) {a(n) = if( n\u003c1, n==0, n%3==2, 0, 2 * sumdiv(n, d, if(n\\d%9, 1, 2) * kronecker( -4, d)))}; /* _Michael Somos_, Jul 04 2015 */",
				"(PARI) {a(n) = my(A, p, e); if( n\u003c1, n==0, A = factor(n); (n%3 \u003c 2) * 2 * prod( k=1, matsize(A)[1], [p, e] = A[k, ]; if( p==2, 1, p==3, 1 + (-1)^e, p%12\u003e6, (1 + (-1)^e) / 2, e+1)))}; /* _Michael Somos_, Jul 04 2015 */",
				"(MAGMA) A := Basis( ModularForms( Gamma1(36), 1), 87); A[1] + 2*A[2] + 2*A[5] + 4*A[10] + 4*A[11] + 4*A[14] + 2*A[17] + 4*A[19];"
			],
			"xref": [
				"Cf. A002175, A004018, A122856, A122865, A258322."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Michael Somos_, Jun 03 2015",
			"references": 5,
			"revision": 37,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2015-06-03T12:12:46-04:00"
		}
	]
}