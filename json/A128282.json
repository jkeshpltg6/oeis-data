{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A128282",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 128282,
			"data": "1,2,2,3,4,3,5,6,6,5,7,8,9,8,7,10,11,12,12,11,10,13,14,15,16,15,14,13,17,18,19,20,20,19,18,17,21,22,23,24,25,24,23,22,21,26,27,28,29,30,30,29,28,27,26,31,32,33,34,35,36,35,34,33,32,31,37,38,39,40,41,42,42,41",
			"name": "Regular symmetric triangle, read by rows, whose left half consists of the positive integers.",
			"comment": [
				"Left half triangle is A000027 (positive integers) (compare with example triangle):",
				"   1;",
				"   2;",
				"   3,  4;",
				"   5,  6;",
				"   7,  8,  9;",
				"  10, 11, 12;",
				"  13, 14, 15, 16;",
				"  17, 18, 19, 20;",
				"  ..."
			],
			"link": [
				"Jianrui Xie, \u003ca href=\"https://arxiv.org/abs/2105.10752\"\u003eOn Symmetric Invertible Binary Pairing Functions\u003c/a\u003e, arXiv:2105.10752 [math.CO], 2021. See (6) p. 3 and p. 5"
			],
			"formula": [
				"T(n,k) = T(n,n-k).",
				"T(2*n,n) = (n+1)^2 = A000290(n+1).",
				"T(n,0) = T(n,n) = A033638(n+1).",
				"From _Yu-Sheng Chang_, May 25 2020: (Start)",
				"O.g.f.: F(z,v) = (z/((-z+1)^3*(z+1)) - v^2*z/((-v*z+1)^3*(v*z+1)))/(1-v) + 1/((-z+1)*(-v*z+1)*(-v*z^2+1)).",
				"T(n,k) = [v^k] (1/8)*(1-v^(n+1))*(2*(n+1)^2 - 1 - (-1)^n)/(1-v) + (v^(2+n) + (1/2*((sqrt(v)-1)^2*(-1)^n - (sqrt(v)+1)^2))*v^((1/2)*n + 1/2) + 1)/(1-v)^2.",
				"T(n,k) = 1 + (1/4)*n*(n+1) + min(k, n-k) + (1/2)*ceiling((1/2)*n). (End)",
				"T(n,k) = ((n+k-1)^2 - ((n+k-1) mod 2))/4 + min(n,k) for n and k \u003e= 1, as an array. See Xie. - _Michel Marcus_, May 25 2021"
			],
			"example": [
				"Triangle begins:",
				"   1;",
				"   2,  2;",
				"   3,  4,  3;",
				"   5,  6,  6,  5;",
				"   7,  8,  9,  8,  7;",
				"  10, 11, 12, 12, 11, 10;",
				"  13, 14, 15, 16, 15, 14, 13;",
				"  17, 18, 19, 20, 20, 19, 18, 17;",
				"  ..."
			],
			"maple": [
				"A := proc(n,k) ## n \u003e= 0 and k = 0 .. n",
				"    1+(1/4)*n*(n+1)+min(k, n-k)+(1/2)*ceil((1/2)*n)",
				"end proc: # _Yu-Sheng Chang_, May 25 2020"
			],
			"xref": [
				"Cf. A000027, A000290, A033638 (1st column and right diagonal)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,2",
			"author": "_Philippe Deléham_, May 03 2007",
			"ext": [
				"Name edited by _Michel Marcus_, May 25 2021"
			],
			"references": 2,
			"revision": 57,
			"time": "2021-05-26T02:18:48-04:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}