{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A094811",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 94811,
			"data": "1,6,26,100,364,1288,4488,15504,53296,182688,625184,2137408,7303360,24946816,85196928,290926848,993379072,3391793664,11580678656,39539651584,134998297600,460915984384,1573671536640,5372862566400,18344123969536",
			"name": "Number of (s(0), s(1), ..., s(2n+1)) such that 0 \u003c s(i) \u003c 8 and |s(i) - s(i-1)| = 1 for i = 1,2,....,2n+1, s(0) = 1, s(2n+1) = 6.",
			"comment": [
				"In general a(n)= 2/m*Sum(r,1,m-1,Sin(r*j*Pi/m)Sin(r*k*Pi/m)(2Cos(r*Pi/m))^(2n+1)) counts (s(0), s(1), ..., s(2n+1)) such that 0 \u003c s(i) \u003c m and |s(i) - s(i-1)| = 1 for i = 1,2,....,2n+1, s(0) = j, s(2n+1) = k."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A094811/b094811.txt\"\u003eTable of n, a(n) for n = 2..1000\u003c/a\u003e",
				"G. Kreweras, \u003ca href=\"/A000108/a000108_1.pdf\"\u003eSur les éventails de segments\u003c/a\u003e, Cahiers du Bureau Universitaire de Recherche Opérationnelle, Institut de Statistique, Université de Paris, #15 (1970), 3-41. [Annotated scanned copy]",
				"László Németh and László Szalay, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL24/Nemeth/nemeth8.html\"\u003eSequences Involving Square Zig-Zag Shapes\u003c/a\u003e, J. Int. Seq., Vol. 24 (2021), Article 21.5.2.",
				"Xavier Gérard Viennot, \u003ca href=\"http://dx.doi.org/10.1016/S0012-365X(01)00265-5\"\u003eA Strahler bijection between Dyck paths and planar trees\u003c/a\u003e. Formal power series and algebraic combinatorics (Barcelona, 1999). Discrete Math. 246 (2002), no. 1-3, 317--329. MR1887493 (2003b:05013)",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (6,-10,4)."
			],
			"formula": [
				"a(n) = (1/4)*Sum(r, 1, 7, Sin(r*Pi/8)Sin(r*3*Pi/4)(2Cos(r*Pi/8))^(2n+1)).",
				"G.f.: x^2/((1-2*x)*(1-4*x+2*x^2)).",
				"a(n) = 6*a(n-1)-10*a(n-2)+4*a(n-3).",
				"a(n) = A005022(n-2), n\u003e2. - _R. J. Mathar_, Sep 05 2008",
				"The g.f. x^3/(1-6x+10x^2-4x^3) occurs on page 320 of Viennot, 2002.",
				"a(n) = (A006012(n)-2^n)/2. - _R. J. Mathar_, Jun 29 2012",
				"a(n) = (-2^(1+n)+(2-sqrt(2))^n+(2+sqrt(2))^n)/4. - _Colin Barker_, Apr 27 2016",
				"E.g.f.: exp(2*x)*sinh(x/sqrt(2))^2. - _Ilya Gutkovskiy_, Apr 27 2016"
			],
			"mathematica": [
				"CoefficientList[Series[1/((1 - 2x)(1 - 4x + 2x^2)), {x, 0, 200}], x] (* _Vincenzo Librandi_, Oct 21 2012 *)",
				"Table[FullSimplify[TrigToExp[(1/4) Sum[Sin[r*Pi/8] Sin[3 r Pi/4] (2 Cos[r Pi/8])^(2 n + 1), {r, 7}]]], {n, 2, 26}] (* _Michael De Vlieger_, Apr 27 2016 *)"
			],
			"program": [
				"(MAGMA) I:=[1, 6, 26]; [n le 3 select I[n] else 6*Self(n-1) - 10*Self(n-2) + 4*Self(n-3): n in [1..30]]; // _Vincenzo Librandi_, Oct 21 2012"
			],
			"xref": [
				"See A005022 for another version."
			],
			"keyword": "nonn,easy",
			"offset": "2,2",
			"author": "_Herbert Kociemba_, Jun 11 2004",
			"ext": [
				"Additional comments from _N. J. A. Sloane_, May 01 2012"
			],
			"references": 4,
			"revision": 32,
			"time": "2021-08-04T21:20:02-04:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}