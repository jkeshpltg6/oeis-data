{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A022388",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 22388,
			"data": "6,13,19,32,51,83,134,217,351,568,919,1487,2406,3893,6299,10192,16491,26683,43174,69857,113031,182888,295919,478807,774726,1253533,2028259,3281792,5310051,8591843,13901894,22493737,36395631,58889368,95284999,154174367,249459366,403633733,653093099",
			"name": "Fibonacci sequence beginning 6, 13.",
			"comment": [
				"The Pisano periods for this sequence are different from those for the Fibonacci numbers (A001175) for modulus 11 and 22. Furthermore, its Pisano periods are exactly the same as those of the Lucas sequence (A000032), given in A106291. - _Klaus Purath_, Apr 20 2019",
				"a(n) is the alternating sum of 5 consecutive Lucas numbers (A000032). Also the sum of 4*k consecutive terms of A000285 divided by Fibonacci(2*k) (A000045), k = {1, 2, …}. All involved sequences extended to negative indices, following the rule a(n-1) = a(n+1) - a(n). - _Klaus Purath_, Jul 29 2019"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A022388/b022388.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Tanya Khovanova, \u003ca href=\"http://www.tanyakhovanova.com/RecursiveSequences/RecursiveSequences.html\"\u003eRecursive Sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1, 1)."
			],
			"formula": [
				"G.f.: (6+7*x)/(1-x-x^2). - _Philippe Deléham_, Nov 20 2008",
				"a(n) = 6*Fibonacci(n+2) + Fibonacci(n) = 6*Fibonacci(n-1) + 13*Fibonacci(n). - _G. C. Greubel_, Mar 02 2018",
				"From _Klaus Purath_, Jul 29 2019: (Start)",
				"L = Lucas (A000032), F = Fibonacci (A000045). All involved sequences extended to negative indices, following the rule a(n-1) = a(n+1) - a(n).",
				"a(n+1) - a(n-4) = L(n)*11.",
				"a(n) = L(n-1) + L(n+4).",
				"a(n) = 3*L(n+1) + L(n+2) = L(n) + 4*L(n+1) = L(n+6) - 4*L(n+2).",
				"a(n) = L(n+1) + 5*F(n+2) = L(n+5) - 5*F(n+1).",
				"a(n) = (7*L(n+1) + 5*F(n+1))/2.",
				"a(n) = (13*L(n+1) + L(n+5) - 5*F(n))/4.",
				"a(n) = 7*F(n) + 6*F(n+1) = 7*F(n+2) - F(n+1).",
				"a(n) = 8*F(n+2) - F(n+3) = 17*F(n+4) - 9*F(n+5).",
				"The following six formulas apply for all sequences of the Fibonacci type.",
				"a(n) = L(2*m)*a(n+2*m) - a(n+4*m).",
				"a(n) = (F(m+2)*a(n+2) - a(m+n+2))/F(m).",
				"a(n) = F(n-m-1)*a(m) + F(n-m)*a(m+1).",
				"a(n)^2 + a(n+3)^2 = 2*(a(n+1)^2 + a(n+2)^2).",
				"a(n)^2 + a(n+2)^2 + a(n+1)^2 + a(n+3)^2 = 3*(a(n)*a(n+2) + a(n+1)*a(n+3)).",
				"3*a(n+2)*a(n+1)*a(n) = a(n+2)^3 - a(n+1)^3 - a(n)^3. (End)",
				"E.g.f.: exp(-2*x/(1+sqrt(5)))*(-15-sqrt(5)+(45+19*sqrt(5))*exp(sqrt(5)*x))/(5+3*sqrt(5)). - _Stefano Spezia_, Aug 16 2019"
			],
			"mathematica": [
				"Table[6*Fibonacci[n+2] + Fibonacci[n], {n, 0, 40}] (* or *) LinearRecurrence[{1,1}, {6,13}, 40] (* _G. C. Greubel_, Mar 02 2018 *)"
			],
			"program": [
				"(PARI) vector(40, n, n--; 6*fibonacci(n+2) + fibonacci(n)) \\\\ _G. C. Greubel_, Mar 02 2018",
				"(MAGMA) [6*Fibonacci(n+2) + Fibonacci(n): n in [0..40]]; // _G. C. Greubel_, Mar 02 2018",
				"(Sage) [6*fibonacci(n+2) + fibonacci(n) for n in (0..40)] # _G. C. Greubel_, Jun 30 2019",
				"(GAP) List([0..40], n-\u003e 6*Fibonacci(n+2) + Fibonacci(n)) # _G. C. Greubel_, Jun 30 2019"
			],
			"xref": [
				"Cf. A000032."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Terms a(36) onward added by _G. C. Greubel_, Mar 02 2018"
			],
			"references": 4,
			"revision": 48,
			"time": "2019-08-22T14:05:52-04:00",
			"created": "1998-06-14T03:00:00-04:00"
		}
	]
}