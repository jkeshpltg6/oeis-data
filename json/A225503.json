{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A225503",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 225503,
			"data": "6,3,15,21,66,78,1326,190,1035,435,465,17205,861,903,9870,5565,1567335,16836,20100,2556,2628,49770,55278,4005,42195,413595,47895,10100265,5995,1437360,32131,8646,1352190,19559385,54397665,1642578,12246,52975,501501,134940,336324807802305",
			"name": "Least triangular number t such that t = prime(n)*triangular(m) for some m\u003e0, or 0 if no such t exists.",
			"comment": [
				"Conjecture: a(n) \u003e 0.",
				"a(n) = (x^2-1)/8 where x is the least odd solution \u003e 1 of the Pell-like equation x^2 - prime(n)*y^2 = 1 - prime(n). - _Robert Israel_, Jan 08 2015"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A225503/b225503.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e (n = 1..200 from Zak Seidov)."
			],
			"example": [
				"See A225502."
			],
			"maple": [
				"F:= proc(n) local p, S,x,y, z, cands, s;",
				"      p:= ithprime(n);",
				"      S:= {isolve(x^2 - p*y^2 = 1-p)};",
				"      for z from 0 do",
				"        cands:= select(s -\u003e (subs(s,x) \u003e 1 and subs(s,x)::odd), simplify(eval(S,_Z1=z)));",
				"        if cands \u003c\u003e {} then",
				"           x:= min(map(subs,cands, x));",
				"           return((x^2-1)/8)",
				"        fi",
				"      od;",
				"end proc:",
				"map(F, [$1..100]); # _Robert Israel_, Jan 08 2015"
			],
			"mathematica": [
				"a[n_] := Module[{p, x0, sol, x, y}, p = Prime[n]; x0 = Which[n == 1, 7, n == 2, 5, True, sol = Table[Solve[x \u003e 1 \u0026\u0026 y \u003e 1 \u0026\u0026 x^2 - p y^2 == 1 - p, {x, y}, Integers] /. C[1] -\u003e c, {c, 0, 1}] // Simplify; Select[x /. Flatten[sol, 1], OddQ] // Min]; (x0^2 - 1)/8];",
				"Array[a, 171] (* _Jean-François Alcover_, Apr 02 2019, after _Robert Israel_ *)"
			],
			"program": [
				"(C)",
				"#include \u003cstdio.h\u003e",
				"#define TOP 300",
				"typedef unsigned long long U64;",
				"U64 isTriangular(U64 a) {",
				"    U64 sr = 1ULL\u003c\u003c32, s, b, t;",
				"    if (a \u003c (sr/2)*(sr+1))  sr\u003e\u003e=1;",
				"    while (a \u003c sr*(sr+1)/2)  sr\u003e\u003e=1;",
				"    for (b = sr\u003e\u003e1; b; b\u003e\u003e=1) {",
				"        s = sr+b;",
				"        if (s\u00261) t = s*((s+1)/2);",
				"        else     t = (s/2)*(s+1);",
				"        if (t \u003e= s \u0026\u0026 a \u003e= t)  sr = s;",
				"    }",
				"    return (sr*(sr+1)/2 == a);",
				"}",
				"int main() {",
				"  U64 i, j, k, m, tm, p, pp = 1, primes[TOP];",
				"  for (primes[0]=2, i = 3; pp \u003c TOP; i+=2) {",
				"    for (p = 1; p \u003c pp; ++p) if (i%primes[p]==0) break;",
				"    if (p==pp) {",
				"        primes[pp++] = i;",
				"        for (j=p=primes[pp-2], m=tm=1; ; j=k, m++, tm+=m) {",
				"           if ((k = p*tm) \u003c j) k=0;",
				"           if (isTriangular(k)) break;",
				"        }",
				"        printf(\"%llu, \", k);",
				"    }",
				"  }",
				"  return 0;",
				"}",
				"(PARI) a(n) = {p = prime(n); k = 1; while (! ((t=k*(k+1)/2) \u0026\u0026 ((t % p) == 0) \u0026\u0026 ispolygonal(t/p, 3)), k++); t;} \\\\ _Michel Marcus_, Jan 08 2015"
			],
			"xref": [
				"Cf. A000217, A112456, A225502."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Alex Ratushnyak_, May 09 2013",
			"ext": [
				"a(171) from _Giovanni Resta_, Jun 19 2013"
			],
			"references": 5,
			"revision": 27,
			"time": "2019-04-02T08:06:31-04:00",
			"created": "2013-05-09T18:10:01-04:00"
		}
	]
}