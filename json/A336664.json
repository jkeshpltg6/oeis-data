{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A336664",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 336664,
			"data": "1,1,2,2,2,2,2,2,3,2,2,2,2,2,3,2,2,3,2,2,2,2,2,2,3,2,3,2,2,3,2,2,2,2,3,3,2,2,3,3,2,2,2,2,4,2,2,2,3,3,3,2,2,3,3,2,2,2,2,2,2,2,3,2,2,2,2,2,2,3,2,2,2,2,4,2,2,3,2,3,3,2,2,2,3,2,3,2,2,4,3,2,2,2,3,2",
			"name": "Number of distinct divisors d of n with the property that d = (the number of nonnegative bases m \u003c n such that m^k == m (mod n))/(the number of nonnegative bases m \u003c n such that -m^k == m (mod n)) for some nonnegative k.",
			"comment": [
				"For any k \u003e= 0, the value of (the number of nonnegative bases m \u003c n such that m^k == m (mod n))/(the number of nonnegative bases m \u003c n such that -m^k == m (mod n)) is the value of some part of n, and is equal or unequal to the value of some divisor of n. Starting with k = 2, these infinite sequences of the parts of n are periodic with a period equal to A002322(n) (or A000010(n)). Also these sequences are different, but among n there are pairs of numbers (1 and 2, 3 and 6, ...) for which they are the same.",
				"For n \u003e= 1, minimal d is equal to 1 and maximal d is equal to A026741(n).",
				"If n: 1, 2, 3, 5, 6, 10, 15, 17, 30, 34, 51, 85, 102, ..., then d = (the number of nonnegative bases m \u003c n such that m^k = m)/(the number of nonnegative bases m \u003c n such that -m^k = m) for all nonnegative k."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A336664/b336664.txt\"\u003eTable of n, a(n) for n = 1..3755\u003c/a\u003e"
			],
			"example": [
				"For n = 1 the a(1) = 1 solution d is 1 (k = 0),",
				"n = 2 the a(2) = 1 solution d is 1 (k = 0),",
				"n = 3 the a(3) = 2 solutions d are 1 (k = 0) and 3 (k = 1),",
				"n = 4 the a(4) = 2 solutions d are 1 (k = 0) and 2 (k = 1),",
				"n = 5 the a(5) = 2 solutions d are 1 (k = 0) and 5 (k = 1),",
				"n = 6 the a(6) = 2 solutions d are 1 (k = 0) and 3 (k = 1),",
				"n = 7 the a(7) = 2 solutions d are 1 (k = 0) and 7 (k = 1),",
				"n = 8 the a(8) = 2 solutions d are 1 (k = 0) and 4 (k = 1),",
				"n = 9 the a(9) = 3 solutions d are 1 (k = 0), 3 (k = 3) and 9 (k = 1)."
			],
			"program": [
				"(PARI) T(n, k) = sum(m=0, n-1, Mod(m, n)^k == m)/sum(m=0, n-1, Mod(-m, n)^k == m); \\\\ A334006",
				"vec(n) = vecsort(vector(n, k, T(n,k-1)),,8);",
				"a(n) = { my(v=vec(n)); sumdiv(n, d, vecsearch(v, d) != 0); }; \\\\ _Michel Marcus_, Aug 27 2020, edited for speed by _Antti Karttunen_, Dec 13 2021"
			],
			"xref": [
				"Cf. A000010, A002322, A026741, A334006."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Juri-Stepan Gerasimov_, Jul 29 2020",
			"references": 3,
			"revision": 25,
			"time": "2021-12-13T16:12:56-05:00",
			"created": "2020-09-26T11:24:20-04:00"
		}
	]
}