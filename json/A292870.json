{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A292870",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 292870,
			"data": "1,1,0,1,1,0,1,2,2,0,1,3,5,5,0,1,4,9,14,15,0,1,5,14,28,44,52,0,1,6,20,48,93,154,203,0,1,7,27,75,169,333,595,877,0,1,8,35,110,280,624,1289,2518,4140,0,1,9,44,154,435,1071,2442,5394,11591,21147,0,1,10,54,208,644,1728,4265,10188,24366,57672,115975,0",
			"name": "Square array A(n,k), n\u003e=0, k\u003e=0, read by antidiagonals, where column k is the expansion of k-th power of continued fraction 1/(1 - x - x^2/(1 - 2*x - 2*x^2/(1 - 3*x - 3*x^2/(1 - 4*x - 4*x^2/(1 - ...))))).",
			"comment": [
				"A(n,k) is the n-th term of the k-fold convolution of Bell numbers with themselves. - _Alois P. Heinz_, Feb 12 2019"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A292870/b292870.txt\"\u003eAntidiagonals n = 0..140, flattened\u003c/a\u003e"
			],
			"formula": [
				"G.f. of column k: (1/(1 - x - x^2/(1 - 2*x - 2*x^2/(1 - 3*x - 3*x^2/(1 - 4*x - 4*x^2/(1 - ...))))))^k, a continued fraction."
			],
			"example": [
				"G.f. of column k: A_k(x) = 1 + k*x + k*(k + 3)*x^2/2 + k*(k^2 + 9*k + 20)*x^3/6 + k*(k^3 + 18*k^2 + 107*k + 234)*x^4/24 + k*(k^4 + 30*k^3 + 335*k^2 + 1770*k + 4104)*x^5/120 + ...",
				"Square array begins:",
				"  1,   1,    1,    1,    1,     1,  ...",
				"  0,   1,    2,    3,    4,     5,  ...",
				"  0,   2,    5,    9,   14,    20,  ...",
				"  0,   5,   14,   28,   48,    75,  ...",
				"  0,  15,   44,   93,  169,   280,  ...",
				"  0,  52,  154,  333,  624,  1071,  ..."
			],
			"maple": [
				"A:= proc(n, k) option remember; `if`(n=0, 1, `if`(k=0, 0,",
				"     `if`(k=1, add(A(n-j, k)*binomial(n-1, j-1), j=1..n),",
				"     (h-\u003e add(A(j, h)*A(n-j, k-h), j=0..n))(iquo(k,2)))))",
				"    end:",
				"seq(seq(A(n, d-n), n=0..d), d=0..12);  # _Alois P. Heinz_, May 31 2018"
			],
			"mathematica": [
				"Table[Function[k, SeriesCoefficient[1/(1 - x + ContinuedFractionK[-i x^2, 1 - (i + 1) x, {i, 1, n}])^k, {x, 0, n}]][j - n], {j, 0, 11}, {n, 0, j}] // Flatten"
			],
			"xref": [
				"Columns k=0-4 give A000007, A000110, A014322, A014323, A014325.",
				"Rows n=0-3 give A000012, A001477, A000096, A005586.",
				"Antidiagonal sums give A137551.",
				"Main diagonal gives A292871.",
				"Cf. A205574 (another version)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Ilya Gutkovskiy_, Sep 25 2017",
			"references": 7,
			"revision": 14,
			"time": "2019-02-12T12:05:51-05:00",
			"created": "2017-09-25T13:08:45-04:00"
		}
	]
}