{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A046034",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 46034,
			"data": "2,3,5,7,22,23,25,27,32,33,35,37,52,53,55,57,72,73,75,77,222,223,225,227,232,233,235,237,252,253,255,257,272,273,275,277,322,323,325,327,332,333,335,337,352,353,355,357,372,373,375,377,522,523,525,527,532",
			"name": "Numbers whose digits are primes.",
			"comment": [
				"A055642(a(n)) = A193238(a(n)). - _Reinhard Zumkeller_, Jul 19 2011",
				"If n is represented as a zerofree base-4 number (see A084544) according to n=d(m)d(m-1)...d(3)d(2)d(1)d(0) then a(n)= sum_{j=0..m} c(d(j))*10^j, where c(k)=2,3,5,7 for k=1..4. - _Hieronymus Fischer_, May 30 2012",
				"According to A153025, it seems that 5, 235 and 72335 are the only terms whose square is again a term, i.e., which are also in the sequence A275971 of square roots of the terms which are squares, listed in A191486. - _M. F. Hasler_, Sep 16 2016"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A046034/b046034.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/SmarandacheSequences.html\"\u003eSmarandache Sequences.\u003c/a\u003e",
				"\u003ca href=\"/index/Ar#10-automatic\"\u003eIndex entries for 10-automatic sequences\u003c/a\u003e."
			],
			"formula": [
				"From _Hieronymus Fischer_, Apr 20, May 30 and Jun 25 2012: (Start)",
				"a(n) = sum_{j=0..m-1} ((2*b(j)+1) mod 8 + floor(b(j)/4) - floor((b(j)-1)/4))*10^j, where m = floor(log_4(3*n+1)), b(j) = floor((3*n+1-4^m)/(3*4^j)).",
				"Also: a(n) = sum_{j=0..m-1} (A010877(A005408(b(j)) + A002265(b(j)) - A002265(b(j)-1))*10^j.",
				"Special values:",
				"a(1*(4^n-1)/3) = 2*(10^n-1)/9.",
				"a(2*(4^n-1)/3) = 1*(10^n-1)/3.",
				"a(3*(4^n-1)/3) = 5*(10^n-1)/9.",
				"a(4*(4^n-1)/3) = 7*(10^n-1)/9.",
				"Inequalities:",
				"a(n) \u003c= 2*(10^log_4(3*n+1)-1)/9, equality holds for n = (4^k-1)/3, k\u003e0.",
				"a(n) \u003c= 2*A084544(n), equality holds iff all digits of A084544(n) are 1.",
				"a(n) \u003e A084544(n).",
				"Lower and upper limits:",
				"lim inf a(n)/10^log_4(n) = 7/90*10^log_4(3) = 0.482321677069870, for n --\u003e inf.",
				"lim sup a(n)/10^log_4(n) = 2/9*10^log_4(3) = 1.37806193448534318470, for n --\u003e inf.",
				"where 10^log_4(n) = n^1.66096404744...",
				"G.f.: g(x) = (x^(1/3)*(1-x))^(-1) sum_{j=\u003e0} 10^j*z(j)^(4/3)*(2 + z(j) + 2*z(j)^2 + 2*z(j)^3 - 7*z(j)^4)/(1-z(j)^4), where z(j) = x^4^j.",
				"Also g(x) = (x^(1/3)*(1-x))^(-1) sum_{j\u003e=0} 10^j*z(j)^(4/3)*(1-z(j))*(2 + 3z(j) + 5*z(j)^2 + 7*z(j)^3)/(1-z(j)^4), where z(j)=x^4^j.",
				"Also: g(x) = (1/(1-x))*(2*h_(4,0)(x) + h_(4,1)(x) + 2*h_(4,2)(x) + 2*h_(4,3)(x) - 7*h_(4,4)(x)), where h_(4,k)(x) = sum_{j\u003e=0} 10^j*x^((4^(j+1)-1)/3)*x^(k*4^j)/(1-x^4^(j+1)).",
				"(End)"
			],
			"example": [
				"a(100)   = 2277,",
				"a(10^3)  = 55327,",
				"a(9881)  = 3233232,",
				"a(10^4)  = 3235757,",
				"a(10922) = 3333333,",
				"a(10^5)  = 227233257."
			],
			"mathematica": [
				"Table[FromDigits /@ Tuples[{2, 3, 5, 7}, n], {n, 3}] // Flatten (* _Michael De Vlieger_, Sep 19 2016 *)"
			],
			"program": [
				"(PARI) is_A046034(n)=Set(isprime(digits(n)))==[1] \\\\ _M. F. Hasler_, Oct 12 2013",
				"(Haskell)",
				"a046034 n = a046034_list !! (n-1)",
				"a046034_list = filter (all (`elem` \"2357\") . show ) [0..]",
				"-- _Reinhard Zumkeller_, Jul 19 2011",
				"(MAGMA) [n: n in [2..532] | Set(Intseq(n)) subset [2, 3, 5, 7]];  // _Bruno Berselli_, Jul 19 2011"
			],
			"xref": [
				"Cf. A046035, A118950, A019546 (primes), A203263, A035232, A039996, A085823, A052382, A084544, A084984, A017042, A001743, A001744, A014261, A014263, A193238, A202267, A202268, A211681."
			],
			"keyword": "nonn,base,easy",
			"offset": "1,1",
			"author": "_Eric W. Weisstein_",
			"ext": [
				"More terms from _Cino Hilliard_, Aug 06 2006",
				"Typo in second formula corrected by _Hieronymus Fischer_, May 12 2012",
				"Two typos in example section corrected by _Hieronymus Fischer_, May 30 2012"
			],
			"references": 96,
			"revision": 74,
			"time": "2020-04-24T08:11:22-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}