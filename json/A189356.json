{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A189356",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 189356,
			"data": "3,41,571,7953,110771,1542841,21489003,299303201,4168755811,58063278153,808717138331,11263976658481,156886956080403,2185153408467161,30435260762459851,423908497265970753,5904283700961130691,82236063316189858921,1145400602725696894203",
			"name": "a(n) gives y-values solving the Diophantine equation 2*x^2 + (x-1)^2 = y^2 for positive x.",
			"comment": [
				"(a(n)-1)/2 gives indices of triangular numbers which are also pentagonal (A046175)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A189356/b189356.txt\"\u003eTable of n, a(n) for n = 1..200\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (14,-1)."
			],
			"formula": [
				"a(n) = 14*a(n-1) - a(n-2).",
				"G.f.: x*(3-x)/(1-14*x+x^2). - _Bruno Berselli_, May 03 2011"
			],
			"mathematica": [
				"LinearRecurrence[{14,-1}, {3, 41}, 19]  (* _Bruno Berselli_, Nov 11 2011 *)"
			],
			"program": [
				"(MAGMA) [n le 2 select 38*n-35 else 14*Self(n-1)-Self(n-2): n in [1..19]]; // _Bruno Berselli_, May 03 2011"
			],
			"xref": [
				"Cf. A081065, A046174, A046175."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Sture Sjöstedt_, May 02 2011",
			"ext": [
				"Extended by _T. D. Noe_, May 02 2011"
			],
			"references": 2,
			"revision": 33,
			"time": "2019-09-01T08:43:22-04:00",
			"created": "2011-05-03T10:04:51-04:00"
		}
	]
}