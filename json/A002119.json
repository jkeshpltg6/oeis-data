{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002119",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2119,
			"id": "M4444 N1880",
			"data": "1,-1,7,-71,1001,-18089,398959,-10391023,312129649,-10622799089,403978495031,-16977719590391,781379079653017,-39085931702241241,2111421691000680031,-122501544009741683039,7597207150294985028449,-501538173463478753560673",
			"name": "Bessel polynomial y_n(-2).",
			"comment": [
				"Absolute values give denominators of successive convergents to e using continued fraction 1+2/(1+1/(6+1/(10+1/(14+1/(18+1/(22+1/26...)))))).",
				"Absolute values give number of different arrangements of nonnegative integers on a set of n 6-sided dice such that the dice can add to any integer from 0 to 6^n-1. For example when n=2, there are 7 arrangements that can result in any total from 0 to 35. Cf. A273013. The number of sides on the dice only needs to be the product of two distinct primes, of which 6 is the first example. - _Elliott Line_, Jun 10 2016"
			],
			"reference": [
				"L. Euler, 1737.",
				"J. W. L. Glaisher, Reports of British Assoc. Adv. Sci., 1871, pp. 16-18.",
				"J. Riordan, Combinatorial Identities, Wiley, 1968, p. 77.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A002119/b002119.txt\"\u003eTable of n, a(n) for n = 0..100\u003c/a\u003e",
				"Leo Chao, Paul DesJarlais and John L Leonard, \u003ca href=\"http://www.jstor.org/stable/3621238\"\u003eA binomial identity, via derangements\u003c/a\u003e, Math. Gaz. 89 (2005), 268-270.",
				"D. H. Lehmer, \u003ca href=\"http://www.jstor.org/stable/1968107\"\u003eArithmetical periodicities of Bessel functions\u003c/a\u003e, Annals of Mathematics, 33 (1932): 143-150. The sequence but with all signs positive is on page 149.",
				"D. H. Lehmer, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-46-99625-1\"\u003eReview of various tables by P. Pederson\u003c/a\u003e, Math. Comp., 2 (1946), 68-69.",
				"\u003ca href=\"/index/Be#Bessel\"\u003eIndex entries for sequences related to Bessel functions or polynomials\u003c/a\u003e"
			],
			"formula": [
				"D-finite with recurrence a(n) = -2(2n-1)*a(n-1) + a(n-2). - _T. D. Noe_, Oct 26 2006",
				"If y = x + Sum_{k\u003e=2} A005363(k)*x^k/k!, then y = x + Sum{k\u003e=2} a(k-2)(-y)^k/k!. - _Michael Somos_, Apr 02 2007",
				"a(-n-1) = a(n). - _Michael Somos_, Apr 02 2007",
				"a(n) = (1/n!)*Integral_{x\u003e=-1} (-x*(1+x))^n*exp(-(1+x)). - _Paul Barry_, Apr 19 2010",
				"G.f.: 1/Q(0), where Q(k)= 1 - x + 2*x*(k+1)/Q(k+1); (continued fraction). - _Sergei N. Gladkovskii_, May 17 2013",
				"Expansion of exp(x) in powers of y = x*(1 + x): exp(x) = 1 + y - y^2/2! + 7*y^3/3! - 71*y^4/4! + 1001*y^5/5! - .... E.g.f.: (1/sqrt(4*x + 1))*exp(sqrt(4*x + 1)/2 - 1/2) = 1 - x + 7*x^2/2! - 71*x^3/3! + .... - _Peter Bala_, Dec 15 2013",
				"a(n) = hypergeom([-n, n+1], [], 1). - _Peter Luschny_, Oct 17 2014",
				"a(n) = sqrt(Pi/exp(1)) * BesselI(1/2+n, 1/2) + (-1)^n * BesselK(1/2+n, 1/2) / sqrt(exp(1)*Pi). - _Vaclav Kotesovec_, Jul 22 2015",
				"a(n) ~ (-1)^n * 2^(2*n+1/2) * n^n / exp(n+1/2). - _Vaclav Kotesovec_, Jul 22 2015",
				"From _G. C. Greubel_, Aug 16 2017: (Start)",
				"G.f.: (1/(1-t))*hypergeometric2f0(1, 1/2; -; -4*t/(1-t)^2).",
				"E.g.f.: (1+4*t)^(-1/2) * exp((sqrt(1+4*t) - 1)/2). (End)",
				"a(n) = Sum_{k=0..n} (-1)^k*binomial(n,k)*binomial(n+k,k)*k!. - _Ilya Gutkovskiy_, Nov 24 2017"
			],
			"maple": [
				"f:=proc(n) option remember; if n \u003c= 1 then 1 else f(n-2)+(4*n-2)*f(n-1); fi; end;",
				"[seq(f(n), n=0..20)]; # This is for the unsigned version. - _N. J. A. Sloane_, May 09 2016"
			],
			"mathematica": [
				"Table[(-1)^k (2k)! Hypergeometric1F1[-k, -2k, -1]/k!, {k, 0, 10}] (* _Vladimir Reshetnikov_, Feb 16 2011 *)",
				"nxt[{n_,a_,b_}]:={n+1,b,a-2b(2n+1)}; NestList[nxt,{1,1,-1},20][[All,2]] (* _Harvey P. Dale_, Aug 18 2017 *)"
			],
			"program": [
				"(PARI) {a(n)= if(n\u003c0, n=-n-1); sum(k=0, n, (2*n-k)!/ (k!*(n-k)!)* (-1)^(n-k) )} /* _Michael Somos_, Apr 02 2007 */",
				"(PARI) {a(n)= local(A); if(n\u003c0, n= -n-1); A= sqrt(1 +4*x +x*O(x^n)); n!*polcoeff( exp((A-1)/2)/A, n)} /* _Michael Somos_, Apr 02 2007 */",
				"(PARI) {a(n)= local(A); if(n\u003c0, n= -n-1); n+=2 ; for(k= 1, n, A+= x*O(x^k); A= truncate( (1+x)* exp(A) -1-A) ); A+= x*O(x^n); A-= A^2; -(-1)^n*n!* polcoeff( serreverse(A), n)} /* _Michael Somos_, Apr 02 2007 */",
				"(Sage)",
				"A002119 = lambda n: hypergeometric([-n, n+1], [], 1)",
				"[simplify(A002119(n)) for n in (0..17)] # _Peter Luschny_, Oct 17 2014"
			],
			"xref": [
				"Cf. A001517, A053556, A053557, A001514, A065920, A065921, A065922, A065707, A000806, A006199, A065923.",
				"See also A033815.",
				"Polynomial coefficients are in A001498."
			],
			"keyword": "sign,easy,nice",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Vladeta Jovovic_, Apr 03 2000"
			],
			"references": 16,
			"revision": 75,
			"time": "2020-02-20T03:16:39-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}