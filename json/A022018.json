{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A022018",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 22018,
			"data": "2,16,129,1040,8385,67604,545057,4394520,35430801,285660700,2303138321,18569044064,149712848033,1207059275044,9731910872129,78463494859944,632611632651505,5100428912583468,41122188953879473,331547494013013232,2673100425407651457",
			"name": "Define the sequence UD(a(0),a(1)) by a(n) is the least integer such that a(n)/a(n-1) \u003e a(n-1)/a(n-2)+1 for even n \u003e= 2 and such that a(n)/a(n-1) \u003e a(n-1)/a(n-2) for odd n\u003e=2. This is UD(2,16).",
			"comment": [
				"The definition uses a recurrence of Shallit's S(a0,a1) sequences if n is even and Pisot T(a0,a1) sequences if n is odd. The UD notation reflects that we are rounding up or down depending on the position in the sequence. - _David Boyd_, Feb 12 2016"
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A022018/b022018.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"D. W. Boyd, \u003ca href=\"https://www.researchgate.net/publication/258834801\"\u003eLinear recurrence relations for some generalized Pisot sequences\u003c/a\u003e, Adv. Numb. Theory, Oxford Univ. Press (1991) 333-340",
				"D. W. Boyd, \u003ca href=\"https://www.researchgate.net/profile/David_Boyd7/publication/262181133\"\u003eLinear recurrence relations for some generalized Pisot sequences\u003c/a\u003e, (1996)",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (8,1,-4)."
			],
			"formula": [
				"Empirical g.f: (2-x^2)/(1-8*x-x^2+4*x^3), holds at least up to n\u003c=50000. - _Robert Israel_, Feb 10 2016",
				"The empirical g.f. found by _Robert Israel_ has been proved. One needs only the definition and the first 6 terms of the sequence. The denominator of the g.f. is the reciprocal of a Pisot polynomial with 2nd largest root real and negative. - _David Boyd_, Mar 06 2016",
				"a(n) = 8*a(n-1)+a(n-2)-4*a(n-3) for n\u003e2. - _Colin Barker_, Aug 09 2016"
			],
			"maple": [
				"UD := proc(a0,a1,n)",
				"    option remember;",
				"    if n = 0 then",
				"        a0 ;",
				"    elif n = 1 then",
				"        a1;",
				"    elif type(n,'even') then",
				"        floor( procname(a0,a1,n-1)^2/procname(a0,a1,n-2)+1) ;",
				"    else",
				"        floor( procname(a0,a1,n-1)^2/procname(a0,a1,n-2)) ;",
				"    end if;",
				"end proc:",
				"A022018 := proc(n)",
				"    UD(2,16,n) ;",
				"end proc: # _R. J. Mathar_, Feb 12 2016"
			],
			"mathematica": [
				"LinearRecurrence[{8, 1, -4}, {2, 16, 129}, 30] (* _Jean-François Alcover_, Dec 12 2016 *)"
			],
			"program": [
				"(PARI) a=[2,16,129]; c=Colrev([8,1,-4]); for(n=2,20,a=concat(a,a[-3..-1]*c));a \\\\ Reproduces the data. - _M. F. Hasler_, Feb 10 2016",
				"(MAGMA) Iv:=[2,16]; [n le 2 select Iv[n] else Floor(Self(n-1)^2/Self(n-2))+(1-(-1)^n)/2: n in [1..20]]; // _Bruno Berselli_, Feb 11 2016"
			],
			"keyword": "nonn",
			"offset": "0,1",
			"author": "_R. K. Guy_",
			"ext": [
				"Definition clarified based on consultance with _David Boyd_ by _Robert Israel_, Feb 12 2016"
			],
			"references": 12,
			"revision": 52,
			"time": "2016-12-12T04:53:01-05:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}