{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A019444",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 19444,
			"data": "1,3,2,6,8,4,11,5,14,16,7,19,21,9,24,10,27,29,12,32,13,35,37,15,40,42,17,45,18,48,50,20,53,55,22,58,23,61,63,25,66,26,69,71,28,74,76,30,79,31,82,84,33,87,34,90,92,36,95,97,38,100,39,103,105,41,108,110,43,113",
			"name": "a_1, a_2, ..., is a permutation of the positive integers such that the average of each initial segment is an integer, using the greedy algorithm to define a_n.",
			"comment": [
				"Self-inverse when considered as a permutation or function, i.e., a(a(n)) = n. - _Howard A. Landman_, Sep 25 2001",
				"That each initial segment has an integer average is trivially equivalent to the sum of the first n elements always being divisible by n. - _Franklin T. Adams-Watters_, Jul 07 2014",
				"Also, a lexicographically minimal sequence of distinct positive integers such that all values of a(n)-n are also distinct. - _Ivan Neretin_, Apr 18 2015"
			],
			"reference": [
				"Muharem Avdispahić and Faruk Zejnulahi, An integer sequence with a divisibility property, Fibonacci Quarterly, Vol. 58:4 (2020), 321-333."
			],
			"link": [
				"Franklin T. Adams-Watters, \u003ca href=\"/A019444/b019444.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"A. Shapovalov, \u003ca href=\"http://kvant.mccme.ru/1995/05/zadachnik_kvanta_matematika.htm\"\u003eProblem M1517\u003c/a\u003e (in Russian), Kvant 5 (1995), 20-21. English translation appeared in \u003ca href=\"http://static.nsta.org/pdfs/QuantumV7N1.pdf\"\u003eQuantum problem M185\u003c/a\u003e, Sept/October 1996 (beware, file is 75Mb).",
				"The Math Forum, \u003ca href=\"http://mathforum.org/wagon/fall96/p818.html\"\u003eProblem of the Week 818\u003c/a\u003e",
				"B. J. Venkatachala, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL12/Venkatachala/venkatachala2.html\"\u003eA curious bijection on natural numbers\u003c/a\u003e, JIS 12 (2009) 09.8.1.",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A002251(n-1) + 1. (Corrected by _M. F. Hasler_, Sep 17 2014)",
				"Let s(n) = (1/n)*Sum_{k=1..n} a(k) = A019446(n). Then if s(n-1) does not occur in a(1),...,a(n-1), a(n) = s(n) = s(n-1); otherwise, a(n) = s(n-1) + n and s(n) = s(n-1) + 1. - _Franklin T. Adams-Watters_, May 20 2010",
				"Lim_{n-\u003einfinity} max(n,a(n))/min(n,a(n)) = phi = A001622. - _Stanislav Sykora_, Jun 12 2017"
			],
			"maple": [
				"P:=proc(q) local a,b,i,n; a:=[1]; b:=1; for i from 2 to 70",
				"do for n from 1 to q do if numboccur(a,n)=0 then",
				"if frac((b+n)/i)=0 then a:=[op(a),n]; b:=b+n; break; fi; fi;",
				"od; od; op(a); end: P(10^9); # _Paolo P. Lava_, Jul 11 2019"
			],
			"mathematica": [
				"a[1]=1; a[n_] := a[n]=Module[{s, v}, s=a/@Range[n-1]; For[v=Mod[ -Plus@@s, n], v\u003c1||MemberQ[s, v], v+=n, Null]; v]",
				"lst = {1}; f[s_List] := Block[{k = 1, len = 1 + Length@ lst, t = Plus @@ lst}, While[ MemberQ[s, k] || Mod[k + t, len] != 0, k++ ]; AppendTo[lst, k]]; Nest[f, lst, 69] (* _Robert G. Wilson v_, May 17 2010 *)",
				"Fold[Append[#1, #2 Ceiling[#2/GoldenRatio] - Total[#1]] \u0026, {1}, Range[2, 70]] (* _Birkas Gyorgy_, May 25 2012 *)"
			],
			"program": [
				"(PARI) al(n)=local(v,s,fnd);v=vector(n);v[1]=s=1;for(k=2,n,fnd=0;for(i=1,k-1,if(v[i]==s,fnd=1;break));v[k]=if(fnd,s+k,s);s+=fnd);v \\\\ _Franklin T. Adams-Watters_, May 20 2010",
				"(PARI) A019444_upto(N, c=0, A=Vec(1, N))={for(n=2, N, A[n]||(#A\u003cA[n]=n+c++)|| A[n+c]=n); A} \\\\ _M. F. Hasler_, Nov 27 2019"
			],
			"xref": [
				"Cf. A019445, A019446, A243700, A001622."
			],
			"keyword": "nonn,nice",
			"offset": "1,2",
			"author": "_R. K. Guy_ and Tom Halverson (halverson(AT)macalester.edu)",
			"references": 15,
			"revision": 79,
			"time": "2021-12-22T02:18:17-05:00",
			"created": "1996-12-11T03:00:00-05:00"
		}
	]
}