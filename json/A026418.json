{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A026418",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 26418,
			"data": "1,0,1,1,2,3,6,11,22,43,87,176,362,748,1560,3270,6897,14613,31104,66459,142518,306603,661572,1431363,3104619,6749390,14704387,32098729,70198656,153785705,337443785,741551614,1631910081,3596083215",
			"name": "Number of ordered trees with n edges and having no branches of length 1.",
			"comment": [
				"Hankel transform is A166446(n+2). - _Paul Barry_, Mar 29 2011"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A026418/b026418.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Jean-Luc Baril and Sergey Kirgizov, \u003ca href=\"https://arxiv.org/abs/2110.02831\"\u003eLattice paths with a first return decomposition constrained by the maximal height of a pattern\u003c/a\u003e, arXiv:2110.02831 [math.CO], 2021.",
				"Jean-Luc Baril, Sergey Kirgizov and Armen Petrossian, \u003ca href=\"http://math.colgate.edu/~integers/t46/t46.Abstract.html\"\u003eMotzkin paths with a restricted first return decomposition\u003c/a\u003e, Integers (2019) Vol. 19, A46.",
				"Paul Barry, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL12/Barry3/barry93.html\"\u003eContinued fractions and transformations of integer sequences\u003c/a\u003e, JIS 12 (2009) 09.7.6.",
				"J. Riordan, \u003ca href=\"http://dx.doi.org/10.1016/S0097-3165(75)80010-0\"\u003eEnumeration of plane trees by branches and endpoints\u003c/a\u003e, J. Comb. Theory (A) 19, 1975, 214-222."
			],
			"formula": [
				"a(n) = sum(binomial(n-2-j, j)*m{j), j=0..floor(n/2)-1), where m(j)=sum(binomial(n, 2k)*binomial(2k, k)/(k+1), k=0..floor(n/2)) is the Motzkin number A001006(j).",
				"G.f.: g=g(z) satisfies z^2g^2-(1-z+z^2)g+1-z+z^2=0",
				"G.f.: [1,1,2,3,...] has g.f. 1/(1-x-x^2-x^4/(1-x-x^2-x^4/(1-x-x^2-x^4/(1... (continued fraction). - _Paul Barry_, Jul 16 2009",
				"G.f.: c(x^2/(1-x+x^2)) where c(x) is the g.f. of A000108.",
				"G.f.: g(x)=1/(1-x^2/(1-x+x^2-x^2*g(x)))=1/(1-x^2/(1-x+x^2-x^2/(1-x^2/(1-x+x^2-x^2/(1-... (continued fraction). - _Paul Barry_, Mar 29 2011",
				"Conjecture: (n+2)*a(n) +(-2*n-1)*a(n-1) +(-n+1)*a(n-2) +(2*n-5)*a(n-3) +3*(-n+4)*a(n-4)=0. - _R. J. Mathar_, Nov 24 2012",
				"G.f.: (1 - x + x^2 - sqrt(1- 2*x - x^2 + 2*x^3 - 3*x^4))/(2*x^2). - _Sergei N. Gladkovskii_, Oct 04 2013",
				"a(n) ~ sqrt(26+2*sqrt(13)) * ((1+sqrt(13))/2)^n / (4 * sqrt(Pi) * n^(3/2)). - _Vaclav Kotesovec_, Feb 12 2014"
			],
			"example": [
				"a(6)=6 because we have the following six ordered trees with 6 edges and no branches of length 1 (hanging from the root): (i) a path of length 6, (ii) a path of length 2 and a path of length 4, (iii) two paths of length 3, (iv) a path of length 4 and a path of length 2, (v) three paths of length 2 and (vi) a path of length 2 at the end of which two paths of length 2 are hanging."
			],
			"mathematica": [
				"CoefficientList[Series[(1-x+x^2-Sqrt[1-2*x-x^2+2*x^3-3*x^4])/(2*x^2), {x, 0, 20}], x] (* _Vaclav Kotesovec_, Feb 12 2014 *)"
			],
			"xref": [
				"Cf. A001006."
			],
			"keyword": "nonn",
			"offset": "0,5",
			"author": "_Emeric Deutsch_, Dec 04 2003",
			"references": 3,
			"revision": 36,
			"time": "2021-10-07T02:04:54-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}