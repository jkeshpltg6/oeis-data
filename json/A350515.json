{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A350515",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 350515,
			"data": "0,0,1,5,1,8,3,2,4,14,3,17,6,4,7,23,5,26,9,6,10,32,7,35,12,8,13,41,9,44,15,10,16,50,11,53,18,12,19,59,13,62,21,14,22,68,15,71,24,16,25,77,17,80,27,18,28,86,19,89,30,20,31,95,21,98,33,22,34,104",
			"name": "a(n) = (n-1)/3 if n mod 3 = 1; a(n) = n/2 if n mod 6 = 0 or n mod 6 = 2; a(n) = (3n+1)/2 if n mod 6 = 3 or n mod 6 = 5.",
			"comment": [
				"This is a variant of the Farkas map (A349407).",
				"Yolcu, Aaronson and Heule prove that the trajectory of the iterates of the map starting from any nonnegative integer always reaches 0.",
				"If displayed as a rectangular array with six columns, the columns are A008585, A005843, A016777, A017221, A005408, A017257 (see example). - _Omar E. Pol_, Jan 02 2022"
			],
			"link": [
				"H. M. Farkas, \"Variants of the 3N+1 Conjecture and Multiplicative Semigroups\", in Entov, Pinchover and Sageev, \u003ca href=\"https://bookstore.ams.org/conm-387\"\u003eGeometry, Spectral Theory, Groups, and Dynamics, Contemporary Mathematics, vol. 387\u003c/a\u003e, American Mathematical Society, 2005, p. 121.",
				"Emre Yolcu, Scott Aaronson and Marijn J. H. Heule, \u003ca href=\"https://arxiv.org/abs/2105.14697\"\u003eAn Automated Approach to the Collatz Conjecture\u003c/a\u003e, arXiv:2105.14697 [cs.LO], 2021, pp. 21-25.",
				"\u003ca href=\"/index/Rec#order_12\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,0,0,0,0,2,0,0,0,0,0,-1)."
			],
			"formula": [
				"a(n) = (A349407(n+1)-1)/2.",
				"a(n) = 2*a(n-6)-a(n-12). - _Wesley Ivan Hurt_, Jan 03 2022"
			],
			"example": [
				"From _Omar E. Pol_, Jan 02 2022: (Start)",
				"Written as a rectangular array with six columns read by rows the sequence begins:",
				"   0,  0,  1,  5,  1,  8;",
				"   3,  2,  4, 14,  3, 17;",
				"   6,  4,  7, 23,  5, 26;",
				"   9,  6, 10, 32,  7, 35;",
				"  12,  8, 13, 41,  9, 44;",
				"  15, 10, 16, 50, 11, 53;",
				"  18, 12, 19, 59, 13, 62;",
				"  21, 14, 22, 68, 15, 71;",
				"  24, 16, 25, 77, 17, 80;",
				"  27, 18, 28, 86, 19, 89;",
				"  30, 20, 31, 95, 21, 98;",
				"...",
				"(End)"
			],
			"mathematica": [
				"nterms=100;Table[If[Mod[n,3]==1,(n-1)/3,If[Mod[n,6]==0||Mod[n,6]==2,n/2,(3n+1)/2]],{n,0,nterms-1}]",
				"(* Second program *)",
				"nterms=100;LinearRecurrence[{0,0,0,0,0,2,0,0,0,0,0,-1},{0,0,1,5,1,8,3,2,4,14,3,17},nterms]"
			],
			"program": [
				"(Python)",
				"def a(n):",
				"    r = n%6",
				"    if r == 1 or r == 4: return (n-1)//3",
				"    if r == 0 or r == 2: return n//2",
				"    if r == 3 or r == 5: return (3*n+1)//2",
				"print([a(n) for n in range(70)]) # _Michael S. Branicky_, Jan 02 2022"
			],
			"xref": [
				"Cf. A005408, A005843, A006370, A014682, A016777, A017221, A017257, A349407, A350279."
			],
			"keyword": "nonn,easy,new",
			"offset": "0,4",
			"author": "_Paolo Xausa_, Jan 02 2022",
			"references": 0,
			"revision": 18,
			"time": "2022-01-04T05:25:48-05:00",
			"created": "2022-01-04T05:25:48-05:00"
		}
	]
}