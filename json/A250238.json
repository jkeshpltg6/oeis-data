{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A250238",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 250238,
			"data": "469,1489,1708,1937,2557,2941,2981,3021,3173,3305,3580,3592,3736",
			"name": "Fundamental discriminants d uniquely characterizing all complex biquadratic fields Q(sqrt(-3),sqrt(d)) which have 3-class group of type (3,3) and second 3-class group isomorphic to SmallGroup(81,9).",
			"comment": [
				"For the discriminants d in A250238, the 3-class field tower of K=Q(sqrt(-3),sqrt(d)) has exactly two stages and the second 3-class group G of K is given by the metabelian 3-group G=SmallGroup(81,9) with transfer kernel type a.1, (0,0,0,0), transfer target type [(3,9),(3,3)^3] and coclass 1. Actually, this is the ground state on the coclass-1 graph.",
				"The reason the 3-class field tower of K must stop at the second Hilbert 3-class field is Blackburn's Theorem on two-generated 3-groups G whose commutator subgroup G' also has two generators. In fact, the group G=SmallGroup(81,9) has two-generated commutator subgroup G' of type (3,3)."
			],
			"reference": [
				"H. U. Besche, B. Eick, and E. A. O'Brien, The SmallGroups Library - a Library of Groups of Small Order, 2005, an accepted and refereed GAP 4 package, available also in MAGMA."
			],
			"link": [
				"N. Blackburn, \u003ca href=\"http://dx.doi.org/10.1017/S0305004100031959\"\u003eOn prime-power groups in which the derived group has two generators\u003c/a\u003e, Proc. Camb. Phil. Soc. 53 (1957), 19-27.",
				"D. C. Mayer, \u003ca href=\"https://doi.org/10.1142/S179304211250025X\"\u003eThe second p-class group of a number field\u003c/a\u003e, Int. J. Number Theory 8 (2) (2012), 471-505.",
				"D. C. Mayer, \u003ca href=\"http://arxiv.org/abs/1403.3899\"\u003eThe second p-class group of a number field\u003c/a\u003e. Preprint: arXiv:1403.3899v1 [math.NT], 2014.",
				"D. C. Mayer, \u003ca href=\"http://arxiv.org/abs/1403.3839\"\u003ePrincipalization algorithm via class group structure\u003c/a\u003e, Preprint: arXiv:1403.3839v1 [math.NT], 2014; J. Théor. Nombres Bordeaux 26 (2014), no. 2, 415-464."
			],
			"program": [
				"(MAGMA)SetClassGroupBounds(\"GRH\"); for n := 469 to 10000 do cnd := false; if (1 eq n mod 4) and IsSquarefree(n) then cnd := true; end if; if (0 eq n mod 4) then r := n div 4; if IsSquarefree(r) and ((2 eq r mod 4) or (3 eq r mod 4)) then cnd := true; end if; end if; if (true eq cnd) then R := QuadraticField(n); E := QuadraticField(-3); K := Compositum(R, E); C, mC := ClassGroup(K); if ([3, 3] eq pPrimaryInvariants(C, 3)) then s := Subgroups(C: Quot := [3]); a := [AbelianExtension(Inverse(mq)*mC) where _, mq := quo\u003cC|x`subgroup\u003e : x in s]; b := [NumberField(x) : x in a]; d := [MaximalOrder(x) : x in a]; b := [AbsoluteField(x) : x in b]; c := [MaximalOrder(x) : x in b]; c := [OptimizedRepresentation(x) : x in b]; b := [NumberField(DefiningPolynomial(x)) : x in c]; a := [Simplify(LLL(MaximalOrder(x))) : x in b]; if IsNormal(b[2]) then H := Compositum(NumberField(a[1]), NumberField(a[2])); else H := Compositum(NumberField(a[1]), NumberField(a[3])); end if; O := MaximalOrder(H); CH := ClassGroup(LLL(O)); if ([3, 3] eq pPrimaryInvariants(CH, 3)) then n, \", \"; end if; end if; end if; end for;"
			],
			"xref": [
				"A006832, A250235, A250236 are supersequences, A250237, A250239, A250240, A250241, A250242 are disjoint sequences."
			],
			"keyword": "nonn,hard,more",
			"offset": "1,1",
			"author": "_Daniel Constantin Mayer_, Nov 16 2014",
			"references": 7,
			"revision": 16,
			"time": "2019-11-23T04:02:48-05:00",
			"created": "2014-11-16T12:12:16-05:00"
		}
	]
}