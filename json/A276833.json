{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A276833",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 276833,
			"data": "1,0,-1,0,-3,0,-5,0,-1,0,-9,0,-11,0,3,0,-15,0,-17,0,5,0,-21,0,-3,0,-1,0,-27,0,-29,0,9,0,15,0,-35,0,11,0,-39,0,-41,0,3,0,-45,0,-5,0,15,0,-51,0,27,0,17,0,-57,0,-59,0,5,0,33,0,-65,0,21,0,-69,0,-71,0,3,0,45,0,-77,0,-1,0,-81,0,45,0,27,0,-87,0,55,0,29,0,51,0,-95,0,9",
			"name": "Sum of mu(d)*phi(d) over divisors d of n.",
			"comment": [
				"Discovered when incorrectly applying Mobius inversion formula.",
				"a(n)*a(m) = a(n*m) if gcd(n,m)=1 (has a simple proof).",
				"Strongly multiplicative: a(p^e) = 2 - p. - _Charles R Greathouse IV_, Oct 01 2019"
			],
			"link": [
				"Indranil Ghosh, \u003ca href=\"/A276833/b276833.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = Sum_{d|n} mu(d)*phi(d).",
				"G.f.: Sum_{k\u003e=1} mu(k)*phi(k)*x^k/(1 - x^k). - _Ilya Gutkovskiy_, Nov 06 2018",
				"a(n) = Product_{p prime and p|n} (2-p). - _Robert FERREOL_, Mar 14 2020",
				"Dirichlet g.f.: zeta(s) * Product_{primes p} (1 - p^(1-s) + p^(-s)). - _Vaclav Kotesovec_, Jun 14 2020"
			],
			"example": [
				"mu(d)*phi(d) = 1*1,-1*1,-1*2, 1*2 for d=1,2,3,6, so a(6) = 1*1-1*1-1*2+1*2 = 0."
			],
			"maple": [
				"with(numtheory):seq(convert(map(x-\u003e2-x,factorset(n)),`*`),n=1..99); # _Robert FERREOL_, Mar 14 2020"
			],
			"mathematica": [
				"Table[Sum[MoebiusMu[d] EulerPhi[d], {d, Divisors[n]}], {n, 99}] (* _Indranil Ghosh_, Mar 10 2017 *)",
				"a[1] = 1; a[n_] := Times @@ ((2 - First[#])\u0026 /@ FactorInteger[n]); Array[a, 100] (* _Amiram Eldar_, Sep 21 2020 *)"
			],
			"program": [
				"(PARI) r=0;fordiv(n,d,r+=moebius(d)*eulerphi(d));r",
				"(PARI) a(n) = sumdiv(n, d, moebius(d)*eulerphi(d)); \\\\ _Michel Marcus_, Sep 30 2016",
				"(PARI) a(n)=my(f=factor(n)[,1]); prod(i=1,#f, 2-f[i]) \\\\ _Charles R Greathouse IV_, Oct 01 2019",
				"(PARI) for(n=1, 100, print1(direuler(p=2, n, (1 - p*X + X)/(1 - X))[n], \", \")) \\\\ _Vaclav Kotesovec_, Jun 14 2020"
			],
			"xref": [
				"For squarefree numbers, the absolute value is equal to A166586 (first exception at 25).",
				"Cf. A097945."
			],
			"keyword": "mult,sign,easy",
			"offset": "1,5",
			"author": "_Jurjen N.E. Bos_, Sep 20 2016",
			"references": 3,
			"revision": 33,
			"time": "2020-09-21T03:33:13-04:00",
			"created": "2016-09-30T13:08:12-04:00"
		}
	]
}