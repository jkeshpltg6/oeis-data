{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A154536",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 154536,
			"data": "3,4,5,6,7,9,11,12,14,15,16,22,25,26,29,31,33,36,39,53,59,60,63,70,72,74,80,87,94,128,141,142,145,152,169,171,173,179,193,210,227,309,339,340,343,350,367,408,410,412",
			"name": "Positive integers that can be written as the sum of a positive Pell number and twice a positive Pell number.",
			"comment": [
				"On Jan 10 2009, _Zhi-Wei Sun_ conjectured that any integer greater than 5 can be expressed as the sum of an odd prime and a term in the above sequence; in other words, each n=6,7,... can be written in the form p+P_s+2*P_t with p an odd prime and s,t\u003e0. This has been verified up to 5*10^13 by _D. S. McNeil_ (from London Univ.). Motivated by this conjecture, Qing-Hu Hou (from Nankai Univ.) observed and _Zhi-Wei Sun_ proved that each term a(n) in the above sequence can be uniquely written in the form P_s+2P_t with s,t\u003e0. Sun noted that 2176 cannot be written as the sum of a prime and two Pell numbers; _D. S. McNeil_ found that 393185153350 cannot be written in the form p+P_s+3P_t and 872377759846 cannot be written in the form p+P_s+4P_t, where p is a prime and s and t are nonnegative.",
				"_Zhi-Wei Sun_ has offered a monetary reward for settling this conjecture."
			],
			"reference": [
				"R. Crocker, On a sum of a prime and two powers of two, Pacific J. Math. 36(1971), 103-107."
			],
			"link": [
				"Zhi-Wei SUN, \u003ca href=\"/A154536/b154536.txt\"\u003eTable of n, a(n), n=1..179.\u003c/a\u003e",
				"D. S. McNeil, \u003ca href=\"https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;bda1acb4.0812\"\u003eSun's strong conjecture\u003c/a\u003e",
				"D. S. McNeil, \u003ca href=\"https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;ab1b9553.0901\"\u003eVarious and sundry: a report on Sun's conjectures\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;a9e706e.0812\"\u003eA summary concerning my conjecture n=p+F_s+F_t\u003c/a\u003e",
				"Terence Tao, \u003ca href=\"http://arxiv.org/abs/0802.3361\"\u003eA remark on primality testing and decimal expansions\u003c/a\u003e, Journal of the Australian Mathematical Society 91:3 (2011), pp. 405-413.",
				"K. J. Wu and Z.-W. Sun, Covers of the integers with odd moduli and their applications to the forms x^m-2^n and x^2-F_{3n}/2, Math. Comp., in press. \u003ca href=\"http://arxiv.org/abs/math/0702382\"\u003earXiv:math.NT/0702382\u003c/a\u003e"
			],
			"example": [
				"For n=12 the a(12)=22 solution is 22 = P_4 + 2*P_3."
			],
			"mathematica": [
				"P[n_]:=P[n]=2*P[n-1]+P[n-2] P[0]=0 P[1]=1 i:=0 Do[Do[If[n==2*P[x]+P[y],i=i+1;Print[i,\" \",n]], {x,1,Max[1,Log[2,n]]},{y,1,Log[2,n]+1}]; Continue,{n,1,100000}]"
			],
			"xref": [
				"Cf. A000129, A000040, A154257, A154285, A154364, A154417, A154421, A156695."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Zhi-Wei Sun_, Jan 11 2009",
			"ext": [
				"Mentioned McNeil's verification record for the representation n = p + P_s + 2P_t and his examples for n not of the form p + P_s + 3P_t and n not of the form p + P_s + 4P_t. - _Zhi-Wei Sun_, Jan 17 2009",
				"_D. S. McNeil_ has verified the conjecture up to 5*10^13. - _Zhi-Wei Sun_, Jan 20 2009"
			],
			"references": 10,
			"revision": 17,
			"time": "2018-09-10T13:49:49-04:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}