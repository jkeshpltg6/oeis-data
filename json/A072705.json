{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A072705",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 72705,
			"data": "1,1,0,1,2,0,1,2,0,0,1,4,0,0,0,1,4,4,0,0,0,1,6,4,0,0,0,0,1,6,8,0,0,0,0,0,1,8,12,0,0,0,0,0,0,1,8,16,8,0,0,0,0,0,0,1,10,20,8,0,0,0,0,0,0,0,1,10,28,16,0,0,0,0,0,0,0,0,1,12,32,24,0,0,0,0,0,0,0,0,0,1,12,40,40,0,0,0,0,0,0,0,0,0,0",
			"name": "Triangle of number of unimodal compositions of n into exactly k distinct terms.",
			"comment": [
				"Also the number of compositions of n into exactly k distinct terms whose negation is unimodal. - _Gus Wiseman_, Mar 06 2020"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A072705/b072705.txt\"\u003eRows n = 1..141, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = 2^(k-1)*A060016(n,k) = T(n-k,k)+2*T(n-k,k-1) [starting with T(0,0)=0, T(0,1)=0 and T(n,1)=1 for n\u003e0]."
			],
			"example": [
				"Rows start: 1; 1,0; 1,2,0; 1,2,0,0; 1,4,0,0,0; 1,4,4,0,0,0; 1,6,4,0,0,0,0; 1,6,8,0,0,0,0,0; etc. T(6,3)=4 since 6 can be written as 1+2+3, 1+3+2, 2+3+1, or 3+2+1 but not 2+1+3 or 3+1+2.",
				"From _Gus Wiseman_, Mar 06 2020: (Start)",
				"Triangle begins:",
				"  1",
				"  1  0",
				"  1  2  0",
				"  1  2  0  0",
				"  1  4  0  0  0",
				"  1  4  4  0  0  0",
				"  1  6  4  0  0  0  0",
				"  1  6  8  0  0  0  0  0",
				"  1  8 12  0  0  0  0  0  0",
				"  1  8 16  8  0  0  0  0  0  0",
				"  1 10 20  8  0  0  0  0  0  0  0",
				"  1 10 28 16  0  0  0  0  0  0  0  0",
				"  1 12 32 24  0  0  0  0  0  0  0  0  0",
				"  1 12 40 40  0  0  0  0  0  0  0  0  0  0",
				"  1 14 48 48 16  0  0  0  0  0  0  0  0  0  0",
				"(End)"
			],
			"maple": [
				"b:= proc(n, i) option remember; `if`(n\u003ei*(i+1)/2, 0, `if`(n=0, 1,",
				"      expand(b(n, i-1) +`if`(i\u003en, 0, x*b(n-i, i-1)))))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i)*ceil(2^(i-1)), i=1..n))(b(n$2)):",
				"seq(T(n), n=1..14);  # _Alois P. Heinz_, Mar 26 2014"
			],
			"mathematica": [
				"b[n_, i_] := b[n, i] = If[n \u003e i*(i+1)/2, 0, If[n == 0, 1, Expand[b[n, i-1] + If[i \u003e n, 0, x*b[n-i, i-1]]]]]; T[n_] := Function[{p}, Table[Coefficient[p, x, i]* Ceiling[2^(i-1)], {i, 1, n}]][b[n, n]]; Table[T[n], {n, 1, 14}] // Flatten (* _Jean-François Alcover_, Feb 26 2015, after _Alois P. Heinz_ *)",
				"unimodQ[q_]:=Or[Length[q]\u003c=1,If[q[[1]]\u003c=q[[2]],unimodQ[Rest[q]],OrderedQ[Reverse[q]]]];",
				"Table[Length[Select[Join@@Permutations/@IntegerPartitions[n,{k}],UnsameQ@@#\u0026\u0026unimodQ[#]\u0026]],{n,12},{k,n}] (* _Gus Wiseman_, Mar 06 2020 *)"
			],
			"xref": [
				"Cf. A060016, A072574, A072704. Row sums are A072706.",
				"Column k = 2 is A052928.",
				"Unimodal compositions are A001523.",
				"Unimodal sequences covering an initial interval are A007052.",
				"Strict compositions are A032020.",
				"Non-unimodal strict compositions are A072707.",
				"Unimodal compositions covering an initial interval are A227038.",
				"Numbers whose prime signature is not unimodal are A332282.",
				"Cf. A059204, A107429, A115981, A329398, A332285, A332286, A332578, A332870, A332874."
			],
			"keyword": "nonn,tabl,look",
			"offset": "1,5",
			"author": "_Henry Bottomley_, Jul 04 2002",
			"references": 7,
			"revision": 19,
			"time": "2020-03-29T16:29:53-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}