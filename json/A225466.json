{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A225466",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 225466,
			"data": "1,2,3,4,21,9,8,117,135,27,16,609,1431,702,81,32,3093,13275,12015,3240,243,64,15561,115479,171990,81405,13851,729,128,77997,970515,2238327,1655640,479682,56133,2187,256,390369,7998111,27533142,29893941,13121514,2561706",
			"name": "Triangle read by rows, 3^k*S_3(n, k) where S_m(n, k) are the Stirling-Frobenius subset numbers of order m; n \u003e= 0, k \u003e= 0.",
			"comment": [
				"The definition of the Stirling-Frobenius subset numbers of order m is in A225468.",
				"From _Wolfdieter Lang_, Apr 09 2017: (Start)",
				"This is the Sheffer triangle (exp(2*x), exp(3*x) - 1), denoted by S2[3,2]. See also A282629 for S2[3,1]. The stirling2 triangle A048993 is in this notation denoted by S2[1,0].",
				"The a-sequence for this Sheffer triangle has e.g.f. 3*x/log(1+x) and is 3*A006232(n)/A006233(n) (Cauchy numbers of the first kind). For a- and z-sequences for Sheffer triangles see the W. Lang link under A006232, also with references).",
				"The z-sequence has e.g.f. (3/(log(1+x)))*(1 - 1/(1+x)^(2/3)) and gives 2*A284862/A284863.",
				"The first column k sequences divided by 3^k are A000079, A016127, A016297, A025999. For the e.g.f.s and o.g.f.s see below.",
				"The row sums give A284864. The alternating row sums give A284865.",
				"This triangle appears in the o.g.f. G(n, x) of the sequence {(2 + 3*m)^n}_{m\u003e=0}, as G(n, x) = Sum_{k=0..n} T(n, k)*k!*x^k/(1-x)^(k+1), n \u003e= 0. Hence the corresponding e.g.f. is, by the linear inverse Laplace transform, E(n, t) = Sum_{m \u003e=0} (2 + 3*m)^n t^m/m! = exp(t)*Sum_{k=0..n} T(n, k)*t^k.",
				"The corresponding Eulerian number triangle is A225117(n, k) = Sum_{m=0..k} (-1)^(k-m)*binomial(n-m, k-m)*T(n, m)*m!, 0 \u003c= k \u003c= n. (End)"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A225466/b225466.txt\"\u003eRows n = 0..50, flattened\u003c/a\u003e",
				"Peter Luschny, \u003ca href=\"http://www.luschny.de/math/euler/GeneralizedEulerianPolynomials.html\"\u003eEulerian polynomials.\u003c/a\u003e",
				"Peter Luschny, \u003ca href=\"http://www.luschny.de/math/euler/StirlingFrobeniusNumbers.html\"\u003eThe Stirling-Frobenius numbers.\u003c/a\u003e",
				"Shi-Mei Ma, Toufik Mansour, Matthias Schork, \u003ca href=\"http://arxiv.org/abs/1308.0169\"\u003eNormal ordering problem and the extensions of the Stirling grammar\u003c/a\u003e, Russian Journal of Mathematical Physics, 2014, 21(2), arXiv:1308.0169 [math.CO], 2013, p. 12."
			],
			"formula": [
				"T(n, k) = (1/k!)*Sum_{j=0..n} binomial(j, n-k)*A_3(n, j) where A_m(n, j) are the generalized Eulerian numbers A225117.",
				"For a recurrence see the Maple program.",
				"T(n, 0) ~ A000079; T(n, 1) ~ A005057; T(n, n) ~ A000244.",
				"From _Wolfdieter Lang_, Apr 09 2017: (Start)",
				"T(n, k) = Sum_{j=0..k} binomial(k,j)*(-1)^(j-k)*(2 + 3*j)^n/k!, 0 \u003c= k \u003c= n.",
				"E.g.f. of triangle: exp(2*z)*exp(x*(exp(3*z)-1)) (Sheffer type).",
				"E.g.f. for sequence of column k is exp(2*x)*((exp(3*x) - 1)^k)/k! (Sheffer property).",
				"O.g.f. for sequence of column k is 3^k*x^k/Product_{j=0..k} (1 - (2+3*j)*x).",
				"A nontrivial recurrence for the column m=0 entries T(n, 0) = 2^n from the z-sequence given above: T(n,0) = n*Sum_{k=0..n-1} z(k)*T(n-1,k), n \u003e= 1, T(0, 0) = 1.",
				"The corresponding recurrence for columns k \u003e= 1 from the a-sequence is T(n, k) = (n/k)* Sum_{j=0..n-k} binomial(k-1+j, k-1)*a(j)*T(n-1, k-1+j).",
				"Recurrence for row polynomials R(n, x) (Meixner type): R(n, x) = ((3*x+2) + 3*x*d_x)*R(n-1, x), with differentiation d_x, for n \u003e= 1, with input R(0, x) = 1.",
				"(End)",
				"Boas-Buck recurrence for column sequence m: T(n, k) = (1/(n - m))*[(n/2)*(4 + 3*m)*T(n-1, k) + m* Sum_{p=m..n-2} binomial(n, p)(-3)^(n-p)*Bernoulli(n-p)*T(p, k)], for n \u003e k \u003e= 0, with input T(k, k) = 3^k. See a comment and references in A282629, An example is given below. - _Wolfdieter Lang_, Aug 11 2017"
			],
			"example": [
				"[n\\k][ 0,     1,      2,       3,       4,      5,     6,    7]",
				"[0]    1,",
				"[1]    2,     3,",
				"[2]    4,    21,      9,",
				"[3]    8,   117,    135,      27,",
				"[4]   16,   609,   1431,     702,      81,",
				"[5]   32,  3093,  13275,   12015,    3240,    243,",
				"[6]   64, 15561, 115479,  171990,   81405,  13851,   729,",
				"[7]  128, 77997, 970515, 2238327, 1655640, 479682, 56133, 2187.",
				"...",
				"From _Wolfdieter Lang_, Aug 11 2017: (Start)",
				"Recurrence (see the Maple program): T(4, 2) = 3*T(3, 1) + (3*2+2)*T(3, 2) = 3*117 + 8*135 = 1431.",
				"Boas-Buck recurrence for column m = 2, and n = 4: T(4,2) = (1/2)*[2*(4 + 3*2)*T(3, 2) + 2*6*(-3)^2*Bernoulli(2)*T(2, 2))] = (1/2)*(20*135 + 12*9*(1/6)*9) = 1431. (End)"
			],
			"maple": [
				"SF_SS := proc(n, k, m) option remember;",
				"if n = 0 and k = 0 then return(1) fi;",
				"if k \u003e n or  k \u003c 0 then return(0) fi;",
				"m*SF_SS(n-1, k-1, m) + (m*(k+1)-1)*SF_SS(n-1, k, m) end:",
				"seq(print(seq(SF_SS(n, k, 3), k=0..n)), n=0..5);"
			],
			"mathematica": [
				"EulerianNumber[n_, k_, m_] := EulerianNumber[n, k, m] = (If[ n == 0, Return[If[k == 0, 1, 0]]]; Return[(m*(n-k)+m-1)*EulerianNumber[n-1, k-1, m] + (m*k+1)*EulerianNumber[n-1, k, m]]); SFSS[n_, k_, m_] := Sum[ EulerianNumber[n, j, m]*Binomial[j, n-k], {j, 0, n}]/k!; Table[ SFSS[n, k, 3], {n, 0, 8}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, May 29 2013, translated from Sage *)"
			],
			"program": [
				"(Sage)",
				"@CachedFunction",
				"def EulerianNumber(n, k, m) :",
				"    if n == 0: return 1 if k == 0 else 0",
				"    return (m*(n-k)+m-1)*EulerianNumber(n-1,k-1,m) + (m*k+1)*EulerianNumber(n-1,k,m)",
				"def SF_SS(n, k, m):",
				"    return add(EulerianNumber(n,j,m)*binomial(j,n-k) for j in (0..n))/ factorial(k)",
				"def A225466(n): return SF_SS(n, k, 3)",
				"(PARI) T(n, k) = sum(j=0, k, binomial(k, j)*(-1)^(j - k)*(2 + 3*j)^n/k!);",
				"for(n=0, 10, for(k=0, n, print1(T(n, k),\", \");); print();) \\\\ _Indranil Ghosh_, Apr 10 2017",
				"(Python)",
				"from sympy import binomial, factorial",
				"def T(n, k): return sum(binomial(k, j)*(-1)**(j - k)*(2 + 3*j)**n//factorial(k) for j in range(k + 1))",
				"for n in range(11): print([T(n, k) for k in range(n + 1)]) # _Indranil Ghosh_, Apr 10 2017"
			],
			"xref": [
				"Cf. A048993 (m=1), A154537 (m=2), A225467 (m=4), A225468.",
				"Cf. A000079, A000244, A005057, A016127, A016297, A025999, A006232/A006233, A225117, A225472, A225468, A282629, A284862/A284863, A284864, A284865."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,2",
			"author": "_Peter Luschny_, May 08 2013",
			"references": 11,
			"revision": 54,
			"time": "2020-04-09T16:38:18-04:00",
			"created": "2013-05-21T14:09:03-04:00"
		}
	]
}