{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003516",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3516,
			"id": "M4417",
			"data": "1,7,36,165,715,3003,12376,50388,203490,817190,3268760,13037895,51895935,206253075,818809200,3247943160,12875774670,51021117810,202112640600,800472431850,3169870830126,12551759587422",
			"name": "Binomial coefficients C(2n+1, n-2).",
			"comment": [
				"a(n) is the number of royal paths (A006318) from (0,0) to (n,n) with exactly one diagonal step off the line y=x. - _David Callan_, Mar 25 2004",
				"a(n) is the total number of DDUU's in all Dyck (n+2)-paths. - _David Scambler_, May 03 2013"
			],
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, 1964 (and various reprintings), p. 828.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A003516/b003516.txt\"\u003eTable of n, a(n) for n = 2..1000\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"Heidi Goodson, \u003ca href=\"https://arxiv.org/abs/1901.08653\"\u003eAn Identity for Vertically Aligned Entries in Pascal's Triangle\u003c/a\u003e, arXiv:1901.08653 [math.CO], 2019.",
				"Milan Janjic, \u003ca href=\"http://www.pmfbl.org/janjic/\"\u003eTwo Enumerative Functions\u003c/a\u003e",
				"Asamoah Nkwanta and Earl R. Barnes, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL15/Nkwanta/nkwanta2.html\"\u003eTwo Catalan-type Riordan Arrays and their Connections to the Chebyshev Polynomials of the First Kind\u003c/a\u003e, Journal of Integer Sequences, Article 12.3.3, 2012. - From _N. J. A. Sloane_, Sep 16 2012",
				"Daniel W. Stasiuk, \u003ca href=\"https://harvest.usask.ca/bitstream/handle/10388/11865/STASIUK-THESIS-2019.pdf\"\u003eAn Enumeration Problem for Sequences of n-ary Trees Arising from Algebraic Operads\u003c/a\u003e, Master's Thesis, University of Saskatchewan-Saskatoon (2018)."
			],
			"formula": [
				"G.f.: 32*x^2/(sqrt(1-4*x)*(sqrt(1-4*x)+1)^5). - _Marco A. Cisneros Guevara_, Jul 18 2011",
				"a(n) = Sum_{k=0,..,n-2} binomial(n+k+2,k). - _Arkadiusz Wesolowski_, Apr 02 2012",
				"(n+3)*(n-2)*a(n) = 2*n*(2*n+1)*a(n-1). - _R. J. Mathar_, Oct 13 2012",
				"G.f.: x^2*c(x)^5/sqrt(1-4*x) = ((-1 + 2*x) + (1 - 3*x + x^2) * c(x))/(x^2*sqrt(1-4*x)), with c(x) the o.g.f. of the Catalan numbers A000108. See the W. Lang link under A115139 for powers of c. - _Wolfdieter Lang_, Sep 10 2016",
				"a(n) ~ 2^(2*n+1)/sqrt(Pi*n). - _Ilya Gutkovskiy_, Sep 10 2016"
			],
			"example": [
				"For n=4, C(2*4+1,4-2) = C(9,2) = 9*8/2 = 36, so a(4) = 36. - _Michael B. Porter_, Sep 10 2016"
			],
			"mathematica": [
				"CoefficientList[ Series[ 32/(((Sqrt[1 - 4 x] + 1)^5)*Sqrt[1 - 4 x]), {x, 0, 25}], x] (* _Robert G. Wilson v_, Aug 08 2011 *)",
				"Table[Binomial[2*n +1,n-2], {n,2,25}] (* _G. C. Greubel_, Jan 23 2017 *)"
			],
			"program": [
				"(MAGMA) [Binomial(2*n+1,n-2): n in [2..25]]; // _Vincenzo Librandi_, Apr 13 2011",
				"(PARI) {a(n) = binomial(2*n+1, n-2)}; \\\\ _G. C. Greubel_, Mar 21 2019",
				"(Sage) [binomial(2*n+1, n-2) for n in (2..25)] # _G. C. Greubel_, Mar 21 2019",
				"(GAP) List([2..25], n-\u003e Binomial(2*n+1, n-2)) # _G. C. Greubel_, Mar 21 2019"
			],
			"xref": [
				"Diagonal 6 of triangle A100257.",
				"Third unsigned column (s=2) of A113187. - _Wolfdieter Lang_, Oct 18 2012",
				"Cf. triangle A114492 - Dyck paths with k DDUU's."
			],
			"keyword": "nonn,easy",
			"offset": "2,2",
			"author": "_N. J. A. Sloane_",
			"references": 11,
			"revision": 79,
			"time": "2019-03-22T00:30:58-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}