{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A208884",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 208884,
			"data": "1,3,3,7,3,9,1,9,9,19,15,27,5,19,17,33,25,43,31,51,9,31,27,51,19,45,9,37,33,63,47,79,7,41,19,55,23,61,25,65,53,95,69,113,79,125,43,91,35,85,17,69,61,115,85,141,99,157,27,87,37,99,81,145,105,171",
			"name": "a(n) = (a(n-1) + n)/2^k where 2^k is the largest power of 2 dividing a(n-1) + n, for n\u003e1 with a(1)=1.",
			"comment": [
				"In other words, to get a(n), add n to a(n-1) and compute the odd part (A000265) of the sum. - _Ralf Stephan_, Oct 27 2013",
				"POSITIONS of odd numbers in the initial 7000000 terms begin:",
				"1: [1, 7, 69, 285, 3601, 5167, 92989, 112651, 6933175, ...];",
				"3: [2, 3, 5, 613, 8461, 46749, 81237, 102171, 126661, 3309589, ...];",
				"5: [13, 97, 2431, 92095, ...];",
				"7: [4, 33, 3167, 78095, 2723179, ...];",
				"9: [6, 8, 9, 21, 27, 303, 2017, 3239, 3765, 6753, 28387, 251451, ...];",
				"11: [75, 15823, 28221, 4091959, 5820487, ...];",
				"13: [22975, 42391, 3729249, ...];",
				"15: [11, 22587, 2527579, 6954893, ...];",
				"17: [15, 51, 3121, 13433, 74763, 376853, 576439, 896899, ...];",
				"19: [10, 14, 25, 35, 291, 77747, 757319, 1227595, 2307099, ...];",
				"21: [1417, 1557, 712229, 2563807, ...];",
				"23: [37, 127, 609, 2211, 5563, 199901, ...];",
				"25: [17, 39, 221, 1145, 3425, 17593, 4318897, ...];",
				"27: [12, 23, 59, 73, 289, 1149, 3393, 20439, 37107, ...];",
				"29: [573, 33315, 61505, 467047, 491359, 1170709, 1492309, 2498593, 3017011, ...];",
				"31: [19, 22, 229, 409, 6199, 60529, 3602675, 4108215, 4604929, ...]; ...",
				"From _Ya-Ping Lu_, Jun 25 2020: (Start)",
				"Conjecture: For any given odd number m, there exists a number n_max such that all odd numbers \u003c= m can be found in the sequence a(n) with n \u003c= n_max. For example:",
				"m = 1, n_max = 1;",
				"m = 3, n_max = 2;",
				"m = 5, n_max = 13;",
				"m = 11, n_max = 75",
				"m = 13, n_max = 22975;",
				"m = 305, n_max = 1025715;",
				"m = 749, n_max = 14695985;",
				"m = 795, n_max = 150788015;",
				"m = 7525, n_max = 31129547917;",
				"...",
				"If the conjecture above is true, this sequence contains all odd numbers. (End)"
			],
			"link": [
				"Paul D. Hanna, \u003ca href=\"/A208884/b208884.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A208884/a208884.png\"\u003eColored scatterplot of the first 100000 terms\u003c/a\u003e (where the color is function of the parity of n)"
			],
			"example": [
				"a(2) = 1 + 2 = 3;",
				"a(3) = (3 + 3)/2 = 3;",
				"a(4) = 3 + 4 = 7;",
				"a(5) = (7 + 5)/4 = 3;",
				"a(6) = 3 + 6 = 9;",
				"a(7) = (9 + 7)/16 = 1; ..."
			],
			"mathematica": [
				"a[1]=1; a[n_] := a[n] = #/2^IntegerExponent[#, 2] \u0026@ (n + a[n-1]); Array[a, 70] (* _Giovanni Resta_, Jun 25 2020 *)"
			],
			"program": [
				"(PARI) {a(n)=if(n==1, 1, (a(n-1)+n)/2^valuation(a(n-1)+n,2))}",
				"(PARI) {A=vector(1024); a(n)=A[n]=if(n==1, 1, (A[n-1]+n)/2^valuation(A[n-1]+n,2))}",
				"for(n=1,#A,print1(a(n),\", \"))"
			],
			"xref": [
				"Cf. A069834, A090895, A114216, A335817."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Paul D. Hanna_, Mar 02 2012",
			"references": 3,
			"revision": 41,
			"time": "2020-06-28T02:26:11-04:00",
			"created": "2012-03-02T18:36:39-05:00"
		}
	]
}