{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A214152",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 214152,
			"data": "1,2,1,6,5,1,24,23,10,1,120,119,78,17,1,720,719,588,207,26,1,5040,5039,4611,2279,458,37,1,40320,40319,38890,24553,6996,891,50,1,362880,362879,358018,268521,101072,18043,1578,65,1,3628800,3628799,3612004,3042210,1438112,337210,40884,2603,82,1",
			"name": "Number of permutations T(n,k) in S_n containing an increasing subsequence of length k; triangle T(n,k), n\u003e=1, 1\u003c=k\u003c=n, read by rows.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A214152/b214152.txt\"\u003eRows n = 1..55, flattened\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PermutationPattern.html\"\u003ePermutation Pattern\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Longest_increasing_subsequence_problem\"\u003eLongest increasing subsequence problem\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Young_tableau\"\u003eYoung tableau\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = Sum_{i=k..n} A047874(n,i).",
				"T(n,k) = A000142(n) - A214015(n,k-1)."
			],
			"example": [
				"T(3,2) = 5.  All 3! = 6 permutations of {1,2,3} contain an increasing subsequence of length 2 with the exception of 321.",
				"Triangle T(n,k) begins:",
				":    1;",
				":    2,    1;",
				":    6,    5,    1;",
				":   24,   23,   10,    1;",
				":  120,  119,   78,   17,   1;",
				":  720,  719,  588,  207,  26,  1;",
				": 5040, 5039, 4611, 2279, 458, 37,  1;"
			],
			"maple": [
				"h:= proc(l) local n; n:=nops(l); add(i, i=l)! /mul(mul(1+l[i]-j",
				"      +add(`if`(l[k]\u003e=j, 1, 0), k=i+1..n), j=1..l[i]), i=1..n)",
				"    end:",
				"g:= (n, i, l)-\u003e `if`(n=0 or i=1, h([l[], 1$n])^2, `if`(i\u003c1, 0,",
				"                 add(g(n-i*j, i-1, [l[], i$j]), j=0..n/i))):",
				"T:= (n, k)-\u003e n! -g(n, k-1, []):",
				"seq(seq(T(n, k), k=1..n), n=1..12);"
			],
			"mathematica": [
				"h[l_] := With[{n = Length[l]}, Sum[i, {i, l}]! / Product[Product[1 + l[[i]] - j + Sum[If[l[[k]] \u003e= j, 1, 0], {k, i+1, n}], {j, 1, l[[i]]}], {i, 1, n}] ]; g[n_, i_, l_] := If[n == 0 || i === 1, h[Join[l, Array[1\u0026, n]]]^2, If[i \u003c 1, 0, Sum[g[n - i*j, i-1, Join[l, Array[i\u0026, j]]], {j, 0, n/i}]]]; t[n_, k_] := n! - g[n, k-1, {}]; Table[Table[t[n, k], {k, 1, n}], {n, 1, 12}] // Flatten (* _Jean-François Alcover_, Dec 17 2013, translated from Maple *)"
			],
			"xref": [
				"Columns k=1-10 give: A000142 (for n\u003e0), A033312, A056986, A158005, A158432, A159139, A159175, A217675, A217676, A217677.",
				"Row sums give: A003316.",
				"T(2n,n) gives A269021.",
				"Diagonal and lower diagonals give: A000012, A002522, A217200, A217193.",
				"Cf. A047874, A214015."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Alois P. Heinz_, Jul 05 2012",
			"references": 15,
			"revision": 44,
			"time": "2018-10-05T20:46:33-04:00",
			"created": "2012-07-06T03:10:18-04:00"
		}
	]
}