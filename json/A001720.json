{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001720",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1720,
			"id": "M3960 N1634",
			"data": "1,5,30,210,1680,15120,151200,1663200,19958400,259459200,3632428800,54486432000,871782912000,14820309504000,266765571072000,5068545850368000,101370917007360000,2128789257154560000,46833363657400320000,1077167364120207360000",
			"name": "a(n) = n!/24.",
			"comment": [
				"The asymptotic expansion of the higher-order exponential integral E(x,m=1,n=5) ~ exp(-x)/x*(1 - 5/x + 30/x^2 - 210/x^3 + 1680/x^4 - 15120/x^5 + 151200/x^6 - 1663200/x^7 + ...) leads to the sequence given above. See A163931 and A130534 for more information. - _Johannes W. Meijer_, Oct 20 2009"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A001720/b001720.txt\"\u003eTable of n, a(n) for n = 4..300\u003c/a\u003e",
				"S. Cerdá, \u003ca href=\"http://arxiv.org/abs/1504.06694\"\u003eA simple scheme to find the solutions to Brocard-Ramanujan Diophantine Equation n! + 1 = m^2\u003c/a\u003e, arXiv:1504.06694 [math.NT], 2015.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=264\"\u003eEncyclopedia of Combinatorial Structures 264\u003c/a\u003e.",
				"Wolfdieter Lang, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL3/LANG/lang.html\"\u003eOn generalizations of Stirling number triangles\u003c/a\u003e, J. Integer Seqs., Vol. 3 (2000), #00.2.4.",
				"D. S. Mitrinovic and R. S. Mitrinovic, \u003ca href=\"http://pefmath2.etf.rs/files/47/77.pdf\"\u003eTableaux d'une classe de nombres reliés aux nombres de Stirling\u003c/a\u003e, Univ. Beograd. Publ. Elektrotehn. Fak. Ser. Mat. Fiz. No. 77 (1962), 1-77.",
				"Alexsandar Petojevic, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL5/Petojevic/petojevic5.html\"\u003eThe Function vM_m(s; a; z) and Some Well-Known Sequences\u003c/a\u003e, Journal of Integer Sequences, Vol. 5 (2002), Article 02.1.7.",
				"\u003ca href=\"/index/Di#divseq\"\u003eIndex to divisibility sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Fa#factorial\"\u003eIndex entries for sequences related to factorial numbers\u003c/a\u003e"
			],
			"formula": [
				"E.g.f. if offset 0: 1/(1-x)^5.",
				"a(n) = A173333(n,4). - _Reinhard Zumkeller_, Feb 19 2010",
				"a(n) = A245334(n,n-4) / 5. - _Reinhard Zumkeller_, Aug 31 2014",
				"G(x) = (1 - (1 + x)^(-4)) / 4 = x - 5 x^2/2! + 30 x^3/3! - ..., an e.g.f. for this signed sequence (for n!/4!), is the compositional inverse of H(x) = (1 - 4*x)^(-1/4) - 1 = x + 5 x^2/2! + 45 x^3/3! + ..., an e.g.f. for A007696. Cf. A094638, A001710 (for n!/2!), and A001715 (for n!/3!). Cf. columns of A094587, A173333, and A213936 and rows of A138533. - _Tom Copeland_, Dec 27 2019",
				"E.g.f.: x^4 / (4! * (1 - x)). - _Ilya Gutkovskiy_, Jul 09 2021"
			],
			"mathematica": [
				"a[n_]:=n!/24; (* _Vladimir Joseph Stephan Orlovsky_, Dec 13 2008 *)",
				"Range[4,30]!/24 (* _Harvey P. Dale_, Jul 27 2021 *)"
			],
			"program": [
				"(MAGMA) [Factorial(n)/24: n in [4..25]]; // _Vincenzo Librandi_, Jul 20 2011",
				"(PARI) a(n)=n!/24 \\\\ _Charles R Greathouse IV_, Jan 12 2012",
				"(Haskell)",
				"a001720 = (flip div 24) . a000142 -- _Reinhard Zumkeller_, Aug 31 2014"
			],
			"xref": [
				"a(n)= A049353(n-3, 1) (first column of triangle).",
				"Cf. A000142, A049459, A051338, A245334.",
				"Cf. A001710, A001715, A007696, A094638.",
				"Cf. A094587, A138533, A173333, A213936."
			],
			"keyword": "nonn,easy",
			"offset": "4,2",
			"author": "_N. J. A. Sloane_",
			"references": 44,
			"revision": 68,
			"time": "2021-07-27T15:47:53-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}