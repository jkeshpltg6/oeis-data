{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A329581",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 329581,
			"data": "0,1,2,3,4,5,6,20,9,8,11,23,7,10,21,50,30,36,17,31,37,16,12,14,25,42,22,67,15,19,28,13,34,18,40,24,41,139,27,49,43,60,124,52,26,57,75,87,32,48,35,44,92,39,29,38,45,33,59,98,64,51,46,218,53,93,58,56,47,135,54,134,55,95,72,62,65,85",
			"name": "For every n \u003e= 0, exactly 11 sums are prime among a(n+i) + a(n+j), 0 \u003c= i \u003c j \u003c 8: lexicographically earliest such sequence of distinct nonnegative numbers.",
			"comment": [
				"That is, there are 11 primes, counted with multiplicity, among the 28 pairwise sums of any 8 consecutive terms.",
				"Is this a permutation of the nonnegative integers?",
				"If so, then the restriction to [1..oo) is a permutation of the positive integers, but not the lexicographically earliest one with this property, which starts (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 13, 24, 23, 30, 29, 14, ...)."
			],
			"link": [
				"M. F. Hasler, \u003ca href=\"/wiki/User:M._F._Hasler/Prime_sums_from_neighboring_terms\"\u003ePrime sums from neighboring terms\u003c/a\u003e, OEIS wiki, Nov. 23, 2019"
			],
			"example": [
				"In P(7) := {0, 1, 2, 3, 4, 5, 6} there are already S(7) := 10 primes 0+2, 0+3, 0+5, 1+2, 1+4, 1+6, 2+3, 2+5, 3+4, 5+6 among the pairwise sums, so the next term a(7) must produce exactly one more prime when added to elements of P(7). We find that a(7) = 20 is the smallest possible term (with 20 + 3 = 23).",
				"Then in P(8) = {1, 2, 3, 4, 5, 6, 20} there are S(8) = 8 primes among the pairwise sums, so a(8) must produce exactly 3 more primes when added to elements of P(8). We find a(8) = 9 is the smallest possibility (with 2+9, 4+9 and 20+9).",
				"And so on."
			],
			"program": [
				"(PARI) A329581(n,show=0,o=0,N=11,M=7,p=[],U,u=o)={for(n=o,n-1, if(show\u003e0,print1(o\", \"), show\u003c0,listput(L,o)); U+=1\u003c\u003c(o-u); U\u003e\u003e=-u+u+=valuation(U+1,2); p=concat(if(#p\u003e=M,p[^1],p),o); my(c=N-sum(i=2,#p, sum(j=1,i-1, isprime(p[i]+p[j]))));if(#p\u003cM\u0026\u0026sum(i=1,#p,isprime(p[i]+u))\u003c=c,o=u)|| for(k=u,oo,bittest(U,k-u)|| sum(i=1,#p,isprime(p[i]+k))!=c||[o=k,break]));show\u0026\u0026print([u]);o} \\\\ optional args: show=1: print a(o..n-1), show=-1: append them on global list L, in both cases print [least unused number] at the end; o=1: start at a(1)=1; N, M: find N primes using M+1 terms"
			],
			"xref": [
				"Cf. A329580 (10 primes using 8 consecutive terms), A329579 (9 primes using 7 consecutive terms), A329425 (6 primes using 5 consecutive terms).",
				"Cf. A329455 (4 primes using 5 consecutive terms), A329455 (3 primes using 5 consecutive terms), A329453 (2 primes using 5 consecutive terms), A329452 (2 primes using 4 consecutive terms).",
				"Cf. A329577 (7 primes using 7 consecutive terms), A329566 (6 primes using 6 consecutive terms), A329449 (4 primes using 4 consecutive terms).",
				"Cf. A329454 (3 primes using 4 consecutive terms), A329411 (2 primes using 3 consecutive terms), A329333 (1 odd prime using 3 terms), A329450 (0 primes using 3 terms).",
				"Cf. A329405 ff: other variants defined for positive integers."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_M. F. Hasler_, Nov 17 2019",
			"references": 13,
			"revision": 10,
			"time": "2020-02-09T16:35:57-05:00",
			"created": "2019-11-17T09:34:45-05:00"
		}
	]
}