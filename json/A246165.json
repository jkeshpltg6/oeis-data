{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A246165",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 246165,
			"data": "1,2,4,3,7,6,11,5,12,10,17,9,23,16,19,8,29,18,35,15,28,25,41,14,31,34,30,24,51,27,59,13,44,43,47,26,67,52,58,22,77,42,83,38,49,61,89,21,70,46,73,53,99,45,69,37,88,75,111,40,119,85,72,20,94,64,127,63,103,68,137,39,143,97,79,78,106,87,151,36",
			"name": "Permutation of natural numbers: a(1) = 1, a(n) = A064989(n)-th integer among those positive integers not occurring earlier in the sequence. [A064989(n) shifts the prime factorization of n one step right].",
			"comment": [
				"Terms at a(2^n) are: 1, 2, 3, 5, 8, 13, 20, 32, 48, 71, 105, 156, 236, 354, 542, 815, 1228, ...",
				"Fixed points begin as: 1, 2, 6, 10, 18, 42, 92, 26372, ..."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A246165/b246165.txt\"\u003eTable of n, a(n) for n = 1..8192\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"example": [
				"By definition, a(1) = 1.",
				"After that, for n = 2, when its prime factorization is shifted once right, results A064989(2) = 1, so we select the 1st of still unused positive natural numbers, which is 2, thus a(2) = 2.",
				"For n = 3 = p_2 (3 is the second prime), when its prime factorization is shifted once right, results A064989(3) = 2 = p_1, so we select 2nd of still unused numbers, which is 4, thus a(3) = 4.",
				"For n = 4, like for all powers of two, the result of right shifting is 1, so we select the smallest still unused number, which is 3, thus a(4) = 3.",
				"For n = 5 = p_3, A064989(5) = 3 = p_2, so we select the 3rd smallest still unused number from [5, 6, 7, 8, ...] which is 7, thus a(5) = 7."
			],
			"program": [
				"(Scheme, with defineperm1-macro from _Antti Karttunen_'s IntSeq-library)",
				"(defineperm1 (A246165 n) (if (\u003c= n 1) n (let loop ((i 1) (the-n-th-one (- (A064989 n) 1))) (cond ((not-lte? (A246166 i) n) (if (zero? the-n-th-one) i (loop (+ i 1) (- the-n-th-one 1)))) (else (loop (+ i 1) the-n-th-one))))))",
				";; We consider a \u003e b (i.e. not less than or equivalent to b) also in case a is nil.",
				";; (Which is needed when using the stateful caching system used by defineperm1-macro):",
				"(define (not-lte? a b) (cond ((not (number? a)) #t) (else (\u003e a b))))"
			],
			"xref": [
				"Inverse: A246166.",
				"Similar permutations: A119435, A126917.",
				"Cf. A064989."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Antti Karttunen_, Aug 17 2014",
			"references": 5,
			"revision": 14,
			"time": "2014-11-02T12:18:37-05:00",
			"created": "2014-08-18T00:53:43-04:00"
		}
	]
}