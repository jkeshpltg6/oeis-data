{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A299480",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 299480,
			"data": "1,0,1,1,2,0,1,2,2,0,2,2,2,0,1,3,3,0,2,2,2,0,2,4,2,0,2,2,4,0,1,4,2,0,3,3,2,0,2,4,4,0,2,2,2,0,2,6,3,0,2,2,4,0,2,4,2,0,4,4,2,0,1,5,4,0,2,2,4,0,3,6,2,0,2,2,4,0,2,6,2,0,4,4,2,0,2,4,6,0,2,2,2,0,2,8,3,0,3,3,4,0,2,4",
			"name": "List of pairs (a,b) where in the n-th pair, a = number of odd divisors of n and b = number of even divisors of n.",
			"comment": [
				"Also sequence found by reading in the upper part of the diagram of periodic curves for the number of divisors of n (see the first diagram in the Links section). Explanation: the number of curves that emerge from the point (n, 0) to the left hand in the upper part of the diagram equals A001227(n) the number of odd divisors of n. The number of curves that emerge from the same point (n, 0) to the right hand in the upper part of the diagram equals A183063(n) the number of even divisors of n. So the n-th pair is (A001227(n), A183063(n)). Also the total number of curves that emerges from the same point (n, 0) equals A000005(n), the number of divisors of n. Note that at the point (n, 0) the inflection point of the curve that emerges with diameter k represents the divisor n/k.",
				"The second diagram in the links section shows only the upper part from the first diagram."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A299480/b299480.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Omar E. Pol, \u003ca href=\"http://www.polprimos.com/imagenespub/poldiv01.jpg\"\u003eDiagram of periodic curves for the number of divisors of n (for n = 1..16)\u003c/a\u003e",
				"Omar E. Pol, \u003ca href=\"http://www.polprimos.com/imagenespub/poldiv02.jpg\"\u003eUpper part of the diagram of periodic curves for the number of divisors of n (for n = 1..16)\u003c/a\u003e"
			],
			"formula": [
				"Pair(a,b) = Pair(A001227(n), A183063(n)).",
				"G.f.: Sum_{n\u003e=1} (x^(2*n-1) + x^(4*n))/(1-x^(4*n)). - _Robert Israel_, Feb 11 2018"
			],
			"example": [
				"Array begins:",
				"n      A001227  A183063",
				"1         1        0",
				"2         1        1",
				"3         2        0",
				"4         1        2",
				"5         2        0",
				"6         2        2",
				"7         2        0",
				"8         1        3",
				"9         3        0",
				"10        2        2",
				"11        2        0",
				"12        2        4",
				"..."
			],
			"maple": [
				"f := proc (n) local t; t := numtheory:-tau(n/2^padic:-ordp(n, 2)); t, numtheory:-tau(n)-t end proc:",
				"map(f, [$1..100]); # _Robert Israel_, Feb 11 2018"
			],
			"mathematica": [
				"m = 105; CoefficientList[Sum[(x^(2n-1) + x^(4n))/(1 - x^(4n)), {n, 1, m/2//Ceiling}] + O[x]^m, x] // Rest (* _Jean-François Alcover_, Mar 22 2019, after _Robert Israel_ *)"
			],
			"xref": [
				"Row sums give A000005.",
				"For another version see A299485.",
				"Cf. A001227, A027750, A048272, A183063."
			],
			"keyword": "nonn,tabf",
			"offset": "1,5",
			"author": "_Omar E. Pol_, Feb 10 2018",
			"references": 10,
			"revision": 38,
			"time": "2019-03-23T01:45:13-04:00",
			"created": "2018-02-10T22:28:45-05:00"
		}
	]
}