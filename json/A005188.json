{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005188",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5188,
			"id": "M0488",
			"data": "1,2,3,4,5,6,7,8,9,153,370,371,407,1634,8208,9474,54748,92727,93084,548834,1741725,4210818,9800817,9926315,24678050,24678051,88593477,146511208,472335975,534494836,912985153,4679307774,32164049650,32164049651",
			"name": "Armstrong (or pluperfect, or Plus Perfect, or narcissistic) numbers: m-digit positive numbers equal to sum of the m-th powers of their digits.",
			"comment": [
				"A finite sequence, the 88th and last term being 115132219018763992565095597973971522401.",
				"Let k = d_1 d_2 ... d_n in base 10; then k is in the sequence iff k = Sum_{i=1..n} d_i^n.",
				"These are the fixed points in the \"Recurring Digital Invariant Variant\" described in A151543.",
				"a(15) = A229381(3) = 8208 is the \"Simpsons' narcissistic number\".",
				"If a(n) is a multiple of 10, then a(n+1) = a(n) + 1, and if a(n) == 1 (mod 10) then a(n-1) = a(n) - 1 except for n = 1, cf. Examples. - _M. F. Hasler_, Oct 18 2018"
			],
			"reference": [
				"J.-M. De Koninck, Ces nombres qui nous fascinent, Entry 88, pp 30-31, Ellipses, Paris 2008.",
				"Lionel E. Deimel, Jr. and Michael T. Jones, Finding Pluperfect Digital Invariants: Techniques, Results and Observations, J. Rec. Math., 14 (1981), 87-108.",
				"J. P. Lamoitier, Fifty Basic Exercises. SYBEX Inc., 1981.",
				"Tomas Antonio Mendes Oliveira e Silva (tos(AT)ci.ua.pt) gave the full sequence in a posting (Article 42889) to sci.math on May 09 1994.",
				"Clifford A. Pickover, A Passion for Mathematics, Wiley, 2005; see p. 68.",
				"Joe Roberts, \"The Lure of the Integers\", page 36.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A005188/b005188.txt\"\u003eTable of n, a(n) for n = 1..88\u003c/a\u003e (the full list of terms, from Winter)",
				"Anonymous, \u003ca href=\"http://everything2.net/index.pl?node_id=1525466\u0026amp;displaytype=printable\u0026amp;lastnode_id=1525466\"\u003eNarcissistic number\u003c/a\u003e",
				"H. de Jong, \u003ca href=\"/A005188/a005188_1.pdf\"\u003eLetter to N. J. A. Sloane\u003c/a\u003e, Mar 8 1988.",
				"L. E. Deimel, \u003ca href=\"http://www.deimel.org/rec_math/DI_1.htm\"\u003eNarcissistic Numbers\u003c/a\u003e",
				"M. Gardner \u0026 N. J. A. Sloane, \u003ca href=\"/A003154/a003154.pdf\"\u003eCorrespondence, 1973-74\u003c/a\u003e",
				"H. Heinz, \u003ca href=\"http://web.archive.org/web/20180303194332/http://www.magic-squares.net:80/narciss.htm#Perfect%20Digital%20Invariants\"\u003eNarcissistic Numbers\u003c/a\u003e (backup from March 2018 on web/archive.org: page no longer available), Sep. 1998, last updated in Sep. 2010",
				"L. H. \u0026 W. Lopez, PlanetMath.Org, \u003ca href=\"http://web.archive.org/web/20121109214834/http://planetmath.org:80/encyclopedia/ArmstrongNumber.html\"\u003eArmstrong number\u003c/a\u003e (latest backup on web.archive.org of ArmstrongNumber.html from 2012), published by L.H. not later than July 2007.",
				"Gordon L. Miller and Mary T. Whalen, \u003ca href=\"https://www.fq.math.ca/Scanned/30-3/miller.pdf\"\u003eArmstrong Numbers: 153 = 1^3 + 5^3 + 3^3\u003c/a\u003e, Fibonacci Quarterly, 30-3 (1992), 221-224.",
				"W. Schneider, \u003ca href=\"http://web.archive.org/web/2004/www.wschnei.de/digit-related-numbers/pdi.html\"\u003ePerfect Digital Invariants: Pluperfect Digital Invariants(PPDIs)\u003c/a\u003e",
				"B. Shader, \u003ca href=\"http://everything2.net/index.pl?node_id=1407017\u0026amp;displaytype=printable\u0026amp;lastnode_id=1407017\"\u003eArmstrong number\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/NarcissisticNumber.html\"\u003eNarcissistic Number\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Narcissistic_number\"\u003eNarcissistic number\u003c/a\u003e",
				"Robert G. Wilson v, \u003ca href=\"/A005188/a005188.pdf\"\u003eLetter to N. J. A. Sloane\u003c/a\u003e, Jan 23 1989.",
				"D. T. Winter, \u003ca href=\"http://web.archive.org/web/20100109234250/http://ftp.cwi.nl:80/dik/Armstrong\"\u003eTable of Armstrong Numbers\u003c/a\u003e (latest backup on web.archive.org from Jan. 2010; page no longer available), published not later than Aug. 2003."
			],
			"example": [
				"153 = 1^3 + 5^3 + 3^3,",
				"8208 = 8^4 + 2^4 + 0^4 + 8^4,",
				"4210818 = 4^7 + 2^7 + 1^7 + 0^7 + 8^7 + 1^7 + 8^7.",
				"The eight terms 370, 24678050, 32164049650, 4338281769391370, 3706907995955475988644380, 19008174136254279995012734740, 186709961001538790100634132976990 and 115132219018763992565095597973971522400 end in a digit zero, therefore their successor a(n) + 1 is the next term a(n+1). This also yields the last term of the sequence. The initial a(1) = 1 is the only term ending in a digit 1 not preceded by a(n) - 1. - _M. F. Hasler_, Oct 18 2018"
			],
			"maple": [
				"filter:= proc(k) local d;",
				"d:= 1 + ilog10(k);",
				"add(s^d, s=convert(k,base,10)) = k",
				"end proc:",
				"select(filter, [$1..10^6]); # _Robert Israel_, Jan 02 2015"
			],
			"mathematica": [
				"f[n_] := Plus @@ (IntegerDigits[n]^Floor[ Log[10, n] + 1]); Select[ Range[10^7], f[ # ] == # \u0026] (* _Robert G. Wilson v_, May 04 2005 *)",
				"Select[Range[10^7],#==Total[IntegerDigits[#]^IntegerLength[#]]\u0026] (* _Harvey P. Dale_, Sep 30 2011 *)"
			],
			"program": [
				"(PARI) is(n)=my(v=digits(n));sum(i=1,#v,v[i]^#v)==n \\\\ _Charles R Greathouse IV_, Nov 20 2012",
				"(PARI) select( is_A005188(n)={n==vecsum([d^#n|d\u003c-n=digits(n)])}, [0..9999]) \\\\ _M. F. Hasler_, Nov 18 2019",
				"(Python)",
				"from itertools import combinations_with_replacement",
				"A005188_list = []",
				"for k in range(1,10):",
				"    a = [i**k for i in range(10)]",
				"    for b in combinations_with_replacement(range(10),k):",
				"        x = sum(map(lambda y:a[y],b))",
				"        if x \u003e 0 and tuple(int(d) for d in sorted(str(x))) == b:",
				"            A005188_list.append(x)",
				"A005188_list = sorted(A005188_list) # _Chai Wah Wu_, Aug 25 2015"
			],
			"xref": [
				"Cf. A001694, A007532, A005934, A003321, A014576, A046074.",
				"Similar to but different from A023052.",
				"Cf. A151543.",
				"Cf. A010343 to A010354 (bases 4 to 9). - _R. J. Mathar_, Jun 28 2009"
			],
			"keyword": "nonn,base,fini,full,nice",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, _Robert G. Wilson v_",
			"ext": [
				"32164049651 from Amit Munje (amit.munje(AT)gmail.com), Oct 07 2006",
				"In order to agree with the Definition, first comment modified by _Jonathan Sondow_, Jan 02 2015",
				"Comment in name moved to comment section and links edited by _M. F. Hasler_, Oct 18 2018",
				"\"Positive\" added to definition - _N. J. A. Sloane_, Nov 18 2019"
			],
			"references": 89,
			"revision": 119,
			"time": "2019-11-21T04:02:39-05:00",
			"created": "1991-05-20T03:00:00-04:00"
		}
	]
}