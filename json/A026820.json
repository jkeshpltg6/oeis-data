{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A026820",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 26820,
			"data": "1,1,2,1,2,3,1,3,4,5,1,3,5,6,7,1,4,7,9,10,11,1,4,8,11,13,14,15,1,5,10,15,18,20,21,22,1,5,12,18,23,26,28,29,30,1,6,14,23,30,35,38,40,41,42,1,6,16,27,37,44,49,52,54,55,56,1,7,19,34,47,58,65,70,73,75,76,77",
			"name": "Euler's table: triangular array T read by rows, where T(n,k) = number of partitions in which every part is \u003c= k for 1 \u003c= k \u003c= n. Also number of partitions of n into at most k parts.",
			"reference": [
				"G. Chrystal, Algebra, Vol. II, p. 558.",
				"D. S. Mitrinovic et al., Handbook of Number Theory, Kluwer, Section XIV.2, p. 493."
			],
			"link": [
				"Alois P. Heinz, Robert G. Wilson v, \u003ca href=\"/A026820/b026820.txt\"\u003eRows n = 1..141, flattened\u003c/a\u003e",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972, p. 831. [scanned copy]",
				"L. Euler, \u003ca href=\"https://gallica.bnf.fr/ark:/12148/bpt6k69587/f335.item\"\u003eIntroductio in Analysin Infinitorum\u003c/a\u003e, Book I, chapter XVI.",
				"T. S. Motzkin, \u003ca href=\"/A000262/a000262.pdf\"\u003eSorting numbers for cylinders and other classification numbers\u003c/a\u003e, in Combinatorics, Proc. Symp. Pure Math. 19, AMS, 1971, pp. 167-176. [Annotated, scanned copy]",
				"OEIS Wiki, \u003ca href=\"http://oeis.org/wiki/Sorting_numbers\"\u003eSorting numbers\u003c/a\u003e",
				"R. Sulzgruber, \u003ca href=\"https://othes.univie.ac.at/30616/\"\u003eThe Symmetry of the q,t-Catalan Numbers\u003c/a\u003e, Masterarbeit, Univ. Wien, 2013.",
				"Sergei Viznyuk, \u003ca href=\"http://phystech.com/ftp/A026820.c\"\u003eC-Program\u003c/a\u003e",
				"Sergei Viznyuk, \u003ca href=\"/A026820/a026820.c.txt\"\u003eLocal copy of C-Program\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PartitionFunctionq.html\"\u003ePartition Function q.\u003c/a\u003e",
				"\u003ca href=\"/index/Par#partN\"\u003eIndex entries for sequences related to partitions\u003c/a\u003e - _Reinhard Zumkeller_, Jan 21 2010"
			],
			"formula": [
				"T(T(n,n),n) = A134737(n). - _Reinhard Zumkeller_, Nov 07 2007",
				"T(A000217(n),n) = A173519(n). - _Reinhard Zumkeller_, Feb 20 2010",
				"T(n,k) = T(n,k-1) + T(n-k,k). - _Thomas Dybdahl Ahle_, Jun 13 2011",
				"T(n,k) = Sum_{i=1..min(k,floor(n/2))} T(n-i,i) + Sum_{j=1+floor(n/2)..k} A000041(n-j). - _Bob Selcoe_, Aug 22 2014 [corrected by _Álvar Ibeas_, Mar 15 2018]",
				"O.g.f.: Product_{i\u003e=0} 1/(1-y*x^i). - _Geoffrey Critzer_, Mar 11 2012",
				"T(n,k) = A008284(n+k,k). - _Álvar Ibeas_, Jan 06 2015"
			],
			"example": [
				"Triangle starts:",
				"  1;",
				"  1, 2;",
				"  1, 2,  3;",
				"  1, 3,  4,  5;",
				"  1, 3,  5,  6,  7;",
				"  1, 4,  7,  9, 10, 11;",
				"  1, 4,  8, 11, 13, 14, 15;",
				"  1, 5, 10, 15, 18, 20, 21, 22;",
				"  1, 5, 12, 18, 23, 26, 28, 29, 30;",
				"  1, 6, 14, 23, 30, 35, 38, 40, 41, 42;",
				"  1, 6, 16, 27, 37, 44, 49, 52, 54, 55, 56;",
				"  ..."
			],
			"maple": [
				"T:= proc(n, k) option remember;",
				"      `if`(n=0 or k=1, 1, T(n, k-1) + `if`(k\u003en, 0, T(n-k, k)))",
				"    end:",
				"seq(seq(T(n, k), k=1..n), n=1..12); # _Alois P. Heinz_, Apr 21 2012"
			],
			"mathematica": [
				"t[n_, k_] := Length@ IntegerPartitions[n, k]; Table[ t[n, k], {n, 12}, {k, n}] // Flatten",
				"(* Second program: *)",
				"T[n_, k_] := T[n, k] = If[n==0 || k==1, 1, T[n, k-1] + If[k\u003en, 0, T[n-k, k]]]; Table[T[n, k], {n, 1, 12}, {k, 1, n}] // Flatten (* _Jean-François Alcover_, Sep 22 2015, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(Haskell)",
				"import Data.List (inits)",
				"a026820 n k = a026820_tabl !! (n-1) !! (k-1)",
				"a026820_row n = a026820_tabl !! (n-1)",
				"a026820_tabl = zipWith",
				"   (\\x -\u003e map (p x) . tail . inits) [1..] $ tail $ inits [1..] where",
				"   p 0 _ = 1",
				"   p _ [] = 0",
				"   p m ks'@(k:ks) = if m \u003c k then 0 else p (m - k) ks' + p m ks",
				"-- _Reinhard Zumkeller_, Dec 18 2013",
				"(PARI) T(n,k)=my(s); forpart(v=n,s++,,k); s \\\\ _Charles R Greathouse IV_, Feb 27 2018"
			],
			"xref": [
				"Partial sums of rows of A008284, row sums give A058397, central terms give A171985, mirror is A058400.",
				"T(n,n) = A000041(n), T(n,1) = A000012(n), T(n,2) = A008619(n) for n\u003e1, T(n,3) = A001399(n) for n\u003e2, T(n,4) = A001400(n) for n\u003e3, T(n,5) = A001401(n) for n\u003e4, T(n,6) = A001402(n) for n\u003e5, T(n,7) = A008636(n) for n\u003e6, T(n,8) = A008637(n) for n\u003e7, T(n,9) = A008638(n) for n\u003e8, T(n,10) = A008639(n) for n\u003e9, T(n,11) = A008640(n) for n\u003e10, T(n,12) = A008641(n) for n\u003e11, T(n,n-2) = A007042(n-1) for n\u003e2, T(n,n-1) = A000065(n) for n\u003e1.",
				"Cf. A008284, A026840, A134737, A173519."
			],
			"keyword": "nonn,tabl,nice",
			"offset": "1,3",
			"author": "_Clark Kimberling_",
			"references": 39,
			"revision": 107,
			"time": "2019-08-04T16:11:55-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}