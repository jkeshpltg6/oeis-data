{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A014081",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 14081,
			"data": "0,0,0,1,0,0,1,2,0,0,0,1,1,1,2,3,0,0,0,1,0,0,1,2,1,1,1,2,2,2,3,4,0,0,0,1,0,0,1,2,0,0,0,1,1,1,2,3,1,1,1,2,1,1,2,3,2,2,2,3,3,3,4,5,0,0,0,1,0,0,1,2,0,0,0,1,1,1,2,3,0,0,0,1,0,0,1,2,1,1,1,2,2,2,3,4,1,1,1,2,1,1,2,3,1",
			"name": "a(n) is the number of occurrences of '11' in binary expansion of n.",
			"comment": [
				"a(n) takes the value k for the first time at n = 2^(k+1)-1. Cf. A000225. - _Robert G. Wilson v_, Apr 02 2009",
				"a(n) = A213629(n,3) for n \u003e 2. - _Reinhard Zumkeller_, Jun 17 2012"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A014081/b014081.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"J.-P. Allouche, \u003ca href=\"http://math.colgate.edu/~integers/graham2/graham2.Abstract.html\"\u003eOn an Inequality in a 1970 Paper of R. L. Graham\u003c/a\u003e, INTEGERS 21A (2021), #A2.",
				"Jean-Paul Allouche and Jeffrey Shallit, \u003ca href=\"https://doi.org/10.1007/BFb0097122\"\u003eSums of digits and the Hurwitz zeta function\u003c/a\u003e, in: K. Nagasaka and E. Fouvry (eds.), Analytic Number Theory, Lecture Notes in Mathematics, Vol. 1434, Springer, Berlin, Heidelberg, 1990, pp. 19-30.",
				"Helmut Prodinger, \u003ca href=\"http://dx.doi.org/10.1137/0603004\"\u003eGeneralizing the sum of digits function\u003c/a\u003e, SIAM J. Algebraic Discrete Methods, Vol. 3, No. 1 (1982), pp. 35-42. MR0644955 (83f:10009). [See B_2(11,n) on p. 35. - _N. J. A. Sloane_, Apr 06 2014]",
				"Michel Rigo and Manon Stipulanti, \u003ca href=\"https://arxiv.org/abs/2103.16966\"\u003eRevisiting regular sequences in light of rational base numeration systems\u003c/a\u003e, arXiv:2103.16966 [cs.FL], 2021. Mentions this sequence.",
				"Ralf Stephan, \u003ca href=\"/somedcgf.html\"\u003eSome divide-and-conquer sequences with (relatively) simple ordinary generating functions\u003c/a\u003e, 2004.",
				"Ralf Stephan, \u003ca href=\"/A079944/a079944.ps\"\u003eTable of generating functions\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DigitBlock.html\"\u003eDigit Block\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Rudin-ShapiroSequence.html\"\u003eRudin-Shapiro Sequence\u003c/a\u003e.",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"formula": [
				"a(4n) = a(4n+1) = a(n), a(4n+2) = a(2n+1), a(4n+3) = a(2n+1) + 1. - _Ralf Stephan_, Aug 21 2003",
				"G.f.: 1/(1-x) * Sum_{k\u003e=0} t^3/((1+t)*(1+t^2)), where t = x^(2^k). - _Ralf Stephan_, Sep 10 2003",
				"a(n) = A000120(n) - A069010(n). - _Ralf Stephan_, Sep 10 2003",
				"Sum_{n\u003e=1} A014081(n)/(n*(n+1)) = A100046 (Allouche and Shallit, 1990). - _Amiram Eldar_, Jun 01 2021"
			],
			"example": [
				"The binary expansion of 15 is 1111, which contains three occurrences of 11, so a(15)=3."
			],
			"maple": [
				"# To count occurrences of 11..1 (k times) in binary expansion of v:",
				"cn := proc(v, k) local n, s, nn, i, j, som, kk;",
				"som := 0;",
				"kk := convert(cat(seq(1, j = 1 .. k)),string);",
				"n := convert(v, binary);",
				"s := convert(n, string);",
				"nn := length(s);",
				"for i to nn - k + 1 do",
				"if substring(s, i .. i + k - 1) = kk then som := som + 1 fi od;",
				"som; end; # This program no longer worked. Corrected by _N. J. A. Sloane_, Apr 06 2014.",
				"[seq(cn(n,2),n=0..300)];",
				"# Alternative:",
				"A014081 := proc(n) option remember;",
				"  if n mod 4 \u003c= 1 then procname(floor(n/4))",
				"elif n mod 4 = 2 then procname(n/2)",
				"else 1 + procname((n-1)/2)",
				"fi",
				"end proc:",
				"A014081(0):= 0:",
				"map(A014081, [$0..1000]); # _Robert Israel_, Sep 04 2015"
			],
			"mathematica": [
				"f[n_] := Count[ Partition[ IntegerDigits[n, 2], 2, 1], {1, 1}]; Table[ f@n, {n, 0, 104}] (* _Robert G. Wilson v_, Apr 02 2009 *)"
			],
			"program": [
				"(Haskell)",
				"import Data.Bits ((.\u0026.))",
				"a014081 n = a000120 (n .\u0026. div n 2)  -- _Reinhard Zumkeller_, Jan 23 2012",
				"(PARI) A014081(n)=sum(i=0,#binary(n)-2,bitand(n\u003e\u003ei,3)==3)  \\\\ _M. F. Hasler_, Jun 06 2012",
				"(PARI) a(n) = hammingweight(bitand(n, n\u003e\u003e1)) ;",
				"vector(105, i, a(i-1))  \\\\ _Gheorghe Coserea_, Aug 30 2015",
				"(Python)",
				"def a(n): return sum([((n\u003e\u003ei)\u00263==3) for i in range(len(bin(n)[2:]) - 1)]) # _Indranil Ghosh_, Jun 03 2017"
			],
			"xref": [
				"Cf. A014082, A033264, A037800, A056973, A000225, A213629, A000120, A069010, A100046.",
				"First differences give A245194.",
				"A245195 gives 2^a(n)."
			],
			"keyword": "nonn,base,easy",
			"offset": "0,8",
			"author": "_Simon Plouffe_",
			"references": 41,
			"revision": 109,
			"time": "2021-10-30T10:46:51-04:00",
			"created": "1998-06-14T03:00:00-04:00"
		}
	]
}