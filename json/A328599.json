{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A328599",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 328599,
			"data": "1,0,0,0,0,2,0,4,2,4,12,8,22,14,36,44,62,114,130,206,264,414,602,822,1250,1672,2520,3518,5146,7408,10448,15224,21496,31284,44718,64170,92314,131618,190084,271870,391188,560978,804264,1155976,1656428,2381306,3414846",
			"name": "Number of compositions of n with no part circularly followed by a divisor or a multiple.",
			"comment": [
				"A composition of n is a finite sequence of positive integers summing to n.",
				"Circularity means the last part is followed by the first."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A328599/b328599.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e"
			],
			"example": [
				"The a(0) = 1 through a(12) = 22 compositions (empty columns not shown):",
				"  ()  (2,3)  (2,5)  (3,5)  (2,7)  (3,7)      (2,9)  (5,7)",
				"      (3,2)  (3,4)  (5,3)  (4,5)  (4,6)      (3,8)  (7,5)",
				"             (4,3)         (5,4)  (6,4)      (4,7)  (2,3,7)",
				"             (5,2)         (7,2)  (7,3)      (5,6)  (2,7,3)",
				"                                  (2,3,5)    (6,5)  (3,2,7)",
				"                                  (2,5,3)    (7,4)  (3,4,5)",
				"                                  (3,2,5)    (8,3)  (3,5,4)",
				"                                  (3,5,2)    (9,2)  (3,7,2)",
				"                                  (5,2,3)           (4,3,5)",
				"                                  (5,3,2)           (4,5,3)",
				"                                  (2,3,2,3)         (5,3,4)",
				"                                  (3,2,3,2)         (5,4,3)",
				"                                                    (7,2,3)",
				"                                                    (7,3,2)",
				"                                                    (2,3,2,5)",
				"                                                    (2,3,4,3)",
				"                                                    (2,5,2,3)",
				"                                                    (3,2,3,4)",
				"                                                    (3,2,5,2)",
				"                                                    (3,4,3,2)",
				"                                                    (4,3,2,3)",
				"                                                    (5,2,3,2)"
			],
			"mathematica": [
				"Table[Length[Select[Join@@Permutations/@IntegerPartitions[n],And@@Not/@Divisible@@@Partition[#,2,1,1]\u0026\u0026And@@Not/@Divisible@@@Reverse/@Partition[#,2,1,1]\u0026]],{n,0,10}]"
			],
			"program": [
				"(PARI)",
				"b(n, q, pred)={my(M=matrix(n, n)); for(k=1, n, M[k, k]=pred(q, k); for(i=1, k-1, M[i, k]=sum(j=1, k-i, if(pred(j, i), M[j, k-i], 0)))); M[q,]}",
				"seq(n)={concat([1], sum(k=1, n, b(n, k, (i,j)-\u003ei%j\u003c\u003e0\u0026\u0026j%i\u003c\u003e0)))} \\\\ _Andrew Howroyd_, Oct 26 2019"
			],
			"xref": [
				"The necklace version is A328601.",
				"The case forbidding only divisors (not multiples) is A328598.",
				"The non-circular version is A328508.",
				"Partitions with no part followed by a divisor are A328171.",
				"Cf. A000740, A008965, A167606, A318729, A318748, A328460, A328593, A328600, A328603, A328608, A328609, A328674."
			],
			"keyword": "nonn",
			"offset": "0,6",
			"author": "_Gus Wiseman_, Oct 25 2019",
			"ext": [
				"Terms a(26) and beyond from _Andrew Howroyd_, Oct 26 2019"
			],
			"references": 8,
			"revision": 12,
			"time": "2019-10-27T05:00:14-04:00",
			"created": "2019-10-26T10:00:03-04:00"
		}
	]
}