{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A237354",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 237354,
			"data": "0,1,2,2,2,2,3,3,3,3,4,3,3,3,4,3,4,3,4,4,4,3,4,4,4,4,4,4,4,4,4,4,4,4,5,4,4,4,5,4,5,4,5,5,4,4,5,4,5,5,5,4,5,4,5,5,5,4,6,4,5,5,5,5,5,4,5,5,5,4,6,4,5,5,5,5,5,4,5,5,5,4,6,5,5,5,5",
			"name": "a(n) is the maximum of omega(g)+omega(h) for all decompositions n=g+h with g\u003e=h\u003e=1.",
			"comment": [
				"omega(g) is defined in A001221.",
				"The smallest n that makes a(n)=2k should be twice the product of the first k-th prime numbers.  For example, a(4)=2, 4=2*2; a(12)=4, 12=2*(2*3); a(60)=6, 60=2*(2*3*5).",
				"The largest n that makes a(n)=k should be smaller than or equal to the product of the first k-th primes plus 1. For example, a(3)=1, 3 = 2+1; a(7)=2, 7=2*3+1; a(23)=3, 23\u003c2*3*5+1=31; a(89)=4, 89\u003c211=2*3*5*7+1."
			],
			"link": [
				"Lei Zhou, \u003ca href=\"/A237354/b237354.txt\"\u003eTable of n, a(n) for n = 2..10000\u003c/a\u003e"
			],
			"example": [
				"For n=2, 2=1+1. 1 does not have prime factor. So a(2)=0+0=0;",
				"For n=3, 3=1+2, 1 does not have prime factor, where 2 has one. So a(3)=0+1=1;",
				"For n=4, 4=1+3=2+2.  From 1+3 we got 1, from 2+2 we got 2.  The larger one is 2.  So a(4)=1+1=2.",
				"...",
				"For n=211, in best case we have 211=105+106=3*5*7+2*53.  So a(211)=3+2=5."
			],
			"maple": [
				"A237354 := proc(n)",
				"    local a,g,om ;",
				"    a := 0 ;",
				"    for g from 1 to n/2 do",
				"        om := A001221(g)+A001221(n-g) ;",
				"        if om \u003e a then",
				"            a := om ;",
				"        end if;",
				"    end do:",
				"    a ;",
				"end proc:",
				"seq(A237354(n),n=2..100) ; # _R. J. Mathar_, Feb 13 2014"
			],
			"mathematica": [
				"Table[ct = 0; Do[h = n - g; c = Length[FactorInteger[g]] + Length[FactorInteger[h]]; If[g == 1, c--]; If[h == 1, c--]; If[c \u003e ct, ct = c], {g, 1, Floor[n/2]}]; ct, {n, 2, 88}]",
				"(* _Wouter Meeussen_ : *) Table[ Max@Table[PrimeNu[ n - k ] + PrimeNu[  k  ], {k, n - 1}], {n, 2, 88}]"
			],
			"xref": [
				"Cf. A237353, A002375, A001221."
			],
			"keyword": "nonn,easy",
			"offset": "2,3",
			"author": "_Lei Zhou_, Feb 06 2014",
			"references": 3,
			"revision": 18,
			"time": "2014-02-14T10:13:28-05:00",
			"created": "2014-02-14T10:13:28-05:00"
		}
	]
}