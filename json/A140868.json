{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A140868",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 140868,
			"data": "4,9,16,21,28,33,38,45,50,57,62,67,74,79,86,91,98,103,108,115,120,127,132,137,144,149,156,161,168,173,178,185,190,197,202,207,214,219,226,231,236,243,248,255,260,267,272,277,284,289,296,301,306,313,318,325,330,337,342,347,354,359,366,371,376,383",
			"name": "a(n) = floor(floor(n*alpha)*alpha) where alpha = 1+sqrt(2) = A014176.",
			"comment": [
				"The sequence of first differences d = 5,7,5,7,5,5,7,...  of this sequence, given by d(n) := a(n+1) - a(n), is equal to the fixed point of the morphism 5 -\u003e 57, 7 -\u003e 575. See Example 6 in my paper \"Morphic words, Beatty sequences and integer images of the Fibonacci language\".  Modulo a change of alphabet, the sequence d occurs at many places in OEIS. See A006337, A159684, A080763, A276862, A276864. - _Michel Dekking_, Feb 18 2020"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A140868/b140868.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Shiri Artstein-Avidan, Aviezri S. Fraenkel and Vera T. Sos, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2007.08.070\"\u003eA two-parameter family of an extension of Beatty\u003c/a\u003e, Discr. Math. 308 (2008), 4578-4588.",
				"Shiri Artstein-avidan, Aviezri S. Fraenkel and Vera T. Sos, \u003ca href=\"http://www.wisdom.weizmann.ac.il/~fraenkel/Papers/coatp8.pdf\"\u003eA two-parameter family of an extension of Beatty sequences\u003c/a\u003e, Discrete Math., 308 (2008), 4578-4588.",
				"M. Dekking, \u003ca href=\"https://doi.org/10.1016/j.tcs.2019.12.036\"\u003eMorphic words, Beatty sequences and integer images of the Fibonacci language\u003c/a\u003e, Theoretical Computer Science  809, 407-417 (2020)."
			],
			"formula": [
				"a(n)= A003151(A003151(n)). - _Michel Dekking_, Feb 18 2020"
			],
			"maple": [
				"Digits := 200: a014176:= 1+sqrt(2) : A140868 := proc(n) global a014176 ; floor(a014176*floor(n*a014176)) ; end: for n from 1 to 100 do printf(\"%d,\",A140868(n)); end: # _R. J. Mathar_, Sep 05 2008"
			],
			"mathematica": [
				"With[{p = 1+Sqrt[2]}, Table[Floor[p*Floor[n*p]], {n, 1, 100}]] (* _G. C. Greubel_, Sep 27 2018 *)"
			],
			"program": [
				"(PARI) vector(100, n, round(floor((1+sqrt(2))*floor(n*(1+sqrt(2)))))) \\\\ _G. C. Greubel_, Sep 27 2018",
				"(MAGMA) [Round(Floor((1+Sqrt(2))*Floor(n*(1+Sqrt(2))))): n in [1..100]]; // _G. C. Greubel_, Sep 27 2018",
				"(Python)",
				"from sympy import integer_nthroot",
				"def A140868(n):",
				"    f = lambda n: n+integer_nthroot(2*n**2,2)[0]",
				"    return f(f(n)) # _Chai Wah Wu_, Mar 17 2021"
			],
			"xref": [
				"Cf. A003151, A006337, A159684, A080763, A276862, A276864."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_, Sep 04 2008",
			"ext": [
				"Corrected definition and extended by _R. J. Mathar_, Sep 05 2008"
			],
			"references": 2,
			"revision": 25,
			"time": "2021-03-17T20:12:00-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}