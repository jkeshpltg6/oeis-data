{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A328177",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 328177,
			"data": "1,3,3,2,5,3,7,6,3,7,11,6,13,7,7,4,17,7,19,5,7,11,23,6,5,15,11,7,29,7,31,12,11,19,7,6,37,19,15,13,41,7,43,15,13,23,47,12,7,15,19,13,53,15,15,14,19,31,59,13,61,31,15,8,13,15,67,21,23,15,71,9",
			"name": "a(n) is the minimal value of the expression d OR (n/d) where d runs through the divisors of n and OR denotes the bitwise OR operator.",
			"link": [
				"Rémy Sigrist, \u003ca href=\"/A328177/b328177.txt\"\u003eTable of n, a(n) for n = 1..16384\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A328177/a328177.png\"\u003eLogarithmic scatterplot of the first 2^16 terms\u003c/a\u003e"
			],
			"formula": [
				"a(n)^2 \u003e= n with equality iff n is a square.",
				"a(p) = p for any odd prime number p."
			],
			"example": [
				"For n = 12:",
				"- we have the following values:",
				"    d   12/d  d OR (12/d)",
				"    --  ----  -----------",
				"     1    12           13",
				"     2     6            6",
				"     3     4            7",
				"     4     3            7",
				"     6     2            6",
				"    12     1           13",
				"- hence a(12) = min({6, 7, 13}) = 6."
			],
			"maple": [
				"a:= n-\u003e min(map(d-\u003e Bits[Or](d, n/d), numtheory[divisors](n))):",
				"seq(a(n), n=1..100);  # _Alois P. Heinz_, Oct 09 2019"
			],
			"program": [
				"(PARI) a(n) = vecmin(apply(d -\u003e bitor(d, n/d), divisors(n)))"
			],
			"xref": [
				"See A328176 and A328178 for similar sequences.",
				"Cf. A218388."
			],
			"keyword": "nonn,base",
			"offset": "1,2",
			"author": "_Rémy Sigrist_, Oct 06 2019",
			"references": 3,
			"revision": 13,
			"time": "2019-10-09T10:56:35-04:00",
			"created": "2019-10-09T04:17:38-04:00"
		}
	]
}