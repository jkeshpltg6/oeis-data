{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A327584",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 327584,
			"data": "1,1,3,4,6,13,48,150,300,666,936,1824,2520,2160,5040,75,536,2820,11144,41346,131304,420084,1191552,3427008,9207456,23466720,61522560,141553560,345346560,777152160,1635096960,3700806480,6998261760,14211912960,27442437120",
			"name": "Number T(n,k) of colored compositions of n using all colors of a k-set such that all parts have different color patterns and the patterns for parts i have i distinct colors in increasing order; triangle T(n,k), k\u003e=0, k\u003c=n\u003c=k*2^(k-1), read by columns.",
			"comment": [
				"T(n,k) is defined for all n\u003e=0 and k\u003e=0.  The triangle displays only positive terms.  All other terms are zero."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A327584/b327584.txt\"\u003eColumns k = 0..7, flattened\u003c/a\u003e"
			],
			"example": [
				"T(3,2) = 4: 2ab1a, 2ab1b, 1a2ab, 1b2ab.",
				"T(3,3) = 13: 3abc, 2ab1c, 2ac1b, 2bc1a, 1a2bc, 1b2ac, 1c2ab, 1a1b1c, 1a1c1b, 1b1a1c, 1b1c1a, 1c1a1b, 1c1b1a.",
				"T(4,2) = 6: 2ab1a1b, 1a2ab1b, 1a1b2ab, 2ab1b1a, 1b2ab1a, 1b1a2ab.",
				"Triangle T(n,k) begins:",
				"  1;",
				"     1;",
				"        3;",
				"        4,  13;",
				"        6,  48,    75;",
				"           150,   536,    541;",
				"           300,  2820,   6320,   4683;",
				"           666, 11144,  50150,  81012,   47293;",
				"           936, 41346, 308080, 903210, 1134952, 545835;",
				"           ..."
			],
			"maple": [
				"C:= binomial:",
				"g:= proc(n) option remember; n*2^(n-1) end:",
				"h:= proc(n) option remember; local k; for k from",
				"      `if`(n=0, 0, h(n-1)) do if g(k)\u003e=n then return k fi od",
				"    end:",
				"b:= proc(n, i, k, p) option remember; `if`(n=0, p!,",
				"      `if`(i\u003c1 or k\u003ch(n), 0, add(b(n-i*j, min(n-i*j, i-1),",
				"        k, p+j)*C(C(k, i), j), j=0..n/i)))",
				"    end:",
				"T:= (n, k)-\u003e add(b(n$2, i, 0)*(-1)^(k-i)*C(k, i), i=0..k):",
				"seq(seq(T(n, k), n=k..k*2^(k-1)), k=0..5);"
			],
			"mathematica": [
				"c = Binomial;",
				"g[n_] := g[n] = n*2^(n - 1);",
				"h[n_] := h[n] = Module[{k}, For[k = If[n == 0, 0,",
				"     h[n - 1]], True, k++, If[g[k] \u003e= n, Return[k]]]];",
				"b[n_, i_, k_, p_] := b[n, i, k, p] = If[n == 0, p!,",
				"     If[i \u003c 1 || k \u003c h[n], 0, Sum[b[n - i*j, Min[n - i*j, i - 1],",
				"     k, p + j]*c[c[k, i], j], {j, 0, n/i}]]];",
				"T[n_, k_] := Sum[b[n, n, i, 0]*(-1)^(k - i)*c[k, i], {i, 0, k}];",
				"Table[Table[T[n, k], {n, k, k*2^(k - 1)}], {k, 0, 5}] // Flatten (* _Jean-François Alcover_, Feb 22 2021, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Main diagonal gives A000670.",
				"Row sums give A321587.",
				"Column sums give A327585.",
				"Cf. A001787, A326962, A327583 (this triangle read by rows)."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_Alois P. Heinz_, Sep 17 2019",
			"references": 5,
			"revision": 25,
			"time": "2021-02-22T09:22:14-05:00",
			"created": "2019-09-17T20:28:49-04:00"
		}
	]
}