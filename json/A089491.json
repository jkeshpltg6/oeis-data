{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A089491",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 89491,
			"data": "9,5,4,9,2,9,6,5,8,5,5,1,3,7,2,0,1,4,6,1,3,3,0,2,5,8,0,2,3,5,0,8,6,1,7,2,2,0,6,7,5,7,8,7,4,4,4,2,7,3,8,6,9,2,4,8,6,0,0,4,0,6,4,3,5,3,3,8,0,7,8,5,8,0,5,3,5,9,2,1,0,5,4,0,6,8,2,8,1,6,5,9,7,5,1,8,5,1,5,7,3,6,4,3,7",
			"name": "Decimal expansion of Buffon's constant 3/Pi.",
			"comment": [
				"Whereas 2/Pi (A060294) is the probability that a needle will land on one of many parallel lines, this is the probability that a needle will land on one of many lines making up a grid.",
				"The probability that the boundary of an equilateral triangle will intersect one of the parallel lines if the triangle edge length l (almost) equals the distance d between each pair of lines. This follows directly from the Weisstein/MathWorld Buffon's Needle Problem link's statement P=p/(Pi*d), where P is the probability of intersection with any convex polygon's boundary if the generalized diameter of that polygon is less than d and p is the perimeter of the polygon. (Take d=l, then p=3d.) - _Rick L. Shepherd_, Jan 11 2006",
				"Related grid problems are discussed in the Weisstein/MathWorld Buffon-Laplace Needle Problem link. - _Rick L. Shepherd_, Jan 11 2006",
				"The area of a regular dodecagon circumscribed in a unit-area circle. - _Amiram Eldar_, Nov 05 2020"
			],
			"reference": [
				"Joe Portney, Portney's Ponderables, Litton Systems, Inc., Appendix 2, 'Buffon's Needle' by Lawrence R. Weill, 200, pp. 135-138."
			],
			"link": [
				"Harry Khamis, \u003ca href=\"http://www.wright.edu/~harry.khamis/buffons_needle_problem/\"\u003eBuffon's Needle Problem\u003c/a\u003e.",
				"Kevin Peterson, \u003ca href=\"https://faculty.lynchburg.edu/peterson_km/buffon.pdf\"\u003eA Problem in Geometric Probability: Buffon's Needle Problem\u003c/a\u003e.",
				"George Reese, \u003ca href=\"http://mste.illinois.edu/activity/buffon/\"\u003eBuffon's Needle, An Analysis and Simulation\u003c/a\u003e.",
				"Shodor Education Foundation, Inc., \u003ca href=\"http://www.shodor.org/interactivate/activities/buffon/#\"\u003eBuffon's needle\u003c/a\u003e.",
				"Washington and Lee University, \u003ca href=\"http://home.wlu.edu/~mcraea/GeometricProbabilityFolder/ApplicationsConvexSets/Problem18/Problem18.html\"\u003eProblem 18: Buffon's Needle Again\u003c/a\u003e. [Broken link]",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/BuffonsNeedleProblem.html\"\u003eBuffon's needle problem\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Buffon-LaplaceNeedleProblem.html\"\u003eBuffon-Laplace needle problem\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/GeneralizedDiameter.html\"\u003eGeneralized Diameter\u003c/a\u003e.",
				"\u003ca href=\"/index/Tra#transcendental\"\u003eIndex entries for transcendental numbers\u003c/a\u003e"
			],
			"formula": [
				"Equals sinc(Pi/6). - _Peter Luschny_, Oct 04 2019",
				"From _Amiram Eldar_, Aug 20 2020: (Start)",
				"Equals Product{k\u003e=1} cos(Pi/(6*2^k)).",
				"Equals Product{k\u003e=1} (1 - 1/(6*k)^2). (End)"
			],
			"example": [
				"3/Pi = 0.95492965855137201461330258023508617220675787444273869248600..."
			],
			"mathematica": [
				"RealDigits[ N[ 3/Pi, 111]][[1]]"
			],
			"program": [
				"(PARI) 3/Pi \\\\ _Michel Marcus_, Nov 05 2020"
			],
			"xref": [
				"Cf. A000796 (Pi), A060294 (2/Pi)."
			],
			"keyword": "cons,nonn",
			"offset": "0,1",
			"author": "_Robert G. Wilson v_, Nov 04 2003",
			"references": 24,
			"revision": 32,
			"time": "2020-11-05T15:14:45-05:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}