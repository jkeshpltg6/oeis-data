{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A081572",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 81572,
			"data": "1,1,1,1,2,2,1,3,5,3,1,4,10,13,5,1,5,17,35,34,8,1,6,26,75,125,89,13,1,7,37,139,338,450,233,21,1,8,50,233,757,1541,1625,610,34,1,9,65,363,1490,4172,7069,5875,1597,55,1,10,82,535,2669,9633,23165,32532,21250,4181,89",
			"name": "Square array of binomial transforms of Fibonacci numbers, read by ascending antidiagonals.",
			"comment": [
				"Array rows are solutions of the recurrence a(n) = (2*k+1)*a(n-1) - A028387(k-1)*a(n-2), where a(0) = 1 and a(1) = k+1."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A081572/b081572.txt\"\u003eAntidiagonal rows n = 0..50, flattened\u003c/a\u003e"
			],
			"formula": [
				"Rows are successive binomial transforms of F(n+1).",
				"T(n, k) = ((5+sqrt(5))/10)*( (2*n + 1 + sqrt(5))/2)^k + ((5-sqrt(5)/10)*( 2*n + 1 - sqrt(5))/2 )^k.",
				"From _G. C. Greubel_, May 27 2021: (Start)",
				"T(n, k) = Sum_{j=0..k} binomial(k,j)*n^(k-j)*Fibonacci(j+1) (square array).",
				"T(n, k) = Sum_{j=0..k} binomial(k,j)*(n-k)^(k-j)*Fibonacci(j+1) (antidiagonal triangle). (End)"
			],
			"example": [
				"The array rows begins as:",
				"  1, 1,  2,   3,    5,     8,     13, ... A000045;",
				"  1, 2,  5,  13,   34,    89,    233, ... A001519;",
				"  1, 3, 10,  35,  125,   450,   1625, ... A081567;",
				"  1, 4, 17,  75,  338,  1541,   7069, ... A081568;",
				"  1, 5, 26, 139,  757,  4172,  23165, ... A081569;",
				"  1, 6, 37, 233, 1490,  9633,  62753, ... A081570;",
				"  1, 7, 50, 363, 2669, 19814, 148153, ... A081571;",
				"Antidiagonal triangle begins as:",
				"  1;",
				"  1, 1;",
				"  1, 2,  2;",
				"  1, 3,  5,   3;",
				"  1, 4, 10,  13,   5;",
				"  1, 5, 17,  35,  34,    8;",
				"  1, 6, 26,  75, 125,   89,   13;",
				"  1, 7, 37, 139, 338,  450,  233,  21;",
				"  1, 8, 50, 233, 757, 1541, 1625, 610, 34;"
			],
			"mathematica": [
				"T[n_, k_]:= If[n==0, Fibonacci[k+1], Sum[Binomial[k, j]*Fibonacci[j+1]*n^(k-j), {j, 0, k}]]; Table[T[n-k, k], {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, May 26 2021 *)"
			],
			"program": [
				"(MAGMA)",
				"A081572:= func\u003c n,k | (\u0026+[Binomial(k,j)*Fibonacci(j+1)*(n-k)^(k-j): j in [0..k]]) \u003e;",
				"[A081572(n,k): k in [0..n], n in [0..12]]; // _G. C. Greubel_, May 27 2021",
				"(Sage)",
				"def A081572(n,k): return sum( binomial(k,j)*fibonacci(j+1)*(n-k)^(k-j) for j in (0..k) )",
				"flatten([[A081572(n,k) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, May 27 2021"
			],
			"xref": [
				"Array row n: A000045 (n=0), A001519 (n=1), A081567 (n=2), A081568 (n=3), A081569 (n=4), A081570 (n=5), A081571 (n=6).",
				"Array column k: A000027 (k=1), A002522 (k=2).",
				"Different from A073133.",
				"Cf. A028387."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,5",
			"author": "_Paul Barry_, Mar 22 2003",
			"references": 2,
			"revision": 18,
			"time": "2021-11-12T12:23:50-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}