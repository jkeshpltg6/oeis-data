{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A055270",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 55270,
			"data": "1,5,36,252,1764,12348,86436,605052,4235364,29647548,207532836,1452729852,10169108964,71183762748,498286339236,3488004374652,24416030622564,170912214357948,1196385500505636,8374698503539452,58622889524776164,410360226673433148,2872521586714032036",
			"name": "a(n) = 7*a(n-1) + (-1)^n * binomial(2,2-n) with a(-1)=0.",
			"comment": [
				"For n \u003e= 2, a(n) is equal to the number of functions f:{1,2,...,n}-\u003e{1,2,3,4,5,6,7} such that for fixed, different x_1, x_2 in {1,2,...,n} and fixed y_1, y_2 in {1,2,3,4,5,6,7} we have f(x_1) \u003c\u003e y_1 and f(x_2) \u003c\u003e y_2. - _Milan Janjic_, Apr 19 2007",
				"a(n) is the number of generalized compositions of n when there are 6*i-1 different types of i, (i=1,2,...). - _Milan Janjic_, Aug 26 2010"
			],
			"reference": [
				"A. H. Beiler, Recreations in the Theory of Numbers, Dover, N.Y., 1964, pp. 122-125, 194-196."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A055270/b055270.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Milan Janjic, \u003ca href=\"http://www.pmfbl.org/janjic/\"\u003eEnumerative Formulas for Some Functions on Finite Sets\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_01\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (7)."
			],
			"formula": [
				"a(n) = 6^2 * 7^(n-2), n \u003e= 2 with a(0)=1, a(1)=5.",
				"G.f.: (1-x)^2/(1-7*x).",
				"a(n) = Sum_{k=0..n} A201780(n,k)*5^k. - _Philippe Deléham_, Dec 05 2011",
				"E.g.f.: (13 - 7*x + 36*exp(7*x))/49. - _G. C. Greubel_, Mar 16 2020"
			],
			"maple": [
				"A055270:= n-\u003e `if`(n\u003c2, 4*n+1, 36*7^(n-2)); seq(A055270(n), n=0..30); # _G. C. Greubel_, Mar 16 2020"
			],
			"mathematica": [
				"Join[{1,5},NestList[7#\u0026,36,20]] (* _Harvey P. Dale_, Sep 04 2017 *)"
			],
			"program": [
				"(MAGMA) [1,5] cat [36*7^(n-2): n in [2..30]]; // _G. C. Greubel_, Mar 16 2020",
				"(Sage) [1,5]+[36*7^(n-2) for n in (2..30)] # _G. C. Greubel_, Mar 16 2020"
			],
			"xref": [
				"Cf. A055272 (first differences of 7^n (A000420))."
			],
			"keyword": "easy,nonn",
			"offset": "0,2",
			"author": "_Barry E. Williams_, May 10 2000",
			"ext": [
				"Terms a(20) onward added by _G. C. Greubel_, Mar 16 2020"
			],
			"references": 3,
			"revision": 35,
			"time": "2020-05-10T15:54:08-04:00",
			"created": "2000-06-15T03:00:00-04:00"
		}
	]
}