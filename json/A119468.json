{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A119468",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 119468,
			"data": "1,1,1,2,2,1,4,6,3,1,8,16,12,4,1,16,40,40,20,5,1,32,96,120,80,30,6,1,64,224,336,280,140,42,7,1,128,512,896,896,560,224,56,8,1,256,1152,2304,2688,2016,1008,336,72,9,1,512,2560,5760,7680,6720,4032,1680,480,90,10,1",
			"name": "Triangle read by rows: T(n,k) = Sum_{j=0..n-k} binomial(n,2j)*binomial(n-2j,k).",
			"comment": [
				"Product of Pascal's triangle A007318 and A119467. Row sums are A007051. Diagonal sums are A113225.",
				"Variant of A080928, A115068 and A082137. - _R. J. Mathar_, Feb 09 2010",
				"Matrix inverse of the Euler tangent triangle A081733. - _Peter Luschny_, Jul 18 2012",
				"Central column: T(2*n,n) = A069723(n). - _Peter Luschny_, Jul 22 2012",
				"Subtriangle of the triangle in A198792. - _Philippe Deléham_, Nov 10 2013"
			],
			"formula": [
				"G.f.: (1 - x - xy)/(1 - 2x - 2x*y + 2x^2*y + x^2*y^2).",
				"Number triangle T(n,k) = Sum_{j=0..n} binomial(n,j)*binomial(j,k)*(1+(-1)^(j-k))/2.",
				"Define matrix: A(n,m,k) = If[m \u003c n, 1, -1];",
				"  p(x,k) = CharacteristicPolynomial[A[n,m,k],x]; then t(n,m) = coefficients(p(x,n)). - _Roger L. Bagula_ and _Gary W. Adamson_, Jan 25 2009",
				"E.g.f.: exp(x*z)/(1-tanh(x)). - _Peter Luschny_, Aug 01 2012",
				"T(n,k) = 2*T(n-1,k) + 2*T(n-1,k-1) - 2*T(n-2,k-1) - T(n-2,k-2) for n \u003e= 2, T(0,0) = T(1,0) = T(1,1) = 1, T(n,k) = 0 if k \u003c 0 or if k \u003e n. - _Philippe Deléham_, Nov 10 2013",
				"E.g.f.: [(e^(2t)+1)/2] e^(tx) = e^(P.(x)t), so this is an Appell sequence with lowering operator D = d/dx and raising operator R = x + 2/(e^(-2D)+1), i.e., D P_n(x) = n P_{n-1}(x) and R p_n(x) = P_{n+1}(x) where P_n(x) = [(x+2)^n + x^n]/2. Also, (P.(x)+y)^n = P_n(x+y), umbrally. R = x + 1 + D - 2 D^3/3! + ... contains the e.g.f.(D) mod signs of A009006 and A155585 and signed, aerated A000182, the zag numbers, so the unsigned differential component 2/[e^(2D)+1] = 2 Sum_{n \u003e= 0} Eta(-n) (-2D)^n/n!, where Eta(s) is the Dirichlet eta function, and 2 *(-2)^n Eta(-n) = (-1)^n (2^(n+1)-4^(n+1)) Zeta(-n) = (2^(n+1)-4^(n+1)) B(n+1)/(n+1) with Zeta(s), the Riemann zeta function, and B(n), the Bernoulli numbers. The polynomials PI_n(x) of A081733 are the umbral compositional inverses of this sequence, i.e., P_n(PI.(x)) = x^n = PI_n(P.(x)) under umbral composition. Aside from the signs and the main diagonals, multiplying this triangle by 2 gives the face-vectors of the hypercubes A038207. - _Tom Copeland_, Sep 27 2015",
				"T(n,k) = 2^(n-k-1+0^(n-k))*binomial(n, k). - _Peter Luschny_, Nov 10 2017"
			],
			"example": [
				"Triangle begins",
				"    1;",
				"    1,    1;",
				"    2,    2,    1;",
				"    4,    6,    3,    1;",
				"    8,   16,   12,    4,    1;",
				"   16,   40,   40,   20,    5,    1;",
				"   32,   96,  120,   80,   30,    6,    1;",
				"   64,  224,  336,  280,  140,   42,    7,   1;",
				"  128,  512,  896,  896,  560,  224,   56,   8,  1;",
				"  256, 1152, 2304, 2688, 2016, 1008,  336,  72,  9,  1;",
				"  512, 2560, 5760, 7680, 6720, 4032, 1680, 480, 90, 10, 1;"
			],
			"maple": [
				"A119468_row := proc(n) local s,t,k;",
				"  s := series(exp(z*x)/(1-tanh(x)),x,n+2);",
				"  t := factorial(n)*coeff(s,x,n); seq(coeff(t,z,k), k=(0..n)) end:",
				"for n from 0 to 7 do A119468_row(n) od; # _Peter Luschny_, Aug 01 2012",
				"# Alternatively:",
				"T := (n, k) -\u003e 2^(n-k-1+0^(n-k))*binomial(n,k):",
				"for n from 0 to 9 do seq(T(n,k), k=0..n) od; # _Peter Luschny_, Nov 10 2017"
			],
			"mathematica": [
				"A[k_] := Table[If[m \u003c n, 1, -1], {m, k}, {n, k}]; a = Join[{{1}}, Table[(-1)^n*CoefficientList[CharacteristicPolynomial[A[n], x], x], {n, 1, 10}]]; Flatten[a] (* _Roger L. Bagula_ and _Gary W. Adamson_, Jan 25 2009 *)"
			],
			"program": [
				"(Sage)",
				"R = PolynomialRing(QQ, 'x')",
				"def p(n,x) :",
				"  return 1 if n==0 else add((-1)^n*binomial(n,k)*(x^(n-k)-1) for k in range(n))",
				"def A119468_row(n):",
				"    x = R.gen()",
				"    return [abs(cf) for cf in list((p(n,x-1)-p(n,x+1))/2+x^n)]",
				"for n in (0..8) : print(A119468_row(n)) # _Peter Luschny_, Jul 22 2012"
			],
			"xref": [
				"A082137 read as triangle with rows reversed.",
				"Cf. A000182, A009006, A038207, A081733, A155585."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "0,4",
			"author": "_Paul Barry_, May 21 2006",
			"references": 8,
			"revision": 63,
			"time": "2020-03-07T05:38:46-05:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}