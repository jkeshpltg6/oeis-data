{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A328383",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 328383,
			"data": "-1,-1,0,-1,-2,-1,2,-3,-2,-1,9,-1,-4,23,1,-1,-4,-1,5,-2,-2,-1,2,-3,24,0,18,-1,-2,-1,6,-5,-2,85,7,-1,-4,21,10,-1,-2,-1,35,53,-4,-1,2,-5,44,18,34,-1,2,21,4,-3,-2,-1,16,-1,-6,21,1,-5,-2,-1,7,85,-2,-1,4,-1,23,55,5,-4,-2,-1,4,9,-2,-1,42,-3,42",
			"name": "a(n) gives the number of iterations of x -\u003e A003415(x) needed to reach the first number which is either a divisor or multiple of n, but not both at the same time. If no such number can ever be reached, a(n) is 0 (when either n is of the form p^p, or if the iteration would never stop). When the number reached is a divisor of n, a(n) is -1 * iteration count.",
			"comment": [
				"The absolute value of a(n) tells how many columns right from the leftmost column in array A258651 one needs to go at row n, before one (again) finds either a divisor or a multiple of n, with 0's reserved for cases like 4 and 27 where the same value continues forever. If one finds a divisor before a multiple, then the value of a(n) will be negative, otherwise it will be positive.",
				"Question: What is the value of a(91) ?"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A328383/b328383.txt\"\u003eTable of n, a(n) for n = 2..90\u003c/a\u003e"
			],
			"formula": [
				"a(A000040(n)) = -1.",
				"a(A051674(n)) = 0."
			],
			"example": [
				"For n = 6, its arithmetic derivative A003415(6) = 5 is neither its divisor nor its multiple, but the second arithmetic derivative A003415(5) = 1 is its divisor, thus a(6) = -2.",
				"For n = 8, its arithmetic derivative A003415(8) = 12 is neither its divisor nor its multiple, but the second arithmetic derivative A003415(12) = 16 is its multiple, thus a(8) = +2.",
				"Numbers reached for n=2..28 (with positions of the form p^p are filled with the same p^p): 1, 1, 4, 1, 1, 1, 16, 1, 1, 1, 8592, 1, 1, 410267320320, 32, 1, 1, 1, 240, 7, 1, 1, 48, 1, 410267320320, 27, 9541095424. For example, we have a(12) = 9 and the 9th arithmetic derivative of 12 is A003415^(9)(12) = 8592 = 716*12."
			],
			"program": [
				"(PARI)",
				"A003415(n) = if(n\u003c=1, 0, my(f=factor(n)); n*sum(i=1, #f~, f[i, 2]/f[i, 1]));",
				"A328383(n) = { my(u=A003415(n),k=1); if(u==n,return(0)); while((n%u) \u0026\u0026 (u%n), k++; u = A003415(u)); if(u%n,-k,k); };"
			],
			"xref": [
				"Cf. A003415, A258651, A328235, A328236.",
				"Cf. A051674 (indices of zeros provided for all n \u003e= 2 either a divisor or multiple can be found).",
				"Cf. A256750, A328248, A328384 for similar counts."
			],
			"keyword": "sign",
			"offset": "2,5",
			"author": "_Antti Karttunen_, Oct 15 2019",
			"references": 3,
			"revision": 29,
			"time": "2019-10-15T13:05:41-04:00",
			"created": "2019-10-15T13:05:41-04:00"
		}
	]
}