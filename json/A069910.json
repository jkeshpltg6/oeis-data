{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A069910",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 69910,
			"data": "1,0,1,1,2,2,3,3,5,5,7,8,11,12,16,18,23,26,33,37,46,52,63,72,87,98,117,133,157,178,209,236,276,312,361,408,471,530,609,686,784,881,1004,1126,1279,1433,1621,1814,2048,2286,2574,2871,3223,3590,4022,4472,5000",
			"name": "Expansion of Product_{i in A069908} 1/(1 - x^i).",
			"comment": [
				"Number 39 of the 130 identities listed in Slater 1952.",
				"Number of partitions of 2*n into distinct odd parts. - _Vladeta Jovovic_, May 08 2003"
			],
			"reference": [
				"M. D. Hirschhorn, The Power of q, Springer, 2017. Chapter 19, Exercises p. 173."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A069910/b069910.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"G. E. Andrews et al., \u003ca href=\"http://dx.doi.org/10.1080/16073606.2001.9639228\"\u003eq-Engel series expansions and Slater's identities\u003c/a\u003e, Quaestiones Math., 24 (2001), 403-416.",
				"George E. Andrews, Jethro van Ekeren and Reimundo Heluani, \u003ca href=\"https://arxiv.org/abs/2005.10769\"\u003eThe singular support of the Ising model\u003c/a\u003e, arXiv:2005.10769 [math.QA], 2020. See (1.4.2) p. 2.",
				"T. Gannon, G. Hoehn, H. Yamauchi, et. al., \u003ca href=\"https://www.math.ksu.edu/~gerald/voas/voa/minimal1.html\"\u003eVOA Unitary Minimal Model m=1\u003c/a\u003e, character.",
				"M. D. Hirschhorn, \u003ca href=\"http://dx.doi.org/10.1016/0097-3165(79)90005-0\"\u003eSome partition theorems of the Rogers-Ramanujan type\u003c/a\u003e, J. Combin. Theory Ser. A 27 (1979), no. 1, 33-37. MR0541341 (80j:05010). See Theorem 4. [From _N. J. A. Sloane_, Mar 19 2012]",
				"Vaclav Kotesovec, \u003ca href=\"http://arxiv.org/abs/1509.08708\"\u003eA method of finding the asymptotics of q-series based on the convolution of generating functions\u003c/a\u003e, arXiv:1509.08708 [math.CO], 2015-2016.",
				"Lucy Joan Slater, \u003ca href=\"https://doi.org/10.1112/plms/s2-54.2.147\"\u003eFurther Identities of the Rogers-Ramanujan Type\u003c/a\u003e, Proc. London Math. Soc., Series 2, vol.s2-54, no.2, pp. 147-167, (1952).",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Jackson-SlaterIdentity.html\"\u003eJackson-Slater Identity\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"M. P. Zaletel and R. S. K. Mong, \u003ca href=\"http://arxiv.org/abs/1208.4862\"\u003eExact Matrix Product States for Quantum Hall Wave Functions\u003c/a\u003e, arXiv preprint arXiv:1208.4862 [cond-mat.str-el], 2012. - _N. J. A. Sloane_, Dec 25 2012"
			],
			"formula": [
				"Euler transform of period 16 sequence [0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, ...]. - _Michael Somos_, Apr 11 2004",
				"G.f.: Sum_{n\u003e=0} q^(2*n^2) / Product_{k=1..2*n} (1 - q^k). - _Joerg Arndt_, Apr 01 2014",
				"a(n) ~ exp(sqrt(n/3)*Pi) / (2^(5/2) * 3^(1/4) * n^(3/4)). - _Vaclav Kotesovec_, Oct 04 2015",
				"Expansion of f(x^3, x^5) / f(-x^2) in powers of x where f(, ) is Ramanujan's general theta function. - _Michael Somos_, Apr 14 2016",
				"a(n) = A000700(2*n).",
				"a(n) = A027356(4n+1,2n+1). - _Alois P. Heinz_, Oct 28 2019",
				"From _Peter Bala_, Feb 08 2021: (Start)",
				"G.f.: A(x) = Product_{n \u003e= 1} (1 + x^(4*n))^2*(1 + x^(4*n-2))*(1 + x^(8*n-3))*(1 + x^(8*n-5)).",
				"The 2 X 2 matrix Product_{k \u003e= 0} [1, x^(2*k+1); x^(2*k+1), 1] = [A(x^2), x*B(x^2); x*B(x)^2, A(x^2)], where B(x) is the g.f. of A069911.",
				"A(x^2) + x*B(x^2) = A^2(-x) + x*B^2(-x) = Product_{k \u003e= 0} 1 + x^(2*k+1), the g.f. of A000700.",
				"A^2(x) + x*B^2(x) is the g.f. of A226622.",
				"(A^2(x) + x*B^2(x))/(A^2(x) - x*B^2(x)) is the g.f. of A208850.",
				"A^4(sqrt(x)) - x*B^4(sqrt(x)) is the g.f. of A029552.",
				"A(x)*B(x) is the g.f. of A226635; A(-x)/B(-x) is the g.f. of A111374; B(-x)/A(-x) is the g.f. of A092869. (End)"
			],
			"example": [
				"G.f. = 1 + x^2 + x^3 + 2*x^4 + 2*x^5 + 3*x^6 + 3*x^7 + 5*x^8 + 5*x^9 + ...",
				"G.f. = q^-1 + q^95 + q^143 + 2*q^191 + 2*q^239 + 3*q^287 + 3*q^335 + ..."
			],
			"maple": [
				"a:= proc(n) option remember; `if`(n=0, 1,",
				"      add(add(d*[0$2, 1$4, 0$5, 1$4, 0][irem(d, 16)+1],",
				"      d=numtheory[divisors](j))*a(n-j), j=1..n)/n)",
				"    end:",
				"seq(a(n), n=0..80);  # _Alois P. Heinz_, Apr 01 2014"
			],
			"mathematica": [
				"max = 56; p = Product[1/(1-x^i), {i, Select[Range[max], MemberQ[{2, 3, 4, 5, 11, 12, 13, 14}, Mod[#, 16]]\u0026]}]; s = Series[p, {x, 0, max}]; a[n_] := Coefficient[s, x, n]; Table[a[n], {n, 0, max}] (* _Jean-François Alcover_, Apr 09 2014 *)",
				"nmax=60; CoefficientList[Series[Product[(1-x^(8*k-1))*(1-x^(8*k-7))*(1-x^(8*k))*(1-x^(16*k-6))*(1-x^(16*k-10))/(1-x^k), {k, 1, nmax}], {x, 0, nmax}], x] (* _Vaclav Kotesovec_, Oct 04 2015 *)",
				"a[ n_] := SeriesCoefficient[ Product[ (1 - x^k)^-{ 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0 }[[ Mod[k, 16] + 1]], {k, n}], {x, 0, n}]; (* _Michael Somos_, Apr 14 2016 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0,0, n=2*n; A = x * O(x^n); polcoeff( eta(-x + A) / eta(x^2 + A), n))}; /* _Michael Somos_, Apr 11 2004 */",
				"(PARI) N=66;  q='q+O('q^N);  S=1+sqrtint(N);",
				"gf=sum(n=0, S, q^(2*n^2) / prod(k=1, 2*n, 1-q^k ) );",
				"Vec(gf)  \\\\ _Joerg Arndt_, Apr 01 2014",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( prod(k=1, n, (1 - x^k + x * O(x^n))^-[ 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0][k%16 + 1]), n))}; /* _Michael Somos_, Apr 14 2016 */"
			],
			"xref": [
				"Cf. A000700, A027356, A035457, A069908, A069909, A069911, A122129, A122135."
			],
			"keyword": "nonn,easy,changed",
			"offset": "0,5",
			"author": "_N. J. A. Sloane_, May 05 2002",
			"references": 11,
			"revision": 70,
			"time": "2022-01-09T09:59:10-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}