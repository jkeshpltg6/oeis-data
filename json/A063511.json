{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A063511",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 63511,
			"data": "1,2,2,4,4,4,4,4,4,4,4,4,4,4,4,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8",
			"name": "a(n) = a(floor(square root(n))) * 2.",
			"comment": [
				"From _Kevin Ryde_, May 11 2020: (Start)",
				"The sqrt steps in the definition are equivalent to A211667 but here factors of 2 instead of counting, so a(n) = 2^A211667(n).  A211667 is a double logarithm and the effect of power 2^ is to turn the second into a rounding.  So a(n) is the bit length of n (see A070939) increased to the next power of 2 if not already a power of 2.  Each n = 2^(2^k) is a new high a(n) = 2^(k+1), since such an n is bit length 2^k+1.",
				"In a microcomputer, it's common for machine words to be power-of-2 sizes such as 16, 32, 64, 128 bits.  a(n) can be thought of as the word size needed to contain integer n.  Some algorithms by their nature expect power-of-2 sizes, for example Schönhage and Strassen's big integer multiplication.",
				"This sequence differs from A334789 (2^log*(n)) for n\u003e=256.  For example a(256)=16 whereas A334789(256)=8.  The respective exponent sequences are A211667 (for here) and A001069 (for A334789) which likewise differ for n\u003e=256.",
				"(End)"
			],
			"link": [
				"Kevin Ryde, \u003ca href=\"/A063511/b063511.txt\"\u003eTable of n, a(n) for n = 1..8192\u003c/a\u003e",
				"Martin Fürer, \u003ca href=\"http://web.archive.org/web/1id_/http://www.cse.psu.edu/~furer/Papers/mult.pdf\"\u003eFaster integer multiplication\u003c/a\u003e, Proceedings of the 39th Annual ACM Symposium on Theory of Computing, June 11-13 2007.  And \u003ca href=\"http://dx.doi.org/10.1137/070711761\"\u003ein SIAM Journal of Computing\u003c/a\u003e, volume 30, number 3, 2009, pages 979-1005.  (See size \"n\" calculation at the start of Algorithm Integer-Multiplication.)",
				"\u003ca href=\"/index/Di#divseq\"\u003eIndex to divisibility sequences\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 2^A211667(n) = 2^ceiling(log_2(log_2(n+1))). - _Kevin Ryde_, May 11 2020"
			],
			"program": [
				"(PARI) a(n) = if(n==1,1, 2\u003c\u003clogint(logint(n,2),2)); \\\\ _Kevin Ryde_, May 11 2020"
			],
			"xref": [
				"Cf. A001146 (indices of new highs), A334789."
			],
			"keyword": "easy,nonn",
			"offset": "1,2",
			"author": "_Reinhard Zumkeller_, Jul 30 2001",
			"ext": [
				"Formula and code by Charles R Greathouse IV moved to A334789 where they apply. - _Kevin Ryde_, May 11 2020"
			],
			"references": 2,
			"revision": 25,
			"time": "2020-05-26T02:41:33-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}