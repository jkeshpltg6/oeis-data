{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A263073",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 263073,
			"data": "1,1,1,2,2,1,2,3,2,4,4,4,5,6,6,8,9,9,12,12,13,16,18,18,22,24,25,29,32,34,40,43,45,52,56,60,68,74,78,88,95,101,113,122,130,145,156,166,184,198,209,231,249,264,290,311,331,361,388,412,448,480,510,554",
			"name": "Expansion of phi(-x^5) / (chi(-x) * chi(-x^15)) in powers of x where phi(), chi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A263073/b263073.txt\"\u003eTable of n, a(n) for n = 0..2500\u003c/a\u003e",
				"Vaclav Kotesovec, \u003ca href=\"http://arxiv.org/abs/1509.08708\"\u003eA method of finding the asymptotics of q-series based on the convolution of generating functions\u003c/a\u003e, arXiv:1509.08708 [math.CO], Sep 30 2015.",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-2/3) * eta(q^2) * eta(q^5)^2 * eta(q^30) / (eta(q) * eta(q^10) * eta(q^15)) in powers of q.",
				"Euler transform of period 30 sequence [1, 0, 1, 0, -1, 0, 1, 0, 1, -1, 1, 0, 1, 0, 0, 0, 1, 0, 1, -1, 1, 0, 1, 0, -1, 0, 1, 0, 1, -1, ...].",
				"a(n) ~ exp(sqrt(7*n/5)*Pi/3) / (2*sqrt(5*n)). - _Vaclav Kotesovec_, Jul 11 2016"
			],
			"example": [
				"G.f. = 1 + x + x^2 + 2*x^3 + 2*x^4 + x^5 + 2*x^6 + 3*x^7 + 2*x^8 + 4*x^9 + ...",
				"G.f. = q^2 + q^5 + q^8 + 2*q^11 + 2*q^14 + q^17 + 2*q^20 + 3*q^23 + 2*q^26 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 4, 0, x^5] / (QPochhammer[ x, x^2] QPochhammer[ x^15, x^30]), {x, 0, n}];",
				"nmax = 100; CoefficientList[Series[Product[(1+x^k) * (1-x^(5*k)) * (1+x^(15*k)) / (1+x^(5*k)), {k, 1, nmax}], {x, 0, nmax}], x] (* _Vaclav Kotesovec_, Jul 11 2016 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A) * eta(x^5 + A)^2 * eta(x^30 + A) / (eta(x + A) * eta(x^10 + A) * eta(x^15 + A)), n))};",
				"(PARI) q='q+O('q^99); Vec(eta(q^2)*eta(q^5)^2*eta(q^30)/(eta(q)*eta(q^10)*eta(q^15))) \\\\ _Altug Alkan_, Jul 31 2018"
			],
			"keyword": "nonn",
			"offset": "0,4",
			"author": "_Michael Somos_, Oct 08 2015",
			"references": 2,
			"revision": 14,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2015-10-08T21:52:24-04:00"
		}
	]
}