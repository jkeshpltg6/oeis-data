{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A210697",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 210697,
			"data": "1,1,1,2,5,2,9,36,36,9,90,495,855,495,90,2025,14175,34830,34830,14175,2025,102060,867510,2776032,4082400,2776032,867510,102060",
			"name": "Triangle read by rows, arising in study of alternating-sign matrices.",
			"comment": [
				"See Mills et al., pp. 353-354 and 359 for precise definition. As of 1983 no formula was known for these numbers.",
				"These are the values of a bivariate generating function for the ASMs by numbers of entries equal to -1 and by position of 1 in the first row (see Example section). Here weight x=3 is chosen, giving a decomposition of the 3-enumeration of the n X n ASMs.",
				"As a triangle of coefficients of polynomials, A210697 has interesting properties relating the (2n+1)-th row and the n-th row (see Mills et al., p. 359)."
			],
			"link": [
				"W. H. Mills, David P Robbins, Howard Rumsey Jr., \u003ca href=\"http://dx.doi.org/10.1016/0097-3165(83)90068-7\"\u003eAlternating sign matrices and descending plane partitions\u003c/a\u003e J. Combin. Theory Ser. A 34 (1983), no. 3, 340--359. MR0700040 (85b:05013). See p. 359."
			],
			"example": [
				"The bivariate g.f. as a table of polynomials.",
				"(degree of x is the count of -1 entries in the ASM)",
				"Setting x=k gives the k-enumeration of the ASMs",
				"n",
				"1 | 1",
				"2 | 1, 1",
				"3 | 2, 2+x, 2",
				"4 | 6+x, 6+7*x+x^2, 6+7*x+x^2, 6+x",
				"5 | 24 + 16*x + 2*x^2, 24 + 52*x + 26*x^2 + 3*x^3, 24 + 64*x + 38*x^2 +",
				"  |      8*x^3 + x^4, 24 + 52*x + 26*x^2 + 3*x^3, 24 + 16*x + 2*x^2",
				"...",
				"Triangle begins:",
				"n",
				"1 |    1",
				"2 |    1     1",
				"3 |    2     5     2",
				"4 |    9    36    36     9",
				"5 |   90   495   855   495    90",
				"6 | 2025 14175 34830 34830 14175  2025",
				"..."
			],
			"xref": [
				"A048601 is the version for x=1.",
				"As for A048601, the row sums A059477 are equal to the first column, shifted by one."
			],
			"keyword": "nonn,tabl,more",
			"offset": "1,4",
			"author": "_N. J. A. Sloane_, Mar 30 2012",
			"ext": [
				"More terms, definitions and examples by _Olivier Gérard_, Apr 02 2015"
			],
			"references": 2,
			"revision": 18,
			"time": "2016-11-25T05:28:38-05:00",
			"created": "2012-03-30T14:08:16-04:00"
		}
	]
}