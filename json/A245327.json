{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A245327",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 245327,
			"data": "1,1,2,2,3,1,3,3,5,2,5,3,4,1,4,5,8,3,8,5,7,2,7,4,7,3,7,4,5,1,5,8,13,5,13,8,11,3,11,7,12,5,12,7,9,2,9,7,11,4,11,7,10,3,10,5,9,4,9,5,6,1,6,13,21,8,21,13,18,5,18,11,19,8,19,11,14,3,14,12,19,7,19,12,17,5,17,9,16,7,16,9,11,2,11,11,18,7,18,11",
			"name": "Numerators in recursive bijection from positive integers to positive rationals (the bijection is f(1) = 1, f(2n) = 1/(f(n)+1), f(2n+1) = f(n)+1 .",
			"comment": [
				"a(n)/A245328(n) enumerates all the reduced nonnegative rational numbers exactly once.",
				"If the terms (n\u003e0) are written as an array (left-aligned fashion) with rows of length 2^m, m = 0,1,2,3,...",
				"1,",
				"1, 2,",
				"2, 3,1, 3,",
				"3, 5,2, 5,3, 4,1, 4,",
				"5, 8,3, 8,5, 7,2, 7,4, 7,3, 7,4,5,1,5,",
				"8,13,5,13,8,11,3,11,7,12,5,12,7,9,2,9,7,11,4,11,7,10,3,10,5,9,4,9,5,6,1,6,",
				"then the sum of the m-th row is 3^m (m = 0,1,2,), and each column k is a Fibonacci sequence.",
				"If the rows are written in a right-aligned fashion:",
				"                                                                        1,",
				"                                                                      1,2,",
				"                                                                  2,3,1,3,",
				"                                                          3,5,2,5,3,4,1,4,",
				"                                      5, 8,3, 8,5, 7,2, 7,4,7,3,7,4,5,1,5,",
				"8,13,5,13,8,11,3,11,7,12,5,12,7,9,2,9,7,11,4,11,7,10,3,10,5,9,4,9,5,6,1,6,",
				"then each column is an arithmetic sequence.",
				"If the sequence is considered by blocks of length 2^m, m = 0,1,2,..., the blocks of this sequence are permutations of terms of blocks from A002487 (Stern's diatomic series or Stern-Brocot sequence), and, more precisely, the reverses of blocks of A020650 ( a(2^m+k) = A020650(2^(m+1)-1-k), m = 0,1,2,..., k = 0,1,2,...,2^m-1).",
				"Moreover, each block is the bit-reversed permutation of the corresponding block of A245325."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A245327/b245327.txt\"\u003eTable of n, a(n) for n = 1..16383\u003c/a\u003e, rows 1-14, flattened.",
				"\u003ca href=\"/index/Fo#fraction_trees\"\u003eIndex entries for fraction trees\u003c/a\u003e"
			],
			"formula": [
				"a(2n) = A245328(2n+1) , a(2n+1) = A245328(2n) , n=0,1,2,3,...",
				"a((2*n+1)*2^m - 2) = A273493(n), n \u003e 0, m \u003e 0. For n = 0, m \u003e 0, A273493(0) = 1 is needed. For n = 1, m = 0, A273493(0) = 1 is needed. For n \u003e 1, m = 0, numerator((2*n-1) = num+den(n-1). - _Yosu Yurramendi_, Mar 02 2017",
				"a(n) = A002487(A284459(n)). - _Yosu Yurramendi_, Aug 23 2021"
			],
			"mathematica": [
				"f[n_] := Which[n == 1, 1, EvenQ@ n, 1/(f[n/2] + 1), True, f[(n - 1)/2] + 1]; Table[Numerator@ f@ k, {n, 7}, {k, 2^(n - 1), 2^n - 1}] // Flatten (* _Michael De Vlieger_, Mar 02 2017 *)"
			],
			"program": [
				"(R)",
				"N  \u003c- 25 # arbitrary",
				"a \u003c- c(1,1,2)",
				"for(n in 1:N){",
				"  a[4*n]   \u003c-          a[2*n+1]",
				"  a[4*n+1] \u003c- a[2*n] + a[2*n+1]",
				"  a[4*n+2] \u003c- a[2*n]",
				"  a[4*n+3] \u003c- a[2*n] + a[2*n+1]",
				"}",
				"a"
			],
			"xref": [
				"Cf. A002487, A020651, A245325, A245328, A273493."
			],
			"keyword": "nonn,frac",
			"offset": "1,3",
			"author": "_Yosu Yurramendi_, Jul 18 2014",
			"references": 11,
			"revision": 22,
			"time": "2021-09-18T15:57:02-04:00",
			"created": "2014-07-18T22:55:11-04:00"
		}
	]
}