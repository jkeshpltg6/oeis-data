{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A064868",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 64868,
			"data": "2344,172,131,174,52,77,75,83,75,81,89,95,101,104,110,133,143,127,133,119,124,129,134,139,144,149,154,159,164,169,174,179,184,189,194,199,204,209,214,219,224,229,234,238,243,248,253,258,263,268,273,278,283",
			"name": "The minimal number which has multiplicative persistence 4 in base n.",
			"comment": [
				"The persistence of a number is the number of times you need to multiply the digits together before reaching a single digit. a(3) and a(4) do not seem to exist."
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A064868/b064868.txt\"\u003eTable of n, a(n) for n = 5..10000\u003c/a\u003e",
				"M. R. Diamond and D. D. Reidpath, \u003ca href=\"http://www.mathe2.uni-bayreuth.de/sascha/oeis/persistence/PERSIST.PDF\"\u003eA counterexample to a conjecture of Sloane and Erdos\u003c/a\u003e, J. Recreational Math., 1998 29(2), 89-92.",
				"Sascha Kurz, \u003ca href=\"http://www.mathe2.uni-bayreuth.de/sascha/oeis/persistence/persistence.html\"\u003ePersistence in different bases\u003c/a\u003e",
				"T. Lamont-Smith, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL24/Lamont/lamont5.html\"\u003eMultiplicative Persistence and Absolute Multiplicative Persistence\u003c/a\u003e, J. Int. Seq., Vol. 24 (2021), Article 21.6.7.",
				"C. Rivera, \u003ca href=\"http://www.primepuzzles.net/puzzles/puzz_022.htm\"\u003eMinimal prime with persistence p\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"http://neilsloane.com/doc/persistence.html\"\u003eThe persistence of a number\u003c/a\u003e, J. Recreational Math., 6 (1973), 97-98.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/MultiplicativePersistence.html\"\u003eMultiplicative Persistence\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 5*n-floor(n/24) for n \u003e 23."
			],
			"example": [
				"a(6) = 172 because 172 = [444]-\u003e[144]-\u003e[24]-\u003e[12]-\u003e[2] and no fewer n has persistence 4 in base 6."
			],
			"mathematica": [
				"With[{m = 4, r = 24}, Table[Block[{k = 1}, While[Length@ FixedPointList[Times @@ IntegerDigits[#, n] \u0026, k] != m + 2, k++]; k], {n, m + 1, r}]~Join~Array[(m + 1) # - Floor[#/r] \u0026, 34, r + 1]] (* _Michael De Vlieger_, Aug 30 2021 *)"
			],
			"program": [
				"(PARI) pers(nn, b) = {ok = 0; p = 0; until (ok, d = digits(nn, b); if (#d == 1, ok = 1, p++); nn = prod(k=1, #d, d[k]); if (nn == 0, ok = 1);); return (p);}",
				"a(n) = {i=0; while (pers(i, n) != 4, i++); return (i);} \\\\ _Michel Marcus_, Jun 30 2013"
			],
			"xref": [
				"Cf. A003001, A031346, A064867, A064869, A064870, A064871, A064872."
			],
			"keyword": "base,easy,nonn",
			"offset": "5,1",
			"author": "_Sascha Kurz_, Oct 09 2001",
			"references": 11,
			"revision": 19,
			"time": "2021-08-30T21:51:06-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}