{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A080637",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 80637,
			"data": "2,3,5,6,7,9,11,12,13,14,15,17,19,21,23,24,25,26,27,28,29,30,31,33,35,37,39,41,43,45,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,65,67,69,71,73,75,77,79,81,83,85,87,89,91,93,95,96,97,98,99,100,101,102",
			"name": "a(n) is smallest positive integer which is consistent with sequence being monotonically increasing and satisfying a(1)=2, a(a(n)) = 2n+1 for n\u003e1.",
			"comment": [
				"Sequence is unique monotonic sequence satisfying a(1)=2, a(a(n)) = 2n+1 for n\u003e1."
			],
			"link": [
				"B. Cloitre, N. J. A. Sloane and M. J. Vandermast, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL6/Cloitre/cloitre2.html\"\u003eNumerical analogues of Aronson's sequence\u003c/a\u003e, J. Integer Seqs., Vol. 6 (2003), #03.2.2.",
				"B. Cloitre, N. J. A. Sloane and M. J. Vandermast, \u003ca href=\"https://arxiv.org/abs/math/0305308\"\u003eNumerical analogues of Aronson's sequence\u003c/a\u003e, arXiv:math/0305308 [math.NT], 2003.",
				"Hsien-Kuei Hwang, S. Janson, T.-H. Tsai, \u003ca href=\"http://140.109.74.92/hk/wp-content/files/2016/12/aat-hhrr-1.pdf\"\u003eExact and asymptotic solutions of the recurrence f(n) = f(floor(n/2)) + f(ceiling(n/2)) + g(n): theory and applications\u003c/a\u003e, Preprint, 2016.",
				"Hsien-Kuei Hwang, S. Janson, T.-H. Tsai, \u003ca href=\"https://doi.org/10.1145/3127585\"\u003eExact and Asymptotic Solutions of a Divide-and-Conquer Recurrence Dividing at Half: Theory and Applications\u003c/a\u003e, ACM Transactions on Algorithms, 13:4 (2017), #47; DOI: 10.1145/3127585.",
				"\u003ca href=\"/index/Aa#aan\"\u003eIndex entries for sequences of the a(a(n)) = 2n family\u003c/a\u003e"
			],
			"formula": [
				"a(3*2^k - 1 + j) = 4*2^k - 1 + 3j/2 + |j|/2 for k \u003e= 0, -2^k \u003c= j \u003c 2^k.",
				"a(2n+1) = 2*a(n) + 1, a(2n) = a(n) + a(n-1) + 1."
			],
			"maple": [
				"t := []; for k from 0 to 6 do for j from -2^k to 2^k-1 do t := [op(t), 4*2^k - 1 + 3*j/2 + abs(j)/2]; od: od: t;"
			],
			"mathematica": [
				"b[n_] := b[n] = If[n\u003c4, n+1, If[OddQ[n], b[(n-1)/2+1]+b[(n-1)/2], 2b[n/2]]];",
				"a[n_] := b[n+1]-1;",
				"a /@ Range[70] (* _Jean-François Alcover_, Oct 31 2019 *)"
			],
			"xref": [
				"Except for first term, same as A079905. Cf. A079000.",
				"A007378, A079905, A080637, A080653 are all essentially the same sequence.",
				"Equals A007378(n+1)-1. First differences give A079882."
			],
			"keyword": "nonn,easy",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_ and _Benoit Cloitre_, Feb 28 2003",
			"references": 5,
			"revision": 20,
			"time": "2019-10-31T06:19:48-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}