{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A052762",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 52762,
			"data": "0,0,0,0,24,120,360,840,1680,3024,5040,7920,11880,17160,24024,32760,43680,57120,73440,93024,116280,143640,175560,212520,255024,303600,358800,421200,491400,570024,657720,755160,863040,982080,1113024",
			"name": "Products of 4 consecutive integers: a(n) = n*(n-1)*(n-2)*(n-3).",
			"comment": [
				"Also, starting with n=4, the square of area of cyclic quadrilateral with sides n, n-1, n-2, n-3. - _Zak Seidov_, Jun 20 2003",
				"a(n) + 1 = A062938(n-4) for n \u003e 4. - _Amarnath Murthy_, Dec 13 2003",
				"For n \u003e 4: a(n) = A173333(n, n-4). - _Reinhard Zumkeller_, Feb 19 2010",
				"Number of n-colorings of the complete graph on 4 vertices, which is also the tetrahedral graph. - _Eric M. Schmidt_, Oct 31 2012",
				"Cf. A130534 for relations to colored forests and disposition of flags on flagpoles. - _Tom Copeland_, Apr 05 2014",
				"Number of 4-permutations of the set {1, 2, ..., n}. - _Joerg Arndt_, Apr 05 2014"
			],
			"link": [
				"Eric M. Schmidt, \u003ca href=\"/A052762/b052762.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=719\"\u003eEncyclopedia of Combinatorial Structures 719\u003c/a\u003e",
				"Luis Manuel Rivera, \u003ca href=\"http://arxiv.org/abs/1406.3081\"\u003eInteger sequences and k-commuting permutations\u003c/a\u003e, arXiv:1406.3081 [math.CO], 2014.",
				"Michelle Rudolph-Lilith, \u003ca href=\"http://arxiv.org/abs/1508.07894\"\u003eOn the Product Representation of Number Sequences, with Application to the Fibonacci Family\u003c/a\u003e, arXiv:1508.07894 [math.NT], 2015.",
				"Eric Weisstein, \u003ca href=\"http://mathworld.wolfram.com/notebooks/PlaneGeometry/CyclicQuadrilateral.nb\"\u003eCyclicQuadrilateral\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Pochhammer_symbol\"\u003ePochhammer symbol\u003c/a\u003e.",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (5, -10, 10, -5, 1)."
			],
			"formula": [
				"a(n) = n*(n-1)*(n-2)*(n-3) = n!/(n-4)! (for n \u003e= 4).",
				"a(n) = A001094(n) - n.",
				"E.g.f.: x^4*exp(x).",
				"Recurrence: {a(1)=0, a(2)=0, a(3)=0, a(4)=24, (-1-n)*a(n) + (n-3)*a(n+1)}.",
				"a(n) = numbperm(n, 4). - _Zerinvary Lajos_, Apr 26 2007",
				"O.g.f.: -24*x^4/(-1+x)^5. - _R. J. Mathar_, Nov 23 2007",
				"a(n) = 5*a(n-1) - 10*a(n-2) + 10*a(n-3) - 5*a(n-4) + a(n-5), with a(0)=0, a(1)=0, a(2)=0, a(3)=0, a(4)=24. - _Harvey P. Dale_, May 09 2012",
				"a(n) = a(n-1) + 4*A007531(n). - _J. M. Bergot_, May 30 2012",
				"a(n) = (n)_4 = Pochhammer(n,4), using the \"falling factorial\" convention; other authors write Pochhammer(x,k) for what is denoted x^(k) in the Wikipedia article, then a(n) = (n-3)^(4). - _M. F. Hasler_, Oct 20 2013",
				"a(n) - 1 = A069756(n-2) for n \u003e= 4. - _Jean-Christophe Hervé_, Nov 01 2015",
				"a(n) = 24 * A000332(n). - _Bruce J. Nicholson_, Apr 03 2017",
				"Sum_{n\u003e=4} 24*(-1)^n/a(n) = A242023. Sum_{n\u003e=4) 1/a(n) = 1/18. - _R. J. Mathar_, Jun 30 2021"
			],
			"maple": [
				"spec := [S,{B=Set(Z),S=Prod(Z,Z,Z,Z,B)},labeled]: seq(combstruct[count](spec,size=n), n=0..20);",
				"seq(numbperm (n,4), n=0..34); # _Zerinvary Lajos_, Apr 26 2007",
				"G(x):=x^4*exp(x): f[0]:=G(x): for n from 1 to 34 do f[n]:=diff(f[n-1],x) od: x:=0: seq(f[n],n=0..34); # _Zerinvary Lajos_, Apr 05 2009"
			],
			"mathematica": [
				"Table[n*(n+1)*(n+2)*(n+3), {n,-3,60}] (* _Vladimir Joseph Stephan Orlovsky_, Apr 21 2010 *)",
				"Times@@@Partition[Range[-3,60], 4, 1] (* _Harvey P. Dale_, May 09 2012 *)",
				"LinearRecurrence[ {5,-10,10,-5,1}, {0,0,0,0,24}, 60] (* _Harvey P. Dale_, May 09 2012 *)"
			],
			"program": [
				"(PARI) a(n)=24*binomial(n,4) \\\\ _Charles R Greathouse IV_, Nov 20 2011",
				"(Maxima) A052762(n):=n*(n-1)*(n-2)*(n-3)$",
				"makelist(A052762(n),n,0,30); /* _Martin Ettl_, Nov 03 2012 */",
				"(MAGMA) [n*(n-1)*(n-2)*(n-3): n in [0..30]]; \\\\ _G. C. Greubel_, Nov 19 2017"
			],
			"xref": [
				"Cf. A001094, A002378, A007531, A052787."
			],
			"keyword": "easy,nonn",
			"offset": "0,5",
			"author": "encyclopedia(AT)pommard.inria.fr, Jan 25 2000",
			"ext": [
				"More terms from _Henry Bottomley_, Mar 20 2000",
				"Formula corrected by _Philippe Deléham_, Dec 12 2003"
			],
			"references": 33,
			"revision": 90,
			"time": "2021-06-30T10:58:42-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}