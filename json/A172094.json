{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A172094",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 172094,
			"data": "1,1,1,3,4,1,11,17,7,1,45,76,40,10,1,197,353,216,72,13,1,903,1688,1145,458,113,16,1,4279,8257,6039,2745,829,163,19,1,20793,41128,31864,15932,5558,1356,222,22,1,103049,207905,168584,90776,35318,10070,2066,290,25,1",
			"name": "The Riordan square of the little Schröder numbers A001003.",
			"comment": [
				"The Riordan square is defined in A321620.",
				"Previous name was: Triangle, read by rows, given by [1,2,1,2,1,2,1,2,1,2,1,2,...] DELTA [1,0,0,0,0,0,0,0,...] where DELTA is the operator defined in A084938.",
				"Riordan array (f(x), f(x)-1) where f(x) is the g.f. of A001003. Equals A122538*A007318."
			],
			"link": [
				"P. Barry, \u003ca href=\"http://arxiv.org/abs/1311.7161\"\u003eComparing two matrices of generalized moments defined by continued fraction expansions\u003c/a\u003e, arXiv preprint arXiv:1311.7161 [math.CO], 2013 and \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Barry3/barry291.html\"\u003eJ. Int. Seq. 17 (2014) # 14.5.1\u003c/a\u003e.",
				"Johann Cigler, \u003ca href=\"https://arxiv.org/abs/1611.05252\"\u003eSome elementary observations on Narayana polynomials and related topics\u003c/a\u003e, arXiv:1611.05252 [math.CO], 2016. See p. 12.",
				"E. Deutsch, L. Ferrari and S. Rinaldi, \u003ca href=\"http://arxiv.org/abs/math/0702638\"\u003eProduction Matrices and Riordan arrays\u003c/a\u003e, arXiv:math/0702638 [math.CO], 2007.",
				"Sheng-Liang Yang, Yan-Ni Dong, and Tian-Xiao He, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2017.07.006\"\u003eSome matrix identities on colored Motzkin paths\u003c/a\u003e, Discrete Mathematics 340.12 (2017): 3081-3091."
			],
			"formula": [
				"T(0, 0) = 1, T(n, k) = 0 if k\u003en, T(n, 0) = T(n-1, 0) + 2*T(n-1, 1), T(n, k) = T(n-1, k-1) + 3*T(n-1, k) + 2*T(n-1, k+1) for k\u003e0.",
				"Sum_{0\u003c=k\u003c=n} T(n, k) = A109980(n).",
				"Sum_{k\u003e=0} T(m, k)*T(n, k)*2^k = T(m+n, 0) = A001003(m+n)."
			],
			"example": [
				"Triangle begins:",
				"     1",
				"     1,      1",
				"     3,      4,      1",
				"    11,     17,      7,     1",
				"    45,     76,     40,    10,     1",
				"   197,    353,    216,    72,    13,     1",
				"   903,   1688,   1345,   458,   113,    16,    1",
				"  4279,   8257,   6039,  2745,   829,   163,   19,   1",
				"20793,  41128,  31864, 15932,  5558,  1356,  222,  22,  1",
				"103049, 207905, 168584, 90776, 35318, 10070, 2066, 290, 25, 1",
				".",
				"Production matrix begins:",
				"1, 1",
				"2, 3, 1",
				"0, 2, 3, 1",
				"0, 0, 2, 3, 1",
				"0, 0, 0, 2, 3, 1",
				"0, 0, 0, 0, 2, 3, 1",
				"0, 0, 0, 0, 0, 2, 3, 1",
				"0, 0, 0, 0, 0, 0, 2, 3, 1",
				"... - _Philippe Deléham_, Sep 24 2014"
			],
			"mathematica": [
				"DELTA[r_, s_, m_] := Module[{p, q, t, x, y}, q[k_] := x r[[k+1]] + y s[[k+1]]; p[0, _] = 1; p[_, -1] = 0; p[n_ /; n \u003e= 1, k_ /; k \u003e= 0] := p[n, k] = p[n, k-1] + q[k] p[n-1, k+1] // Expand; t[n_, k_] := Coefficient[p[n, 0], x^(n-k)*y^k]; t[0, 0] = p[0, 0]; Table[t[n, k], {n, 0, m}, {k, 0, n}]];",
				"nmax = 9;",
				"DELTA[Table[{1, 2}, (nmax+1)/2] // Flatten, Prepend[Table[0, {nmax}], 1], nmax] // Flatten (* _Jean-François Alcover_, Aug 07 2018 *)",
				"(* Function RiordanSquare defined in A321620. *)",
				"RiordanSquare[(1 + x - Sqrt[1 - 6x + x^2])/(4x), 11] // Flatten  (* _Peter Luschny_, Nov 27 2018 *)"
			],
			"xref": [
				"T(n, 0) = A001003(n) (little Schröder), A109980 (row sums).",
				"Diagonals: A239204, A000012, A016777.",
				"Cf. A122538, A007318, A321620."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Philippe Deléham_, Jan 25 2010",
			"ext": [
				"New name by _Peter Luschny_, Nov 27 2018"
			],
			"references": 3,
			"revision": 35,
			"time": "2018-11-27T19:27:16-05:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}