{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A117938",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 117938,
			"data": "1,1,1,1,2,3,1,3,6,4,1,4,11,14,7,1,5,18,36,34,11,1,6,27,76,119,82,18,1,7,38,140,322,393,198,29,1,8,51,234,727,1364,1298,478,47,1,9,66,364,1442,3775,5778,4287,1154,76,1,10,83,536,2599,8886,19602,24476,14159,2786,123",
			"name": "Triangle, columns generated from Lucas Polynomials.",
			"comment": [
				"Companion triangle using Fibonacci polynomial generators = A073133. Inverse binomial transforms of the columns defines rows of A117937 (with some adjustments of offset).",
				"A309220 is another version of the same triangle (except it omits the last diagonal), and perhaps has a clearer definition. - _N. J. A. Sloane_, Aug 13 2019"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A117938/b117938.txt\"\u003eRows n = 1..50 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"Columns are f(x), x = 1, 2, 3, ..., of the Lucas Polynomials: (1, defined different from A034807 and A114525); (x); (x^2 + 2); (x^3 + 3*x); (x^4 + 4*x^2 + 2); (x^5 + 5*x^3 + 5*x); (x^6 + 6*x^4 + 9*x^2 + 2); (x^7 + 7*x^5 + 14*x^3 + 7*x); ..."
			],
			"example": [
				"First few rows of the triangle are:",
				"  1;",
				"  1, 1;",
				"  1, 2,  3;",
				"  1, 3,  6,   4;",
				"  1, 4, 11,  14,   7;",
				"  1, 5, 18,  36,  34,  11;",
				"  1, 6, 27,  76, 119,  82,  18;",
				"  1, 7, 38, 140, 322, 393, 198, 29;",
				"  ...",
				"For example, T(7,4) = 76 = f(4), x^3 + 3*x = 64 + 12 = 76."
			],
			"maple": [
				"Lucas := proc(n,x) # see A114525",
				"    option remember;",
				"    if  n=0 then",
				"        2;",
				"    elif n =1 then",
				"        x ;",
				"    else",
				"        x*procname(n-1,x)+procname(n-2,x) ;",
				"    end if;",
				"    expand(%) ;",
				"end proc:",
				"A117938 := proc(n::integer,k::integer)",
				"    if k = 1 then",
				"        1;",
				"    else",
				"        subs(x=n-k+1,Lucas(k-1,x)) ;",
				"    end if;",
				"end proc:",
				"seq(seq(A117938(n,k),k=1..n),n=1..12) ; # _R. J. Mathar_, Aug 16 2019"
			],
			"mathematica": [
				"T[n_, k_]:= LucasL[k-1, n-k+1] - Boole[k==1];",
				"Table[T[n, k], {n, 12}, {k, n}]//Flatten (* _G. C. Greubel_, Oct 28 2021 *)"
			],
			"program": [
				"(Sage)",
				"def A117938(n,k): return 1 if (k==1) else round(2^(1-k)*( (n-k+1 + sqrt((n-k)*(n-k+2) + 5))^(k-1) + (n-k+1 - sqrt((n-k)*(n-k+2) + 5))^(k-1) ))",
				"flatten([[A117938(n,k) for k in (1..n)] for n in (1..12)]) # _G. C. Greubel_, Oct 28 2021"
			],
			"xref": [
				"Cf. A104509, A114525, A117936, A117937, A118980, A118981, A309220.",
				"Cf. A000204 (diagonal), A059100 (column 3), A061989 (column 4)."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "1,5",
			"author": "_Gary W. Adamson_, Apr 03 2006",
			"ext": [
				"Terms a(51) and a(52) corrected by _G. C. Greubel_, Oct 28 2021"
			],
			"references": 5,
			"revision": 21,
			"time": "2021-10-29T11:37:02-04:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}