{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A156601",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 156601,
			"data": "1,1,1,1,-6,1,1,35,35,1,1,-204,1190,-204,1,1,1189,40426,40426,1189,1,1,-6930,1373295,-8004348,1373295,-6930,1,1,40391,46651605,1584821667,1584821667,46651605,40391,1,1,-235416,1584781276,-313786692648,1828884203718,-313786692648,1584781276,-235416,1",
			"name": "Triangle T(n, k, m) = t(n, m)/(t(k, m)*t(n-k, m)), where t(n, k) = Product_{j=1..n} p(j, k+1), p(n, x) = Sum_{j=0..n} (-1)^j*A053122(n, j)*x^j, and m = 7, read by rows.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A156601/b156601.txt\"\u003eRows n = 0..50 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k, m) = t(n, m)/(t(k, m)*t(n-k, m)), where t(n, k) = Product_{j=1..n} p(j, k+1), p(n, x) = Sum_{j=0..n} (-1)^j*A053122(n, j)*x^j, and m = 7."
			],
			"example": [
				"Triangle begins as:",
				"  1;",
				"  1,     1;",
				"  1,    -6,        1;",
				"  1,    35,       35,          1;",
				"  1,  -204,     1190,       -204,          1;",
				"  1,  1189,    40426,      40426,       1189,        1;",
				"  1, -6930,  1373295,   -8004348,    1373295,    -6930,     1;",
				"  1, 40391, 46651605, 1584821667, 1584821667, 46651605, 40391, 1;"
			],
			"mathematica": [
				"(* First program *)",
				"b[n_, k_]:= If[k==n, 2, If[k==n-1 || k==n+1, -1, 0]];",
				"M[d_]:= Table[b[n, k], {n,d}, {k,d}];",
				"p[x_, n_]:= If[n==0, 1, CharacteristicPolynomial[M[n], x]];",
				"f= Table[p[x, n], {n,0,20}];",
				"t[n_, k_]:= If[k==0, n!, Product[f[[j]], {j, n}]/.x-\u003e(k+1)];",
				"T[n_, k_, m_]:= If[n==0, 1, t[n, m]/(t[k, m]*t[n-k, m])];",
				"Table[T[n, k, 7], {n,0,12}, {k,0,n}]//TableForm (* modified by _G. C. Greubel_, Jun 25 2021 *)",
				"(* Second program *)",
				"t[n_, k_]:= t[n, k]= If[n==0, 1, If[k==0, (n-1)!, Product[(-1)^j*Simplify[ChebyshevU[j, x/2 - 1]], {j,0,n-1}]/.x-\u003e(k+1)]];",
				"T[n_, k_, m_]:= T[n, k, m]= t[n, m]/(t[k, m]*t[n-k, m]);",
				"Table[T[n, k, 7], {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, Jun 25 2021 *)"
			],
			"program": [
				"(Sage)",
				"@CachedFunction",
				"def t(n, k):",
				"    if (n==0): return 1",
				"    elif (k==0): return factorial(n-1)",
				"    else: return product( (-1)^j*chebyshev_U(j, (k-1)/2) for j in (0..n-1) )",
				"def T(n,k,m): return t(n,m)/(t(k,m)*t(n-k,m))",
				"flatten([[T(n, k, 7) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Jun 25 2021"
			],
			"xref": [
				"Cf. A007318 (m=0), A034801 (m=4), A156599 (m=5), A156600 (m=6), this sequence (m=7), A156602 (m=8), A156603.",
				"Cf. A053122."
			],
			"keyword": "sign,tabl",
			"offset": "0,5",
			"author": "_Roger L. Bagula_, Feb 11 2009",
			"ext": [
				"Edited by _G. C. Greubel_, Jun 25 2021"
			],
			"references": 5,
			"revision": 5,
			"time": "2021-06-25T23:14:57-04:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}