{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A051000",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 51000,
			"data": "1,1,28,1,126,28,344,1,757,126,1332,28,2198,344,3528,1,4914,757,6860,126,9632,1332,12168,28,15751,2198,20440,344,24390,3528,29792,1,37296,4914,43344,757,50654,6860,61544,126,68922,9632,79508,1332,95382,12168,103824,28",
			"name": "Sum of cubes of odd divisors of n.",
			"comment": [
				"The sum of cubes of even divisors of 2*k equals 8*A001158(k), and the sum of cubes of even divisors of 2*k-1 vanishes, for k \u003e= 1. - _Wolfdieter Lang_, Jan 07 2017",
				"Sum_{k\u003e=1} 1/a(k) diverges. - _Vaclav Kotesovec_, Sep 21 2020"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A051000/b051000.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"J. W. L. Glaisher, \u003ca href=\"https://books.google.com/books?id=bLs9AQAAMAAJ\u0026amp;pg=RA1-PA1\"\u003eOn the representations of a number as the sum of two, four, six, eight, ten, and twelve squares\u003c/a\u003e, Quart. J. Math. 38 (1907), 1-62 (see p. 4 and p. 8).",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/OddDivisorFunction.html\"\u003eOdd Divisor Function\u003c/a\u003e.",
				"\u003ca href=\"/index/Ge#Glaisher\"\u003eIndex entries for sequences mentioned by Glaisher\u003c/a\u003e"
			],
			"formula": [
				"Dirichlet g.f.: (1-2^(3-s))*zeta(s)*zeta(s-3). Dirichlet convolution of (-1)^n*A176415(n) and A000578. - _R. J. Mathar_, Apr 06 2011",
				"a(n) = Sum_{k=1..A001227(n)} A182469(n,k)^3. - _Reinhard Zumkeller_, May 01 2012",
				"G.f.: Sum_{k\u003e=1} (2*k - 1)^3*x^(2*k-1)/(1 - x^(2*k-1)). - _Ilya Gutkovskiy_, Jan 04 2017",
				"Sum_{k=1..n} a(k) ~ Pi^4 * n^4 / 720. - _Vaclav Kotesovec_, Jan 31 2019",
				"Multiplicative with a(2^e) = 1 and a(p^e) = (p^(3*e+3)-1)/(p^3-1) for p \u003e 2. - _Amiram Eldar_, Sep 14 2020",
				"For k\u003e=0, a(2^k) = 1. - _Vaclav Kotesovec_, Sep 21 2020",
				"G.f.: Sum_{n \u003e= 1} x^n*(1 + 23*x^(2*n) + 23*x^(4*n) + x^(6*n))/(1 - x^(2*n))^4. See row 4 of A060187. - _Peter Bala_, Dec 20 2021"
			],
			"mathematica": [
				"Table[Total[Select[Divisors[n],OddQ]^3],{n,50}] (* _Harvey P. Dale_, Jun 28 2012 *)",
				"f[2, e_] := 1; f[p_, e_] := (p^(3*e + 3) - 1)/(p^3 - 1); a[1] = 1; a[n_] := Times @@ f @@@ FactorInteger[n]; Array[a, 100] (* _Amiram Eldar_, Sep 14 2020 *)"
			],
			"program": [
				"(Haskell)",
				"a051000 = sum . map (^ 3) . a182469_row",
				"-- _Reinhard Zumkeller_, May 01 2012",
				"(PARI) a(n) = sumdiv(n, d, (d%2)*d^3); \\\\ _Michel Marcus_, Jan 04 2017"
			],
			"xref": [
				"Cf. A000593, A001227, A001158, A050999, A051001, A051002."
			],
			"keyword": "nonn,mult",
			"offset": "1,3",
			"author": "_Eric W. Weisstein_",
			"references": 20,
			"revision": 49,
			"time": "2021-12-21T07:41:30-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}