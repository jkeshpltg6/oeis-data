{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A228072",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 228072,
			"data": "1,8,-10,16,37,-40,-50,-80,-30,40,128,48,-25,80,-34,320,-320,-160,310,-400,410,152,-370,-416,-87,-240,-410,400,320,-200,30,592,500,776,384,400,-630,-200,-640,-1120,-359,552,300,-272,-326,-800,2560,-400,-110",
			"name": "Expansion of psi(x^2)^2 * phi(-x^2)^6 + 8 * x * psi(x^2)^6 * phi(-x^2)^2 in powers of x where phi(), psi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A228072/b228072.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Hossein Movasati, Younes Nikdelan, \u003ca href=\"https://arxiv.org/abs/1803.01414\"\u003eProduct formulas for weight two newforms\u003c/a\u003e, arXiv:1803.01414 [math.NT], 2018.",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of q^(-1/2) * ((eta(q^2)^5 / eta(q^4))^2 + 8 * (eta(q^4)^5 / eta(q^2))^2) in powers of q.",
				"Expansion of q^(-1/2) * (eta(q^2)^12 + 8 * eta(q^4)^12) / ( eta(q^2) * eta(q^4) )^2 in powers of q.",
				"a(n) = b(2*n + 1) where b(n) is multiplicative with b(2^e) = 0^e, b(p^e) = b(p) * b(p^(e-1)) - p^3 * b(p^(e-2)) if p\u003e2.",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (8 t)) = 8^2 (t / i)^4 f(t) where q = exp(2 Pi i t).",
				"a(2*n) = A227695(n). a(2*n + 1) = 8 * A227317(n).",
				"If F(x) is the g.f. for A002171, then A(x) * F(x^2) = B(x) the g.f. for A227239. - _Michael Somos_, Jan 08 2015"
			],
			"example": [
				"G.f. = 1 + 8*x - 10*x^2 + 16*x^3 + 37*x^4 - 40*x^5 - 50*x^6 - 80*x^7 - 30*x^8 + ...",
				"G.f. = q + 8*q^3 - 10*q^5 + 16*q^7 + 37*q^9 - 40*q^11 - 50*q^13 - 80*q^15 - 30*q^17 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (QPochhammer[ x^2]^12 + 8 x QPochhammer[ x^4]^12) / (QPochhammer[ x^2] QPochhammer[ x^4])^2, {x, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c0, 0, A = x * O(x^n);polcoeff( (eta(x^2 + A)^5 / eta(x^4 + A))^2 + 8 * x * (eta(x^4 + A)^5 / eta(x^2 + A))^2, n))};"
			],
			"xref": [
				"Cf. A002171, A227239, A227317, A227695."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Sep 02 2013",
			"references": 1,
			"revision": 32,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2013-09-02T12:33:16-04:00"
		}
	]
}