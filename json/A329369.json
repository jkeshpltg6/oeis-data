{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A329369",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 329369,
			"data": "1,1,3,1,7,3,7,1,15,7,17,3,31,7,15,1,31,15,37,7,69,17,37,3,115,31,69,7,115,15,31,1,63,31,77,15,145,37,81,7,245,69,155,17,261,37,77,3,391,115,261,31,445,69,145,7,675,115,245,15,391,31,63,1,127,63",
			"name": "Number of permutations of {1,2,...,m} with excedance set constructed by taking m-i (0 \u003c i \u003c m) if b(i-1) = 1 where b(k)b(k-1)...b(1)b(0) (0 \u003c= k \u003c m-1) is the binary expansion of n.",
			"comment": [
				"Another version of A152884.",
				"The excedance set of a permutation p of {1,2,...,m} is the set of indices i such that p(i) \u003e i; it is a subset of {1,2,...,m-1}.",
				"Great work on this subject was done by R. Ehrenborg and E. Steingrimsson, so most of the formulas given below are just their results translated into the language of binary sequences.",
				"Equivalently, number of open tours by a biased rook on a specific f(n) X 1 board, which ends on a white cell, where f(n) = A070941(n) = floor(log_2(2n)) + 1 and cells are colored white or black according to the binary representation of 2n. A cell is colored white if the binary digit is 0 and a cell is colored black if the binary digit is 1. A biased rook on a white cell moves only to the left and otherwise moves only to the right. - _Mikhail Kurkov_, May 18 2021",
				"Inverse modulo 2 binomial transform of A284005. Combinatorial interpretation: note that binomial(n, k) mod 2 = 1 if k belongs to A295989(n, j) which is all possible numbers resulting from changing ones to zeros in binary expansion of n. Write out all open tours by a biased rook from A284005 (see the \"Comments on A284005\" for definition) for those numbers, exclude tours which have a duplicate and you will get open tours by a biased rook from A329369. - _Mikhail Kurkov_, Dec 15 2021"
			],
			"link": [
				"Mikhail Kurkov, \u003ca href=\"/A329369/b329369.txt\"\u003eTable of n, a(n) for n = 0..8191\u003c/a\u003e",
				"R. Ehrenborg and E. Steingrimsson, \u003ca href=\"http://dx.doi.org/10.1006/aama.1999.0671\"\u003eThe excedance set of a permutation\u003c/a\u003e, Advances in Appl. Math., 24, 284-299, 2000."
			],
			"formula": [
				"a(2n+1) = a(n) for n \u003e= 0.",
				"a(2n) = a(n) + a(n - 2^f(n)) + a(2n - 2^f(n)) for n \u003e 0 with a(0)=1 where f(n) = A007814(n).",
				"a(2^m*(2n+1)) = Sum_{k=0..m} binomial(m+1,k) a(2^k*n) = a(2^m*n) + a(2^(m-1)*(2n+1)) + a(2^(m-1)*(4n+1)) for m \u003e 0, n \u003e= 0.",
				"a(2n) = a(2*f(n)) + a(2n - 2^g(n)) + a(2*f(n) + 2^g(n)) for n \u003e 0 with a(0)=1 where f(n) = A053645(n), g(n) = A063250(n).",
				"a(2n) = 2*a(n + f(n)) + a(2*f(n)) for n \u003e 0, floor(n/3) \u003c 2^(floor(log_2(n))-1) (in other words, for 2^m + k where 0 \u003c= k \u003c 2^(m-1), m \u003e 0) with a(0)=1 where f(n) = A053645(n).",
				"a(2n) = a(f(n,-1)) + a(f(n,0)) + a(f(n,1)) for n \u003e 0 with a(0)=1 where f(n,k) = 2*(f(floor(n/2),k) + n mod 2) + k*A036987(n) for n \u003e 1 with f(1,k) = abs(k).",
				"a(n) = Sum_{j=0..2^f(n)-1} (-1)^(f(n)-f(j)) Product_{k=0..f(n)} (1 + f(floor(j/2^k)))^T(n,k) for n \u003e 0 with a(0)=1 where f(n) = A000120(n), T(n,k) = T(floor(n/2),k - n mod 2) for k \u003e 0 with T(n,0) = A001511(n).",
				"Sum_{k=0..2^n-1} a(k) = (n+1)! for n \u003e= 0.",
				"a((4^n-1)/3) = A110501(n+1) for n \u003e= 0.",
				"a(2^2*(2^n-1)) = A091344(n+1), a(2^3*(2^n-1)) = A091347(n+1), a(2^4*(2^n-1)) = A091348(n+1). More generally, a(2^m*(2^n-1)) = a(2^n*(2^m-1)) = T(n+1,m) for n \u003e= 0, m \u003e= 0 where T(n,m) = Sum_{k=1..n} k!*k^m*Stirling2(n,k)*(-1)^(n-k).",
				"a(n) = (1 + A023416(n))*a(f(n)) + Sum_{k=0..floor(log_2(n))-1} (1-T(n,k))*a(f(n) + 2^k*(1 - T(n,k))) for n\u003e1 with a(0) = 1, a(1) = 1, and where f(n) = A053645(n), T(n,k) = floor(n/2^k) mod 2. - _Mikhail Kurkov_, Jun 23 2021"
			],
			"example": [
				"a(1) = 1 because the 1st excedance set is {m-1} and the permutations of {1,2,...,m} with such excedance set are 21, 132, 1243, 12354 and so on, i.e., for a given m we always have 1 permutation.",
				"a(2) = 3 because the 2nd excedance set is {m-2} and the permutations of {1,2,...,m} with such excedance set are 213, 312, 321, 1324, 1423, 1432, 12435, 12534, 12543 and so on, i.e., for a given m we always have 3 permutations.",
				"a(3) = 1 because the 3rd excedance set is {m-2, m-1} and the permutations of {1,2,...,m} with such excedance set are 321, 1342, 12453 and so on, i.e., for a given m we always have 1 permutation."
			],
			"mathematica": [
				"a[n_] := a[n] = Which[n == 0, 1, OddQ[n], a[(n-1)/2], True, a[n/2] + a[n/2 - 2^IntegerExponent[n/2, 2]] + a[n - 2^IntegerExponent[n/2, 2]]];",
				"a /@ Range[0, 65] (* _Jean-François Alcover_, Feb 13 2020 *)"
			],
			"xref": [
				"Cf. A000120, A001511, A007814, A036987, A053645, A063250, A091344, A091347, A091348, A110501, A136126 (related triangle), A152884 (lexicographic order), A329718, A344902, A344947.",
				"Similar recurrences: A006014, A124758, A217924, A243499, A284005, A290615, A341392, A347204, A347205."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Mikhail Kurkov_, Nov 12 2019",
			"references": 16,
			"revision": 139,
			"time": "2021-12-18T08:07:04-05:00",
			"created": "2019-11-17T16:08:19-05:00"
		}
	]
}