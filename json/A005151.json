{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005151",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5151,
			"id": "M4779",
			"data": "1,11,21,1112,3112,211213,312213,212223,114213,31121314,41122314,31221324,21322314,21322314,21322314,21322314,21322314,21322314,21322314,21322314,21322314,21322314,21322314,21322314,21322314,21322314",
			"name": "Summarize the previous term (digits in increasing order), starting with a(1) = 1.",
			"comment": [
				"a(n) = 21322314 for n \u003e 12. - _Reinhard Zumkeller_, Jan 25 2014",
				"The digits of each term a(n) are a permutation of those of the corresponding term A063850(n). - _Chayim Lowen_, Jul 16 2015"
			],
			"reference": [
				"C. Fleenor, \"A litteral sequence\", Solution to Problem 2562, Journal of Recreational Mathematics, vol. 31 No. 4 pp. 307 2002-3 Baywood NY.",
				"Problem in J. Recreational Math., 30 (4) (1999-2000), p. 309.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A005151/b005151.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"V. Bronstein and A. S. Fraenkel, \u003ca href=\"http://www.jstor.org/stable/2975323\"\u003eOn a curious property of counting sequences\u003c/a\u003e, Amer. Math. Monthly, 101 (1994), 560-563.",
				"Onno M. Cain and Sela T. Enin, \u003ca href=\"https://arxiv.org/abs/2004.00209\"\u003eInventory Loops (i.e. Counting Sequences) have Pre-period 2 max S_1 + 60\u003c/a\u003e, arXiv:2004.00209 [math.NT], 2020.",
				"X. Gourdon and B. Salvy, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(95)00133-H\"\u003eEffective asymptotics of linear recurrences with rational coefficients\u003c/a\u003e, Discrete Mathematics, vol. 153, no. 1-3, 1996, pages 145-163.",
				"James Henle, \u003ca href=\"http://scholarship.claremont.edu/jhm/vol1/iss1/7\"\u003eIs (some) mathematics poetry?\u003c/a\u003e, Journal of Humanistic Mathematics 1:1 (2011), pp. 94-100.",
				"Madras Math's Amazing Number Facts, \u003ca href=\"https://web.archive.org/web/20041105013844/http://www.users.zetnet.co.uk/madras/maths/amazingnofacts/fact013.html\"\u003eFact No. 13\u003c/a\u003e",
				"Madras Math, \u003ca href=\"https://web.archive.org/web/20041103085145/http://www.users.zetnet.co.uk/madras/maths/descriptive6.html\"\u003eDescriptive Number\u003c/a\u003e",
				"Trevor Scheopner, \u003ca href=\"https://web.archive.org/web/20171109082421/http://pumj.org/docs/Issue1/Article_4.pdf\"\u003eThe Cyclic Nature (and Other Intriguing Properties) of Descriptive Numbers\u003c/a\u003e, Princeton Undergraduate Mathematics Journal, Issue 1, Article 4.",
				"L. J. Upton, \u003ca href=\"/A005151/a005151.pdf\"\u003eLetter to N. J. A. Sloane\u003c/a\u003e, Jan 8 1991.",
				"\u003ca href=\"/index/Rec#order_01\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1)."
			],
			"formula": [
				"a(n+1) = A047842(a(n)). - _M. F. Hasler_, Feb 25 2018",
				"G.f.: x*(1 + 10*x + 10*x^2 + 1091*x^3 + 2000*x^4 + 208101*x^5 + 101000*x^6 - 99990*x^7 - 98010*x^8 + 31007101*x^9 + 10001000*x^10 - 9900990*x^11 - 9899010*x^12) / (1 - x). - _Colin Barker_, Aug 23 2018"
			],
			"example": [
				"The term after 312213 is obtained by saying \"Two 1's, two 2's, two 3's\", which gives 21-22-23, i.e., 212223."
			],
			"maple": [
				"P:=proc(q,h) local a,b,c,j,k,n; a:=h; print(a);",
				"for n from 1 to q do a:=convert(a,base,10); b:=0;",
				"for k from 0 to 9 do c:=0; for j from 1 to nops(a) do",
				"if a[j]=k then c:=c+1; fi; od;",
				"if c\u003e0 then b:=b*10^(ilog10(c*10+k)+1)+c*10+k; fi; od;",
				"a:=b; print(a); od; end: P(10,1); # _Paolo P. Lava_, Mar 21 2018"
			],
			"mathematica": [
				"RunLengthEncode[x_List] := (Through[{Length, First}[ #1]] \u0026) /@ Split[ Sort[x]]; LookAndSay[n_, d_:1] := NestList[ Flatten[ RunLengthEncode[ # ]] \u0026, {d}, n - 1]; F[n_] := LookAndSay[n, 1][[n]]; Table[ FromDigits[ F[n]], {n, 25}] (* _Robert G. Wilson v_, Jan 22 2004 *)",
				"a[1] = 1; a[n_] := a[n] = FromDigits[Reverse /@ Sort[Tally[a[n-1] // IntegerDigits], #1[[1]] \u003c #2[[1]]\u0026] // Flatten]; Array[a, 26] (* _Jean-François Alcover_, Jan 25 2016 *)"
			],
			"program": [
				"(Haskell)",
				"import Data.List (group, sort, transpose)",
				"a005151 n = a005151_list !! (n-1)",
				"a005151_list = 1 : f [1] :: [Integer] where",
				"   f xs = (read $ concatMap show ys) : f ys where",
				"          ys = concat $ transpose [map length zss, map head zss]",
				"          zss = group $ sort xs",
				"-- _Reinhard Zumkeller_, Jan 25 2014",
				"(PARI) say(n) = {digs = digits(n); d = vecsort(digs,,8); s = \"\"; for (k=1, #d, nbk = #select(x-\u003ex==d[k], digs); s = concat(s, Str(nbk)); s = concat(s, d[k]);); eval(s);}",
				"lista(nn) = {print1(n = 1, \", \"); for (k=1, nn, m = say(n); print1(m, \", \"); n = m;);} \\\\ _Michel Marcus_, Feb 12 2016",
				"(PARI) a(n,show_all=1,a=1)={for(i=2,n,show_all\u0026\u0026print1(a\",\");a=A047842(a));a} \\\\ _M. F. Hasler_, Feb 25 2018",
				"(PARI) Vec(x*(1 + 10*x + 10*x^2 + 1091*x^3 + 2000*x^4 + 208101*x^5 + 101000*x^6 - 99990*x^7 - 98010*x^8 + 31007101*x^9 + 10001000*x^10 - 9900990*x^11 - 9899010*x^12) / (1 - x) + O(x^40)) \\\\ _Colin Barker_, Aug 23 2018",
				"(Python)",
				"from itertools import accumulate, groupby, repeat",
				"def summarize(n, _):",
				"  return int(\"\".join(str(len(list(g)))+k for k, g in groupby(sorted(str(n)))))",
				"def aupton(nn): return list(accumulate(repeat(1, nn+1), summarize))",
				"print(aupton(25)) # _Michael S. Branicky_, Jan 11 2021"
			],
			"xref": [
				"Cf. A005150, A047842. See A083671 for another version.",
				"Cf. A023989, A118628, A060857."
			],
			"keyword": "nonn,base,easy",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"references": 45,
			"revision": 83,
			"time": "2021-01-11T13:21:54-05:00",
			"created": "1991-05-16T03:00:00-04:00"
		}
	]
}