{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A321981",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 321981,
			"data": "1,2,0,6,0,0,16,0,2,0,0,40,12,2,0,0,0,0,96,16,44,6,0,0,0,0,0,0,0,224,136,66,52,2,4,0,2,0,0,0,0,0,0,0,512,384,208,96,30,178,0,18,30,2,0,0,0,0,0,0,0,0,0,0,0,0,1152,1024,584,522,138,588,102",
			"name": "Row n gives the chromatic symmetric function of the n-girder, expanded in terms of elementary symmetric functions and ordered by Heinz number.",
			"comment": [
				"The Heinz number of an integer partition (y_1, ..., y_k) is prime(y_1) * ... * prime(y_k).",
				"A stable partition of a graph is a set partition of the vertices where no edge has both ends in the same block. The chromatic symmetric function is given by X_G = Sum_p m(t(p)) where the sum is over all stable partitions of G, t(p) is the integer partition whose parts are the block-sizes of p, and m is augmented monomial symmetric functions (see A321895).",
				"The n-girder has n vertices and looks like:",
				"  2-4-6-     -n",
				"  |\\|\\|\\ ... \\|",
				"  1-3-5-     n-1",
				"Conjecture: All terms are nonnegative (verified up to n = 10). This is a special case of Stanley and Stembridge's poset-chain conjecture."
			],
			"link": [
				"Richard P. Stanley, \u003ca href=\"http://www-math.mit.edu/~rstan/pubs/pubfiles/100.pdf\"\u003eA symmetric function generalization of the chromatic polynomial of a graph\u003c/a\u003e, Advances in Math. 111 (1995), 166-194.",
				"Richard P. Stanley, \u003ca href=\"http://www-math.mit.edu/~rstan/papers/taor.pdf\"\u003eGraph colorings and related symmetric functions: ideas and applications\u003c/a\u003e, Discrete Mathematics 193 (1998), 267-286.",
				"Richard P. Stanley and John R. Stembridge, \u003ca href=\"https://doi.org/10.1016/0097-3165(93)90048-D\"\u003eOn immanants of Jacobi-Trudi matrices and permutations with restricted position\u003c/a\u003e, Journal of Combinatorial Theory Series A 62-2 (1993), 261-279.",
				"Gus Wiseman, \u003ca href=\"http://arxiv.org/abs/0709.0430\"\u003eEnumeration of paths and cycles and e-coefficients of incomparability graphs\u003c/a\u003e."
			],
			"example": [
				"Triangle begins:",
				"    1",
				"    2   0",
				"    6   0   0",
				"   16   0   2   0   0",
				"   40  12   2   0   0   0   0",
				"   96  16  44   6   0   0   0   0   0   0   0",
				"  224 136  66  52   2   4   0   2   0   0   0   0   0   0   0",
				"For example, row 6 gives: X_G6 = 96e(6) + 6e(33) + 16e(42) + 44e(51)."
			],
			"xref": [
				"Row sums are A025192.",
				"Cf. A000569, A001187, A006125, A056239, A229048, A240936, A245883, A277203, A321750, A321911, A321918, A321914, A321979, A321980, A321982."
			],
			"keyword": "nonn,tabf",
			"offset": "1,2",
			"author": "_Gus Wiseman_, Nov 23 2018",
			"references": 4,
			"revision": 4,
			"time": "2018-11-23T21:14:31-05:00",
			"created": "2018-11-23T21:14:31-05:00"
		}
	]
}