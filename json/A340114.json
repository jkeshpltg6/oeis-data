{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A340114",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 340114,
			"data": "1,2,1,3,6,2,4,8,1,2,4,12,6,18,9,3,15,30,10,5,35,7,21,42,14,28,56,8,16,48,24,72,36,2,1,3,9,18,6,12,24,8,40,20,10,5,55,11,33,66,22,44,4,52,26,78,39,13,91,7,49,98,14,42,126,63,21,105,15,45,135,27,81",
			"name": "Table T(n,k), n\u003e=1, k\u003e=1, row n being the lexicographically earliest of the longest sequences of distinct positive integers in which the k-th term does not exceed n*k and the smaller of adjacent terms divides the larger, giving a prime.",
			"comment": [
				"The longest sequence is finite for all n. We can deduce this, because we know from the work of Saias that A337125(m)/m * log m is bounded, where A337125(m) is the length of the longest simple path in the divisor graph of {1,...,m}. See the comment in A337125 giving constraints on its terms.",
				"The sequence of row lengths starts 2, 6, 25, 97."
			],
			"link": [
				"Peter Munn, \u003ca href=\"/A340114/b340114.txt\"\u003eRows n = 1..4 of triangle, flattened\u003c/a\u003e",
				"E. Saias, \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/aa/aa83/aa8333.pdf\"\u003eApplications des entiers à diviseurs denses\u003c/a\u003e, Acta Arithmetica, 83, 3 (1998), 225-240."
			],
			"formula": [
				"For n \u003e= 1, 1 \u003c= k \u003c= row length(n), T(n,k) \u003c= n * k.",
				"For n \u003e= 1, 1 \u003c= k \u003c row length(n), max(T(n,k+1)/T(n,k), T(n,k)/T(n,k+1)) is in A000040."
			],
			"example": [
				"For n = 1, the only sequences of distinct positive integers that have their k-th term not exceeding 1*k = k, are those whose n-th term is k. The longest such sequence in which the smaller of adjacent terms divides the larger, giving a prime, is (1, 2), since 3/2 is 1.5. So row 1 has length 2, with T(1,1) = 1, T(1,2) = 2.",
				"Table begins:",
				"1, 2;",
				"1, 3, 6, 2, 4, 8;",
				"1, 2, 4, 12, 6, 18, 9, 3, 15, 30, 10, 5, 35, 7, 21, 42, 14, 28, 56, 8, 16, 48, 24, 72, 36;",
				"..."
			],
			"xref": [
				"Cf. A000040, A300012, A337125, A339491."
			],
			"keyword": "nonn,hard,tabf",
			"offset": "1,2",
			"author": "_Peter Munn_, Dec 28 2020",
			"references": 2,
			"revision": 23,
			"time": "2021-03-06T08:19:34-05:00",
			"created": "2021-02-20T03:47:52-05:00"
		}
	]
}