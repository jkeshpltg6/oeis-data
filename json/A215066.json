{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A215066",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 215066,
			"data": "1,1,7,127,4315,235831,18911467,2091412807,305035062955,56729101908151,13102338649018027,3679320979659518887,1234515698986458346795,487763952468349266962071,224150079034073231822617387,118541831524545132821950527367",
			"name": "Expansion of e.g.f.: Sum_{n\u003e=0} Product_{k=1..n} (exp((2*k-1)*x) - 1).",
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A215066/b215066.txt\"\u003eTable of n, a(n) for n = 0..234\u003c/a\u003e",
				"A. Folsom, K. Ono and R. C. Rhoades, \u003ca href=\"https://www.semanticscholar.org/paper/RAMANUJAN%E2%80%99S-RADIAL-LIMITS-Ono-Rhoades/e9ce236b0b74f08dfbb983970f11d28d87872228\"\u003eRamanujan's radial limits\u003c/a\u003e, 2013. - From _N. J. A. Sloane_, Feb 09 2013",
				"Hsien-Kuei Hwang and Emma Yu Jin, \u003ca href=\"https://arxiv.org/abs/1911.06690\"\u003eAsymptotics and statistics on Fishburn matrices and their generalizations\u003c/a\u003e, arXiv:1911.06690 [math.CO], 2019."
			],
			"formula": [
				"Folsom et al. give a closed form for a(n). - _N. J. A. Sloane_, Feb 09 2013",
				"E.g.f.: 1 + (exp(x)-1)/(W(0)-exp(x)+1), where W(k) = (exp(x))^(2*k+1) - ((exp(x))^(2*k+3)-1)/W(k+1); (continued fraction). - _Sergei N. Gladkovskii_, Jan 05 2014",
				"a(n) ~ sqrt(6) * 24^n * (n!)^2 / (sqrt(n) * Pi^(2*n+3/2)). - _Vaclav Kotesovec_, May 04 2014",
				"E.g.f.: 1/2*( 1 + Sum_{n\u003e=0} exp((2*n+1)*x)*Product_{k=1..n} (exp((2*k-1)*x) - 1) ). Cf. A053250 and A207569. - _Peter Bala_, May 15 2017",
				"Conjectural g.f.: Sum_{n \u003e= 0} (-1)^n*Product_{k = 1..n} (1 + (-1)^k*exp(- k*t)). Cf. A158690. - _Peter Bala_, Jan 28 2021"
			],
			"example": [
				"E.g.f.: A(x) = 1 + x + 7*x^2/2! + 127*x^3/3! + 4315*x^4/4! + 235831*x^5/5! +...",
				"where",
				"A(x) = 1 + (exp(x)-1) + (exp(x)-1)*(exp(3*x)-1) + (exp(x)-1)*(exp(3*x)-1)*(exp(5*x)-1) + (exp(x)-1)*(exp(3*x)-1)*(exp(5*x)-1)*(exp(7*x)-1) + (exp(x)-1)*(exp(3*x)-1)*(exp(5*x)-1)*(exp(7*x)-1)*(exp(9*x)-1) +..."
			],
			"maple": [
				"m:= 20; S:= series( add(mul(exp((2*k-1)*x)-1, k=1..j), j=0..m+1), x, m+1): seq(factorial(j)*coeff(S, x, j), j = 0..m); # _G. C. Greubel_, Feb 07 2020"
			],
			"mathematica": [
				"Table[((-1)^n*2*Sum[Sum[n!/(a!*(2b)!*(n-a-2b)!)*(3/2)^a*(5/2)^(2b) * EulerE[2a+2b],{a,0,n}],{b,0,n/2}] + 2*(-1)^n*Sum[n!/((n-2b)!*(2b)!)*(3/2)^(n-2b)*(1/2)^(2b)*EulerE[2n-2b],{b,0,n/2}])/4,{n,0,20}] (* _Vaclav Kotesovec_, May 04 2014 after A. Folsom *)",
				"With[{m=20}, CoefficientList[Series[Sum[Product[Exp[(2*k-1)*x] -1, {k, j}], {j, 0, m+2}], {x,0,m}], x]*Range[0, m]!] (* _G. C. Greubel_, Feb 07 2020 *)"
			],
			"program": [
				"(PARI) {a(n)=n!*polcoeff(sum(m=0, n+1, prod(k=1, m, exp((2*k-1)*x+x*O(x^n))-1)), n)}",
				"for(n=0, 26, print1(a(n), \", \"))",
				"(MAGMA) m:=20; R\u003cx\u003e:=PowerSeriesRing(Rationals(), m); b:=Coefficients(R!( (\u0026+[(\u0026*[Exp((2*k-1)*x) -1: k in [1..j]]): j in [1..m+1]]) )); [1] cat [Factorial(n)*b[n]: n in [1..m-1]]; // _G. C. Greubel_, Feb 07 2020",
				"(Sage)",
				"m=20;",
				"def A215066_list(prec):",
				"    P.\u003cx\u003e = PowerSeriesRing(QQ, prec)",
				"    return P( sum(product(exp((2*k-1)*x)-1 for k in (1..j)) for j in (0..m)) ).list()",
				"a=A215066_list(m+1); [factorial(n)*a[n] for n in (0..m)] # _G. C. Greubel_, Feb 07 2020"
			],
			"xref": [
				"Cf. A053250, A158690, A207569, A214687."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Paul D. Hanna_, Aug 01 2012",
			"references": 6,
			"revision": 40,
			"time": "2021-12-11T02:10:47-05:00",
			"created": "2012-08-01T21:38:05-04:00"
		}
	]
}