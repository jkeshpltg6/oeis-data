{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A141412",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 141412,
			"data": "1,2,1,3,1,1,4,12,2,1,5,6,4,1,1,6,180,8,6,2,1,7,10,15,2,6,1,1,8,560,240,240,6,4,2,1,9,1260,15120,20,144,1,12,1,1,10,12600,672,945,32,240,8,3,2,1,11,1260,8400,1512,3024,48,240,3,1,1,1,12,166320,100800,64800,12096,12096",
			"name": "Table c(n,k) of the denominators of coefficients [x^k] P(n,x) of the polynomials P(n,x) of A129891.",
			"comment": [
				"Polynomials are characteristic polynomials of a particular John Couch Adams matrix.",
				"General term: ((((-1)^(n-j))*C(j, n))*n!)*Integral (from 0 to i) (u*(u-1)*(u-2)* ... *(u-n))/(u-j)) du, i,j from 1 to n (see Flajolet et al.).",
				"Denominators are 1, 2, 12, 24, 720 = A091137.",
				"These polynomials come from the explicit case. The less interesting implicit case has the same denominators (see P. Curtz reference)."
			],
			"reference": [
				"P. Flajolet, X. Gourdon, B. Salvy, Gazette des Mathématiciens 55, 1993, p.67.",
				"P. Curtz Integration .. note 12, C.C.S.A., Arcueil 1969, p. 61; ibid. pp. 62-65."
			],
			"link": [
				"Bakir Farhi, \u003ca href=\"https://arxiv.org/abs/1810.07560\"\u003eOn the derivatives of the integer-valued polynomials\u003c/a\u003e, arXiv:1810.07560 [math.NT], 2018."
			],
			"formula": [
				"Conjecture: T(n, k) = d(n+1, k+1), with d(n,k) = denominator(A000254(n, k)*k!/n!) where A000254 are the unsigned Stirling numbers of the 1st kind. See d(n,k) in Farhi link. - _Michel Marcus_, Oct 18 2018"
			],
			"example": [
				"Triangle begins:",
				"  1,",
				"  2, 1,",
				"  3, 1, 1,",
				"  4, 12, 2, 1,",
				"  5, 6, 4, 1, 1,",
				"  6, 180, 8, 6, 2, 1,",
				"  7, 10, 15, 2, 6, 1, 1,",
				"  ..."
			],
			"maple": [
				"P := proc(n,x) option remember ; if n =0 then 1; else (-1)^n/(n+1)+x*add( (-1)^i/(i+1)*procname(n-1-i,x),i=0..n-1) ; expand(%) ; fi; end:",
				"A141412 := proc(n,k) p := P(n,x) ; denom(coeftayl(p,x=0,k)) ; end: seq(seq(A141412(n,k),k=0..n),n=0..13) ; # _R. J. Mathar_, Aug 24 2009"
			],
			"mathematica": [
				"p[0] = 1; p[n_] := p[n] = (-1)^n/(n+1) + x*Sum[(-1)^k*p[n-1-k] / (k+1), {k, 0, n-1}]; Denominator[ Flatten[ Table[ CoefficientList[p[n], x], {n, 0, 11}]]][[1 ;; 72]] (* _Jean-François Alcover_, Jun 17 2011 *)"
			],
			"xref": [
				"Cf. A140749 (numerators)."
			],
			"keyword": "nonn,frac,tabl,uned",
			"offset": "0,2",
			"author": "_Paul Curtz_, Aug 04 2008",
			"ext": [
				"Partially edited by _R. J. Mathar_, Aug 24 2009"
			],
			"references": 7,
			"revision": 25,
			"time": "2018-10-18T09:10:41-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}