{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A215500",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 215500,
			"data": "4,2,10,14,54,112,340,814,2254,5702,15250,39404,104004,270922,711490,1859134,4873054,12748472,33391060,87394454,228841254,599050102,1568437210,4106054164,10750060804,28143585362,73681573690,192899714414,505019869254,1322156172352",
			"name": "a(n) = ((sqrt(5) + 3)^n + (-sqrt(5) -1)^n + (-sqrt(5) + 3)^n + (sqrt(5) - 1)^n) / 2^n.",
			"comment": [
				"Inverse binomial transform is (-1)^n * a(n). - _Michael Somos_, Jun 02 2014"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A215500/b215500.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,3,-4,1)."
			],
			"formula": [
				"G.f.: 2*(2-x)*(1+x)*(1-2*x)/((1-3*x+x^2)*(1+x-x^2)). - _Colin Barker_, Aug 19 2012",
				"a(n) = 2*a(n-1)+3*a(n-2)-4*a(n-3)+a(n-4). - _Colin Barker_, Aug 20 2012",
				"a(-n) = A124697(n) if n\u003e0. - _Michael Somos_, Jun 02 2014"
			],
			"example": [
				"G.f. = 4 + 2*x + 10*x^2 + 14*x^3 + 54*x^4 + 112*x^5 + 340*x^6 + ..."
			],
			"maple": [
				"A215500 := x -\u003e ((sqrt(5)+3)^x+(-sqrt(5)-1)^x+(-sqrt(5)+3)^x+(sqrt(5)-1)^x)/2^x;",
				"seq(simplify(A215500(i)),i=0..29);"
			],
			"mathematica": [
				"a[n_] := ((Sqrt[5] + 3)^n + (-Sqrt[5] - 1)^n + (-Sqrt[5] + 3)^n + (Sqrt[5] - 1)^n)/2^n; Table[a[n] // Simplify, {n, 0, 29}] (* _Jean-François Alcover_, Jul 02 2013 *)",
				"LinearRecurrence[{2,3,-4,1}, {4, 2, 10, 14}, 50] (* _G. C. Greubel_, Apr 23 2018 *)"
			],
			"program": [
				"(Sage)",
				"def A215500(x) :",
				"    return ((sqrt(5)+3)^x+(-sqrt(5)-1)^x+(-sqrt(5)+3)^x+(sqrt(5)-1)^x)/2^x",
				"[A215500(i).round() for i in (0..29)]",
				"(PARI) {a(n) = polsym( (1 + (-1)^(n\u003e0)*x - x^2) * (1 - 3*x + x^2), abs(n))[1 + abs(n)]}; /* _Michael Somos_, Jun 02 2014 */",
				"(MAGMA) I:=[4,2,10,14]; [n le 4 select I[n] else 2*Self(n-1) + 3*Self(n-2) - 4*Self(n-3) + Self(n-4): n in [1..30]]; // _G. C. Greubel_, Apr 23 2018"
			],
			"xref": [
				"Cf. A051927, A215502, A215503.",
				"Cf. A124697."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_Peter Luschny_, Aug 13 2012",
			"references": 3,
			"revision": 25,
			"time": "2018-04-24T03:34:36-04:00",
			"created": "2012-08-14T00:04:47-04:00"
		}
	]
}