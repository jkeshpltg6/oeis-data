{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A201552",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 201552,
			"data": "1,1,3,1,5,7,1,7,19,19,1,9,37,85,51,1,11,61,231,381,141,1,13,91,489,1451,1751,393,1,15,127,891,3951,9331,8135,1107,1,17,169,1469,8801,32661,60691,38165,3139,1,19,217,2255,17151,88913,273127,398567,180325,8953,1",
			"name": "Square array read by diagonals: T(n,k) = number of arrays of n integers in -k..k with sum equal to 0.",
			"comment": [
				"Equivalently, the number of compositions of n*(k + 1) into n parts with maximum part size 2*k+1. - _Andrew Howroyd_, Oct 14 2017"
			],
			"link": [
				"R. H. Hardin, \u003ca href=\"/A201552/b201552.txt\"\u003eTable of n, a(n) for n = 1..9999\u003c/a\u003e"
			],
			"formula": [
				"Empirical: T(n,k) = Sum_{i=0..floor(k*n/(2*k+1))} (-1)^i*binomial(n,i)*binomial((k+1)*n-(2*k+1)*i-1,n-1).",
				"The above empirical formula is true and can be derived from the formula for the number of compositions with given number of parts and maximum part size. - _Andrew Howroyd_, Oct 14 2017",
				"Empirical for rows:",
				"T(1,k) = 1",
				"T(2,k) = 2*k + 1",
				"T(3,k) = 3*k^2 + 3*k + 1",
				"T(4,k) = (16/3)*k^3 + 8*k^2 + (14/3)*k + 1",
				"T(5,k) = (115/12)*k^4 + (115/6)*k^3 + (185/12)*k^2 + (35/6)*k + 1",
				"T(6,k) = (88/5)*k^5 + 44*k^4 + 46*k^3 + 25*k^2 + (37/5)*k + 1",
				"T(7,k) = (5887/180)*k^6 + (5887/60)*k^5 + (2275/18)*k^4 + (357/4)*k^3 + (6643/180)*k^2 + (259/30)*k + 1",
				"T(m,k) = (1/Pi)*integral_{x=0..Pi} (sin((k+1/2)x)/sin(x/2))^m dx; for the proof see Dirichlet's Kernel (http://en.wikipedia.org/wiki/Dirichlet_kernel); so f(m,n) = (1/Pi)*integral_{x=0..Pi} (Sum_{k=-n..n} exp(I*k*x))^m dx = sum(integral(exp(I(k_1+...+k_m).x),x=0..Pi)/Pi,{k_1,...,k_m=-n..n}) = sum(delta_0(k1+...+k_m),{k_1,...,k_m=-n..n}) = number of arrays of m integers in -n..n with sum zero. - _Yalcin Aktar_, Dec 03 2011"
			],
			"example": [
				"Some solutions for n=7, k=3:",
				"..1...-2....1...-1....1...-3....0....0....1....2....3...-3....0....2....1....0",
				".-1....2...-2....2....2....2...-1....0....2....2...-2...-1...-2...-1....2...-1",
				".-3...-1....1...-3....2....1....0....1....3....0....2....0...-1....2...-2...-1",
				"..0....3....3....3...-2...-2....3....3...-3...-3....0...-1...-1...-1....0....3",
				"..2...-1...-1...-1...-3....0...-3...-2....1...-1...-1....1....1....0....3...-1",
				"..2...-1...-3....0....2....3....0....1...-2....1....1....1....3...-2...-3...-3",
				".-1....0....1....0...-2...-1....1...-3...-2...-1...-3....3....0....0...-1....3",
				"Table starts:",
				".   1,      1,       1,        1,        1,         1,...",
				".   3,      5,       7,        9,       11,        13,...",
				".   7,     19,      37,       61,       91,       127,...",
				".  19,     85,     231,      489,      891,      1469,...",
				".  51,    381,    1451,     3951,     8801,     17151,...",
				". 141,   1751,    9331,    32661,    88913,    204763,...",
				". 393,   8135,   60691,   273127,   908755,   2473325,...",
				".1107,  38165,  398567,  2306025,  9377467,  30162301,...",
				".3139, 180325, 2636263, 19610233, 97464799, 370487485,..."
			],
			"mathematica": [
				"comps[r_, m_, k_] := Sum[(-1)^i*Binomial[r - 1 - i*m, k - 1]*Binomial[k, i], {i, 0, Floor[(r - k)/m]}];  T[n_, k_] := comps[n*(k + 1), 2*k + 1, n]; Table[T[n - k + 1, k], {n, 1, 11}, {k, n, 1, -1}] // Flatten (* _Jean-François Alcover_, Oct 31 2017, after _Andrew Howroyd_ *)"
			],
			"program": [
				"(PARI)",
				"comps(r, m, k)=sum(i=0, floor((r-k)/m), (-1)^i*binomial(r-1-i*m, k-1)*binomial(k, i));",
				"T(n,k) = comps(n*(k+1), 2*k+1, n); \\\\ _Andrew Howroyd_, Oct 14 2017"
			],
			"xref": [
				"Columns 1-10: A002426, A005191, A025012, A025014, A201549, A201550, A201551, A322538, A322539, A322540.",
				"Rows 3-10: A003215, A063496(n+1), A083669, A201553, A201554, A322535, A322536, A322537.",
				"Cf. A286928."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_R. H. Hardin_, Dec 02 2011",
			"references": 17,
			"revision": 40,
			"time": "2018-12-15T03:27:57-05:00",
			"created": "2011-12-02T16:46:52-05:00"
		}
	]
}