{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A329563",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 329563,
			"data": "1,2,3,4,5,8,9,14,6,23,17,7,12,24,10,13,19,16,18,25,22,15,28,21,26,32,75,20,11,27,56,30,41,53,29,38,60,44,35,113,36,31,48,61,37,42,46,33,34,55,39,40,49,58,45,43,52,51,106,57,62,50,87,47,54,59,80,66,83,68",
			"name": "For all n \u003e= 1, exactly five sums are prime among a(n+i) + a(n+j), 0 \u003c= i \u003c j \u003c 5; lexicographically earliest such sequence of distinct positive numbers.",
			"comment": [
				"That is, there are 5 primes, counted with multiplicity, among the 10 pairwise sums of any 5 consecutive terms.",
				"Conjectured to be a permutation of the positive integers.",
				"This sequence is quite different from the restriction of the \"nonnegative\" variant A329564 to positive indices: it seems that the two have no common terms beyond a(6) = 8, except for the accidental a(22) = 15 and maybe some later coincidences of this type. There also appears to be no other simple relation between the terms of these sequences, in contrast to, e.g., A055265 vs. A128280."
			],
			"link": [
				"M. F. Hasler, \u003ca href=\"/wiki/User:M._F._Hasler/Prime_sums_from_neighboring_terms\"\u003ePrime sums from neighboring terms\u003c/a\u003e, OEIS wiki, Nov. 23, 2019"
			],
			"example": [
				"For n = 1, we consider pairwise sums among the first 5 terms chosen as small as possible, a(1..5) = (1, 2, 3, 4, 5). We see that we have indeed 5 primes among the sums 1+2, 1+3, 1+4, 1+5, 2+3, 2+4, 2+5, 3+4, 3+5, 4+5.",
				"Then, to get a(6), consider first the pairwise sums among terms a(2..5), (2+3, 2+4, 2+5; 3+4, 3+5; 4+5), among which there are 3 primes, counted with multiplicity (i.e., the prime 7 is there two times). So the new term a(6) must give exactly two more prime sums with the terms a(2..5). We find that 6 or 7 would give just one more (5+6 resp. 4+7), but a(6) = 8 gives exactly two more, 3+8 and 5+8."
			],
			"program": [
				"(PARI) {A329563(n,show=1,o=1,N=5,M=4,p=[],u=o,U)=for(n=o,n-1, show\u003e0\u0026\u0026 print1(o\",\"); show\u003c0\u0026\u0026 listput(L,o); U+=1\u003c\u003c(o-u); U\u003e\u003e=-u+u+=valuation(U+1,2); p=concat(if(#p\u003e=M,p[^1],p),o); my(c=N-sum(i=2,#p, sum(j=1,i-1, isprime(p[i]+p[j])))); if(#p\u003cM\u0026\u0026sum(i=1,#p,isprime(p[i]+u))\u003c=c,o=u)|| for(k=u,oo,bittest(U,k-u)|| sum(i=1,#p,isprime(p[i]+k))!=c|| [o=k,break]));show\u0026\u0026print([u]);o} \\\\ optional args: show=1: print a(o..n-1), show=-1: append them on global list L, in both cases print [least unused number] at the end. See the wiki page for a function S() which returns a vector: a(0..n-1) = S(5,5;1)."
			],
			"xref": [
				"Cf. A329425 (6 primes using 5 consecutive terms), A329566 (6 primes using 6 consecutive terms).",
				"Cf. A329449 (4 primes using 4 consecutive terms), A329456 (4 primes using 5 consecutive terms).",
				"Cf. A329454 (3 primes using 4 consecutive terms), A329455 (3 primes using 5 consecutive terms).",
				"Cf. A329411 (2 primes using 3 consecutive terms), A329452 (2 primes using 4 consecutive terms), A329453 (2 primes using 5 consecutive terms).",
				"Cf. A329333 (1 (odd) prime using 3 terms), A128280 \u0026 A055265 (1 prime using 2 terms); A055266 \u0026 A253074 (0 primes using 2 terms), A329405 \u0026 A329450 (0 primes using 3 terms), A329406 ff: other variants."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_M. F. Hasler_, Feb 09 2020",
			"references": 6,
			"revision": 7,
			"time": "2020-02-12T19:33:00-05:00",
			"created": "2020-02-09T22:47:53-05:00"
		}
	]
}