{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A219766",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 219766,
			"data": "0,0,0,0,0,0,0,0,2,6,22,67,213,744,2609,9016,31426,110381,390223,1383905,4931307,17633765,63301415,228130900",
			"name": "Number of nonsquare simple perfect squared rectangles of order n up to symmetry.",
			"comment": [
				"A squared rectangle (which may be a square, but not in this particular sequence) is a rectangle dissected into a finite number, two or more, of integer sized squares. If no two of these squares have the same size the squared rectangle is perfect. A squared rectangle is simple if it does not contain a smaller squared rectangle. The order of a squared rectangle is the number of constituent squares."
			],
			"reference": [
				"See A006983 and A217156 for references."
			],
			"link": [
				"Stuart E Anderson, \u003ca href=\"http://www.squaring.net/sq/sr/spsr/spsr.html\"\u003eSimple Perfect Squared Rectangles \u003c/a\u003e [Nonsquare rectangles only]",
				"I. Gambini, \u003ca href=\"http://alain.colmerauer.free.fr/alcol/ArchivesPublications/Gambini/carres.pdf\"\u003eQuant aux carrés carrelés\u003c/a\u003e, Thesis, Université de la Méditerranée Aix-Marseille II, 1999, p. 24.",
				"See A006983 and A217156 for further links."
			],
			"formula": [
				"a(n) = A002839(n) - A006983(n).",
				"In 'A Census of Planar Maps', William Tutte gave an asymptotic formula for the number of perfect squared rectangles where n is the number of elements in the dissection (the order):",
				"a(n) ~ ((n^(-5/2))*(4^n))/(2^5*sqrt(Pi))."
			],
			"mathematica": [
				"A[s_Integer] := With[{s6 = StringPadLeft[ToString[s], 6, \"0\"]}, Cases[ Import[\"https://oeis.org/A\" \u003c\u003e s6 \u003c\u003e \"/b\" \u003c\u003e s6 \u003c\u003e \".txt\", \"Table\"], {_, _}][[All, 2]]];",
				"A002839 = A@002839;",
				"A006983 = A@006983;",
				"a[n_] := A002839[[n]] - A006983[[n]];",
				"a /@ Range[24] (* _Jean-François Alcover_, Jan 13 2020 *)"
			],
			"xref": [
				"Cf. A002839, A006983, A002962, A002881, A181735.",
				"Cf. A217153, A217154, A217156."
			],
			"keyword": "nonn,hard,more",
			"offset": "1,9",
			"author": "_Stuart E Anderson_, Nov 27 2012",
			"ext": [
				"a(9)-a(24) enumerated Gambini 1999, confirmed by _Stuart E Anderson_ Dec 07 2012"
			],
			"references": 3,
			"revision": 35,
			"time": "2021-11-11T02:13:48-05:00",
			"created": "2012-12-07T11:55:13-05:00"
		}
	]
}