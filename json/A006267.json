{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006267",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6267,
			"id": "M3699",
			"data": "1,4,76,439204,84722519070079276,608130213374088941214747405817720942127490792974404",
			"name": "Continued cotangent for the golden ratio.",
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Harry J. Smith, \u003ca href=\"/A006267/b006267.txt\"\u003eTable of n, a(n) for n = 0..7\u003c/a\u003e",
				"Mohammad K. Azarian, \u003ca href=\"https://doi.org/10.35834/1998/1003176\"\u003eProblem 123\u003c/a\u003e, Missouri Journal of Mathematical Sciences, Vol. 10, No. 3 (Fall 1998), p. 176; \u003ca href=\"https://doi.org/10.35834/2000/1201050\"\u003eSolution\u003c/a\u003e, ibid., Vol. 12, No. 1 (Winter 2000), pp. 61-62.",
				"Jeffrey Shallit, \u003ca href=\"http://archive.org/details/jresv80Bn2p285\"\u003ePredictable regular continued cotangent expansions\u003c/a\u003e, J. Res. Nat. Bur. Standards Sect. B, Vol. 80B, No. 2 (1976), pp. 285-290.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/LehmerCotangentExpansion.html\"\u003eLehmer Cotangent Expansion\u003c/a\u003e."
			],
			"formula": [
				"(1+sqrt(5))/2 = cot(Sum_{n\u003e=0} (-1)^n*acot(a(n))); let b(0) = (1+sqrt(5))/2, b(n) = (b(n-1)*floor(b(n-1))+1)/(b(n-1)-floor(b(n-1)) then a(n) = floor(b(n)). - _Benoit Cloitre_, Apr 10 2003",
				"a(n) = A000204(3^n). - _Benoit Cloitre_, Sep 18 2005",
				"a(n) = Round(c^(3^n)) where c = GoldenRatio = 1.6180339887498948482... = (sqrt(5)+1)/2 (A001622). - _Artur Jasinski_, Sep 22 2008",
				"Recurrence a(n+1) = a(n)^3 + 3*a(n), a(0)=4. - _Artur Jasinski_, Sep 24 2008",
				"a(n+1) = Product_{k = 0..n} A002813(k). Thus a(n) divides a(n+1). - _Peter Bala_, Nov 22 2012",
				"Sum_{n\u003e=0} a(n)^2/A045529(n+1) = 1. - _Amiram Eldar_, Jan 12 2022"
			],
			"mathematica": [
				"c = N[GoldenRatio, 1000]; Table[Round[c^(3^n)], {n, 1, 8}] (* _Artur Jasinski_, Sep 22 2008 *)",
				"a = {}; x = 4; Do[AppendTo[a, x]; x = x^3 + 3 x, {n, 1, 10}]; a (* _Artur Jasinski_, Sep 24 2008 *)"
			],
			"program": [
				"(PARI) bn=vector(100); b(n)=if(n\u003c0,0,bn[n]); bn[1]=(1+sqrt(5))/2; for(n=2,10,bn[n]=(b(n-1)*floor(b(n-1))+1)/(b(n-1)-floor(b(n-1)))) a(n)=floor(b(n+1))",
				"(PARI) { default(realprecision, 10000); bn=vector(8); bn[1]=(1+sqrt(5))/2; for(n=2, 8, bn[n]=(bn[n-1]*floor(bn[n-1]) + 1)/(bn[n-1] - floor(bn[n-1]))); for (n=1, 8, write(\"b006267.txt\", n-1, \" \", floor(bn[n]))); } \\\\ _Harry J. Smith_, May 04 2009"
			],
			"xref": [
				"Cf. A000204, A001622, A002666, A002667, A002668, A002813, A045529."
			],
			"keyword": "nonn,changed",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"The next term is too large to include."
			],
			"references": 19,
			"revision": 46,
			"time": "2022-01-12T08:57:14-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}