{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A306825",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 306825,
			"data": "1,4,15,14,209,13,2911,194,2703,181,564719,193,7865521,2521,34945,37634,1525870529,2701,21252634831,37441,6779137,489061,4122901604639,37633,274758906449,6811741,19726764303,7263361,11140078609864049,40321,155161278879431551",
			"name": "Primitive part of A001353(n).",
			"comment": [
				"A prime p is called a unique-period prime in base b if there is no other prime q such that the period length of 1/q is equal to that of 1/p. If q = a(2p) = A001353(2*p)/(4*A001353(p)) = ((2 + sqrt(3))^p + (2 - sqrt(3))^p)/4 is prime (this happens for p = 3, 5, 7, 11, 13, 17, 19, 79, 151, 199, 233, 251, 317, ...), where p is an odd prime, then q is a unique-period prime in base b = (sqrt(12*q^2 - 3) - 1)/2 (1/q has period length 3) as well as in base b' = (sqrt(12*q^2 - 3) + 1)/2 (1/q has period length 6). For example, a(6) = 13 is prime, so 13 is the only prime whose reciprocal has period length 3 in base 22 and the only prime whose reciprocal has period length 6 in base 23. Compare: If q = A000129(p) = A008555(p), then q is a unique-period prime in base b = sqrt(2*q^2 - 1) (1/q has period length 4).",
				"By Lucas-Lehmer test, p is a Mersenne prime \u003e 3 if and only if the smallest k such that p divides a(k) is k = (p - 1)/2.",
				"For primes p, p^2 divides a(k) for some k if and only if p = 2 or p is in A238490. If p \u003e 2, the only possible values for k are the divisors of (p - Legendre(3,p))/2 (e.g., 103^2 divides a(52) = 53028360515521 = 103^2 * 4998431569).",
				"Conjecturally there must be infinitely many primes p such that a(p) is prime, but no such p is known."
			],
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/SylvesterCyclotomicNumber.html\"\u003eSylvester Cyclotomic Number\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Lucas-Lehmer_primality_test\"\u003eLucas Lehmer Primality Test\u003c/a\u003e"
			],
			"formula": [
				"Product_{d|n} a(d) = A001353(n), that is, a(n) = A001353(n)/(Product_{d\u003cn, d|n} a(d)). Equivalently, a(n) = Product_{d|n} A001353(d)^mu(n/d), where mu = A008683."
			],
			"example": [
				"For n = 8 we have: a(1) = A001353(1), a(1)*a(2) = A001353(2), a(1)*a(2)*a(4) = A001353(4), a(1)*a(2)*a(4)*a(8) = A001353(8). The solution is a(1) = 1, a(2) = 4, a(4) = 14 and a(8) = 194."
			],
			"program": [
				"(PARI) b(n) = if(n==1, [1], my(v=vector(n)); v[1]=1; v[2]=4; for(i=3, n, v[i]=4*v[i-1]-v[i-2]); v)",
				"a(n) = my(d=divisors(n)); prod(i=1, #d, (b(n)[d[i]])^moebius(n/d[i]))"
			],
			"xref": [
				"Cf. A001353, A238490.",
				"Similar sequences: A061446, A008555."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Jianing Song_, Mar 16 2019",
			"references": 2,
			"revision": 31,
			"time": "2019-03-16T13:14:04-04:00",
			"created": "2019-03-16T13:14:04-04:00"
		}
	]
}