{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A212220",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 212220,
			"data": "1,-3,2,0,1,-12,58,-137,154,-64,0,1,-27,324,-2223,9414,-24879,39528,-33966,11828,0,1,-48,1064,-14244,126936,-784788,3409590,-10329081,21197804,-27779384,20648794,-6476644,0,1,-75,2650,-58100,878200,-9632440,78681510",
			"name": "Triangle T(n,k), n\u003e=1, 0\u003c=k\u003c=3n, read by rows: row n gives the coefficients of the chromatic polynomial of the complete tripartite graph K_(n,n,n), highest powers first.",
			"comment": [
				"The complete tripartite graph K_(n,n,n) has 3*n vertices and 3*n^2 = A033428(n) edges. The chromatic polynomial of K_(n,n,n) has 3*n+1 = A016777(n) coefficients."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A212220/b212220.txt\"\u003eRows n = 1..58, flattened\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CompleteTripartiteGraph.html\"\u003eComplete Tripartite Graph\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Chromatic_polynomial\"\u003eChromatic Polynomial\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = [q^(3*n-k)] Sum_{k,m=1..n} S2(n,k) * S2(n,m) * (q-k-m)^n * Product_{i=0..k+m-1} (q-i) with S2 = A008277."
			],
			"example": [
				"2 example graphs:             +-------------+",
				".                             | +-------+   |",
				".                             +-o---o---o   |",
				".                                \\ / \\ / \\ /",
				".                                 X   X   X",
				".                                / \\ / \\ / \\",
				".              o---o---o      +-o---o---o   |",
				".              +-------+      | +-------+   |",
				".                             +-------------+",
				"Graph:         K_(1,1,1)        K_(2,2,2)",
				"Vertices:          3                6",
				"Edges:             3               12",
				"The complete tripartite graph K_(1,1,1) is the cycle graph C_3 with chromatic polynomial q*(q-1)*(q-2) = q^3 -3*q^2 +2*q =\u003e [1, -3, 2, 0].",
				"Triangle T(n,k) begins:",
				"1,   -3,    2,       0;",
				"1,  -12,   58,    -137,     154,       -64,         0;",
				"1,  -27,  324,   -2223,    9414,    -24879,     39528, ...",
				"1,  -48, 1064,  -14244,  126936,   -784788,   3409590, ...",
				"1,  -75, 2650,  -58100,  878200,  -9632440,  78681510, ...",
				"1, -108, 5562, -180585, 4123350, -70008186, 912054348, ..."
			],
			"maple": [
				"P:= proc(n) option remember;",
				"       expand(add(add(Stirling2(n, k) *Stirling2(n, m)",
				"       *mul(q-i, i=0..k+m-1) *(q-k-m)^n, m=1..n), k=1..n))",
				"    end:",
				"T:= n-\u003e seq(coeff(P(n), q, 3*n-k), k=0..3*n):",
				"seq(T(n), n=1..6);"
			],
			"mathematica": [
				"P[n_] := P[n] = Expand[Sum[Sum[StirlingS2[n, k] *StirlingS2[n, m]*Product[q - i, {i, 0, k + m - 1}]*(q - k - m)^n, {m, 1, n}], {k, 1, n}]];",
				"T[n_] := Table[Coefficient[P[n], q, 3*n - k], {k, 0, 3*n}];",
				"Array[T, 6] // Flatten (* _Jean-François Alcover_, May 29 2018, from Maple *)"
			],
			"xref": [
				"Columns k=0-1 give: A000012, (-1)*A033428.",
				"Row sums and last elements of rows give: A000004, row lengths give: A016777.",
				"Cf. A008277, A182553, A212221."
			],
			"keyword": "sign,tabf",
			"offset": "1,2",
			"author": "_Alois P. Heinz_, May 06 2012",
			"references": 3,
			"revision": 21,
			"time": "2018-05-29T08:08:39-04:00",
			"created": "2012-05-07T11:08:59-04:00"
		}
	]
}