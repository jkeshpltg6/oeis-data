{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A078458",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 78458,
			"data": "0,2,1,4,2,3,1,6,2,4,1,5,2,3,3,8,2,4,1,6,2,3,1,7,4,4,3,5,2,5,1,10,2,4,3,6,2,3,3,8,2,4,1,5,4,3,1,9,2,6,3,6,2,5,3,7,2,4,1,7,2,3,3,12,4,4,1,6,2,5,1,8,2,4,5,5,2,5,1,10,4,4,1,6,4,3,3,7,2,6,3,5,2,3,3,11,2,4,3,8,2,5,1,8",
			"name": "Total number of factors in a factorization of n into Gaussian primes.",
			"comment": [
				"a(n)+1 is also the total number of factors in a factorization of n+n*i into Gaussian primes. - _Jason Kimberley_, Dec 17 2011"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A078458/b078458.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A078458/a078458.txt\"\u003ePARI program for finding prime decomposition of Gaussian integers\u003c/a\u003e",
				"Eric W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/GaussianPrime.html\"\u003eMathWorld: Gaussian Prime\u003c/a\u003e",
				"\u003ca href=\"/index/Ga#gaussians\"\u003eIndex entries for Gaussian integers and primes\u003c/a\u003e"
			],
			"formula": [
				"Fully additive with a(p)=2 if p=2 or p mod 4=1 and a(p)=1 if p mod 4=3. - _Vladeta Jovovic_, Jan 20 2003",
				"a(n) depends on the number of primes of the forms 4k+1 (A083025) and 4k-1 (A065339) and on the highest power of 2 dividing n (A007814): a(n) = 2*A007814(n) + 2*A083025(n) + A065339(n) - _T. D. Noe_, Jul 14 2003"
			],
			"example": [
				"2 = (1+i)*(1-i), so a(2) = 2; 9 = 3*3, so a(9) = 2.",
				"a(1006655265000) = a(2^3*3^2*5^4*7^5*11^3) = 3*a(2)+2*a(3)+4*a(5)+5*a(7)+3*a(11) = 3*2+2*1+4*2+5*1+3*1 = 24. - _Vladeta Jovovic_, Jan 20 2003"
			],
			"mathematica": [
				"Join[{0}, Table[f = FactorInteger[n, GaussianIntegers -\u003e True]; cnt = Total[Transpose[f][[2]]]; If[MemberQ[{-1, I, -I}, f[[1, 1]]], cnt--]; cnt, {n, 2, 100}]] (* _T. D. Noe_, Mar 31 2014 *)"
			],
			"program": [
				"(PARI) a(n)=my(f=factor(n)); sum(i=1,#f~,if(f[i,1]%4==3,1,2)*f[i,2]) \\\\ _Charles R Greathouse IV_, Mar 31 2014"
			],
			"xref": [
				"Cf. A078908-A078911, A007814, A065339, A083025, A086275 (number of distinct Gaussian primes in the factorization of n).",
				"Cf. A239626, A239627 (including units)."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_, Jan 11 2003",
			"ext": [
				"More terms from _Vladeta Jovovic_, Jan 12 2003"
			],
			"references": 15,
			"revision": 23,
			"time": "2014-05-01T02:40:01-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}