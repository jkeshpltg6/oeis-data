{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A051775",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 51775,
			"data": "0,0,0,0,1,0,0,2,2,0,0,3,3,3,0,0,4,1,1,4,0,0,5,8,2,8,5,0,0,6,10,12,12,10,6,0,0,7,11,15,6,15,11,7,0,0,8,9,13,2,2,13,9,8,0,0,9,12,14,14,7,14,14,12,9,0,0,10,14,4,10,8,8,10,4,14,10,0,0,11,15,7,11",
			"name": "Table T(n,m) = Nim-product of n and m, read by antidiagonals, for n \u003e= 0, m \u003e= 0.",
			"comment": [
				"Note on an algorithm, _R. J. Mathar_, May 29 2011: (Start)",
				"Let N* denote the Nim-product and N+ the Nim-sum (A003987) of two numbers, and let * and + denote the usual multiplication and addition.",
				"To compute n N* m, write n and m separately as Nim-sums with the aid of the binary representation of n = n0 + n1*2 + n2*4 + n3*8 + n4*16.. and m = m0 + m1*2 + m2*4 + m3*8 + m4*16... . Because Nim-summation is the same as the binary XOR-function, the + may then be replaced by N+ in both sums:",
				"n = Nim-sum_i 2^a(i) and m = Nim-sum_j 2^b(j) with two integer sequences a(i) and b(j).",
				"Because N+ and N* are the operations in a field, N+ and N* are distributive, which is used to write the product over the sums as a double-Nim-sum over Nim-products:",
				"n N* m = Nim-sum_{i,j} 2^a(i) N* 2^b(j) .",
				"What remains is to compute the Nim-products of powers of 2.",
				"Splitting a(i) and b(j) separately into (ordinary) products of Fermat numbers A001146 (i.e., writing a(i) and b(j) in binary), and noting that the ordinary product of distinct Fermat numbers equals the Nim-product of distinct Fermat numbers,",
				"2^a(i) N* 2^b(j) = 2^(2^A0) N* 2^(2^A1) N* ... N* 2^(2^B0) N* 2^(2^B1) N* ... for two binary integer sequences A and B.",
				"This finite product is regrouped by pairing the cases for the same bit in the A-sequence and in the B-sequence. If the bit is set in both sequences, use that the Nim-square of a Fermat number is 3/2 times (ordinary multiple of) that Fermat number; if the bit is set only in one of the two sequences, use (again) that the Nim-product of distinct Fermat numbers is the ordinary product.",
				"Due to the potential presence of the Nim-squares, this leaves in general a Nim-product which is treated by recursion.",
				"This algorithm is implemented in the Maple program in the b-file. nimprodP2() calculates the Nim-product of two powers of 2. (End)"
			],
			"reference": [
				"J. H. Conway, On Numbers and Games, Academic Press, p. 52."
			],
			"link": [
				"R. J. Mathar, \u003ca href=\"/A051775/b051775.txt\"\u003eTable of n, a(n) for n = 0..1890\u003c/a\u003e",
				"Tilman Piesk, \u003ca href=\"/A051775/a051775.txt\"\u003e256x256 table\u003c/a\u003e and \u003ca href=\"https://commons.wikimedia.org/wiki/Category:Nimber_multiplication_8_bit;_dual\"\u003edual matrix\u003c/a\u003e",
				"Rémy Sigrist, \u003ca href=\"/A051775/a051775.png\"\u003eColored representation of T(x,y) for x = 0..1023 and y = 0..1023\u003c/a\u003e (where the hue is function of T(x,y))",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Nimber\"\u003eNimber\u003c/a\u003e",
				"\u003ca href=\"/index/Ni#Nimmult\"\u003eIndex entries for sequences related to Nim-multiplication\u003c/a\u003e"
			],
			"example": [
				"The table begins:",
				"  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0 ...",
				"  0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 ...",
				"  0  2  3  1  8 10 11  9 12 14 15 13  4  6  7  5 ...",
				"  0  3  1  2 12 15 13 14  4  7  5  6  8 11  9 10 ...",
				"  0  4  8 12  6  2 14 10 11 15  3  7 13  9  5  1 ...",
				"  0  5 10 15  2  7  8 13  3  6  9 12  1  4 11 14 ...",
				"  (...)"
			],
			"maple": [
				"We continue from A003987: to compute a Nim-multiplication table using (a) an addition table AT := array(0..NA, 0..NA) and (b) a nimsum procedure for larger values; MT := array(0..N,0..N); for a from 0 to N do MT[a,0] := 0; MT[0,a] := 0; MT[a,1] := a; MT[1,a] := a; od: for a from 2 to N do for b from a to N do t1 := {}; for i from 0 to a-1 do for j from 0 to b-1 do u1 := MT[i,b]; u2 := MT[a,j];",
				"if u1\u003c=NA and u2\u003c=NA then u12 := AT[u1,u2]; else u12 := nimsum(u1,u2); fi; u3 := MT[i,j]; if u12\u003c=NA and u3\u003c=NA then u4 := AT[u12,u3]; else u4 := nimsum(u12,u3); fi; t1 := { op(t1), u4}; #t1 := { op(t1), AT[ AT[ MT[i,b], MT[a,j] ], MT[i,j] ] }; od; od;",
				"t2 := sort(convert(t1,list)); j := nops(t2); for i from 1 to nops(t2) do if t2[i] \u003c\u003e i-1 then j := i-1; break; fi; od; MT[a,b] := j; MT[b,a] := j; od; od;"
			],
			"program": [
				"(PARI) NP_table=Map(); NP(x,y)={ if(x\u003c2 || y\u003c2, x*y, mapisdefined(NP_table, if(y\u003ex, [x,y]=[y,x], [x,y])), mapget(NP_table,[x,y]), x==3, y-1, x==2, 3, my(F=4); until(!F *= F, if(x\u003c2*F, F=if(x\u003eF, bitxor(NP(F,y), NP(x-F,y)), y\u003cx, x*y, x\\2*3); break); my(t = 2*F); until(F*F \u003c= t *= 2, if(x==t, if(y\u003cF, F=NP(NP(y,t\\F),F); break(2)); my(i = F); until(t \u003c= i *= 2, if(y\u003c2*i, F=if(y\u003ei, bitxor(NP(t,i), NP(t,y-i)), NP(F\\2*3, NP(t/F,i/F))); break(3))); if(y==t, F=NP(F\\2*3, NP(t/F,t/F)); break(2))); if(x\u003c2*t, F=bitxor(NP(t,y), NP(x-t,y)); break(2)))); mapput(NP_table,[x,y], F); F)} \\\\ _M. F. Hasler_, Jan 18 2021",
				"A051775(n,m=\"\")={if(m!=\"\", NP(n,m), NP((1+m=sqrtint(8*n+1)\\/2)*m/2-n-1, n-m*(m-1)/2))} \\\\ Then A051775(n) = a(n) [flattened sequence, cf. A025581 \u0026 A002262], A051775(n,m) = T(n,m): for example, {matrix(6,15,m,n, A051775(m,n))} - _M. F. Hasler_, Jan 22 2021"
			],
			"xref": [
				"Cf. A051776, A003987."
			],
			"keyword": "tabl,nonn,easy,nice",
			"offset": "0,8",
			"author": "_N. J. A. Sloane_, Dec 19 1999",
			"references": 29,
			"revision": 50,
			"time": "2021-01-22T20:46:22-05:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}