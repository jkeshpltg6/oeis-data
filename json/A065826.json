{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A065826",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 65826,
			"data": "1,1,2,1,8,3,1,22,33,4,1,52,198,104,5,1,114,906,1208,285,6,1,240,3573,9664,5955,720,7,1,494,12879,62476,78095,25758,1729,8,1,1004,43824,352936,780950,529404,102256,4016,9,1,2026,143520,1820768,6551770,7862124,3186344,382720,9117,10",
			"name": "Triangle with T(n,k) = k*E(n,k) where E(n,k) are Eulerian numbers A008292.",
			"comment": [
				"Row sums are (n+1)!/2, i.e., A001710 offset, implying that if n balls are put at random into n boxes, the expected number of boxes with at least one ball is (n+1)/2 and the expected number of empty boxes is (n-1)/2.",
				"T(n,k) is the number of permutations of {1,2,...,n+1} that start with an ascent and that have k-1 descents. - _Ira M. Gessel_, May 02 2017"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A065826/b065826.txt\"\u003eRows n = 1..141, flattened\u003c/a\u003e",
				"Ron M. Adin, Sergi Elizalde, Victor Reiner, Yuval Roichman, \u003ca href=\"http://ceur-ws.org/Vol-2113/paper2.pdf\"\u003eCyclic descent extensions and distributions\u003c/a\u003e, Semantic Sensor Networks Workshop 2018, CEUR Workshop Proceedings (2018) Vol. 2113.",
				"Ron M. Adin, Victor Reiner, Yuval Roichman, \u003ca href=\"https://arxiv.org/abs/1710.06664\"\u003eOn cyclic descents for tableaux\u003c/a\u003e, arXiv:1710.06664 [math.CO], 2017."
			],
			"formula": [
				"T(n, k) = k*(a(n-1, k) + a(n-1, k-1)*(n-k+1)/(k-1)) [with T(n, 1) = 1] = Sum_{j=0..k} k*(-1)^j*(k-j)^n*binomial(n+1, j).",
				"E.g.f.: (exp(x*(1-t)) - 1 - x*(1-t))/((1-t)*(1 - t*exp(x*(1-t)))). - _Ira M. Gessel_, May 02 2017"
			],
			"example": [
				"Rows start:",
				"  1;",
				"  1,   2;",
				"  1,   8,     3;",
				"  1,  22,    33,     4;",
				"  1,  52,   198,   104,     5;",
				"  1, 114,   906,  1208,   285,     6;",
				"  1, 240,  3573,  9664,  5955,   720,    7;",
				"  1, 494, 12879, 62476, 78095, 25758, 1729, 8;",
				"  etc."
			],
			"maple": [
				"T:=(n,k)-\u003eadd(k*(-1)^j*(k-j)^n*binomial(n+1,j),j=0..k): seq(seq(T(n,k),k=1..n),n=1..10); # _Muniru A Asiru_, Mar 09 2019"
			],
			"mathematica": [
				"Array[Range[Length@ #] # \u0026@ CoefficientList[(1 - x)^(# + 1)*PolyLog[-#, x]/x, x] \u0026, 10] (* _Michael De Vlieger_, Sep 24 2018, after _Vaclav Kotesovec_ at A008292 *)"
			],
			"program": [
				"(GAP) Flat(List([1..10],n-\u003eList([1..n],k-\u003eSum([0..k],j-\u003ek*(-1)^j*(k-j)^n*Binomial(n+1,j))))); # _Muniru A Asiru_, Mar 09 2019"
			],
			"xref": [
				"Cf. A008292."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Henry Bottomley_, Dec 06 2001",
			"references": 3,
			"revision": 38,
			"time": "2021-06-25T13:07:48-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}