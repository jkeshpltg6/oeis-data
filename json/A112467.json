{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A112467",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 112467,
			"data": "1,-1,1,-1,0,1,-1,-1,1,1,-1,-2,0,2,1,-1,-3,-2,2,3,1,-1,-4,-5,0,5,4,1,-1,-5,-9,-5,5,9,5,1,-1,-6,-14,-14,0,14,14,6,1,-1,-7,-20,-28,-14,14,28,20,7,1,-1,-8,-27,-48,-42,0,42,48,27,8,1,-1,-9,-35,-75,-90,-42,42,90,75,35,9,1,-1,-10,-44,-110,-165,-132,0,132,165,110",
			"name": "Riordan array ((1-2x)/(1-x), x/(1-x)).",
			"comment": [
				"Row sums are A000007. Diagonal sums are -F(n-2). Inverse is A112468. T(2n,n)=0.",
				"(-1,1)-Pascal triangle. - _Philippe Deléham_, Aug 07 2006",
				"Apart from initial term, same as A008482. - _Philippe Deléham_, Nov 07 2006",
				"Each column equals the cumulative sum of the previous column. - _Mats Granvik_, Mar 15 2010",
				"Reading along antidiagonals generates in essence rows of A192174. - _Paul Curtz_, Oct 02 2011",
				"Triangle T(n,k), read by rows, given by (-1,2,0,0,0,0,0,0,0,...) DELTA (1,0,0,0,0,0,0,0,0,...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Nov 01 2011"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A112467/b112467.txt\"\u003eRows n = 0..100 of triangle, flattened\u003c/a\u003e",
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/1912.01124\"\u003eA Note on Riordan Arrays with Catalan Halves\u003c/a\u003e, arXiv:1912.01124 [math.CO], 2019.",
				"E. Deutsch, L. Ferrari and S. Rinaldi, \u003ca href=\"http://dx.doi.org/10.1016/j.aam.2004.05.002\"\u003eProduction Matrices\u003c/a\u003e, Advances in Mathematics, 34 (2005) pp. 101-122.",
				"D. Foata, G.-N. Han, \u003ca href=\"http://dx.doi.org/10.1007/s11139-009-9194-9\"\u003eThe doubloon polynomial triangle\u003c/a\u003e, Ram. J. 23 (2010), 107-126.",
				"Jack Ramsay, \u003ca href=\"/A349812/a349812.pdf\"\u003eOn Arithmetical Triangles\u003c/a\u003e, The Pulse of Long Island, June 1965 [Mentions application to design of antenna arrays. Annotated scan.]"
			],
			"formula": [
				"Number triangle T(n, k) = binomial(n, n-k) - 2*binomial(n-1, n-k-1);",
				"Sum_{k=0..n} T(n, k)*x^k = (x-1)*(x+1)^(n-1). - _Philippe Deléham_, Oct 03 2005",
				"T(n,k) = ((2*k-n)/n)*binomial(n, k), with T(0,0)=1. - _Roger L. Bagula_, Feb 16 2009; modified by _G. C. Greubel_, Dec 04 2019",
				"T(n,k) = T(n-1,k-1) + T(n-1,k) with T(0,0)=1, T(1,0)=-1, T(n,k)=0 for k\u003en or for n\u003c0. - _Philippe Deléham_, Nov 01 2011",
				"G.f.: (1-2x)/(1-(1+y)*x). - _Philippe Deléham_, Dec 15 2011",
				"Sum_{k=0..n} T(n,k)*x^k = A000007(n), A133494(n), A081294(n), A005053(n), A067411(n), A199661(n), A083233(n) for x = 1, 2, 3, 4, 5, 6, 7, respectively. - _Philippe Deléham_, Dec 15 2011",
				"exp(x) * e.g.f. for row n = e.g.f. for diagonal n. For example, for n = 3 we have exp(x)*(-1 - x + x^2/2! + x^3/3!) = -1 - 2*x - 2*x^2/2! + 5*x^4/4! + 14*x^5/5! + .... The same property holds more generally for Riordan arrays of the form ( f(x), x/(1 - x) ). - _Peter Bala_, Dec 21 2014",
				"Sum_{k=0..n} T(n,k) = 0^n = A000007(n). - _G. C. Greubel_, Dec 04 2019"
			],
			"example": [
				"Triangle starts:",
				"    1;",
				"   -1,  1;",
				"   -1,  0,   1;",
				"   -1, -1,   1,   1;",
				"   -1, -2,   0,   2,   1;",
				"   -1, -3,  -2,   2,   3,   1;",
				"   -1, -4,  -5,   0,   5,   4,  1;",
				"   -1, -5,  -9,  -5,   5,   9,  5,  1;",
				"   -1, -6, -14, -14,   0,  14, 14,  6,  1;",
				"   -1, -7, -20, -28, -14,  14, 28, 20,  7,  1;",
				"   -1, -8, -27, -48, -42,   0, 42, 48, 27,  8, 1;",
				"   -1, -9, -35, -75, -90, -42, 42, 90, 75, 35, 9, 1;",
				"  ...",
				"Production matrix begins",
				"   1,  1,",
				"  -2, -1,  1,",
				"   2,  0, -1,  1,",
				"  -2,  0,  0, -1,  1,",
				"   2,  0,  0,  0, -1,  1,",
				"  -2,  0,  0,  0,  0, -1,  1,",
				"   2,  0,  0,  0,  0,  0, -1,  1",
				"- _Paul Barry_, Apr 08 2011"
			],
			"maple": [
				"seq(seq( `if`(n=0, 1, (2*k-n)*binomial(n,k)/n), k=0..n), n=0..10); # _G. C. Greubel_, Dec 04 2019"
			],
			"mathematica": [
				"T[n_, k_]= If[n==0, 1, ((2*k-n)/n)*Binomial[n, k]]; Table[T[n, k], {n, 0, 10}, {k, 0, n}]//Flatten (* _Roger L. Bagula_, Feb 16 2009; modified by _G. C. Greubel_, Dec 04 2019 *)"
			],
			"program": [
				"(PARI) T(n, k) = if(n==0, 1, (2*k-n)*binomial(n,k)/n ); \\\\ _G. C. Greubel_, Dec 04 2019",
				"(MAGMA) [n eq 0 select 1 else (2*k-n)*Binomial(n,k)/n: k in [0..n], n in [0..10]]; // _G. C. Greubel_, Dec 04 2019",
				"(Sage)",
				"def T(n, k):",
				"    if (n==0): return 1",
				"    else: return (2*k-n)*binomial(n,k)/n",
				"[[T(n, k) for k in (0..n)] for n in (0..10)] # _G. C. Greubel_, Dec 04 2019"
			],
			"xref": [
				"Same first 3 rows as in A054525.",
				"Cf. A008482, A037012, A080232, A112466, A112467, A174293, A174294, A174295, A174296, A174297."
			],
			"keyword": "easy,sign,tabl",
			"offset": "0,12",
			"author": "_Paul Barry_, Sep 06 2005",
			"references": 24,
			"revision": 60,
			"time": "2021-12-23T19:14:23-05:00",
			"created": "2005-09-21T03:00:00-04:00"
		}
	]
}