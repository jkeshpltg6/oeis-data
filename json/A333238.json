{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A333238",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 333238,
			"data": "2,3,2,2,5,2,3,2,7,2,3,2,3,2,3,5,2,3,11,2,3,5,2,3,13,2,3,7,2,3,5,2,3,5,2,3,5,17,2,3,5,7,2,3,5,19,2,3,5,7,2,3,5,7,2,3,5,11,2,3,5,23,2,3,5,7,11,2,3,5,7,2,3,5,7,13,2,3,5,7,2,3,5,7,11",
			"name": "Irregular table where row n lists the distinct smallest primes p of prime partitions of n.",
			"comment": [
				"A prime partition of n is an integer partition wherein all parts are prime. For instance, (3 + 2) is a prime partition of the sum 5; for n = 5, (5) is also a prime partition. For 6, we have two prime partitions (3 + 3) and (2 + 2 + 2).",
				"We note that there are no prime partitions for n = 1, therefore the offset of this sequence is 2.",
				"The number of prime partitions of n is shown by A000607(n).",
				"For prime p, row p includes p itself as the largest term, since p is the sum of (p).",
				"The product of all terms in row n gives A333129(n). - _Alois P. Heinz_, Mar 16 2020",
				"From _David James Sycamore_, Mar 28 2020: (Start)",
				"In the irregular table below, T(n,k) is either prime(k) or is empty. The former means there is at least one prime partition of n with least part prime(k), the latter means that no such partition exists. T(n,k) empty is not recorded in the data.",
				"Recursion for n \u003e= 4: T(n,k) = prime(k) iff T((n-prime(k)), k) = prime(k), or there is a q \u003e k such that T((n-prime(k)), q)) = prime(q); else T(n,k) is empty. Example: T(17,3) = 5 because T(12,3) = prime(3) = 5. T(10,2) = 3 since although T(7,2) is empty, T(7,4) = prime(4) = 7. (End)"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A333238/b333238.txt\"\u003eRows n = 2..1000, flattened\u003c/a\u003e (rows n=2..240 from Michael De Vlieger)",
				"Michael De Vlieger, \u003ca href=\"/A333238/a333238.png\"\u003eLabeled plot of p at (pi(p), n)\u003c/a\u003e, for 2 \u003c= n \u003c= 240, with terms n in A330507 shown in red."
			],
			"example": [
				"The least primes among the prime partitions of 5 are 2 and 5, cf. the 2 prime partitions of 5: (5) and (3, 2), thus row 5 lists {2, 5}.",
				"The least primes among the prime partitions of 6 are 2 and 3, cf. the two prime partitions of 6, (3, 3), and (2, 2, 2), thus row 6 lists {2, 3}.",
				"Row 7 contains {2, 7} because there are 3 prime partitions of 7: (7), (5, 2), (3, 2, 2). Note that 2 is the smallest part of the latter two partitions, thus only 2 and 7 are distinct.",
				"Table plotting prime p in row n at pi(p) place, intervening primes missing from row n are shown by \".\" as a place holder:",
				"n      Primes in row n",
				"----------------------",
				"2:     2",
				"3:     .   3",
				"4:     2",
				"5:     2   .   5",
				"6:     2   3",
				"7:     2   .   .   7",
				"8:     2   3",
				"9:     2   3",
				"10:    2   3   5",
				"11:    2   3   .   .  11",
				"12:    2   3   5",
				"13:    2   3   .   .   .  13",
				"14:    2   3   .   7",
				"15:    2   3   5",
				"16:    2   3   5",
				"17:    2   3   5   .   .   .  17",
				"..."
			],
			"maple": [
				"b:= proc(n, p, t) option remember; `if`(n=0, 1, `if`(p\u003en, 0, (q-\u003e",
				"      add(b(n-p*j, q, 1), j=1..n/p)*t^p+b(n, q, t))(nextprime(p))))",
				"    end:",
				"T:= proc(n) option remember; (p-\u003e seq(`if`(isprime(i) and",
				"      coeff(p, x, i)\u003e0, i, [][]), i=2..degree(p)))(b(n, 2, x))",
				"    end:",
				"seq(T(n), n=2..40);  # _Alois P. Heinz_, Mar 16 2020"
			],
			"mathematica": [
				"Block[{a, m = 20, s}, a = ConstantArray[{}, m]; s = {Prime@ PrimePi@ m}; Do[If[# \u003c= m, If[FreeQ[a[[#]], First@ s], a = ReplacePart[a, # -\u003e Append[a[[#]], First@ s]], Nothing]; AppendTo[s, Last@ s], If[Last@ s == 2, s = DeleteCases[s, 2]; If[Length@ s == 0, Break[], s = MapAt[Prime[PrimePi[#] - 1] \u0026, s, -1]], s = MapAt[Prime[PrimePi[#] - 1] \u0026, s, -1]]] \u0026@ Total[s], {i, Infinity}]; Union /@ a // Flatten]"
			],
			"xref": [
				"Cf. A330507, A333129, A333259, A333365."
			],
			"keyword": "nonn,tabf",
			"offset": "2,1",
			"author": "_Michael De Vlieger_, _David James Sycamore_, Mar 12 2020",
			"references": 9,
			"revision": 33,
			"time": "2021-08-29T01:56:17-04:00",
			"created": "2020-03-13T20:47:50-04:00"
		}
	]
}