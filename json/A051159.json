{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A051159",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 51159,
			"data": "1,1,1,1,0,1,1,1,1,1,1,0,2,0,1,1,1,2,2,1,1,1,0,3,0,3,0,1,1,1,3,3,3,3,1,1,1,0,4,0,6,0,4,0,1,1,1,4,4,6,6,4,4,1,1,1,0,5,0,10,0,10,0,5,0,1,1,1,5,5,10,10,10,10,5,5,1,1,1,0,6,0,15,0,20,0,15,0,6,0,1,1,1,6,6,15,15,20,20,15,15,6,6,1,1",
			"name": "Triangular array made of three copies of Pascal's triangle.",
			"comment": [
				"Computing each term modulo 2 also gives A047999, i.e., a(n) mod 2 = A007318(n) mod 2 for all n. (The triangle is paritywise isomorphic to Pascal's Triangle.) - _Antti Karttunen_",
				"5th row/column gives entries of A000217 (triangular numbers C(n+1,2)) repeated twice and every other entry in 6th row/column form A000217. 7th row/column gives entries of A000292 (Tetrahedral (or pyramidal) nos: C(n+3,3)) repeated twice and every other entry in 8th row/column form A000292. 9th row/column gives entries of A000332 (binomial coefficients binomial(n,4)) repeated twice and every other entry in 10th row/column form A000332. 11th row/column gives entries of A000389 (binomial coefficients C(n,5)) repeated twice and every other entry in 12th row/column form A000389. - _Gerald McGarvey_, Aug 21 2004",
				"If Sum_{k=0..n} A(k)*T(n,k) = B(n), the sequence B is the S-D transform of the sequence A. - _Philippe Deléham_, Aug 02 2006",
				"Number of n-bead black-white reversible strings with k black beads; also binary grids; string is palindromic. - _Yosu Yurramendi_, Aug 07 2008",
				"Row sums give A016116(n+1). - _Yosu Yurramendi_, Aug 07 2008 [corrected by _Petros Hadjicostas_, Nov 04 2017]",
				"Coefficients in expansion of (x + y)^n where x and y anticommute (y x = -x y), that is, q-binomial coefficients when q = -1. - _Michael Somos_, Feb 16 2009",
				"The sequence of coefficients of a general polynomial recursion that links at w=2 to the Pascal triangle is here w=0. Row sums are {1, 2, 2, 4, 4, 8, 8, 16, 16, 32, 32, 64, ...}. - _Roger L. Bagula_ and _Gary W. Adamson_, Dec 04 2009",
				"T(n,k) is the number of palindromic compositions of n+1 with exactly k+1 parts. T(6,4) = 3 because we have the following compositions of n+1=7 with length k+1=5: 1+1+3+1+1, 2+1+1+1+2, 1+2+1+2+1. - _Geoffrey Critzer_, Mar 15 2014 [corrected by _Petros Hadjicostas_, Nov 03 2017]",
				"Let P(n,k) be the number of palindromic compositions of n with exactly k parts. MacMahon (1893) was the first to prove that P(n,k) = T(n-1,k-1), where T(n,k) are the numbers in this sequence (see the comment above by G. Critzer). He actually proved that, for 1 \u003c= s \u003c= m, we have P(2*m,2*s) = P(2*m,2*s-1) = P(2*m-1, 2*s-1) = bin(m-1, s-1), but  P(2*m-1, 2*s) = 0. For the current sequence, this can be translated into T(2*m-1, 2*s-1) = T(2*m-1,2*s-2) = T(2*m-2, 2*s-2) = bin(m-1,s-1), but T(2m-2, 2*s-1) = 0 (valid again for 1 \u003c= s \u003c= m). - _Petros Hadjicostas_, Nov 03 2017",
				"T is the infinite lower triangular matrix for this sequence; define two others, U and V; let U(n,k)=e_k(-1,2,-3,...,(-1)^n n), where e_k is the k-th elementary symmetric polynomial, and let V be the diagonal matrix A057077 (periodic sequence 1,1,-1,-1). Clearly V^-1 = V. Conjecture: U = U^-1, T = U . V, T^-1 = V . U, and |T| = |U|. - _George Beck_, Dec 16 2017",
				"Let T*(n,k)=T(n,k) except when n is odd and k=(n+1)/2, where T*(n,k) = T(n,k)+2^((n-1)/2). Thus, T*(n,k) is the number of non-isomorphic symmetric stairs with n cells and k steps, i.e., k-1 changes of direction. See A016116. - _Christian Barrientos_ and _Sarah Minion_, Jul 29 2018"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A051159/b051159.txt\"\u003eRows n = 0..120 of triangle, flattened\u003c/a\u003e",
				"Paul Barry, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL19/Barry/barry321.html\"\u003eJacobsthal Decompositions of Pascal's Triangle, Ternary Trees, and Alternating Sign Matrices\u003c/a\u003e, Journal of Integer Sequences, 19, 2016, #16.3.5.",
				"E. Burlachenko, \u003ca href=\"https://arxiv.org/abs/1612.00970\"\u003eFractal generalized Pascal matrices\u003c/a\u003e, arXiv:1612.00970 [math.NT], 2016. See p. 3.",
				"S. J. Cyvin, B. N. Cyvin, and J. Brunvoll, \u003ca href=\"https://hrcak.srce.hr/177109\"\u003eUnbranched catacondensed polygonal systems containing hexagons and tetragons\u003c/a\u003e, Croatica Chem. Acta, 69 (1996), 757-774. See Table 1 on p. 763.",
				"D. E. Davenport, L. W. Shapiro and L. C. Woodson, \u003ca href=\"https://doi.org/10.37236/2034\"\u003eThe Double Riordan Group\u003c/a\u003e, The Electronic Journal of Combinatorics, 18(2) (2012).",
				"M. E. Horn, \u003ca href=\"http://arXiv.org/abs/physics/0611277\"\u003eThe Didactical Relevance of the Pauli Pascal Triangle\u003c/a\u003e, arXiv:physics/0611277 [physics.ed-ph], 2006. [_Michael Somos_]",
				"F. Al-Kharousi, R. Kehinde, and A. Umar, \u003ca href=\"http://ajc.maths.uq.edu.au/pdf/58/ajc_v58_p365.pdf\"\u003eCombinatorial results for certain semigroups of partial isometries of a finite chain\u003c/a\u003e, The Australasian Journal of Combinatorics, Volume 58 (3) (2014), 363-375.",
				"P. A. MacMahon, \u003ca href=\"http://www.jstor.org/stable/90632\"\u003eMemoir on the Theory of the Compositions of Numbers\u003c/a\u003e, Phil. Trans. Royal Soc. London A, 184 (1893), 835-901.",
				"\u003ca href=\"/index/Pas#Pascal\"\u003eIndex entries for triangles and arrays related to Pascal's triangle\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = T(n-1, k-1) + T(n-1, k) if n odd or k even, else 0. T(0, 0) = 1.",
				"T(n, k) = T(n-2, k-2) + T(n-2, k). T(0, 0) = T(1, 0) = T(1, 1) = 1.",
				"Square array made by setting first row/column to 1's (A(i, 0) = A(0, j) = 1); A(1, 1) = 0; A(1, j) = A(1, j-2); A(i, 1) = A(i-2, 1); other entries A(i, j) = A(i-2, j) + A(i, j-2). - _Gerald McGarvey_, Aug 21 2004",
				"Sum_{k=0..n} k * T(n,k) = A093968(n); A093968 = S-D transform of A001477. - _Philippe Deléham_, Aug 02 2006",
				"Equals 2*A034851 - A007318. - _Gary W. Adamson_, Dec 31 2007. [Corrected by _Yosu Yurramendi_, Aug 07 2008]",
				"A051160(n, k) = (-1)^floor(k/2) * T(n, k).",
				"Sum_{k = 0..n} T(n,k)*x^k = A000012(n), A016116(n+1), A056487(n), A136859(n+2) for x = 0, 1, 2, 3 respectively. - _Philippe Deléham_, Mar 11 2014",
				"G.f.: (1+x+x*y)/(1-x^2-y^2*x^2). - _Philippe Deléham_, Mar 11 2014",
				"For n,k \u003e= 1, T(n, k) = 0 when n odd and k even; otherwise, T(n, k) = binomial(floor((n-1)/2), floor((k-1)/2)). - _Christian Barrientos_, Mar 14 2020",
				"From _Werner Schulte_, Jun 25 2021: (Start)",
				"T(n,k) = T(n-1,k-1) + (-1)^k * T(n-1,k) for 0 \u003c k \u003c n with initial values T(n,0) = T(n,n) = 1 for n \u003e= 0.",
				"Matrix inverse is T^-1(n,k) = (-1)^((n-k)*(n+k+1)/2) * T(n,k) for 0 \u003c= k \u003c= n. (End)",
				"From _Peter Bala_, Aug 08 2021: (Start)",
				"Double Riordan array ( 1/(1 - x); x/(1 + x), x/(1 - x) ) in the notation of Davenport et al.",
				"G.f. for column 2*n: (1 + x)*x^(2*n)/(1 - x^2)^(n+1); G.f. for column 2*n+1: x^(2*n+1)/(1 - x^2)^(n+1)",
				"Row polynomials: R(2*n,x) = (1 + x^2)^n; R(2*n+1,x) = (1 + x)*(1 + x^2)^n.",
				"The infinitesimal generator of this triangle has the sequence [1, 0, 1, 0, 1, 0, ...] on the main subdiagonal, the sequence [1, 1, 2, 2, 3, 3, 4, 4, ...] on the diagonal immediately below and zeros elsewhere.",
				"Let T denote this lower triangular array. Then T^a, for a in C, is the double Riordan array ( (1 + a*x)/(1 - a*x^2); x/(1 + a*x), (1 + a*x)/(1 - a*x^2) ) with o.g.f. (1 + x*(a + y))/(1 - x^2*(a + y^2)) =  1 + (a + y)*x + (a + y^2)*x^2 + (a^2 + a*y + a*y^2 + y^3)*x^3 + (a^2 + 2*a*y^2 + y^4)*x^4 + ....",
				"The (2*n)-th row polynomial of T^a is (a + y^2)^n; The (2*n+1)-th row polynomial of T^a is (a + y)*(a + y^2)^n. (End)"
			],
			"example": [
				"Triangle starts:",
				"{1},",
				"{1,  1},",
				"{1,  0,  1},",
				"{1,  1,  1,  1},",
				"{1,  0,  2,  0,  1},",
				"{1,  1,  2,  2,  1,  1},",
				"{1,  0,  3,  0,  3,  0,  1},",
				"{1,  1,  3,  3,  3,  3,  1,  1},",
				"{1,  0,  4,  0,  6,  0,  4,  0,  1},",
				"{1,  1,  4,  4,  6,  6,  4,  4,  1,  1},",
				"{1,  0,  5,  0, 10,  0, 10,  0,  5,  0,  1},",
				"{1,  1,  5,  5, 10, 10, 10, 10,  5,  5,  1,  1}",
				"... - _Roger L. Bagula_ and _Gary W. Adamson_, Dec 04 2009"
			],
			"maple": [
				"T:= proc(n, k) option remember; `if`(n=0 and k=0, 1,",
				"      `if`(n\u003c0 or k\u003c0, 0, `if`(irem(n, 2)=1 or",
				"       irem(k, 2)=0, T(n-1, k-1) + T(n-1, k), 0)))",
				"    end:",
				"seq(seq(T(n, k), k=0..n), n=0..14);  # _Alois P. Heinz_, Jul 12 2014"
			],
			"mathematica": [
				"T[ n_, k_] := QBinomial[n, k, -1]; (* _Michael Somos_, Jun 14 2011; since V7 *)",
				"Clear[p, n, x, a]",
				"w = 0;",
				"p[x, 1] := 1;",
				"p[x_, n_] := p[x, n] = If[Mod[n, 2] == 0, (x + 1)*p[x, n - 1], (x^2 + w*x + 1)^Floor[n/2]]",
				"a = Table[CoefficientList[p[x, n], x], {n, 1, 12}]",
				"Flatten[a] (* _Roger L. Bagula_ and _Gary W. Adamson_, Dec 04 2009 *)"
			],
			"program": [
				"(PARI) {T(n, k) = binomial(n%2, k%2) * binomial(n\\2, k\\2)};",
				"(Haskell)",
				"a051159 n k = a051159_tabl !! n !! k",
				"a051159_row n = a051159_tabl !! n",
				"a051159_tabl = [1] : f [1] [1,1] where",
				"   f us vs = vs : f vs (zipWith (+) ([0,0] ++ us) (us ++ [0,0]))",
				"-- _Reinhard Zumkeller_, Apr 25 2013",
				"(SageMath)",
				"@cached_function",
				"def T(n, k):",
				"    if k == 0 or k == n: return 1",
				"    return T(n-1, k-1) + (-1)^k*T(n-1, k)",
				"for n in (0..12): print([T(n, k) for k in (0..n)]) # _Peter Luschny_, Jul 06 2021"
			],
			"xref": [
				"Cf. A007318, A051160, A016116, A034851, A169623, A036355."
			],
			"keyword": "nonn,tabl,easy,nice",
			"offset": "0,13",
			"author": "_Michael Somos_, Oct 14 1999",
			"references": 25,
			"revision": 126,
			"time": "2021-09-18T11:17:56-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}