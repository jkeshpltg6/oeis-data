{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A271206",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 271206,
			"data": "1,1,2,4,1,10,4,1,28,18,5,1,89,77,30,6,1,315,345,164,45,7,1,1233,1617,919,299,63,8,1,5285,8003,5262,2011,492,84,9,1,24583,41871,31180,13611,3857,754,108,10,1,123062,231474,191889,94020,30128,6755,1095,135,11,1",
			"name": "Number T(n,k) of set partitions of [n] having exactly k triples (t,t+1,t+2) such that t+i is in block b+i for some b; triangle T(n,k), n\u003e=0, 0\u003c=k\u003c=max(0,n-2), read by rows.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A271206/b271206.txt\"\u003eRows n = 0..100, flattened\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Partition_of_a_set\"\u003ePartition of a set\u003c/a\u003e"
			],
			"example": [
				"T(3,1) = 1: 1|2|3.",
				"T(4,1) = 4: 12|3|4, 14|2|3, 1|24|3, 1|2|34.",
				"T(5,1) = 18: 123|4|5, 125|3|4, 12|35|4, 12|3|45, 13|24|5, 1|23|4|5, 145|2|3, 14|25|3, 14|2|35, 14|2|3|5, 15|24|3, 1|245|3, 1|24|35, 1|24|3|5, 15|2|34, 1|25|34, 1|2|345, 1|2|34|5.",
				"T(5,2) = 5: 12|3|4|5, 15|2|3|4, 1|25|3|4, 1|2|35|4, 1|2|3|45.",
				"T(5,3) = 1: 1|2|3|4|5.",
				"Triangle T(n,k) begins:",
				":  0 :     1;",
				":  1 :     1;",
				":  2 :     2;",
				":  3 :     4,     1;",
				":  4 :    10,     4,     1;",
				":  5 :    28,    18,     5,     1;",
				":  6 :    89,    77,    30,     6,    1;",
				":  7 :   315,   345,   164,    45,    7,   1;",
				":  8 :  1233,  1617,   919,   299,   63,   8,   1;",
				":  9 :  5285,  8003,  5262,  2011,  492,  84,   9,  1;",
				": 10 : 24583, 41871, 31180, 13611, 3857, 754, 108, 10, 1;"
			],
			"maple": [
				"b:= proc(n, i, t, m) option remember; expand(`if`(n=0, 1, add((v-\u003e",
				"     `if`(t and v, x, 1)*b(n-1, j, v, max(m, j)))(j=i+1), j=1..m+1)))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=0..degree(p)))(b(n, 1, false, 0)):",
				"seq(T(n), n=0..14);"
			],
			"mathematica": [
				"b[n_, i_, t_, m_] := b[n, i, t, m] = Expand[If[n==0, 1, Sum[Function[v, If[t \u0026\u0026 v, x, 1]*b[n-1, j, v, Max[m, j]]][j==i+1], {j, 1, m+1}]]]; T[n_] := Function[p, Table[Coefficient[p, x, i], {i, 0, Exponent[p, x]}]][b[n, 1, False, 0]]; Table[T[n], {n, 0, 14}] // Flatten (* _Jean-François Alcover_, Feb 05 2017, translated from Maple *)"
			],
			"xref": [
				"Column k=0 gives A271207.",
				"Row sums give A000110.",
				"Cf. A185982."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_Alois P. Heinz_, Apr 01 2016",
			"references": 3,
			"revision": 15,
			"time": "2017-02-05T06:43:50-05:00",
			"created": "2016-04-02T20:38:46-04:00"
		}
	]
}