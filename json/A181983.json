{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A181983",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 181983,
			"data": "0,1,-2,3,-4,5,-6,7,-8,9,-10,11,-12,13,-14,15,-16,17,-18,19,-20,21,-22,23,-24,25,-26,27,-28,29,-30,31,-32,33,-34,35,-36,37,-38,39,-40,41,-42,43,-44,45,-46,47,-48,49,-50,51,-52,53,-54,55,-56,57,-58,59",
			"name": "a(n) = (-1)^(n+1) * n.",
			"comment": [
				"This is the Lucas U(-2,1) sequence. - _R. J. Mathar_, Jan 08 2013",
				"Apparently the Mobius transform of A002129. - _R. J. Mathar_, Jan 08 2013",
				"For n\u003e0, a(n) is also the determinant of the symmetric n X n matrix M defined by M(i,j) = max(i,j) for 1 \u003c= i,j \u003c= n. - _Enrique Pérez Herrero_, Jan 14 2013",
				"The sums of the terms of this sequence is the divergent series 1 - 2 + 3 - 4 + ... . Euler summed it to 1/4 which was one of the first examples of summing divergent series. - _Michael Somos_, Jun 05 2013"
			],
			"link": [
				"Enrique Pérez Herrero, \u003ca href=\"http://psychedelic-geometry.blogspot.com/2013/01/max-determinant.html\"\u003eMax Determinant\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/1_%E2%88%92_2_%2B_3_%E2%88%92_4_%2B_%C2%B7_%C2%B7_%C2%B7\"\u003e1 - 2 + 3 - 4 + ...\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Lucas_sequence#Specific_names\"\u003eLucas sequence\u003c/a\u003e",
				"\u003ca href=\"/index/Lu#Lucas\"\u003eIndex entries for Lucas sequences\u003c/a\u003e"
			],
			"formula": [
				"G.f.: x / (1 + x)^2.",
				"E.g.f.: x / exp(x).",
				"a(n) = -a(-n) = -(-1)^n * A001477(n) for all n in Z.",
				"a(n+1) = p(n+1) where p(x) is the unique degree-n polynomial such that p(k) = Bernoulli(k) for k = 0, 1, ..., n.",
				"A001787(n) = p(0) where p(x) is the unique degree-n polynomial such that p(k) = a(k) for k = 1, ..., n+1. - _Michael Somos_, Jun 05 2013",
				"Euler transform of length 2 sequence [-2, 2].",
				"Series reversion of g.f. is A000108(n) (Catalan numbers) with a(0)=0.",
				"Series reversion of e.g.f. is A000169. INVERT transform omitting a(0)=0 is A049347. PSUM transform is A001057. BINOMIAL transform is A154955. - _Michael Somos_, Jun 05 2013",
				"n * a(n) = A162395(n). - _Michael Somos_, Jun 05 2013",
				"a(n) = - A038608(n). - _Reinhard Zumkeller_, Mar 20 2013",
				"a(n+2) = a(n) - 2*(-1)^n. - _G. C. Greubel_, Aug 11 2018",
				"a(n) = - A274922(n) if n\u003e0. - _Michael Somos_, Sep 24 2019"
			],
			"example": [
				"G.f. = x - 2*x^2 + 3*x^3 - 4*x^4 + 5*x^5 - 6*x^6 + 7*x^7 - 8*x^8 + 9*x^9 + ..."
			],
			"maple": [
				"A181983:=n-\u003e-(-1)^n * n; seq(A181983(n), n=0..100); # _Wesley Ivan Hurt_, Feb 26 2014"
			],
			"mathematica": [
				"a[ n_] := -(-1)^n n;",
				"a[ n_] := Sign[n] SeriesCoefficient[ x / (1 + x)^2, {x, 0, Abs @n}];",
				"a[ n_] := Sign[n] (Abs @n)! SeriesCoefficient[ x / Exp[ x], {x, 0, Abs @n}];"
			],
			"program": [
				"(PARI) {a(n) = -(-1)^n * n};",
				"(Haskell)",
				"a181983 = negate . a038608",
				"a181983_list = [0, 1] ++ map negate",
				"   (zipWith (+) a181983_list (map (* 2) $ tail a181983_list))",
				"-- _Reinhard Zumkeller_, Mar 20 2013",
				"(MAGMA) [(-1)^(n+1)*n: n in [0..30]]; // _G. C. Greubel_, Aug 11 2018"
			],
			"xref": [
				"Cf. A000108, A000169, A001057, A001477, A001787, A002129, A038608, A049347, A154955, A274922."
			],
			"keyword": "sign,mult,easy",
			"offset": "0,3",
			"author": "_Michael Somos_, Apr 04 2012",
			"references": 23,
			"revision": 67,
			"time": "2021-09-28T06:05:24-04:00",
			"created": "2012-04-04T17:01:58-04:00"
		}
	]
}