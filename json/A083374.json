{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A083374",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 83374,
			"data": "0,6,36,120,300,630,1176,2016,3240,4950,7260,10296,14196,19110,25200,32640,41616,52326,64980,79800,97020,116886,139656,165600,195000,228150,265356,306936,353220,404550,461280,523776,592416,667590,749700,839160,936396",
			"name": "a(n) = n^2 * (n^2 - 1)/2.",
			"comment": [
				"Triangular numbers t_n as n runs through the squares.",
				"Partial sums of A055112: If one generated Pythagorean primitive triangles from n, n+1, then the collective areas of n of them would be equal to the numbers in this sequence. The sum of the first three triangles is 6+30+84 = 120 which is the third nonzero term of the sequence. - _J. M. Bergot_, Jul 14 2011",
				"Second leg of Pythagorean triangles with smallest side a cube: A000578(n)^2 + a(n)^2 = A037270(n)^2. - _Martin Renner_, Nov 12 2011",
				"a(n) is the number of segments on an n X n grid or geoboard. - _Martin Renner_, Apr 17 2014",
				"Consider the partitions of 2n into two parts (p,q). Then a(n) is the total volume of the family of rectangular prisms with dimensions p, q and |q-p|. - _Wesley Ivan Hurt_, Apr 15 2018"
			],
			"reference": [
				"Albert H. Beiler, Recreations in the theory of numbers, New York: Dover, (2nd ed.) 1966, p. 106, table 55."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A083374/b083374.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Somaya Barati, Beáta Bényi, Abbas Jafarzadeh and Daniel Yaqubi, \u003ca href=\"https://arxiv.org/abs/1812.02955\"\u003eMixed restricted Stirling numbers\u003c/a\u003e, arXiv:1812.02955 [math.CO], 2018.",
				"Franck Ramaharo, \u003ca href=\"https://arxiv.org/abs/1805.10680\"\u003eA generating polynomial for the pretzel knot\u003c/a\u003e, arXiv:1805.10680 [math.CO], 2018.",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (5,-10,10,-5,1)."
			],
			"formula": [
				"a(n) = (n + 1) * A006002(n).",
				"a(n) = A047928(n)/2 = A002415(n+1)*6 = A006011(n+1)*2 = A008911(n+1)*3. - _Zerinvary Lajos_, May 09 2007",
				"a(n) = binomial(n^2,2), n\u003e=1. - _Zerinvary Lajos_, Jan 07 2008",
				"a(n) = 5*a(n-1)-10*a(n-2)+10*a(n-3)-5*a(n-4)+a(n-5) for n\u003e5. - _R. J. Mathar_, Apr 10 2009",
				"G.f.: -6*x^2*(1+x)/(x-1)^5. - _R. J. Mathar_, Apr 10 2009",
				"Sum_{n\u003e1} 1/a(n) = (21 - 2*Pi^2)/6. - _Enrique Pérez Herrero_, Apr 01 2013",
				"a(n) = Sum_{k=0..n-1} k*A000217(2*k+1). - _Bruno Berselli_, Sep 04 2013",
				"a(n) = 2*A000217(n-1)*A000217(n). - _Gionata Neri_, May 04 2015",
				"a(n) = Sum_{i=1..n^2-1} i. - _Wesley Ivan Hurt_, Nov 24 2015",
				"E.g.f.: exp(x)*x^2*(6 + 6*x + x^2)/2. - _Stefano Spezia_, Jun 06 2021",
				"Sum_{n\u003e=2} (-1)^n/a(n) = Pi^2/6 - 3/2. - _Amiram Eldar_, Nov 02 2021"
			],
			"maple": [
				"A083374 := proc(n) n^2*(n^2-1)/2 ; end proc: # _R. J. Mathar_, Aug 23 2011"
			],
			"mathematica": [
				"Table[n^2*(n^2-1)/2, {n,40}] (* _T. D. Noe_, Oct 25 2006 *)"
			],
			"program": [
				"(PARI) a(n)=binomial(n^2,2) \\\\ _Charles R Greathouse IV_, Aug 23 2011",
				"(MAGMA) [n^2*(n^2-1)/2: n in [1..40]]; // _Vincenzo Librandi_, Sep 14 2011",
				"(MAGMA) A000217:=func\u003ci | i*(i+1)/2\u003e; [\u0026+[k*A000217(2*k+1): k in [0..n-1]]: n in [1..40]]; // _Bruno Berselli_, Sep 04 2013"
			],
			"xref": [
				"Cf. A000217, A000578, A002415, A006002, A006011, A008911, A037270, A047928, A055112."
			],
			"keyword": "easy,nonn",
			"offset": "1,2",
			"author": "Alan Sutcliffe (alansut(AT)ntlworld.com), Jun 05 2003",
			"ext": [
				"Corrected and extended by _T. D. Noe_, Oct 25 2006"
			],
			"references": 37,
			"revision": 93,
			"time": "2021-11-02T06:29:46-04:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}