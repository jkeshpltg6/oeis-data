{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A075125",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 75125,
			"data": "0,0,0,1,0,2,2,5,10,21,46,102,230,526,1216,2838,6678,15825,37734,90469,217962,527418,1281250,3123603,7639784,18740795,46096732,113666820,280928470,695796891,1726744166,4293121609,10692145390,26671959375,66634602702",
			"name": "Number of parallelogram polyominoes of site-perimeter n (also called staircase polyominoes, although that term is overused).",
			"comment": [
				"a(n) is the number of Dyck n-paths with no UDU's and no DUD's (A004148) whose first ascent is of length 3. For example, a(5)=2 counts UUUDDUUDDD, UUUDDDUUDD. - _David Callan_, May 08 2007",
				"From _Emeric Deutsch_, Nov 07 2009: (Start)",
				"a(n) = Sum_{k\u003e=0} k*A166299(n-2,k).",
				"Number of UUDD's starting at level 0 in all Dyck paths of semilength n-2 that have no ascents and no descents of length 1. Example: a(6)=2 because in UUDDUUDD and UUUUDDDD we have 2 + 0 = 2 UUDD's starting at level 0. (The Dyck paths having no ascents and no descents of length 1 are enumerated by the secondary structure numbers A004148).",
				"(End)"
			],
			"reference": [
				"M. P. Delest, D. Gouyou-Beauchamps and B. Vauquelin, Enumeration of parallelogram polyominoes with given bond and site parameter, Graphs and Combinatorics, 3(1987),325-339. [From _Emeric Deutsch_, Nov 07 2009]"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A075125/b075125.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"M. Bousquet-Mélou and A. Rechnitzer, \u003ca href=\"http://www.labri.fr/Perso/~bousquet/Articles/convexes.html\"\u003eThe site-perimeter of bargraphs\u003c/a\u003e, Adv. in Appl. Math. 31 (2003), 86-112."
			],
			"formula": [
				"G.f.: p^2/2*(1-p^2-2*p^3+p^4-(1+p-p^2)*sqrt((1+p+p^2)*(1-3*p+p^2)));",
				"a(n) ~ sqrt(2) * ((3+sqrt(5))/2)^n / (sqrt(377 + 843/sqrt(5)) * sqrt(Pi) * n^(3/2)). - _Vaclav Kotesovec_, Mar 21 2014. Equivalently, a(n) ~ 5^(1/4) * phi^(2*n - 7) / (sqrt(Pi) * n^(3/2)), where phi = A001622 is the golden ratio. - _Vaclav Kotesovec_, Dec 07 2021",
				"Conjecture: -(2*n-11)*(n-2)*(2*n-9)*a(n) +4*(2*n-11)*(n-3)*(n-5)*a(n-1) +(4*n^3-60*n^2+317*n-582)*a(n-2) +2*(2*n-7)*(2*n^2-26*n+81)*a(n-3) -(n-10)*(2*n-7)*(2*n-9)*a(n-4)=0. - _R. J. Mathar_, May 30 2016",
				"a(n) = 2 * Sum_{k=floor((n-2)/2)..n-3} C(k-2,2*k-n+2)*C(k+1,n-k-3)/(k+1). - _Vladimir Kruchinin_, Oct 12 2020"
			],
			"maple": [
				"G := 4*z^4/(1+z-z^2+sqrt((1+z+z^2)*(1-3*z+z^2)))^2: Gser := series(G, z = 0, 32): seq(coeff(Gser, z, n), n = 1 .. 30); # _Emeric Deutsch_, Nov 07 2009"
			],
			"mathematica": [
				"Rest[CoefficientList[Series[4 x^4/(1 + x - x^2 + Sqrt[(1 + x + x^2) (1 - 3 x + x^2)])^2, {x, 0, 40}], x]] (* _Vaclav Kotesovec_, Mar 21 2014 *)"
			],
			"program": [
				"(Maxima)",
				"a(n):=2*sum((binomial(k-2,2*k-n+2)*binomial(k+1,n-k-3))/(k+1),k,floor((n-2)/2),n-3); /* _Vladimir Kruchinin_, Oct 12 2020 */"
			],
			"xref": [
				"Cf. A004148, A006958, A075126, A166299."
			],
			"keyword": "nonn",
			"offset": "1,6",
			"author": "Andrew Rechnitzer (a.rechnitzer(AT)ms.unimelb.edu.au), Sep 09 2002",
			"ext": [
				"Offset changed to 1 by _Emeric Deutsch_, Nov 07 2009",
				"More terms from _Vincenzo Librandi_, Mar 22 2014",
				"Name modified by _Alois P. Heinz_, Sep 21 2016"
			],
			"references": 4,
			"revision": 35,
			"time": "2021-12-07T02:49:01-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}