{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A060125",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 60125,
			"data": "0,1,2,5,4,3,6,7,14,23,22,15,12,19,8,11,16,21,18,13,20,17,10,9,24,25,26,29,28,27,54,55,86,119,118,87,84,115,56,59,88,117,114,85,116,89,58,57,48,49,74,101,100,75,30,31,38,47,46,39,60,67,80,107,112,93,66,61,92",
			"name": "Self-inverse infinite permutation which shows the position of the inverse of each finite permutation in A060117 (or A060118) in the same sequence; or equally, the cross-indexing between A060117 and A060118.",
			"comment": [
				"PermRank3Aux is a slight modification of rank2 algorithm presented in Myrvold-Ruskey article."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A060125/b060125.txt\"\u003eTable of n, a(n) for n = 0..5040\u003c/a\u003e",
				"W. Myrvold and F. Ruskey, \u003ca href=\"https://doi.org/10.1016/S0020-0190(01)00141-7\"\u003eRanking and Unranking Permutations in Linear Time\u003c/a\u003e, Inform. Process. Lett. 79 (2001), no. 6, 281-284.",
				"\u003ca href=\"/index/Fa#facbase\"\u003eIndex entries for sequences related to factorial base representation\u003c/a\u003e",
				"\u003ca href=\"/index/Per#IntegerPermutation\"\u003eIndex entries for sequences that are permutations of the natural numbers\u003c/a\u003e"
			],
			"maple": [
				"with(group); permul := (a,b) -\u003e mulperms(b,a); swap := (p,i,j) -\u003e convert(permul(convert(p,'disjcyc'),[[i,j]]),'permlist',nops(p));",
				"PermRank3Aux := proc(n, p, q) if(1 = n) then RETURN(0); else RETURN((n-p[n])*((n-1)!) + PermRank3Aux(n-1,swap(p,n,q[n]),swap(q,n,p[n]))); fi; end;",
				"PermRank3R := p -\u003e PermRank3Aux(nops(p),p,convert(invperm(convert(p,'disjcyc')),'permlist',nops(p)));",
				"PermRank3L := p -\u003e PermRank3Aux(nops(p),convert(invperm(convert(p,'disjcyc')),'permlist',nops(p)),p);",
				"# a(n) = PermRank3L(PermUnrank3R(n)) or PermRank3R(PermUnrank3L(n)) or PermRank3L(convert(invperm(convert(PermUnrank3L(j), 'disjcyc')), 'permlist', nops(PermUnrank3L(j))))"
			],
			"xref": [
				"Cf. A060117, A060118, A060126, A060127, A275957, A275958.",
				"Cf. A261220 (fixed points).",
				"Cf. A056019 (compare the scatter plots)."
			],
			"keyword": "nonn,base,look",
			"offset": "0,3",
			"author": "_Antti Karttunen_, Mar 02 2001",
			"references": 18,
			"revision": 20,
			"time": "2020-05-14T07:41:35-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}