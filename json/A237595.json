{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A237595",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 237595,
			"data": "0,1,3,0,3,1,3,3,3,1,5,2,6,3,4,2,6,3,7,3,2,6,8,1,10,3,5,8,9,2,9,6,3,5,14,5,11,6,9,3,13,8,11,8,8,6,8,8,11,9,6,12,15,10,11,5,11,12,13,9,12,9,5,17,15,9,18,13,11,12",
			"name": "a(n) = |{1 \u003c= k \u003c= n: n + pi(k^2) is prime}|, where pi(.) is given by A000720.",
			"comment": [
				"Conjecture: (i) a(n) \u003e 0 for all n \u003e 4.",
				"(ii) If n \u003e 1 then n + pi(k*(k-1)) is prime for some k = 1, ..., n.",
				"(iii) For any integer n \u003e 1, there is a positive integer k \u003c= (n+1)/2 such that pi(n + k*(k+1)/2) is prime.",
				"(iv) Any integer n \u003e 1 can be written as p + pi(k*(k+1)/2), where p is a prime and k is among 1, ..., n-1."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A237595/b237595.txt\"\u003eTable of n, a(n) for n = 1..3000\u003c/a\u003e",
				"Z.-W. Sun, \u003ca href=\"http://arxiv.org/abs/1402.6641\"\u003eProblems on combinatorial properties of primes\u003c/a\u003e, arXiv:1402.6641, 2014"
			],
			"example": [
				"a(2) = 1 since 2 + pi(1^2) = 2 is prime.",
				"a(6) = 1 since 6 + pi(6^2) = 6 + 11 = 17 is prime.",
				"a(10) = 1 since 10 + pi(5^2) = 10 + 9 = 19 is prime.",
				"a(21) = 2 since 21 + pi(2^2) = 23 and 21 + pi(9^2) = 43 are both prime.",
				"a(24) = 1 since 24 + pi(21^2) = 24 + 85 = 109 is prime."
			],
			"mathematica": [
				"a[n_]:=Sum[If[PrimeQ[n+PrimePi[k^2]],1,0],{k,1,n}]",
				"Table[a[n],{n,1,70}]"
			],
			"xref": [
				"Cf. A000040, A000720, A237453, A237496, A237497, A237578, A237582."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Zhi-Wei Sun_, Feb 09 2014",
			"references": 3,
			"revision": 9,
			"time": "2014-04-06T22:18:51-04:00",
			"created": "2014-02-10T00:44:30-05:00"
		}
	]
}