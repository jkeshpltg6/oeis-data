{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A246928",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 246928,
			"data": "1,2,0,4,10,0,4,16,0,2,8,0,12,8,0,16,26,0,0,24,0,8,8,0,20,10,0,4,32,0,8,48,0,8,16,0,10,8,0,32,40,0,8,24,0,0,16,0,28,18,0,24,40,0,4,64,0,8,8,0,32,24,0,16,58,0,16,24,0,16,16,0,0,16,0,28",
			"name": "Number of integer solutions to x^2 + 3*y^2 + 3*z^2 = n.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A246928/b246928.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of phi(q) * phi(q^3)^2 in powers of q where phi() is a Ramanujan theta function.",
				"Expansion of eta(q^2)^5 * eta(q^6)^10 / (eta(q)^2 * eta(q^3)^4 * eta(q^4)^2 * eta(q^12)^4) in powers of q.",
				"Euler transform of period 12 sequence [ 2, -3, 6, -1, 2, -9, 2, -1, 6, -3, 2, -3, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (64 t)) = 24^(1/2) (t/i)^(3/2) g(t) where q = exp(2 Pi i t) and g() is the g.f. for A034933.",
				"G.f. theta_3(q) * theta_3(q^3)^2.",
				"a(3*n) = A034933(n). a(3*n + 1) = 2 * A246926(n). a(3*n + 2) = 0."
			],
			"example": [
				"G.f. = 1 + 2*q + 4*q^3 + 10*q^4 + 4*q^6 + 16*q^7 + 2*q^9 + 8*q^10 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 3, 0, q] EllipticTheta[ 3, 0, q^3]^2, {q, 0, n}]; (* _Michael Somos_, Jan 08 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n==0, qfrep( [ 1, 0, 0; 0, 3, 0; 0, 0, 3], n)[n] * 2)};",
				"(PARI) {a(n) = local(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^5 * eta(x^6 + A)^10 / (eta(x + A)^2 * eta(x^3 + A)^4 * eta(x^4 + A)^2 * eta(x^12 + A)^4), n))};",
				"(MAGMA) A := Basis( ModularForms( Gamma1(12), 3/2), 76); A[1] + 2*A[2] + 4*A[4] + 10*A[5];"
			],
			"xref": [
				"Cf. A034933, A246926."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Michael Somos_, Sep 07 2014",
			"references": 2,
			"revision": 18,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2014-09-07T19:23:37-04:00"
		}
	]
}