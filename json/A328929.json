{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A328929",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 328929,
			"data": "0,2,1,1,1,1,1,1,1,1,2,2,3,3,2,2,3,2,3,2,2,3,3,2,3,3,2,2,3,3,4,3,3,2,3,4,2,3,2,3,2,3,2,3,4,3,4,5,3,3,4,5,3,4,5,4,4,5,6,4,5,3,4,5,4,5,6,4,3,4,5,4,4,5,4,4,5,4,3,4,5,4,4,5,4,4,5,4,5,4,4,5,4,4,5,6,5,5,6",
			"name": "Squares visited by a knight moving on a square-ringed numbered board where the knight moves to the smallest numbered unvisited square; the minimum distance from the origin is used if the square numbers are equal; the smallest spiral number ordering is used if the distances are equal.",
			"comment": [
				"This sequence uses the number of the square ring of squares surrounding the 0-numbered origin to enumerate each square on the board. At each step the knight goes to an unvisited square with the smallest square number. If the knight has a choice of two or more squares with the same number it then chooses the square which is the closest to the 0-squared origin. If two or more squares are found which also have the same distance to the origin, then the square which was first drawn in a square spiral numbering is chosen, i.e., the smallest spiral numbered square as per A316667.",
				"The sequence is finite. After 25108 steps a square with the number 73 (standard spiral number = 21041) is visited, after which all neighboring squares have been visited."
			],
			"link": [
				"M. F. Hasler, \u003ca href=\"/wiki/Knight_tours\"\u003eKnight tours\u003c/a\u003e, OEIS wiki, Nov. 2019.",
				"Scott R. Shannon, \u003ca href=\"/A328929/a328929.png\"\u003eImage showing the 25108 steps of the knight's path\u003c/a\u003e. The green dot is the first square and the red dot the last. Blue dots show the eight occupied squares surrounding the final square; the final square is on the boundary at about the 4:30 clock position.",
				"N. J. A. Sloane and Brady Haran, \u003ca href=\"https://www.youtube.com/watch?v=RGQe8waGJ4w\"\u003eThe Trapped Knight\u003c/a\u003e, Numberphile video (2019)."
			],
			"formula": [
				"a(n) = max(|A174344(p)|, |A274923(p)|), p = A328908(n)+1. - _M. F. Hasler_, Nov 04 2019"
			],
			"example": [
				"The squares are labeled using the number of the square ring of squares surrounding the origin:",
				".",
				"    +---+---+---+---+---+---+---+",
				"    | 3 | 3 | 3 | 3 | 3 | 3 | 3 |",
				"    +---+---+---+---+---+---+---+",
				"    | 3 | 2 | 2 | 2 | 2 | 2 | 3 |",
				"    +---+---+---+---+---+---+---+",
				"    | 3 | 2 | 1 | 1 | 1 | 2 | 3 |",
				"    +---+---+---+---+---+---+---+",
				"    | 3 | 2 | 1 | 0 | 1 | 2 | 3 |",
				"    +---+---+---+---+---+---+---+",
				"    | 3 | 2 | 1 | 1 | 1 | 2 | 3 |",
				"    +---+---+---+---+---+---+---+",
				"    | 3 | 2 | 2 | 2 | 2 | 2 | 3 |",
				"    +---+---+---+---+---+---+---+",
				"    | 3 | 3 | 3 | 3 | 3 | 3 | 3 |",
				"    +---+---+---+---+---+---+---+",
				".",
				"If the knight has a choice of two or more squares with the same number which also have the same distance from the origin, then the square with the minimum square spiral number, as shown in A316667, is chosen."
			],
			"program": [
				"(PARI) A328929(n)=normlp(coords(A328909(n))) \\\\ with coords() defined in A328909 or in A296030. - _M. F. Hasler_, Nov 04 2019"
			],
			"xref": [
				"Cf. A316667, A326922.",
				"Cf. A328909 (number of the visited square, following spiral numbering).",
				"Cf. A326922 (variant using Euclidean or L2-norm), A328928 (variant with 1-norm = taxicab distance); A326924, A328908 (corresponding trajectories, i.e., spiral number of squares)."
			],
			"keyword": "nonn,fini",
			"offset": "0,2",
			"author": "_Scott R. Shannon_, Oct 31 2019",
			"references": 13,
			"revision": 18,
			"time": "2021-05-04T01:08:41-04:00",
			"created": "2019-10-31T12:02:01-04:00"
		}
	]
}