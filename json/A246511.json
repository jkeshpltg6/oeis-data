{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A246511",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 246511,
			"data": "1,-13,103,219,-26139,503957,-4066061,-54914149,2550230113,-43157232273,192777017511,10118180981037,-318814450789587,4344955121014089,6807591584551563,-1781238363905009253,42912636577174295769,-425791821468024981709,-5452095049517604924017,305524943325956601071159",
			"name": "a(n) = (Sum_{k=0..n-1} (-1)^k*(2k+1)*C(n-1,k)^2*C(n+k,k)^2)/n, where C(n,k) denotes the binomial coefficient n!/(k!*(n-k)!).",
			"comment": [
				"Zhi-Wei Sun proved that a(n) is always an integer, and that Sum_{k=0..n-1}(2k+1)*A(k) = n^3*a(n), where A(k) = Sum_{j=0..k} (-1)^j*(2j+1)^2*C(k,j)^2*C(k+j,j)^2.",
				"The Zeilberger algorithm could yield a complicated fourth-order recurrence for this sequence."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A246511/b246511.txt\"\u003eTable of n, a(n) for n = 1..100\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1408.5381\"\u003eTwo new kinds of numbers and their arithmetic properties\u003c/a\u003e, arXiv:1408.5381 [math.NT], 2014-2018."
			],
			"formula": [
				"a(n) = hypergeom([3/2, 1-n, 1-n, n+1, n+1], [1/2, 1, 1, 1], -1)/n. - _Robert Israel_, Aug 28 2014",
				"Recurrence: (n-1)^2*n^3*(2*n-7)*(2*n-5)*(40*n^6 - 600*n^5 + 3612*n^4 - 11120*n^3 + 18354*n^2 - 15270*n + 4949)*a(n) = -2*(n-1)^2*(2*n-7)*(1120*n^10 - 21280*n^9 + 173136*n^8 - 789528*n^7 + 2217244*n^6 - 3965700*n^5 + 4511984*n^4 - 3162198*n^3 + 1267357*n^2 - 247675*n + 14910)*a(n-1) - 2*(n-2)*(2*n-7)*(2*n-1)*(9080*n^10 - 181600*n^9 + 1569004*n^8 - 7670464*n^7 + 23311258*n^6 - 45445432*n^5 + 56332869*n^4 - 42029480*n^3 + 16243359*n^2 - 1773884*n - 347928)*a(n-2) - 2*(n-3)^2*(2*n-1)*(1120*n^10 - 23520*n^9 + 213456*n^8 - 1095144*n^7 + 3485308*n^6 - 7092252*n^5 + 9139424*n^4 - 7057450*n^3 + 2811541*n^2 - 317773*n - 61278)*a(n-3) - (n-4)^3*(n-3)^2*(2*n-3)*(2*n-1)*(40*n^6 - 360*n^5 + 1212*n^4 - 1872*n^3 + 1266*n^2 - 234*n - 35)*a(n-4). - _Vaclav Kotesovec_, Sep 07 2014"
			],
			"example": [
				"a(2) = -13 since Sum_{k=0,1}(-1)^k*(2k+1)C(1,k)^2*C(2+k,k)^2 = 1 - 3*3^2 = 2*(-13)."
			],
			"maple": [
				"a:= n -\u003e add((-1)^k*(2*k+1)*binomial(n-1,k)^2*binomial(n+k,k)^2,k=0..n-1)/n:",
				"seq(a(n),n=1..40); # _Robert Israel_, Aug 28 2014"
			],
			"mathematica": [
				"a[n_]:=Sum[(-1)^k*(2k+1)*Binomial[n-1,k]^2*Binomial[n+k,k]^2,{k,0,n-1}]/n",
				"Table[a[n],{n,1,20}]"
			],
			"xref": [
				"Cf. A246065, A246138, A246459, A246460, A246461, A246462."
			],
			"keyword": "sign",
			"offset": "1,2",
			"author": "_Zhi-Wei Sun_, Aug 27 2014",
			"references": 5,
			"revision": 18,
			"time": "2019-08-05T05:33:33-04:00",
			"created": "2014-08-28T05:37:53-04:00"
		}
	]
}