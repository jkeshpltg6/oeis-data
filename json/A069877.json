{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A069877",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 69877,
			"data": "1,2,4,8,16,32,64,128,256,512,2,6,12,24,48,96,192,384,768,1536,4,12,36,72,144,288,576,1152,2304,4608,8,24,72,216,432,864,1728,3456,6912,13824,16,48,144,432,1296,2592,5184,10368,20736,41472,32,96,288,864,2592,7776,15552,31104,62208,124416,64,192,576,1728,5184,15552,46656,93312,186624,373248,128",
			"name": "Smallest number with a prime signature whose indices are the decimal digits of n.",
			"comment": [
				"From _Antti Karttunen_, Nov 17 2016: (Start)",
				"This is a filter-sequence for decimal base: a(n) = the least number with the same prime signature as A054842(n).",
				"This sequence can be used for filtering certain base-10 related sequences, because it matches only with any such sequence b that can be computed as b(n) = f(A054842(n)), where f(n) is any function that depends only on the prime signature of n (some of these are listed under the index entry for \"sequences computed from exponents in ...\").",
				"Matching in this context means that the sequence a matches with the sequence b iff for all i, j: a(i) = a(j) =\u003e b(i) = b(j). In other words, iff the sequence b partitions the natural numbers to the same or coarser equivalence classes (as/than the sequence a) by the distinct values it obtains.",
				"Any such sequence should match where the result is computed from the nonzero decimal digits of n, but does not depend on their order. These include for example, A007953 (digital sum and any of its variants), A010888 (digital root of n) and A051801 (product of the nonzero digits of n). As of Nov 11 2016, there were a couple of hundred such sequences that seemed to match with this one. These are given at the \"List of sequences whose equivalence classes ...\" link.",
				"(End)"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A069877/b069877.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"A. Karttunen, \u003ca href=\"/A069877/a069877.txt\"\u003eList of sequences whose equivalence classes seem to match with this sequence (as of Nov 11 2016)\u003c/a\u003e",
				"\u003ca href=\"/index/Eu#epf\"\u003eIndex entries for sequences computed from exponents in factorization of n\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A046523(A054842(n)). - _Antti Karttunen_, Nov 16 2016"
			],
			"example": [
				"a(12) = 2^2 * 3^1 = 12. a(231) = 2^3 * 3^2 * 5^1 = 360."
			],
			"xref": [
				"Cf. A046523, A054842, A061509.",
				"Cf. A278222, A278226, A278236 for similar filter sequences constructed for other bases.",
				"Sequences that partition N into same or coarser equivalence classes: too numerous to list all here, but at least A007953, A010888, A051801 are included. See the separate list given in links."
			],
			"keyword": "nonn,base",
			"offset": "0,2",
			"author": "_Amarnath Murthy_, Apr 25 2002",
			"ext": [
				"More terms from Larry Reeves (larryr(AT)acm.org), Jul 05 2002",
				"a(0)=1 prepended and more terms added by _Antti Karttunen_, Nov 16 2016"
			],
			"references": 6,
			"revision": 40,
			"time": "2016-11-19T08:24:30-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}