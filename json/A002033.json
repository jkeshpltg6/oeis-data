{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002033",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2033,
			"id": "M0131 N0053",
			"data": "1,1,1,2,1,3,1,4,2,3,1,8,1,3,3,8,1,8,1,8,3,3,1,20,2,3,4,8,1,13,1,16,3,3,3,26,1,3,3,20,1,13,1,8,8,3,1,48,2,8,3,8,1,20,3,20,3,3,1,44,1,3,8,32,3,13,1,8,3,13,1,76,1,3,8,8,3,13,1,48,8,3,1,44,3,3,3,20,1,44,3,8,3,3,3,112",
			"name": "Number of perfect partitions of n.",
			"comment": [
				"A perfect partition of n is one which contains just one partition of every number less than n when repeated parts are regarded as indistinguishable. Thus 1^n is a perfect partition for every n; and for n = 7, 4 1^3, 4 2 1, 2^3 1 and 1^7 are all perfect partitions. [Riordan]",
				"Also number of ordered factorizations of n+1, see A074206.",
				"Also number of gozinta chains from 1 to n (see A034776). - _David W. Wilson_",
				"a(n) is the permanent of the n X n matrix with (i,j) entry = 1 if j|i+1 and = 0 otherwise. For n=3 the matrix is {{1, 1, 0}, {1, 0, 1}, {1, 1, 0}} with permanent = 2. - _David Callan_, Oct 19 2005",
				"Appears to be the number of permutations that contribute to the determinant that gives the Moebius function. Verified up to a(9). - _Mats Granvik_, Sep 13 2008",
				"Dirichlet inverse of A153881 (assuming offset 1). - _Mats Granvik_, Jan 03 2009",
				"Equals row sums of triangle A176917. - _Gary W. Adamson_, Apr 28 2010",
				"A partition is perfect iff it is complete (A126796) and knapsack (A108917). - _Gus Wiseman_, Jun 22 2016",
				"a(n) is also the number of series-reduced planted achiral trees with n + 1 unlabeled leaves, where a rooted tree is series-reduced if all terminal subtrees have at least two branches, and achiral if all branches directly under any given node are equal. Also Moebius transform of A067824. - _Gus Wiseman_, Jul 13 2018"
			],
			"reference": [
				"L. Comtet, Advanced Combinatorics, Reidel, 1974, p. 126, see #27.",
				"R. Honsberger, Mathematical Gems III, M.A.A., 1985, p. 141.",
				"D. E. Knuth, The Art of Computer Programming, Pre-Fasc. 3b, Sect. 7.2.1.5, no. 67, p. 25.",
				"P. A. MacMahon, The theory of perfect partitions and the compositions of multipartite numbers, Messenger Math., 20 (1891), 103-119.",
				"J. Riordan, An Introduction to Combinatorial Analysis, Wiley, 1958, pp. 123-124.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A002033/b002033.txt\"\u003eTable of n, a(n) for n = 0..9999\u003c/a\u003e",
				"Daniele A. Gewurz and Francesca Merola, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL6/Gewurz/gewurz5.html\"\u003eSequences realized as Parker vectors of oligomorphic permutation groups\u003c/a\u003e, J. Integer Seqs., Vol. 6, 2003.",
				"HoKyu Lee, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2006.01.007\"\u003eDouble perfect partitions\u003c/a\u003e, Discrete Math., 306 (2006), 519-525.",
				"Paul Pollack, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9939-2012-11254-7\"\u003eOn the parity of the number of multiplicative partitions and related problems\u003c/a\u003e, Proc. Amer. Math. Soc. 140 (2012), 3793-3803.",
				"J. Riordan, \u003ca href=\"/A002033/a002033.pdf\"\u003eLetter to N. J. A. Sloane, Dec. 1970\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PerfectPartition.html\"\u003ePerfect Partition\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DirichletSeriesGeneratingFunction.html\"\u003eDirichlet Series Generating Function\u003c/a\u003e",
				"\u003ca href=\"/index/Cor#core\"\u003eIndex entries for \"core\" sequences\u003c/a\u003e"
			],
			"formula": [
				"From _David Wasserman_, Nov 14 2006: (Start)",
				"a(n-1) = Sum_{i|d, i \u003c n} a(i-1).",
				"a(p^k-1) = 2^(k-1).",
				"a(n-1) = A067824(n)/2 for n \u003e 1.",
				"a(A122408(n)-1) = A122408(n)/2. (End)",
				"a(A025487(n)-1) = A050324(n). - _R. J. Mathar_, May 26 2017",
				"a(n) = (A253249(n+1)+1)/4, n \u003e 0. - _Geoffrey Critzer_, Aug 19 2020"
			],
			"example": [
				"n=0: 1 (the empty partition)",
				"n=1: 1 (1)",
				"n=2: 1 (11)",
				"n=3: 2 (21, 111)",
				"n=4: 1 (1111)",
				"n=5: 3 (311, 221, 11111)",
				"n=6: 1 (111111)",
				"n=7: 4 (4111, 421, 2221, 1111111)",
				"From _Gus Wiseman_, Jul 13 2018: (Start)",
				"The a(11) = 8 series-reduced planted achiral trees with 12 unlabeled leaves:",
				"  (oooooooooooo)",
				"  ((oooooo)(oooooo))",
				"  ((oooo)(oooo)(oooo))",
				"  ((ooo)(ooo)(ooo)(ooo))",
				"  ((oo)(oo)(oo)(oo)(oo)(oo))",
				"  (((ooo)(ooo))((ooo)(ooo)))",
				"  (((oo)(oo)(oo))((oo)(oo)(oo)))",
				"  (((oo)(oo))((oo)(oo))((oo)(oo)))",
				"(End)"
			],
			"maple": [
				"a := array(1..150): for k from 1 to 150 do a[k] := 0 od: a[1] := 1: for j from 2 to 150 do for m from 1 to j-1 do if j mod m = 0 then a[j] := a[j]+a[m] fi: od: od: for k from 1 to 150 do printf(`%d,`,a[k]) od: # _James A. Sellers_, Dec 07 2000",
				"# alternative",
				"A002033 := proc(n)",
				"    option remember;",
				"    local a;",
				"    if n \u003c= 2 then",
				"        return 1;",
				"    else",
				"        a := 0 ;",
				"        for i from 0 to n-1 do",
				"            if modp(n+1,i+1) = 0 then",
				"                a := a+procname(i);",
				"            end if;",
				"        end do:",
				"    end if;",
				"    a ;",
				"end proc: # _R. J. Mathar_, May 25 2017"
			],
			"mathematica": [
				"a[0] = 1; a[1] = 1; a[n_] := a[n] = a /@ Most[Divisors[n]] // Total; a /@ Range[96]  (* _Jean-François Alcover_, Apr 06 2011, updated Sep 23 2014. NOTE: This produces A074206(n) = a(n-1). - _M. F. Hasler_, Oct 12 2018 *)"
			],
			"program": [
				"(PARI) A002033(n) = if(n,sumdiv(n+1,i,if(i\u003c=n,A002033(i-1))),1) \\\\ _Michael B. Porter_, Nov 01 2009, corrected by _M. F. Hasler_, Oct 12 2018",
				"(Python)",
				"from functools import lru_cache",
				"from sympy import divisors",
				"@lru_cache(maxsize=None)",
				"def A002033(n):",
				"    if n \u003c= 1:",
				"        return 1",
				"    return sum(A002033(i-1) for i in divisors(n+1,generator=True) if i \u003c= n) # _Chai Wah Wu_, Jan 12 2022"
			],
			"xref": [
				"Same as A074206, up to the offset and initial term there.",
				"Cf. A001055, A050324.",
				"a(A002110)=A000670.",
				"Cf. A000123, A100529, A117621.",
				"Cf. A176917.",
				"For parity see A008966.",
				"Cf. A126796, A108917.",
				"Cf. A001678, A003238, A067824, A167865, A214577, A289078, A292504, A316782."
			],
			"keyword": "nonn,core,easy,nice,changed",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Edited by _M. F. Hasler_, Oct 12 2018"
			],
			"references": 221,
			"revision": 109,
			"time": "2022-01-12T20:25:25-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}