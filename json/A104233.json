{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A104233",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 104233,
			"data": "125,128,216,243,256,343,512,625,729,1000,1015,1016,1017,1018,1019,1020,1021,1022,1023,1024,1025,1026,1027,1028,1029,1030,1031,1032,1033,1080,1089,1125,1152,1156,1215,1225,1250,1280,1287,1288,1289,1290,1291,1292,1293,1294",
			"name": "Positive integers which have a \"compact\" representation using fewer decimal digits than just writing the number normally.",
			"comment": [
				"You are allowed to use the following symbols as well:",
				"( ) grouping",
				"+ addition",
				"- subtraction",
				"* multiplication",
				"/ division",
				"^ exponentiation",
				"Note that 1015 to 1033 are all representable in the form 4^5-d or 4^5+d, where d is a single digit.",
				"The complexity of a number has been defined in several different ways by different authors. See the Index to the OEIS for other definitions. - _Jonathan Vos Post_, Apr 02 2005",
				"From _Bernard Schott_, Feb 10 2021: (Start)",
				"These numbers are called \"entiers compressibles\" in French.",
				"There are no 1-digit or 2-digit terms.",
				"The 3-digit terms are all of the form m^q, with m, q \u003e 1.",
				"The 4-digit terms are of the form m^q with m, q \u003e 1, or of the form m^q+-d or m^q*r with m, q, r \u003e 1, d \u003e= 0, and m, q, r, d are all digits (see examples where [...] is a corresponding \"compact\" representation. (End)"
			],
			"reference": [
				"R. K. Guy, Unsolved Problems Number Theory, Sect. F26."
			],
			"link": [
				"J. Arias de Reyna, \u003ca href=\"/A005245/a005245_1.pdf\"\u003eComplejidad de los números naturales\u003c/a\u003e, Gaceta R. Soc. Mat. Esp., 3 (2000), 230-250. [Cached copy, with permission]",
				"J. Arias de Reyna, \u003ca href=\"https://arxiv.org/abs/2111.03345\"\u003eComplexity of natural numbers\u003c/a\u003e, arXiv:2111.03345 [math.NT], 2021.",
				"Diophante, \u003ca href=\"http://www.diophante.fr/problemes-par-themes/arithmetique-et-algebre/a1-pot-pourri/1861-a164-les-entiers-compressibles\"\u003eA164, Les entiers compressibles\u003c/a\u003e (in French).",
				"R. K. Guy, \u003ca href=\"http://www.jstor.org/stable/2323338\"\u003eSome suspiciously simple sequences\u003c/a\u003e, Amer. Math. Monthly 93 (1986), 186-190; 94 (1987), 965; 96 (1989), 905.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/IntegerComplexity.html\"\u003eInteger Complexity\u003c/a\u003e"
			],
			"example": [
				"From _Bernard Schott_, Feb 10 2021: (Start)",
				"a(1) = 125 = [5^3] = 5*5*5 is the smallest cube.",
				"a(5) = 256 = [2^8] = [4^4] = 16*16 is the smallest square.",
				"a(6) = 343 = [7^3] is the smallest palindrome.",
				"a(15) = 1019 = [4^5-5] is the smallest prime.",
				"6555 = [3^8-5] = [35^2] = T(49) = 49*50/2 is the smallest triangular number.",
				"362880 = 9! = [70*72^2] = [8*(6^6-6^4)] is the smallest factorial.",
				"The smallest zeroless pandigital number 123456789 = [(10^10-91)/81] = [3*(6415^2+38)] is a term. (End)"
			],
			"xref": [
				"Cf. A036057, A005245, A003313, A076142, A076091, A061373, A005421, A064097, A005520, A025280, A003037."
			],
			"keyword": "nonn,base",
			"offset": "1,1",
			"author": "_Jack Brennen_, Apr 01 2005",
			"ext": [
				"More terms from _Bernard Schott_, Feb 10 2021",
				"Missing terms added by _David A. Corneth_, Feb 14 2021"
			],
			"references": 0,
			"revision": 53,
			"time": "2021-11-08T02:22:47-05:00",
			"created": "2005-04-09T03:00:00-04:00"
		}
	]
}