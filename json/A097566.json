{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097566",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97566,
			"data": "1,1,0,1,5,5,1,5,20,20,6,20,65,65,25,66,185,185,85,190,481,482,250,501,1165,1170,666,1230,2666,2685,1646,2850,5827,5887,3830,6303,12251,12415,8487,13395,24912,25323,18052,27507,49215,50176,37072,54832,94781,96905",
			"name": "Number of partitions p of n for which Odd(p) = Odd(p') (mod 4), where p' is the conjugate of p.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Odd(p) is the number of odd parts of a partition p. a(n) is denoted t(n) in Problem 10969."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A097566/b097566.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"George E. Andrews, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v11i2r1\"\u003eOn a Partition Function of Richard Stanley\u003c/a\u003e, The Electronic Journal of Combinatorics, Volume 11, Issue 2 (2004-6) (The Stanley Festschrift volume), Research Paper #R1.",
				"M. Ishikawa and J. Zeng, \u003ca href=\"https://doi.org/10.1016/j.disc.2007.12.064\"\u003eThe Andrews-Stanley partition function and Al-Salam-Chihara polynomials\u003c/a\u003e, Disc. Math., 309 (2009), 151-175. (See t(n) p. 151. Note that there is a typo in the g.f. for f(n) - see A144558.) [Added by _N. J. A. Sloane_, Jan 25 2009.]",
				"Andrew V. Sills, \u003ca href=\"https://doi.org/10.1155/S0161171204401380\"\u003eA Combinatorial proof of a partition identity of Andrews and Stanley\u003c/a\u003e, International Journal of Mathematics and Mathematical Sciences, Volume 2004, Issue 47, Pages 2495-2501.",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"R. P. Stanley, \u003ca href=\"http://www.jstor.org/stable/3072410\"\u003eProblem 10969\u003c/a\u003e, Amer. Math. Monthly, 109 (2002), 760. as mentioned in link."
			],
			"formula": [
				"From _Michael Somos_, May 04 2011: (Start)",
				"Expansion of q^(1/24) * eta(q^2)^2 * eta(q^16)^5 / (eta(q) * eta(q^4)^5 * eta(q^32)^2) in powers of q.",
				"Expansion of phi(x^8) / (phi(x^2) * f(-x)) in powers of x where phi(), f() are Ramanujan theta functions.",
				"Euler transform of period 32 sequence [ 1, -1, 1, 4, 1, -1, 1, 4, 1, -1, 1, 4, 1, -1, 1, -1, 1, -1, 1, 4, 1, -1, 1, 4, 1, -1, 1, 4, 1, -1, 1, 1, ...].",
				"G.f.: theta_3(x^8) / (theta_3(x^2) * Product_{k\u003e0} (1 - x^k)) = A000041(x) * A112128(x^2).",
				"a(n) = (A000041(n) + A085261(n)) / 2.",
				"(End)"
			],
			"example": [
				"G.f. = 1 + x + x^3 + 5*x^4 + 5*x^5 + x^6 + 5*x^7 + 20*x^8 + 20*x^9 + 6*x^10 + ...",
				"G.f. = 1/q + q^23 + q^71 + 5*q^95 + 5*q^119 + q^143 + 5*q^167 + 20*q^191 + 20*q^215 + ...",
				"a(5) = 5 because only the partitions {5}, {3,2}, {3,1,1}, {2,2,1}, {1,1,1,1,1} have conjugates resp. {1,1,1,1,1}, {2,2,1}, {3,1,1}, {3,2}, {5} with matching counts of odd elements (resp. (1,5), (1,1), (3,3), (1,1), (5,1) being congruent modulo 4 )."
			],
			"maple": [
				"with(combinat); t1:=mul( (1+q^(2*n-1))/((1-q^(4*n))*(1+q^(4*n-2))^2), n=1..100): t2:=series(t1,q,100): f:=n-\u003ecoeff(t2,q,n); p:=numbpart; t:=n-\u003e(p(n)+f(n))/2; # _N. J. A. Sloane_, Jan 25 2009"
			],
			"mathematica": [
				"fStanley[n_Integer]:=Product[(1+q^(2i-1))/(1-q^(4i))/(1+q^(4i-2))^2, {i, n}]; Table[PartitionsP[n]/2+1/2*Coefficient[Series[fStanley[n], {q, 0, n+1}], q^n], {n, 64}] or Table[Count[Partitions[n], q_/;Mod[Count[q, w_/;OddQ[w]]- Count[TransposePartition[q], w_/;OddQ[w]], 4]===0], {n, 24}]",
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 3, 0, x^8] / (EllipticTheta[ 3, 0, x^2] QPochhammer[ x]), {x, 0, n}]; (* _Michael Somos_, Jun 01 2014 *)"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^2 * eta(x^16 + A)^5 / (eta(x + A) * eta(x^4 + A)^5 * eta(x^32 + A)^2), n))}; /* _Michael Somos_, May 04 2011 */"
			],
			"xref": [
				"Cf. A000041, A085261, A097567, A112128, A190101."
			],
			"keyword": "easy,nonn",
			"offset": "0,5",
			"author": "_Wouter Meeussen_, Aug 28 2004",
			"references": 4,
			"revision": 45,
			"time": "2021-03-12T22:24:43-05:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}