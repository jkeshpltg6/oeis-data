{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A285341",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 285341,
			"data": "1,0,1,1,1,0,1,0,1,1,1,0,1,1,1,0,1,1,1,0,1,0,1,1,1,0,1,0,1,1,1,0,1,1,1,0,1,1,1,0,1,0,1,1,1,0,1,1,1,0,1,1,1,0,1,0,1,1,1,0,1,1,1,0,1,1,1,0,1,0,1,1,1,0,1,0,1,1,1,0,1,1,1,0,1,1",
			"name": "Fixed point of the morphism 0 -\u003e 10, 1 -\u003e 1011.",
			"comment": [
				"From __Michel Dekking_, Sep 08 2019: (Start)",
				"A short proof of Mathar's conjecture can be obtained by using the fact that sigma and tau are conjugate morphisms. For all words w one has",
				"    tau(w) = 1^{-1} sigma(w) 1.",
				"The proof is again by induction. Suppose that",
				"    (A) : 0 sigma^n(0) = tau^n(0) 0",
				"holds. Then, choosing w = tau^n(0):",
				"    tau^{n+1}(0) 0 = tau(tau^n(0)) 0 =",
				"    1^{-1} sigma(tau^n(0)) 1 0 =",
				"    1^{-1} sigma(0 sigma^n(0) 0^{-1}) 10 =",
				"    1^{-1} 10 sigma^{n+1}(0) 0^{-1}1^{-1} 10 =",
				"    0 sigma^{n+1}.",
				"So (A) holds for n+1.",
				"(End)"
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A285341/b285341.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Fi#FIXEDPOINTS\"\u003eIndex entries for sequences that are fixed points of mappings\u003c/a\u003e"
			],
			"formula": [
				"Conjecture: a(n) = A284893(n+1). - _R. J. Mathar_, May 08 2017",
				"From _Michel Dekking_, Feb 05 2018: (Start)",
				"Proof of this conjecture: let sigma be the morphism 0 -\u003e 10, 1 -\u003e 1011, and let tau be the morphism 0 -\u003e 01, 1 -\u003e 0111, which has A284893 as a fixed point. It clearly suffices to prove the relation, for all n=1,2,3,...:",
				"      (A) : 0 sigma^n(0) = tau^n(0) 0",
				"To prove such a thing one needs a second relation, for all n=1,2,3,...:",
				"      (B) : 0 sigma^n(1) 0^{-1} = tau^n(0) tau^n(1) [tau^n(0)]^{-1}",
				"Here 0^{-1} and [tau^n(0)]^{-1} are the free group inverses of 0 and tau^n(0).",
				"For n=1, we do indeed have:",
				"    (a) 0 sigma(10) = 0101110 = 0101110 = tau(01)0",
				"    (b) 0 sigma(1) 0^{-1} tau(0) = 010111 = tau(0)tau(1).",
				"Using the induction hypothesis with (A) and (B) in the second line, one obtains",
				"       0 sigma^{n+1}(0) = 0 sigma^n(1) 0^{-1} 0 sigma^n(0)",
				"                        = tau^n(0) tau^n(1) [tau^n(0)]^{-1} tau^n(0) 0",
				"                        = tau^{n+1}(0) 0.",
				"Similarly,",
				"       0 sigma^{n+1}(1) 0^{-1}",
				"        = 0 sigma^n(1) 0^{-1} 0 sigma^n(0) 0^{-1} 0 sigma^n(11) 0^{-1}",
				"        = tau^n(0)tau^n(1)[tau^n(0)]^{-1} tau^n(0) 0 0^{-1}",
				"       tau^n(0) tau^n(11) [tau^n(0)]^{-1}",
				"        = tau^{n+1}(0) tau^{n+1}(1) [tau^n(1)]^{-1}[tau^n(0)]^{-1}",
				"        = tau^{n+1}(0) tau^{n+1}(1) [tau^{n+1}(0)]^{-1}. (End)"
			],
			"example": [
				"0 -\u003e 10-\u003e 1011 -\u003e 10111010111011."
			],
			"mathematica": [
				"s = Nest[Flatten[# /. {0 -\u003e {1, 0}, 1 -\u003e {1, 0, 1, 1}}] \u0026, {0}, 10]; (* A285341 *)",
				"u = Flatten[Position[s, 0]];  (* A285342 *)",
				"Flatten[Position[s, 1]];  (* A285343 *)",
				"u/2 (* A285344)"
			],
			"xref": [
				"Cf. A285342, A285343, A285344."
			],
			"keyword": "nonn,easy",
			"offset": "1",
			"author": "_Clark Kimberling_, Apr 25 2017",
			"references": 5,
			"revision": 31,
			"time": "2019-09-09T01:08:02-04:00",
			"created": "2017-04-25T11:46:18-04:00"
		}
	]
}