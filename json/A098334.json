{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A098334",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 98334,
			"data": "1,1,-7,-23,49,401,41,-5767,-11423,65569,299353,-441847,-5511791,-3665999,79937417,212712857,-861871423,-5076450239,3966949049,89482678313,110424995569,-1233175514671,-4202194115863,11830822055353,91629438996001,-13485315511199",
			"name": "Expansion of 1/sqrt(1-2x+17x^2).",
			"comment": [
				"Central coefficients of (1+x-4x^2)^n.",
				"Binomial transform of 1/sqrt(1+16x^2), or (1,0,-8,0,96,0,-1280,...).",
				"Binomial transform is A098337."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A098334/b098334.txt\"\u003eTable of n, a(n) for n = 0..200\u003c/a\u003e",
				"Hacène Belbachir, Abdelghani Mehdaoui, László Szalay, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL22/Szalay/szalay42.html\"\u003eDiagonal Sums in the Pascal Pyramid, II: Applications\u003c/a\u003e, J. Int. Seq., Vol. 22 (2019), Article 19.3.5.",
				"Tony D. Noe, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL9/Noe/noe35.html\"\u003eOn the Divisibility of Generalized Central Trinomial Coefficients\u003c/a\u003e, Journal of Integer Sequences, Vol. 9 (2006), Article 06.2.7."
			],
			"formula": [
				"E.g.f.: exp(x)BesselI(0, 4*I*x), I=sqrt(-1);",
				"a(n) = sum{k=0..floor(n/2), binomial(n, 2k)binomial(2k, k)(-4)^k};",
				"a(n) = sum{k=0..floor(n/2), binomial(n, k)binomial(n-k, k)(-4)^k);",
				"a(n) = sum{k=0..n, binomial(n, k)binomial(k, k/2)cos(pi*k/2)2^k}3.",
				"D-finite with recurrence: n*a(n) +(-2*n+1)*a(n-1) +17*(n-1)*a(n-2)=0. - _R. J. Mathar_, Nov 24 2012",
				"Lim sup n-\u003einfinity |a(n)|^(1/n) = sqrt(17). - _Vaclav Kotesovec_, Feb 09 2014",
				"a(n) = hypergeom([-n/2, 1/2-n/2], [1], -16). - _Peter Luschny_, Sep 18 2014",
				"a(n) = (sqrt(17))^n*P(n,1/sqrt(17)), where P(n,x) is the Legendre polynomial of degree n. - _Peter Bala_, Mar 18 2018"
			],
			"maple": [
				"a := n -\u003e hypergeom([-n/2, 1/2-n/2], [1], -16);",
				"seq(round(evalf(a(n),99)), n=0..28); # _Peter Luschny_, Sep 18 2014"
			],
			"mathematica": [
				"CoefficientList[Series[1/Sqrt[1-2*x+17*x^2], {x, 0, 20}], x] (* _Vaclav Kotesovec_, Feb 09 2014 *)"
			],
			"program": [
				"(PARI) x='x+O('x^99); Vec(1/(1-2*x+17*x^2)^(1/2)) \\\\ _Altug Alkan_, Mar 18 2018"
			],
			"xref": [
				"Cf. A098331, A098332, A098333."
			],
			"keyword": "easy,sign",
			"offset": "0,3",
			"author": "_Paul Barry_, Sep 03 2004",
			"references": 6,
			"revision": 31,
			"time": "2020-01-30T21:29:15-05:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}