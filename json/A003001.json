{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003001",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3001,
			"id": "M4687",
			"data": "0,10,25,39,77,679,6788,68889,2677889,26888999,3778888999,277777788888899",
			"name": "Smallest number of multiplicative persistence n.",
			"comment": [
				"Probably finite.",
				"The persistence of a number (A031346) is the number of times you need to multiply the digits together before reaching a single digit.",
				"From _David A. Corneth_, Sep 23 2016: (Start)",
				"For n \u003e 1, the digit 0 doesn't occur. Therefore the digit 1 doesn't occur and all terms have digits in nondecreasing order.",
				"a(n) consists of at most one three and at most one two but not both. If they contain both, they could be replaced with a single digit 6 giving a lesser number. Two threes can be replaced with a 9. Similarily, there's at most one four and one six but not both. Two sixes can be replaced with 49. A four and a six can be replaced with a three and an eight. For n \u003e 2, an even number and a five don't occur together.",
				"Summarizing, a term a(n) for n \u003e 2 consists of 7's, 8's and 9's with a prefix of one of the following sets of digits: {{}, {2}, {3}, {4}, {6}, {2,6}, {3,5}, {5, 5,...}} [Amended by _Kohei Sakai_, May 27 2017]",
				"No more up to 10^200. (End)",
				"From _Benjamin Chaffin_, Sep 29 2016: (Start)",
				"Let p(n) be the product of the digits of n, and P(n) be the multiplicative persistence of n. Any p(n) \u003e 1 must have only prime factors from one of the two sets {2,3,7} or {3,5,7}. The following are true of all p(n) \u003c 10^20000:",
				"The largest p(n) with P(p(n))=10 is 2^4 * 3^20 * 7^5. The only other such p(n) known is p(a(11))=2^19 * 3^4 * 7^6.",
				"The largest p(n) with P(p(n))=9 is 2^33 * 3^3 (12 digits).",
				"The largest p(n) with P(p(n))=8 is 2^9 * 3^5 * 7^8 (12 digits).",
				"The largest p(n) with P(p(n))=7 is 2^24 * 3^18 (16 digits).",
				"The largest p(n) with P(p(n))=6 is 2^24 * 3^6 * 7^6 (16 digits).",
				"The largest p(n) with P(p(n))=5 is 2^35 * 3^2 * 7^6 (17 digits).",
				"The largest p(n) with P(p(n))=4 is 2^59 * 3^5 * 7^2 (22 digits).",
				"The largest p(n) with P(p(n))=3 is 2^4 * 3^17 * 7^38 (42 digits).",
				"The largest p(n) with P(p(n))=2 is 2^25 * 3^227 * 7^28 (140 digits).",
				"All p(n) between 10^140 and 10^20000 have a persistence of 1, meaning they contain a 0 digit. (End)",
				"_Benjamin Chaffin_'s comments imply that there are no more terms up to 10^20585. For every number N between 10^200 with 10^20585 with persistence greater than 1, the product of the digits of N is between 10^140 and 10^20000, and each of these products has a persistence of 1.  -  _David Radcliffe_, Mar 22 2019",
				"From _A.H.M. Smeets_, Nov 16 2018: (Start)",
				"Let p_10(n) be the product of the digits of n in base 10. We can define an equivalence relation DP_10 on n by n DP_10 m if and only if p_10(n) = p_10(m); the name DP_b for the equivalence relation stands for \"digits product for representation in base b\". A number n is called the class representative number of class n/DP_10 if and only if p_10(n) = p_10(m), m \u003e= n; i.e., if it is the smallest number of that class; it is also called the reduced number.",
				"For any multiplicative persistence, except multiplicative persistence 2, the set of class representative numbers with that multiplicative persistence is conjectured to be finite.",
				"Each class representative number represents an infinite set of numbers with the same multiplicative persistence.",
				"For multiplicative persistence 2, only the set of class representative numbers which end in the digit zero is infinite. The table of numbers of class representative numbers of different multiplicative persistence (mp) is given by:",
				"               final digit",
				"mp  total      0   1   2   3   4   5   6   7   8   9",
				"====================================================",
				"0      10      1   1   1   1   1   1   1   1   1   1",
				"1      10      1   1   1   1   1   1   1   1   1   1",
				"2     inf    inf   0   4   0   1   1   5   0   7   0",
				"3   12199  12161   0   8   0   3   3   8   0  16   0",
				"4     408    342   0  14   0   5   4  19   0  24   0",
				"5     151     88   0   9   0   1   3  37   0  13   0",
				"6      41     24   0   1   0   0   0  14   0   2   0",
				"7      13      9   0   0   0   0   0   4   0   0   0",
				"8       8      7   0   0   0   0   0   1   0   0   0",
				"9       5      5   0   0   0   0   0   0   0   0   0",
				"10      2      2   0   0   0   0   0   0   0   0   0",
				"11      2      2   0   0   0   0   0   0   0   0   0",
				"It is observed from this that for the reduced numbers with multiplicative persistence 1, the primes 11, 13, 17 and 19, will not occur in any trajectory of another (larger) number; i.e., all numbers represented by the reduced numbers 11, 13, 17 and 19 have a prime factor of at least 11 (conjectured from the observations).",
				"Example for numbers represented by the reduced number 19: 91 = 7*13, 133 = 7*19, 313 is prime, 331 is prime, 119 = 7*17, 191 is prime, 911 is prime, 1133 = 11*103, 1313 = 13*101, 1331 = 11^3, 3113 = 11*283, 3131 = 31*101 and 3311 = 7*11*43.",
				"In fact all trajectories can be projected to a trajectory in one of the ten trees with reduced numbers with roots 0..9, and the numbers represented by the reduced number of each leaf have a prime factor of at least 11 (as conjectured from the observations).",
				"Example of the trajectory of 277777788888899 (see A121111) in the tree of reduced numbers (the unreduced numbers are given between brackets): 277777788888899 -\u003e 3778888999 (4996238671872) -\u003e 26888999 (438939648) -\u003e 2677889 (4478976) -\u003e 68889 (338688) -\u003e 6788 (27648) -\u003e 2688 (2688) -\u003e 678 (768) -\u003e 69 (336) -\u003e 45 (54) -\u003e 10 (20) -\u003e 0. (End)"
			],
			"reference": [
				"Alex Bellos, Here's Looking at Euclid: A Surprising Excursion Through the Astonishing World of Math, Free Press, 2010, page 176.",
				"M. Gardner, Fractal Music, Hypercards and More, Freeman, NY, 1991, pp. 170, 186.",
				"C. A. Pickover, Wonders of Numbers, \"Persistence\", Chapter 28, Oxford University Press NY 2001.",
				"Clifford A. Pickover, A Passion for Mathematics, Wiley, 2005; see p. 66.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"de Faria, Edson, and Charles Tresser, \u003ca href=\"http://arxiv.org/abs/1307.1188\"\u003eOn Sloane's persistence problem\u003c/a\u003e, arXiv preprint arXiv:1307.1188 [math.DS], 2013.",
				"de Faria, Edson, and Charles Tresser, \u003ca href=\"http://dx.doi.org/10.1080/10586458.2014.910849\"\u003eOn Sloane's persistence problem\u003c/a\u003e, Experimental Math., 23 (No. 4, 2014), 363-382.",
				"Mark R. Diamond, \u003ca href=\"http://web.archive.org/web/20160313225019/http://markdiamond.com.au/download/joous-3-1-1.pdf\"\u003eMultiplicative persistence base 10: some new null results\u003c/a\u003e, 2011.",
				"Brady Haran and Matt Parker, \u003ca href=\"https://www.youtube.com/watch?v=Wim9WJeDTHQ\"\u003eWhat's special about 277777788888899?\u003c/a\u003e, Numberphile video, 2019.",
				"T. Lamont-Smith, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL24/Lamont/lamont5.html\"\u003eMultiplicative Persistence and Absolute Multiplicative Persistence\u003c/a\u003e, J. Int. Seq., Vol. 24 (2021), Article 21.6.7.",
				"Kevin McElwee, \u003ca href=\"https://medium.com/@kevinrmcelwee/multiplicative-persistence-is-solved-1937692b26cc\"\u003eAn algorithm for multiplicative persistence research\u003c/a\u003e, Jul 13 2019.",
				"S. Perez and R. Styer, \u003ca href=\"http://www41.homepage.villanova.edu/robert.styer/MultiplicativePersistence/PersistenceStephPerezJournalArtAug2013.pdf\"\u003ePersistence: A Digit Problem\u003c/a\u003e",
				"W. Schneider, \u003ca href=\"http://web.archive.org/web/2004/www.wschnei.de/digit-related-numbers/persistence.html\"\u003eThe Persistence of a Number\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"http://neilsloane.com/doc/persistence.html\"\u003eThe persistence of a number\u003c/a\u003e, J. Recreational Math., 6 (1973), 97-98.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/MultiplicativePersistence.html\"\u003eMultiplicative Persistence\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Persistence_of_a_number\"\u003ePersistence of a number\u003c/a\u003e",
				"Susan Worst, \u003ca href=\"/A003001/a003001.pdf\"\u003eMultiplicative persistence of base four numbers\u003c/a\u003e [Scanned copy of manuscript and correspondence, May 1980]"
			],
			"example": [
				"77 -\u003e 49 -\u003e 36 -\u003e 18 -\u003e 8 has persistence 4."
			],
			"mathematica": [
				"lst = {}; n = 0; Do[While[True, k = n; c = 0; While[k \u003e 9, k = Times @@ IntegerDigits[k]; c++]; If[c == l, Break[]]; n++]; AppendTo[lst, n], {l, 0, 7}]; lst (* _Arkadiusz Wesolowski_, May 01 2012 *)"
			],
			"program": [
				"(PARI) vecprod(w)=prod(i=1,#w,w[i]);",
				"persistence(x)={my(y=digits(x),c=0);while(#y\u003e1,y=digits(vecprod(y));c++);return(c)}",
				"firstTermsA003001(U)={my(ans=vector(U),k=(U\u003e1),z);while(k+1\u003c=U,if(persistence(z)==k,ans[k++]=z);z++);return(ans)}",
				"\\\\ Finds the first U terms (is slow); _R. J. Cano_, Sep 11 2016"
			],
			"xref": [
				"Cf. A031346 (persistence), A133500 (powertrain), A133048 (powerback), A006050, A007954, A031286, A031347, A033908, A046511, A121105-A121111."
			],
			"keyword": "nonn,nice,base,more,hard",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 64,
			"revision": 153,
			"time": "2021-08-30T21:33:48-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}