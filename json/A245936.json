{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A245936",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 245936,
			"data": "1,2,1,1,2,2,1,2,2,1,2,1,1,2,2,1,2,2,1,1,2,1,2,2,1,2,1,1,2,1,1,2,2,1,2,2,1,2,1,1,2,2,1,2,2,1,1,2,1,1,2,1,2,2,1,2,1,1,2,2,1,2,2,1,2,1,1,2,1,1,2,2,1,2,1,1,2,1,2,2,1,1,2,1,1,2",
			"name": "Limit-reverse of the Kolakoski sequence (A000002), with first term as initial block.",
			"comment": [
				"Suppose S = (s(0),s(1),s(2),...) is an infinite sequence such that every finite block of consecutive terms occurs infinitely many times in S.  (It is assumed that A006337 is such a sequence.)  Let B = B(m,k) = (s(m-k),s(m-k+1),...,s(m)) be such a block, where m \u003e= 0 and k \u003e= 0.  Let m(1) be the least i \u003e m such that (s(i-k),s(i-k+1),...,s(i)) = B(m,k), and put B(m(1),k+1) = (s(m(1)-k-1),s(m(1)-k),...,s(m(1))).  Let m(2) be the least i \u003e m(1) such that (s(i-k-1),s(i-k),...,s(i)) = B(m(1),k+1), and put B(m(2),k+2) = (s(m(2)-k-2),s(m(2)-k-1),...,s(m(2))).  Continuing in this manner gives a sequence of blocks B(m(n),k+n).  Let B'(n) = reverse(B(m(n),k+n)), so that for n \u003e= 1, B'(n) comes from B'(n-1) by suffixing a single term; thus the limit of B'(n) is defined; we call it the \"limit-reverse of S with initial block B(m,k)\", denoted by S*(m,k), or simply S*.  (Since A000002 has offset 1, the above definition is adapted accordingly, so that s(n) = A000002(n+1) for n \u003e= 0.)",
				"...",
				"The sequence (m(i)), where m(0) = 1, is the \"index sequence for limit-reversing S with initial block B(m,k)\" or simply the index sequence for S*, as in A245937."
			],
			"example": [
				"S = A000002 (re-indexed to start with s(0) = 1, with B = (s(0)); that is, (m,k) = (0,0); S = (1,2,2,1,1,2,1,2,2,1,2,2,1,1,2,1,1,2,2,1,2,1,1,...)",
				"B'(0) = (1)",
				"B'(1) = (1,2)",
				"B'(2) = (1,2,1)",
				"B'(3) = (1,2,1,1)",
				"B'(4) = (1,2,1,1,2)",
				"B'(5) = (1,2,1,1,2,2)",
				"S* = (1, 2, 1, 1, 2, 2, 1, 2, 2, 1, 2, 1,...),",
				"with index sequence (1, 4, 7, 16, 25, 31, 43, 61, 70, 88, 97, 115,...)"
			],
			"mathematica": [
				"z = 110; seqPosition2[list_, seqtofind_] := Last[Last[Position[Partition[list, Length[#], 1], Flatten[{___, #, ___}], 1, 2]]] \u0026[seqtofind];",
				"n = 32; s = Prepend[Nest[Flatten[Partition[#, 2] /. {{2, 2} -\u003e {2, 2, 1, 1}, {2, 1} -\u003e {2, 2, 1}, {1, 2} -\u003e {2, 1, 1}, {1, 1} -\u003e {2, 1}}] \u0026, {2, 2}, n], 1]; ans = Join[{s[[p[0] = pos = seqPosition2[s, #] - 1]]}, #] \u0026[{s[[1]]}]; cfs = Table[s = Drop[s, pos - 1]; ans = Join[{s[[p[n] = pos = seqPosition2[s, #] - 1]]}, #] \u0026[ans], {n, z}]; rcf = Last[Map[Reverse, cfs]]  (* A245936 *)"
			],
			"xref": [
				"Cf. A000002, A245937, A245920."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Clark Kimberling_ and _Peter J. C. Moses_, Aug 07 2014",
			"references": 2,
			"revision": 13,
			"time": "2017-11-22T01:15:03-05:00",
			"created": "2014-08-22T10:12:01-04:00"
		}
	]
}