{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A076467",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 76467,
			"data": "1,8,16,27,32,64,81,125,128,216,243,256,343,512,625,729,1000,1024,1296,1331,1728,2048,2187,2197,2401,2744,3125,3375,4096,4913,5832,6561,6859,7776,8000,8192,9261,10000,10648,12167,13824,14641,15625,16384,16807",
			"name": "Perfect powers m^k where m is a positive integer and k \u003e= 3.",
			"comment": [
				"If p|n with p prime then p^3|n."
			],
			"link": [
				"Reinhard Zumkeller and Alois P. Heinz, \u003ca href=\"/A076467/b076467.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (terms n = 1..250 from Reinhard Zumkeller)"
			],
			"formula": [
				"For n \u003e 1: GCD(exponents in prime factorization of a(n)) \u003e 2, cf. A124010. - _Reinhard Zumkeller_, Apr 13 2012"
			],
			"maple": [
				"N:= 10^5: # to get all terms \u003c= N",
				"S:= {1, seq(seq(m^k, m = 2 .. floor(N^(1/k))),k=3..ilog2(N))}:",
				"sort(convert(S,list)); # _Robert Israel_, Sep 30 2015"
			],
			"mathematica": [
				"a = {1}; Do[ If[ Apply[ GCD, Last[ Transpose[ FactorInteger[n]]]] \u003e 2, a = Append[a, n]; Print[n]], {n, 2, 17575}]; a",
				"(* Second program: *)",
				"n = 10^5; Join[{1}, Table[m^k, {k, 3, Floor[Log[2, n]]}, {m, 2, Floor[n^(1/k)]}] // Flatten // Union] (* _Jean-François Alcover_, Feb 13 2018, after _Robert Israel_ *)"
			],
			"program": [
				"(Haskell)",
				"a076467 n = a076467_list !! (n-1)",
				"a076467_list = 1 : filter ((\u003e 2) . foldl1 gcd . a124010_row) [2..]",
				"-- _Reinhard Zumkeller_, Apr 13 2012",
				"(Haskell)",
				"import qualified Data.Set as Set (null)",
				"import Data.Set (empty, insert, deleteFindMin)",
				"a076467 n = a076467_list !! (n-1)",
				"a076467_list = 1 : f [2..] empty where",
				"   f xs'@(x:xs) s | Set.null s || m \u003e x ^ 3 = f xs $ insert (x ^ 3, x) s",
				"                  | m == x ^ 3  = f xs s",
				"                  | otherwise = m : f xs' (insert (m * b, b) s')",
				"                  where ((m, b), s') = deleteFindMin s",
				"-- _Reinhard Zumkeller_, Jun 18 2013",
				"(PARI) is(n)=ispower(n)\u003e2||n==1 \\\\ _Charles R Greathouse IV_, Sep 03 2015, edited for n=1 by _M. F. Hasler_, May 26 2018",
				"From _Anatoly E. Voevudko_, Sep 29 2015, edited by _M. F. Hasler_, May 25 2018: (Start)",
				"(PARI) A076467(lim)={my(L=List(1),lim2=logint(lim,2),m,k);for(k=3,lim2, for(m=2,sqrtnint(lim,k),listput(L, m^k);));listsort(L,1);L}",
				"b076467(lim)={my(L=A076467(lim)); for(i=1,#L,print(i ,\" \",L[i]));} \\\\ (End)",
				"(PARI) A076467_vec(LIM,S=List(1))={for(x=2,sqrtnint(LIM,3),for(k=3, logint(LIM, x), listput(S, x^k))); Set(S)} \\\\ _M. F. Hasler_, May 25 2018"
			],
			"xref": [
				"Subsequence of A036966.",
				"Cf. A001597, A076468, A076469, A076470."
			],
			"keyword": "nonn,changed",
			"offset": "1,2",
			"author": "_Robert G. Wilson v_, Oct 14 2002",
			"ext": [
				"Edited by _Robert Israel_, Sep 30 2015"
			],
			"references": 26,
			"revision": 38,
			"time": "2022-01-12T16:21:49-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}