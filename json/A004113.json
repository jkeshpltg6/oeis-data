{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A004113",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 4113,
			"id": "M1629",
			"data": "1,2,6,18,60,204,734,2694,10162,38982,151920,599244,2389028,9608668,38945230,158904230,652178206,2690598570,11151718166,46412717826,193891596436,812748036380,3417407089470,14410094628558,60920843101858,258169745573158,1096494947168142",
			"name": "Number of rooted trees with n nodes and 2-colored non-leaf nodes.",
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A004113/b004113.txt\"\u003eTable of n, a(n) for n = 1..500\u003c/a\u003e",
				"F. Harary, R. W. Robinson and A. J. Schwenk, \u003ca href=\"https://doi.org/10.1017/S1446788700016190\"\u003eTwenty-step algorithm for determining the asymptotic number of trees of various species\u003c/a\u003e, J. Austral. Math. Soc., Series A, 20 (1975), 483-503. \u003ca href=\"https://doi.org/10.1017/S1446788700033760\"\u003eErrata\u003c/a\u003e: Vol. A 41 (1986), p. 325.",
				"N. J. A. Sloane, \u003ca href=\"/transforms.txt\"\u003eTransforms\u003c/a\u003e",
				"\u003ca href=\"/index/Ro#rooted\"\u003eIndex entries for sequences related to rooted trees\u003c/a\u003e",
				"\u003ca href=\"/index/Tra#trees\"\u003eIndex entries for sequences related to trees\u003c/a\u003e"
			],
			"formula": [
				"Shifts left and halves under EULER transform.",
				"a(n) ~ c * d^n / n^(3/2), where d = 4.49415643203339504537343052838796824... and c = 0.368722987377516657464802259... - _Vaclav Kotesovec_, Feb 28 2014"
			],
			"maple": [
				"with(numtheory): etr:= proc(p) local b; b:= proc(n) option remember; `if`(n=0, 1, (add(add(d*p(d), d=divisors(j)) *b(n-j), j=1..n))/n) end end: b:= etr(a): a:= n-\u003e `if`(n\u003c=1, n, 2*b(n-1)): seq(a(n), n=1..30); # _Alois P. Heinz_, Sep 06 2008"
			],
			"mathematica": [
				"etr[p_] := Module[{b}, b[n_] := b[n] = If[n == 0, 1, Sum[Sum[d*p[d], {d, Divisors[j]}]*b[n - j], {j, 1, n}]/n ]; b]; b = etr[a]; a[n_] := If[n \u003c= 1, n, 2*b[n - 1]]; Table[a[n], {n, 1, 27}] (* _Jean-François Alcover_, Jan 29 2013, translated from _Alois P. Heinz_'s Maple program *)"
			],
			"xref": [
				"Cf. A004114, A052316, A052317."
			],
			"keyword": "nonn,eigen",
			"offset": "1,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Extended with better description from _Christian G. Bower_, Apr 15 1998"
			],
			"references": 5,
			"revision": 38,
			"time": "2021-12-26T20:50:55-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}