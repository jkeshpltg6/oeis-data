{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A079484",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 79484,
			"data": "1,3,45,1575,99225,9823275,1404728325,273922023375,69850115960625,22561587455281875,9002073394657468125,4348001449619557104375,2500100833531245335015625,1687568062633590601135546875,1321365793042101440689133203125",
			"name": "a(n) = (2n-1)!! * (2n+1)!!, where the double factorial is A006882.",
			"comment": [
				"a(n) is the determinant of M(2n-1) where M(k) is the k X k matrix with m(i,j)=j if i+j=k m(i,j)=i otherwise.",
				"(-1)^n*a(n)/2^(2n-1) is the permanent of the (m X m) matrix {1/(x_i-y_j), 1\u003c=i\u003c=m, 1\u003c=j\u003c=m}, where x_1,x_2,...,x_m are the zeros of x^m-1 and y_1,y_2,...,y_m the zeros of y^m+1 and m=2n-1.",
				"a(n) is the number of permutations in S_{2n+1} in which all cycles have odd length. - _José H. Nieto S._, Jan 09 2012",
				"In 1881, R. F. Scott posed a conjecture that the absolute value of permanent of square matrix with elements a(i,j)= (x_i - y_j)^(-1), where x_1,...,x_n are roots of x^n=1, while y_1,...,y_n are roots of y^n=-1, equals a((n-1)/2)/2^n, if n\u003e=1 is odd, and 0, if n\u003e=2 is even. After a century (in 1979), the conjecture was proved by H. Minc. - _Vladimir Shevelev_, Dec 01 2013",
				"Number of 3-bundled increasing bilabeled trees with 2n labels. - _Markus Kuba_, Nov 18 2014",
				"a(n) is the number of rooted, binary, leaf-labeled topologies with 2n+2 leaves that have n+1 cherry nodes. - _Noah A Rosenberg_, Feb 12 2019"
			],
			"reference": [
				"M. Bóna, A walk through combinatorics, World Scientific, 2006."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A079484/b079484.txt\"\u003eTable of n, a(n) for n = 0..224\u003c/a\u003e",
				"Cyril Banderier, Markus Kuba, and Michael Wallner, \u003ca href=\"https://arxiv.org/abs/2103.03751\"\u003eAnalytic Combinatorics of Composition schemes and phase transitions with mixed Poisson distributions\u003c/a\u003e, arXiv:2103.03751 [math.PR], 2021.",
				"G.-N. Han and C. Krattenthaler, \u003ca href=\"http://arXiv.org/abs/math.RA/0003072\"\u003eRectangular Scott-type permanents\u003c/a\u003e, arXiv:math/0003072 [math.RA], 2000.",
				"Markus Kuba and Alois Panholzer, \u003ca href=\"http://arxiv.org/abs/1411.4587\"\u003eCombinatorial families of multilabelled increasing trees and hook-length formulas\u003c/a\u003e, arXiv:1411.4587 [math.CO], 17 Nov 2014.",
				"MathOverflow, \u003ca href=\"https://mathoverflow.net/questions/112062/geometric-physical-probabilistic-interpretations-of-riemann-zetan1/401540#401540\"\u003eGeometric / physical / probabilistic interpretations of Riemann zeta(n\u003e1)?\u003c/a\u003e, answer by Tom Copeland posted in Aug 2021.",
				"H. Minc, \u003ca href=\"http://dx.doi.org/10.1016/0024-3795(79)90128-9\"\u003eOn a conjecture of R. F. Scott (1881)\u003c/a\u003e, Linear Algebra Appl., 28(1979), 141-153.",
				"Theodoros Theodoulidis, \u003ca href=\"http://dx.doi.org/10.3311/PPee.7894\"\u003eOn the Closed-Form Expression of Carson’s Integral\u003c/a\u003e, Period. Polytech. Elec. Eng. Comp. Sci., Vol. 59, No. 1 (2015), pp. 26-29.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/StruveFunction.html\"\u003eStruve function\u003c/a\u003e"
			],
			"formula": [
				"D-finite with recurrence a(n) = (4*n^2 - 1) * a(n-1) for all n in Z.",
				"a(n) = A001147(n)*A001147(n+1).",
				"E.g.f.: 1/(1-x^2)^(3/2) (with interpolated zeros). - _Paul Barry_, May 26 2003",
				"a(n) = (2n+1)! * C(2n, n) / 2^(2n). - _Ralf Stephan_, Mar 22 2004.",
				"Alternatingly signed values have e.g.f. sqrt(1+x^2).",
				"a(n) is the value of the n-th moment of : 1/Pi BesselK(1, sqrt(x)) on the positive part of the real line. - _Olivier Gérard_, May 20 2009",
				"a(n) = -2^(2*n-1)*exp(i*n*Pi)*gamma(1/2+n)/gamma(3/2-n). - _Gerry Martens_, Mar 07 2011",
				"E.g.f. (odd powers) tan(arcsin(x)) = Sum_{n\u003e=0} (2n-1)!!*(2n+1)!!*x^(2*n+1)/(2*n+1)!. - _Vladimir Kruchinin_, Apr 22 2011",
				"G.f.: 1 + x*(G(0) - 1)/(x-1) where G(k) = 1 - ((2*k+2)^2-1)/(1-x/(x - 1/G(k+1))); ( continued fraction ). - _Sergei N. Gladkovskii_, Jan 15 2013",
				"a(n) = (2^(2*n+3)*Gamma(n+3/2)*Gamma(n+5/2))/Pi. - _Jean-François Alcover_, Jul 20 2015",
				"Lim_{n-\u003einfinity} 4^n*(n!)^2/a(n) = Pi/2. - _Daniel Suteu_, Feb 05 2017",
				"From _Michael Somos_, May 04 2017: (Start)",
				"a(n) = (2*n + 1) * A001818(n).",
				"E.g.f.: Sum_{n\u003e=0} a(n) * x^(2*n+1) / (2*n+1)! = x / sqrt(1 - x^2) = tan(arcsin(x)).",
				"Given e.g.f. A(x) = y, then x * y' = y + y^3.",
				"a(n) = -1 / a(-1-n) for all n in Z.",
				"0 = +a(n)*(+288*a(n+2) -60*a(n+3) +a(n+4)) +a(n+1)*(-36*a(n+2) -4*a(n+3)) +a(n+2)*(+3*a(n+2)) for all n in Z. (End)",
				"a(n) = Sum_{k=0..2n} (k+1) * A316728(n,k). - _Alois P. Heinz_, Jul 12 2018"
			],
			"example": [
				"G.f. = 1 + 3*x + 45*x^2 + 1575*x^3 + 99225*x^4 + 9823275*x^5 + ...",
				"M(5) =",
				"[1, 2, 3, 1, 5]",
				"[1, 2, 2, 4, 5]",
				"[1, 3, 3, 4, 5]",
				"[4, 2, 3, 4, 5]",
				"[1, 2, 3, 4, 5].",
				"int( x^3 besselK(1, sqrt(x)), x=0..infty) = 1575 Pi. - _Olivier Gérard_, May 20 2009"
			],
			"maple": [
				"A079484:= n-\u003e doublefactorial(2*n-1)*doublefactorial(2*n+1):",
				"seq(A079484(n), n=0..15);  # _Alois P. Heinz_, Jan 30 2013"
			],
			"mathematica": [
				"a[n_] := (2n - 1)!!*(2n + 1)!!; Table[a[n], {n, 0, 13}] (* _Jean-François Alcover_, Jan 30 2013 *)"
			],
			"program": [
				"(PARI) /* Formula using the zeta function and a log integral:*/",
				"L(n)=  intnum(t=0,1,log(1-1/t)^n);",
				"Zetai(n)= -I*I^n*(2*Pi)^(n-1)/(n-1)*L(1-n);",
				"a(n)=-I*2^(2*n-1)*Zetai(1/2-n)*L(-1/2+n)/(Zetai(-1/2+n)*L(1/2-n));",
				"/* _Gerry Martens_, Mar 07 2011 */",
				"(MAGMA)  I:=[1,3]; [n le 2 select I[n] else (4*n^2-8*n+3)*Self(n-1): n in [1..20]]; // _Vincenzo Librandi_, Nov 18 2014",
				"(PARI) {a(n) = if( n\u003c0, -1 / self()(-1-n), (2*n + 1)! * (2*n)! / (n! * 2^n)^2 )}; /* _Michael Somos_, May 04 2017 */",
				"(PARI) {a(n) = if( n\u003c0, -1 / self()(-1-n), my(m = 2*n + 1); m! * polcoeff( x / sqrt( 1 - x^2 + x * O(x^m) ), m))}; /* _Michael Somos_, May 04 2017 */"
			],
			"xref": [
				"Cf. A001818, A000165.",
				"Bisection of A000246, A053195, |A013069|, |A046126|. Cf. A000909.",
				"Cf. A001044, A010791, |A129464|, A114779, are also values of similar moments.",
				"Equals the row sums of A162005.",
				"Cf. A316728.",
				"Diagonal elements of A306364 in even-numbered rows."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Benoit Cloitre_, Jan 17 2003",
			"ext": [
				"Simpler description from Daniel Flath (deflath(AT)yahoo.com), Mar 05 2004"
			],
			"references": 20,
			"revision": 135,
			"time": "2021-11-14T01:30:42-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}