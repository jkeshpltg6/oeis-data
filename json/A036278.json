{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A036278",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 36278,
			"data": "1,3,45,945,4725,93555,638512875,18243225,162820783125,38979295480125,1531329465290625,13447856940643125,201919571963756521875,11094481976030578125,564653660170076273671875,5660878804669082674070015625,31245110285511170603633203125",
			"name": "Denominators in Taylor series for cot x.",
			"reference": [
				"M. Abramowitz and I. A. Stegun, eds., Handbook of Mathematical Functions, National Bureau of Standards Applied Math. Series 55, Tenth Printing, 1972, p. 75 (4.3.70).",
				"G. W. Caunt, Infinitesimal Calculus, Oxford Univ. Press, 1914, p. 477.",
				"L. Comtet, Advanced Combinatorics, Reidel, 1974, p. 88.",
				"H. Rademacher, Topics in Analytic Number Theory, Springer, 1973, Chap. 1, p. 19."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A036278/b036278.txt\"\u003eTable of n, a(n) for n = -1..250\u003c/a\u003e (terms -1..100 from T. D. Noe)",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards Applied Math. Series 55, Tenth Printing, 1972, p. 75 (4.3.70).",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Cotangent.html\"\u003eCotangent\u003c/a\u003e"
			],
			"formula": [
				"cot(x) = Sum_{k\u003e=0} (-1)^k B_{2k} 4^k x^(2k-1) / (2k)!.",
				"a(n) = denominator(A000182(n)/(4^n-1)), n\u003e0.",
				"a(n) = denominator for coth x;",
				"coth(x) = W(0)/x -1, W(k) = k+1+2*x-2*x*(k+1)/W(k+1); (continued fraction). - _Sergei N. Gladkovskii_, Nov 27 2011",
				"coth(x) = Q(0)/(1+x)/x^2 - 1 where Q(k) = 2*k^3 + (2*x+3)*k^2 + (2*x^2+3*x+1)*k + 2*x^3 + 2*x^2 + x - 2*x^2*(k+1)*(2*k+1)*(x+k)*(x+k+2)/Q(k+1); (continued fraction). - _Sergei N. Gladkovskii_, Feb 28 2013",
				"a(n) = denominator of 4^(n+1)*Zeta(-2*n-1)/(2*n+1)! for n \u003e= 0. - _Peter Luschny_, Jun 20 2013",
				"a(n) = denominator(r(n)), with the negative rational numbers r(n) = [x^n]( (cot(sqrt(x))-1/sqrt(x))/sqrt(x)) for n \u003e= 0. - _Wolfdieter Lang_, Oct 07 2016"
			],
			"example": [
				"G.f. = 1/x - (1/3)*x - (1/45)*x^3 - (2/945)*x^5 - (1/4725)*x^7 - (2/93555)*x^9 + O(x^11)."
			],
			"maple": [
				"A036278 := n -\u003e `if`(n\u003c0, 1, denom(4^(n+1)*Zeta(-2*n-1)/(2*n+1)!));",
				"seq(A036278(n), n = -1..20);  # _Peter Luschny_, Jun 20 2013"
			],
			"mathematica": [
				"Denominator[Select[List@@Series[Cot[x],{x,0,20}][[3]],#!=0\u0026]]  (* _Harvey P. Dale_, Apr 11 2011 *)",
				"a[n_] := (-1)^(n+1)*4^(n+1)*BernoulliB[2*n+2]/(2*n+2)! // Denominator; Table[a[n], {n, -1, 20}] (* _Jean-François Alcover_, Apr 14 2014, after _Peter Luschny_ *)"
			],
			"program": [
				"(PARI) apply(r-\u003edenominator(r),Vec(1/tan(x))) \\\\ _Charles R Greathouse IV_, Apr 14 2014",
				"(PARI) a(n) = denominator((-1)^(n+1)*4^(n+1)*bernfrac(2*n+2)/(2*n+2)!); \\\\  _Altug Alkan_, Dec 02 2015",
				"(Python)",
				"from sympy import bernoulli, factorial",
				"def a(n):",
				"    return ((-4)**(n + 1)*bernoulli(2*n + 2)/factorial(2*n + 2)).denominator()",
				"print([a(n) for n in range(-1, 20)]) # _Indranil Ghosh_, Jun 23 2017",
				"(MAGMA) [Denominator((-1)^(n+1)*4^(n+1)*Bernoulli(2*n+2)/Factorial(2*n+2) ): n in [-1..20]]; // _G. C. Greubel_, Jul 03 2019",
				"(Sage) [denominator( (-1)^(n+1)*4^(n+1)*bernoulli(2*n+2)/factorial(2*n+2) ) for n in (-1..20)] # _G. C. Greubel_, Jul 03 2019",
				"(GAP) List([-1..20], n-\u003e DenominatorRat( (-1)^(n+1)*4^(n+1)* Bernoulli(2*n+2)/Factorial(2*n+2) )) # _G. C. Greubel_, Jul 03 2019"
			],
			"xref": [
				"Cf. A000182, A002431 (numerators)."
			],
			"keyword": "nonn,frac,easy,nice",
			"offset": "-1,2",
			"author": "_N. J. A. Sloane_",
			"references": 5,
			"revision": 71,
			"time": "2020-05-04T11:55:13-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}