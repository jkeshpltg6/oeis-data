{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A097026",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 97026,
			"data": "1,1,1,1,2,2,2,1,2,2,1,2,1,1,1,1,1,1,2,1,2,2,4,1,1,1,2,1,4,4,2,1,4,4,2,4,2,2,6,4,2,4,6,4,2,2,6,4,6,2,1,2,6,2,6,2,1,1,6,2,6,6,6,1,2,6,6,6,6,6,6,2,6,6,6,6,6,6,6,2,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,2,6,6,6,6,6,6,6,6,6",
			"name": "Function f(x) = phi(x) + floor(x/2) is iterated, starting at x=n; a(n) is the length of terminal cycle (or 0 if no finite cycle exists).",
			"comment": [
				"While iteration of phi(x) always leads to a fixed point, f(x) = phi(x) + incr(x) may result in cycles or be divergent. What is the magnitude of the added incrementing function?",
				"Some initial values are hard to analyze. The first is n=163, so perhaps a(163)=0 by definition.",
				"Observation regarding the above comment: most n \u003c= 1000 have 1 \u003c= a(n) \u003c= 12; the following have unresolved cycles at 10^3 iterations of f(x): {163, 182, 196, 243, 283, 331, 423, 487, 495, 503, 511, 523, 533, 551, 559, 571, 583, 591, 593, ...}. - _Michael De Vlieger_, May 16 2017"
			],
			"link": [
				"Michael De Vlieger, \u003ca href=\"/A097026/b097026.txt\"\u003eTable of n, a(n) for n = 1..162\u003c/a\u003e",
				"Michael De Vlieger, \u003ca href=\"/A097026/a097026.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e with 0 representing terms that have unresolved cycles at 10^3 iterations of f(x)."
			],
			"formula": [
				"For n=2^j: a(2^j)=1, powers of 2 are fixed points."
			],
			"example": [
				"n=70: iteration list = {70, 59, 87, 99, 109, 162, 135, 139, 207, 235, 301, 402, 333, 382, 381, 442, [413, 554, 553, 744, 612, 498], 413}, a(70)=6;"
			],
			"mathematica": [
				"With[{nn = 10^3}, Table[Count[Values@ PositionIndex@ NestList[EulerPhi@ # + Floor[#/2] \u0026, n, nn], s_ /; Length@ s \u003e 1], {n, 105}]] (* _Michael De Vlieger_, May 16 2017 *)"
			],
			"program": [
				"(PARI) findpos(newn, v) = {forstep(k=#v, 1, -1, if (v[k] == newn, return(k)););}",
				"a(n) = {ok = 0; v = [n]; while(!ok, newn = eulerphi(n) + n\\2; ipos = findpos(newn, v); if (ipos, ok = 1; break); v = concat(v, newn); n = newn;); #v - ipos + 1;} \\\\ _Michel Marcus_, Jan 03 2017"
			],
			"xref": [
				"Cf. A000010, A097027, A097028, A097029."
			],
			"keyword": "nonn",
			"offset": "1,5",
			"author": "_Labos Elemer_, Aug 27 2004",
			"references": 6,
			"revision": 28,
			"time": "2017-05-20T21:44:26-04:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}