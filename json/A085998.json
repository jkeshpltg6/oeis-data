{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A085998",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 85998,
			"data": "0,0,0,0,5,0,8,3,0,4,7,2,1,5,0,1,9,7,8,8,9,2,3,5,2,5,9,1,5,0,9,2,3,4,1,1,1,8,9,6,2,2,3,8,0,6,8,9,8,8,1,6,3,9,3,9,9,7,9,5,2,1,6,0,2,5,6,1,3,0,2,8,9,2,1,4,9,7,3,7,8,7,3,7,8,4,6,1,2,7,6,5,4,7,9,2,4,2,9,1,1,2,4,8,1",
			"name": "Decimal expansion of the prime zeta modulo function at 9 for primes of the form 4k+3.",
			"link": [
				"Jean-François Alcover, \u003ca href=\"/A085998/b085998.txt\"\u003eTable of n, a(n) for n = 0..1006\u003c/a\u003e",
				"P. Flajolet and I. Vardi, \u003ca href=\"http://algo.inria.fr/flajolet/Publications/landau.ps\"\u003eZeta Function Expansions of Classical Constants\u003c/a\u003e, Unpublished manuscript. 1996.",
				"X. Gourdon and P. Sebah, \u003ca href=\"http://numbers.computation.free.fr/Constants/Miscellaneous/constantsNumTheory.html\"\u003eSome Constants from Number theory\u003c/a\u003e.",
				"R. J. Mathar, \u003ca href=\"http://arxiv.org/abs/1008.2547\"\u003eTable of ... Prime Zeta Modulo functions\u003c/a\u003e, arXiv:1008.2547 [math.NT], 2010-2015, value P(m=4, n=3, s=9), page 21.",
				"\u003ca href=\"/index/Z#zeta_function\"\u003eOEIS index to entries related to the (prime) zeta function\u003c/a\u003e."
			],
			"formula": [
				"Zeta_R(9) = Sum_{primes p == 3 (mod 4)} 1/p^9",
				"  = (1/2)*Sum_{n\u003e=0} mobius(2*n+1) *log(b((2*n+1)*9))/(2*n+1),",
				"  where b(x) = (1-2^(-x))*zeta(x)/L(x) and L(x) is the Dirichlet Beta function."
			],
			"example": [
				"0.000050830472150197889235259150923411189622380689881639399795... ~ 5.08...*10^-5"
			],
			"mathematica": [
				"digits = 1003;",
				"nmax0 = 100;(* initial number of sum terms *)",
				"dnmax = 10;(* nmax increment *)",
				"dd = 10;(* precision excess *)",
				"Clear[PrimeZeta43];",
				"f[s_] := (1 - 2^(-s))*(Zeta[s]/DirichletBeta[s]);",
				"PrimeZeta43[s_, nmax_] := PrimeZeta43[s, nmax] = (1/2) Sum[MoebiusMu[2 n + 1]*Log[f[(2 n + 1)*9]]/(2 n + 1), {n, 0, nmax}] // N[#, digits + dd] \u0026;",
				"PrimeZeta43[9, nmax = nmax0];",
				"PrimeZeta43[9, nmax += dnmax];",
				"While[Abs[PrimeZeta43[9, nmax] - PrimeZeta43[9, nmax - dnmax]] \u003e 10^-(digits + dd), Print[\"nmax = \", nmax]; nmax += dnmax];",
				"PrimeZeta43[9] = PrimeZeta43[9, nmax];",
				"Join[{0, 0, 0, 0}, RealDigits[PrimeZeta43[9], 10, digits][[1]]] (* _Jean-François Alcover_, Jun 22 2011, updated May 07 2021 *)"
			],
			"program": [
				"(PARI) A085998_upto(N=100)={localprec(N+3); digits((PrimeZeta43(9)+1)\\.1^N)[^1]} \\\\ see A085991 for the PrimeZeta43 function. - _M. F. Hasler_, Apr 25 2021"
			],
			"xref": [
				"Cf. A085991 .. A085997 (Zeta_R(2..8)).",
				"Cf. A086039 (analog for primes 4k+1), A085969 (PrimeZeta(9)), A002145 (primes 4k+3)."
			],
			"keyword": "cons,nonn",
			"offset": "0,5",
			"author": "Antonio G. Astudillo (afg_astudillo(AT)lycos.com), Jul 06 2003",
			"ext": [
				"Edited by _M. F. Hasler_, Apr 25 2021"
			],
			"references": 10,
			"revision": 29,
			"time": "2021-05-08T08:37:31-04:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}