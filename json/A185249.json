{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A185249",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 185249,
			"data": "1,0,1,1,0,1,0,2,0,1,1,0,5,0,1,0,3,0,14,0,1,1,0,14,0,42,0,1,0,4,0,84,0,132,0,1,1,0,30,0,594,0,429,0,1,0,5,0,330,0,4719,0,1430,0,1,1,0,55,0,4719,0,40898,0,4862,0,1,0,6,0,1001,0,81796,0,379236,0,16796,0,1,1,0,91,0,26026,0,1643356,0,3711916,0,58786,0,1",
			"name": "Triangle read by rows: Table III.5 of Myriam de Sainte-Catherine's 1983 thesis.",
			"comment": [
				"I have a photocopy of certain pages of the thesis, but unfortunately not enough to find the definition of this table. I have written to the author.",
				"(Added later) However, _Alois P. Heinz_ found a formula involving Catalan numbers which matches all the data and is surely correct, so the triangle is no longer a mystery.",
				"Reading upwards along antidiagonals gives A123352.",
				"From _Petros Hadjicostas_, Sep 04 2019: (Start)",
				"Consider \"Young tableaux with entries from the set {1,...,n}, strictly increasing in rows and not decreasing in columns. Note that usually the reverse convention between rows and columns is used.\"",
				"de Sainte-Catherine and Viennot (1986) proved that \"the number b_{n,k} of such Young tableaux having only columns with an even number of elements and bounded by height p = 2*k\" is given by b_{n,k} = Product_{1 \u003c= i \u003c= j \u003c= n} (2*k + i + j)/(i + j).\" In Section 6 of their paper, they give an interpretation of this formula in terms of Pfaffians and perfect matchings.",
				"It turns out that for the current array, T(n,k) = b_{k, (n-k)/2} if n-k is even, and 0 otherwise (for n \u003e= 0 and 0 \u003c= k \u003c= n). It is unknown, however, what kind of interpretation Myriam de Sainte-Catherine gave to the number T(n,k) three years earlier in her 1983 Ph.D. dissertation. It may be distantly related to the numbers b_{n,k} that are found in her 1986 paper with G. Viennot.",
				"(End)"
			],
			"reference": [
				"Myriam de Sainte-Catherine, Couplages et Pfaffiens en Combinatoire. Physique et Informatique. Ph.D. Dissertation, Université Bordeaux I, 1983."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A185249/b185249.txt\"\u003eRows n = 0..100, flattened\u003c/a\u003e",
				"M. de Sainte-Catherine and G. Viennot, \u003ca href=\"https://doi.org/10.1007/BFb0072509\"\u003eEnumeration of certain Young tableaux with bounded height\u003c/a\u003e, in: G. Labelle and P. Leroux (eds), \u003ca href=\"https://doi.org/10.1007/BFb0072503\"\u003eCombinatoire énumérative\u003c/a\u003e, Lecture Notes in Mathematics, vol. 1234, Springer, Berlin, Heidelberg, 1986, pp. 58-67."
			],
			"formula": [
				"T(n,k) = Product_{1 \u003c= i \u003c= j \u003c= k} (n-k + i + j)/(i + j) if n - k is even, and = 0 otherwise (for n \u003e= 0 and 0 \u003c= k \u003c= n). - _Petros Hadjicostas_, Sep 04 2019"
			],
			"example": [
				"Triangle begins:",
				"  1",
				"  0 1",
				"  1 0  1",
				"  0 2  0    1",
				"  1 0  5    0     1",
				"  0 3  0   14     0     1",
				"  1 0 14    0    42     0       1",
				"  0 4  0   84     0   132       0      1",
				"  1 0 30    0   594     0     429      0       1",
				"  0 5  0  330     0  4719       0   1430       0     1",
				"  1 0 55    0  4719     0   40898      0    4862     0     1",
				"  0 6  0 1001     0 81796       0 379236       0 16796     0 1",
				"  1 0 91    0 26026     0 1643356      0 3711916     0 58786 0 1",
				"  ..."
			],
			"maple": [
				"with(LinearAlgebra):",
				"ctln:= proc(n) option remember; binomial(2*n, n)/(n+1) end:",
				"T := proc(n, k)",
				"       if n=k then 1",
				"     elif irem(n+k, 2)=1 then 0",
				"     else Determinant(Matrix((n-k)/2, (i, j)-\u003e ctln(i+j-1+k)))",
				"       fi",
				"     end:",
				"seq(seq(T(n,k), k=0..n), n=0..12);  # _Alois P. Heinz_, Feb 15 2011"
			],
			"mathematica": [
				"t[n_, n_] = 1; t[n_, k_] /; Mod[n+k, 2] == 1 = 0; t[n_, k_] := Array[CatalanNumber[#1 + #2 - 1 + k]\u0026, {(n-k)/2, (n-k)/2}] // Det; Table[t[n, k], {n, 0, 12}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Jan 14 2014, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Diagonals give A000108, A005700, A006149, A006150, A006151, A000330, A006858, A091962.",
				"Row sums give A186232. Nonzero diagonals give columns in A078920.",
				"Cf. A179898."
			],
			"keyword": "tabl,nonn",
			"offset": "0,8",
			"author": "_N. J. A. Sloane_, Feb 15 2011",
			"ext": [
				"Typo in data corrected by _Alois P. Heinz_, Feb 15 2011"
			],
			"references": 4,
			"revision": 71,
			"time": "2019-10-16T01:30:24-04:00",
			"created": "2011-01-25T22:43:09-05:00"
		}
	]
}