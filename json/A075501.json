{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A075501",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 75501,
			"data": "1,6,1,36,18,1,216,252,36,1,1296,3240,900,60,1,7776,40176,19440,2340,90,1,46656,489888,390096,75600,5040,126,1,279936,5925312,7511616,2204496,226800,9576,168,1,1679616,71383680",
			"name": "Stirling2 triangle with scaled diagonals (powers of 6).",
			"comment": [
				"This is a lower triangular infinite matrix of the Jabotinsky type. See the Knuth reference given in A039692 for exponential convolution arrays.",
				"The row polynomials p(n,x) := Sum_{m=1..n} a(n,m)x^m, n \u003e= 1, have e.g.f. J(x; z)= exp((exp(6*z) - 1)*x/6) - 1."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A075501/b075501.txt\"\u003eTable of n, a(n) for n = 1..1275\u003c/a\u003e"
			],
			"formula": [
				"a(n, m) = (6^(n-m)) * stirling2(n, m).",
				"a(n, m) = (Sum_{p=0..m-1} A075513(m, p)*((p+1)*6)^(n-m))/(m-1)! for n \u003e= m \u003e= 1, else 0.",
				"a(n, m) = 6m*a(n-1, m) + a(n-1, m-1), n \u003e= m \u003e= 1, else 0, with a(n, 0) := 0 and a(1, 1)=1.",
				"G.f. for m-th column: (x^m)/Product_{k=1..m}(1-6k*x), m \u003e= 1.",
				"E.g.f. for m-th column: (((exp(6x)-1)/6)^m)/m!, m \u003e= 1."
			],
			"example": [
				"[1]; [6,1]; [36,18,1]; ...; p(3,x) = x(36 + 18*x + x^2).",
				"From _Andrew Howroyd_, Mar 25 2017: (Start)",
				"Triangle starts",
				"*      1",
				"*      6       1",
				"*     36      18       1",
				"*    216     252      36       1",
				"*   1296    3240     900      60      1",
				"*   7776   40176   19440    2340     90    1",
				"*  46656  489888  390096   75600   5040  126   1",
				"* 279936 5925312 7511616 2204496 226800 9576 168 1",
				"(End)"
			],
			"maple": [
				"# The function BellMatrix is defined in A264428.",
				"# Adds (1,0,0,0, ..) as column 0.",
				"BellMatrix(n -\u003e 6^n, 9); # _Peter Luschny_, Jan 28 2016"
			],
			"mathematica": [
				"Flatten[Table[6^(n - m) StirlingS2[n, m], {n, 11}, {m, n}]] (* _Indranil Ghosh_, Mar 25 2017 *)",
				"BellMatrix[f_Function, len_] := With[{t = Array[f, len, 0]}, Table[BellY[n, k, t], {n, 0, len - 1}, {k, 0, len - 1}]];",
				"rows = 10;",
				"M = BellMatrix[6^#\u0026, rows];",
				"Table[M[[n, k]], {n, 2, rows}, {k, 2, n}] // Flatten (* _Jean-François Alcover_, Jun 23 2018, after _Peter Luschny_ *)"
			],
			"program": [
				"(PARI) for(n=1, 11, for(m=1, n, print1(6^(n - m) * stirling(n, m, 2),\", \");); print();) \\\\ _Indranil Ghosh_, Mar 25 2017"
			],
			"xref": [
				"Columns 1-7 are A000400, A016175, A075916-A075920. Row sums are A005012.",
				"Cf. A075500, A075502."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "1,2",
			"author": "_Wolfdieter Lang_, Oct 02 2002",
			"references": 9,
			"revision": 20,
			"time": "2018-06-23T07:25:56-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}