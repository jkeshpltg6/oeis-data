{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A061704",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 61704,
			"data": "1,1,1,1,1,1,1,2,1,1,1,1,1,1,1,2,1,1,1,1,1,1,1,2,1,1,2,1,1,1,1,2,1,1,1,1,1,1,1,2,1,1,1,1,1,1,1,2,1,1,1,1,1,2,1,2,1,1,1,1,1,1,1,3,1,1,1,1,1,1,1,2,1,1,1,1,1,1,1,2,2,1,1,1,1,1,1,2,1,1,1,1,1,1,1,2,1,1,1,1,1,1,1,2,1",
			"name": "Number of cubes dividing n.",
			"link": [
				"Antti Karttunen, \u003ca href=\"/A061704/b061704.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Vaclav Kotesovec, \u003ca href=\"/A061704/a061704.jpg\"\u003eGraph - the asymptotic ratio (100000 terms)\u003c/a\u003e",
				"\u003ca href=\"/index/Eu#epf\"\u003eIndex entries for sequences computed from exponents in factorization of n\u003c/a\u003e"
			],
			"formula": [
				"Multiplicative with a(p^e) = floor(e/3) + 1. - _Mitch Harris_, Apr 19 2005",
				"G.f.: Sum_{n\u003e=1} x^(n^3)/(1-x^(n^3)). - _Joerg Arndt_, Jan 30 2011",
				"a(n) = A000005(A053150(n)).",
				"Dirichlet g.f.: zeta(3*s)*zeta(s). - _Geoffrey Critzer_, Feb 07 2015",
				"Sum_{k=1..n} a(k) ~ zeta(3)*n + zeta(1/3)*n^(1/3). - _Vaclav Kotesovec_, Dec 01 2020",
				"a(n) = Sum_{k=1..n} (1 - ceiling(n/k^3) + floor(n/k^3)). - _Wesley Ivan Hurt_, Jan 28 2021"
			],
			"example": [
				"a(128) = 4 since 128 is divisible by 1^3 = 1, 2^3 = 8 and 4^3 = 64."
			],
			"maple": [
				"N:= 1000: # to get a(1)..a(N)",
				"G:= add(x^(n^3)/(1-x^(n^3)),n=1..floor(N^(1/3))):",
				"S:= series(G,x,N+1):",
				"seq(coeff(S,x,j),j=1..N); # _Robert Israel_, Jul 28 2017"
			],
			"mathematica": [
				"nn = 100; f[list_, i_]:= list[[i]]; Table[ DirichletConvolve[ f[ Boole[ Map[ IntegerQ[#] \u0026, Map[#^(1/3) \u0026, Range[nn]]]], n],f[Table[1, {nn}], n], n, m], {m, 1, nn}] (* _Geoffrey Critzer_, Feb 07 2015 *)",
				"Table[DivisorSum[n, 1 \u0026, IntegerQ[#^(1/3)] \u0026], {n, 105}] (* _Michael De Vlieger_, Jul 28 2017 *)",
				"f[p_, e_] := 1 + Floor[e/3]; a[1] = 1; a[n_] := Times @@ (f @@@ FactorInteger[n]); Array[a, 100] (* _Amiram Eldar_, Sep 15 2020 *)"
			],
			"program": [
				"(PARI) a(n) = sumdiv(n, d, ispower(d, 3)); \\\\ _Michel Marcus_, Jan 31 2015"
			],
			"xref": [
				"Cf. A000005, A000578, A046951, A053150."
			],
			"keyword": "nonn,mult",
			"offset": "1,8",
			"author": "_Henry Bottomley_, Jun 18 2001",
			"references": 18,
			"revision": 44,
			"time": "2021-02-01T21:52:38-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}