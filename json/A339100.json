{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A339100",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 339100,
			"data": "1,6,1,60,1,42,1,120,1,66,1,5460,1,6,1,4080,1,798,1,3300,1,138,1,10920,1,6,1,1740,1,14322,1,8160,1,6,1,3838380,1,6,1,270600,1,12642,1,1380,1,282,1,371280,1,66,1,3180,1,798,1,3480,1,354,1,567867300",
			"name": "a(n) = GCD({(2*n-k)*T(n,k)+(k+1)*T(n,k+1), k=0..n}), where T(n,k) stands for A214406 (the second-order Eulerian numbers of type B).",
			"comment": [
				"Define recursively the rational fractions R_n by: R_0(x)=1; R_{n+1}(x) = (R_n(x)*x/(1-x^2))'. 2*a(n) is the maximal integer that can be factored out of the numerator of R'_n -- staying with polynomials with integer coefficients.",
				"Empirical observations: the prime factorizations of the a(n) follow a pattern: the 2-adic valuation of a(n) is the 2-adic valuation of n; the 3-adic valuation of a(n) is (n mod 2); for p a prime \u003e= 5, the p-adic valuation of a(n) is 0 (if p-1 does not divide n), 1 (if p-1 divides n but p does not) or 2 (if both p-1 and p divide n). So, a(n) = 1 when n is odd, and the prime factorizations of a(n) for the first few even n are:",
				"  \\ p|",
				"   \\ |  2  3  5  7 11 13 17 19 23 29 31 37 41 43",
				"  n \\|",
				"  ---+-------------------------------------------",
				"   2 |  1  1  .  .  .  .  .  .  .  .  .  .  .  .",
				"   4 |  2  1  1  .  .  .  .  .  .  .  .  .  .  .",
				"   6 |  1  1  .  1  .  .  .  .  .  .  .  .  .  .",
				"   8 |  3  1  1  .  .  .  .  .  .  .  .  .  .  .",
				"  10 |  1  1  .  .  1  .  .  .  .  .  .  .  .  .",
				"  12 |  2  1  1  1  .  1  .  .  .  .  .  .  .  .",
				"  14 |  1  1  .  .  .  .  .  .  .  .  .  .  .  .",
				"  16 |  4  1  1  .  .  .  1  .  .  .  .  .  .  .",
				"  18 |  1  1  .  1  .  .  .  1  .  .  .  .  .  .",
				"  20 |  2  1  2  .  1  .  .  .  .  .  .  .  .  .",
				"  22 |  1  1  .  .  .  .  .  .  1  .  .  .  .  .",
				"  24 |  3  1  1  1  .  1  .  .  .  .  .  .  .  .",
				"  26 |  1  1  .  .  .  .  .  .  .  .  .  .  .  .",
				"  28 |  2  1  1  .  .  .  .  .  .  1  .  .  .  .",
				"  30 |  1  1  .  1  1  .  .  .  .  .  1  .  .  .",
				"  32 |  5  1  1  .  .  .  1  .  .  .  .  .  .  .",
				"  34 |  1  1  .  .  .  .  .  .  .  .  .  .  .  .",
				"  36 |  2  1  1  1  .  1  .  1  .  .  .  1  .  .",
				"  38 |  1  1  .  .  .  .  .  .  .  .  .  .  .  .",
				"  40 |  3  1  2  .  1  .  .  .  .  .  .  .  1  .",
				"  42 |  1  1  .  2  .  .  .  .  .  .  .  .  .  1"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A339100/b339100.txt\"\u003eTable of n, a(n) for n = 1..1201\u003c/a\u003e"
			],
			"example": [
				"In A214406, row number 4 is:",
				"(k=0) (k=1) (k=2) (k=3) (k=4)",
				"  1    112   718   744   105",
				"Now,",
				"(2*4-0)*  1 + (0+1)*112 =  120",
				"(2*4-1)*112 + (1+1)*718 = 2220",
				"(2*4-2)*718 + (2+1)*744 = 6540",
				"(2*4-3)*744 + (3+1)*105 = 4140",
				"(2*4-4)*105 + (4+1)*  0 =  420",
				"The GCD of {120, 2220, 6540, 4140, 420} is 60, so a(4)=60."
			],
			"mathematica": [
				"T[n_,k_]:=T[n,k]=If[n==0\u0026\u0026k==0,1,If[n==0||k\u003c0||k\u003en,0,(4*n-2*k-1)*T[n-1,k-1]+(2*k+1)*T[n-1,k]]]",
				"A[n_]:=Table[(2*n-k)*T[n,k]+(k+1)*T[n,k+1],{k,0,n}]/.{List-\u003eGCD}",
				"Table[A[n],{n,1,100}]"
			],
			"program": [
				"(PARI)",
				"r(n)=if(n==0,1,(r(n-1)*x/(1-x^2))')",
				"a(n)=my(p=(r(n))'*(1-x^2)^(2*n+1)/2);p/factorback(factor(p))",
				"for(n=1,60,print1(a(n),\", \"))"
			],
			"xref": [
				"Cf. A214406, A165886."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Luc Rousseau_, Nov 23 2020",
			"references": 1,
			"revision": 16,
			"time": "2021-12-14T00:32:12-05:00",
			"created": "2020-12-23T20:26:01-05:00"
		}
	]
}