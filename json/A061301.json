{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A061301",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 61301,
			"data": "1,2,16,4096,4294967296,1208925819614629174706176,6277101735386680763835789423207666416102355444464034512896",
			"name": "a(n) = 2^(n*2^(n-1)).",
			"comment": [
				"Determinant of character table of elementary Abelian group (C_2)^n.",
				"a(7) has 135 digits. - _Jason Earls_, Jun 11 2001",
				"Number of functions f:2^X-\u003e2^X where X is an n-element set such that f(A) is a subset of A for all A in 2^X (where 2^X denotes the power set of X). - _W. Edwin Clark_, Nov 06 2003"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A061301/b061301.txt\"\u003eTable of n, a(n) for n = 0..9\u003c/a\u003e",
				"F. Echenique, \u003ca href=\"https://doi.org/10.1016/j.geb.2006.03.009\"\u003eCounting Combinatorial Choice Rules\u003c/a\u003e, Games and Economic Behavior, Vol. 58, No. 2 (2007), 231-245.",
				"Lara Pudwell, Nathan Chenette, Manda Riehl, \u003ca href=\"http://faculty.valpo.edu/lpudwell/slides/JMM2020_Pudwell.pdf\"\u003eStatistics on Hypercube Orientations\u003c/a\u003e, AMS Special Session on Experimental and Computer Assisted Mathematics, Joint Mathematics Meetings (Denver 2020)."
			],
			"formula": [
				"a(n) = 2^Sum_{i=0..n} i*binomial(n, i) = 2^(2^(n-1)*n). - _W. Edwin Clark_, Nov 06 2003"
			],
			"example": [
				"a(2) = 16 because the character table for C_2 X C_2 is / 1 1 1 1 / 1 -1 -1 1 / 1 -1 1 -1 / 1 1-1 -1 / with determinant 16 = (2^2)^(2^1).",
				"a(1) = 2 since 2^{1} = { {}, {1}} and the functions f : 2^{1}-\u003e2^{1} satisfying f(A) is a subset of A for all A are g and h where g({})={}, g({1})={} and h({}) = {}, h({1})={1}. - _W. Edwin Clark_, Nov 06 2003"
			],
			"mathematica": [
				"Table[2^(n 2^(n - 1)), {n, 0, 7}] (* _Vincenzo Librandi_, Sep 02 2018 *)"
			],
			"program": [
				"(MAGMA) [2^(n*2^(n-1)): n in [0..5]]; // _Vincenzo Librandi_, Sep 02 2018"
			],
			"xref": [
				"Cf. A088322."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "Ahmed Fares (ahmedfares(AT)my-deja.com), Jun 05 2001",
			"ext": [
				"More terms from _Jason Earls_, Jun 11 2001",
				"Edited by _N. J. A. Sloane_, Oct 27 2008 at the suggestion of _R. J. Mathar_",
				"Offset changed to 0 by _Vincenzo Librandi_, Sep 02 2018"
			],
			"references": 4,
			"revision": 26,
			"time": "2020-06-23T18:52:59-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}