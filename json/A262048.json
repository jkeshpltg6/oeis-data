{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A262048",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 262048,
			"data": "1,1,2,2,2,0,0,2,3,1,1,3,3,0,0,3,4,1,2,2,1,4,4,0,0,0,0,4,5,2,1,1,2,5,5,0,2,2,0,5,6,2,1,0,0,1,2,6,6,0,0,0,0,0,0,6,7,2,4,2,2,4,2,7,7,0,0,0,0,0,0,7,8,3,1,0,0,1,3,8,8,0,2,1,2,2,1,2,0,8,9,3,2,1,1,1,1,2,3,9",
			"name": "Irregular triangle read by rows: T(n, k) = A262045(n, k) * A237593(n, k), n \u003e= 1, 1 \u003c= k \u003c= 2 * A003056(n).",
			"comment": [
				"This sequence lists the areas of rectangular pieces computed from the lengths of the legs in the Dyck path for n given in the n-th row of A237593 and the widths between the legs of the (n-1)-st and n-th Dyck paths given in the n-th row of A262045. The sum of the areas of all pieces appears to equal sigma(n), adjusting for a double count of the width at the center (that matters when the two symmetric Dyck paths enclose an odd number of regions).",
				"T(n,k) is the sum of the widths of the symmetric representation of sigma(n) that are associated with the k-th line segment of the n-th Dyck path of the original diagram of the symmetric representation of sigma in the first quadrant after the diagram has been partitioned into two octants whose vertices are (for example) at (0, 1) and (1, 0). Note that the new diagram contains two main diagonals that are parallel between them, see example. - _Omar E. Pol_, Sep 30 2015"
			],
			"example": [
				"For the 9th row: [5 2 2 2 2 5] in A237593 and [1 0 1 1 0 1] in A262045 result in [5 0 2 2 0 5] in this sequence which defines three regions between the Dyck paths for 8 and 9. The areas of the three regions are 5, 3 and 5; note that the width at the center of the middle region is 1 and is counted twice when adding up contiguous subsequences of nonzero numbers. The sum of the three areas is sigma(9) = 13.",
				"For the 19th row: [10 4 2 2 1 1 2 2 4 10] in A237593 and [1 0 0 0 0 0 0 0 0 1] in A262045 result in [10 0 0 0 0 0 0 0 0 10] in this sequence which defines two disjoint regions of area 10 each between the Dyck paths for 18 and 19; their sum is sigma(19) = 20.",
				"The first 20 rows of the triangle:",
				"-------------------------------------",
				"n\\k   1  2  3  4  5  6  7  8  9  10",
				"-------------------------------------",
				"1:    1, 1;",
				"2:    2, 2;",
				"3:    2, 0, 0, 2;",
				"4:    3, 1, 1, 3;",
				"5:    3, 0, 0, 3;",
				"6:    4, 1, 2, 2, 1, 4;",
				"7:    4, 0, 0, 0, 0, 4;",
				"8:    5, 2, 1, 1, 2, 5;",
				"9:    5, 0, 2, 2, 0, 5;",
				"10:   6, 2, 1, 0, 0, 1, 2, 6;",
				"11:   6, 0, 0, 0, 0, 0, 0, 6;",
				"12:   7, 2, 4, 2, 2, 4, 2, 7;",
				"13:   7, 0, 0, 0, 0, 0, 0, 7;",
				"14:   8, 3, 1, 0, 0, 1, 3, 8;",
				"15:   8, 0, 2, 1, 2, 2, 1, 2, 0,  8;",
				"16:   9, 3, 2, 1, 1, 1, 1, 2, 3,  9;",
				"17:   9, 0, 0, 0, 0, 0, 0, 0, 0,  9;",
				"18:  10, 3, 4, 2, 1, 1, 2, 4, 3, 10;",
				"19:  10, 0, 0, 0, 0, 0, 0, 0, 0, 10;",
				"20:  11, 4, 2, 1, 4, 4, 1, 2, 4, 11;",
				"...",
				"A diagram of the first six rows shows the regions with an \"x\" marking the cells on the diagonal that are double counted.",
				".                                        _ _ _ _",
				".                                       |_|_|_|_|_",
				".                                             |_|x|_",
				".                                             |x|_|_|",
				".                  |4 1 2 2 1 4] \u0026 sigma(6)=12    |_|",
				".                                                 |_|",
				".                             _ _ _               |_|",
				".                            |_|_|_|",
				".                                  |_ _",
				".           [3 0 0 3] \u0026 sigma(5)=6   |_|",
				".                                    |_|",
				".                    _ _ _           |_|",
				".                   |_|_|_|_",
				".                       |x|_|",
				".                         |_| [3 1 1 3] \u0026 sigma(4)=7",
				".             _ _         |_|",
				".            |_|_|_",
				".                |_|    [2 0 0 2] \u0026 sigma(3)=4",
				".                |_|",
				".      _ _",
				".     |_ x|    [2 2] \u0026 sigma(2)=3",
				".       |_|",
				".  _",
				". |x|    [1 1] \u0026 sigma(1)=1",
				".",
				"From _Omar E. Pol_, Sep 30 2015: (Start)",
				"Illustration of the 12th row of the triangle:",
				".",
				".           7",
				".     _ _ _ _ _ _ _  2",
				".     / / / / / / /|   4",
				".                 /|_ _  2",
				".                 / / /|",
				".                 / / /      2",
				".                   /        _ 4",
				".                          / /|  2",
				".                        / / /|_ _",
				".                          / / / /|",
				".                                /|",
				".                                /|",
				".                                /| 7",
				".                                /|",
				".                                /|",
				".                                /|",
				".",
				"The original diagram of the symmetric representation of sigma(12) in the first quadrant has been partitioned into two octants. Note that now there are two main diagonals in the diagram. The sums of the widths associated to the successive line segments of the partitioned zig-zag path give [7, 2, 4, 2, 2, 4, 2, 7], the same as the 12th row of the triangle.",
				"(End)"
			],
			"mathematica": [
				"(* auxiliary functions are defined in",
				"A237048, A237593 and A262045 respectively *)",
				"s[n_,k_]:=Ceiling[(n+1)/k-(k+1)/2] - Ceiling[(n+1)/(k+1)-(k+2)/2]",
				"t[n_,k_]:=If[k\u003c=row[n], s[n, k], s[n, 2*row[n]+1-k]",
				"a262048[n_]:=Map[t[n, #]\u0026, Range[2*row[n]]] a262045[n]",
				"Flatten[Map[a262048, Range[16]]](* data *)"
			],
			"xref": [
				"Cf. A000203, A003056, A196020, A236104, A237048, A237270, A237271, A237591, A237593, A262045."
			],
			"keyword": "nonn,tabf",
			"offset": "1,3",
			"author": "_Hartmut F. W. Hoft_, Sep 09 2015",
			"references": 4,
			"revision": 44,
			"time": "2015-10-14T17:25:50-04:00",
			"created": "2015-10-14T17:25:50-04:00"
		}
	]
}