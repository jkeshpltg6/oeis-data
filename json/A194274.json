{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A194274",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 194274,
			"data": "0,1,4,8,12,17,24,32,40,49,60,72,84,97,112,128,144,161,180,200,220,241,264,288,312,337,364,392,420,449,480,512,544,577,612,648,684,721,760,800,840,881,924,968,1012,1057,1104,1152,1200,1249,1300,1352,1404",
			"name": "Concentric square numbers (see Comments lines for definition).",
			"comment": [
				"Cellular automaton on the first quadrant of the square grid. The sequence gives the number of cells \"ON\" in the structure after n-th stage. A098181 gives the first differences. For a definition without words see the illustration of initial terms in the example section. For other concentric polygonal numbers see A194273, A194275 and A032528.",
				"Also, union of A046092 and A077221, the bisections of this sequence.",
				"Also row sums of an infinite square array T(n,k) in which column k lists 4*k-1 zeros followed by the numbers A008574 (see example)."
			],
			"link": [
				"Arkadiusz Wesolowski, \u003ca href=\"/A194274/b194274.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Ce#cell\"\u003eIndex entries for sequences related to cellular automata\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-4,4,-3,1)."
			],
			"formula": [
				"a(n) = n^2 - a(n-2), with a(0)=0, a(1)=1. - _Alex Ratushnyak_, Aug 03 2012",
				"G.f.: x*(1+x) / ((1+x^2)*(1-x)^3). a(n) = (A005563(n)-A056594(n-1))/2. - _R. J. Mathar_, Aug 22 2011",
				"a(n) = a(-n-2) = (2*n*(n+2)+(1-(-1)^n)*i^(n+1))/4, where i=sqrt(-1). - _Bruno Berselli_, Sep 22 2011",
				"a(n) = floor(3*n/4) + floor((n*(n+2)+1)/2) - floor((3*n+1)/4). - _Arkadiusz Wesolowski_, Nov 08 2011",
				"a(0)=0, a(1)=1, a(2)=4, a(3)=8, a(4)=12, a(n)=3*a(n-1)-4*a(n-2)+ 4*a(n-3)- 3*a(n-4)+a(n-5). - _Harvey P. Dale_, Sep 11 2013"
			],
			"example": [
				"Using the numbers A008574 we can write:",
				"0, 1, 4, 8, 12, 16, 20, 24, 28, 32, 36, ...",
				"0, 0, 0, 0, 0,  1,   4,  8, 12, 16, 20, ...",
				"0, 0, 0, 0, 0,  0,   0,  0,  0,  1,  4, ...",
				"And so on.",
				"===========================================",
				"The sums of the columns give this sequence:",
				"0, 1, 4, 8, 12, 17, 24, 32, 40, 49, 60, ...",
				"...",
				"Illustration of initial terms:",
				".                                         o o o o o o",
				".                             o o o o o   o         o",
				".                   o o o o   o       o   o   o o   o",
				".           o o o   o     o   o   o   o   o   o o   o",
				".     o o   o   o   o     o   o       o   o         o",
				". o   o o   o o o   o o o o   o o o o o   o o o o o o",
				".",
				". 1    4      8        12         17           24"
			],
			"mathematica": [
				"Table[Floor[3*n/4] + Floor[(n*(n + 2) + 1)/2] - Floor[(3*n + 1)/4], {n, 0, 52}] (* _Arkadiusz Wesolowski_, Nov 08 2011 *)",
				"RecurrenceTable[{a[0]==0,a[1]==1,a[n]==n^2-a[n-2]},a,{n,60}] (* or *) LinearRecurrence[{3,-4,4,-3,1},{0,1,4,8,12},60] (* _Harvey P. Dale_, Sep 11 2013 *)"
			],
			"program": [
				"(Python)",
				"prpr = 0",
				"prev = 1",
				"for n in range(2,777):",
				"    print(str(prpr), end=\", \")",
				"    curr = n*n - prpr",
				"    prpr = prev",
				"    prev = curr",
				"# _Alex Ratushnyak_, Aug 03 2012"
			],
			"xref": [
				"Cf. A000290, A085250, A008574, A032528, A046092, A077221, A098181, A194273, A194275."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Omar E. Pol_, Aug 20 2011",
			"references": 8,
			"revision": 52,
			"time": "2021-03-30T04:40:38-04:00",
			"created": "2011-08-22T13:24:24-04:00"
		}
	]
}