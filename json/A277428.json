{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A277428",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 277428,
			"data": "0,1,4,9,11,22,75,105,449,425,1426,2837,4765,2775,21895,57558,87602,145177,123788,694479,677326,1516496,3363284,2048443,26968428,24488513,98733728",
			"name": "a(n) = the n-bit number in which the i-th bit is 1 if and only if prime(i) divides A060795(n).",
			"comment": [
				"a(n) is also the n-bit number in which the i-th bit is 1 if and only if prime(i) does not divide A060796(n).",
				"a(n) is also the encoding of the fraction defined as follows:",
				"Consider the set of fractions that can be built by only using each prime number from prime(1) to prime(n) exactly once as factors, either in the numerator or in the numerator. There are 2^n such fractions. One of them, let's call it x, has the property of yielding the result nearest to 1. a(n) is the n-bit number in which the i-th bit is 1 if prime(i) appears in the numerator of x, 0 if prime(i) appears in the denominator of x.",
				"Remark: x is A060795(n) / A060796(n). Notice how in this prime-rank-to-bit representation, A060795(n) and A060796(n) are each other's bitwise negation."
			],
			"example": [
				"For n = 1, two distinct fractions can be written with the first prime number, namely 1/2 and 2. Of the two, 1/2 is nearer to 1. 1/2 has its 2 below the fraction bar, so its binary encoding is 0, which yields a(1) = 0.",
				"For n = 2, four distinct fractions can be written with the first two prime numbers, namely 1/6, 2/3, 3/2 and 6. 2/3 is the nearest to 1. 2/3 has its 2 above the fraction bar and its 3 below, so its encoding is 01, which yields a(2) = 1."
			],
			"mathematica": [
				"{0}~Join~Table[Function[p, FromDigits[#, 2] \u0026@ Reverse@ MapAt[# + 1 \u0026, ConstantArray[0, n], Partition[#, 1, 1]] \u0026@ PrimePi@ FactorInteger[Numerator@ #][[All, 1]] \u0026@ Max@ Select[Map[p/#^2 \u0026, Divisors@ p], # \u003c 1 \u0026]][Times @@ Prime@ Range@ n], {n, 2, 23}] (* _Michael De Vlieger_, Oct 19 2016 *)"
			],
			"program": [
				"(Java)",
				"package oeis;",
				"public class BinaryEncodedBestPrimeSetup {",
				"  // Brute force implementation... Can it be improved?",
				"public static int PRIME[] = { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, /* to be continued */ };",
				"public static void main(String args[]) {",
				"  int nMax = PRIME.length; // number of terms of the sequence",
				"  for (int n = 1; n \u003c= nMax; n ++) {",
				"   if (n \u003e 1) {",
				"    System.out.print(\", \");",
				"   }",
				"   System.out.print(u(n));",
				"  }",
				"}",
				"private static int u(int n) {",
				"  double bestMul = 0.0;",
				"  int bestSetup = -1;",
				"  int s = 0; // binary-encoded setup number",
				"  for (s = 0; s \u003c (1 \u003c\u003c n); s ++) {",
				"   double mul = 1.0;",
				"   int i = 0; // prime number #",
				"   for (i = 0; i \u003c n; i ++) {",
				"    if ((s \u0026 (1 \u003c\u003c i)) != 0) {",
				"     mul *= PRIME[i]; // 1 = above fraction bar",
				"    } else {",
				"     mul /= PRIME[i]; // 0 = below fraction bar",
				"    }",
				"   }",
				"   if (mul \u003c 1.0) {",
				"    if (mul \u003e bestMul) {",
				"     bestMul = mul;",
				"     bestSetup = s;",
				"    }",
				"   }",
				"  }",
				"  return bestSetup;",
				"}",
				"}",
				"(PARI) a060795(n) = my(m=prod(i=1, n, prime(i))); divisors(m)[numdiv(m)\\2];",
				"a(n) = {my(a95 = a060795(n)); v = vector(n, i, (a95 % prime(i))==0); subst(Polrev(v), x, 2); } \\\\ _Michel Marcus_, Dec 03 2016"
			],
			"xref": [
				"Encodes A060795 and A060796. Cf. A002110, A261144."
			],
			"keyword": "nonn,base,more",
			"offset": "1,3",
			"author": "_Luc Rousseau_, Oct 14 2016",
			"ext": [
				"a(22)-a(27) from _Michael De Vlieger_, Oct 19 2016"
			],
			"references": 0,
			"revision": 35,
			"time": "2016-12-03T12:04:23-05:00",
			"created": "2016-12-03T12:04:23-05:00"
		}
	]
}