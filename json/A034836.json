{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A034836",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 34836,
			"data": "1,1,1,2,1,2,1,3,2,2,1,4,1,2,2,4,1,4,1,4,2,2,1,6,2,2,3,4,1,5,1,5,2,2,2,8,1,2,2,6,1,5,1,4,4,2,1,9,2,4,2,4,1,6,2,6,2,2,1,10,1,2,4,7,2,5,1,4,2,5,1,12,1,2,4,4,2,5,1,9,4,2,1,10,2,2,2,6,1,10,2,4,2,2,2,12,1,4,4,8",
			"name": "Number of ways to write n as n = x*y*z with 1 \u003c= x \u003c= y \u003c= z.",
			"comment": [
				"Number of boxes with integer edge lengths and volume n.",
				"Starts the same as, but is different from, A033273. First values of n such that a(n) differs from A033273(n) are 36,48,60,64,72,80,84,90,96,100. - _Benoit Cloitre_, Nov 25 2002",
				"a(n) depends only on the signature of n; the sorted exponents of n. For instance, a(12) and a(18) are the same because both 12 and 18 have signature (1,2). - _T. D. Noe_, Nov 02 2011",
				"Number of 3D grids of n congruent cubes, in a box, modulo rotation (cf. A007425 and A140773 for boxes instead of cubes; cf. A038548 for the 2D case). - _Manfred Boergens_, Apr 06 2021"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A034836/b034836.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"D. Andrica and E. J. Ionascu, \u003ca href=\"http://www.emis.de/journals/ASUO/mathematics_/vol22-1/Andrica_D__Ionascu_E.J._nou-1__final_.pdf\"\u003eOn the number of polynomials with coefficients in [n]\u003c/a\u003e, An. St. Univ. Ovidius Constanta, 2013.",
				"\u003ca href=\"/index/Eu#epf\"\u003eIndex entries for sequences computed from exponents in factorization of n\u003c/a\u003e"
			],
			"formula": [
				"From _Ton Biegstraaten_, Jan 04 2016: (Start)",
				"Given a number n, let s(1),...,s(m) be the signature list of n, and a(n) the resulting number in the sequence.",
				"Then np = Product_{k=1..m} binomial(2+s(k),2) is the total number of products solely based on the combination of exponents. The multiplicity of powers is not taken into account (e.g., all combinations of 1,2,4 (6 times) but (2,2,2) only once). See next formulas to compute corrections for 3rd and 2nd powers.",
				"Let ntp = Product_{k=1..m} (floor((s(k) - s(k) mod(3))/s(k))) if the number is a 3rd power or not resulting in 1 or 0.",
				"Let nsq = Product_{k=1..m} (floor(s(k)/2) + 1) is the number of squares.",
				"Conjecture: a(n) = (np + 3*(nsq - ntp) + 5*ntp)/6 = (np + 3*nsq + 2*ntp)/6.",
				"Example: n = 1728; s = [3,6]; np = 10*28 = 280; nsq = 2*4 = 8; ntp = 1 so a(1728)=51 (as in the b-file).",
				"(End)",
				"a(n) \u003e= A226378(n) for all n \u003e= 1. - _Antti Karttunen_, Aug 30 2017",
				"From _Bernard Schott_, Dec 12 2021: (Start)",
				"a(n) = 1 iff n = 1 or n is prime (A008578).",
				"a(n) = 2 iff n is semiprime (A001358) (see examples). (End)"
			],
			"example": [
				"a(12) = 4 because we can write 12 = 1*1*12 = 1*2*6 = 1*3*4 = 2*2*3.",
				"a(36) = 8 because we can write 36 = 1*1*36 = 1*2*18 = 1*3*12 = 1*4*9 = 1*6*6 = 2*2*9 = 2*3*6 = 3*3*4.",
				"For n = p*q, p \u003c q primes: a(n) = 2 because we can write n = 1*1*pq = 1*p*q.",
				"For n = p^2, p prime: a(n) = 2 because we can write n = 1*1*p^2 = 1*p*p."
			],
			"maple": [
				"f:=proc(n) local t1,i,j,k; t1:=0; for i from 1 to n do for j from i to n do for k from j to n do if i*j*k = n then t1:=t1+1; fi; od: od: od: t1; end;"
			],
			"mathematica": [
				"Table[c=0; Do[If[i\u003c=j\u003c=k \u0026\u0026 i*j*k==n,c++],{i,t=Divisors[n]},{j,t},{k,t}]; c,{n,100}] (* _Jayanta Basu_, May 23 2013 *)",
				"(* Similar to the first Mathematica code but with fewer steps in Do[..] *)",
				"b=0; d=Divisors[n]; r=Length[d];",
				"Do[If[d[[h]] d[[i]] d[[j]]==n, b++], {h, r}, {i, h, r}, {j, i, r}]; b (* _Manfred Boergens_, Apr 06 2021 *)"
			],
			"program": [
				"(PARI) A038548(n)=sumdiv(n, d, d*d\u003c=n) /* \u003c== rhs from A038548 (_Michael Somos_) */",
				"a(n)=sumdiv(n, d, if(d^3\u003c=n, A038548(n/d) - sumdiv(n/d, d0, d0\u003cd))) \\\\ _Rick L. Shepherd_, Aug 27 2006"
			],
			"xref": [
				"See also: writing n = x*y (A038548, unordered, A000005, ordered), n = x*y*z (this sequence, unordered, A007425, ordered), n = w*x*y*z (A007426, ordered)",
				"Cf. A001358, A008578, A088432, A088433, A088434.",
				"Differs from A033273 and A226378 for the first time at n=36."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Erich Friedman_",
			"ext": [
				"Definition simplified by _Jonathan Sondow_, Oct 03 2013"
			],
			"references": 35,
			"revision": 67,
			"time": "2021-12-12T09:28:07-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}