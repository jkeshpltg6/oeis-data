{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A145876",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 145876,
			"data": "1,1,1,2,2,2,5,7,7,5,16,26,36,26,16,61,117,182,182,117,61,272,594,1056,1196,1056,594,272,1385,3407,6669,8699,8699,6669,3407,1385,7936,21682,46348,67054,76840,67054,46348,21682,7936,50521,151853,350240,556952,704834,704834,556952,350240,151853,50521",
			"name": "Triangle read by rows: T(n,k) is the number of permutations of [n] having k-1 alternating descents (1\u003c=k\u003c=n). The index i is an alternating descent of a permutation p if either i is odd and p(i)\u003ep(i+1), or i is even and p(i)\u003cp(i+1).",
			"comment": [
				"Row sums are the factorials (A000142).",
				"T(n,1) = T(n,n) = A000111(n) (Euler or up-down numbers).",
				"Sum(k*T(n,k), k=1..n) = (n+1)!/2 = A001710(n+1).",
				"From _Peter Bala_, Jun 11 2011: (Start)",
				"Koutras has defined generalized Eulerian numbers associated with a sequence of polynomials - the ordinary Eulerian numbers A008292 being associated with the sequence of monomials x^n. The present array is the triangle of Eulerian numbers associated with the sequence of zigzag polynomials Z(n,x) defined in A147309.",
				"See A109449, A147315 and A185424 for the respective analogs of the Pascal triangle, the Stirling numbers of the second kind and the Bernoulli numbers, associated with the sequence of zigzag polynomials. (End)",
				"From _Vaclav Kotesovec_, Apr 29 2018: (Start)",
				"In general, for fixed k\u003e=1, T(n,k) ~ (4-Pi)^(k-1) * 2^(n+2) * n^(k-1) * n! / ((k-1)! * Pi^(n + k)).",
				"Equivalently, for fixed k\u003e=1, T(n,k) ~ (4-Pi)^(k-1) * 2^(n + 5/2) * n^(n + k - 1/2) / ((k-1)! * Pi^(n + k - 1/2) * exp(n)). (End)"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A145876/b145876.txt\"\u003eTable of n, a(n) for n = 1..141, flattened\u003c/a\u003e",
				"D. Chebikin, \u003ca href=\"https://doi.org/10.37236/856\"\u003eVariations on descents and inversions in permutations\u003c/a\u003e, The Electronic J. of Combinatorics, 15 (2008), #R132.",
				"M. V. Koutras, \u003ca href=\"http://www.fq.math.ca/Scanned/32-1/koutras.pdf\"\u003eEulerian numbers associated with sequences of polynomials\u003c/a\u003e, The Fibonacci Quarterly, 32 (1994), 44-57.",
				"Shi-Mei Ma, Qi Fang, Toufik Mansour, and Yeong-Nan Yeh, \u003ca href=\"https://arxiv.org/abs/2104.09374\"\u003eAlternating Eulerian polynomials and left peak polynomials\u003c/a\u003e, arXiv:2104.09374 [math.CO], 2021.",
				"S.-M. Ma and Y.-M. Yeh, \u003ca href=\"https://doi.org/10.1016/j.disc.2015.12.007\"\u003eEnumeration of permutations by number of alternating descents\u003c/a\u003e, Discr. Math., 339 (2016), 1362-1367."
			],
			"formula": [
				"E.g.f.: F(t,u) = t*(1-tan(u*(t-1))-sec(u*(t-1)))/(tan(u*(t-1))+sec(u*(t-1))-t).",
				"From _Peter Bala_, Jun 11 2011: (Start)",
				"T(n,k) = Sum_{j = 0..k} (-1)^(k-j)*binomial(n+1,k-j)*Z(n,j), where Z(n,x) are the zigzag polynomials defined in A147309.",
				"Let M denote the triangular array A109449. The first column of the array (I-x*M)^-1 is a sequence of rational functions in x whose numerator polynomials are the row polynomials of the present array.",
				"(End)",
				"From _Vladimir Shevelev_, Jul 01 2011:  (Start)",
				"a(2^(2*n-1)-2^(n-1)+1) == 1 (mod 2^n).",
				"If n is odd prime, then a(2*n^2-n+1) == 1 (mod 2*n) and a((n^2-n+2)/2) == (-1)^((n-1)/2).",
				"(End)"
			],
			"example": [
				"T(4,3) = 7 because we have 1243, 4123, 1342, 3124, 2134, 2341 and 4321. For example, for p=1342 the alternating descent is {2,3}; indeed, 2 is even and p(2)=3 \u003c p(3)=4, while 3 is odd and p(3)=4 \u003e p(4)=2.",
				"Triangle starts:",
				"     1;",
				"     1,    1;",
				"     2,    2,    2;",
				"     5,    7,    7,    5;",
				"    16,   26,   36,   26,   16;",
				"    61,  117,  182,  182,  117,   61;",
				"   272,  594, 1056, 1196, 1056,  594,  272;",
				"  1385, 3407, 6669, 8699, 8699, 6669, 3407, 1385;"
			],
			"maple": [
				"F:=t*(1-tan(u*(t-1))-sec(u*(t-1)))/(tan(u*(t-1))+sec(u*(t-1))-t): Fser:= simplify(series(F,u=0,12)): for n from 0 to 10 do P[n]:=sort(expand(factorial(n)*coeff(Fser,u,n))) end do: for n to 10 do seq(coeff(P[n],t,j),j=1..n) end do; # yields sequence in triangular form",
				"# second Maple program:",
				"b:= proc(u, o) option remember; expand(`if`(u+o=0, 1,",
				"       add(b(o+j-1, u-j)*x, j=1..u)+",
				"       add(b(o-j, u-1+j),   j=1..o)))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, x, i), i=1..n))(b(n, 0)):",
				"seq(T(n), n=1..12);  # _Alois P. Heinz_, Nov 18 2013, Apr 15 2018"
			],
			"mathematica": [
				"b[u_, o_, t_] := b[u, o, t] = If[u+o == 0, 1, Expand[",
				"     Sum[b[u+j-1, o-j, !t]*If[t, 1, x], {j, 1, o}] +",
				"     Sum[b[u-j, o+j-1, !t]*If[t, x, 1], {j, 1, u}]]];",
				"T[n_] := Function[{p}, Table[Coefficient[p, x, i], {i, 0, n-1}]][b[0, n, True]];",
				"Table[T[n], {n, 1, 12}] // Flatten (* _Jean-François Alcover_, Feb 19 2015, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A000142, A001710, A008292, A109449, A147315, A185424.",
				"Columns k=1-10 give: A000111, A302691, A302895, A302896, A302897, A302898, A302899, A302900, A302901, A302902.",
				"Cf. A302903 (T(2n+1,n+1)), A302904 (T(2n,n)), A302905 (T(n,ceiling(n/2)))."
			],
			"keyword": "nonn,tabl",
			"offset": "1,4",
			"author": "_Emeric Deutsch_, Oct 22 2008",
			"references": 18,
			"revision": 73,
			"time": "2021-04-21T12:37:39-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}