{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A244108",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 244108,
			"data": "1,1,2,2,4,16,40,80,80,8,64,400,2240,11360,55040,253440,1056000,3801600,10982400,21964800,21964800,16,208,2048,18816,168768,1508032,13501312,121362560,1099169280,10049994240,92644597760,857213660160,7907423180800,72155129446400",
			"name": "Number T(n,k) of permutations of {1,2,...,n} that result in a binary search tree of height k; triangle T(n,k), k\u003e=0, k\u003c=n\u003c=2^k-1, read by columns.",
			"comment": [
				"Empty external nodes are counted in determining the height of a search tree."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A244108/b244108.txt\"\u003eColumns k = 0..9, flattened\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Binary_search_tree\"\u003eBinary search tree\u003c/a\u003e",
				"\u003ca href=\"/index/Ro#rooted\"\u003eIndex entries for sequences related to rooted trees\u003c/a\u003e",
				"\u003ca href=\"/index/Tra#trees\"\u003eIndex entries for sequences related to trees\u003c/a\u003e"
			],
			"formula": [
				"Sum_{k=0..n} k * T(n,k) = A316944(n).",
				"Sum_{k=n..2^n-1} k * T(k,n) = A317012(n)."
			],
			"example": [
				"Triangle T(n,k) begins:",
				": 1;",
				":    1;",
				":       2;",
				":       2,  4;",
				":          16,      8;",
				":          40,     64,      16;",
				":          80,    400,     208,      32;",
				":          80,   2240,    2048,     608,     64;",
				":               11360,   18816,    8352,   1664,   128;",
				":               55040,  168768,  104448,  30016,  4352,   256;",
				":              253440, 1508032, 1277568, 479040, 99200, 11008, 512;"
			],
			"maple": [
				"b:= proc(n, k) option remember; `if`(n\u003c2, `if`(k\u003cn, 0, 1),",
				"      add(binomial(n-1, r)*b(r, k-1)*b(n-1-r, k-1), r=0..n-1))",
				"    end:",
				"T:= (n, k)-\u003e b(n, k)-b(n, k-1):",
				"seq(seq(T(n, k), n=k..2^k-1), k=0..5);"
			],
			"mathematica": [
				"b[n_, k_] := b[n, k] = If[n\u003c2, If[k\u003cn, 0, 1], Sum[Binomial[n-1, r]*b[r, k-1]*b[n-1-r, k-1], {r, 0, n-1}]]; T[n_, k_] := b[n, k] - b[n, k-1]; Table[T[n, k], {k, 0, 5}, {n, k, 2^k-1}] // Flatten (* _Jean-François Alcover_, Feb 19 2017, translated from Maple *)"
			],
			"xref": [
				"Row sums give A000142.",
				"Column sums give A227822.",
				"Main diagonal gives A011782, lower diagonal gives A076616.",
				"T(n,A000523(n)+1) = A076615(n).",
				"T(2^n-1,n) = A056972(n).",
				"T(2n,n) = A265846(n).",
				"Cf. A195581 (the same read by rows), A195582, A195583, A316944, A317012."
			],
			"keyword": "nonn,tabf",
			"offset": "0,3",
			"author": "_Alois P. Heinz_, Dec 21 2015",
			"references": 11,
			"revision": 67,
			"time": "2020-07-07T20:20:38-04:00",
			"created": "2015-12-21T15:11:18-05:00"
		}
	]
}