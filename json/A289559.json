{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A289559",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 289559,
			"data": "1,2,3,3,3,6,7,3,7,6,11,9,10,14,9,3,13,14,19,9,21,22,23,9,11,20,19,21,22,18,31,6,33,26,21,21,37,38,30,9,41,42,43,33,21,46,47,9,43,22,39,30,53,38,33,21,57,44,59,27,61,62,49,11,30,66,67",
			"name": "Number of modulo n residues among sums of two fourth powers.",
			"comment": [
				"Conjecture: the only primes p for which a(p) \u003c p are 5, 13, 17, 29. - _Robert Israel_, Jul 09 2017",
				"Conjecture is true: see Math Overflow link. - _Robert Israel_, Apr 01 2020"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A289559/b289559.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Mathematics Overflow, \u003ca href=\"https://mathoverflow.net/questions/356270/does-the-expression-x4-y4-take-on-all-values-in-mathbbz-p-mathbbz\"\u003eDoes the expression x^4+y^4 take on all values in Z/pZ\u003c/a\u003e (see answer by J. Silverman)"
			],
			"example": [
				"a(7) = 7 because (j^4 + k^4) mod 7, where j and k are integers, can take on all 7 values 0..6; e.g.:",
				"   (0^4 + 0^4) mod 7 = ( 0 +  0) mod 7 =  0 mod 7 = 0;",
				"   (0^4 + 1^4) mod 7 = ( 0 +  1) mod 7 =  1 mod 7 = 1;",
				"   (1^4 + 1^4) mod 7 = ( 1 +  1) mod 7 =  2 mod 7 = 2;",
				"   (1^4 + 2^4) mod 7 = ( 1 + 16) mod 7 = 17 mod 7 = 3;",
				"   (2^4 + 2^4) mod 7 = (16 + 16) mod 7 = 32 mod 7 = 4;",
				"   (1^4 + 3^4) mod 7 = ( 1 + 81) mod 7 = 82 mod 7 = 5;",
				"   (2^4 + 3^4) mod 7 = (16 + 81) mod 7 = 97 mod 7 = 6.",
				"a(16) = 3 because (j^4 + k^4) mod 16 can take on only the three values 0, 1, and 2. (This is because j^4 mod 16 = 0 for all even j and 1 for all odd j.)"
			],
			"maple": [
				"f1:= proc(n) option remember; local S;",
				"    S:= {seq(x^4 mod n, x=0..n-1)};",
				"  nops({seq(seq(S[i]+S[j] mod n,i=1..j),j=1..nops(S))});",
				"end proc:",
				"f:= proc(n) local t;",
				"mul(f1(t[1]^t[2]), t = ifactors(n)[2])",
				"end proc:",
				"map(f, [$1..100]); # _Robert Israel_, Jul 09 2017"
			],
			"mathematica": [
				"f1[n_] := f1[n] = Module[{S = Table[Mod[x^4, n], {x, 0, n-1}] // Union}, Table[Mod[S[[i]] + S[[j]], n], {j, 1, Length[S]}, {i, 1, j}] // Flatten // Union // Length];",
				"f[n_] := Module[{p, e}, Product[{p, e} = pe; f1[p^e], {pe, FactorInteger[n]}]];",
				"Array[f, 100] (* _Jean-François Alcover_, Jul 30 2020, after Maple *)"
			],
			"program": [
				"(PARI) a(n) = #Set(vector(n^2, i, ((i%n)^4 + (i\\n)^4) % n)); \\\\ _Michel Marcus_, Jul 08 2017"
			],
			"xref": [
				"Cf. A155918 (gives number of modulo n residues among sums of two squares)."
			],
			"keyword": "nonn,mult",
			"offset": "1,2",
			"author": "_Jon E. Schoenfield_, Jul 08 2017",
			"references": 2,
			"revision": 20,
			"time": "2020-07-30T07:17:39-04:00",
			"created": "2017-07-08T00:58:46-04:00"
		}
	]
}