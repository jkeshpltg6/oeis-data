{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A210030",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 210030,
			"data": "1,-2,-2,4,6,-8,-12,16,22,-30,-40,52,68,-88,-112,144,182,-228,-286,356,440,-544,-668,816,996,-1210,-1464,1768,2128,-2552,-3056,3648,4342,-5160,-6116,7232,8538,-10056,-11820,13872,16248,-18996,-22176,25844,30068",
			"name": "Expansion of phi(-q) / phi(q^2) in powers of q where phi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A210030/b210030.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of eta(q)^2 * eta(q^2) * eta(q^8)^2 / eta(q^4)^5 in powers of q.",
				"Euler transform of period 8 sequence [ -2, -3, -2, 2, -2, -3, -2, 0, ...].",
				"G.f.: (Sum_k (-1)^k * x^k^2) / (Sum_k x^(2 * k^2)).",
				"a(n) = (-1)^n * A080015(n) = (-1)^[(n + 1) / 4] * A080054(n).",
				"Convolution inverse of A208850."
			],
			"example": [
				"1 - 2*q - 2*q^2 + 4*q^3 + 6*q^4 - 8*q^5 - 12*q^6 + 16*q^7 + 22*q^8 + ..."
			],
			"mathematica": [
				"a[n_]:= SeriesCoefficient[EllipticTheta[3, 0, -q]/EllipticTheta[3, 0, q^2], {q, 0, n}]; Table[a[n], {n, 0, 50}] (* _G. C. Greubel_, Dec 17 2017 *)"
			],
			"program": [
				"(PARI) {a(n) = local(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A)^2 * eta(x^2 + A) * eta(x^8 + A)^2 / eta(x^4 + A)^5, n))}"
			],
			"xref": [
				"Cf. A080015, A080054, A208850."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Mar 16 2012",
			"references": 4,
			"revision": 14,
			"time": "2021-03-12T22:24:46-05:00",
			"created": "2012-03-16T18:37:07-04:00"
		}
	]
}