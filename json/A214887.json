{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A214887",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 214887,
			"data": "1,7,7,49,343,16807,5764801,96889010407,558545864083284007,54116956037952111668959660849,30226801971775055948247051683954096612865741943",
			"name": "a(n) = a(n-1)*a(n-2) with a(0)=1, a(1)=7.",
			"comment": [
				"a(17) has 1350 digits.",
				"From _Peter Bala_, Nov 01 2013: (Start)",
				"Let phi = 1/2*(1 + sqrt(5)) denote the golden ratio A001622. This sequence is the simple continued fraction expansion of the constant c := 6*sum {n = 1..inf} 1/7^floor(n*phi) (= 36*sum {n = 1..inf} floor(n/phi)/7^n) = 0.87718 67194 00499 51922 ... = 1/(1 + 1/(7 + 1/(7 + 1/(49 + 1/(343 + 1/(16807 + 1/(5764801 + ...))))))). The constant c is known to be transcendental (see Adams and Davison 1977). Cf. A014565.",
				"Furthermore, for k = 0,1,2,... if we define the real number X(k) = sum {n \u003e= 1} 1/7^(n*Fibonacci(k) + Fibonacci(k+1)*floor(n*phi)) then the real number X(k+1)/X(k) has the simple continued fraction expansion [0; a(k+1), a(k+2), a(k+3), ...] (apply Bowman 1988, Corollary 1). (End)"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A214887/b214887.txt\"\u003eTable of n, a(n) for n = 0..16\u003c/a\u003e",
				"W. W. Adams and J. L. Davison, \u003ca href=\"http://www.jstor.org/stable/2041889\"\u003eA remarkable class of continued fractions\u003c/a\u003e, Proc. Amer. Math. Soc. 65 (1977), 194-198.",
				"P. G. Anderson, T. C. Brown, P. J.-S. Shiue, \u003ca href=\"http://people.math.sfu.ca/~vjungic/tbrown/tom-28.pdf\"\u003eA simple proof of a remarkable continued fraction identity\u003c/a\u003e, Proc. Amer. Math. Soc. 123 (1995), 2005-2009.",
				"D. Bowman, \u003ca href=\"http://www.fq.math.ca/26-1.html\"\u003eA new generalization of Davison's theorem\u003c/a\u003e, Fib. Quart. Volume 26 (1988), 40-45"
			],
			"formula": [
				"a(n) = 7^Fibonacci(n)."
			],
			"maple": [
				"a:= n-\u003e 7^(\u003c\u003c1|1\u003e, \u003c1|0\u003e\u003e^n)[1, 2]:",
				"seq(a(n), n=0..12);  # _Alois P. Heinz_, Jun 17 2014"
			],
			"mathematica": [
				"7^Fibonacci[Range[0,10]]",
				"nxt[{a_,b_}]:={b,a*b}; Transpose[NestList[nxt,{1,7},10]][[1]] (* _Harvey P. Dale_, Jun 10 2014 *)"
			],
			"program": [
				"(MAGMA) [7^Fibonacci(n): n in [0..10]];"
			],
			"xref": [
				"Cf. A000045, A000301, A010098, A010099, A010100, A214706, A014565, A215270, A215271, A215272.",
				"Column k=7 of A244003."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Vincenzo Librandi_, Aug 01 2012",
			"references": 9,
			"revision": 33,
			"time": "2014-06-17T13:35:18-04:00",
			"created": "2012-08-01T07:50:22-04:00"
		}
	]
}