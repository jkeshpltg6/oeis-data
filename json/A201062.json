{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A201062",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 201062,
			"data": "90,1770,2190,10080,24360,35910,156750,208620,304920,306390,328020,422190,526350,639330,706860,866460,1030770,1111620,1147440,1151100,1447530,1769670,1793070,2024610,2320170,2335080,2403570",
			"name": "Record (maximal) gaps between prime quintuplets (p, p+4, p+6, p+10, p+12).",
			"comment": [
				"Prime quintuplets (p, p+4, p+6, p+10, p+12) are one of the two types of densest permissible constellations of 5 primes (A022006 and A022007). Average gaps between prime k-tuples can be deduced from the Hardy-Littlewood k-tuple conjecture and are O(log^k(p)), with k=5 for quintuplets. If a gap is larger than all preceding gaps, we call it a maximal gap, or a record gap. Maximal gaps may be significantly larger than average gaps; this sequence suggests that maximal gaps between quintuplets are O(log^6(p)).",
				"A201063 lists initial primes in quintuplets (p, p+4, p+6, p+10, p+12) preceding the maximal gaps. A233433 lists the corresponding primes at the end of the maximal gaps."
			],
			"link": [
				"Alexei Kourbatov, \u003ca href=\"/A201062/b201062.txt\"\u003eTable of n, a(n) for n = 1..71\u003c/a\u003e",
				"Tony Forbes, \u003ca href=\"http://anthony.d.forbes.googlepages.com/ktuplets.htm\"\u003ePrime k-tuplets\u003c/a\u003e",
				"G. H. Hardy and J. E. Littlewood, \u003ca href=\"https://dx.doi.org/10.1007/BF02403921\"\u003eSome problems of 'Partitio numerorum'; III: on the expression of a number as a sum of primes\u003c/a\u003e, Acta Mathematica, Vol. 44, pp. 1-70, 1923.",
				"Alexei Kourbatov, \u003ca href=\"http://www.javascripter.net/math/primes/maximalgapsbetweenprimequintuplets.htm\"\u003eMaximal gaps between prime quintuplets\u003c/a\u003e (graphs/data up to 10^15)",
				"A. Kourbatov, \u003ca href=\"http://arxiv.org/abs/1301.2242\"\u003eMaximal gaps between prime k-tuples: a statistical approach\u003c/a\u003e, arXiv preprint arXiv:1301.2242 [math.NT], 2013 and \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL16/Kourbatov/kourbatov3.html\"\u003eJ. Int. Seq. 16 (2013) #13.5.2\u003c/a\u003e",
				"Alexei Kourbatov, \u003ca href=\"http://arxiv.org/abs/1309.4053\"\u003eTables of record gaps between prime constellations\u003c/a\u003e, arXiv preprint arXiv:1309.4053 [math.NT], 2013.",
				"Alexei Kourbatov, \u003ca href=\"http://arxiv.org/abs/1401.6959\"\u003eThe distribution of maximal prime gaps in Cramer's probabilistic model of primes\u003c/a\u003e, arXiv preprint arXiv:1401.6959 [math.NT], 2014.",
				"Alexei Kourbatov and Marek Wolf, \u003ca href=\"http://arxiv.org/abs/1901.03785\"\u003ePredicting maximal gaps in sets of primes\u003c/a\u003e, arXiv preprint arXiv:1901.03785 [math.NT], 2019.",
				"Eric W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/k-TupleConjecture.html\"\u003ek-Tuple Conjecture\u003c/a\u003e"
			],
			"formula": [
				"(1) Upper bound: gaps between prime quintuplets are smaller than 0.0987*(log p)^6, where p is the prime at the end of the gap.",
				"(2) Estimate for the actual size of the maximal gap that ends at p: maximal gap ~ a(log(p/a)-0.4), where a = 0.0987*(log p)^5 is the average gap between quintuplets near p, as predicted by the Hardy-Littlewood k-tuple conjecture.",
				"Formulas (1) and (2) are asymptotically equal as p tends to infinity. However, (1) yields values greater than all known gaps, while (2) yields \"good guesses\" that may be either above or below the actual size of known maximal gaps.",
				"Both formulas (1) and (2) are derived from the Hardy-Littlewood k-tuple conjecture via probability-based heuristics relating the expected maximal gap size to the average gap. Neither of the formulas has a rigorous proof (the k-tuple conjecture itself has no formal proof either). In both formulas, the constant ~0.0987 is reciprocal to the Hardy-Littlewood 5-tuple constant 10.1317..."
			],
			"example": [
				"The gap of 90 between quintuplets starting at p=7 and p=97 is the very first gap, so a(1)=90. The gap of 1770 between quintuplets starting at p=97 and p=1867 is a maximal gap - larger than any preceding gap; therefore a(2)=1770. The gap after p=1867 is smaller, so a new term is not added."
			],
			"xref": [
				"Cf. A022007 (prime quintuplets p, p+4, p+6, p+10, p+12), A113274, A113404, A200503, A201596, A201598, A201073, A201051, A201251, A202281, A202361, A201063, A002386, A233433."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Alexei Kourbatov_, Nov 26 2011",
			"references": 11,
			"revision": 44,
			"time": "2019-01-15T15:40:55-05:00",
			"created": "2011-11-28T12:57:41-05:00"
		}
	]
}