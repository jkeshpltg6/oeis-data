{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A112264",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 112264,
			"data": "0,2,3,4,5,5,7,6,6,7,1,7,1,9,8,8,1,8,1,9,10,3,2,9,10,3,9,11,2,10,3,10,4,3,12,10,3,3,4,11,4,12,4,5,11,4,4,11,14,12,4,5,5,11,6,13,4,4,5,12,6,5,13,12,6,6,6,5,5,14,7,12,7,5,13,5,8,6,7,13,12,6,8,14,6,6,5,7,8,13,8,6,6,6",
			"name": "Sum of initial digits of prime factors (with multiplicity) of n.",
			"comment": [
				"For primes p, elements of A000040, a(p) = A000030(p). The cumulative sum of this sequence is A112265. Primes in the cumulative sum are A112266. This is a base 10 sequence, the base 1 equivalent is A001222(n) = BigOmega(n) = e_1 + e_2 + ... + e_k, the number of prime factors (with multiplicity), where k = A001221(n) = SmallOmega(n). The base 2 equivalent is equal to the base 1 equivalent."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A112264/b112264.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PrimeFactor.html\"\u003ePrime Factor\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DistinctPrimeFactors.html\"\u003eDistinct Prime Factors\u003c/a\u003e"
			],
			"formula": [
				"a(1) = 0 and given the prime factorization n = (p_1)^(e_1) * (p_2)^(e_2) * ... * (p_k)^(e_k) then a(n) = (e_1)*A000030(p_1) + (e_2)*A000030(p_2) + ... + (e_k)*A000030(p_l)."
			],
			"example": [
				"a(4) = 4 because 4 = 2*2, so the sum of the initial digits is 2 + 2 = 4.",
				"a(11) = 1 because 11 is prime and its initial digit is 1.",
				"a(22) = 3 because 22 = 2*11, so the sum of the initial digits is 2 + 1 = 3.",
				"a(98) = 16 because 98 = 2 * 7^2, so the sum of the initial digits is 2 + 7 + 7 = 16.",
				"a(100) = 14 because 100 = 2^2 * 5^2, so the sum of the initial digits is 2 + 2 + 5 + 5 = 14.",
				"a(121) = 2 because 121 = 11^2, so the sum of the initial digits is 1 + 1 = 2.",
				"a(361) = 2 because 361 = 19^2, so the sum of the initial digits is 1 + 1 = 2."
			],
			"mathematica": [
				"f[1] = 0; f[n_] := Plus @@ (#[[2]] First@IntegerDigits[#[[1]]] \u0026 /@ FactorInteger[n]); Array[f, 94] (* _Giovanni Resta_, Jun 17 2016 *)"
			],
			"xref": [
				"Cf. A000030, A000040, A001221, A001222, A112266 to A112273."
			],
			"keyword": "base,easy,nonn",
			"offset": "1,2",
			"author": "_Jonathan Vos Post_, Aug 30 2005",
			"ext": [
				"a(6) and a(35) corrected by _Giovanni Resta_, Jun 17 2016"
			],
			"references": 3,
			"revision": 15,
			"time": "2017-05-11T04:03:00-04:00",
			"created": "2005-09-21T03:00:00-04:00"
		}
	]
}