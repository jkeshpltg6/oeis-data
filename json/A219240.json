{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A219240",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 219240,
			"data": "1,0,0,0,1,-1,0,3,0,-3,0,1,0,0,0,-8,0,12,0,-6,0,1,1,0,-9,0,30,0,-45,0,30,0,-9,0,1,0,0,0,27,0,-108,0,171,0,-136,0,57,0,-12,0,1,-1,0,18,0,-123,0,399,0,-651,0,588,0,-308,0,93,0,-15,0,1,0,0,0,-64,0,480,0,-1488,0,2488,0,-2472,0,1524,0,-588,0,138,0,-18,0,1",
			"name": "Coefficient array for the cube of Chebyshev's S polynomials.",
			"comment": [
				"The row lengths sequence is 3*n+1 = A016777(n).",
				"For the coefficient triangle for Chebyshev's S polynomials see A049310.",
				"The o.g.f. for S(n,x)^3, n \u003e= 0, is GS(3;x,z) = (1+z^2+2*z*x)/ ((1+z^2-z*x)*(1+z^2-z*x*(x^2-3))). This is obtained from the de Moivre-Binet formula for S(n,x) and the binomial theorem.",
				"In general the monic integer Chebyshev polynomial tau(n,x):= R(2*n+1,x)/x enters, where R(n,x) = 2*T(n,x/2) with Chebyshev's T polynomial (for R see A127672), and the coefficient triangle for tau is given in A111125 (here for the third power of S only tau(0,x) = 1 and tau(1,x) = x^2 - 3 enter)."
			],
			"formula": [
				"a(n,m) = [x^m] S(n, x)^3, n \u003e= 0, 0 \u003c= m \u003c= 3*n, with Chebyshev's S polynomials (see A049310).",
				"a(n,m) = [x^m]([z^n] GS(3;x,z)), with the o.g.f. GS(3;x,z) given above in a comment.",
				"The row polynomials p(n, x) := Sum_{m=0..3*n} a(n,m)*x^m = S(n, x)^3 are (S(3*n+2, x) - 3*S(n, x))/(x^2 - 4). For the factorization of S polynomials see comments on A049310. - _Wolfdieter Lang_, Apr 09 2018"
			],
			"example": [
				"The array a(n,m) begins:",
				"n\\m   0  1  2  3  4    5   6    7  8    9 10  11 12  13 14 15",
				"n=0:  1",
				"n=1:  0  0  0  1",
				"n=2: -1  0  3  0 -3    0   1",
				"n=3:  0  0  0 -8  0   12   0  -6   0    1",
				"n=4:  1  0 -9  0 30    0 -45   0  30    0 -9   0  1",
				"n=5:  0  0  0 27  0 -108   0  171  0 -136  0  57  0 -12  0  1",
				"...",
				"Row n=6: [-1, 0, 18, 0, -123, 0, 399, 0, -651, 0, 588, 0, -308, 0, 93, 0, -15, 0, 1],",
				"Row n=7: [0, 0, 0, -64, 0, 480, 0, -1488, 0, 2488, 0, -2472, 0, 1524, 0, -588, 0, 138, 0, -18, 0, 1],",
				"Row n=8: [1, 0, -30, 0, 345, 0, -1921, 0, 5598, 0, -9540, 0, 10212, 0, -7137, 0, 3303, 0, -1003, 0, 192, 0, -21, 0, 1].",
				"n=2: S(2,x)^3 = (x^2 - 1)^3 = -1 + 3*x^2 - 3*x^4 + x^6.",
				"n=3: S(3,x)^3 = (x^3 - 2*x)^3 = -8*x^3 + 12*x^5 - 6*x^7 + x^9."
			],
			"xref": [
				"Cf. A049310, A127672, A158454 (square of S polynomials), A219234 (fourth power of S polynomials)."
			],
			"keyword": "sign,tabf,easy",
			"offset": "0,8",
			"author": "_Wolfdieter Lang_, Dec 12 2012",
			"references": 3,
			"revision": 13,
			"time": "2018-04-10T15:56:34-04:00",
			"created": "2012-12-13T11:12:09-05:00"
		}
	]
}