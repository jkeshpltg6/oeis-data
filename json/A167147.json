{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A167147",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 167147,
			"data": "4,26,444,11998,382716,15844060,766387489,42716991304,2704983373484,193042233338665,15032959574223321,1289808484211222447,120472472277271660102,12036408131864572935262,1297010265532587186011353,151499235341042432049982767,18434494194245279115211501310,2430305919107872967957571237320,334200348422242729412526022526012",
			"name": "a(n) = Number of subsets with n members of the set {1..n^2} such that the sum of the members is prime.",
			"comment": [
				"The sequence was conceived by _Zak Seidov_.",
				"Further terms were calculated by _Alois P. Heinz_."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A167147/b167147.txt\"\u003eTable of n, a(n) for n = 2..29\u003c/a\u003e"
			],
			"example": [
				"When n=2, a(2) = 4 because there are 4 subsets of the set {1,2,3,4} with prime sums: {1,2}=\u003e3, {1,4}=\u003e5, {2,3}=\u003e5, {3,4}=\u003e7.",
				"When n=3, a(3) = 26 because there are 26 subsets of the set {1,2,3,4,5,6,7,8,9} with prime sums: {1,2,4}=\u003e7, {1,2,8}=\u003e11, {1,3,7}=\u003e11, {1,3,9}=\u003e13, {1,4,6}=\u003e11, {1,4,8}=\u003e13, {1,5,7}=\u003e13, {1,7,9}=\u003e17, {2,3,6}=\u003e11, {2,3,8}=\u003e13, {2,4,5}=\u003e11, {2,4,7}=\u003e13, {2,5,6}=\u003e13, {2,6,9}=\u003e17, {2,7,8}=\u003e17, {2,8,9}=\u003e19, {3,4,6}=\u003e13, {3,5,9}=\u003e17, {3,6,8}=\u003e17, {3,7,9}=\u003e19, {4,5,8}=\u003e17, {4,6,7}=\u003e17, {4,6,9}=\u003e19, {4,7,8}=\u003e19, {5,6,8}=\u003e19, {6,8,9}=\u003e23."
			],
			"maple": [
				"g:= proc(n, i, t) option remember;",
				"      if n\u003c0 or t\u003c0 then 0",
				"    elif n=0 then `if`(t=0, 1, 0)",
				"    elif i\u003c1 or i\u003ct or (i+(1-t)/2)*t\u003cn then 0",
				"    else g(n, i-1, t) + g(n-i, i-1, t-1)",
				"      fi",
				"    end;",
				"a:= proc(n) option remember;",
				"      add(`if`(isprime(k), g(k, min(k, n^2), n), 0), k=2..n^2*(n^2+1)/2)",
				"    end:",
				"seq(a(n), n=2..13);",
				"# Coded by _Alois P. Heinz_"
			],
			"mathematica": [
				"g[n_, i_, t_] := g[n, i, t] = Which[n\u003c0 || t\u003c0, 0, n == 0, If[t == 0, 1, 0], i\u003c1 || i\u003ct || (i+(1-t)/2)*t \u003c n, 0, True, g[n, i-1, t] + g[n-i, i-1, t-1]]; a[n_] := a[n] = Sum[If[PrimeQ[k], g[k, Min[k, n^2], n], 0], {k, 2, n^2*(n^2 + 1)/2}]; Table[Print[a[n]]; a[n], {n, 2, 13}] (* _Jean-François Alcover_, Oct 24 2016, after _Alois P. Heinz_ *)"
			],
			"keyword": "nonn",
			"offset": "2,1",
			"author": "_Christopher Hunt Gribble_, Oct 28 2009",
			"references": 4,
			"revision": 23,
			"time": "2016-10-24T17:11:32-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}