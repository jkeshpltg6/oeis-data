{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A348853",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 348853,
			"data": "1,1,1,4,1,6,4,1,9,6,4,12,1,14,9,6,17,4,19,12,1,22,14,9,25,6,27,17,4,30,19,12,33,1,35,22,14,38,9,40,25,6,43,27,17,46,4,48,30,19,51,12,53,33,1,56,35,22,59,14,61,38,9,64,40,25,67,6,69,43,27,72",
			"name": "Delete any least significant 0's from the Zeckendorf representation of n, leaving its \"odd\" part.",
			"comment": [
				"Terms are odd Zeckendorfs A003622 and the fixed points are where n is odd already so that a(n) = n iff n is in A003622.",
				"A139764(n) is the least significant \"10..00\" part of n so Zeckendorf multiplication n = A101646(a(n), A139764(n)).",
				"The equivalent delete least significant 0's in binary is A000265 so that conversion to Fibbinary (A003714) and back gives a(n) = A022290(A000265(A003714(n))).",
				"a(n) = 1 iff n is a Fibonacci number \u003e= 1 (A000045) since they are Zeckendorf 100..00.",
				"a(n) = 4 iff n is a Lucas number \u003e= 4 (A000032) since they are Zeckendorf 10100..00 which reduces to 101.",
				"In the Wythoff array A035513, a(n) is the term in column 0 of the row containing n, and hence the formula below using row number A019586 to select which of the odds (column 0) is a(n)."
			],
			"link": [
				"Kevin Ryde, \u003ca href=\"/A348853/b348853.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Z#Zeckendorf\"\u003eIndex entries for sequences related to Zeckendorf expansion of n\u003c/a\u003e"
			],
			"formula": [
				"a(n) = n if A003849(n)=1, otherwise a(n) = a(A005206(n)) = a(A319433(n).",
				"a(n) = A003622(A019586(n) + 1)."
			],
			"example": [
				"n    = 81 = Zeckendorf 101001000",
				"a(n) = 19 = Zeckendorf 101001"
			],
			"program": [
				"(PARI) my(phi=quadgen(5)); a(n) = my(q,r); while([q,r]=divrem(n+2,phi); r\u003c1, n=q-1); n;"
			],
			"xref": [
				"Cf. A189920 (Zeckendorf digits), A003622 (odds), A003849 (final digit), A005206, A319433 (shift down).",
				"Cf. A000045 (Fibonacci), A000032 (Lucas).",
				"Cf. A035513 (Wythoff array), A019586 (row number).",
				"Cf. A003714 (Fibbinary), A022290 (its inverse).",
				"In other bases: A000265 (binary), A004151 (decimal)."
			],
			"keyword": "base,easy,nonn",
			"offset": "1,4",
			"author": "_Kevin Ryde_, Nov 14 2021",
			"references": 2,
			"revision": 35,
			"time": "2021-11-20T20:40:24-05:00",
			"created": "2021-11-19T07:55:28-05:00"
		}
	]
}