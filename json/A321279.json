{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A321279",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 321279,
			"data": "0,1,2,1,2,1,3,1,1,2,2,2,4,2,2,1,2,3,4,4,2,4,3,4,4,3,4,6,4,6,2,1,4,6,4,9,6,5,3,9,2,8,4,9,8,7,4,8,4,12,6,12,5,16,8,17,5,7,2,19,6,10,10,1,6,13,2,16,7,16,6,27,4,7,16,20,8,15,4,22",
			"name": "Number of z-trees with product A181821(n). Number of connected antichains of multisets with multiset density -1, of a multiset whose multiplicities are the prime indices of n.",
			"comment": [
				"This multiset (row n of A305936) is generally not the same as the multiset of prime indices of n. For example, the prime indices of 12 are {1,1,2}, while a multiset whose multiplicities are {1,1,2} is {1,1,2,3}.",
				"The multiset density of a multiset partition is the sum of the numbers of distinct vertices in each part minus the number of parts minus the number of vertices."
			],
			"example": [
				"The sequence of antichains begins:",
				"   2: {{1}}",
				"   3: {{1,1}}",
				"   3: {{1},{1}}",
				"   4: {{1,2}}",
				"   5: {{1,1,1}}",
				"   5: {{1},{1},{1}}",
				"   6: {{1,1,2}}",
				"   7: {{1,1,1,1}}",
				"   7: {{1,1},{1,1}}",
				"   7: {{1},{1},{1},{1}}",
				"   8: {{1,2,3}}",
				"   9: {{1,1,2,2}}",
				"  10: {{1,1,1,2}}",
				"  10: {{1,1},{1,2}}",
				"  11: {{1,1,1,1,1}}",
				"  11: {{1},{1},{1},{1},{1}}",
				"  12: {{1,1,2,3}}",
				"  12: {{1,2},{1,3}}",
				"  13: {{1,1,1,1,1,1}}",
				"  13: {{1,1,1},{1,1,1}}",
				"  13: {{1,1},{1,1},{1,1}}",
				"  13: {{1},{1},{1},{1},{1},{1}}",
				"  14: {{1,1,1,1,2}}",
				"  14: {{1,2},{1,1,1}}",
				"  15: {{1,1,1,2,2}}",
				"  15: {{1,1},{1,2,2}}",
				"  16: {{1,2,3,4}}"
			],
			"mathematica": [
				"facs[n_]:=If[n\u003c=1,{{}},Join@@Table[Map[Prepend[#,d]\u0026,Select[facs[n/d],Min@@#\u003e=d\u0026]],{d,Rest[Divisors[n]]}]];",
				"nrmptn[n_]:=Join@@MapIndexed[Table[#2[[1]],{#1}]\u0026,If[n==1,{},Flatten[Cases[FactorInteger[n]//Reverse,{p_,k_}:\u003eTable[PrimePi[p],{k}]]]]];",
				"zsm[s_]:=With[{c=Select[Tuples[Range[Length[s]],2],And[Less@@#,GCD@@s[[#]]]\u003e1\u0026]},If[c=={},s,zsm[Union[Append[Delete[s,List/@c[[1]]],LCM@@s[[c[[1]]]]]]]]];",
				"zensity[s_]:=Total[(PrimeNu[#]-1\u0026)/@s]-PrimeNu[LCM@@s];",
				"Table[Length[Select[facs[Times@@Prime/@nrmptn[n]],And[zensity[#]==-1,Length[zsm[#]]==1,Select[Tuples[#,2],UnsameQ@@#\u0026\u0026Divisible@@#\u0026]=={}]\u0026]],{n,50}]"
			],
			"xref": [
				"Cf. A001055, A007718, A030019, A181821, A293607, A303837, A304382, A305081, A305936, A318284, A321229, A321270, A321271, A321272."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Gus Wiseman_, Nov 01 2018",
			"references": 1,
			"revision": 4,
			"time": "2018-11-02T11:23:18-04:00",
			"created": "2018-11-02T11:23:18-04:00"
		}
	]
}