{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A093467",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 93467,
			"data": "1,2,3,6,14,35,90,234,611,1598,4182,10947,28658,75026,196419,514230,1346270,3524579,9227466,24157818,63245987,165580142,433494438,1134903171,2971215074,7778742050,20365011075,53316291174,139583862446",
			"name": "a(1) = 1, a(2) = 2; for n \u003e= 2, a(n+1) = a(n) + Sum_{i = 1..n} (a(i) - a(1)).",
			"comment": [
				"If the \"man or boy\" program A(k, x1, x2, x3) from the program section is run with k \u003e 0 and arbitrary x1, x2, and x3, the result is A055588(k-1)*x1 + A001519(k-1)*x2. - _Eric M. Schmidt_, Jun 24 2021"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A093467/b093467.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"K. Kuhapatanakul, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL16/Kuhapatanakul/kuha4.html\"\u003eOn the Sums of Reciprocal Generalized Fibonacci Numbers\u003c/a\u003e, J. Int. Seq. 16 (2013) #13.7.1. See Theorem 3 p.3.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-4,1)."
			],
			"formula": [
				"a(n) = 3*a(n-1) - a(n-2) - 1, n \u003e 3. - _Robert G. Wilson v_, Apr 08 2004",
				"G.f.: x - x^2*(2*x-1)*(x-2) / ( (x-1)*(x^2-3*x+1) ). - _R. J. Mathar_, Sep 06 2014",
				"a(n) = A055588(n-2) + A001519(n-2), n \u003e 1. - _Eric M. Schmidt_, Jun 24 2021"
			],
			"mathematica": [
				"a[1] = 1; a[2] = 2; a[n_] := a[n] = a[n - 1] + Sum[a[i] - a[1], {i, n - 1}]; Table[ a[n], {n, 30}]",
				"Join[{1}, LinearRecurrence[{4, -4, 1}, {2, 3, 6}, 30]] (* _Vincenzo Librandi_, Feb 08 2017 *)"
			],
			"program": [
				"(PARI) a(n)=if(n==1,1,if(n==2,2,a(n-1)+sum(i=1,n-1,a(i)-a(1)))) \\\\ _Edward Jiang_, Sep 06 2014",
				"(ALGOL-60) begin integer procedure A(k, x1, x2, x3);",
				"    value k; integer k;",
				"    integer x1, x2, x3;",
				"    begin integer procedure b;",
				"        begin",
				"            k:= k - 1;",
				"            B:= A := A (k, B, x1, x2);",
				"        end;",
				"        A := if k \u003c= 0 then x2 + x3 else B;",
				"    end;",
				"    integer i;",
				"    for i:= 0 step 1 until 20 do",
				"        print (A (i, 1, 1, 0));",
				"end",
				"comment The above is a simplified Man or Boy Test program (cf. A132343), omitting the negative parameters from the original. - _Leonid Broukhis_, Feb 07 2017",
				"(MAGMA) I:=[2,3,6]; [1] cat [n le 3 select I[n] else  4*Self(n-1)-4*Self(n-2)+Self(n-3): n in [1..30]]; // _Vincenzo Librandi_, Feb 08 2017"
			],
			"xref": [
				"Cf. A093468.",
				"Essentially the same as A032908.",
				"Cf. A001519, A055588."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Amarnath Murthy_, Apr 07 2004",
			"ext": [
				"More terms from _Robert G. Wilson v_, Apr 08 2004"
			],
			"references": 3,
			"revision": 44,
			"time": "2021-07-16T21:22:52-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}