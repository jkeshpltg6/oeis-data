{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A050443",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 50443,
			"data": "4,0,0,3,4,0,3,7,4,3,10,11,7,13,21,18,20,34,39,38,54,73,77,92,127,150,169,219,277,319,388,496,596,707,884,1092,1303,1591,1976,2395,2894,3567,4371,5289,6461,7938,9660,11750,14399,17598,21410,26149,31997",
			"name": "a(0)=4, a(1)=0, a(2)=0, a(3)=3; thereafter a(n) = a(n-3) + a(n-4).",
			"comment": [
				"Related to Perrin sequence. a(p) is divisible by p for primes p.",
				"Wells states that Mihaly Bencze [Beneze] (1998) proved the divisibility property for this sequence: that a(n) is always divisible by n when n is prime. - _Gary W. Adamson_, Nov 14 2006"
			],
			"reference": [
				"David Wells, \"Prime Numbers, the Most Mysterious Figures in Math\", John Wiley \u0026 Sons, Inc.; 2005, p. 103."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A050443/b050443.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e (terms 0..1000 from T. D. Noe)",
				"Mihaly Bencze, Dan Saracino, and Allen Stenger, \u003ca href=\"https://www.jstor.org/stable/2589334\"\u003eSolution of Problem 10655: A Recurrence Generating Multiples of Primes\u003c/a\u003e, American Mathematical Monthly 107 (2000) 281-282.",
				"Gregory T. Minton, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9939-2014-12168-X\"\u003eLinear recurrence sequences satisfying congruence conditions\u003c/a\u003e, Proc. Amer. Math. Soc. 142 (2014), no. 7, 2337--2352. MR3195758.",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,0,1,1)."
			],
			"formula": [
				"G.f.: (4-x^3)/(1-x^3-x^4). - _Christian G. Bower_, Dec 23 1999",
				"a(n) = (x_1)^n + (x_2)^n + (x_3)^n + (x_4)^n where (x_i) 1 \u003c= i \u003c= 4 are the roots of x^4 = x + 1. - _Benoit Cloitre_, Oct 27 2003",
				"Let M = the 4 X 4 matrix [0,1,0,0; 0,0,1,0; 0,0,0,1; 1,1,0,0]; then a(n) = the leftmost term of M^n * [4,0,0,3]. Example: a(13) = 13 since M^13 * [4,0,0,3] = [13,21,18,20]. - _Gary W. Adamson_, Nov 14 2006",
				"a(0) = 4 and a(n) = n*Sum_{k=1..floor(n/3)} binomial(k,n-3*k)/k for n \u003e 0. - _Seiichi Manyama_, Mar 04 2019",
				"From _Aleksander Bosek_, Mar 10 2019: (Start)",
				"a(n+10) = a(n+5) + 2*a(n+3) + a(n).",
				"a(n+11) = a(n+6) + 3*a(n+1) + 2*a(n).",
				"a(n+12) = a(n+10) + 5*a(n+5) + a(n).",
				"a(n+12) = 3*a(n+5) + a(n+3) + a(n).",
				"a(n+13) = 3*a(n+6) + 2*a(n+1) + a(n).",
				"a(n+14) = 2*a(n+8) + 3*a(n+3) + a(n).",
				"a(n+15) = 2*a(n+7) + 4*a(n+5) + a(n).",
				"a(n+15) = 2*a(n+9) + 4*a(n+1) + 3*a(n).",
				"a(n+19) = a(n+17) + 5*a(n+5) + a(n).",
				"a(n+20) = 5*a(n+10) + 6*a(n+5) + a(n).",
				"a(n+22) = a*(n+21) + 5*a(n+5) + a(n).",
				"a(n+25) = 2*a(n+21) + 5*a(n+5) + a(n).",
				"a((s+4)*n+m) = Sum_{l=0..n} binomial(n-l,l)*a(s*n+l+m) for every m,s \u003e 0.",
				"a(m) = Sum_{l=0..n}(-1)^{n-l}*binomial(n-l,l)*a(m+n+3*l) for every m \u003e 0. (End)",
				"a(n) = 4*A017817(n)-A017817(n-3). - _R. J. Mathar_, Aug 10 2021"
			],
			"example": [
				"a(11) = 11 because a(7) = 7 and a(8) = 4."
			],
			"mathematica": [
				"LinearRecurrence[{0,0,1,1}, {4,0,0,3}, 60] (* _G. C. Greubel_, Mar 04 2019 *)"
			],
			"program": [
				"(PARI) polsym(x^4-x-1,55) \\\\ _Joerg Arndt_, Mar 04 2019",
				"(MAGMA) I:=[4,0,0,3]; [n le 4 select I[n] else Self(n-3) +Self(n-4): n in [1..60]]; // _G. C. Greubel_, Mar 04 2019",
				"(Sage) ((4-x^3)/(1-x^3-x^4)).series(x, 60).coefficients(x, sparse=False) # _G. C. Greubel_, Mar 04 2019",
				"(GAP) a:=[4,0,0,3];; for n in [5..60] do a[n]:=a[n-3]+a[n-4]; od; Print(a); # _Muniru A Asiru_, Mar 09 2019"
			],
			"xref": [
				"Column 3 of A306646.",
				"Cf. A001608, A052338.",
				"Cf. A087935, A087936."
			],
			"keyword": "easy,nonn",
			"offset": "0,1",
			"author": "Tony Davie (ad(AT)dcs.st-and.ac.uk), Dec 23 1999",
			"ext": [
				"More terms from _Christian G. Bower_, Dec 23 1999",
				"More terms from _Benoit Cloitre_, Oct 27 2003"
			],
			"references": 6,
			"revision": 57,
			"time": "2021-08-10T05:57:47-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}