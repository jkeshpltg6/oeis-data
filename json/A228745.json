{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A228745",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 228745,
			"data": "1,-6,24,-24,24,-36,96,-48,24,-78,144,-72,96,-84,192,-144,24,-108,312,-120,144,-192,288,-144,96,-186,336,-240,192,-180,576,-192,24,-288,432,-288,312,-228,480,-336,144,-252,768,-264,288,-468,576,-288,96,-342,744",
			"name": "Expansion of (phi(q)^4 + 7 * phi(-q)^4) / 8 in powers of q where phi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A228745/b228745.txt\"\u003eTable of n, a(n) for n = 0..2500\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"a(n) = -6 * b(n) where b() is multiplicative with a(0) = 1, b(2^e) = -4 if e\u003e1, b(p^e) = b(p) * b(p^(e-1)) - p * b(p^(e-2)), if p\u003e2.",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (4 t)) = 1/2 (t/i)^2 g(t) where q = exp(2 Pi i t) and g() is the g.f. for A228746.",
				"G.f.: ( (Sum_{k in Z} x^k^2)^4 + 7 * (Sum_{k in Z} (-x)^k^2)^4 ) / 8.",
				"a(2*n) = A004011(n). a(2*n + 1) = -6 * A008438(n).",
				"Convolution with A005875 is A003781."
			],
			"example": [
				"G.f. = 1 - 6*q + 24*q^2 - 24*q^3 + 24*q^4 - 36*q^5 + 96*q^6 - 48*q^7 + 24*q^8 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (EllipticTheta[ 3, 0, q]^4 + 7 EllipticTheta[ 4, 0, q]^4) / 8, {q, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = sum(k=1, sqrtint(n), 2 * x^k^2, 1 + x * O(x^n)); polcoeff( (A^4 + 7 * subst(A, x, -x)^4) / 8, n))};",
				"(MAGMA) A := Basis( ModularForms( Gamma0(4), 2), 51); A[1] - 6*A[2]; /* _Michael Somos_, Aug 21 2014 */"
			],
			"xref": [
				"Cf. A003781, A004011, A005875, A008438, A228746."
			],
			"keyword": "sign",
			"offset": "0,2",
			"author": "_Michael Somos_, Sep 02 2013",
			"references": 2,
			"revision": 14,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2013-09-02T22:38:02-04:00"
		}
	]
}