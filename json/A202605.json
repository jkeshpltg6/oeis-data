{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A202605",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 202605,
			"data": "1,-1,1,-3,1,1,-6,9,-1,1,-9,26,-24,1,1,-12,52,-96,64,-1,1,-15,87,-243,326,-168,1,1,-18,131,-492,1003,-1050,441,-1,1,-21,184,-870,2392,-3816,3265,-1155,1,1,-24,246,-1404,4871,-10500,13710",
			"name": "Array: row n shows the coefficients of the characteristic polynomial of the n-th principal submatrix of the Fibonacci self-fusion matrix (A202453).",
			"comment": [
				"Let p(n)=p(n,x) be the characteristic polynomial of the n-th principal submatrix. The zeros of p(n) are positive and interlace the zeros of p(n+1). (See the references and examples.)",
				"Following is a guide to sequences (f(n)) for symmetric matrices (self-fusion matrices) and characteristic polynomials. Notation: F(k)=A000045(k) (Fibonacci numbers); floor(n*tau)=A000201(n) (lower Wythoff sequence; \"periodic x,y\" represents the sequence (x,y,x,y,x,y,...).",
				"f(n)........ symmetric matrix.. char. polynomial",
				"1............... A087062....... A202672",
				"n............... A115262....... A202673",
				"n^2............. A202670....... A202671",
				"2n-1............ A202674....... A202675",
				"3n-2............ A202676....... A202677",
				"n(n+1)/2........ A185957....... A202678",
				"2^n-1........... A202873....... A202767",
				"2^(n-1)......... A115216....... A202868",
				"floor(n*tau).... A202869....... A202870",
				"F(n)............ A202453....... A202605",
				"F(n+1).......... A202874....... A202875",
				"Lucas(n)........ A202871....... A202872",
				"F(n+2)-1........ A202876....... A202877",
				"F(n+3)-2........ A202970....... A202971",
				"(F(n))^2........ A203001....... A203002",
				"(F(n+1))^2...... A203003....... A203004",
				"C(2n,n)......... A115255....... A203005",
				"(-1)^(n+1)...... A003983....... A076757",
				"periodic 1,0.... A203905....... A203906",
				"periodic 1,0,0.. A203945....... A203946",
				"periodic 1,0,1.. A203947....... A203948",
				"periodic 1,1,0.. A203949....... A203950",
				"periodic 1,0,0,0 A203951....... A203952",
				"periodic 1,2.... A203953....... A203954",
				"periodic 1,2,3.. A203955....... A203956",
				"...",
				"In the cases listed above, the zeros of the characteristic polynomials are positive. If more general symmetric matrices are used, the zeros are all real but not necessarily positive - but they do have the interlace property. For a guide to such matrices and polynomials, see A202605."
			],
			"link": [
				"S.-G. Hwang, \u003ca href=\"http://matrix.skku.ac.kr/Series-E/Monthly-E.pdf\"\u003eCauchy's interlace theorem for eigenvalues of Hermitian matrices\u003c/a\u003e, American Mathematical Monthly 111 (2004) 157-159.",
				"Clark Kimberling, \u003ca href=\"https://www.fq.math.ca/Papers1/52-3/Kimberling11132013.pdf\"\u003eFusion, Fission, and Factors\u003c/a\u003e, Fib. Q., 52 (2014), 195-202.",
				"A. Mercer and P. Mercer, \u003ca href=\"http://dx.doi.org/10.1155/S016117120000257X\"\u003eCauchy's interlace theorem and lower bounds for the spectral radius\u003c/a\u003e, International Journal of Mathematics and Mathematical Sciences 23, no. 8 (2000) 563-566."
			],
			"example": [
				"The 1st principal submatrix (ps) of A202453 is {{1}} (using Mathematica matrix notation), with p(1) = 1-x and zero-set {1}.",
				"...",
				"The 2nd ps is {{1,1},{1,2}}, with p(2) = 1-3x+x^2 and zero-set {0.382..., 2.618...}.",
				"...",
				"The 3rd ps is {{1,1,2},{1,2,3},{2,3,6}}, with p(3) = 1-6x+9x^2-x^3 and zero-set {0.283..., 0.426..., 8.290...}.",
				"  ...",
				"Top of the array A202605:",
				"1,   -1;",
				"1,   -3,    1;",
				"1,   -6,    9,   -1;",
				"1,   -9,   26,  -24,    1;",
				"1,  -12,   52,  -96,   64,   -1;",
				"1,  -15,   87, -243,  326, -168,    1;"
			],
			"mathematica": [
				"f[k_] := Fibonacci[k];",
				"U[n_] := NestList[Most[Prepend[#, 0]] \u0026, #, Length[#] - 1] \u0026[Table[f[k], {k, 1, n}]];",
				"L[n_] := Transpose[U[n]];",
				"F[n_] := CharacteristicPolynomial[L[n].U[n], x];",
				"c[n_] := CoefficientList[F[n], x]",
				"TableForm[Flatten[Table[F[n], {n, 1, 10}]]]",
				"Table[c[n], {n, 1, 12}]",
				"Flatten[%]",
				"TableForm[Table[c[n], {n, 1, 10}]]"
			],
			"xref": [
				"Cf. A000045, A202453."
			],
			"keyword": "tabl,sign",
			"offset": "1,4",
			"author": "_Clark Kimberling_, Dec 21 2011",
			"references": 78,
			"revision": 29,
			"time": "2020-02-18T18:35:00-05:00",
			"created": "2011-12-21T18:15:34-05:00"
		}
	]
}