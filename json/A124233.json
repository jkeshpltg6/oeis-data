{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A124233",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 124233,
			"data": "1,1,1,2,1,1,2,2,1,3,1,0,2,0,2,2,1,0,3,0,1,4,0,2,2,1,0,4,2,2,2,0,1,0,0,2,3,0,0,0,1,2,4,2,0,3,2,2,2,3,1,0,0,0,4,0,2,0,2,0,2,2,0,6,1,0,0,2,0,4,2,0,3,0,0,2,0,0,0,0,1,5,2,2,4,0,2",
			"name": "Expansion of psi(q) * phi(-q^10) * chi(-q^5) / chi(-q^2) in powers of q where phi(), psi(), chi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Number 38 of the 74 eta-quotients listed in Table I of Martin (1996)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A124233/b124233.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Y. Martin, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-96-01743-6\"\u003eMultiplicative eta-quotients\u003c/a\u003e, Trans. Amer. Math. Soc. 348 (1996), no. 12, 4825-4856, see page 4852 Table I.",
				"Michael Somos, \u003ca href=\"/A030203/a030203.txt\"\u003eIndex to Yves Martin's list of 74 multiplicative eta-quotients and their A-numbers\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of eta(q^2) * eta(q^4) * eta(q^5) * eta(q^10) / (eta(q) * eta(q^20)) in powers of q.",
				"Euler transform of period 20 sequence [ 1, 0, 1, -1, 0, 0, 1, -1, 1, -2, 1, -1, 1, 0, 0, -1, 1, 0, 1, -2, ...].",
				"Moebius transform is period 20 sequence [ 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, -1, 0, -1, 0, 0, 0, -1, 0, -1, 0, ...].",
				"a(n) is multiplicative with a(2^e) = a(5^e) = 1, a(p^e) = e+1 if p == 1, 3, 7, 9 (mod 20), a(p^e) = (1 + (-1)^e) / 2 if p == 11, 13, 17, 19 (mod 20).",
				"G.f.: 1 + Sum_{k\u003e0} x^k * (1 + x^(2*k)) * (1 + x^(6*k)) / (1 + x^(10*k)).",
				"a(2*n) = a(5*n) = a(n), a(20*n + 11) = a(20*n + 13) = a(20*n + 17) = a(20*n + 19) = 0.",
				"a(n) = A035170(n) unless n=0. a(2*n + 1) = A129390(n). a(4*n + 3) = 2 * A033764(n)."
			],
			"example": [
				"G.f. = 1 + q + q^2 + 2*q^3 + q^4 + q^5 + 2*q^6 + 2*q^7 + q^8 + 3*q^9 + q^10 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 1, Boole[n == 0], DivisorSum[ n, KroneckerSymbol[ -20, #] \u0026]]; (* _Michael Somos_, Jul 09 2015 *)",
				"a[ n_] := SeriesCoefficient[ QPochhammer[ q^2] QPochhammer[ q^4] QPochhammer[ q^5] QPochhammer[ q^10] / (QPochhammer[ q] QPochhammer[ q^20]), {q, 0, n}]; (* _Michael Somos_, Jul 09 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n==0, sumdiv( n, d, kronecker( -20, d)))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A) * eta(x^4 + A) * eta(x^5 + A) * eta(x^10 + A) / eta(x + A) / eta(x^20 + A), n))};"
			],
			"xref": [
				"Cf. A033764, A035170, A129390."
			],
			"keyword": "nonn,mult",
			"offset": "0,4",
			"author": "_Michael Somos_, Oct 21 2006",
			"references": 2,
			"revision": 23,
			"time": "2021-03-12T22:24:44-05:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}