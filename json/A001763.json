{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A001763",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 1763,
			"id": "M4279 N1788",
			"data": "1,1,6,72,1320,32760,1028160,39070080,1744364160,89513424000,5191778592000,335885501952000,23982224839372800,1873278229119897600,158905670470170624000,14547557832075620352000,1429628183315795054592000,150110959248158480732160000",
			"name": "Number of dissections of a ball: (3n+3)!/(2n+3)!.",
			"comment": [
				"With offset 1, a(n) = number of labeled plane trees (A006963) on n vertices in which vertices of degree d come in d colors or, equivalently, each vertex has a favorite neighbor (n\u003e=2). For example, there are 2 unlabeled plane trees with 4 vertices: the path and the star. There are 4!/2 ways to label the path and 4!/3 ways to label the star. There are 4 choices for coloring vertices in the path and 3 choices for coloring vertices in the star. The count for 4 vertices is thus 12*4 + 8*3 = 72. [_David Callan_, Aug 22 2014]"
			],
			"reference": [
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A001763/b001763.txt\"\u003eTable of n, a(n) for n = -1..100\u003c/a\u003e",
				"L. W. Beineke and R. E. Pippert, \u003ca href=\"http://dx.doi.org/ 10.1007/BF02330563\"\u003eEnumerating labeled k-dimensional trees and ball dissections\u003c/a\u003e, pp. 12-26 of Proceedings of Second Chapel Hill Conference on Combinatorial Mathematics and Its Applications, University of North Carolina, Chapel Hill, 1970. Reprinted in Math. Annalen, 191 (1971), 87-98.",
				"INRIA Algorithms Project, \u003ca href=\"http://ecs.inria.fr/services/structure?nbr=407\"\u003eEncyclopedia of Combinatorial Structures 407\u003c/a\u003e"
			],
			"formula": [
				"E.g.f.: A(x)=(2/sqrt(3*x))*sin(arcsin(3*sqrt(3*x)/2)/3)=1+6*x/(Q(0)-6*x); Q(k)=3*x*(3*k+1)*(3*k+2)+2*(2*(k^2)+5*k+3)-6*x*(2*(k^2)+5*k+3)*(3*k+4)*(3*k+5)/Q(k+1) ; (continued fraction). - _Sergei N. Gladkovskii_, Nov 27 2011",
				"E.g.f. (starting at n=0 term): -(1/3)*(3*cos((2/3)*arcsin((3/2)*3^(1/2)*x^(1/2)))*x^(1/2)*(-27*x+4)^(1/2)+9*sin((2/3)*arcsin((3/2)*3^(1/2)*x^(1/2)))*3^(1/2)*x-2*sin((2/3)*arcsin((3/2)*3^(1/2)*x^(1/2)))*3^(1/2))/(x^(3/2)*(-27*x+4)^(1/2)). - _Robert Israel_, Aug 22 2014"
			],
			"maple": [
				"A001763:=n-\u003e(3*n+3)!/(2*n+3)!: seq(A001763(n), n=-1..20); # _Wesley Ivan Hurt_, Aug 23 2014"
			],
			"mathematica": [
				"Table[(3*n + 3)!/(2*n + 3)!, {n, -1, 20}] (* _T. D. Noe_, Aug 10 2012 *)"
			],
			"xref": [
				"Cf. A001762."
			],
			"keyword": "nonn",
			"offset": "-1,3",
			"author": "_N. J. A. Sloane_.",
			"references": 7,
			"revision": 35,
			"time": "2017-04-18T07:02:41-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}