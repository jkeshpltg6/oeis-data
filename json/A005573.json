{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005573",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5573,
			"id": "M3943",
			"data": "1,5,26,139,758,4194,23460,132339,751526,4290838,24607628,141648830,817952188,4736107172,27487711752,159864676803,931448227590,5435879858958,31769632683132,185918669183370,1089302293140564",
			"name": "Number of walks on cubic lattice (starting from origin and not going below xy plane).",
			"comment": [
				"Binomial transform of A026378, second binomial transform of A001700. - _Philippe Deléham_, Jan 28 2007",
				"The Hankel transform of [1,1,5,26,139,758,...] is [1,4,15,56,209,...](see A001353). - _Philippe Deléham_, Apr 13 2007"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A005573/b005573.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Isaac DeJager, Madeleine Naquin, Frank Seidl, \u003ca href=\"https://www.valpo.edu/mathematics-statistics/files/2019/08/Drube2019.pdf\"\u003eColored Motzkin Paths of Higher Order\u003c/a\u003e, VERUM 2019.",
				"E. Deutsch et al., \u003ca href=\"http://www.jstor.org/stable/2695431\"\u003eProblem 10795: Three-Dimensional Lattice Walks in the Upper Half-Space\u003c/a\u003e, Amer. Math. Monthly, 108 (Dec. 2001), 980.",
				"Rigoberto Flórez, Leandro Junes, José L. Ramírez, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL21/Florez/florez4.html\"\u003eFurther Results on Paths in an n-Dimensional Cubic Lattice\u003c/a\u003e, Journal of Integer Sequences, Vol. 21 (2018), Article 18.1.2.",
				"R. K. Guy, \u003ca href=\"/A005555/a005555.pdf\"\u003eLetter to N. J. A. Sloane, May 1990\u003c/a\u003e",
				"R. K. Guy, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL3/GUY/catwalks.html\"\u003eCatwalks, Sandsteps and Pascal Pyramids\u003c/a\u003e, J. Integer Seqs., Vol. 3 (2000), #00.1.6.",
				"Aoife Hennessy, \u003ca href=\"http://repository.wit.ie/1693\"\u003eA Study of Riordan Arrays with Applications to Continued Fractions, Orthogonal Polynomials and Lattice Paths\u003c/a\u003e, Ph. D. Thesis, Waterford Institute of Technology, Oct. 2011."
			],
			"formula": [
				"From _Emeric Deutsch_, Jan 09 2003; corrected by _Roland Bacher_: (Start)",
				"a(n) = Sum_{i=0..n} (-1)^i*6^(n-i)*binomial(n, i)*binomial(2*i, i)/(i+1);",
				"g.f. A(x) satisfies: x(1-6x)A^2 + (1-6x)A - 1 = 0. (End)",
				"From _Henry Bottomley_, Aug 23 2001: (Start)",
				"a(n) = 6*a(n-1) - A005572(n-1).",
				"a(n) = Sum_{j=0..n} 4^(n-j)*binomial(n, floor(n/2))*binomial(n, j). (End)",
				"a(n) = Sum_{k=0..n} binomial(n, k)*binomial(2*k+1, k)*2^(n-k).",
				"a(n) = Sum_{k=0..n} (-1)^k*binomial(n, k)*Catalan(k)*6^(n-k).",
				"D-finite with recurrence (n+1)*a(n) = (8*n+2)*a(n-1)-(12*n-12)*a(n-2). - _Vladeta Jovovic_, Jul 16 2004",
				"a(n) = Sum_{k=0..n} A052179(n,k). - _Philippe Deléham_, Jan 28 2007",
				"Conjecture: a(n)= 6^n * hypergeom([1/2,-n],[2], 2/3). - _Benjamin Phillabaum_, Feb 20 2011",
				"From _Paul Barry_, Apr 21 2009: (Start)",
				"G.f.: (sqrt((1-2*x)/(1-6*x)) - 1)/(2*x).",
				"G.f.: 1/(1-5*x-x^2/(1-4*x-x^2/(1-4*x-x^2/(1-4*x-x^2/(1-... (continued fraction). (End)",
				"G.f.: 1/(1 - 4*x - x*(1 - 2*x)/(1 - 2*x - x*(1 - 2*x)/(1 - 2*x - x*(1 - 2*x)/(1 - 2*x - x*(1 - 2*x)/(1...(continued fraction). - Aoife Hennessy (aoife.hennessy(AT)gmail.com), Jul 02 2010",
				"a(n) ~ 6^(n+1/2)/sqrt(Pi*n). - _Vaclav Kotesovec_, Oct 05 2012",
				"G.f.: G(0)/(2*x) - 1/(2*x), where G(k)= 1 + 4*x*(4*k+1)/( (4*k+2)*(1-2*x) - 2*x*(1-2*x)*(2*k+1)*(4*k+3)/(x*(4*k+3) + (1-2*x)*(k+1)/G(k+1))); (continued fraction). - _Sergei N. Gladkovskii_, Jun 24 2013",
				"a(n) = 2^n*hypergeom([-n, 3/2], [2], -2). - _Peter Luschny_, Apr 26 2016",
				"E.g.f.: exp(4*x)*(BesselI(0,2*x) + BesselI(1,2*x)). - _Ilya Gutkovskiy_, Sep 20 2017"
			],
			"mathematica": [
				"CoefficientList[Series[(Sqrt[(1-2x)/(1-6x)]-1)/(2x),{x,0,20}],x] (* _Harvey P. Dale_, Jun 24 2011 *)",
				"a[n_] := 6^n Hypergeometric2F1[1/2, -n, 2, 2/3]; Table[a[n], {n, 0, 20}] (* _Jean-François Alcover_, Apr 11 2017 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^30)); Vec((sqrt((1-2*x)/(1-6*x)) -1)/(2*x)) \\\\ _G. C. Greubel_, May 02 2019",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Rationals(), 30); Coefficients(R!( (Sqrt((1-2*x)/(1-6*x)) -1)/(2*x) )); // _G. C. Greubel_, May 02 2019",
				"(Sage) ((sqrt((1-2*x)/(1-6*x)) -1)/(2*x)).series(x, 30).coefficients(x, sparse=False) # _G. C. Greubel_, May 02 2019"
			],
			"keyword": "nonn,walk,easy,nice",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _Henry Bottomley_, Aug 23 2001"
			],
			"references": 14,
			"revision": 91,
			"time": "2020-02-20T03:26:19-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}