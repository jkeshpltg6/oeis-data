{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A080846",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 80846,
			"data": "0,1,0,0,1,1,0,1,0,0,1,0,0,1,1,0,1,1,0,1,0,0,1,1,0,1,0,0,1,0,0,1,1,0,1,0,0,1,0,0,1,1,0,1,1,0,1,0,0,1,1,0,1,1,0,1,0,0,1,1,0,1,0,0,1,0,0,1,1,0,1,1,0,1,0,0,1,1,0,1,0,0,1,0,0,1,1,0,1,0,0,1,0,0,1,1,0,1,1,0,1,0,0,1,1",
			"name": "Fixed point of the morphism 0-\u003e010, 1-\u003e011, starting from a(1) = 0.",
			"comment": [
				"A cubefree word.",
				"A generalized choral sequence c(3n+r_0)=0, c(3n+r_1)=1, c(3n+r_c)=c(n), with r_0=0, r_1=1, and r_c=2. - Joel Reyes Noche (joel.noche(AT)up.edu.ph), Jul 09 2009",
				"From _Joerg Arndt_, Apr 15 2010: (Start)",
				"Turns (by 120 degrees) of the terdragon curve which can be rendered as follows:",
				"  [Init] Set n=0 and direction=0.",
				"  [Draw] Draw a unit line (in the current direction). Turn left/right if a(n) is zero/nonzero respectively.",
				"  [Next] Set n=n+1 and goto (draw).",
				"See fxtbook link below.",
				"(End)"
			],
			"reference": [
				"J. Berstel and J. Karhumaki, Combinatorics on words - a tutorial, Bull. EATCS, #79 (2003), pp. 178-228.",
				"J. R. Noche, Generalized Choral Sequences, Matimyas Matematika, 31(2008), 25-28. [From Joel Reyes Noche (joel.noche(AT)up.edu.ph), Jul 09 2009]"
			],
			"link": [
				"Joerg Arndt \u003ca href=\"http://www.jjj.de/fxt/#fxtbook\"\u003eMatters Computational (The Fxtbook)\u003c/a\u003e, section 1.31.4, pp. 92-95; dragon curve picture on p. 93.",
				"Jean Berstel, \u003ca href=\"http://www-igm.univ-mlv.fr/~berstel/\"\u003eHome Page\u003c/a\u003e",
				"Dimitri Hendriks, Frits G. W. Dannenberg, Jorg Endrullis, Mark Dow and Jan Willem Klop, \u003ca href=\"http://arxiv.org/abs/1201.3786\"\u003eArithmetic Self-Similarity of Infinite Sequences\u003c/a\u003e, arXiv preprint 1201.3786 [math.CO], 2012.",
				"\u003ca href=\"/index/Fi#FIXEDPOINTS\"\u003eIndex entries for sequences that are fixed points of mappings\u003c/a\u003e"
			],
			"formula": [
				"a(n) = (A062756(n) - A062756(n+1) + 1)/2, where A062756(n) is the number of 1's in the ternary expansion of n. From formula in A062756: g.f.: A(x) = 1/(1-x)/2 - Sum_{k\u003e=0} x^(3^k-1)/(1+x^(3^k)+x^(2*3^k))/2. - _Paul D. Hanna_, Feb 24 2006",
				"Given g.f. A(x) then B(x) = x * A(x) satisfies B(x) = x^2 / (1 - x^3) + B(x^3). - _Michael Somos_, Jul 29 2009",
				"a(3*n) = 0, a(3*n + 1) = 1, a(3*n - 1) = a(n - 1). - _Michael Somos_, Jul 29 2009",
				"a(n) = -1 + A060236(n). - _Joerg Arndt_, Jan 21 2013"
			],
			"example": [
				"Start: 0",
				"Rules:",
				"  0 --\u003e 010",
				"  1 --\u003e 011",
				"-------------",
				"0:   (#=1)",
				"  0",
				"1:   (#=3)",
				"  010",
				"2:   (#=9)",
				"  010011010",
				"3:   (#=27)",
				"  010011010010011011010011010",
				"4:   (#=81)",
				"  010011010010011011010011010010011010010011011010011011010011010010011011010011010"
			],
			"maple": [
				"a:= proc(n) option remember; local m, r;",
				"      r:= irem(n, 3, 'm');",
				"      `if`(r\u003c2, r, a(m))",
				"    end:",
				"seq(a(n), n=0..1000);"
			],
			"mathematica": [
				"Nest[Flatten[ # /. {0 -\u003e {0, 1, 0}, 1 -\u003e {0, 1, 1}}] \u0026, {0}, 5]"
			],
			"program": [
				"(PARI) {a(n)=if(n\u003c1,0,polcoeff(1/(1-x)/2-sum(k=0,ceil(log(n+1)/log(3)), x^(3^k-1)/(1+x^(3^k)+x^(2*3^k)+x*O(x^n)))/2,n))} \\\\ _Paul D. Hanna_, Feb 24 2006",
				"(PARI) {a(n) = if( n\u003c1, 0, n++; n / 3^valuation(n, 3) % 3 -1 )} /* _Michael Somos_, Jul 29 2009 */",
				"(C++) /* CAT algorithm */",
				"bool bit_dragon3_turn(ulong \u0026x)",
				"/* Increment the radix-3 word x and return whether",
				"   the number of ones in x is decreased. */",
				"{",
				"    ulong s = 0;",
				"    while ( (x \u0026 3) == 2 ) { x \u003e\u003e= 2; ++s; } /* scan over nines */",
				"    bool tr = ( (x \u0026 3) != 0 ); /* incremented word will have one less 1 */",
				"    ++x; /* increment next digit */",
				"    x \u003c\u003c= (s\u003c\u003c1); /* shift back */",
				"    return tr;",
				"} /* _Joerg Arndt_, Apr 15 2010 */"
			],
			"xref": [
				"Cf. A137893 (complement), A060236 (as 1,2), A343785 (as +-1), A189640 (essentially the same).",
				"Cf. A062756, A026179 (indices of 1's except n=1), A189672 (partial sums).",
				"Cf. A189628 (guide)."
			],
			"keyword": "nonn,easy",
			"offset": "1",
			"author": "_N. J. A. Sloane_, Mar 29 2003",
			"ext": [
				"More terms from _Wouter Meeussen_, Apr 01 2003"
			],
			"references": 14,
			"revision": 66,
			"time": "2021-11-10T01:02:31-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}