{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002524",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2524,
			"id": "M1600 N0626",
			"data": "1,1,2,6,14,31,73,172,400,932,2177,5081,11854,27662,64554,150639,351521,820296,1914208,4466904,10423761,24324417,56762346,132458006,309097942,721296815,1683185225,3927803988,9165743600,21388759708,49911830577,116471963129",
			"name": "Number of permutations of length n within distance 2 of a fixed permutation.",
			"comment": [
				"From _Torleiv Kløve_, Jan 09 2009: (Start)",
				"Let V(d,n) be the number of permutations of length n within distance d of a fixed permutation. For d=1,2,3,4,...,10 these are A000045, A002524, A002526, A072856, A154654, A154655, A154656, A154657, A154658, A154659. The generating function is a rational function f_d(z)/g_d(z) (see the Kløve report referenced here). For d\u003c=6, deg g_d = 2^{d-1} + binomial(2*d,d)/2 and deg f_d(z) = deg g_d(z)-2d. As a table:",
				"   d deg g_d deg f_d",
				"   1    2       0",
				"   2    5       1",
				"   3   14       8",
				"   4   43      35",
				"   5  142     132",
				"   6  494     482",
				"(End)",
				"For positive n, a(n) equals the permanent of the n X n matrix with 1's along the five central diagonals, and 0's everywhere else. - _John M. Campbell_, Jul 09 2011"
			],
			"reference": [
				"D. H. Lehmer, Permutations with strongly restricted displacements. Combinatorial theory and its applications, II (Proc. Colloq., Balatonfured, 1969), pp. 755-770. North-Holland, Amsterdam, 1970.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).",
				"R. P. Stanley, Enumerative Combinatorics I, Example 4.7.16, p. 253."
			],
			"link": [
				"R. H. Hardin, \u003ca href=\"/A002524/b002524.txt\"\u003eTable of n, a(n) for n = 0..400\u003c/a\u003e, Jul 11 2010",
				"V. Baltic, \u003ca href=\"http://dx.doi.org/10.2298/AADM1000008B\"\u003eOn the number of certain types of strongly restricted permutations\u003c/a\u003e, Appl. An. Disc. Math. 4 (2010), 119-135; DOI:10.2298/AADM1000008B.",
				"M. Barnabei, F. Bonetti and M. Silimbani, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v19i3p25\"\u003eTwo permutation classes related to the Bubble Sort operator\u003c/a\u003e, Electronic Journal of Combinatorics 19(3) (2012), #P25. - From _N. J. A. Sloane_, Dec 25 2012",
				"Fan R. K. Chung, Persi Diaconis, and Ron Graham, \u003ca href=\"https://statweb.stanford.edu/~cgates/PERSI/papers/sequential_sampling_10.pdf\"\u003ePermanental generating functions and sequential importance sampling\u003c/a\u003e, Stanford University (2018).",
				"Torleiv Kløve, \u003ca href=\"http://www.ii.uib.no/publikasjoner/texrap/pdf/2008-376.pdf\"\u003eSpheres of Permutations under the Infinity Norm - Permutations with limited displacement\u003c/a\u003e, Reports in Informatics, Department of Informatics, University of Bergen, Norway, no. 376, November 2008.",
				"Torleiv Kløve, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v16i1r104\"\u003eGenerating functions for the number of permutations with limited displacement\u003c/a\u003e, Electron. J. Combin., 16 (2009), #R104. - From _N. J. A. Sloane_, May 04 2011.",
				"O. Krafft and M. Schaefer, \u003ca href=\"https://www.fq.math.ca/Scanned/40-5/krafft.pdf\"\u003eOn the number of permutations within a given distance\u003c/a\u003e, Fib. Quart. 40 (5) (2002) 429-434.",
				"Ting Kuo, \u003ca href=\"http://www.csroc.org.tw/journal/JOC25-2/JOC25-2-4.pdf\"\u003eK-sorted Permutations with Weakly Restricted Displacements\u003c/a\u003e, Journal of Computers, 2014. See p. 36.",
				"R. Lagrange, \u003ca href=\"http://archive.numdam.org/article/ASENS_1962_3_79_3_199_0.pdf\"\u003eQuelques résultats dans la métrique des permutations\u003c/a\u003e, Annales Scientifiques de l'École Normale Supérieure, Paris, 79 (1962), 199-241.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992.",
				"Andrew Tsao, \u003ca href=\"https://purl.stanford.edu/kq800hs0924\"\u003eSampling Methodology for Intractable Counting Problems\u003c/a\u003e, Ph. D. Thesis, Stanford University (2020).",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,0,2,0,-1)."
			],
			"formula": [
				"G.f.: (1-x)/(1-2*x-2*x^3+x^5). - _Simon Plouffe_ in his 1992 dissertation."
			],
			"mathematica": [
				"CoefficientList[Series[(1 - z)/(z^5 - 2 z^3 - 2 z + 1), {z, 0, 100}], z] (* _Vladimir Joseph Stephan Orlovsky_, Jun 24 2011 *)"
			],
			"program": [
				"(PARI) a(n)=if(n,([0,1,0,0,0; 0,0,1,0,0; 0,0,0,1,0; 0,0,0,0,1; -1,0,2,0,2]^n*[1;1;2;6;14])[1,1],1) \\\\ _Charles R Greathouse IV_, Jul 28 2015"
			],
			"xref": [
				"Column k=2 of A306209."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Typo in comment corrected by _Vaclav Kotesovec_, Dec 01 2012"
			],
			"references": 88,
			"revision": 88,
			"time": "2021-03-12T22:32:35-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}