{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A290491",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 290491,
			"data": "2,2,2,1,2,3,2,2,2,1,3,4,3,2,3,5,3,1,4,3,3,4,3,2,2,5,5,1,3,2,4,3,3,3,2,6,3,2,1,3,6,4,2,2,3,5,3,3,1,2,4,3,4,2,5,4,5,5,3,2,7,4,4,3,2,6,3,4,4,3,9,3,2,3,3,6,5,4,4,3,8,5,2,2,3,6,3,3,4,4,5,7,2,3,3,8,6,1,5,4",
			"name": "Number of ways to write 12*n+1 as x^2 + 4*y^2 + 8*z^4, where x and y are positive integers and z is a nonnegative integer.",
			"comment": [
				"Conjecture: (i) a(n) \u003e 0 for all n \u003e 0, and a(n) = 1 only for n = 4, 10, 18, 28, 39, 49, 98, 142, 163, 184, 208, 320, 382, 408, 814, 910, 1414, 2139, 2674, 3188, 3213, 4230, 6279, 25482.",
				"(ii) All the numbers 16*n+5 (n = 0,1,2,...) can be written as x^4 + 4*y^2 + z^2, where x,y,z are integers with y \u003e 0 and z \u003e 0.",
				"(iii) All the numbers 24*n+1 (n = 0,1,2,...) can be written as 12*x^4 + 4*y^2 + z^2 with x,y,z integers. Also, all the numbers 24*n+9 (n = 0,1,2,...) can be written as 2*x^4 + 6*y^2 + z^2 with x,y,z positive integers.",
				"(iv) All the numbers 24*n+2 (n = 0,1,2,...) can be written as x^4 + 9*y^2 + z^2, where x,y,z are integers with z \u003e 0. Also, all the numbers 24*n+17 (n = 0,1,2,...) can be written as x^4 + 16*y^2 + z^2, where x,y,z are integers with y \u003e 0 and z \u003e 0.",
				"(v) All the numbers 30*n+3 (n = 1,2,3,...) can be written as 2*x^4 + 3*y^2 + z^2 with x,y,z positive integers. Also, all the numbers 30*n+21 (n = 0,1,2,...) can be written as 2*x^4 + 3*y^2 + z^2, where x,y,z are integers with z \u003e 0."
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A290491/b290491.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://math.scichina.com:8081/sciAe/EN/abstract/abstract517007.shtml\"\u003eOn universal sums of polygonal numbers\u003c/a\u003e, Sci. China Math. 58(2015), 1367-1396.",
				"Zhi-Wei Sun, \u003ca href=\"http://arxiv.org/abs/1502.03056\"\u003eOn universal sums x(ax+b)/2+y(cy+d)/2+z(ez+f)/2\u003c/a\u003e, arXiv:1502.03056 [math.NT], 2015-2017.",
				"Zhi-Wei Sun, \u003ca href=\"http://maths.nju.edu.cn/~zwsun/179b.pdf\"\u003eNew conjectures on representations of integers (I)\u003c/a\u003e, Nanjing Univ. J. Math. Biquarterly 34(2017), no. 2, 97-120."
			],
			"example": [
				"a(10) = 1 since 12*10+1 = 7^2 + 4*4^2 + 8*1^4.",
				"a(28) = 1 since 12*28+1 = 9^2 + 4*8^2 + 8*0^4.",
				"a(49) = 1 since 12*49+1 = 19^2 + 4*5^2 + 8*2^4.",
				"a(3188) = 1 since 12*3188+1 = 103^2 + 4*80^2 + 8*4^4.",
				"a(3213) = 1 since 12*3213+1 = 91^2 + 4*87^2 + 8*0^4.",
				"a(4230) = 1 since 12*4230+1 = 223^2 + 4*16^2 + 8*1^4.",
				"a(6279) = 1 since 12*6279+1 = 19^2 + 4*75^2 + 8*9^4.",
				"a(25482) = 1 since 12*25482+1 = 531^2 + 4*58^2 + 8*6^4."
			],
			"mathematica": [
				"SQ[n_]:=SQ[n]=IntegerQ[Sqrt[n]];",
				"Do[r=0;Do[If[SQ[12n+1-8x^4-4y^2],r=r+1],{x,0,((12n+1)/8)^(1/4)},{y,1,Sqrt[(12n+1-8x^4)/4]}];Print[n,\" \",r],{n,1,100}]"
			],
			"xref": [
				"Cf. A000290, A000583, A270566, A286885, A286944, A287616, A290342, A290472."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Zhi-Wei Sun_, Aug 03 2017",
			"references": 4,
			"revision": 17,
			"time": "2017-12-30T09:44:52-05:00",
			"created": "2017-08-04T03:16:42-04:00"
		}
	]
}