{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A134178",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 134178,
			"data": "1,1,-2,-2,0,1,2,0,0,-1,-4,0,1,0,6,2,0,1,-8,0,0,0,12,0,-1,-1,-18,-4,0,-1,24,0,0,2,-32,0,0,1,44,6,0,-2,-58,0,0,-1,76,0,1,2,-100,-8,0,1,128,0,0,-3,-164,0,0,-1,210,12,0,4,-264,0,0,2,332,0,-1,-5,-416,-18,0,-2,516,0,0,5,-640,0,-1,2,790,24",
			"name": "Expansion of chi(x) * chi(-x^2)^2 * chi(-x^3) * chi(-x^4) * chi(x^6)^2 * chi(-x^12) in powers of x where chi() is a Ramanujan theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A134178/b134178.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Euler transform of period 24 sequence [ 1, -3, 0, -1, 1, -1, 1, 0, 0, -3, 1, -4, 1, -3, 0, 0, 1, -1, 1, -1, 0, -3, 1, 0, ...].",
				"a(12*n + 4) = a(12*n + 7) = a(12*n + 8) = a(12*n + 11) = 0.",
				"a(4*n + 1) = a(12*n) = A029838(n). a(4*n + 2) = a(12*n + 3) = -2 * A083365(n)."
			],
			"example": [
				"G.f. = 1 + x - 2*x^2 - 2*x^3 + x^5 + 2*x^6 - x^9 - 4*x^10 + x^12 + 6*x^14 + ...",
				"G.f. = q^-3 + q^-1 - 2*q - 2*q^3 + q^7 + 2*q^9 - q^15 - 4*q^17 + q^21 + 6*q^25 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ -x, x^2] QPochhammer[ x^2, x^4]^2 QPochhammer[ x^3, x^6] QPochhammer[ x^4, x^8] QPochhammer[-x^6, x^12]^2 QPochhammer[ x^12, x^24], {x, 0, n}]; (* _Michael Somos_, Oct 25 2015 *)",
				"a[ n_] := SeriesCoefficient[ QPochhammer[ -x^12, x^24] QPochhammer[ x^24, x^48] + x QPochhammer[ -x^4, x^8] QPochhammer[ x^8, x^16] - 2 x^2 QPochhammer[ x^16] / QPochhammer[ -x^4] - 2 x^3 QPochhammer[ x^48] / QPochhammer[ -x^12], {x, 0, n}]; (* _Michael Somos_, Oct 25 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^4 * eta(x^3 + A) * eta(x^12 + A)^5 / (eta(x + A) * eta(x^4 + A)^2 * eta(x^6 + A)^3 * eta(x^8 + A) * eta(x^24 + A)^3), n))};"
			],
			"xref": [
				"Cf. A029838, A083365."
			],
			"keyword": "sign",
			"offset": "0,3",
			"author": "_Michael Somos_, Oct 11 2007",
			"references": 3,
			"revision": 13,
			"time": "2021-03-12T22:24:45-05:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}