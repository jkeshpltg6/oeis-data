{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A045479",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 45479,
			"data": "1,-8,276,-2048,11202,-49152,184024,-614400,1881471,-5373952,14478180,-37122048,91231550,-216072192,495248952,-1102430208,2390434947,-5061476352,10487167336,-21301241856,42481784514,-83300614144",
			"name": "McKay-Thompson series of class 2B for the Monster group with a(0) = -8.",
			"comment": [
				"Unsigned sequence gives McKay-Thompson series of class 4A for Monster; also character of extremal vertex operator algebra of rank 12.",
				"The value of a(0) is the Rademacher constant for the modular function and appears in Conway and Norton's Table 4. - _Michael Somos_, Mar 08 2011"
			],
			"reference": [
				"G. Hoehn, Selbstduale Vertexoperatorsuperalgebren und das Babymonster, Bonner Mathematische Schriften, Vol. 286 (1996), 1-85."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A045479/b045479.txt\"\u003eTable of n, a(n) for n = -1..1000\u003c/a\u003e",
				"R. E. Borcherds, \u003ca href=\"http://www.math.berkeley.edu/~reb/papers/\"\u003eIntroduction to the monster Lie algebra\u003c/a\u003e, pp. 99-107 of M. Liebeck and J. Saxl, editors, Groups, Combinatorics and Geometry (Durham, 1990). London Math. Soc. Lect. Notes 165, Cambridge Univ. Press, 1992.",
				"B. Brent, \u003ca href=\"http://www.emis.de/journals/EM/expmath/volumes/7/7.html\"\u003eQuadratic Minima and Modular Forms\u003c/a\u003e, Experimental Mathematics, v.7 no.3, 257-274.",
				"J. H. Conway and S. P. Norton, \u003ca href=\"https://doi.org/10.1112/blms/11.3.308\"\u003eMonstrous Moonshine\u003c/a\u003e, Bull. Lond. Math. Soc. 11 (1979) 308-339.",
				"D. Ford, J. McKay and S. P. Norton, \u003ca href=\"http://dx.doi.org/10.1080/00927879408825127\"\u003eMore on replicable functions\u003c/a\u003e, Commun. Algebra 22, No. 13, 5175-5193 (1994).",
				"G. Hoehn (gerald(AT)math.ksu.edu), Selbstduale Vertexoperatorsuperalgebren und das Babymonster, Doctoral Dissertation, Univ. Bonn, Jul 15 1995 (\u003ca href=\"http://www.math.ksu.edu/~gerald/papers/dr.pdf\"\u003epdf\u003c/a\u003e, \u003ca href=\"http://www.math.ksu.edu/~gerald/papers/dr.ps.gz\"\u003eps\u003c/a\u003e).",
				"J. McKay and H. Strauss, \u003ca href=\"http://dx.doi.org/10.1080/00927879008823911\"\u003eThe q-series of monstrous moonshine and the decomposition of the head characters\u003c/a\u003e, Comm. Algebra 18 (1990), no. 1, 253-278.",
				"\u003ca href=\"/index/Mat#McKay_Thompson\"\u003eIndex entries for McKay-Thompson series for Monster simple group\u003c/a\u003e"
			],
			"formula": [
				"Expansion of 16 + (eta(q) / eta(q^2))^24 in powers of q. - _Michael Somos_, Mar 08 2011",
				"a(n) ~ (-1)^(n+1) * exp(2*Pi*sqrt(n)) / (2*n^(3/4)). - _Vaclav Kotesovec_, Sep 07 2017"
			],
			"example": [
				"1/q - 8 + 276*q - 2048*q^2 + 11202*q^3 - 49152*q^4 + 184024*q^5 + ..."
			],
			"mathematica": [
				"a[0] = -8; a[n_] := SeriesCoefficient[ Product[1 - q^k, {k, 1, n+1, 2}]^24/q, {q, 0, n}]; Table[a[n], {n, -1, 20}] (* _Jean-François Alcover_, Oct 14 2013, after _Michael Somos_ *)",
				"QP = QPochhammer; s = 16*q + (QP[q]/QP[q^2])^24 + O[q]^30; CoefficientList[s, q] (* _Jean-François Alcover_, Nov 15 2015, after _Michael Somos_ *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c-1, 0, n++; A = x * O(x^n); polcoeff( 16 * x + (eta(x + A) / eta(x^2 + A))^24, n))}; /* _Michael Somos_, Mar 08 2011 */"
			],
			"xref": [
				"A134786, A045479, A007191, A097340, A035099, A007246, A107080 are all essentially the same sequence."
			],
			"keyword": "sign,easy,nice",
			"offset": "-1,2",
			"author": "_N. J. A. Sloane_",
			"references": 7,
			"revision": 41,
			"time": "2020-08-03T00:46:03-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}