{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A135313",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 135313,
			"data": "1,0,1,0,1,3,0,1,12,13,0,1,61,106,75,0,1,310,1105,1035,541,0,1,1821,12075,16025,11301,4683,0,1,11592,141533,267715,239379,137774,47293,0,1,80963,1812216,4798983,5287506,3794378,1863044,545835,0,1,608832,25188019,92374107,124878033,105494886,64432638,27733869,7087261",
			"name": "Triangle of numbers T(n,k) (n\u003e=0, n\u003e=k\u003e=0) of transitive reflexive early confluent binary relations R on n labeled elements where k=max_{x}(|{y : xRy}|), read by rows.",
			"comment": [
				"R is early confluent iff (xRy and xRz) implies (yRz or zRy) for all x, y, z.",
				"Conjecture: For fixed k\u003e=0, A135313(n+k,n) ~ n! * n^(2*k)  / (2^(k+1) * k! * log(2)^(n+k+1)). - _Vaclav Kotesovec_, Nov 20 2021"
			],
			"reference": [
				"A. P. Heinz (1990). Analyse der Grenzen und Möglichkeiten schneller Tableauoptimierung. PhD Thesis, Albert-Ludwigs-Universität Freiburg, Freiburg i. Br., Germany."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A135313/b135313.txt\"\u003eRows n = 0..140, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,0) = A135302(n,0), T(n,k) = A135302(n,k) - A135302(n,k-1) for k\u003e0.",
				"E.g.f. of column k=0: tt_0(x) = 1, e.g.f. of column k\u003e0: tt_k(x) = t_k(x) -t_{k-1}(x), where t_k(x) = exp (Sum_{m=1..k} x^m/m! * t_{k-m}(x)) if k\u003e=0 and t_k(x) = 0 else."
			],
			"example": [
				"T(3,3) = 13 because there are 13 relations of the given kind for 3 elements:  (1) 1R2, 2R1, 1R3, 3R1, 2R3, 3R2;  (2) 1R2, 1R3, 2R3, 3R2;  (3) 2R1, 2R3, 1R3, 3R1;  (4) 3R1, 3R2, 1R2, 2R1;  (5) 2R1, 3R1, 2R3, 3R2; (6) 1R2, 3R2, 1R3, 3R1;  (7) 1R3, 2R3, 1R2, 2R1;  (8) 1R2, 2R3, 1R3;  (9) 1R3, 3R2, 1R2;  (10) 2R1, 1R3, 2R3;  (11) 2R3, 3R1, 2R1;  (12) 3R1, 1R2, 3R2;  (13) 3R2, 2R1, 3R1; (the reflexive relationships 1R1, 2R2, 3R3 have been omitted for brevity).",
				"Triangle T(n,k) begins:",
				"  1;",
				"  0,  1;",
				"  0,  1,    3;",
				"  0,  1,   12,    13;",
				"  0,  1,   61,   106,    75;",
				"  0,  1,  310,  1105,  1035,   541;",
				"  0,  1, 1821, 12075, 16025, 11301, 4683;",
				"  ..."
			],
			"maple": [
				"t:= proc(k) option remember; `if`(k\u003c0, 0,",
				"      unapply(exp(add(x^m/m!*t(k-m)(x), m=1..k)), x))",
				"    end:",
				"tt:= proc(k) option remember;",
				"       unapply((t(k)-t(k-1))(x), x)",
				"     end:",
				"T:= proc(n, k) option remember;",
				"      coeff(series(tt(k)(x), x, n+1), x, n)*n!",
				"    end:",
				"seq(seq(T(n, k), k=0..n), n=0..12);"
			],
			"mathematica": [
				"f[0, _] = 1; f[k_, x_] := f[k, x] = Exp[Sum[x^m/m!*f[k - m, x], {m, 1, k}]]; (* a = A135302 *) a[0, 0] = 1; a[_, 0] = 0; a[n_, k_] := SeriesCoefficient[f[k, x], {x, 0, n}]*n!; t[n_, 0] := a[n, 0]; t[n_, k_] := a[n, k] - a[n, k-1]; Table[t[n, k], {n, 0, 9}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Dec 06 2013, after A135302 *)"
			],
			"xref": [
				"Columns k=0-10 give: A000007, A057427, A218092, A218093, A218094, A218095, A218096, A218097, A218098, A218099, A218091.",
				"Main diagonal and lower diagonals give: A000670, A218111, A218112, A218103, A218104, A218105, A218106, A218107, A218108, A218109, A218110.",
				"Row sums are in A052880.",
				"T(2n,n) gives A261238.",
				"Cf. A135302."
			],
			"keyword": "nonn,tabl",
			"offset": "0,6",
			"author": "_Alois P. Heinz_, Dec 05 2007",
			"references": 24,
			"revision": 44,
			"time": "2021-11-20T10:52:22-05:00",
			"created": "2007-12-09T03:00:00-05:00"
		}
	]
}