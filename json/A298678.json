{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A298678",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 298678,
			"data": "1,0,7,12,73,216,919,3204,12409,45408,171271,635580,2379241,8865000,33113527,123523572,461111833,1720661616,6422058919,23966525484,89446140169,333813840888,1245817611991,4649439829860,17351975261881,64758394108800,241681735391047",
			"name": "Start with the hexagonal tile of the Shield tiling and recursively apply the substitution rule. a(n) is the number of hexagonal tiles after n iterations.",
			"comment": [
				"The following substitution rules apply to the tiles:",
				"triangle with 6 markings -\u003e 1 hexagon",
				"triangle with 4 markings -\u003e 1 square, 2 triangles with 4 markings",
				"square                   -\u003e 1 square, 4 triangles with 6 markings",
				"hexagon                  -\u003e 7 triangles with 6 markings, 3 triangles with 4 markings, 3 squares",
				"For n \u003e 0, a(n) is also the number of triangles with 6 markings after n iterations when starting with the hexagon.",
				"a(n) is also the number of triangles with 6 markings after n iterations when starting with the triangle with 6 markings.",
				"a(n) is also the number of hexagons after n iterations when starting with the triangle with 6 markings."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A298678/b298678.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"F. Gähler, \u003ca href=\"https://doi.org/10.1016/0022-3093(93)90335-U\"\u003eMatching rules for quasicrystals: the composition-decomposition method\u003c/a\u003e, Journal of Non-Crystalline Solids, 153-154 (1993), 160-164.",
				"Tilings Encyclopedia, \u003ca href=\"https://tilings.math.uni-bielefeld.de/substitution/shield\"\u003eShield\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,7,-2)."
			],
			"formula": [
				"G.f.: (1-2*x)/((1+2*x)*(1-4*x+x^2)). - _Joerg Arndt_, Jan 25 2018",
				"13*a(n) = A077235(n) + 8*(-2)^n. - _Bruno Berselli_, Jan 25 2018",
				"From _Colin Barker_, Jan 25 2018: (Start)",
				"a(n) = (1/26)*((-1)^n*2^(4+n) + (5-2*sqrt(3))*(2-sqrt(3))^n + (2+sqrt(3))^n*(5+2*sqrt(3))).",
				"a(n) = 2*a(n-1) + 7*a(n-2) - 2*a(n-3) for n\u003e2.",
				"(End)"
			],
			"program": [
				"(PARI) /* The function substitute() takes as argument a 4-element vector, where the first, second, third and fourth elements respectively are the number of triangles with 6 markings, the number of triangles with 4 markings, the number of squares and the number of hexagons that are to be substituted. The function returns a vector w, where the first, second, third and fourth elements respectively are the number of triangles with 6 markings, the number of triangles with 4 markings, the number of squares and the number of hexagons resulting from the substitution. */",
				"substitute(v) = my(w=vector(4)); for(k=1, #v, while(v[1] \u003e 0, w[4]++; v[1]--); while(v[2] \u003e 0, w[3]++; w[2]=w[2]+2; v[2]--); while(v[3] \u003e 0, w[3]++; w[1]=w[1]+4; v[3]--); while(v[4] \u003e 0, w[1]=w[1]+7; w[2]=w[2]+3; w[3]=w[3]+3; v[4]--)); w",
				"terms(n) = my(v=[0, 0, 0, 1], i=0); while(1, print1(v[4], \", \"); i++; if(i==n, break, v=substitute(v)))",
				"(PARI) Vec((1-2*x)/((1+2*x)*(1-4*x+x^2)) + O(x^40)) \\\\ _Colin Barker_, Jan 25 2018"
			],
			"xref": [
				"Cf. A298679, A298680, A298681, A298682, A298683."
			],
			"keyword": "nonn,easy",
			"offset": "0,3",
			"author": "_Felix Fröhlich_, Jan 24 2018",
			"ext": [
				"More terms from _Colin Barker_, Jan 25 2018"
			],
			"references": 6,
			"revision": 22,
			"time": "2018-01-29T05:49:01-05:00",
			"created": "2018-01-28T19:00:36-05:00"
		}
	]
}