{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A251756",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 251756,
			"data": "0,4,8,12,6,18,9,27,36,16,20,10,30,15,45,54,24,28,14,42,21,63,72,32,40,44,22,66,33,99,81,90,48,52,26,78,39,117,108,56,60,50,25,75,100,64,68,34,102,51,153,126,70,35,105,84,76,38,114,57,171,135,120,80",
			"name": "a(0) = 0; for n\u003e0, a(n) is the smallest integer not already in the list with a composite common factor with a(n-1).",
			"comment": [
				"It appears that this sequence includes every composite number.",
				"The values are grouped close to five lines extending from the origin with respective slope of approximately { 0.608, 0.912, 1.22, 1.82, 2.74 } = {1, 1.5, 2, 3, 4.5} * 0.608. (As in A098550 these lines are not really straight.) - _M. F. Hasler_, Dec 14 2014"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A251756/b251756.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"David L. Applegate, Hans Havermann, Bob Selcoe, Vladimir Shevelev, N. J. A. Sloane, and Reinhard Zumkeller, The Yellowstone Permutation, arXiv preprint arXiv:1501.01669, 2015 and \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL18/Sloane/sloane9.html\"\u003eJ. Int. Seq. 18 (2015) 15.6.7\u003c/a\u003e."
			],
			"mathematica": [
				"g[a_List] := Block[{k = 4}, While[Not[CompositeQ[GCD[a[[-1]], k]]] || MemberQ[a, k], k++]; Append[a, k]]; Nest[g, {0}, 63] (* _L. Edson Jeffery_, Dec 08 2014 (after _Robert G. Wilson v_) *)"
			],
			"program": [
				"(PARI) invecn(v, k, x)=for(i=1, k, if(v[i]==x, return(i))); 0",
				"alist(n)=local(v=vector(n), x, g); v[1]=4; for(k=2, n, x=4; while(invecn(v, k-1, x)||(g=gcd(v[k-1], x))==1||isprime(g), x++); v[k]=x); v",
				"(Haskell)",
				"import Data.List (delete)",
				"a251756 n = a251756_list !! (n-1)",
				"a251756_list = 0 : f 0 a002808_list where",
				"   f x zs = g zs where",
				"     g (y:ys) | d == 1 || a010051' d == 1 = g ys",
				"              | otherwise = y : f y (delete y zs)",
				"              where d = gcd x y",
				"-- _Reinhard Zumkeller_, Dec 08 2014",
				"(Python)",
				"from gmpy2 import gcd, is_prime",
				"A251756_list, l, s, b = [0], 0, 1, {}",
				"for _ in range(10**3):",
				"....i = s",
				"....while True:",
				"........if not i in b:",
				"............m = gcd(i,l)",
				"............if not (m == 1 or is_prime(m)):",
				"................A251756_list.append(i)",
				"................l, b[i] = i, True",
				"................while s in b:",
				"....................b.pop(s)",
				"....................s += 1",
				"................break",
				"........i += 1 # _Chai Wah Wu_, Dec 08 2014"
			],
			"xref": [
				"Cf. A064413, A002808, A010051, A098550."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Franklin T. Adams-Watters_, Dec 08 2014",
			"references": 2,
			"revision": 27,
			"time": "2018-05-15T16:48:16-04:00",
			"created": "2014-12-08T02:55:18-05:00"
		}
	]
}