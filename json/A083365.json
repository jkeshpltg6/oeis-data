{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A083365",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 83365,
			"data": "1,-1,2,-3,4,-6,9,-12,16,-22,29,-38,50,-64,82,-105,132,-166,208,-258,320,-395,484,-592,722,-876,1060,-1280,1539,-1846,2210,-2636,3138,-3728,4416,-5222,6163,-7256,8528,-10006,11716,-13696,15986,-18624,21666,-25169,29190,-33808,39104",
			"name": "Expansion of psi(x) / phi(x) in powers of x where phi(), psi() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Convolution square is A079006.",
				"Convolution inverse is A029838."
			],
			"reference": [
				"B. C. Berndt, Ramanujan's Notebooks Part III, Springer-Verlag, see p. 221 Entry 1(i).",
				"A. Cayley, A memoir on the transformation of elliptic functions, Collected Mathematical Papers. Vols. 1-13, Cambridge Univ. Press, London, 1889-1897, Vol. 9, p. 128.",
				"H. T. Davis, Introduction to nonlinear differential and integral equations, Dover Publications, Inc., New York 1962, p. 170 MR0181773 (31 #6000)"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A083365/b083365.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"A. Cayley, \u003ca href=\"/A001934/a001934.pdf\"\u003eA memoir on the transformation of elliptic functions\u003c/a\u003e, Philosophical Transactions of the Royal Society of London (1874): 397-456; Collected Mathematical Papers. Vols. 1-13, Cambridge Univ. Press, London, 1889-1897, included in Vol. 9. [Annotated scan of pages 126-129]",
				"W. Duke, \u003ca href=\"http://dx.doi.org/10.1090/S0273-0979-05-01047-5\"\u003eContinued fractions and modular functions\u003c/a\u003e, Bull. Amer. Math. Soc. 42 (2005), 137-162; see Eqs. (9.1),(9.3).",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e, \u003ca href=\"http://mathworld.wolfram.com/q-PochhammerSymbol.html\"\u003eq-Pochhammer Symbol\u003c/a\u003e"
			],
			"formula": [
				"Expansion of f(-x^4) / f(x) = psi(x) / phi(x) = psi(x^2) / psi(x) = psi(-x) / phi(-x^2) = 1 / (chi(x) * chi(-x^2)) = 1 / (chi^2(x) * chi(-x)) = chi(-x) / chi^2(-x^2) = (psi(x^2) / phi(x))^(1/2) in powers of x where phi(), psi(), chi(), f() are Ramanujan theta functions.",
				"Expansion of k^(1/4) / (2^(1/2) * q^(1/8)) in powers of q where k is elliptic modulus and q is the nome.",
				"Expansion of q^(-1/8) * eta(q) * eta(q^4)^2 / eta(q^2)^3 in powers of q.",
				"Given g.f. A(x), then B(q) = q * A(q^8) satisfies 0 = f(B(q), B(q^2)) where f(u, v) = v^2 - u^4 * (1 + 4*v^4).",
				"Given g.f. A(x), then B(q) = q * A(q^8) satisfies 0 = f(B(q), B(q^3)) where f(u, v) = v^4 - u^4 + u*v + 4*(u*v)^3.",
				"Given g.f. A(x), then B(q) = q * A(q^8) satisfies 0 = f(B(q), B(q^2), B(q^4)) where f(u, v, w) = w - u^2*v*(1 + 2*w^2). - _Michael Somos_, May 29 2005",
				"Given g.f. A(x), then B(q) = q * A(q^8) satisfies 0 = f(B(q), B(q^2), B(q^3), B(q^6)) where f(u1, u2, u3, u6) = u2*u6 - u1*u3 * (u2^2 + u6^2). - _Michael Somos_, May 29 2005",
				"Given g.f. A(x), then B(q) = sqrt(2) * q * A(q^8) satisfies 0 = f(B(q), B(q^7)) where f(u, v) = (1 - u^8) * (1 - v^8) - (1 - u*v)^8. - _Michael Somos_, Jan 01 2006",
				"Euler transform of period 4 sequence [-1, 2, -1, 0, ...].",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (32 t)) = 2^(-1/2) * g(t) where q = exp(2 Pi i t) and g() is the g.f. for A108494. - _Michael Somos_, Feb 29 2012",
				"G.f.: Product_{k\u003e0} (1 + x^(2*k)) / (1 + x^(2*k - 1)) = (Sum_{k\u003e0} x^(k^2 - k)) / (Sum_{k\u003e0} x^((k^2 - k)/2)).",
				"G.f.: 1 / (1 + x / (1 + x + x^2 / (1 + x^2 + x^3 / (1 + x^3 + ...)))).",
				"A001935(n) = (-1)^n a(n).",
				"G.f.: (1+1/Q(0))/2, where Q(k)= 1 + x^(k+1) + x^(k+1)/Q(k+1); (continued fraction). - _Sergei N. Gladkovskii_, Apr 30 2013",
				"a(n) ~ (-1)^n * exp(Pi*sqrt(n/2))/(2^(11/4)*n^(3/4)). - _Vaclav Kotesovec_, Jul 04 2016",
				"G.f.: (-x^2; x^2)_{-1/2} = ((-1; x^2)_{1/2})/2, where (a; q)_n is the q-Pochhammer symbol. - _Vladimir Reshetnikov_, Nov 20 2016",
				"a(0) = 1, a(n) = -(1/n)*Sum_{k=1..n} A109506(k)*a(n-k) for n \u003e 0. - _Seiichi Manyama_, Apr 14 2017",
				"a(n) ~ (-1)^n * exp(Pi*sqrt(n/2)) / (2^(11/4) * n^(3/4)). - _Vaclav Kotesovec_, Nov 15 2017",
				"G.f.: exp(Sum_{k\u003e=1} (-1)^k*x^k/(k*(1 + x^k))). - _Ilya Gutkovskiy_, May 28 2018"
			],
			"example": [
				"G.f. = 1 - x + 2*x^2 - 3*x^3 + 4*x^4 - 6*x^5 + 9*x^6 - 12*x^7 + 16*x^8 - 22*x^9 + ...",
				"G.f. = q - q^9 + 2*q^17 - 3*q^25 + 4*q^33 - 6*q^41 + 9*q^49 - 12*q^57 + 16*q^65 + ..."
			],
			"mathematica": [
				"phi[x_] := EllipticTheta[3, 0, x]; psi[x_] := (1/2)*x^(-1/8)*EllipticTheta[2, 0, x^(1/2)]; s = Series[ psi[x]/phi[x], {x, 0, 100}]; A083365 = CoefficientList[s, x] (* _Jean-François Alcover_, Feb 18 2015 *)",
				"nmax = 50; CoefficientList[Series[Product[(1 + x^(2*k))^2/(1 + x^k), {k, 1, nmax}], {x, 0, nmax}], x] (* _Vaclav Kotesovec_, Jul 04 2016 *)",
				"(QPochhammer[-x^2, x^2, -1/2] + O[x]^50)[[3]] (* _Vladimir Reshetnikov_, Nov 20 2016 *)",
				"a[ n_] := SeriesCoefficient[ QPochhammer[ -x^2, x^2] / QPochhammer[ -x, x^2], {x, 0, n}]; (* _Michael Somos_, Oct 10 2019~ *)"
			],
			"program": [
				"(PARI) {a(n) = my(A, m); if( n\u003c0, 0, A = 1 + O(x); m=1; while( m\u003c=n, m*=2; A = subst(A, x, x^2); A = sqrt(A / (1 + 4 * x * A^2))); polcoeff(sqrt(A), n))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = contfracpnqn( matrix(2, (sqrtint(8*n + 1) + 1)\\2, i, j, if( i==1, x^(j-1), 1 + if( j\u003e1, x^(j-1))))); polcoeff(A[2,1] / A[1,1] + x * O(x^n), n))};",
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A) * eta(x^4 + A)^2 / eta(x^2 + A)^3, n))};"
			],
			"xref": [
				"Cf. A001935, A029838, A108494.",
				"(psi(x) / phi(x))^b: this sequence (b=1), A079006 (b=2), A187053 (b=3), A001938 (b=4), A195861 (b=5), A320049 (b=6), A320050 (b=7)."
			],
			"keyword": "sign",
			"offset": "0,3",
			"author": "_Michael Somos_, Apr 24 2003",
			"references": 19,
			"revision": 71,
			"time": "2021-03-12T22:24:42-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}