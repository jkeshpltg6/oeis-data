{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A265747",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 265747,
			"data": "0,1,2,10,11,100,101,102,110,111,200,1000,1001,1002,1010,1011,1100,1101,1102,1110,1111,10000,10001,10002,10010,10011,10100,10101,10102,10110,10111,10200,11000,11001,11002,11010,11011,11100,11101,11102,11110,11111,20000,100000,100001,100002,100010,100011,100100",
			"name": "Numbers written in Jacobsthal greedy base.",
			"comment": [
				"These are called \"Jacobsthal Representation Numbers\" in Horadam's 1996 paper.",
				"Sum_{i=0..} digit(i)*A001045(2+digit(i)) recovers n from such representation a(n), where digit(0) stands for the least significant digit (at the right), and A001045(k) gives the k-th Jacobsthal number.",
				"No larger digits than 2 will occur, which allows representing the same sequence in a more compact form by base-3 coding in A265746.",
				"Sequence A197911 gives the terms with no digit \"2\" in their representation, while its complement A003158 gives the terms where \"2\" occurs at least once.",
				"Numbers beginning with digit \"2\" in this representation are given by A020988(n) [= 2*A002450(n) = 2*A001045(2n)]."
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A265747/b265747.txt\"\u003eTable of n, a(n) for n = 0..10923\u003c/a\u003e",
				"A. F. Horadam, \u003ca href=\"http://www.fq.math.ca/Scanned/34-1/horadam2.pdf\"\u003eJacobsthal Representation Numbers\u003c/a\u003e, Fib Quart. 34, 40-54, 1996. (See especially page 50, which is page 11 in PDF.)"
			],
			"formula": [
				"a(0) = 0; for n \u003e= 1, a(n) = 10^(A130249(n)-2) + a(n-A001045(A130249(n))).",
				"a(n) = A007089(A265746(n))."
			],
			"example": [
				"For n=7, when selecting the terms of A001045 with the greedy algorithm, we need terms A001045(4) + A001045(2) + A001045(2) = 5 + 1 + 1, thus a(7) = \"102\".",
				"For n=10, we need A001045(4) + A001045(4) = 5+5, thus a(10) = \"200\"."
			],
			"program": [
				"(Scheme, with memoization-macro definec)",
				"(definec (A265747 n) (if (zero? n) n (+ (expt 10 (- (A130249 n) 2)) (A265747 (- n (A001045 (A130249 n)))))))",
				"(Python)",
				"def greedyJ(n): m = (3*n+1).bit_length() - 1; return (m, (2**m-(-1)**m)//3)",
				"def a(n):",
				"    if n == 0: return 0",
				"    place, value = greedyJ(n)",
				"    return 10**(place-2) + a(n - value)",
				"print([a(n) for n in range(49)]) # _Michael S. Branicky_, Jul 11 2021"
			],
			"xref": [
				"Cf. A001045, A002450, A020988, A007089, A197911, A003158.",
				"Cf. A265745 (sum of digits).",
				"Cf. A265746 (same numbers interpreted in base-3, then shown in decimal).",
				"Cf. A084639 (positions of repunits).",
				"Cf. A007961, A014417, A014418, A244159 for analogous sequences."
			],
			"keyword": "nonn,base",
			"offset": "0,3",
			"author": "_Antti Karttunen_, Dec 17 2015",
			"references": 6,
			"revision": 38,
			"time": "2021-07-11T13:07:43-04:00",
			"created": "2015-12-18T11:20:50-05:00"
		}
	]
}