{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A214676",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 214676,
			"data": "1,1,11,1,2,111,1,2,11,1111,1,2,3,12,11111,1,2,3,11,21,111111,1,2,3,4,12,22,1111111,1,2,3,4,11,13,111,11111111,1,2,3,4,5,12,21,112,111111111,1,2,3,4,5,11,13,22,121,1111111111",
			"name": "A(n,k) is n represented in bijective base-k numeration; square array A(n,k), n\u003e=1, k\u003e=1, read by antidiagonals.",
			"comment": [
				"The digit set for bijective base-k numeration is {1, 2, ..., k}."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A214676/b214676.txt\"\u003eAntidiagonals n = 1..18, flattened\u003c/a\u003e",
				"R. R. Forslund, \u003ca href=\"http://www.emis.de/journals/SWJPAM/Vol1_1995/rrf01.ps\"\u003eA logical alternative to the existing positional number system\u003c/a\u003e, Southwest Journal of Pure and Applied Mathematics, Vol. 1, 1995, 27-29.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Zerofree.html\"\u003eZerofree\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Bijective_numeration\"\u003eBijective numeration\u003c/a\u003e"
			],
			"example": [
				"Square array A(n,k) begins:",
				":         1,   1,  1,  1,  1,  1,  1,  1, ...",
				":        11,   2,  2,  2,  2,  2,  2,  2, ...",
				":       111,  11,  3,  3,  3,  3,  3,  3, ...",
				":      1111,  12, 11,  4,  4,  4,  4,  4, ...",
				":     11111,  21, 12, 11,  5,  5,  5,  5, ...",
				":    111111,  22, 13, 12, 11,  6,  6,  6, ...",
				":   1111111, 111, 21, 13, 12, 11,  7,  7, ...",
				":  11111111, 112, 22, 14, 13, 12, 11,  8, ..."
			],
			"maple": [
				"A:= proc(n, b) local d, l, m; m:= n; l:= NULL;",
				"      while m\u003e0 do  d:= irem(m, b, 'm');",
				"        if d=0 then d:=b; m:=m-1 fi;",
				"        l:= d, l",
				"      od; parse(cat(l))",
				"    end:",
				"seq(seq(A(n, 1+d-n), n=1..d), d=1..12);"
			],
			"mathematica": [
				"A[n_, b_] := Module[{d, l, m}, m = n; l = Nothing; While[m \u003e 0, {m, d} = QuotientRemainder[m, b]; If[d == 0, d = b; m--]; l = {d, l}]; FromDigits @ Flatten @ l];",
				"Table[A[n, d-n+1], {d, 1, 12}, {n, 1, d}] // Flatten (* _Jean-François Alcover_, May 28 2019, from Maple *)"
			],
			"xref": [
				"Columns k=1-9 give: A000042, A007931, A007932, A084544, A084545, A057436, A214677, A214678, A052382.",
				"A(n+1,n) gives A010850."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Alois P. Heinz_, Jul 25 2012",
			"references": 14,
			"revision": 26,
			"time": "2020-02-14T10:05:27-05:00",
			"created": "2012-07-28T18:03:21-04:00"
		}
	]
}