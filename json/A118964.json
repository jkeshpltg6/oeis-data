{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A118964",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 118964,
			"data": "2,5,1,14,5,1,42,19,8,1,132,67,40,12,1,429,232,166,79,17,1,1430,804,634,395,145,23,1,4862,2806,2335,1708,879,249,30,1,16796,9878,8480,6824,4376,1823,404,38,1,58786,35072,30691,26137,19334,10521,3542,625,47,1",
			"name": "Triangle read by rows: T(n,k) is the number of Grand Dyck paths of semilength n that have k double rises above the x-axis (n\u003e=1,k\u003e=0). (A Grand Dyck path of semilength n is a path in the half-plane x\u003e=0, starting at (0,0), ending at (2n,0) and consisting of steps u=(1,1) and d=(1,-1); a double rise in a Grand Dyck path is an occurrence of uu in the path.)",
			"comment": [
				"Row sums are the central binomial coefficients (A000984). T(n,0)=A000108(n+1) (the Catalan numbers). T(n,1)=A114277(n-2). Sum(k*T(n,k),k\u003e=0)=A000531(n-1). For all double rises (above, below and on the x-axis), see A118963."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A118964/b118964.txt\"\u003eRows n = 1..141, flattened\u003c/a\u003e"
			],
			"formula": [
				"G.f.: G(t,z)=(1+r)/[1-z(1+r)C]-1, where r=r(t,z) is the Narayana function, defined by (1+r)(1+tr)z=r, r(t,0)=0 and C=C(z)=[1-sqrt(1-4z)]/(2z) is the Catalan function. More generally, the g.f. H=H(t,s,u,z), where t,s and u mark double rises above, below and on the x-axis, respectively, is H=[1+r(s,z)]/[1-z(1+tr(t,z))(1+ur(s,z))]."
			],
			"example": [
				"T(3,1) = 5 because we have u/ududd,u/uddud,udu/udd,duu/udd and u/udddu (the double rises above the x-axis are indicated by /.",
				"Triangle starts:",
				"2;",
				"5,   1;",
				"14,  5,  1;",
				"42,  19, 8,  1;",
				"132, 67, 40, 12, 1;"
			],
			"maple": [
				"C:=(1-sqrt(1-4*z))/2/z: r:=(1-z-t*z-sqrt(z^2*t^2-2*z^2*t-2*z*t+z^2-2*z+1))/2/t/z: G:=(1+r)/(1-z*C*(1+r))-1: Gser:=simplify(series(G,z=0,15)): for n from 1 to 11 do P[n]:=coeff(Gser,z,n) od: for n from 1 to 11 do seq(coeff(P[n],t,j),j=0..n-1) od; # yields sequence in triangular form",
				"# second Maple program:",
				"b:= proc(x, y, t) option remember; `if`(abs(y)\u003ex, 0,",
				"      `if`(x=0, 1, expand(`if`(t=2, z, 1)*b(x-1, y+1,",
				"      `if`(y\u003e=0, min(t+1, 2), 1)) +b(x-1, y-1, 1))))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p,z,i), i=0..n-1))(b(2*n, 0, 1)):",
				"seq(T(n), n=1..12);  # _Alois P. Heinz_, Jun 16 2014"
			],
			"mathematica": [
				"b[x_, y_, t_] := b[x, y, t] = If[Abs[y] \u003e x, 0, If[x == 0, 1, Expand[If[t == 2, z, 1]*b[x-1, y+1, If[y \u003e= 0, Min[t+1, 2], 1]] + b[x-1, y-1, 1]]]]; T[n_] := Function[ {p}, Table[Coefficient[p, z, i], {i, 0, n-1}]][b[2*n, 0, 1]]; Table[T[n], {n, 1, 12}] // Flatten (* _Jean-François Alcover_, Feb 19 2015, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A000984, A000108, A114277, A000531, A118963."
			],
			"keyword": "nonn,tabl",
			"offset": "1,1",
			"author": "_Emeric Deutsch_, May 07 2006",
			"ext": [
				"Keyword tabf changed to tabl by _Michel Marcus_, Apr 07 2013"
			],
			"references": 2,
			"revision": 16,
			"time": "2018-07-18T19:13:09-04:00",
			"created": "2006-05-19T03:00:00-04:00"
		}
	]
}