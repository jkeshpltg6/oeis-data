{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A190146",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 190146,
			"data": "2,3,3,0,0,9",
			"name": "Decimal expansion of Sum_{k\u003e=2} (1/Sum_{j=2..k} j'), where n' is the arithmetic derivative of n.",
			"comment": [
				"Slow convergence.",
				"a(7) is likely either 3 or 4. Is there a simple proof that this sum converges? - _Nathaniel Johnston_, May 24 2011",
				"It is apparent that the difference between this sum and the m-th partial sum (i.e., Sum_{k=2..m} (1/Sum_{j=2..k} j')) converges to C/m where C = 2.58679783..., so the convergence of the partial sums can be accelerated by a simple extrapolation (see Example section). - _Jon E. Schoenfield_, Jan 01 2019"
			],
			"example": [
				"1/2' + 1/(2'+3') + 1/(2'+3'+4') + 1/(2'+3'+4'+5') + ... = 1 + 1/2 + 1/6 + 1/7 + ... = 2.33009...",
				"From _Jon E. Schoenfield_, Jan 01 2019: (Start)",
				"Let S(m) be the m-th partial sum, i.e., S(m) = Sum_{k=2..m} (1/Sum_{j=2..k} j'), so lim_{m-\u003einf} S(m) = A is the constant whose decimal digits are the terms of this sequence. It appears that m*(A - S(m)) approaches a constant fairly quickly, so an extrapolation such as S'(2*m) = 2*S(2*m) - S(m) allows a significant acceleration of the convergence of the partial sums.",
				"Letting f(d) = S(2^d) and evaluating f(d) at d = 0, 1, 2, ..., 29 gives the following results:",
				"                                               (f(d)-f(d-1))",
				"   d         f(d)            2*f(d) - f(d-1)       * 2^d",
				"  == ==================== ==================== =============",
				"   0 0.000000000000000000 -------------------- -------------",
				"   1 1.000000000000000000 2.000000000000000000 2.00000000000",
				"   2 1.666666666666666667 2.333333333333333333 2.66666666667",
				"   3 2.009780219780219780 2.352893772893772894 2.74490842491",
				"   4 2.168641115495430913 2.327502011210642045 2.54177433144",
				"   5 2.249470661422227179 2.330300207349023445 2.58654546966",
				"   6 2.289733935307516488 2.329997209192805797 2.57684952866",
				"   7 2.309907327553392838 2.330080719799269188 2.58219420747",
				"   8 2.319997440119766296 2.330087552686139754 2.58306881699",
				"   9 2.325043274992913392 2.330089109866060489 2.58346745505",
				"  10 2.327568044310015805 2.330092813627118217 2.58536378071",
				"  11 2.328830986923564300 2.330093929537112796 2.58650647255",
				"  12 2.329462418954684871 2.330093850985805442 2.58634559947",
				"  13 2.329778172138129318 2.330093925321573765 2.58665007878",
				"  14 2.329936056061788876 2.330093939985448435 2.58677020524",
				"  15 2.330014997129428390 2.330093938197067903 2.58674090441",
				"  16 2.330054468293665605 2.330093939457902821 2.58678221945",
				"  17 2.330074203909579733 2.330093939525493861 2.58678664910",
				"  18 2.330084071740119642 2.330093939570659550 2.58679256905",
				"  19 2.330089005663740362 2.330093939587361083 2.58679694726",
				"  20 2.330091472624831103 2.330093939585921843 2.58679619269",
				"  21 2.330092706105927748 2.330093939587024393 2.58679734879",
				"  22 2.330093322846521057 2.330093939587114365 2.58679753748",
				"  23 2.330093631216832915 2.330093939587144773 2.58679766502",
				"  24 2.330093785401996210 2.330093939587159506 2.58679778860",
				"  25 2.330093862494578594 2.330093939587160977 2.58679781329",
				"  26 2.330093901040869895 2.330093939587161197 2.58679782067",
				"  27 2.330093920314015649 2.330093939587161402 2.58679783443",
				"  28 2.330093929950588520 2.330093939587161391 2.58679783287",
				"  29 2.330093934768874959 2.330093939587161398 2.58679783490",
				"(End)"
			],
			"maple": [
				"with(numtheory);",
				"P:=proc(i)",
				"local a,b,f,n,p,pfs;",
				"a:=0; b:=0;",
				"for n from 2 to i do",
				"  pfs:=ifactors(n)[2];",
				"  f:=n*add(op(2,p)/op(1,p),p=pfs);",
				"  b:=b+f; a:=a+1/b;",
				"od;",
				"print(evalf(a,300));",
				"end:",
				"P(1000);"
			],
			"mathematica": [
				"digits = 5; d[0] = d[1] = 0; d[n_] := d[n] = n*Total[Apply[#2/#1 \u0026, FactorInteger[n], {1}]]; p[m_] := p[m] = Sum[1/Sum[d[j], {j, 2, k}], {k, 2, m}] // RealDigits[#, 10, digits]\u0026 // First; p[digits]; p[m = 2*digits]; While[Print[\"p(\", m, \") = \", p[m]]; p[m] != p[m/2], m = 2*m]; p[m] (* _Jean-François Alcover_, Feb 21 2014 *)"
			],
			"xref": [
				"Cf. A003415, A190144, A190145, A190147."
			],
			"keyword": "nonn,more,cons",
			"offset": "1,1",
			"author": "_Paolo P. Lava_, May 05 2011",
			"ext": [
				"a(6) corrected and a(7) removed by _Nathaniel Johnston_, May 24 2011"
			],
			"references": 5,
			"revision": 25,
			"time": "2019-01-01T07:08:08-05:00",
			"created": "2011-05-13T21:42:10-04:00"
		}
	]
}