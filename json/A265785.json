{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A265785",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 265785,
			"data": "2,3,5,13,73,89,233,1597,40823,67273",
			"name": "Denominators of lower primes-only best approximates (POBAs) to sqrt(5); see Comments.",
			"comment": [
				"Suppose that x \u003e 0. A fraction p/q of primes is a lower primes-only best approximate, and we write \"p/q is in L(x)\", if u/v \u003c p/q \u003c x \u003c p'/q for all primes u and v such that v \u003c q, where p' is least prime \u003e p.",
				"Let q(1) be the least prime q such that u/q \u003c x for some prime u, and let p(1) be the greatest such u. The sequence L(x) follows inductively: for n \u003e 1, let q(n) is the least prime q such that p(n)/q(n) \u003c p/q \u003c x for some prime p. Let q(n+1) = q and let p(n+1) be the greatest prime p such that p(n)/q(n) \u003c p/q \u003c x.",
				"For a guide to POBAs, lower POBAs, and upper POBAs, see A265759."
			],
			"example": [
				"The lower POBAs to sqrt(5) start with 3/2, 5/3, 11/5, 29/13, 163/73, 199/89, 521/233. For example, if p and q are primes and q \u003e 73, and p/q \u003c sqrt(5), then 163/73 is closer to sqrt(5) than p/q is."
			],
			"mathematica": [
				"x = Sqrt[5]; z = 1000; p[k_] := p[k] = Prime[k];",
				"t = Table[Max[Table[NextPrime[x*p[k], -1]/p[k], {k, 1, n}]], {n, 1, z}];",
				"d = DeleteDuplicates[t]; tL = Select[d, # \u003e 0 \u0026] (* lower POBA *)",
				"t = Table[Min[Table[NextPrime[x*p[k]]/p[k], {k, 1, n}]], {n, 1, z}];",
				"d = DeleteDuplicates[t]; tU = Select[d, # \u003e 0 \u0026] (* upper POBA *)",
				"v = Sort[Union[tL, tU], Abs[#1 - x] \u003e Abs[#2 - x] \u0026];",
				"b = Denominator[v]; s = Select[Range[Length[b]], b[[#]] == Min[Drop[b, # - 1]] \u0026];",
				"y = Table[v[[s[[n]]]], {n, 1, Length[s]}] (* POBA, A265782/A265783 *)",
				"Numerator[tL]   (* A265784 *)",
				"Denominator[tL] (* A265785 *)",
				"Numerator[tU]   (* A265786 *)",
				"Denominator[tU] (* A265787 *)",
				"Numerator[y]    (* A222588 *)",
				"Denominator[y]  (* A265789 *)"
			],
			"xref": [
				"Cf. A000040, A265759, A265784, A265786, A265787, A265788, A265789."
			],
			"keyword": "nonn,frac,more",
			"offset": "1,1",
			"author": "_Clark Kimberling_, Dec 23 2015",
			"ext": [
				"a(9)-a(10) from _Robert Price_, Apr 05 2019"
			],
			"references": 7,
			"revision": 10,
			"time": "2019-04-06T01:09:27-04:00",
			"created": "2015-12-29T04:22:49-05:00"
		}
	]
}