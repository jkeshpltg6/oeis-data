{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A047949",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 47949,
			"data": "0,0,1,2,1,4,5,4,7,8,7,10,9,8,13,14,13,12,17,16,19,20,19,22,21,20,25,24,23,28,29,28,27,32,31,34,35,34,33,38,37,40,39,38,43,42,41,30,47,46,49,50,49,52,53,52,55,54,53,48,51,50,45,62,61,64,63,62,67,68,67,66",
			"name": "a(n) is the largest m such that n-m and n+m are both primes, or -1 if no such m exists.",
			"comment": [
				"A067076 is a subsequence of this sequence: when 2m+3 is prime a(m+3) = m. Moreover, it is the subsequence of records (maximal increasing subsequence): let m=a(n), with p=n-m and q=p+2m both odd primes \u003e 3; now 3+2(m+(p-3)/2)=q and hence a(3+m+(p-3)/2) \u003e= m+(p-3)/2 \u003e m = a(n) but 3+m+(p-3)/2 \u003c n. - _Jason Kimberley_, Aug 30 2012 and Oct 10 2012",
				"Goldbach's conjecture says a(n) \u003e= 0 for all n. - _Robert Israel_, Apr 15 2015",
				"a(n) is the Goldbach partition of 2n which results in the maximum spread divided by 2. - _Robert G. Wilson v_, Jun 18 2018"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A047949/b047949.txt\"\u003eTable of n, a(n) for n = 2..10000\u003c/a\u003e",
				"OEIS (Plot 2), \u003ca href=\"/plot2a?name1=A067076\u0026amp;name2=A098090\u0026amp;tform1=untransformed\u0026amp;tform2=untransformed\u0026amp;shift=0\u0026amp;radiop1=xy\u0026amp;drawpoints=true\"\u003eA067076 vs A098090\u003c/a\u003e (n-m=3). - _Jason Kimberley_, Oct 01 2012"
			],
			"formula": [
				"a(n) = n - A020481(n).",
				"a(n) = (A020482(n) - A020481(n))/2. - _Gionata Neri_, Apr 15 2015"
			],
			"example": [
				"49-30=19 and 49+30=79 are primes, so a(49)=30."
			],
			"maple": [
				"a:= proc(n)",
				"local k;",
				"  for k from n - 1 to 0 by -2 do",
				"     if isprime(n+k) and isprime(n-k) then return(k) fi",
				"od:",
				"-1",
				"end proc:",
				"0, seq(a(n),n=3..1000); # _Robert Israel_, Apr 16 2015"
			],
			"mathematica": [
				"a[2] = a[3] = 0; a[n_] := (For[m = n - 2, m \u003e= 0, m--, If[PrimeQ[n - m] \u0026\u0026 PrimeQ[n + m], Break[]]]; m); Table[a[n], {n, 2, 100}] (* _Jean-François Alcover_, Sep 04 2013 *)",
				"lm[n_]:=Module[{m=n-2},While[!AllTrue[n+{m,-m},PrimeQ],m--];m]; Join[{0,0}, Array[ lm,70,4]] (* The program uses the AllTrue function from Mathematica version 10 *) (* _Harvey P. Dale_, Dec 03 2014 *)",
				"f[n_] := Block[{q = 2}, While[q \u003c= n \u0026\u0026 !PrimeQ[2n -q], q = NextPrime@ q]; n - q]; Array[f, 72, 2] (* _Robert G. Wilson v_, Jun 18 2018 *)"
			],
			"program": [
				"(PARI) a(n) = {if (n==2 || n==3, return (0)); my(m = 1, lastm = -1, do = 1); while (do, if (isprime(n-m) \u0026\u0026 isprime(n+m), lastm = m); m++; if (m == n - 1, do = 0);); return (lastm);} \\\\ _Michel Marcus_, Jun 09 2013",
				"(PARI) a(n)=if(n\u003c4,0,forprime(p=3,n-1,if(isprime(2*n-p),return(n-p)));-1) \\\\ _Ralf Stephan_, Dec 29 2013",
				"(Haskell)",
				"a047949 n = if null qs then -1 else head qs  where",
				"   qs = [m | m \u003c- [n, n-1 .. 0], a010051' (n+m) == 1, a010051' (n-m) == 1]",
				"-- _Reinhard Zumkeller_, Nov 02 2015"
			],
			"xref": [
				"Cf. A047160, A067076, A182138.",
				"Cf. A020481.",
				"Cf. A010051."
			],
			"keyword": "easy,nice,nonn",
			"offset": "2,4",
			"author": "_Lior Manor_",
			"ext": [
				"Corrected by _Harvey P. Dale_, Dec 21 2000"
			],
			"references": 7,
			"revision": 58,
			"time": "2018-07-01T14:46:03-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}