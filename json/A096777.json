{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A096777",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 96777,
			"data": "1,2,3,5,8,11,15,20,25,31,38,45,53,62,71,81,92,103,115,128,141,155,170,185,201,218,235,253,272,291,311,332,353,375,398,421,445,470,495,521,548,575,603,632,661,691,722,753,785,818,851,885,920,955,991,1028",
			"name": "a(n) = a(n-1) + Sum_{k=1..n-1}(a(k) mod 2), a(1) = 1.",
			"comment": [
				"a(n+1) - a(n) = A004396(n).",
				"a(n) = a(n-1) + (number of odd terms so far in the sequence). Example: 15 is 11 + 4 odd terms so far in the sequence (they are 1,3,5,11). See A007980 for the same construction with even integers. - _Eric Angelini_, Aug 05 2007",
				"A016789 and A032766 give positions where even and odd terms occur; a(3*n)=A056106(n); a(3*n-1)=A077588(n); a(3*n-2)=A056108(n). - _Reinhard Zumkeller_, Dec 29 2007"
			],
			"link": [
				"R. Zumkeller, \u003ca href=\"/A096777/b096777.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"J.-L. Baril, T. Mansour, A. Petrossian, \u003ca href=\"http://jl.baril.u-bourgogne.fr/equival.pdf\"\u003eEquivalence classes of permutations modulo excedances\u003c/a\u003e, 2014.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/OddNumber.html\"\u003eOdd Number\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,-1,1,-2,1)."
			],
			"formula": [
				"a(n) = floor(n/3) * (3*floor(n/3) + 2*(n mod 3) - 1) + n mod 3 + 0^(n mod 3). - _Reinhard Zumkeller_, Dec 29 2007",
				"a(n) = floor((n-2)^2/3) + n. - _Christopher Hunt Gribble_, Mar 06 2014",
				"G.f.: -x*(x^4+1) / ((x-1)^3*(x^2+x+1)). - _Colin Barker_, Mar 07 2014",
				"Euler transform of finite sequence [2, 0, 1, 1, 0, 0, 0, -1]. - _Michael Somos_, Apr 18 2020"
			],
			"example": [
				"G.f. = x + 2*x^2 + 3*x^3 + 5*x^4 + 8*x^5 + 11*x^6 + 15*x^7 + 20*x^8 + ... - _Michael Somos_, Apr 18 2020"
			],
			"maple": [
				"A096777:=n-\u003en + floor((n-2)^2/3); seq(A096777(n), n=1..100); # _Wesley Ivan Hurt_, Mar 06 2014"
			],
			"mathematica": [
				"Table[n + Floor[(n-2)^2/3], {n, 100}] (* _Wesley Ivan Hurt_, Mar 06 2014 *)"
			],
			"program": [
				"(PARI) a(n)=(n-2)^2\\3+n \\\\ _Charles R Greathouse IV_, Mar 06 2014",
				"(Haskell)",
				"a096777 n = a096777_list !! (n-1)",
				"a096777_list = 1 : zipWith (+) a096777_list",
				"                               (scanl1 (+) (map (`mod` 2) a096777_list))",
				"-- _Reinhard Zumkeller_, Mar 11 2014",
				"(MAGMA) [Floor((n-2)^2/3)+n: n in [1..60]]; // _Vincenzo Librandi_, Dec 27 2015"
			],
			"xref": [
				"Cf. A131093, A097602."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Reinhard Zumkeller_, Jul 09 2004",
			"references": 9,
			"revision": 35,
			"time": "2020-04-18T21:03:36-04:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}