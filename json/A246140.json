{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A246140",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 246140,
			"data": "1,2,1,1,2,1,2,1,2,1,1,2,1,2,1,1,2,1,2,1,2,1,1,2,1,2,1,1,2,1,2,1,1,2,1,2,1,2,1,1,2,1,2,1,1,2,1,2,1,2,1,1,2,1,2,1,1,2,1,2,1,1,2,1,2,1,2,1,1,2,1,2,1,1,2,1,2,1,2,1,1,2,1,2,1,1",
			"name": "Limiting block extension of  A006337 (difference sequence of the Beatty sequence for sqrt(2)) with first term as initial block.",
			"comment": [
				"Suppose S = (s(0), s(1), s(2), ...) is an infinite sequence such that every finite block of consecutive terms occurs infinitely many times in S.  (It is assumed that A006337 is such a sequence.)  Let B = B(m,k) = (s(m), s(m+1),...s(m+k)) be such a block, where m \u003e= 0 and k \u003e= 0.  Let m(1) be the least i \u003e m such that (s(i), s(i+1),...,s(i+k)) = B(m,k), and put B(m(1),k+1) = (s(m(1)), s(m(1)+1),...s(m(1)+k+1)).  Let m(2) be the least i \u003e m(1) such that (s(i), s(i+1),...,s(i+k)) = B(m(1),k+1), and put B(m(2),k+2) = (s(m(2)), s(m(2)+1),...s(m(2)+k+2)).  Continuing in this manner gives a sequence of blocks B'(n) = B(m(n),k+n), so that for n \u003e= 0, B'(n+1) comes from B'(n) by suffixing a single term; thus the limit of B'(n) is defined; we call it the \"limiting block extension of S with initial block B(m,k)\", denoted by S^ in case the initial block is s(0).",
				"The sequence (m(i)), where m(0) = 0, is the \"index sequence for limit-block extending S with initial block B(m,k)\", as in A246128.  If the sequence S is given with offset 1, then the role played by s(0) in the above definitions is played by s(1) instead, as in the case of A246140 and A246141.",
				"Limiting block extensions are analogous to limit-reverse sequences, S*, defined at A245920.  The essential difference is that S^ is formed by extending each new block one term to the right, whereas S* is formed by extending each new block one term to the left (and then reversing)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A246140/b246140.txt\"\u003eTable of n, a(n) for n = 1..550\u003c/a\u003e"
			],
			"example": [
				"S = A006337, with B = (s(1)); that is, (m,k) = (1,0)",
				"S = (1, 2, 1, 2, 1, 1, 2, 1, 2, 1, 1, 2, 1, 2,...)",
				"B'(0) = (1)",
				"B'(1) = (1,2)",
				"B'(2) = (1,2,1)",
				"B'(3) = (1,2,1,1)",
				"B'(4) = (1,2,1,1,2)",
				"B'(5) = (1,2,1,1,2,1)",
				"S^ = (1, 2, 1, 1, 2, 1, 2, 1, 2, 1, 1, 2, 1,...),",
				"with index sequence (1,3,6,8,15,...)"
			],
			"mathematica": [
				"seqPosition1[list_, seqtofind_] := If[Length[#] \u003e Length[list], {}, Last[Last[      Position[Partition[list, Length[#], 1], Flatten[{___, #, ___}], 1, 1]]]] \u0026[seqtofind]; s =  Differences[Table[Floor[n*Sqrt[2]], {n, 10000}]]; Take[s, 60]",
				"t = {{1}}; p[0] = seqPosition1[s, Last[t]]; s = Drop[s, p[0]]; Off[Last::nolast]; n = 1; While[(p[n] = seqPosition1[s, Last[t]]) \u003e 0, (AppendTo[t, Take[s, {#, # +Length[Last[t]]}]]; s = Drop[s, #]) \u0026[p[n]]; n++]; On[Last::nolast]; Last[t] (* A246140 *)",
				"Accumulate[Table[p[k], {k, 0, n - 1}]] (* A246141 *)"
			],
			"xref": [
				"Cf. A246141, A246127, A246142, A246144, A246146, A006337."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Clark Kimberling_ and _Peter J. C. Moses_, Aug 17 2014",
			"references": 5,
			"revision": 15,
			"time": "2018-07-27T09:36:28-04:00",
			"created": "2014-08-22T10:20:13-04:00"
		}
	]
}