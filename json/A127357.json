{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A127357",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 127357,
			"data": "1,2,-5,-28,-11,230,559,-952,-6935,-5302,51811,151340,-163619,-1689298,-1906025,11391632,39937489,-22649710,-404736821,-605626252,2431378885,10313394038,-1255621889",
			"name": "Expansion of 1/(1 - 2*x + 9*x^2).",
			"comment": [
				"Hankel transform of A100193. A member of the family of sequences with g.f. 1/(1-2*x+r^2*x^2) which are the Hankel transforms of the sequences given by Sum_{k=0..n} binomial(2*n,k)*r^(n-k).",
				"From _Peter Bala_, Apr 01 2018: (Start)",
				"With offset 1, this is the Lucas sequence U(n,2,9). The companion Lucas sequence V(n,2,9) is 2*A025172(n).",
				"Define a binary operation o on rational numbers by x o y = (x + y)/(1 - 2*x*y). This is a commutative and associative operation with identity 0. Then 2 o 2 o ... o 2 (n terms) = 2*A127357(n-1)/A025172(n). Cf. A088137 and A087455. (End)"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A127357/b127357.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Lucas_sequence\"\u003eLucas sequence\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (2,-9)"
			],
			"formula": [
				"a(n) = Sum_{k=0..n} binomial(n-k,k)*2^(n-2*k)*(-9)^k.",
				"a(n) = 2*a(n-1) - 9*a(n-2) for n \u003e= 2. - _Vincenzo Librandi_, Mar 22 2011",
				"a(n) = ((1-2*sqrt(2)*i)^n-(1+2*sqrt(2)*i)^n)*i/(4*sqrt(2)), where i=sqrt(-1). - _Bruno Berselli_, Jul 01 2011",
				"From _Vladimir Reshetnikov_, Oct 15 2016: (Start)",
				"a(n) = 3^n*(cos(n*theta) + sin(n*theta)*sqrt(2)/4), theta = arctan(2*sqrt(2)).",
				"E.g.f.: exp(x)*(cos(2*sqrt(2)*x) + sin(2*sqrt(2)*x)*sqrt(2)/4). (End)",
				"a(n) = 2^n*Product_{k=1..n}(1 + 3*cos(k*Pi/(n+1))). - _Peter Luschny_, Nov 28 2019"
			],
			"maple": [
				"c := 2*sqrt(2): g := exp(x)*(sin(c*x)+c*cos(c*x))/c: ser := series(g,x,32):",
				"seq(n!*coeff(ser,x,n), n=0..22); # _Peter Luschny_, Oct 19 2016"
			],
			"mathematica": [
				"Join[{a=1,b=2},Table[c=2*b-9*a;a=b;b=c,{n,60}]] (* _Vladimir Joseph Stephan Orlovsky_, Jan 18 2011*)",
				"RootReduce@Table[3^n (Cos[n ArcTan[2 Sqrt[2]]] + Sin[n ArcTan[2 Sqrt[2]]] Sqrt[2]/4), {n, 0, 20}] (* _Vladimir Reshetnikov_, Oct 15 2016 *)"
			],
			"program": [
				"(Sage) [lucas_number1(n,2,9) for n in range(1, 24)] # _Zerinvary Lajos_, Apr 23 2009",
				"(MAGMA) m:=23; R\u003cx\u003e:=PowerSeriesRing(Integers(), m); Coefficients(R!(1/(1-2*x+9*x^2))); // _Bruno Berselli_, Jul 01 2011",
				"(Maxima) makelist(coeff(taylor(1/(1-2*x+9*x^2), x, 0, n), x, n), n, 0, 22); /* _Bruno Berselli_, Jul 01 2011 */",
				"(PARI) Vec(1/(1-2*x+9*x^2)+O(x^99)) \\\\ _Charles R Greathouse IV_, Sep 26 2012",
				"(GAP) a:=[1,2];; for n in [3..25] do a[n]:=2*a[n-1]-9*a[n-2]; od; a; # _Muniru A Asiru_, Oct 23 2018"
			],
			"xref": [
				"Variant is A025170.",
				"Cf. A025172, A088137, A087455."
			],
			"keyword": "sign,easy",
			"offset": "0,2",
			"author": "_Paul Barry_, Jan 11 2007",
			"references": 8,
			"revision": 49,
			"time": "2019-12-07T12:18:25-05:00",
			"created": "2007-01-12T03:00:00-05:00"
		}
	]
}