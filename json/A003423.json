{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003423",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3423,
			"id": "M4215",
			"data": "6,34,1154,1331714,1773462177794,3145168096065837266706434,9892082352510403757550172975146702122837936996354",
			"name": "a(n) = a(n-1)^2 - 2.",
			"comment": [
				"If x is either of the roots of x^2 - 6*x + 1 = 0 (i.e., x = 3 +- 2*sqrt(2)), then x^(2^n) + 1 = a(n)*x^(2^(n-1)). For example, x^8 + 1 = 1154*x^4. -_ James East_, Oct 05 2018"
			],
			"reference": [
				"L. E. Dickson, History of the Theory of Numbers. Carnegie Institute Public. 256, Washington, DC, Vol. 1, 1919; Vol. 2, 1920; Vol. 3, 1923, see vol. 1, p. 376.",
				"E. Lucas, \"Théorie des Fonctions Numériques Simplement Périodiques, II\", Amer. J. Math., 1 (1878), 289-321.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A003423/b003423.txt\"\u003eTable of n, a(n) for n = 0..10\u003c/a\u003e",
				"P. Liardet and P. Stambul, \u003ca href=\"http://www.numdam.org/item?id=JTNB_2000__12_1_37_0\"\u003eSeries d'Engel et fractions continuees\u003c/a\u003e, Journal de Théorie des Nombres de Bordeaux 12 (2000), 37-68.",
				"Jeffrey Shallit, \u003ca href=\"http://www.jstor.org/stable/2690344\"\u003eAn interesting continued fraction\u003c/a\u003e, Math. Mag., 48 (1975), 207-211.",
				"J. Shallit, \u003ca href=\"/A005248/a005248_1.pdf\"\u003eAn interesting continued fraction\u003c/a\u003e, Math. Mag., 48 (1975), 207-211. [Annotated scanned copy]",
				"J. Shallit \u0026 N. J. A. Sloane, \u003ca href=\"/A002949/a002949.pdf\"\u003eCorrespondence 1974-1975\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Engel_expansion\"\u003eEngel Expansion\u003c/a\u003e",
				"\u003ca href=\"/index/Aa#AHSL\"\u003eIndex entries for sequences of form a(n+1)=a(n)^2 + ...\u003c/a\u003e"
			],
			"formula": [
				"a(n) = ceiling(c^(2^n)) where c = 3 + 2*sqrt(2) is the largest root of x^2 - 6x + 1 = 0. - _Benoit Cloitre_, Dec 03 2002",
				"From _Paul D. Hanna_, Aug 11 2004: (Start)",
				"a(n) = (3+sqrt(8))^(2^n) + (3-sqrt(8))^(2^n).",
				"Sum_{n\u003e=0} 1/(Product_{k=0..n} a(k) ) = 3 - sqrt(8). (End)",
				"a(n) = 2*A001601(n+1).",
				"a(n-1) = Round((1 + sqrt(2))^(2^n)). - _Artur Jasinski_, Sep 25 2008",
				"a(n) = 2*T(2^n,3) where T(n,x) is the Chebyshev polynomial of the first kind. - _Leonid Bedratyuk_, Mar 17 2011",
				"Engel expansion of 3 - 2*sqrt(2). Thus 3 - 2*sqrt(2) = 1/6 + 1/(6*34) + 1/(6*34*1154) + .... See Liardet and Stambul. - _Peter Bala_, Oct 31 2012",
				"From _Peter Bala_, Nov 11 2012: (Start)",
				"4*sqrt(2)/7 = Product_{n\u003e=0} (1 - 1/a(n))",
				"sqrt(2) = Product_{n\u003e=0} (1 + 2/a(n)).",
				"a(n) - 1 = A145505(n+1). (End)"
			],
			"maple": [
				"a:= n-\u003e simplify(2*ChebyshevT(2^n, 3), 'ChebyshevT'):",
				"seq(a(n), n=0..7);"
			],
			"mathematica": [
				"a[1] := 6; a[n_] := a[n - 1]^2 - 2; Table[a[n], {n, 1, 8}] (* _Stefan Steinerberger_, Apr 11 2006 *)",
				"Table[Round[(1 + Sqrt[2])^(2^n)], {n, 1, 7}] (* _Artur Jasinski_, Sep 25 2008 *)",
				"NestList[#^2-2\u0026,6,10] (* _Harvey P. Dale_, Nov 11 2011 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c1, 6*(n==0), a(n-1)^2-2)"
			],
			"xref": [
				"Cf. A001566 (starting with 3), A003010 (starting with 4), A003487 (starting with 5).",
				"Cf. A145505."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_N. J. A. Sloane_",
			"references": 8,
			"revision": 80,
			"time": "2021-05-23T02:37:50-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}