{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005013",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5013,
			"id": "M3214",
			"data": "0,1,1,4,3,11,8,29,21,76,55,199,144,521,377,1364,987,3571,2584,9349,6765,24476,17711,64079,46368,167761,121393,439204,317811,1149851,832040,3010349,2178309,7881196,5702887,20633239,14930352,54018521,39088169,141422324",
			"name": "a(n) = 3*a(n-2) - a(n-4), a(0)=0, a(1)=1, a(2)=1, a(3)=4. Alternates Fibonacci (A000045) and Lucas (A000032) sequences for even and odd n.",
			"comment": [
				"S(n,sqrt(5)), with the Chebyshev polynomials A049310, is an integer sequence in the real quadratic number field Q(sqrt(5)) with basis numbers \u003c1,phi\u003e, phi:=(1+sqrt(5))/2. S(n,sqrt(5)) = A(n) + 2*B(n)*phi, with A(n)= a(n+1)*(-1)^n and B(n)= A147600(n-1), n\u003e=0, with A147600(-1):=0.",
				"a(n) = p(n+1) where p(x) is the unique degree-(n-1) polynomial such that p(k) = Fibonacci(k) for k = 1, ..., n. - _Michael Somos_, Jan 08 2012",
				"Row sums of A227431. - _Richard R. Forberg_, Jul 29 2013",
				"This is the sequence of Lehmer numbers u_n(sqrt(R),Q) with the parameters R = 5 and Q = 1. It is a strong divisibility sequence, that is, gcd(a(n), a(m)) = a(gcd(n,m)) for all natural numbers n and m. The sequence satisfies a linear recurrence of order four. - _Peter Bala_, Apr 18 2014",
				"The sequence of convergents of the 2-periodic continued fraction [0; 1, -5, 1, -5, ...] = 1/(1 - 1/(5 - 1/(1 - 1/(5 - ...)))) = (1/2)*(5 - sqrt(5)) begins [0/1, 1/1, 5/4, 4/3, 15/11, 11/8, 40/29, ...]; the denominators give the present sequence. The sequence of numerators [0, 1, 5, 4, 15, 11, 40, ...] is A203976. Cf. A108412 and A026741. - _Peter Bala_, May 19 2014",
				"Define a binary operation o on the real numbers by x o y = x*sqrt(1 + y^2) + y*sqrt(1 + x^2). The operation o is commutative and associative with identity 0. We have (1/2)*a(2*n + 1) = 1/2 o 1/2 o ... o 1/2 (2*n + 1 terms) and (1/2)*sqrt(5)* a(2*n) = 1/2 o 1/2 o ... o 1/2 (2*n terms). Cf. A084068 and A049629. - _Peter Bala_, Mar 23 2018"
			],
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A005013/b005013.txt\"\u003eTable of n, a(n) for n = 0..2000\u003c/a\u003e (terms 0..500 from T. D. Noe)",
				"A. F. Horadam, R. P. Loh and A. G. Shannon, \u003ca href=\"/A005013/a005013.pdf\"\u003eDivisibility properties of some Fibonacci-type sequences\u003c/a\u003e, pp. 55-64 of Combinatorial Mathematics VI (Armidale 1978), Lect. Notes Math. 748, 1979. [Annotated scanned copy]",
				"Seong Ju Kim, R. Stees and L. Taalman, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL19/Stees/stees4.html\"\u003eSequences of Spiral Knot Determinants\u003c/a\u003e, Journal of Integer Sequences, Vol. 19 (2016), #16.1.4.",
				"D. H. Lehmer, \u003ca href=\"http://www.jstor.org/stable/1968235\"\u003eAn extended theory of Lucas' functions\u003c/a\u003e, Annals of Mathematics, Second Series, Vol. 31, No. 3 (Jul., 1930), pp. 419-448.",
				"Simon Plouffe, \u003ca href=\"https://arxiv.org/abs/0911.4975\"\u003eApproximations de séries génératrices et quelques conjectures\u003c/a\u003e, Dissertation, Université du Québec à Montréal, 1992.",
				"Simon Plouffe, \u003ca href=\"/A000051/a000051_2.pdf\"\u003e1031 Generating Functions\u003c/a\u003e, Appendix to Thesis, Montreal, 1992.",
				"E. L. Roettger and H. C. Williams, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL24/Roettger/roettger12.html\"\u003eAppearance of Primes in Fourth-Order Odd Divisibility Sequences\u003c/a\u003e, J. Int. Seq., Vol. 24 (2021), Article 21.7.5.",
				"E. W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/LehmerNumber.html\"\u003eMathWorld: Lehmer Number\u003c/a\u003e",
				"H. C. Williams and R. K. Guy, \u003ca href=\"https://www.emis.de/journals/INTEGERS/papers/p33/p33.Abstract.html\"\u003eOdd and even linear divisibility sequences of order 4\u003c/a\u003e, INTEGERS, 2015, #A33.",
				"\u003ca href=\"/index/Di#divseq\"\u003eIndex to divisibility sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,3,0,-1)."
			],
			"formula": [
				"a(1) = a(2) = 1, a(3) = 4, a(n) = (a(n-1) * a(n-2) - 1) / a(n-3), unless n=3. a(-n) = -a(n).",
				"a(2n) = A001906(n), a(2n+1) = A002878(n). a(n)=F(n+1)+(-1)^(n+1)F(n-1). - Mario Catalani (mario.catalani(AT)unito.it), Sep 20 2002",
				"G.f.: x*(1+x+x^2)/((1-x-x^2)*(1+x-x^2)).",
				"a(n) = Product_{k=1..floor((n-1)/2)} (1 + 4*sin(k*Pi/n)^2). - _Roger L. Bagula_ and _Gary W. Adamson_, Nov 26 2008",
				"Binomial transform is A096140. - _Michael Somos_, Apr 13 2012",
				"From _Peter Bala_, Apr 18 2014: (Start)",
				"a(n) = (alpha^n - beta^n)/(alpha - beta) for n odd, and a(n) = (alpha^n - beta^n)/(alpha^2 - beta^2) for n even, where alpha = (1/2)*(sqrt(5) + 1) and beta = (1/2)*(sqrt(5) - 1). Equivalently, a(n) = U(n-1, sqrt(5)/2) for n odd and a(n) = (1/sqrt(5))*U(n-1, sqrt(5)/2) for n even, where U(n,x) is the Chebyshev polynomial of the second kind. (End)",
				"E.g.f.: (Phi/sqrt(5))*exp(-Phi*x)*(exp(x)-1)*(exp(sqrt(5)*x) - 1/(Phi)^2), where Phi = (1+sqrt(5))/2. - _G. C. Greubel_, Feb 08 2016",
				"a(n) = (5^floor((n-1)/2)/2^(n-1))*Sum_{k=0..n-1} binomial(n-1,k)/5^floor(k/2). - _Tony Foster III_, Oct 21 2018",
				"a(n) = hypergeom([(1 - n)/2, (n + 1) mod 2 - n/2], [1 - n], -4) for n \u003e= 2. - _Peter Luschny_, Sep 03 2019"
			],
			"example": [
				"G.f. = x + x^2 + 4*x^3 + 3*x^4 + 11*x^5 + 8*x^6 + 29*x^7 + 21*x^8 + 76*x^9 + ...",
				"a(3) = 4 since p(x) = (x^2 - 3*x + 4) / 2 interpolates p(1) = 1, p(2) = 1, p(3) = 2, and p(4) = 4. - _Michael Somos_, Jan 08 2012"
			],
			"maple": [
				"with(combinat): A005013 := n-\u003e if n mod 2 = 0 then fibonacci(n) else fibonacci(n+1)+fibonacci(n-1); fi;",
				"A005013:=z*(z**2+z+1)/((z**2+z-1)*(z**2-z-1)); # _Simon Plouffe_ in his 1992 dissertation"
			],
			"mathematica": [
				"CoefficientList[Series[(x + x^2 + x^3)/(1 - 3x^2 + x^4), {x, 0, 40}], x]",
				"f[n_] = Product[(1 + 4*Sin[k*Pi/n]^2), {k, 1, Floor[(n - 1)/2]}]; a = Table[f[n], {n, 0, 30}]; Round[a]; FullSimplify[ExpandAll[a]] (* _Roger L. Bagula_ and _Gary W. Adamson_, Nov 26 2008 *)",
				"LinearRecurrence[{0, 3, 0, -1}, {0, 1, 1, 4}, 100] (* _G. C. Greubel_, Feb 08 2016 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n%2, fibonacci(n+1) + fibonacci(n-1), fibonacci(n))}; /* _Michael Somos_, Jan 08 2012 */",
				"(PARI) {a(n) = if( n\u003c0, -a(-n), subst( polinterpolate( vector( n, k, fibonacci(k))), x, n+1))}; /* _Michael Somos_, Jan 08 2012 */",
				"(Haskell)",
				"a005013 n = a005013_list !! n",
				"a005013_list = alt a000045_list a000032_list where",
				"   alt (f:_:fs) (_:l:ls) = f : l : alt fs ls",
				"-- _Reinhard Zumkeller_, Jan 10 2012",
				"(MAGMA) I:=[0,1,1,4]; [n le 4 select I[n]  else 3*Self(n-2) - Self(n-4): n in [1..40]]; // _Vincenzo Librandi_, Feb 09 2016",
				"(GAP) a:=[0,1,1,4];; for n in [5..40] do a[n]:=3*a[n-2]-a[n-4]; od; a; # _Muniru A Asiru_, Oct 21 2018"
			],
			"xref": [
				"Cf. A000032, A000045, A001906, A002878, A005247, A096140, A026741, A108412, A084068, A049629."
			],
			"keyword": "nonn,easy",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Additional comments from _Michael Somos_, Jun 01 2000"
			],
			"references": 17,
			"revision": 116,
			"time": "2021-11-09T18:04:02-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}