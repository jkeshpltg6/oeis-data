{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A040976",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 40976,
			"data": "0,1,3,5,9,11,15,17,21,27,29,35,39,41,45,51,57,59,65,69,71,77,81,87,95,99,101,105,107,111,125,129,135,137,147,149,155,161,165,171,177,179,189,191,195,197,209,221,225,227,231,237,239,249,255,261",
			"name": "a(n) = prime(n) - 2.",
			"comment": [
				"Numbers n such that n! reduced mod (n+2) is 1. - _Benoit Cloitre_, Mar 11 2002",
				"The first a(n) numbers starting from 2 are divisible by primes up to p(n-1). - _Lekraj Beedassy_, Jun 21 2006",
				"The terms in this sequence are the cumulative sums of distances from one prime to another. For example for the distance from the first to 26th prime, 2 to 101, the cumulative sum of distances is 99, always the last prime, here 101, minus 2. - _Enoch Haga_, Apr 24 2006",
				"The primes in this sequence are the initial primes of pairs of twin primes. - _Sebastiao Antonio da Silva_, Dec 21 2008",
				"Note that many, but not all, of these numbers satisfy x such that x^(x+1) = 1 mod (x+2). The first exception is 339. - _Thomas Ordowski_, Nov 27 2013",
				"If this sequence had an infinite number of primes, the twin prime conjecture would follow. Sequence holds all primes in A001359. - _John W. Nicholson_, Apr 14 2014"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A040976/b040976.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"S. A. Khan, \u003ca href=\"http://arxiv.org/abs/1203.2083\"\u003ePrimes in Geometric-Arithmetic Progression\u003c/a\u003e, arXiv preprint arXiv:1203.2083 [math.NT], 2012."
			],
			"formula": [
				"a(n) = A000040(n) - 2 = Sum_{i=1..n-1} A001223(i).",
				"For n \u003e 2: A092953(a(n)) = 1. - _Reinhard Zumkeller_, Nov 10 2012"
			],
			"example": [
				"a(13) = 39, because A000040(13) = 41."
			],
			"maple": [
				"seq(ithprime(n)-2,n=1..2000); # _Muniru A Asiru_, Jan 31 2018"
			],
			"mathematica": [
				"Prime[Range[22]]-2 (* _Vladimir Joseph Stephan Orlovsky_, Apr 29 2008 *)"
			],
			"program": [
				"(Haskell)",
				"a040976 n = a000040 n - 2",
				"a040976_list = map (subtract 2) a000040_list",
				"-- _Reinhard Zumkeller_, Feb 22 2012",
				"(PARI) a(n)=prime(n)-2 \\\\ _Charles R Greathouse IV_, Nov 20 2012",
				"(MAGMA) [NthPrime(n)-2: n in [1..60]]; // _Vincenzo Librandi_, Jan 31 2018",
				"(GAP) Filtered([1..10^2],IsPrime)-2; # _Muniru A Asiru_, Jan 31 2018"
			],
			"xref": [
				"Cf. A000040, A001223, A014689, A014692."
			],
			"keyword": "nonn,nice,easy",
			"offset": "1,3",
			"author": "_Felice Russo_",
			"references": 55,
			"revision": 77,
			"time": "2018-02-13T02:46:15-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}