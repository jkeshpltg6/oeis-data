{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A346538",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 346538,
			"data": "1,1,1,7,3,7,29,11,11,29,173,72,25,72,173,937,382,108,108,382,937,5527,2295,803,241,803,2295,5527,32309,13391,4632,1152,1152,4632,13391,32309,193663,80677,29450,9132,2545,9132,29450,80677,193663",
			"name": "Table read by antidiagonals: T(n,k) is the number of paths in the Z X Z grid joining (0,0) and (n,k) each of whose steps increases the Euclidean distance to the origin and has coordinates with absolute value at most 1.",
			"formula": [
				"T(n,k) = T(k,n)."
			],
			"example": [
				"Array begins:",
				"     1,     1,     7,    29,    173,    937,   5527, ...",
				"     1,     3,    11,    72,    382,   2295,  13391, ...",
				"     7,    11,    25,   108,    803,   4632,  29450, ...",
				"    29,    72,   108,   241,   1152,   9132,  56043, ...",
				"   173,   382,   803,  1152,   2545,  12829, 106207, ...",
				"   937,  2295,  4632,  9132,  12829,  28203, 147239, ...",
				"  5527, 13391, 29450, 56043, 106207, 147239, 322681, ...",
				"  ...",
				"T(6,4) = T(5,3) + T(5,4) + T(5,5) + T(6,3) = 9132 + 12829 + 28203 + 56043 =106207.",
				"T(7,5) = T(6,4) + T(6,5) + T(6,6) + T(7,4).",
				"T(7,6) = T(6,6) + T(7,5) + T(6,5).",
				"T(0,5) = T(-1,4) + T(0,4) + T(1,4)."
			],
			"maple": [
				"T:= proc(n, k) option remember; `if`([n, k]=[0$2], 1, add(add(",
				"     `if`(i^2+j^2\u003cn^2+k^2, T(i, j), 0), j=k-1..k+1), i=n-1..n+1))",
				"    end:",
				"seq(seq(T(n, d-n), n=0..d), d=0..8);  # _Alois P. Heinz_, Sep 08 2021"
			],
			"mathematica": [
				"rodean[{m_, n_}] := Select[ Complement[ Flatten[Table[{m, n} + {s, t}, {s, -1, 1}, {t, -1, 1}], 1] // Union, {{m, n}}], #[[1]]^2 + #[[2]]^2 \u003c m^2 + n^2 \u0026];",
				"$RecursionLimit = 10^6; Clear[T]; T[{0, 0}] = 1;",
				"T[{m_, n_}] := T[{m, n}] = Sum[T[rodean[{m, n}][[i]]],{i,Length[rodean[{m,n}]]}] ;",
				"Table[T[{k, n - k}], {n, 0, 12}, {k, 0, n}] // Flatten",
				"(* Second program: *)",
				"T[n_, k_] := T[n, k] = If[{n, k} == {0, 0}, 1, Sum[Sum[If[i^2 + j^2 \u003c n^2 + k^2, T[i, j], 0], {j, k - 1, k + 1}], {i, n - 1, n + 1}]];",
				"Table[Table[T[n, d - n], {n, 0, d}], {d, 0, 8}] // Flatten (* _Jean-François Alcover_, Nov 03 2021, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Main diagonal gives A346539.",
				"Column (or row) k=0 gives A347814.",
				"Cf. A008288, A001850, A001263, A006318, A001006, A114486."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_José María Grau Ribas_, Jul 23 2021",
			"references": 2,
			"revision": 39,
			"time": "2021-11-03T08:38:44-04:00",
			"created": "2021-09-10T22:04:27-04:00"
		}
	]
}