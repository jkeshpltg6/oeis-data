{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A190904",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 190904,
			"data": "1,1,1,0,-3,-12,-27,0,441,3024,11529,0,-442827,-4390848,-23444883,0,1636819569,21224560896,145703137041,0,-16106380394643,-257991277243392,-2164638920874507,0,347592265948756521",
			"name": "a(n) = Sum_{k=0..n-1} cos(Pi*k/2)*binomial(n-1,k)*a(n-1-k)*a(k) for n \u003e 0, a(0) = 1.",
			"link": [
				"M. Abramowitz and I. A. Stegun, eds.,\u003ca href=\"http://people.math.sfu.ca/~cbm/aands/toc.htm\"\u003e Handbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972",
				"Eric W. Weisstein, \u003ca href=\"http://mathworld.wolfram.com/JacobiEllipticFunctions.html\"\u003eJacobi Elliptic Functions\u003c/a\u003e"
			],
			"formula": [
				"Let F(n,x) = Sum_{k=0..n-1} cos(Pi*k*x)*binomial(n-1,k)*F(n-1-k,x)* F(k,x), then",
				"F(n, 0)   = n! = A000142(n),",
				"F(n, 1/2) = a(n),",
				"F(n, 1)   = 2^n*Euler_{n}(1) = A_{n}(-1) = A155585(n).",
				"a(2*n) = A159601(n)*(-1)^floor((n-1)/2).",
				"a(2*n+1) = A104203(2*n+1).",
				"From Peter Bala, Aug 25 2011: (Start)",
				"The sequence entries may be calculated as follows: Define the nested derivative D^n[f](x) by means of the recursion D^0[f](x) = 1 and D^(n+1)[f](x) = d/dx(f(x)*D^n[f](x)) for n \u003e= 0. The coefficients in the expansion of D^n[f](x) in powers of f(x) can be found in A145271. Then we have",
				"a(2*n) = D^(2*n)[sqrt(1+sin^2(x))](0)",
				"a(2*n+1) = D^(2*n)[sqrt(1-x^4)](0).",
				"The generating function involves the Jacobian elliptic functions. Define E(u,k) := cn(i*u,k)-i*sn(i*u,k) = 1+u+u^2/2!+(1+k^2)*u^3/3!+(1+4*k^2)*u^4/4!+..., where cn(u,k) and sn(u,k) are Jacobian elliptic functions of modulus k (see A060627 and A060628). Then the e.g.f. A(u) for this sequence is",
				"A(u) := E(u,i) = 1+u+u^2/2!-3*u^4/4!-12*u^5/5!-27*u^6/6!+....",
				"Proof:",
				"Using well-known properties of the Jacobian elliptic functions (see for example Abramowitz and Stegun, Chapter 16) we find the generating function A(u) satisfies the differential equation",
				"(d/du)A(u) = dn(i*u,i)*A(u) = 1/2*(A(i*u)+A(-i*u))*A(u), which leads to a recurrence for the coefficients of A(u):",
				"a(n+1) = sum{k=0..floor(n/2)} (-1)^k*binomial(n,2*k)*a(2*k)*a(n-2*k) with a(0) = 1. This recurrence is equivalent to the defining recurrence for this sequence given above.",
				"End proof.",
				"The generating function A(u) satisfies 1/A(u) = A(-u).",
				"Compare entries of this sequence with those of A104203, A159600, A193541 and A193544.",
				"(End)"
			],
			"maple": [
				"A190904 := proc(n) option remember; `if`(n=0,1,add(((1-irem(k,2))*(-1)^ iquo(k,2))*binomial(n-1,k)*A190904(n-1-k)*A190904(k),k=0..n-1)) end:"
			],
			"mathematica": [
				"a[0] = 1;",
				"a[n_] := a[n] =",
				"  Sum[Mod[(k+1)^3, 4, -1] Binomial[n-1, k] a[n-k-1] a[k], {k, 0, n-1}];",
				"Table[a[n], {n, 0, 24}] (* _Jean-François Alcover_, Jun 24 2019 *)"
			],
			"xref": [
				"Cf. A060627, A060628, A104203, A159600, A193541, A193544."
			],
			"keyword": "sign",
			"offset": "0,5",
			"author": "_Peter Luschny_, Jul 26 2011",
			"references": 2,
			"revision": 17,
			"time": "2019-06-24T10:29:47-04:00",
			"created": "2011-07-27T04:19:42-04:00"
		}
	]
}