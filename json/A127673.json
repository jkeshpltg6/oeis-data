{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A127673",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 127673,
			"data": "1,1,1,1,4,3,1,6,15,10,1,8,28,56,35,1,10,45,120,210,126,1,12,66,220,495,792,462,1,14,91,364,1001,2002,3003,1716,1,16,120,560,1820,4368,8008,11440,6435,1,18,153,816,3060,8568,18564,31824,43758,24310,1,20,190",
			"name": "One half of even powers of 2*x in terms of Chebyshev's T-polynomials.",
			"comment": [
				"See A122366 for one half of odd powers of 2*x in terms of Chebyshev's T-polynomials.",
				"This is, for n \u003e= 1, the left half of Pascal's triangle for even rows with the central coefficients divided by 2.",
				"The signed version of this triangle, b(n,k) := a(n,k)*(-1)^(n-k), appears in the formula (1/2)*(2*sin(phi))^(2*n) = (Sum_{k=0..n-1} b(n,k)*cos(2*(n-k)*phi)) + a(n,n).",
				"Correspondingly, (1/2)*(4*(1-x^2))^n = (Sum_{k=0..n-1} b(n,k)*T(2*(n-k),x)) + a(n,n).",
				"The proofs follow from Euler's formula 2*x = 2*cos(phi) = exp(i*phi) + exp(-i*phi) or 2*sqrt(1-x^2) = 2*sin(phi) = (exp(i*phi) - exp(-i*phi))/i and the binomial formula."
			],
			"reference": [
				"Theodore J. Rivlin, Chebyshev polynomials: from approximation theory to algebra and number theory, 2. ed., Wiley, New York, 1990. pp. 54-55, Ex. 1.5.31."
			],
			"link": [
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards, Applied Math. Series 55, Tenth Printing, 1972 [alternative scanned copy].",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards Applied Math. Series 55, Tenth Printing, 1972, p. 795.",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e",
				"\u003ca href=\"/index/Pas#Pascal\"\u003eIndex entries for triangles and arrays related to Pascal's triangle\u003c/a\u003e"
			],
			"formula": [
				"a(n,k) = binomial(2*n,k), k=0..n-1 and a(n,n) = binomial(2*n,n)/2, n \u003e= 1. Instead of a(0,0)=1 one should take 1/2."
			],
			"example": [
				"[1/2];",
				"[ 1, 1];",
				"[ 1, 4,  3];",
				"[ 1, 6, 15, 10];",
				"[ 1, 8, 28, 56, 35];",
				"...",
				"Row n=3: [1, 6, 15, 20/2 = 10] appears in ((2*x)^6)/2 = 1*T(6,x) + 6*T(4,x) + 15*T(2,x) + 10.",
				"Row n=3: [1, 6, 15, 20/2 = 10] appears in ((2*cos(phi))^6)/2 = 1*cos(6*phi) + 6*cos(4*phi) + 15*cos(2*phi) + 10.",
				"The signed row n=3, [-1, 6, -15, +20/2 = 10], appears in ((4*(1-x^2))^3)/2 = -1*T(6,x) + 6*T(4,x) - 15*T(2,x) + 10).",
				"The signed row n=3, [-1, 6, -15, +20/2 = 10], appears in ((2*sin(phi))^6)/2 = -1*cos(6*phi) + 6*cos(4*phi) - 15*cos(2*phi) + 10."
			],
			"xref": [
				"Cf. A122366, A008314, A008311."
			],
			"keyword": "nonn,tabl,easy",
			"offset": "0,5",
			"author": "_Wolfdieter Lang_, Mar 07 2007",
			"references": 4,
			"revision": 15,
			"time": "2019-11-10T20:26:22-05:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}