{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A129401",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 129401,
			"data": "9,15,27,25,21,45,33,35,81,75,63,55,39,135,49,51,125,99,105,243,65,57,77,225,69,85,189,165,117,175,87,405,121,147,95,153,375,91,297,115,93,315,111,275,729,119,195,171,145,231,675,123,245,207,143,255,567,625",
			"name": "a(n) is the result of replacing with its successor prime each prime in the factorization of the n-th composite number.",
			"comment": [
				"Each odd composite number appears in the sequence exactly once. - _Jon E. Schoenfield_, Jun 05 2007",
				"Prime factors are used with multiplicity, e.g., the factors of 4 are 2 and 2, and both terms are replaced by 3, so a(1) = 3*3 = 9. - _Harvey P. Dale_, Mar 19 2013"
			],
			"link": [
				"Harvey P. Dale, \u003ca href=\"/A129401/b129401.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A003961(A002808(n)). - _Jon E. Schoenfield_, Jun 04 2007 [edited, at the suggestion of _Michel Marcus_, by _Jon E. Schoenfield_, Feb 18 2018]",
				"a(n) = A045965(A002808(n)). - _Ivan N. Ianakiev_, Feb 15 2018"
			],
			"example": [
				"a(19) = 105 because the factorization of the 19th composite number (i.e., 30) is 2*3*5 and replacing each prime factor with the next prime results in 3*5*7 which remultiplies to 105."
			],
			"mathematica": [
				"cnp[n_]:=Times@@(NextPrime/@Flatten[Table[#[[1]],{#[[2]]}]\u0026/@ FactorInteger[ n]]); With[{nn=100},cnp/@Complement[Range[2,nn],Prime[Range[PrimePi[nn]]]]] (* _Harvey P. Dale_, Mar 19 2013 *)"
			],
			"program": [
				"(PARI) lista(nn) = {forcomposite(c=1, nn, my (f = factor(c)); for (k=1, #f~, f[k,1] = nextprime(f[k,1]+1)); print1(factorback(f), \", \"););} \\\\ _Michel Marcus_, Feb 26 2018"
			],
			"xref": [
				"Cf. A002808 (composite numbers), A003961."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Ben Paul Thurston_, May 28 2007",
			"ext": [
				"More terms from _Jon E. Schoenfield_, Jun 05 2007",
				"Name edited by _Jon E. Schoenfield_, Feb 18 2018"
			],
			"references": 1,
			"revision": 25,
			"time": "2018-03-01T08:34:44-05:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}