{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A055642",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 55642,
			"data": "1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3",
			"name": "Number of digits in decimal expansion of n.",
			"comment": [
				"From _Hieronymus Fischer_, Jun 08 2012: (Start)",
				"For n\u003e0 the first differences of A117804.",
				"The total number of digits necessary to write down all the numbers 0, 1, 2, ... n is given by A117804(n+1). (End)",
				"Here a(0) = 1, but a different common convention is to consider that the expansion of 0 in any base b \u003e 0 has 0 terms and digits. - _M. F. Hasler_, Dec 07 2018"
			],
			"link": [
				"Charles R Greathouse IV, \u003ca href=\"/A055642/b055642.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e"
			],
			"formula": [
				"a(A046760(n)) \u003c A050252(A046760(n)); a(A046759(n)) \u003e A050252(A046759(n)). - _Reinhard Zumkeller_, Jun 21 2011",
				"a(n) = A196563(n) + A196564(n).",
				"a(n) = 1 + floor(log_10(n)) = 1 + A004216(n) = ceiling(log_10(n+1)) = A004218(n+1), if n \u003e= 1. - _Daniel Forgues_, Mar 27 2014",
				"a(A046758(n)) = A050252(A046758(n)). - _Reinhard Zumkeller_, Jun 21 2011",
				"a(n) = A117804((n+1) - A117804(n), n\u003e0. - _Hieronymus Fischer_, Jun 08 2012",
				"G.f.: g(x) = 1 + (1/(1-x))*sum_{j\u003e=0} x^(10^j). - _Hieronymus Fischer_, Jun 08 2012",
				"a(n) = A262190(n) for n \u003c 100; a(A262198(n)) != A262190(A262198(n)). - _Reinhard Zumkeller_, Sep 14 2015"
			],
			"example": [
				"Examples:",
				"999: 1 + floor(log_10(999)) = 1 + floor(2.x) = 1 + 2 = 3 or",
				"      ceiling(log_10(999+1)) = ceiling(log_10(1000)) = ceiling(3) = 3;",
				"1000: 1 + floor(log_10(1000)) = 1 + floor(3) = 1 + 3 = 4 or",
				"      ceiling(log_10(1000+1)) = ceiling(log_10(1001)) = ceiling(3.x) = 4;",
				"1001: 1 + floor(log_10(1001)) = 1 + floor(3.x) = 1 + 3 = 4 or",
				"      ceiling(log_10(1001+1)) = ceiling(log_10(1002)) = ceiling(3.x) = 4;"
			],
			"maple": [
				"A055642 := proc(n)",
				"        max(1,ilog10(n)+1) ;",
				"end proc:  # _R. J. Mathar_, Nov 30 2011"
			],
			"mathematica": [
				"Join[{1}, Array[ Floor[ Log[10, 10# ]] \u0026, 104]] (* _Robert G. Wilson v_, Jan 04 2006 *)",
				"Join[{1},Table[IntegerLength[n],{n,104}]]",
				"IntegerLength[Range[0,120]] (* _Harvey P. Dale_, Jul 02 2016 *)"
			],
			"program": [
				"(PARI) a(n)=#Str(n) \\\\ _M. F. Hasler_, Nov 17 2008",
				"(PARI) A055642(n)=logint(n+!n,10)+1 \\\\ Increasingly faster than the above, for larger n. (About twice as fast for n ~ 10^7.) - _M. F. Hasler_, Dec 07 2018",
				"(Haskell)",
				"a055642 :: Integer -\u003e Int",
				"a055642 = length . show  -- _Reinhard Zumkeller_, Feb 19 2012, Apr 26 2011",
				"(MAGMA) [ #Intseq(n): n in [0..105] ];   //  _Bruno Berselli_, Jun 30 2011",
				"(Common Lisp) (defun A055642 (n) (if (zerop n) 1 (floor (log n 10)))) ; _James Spahlinger_, Oct 13 2012"
			],
			"xref": [
				"Cf. A043537, A178788, A046034, A019546, A054899, A122840, A055640, A055641, A102669-A102685, A117804, A160093, A160094, A196563, A196564, A000120, A000788, A023416, A059015 (for base 2).",
				"Cf. A262190, A262198.",
				"Cf. A007953: sum of digits."
			],
			"keyword": "base,easy,nonn,nice",
			"offset": "0,11",
			"author": "_Henry Bottomley_, Jun 06 2000",
			"references": 406,
			"revision": 85,
			"time": "2021-10-29T10:07:38-04:00",
			"created": "2000-06-15T03:00:00-04:00"
		}
	]
}