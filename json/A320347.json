{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A320347",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 320347,
			"data": "1,1,2,2,3,3,5,6,6,9,11,10,15,18,19,24,31,29,40,44,51,56,72,69,90,97,114,125,154,151,192,207,237,255,304,314,377,401,457,493,573,596,698,750,845,905,1034,1104,1255,1354,1507,1624,1817,1955,2178,2357,2605,2794,3077,3380",
			"name": "Number of partitions of n into distinct parts (a_1, a_2, ... , a_m) (a_1 \u003e a_2 \u003e ... \u003e a_m and Sum_{k=1..m} a_k = n) such that a1 - a2, a2 - a3, ..., a_{m-1} - a_m are different.",
			"comment": [
				"In other words, a(n) is the number of strict integer partitions of n with distinct first differences. - _Gus Wiseman_, Mar 25 2021"
			],
			"link": [
				"Fausto A. C. Cariboni, \u003ca href=\"/A320347/b320347.txt\"\u003eTable of n, a(n) for n = 1..500\u003c/a\u003e (terms 1..100 from Seiichi Manyama)",
				"Gus Wiseman, \u003ca href=\"/A325325/a325325.txt\"\u003eSequences counting and ranking integer partitions by the differences of their successive parts.\u003c/a\u003e"
			],
			"example": [
				"n = 9",
				"[9]        ooooooooo",
				"------------------------------------",
				"[8, 1]      *******o  a_1 - a_2 = 7.",
				"            oooooooo",
				"------------------------------------",
				"[7, 2]       *****oo  a_1 - a_2 = 5.",
				"             ooooooo",
				"------------------------------------",
				"[6, 3]        ***ooo  a_1 - a_2 = 3.",
				"              oooooo",
				"------------------------------------",
				"[6, 2, 1]         *o  a_2 - a_3 = 1.",
				"              ****oo  a_1 - a_2 = 4.",
				"              oooooo",
				"------------------------------------",
				"[5, 4]         *oooo  a_1 - a_2 = 1.",
				"               ooooo",
				"------------------------------------",
				"a(9) = 6."
			],
			"mathematica": [
				"Table[Length[Select[IntegerPartitions[n],UnsameQ@@#\u0026\u0026UnsameQ@@Differences[#]\u0026]],{n,0,30}] (* _Gus Wiseman_, Mar 27 2021 *)"
			],
			"xref": [
				"Cf. A000009, A320348.",
				"The equal instead of distinct version is A049980.",
				"The non-strict version is A325325 (ranking: A325368).",
				"The non-strict ordered version is A325545.",
				"The version for first quotients is A342520 (non-strict: A342514).",
				"Cf. A003114, A003242, A005117, A238710, A342530."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Seiichi Manyama_, Oct 11 2018",
			"references": 3,
			"revision": 37,
			"time": "2021-03-27T22:46:09-04:00",
			"created": "2018-10-11T14:10:40-04:00"
		}
	]
}