{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A331265",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 331265,
			"data": "0,279,656,1139,1860,2883,4340,6419,9156,13299,19220,27683,39780,55719,79856,114359,163680,234183,327080,467759,668856,956319,1367240,1908683,2728620,3900699,5576156,7971179,11126940,15905883,22737260,32502539,46461756,64854879,92708600,132524783",
			"name": "Nonnegative values x of solutions (x, y) to the Diophantine equation x^2 + (x + 31^2)^2 = y^2.",
			"comment": [
				"For the generic case x^2 + (x + p^2)^2 = y^2 with p = 2*m^2 - 1 a (prime) number in A066436, m \u003e= 4 (means p \u003e= 31), the first five consecutive solutions are (0, p^2), (4*m^3+2*m^2-2*m-1, 4*m^4+4*m^3-2*m-1), (8*m^3+8*m^2+4*m, 4*m^4+8*m^3+12*m^2+4*m+1), (12*m^4-40*m^3+44*m^2-20*m+3, 20*m^4-56*m^3+60*m^2-28*m+5), (12*m^4-20*m^3+2*m^2+10*m-4, 20*m^4-28*m^3+14*m-5) and the other solutions are defined by (X(n), Y(n)) = (3*X(n-5) + 2*Y(n-5) + p^2, 4*X(n-5) + 3*Y(n-5) + 2*p^2).",
				"X(n) = 6*X(n-5) - X(n-10) + 2*p^2, and Y(n) = 6*Y(n-5) - Y(n-10) (can be easily proved using X(n) = 3*X(n-5) + 2*Y(n-5) + p^2, and Y(n) = 4*X(n-5) + 3*Y(n-5) + 2*p^2)."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A331265/b331265.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_11\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,0,0,0,6,-6,0,0,0,-1,1)."
			],
			"formula": [
				"a(n) = 6*a(n-5) - a(n-10) + 1922 for n \u003e= 11; a(1)=0, a(2)=279, a(3)=656, a(4)=1139, a(5)=1860, a(6)=2883, a(7)=4340, a(8)=6419, a(9)=9156, a(10)=13299.",
				"From _Colin Barker_, Feb 12 2020: (Start)",
				"G.f.: x^2*(279 + 377*x + 483*x^2 + 721*x^3 + 1023*x^4 - 217*x^5 - 183*x^6 - 161*x^7 - 183*x^8 - 217*x^9) / ((1 - x)*(1 - 6*x^5 + x^10)).",
				"a(n) = a(n-1) + 6*a(n-5) - 6*a(n-6) - a(n-10) + a(n-11) for n\u003e11.",
				"(End)"
			],
			"example": [
				"For p=31 (m=4) the first five (5) consecutive solutions are (0, 961), (279, 1271), (656, 1745), (1139, 2389), (1860, 3379)."
			],
			"mathematica": [
				"LinearRecurrence[{1, 0, 0, 0, 6, -6, 0, 0, 0, -1, 1}, {0, 279, 656, 1139, 1860, 2883, 4340, 6419, 9156, 13299, 19220}, 36] (* _Jean-François Alcover_, Feb 12 2020 *)"
			],
			"program": [
				"(MAGMA) I:=[0, 279, 656, 1139, 1860, 2883, 4340, 6419, 9156, 13299]; [n le 10 select I[n] else 6*Self(n-5) - Self(n-10)+1922: n in [1..100]];",
				"(PARI) concat(0, Vec(x^2*(279 + 377*x + 483*x^2 + 721*x^3 + 1023*x^4 - 217*x^5 - 183*x^6 - 161*x^7 - 183*x^8 - 217*x^9) / ((1 - x)*(1 - 6*x^5 + x^10)) + O(x^30))) \\\\ _Colin Barker_, Feb 12 2020"
			],
			"xref": [
				"Cf. A066436 (Primes of the form 2*m^2 - 1).",
				"Solutions x to x^2+(x+p^2)^2=y^2: A118554 (p=7), A207059 (p=17), A309998 (p=23), this sequence (p=31), A332000 (p=47)."
			],
			"keyword": "nonn,easy",
			"offset": "1,2",
			"author": "_Mohamed Bouhamida_, Feb 12 2020",
			"references": 2,
			"revision": 41,
			"time": "2020-03-18T14:04:22-04:00",
			"created": "2020-03-03T13:30:53-05:00"
		}
	]
}