{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A160256",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 160256,
			"data": "1,2,3,4,6,8,9,16,18,24,12,10,30,5,36,15,48,20,60,7,120,14,180,21,240,28,300,35,360,42,420,11,840,22,1260,33,1680,44,2100,55,2520,66,2940,77,3360,88,3780,110,378,165,126,220,63,440,189,880,567,1760",
			"name": "a(1)=1, a(2)=2. For n \u003e=3, a(n) = the smallest positive integer not occurring earlier in the sequence such that a(n)*a(n-1)/a(n-2) is an integer.",
			"comment": [
				"Is this sequence a permutation of the positive integers?",
				"a(n+2)*a(n+1)/a(n) = A160257(n).",
				"From _Alois P. Heinz_, May 07 2009: (Start)",
				"After computing about 10^7 elements of A160256 we have",
				"a(10000000) = 2099597439752627193722111679586865799879114417",
				"a(10000001) = 992131130100042530286371815859160",
				"Largest element so far:",
				"a(8968546) = 24941014474345046106920043019655502800839523254002490663461\\",
				"524119982890708516899294655028121578883343551450916846444559467340663409\\",
				"549447588184641816",
				"Still missing:",
				"19, 23, 27, 29, 31, 32, 37, 38, 41, 43, 45, 46, 47, 53, 54, 57, 58, 59,",
				"61, 62, 64, 67, 69, 71, 72, 73, 74, 76, 79, 81, 82, 83, 86, 87, 89, 90,",
				"92, 93, 94, 95, 96, 97, 101, 103, 105, 106, 107, 108, 109, 111, 112, 113,",
				"114, 115, 116, 118, 122, 123, 124, 125, 127, 128, 129, 131, 133, 134, ...",
				"Primes in sequence so far:",
				"2, 3, 5, 7, 11, 13, 17",
				"The sequence consists of two subsequences, even (=red) and odd (=blue), see plot. (End)",
				"a(n) is the least multiple of a(n-2)/gcd(a(n-1),a(n-2)) that has not previously occurred. - _Thomas Ordowski_, Jul 15 2015"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A160256/b160256.txt\"\u003eTable of n, a(n) for n = 1..130000\u003c/a\u003e",
				"Alois P. Heinz, \u003ca href=\"/A160256/a160256_plot.jpg\"\u003eColor plot of first 600 terms\u003c/a\u003e"
			],
			"maple": [
				"b:= proc(n) option remember; false end:",
				"a:= proc(n) option remember; local k, m;",
				"      if n\u003c3 then b(n):=true; n",
				"    else m:= denom(a(n-1)/a(n-2));",
				"         for k from m by m while b(k) do od;",
				"         b(k):= true; k",
				"      fi",
				"    end:",
				"seq(a(n), n=1..100); # _Alois P. Heinz_, May 16 2009"
			],
			"mathematica": [
				"f[s_List] := Block[{k = 1, m = Denominator[ s[[ -1]]/s[[ -2]]]}, While[ MemberQ[s, k*m] || Mod[k*m*s[[ -1]], s[[ -2]]] != 0, k++ ]; Append[s, k*m]]; Nest[f, {1, 2}, 56] (* _Robert G. Wilson v_, May 17 2009 *)"
			],
			"program": [
				"(PARI)",
				"LQ(nMax)={my(a1=1,a2=1,L=1/*least unseen number*/,S=[]/*used numbers above L*/);",
				"while(1, /*cleanup*/ while( setsearch(S,L),S=setminus(S,Set(L));L++);",
				"/*search*/ for(a=L,nMax, a*a2%a1 \u0026 next; setsearch(S,a) \u0026 next;",
				"print1(a\",\"); a1=a2; S=setunion(S,Set(a2=a)); next(2));return(L))} \\\\ _M. F. Hasler_, May 06 2009",
				"(PARI) L=10^4;a=vector(L);b=[1,2];a[1]=1;a[2]=2;sb=2;P2=2;pending=[];sp=0;for(n=3,L,if(issquare(n),b=vecsort(concat(b,pending));sb=n-1;while(sb\u003e=2*P2,P2*=2);sp=0;pending=[]);c=a[n-2]/gcd(a[n-2],a[n-1]);u=0;while(1,u+=c;found=0;s=0;pow2=P2;while(pow2,s2=s+pow2;if((s2\u003c=sb)\u0026\u0026(b[s2]\u003c=u),s=s2);pow2\\=2);if((s\u003e0)\u0026\u0026(b[s]==u),found=1,for(i=1,sp,if(pending[i]==u,found=1;break)));if(found==0,break));a[n]=u;pending=concat(pending,u);sp++);a \\\\ _Robert Gerbicz_, May 16 2009]",
				"(Haskell)",
				"import Data.List (delete)",
				"a160256 n = a160256_list !! (n-1)",
				"a160256_list = 1 : 2 : f 1 2 [3..] where",
				"   f u v ws = g ws where",
				"     g (x:xs) | mod (x * v) u == 0 = x : f v x (delete x ws)",
				"              | otherwise          = g xs",
				"-- _Reinhard Zumkeller_, Jan 31 2014",
				"(Python)",
				"from __future__ import division",
				"from fractions import gcd",
				"A160256_list, l1, l2, m, b = [1,2], 2, 1, 1, {1,2}",
				"for _ in range(10**3):",
				"....i = m",
				"....while True:",
				"........if not i in b:",
				"............A160256_list.append(i)",
				"............l1, l2, m = i, l1, l1//gcd(l1,i)",
				"............b.add(i)",
				"............break",
				"........i += m # _Chai Wah Wu_, Dec 09 2014"
			],
			"xref": [
				"Cf. A075075, A160257, A151413, A160218, A151546, A064413.",
				"For records see A151545, A151547."
			],
			"keyword": "nonn,look",
			"offset": "1,2",
			"author": "_Leroy Quet_, May 06 2009",
			"ext": [
				"More terms from _M. F. Hasler_, May 06 2009",
				"Edited by _N. J. A. Sloane_, May 16 2009"
			],
			"references": 8,
			"revision": 29,
			"time": "2015-08-23T11:15:56-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}