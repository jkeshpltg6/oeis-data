{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A265398",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 265398,
			"data": "1,2,3,4,6,6,15,8,9,12,35,12,77,30,18,16,143,18,221,24,45,70,323,24,36,154,27,60,437,36,667,32,105,286,90,36,899,442,231,48,1147,90,1517,140,54,646,1763,48,225,72,429,308,2021,54,210,120,663,874,2491,72,3127,1334,135,64,462,210,3599,572,969,180,4087,72",
			"name": "Perform one x^2 -\u003e x+1 reduction for the polynomial with nonnegative integer coefficients that is encoded in the prime factorization of n.",
			"comment": [
				"Completely multiplicative with a(2) = 2, a(3) = 3, a(prime(k)) = prime(k-1) * prime(k-2) for k \u003e 2. - _Andrew Howroyd_ \u0026 _Antti Karttunen_, Aug 04 2018"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A265398/b265398.txt\"\u003eTable of n, a(n) for n = 1..10080\u003c/a\u003e"
			],
			"formula": [
				"a(1) = 1; for n \u003e 1, a(n) = A064989(A064989(A065330(n))) * A064989(A065330(n)) * A065331(n)."
			],
			"mathematica": [
				"a[n_] := a[n] = Module[{k, p, e}, Which[n\u003c4, n, PrimeQ[n], k = PrimePi[n]; Prime[k-1] Prime[k-2], True, Product[{p, e} = pe; a[p]^e, {pe, FactorInteger[n]}]]];",
				"a /@ Range[1, 72] (* _Jean-François Alcover_, Sep 20 2019 *)"
			],
			"program": [
				"(PARI)",
				"A065330(n) = { while(0 == (n%2), n = n/2); while(0 == (n%3), n = n/3); n; }",
				"A065331 = n -\u003e n/A065330(n);",
				"A064989(n) = {my(f); f = factor(n); if((n\u003e1 \u0026\u0026 f[1,1]==2), f[1,2] = 0); for (i=1, #f~, f[i,1] = precprime(f[i,1]-1)); factorback(f)};",
				"A265398(n) = { my(a); if(1 == n, n, a = A064989(A065330(n)); A064989(a)*a*A065331(n)); };",
				"(Scheme)",
				"(definec (A265398 n) (if (= 1 n) n (* (A065331 n) (A064989 (A065330 n)) (A064989 (A064989 (A065330 n))))))"
			],
			"xref": [
				"Cf. A064989, A065330, A065331.",
				"Cf. also A192232, A206296, A265399."
			],
			"keyword": "nonn,mult",
			"offset": "1,2",
			"author": "_Antti Karttunen_, Dec 15 2015",
			"ext": [
				"Keyword mult added by _Antti Karttunen_, Aug 04 2018"
			],
			"references": 6,
			"revision": 19,
			"time": "2019-09-20T03:54:02-04:00",
			"created": "2015-12-18T11:22:28-05:00"
		}
	]
}