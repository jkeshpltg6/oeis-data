{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A014455",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 14455,
			"data": "1,4,6,8,12,8,8,16,6,12,24,8,24,24,0,16,12,16,30,24,24,16,24,16,8,28,24,32,48,8,0,32,6,32,48,16,36,40,24,16,24,16,48,40,24,40,0,32,24,36,30,16,72,24,32,48,0,32,72,24,48,40,0,48,12,16,48,56,48,32,48,16,30,64",
			"name": "Theta series of quadratic form with Gram matrix [ 1, 0, 0; 0, 1, 0; 0, 0, 2 ]. Number of integer solutions to x^2 + y^2 + 2*z^2 = n.",
			"comment": [
				"This is the tetragonal P lattice (the classical holotype) of dimension 3.",
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"John Cannon, \u003ca href=\"/A014455/b014455.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"G. Nebe and N. J. A. Sloane, \u003ca href=\"http://www.math.rwth-aachen.de/~Gabriele.Nebe/LATTICES/tetragonalP.html\"\u003eHome page for this lattice\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of phi(q)^2 * phi(q^2) = psi(q)^4 / psi(q^4) in powers of q where phi(), psi() are Ramanujan theta functions. - _Michael Somos_, Apr 07 2012",
				"Expansion of eta(q^2)^8 * eta(q^4) / (eta(q)^4 * eta(q^8)^2) in powers of q. - _Michael Somos_, Jul 05 2005",
				"Euler transform of period 8 sequence [4, -4, 4, -5, 4, -4, 4, -3, ...]. - _Michael Somos_, Jul 07 2005",
				"G.f.: theta_3(q)^2 * theta_3(q^2) = Product_{k\u003e0} (1 - x^(2*k))^8 * (1 - x^(4*k)) / ((1 - x^k)^4 * (1 - x^(8*k))^2).",
				"There is a classical formula (essentially due to Gauss): Write (uniquely) -2n=D(2^vf)^2, with D\u003c0 fundamental discriminant, f odd, v\u003e=-1. Then a(n)=12L((D/.),0)(1-(D/2))\\sum_{d\\mid f}\\mu(d)(D/d)sigma(f/d) (the formula for A005875), except that the factor (1-(D/2)) has to be replaced by 1/3 if v=-1 and by 1 if v=0 (and kept if v\u003e=1). Here mu() is the Moebius function, (D/2) and (D/d) are Kronecker-Legendre symbols, sigma() is the sum of divisors function, L((D/.),0)=h(D)/(w(D)/2) is the value at 0 of the L() function of the quadratic character (D/.), equal to the class number h(D) divided by 2 or 3 in the special cases D=-4 and -3. - Henri Cohen (Henri.Cohen(AT)math.u-bordeaux1.fr), May 12 2010",
				"a(2*n) = a(8*n) = A005875(n). a(2*n + 1) = A005877(n) = 4 * A045828(n). a(4*n) = A004015(n). a(4*n + 2) = 2 * A045826(n). a(8*n + 4) = 12 * A045828(n). a(8*n + 7) = 16 * A033763(n). a(16*n + 6) = 8 * A008443(n). a(16*n + 14) = 0. - _Michael Somos_, Apr 07 2012",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (8 t)) = 32^(1/2) (t/i)^(3/2) g(t) where q = exp(2 Pi i t) and g() is the g.f. for A246631."
			],
			"example": [
				"G.f. = 1 + 4*q + 6*q^2 + 8*q^3 + 12*q^4 + 8*q^5 + 8*q^6 + 16*q^7 + 6*q^8 + 12*q^9 + ..."
			],
			"mathematica": [
				"r[n_, z_] := Reduce[x^2 + y^2 + 2*z^2 == n, {x, y}, Integers]; a[n_] := Module[{rn0, rnz, k0, k}, rn0 = r[n, 0]; k0 = If[rn0 === False, 0, If[Head[rn0] === And, 1, Length[rn0]]]; For[k = 0; z = 1, z \u003c= Ceiling[Sqrt[n/2]], z++, rnz = r[n, z]; If[rnz =!= False, k = If[Head[rnz] === And, k+1, k + Length[rnz]]]]; k0 + 2*k]; Table[a[n], {n, 0, 100}] (* _Jean-François Alcover_, Oct 07 2013 *)",
				"a[ n_] := SeriesCoefficient[ EllipticTheta[ 3, 0, q]^2 EllipticTheta[ 3, 0, q^2], {q, 0, n}]; (* _Michael Somos_, Aug 31 2014 *)",
				"QP = QPochhammer; s = QP[q^2]^8*(QP[q^4]/(QP[q]^4*QP[q^8]^2)) + O[q]^80; CoefficientList[s, q] (* _Jean-François Alcover_, Nov 25 2015, after _Michael Somos_ *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c1, n==0, 2 * qfrep([ 1, 0, 0; 0, 1, 0; 0, 0, 2], n)[n])}; /* _Michael Somos_, Jul 05 2005 */",
				"(PARI) {a(n) = local(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x^2 + A)^8 * eta(x^4 + A) / (eta(x + A)^4 * eta(x^8 + A)^2), n))}; /* _Michael Somos_, Jul 05 2005 */",
				"(MAGMA) A := Basis( ModularForms( Gamma1(8), 3/2), 40); A[1] + 4*A[2] + 6*A[3] + 8*A[4]; /* _Michael Somos_, Aug 31 2014 */"
			],
			"xref": [
				"Cf. A004015, A005875, A005877, A008443, A033763, A045826, A045828, A156384, A213024, A246631."
			],
			"keyword": "nonn,nice,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_",
			"references": 10,
			"revision": 46,
			"time": "2021-12-16T22:43:21-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}