{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A136630",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 136630,
			"data": "1,0,1,0,0,1,0,1,0,1,0,0,4,0,1,0,1,0,10,0,1,0,0,16,0,20,0,1,0,1,0,91,0,35,0,1,0,0,64,0,336,0,56,0,1,0,1,0,820,0,966,0,84,0,1,0,0,256,0,5440,0,2352,0,120,0,1,0,1,0,7381,0,24970,0,5082,0,165,0,1,0,0,1024,0,87296,0",
			"name": "Triangular array: T(n,k) counts the partitions of the set [n] into k odd sized blocks.",
			"comment": [
				"For partitions into blocks of even size see A156289.",
				"Essentially the unsigned matrix inverse of triangle A121408.",
				"From _Peter Bala_, Jul 28 2014: (Start)",
				"Define a polynomial sequence x_(n) by setting x_(0) = 1 and for n = 1,2,... setting x_(n) = x*(x + n - 2)*(x + n - 4)*...*(x + n - 2*(n - 1)). Then this table is the triangle of connection constants for expressing the monomial polynomials x^n in terms of the basis x_(k), that is, x^n = sum {k = 0..n} T(n,k)*x_(k) for n = 0,1,2,.... An example is given below.",
				"Let M denote the lower unit triangular array A119467 and for k = 0,1,2,... define M(k) to be the lower unit triangular block array",
				"/I_k 0\\",
				"\\ 0  M/ having the k x k identity matrix I_k as the upper left block; in particular, M(0) = M. Then the present triangle, omitting the first row and column, equals the infinite matrix product M(0)*M(1)*M(2)*.... (End)",
				"Also the Bell transform of A000035(n+1). For the definition of the Bell transform see A264428. - _Peter Luschny_, Jan 27 2016"
			],
			"reference": [
				"L. Comtet, Analyse Combinatoire, Presses Univ. de France, 1970, Vol. II, pages 61-62.",
				"L. Comtet, Advanced Combinatorics, Reidel, 1974, pp. 225-226."
			],
			"link": [
				"Ch. A. Charalambides, \u003ca href=\"http://www.fq.math.ca/Scanned/19-5/charalambides.pdf\"\u003eCentral factorial numbers and related expansions\u003c/a\u003e, Fib. Quarterly, Vol. 19, No 5, Dec 1981, pp. 451-456."
			],
			"formula": [
				"G.f. for column k: x^k/Product_{j=0..floor(k/2)} (1 - (2*j + k-2*floor(k/2))^2 * x^2).",
				"G.f. for column 2*k: x^(2*k)/Product_{j=0..k} (1 - (2*j)^2*x^2).",
				"G.f. for column 2*k+1: x^(2*k+1)/Product_{j=0..k} (1 - (2*j+1)^2*x^2).",
				"From _Peter Bala_, Feb 21 2011 (Start)",
				"T(n,k) = 1/(2^k*k!)*Sum_{j = 0..k} (-1)^(k-j)*binomial(k,j)*(2*j-k)^n,",
				"Recurrence relation T(n+2,k) = T(n,k-2) + k^2*T(n,k).",
				"E.g.f.: F(x,z) = exp(x*sinh(z)) = Sum_{n\u003e=0} R(n,x)*z^n/n! = 1 + x*z + x^2*z^2/2! + (x+x^3)*z^3/3! + ....",
				"The row polynomials R(n,x) begin",
				"R(1,x) = x",
				"R(2,x) = x^2",
				"R(3,x) = x+x^3.",
				"The e.g.f. F(x,z) satisfies the partial differential equation d^2/dz^2(F) = x^2*F + x*F' + x^2*F'' where ' denotes differentiation w.r.t. x.",
				"Hence the row polynomials satisfy the recurrence relation R(n+2,x) = x^2*R(n,x) + x*R'(n,x) + x^2*R''(n,x) with R(0,x) = 1.",
				"The recurrence relation for T(n,k) given above follows from this.",
				"(End)",
				"For the corresponding triangle of ordered partitions into odd-sized blocks see A196776. Let P denote Pascal's triangle A070318 and put M = 1/2*(P-P^-1). M is A162590 (see also A131047). Then the first column of exp(t*M) lists the row polynomials for the present triangle. - _Peter Bala_, Oct 06 2011",
				"Row generating polynomials equal D^n(exp(x*t)) evaluated at x = 0, where D is the operator sqrt(1+x^2)*d/dx. Cf. A196776. - _Peter Bala_, Dec 06 2011",
				"From _Peter Bala_, Jul 28 2014: (Start)",
				"E.g.f.: exp(t*sinh(x)) = 1 + t*x + t^2*x^2/2! + (t + t^3)*x^3/3! + ....",
				"Hockey-stick recurrence: T(n+1,k+1) = Sum_{i = 0..floor((n-k)/2)} binomial(n,2*i)*T(n-2*i,k).",
				"Recurrence equation for the row polynomials R(n,t):",
				"R(n+1,t) = t*Sum_{k = 0..floor(n/2)} binomial(n,2*k)*R(n-2*k,t) with R(0,t) = 1. (End)"
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  0, 1;",
				"  0, 0,   1;",
				"  0, 1,   0,    1;",
				"  0, 0,   4,    0,    1;",
				"  0, 1,   0,   10,    0,     1;",
				"  0, 0,  16,    0,   20,     0,    1;",
				"  0, 1,   0,   91,    0,    35,    0,    1;",
				"  0, 0,  64,    0,  336,     0,   56,    0,   1;",
				"  0, 1,   0,  820,    0,   966,    0,   84,   0,   1;",
				"  0, 0, 256,    0, 5440,     0, 2352,    0, 120,   0, 1;",
				"  0, 1,   0, 7381,    0, 24970,    0, 5082,   0, 165, 0, 1;",
				"T(5,3) = 10. The ten partitions of the set [5] into 3 odd-sized blocks are",
				"(1)(2)(345), (1)(3)(245), (1)(4)(235), (1)(5)(234), (2)(3)(145),",
				"(2)(4)(135), (2)(5)(134), (3)(4)(125), (3)(5)(124), (4)(5)(123).",
				"Connection constants: Row 5 = [0,1,0,10,0,1]. Hence, with the polynomial sequence x_(n) as defined in the Comments section we have x^5 = x_(1) + 10*x_(3) + x_(5) = x + 10*x*(x+1)*(x-1) + x*(x+3)*(x+1)*(x-1)*(x-3)."
			],
			"maple": [
				"A136630 := proc (n, k) option remember; if k \u003c 0 or n \u003c k then 0 elif k = n then 1 else procname(n-2, k-2) + k^2*procname(n-2, k) end if end proc: seq(seq(A136630(n, k), k = 1 .. n), n = 1 .. 12); # _Peter Bala_, Jul 27 2014",
				"# The function BellMatrix is defined in A264428.",
				"BellMatrix(n -\u003e (n+1) mod 2, 9); # _Peter Luschny_, Jan 27 2016"
			],
			"mathematica": [
				"t[n_, k_] := Coefficient[ x^k/Product[ 1 - (2*j + k - 2*Quotient[k, 2])^2*x^2, {j, 0, k/2}] + x*O[x]^n, x, n]; Table[t[n, k], {n, 0, 12}, {k, 0, n}] // Flatten (* _Jean-François Alcover_, Nov 22 2013, after Pari *)",
				"BellMatrix[f_Function, len_] := With[{t = Array[f, len, 0]}, Table[BellY[n, k, t], {n, 0, len-1}, {k, 0, len-1}]];",
				"rows = 13;",
				"M = BellMatrix[Mod[#+1, 2]\u0026, rows];",
				"Table[M[[n, k]], {n, 1, rows}, {k, 1, n}] // Flatten (* _Jean-François Alcover_, Jun 23 2018, after _Peter Luschny_ *)"
			],
			"program": [
				"(PARI) {T(n,k)=polcoeff(x^k/prod(j=0,k\\2,1-(2*j+k-2*(k\\2))^2*x^2 +x*O(x^n)),n)}"
			],
			"xref": [
				"Cf. A121408; A136631 (antidiagonal sums), A003724 (row sums), A136632; A002452 (column 3), A002453 (column 5); A008958 (central factorial triangle), A156289. A185690, A196776."
			],
			"keyword": "nonn,tabl",
			"offset": "0,13",
			"author": "_Paul D. Hanna_, Jan 14 2008",
			"references": 11,
			"revision": 37,
			"time": "2018-06-23T06:10:53-04:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}