{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A145599",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 145599,
			"data": "1,5,5,15,35,15,35,140,140,35,70,420,720,420,70,126,1050,2700,2700,1050,126,210,2310,8250,12375,8250,2310,210,330,4620,21780,45375,45375,21780,4620,330,495,8580,51480,141570,196625,141570,51480,8580,495,715,15015",
			"name": "Triangular array of generalized Narayana numbers: T(n,k) = 5/(n+1)*binomial(n+1,k+4)*binomial(n+1,k-1).",
			"comment": [
				"T(n,k) is the number of walks of n unit steps, each step in the direction either up (U), down (D), right (R) or left (L), starting from (0,0) and finishing at lattice points on the horizontal line y = 4 and which remain in the upper half-plane y \u003e= 0. An example is given in the Example section below.",
				"The current array is the case r = 4 of the generalized Narayana numbers N_r(n,k) := (r + 1)/(n + 1)*binomial(n + 1,k + r)*binomial(n + 1,k - 1), which count walks of n steps from the origin to points on the horizontal line y = r that remain in the upper half-plane. Case r = 0 gives the table of Narayana numbers A001263 (but with an offset of 0 in the row numbering). For other cases see A145596 (r = 1), A145597 (r = 2) and A145598 (r = 3)."
			],
			"link": [
				"F. Cai, Q.-H. Hou, Y. Sun, A. L. B. Yang, \u003ca href=\"http://arxiv.org/abs/1808.05736\"\u003eCombinatorial identities related to 2x2 submatrices of recursive matrices\u003c/a\u003e, arXiv:1808.05736 Table 2.1 for k=4.",
				"R. K. Guy, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL3/GUY/catwalks.html\"\u003eCatwalks, sandsteps and Pascal pyramids\u003c/a\u003e, J. Integer Sequences, Vol. 3 (2000), Article #00.1.6"
			],
			"formula": [
				"T(n,k) = 5/(n+1)*binomial(n+1,k+4)*binomial(n+1,k-1) for n \u003e=4 and 1 \u003c= k \u003c= n-3. In the notation of [Guy], T(n,k) equals w_n(x,y) at (x,y) = (2*k - n + 2,4). Row sums A003519.",
				"O.g.f. for column k+2: 5/(k + 1) * y^(k+5)/(1 - y)^(k+7) * Jacobi_P(k,5,1,(1 + y)/(1 - y)).",
				"Identities for row polynomials R_n(x) := sum {k = 1..n-3} T(n,k)*x^k:",
				"x^4*R_(n-1)(x) = 5*(n - 1)*(n - 2)*(n - 3)*(n - 4)/((n + 1)*(n + 2)*(n + 3)*(n + 4)*(n + 5)) * sum {k = 0..n} binomial(n + 5,k) * binomial(2n - k,n) * (x - 1)^k;",
				"sum {k = 1..n} (-1)^k*binomial(n,k)*R_k(x^2)*(1 + x)^(2*(n-k)) = R_n(1)*x^(n-2) = A003519(n)*x^(n-2).",
				"Row generating polynomial R_(n+4)(x) = 5/(n+5)*x*(1-x)^n * Jacobi_P(n,5,5,(1+x)/(1-x)). [From _Peter Bala_, Oct 31 2008]"
			],
			"example": [
				"Triangle starts",
				"n\\k|...1......2......3......4......5......6",
				"===========================================",
				".4.|...1",
				".5.|...5......5",
				".6.|..15.....35.....15",
				".7.|..35....140....140.....35",
				".8.|..70....420....720....420.....70",
				".9.|.126...1050...2700...2700...1050....126",
				"...",
				"T(5,2) = 5: the 5 walks of length 5 from (0,0) to (1,4) are",
				"UUUUR, UUURU, UURUU, URUUU and RUUUU."
			],
			"maple": [
				"with(combinat):",
				"T:= (n,k) -\u003e 5/(n+1)*binomial(n+1,k+4)*binomial(n+1,k-1):",
				"for n from 4 to 13 do",
				"seq(T(n,k),k = 1..n-3);",
				"end do;"
			],
			"mathematica": [
				"Table[5/(n+1) Binomial[n+1,k+4]Binomial[n+1,k-1],{n,4,20},{k,0,n}]/.(0-\u003e Nothing)//Flatten (* _Harvey P. Dale_, Jan 25 2021 *)"
			],
			"xref": [
				"A003519 (row sums), A001263, A145596, A145597, A145598, A145603."
			],
			"keyword": "easy,nonn,tabl",
			"offset": "4,2",
			"author": "_Peter Bala_, Oct 15 2008",
			"references": 5,
			"revision": 10,
			"time": "2021-01-25T14:26:37-05:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}