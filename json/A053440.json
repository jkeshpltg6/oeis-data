{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A053440",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 53440,
			"data": "1,3,2,7,12,6,15,50,60,24,31,180,390,360,120,63,602,2100,3360,2520,720,127,1932,10206,25200,31920,20160,5040,255,6050,46620,166824,317520,332640,181440,40320,511,18660,204630,1020600,2739240,4233600,3780000",
			"name": "Number of k-simplices in the first derived complex of the standard triangulation of an n-simplex. Equivalently, T(n,k) is the number of ascending chains of length k+1 of nonempty subsets of the set {1, 2, ..., n+1}.",
			"comment": [
				"T(n,k) is the number of length k+1 sequences of nonempty mutually disjoint subsets of {1,2,...,n+1}.  The e.g.f. for the column corresponding to k is exp(x)*(exp(x)-1)^(k+1). - _Geoffrey Critzer_, Dec 20 2011"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A053440/b053440.txt\"\u003eTable of n, a(n) for the first 50 rows, flattened\u003c/a\u003e",
				"F. Brenti and V. Welker, \u003ca href=\"http://dx.doi.org/10.1007/s00209-007-0251-z\"\u003ef-vectors of barycentric subdivisions\u003c/a\u003e Math. Z., 259(4), 849-865, 2008.",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Barycentric_subdivision\"\u003eBarycentric subdivision\u003c/a\u003e"
			],
			"formula": [
				"T(0,k) = delta(0,k), T(n,k) = delta(0,k) + (k+1)(T(n-1,k-1) + (k+2)T(n-1,k)).",
				"E.g.f.: exp(x)*(exp(x)-1)/(1-y*(exp(x)-1)). - _Vladeta Jovovic_, Apr 13 2003",
				"T(n,k) = Sum_{i = 0..n} binomial(n+1,i+1)*(k+1)!*Stirling2(i+1,k+1) = (k+1)!*Stirling2(n+2,k+2) (Brenti and Welker). Row sums are A002050. - _Peter Bala_, Jul 12 2014"
			],
			"example": [
				"T(2,1) = 12 because there are 12 such length 2 sequences of subsets of {1,2,3}: ({1},{2}), ({1},{3}), ({2},{3}), ({1},{2,3}), ({2},{1,3}), ({3},{1,2}) with two orderings for each. - _Geoffrey Critzer_, Dec 20 2011",
				"Triangle begins:",
				"   1",
				"   3      2",
				"   7     12      6",
				"  15     50     60     24",
				"  31    180    390    360    120"
			],
			"maple": [
				"with(combinat):",
				"a := (n, k) -\u003e (k+1)!*stirling2(n+2, k+2):",
				"seq(print(seq(a(n, k), k = 0..n)), n = 0..10);"
			],
			"mathematica": [
				"nn = 5; a = Exp[ x] - 1 ; f[list_] := Select[list, # \u003e 0 \u0026];Map[f, Transpose[Table[Drop[Range[0, nn]!CoefficientList[Series[a^k  Exp[x], {x, 0, nn}],x], 1], {k, 1, 5}]]] // Grid (* _Geoffrey Critzer_, Dec 20 2011 *)",
				"Table[(k+1)!*StirlingS2[n+2,k+2], {n,0,10}, {k,0,n}]//Flatten (* _G. C. Greubel_, Nov 19 2017 *)"
			],
			"program": [
				"(PARI) for(n=0,10, for(k=0,n, print1((k+1)!*stirling(n+2,k+2,2), \", \"))) \\\\ _G. C. Greubel_, Nov 19 2017"
			],
			"xref": [
				"Cf. A028246.",
				"Cf. A002050 (row sums), A019538."
			],
			"keyword": "nonn,easy,tabl,nice",
			"offset": "0,2",
			"author": "_Rob Arthan_, Jan 12 2000",
			"ext": [
				"More terms from _James A. Sellers_, Jan 14 2000"
			],
			"references": 6,
			"revision": 27,
			"time": "2017-11-19T01:58:00-05:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}