{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A143898",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 143898,
			"data": "1,2,1,1,1,2,1,1,1,1,2,1,2,1,3,1,1,1,3,2,1,1,1,2,2,2,2,2,2,2,1,1,3,2,1,2,3,2,1,3,1,2,2,2,2,2,2,2,2,3,2,3,2,2,2,2,1,2,2,3,2,3,3,1,4,2,3,2,1,3,2,3,2,2,2,4,1,4,2,2,2,2,3,2,3,2,4,3,2,3,3,3,3,1,3,3,2,3,3,2,3,5,3,1,1",
			"name": "Number of primes between n^K and (n+1)^K, where K = log(1151)/log(95).",
			"comment": [
				"This value of K is conjectured to be the least possible such that there is at least one prime in the range n^K to (n+1)^K for n\u003e0. This value of K was found using exact interval arithmetic. For each n \u003c= 110 and for each prime p in the range n to n^1.7, we computed an interval k(n,p) such that p is between n^k(n,p) and (n+1)^k(n,p). The intersection of all these intervals produces a list of intervals. The least value in those intervals is K, which is log(1151)/log(95). We computed 10^5 terms of this sequence to give us confidence that a(n)\u003e0 for all n.",
				"More details about the algorithm: The n^1.7 limit was chosen because we were fairly certain that K would be less than 1.7. Let k(n) be the union of the intervals k(n,p) for p\u003cn^1.7. Then k(n) is the set of exponents e such that the range n^e to (n+1)^e always contains a prime. Let k be the intersection of all the k(n) intervals for n up to N. Then k is the set of exponents e such that there is always a prime in the range n^e to (n+1)^e for n\u003c=N. The number K is the least number in the set k. It appears that as N becomes larger, the set k converges. See A143935. [_T. D. Noe_, Sep 08 2008]",
				"The constant log(1151)/log(95) is A194362. - _John W. Nicholson_, Nov 25 2013",
				"1151 counts as a qualifying prime towards both a(94)=1 and a(95)=3, in accordance with use of closed ranges. If prime p were counted only when n^K \u003c p \u003c= (n+1)^K, then term 95 would be 2. If prime p were counted only when n^K \u003c= p \u003c (n+1)^K, then term 94 would be 0. The conjecture in the author's comment implies K is the greatest real value such that for all e \u003c= K there exists n \u003e 0 with no prime p satisfying n^e \u003c= p \u003c (n+1)^e. - _Peter Munn_, Mar 02 2017",
				"The author's description of the calculation of K implies that K is not an isolated qualifying value; equivalently that K is also the least real value for which there is a positive epsilon such that for all exponent e, K \u003c= e \u003c= K+epsilon and integer n \u003e 0 there is a prime p satisfying n^e \u003c= p \u003c= (n+1)^e. This is a necessary precondition for my Mar 02 2017 deduction from the author's conjecture. - _Peter Munn_, Aug 21 2019"
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A143898/b143898.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"mathematica": [
				"k= 1.547777108714197624815033; Table[Length[Select[Range[Ceiling[n^k],Floor[(n+1)^k]], PrimeQ]], {n,150}] (* _T. D. Noe_, Sep 08 2008 *)"
			],
			"xref": [
				"A014085 (number of primes between n^2 and (n+1)^2), both A134034 and A143935 use a larger K."
			],
			"keyword": "nice,nonn",
			"offset": "1,2",
			"author": "_T. D. Noe_, Sep 04 2008, Sep 26 2009, Oct 21 2009",
			"ext": [
				"Removed some comments which changed the definition of this sequence. - _N. J. A. Sloane_, Oct 21 2009"
			],
			"references": 8,
			"revision": 26,
			"time": "2019-08-23T00:07:12-04:00",
			"created": "2009-01-09T03:00:00-05:00"
		}
	]
}