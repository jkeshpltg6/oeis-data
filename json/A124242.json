{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A124242",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 124242,
			"data": "1,-1,1,1,-2,0,2,-2,-1,4,-1,-4,4,1,-6,3,6,-7,-3,10,-4,-10,12,6,-18,5,18,-20,-8,30,-10,-29,31,12,-46,17,44,-47,-20,68,-23,-66,72,31,-104,33,98,-107,-44,156,-51,-144,154,61,-220,75,206,-220,-90,310,-104,-290,312,131,-442,143,408,-437,-178,618,-202",
			"name": "Expansion of a parametrization of Ramanujan's continued fraction.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"reference": [
				"Srinivasa Ramanujan, The Lost Notebook and Other Unpublished Papers, Narosa Publishing House, New Delhi, 1988, p. 53"
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A124242/b124242.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Euler transform of period 10 sequence [ -1, 1, 2, -1, -2, -1, 2, 1, -1, 0, ...].",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2)) where f(u, v) = v^2 - (2-u) * (2 - (2-u) * (2-v)).",
				"Given g.f. A(x) =: k, then B(x) = (1-k) * (k / (2-k))^2, B(x^2) = (1-k)^2 * ((2-k) / k) where B(x) is the g.f. for A078905.",
				"Expansion of f(-x^5, -x^10)^3 / (f(x, x^4) * f(-x^3, -x^7)^2) in powers of x where f(, ) is Ramanujan's general theta function. - _Michael Somos_, Jan 06 2016",
				"G.f.: Product_{k\u003e0} ((1 - x^(10k-5)) / ((1 - x^(10k-3)) * (1 - x^(10k-7))))^2 * (1 - x^(10k-1)) * (1 - x^(10k-4)) * (1 - x^(10k-6)) * (1 - x^(10k-9) / ((1-x^(10k-2)) * (1-x^(10k-8))).",
				"-a(n) = A112274(n) unless n = 0.",
				"G.f.: 1 - r(q) * r(q^2)^2 where r() is the Rogers-Ramanujan continued fraction. - _Seiichi Manyama_, Apr 18 2017"
			],
			"example": [
				"G.f. = 1 - x + x^2 + x^3 - 2*x^4 + 2*x^6 - 2*x^7  - x^8 + 4*x^9 - x^10 - 4*x^11 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ Product[ (1 - x^k)^{ 1, -1, -2, 1, 2, 1, -2, -1, 1, 0}[[Mod[k, 10, 1]]], {k, n}], {x, 0, n}]; (* _Michael Somos_, Jan 06 2016 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( prod(k=1, n, (1 - x^k + A)^[0, 1, -1, -2, 1, 2, 1, -2, -1, 1][k%10+1]), n))};"
			],
			"xref": [
				"Cf. A078905, A112274, A112803, A285348."
			],
			"keyword": "sign",
			"offset": "0,5",
			"author": "_Michael Somos_, Oct 27 2006",
			"references": 3,
			"revision": 20,
			"time": "2021-03-12T22:24:44-05:00",
			"created": "2006-12-06T03:00:00-05:00"
		}
	]
}