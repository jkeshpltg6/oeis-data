{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A348410",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 348410,
			"data": "1,1,5,19,85,376,1715,7890,36693,171820,809380,3830619,18201235,86770516,414836210,1988138644,9548771157,45948159420,221470766204,1069091485500,5167705849460,25009724705460,121171296320475,587662804774890,2852708925078675,13859743127937876",
			"name": "Number of nonnegative integer solutions to n = Sum_{i=1..n} (a_i+b_i), with b_i even.",
			"comment": [
				"Suppose n objects to be distributed in 2n baskets, half of these white and half black. White baskets may contain 0 or any number of objects, whilst black baskets may contain 0 or an even number of objects. a(n) is the number of distinct possible distributions."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A348410/b348410.txt\"\u003eTable of n, a(n) for n = 0..1441\u003c/a\u003e"
			],
			"formula": [
				"Conjecture: D-finite with recurrence +7168*n*(2*n-1)*(n-1)*a(n) -64*(n-1)*(1759*n^2-5294*n+5112)*a(n-1) +12*(7561*n^3-75690*n^2+165271*n-101070)*a(n-2) +5*(110593*n^3-743946*n^2+1659971*n-1232778)*a(n-3) +2680*(4*n-15)*(2*n-7)*(4*n-13)*a(n-4)=0. - _R. J. Mathar_, Oct 19 2021",
				"From _Vaclav Kotesovec_, Nov 01 2021: (Start)",
				"Recurrence (of order 2): 16*(n-1)*n*(2*n - 1)*(51*n^2 - 162*n + 127)*a(n) = (n-1)*(5457*n^4 - 22791*n^3 + 32144*n^2 - 17536*n + 3072)*a(n-1) + 8*(2*n - 3)*(4*n - 7)*(4*n - 5)*(51*n^2 - 60*n + 16)*a(n-2).",
				"a(n) ~ sqrt(3 + 5/sqrt(17)) * (107 + 51*sqrt(17))^n / (sqrt(Pi*n) * 2^(6*n+2)).",
				"(End)"
			],
			"example": [
				"Some examples (semicolon separates white basket from black baskets).",
				"For n=1: {{1 ; 0}} - Total possible ways: 1.",
				"For n=2: {{0 , 0 ; 0, 2}, {0 , 0 ; 2, 0}, {0 , 2 ; 0, 0}, {1 , 1 ; 0, 0}, {2 , 0 ; 0, 0}} - Total possible ways: 5."
			],
			"maple": [
				"b:= proc(n, t) option remember; `if`(t=0, 1-signum(n),",
				"      add(b(n-j, t-1)*(1+iquo(j, 2)), j=0..n))",
				"    end:",
				"a:= n-\u003e b(n$2):",
				"seq(a(n), n=0..25);  # _Alois P. Heinz_, Oct 17 2021"
			],
			"mathematica": [
				"(* giveList=True produces the list of solutions *)",
				"(* giveList=False gives the number of solutions *)",
				"counter[objects_, giveList_: False] :=",
				"  Module[{n = objects, nb, eq1, eqa, eqb, eqs, var, sol, var2, list},",
				"   nb = n;",
				"   eq1 = {Total[Map[a[#] + 2*b[#] \u0026, Range[nb]]] - n == 0};",
				"   eqa = {And @@ Map[0 \u003c= a[#] \u003c= n \u0026, Range[nb]]};",
				"   eqb = {And @@ Map[0 \u003c= b[#] \u003c= n \u0026, Range[nb]]};",
				"   eqs = {And @@ Join[eq1, eqa, eqb]};",
				"   var = Flatten[Map[{a[#], b[#]} \u0026, Range[nb]]];",
				"   var = Join[Map[a[#] \u0026, Range[nb]], Map[b[#] \u0026, Range[nb]]];",
				"   sol = Solve[eqs, var, Integers];",
				"   var2 = Join[Map[a[#] \u0026, Range[nb]], Map[2*b[#] \u0026, Range[nb]]];",
				"   list = Sort[Map[var2 /. # \u0026, sol]];",
				"   list = Map[StringReplace[ToString[#], {\",\" -\u003e \" ;\"}, n] \u0026, list];",
				"   list = Map[StringReplace[#, {\";\" -\u003e \",\"}, n - 1] \u0026, list];",
				"   Return[",
				"    If[giveList, Print[\"Total: \", Length[list]]; list, Length[sol]]];",
				"   ];"
			],
			"xref": [
				"Cf. A033715, A033716, A034297, A088218, A294860, A348474."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_César Eliud Lozada_, Oct 17 2021",
			"ext": [
				"More terms from _Alois P. Heinz_, Oct 17 2021"
			],
			"references": 2,
			"revision": 26,
			"time": "2021-11-01T04:55:42-04:00",
			"created": "2021-10-18T19:15:04-04:00"
		}
	]
}