{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A053347",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 53347,
			"data": "1,10,54,210,660,1782,4290,9438,19305,37180,68068,119340,201552,329460,523260,810084,1225785,1817046,2643850,3782350,5328180,7400250,10145070,13741650,18407025,24402456,32040360,41692024,53796160",
			"name": "a(n) = binomial(n+7, 7)*(n+4)/4.",
			"comment": [
				"If a 2-set Y and an (n-3)-set Z are disjoint subsets of an n-set X then a(n-9) is the number of 9-subsets of X intersecting both Y and Z. - _Milan Janjic_, Sep 08 2007",
				"8-dimensional square numbers, seventh partial sums of binomial transform of [1, 2, 0, 0, 0, ...]. a(n) = sum{i=0,n,C(n+7, i+7)*b(i)}, where b(i) = [1, 2, 0, 0, 0, ...]. - Borislav St. Borisov (b.st.borisov(AT)abv.bg), Mar 05 2009",
				"2*a(n) is number of ways to place 7 queens on an (n+7) X (n+7) chessboard so that they diagonally attack each other exactly 21 times. The maximal possible attack number, p=binomial(k,2)=21 for k=7 queens, is achievable only when all queens are on the same diagonal. In graph-theory representation they thus form the corresponding complete graph. - _Antal Pinter_, Dec 27 2015",
				"Coefficients in the terminating series identity 1 - 10*n/(n + 9) + 54*n*(n - 1)/((n + 9)*(n + 10)) - 210*n*(n - 1)*(n - 2)/((n + 9)*(n + 10)*(n + 11)) + ... = 0 for n = 1,2,3,.... Cf. A050486. - _Peter Bala_, Feb 18 2019"
			],
			"reference": [
				"A. H. Beiler, Recreations in the Theory of Numbers, Dover, N.Y., 1964, pp. 194-196."
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A053347/b053347.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Milan Janjic, \u003ca href=\"http://www.pmfbl.org/janjic/\"\u003eTwo Enumerative Functions\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_09\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (9,-36,84,-126,126,-84,36,-9,1).",
				"\u003ca href=\"/index/Ch#Cheby\"\u003eIndex entries for sequences related to Chebyshev polynomials.\u003c/a\u003e"
			],
			"formula": [
				"a(n) = ((-1)^n)*A053120(2*n+8, 8)/2^7 (1/128 of ninth unsigned column of Chebyshev T-triangle, zeros omitted).",
				"G.f.: (1+x)/(1-x)^9.",
				"a(n) = 2*C(n+8, 8) - C(n+7, 7). - _Paul Barry_, Mar 04 2003",
				"Equals A027803/35 = C(n+4, n)*C(n+7, 4)/35. - _Zerinvary Lajos_, May 25 2005",
				"a(n) = C(n+7, 7) + 2*C(n+7, 8). - Borislav St. Borisov (b.st.borisov(AT)abv.bg), Mar 05 2009",
				"a(n) = (n^8 + 32*n^7 + 434*n^6 + 3248*n^5 + 14609*n^4 + 40208*n^3 + 65596*n^2 + 57312*n + 20160)/20160. - _Chai Wah Wu_, Jan 24 2016",
				"Sum_{n\u003e=0} 1/a(n) = 41503/45 - 280/3*Pi^2. - _Jaume Oliver Lafont_, Jul 17 2017"
			],
			"maple": [
				"A053347:=n-\u003ebinomial(n+7,7)*(n+4)/4: seq(A053347(n), n=0..50); # _Wesley Ivan Hurt_, Jul 16 2017"
			],
			"mathematica": [
				"s1=s2=s3=s4=s5=s6=0; lst={}; Do[s1+=n^2; s2+=s1; s3+=s2; s4+=s3; s5+=s4; s6+=s5; AppendTo[lst,s6],{n,0,7!}]; lst (* _Vladimir Joseph Stephan Orlovsky_, Jan 15 2009 *)",
				"CoefficientList[Series[(1 + x) / (1 - x)^9, {x, 0, 50}], x] (* _Vincenzo Librandi_, Jun 09 2013 *)",
				"Table[SeriesCoefficient[(1 + x)/(1 - x)^9, {x, 0, n}], {n, 0, 28}] (* or *)",
				"Table[Binomial[n + 7, 7] (n + 4)/4, {n, 0, 28}] (* _Michael De Vlieger_, Dec 31 2015 *)"
			],
			"program": [
				"(PARI) a(n)=binomial(n+7,7)*(n+4)/4 \\\\ _Charles R Greathouse IV_, Jun 10 2011",
				"(MAGMA) [Binomial(n+7,7)+2*Binomial(n+7,8): n in [0..35]]; // _Vincenzo Librandi_, Jun 09 2013",
				"(Python)",
				"A053347_list, m = [], [2]+[1]*8",
				"for _ in range(10**2):",
				"    A053347_list.append(m[-1])",
				"    print(m[-1])",
				"    for i in range(8):",
				"        m[i+1] += m[i] # _Chai Wah Wu_, Jan 24 2016"
			],
			"xref": [
				"Partial sums of A050486.",
				"Cf. A005585, A040977."
			],
			"keyword": "easy,nonn",
			"offset": "0,2",
			"author": "_Barry E. Williams_, Jan 06 2000",
			"references": 22,
			"revision": 63,
			"time": "2019-02-19T04:41:40-05:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}