{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A072942",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 72942,
			"data": "3,4,3,4,12,6,3,4,3,12,20,24687390,3,72,62,4,20,1102903830,12,58051620,3,1793172,468,1035844571580,62,882,398,75274140,6,81206805256038,14,1288005000,78428,93888,37664,24380304369772260,432,3300,21962",
			"name": "a(n) is the least x such that the cyclotomic polynomial values Phi(d,x) are prime for all d dividing n.",
			"comment": [
				"An equivalent formulation is that a(n) is smallest number x such that x^n-1 factors only into its algebraic factors.",
				"Many more terms are known, in particular terms at prime indices. Massively composite n are the hardest to find - term 256 alone took a month to find. Contact the author for more terms beyond the gaps.",
				"2 never appears in the sequence because Phi(1,2) = 1, which is irreducible but not prime.",
				"a(n) is the smallest number x \u003e 2 such that A001222(x^n-1) = A000005(n). - _Thomas Ordowski_, Jan 31 2018",
				"All terms are in A008864. If n is even, a(n) is in A014574. - _Robert Israel_, Jan 31 2018"
			],
			"link": [
				"Don Reble, \u003ca href=\"/A072942/b072942.txt\"\u003eTable of n, a(n) for n = 1..47\u003c/a\u003e",
				"Don Reble, \u003ca href=\"/A072942/a072942.txt\"\u003eTable of n, a(n) for n = 1..99\u003c/a\u003e (with question marks at the unknown entries)"
			],
			"example": [
				"a(16)=4 because 4^16-1 = 3*5*17*257*65537, which are the 5 algebraic factors."
			],
			"maple": [
				"f:= proc(n) local p, C, x, d;",
				"  C:= [seq(numtheory:-cyclotomic(d,x), d = numtheory:-divisors(n) minus {1})];",
				"  p:= 1;",
				"  do",
				"    p:= nextprime(p);",
				"    if andmap(isprime, subs(x=p+1, C)) then return p+1 fi",
				"  od:",
				"end proc:",
				"map(f, [$1..29]); # _Robert Israel_, Jan 31 2018"
			],
			"mathematica": [
				"Table[With[{d = Divisors@ n}, SelectFirst[Range[10^3], AllTrue[Cyclotomic[d, #], PrimeQ] \u0026]], {n, 11}] (* _Michael De Vlieger_, Jan 31 2018 *)"
			],
			"program": [
				"(PARI) for(d=1,17,ds=divisors(d); print(\"Searching for d|\"d\":\"ds); forprime(p=2,499999,okc=1; for(c=2,length(ds), if(!isprime(subst(polcyclo(ds[c]),x,p+1)),okc=0; break)); if(okc, for(c=1,length(ds), print(\"Phi(\"ds[c]\",\"p+1\")=\"subst(polcyclo(ds[c]),x,p+1))); break)))",
				"(PARI) isok(n, x) = {fordiv(n, d, if (! isprime(polcyclo(d, x)), return(0));); return(1);}",
				"a(n) = {my(x=2); while (! isok(n, x), x++); x;} \\\\ _Michel Marcus_, Jan 31 2018"
			],
			"xref": [
				"Cf. A000005, A001222, A008864, A014574, A070737."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Phil Carmody_, Aug 12 2002",
			"ext": [
				"Corrected and extended by _Don Reble_, Feb 03 2014",
				"Edited by _N. J. A. Sloane_, Mar 01 2014 at the suggestion of _Phil Carmody_ and _Don Reble_"
			],
			"references": 1,
			"revision": 47,
			"time": "2018-07-12T08:57:35-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}