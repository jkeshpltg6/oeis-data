{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A212623",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 212623,
			"data": "1,1,1,2,1,3,1,1,3,1,1,4,3,1,4,3,1,4,3,1,1,4,3,1,1,5,6,1,1,5,6,1,1,5,6,1,1,5,6,2,1,5,6,2,1,5,6,2,1,6,10,4,1,5,6,4,1,1,5,6,2,1,6,10,5,1,5,6,4,1,1,6,10,5,1,1,6,10,5,1,1,6,10,4,1,6,10,5,1,6,10,7,2,1,7,15",
			"name": "Irregular triangle read by rows: T(n,k) is the number of independent vertex subsets with k vertices of the rooted tree with Matula-Goebel number n (n\u003e=1, k\u003e=0).",
			"comment": [
				"A vertex subset in a tree is said to be independent if no pair of vertices is connected by an edge. The empty set is considered to be independent.",
				"The Matula-Goebel number of a rooted tree can be defined in the following recursive manner: to the one-vertex tree there corresponds the number 1; to a tree T with root degree 1 there corresponds the t-th prime number, where t is the Matula-Goebel number of the tree obtained from T by deleting the edge emanating from the root; to a tree T with root degree m\u003e=2 there corresponds the product of the Matula-Goebel numbers of the m branches of T.",
				"Sum of entries in row n = A184165(n) = number of independent vertex subset (the Merrifield-Simmons index).",
				"Sum(k*T(n,k), k\u003e=0) = A212624(n) = number of vertices in all independent vertex subsets.",
				"Number of entries in row n = 1 + number of vertices in the largest independent vertex susbset = 1 + A212625(n).",
				"Last entry in row n = A212626(n) = number of largest independent vertex subsets.",
				"With the given Maple program, the command P(n) yields the generating polynomial of row n."
			],
			"reference": [
				"H. Prodinger and R. F. Tichy, Fibonacci numbers of graphs, Fibonacci Quart., 20, 1982, 16-21.",
				"F. Goebel, On a 1-1 correspondence between rooted trees and natural numbers, J. Combin. Theory, B 29 (1980), 141-143.",
				"I. Gutman and A. Ivic, On Matula numbers, Discrete Math., 150, 1996, 131-142.",
				"I. Gutman and Y-N. Yeh, Deducing properties of trees from their Matula numbers, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"D. W. Matula, A natural rooted tree enumeration by prime factorization, SIAM Review, 10, 1968, 273."
			],
			"link": [
				"E. Deutsch, \u003ca href=\"http://arxiv.org/abs/1111.4288\"\u003e Rooted tree statistics from Matula numbers\u003c/a\u003e, arXiv:1111.4288.",
				"\u003ca href=\"/index/Mat#matula\"\u003eIndex entries for sequences related to Matula-Goebel numbers\u003c/a\u003e"
			],
			"formula": [
				"Define R(n) =R(n,x)  (S(n)=S(n,x)) the generating polynomial of the  independent vertex subsets that contain (do not contain) the root of the rooted tree with Matula-Goebel number n. Then R(1)=x, S(1)=1, R(the t-th prime) = x*S(t), S(the t-th prime) = R(t) + S(t); R(rs) = R(r)R(s)/x, S(rs) = S(r)S(s), (r,s\u003e=2)."
			],
			"example": [
				"Row 5 is [1,4,3] because the rooted tree with Matula-Goebel number 5 is the path tree R - A - B - C with independent vertex subsets: {}, {R}, {A}, {B}, {C}, {R,B}, {R,C}, {A,C}.",
				"Triangle starts:",
				"1, 1;",
				"1, 2;",
				"1, 3, 1;",
				"1, 3, 1;",
				"1, 4, 3;"
			],
			"maple": [
				"with(numtheory): A := proc (n) local r, s: r := proc (n) options operator, arrow: op(1, factorset(n)) end proc: s := proc (n) options operator, arrow: n/r(n) end proc: if n = 1 then [x, 1] elif bigomega(n) = 1 then [expand(x*A(pi(n))[2]), expand(A(pi(n))[1])+A(pi(n))[2]] else [sort(expand(A(r(n))[1]*A(s(n))[1]/x)), sort(expand(A(r(n))[2]*A(s(n))[2]))] end if end proc: P := proc (n) options operator, arrow: sort(A(n)[1]+A(n)[2]) end proc: for n to 35 do seq(coeff(P(n), x, k), k = 0 .. degree(P(n))) end do; % yields sequence in triangular form"
			],
			"xref": [
				"Cf. A212618, A212619, A212620, A212621, A212622, A212624, A212625, A212626, A212627, A212628, A212629, A212630, A212631, A212632."
			],
			"keyword": "nonn,tabf",
			"offset": "1,4",
			"author": "_Emeric Deutsch_, Jun 01 2012",
			"references": 10,
			"revision": 15,
			"time": "2017-03-07T11:25:31-05:00",
			"created": "2012-06-02T15:04:19-04:00"
		}
	]
}