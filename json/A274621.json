{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A274621",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 274621,
			"data": "1,-2,3,-6,11,-18,28,-44,69,-104,152,-222,323,-460,645,-902,1254,-1722,2343,-3174,4278,-5722,7601,-10056,13250,-17358,22623,-29382,38021,-48984,62857,-80404,102528,-130282,165002,-208398,262495,-329666,412878,-515840,642941,-799362,991478",
			"name": "Coefficients in the expansion Product_{ n\u003e=1 } (1-q^(2n-1))^2/(1-q^(2n))^2.",
			"comment": [
				"This is the reciprocal of the g.f. for A008441.",
				"From _Wolfdieter Lang_, Jul 05 2016: (Start)",
				"The g.f. is the square of the one for A106507.",
				"Expansion of 1/(k/(4*q^(1/2)) * (2/Pi)*K(k)) in powers of q^2, where k is the modulus (k^2 is the parameter), K is the real quarter period and q is the Jacobi nome of elliptic functions. See a similar Jul 05 2016 comment on A008441. This appears as a factor in the sn and cn formulas of Abramowitz-Stegun. p. 575, 16.23.1 and 16.23.2. (End)"
			],
			"reference": [
				"R. W. Gosper, Experiments and discoveries in q-trigonometry, in Symbolic Computation, Number Theory, Special Functions, Physics and Combinatorics. Editors: F. G. Garvan and M. E. H. Ismail. Kluwer, Dordrecht, Netherlands, 2001, pp. 79-105."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A274621/b274621.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e (terms 0..1000 from Vincenzo Librandi)",
				"M. Abramowitz and I. A. Stegun, eds., \u003ca href=\"http://www.convertit.com/Go/ConvertIt/Reference/AMS55.ASP\"\u003eHandbook of Mathematical Functions\u003c/a\u003e, National Bureau of Standards Applied Math. Series 55, Tenth Printing, 1972, p. 575, 16.23.1 and 16.23.2.",
				"R. W. Gosper, \u003ca href=\"/A274621/a274621.pdf\"\u003eExperiments and discoveries in q-trigonometry\u003c/a\u003e, Preprint.",
				"R. W. Gosper, \u003ca href=\"/A274621/a274621_1.pdf\"\u003eq-Trigonometry: Some Prefatory Afterthoughts\u003c/a\u003e"
			],
			"formula": [
				"From _Wolfdieter Lang_, Jul 05 2016: (Start)",
				"G.f.: 1/(theta_2(0, sqrt(q))/(2*q^(1/8)))^2, with the Jacobi theta_2 function.",
				"G.f.: 1/(Sum_{n \u003e= 0} q^(n*(n+1)/2))^2.",
				"G.f.: 1/(Prod_{n \u003e= 1} (1 - q^n) * (1 + q^n)^2)^2 = 1/(Prod_{n \u003e= 1} (1 - q^(2*n)) * (1 + q^n ))^2 = Prod_{n \u003e= 1} (1 - q^(2n-1))^2 / (1 - q^(2n))^2. For the last equality, giving the g.f. of the name, see the Euler identity, mentioned in a Jul 05 2016 comment of A010054. (End)",
				"a(n) ~ (-1)^n * exp(Pi*sqrt(n)) / (2^(5/2)*n^(5/4)). - _Vaclav Kotesovec_, Jul 05 2016"
			],
			"mathematica": [
				"nmax = 40; CoefficientList[Series[Product[(1 - x^(2*k-1))^2 / (1 - x^(2*k))^2, {k, 1, nmax}], {x, 0, nmax}], x] (* _Vaclav Kotesovec_, Jul 05 2016 *)"
			],
			"xref": [
				"If the signs are deleted we get A273225.",
				"Cf. A008441, A010054, A106507."
			],
			"keyword": "sign,easy,changed",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Jul 03 2016",
			"references": 10,
			"revision": 35,
			"time": "2022-01-05T05:20:00-05:00",
			"created": "2016-07-03T23:06:25-04:00"
		}
	]
}