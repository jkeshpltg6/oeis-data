{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007865",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7865,
			"data": "1,2,3,6,9,16,24,42,61,108,151,253,369,607,847,1400,1954,3139,4398,6976,9583,15456,20982,32816,45417,70109,94499,148234,200768,308213,415543,634270,849877,1311244,1739022,2630061,3540355,5344961,7051789,10747207,14158720,21295570,28188520",
			"name": "Number of sum-free subsets of {1, ..., n}.",
			"comment": [
				"More precisely, subsets of {1,...,n} containing no solutions of x+y=z.",
				"There are two proofs that a(n) is 2^{n/2}(1+o(1)), as Paul Erdős and I conjectured.",
				"In sumset notation, number of subsets A of {1,...,n} such that the intersection of A and 2A is empty. Using the Mathematica program, all such subsets can be printed. - _T. D. Noe_, Apr 20 2004",
				"The Sapozhenko paper has many additional references.",
				"If this sequence counts sum-free sets, then A326083 counts sum-closed sets, which is different from sum-full sets (A093971). - _Gus Wiseman_, Jul 08 2019"
			],
			"reference": [
				"S. R. Finch, Mathematical Constants, Cambridge, 2003, pp. 180-183."
			],
			"link": [
				"Fausto A. C. Cariboni, \u003ca href=\"/A007865/b007865.txt\"\u003eTable of n, a(n) for n = 0..88\u003c/a\u003e, (terms up to a(70) from Per Hakan Lundow)",
				"P. J. Cameron and P. Erdős, \u003ca href=\"https://www.researchgate.net/publication/247043302_On_the_number_of_sets_of_integers_with_various_properties\"\u003eOn the number of integers with various properties\u003c/a\u003e, in R. A. Mullin, ed., Number Theory: Proc. First Con. of Canad. Number Theory Assoc. Conf., Banff, De Gruyter, Berlin, 1990, pp. 61-79.",
				"Steven R. Finch, \u003ca href=\"http://www.people.fas.harvard.edu/~sfinch/constant/cameron/sf/sf.html\"\u003eSeveral Problems Concerning Sum-Free Sets\u003c/a\u003e [Broken link]",
				"Steven R. Finch, \u003ca href=\"http://web.archive.org/web/20010604180137/http://www.mathsoft.com/asolve/constant/cameron/cameron.html\"\u003eSeveral Problems Concerning Sum-Free Sets\u003c/a\u003e [From the Wayback machine]",
				"Ben Green and Imre Z. Ruzsa, \u003ca href=\"http://arxiv.org/abs/math/0307142\"\u003eSum-free sets in abelian groups\u003c/a\u003e, arXiv:math/0307142 [math.CO], 2004.",
				"A. A. Sapozhenko, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2007.08.103\"\u003eThe Cameron-Erdős conjecture\u003c/a\u003e, Discrete Math., 308 (2008), 4361-4369.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Sum-FreeSet.html\"\u003eSum-Free Set\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A050291(n)-A088810(n) = A085489(n)-A088811(n) = A050291(n)+A085489(n)-A088813(n). - _Reinhard Zumkeller_, Oct 19 2003"
			],
			"example": [
				"{} has one sum-free subset, the empty set, so a(0)=1; {1} has two sum-free subsets, {} and {1}, so a(1)=2.",
				"a(2) = 3: 0,1,2.",
				"a(3) = 6: 0,1,2,3,13,23.",
				"a(4) = 9: 0,1,2,3,4,13,14,23,34."
			],
			"maple": [
				"S3S:= {{}}: a[0]:= 1: for n from 1 to 35 do S3S:= S3S union map(t -\u003e t union {n}, select(t -\u003e (t intersect map(q -\u003e n-q,t)={}),S3S)); a[n]:= nops(S3S) od: seq(a[n],n=0..35); # Code for computing (the number of) sum-free subsets of {1, ..., n} - _Robert Israel_"
			],
			"mathematica": [
				"SumFreeSet[ 0 ] = {{}}; SumFreeSet[ n_ ] := SumFreeSet[ n ] = Union[ SumFreeSet[ n - 1 ], Union[ #, {n} ] \u0026 /@ Select[ SumFreeSet[ n - 1 ], Intersection[ #, n - # ] == {} \u0026 ] ] As a check, enter Length /@ SumFreeSet /@ Range[ 0, 30 ] Alternatively, use NestList. n = 0; Length /@ NestList[ (++n; Union[ #, Union[ #, {n} ] \u0026 /@ Select[ #, Intersection[ #, n - # ] == {} \u0026 ] ]) \u0026, {{}}, 30 ] (* from Paul Abbott, based on _Robert Israel_'s Maple code *)",
				"Timing[ n = 0; Last[ Reap[ Nest[ (++n; Sow[ Length[ # ] ]; Union[ #, Union[ #, {n} ]\u0026 /@ Select[ #, Intersection[ #, n - # ] == {} \u0026 ] ]) \u0026, {{}}, 36 ] ] ] ] (* improved code from Paul Abbott, Nov 24 2005 *)",
				"Table[Length[Select[Subsets[Range[n]],Intersection[#,Total/@Tuples[#,2]]=={}\u0026]],{n,1,10}] (* _Gus Wiseman_, Jul 08 2019 *)"
			],
			"program": [
				"(PARI) \\\\ good only for n \u003c= 25:",
				"sumfree(v) = {for(i=1, #v, for (j=1, i, if (setsearch(v, v[i]+v[j]), return (0)););); return (1);}",
				"a(n) = {my(nb = 0); forsubset(n, s, if (sumfree(Set(s)), nb++);); nb;} \\\\ _Michel Marcus_, Nov 08 2020"
			],
			"xref": [
				"See A085489 for another version.",
				"Cf. A211316, A211317, A093970, A093971 (number of sum-full subsets of 1..n).",
				"Cf. A050291, A103580, A151897, A308546, A326020, A326080, A326083."
			],
			"keyword": "nonn,nice",
			"offset": "0,2",
			"author": "_Peter J. Cameron_",
			"ext": [
				"More terms from _John W. Layman_, Oct 21 2000",
				"Extended through 2630061 by _Robert Israel_, Nov 16 2005; two further terms from Alec Mihailovs (alec(AT)mihailovs.com) (using _Robert Israel_'s procedure), Nov 16 2005",
				"7051789 from _Eric W. Weisstein_, Nov 17 2005",
				"10747207, 14158720, 21295570, 28188520 from _Eric W. Weisstein_, Nov 28 2005, using Paul Abbott's Mathematica code"
			],
			"references": 47,
			"revision": 71,
			"time": "2020-11-08T09:22:57-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}