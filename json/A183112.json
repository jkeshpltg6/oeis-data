{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A183112",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 183112,
			"data": "0,1,4,13,38,113,336,1001,2994,8965,26868,80565,241630,724793,2174232,6522465,19567050,58700621,176101052,528301933,1584903926,4754708929,14264122464,42792360793,128377072354,385131201813,1155393582212,3466180711333,10398542080270",
			"name": "Magnetic Tower of Hanoi, total number of moves, optimally solving the [RED ; BLUE ; NEUTRAL] or [NEUTRAL ; RED ; BLUE] pre-colored puzzle.",
			"comment": [
				"The Magnetic Tower of Hanoi puzzle is described in link 1 listed below. The Magnetic Tower is pre-colored. Pre-coloring is [RED ; BLUE ; NEUTRAL] or [NEUTRAL ; RED ; BLUE], given in [Source ; Intermediate ; Destination] order. The solution algorithm producing the sequence is optimal (the sequence presented gives the minimum number of moves to solve the puzzle for the given pre-coloring configurations). Optimal solutions are discussed and their optimality is proved in link 2 listed below.",
				"Number of moves to solve the given puzzle, for large N, is close to 0.5*(10/11)*3^N ~ 0.5*0.909*3^(N). Series designation: S909(N)."
			],
			"reference": [
				"\"The Magnetic Tower of Hanoi\", Uri Levy, Journal of Recreational Mathematics, Volume 35 Number 3 (2006),  2010, pp 173."
			],
			"link": [
				"U. Levy, \u003ca href=\"http://arxiv.org/abs/1003.0225\"\u003eThe Magnetic Tower of Hanoi\u003c/a\u003e, arXiv:1003.0225",
				"Uri Levy, \u003ca href=\"http://arxiv.org/abs/1011.3843\"\u003eMagnetic Towers of Hanoi and their Optimal Solutions\u003c/a\u003e, arXiv:1011.3843",
				"Web applet \u003ca href=\"http://www.weizmann.ac.il/zemed/davidson_online/mtoh/MTOHeng.html\"\u003eto play The Magnetic Tower of Hanoi\u003c/a\u003e",
				"\u003ca href=\"/index/Rec\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (4,-2,-2,-5,6)."
			],
			"formula": [
				"Recurrence Relations (a(n)=S909(n) as in the referenced papers):",
				"a(n) = a(n-2) + a(n-3) + 3^(n-1) + 3^(n-3) + 2; n \u003e= 3 ; a(0)  = 0",
				"Closed-Form Expression:",
				"Define:",
				"λ1 = [1+sqrt(26/27)]^(1/3) +  [1-sqrt(26/27)]^(1/3)",
				"λ2 = -0.5* λ1 + 0.5*i*{[sqrt(27)+sqrt(26)]^(1/3)- [sqrt(27)-sqrt(26)]^(1/3)}",
				"λ3 = -0.5* λ1 - 0.5*i*{[sqrt(27)+sqrt(26)]^(1/3)- [sqrt(27)-sqrt(26)]^(1/3)}",
				"AS = [(7/11)* λ2* λ3 - (10/11)*(λ2 + λ3) + (19/11)]/[( λ2 - λ1)*( λ3 - λ1)]",
				"BS = [(7/11)* λ1* λ3 - (10/11)*(λ1 + λ3) + (19/11)]/[( λ1 - λ2)*( λ3 - λ2)]",
				"CS = [(7/11)* λ1* λ2 - (10/11)*(λ1 + λ2) + (19/11)]/[( λ2 - λ3)*( λ1 - λ3)]",
				"For any n \u003e 0:",
				"a(n) = (5/11)*3^n + AS* λ1^(n-1) + BS* λ2^(n-1) + CS* λ3^(n-1) - 1.",
				"G.f.: x*(1-x^2-4*x^3)/((1-x)*(1-3*x)*(1-x^2-2*x^3)); a(n)=4*a(n-1)-2*a(n-2)-2*a(n-3)-5*a(n-4)+6*a(n-5) with n\u003e5. - Bruno Berselli, Dec 29 2010"
			],
			"mathematica": [
				"LinearRecurrence[{4, -2, -2, -5, 6}, {0, 1, 4, 13, 38}, 21] (* _Jean-François Alcover_, Dec 14 2018 *)"
			],
			"xref": [
				"A183111 is an \"original\" sequence describing the number of moves of disk number k, optimally solving the pre-colored puzzle at hand. The integer sequence listed above is the partial sums of the A183111 original sequence.",
				"A003462 \"Partial sums of A000244\" is the sequence (also) describing the total number of moves solving [RED ; BLUE ; BLUE] or [RED ; RED ; BLUE] pre-colored Magnetic Tower of Hanoi puzzle.",
				"Cf. A183111 - A183125."
			],
			"keyword": "nonn",
			"offset": "0,3",
			"author": "_Uri Levy_, Dec 25 2010",
			"references": 2,
			"revision": 28,
			"time": "2018-12-14T11:48:34-05:00",
			"created": "2010-12-25T08:06:53-05:00"
		}
	]
}