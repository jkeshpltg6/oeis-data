{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A331553",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 331553,
			"data": "0,0,1,1,2,2,2,3,3,0,3,3,4,4,1,4,1,4,4,5,5,2,5,2,2,5,2,2,5,5,6,6,3,6,3,3,6,3,3,3,6,3,3,6,6,7,7,4,7,4,4,7,4,4,4,4,7,4,4,4,4,7,4,4,4,7,7,8,8,5,8,5,5,8,5,5,5,5,8,5,5,5,5,5,8,0,5",
			"name": "Irregular triangle T(n,k) = A115722(n,k)^2 - n.",
			"comment": [
				"Let P be an integer partition of n, and let D be the Durfee square of P with side length s, thus area s^2. We borrow the term \"square excess\" from A053186(n), which is simply the difference n - floor(sqrt(n)). This sequence lists the \"Durfee square excess\" of P = n - s^2 for all partitions P of n in reverse lexicographic order.",
				"Zero appears in row n for n that are perfect squares. Let r = sqrt(n). For perfect square n, there exists a partition of n that consists of a run of r parts that are each r themselves; e.g., for n = 4, we have {2, 2}, for n = 9, we have {3, 3, 3}. It is clear through the Ferrers diagram of these partitions that they are equivalent to their Durfee square, thus n - s^2 = 0.",
				"Since the partitions of any n contain Durfee squares in the range of 1 \u003c= s \u003c= floor(sqrt(n)) (with perfect square n also including k = 0), the distinct Durfee square excesses must be the differences n - s^2 for 1 \u003c= s \u003c= floor(sqrt(n))."
			],
			"link": [
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/DurfeeSquare.html\"\u003eDurfee Square.\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = A115722(n,k)^2 - n.",
				"2 * A116365(n) = sum of row n."
			],
			"example": [
				"Table begins:",
				"0: 0;",
				"1: 0;",
				"2: 1, 1;",
				"3: 2, 2, 2;",
				"4: 3, 3, 0, 3, 3;",
				"5: 4, 4, 1, 4, 1, 4, 4;",
				"6: 5, 5, 2, 5, 2, 2, 5, 2, 2, 5, 5;",
				"7: 6, 6, 3, 6, 3, 3, 6, 3, 3, 3, 6, 3, 3, 6, 6;",
				"...",
				"Table of distinct terms:",
				"1:  0;",
				"2:  1;",
				"3:  2;",
				"4:  0,  3;",
				"5:  1,  4;",
				"6:  2,  5;",
				"7:  3,  6;",
				"8:  4,  7;",
				"9:  0,  5,  8;",
				"...",
				"For n = 4, the partitions are {4}, {3, 1}, {2, 2}, {2, 1, 1}, {1, 1, 1, 1}. The partition {2, 2} has Durfee square s = 2; for all partitions except {2, 2}, we have Durfee square with s = 1."
			],
			"mathematica": [
				"{0}~Join~Array[Map[Total@ # - Block[{k = Length@ #}, While[Nand[k \u003e 0, AllTrue[Take[#, k], # \u003e= k \u0026]], k--]; k]^2 \u0026, IntegerPartitions[#]] \u0026, 12] // Flatten"
			],
			"xref": [
				"Cf. A115722, A116365."
			],
			"keyword": "nonn,tabf",
			"offset": "0,5",
			"author": "_Michael De Vlieger_, Jan 20 2020",
			"references": 0,
			"revision": 4,
			"time": "2020-01-20T12:57:14-05:00",
			"created": "2020-01-20T12:57:14-05:00"
		}
	]
}