{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A213280",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 213280,
			"data": "0,0,1,0,3,4,0,9,16,18,0,45,80,90,96,0,225,400,540,576,600,0,1575,2800,3780,4032,4200,4320,0,11025,22400,26460,32256,33600,34560,35280,0,99225,179200,238140,290304,302400,311040,317520,322560,0,893025,1792000,2381400,2612736,3024000,3110400,3175200,3225600,3265920",
			"name": "Triangle read by rows: T(n,k) (n\u003e=1, 1 \u003c= k \u003c= n) = number of permutations of [1..n] in which none of the cycle lengths are divisible by k.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A213280/b213280.txt\"\u003eRows n = 1..141, flattened\u003c/a\u003e",
				"E. D. Bolker and A. M. Gleason, \u003ca href=\"http://dx.doi.org/10.1016/0097-3165(80)90012-6\"\u003eCounting permutations\u003c/a\u003e, J. Combin. Thy., A 29 (1980), 236-242."
			],
			"example": [
				"Triangle begins",
				"[0],",
				"[0, 1],",
				"[0, 3, 4],",
				"[0, 9, 16, 18],",
				"[0, 45, 80, 90, 96],",
				"[0, 225, 400, 540, 576, 600],",
				"[0, 1575, 2800, 3780, 4032, 4200, 4320],",
				"[0, 11025, 22400, 26460, 32256, 33600, 34560, 35280],",
				"[0, 99225, 179200, 238140, 290304, 302400, 311040, 317520, 322560],",
				"[0, 893025, 1792000, 2381400, 2612736, 3024000, 3110400, 3175200, 3225600, 3265920],",
				"[0, 9823275, 19712000, 26195400, 28740096, 33264000, 34214400, 34927200, 35481600, 35925120, 36288000],",
				"[0, 108056025, 216832000, 288149400, 344881152, 365904000, 410572800, 419126400, 425779200, 431101440, 435456000, 439084800],",
				"..."
			],
			"maple": [
				"read transforms;",
				"f:=(n,d)-\u003emul(j-did(j,d),j=1..n); # did(d,j) = 1 iff j divides d, otherwise 0",
				"g:=n-\u003e[seq(f(n,d),d=1..n)];",
				"[seq(g(n),n=1..14)];",
				"# second Maple program:",
				"T:= proc(n, k) option remember; `if`(n=0, 1, add(",
				"      `if`(irem(j, k)=0, 0, binomial(n-1, j-1)*(j-1)!*",
				"       T(n-j, k)), j=1..n))",
				"    end:",
				"seq(seq(T(n, k), k=1..n), n=1..12);  # _Alois P. Heinz_, May 14 2016"
			],
			"mathematica": [
				"T[n_, k_] := T[n, k] = If[n == 0, 1, Sum[If[Mod[j, k] == 0, 0, Binomial[n - 1, j - 1]*(j - 1)!*T[n - j, k]], {j, 1, n}]];",
				"Table[T[n, k], {n, 1, 12}, {k, 1, n}] // Flatten (* _Jean-François Alcover_, May 26 2016, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Cf. A001563 (diagonal of triangle), A213279."
			],
			"keyword": "nonn,tabl",
			"offset": "1,5",
			"author": "_N. J. A. Sloane_, Jun 08 2012",
			"references": 2,
			"revision": 23,
			"time": "2016-05-26T04:00:19-04:00",
			"created": "2012-06-08T15:53:13-04:00"
		}
	]
}