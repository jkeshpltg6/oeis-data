{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A075326",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 75326,
			"data": "0,3,9,13,18,23,29,33,39,43,49,53,58,63,69,73,78,83,89,93,98,103,109,113,119,123,129,133,138,143,149,153,159,163,169,173,178,183,189,193,199,203,209,213,218,223,229,233,238,243,249,253,258,263,269,273,279,283",
			"name": "Anti-Fibonacci numbers: start with a(0) = 0, and extend by rule that the next term is the sum of the two smallest numbers that are not in the sequence nor were used to form an earlier sum.",
			"comment": [
				"In more detail, the sequence is constructed as follows: Start with a(1) = 0. The missing numbers are 1 2 3 4 5 6 ... Add the first two, and we get 3, which is therefore a(2). Cross 1, 2, and 1+2=3 off the missing list. The first two missing numbers are now 4 and 5, so a(3) = 4+5 = 9. Cross off 4,5,9 from the missing list. Repeat.",
				"In other words, this is the sum of consecutive pairs in the sequence 1, 2, 4, 5, 6, 7, 8, 10, 11, 12, 14, 15, ..., (A249031) the complement to the present one in the natural numbers. For example, a(1)=1+2=3, a(2)=4+5=9, a(3)=6+7=13, ... - Philippe Lallouet (philip.lallouet(AT)orange.fr), May 08 2008",
				"The new definition is due to Philippe Lalloue (philip.lallouet(AT)orange.fr), May 08 2008, while the name anti-Fibonacci numbers is due to D. R. Hofstadter, Oct 23 2014.",
				"Original definition: second members of pairs in A075325.",
				"If instead we take the sum of the last used non-member and the most recent (i.e. 1+2, 2+4, 4+5, 5+7, etc.), we get A008585. - _Jon Perry_, Nov 01 2014",
				"The sequences a = A075325, b = A047215, and c = A075326 are the solutions of the system of complementary equations defined recursively as follows:",
				"a(n) = least new,",
				"b(n) = least new,",
				"c(n) = a(n) + b(n),",
				"where \"least new k\" means the least positive integer not yet placed.  For anti-tribonacci numbers, see A265389; for anti-tetranacci, see A299405. - _Clark Kimberling_, May 01 2018",
				"We see the Fibonacci numbers 3, 13, 89 and 233 occur in this sequence of anti-Fibonacci numbers. Are there infinitely many Fibonacci numbers occurring in (a(n))?  The answer is yes: at least 13% of the Fibonacci numbers occur in (a(n)). This follows from Thomas Zaslavsky's formula, which implies that the sequence A017305 = (10n+3) is a subsequence of (a(n)). The Fibonacci sequence A000045 modulo 10 equals A003893, and has period 60. In this period 8 numbers 3 occur. - _Michel Dekking_, Feb 14 2019"
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A075326/b075326.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"D. R. Hofstadter, \u003ca href=\"/A075326/a075326_1.pdf\"\u003eAnti-Fibonacci numbers\u003c/a\u003e, Oct 23 2014",
				"Thomas Zaslavsky, \u003ca href=\"/A075326/a075326_2.pdf\"\u003eAnti-Fibonacci Numbers: A Formula\u003c/a\u003e, Sep 26 2016"
			],
			"formula": [
				"See Zaslavsky (2016) link."
			],
			"maple": [
				"# Maple code for M+1 terms of sequence, from _N. J. A. Sloane_, Oct 26 2014",
				"c:=0; a:=[c]; t:=0; M:=100;",
				"for n from 1 to M do",
				"s:=t+1; if s in a then s:=s+1; fi;",
				"t:=s+1; if t in a then t:=t+1; fi;",
				"c:=s+t;",
				"a:=[op(a),c];",
				"od:",
				"[seq(a[n],n=1..nops(a))];"
			],
			"mathematica": [
				"(* Three sequences a,b,c as in Comments *)",
				"z = 200;",
				"mex[list_, start_] := (NestWhile[# + 1 \u0026, start, MemberQ[list, #] \u0026]);",
				"a = {}; b = {}; c = {};",
				"Do[AppendTo[a,",
				"   mex[Flatten[{a, b, c}], If[Length[a] == 0, 1, Last[a]]]];",
				"  AppendTo[b, mex[Flatten[{a, b, c}], Last[a]]];",
				"  AppendTo[c, Last[a] + Last[b]], {z}];",
				"Take[a, 100] (* A075425 *)",
				"Take[b, 100] (* A047215 *)",
				"Take[c, 100] (* A075326 *)",
				"Grid[{Join[{\"n\"}, Range[0, 20]], Join[{\"a(n)\"}, Take[a, 21]],",
				"  Join[{\"b(n)\"}, Take[b, 21]], Join[{\"c(n)\"}, Take[c, 21]]},",
				"Alignment -\u003e \".\",",
				"Dividers -\u003e {{2 -\u003e Red, -1 -\u003e Blue}, {2 -\u003e Red, -1 -\u003e Blue}}]",
				"(* _Peter J. C. Moses_, Apr 26 2018 *)",
				"********",
				"(* Sequence \"a\" via A035263 substitutions *)",
				"Accumulate[Prepend[Flatten[Nest[Flatten[# /. {0 -\u003e {1, 1}, 1 -\u003e {1, 0}}] \u0026, {0}, 7] /. Thread[{0, 1} -\u003e {{5, 5}, {6, 4}}]], 3]]",
				"(* _Peter J. C. Moses_,  May 01 2018 *)",
				"********",
				"(* Sequence \"a\" via Hofstadter substitutions; see his 2014 link  *)",
				"morph = Rest[Nest[Flatten[#/.{1-\u003e{3},3-\u003e{1,1,3}}]\u0026,{1},6]]",
				"hoff = Accumulate[Prepend[Flatten[morph/.Thread[{1,3}-\u003e{{6,4,5,5},{6,4,6,4,6,4,5,5}}]],3]]",
				"(* _Peter J. C. Moses_,  May 01 2018 *)"
			],
			"program": [
				"(Haskell)",
				"import Data.List ((\\\\))",
				"a075326 n = a075326_list !! n",
				"a075326_list = 0 : f [1..] where",
				"   f ws@(u:v:_) = y : f (ws \\\\ [u, v, y]) where y = u + v",
				"-- _Reinhard Zumkeller_, Oct 26 2014"
			],
			"xref": [
				"Cf. A008585, A075325, A075327, A249031, A249032 (first differences), A000045.",
				"Cf. also A079523, A131323, A249031, A249032, A249406."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Amarnath Murthy_, Sep 16 2002",
			"ext": [
				"More terms from _David Wasserman_, Jan 16 2005",
				"Entry revised (including the addition of an initial 0) by _N. J. A. Sloane_, Oct 26 2014 and Sep 26 2016 (following a suggestion from _Thomas Zaslavsky_)."
			],
			"references": 22,
			"revision": 57,
			"time": "2019-02-21T04:18:29-05:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}