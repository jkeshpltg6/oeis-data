{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A118354",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 118354,
			"data": "1,1,0,1,1,0,1,2,6,0,1,3,13,42,0,1,4,21,96,325,0,1,5,30,163,770,2688,0,1,6,40,244,1353,6530,23286,0,1,7,51,340,2093,11760,57612,208659,0,1,8,63,452,3010,18636,105681,523446,1918314,0,1,9,76,581,4125,27441,170580,973953,4864795,17994264,0",
			"name": "Convolution triangle, read by rows, where diagonals are successive self-convolutions of A118351.",
			"comment": [
				"A118351 equals the central terms of pendular triangle A118350 and the lower diagonals of this triangle form the semi-diagonals of the triangle A118350."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A118354/b118354.txt\"\u003eRows n = 0..100 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"Since g.f. G=G(x) of A118351 satisfies: G = 1 - 3*x*G + 3*x*G^2 + x*G^3 then",
				"T(n,k) = T(n-1,k) - 3*T(n-1,k-1) + 3*T(n,k-1) + T(n+1,k-1).",
				"Recurrence involving antidiagonals:",
				"T(n,k) = T(n-1,k) + Sum_{j=1..k} [4*T(n-1+j,k-j) - 3*T(n-2+j,k-j)] for n\u003ek\u003e=0."
			],
			"example": [
				"Show: T(n,k) = T(n-1,k) - 3*T(n-1,k-1) + 3*T(n,k-1) + T(n+1,k-1)",
				"at n=8,k=4: T(8,4) = T(7,4) - 3*T(7,3) + 3*T(8,3) + T(9,3)",
				"or: 2093 = 1353 - 3*244 + 3*340 + 452.",
				"Triangle begins:",
				"  1;",
				"  1, 0;",
				"  1, 1,  0;",
				"  1, 2,  6,   0;",
				"  1, 3, 13,  42,    0;",
				"  1, 4, 21,  96,  325,     0;",
				"  1, 5, 30, 163,  770,  2688,      0;",
				"  1, 6, 40, 244, 1353,  6530,  23286,      0;",
				"  1, 7, 51, 340, 2093, 11760,  57612, 208659,       0;",
				"  1, 8, 63, 452, 3010, 18636, 105681, 523446, 1918314,        0;",
				"  1, 9, 76, 581, 4125, 27441, 170580, 973953, 4864795, 17994264, 0; ..."
			],
			"mathematica": [
				"T[n_, k_]:= T[n, k]= If[k==0, 1, If[k==n, 0, T[n-1, k] -3*T[n-1, k-1] +3*T[n, k-1] +T[n+1, k-1]]];",
				"Table[T[n, k], {n,0,12}, {k,0,n}]//Flatten (* _G. C. Greubel_, Feb 18 2021 *)"
			],
			"program": [
				"(PARI) {T(n,k)=polcoeff((serreverse(x*(1-3*x+sqrt((1-3*x)*(1-7*x)+x*O(x^k)))/2/(1-3*x))/x)^(n-k),k)}",
				"for(n=0,10,for(k=0,n,print1(T(n,k),\", \"));print(\"\"))",
				"(Sage)",
				"@CachedFunction",
				"def T(n, k):",
				"    if (k==0): return 1",
				"    elif (k==n): return 0",
				"    else: return T(n-1, k) - 3*T(n-1, k-1) + 3*T(n, k-1) + T(n+1, k-1)",
				"flatten([[T(n,k) for k in (0..n)] for n in (0..12)]) # _G. C. Greubel_, Feb 18 2021"
			],
			"xref": [
				"Cf. A118350, A118351, A118352, A118353.",
				"Row sums: A151616."
			],
			"keyword": "nonn,tabl",
			"offset": "0,8",
			"author": "_Paul D. Hanna_, Apr 26 2006",
			"references": 6,
			"revision": 10,
			"time": "2021-02-18T14:16:10-05:00",
			"created": "2006-05-19T03:00:00-04:00"
		}
	]
}