{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A028949",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 28949,
			"data": "1,3,4,4,5,4,4,6,4,6,4,6,6,4,6,4,6,6,4,6,4,5,8,4,10,4,4,6,6,10,4,4,6,4,6,4,4,10,6,6,4,6,10,4,6,6,6,8,4,6,8,4,6,6,8,6,6,6,6,8,6,4,10,6,6,6,4,10,6,6,4,4,8,4,6,6,6,6,6,14,4,6,6,4,6,4,4,10,4,10,6,4,10,6,10,6,10,10,4",
			"name": "Write numbers from 1 to n(n+1)/2 in a left-justified lower triangular array (a) downwards and (b) across; a(n) is number of numbers in same position in both.",
			"comment": [
				"From _Timothy L. Tiffin_, Aug 15 2021: (Start)",
				"Each a(n) also gives the number of 2-combinations from the set {1,2,3,...,n+1} that agree when written in (a) lexicographic order and (b) colexicographic order. For example, the 15 2-combinations from the set {1,2,3,4,5,6} using each order (with ** indicating agreement) would be:",
				"        (a)  12  13  14  15  16  23  24  25  26  34  35  36  45  46  56",
				"        (b)  12  13  23  14  24  34  15  25  35  45  16  26  36  46  56",
				"             **  **                      **                      **  **",
				"So, a(5) = 5. A way to visualize this correspondence is to take a 5 X 5 matrix with columns labeled 1, 2, 3, 4, 5 and rows labeled 2, 3, 4, 5, 6 and construct a left-justified lower-triangular array of 2-combinations as seen in the first diagram below:",
				"                   1  2  3  4  5                   1 2 3 4 5",
				"                  ---------------                 -----------",
				"                2| 12                           2| x",
				"                3| 13 23                        3| x o",
				"                4| 14 24 34                     4| o o o",
				"                5| 15 25 35 45                  5| o x o o",
				"                6| 16 26 36 46 56               6| o o o x x",
				"Now, traversing through this triangular array (a) downwards or (b) across will respectively generate the lexicographic ordering or the colexicographic ordering seen above. In the second diagram above, \"x\" indicates where the 2-combinations agree and \"o\" indicates where they don't. (End)"
			],
			"formula": [
				"For n\u003e1, a(n) = 2 + d(2n^2 - 6n + 5), where d(k) is number of divisors of k. - _Dean Hickerson_"
			],
			"example": [
				"For n=1, (a) = 1, (b) = 1, so a(1)=1.",
				"For n=3, (a) = 1; 2 4; 3 5 6, (b) = 1; 2 3; 4 5 6, so a(3)=4."
			],
			"mathematica": [
				"Join[{1},Table[2+DivisorSigma[0,2n^2-6n+5],{n,2,130}]] (* _Harvey P. Dale_, Jan 12 2022 *)"
			],
			"program": [
				"(PARI) a(n) = if (n==1, 1, 2 + numdiv(2*n^2 - 6*n + 5)) \\\\ _Michel Marcus_, Jun 15 2013"
			],
			"keyword": "nonn,easy,changed",
			"offset": "1,2",
			"author": "_James Propp_, Dec 11 1999",
			"ext": [
				"More terms from _David Radcliffe_",
				"\"left-justified lower\" added to the Name by _Timothy L. Tiffin_, Aug 15 2021"
			],
			"references": 0,
			"revision": 42,
			"time": "2022-01-12T18:41:48-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}