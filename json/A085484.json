{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A085484",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 85484,
			"data": "1,1,1,2,2,2,8,4,4,8,40,12,8,12,40,224,52,20,20,52,224,1368,276,72,40,72,276,1368,9008,1644,348,112,112,348,1644,9008,63488,10652,1992,460,224,460,1992,10652,63488,476160,74140,12644,2452,684,684,2452,12644,74140,476160",
			"name": "Symmetric square array, read by antidiagonals: T(k, k) = T(0, k + 1) = Sum_{m = 0..k} C(k, m)*T(m, k - m) for k \u003e= 0; T(0, 0) = 1; T(n, k) = T(n - 1, k) + T(n, k - 1) for n, k \u003e= 1.",
			"comment": [
				"The main diagonal is equal to the first row shifted left.",
				"Antidiagonal sums give A085486. First row is A085485; table is symmetric under transpose, so that first column equals the first row. Second row gives partial sums of first row."
			],
			"example": [
				"Rows begin:",
				"      1     1     2      8     40    224   1368   9008 ...",
				"      1     2     4     12     52    276   1644  10652 ...",
				"      2     4     8     20     72    348   1992  12644 ...",
				"      8    12    20     40    112    460   2452  15096 ...",
				"     40    52    72    112    224    684   3136  18232 ...",
				"    224   276   348    460    684   1368   4504  22736 ...",
				"   1368  1644  1992   2452   3136   4504   9008  31744 ...",
				"   9008 10652 12644  15096  18232  22736  31744  63488 ...",
				"  63488 74140 86784 101880 120112 142848 174592 238080 ..."
			],
			"maple": [
				"A := proc(n,k) option remember;",
				"    if n = 0 then",
				"        1",
				"    elif n \u003e 2*k then",
				"        A(n, n-k)",
				"    elif k = n then",
				"        add(binomial(n-1, i) * A(n-1, i), i = 0 .. n - 1)",
				"    else",
				"        A(n-1,k)+A(n-1,k-1)",
				"    end if",
				"end proc:",
				"for n from 0 to 6 do seq(A(n+k,k), k=0..12) od; # _Yu-Sheng Chang_, Jan 16 2020"
			],
			"program": [
				"(PARI) A85484=Map(); A085484(n,k)={if(n\u003ek, [n,k]=[k,n], !k, return(1), n==k, n=!k++); mapisdefined(A85484, [n,k])|| mapput(A85484,[n,k], if(n, A085484(n-1,k)+A085484(n,k-1), sum(m=0,k-1, binomial(k-1,m)*A085484(m,k-1-m)))); mapget(A85484,[n,k])} \\\\ _M. F. Hasler_, Feb 17 2020"
			],
			"xref": [
				"Cf. A085485 (first row and diagonal), A085486 (antidiagonal sums)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,4",
			"author": "_Paul D. Hanna_, Jul 02 2003",
			"ext": [
				"a(17)-a(18) corrected by _Yu-Sheng Chang_, Jan 16 2020",
				"Name edited by _M. F. Hasler_, Feb 17 2020"
			],
			"references": 3,
			"revision": 34,
			"time": "2020-03-13T11:37:57-04:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}