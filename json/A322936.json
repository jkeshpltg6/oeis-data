{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A322936",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 322936,
			"data": "1,0,0,0,3,0,4,5,3,5,5,7,7,3,4,6,7,8,9,5,7,5,7,8,9,10,11,3,5,9,11,4,8,11,13,7,9,11,13,3,5,6,7,9,10,11,12,13,14,15,5,7,11,13,4,5,7,8,10,11,12,13,14,15,16,17,3,7,9,11,13,17,8,11,13,16,17,19",
			"name": "Triangular array in which the n-th row lists the numbers strongly prime to n (in ascending order). For the empty rows n = 2, 3, 4 and 6 we set by convention 0.",
			"comment": [
				"a is strongly prime to n if and only if a \u003c= n is prime to n and a does not divide n-1. See the link to 'Strong Coprimality'. (Our terminology follows the plea of Knuth, Graham and Patashnik in Concrete Mathematics, p. 115.)"
			],
			"link": [
				"Peter Luschny, \u003ca href=\"http://oeis.org/wiki/User:Peter_Luschny/StrongCoprimality\"\u003eStrong Coprimality\u003c/a\u003e"
			],
			"example": [
				"The length of row n is A181830(n) = phi(n) - tau(n-1). The triangular array starts:",
				"[1] {1}",
				"[2] {}",
				"[3] {}",
				"[4] {}",
				"[5] {3}",
				"[6] {}",
				"[7] {4, 5}",
				"[8] {3, 5}",
				"[9] {5, 7}",
				"[11] {3, 4, 6, 7, 8, 9}",
				"[12] {5, 7}",
				"[10] {7}",
				"[13] {5, 7, 8, 9, 10, 11}",
				"[14] {3, 5, 9, 11}",
				"[15] {4, 8, 11, 13}",
				"[16] {7, 9, 11, 13}",
				"[17] {3, 5, 6, 7, 9, 10, 11, 12, 13, 14, 15}",
				"[18] {5, 7, 11, 13}",
				"[19] {4, 5, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17}",
				"[20] {3, 7, 9, 11, 13, 17}"
			],
			"maple": [
				"StrongCoprimes := n -\u003e select(k -\u003e igcd(k, n)=1, {$1..n}) minus numtheory:-divisors(n-1):",
				"A322936row:=  proc(n) if n in {2, 3, 4, 6} then return 0 else op(StrongCoprimes(n)) fi end:",
				"seq(A322936row(n), n=1..20);"
			],
			"mathematica": [
				"Table[If[n == 1, {1}, Select[Range[2, n], And[GCD[#, n] == 1, Mod[n - 1, #] != 0] \u0026] /. {} -\u003e {0}], {n, 21}] // Flatten (* _Michael De Vlieger_, Apr 01 2019 *)"
			],
			"program": [
				"(Sage)",
				"def primeto(n):",
				"    return [p for p in range(n) if gcd(p, n) == 1]",
				"def strongly_primeto(n):",
				"    return [p for p in set(primeto(n)) - set((n-1).divisors())]",
				"def A322936row(n):",
				"    if n == 1: return [1]",
				"    if n in [2, 3, 4, 6]: return [0]",
				"    return sorted(strongly_primeto(n))",
				"for n in (1..21): print(A322936row(n))"
			],
			"xref": [
				"Cf. A000010, A038566, A181831, A181832, A181833, A181834, A181835, A181836, A322937."
			],
			"keyword": "nonn,tabf",
			"offset": "1,5",
			"author": "_Peter Luschny_, Apr 01 2019",
			"references": 2,
			"revision": 19,
			"time": "2019-04-02T17:53:06-04:00",
			"created": "2019-04-02T05:43:14-04:00"
		}
	]
}