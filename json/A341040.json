{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A341040",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 341040,
			"data": "1,0,1,0,0,0,1,0,0,1,0,0,0,0,1,0,0,1,0,0,0,0,1,0,0,0,1,0,0,1,0,0,1,0,0,0,0,1,0,0,0,1,0,0,0,0,1,1,0,0,1,1,0,0,0,0,1,1,0,0,0,1,1,0,0,0,0,0,1,0,0,0,1,0,1,0,0,1,0,0,0,1,0,0,0,0,1,0,0,1,0,0,1,1,0,0,0,1,0,0,0,0,1,1",
			"name": "Number T(n,k) of partitions of n into k distinct nonzero squares; triangle T(n,k), n\u003e=0, 0\u003c=k\u003c=A248509(n), read by rows.",
			"comment": [
				"T(n,k) is defined for n, k \u003e= 0.  The triangle contains only the terms with 0 \u003c= k \u003c= A248509(n).  T(n,k) = 0 for k \u003e A248509(n)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A341040/b341040.txt\"\u003eRows n = 0..4000, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n,k) = [x^n*y^k] Product_{j\u003e=1} (1 + y*x^(j^2)).",
				"T(A000330(n),n) = 1.",
				"Row n = [0] \u003c=\u003e n in { A001422 }.",
				"Sum_{k\u003e=0} 2^k * T(n,k) = A279360(n).",
				"Sum_{k\u003e=0} k * T(n,k) = A281542(n).",
				"Sum_{k\u003e=0} (-1)^k * T(n,k) = A276516(n)."
			],
			"example": [
				"T(62,3) = 2 is the first term \u003e 1 and counts partitions [49,9,4] and [36,25,1].",
				"Triangle T(n,k) begins:",
				"  1;",
				"  0, 1;",
				"  0;",
				"  0;",
				"  0, 1;",
				"  0, 0, 1;",
				"  0;",
				"  0;",
				"  0;",
				"  0, 1;",
				"  0, 0, 1;",
				"  0;",
				"  0;",
				"  0, 0, 1;",
				"  0, 0, 0, 1;",
				"  ..."
			],
			"maple": [
				"b:= proc(n, i) option remember; `if`(n=0, 1, `if`(i\u003c1, 0,",
				"      b(n, i-1)+`if`(i^2\u003en, 0, expand(b(n-i^2, i-1)*x))))",
				"    end:",
				"T:= n-\u003e(p-\u003eseq(coeff(p, x, i), i=0..max(0, degree(p))))(b(n, isqrt(n))):",
				"seq(T(n), n=0..45);"
			],
			"mathematica": [
				"b[n_, i_] := b[n, i] = If[n == 0, 1, If[i \u003c 1, 0,",
				"b[n, i - 1] + If[i^2 \u003e n, 0, Expand[b[n - i^2, i - 1]*x]]]];",
				"T[n_] := CoefficientList[b[n, Floor@Sqrt[n]], x] /. {} -\u003e {0};",
				"T /@ Range[0, 45] // Flatten (* _Jean-François Alcover_, Feb 15 2021, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Columns k=0-10 give: A000007, A010052 (for n\u003e0), A025441, A025442, A025443, A025444, A340988, A340998, A340999, A341000, A341001.",
				"Row sums give A033461.",
				"Cf. A000290, A000330, A001422, A243148, A248509, A276516, A279360, A281542, A337165."
			],
			"keyword": "nonn,look,tabf",
			"offset": "0",
			"author": "_Alois P. Heinz_, Feb 03 2021",
			"references": 19,
			"revision": 40,
			"time": "2021-02-15T07:27:11-05:00",
			"created": "2021-02-03T15:37:34-05:00"
		}
	]
}