{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A091187",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 91187,
			"data": "1,1,1,1,2,2,1,3,6,4,1,4,12,16,9,1,5,20,40,45,21,1,6,30,80,135,126,51,1,7,42,140,315,441,357,127,1,8,56,224,630,1176,1428,1016,323,1,9,72,336,1134,2646,4284,4572,2907,835,1,10,90,480,1890,5292,10710,15240,14535",
			"name": "Triangle read by rows: T(n,k) is the number of ordered trees with n edges and k branches.",
			"comment": [
				"Row sums are the Catalan numbers A000108. Diagonal entries are the Motzkin numbers A001006.",
				"Equals binomial transform of an infinite lower triangular matrix with A001006 as the main diagonal and the rest zeros. [_Gary W. Adamson_, Dec 31 2008] [Corrected by _Paul Barry_, Mar 06 2011]",
				"Reversal of A091869. Diagonal sums are A026418(n+2). [_Paul Barry_, Mar 06 2011]"
			],
			"link": [
				"J.-L. Baril, S. Kirgizov, \u003ca href=\"http://jl.baril.u-bourgogne.fr/Stirling.pdf\"\u003eThe pure descent statistic on permutations\u003c/a\u003e, Preprint, 2016.",
				"J. Riordan, \u003ca href=\"http://dx.doi.org/10.1016/S0097-3165(75)80010-0\"\u003eEnumeration of plane trees by branches and endpoints\u003c/a\u003e, J. Combinat. Theory, Ser A, 19, 214-222, 1975."
			],
			"formula": [
				"T(n,k) = M(k-1)*binomial(n-1, k-1), where M(k) = A001006(k) = (Sum_{q=0..ceiling((k+1)/2)} binomial(k+1, q)*binomial(k+1-q, q-1))/(k+1) is a Motzkin number.",
				"G.f.: G = G(t,z) satisfies t*z*G^2 -(1 - z + t*z)*G + 1- z + t*z = 0.",
				"From _Paul Barry_, Mar 06 2011: (Start)",
				"G.f.: 1/(1-x-xy-x^2y^2/(1-x-xy-x^2y^2/(1-x-xy-x^2y^2/(1-... (continued fraction).",
				"G.f.: (1-x(1+y)-sqrt(1-2x(1+y)+x^2(1+2y-3y^2)))/(2x^2*y^2).",
				"E.g.f.: exp(x(1+y))*Bessel_I(1,2*x*y)/(x*y). (End)"
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  1, 1;",
				"  1, 2,  2;",
				"  1, 3,  6,   4;",
				"  1, 4, 12,  16,   9;",
				"  1, 5, 20,  40,  45,  21;",
				"  1, 6, 30,  80, 135, 126,  51;",
				"  1, 7, 42, 140, 315, 441, 357, 127;"
			],
			"maple": [
				"M := n-\u003esum(binomial(n+1,q)*binomial(n+1-q,q-1),q=0..ceil((n+1)/2))/(n+1): T := (n,k)-\u003ebinomial(n-1,k-1)*M(k-1): seq(seq(T(n,k),k=1..n),n=1..13);"
			],
			"mathematica": [
				"(* m = MotzkinNumber *) m[0] = 1; m[n_] := m[n] = m[n - 1] + Sum[m[k]*m[n - 2 - k], {k, 0, n - 2}]; t[n_, k_] := m[k - 1]*Binomial[n - 1, k - 1]; Table[t[n, k], {n, 1, 11}, {k, 1, n}] // Flatten (* _Jean-François Alcover_, Jul 10 2013 *)"
			],
			"xref": [
				"Cf. A001006, A000108.",
				"Cf. A007476. [_Gary W. Adamson_, Dec 31 2008]"
			],
			"keyword": "nonn,tabl",
			"offset": "1,5",
			"author": "_Emeric Deutsch_, Feb 23 2004",
			"references": 1,
			"revision": 27,
			"time": "2019-06-16T08:07:26-04:00",
			"created": "2004-06-12T03:00:00-04:00"
		}
	]
}